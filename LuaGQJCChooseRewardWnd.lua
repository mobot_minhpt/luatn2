require("common/common_include")
local CItem=import "L10.Game.CItem"
local UIProgressBar = import "UIProgressBar"
local UIGrid = import "UIGrid"
local IdPartition = import "L10.Game.IdPartition"
local CUITexture = import "L10.UI.CUITexture"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
local Item_Item = import "L10.Game.Item_Item"
local Input = import "UnityEngine.Input"
local Application = import "UnityEngine.Application"
local RuntimePlatform = import "UnityEngine.RuntimePlatform"
local Vector3 = import "UnityEngine.Vector3"
local TouchPhase = import "UnityEngine.TouchPhase"
local CUICommonDef = import "L10.UI.CUICommonDef"
local MessageMgr = import "L10.Game.MessageMgr"
local MessageWndManager = import "L10.UI.MessageWndManager"
local PlayerSettings = import "L10.Game.PlayerSettings"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType=import "L10.UI.CItemInfoMgr+AlignType"
local EnumItemPlace = import "L10.Game.EnumItemPlace"

LuaGQJCChooseRewardWnd = class()

RegistClassMember(LuaGQJCChooseRewardWnd, "TimeProgressBar")
RegistClassMember(LuaGQJCChooseRewardWnd, "TimeCountDownLabel")
RegistClassMember(LuaGQJCChooseRewardWnd, "RewardGrid")
RegistClassMember(LuaGQJCChooseRewardWnd, "RewardTemplate")
RegistClassMember(LuaGQJCChooseRewardWnd, "CloseBtn")

RegistClassMember(LuaGQJCChooseRewardWnd, "ChosenSkill1")
RegistClassMember(LuaGQJCChooseRewardWnd, "ChosenSkill2")
RegistClassMember(LuaGQJCChooseRewardWnd, "ChosenDrug")

RegistClassMember(LuaGQJCChooseRewardWnd, "CloneSkill")
RegistClassMember(LuaGQJCChooseRewardWnd, "CloneDrug")

RegistClassMember(LuaGQJCChooseRewardWnd, "ChooseInterval")
RegistClassMember(LuaGQJCChooseRewardWnd, "CurrentTime")
RegistClassMember(LuaGQJCChooseRewardWnd, "ChooseTick")

RegistClassMember(LuaGQJCChooseRewardWnd, "CandidateInfos")
RegistClassMember(LuaGQJCChooseRewardWnd, "SelectionInfos")

RegistClassMember(LuaGQJCChooseRewardWnd, "Dragging")
RegistClassMember(LuaGQJCChooseRewardWnd, "LastPos")
RegistClassMember(LuaGQJCChooseRewardWnd, "FingerIndex")
RegistClassMember(LuaGQJCChooseRewardWnd, "SelectedId")

function LuaGQJCChooseRewardWnd:Init()
	self:InitClassMembers()
	self:InitValues()

end

function LuaGQJCChooseRewardWnd:InitClassMembers()
	self.TimeProgressBar = self.transform:Find("Anchor/HeadView/TimeProgress"):GetComponent(typeof(UIProgressBar))
	self.TimeCountDownLabel = self.transform:Find("Anchor/HeadView/TimeCountDownLabel"):GetComponent(typeof(UILabel))
	self.RewardGrid = self.transform:Find("Anchor/Content/RewardGrid"):GetComponent(typeof(UIGrid))
	self.RewardTemplate = self.transform:Find("Anchor/Content/RewardTemplate").gameObject
	self.RewardTemplate:SetActive(false)
	self.CloseBtn = self.transform:Find("Wnd_Bg_Secondary_3/CloseButton").gameObject

	self.ChosenSkill1 = self.transform:Find("Anchor/ChosenView/ChosenSkill1").gameObject
	self.ChosenSkill2 = self.transform:Find("Anchor/ChosenView/ChosenSkill2").gameObject
	self.ChosenDrug = self.transform:Find("Anchor/ChosenView/ChosenDrug").gameObject

	self.CloneSkill = self.transform:Find("Anchor/CloneChoosePanel/CloneSkill").gameObject
	self.CloneSkill:SetActive(false)
	self.CloneDrug = self.transform:Find("Anchor/CloneChoosePanel/CloneDrug").gameObject
	self.CloneDrug:SetActive(false)

end

function LuaGQJCChooseRewardWnd:InitValues()

	self.ChooseInterval = 15
	self.CurrentTime = self.ChooseInterval
	self.CandidateInfos = {}
	self.SelectionInfos = {}
	self.TimeProgressBar.value = 1


	self.Dragging = false
	self.FingerIndex = -1

	self.ChooseTick = RegisterTickWithDuration(function ()
		if self.CurrentTime > 0 then
			self.CurrentTime = self.CurrentTime - 1
			self.TimeCountDownLabel.text = SafeStringFormat3(LocalString.GetString("[FF5050]%d秒后自动关闭[-]"), self.CurrentTime)
			self.TimeProgressBar.value = self.CurrentTime / self.ChooseInterval
		else
			CUIManager.CloseUI("GQJCChooseRewardWnd")
		end
	end, 1000, 1000 * (self.ChooseInterval+1))

	local onCloseClicked = function (go)
		self:OnCloseBtnClicked(go)
	end
	CommonDefs.AddOnClickListener(self.CloseBtn,DelegateFactory.Action_GameObject(onCloseClicked),false)
	self:UpdateRewardInfos(CLuaGuoQingJiaoChangMgr.m_RewardCandidates, CLuaGuoQingJiaoChangMgr.m_RewardSelections)


	local ChoseSkill1Icon = self.ChosenSkill1.transform:Find("ChosenSkillIcon").gameObject
	local choseSkill2Icon = self.ChosenSkill2.transform:Find("ChosenSkillIcon").gameObject
	local chosenDrugIcon = self.ChosenDrug.transform:Find("ChosenDrugIcon").gameObject

	local onChosenSkill1Clicked = function (go)
		self:OnChosenSkillClicked(go, 1)
	end
	CommonDefs.AddOnClickListener(ChoseSkill1Icon, DelegateFactory.Action_GameObject(onChosenSkill1Clicked), false)

	local onChosenSkill2Clicked = function (go)
		self:OnChosenSkillClicked(go, 2)
	end
	CommonDefs.AddOnClickListener(choseSkill2Icon, DelegateFactory.Action_GameObject(onChosenSkill2Clicked), false)

	local onChosenDrugClicked = function (go)
		-- body
	end
	CommonDefs.AddOnClickListener(chosenDrugIcon, DelegateFactory.Action_GameObject(onChosenDrugClicked), false)
end

function LuaGQJCChooseRewardWnd:OnChosenSkillClicked(go, index)
	if self.SelectionInfos[index] and self.SelectionInfos[index] ~=0 then
		CSkillInfoMgr.ShowSkillInfoWnd(self.SelectionInfos[index], true, 0, 0, nil)
	end
end

function LuaGQJCChooseRewardWnd:OnChosenDrugClicked()
	if self.SelectionInfos[3] and self.SelectionInfos[3] ~= 0 then
		CItemInfoMgr.ShowLinkItemTemplateInfo(self.SelectionInfos[3], false, nil, AlignType.Default, 0, 0, 0, 0)
	end
end

function LuaGQJCChooseRewardWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateGQJCReward", self, "UpdateRewardInfos")
end

function LuaGQJCChooseRewardWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateGQJCReward", self, "UpdateRewardInfos")
	if self.ChooseTick ~= nil then
      UnRegisterTick(self.ChooseTick)
      self.ChooseTick=nil
    end
end

function LuaGQJCChooseRewardWnd:OnCloseBtnClicked(go)
	-- GQJCRewardCloseConfirm
	if PlayerSettings.GQJCRewardCloseConfirm then
		local msg = g_MessageMgr:FormatMessage("GQJC_CLOSE_REWARD_CONFIRM")
		local onOKClicked = function (notNeedConfirm)
			PlayerSettings.GQJCRewardCloseConfirm = not notNeedConfirm
			-- type, pos, id
			Gac2Gas.RequestSelectTempGQJC(0, 0, 0)
			CUIManager.CloseUI("GQJCChooseRewardWnd")
		end

		local onCancelClicked = function (notNeedConfirm)
			PlayerSettings.GQJCRewardCloseConfirm = not notNeedConfirm
		end
		MessageWndManager.ShowMessageWithCheckBox(msg, LocalString.GetString("下次不再提示"), false, false, 0, 
				DelegateFactory.Action_bool(onOKClicked), DelegateFactory.Action_bool(onCancelClicked), nil, nil)
	else
		Gac2Gas.RequestSelectTempGQJC(0, 0, 0)
	end			
end


function LuaGQJCChooseRewardWnd:UpdateRewardInfos(candidates, selections)
	if not candidates or not selections then
		CUIManager.CloseUI("GQJCChooseRewardWnd")
		return
	end
	self.CandidateInfos = candidates
	self.SelectionInfos = selections
	self:InitCandidates()
	self:InitSelections()

end


function LuaGQJCChooseRewardWnd:InitCandidates()
	CUICommonDef.ClearTransform(self.RewardGrid.transform)
	for k, v in ipairs(self.CandidateInfos) do
		local go = NGUITools.AddChild(self.RewardGrid.gameObject, self.RewardTemplate)
		go:SetActive(true)
		self:InitCandidateItem(go, v)
	end
	self.RewardGrid:Reposition()
end

function LuaGQJCChooseRewardWnd:InitCandidateItem(go, info)
	local skillObject = go.transform:Find("RewardSkillIcon").gameObject
	local drugObject = go.transform:Find("RewardDrugIcon").gameObject

	local skillIcon = go.transform:Find("RewardSkillIcon/Mask/Icon"):GetComponent(typeof(CUITexture))
	local drugIcon = go.transform:Find("RewardDrugIcon/Mask/Icon"):GetComponent(typeof(CUITexture))
	skillIcon:Clear()
	drugIcon:Clear()

	local rewardNameLabel = go.transform:Find("RewardName"):GetComponent(typeof(UILabel))
	local descLabel = go.transform:Find("DescPanel/DescLabel"):GetComponent(typeof(UILabel))

	if IdPartition.IdIsSkill(info) then
		skillObject:SetActive(true)
		drugObject:SetActive(false)
		local skill = Skill_AllSkills.GetData(info)
		if skill then
			skillIcon:LoadSkillIcon(skill.SkillIcon)
			rewardNameLabel.text = skill.Name
			descLabel.text = skill.Display
		end
	else
		skillObject:SetActive(false)
		drugObject:SetActive(true)
		local item = Item_Item.GetData(info)
		if item then
			drugIcon:LoadMaterial(item.Icon)
			rewardNameLabel.text = item.Name
			descLabel.text = CItem.GetItemDescription(info,true)
		end
	end

	-- 处理拖动函数
	local onDragSkill = function (go)
		self:OnDragSkillIcon(go, info)
	end
	CommonDefs.AddOnDragStartListener(skillObject, DelegateFactory.Action_GameObject(onDragSkill), false)

	local onDragDrug = function (go)
		self:OnDragDrugIcon(go, info)
	end
	CommonDefs.AddOnDragStartListener(drugObject, DelegateFactory.Action_GameObject(onDragDrug), false)
end

-- 显示已经选择的奖品
function LuaGQJCChooseRewardWnd:InitSelections()
	for i = 1, #self.SelectionInfos, 1 do
		if i == 1 then
			self:InitSelectionItem_Skill(self.ChosenSkill1, self.SelectionInfos[i], i)
		elseif i == 2 then
			self:InitSelectionItem_Skill(self.ChosenSkill2, self.SelectionInfos[i], i)
		elseif i ==3 then
			self:InitSelectionItem_Drug(self.ChosenDrug, self.SelectionInfos[i])
		end
	end
end

function LuaGQJCChooseRewardWnd:InitSelectionItem_Skill(go, info, index)
	local skillIcon = go.transform:Find("ChosenSkillIcon/Mask/Icon"):GetComponent(typeof(CUITexture))
	skillIcon:Clear()
	local skillName = go.transform:Find("SkillName"):GetComponent(typeof(UILabel))
	skillName.text = SafeStringFormat3(LocalString.GetString("临时技能栏%s"), tostring(index))

	local skill = Skill_AllSkills.GetData(info)
	if not skill then return end
	skillIcon:LoadSkillIcon(skill.SkillIcon)
	skillName.text = skill.Name
end

function LuaGQJCChooseRewardWnd:InitSelectionItem_Drug(go, info)
	local drugIcon = go.transform:Find("ChosenDrugIcon/Mask/Icon"):GetComponent(typeof(CUITexture))
	drugIcon:Clear()
	local drugName = go.transform:Find("DrugName"):GetComponent(typeof(UILabel))
	drugName.text = LocalString.GetString("药品栏")

	local item = Item_Item.GetData(info)
	if not item then return end
	drugIcon:LoadMaterial(item.Icon)
	drugName.text = item.Name
end

function LuaGQJCChooseRewardWnd:OnDragSkillIcon(go, info)
	local skill = Skill_AllSkills.GetData(info)
	if not skill then return end

	self.SelectedId = info

	local skillIcon = self.CloneSkill.transform:Find("Mask/Icon"):GetComponent(typeof(CUITexture))
	skillIcon:LoadSkillIcon(skill.SkillIcon)
	self.CloneSkill:SetActive(true)

	self:StartDrag()
end

function LuaGQJCChooseRewardWnd:OnDragDrugIcon(go, info)
	local item = Item_Item.GetData(info)
	if not item then return end

	self.SelectedId = info

	local drugIcon = self.CloneDrug.transform:Find("Mask/Icon"):GetComponent(typeof(CUITexture))
	drugIcon:LoadMaterial(item.Icon)
	self.CloneDrug:SetActive(true)

	self:StartDrag()
end

function LuaGQJCChooseRewardWnd:StartDrag()
	self.Dragging = true
	self.LastPos = Input.mousePosition

	if Application.platform == RuntimePlatform.IPhonePlayer or Application.platform == RuntimePlatform.Android then
		self.FingerIndex = Input.GetTouch(0).fingerId
		local pos = Input.GetTouch(0).position
		self.LastPos = Vector3(pos.x, pos.y, 0)
    end
end

function LuaGQJCChooseRewardWnd:Update()
	if not self.Dragging then
        return
    end

    if Application.platform == RuntimePlatform.IPhonePlayer or Application.platform == RuntimePlatform.Android then
        do
            local i = 0
            while i < Input.touchCount do
                local touch = Input.GetTouch(i)
                if touch.fingerId == self.FingerIndex then
                    if touch.phase ~= TouchPhase.Ended and touch.phase ~= TouchPhase.Canceled then
                        if touch.phase ~= TouchPhase.Stationary then
                            local tempPos = Vector3(touch.position.x, touch.position.y, 0)
                        	local pos = CUIManager.UIMainCamera:ScreenToWorldPoint(tempPos)
                            self.CloneSkill.transform.position = pos
                            self.CloneDrug.transform.position = pos
                            self.LastPos = pos
                        end
                        return
                    else
                        break
                    end
                end
                i = i + 1
            end
        end
    else
        if Input.GetMouseButton(0) then
        	local pos = CUIManager.UIMainCamera:ScreenToWorldPoint(Input.mousePosition)
            self.CloneSkill.transform.position = pos
            self.CloneDrug.transform.position = pos
            self.LastPos = Input.mousePosition
            if not Input.GetMouseButtonUp(0) then
                return
            end
        end
    end

    self.CloneSkill:SetActive(false)
    self.CloneDrug:SetActive(false)
    self.Dragging = false
    self.FingerIndex = -1

    local hoveredObject = CUICommonDef.SelectedUIWithRacast
    self:ToChooseReward(hoveredObject)


end

function LuaGQJCChooseRewardWnd:ToChooseReward(go)
	local ChoseSkill1Icon = self.ChosenSkill1.transform:Find("ChosenSkillIcon").gameObject
	local choseSkill2Icon = self.ChosenSkill2.transform:Find("ChosenSkillIcon").gameObject
	local chosenDrugIcon = self.ChosenDrug.transform:Find("ChosenDrugIcon").gameObject

	--RequestSelectTempGQJC
	if go:Equals(ChoseSkill1Icon) then
		self:OnChooseSkill(1)
	elseif go:Equals(choseSkill2Icon) then
		self:OnChooseSkill(2)
	elseif go:Equals(chosenDrugIcon) then
		self:OnChooseDrug()
	end
end

function LuaGQJCChooseRewardWnd:OnChooseSkill(index)
	if IdPartition.IdIsSkill(self.SelectedId) then
		local skill = Skill_AllSkills.GetData(self.SelectedId)
		if not skill then return end

		local skillIcon = self[SafeStringFormat3("ChosenSkill%s", tostring(index))].transform:Find("ChosenSkillIcon/Mask/Icon"):GetComponent(typeof(CUITexture))
		local skillName = self[SafeStringFormat3("ChosenSkill%s", tostring(index))].transform:Find("SkillName"):GetComponent(typeof(UILabel))

		skillName.text = skill.Name
		skillIcon:LoadSkillIcon(skill.SkillIcon)

		if PlayerSettings.GQJCRewardSecondConfirm then
			local msg = SafeStringFormat3(LocalString.GetString("确认将[FFFF00]%s[-]放在临时技能栏%s吗？"), skill.Name, tostring(index))
			-- 该位置已经有技能了
			if self.SelectionInfos[index] ~=0 then
				local oldSkill = Skill_AllSkills.GetData(self.SelectionInfos[index])
				if oldSkill then
					msg = SafeStringFormat3(LocalString.GetString("确认将[FFFF00]%s[-]替换为[FFFF00]%s[-]吗？"), oldSkill.Name, skill.Name)
				end
			end

			local onOKClicked = function (notNeedConfirm)
				PlayerSettings.GQJCRewardSecondConfirm = not notNeedConfirm
				-- type, pos, id
				Gac2Gas.RequestSelectTempGQJC(1, index, self.SelectedId)
			end

			local onCancelClicked = function (notNeedConfirm)
				PlayerSettings.GQJCRewardSecondConfirm = not notNeedConfirm
				-- 取消skillIcon的显示
				skillName.text = SafeStringFormat3(LocalString.GetString("临时技能栏%s"), tostring(index))
				skillIcon:Clear()
				local oldSkill = Skill_AllSkills.GetData(self.SelectionInfos[index])
				if oldSkill then
					skillName.text = oldSkill.Name
					skillIcon:LoadSkillIcon(oldSkill.SkillIcon)
				end
			end

			MessageWndManager.ShowMessageWithCheckBox(msg, LocalString.GetString("下次不再提示"), false, false, 0, 
				DelegateFactory.Action_bool(onOKClicked), DelegateFactory.Action_bool(onCancelClicked), nil, nil)
		else
			Gac2Gas.RequestSelectTempGQJC(1, index, self.SelectedId)
		end
	else
		MessageMgr.Inst:ShowMessage("GQJC_DRAG_TO_DRUG", {})
	end
end

function LuaGQJCChooseRewardWnd:OnChooseDrug()
	if IdPartition.IdIsItem(self.SelectedId) then
		local item = Item_Item.GetData(self.SelectedId)
		if not item then return end

		-- 增加对包裹空余位置的检查
		if CClientMainPlayer.Inst.ItemProp:GetEmptyPosCount(EnumItemPlace.Bag) < 1 then
			MessageMgr.Inst:ShowMessage("NEED_BAG_SPACE", {1})
			return
		end


		local drugIcon = self.ChosenDrug.transform:Find("ChosenDrugIcon/Mask/Icon"):GetComponent(typeof(CUITexture))
		local drugName = self.ChosenDrug.transform:Find("DrugName"):GetComponent(typeof(UILabel))

		drugIcon:LoadMaterial(item.Icon)
		drugName.text = item.Name

		if PlayerSettings.GQJCRewardSecondConfirm then
			local msg = SafeStringFormat3(LocalString.GetString("确认将[FFFF00]%s[-]放在药品栏吗？"), item.Name)

			-- 该位置已经有药品了
			if self.SelectionInfos[3] ~= 0 then
				local oldItem = Item_Item.GetData(self.SelectionInfos[3])
				if oldItem then
					msg = SafeStringFormat3(LocalString.GetString("确认将[FFFF00]%s[-]替换为[FFFF00]%s[-]吗？"), oldItem.Name, item.Name)
				end
			end
			local onOKClicked = function (notNeedConfirm)
				PlayerSettings.GQJCRewardSecondConfirm = not notNeedConfirm
				-- type, pos, id
				Gac2Gas.RequestSelectTempGQJC(2, 3, self.SelectedId)
			end

			local onCancelClicked = function (notNeedConfirm)
				PlayerSettings.GQJCRewardSecondConfirm = not notNeedConfirm
				drugName.text = LocalString.GetString("药品栏")
				drugIcon:Clear()
				local oldItem = Item_Item.GetData(self.SelectionInfos[3])
				if oldItem then
					drugIcon:LoadMaterial(oldItem.Icon)
					drugName.text = oldItem.Name
				end
			end

			MessageWndManager.ShowMessageWithCheckBox(msg, LocalString.GetString("下次不再提示"), false, false, 0, 
				DelegateFactory.Action_bool(onOKClicked), DelegateFactory.Action_bool(onCancelClicked), nil, nil)
		else
			Gac2Gas.RequestSelectTempGQJC(2, 3, self.SelectedId)
		end

	else
		MessageMgr.Inst:ShowMessage("GQJC_DRAG_TO_SKILL", {})
	end
end

return LuaGQJCChooseRewardWnd
