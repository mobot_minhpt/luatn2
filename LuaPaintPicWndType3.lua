local Object=import "System.Object"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local UISprite = import "UISprite"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CUITexture = import "L10.UI.CUITexture"
local CPaintTextureDLN = import "L10.UI.CPaintTextureDLN"

LuaPaintPicWndType3 = class()
RegistChildComponent(LuaPaintPicWndType3,"closeBtn", GameObject)
RegistChildComponent(LuaPaintPicWndType3,"bgTexture", CUITexture)
RegistChildComponent(LuaPaintPicWndType3,"paintNode", GameObject)
RegistChildComponent(LuaPaintPicWndType3,"upTexture", CUITexture)

RegistClassMember(LuaPaintPicWndType3, "taskID")
RegistClassMember(LuaPaintPicWndType3, "taskData")
RegistClassMember(LuaPaintPicWndType3, "tick")

function LuaPaintPicWndType3:Close()
	CUIManager.CloseUI(CLuaUIResources.PaintPicWndType3)
end

function LuaPaintPicWndType3:Finish()
	--Gac2Gas.FinishTaskPainting(self.taskID)
	local empty = CreateFromClass(MakeGenericClass(List, Object))
	Gac2Gas.FinishClientTaskEvent(self.taskID,"PaintingType3",MsgPackImpl.pack(empty))
	RegisterTickWithDuration(function ()
		self:Close()
	end,2000,2000)
end

function LuaPaintPicWndType3:SetStateStart(state)
	if self.taskID then
		local count = ZhuJueJuQing_Painting.GetDataCount()
		for i=1,count do
			local data = ZhuJueJuQing_Painting.GetData(i)
			if data and data.TaskID == self.taskID then
				self.taskData = data
				break
			end
		end
	end

	if self.taskData then
		self.bgTexture:LoadMaterial("UI/Texture/Transparent/Material/" .. self.taskData.BackgroundText .. ".mat",false)
		self.upTexture:LoadTexture("UI/Texture/Transparent/" .. self.taskData.CoverText .. ".png",false)
		self.upTexture.gameObject:SetActive(true)
		self.bgTexture.gameObject:SetActive(false)

		local paintTexture = self.upTexture.transform.parent:GetComponent(typeof(CPaintTextureDLN))
		UnRegisterTick(self.tick)
		self.tick = RegisterTick(function ()
			if self.upTexture.mainTexture ~= nil then
				self.bgTexture.gameObject:SetActive(true)
				if paintTexture then
					paintTexture:Awake()
				end

				UnRegisterTick(self.tick)
			end
		end,400)
	end
end

function LuaPaintPicWndType3:SetStateEnd(stateIndex)
  self:Finish()
end

function LuaPaintPicWndType3:SetPercent(percent)
	if self.ProgressNodeTable[LuaShenbingMgr.DLNState] and percent[0] then
		self.ProgressNodeTable[LuaShenbingMgr.DLNState].transform:Find("bg"):GetComponent(typeof(UISprite)).fillAmount = percent[0]
	end
end

function LuaPaintPicWndType3:OnEnable()
	g_ScriptEvent:AddListener("UpdateDLNDraw", self, "SetStateEnd")
	--g_ScriptEvent:AddListener("UpdateDLNState", self, "SetPercent")
end

function LuaPaintPicWndType3:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateDLNDraw", self, "SetStateEnd")
	UnRegisterTick(self.tick)
	--g_ScriptEvent:RemoveListener("UpdateDLNState", self, "SetPercent")
end

function LuaPaintPicWndType3:Init()
	UnRegisterTick(self.tick)
	self.tick = nil
	local onCloseClick = function(go)
    MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("Paint_Not_Done"), DelegateFactory.Action(function ()
      self:Close()
    end), nil, nil, nil, false)
	end

	CommonDefs.AddOnClickListener(self.closeBtn,DelegateFactory.Action_GameObject(onCloseClick),false)

	self.taskID = tonumber(LuaPaintPicMgr.ShowArgs)
	self:SetStateStart(1)
end

return LuaPaintPicWndType3
