local CLinyuShopWnd = import "L10.UI.CLinyuShopWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local DelegateFactory = import "DelegateFactory"
local QnRadioBox = import "L10.UI.QnRadioBox"
local QnTableView = import "L10.UI.QnTableView"
local CLinyuShopPopupMenu = import "L10.UI.CLinyuShopPopupMenu"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"

LuaWishShopWnd = class()
RegistChildComponent(LuaWishShopWnd, "RadioBox", QnRadioBox)
RegistChildComponent(LuaWishShopWnd, "PopupMenu", CLinyuShopPopupMenu)
RegistChildComponent(LuaWishShopWnd, "Table", QnTableView)
RegistChildComponent(LuaWishShopWnd, "BuyCountButton", QnAddSubAndInputButton)
--RegistChildComponent(LuaWishShopWnd, "MoneyCtrl", CCurentMoneyCtrl)
--RegistChildComponent(LuaWishShopWnd, "BuyAreaRoot", GameObject)
--RegistChildComponent(LuaWishShopWnd, "BuyButton", QnButton)
--RegistChildComponent(LuaWishShopWnd, "SendArea", GameObject)
--RegistChildComponent(LuaWishShopWnd, "SendAreaBuyButton", QnButton)
--RegistChildComponent(LuaWishShopWnd, "SendAreaSendButton", QnButton)
--RegistChildComponent(LuaWishShopWnd, "TianQiButton", QnButton)
--RegistChildComponent(LuaWishShopWnd, "OnSellRoot", GameObject)
--RegistChildComponent(LuaWishShopWnd, "PreSellRoot", GameObject)


function LuaWishShopWnd:OnEnable()

end

function LuaWishShopWnd:OnDisable()

end

function LuaWishShopWnd:ChangeIndexToRealCategory(index)
  local changeTable = {4,2,3,5}

  return changeTable[index+1]
end

function LuaWishShopWnd:Init()
  self.RadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn, _index)
    local index = self:ChangeIndexToRealCategory(_index)
    if self.CurrentCategory ~= index then
      self.CurrentSubCategory = 0
    end
    self.CurrentCategory = index
    self.CurrentSelectedItem = - 1
    self.m_SubCategorys = CShopMallMgr.GetLingYuMallSubCategory(self.CurrentCategory - 1)
    --排除几个分类不显示子分类
    if self.CurrentCategory - 1 ~= CLinyuShopWnd.LINGYU_XINSHOU_CATEGORY and self.CurrentCategory - 1 ~= CLinyuShopWnd.LINGYU_GIFT_CATEGORY and self.CurrentCategory - 1 ~= - 1 and self.m_SubCategorys ~= nil and self.m_SubCategorys.Count > 1 then
      local popupIndex = CommonDefs.ListIndexOf(self.m_SubCategorys, self.CurrentSubCategory)
      if popupIndex < 0 or popupIndex > self.m_SubCategorys.Count then
        popupIndex = 0
      end

      if CShopMallMgr.SelectSubCategory ~= 0 then
        popupIndex = CommonDefs.ListIndexOf(self.m_SubCategorys, CShopMallMgr.SelectSubCategory)
      end
      self.PopupMenu:Init(button.transform, self.m_SubCategorys, popupIndex)
    else
      self.PopupMenu:ShowSelf(false)
    end
    self:UpdateView(false)
  end)
end

function LuaWishShopWnd:IsGiftCategory(category)
  local GiftCategory = CShopMallMgr.GetLingYuMallInfo(category)
  if not GiftCategory or GiftCategory.Count == 0 then
    return true
  else
    return false
  end
end

function LuaWishShopWnd:UpdateView(updatePosition)
  self.m_CurrentItems = CShopMallMgr.GetLingYuMallInfo(self.CurrentCategory - 1, self.CurrentSubCategory)
  --self.Table:ReloadData(true, updatePosition)
  if self.m_CurrentItems ~= nil and self.m_CurrentItems.Count > 0 then
    self.BuyCountButton:SetMinMax(1, 999, 1)
    if self.BuyCountButton:GetValue() > 999 or self.BuyCountButton:GetValue() < 1 then
      self.BuyCountButton:SetValue(1, true)
    end
    if self.CurrentSelectedItem >= 0 and self.CurrentSelectedItem < self.m_CurrentItems.Count then
      self.Table:SetSelectRow(self.CurrentSelectedItem, true)
    else
      self.Table:SetSelectRow(0, true)
    end
  else
    self.BuyCountButton:SetMinMax(0, 0, 1)
    self.BuyCountButton:SetValue(0, true)
    self.CurrentSelectedItem = - 1
  end

  for i=0,self.m_CurrentItems.Count,1 do

  end
end

function LuaWishShopWnd:UpdateTotalPrice()
end
