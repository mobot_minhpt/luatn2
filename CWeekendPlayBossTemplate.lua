-- Auto Generated!!
local CWeekendPlayBossTemplate = import "L10.UI.CWeekendPlayBossTemplate"
CWeekendPlayBossTemplate.m_Init_CS2LuaHook = function (this, icon, name, hasKilled, hasOpen) 
    this.iconTexture:LoadNPCPortrait(icon, false)
    this.bossName.text = name
    this.killTip:SetActive(hasKilled)
    this.killingTip:SetActive(not hasKilled and hasOpen)
    this.notOpenTip:SetActive(not hasOpen and not hasKilled)
    this.button.Enabled = hasOpen
end
