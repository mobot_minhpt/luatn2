local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local GameObject = import "UnityEngine.GameObject"
local UIProgressBar = import "UIProgressBar"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local QnButton = import "L10.UI.QnButton"
local CUIFx = import "L10.UI.CUIFx"
local Profession = import "L10.Game.Profession"
local EnumClass = import "L10.Game.EnumClass"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local AlignType = import "L10.UI.CTooltip+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType2 = import "L10.UI.CItemInfoMgr+AlignType"

LuaAntiProfessionLevelUpWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaAntiProfessionLevelUpWnd, "TitleLab", "TitleLab", UILabel)
RegistChildComponent(LuaAntiProfessionLevelUpWnd, "MaxLab", "MaxLab", UILabel)
RegistChildComponent(LuaAntiProfessionLevelUpWnd, "Icon", "Icon", CUITexture)
RegistChildComponent(LuaAntiProfessionLevelUpWnd, "LevelUp", "LevelUp", GameObject)
RegistChildComponent(LuaAntiProfessionLevelUpWnd, "Tupo", "Tupo", GameObject)
RegistChildComponent(LuaAntiProfessionLevelUpWnd, "MaxLevel", "MaxLevel", GameObject)
RegistChildComponent(LuaAntiProfessionLevelUpWnd, "CurLab", "CurLab", UILabel)
RegistChildComponent(LuaAntiProfessionLevelUpWnd, "NextLab", "NextLab", UILabel)
RegistChildComponent(LuaAntiProfessionLevelUpWnd, "NeedLab", "NeedLab", UILabel)
RegistChildComponent(LuaAntiProfessionLevelUpWnd, "RateLab", "RateLab", UILabel)
RegistChildComponent(LuaAntiProfessionLevelUpWnd, "ProgressBar", "ProgressBar", UIProgressBar)
RegistChildComponent(LuaAntiProfessionLevelUpWnd, "ProgressLab", "ProgressLab", UILabel)
RegistChildComponent(LuaAntiProfessionLevelUpWnd, "Cost1", "Cost1", GameObject)
RegistChildComponent(LuaAntiProfessionLevelUpWnd, "Cost2", "Cost2", GameObject)
RegistChildComponent(LuaAntiProfessionLevelUpWnd, "TupoEffectLab", "TupoEffectLab", UILabel)
RegistChildComponent(LuaAntiProfessionLevelUpWnd, "QnCostAndOwnMoney", "QnCostAndOwnMoney", CCurentMoneyCtrl)
RegistChildComponent(LuaAntiProfessionLevelUpWnd, "Cost", "Cost", UILabel)
RegistChildComponent(LuaAntiProfessionLevelUpWnd, "Own", "Own", UILabel)
RegistChildComponent(LuaAntiProfessionLevelUpWnd, "MaxEffectLab", "MaxEffectLab", UILabel)
RegistChildComponent(LuaAntiProfessionLevelUpWnd, "OpBtn", "OpBtn", QnButton)
RegistChildComponent(LuaAntiProfessionLevelUpWnd, "TipBtn", "TipBtn", QnButton)
RegistChildComponent(LuaAntiProfessionLevelUpWnd, "Fx", "Fx", CUIFx)
RegistChildComponent(LuaAntiProfessionLevelUpWnd, "WndTitleLab", "TitleLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaAntiProfessionLevelUpWnd, "m_Info")
RegistClassMember(LuaAntiProfessionLevelUpWnd, "m_ProName")
RegistClassMember(LuaAntiProfessionLevelUpWnd, "m_ExpCostAndOwn")
RegistClassMember(LuaAntiProfessionLevelUpWnd, "m_CostTip")

function LuaAntiProfessionLevelUpWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

    self.m_ExpCostAndOwn= self.transform:Find("Anchor/Middle/Tupo/Costs/ExpCost/QnCostAndOwnMoney"):GetComponent(typeof(CCurentMoneyCtrl))
    self.m_CostTip      = self.transform:Find("Anchor/Middle/Tupo/Costs/CostTip"):GetComponent(typeof(UILabel))
end

function LuaAntiProfessionLevelUpWnd:Init()
    self.TipBtn.OnClick = DelegateFactory.Action_QnButton(function (btn)
        g_MessageMgr:ShowMessage("ANTIPROFESSION_LEVELUPVIEW_TIP")
    end)

    self:InitInfo()
end

--@region UIEvent

--@endregion UIEvent

function LuaAntiProfessionLevelUpWnd:OnEnable()
    g_ScriptEvent:AddListener("AntiProfessionUpgradeSkillResult", self, "OnAntiProfessionUpgradeSkillResult")
    g_ScriptEvent:AddListener("AntiProfessionBreakLimitResult", self, "OnAntiProfessionBreakLimitResult")
    g_ScriptEvent:AddListener("AntiProfessionUpdateScore", self, "OnAntiProfessionUpdateScore")
    g_ScriptEvent:AddListener("MainPlayerPlayPropUpdate", self, "InitTupoOwn")
    g_ScriptEvent:AddListener("MainPlayerUpdateMoney", self, "InitTupoOwn")
end

function LuaAntiProfessionLevelUpWnd:OnDisable()
    g_ScriptEvent:RemoveListener("AntiProfessionUpgradeSkillResult", self, "OnAntiProfessionUpgradeSkillResult")
    g_ScriptEvent:RemoveListener("AntiProfessionBreakLimitResult", self, "OnAntiProfessionBreakLimitResult")
    g_ScriptEvent:RemoveListener("AntiProfessionUpdateScore", self, "OnAntiProfessionUpdateScore")
    g_ScriptEvent:RemoveListener("MainPlayerPlayPropUpdate", self, "InitTupoOwn")
    g_ScriptEvent:RemoveListener("MainPlayerUpdateMoney", self, "InitTupoOwn")
end

function LuaAntiProfessionLevelUpWnd:OnAntiProfessionUpgradeSkillResult(bSuccess, Index, Level, FailTimes)
    if Index == LuaZongMenMgr.m_SelectedAntiProfessionIndex then
        self:InitInfo()
        if bSuccess then
            self.Fx:DestroyFx()
            self.Fx:LoadFx("fx/ui/prefab/UI_zhiyekefu_tupo.prefab")
        else
            CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.AntiProfessionLevelUpSecurity)
        end
    end
end
function LuaAntiProfessionLevelUpWnd:OnAntiProfessionBreakLimitResult(bSuccess, Index, MaxLevel)
    if Index == LuaZongMenMgr.m_SelectedAntiProfessionIndex then
        self:InitInfo()
        if bSuccess then
            self.Fx:DestroyFx()
            self.Fx:LoadFx("fx/ui/prefab/UI_zhiyekefu_tupochengong.prefab")
        end
    end
end

function LuaAntiProfessionLevelUpWnd:OnAntiProfessionUpdateScore(profession, score)
    if profession == self.m_Info.Profession then
        self:InitInfo()
    end
end

function LuaAntiProfessionLevelUpWnd:InitInfo()
    if CClientMainPlayer.Inst == nil then
        CUIManager.CloseUI(CLuaUIResources.AntiProfessionLevelUpWnd)
    end

    self.m_Info = CClientMainPlayer.Inst.SkillProp.AntiProfessionSkill[LuaZongMenMgr.m_SelectedAntiProfessionIndex]
    local proClass = CommonDefs.ConvertIntToEnum(typeof(EnumClass), self.m_Info.Profession)
    self.m_ProName = Profession.GetFullName(proClass)
    local skillName = SoulCore_Settings.GetData().AntiProfessionName[self.m_Info.Profession - 1]
    self.TitleLab.text = SafeStringFormat3(LocalString.GetString("%s [c][ffffff]lv.%d[-][/c]"), skillName, self.m_Info.CurLevel) 
    self.MaxLab.text = SafeStringFormat3(LocalString.GetString("等级上限 [c][ffffff]lv.%d[-][/c]"), self.m_Info.MaxLevel)
    self.Icon:LoadMaterial(Profession.GetLargeIcon(proClass))

    if self.m_Info.CurLevel >= SoulCore_AntiProfessionLevel.GetDataCount() then
        --满级
        self:InitMaxLevel()
    elseif self.m_Info.CurLevel >= self.m_Info.MaxLevel then
        --需要突破
        self:InitTupo()
    else
        --可以升级
        self:InitLevelUp(self.m_ProName)
    end
end

function LuaAntiProfessionLevelUpWnd:InitTupo()
    self.LevelUp:SetActive(false)
    self.Tupo:SetActive(true)
    self.MaxLevel:SetActive(false)

    self.OpBtn.Enabled = true
    self.OpBtn.Text = LocalString.GetString("突  破")
    self.WndTitleLab.text = LocalString.GetString("克符技能突破")

    local curMaxLv = self.m_Info.MaxLevel
    local nextMaxLv = curMaxLv
    for i = curMaxLv + 1, SoulCore_AntiProfessionLevel.GetDataCount() do
        local data = SoulCore_AntiProfessionLevel.GetData(i)
        if data.IsBreakLimitLevel == 1 then
            nextMaxLv = i
            self:InitTupoOwn()
            self.QnCostAndOwnMoney:SetCost(data.BreakLimitMoney)
            self.m_ExpCostAndOwn:SetCost(data.BreakLimitExp)
            self.m_CostTip.text = self:GetExpAndMoneyTip(data)
            break
        end
    end
    self.TupoEffectLab.text = SafeStringFormat3(LocalString.GetString("突破后等级上限从[c][ffff00]%d级[-][/c]提高至[c][ffff00]%d级[-][/c]\n成功率[c][00ff60]100%%[-][/c]"), curMaxLv, nextMaxLv)

    self.OpBtn.OnClick = DelegateFactory.Action_QnButton(function (btn)
        self:TupoCostCheck()
    end)
end

function LuaAntiProfessionLevelUpWnd:InitTupoOwn()
    local freeSilver = CClientMainPlayer.Inst.FreeSilver
    --优先消耗银票
    if CClientMainPlayer.Inst.FreeSilver > 0 then
        self.QnCostAndOwnMoney:SetType(EnumMoneyType_lua.YinPiao, 0, false)
    else
        self.QnCostAndOwnMoney:SetType(EnumMoneyType_lua.YinLiang, 0, false)
    end

    --经验
    local skillPrivateExp = CClientMainPlayer.Inst.SkillProp.SkillPrivateExp
    local exp = CClientMainPlayer.Inst.PlayProp.Exp
    --优先消耗专有经验 得手动更新下当前经验
    if CClientMainPlayer.Inst.SkillProp.SkillPrivateExp > 0 then
        self.m_ExpCostAndOwn:SetType(EnumMoneyType_lua.PrivateExp, 0, false)
        self.m_ExpCostAndOwn.m_OwnLabel.text = tostring(skillPrivateExp)
    else
        self.m_ExpCostAndOwn:SetType(EnumMoneyType_lua.Exp, 0, false)
        self.m_ExpCostAndOwn.m_OwnLabel.text = tostring(exp)
    end
end

function LuaAntiProfessionLevelUpWnd:InitLevelUp()
    self.LevelUp:SetActive(true)
    self.Tupo:SetActive(false)
    self.MaxLevel:SetActive(false)
    
    self.OpBtn.Enabled = true
    self.OpBtn.Text = LocalString.GetString("升  级")
    self.WndTitleLab.text = LocalString.GetString("克符技能升级")

    -- Effect
    local curLevelData = SoulCore_AntiProfessionLevel.GetData(self.m_Info.CurLevel)
    local nextLevelData = SoulCore_AntiProfessionLevel.GetData(self.m_Info.CurLevel + 1)

    local nextMul = nextLevelData.AntiProfessionMul
    local curMul = 0
    if curLevelData ~= nil then
        curMul = curLevelData.AntiProfessionMul
    end
    self.CurLab.text = SafeStringFormat3(LocalString.GetString("对%s伤害[c][ffff00]+%.1f%%[-][/c]"), self.m_ProName, curMul * 100)
    self.NextLab.text = SafeStringFormat3(LocalString.GetString("对%s伤害[c][ffff00]+%.1f%%[-][/c]"), self.m_ProName, nextMul * 100)

    -- Info
    local nextSkillId = LuaZongMenMgr.m_SelectedAntiProfessionIndex * 100 + self.m_Info.CurLevel + 1
    local nextSkillData = SoulCore_AntiProfessionSkills.GetData(nextSkillId)
    local playerLv = CClientMainPlayer.Inst.MaxLevel
    local playerLearnLv = nextSkillData.PlayerLearnLv
    local playerSoulCoreLv = CClientMainPlayer.Inst.SkillProp.SoulCore.Level
    local playerLearnSoulCoreLv = nextSkillData.SoulCoreLevel
    local needScore = nextSkillData.InScore
    local upgradeSuccessRate = nextSkillData.UpgradeSuccessRate
	local downGradeRate = nextSkillData.DownGradeRate
	local securityTimes = nextSkillData.SecurityTimes
    --local failWeight = nextSkillData.FailWeight  
    local failTimes = self.m_Info.UpgradeFailTimes

    if playerLv < playerLearnLv then
        self.NeedLab.text = SafeStringFormat3("[c][E34D55]%s;[-][/c]", SafeStringFormat3(LocalString.GetString("角色等级达到%d级"), playerLearnLv))
    else
        self.NeedLab.text = SafeStringFormat3("[c][FFFFFF]%s;[-][/c]", SafeStringFormat3(LocalString.GetString("角色等级达到%d级"), playerLearnLv))
    end

    if playerSoulCoreLv < playerLearnSoulCoreLv then
        self.NeedLab.text = self.NeedLab.text .. SafeStringFormat3(" [c][E34D55]%s[-][/c]", SafeStringFormat3(LocalString.GetString("灵核等级达到%d级"), playerLearnSoulCoreLv))
    else
        self.NeedLab.text = self.NeedLab.text .. SafeStringFormat3(" [c][FFFFFF]%s[-][/c]", SafeStringFormat3(LocalString.GetString("灵核等级达到%d级"), playerLearnSoulCoreLv))
    end

    local progress = failTimes / securityTimes
    if progress < 1 then
        local curLevel = self.m_Info.CurLevel
        local securityLevel = 1
        for i = curLevel, 1, -1 do
            local data = SoulCore_AntiProfessionLevel.GetData(i)
            if data.IsSecurityLevel == 1 then
                securityLevel = i
                break
            end
        end
        if upgradeSuccessRate < 1 then
            self.RateLab.text = SafeStringFormat3(LocalString.GetString("[ffff00]%.0f%%（失败后从%d级降至%d级）[-][/c]"), upgradeSuccessRate * 100, curLevel, securityLevel)
        else 
            self.RateLab.text = SafeStringFormat3(LocalString.GetString("[00ff60]%.0f%%[-][/c]"), upgradeSuccessRate * 100)
        end
    else 
        self.RateLab.text = LocalString.GetString("[c][00ff60]100%[-][/c][c][ffff00]（突破成功清空保底进度）[-][/c]")
    end

    self.ProgressBar.value = progress
    self.ProgressLab.text = SafeStringFormat3("%d/%d", failTimes, securityTimes)

    self:InitLevelUpCost(needScore)
    self.OpBtn.OnClick = DelegateFactory.Action_QnButton(function (btn)
        Gac2Gas.AntiProfession_UpgradeSkill(LuaZongMenMgr.m_SelectedAntiProfessionIndex)
    end)
end

function LuaAntiProfessionLevelUpWnd:InitLevelUpCost(needScore)
    self.Cost1:SetActive(true)
    self.Cost2:SetActive(false)

    self:InitCost(self.Cost1, self.m_Info.Profession, needScore)
end

function LuaAntiProfessionLevelUpWnd:InitCost(item, profession, needScore)
    local btn       = item:GetComponent(typeof(QnButton))
    local icon      = item.transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local getLab    = item.transform:Find("GetLab"):GetComponent(typeof(UILabel))
    local numLab    = item.transform:Find("NumLab"):GetComponent(typeof(UILabel))

    icon:LoadMaterial(LuaZongMenMgr:GetProfessionScoreIconPath(profession))
    btn.OnClick = DelegateFactory.Action_QnButton(function(btn)
        local itemId = SoulCore_Settings.GetData().AntiProfessionItemId[profession - 1][1]
        if getLab.gameObject.activeSelf then
            CItemAccessListMgr.Inst:ShowItemAccessInfo(itemId, true, btn.transform, AlignType.Right)
        else
            CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType2.Default, 0, 0, 0, 0)
        end
    end)

    local ownScore = CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.SkillProp.AntiProfessionScore, profession) and CClientMainPlayer.Inst.SkillProp.AntiProfessionScore[profession] or 0

    if ownScore >= needScore then
        getLab.gameObject:SetActive(false)
        numLab.text = SafeStringFormat3("%d/%d", ownScore, needScore)
    else
        getLab.gameObject:SetActive(true)
        numLab.text = SafeStringFormat3("[c][ff0000]%d[-][/c]/%d", ownScore, needScore)
    end
end

function LuaAntiProfessionLevelUpWnd:InitMaxLevel()
    self.LevelUp:SetActive(false)
    self.Tupo:SetActive(false)
    self.MaxLevel:SetActive(true)

    self.OpBtn.Enabled = false
    self.OpBtn.Text = LocalString.GetString("已满级")
    local data =  SoulCore_AntiProfessionLevel.GetData(self.m_Info.CurLevel)
    self.MaxEffectLab.text = SafeStringFormat3(LocalString.GetString("对%s伤害[c][ffff00]+%.1f%%[-][/c]"), self.m_ProName, data.AntiProfessionMul * 100)

    self.OpBtn.OnClick = nil
end

function LuaAntiProfessionLevelUpWnd:GetGuideGo(methodName)
    if methodName=="SecuritySlider" then
        return self.ProgressBar.gameObject
    end
end

function LuaAntiProfessionLevelUpWnd:GetExpAndMoneyTip(nextSkill)
    local skillPrivateExp = CClientMainPlayer.Inst.SkillProp.SkillPrivateExp
    local exp = CClientMainPlayer.Inst.PlayProp.Exp

    local freeSilver = CClientMainPlayer.Inst.FreeSilver
    local silver = CClientMainPlayer.Inst.Silver

    local expEnough = (skillPrivateExp + exp) >= nextSkill.BreakLimitExp
    local moneyEnough = (freeSilver + silver >= 0) and ((freeSilver + silver) >= nextSkill.BreakLimitMoney)

    if expEnough and moneyEnough then
        local icons1 = ""
        local icons2 = ""
        --专有经验，前世经验、个人经验
        local privateExpNotEnough = (skillPrivateExp > 0 and skillPrivateExp < nextSkill.BreakLimitExp)
        
        if skillPrivateExp ~= 0 then
            --显示专有经验图标
            if skillPrivateExp < nextSkill.BreakLimitExp then
                icons1 = icons1 .. "#291"
                icons2 = icons2 .. "#290"
                --个人经验图标
            end
        end

        if (freeSilver > 0 and freeSilver < nextSkill.BreakLimitMoney) then
            icons1 = icons1 .. "#288"
            icons2 = icons2 .. "#287"
        end
        --银两图标

        if not System.String.IsNullOrEmpty(icons2) then
            return System.String.Format(LocalString.GetString("{0}不足的部分将从{1}中补足"), icons1, icons2)
        end
    else
        return LocalString.GetString("[c][ff5050]#291#288不足的部分无法从#290或#287中补足[-][/c]")
    end

    return ""
end

function LuaAntiProfessionLevelUpWnd:TupoCostCheck()
    local freeSilver = CClientMainPlayer.Inst.FreeSilver
    local silver = CClientMainPlayer.Inst.Silver
    local cost = self.QnCostAndOwnMoney.cost
    local freeEnough = freeSilver >= cost
    local totalEnough = silver + freeSilver >= cost
    if totalEnough and not freeEnough and freeSilver ~= 0 then
        local silverCost = cost - freeSilver
        MessageWndManager.ShowOKCancelMessage(SafeStringFormat3(LocalString.GetString("突破所需银票不足，是否消耗%d银两进行补足？"), silverCost), DelegateFactory.Action(function ()
            Gac2Gas.AntiProfession_BreakLimit(LuaZongMenMgr.m_SelectedAntiProfessionIndex)
        end), nil, nil, nil, false)
    else 
        Gac2Gas.AntiProfession_BreakLimit(LuaZongMenMgr.m_SelectedAntiProfessionIndex)
    end
end
