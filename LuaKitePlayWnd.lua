require("3rdParty/ScriptEvent")
require("common/common_include")
local CMainCamera = import "L10.Engine.CMainCamera"
local Gac2Gas = import "L10.Game.Gac2Gas"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CUIManager = import "L10.UI.CUIManager"
local CUIRes = import "L10.UI.CUIResources"
local RenderTexture = import "UnityEngine.RenderTexture"
local Screen = import "UnityEngine.Screen"
local RenderTextureFormat = import "UnityEngine.RenderTextureFormat"
local RenderTextureReadWrite = import "UnityEngine.RenderTextureReadWrite"
local TextureWrapMode = import "UnityEngine.TextureWrapMode"
local FilterMode = import "UnityEngine.FilterMode"
local Camera = import "UnityEngine.Camera"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local GameObject = import "UnityEngine.GameObject"
local CameraClearFlags = import "UnityEngine.CameraClearFlags"
local LayerDefine = import "L10.Engine.LayerDefine"
local Quaternion = import "UnityEngine.Quaternion"
local Vector3 = import "UnityEngine.Vector3"
local CPreDrawMgr = import "L10.Engine.CPreDrawMgr"
local KitePlayMgr = import "L10.Game.KitePlayMgr"
local Time = import "UnityEngine.Time"
local VirtualJoyStick = import "L10.UI.VirtualJoyStick"
local TweenPosition = import "TweenPosition"
local TweenRotation = import "TweenRotation"

CLuaKitePlayWnd=class()
RegistClassMember(CLuaKitePlayWnd,"CloseBtn")
RegistClassMember(CLuaKitePlayWnd,"ShowTexture")
RegistClassMember(CLuaKitePlayWnd,"TargetCameraNode")
RegistClassMember(CLuaKitePlayWnd,"LeftVSNode")
RegistClassMember(CLuaKitePlayWnd,"RightVSNode")
RegistClassMember(CLuaKitePlayWnd,"ScoreLabel")
RegistClassMember(CLuaKitePlayWnd,"TimeLabel")
RegistClassMember(CLuaKitePlayWnd,"SettingBtn")
RegistClassMember(CLuaKitePlayWnd,"SettingPanel")

local cameraNode = nil
local selfPlayerId = 0

local KiteItemTable = {}

function CLuaKitePlayWnd:Awake()
end

function CLuaKitePlayWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateKitePos", self, "UpdatePos")
	g_ScriptEvent:AddListener("AddNewKite", self, "InitPos")
	g_ScriptEvent:AddListener("DeleteKite", self, "DeletePlayerKite")
	g_ScriptEvent:AddListener("UpdateRealTimeScore", self, "UpdateShowInfo")
end

function CLuaKitePlayWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateKitePos", self, "UpdatePos")
	g_ScriptEvent:RemoveListener("AddNewKite", self, "InitPos")
	g_ScriptEvent:RemoveListener("DeleteKite", self, "DeletePlayerKite")
	g_ScriptEvent:RemoveListener("UpdateRealTimeScore", self, "UpdateShowInfo")
end

function CLuaKitePlayWnd:CheckKitePosAndMoveCamera(deltaTime)
	if not CClientMainPlayer.Inst then
		return
	end
	local playerId = CClientMainPlayer.Inst.Id
	if KiteItemTable[playerId] and cameraNode then
		local cameraScript = cameraNode:GetComponent(typeof(Camera))
		local kiteWorldPos = cameraScript:WorldToScreenPoint(KiteItemTable[playerId].transform.position)
		local posRotate = {cameraNode.transform.rotation.eulerAngles.x,cameraNode.transform.rotation.eulerAngles.y,cameraNode.transform.rotation.eulerAngles.z}
		if kiteWorldPos.x < 1920 * LuaKitePlayMgr.KiteCameraMoveTriggerRatio / LuaKitePlayMgr.KiteCameraMoveTotalRatio then
			--cameraNode.transform.rotation = Quaternion.Euler(cameraNode.transform.rotation.eulerAngles.x,cameraNode.transform.rotation.eulerAngles.y - deltaTime * LuaKitePlayMgr.KiteCameraMoveSpeed,cameraNode.transform.rotation.eulerAngles.z)
			posRotate[2] = posRotate[2] - deltaTime * LuaKitePlayMgr.KiteCameraMoveSpeed
		elseif kiteWorldPos.x > 1920 * (LuaKitePlayMgr.KiteCameraMoveTotalRatio - LuaKitePlayMgr.KiteCameraMoveTriggerRatio) / LuaKitePlayMgr.KiteCameraMoveTotalRatio then
			--cameraNode.transform.rotation = Quaternion.Euler(cameraNode.transform.rotation.eulerAngles.x,cameraNode.transform.rotation.eulerAngles.y + deltaTime * LuaKitePlayMgr.KiteCameraMoveSpeed,cameraNode.transform.rotation.eulerAngles.z)
			posRotate[2] = posRotate[2] + deltaTime * LuaKitePlayMgr.KiteCameraMoveSpeed
		end

		if kiteWorldPos.y < 1080 * LuaKitePlayMgr.KiteCameraMoveTriggerRatio / LuaKitePlayMgr.KiteCameraMoveTotalRatio then
			--cameraNode.transform.rotation = Quaternion.Euler(cameraNode.transform.rotation.eulerAngles.x + deltaTime * LuaKitePlayMgr.KiteCameraMoveSpeed,cameraNode.transform.rotation.eulerAngles.y,cameraNode.transform.rotation.eulerAngles.z)
			posRotate[1] = posRotate[1] + deltaTime * LuaKitePlayMgr.KiteCameraMoveSpeed
		elseif kiteWorldPos.y > 1080 * (LuaKitePlayMgr.KiteCameraMoveTotalRatio - LuaKitePlayMgr.KiteCameraMoveTriggerRatio) / LuaKitePlayMgr.KiteCameraMoveTotalRatio then
			--cameraNode.transform.rotation = Quaternion.Euler(cameraNode.transform.rotation.eulerAngles.x - deltaTime * LuaKitePlayMgr.KiteCameraMoveSpeed,cameraNode.transform.rotation.eulerAngles.y,cameraNode.transform.rotation.eulerAngles.z)
			posRotate[1] = posRotate[1] - deltaTime * LuaKitePlayMgr.KiteCameraMoveSpeed
		end

		TweenRotation.Begin(cameraNode,deltaTime,
			Quaternion.Euler(posRotate[1],posRotate[2],posRotate[3])
		)
	end
end

function CLuaKitePlayWnd:Update()
	if LuaKitePlayMgr.RealTimeTime > 0 then
		if not self.TimeLabel.gameObject.activeSelf then
			self.TimeLabel.gameObject:SetActive(true)
		end
		local min = math.floor(LuaKitePlayMgr.RealTimeTime / 60)
		local sec = math.ceil(LuaKitePlayMgr.RealTimeTime - min * 60)
		if sec < 10 then
			sec = '0' .. sec
		end
		self.TimeLabel.text = min .. ':' .. sec
		LuaKitePlayMgr.RealTimeTime = LuaKitePlayMgr.RealTimeTime - Time.deltaTime
	else
		if self.TimeLabel.gameObject.activeSelf then
			self.TimeLabel.gameObject:SetActive(false)
		end
	end

	if LuaKitePlayMgr.EnableGyroControl then
		if Input.gyro then
			if Time.time - LuaKitePlayMgr.lastSendTime > LuaKitePlayMgr.perSendTime then
				LuaKitePlayMgr.lastSendTime = Time.time
				Gac2Gas.SendZhiYuanInstruction(Input.gyro.gravity.x,Input.gyro.gravity.y,Input.gyro.gravity.z)
			end
		end
	end

	if cameraNode then
		self:CheckKitePosAndMoveCamera(Time.deltaTime)
	end
end

function CLuaKitePlayWnd:UpdateShowInfo()
	self.ScoreLabel.text = LuaKitePlayMgr.RealTimeScore
end

function CLuaKitePlayWnd:DeletePlayerKite(playerId)
	if KiteItemTable[playerId] then
		GameObject.Destroy(KiteItemTable[playerId])
		KiteItemTable[playerId] = nil
	end
end

function CLuaKitePlayWnd:InitPos(posData)
	if not posData or KiteItemTable[posData.playerId] then
		return
	end
	local onLoadFinished = function(kiteNode)
		KiteItemTable[posData.playerId] = kiteNode
		kiteNode.transform.position = Vector3(posData.kitePos[1],posData.kitePos[2],posData.kitePos[3])
		self:UpdatePos(posData)
	end
	KitePlayMgr.Inst:InitKite(posData.kiteType,DelegateFactory.Action_GameObject(onLoadFinished))
end

function CLuaKitePlayWnd:CalMoveDir(dir)
	if not CClientMainPlayer.Inst then
		return
	end
	local selfData = LuaKitePlayMgr.m_DataTable[CClientMainPlayer.Inst.Id]
	local x = selfData.kitePos[1] - selfData.playerPos[1]
	local y = selfData.kitePos[2] - selfData.playerPos[2]
	local z = selfData.kitePos[3] - selfData.playerPos[3]
	local _L = math.sqrt(x*x+z*z)
	local _R = math.sqrt(y*y+x*x+z*z)
	local newX = 0
	local newY = 0
	local newZ = 0
	if dir.y ~= 0 then
		newY = newY + dir.y
	end
	if _L > 0 then
		--newX = x * dir.z / _L + z * dir.x / _L
		--newZ = -z * dir.z / _L - x * dir.x / _L
		newX = dir.z
		newZ = -dir.x
	end
	--newX = 0

	return newX * LuaKitePlayMgr.leftMoveRatio,newY * LuaKitePlayMgr.rightMoveRatio,newZ * LuaKitePlayMgr.leftMoveRatio
end

local saveKitePos = nil

function CLuaKitePlayWnd:UpdatePos(posData)
	if not posData or not KiteItemTable[posData.playerId] then
		return
	end
	local kiteNode = KiteItemTable[posData.playerId]

	--kiteNode.transform.position = Vector3(posData.kitePos[1],posData.kitePos[2],posData.kitePos[3])
	if saveKitePos then
		kiteNode.transform.position = Vector3(saveKitePos[1],saveKitePos[2],saveKitePos[3])
	end

	TweenPosition.Begin(kiteNode,0.14,Vector3(posData.kitePos[1],posData.kitePos[2],posData.kitePos[3]))
	saveKitePos = posData.kitePos
	--TweenRotation.Begin(kiteNode,0.105,KitePlayMgr.QuaDot(
	--Quaternion.LookRotation(Vector3(posData.kitePos[1] - posData.playerPos[1],posData.kitePos[2] - posData.playerPos[2],posData.kitePos[3] - posData.playerPos[3])),
	--Quaternion.LookRotation(Vector3(posData.kitePos[1] - kiteNode.transform.position.x,posData.kitePos[2] - kiteNode.transform.position.y,posData.kitePos[3] - kiteNode.transform.position.z))
	--))

	--kiteNode.transform.rotation =

	local ratio = 15
	local extraRation = 1.4
	TweenRotation.Begin(kiteNode,0.14,
		Quaternion.LookRotation(Vector3(
		posData.kitePos[1] - posData.playerPos[1] - (posData.kitePos[1] - kiteNode.transform.position.x) * ratio * extraRation,
		posData.kitePos[2] - posData.playerPos[2] - (posData.kitePos[2] - kiteNode.transform.position.y) * ratio,
		posData.kitePos[3] - posData.playerPos[3] - (posData.kitePos[3] - kiteNode.transform.position.z) * ratio * extraRation))
	)
end

function CLuaKitePlayWnd:Init()
	local onCloseClick = function(go)
		CUIManager.CloseUI(CUIRes.KitePlayWnd)
	end
	CommonDefs.AddOnClickListener(self.CloseBtn,DelegateFactory.Action_GameObject(onCloseClick),false)

	self.ScoreLabel.text = '0'
	self.TimeLabel.gameObject:SetActive(false)

	CPreDrawMgr.m_bEnableRectPreDraw = false

	if 1920 / Screen.width > 1080 / Screen.height then
		self.ShowTexture.width = 1920--Screen.width
		self.ShowTexture.height = Screen.height * 1920 / Screen.width--Screen.height
	else
		self.ShowTexture.width = Screen.width * 1080 / Screen.height--Screen.width
		self.ShowTexture.height = 1080--Screen.height
	end

	local renderTex = RenderTexture(self.ShowTexture.width,self.ShowTexture.height,32,RenderTextureFormat.ARGB32,RenderTextureReadWrite.Default)
	renderTex.wrapMode = TextureWrapMode.Clamp
	renderTex.filterMode = FilterMode.Bilinear
	renderTex.anisoLevel = 2
	self.ShowTexture.mainTexture = renderTex

	--self.TargetCameraNode.transform.position = CClientMainPlayer.Inst.WorldPos
	self.TargetCameraNode:SetActive(false)

	cameraNode = GameObject("__KitePlayCamera__")
	local cameraScript = CommonDefs.AddCameraComponent(cameraNode)
    CMainCamera.Inst:SetCameraEnableStatus(false,"use_another_camera", false)
	cameraScript.clearFlags = CameraClearFlags.Skybox
  --cameraScript.backgroundColor = Color(49 / 255, 77 / 255, 121 / 255, 5 / 255)
  cameraScript.cullingMask = 2^LayerDefine.ModelForNGUI_3D + 2^LayerDefine.Effect_3D + 2^LayerDefine.Default + 2^LayerDefine.Water + 2^LayerDefine.WaterReflection + 2^LayerDefine.Terrain
  cameraScript.orthographic = false
  cameraScript.fieldOfView = 60
  cameraScript.farClipPlane = 40
  cameraScript.nearClipPlane = 0.1
  cameraScript.depth = -10
  cameraScript.transform.parent = CUIManager.instance.transform
	local selfData = LuaKitePlayMgr.m_DataTable[CClientMainPlayer.Inst.Id]
	cameraNode.transform.position = Vector3(selfData.playerPos[1],selfData.playerPos[2],selfData.playerPos[3])

  cameraScript.transform.rotation = Quaternion.LookRotation(Vector3(selfData.kitePos[1] - selfData.playerPos[1],selfData.kitePos[2] - selfData.playerPos[2],selfData.kitePos[3] - selfData.playerPos[3]))
  --cameraScript.transform.localScale = Vector3.one
	cameraScript.targetTexture = renderTex

	--lineNode = GameObject("__KitePlayLine__")
	--local lineRenderScript = lineNode:AddComponent(typeof(LineRenderer))
	--lineRenderScript.material:SetColor("_Color", Color.white)
	--lineRenderScript:SetWidth(1,1)
	--lineRenderScript:SetPosition(0,cameraNode.transform.position)
	--lineRenderScript:SetPosition(1,Vector3(selfData.kitePos[1],selfData.kitePos[2],selfData.kitePos[3]))

	selfPlayerId = CClientMainPlayer.Inst.Id

	for i,v in pairs(LuaKitePlayMgr.m_DataTable) do
		self:InitPos(v)
	end
	local onLeftVSNodeMove = function(dir)
		LuaKitePlayMgr.leftMove.x = dir.x
		LuaKitePlayMgr.leftMove.y = 0
		LuaKitePlayMgr.leftMove.z = dir.y
		if Time.time - LuaKitePlayMgr.lastSendTime > LuaKitePlayMgr.perSendTime then
				LuaKitePlayMgr.lastSendTime = Time.time
				local newX,newY,newZ = self:CalMoveDir({x = LuaKitePlayMgr.leftMove.x,y = LuaKitePlayMgr.rightMove.y,z = LuaKitePlayMgr.leftMove.z})
				Gac2Gas.SendZhiYuanInstruction(newX,newY,newZ)
		end
	end
	local onLeftVSNodeEnd = function()
		LuaKitePlayMgr.leftMove = {x=0,y=0,z=0}

	end
	local onRightVSNodeMove = function(dir)
		LuaKitePlayMgr.rightMove.x = 0
		LuaKitePlayMgr.rightMove.y = dir.y
		LuaKitePlayMgr.rightMove.z = 0
		if Time.time - LuaKitePlayMgr.lastSendTime > LuaKitePlayMgr.perSendTime then
				LuaKitePlayMgr.lastSendTime = Time.time
				local newX,newY,newZ = self:CalMoveDir({x = LuaKitePlayMgr.leftMove.x,y = LuaKitePlayMgr.rightMove.y,z = LuaKitePlayMgr.leftMove.z})
				Gac2Gas.SendZhiYuanInstruction(newX,newY,newZ)
		end
	end
	local onRightVSNodeEnd = function()
		LuaKitePlayMgr.rightMove = {x=0,y=0,z=0}

	end
	CommonDefs.AddVSOnDraggingListener(self.LeftVSNode,DelegateFactory.Action_Vector3(onLeftVSNodeMove),false)
	CommonDefs.AddVSOnDragFinishListener(self.LeftVSNode,DelegateFactory.Action(onLeftVSNodeEnd),false)
	CommonDefs.AddVSOnDraggingListener(self.RightVSNode,DelegateFactory.Action_Vector3(onRightVSNodeMove),false)
	CommonDefs.AddVSOnDragFinishListener(self.RightVSNode,DelegateFactory.Action(onRightVSNodeEnd),false)
	local vsscript = self.RightVSNode:GetComponent(typeof(VirtualJoyStick))
	vsscript.EnableMoveWithFinger = false
	vsscript.LockVerMove = true

	self:InitSettingPanel()
end

local nowChooseSettingNode = nil

function CLuaKitePlayWnd:OpenSettingPanel()
	self.SettingPanel:SetActive(true)
	local node1 = self.SettingPanel.transform:Find("node/Option1/click").gameObject
	local node2 = self.SettingPanel.transform:Find("node/Option2/click").gameObject
	local bgBtn = self.SettingPanel.transform:Find("darkbg").gameObject
	local onCloseSettingPanelClick = function(go)
		self.SettingPanel:SetActive(false)
	end
	CommonDefs.AddOnClickListener(bgBtn,DelegateFactory.Action_GameObject(onCloseSettingPanelClick),false)

	local onSetMoveControl = function(go)
		if nowChooseSettingNode then
			nowChooseSettingNode:SetActive(false)
		end
		nowChooseSettingNode = go.transform:Find("node").gameObject
		nowChooseSettingNode:SetActive(true)
		self.LeftVSNode:SetActive(true)
		self.RightVSNode:SetActive(true)
		LuaKitePlayMgr.EnableGyroControl = false
	end
	CommonDefs.AddOnClickListener(node1,DelegateFactory.Action_GameObject(onSetMoveControl),false)

	local onSetGryoControl = function(go)
		if nowChooseSettingNode then
			nowChooseSettingNode:SetActive(false)
		end
		nowChooseSettingNode = go.transform:Find("node").gameObject
		nowChooseSettingNode:SetActive(true)
		self.LeftVSNode:SetActive(false)
		self.RightVSNode:SetActive(false)
		LuaKitePlayMgr.EnableGyroControl = true
	end
	CommonDefs.AddOnClickListener(node2,DelegateFactory.Action_GameObject(onSetGryoControl),false)
end

function CLuaKitePlayWnd:InitSettingPanel()
	if Application.platform == RuntimePlatform.IPhonePlayer or Application.platform == RuntimePlatform.Android or Application.platform == RuntimePlatform.WindowsEditor then
		self.LeftVSNode:SetActive(true)
		self.RightVSNode:SetActive(true)
		LuaKitePlayMgr.EnableGyroControl = false
		self:OpenSettingPanel()
		local onOpenSettingPanelClick = function(go)
			self:OpenSettingPanel()
		end
		CommonDefs.AddOnClickListener(self.SettingBtn,DelegateFactory.Action_GameObject(onOpenSettingPanelClick),false)
	else
		LuaKitePlayMgr.EnableGyroControl = false
		self.LeftVSNode:SetActive(true)
		self.RightVSNode:SetActive(true)
	end
end

function CLuaKitePlayWnd:OnDestroy()
    CMainCamera.Inst:SetCameraEnableStatus(true,"use_another_camera", false)
	Gac2Gas.RequestQuitZhiYuanPlay()
	CPreDrawMgr.m_bEnableRectPreDraw = true
	if cameraNode then
		GameObject.Destroy(cameraNode)
	end
	for i,v in pairs(KiteItemTable) do
		if v then
			GameObject.Destroy(v)
		end
	end
	KiteItemTable = {}
end

return CLuaKitePlayWnd
