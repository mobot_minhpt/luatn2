local CChatLinkMgr=import "CChatLinkMgr"

local QnTableView=import "L10.UI.QnTableView"
local UIScrollView = import "UIScrollView"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local CUITexture = import "L10.UI.CUITexture"
local CButton = import "L10.UI.CButton"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local MengHuaLu_Item = import "L10.Game.MengHuaLu_Item"
local MessageMgr = import "L10.Game.MessageMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local MengHuaLu_QiChang = import "L10.Game.MengHuaLu_QiChang"
local Baby_QiChang = import "L10.Game.Baby_QiChang"
local EnumQualityType = import "L10.Game.EnumQualityType"

LuaMengHuaLuShopWnd = class()

RegistChildComponent(LuaMengHuaLuShopWnd, "GoodsGrid", QnTableView)
RegistChildComponent(LuaMengHuaLuShopWnd, "NameLabel", UILabel)
RegistChildComponent(LuaMengHuaLuShopWnd, "DescLabel", UILabel)
RegistChildComponent(LuaMengHuaLuShopWnd, "DescScrollView", UIScrollView)
RegistChildComponent(LuaMengHuaLuShopWnd, "NumberInput", QnAddSubAndInputButton)
RegistChildComponent(LuaMengHuaLuShopWnd, "Icon", CUITexture)
RegistChildComponent(LuaMengHuaLuShopWnd, "BuyBtn", CButton)
RegistChildComponent(LuaMengHuaLuShopWnd, "CostLabel", UILabel)
RegistChildComponent(LuaMengHuaLuShopWnd, "OwnLabel", UILabel)
RegistChildComponent(LuaMengHuaLuShopWnd, "AddBtn", GameObject)
RegistChildComponent(LuaMengHuaLuShopWnd, "ItemQualitySprite", UISprite)
RegistChildComponent(LuaMengHuaLuShopWnd, "Learned", GameObject)

RegistClassMember(LuaMengHuaLuShopWnd, "SelectedItemIndex")
RegistClassMember(LuaMengHuaLuShopWnd, "AllQiChang")
RegistClassMember(LuaMengHuaLuShopWnd, "GoodsInfos")

function LuaMengHuaLuShopWnd:Init()
	self:InitClassMembers()
	self:InitValues()

end

function LuaMengHuaLuShopWnd:InitClassMembers()
	self.SelectedItemIndex = 0
	self.AllQiChang = nil
end

function LuaMengHuaLuShopWnd:UpdateGoodInfos()
	self.GoodsInfos = {}
	self.AllQiChang = LuaMengHuaLuMgr.m_ShopQiChangs

	for i = 0, LuaMengHuaLuMgr.m_Goods.Count-1 do
		local goodInfo = LuaMengHuaLuMgr.m_Goods[i]
		if goodInfo.Count >= 2 then

			local itemId = goodInfo[0]
			local price = goodInfo[1]
			local isLearnedQiChang = self:IsLearnedQiChang(itemId)

			table.insert(self.GoodsInfos, {
				itemId = itemId,
				price = price,
				isLearnedQiChang = isLearnedQiChang,
				})
		end
		
	end
	self.GoodsGrid:ReloadData(false, false)
	self.GoodsGrid:SetSelectRow(0, true)
end

function LuaMengHuaLuShopWnd:IsLearnedQiChang(qichangId)
	if not self.AllQiChang then return false end
	for i = 0, self.AllQiChang.Count-1 do
		if self.AllQiChang[i] == qichangId then
			return true
		end
	end
	return false
end

function LuaMengHuaLuShopWnd:InitValues()

	local onAddBtnClicked = function (go)
		self:OnAddBtnClicked(go)
	end
	CommonDefs.AddOnClickListener(self.AddBtn, DelegateFactory.Action_GameObject(onAddBtnClicked), false)

	local onBuyBtnClicked = function (go)
		self:OnBuyBtnClicked(go)
	end
	CommonDefs.AddOnClickListener(self.BuyBtn.gameObject, DelegateFactory.Action_GameObject(onBuyBtnClicked), false)

	self.GoodsGrid.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.GoodsInfos
        end, function(item, index)
            self:InitItem(item.transform,index)
     end)

	self.GoodsGrid.OnSelectAtRow = DelegateFactory.Action_int(function(row)
        self.SelectedItemIndex = row
        self:UpdateDescription()

        self:RefreshInputMinMax(row)
    end)

	self:UpdateGoodInfos()
end

function LuaMengHuaLuShopWnd:InitItem(item, index)
	local goodInfo = self.GoodsInfos[index+1]
	if not goodInfo then return end

	local itemId = goodInfo.itemId
	local price = goodInfo.price
	local isLearnedQiChang = goodInfo.isLearnedQiChang
	

	local icon = item.transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local priceLabel = item.transform:Find("PriceLabel"):GetComponent(typeof(UILabel))
    local numLimitLabel = item.transform:Find("NumLimitLabel"):GetComponent(typeof(UILabel))
    local itemQualitySprite = item.transform:Find("Icon/ItemQualitySprite"):GetComponent(typeof(UISprite))
    local learned = item.transform:Find("Learned").gameObject
    icon:Clear()

	local item = MengHuaLu_Item.GetData(itemId)

	if item then
		icon:LoadMaterial(item.Icon)
    	nameLabel.text = self:GetItemName(item)
   		priceLabel.text = tostring(price)
    	numLimitLabel.text = nil
    	itemQualitySprite.spriteName = CUICommonDef.GetItemCellBorder(self:GetItemQuality(item.Color))
    	learned:SetActive(isLearnedQiChang)
	end
    
end

function LuaMengHuaLuShopWnd:UpdateDescription()
    local goodInfo = self.GoodsInfos[self.SelectedItemIndex+1]
    if not goodInfo then return end

    local itemId = goodInfo.itemId
	local count = 1
	local price = goodInfo.price
	local isLearnedQiChang = goodInfo.isLearnedQiChang

	local item = MengHuaLu_Item.GetData(itemId)
	if item then
		self.Icon:LoadMaterial(item.Icon)
		self.NameLabel.text = self:GetItemName(item)
    	self.DescLabel.text = CChatLinkMgr.TranslateToNGUIText(item.Description, false)
    	self.DescScrollView:ResetPosition()
    	self.CostLabel.text = tostring(price)
    	self.ItemQualitySprite.spriteName = CUICommonDef.GetItemCellBorder(self:GetItemQuality(item.Color))

    	self.NumberInput.onValueChanged = DelegateFactory.Action_uint(function(val)
        	local goodInfo = self.GoodsInfos[self.SelectedItemIndex+1]
        	local itemId = goodInfo.itemId
			local price = goodInfo.price
			local isLearnedQiChang = goodInfo.isLearnedQiChang

        	self.CostLabel.text = tostring(val * price)
    	end)
    	self.NumberInput:SetValue(1, true)
   		self.CostLabel.text = tostring(price)
    	self.Learned:SetActive(isLearnedQiChang)
	end
	
end

function LuaMengHuaLuShopWnd:RefreshInputMinMax(row)
	local goodInfo = self.GoodsInfos[row+1]
	if not goodInfo then return end

	local itemId = goodInfo.itemId
	local price = goodInfo.price
	local isLearnedQiChang = goodInfo.isLearnedQiChang

	self.OwnLabel.text = tostring(self:GetBabyGold())
	local maxNum = (math.floor(self:GetBabyGold() / price))
	self.NumberInput:SetMinMax(1, 1, 1)

end

-- 获得商店中物品的名字（气场带有颜色）
function LuaMengHuaLuShopWnd:GetItemName(item)
	if MengHuaLu_QiChang.Exists(item.ItemId) then
		local qichang = MengHuaLu_QiChang.GetData(item.ItemId)
		local babyQichang = Baby_QiChang.GetData(qichang.QiChangID)
		local name = babyQichang.NameM
		if LuaMengHuaLuMgr.m_BabyInfo and LuaMengHuaLuMgr.m_BabyInfo.AppearanceData[1] == 1 then
			name = babyQichang.NameF
		end
		return SafeStringFormat3("[%s]%s[-]", LuaBabyMgr.GetQiChangNameColor(babyQichang.Quality-1), name)
	else
		return item.Name
	end
end

function LuaMengHuaLuShopWnd:UpdateGoods()
	self:UpdateGoodInfos()
	self.GoodsGrid:ReloadData(false, false)
	self.GoodsGrid:SetSelectRow(0, true)
end

function LuaMengHuaLuShopWnd:GetBabyGold()
	return LuaMengHuaLuMgr.m_BabyInfo.Gold
end

function LuaMengHuaLuShopWnd:OnAddBtnClicked(go)
	MessageMgr.Inst:ShowMessage("MENGHUALU_ADD_MONEY", {})
end

function LuaMengHuaLuShopWnd:OnBuyBtnClicked(go)
	local needCount = self.NumberInput:GetValue()
	if needCount <= 0 then
		MessageMgr.Inst:ShowMessage("MENGHUALU_BUY_ZERO_ITEM", {})
		return
	end

	local goodInfo = self.GoodsInfos[self.SelectedItemIndex+1]
	local itemId = goodInfo.itemId
	local price = goodInfo.price
	local isLearnedQiChang = goodInfo.isLearnedQiChang

	if isLearnedQiChang then
		MessageMgr.Inst:ShowMessage("MENGHUALU_ITEM_ALREADY_GOT", {})
	else
		if self:GetBabyGold() >= needCount * price then
			Gac2Gas.RequestMengHuaLuBuyItem(itemId, needCount)
		else
			MessageMgr.Inst:ShowMessage("MENGHUALU_BUY_ITEM_NOT_ENOUTH_MONEY", {})
		end
	end
end

-- 更新宝宝的金币
function LuaMengHuaLuShopWnd:UpdateBabyGold(grid, gold)
	if grid == 0 then
		self.OwnLabel.text = tostring(self:GetBabyGold())
	end
end

function LuaMengHuaLuShopWnd:GetItemQuality(nameColor)
	if nameColor == "COLOR_WHITE" then
		return EnumQualityType.White
	elseif nameColor == "COLOR_GOLD" then
		return EnumQualityType.Yellow
	elseif nameColor == "COLOR_ORANGE" then
		return EnumQualityType.Orange
	elseif nameColor == "COLOR_GREEN" then
		return EnumQualityType.White
	elseif nameColor == "COLOR_BLUE" then
		return EnumQualityType.Blue
	elseif nameColor == "COLOR_RED" then
		return EnumQualityType.Red
	elseif nameColor == "COLOR_PURPLE" then
		return EnumQualityType.Purple
	end
end

function LuaMengHuaLuShopWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateMengHuaLuGoods", self, "UpdateGoods")
	g_ScriptEvent:AddListener("SyncMengHuaLuCharacterGold", self, "UpdateBabyGold")
end

function LuaMengHuaLuShopWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateMengHuaLuGoods", self, "UpdateGoods")
	g_ScriptEvent:RemoveListener("SyncMengHuaLuCharacterGold", self, "UpdateBabyGold")
end

return LuaMengHuaLuShopWnd
