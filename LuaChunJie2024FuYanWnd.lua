
LuaChunJie2024FuYanWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaChunJie2024FuYanWnd, "tipButton", "TipButton", GameObject)
RegistChildComponent(LuaChunJie2024FuYanWnd, "prepareRoot", "Prepare", Transform)
RegistChildComponent(LuaChunJie2024FuYanWnd, "cookRoot", "Cook", Transform)
RegistChildComponent(LuaChunJie2024FuYanWnd, "eatRoot", "Eat", Transform)
--@endregion RegistChildComponent end

function LuaChunJie2024FuYanWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	UIEventListener.Get(self.tipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)	    self:OnTipButtonClick()	end)
    --@endregion EventBind end
end

function LuaChunJie2024FuYanWnd:Init()

end

--@region UIEvent

function LuaChunJie2024FuYanWnd:OnTipButtonClick()
end

--@endregion UIEvent
