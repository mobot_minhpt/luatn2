-- Auto Generated!!
local BoxCollider = import "UnityEngine.BoxCollider"
local Byte = import "System.Byte"
local CButton = import "L10.UI.CButton"
local CChatInputLink = import "CChatInputLink"
local CChatInputMgr = import "L10.UI.CChatInputMgr"
local CChatLinkMgr = import "CChatLinkMgr"
local CChatMgr = import "L10.Game.CChatMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CommonDefs = import "L10.Game.CommonDefs"
local Constants = import "L10.Game.Constants"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local CPersonalSpaceSendMomentWnd = import "L10.UI.CPersonalSpaceSendMomentWnd"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local CQnSymbolParser = import "CQnSymbolParser"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local DelegateFactory = import "DelegateFactory"
local EChatPanel = import "L10.Game.EChatPanel"
local ENUM_AVATAR_RESULT = import "ENUM_AVATAR_RESULT"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local Int32 = import "System.Int32"
local L10 = import "L10"
local LoadPicMgr = import "L10.Game.LoadPicMgr"
local LocalString = import "LocalString"
local NativeHandle = import "NativeHandle"
local NGUITools = import "NGUITools"
local Object = import "UnityEngine.Object"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local String = import "System.String"
local Texture2D = import "UnityEngine.Texture2D"
local TextureFormat = import "UnityEngine.TextureFormat"
local Time = import "UnityEngine.Time"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local UITexture = import "UITexture"
local Vector3 = import "UnityEngine.Vector3"
local VoiceManager = import "VoiceManager"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local UIInput = import "UIInput"
local UIWidget = import "UIWidget"
local CSpeechCtrlMgr = import "L10.Game.CSpeechCtrlMgr"
local CSpeechCtrlType = import "L10.Game.CSpeechCtrlType"

LuaPersonalSpaceSendMoment = {}

function LuaPersonalSpaceSendMoment:StartTick(this)
  if self.m_Tick then
    UnRegisterTick(self.m_Tick)
  end
  local perTime = 500
  local totalNum = 999999
  local maxLine = 14
  self.m_TickCount = 0
  self.m_Tick = RegisterTickWithDuration(function ()
    local selectEnd = this.sendStatusPanel_normalStatusInput.selectionEnd
    local totalString = CommonDefs.StringLength(this.sendStatusPanel_normalStatusInput.value)
    self.inputStatusLabel.text = SafeStringFormat3(LocalString.GetString('内容最多800个字，还剩[FFFF00]%s[-][c]个字'),800-totalString)
    if not self.saveInputLabelHeight then
      self.saveInputLabelHeight = self.inputLabel.height
    end
    if selectEnd == totalString then
      if self.saveInputLabelHeight ~= self.inputLabel.height then
        if self.inputLabel.height > 468 then
          self.inputScrollView.contentPivot = UIWidget.Pivot.Bottom
        else
          self.inputScrollView.contentPivot = UIWidget.Pivot.Top
        end
        self.inputTable:Reposition()
        self.inputScrollView:ResetPosition()
        self.saveInputLabelHeight = self.inputLabel.height
      end
    end

    self.m_TickCount = self.m_TickCount + 1
    if self.m_TickCount > totalNum/2 then
      self:StartTick(this)
    end
  end,500,500*totalNum)
end

function LuaPersonalSpaceSendMoment:InitSendLongText(this)
  self.inputStatusLabel = this.sendStatusPanel.transform:Find('statusLabel'):GetComponent(typeof(UILabel))
  self.inputLabel = this.sendStatusPanel.transform:Find('sc/ScrollView/Table/Label'):GetComponent(typeof(UILabel))
  self.inputScrollView = this.sendStatusPanel.transform:Find('sc/ScrollView'):GetComponent(typeof(UIScrollView))
  self.inputTable = this.sendStatusPanel.transform:Find('sc/ScrollView/Table'):GetComponent(typeof(UITable))

  local closeBtn = this.sendStatusPanel.transform:Find('Wnd_Bg_Secondary_1/CloseButton').gameObject
  UIEventListener.Get(closeBtn).onClick = DelegateFactory.VoidDelegate(function (p)
    VoiceManager.onPicMsg = nil
    this:Close()
  end)

  self:StartTick(this)
end


CPersonalSpaceSendMomentWnd.m_SetPicShowAndSaveData_CS2LuaHook = function (this, textureByte, _tex)
    if this.nowPicNode ~= nil then
        if CommonDefs.GetComponent_Component_Type(this.nowPicNode.transform:Find("pic"), typeof(UITexture)).mainTexture == nil then
            this.nowPicNode.transform:Find("pic").gameObject:SetActive(true)
            this.nowPicNode.transform:Find("bg").gameObject:SetActive(false)
            this.nowPicNode.transform:Find("videoNode").gameObject:SetActive(false)
            if _tex == nil then
                local tex = CreateFromClass(Texture2D, 4, 4, TextureFormat.RGBA32, false)
                CommonDefs.LoadImage(tex, textureByte)
                CommonDefs.GetComponent_Component_Type(this.nowPicNode.transform:Find("pic"), typeof(UITexture)).mainTexture = tex
            else
                CommonDefs.GetComponent_Component_Type(this.nowPicNode.transform:Find("pic"), typeof(UITexture)).mainTexture = _tex
            end

            CommonDefs.DictSet(this.saveTextureDic, typeof(Int32), this.picCount - 1, typeof(MakeArrayClass(Byte)), textureByte)

            CommonDefs.GetComponent_GameObject_Type(this.nowPicNode, typeof(BoxCollider)).enabled = false
            this.nowPicNode = nil

            CommonDefs.GetComponent_Component_Type(this.picListNode.transform.parent:Find("title"), typeof(UILabel)).text = LocalString.GetString("还可添加") .. (6 - this.saveTextureDic.Count) .. LocalString.GetString("张图片")
            this:SetAddPicBtn()

            return
        end
    end

    if _tex ~= nil then
        Object.Destroy(_tex)
    end
end
CPersonalSpaceSendMomentWnd.m_AddPicBack_CS2LuaHook = function (this, textureBytes)
    if LoadPicMgr.CheckGifPic(textureBytes) then
        --Main.Inst.StartCoroutine(LoadPicMgr.Inst.GetTextureListCoroutine(textureBytes, delegate(List<UniGif.GifTexture> gifTexList)
        --{
        --    if (gifTexList != null)
        --    {
        --        if (gifTexList.Count > 0)
        --        {
        --            UniGif.GifTexture gif_texture = gifTexList[0];
        --            SetPicShowAndSaveData(textureBytes, gif_texture.m_texture2d);
        --        }
        --    }
        --    else
        --    {
        --        Debug.LogError("Gif texture get error.");
        --    }
        --}));
    else
        this:SetPicShowAndSaveData(textureBytes, nil)
    end
end
CPersonalSpaceSendMomentWnd.m_OnMomentAvaterCallBack_CS2LuaHook = function (this, strResult)
    if (strResult == ToStringWrap(ENUM_AVATAR_RESULT.eResult_Success)) then
        -- 成功
        this:StartCoroutine(this:LoadMomentTexture(CPersonalSpaceSendMomentWnd.MomentPicSaveName))
    elseif (strResult == ToStringWrap(ENUM_AVATAR_RESULT.eResult_Cancel)) then
        -- 取消
    elseif (strResult == ToStringWrap(ENUM_AVATAR_RESULT.eResult_Failed)) then
        -- 失败
    end
end
CPersonalSpaceSendMomentWnd.m_AddMomentVideo_CS2LuaHook = function (this)

    if CSpeechCtrlMgr.Inst:CheckIfNeedSpeechCtrl(CSpeechCtrlType.Type_PersonalSpace_Add_Moment_Video, true) then
        return
    end

    if CClientMainPlayer.Inst == nil then
        return
    end

    if not LoadPicMgr.EnableMomentVideo then
        return
    end

    VoiceManager.onPicMsg = MakeDelegateFromCSFunction(this.SetMomentVideoBack, MakeGenericClass(Action1, String), this)
    this.m_Native:SettingVideoFromMobile("Main/VoiceListener", "OnAvaterCallBack", CPersonalSpaceSendMomentWnd.MomentVideoSaveName, MakeDelegateFromCSFunction(this.AddVideoBack, MakeGenericClass(Action1, MakeArrayClass(Byte)), this))
end
CPersonalSpaceSendMomentWnd.m_AddVideoBack_CS2LuaHook = function (this, data)
    if data ~= nil and data.Length >= LoadPicMgr.VideoMaxSize then
        g_MessageMgr:ShowMessage("MENGDAO_VIDEO_EXCEEDSIZE")
        return
    end

    if this.nowPicNode ~= nil then
        CommonDefs.GetComponent_Component_Type(this.picListNode.transform.parent:Find("title"), typeof(UILabel)).text = LocalString.GetString("最多可添加一个视频")
        this.nowPicNode.transform:Find("bg").gameObject:SetActive(false)
        this.nowPicNode.transform:Find("videoNode").gameObject:SetActive(true)
        this.videoData = data
    end
end
CPersonalSpaceSendMomentWnd.m_ShowBtn_CS2LuaHook = function (this, target)
    local alignType = CPopupMenuInfoMgr.AlignType.Right
    local rows = CreateFromClass(MakeGenericClass(List, PopupMenuItemData))
    CommonDefs.ListAdd(rows, typeof(PopupMenuItemData), PopupMenuItemData(LocalString.GetString("图片"), DelegateFactory.Action_int(function (row)
        this:AddMomentPic(target)
    end), false, nil))
    CommonDefs.ListAdd(rows, typeof(PopupMenuItemData), PopupMenuItemData(LocalString.GetString("视频"), DelegateFactory.Action_int(function (row)
        this:AddMomentVideo()
    end), false, nil))

    L10.UI.CPopupMenuInfoMgr.ShowPopupMenu(CommonDefs.ListToArray(rows), target.transform, alignType)
end
CPersonalSpaceSendMomentWnd.m_SendMoment_CS2LuaHook = function (this)

    local textureList = CreateFromClass(MakeGenericClass(List, MakeArrayClass(Byte)))

    do
        local i = 0
        while i < this.picListNode.transform.childCount do
            local momentPicNode = this.picListNode.transform:GetChild(i):Find("pic").gameObject
            local texture = CommonDefs.GetComponent_GameObject_Type(momentPicNode, typeof(UITexture)).mainTexture
            if not momentPicNode.activeSelf then
                texture = nil
            end

            --if (texture != null)
            --    textureList.Add(texture);
            if CommonDefs.DictContains(this.saveTextureDic, typeof(Int32), i) then
                CommonDefs.ListAdd(textureList, typeof(MakeArrayClass(Byte)), CommonDefs.DictGetValue(this.saveTextureDic, typeof(Int32), i))
            end
            i = i + 1
        end
    end

    local sendInfo = this.sendStatusPanel_statusInput.value
    --if (string.IsNullOrEmpty(sendInfo) && textureList.Count == 0)
    --{
    --    MessageMgr.Inst.ShowMessage("ENTER_TEXT_TIP");
    --    return;
    --}
    this.sendStatusPanel_statusInput.value = CQnSymbolParser.FilterExceededEmoticons(StringTrim(sendInfo), Constants.MaxEmotionNum)
    local msg = this.sendStatusPanel_statusInput.value
    if this.sendStatusPanel_normalStatusInput.gameObject.activeSelf then
        msg = this.sendStatusPanel_normalStatusInput.value
    elseif this.sendStatusPanel_statusInput.gameObject.activeSelf then
        msg = this.sendStatusPanel_statusInput.value
    end

    if Time.realtimeSinceStartup - this.lastSendTime < 1 then
        return
    end
    this.lastSendTime = Time.realtimeSinceStartup
    local sendText = CChatInputLink.Encapsulate(msg, this.existingLinks)

    if this.sendStatusPanel_normalStatusInput.gameObject.activeSelf then
        sendText = CChatMgr.Inst:FilterYangYangEmotion(sendText)
        if System.String.IsNullOrEmpty(sendText) and textureList.Count == 0 and this.videoData and this.videoData.Length == 0 then
            return
        end
    elseif this.sendStatusPanel_statusInput.gameObject.activeSelf then
        sendText = CWordFilterMgr.Inst:DoFilterOnSendPersonalSpace(sendText, true)
        if sendText == nil then
            return
            --未通过过滤
        end
        --sharetext部分不再进行过滤
        sendText = this.shareText .. CChatMgr.Inst:FilterYangYangEmotion(sendText)
    end

    if textureList.Count == 0 then
        if this.videoData and this.videoData.Length > 0 then
          g_MessageMgr:ShowMessage("MENGDAO_SHIPIN_SHANGCHUANZHONG")
            CPersonalSpaceMgr.Inst:UploadPic(CPersonalSpaceMgr.Upload_VideoType, this.videoData, DelegateFactory.Action_string(function (url)
                if not this.sendStatusPanel then
                    return
                end
                local videoArray = Table2ArrayWithCount({url}, 1, MakeArrayClass(System.String))

                CPersonalSpaceMgr.Inst:AddMoment(sendText, nil, videoArray, DelegateFactory.Action_string_CPersonalSpace_AdvRet(function (textAfterFilter, data)
                    if data.code == 0 then
                        if this.IsShareText and CPersonalSpaceMgr.Inst.OnShareTextComplete ~= nil then
                            GenericDelegateInvoke(CPersonalSpaceMgr.Inst.OnShareTextComplete, Table2ArrayWithCount({this.shareText}, 1, MakeArrayClass(Object)))
                        end
                    else
                        g_MessageMgr:ShowMessage("CUSTOM_STRING2", data.msg)
                    end
                    if not this.sendStatusPanel then
                        return
                    end
                    g_MessageMgr:ShowMessage("MENGDAO_SHIPIN_SHANGCHUANCHENGGONG")
                    --AddMomentBack(textAfterFilter, data);
                    --sendStatusPanel.SetActive(false);
                    EventManager.BroadcastInternalForLua(EnumEventType.PersonalSpaceAddNewMoment, {textAfterFilter, data})
                    this:Close()
                end), DelegateFactory.Action(function ()
                    --MessageMgr.Inst.ShowMessage("JINGLING_MENGDAO_SHARE_FAILED");
                end), this.IsShareText)
            end))
        else
            CPersonalSpaceMgr.Inst:AddMoment(sendText, nil, nil, DelegateFactory.Action_string_CPersonalSpace_AdvRet(function (textAfterFilter, data)
                if data.code == 0 then
                    if this.IsShareText and CPersonalSpaceMgr.Inst.OnShareTextComplete ~= nil then
                        GenericDelegateInvoke(CPersonalSpaceMgr.Inst.OnShareTextComplete, Table2ArrayWithCount({this.shareText}, 1, MakeArrayClass(Object)))
                    end

                    EventManager.BroadcastInternalForLua(EnumEventType.PersonalSpaceAddNewMoment, {textAfterFilter, data})
                else
                    g_MessageMgr:ShowMessage("CUSTOM_STRING2", data.msg)
                end
                if not this.sendStatusPanel then
                    return
                end
                this:Close()
            end), DelegateFactory.Action(function ()
                g_MessageMgr:ShowMessage("INGAME_MENGDAO_SHARE_FAILED")
            end), this.IsShareText)
        end
    else
        local totalCount = 0
        local textureArray = CreateFromClass(MakeArrayClass(System.String), textureList.Count)

        g_MessageMgr:ShowMessage("MENGDAO_TUPIAN_SHANGCHUANZHONG")

        do
            local i = 0
            while i < textureList.Count do
                local index = i
                CPersonalSpaceMgr.Inst:UploadPic(CPersonalSpaceMgr.Upload_MomentPhotoType, textureList[i], DelegateFactory.Action_string(function (url)
                    if not this.sendStatusPanel then
                        return
                    end

                    totalCount = totalCount + 1
                    textureArray[index] = url

                    if totalCount >= textureList.Count then
                        CPersonalSpaceMgr.Inst:AddMoment(sendText, textureArray, nil, DelegateFactory.Action_string_CPersonalSpace_AdvRet(function (textAfterFilter, data)
                            if data.code == 0 then
                                if this.IsShareText and CPersonalSpaceMgr.Inst.OnShareTextComplete ~= nil then
                                    GenericDelegateInvoke(CPersonalSpaceMgr.Inst.OnShareTextComplete, Table2ArrayWithCount({this.shareText}, 1, MakeArrayClass(Object)))
                                end
                            else
                                g_MessageMgr:ShowMessage("CUSTOM_STRING2", data.msg)
                            end
                            if not this.sendStatusPanel then
                                return
                            end
                            g_MessageMgr:ShowMessage("MENGDAO_TUPIAN_SHANGCHUANCHENGGONG")
                            --AddMomentBack(textAfterFilter, data);
                            --sendStatusPanel.SetActive(false);
                            EventManager.BroadcastInternalForLua(EnumEventType.PersonalSpaceAddNewMoment, {textAfterFilter, data})
                            this:Close()
                        end), DelegateFactory.Action(function ()
                            --MessageMgr.Inst.ShowMessage("JINGLING_MENGDAO_SHARE_FAILED");
                        end), this.IsShareText)
                    end
                end))
                i = i + 1
            end
        end
    end
    CommonDefs.GetComponent_GameObject_Type(this.sendStatusPanel_submitBtn, typeof(CButton)).enabled = false
end
CPersonalSpaceSendMomentWnd.m_OnInputEmoticon_CS2LuaHook = function (this, prefix)
    if this.sendStatusPanel_statusInput ~= nil then
        this.sendStatusPanel_statusInput.value = this.sendStatusPanel_statusInput.value .. ("#" .. prefix)
    end
    if this.sendStatusPanel_normalStatusInput ~= nil then
        this.sendStatusPanel_normalStatusInput.value = this.sendStatusPanel_normalStatusInput.value .. ("#" .. prefix)
    end
end
CPersonalSpaceSendMomentWnd.m_OnInputBabyLink_CS2LuaHook = function (this, babyId, babyName)
    local text = nil
    text = CChatInputLink.AppendBabyLink(this.sendStatusPanel_statusInput.value, babyId, babyName, this.existingLinks)
    this.sendStatusPanel_statusInput.value = text
    this.sendStatusPanel_normalStatusInput.value = text
end
CPersonalSpaceSendMomentWnd.m_OnEmoticonButtonClick_CS2LuaHook = function (this, go)
    if not this.IsShareText then
        CChatInputMgr.ShowChatInputWnd(CChatInputMgr.EParentType.PersonalSpace, this, go, EChatPanel.Undefined, 0, 0)
    else
        CChatInputMgr.ShowChatInputWnd(CChatInputMgr.EParentType.JingLingShareToPersonalSpace, this, go, EChatPanel.Undefined, 0, 0)
    end
end
CPersonalSpaceSendMomentWnd.m_SetAddPicBtn_CS2LuaHook = function (this)
    if this.picCount < 6 then
        local node = NGUITools.AddChild(this.picListNode, this.picTemplate)
        node:SetActive(true)
        node.transform.localPosition = Vector3(this.picCount * this.picDis, 0, 0)
        this.nowPicNode = node
        this.picCount = this.picCount + 1
        UIEventListener.Get(node).onClick = MakeDelegateFromCSFunction(this.AddMomentPic, VoidDelegate, this)
    end
end
CPersonalSpaceSendMomentWnd.m_SetAddPicAndVideoBtn_CS2LuaHook = function (this)
    Extensions.RemoveAllChildren(this.picListNode.transform)

    local node = NGUITools.AddChild(this.picListNode, this.picTemplate)
    node:SetActive(true)
    node.transform.localPosition = Vector3(this.picCount * this.picDis, 0, 0)
    this.nowPicNode = node
    this.picCount = this.picCount + 1
    UIEventListener.Get(node).onClick = MakeDelegateFromCSFunction(this.ShowBtn, VoidDelegate, this)
end
CPersonalSpaceSendMomentWnd.m_InitSendStatusPanel_CS2LuaHook = function (this)
    this.sendStatusPanel:SetActive(true)

    CommonDefs.DictClear(this.existingLinks)
    this.sendStatusPanel_statusInput.value = ""

    UIEventListener.Get(this.sendStatusPanel_cancelBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        VoiceManager.onPicMsg = nil
        --sendStatusPanel.SetActive(false);
        this:Close()
    end)

    --for (int i = 1; i <= 3; i++)
    --{
    --    GameObject addPicBtn = sendStatusPanel.transform.FindChild("AddPic/add" + i + "/button").gameObject;
    --    sendStatusPanel.transform.FindChild("AddPic/add" + i + "/pic").gameObject.SetActive(false);
    --    sendStatusPanel.transform.FindChild("AddPic/add" + i + "/pic").GetComponent<UITexture>().mainTexture = null;
    --    sendStatusPanel.transform.FindChild("AddPic/add" + i + "/bg").gameObject.SetActive(true);
    --    UIEventListener.Get(addPicBtn).onClick = AddMomentPic;
    --}

    if LoadPicMgr.EnableMomentVideo then
        this:SetAddPicAndVideoBtn()
        CommonDefs.GetComponent_Component_Type(this.picListNode.transform.parent:Find("title"), typeof(UILabel)).text = LocalString.GetString("添加图片(最多6张)或视频")
    else
        this:SetAddPicBtn()
        CommonDefs.GetComponent_Component_Type(this.picListNode.transform.parent:Find("title"), typeof(UILabel)).text = LocalString.GetString("添加图片(最多6张)")
    end

    CommonDefs.GetComponent_GameObject_Type(this.sendStatusPanel_submitBtn, typeof(CButton)).enabled = true
    UIEventListener.Get(this.sendStatusPanel_submitBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        this:SendMoment()
    end)

    UIEventListener.Get(this.sendStatusPanel_emotionBtn).onClick = MakeDelegateFromCSFunction(this.OnEmoticonButtonClick, VoidDelegate, this)
end
CPersonalSpaceSendMomentWnd.m_Init_CS2LuaHook = function (this)
  this.picTemplate:SetActive(false)
  this.m_Native = CreateFromClass(NativeHandle)

  local normalSendPanel = this.sendStatusPanel
  local longSendPanel = this.transform:Find('Anchor/LongStatusPanel').gameObject
  normalSendPanel:SetActive(false)
  longSendPanel:SetActive(false)

  if LuaPersonalSpaceMgrReal.SendMomentType and LuaPersonalSpaceMgrReal.SendMomentType == 2 then
    this.sendStatusPanel = longSendPanel
    this.sendStatusPanel:SetActive(true)

    this.sendStatusPanel_normalStatusInput = this.sendStatusPanel.transform:Find('StatusText'):GetComponent(typeof(UIInput))
    this.sendStatusPanel_submitBtn = this.sendStatusPanel.transform:Find('opNode/SubmitButton').gameObject
    this.sendStatusPanel_cancelBtn = this.sendStatusPanel.transform:Find('opNode/CancelButton').gameObject
    this.sendStatusPanel_emotionBtn = this.sendStatusPanel.transform:Find('opNode/EmotionButton').gameObject
    this.picListNode = this.sendStatusPanel.transform:Find('opNode/AddPic/picListNode').gameObject
    this.picTemplate = this.sendStatusPanel.transform:Find('opNode/addNode').gameObject

    this.sendStatusPanel_statusInput.gameObject:SetActive(false)
    this.sendStatusPanel_DefaultTextLabel.gameObject:SetActive(false)
    this.sendStatusPanel_normalStatusInput.gameObject:SetActive(true)
    LuaPersonalSpaceSendMoment:InitSendLongText(this)
  else
    this.sendStatusPanel = normalSendPanel

    this.shareText = CPersonalSpaceMgr.Inst.shareText
    if not System.String.IsNullOrEmpty(this.shareText) then
      this.sendStatusPanel_statusInput.gameObject:SetActive(true)
      this.sendStatusPanel_DefaultTextLabel.gameObject:SetActive(true)
      this.sendStatusPanel_normalStatusInput.gameObject:SetActive(false)

      this.sendStatusPanel_statusInput.characterLimit = this.InputNumLimit

      local originText = CChatLinkMgr.TranslateToNGUIText(this.shareText, false)
      local outerText = System.String.Empty
      this.sendStatusPanel_DefaultTextLabel.text = originText
      --赋值对label各项参数进行初始化，否则下面的计算会出问题，label类型需要clamp content， maxlines = 1
      local default
      default, outerText = this.sendStatusPanel_DefaultTextLabel:Wrap(originText)
      if not default then
        outerText = CommonDefs.StringSubstring2(outerText, 0, CommonDefs.StringLength(outerText) - 1) .. "..."
        this.sendStatusPanel_DefaultTextLabel.text = outerText
      end
    else
      this.sendStatusPanel_statusInput.gameObject:SetActive(false)
      this.sendStatusPanel_DefaultTextLabel.gameObject:SetActive(false)
      this.sendStatusPanel_normalStatusInput.gameObject:SetActive(true)
    end
  end

  this:InitSendStatusPanel()
end
CPersonalSpaceSendMomentWnd.m_hookOnDestroy = function()
  if LuaPersonalSpaceSendMoment.m_Tick then
    UnRegisterTick(LuaPersonalSpaceSendMoment.m_Tick)
    LuaPersonalSpaceSendMoment.m_Tick = nil
  end
  LuaPersonalSpaceMgrReal.SendMomentType = nil
end
