local CItemCountUpdate = import "L10.UI.CItemCountUpdate"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local UILabel = import "UILabel"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local CItemMgr = import "L10.Game.CItemMgr"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CTooltip = import "L10.UI.CTooltip"
local MessageMgr=import "L10.Game.MessageMgr"

LuaEquipAdvDisassembleWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaEquipAdvDisassembleWnd, "CostItem", "CostItem", CItemCountUpdate)
RegistChildComponent(LuaEquipAdvDisassembleWnd, "DisassembleButton", "DisassembleButton", GameObject)
RegistChildComponent(LuaEquipAdvDisassembleWnd, "Equip1", "Equip1", GameObject)
RegistChildComponent(LuaEquipAdvDisassembleWnd, "Toggle", "Toggle", QnSelectableButton)
RegistChildComponent(LuaEquipAdvDisassembleWnd, "CostTipLabel", "CostTipLabel", UILabel)
RegistChildComponent(LuaEquipAdvDisassembleWnd, "QnCostAndOwnMoney", "QnCostAndOwnMoney", CCurentMoneyCtrl)
RegistChildComponent(LuaEquipAdvDisassembleWnd, "ItemGrid", "ItemGrid", GameObject)
RegistChildComponent(LuaEquipAdvDisassembleWnd, "GetItem", "GetItem", GameObject)

--@endregion RegistChildComponent end

RegistClassMember(LuaEquipAdvDisassembleWnd, "m_itemId") --拆解的道具
RegistClassMember(LuaEquipAdvDisassembleWnd, "m_getItems") --可以获得的道具

RegistClassMember(LuaEquipAdvDisassembleWnd, "m_costTemplateId") --消耗道具
RegistClassMember(LuaEquipAdvDisassembleWnd, "m_needCount")

RegistClassMember(LuaEquipAdvDisassembleWnd, "m_getTemplateId") --额外获得的道具
RegistClassMember(LuaEquipAdvDisassembleWnd, "m_getCount")

RegistClassMember(LuaEquipAdvDisassembleWnd, "m_costCheck")

function LuaEquipAdvDisassembleWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    UIEventListener.Get(self.CostItem.gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:OnCostItemClick()
        end
    )

    UIEventListener.Get(self.DisassembleButton.gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:OnDisassembleButtonClick()
        end
    )

    UIEventListener.Get(self.Equip1.gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:OnEquip1Click()
        end
    )

    self.Toggle.OnButtonSelected =
        DelegateFactory.Action_bool(
        function(selected)
            self:OnToggleSelected(selected)
        end
    )

    --@endregion EventBind end
    
end

function LuaEquipAdvDisassembleWnd:Init()
    self.m_itemId = CLuaEquipMgr.DisassembleItemId
    local items, ccm = CLuaEquipMgr.GetDisassembleRetItems(self.m_itemId)
    self.m_getItems = items

    local settingData = EquipBaptize_Setting.GetData()

    self.m_costTemplateId = settingData.ChangChunMuItemId
    self.m_getTemplateId = settingData.DongGuangZhuItemId
    self.m_needCount = ccm[1]
    self.m_getCount = ccm[2]

    self:InitMyMoney()

    self:InitEquip(self.Equip1, self.m_itemId)
    self:InitItem()

    self:OnToggleSelected(true)
end

--[[
    @desc: 消耗物
    author:Codee
    time:2022-06-27 15:34:18
    @return:
]]
function LuaEquipAdvDisassembleWnd:InitItem()
    local template = CItemMgr.Inst:GetItemTemplate(self.m_costTemplateId)
    local tf = self.CostItem.transform
    local icon = FindChildWithType(tf, "MatIcon", typeof(CUITexture))
    local mask = FindChildWithType(tf, "Mask", typeof(GameObject))
    if template then
        icon:LoadMaterial(template.Icon)
    else
        icon:Clear()
    end
    self.CostItem.templateId = self.m_costTemplateId

    local function OnCountChange(val)
        if val >= self.m_needCount then
            mask:SetActive(false)
            self.CostItem.format = SafeStringFormat3("{0}/%d", self.m_needCount)
        else
            mask:SetActive(true)
            self.CostItem.format = SafeStringFormat3("[ff0000]{0}[-]/%d", self.m_needCount)
        end
    end

    self.CostItem.onChange = DelegateFactory.Action_int(OnCountChange)
    self.CostItem:UpdateCount()
end

--[[
    @desc: 拆解的装备
    author:Codee
    time:2022-06-27 15:34:27
    --@ctrlgo:
	--@itemid: 
    @return:
]]
function LuaEquipAdvDisassembleWnd:InitEquip(ctrlgo, itemid)
    local tf = ctrlgo.transform
    local icon = FindChildWithType(tf, "EquipIcon", typeof(CUITexture))
    local qualitySprite = FindChildWithType(tf, "Quality", typeof(UISprite))
    local bindSprite = FindChildWithType(tf, "BindSprite", typeof(UISprite))

    local nameLabel = FindChildWithType(tf, "Label", typeof(UILabel))

    local commonItem = CItemMgr.Inst:GetById(itemid)

    icon:LoadMaterial(commonItem.Icon)

    qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(commonItem.Equip.QualityType)

    if bindSprite then
        bindSprite.spriteName = commonItem.BindOrEquipCornerMark
    end
    nameLabel.text = commonItem.Equip.ColoredDisplayName
end

--[[
    @desc: 获得的道具
    author:Codee
    time:2022-06-27 15:34:36
    @return:
]]
function LuaEquipAdvDisassembleWnd:InitGetItems()
    CUICommonDef.ClearTransform(self.ItemGrid.transform)

    if self.m_costCheck then
        local tempid = self.m_getTemplateId --可以获得的额外的道具id
        local count = self.m_getCount --可以获得的额外的道具数量

        if tempid > 0 then
            local go = NGUITools.AddChild(self.ItemGrid.gameObject, self.GetItem)
            go:SetActive(true)
            self:InitGetItem(go, tempid, count)
        end
    end
    for i, v in ipairs(self.m_getItems) do
        local itemtid = v[1]
        local count = v[2]
        local go = NGUITools.AddChild(self.ItemGrid.gameObject, self.GetItem)
        go:SetActive(true)
        self:InitGetItem(go, itemtid, count)
    end

    local parent = self.ItemGrid.transform
    local childCount = parent.childCount
    if childCount == 1 then
        LuaUtils.SetLocalPosition(parent:GetChild(0), 0, 0, 0)
    elseif childCount == 2 then
        LuaUtils.SetLocalPosition(parent:GetChild(0), -70, 0, 0)
        LuaUtils.SetLocalPosition(parent:GetChild(1), 70, 0, 0)
    elseif childCount == 3 then
        LuaUtils.SetLocalPosition(parent:GetChild(0), 0, 70, 0)
        LuaUtils.SetLocalPosition(parent:GetChild(1), -70, -70, 0)
        LuaUtils.SetLocalPosition(parent:GetChild(2), 70, -70, 0)
    elseif childCount == 4 then
        LuaUtils.SetLocalPosition(parent:GetChild(0), -70, 70, 0)
        LuaUtils.SetLocalPosition(parent:GetChild(1), 70, 70, 0)
        LuaUtils.SetLocalPosition(parent:GetChild(2), -70, -70, 0)
        LuaUtils.SetLocalPosition(parent:GetChild(3), 70, -70, 0)
    else
        self.m_Grid:Reposition()
    end
end

function LuaEquipAdvDisassembleWnd:InitGetItem(ctrlgo, itemtid, count)
    local tf = ctrlgo.transform
    local icon = FindChildWithType(tf, "Icon", typeof(CUITexture))
    local label = FindChildWithType(tf, "Label", typeof(UILabel))
    local data = Item_Item.GetData(itemtid)
    if data then
        icon:LoadMaterial(data.Icon)
    else
        icon:Clear()
    end
    label.text = tostring(count)

    CommonDefs.AddOnClickListener(
        ctrlgo,
        DelegateFactory.Action_GameObject(
            function(go)
                CItemInfoMgr.ShowLinkItemTemplateInfo(itemtid, false, nil, AlignType.ScreenRight, 0, 0, 0, 0)
            end
        ),
        false
    )
end

function LuaEquipAdvDisassembleWnd:InitMyMoney()
    local commonItem = CItemMgr.Inst:GetById(self.m_itemId)
    if commonItem and commonItem.IsBinded then
        self.QnCostAndOwnMoney.m_Type = EnumMoneyType.YinPiao
    end
    self.QnCostAndOwnMoney:SetCost(CLuaEquipMgr.GetCostYinLiang(self.m_itemId))
end

--@region UIEvent

function LuaEquipAdvDisassembleWnd:OnCostItemClick()
    if self.CostItem.count >= self.m_needCount then
        CItemInfoMgr.ShowLinkItemTemplateInfo(self.m_costTemplateId, false, nil, AlignType.ScreenRight, 0, 0, 0, 0)
    else
        CItemAccessListMgr.Inst:ShowItemAccessInfo(self.m_costTemplateId, false, nil, CTooltip.AlignType.Right)
    end
end

function LuaEquipAdvDisassembleWnd:OnDisassembleButtonClick()
    if not self.QnCostAndOwnMoney.moneyEnough then
        MessageMgr.Inst:ShowMessage("SILVER_NOT_ENOUGH", {})
        return
    end

    if self.m_costCheck then --勾选消耗长春木
        if self.CostItem.count < self.m_needCount then --但是数量不足
            MessageMgr.Inst:ShowMessage("Equip_ChaiJie_ChangChunMuItem_Is_Lack", {})
            return
        end
    end

    --通知服务器进行分解
    local itemInfo = CItemMgr.Inst:GetItemInfo(self.m_itemId)
    if itemInfo then
        --关闭界面
        CLuaEquipMgr.RequestDisassembleEquip(itemInfo.place, self.m_itemId,false, self.m_costCheck)
    end
end

function LuaEquipAdvDisassembleWnd:OnEquip1Click()
    local commonItem = CItemMgr.Inst:GetById(self.m_itemId)
    if commonItem then
        CItemInfoMgr.ShowLinkItemInfo(commonItem)
    end
end

function LuaEquipAdvDisassembleWnd:OnToggleSelected(selected)
    self.m_costCheck = selected
    if selected then
        self.CostTipLabel.text = LocalString.GetString("六词条及以上紫装可勾选消耗长春木获得额外物品(当前消耗长春木)")
    else
        self.CostTipLabel.text = LocalString.GetString("六词条及以上紫装可勾选消耗长春木获得额外物品(当前不消耗)")
    end

    self:InitGetItems()
end

--@endregion UIEvent
