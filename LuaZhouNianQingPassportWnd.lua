local Renderer = import "UnityEngine.Renderer"
local Animation = import "UnityEngine.Animation"
local ParticleSystem = import "UnityEngine.ParticleSystem"
local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local Item_Item = import "L10.Game.Item_Item"
local UITabBar = import "L10.UI.UITabBar"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local Ease = import "DG.Tweening.Ease"
local PathMode = import "DG.Tweening.PathMode"
local PathType = import "DG.Tweening.PathType"
local Vector4 = import "UnityEngine.Vector4"


LuaZhouNianQingPassportWnd = class()

RegistClassMember(LuaZhouNianQingPassportWnd, "m_LvLabel")
RegistClassMember(LuaZhouNianQingPassportWnd, "m_ExtLabel")
RegistClassMember(LuaZhouNianQingPassportWnd, "m_NeedExpLb")
RegistClassMember(LuaZhouNianQingPassportWnd, "m_ExpIcon")
RegistClassMember(LuaZhouNianQingPassportWnd, "m_BuyLevelBtn")
RegistClassMember(LuaZhouNianQingPassportWnd, "m_ExpSlider")
RegistClassMember(LuaZhouNianQingPassportWnd, "m_TipBtn")
RegistClassMember(LuaZhouNianQingPassportWnd, "m_TabBar")
RegistClassMember(LuaZhouNianQingPassportWnd, "m_TabBtn1")
RegistClassMember(LuaZhouNianQingPassportWnd, "m_TabBtn2")
RegistClassMember(LuaZhouNianQingPassportWnd, "m_TabBtn3")
RegistClassMember(LuaZhouNianQingPassportWnd, "m_RewardTab")
RegistClassMember(LuaZhouNianQingPassportWnd, "m_TaskTab")
RegistClassMember(LuaZhouNianQingPassportWnd, "m_ShopTab")
RegistClassMember(LuaZhouNianQingPassportWnd, "m_BuyAdvPassBtn")
RegistClassMember(LuaZhouNianQingPassportWnd, "m_OneKeyBtn")
RegistClassMember(LuaZhouNianQingPassportWnd, "m_LeftBackBtn")
RegistClassMember(LuaZhouNianQingPassportWnd, "m_RightBackBtn")

RegistClassMember(LuaZhouNianQingPassportWnd, "m_RewardTableView")
RegistClassMember(LuaZhouNianQingPassportWnd, "m_RewardScrollView")
RegistClassMember(LuaZhouNianQingPassportWnd, "m_ImpTitle")
RegistClassMember(LuaZhouNianQingPassportWnd, "m_ImpItem1")
RegistClassMember(LuaZhouNianQingPassportWnd, "m_ImpItem2")
RegistClassMember(LuaZhouNianQingPassportWnd, "m_ImpItem3")
RegistClassMember(LuaZhouNianQingPassportWnd, "m_Vip1Btn")
RegistClassMember(LuaZhouNianQingPassportWnd, "m_Vip2Btn")
RegistClassMember(LuaZhouNianQingPassportWnd, "m_UnlockVip1Fx")
RegistClassMember(LuaZhouNianQingPassportWnd, "m_UnlockVip2Fx")
RegistClassMember(LuaZhouNianQingPassportWnd, "m_LastSelectedImg")
RegistClassMember(LuaZhouNianQingPassportWnd, "m_LastSelectedSprite")

RegistClassMember(LuaZhouNianQingPassportWnd, "m_DailyTaskTableView")
RegistClassMember(LuaZhouNianQingPassportWnd, "m_WeekTaskTableView")

RegistClassMember(LuaZhouNianQingPassportWnd, "m_CurLevel")
RegistClassMember(LuaZhouNianQingPassportWnd, "m_CurImpLv")
RegistClassMember(LuaZhouNianQingPassportWnd, "m_CurProgress")
RegistClassMember(LuaZhouNianQingPassportWnd, "m_CurTab")
RegistClassMember(LuaZhouNianQingPassportWnd, "m_FirstReward")
RegistClassMember(LuaZhouNianQingPassportWnd, "m_RewardScrollViewStPosX")
RegistClassMember(LuaZhouNianQingPassportWnd, "m_LeftBorderReward")
RegistClassMember(LuaZhouNianQingPassportWnd, "m_RightBorderReward")
RegistClassMember(LuaZhouNianQingPassportWnd, "m_CurReward")
RegistClassMember(LuaZhouNianQingPassportWnd, "m_HasInited")
RegistClassMember(LuaZhouNianQingPassportWnd, "m_RewardAvailable")
RegistClassMember(LuaZhouNianQingPassportWnd, "m_RefreshShopTick")

function LuaZhouNianQingPassportWnd:Awake()
    self.m_LvLabel = self.transform:Find("TopView/ExpLv/LvLb"):GetComponent(typeof(UILabel))
    self.transform:Find("TopView/TimeLb"):GetComponent(typeof(UILabel)).text = LocalString.GetString("活动时间:")..g_MessageMgr:FormatMessage("ZNQ2023_LookForKid_Wnd_Msg1")
    self.m_ExtLabel = self.transform:Find("TopView/ExpLv/ExtSprite/ExtLabel"):GetComponent(typeof(UILabel))
    self.m_NeedExpLb = self.transform:Find("TopView/ExpLv/NeedExpLb"):GetComponent(typeof(UILabel))
    self.m_ExpIcon = self.transform:Find("TopView/ExpLv/ExpIcon").gameObject
    self.m_BuyLevelBtn = self.transform:Find("TopView/ExpLv/BuyLevelBtn").gameObject
    self.m_TipBtn = self.transform:Find("TopView/HelpBtn").gameObject
    self.m_ExpSlider = self.transform:Find("TopView/ExpLv/ExpSlider"):GetComponent(typeof(UISlider))
    self.m_TabBar = self.transform:Find("TabBtnsPanel"):GetComponent(typeof(UITabBar))
    self.m_RewardTab = self.transform:Find("RewardTabPanel")
    self.m_TaskTab = self.transform:Find("TaskTabPanel")
    self.m_ShopTab = self.transform:Find("ShopTabPanel")
    for i = 1, 3 do
        self["m_TabBtn"..i] = self.transform:Find("TabBtnsPanel"):GetChild(i - 1)
        self:SetRedDot(i, false)
    end
    self.m_TabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
        self:OnTabChange(index)
    end)
    UIEventListener.Get(self.m_BuyLevelBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.ShowUI(CLuaUIResources.ZhouNianQingPassportBuyLevelWnd)
    end)
    UIEventListener.Get(self.m_TipBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        g_MessageMgr:ShowMessage("ZNQ2023_ZhanLing_RuleTip")
    end)

    self.m_RewardTableView = self.transform:Find("RewardTabPanel/QnTableView"):GetComponent(typeof(QnTableView))
    self.m_RewardTableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #LuaZhouNianQing2023Mgr.m_ZhanLingData
        end,
        function(item, row)
            self:InitItem(item, row)
        end)

    self.m_RewardScrollView = self.m_RewardTableView.transform:Find("ScrollView"):GetComponent(typeof(UIScrollView))
    self.m_RewardScrollViewStPosX = self.m_RewardScrollView.transform.localPosition.x
    --self.m_RewardScrollView.onMomentumMove = DelegateFactory.OnDragNotification(function()
    --    self:OnRewardScroll()
    --end)
    self.m_RewardScrollView.horizontalScrollBar.OnChangeValue = DelegateFactory.Action_float(function(value)
        self:OnRewardScroll(value)
    end)
    self.m_ImpTitle = self.m_RewardTab:Find("Title/NextLabel"):GetComponent(typeof(UILabel))
    for i = 1, 3 do
        self["m_ImpItem"..i] = self.m_RewardTab:Find("Line"..i.."/Sprite2/Panel")
    end
    self.m_LeftBackBtn = self.m_RewardTab:Find("Arrow/LeftBackArrow").gameObject
    self.m_RightBackBtn = self.m_RewardTab:Find("Arrow/RightBackArrow").gameObject
    UIEventListener.Get(self.m_RightBackBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self.m_RewardScrollView:MoveRelative(Vector3(-math.abs(self.m_CurLevel - self.m_RightBorderReward) * 162 - 81, 0, 0))
		self.m_RewardScrollView:RestrictWithinBounds(true, true, true)
        self:OnRewardScroll(self.m_RewardScrollView.horizontalScrollBar.value)
    end)
    UIEventListener.Get(self.m_LeftBackBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self.m_RewardScrollView:MoveRelative(Vector3(math.abs(self.m_CurLevel - self.m_LeftBorderReward) * 162 + 81, 0, 0))
		self.m_RewardScrollView:RestrictWithinBounds(true, true, true)
        self:OnRewardScroll(self.m_RewardScrollView.horizontalScrollBar.value)
    end)

    self.m_Vip1Btn = self.transform:Find("RewardTabPanel/Line2/Sprite1").gameObject
    UIEventListener.Get(self.m_Vip1Btn).onClick = DelegateFactory.VoidDelegate(function(go)
        if not LuaZhouNianQing2023Mgr.m_IsVip1 then
            CUIManager.ShowUI(CLuaUIResources.ZhouNianQingPassportUnlockWnd)
        end
    end)
    self.m_Vip2Btn = self.transform:Find("RewardTabPanel/Line3/Sprite1").gameObject
    UIEventListener.Get(self.m_Vip2Btn).onClick = DelegateFactory.VoidDelegate(function(go)
        if not LuaZhouNianQing2023Mgr.m_IsVip2 then
            CUIManager.ShowUI(CLuaUIResources.ZhouNianQingPassportUnlockWnd)
        end
    end)
    self.m_BuyAdvPassBtn = self.transform:Find("RewardTabPanel/BuyAdvPassBtn").gameObject
    UIEventListener.Get(self.m_BuyAdvPassBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.ShowUI(CLuaUIResources.ZhouNianQingPassportUnlockWnd)
    end)
    self.m_OneKeyBtn = self.transform:Find("RewardTabPanel/OneKeyBtn").gameObject
    UIEventListener.Get(self.m_OneKeyBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        if self.m_RewardAvailable then 
            Gac2Gas.RequestGetLianDanLuReward(0, 0)
        else
            g_MessageMgr:ShowCustomMsg(LocalString.GetString("当前没有奖励可领取"))
        end
    end)
    self.m_UnlockVip1Fx = self.transform:Find("RewardTabPanel/Line2/vfx").gameObject
    self.m_UnlockVip2Fx = self.transform:Find("RewardTabPanel/Line3/vfx").gameObject

    self.m_DailyTaskTableView = self.m_TaskTab:Find("QnTableView1"):GetComponent(typeof(QnTableView))
    self.m_WeekTaskTableView = self.m_TaskTab:Find("QnTableView2"):GetComponent(typeof(QnTableView))
    self.m_DailyTaskTableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #LuaZhouNianQing2023Mgr.m_DailyTask
        end,
        function(item, row)
            self:InitTaskItem(item, row, true)
        end)
    self.m_WeekTaskTableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #LuaZhouNianQing2023Mgr.m_WeekTask
        end,
        function(item, row)
            self:InitTaskItem(item, row, false)
        end)
    self.m_DailyTaskTableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
        self:OnTaskSelectAtRow(row, true)
    end)
    self.m_WeekTaskTableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
        self:OnTaskSelectAtRow(row, false)
    end)
end

function LuaZhouNianQingPassportWnd:OnEnable()
    g_ScriptEvent:AddListener("ZNQ2023_ReceiveZhanLingRewardSuccess", self, "OnReceiveZhanLingRewardSuccess")
	g_ScriptEvent:AddListener("ZNQ2023_UnLockZhanLingVipSuccess", self, "OnUnLockZhanLingVipSuccess")
	g_ScriptEvent:AddListener("ZNQ2023_SyncZhanLingPlayData", self, "Init")
    g_ScriptEvent:AddListener("UpdateNpcShopLimitItemCount", self, "OnUpdateNPCShopInfo")
    g_ScriptEvent:AddListener("UpdateNPCShopInfo", self, "OnUpdateNPCShopInfo")
end

function LuaZhouNianQingPassportWnd:OnDisable()
    g_ScriptEvent:RemoveListener("ZNQ2023_ReceiveZhanLingRewardSuccess", self, "OnReceiveZhanLingRewardSuccess")
	g_ScriptEvent:RemoveListener("ZNQ2023_UnLockZhanLingVipSuccess", self, "OnUnLockZhanLingVipSuccess")
	g_ScriptEvent:RemoveListener("ZNQ2023_SyncZhanLingPlayData", self, "Init")
    g_ScriptEvent:RemoveListener("UpdateNpcShopLimitItemCount", self, "OnUpdateNPCShopInfo")
    g_ScriptEvent:RemoveListener("UpdateNPCShopInfo", self, "OnUpdateNPCShopInfo")
end

function LuaZhouNianQingPassportWnd:OnDestroy()
    UnRegisterTick(self.m_RefreshShopTick)
    self.m_RefreshShopTick = nil
end

function LuaZhouNianQingPassportWnd:InitAlert(bAlert)
    self.m_TabBar.transform:GetChild(0):Find("AlertSprite").gameObject:SetActive(bAlert)
    --self.m_TabBar.transform:GetChild(1):Find("AlertSprite").gameObject:SetActive(false)
    --self.m_TabBar.transform:GetChild(2):Find("AlertSprite").gameObject:SetActive(false)
    self.m_OneKeyBtn.transform:Find("Sprite").gameObject:SetActive(bAlert)
end

function LuaZhouNianQingPassportWnd:OnUpdateNPCShopInfo(shopId, templateId, remainCount)
    UnRegisterTick(self.m_RefreshShopTick)
    self.m_RefreshShopTick = RegisterTickOnce(function() 
        if CLuaNPCShopInfoMgr.m_npcShopInfo.ShopId == 34000097 then 
            local limitData = Shop_PlayScore.GetData(CLuaNPCShopInfoMgr.m_npcShopInfo.ShopId).Limit
            local id2limit = {}
            for i = 0, limitData.Length - 1 do
                id2limit[limitData[i][0]] = limitData[i][1]
            end
            local items = CLuaNPCShopInfoMgr.m_npcShopInfo.ItemGoods
            local prices = CLuaNPCShopInfoMgr.m_npcShopInfo.Prices
            local limits = CLuaNPCShopInfoMgr.m_npcShopInfo.Limits
            if templateId and remainCount then
                limits[templateId].limit = remainCount
            end
            local template = self.m_ShopTab:Find("Template").gameObject
            template:SetActive(false)
            local grid = self.m_ShopTab:Find("ScrollView/Grid"):GetComponent(typeof(UIGrid))
            Extensions.RemoveAllChildren(grid.transform)
            for i = 1, #items do
                local obj = NGUITools.AddChild(grid.gameObject, template)
                obj:SetActive(true)
                local data = Item_Item.GetData(items[i])
                obj.transform:Find("panel/Name"):GetComponent(typeof(UILabel)).text = data.Name
                obj.transform:Find("panel/Limit"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("限购%d/%d"), limits[items[i]].limit, id2limit[items[i]])
                obj.transform:Find("panel/Price"):GetComponent(typeof(UILabel)).text = prices[items[i]]
                local icon = obj.transform:Find("panel/Item/Icon")
                icon:GetComponent(typeof(CUITexture)):LoadMaterial(data.Icon)
                local btn = obj.transform:Find("panel/ExchangeBtn").gameObject
                if limits[items[i]].limit == 0 then
                    CUICommonDef.SetActive(btn, false, true)
                    btn.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("售 罄")
                else
                    UIEventListener.Get(btn).onClick = DelegateFactory.VoidDelegate(function(go)
                        if CClientMainPlayer.Inst then 
                            local score = 0
                            local info = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayScore, EnumTempPlayScoreKey_lua.ZhanLingScore)
                            if info then
                                if info.ExpiredTime and info.ExpiredTime <= CServerTimeMgr.Inst.timeStamp then
                                    score = 0 
                                else 
                                    score = info.Score
                                end
                            end
                            if score >= prices[items[i]] then
                                g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("ZNQ2023_SHOP_EXCHANGE_CONFIRM", prices[items[i]], data.Name), function()
                                    Gac2Gas.BuyNpcShopItem(CLuaNPCShopInfoMgr.m_npcShopInfo.NpcEngineId, CLuaNPCShopInfoMgr.m_npcShopInfo.ShopId, items[i], 1)
                                end, nil, nil, nil, false) 
                            else
                                g_MessageMgr:ShowMessage("ZNQ2023_SHOP_SCORE_NOT_ENOUGH")
                            end 
                        end
                    end)
                end
                UIEventListener.Get(icon.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
                    CItemInfoMgr.ShowLinkItemTemplateInfo(items[i], false, nil, AlignType.Default, 0, 0, 0, 0)
                end)
            end
            grid:Reposition()
    
            local scrollView = grid.transform.parent:GetComponent(typeof(UIScrollView))
            scrollView:MoveRelative(Vector3(0.1, 0, 0))
            --scrollView:RestrictWithinBounds(true, true, true)
        end
    
        if CClientMainPlayer.Inst then 
            local score = 0
            local info = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayScore, EnumTempPlayScoreKey_lua.ZhanLingScore)
            if info then
                if info.ExpiredTime and info.ExpiredTime <= CServerTimeMgr.Inst.timeStamp then
                    score = 0 
                else 
                    score = info.Score
                end
            end
            self.m_ShopTab:Find("Money"):GetChild(3):GetComponent(typeof(UILabel)).text = score
        end
    end, 33)
end

function LuaZhouNianQingPassportWnd:OnReceiveZhanLingRewardSuccess(level, index)
    self.m_RewardAvailable = false
    self.m_FirstReward = nil
    self.m_RewardTableView:ReloadData(false, false)
end

function LuaZhouNianQingPassportWnd:OnUnLockZhanLingVipSuccess(vip)
    CUIManager.CloseUI(CLuaUIResources.ZhouNianQingPassportUnlockWnd)

    self.m_CurLevel, self.m_CurProgress = LuaZhouNianQing2023Mgr:GetCurZhanLingLevel()
    local items = {}
    for i = 1, self.m_CurLevel do
        local item = LuaZhouNianQing2023Mgr.m_ZhanLingData[i]["item"..(vip+1)]
        if item then
            table.insert(items, {ItemID = item[1], Count = item[2]})
        end
    end
    if vip == 2 then 
        local exPrize = ZhanLing_Setting.GetData().Vip2ExtraPrize
        for j = 0, exPrize.Length - 1 do
            local prize = exPrize[j]
            table.insert(items, {ItemID = tonumber(prize[0]), Count = tonumber(prize[1])})
        end
    end
    LuaCommonGetRewardWnd.m_Reward1List = LuaZhouNianQing2023Mgr:MergeItems(items)
    items = {}
    for i = self.m_CurLevel + 1, ZhanLing_Reward.GetDataCount()  do
        local item = LuaZhouNianQing2023Mgr.m_ZhanLingData[i]["item"..(vip+1)]
        if item then
            table.insert(items, {ItemID = item[1], Count = item[2]})
        end
    end
    LuaCommonGetRewardWnd.m_Reward2List = LuaZhouNianQing2023Mgr:MergeItems(items)
    LuaCommonGetRewardWnd.m_materialName = "gongxijiesuo"
    LuaCommonGetRewardWnd.m_Reward2Label = #items > 0 and LocalString.GetString("升级愿灯还可获得：") or ""
    LuaCommonGetRewardWnd.m_hint = ""
    LuaCommonGetRewardWnd.m_button = {
        {
            spriteName = "blue", buttonLabel = LocalString.GetString("确定"), 
            clickCB = function() 
                CUIManager.CloseUI(CLuaUIResources.CommonGetRewardWnd) 
                CUIManager.CloseUI(CLuaUIResources.ZhouNianQingPassportUnlockWnd) 
                if self then 
                    self:PlayUnlockFx(vip)
                    RegisterTickOnce(function()
                        Gac2Gas.QueryLianDanLuAlertInfo()
                        Gac2Gas.QueryLianDanLuPlayData()
                    end, 1000)
                end
            end
        },
    }
    LuaCommonGetRewardWnd.m_hideCloseBtn = true
    CUIManager.ShowUI(CLuaUIResources.CommonGetRewardWnd)
end

function LuaZhouNianQingPassportWnd:OnTabChange(index)
    self.m_RewardTab.gameObject:SetActive(false)
    self.m_TaskTab.gameObject:SetActive(false)
    self.m_ShopTab.gameObject:SetActive(false)
    for i = 1, 3 do
        self["m_TabBtn"..i]:Find("Label").gameObject:SetActive(i ~= index + 1)
        if i <= 2 then 
            self["m_UnlockVip"..i.."Fx"]:SetActive(false)
        end
    end
    
    if self.m_LastSelectedImg then 
        self.m_LastSelectedImg:SetActive(false) 
        self.m_LastSelectedImg = nil
    end
    if self.m_LastSelectedSprite then 
        self.m_LastSelectedSprite:SetActive(false)
        self.m_LastSelectedSprite = nil
    end
    
    if index == 0 then -- Reward
        self.m_RewardTab.gameObject:SetActive(true)
        self:InitReward()
    elseif index == 1 then -- Task
        self.m_TaskTab.gameObject:SetActive(true)
        self:InitTask()
    elseif index == 2 then -- Shop
        self.m_ShopTab.gameObject:SetActive(true)
        self:InitShop()
    end

    self.m_CurTab = index
end

function LuaZhouNianQingPassportWnd:Init()
    self:InitTop()
    self:InitAlert(LuaZhouNianQing2023Mgr.m_bAlert)
    if not self.m_HasInited then 
        self.m_CurTab = (LuaZhouNianQing2023Mgr.m_bAlert or LuaZhouNianQing2023Mgr.m_AllTaskFinish) and 0 or 1 
    end
    self.m_TabBar:ChangeTab(self.m_CurTab, false)

    self.m_HasInited = true
end

function LuaZhouNianQingPassportWnd:InitTop()
    self.m_CurLevel, self.m_CurProgress = LuaZhouNianQing2023Mgr:GetCurZhanLingLevel()
	self.m_LvLabel.text = self.m_CurLevel
    if LuaZhouNianQing2023Mgr.m_IsVip2 then 
        self.m_ExtLabel.transform.parent.gameObject:SetActive(true)
        self.m_ExtLabel.text = "+"..math.floor((ZhanLing_Setting.GetData().Vip2AddRate-1)*100).."%"..LocalString.GetString("任务经验获得")
    else
        self.m_ExtLabel.transform.parent.gameObject:SetActive(false)
    end
    

    local curLvProgress = LuaZhouNianQing2023Mgr.m_Level2NeedProgress[self.m_CurLevel] or 0
	local nextLvProgress = LuaZhouNianQing2023Mgr.m_Level2NeedProgress[self.m_CurLevel + 1]
	if nextLvProgress then
		self.m_NeedExpLb.text = SafeStringFormat3(LocalString.GetString("升至下级还需%d灯芒"), nextLvProgress - self.m_CurProgress)
        self.m_ExpSlider.value = (self.m_CurProgress - curLvProgress) / (nextLvProgress - curLvProgress)
	else -- 满级
		self.m_NeedExpLb.text = LocalString.GetString("愿灯已满级")
        self.m_ExpSlider.value = 1
        self.m_ExpIcon:SetActive(false)
        self.m_BuyLevelBtn:SetActive(false)
	end
end

function LuaZhouNianQingPassportWnd:InitReward()
    self.m_Vip1Btn.transform:Find("Panel (1)/LockedSprite").gameObject:SetActive(not LuaZhouNianQing2023Mgr.m_IsVip1)
    self.m_Vip2Btn.transform:Find("Panel (1)/LockedSprite").gameObject:SetActive(not LuaZhouNianQing2023Mgr.m_IsVip2)
    self.m_BuyAdvPassBtn:SetActive(not (LuaZhouNianQing2023Mgr.m_IsVip1 and LuaZhouNianQing2023Mgr.m_IsVip2))
    self.m_RewardAvailable = false
    self.m_FirstReward = nil
    self.m_RewardTableView:ReloadData(false, false)
    self.m_OneKeyBtn.transform:Find("Sprite").gameObject:SetActive(LuaZhouNianQing2023Mgr.m_bAlert)

    if self.m_FirstReward then
        self.m_RewardScrollView:MoveRelative(Vector3(-(self.m_FirstReward-1) * 162, 0, 0))
    elseif self.m_CurLevel > 0 then
        self.m_RewardScrollView:MoveRelative(Vector3(-(self.m_CurLevel-1) * 162, 0, 0))
    end
    self.m_RewardScrollView:RestrictWithinBounds(true, true, true)
    self:OnRewardScroll(self.m_RewardScrollView.horizontalScrollBar.value)
end

function LuaZhouNianQingPassportWnd:InitTask()
    self.m_TaskTab.transform:Find("TitleLabel1"):GetComponent(typeof(UILabel)).text = 
        LocalString.GetString("今日任务  ")..LuaZhouNianQing2023Mgr:GetFinishTaskCnt(true).."/"..#LuaZhouNianQing2023Mgr.m_DailyTask
    self.m_TaskTab.transform:Find("TitleLabel2"):GetComponent(typeof(UILabel)).text = 
        LocalString.GetString("本周任务  ")..LuaZhouNianQing2023Mgr:GetFinishTaskCnt(false).."/"..#LuaZhouNianQing2023Mgr.m_WeekTask

    --已完成的任务放在最后
    local sortfuc = function(a,b)
        if a.progress == 1 and b.progress ~= 1 then
            return false
        elseif a.progress ~= 1 and b.progress == 1 then 
            return true
        else
            return a.taskId < b.taskId
        end  
    end
    
    table.sort(LuaZhouNianQing2023Mgr.m_DailyTask, sortfuc)
    table.sort(LuaZhouNianQing2023Mgr.m_WeekTask, sortfuc)
    self.m_DailyTaskTableView:ReloadData(false, false)
    self.m_WeekTaskTableView:ReloadData(false, false)
    self:InitFinishTask()
end

function LuaZhouNianQingPassportWnd:InitShop()
    CLuaNPCShopInfoMgr.ShowScoreShop("ZhanLingScore")
    self.m_ShopTab:Find("Hint"):GetComponent(typeof(UILabel)).text = g_MessageMgr:FormatMessage("ZNQ2023_SHOP_TIPS")
end

function LuaZhouNianQingPassportWnd:InitItem(item, row)
	local data = LuaZhouNianQing2023Mgr.m_ZhanLingData[row + 1]
	local level = data.level
	local flag = data.flag

    local lockBg = item.transform:Find("LockBg")
    local unlockBg = item.transform:Find("UnlockBg")
    lockBg.gameObject:SetActive(self.m_CurLevel<level)
    unlockBg.gameObject:SetActive(self.m_CurLevel>=level)
    local bg = self.m_CurLevel>=level and unlockBg or lockBg
    local label = bg:Find("Label"):GetComponent(typeof(UILabel))
    label.text = SafeStringFormat3(LocalString.GetString("%s级"), tostring(level))
    
    label.transform:GetChild(0).gameObject:SetActive(level > ZhanLing_Setting.GetData().ChangePriceLevel)
    item.transform:Find("SelectImg").gameObject:SetActive(level == self.m_CurLevel)

    for i = 1, 3 do
        self:InitRewardItem(item.transform:Find("Item"..i),i,level,flag)
    end

	if self.m_CurLevel == level or self.m_CurLevel == 0 and level == 1 then
        self.m_CurReward = item.transform
    end
end

function LuaZhouNianQingPassportWnd:InitRewardItem(item, idx, level, flag)
    local selectImg = item.parent:Find("SelectImg")
    if selectImg then selectImg = selectImg.gameObject end
    item = item:GetChild(0)
    local icon = item:Find("IconTexture"):GetComponent(typeof(CUITexture))
    local countLabel = item:Find("AmountLabel"):GetComponent(typeof(UILabel))
    local lock = item:Find("LockedSprite").gameObject
    local hasGotTag = item:Find("ClearupCheckbox").gameObject
    local fx = item:Find("Fx"):GetComponent(typeof(CUIFx))
    local qualitySprite = item:Find("QualitySprite"):GetComponent(typeof(UISprite))
    local selectedSprite = item:Find("SelectedSprite").gameObject
    local bindSprite = item:Find("BindSprite").gameObject

    local data = LuaZhouNianQing2023Mgr.m_ZhanLingData[level]
    local rewards
    local isVipUnLock = true
    if idx == 1 then
        rewards = data.item1
    elseif idx == 2 then
        rewards = data.item2
        isVipUnLock = LuaZhouNianQing2023Mgr.m_IsVip1
    elseif idx == 3 then
        rewards = data.item3
        isVipUnLock = LuaZhouNianQing2023Mgr.m_IsVip2
    end
    local itemId
    countLabel.text = nil
    if rewards then
        item.gameObject:SetActive(true)
        itemId = rewards[1]
        local count = rewards[2]
        local itemdata = Item_Item.GetData(itemId)
        icon:LoadMaterial(itemdata.Icon)
        countLabel.text = count > 1 and count or nil
        qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(itemdata, nil, false)
    else
        item.gameObject:SetActive(false)
        return
    end

    local isUnlock = false
    if isVipUnLock then
        if level <= self.m_CurLevel then
            isUnlock = true
        end 
    end
    lock:SetActive(not isUnlock)

    local bitmask = 2^(idx-1)
    local isGot = false
    if bit.band(flag,bitmask) ~= 0 then
        isGot = true
    else
        isGot = false
    end
    hasGotTag:SetActive(isGot)
    
    if isUnlock and not isGot then -- 可领取  
        if not self.m_FirstReward then 
            self.m_FirstReward = level 
        else
            self.m_FirstReward = math.min(self.m_FirstReward, level)
        end
        self.m_RewardAvailable = true
        fx:LoadFx("fx/ui/prefab/UI_kuang_yellow02.prefab")
        local b = NGUIMath.CalculateRelativeWidgetBounds(item.transform)
        local waypoints = CUICommonDef.GetWayPoints(Vector4(b.min.x, b.max.y, b.size.x, b.size.y), 1, true)
        LuaTweenUtils.DOLuaLocalPath(fx.transform, waypoints, 2, PathType.Linear, PathMode.Ignore, 2, - 1, Ease.Linear)
        
        UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            Gac2Gas.RequestGetLianDanLuReward(level, idx)
        end)
    else
        fx:DestroyFx()
        if itemId then
            UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
                if self.m_LastSelectedSprite then self.m_LastSelectedSprite:SetActive(false) end
                if self.m_LastSelectedImg then self.m_LastSelectedImg:SetActive(false) end
                if selectedSprite then selectedSprite:SetActive(true) end
                if selectImg then selectImg:SetActive(true) end
                self.m_LastSelectedSprite = selectedSprite
                self.m_LastSelectedImg = level ~= self.m_CurLevel and selectImg or nil
                CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType.Default, 0, 0, 0, 0)
            end)
        end
    end
end

function LuaZhouNianQingPassportWnd:InitTaskItem(item, row, isDaily)
    local goBtn = item.transform:Find("panel/GotoBtn")
    local descLabel = item.transform:Find("panel/Desc"):GetComponent(typeof(UILabel))
    local complete = item.transform:Find("panel/Complete")
    local expLabel = item.transform:Find("panel/Exp/Label"):GetComponent(typeof(UILabel))

    local data
    if isDaily then
        data = LuaZhouNianQing2023Mgr.m_DailyTask[row+1]
    else
        data = LuaZhouNianQing2023Mgr.m_WeekTask[row+1]
    end
    if data then
        local id = data.taskId
        local finishCount = data.finishCount
        local taskData = ZhanLing_Task.GetData(id)
        local progress = data.progress
        local target = taskData.Target

        if target > finishCount then 
            descLabel.text =  "[733400]"..taskData.Desc..SafeStringFormat3("([ff0000]%d[733400]/%d)",finishCount,target)
        else
            descLabel.text =  "[733400]"..taskData.Desc..SafeStringFormat3("(%d/%d)",finishCount,target)
        end
        complete.gameObject:SetActive(progress >= 1)
        goBtn.gameObject:SetActive(progress < 1)
        if progress < 1 or not progress then
            UIEventListener.Get(goBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
                LuaZhouNianQing2023Mgr:OpenWnd(id)
            end)
        end
        local expStr = "[281200]+"..taskData.ProgressReward
        if LuaZhouNianQing2023Mgr.m_IsVip2 then
            expStr = expStr.."[ff0000]"..SafeStringFormat3("x%.1f", ZhanLing_Setting.GetData().Vip2AddRate)
        end
        expLabel.text = expStr
    end
end

function LuaZhouNianQingPassportWnd:OnTaskSelectAtRow(row,isDaily)
    self:InitFinishTask()
end

function LuaZhouNianQingPassportWnd:InitFinishTask()
    --[[for i=0,self.m_DailyTaskTableView.m_ItemList.Count-1 do
        local p = LuaZhouNianQing2023Mgr.m_DailyTask[i+1].progress
        local item = self.m_DailyTaskTableView.m_ItemList[i]
        local pos = item.transform.localPosition
        if p >= 1 then
            pos.z = -1
        else
            pos.z = 0
        end
        item.transform.localPosition = pos
    end
    for ii=0,self.m_WeekTaskTableView.m_ItemList.Count-1 do
        local p = LuaZhouNianQing2023Mgr.m_WeekTask[ii+1].progress
        local item = self.m_WeekTaskTableView.m_ItemList[ii]
        local pos = item.transform.localPosition
        if p >= 1 then
            pos.z = -1
        else
            pos.z = 0
        end
        item.transform.localPosition = pos
    end--]]
end

function LuaZhouNianQingPassportWnd:PlayUnlockFx(idx)
    local vfx = self["m_UnlockVip"..idx.."Fx"]
    vfx:SetActive(true)
    vfx:GetComponent(typeof(Animation)):Play("zhounianqingpassportwnd_jiesuo_1")
    local rs = CommonDefs.GetComponentsInChildren_Component_Type(vfx.transform, typeof(Renderer))
    for i = 0, rs.Length - 1 do
        rs[i].sortingOrder = 1
    end
    local ps = CommonDefs.GetComponentsInChildren_Component_Type(vfx.transform, typeof(ParticleSystem))
    for i = 0, ps.Length - 1 do
        ps[i]:Clear()
        ps[i]:Play()
    end
    vfx.transform.parent:Find("Sprite1"):GetChild(0):Find("LockedSprite").gameObject:SetActive(false)
    --vfx.transform.parent:Find("Sprite2"):GetChild(0):GetChild(0):Find("LockedSprite").gameObject:SetActive(false)
end

function LuaZhouNianQingPassportWnd:SetRedDot(idx, state)
    self["m_TabBtn"..idx].transform:Find("AlertSprite").gameObject:SetActive(state)
end

function LuaZhouNianQingPassportWnd:OnRewardScroll(value)
    local vdelta = 7.5
    local impcount = 10

    local totalCnt = ZhanLing_Reward.GetDataCount()
    local index = math.ceil((totalCnt - vdelta) * value + vdelta)
    local implv = (math.floor((index - 1) / impcount) + 1) * impcount 

    local leftIndex = math.ceil((totalCnt - vdelta) * value)
    leftIndex = math.min(math.max(leftIndex, 1), totalCnt)
    self.m_LeftBorderReward = leftIndex
    index = math.min(math.max(index, 1), totalCnt)
    self.m_RightBorderReward = index
    
    self.m_LeftBackBtn:SetActive(leftIndex > 1 and self.m_CurLevel < leftIndex)
    self.m_RightBackBtn:SetActive(self.m_CurLevel > index)

    implv = math.min(math.max(implv, 1), math.floor(totalCnt / 10) * 10)
    --if implv == self.m_CurImpLv then return end
    self.m_CurImpLv = implv

    self:RefreshImpItems()
end

function LuaZhouNianQingPassportWnd:RefreshImpItems()
    self.m_ImpTitle.text = self.m_CurImpLv..LocalString.GetString("级可领")
    for i = 1, 3 do
        self:InitRewardItem(self["m_ImpItem"..i], i, self.m_CurImpLv, LuaZhouNianQing2023Mgr.m_ZhanLingData[self.m_CurImpLv].flag)
    end
end
