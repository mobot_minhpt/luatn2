require("common/common_include")
local CPopupMenuInfoMgr=import "L10.UI.CPopupMenuInfoMgr"
local PopupMenuItemData=import "L10.UI.PopupMenuItemData"
local AlignType=import "L10.UI.CPopupMenuInfoMgr+AlignType"
local LuaGameObject=import "LuaGameObject"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local Gac2Gas=import "L10.Game.Gac2Gas"
local MessageWndManager=import "L10.UI.MessageWndManager"
local CQuanMinPKMgr=import "L10.Game.CQuanMinPKMgr"
local MessageMgr=import "L10.Game.MessageMgr"
local CGameReplayMgr = import "L10.Game.CGameReplayMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

CLuaQMPKGameVideoWnd=class()
RegistClassMember(CLuaQMPKGameVideoWnd,"m_Time")
RegistClassMember(CLuaQMPKGameVideoWnd,"m_TimeLabel")
RegistClassMember(CLuaQMPKGameVideoWnd,"m_TimeTable")
RegistClassMember(CLuaQMPKGameVideoWnd,"m_TimeInitTable")

RegistClassMember(CLuaQMPKGameVideoWnd,"m_NameInput")
RegistClassMember(CLuaQMPKGameVideoWnd,"m_TableViewDataSource")
RegistClassMember(CLuaQMPKGameVideoWnd,"m_TableView")

RegistClassMember(CLuaQMPKGameVideoWnd,"m_DataList")
RegistClassMember(CLuaQMPKGameVideoWnd,"m_VideoFileName")
RegistClassMember(CLuaQMPKGameVideoWnd,"m_VideoFileUrl")
RegistClassMember(CLuaQMPKGameVideoWnd,"m_VideoSceneId")
RegistClassMember(CLuaQMPKGameVideoWnd,"m_VideoIsZip")
RegistClassMember(CLuaQMPKGameVideoWnd,"m_DeleteButton")
RegistClassMember(CLuaQMPKGameVideoWnd,"m_WatchButton")

RegistClassMember(CLuaQMPKGameVideoWnd,"m_FilterYear")

RegistClassMember(CLuaQMPKGameVideoWnd,"m_SearchText")


function CLuaQMPKGameVideoWnd:Init()
    self.m_TimeTable= {
        LocalString.GetString("64进32"),
        LocalString.GetString("32进16"),
        LocalString.GetString("16进8"),
        LocalString.GetString("8进4"),
        LocalString.GetString("决赛"),
    }
    local g = LuaGameObject.GetChildNoGC(self.transform,"CloseButton").gameObject
    UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.CloseUI(CLuaUIResources.QMPKGameVideoWnd)
    end)
    LuaGameObject.GetChildNoGC(self.transform,"GroupChangeButton").gameObject:SetActive(false)

    --为了方便不改prefab
    self.m_TimeLabel=LuaGameObject.GetChildNoGC(self.transform,"TimeLabel").label
    self.m_NameInput=LuaGameObject.GetChildNoGC(self.transform,"NameInput").input

    self.m_DeleteButton=LuaGameObject.GetChildNoGC(self.transform,"DeleteButton").gameObject
    self.m_DeleteButton:SetActive(false)
    local g = LuaGameObject.GetChildNoGC(self.transform,"DeleteButton").gameObject
    UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
        --删除文件
        local index=self.m_TableView.currentSelectRow
        if index>-1 then
            if index<#self.m_DataList then
                if self.m_DataList[index+1] then
                    local fileName=self.m_DataList[index+1].m_FileName
                    CGameReplayMgr.Instance:DeleteFile(fileName)
                    self:RefreshItem(fileName)
                end
            end
        end
    end)

    self.m_WatchButton=LuaGameObject.GetChildNoGC(self.transform,"WatchButton").gameObject
    self.m_WatchButton:SetActive(false)
    local g = LuaGameObject.GetChildNoGC(self.transform,"WatchButton").gameObject
    UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
        self:WatchVideo()
    end)

    local function SelectGroupAction(index)
        if self.m_TimeTable and #self.m_TimeTable>index then
            self.m_FilterYear = index + 1
            self:RefreshList(false)
            -- 决赛
        end
    end
    local selectGroup=DelegateFactory.Action_int(SelectGroupAction)
    local g = LuaGameObject.GetChildNoGC(self.transform,"TimeChangeButton").gameObject
    UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
        local t={}
        for i=1,#self.m_TimeTable do
            local item=PopupMenuItemData(tostring(self.m_TimeTable[i]),selectGroup,false,nil)
            table.insert(t, item)
        end
        local array=Table2Array(t,MakeArrayClass(PopupMenuItemData))
        CPopupMenuInfoMgr.ShowPopupMenu(array, go.transform, AlignType.Bottom,1,nil,600,true,296)
    end)
    local g = LuaGameObject.GetChildNoGC(self.transform,"SearchButton").gameObject
    UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
        if cs_string.IsNullOrEmpty(self.m_NameInput.value) then
            --请输入搜索内容
        else
            self.m_SearchText=self.m_NameInput.value
            self:RefreshList(true)
        end
    end)

    self.m_TableView=LuaGameObject.GetChildNoGC(self.transform,"ContentTable").tableView
    local function InitItem(item,index)
        if index % 2 == 1 then
            item:SetBackgroundTexture("common_textbg_02_dark")
        else
            item:SetBackgroundTexture("common_textbg_02_light")
        end
        self:InitItem(item,index)
    end
    self.m_TableViewDataSource=DefaultTableViewDataSource.CreateByCount(0,InitItem)
    self.m_TableView.m_DataSource=self.m_TableViewDataSource
    self.m_TableView.OnSelectAtRow=DelegateFactory.Action_int(function (row)
        self:OnSelectAtRow(row)
    end)

    --初始化列表
    self.m_FilterYear = 1
    self.m_TimeLabel.text = self.m_TimeTable[self.m_FilterYear]

    CLuaQMPKMgr.DownloadQMPKPlayList()
    local curIndex = self:GetCurGameProgressNode()
    SelectGroupAction(curIndex)
end
function CLuaQMPKGameVideoWnd:GetCurGameProgressNode()
    local progressList = {}
    local curTimeStamp = CServerTimeMgr.Inst.timeStamp
    local TaoTaiStartTime = g_LuaUtil:StrSplit(QuanMinPK_Setting.GetData().TaoTaiSai_StartPlay_Time," ")[1]
    local TaoTaiStartTimeStamp = CServerTimeMgr.Inst:GetTimeStampByStr(SafeStringFormat3("%s 00:00",TaoTaiStartTime))
    local index = 0
    QuanMinPK_Schedule.Foreach(function (key, data)
        if data.ProgressNode > 0 then
            local curStamp = CServerTimeMgr.Inst:GetTimeStampByStr(SafeStringFormat3("%s 00:00",data.Date))
            if curStamp > TaoTaiStartTimeStamp then
                if curStamp < curTimeStamp then
                    index = index + 1
                else
                    return index
                end
            end
        end
    end)
    return index
end

function CLuaQMPKGameVideoWnd:InitItem(item,index)
    local tf=item.transform
    local team1NameLabel=LuaGameObject.GetChildNoGC(tf,"NameLabel1").label
    team1NameLabel.text=" "
    local result1Sprite=LuaGameObject.GetChildNoGC(tf,"ResultSprite1").sprite
    result1Sprite.spriteName=" "
    local result2Sprite=LuaGameObject.GetChildNoGC(tf,"ResultSprite2").sprite
    result2Sprite.spriteName=" "
    local team2NameLabel=LuaGameObject.GetChildNoGC(tf,"NameLabel2").label
    team2NameLabel.text=" "

    local downloadingMark=LuaGameObject.GetChildNoGC(tf,"DownloadingMark").gameObject
    downloadingMark:SetActive(false)
    local okMark=LuaGameObject.GetChildNoGC(tf,"OkMark").gameObject
    okMark:SetActive(false)

    if index<#self.m_DataList then
        local record=self.m_DataList[index+1]
        if record then
            team1NameLabel.text=record.m_AttackZhanduiName
            team2NameLabel.text=record.m_DefendZhanduiName
            --结果
            if record.m_WinZhanduiId == record.m_AttackZhanduiId then
                result1Sprite.spriteName="common_fight_result_win"
            else
                result1Sprite.spriteName="common_fight_result_lose"
            end
            if record.m_WinZhanduiId == record.m_DefendZhanduiId then
                result2Sprite.spriteName="common_fight_result_win"
            else
                result2Sprite.spriteName="common_fight_result_lose"
            end

            local isDownloaded=CGameReplayMgr.Instance:IsFileDownloaded(record.m_FileName)
            if isDownloaded then--已经下载完毕
                okMark:SetActive(true)
                -- cannotwatchMark:SetActive(false)
                downloadingMark:SetActive(false)
            else--没有下载或者下载中
                okMark:SetActive(false)
                if CGameReplayMgr.Instance:IsFileDownloading(record.m_FileName) then
                    downloadingMark:SetActive(true)
                    -- cannotwatchMark:SetActive(false)
                else
                    downloadingMark:SetActive(false)
                    -- cannotwatchMark:SetActive(true)
                end
            end
        end
    end
end
function  CLuaQMPKGameVideoWnd:OnSelectAtRow(row )
    self.m_WatchButton:SetActive(true)
    -- body
    self.m_VideoFileName=""
    self.m_VideoFileUrl = ""
    self.m_VideoSceneId = 0
    self.m_VideoIsZip = false
    if self.m_DataList and row<#self.m_DataList then
        local record=self.m_DataList[row+1]
        if record then
            local isDownloaded=CGameReplayMgr.Instance:IsFileDownloaded(record.m_FileName)
            self.m_DeleteButton:SetActive(isDownloaded)
            self.m_VideoFileName=record.m_FileName
            self.m_VideoFileUrl = record.m_FileUrl
            self.m_VideoSceneId = record.m_WatchSceneId
            self.m_VideoIsZip = record.m_IsZip
        end
    end
    local label=LuaGameObject.GetChildNoGC(self.m_WatchButton.transform,"Label").label
    local sprite=LuaGameObject.GetLuaGameObjectNoGC(self.m_WatchButton.transform).sprite
    if label and sprite then
        if CGameReplayMgr.Instance:IsFileDownloaded(self.m_VideoFileName) then
            --进入观战
            label.effectColor=Color(0.45,0.27,0.12)
            label.text=LocalString.GetString("进入观战")
            sprite.spriteName="common_button_orange_normal"
            
        else
            --观战
            label.effectColor=Color(0.12,0.28,0.45)
            label.text=LocalString.GetString("观战")
            sprite.spriteName="common_button_darkblue_3_n"
        end
    end
end

function CLuaQMPKGameVideoWnd:RefreshList(isSearch)
    if not self.m_FilterYear then
        self.m_FilterYear=1
    end
    self.m_TimeLabel.text = self.m_TimeTable[self.m_FilterYear]

    self.m_DataList={}
    if CQuanMinPKMgr.Inst.m_VideoRecordList then
        local len=CQuanMinPKMgr.Inst.m_VideoRecordList.Count
        for i=0,len-1 do
            local record=CQuanMinPKMgr.Inst.m_VideoRecordList[i]
            if isSearch then
                if record:IsMatchSearch(self.m_SearchText) then
                    table.insert(self.m_DataList, record)
                end
            else
                -- 决赛
                if record.m_PlayStage == 5 and self.m_FilterYear == 5 then
                    table.insert(self.m_DataList, record)
                    table.sort(self.m_DataList,function(a,b)
                        return a.m_PlayIndex < b.m_PlayIndex
                    end)
                --淘汰赛
                elseif record.m_PlayStage == 4 then
                    if record.m_TaoTaiSaiStage == self.m_FilterYear then
                        table.insert(self.m_DataList, record)
                    end
                end
            end
        end
    end
    self.m_TableViewDataSource.count=#self.m_DataList
    self.m_TableView:ReloadData(true,false)
    self.m_DeleteButton:SetActive(false)
    self.m_WatchButton:SetActive(false)
    if #self.m_DataList>0 then
        self.m_TableView:SetSelectRow(0,true)
    end
end

function CLuaQMPKGameVideoWnd:WatchVideo()
    if self.m_TableView.currentSelectRow<0 then
        return
    end

    if CGameReplayMgr.Instance:IsFileDownloaded(self.m_VideoFileName) then
        Gac2Gas.RequestEnterGameVideoScene(self.m_VideoSceneId, self.m_VideoFileName)
    else
        local function onOk()
            CGameReplayMgr.Instance:DownloadFile(self.m_VideoFileName, self.m_VideoFileUrl, self.m_VideoIsZip)
            --设置状态为下载中
            self:RefreshItem(self.m_VideoFileName)
        end
        --判断是否wifi环境
        if CommonDefs.IsInWifiNetwork then
            local str=MessageMgr.Inst:FormatMessage("BWDH_WatchVideo_NeedDownload",{})
            MessageWndManager.ShowOKCancelMessage(str,DelegateFactory.Action(onOk),nil,LocalString.GetString("下载"),nil,false)
        else
            --传入大小
            local str=MessageMgr.Inst:FormatMessage("BWDH_WatchVideo_NeedDownload_NotWifi",{self:GetCurrentSize()})
            MessageWndManager.ShowOKCancelMessage(str,DelegateFactory.Action(onOk),nil,LocalString.GetString("下载"),nil,false)
        end
    end
end
function CLuaQMPKGameVideoWnd:OnEnable()
    g_ScriptEvent:AddListener("GameVideoListDownloaded", self, "OnGameVideoListDownloaded")
    g_ScriptEvent:AddListener("GameVideoDownloaded", self, "OnGameVideoDownloaded")
end

function CLuaQMPKGameVideoWnd:OnDisable()
    g_ScriptEvent:RemoveListener("GameVideoListDownloaded", self, "OnGameVideoListDownloaded")
    g_ScriptEvent:RemoveListener("GameVideoDownloaded", self, "OnGameVideoDownloaded")
end
function  CLuaQMPKGameVideoWnd:OnGameVideoDownloaded( args )
    local success=args[0]
    local fileName=args[1]

    self:RefreshItem(fileName)
end
function CLuaQMPKGameVideoWnd:RefreshItem(fileName)
    --遍历 然后设置状态
    local index=-1
    for k,v in ipairs(self.m_DataList) do
        if v.m_FileName==fileName then
            index=k-1
        end
    end
    if index>-1 then
        self:InitItem(self.m_TableView:GetItemAtRow(index),index)
        --选中的item可能刷新了
        if index==self.m_TableView.currentSelectRow then
            self:OnSelectAtRow(self.m_TableView.currentSelectRow)
        end
    end
end
function CLuaQMPKGameVideoWnd:GetCurrentSize()
    if self.m_TableView.currentSelectRow>-1 then
        local data=self.m_DataList[self.m_TableView.currentSelectRow+1]
        if data then
            return SafeStringFormat3("%.2f",data.m_FileSize/1000000)
        end
    end
    return 0
end

function  CLuaQMPKGameVideoWnd:OnGameVideoListDownloaded( args )
    local success=args[0]
    local playName=args[1]
    if success then
        if playName=="qmpk" then
            self:RefreshList(false)
        end
    end
end
return CLuaQMPKGameVideoWnd
