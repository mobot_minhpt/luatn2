local Screen=import "UnityEngine.Screen"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local Object = import "System.Object"

--戳泡泡玩法

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"

LuaZhuErDanChuoPaoPaoWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaZhuErDanChuoPaoPaoWnd, "PaoPaoTemplate", "PaoPaoTemplate", GameObject)
RegistChildComponent(LuaZhuErDanChuoPaoPaoWnd, "CountDownLabel", "CountDownLabel", UILabel)
RegistChildComponent(LuaZhuErDanChuoPaoPaoWnd, "ScoreLabel", "ScoreLabel", UILabel)
RegistChildComponent(LuaZhuErDanChuoPaoPaoWnd, "AllScoreLabel", "AllScoreLabel", UILabel)
RegistChildComponent(LuaZhuErDanChuoPaoPaoWnd, "Success", "Success", GameObject)
RegistChildComponent(LuaZhuErDanChuoPaoPaoWnd, "Fail", "Fail", GameObject)
RegistChildComponent(LuaZhuErDanChuoPaoPaoWnd, "TipLabel", "TipLabel", UILabel)
RegistChildComponent(LuaZhuErDanChuoPaoPaoWnd, "ToyRoot", "ToyRoot", GameObject)
RegistChildComponent(LuaZhuErDanChuoPaoPaoWnd, "ResultFx", "ResultFx", CUIFx)
RegistChildComponent(LuaZhuErDanChuoPaoPaoWnd, "Progress", "Progress", UISlider)

--@endregion RegistChildComponent end

RegistClassMember(LuaZhuErDanChuoPaoPaoWnd, "m_CountDownTick")
RegistClassMember(LuaZhuErDanChuoPaoPaoWnd, "m_TimeCounter")
RegistClassMember(LuaZhuErDanChuoPaoPaoWnd, "m_EmitIntervalTime")
RegistClassMember(LuaZhuErDanChuoPaoPaoWnd, "m_EmitTimestamp")

RegistClassMember(LuaZhuErDanChuoPaoPaoWnd, "m_TotalTime")
RegistClassMember(LuaZhuErDanChuoPaoPaoWnd, "m_PassScore")
RegistClassMember(LuaZhuErDanChuoPaoPaoWnd, "m_GameScore")
RegistClassMember(LuaZhuErDanChuoPaoPaoWnd, "m_StartTime")
RegistClassMember(LuaZhuErDanChuoPaoPaoWnd, "m_IsFinished")

function LuaZhuErDanChuoPaoPaoWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
	self.PaoPaoTemplate:SetActive(false)
	self.m_StartTime=0
	self.TipLabel.text = g_MessageMgr:FormatMessage("ZhuErDan_ChuoPaoPao_Tip")
end

function LuaZhuErDanChuoPaoPaoWnd:Init()
	self.m_EmitIntervalTime = 0.5
	self.m_EmitTimestamp = 0
	self.m_GameScore = 0

	local setting = ZhuJueJuQing_ZhuErDanSetting.GetData()
	self.m_TotalTime = setting.FallingToyTotalTime
	self.m_PassScore = setting.FallingToyPassScore
	self.AllScoreLabel.text = "/"..tostring(self.m_PassScore)

	self.Progress.value = 1
	self.m_StartTime=Time.time
	self.m_IsFinished = false
	-- self.m_PassScore = 100
	self:BeginGame()
end

--@region UIEvent

--@endregion UIEvent

function LuaZhuErDanChuoPaoPaoWnd:Update()
	if self.m_IsFinished then return end

	if self.m_StartTime>0 then
		local v = 1-(Time.time-self.m_StartTime)/self.m_TotalTime
		if v>=0 then
			self.Progress.value = v
		end
	end
	
	self.m_EmitTimestamp = self.m_EmitTimestamp + Time.deltaTime
	if self.m_EmitTimestamp > self.m_EmitIntervalTime then
		self.m_EmitTimestamp = 0
		self:EmitToy()
	end
end

function LuaZhuErDanChuoPaoPaoWnd:OnEnable()
	g_ScriptEvent:AddListener("ZhuErDan_ChuoPaoPao_AddScore",self,"OnAddScore")
end
function LuaZhuErDanChuoPaoPaoWnd:OnDisable()
	g_ScriptEvent:RemoveListener("ZhuErDan_ChuoPaoPao_AddScore",self,"OnAddScore")
end
function LuaZhuErDanChuoPaoPaoWnd:OnAddScore(score)
	self.m_GameScore = self.m_GameScore+score
	self.ScoreLabel.text = tostring(self.m_GameScore)
	if self.m_GameScore >= self.m_PassScore then
		self:EndGame()
	end
end

function LuaZhuErDanChuoPaoPaoWnd:DestroyCountDownTick()
	self.m_TimeCounter = 0
	if self.m_CountDownTick then
		UnRegisterTick(self.m_CountDownTick)
		self.m_CountDownTick = nil
	end
end
function LuaZhuErDanChuoPaoPaoWnd:BeginGame()
	self.m_GameScore = 0
	self.m_TimeCounter = 0

	-- 倒计时
	self:DestroyCountDownTick()
	self.m_CountDownTick = RegisterTick(function ()
		self.m_TimeCounter = self.m_TimeCounter + 1
		local leftTime = self.m_TotalTime - self.m_TimeCounter
		if leftTime < 0 then
			self:EndGame()
			self:DestroyCountDownTick()
		else
			self.CountDownLabel.text = SafeStringFormat3(LocalString.GetString("倒计时30秒"),leftTime)
		end
	end, 1000)

	self.Success:SetActive(false)
	self.Fail:SetActive(false)
end

function LuaZhuErDanChuoPaoPaoWnd:EmitToy()
	local pos = Vector3.zero
	pos = CUIManager.UIMainCamera:ScreenToWorldPoint(Vector3(math.random()*(Screen.width-200)+100, Screen.height, 0))
	local template, score = self.PaoPaoTemplate,1
	local toy = GameObject.Instantiate(template, pos, self.ToyRoot.transform.rotation)
	if toy then
		if toy.transform:GetComponent(typeof(CCommonLuaScript)) then
			toy.transform:GetComponent(typeof(CCommonLuaScript)):Init({score})
		end
		toy.transform.parent = self.ToyRoot.transform
		toy.transform.localScale = Vector3.one
		toy:SetActive(true)
	end
end


function LuaZhuErDanChuoPaoPaoWnd:EndGame()
	self.m_IsFinished = true
	if self.m_GameScore >= self.m_PassScore then
		-- self.Success:SetActive(true)
		self.ResultFx:LoadFx("Fx/UI/Prefab/UI_suiping_02.prefab")

		local empty = CreateFromClass(MakeGenericClass(List, Object))
		empty = MsgPackImpl.pack(empty)
		Gac2Gas.FinishClientTaskEvent(22120624,"ZhuErDan_ChuoPaoPao",empty)
	else
		self.Fail:SetActive(true)
		self.ResultFx:LoadFx("Fx/UI/Prefab/UI_pengpengche_shibai.prefab")
	end
	RegisterTickOnce(function ()
		CUIManager.CloseUI(CLuaUIResources.ZhuErDanChuoPaoPaoWnd)
	end, 5000)
end

function LuaZhuErDanChuoPaoPaoWnd:OnDestroy()
    self:DestroyCountDownTick()
end