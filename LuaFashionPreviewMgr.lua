local CFashionPreviewMgr = import "L10.UI.CFashionPreviewMgr"
local CUIManager = import "L10.UI.CUIManager"
local EnumShopMallFashionPreviewType = import "L10.UI.EnumShopMallFashionPreviewType"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Camera = import "UnityEngine.Camera"
local ShaderEx = import "ShaderEx"
local Main = import "L10.Engine.Main"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CFashionMgr = import "L10.Game.CFashionMgr"
local CExpressionAppearanceInfo = import "L10.Game.CExpressionAppearanceInfo"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CZuoQiMgr = import "L10.Game.CZuoQiMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local Constants = import "L10.Game.Constants"
local CRepertoryMgr = import "L10.Game.CRepertoryMgr"
local EnumGender = import "L10.Game.EnumGender"
local CExpressionActionMgr = import "L10.Game.CExpressionActionMgr"

LuaShopMallFashionPreviewMgr = {}
LuaShopMallFashionPreviewMgr.EnumLingYuMallSubCategory ={
    ZuoQi = 1,
    ShiZhuang = 2,
    Umbrella = 3,
    BackPendant = 10
}
LuaShopMallFashionPreviewMgr.selectableSequence = nil
LuaShopMallFashionPreviewMgr.hasEntiretyHeadId = nil
LuaShopMallFashionPreviewMgr.KaiSanAppearanceGroup = 1
LuaShopMallFashionPreviewMgr.recentlyChooseFashion = nil
LuaShopMallFashionPreviewMgr.default_HideBodyFashionEffect = 0
LuaShopMallFashionPreviewMgr.default_HideHeadFashionEffect = 0
LuaShopMallFashionPreviewMgr.default_BodyFashionID = nil
LuaShopMallFashionPreviewMgr.enableMultiLight = true
LuaShopMallFashionPreviewMgr.isTransformed = false
LuaShopMallFashionPreviewMgr.blindBoxItemId = 0
LuaShopMallFashionPreviewMgr.blindBoxSelectIndex = 0
LuaShopMallFashionPreviewMgr.fashionBlindBoxSelectIndex = 0
LuaShopMallFashionPreviewMgr.zuoqiBlindBoxSelectIndex = 0
LuaShopMallFashionPreviewMgr.headfashionBlindBoxItemId = 0
LuaShopMallFashionPreviewMgr.bodyfashionBlindBoxItemId = 0
LuaShopMallFashionPreviewMgr.zuoqiBlindBoxItemId = 0

LuaShopMallFashionPreviewMgr.backpendantFashionId = 0

if Main.Inst and Main.Inst.EngineVersion < ShaderEx.s_MinVersion and Main.Inst.EngineVersion ~= - 1 then
    LuaShopMallFashionPreviewMgr.enableMultiLight = false
end

function LuaShopMallFashionPreviewMgr:ShowTryToBuyWnd(selectableSequence, ownItem2FashionMap)
    self.selectableSequence = selectableSequence
    CUIManager.ShowUI(CLuaUIResources.FashionPreviewTryToBuyWnd)
end

function LuaShopMallFashionPreviewMgr:OnClose()
    self:OnTakeOff()

    CFashionPreviewMgr.curShopMallPreviewType = EnumShopMallFashionPreviewType.None
    self.hasEntiretyHeadId = nil
    self.selectableSequence = nil
    self.ownItem2FashionMap = nil
    self.default_BodyFashionID = nil
    self.recentlyChooseFashion = nil
    self.blindBoxItemId = 0
    self.headfashionBlindBoxItemId = 0
    self.bodyfashionBlindBoxItemId = 0
    self.zuoqiBlindBoxItemId = 0
    self.blindBoxSelectIndex = 0
    self.zuoqiBlindBoxSelectIndex = 0
    self.fashionBlindBoxSelectIndex = 0
    self.m_FashionItemId2PreviewChestId = {}
end

function LuaShopMallFashionPreviewMgr:GetCurCategory()
    if CFashionPreviewMgr.HeadFashionID ~= 0 or CFashionPreviewMgr.BodyFashionID ~= 0 then
        return LuaShopMallFashionPreviewMgr.EnumLingYuMallSubCategory.ShiZhuang
    elseif CFashionPreviewMgr.UmbrellaFashionID ~= 0 then
        return LuaShopMallFashionPreviewMgr.EnumLingYuMallSubCategory.Umbrella
    elseif CFashionPreviewMgr.ZuoQiFashionID ~= 0 then
        return LuaShopMallFashionPreviewMgr.EnumLingYuMallSubCategory.ZuoQi
    elseif self.backpendantFashionId ~= 0 then
        return LuaShopMallFashionPreviewMgr.EnumLingYuMallSubCategory.BackPendant
    end
    return LuaShopMallFashionPreviewMgr.EnumLingYuMallSubCategory.ShiZhuang
end

function LuaShopMallFashionPreviewMgr:SetBodyFashion(fashionId,blindBoxId)
    blindBoxId = blindBoxId and blindBoxId or 0
    local isSelected = (CFashionPreviewMgr.BodyFashionID ~= fashionId) or (blindBoxId ~= 0) or (self.m_PreviewChestId ~= 0)
    local lastfashion = Fashion_Fashion.GetData(CFashionPreviewMgr.BodyFashionID)
    CFashionPreviewMgr.BodyFashionID = isSelected and fashionId or 0
    self:UpdateRecentlyChooseFashion(isSelected, false)

    if self.hasEntiretyHeadId then
        self.hasEntiretyHeadId = nil
        CFashionPreviewMgr.HeadFashionID = 0
        return
    end
    if self.m_PreviewChestId ~= 0 then
        CFashionPreviewMgr.HeadFashionID = 0
    end
    local fashion = Fashion_Fashion.GetData(fashionId)
    if fashion then
        local entiretyHeadFashion = Fashion_Fashion.GetData(fashion.EntiretyHeadId)
        if entiretyHeadFashion then
            self.hasEntiretyHeadId = true
            CFashionPreviewMgr.HeadFashionID = fashion.EntiretyHeadId
        end
    end
    if self.headfashionBlindBoxItemId == self.bodyfashionBlindBoxItemId then
        self.headfashionBlindBoxItemId = 0
    end
    self.bodyfashionBlindBoxItemId = blindBoxId
    self.blindBoxItemId = blindBoxId
    self.m_PreviewChestId = 0
end

function LuaShopMallFashionPreviewMgr:UpdateRecentlyChooseFashion(isSelected, isHead)
    local bodyFashion = Fashion_Fashion.GetData(CFashionPreviewMgr.BodyFashionID)
    local headFashion = Fashion_Fashion.GetData(CFashionPreviewMgr.HeadFashionID)
    local curSelectFashion = isHead and headFashion or bodyFashion
    local otherFashion = isHead and bodyFashion or headFashion
    if isSelected then
        if curSelectFashion then
            self.recentlyChooseFashion = curSelectFashion
        end
    else
        if otherFashion then
            self.recentlyChooseFashion = otherFashion
        else
            self.recentlyChooseFashion = nil
        end
    end
end

function LuaShopMallFashionPreviewMgr:SetHeadFashion(fashionId,blindBoxId)
    blindBoxId = blindBoxId and blindBoxId or 0
    local isSelected = (CFashionPreviewMgr.HeadFashionID ~= fashionId) or (blindBoxId ~= 0) or (self.m_PreviewChestId ~= 0)
    local lastfashion = Fashion_Fashion.GetData(CFashionPreviewMgr.HeadFashionID)
    CFashionPreviewMgr.HeadFashionID = isSelected and fashionId or 0
    self:UpdateRecentlyChooseFashion(isSelected, true)
    if self.m_PreviewChestId ~= 0 then
        CFashionPreviewMgr.BodyFashionID = 0
    end
    if self.hasEntiretyHeadId then
        self.hasEntiretyHeadId = nil
        CFashionPreviewMgr.BodyFashionID = 0
    end
    if self.headfashionBlindBoxItemId == self.bodyfashionBlindBoxItemId then
        self.bodyfashionBlindBoxItemId = 0
    end
    self.headfashionBlindBoxItemId = blindBoxId
    self.blindBoxItemId = blindBoxId
    self.m_PreviewChestId = 0
end

function LuaShopMallFashionPreviewMgr:IsTransformable()
    local bodyFashion = Fashion_Fashion.GetData(CFashionPreviewMgr.BodyFashionID)
    local headFashion = Fashion_Fashion.GetData(CFashionPreviewMgr.HeadFashionID)

    if bodyFashion and headFashion and bodyFashion.Appearance == headFashion.Appearance then
        local bodyFashionExpressionId = CClientMainPlayer.Inst.Gender == EnumGender.Male and bodyFashion.TransformMonsterMale or bodyFashion.TransformMonsterFemale
        local headFashionExpressionId = CClientMainPlayer.Inst.Gender == EnumGender.Male and headFashion.TransformMonsterMale or headFashion.TransformMonsterFemale
        if bodyFashionExpressionId ~= 0 and headFashionExpressionId ~= 0 then
            return bodyFashionExpressionId == headFashionExpressionId
        end
    end

    return false
end

function LuaShopMallFashionPreviewMgr:SetZuoQiFashion(vehicleId,blindBoxId)
    blindBoxId = blindBoxId and blindBoxId or 0
    local isSelected = CFashionPreviewMgr.ZuoQiFashionID ~= vehicleId
    CFashionPreviewMgr.ZuoQiFashionID = isSelected and vehicleId or 0
    self.isNeedResetModelScale = true
    self.zuoqiBlindBoxItemId = blindBoxId
    self.blindBoxItemId = blindBoxId
end

function LuaShopMallFashionPreviewMgr:SetBackPendantFashion(bpid,blindBoxId)
    blindBoxId = blindBoxId and blindBoxId or 0
    local isSelected = (self.backpendantFashionId ~= bpid) or (blindBoxId ~= 0)
    self.backpendantFashionId = isSelected and bpid or 0
    self.isNeedResetModelScale = true
    self.blindBoxItemId = blindBoxId
end

function LuaShopMallFashionPreviewMgr:SetUmbrellaFashion(expression_Appearance_PreviewDefine)
    local isSelected = expression_Appearance_PreviewDefine ~= CFashionPreviewMgr.UmbrellaFashionID
    CFashionPreviewMgr.UmbrellaFashionID = isSelected and expression_Appearance_PreviewDefine or 0
end

function LuaShopMallFashionPreviewMgr:ShowFashionPreview(pos2fashionIdMap,blindBoxItemId)
    self:CheckSuits()
    for pos, fashionId in pairs(pos2fashionIdMap) do
        local fashion = Fashion_Fashion.GetData(fashionId)
        if fashion then
            local sexualityIsFit = true
            if fashion.GenderLimit > 0 then
                if fashion.GenderLimit == 1 and CClientMainPlayer.Inst.AppearanceProp.Gender ~= EnumGender.Male then
                    sexualityIsFit = false
                elseif fashion.GenderLimit == 2 and CClientMainPlayer.Inst.AppearanceProp.Gender ~= EnumGender.Female then
                    sexualityIsFit = false
                end
            end
            if not sexualityIsFit then
                g_MessageMgr:ShowMessage("Fashion_Gender_Limit_Cannot_TakeOn")
                return
            end
            if fashion.Position == 0 then
                self:SetHeadFashion(fashionId,blindBoxItemId)
            elseif fashion.Position == 1 then
                self:SetBodyFashion(fashionId,blindBoxItemId)  
            elseif fashion.Position == 3 then
                self:SetBackPendantFashion(fashionId,blindBoxItemId)
            end     
        end
    end
    self:OpenShopMallFashionPreviewWnd()
end


LuaShopMallFashionPreviewMgr.m_FashionItemId2PreviewChestId = {}
LuaShopMallFashionPreviewMgr.m_PreviewChestId = 0
function LuaShopMallFashionPreviewMgr:SetPreviewChest(previewChestData)
    if not previewChestData then return end
    self.m_PreviewChestId = previewChestData.ID
    self.hasEntiretyHeadId = nil
    CFashionPreviewMgr.HeadFashionID = 0
    CFashionPreviewMgr.BodyFashionID = 0
    self.headfashionBlindBoxItemId = 0
    self.bodyfashionBlindBoxItemId = 0
    self.blindBoxItemId = 0
    for i = 0, previewChestData.List.Length - 1 do
        local itemId = previewChestData.List[i]
        local fashion = Fashion_Fashion.GetDataBySubKey("ItemID",itemId)
        if fashion then
            local sexualityIsFit = true
            if fashion.GenderLimit > 0 then
                if fashion.GenderLimit == 1 and CClientMainPlayer.Inst.AppearanceProp.Gender ~= EnumGender.Male then
                    sexualityIsFit = false
                elseif fashion.GenderLimit == 2 and CClientMainPlayer.Inst.AppearanceProp.Gender ~= EnumGender.Female then
                    sexualityIsFit = false
                end
            end
            if not sexualityIsFit then
                g_MessageMgr:ShowMessage("Fashion_Gender_Limit_Cannot_TakeOn")
                return
            end
            self.m_FashionItemId2PreviewChestId[itemId] = previewChestData.ID
            if fashion.Position == 0 then
                CFashionPreviewMgr.HeadFashionID = fashion.ID
            elseif fashion.Position == 1 then
                CFashionPreviewMgr.BodyFashionID = fashion.ID
            end
        end   
    end
    CFashionPreviewMgr.curShopMallPreviewType = EnumShopMallFashionPreviewType.PreviewAllFashion
end

function LuaShopMallFashionPreviewMgr:ShowPreviewChest(previewChestData)
    self:SetPreviewChest(previewChestData)
    self:OpenShopMallFashionPreviewWnd()
end

function LuaShopMallFashionPreviewMgr:ShowZuiQiPreview(id,blindBoxItemId)
    self:CheckSuits()
    local ZuoQi =ZuoQi_ZuoQi.GetData(id)
    if ZuoQi then
        self:SetZuoQiFashion(id,blindBoxItemId)
        self:OpenShopMallFashionPreviewWnd()
    end
end

function LuaShopMallFashionPreviewMgr:ShowUmbrellaPreview(expression_Appearance_PreviewDefine)
    self:CheckSuits()
    if expression_Appearance_PreviewDefine then
        self:SetUmbrellaFashion(expression_Appearance_PreviewDefine)
        self:OpenShopMallFashionPreviewWnd()
    end
end

function LuaShopMallFashionPreviewMgr:ShowBlindBox(id,blindBoxData)
    local vehicleId, pos2fashionIdMap,itemIds = self:RandomGetBlindBoxResult(id,blindBoxData)
    if vehicleId ~= 0 then
        self:ShowZuiQiPreview(vehicleId,blindBoxData and blindBoxData.ID or 0)
    else
        self:ShowFashionPreview(pos2fashionIdMap, blindBoxData and blindBoxData.ID or 0)
    end
end

function LuaShopMallFashionPreviewMgr:RandomGetBlindBoxResult(id,blindBoxData)
    self.blindBoxItemId = id
    local lingYuMalldata = Mall_LingYuMall.GetData(id)
    local vehicleId, pos2fashionIdMap, itemIds = 0, {}, Table2Array({}, MakeArrayClass(UInt32))
    if not lingYuMalldata then
        lingYuMalldata = Mall_LingYuMallLimit.GetData(id)
    end
    if lingYuMalldata and blindBoxData and blindBoxData.List and blindBoxData.List.Length > 0 then
        vehicleId, pos2fashionIdMap,itemIds = self:GetBlindBoxResult(math.random(0,blindBoxData.List.Length - 1),blindBoxData,lingYuMalldata)
    end
    return vehicleId, pos2fashionIdMap,itemIds
end

function LuaShopMallFashionPreviewMgr:GetBlindBoxResult(index,blindBoxData,lingYuMalldata)
    local vehicleId, pos2fashionIdMap = 0,{}
    self.blindBoxSelectIndex = index
    local itemIds = blindBoxData.List[index]
    for i = 0, itemIds.Length - 1 do
        local itemId = itemIds[i]
        if lingYuMalldata.SubCategory == self.EnumLingYuMallSubCategory.ZuoQi then
            local data = ZuoQi_ZuoQi.GetDataBySubKey("ItemID",itemId)
            if data then
                vehicleId = data.ID
                self.zuoqiBlindBoxSelectIndex = index
            end
        elseif lingYuMalldata.SubCategory == self.EnumLingYuMallSubCategory.ShiZhuang then
            local data = self:GetBlindBoxFashionData(itemId)
            if data then
                pos2fashionIdMap[data.Position] = data.ID
                self.fashionBlindBoxSelectIndex = index
            end
        end
    end
    return vehicleId, pos2fashionIdMap, itemIds
end

function LuaShopMallFashionPreviewMgr:GetBlindBoxFashionData(itemId)
    local data = Fashion_Fashion.GetDataBySubKey("ItemID",itemId)
    if not data then
        Fashion_Fashion.Foreach(function(k,v)
            if v.AllOtherItemID then
                for i=0,v.AllOtherItemID.Length-1 do
                    local id = v.AllOtherItemID[i]
                    if id == itemId then
                        data = Fashion_Fashion.GetData(k)
                        itemId = v.ItemID
                        break
                    end
                end
            end
        end)
    end
    return data,itemId
end

function LuaShopMallFashionPreviewMgr:ShowBlindBoxPreviewWnd(itemId)
    self.blindBoxItemId = itemId
    CUIManager.ShowUI(CLuaUIResources.BlindBoxPreviewWnd)
end

function LuaShopMallFashionPreviewMgr:OpenShopMallFashionPreviewWnd()
    -- 防止出现可能存在的 多光源冲突的问题
    if CUIManager.IsLoaded(CLuaUIResources.Shuangshiyi2020FashionPreviewWnd) then
		CUIManager.CloseUI(CLuaUIResources.Shuangshiyi2020FashionPreviewWnd)
    end

    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer and mainPlayer.AppearanceProp then
        self.default_HideBodyFashionEffect = mainPlayer.AppearanceProp.HideBodyFashionEffect
        self.default_HideHeadFashionEffect = mainPlayer.AppearanceProp.HideHeadFashionEffect
        CFashionPreviewMgr.shopMallPreview_HideHeadFashionEffect = self.default_HideHeadFashionEffect
        CFashionPreviewMgr.shopMallPreview_HideBodyFashionEffect = self.default_HideBodyFashionEffect
    end

    self.KaiSanAppearanceGroup = CommonDefs.Convert_ToUInt32(Fashion_Setting.GetData("FashionPreview_UmbrellaGroup").Value)
    CFashionPreviewMgr.curShopMallPreviewType = EnumShopMallFashionPreviewType.PreviewAllFashion
    CFashionPreviewMgr.curPreviewType = nil
    CUIManager.ShowUI(CUIResources.ShopMallFashionPreviewWnd)
end

function LuaShopMallFashionPreviewMgr:InitForLuozhuangSetting()
    if CFashionPreviewMgr.BodyFashionID ~= 0 then
        CFashionPreviewMgr.shopMallPreview_HideBodyFashionEffect = 0
    else
        CFashionPreviewMgr.shopMallPreview_HideBodyFashionEffect = LuaShopMallFashionPreviewMgr.default_HideBodyFashionEffect
    end
    if CFashionPreviewMgr.HeadFashionID ~= 0 then
        CFashionPreviewMgr.shopMallPreview_HideHeadFashionEffect = 0
    else
        CFashionPreviewMgr.shopMallPreview_HideHeadFashionEffect = LuaShopMallFashionPreviewMgr.default_HideHeadFashionEffect
    end

    if self.default_BodyFashionID then
        if CFashionPreviewMgr.BodyFashionID ~= 0 and CFashionPreviewMgr.HeadFashionID == 0 then
            CFashionPreviewMgr.shopMallPreview_HideHeadFashionEffect = 1
        elseif CFashionPreviewMgr.BodyFashionID == 0 and CFashionPreviewMgr.HeadFashionID ~= 0 then
            CFashionPreviewMgr.shopMallPreview_HideBodyFashionEffect = 1
        end
    end
end

--断红尘处理
function LuaShopMallFashionPreviewMgr:CheckSuits()
    local mainPlayer = CClientMainPlayer.Inst

    if mainPlayer and mainPlayer.AppearanceProp then
        local bodyId = CClientMainPlayer.Inst.AppearanceProp.BodyFashionId
        local body = Fashion_Fashion.GetData(bodyId)
        if body then
            local entiretyHeadFashion = Fashion_Fashion.GetData(body.EntiretyHeadId)
            if entiretyHeadFashion then
                self.default_BodyFashionID = bodyId
            end
        end
    end
end

function LuaShopMallFashionPreviewMgr:OnTakeOff()
    CFashionPreviewMgr.HeadFashionID = 0
    CFashionPreviewMgr.BodyFashionID = 0
    CFashionPreviewMgr.ZuoQiFashionID = 0
    CFashionPreviewMgr.UmbrellaFashionID = 0
    self.backpendantFashionId = 0
    self.m_PreviewChestId = 0
    self.headfashionBlindBoxItemId = 0
    self.bodyfashionBlindBoxItemId = 0
    self.zuoqiBlindBoxItemId = 0
    self.blindBoxItemId = 0
end

LuaShopMallFashionPreviewMgr.m_OwnedShopItems = {}
LuaShopMallFashionPreviewMgr.m_OwnedLockGroupItemIds = {} --已拥有的解锁表情的道具id提示玩家已拥有
function LuaShopMallFashionPreviewMgr:GetOwnedShopItemsInfo()
    self.m_OwnedShopItems = {}
    if not CClientMainPlayer.Inst then return end

    self:GetUsedZuoQiOrFashionItems()
    self:GetOwnerSanItems()
    self:GetBagOrWarehouseItems()
    self:GetOwnExpressions()
    self:GetOwnLvJings()

    return self.m_OwnedShopItems
end

function LuaShopMallFashionPreviewMgr:GetOwnExpressions()
    CExpressionActionMgr.Inst:InitData()
    CommonDefs.EnumerableIterate(CExpressionActionMgr.Inst.ThumbnailInfos.Values, DelegateFactory.Action_object(function (v)
        if v:GetDisplay() and not v:GetIsLock() then
            local id = v:GetID()
            local data = Expression_Show.GetData(id)
            if data then
                local lockGroupdata = Expression_LockGroup.GetData(data.LockGroup)
                if lockGroupdata then
                    self.m_OwnedShopItems[lockGroupdata.ItemID] = true
                    self.m_OwnedLockGroupItemIds[lockGroupdata.ItemID] = true
                end
            end
        end
    end))
end

function LuaShopMallFashionPreviewMgr:GetOwnLvJings()
	local lockNum = 0
    if CommonDefs.DictContains(CClientMainPlayer.Inst.PlayProp.PersistPlayData, typeof(UInt32), EnumPersistPlayDataKey_lua.eUnLockPaiZhaoLvJing) then
		lockNum = tonumber(CClientMainPlayer.Inst.PlayProp.PersistPlayData[EnumPersistPlayDataKey_lua.eUnLockPaiZhaoLvJing].StringData)
	end
	local unlockTable = {}
	if lockNum then
		while lockNum > 0 do
			local r = lockNum % 2
			lockNum = math.floor(lockNum/2)
			table.insert(unlockTable,r)
		end
	end

    PaiZhao_LvJing.Foreach(function (key,data)
        local lockGroup = data.LockGroup
        if unlockTable[lockGroup] and unlockTable[lockGroup] == 1 and data.Locked ~= 0 then
            self.m_OwnedShopItems[data.Locked] = true
        end
    end)
end

function LuaShopMallFashionPreviewMgr:GetOwnerSanItems()
    --if not CClientMainPlayer.Inst then return end
    local appearanceData = CClientMainPlayer.Inst.PlayProp.ExpressionAppearanceData
    local appearances = CreateFromClass(MakeGenericClass(Dictionary, UInt32, CExpressionAppearanceInfo))
    local appearanceGroup = self.KaiSanAppearanceGroup
    if CommonDefs.DictContains(appearanceData, typeof(Byte), appearanceGroup) then
        appearances = CommonDefs.DictGetValue(appearanceData, typeof(Byte), appearanceGroup).Appearances
    end

    --换伞页面已解锁（未到期）的伞面
    CommonDefs.EnumerableIterate(appearances.Keys, DelegateFactory.Action_object(function (___value)
        local key = ___value
        local info = CommonDefs.DictGetValue(appearances, typeof(UInt32), key)
        if info.IsExpired ~= 1 then
            local data = Expression_Appearance.GetData(key)
            if data then
                self.m_OwnedShopItems[data.ItemId] = true
            end
        end
    end))
end

function LuaShopMallFashionPreviewMgr:GetUsedZuoQiOrFashionItems()
    --if not CClientMainPlayer.Inst then return end
    --衣橱中已使用（未到期）时装
    CommonDefs.DictIterate(CClientMainPlayer.Inst.WardrobeProp.FashionMainInfos, DelegateFactory.Action_object_object(function (key, info)
        local fashion = Fashion_Fashion.GetData(key)
        if fashion==nil then return end --拓本在另外的表里面
        if fashion.IsNewWardrobe==1 and LuaAppearancePreviewMgr:IsMainPlayerFashionAvailable(key) then
            self.m_OwnedShopItems[fashion.ItemID] = true
        end
    end))
    --坐骑页面已使用（未到期）坐骑
    CommonDefs.ListIterate(CZuoQiMgr.Inst.zuoqis, DelegateFactory.Action_object(function (___value)
        local zuoqi = ___value
        local zqdata = ZuoQi_ZuoQi.GetData(zuoqi.templateId)
        if zqdata ~= nil then
            if zuoqi.expTime > CServerTimeMgr.Inst.timeStamp then
                self.m_OwnedShopItems[zqdata.ItemID] = true
            end
        end
    end))
end

function LuaShopMallFashionPreviewMgr:GetBagOrWarehouseItemIds()
    local tempOwnerItemIds = {}
    --包裹
    local bagSize = CClientMainPlayer.Inst.ItemProp:GetPlaceSize(EnumItemPlace.Bag)
    for i = 1, bagSize, 1 do
        local id = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Bag, i)
        table.insert(tempOwnerItemIds, id)
    end

    --仓库行囊
    local availableNum = math.min(CClientMainPlayer.Inst.ItemProp.RepertoryAlreadyOpenNum, Constants.TotalWarehouseNum)
    local maxpos_id = (availableNum + CLuaWarehouseView.m_XingNangNum) * Constants.SingeWarehouseSize
    for pos_id = 1, maxpos_id do
        local id = CClientMainPlayer.Inst.RepertoryProp:GetItemAt(pos_id)
        table.insert(tempOwnerItemIds, id)
    end

    return tempOwnerItemIds
end

function LuaShopMallFashionPreviewMgr:GetBagOrWarehouseItems()
    local tempOwnerItemIds = self:GetBagOrWarehouseItemIds()
    local kaisanSet = {}
    local lockGroupItemIds = {}
    local lvJingSet ={}
    Expression_Appearance.Foreach(function (key,data)
        if data.Group == LuaShopMallFashionPreviewMgr.KaiSanAppearanceGroup then
            if data.ItemId ~= 0 then
                kaisanSet[data.ItemId] = true
            end
        end
    end)
    Expression_LockGroup.Foreach(function (key,data)
        lockGroupItemIds[data.ItemID] = true
    end)
    PaiZhao_LvJing.Foreach(function (key,data)
        if data.Locked ~= 0 then
            lvJingSet[data.Locked] = true
        end
    end)

    for _,id in pairs(tempOwnerItemIds) do
        if (not System.String.IsNullOrEmpty(id)) then
            local item = CItemMgr.Inst:GetById(id)
            if (item and item.IsItem  and not item:IsItemExpire()) then      
                local templateId = item.TemplateId
                if kaisanSet[templateId] or lvJingSet[templateId] or CZuoQiMgr.Inst:GetZuoqiIDByItemTemplateId(templateId) ~= 0 or CFashionMgr.Inst:GetFashionByItem(templateId) ~= 0 then
                    self.m_OwnedShopItems[templateId] = true
                end
                if lockGroupItemIds[templateId] then
                    self.m_OwnedShopItems[templateId] = true
                    self.m_OwnedLockGroupItemIds[templateId] = true
                end
                local blindBoxData = Item_BlindBox.GetData(templateId)
                if blindBoxData and blindBoxData.List.Length > 0 then
                    self.m_OwnedShopItems[templateId] = true
                    for i = 0, blindBoxData.List.Length - 1 do
                        for j = 0, blindBoxData.List[i].Length - 1 do
                            local templateId = blindBoxData.List[i][j]
                            self.m_OwnedShopItems[templateId] = true
                        end
                    end
                end
            end
        end
    end
end

function LuaShopMallFashionPreviewMgr:RequestSyncRepertoryForMallPreview()
    if not CRepertoryMgr.Inst.HasCacheItems then
        Gac2Gas.RequestSyncRepertoryForMallPreview()
    end
end

function LuaShopMallFashionPreviewMgr:BuyMallItem(template, type, count)
    local owned = self.m_OwnedShopItems[template.ItemId]
    if System.String.IsNullOrEmpty(template.ConfirmMessage) then
        if owned then
            MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("LinyuShopWnd_Buy_Owned_MallItem_Confirm"), DelegateFactory.Action(function ()
                Gac2Gas.BuyMallItem(type, template.ItemId, count)
            end), nil, nil, nil, false)
        else
            Gac2Gas.BuyMallItem(type, template.ItemId, count)
        end
    else
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage(template.ConfirmMessage), DelegateFactory.Action(function ()
            if owned then
                MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("LinyuShopWnd_Buy_Owned_MallItem_Confirm"), DelegateFactory.Action(function ()
                    Gac2Gas.BuyMallItem(type, template.ItemId, count)
                end), nil, nil, nil, false)
            else
                Gac2Gas.BuyMallItem(type, template.ItemId, count)
            end
        end), nil, nil, nil, false)
    end
end

LuaModelTextureLoaderMgr = {}

function LuaModelTextureLoaderMgr:SetModelCameraColor(identifier)
    if CUIManager.instance then
        local  t = CUIManager.instance.transform:Find(identifier)
        if t then
            local camera = t:GetComponent(typeof(Camera))
            if camera then
                camera.backgroundColor = Color(0,0,0,0)
            end
        end
    end
end
----------------------------------------
---Gas2Gac
----------------------------------------
function Gas2Gac.OneKeyBuyMallItemResult(result)
    if result then
        CUIManager.CloseUI(CLuaUIResources.FashionPreviewTryToBuyWnd)
    end
end

function Gas2Gac.SyncRepertoryForMallPreviewDone()
    g_ScriptEvent:BroadcastInLua("OnSyncRepertoryForMallPreviewDone")
end
