local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local Animation = import "UnityEngine.Animation"
local ParticleSystem = import "UnityEngine.ParticleSystem"
local CButton = import "L10.UI.CButton"
local Item_Item = import "L10.Game.Item_Item"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CTeamMgr = import "L10.Game.CTeamMgr"
local CChatHelper = import "L10.UI.CChatHelper"
local CSocialWndMgr = import "L10.UI.CSocialWndMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CRankData = import "L10.UI.CRankData"
local CNumberKeyBoardMgr = import "CNumberKeyBoardMgr"
local AlignType = import "L10.UI.CTooltip+AlignType"
local CItemMgr = import "L10.Game.CItemMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
LuaYaYunHui2023LotteryWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaYaYunHui2023LotteryWnd, "MainView", "MainView", GameObject)
RegistChildComponent(LuaYaYunHui2023LotteryWnd, "OperateView", "OperateView", GameObject)
RegistChildComponent(LuaYaYunHui2023LotteryWnd, "AwardLabel", "AwardLabel", UILabel)
RegistChildComponent(LuaYaYunHui2023LotteryWnd, "BottomView", "BottomView", GameObject)
RegistChildComponent(LuaYaYunHui2023LotteryWnd, "TabTemplate", "TabTemplate", GameObject)
RegistChildComponent(LuaYaYunHui2023LotteryWnd, "Table", "Table", GameObject)
RegistChildComponent(LuaYaYunHui2023LotteryWnd, "RankBtn", "RankBtn", GameObject)
RegistChildComponent(LuaYaYunHui2023LotteryWnd, "Btn1", "Btn1", CButton)
RegistChildComponent(LuaYaYunHui2023LotteryWnd, "Btn2", "Btn2", CButton)
RegistChildComponent(LuaYaYunHui2023LotteryWnd, "Question", "Question", UILabel)
RegistChildComponent(LuaYaYunHui2023LotteryWnd, "Arrow1", "Arrow1", GameObject)
RegistChildComponent(LuaYaYunHui2023LotteryWnd, "Arrow2", "Arrow2", GameObject)
RegistChildComponent(LuaYaYunHui2023LotteryWnd, "BuyView", "BuyView", GameObject)
RegistChildComponent(LuaYaYunHui2023LotteryWnd, "AddView", "AddView", GameObject)
RegistChildComponent(LuaYaYunHui2023LotteryWnd, "SuccessView", "SuccessView", GameObject)
RegistChildComponent(LuaYaYunHui2023LotteryWnd, "FailView", "FailView", GameObject)
RegistChildComponent(LuaYaYunHui2023LotteryWnd, "SpecialView", "SpecialView", GameObject)
RegistChildComponent(LuaYaYunHui2023LotteryWnd, "DesLabel", "DesLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaYaYunHui2023LotteryWnd, "ShareBtn")
RegistClassMember(LuaYaYunHui2023LotteryWnd, "m_BuyBtn")
RegistClassMember(LuaYaYunHui2023LotteryWnd, "m_AddBtn")
RegistClassMember(LuaYaYunHui2023LotteryWnd, "m_GetBtn")
RegistClassMember(LuaYaYunHui2023LotteryWnd, "m_SpecialBtn")
RegistClassMember(LuaYaYunHui2023LotteryWnd, "m_Progress1")
RegistClassMember(LuaYaYunHui2023LotteryWnd, "m_Progress2")
RegistClassMember(LuaYaYunHui2023LotteryWnd, "m_AwardLabel1")
RegistClassMember(LuaYaYunHui2023LotteryWnd, "m_AwardLabel2")
RegistClassMember(LuaYaYunHui2023LotteryWnd, "m_DescLabel1")
RegistClassMember(LuaYaYunHui2023LotteryWnd, "m_DescLabel2")
RegistClassMember(LuaYaYunHui2023LotteryWnd, "m_DueLabel")
RegistClassMember(LuaYaYunHui2023LotteryWnd, "m_OperateViewBg")
RegistClassMember(LuaYaYunHui2023LotteryWnd, "m_OperateViewBg2")
RegistClassMember(LuaYaYunHui2023LotteryWnd, "m_AddViewBg")
RegistClassMember(LuaYaYunHui2023LotteryWnd, "m_PrizePoolAnchor")
RegistClassMember(LuaYaYunHui2023LotteryWnd, "m_QnaddDecreaseButton")   
RegistClassMember(LuaYaYunHui2023LotteryWnd, "m_NumInputLabel") 

RegistClassMember(LuaYaYunHui2023LotteryWnd, "m_CurAddBetting")         -- 竞猜或加注份数
RegistClassMember(LuaYaYunHui2023LotteryWnd, "m_TabViewList")           -- Tab对象
RegistClassMember(LuaYaYunHui2023LotteryWnd, "m_RightChoiceList")       -- 所有期竞猜的正确选项
RegistClassMember(LuaYaYunHui2023LotteryWnd, "m_LotteryStatus")         -- 所有期竞猜的状态
RegistClassMember(LuaYaYunHui2023LotteryWnd, "m_CurQuestionDesInfo")    -- 当前竞猜题目信息
RegistClassMember(LuaYaYunHui2023LotteryWnd, "m_GetAwardData")          -- 玩家领奖及投注数据

RegistClassMember(LuaYaYunHui2023LotteryWnd, "m_Id")                -- 当前竞猜期数
RegistClassMember(LuaYaYunHui2023LotteryWnd, "m_ShowViewStatus")    -- 当前期竞猜状态
RegistClassMember(LuaYaYunHui2023LotteryWnd, "m_Select")            -- 当前期竞猜玩家选择选项
RegistClassMember(LuaYaYunHui2023LotteryWnd, "m_HasBet")            -- 当前期竞猜玩家已经投注选项
RegistClassMember(LuaYaYunHui2023LotteryWnd, "m_HasBetValue")       -- 当前期竞猜玩家已经投注金额
RegistClassMember(LuaYaYunHui2023LotteryWnd, "m_CurPrizePool")      -- 当前期竞猜奖池奖励数量

RegistClassMember(LuaYaYunHui2023LotteryWnd, "m_CanOpenRank")       -- 是否能打开排行榜
RegistClassMember(LuaYaYunHui2023LotteryWnd, "m_MaxBetting")            -- 最多加注份数
RegistClassMember(LuaYaYunHui2023LotteryWnd, "m_ValuePrePart")      -- 每份投注的银两
RegistClassMember(LuaYaYunHui2023LotteryWnd, "m_AddBetLineFx")
RegistClassMember(LuaYaYunHui2023LotteryWnd, "m_GetAwardFx")
RegistClassMember(LuaYaYunHui2023LotteryWnd, "m_FxTick")
RegistClassMember(LuaYaYunHui2023LotteryWnd, "m_RankData")
RegistClassMember(LuaYaYunHui2023LotteryWnd, "m_CanUseJingCaiAmount")   -- 可用竞猜抵扣金额

EnumAsianGames2023GambleType = {
	eNotOpen = 1,
	eOpen = 2,
	eClose = 3,
	eReward = 4
}
EnumYaYunHui2023LotteryViewStatus = {
    default = 0,    
    buybetting = 1, -- 购买或追加
    addbetting = 2, -- 是否追加
    betSuccess = 4, -- 投注成功
    betFailed = 5,  -- 投注失败
    other = 6,      -- 特殊情况
}

function LuaYaYunHui2023LotteryWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    UIEventListener.Get(self.Btn1.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnOptionClick(1)
    end)

    UIEventListener.Get(self.Btn2.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnOptionClick(2)
    end)

    UIEventListener.Get(self.RankBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        if self.m_RankData and self.m_RankData.Count > 0 then
            CUIManager.ShowUI(CLuaUIResources.YaYunHui2023LotteryRankWnd)
        else
            g_MessageMgr:ShowMessage("YaYunHui2023_LotteryRank_NoData")
        end
    end)

    self.TabTemplate.gameObject:SetActive(false)
    Extensions.RemoveAllChildren(self.Table.transform)
    self.m_CurQuestionDesInfo = nil
    self.m_LotteryStatus = nil
    self.m_RightChoiceList = nil
    self.m_TabViewList = nil
    self.m_PrizePoolAnchor = nil

    self.m_FxTick = nil
    
    self.m_CurAddBetting = 1
    self.m_Id = 0
    self.m_Select = 0
    self.m_HasBet = 0
    self.m_CanUseJingCaiAmount = 0
    self.m_ShowViewStatus = EnumYaYunHui2023LotteryViewStatus.default
    self.m_CurPrizePool = 0
    self.m_HasBetValue = 0
    self.m_DueLabel = self.transform:Find("Anchor/LotteryView/Right/CurReward/DueLabel"):GetComponent(typeof(UILabel))
    self.m_DescLabel1 = self.Btn1.transform:Find("DescLabel"):GetComponent(typeof(UILabel))
    self.m_DescLabel2 = self.Btn2.transform:Find("DescLabel"):GetComponent(typeof(UILabel))
    self.m_Progress1 = self.Btn1.transform:Find("ProgressBar"):GetComponent(typeof(UISlider))
    self.m_Progress2 = self.Btn2.transform:Find("ProgressBar"):GetComponent(typeof(UISlider))
    self.m_AwardLabel1 = self.Btn1.transform:Find("AwardLabel"):GetComponent(typeof(UILabel))
    self.m_AwardLabel2 = self.Btn2.transform:Find("AwardLabel"):GetComponent(typeof(UILabel))
    self.m_OperateViewBg = self.OperateView.transform:Find("OperateViewBg"):GetComponent(typeof(UITexture))
    self.m_OperateViewBg2 = self.m_OperateViewBg.transform:Find("Sprite"):GetComponent(typeof(UISprite))
    self.m_AddViewBg = self.AddView.transform:Find("Bg"):GetComponent(typeof(UITexture))
    self.m_Ani = self.transform:GetComponent(typeof(Animation)) 
    self.m_AddBetFx = self.transform:Find("Anchor/LotteryView/Right/CUIFxqianbi/qianbi"):GetComponent(typeof(ParticleSystem))
    self.ShareBtn = self.BottomView.transform:Find("Sharebtn").gameObject
    self.m_BuyBtn = self.BuyView.transform:Find("BuyButton"):GetComponent(typeof(CButton))
    self.m_AddBtn = self.AddView.transform:Find("AddButton"):GetComponent(typeof(CButton))
    self.m_GetBtn = self.SuccessView.transform:Find("GetButton"):GetComponent(typeof(CButton))
    self.m_SpecialBtn = self.SpecialView.transform:Find("RewardButton"):GetComponent(typeof(CButton))
    self.m_QnaddDecreaseButton = self.BuyView.transform:Find("QnIncreseAndDecreaseButton"):GetComponent(typeof(QnAddSubAndInputButton))
    self.m_AddBetFx.gameObject:SetActive(false)
    local getRewardFx = self.transform:Find("Anchor/LotteryView/Right/manping").gameObject
    getRewardFx.gameObject:SetActive(false)
    local gambleSetting = g_LuaUtil:StrSplit(AsianGames2023_Setting.GetData().GambleSetting, ",")
    self.m_ValuePrePart = gambleSetting and tonumber(gambleSetting[1]) or 1
    self.m_MaxBetting =  gambleSetting and tonumber(gambleSetting[2]) or 1

    self.m_DueLabel.text = nil
    self.m_NumInputLabel = self.m_QnaddDecreaseButton.transform:Find("QnNumberInput/Label"):GetComponent(typeof(UILabel))
    UIEventListener.Get(self.m_NumInputLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnNumberInputClick()
    end)
end

function LuaYaYunHui2023LotteryWnd:Init()
    self.Arrow1.gameObject:SetActive(false)
    self.Arrow2.gameObject:SetActive(false)
    self.MainView.gameObject:SetActive(false)
    self.OperateView.gameObject:SetActive(false)
    self.AwardLabel.text = self.m_CurPrizePool

    UIEventListener.Get(self.m_AddBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        if self.m_ShowViewStatus == EnumYaYunHui2023LotteryViewStatus.addbetting then
            self.m_ShowViewStatus = EnumYaYunHui2023LotteryViewStatus.buybetting
            self:ShowView()
        end
    end)
    UIEventListener.Get(self.m_GetBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        if self.m_Ani then
            self.m_Ani:Play("yayunhui2023lotterywnd_lingjiang")
        end
        Gac2Gas.AsianGames2023RequestGetGambleReward(self.m_Id)
    end)
    UIEventListener.Get(self.m_SpecialBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        Gac2Gas.AsianGames2023RequestGetGambleReward(self.m_Id)
    end)
    self.SpecialView.transform:Find("Label"):GetComponent(typeof(UILabel)).text = g_MessageMgr:FormatMessage("YaYunHui2023_SpecialView_Desc")
    self:InitBottom()
    self:InitBuyView()
    Gac2Gas.QueryRank(AsianGames2023_Setting.GetData().GambleRankId)
    Gac2Gas.AsianGames2023QueryGambleStatus()
end

function LuaYaYunHui2023LotteryWnd:OnAsianGames2023RequestQuestionsResult(gambleStatusCache)
    self.m_LotteryStatus = gambleStatusCache[1]
    self.m_RightChoiceList = gambleStatusCache[2]

    self:GetPlayerRewardData()
    if not self.m_TabViewList then
        self:InitTabView()
    end
    self:OnSelectTab(#self.m_TabViewList)
end

function LuaYaYunHui2023LotteryWnd:InitTabView()
    self.m_TabViewList = {}
    Extensions.RemoveAllChildren(self.Table.transform)
    local totalData = AsianGames2023_Gamble.GetDataCount()
    local curTime = CServerTimeMgr.Inst.timeStamp
    for i = totalData,1,-1 do
        local data = AsianGames2023_Gamble.GetData(i)
        local startTimeStr = data.OpenTime
        local openTimeStamp = CServerTimeMgr.Inst:GetTimeStampByStr(startTimeStr)
        local needShow = curTime >= openTimeStamp and self.m_LotteryStatus and self.m_LotteryStatus[i] ~= EnumAsianGames2023GambleType.eNotOpen
        if needShow then

            local year,month,day,_ , _ = string.match(startTimeStr,"(%d+)-(%d+)-(%d+) (%d+):(%d+)")
            local tab = CUICommonDef.AddChild(self.Table.gameObject,self.TabTemplate.gameObject)
            tab.gameObject:SetActive(true)
            local highlight = tab.transform:Find("Highlight").gameObject
            local normal = tab.transform:Find("Normal").gameObject
            highlight.gameObject:SetActive(false)
            normal.gameObject:SetActive(true)

            local dateStr = SafeStringFormat3(LocalString.GetString("%d月%d日"),month,day)
            highlight.transform:Find("Label"):GetComponent(typeof(UILabel)).text = dateStr
            normal.transform:Find("Label"):GetComponent(typeof(UILabel)).text = dateStr
            local normalRedDot = normal.transform:Find("Sprite").gameObject
            local highlightRedDot = highlight.transform:Find("Sprite").gameObject
            local needShowRedDot = self:GetRedDotStatus(i)
            normalRedDot.gameObject:SetActive(needShowRedDot)
            highlightRedDot.gameObject:SetActive(needShowRedDot)
            self.m_TabViewList[i] = {
                id = i,
                normalView = normal,
                highlightView = highlight,
                nRedDot = normalRedDot,
                sRedDot = highlightRedDot,
            }
            local index = i
            UIEventListener.Get(tab.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
                self:OnSelectTab(index)
            end)
        end
    end
    self.Table:GetComponent(typeof(UITable)):Reposition()
end

function LuaYaYunHui2023LotteryWnd:OnSelectTab(index)
    if self.m_Id == index then return end
    Gac2Gas.AsianGames2023QueryGambleInfo(index)
end

function LuaYaYunHui2023LotteryWnd:OnAsianGames2023QueryGambleInfo(id, status, choice, choiceOneNum, choiceTwoNum, description)

    -- status 当期竞猜状态
    -- choice 正确选项,选项为0表示特殊情况无正确选项
    -- choiceOneNum 选项1选择人数
    -- choiceTwoNum 选项2选择人数
    -- description 题目和选项，description, optionOneDescription, optionTwoDescription，如果为空表则读本地策划表
    if self.m_TabViewList and self.m_TabViewList[self.m_Id] then
        self.m_TabViewList[self.m_Id].normalView.gameObject:SetActive(true)
        self.m_TabViewList[self.m_Id].highlightView.gameObject:SetActive(false)
    end
    if self.m_TabViewList and self.m_TabViewList[id] then
        self.m_TabViewList[id].normalView.gameObject:SetActive(false)
        self.m_TabViewList[id].highlightView.gameObject:SetActive(true)
    end
    local question = ""
    local option1 = ""
    local option2 = ""
    local dueTime = ""
    local data = AsianGames2023_Gamble.GetData(id)
    if data then dueTime = data.CloseTime end
    if #description > 0 then
        question = description[1]
        option1 = description[2]
        option2 = description[3]
    else
        
        if data then
            question = data.Description
            option1 = data.OptionOneDescription
            option2 = data.OptionTwoDescription
        end
    end
    self.m_Id = id
    self.m_CurQuestionDesInfo = {}
    self.m_CurQuestionDesInfo.question = string.gsub(question, "#r", "\n")
    self.m_CurQuestionDesInfo.answerList = {option1,option2}
    self.m_CurQuestionDesInfo.choice = choice
    self.m_CurQuestionDesInfo.choiceInfo = {choiceOneNum,choiceTwoNum}
    self.m_CurQuestionDesInfo.status = status
    self.m_CurQuestionDesInfo.closeTime = dueTime

    self:UpdateMainView()
    self.Table:GetComponent(typeof(UITable)):Reposition()
end



function LuaYaYunHui2023LotteryWnd:UpdateMainView()
    if not self.m_CurQuestionDesInfo then return end
    local dataInfo = self.m_CurQuestionDesInfo
    self.Question.text = dataInfo.question
    self.m_DescLabel1.text = dataInfo.answerList[1]
    self.m_DescLabel2.text = dataInfo.answerList[2]
    self.m_DueLabel.text = SafeStringFormat3(LocalString.GetString("本期竞猜截止时间 %s"), dataInfo.closeTime)

    self.m_ShowViewStatus = EnumYaYunHui2023LotteryViewStatus.default
    self:UpdateChoiceShowInfo()
    
    self.m_Select = 0
    self:UpdateCurBetInfo()
    self.Btn1.Enabled = true
    self.Btn2.Enabled = true
    if dataInfo.status == EnumAsianGames2023GambleType.eOpen then -- 可投注
        if self.m_HasBet > 0 then    -- 已经投注
            self.m_ShowViewStatus = EnumYaYunHui2023LotteryViewStatus.addbetting
        else    -- 未投注
            self.m_ShowViewStatus = EnumYaYunHui2023LotteryViewStatus.buybetting
        end
    elseif dataInfo.status == EnumAsianGames2023GambleType.eClose then  -- 投注结束
        if self.m_HasBet > 0 then    -- 已经投注
            self.m_ShowViewStatus = EnumYaYunHui2023LotteryViewStatus.addbetting
        else
            self.m_ShowViewStatus = EnumYaYunHui2023LotteryViewStatus.betFailed
        end
    elseif dataInfo.status == EnumAsianGames2023GambleType.eReward then
        local choice = dataInfo.choice
        if choice == 0 then 
            self.m_ShowViewStatus = EnumYaYunHui2023LotteryViewStatus.other
        else
            if self.m_GetAwardData and self.m_GetAwardData[self.m_Id] and  self.m_GetAwardData[self.m_Id][1] == choice then
                self.m_ShowViewStatus = EnumYaYunHui2023LotteryViewStatus.betSuccess
            else
                self.m_ShowViewStatus = EnumYaYunHui2023LotteryViewStatus.betFailed
            end
        end
        self.Btn1.Enabled = choice == 1
        self.Btn2.Enabled = choice == 2
        self.m_DescLabel1.text = dataInfo.answerList[1]..(choice == 1 and LocalString.GetString("（正确答案）") or "")
        self.m_DescLabel2.text = dataInfo.answerList[2]..(choice == 2 and LocalString.GetString("（正确答案）") or "")
        if self.m_HasBet <= 0 then 
            self.m_ShowViewStatus = EnumYaYunHui2023LotteryViewStatus.betFailed
        end
    end

    if self.m_HasBet > 0 then
        self:OnOptionClick(self.m_HasBet)
    else
        self:ShowView()
    end
end

function LuaYaYunHui2023LotteryWnd:UpdateChoiceShowInfo()
    local dataInfo = self.m_CurQuestionDesInfo
    local precent1 = dataInfo.choiceInfo[1] / (dataInfo.choiceInfo[1] + dataInfo.choiceInfo[2])
    local precent2 = dataInfo.choiceInfo[2] / (dataInfo.choiceInfo[1] + dataInfo.choiceInfo[2])
    self.m_Progress1.value = precent1
    self.m_Progress2.value = precent2
    
    self.m_CurPrizePool = (dataInfo.choiceInfo[1] + dataInfo.choiceInfo[2]) * self.m_ValuePrePart
    local responseRateFormula = GetFormula(AsianGames2023_Setting.GetData().GambleOddsFormulaId)
    local response1Rate = responseRateFormula and responseRateFormula(nil, nil, {self.m_ValuePrePart * dataInfo.choiceInfo[1],self.m_CurPrizePool}) or 1
    local response2Rate = responseRateFormula and responseRateFormula(nil, nil, {self.m_ValuePrePart * dataInfo.choiceInfo[2],self.m_CurPrizePool}) or 1
    self.AwardLabel.text = self.m_CurPrizePool
    self:UpdatePrizePoolAnchor(1)
    self.m_AwardLabel1.text = SafeStringFormat3(LocalString.GetString("%.1f%%选择 %.2f回报率"),precent1 * 100,response1Rate)
    self.m_AwardLabel2.text = SafeStringFormat3(LocalString.GetString("%.1f%%选择 %.2f回报率"),precent2 * 100,response2Rate)
end

function LuaYaYunHui2023LotteryWnd:UpdatePrizePoolAnchor(index)
    if not self.m_PrizePoolAnchor then
        self.m_PrizePoolAnchor = {}
        self.m_PrizePoolAnchor[1] = {
            self.AwardLabel.transform:Find("Sprite"):GetComponent(typeof(UIWidget)),
            self.transform:Find("Anchor/LotteryView/Right/CurReward/Icon"):GetComponent(typeof(UIWidget)),
            self.transform:Find("Anchor/LotteryView/Right/CurReward/Label"):GetComponent(typeof(UIWidget))
        }
        self.m_PrizePoolAnchor[2] = {
            self.BuyView.transform:Find("ResLabel/Texture"):GetComponent(typeof(UIWidget)),
            self.BuyView.transform:Find("ResLabel/Sprite"):GetComponent(typeof(UIWidget)),
            self.BuyView.transform:Find("ResLabel/Label"):GetComponent(typeof(UIWidget)),
        }
        self.m_PrizePoolAnchor[3] = {
            self.AddView.transform:Find("ShowLabel/Texture"):GetComponent(typeof(UIWidget)),
            self.AddView.transform:Find("ShowLabel/Sprite"):GetComponent(typeof(UIWidget)),
        }
        self.m_PrizePoolAnchor[4] = {
            self.AddView.transform:Find("ResLabel/Texture"):GetComponent(typeof(UIWidget)),
            self.AddView.transform:Find("ResLabel/Sprite"):GetComponent(typeof(UIWidget)),
            self.AddView.transform:Find("ResLabel/Label"):GetComponent(typeof(UIWidget)),
        }
        self.m_PrizePoolAnchor[5] = {
            self.AddView.transform:Find("ResLabel/Texture"):GetComponent(typeof(UIWidget)),
            self.AddView.transform:Find("ResLabel/Sprite"):GetComponent(typeof(UIWidget)),
            self.AddView.transform:Find("ResLabel/Label"):GetComponent(typeof(UIWidget)),
        }
        self.m_PrizePoolAnchor[6] = {
            self.SuccessView.transform:Find("ShowLabel/Texture"):GetComponent(typeof(UIWidget)),
            self.SuccessView.transform:Find("ShowLabel/Sprite"):GetComponent(typeof(UIWidget)),
        }
        self.m_PrizePoolAnchor[7] = {
            self.SpecialView.transform:Find("ShowLabel/Texture"):GetComponent(typeof(UIWidget)),
            self.SpecialView.transform:Find("ShowLabel/Sprite"):GetComponent(typeof(UIWidget)),
            self.SpecialView.transform:Find("ShowLabel/Label"):GetComponent(typeof(UIWidget)),
        }
    end
    for k,v in pairs(self.m_PrizePoolAnchor[index]) do
        if v then
            v:ResetAndUpdateAnchors()
        end
    end
end

function LuaYaYunHui2023LotteryWnd:UpdateCurBetInfo()
    self.m_HasBet = (self.m_GetAwardData and self.m_GetAwardData[self.m_Id] and self.m_GetAwardData[self.m_Id][1]) or 0
    self.m_HasBetValue = (self.m_GetAwardData and self.m_GetAwardData[self.m_Id] and self.m_GetAwardData[self.m_Id][3]) or 0
end

function LuaYaYunHui2023LotteryWnd:InitBottom()
    -- local itemId = AsianGames2023_Setting.GetData().GambleBetAwardTemplateId
    -- local itemData = Item_Item.GetData(itemId)
    -- if itemData then
    --     self.ItemLink.text = SafeStringFormat3("[%s]",itemData.Name)
    --     UIEventListener.Get(self.ItemLink.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
    --         CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
    --     end)
    -- end
    
    UIEventListener.Get(self.ShareBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnShareBtnClick()
    end)
    local infoBtn = self.BottomView.transform:Find("InfoBtn").gameObject
    UIEventListener.Get(infoBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        g_MessageMgr:ShowMessage("YaYunHui2023_BetRule")
    end)
    self:RefreshItem()
end
function LuaYaYunHui2023LotteryWnd:OnShareBtnClick()
    local popupList={}
    table.insert( popupList,PopupMenuItemData(LocalString.GetString("分享至世界"), DelegateFactory.Action_int(function(index) 
        self:ShareLottery(EChatPanel.World)
    end),false, nil, EnumPopupMenuItemStyle.Default))
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst:IsInGuild() then
        table.insert( popupList,PopupMenuItemData(LocalString.GetString("分享至帮会"), DelegateFactory.Action_int(function(index) 
            self:ShareLottery(EChatPanel.Guild)
        end),false, nil, EnumPopupMenuItemStyle.Default))
    end
    if CTeamMgr.Inst:TeamExists() then
        table.insert( popupList,PopupMenuItemData(LocalString.GetString("分享至队伍"), DelegateFactory.Action_int(function(index) 
            self:ShareLottery(EChatPanel.Team)
        end),false, nil, EnumPopupMenuItemStyle.Default))
    end
    local array=Table2Array(popupList,MakeArrayClass(PopupMenuItemData))
    local side = CPopupMenuInfoMgr.AlignType.Right
    CPopupMenuInfoMgr.ShowPopupMenu(array,self.ShareBtn.transform, side, 1, nil, 600, true, 220)
end
function LuaYaYunHui2023LotteryWnd:ShareLottery(channel)
    if not self.m_CurQuestionDesInfo then return end
    local optionLabelArray = {"A.","B."}
    local optionText = ""
    for i = 1,#self.m_CurQuestionDesInfo.answerList do
        local optionLabel = optionLabelArray[i]
        local answer = self.m_CurQuestionDesInfo.answerList[i]
        optionLabel = optionLabel .. answer
        optionText = optionText .. ((i > 1) and " " or "") .. optionLabel
    end
    local questionText = g_LuaUtil:StrSplit(self.m_CurQuestionDesInfo.question,"\n")[1]
    local link = g_MessageMgr:FormatMessage("YaYunHui2023_Share_Lottery", questionText, optionText)
    CChatHelper.SendMsgWithFilterOption(channel,link,0, true)
    CSocialWndMgr.ShowChatWnd()
end
function LuaYaYunHui2023LotteryWnd:OnOptionClick(index)
    local status = self.m_CurQuestionDesInfo and self.m_CurQuestionDesInfo.status or EnumAsianGames2023GambleType.eNotOpen
    if self.m_Select == 0 then
        if status == EnumAsianGames2023GambleType.eOpen or index == self.m_HasBet then
            self.Arrow1.gameObject:SetActive(index == 1)
            self.Arrow2.gameObject:SetActive(index == 2)
            if self.m_Ani then
                if self.m_Ani then self.m_Ani:Stop() end
                self.m_Ani:Play("yayunhui2023lotterywnd_dakai")
            end
        else
            g_MessageMgr:ShowMessage("YaYunHui2023_BetOutOfData")
            return
        end
    elseif (self.m_Select ~= 0 and self.m_HasBet == 0) or self.m_HasBet == index then
        if index == 1 then
            self.Arrow1.gameObject:SetActive(true)
            if self.m_Ani then
                if self.m_Ani then self.m_Ani:Stop() end
                self.m_Ani:Play("yayunhui2023lotterywnd_tab01")
            end
        else
            self.Arrow2.gameObject:SetActive(true)
            if self.m_Ani then
                if self.m_Ani then self.m_Ani:Stop() end
                self.m_Ani:Play("yayunhui2023lotterywnd_tab02")
            end
        end
    else
        g_MessageMgr:ShowMessage("YaYunHui2023_CannotBetAnnotherOption")
        return
    end
    self.m_Select = index
    self:ShowView()
end

function LuaYaYunHui2023LotteryWnd:ShowView()
    self.MainView.gameObject:SetActive(true)
    self.OperateView.gameObject:SetActive(true)
    if self.m_Select == 0 and self.m_ShowViewStatus ~= EnumYaYunHui2023LotteryViewStatus.betFailed then   -- 没有选择时，不显示操作窗口
        if self.m_Ani then self.m_Ani:Stop() end
        self.MainView.gameObject:SetActive(true)
        self.OperateView.gameObject:SetActive(false)
        local view = self.MainView.transform:Find("zhuneirong").gameObject
        Extensions.SetLocalPositionX(view.transform, 274)
        return
    elseif self.m_Select == 1 then
        self.m_OperateViewBg.color = NGUIText.ParseColor24("c3f0ea", 0)
        self.m_OperateViewBg2.color = NGUIText.ParseColor24("abd6db", 0)
        self.m_OperateViewBg2.alpha = 0.2
        self.m_AddViewBg.color = NGUIText.ParseColor24("a3cfd4", 0)
    elseif self.m_Select == 2 then
        self.m_OperateViewBg.color = NGUIText.ParseColor24("ebc3c6", 0)
        self.m_OperateViewBg2.color = NGUIText.ParseColor24("efccce", 0)
        self.m_OperateViewBg2.alpha = 0.2
        self.m_AddViewBg.color = NGUIText.ParseColor24("eccecb", 0)
    else
        self.Arrow1.gameObject:SetActive(false)
        self.Arrow2.gameObject:SetActive(false)
        if self.m_Ani then
            if self.m_Ani then self.m_Ani:Stop() end
            self.m_Ani:Play("yayunhui2023lotterywnd_dakai")
        end
        self.m_OperateViewBg.color = NGUIText.ParseColor24("ffffff", 0)
        self.m_OperateViewBg2.color = NGUIText.ParseColor24("ffffff", 0)
        self.m_OperateViewBg2.alpha = 0.2
        self.m_AddViewBg.color = NGUIText.ParseColor24("ffffff", 0)
    end
    self.BuyView.gameObject:SetActive(self.m_ShowViewStatus == EnumYaYunHui2023LotteryViewStatus.buybetting)
    self.AddView.gameObject:SetActive(self.m_ShowViewStatus == EnumYaYunHui2023LotteryViewStatus.addbetting)
    self.SuccessView.gameObject:SetActive(self.m_ShowViewStatus == EnumYaYunHui2023LotteryViewStatus.betSuccess)
    self.FailView.gameObject:SetActive(self.m_ShowViewStatus == EnumYaYunHui2023LotteryViewStatus.betFailed)
    self.SpecialView.gameObject:SetActive(self.m_ShowViewStatus == EnumYaYunHui2023LotteryViewStatus.other)

    if self.m_ShowViewStatus == EnumYaYunHui2023LotteryViewStatus.buybetting then
        self:UpdateBuyView()
    elseif self.m_ShowViewStatus == EnumYaYunHui2023LotteryViewStatus.addbetting then
        self:UpdateAddView()
    elseif self.m_ShowViewStatus == EnumYaYunHui2023LotteryViewStatus.betSuccess then
        self:UpdateSuccessView()
    elseif self.m_ShowViewStatus == EnumYaYunHui2023LotteryViewStatus.betFailed then
        self:UpdateFailView()
    elseif self.m_ShowViewStatus == EnumYaYunHui2023LotteryViewStatus.other then
        self:UpdateSpecialView()
    end
end

function LuaYaYunHui2023LotteryWnd:InitBuyView()
    local curentMoneyControl = self.BuyView.transform:Find("QnCostAndOwnMoney"):GetComponent(typeof(CCurentMoneyCtrl))
    local resLabel = self.BuyView.transform:Find("ResLabel/Num"):GetComponent(typeof(UILabel))
    self.m_QnaddDecreaseButton:SetMinMax(1,self.m_MaxBetting,1)
    
    local onValueChange = function (value)
        self:OnAddChangeValue(value)
    end
    UIEventListener.Get(self.m_BuyBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        if self.m_HasBetValue >= self.m_MaxBetting then
            g_MessageMgr:ShowMessage("YaYunHui2023_BetOutOfRange")
        elseif curentMoneyControl:GetCost() <= curentMoneyControl:GetOwn() then
            self:OnBuyBtnClick()
        else
            g_MessageMgr:ShowMessage("SILVER_NOT_ENOUGH")
        end
    end)
    onValueChange(self.m_CurAddBetting)
    self.m_QnaddDecreaseButton.onValueChanged = DelegateFactory.Action_uint(onValueChange)
end


function LuaYaYunHui2023LotteryWnd:UpdateBuyView()
    self.m_BuyBtn.transform:Find("Label"):GetComponent(typeof(UILabel)).text = self.m_HasBetValue <= 0 and LocalString.GetString("竞猜") or LocalString.GetString("追加投入")
    local hasBet = self.m_GetAwardData and self.m_GetAwardData[self.m_Id] and self.m_GetAwardData[self.m_Id][3] or 0
    if  hasBet >= self.m_MaxBetting then
        self.m_QnaddDecreaseButton:SetMinMax(0,0,1)
        self.m_QnaddDecreaseButton:SetValue(0,true)
        return
    else
        self.m_QnaddDecreaseButton:SetMinMax(1,(self.m_MaxBetting - hasBet),1)
        self.m_CurAddBetting = math.min((self.m_MaxBetting - hasBet),math.max(self.m_CurAddBetting,1))
        self.m_QnaddDecreaseButton:SetValue(self.m_CurAddBetting,true)
    end
end

function LuaYaYunHui2023LotteryWnd:OnAddChangeValue(value)
    local curentMoneyControl = self.BuyView.transform:Find("QnCostAndOwnMoney"):GetComponent(typeof(CCurentMoneyCtrl))
    local resLabel = self.BuyView.transform:Find("ResLabel/Num"):GetComponent(typeof(UILabel))
    if self.m_HasBetValue <= 0 then
        self.m_QnaddDecreaseButton:OverrideText(value)
    else
        local text = SafeStringFormat3("[B7B7B7]%d[-]  + %d",self.m_HasBetValue, value)
        self.m_QnaddDecreaseButton:OverrideText(text)
    end
    curentMoneyControl:SetCost(math.max(0, value * self.m_ValuePrePart - self.m_CanUseJingCaiAmount))
    -- e[1]自己的下注金额；e[2]押注项的总金额；e[3]本题的总金额
    if self.m_CurQuestionDesInfo and self.m_CurQuestionDesInfo.choiceInfo[self.m_Select] then
        resLabel.text = self:GetBetReward(
            (value + self.m_HasBetValue) * self.m_ValuePrePart,
            (self.m_CurQuestionDesInfo.choiceInfo[self.m_Select] + value) * self.m_ValuePrePart ,
            self.m_CurPrizePool + self.m_ValuePrePart * value)
        self:UpdatePrizePoolAnchor(2)
    end
    self.m_CurAddBetting = value
end

function LuaYaYunHui2023LotteryWnd:UpdateAddView()
    local showLabel = self.AddView.transform:Find("ShowLabel"):GetComponent(typeof(UILabel))
    local showValueLabel = showLabel.transform:Find("Label"):GetComponent(typeof(UILabel))
    local resLabel = self.AddView.transform:Find("ResLabel/Num"):GetComponent(typeof(UILabel))
    showLabel.text = SafeStringFormat3(LocalString.GetString("已投%d份，合计"),tonumber(self.m_HasBetValue))
    showValueLabel.text = self.m_HasBetValue * self.m_ValuePrePart
    resLabel.text = self:GetBetReward(
        self.m_HasBetValue * self.m_ValuePrePart,
        self.m_CurQuestionDesInfo.choiceInfo[self.m_Select] * self.m_ValuePrePart,
        self.m_CurPrizePool)
    if self.m_CurQuestionDesInfo and self.m_CurQuestionDesInfo.status == EnumAsianGames2023GambleType.eOpen then -- 可以追加
        self.m_AddBtn.Enabled = true
        self.m_AddBtn.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("追加投入")
    else
        self.m_AddBtn.Enabled = false
        self.m_AddBtn.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("本场竞猜已结束")
    end
    self:UpdatePrizePoolAnchor(3)
    self:UpdatePrizePoolAnchor(4)
end

function LuaYaYunHui2023LotteryWnd:UpdateSuccessView()
    local showLabel = self.SuccessView.transform:Find("ShowLabel"):GetComponent(typeof(UILabel))
    local resLabel = showLabel.transform:Find("Label"):GetComponent(typeof(UILabel))
    showLabel.text = SafeStringFormat3(LocalString.GetString("已投%d份，合计"),tonumber(self.m_HasBetValue))
    resLabel.text = self:GetBetReward(
        self.m_HasBetValue * self.m_ValuePrePart,
        self.m_CurQuestionDesInfo.choiceInfo[self.m_Select] * self.m_ValuePrePart,
        self.m_CurPrizePool)
    self:UpdatePrizePoolAnchor(5)
    if self.m_GetAwardData and self.m_GetAwardData[self.m_Id] and (not self.m_GetAwardData[self.m_Id][2]) then  -- 还未领奖
        self.m_GetBtn.Enabled = true
        self.m_GetBtn.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("领取奖励")
    else
        self.m_GetBtn.Enabled = false
        self.m_GetBtn.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("已领奖")
    end
end

function LuaYaYunHui2023LotteryWnd:UpdateSpecialView()
    local resLabel = self.SpecialView.transform:Find("ShowLabel/Num"):GetComponent(typeof(UILabel))
    resLabel.text = tonumber(self.m_HasBetValue * self.m_ValuePrePart)
    self:UpdatePrizePoolAnchor(6)
    if self.m_GetAwardData and self.m_GetAwardData[self.m_Id] and (not self.m_GetAwardData[self.m_Id][2]) then  -- 还未领奖
        self.m_SpecialBtn.Enabled = true
        self.m_SpecialBtn.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("领取奖励")
    else
        self.m_SpecialBtn.Enabled = false
        self.m_SpecialBtn.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("已领奖")
    end
end

function LuaYaYunHui2023LotteryWnd:UpdateFailView()
    local textLabel = self.FailView.transform:Find("Label"):GetComponent(typeof(UILabel))
    if self.m_HasBet > 0 then
        textLabel.text = LocalString.GetString("很遗憾\n竞猜失败")
    else
        textLabel.text = g_MessageMgr:FormatMessage("YaYunHui2023_BetOutOfData")
    end

end

function LuaYaYunHui2023LotteryWnd:OnBuyBtnClick()
    if self.m_ShowViewStatus == EnumYaYunHui2023LotteryViewStatus.buybetting then
        self.m_ShowViewStatus = EnumYaYunHui2023LotteryViewStatus.addbetting
    end
    self.m_AddBetFx.gameObject:SetActive(true)
    self.m_AddBetFx:Stop()
    self.m_AddBetFx:Play()

    Gac2Gas.AsianGames2023RequestGambleBet(self.m_Id,self.m_Select,self.m_CurAddBetting)
end


function LuaYaYunHui2023LotteryWnd:OnUpdatePlayDataWithKey(argv)
    if argv[0] ~= EnumTempPlayDataKey_lua.eAsianGames2023Data then return end
    self:GetPlayerRewardData()
    self:UpdateCurBetInfo()
    self:UpdateRedDot()
    self:ShowView()
    
end

function LuaYaYunHui2023LotteryWnd:UpdateRedDot()
    if not self.m_TabViewList then return end
    for k,v in pairs(self.m_TabViewList) do
        local id = v.id
        local needShowRedDot = self:GetRedDotStatus(id)
        v.nRedDot.gameObject:SetActive(needShowRedDot)
        v.sRedDot.gameObject:SetActive(needShowRedDot)
    end
end

function LuaYaYunHui2023LotteryWnd:GetPlayerRewardData()
    if CClientMainPlayer.Inst and CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eAsianGames2023Data) then
        local playDataU = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eAsianGames2023Data)
        local tab = g_MessagePack.unpack(playDataU.Data.Data)
        self.m_GetAwardData = tab and tab[EnumAsianGames2023DataType.eGamble] or {}
        -- { 0, false, 0 } -- {玩家选项,0表示没选选项;是否领奖;已经投注份数}
    end
end

function LuaYaYunHui2023LotteryWnd:GetRedDotStatus(index)

    if not self.m_GetAwardData or not self.m_LotteryStatus or not self.m_RightChoiceList then return false end
    if not self.m_GetAwardData[index] or not self.m_LotteryStatus[index] or not self.m_RightChoiceList[index] then return false end

    return  self.m_LotteryStatus[index] == EnumAsianGames2023GambleType.eReward and (not self.m_GetAwardData[index][2]) and
            (self.m_RightChoiceList[index] == self.m_GetAwardData[index][1] or self.m_RightChoiceList[index] == 0) and self.m_GetAwardData[index][3] > 0
end

function LuaYaYunHui2023LotteryWnd:GetBetReward(hasBetValue,totalChoiceValue,totalValue)
    local formula = GetFormula(AsianGames2023_Setting.GetData().GambleBetFormulaId)

    return formula and formula(nil, nil, {hasBetValue, totalChoiceValue, totalValue}) or hasBetValue
end

function LuaYaYunHui2023LotteryWnd:OnAsianGames2023RequestGambleBet(id,choiceOneNum, choiceTwoNum)
    if self.m_Id ~= id or not self.m_CurQuestionDesInfo then return end
    if self.m_FxTick then 
        UnRegisterTick(self.m_FxTick) 
        self.m_FxTick = nil 
        self:UpdateChoiceShowInfo()
    end
    self.m_CurQuestionDesInfo.choiceInfo = {choiceOneNum,choiceTwoNum}
    local totalPrizePool = (choiceOneNum + choiceTwoNum) * self.m_ValuePrePart
    if totalPrizePool ~= self.m_CurPrizePool then
        local delta = (totalPrizePool - self.m_CurPrizePool) / 20
        local cnt = 1
        self.m_FxTick = RegisterTickWithDuration(function()
            if cnt >= 20 then
                UnRegisterTick(self.m_FxTick) 
                self.m_FxTick = nil 
                self:UpdateChoiceShowInfo()
                self:ShowView()
                return
            end
            self.AwardLabel.text = self.m_CurPrizePool + delta * cnt
            self:UpdatePrizePoolAnchor(1)
            cnt = cnt + 1
        end,50,1000)
    else
        self:UpdateChoiceShowInfo()
        self:ShowView()
    end
end

function LuaYaYunHui2023LotteryWnd:RefreshItem()
    local itemid = AsianGames2023_Setting.GetData().JIngCaiQuan
    local count = AsianGames2023_Setting.GetData().JIngCaiQuanNum
    local curNum = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, itemid)
    if curNum > 0 then
        self.DesLabel.text = g_MessageMgr:FormatMessage("YaYunHui2023_HaveJingCaiQuan",curNum,count * curNum)
        self.m_CanUseJingCaiAmount = curNum * count
    else
        self.DesLabel.text = g_MessageMgr:FormatMessage("YaYunHui2023_HaveNoJingCaiQuan")
        self.m_CanUseJingCaiAmount = 0
    end
    self.BottomView.gameObject:GetComponent(typeof(UITable)):Reposition()
    self:OnAddChangeValue(self.m_CurAddBetting)
end

function LuaYaYunHui2023LotteryWnd:OnRankDataReady()
    if CLuaRankData.m_CurRankId ~= AsianGames2023_Setting.GetData().GambleRankId then return end
    self.m_RankData =  CRankData.Inst and CRankData.Inst.RankList
end

function LuaYaYunHui2023LotteryWnd:OnNumberInputClick()
    CNumberKeyBoardMgr.ShowNumberKeyBoardWithRange(self.m_QnaddDecreaseButton:GetMinValue(), self.m_QnaddDecreaseButton:GetMaxValue(), self.m_QnaddDecreaseButton:GetMinValue(), 100, DelegateFactory.Action_int(function (val)
        self.m_QnaddDecreaseButton:SetValue(val)
        self:OnAddChangeValue(val)
    end), DelegateFactory.Action_int(function (val)
        self.m_QnaddDecreaseButton:SetValue(val)
        self:OnAddChangeValue(val)
    end), self.m_NumInputLabel, AlignType.Bottom, true)
end
function LuaYaYunHui2023LotteryWnd:OnEnable()
    g_ScriptEvent:AddListener("AsianGames2023QueryGambleStatusResult",self,"OnAsianGames2023RequestQuestionsResult")
    g_ScriptEvent:AddListener("AsianGames2023QueryGambleInfo",self,"OnAsianGames2023QueryGambleInfo")
    g_ScriptEvent:AddListener("UpdateTempPlayDataWithKey",self,"OnUpdatePlayDataWithKey")
    g_ScriptEvent:AddListener("AsianGames2023RequestGambleBet",self,"OnAsianGames2023RequestGambleBet")
    g_ScriptEvent:AddListener("OnRankDataReady", self, "OnRankDataReady")
    g_ScriptEvent:AddListener("SendItem", self, "RefreshItem")
	g_ScriptEvent:AddListener("SetItemAt", self, "RefreshItem")
end

function LuaYaYunHui2023LotteryWnd:OnDisable()
    g_ScriptEvent:RemoveListener("AsianGames2023QueryGambleStatusResult",self,"OnAsianGames2023RequestQuestionsResult")
    g_ScriptEvent:RemoveListener("AsianGames2023QueryGambleInfo",self,"OnAsianGames2023QueryGambleInfo")
    g_ScriptEvent:RemoveListener("UpdateTempPlayDataWithKey",self,"OnUpdatePlayDataWithKey")
    g_ScriptEvent:RemoveListener("AsianGames2023RequestGambleBet",self,"OnAsianGames2023RequestGambleBet")
    g_ScriptEvent:RemoveListener("OnRankDataReady", self, "OnRankDataReady")
    g_ScriptEvent:RemoveListener("SendItem", self, "RefreshItem")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "RefreshItem")
    if self.m_FxTick then UnRegisterTick(self.m_FxTick) self.m_FxTick = nil end
end

--@region UIEvent

--@endregion UIEvent

