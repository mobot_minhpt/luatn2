-- Auto Generated!!
local CFreightMgr = import "L10.Game.CFreightMgr"
local CFreightTaskItemCell = import "L10.UI.CFreightTaskItemCell"
local CFreightTaskWnd = import "L10.UI.CFreightTaskWnd"
local Extensions = import "Extensions"
local LocalString = import "LocalString"
local NGUITools = import "NGUITools"
CFreightTaskWnd.m_Init_CS2LuaHook = function (this) 
    local arrived = CFreightMgr.Inst.couldAcceptTask
    local leftTime = CFreightMgr.Inst.startTime - CFreightMgr.Inst.currentTime
    if leftTime <= 0 then
        leftTime = leftTime + (86400 --[[24 * 3600]])
    end
    this.haventArrived.gameObject:SetActive(not arrived)
    if leftTime >= 3600 then
        this.haventArrived.text = System.String.Format(LocalString.GetString("距离商队到达还有{0}小时"), math.ceil(leftTime / 3600))
    else
        this.haventArrived.text = System.String.Format(LocalString.GetString("距离商队到达还有{0}分钟"), math.ceil(leftTime % 3600 / 60))
    end
    this.hasArrived:SetActive(arrived)
    --nextOrder.SetActive(!arrived);
    this.nextOrder:SetActive(true)
    this.itemList.gameObject:SetActive(true)
    this:InitItemList()
    --startBtn.SetActive(arrived);
    this.startBtn:SetActive(false)
end
CFreightTaskWnd.m_InitItemList_CS2LuaHook = function (this) 
    Extensions.RemoveAllChildren(this.itemList.transform)
    if CFreightMgr.Inst.cargoList.Count < 3 then
        return
    end
    for i = 0, 2 do
        local item = NGUITools.AddChild(this.itemList.gameObject, this.itemCell)
        CommonDefs.GetComponent_GameObject_Type(item, typeof(CFreightTaskItemCell)):Init(CFreightMgr.Inst.cargoList[i].templateId, 0)
        item:SetActive(true)
    end
    this.itemList:Reposition()
end
