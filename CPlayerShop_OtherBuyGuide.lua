-- Auto Generated!!
local CPlayerShop_OtherBuyGuide = import "L10.UI.CPlayerShop_OtherBuyGuide"
local EnumItemType = import "L10.Game.EnumItemType"
CPlayerShop_OtherBuyGuide.m_Init_CS2LuaHook = function (this, type, templateId) 
    this.m_Type = type
    this.m_SearchExpBook = false
    if type == EnumItemType.WushanStone then
        this.m_TipLabel.text = CPlayerShop_OtherBuyGuide.WushanshiTip
        this.m_SearchTemplateId = CPlayerShop_OtherBuyGuide.WushanshiId
        this.gameObject:SetActive(true)
    elseif type == EnumItemType.Gem then
        this.m_TipLabel.text = CPlayerShop_OtherBuyGuide.BaoshiTip
        this.m_SearchTemplateId = CPlayerShop_OtherBuyGuide.BaoshiId
        this.gameObject:SetActive(true)
    elseif type == EnumItemType.Other then
        --目前其他就是指经验丹书
        this.m_SearchExpBook = true
        this.m_TipLabel.text = CPlayerShop_OtherBuyGuide.JingYanBookTip
        this.m_SearchTemplateId = CPlayerShop_OtherBuyGuide.BaoshiId
        this.gameObject:SetActive(true)
    else
        this.gameObject:SetActive(false)
    end
end
