require("common/common_include")

local CBaseWnd = import "L10.UI.CBaseWnd"
local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local CommonDefs = import "L10.Game.CommonDefs"

LuaCityWarPlaceBiaoCheConfirmWnd = class()
RegistClassMember(LuaCityWarPlaceBiaoCheConfirmWnd,"m_TitleLabel")
RegistClassMember(LuaCityWarPlaceBiaoCheConfirmWnd,"m_NeedLabel")
RegistClassMember(LuaCityWarPlaceBiaoCheConfirmWnd,"m_OwnLabel")
RegistClassMember(LuaCityWarPlaceBiaoCheConfirmWnd,"m_CostLabel")
RegistClassMember(LuaCityWarPlaceBiaoCheConfirmWnd,"m_CancelBtn")
RegistClassMember(LuaCityWarPlaceBiaoCheConfirmWnd,"m_OkBtn")

function LuaCityWarPlaceBiaoCheConfirmWnd:Awake()
    
end

function LuaCityWarPlaceBiaoCheConfirmWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaCityWarPlaceBiaoCheConfirmWnd:Init()

	self.m_TitleLabel = self.transform:Find("Anchor/TitleLabel"):GetComponent(typeof(UILabel))
	self.m_NeedLabel = self.transform:Find("Anchor/Node/NeedLabel"):GetComponent(typeof(UILabel))
	self.m_OwnLabel = self.transform:Find("Anchor/Node/OwnLabel"):GetComponent(typeof(UILabel))
	self.m_CancelBtn = self.transform:Find("Anchor/CancelButton").gameObject
	self.m_OkBtn = self.transform:Find("Anchor/OkButton").gameObject
	
	if not CLuaCityWarMgr.BiaoChePlaceConfirmInfo then self:Close() return end

	local map = CityWar_Map.GetData(CLuaCityWarMgr.BiaoChePlaceConfirmInfo.mapId)

	if not map then self:Close() return end

	self.m_TitleLabel.text = g_MessageMgr:FormatMessage("YAYUN_SET_BIAOCHE_CONFIRM", map.Name)
	self.m_NeedLabel.text = tostring(CLuaCityWarMgr.BiaoChePlaceConfirmInfo.cost)
	self.m_OwnLabel.text = tostring(CLuaCityWarMgr.BiaoChePlaceConfirmInfo.guildSilver)

	CommonDefs.AddOnClickListener(self.m_CancelBtn, DelegateFactory.Action_GameObject(function(go) self:OnCancelButtonClick() end), false)
	CommonDefs.AddOnClickListener(self.m_OkBtn, DelegateFactory.Action_GameObject(function(go) self:OnOKButtonClick() end), false)

end

function LuaCityWarPlaceBiaoCheConfirmWnd:OnCancelButtonClick()
	self:Close()
end

function LuaCityWarPlaceBiaoCheConfirmWnd:OnOKButtonClick()
	Gac2Gas.RequestSetBiaoCheMap(CLuaCityWarMgr.BiaoChePlaceConfirmInfo.mapId, CLuaCityWarMgr.BiaoChePlaceConfirmInfo.otherMapId)
	self:Close()
end


return LuaCityWarPlaceBiaoCheConfirmWnd
