-- Auto Generated!!
local CPlayerShopBillItem = import "L10.UI.CPlayerShopBillItem"
CPlayerShopBillItem.m_UpdateData_CS2LuaHook = function (this, data)
    if data ~= nil then
        this.m_ItemNameLabel.text = data.ItemName
        this.m_ItemCountLabel.text = tostring(data.ItemCount)
        this.m_MoneyCountLabel.text = tostring(data.TotalPrice)
    end
end
