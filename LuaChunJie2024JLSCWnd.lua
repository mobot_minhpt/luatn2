
LuaChunJie2024JLSCWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaChunJie2024JLSCWnd, "timeLabel", "TimeLabel", Transform)
RegistChildComponent(LuaChunJie2024JLSCWnd, "tipButton", "TipButton", GameObject)
RegistChildComponent(LuaChunJie2024JLSCWnd, "template", "Template", GameObject)
RegistChildComponent(LuaChunJie2024JLSCWnd, "table", "Table", UITable)
--@endregion RegistChildComponent end

function LuaChunJie2024JLSCWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	UIEventListener.Get(self.tipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)

    --@endregion EventBind end
end

function LuaChunJie2024JLSCWnd:Init()

end

--@region UIEvent

function LuaChunJie2024JLSCWnd:OnTipButtonClick()
end

--@endregion UIEvent
