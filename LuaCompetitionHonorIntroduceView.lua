local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"

LuaCompetitionHonorIntroduceView = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaCompetitionHonorIntroduceView, "RuleLabel", UILabel)
RegistChildComponent(LuaCompetitionHonorIntroduceView, "ImageRule", CUITexture)
RegistChildComponent(LuaCompetitionHonorIntroduceView, "ImageLabel", UILabel)
RegistChildComponent(LuaCompetitionHonorIntroduceView, "NextArrow", GameObject)
RegistChildComponent(LuaCompetitionHonorIntroduceView, "LastArrow", GameObject)
RegistChildComponent(LuaCompetitionHonorIntroduceView, "IndicatorTabel", UITable)
RegistChildComponent(LuaCompetitionHonorIntroduceView, "IndicatorTemplate", GameObject)

RegistClassMember(LuaCompetitionHonorIntroduceView, "m_Tick")
RegistClassMember(LuaCompetitionHonorIntroduceView, "m_CompetitionId")
RegistClassMember(LuaCompetitionHonorIntroduceView, "m_IndicatorList")
RegistClassMember(LuaCompetitionHonorIntroduceView, "m_RuleList")
RegistClassMember(LuaCompetitionHonorIntroduceView, "m_RuleIndex")
RegistClassMember(LuaCompetitionHonorIntroduceView, "m_PlayerInfoList")
--@endregion RegistChildComponent end

function LuaCompetitionHonorIntroduceView:Awake()
    UIEventListener.Get(self.NextArrow).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnNextArrowClick()
    end)
    UIEventListener.Get(self.LastArrow).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnLastArrowClick()
    end)
end

function LuaCompetitionHonorIntroduceView:InitView(competitionId)
    self.m_CompetitionId = competitionId
    self:InitRuleList()
    
    -- 规则自动切换
    local time = CompetitionHonor_Setting.GetData().RuleChangeTick
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
    end
    self.m_Tick = RegisterTick(function()
        self:OnNextArrowClick()
    end, time * 1000)
end

function LuaCompetitionHonorIntroduceView:InitRuleList()
    self.RuleLabel.text = tostring(self.m_CompetitionId)
    self.m_RuleList = {}
    for i = 1, 3 do 
        table.insert(self.m_RuleList, {image = "UI/Texture/NonTransparent/Material/xuejingkuanghuan_yindao_01.mat", text = tostring(i)})
    end
    self.m_IndicatorList = {}
    Extensions.RemoveAllChildren(self.IndicatorTabel.transform)
    self.IndicatorTemplate.gameObject:SetActive(false)
    self.m_PageTbl = {}
    for i = 1, #self.m_RuleList do
        local indicator = NGUITools.AddChild(self.IndicatorTabel.gameObject, self.IndicatorTemplate)
        indicator.gameObject:SetActive(true)
        table.insert(self.m_IndicatorList, indicator)
    end    
    self.IndicatorTabel:Reposition()
    self.m_RuleIndex = 1
    self:UpdateImageRule()
end

function LuaCompetitionHonorIntroduceView:UpdateImageRule()
    -- self.ImageRule:LoadMaterial(self.m_RuleList[self.m_RuleIndex].image)
    -- self.ImageLabel.text = CChatLinkMgr.TranslateToNGUIText(self.m_RuleList[self.m_RuleIndex].text)
    -- for i = 1, #self.m_IndicatorList do
    --     self.m_IndicatorList[i].transform:Find("Selected").gameObject:SetActive(i == self.m_RuleIndex)
    --     self.m_IndicatorList[i].transform:Find("NoSelected").gameObject:SetActive(i ~= self.m_RuleIndex)
    -- end
end

--@region UIEvent
function LuaCompetitionHonorIntroduceView:OnNextArrowClick()
    self.m_RuleIndex = self.m_RuleIndex >= #self.m_RuleList and 1 or self.m_RuleIndex + 1
    self:UpdateImageRule()
end
function LuaCompetitionHonorIntroduceView:OnLastArrowClick()
    self.m_RuleIndex = self.m_RuleIndex <= 1 and #self.m_RuleList or self.m_RuleIndex - 1
    self:UpdateImageRule()
end
--@endregion UIEvent

function LuaCompetitionHonorIntroduceView:OnDisable()
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
    end
end