-- local Mall_LingYuMall = import "L10.Game.Mall_LingYuMall"
local EventDelegate = import "EventDelegate"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
-- local DelegateFactory = import "DelegateFactory"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
-- local CommonDefs = import "L10.Game.CommonDefs"
local CItemMgr = import "L10.Game.CItemMgr"
local CEquipmentProcessMgr = import "L10.Game.CEquipmentProcessMgr"
local CEquipmentBaptizeMgr = import "L10.Game.CEquipmentBaptizeMgr"
local CEquipBaptizeJingPing=import "L10.UI.CEquipBaptizeJingPing"
local CCurentMoneyCtrl=import "L10.UI.CCurentMoneyCtrl"


CLuaEquipBaptizeCost = class()
RegistClassMember(CLuaEquipBaptizeCost,"autoCostJade")
RegistClassMember(CLuaEquipBaptizeCost,"jingpingNode")
RegistClassMember(CLuaEquipBaptizeCost,"jadeNode")
RegistClassMember(CLuaEquipBaptizeCost,"jingping")
RegistClassMember(CLuaEquipBaptizeCost,"lingyuCost")
RegistClassMember(CLuaEquipBaptizeCost,"yinliangCost")
RegistClassMember(CLuaEquipBaptizeCost,"jingpingCostNum")
-- RegistClassMember(CLuaEquipBaptizeCost,"<TemplateId>k__BackingField")
RegistClassMember(CLuaEquipBaptizeCost,"TemplateId")

function CLuaEquipBaptizeCost:Awake()
    self.autoCostJade = self.transform:Find("AutoCostJadeToggle/Toggle"):GetComponent(typeof(UIToggle))
    self.jingpingNode = self.transform:Find("JingPingNode")
    self.jadeNode = self.transform:Find("LingYuCost")
    self.jingping = self.transform:Find("JingPingNode"):GetComponent(typeof(CEquipBaptizeJingPing))
    self.lingyuCost = self.transform:Find("LingYuCost"):GetComponent(typeof(CCurentMoneyCtrl))
    self.yinliangCost = self.transform:Find("YinLiangCost"):GetComponent(typeof(CCurentMoneyCtrl))
self.jingpingCostNum = 0
--self.<TemplateId>k__BackingField = ?
self.TemplateId = 0

    self.autoCostJade.value = false
    self.jingpingNode.gameObject:SetActive(true)
    self.jadeNode.gameObject:SetActive(false)

    CommonDefs.ListAdd(self.autoCostJade.onChange, typeof(EventDelegate), CreateFromClass(EventDelegate, DelegateFactory.Callback(function () 
        self:UpdateState()
    end)))
    self.jingping.onCountUpdate = DelegateFactory.Action(function()
        self:UpdateState()
    end)
    -- < MakeDelegateFromCSFunction >(self.UpdateState, Action0, self)
    self.TemplateId = CEquipmentProcessMgr.Inst.baptizeWordCostBaseItemId
    local canUseBindLingyu = CShopMallMgr.CanUseBindJadeBuy(self.TemplateId)
    self.lingyuCost:SetType(canUseBindLingyu and EnumMoneyType.LingYu_WithBind or EnumMoneyType.LingYu, EnumPlayScoreKey.NONE, true)
end
function CLuaEquipBaptizeCost:UpdateState( )
    if not self.autoCostJade.value then
        self.jingpingNode.gameObject:SetActive(true)
        self.jadeNode.gameObject:SetActive(false)
    else
        if self.jingping.count < self.jingpingCostNum then
            self.jingpingNode.gameObject:SetActive(false)
            self.jadeNode.gameObject:SetActive(true)
            self:UpdateLingYuCost()
        else
            self.jingpingNode.gameObject:SetActive(true)
            self.jadeNode.gameObject:SetActive(false)
        end
    end
end

function CLuaEquipBaptizeCost:lingyuEnough()
    return self.lingyuCost.moneyEnough
end
function CLuaEquipBaptizeCost:LingYuCost()
    return self.lingyuCost:GetCost()
end

function CLuaEquipBaptizeCost:isAutoCostJade()
    return self.autoCostJade.value
end

function CLuaEquipBaptizeCost:isJingPingEnough()
    if self.jingping.count>= self.jingpingCostNum then
        return true
    end
    return false
end
function CLuaEquipBaptizeCost:yinliangEnough()
    local equip = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
    local commonItem = CItemMgr.Inst:GetById(equip.itemId)
    if commonItem and commonItem.IsBinded then
        return true
    else
        return self.yinliangCost.moneyEnough
    end
end

function CLuaEquipBaptizeCost:UpdateLingYuCost()
    --净瓶拥有数量
    local haveJingPingNum = CEquipmentProcessMgr.Inst:GetWordCostMatNumInBag()

    local equip = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
    local cost = CEquipmentProcessMgr.Inst:GetBaptizeCost(equip)

    local needJingPingNum = cost and cost.WordCost[1] or 0

    if haveJingPingNum>=needJingPingNum then
        self.lingyuCost:SetCost(0)
    else
        local num = needJingPingNum - haveJingPingNum
        local templateId = CEquipmentProcessMgr.Inst.baptizeWordCostBaseItemId
        local priceData = Mall_LingYuMall.GetData(templateId)
        local price = priceData and priceData.Jade*num or 0
        self.lingyuCost:SetCost(price)
    end
end

function CLuaEquipBaptizeCost:SetBaseInfo( )
    local equip = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
    local equipItem = CEquipmentProcessMgr.Inst.BaptizingEquipment
    local cost = CEquipmentProcessMgr.Inst:GetBaptizeCost(equip)
    if cost ~= nil then
        self.jingpingCostNum = cost.WordCost[1]

        self.TemplateId = cost.WordCost[0]
        local canUseBindLingyu = CShopMallMgr.CanUseBindJadeBuy(self.TemplateId)
        self.lingyuCost:SetType(canUseBindLingyu and EnumMoneyType.LingYu_WithBind or EnumMoneyType.LingYu, EnumPlayScoreKey.NONE, true)

        local costJade = Mall_LingYuMall.GetData(cost.WordCost[0])
        self.jingping:UpdateCount()
        self:UpdateLingYuCost()
        local commonItem = CItemMgr.Inst:GetById(equip.itemId)

        local tc = EquipmentTemplate_Equip.GetData(commonItem.TemplateId).TC
        local yinliang = CEquipmentBaptizeMgr.Inst:GetBaptizeWordYinLiangCost(tc, EnumToInt(commonItem.Equip.Color))
        self.yinliangCost:SetCost(yinliang)
        self.yinliangCost.gameObject:SetActive(true)
        --看是否绑定
        if commonItem.IsBinded then
            self.yinliangCost:SetCost(0)
        else
            self.yinliangCost:SetCost(yinliang)
        end
    end
end

