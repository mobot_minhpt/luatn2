local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CUITexture = import "L10.UI.CUITexture"
local CUIFx = import "L10.UI.CUIFx"
local UILabel = import "UILabel"
local CLoginMgr = import "L10.Game.CLoginMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"

LuaQueQiaoXianQuResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaQueQiaoXianQuResultWnd, "ShareBtn", "ShareBtn", GameObject)
RegistChildComponent(LuaQueQiaoXianQuResultWnd, "PlayerInfoNode", "PlayerInfoNode", GameObject)
RegistChildComponent(LuaQueQiaoXianQuResultWnd, "CloseBtn", "CloseBtn", GameObject)
RegistChildComponent(LuaQueQiaoXianQuResultWnd, "Success", "Success", GameObject)
RegistChildComponent(LuaQueQiaoXianQuResultWnd, "Failed", "Failed", GameObject)
RegistChildComponent(LuaQueQiaoXianQuResultWnd, "MyIcon", "MyIcon", GameObject)
RegistChildComponent(LuaQueQiaoXianQuResultWnd, "TeammateIcon", "TeammateIcon", GameObject)

--@endregion RegistChildComponent end

RegistClassMember(LuaQueQiaoXianQuResultWnd, "m_ShareTick")
RegistClassMember(LuaQueQiaoXianQuResultWnd, "m_fx")

function LuaQueQiaoXianQuResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	local onShareClick = function(go)
		self:SetOpBtnVisible(false)
		if self.m_ShareTick then
		  UnRegisterTick(self.m_ShareTick)
		  self.m_ShareTick = nil
		end
		self.m_ShareTick = RegisterTickWithDuration(function ()
		  self:SetOpBtnVisible(true)
		end, 1000, 1000)
		CUICommonDef.CaptureScreenAndShare()
	  end
	  CommonDefs.AddOnClickListener(self.ShareBtn,DelegateFactory.Action_GameObject(onShareClick),false)
    --@endregion EventBind end
	if CommonDefs.IS_VN_CLIENT then
		self.ShareBtn:SetActive(false)
	end
end

function LuaQueQiaoXianQuResultWnd:Init()
	self.m_fx = self.transform:Find("Anchor/Info/Result/fx"):GetComponent(typeof(CUIFx))
	self.m_fx:DestroyFx()
	self.Success:SetActive(LuaQiXi2021Mgr.playResult)
	self.Failed:SetActive(not LuaQiXi2021Mgr.playResult)
	if LuaQiXi2021Mgr.playResult then
		self.m_fx:LoadFx("Fx/UI/Prefab/UI_queqiao_chenggong.prefab")
	else
		self.m_fx:LoadFx("Fx/UI/Prefab/UI_queqiao_shibai.prefab")
	end
	self:InitPlayerInfo()
	self:InitMe()
	self:InitPartner()
end

function LuaQueQiaoXianQuResultWnd:InitPartner()
	if LuaQiXi2021Mgr.playResultParnter then
		self.TeammateIcon.transform:Find("Name"):GetComponent(typeof(UILabel)).text = LuaQiXi2021Mgr.playResultParnter.name
		self.TeammateIcon:GetComponent(typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(LuaQiXi2021Mgr.playResultParnter.class, LuaQiXi2021Mgr.playResultParnter.gender, -1), false)
	end
end

function LuaQueQiaoXianQuResultWnd:InitMe()
	self.MyIcon.transform:Find("Name"):GetComponent(typeof(UILabel)).text = LuaQiXi2021Mgr.MyInfo.Name
	self.MyIcon:GetComponent(typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(LuaQiXi2021Mgr.MyInfo.Class, LuaQiXi2021Mgr.MyInfo.Gender, -1), false)
end

function LuaQueQiaoXianQuResultWnd:InitPlayerInfo()
	if CLoginMgr.Inst then
		local myGameServer = CLoginMgr.Inst:GetSelectedGameServer()
		CommonDefs.GetComponent_Component_Type(self.PlayerInfoNode.transform:Find("Server"), typeof(UILabel)).text = myGameServer.name
	end

	if CClientMainPlayer.Inst then
		CommonDefs.GetComponent_Component_Type(self.PlayerInfoNode.transform:Find("Name"), typeof(UILabel)).text = CClientMainPlayer.Inst.Name
		CommonDefs.GetComponent_Component_Type(self.PlayerInfoNode.transform:Find("Icon/Lv"), typeof(UILabel)).text = tostring(CClientMainPlayer.Inst.Level)
		CommonDefs.GetComponent_Component_Type(self.PlayerInfoNode.transform:Find("Icon"), typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(CClientMainPlayer.Inst.Class, CClientMainPlayer.Inst.Gender, -1), false)
		CommonDefs.GetComponent_Component_Type(self.PlayerInfoNode.transform:Find("ID"), typeof(UILabel)).text = tostring(CClientMainPlayer.Inst.Id)
		CommonDefs.GetComponent_Component_Type(self.PlayerInfoNode.transform:Find("Server"), typeof(UILabel)).text = CClientMainPlayer.Inst:GetMyServerName()
	elseif LuaQiXi2021Mgr.MyInfo.Name then
		CommonDefs.GetComponent_Component_Type(self.PlayerInfoNode.transform:Find("Name"), typeof(UILabel)).text = LuaQiXi2021Mgr.MyInfo.Name
		CommonDefs.GetComponent_Component_Type(self.PlayerInfoNode.transform:Find("Icon/Lv"), typeof(UILabel)).text = tostring(LuaQiXi2021Mgr.MyInfo.Lv)
		CommonDefs.GetComponent_Component_Type(self.PlayerInfoNode.transform:Find("Icon"), typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(LuaQiXi2021Mgr.MyInfo.Class, LuaQiXi2021Mgr.MyInfo.Gender, -1), false)
		CommonDefs.GetComponent_Component_Type(self.PlayerInfoNode.transform:Find("ID"), typeof(UILabel)).text = tostring(LuaQiXi2021Mgr.MyInfo.Id)
		CommonDefs.GetComponent_Component_Type(self.PlayerInfoNode.transform:Find("Server"), typeof(UILabel)).text = LuaQiXi2021Mgr.MyInfo.ServerName
	end
end

function LuaQueQiaoXianQuResultWnd:SetOpBtnVisible(sign)
	self.CloseBtn:SetActive(sign)
	self.ShareBtn:SetActive(sign)
end
--@region UIEvent

function LuaQueQiaoXianQuResultWnd:OnDisable()
	if self.m_ShareTick~=nil then UnRegisterTick(self.m_ShareTick) end
end

--@endregion UIEvent

