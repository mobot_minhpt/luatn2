-- Auto Generated!!
local CYuanDanQiangDanRankItem = import "L10.UI.CYuanDanQiangDanRankItem"
local LocalString = import "LocalString"
CYuanDanQiangDanRankItem.m_Init_CS2LuaHook = function (this, playerInfo, isMe) 
    local default
    if isMe then
        default = this.selfColorStr
    else
        default = this.notSelfColorStr
    end
    local textColor = default
    this.playerNameLabel.text = System.String.Format("{0}{1}[-]", textColor, playerInfo.playerName)
    this.roundTimesLabel.text = System.String.Format(LocalString.GetString("{0}第{1}场[-]"), textColor, playerInfo.roundTimes)
    this.eggIcon:SetActive(playerInfo.ownEgg)
    this.playerNameLabel.color = playerInfo.alive and this.aliveColor or this.deadColor
    this.roundTimesLabel.color = playerInfo.alive and this.aliveColor or this.deadColor
end
