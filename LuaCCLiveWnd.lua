require("common/common_include")

local CBaseWnd = import "L10.UI.CBaseWnd"
local DelegateFactory = import "DelegateFactory"
local CommonDefs = import "L10.Game.CommonDefs"
local LocalString = import "LocalString"
local UITexture = import "UITexture"
local UILabel = import "UILabel"
local UISprite = import "UISprite"
local CCCChatMgr = import "L10.Game.CCCChatMgr"
local CCPlayerCtrl = import "L10.Game.CCPlayerCtrl"
local CCPlayer = import "CCPlayer"
local Texture2D = import "UnityEngine.Texture2D"
local TextureFormat = import "UnityEngine.TextureFormat"
local Constants = import "L10.Game.Constants"
local CPopupMenuInfoMgrAlignType = import "L10.UI.CPopupMenuInfoMgr+AlignType"
local UIWidget = import "UIWidget"
local LuaTweenUtils = import "LuaTweenUtils"
local Ease = import "DG.Tweening.Ease"
local CUIManager = import "L10.UI.CUIManager"
local UIRoot = import "UIRoot"
local Screen = import "UnityEngine.Screen"
local UITable = import "UITable"
local CCommonDanMuCtrl = import "L10.UI.CCommonDanMuCtrl"
local UIInput = import "UIInput"
local CChatHelper = import "L10.UI.CChatHelper"
local EChatPanel = import "L10.Game.EChatPanel"

local PlayerSettings = import "L10.Game.PlayerSettings"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local CCommonSelector = import "L10.UI.CCommonSelector"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"
local Time = import "UnityEngine.Time"
local SoundManager = import "SoundManager"
local CTickMgr = import "L10.Engine.CTickMgr"
local ETickType = import "L10.Engine.ETickType"
local CVoiceMgr = import "L10.Game.CVoiceMgr"
local CWebBrowserMgr = import "L10.Game.CWebBrowserMgr"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CJoinCCChannelRequestSourceInfo = import "L10.Game.CJoinCCChannelRequestSourceInfo"
local CSpeechCtrlMgr = import "L10.Game.CSpeechCtrlMgr"
local CSpeechCtrlType = import "L10.Game.CSpeechCtrlType"
local EnumCCMiniStreamType = import "L10.Game.EnumCCMiniStreamType"

LuaCCLiveWnd = class()

RegistChildComponent(LuaCCLiveWnd , "m_JingCaiButton","JingCaiButton", GameObject)

RegistClassMember(LuaCCLiveWnd,"m_closeBtn")
RegistClassMember(LuaCCLiveWnd,"m_AnchorBg")
RegistClassMember(LuaCCLiveWnd,"m_MaskBg")
RegistClassMember(LuaCCLiveWnd,"m_PlayerCtrl")
RegistClassMember(LuaCCLiveWnd,"m_Indicator")
RegistClassMember(LuaCCLiveWnd,"m_IndicatorLabel")
RegistClassMember(LuaCCLiveWnd,"m_PlayPanel")
RegistClassMember(LuaCCLiveWnd,"m_PlayButton")
RegistClassMember(LuaCCLiveWnd,"m_PlayIcon")
RegistClassMember(LuaCCLiveWnd,"m_ResolutionSelector")
RegistClassMember(LuaCCLiveWnd,"m_ZhuboPortrait")
RegistClassMember(LuaCCLiveWnd,"m_ZhuboNameLabel")
RegistClassMember(LuaCCLiveWnd,"m_LiveNameLabel")
RegistClassMember(LuaCCLiveWnd,"m_FollowingButton")
RegistClassMember(LuaCCLiveWnd,"m_DanMuButton")
RegistClassMember(LuaCCLiveWnd,"m_MsgButton")
RegistClassMember(LuaCCLiveWnd,"m_SwitchButton")
RegistClassMember(LuaCCLiveWnd,"m_ShareButton")
RegistClassMember(LuaCCLiveWnd,"m_ExpandButton")


RegistClassMember(LuaCCLiveWnd,"m_SendBar")
RegistClassMember(LuaCCLiveWnd,"m_SendButton")
RegistClassMember(LuaCCLiveWnd,"m_BackButton")
RegistClassMember(LuaCCLiveWnd,"m_Input")

RegistClassMember(LuaCCLiveWnd,"m_LastChangeVbrTime")
RegistClassMember(LuaCCLiveWnd,"m_HidePlayPanelTick")

RegistClassMember(LuaCCLiveWnd,"m_CCLogo")
RegistClassMember(LuaCCLiveWnd,"m_CCActivityTexture")

RegistClassMember(LuaCCLiveWnd,"m_CtrlRoot")
RegistClassMember(LuaCCLiveWnd,"m_ButtonTable")
RegistClassMember(LuaCCLiveWnd,"m_FollowingIcon")
RegistClassMember(LuaCCLiveWnd,"m_ExpandIcon")
RegistClassMember(LuaCCLiveWnd,"m_DanMuCtrl")
RegistClassMember(LuaCCLiveWnd,"m_LastPlayURL")
RegistClassMember(LuaCCLiveWnd,"cacheTex")
RegistClassMember(LuaCCLiveWnd,"m_IsNormalSize")
RegistClassMember(LuaCCLiveWnd,"m_IsPlaying")
RegistClassMember(LuaCCLiveWnd,"m_CCActivityInfo")
RegistClassMember(LuaCCLiveWnd,"m_HideCCActivityTick")
RegistClassMember(LuaCCLiveWnd,"m_JingCaiButtonState")

function LuaCCLiveWnd:Awake()
    self.m_JingCaiButton:SetActive(false)
	UIEventListener.Get(self.m_JingCaiButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
		self:OnJingCaiButtonClick()
	end)
	Gac2Gas.QueryAttachJingCaiButton()
end

function LuaCCLiveWnd:OnJingCaiButtonClick()
	CUIManager.ShowUI(CLuaUIResources.StarBiwuJingCaiWnd)
end

function LuaCCLiveWnd:OnJingCaiButtonUpdateState(state)
	self.m_JingCaiButtonState = state
	self.m_JingCaiButton:SetActive(state)
	self.m_ButtonTable:Reposition()
end

function LuaCCLiveWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaCCLiveWnd:Init()
	self.m_closeBtn = self.transform:Find("Anchor/Texture/CloseButton").gameObject
	CommonDefs.AddOnClickListener(self.m_closeBtn, DelegateFactory.Action_GameObject(function(go) self:Close() end), false)

	self.m_AnchorBg = self.transform:Find("Anchor/Texture"):GetComponent(typeof(UIWidget))
	self.m_MaskBg = self.transform:Find("Anchor/Mask").gameObject
	self.m_PlayerCtrl = self.transform:Find("Anchor/Texture/Texture"):GetComponent(typeof(CCPlayerCtrl))

	self.m_Indicator = self.transform:Find("Anchor/Texture/Indicator").gameObject
	self.m_IndicatorLabel = self.transform:Find("Anchor/Texture/Indicator/TextLabel"):GetComponent(typeof(UILabel))

	self.m_PlayPanel = self.transform:Find("Anchor/Texture/PlayPanel").gameObject
	self.m_PlayButton = self.transform:Find("Anchor/Texture/PlayPanel/Container/PlayButton").gameObject
	self.m_PlayIcon = self.transform:Find("Anchor/Texture/PlayPanel/Container/PlayButton/PlayIcon"):GetComponent(typeof(UISprite))
	self.m_ResolutionSelector = self.transform:Find("Anchor/Texture/PlayPanel/ResolutionSelector"):GetComponent(typeof(CCommonSelector))

	self.m_ZhuboPortrait =  self.transform:Find("Anchor/Texture/BottomPanel/CtrlRoot/Container/Head/Portrait"):GetComponent(typeof(UITexture))
	self.m_ZhuboNameLabel = self.transform:Find("Anchor/Texture/BottomPanel/CtrlRoot/Container/ZhuboNameLabel"):GetComponent(typeof(UILabel))
	self.m_LiveNameLabel = self.transform:Find("Anchor/Texture/BottomPanel/CtrlRoot/Container/LiveNameLabel"):GetComponent(typeof(UILabel))

	self.m_CtrlRoot = self.transform:Find("Anchor/Texture/BottomPanel/CtrlRoot").gameObject
	self.m_SendBar = self.transform:Find("Anchor/Texture/BottomPanel/SendBar").gameObject
	self.m_ButtonTable = self.transform:Find("Anchor/Texture/BottomPanel/CtrlRoot/Table"):GetComponent(typeof(UITable))
	self.m_FollowingButton = self.transform:Find("Anchor/Texture/BottomPanel/CtrlRoot/Table/FollowingButton").gameObject
	self.m_DanMuButton = self.transform:Find("Anchor/Texture/BottomPanel/CtrlRoot/Table/DanMuButton").gameObject
	self.m_MsgButton = self.transform:Find("Anchor/Texture/BottomPanel/CtrlRoot/Table/MsgButton").gameObject
	self.m_SwitchButton = self.transform:Find("Anchor/Texture/BottomPanel/CtrlRoot/Table/SwitchButton").gameObject
	self.m_ShareButton = self.transform:Find("Anchor/Texture/BottomPanel/CtrlRoot/Table/ShareButton").gameObject
	self.m_ExpandButton = self.transform:Find("Anchor/Texture/BottomPanel/CtrlRoot/Table/ExpandButton").gameObject

	self.m_FollowingIcon = self.transform:Find("Anchor/Texture/BottomPanel/CtrlRoot/Table/FollowingButton/Icon"):GetComponent(typeof(UISprite))
	self.m_ExpandIcon = self.transform:Find("Anchor/Texture/BottomPanel/CtrlRoot/Table/ExpandButton/Icon"):GetComponent(typeof(UISprite))

	self.m_DanMuCtrl = self.transform:Find("DanMu"):GetComponent(typeof(CCommonDanMuCtrl))
	self.m_SendButton = self.transform:Find("Anchor/Texture/BottomPanel/SendBar/SendButton").gameObject
	self.m_BackButton = self.transform:Find("Anchor/Texture/BottomPanel/SendBar/BackButton").gameObject
	self.m_Input = self.transform:Find("Anchor/Texture/BottomPanel/SendBar/Input"):GetComponent(typeof(UIInput))

	self.m_CCLogo = self.transform:Find("Anchor/Texture/CCLogo").gameObject
	self.m_CCLogo:SetActive(self:EnableCCLogo())
	self.m_CCActivityTexture = self.transform:Find("Anchor/Texture/ActivityTexture"):GetComponent(typeof(UITexture))
	CommonDefs.AddOnClickListener(self.m_PlayerCtrl.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnVideoTextureClick() end), false)
	--两个对象处理拖动
	CommonDefs.AddOnDragListener(self.m_PlayerCtrl.gameObject, DelegateFactory.Action_GameObject_Vector2(function(go, delta) self:OnDragWnd(go, delta) end), false)
	CommonDefs.AddOnDragListener(self.m_PlayPanel, DelegateFactory.Action_GameObject_Vector2(function(go, delta) self:OnDragWnd(go, delta) end), false)
	CommonDefs.AddOnClickListener(self.m_PlayPanel, DelegateFactory.Action_GameObject(function(go) self:OnPlayPanelClick() end), false)
	CommonDefs.AddOnClickListener(self.m_PlayButton.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnPlayButtonClick() end), false)
	CommonDefs.AddOnClickListener(self.m_FollowingButton, DelegateFactory.Action_GameObject(function(go) self:OnFollowingButtonClick() end), false)
	CommonDefs.AddOnClickListener(self.m_DanMuButton, DelegateFactory.Action_GameObject(function(go) self:OnDanMuButtonClick() end), false)
	CommonDefs.AddOnClickListener(self.m_MsgButton, DelegateFactory.Action_GameObject(function(go) self:OnMsgButtonClick() end), false)
	CommonDefs.AddOnClickListener(self.m_SwitchButton, DelegateFactory.Action_GameObject(function(go) self:OnSwitchButtonClick() end), false)
	CommonDefs.AddOnClickListener(self.m_ShareButton, DelegateFactory.Action_GameObject(function(go) self:OnShareButtonClick() end), false)
	CommonDefs.AddOnClickListener(self.m_ExpandButton, DelegateFactory.Action_GameObject(function(go) self:OnExpandButonClick() end), false)

	CommonDefs.AddOnClickListener(self.m_SendButton, DelegateFactory.Action_GameObject(function(go) self:OnSendButtonClick() end), false)
	CommonDefs.AddOnClickListener(self.m_BackButton, DelegateFactory.Action_GameObject(function(go) self:OnBackButtonClick() end), false)

	CommonDefs.AddOnClickListener(self.m_CCLogo, DelegateFactory.Action_GameObject(function(go) self:OnCCLogoClick() end), false)
	CommonDefs.AddOnClickListener(self.m_CCActivityTexture.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnCCActivityTextureClick() end), false)

	self.m_DanMuCtrl.ChatLinkSupported = true
	self.m_PlayPanel:SetActive(false)

	self.m_PlayerCtrl.OnVbrInfoUpdated = DelegateFactory.Action_string_string(function(val, vbrlist)
		self:OnVbrInfoUpdated(val, vbrlist)
	end)
	self.m_PlayerCtrl.OnBeginPlay = DelegateFactory.Action(function()
		self:OnBeginPlay()
	end)
	self.m_ResolutionSelector:SetPopupAlignType(CPopupMenuInfoMgrAlignType.Top)
	self.m_ResolutionSelector:SetMenuItemStyle(EnumPopupMenuItemStyle.Light)
	self.m_ResolutionSelector:SetShowDefaultSelectedItem(true)
	self.m_ResolutionSelector:SetName("")

	self:PlayVideo()

	self:InitZhuboInfo()
	self:InitCCFollowingInfo()

	if self.m_IsNormalSize == nil then
		self.m_IsNormalSize = true
		self:InitPlayerSize()
	end
	if not self.m_IsPlaying then
		self.m_IsPlaying = true
		self.m_PlayIcon.spriteName = Constants.Common_Pause_Icon
	end
end

function LuaCCLiveWnd:PlayVideo()
	if self.m_LastPlayURL and self.m_LastPlayURL == CCCChatMgr.Inst.CCLiveURL then
		return
	end
	self.m_LastPlayURL = CCCChatMgr.Inst.CCLiveURL
	self.m_Indicator:SetActive(true)
	self.m_IndicatorLabel.text = g_MessageMgr:FormatMessage("ZHIBO_LOADING")
	self.m_PlayerCtrl:Play(self.m_LastPlayURL)
end

function LuaCCLiveWnd:OnVbrInfoUpdated(val, vbrlist)
	self.m_ResolutionSelector:SetName(CCPlayerCtrl.GetVbr(val))
	local array = self.m_PlayerCtrl:GetVbrList()
	local textTbl = {}
	if array then
		for i=0,array.Length-1 do
			table.insert(textTbl, CCPlayerCtrl.GetVbr(array[i]))
		end
	end
	local textList = InitializeListWithArray(CreateFromClass(MakeGenericClass(List, String)), String,  Table2Array(textTbl, MakeArrayClass(String)))

	self.m_ResolutionSelector:Init(textList, DelegateFactory.Action_int(function ( index )
		if index>=0 and index < array.Length then
			if not self.m_LastChangeVbrTime  or Time.realtimeSinceStartup - self.m_LastChangeVbrTime > 3 then
				self.m_LastChangeVbrTime = Time.realtimeSinceStartup
				self.m_ResolutionSelector:SetName(textList[index])
				self.m_PlayerCtrl:SetVbrName(array[index])
			else
				g_MessageMgr:ShowMessage("Change_Vbr_Too_Frequently")
			end

		end
	end))
end

function LuaCCLiveWnd:OnBeginPlay()
	if self.m_Indicator then
		self.m_Indicator:SetActive(false)
	end
	self.m_IsPlaying = self.m_PlayerCtrl:IsPlaying()
	self:RefreshPlayIcon()
end

function LuaCCLiveWnd:InitZhuboInfo()
	local info = CCCChatMgr.Inst:GetCurrentStation_V2()
	if info then
		if not self.cacheTex then
			self.cacheTex = Texture2D(128, 128, TextureFormat.RGBA32, false)
			self.m_ZhuboPortrait.mainTexture = nil
		end
		self.m_ZhuboNameLabel.text = info.liveInfo.nickName
		self.m_LiveNameLabel.text = info.liveInfo.title

        local bytes = CCCChatMgr.Inst:GetCCPortraitImage(info.liveInfo.purl)
        if bytes then
            CommonDefs.LoadImage(self.cacheTex, bytes)
            self.m_ZhuboPortrait.mainTexture = self.cacheTex
        else
            CCCChatMgr.Inst:DownloadCCPortraitImage(info.liveInfo.purl)
        end
	end
	self:InitCCActivityInfo(true)
end

function LuaCCLiveWnd:InitCCFollowingInfo()
	local info = CCCChatMgr.Inst:GetCurrentStation_V2()
	local isFollowing = false
	if info and CCCChatMgr.Inst:IsCCFollowing(info.liveInfo.ccid) then
		self.m_FollowingIcon.spriteName = Constants.CCFollowingIcon
	else
		self.m_FollowingIcon.spriteName = Constants.CCUnFollowingIcon
    end
end

function LuaCCLiveWnd:InitPlayerSize()
	if self.m_IsNormalSize then
		self.m_AnchorBg.width = 1582
		self.m_MaskBg:SetActive(true)
		LuaTweenUtils.DOKill(self.m_AnchorBg.transform, false)
		LuaTweenUtils.SetEase(LuaTweenUtils.TweenPosition(self.m_AnchorBg.transform, 0, 38, 0, 0.2), Ease.OutCubic)

		self.m_ShareButton:SetActive(true)
		self.m_ZhuboNameLabel.enabled = true
		self.m_LiveNameLabel.enabled = true
		self.m_ButtonTable:Reposition()

		self.m_ExpandIcon.spriteName = Constants.CommonCollapseIcon
		self.m_CCActivityTexture.transform.localScale = Vector3(1,1,1)
	else
		self.m_AnchorBg.width = 628
		self.m_MaskBg:SetActive(false)
		LuaTweenUtils.DOKill(self.m_AnchorBg.transform, false)
		local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject)
        local virtualScreenWidth = Screen.width * CUIManager.UIMainCamera.rect.width * scale
		LuaTweenUtils.SetEase(LuaTweenUtils.TweenPosition(self.m_AnchorBg.transform, self.m_AnchorBg.width*0.5-virtualScreenWidth*0.5, 38, 0, 0.2), Ease.OutCubic)
	
		self.m_ShareButton:SetActive(false)
		self.m_ZhuboNameLabel.enabled = false
		self.m_LiveNameLabel.enabled = false
		self.m_ButtonTable:Reposition()

		self.m_ExpandIcon.spriteName = Constants.CommonExpandIcon
		self.m_CCActivityTexture.transform.localScale = Vector3(0.5,0.5,1)
	end
end

function LuaCCLiveWnd:RefreshPlayIcon()
	if self.m_IsPlaying then
		self.m_PlayIcon.spriteName =  Constants.Common_Pause_Icon
	else 
		self.m_PlayIcon.spriteName =  Constants.Common_Play_Icon
	end
end
-------------------
-- 控制播放面板的自动隐藏
-------------------
function LuaCCLiveWnd:StartHidePlayPanelTick()
	self:CancelHidePlayPanelTick()
	self.m_HidePlayPanelTick = CTickMgr.Register(DelegateFactory.Action(function ()
		self.m_HidePlayPanelTick = nil
		self.m_PlayPanel:SetActive(false)
	end), 3 * 1000, ETickType.Once)
end

function LuaCCLiveWnd:CancelHidePlayPanelTick()
	if self.m_HidePlayPanelTick then
		invoke(self.m_HidePlayPanelTick)
		self.m_HidePlayPanelTick = nil
	end
end

function LuaCCLiveWnd:OnEnable()
	SoundManager.Inst:ChangeBgMusicAndSoundVolume("ccliveopening", 0, false)
	g_ScriptEvent:AddListener("OnZhuboPortraitReady", self, "OnZhuboPortraitReady")
	g_ScriptEvent:AddListener("OnCCFollowingUpdate", self, "OnCCFollowingUpdate")
	g_ScriptEvent:AddListener("OnRecvMsgFromCC", self, "OnRecvMsgFromCC")
	g_ScriptEvent:AddListener("OnZhuboStationInfoUpdate", self, "OnZhuboStationInfoUpdate")
	g_ScriptEvent:AddListener("OnCCLiveWndJingCaiButtonUpdateState", self, "OnJingCaiButtonUpdateState")
	g_ScriptEvent:AddListener("OnGetCCLiveActivityInfo", self, "OnGetCCLiveActivityInfo")
end

function LuaCCLiveWnd:OnDisable()
	SoundManager.Inst:ChangeBgMusicAndSoundVolume("ccliveopening", 0, true)
	self:CancelHidePlayPanelTick()
	self:StopHideCCActivityTick()
 	g_ScriptEvent:RemoveListener("OnZhuboPortraitReady", self, "OnZhuboPortraitReady")
 	g_ScriptEvent:RemoveListener("OnCCFollowingUpdate", self, "OnCCFollowingUpdate")
 	g_ScriptEvent:RemoveListener("OnRecvMsgFromCC", self, "OnRecvMsgFromCC")
 	g_ScriptEvent:RemoveListener("OnZhuboStationInfoUpdate", self, "OnZhuboStationInfoUpdate")
	g_ScriptEvent:RemoveListener("OnCCLiveWndJingCaiButtonUpdateState", self, "OnJingCaiButtonUpdateState")
	g_ScriptEvent:RemoveListener("OnGetCCLiveActivityInfo", self, "OnGetCCLiveActivityInfo")
end

function LuaCCLiveWnd:OnVideoTextureClick()
	if self.m_PlayPanel.activeSelf then
		self:CancelHidePlayPanelTick()
		self.m_PlayPanel:SetActive(false)
	else
		self.m_PlayPanel:SetActive(true)
		self:StartHidePlayPanelTick()
	end
end

function LuaCCLiveWnd:OnDragWnd(go, delta)
	if self.m_IsNormalSize then return end

	local leftSpace = 100

	local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject)
    local virtualScreenWidth = Screen.width * CUIManager.UIMainCamera.rect.width * scale
    local virtualScreenHeight = Screen.height * scale

    local v = self.m_AnchorBg.transform.localPosition
    v.x = v.x + delta.x * CUIManager.UIMainCamera.rect.width * scale
    v.y = v.y + delta.y * scale

    --检查左右边界
    if v.x + self.m_AnchorBg.width * 0.5 + virtualScreenWidth * 0.5 < leftSpace then
    	v.x = - virtualScreenWidth * 0.5 + leftSpace - self.m_AnchorBg.width * 0.5
    elseif virtualScreenWidth * 0.5 - (v.x - self.m_AnchorBg.width * 0.5) < leftSpace then
    	v.x = virtualScreenWidth * 0.5 - leftSpace + self.m_AnchorBg.width * 0.5
    end
    --检查上下边界
    if virtualScreenHeight * 0.5 - (v.y - self.m_AnchorBg.height * 0.5) < leftSpace then
    	v.y = virtualScreenHeight * 0.5 - leftSpace + self.m_AnchorBg.height * 0.5
    elseif v.y + self.m_AnchorBg.height * 0.5 + virtualScreenHeight * 0.5 < leftSpace then
    	v.y = -virtualScreenHeight * 0.5 + leftSpace - self.m_AnchorBg.height * 0.5
    end

    self.m_AnchorBg.transform.localPosition = v
end

function LuaCCLiveWnd:OnPlayPanelClick()
	self:StartHidePlayPanelTick() --重置计时
end

function LuaCCLiveWnd:OnPlayButtonClick()
	if self.m_IsPlaying then
		self.m_IsPlaying = false
		self.m_PlayerCtrl:Pause()
	else
		self.m_IsPlaying = true
		self.m_PlayerCtrl:Resume()
	end
	self:RefreshPlayIcon()
	self:StartHidePlayPanelTick() --重置计时
end

function LuaCCLiveWnd:OnFollowingButtonClick()
	local info = CCCChatMgr.Inst:GetCurrentStation_V2()
	if info then
		CCCChatMgr.Inst:ChangeCCFollowing(info.liveInfo.ccid)
	end
end

function LuaCCLiveWnd:OnDanMuButtonClick()
	local selectAction = DelegateFactory.Action_int(function (index)
		self:OnSelectDanMuType(index)
	end)

	local danmuType = PlayerSettings.CCLiveDanMuType

    local item1 = PopupMenuItemData(LocalString.GetString("上方弹幕"), selectAction, false, nil, EnumPopupMenuItemStyle.Light)
    local item2 = PopupMenuItemData(LocalString.GetString("全屏弹幕"), selectAction, false, nil, EnumPopupMenuItemStyle.Light)
    local item3 = PopupMenuItemData(LocalString.GetString("无弹幕"), selectAction, false, nil, EnumPopupMenuItemStyle.Light)
    local tbl = {item1, item2, item3}
    local array = Table2Array(tbl, MakeArrayClass(PopupMenuItemData))
    CPopupMenuInfoMgr.ShowPopupMenuWithDefaultSelectIndex(array, danmuType, self.m_DanMuButton.transform, CPopupMenuInfoMgrAlignType.Top)
end

function LuaCCLiveWnd:OnSelectDanMuType(index)
	if index>=0 and index<=2 then
		PlayerSettings.CCLiveDanMuType = index
		if index == 2 then
			self.m_DanMuCtrl:ClearBullets()
		end
	end
end

function LuaCCLiveWnd:OnMsgButtonClick()
	self.m_CtrlRoot:SetActive(false)
	self.m_SendBar:SetActive(true)
end

function LuaCCLiveWnd:OnSwitchButtonClick()
	local info = CCCChatMgr.Inst:GetCurrentStation_V2()
	if info then
		CCCChatMgr.Inst:ChangeToCCAudioMode(info.liveInfo.ccid,CJoinCCChannelRequestSourceInfo.ZhiboChannel)
	end
end

function LuaCCLiveWnd:OnShareButtonClick()
	local info = CCCChatMgr.Inst:GetCurrentStation_V2()
	if info then
		local ccid = info.liveInfo.ccid
        local name = info.liveInfo.nickName
        CCCChatMgr.Inst:ShareZhubo(self.m_ShareButton, ccid, name, CPopupMenuInfoMgrAlignType.Top)
	end
end

function LuaCCLiveWnd:OnExpandButonClick()
	if self.m_IsNormalSize then
		self.m_IsNormalSize = false
	else
		self.m_IsNormalSize = true
	end
	self.m_JingCaiButton.gameObject:SetActive(self.m_IsNormalSize and self.m_JingCaiButtonState)
	self:InitPlayerSize()
end

function LuaCCLiveWnd:OnSendButtonClick()
	local val = self.m_Input.value
	if val==nil or val == "" then 
		return
	end
	CChatHelper.SendMsg(EChatPanel.CC, val, 0)
	self.m_Input.value = ""
end

function LuaCCLiveWnd:OnBackButtonClick()
	self.m_CtrlRoot:SetActive(true)
	self.m_SendBar:SetActive(false)
end

function LuaCCLiveWnd:OnZhuboPortraitReady(args)
	local url = args[0]
	local info = CCCChatMgr.Inst:GetCurrentStation_V2()
	if info and info.liveInfo.purl == url then
		local bytes = CCCChatMgr.Inst:GetCCPortraitImage(info.liveInfo.purl)
        if bytes then
            CommonDefs.LoadImage(self.cacheTex, bytes)
            self.m_ZhuboPortrait.mainTexture = self.cacheTex
        end
	end
end

function LuaCCLiveWnd:OnCCFollowingUpdate(args)
	self:InitCCFollowingInfo()
end

function LuaCCLiveWnd:OnRecvMsgFromCC(args)
	if CSpeechCtrlMgr.Inst:CheckIfNeedSpeechCtrl(CSpeechCtrlType.Type_CC_Live_DanMu, false) then
		return
	end
	local msg = args[0]
	if CVoiceMgr.Inst:IsVoice(msg) then
		return --弹幕跳过语音消息
	end
	local danmuType = PlayerSettings.CCLiveDanMuType
	if danmuType == 0 then
		self.m_DanMuCtrl:AddBullet(msg, true)
	elseif danmuType == 1 then
		self.m_DanMuCtrl:AddBullet(msg, false)
	else
		--discard
	end
end

function LuaCCLiveWnd:OnZhuboStationInfoUpdate(args)
	local info = CCCChatMgr.Inst:GetCurrentStation_V2()
	if info and info.liveInfo.isLive then
		self:InitZhuboInfo()
		self:PlayVideo()
	else
		g_MessageMgr:ShowMessage("ZHUBO_XIABO")
		self:Close()
	end
end

function LuaCCLiveWnd:EnableCCLogo()
	return true
end

function LuaCCLiveWnd:OnCCLogoClick()
	local info = CCCChatMgr.Inst:GetCurrentStation_V2()
	if info and info.liveInfo.isLive then
		if CCPlayer.CCPlayerPluginOpenCCAppWithRoomId(info.roomid, info.cid) then
			return
		end
	end


	local msg = g_MessageMgr:FormatMessage("CC_TIAOZHUAN_WBEPAGE")
	MessageWndManager.ShowOKCancelMessage(msg, 
		DelegateFactory.Action(function() self:OpenCCPage() end),
		nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
end

function LuaCCLiveWnd:OpenCCPage()
	local info = CCCChatMgr.Inst:GetCurrentStation_V2()
	if info and info.liveInfo.isLive then
		CWebBrowserMgr.Inst:OpenUrl(SafeStringFormat3("https://h5.cc.163.com/cc/%d?hide=2&from=4565",info.liveInfo.ccid))
	end
end

function LuaCCLiveWnd:OnCCActivityTextureClick()
	if self.m_CCActivityInfo and self.m_CCActivityInfo.link then
		CWebBrowserMgr.Inst:OpenUrl(self.m_CCActivityInfo.link)
	end
	self:StartHideCCActivityTick()
end

function LuaCCLiveWnd:InitCCActivityInfo(needQuery)
	self:StopHideCCActivityTick()
	local info = CCCChatMgr.Inst:GetCurrentStation_V2()
	if info then
		if self.m_CCActivityInfo==nil or self.m_CCActivityInfo.anchor_uid~=info.anchor_uid or self.m_CCActivityInfo.cid~=info.cid then
			self.m_CCActivityTexture.gameObject:SetActive(false)
			if needQuery then
				LuaCCChatMgr:GetCCLiveActivityInfo(info.anchor_uid, info.cid) --发送请求				
			end
		elseif self.m_CCActivityInfo.available then
			self.m_CCActivityTexture.gameObject:SetActive(true)
			self.m_CCActivityTexture.mainTexture = self.m_CCActivityInfo.iconTexture
			self:StartHideCCActivityTick()
		else
			self.m_CCActivityTexture.gameObject:SetActive(false)
		end
	else
		self.m_CCActivityTexture.gameObject:SetActive(false)
	end
end

function LuaCCLiveWnd:OnGetCCLiveActivityInfo(activityInfo)
	self.m_CCActivityInfo = activityInfo
	self:InitCCActivityInfo(false)
end

function LuaCCLiveWnd:StartHideCCActivityTick()
    self:StopHideCCActivityTick()
    self.m_HideCCActivityTick = RegisterTickOnce(function()
        self.m_CCActivityTexture.gameObject:SetActive(false)
    end, 30000) --30s自动隐藏
end

function LuaCCLiveWnd:StopHideCCActivityTick()
    if self.m_HideCCActivityTick then
        UnRegisterTick(self.m_HideCCActivityTick)
        self.m_HideCCActivityTick = nil
    end
end

function LuaCCLiveWnd:OnDestroy()
	CCCChatMgr.Inst:LeaveRemoteCCChannel(EnumCCMiniStreamType.eZhubo)
end
