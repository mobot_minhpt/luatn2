local DelegateFactory  = import "DelegateFactory"
local UIEventListener  = import "UIEventListener"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local EnumMoneyType    = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local CShopMallMgr     = import "L10.UI.CShopMallMgr"
local Constants        = import "L10.Game.Constants"
local CItemMgr         = import "L10.Game.CItemMgr"
local EnumItemPlace    = import "L10.Game.EnumItemPlace"

LuaWeddingSelectWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaWeddingSelectWnd, "items")
RegistClassMember(LuaWeddingSelectWnd, "upLabels")
RegistClassMember(LuaWeddingSelectWnd, "selects")
RegistClassMember(LuaWeddingSelectWnd, "nums")
RegistClassMember(LuaWeddingSelectWnd, "okButton")
RegistClassMember(LuaWeddingSelectWnd, "okButtonLabel")
RegistClassMember(LuaWeddingSelectWnd, "moneyCtrl")

RegistClassMember(LuaWeddingSelectWnd, "curSelectId")
RegistClassMember(LuaWeddingSelectWnd, "dataTbl")

function LuaWeddingSelectWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self:InitUIComponents()
    self:InitEventListener()
    self:InitActive()
end

-- 初始化UI组件
function LuaWeddingSelectWnd:InitUIComponents()
    local anchor = self.transform:Find("Anchor")
    local parent = anchor:Find(LuaWeddingIterationMgr.selectWndItem)

    self.items = {parent:Find("Left"), parent:Find("Right")}
    self.upLabels = {}
    self.selects = {}
    self:InitHalfUI(self.items[1])
    self:InitHalfUI(self.items[2])

    self.moneyCtrl = anchor:Find("QnCostAndOwnMoney"):GetComponent(typeof(CCurentMoneyCtrl))
    self.okButton = anchor:Find("OKButton").gameObject
    self.okButtonLabel = self.okButton.transform:Find("Label"):GetComponent(typeof(UILabel))

    if LuaWeddingIterationMgr.selectWndItem == "Candy" then
        self.nums = {}
        for i = 1, #self.upLabels do
            table.insert(self.nums, self.upLabels[i].transform:Find("Num"):GetComponent(typeof(UILabel)))
        end
    end
end

function LuaWeddingSelectWnd:InitHalfUI(parent)
    table.insert(self.upLabels, parent:Find("Label"):GetComponent(typeof(UILabel)))
    table.insert(self.selects, parent:Find("Select").gameObject)
end

-- 初始化响应
function LuaWeddingSelectWnd:InitEventListener()
	UIEventListener.Get(self.okButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOKButtonClick()
	end)

    for i = 1, 2 do
        UIEventListener.Get(self.items[i].gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnItemClick(i)
        end)
    end
end

-- 初始化active
function LuaWeddingSelectWnd:InitActive()
    local anchor = self.transform:Find("Anchor")
    anchor:Find("Feast").gameObject:SetActive(false)
    anchor:Find("Candy").gameObject:SetActive(false)
    anchor:Find("Firework").gameObject:SetActive(false)
    anchor:Find(LuaWeddingIterationMgr.selectWndItem).gameObject:SetActive(true)

    for i = 1, 2 do
        self.selects[i]:SetActive(false)
    end
end


function LuaWeddingSelectWnd:Init()
    self:InitDataTbl()
    self:InitNum()
    self:InitIcon()

    self:OnItemClick(2)
end

-- 初始化数据表
function LuaWeddingSelectWnd:InitDataTbl()
    local selectWndItem = LuaWeddingIterationMgr.selectWndItem
    local tbl = WeddingIteration_Cost

    if selectWndItem == "Feast" then
        self.dataTbl = {tbl.GetData("SimpleFeast"), tbl.GetData("LuxuryFeast")}
    elseif selectWndItem == "Firework" then
        self.dataTbl = {tbl.GetData("SimpleFire"), tbl.GetData("LuxuryFire")}
    elseif selectWndItem == "Candy" then
        self.dataTbl = {tbl.GetData("SimpleCandy"), tbl.GetData("LuxuryCandy")}
    end
end

-- 初始化名称以及图像
function LuaWeddingSelectWnd:InitIcon()
    local selectWndItem = LuaWeddingIterationMgr.selectWndItem

    for i = 1, 2 do
        self.upLabels[i].text = self.dataTbl[i].Name
    end

    if selectWndItem == "Feast" then
        self.okButtonLabel.text = LocalString.GetString("摆酒")
    elseif selectWndItem == "Firework" then
        self.okButtonLabel.text = LocalString.GetString("燃放烟花")
    elseif selectWndItem == "Candy" then
        self.okButtonLabel.text = LocalString.GetString("撒糖")
    end
end

-- 初始化数量
function LuaWeddingSelectWnd:InitNum()
    if LuaWeddingIterationMgr.selectWndItem ~= "Candy" then return end

    self.nums[1].text = "[c][ff0000]0[-]/1"

    local luxuryCandyItemId = WeddingIteration_Setting.GetData().LuxuryCandyCostItemId
    local bindCount, notbindCount
    bindCount, notbindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(luxuryCandyItemId)
    if bindCount + notbindCount > 0 then
        self.nums[2].text = SafeStringFormat3("%d/1", bindCount + notbindCount)
    else
        self.nums[2].text = "[c][ff0000]0[-]/1"
    end
end

-- 更新花费和拥有
function LuaWeddingSelectWnd:UpdateCostAndOwn()
    local id = self.curSelectId

    local fee = self.dataTbl[id].Fee

    if fee == 1 then
        self.moneyCtrl:SetType(EnumMoneyType.LingYu, EnumPlayScoreKey.NONE, true)
    elseif fee == 2 then
        self.moneyCtrl:SetType(EnumMoneyType.YinLiang, EnumPlayScoreKey.NONE, true)
    end
    self.moneyCtrl:SetCost(self.dataTbl[id].Cost)
end

function LuaWeddingSelectWnd:ClickOK()
    local selectWndItem = LuaWeddingIterationMgr.selectWndItem
    local id = self.curSelectId

    local bSimple = id == 1
    local cost = self.dataTbl[id].Cost

    if self.moneyCtrl.moneyEnough then
        if selectWndItem == "Feast" then
            Gac2Gas.NewWeddingRequestOpenJiuXi(bSimple, cost)
            CUIManager.CloseUI(CLuaUIResources.WeddingSelectWnd)
        elseif selectWndItem == "Firework" then
            local msg = g_MessageMgr:FormatMessage("Wedding_Play_Firework_Confirm")
            MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function ()
                Gac2Gas.NewWeddingRequestSetFirework(bSimple, cost, false)
                CUIManager.CloseUI(CLuaUIResources.WeddingSelectWnd)
            end), nil, nil, nil, false)
        elseif selectWndItem == "Candy" then
            -- 旧的RPC 之后需要调整
            if id == 1 then
                Gac2Gas.RequestSendSimpleCandy()
            else
                Gac2Gas.RequestSendLuxuryCandy()
            end
            CUIManager.CloseUI(CLuaUIResources.WeddingSelectWnd)
        end
    else
        local fee = self.dataTbl[id].Fee
        if fee == 1 then
            MessageWndManager.ShowOKCancelMessage(LocalString.GetString("灵玉不足，是否前往充值？"), DelegateFactory.Action(function ()
                CShopMallMgr.ShowChargeWnd()
            end), nil, LocalString.GetString("充值"), nil, false)
        elseif fee == 2 then
            local txt = g_MessageMgr:FormatMessage("NotEnough_Silver_QuickBuy")
            MessageWndManager.ShowOKCancelMessage(txt, DelegateFactory.Action(function ()
                CShopMallMgr.ShowLinyuShoppingMall(Constants.SilverItemId)
            end), nil, nil, nil, false)
        end
    end
end

--@region UIEvent

function LuaWeddingSelectWnd:OnItemClick(id)
    self.selects[id]:SetActive(true)
    self.selects[3 - id]:SetActive(false)
    self.curSelectId = id
    self:UpdateCostAndOwn()
end

function LuaWeddingSelectWnd:OnOKButtonClick()
    local selectWndItem = LuaWeddingIterationMgr.selectWndItem
    local id = self.curSelectId

    local bSimple = id == 1
    local cost = self.dataTbl[id].Cost

    if selectWndItem == "Firework" and not bSimple then
        local ticketId = WeddingIteration_Setting.GetData().LuxuryFireworkTicketItemId
        local count = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, ticketId)

        if count > 0 then
            MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("WEDDING_LIHUAQUAN"), DelegateFactory.Action(function ()
                Gac2Gas.NewWeddingRequestSetFirework(bSimple, cost, true)
                CUIManager.CloseUI(CLuaUIResources.WeddingSelectWnd)
            end),  DelegateFactory.Action(function ()
                self:ClickOK()
            end), nil, nil, false)
        else
            self:ClickOK()
        end
    else
        self:ClickOK()
    end
end

--@endregion UIEvent

