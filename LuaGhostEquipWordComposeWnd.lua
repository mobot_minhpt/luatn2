require("common/common_include")
local LuaGameObject=import "LuaGameObject"
local Gac2Gas=import "L10.Game.Gac2Gas"
local CEquipmentProcessMgr=import "L10.Game.CEquipmentProcessMgr"
local MessageWndManager=import "L10.UI.MessageWndManager"
local MessageMgr=import "L10.Game.MessageMgr"
local Convert=import "System.Convert"
local EquipBaptize_Setting=import "L10.Game.EquipBaptize_Setting"
local EquipmentTemplate_Equip=import "L10.Game.EquipmentTemplate_Equip"
local CItemInfoMgr=import "L10.UI.CItemInfoMgr"
local CItemMgr=import "L10.Game.CItemMgr"
local CItemAccessListMgr=import "L10.UI.CItemAccessListMgr"
local CItemChooseMgr=import "L10.UI.CItemChooseMgr"
local AlignType=import "L10.UI.CItemInfoMgr+AlignType"
local CUIFxPaths=import "L10.UI.CUIFxPaths"
local LuaTweenUtils=import "LuaTweenUtils"

CLuaGhostEquipWordComposeWnd=class()
RegistClassMember(CLuaGhostEquipWordComposeWnd,"m_ComposeButton")
-- RegistClassMember(CLuaGhostEquipWordComposeWnd,"m_Name1")
RegistClassMember(CLuaGhostEquipWordComposeWnd,"m_MoneyCtrl")
RegistClassMember(CLuaGhostEquipWordComposeWnd,"m_ItemCountUpdate")
RegistClassMember(CLuaGhostEquipWordComposeWnd,"m_NeedCount")
RegistClassMember(CLuaGhostEquipWordComposeWnd,"m_CostTemplateId")

RegistClassMember(CLuaGhostEquipWordComposeWnd,"m_CostTemplateId")

RegistClassMember(CLuaGhostEquipWordComposeWnd,"m_Icon")

RegistClassMember(CLuaGhostEquipWordComposeWnd,"m_Place")
RegistClassMember(CLuaGhostEquipWordComposeWnd,"m_Pos")
RegistClassMember(CLuaGhostEquipWordComposeWnd,"m_ItemId")
RegistClassMember(CLuaGhostEquipWordComposeWnd,"m_CostItemTemplateId")
RegistClassMember(CLuaGhostEquipWordComposeWnd,"m_CostItemCount")
RegistClassMember(CLuaGhostEquipWordComposeWnd,"m_Mask")
RegistClassMember(CLuaGhostEquipWordComposeWnd,"m_ComposedEquip")
RegistClassMember(CLuaGhostEquipWordComposeWnd,"m_AddEquipGo")

RegistClassMember(CLuaGhostEquipWordComposeWnd,"m_Equip2")
RegistClassMember(CLuaGhostEquipWordComposeWnd,"m_TargetLabel")

RegistClassMember(CLuaGhostEquipWordComposeWnd,"m_UIFx")


function CLuaGhostEquipWordComposeWnd:Init()
    self.m_ComposedEquip=nil
    self.m_NeedCount=0
    self.m_CostTemplateId=0
    -- selectedEquip
    self.m_TargetLabel=LuaGameObject.GetChildNoGC(self.transform,"TargetLabel").label

    local selectedEquip=CEquipmentProcessMgr.Inst.selectedEquipForGhostCompose
    local equip1=LuaGameObject.GetChildNoGC(self.transform,"Equip1").transform
    local equip3=LuaGameObject.GetChildNoGC(self.transform,"Equip3").transform
    self.m_Equip2=LuaGameObject.GetChildNoGC(self.transform,"Equip2").transform
    local equip2Go=self.m_Equip2.gameObject
    self.m_AddEquipGo=LuaGameObject.GetChildNoGC(self.m_Equip2.transform,"Add").gameObject
    
    self:InitSourceItem(equip1,selectedEquip)
    self:InitTarget(equip3,selectedEquip)
    self.m_Place=Convert.ToUInt32(selectedEquip.place)
    self.m_Pos=selectedEquip.pos
    self.m_ItemId=selectedEquip.itemId
    -- self:InitTargetLabel()
    Gac2Gas.QueryHeChengGuiCost( self.m_Place,self.m_Pos,self.m_ItemId)
    
    local g = LuaGameObject.GetChildNoGC(self.transform,"TipButton").gameObject
    UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
        --tip
        MessageMgr.Inst:ShowMessage("Double_EquipWords_HelpTips", {})
    end)
    self.m_ComposeButton=LuaGameObject.GetChildNoGC(self.transform,"OKButton").gameObject
    CommonDefs.AddOnClickListener(self.m_ComposeButton,DelegateFactory.Action_GameObject(function(go)
        self:PreRequestHeChengGui()
    end),false)
    
    self.m_MoneyCtrl=LuaGameObject.GetChildNoGC(self.transform,"QnCostAndOwnMoney").moneyCtrl
    self.m_MoneyCtrl:SetCost(self:GetYinLingCost(selectedEquip))

    self.m_ItemCountUpdate=LuaGameObject.GetChildNoGC(self.transform,"Cost").itemCountUpdate
    local itemGo=LuaGameObject.GetChildNoGC(self.transform,"Cost").gameObject

    CommonDefs.AddOnClickListener(itemGo,DelegateFactory.Action_GameObject(function(go)
        if self.m_ItemCountUpdate.count>=self.m_NeedCount then
            CItemInfoMgr.ShowLinkItemTemplateInfo(self.m_CostTemplateId,false,nil,AlignType.ScreenRight,0,0,0,0)
        else
            CItemAccessListMgr.Inst:ShowItemAccessInfo(self.m_CostTemplateId, false, go.transform)
        end
    end),false)


    self.m_Icon=LuaGameObject.GetChildNoGC(self.transform,"MatIcon").cTexture
    self.m_Mask=LuaGameObject.GetChildNoGC(self.transform,"Mask").gameObject


    CommonDefs.AddOnClickListener(self.m_Equip2.gameObject,DelegateFactory.Action_GameObject(function(go)
            CItemChooseMgr.ShowWndWithGhostCompose()
    end),false)
    CommonDefs.AddOnClickListener(equip1.gameObject,DelegateFactory.Action_GameObject(function(go)
        --成功之后就不能点击了
        local commonItem = CItemMgr.Inst:GetById(self.m_ItemId)
        if commonItem then
            CItemInfoMgr.ShowLinkItemInfo(commonItem)
        end
    end),false)

    self.m_UIFx=LuaGameObject.GetChildNoGC(self.transform,"UIFx").uiFx
end

function CLuaGhostEquipWordComposeWnd:ShowFx()
    self.m_UIFx:DestroyFx()
    self.m_UIFx:LoadFx(CUIFxPaths.CommonBlueLineFxPath)

    local path={Vector3(64,-64,0),Vector3(-64,-64,0),Vector3(-64,64,0),Vector3(64,64,0),Vector3(64,-64,0)}
    local array = Table2Array(path, MakeArrayClass(Vector3))
    LuaUtils.SetLocalPosition(self.m_UIFx.FxRoot,64,-64,0)
    LuaTweenUtils.DODefaultLocalPath(self.m_UIFx.FxRoot,array,2)
end

function CLuaGhostEquipWordComposeWnd:InitSourceItem(tf,itemInfo)
    local icon=LuaGameObject.GetChildNoGC(tf,"Icon").cTexture
    local qualitySprite=LuaGameObject.GetChildNoGC(tf,"Quality").sprite
    local bindSprite=LuaGameObject.GetChildNoGC(tf,"BindSprite").sprite

    local nameLabel=FindChild(tf,"Name"):GetComponent(typeof(UILabel))

    local commonItem = CItemMgr.Inst:GetById(itemInfo.itemId)

    icon:LoadMaterial(commonItem.Icon)

    qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(commonItem.Equip.QualityType)

    if bindSprite then
        bindSprite.spriteName = commonItem.BindOrEquipCornerMark
    end
    if commonItem.Equip.HasTwoWordSet then
        nameLabel.text=LocalString.GetString("二套词条")
    else
        nameLabel.text=LocalString.GetString("单套词条")
    end
end
function CLuaGhostEquipWordComposeWnd:InitTarget(tf,itemInfo)
    local icon=LuaGameObject.GetChildNoGC(tf,"Icon").cTexture
    -- local qualitySprite=LuaGameObject.GetChildNoGC(tf,"Quality").sprite
    local bindSprite=LuaGameObject.GetChildNoGC(tf,"BindSprite").sprite

    local commonItem = CItemMgr.Inst:GetById(itemInfo.itemId)

    icon:LoadMaterial(commonItem.Icon)

    if bindSprite then
        bindSprite.spriteName = commonItem.BindOrEquipCornerMark;
    end
end

function CLuaGhostEquipWordComposeWnd:ActivateTarget(tf)
    LuaUtils.SetWidgetsAlpha(tf,1)
    self:ShowFx()
    local bindSprite=LuaGameObject.GetChildNoGC(tf,"BindSprite").sprite
    local commonItem = CItemMgr.Inst:GetById(self.m_ItemId)
    if commonItem and bindSprite then
        bindSprite.spriteName = commonItem.BindOrEquipCornerMark;
    end
end

function CLuaGhostEquipWordComposeWnd:CleanItem(tf)
    local icon=LuaGameObject.GetChildNoGC(tf,"Icon").cTexture
    local qualitySprite=LuaGameObject.GetChildNoGC(tf,"Quality").sprite
    qualitySprite.spriteName="common_itemcell_border"
    local bindSprite=LuaGameObject.GetChildNoGC(tf,"BindSprite").sprite
    if bindSprite then
        bindSprite.spriteName=""
    end
    icon:Clear()

    CommonDefs.AddOnClickListener(tf.gameObject,DelegateFactory.Action_GameObject(function(go) end),false)
end

function CLuaGhostEquipWordComposeWnd:InitComposedEquip(tf)
    local commonItem = CItemMgr.Inst:GetById(self.m_ComposedEquip)

    local icon=LuaGameObject.GetChildNoGC(tf,"Icon").cTexture
    local qualitySprite=LuaGameObject.GetChildNoGC(tf,"Quality").sprite
    local bindSprite=LuaGameObject.GetChildNoGC(tf,"BindSprite").sprite

    icon:LoadMaterial(commonItem.Icon)

    qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(commonItem.Equip.QualityType)

    if bindSprite then
        bindSprite.spriteName = commonItem.BindOrEquipCornerMark;
    end
end
function CLuaGhostEquipWordComposeWnd:CleanComposedEquip(tf)
    self.m_ComposedEquip=nil
    self.m_AddEquipGo:SetActive(true)

    local icon=LuaGameObject.GetChildNoGC(tf,"Icon").cTexture
    local qualitySprite=LuaGameObject.GetChildNoGC(tf,"Quality").sprite
    local bindSprite=LuaGameObject.GetChildNoGC(tf,"BindSprite").sprite
    icon:Clear()
    qualitySprite.spriteName="common_itemcell_border"
    if bindSprite then
        bindSprite.spriteName=""
    end
end

function CLuaGhostEquipWordComposeWnd:OnEnable()
    g_ScriptEvent:AddListener("QueryHeChengGuiCostReturn", self, "OnQueryHeChengGuiCostReturn")
    g_ScriptEvent:AddListener("GhostComposeSelectEquip", self, "OnGhostComposeSelectEquip")
    -- g_ScriptEvent:AddListener("SendItem", self, "OnSendItem")
    g_ScriptEvent:AddListener("RequestCanHeChengGuiReturn", self, "OnRequestCanHeChengGuiReturn")
    g_ScriptEvent:AddListener("NotifyHeChengGuiSuccess", self, "OnNotifyHeChengGuiSuccess")
    
end

function CLuaGhostEquipWordComposeWnd:OnDisable()
    g_ScriptEvent:RemoveListener("QueryHeChengGuiCostReturn", self, "OnQueryHeChengGuiCostReturn")
    g_ScriptEvent:RemoveListener("GhostComposeSelectEquip", self, "OnGhostComposeSelectEquip")
    -- g_ScriptEvent:RemoveListener("SendItem", self, "OnSendItem")
    g_ScriptEvent:RemoveListener("RequestCanHeChengGuiReturn", self, "OnRequestCanHeChengGuiReturn")
    g_ScriptEvent:RemoveListener("NotifyHeChengGuiSuccess", self, "OnNotifyHeChengGuiSuccess")
    
end
function CLuaGhostEquipWordComposeWnd:OnRequestCanHeChengGuiReturn(args)
    local array=args[0]
    
    local place=array[0]
    local pos=array[1]
    local targetEquipId=array[2]
    local canHeCheng=array[3]
    local equipNewGrade=array[4]
    if canHeCheng then
        if targetEquipId==self.m_ItemId then
            local itemInfo = CItemMgr.Inst:GetItemInfo(self.m_ComposedEquip)
            if itemInfo then
                local sourcePlace=Convert.ToUInt32(itemInfo.place)
                local sourcePos=itemInfo.pos
                Gac2Gas.RequestHeChengGui(self.m_Place,self.m_Pos,self.m_ItemId,sourcePlace,sourcePos,self.m_ComposedEquip)
            end
        end 
    end
end
function CLuaGhostEquipWordComposeWnd:PreRequestHeChengGui()
    --是否选择装备
    if not self.m_ComposedEquip then
        MessageMgr.Inst:ShowMessage("Double_EquipWords_Need_Select_Equip", {})
        return
    end
    --九曲珠
    if self.m_ItemCountUpdate.count<self.m_NeedCount then
        MessageMgr.Inst:ShowMessage("Ghost_Equip_Compose_CaiLiao_Is_Lack", {})
        return
    end
    if not self.m_MoneyCtrl.moneyEnough then
        MessageMgr.Inst:ShowMessage("SILVER_NOT_ENOUGH", {})
        return
    end
    

    local itemInfo = CItemMgr.Inst:GetItemInfo(self.m_ComposedEquip)
    if not itemInfo then return end

    local commonItem1 = CItemMgr.Inst:GetById(self.m_ItemId)
    local commonItem2 = CItemMgr.Inst:GetById(self.m_ComposedEquip)

    local function onOk()
        local sourcePlace=Convert.ToUInt32(itemInfo.place)
        local sourcePos=itemInfo.pos
        Gac2Gas.RequestCanHeChengGui(self.m_Place,self.m_Pos,self.m_ItemId,sourcePlace,sourcePos,self.m_ComposedEquip)
    end

    local function onRequest()
        if not commonItem1.Equip.HasTwoWordSet then
            local name1=commonItem1.Name
            local name2=commonItem2.Name
    
            local str=MessageMgr.Inst:FormatMessage("Double_EquipWords_Confirm",{name1,name2})
            MessageWndManager.ShowOKCancelMessage(str,DelegateFactory.Action(onOk),nil,nil,nil,false)
        else
            local name1=commonItem1.Name
            local str=MessageMgr.Inst:FormatMessage("Double_EquipWords_Confirm_HasTwoWordSet",{name1})
            MessageWndManager.ShowOKCancelMessage(str,DelegateFactory.Action(onOk),nil,nil,nil,false)
        end
    end
    
    local function onCheckWord()
        local secondWordSet = commonItem1.Equip.HasTwoWordSet and commonItem1.Equip.IsSecondWordSetActive or false
        local hasTempData = commonItem1.Equip:HasTempWordDataInWordSet(secondWordSet)
        local hasTempDataOtherSet = commonItem1.Equip:HasTempWordDataInWordSet(not secondWordSet)
        if hasTempData then
            MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("CITIAO_NOTICE"),DelegateFactory.Action(onRequest),nil,nil,nil,false)
        elseif hasTempDataOtherSet then
            MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("CITIAO_NOTICE_OTHERSET"),DelegateFactory.Action(onRequest),nil,nil,nil,false)
        else
            onRequest()
        end
    end

    --有没有不可修词条
    local fixlist= commonItem2.Equip:GetFixedWordList(false)
    local extlist=commonItem2.Equip:GetExtWordList(false)

    local have=false
    for i=1,fixlist.Count do
            if math.floor(fixlist[i-1]/100)==251025 then
            have=true
            break
        end
    end
    if not have then
        for i=1,extlist.Count do
            if math.floor(extlist[i-1]/100)==251025 then
                have=true
                break
            end
        end
    end
    --有不可修词条 提示
    if have then
        local str=MessageMgr.Inst:FormatMessage("Double_EquipWords_CanNotRepair_Confirm",{})
        MessageWndManager.ShowOKCancelMessage(str,DelegateFactory.Action(onCheckWord),nil,nil,nil,false)
    else
        onCheckWord()
    end
end

function CLuaGhostEquipWordComposeWnd:OnNotifyHeChengGuiSuccess(args)
    local itemId=args[0]
    if self.m_ItemId==itemId then
        --看看是否有双词条，如果有就算是成功了
        local commonItem = CItemMgr.Inst:GetById(itemId)
        if commonItem and commonItem.Equip.HasTwoWordSet then
            --成功之后就置灰
            CUICommonDef.SetActive(self.m_ComposeButton,false,true)
            self.m_ComposedEquip=nil
            local equip1=LuaGameObject.GetChildNoGC(self.transform,"Equip1").transform
            local equip2=LuaGameObject.GetChildNoGC(self.transform,"Equip2").transform
            local equip3=LuaGameObject.GetChildNoGC(self.transform,"Equip3").transform
            self:CleanItem(equip1)
            self:CleanItem(equip2)

            self:ActivateTarget(equip3)

            CommonDefs.AddOnClickListener(equip3.gameObject,DelegateFactory.Action_GameObject(function(go) 
                --可以点击查看
                local itemInfo=CEquipmentProcessMgr.Inst.selectedEquipForGhostCompose
                local commonItem = CItemMgr.Inst:GetById(itemInfo.itemId)
                if commonItem then
                    CItemInfoMgr.ShowLinkItemInfo(commonItem)
                end
            end),false)
        end
    end
end

function CLuaGhostEquipWordComposeWnd:OnGhostComposeSelectEquip(args)
    local itemId=args[0]
    local commonItem = CItemMgr.Inst:GetById(itemId)
    if commonItem then
        if commonItem.Equip.IsLostSoul>0 then
            MessageMgr.Inst:ShowMessage("Double_EquipWords_Fail_IsShiHun",{})
            self:CleanComposedEquip(self.m_Equip2)
            return
        end

        self.m_ComposedEquip=itemId
        self.m_AddEquipGo:SetActive(false)
        self:InitComposedEquip(self.m_Equip2)
    end
end

function CLuaGhostEquipWordComposeWnd:GetYinLingCost(targetEquip)
    local commonItem = CItemMgr.Inst:GetById(targetEquip.itemId)
    
	local gradeRate = 0
	local typeRate = 0
    local grade = commonItem.Grade
    
    local designData = EquipmentTemplate_Equip.GetData(commonItem.TemplateId)

    local equipType = designData.Type
    
    local settingData=EquipBaptize_Setting.GetData()

    local gradeRates=settingData.HeChengGuiGradeRate
    for i=1,gradeRates.Length,3 do
        local lower=gradeRates[i-1]
        local upper=gradeRates[i]
        if grade<=upper and grade>=lower then
            gradeRate=gradeRates[i+1]
            break
        end
    end

    local yinliangRates=settingData.HeChengGuiTypeRate
    for i=1,yinliangRates.Length,2 do
        local type=yinliangRates[i-1]
        if type==equipType then
            typeRate=yinliangRates[i]
            break
        end
    end
	return math.floor( gradeRate * typeRate)
end

function CLuaGhostEquipWordComposeWnd:OnQueryHeChengGuiCostReturn(args)
    local array=args[0]

    local place=array[0]
    local pos=array[1]
    local targetEquipId=array[2]
    local costYinLiang=array[3]
    local costItemTemplateId=array[4]
    local costItemCount=array[5]
    if place==self.m_Place and pos==self.m_Pos and targetEquipId==self.m_ItemId then
        self.m_CostTemplateId=costItemTemplateId
        self.m_NeedCount=costItemCount

        local template = CItemMgr.Inst:GetItemTemplate(self.m_CostTemplateId)
        if template then
            self.m_Icon:LoadMaterial(template.Icon)
        else
            self.m_Icon:Clear()
        end
        self.m_ItemCountUpdate.templateId=self.m_CostTemplateId
        local function OnCountChange(val)
            if val>=self.m_NeedCount then
                self.m_Mask:SetActive(false)
                self.m_ItemCountUpdate.format=SafeStringFormat3("{0}/%d",self.m_NeedCount)
            else
                self.m_Mask:SetActive(true)
                self.m_ItemCountUpdate.format=SafeStringFormat3("[ff0000]{0}[-]/%d",self.m_NeedCount)
            end
        end
        self.m_ItemCountUpdate.onChange=DelegateFactory.Action_int(OnCountChange)
        self.m_ItemCountUpdate:UpdateCount()
    end
end