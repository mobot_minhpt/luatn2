local CUIFx = import "L10.UI.CUIFx"
local UILabel = import "UILabel"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

CLuaQingMingJieZiTipWnd = class()

RegistChildComponent(CLuaQingMingJieZiTipWnd, "RoundLabel", UILabel)
RegistChildComponent(CLuaQingMingJieZiTipWnd, "FxNode", CUIFx)
RegistClassMember(CLuaQingMingJieZiTipWnd,"m_CloseTick")
RegistChildComponent(CLuaQingMingJieZiTipWnd, "RemainTimeLabel", UILabel)

RegistClassMember(CLuaQingMingJieZiTipWnd,"m_Round")
RegistClassMember(CLuaQingMingJieZiTipWnd, "m_RoundEndTime")
RegistClassMember(CLuaQingMingJieZiTipWnd, "m_PrepareText")

function CLuaQingMingJieZiTipWnd:Awake()
    self.m_CloseTick = nil
    self.m_RoundEndTime = CLuaQingMing2020Mgr.RoundEndTime
    self.m_PrepareText = "00:00"
    self.m_Round = 1
end

function CLuaQingMingJieZiTipWnd:OnEnable()
    g_ScriptEvent:AddListener("ShowQmPvePassRoundFx", self, "OnShowQmPvePassRoundFx")
    g_ScriptEvent:AddListener("ShowQmPveJieZiSucceedFx", self, "OnShowQmPveJieZiSucceedFx")
end

function CLuaQingMingJieZiTipWnd:OnDisable()
    g_ScriptEvent:RemoveListener("ShowQmPvePassRoundFx", self, "OnShowQmPvePassRoundFx")
    g_ScriptEvent:RemoveListener("ShowQmPveJieZiSucceedFx", self, "OnShowQmPveJieZiSucceedFx")
end

function CLuaQingMingJieZiTipWnd:Init()
    local duration = QingMing2019_QingMing2020Pve.GetData().RoundDuration
    local min = math.floor(duration/60)
	local sec = math.floor(duration - min * 60)
	if sec < 10 then
		sec = '0'..sec
	end
	local s = min .. ':' .. sec
    SafeStringFormat3(LocalString.GetString("[ACF9FF]%s 第%d关[-]"), s, self.m_Round)
    self.m_PrepareText = s
end

function CLuaQingMingJieZiTipWnd:DelayClose()
    if self.m_CloseTick then
        UnRegisterTick(self.m_CloseTick)
        self.m_CloseTick = nil
    end
    self.m_CloseTick = RegisterTickOnce(function ( ... )
        self.FxNode:DestroyFx()
        self.RoundLabel.gameObject:SetActive(false)     
		end, 1000 * 2)
end

function CLuaQingMingJieZiTipWnd:OnShowQmPvePassRoundFx(upToRoundIndex) 
    self.m_Round = upToRoundIndex
    if not self.m_Round then
        self.RoundLabel.gameObject:SetActive(false)
    end
    self.RoundLabel.gameObject:SetActive(true)
    self.RoundLabel.text = SafeStringFormat3(LocalString.GetString("第%d关"),self.m_Round)
    self.FxNode:LoadFx(QingMing2019_QingMing2020Pve.GetData().RoundFx)
    self:DelayClose()
end

function CLuaQingMingJieZiTipWnd:OnShowQmPveJieZiSucceedFx()
    local riddleFx = QingMing2019_QingMing2020Pve.GetData().RiddleFx
    if riddleFx then
        self.FxNode:LoadFx(riddleFx)
        self:DelayClose()
    end
end

function CLuaQingMingJieZiTipWnd:Update()
    if CLuaQingMing2020Mgr.RoundEndTime and CLuaQingMing2020Mgr.IsInPvePlay() then
		local restTime = CLuaQingMing2020Mgr.RoundEndTime - CServerTimeMgr.Inst.timeStamp
		if restTime <= 0 then
            self.RemainTimeLabel.text = SafeStringFormat3(LocalString.GetString("[ACF9FF]%s 第%d关[-]"), self.m_PrepareText, self.m_Round)
		else
			local min = math.floor(restTime/60)
			local sec = math.floor(restTime - min * 60)
			if sec < 10 then
				sec = '0'..sec
			end
			local s = min .. ':' .. sec
            self.RemainTimeLabel.text = SafeStringFormat3(LocalString.GetString("[ACF9FF]%s 第%d关[-]"), s, self.m_Round)
		end
	end
end