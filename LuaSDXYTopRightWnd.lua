local GameObject = import "UnityEngine.GameObject"
local UIGrid = import "UIGrid"

local DelegateFactory  = import "DelegateFactory"
local Vector3 = import "UnityEngine.Vector3"

LuaSDXYTopRightWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaSDXYTopRightWnd, "ExpandButton", "ExpandButton", GameObject)
RegistChildComponent(LuaSDXYTopRightWnd, "StarGrid", "StarGrid", UIGrid)
RegistChildComponent(LuaSDXYTopRightWnd, "Star", "Star", GameObject)

--@endregion RegistChildComponent end

function LuaSDXYTopRightWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.ExpandButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnExpandButtonClick()
	end)

    --@endregion EventBind end
end

function LuaSDXYTopRightWnd:OnEnable()
	--SyncXingYuLightedStars
	g_ScriptEvent:AddListener("SyncXingYuLightedStars", self, "OnSyncXingYuLightedStars")
	g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
end

function LuaSDXYTopRightWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SyncXingYuLightedStars", self, "OnSyncXingYuLightedStars")
	g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
end

function LuaSDXYTopRightWnd:Init()
	--print("Init sdxy Top Right",LuaChristmas2021Mgr.m_LastXingYuLightedStarsList and LuaChristmas2021Mgr.m_LastXingYuLightedStarsList.Count or 0)
	self.Star:SetActive(false)
	self:OnSyncXingYuLightedStars(nil)
end

function LuaSDXYTopRightWnd:OnSyncXingYuLightedStars(list)
	--local list = MsgPackImpl.unpack(ud)
	Extensions.RemoveAllChildren(self.StarGrid.transform)
	if not list then
		list = LuaChristmas2021Mgr.m_LastXingYuLightedStarsList
	end
	if list then
		local count = list.Count 
		for i=1,count,1 do
			local star = NGUITools.AddChild(self.StarGrid.gameObject,self.Star)
        	star:SetActive(true)
		end
		self.StarGrid:Reposition()
		if count == 10 then
			--进度满了以后需要隐藏
			self.transform:Find("Anchor").gameObject:SetActive(false)
		end
	end
end

function LuaSDXYTopRightWnd:OnHideTopAndRightTipWnd()
    self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 180)
end
--@region UIEvent

function LuaSDXYTopRightWnd:OnExpandButtonClick()
	self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 0)
    CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end


--@endregion UIEvent

