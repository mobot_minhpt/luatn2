-- Auto Generated!!
local CClientFurnitureMgr = import "L10.Game.CClientFurnitureMgr"
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CTooltip = import "L10.UI.CTooltip"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUITexture = import "L10.UI.CUITexture"
local CWeixiuItemList = import "L10.UI.CWeixiuItemList"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local Gac2Gas = import "L10.Game.Gac2Gas"
local GameObject = import "UnityEngine.GameObject"
local Item_Item = import "L10.Game.Item_Item"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local NGUITools = import "NGUITools"
local Object = import "System.Object"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local UInt32 = import "System.UInt32"
local UISprite = import "UISprite"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local Zhuangshiwu_Setting = import "L10.Game.Zhuangshiwu_Setting"
local Zhuangshiwu_Zhuangshiwu = import "L10.Game.Zhuangshiwu_Zhuangshiwu"
CWeixiuItemList.m_Init_CS2LuaHook = function (this) 
    this:InitList()
end
CWeixiuItemList.m_OnEnable_CS2LuaHook = function (this) 
    EventManager.AddListener(EnumEventType.OnUpdateHouseWeixiuResource, MakeDelegateFromCSFunction(this.InitList, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.SendItem, MakeDelegateFromCSFunction(this.OnSendItem, MakeGenericClass(Action1, String), this))
    EventManager.AddListenerInternal(EnumEventType.SetItemAt, MakeDelegateFromCSFunction(this.OnSetItemAt, MakeGenericClass(Action4, EnumItemPlace, UInt32, String, String), this))
end
CWeixiuItemList.m_OnDisable_CS2LuaHook = function (this) 
    EventManager.RemoveListener(EnumEventType.OnUpdateHouseWeixiuResource, MakeDelegateFromCSFunction(this.InitList, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.SendItem, MakeDelegateFromCSFunction(this.OnSendItem, MakeGenericClass(Action1, String), this))
    EventManager.RemoveListenerInternal(EnumEventType.SetItemAt, MakeDelegateFromCSFunction(this.OnSetItemAt, MakeGenericClass(Action4, EnumItemPlace, UInt32, String, String), this))
end
CWeixiuItemList.m_InitList_CS2LuaHook = function (this) 
    CommonDefs.ListClear(this.ItemList)
    CommonDefs.ListClear(this.ItemIdList)
    CommonDefs.ListClear(this.BtnList)

    this.repaireItemCount = 0
    local repaireItemId = ""

    Extensions.RemoveAllChildren(this.ItemScroll.transform)

    local orangeItemList = CreateFromClass(MakeGenericClass(List, String))
    local blueItemList = CreateFromClass(MakeGenericClass(List, String))
    local redItemList = CreateFromClass(MakeGenericClass(List, String))
    local purpleItemList = CreateFromClass(MakeGenericClass(List, String))

    local packageSize = CClientMainPlayer.Inst.ItemProp:GetPlaceMaxSize(EnumItemPlace.Bag)
    do
        local i = 1
        while i <= packageSize do
            local itemid = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Bag, i)
            if itemid ~= nil then
                local item = CItemMgr.Inst:GetById(itemid)
                if item ~= nil then
                    local data = Item_Item.GetData(item.TemplateId)
                    local zswtid = CClientFurnitureMgr.GetZhuangshiwuIdFromItemData(data)
                    local zswdata = Zhuangshiwu_Zhuangshiwu.GetData(zswtid)
                    if zswdata ~= nil and zswdata.Type ~= EnumFurnitureType_lua.eLingshou and zswdata.Repair > 0 then
                        if data.NameColor == "COLOR_ORANGE" then
                            CommonDefs.ListAdd(orangeItemList, typeof(String), itemid)
                        elseif data.NameColor == "COLOR_BLUE" then
                            CommonDefs.ListAdd(blueItemList, typeof(String), itemid)
                        elseif data.NameColor == "COLOR_RED" then
                            CommonDefs.ListAdd(redItemList, typeof(String), itemid)
                        elseif data.NameColor == "COLOR_PURPLE" then
                            CommonDefs.ListAdd(purpleItemList, typeof(String), itemid)
                        end
                    end
                    if item.TemplateId == Zhuangshiwu_Setting.GetData().RepaireMaterialItemTempId then
                        this.repaireItemCount = this.repaireItemCount + item.Item.Count
                        repaireItemId = itemid
                    end
                end
            end
            i = i + 1
        end
    end

    do
        local designData = Item_Item.GetData(Zhuangshiwu_Setting.GetData().RepaireMaterialItemTempId)

        local FurnitureItemUnit = NGUITools.AddChild(this.ItemScroll.gameObject, this.ItemTemplate)
        FurnitureItemUnit:SetActive(true)
        this:InitCell(FurnitureItemUnit, designData.Name, this.repaireItemCount * Zhuangshiwu_Setting.GetData().MerterialPerItem, this.repaireItemCount, designData.Icon, designData)
        local texture = FurnitureItemUnit.transform:Find("Texture").gameObject
        if this.repaireItemCount <= 0 then
            local noAmountSprite = FurnitureItemUnit.transform:Find("NoAmountSprite").gameObject
            noAmountSprite:SetActive(true)
            UIEventListener.Get(FurnitureItemUnit).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(FurnitureItemUnit).onClick, MakeDelegateFromCSFunction(this.OnNoAmountClick, VoidDelegate, this), true)
            UIEventListener.Get(texture).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(texture).onClick, MakeDelegateFromCSFunction(this.OnNoAmountClick, VoidDelegate, this), true)
        else
            UIEventListener.Get(FurnitureItemUnit).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(FurnitureItemUnit).onClick, MakeDelegateFromCSFunction(this.OnItemClicked, VoidDelegate, this), true)
            UIEventListener.Get(texture).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(texture).onClick, MakeDelegateFromCSFunction(this.OnItemClicked, VoidDelegate, this), true)
        end

        CommonDefs.ListAdd(this.ItemList, typeof(GameObject), FurnitureItemUnit)
        CommonDefs.ListAdd(this.BtnList, typeof(GameObject), texture)
        CommonDefs.ListAdd(this.ItemIdList, typeof(String), repaireItemId)
    end


    local act = DelegateFactory.Action_List_string(function (list) 
        do
            local i = 0
            while i < list.Count do
                local itemid = list[i]
                if itemid ~= nil then
                    local item = CItemMgr.Inst:GetById(itemid)
                    if item ~= nil then
                        local data = Item_Item.GetData(item.TemplateId)
                        local zswtid = CClientFurnitureMgr.GetZhuangshiwuIdFromItemData(data)
                        local zswdata = Zhuangshiwu_Zhuangshiwu.GetData(zswtid)
                        if zswdata ~= nil then
                            local FurnitureItemUnit = NGUITools.AddChild(this.ItemScroll.gameObject, this.ItemTemplate)
                            FurnitureItemUnit:SetActive(true)
                            this:InitCell(FurnitureItemUnit, data.Name, zswdata.Repair, item.Amount, data.Icon, data)

                            CommonDefs.ListAdd(this.ItemList, typeof(GameObject), FurnitureItemUnit)
                            UIEventListener.Get(FurnitureItemUnit).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(FurnitureItemUnit).onClick, MakeDelegateFromCSFunction(this.OnItemClicked, VoidDelegate, this), true)

                            local texture = FurnitureItemUnit.transform:Find("Texture").gameObject
                            UIEventListener.Get(texture).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(texture).onClick, MakeDelegateFromCSFunction(this.OnItemClicked, VoidDelegate, this), true)

                            CommonDefs.ListAdd(this.BtnList, typeof(GameObject), texture)

                            CommonDefs.ListAdd(this.ItemIdList, typeof(String), itemid)
                        end
                    end
                end
                i = i + 1
            end
        end
    end)

    GenericDelegateInvoke(act, Table2ArrayWithCount({orangeItemList}, 1, MakeArrayClass(Object)))
    GenericDelegateInvoke(act, Table2ArrayWithCount({blueItemList}, 1, MakeArrayClass(Object)))
    GenericDelegateInvoke(act, Table2ArrayWithCount({redItemList}, 1, MakeArrayClass(Object)))
    GenericDelegateInvoke(act, Table2ArrayWithCount({purpleItemList}, 1, MakeArrayClass(Object)))


    this.Grid:Reposition()
    this.ItemScroll:ResetPosition()
end
CWeixiuItemList.m_InitCell_CS2LuaHook = function (this, cell, name, weixiuResource, count, icon, itemdata) 
    local texture = cell.transform:Find("Texture").gameObject
    if texture ~= nil then
        local tex = CommonDefs.GetComponent_GameObject_Type(texture, typeof(CUITexture))
        if tex ~= nil then
            tex:LoadMaterial(icon)
        end
    end

    local Name = cell.transform:Find("Name").gameObject
    if Name ~= nil then
        local tex = CommonDefs.GetComponent_GameObject_Type(Name, typeof(UILabel))
        if tex ~= nil then
            tex.text = name
        end
    end

    local desc = cell.transform:Find("Description").gameObject
    if desc ~= nil then
        local tex = CommonDefs.GetComponent_GameObject_Type(desc, typeof(UILabel))
        if tex ~= nil then
            tex.text = System.String.Format(LocalString.GetString("可兑换{0}维修资源"), weixiuResource)
        end
    end

    local Count = cell.transform:Find("Label").gameObject
    if Count ~= nil then
        local tex = CommonDefs.GetComponent_GameObject_Type(Count, typeof(UILabel))
        if tex ~= nil then
            tex.text = tostring(count)
        end
    end

    local SelectedSprite = cell.transform:Find("SelectSprite").gameObject
    if SelectedSprite ~= nil then
        SelectedSprite:SetActive(false)
    end

    local QualitySprite = cell.transform:Find("QualitySprite").gameObject
    if QualitySprite ~= nil then
        local spri = CommonDefs.GetComponent_GameObject_Type(QualitySprite, typeof(UISprite))
        if spri ~= nil then
            spri.spriteName = CUICommonDef.GetItemCellBorder(itemdata, nil, false)
        end
    end
end
CWeixiuItemList.m_OnNoAmountClick_CS2LuaHook = function (this, go) 
    if not CClientHouseMgr.Inst:IsInOwnHouse() then
        g_MessageMgr:ShowMessage("OPERATION_NEED_IN_OWN_HOUSE")
        return
    end
    CItemAccessListMgr.Inst:ShowItemAccessInfo(Zhuangshiwu_Setting.GetData().RepaireMaterialItemTempId, false, go.transform, CTooltip.AlignType.Right)
end
CWeixiuItemList.m_OnItemClicked_CS2LuaHook = function (this, go) 
    if not CClientHouseMgr.Inst:IsInOwnHouse() then
        g_MessageMgr:ShowMessage("OPERATION_NEED_IN_OWN_HOUSE")
        return
    end

    local repaireItemTempId = Zhuangshiwu_Setting.GetData().RepaireMaterialItemTempId
    do
        local i = 0
        while i < this.ItemList.Count do
            local SelectedSprite = this.ItemList[i].transform:Find("SelectSprite").gameObject
            if this.ItemList[i] == go and this.ItemIdList.Count > i then
                SelectedSprite:SetActive(true)

                local itemid = this.ItemIdList[i]
                local item = CItemMgr.Inst:GetById(itemid)
                if item ~= nil then
                    local data = Item_Item.GetData(item.TemplateId)
                    if data ~= nil then
                        local zswid = CClientFurnitureMgr.GetZhuangshiwuIdFromItemData(data)
                        local zswdata = Zhuangshiwu_Zhuangshiwu.GetData(zswid)
                        local itemTempId = item.TemplateId
                        if itemTempId == repaireItemTempId then
                            local msg = g_MessageMgr:FormatMessage("FURNITURE_EXCHANGE_NUM_TIPS")
                            CLuaNumberInputMgr.ShowNumInputBox(0, this.repaireItemCount, this.repaireItemCount, function (count) 
                                Gac2Gas.ConvertRepaireItem2WeixiuResource(count)
                            end, msg, - 1)
                        elseif zswdata ~= nil then
                            if CWeixiuItemList.sbNeedRemind then
                                local repaireCount = zswdata.Repair
                                MessageWndManager.ShowMessageWithCheckBox(System.String.Format(LocalString.GetString("确定将{0}兑换为维修资源{1}？"), data.Name, repaireCount), LocalString.GetString("之后不再提醒"), false, DelegateFactory.Action_bool(function (bNoMoreRemind) 
                                    Gac2Gas.ConvertItem2WeixiuResource(0, itemid)
                                    if bNoMoreRemind then
                                        CWeixiuItemList.sbNeedRemind = false
                                    end
                                end), nil, nil, nil)
                            else
                                Gac2Gas.ConvertItem2WeixiuResource(0, itemid)
                            end
                        end
                    end
                end
            else
                SelectedSprite:SetActive(false)
            end
            i = i + 1
        end
    end

    do
        local i = 0
        while i < this.BtnList.Count do
            local BtnSelectedSprite = this.BtnList[i].transform.parent:Find("BtnSelectSprite").gameObject
            if this.BtnList[i] == go and this.ItemIdList.Count > i then
                BtnSelectedSprite:SetActive(true)
                CItemInfoMgr.ShowFurnitureItemInfo(this.ItemIdList[i], nil)
            else
                BtnSelectedSprite:SetActive(false)
            end
            i = i + 1
        end
    end
end
