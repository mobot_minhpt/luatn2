local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CChatHelper = import "L10.UI.CChatHelper"
local StringBuilder = import "System.Text.StringBuilder"
local CChatLinkMgr = import "CChatLinkMgr"
local CVoiceMsg = import "L10.Game.CVoiceMsg"
local CVoiceMgr = import "L10.Game.CVoiceMgr"
local Ease = import "DG.Tweening.Ease"
local EnumEventType = import "EnumEventType"

LuaChatDanMuTemplate = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistClassMember(LuaChatDanMuTemplate, "m_MsgLabel")
RegistClassMember(LuaChatDanMuTemplate, "m_MsgBg")
RegistClassMember(LuaChatDanMuTemplate, "m_VoiceIcon")
RegistClassMember(LuaChatDanMuTemplate, "m_ChannelNameLabel")
RegistClassMember(LuaChatDanMuTemplate, "m_ChannelSprite")

RegistClassMember(LuaChatDanMuTemplate, "m_VoiceId")
RegistClassMember(LuaChatDanMuTemplate, "m_Msg")
RegistClassMember(LuaChatDanMuTemplate, "screenWidth")
--@endregion RegistChildComponent end

function LuaChatDanMuTemplate:Awake()
    self.m_MsgLabel = self.gameObject:GetComponent(typeof(UILabel))
    self.m_MsgBg = self.transform:Find("Bg")
    self.m_VoiceIcon = self.transform:Find("VoiceIcon"):GetComponent(typeof(UISprite))
    self.m_ChannelNameLabel = self.transform:Find("ChannelNameLabel"):GetComponent(typeof(UILabel))
    self.m_ChannelSprite = self.transform:Find("ChannelNameLabel/ChannelSprite"):GetComponent(typeof(UISprite))
	self.m_OnNewVoiceTextReady = DelegateFactory.Action_string_string(function(voiceId, text)
        self:OnNewVoiceTextReady(voiceId, text)
    end)
end

function LuaChatDanMuTemplate:Init(msg, screenWidth, screenHeight)
    self.screenWidth = screenWidth
    local bulletWidth = self:InitMsg(msg)
    self.transform.localPosition = Vector3(math.floor(screenWidth / 2), UnityEngine_Random(math.floor(- screenHeight / 2), math.floor(screenHeight / 2)), 0)
    CommonDefs.OnComplete_Tweener(CommonDefs.SetEase_Tweener(LuaTweenUtils.DOLocalMoveX(self.transform, math.floor(- screenWidth / 2) - bulletWidth, UnityEngine_Random(6, 12), false), Ease.Linear), DelegateFactory.TweenCallback(function () 
	    g_ScriptEvent:BroadcastInLua("ChatDanMuRecycle", self)
    end))
end

function LuaChatDanMuTemplate:InitMsg(msg)
    self.m_Msg = msg
    -- 频道
    local channel = CChatHelper.GetChannelByName(msg.channelName)
    self.m_ChannelSprite.spriteName = CChatHelper.GetChannelNameBg(channel, "chatdanmubullet")
    self.m_ChannelNameLabel.text = msg.channelName
    local stringbuilder = NewStringBuilderWraper(StringBuilder)
    if msg.fromUserId > 0 then
        stringbuilder:Append(SafeStringFormat3("[c][F8FF7C][%s][-][/c]", msg.fromUserName))
    end
    self.m_MsgLabel.text = ToStringWrap(stringbuilder)
    -- 声音图标
    local voiceIcon = self.transform:Find("VoiceIcon"):GetComponent(typeof(UISprite))
    if msg.isVoice then
        self.m_MsgLabel:UpdateNGUIText()
        NGUIText.regionWidth = 1000000
        LuaUtils.SetLocalPositionX(voiceIcon.transform, NGUIText.CalculatePrintedSize(ToStringWrap(stringbuilder)).x)
        local spaceWidth = Mathf.Max(2, NGUIText.GetGlyphWidth(32, 32))
        local total = 0
        while (total <= voiceIcon.width) do
            stringbuilder:Append(" ")
            total = total + spaceWidth
        end
        local voice = CVoiceMsg.Parse(msg.message)
        local voiceMsg = System.String.IsNullOrEmpty(voice.Text) and CVoiceMgr.Inst:GetVoiceText(voice.VoiceId) or voice.Text;
        stringbuilder:Append(CChatLinkMgr.TranslateToNGUIText(voiceMsg))
        voiceIcon.gameObject:SetActive(true)
        self.m_VoiceId = voice.VoiceId
    else
        stringbuilder:Append(CChatLinkMgr.TranslateToNGUIText(msg.message))
        voiceIcon.gameObject:SetActive(false)
    end
    self:WrapText(self.m_MsgLabel, ToStringWrap(stringbuilder))
    return self.m_MsgLabel.width + self.m_MsgLabel.width
end

function LuaChatDanMuTemplate:WrapText(label, originText)
    --赋值对label各项参数进行初始化，否则下面的计算会出问题，label类型需要clamp content， maxlines = 1
    label.text = originText
    local fit = true
    local outerText = ""
    fit, outerText = label:Wrap(originText)
    if not fit then
        outerText = CommonDefs.StringSubstring2(outerText, 0, CommonDefs.StringLength(outerText) - 1) .. "..."
        label.text = outerText
    end
end

function LuaChatDanMuTemplate:OnNewVoiceTextReady(voiceId, text)
    if self.m_VoiceId == voiceId and self.m_Msg then
        LuaTweenUtils.DOKill(self.transform, false)
        if self.m_Msg then
            local bulletWidth = self:InitMsg(self.m_Msg)
            CommonDefs.OnComplete_Tweener(CommonDefs.SetEase_Tweener(LuaTweenUtils.DOLocalMoveX(self.transform, math.floor(- self.screenWidth / 2) - bulletWidth, UnityEngine_Random(6, 12), false), Ease.Linear), DelegateFactory.TweenCallback(function () 
                g_ScriptEvent:BroadcastInLua("ChatDanMuRecycle", self)
            end))
        end
    end
end

--@region UIEvent
function LuaChatDanMuTemplate:OnEnable()
    EventManager.AddListenerInternal(EnumEventType.NewVoiceTextReady, self.m_OnNewVoiceTextReady)
end

function LuaChatDanMuTemplate:OnDisable()
    EventManager.RemoveListenerInternal(EnumEventType.NewVoiceTextReady, self.m_OnNewVoiceTextReady)
    LuaTweenUtils.DOKill(self.transform, false)
end
--@endregion UIEvent

