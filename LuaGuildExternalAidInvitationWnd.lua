local QnTableView = import "L10.UI.QnTableView"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CChatHelper = import "L10.UI.CChatHelper"

LuaGuildExternalAidInvitationWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaGuildExternalAidInvitationWnd, "TableView", "TableView", QnTableView)

--@endregion RegistChildComponent end
RegistClassMember(LuaGuildExternalAidInvitationWnd,"m_List")
RegistClassMember(LuaGuildExternalAidInvitationWnd,"m_SelectRow")
RegistClassMember(LuaGuildExternalAidInvitationWnd,"m_TimeLabelList")
RegistClassMember(LuaGuildExternalAidInvitationWnd,"m_UpdateTimeTick")

function LuaGuildExternalAidInvitationWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaGuildExternalAidInvitationWnd:Init()
    self.m_TimeLabelList = {}
    self.TableView.m_DataSource = DefaultTableViewDataSource.Create(function() 
        return self.m_List and #self.m_List or 0
    end, function(item, index)
        self:InitItem(item, index)
    end)

    self.TableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
        self:OnSelectAtRow(row)
    end)
    self:OnSendGuildForeignAidInviteInfo()
    self:CancelUpdateTimeTick()
    self.m_UpdateTimeTick = RegisterTick(function ()
        self:UpdateTimeList()
    end,500)
end

function LuaGuildExternalAidInvitationWnd:UpdateTimeList()
    for index, info in pairs(self.m_TimeLabelList) do
        local label ,time = info.label,info.time
        local restTime = time - CServerTimeMgr.Inst.timeStamp
        if label then
            label.text = SafeStringFormat3(LocalString.GetString("%02d:%02d:%02d"),math.floor(restTime / 3600), math.floor((restTime % 3600) / 60), restTime % 60)
        end
        if restTime == 0 then
            label.text = ""
        end    
    end
end

function LuaGuildExternalAidInvitationWnd:CancelUpdateTimeTick()
    if self.m_UpdateTimeTick then
        UnRegisterTick(self.m_UpdateTimeTick)
        self.m_UpdateTimeTick = nil
    end
end

function LuaGuildExternalAidInvitationWnd:InitItem(item, index)
    local data = self.m_List[index + 1]
    local guildName = data.guildName
    local endTime = data.expireTime
    local restTime = data.expireTime

    local guildNameLabel = item.transform:Find("GuildNameLabel"):GetComponent(typeof(UILabel))
    local rankLabel = item.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
    local timeLabel = item.transform:Find("TimeLabel"):GetComponent(typeof(UILabel))
    local talkButton = item.transform:Find("TalkButton").gameObject
    local joinButton = item.transform:Find("JoinButton").gameObject
    local refuseButton = item.transform:Find("RefuseButton").gameObject

    UIEventListener.Get(talkButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTalkButtonClick(index)
	end)
    UIEventListener.Get(joinButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnJoinButtonClick(index)
	end)
    UIEventListener.Get(refuseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRefuseButtonClick(index)
	end)

    guildNameLabel.text = SafeStringFormat3(LocalString.GetString("%s发出邀请"),guildName)
    timeLabel.text = SafeStringFormat3(LocalString.GetString("%02d:%02d:%02d"),math.floor(restTime / 3600), math.floor((restTime % 3600) / 60), restTime % 60)
    local groupNameArray = {
        LocalString.GetString("甲级"),
        LocalString.GetString("乙级"),
        LocalString.GetString("丙级"),
        LocalString.GetString("丁级"),
        LocalString.GetString("戊级"),
        LocalString.GetString("己级"),
        LocalString.GetString("庚级"),
        LocalString.GetString("辛级"),
        LocalString.GetString("壬级"),
        LocalString.GetString("癸级"),
        
        LocalString.GetString("子级"),
        LocalString.GetString("丑级"),
        LocalString.GetString("寅级"),
        LocalString.GetString("卯级"),
        LocalString.GetString("辰级"),
        LocalString.GetString("巳级"),
        LocalString.GetString("午级"),
        LocalString.GetString("未级"),
        LocalString.GetString("申级"),
        LocalString.GetString("酉级"),
        LocalString.GetString("戌级"),
        LocalString.GetString("亥级")
    }
    rankLabel.text = (data.leagueRank == 0 or data.leagueLevel == 0) and "" or SafeStringFormat3(LocalString.GetString("%s第%d名"),groupNameArray[data.leagueLevel], data.leagueRank)
    self.m_TimeLabelList[index] = {label = timeLabel, time = CServerTimeMgr.Inst.timeStamp + restTime}
end

function LuaGuildExternalAidInvitationWnd:OnTalkButtonClick(row)
    local data = self.m_List[row + 1]
	CChatHelper.ShowFriendWnd(data.leaderId, data.leaderName)
end

function LuaGuildExternalAidInvitationWnd:OnJoinButtonClick(index)
    local data = self.m_List[index + 1]
    LuaGuildExternalAidMgr:GuildForeignAidApply(data.guildId,"")
end

function LuaGuildExternalAidInvitationWnd:OnRefuseButtonClick(index)
    local data = self.m_List[index + 1]
    LuaGuildExternalAidMgr:GuildForeignAidCancelApply(data.guildId)
end

function LuaGuildExternalAidInvitationWnd:OnSelectAtRow(row)
    self.m_SelectRow = row
    local data = self.m_List[row + 1]

end
--@region UIEvent

--@endregion UIEvent
function LuaGuildExternalAidInvitationWnd:OnEnable()
    g_ScriptEvent:AddListener("OnSendGuildForeignAidInviteInfo", self, "OnSendGuildForeignAidInviteInfo")
end

function LuaGuildExternalAidInvitationWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnSendGuildForeignAidInviteInfo", self, "OnSendGuildForeignAidInviteInfo")
    self:CancelUpdateTimeTick()
end

function LuaGuildExternalAidInvitationWnd:OnSendGuildForeignAidInviteInfo()
    self.m_List = LuaGuildExternalAidMgr.m_InviteInfo
    table.sort(self.m_List,function(a,b)
        return a.expireTime > b.expireTime
    end)
    self.m_TimeLabelList = {}
    self.TableView:ReloadData(true, true)
end