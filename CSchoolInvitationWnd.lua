-- Auto Generated!!
local CSchoolInvitationWnd = import "L10.UI.CSchoolInvitationWnd"
local Gac2Gas = import "L10.Game.Gac2Gas"
CSchoolInvitationWnd.m_OnOkButtonClick_CS2LuaHook = function (this, go) 
    local campusCode = 0
    local default
    default, campusCode = System.UInt32.TryParse(this.codeLabel.text)
    if default then
        Gac2Gas.SubmitCampusCode(campusCode)
    else
        g_MessageMgr:ShowMessage("INPUT_WRONG_CODE")
    end
end
