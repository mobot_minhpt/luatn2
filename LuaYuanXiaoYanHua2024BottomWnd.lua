local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"

local QnTableView = import "L10.UI.QnTableView"
local GameObject = import "UnityEngine.GameObject"
local UITabBar = import "L10.UI.UITabBar"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local Animation = import "UnityEngine.Animation"

LuaYuanXiaoYanHua2024BottomWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaYuanXiaoYanHua2024BottomWnd, "TableView", "TableView", QnTableView)
RegistChildComponent(LuaYuanXiaoYanHua2024BottomWnd, "UseBtn", "UseBtn", GameObject)
RegistChildComponent(LuaYuanXiaoYanHua2024BottomWnd, "PreviewBtn", "PreviewBtn", GameObject)
RegistChildComponent(LuaYuanXiaoYanHua2024BottomWnd, "TabBar", "TabBar", UITabBar)
RegistChildComponent(LuaYuanXiaoYanHua2024BottomWnd, "MoneyPocket", "MoneyPocket", GameObject)
RegistChildComponent(LuaYuanXiaoYanHua2024BottomWnd, "MoneyCtrl1", "MoneyCtrl1", CCurentMoneyCtrl)
RegistChildComponent(LuaYuanXiaoYanHua2024BottomWnd, "MoneyCtrl2", "MoneyCtrl2", CCurentMoneyCtrl)
RegistChildComponent(LuaYuanXiaoYanHua2024BottomWnd, "CloseBtn", "CloseBtn", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaYuanXiaoYanHua2024BottomWnd,"m_Animation")
RegistClassMember(LuaYuanXiaoYanHua2024BottomWnd,"m_CloseTick")

function LuaYuanXiaoYanHua2024BottomWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.UseBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnUseBtnClick()
	end)


	
	UIEventListener.Get(self.PreviewBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnPreviewBtnClick()
	end)


	
	UIEventListener.Get(self.CloseBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCloseBtnClick()
	end)


    --@endregion EventBind end
	self.m_Animation = self.transform:GetComponent(typeof(Animation))
end

function LuaYuanXiaoYanHua2024BottomWnd:OnEnable()
	self.m_Animation:Play("yuanxiaoyanhua2024bottomwnd_show")
end

function LuaYuanXiaoYanHua2024BottomWnd:OnDisable()
	if self.m_CloseTick then
		UnRegisterTick(self.m_CloseTick)
		self.m_CloseTick = nil
	end
end

function LuaYuanXiaoYanHua2024BottomWnd:Init()

end

--@region UIEvent

function LuaYuanXiaoYanHua2024BottomWnd:OnUseBtnClick()
end

function LuaYuanXiaoYanHua2024BottomWnd:OnPreviewBtnClick()
end


function LuaYuanXiaoYanHua2024BottomWnd:OnCloseBtnClick()
	self.m_Animation:Play("yuanxiaoyanhua2024bottomwnd_hide")
	self.m_CloseTick = RegisterTickOnce(function()
		CUIManager.CloseUI(CLuaUIResources.YuanXiaoYanHua2024BottomWnd)
		self.m_CloseTick = nil
	end,1000)
end


--@endregion UIEvent

