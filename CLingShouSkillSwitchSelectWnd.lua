-- Auto Generated!!
local Boolean = import "System.Boolean"
local CLingShouMgr = import "L10.Game.CLingShouMgr"
local CLingShouSkillSwitchSelectItem = import "L10.UI.CLingShouSkillSwitchSelectItem"
local CLingShouSkillSwitchSelectWnd = import "L10.UI.CLingShouSkillSwitchSelectWnd"
local CLingShouSwitchSkillMgr = import "L10.Game.CLingShouSwitchSkillMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CUICommonDef = import "L10.UI.CUICommonDef"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local GameObject = import "UnityEngine.GameObject"
local LocalString = import "LocalString"
local UInt32 = import "System.UInt32"
local Vector3 = import "UnityEngine.Vector3"
CLingShouSkillSwitchSelectWnd.m_Init_CS2LuaHook = function (this) 
    if CLingShouSwitchSkillMgr.Inst.SwitchAdvanceSkill then
        --显示两个技能，然后是选择的模式
        this.lingshouId = CLingShouSwitchSkillMgr.Inst.srcLingShouId
        local count = 0
        if CLingShouSwitchSkillMgr.Inst.AdvanceSkillToSwitch > 0 then
            count = 1
        end
        this.titleLabel.text = System.String.Format(LocalString.GetString("选择要转移的技能 {0}/{1}"), count, 1)
    else
        this.lingshouId = CLingShouSwitchSkillMgr.Inst.destLingShouId
        this.titleLabel.text = System.String.Format(LocalString.GetString("锁定不被替换的技能 {0}/{1}"), CLingShouSwitchSkillMgr.Inst.LockedSkillCount, CLingShouSwitchSkillMgr.Inst.CanLockCount)
    end
    this:GetLingShouDetails(this.lingshouId)
end
CLingShouSkillSwitchSelectWnd.m_OnEnable_CS2LuaHook = function (this) 
    --EventManager.AddListener<string, CLingShouMgr.LingShouDetails>(EnumEventType.GetLingShouDetails, GetLingShouDetails);
    --EventManager.AddListener<string, CLingShouMgr.LingShouDetails>(EnumEventType.GetLingShouDetails, GetLingShouDetails);
    EventManager.AddListener(EnumEventType.LingShouSwitchSkill_UpdateLockCount, MakeDelegateFromCSFunction(this.UpdateLockCount, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.LingShouSwitchSkill_ChooseSkill, MakeDelegateFromCSFunction(this.OnChooseSkill, MakeGenericClass(Action2, UInt32, Boolean), this))
end
CLingShouSkillSwitchSelectWnd.m_OnChooseSkill_CS2LuaHook = function (this, id, state) 
    local count = 0
    if CLingShouSwitchSkillMgr.Inst.AdvanceSkillToSwitch > 0 then
        count = 1
    end
    this.titleLabel.text = System.String.Format(LocalString.GetString("选择要转移的技能 {0}/{1}"), count, 1)
end
CLingShouSkillSwitchSelectWnd.m_UpdateLockCount_CS2LuaHook = function (this) 
    if not CLingShouSwitchSkillMgr.Inst.SwitchAdvanceSkill then
        this.titleLabel.text = System.String.Format(LocalString.GetString("技能锁定{0}/{1}"), CLingShouSwitchSkillMgr.Inst.LockedSkillCount, CLingShouSwitchSkillMgr.Inst.CanLockCount)
    end
end
CLingShouSkillSwitchSelectWnd.m_GetLingShouDetails_CS2LuaHook = function (this, lingShouId) 
    local details = CLingShouMgr.Inst:GetLingShouDetails(this.lingshouId)
    if details == nil then
        return
    end
    --if (lingShouId == this.lingshouId)
    --{
    CUICommonDef.ClearTransform(this.grid.transform)
    local skills = details.data.Props.SkillListForSave
    if CLingShouSwitchSkillMgr.Inst.SwitchAdvanceSkill then
        for i = 1, 2 do
            local obj = TypeAs(CommonDefs.Object_Instantiate(this.itemPrefab), typeof(GameObject))
            obj.transform.parent = this.grid.transform
            obj.transform.localScale = Vector3.one
            obj:SetActive(true)
            local cmp = CommonDefs.GetComponent_GameObject_Type(obj, typeof(CLingShouSkillSwitchSelectItem))
            cmp:Init(skills[i])
        end
    else
        for i = 3, 8 do
            local obj = TypeAs(CommonDefs.Object_Instantiate(this.itemPrefab), typeof(GameObject))
            obj.transform.parent = this.grid.transform
            obj.transform.localScale = Vector3.one
            obj:SetActive(true)
            local cmp = CommonDefs.GetComponent_GameObject_Type(obj, typeof(CLingShouSkillSwitchSelectItem))
            cmp:Init(skills[i])
        end
    end
end
