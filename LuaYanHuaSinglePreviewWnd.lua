local CUITexture=import "L10.UI.CUITexture"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local LayerDefine = import "L10.Engine.LayerDefine"
local CEffectMgr = import "L10.Game.CEffectMgr"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"

local CTickMgr = import "L10.Engine.CTickMgr"
local DelegateFactory = import "DelegateFactory"
local ETickType = import "L10.Engine.ETickType"
local Camera = import "UnityEngine.Camera"
local Color = import "UnityEngine.Color"

CLuaYanHuaSinglePreviewWnd = class()

RegistChildComponent(CLuaYanHuaSinglePreviewWnd, "m_ModelTexture","Preview",CUITexture)
RegistClassMember(CLuaYanHuaSinglePreviewWnd, "m_ModelTextureLoader")
RegistClassMember(CLuaYanHuaSinglePreviewWnd, "m_YanHuaInfo")
RegistClassMember(CLuaYanHuaSinglePreviewWnd, "m_FxList")

function CLuaYanHuaSinglePreviewWnd:Awake()
    self.m_ModelTextureLoader = LuaDefaultModelTextureLoader.Create(function (ro)
        self:LoadModel(ro)
    end)
    self.m_FxList = {}
end

function CLuaYanHuaSinglePreviewWnd:Init()
    self.m_ModelTexture.mainTexture = CUIManager.CreateModelTexture("__yanhua_preview__", self.m_ModelTextureLoader,0,0.05,-8,30,false,true,1)--,0.05,-8,30
    local index = CLuaYanHuaEditorMgr.CurPreViewLiBaoIndex
    self.m_YanHuaInfo = CLuaYanHuaEditorMgr.YanHuaLightListTbl[index]
    local type = self.m_YanHuaInfo.type
    if type == "normal" then
        self:PreviewNormalFx()
    elseif type == "custom" then
        self:PreviewCustomFx()
    end
    self.cancelTick = CTickMgr.Register(DelegateFactory.Action(function () 
        local index = CLuaYanHuaEditorMgr.CurPreViewLiBaoIndex
        self.m_YanHuaInfo = CLuaYanHuaEditorMgr.YanHuaLightListTbl[index]
        local type = self.m_YanHuaInfo.type
        if type == "normal" then
            self:PreviewNormalFx()
        elseif type == "custom" then
            self:PreviewCustomFx()
        end
    end), 3000, ETickType.Loop)
end

function CLuaYanHuaSinglePreviewWnd:PreviewNormalFx()
    local info = self.m_YanHuaInfo
    local pos = self.m_Pos
    local data = YanHua_Default.GetData(info.normalId)
    if not data then return end

    local colors = nil
    if info.colorIndex then
        local color = CLuaYanHuaEditorMgr.Colors[info.colorIndex]
        colors = CreateFromClass(MakeArrayClass(Color), 1)
        colors[0] = color
    end

    local size = 1
    if info.size then
        size = info.size
    end
    local fxid1 = data.FxID1
    local fxid2 = data.FxID2
    if data.ShengkongEffect == 1 then
        local layer = LayerDefine.ModelForNGUI_3D
        local fx1 = CEffectMgr.Inst:AddCustomYanHuaWpFX(fxid2, pos, 0,1,colors,nil,0, 1, layer, EnumWarnFXType.None, nil, 0, 0)--self.m_RO.transform
        local fx2 = CEffectMgr.Inst:AddCustomYanHuaWpFX(fxid1, Vector3(pos.x,pos.y+10,pos.z), 0, size,colors,nil,1000, 1, layer, EnumWarnFXType.None, nil, 0, 0)
        table.insert(self.m_FxList,fx1)
        table.insert(self.m_FxList,fx2)
    else
        local layer = LayerDefine.ModelForNGUI_3D
        local fx = CEffectMgr.Inst:AddCustomYanHuaWpFX(fxid1, Vector3(pos.x,pos.y+1,pos.z), 200, size,colors,nil,0, 1, layer, EnumWarnFXType.None, nil, 0, 0)
        table.insert(self.m_FxList,fx)
    end
end

function CLuaYanHuaSinglePreviewWnd:PreviewCustomFx()
    local info = self.m_YanHuaInfo
    local pos = self.m_Pos
    local canvasIndex = CLuaYanHuaEditorMgr.CurPreviewCanvasIndex
    canvasIndex = tonumber(canvasIndex)
    local texture = CLuaYanHuaEditorMgr.MyYanHuaPicData[canvasIndex].texture

    local size = 1
    if info.size then
        size = info.size
    end

    local layer = LayerDefine.ModelForNGUI_3D
    local fx1 = CEffectMgr.Inst:AddCustomYanHuaWpFX(88801851, pos, 0,1,nil,nil,0, 1, layer, EnumWarnFXType.None, nil, 0, 0)--self.m_RO.transform
    local fx2 = CEffectMgr.Inst:AddCustomYanHuaWpFX(88801838, Vector3(pos.x,pos.y+10,pos.z), 240, size,nil,texture,1000, 1, layer, EnumWarnFXType.None, nil, 0, 0)
    table.insert(self.m_FxList,fx1)
    table.insert(self.m_FxList,fx2)
end


function CLuaYanHuaSinglePreviewWnd:LoadModel(ro) 
    self.m_Pos = ro.transform.position
    self.m_Pos2 = Vector3(self.m_Pos.x,self.m_Pos.y+10,self.m_Pos.z)
    self.m_RO = ro

    self.m_Camera = ro.transform.parent.parent:GetComponent(typeof(Camera))
    self.m_Camera.farClipPlane = 30

    self:CancelTick()
end
function CLuaYanHuaSinglePreviewWnd:OnDestroy()
    self.m_ModelTexture.mainTexture=nil
    CUIManager.DestroyModelTexture("__yanhua_preview__")
    for i,fx in ipairs(self.m_FxList) do 
        if fx then
            fx:Destroy()
        end
    end
end
function CLuaYanHuaSinglePreviewWnd:CancelTick()
    if self.cancelTick ~= nil then
        invoke(self.cancelTick)
        self.cancelTick = nil
    end
end
function CLuaYanHuaSinglePreviewWnd:OnDisable()
	if self.cancelTick ~= nil then
		UnRegisterTick(self.cancelTick)
	end
end
