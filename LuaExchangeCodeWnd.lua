local CExchangeCodeWnd = import "L10.UI.CExchangeCodeWnd"
local CLoginMgr = import "L10.Game.CLoginMgr"

local getProjectId = function()
    if CommonDefs.IS_VN_CLIENT then return "l10vn" end
    return "l10"
end

CExchangeCodeWnd.m_hookGetParams = function(this, sn)
    return  "cdkey_sn?gameid="  ..getProjectId()..
            "&sn="              ..sn..
            "&acct="            ..CLoginMgr.Inst.SyncUsername..
            "&aid="             ..CClientMainPlayer.Inst.BasicProp.ClientData.Extra.Aid..
            "&hostnum="         ..CClientMainPlayer.Inst:GetMyServerId()..
            "&unum="            ..CClientMainPlayer.Inst.Id..
            "&app_channel="     ..CLoginMgr.Inst:GetAppChannel()..
            "&grade="           ..CClientMainPlayer.Inst.Level
end

CExchangeCodeWnd.m_hookRetMsg = function(this, retcode, url) 
    if CExchangeCodeWnd.s_EnableTestEnvironment then 
        g_MessageMgr:ShowCustomMsg("retcode: " .. retcode .. "\nurl: " .. url); 
    end 
    local data = Message_GiftCode.GetData(retcode)
    g_MessageMgr:ShowMessage(data and data.name or "GC_ERROR_USED" ) 
end
