-- Auto Generated!!
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CLivingSkillMgr = import "L10.Game.CLivingSkillMgr"
local CLivingSkillQuickMakeMgr = import "L10.UI.CLivingSkillQuickMakeMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local ItemGet_item = import "L10.Game.ItemGet_item"
local LifeSkill_Item = import "L10.Game.LifeSkill_Item"
local LifeSkill_SkillType = import "L10.Game.LifeSkill_SkillType"
local LocalString = import "LocalString"
local StringActionKeyValuePair = import "L10.Game.StringActionKeyValuePair"
local UInt32 = import "System.UInt32"
CLivingSkillQuickMakeMgr.m_GetActionPairs_CS2LuaHook = function (this, itemId, templateId) 
    --采集、钓鱼、前往制作
    --采集、钓鱼、前往制作
    local actionPair = CreateFromClass(StringActionKeyValuePair, this.mButtonName, MakeDelegateFromCSFunction(this.CommonEntry, Action0, this))
    local actionPairs = CreateFromClass(MakeGenericClass(List, StringActionKeyValuePair))
    CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), actionPair)
    return actionPairs
end
CLivingSkillQuickMakeMgr.m_PreCheckLevel_CS2LuaHook = function (templateId) 
    local data = LifeSkill_Item.GetData(templateId)
    local requireLevel = data.SkillLev
    if CommonDefs.DictContains(CClientMainPlayer.Inst.SkillProp.LifeSkillGradeMap, typeof(UInt32), data.SkillId) then
        local skillLevel = CommonDefs.DictGetValue(CClientMainPlayer.Inst.SkillProp.LifeSkillGradeMap, typeof(UInt32), data.SkillId)
        if skillLevel < requireLevel then
            return false
        else
            return true
        end
    end
    return false
end
CLivingSkillQuickMakeMgr.m_CommonEntry_CS2LuaHook = function (this) 
    --CUIManager.CloseUI(CIndirectUIResources.ItemInfoWnd);
    if this.mCallback ~= nil then
        invoke(this.mCallback)
    end
end
CLivingSkillQuickMakeMgr.m_ShowMakeTipWnd_CS2LuaHook = function (this, templateId) 
    this.mTemplateId = templateId
    local data = LifeSkill_Item.GetData(templateId)
    if data == nil then
        --CLogMgr.LogError("not LifeSkill_Item :" + templateId);
        this.mCallback = MakeDelegateFromCSFunction(this.ShowItemAccessInfo, Action0, this)
        this.mButtonName = LocalString.GetString("获取")
        CItemInfoMgr.ShowLinkItemTemplateInfo(templateId, false, this, AlignType.Default, 0, 0, 0, 0)
        return
    end
    local skillTypeData = LifeSkill_SkillType.GetData(data.SkillId)
    this.mCallback = nil
    if ItemGet_item.Exists(this.mTemplateId) then
        this.mCallback = MakeDelegateFromCSFunction(this.ShowItemAccessInfo, Action0, this)
    else
        this.mCallback = MakeDelegateFromCSFunction(this.GoTo, Action0, this)
    end
    repeat
        local default = skillTypeData.Type
        if default == 1 then
            this.mButtonName = LocalString.GetString("钓鱼")
            break
        elseif default == 2 then
            this.mButtonName = LocalString.GetString("采集")
            break
        elseif default == 3 then
            this.mButtonName = LocalString.GetString("前往制作")
            break
        elseif default == 4 then
            this.mButtonName = LocalString.GetString("前往制作")
            break
        elseif default == 5 then
            this.mButtonName = LocalString.GetString("获取")
            break
        end
    until 1
    CItemInfoMgr.ShowLinkItemTemplateInfo(templateId, false, this, AlignType.Default, 0, 0, 0, 0)
end
CLivingSkillQuickMakeMgr.m_GoTo_CS2LuaHook = function (this) 
    if not CLivingSkillQuickMakeMgr.PreCheckLevel(this.mTemplateId) then
        g_MessageMgr:ShowMessage("USE_LIFE_SKILL_HIGH_LV")
        return
    end
    CLivingSkillMgr.Inst:GoTo(this.mTemplateId)
end
