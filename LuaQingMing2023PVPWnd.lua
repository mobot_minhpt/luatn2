local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CTipParagraphItem = import "L10.UI.CTipParagraphItem"
local MessageMgr = import "L10.Game.MessageMgr"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local CQnReturnAwardTemplate = import "L10.UI.CQnReturnAwardTemplate"
local CItemMgr = import "L10.Game.CItemMgr"
local CCarnivalLotteryMgr = import "L10.Game.CCarnivalLotteryMgr"
local CUITexture = import "L10.UI.CUITexture"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CChatLinkMgr = import "CChatLinkMgr"

LuaQingMing2023PVPWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaQingMing2023PVPWnd, "RuleTemplate", GameObject)
RegistChildComponent(LuaQingMing2023PVPWnd, "RuleTabel", UITable)
RegistChildComponent(LuaQingMing2023PVPWnd, "RankBtn", GameObject)
RegistChildComponent(LuaQingMing2023PVPWnd, "PersonBtn", GameObject)
RegistChildComponent(LuaQingMing2023PVPWnd, "TeamBtn", GameObject)
RegistChildComponent(LuaQingMing2023PVPWnd, "CancelBtn", GameObject)
RegistChildComponent(LuaQingMing2023PVPWnd, "ItemCell1", CQnReturnAwardTemplate)
RegistChildComponent(LuaQingMing2023PVPWnd, "ItemCell2", CQnReturnAwardTemplate)
RegistChildComponent(LuaQingMing2023PVPWnd, "RuleImage", CUITexture)
RegistChildComponent(LuaQingMing2023PVPWnd, "RuleLabel", UILabel)
RegistChildComponent(LuaQingMing2023PVPWnd, "NextArrow", GameObject)
RegistChildComponent(LuaQingMing2023PVPWnd, "LastArrow", GameObject)
RegistChildComponent(LuaQingMing2023PVPWnd, "TitleLabel", UILabel)

RegistClassMember(LuaQingMing2023PVPWnd, "m_GamePlayId")
RegistClassMember(LuaQingMing2023PVPWnd, "m_RuleIndex")

--@endregion RegistChildComponent end

function LuaQingMing2023PVPWnd:Awake()
    UIEventListener.Get(self.RankBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnRankBtnClick()
    end)
    UIEventListener.Get(self.PersonBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnPersonBtnClick()
    end)
    UIEventListener.Get(self.TeamBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnTeamBtnClick()
    end)
    UIEventListener.Get(self.CancelBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnCancelBtnClick()
    end)
    
    UIEventListener.Get(self.NextArrow).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnNextArrowClick()
    end)
    UIEventListener.Get(self.LastArrow).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnLastArrowClick()
    end)
end

function LuaQingMing2023PVPWnd:Init()
    self.m_RuleIndex = 1
    LuaQingMing2023Mgr:GetRuleList()
    self:UpdateImageRule()

    local setData = QingMing2023_PVPSetting.GetData()
    self.m_GamePlayId = setData.GamePlayId
    self:InitRuleDesc()
    self:InitRewardInfo(setData.WinAwardItemId, setData.EngageAwardItemId)

    if CClientMainPlayer.Inst then
        Gac2Gas.GlobalMatch_RequestCheckSignUp(self.m_GamePlayId, CClientMainPlayer.Inst.Id)
    end
end

function LuaQingMing2023PVPWnd:InitRuleDesc()
    self.RuleTemplate:SetActive(false)
    Extensions.RemoveAllChildren(self.RuleTabel.transform)
	local ruleMessage = MessageMgr.Inst:FormatMessage("QINGMING2023_PVP_PLAY_TIPS", {})
	local ruleTipInfo = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(ruleMessage)
    if ruleTipInfo == nil then return end
	for i = 0, ruleTipInfo.paragraphs.Count - 1 do
		local template = CUICommonDef.AddChild(self.RuleTabel.gameObject, self.RuleTemplate)
		template:GetComponent(typeof(CTipParagraphItem)):Init(ruleTipInfo.paragraphs[i], 0x553E37ff)
		template:SetActive(true)
	end
	self.RuleTabel:Reposition()
end

function LuaQingMing2023PVPWnd:InitRewardInfo(winRewardId, DailyRewardId)
    self.ItemCell1:Init(CItemMgr.Inst:GetItemTemplate(winRewardId), 1)
    self.ItemCell1.transform:Find("mask").gameObject:SetActive(CCarnivalLotteryMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eQingMing2023PVPWinnerAward) ~= 0)
    self.ItemCell2:Init(CItemMgr.Inst:GetItemTemplate(DailyRewardId), 1)
    self.ItemCell2.transform:Find("mask").gameObject:SetActive(CCarnivalLotteryMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eQingMing2023PVPEngageAward) ~= 0)
end

function LuaQingMing2023PVPWnd:UpdateImageRule()
    self.RuleImage:LoadMaterial(LuaQingMing2023Mgr.m_PVP_RuleList[self.m_RuleIndex].ImageRulePath)
    self.RuleLabel.text = CChatLinkMgr.TranslateToNGUIText(LuaQingMing2023Mgr.m_PVP_RuleList[self.m_RuleIndex].ImageRuleString)
    self.TitleLabel.text = LuaQingMing2023Mgr.m_PVP_RuleList[self.m_RuleIndex].Name
end

function LuaQingMing2023PVPWnd:UpdateMatchButton(isMatching)
    -- 改变下方报名按钮状态
    self.PersonBtn:SetActive(not isMatching)
    self.TeamBtn:SetActive(not isMatching)
    self.CancelBtn:SetActive(isMatching)
end

function LuaQingMing2023PVPWnd:OnGlobalMatch_CheckInMatchingResult(playerId, playId, isInMatching, resultStr)
    if playId == self.m_GamePlayId then
        self:UpdateMatchButton(isInMatching)
    end
end

function LuaQingMing2023PVPWnd:OnGlobalMatch_SignUpPlayResult(playId, success)
    if playId == self.m_GamePlayId and success then
        self:UpdateMatchButton(true)
    end
end

function LuaQingMing2023PVPWnd:OnGlobalMatch_CancelSignUpResult(playId, success)
    if playId == self.m_GamePlayId and success then
        self:UpdateMatchButton(false)
    end
end

--@region UIEvent
function LuaQingMing2023PVPWnd:OnRankBtnClick()
    Gac2Gas.QingMing2023PvpQueryTopRank()
end

function LuaQingMing2023PVPWnd:OnPersonBtnClick()
    Gac2Gas.GlobalMatch_RequestSignUp(self.m_GamePlayId)
end
function LuaQingMing2023PVPWnd:OnTeamBtnClick()
    Gac2Gas.EnterQingMing2023PVP()
end
function LuaQingMing2023PVPWnd:OnCancelBtnClick()
	Gac2Gas.GlobalMatch_RequestCancelSignUp(self.m_GamePlayId)
end

function LuaQingMing2023PVPWnd:OnNextArrowClick()
    self.m_RuleIndex = self.m_RuleIndex >= #LuaQingMing2023Mgr.m_PVP_RuleList and 1 or self.m_RuleIndex + 1
    if LuaQingMing2023Mgr.m_PVP_RuleList[self.m_RuleIndex].ImageRulePath == "" then
        self:OnNextArrowClick()
    else
        self:UpdateImageRule()
    end
end
function LuaQingMing2023PVPWnd:OnLastArrowClick()
    self.m_RuleIndex = self.m_RuleIndex <= 1 and #LuaQingMing2023Mgr.m_PVP_RuleList or self.m_RuleIndex - 1
    if LuaQingMing2023Mgr.m_PVP_RuleList[self.m_RuleIndex].ImageRulePath == "" then
        self:OnLastArrowClick()
    else
        self:UpdateImageRule()
    end
end
--@endregion UIEvent

function LuaQingMing2023PVPWnd:OnEnable()
    g_ScriptEvent:AddListener("GlobalMatch_CheckInMatchingResult", self, "OnGlobalMatch_CheckInMatchingResult")
    g_ScriptEvent:AddListener("GlobalMatch_SignUpPlayResult", self, "OnGlobalMatch_SignUpPlayResult")
    g_ScriptEvent:AddListener("GlobalMatch_CancelSignUpResult", self, "OnGlobalMatch_CancelSignUpResult")
end

function LuaQingMing2023PVPWnd:OnDisable()
    g_ScriptEvent:RemoveListener("GlobalMatch_CheckInMatchingResult", self, "OnGlobalMatch_CheckInMatchingResult")
    g_ScriptEvent:RemoveListener("GlobalMatch_SignUpPlayResult", self, "OnGlobalMatch_SignUpPlayResult")
    g_ScriptEvent:RemoveListener("GlobalMatch_CancelSignUpResult", self, "OnGlobalMatch_CancelSignUpResult")
end
