local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CButton = import "L10.UI.CButton"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CNumberKeyBoardMgr = import "CNumberKeyBoardMgr"
local AlignType = import "L10.UI.CTooltip+AlignType"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local QnRichLabel = import "QnRichLabel"
local UIWidget = import "UIWidget"

LuaNanDuFanHuaExchangeMoneyWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaNanDuFanHuaExchangeMoneyWnd, "CancelBtn", "CancelBtn", CButton)
RegistChildComponent(LuaNanDuFanHuaExchangeMoneyWnd, "OKBtn", "OKBtn", CButton)
RegistChildComponent(LuaNanDuFanHuaExchangeMoneyWnd, "CostLab", "CostLab", UILabel)
RegistChildComponent(LuaNanDuFanHuaExchangeMoneyWnd, "QnIncreseAndDecreaseButton", "QnIncreseAndDecreaseButton", GameObject)
RegistChildComponent(LuaNanDuFanHuaExchangeMoneyWnd, "Tip", "Tip", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaNanDuFanHuaExchangeMoneyWnd, "m_DecreaseButton")
RegistClassMember(LuaNanDuFanHuaExchangeMoneyWnd, "m_IncreaseButton")
RegistClassMember(LuaNanDuFanHuaExchangeMoneyWnd, "m_InputLab")
RegistClassMember(LuaNanDuFanHuaExchangeMoneyWnd, "m_InputNum")
RegistClassMember(LuaNanDuFanHuaExchangeMoneyWnd, "m_InputNumMax")

function LuaNanDuFanHuaExchangeMoneyWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.m_DecreaseButton   = self.QnIncreseAndDecreaseButton.transform:Find("DecreaseButton"):GetComponent(typeof(QnButton))
    self.m_IncreaseButton   = self.QnIncreseAndDecreaseButton.transform:Find("IncreaseButton"):GetComponent(typeof(QnButton))
    self.m_InputLab         = self.QnIncreseAndDecreaseButton.transform:Find("InputLab"):GetComponent(typeof(UILabel))

    UIEventListener.Get(self.m_DecreaseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnDecreaseButtonClick()
    end)

    UIEventListener.Get(self.m_IncreaseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnIncreaseButtonClick()
    end)

    UIEventListener.Get(self.m_InputLab.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnInputLabClick()
    end)

    UIEventListener.Get(self.OKBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnOKBtnClick()
    end)

    UIEventListener.Get(self.CancelBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnCancelBtnClick()
    end)

    self.lordData = {}
    if CClientMainPlayer.Inst and CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, LuaYiRenSiMgr.LordTempPlayDataKey) then
        local playData = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, LuaYiRenSiMgr.LordTempPlayDataKey)
        if playData.ExpiredTime > CServerTimeMgr.Inst.timeStamp and playData.Data and playData.Data.Data then
            local data = g_MessagePack.unpack(playData.Data.Data)
            self.lordData = data[1]
        end
    end
    local isLord = self.lordData[LuaYiRenSiMgr.LordPropertyKey.isLordIndex] and self.lordData[LuaYiRenSiMgr.LordPropertyKey.isLordIndex] == 1 or false
    self.jade = CClientMainPlayer.Inst.Jade + CClientMainPlayer.Inst.BindJade

    self.m_MoneyPerJade = isLord and NanDuFanHua_LordSetting.GetData().LordJade2Money or NanDuFanHua_LordSetting.GetData().Jade2Money
    self.Tip.gameObject:AddComponent(typeof(QnRichLabel))
    self.Tip.transform:GetComponent(typeof(UIWidget)).pivot = UIWidget.Pivot.Left
    self.Tip.EnableEmotion = true
    self.Tip.text = CUICommonDef.TranslateToNGUIText(isLord and g_MessageMgr:FormatMessage("NANDUFANHUA_LORD_EXCHANGEMONEY_TIP", self.jade) or g_MessageMgr:FormatMessage("NANDUFANHUA_EXCHANGEMONEY_TIP", self.jade))
end

function LuaNanDuFanHuaExchangeMoneyWnd:Init()
    self.m_InputNumMax = math.min(self.jade * self.m_MoneyPerJade, NanDuFanHua_LordSetting.GetData().MaxExchangeJadePerTime * self.m_MoneyPerJade)
    self.m_InputNumMax = math.max(0, self.m_InputNumMax)
    self:SetInputNum(self.m_MoneyPerJade)
end

--@region UIEvent

--@endregion UIEvent

function LuaNanDuFanHuaExchangeMoneyWnd:OnDecreaseButtonClick()
    self:SetInputNum(self.m_InputNum - self.m_MoneyPerJade)
end

function LuaNanDuFanHuaExchangeMoneyWnd:OnIncreaseButtonClick()
    self:SetInputNum(self.m_InputNum + self.m_MoneyPerJade)
end

function LuaNanDuFanHuaExchangeMoneyWnd:OnInputLabClick()
    CNumberKeyBoardMgr.ShowNumberKeyBoardWithRange(0, self.m_InputNumMax, self.m_InputNum, 100, DelegateFactory.Action_int(function (val)
        self.m_InputLab.text = val
    end), DelegateFactory.Action_int(function (val)
        local ceilVal = math.ceil(val / self.m_MoneyPerJade) * self.m_MoneyPerJade
        self:SetInputNum(ceilVal)
        --self:SetInputNum(val)
    end), self.m_InputLab, AlignType.Right, true)
end

function LuaNanDuFanHuaExchangeMoneyWnd:SetInputNum(num)
    if num < 0 then
        num = 0
    end

    if num > self.m_InputNumMax then
        num = self.m_InputNumMax
    end

    self.m_InputNum = num
    self.m_InputLab.text = num
    self.CostLab.text = num / self.m_MoneyPerJade

    self.m_IncreaseButton.Enabled = self.m_InputNumMax - num > 0
    self.m_DecreaseButton.Enabled = num > 0
    if self.m_InputNumMax - num < 0 then
        if self.m_IncreaseTick ~= nil then
            UnRegisterTick(self.m_IncreaseTick)
            self.m_IncreaseTick = nil
        end
    end

    if num < 0 then
        if self.m_DecreaseTick ~= nil then
            UnRegisterTick(self.m_DecreaseTick)
            self.m_DecreaseTick = nil
        end
    end
end

function LuaNanDuFanHuaExchangeMoneyWnd:OnCancelBtnClick()
    CUIManager.CloseUI(CLuaUIResources.NanDuFanHuaExchangeMoneyWnd)
end

function LuaNanDuFanHuaExchangeMoneyWnd:OnOKBtnClick()
    Gac2Gas.RequestBuyNanDuLordMoney(self.m_InputNum / self.m_MoneyPerJade)
    CUIManager.CloseUI(CLuaUIResources.NanDuFanHuaExchangeMoneyWnd)
end
