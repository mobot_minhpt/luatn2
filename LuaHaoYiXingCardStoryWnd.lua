local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local EShareType = import "L10.UI.EShareType"

LuaHaoYiXingCardStoryWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaHaoYiXingCardStoryWnd, "NameLabel", "NameLabel", UILabel)
RegistChildComponent(LuaHaoYiXingCardStoryWnd, "Texture", "Texture", CUITexture)
RegistChildComponent(LuaHaoYiXingCardStoryWnd, "DesLabel", "DesLabel", UILabel)
RegistChildComponent(LuaHaoYiXingCardStoryWnd, "ShareButton", "ShareButton", GameObject)
--@endregion RegistChildComponent end

function LuaHaoYiXingCardStoryWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    UIEventListener.Get(self.ShareButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareButtonClick()
	end)
    --@endregion EventBind end
end

function LuaHaoYiXingCardStoryWnd:Init()
    local data = SpokesmanTCG_ErHaTCG.GetData(LuaHaoYiXingMgr.m_PreviewWndPreviewCardID)
    self.NameLabel.text = data.Name
    self.DesLabel.text = data.Describe
    self.Texture:LoadMaterial(data.Res)
end

--@region UIEvent
function LuaHaoYiXingCardStoryWnd:OnShareButtonClick()
    CUICommonDef.CaptureScreen(
            "screenshot",
            true,
            false,
            self.ShareButton,
            DelegateFactory.Action_string_bytes(
                    function(fullPath, jpgBytes)
                        if  CLuaShareMgr.IsOpenShare() then
                            ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, fullPath)
                        else
                            CLuaShareMgr.SimpleShareLocalImage2PersonalSpace(fullPath)
                        end
                    end
            ),
            false
    )
end
--@endregion UIEvent

