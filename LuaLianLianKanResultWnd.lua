local Ease = import "DG.Tweening.Ease"
local Initialization_Init=import "L10.Game.Initialization_Init"
local Color=import "UnityEngine.Color"
local CUITexture=import "L10.UI.CUITexture"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local UIGrid=import "UIGrid"
CLuaLianLianKanResultWnd=class()
RegistClassMember(CLuaLianLianKanResultWnd,"m_ShareButton")

function CLuaLianLianKanResultWnd:Init()
    self.m_ShareButton=FindChild(self.transform,"Button").gameObject

    UIEventListener.Get(self.m_ShareButton).onClick=DelegateFactory.VoidDelegate(function(go)
        --统一分享 2018.12.03 cgz
        CUICommonDef.CaptureScreenAndShare()
    end)

    local resultLabel = FindChild(self.transform,"ResultLabel"):GetComponent(typeof(UILabel))
    resultLabel.text=SafeStringFormat3(LocalString.GetString("共闯%s关"),0)

    LuaTweenUtils.SetEase(LuaTweenUtils.TweenScale(resultLabel.transform, Vector3(0.8,0.8,1),Vector3(1,1,1),0.5), Ease.OutBack)
    LuaTweenUtils.TweenAlpha(resultLabel.transform, 0,1,0.3)


    local timeLabel = FindChild(self.transform,"TimeLabel"):GetComponent(typeof(UILabel))
    local minute=math.floor(CLuaLianLianKanMgr.m_ResultCostTime/60)
    local second=CLuaLianLianKanMgr.m_ResultCostTime%60
    timeLabel.text=SafeStringFormat3(LocalString.GetString("%d分%d秒"),minute,second)

    local portrait1 = FindChild(self.transform,"Portrait1"):GetComponent(typeof(CUITexture))
    local portrait2 = FindChild(self.transform,"Portrait2"):GetComponent(typeof(CUITexture))
    local descLabel1 = FindChild(self.transform,"DescLabel1"):GetComponent(typeof(UILabel))
    descLabel1.text=nil
    local descLabel2 = FindChild(self.transform,"DescLabel2"):GetComponent(typeof(UILabel))
    descLabel2.text=nil
    local nameLabel1 = FindChild(self.transform,"NameLabel1"):GetComponent(typeof(UILabel))
    nameLabel1.text=nil
    local nameLabel2 = FindChild(self.transform,"NameLabel2"):GetComponent(typeof(UILabel))
    nameLabel2.text=nil
    local scoreLabel1 = FindChild(self.transform,"ScoreLabel1"):GetComponent(typeof(UILabel))
    scoreLabel1.text=nil
    local scoreLabel2 = FindChild(self.transform,"ScoreLabel2"):GetComponent(typeof(UILabel))
    scoreLabel2.text=nil

    local parent = FindChild(self.transform,"Grid").gameObject
    Extensions.RemoveAllChildren(parent.transform)
    local itemPrefab = FindChild(self.transform,"Item").gameObject
    itemPrefab:SetActive(false)
    for i=1,5 do
        local go = NGUITools.AddChild(parent,itemPrefab)
        go:SetActive(true)
        self:InitItem(go.transform,i)
    end
    parent:GetComponent(typeof(UIGrid)):Reposition()

    local myId=CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
    -- CLuaLianLianKanMgr.m_ResultInfo
    local info1=nil
    local info2=nil
    if CLuaLianLianKanMgr.m_ResultInfo then
        info1=CLuaLianLianKanMgr.m_ResultInfo[1]
        info2=CLuaLianLianKanMgr.m_ResultInfo[2]
    end

    local initFunc=function(info,portrait,descLabel,nameLabel,scoreLabel)
        local initData=Initialization_Init.GetData(info.class*100+info.gender)
        if initData then
            portrait:LoadPortrait(initData.ResName,false)
        end
        if info.newRecord then
            descLabel.text=LocalString.GetString("新纪录")
            descLabel.color = Color(1,237/255,95/255)
        elseif info.maxscore>0 then
            descLabel.text=LocalString.GetString("今日最高".." "..tostring(info.maxscore))
            descLabel.color = Color(0.77,0.77,0.77)
        else
            descLabel.text=LocalString.GetString("今日最高".." "..LocalString.GetString("暂无"))
            descLabel.color = Color(0.77,0.77,0.77)
        end
        scoreLabel.text=tostring(info.score)
        nameLabel.text=info.name
        if info.playerId==myId then
            nameLabel.color=Color(0,1,0)
        else
            nameLabel.color=Color(1,1,1)
        end
    end

    if info1 then
        initFunc(info1,portrait1,descLabel1,nameLabel1,scoreLabel1)
        local count=0
        for i,v in ipairs(info1.questions) do
            if v.pass then
                count=count+1
            end
        end
        resultLabel.text=SafeStringFormat3(LocalString.GetString("共闯%s关"),count)
    end

    if info2 then
        initFunc(info2,portrait2,descLabel2,nameLabel2,scoreLabel2)
    end
end

function CLuaLianLianKanResultWnd:InitItem(transform,index)
    local answer1 = FindChild(transform,"AnswerLabel1"):GetComponent(typeof(UILabel))
    answer1.text=nil
    local answer2 = FindChild(transform,"AnswerLabel2"):GetComponent(typeof(UILabel))
    answer2.text=nil
    local result1 = FindChild(transform,"ResultLabel1"):GetComponent(typeof(UILabel))
    result1.text=nil
    local result2 = FindChild(transform,"ResultLabel2"):GetComponent(typeof(UILabel))
    result2.text=nil

    local resultSprite = FindChild(transform,"ResultSprite").gameObject
    local resultSprite2 = FindChild(transform,"ResultSprite2").gameObject
    local levelLabel = FindChild(transform,"LevelLabel"):GetComponent(typeof(UILabel))
    
    local info1=nil
    local info2=nil
    if CLuaLianLianKanMgr.m_ResultInfo then
        info1=CLuaLianLianKanMgr.m_ResultInfo[1]
        info2=CLuaLianLianKanMgr.m_ResultInfo[2]
    end
    

    local sameAnswer=false
    if info1 and info2 then
        local id1 = info1.questions[index] and info1.questions[index].answerIdx or 0
        local id2 = info2.questions[index] and info2.questions[index].answerIdx or 0
        if id1>0 and id1==id2 then
            sameAnswer=true
        end
    end

    local func=function(question,answerLabel,scoreLabel)
        local design = LianLianKan2018_Question.GetData(question.questionId)
        if question.answerIdx==1 then
            answerLabel.text=design.Answer1
        elseif question.answerIdx==2 then
            answerLabel.text=design.Answer2
        else
            answerLabel.text=nil
        end
        if sameAnswer then
            scoreLabel.text=SafeStringFormat3("%d[c][ffff00](+%d)[-][/c]",question.score,question.addscore)
            answerLabel.color=Color(1,1,0)
        else
            scoreLabel.text=tostring(question.score)
            answerLabel.color=Color(1,1,1)
        end
    end

    if info1 then
        local data = info1.questions[index]
        if data then
            func(data,answer1,result1)
            if data.pass then
                resultSprite:SetActive(true)
                resultSprite2:SetActive(false)
                levelLabel.gameObject:SetActive(false)
            else
                resultSprite:SetActive(false)
                resultSprite2:SetActive(true)
                levelLabel.gameObject:SetActive(false)
            end
        else
            resultSprite:SetActive(false)
            resultSprite2:SetActive(false)
            levelLabel.gameObject:SetActive(true)
            levelLabel.text=SafeStringFormat3(LocalString.GetString("第%s关"),index)
        end

    end

    if info2 then
        local data = info2.questions[index]
        if data then
            func(data,answer2,result2)
        end
    end

end
