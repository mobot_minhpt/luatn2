-- Auto Generated!!
local CLingShouBabyModelTextureLoader = import "L10.UI.CLingShouBabyModelTextureLoader"
local Color = import "UnityEngine.Color"
local Color32 = import "UnityEngine.Color32"
local CommonDefs = import "L10.Game.CommonDefs"
local CUIManager = import "L10.UI.CUIManager"
local LingShouBaby_BabyTemplate = import "L10.Game.LingShouBaby_BabyTemplate"
CLingShouBabyModelTextureLoader.m_Init_CS2LuaHook = function (this, templateId) 
    local data = LingShouBaby_BabyTemplate.GetData(templateId)
    if data ~= nil then
        local color = CreateFromClass(MakeGenericClass(List, Color))
        local colorArray = data.FXColor
        if colorArray ~= nil then
            do
                local i = 3
                while i < colorArray.Length do
                    CommonDefs.ListAdd(color, typeof(Color), CommonDefs.ImplicitConvert_Color_Color32(CreateFromClass(Color32, colorArray[i - 3], colorArray[i - 2], colorArray[i - 1], colorArray[i])))
                    i = i + 4
                end
            end
        end

        this.texture.mainTexture = this:CreateTexture(data.Name, data.PrefabPath, data.UIScale, data.FX, CommonDefs.ListToArray(color), data.FXScale)
    end
end
CLingShouBabyModelTextureLoader.m_CreateTexture_CS2LuaHook = function (this, Name, path, scale, FX, fxColor, fxScale) 
    this.Name = this.name
    this.path = path
    this.scale = scale
    this.FX = FX
    this.fxColor = fxColor
    this.fxScale = fxScale

    --this.resName = resName;
    --this.resId = resId;
    --this.scale = scale;
    --this.FX = FX;
    --this.fxColor = fxColor;
    --this.fxScale = fxScale;
    return CUIManager.CreateModelTexture(System.String.Format("__{0}__", this:GetInstanceID()), this, - 145, 0.05, -1, 4.66, false, true, 1)
end
