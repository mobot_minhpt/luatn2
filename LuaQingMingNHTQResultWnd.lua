local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"

LuaQingMingNHTQResultWnd = class()

RegistChildComponent(LuaQingMingNHTQResultWnd, "CloseButton", "CloseButton", GameObject)
RegistChildComponent(LuaQingMingNHTQResultWnd, "time", "time", UILabel)
RegistChildComponent(LuaQingMingNHTQResultWnd, "ItemCell", "ItemCell", GameObject)
RegistChildComponent(LuaQingMingNHTQResultWnd, "desc", "desc", UILabel)

function LuaQingMingNHTQResultWnd:Awake()
end

function LuaQingMingNHTQResultWnd:InitRestTime(time)
  local str = nil
  if time == 0 then
      str = '0:0'
  else
    local min = math.floor(time/60)
    local sec = time%60
    if sec < 10 then
      str = min..':0'..sec
    else
      str = min..':'..sec
    end
  end

  self.time.text = LocalString.GetString('剩余时间 ') .. str
end

function LuaQingMingNHTQResultWnd:InitItem(itemId)
  local design = Item_Item.GetData(itemId)
  local icon = self.ItemCell.transform:Find("Icon"):GetComponent(typeof(CUITexture))
  icon:LoadMaterial(design.Icon)
  self.ItemCell.transform:Find('NumLabel').gameObject:SetActive(false)
  self.desc.text = LocalString.GetString('获得')..design.Name
end

function LuaQingMingNHTQResultWnd:Init()
  if LuaQingMing2021Mgr.GameResult then
    local restTime = LuaQingMing2021Mgr.GameResult[1]
    local itemId = LuaQingMing2021Mgr.GameResult[2]

    self:InitRestTime(restTime)
    self:InitItem(itemId)
  end


	local onCloseClick = function(go)
    CUIManager.CloseUI(CLuaUIResources.QingMingNHTQResultWnd)
	end

	CommonDefs.AddOnClickListener(self.CloseButton,DelegateFactory.Action_GameObject(onCloseClick),false)
end

--@region UIEvent

--@endregion
