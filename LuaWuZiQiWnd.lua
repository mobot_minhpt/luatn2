--import
local GameObject		= import "UnityEngine.GameObject"

local DelegateFactory   = import "DelegateFactory"

local CommonDefs        = import "L10.Game.CommonDefs"

local CUIManager        = import "L10.UI.CUIManager"
--define
CLuaWuZiQiWnd = class()

--RegistChildComponent
RegistChildComponent(CLuaWuZiQiWnd, "Item1",	GameObject)
RegistChildComponent(CLuaWuZiQiWnd, "Item2",	GameObject)
RegistChildComponent(CLuaWuZiQiWnd, "Item3",	GameObject)
RegistChildComponent(CLuaWuZiQiWnd, "WrongTip",	GameObject)
RegistChildComponent(CLuaWuZiQiWnd, "RightTip",	GameObject)
RegistChildComponent(CLuaWuZiQiWnd, "Chess",	GameObject)

--RegistClassMember
RegistClassMember(CLuaWuZiQiWnd, "m_items")
RegistClassMember(CLuaWuZiQiWnd, "delayTick")


CLuaWuZiQiWnd.TaskID = 0
function CLuaWuZiQiWnd.Show(taskid)
    CLuaWuZiQiWnd.TaskID = taskid
    CUIManager.ShowUI(CLuaUIResources.WuZiQiWnd)
end

--@region flow function

function CLuaWuZiQiWnd:Init()

    self.m_items = {}
    self.m_items[1]={Ctrl = self.Item1} 
    self.m_items[2]={Ctrl = self.Item2} 
    self.m_items[3]={Ctrl = self.Item3} 
    for i=1,#self.m_items do
        local click = function(go)
            self:OnItemClick(i)
        end
        CommonDefs.AddOnClickListener(self.m_items[i].Ctrl,DelegateFactory.Action_GameObject(click),false)
    end

    self:Repaint()
end

function CLuaWuZiQiWnd:OnEnable()
end

function CLuaWuZiQiWnd:OnDisable()
    if self.delayTick then 
		UnRegisterTick(self.delayTick)
		self.delayTick = nil
	end
end

--@endregion

function CLuaWuZiQiWnd:OnItemClick(index)

    for i=1,#self.m_items do
        self.m_items[i].Ctrl:SetActive(false)
    end

    self:AddChess(index)
    if index == 2 then 
        self.RightTip:SetActive(true)
        local delayreq = function()
            self:DelayReq()
        end
        self:DoTickOnce(delayreq,1.5)
    else
        self.WrongTip:SetActive(true)
        local repaint = function()
            self:Repaint()
        end
        self:DoTickOnce(repaint,1.5)
    end
end

function CLuaWuZiQiWnd:DelayReq()
    Gac2Gas.RequestFinishShanGuiTask(CLuaWuZiQiWnd.TaskID)
    CUIManager.CloseUI(CLuaUIResources.WuZiQiWnd)
end

function CLuaWuZiQiWnd:AddChess(index)
    local ctrl = self.m_items[index].Ctrl
    self.Chess.transform.localPosition = ctrl.transform.localPosition
    self.Chess:SetActive(true)
end

function CLuaWuZiQiWnd:Repaint()
    self.Chess:SetActive(false)
    self.RightTip:SetActive(false)
    self.WrongTip:SetActive(false)

    for i=1,#self.m_items do
        self.m_items[i].Ctrl:SetActive(true)
    end
end

function CLuaWuZiQiWnd:DoTickOnce(callback,second)
    if self.delayTick then 
		UnRegisterTick(self.delayTick)
		self.delayTick = nil
	end
	self.delayTick = RegisterTickOnce(callback,second * 1000)
end