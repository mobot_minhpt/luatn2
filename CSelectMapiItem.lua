-- Auto Generated!!
local Boolean = import "System.Boolean"
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CLingShouBaseMgr = import "L10.Game.CLingShouBaseMgr"
local CMapiView = import "L10.UI.CMapiView"
local CommonDefs = import "L10.Game.CommonDefs"
local CSelectMapiItem = import "L10.UI.CSelectMapiItem"
local CSelectMapiWnd = import "L10.UI.CSelectMapiWnd"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CZuoQiMgr = import "L10.Game.CZuoQiMgr"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EventManager = import "EventManager"
local Gac2Gas = import "L10.Game.Gac2Gas"
local GameObject = import "UnityEngine.GameObject"
local Item_Item = import "L10.Game.Item_Item"
local LocalString = import "LocalString"
local MapiSelectWndType = import "L10.UI.MapiSelectWndType"
local Object = import "System.Object"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local ZuoQi_Setting = import "L10.Game.ZuoQi_Setting"
CSelectMapiItem.m_InitFangmuMapi_CS2LuaHook = function (this, zqdata) 
    this.fangmuMapiId = zqdata.id

    this.selectedSprite.gameObject:SetActive(false)
    this.nameLabel.text = zqdata.mapiInfo.Name

    local setting = ZuoQi_Setting.GetData()
    local icon = nil
    if zqdata.mapiInfo ~= nil then
        icon = setting.MapiIcon[zqdata.mapiInfo.FuseId]
        if zqdata.quality == 4 then
            icon = setting.SpecialMapiIcon[0]
        elseif zqdata.quality == 5 then
			icon = setting.SpecialMapiIcon[1]
		end
        this.mapiItemTexture:LoadMaterial(icon)
    end
    local appearanceName = CZuoQiMgr.GetAppearanceName(zqdata.mapiInfo.PinzhongId, zqdata.mapiInfo.Quality)
    local attrName = CZuoQiMgr.GetAttributeName(zqdata.mapiInfo.AttributeNameId)
    this.descLabel.text = System.String.Format(LocalString.GetString("{0}·{1} {2}"), appearanceName, attrName, zqdata.mapiInfo.Level)

    local fangmued = false
    local furnitureInfo = CClientHouseMgr.Inst.mFurnitureProp
    if furnitureInfo ~= nil then
        CommonDefs.DictIterate(furnitureInfo.MapiAppearanceInfo, DelegateFactory.Action_object_object(function (___key, ___value) 
            local v = {}
            v.Key = ___key
            v.Value = ___value
            if v.Value.ZuoqiId == zqdata.id then
                fangmued = true
            end
        end))
    end
    this.checkBox:SetSelected(fangmued, true)
    if fangmued and not CommonDefs.ListContains(CSelectMapiWnd.sCurFangmuedMapiList, typeof(String), this.fangmuMapiId) then
        CommonDefs.ListAdd(CSelectMapiWnd.sCurFangmuedMapiList, typeof(String), this.fangmuMapiId)
    end
end
CSelectMapiItem.m_InitLingshou_CS2LuaHook = function (this, lingshou, shouhuedByCurMapi, shouhuedByOtherMapi) 
    this.lingshouid = lingshou.id
    this.selectedSprite.gameObject:SetActive(false)

    this.nameLabel.text = lingshou.name
    local evolveStr = CLingShouBaseMgr.GetEvolveGradeDesc(lingshou.evolveGrade)
    local levelStr = System.String.Format(LocalString.GetString(" {0}级"), lingshou.level)
    this.descLabel.text = evolveStr .. levelStr
    this.mapiItemTexture:LoadNPCPortrait(CLingShouBaseMgr.GetLingShouIcon(lingshou.templateId), false)

    this.checkBox:SetSelected(shouhuedByCurMapi, true)
    if shouhuedByOtherMapi then
        CUICommonDef.SetActive(this.mapiItemTexture.gameObject, false, true)
        CUICommonDef.SetActive(this.nameLabel.gameObject, false, true)
        CUICommonDef.SetActive(this.descLabel.gameObject, false, true)
        CUICommonDef.SetActive(this.checkBox.gameObject, false, true)
        CUICommonDef.SetActive(this.selectedSprite.gameObject, false, true)
    end
end
CSelectMapiItem.m_Callback_CS2LuaHook = function (this, go) 
    if CSelectMapiWnd.sType == MapiSelectWndType.eSelectExpItem and this.mCurExpItemTid ~= 0 and CMapiView.sCurZuoqiId ~= nil then
        local bagPos = 0
        local itemid = nil
        local default
        default, bagPos, itemid = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, this.mCurExpItemTid)
        if default then
            Gac2Gas.RequestAddMapiExpWithItem(CMapiView.sCurZuoqiId, itemid, bagPos)
        end
    end
end
CSelectMapiItem.m_InitExpItem_CS2LuaHook = function (this, expItemTid, expAmount) 
    this.mCurExpItemTid = expItemTid
    this.mCurExpAmount = expAmount
    local itemdata = Item_Item.GetData(expItemTid)
    if itemdata == nil then
        return
    end

    this.nameLabel.text = itemdata.Name
    this.selectedSprite.gameObject:SetActive(false)
    this.descLabel.text = System.String.Format(LocalString.GetString("{0}马匹经验"), expAmount)
    this.mapiItemTexture:LoadMaterial(itemdata.Icon)

    local amount = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, this.mCurExpItemTid)
    this.getSprite.gameObject:SetActive(amount <= 0)
    this.numberLabel.text = tostring(amount)

    if this.longPressButton ~= nil then
        this.longPressButton.callback = MakeDelegateFromCSFunction(this.Callback, MakeGenericClass(Action1, GameObject), this)
        this.longPressButton.delta = 0.165 --[[0.33f/2.0f]]
        CUICommonDef.AddButtonScale(this.gameObject)
    end
end
CSelectMapiItem.m_InitMapi_CS2LuaHook = function (this, zqdata, act) 
    this.zqid = zqdata.id
    this.selectedSprite.gameObject:SetActive(false)

    local setting = ZuoQi_Setting.GetData()

    if zqdata.mapiInfo ~= nil then
        local icon = nil
        icon = setting.MapiIcon[zqdata.mapiInfo.FuseId]
        if zqdata.quality == 4 then
            icon = setting.SpecialMapiIcon[0]
        elseif zqdata.quality == 5 then
			icon = setting.SpecialMapiIcon[1]
		end
        this.mapiItemTexture:LoadMaterial(icon)
        this.nameLabel.text = zqdata.mapiName

        local appearanceName = CZuoQiMgr.GetAppearanceName(zqdata.mapiInfo.PinzhongId, zqdata.mapiInfo.Quality)
        local attrName = CZuoQiMgr.GetAttributeName(zqdata.mapiInfo.AttributeNameId)
        this.descLabel.text = System.String.Format(LocalString.GetString("{0}·{1} {2}"), appearanceName, attrName, zqdata.mapiInfo.Level)

        this.actSelectMapi = act
    end
end
CSelectMapiItem.m_OnEnable_CS2LuaHook = function (this) 
    EventManager.AddListenerInternal(EnumEventType.OnSyncMapiInfo, MakeDelegateFromCSFunction(this.OnSyncMapiInfo, MakeGenericClass(Action1, String), this))
    EventManager.AddListenerInternal(EnumEventType.SetItemAt, MakeDelegateFromCSFunction(this.OnSetItemAt, MakeGenericClass(Action4, EnumItemPlace, UInt32, String, String), this))
    UIEventListener.Get(this.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.gameObject).onClick, MakeDelegateFromCSFunction(this.OnClickBtn, VoidDelegate, this), true)

    if this.checkBox ~= nil then
        this.checkBox.OnValueChanged = CommonDefs.CombineListner_Action_bool(this.checkBox.OnValueChanged, MakeDelegateFromCSFunction(this.OnCheckBoxValueChanged, MakeGenericClass(Action1, Boolean), this), true)
    end
    if this.getSprite ~= nil then
        UIEventListener.Get(this.getSprite.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.getSprite.gameObject).onClick, MakeDelegateFromCSFunction(this.OnClickGetSprite, VoidDelegate, this), true)
    end
end
CSelectMapiItem.m_OnDisable_CS2LuaHook = function (this) 
    EventManager.RemoveListenerInternal(EnumEventType.OnSyncMapiInfo, MakeDelegateFromCSFunction(this.OnSyncMapiInfo, MakeGenericClass(Action1, String), this))
    EventManager.RemoveListenerInternal(EnumEventType.SetItemAt, MakeDelegateFromCSFunction(this.OnSetItemAt, MakeGenericClass(Action4, EnumItemPlace, UInt32, String, String), this))
    UIEventListener.Get(this.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.gameObject).onClick, MakeDelegateFromCSFunction(this.OnClickBtn, VoidDelegate, this), false)

    if this.checkBox ~= nil then
        this.checkBox.OnValueChanged = CommonDefs.CombineListner_Action_bool(this.checkBox.OnValueChanged, MakeDelegateFromCSFunction(this.OnCheckBoxValueChanged, MakeGenericClass(Action1, Boolean), this), false)
    end

    if this.getSprite ~= nil then
        UIEventListener.Get(this.getSprite.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.getSprite.gameObject).onClick, MakeDelegateFromCSFunction(this.OnClickGetSprite, VoidDelegate, this), false)
    end
end
CSelectMapiItem.m_OnCheckBoxValueChanged_CS2LuaHook = function (this, val) 
    if CSelectMapiWnd.sType == MapiSelectWndType.eShouhuSelectLingshou then
        if String.IsNullOrEmpty(this.lingshouid) then
            return
        end

        if this.checkBox ~= nil then
            if val then
                local idx = - 1
                do
                    local i = 0
                    while i < CSelectMapiWnd.sCurSelectedLingshou.Length do
                        if String.IsNullOrEmpty(CSelectMapiWnd.sCurSelectedLingshou[i]) then
                            idx = i
                            break
                        end
                        i = i + 1
                    end
                end
                if idx == - 1 then
                    g_MessageMgr:ShowMessage("Mapi_Shouhu_Lingshou_Too_Many")
                    this.checkBox:SetSelected(false, true)
                    return
                else
                    CSelectMapiWnd.sCurSelectedLingshou[idx] = this.lingshouid
                end
            else
                local idx = - 1
                do
                    local i = 0
                    while i < CSelectMapiWnd.sCurSelectedLingshou.Length do
                        if (this.lingshouid == CSelectMapiWnd.sCurSelectedLingshou[i]) then
                            idx = i
                            break
                        end
                        i = i + 1
                    end
                end
                if idx ~= - 1 then
                    CSelectMapiWnd.sCurSelectedLingshou[idx] = ""
                end
            end
        end
    elseif CSelectMapiWnd.sType == MapiSelectWndType.eSelectFangmu then
        if System.String.IsNullOrEmpty(this.fangmuMapiId) then
            return
        end
        if this.checkBox ~= nil then
            if val then
                if CZuoQiMgr.Inst:CheckMapiNotInMajiu(this.fangmuMapiId) then
                    if not CommonDefs.ListContains(CSelectMapiWnd.sCurFangmuedMapiList, typeof(String), this.fangmuMapiId) then
                        CommonDefs.ListAdd(CSelectMapiWnd.sCurFangmuedMapiList, typeof(String), this.fangmuMapiId)
                    end
                else
                    this.checkBox:SetSelected(not val, true)
                end
            else
                if CommonDefs.ListContains(CSelectMapiWnd.sCurFangmuedMapiList, typeof(String), this.fangmuMapiId) then
                    CommonDefs.ListRemove(CSelectMapiWnd.sCurFangmuedMapiList, typeof(String), this.fangmuMapiId)
                end
            end
        end
    end
end
CSelectMapiItem.m_OnClickBtn_CS2LuaHook = function (this, go) 

    if CSelectMapiWnd.sType == MapiSelectWndType.eFanyuSelectMapi and this.actSelectMapi ~= nil then
        GenericDelegateInvoke(this.actSelectMapi, Table2ArrayWithCount({this.zqid}, 1, MakeArrayClass(Object)))
    elseif CSelectMapiWnd.sType == MapiSelectWndType.eSelectExpItem and this.mCurExpItemTid ~= 0 and CMapiView.sCurZuoqiId ~= nil then
        do
            local i = 0
            while i < CSelectMapiWnd.sExpItemDic.Count do
                CSelectMapiWnd.sExpItemDic[i]:SetSelected(CSelectMapiWnd.sExpItemDic[i].gameObject == this.gameObject)
                i = i + 1
            end
        end
    elseif CSelectMapiWnd.sType == MapiSelectWndType.eSelectMajiu and this.actSelectMapi ~= nil then
        GenericDelegateInvoke(this.actSelectMapi, Table2ArrayWithCount({this.zqid}, 1, MakeArrayClass(Object)))
    elseif CSelectMapiWnd.sType == MapiSelectWndType.eSelectLifeItem and this.mCurLifeItemTid ~= 0 and CMapiView.sCurZuoqiId ~= nil then
        do
            local i = 0
            while i < CSelectMapiWnd.sLifeItemDic.Count do
                CSelectMapiWnd.sLifeItemDic[i]:SetSelected(CSelectMapiWnd.sLifeItemDic[i].gameObject == this.gameObject)
                i = i + 1
            end
        end
    end
end

CSelectMapiItem.m_AddLifeCallback_CS2LuaHook = function (this, go) 
    if CSelectMapiWnd.sType == MapiSelectWndType.eSelectLifeItem and this.mCurLifeItemTid ~= 0 and CMapiView.sCurZuoqiId ~= nil then
        local bagPos = 0
        local itemid = nil
        local default
        default, bagPos, itemid = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, this.mCurLifeItemTid)
        if default then
            Gac2Gas.RequestAddMapiLifeWithItem(CMapiView.sCurZuoqiId, itemid, bagPos)
        end
    end
end
CSelectMapiItem.m_InitLifeItem_CS2LuaHook = function (this, lifeItemTid, lifeAmount) 
    this.mCurLifeItemTid = lifeItemTid
    this.mCurLifeAmount = lifeAmount
    local itemdata = Item_Item.GetData(lifeItemTid)
    if itemdata == nil then
        return
    end

    this.nameLabel.text = itemdata.Name
    this.selectedSprite.gameObject:SetActive(false)
    this.descLabel.text = System.String.Format(LocalString.GetString("增加马匹寿命{0}天"), lifeAmount)
    this.mapiItemTexture:LoadMaterial(itemdata.Icon)

    local amount = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, this.mCurLifeItemTid)
    this.getSprite.gameObject:SetActive(amount <= 0)
    this.numberLabel.text = tostring(amount)

    if this.longPressButton ~= nil then
        this.longPressButton.callback = MakeDelegateFromCSFunction(this.AddLifeCallback, MakeGenericClass(Action1, GameObject), this)
        this.longPressButton.delta = 0.165 --[[0.33f/2.0f]]
        CUICommonDef.AddButtonScale(this.gameObject)
    end
end
