local Profession = import "L10.Game.Profession"
local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local UILabel = import "UILabel"
local UISprite = import "UISprite"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local AlignType = import "CPlayerInfoMgr+AlignType"

LuaStarBiWuGiftContributionRankWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaStarBiWuGiftContributionRankWnd, "NameLabel", "NameLabel", UILabel)
RegistChildComponent(LuaStarBiWuGiftContributionRankWnd, "MyPopularValueLabel", "MyPopularValueLabel", UILabel)
RegistChildComponent(LuaStarBiWuGiftContributionRankWnd, "MyRankLabel", "MyRankLabel", UILabel)
RegistChildComponent(LuaStarBiWuGiftContributionRankWnd, "MyRankImage", "MyRankImage", UISprite)
RegistChildComponent(LuaStarBiWuGiftContributionRankWnd, "NoneRankLabel", "NoneRankLabel", UILabel)
RegistChildComponent(LuaStarBiWuGiftContributionRankWnd, "RankTableView", "RankTableView", QnAdvanceGridView)
RegistChildComponent(LuaStarBiWuGiftContributionRankWnd, "TeamListTableView", "TeamListTableView", QnAdvanceGridView)

--@endregion RegistChildComponent end
RegistClassMember(LuaStarBiWuGiftContributionRankWnd,"m_RankList")
RegistClassMember(LuaStarBiWuGiftContributionRankWnd,"m_TeamList")
RegistClassMember(LuaStarBiWuGiftContributionRankWnd,"m_IsMyTeam")

function LuaStarBiWuGiftContributionRankWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaStarBiWuGiftContributionRankWnd:Init()
    self.NoneRankLabel.text = g_MessageMgr:FormatMessage("LeiGuView_NoneRank")
    self.RankTableView.m_DataSource = DefaultTableViewDataSource.Create2(
        function() 
            return #self.m_RankList
        end,
        function(view,index)
            local item = view:GetFromPool(index == 0 and 1 or 0)
            item:SetBackgroundTexture(index % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite)
            self:InitRankItem(item, index)
            return item
        end)
    self.RankTableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        self:OnRankTableViewSelectAtRow(row)
    end)
    self.TeamListTableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_TeamList
        end,
        function(item, index)
            self:InitTeamItem(item, index)
        end
    )
    self.MyRankImage.spriteName = ""
    self.MyRankLabel.text = LocalString.GetString("未上榜")
    self.MyPopularValueLabel.text = ""
    self.NameLabel.text = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Name or ""
    Gac2Gas.QueryStarBiwuZhanduiDetailRank(CLuaStarBiwuMgr.m_GiftContributionRankWndZhanDuiId)
end

function LuaStarBiWuGiftContributionRankWnd:InitRankItem(item, index)
    local rankLabel = item.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
	local rankImage = item.transform:Find("RankLabel/RankImage"):GetComponent(typeof(UISprite))
    local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local serverLabel = item.transform:Find("ServerLabel"):GetComponent(typeof(UILabel))
    local valLabel = item.transform:Find("ValLabel"):GetComponent(typeof(UILabel))

    local t = self.m_RankList[index + 1]
    rankLabel.text = LocalString.GetString("未上榜")
    rankImage.spriteName= ""
    nameLabel.text =  ""
    serverLabel.text =  ""
    valLabel.text =  ""
    if t then
        rankLabel.text = ""
        local rank = t.rankPos
        if rank==1 then
            rankImage.spriteName="Rank_No.1"
        elseif rank==2 then
            rankImage.spriteName="Rank_No.2png"
        elseif rank==3 then
            rankImage.spriteName="Rank_No.3png"
        else
            if rank > 0 then
                rankLabel.text = rank
            elseif rank == 0 then
                rankLabel.text = "-"
            else
                rankLabel.text = LocalString.GetString("未上榜")
            end
        end
        nameLabel.text = t.playerName
        serverLabel.text = t.serverName 
        valLabel.text = t.rankValue
    end
    UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        if t.playerId then
            CPlayerInfoMgr.ShowPlayerPopupMenu(t.playerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
        end	    
	end)
end

function LuaStarBiWuGiftContributionRankWnd:InitTeamItem(item, index)
    local classSprite = item.transform:Find("ClassSprite"):GetComponent(typeof(UISprite))
    local leaderSprite = item.transform:Find("LeaderSprite")
    local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    
    local t = self.m_TeamList[index + 1]
    classSprite.spriteName = Profession.GetIconByNumber(t.playerCls)
    leaderSprite.gameObject:SetActive(t.playerId == CLuaStarBiwuMgr.m_GiftContributionRankWndZhanDuiId)
    nameLabel.text = SafeStringFormat3(LocalString.GetString("%s[CACACA](%s)"), t.playerName, t.serverName)
end

function LuaStarBiWuGiftContributionRankWnd:OnEnable()
    g_ScriptEvent:AddListener("OnSyncStarBiwuZhanduiDetailRank",self,"OnSyncStarBiwuZhanduiDetailRank")
end

function LuaStarBiWuGiftContributionRankWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnSyncStarBiwuZhanduiDetailRank",self,"OnSyncStarBiwuZhanduiDetailRank")
end

function LuaStarBiWuGiftContributionRankWnd:OnSyncStarBiwuZhanduiDetailRank()
    self.m_RankList = CLuaStarBiwuMgr.m_ZhanduiDetailRankInfo.rankList
    local myInfo = nil 
    if CClientMainPlayer.Inst then
        myInfo = {
            playerName = CClientMainPlayer.Inst.Name,
            serverName = CClientMainPlayer.Inst:GetMyServerName(),
            rankValue = 0,
            rankPos = -1
        }
    end
    for i, t in pairs(CLuaStarBiwuMgr.m_ZhanduiDetailRankInfo.rankList) do
		if CClientMainPlayer.Inst and t.playerId == CClientMainPlayer.Inst.Id then
            self.m_IsMyTeam = true
            myInfo = CLuaStarBiwuMgr.m_ZhanduiDetailRankInfo.rankList[i]
		end
	end
    table.insert(self.m_RankList,1, myInfo)
    self.RankTableView:ReloadData(true, false)
    self.NoneRankLabel.gameObject:SetActive(#self.m_RankList == 0)
    self:InitLeftView()
    self.m_TeamList = {}
    for id, t in pairs(CLuaStarBiwuMgr.m_ZhanduiDetailRankInfo and CLuaStarBiwuMgr.m_ZhanduiDetailRankInfo.memberList or {}) do
        table.insert(self.m_TeamList,t)
    end
    self.TeamListTableView:ReloadData(true, false)
end

function LuaStarBiWuGiftContributionRankWnd:InitLeftView()
    for i, t in pairs(CLuaStarBiwuMgr.m_ZhanduiRenqiRankInfo.rankList) do
        if t.zhanduiId == CLuaStarBiwuMgr.m_GiftContributionRankWndZhanDuiId then
            self.NameLabel.text = t.zhanduiName
            self.MyPopularValueLabel.text = t.renqi
            self.MyRankImage.spriteName= ""
            self.MyRankLabel.text = ""
            local rank = t.rank
            if rank==1 then
                self.MyRankImage.spriteName="Rank_No.1"
            elseif rank==2 then
                self.MyRankImage.spriteName="Rank_No.2png"
            elseif rank==3 then
                self.MyRankImage.spriteName="Rank_No.3png"
            else
                if rank > 0 then
                    self.MyRankLabel.text = rank
                else
                    self.MyRankLabel.text = LocalString.GetString("未上榜")
                end
            end
            break
        end
    end
end
--@region UIEvent
function LuaStarBiWuGiftContributionRankWnd:OnRankTableViewSelectAtRow(index)
    if index ~= 0 then
        local t = self.m_RankList[index + 1]
        CPlayerInfoMgr.ShowPlayerPopupMenu(t.playerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
    end
end
--@endregion UIEvent

