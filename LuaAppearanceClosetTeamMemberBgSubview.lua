local CUITexture = import "L10.UI.CUITexture"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local Extensions = import "Extensions"
local CButton = import "L10.UI.CButton"
local EnumGender = import "L10.Game.EnumGender"
local UISprite = import "UISprite"
local LuaTweenUtils = import "LuaTweenUtils"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"

LuaAppearanceClosetTeamMemberBgSubview = class()

RegistChildComponent(LuaAppearanceClosetTeamMemberBgSubview,"m_TeamMemberBgItem", "CommonClosetSingleLineItemCell", GameObject)
RegistChildComponent(LuaAppearanceClosetTeamMemberBgSubview,"m_ItemDisplay", "AppearanceCommonButtonDisplayView", CCommonLuaScript)
RegistChildComponent(LuaAppearanceClosetTeamMemberBgSubview,"m_TeamMemberDisplaySubview", "AppearanceTeamMemberDisplaySubview", CCommonLuaScript)
RegistChildComponent(LuaAppearanceClosetTeamMemberBgSubview,"m_ContentTable", "ContentTable", UITable)
RegistChildComponent(LuaAppearanceClosetTeamMemberBgSubview,"m_ContentScrollView", "ContentScrollView", UIScrollView)

RegistClassMember(LuaAppearanceClosetTeamMemberBgSubview, "m_AllBgs")
RegistClassMember(LuaAppearanceClosetTeamMemberBgSubview, "m_SelectedDataId")

function LuaAppearanceClosetTeamMemberBgSubview:Awake()
end

function LuaAppearanceClosetTeamMemberBgSubview:Init()
    self:PlayAppearAnimation()
    self:SetDefaultSelection()
    self:LoadData()
end

function LuaAppearanceClosetTeamMemberBgSubview:PlayAppearAnimation()
    self.m_TeamMemberDisplaySubview.gameObject:SetActive(true)
    LuaTweenUtils.TweenAlpha(self.m_TeamMemberDisplaySubview.transform:GetComponent(typeof(UIWidget)), 0, 1, 0.8)
    self.m_TeamMemberDisplaySubview:Init()
end

function LuaAppearanceClosetTeamMemberBgSubview:LoadData()
    self.m_AllBgs = LuaAppearancePreviewMgr:GetAllTeamMemberBgInfo(true)

    Extensions.RemoveAllChildren(self.m_ContentTable.transform)
    self.m_ContentTable.gameObject:SetActive(true)

    for i = 1, # self.m_AllBgs do
        local child = CUICommonDef.AddChild(self.m_ContentTable.gameObject, self.m_TeamMemberBgItem)
        child:SetActive(true)
        self:InitItem(child, self.m_AllBgs[i])
    end
    self.m_ContentTable:Reposition()
    self.m_ContentScrollView:ResetPosition()
    self:InitSelection()
end

function LuaAppearanceClosetTeamMemberBgSubview:SetDefaultSelection()
    self.m_SelectedDataId = LuaAppearancePreviewMgr:GetCurrentInUseTeamMemberBg()
end

function LuaAppearanceClosetTeamMemberBgSubview:InitSelection()
    if self.m_SelectedDataId == 0 then
        self:OnItemClick(nil)
        return
    end

    local childCount = self.m_ContentTable.transform.childCount
    for i=0, childCount-1 do
        local childGo = self.m_ContentTable.transform:GetChild(i).gameObject
        local appearanceData = self.m_AllBgs[i+1]
        if appearanceData.id == self.m_SelectedDataId then
            self:OnItemClick(childGo)
            break
        end
    end
end

function LuaAppearanceClosetTeamMemberBgSubview:InitItem(itemGo, appearanceData)
    local iconTexture = itemGo.transform:Find("Item"):GetComponent(typeof(CUITexture))
    local cornerGo = itemGo.transform:Find("Item/Corner").gameObject
    local disabledGo = itemGo.transform:Find("Item/Disabled").gameObject
    local nameLabel = itemGo.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local conditionLabel = itemGo.transform:Find("ConditionLabel"):GetComponent(typeof(UILabel))
    iconTexture:LoadMaterial(appearanceData.icon)
    nameLabel.text = appearanceData.name
    conditionLabel.text = LuaAppearancePreviewMgr:GetTeamMemberBgConditionText(appearanceData.id)
    disabledGo:SetActive(not LuaAppearancePreviewMgr:MainPlayerHasTeamMemberBg(appearanceData.id))
    cornerGo:SetActive(LuaAppearancePreviewMgr:IsCurrentInUseTeamMemberBg(appearanceData.id))
    itemGo:GetComponent(typeof(CButton)).Selected = (self.m_SelectedDataId and self.m_SelectedDataId==appearanceData.id or false)

    UIEventListener.Get(itemGo).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnItemClick(go)
    end)
end

function LuaAppearanceClosetTeamMemberBgSubview:OnItemClick(go)
    local childCount = self.m_ContentTable.transform.childCount
    for i=0, childCount-1 do
        local childGo = self.m_ContentTable.transform:GetChild(i).gameObject
        if childGo == go then
            childGo.transform:GetComponent(typeof(CButton)).Selected = true
            self.m_SelectedDataId = self.m_AllBgs[i+1].id
        else
            childGo.transform:GetComponent(typeof(CButton)).Selected = false
        end
    end
    self:UpdateItemDisplay()
end

function LuaAppearanceClosetTeamMemberBgSubview:UpdateItemDisplay()
    local id = self.m_SelectedDataId and self.m_SelectedDataId or 0
    if id==0 then
        self.m_TeamMemberDisplaySubview:SetContent("", LuaAppearancePreviewMgr:GetDefaultTeamMemberBgName(), "", "")
        self.m_ItemDisplay.gameObject:SetActive(false)
        return
    end
    local appearanceData = nil
    for i=1,#self.m_AllBgs do
        if self.m_AllBgs[i].id == id then
            appearanceData = self.m_AllBgs[i]
            break
        end
    end
    self.m_TeamMemberDisplaySubview:SetContent(appearanceData.name, appearanceData.memberBg, appearanceData.memberBorder, appearanceData.teamBg)
    self.m_ItemDisplay.gameObject:SetActive(true)
    local buttonTbl = {}
    local inUse = LuaAppearancePreviewMgr:IsCurrentInUseTeamMemberBg(appearanceData.id)

    if inUse then
        table.insert(buttonTbl, {text=LocalString.GetString("脱下"), isYellow=false, action=function(go) self:OnRemoveButtonClick() end})
    else
        table.insert(buttonTbl, {text=LocalString.GetString("更换"), isYellow=false, action=function(go) self:OnApplyButtonClick() end})
    end

    self.m_ItemDisplay:Init(buttonTbl)
end

function LuaAppearanceClosetTeamMemberBgSubview:OnApplyButtonClick()
    local exist = LuaAppearancePreviewMgr:MainPlayerHasTeamMemberBg(self.m_SelectedDataId)
    if exist then
        LuaAppearancePreviewMgr:RequestSetTeamMemberBg(self.m_SelectedDataId)
    end
end

function LuaAppearanceClosetTeamMemberBgSubview:OnRemoveButtonClick()
    LuaAppearancePreviewMgr:RequestSetTeamMemberBg(0)
end

function LuaAppearanceClosetTeamMemberBgSubview:OnEnable()
    g_ScriptEvent:AddListener("OnUnLockTeamMemberBgSuccess", self, "OnUnLockTeamMemberBgSuccess")
    g_ScriptEvent:AddListener("OnUseTeamMemberBgSuccess", self, "OnUseTeamMemberBgSuccess")
end

function LuaAppearanceClosetTeamMemberBgSubview:OnDisable()
    g_ScriptEvent:RemoveListener("OnUnLockTeamMemberBgSuccess", self, "OnUnLockTeamMemberBgSuccess")
    g_ScriptEvent:RemoveListener("OnUseTeamMemberBgSuccess", self, "OnUseTeamMemberBgSuccess")
end

function LuaAppearanceClosetTeamMemberBgSubview:OnUnLockTeamMemberBgSuccess(id)
    self:LoadData()
end

function LuaAppearanceClosetTeamMemberBgSubview:OnUseTeamMemberBgSuccess(id)
    self:SetDefaultSelection()
    self:LoadData()
end
