local DelegateFactory = import "DelegateFactory"
local CServerTimeMgr  = import "L10.Game.CServerTimeMgr"
local CButton         = import "L10.UI.CButton"
local Animation       = import "UnityEngine.Animation"

LuaWorldCup2022DailyLotteryView = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaWorldCup2022DailyLotteryView, "tabTemplate")
RegistClassMember(LuaWorldCup2022DailyLotteryView, "tabTable")
RegistClassMember(LuaWorldCup2022DailyLotteryView, "matchTemplate")
RegistClassMember(LuaWorldCup2022DailyLotteryView, "matchTable")
RegistClassMember(LuaWorldCup2022DailyLotteryView, "matchScrollView")
RegistClassMember(LuaWorldCup2022DailyLotteryView, "moneyFxAnim")

RegistClassMember(LuaWorldCup2022DailyLotteryView, "tabInfoTable")
RegistClassMember(LuaWorldCup2022DailyLotteryView, "curTab")

function LuaWorldCup2022DailyLotteryView:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self:InitUIComponents()
    self.tabTemplate:SetActive(false)
    self.matchTemplate:SetActive(false)
end

function LuaWorldCup2022DailyLotteryView:InitUIComponents()
    self.tabTemplate = self.transform:Find("Left/TabTemplate").gameObject
    self.tabTable = self.transform:Find("Left/ScrollView/Table"):GetComponent(typeof(UITable))
    self.matchTemplate = self.transform:Find("Right/Template").gameObject
    self.matchTable = self.transform:Find("Right/ScrollView/Table"):GetComponent(typeof(UITable))
    self.matchScrollView = self.transform:Find("Right/ScrollView"):GetComponent(typeof(UIScrollView))
    self.moneyFxAnim = self.transform:Find("vfx_qianbi"):GetComponent(typeof(Animation))
end

function LuaWorldCup2022DailyLotteryView:OnEnable()
    g_ScriptEvent:AddListener("WorldCupJingCai_UpdateAllJingCaiData", self, "UpdateAllJingCaiData")
    g_ScriptEvent:AddListener("WorldCup2022JingCaiVoteSuccess", self, "JingCaiVoteSuccess")
    g_ScriptEvent:AddListener("WorldCupJingCai_SendPeriodAndRoundData", self, "SendPeriodAndRoundData")
    g_ScriptEvent:AddListener("WorldCup2022OnReceiveJingCaiAwardSuccess", self, "OnReceiveJingCaiAwardSuccess")
    self:UpdateInfo()
end

function LuaWorldCup2022DailyLotteryView:OnDisable()
    g_ScriptEvent:RemoveListener("WorldCupJingCai_UpdateAllJingCaiData", self, "UpdateAllJingCaiData")
    g_ScriptEvent:RemoveListener("WorldCup2022JingCaiVoteSuccess", self, "JingCaiVoteSuccess")
    g_ScriptEvent:RemoveListener("WorldCupJingCai_SendPeriodAndRoundData", self, "SendPeriodAndRoundData")
    g_ScriptEvent:RemoveListener("WorldCup2022OnReceiveJingCaiAwardSuccess", self, "OnReceiveJingCaiAwardSuccess")
end

function LuaWorldCup2022DailyLotteryView:UpdateAllJingCaiData()
    self:UpdateInfo()
end

function LuaWorldCup2022DailyLotteryView:JingCaiVoteSuccess()
    self:UpdateInfo()
end

function LuaWorldCup2022DailyLotteryView:SendPeriodAndRoundData()
    self:UpdateInfo()
end

function LuaWorldCup2022DailyLotteryView:OnReceiveJingCaiAwardSuccess()
    self.moneyFxAnim:Play()

end

function LuaWorldCup2022DailyLotteryView:UpdateInfo()
    self:UpdateTab2Id()
    self:UpdateTabTable()
end

function LuaWorldCup2022DailyLotteryView:UpdateTab2Id()
    self.tabInfoTable = {}

    if not LuaWorldCup2022Mgr.lotteryInfo.allRoundResultData or not LuaWorldCup2022Mgr.lotteryInfo.jingCaiData then return end
    local timeTable = {}
    for id, data in pairs(LuaWorldCup2022Mgr.lotteryInfo.allRoundResultData) do
        local jingCaiStartTime = CServerTimeMgr.Inst:GetTimeStampByStr(WorldCup_TodayJingCai.GetData(id).BeginTime)
        if LuaWorldCup2022Mgr:GetCurrentTime() >= jingCaiStartTime then
            local playTime = WorldCup_PlayInfo.GetData(id).Time

            local month, day, hour, min = string.match(playTime, "(%d+).(%d+) (%d+):(%d+)")
            local year = WorldCup_Setting.GetData().Year
            local matchStartTime = CServerTimeMgr.Inst:GetTimeStampByStr(SafeStringFormat3("%d-%d-%d %d:%d", year, month, day, hour, min))
            table.insert(timeTable, {id = id, month = month, day = day, hour = hour, min = min, matchStartTime = matchStartTime})
        end
    end

    if #timeTable == 0 then return end

    table.sort(timeTable, function (a, b)
        if a.month ~= b.month then
            return a.month > b.month
        elseif a.day ~= b.day then
            return a.day > b.day
        elseif a.hour ~= b.hour then
            return a.hour < b.hour
        elseif a.min ~= b.min then
            return a.min < b.min
        else
            return a.id < b.id
        end
    end)

    local tabId = 1
    self.tabInfoTable[tabId] = {}
    self.tabInfoTable[tabId].list = {}
    for i = 1, #timeTable do
        local time = SafeStringFormat3(LocalString.GetString("%d月%d日"), timeTable[i].month, timeTable[i].day)
        if self.tabInfoTable[tabId].time and time ~= self.tabInfoTable[tabId].time then
            tabId = tabId + 1
            self.tabInfoTable[tabId] = {}
            self.tabInfoTable[tabId].list = {}
        end

        if not self.tabInfoTable[tabId].time then
            self.tabInfoTable[tabId].time = time
        end

        table.insert(self.tabInfoTable[tabId].list, {id = timeTable[i].id, hour = timeTable[i].hour, min = timeTable[i].min, matchStartTime = timeTable[i].matchStartTime})
    end
end

function LuaWorldCup2022DailyLotteryView:UpdateTabTable()
    local num = #self.tabInfoTable
    if num == 0 then return end

    self:UpdateChildNum(self.tabTable.transform, self.tabTemplate, num)

    self.curTab = (self.curTab and self.curTab <= num) and self.curTab or 1
    for i = 1, num do
        local child = self.tabTable.transform:GetChild(i - 1)
        child.gameObject:SetActive(true)

        local normal = child:Find("Normal")
        local highlight = child:Find("Highlight")
        normal.gameObject:SetActive(self.curTab ~= i)
        highlight.gameObject:SetActive(self.curTab == i)
        normal:Find("Label"):GetComponent(typeof(UILabel)).text = self.tabInfoTable[i].time
        highlight:Find("Label"):GetComponent(typeof(UILabel)).text = self.tabInfoTable[i].time

        local redDot = normal.transform:Find("Sprite").gameObject
        local list = self.tabInfoTable[i].list
        local hasAward = false
        for _, data in pairs(list) do
            local id = data.id
            local jingCaiData = LuaWorldCup2022Mgr.lotteryInfo.jingCaiData.m_JingCai[id]
            if jingCaiData and jingCaiData[4] == 0 then
                local result = LuaWorldCup2022Mgr.lotteryInfo.allPeriodResultData[id]
                if result and tostring(result) == tostring(jingCaiData[2]) then
                    hasAward = true
                    break
                end
            end
        end
        redDot:SetActive(hasAward)

        UIEventListener.Get(normal.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnTabChange(i)
        end)
    end

    self.tabTable:Reposition()
    self:UpdateMatchTable()
end

function LuaWorldCup2022DailyLotteryView:UpdateChildNum(parent, template, num)
    local childCount = parent.childCount
    for i = 1, childCount do
        parent:GetChild(i - 1).gameObject:SetActive(false)
    end

    if childCount < num then
        for i = childCount + 1, num do
            NGUITools.AddChild(parent.gameObject, template)
        end
    end
end

function LuaWorldCup2022DailyLotteryView:UpdateMatchTable()
    local list = self.tabInfoTable[self.curTab].list
    local num = #list

    self:UpdateChildNum(self.matchTable.transform, self.matchTemplate, num)

    for i = 1, num do
        local id = list[i].id
        local child = self.matchTable.transform:GetChild(i - 1)
        child.gameObject:SetActive(true)

        child:Find("Id"):GetComponent(typeof(UILabel)).text = i
        child:Find("Type"):GetComponent(typeof(UILabel)).text = WorldCup_PlayInfo.GetData(id).MatchName

        local matchInfo = LuaWorldCup2022Mgr.lotteryInfo.allRoundResultData[id]
        local home = child:Find("Home")
        local homeTeamData = WorldCup_TeamInfo.GetData(matchInfo[1])
        home:Find("Name"):GetComponent(typeof(UILabel)).text = homeTeamData.Name
        local homeIcon = home:Find("Icon"):GetComponent(typeof(CUITexture))
        homeIcon:LoadMaterial(homeTeamData.Icon)
        local homeDraw = home:Find("Draw").gameObject
        local homeWin = home:Find("Win").gameObject
        homeDraw:SetActive(false)
        homeWin:SetActive(false)

        local away = child:Find("Away")
        local awayTeamData = WorldCup_TeamInfo.GetData(matchInfo[2])
        away:Find("Name"):GetComponent(typeof(UILabel)).text = awayTeamData.Name
        local awayIcon = away:Find("Icon"):GetComponent(typeof(CUITexture))
        awayIcon:LoadMaterial(awayTeamData.Icon)
        local awayDraw = away:Find("Draw").gameObject
        local awayWin = away:Find("Win").gameObject
        awayDraw:SetActive(false)
        awayWin:SetActive(false)

        local jingCaiData = LuaWorldCup2022Mgr.lotteryInfo.jingCaiData.m_JingCai[id]
        if jingCaiData then
            local value = tostring(jingCaiData[2])
            if value == EnumWorldCupJingCaiVoteOption.HostTeamWin then
                homeWin:SetActive(true)
            elseif value == EnumWorldCupJingCaiVoteOption.HostTeamLose then
                awayWin:SetActive(true)
            elseif value == EnumWorldCupJingCaiVoteOption.Draw then
                homeDraw:SetActive(true)
                awayDraw:SetActive(true)
            end
        end

        LuaUtils.SetLocalPositionZ(homeIcon.transform, (matchInfo[6] >= 0 and matchInfo[6] < matchInfo[7]) and -1 or 0)
        LuaUtils.SetLocalPositionZ(awayIcon.transform, (matchInfo[6] >= 0 and matchInfo[6] > matchInfo[7]) and -1 or 0)

        self:UpdateVS(child, matchInfo, list[i].matchStartTime, list[i].hour, list[i].min)
        self:UpdateOdd(child, jingCaiData, id, matchInfo[1], matchInfo[2])
    end

    self.matchTable:Reposition()
    self.matchScrollView:ResetPosition()
end

function LuaWorldCup2022DailyLotteryView:UpdateVS(child, matchInfo, matchStartTime, hour, min)
    local vs = child:Find("VS")
    local score = vs:Find("Score"):GetComponent(typeof(UILabel))
    local time = vs:Find("Time"):GetComponent(typeof(UILabel))
    local going = vs:Find("Going").gameObject

    score.gameObject:SetActive(false)
    time.gameObject:SetActive(false)
    going:SetActive(false)
    local score1, score2, totalScore1, totalScore2 = matchInfo[3], matchInfo[4], matchInfo[6], matchInfo[7]
    if LuaWorldCup2022Mgr:GetCurrentTime() < matchStartTime then
        time.text = SafeStringFormat3("%02d:%02d", hour, min)
        time.gameObject:SetActive(true)
    elseif score1 < 0 then
        going:SetActive(true)
    else
        if score1 > score2 then
            score.text = SafeStringFormat3("[FF5050]%d[-] : %d", score1, score2)
        elseif score1 < score2 then
            score.text = SafeStringFormat3("%d : [FF5050]%d[-]", score1, score2)
        else
            score.text = SafeStringFormat3("[FF5050]%d : %d[-]", score1, score2)
        end

        local totalScore = score.transform:Find("TotalScore"):GetComponent(typeof(UILabel))
        if score1 == totalScore1 and score2 == totalScore2 then
            totalScore.gameObject:SetActive(false)
        else
            totalScore.text = SafeStringFormat3(LocalString.GetString("总比分 %d:%d"), totalScore1, totalScore2)
            totalScore.gameObject:SetActive(true)
        end
        score.gameObject:SetActive(true)
    end
end

function LuaWorldCup2022DailyLotteryView:UpdateOdd(child, jingCaiData, id, teamId1, teamId2)
    local prize = child:Find("Prize"):GetComponent(typeof(UILabel))
    local bet = child:Find("Bet"):GetComponent(typeof(UILabel))
    local fail = child:Find("fail").gameObject
    local bottom = child:Find("Bottom"):GetComponent(typeof(UILabel))
    local lotteryButton = child:Find("LotteryButton").gameObject
    local receiveButton = child:Find("ReceiveButton"):GetComponent(typeof(CButton))
    local tipButton = child:Find("Bet/TipButton").gameObject
    prize.gameObject:SetActive(false)
    bet.gameObject:SetActive(false)
    fail.gameObject:SetActive(false)
    lotteryButton:SetActive(false)
    receiveButton.gameObject:SetActive(false)
    tipButton:SetActive(false)

    local jingCaiEndTime = CServerTimeMgr.Inst:GetTimeStampByStr(WorldCup_TodayJingCai.GetData(id).EndTime)
    local jingCaiResult = LuaWorldCup2022Mgr.lotteryInfo.allPeriodResultData[id]
    local isJingCaiEnd = LuaWorldCup2022Mgr:GetCurrentTime() >= jingCaiEndTime
    local odd = LuaWorldCup2022Mgr.lotteryInfo.odds[id]

    if not isJingCaiEnd and not jingCaiData then
        prize.gameObject:SetActive(true)
        prize.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("奖池")
        prize.text = odd and odd[1] or 0
        lotteryButton:SetActive(true)
        UIEventListener.Get(lotteryButton).onClick = DelegateFactory.VoidDelegate(function (go)
            LuaWorldCup2022Mgr:ShowLotterySelectWnd(id)
        end)
    elseif not jingCaiResult and jingCaiData then
        prize.gameObject:SetActive(true)
        prize.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("奖池")
        prize.text = odd and odd[1] or 0
        bet.gameObject:SetActive(true)
        LuaUtils.SetLocalPositionY(bet.transform, -18)
        local value, num = tostring(jingCaiData[2]), jingCaiData[3]
        if value == EnumWorldCupJingCaiVoteOption.Draw then
            bet.text = SafeStringFormat3(LocalString.GetString("已投%d注，平局"), num)
        else
            local winTeamId = value == EnumWorldCupJingCaiVoteOption.HostTeamWin and teamId1 or teamId2
            bet.text = SafeStringFormat3(LocalString.GetString("已投%d注，%s胜"), num, WorldCup_TeamInfo.GetData(winTeamId).Name)
        end
    elseif isJingCaiEnd and not jingCaiData then
        bet.gameObject:SetActive(true)
        bet.text = LocalString.GetString("投注已结束，未参与")
        LuaUtils.SetLocalPositionY(bet.transform, 0)
    elseif jingCaiResult and jingCaiData then
        local value, hasReward = tostring(jingCaiData[2]), jingCaiData[4]
        if value == tostring(jingCaiResult) then
            prize.gameObject:SetActive(true)
            prize.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("中奖")
            prize.text = odd and odd[3] or 0
            receiveButton.gameObject:SetActive(true)

            local redDot = receiveButton.transform:Find("Sprite").gameObject
            if hasReward == 0 then
                redDot:SetActive(true)
                receiveButton.Enabled = true
                receiveButton.Text = LocalString.GetString("领 取")

                UIEventListener.Get(receiveButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
                    Gac2Gas.WorldCupJingCai_RequestReceiveJingCaiAward(id)
                end)
            else
                redDot:SetActive(false)
                receiveButton.Enabled = false
                receiveButton.Text = LocalString.GetString("已领奖")
            end
        else
            fail:SetActive(true)

            local matchInfo = LuaWorldCup2022Mgr.lotteryInfo.allRoundResultData[id]
            if matchInfo[3] ~= matchInfo[6] or matchInfo[4] ~= matchInfo[7] then
                tipButton:SetActive(true)
                UIEventListener.Get(tipButton).onClick = DelegateFactory.VoidDelegate(function (go)
                    g_MessageMgr:ShowMessage("WORLDCUP2022_LOTTERY_FAIL_TIP")
                end)
            end
        end
    end

    local str1 = isJingCaiEnd and LocalString.GetString("最终") or LocalString.GetString("实时")
    local name1 = WorldCup_TeamInfo.GetData(teamId1).Name
    local name2 = WorldCup_TeamInfo.GetData(teamId2).Name
    local odd1 = odd and odd[2][EnumWorldCupJingCaiVoteOption.HostTeamWin] / 100 or 1
    local odd2 = odd and odd[2][EnumWorldCupJingCaiVoteOption.HostTeamLose] / 100 or 1
    local odd3 = odd and odd[2][EnumWorldCupJingCaiVoteOption.Draw] / 100 or 1
    bottom.text = SafeStringFormat3(LocalString.GetString("%s回报倍数：%s(%.2f)，%s(%.2f)，平局(%.2f)"), str1, name1, odd1, name2, odd2, odd3)
end

--@region UIEvent

function LuaWorldCup2022DailyLotteryView:OnTabChange(id)
    if self.curTab then
        local beforeTab = self.tabTable.transform:GetChild(self.curTab - 1)
        beforeTab:Find("Normal").gameObject:SetActive(true)
        beforeTab:Find("Highlight").gameObject:SetActive(false)
    end

    self.curTab = id
    local tab = self.tabTable.transform:GetChild(self.curTab - 1)
    tab:Find("Normal").gameObject:SetActive(false)
    local highlight = tab:Find("Highlight")
    highlight.gameObject:SetActive(true)
    highlight:GetComponent(typeof(Animation)):Play()

    self:UpdateMatchTable()
end

--@endregion UIEvent
