local CRenderScene = import "L10.Engine.Scene.CRenderScene"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local PathFindDefs = import "L10.Engine.PathFinding.PathFindDefs"
local EMoveType = import "L10.Engine.EMoveType"
local EMoveToResult = import "L10.Engine.EMoveToResult"
local Setting = import "L10.Engine.Setting"
local Vector3 = import "UnityEngine.Vector3"
local Mathf = import "UnityEngine.Mathf"
local CPos = import "L10.Engine.CPos"

CLuaDirAutoMoveMgr = class()
CLuaDirAutoMoveMgr.m_BaseVerticalSpeed = 5.0
CLuaDirAutoMoveMgr.m_BaseHorizontalSpeed = 2.0
CLuaDirAutoMoveMgr.m_TargetDir = nil
CLuaDirAutoMoveMgr.m_MoveDir = nil

CLuaDirAutoMoveMgr.m_DragDir = nil
CLuaDirAutoMoveMgr.m_DragDot = 0
CLuaDirAutoMoveMgr.m_Threshold = 0.0001
CLuaDirAutoMoveMgr.m_ReverseDrag = false

function CLuaDirAutoMoveMgr:AddListener()
    g_ScriptEvent:RemoveListener("MainPlayerMoveEnded", self, "OnMainPlayerMoveEnded")
	g_ScriptEvent:RemoveListener("PlayerDragJoyStick", self, "OnPlayerDragJoyStick")
    g_ScriptEvent:RemoveListener("PlayerDragJoyStickComplete", self, "OnPlayerDragJoyStickComplete")
	g_ScriptEvent:AddListener("MainPlayerMoveEnded", self, "OnMainPlayerMoveEnded")
    g_ScriptEvent:AddListener("PlayerDragJoyStick", self, "OnPlayerDragJoyStick")
    g_ScriptEvent:AddListener("PlayerDragJoyStickComplete", self, "OnPlayerDragJoyStickComplete")
end
CLuaDirAutoMoveMgr:AddListener()

function CLuaDirAutoMoveMgr:DoMove(bMoveEnd)
	if not (CClientMainPlayer.Inst and CClientMainPlayer.Inst.IsInDirAutoMove) then return end

	local speed = CClientMainPlayer.Inst.MaxSpeed
	local pixelSpeed = CClientMainPlayer.Inst.MaxPixelSpeed

	local deltaVec = CommonDefs.op_Multiply_Single_Vector3(2 * speed, self.m_MoveDir)

	local posX = CClientMainPlayer.Inst.WorldPos.x + deltaVec.x
	local posY = CClientMainPlayer.Inst.WorldPos.z + deltaVec.z
	posX = math.max(math.min(posX, CRenderScene.Inst.MapWidth - 1), 1)
	posY = math.max(math.min(posY, CRenderScene.Inst.MapHeight - 1), 1)

	local target = CPos(posX * Setting.eGridSpan, posY * Setting.eGridSpan)
	local ret = CClientMainPlayer.Inst:MoveTo(target, pixelSpeed, 0,0,0, PathFindDefs.EFindPathType.eFPT_HypoLine, CClientMainPlayer.Inst.BarrierType, false, EMoveType.Normal, PathFindDefs.MAX_REGION_LIMIT)

	if ret ~= EMoveToResult.Success then
		if ret ~= EMoveToResult.NotAllowed and ret ~= EMoveToResult.SuperPosition and self.m_DragDot ~= 0 then
			ret = CClientMainPlayer.Inst:MoveTo(target, pixelSpeed, 0,0,0, PathFindDefs.EFindPathType.eFPT_HypoLineHelper, CClientMainPlayer.Inst.BarrierType, false, EMoveType.Normal, PathFindDefs.MAX_REGION_LIMIT)
		end
		if ret ~= EMoveToResult.Success then
			if self.m_RetryMoveTick ~= nil then
				UnRegisterTick(self.m_RetryMoveTick)
				self.m_RetryMoveTick = nil
			end
			self.m_RetryMoveTick = RegisterTickOnce(function()
				self:DoMove()
			end, 100)
		end
	end
end

function CLuaDirAutoMoveMgr:UpdateMoveDir()
	if not (CClientMainPlayer.Inst and CClientMainPlayer.Inst.IsInDirAutoMove) then return end

	local dragDir = self.m_DragDir
	local targetDir = self.m_TargetDir

	local horizontal = Vector3(targetDir.z, 0, -targetDir.x)
	local horizontalDot = Vector3.Dot(dragDir, horizontal)
	local dragDot = 0
	local flag = self.m_ReverseDrag and -1 or 1
	if horizontalDot < -0.01 or horizontalDot > 0.01 then
		dragDot = flag * horizontalDot
	end
	horizontal = CommonDefs.op_Multiply_Single_Vector3(dragDot * self.m_BaseHorizontalSpeed, horizontal)
	local vertical = CommonDefs.op_Multiply_Single_Vector3(self.m_BaseVerticalSpeed, targetDir)
	self.m_MoveDir = CommonDefs.op_Addition_Vector3_Vector3(horizontal, vertical).normalized
	self.m_DragDot = dragDot
end

function CLuaDirAutoMoveMgr:ChangeMoveDir(dir, bForce)
	if not (CClientMainPlayer.Inst and CClientMainPlayer.Inst.IsInDirAutoMove) then return end

	local dragDir = self.m_DragDir
	if bForce or math.abs(dragDir.x - dir.x) > self.m_Threshold or math.abs(dragDir.z - dir.z) > self.m_Threshold then
		self.m_DragDir = Vector3(dir.x, 0, dir.z)
		self:UpdateMoveDir()
		self:DoMove()
	end
end

function CLuaDirAutoMoveMgr:OnMainPlayerMoveEnded()
	self:DoMove(true)
end

function CLuaDirAutoMoveMgr:OnPlayerDragJoyStick(args)
	self:ChangeMoveDir(args[0])
end

function CLuaDirAutoMoveMgr:OnPlayerDragJoyStickComplete()
	self:ChangeMoveDir(Vector3(0, 0, 0), true)
end

function Gas2Gac.StartDirAutoMove(vSpeed, hSpeed, dir)
	if not CClientMainPlayer.Inst then return end

	CLuaDirAutoMoveMgr.m_BaseVerticalSpeed = vSpeed
	CLuaDirAutoMoveMgr.m_BaseHorizontalSpeed = hSpeed

	local radian = Mathf.Deg2Rad * dir
	local dirX = Mathf.Cos(radian)
	local dirZ = Mathf.Sin(radian)
	CLuaDirAutoMoveMgr.m_TargetDir = Vector3(dirX, 0, dirZ)
	CLuaDirAutoMoveMgr.m_MoveDir = Vector3(dirX, 0, dirZ)
	CLuaDirAutoMoveMgr.m_DragDir = Vector3(0, 0, 0)
	CLuaDirAutoMoveMgr.m_DragDot = 0

	CClientMainPlayer.Inst.IsInDirAutoMove = true
	CLuaDirAutoMoveMgr:DoMove()
end

function Gas2Gac.EndDirAutoMove()
	if not CClientMainPlayer.Inst then return end

	CClientMainPlayer.Inst.IsInDirAutoMove = false
    CClientMainPlayer.Inst:StopMove()
end

function Gas2Gac.SetDirAutoMoveReverseDrag(bReverse)
	if not CClientMainPlayer.Inst then return end

	CLuaDirAutoMoveMgr.m_ReverseDrag = bReverse
	CLuaDirAutoMoveMgr:DoMove()
end
