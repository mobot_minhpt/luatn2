
local DefaultTableViewDataSource    = import "L10.UI.DefaultTableViewDataSource"
local QnTableView                   = import "L10.UI.QnTableView"
local UILabel                       = import "UILabel"
local CServerTimeMgr                = import "L10.Game.CServerTimeMgr"
local Color                         = import "UnityEngine.Color"
local NGUIText                      = import "NGUIText"

LuaCityWarAwardWnd = class()

--------RegistChildComponent-------
RegistChildComponent(LuaCityWarAwardWnd,         "AdvView",         QnTableView)
RegistChildComponent(LuaCityWarAwardWnd,         "DayAward",        GameObject)
RegistChildComponent(LuaCityWarAwardWnd,         "WeekAward",       GameObject)

RegistChildComponent(LuaCityWarAwardWnd,         "Pos",             UILabel)
RegistChildComponent(LuaCityWarAwardWnd,         "Level_1",         GameObject)
RegistChildComponent(LuaCityWarAwardWnd,         "Level_2",         GameObject)
RegistChildComponent(LuaCityWarAwardWnd,         "Level_3",         GameObject)
RegistChildComponent(LuaCityWarAwardWnd,         "Level_4",         GameObject)
RegistChildComponent(LuaCityWarAwardWnd,         "Level_5",         GameObject)
RegistChildComponent(LuaCityWarAwardWnd,         "Level_6",         GameObject)
RegistChildComponent(LuaCityWarAwardWnd,         "Level_7",         GameObject)
RegistChildComponent(LuaCityWarAwardWnd,         "LevelBtn_1",      GameObject)
RegistChildComponent(LuaCityWarAwardWnd,         "LevelBtn_2",      GameObject)
RegistChildComponent(LuaCityWarAwardWnd,         "LevelBtn_3",      GameObject)
RegistChildComponent(LuaCityWarAwardWnd,         "LevelBtn_4",      GameObject)
RegistChildComponent(LuaCityWarAwardWnd,         "LevelBtn_5",      GameObject)
RegistChildComponent(LuaCityWarAwardWnd,         "LevelBtn_6",      GameObject)
RegistChildComponent(LuaCityWarAwardWnd,         "LevelBtn_7",      GameObject)
RegistChildComponent(LuaCityWarAwardWnd,         "CityLevelLable",  UILabel)
RegistChildComponent(LuaCityWarAwardWnd,         "DayMw",           UILabel)
RegistChildComponent(LuaCityWarAwardWnd,         "DayExp",          UILabel)
RegistChildComponent(LuaCityWarAwardWnd,         "DayAwardBtn",     GameObject)
RegistChildComponent(LuaCityWarAwardWnd,         "DayAwardLabel",   UILabel)
RegistChildComponent(LuaCityWarAwardWnd,         "CityClassName_1", UILabel)
RegistChildComponent(LuaCityWarAwardWnd,         "CityClassName_2", UILabel)
RegistChildComponent(LuaCityWarAwardWnd,         "CityClassName_3", UILabel)
RegistChildComponent(LuaCityWarAwardWnd,         "CityClassName_4", UILabel)
RegistChildComponent(LuaCityWarAwardWnd,         "CityClassName_5", UILabel)
RegistChildComponent(LuaCityWarAwardWnd,         "CityClassName_6", UILabel)
RegistChildComponent(LuaCityWarAwardWnd,         "CityClassName_7", UILabel)
RegistChildComponent(LuaCityWarAwardWnd,         "TurnBtn",         GameObject)


-----上面是日奖励界面-------
-----下面是周奖励界面-------
RegistChildComponent(LuaCityWarAwardWnd,         "PosLabelScore",   UILabel)
RegistChildComponent(LuaCityWarAwardWnd,         "Score",           UILabel)
RegistChildComponent(LuaCityWarAwardWnd,         "PosLabelDes",     UILabel)
RegistChildComponent(LuaCityWarAwardWnd,         "WeekMW",          UILabel)
RegistChildComponent(LuaCityWarAwardWnd,         "WeekBG",          UILabel)
RegistChildComponent(LuaCityWarAwardWnd,         "WeekYinPiao",     UILabel)
RegistChildComponent(LuaCityWarAwardWnd,         "WeekAwardBtn",    GameObject)
RegistChildComponent(LuaCityWarAwardWnd,         "WeekAwardLabel",  UILabel)

---------RegistClassMember-------
RegistClassMember(LuaCityWarAwardWnd,            "m_DataType")          --  当前数据类型
RegistClassMember(LuaCityWarAwardWnd,            "m_DataIndex")         --  当前数据id
RegistClassMember(LuaCityWarAwardWnd,            "m_LevelTable")
RegistClassMember(LuaCityWarAwardWnd,            "m_LevelNameTable")
RegistClassMember(LuaCityWarAwardWnd,            "m_LevelBtnTable")
RegistClassMember(LuaCityWarAwardWnd,            "m_HourAwardTable")    --  每日奖励城战每小时奖励列表
RegistClassMember(LuaCityWarAwardWnd,            "CityClassNameList")
RegistClassMember(LuaCityWarAwardWnd,            "m_AwardIndex")        --  当前邮件在列表中的index

RegistClassMember(LuaCityWarAwardWnd,            "m_IsHeap")            --  是否在累积中

function LuaCityWarAwardWnd:OnEnable()
    -- g_ScriptEvent:AddListener("QueryTerritoryOccupyAwardListResult", self, "QueryTerritoryOccupyAwardListResult")
    g_ScriptEvent:AddListener("QueryTerritoryDayOccupyAwardResult", self, "QueryTerritoryDayOccupyAwardResult")
    g_ScriptEvent:AddListener("QueryTerritoryWeekOccupyAwardResult", self, "QueryTerritoryWeekOccupyAwardResult")
    g_ScriptEvent:AddListener("RequestTerritoryOccupyAwardSuccess", self, "RequestTerritoryOccupyAwardSuccess")
end

function LuaCityWarAwardWnd:OnDisable()
    -- g_ScriptEvent:RemoveListener("QueryTerritoryOccupyAwardListResult", self, "QueryTerritoryOccupyAwardListResult")
    g_ScriptEvent:RemoveListener("QueryTerritoryDayOccupyAwardResult", self, "QueryTerritoryDayOccupyAwardResult")
    g_ScriptEvent:RemoveListener("QueryTerritoryWeekOccupyAwardResult", self, "QueryTerritoryWeekOccupyAwardResult")
    g_ScriptEvent:RemoveListener("RequestTerritoryOccupyAwardSuccess", self, "RequestTerritoryOccupyAwardSuccess")
end

function LuaCityWarAwardWnd:Init()
    self.DayAward:SetActive(false)
    self.WeekAward:SetActive(false)
    self.m_LevelTable = {self.Level_1,self.Level_2,self.Level_3,self.Level_4,self.Level_5,self.Level_6,self.Level_7}
    self.m_LevelBtnTable = {self.LevelBtn_1,self.LevelBtn_2,self.LevelBtn_3,self.LevelBtn_4,self.LevelBtn_5,self.LevelBtn_6,self.LevelBtn_7}
    self.m_LevelNameTable = {self.CityClassName_1,self.CityClassName_2,self.CityClassName_3,self.CityClassName_4,self.CityClassName_5,self.CityClassName_6,self.CityClassName_7}
    for i=1,#self.m_LevelTable do
        self.m_LevelTable[i]:SetActive(false)
    end
    for i=1,#self.m_LevelBtnTable do
        UIEventListener.Get(self.m_LevelBtnTable[i]).onClick = DelegateFactory.VoidDelegate(function(p)
            self:SelectCityLevel(i)
        end)
    end
    UIEventListener.Get(self.DayAwardBtn).onClick = DelegateFactory.VoidDelegate(function(p)
        Gac2Gas.RequestTerritoryOccupyAward(self.m_DataType,self.m_DataIndex)
    end)
    UIEventListener.Get(self.WeekAwardBtn).onClick = DelegateFactory.VoidDelegate(function(p)
        Gac2Gas.RequestTerritoryOccupyAward(self.m_DataType,self.m_DataIndex)
    end)
    UIEventListener.Get(self.TurnBtn).onClick = DelegateFactory.VoidDelegate(function(p)
        CUIManager.ShowUI(CLuaUIResources.CityWarContributionWnd)
    end)
    -- Gac2Gas.QueryTerritoryOccupyAwardList()

    self.CityClassNameList = {}
    local LevelNameList = CityWar_Setting.GetData().RewardClassName
    for i=0,LevelNameList.Length - 1 do
        self.m_LevelNameTable[i+1].text = LevelNameList[i]
        table.insert( self.CityClassNameList, LevelNameList[i] )
    end
    self:InitAdvView()
end

function LuaCityWarAwardWnd:InitAdvView()
    self.AdvView.m_DataSource = DefaultTableViewDataSource.Create(function()
        return #CLuaCityWarMgr.AwardTable
    end, function(item, index)
        self:InitItem(item, index)
    end)

    self.AdvView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
        self:QueryAndRefreshtDate(row + 1)
        local redRot = self.AdvView:GetItemAtRow(row).transform:Find("RedDot").gameObject
        redRot:SetActive(false)
    end)
    self.AdvView:ReloadData(true,false)
    self.AdvView:SetSelectRow(0,true)
end

function LuaCityWarAwardWnd:InitItem(item,index)
    item.name = index
    local TypeLabel = item.transform:Find("TypeLabel"):GetComponent(typeof(UILabel))
    local StateLabel = item.transform:Find("StateLabel"):GetComponent(typeof(UILabel))
    local RedDot = item.transform:Find("RedDot").gameObject
    RedDot:SetActive(false)
    TypeLabel.text = self:GetDataTypeStr(index + 1)
    local stateStr,col = self:GetStateStr(index + 1,RedDot)
    StateLabel.text = stateStr
    StateLabel.color = col
end

function LuaCityWarAwardWnd:GetDataTypeStr(i)
    local dataStr = ""
    if CLuaCityWarMgr.AwardTable[i].awardType == 1  then
        if CLuaCityWarMgr.AwardTable[i].temp == 1 then
            dataStr = LocalString.GetString("今日奖励")
        else
            dataStr = os.date(LocalString.GetString('%y.%m.%d 每日奖励'),(CLuaCityWarMgr.StartTime + CLuaCityWarMgr.AwardTable[i].awardIndex*86400))
        end
    else
        if CLuaCityWarMgr.AwardTable[i].temp == 1 then
            dataStr = LocalString.GetString("本周奖励")
        else
            dataStr = SafeStringFormat3(LocalString.GetString("跨服城战第%s周奖励"), CLuaCityWarMgr.AwardTable[i].awardIndex)
        end
    end
    return dataStr
end

function LuaCityWarAwardWnd:GetStateStr(i,redDot)
    local stateStr = ""
    local color = Color.green
    if CLuaCityWarMgr.AwardTable[i].temp == 1 then
        stateStr = LocalString.GetString("累积中")
        color = Color.yellow
    else
        if CLuaCityWarMgr.AwardTable[i].flag == 1 then
            stateStr = LocalString.GetString("已领取")
            color = NGUIText.ParseColor24("00830B", 0)
        elseif CLuaCityWarMgr.AwardTable[i].flag == 0 then
            local endTime = CServerTimeMgr.ConvertTimeStampToZone8Time(CLuaCityWarMgr.AwardTable[i].expireTime)
            local now = CServerTimeMgr.Inst:GetZone8Time()
            local span = endTime:Subtract(now)
            if span.TotalSeconds > 0 then
                stateStr = SafeStringFormat3(LocalString.GetString("%s天%s:%s后过期"),span.Days,span.Hours,span.Minutes)
                color = Color.red
                redDot:SetActive(true)
            else
                stateStr = LocalString.GetString("已过期")
                color = NGUIText.ParseColor24("5d5d5d", 0)
            end
        else
            stateStr = LocalString.GetString("无奖励可领取")
            color = Color.red
        end
    end
    return stateStr,color
end

function LuaCityWarAwardWnd:QueryAndRefreshtDate(i)
    self.m_DataType = CLuaCityWarMgr.AwardTable[i].awardType
    self.m_DataIndex = CLuaCityWarMgr.AwardTable[i].awardIndex
    self.m_AwardIndex = i
    Gac2Gas.QueryTerritoryOccupyAward(self.m_DataType,self.m_DataIndex)
end

function LuaCityWarAwardWnd:QueryTerritoryDayOccupyAwardResult(temp, dayIndex, mapId, templateId, index, totalMingWang, totalExp, hourAwardTable)    --  刷新日奖励界面
    if self.m_DataType ~=1  or self.m_DataIndex ~= dayIndex then return end

    self.WeekAward:SetActive(false)
    self.DayAward:SetActive(true)
    local PosStr = LocalString.GetString("暂未占领城池")
    self.m_HourAwardTable = hourAwardTable
    if index ~= 0 then
        local myCityName = CityWar_MapCity.GetData(templateId).Name     --占领城池名
        local regionName = CityWar_Map.GetData(mapId).Name              --领土名称
        local regionLevel = CityWar_Map.GetData(mapId).Level            --领土等级
        PosStr = SafeStringFormat3(LocalString.GetString("%s%s（%s级领土）"),regionName,myCityName,regionLevel)
    end
    self.Pos.text = PosStr
    self:SelectCityLevel(8-index)   --  这里服务器给的index从7-1是反的，图片要对应换一下
    self.DayMw.text = totalMingWang
    self.DayExp.text = totalExp
    self:RefreshBtnState(self.m_AwardIndex)
end

function LuaCityWarAwardWnd:SelectCityLevel(index)
    for i=1,#self.m_LevelTable do
        if i == index then
            self.m_LevelTable[i]:SetActive(true)
        else
            self.m_LevelTable[i]:SetActive(false)
        end
    end
    local PosDesStr = ""
    if index ~= 8 then
        PosDesStr = SafeStringFormat3(LocalString.GetString("占领%s每小时可获得%s领土名望，%s角色经验"),self.CityClassNameList[index],self.m_HourAwardTable[8-index].mingwang,self.m_HourAwardTable[8-index].exp)
    end
    self.CityLevelLable.text = PosDesStr
end

function LuaCityWarAwardWnd:QueryTerritoryWeekOccupyAwardResult(temp, weekIndex, mapId, templateId, index, evaluation, yinpiao, banggong, mingwang)   --  刷新周奖励界面
    if self.m_DataType ~=2  or self.m_DataIndex ~= weekIndex then return end

    self.DayAward:SetActive(false)
    self.WeekAward:SetActive(true)
    self.PosLabelScore.text = SafeStringFormat3(LocalString.GetString("跨服城战第%s周个人评价"),self.m_DataIndex)
    self.Score.text = evaluation
    local data = CityWar_Setting.GetData()
    local myCityName = CityWar_MapCity.GetData(templateId).Name     --占领城池名
    local regionName = CityWar_Map.GetData(mapId).Name              --领土名称
    self.PosLabelDes.text = SafeStringFormat3( data.WeekRewardDetailMin,regionName,myCityName)
    if index == 5 then
        self.PosLabelDes.text =SafeStringFormat3( data.WeekRewardDetailMid,regionName,myCityName)
    elseif index>=1 and index<=4 then
        self.PosLabelDes.text =SafeStringFormat3( data.WeekRewardDetailMax,regionName,myCityName)
    end
    self.WeekBG.text = banggong
    self.WeekMW.text = mingwang
    self.WeekYinPiao.text = yinpiao
    self:RefreshBtnState(self.m_AwardIndex)
end

function LuaCityWarAwardWnd:RequestTerritoryOccupyAwardSuccess(awardType, awardIndex)   --  领取成功后刷新左侧列表
    for i=1,#CLuaCityWarMgr.AwardTable do
        if CLuaCityWarMgr.AwardTable[i].awardType == awardType and CLuaCityWarMgr.AwardTable[i].awardIndex == awardIndex then       --  返回时检测是否还在当前面板
           self:RefreshItem(i)
           self:RefreshBtnState(i)
        end
    end
end

function LuaCityWarAwardWnd:RefreshBtnState(i)
    local btn = self.DayAwardBtn
    local Lable = self.DayAwardLabel
    if self.m_DataType == 2 then
        btn = self.WeekAwardBtn
        Lable = self.WeekAwardLabel
    end
    btn:SetActive(false)
    Lable.gameObject:SetActive(true)

    if CLuaCityWarMgr.AwardTable[i].temp == 1 then
        if self.m_DataType == 1 then
            Lable.text = LocalString.GetString("今日奖励累积中，明天0点即可领取")
        else
            Lable.text = LocalString.GetString("本周奖励累积中，下周一0点05分即可领取")
        end
        Lable.color = Color.yellow
    else
        if CLuaCityWarMgr.AwardTable[i].flag == 1 then
            Lable.text = LocalString.GetString("奖励已领取")
            Lable.color = NGUIText.ParseColor24("00830B", 0)
        elseif CLuaCityWarMgr.AwardTable[i].flag == 0 then
            local endTime = CServerTimeMgr.ConvertTimeStampToZone8Time(CLuaCityWarMgr.AwardTable[i].expireTime)
            local now = CServerTimeMgr.Inst:GetZone8Time()
            local span = endTime:Subtract(now)
            if span.TotalSeconds > 0 then       --  可领取
                btn:SetActive(true)
                Lable.gameObject:SetActive(false)
            else
                Lable.text = LocalString.GetString("奖励已过期")
                Lable.color = NGUIText.ParseColor24("5d5d5d", 0)
            end
        else
            Lable.text = LocalString.GetString("无奖励可领取")
            Lable.color = Color.red
        end
    end
end

function LuaCityWarAwardWnd:RefreshItem(row)
    CLuaCityWarMgr.AwardTable[row].flag = 1
    local item = self.AdvView:GetItemAtRow(row-1)
    if item then
        local StateLabel = item.transform:Find("StateLabel"):GetComponent(typeof(UILabel))
        local stateStr,col = self:GetStateStr(row)
        StateLabel.text = stateStr
        StateLabel.color = col
    end
end
