local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local UIInput = import "UIInput"
local CButton = import "L10.UI.CButton"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CRealNameCheckMgr = import "L10.Game.CRealNameCheckMgr"

LuaQMPKCertificationWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaQMPKCertificationWnd, "TipBtn", "TipBtn", GameObject)
RegistChildComponent(LuaQMPKCertificationWnd, "TipLab", "TipLab", UILabel)
RegistChildComponent(LuaQMPKCertificationWnd, "NameInput", "NameInput", UIInput)
RegistChildComponent(LuaQMPKCertificationWnd, "IdInput", "IdInput", UIInput)
RegistChildComponent(LuaQMPKCertificationWnd, "ConfirmButton", "ConfirmButton", CButton)
RegistChildComponent(LuaQMPKCertificationWnd, "FinishNameLab", "FinishNameLab", UILabel)
RegistChildComponent(LuaQMPKCertificationWnd, "FinishIdLab", "FinishIdLab", UILabel)
RegistChildComponent(LuaQMPKCertificationWnd, "FinishBg", "FinishBg", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaQMPKCertificationWnd, "m_Name")
RegistClassMember(LuaQMPKCertificationWnd, "m_Id")

function LuaQMPKCertificationWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    UIEventListener.Get(self.TipBtn).onClick=DelegateFactory.VoidDelegate(function(go)
       g_MessageMgr:ShowMessage("QMPK_Certification_Tip")
    end)

    UIEventListener.Get(self.ConfirmButton.gameObject).onClick=DelegateFactory.VoidDelegate(function(go)
        self:OnConfirmButtonClicked(go)
    end)
end

function LuaQMPKCertificationWnd:Init()
    if CClientMainPlayer.Inst and CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eQmpkPlayData) then
        local playData = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eQmpkPlayData)
        
        if playData.ExpiredTime > CServerTimeMgr.Inst.timeStamp and playData.Data and playData.Data.Data then
            local data = g_MessagePack.unpack(playData.Data.Data)
            self.m_Name = data.m_ShiMing_Name
            self.m_Id = data.m_ShiMing_Id
        end
    end

    if self.m_Name and self.m_Id then
        self:InitFinishInfo(self.m_Name, self.m_Id)
        self:InitEditUI(false)
    else
        self:InitEditUI(true)
    end
end

function LuaQMPKCertificationWnd:OnEnable()
    g_ScriptEvent:AddListener("SubmitQmpkCertificationInfoSuccess", self, "OnSubmitQmpkCertificationInfoSuccess")
end

function LuaQMPKCertificationWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SubmitQmpkCertificationInfoSuccess", self, "OnSubmitQmpkCertificationInfoSuccess")
end

function LuaQMPKCertificationWnd:OnSubmitQmpkCertificationInfoSuccess(name, id)
    self:InitFinishInfo(name, id)
    self:InitEditUI(false)
end

function LuaQMPKCertificationWnd:InitFinishInfo(name, id)
    local chineseLength = #LocalString.GetString("一")
    local replaceNameStr = string.sub(name, 1+chineseLength, #name)
    local replaceIdStr = string.sub(id, 3, #id - 2)

    self.FinishNameLab.text = string.gsub(name, replaceNameStr, string.rep("*", #replaceNameStr / chineseLength))
    self.FinishIdLab.text = string.gsub(id, replaceIdStr, string.rep("*", #replaceIdStr))
end
--@region UIEvent

--@endregion UIEvent
function LuaQMPKCertificationWnd:InitEditUI(isEditable)
    self.NameInput.gameObject:SetActive(isEditable)
    self.IdInput.gameObject:SetActive(isEditable)
    self.ConfirmButton.gameObject:SetActive(isEditable)
    self.FinishNameLab.gameObject:SetActive(not isEditable)
    self.FinishIdLab.gameObject:SetActive(not isEditable)
    self.FinishBg.gameObject:SetActive(not isEditable)

    if not isEditable then
        self.TipLab.text = LocalString.GetString("已完成身份认证，如有问题请联系客服")
    else
        self.TipLab.text = LocalString.GetString("完成身份认证后才可参加淘汰赛")
    end
end

function LuaQMPKCertificationWnd:OnConfirmButtonClicked(go)
    local name = self.NameInput.value
    local id = self.IdInput.value

    if self:CheckName(name) and self:CheckId(id) then
        LuaQMPKCertificationConfirmWnd.s_Name = name
        LuaQMPKCertificationConfirmWnd.s_Id = id
        CUIManager.ShowUI(CLuaUIResources.QMPKCertificationConfirmWnd)
    else
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("请输入正确的个人信息"))
    end
end

function LuaQMPKCertificationWnd:CheckName(name)
    if #name ~= 0 and #name <= 10 then
        return true
    end

    return false
end

function LuaQMPKCertificationWnd:CheckId(id)
    return CRealNameCheckMgr.Inst:CheckChineseCardNum(id)
end