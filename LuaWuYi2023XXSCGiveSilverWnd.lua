local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local UIScrollView = import "UIScrollView"
local UIGrid = import "UIGrid"
local CIMMgr = import "L10.Game.CIMMgr"
local RelationshipType = import "L10.Game.RelationshipType"
local FriendItemData = import "FriendItemData"
local GroupInfo = import "L10.UI.CFriendContactListView+GroupInfo"
local Profession = import "L10.Game.Profession"
local EnumClass = import "L10.Game.EnumClass"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local AlignType = import "CPlayerInfoMgr+AlignType"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local PlayerOperationData = import "L10.Game.PlayerOperationData"

LuaWuYi2023XXSCGiveSilverWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaWuYi2023XXSCGiveSilverWnd, "TopLabel", "TopLabel", UILabel)
RegistChildComponent(LuaWuYi2023XXSCGiveSilverWnd, "ScrollView", "ScrollView", UIScrollView)
RegistChildComponent(LuaWuYi2023XXSCGiveSilverWnd, "Grid", "Grid", UIGrid)
RegistChildComponent(LuaWuYi2023XXSCGiveSilverWnd, "Template", "Template", GameObject)
RegistChildComponent(LuaWuYi2023XXSCGiveSilverWnd, "NoneFriendLabel", "NoneFriendLabel", GameObject)
RegistChildComponent(LuaWuYi2023XXSCGiveSilverWnd, "AddFriendButton", "AddFriendButton", GameObject)
RegistChildComponent(LuaWuYi2023XXSCGiveSilverWnd, "CloseButton", "CloseButton", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaWuYi2023XXSCGiveSilverWnd, "m_Friends")
RegistClassMember(LuaWuYi2023XXSCGiveSilverWnd, "m_CurSilver")
RegistClassMember(LuaWuYi2023XXSCGiveSilverWnd, "m_Id2Item")

function LuaWuYi2023XXSCGiveSilverWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.AddFriendButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnAddFriendButtonClick()
	end)


	
	UIEventListener.Get(self.CloseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCloseButtonClick()
	end)


    --@endregion EventBind end
end

function LuaWuYi2023XXSCGiveSilverWnd:Init()
	self.Template.gameObject:SetActive(false)
	self.m_CurSilver = LuaWuYi2023Mgr.m_SilverCanGive
	self.TopLabel.text = g_MessageMgr:FormatMessage("WuYi2023XXSCGiveSilverWnd_TopLabel",LuaWuYi2023Mgr.m_SilverCanGive)
	Extensions.RemoveAllChildren(self.Grid.transform)

	self:GetFriendData()
	self.m_Id2Item = {}
	for _, t in pairs(self.m_Friends) do
		local obj = NGUITools.AddChild(self.Grid.gameObject, self.Template.gameObject)
		obj:SetActive(true)

		self:InitItem(obj, t, 0)
	end

	self.Grid:Reposition()
	self.ScrollView:ResetPosition()
	self.AddFriendButton.gameObject:SetActive(#self.m_Friends == 0)
	self.NoneFriendLabel.gameObject:SetActive(#self.m_Friends == 0)
end

function LuaWuYi2023XXSCGiveSilverWnd:OnEnable()
	g_ScriptEvent:AddListener("OnFriendWndClose", self, "OnFriendWndClose")
	g_ScriptEvent:AddListener("OnXinXiangShiChengSendCoinSuccess", self, "OnXinXiangShiChengSendCoinSuccess")
end

function LuaWuYi2023XXSCGiveSilverWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnFriendWndClose", self, "OnFriendWndClose")
	g_ScriptEvent:RemoveListener("OnXinXiangShiChengSendCoinSuccess", self, "OnXinXiangShiChengSendCoinSuccess")
end

function LuaWuYi2023XXSCGiveSilverWnd:OnFriendWndClose()
	self:Init()
end

function LuaWuYi2023XXSCGiveSilverWnd:OnXinXiangShiChengSendCoinSuccess(friendId)
	local info = self.m_Id2Item[friendId]
	self:InitItem(info.item, info.data, info.num + 1)
	self.m_CurSilver = self.m_CurSilver - 1
	if self.m_CurSilver <= 0 then
		CUIManager.CloseUI(CLuaUIResources.WuYi2023XXSCGiveSilverWnd)
	end
end

function LuaWuYi2023XXSCGiveSilverWnd:InitItem(obj, data, num)
	local icon = obj.transform:Find("Icon"):GetComponent(typeof(CUITexture))
	local levelLabel = obj.transform:Find("LevelLabel"):GetComponent(typeof(UILabel))
	local classSprite = obj.transform:Find("ClassSprite"):GetComponent(typeof(UISprite))
	local nameLabel = obj.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
	local giveObj = obj.transform:Find("Give")
	local button = obj.transform:Find("Button")
	local giveNumLabel = obj.transform:Find("Give/GiveNumLabel"):GetComponent(typeof(UILabel))

	icon:LoadNPCPortrait(data.friendData.portraitName, not data.friendData.isOnline)
	levelLabel.text = data.friendData.level
	levelLabel.color = data.info.XianFanStatus > 0  and NGUIText.ParseColor24("fe7900", 0) or Color.white
	classSprite.spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), data.info.Class))
	nameLabel.text = data.friendData.name
	nameLabel.alpha = data.friendData.isOnline and 1 or 0.5
	giveObj.gameObject:SetActive(num and num > 0)
	if num then
		giveNumLabel.text = num
	else
		num = 0
	end
	UIEventListener.Get(button.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    Gac2Gas.XinXiangShiChengSendCoin(data.friendData.playerId)
	end)
	UIEventListener.Get(icon.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    CPlayerInfoMgr.ShowPlayerPopupMenu(data.friendData.playerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
	end)
	Extensions.SetLocalPositionZ(classSprite.transform, data.friendData.isOnline and 0 or -1)
	self.m_Id2Item[data.friendData.playerId] = {item = obj, num = num, data = data}
end

function LuaWuYi2023XXSCGiveSilverWnd:GetFriendData()
	self.m_Friends = {}
	for i = 0,CIMMgr.MAX_FRIEND_GROUP_NUM - 1 do
        local groupName = CIMMgr.Inst:GetFriendGroupName(i + 1)
        local info = CreateFromClass(GroupInfo, i + 1, groupName, RelationshipType.Friend)
        local friends = CIMMgr.Inst:GetFriendsByGroup(info.groupId)
        CommonDefs.ListIterate(friends, DelegateFactory.Action_object(function (___value) 
            local playerId = ___value
			local info = CIMMgr.Inst:GetBasicInfo(playerId)
            local friendData = CreateFromClass(FriendItemData, info)
			table.insert(self.m_Friends,{friendData = friendData, info = info})
        end))
    end
	table.sort(self.m_Friends,function(a,b)
		if a.friendData.isOnline ~= b.friendData.isOnline then
			return a.friendData.isOnline
		end
		return a.friendData.friendliness > b.friendData.friendliness
	end)
end

--@region UIEvent

function LuaWuYi2023XXSCGiveSilverWnd:OnAddFriendButtonClick()
	PlayerOperationData.Inst.needChatWithOther = true
	CUIManager.ShowUI(CUIResources.FriendWnd)
end


function LuaWuYi2023XXSCGiveSilverWnd:OnCloseButtonClick()
	if self.m_CurSilver > 0 then
		local msg = g_MessageMgr:FormatMessage("WuYi2023XXSCGiveSilverWnd_GiveSilver_Confirm", self.m_CurSilver)
		MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
			
		end),DelegateFactory.Action(function()
			CUIManager.CloseUI(CLuaUIResources.WuYi2023XXSCGiveSilverWnd)
		end),LocalString.GetString("确定"),LocalString.GetString("取消"),false)
		return
	end
	CUIManager.CloseUI(CLuaUIResources.WuYi2023XXSCGiveSilverWnd)
end


--@endregion UIEvent

