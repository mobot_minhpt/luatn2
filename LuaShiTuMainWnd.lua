local GameObject = import "UnityEngine.GameObject"

local CShiTuMgr = import "L10.Game.CShiTuMgr"
local UITabBar = import "L10.UI.UITabBar"

LuaShiTuMainWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaShiTuMainWnd, "TabBar", "TabBar", UITabBar)
RegistChildComponent(LuaShiTuMainWnd, "ShiTuMainWndEmptyView", "ShiTuMainWndEmptyView", GameObject)
RegistChildComponent(LuaShiTuMainWnd, "Tab1Alert", "Tab1Alert", GameObject)
RegistChildComponent(LuaShiTuMainWnd, "Tab2Alert", "Tab2Alert", GameObject)
RegistChildComponent(LuaShiTuMainWnd, "ShiTuMainWndShiTuView", "ShiTuMainWndShiTuView", GameObject)

--@endregion RegistChildComponent end

function LuaShiTuMainWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.TabBar.OnTabChange = DelegateFactory.Action_GameObject_int(
        function(go, index)
            self:OnTabChange(go, index)
        end
    )
end

function LuaShiTuMainWnd:Init()
    self.ShiTuMainWndShiTuView.gameObject:SetActive(false)
    self.ShiTuMainWndEmptyView.gameObject:SetActive(false)
    self:InitTabIndex()
    Gac2Gas.RequestHasShiTuShiMenReward()
end

function LuaShiTuMainWnd:InitTabIndex()
    local tabIndex = LuaShiTuMgr.m_ShiTuMainWndTabIndex
    self.TabBar:ChangeTab(tabIndex, false)
end

function LuaShiTuMainWnd:OnEnable()
    g_ScriptEvent:AddListener("OnShiTuRelationshipChanged", self, "OnShiTuRelationshipChanged")
    g_ScriptEvent:AddListener("OnSendShiTuShiMenHasReward", self, "OnSendShiTuShiMenHasReward")
end

function LuaShiTuMainWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnShiTuRelationshipChanged", self, "OnShiTuRelationshipChanged")
    g_ScriptEvent:RemoveListener("OnSendShiTuShiMenHasReward", self, "OnSendShiTuShiMenHasReward")
end

function LuaShiTuMainWnd:OnShiTuRelationshipChanged()
    self.ShiTuMainWndShiTuView.gameObject:SetActive(false)
    self.ShiTuMainWndEmptyView.gameObject:SetActive(false)
    local hasShifu = CShiTuMgr.Inst:GetCurrentShiFuId() ~= 0
    local hasTudi = CShiTuMgr.Inst:GetAllCurrentTuDi().Count ~= 0
    self.ShiTuMainWndShiTuView.gameObject:SetActive((LuaShiTuMgr.m_ShiTuMainWndTabIndex == 0 and hasShifu) or (LuaShiTuMgr.m_ShiTuMainWndTabIndex == 1 and hasTudi))
    self.ShiTuMainWndEmptyView.gameObject:SetActive((LuaShiTuMgr.m_ShiTuMainWndTabIndex == 0 and not hasShifu) or (LuaShiTuMgr.m_ShiTuMainWndTabIndex == 1 and not hasTudi))
    if not hasTudi then
        CUIManager.CloseUI(CLuaUIResources.WaiMenDiZiWnd)
    end
end

function LuaShiTuMainWnd:OnSendShiTuShiMenHasReward(hasReward1,hasReward2)
    self.Tab1Alert.gameObject:SetActive(hasReward1)
    self.Tab2Alert.gameObject:SetActive(hasReward2)
end

--@region UIEvent
function LuaShiTuMainWnd:OnTabChange(go, index)
    self.ShiTuMainWndShiTuView.gameObject:SetActive(false)
    self.ShiTuMainWndEmptyView.gameObject:SetActive(false)
    LuaShiTuMgr.m_ShiTuMainWndTabIndex = index
    local hasShifu = CShiTuMgr.Inst:GetCurrentShiFuId() ~= 0
    local hasTudi = CShiTuMgr.Inst:GetAllCurrentTuDi().Count ~= 0
    self.ShiTuMainWndShiTuView.gameObject:SetActive((index == 0 and hasShifu) or (index == 1 and hasTudi))
    self.ShiTuMainWndEmptyView.gameObject:SetActive((index == 0 and not hasShifu) or (index == 1 and not hasTudi))
end
--@endregion UIEvent

