local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"


CLuaQMPKTopRightMenu = class()
RegistClassMember(CLuaQMPKTopRightMenu,"m_TeamBtn")
RegistClassMember(CLuaQMPKTopRightMenu,"m_ConfigBtn")
RegistClassMember(CLuaQMPKTopRightMenu,"m_FightBtn")
RegistClassMember(CLuaQMPKTopRightMenu,"m_TeamAlertObj")
RegistClassMember(CLuaQMPKTopRightMenu,"m_ConfigAlertObj")
RegistClassMember(CLuaQMPKTopRightMenu,"m_MatchingObj")
RegistClassMember(CLuaQMPKTopRightMenu,"m_HasSetInfo")
RegistClassMember(CLuaQMPKTopRightMenu,"m_HasRequestPlayer")

function CLuaQMPKTopRightMenu:OnClickActivityButton(go)
    local list={}
    local item = PopupMenuItemData(LocalString.GetString("全民之星"), DelegateFactory.Action_int(function (index) 
        CUIManager.ShowUI(CLuaUIResources.QMPKStarWnd)
    end), false, nil)
    table.insert( list,item)

    CPopupMenuInfoMgr.ShowPopupMenu(Table2Array(list,MakeArrayClass(PopupMenuItemData)), go.transform, CPopupMenuInfoMgr.AlignType.Bottom, 1, nil, 600, true, 296)
end

function CLuaQMPKTopRightMenu:OnEnable( )
    g_ScriptEvent:AddListener("PlayerRequestJoinQmpkZhanDui", self, "OnPlayerRequestJoinZhanDui")
    g_ScriptEvent:AddListener("QMPKReplySelfPersonalInfo", self, "OnReplySelfPersonalInfo")
    g_ScriptEvent:AddListener("QMPKSyncLoginCount", self, "OnReplySelfPersonalInfo")
    g_ScriptEvent:AddListener("QMPKUpdateFreeMatchingState", self, "UpdateFreeMatchingState")
end
function CLuaQMPKTopRightMenu:OnDisable( )
    g_ScriptEvent:RemoveListener("PlayerRequestJoinQmpkZhanDui", self, "OnPlayerRequestJoinZhanDui")
    g_ScriptEvent:RemoveListener("QMPKReplySelfPersonalInfo", self, "OnReplySelfPersonalInfo")
    g_ScriptEvent:RemoveListener("QMPKSyncLoginCount", self, "OnReplySelfPersonalInfo")
    g_ScriptEvent:RemoveListener("QMPKUpdateFreeMatchingState", self, "UpdateFreeMatchingState")
end
function CLuaQMPKTopRightMenu:UpdateFreeMatchingState( )
    self.m_MatchingObj:SetActive(CQuanMinPKMgr.Inst.m_IsFreeMatching)
end
function CLuaQMPKTopRightMenu:OnReplySelfPersonalInfo( args )
    self.m_HasSetInfo = CQuanMinPKMgr.Inst.m_HasSetInfo
    self:RefreshAlert()
    --SetConfigFx(CQuanMinPKMgr.Inst.m_IsNewbie);
end
function CLuaQMPKTopRightMenu:OnPlayerRequestJoinZhanDui( )
    if CQuanMinPKMgr.Inst.m_IsTeamLeader then
        self.m_HasRequestPlayer = true
    end
    self:RefreshAlert()
end
function CLuaQMPKTopRightMenu:Init( )
    self.m_TeamBtn = self.transform:Find("Anchor/Offset/TopRight/SigninButton").gameObject
    self.m_ConfigBtn = self.transform:Find("Anchor/Offset/TopRight/ConfigButton").gameObject
    self.m_FightBtn = self.transform:Find("Anchor/Offset/TopRight/ShopButton").gameObject
    self.m_TeamAlertObj = self.transform:Find("Anchor/Offset/TopRight/SigninButton/Sprite").gameObject
    self.m_ConfigAlertObj = self.transform:Find("Anchor/Offset/TopRight/ConfigButton/Sprite").gameObject
    self.m_MatchingObj = self.transform:Find("Anchor/Offset/TopRight/ShopButton/Label").gameObject
    self.m_HasSetInfo = true
    self.m_HasRequestPlayer = false

    UIEventListener.Get(self.m_TeamBtn).onClick =DelegateFactory.VoidDelegate(function(go) self:OnTeamBtnClick(go) end)
    UIEventListener.Get(self.m_ConfigBtn).onClick =DelegateFactory.VoidDelegate(function(go) self:OnConfigBtnClick(go) end)
    UIEventListener.Get(self.m_FightBtn).onClick =DelegateFactory.VoidDelegate(function(go) self:OnFightBtnClick(go) end)

    local activityButton=FindChild(self.transform,"ActivityButton").gameObject
    UIEventListener.Get(activityButton).onClick=DelegateFactory.VoidDelegate(function(go)
        self:OnClickActivityButton(go)
    end)

    self.m_TeamAlertObj:SetActive(false)
    self.m_ConfigAlertObj:SetActive(false)
    self.m_MatchingObj:SetActive(false)
    self:OnReplySelfPersonalInfo()
    self:UpdateFreeMatchingState()
end
function CLuaQMPKTopRightMenu:RefreshAlert( )
    self.m_TeamAlertObj:SetActive(self.m_HasRequestPlayer)
    self.m_ConfigAlertObj:SetActive(not self.m_HasSetInfo or CQuanMinPKMgr.Inst.m_LoginCount <= 0)
end
function CLuaQMPKTopRightMenu:OnTeamBtnClick( go) 
    local list ={}
    local myTeam = PopupMenuItemData(LocalString.GetString("我的战队"), DelegateFactory.Action_int(function (index) 
        self.m_HasRequestPlayer = false
        self:RefreshAlert()
        CUIManager.ShowUI(CLuaUIResources.QMPKSelfZhanDuiWnd)
    end), self.m_HasRequestPlayer, nil)
    if CQuanMinPKMgr.Inst.m_HasZhanDui then
        table.insert( list,myTeam )
    end

    local searchZhanDui = PopupMenuItemData(LocalString.GetString("战队列表"), DelegateFactory.Action_int(function (index) 
        CUIManager.ShowUI(CLuaUIResources.QMPKZhanDuiSearchWnd)
    end), false, nil)
    table.insert( list,searchZhanDui)

    CPopupMenuInfoMgr.ShowPopupMenu(Table2Array(list,MakeArrayClass(PopupMenuItemData)), go.transform, CPopupMenuInfoMgr.AlignType.Bottom, 1, nil, 600, true, 296)
end
function CLuaQMPKTopRightMenu:OnConfigBtnClick( go) 
    local list ={}-- CreateFromClass(MakeGenericClass(List, PopupMenuItemData))
    local equipment = PopupMenuItemData(LocalString.GetString("基础配置"), DelegateFactory.Action_int(function (index) 
        CUIManager.ShowUI(CLuaUIResources.QMPKConfigMainWnd)
        CQuanMinPKMgr.Inst.m_LoginCount = 1
        self.m_ConfigAlertObj:SetActive(false)
    end), CQuanMinPKMgr.Inst.m_LoginCount <= 0, nil)
    table.insert( list,equipment )

    local goods = PopupMenuItemData(LocalString.GetString("物品领取"), DelegateFactory.Action_int(function (index) 
        CUIManager.ShowUI(CLuaUIResources.QMPKGetItemWnd)
    end), false, nil)
    table.insert( list,goods )

    local myInfo = PopupMenuItemData(LocalString.GetString("个人名片"), DelegateFactory.Action_int(function (index) 
        self.m_HasSetInfo = true
        self:RefreshAlert()
        CUIManager.ShowUI(CLuaUIResources.QMPKInfoWnd)
    end), not self.m_HasSetInfo, nil)
    table.insert( list,myInfo )


    CPopupMenuInfoMgr.ShowPopupMenu(Table2Array(list,MakeArrayClass(PopupMenuItemData)), go.transform, CPopupMenuInfoMgr.AlignType.Bottom, 1, nil, 600, true, 296)
end
function CLuaQMPKTopRightMenu:OnFightBtnClick( go) 
    if CQuanMinPKMgr.Inst.m_IsFreeMatching then
        CUIManager.ShowUI(CLuaUIResources.QMPKMatchingWnd)
    else
        CUIManager.ShowUI(CLuaUIResources.QMPKBattleWnd)
    end
end

return CLuaQMPKTopRightMenu
