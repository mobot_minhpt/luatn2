local UIWidget = import "UIWidget"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"

LuaLiuRuShiJianShuWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaLiuRuShiJianShuWnd, "LastArrow", "LastArrow", GameObject)
RegistChildComponent(LuaLiuRuShiJianShuWnd, "NextArrow", "NextArrow", GameObject)
RegistChildComponent(LuaLiuRuShiJianShuWnd, "TrueBtn", "TrueBtn", GameObject)
RegistChildComponent(LuaLiuRuShiJianShuWnd, "FalseBtn", "FalseBtn", GameObject)
RegistChildComponent(LuaLiuRuShiJianShuWnd, "LeftLabel", "LeftLabel", UILabel)
RegistChildComponent(LuaLiuRuShiJianShuWnd, "RightLabel", "RightLabel", UILabel)
RegistChildComponent(LuaLiuRuShiJianShuWnd, "Content", "Content", UIWidget)

--@endregion RegistChildComponent end
RegistClassMember(LuaLiuRuShiJianShuWnd, "m_TaskId")
RegistClassMember(LuaLiuRuShiJianShuWnd, "m_BookId")
RegistClassMember(LuaLiuRuShiJianShuWnd, "m_LPage")
RegistClassMember(LuaLiuRuShiJianShuWnd, "m_IsChangingPage")
RegistClassMember(LuaLiuRuShiJianShuWnd, "m_IsFinish")
RegistClassMember(LuaLiuRuShiJianShuWnd, "m_FadeOutTime")
RegistClassMember(LuaLiuRuShiJianShuWnd, "m_FadeInTime")
RegistClassMember(LuaLiuRuShiJianShuWnd, "m_HideCurrPageTweener")
RegistClassMember(LuaLiuRuShiJianShuWnd, "m_ShowNewPageTweener")

function LuaLiuRuShiJianShuWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.LastArrow.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)	    self:OnLastArrowClick()	end)

	UIEventListener.Get(self.NextArrow.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)	    self:OnNextArrowClick()	end)

	UIEventListener.Get(self.TrueBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)	    self:OnTrueBtnClick()	end)

	UIEventListener.Get(self.FalseBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)	    self:OnFalseBtnClick()	end)

    --@endregion EventBind end
end

function LuaLiuRuShiJianShuWnd:Init()
	self.m_BookId = LuaLiuRuShiMgr.m_JianShuInfo.bookId or 1
	self.m_TaskId = LuaLiuRuShiMgr.m_JianShuInfo.taskId or 0
	self.m_LPage = 1
	self.m_FadeOutTime = 1
	self.m_FadeInTime = 1
	self.m_IsFinish = false
	local design = NanDuFanHua_LiuRuShiJianShu.GetData(self.m_BookId) or NanDuFanHua_LiuRuShiJianShu.GetData(1)
	self.LeftLabel.text = CChatLinkMgr.TranslateToNGUIText(design.Page1)
	self.RightLabel.text = CChatLinkMgr.TranslateToNGUIText(design.Page2)
	self.LastArrow:SetActive(false)
	if design.PageNum <= 2 then
		self.NextArrow:SetActive(false)
	else
		self.NextArrow:SetActive(true)
	end
end

function LuaLiuRuShiJianShuWnd:OnDestroy()
    LuaTweenUtils.Kill(self.m_HideCurrPageTweener, false)
    LuaTweenUtils.Kill(self.m_ShowNewPageTweener, false)
end

function LuaLiuRuShiJianShuWnd:HideCurrPage(newLPage)
    LuaTweenUtils.Kill(self.m_HideCurrPageTweener, false)
	LuaTweenUtils.Kill(self.m_ShowNewPageTweener, false)
	self.m_HideCurrPageTweener = LuaTweenUtils.TweenAlpha(self.Content, 1, 0, self.m_FadeOutTime,function() 
		self:ShowNewPage(newLPage)
    end)
end

function LuaLiuRuShiJianShuWnd:ShowNewPage(newLPage)
	local design = NanDuFanHua_LiuRuShiJianShu.GetData(self.m_BookId) or NanDuFanHua_LiuRuShiJianShu.GetData(1)

	self.LeftLabel.text = CChatLinkMgr.TranslateToNGUIText(design["Page"..newLPage])
	local newRPage = newLPage + 1
	if newRPage > design.PageNum then
		self.RightLabel.text = ""
	else
		self.RightLabel.text = CChatLinkMgr.TranslateToNGUIText(design["Page"..newRPage])
	end
	
	if newLPage <= 1 then
		self.LastArrow:SetActive(false)
	else
		self.LastArrow:SetActive(true)
	end
	if newRPage >= design.PageNum then
		self.NextArrow:SetActive(false)
	else
		self.NextArrow:SetActive(true)
	end

	LuaTweenUtils.Kill(self.m_ShowNewPageTweener, false)
	self.m_ShowNewPageTweener = LuaTweenUtils.TweenAlpha(self.Content, 0, 1, self.m_FadeInTime,function() 
		self.m_LPage = newLPage
		self.m_IsChangingPage = false
    end)
end

function LuaLiuRuShiJianShuWnd:FinishTask()
	self.m_IsFinish = true
	local empty = CreateFromClass(MakeGenericClass(List, Object))
	empty = MsgPackImpl.pack(empty)
	Gac2Gas.FinishClientTaskEventWithConditionId(self.m_TaskId, "LiuRuShi_JianShu", empty, self.m_BookId)
	CUIManager.CloseUI(CLuaUIResources.LiuRuShiJianShuWnd)
end

function LuaLiuRuShiJianShuWnd:FailTask()
	self.m_IsFinish = true
	LuaLiuRuShiMgr:ShowLiuRuShiTaskFailWnd(self.m_TaskId, NanDuFanHua_LiuRuShiSetting.GetData().JianShuTaskFailText)
	CUIManager.CloseUI(CLuaUIResources.LiuRuShiJianShuWnd)
end

--@region UIEvent

function LuaLiuRuShiJianShuWnd:OnLastArrowClick()
	if self.m_LPage == 1 then return end
	if self.m_IsChangingPage or self.m_IsFinish then return end

	local newLPage = math.max(self.m_LPage - 2, 1)
	self.m_IsChangingPage = true
	self:HideCurrPage(newLPage)
end

function LuaLiuRuShiJianShuWnd:OnNextArrowClick()
	if self.m_IsChangingPage then return end
	local design = NanDuFanHua_LiuRuShiJianShu.GetData(self.m_BookId) or NanDuFanHua_LiuRuShiJianShu.GetData(1)
	if self.m_LPage + 1 >= design.PageNum then return end

	local newLPage = self.m_LPage + 2
	self.m_IsChangingPage = true
	self:HideCurrPage(newLPage)
end

function LuaLiuRuShiJianShuWnd:OnTrueBtnClick()
	local design = NanDuFanHua_LiuRuShiJianShu.GetData(self.m_BookId) or NanDuFanHua_LiuRuShiJianShu.GetData(1)
	if design.Answer == 1 then
		self:FinishTask()
	else
		self:FailTask()
	end
end

function LuaLiuRuShiJianShuWnd:OnFalseBtnClick()
	local design = NanDuFanHua_LiuRuShiJianShu.GetData(self.m_BookId) or NanDuFanHua_LiuRuShiJianShu.GetData(1)
	if design.Answer == 1 then
		self:FailTask()
	else
		self:FinishTask()
	end
end

--@endregion UIEvent

