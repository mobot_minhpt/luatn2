
local EUIModuleGroup = import "L10.UI.CUIManager+EUIModuleGroup"
local CUIModule = import "L10.UI.CUIModule"

CUIManager.m_hookAppendUIGroups = function(this)
    local EBgGameUI_Table = {
        CLuaUIResources.ZongMenTopRightWnd,
        CLuaUIResources.QiXi2021RightBottom,
        CLuaUIResources.OffWorldPlay1TopRightWnd,
        CLuaUIResources.OffWorldPlay2TopRightWnd,
        CLuaUIResources.HMLZPlayingWnd,
        CLuaUIResources.LuoCha2021PlayingWnd,
        CLuaUIResources.WeddingTopWnd,
        CLuaUIResources.WeddingBottomWnd,
        CLuaUIResources.YinHunPoZhiTopRightWnd,
        CLuaUIResources.GaoChangTopRightWnd,
        CLuaUIResources.WeddingSprinkleCandyWnd,
        CLuaUIResources.GuildTerritorialWarsTopRightWnd,
        CLuaUIResources.GuildTerritorialWarsChallengeTopRightWnd,
        CLuaUIResources.GuildTerritorialWarsPeripheryAlertWnd,
        CLuaUIResources.HaiZhanDriveWnd,
        CLuaUIResources.HouseRollerCoasterWnd,
        CLuaUIResources.JuDianBattlePlayingWnd,
        CLuaUIResources.QiXi2022FindItemSkillWnd,
        CLuaUIResources.LiuYi2022PopoPlayingWnd,
        CLuaUIResources.ShuiManJinShanStateWnd,
        CLuaUIResources.WuLiangCompanionBtnWnd,
        CLuaUIResources.JuDianBattleDailyPlayingWnd,
        CLuaUIResources.JuDianBattlePlayingWnd,
        CLuaUIResources.MengQuanHengXingTopRightWnd,
        CLuaUIResources.ShenZhaiTanBaoKickSkillWnd,
        CLuaUIResources.GuoQingPvPPlayingWnd,
        CLuaUIResources.TurkeyMatchStateWnd,
        CLuaUIResources.TurkeyMatchSkillWnd,
        CLuaUIResources.CuJuMinimap,
        CLuaUIResources.ChunJie2023ShengYanTopRightWnd,
        CLuaUIResources.HaiZhanStateWnd,
        CLuaUIResources.WuYi2023FYZXTopRightWnd,
		CLuaUIResources.DuanWu2023PVPVETopRightWnd,
		CLuaUIResources.QingMing2023PVPTopRightWnd,
		CLuaUIResources.ArenaJingYuanTopRightWnd,
        CLuaUIResources.DaFuWongMainPlayWnd,
        CLuaUIResources.DaFuWongPlayLandTopWnd,
        CLuaUIResources.LiuYi2023DaoDanXiaoGuiStateWnd,
        CLuaUIResources.Carnival2023PVEPlayerCountWnd,
        CLuaUIResources.ZYPDStateWnd,
        CLuaUIResources.YaoYeManJuanStateWnd,
        CLuaUIResources.HengYuDangKouPlayingWnd,
        CLuaUIResources.Christmas2023BingXuePlayPlayingWnd,
        CLuaUIResources.YuanDan2024PVEGridWnd,
        CLuaUIResources.ZNQ2024NLDBTopRightWnd,
    }
    local ListToAdd = Table2List(EBgGameUI_Table, MakeGenericClass(List, CUIModule))
    CommonDefs.ListAddRange(CommonDefs.DictGetValue_LuaCall(this.mUIGroups, EUIModuleGroup.EBgGameUI), ListToAdd)

end

--暂时放这里
local CMiddleNoticeCenterWnd = import "L10.UI.CMiddleNoticeCenterWnd"
local Lua=import "L10.Engine.Lua"
CMiddleNoticeCenterWnd.m_hookInit = function(this)
    Lua.s_IsHook = false
    local yunAlert = this.transform:Find("YunAlert")
    if yunAlert then
        yunAlert.gameObject:SetActive(CommonDefs.IS_LEBIAN_YUN_CLIENT)
    end
end

local CCutSceneSoundSettingButton = import "L10.UI.CCutSceneSoundSettingButton"
CCutSceneSoundSettingButton.m_hookOnClickSoundSettingButton = function(this, go)
    if go and LuaMiniVolumeSettingsMgr.s_CutSceneUIInstance.gameObject then
        if not LuaMiniVolumeSettingsMgr.s_CutSceneUIInstance.gameObject.activeSelf then
            LuaMiniVolumeSettingsMgr.s_OpenButton = go
            LuaMiniVolumeSettingsMgr.s_TargetPosition = go.transform.position
            LuaMiniVolumeSettingsMgr.s_Pivot = UIWidget.Pivot.Left
            LuaMiniVolumeSettingsMgr.s_CutSceneUIInstance.gameObject:SetActive(true)
            go.transform:Find("Selected").gameObject:SetActive(true)
        else
            LuaMiniVolumeSettingsMgr.s_CutSceneUIInstance.gameObject:SetActive(false)
            go.transform:Find("Selected").gameObject:SetActive(false)
        end
    end
end
