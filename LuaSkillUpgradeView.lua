local CSkillUpgradeTopView = import "L10.UI.CSkillUpgradeTopView"
local CSkillUpgradeBottomView = import "L10.UI.CSkillUpgradeBottomView"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"

LuaSkillUpgradeView = class()
RegistClassMember(LuaSkillUpgradeView,"m_TopView")
RegistClassMember(LuaSkillUpgradeView,"m_BottomView")

function LuaSkillUpgradeView:Awake()
    self:InitComponents()
end

function LuaSkillUpgradeView:OnEnable()
    self.m_TopView.OnSkillClassSelect = CommonDefs.CombineListner_Action_uint(self.m_TopView.OnSkillClassSelect,
            DelegateFactory.Action_uint(function (skillClassId)
                self:OnSkillClassSelect(skillClassId)
            end)
    , true)
    self:Init()
end

function LuaSkillUpgradeView:OnDisable()
    self.m_TopView.OnSkillClassSelect = CommonDefs.CombineListner_Action_uint(self.m_TopView.OnSkillClassSelect,
            DelegateFactory.Action_uint(function (skillClassId)
                self:OnSkillClassSelect(skillClassId)
            end)
    , false)
end

function LuaSkillUpgradeView:InitComponents()
    self.m_TopView = self.transform:Find("Anchor/TopView"):GetComponent(typeof(CSkillUpgradeTopView))
    self.m_BottomView = self.transform:Find("Anchor/BottomView"):GetComponent(typeof(CSkillUpgradeBottomView))
end

function LuaSkillUpgradeView:Init()
    self.m_TopView:Init(0)
    self:InitDetailBtn()
end

function LuaSkillUpgradeView:OnSkillClassSelect(skillClassId)
    self.m_BottomView:Init(skillClassId)
end

function LuaSkillUpgradeView:Get3rdSkillTab()
    local skillClass = CLuaGuideMgr.Get3rdSkillClass()
    return self.m_TopView:GetSkillTab(skillClass)
end
function LuaSkillUpgradeView:Get3rdSkillItem()
    local skillClass = CLuaGuideMgr.Get3rdSkillClass()
    return self.m_TopView:GetSkillItem(skillClass)
end
function LuaSkillUpgradeView:Get6thSkillTab()
    local skillClass = CLuaGuideMgr.Get6thSkillClass()
    return self.m_TopView:GetSkillTab(skillClass)
end
function LuaSkillUpgradeView:Get6thSkillItem()
    local skillClass = CLuaGuideMgr.Get6thSkillClass()
    return self.m_TopView:GetSkillItem(skillClass)
end
function LuaSkillUpgradeView:GetUpgradeButton()
    return self.m_BottomView:GetUpgradeButton()
end

function LuaSkillUpgradeView:InitDetailBtn()
    local Btn = self.transform:Find("Anchor/BottomView/DetailButton").gameObject
    Btn:SetActive(false)
    if LuaPlayerPropertyMgr.PropertyGuide then 
        Btn:SetActive(true)
        UIEventListener.Get(Btn).onClick = DelegateFactory.VoidDelegate(function (go)
            LuaPlayerPropertyMgr:QuerySkillDetailWnd(self.m_BottomView.curSkillId,self.m_BottomView.curSkillClsId)
        end)
    end
end

function LuaSkillUpgradeView:GetTargetUpgradeSkillIcon()
    return self.m_TopView:GetTargetUpgradeSkillIcon()
end
function LuaSkillUpgradeView:GetTargetUpgradeSkillTab()
    return self.m_TopView:GetTargetUpgradeSkillTab()
end