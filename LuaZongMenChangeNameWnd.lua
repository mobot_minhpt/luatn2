






local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local UIInput = import "UIInput"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local GameObject = import "UnityEngine.GameObject"
local UIGrid = import "UIGrid"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local Constants = import "L10.Game.Constants"

LuaZongMenChangeNameWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaZongMenChangeNameWnd, "ZongMenNameLabel", "ZongMenNameLabel", UILabel)
RegistChildComponent(LuaZongMenChangeNameWnd, "Input", "Input", UIInput)
RegistChildComponent(LuaZongMenChangeNameWnd, "SelectNameSuffixButton", "SelectNameSuffixButton", QnSelectableButton)
RegistChildComponent(LuaZongMenChangeNameWnd, "NameSuffixLabel", "NameSuffixLabel", UILabel)
RegistChildComponent(LuaZongMenChangeNameWnd, "NameSuffixList", "NameSuffixList", GameObject)
RegistChildComponent(LuaZongMenChangeNameWnd, "NameSuffixGrid", "NameSuffixGrid", UIGrid)
RegistChildComponent(LuaZongMenChangeNameWnd, "SelectNameSuffixButtonTemplate", "SelectNameSuffixButtonTemplate", GameObject)
RegistChildComponent(LuaZongMenChangeNameWnd, "OkButton", "OkButton", GameObject)
RegistChildComponent(LuaZongMenChangeNameWnd, "Blank", "Blank", GameObject)
RegistChildComponent(LuaZongMenChangeNameWnd, "LastNamesLabel", "LastNamesLabel", UILabel)
RegistChildComponent(LuaZongMenChangeNameWnd, "LastChangeTimeLabel", "LastChangeTimeLabel", UILabel)
RegistChildComponent(LuaZongMenChangeNameWnd, "CostLabel", "CostLabel", UILabel)
RegistChildComponent(LuaZongMenChangeNameWnd, "AlertHeFuCostLabel", "AlertHeFuCostLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaZongMenChangeNameWnd, "m_NameSuffixQnRadioBox")

function LuaZongMenChangeNameWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	self.SelectNameSuffixButton.OnButtonSelected = DelegateFactory.Action_bool(function (selected)
	    self:OnSelectNameSuffixButtonSelected(selected)
	end)

	UIEventListener.Get(self.OkButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOkButtonClick()
	end)

	UIEventListener.Get(self.Blank.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBlankClick()
	end)

    --@endregion EventBind end
end

function LuaZongMenChangeNameWnd:Init()
    if CommonDefs.IS_VN_CLIENT then
        self.Input.characterLimit = 30
    end
    self.AlertHeFuCostLabel.gameObject:SetActive(false)
    self:InitNameSuffixQnRadioBox()
    self.ZongMenNameLabel.text = LuaZongMenMgr.m_ZongMenName
    self.LastNamesLabel.text = ""
    self.LastChangeTimeLabel.text = ""
    self.CostLabel.text = Menpai_Setting.GetData("SectChangeNameCost").Value
    if CClientMainPlayer.Inst then
        Gac2Gas.QuerySectAlterNameInfo(CClientMainPlayer.Inst.BasicProp.SectId)
    end
end

--@region UIEvent

function LuaZongMenChangeNameWnd:OnSelectNameSuffixButtonSelected(selected)
	if not self.m_NameSuffixQnRadioBox then return end
	self.m_NameSuffixQnRadioBox.gameObject:SetActive(selected)
end


function LuaZongMenChangeNameWnd:OnOkButtonClick()
    local str = tostring(self.Input.value)
    str = string.gsub(str, "%s+", "")
    local error = false
    if cs_string.IsNullOrEmpty(str) then
        error = true
    end
    if CommonDefs.IsSeaMultiLang() then
        local len = CommonDefs.StringLength(str)
        -- 宗派名VARBINARY(40)
        if not error and (len < Constants.SeaMinCharLimitForSectName or len > Constants.SeaMaxCharLimitForSectName) then
            error = true
        end
    else
        local len = CUICommonDef.GetStrByteLength(str)
        if not error and len< 4 then
            error = true
        end
    end
    if not CommonDefs.IS_VN_CLIENT then
        if not error then
            if not System.Text.RegularExpressions.Regex.IsMatch(str, "^[\\u4e00-\\u9fbb]+$") then
                error = true
            end
        end
    end
    if error then
        if CommonDefs.IsSeaMultiLang() then
            g_MessageMgr:ShowMessage("Sea_MenPaiCreate_NameConfirm", Constants.SeaMinCharLimitForSectName, Constants.SeaMaxCharLimitForSectName)
        else
            g_MessageMgr:ShowMessage("MenPaiCreate_NameConfirm")
        end
        return
    end

    local text = str.. self.NameSuffixLabel.text
    local ret = CWordFilterMgr.Inst:CheckName(text, LocalString.GetString("宗派改名"))
    if not ret then
        g_MessageMgr:ShowMessage("FENZU_VIOLATION")
        return
    end
    if CClientMainPlayer.Inst then
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("ZongMen_ChangeName_Confirm",text),DelegateFactory.Action(function()
			Gac2Gas.RequestAlterSectName(CClientMainPlayer.Inst.BasicProp.SectId, text)
		end),nil,LocalString.GetString("确定"),LocalString.GetString("取消"),false)  
    end
    CUIManager.CloseUI(CLuaUIResources.ZongMenChangeNameWnd)
end


function LuaZongMenChangeNameWnd:OnBlankClick()
    self.SelectNameSuffixButton:SetSelected(false)
end


--@endregion UIEvent

function LuaZongMenChangeNameWnd:InitNameSuffixQnRadioBox()
    self.SelectNameSuffixButtonTemplate:SetActive(false)
    local list = g_LuaUtil:StrSplit(Menpai_Setting.GetData("NameSuffixList").Value, ",")
    Extensions.RemoveAllChildren(self.NameSuffixGrid.transform)
    self.m_NameSuffixQnRadioBox = self.NameSuffixList:AddComponent(typeof(QnRadioBox))
    self.m_NameSuffixQnRadioBox.m_RadioButtons = CreateFromClass(MakeArrayClass(QnSelectableButton), #list)
    for i,text in pairs(list) do
        local go = NGUITools.AddChild(self.NameSuffixGrid.gameObject, self.SelectNameSuffixButtonTemplate)
        go:SetActive(true)
        go.transform:Find("Label"):GetComponent(typeof(UILabel)).text = text
        self.m_NameSuffixQnRadioBox.m_RadioButtons[i - 1] = go:GetComponent(typeof(QnSelectableButton))
        self.m_NameSuffixQnRadioBox.m_RadioButtons[i - 1].OnClick = MakeDelegateFromCSFunction(self.m_NameSuffixQnRadioBox.On_Click, MakeGenericClass(Action1, QnButton), self.m_NameSuffixQnRadioBox)
    end
    self.NameSuffixGrid:Reposition()
    self.SelectNameSuffixButton:SetSelected(false)
    self.m_NameSuffixQnRadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn,index)
        self:OnSelectNameSuffixRadioBox(index)
    end)
    self.m_NameSuffixQnRadioBox:ChangeTo(0, true)
end

function LuaZongMenChangeNameWnd:OnSelectNameSuffixRadioBox(index)
	if not self.m_NameSuffixQnRadioBox or not self.m_NameSuffixQnRadioBox.m_RadioButtons or self.m_NameSuffixQnRadioBox.m_RadioButtons.Length <= index then return end
	self.NameSuffixLabel.text = self.m_NameSuffixQnRadioBox.m_RadioButtons[index].Text
end

function LuaZongMenChangeNameWnd:OnEnable()
    g_ScriptEvent:AddListener("OnSyncSectAlterNameInfo", self, "OnSyncSectAlterNameInfo")
end

function LuaZongMenChangeNameWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnSyncSectAlterNameInfo", self, "OnSyncSectAlterNameInfo")
end

function LuaZongMenChangeNameWnd:OnSyncSectAlterNameInfo(lastAlterNameTime, alterNameCost, nameUsedListUd)
    local list = MsgPackImpl.unpack(nameUsedListUd) 
    if list.Count == 0 then
        self.LastNamesLabel.text = LocalString.GetString("无")
    elseif list.Count == 1 then
        self.LastNamesLabel.text = list[0]
    elseif list.Count == 2 then
        self.LastNamesLabel.text = list[0] .. LocalString.GetString("、") .. list[1]
    end
    if lastAlterNameTime > 0 then
        local now = CServerTimeMgr.Inst:GetZone8Time()
        local lastTime = CServerTimeMgr.ConvertTimeStampToZone8Time(lastAlterNameTime)
        local dayDiff =  CServerTimeMgr.Inst:DayDiff(now,lastTime)
        self.LastChangeTimeLabel.text = SafeStringFormat3(LocalString.GetString("%d天"),dayDiff < 1 and 1 or dayDiff) 
    else
        self.LastChangeTimeLabel.text = LocalString.GetString("无")
    end
    
    self.CostLabel.text = alterNameCost
    self.AlertHeFuCostLabel.gameObject:SetActive(alterNameCost == 0)
end
