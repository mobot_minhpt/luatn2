local CClientFurnitureMgr=import "L10.Game.CClientFurnitureMgr"
local EnumMoneyType = import "L10.Game.EnumMoneyType"

CLuaWinterHouseUnlockPreviewMgr = class()

CLuaWinterHouseUnlockPreviewMgr.TemplateId = nil

function CLuaWinterHouseUnlockPreviewMgr.OpenPreviewWnd(fId, templateId)
    CLuaWinterHouseUnlockPreviewMgr.FurnitureId = fId
    CLuaWinterHouseUnlockPreviewMgr.TemplateId = templateId

    local data = WinterHouse_Zhuangshiwu.GetData(templateId)

    if data ~= nil then
        CLuaWinterHouseUnlockPreviewMgr.Cost = data.Price
        local zsw = Zhuangshiwu_Zhuangshiwu.GetData(templateId)
        local msg = CClientFurnitureMgr.Inst.m_IsWinter and g_MessageMgr:FormatMessage("WinterHouse_BuySkin_Confirm", zsw.Name) or g_MessageMgr:FormatMessage("WinterHouse_BuySkin_Off_Season_Confirm", zsw.Name)
        if data.Currency == 1 then
            --银两
            CLuaWinterHouseUnlockPreviewMgr.MoneyType = EnumMoneyType.YinLiang
        elseif data.Currency == 2 then
            --灵玉
            CLuaWinterHouseUnlockPreviewMgr.MoneyType = EnumMoneyType.LingYu
        elseif data.Currency == 3 then
            CLuaWinterHouseUnlockPreviewMgr.MoneyType = EnumMoneyType.YinPiao
        end
        CLuaWinterHouseUnlockPreviewMgr.ConfirmMsg = msg
        CUIManager.ShowUI(CLuaUIResources.WinterHouseUnlockPreviewWnd)
    end
end

function CLuaWinterHouseUnlockPreviewMgr.ResetParms()
    CLuaWinterHouseUnlockPreviewMgr.TemplateId = nil
    CLuaWinterHouseUnlockPreviewMgr.FurnitureId = nil
    CLuaWinterHouseUnlockPreviewMgr.Cost = 0
    CLuaWinterHouseUnlockPreviewMgr.MoneyType = EnumMoneyType.YinLiang
    CLuaWinterHouseUnlockPreviewMgr.ConfirmMsg = ""

end