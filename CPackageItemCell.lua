-- Auto Generated!!
local CClientFurnitureMgr = import "L10.Game.CClientFurnitureMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CPackageItemCell = import "L10.UI.CPackageItemCell"
local Item_Item = import "L10.Game.Item_Item"
local Zhuangshiwu_Zhuangshiwu = import "L10.Game.Zhuangshiwu_Zhuangshiwu"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
CPackageItemCell.m_CheckExtraSprite_CS2LuaHook = function (this, item)
    if this.extraSprite ~= nil then
        this.extraSprite.gameObject:SetActive(false)
        if item ~= nil then
            local data = Item_Item.GetData(item.TemplateId)
            if data ~= nil and data.Type == EnumItemType_lua.Zhuangshiwu then
                local extraString = data.ExtraAttribute
                if (string.find(extraString, "zhuangshiwu", 1, true) ~= nil) then
                    local number = CommonDefs.StringSubstring1(extraString, 12)
                    local zswTid
                    local default
                    default, zswTid = System.UInt32.TryParse(number)
                    if default then
                        local zswdata = Zhuangshiwu_Zhuangshiwu.GetData(zswTid)
                        if zswdata ~= nil and zswdata.Type == EnumFurnitureType_lua.eLingshou then
                            local name = CClientFurnitureMgr.GetLingshouSpriteName(zswdata.Quality)
                            if this.extraSprite ~= nil then
                                this.extraSprite.gameObject:SetActive(true)
                                this.extraSprite.spriteName = name
                            end
                        end
                    end
                end
            end
        end
    end

    local expiringNoticeSpriteTransform = this.transform:Find('ExpiringNoticeSprite')
    if expiringNoticeSpriteTransform then
        local expiringNoticeNode = expiringNoticeSpriteTransform.gameObject
        expiringNoticeNode:SetActive(false)
        if item ~= nil and item.IsItem then
            local expiredTime = CServerTimeMgr.ConvertTimeStampToZone8Time(item.Item:GetRealExpiredTime())
            local span = expiredTime:Subtract(CServerTimeMgr.Inst:GetZone8Time())
            if span.TotalHours > 0 and span.TotalHours <= GameSetting_Client.GetData().ItemExpiringNoticeTime then
                expiringNoticeNode:SetActive(true)
            end
        end
    end

end
