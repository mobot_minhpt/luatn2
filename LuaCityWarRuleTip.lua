require("common/common_include")

local UILabel = import "UILabel"
local NGUITools = import "NGUITools"
local UITable = import "UITable"

CLuaCityWarRuleTip = class()
CLuaCityWarRuleTip.Path = "ui/citywar/LuaCityWarRuleTip"

function CLuaCityWarRuleTip:Init( ... )
	local templateObj = self.transform:Find("Offset/Template").gameObject
	templateObj:SetActive(false)
	local paraObj = self.transform:Find("Offset/Template/Item").gameObject
	paraObj:SetActive(false)
	local rootTable = self.transform:Find("Offset/Scroll View/Table"):GetComponent(typeof(UITable))

 	local content = g_MessageMgr:FormatMessage("KFCZ_RULE_INTERFACE_MSG")
 	local index = 1
 	while true do
 		local prevTag, _ = string.find(content, "<title>")
 		local nextTag, _ = string.find(content, "<title>", prevTag + 1)
 		local curContent = content
 		if nextTag then
 			curContent = string.sub(content, prevTag, nextTag - 1)
 			content = string.sub(content, nextTag)
 		end

 		local obj = NGUITools.AddChild(rootTable.gameObject, templateObj)
 		obj:SetActive(true)

 		local title = string.match(curContent, "<title>(.*)</title>")
 		obj.transform:Find("Title"):GetComponent(typeof(UILabel)).text = title

 		local pTable = obj.transform:Find("Table"):GetComponent(typeof(UITable))
 		for p in string.gmatch(curContent, "<p>(.-)</p>") do
 			local pObj = NGUITools.AddChild(pTable.gameObject, paraObj)
 			pObj:SetActive(true)
 			pObj.transform:Find("Label"):GetComponent(typeof(UILabel)).text = p
 		end
 		pTable:Reposition()

 		if not nextTag then break end
 	end
 	rootTable:Reposition()
end
