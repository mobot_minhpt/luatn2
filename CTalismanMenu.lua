-- Auto Generated!!
local CTalismanMenu = import "L10.UI.CTalismanMenu"
local CTalismanMenuMgr = import "L10.UI.CTalismanMenuMgr"
local CTalismanMenuTemplate = import "L10.UI.CTalismanMenuTemplate"
local Extensions = import "Extensions"
local LocalString = import "LocalString"
local NGUITools = import "NGUITools"
local Vector3 = import "UnityEngine.Vector3"
CTalismanMenu.m_Init_CS2LuaHook = function (this) 
    Extensions.RemoveAllChildren(this.grid.transform)
    this.menuTemplate:SetActive(false)
    local len = CClientMainPlayer.Inst.ItemProp.MultipleColumTalismanNum 
    for i = 0, len - 1 do
        local instance = NGUITools.AddChild(this.grid.gameObject, this.menuTemplate)
        instance:SetActive(true)
        local template = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CTalismanMenuTemplate))
        if template ~= nil then
            if CLuaQMPKMgr.s_IsOtherPlayer then
                template:Init(true, System.String.Format(LocalString.GetString("法宝栏{0}"), i + 1), i, false)
            else
                template:Init(i ~= 1 or CTalismanMenuMgr.talismanMenu2Open, System.String.Format(LocalString.GetString("法宝栏{0}"), i + 1), i, CTalismanMenuMgr.curSelectIndex == i)
            end
        end
    end
    this.background.height = this.menuTemplate:GetComponent(typeof(UISprite)).height * len + 18
    this.grid:Reposition()
    if CLuaTalismanMenu.m_MenuPosition then
        local x = CLuaTalismanMenu.m_MenuPosition.x 
        local y = CLuaTalismanMenu.m_MenuPosition.y
        this.background.transform.position = Vector3(x, y, 0)
        CLuaTalismanMenu.m_MenuPosition = nil
    end
end

CLuaTalismanMenu = {}
CLuaTalismanMenu.m_MenuPosition = nil