local GameObject = import "UnityEngine.GameObject"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local UISlider = import "UISlider"
local QnButton = import "L10.UI.QnButton"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local CUIFx = import "L10.UI.CUIFx"
local CNumberKeyBoardMgr = import "CNumberKeyBoardMgr"
local AlignType = import "L10.UI.CTooltip+AlignType"
local MessageWndManager = import "L10.UI.MessageWndManager"

LuaRepairTongTianTaSubmitSilverWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaRepairTongTianTaSubmitSilverWnd, "m_ProgressLab", "ProgressLab", UILabel)
RegistChildComponent(LuaRepairTongTianTaSubmitSilverWnd, "m_Slider", "Slider", UISlider)
RegistChildComponent(LuaRepairTongTianTaSubmitSilverWnd, "m_InputBtn", "InputBtn", QnButton)
RegistChildComponent(LuaRepairTongTianTaSubmitSilverWnd, "m_NumLab", "NumLab", UILabel)
RegistChildComponent(LuaRepairTongTianTaSubmitSilverWnd, "m_MaxInputLab", "MaxInputLab", UILabel)
RegistChildComponent(LuaRepairTongTianTaSubmitSilverWnd, "m_AddPrestigeLab", "AddPrestigeLab", UILabel)
RegistChildComponent(LuaRepairTongTianTaSubmitSilverWnd, "m_AddProgress", "AddProgress", UILabel)
RegistChildComponent(LuaRepairTongTianTaSubmitSilverWnd, "m_QnCostAndOwnMoney", "QnCostAndOwnMoney", CCurentMoneyCtrl)
RegistChildComponent(LuaRepairTongTianTaSubmitSilverWnd, "m_TipBtn", "TipBtn", QnButton)
RegistChildComponent(LuaRepairTongTianTaSubmitSilverWnd, "m_OpBtn", "OpBtn", QnButton)
RegistChildComponent(LuaRepairTongTianTaSubmitSilverWnd, "m_Fx", "Fx", CUIFx)
RegistChildComponent(LuaRepairTongTianTaSubmitSilverWnd, "m_Input", "Input", GameObject)
RegistChildComponent(LuaRepairTongTianTaSubmitSilverWnd, "m_Effect", "Effect", GameObject)
RegistChildComponent(LuaRepairTongTianTaSubmitSilverWnd, "m_SubmitToMaxLabel", "SubmitToMaxLabel", UILabel)

--@endregion RegistChildComponent end

RegistClassMember(LuaRepairTongTianTaSubmitSilverWnd, "m_InputNum")
RegistClassMember(LuaRepairTongTianTaSubmitSilverWnd, "m_InputMax")
RegistClassMember(LuaRepairTongTianTaSubmitSilverWnd, "m_TotalSubmitedSilver")
RegistClassMember(LuaRepairTongTianTaSubmitSilverWnd, "m_NeedSilverToRepairTongTianTa")

function LuaRepairTongTianTaSubmitSilverWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaRepairTongTianTaSubmitSilverWnd:Init()
    self:UpdateInfo(false)
    self.m_InputBtn.OnClick = DelegateFactory.Action_QnButton(function (go)
        self:ShowInputWnd()
    end)

    self.m_OpBtn.OnClick = DelegateFactory.Action_QnButton(function (go)
        if self.m_InputNum ~= nil and self.m_InputNum == 0 then
            g_MessageMgr:ShowMessage("CUSTOM_STRING2",LocalString.GetString("请输入需要捐献的银两"))
        elseif self.m_InputNum ~= nil and self.m_InputNum ~= 0 then
            MessageWndManager.ShowOKCancelMessage(SafeStringFormat3(LocalString.GetString("是否捐献%d银两修复通天塔？"), self.m_InputNum), DelegateFactory.Action(function ()
                Gac2Gas.SubmitSilverToRepairTongTianTa(self.m_InputNum)
            end), nil, nil, nil, false)
        end
    end)

    self.m_TipBtn.OnClick = DelegateFactory.Action_QnButton(function (go)
        g_MessageMgr:ShowMessage("ZongMen_RepairTongTianTa_Tip")
    end)

end

--@region UIEvent

--@endregion UIEvent

function LuaRepairTongTianTaSubmitSilverWnd:OnEnable()
    g_ScriptEvent:AddListener("RefreshRepairTongTianTaSubmitSilverWnd", self, "RefreshInfo")
end

function LuaRepairTongTianTaSubmitSilverWnd:OnDisable()
    g_ScriptEvent:RemoveListener("RefreshRepairTongTianTaSubmitSilverWnd", self, "RefreshInfo")
end

function LuaRepairTongTianTaSubmitSilverWnd:RefreshInfo()
    self:UpdateInfo(true)
end

function LuaRepairTongTianTaSubmitSilverWnd:UpdateInfo(doAnim)
    if LuaZongMenMgr.m_RepairTongTianTaInfo == nil then
        CUIManager.CloseUI(CLuaUIResources.RepairTongTianTaSubmitSilverWnd)
        return
    end
    self.m_InputMax = LuaZongMenMgr.m_RepairTongTianTaInfo[1]
    self.m_TotalSubmitedSilver = LuaZongMenMgr.m_RepairTongTianTaInfo[3]
    self.m_NeedSilverToRepairTongTianTa = LuaZongMenMgr.m_RepairTongTianTaInfo[4]
    self.m_MaxInputLab.text = self.m_InputMax

    self:CheckSubmitToMax()
    self:UpdateProgress(doAnim)
    self:SetSubmitSilver(self.m_InputMax)
end

function LuaRepairTongTianTaSubmitSilverWnd:ShowInputWnd()
    CNumberKeyBoardMgr.ShowNumberKeyBoardWithRange(0, self.m_InputMax, self.m_InputNum, 100, DelegateFactory.Action_int(function (val) 
        self:SetSubmitSilver(val)
    end), DelegateFactory.Action_int(function (val) 
        self:SetSubmitSilver(val)
    end), self.m_NumLab, AlignType.Right, true)
end

function LuaRepairTongTianTaSubmitSilverWnd:SetSubmitSilver(silver)
    self.m_InputNum = silver
    self.m_NumLab.text = silver
    self.m_QnCostAndOwnMoney:SetCost(silver)
    self.m_AddPrestigeLab.text = SafeStringFormat3("+%d", self:GetAddPrestige(silver))
    self.m_AddProgress.text  = SafeStringFormat3("+%d", self:GetAddProgress(silver))
end

function LuaRepairTongTianTaSubmitSilverWnd:GetAddPrestige(silver)
    local formula = AllFormulas.Action_Formula[Menpai_EventSetting.GetData().SubmitSilver2ShengWangFormulaId].Formula
    if formula ~= nil then
        return formula(nil, nil, {silver})
    end
    return 0
end

function LuaRepairTongTianTaSubmitSilverWnd:GetAddProgress(silver)
    return silver
end
-- 个人捐献达到最大值
function LuaRepairTongTianTaSubmitSilverWnd:CheckSubmitToMax()
    if self.m_InputMax <= 0  and self.m_TotalSubmitedSilver < self.m_NeedSilverToRepairTongTianTa  then
        self.m_Input:SetActive(false)
        self.m_Effect:SetActive(false)
        self.m_SubmitToMaxLabel.text = LocalString.GetString("您捐献的银两已达最大值")
        self.m_SubmitToMaxLabel.gameObject:SetActive(true)
        self.m_OpBtn.Enabled = false
    else 
        self.m_Input:SetActive(true)
        self.m_Effect:SetActive(true)
        self.m_SubmitToMaxLabel.gameObject:SetActive(false)
        self.m_OpBtn.Enabled = true
    end
end

function LuaRepairTongTianTaSubmitSilverWnd:UpdateProgress(doAnim)
    local percent = self.m_TotalSubmitedSilver / self.m_NeedSilverToRepairTongTianTa
    if doAnim == true then
        self:DoAnim(percent)
    else 
        self.m_Slider.value = percent
    end
    self.m_ProgressLab.text = SafeStringFormat3("%d/%d", self.m_TotalSubmitedSilver, self.m_NeedSilverToRepairTongTianTa)
end

function LuaRepairTongTianTaSubmitSilverWnd:DoAnim(targetValue)
    self.m_Fx:DestroyFx()
    self.m_Fx:LoadFx("fx/ui/prefab/UI_liuxingyu_jindutiao01.prefab")
    
    if self.m_Tweener ~= nil then
        LuaTweenUtils.Kill(self.m_Tweener, false)
    end
    self.m_Tweener = LuaTweenUtils.TweenFloat(self.m_Slider.value, targetValue, 0.5, function (val)
		self.m_Slider.value = val
	end)

    CommonDefs.OnComplete_Tweener(self.m_Tweener, DelegateFactory.TweenCallback(function()
        self.m_Fx:DestroyFx()
        self.m_Tweener = nil
    end))
end

function LuaRepairTongTianTaSubmitSilverWnd:OnDestroy()
    LuaTweenUtils.Kill(self.m_Tweener, false)
    self.m_Fx:DestroyFx()

    LuaZongMenMgr.m_RepairTongTianTaInfo = nil
end