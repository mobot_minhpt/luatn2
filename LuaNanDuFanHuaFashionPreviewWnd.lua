local CFashionPreviewTextureLoader = import "L10.UI.CFashionPreviewTextureLoader"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local NGUITools = import "NGUITools"
local DelegateFactory = import "DelegateFactory"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local Extensions = import "Extensions"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local QnButton = import "L10.UI.QnButton"
local Vector3 = import "UnityEngine.Vector3"
local UISprite = import "UISprite"
local EnumShopMallFashionPreviewType = import "L10.UI.EnumShopMallFashionPreviewType"
local UIEventListener = import "UIEventListener"
local CItemMgr = import "L10.Game.CItemMgr"
local String = import "System.String"
local LocalString = import "LocalString"
local Money = import "L10.Game.Money"
local CUITexture = import "L10.UI.CUITexture"
local QnLabel = import "L10.UI.QnLabel"
local UIGrid = import "UIGrid"
local UIScrollView = import "UIScrollView"
local IdPartition = import "L10.Game.IdPartition"
local CItem = import "L10.Game.CItem"
local CEquipment = import "L10.Game.CEquipment"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local CUIManager = import "L10.UI.CUIManager"
local GestureRecognizer = import "L10.Engine.GestureRecognizer"
local GestureType = import "L10.Engine.GestureType"
local QnRadioBox = import "L10.UI.QnRadioBox"
local CFashionPreviewMgr = import "L10.UI.CFashionPreviewMgr"
local UITexture = import "UITexture"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local Shader = import "UnityEngine.Shader"
local CRenderObject = import "L10.Engine.CRenderObject"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local CEffectMgr = import "L10.Game.CEffectMgr"
local LayerDefine = import "L10.Engine.LayerDefine"
local QualitySettings = import "UnityEngine.QualitySettings"
local CMainCamera = import "L10.Engine.CMainCamera"
local Quaternion = import "UnityEngine.Quaternion"
local UIPanel = import "UIPanel"
local CCPlayerCtrl = import "L10.Game.CCPlayerCtrl"
local CWinCGMgr = import "L10.Game.CWinCGMgr"
local QnTableItem = import "L10.UI.QnTableItem"
local EnumGender = import "L10.Game.EnumGender"
local UIProgressBar = import "UIProgressBar"
local CClientObject = import "L10.Game.CClientObject"
local ShaderLodParams = import "L10.Game.ShaderLodParams"
local QnCheckBox = import "L10.UI.QnCheckBox"
local UILabel = import "UILabel"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaNanDuFanHuaFashionPreviewWnd = class()

RegistChildComponent(LuaNanDuFanHuaFashionPreviewWnd, "qnModelPreviewer", CFashionPreviewTextureLoader)
RegistChildComponent(LuaNanDuFanHuaFashionPreviewWnd, "tabButtons", GameObject)
RegistChildComponent(LuaNanDuFanHuaFashionPreviewWnd, "scrollViewBg", UITexture)
RegistChildComponent(LuaNanDuFanHuaFashionPreviewWnd, "scrollView", UIScrollView)
RegistChildComponent(LuaNanDuFanHuaFashionPreviewWnd, "buyButton", QnButton)
RegistChildComponent(LuaNanDuFanHuaFashionPreviewWnd, "pool", GameObject)
RegistChildComponent(LuaNanDuFanHuaFashionPreviewWnd, "grid", UIGrid)
RegistChildComponent(LuaNanDuFanHuaFashionPreviewWnd, "qnCostAndOwnMoney", CCurentMoneyCtrl)
RegistChildComponent(LuaNanDuFanHuaFashionPreviewWnd, "changeXianFanRadioBox", QnRadioBox)
RegistChildComponent(LuaNanDuFanHuaFashionPreviewWnd, "takeOffButton", QnButton)
RegistChildComponent(LuaNanDuFanHuaFashionPreviewWnd, "BuyArea", GameObject)
RegistChildComponent(LuaNanDuFanHuaFashionPreviewWnd, "BuySingletonShopGoodsButton", QnButton)
RegistChildComponent(LuaNanDuFanHuaFashionPreviewWnd, "OpenUrlButton", QnButton)
RegistChildComponent(LuaNanDuFanHuaFashionPreviewWnd, "m_DoExpressionButton", "DoExpressionButton",QnButton)
RegistChildComponent(LuaNanDuFanHuaFashionPreviewWnd, "m_ShopRoot", "ShopRoot", QnTableView)
RegistChildComponent(LuaNanDuFanHuaFashionPreviewWnd, "m_TransformButton", "TransformButton", QnButton)
RegistChildComponent(LuaNanDuFanHuaFashionPreviewWnd, "m_TransformHighlight", "highlight", GameObject)
RegistChildComponent(LuaNanDuFanHuaFashionPreviewWnd, "m_BlindBoxView", "BlindBoxView", GameObject)
RegistChildComponent(LuaNanDuFanHuaFashionPreviewWnd, "m_XianFanButton2", "XianFanButton2", QnSelectableButton)
RegistChildComponent(LuaNanDuFanHuaFashionPreviewWnd, "m_BlindBoxViewLastPageBtn", "BlindBoxViewLastPageBtn", GameObject)
RegistChildComponent(LuaNanDuFanHuaFashionPreviewWnd, "m_BlindBoxViewNextPageBtn", "BlindBoxViewNextPageBtn", GameObject)
RegistChildComponent(LuaNanDuFanHuaFashionPreviewWnd, "m_BlindBoxNameLabel", "BlindBoxNameLabel", UILabel)
RegistChildComponent(LuaNanDuFanHuaFashionPreviewWnd, "m_BlindBoxSpriteTemplate", "BlindBoxSpriteTemplate", GameObject)
RegistChildComponent(LuaNanDuFanHuaFashionPreviewWnd, "m_BlindBoxViewTable", "BlindBoxViewTable", UITable)
RegistChildComponent(LuaNanDuFanHuaFashionPreviewWnd, "headAlphaPreviewBtn", "headAlphaPreviewBtn", QnCheckBox)

RegistClassMember(LuaNanDuFanHuaFashionPreviewWnd, "curCategory")
RegistClassMember(LuaNanDuFanHuaFashionPreviewWnd, "mallInfo")
RegistClassMember(LuaNanDuFanHuaFashionPreviewWnd, "tabs")
RegistClassMember(LuaNanDuFanHuaFashionPreviewWnd, "isShrinkShopRoot")
RegistClassMember(LuaNanDuFanHuaFashionPreviewWnd, "curSelectableSequence")
RegistClassMember(LuaNanDuFanHuaFashionPreviewWnd, "curCostMoney")
RegistClassMember(LuaNanDuFanHuaFashionPreviewWnd, "itemID2ShopMallItemMap")
RegistClassMember(LuaNanDuFanHuaFashionPreviewWnd, "onPinchInDelegate")
RegistClassMember(LuaNanDuFanHuaFashionPreviewWnd, "onPinchOutDelegate")
RegistClassMember(LuaNanDuFanHuaFashionPreviewWnd, "isSupportFeiSheng")
RegistClassMember(LuaNanDuFanHuaFashionPreviewWnd, "localModelScale")
RegistClassMember(LuaNanDuFanHuaFashionPreviewWnd, "localModelPos")
RegistClassMember(LuaNanDuFanHuaFashionPreviewWnd, "defaultModelLocalPos")
RegistClassMember(LuaNanDuFanHuaFashionPreviewWnd, "maxPreviewScale")
RegistClassMember(LuaNanDuFanHuaFashionPreviewWnd, "maxPreviewOffsetY")
RegistClassMember(LuaNanDuFanHuaFashionPreviewWnd, "curSelectableFashionID")
RegistClassMember(LuaNanDuFanHuaFashionPreviewWnd, "babyFashionMallDict")
RegistClassMember(LuaNanDuFanHuaFashionPreviewWnd, "bfirstInit")
RegistClassMember(LuaNanDuFanHuaFashionPreviewWnd, "shadowCamera")
RegistClassMember(LuaNanDuFanHuaFashionPreviewWnd, "shadowCameraRelativePos")
RegistClassMember(LuaNanDuFanHuaFashionPreviewWnd, "lightAndFloorAndWall")
RegistClassMember(LuaNanDuFanHuaFashionPreviewWnd, "lightAndFloorAndWallRelativePos")
RegistClassMember(LuaNanDuFanHuaFashionPreviewWnd, "lastpixelLightCount")
RegistClassMember(LuaNanDuFanHuaFashionPreviewWnd, "floor")
RegistClassMember(LuaNanDuFanHuaFashionPreviewWnd, "beiguangLight")
RegistClassMember(LuaNanDuFanHuaFashionPreviewWnd, "floorInitialLocalPos")
RegistClassMember(LuaNanDuFanHuaFashionPreviewWnd, "modelRoot")
RegistClassMember(LuaNanDuFanHuaFashionPreviewWnd, "m_CgName")
RegistClassMember(LuaNanDuFanHuaFashionPreviewWnd, "m_OwnedShopItems") --拥有的所有道具
RegistClassMember(LuaNanDuFanHuaFashionPreviewWnd, "m_ItemID2IsPreview")
RegistClassMember(LuaNanDuFanHuaFashionPreviewWnd, "transformHighlight")
RegistClassMember(LuaNanDuFanHuaFashionPreviewWnd, "m_BlindBoxResultItemId")
RegistClassMember(LuaNanDuFanHuaFashionPreviewWnd, "m_EmptyLabel")

function LuaNanDuFanHuaFashionPreviewWnd:Init()
    self.passLevel = 0
    self.fashionData = {}
    if CClientMainPlayer.Inst and CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, LuaYiRenSiMgr.LordTempPlayDataKey) then
        local playData = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, LuaYiRenSiMgr.LordTempPlayDataKey)
        if playData.ExpiredTime > CServerTimeMgr.Inst.timeStamp and playData.Data and playData.Data.Data then
            local data = g_MessagePack.unpack(playData.Data.Data)
            self.fashionData = data[5]
        end
    end
    
    self.gameObject:GetComponent(typeof(UIPanel)).IgnoreIphoneXMargin = true
    self:InitData()
    self.localModelScale = 1
    self.curCategory = LuaShopMallFashionPreviewMgr.EnumLingYuMallSubCategory.ShiZhuang

    self.itemID2ShopMallItemMap = {}
    self.curSelectableSequence = {}
    self.m_ItemID2IsPreview = {}
    self.curCostMoney = 0
    self:LoadMallInfo()
    self.m_BlindBoxSpriteTemplate:SetActive(false)
    UIEventListener.Get(self.qnModelPreviewer.resetButton).onClick = CommonDefs.CombineListner_VoidDelegate(
                UIEventListener.Get(self.qnModelPreviewer.resetButton).onClick, 
                DelegateFactory.VoidDelegate(
                    function()
                        self:OnRestButtonClick()
                    end), 
                true
        )
    self.takeOffButton.OnClick = DelegateFactory.Action_QnButton(
        function(btn)
            self:OnTakeOffButtonClick()
        end
    )
    self.OpenUrlButton.gameObject:SetActive(false)
    self.OpenUrlButton.OnClick = DelegateFactory.Action_QnButton(
        function(btn)
            self:OpenUrl()
        end
    )
    self.m_TransformButton.OnClick = DelegateFactory.Action_QnButton(
        function(btn)
            self:Transform()
        end
    )
    self.m_DoExpressionButton.OnClick = DelegateFactory.Action_QnButton(
        function(btn)
            self:DoExpression()
        end
    )
    self.headAlphaPreviewBtn.OnValueChanged = DelegateFactory.Action_bool(
        function(val)
            self:ProcessSetHeadAlpha(val)
        end
    )
    CFashionPreviewMgr.curShopMallPreviewType = EnumShopMallFashionPreviewType.PreviewAllFashion
    self:InitModelPreviewer()
    self:InitHeadAlpha()
    self:InitRadioBox()
    self:InitTabButtons()
    self:InitShopRoot()
    self:InitSellRoot()
    self:InitTransformButton()
    self:InitDoExpressionButton()
    self.qnCostAndOwnMoney:SetType(EnumMoneyType.NanDuTongBao)
    Gac2Gas.RequestDaFuWengPlayData()
end

function LuaNanDuFanHuaFashionPreviewWnd:InitHeadAlpha()
    local haveha = false
    if CClientMainPlayer.Inst then
        haveha = LuaFashionMgr.FashionHaveHeadAlpha(CFashionPreviewMgr.HeadFashionID, CClientMainPlayer.Inst.Gender)
    end
    self.headAlphaPreviewBtn.gameObject:SetActive(haveha)
end

function LuaNanDuFanHuaFashionPreviewWnd:ProcessSetHeadAlpha(val)
    local appearData = self.qnModelPreviewer.needShowAppearance
    appearData.HideTouSha = val and 1 or 0
    if val then
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("你已关闭头部时装的头饰显示"))
    else
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("你已开启头部时装的头饰显示"))
    end
    self:InitModelPreviewer()
end

function LuaNanDuFanHuaFashionPreviewWnd:OpenUrl()
    if self.m_CgName then
        if not CCPlayerCtrl.IsSupportted() then
            g_MessageMgr:ShowMessage("CUSTOM_STRING1", LocalString.GetString("客户端引擎版本过旧，不能播放此视频，请更新版本后再试"))
            return
        end
        local url = CommonDefs.IS_PUB_RELEASE and "http://l10.gph.netease.com/fashionpreviewvideo/" or "http://l10-patch.leihuo.netease.com/fashionpreviewvideo/"
        CWinCGMgr.Inst.m_CGUrl = url .. self.m_CgName
        CUIManager.ShowUI(CUIResources.WinCGWnd)
    end
end

--调整了一下纹理的大小或位置后在不改策划表的前提下，用以修正模型的位置
function LuaNanDuFanHuaFashionPreviewWnd:FixPos(pos)
    return pos.x + 0.56, pos.y, pos.z / 2
end

function LuaNanDuFanHuaFashionPreviewWnd:InitData()
    local s = Fashion_Setting.GetData("FashionPreview_DefaultModelLocalPos").Value
    local function split(str, reps)
        local resultStrList = {}
        string.gsub(
                str,
                "[^" .. reps .. "]+",
                function(w)
                    table.insert(resultStrList, w)
                end
        )
        return resultStrList[1], resultStrList[2], resultStrList[3]
    end
    self.defaultModelLocalPos = Vector3.zero
    self.defaultModelLocalPos.x, self.defaultModelLocalPos.y, self.defaultModelLocalPos.z = split(s, ",")
    self.defaultModelLocalPos.x, self.defaultModelLocalPos.y, self.defaultModelLocalPos.z = LuaNanDuFanHuaFashionPreviewWnd:FixPos(self.defaultModelLocalPos)
    if CClientMainPlayer.Inst then
        Fashion_FashionPreview.Foreach(
                function(key, data)
                    local class = CommonDefs.Convert_ToUInt32(CClientMainPlayer.Inst.Class)
                    local gender = CommonDefs.Convert_ToUInt32(CClientMainPlayer.Inst.Gender)
                    if data.Class == class and data.Gender == gender then
                        self.maxPreviewScale = data.MaxPreviewScale
                        self.maxPreviewOffsetY = data.MaxPreviewOffsetY
                    end
                end
        )
    end
end

function LuaNanDuFanHuaFashionPreviewWnd:OnEnable()
    self.onPinchInDelegate =
    DelegateFactory.Action_GenericGesture(
            function(gesture)
                self:OnPinchIn(gesture)
            end
    )
    self.onPinchOutDelegate =
    DelegateFactory.Action_GenericGesture(
            function(gesture)
                self:OnPinchOut(gesture)
            end
    )
    GestureRecognizer.AddGestureListener(GestureType.OnUIPinchIn, self.onPinchInDelegate)
    GestureRecognizer.AddGestureListener(GestureType.OnUIPinchOut, self.onPinchOutDelegate)
    g_ScriptEvent:AddListener("MouseScrollWheel", self, "OnMouseScrollWheel")
    g_ScriptEvent:AddListener("SetItemAt", self, "OnSetItemAt")
    g_ScriptEvent:AddListener("OnSyncRepertoryForMallPreviewDone", self, "OnSyncRepertoryForMallPreviewDone")
    if LuaShopMallFashionPreviewMgr.enableMultiLight then
        self.lastpixelLightCount = 0
        QualitySettings.pixelLightCount = 3
        if Shader.globalMaximumLOD == ShaderLodParams.SHADER_LOD_HIGH_LEVEL then
            Shader.globalMaximumLOD = ShaderLodParams.SHADER_LOD_MULTILIGHT
        end
    end
    g_ScriptEvent:AddListener("UpdateTempPlayDataWithKey", self, "OnUpdatePlayProp")
    g_ScriptEvent:AddListener("OnTongXingZhengDataUpdate", self, "OnTongXingZhengDataUpdate")
end

function LuaNanDuFanHuaFashionPreviewWnd:OnDisable()
    GestureRecognizer.RemoveGestureListener(GestureType.OnUIPinchIn, self.onPinchInDelegate)
    GestureRecognizer.RemoveGestureListener(GestureType.OnUIPinchOut, self.onPinchOutDelegate)
    self.onPinchInDelegate = nil
    self.onPinchOutDelegate = nil
    g_ScriptEvent:RemoveListener("MouseScrollWheel", self, "OnMouseScrollWheel")
    g_ScriptEvent:RemoveListener("SetItemAt", self, "OnSetItemAt")
    g_ScriptEvent:RemoveListener("OnSyncRepertoryForMallPreviewDone", self, "OnSyncRepertoryForMallPreviewDone")
    LuaShopMallFashionPreviewMgr:OnClose()
    if LuaShopMallFashionPreviewMgr.enableMultiLight and self.lastpixelLightCount then
        QualitySettings.pixelLightCount = self.lastpixelLightCount
        if Shader.globalMaximumLOD == ShaderLodParams.SHADER_LOD_MULTILIGHT then
            Shader.globalMaximumLOD = ShaderLodParams.SHADER_LOD_HIGH_LEVEL
        end
    end
    local progress = self.m_DoExpressionButton.transform:Find("Progress"):GetComponent(typeof(UIProgressBar))
    LuaTweenUtils.DOKill(progress.transform, false)
    g_ScriptEvent:RemoveListener("UpdateTempPlayDataWithKey", self, "OnUpdatePlayProp")
    g_ScriptEvent:RemoveListener("OnTongXingZhengDataUpdate", self, "OnTongXingZhengDataUpdate")
end

function LuaNanDuFanHuaFashionPreviewWnd:OnTongXingZhengDataUpdate()
    local res = LuaDaFuWongMgr:GetTongXingZhengDataInfo()
    if res then 
        self.passLevel = res.lv
    else
        self.passLevel = 0
    end
end

function LuaNanDuFanHuaFashionPreviewWnd:OnUpdatePlayProp()
    self.fashionData = {}
    if CClientMainPlayer.Inst and CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, LuaYiRenSiMgr.LordTempPlayDataKey) then
        local playData = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, LuaYiRenSiMgr.LordTempPlayDataKey)
        if playData.ExpiredTime > CServerTimeMgr.Inst.timeStamp and playData.Data and playData.Data.Data then
            local data = g_MessagePack.unpack(playData.Data.Data)
            self.fashionData = data[5]
        end
    end
    
    self.curCostMoney = 0
    self.qnCostAndOwnMoney:SetType(EnumMoneyType.NanDuTongBao)
    self.qnCostAndOwnMoney:SetCost(self.curCostMoney)

    self.m_ShopRoot:ReloadData(true, true)
end

function LuaNanDuFanHuaFashionPreviewWnd:OnSetItemAt(args)
    local place, position, oldItemId, newItemId = args[0], args[1], args[2], args[3]
    local item = CItemMgr.Inst:GetById(newItemId)
    if item then
        local templateId = item.TemplateId
        self.m_OwnedShopItems = LuaShopMallFashionPreviewMgr:GetOwnedShopItemsInfo()

        for i = 0, self.m_ShopRoot.m_ItemList.Count - 1 do
            local item = self.m_ShopRoot.m_ItemList[i]
            local mall = self.mallInfo[item.Row + 1]
            local id = mall.ItemId
            local isOwned = self.m_OwnedShopItems[id]
            if id == templateId then
                local ownerSprite = item.transform:Find("Owner").gameObject
                ownerSprite:SetActive(isOwned)
            end
        end
    end
end

function LuaNanDuFanHuaFashionPreviewWnd:ClearShopMallFashionPreview()
    CFashionPreviewMgr.HeadFashionID = 0
    CFashionPreviewMgr.BodyFashionID = 0
    CFashionPreviewMgr.ZuoQiFashionID = 0
    CFashionPreviewMgr.UmbrellaFashionID = 0
end

function LuaNanDuFanHuaFashionPreviewWnd:FixInitModelPreviewer()
    --待坐骑和角色全部加载完后才显示角色以修复短暂的卡在地上的现象
    self.qnModelPreviewer:LoadModel()
    self.qnModelPreviewer.m_RO.Visible = false
    self.qnModelPreviewer.m_RO:AddLoadAllFinishedListener(
            DelegateFactory.Action_CRenderObject(
                    function(ro)
                        if ro.VehicleRO then
                            ro.VehicleRO:AddLoadAllFinishedListener(
                                    DelegateFactory.Action_CRenderObject(
                                            function(_ro)
                                                self.qnModelPreviewer.m_RO.Visible = true
                                                if CFashionPreviewMgr.UmbrellaFashionID > 0 then
                                                    local gender =  CClientMainPlayer.Inst and EnumToInt(CClientMainPlayer.Inst.AppearanceProp.Gender) or 0
                                                    CClientObject.ShowExpressionAction(self.qnModelPreviewer.m_RO, CFashionPreviewMgr.UmbrellaFashionID,  gender)
                                                end
                                            end
                                    )
                            )
                        else
                            self.qnModelPreviewer.m_RO.Visible = true
                        end
                    end
            )
    )
end

function LuaNanDuFanHuaFashionPreviewWnd:InitModelPreviewer(isNeedResetModelScale)
    LuaShopMallFashionPreviewMgr:InitForLuozhuangSetting()
    if not self.bfirstInit then
        self.qnModelPreviewer:Init(false)
    else
        self:FixInitModelPreviewer()
    end

    LuaModelTextureLoaderMgr:SetModelCameraColor(self.qnModelPreviewer.identifier)

    if isNeedResetModelScale then
        self.localModelScale = 1
    end

    local zuoqi = ZuoQi_ZuoQi.GetData(CFashionPreviewMgr.ZuoQiFashionID)
    local d = (zuoqi and zuoqi.ShopMallMaxPreviewScale or self.maxPreviewScale) - 1
    local t = (self.localModelScale - 1) / d
    t = math.max(math.min(1, t), 0)
    local y = math.lerp(0, zuoqi and zuoqi.ShopMallMaxPreviewYOffSet or self.maxPreviewOffsetY, t)
    local pos = Vector3(self.defaultModelLocalPos.x, self.defaultModelLocalPos.y + y, self.defaultModelLocalPos.z)

    self:SetModelPosAndScale(pos, self.localModelScale)
    if not self.bfirstInit then
        self:InitShadowCameraAndLight()
    end

    if CFashionPreviewMgr.ZuoQiFashionID ~= 0 then
        local zuoqi = ZuoQi_ZuoQi.GetData(CFashionPreviewMgr.ZuoQiFashionID)
        if zuoqi then
            self.qnModelPreviewer.m_RO:AddLoadAllFinishedListener(
                    DelegateFactory.Action_CRenderObject(
                            function(ro)
                                if ro.VehicleRO then
                                    ro.VehicleRO:AddLoadAllFinishedListener(
                                            DelegateFactory.Action_CRenderObject(
                                                    function(_ro)
                                                        _ro.transform.localRotation = Quaternion.Euler(zuoqi.ShopMallMaxPreviewRX, zuoqi.ShopMallMaxPreviewRY, 0)
                                                    end
                                            )
                                    )
                                end
                            end
                    )
            )
            if self.beiguangLight then
                self.beiguangLight.gameObject:SetActive(zuoqi.DisableShopMallBeiGuang == 0)
            end
            if self.floor then
                self.floor.transform.localPosition = Vector3(self.floorInitialLocalPos.x, self.floorInitialLocalPos.y + zuoqi.ShopMallFloorHeight, self.floorInitialLocalPos.z)
            end
        end
    else
        if self.floor then
            self.floor.transform.localPosition = self.floorInitialLocalPos
        end
        if self.beiguangLight then
            self.beiguangLight.gameObject:SetActive(true)
        end
    end

    if not self.bfirstInit then
        self.bfirstInit = true
    end
end

function LuaNanDuFanHuaFashionPreviewWnd:SetModelPosAndScale(pos, scale)
    self.localModelScale = scale
    if scale > 0 then
        pos = Vector3(pos.x / scale, pos.y / scale, pos.z / scale)
    end
    self.localModelPos = pos
    local PreviewScale = 1
    local zuoqi = ZuoQi_ZuoQi.GetData(CFashionPreviewMgr.ZuoQiFashionID)
    if zuoqi then
        PreviewScale = zuoqi.PreviewScale
    end
    CUIManager.SetModelScale(self.qnModelPreviewer.identifier, Vector3(PreviewScale, PreviewScale, PreviewScale))
    CUIManager.SetModelPosition(self.qnModelPreviewer.identifier, pos)
    if not LuaShopMallFashionPreviewMgr.enableMultiLight then
        return
    end
    self:SetShadowCameraAndLightPos(pos)
    CMainCamera.Inst:OnPreRenderShadowmap()
end

function LuaNanDuFanHuaFashionPreviewWnd:InitRadioBox()
    local appearData = self.qnModelPreviewer.needShowAppearance
    self.isSupportFeiSheng = CClientMainPlayer.IsFashionSupportFeiSheng(appearData.HeadFashionId, appearData.BodyFashionId, appearData.HideHeadFashionEffect, appearData.HideBodyFashionEffect)
    self.changeXianFanRadioBox:Awake()
    self.changeXianFanRadioBox.OnSelect =
    DelegateFactory.Action_QnButton_int(
            function(btn, index)
                self.m_XianFanButton2:SetSelected(index == 1, false)
            end
    )
    if CClientMainPlayer.Inst then
        self.changeXianFanRadioBox.Visible = CClientMainPlayer.Inst.HasFeiSheng
        self.m_XianFanButton2.Visible = CClientMainPlayer.Inst.HasFeiSheng
    end
    local isXianShen = appearData.FeiShengAppearanceXianFanStatus > 0 and self.isSupportFeiSheng
    self.changeXianFanRadioBox:ChangeTo(isXianShen and 1 or 0, false)
    self.m_XianFanButton2:SetSelected(isXianShen, false)
    self.m_XianFanButton2.OnButtonSelected =
    DelegateFactory.Action_bool(
            function(b)
                self.changeXianFanRadioBox:ChangeTo(b and 1 or 0, false)
                self:SetXianFan(b, appearData)
            end
    )
end

function LuaNanDuFanHuaFashionPreviewWnd:SetXianFan(isXianShen, appearData)
    if isXianShen then
        if self.isSupportFeiSheng then
            appearData.FeiShengAppearanceXianFanStatus = 1
        else
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("穿戴凡身拓本时无法预览仙身外观"))
            self.changeXianFanRadioBox:ChangeTo(0, false)
        end
    elseif CClientMainPlayer.Inst then
        if CClientMainPlayer.Inst.Is150GhostBody or CClientMainPlayer.Inst.Is150GhostHead then
            g_MessageMgr:ShowMessage("Tip_FanShen_Ghost150")
        end
        appearData.FeiShengAppearanceXianFanStatus = 0
    end
    if self.m_DoExpressionButton.gameObject.activeSelf then
        self:DoExpression()
    else
        self:InitModelPreviewer()
    end
end

function LuaNanDuFanHuaFashionPreviewWnd:InitTabButtons()
    local cats = LuaShopMallFashionPreviewMgr.EnumLingYuMallSubCategory
    local ids = {
        cats.ZuoQi,
        cats.ShiZhuang,
    }

    self.tabs = {}
    for i = 1, #ids do
        local category = ids[i]
        --local data = Mall_LingYuMallSubCategory.GetData(category)
        local obj = self.tabButtons.transform:Find("Tab" .. i).gameObject
        local btn = obj:GetComponent(typeof(QnSelectableButton))
        table.insert(self.tabs, btn)
        --btn.m_BackGround.mSpriteName = data.SpriteName
        local t = category
        --btn.m_BackGround.alpha = 0.5
        btn.OnClick =
        DelegateFactory.Action_QnButton(
                function(btn)
                    self:OnTabButtonClick(t)
                    for _, _btn in pairs(self.tabs) do
                        if btn ~= _btn then
                            _btn:SetSelected(false, false)
                            --_btn.m_BackGround.alpha = 0.5
                        end
                    end
                    btn:SetSelected(true, false)
                    --btn.m_BackGround.alpha = 1
                end
        )
        btn:SetSelected(category == self.curCategory, false)
        --btn.m_BackGround.alpha = category == self.curCategory and 1 or 0.5
        obj:SetActive(true)
    end
end

function LuaNanDuFanHuaFashionPreviewWnd:InitShopRoot()
    self.m_EmptyLabel = FindChildWithType(self.m_ShopRoot.transform, "EmptyLabel", typeof(UILabel))
    self.m_ShopRoot.m_DataSource =
    DefaultTableViewDataSource.Create2(
            function()
                return self:NumberOfRows()
            end,
            function(item, index)
                return self:ItemAt(nil, index)
            end
    )
    local callback =
    DelegateFactory.Action_int(
            function(row)
                self:OnSelectAtRow(row)
            end
    )
    self.m_ShopRoot.OnSelectAtRow = callback
    self:Reload()
    self.isShrinkShopRoot = false
end

function LuaNanDuFanHuaFashionPreviewWnd:InitSellRoot()
    self.buyButton.OnClick =
    DelegateFactory.Action_QnButton(
            function(btn)
                self:OnBuyButtonClick()
            end
    )
    self.BuySingletonShopGoodsButton.OnClick =
    DelegateFactory.Action_QnButton(
            function(btn)
                self:BuySingletonShopGoods()
            end
    )
end

function LuaNanDuFanHuaFashionPreviewWnd:OnTabButtonClick(category)
    self.curCategory = category
    LuaShopMallFashionPreviewMgr.blindBoxItemId = 0
    if self.curCategory == LuaShopMallFashionPreviewMgr.EnumLingYuMallSubCategory.ShiZhuang then
        if LuaShopMallFashionPreviewMgr.headfashionBlindBoxItemId ~= 0 then
            LuaShopMallFashionPreviewMgr.blindBoxItemId = LuaShopMallFashionPreviewMgr.headfashionBlindBoxItemId
        elseif LuaShopMallFashionPreviewMgr.bodyfashionBlindBoxItemId ~= 0 then
            LuaShopMallFashionPreviewMgr.blindBoxItemId = LuaShopMallFashionPreviewMgr.bodyfashionBlindBoxItemId
        end
        LuaShopMallFashionPreviewMgr.blindBoxSelectIndex = LuaShopMallFashionPreviewMgr.fashionBlindBoxSelectIndex
    elseif self.curCategory == LuaShopMallFashionPreviewMgr.EnumLingYuMallSubCategory.ZuoQi then
        LuaShopMallFashionPreviewMgr.blindBoxItemId = LuaShopMallFashionPreviewMgr.zuoqiBlindBoxItemId
        LuaShopMallFashionPreviewMgr.blindBoxSelectIndex = LuaShopMallFashionPreviewMgr.zuoqiBlindBoxSelectIndex
    end
    self:UpdateCostMoney()
    self:Reload()
end

function LuaNanDuFanHuaFashionPreviewWnd:OnBuyButtonClick()
    
    --购买预览界面
    local isFailOwnCheck = true
    local haveContent = false
    local combineNameStr = ""
    for k, v in pairs( self.curSelectableSequence) do
        haveContent = true
        local isOwned = false
        local cfgId = v.ID
        if v.FashionId then
            --时装类
            isOwned = bit.band(self.fashionData[4] and self.fashionData[4] or 0, bit.lshift(1, cfgId - 1)) > 0
            local fashionCfgData = NanDuFanHua_LordFashion.GetData(cfgId)
            local itemCfg = CItemMgr.Inst:GetItemTemplate(v.ItemId)
            if combineNameStr == "" then
                combineNameStr = combineNameStr .. itemCfg.Name
            else
                combineNameStr = combineNameStr .. "," .. itemCfg.Name
            end
            if (self.passLevel < fashionCfgData.Pass) then
                --有通行证等级不满足的, 不继续往下了
                g_MessageMgr:ShowMessage("NANDULORD_BUY_FASHION_HONOUR_NOT_ENOUGH", tostring(fashionCfgData.Pass), itemCfg.Name)
                return
            end
            isFailOwnCheck = isFailOwnCheck and isOwned
        elseif v.ZuoQiId then
            isOwned = bit.band(self.fashionData[5] and self.fashionData[5] or 0, bit.lshift(1, cfgId - 1)) > 0
            local zuoqiCfgData = NanDuFanHua_LordZuoQi.GetData(cfgId)
            local itemCfg = CItemMgr.Inst:GetItemTemplate(v.ItemId)
            if combineNameStr == "" then
                combineNameStr = combineNameStr .. itemCfg.Name
            else
                combineNameStr = combineNameStr .. "," .. itemCfg.Name
            end
            if (self.passLevel < zuoqiCfgData.Pass) then
                --有通行证等级不满足的, 不继续往下了
                g_MessageMgr:ShowMessage("NANDULORD_BUY_FASHION_HONOUR_NOT_ENOUGH", tostring(zuoqiCfgData.Pass), itemCfg.Name)
                return
            end
            isFailOwnCheck = isFailOwnCheck and isOwned
        end
    end

    if not haveContent then
        --无内容时, 需要弹另外一条msg
        g_MessageMgr:ShowMessage("MENGHUALU_BUY_ZERO_ITEM")
        return
    end

    if isFailOwnCheck then
        --全拥有, 弹一个msg 不继续往下了 NANDULORD_ALREADY_UNLOCK_FASHION
        g_MessageMgr:ShowMessage("NANDULORD_ALREADY_UNLOCK_FASHION", combineNameStr)
        return
    end
    
    local costMoney = 0
    for itemID, shopMallTemlate in pairs(self.curSelectableSequence) do
        costMoney = costMoney + shopMallTemlate.Price
    end
    if costMoney > 0 then
        --LuaShopMallFashionPreviewMgr:ShowTryToBuyWnd(self.curSelectableSequence)
        LuaShopMallFashionPreviewMgr.selectableSequence = self.curSelectableSequence
        CUIManager.ShowUI(CLuaUIResources.NanDuFanHuaFashionPreviewTryToBuyWnd)
    end
end

function LuaNanDuFanHuaFashionPreviewWnd:LoadMallInfo()
    --local list = CShopMallMgr.GetLingYuMallInfo(3, self.curCategory)
    local list = {}
    if self.curCategory == LuaShopMallFashionPreviewMgr.EnumLingYuMallSubCategory.ShiZhuang then
        NanDuFanHua_LordFashion.Foreach(function(k, v)
            table.insert(list, NanDuFanHua_LordFashion.GetData(k))
        end)
    elseif self.curCategory == LuaShopMallFashionPreviewMgr.EnumLingYuMallSubCategory.ZuoQi then
        NanDuFanHua_LordZuoQi.Foreach(function(k, v)
            table.insert(list, NanDuFanHua_LordZuoQi.GetData(k))
        end)
    end
    
    local babyFashionMallDict = {}
    Baby_Fashion.ForeachKey(
        function(key)
            local fashion = Baby_Fashion.GetData(key)
            babyFashionMallDict[fashion.ItemID] = true
        end
    )
    self.mallInfo = {}
    for i = 1, #list do
        local template = list[i]
        local itemId = nil
        if self.curCategory == LuaShopMallFashionPreviewMgr.EnumLingYuMallSubCategory.ShiZhuang then
            itemId = Fashion_Fashion.GetData(template.FashionId).ItemID
        elseif self.curCategory == LuaShopMallFashionPreviewMgr.EnumLingYuMallSubCategory.ZuoQi then
            itemId = ZuoQi_ZuoQi.GetData(template.ZuoQiId).ItemID
        end
        
        if not babyFashionMallDict[itemId] then
            table.insert(self.mallInfo, template)
            self.mallInfo[i].ItemId = itemId
        end
    end
    for i = 1, #self.mallInfo do
        self.itemID2ShopMallItemMap[self.mallInfo[i].ItemId] = self.mallInfo[i]
    end
end

function LuaNanDuFanHuaFashionPreviewWnd:Reload()
    self:LoadMallInfo()
    self.m_OwnedShopItems = LuaShopMallFashionPreviewMgr:GetOwnedShopItemsInfo()
    self.m_ShopRoot:ReloadData(true, true)
    self.scrollView:ResetPosition()
    self:UpdateCurSelectableSequence()
    local ct = self:NumberOfRows()
    if self.m_EmptyLabel then
        self.m_EmptyLabel.gameObject:SetActive(ct <= 0)
        self.m_EmptyLabel.text = LocalString.GetString("当前没有可以预览的物品")
    end
end

function LuaNanDuFanHuaFashionPreviewWnd:NumberOfRows()
    return self.mallInfo and #self.mallInfo or 0
end

function LuaNanDuFanHuaFashionPreviewWnd:ItemAt(view, row)
    if not self.mallInfo then
        return
    end
    local template = self.mallInfo[row + 1]
    local item = self.m_ShopRoot:GetFromPool(self.isShrinkShopRoot and 1 or 0)
    self:InitItem(item.transform, template)
    return item
end

function LuaNanDuFanHuaFashionPreviewWnd:InitItem(transform, template)
    local cfgId = template.ID
    local isOwned = false
    local itemCfg = Item_Item.GetData(template.ItemId)
    local templateId = template.ItemId

    -- -1是坐骑的意思, 0-3是fashion表里的 position
    local templateType = nil

    if template.FashionId then
        --时装类
        isOwned = bit.band(self.fashionData[4] and self.fashionData[4] or 0, bit.lshift(1, cfgId - 1)) > 0
        templateType = Fashion_Fashion.GetData(template.FashionId).Position + 1
    elseif template.ZuoQiId then
        isOwned = bit.band(self.fashionData[5] and self.fashionData[5] or 0, bit.lshift(1, cfgId - 1)) > 0
        templateType = 3
    end

    local price = template.Price
    local discountInfo
    local isPreview = self.m_ItemID2IsPreview[templateId]
    local previewTag = transform:Find("PreviewTag"):GetComponent(typeof(UISprite))
    local transformableIcon = transform:Find("Transformable").gameObject

    previewTag.gameObject:SetActive(isPreview)
    
    --transform:Find("Sprite"):GetComponent(typeof(UISprite)).spriteName = Money.GetIconName(EnumMoneyType.LingYu, EnumPlayScoreKey.NONE)
    local texture = transform:Find("Texture"):GetComponent(typeof(CUITexture))

    texture:LoadMaterial(itemCfg.Icon)
    local nameLabel = transform:Find("Name")
    if nameLabel then
        nameLabel:GetComponent(typeof(QnLabel)).Text = itemCfg.Name
    end
    local priceLabel = transform:Find("Price")
    if priceLabel then
        priceLabel:GetComponent(typeof(QnLabel)).Text = tostring(price)
    end
    local discountLabel = transform:Find("Discount"):GetComponent(typeof(QnLabel))
    if not String.IsNullOrEmpty(discountInfo) then
        discountLabel.Text = discountInfo
        discountLabel.gameObject:SetActive(true)
    else
        discountLabel.gameObject:SetActive(false)
    end
    local disableSprite = transform:Find("Texture/DisableSprite"):GetComponent(typeof(UISprite))
    disableSprite.enabled = false
    if IdPartition.IdIsItem(templateId) then
        disableSprite.enabled = not CItem.GetMainPlayerIsFit(templateId, true)
    elseif IdPartition.IdIsEquip(templateId) then
        disableSprite.enabled = not CEquipment.GetMainPlayerIsFit(templateId, true)
    end
    local ownerSprite = transform:Find("WearOnSetting").gameObject
    ownerSprite:SetActive(isOwned)
    priceLabel.gameObject:SetActive(not isOwned)
    transform.gameObject:SetActive(true)

    
    local btn = ownerSprite.transform:GetComponent(typeof(QnSelectableButton))
    btn.OnClick = DelegateFactory.Action_QnButton(function(btn)
        self:OnWearOnSettingButtonClick(template, btn:isSeleted())
    end)
    btn:SetSelected((self.fashionData[templateType] and self.fashionData[templateType] or 0) == cfgId, false)

    transformableIcon:SetActive(CLuaShopMallMgr:IsShowTransformableFashionIcon(templateId))
end

function LuaNanDuFanHuaFashionPreviewWnd:OnWearOnSettingButtonClick(template, isOn)
    local clickCfgId = template.ID
     -- -1是坐骑的意思, 0-3是fashion表里的 position
    local clickType = nil
    if template.FashionId then
        clickType = Fashion_Fashion.GetData(template.FashionId).Position
    else
        clickType = -1
    end

    for i = 0, self.m_ShopRoot.m_ItemList.Count - 1 do
        local item = self.m_ShopRoot.m_ItemList[i]
        local curTemplate = self.mallInfo[item.Row + 1]
        local id = curTemplate.ItemId
        local cfgId = curTemplate.ID
        local isOwned = false
        if curTemplate.FashionId then
            --时装类
            isOwned = bit.band(self.fashionData[4] and self.fashionData[4] or 0, bit.lshift(1, cfgId - 1)) > 0
            local type = Fashion_Fashion.GetData(curTemplate.FashionId).Position
            if type == clickType and template.ID ~= cfgId then
                local btn = item.transform:Find("WearOnSetting"):GetComponent(typeof(QnSelectableButton))
                btn:SetSelected(false, false)
            end
        elseif curTemplate.ZuoQiId then
            isOwned = bit.band(self.fashionData[5] and self.fashionData[5] or 0, bit.lshift(1, cfgId - 1)) > 0
            if template.ID ~= cfgId then
                local btn = item.transform:Find("WearOnSetting"):GetComponent(typeof(QnSelectableButton))
                btn:SetSelected(false, false)
            end
        end
    end

    if isOn then
        if template.FashionId then
            Gac2Gas.TakeOnNanDuLordFashion(clickCfgId)
        else
            Gac2Gas.RideOnNanDuLordZuoQi(clickCfgId)
        end
    else
        if template.FashionId then
            Gac2Gas.TakeOffNanDuLordFashion(clickType)
        else
            Gac2Gas.RideOffNanDuLordZuoQi()
        end
    end
end

function LuaNanDuFanHuaFashionPreviewWnd:RefreshAllOwnerItemInfo()
    --self:Reload()
    self.m_OwnedShopItems = LuaShopMallFashionPreviewMgr:GetOwnedShopItemsInfo()

    for i = 0, self.m_ShopRoot.m_ItemList.Count - 1 do
        local item = self.m_ShopRoot.m_ItemList[i]
        local mall = self.mallInfo[item.Row + 1]
        local id = mall.ItemId
        local isOwned = self.m_OwnedShopItems[id]
        local ownerSprite = item.transform:Find("Owner").gameObject
        ownerSprite:SetActive(isOwned)
    end
end

function LuaNanDuFanHuaFashionPreviewWnd:OnSelectAtRow(row)
    local template = self.mallInfo[row + 1]
    self:OnItemClick(nil, template)
    self:UpdateCurSelectableSequence()
    self:InitTransformButton()
    self:InitDoExpressionButton()
    self:InitHeadAlpha()
end

function LuaNanDuFanHuaFashionPreviewWnd:OnItemClick(btn, template)
    local templateId = template.ItemId
    if template.FashionId then
        --时装类
        self:PreviewFashion(template.FashionId)
    elseif template.ZuoQiId then
        self:PreviewZuoQi(template.ZuoQiId)
    end

    self.changeXianFanRadioBox.Visible = CClientMainPlayer.Inst.HasFeiSheng
end

function LuaNanDuFanHuaFashionPreviewWnd:PreviewFashion(fashionId, blindBoxItemId)
    if fashionId ~= 0 then
        local fashion = Fashion_Fashion.GetData(fashionId)
        if fashion then
            local sexualityIsFit = true
            if fashion.GenderLimit > 0 then
                if fashion.GenderLimit == 1 and CClientMainPlayer.Inst.AppearanceProp.Gender ~= EnumGender.Male then
                    sexualityIsFit = false
                elseif fashion.GenderLimit == 2 and CClientMainPlayer.Inst.AppearanceProp.Gender ~= EnumGender.Female then
                    sexualityIsFit = false
                end
            end
            if not sexualityIsFit then
                g_MessageMgr:ShowMessage("Fashion_Gender_Limit_Cannot_TakeOn")
                return
            end
        end
        CFashionPreviewMgr.curShopMallPreviewType = EnumShopMallFashionPreviewType.PreviewAllFashion
        local pos = Fashion_Fashion.GetData(fashionId).Position
        if pos == 1 then
            LuaShopMallFashionPreviewMgr:SetBodyFashion(fashionId, blindBoxItemId and blindBoxItemId or 0)
        elseif pos == 0 then
            LuaShopMallFashionPreviewMgr:SetHeadFashion(fashionId, blindBoxItemId and blindBoxItemId or 0)
        elseif pos == 3 then
            LuaShopMallFashionPreviewMgr:SetBackPendantFashion(fashionId, 0)
        end
        self:InitModelPreviewer()
    end
end

function LuaNanDuFanHuaFashionPreviewWnd:PreviewZuoQi(id, blindBoxItemId)
    local vehicleId = id
    if vehicleId ~= 0 then
        CFashionPreviewMgr.curShopMallPreviewType = EnumShopMallFashionPreviewType.PreviewAllFashion
        LuaShopMallFashionPreviewMgr:SetZuoQiFashion(vehicleId, blindBoxItemId and blindBoxItemId or 0)
        self:InitModelPreviewer(true)
    end
end

function LuaNanDuFanHuaFashionPreviewWnd:UpdateCostMoney()
    self.curCostMoney = 0

    local itemId, b_canBuy, previewUrl = self:GetCurSelectSingletonShopGoods()
    self.m_CgName = previewUrl
    self.OpenUrlButton.gameObject:SetActive(not CommonDefs.IS_HMT_CLIENT and not CommonDefs.IS_VN_CLIENT and self.m_CgName and not cs_string.IsNullOrEmpty(self.m_CgName))
    if b_canBuy then
        local Item = self.curSelectableSequence[itemId]
        if Item then
            self.curCostMoney = Item.Price
            self.qnCostAndOwnMoney:SetType(EnumMoneyType.NanDuTongBao)
        end
    end
    self.qnCostAndOwnMoney:SetCost(self.curCostMoney)
end

function LuaNanDuFanHuaFashionPreviewWnd:UpdateCurSelectableSequence()
    self.curSelectableSequence = {}
    local headFashion = Fashion_Fashion.GetData(CFashionPreviewMgr.HeadFashionID)
    if headFashion then
        self.curSelectableSequence[headFashion.ItemID] = self.itemID2ShopMallItemMap[headFashion.ItemID]
    end
    local bodyFashion = Fashion_Fashion.GetData(CFashionPreviewMgr.BodyFashionID)
    if bodyFashion then
        self.curSelectableSequence[bodyFashion.ItemID] = self.itemID2ShopMallItemMap[bodyFashion.ItemID]
    end

    local backpendantFashion = Fashion_Fashion.GetData(LuaShopMallFashionPreviewMgr.backpendantFashionId)
    if backpendantFashion then
        self.curSelectableSequence[backpendantFashion.ItemID] = self.itemID2ShopMallItemMap[backpendantFashion.ItemID]
    end

    local foundExpressionAppearanceKey = nil
    Expression_Appearance.Foreach(
            function(key, data)
                if foundExpressionAppearanceKey == nil and data.Group == LuaShopMallFashionPreviewMgr.KaiSanAppearanceGroup and CFashionPreviewMgr.UmbrellaFashionID == data.PreviewDefine then
                    foundExpressionAppearanceKey = data.ItemId
                    self.curSelectableSequence[foundExpressionAppearanceKey] = self.itemID2ShopMallItemMap[foundExpressionAppearanceKey]
                end
            end
    )

    local zuoqi = ZuoQi_ZuoQi.GetData(CFashionPreviewMgr.ZuoQiFashionID)
    if zuoqi then
        self.curSelectableSequence[zuoqi.ItemID] = self.itemID2ShopMallItemMap[zuoqi.ItemID]
    end
    local blindBoxData = Item_BlindBox.GetData(LuaShopMallFashionPreviewMgr.blindBoxItemId)
    if blindBoxData then
        self.curSelectableSequence[LuaShopMallFashionPreviewMgr.blindBoxItemId] = self.itemID2ShopMallItemMap[LuaShopMallFashionPreviewMgr.blindBoxItemId]
    end
    self.curSelectableSequence[LuaShopMallFashionPreviewMgr.headfashionBlindBoxItemId] = self.itemID2ShopMallItemMap[LuaShopMallFashionPreviewMgr.headfashionBlindBoxItemId]
    self.curSelectableSequence[LuaShopMallFashionPreviewMgr.bodyfashionBlindBoxItemId] = self.itemID2ShopMallItemMap[LuaShopMallFashionPreviewMgr.bodyfashionBlindBoxItemId]
    self.curSelectableSequence[LuaShopMallFashionPreviewMgr.zuoqiBlindBoxItemId] = self.itemID2ShopMallItemMap[LuaShopMallFashionPreviewMgr.zuoqiBlindBoxItemId]
    local selectId = self:GetCurSelectSingletonShopGoods()
    for index, template in pairs(self.mallInfo) do
        local id = template.ItemId
        local isSelected, isPreview = false, false
        if self.curCategory == LuaShopMallFashionPreviewMgr.EnumLingYuMallSubCategory.ShiZhuang then
            if LuaShopMallFashionPreviewMgr.recentlyChooseFashion and id == LuaShopMallFashionPreviewMgr.recentlyChooseFashion.ItemID then
                isSelected = true
            end
            if (headFashion and (id == headFashion.ItemID)) or (bodyFashion and (id == bodyFashion.ItemID)) then
                isPreview = true
            end
            if id == LuaShopMallFashionPreviewMgr.headfashionBlindBoxItemId or id == LuaShopMallFashionPreviewMgr.bodyfashionBlindBoxItemId then
                isPreview = true
            end
        elseif self.curCategory == LuaShopMallFashionPreviewMgr.EnumLingYuMallSubCategory.ZuoQi then
            if zuoqi and (id == zuoqi.ItemID) then
                isSelected = true
                isPreview = true
            end
            if id == LuaShopMallFashionPreviewMgr.zuoqiBlindBoxItemId then
                isSelected = true
                isPreview = true
            end
        elseif self.curCategory == LuaShopMallFashionPreviewMgr.EnumLingYuMallSubCategory.BackPendant then
            if backpendantFashion and (id == backpendantFashion.ItemID) then
                isSelected = true
                isPreview = true
            end
        elseif self.curCategory == LuaShopMallFashionPreviewMgr.EnumLingYuMallSubCategory.Umbrella then
            if foundExpressionAppearanceKey and (id == foundExpressionAppearanceKey) then
                isSelected = true
                isPreview = true
            end
        end
        self.m_ItemID2IsPreview[tonumber(id)] = isPreview
        if isSelected then
            selectId = id
        end
    end
    for i = 0, self.m_ShopRoot.m_ItemList.Count - 1 do
        local item = self.m_ShopRoot.m_ItemList[i]
        local template = self.mallInfo[item.Row + 1]
        local id = template.ItemId
        local isPreview = self.m_ItemID2IsPreview[tonumber(id)]
        item.transform:Find("PreviewTag").gameObject:SetActive(isPreview)
        item.transform:GetComponent(typeof(QnTableItem)).m_BackGround.spriteName = id == selectId and "common_btn_07_highlight" or "common_btn_07_normal"
    end
    self:UpdateCostMoney()
    --g_ScriptEvent:BroadcastInLua("ShopMallFashionPreviewWnd_UpdateDescription", {selectId, self.curCategory})
    self:UpdateDescription(selectId, self.curCategory)

    self:InitBlindBoxView(selectId)
end

function LuaNanDuFanHuaFashionPreviewWnd:InitBlindBoxView(selectId)
    self.m_BlindBoxView:SetActive(selectId ~= 0 and selectId == LuaShopMallFashionPreviewMgr.blindBoxItemId)
    self.changeXianFanRadioBox.Visible = CClientMainPlayer.Inst.HasFeiSheng and (LuaShopMallFashionPreviewMgr.blindBoxItemId == 0 or selectId ~= LuaShopMallFashionPreviewMgr.blindBoxItemId)
    if selectId == LuaShopMallFashionPreviewMgr.blindBoxItemId then
        local blindBoxData = Item_BlindBox.GetData(selectId)
        if not blindBoxData then
            return
        end
        self.m_BlindBoxResultItemId = blindBoxData.List[LuaShopMallFashionPreviewMgr.blindBoxSelectIndex][0]
        self.m_BlindBoxNameLabel.text = blindBoxData.Names[LuaShopMallFashionPreviewMgr.blindBoxSelectIndex]
        Extensions.RemoveAllChildren(self.m_BlindBoxViewTable.transform)
        for i = 0, blindBoxData.List.Length - 1 do
            local go = NGUITools.AddChild(self.m_BlindBoxViewTable.gameObject, self.m_BlindBoxSpriteTemplate)
            go:SetActive(true)
            go:GetComponent(typeof(UISprite)).enabled = i == LuaShopMallFashionPreviewMgr.blindBoxSelectIndex
        end
        self.m_BlindBoxViewTable:Reposition()
        --g_ScriptEvent:BroadcastInLua("ShopMallFashionPreviewWnd_UpdateDescription", {selectId, self.curCategory})
        self:UpdateDescription(selectId, self.curCategory)
    end
end

function LuaNanDuFanHuaFashionPreviewWnd:UpdateDescription(itemID, curCategory)
    local descriptionRoot = self.transform:Find("Panel/title/DescriptionRoot")
    local nameLabel = descriptionRoot:Find("NameLabel"):GetComponent(typeof(UILabel))
    --local labelTemplate = descriptionRoot:Find("LabelTemplate")
    local descriptionLabel = descriptionRoot:Find("ScrollView/Table/Label"):GetComponent(typeof(UILabel))
    
    if itemID == nil then
        nameLabel.gameObject:SetActive(false)
        descriptionLabel.gameObject:SetActive(false)
        return
    end
    local item = Item_Item.GetData(itemID)
    if not item then
        nameLabel.gameObject:SetActive(false)
        descriptionLabel.gameObject:SetActive(false)
        return
    end
    nameLabel.gameObject:SetActive(true)
    local Name = item.Name
    nameLabel.text = Name

    descriptionLabel.gameObject:SetActive(true)
    descriptionLabel.text = CUICommonDef.TranslateToNGUIText(item.Description)
end

function LuaNanDuFanHuaFashionPreviewWnd:OnRestButtonClick()
    self:SetModelPosAndScale(self.defaultModelLocalPos, 1)
end

function LuaNanDuFanHuaFashionPreviewWnd:OnTakeOffButtonClick()
    LuaShopMallFashionPreviewMgr.headfashionBlindBoxItemId = 0
    LuaShopMallFashionPreviewMgr.bodyfashionBlindBoxItemId = 0
    LuaShopMallFashionPreviewMgr.zuoqiBlindBoxItemId = 0
    LuaShopMallFashionPreviewMgr.blindBoxItemId = 0
    CFashionPreviewMgr.curShopMallPreviewType = EnumShopMallFashionPreviewType.TakeOffAlllFashion
    LuaShopMallFashionPreviewMgr:OnTakeOff()
    self:InitModelPreviewer()
    LuaShopMallFashionPreviewMgr.recentlyChooseFashion = nil
    --g_ScriptEvent:BroadcastInLua("ShopMallFashionPreviewWnd_UpdateDescription", {nil, self.curCategory})
    self:UpdateDescription(nil, self.curCategory)

    for index, template in pairs(self.mallInfo) do
        self.m_ShopRoot:SetSelectRow(index - 1, false)
    end
    self:UpdateCurSelectableSequence()
    self:InitDoExpressionButton()
end

function LuaNanDuFanHuaFashionPreviewWnd:GetCurSelectSingletonShopGoods()
    local itemId, b_canBuy, previewUrl
    if self.curCategory == LuaShopMallFashionPreviewMgr.EnumLingYuMallSubCategory.ZuoQi then
        --坐骑
        local zuoqi = ZuoQi_ZuoQi.GetData(CFashionPreviewMgr.ZuoQiFashionID)
        if zuoqi then
            itemId = zuoqi.ItemID
            previewUrl = zuoqi.PreviewVideoUrl
            b_canBuy = true
        end
        if LuaShopMallFashionPreviewMgr.zuoqiBlindBoxItemId ~= 0 then
            itemId = LuaShopMallFashionPreviewMgr.zuoqiBlindBoxItemId
            b_canBuy = true
            local blindBoxData = Item_BlindBox.GetData(itemId)
            if blindBoxData then
                previewUrl = blindBoxData.PreviewVideoUrl
            end
        end
    elseif self.curCategory == LuaShopMallFashionPreviewMgr.EnumLingYuMallSubCategory.BackPendant then
        local beishi = Fashion_Fashion.GetData(LuaShopMallFashionPreviewMgr.backpendantFashionId)
        if beishi then
            itemId = beishi.ItemID
            previewUrl = beishi.PreviewVideoUrl
            b_canBuy = true
        end
    elseif self.curCategory == LuaShopMallFashionPreviewMgr.EnumLingYuMallSubCategory.ShiZhuang then
        --时装
        local blindBoxItemId = 0
        if CFashionPreviewMgr.HeadFashionID == 0 then
            local bodyFashion = Fashion_Fashion.GetData(CFashionPreviewMgr.BodyFashionID)
            if bodyFashion then
                itemId = bodyFashion.ItemID
                previewUrl = bodyFashion.PreviewVideoUrl
                b_canBuy = true
            end
            if LuaShopMallFashionPreviewMgr.bodyfashionBlindBoxItemId ~= 0 then
                blindBoxItemId = LuaShopMallFashionPreviewMgr.bodyfashionBlindBoxItemId
            end
        elseif CFashionPreviewMgr.BodyFashionID == 0 then
            local headFashion = Fashion_Fashion.GetData(CFashionPreviewMgr.HeadFashionID)
            if headFashion then
                itemId = headFashion.ItemID
                previewUrl = headFashion.PreviewVideoUrl
                b_canBuy = true
            end
            if LuaShopMallFashionPreviewMgr.headfashionBlindBoxItemId ~= 0 then
                blindBoxItemId = LuaShopMallFashionPreviewMgr.headfashionBlindBoxItemId
            end
        elseif LuaShopMallFashionPreviewMgr.recentlyChooseFashion then
            itemId = LuaShopMallFashionPreviewMgr.recentlyChooseFashion.ItemID
            previewUrl = LuaShopMallFashionPreviewMgr.recentlyChooseFashion.PreviewVideoUrl
            b_canBuy = true
            if LuaShopMallFashionPreviewMgr.recentlyChooseFashion.Position == 0 and LuaShopMallFashionPreviewMgr.headfashionBlindBoxItemId ~= 0 then
                blindBoxItemId = LuaShopMallFashionPreviewMgr.headfashionBlindBoxItemId
            elseif LuaShopMallFashionPreviewMgr.recentlyChooseFashion.Position == 1 and LuaShopMallFashionPreviewMgr.bodyfashionBlindBoxItemId ~= 0 then
                blindBoxItemId = LuaShopMallFashionPreviewMgr.bodyfashionBlindBoxItemId
            end
        end
        if blindBoxItemId ~= 0 then
            itemId = blindBoxItemId
            b_canBuy = true
            local blindBoxData = Item_BlindBox.GetData(itemId)
            if blindBoxData then
                previewUrl = blindBoxData.PreviewVideoUrl
            end
        end
    elseif self.curCategory == LuaShopMallFashionPreviewMgr.EnumLingYuMallSubCategory.Umbrella then
        --伞
        local foundExpressionAppearanceKey = nil
        Expression_Appearance.Foreach(
                function(key, data)
                    if foundExpressionAppearanceKey == nil and data.Group == LuaShopMallFashionPreviewMgr.KaiSanAppearanceGroup and CFashionPreviewMgr.UmbrellaFashionID == data.PreviewDefine then
                        itemId = data.ItemId
                        foundExpressionAppearanceKey = itemId
                        b_canBuy = true
                    end
                end
        )
    end
    return itemId, b_canBuy, previewUrl
end

function LuaNanDuFanHuaFashionPreviewWnd:BuySingletonShopGoods()
    local itemId, b_canBuy = self:GetCurSelectSingletonShopGoods()
    if b_canBuy then
        local item = self.curSelectableSequence[itemId]
        self:BuyMallItem(item, EShopMallRegion_lua.ELingyuMall, 1)
    else
        local msgTable = {
            [1] = LocalString.GetString("坐骑"),
            [2] = LocalString.GetString("时装"),
            [3] = LocalString.GetString("伞面"),
            [10] = LocalString.GetString("背饰"),
        }
        g_MessageMgr:ShowMessage("ShopMallFashionPreviewWnd_Verify_ShopMallGoods_IsSelected", msgTable[self.curCategory])
    end
end

function LuaNanDuFanHuaFashionPreviewWnd:BuyMallItem(template, type, count)
    local isZuoQi = false
    if template.ZuoQiId then
        isZuoQi = true
    end
    local realBuyFunc = function(_isZuoQi, cfgId)
        if _isZuoQi then
            Gac2Gas.UnlockNanDuLordZuoQi(cfgId)
        else
            Gac2Gas.UnlockNanDuLordFashion(cfgId)
        end
    end
    
    local owned = self.m_OwnedShopItems[template.ItemId]
    if System.String.IsNullOrEmpty(template.ConfirmMessage) then
        if owned then
            MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("LinyuShopWnd_Buy_Owned_MallItem_Confirm"), DelegateFactory.Action(function ()
                realBuyFunc(isZuoQi, template.ID)
            end), nil, nil, nil, false)
        else
            realBuyFunc(isZuoQi, template.ID)
        end
    else
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage(template.ConfirmMessage), DelegateFactory.Action(function ()
            if owned then
                MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("LinyuShopWnd_Buy_Owned_MallItem_Confirm"), DelegateFactory.Action(function ()
                    realBuyFunc(isZuoQi, template.ID)
                end), nil, nil, nil, false)
            else
                realBuyFunc(isZuoQi, template.ID)
            end
        end), nil, nil, nil, false)
    end
end

function LuaNanDuFanHuaFashionPreviewWnd:OnPinchIn(gesture)
    local deltaPinch = gesture.deltaPinch
    local pinchScale = math.min(math.max(deltaPinch / 400, 0), 1)
    self:OnPinch(-pinchScale)
end

function LuaNanDuFanHuaFashionPreviewWnd:OnPinchOut(gesture)
    local deltaPinch = gesture.deltaPinch
    local pinchScale = math.min(math.max(deltaPinch / 400, 0), 1)
    self:OnPinch(pinchScale)
end

function LuaNanDuFanHuaFashionPreviewWnd:OnMouseScrollWheel()
    local v = Input.GetAxis("Mouse ScrollWheel")
    self:OnPinch(v)
end

function LuaNanDuFanHuaFashionPreviewWnd:OnPinch(delta)
    local zuoqi = ZuoQi_ZuoQi.GetData(CFashionPreviewMgr.ZuoQiFashionID)
    local d = (zuoqi and zuoqi.ShopMallMaxPreviewScale or self.maxPreviewScale) - 1
    local t = (self.localModelScale - 1) / d

    t = t + delta

    t = math.max(math.min(1, t), 0)
    local s = math.lerp(1, zuoqi and zuoqi.ShopMallMaxPreviewScale or self.maxPreviewScale, t)
    local y = math.lerp(0, zuoqi and zuoqi.ShopMallMaxPreviewYOffSet or self.maxPreviewOffsetY, t)
    local pos = Vector3(self.defaultModelLocalPos.x, self.defaultModelLocalPos.y + y, self.defaultModelLocalPos.z)
    self:SetModelPosAndScale(pos, s)
end

function LuaNanDuFanHuaFashionPreviewWnd:InitShadowCameraAndLight()
    if not LuaShopMallFashionPreviewMgr.enableMultiLight then
        return
    end
    local t = CUIManager.instance.transform:Find(self.qnModelPreviewer.identifier)
    if not t then
        return
    end
    self.shadowCamera = t:Find("UI Model Shadow Camera")
    if not self.shadowCamera then
        return
    end
    self.shadowCameraRelativePos = {
        self.localModelPos.x - self.shadowCamera.localPosition.x,
        self.localModelPos.y - self.shadowCamera.localPosition.y,
        self.localModelPos.z - self.shadowCamera.localPosition.z
    }
    --local newRO = CRenderObject.CreateRenderObject(t.gameObject, "LightAndFloorAndWall")
    --local fx =
    --CEffectMgr.Inst:AddObjectFX(
    --        88801543,
    --        newRO,
    --        0,
    --        1,
    --        1,
    --        nil,
    --        false,
    --        EnumWarnFXType.None,
    --        Vector3.zero,
    --        Vector3.zero,
    --        DelegateFactory.Action_GameObject(
    --                function(go)
    --                    NGUITools.SetLayer(newRO.gameObject, LayerDefine.Effect_3D)
    --                    self.floor = go.transform:Find("Anchor/beijing")
    --                    self.floorInitialLocalPos = self.floor.transform.localPosition
    --                    self.beiguangLight = go.transform:Find("Anchor/beiguang")
    --                end
    --        )
    --)
    --self.lightAndFloorAndWall = newRO.transform
    --self.lightAndFloorAndWallRelativePos = {
    --    self.localModelPos.x - self.lightAndFloorAndWall.localPosition.x,
    --    self.localModelPos.y - self.lightAndFloorAndWall.localPosition.y,
    --    self.localModelPos.z - self.lightAndFloorAndWall.localPosition.z
    --}
    --newRO:AddFX("LightAndFloorAndWall", fx, -1)
end

function LuaNanDuFanHuaFashionPreviewWnd:SetShadowCameraAndLightPos(pos)
    if not LuaShopMallFashionPreviewMgr.enableMultiLight then
        return
    end
    if not self.shadowCamera then
        return
    end
    if (not self.shadowCameraRelativePos) or (#self.shadowCameraRelativePos ~= 3) then
        return
    end
    self.shadowCamera.transform.localPosition = Vector3(pos.x - self.shadowCameraRelativePos[1], pos.y - self.shadowCameraRelativePos[2], pos.z - self.shadowCameraRelativePos[3])
    --self.lightAndFloorAndWall.transform.localPosition =
    --Vector3(pos.x - self.lightAndFloorAndWallRelativePos[1], pos.y - self.lightAndFloorAndWallRelativePos[2], pos.z - self.lightAndFloorAndWallRelativePos[3])
end

function LuaNanDuFanHuaFashionPreviewWnd:GetModelRoot()
    if not self.modelRoot then
        local t = CUIManager.instance.transform:Find(self.qnModelPreviewer.identifier)
        if t then
            self.modelRoot = t:Find("ModelRoot")
        end
    end
    return self.modelRoot
end

function LuaNanDuFanHuaFashionPreviewWnd:OnSyncRepertoryForMallPreviewDone()
    self:RefreshAllOwnerItemInfo()
end

function LuaNanDuFanHuaFashionPreviewWnd:InitTransformButton()
    if IsTransformableFashionOpen() and CFashionPreviewMgr.UmbrellaFashionID == 0 then
        local isTransformable = LuaShopMallFashionPreviewMgr:IsTransformable()
        self.m_TransformButton.gameObject:SetActive(isTransformable)
        if not isTransformable then
            LuaShopMallFashionPreviewMgr.isTransformed = false
        end
        self:UpdateTransformState()
    else
        local changeState = LuaShopMallFashionPreviewMgr.isTransformed ~= false
        self.m_TransformButton.gameObject:SetActive(false)
        LuaShopMallFashionPreviewMgr.isTransformed = false
        if changeState then
            self:InitModelPreviewer()
        end
    end
end

function LuaNanDuFanHuaFashionPreviewWnd:Transform()
    LuaShopMallFashionPreviewMgr.isTransformed = not LuaShopMallFashionPreviewMgr.isTransformed
    self:UpdateTransformState()

    self:InitModelPreviewer()
end

function LuaNanDuFanHuaFashionPreviewWnd:UpdateTransformState()
    if LuaShopMallFashionPreviewMgr.isTransformed == true then
        self.m_TransformButton.Text = LocalString.GetString("褪形")
        self.m_TransformHighlight:SetActive(true)
    else
        self.m_TransformButton.Text = LocalString.GetString("化形")
        self.m_TransformHighlight:SetActive(false)
    end
end

function LuaNanDuFanHuaFashionPreviewWnd:DoExpression()
    if CClientMainPlayer.Inst == nil then
        return
    end
    if not self.m_DoExpressionButton.gameObject.activeSelf then
        return
    end
    if CFashionPreviewMgr.ZuoQiFashionID ~= 0 then
        CFashionPreviewMgr.ZuoQiFashionID = 0
    end
    if CFashionPreviewMgr.UmbrellaFashionID ~= 0 then
        CFashionPreviewMgr.UmbrellaFashionID = 0
    end
    self:UpdateCurSelectableSequence()
    local appearData = self.qnModelPreviewer.needShowAppearance
    local headFashion = Fashion_Fashion.GetData(CFashionPreviewMgr.HeadFashionID ~= 0 and CFashionPreviewMgr.HeadFashionID or appearData.HeadFashionId)
    if headFashion == nil then
        return
    end
    local time = CClientMainPlayer.Inst:GetExpressionAnimationTime(headFashion.SpecialExpression)
    local progress = self.m_DoExpressionButton.transform:Find("Progress"):GetComponent(typeof(UIProgressBar))
    progress.gameObject:SetActive(true)
    LuaTweenUtils.DOKill(progress.transform, false)
    local tween = LuaTweenUtils.TweenFloat(0, 1, time, function ( val )
        progress.value = val
    end,function ()
        progress.gameObject:SetActive(false)
    end)
    LuaTweenUtils.SetTarget(tween,progress.transform)
    self:InitModelPreviewer(true)
    CClientMainPlayer.ShowExpressionAction(self.qnModelPreviewer.m_RO,CClientMainPlayer.Inst.Gender,headFashion.SpecialExpression)
end

function LuaNanDuFanHuaFashionPreviewWnd:InitDoExpressionButton()
    self.m_DoExpressionButton.gameObject:SetActive(false)
    local appearData = self.qnModelPreviewer.needShowAppearance
    local headFashion = Fashion_Fashion.GetData(CFashionPreviewMgr.HeadFashionID ~= 0 and CFashionPreviewMgr.HeadFashionID or appearData.HeadFashionId)
    local bodyFashion = Fashion_Fashion.GetData(CFashionPreviewMgr.BodyFashionID ~= 0 and CFashionPreviewMgr.BodyFashionID or appearData.BodyFashionId)
    if CClientMainPlayer.Inst.AppearanceProp.Gender ~= EnumGender.Monster and headFashion and bodyFashion and headFashion.SpecialExpression ~= 0 and headFashion.SpecialExpression == bodyFashion.SpecialExpression then
        self.m_DoExpressionButton.gameObject:SetActive(true)
        local progress = self.m_DoExpressionButton.transform:Find("Progress"):GetComponent(typeof(UIProgressBar))
        progress.gameObject:SetActive(false)
    end
end