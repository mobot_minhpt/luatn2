local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local CUITexture = import "L10.UI.CUITexture"
local Extensions = import "Extensions"
local Vector3 = import "UnityEngine.Vector3"
local CItemMgr = import "L10.Game.CItemMgr"
local CItemAccessTipMgr = import "L10.UI.CItemAccessTipMgr"
local CTooltipAlignType = import "L10.UI.CTooltip+AlignType"

LuaCarnivalCaiPiao2019Wnd = class()
RegistChildComponent(LuaCarnivalCaiPiao2019Wnd,"closeBtn", GameObject)
RegistChildComponent(LuaCarnivalCaiPiao2019Wnd,"tab1", GameObject)
RegistChildComponent(LuaCarnivalCaiPiao2019Wnd,"tab1Normal", GameObject)
RegistChildComponent(LuaCarnivalCaiPiao2019Wnd,"tab1Light", GameObject)

RegistChildComponent(LuaCarnivalCaiPiao2019Wnd,"tab2", GameObject)
RegistChildComponent(LuaCarnivalCaiPiao2019Wnd,"tab2Normal", GameObject)
RegistChildComponent(LuaCarnivalCaiPiao2019Wnd,"tab2Light", GameObject)

RegistChildComponent(LuaCarnivalCaiPiao2019Wnd,"node1", GameObject)
RegistChildComponent(LuaCarnivalCaiPiao2019Wnd,"playerNode", GameObject)
RegistChildComponent(LuaCarnivalCaiPiao2019Wnd,"playerIconNode", GameObject)

RegistChildComponent(LuaCarnivalCaiPiao2019Wnd,"node2", GameObject)

RegistClassMember(LuaCarnivalCaiPiao2019Wnd, "maxChooseNum")
RegistClassMember(LuaCarnivalCaiPiao2019Wnd, "chooseIconTable")
RegistClassMember(LuaCarnivalCaiPiao2019Wnd, "chooseIconNodeTable")
RegistClassMember(LuaCarnivalCaiPiao2019Wnd, "buyBonusBtn")
RegistClassMember(LuaCarnivalCaiPiao2019Wnd, "buyBonusResNode")
RegistClassMember(LuaCarnivalCaiPiao2019Wnd, "buyBonusTip")

RegistClassMember(LuaCarnivalCaiPiao2019Wnd, "hisTempalteNode")

RegistClassMember(LuaCarnivalCaiPiao2019Wnd, "hisDataTable")

RegistClassMember(LuaCarnivalCaiPiao2019Wnd, "totalRewardNum")
RegistClassMember(LuaCarnivalCaiPiao2019Wnd, "totalChooseNode")

function LuaCarnivalCaiPiao2019Wnd:Close()
	CUIManager.CloseUI(CLuaUIResources.CarnivalCaiPiao2019Wnd)
end

function LuaCarnivalCaiPiao2019Wnd:OnEnable()
	g_ScriptEvent:AddListener("CnvLotteryAddSucc", self, "AddLotterySucc")
	g_ScriptEvent:AddListener("CnvLotteryHisInfoBack", self, "InitNode2Data")
	--g_ScriptEvent:AddListener("CnvLotteryTotalRewardInfoBack", self, "TotalRewardInfoBack")
end

function LuaCarnivalCaiPiao2019Wnd:OnDisable()
	g_ScriptEvent:RemoveListener("CnvLotteryAddSucc", self, "AddLotterySucc")
	g_ScriptEvent:RemoveListener("CnvLotteryHisInfoBack", self, "InitNode2Data")
	--g_ScriptEvent:RemoveListener("CnvLotteryTotalRewardInfoBack", self, "TotalRewardInfoBack")
end

function LuaCarnivalCaiPiao2019Wnd:Split(str, pat)
   local t = {}  -- NOTE: use {n = 0} in Lua-5.0
   local fpat = "(.-)" .. pat
   local last_end = 1
   local s, e, cap = str:find(fpat, 1)
   while s do
      if s ~= 1 or cap ~= "" then
         table.insert(t,cap)
      end
      last_end = e+1
      s, e, cap = str:find(fpat, last_end)
   end
   if last_end <= #str then
      cap = str:sub(last_end)
      table.insert(t, cap)
   end
   return t
end

function LuaCarnivalCaiPiao2019Wnd:TotalRewardInfoBack(info)
	local formulaId = JiaNianHua_Lottery.GetData().JackpotFormulaId
	local giftItems = JiaNianHua_Lottery.GetData().GiftItemId
	local giftItemTable = self:Split(giftItems,';')
	local valTable = {}
	for i,v in pairs(giftItemTable) do
		local value = tonumber(v)
		if value and value > 0 then
			if CommonDefs.DictContains(info, typeof(String), tostring(value)) then
				valTable[value] = tonumber(info[tostring(value)])
			else
				valTable[value] = 0
			end
		end
	end

	local val = AllFormulas.Action_Formula[formulaId].Formula(nil, nil, {valTable})
	self.totalRewardNum = val
  local totalBonus = self.node2.transform:Find('num'):GetComponent(typeof(UILabel))
	totalBonus.text = self.totalRewardNum
end

function LuaCarnivalCaiPiao2019Wnd:AddLotterySucc()
  self.buyBonusBtn:SetActive(true)
  self:InitNode1Data()
end

function LuaCarnivalCaiPiao2019Wnd:OnNeedItemClicked(go, itemId)
	CItemAccessTipMgr.Inst:ShowAccessTip(nil, itemId, false, go.transform, CTooltipAlignType.Top)
end

function LuaCarnivalCaiPiao2019Wnd:InitNeedItem(go, itemId, count)
	local item = Item_Item.GetData(itemId)
	if not item then return end

	local IconTexture = go.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
	local DisableSprite = go.transform:Find("DisableSprite").gameObject
	local GetHint = go.transform:Find("GetHint").gameObject
	local CountLabel = go.transform:Find("CountLabel"):GetComponent(typeof(UILabel))
	--local NameLabel = go.transform:Find("Label"):GetComponent(typeof(UILabel))
	--NameLabel.text = item.Name

	IconTexture:LoadMaterial(item.Icon)
	local bindItemCountInBag, notBindItemCountInBag = CItemMgr.Inst:CalcItemCountInBagByTemplateId(itemId)
	local totalCount = bindItemCountInBag + notBindItemCountInBag
	if totalCount < count then
		DisableSprite:SetActive(true)
		GetHint:SetActive(true)
		CountLabel.text = SafeStringFormat3("[FF0000]%d[-]/%d", totalCount,count)

		local onNeedItemClicked = function (go)
			self:OnNeedItemClicked(go, itemId)
		end
		CommonDefs.AddOnClickListener(IconTexture.gameObject, DelegateFactory.Action_GameObject(onNeedItemClicked), false)
		return false
	else
		DisableSprite:SetActive(false)
		GetHint:SetActive(false)
		CountLabel.text = SafeStringFormat3("%d/%d", totalCount, count)
		return true
	end
end

function LuaCarnivalCaiPiao2019Wnd:InitSelfChooseNode()
  for i,v in pairs(self.chooseIconNodeTable) do
    v.transform:Find('Mask/Icon').gameObject:SetActive(false)
    v.transform:Find('Mask/question_mark').gameObject:SetActive(true)
  end

  if not self.chooseIconTable then
    self.chooseIconTable = {}
  end
	table.sort(self.chooseIconTable,function(a,b) return a.index < b.index end)

  if self.chooseIconTable then
    for i,v in ipairs(self.chooseIconTable) do
      local data = JiaNianHua_LotteryIcon.GetData(v.index)
      if data then
        local node = self.chooseIconNodeTable[i]
        node.transform:Find('Mask/Icon').gameObject:SetActive(true)
        node.transform:Find('Mask/question_mark').gameObject:SetActive(false)
				node.transform:Find("Mask/Icon"):GetComponent(typeof(CUITexture)):LoadMaterial(data.Portrait)
      end
    end
  end
end

function LuaCarnivalCaiPiao2019Wnd:AddCaiPiaoChooseNode(index,lightNode)
  if not self.chooseIconTable then
    self.chooseIconTable = {}
  end
  if #self.chooseIconTable >= self.maxChooseNum and not lightNode.activeSelf then
		--g_MessageMgr:ShowMessage("Cnv_Lottery_More_Four_Number")
    return
  end

  if lightNode.activeSelf then
    lightNode:SetActive(false)
    for i,v in pairs(self.chooseIconTable) do
      if v.index == index then
        table.remove(self.chooseIconTable,i)
        break
      end
    end
  else
    lightNode:SetActive(true)
    table.insert(self.chooseIconTable,{index = index})
  end

  self:InitSelfChooseNode()
end

function LuaCarnivalCaiPiao2019Wnd:InitNode1Data()
  for i,v in pairs(self.chooseIconNodeTable) do
    v.transform:Find('Mask/Icon').gameObject:SetActive(false)
    v.transform:Find('Mask/question_mark').gameObject:SetActive(true)
  end
	self.chooseIconTable = {}
	self.totalChooseNode = {}
	Extensions.RemoveAllChildren(self.playerNode.transform)
  local originPos = Vector3(-527,199,0)
  local width = 150
  local height = 150
  local widthNum = 5

  self:InitNeedItem(self.buyBonusResNode,JiaNianHua_Lottery.GetData().LotteryItemId,1)

  local count = JiaNianHua_LotteryIcon.GetDataCount()
  for i=1,count do
		local index = 1000 + i
    local data = JiaNianHua_LotteryIcon.GetData(index)
    if data then
      local node = NGUITools.AddChild(self.playerNode,self.playerIconNode)
      node:SetActive(true)
      local w = (i - 1) % widthNum
      local h = (i - w - 1) / widthNum
      node.transform.localPosition = Vector3(originPos.x + width * w,originPos.y - height * h,originPos.z)
			node.transform:Find("Mask/Icon"):GetComponent(typeof(CUITexture)):LoadMaterial(data.Portrait)
      local lightNode = node.transform:Find('light').gameObject
      lightNode:SetActive(false)
      local nodeClickFunc = function(go)
        self:AddCaiPiaoChooseNode(index,lightNode)
      end
			self.totalChooseNode[index] = node
      CommonDefs.AddOnClickListener(node,DelegateFactory.Action_GameObject(nodeClickFunc),false)
    end
  end

  local randomBtn = self.node1.transform:Find('RandomBtn').gameObject
	local onRandomCaiPiaoClick = function(go)
		self:RandomChooseCaiPiao()
	end
	CommonDefs.AddOnClickListener(randomBtn,DelegateFactory.Action_GameObject(onRandomCaiPiaoClick),false)
end

function LuaCarnivalCaiPiao2019Wnd:RandomChooseCaiPiao()
	if not self.totalChooseNode then
		return
	end

	self.chooseIconTable = {}
	for i,v in pairs(self.totalChooseNode) do
		local lightNode = v.transform:Find('light').gameObject
		lightNode:SetActive(false)
	end

  local count = JiaNianHua_LotteryIcon.GetDataCount()
	local randomTable = {}
	for i=1,count do
		table.insert(randomTable,i+1000)
	end

	local chooseTable = {}
	for i=1,4 do
		local index = math.random(1, #randomTable)
		local chooseIndex = randomTable[index]
		table.remove(randomTable,index)
		chooseTable[chooseIndex] = true
	end

	for i,v in pairs(self.totalChooseNode) do
		if chooseTable[i] then
      local lightNode = v.transform:Find('light').gameObject
			self:AddCaiPiaoChooseNode(i,lightNode)
		end
	end
end

function LuaCarnivalCaiPiao2019Wnd:InitNode1()
  self.node1:SetActive(true)
  self.node2:SetActive(false)
end

function LuaCarnivalCaiPiao2019Wnd:InitNode2()
  self.node1:SetActive(false)
  self.node2:SetActive(true)
  local bonusNodeTable = {
    self.node2.transform:Find('myChoiceNode/node1'),
    self.node2.transform:Find('myChoiceNode/node2'),
    self.node2.transform:Find('myChoiceNode/node3'),
    self.node2.transform:Find('myChoiceNode/node4'),
  }
  for i,v in pairs(bonusNodeTable) do
    v.transform:Find('Mask/Icon').gameObject:SetActive(false)
    v.transform:Find('Mask/question_mark').gameObject:SetActive(true)
  end
  Gac2Gas.CnvLotteryQueryLottery()
end

function LuaCarnivalCaiPiao2019Wnd:InitNode2BonusInfo()

end

function LuaCarnivalCaiPiao2019Wnd:InitNode2Data(data)
	self:InitNode2BonusInfo()

  local bonusNodeTable = {
    self.node2.transform:Find('myChoiceNode/node1'),
    self.node2.transform:Find('myChoiceNode/node2'),
    self.node2.transform:Find('myChoiceNode/node3'),
    self.node2.transform:Find('myChoiceNode/node4'),
  }
  local resultData = data.result
	local resultTable = {}
	local resultSign = false
  if resultData then
    local count = resultData.Count - 1
    for i = 0, count do
      local index = resultData[i]
      local data = JiaNianHua_LotteryIcon.GetData(index)
			resultTable[index] = true
      if data then
				resultSign = true
        local node = bonusNodeTable[i+1]
        node.transform:Find('Mask/Icon').gameObject:SetActive(true)
        node.transform:Find('Mask/question_mark').gameObject:SetActive(false)
				node.transform:Find("Mask/Icon"):GetComponent(typeof(CUITexture)):LoadMaterial(data.Portrait)
      end
    end
  end

  local totalBonus = self.node2.transform:Find('num'):GetComponent(typeof(UILabel))
	totalBonus.text = data.canReward

  local _table = self.node2.transform:Find('record/scrollview/table'):GetComponent(typeof(UITable))
  local scrollview = self.node2.transform:Find('record/scrollview'):GetComponent(typeof(UIScrollView))

	Extensions.RemoveAllChildren(_table.transform)
  local getRewardBtn = self.node2.transform:Find('GetBonusBtn').gameObject
  local tipNode = self.node2.transform:Find('TipNode').gameObject
	if not resultSign then
		getRewardBtn:SetActive(false)
		tipNode:SetActive(true)
	else
		getRewardBtn:SetActive(true)
		tipNode:SetActive(false)
	end

  local hisData = data.his
  if hisData then
    local count = hisData.Count - 1

    for i = 0,count do
			local data = hisData[i]
			if data then
				local node = NGUITools.AddChild(_table.gameObject,self.hisTempalteNode)
				node:SetActive(true)

				local timestamp = data.CreateTime
				local timetable = os.date('*t',timestamp)
				local mins = timetable.min
				if mins < 10 then
					mins = '0' .. mins
				end
				local timestring = SafeStringFormat3('%s.%s.%s %s:%s',timetable.year,timetable.month,timetable.day,timetable.hour,mins)
				node.transform:Find('time'):GetComponent(typeof(UILabel)).text = timestring

				local level = 0
				if CommonDefs.DictContains(data, typeof(String), 'Level') then
					level = tonumber(data.Level)
				end
				if not level then
					level = 0
				end
				level = level + 1
				local levelString = {LocalString.GetString('未中奖'),LocalString.GetString('一等奖'),LocalString.GetString('二等奖'),LocalString.GetString('三等奖'),LocalString.GetString('四等奖')}
				if not resultSign then
					node.transform:Find('result'):GetComponent(typeof(UILabel)).text = LocalString.GetString('未开奖')
				else
					node.transform:Find('result'):GetComponent(typeof(UILabel)).text = levelString[level]
				end

				local iconData = data.DataList
				if iconData then
					local bonusNodeTable = {
						node.transform:Find('myChoiceNode/node1'),
						node.transform:Find('myChoiceNode/node2'),
						node.transform:Find('myChoiceNode/node3'),
						node.transform:Find('myChoiceNode/node4'),
					}
					for i,v in pairs(bonusNodeTable) do
						v.transform:Find('Mask/Icon').gameObject:SetActive(false)
						v.transform:Find('Mask/question_mark').gameObject:SetActive(true)
					end

					local count = iconData.Count - 1
					local indexTable = {}
					for i=0,count do
						local index = tonumber(iconData[i])
						table.insert(indexTable,index)
					end
					table.sort(indexTable)

					for i,v in ipairs(indexTable) do
						local data = JiaNianHua_LotteryIcon.GetData(v)
						if data then
							local node = bonusNodeTable[i]
							node.transform:Find('Mask/Icon').gameObject:SetActive(true)
							node.transform:Find('Mask/question_mark').gameObject:SetActive(false)
							node.transform:Find("Mask/Icon"):GetComponent(typeof(CUITexture)):LoadMaterial(data.Portrait)
							if resultTable[v] then
								node.transform:Find('Mask/greenbg').gameObject:SetActive(true)
							else
								node.transform:Find('Mask/greenbg').gameObject:SetActive(false)
							end
						end
					end
				end
			end
    end
    _table:Reposition()
    scrollview:ResetPosition()
  end
end

function LuaCarnivalCaiPiao2019Wnd:InitTab()
  local onTab1Click = function(go)
    if self.tab1Normal.activeSelf then
      self.tab1Normal:SetActive(false)
      self.tab1Light:SetActive(true)
      self.tab2Normal:SetActive(true)
      self.tab2Light:SetActive(false)
      self:InitNode1()
    else

    end
  end
  local onTab2Click = function(go)
    if self.tab2Normal.activeSelf then
      self.tab1Normal:SetActive(true)
      self.tab1Light:SetActive(false)
      self.tab2Normal:SetActive(false)
      self.tab2Light:SetActive(true)
      self:InitNode2()
    else

    end
  end

	CommonDefs.AddOnClickListener(self.tab1,DelegateFactory.Action_GameObject(onTab1Click),false)
	CommonDefs.AddOnClickListener(self.tab2,DelegateFactory.Action_GameObject(onTab2Click),false)

  self:InitNode1Data()
  onTab1Click()

  self.playerIconNode:SetActive(false)
end

function LuaCarnivalCaiPiao2019Wnd:Init()
	local onCloseClick = function(go)
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.closeBtn,DelegateFactory.Action_GameObject(onCloseClick),false)
  self.buyBonusBtn = self.node1.transform:Find('GetBonusBtn').gameObject
	local onSubmitCaiPiaoClick = function(go)
    if self.chooseIconTable then
			if #self.chooseIconTable < self.maxChooseNum then
				--g_MessageMgr:ShowMessage("Cnv_Lottery_Less_Four_Number")
				return
			end

      Gac2Gas.CnvLotteryAddTicket(self.chooseIconTable[1].index, self.chooseIconTable[2].index, self.chooseIconTable[3].index, self.chooseIconTable[4].index)

      self.buyBonusBtn:SetActive(false)
    end
	end
	CommonDefs.AddOnClickListener(self.buyBonusBtn,DelegateFactory.Action_GameObject(onSubmitCaiPiaoClick),false)

  local getRewardBtn = self.node2.transform:Find('GetBonusBtn').gameObject
	local onRewardCaiPiaoClick = function(go)
    Gac2Gas.CnvLotteryGetReward()
	end
	CommonDefs.AddOnClickListener(getRewardBtn,DelegateFactory.Action_GameObject(onRewardCaiPiaoClick),false)

  --Gac2Gas.CnvLotteryQueryPrizePool()

  self.maxChooseNum = 4
  self.chooseIconNodeTable = {
    self.node1.transform:Find('myChoiceNode/node1'),
    self.node1.transform:Find('myChoiceNode/node2'),
    self.node1.transform:Find('myChoiceNode/node3'),
    self.node1.transform:Find('myChoiceNode/node4'),
  }
  self.buyBonusResNode = self.node1.transform:Find('resNode').gameObject
  self.buyBonusTip = self.node1.transform:Find('tipBtn').gameObject

	local onBonusTipClick = function(go)
		g_MessageMgr:ShowMessage("Cnv_Lottery_Tips")
	end
	CommonDefs.AddOnClickListener(self.buyBonusTip,DelegateFactory.Action_GameObject(onBonusTipClick),false)

  local awardTime2 = self.node2.transform:Find('awardTime'):GetComponent(typeof(UILabel))
  awardTime2.text = JiaNianHua_Lottery.GetData().LotteryTime

  self.hisTempalteNode = self.node2.transform:Find('infoNode').gameObject
  self.hisTempalteNode:SetActive(false)

  self:InitSelfChooseNode()
  self:InitTab()

end

return LuaCarnivalCaiPiao2019Wnd
