local Extensions = import "Extensions"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local CTipParagraphItem = import "L10.UI.CTipParagraphItem"

LuaZongMenCreateIntroductionWnd = class()

RegistChildComponent(LuaZongMenCreateIntroductionWnd,"m_AniView","AniView", GameObject)
RegistChildComponent(LuaZongMenCreateIntroductionWnd,"m_IntroductionView","IntroductionView", GameObject)
RegistChildComponent(LuaZongMenCreateIntroductionWnd,"m_LetterFx","LetterFx", CUIFx)
RegistChildComponent(LuaZongMenCreateIntroductionWnd,"m_ReadMeButton","ReadMeButton", GameObject)
RegistChildComponent(LuaZongMenCreateIntroductionWnd,"m_CreateButton","CreateButton", GameObject)
RegistChildComponent(LuaZongMenCreateIntroductionWnd,"m_LeftConditionTemplate","LeftConditionTemplate", GameObject)
RegistChildComponent(LuaZongMenCreateIntroductionWnd,"m_LeftGrid","LeftGrid", UIGrid)
RegistChildComponent(LuaZongMenCreateIntroductionWnd,"m_RightConditionTemplate","RightConditionTemplate", GameObject)
RegistChildComponent(LuaZongMenCreateIntroductionWnd,"m_RightGrid","RightGrid", UIGrid)
RegistChildComponent(LuaZongMenCreateIntroductionWnd,"m_ChooseExpressionView","ChooseExpressionView", GameObject)
RegistChildComponent(LuaZongMenCreateIntroductionWnd,"m_ChooseExpressionSprite","ChooseExpressionSprite", GameObject)
RegistChildComponent(LuaZongMenCreateIntroductionWnd,"m_ExpressionViewIcon","ExpressionViewIcon", CUITexture)
RegistChildComponent(LuaZongMenCreateIntroductionWnd,"m_ExpressionNameLabel","ExpressionNameLabel", UILabel)
RegistChildComponent(LuaZongMenCreateIntroductionWnd,"m_BigFuxiExpressionNameLabel","BigFuxiExpressionNameLabel", UILabel)

RegistClassMember(LuaZongMenCreateIntroductionWnd, "m_WaitAniEndTick")
RegistClassMember(LuaZongMenCreateIntroductionWnd, "m_CheckExpression")

function LuaZongMenCreateIntroductionWnd:Init()
    self.m_LetterFx:LoadFx("fx/ui/prefab/ui_xinfendakai.prefab")
    self.m_AniView:SetActive(true)
    self.m_IntroductionView:SetActive(false)
    self.m_ChooseExpressionView:SetActive(false)
    self.m_ExpressionNameLabel.text = ""
    self.m_BigFuxiExpressionNameLabel.text = ""
    self.m_ExpressionViewIcon:LoadMaterial("")
    self.m_WaitAniEndTick = RegisterTickOnce(function ()
        self:ShowIntroductionView()
    end,2400)
    self:InitAccessionConditionsView()
    self:InitCreaterConditionsView()
    LuaZongMenMgr.m_CreateSectExpressionId = 0
    LuaZongMenMgr.m_CreateSectFuXiDanceMotionId = 0
end

function LuaZongMenCreateIntroductionWnd:ShowIntroductionView()
    self.m_IntroductionView:SetActive(true)
end

function LuaZongMenCreateIntroductionWnd:ReadMe()
    g_MessageMgr:ShowMessage("MenPai_AccessionConditions")
end

function LuaZongMenCreateIntroductionWnd:OnDisable()
    --LuaZongMenMgr.m_RuMenGuiZe = nil
end

function LuaZongMenCreateIntroductionWnd:CreateZongMen()
    if self.m_CheckExpression then
        if LuaZongMenMgr.m_CreateSectExpressionId == 0 and LuaZongMenMgr.m_CreateSectFuXiDanceMotionId == 0 then
            g_MessageMgr:ShowMessage("COMMON_EXPRESSION_SELECTION_WND_NO_SELECTION")
            return
        end
    end
    LuaZongMenMgr:OpenZongMenCreateWnd()
end

function LuaZongMenCreateIntroductionWnd:InitAccessionConditionsView()
    self.m_LeftConditionTemplate:SetActive(false)
    UIEventListener.Get(self.m_ReadMeButton).onClick = DelegateFactory.VoidDelegate(function (p)
        self:ReadMe()
    end)
    UIEventListener.Get(self.m_ExpressionViewIcon.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
        self:OnExpressionViewIconClicked()
    end)
    local list = LuaZongMenMgr.m_RuMenGuiZe
    Extensions.RemoveAllChildren(self.m_LeftGrid.transform)
    local indexTextArr = {LocalString.GetString("一"),LocalString.GetString("二"),LocalString.GetString("三"),
    LocalString.GetString("四"),LocalString.GetString("五"),LocalString.GetString("六"),LocalString.GetString("七")}
    if list then
        for i = 0,list.Count - 1 do
            local id = list[i]
            local text = Menpai_JoinStandard.GetData(id).Description
            local go = NGUITools.AddChild(self.m_LeftGrid.gameObject, self.m_LeftConditionTemplate)
            if id == tonumber(Menpai_Setting.GetData("CheckExpressionJoinStandard").Value) then
                self.m_CheckExpression = true
                self.m_ChooseExpressionView:SetActive(true)
            end
            go:SetActive(true)
            go.transform:Find("Label"):GetComponent(typeof(UILabel)).text = text
            go.transform:Find("IndexLabel"):GetComponent(typeof(UILabel)).text = indexTextArr[i + 1]
        end
    end
    self.m_LeftGrid:Reposition()
end

function LuaZongMenCreateIntroductionWnd:OnExpressionViewIconClicked()
    LuaExpressionMgr:ShowCommonExpressionSelectionWnd(LocalString.GetString( "请选择弟子入派动作"), true, false, function(data)
        LuaZongMenMgr.m_CreateSectExpressionId = 0
        LuaZongMenMgr.m_CreateSectFuXiDanceMotionId = 0
        local info = data.info
        self.m_ChooseExpressionSprite:SetActive(false)
        self.m_ExpressionViewIcon:LoadMaterial("")
        if data.isRegular then
            self.m_ExpressionViewIcon:LoadMaterial(info:GetIcon())	
            self.m_ExpressionViewIcon.gameObject:SetActive(true)
            LuaZongMenMgr.m_CreateSectExpressionId = info:GetID()
        else
            LuaZongMenMgr.m_CreateSectExpressionId = 47000714
            LuaZongMenMgr.m_CreateSectFuXiDanceMotionId = info.motionId
        end
        self.m_ExpressionNameLabel.text = data.isRegular and info:GetName() or info.motionName
        self.m_BigFuxiExpressionNameLabel.text = data.isRegular and "" or CommonDefs.StringAt(info.motionName,0)
    end)
end

function LuaZongMenCreateIntroductionWnd:InitCreaterConditionsView()
    self.m_RightConditionTemplate:SetActive(false)
    UIEventListener.Get(self.m_CreateButton).onClick = DelegateFactory.VoidDelegate(function (p)
        self:CreateZongMen()
    end)
    local msg = g_MessageMgr:FormatMessage("MenPai_CreaterConditions")
    local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(msg)
    Extensions.RemoveAllChildren(self.m_RightGrid.transform)
    for i = 0, info.paragraphs.Count - 1 do
        local paragraphGo = CUICommonDef.AddChild(self.m_RightGrid.gameObject, self.m_RightConditionTemplate)
        paragraphGo:SetActive(true)
        CommonDefs.GetComponent_GameObject_Type(paragraphGo, typeof(CTipParagraphItem)):Init(info.paragraphs[i], 255)
    end
    self.m_RightGrid:Reposition()
end