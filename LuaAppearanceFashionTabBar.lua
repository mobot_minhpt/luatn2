local CUITexture = import "L10.UI.CUITexture"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local Extensions = import "Extensions"
local CButton = import "L10.UI.CButton"
local UISprite = import "UISprite"

LuaAppearanceFashionTabBar = class()

RegistChildComponent(LuaAppearanceFashionTabBar,"m_TabRoot", "TabRoot", UIGrid)
RegistChildComponent(LuaAppearanceFashionTabBar,"m_TabTemplate", "TabTemplate", GameObject)

RegistClassMember(LuaAppearanceFashionTabBar, "m_SelectedIndex")
RegistClassMember(LuaAppearanceFashionTabBar, "m_OnTabChangeFunc")

function LuaAppearanceFashionTabBar:Awake()
    self.m_TabTemplate:SetActive(false)
end


function LuaAppearanceFashionTabBar:Init(info, onTabChangeFunc)
    self.m_SelectedIndex = -1
    self.m_OnTabChangeFunc = onTabChangeFunc
    local childCount = self.m_TabRoot.transform.childCount
    for i=0, childCount-1 do
        local childGo = self.m_TabRoot.transform:GetChild(i).gameObject
        childGo:SetActive(false)
    end

    for i=1,#info do
        local childGo = nil
        if i<=childCount then
            childGo = self.m_TabRoot.transform:GetChild(i-1).gameObject
        else
            childGo = CUICommonDef.AddChild(self.m_TabRoot.gameObject, self.m_TabTemplate)
        end
        childGo:SetActive(true)
        self:InitItem(childGo, i-1, info[i])
    end
    self.m_TabRoot:Reposition()
end

function LuaAppearanceFashionTabBar:GetSelectedIndex()
    return self.m_SelectedIndex
end

--切换选中的节点，index下标从0开始, ignoreCallback为true时不会有回调发生
function LuaAppearanceFashionTabBar:ChangeTab(index, ignoreCallback, isClick)
    if index<0 or index>=self.m_TabRoot.transform.childCount then
        return
    end
    local child = self.m_TabRoot.transform:GetChild(index)
    self:OnItemClick(child.gameObject, ignoreCallback, isClick)
end

function LuaAppearanceFashionTabBar:InitItem(itemGo, childIndex, info)
    local iconTexture = itemGo.transform:GetComponent(typeof(CUITexture))
    local selectedGo = itemGo.transform:Find("Selected").gameObject
    local cornerGo = itemGo.transform:Find("Corner").gameObject

    iconTexture:LoadMaterial(info.icon)
    selectedGo:SetActive(childIndex==self.m_SelectedIndex)
    cornerGo:SetActive(info.isInUse)
    CUICommonDef.SetGrey(itemGo, not info.isOwned)

    UIEventListener.Get(itemGo).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnItemClick(itemGo, false, true)
    end)
end

function LuaAppearanceFashionTabBar:OnItemClick(itemGo, ignoreCallback, isClick)
    local childCount = self.m_TabRoot.transform.childCount
    for i=0, childCount-1 do
        local childGo = self.m_TabRoot.transform:GetChild(i).gameObject
        if childGo == itemGo then
            childGo.transform:Find("Selected").gameObject:SetActive(true)
            self.m_SelectedIndex = i
            if not ignoreCallback and self.m_OnTabChangeFunc then
                self.m_OnTabChangeFunc(itemGo, self.m_SelectedIndex, isClick)
            end
        else
            childGo.transform:Find("Selected").gameObject:SetActive(false)
        end
    end
end