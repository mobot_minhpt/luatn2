-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Color = import "UnityEngine.Color"
local CommonDefs = import "L10.Game.CommonDefs"
local CTooltip = import "L10.UI.CTooltip"
local CZhaoHunWnd = import "L10.UI.CZhaoHunWnd"
local DelegateFactory = import "DelegateFactory"
local DuoHun_Setting = import "L10.Game.DuoHun_Setting"
local EnumEventType = import "EnumEventType"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local EventManager = import "EventManager"
local NGUITools = import "NGUITools"
local QnMoneyCostMesssageMgr = import "L10.UI.QnMoneyCostMesssageMgr"
local UIEventListener = import "UIEventListener"
local UISprite = import "UISprite"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CZhaoHunWnd.m_OnEnable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.zhaohunButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.zhaohunButton).onClick, MakeDelegateFromCSFunction(this.OnZhaoHunButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.challengeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.challengeButton).onClick, MakeDelegateFromCSFunction(this.OnChallengeButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.closeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeButton).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.taskDetailButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.taskDetailButton).onClick, MakeDelegateFromCSFunction(this.OnTaskDetailButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.backButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.backButton).onClick, MakeDelegateFromCSFunction(this.OnBackButtonClick, VoidDelegate, this), true)
    EventManager.AddListener(EnumEventType.OnZhaoHunTaskUpdate, MakeDelegateFromCSFunction(this.OnZhaoHunTaskUpdate, Action0, this))
end
CZhaoHunWnd.m_OnDisable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.zhaohunButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.zhaohunButton).onClick, MakeDelegateFromCSFunction(this.OnZhaoHunButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.challengeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.challengeButton).onClick, MakeDelegateFromCSFunction(this.OnChallengeButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.closeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeButton).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.taskDetailButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.taskDetailButton).onClick, MakeDelegateFromCSFunction(this.OnTaskDetailButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.backButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.backButton).onClick, MakeDelegateFromCSFunction(this.OnBackButtonClick, VoidDelegate, this), false)
    EventManager.RemoveListener(EnumEventType.OnZhaoHunTaskUpdate, MakeDelegateFromCSFunction(this.OnZhaoHunTaskUpdate, Action0, this))
end
CZhaoHunWnd.m_OnTaskDetailButtonClick_CS2LuaHook = function (this, go) 
    if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.PlayProp.ZhaoHunInfo ~= nil then
        local task = CClientMainPlayer.Inst.PlayProp.ZhaoHunInfo:GetCurrentTask()
        if task ~= nil then
            CTooltip.Show(task.TaskDetailDesc, go.transform, CTooltip.AlignType.Top)
        end
    end
end
CZhaoHunWnd.m_OnZhaoHunTaskUpdate_CS2LuaHook = function (this) 
    if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.PlayProp.ZhaoHunInfo ~= nil then
        local taskInfo = CClientMainPlayer.Inst.PlayProp.ZhaoHunInfo
        local currentTask = CClientMainPlayer.Inst.PlayProp.ZhaoHunInfo:GetCurrentTask()
        this.hunProgressLabel.text = System.String.Format("{0}/100", taskInfo.CurrentProgress)
        this.hunProgress.fillAmount = taskInfo.CurrentProgress / 100
        if currentTask ~= nil then
            this.zhaohunLight[currentTask.GuaXiangId - 1]:ResetToBeginning()
            this.zhaohunLight[currentTask.GuaXiangId - 1]:PlayForward()
            this.taskDesc.text = currentTask.TaskDesc
            this.taskDesc.gameObject:SetActive(true)
            this.taskTween:ResetToBeginning()
            this.taskTween:PlayForward()
            this.taskDetailButton:SetActive(currentTask.TaskType ~= 1)
        else
            this:CleanBaguaLight()
        end
    end
end
CZhaoHunWnd.m_CleanBaguaLight_CS2LuaHook = function (this) 
    this.taskDesc.gameObject:SetActive(false)
    do
        local i = 0
        while i < this.zhaohunLight.Length do
            this.zhaohunLight[i]:ResetToBeginning()
            this.shine[i]:ResetToBeginning()
            i = i + 1
        end
    end
end
CZhaoHunWnd.m_StartZhaoHun_CS2LuaHook = function (this, index) 
    this.taskIndex = index
    this:CleanBaguaLight()
    this.zhaohunSpeedUpDeltaTime = this.startSecond
    this.currentBaGuaIndex = 0
    this.zhaohunStatus = true
    this.currentConstantCount = 0
    this.constantCount = NGUITools.RandomRange(16, 31)
    this.zhaohunSlowDownDeltaTime = this.slowDownDeltaSecond
    this:InitBaGua()
end
CZhaoHunWnd.m_InitBaGua_CS2LuaHook = function (this) 
    for i = 0, 7 do
        local sprite = CommonDefs.GetComponent_GameObject_Type(this.shine[i].gameObject, typeof(UISprite))
        local lightSprite = CommonDefs.GetComponent_GameObject_Type(this.zhaohunLight[i].gameObject, typeof(UISprite))
        if sprite ~= nil then
            sprite.color = Color(sprite.color.r, sprite.color.g, sprite.color.b, 0)
        end
        if lightSprite ~= nil then
            lightSprite.color = Color(lightSprite.color.r, lightSprite.color.g, lightSprite.color.b, 0)
        end
    end
end
CZhaoHunWnd.m_OnRefreshZhaoHun_CS2LuaHook = function (this, index) 
    if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.PlayProp.ZhaoHunInfo ~= nil then
        local taskInfo = CClientMainPlayer.Inst.PlayProp.ZhaoHunInfo:GetCurrentTask()
        if taskInfo ~= nil and not taskInfo.IsFinished then
            local silverCost = DuoHun_Setting.GetData().ResetNormalTaskCost
            if index == 2 then
                silverCost = DuoHun_Setting.GetData().ResetAdvancedTaskCost
            end
            QnMoneyCostMesssageMgr.ShowMoneyCostMessageBox(EnumMoneyType.YinPiao, DuoHun_Setting.GetData().ResetZhaohunTast, silverCost, DelegateFactory.Action(function () 
                this:StartZhaoHun(index)
            end), nil, false, true, EnumPlayScoreKey.NONE, false)
            return
        end
    end
    this:StartZhaoHun(index)
end
