-- Auto Generated!!
local AlignType = import "L10.UI.CTooltip+AlignType"
local AutoDrugWndMgr = import "L10.UI.AutoDrugWndMgr"
local CButton = import "L10.UI.CButton"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CDrugItemList = import "L10.UI.CDrugItemList"
local CDrugItemListMgr = import "L10.UI.CDrugItemListMgr"
local CDrugItemTemplate = import "L10.UI.CDrugItemTemplate"
local CGameSettingMgr = import "L10.Game.CGameSettingMgr"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CLogMgr = import "L10.CLogMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local DelegateFactory = import "DelegateFactory"
local Extensions = import "Extensions"
local Gac2Gas = import "L10.Game.Gac2Gas"
local GameObject = import "UnityEngine.GameObject"
local GameSetting_Common_Wapper = import "L10.Game.GameSetting_Common_Wapper"
local Item_Item = import "L10.Game.Item_Item"
local L10 = import "L10"
local LocalString = import "LocalString"
local NGUITools = import "NGUITools"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local VoidDelegate = import "UIEventListener+VoidDelegate"

LuaDrugItemList = {}
function LuaDrugItemList:PressItemNode(node,isPressed,this,item,scrollview)
  if self.m_Tick then
    UnRegisterTick(self.m_Tick)
  end
  if isPressed then
    self.localYSave = scrollview.transform.localPosition.y
    local fxTime = 200
    if this and node then
      self.m_Tick = RegisterTickWithDuration(function ()
        local newY = scrollview.transform.localPosition.y
        if self.localYSave then
          if math.abs(self.localYSave - newY) < 3 then
            if not CDrugItemListMgr.isFood then
              CUIManager.CloseUI(CUIResources.AutoDrugItemList)
            end
            CItemAccessListMgr.Inst:ShowItemAccessInfo(item.ID, true, this.anchor, AlignType.Right)
          end
        end
      end,fxTime,fxTime)
    end
  end
end

CDrugItemList.m_Init_CS2LuaHook = function (this)
    --throw new System.NotImplementedException();
    --throw new System.NotImplementedException();
    if CDrugItemListMgr.isFood then
        this.anchor.localPosition = this.FoodEquipPosition
    else
        this.anchor.localPosition = this.DrugEquipPosition
    end
    this:InitList(CDrugItemListMgr.isHp, CDrugItemListMgr.isFood)
end
CDrugItemList.m_InitList_CS2LuaHook = function (this, isHP, isFood)
    local itemList = nil
    CommonDefs.ListClear(this.drugs)
    local haveList = CreateFromClass(MakeGenericClass(List, UInt32))
    local havenoList = CreateFromClass(MakeGenericClass(List, UInt32))

    if isFood then
        local default
        if isHP then
            default = CGameSettingMgr.Inst.hpFoodId
        else
            default = CGameSettingMgr.Inst.mpFoodId
        end
        itemList = default
        local sortList = InitializeListWithArray(CreateFromClass(MakeGenericClass(List, UInt32)), UInt32, itemList)
        this:AddDrugsToList(sortList, haveList, havenoList)
    else
        itemList = GameSetting_Common_Wapper.Inst.BigDrugs
        local sortList = InitializeListWithArray(CreateFromClass(MakeGenericClass(List, UInt32)), UInt32, itemList)
        this:AddDrugsToList(sortList, haveList, havenoList)

        itemList = GameSetting_Common_Wapper.Inst.HpDrugs
        sortList = InitializeListWithArray(CreateFromClass(MakeGenericClass(List, UInt32)), UInt32, itemList)
        this:AddDrugsToList(sortList, haveList, havenoList)

        itemList = GameSetting_Common_Wapper.Inst.MpDrugs
        sortList = InitializeListWithArray(CreateFromClass(MakeGenericClass(List, UInt32)), UInt32, itemList)
        this:AddDrugsToList(sortList, haveList, havenoList)
    end
    do
        local i = 0
        while i < haveList.Count do
            local item = CItemMgr.Inst:GetItemTemplate(haveList[i])
            if item ~= nil and CLuaDesignMgr.Item_Item_GetAdjustedGradeCheck(item) <= CClientMainPlayer.Inst.Level then
                CommonDefs.ListAdd(this.drugs, typeof(Item_Item), item)
            end
            i = i + 1
        end
    end
    do
        local i = 0
        while i < havenoList.Count do
            local item = CItemMgr.Inst:GetItemTemplate(havenoList[i])
            if item ~= nil and CLuaDesignMgr.Item_Item_GetAdjustedGradeCheck(item) <= CClientMainPlayer.Inst.Level then
                CommonDefs.ListAdd(this.drugs, typeof(Item_Item), item)
            end
            i = i + 1
        end
    end


    CommonDefs.ListClear(this.ListItems)
    Extensions.RemoveAllChildren(this.DrugScroll.transform)

    do
        local i = 0
        while i < this.drugs.Count do
            local DrugItemUnit = NGUITools.AddChild(this.DrugScroll.gameObject, this.DrugItemTemplate)
            DrugItemUnit:SetActive(true)
            CommonDefs.GetComponent_GameObject_Type(DrugItemUnit, typeof(CDrugItemTemplate)):InitItemInfo(this.drugs[i])

            CommonDefs.ListAdd(this.ListItems, typeof(GameObject), DrugItemUnit)
            UIEventListener.Get(DrugItemUnit).onClick = MakeDelegateFromCSFunction(this.OnItemClick, VoidDelegate, this)
            local sc = this
            local item = this.drugs[i]
            local drag = this.DrugScroll
            local scrollview = this.DrugScroll
            UIEventListener.Get(DrugItemUnit).onPress = LuaUtils.BoolDelegate(function (go, isPressed)
              LuaDrugItemList:PressItemNode(go, isPressed, sc, item, scrollview)
            end)

            i = i + 1
        end
    end

    this.Grid:Reposition()
    this.DrugScroll:ResetPosition()
end
CDrugItemList.m_AddDrugsToList_CS2LuaHook = function (this, sortList, havelist, havenoList)
    local t1 = CreateFromClass(MakeGenericClass(List, UInt32))
    local t2 = CreateFromClass(MakeGenericClass(List, UInt32))

    do
        local i = 0
        while i < sortList.Count do
            local bindCount, notbindCount
            local id = sortList[i]
            bindCount, notbindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(id)
            local item1 = CItemMgr.Inst:GetItemTemplate(id)
            if bindCount + notbindCount > 0 then
                CommonDefs.ListAdd(t1, typeof(UInt32), id)
            else
                CommonDefs.ListAdd(t2, typeof(UInt32), id)
            end
            i = i + 1
        end
    end
    CommonDefs.ListSort1(t1, typeof(UInt32), DelegateFactory.Comparison_uint(function (x, y)
        return - NumberCompareTo(x, y)
    end))
    CommonDefs.ListAddRange(havelist, t1)
    CommonDefs.ListSort1(t2, typeof(UInt32), DelegateFactory.Comparison_uint(function (x, y)
        return - NumberCompareTo(x, y)
    end))
    CommonDefs.ListAddRange(havenoList, t2)
end
CDrugItemList.m_OnItemClick_CS2LuaHook = function (this, go)
    do
        local i = 0
        while i < this.ListItems.Count do
            CommonDefs.GetComponent_GameObject_Type(this.ListItems[i], typeof(CButton)).Selected = go == this.ListItems[i]
            i = i + 1
        end
    end
    local item = CommonDefs.GetComponent_GameObject_Type(go, typeof(CDrugItemTemplate)).Item
    --Item_Item item = CItemMgr.Inst.GetItemTemplate(itemInfo.templateId);
    local bindCount, notBindCount
    bindCount, notBindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(item.ID)
    if bindCount + notBindCount <= 0 then
        if not CDrugItemListMgr.isFood then
            CUIManager.CloseUI(CUIResources.AutoDrugItemList)
        end
        CItemAccessListMgr.Inst:ShowItemAccessInfo(item.ID, true, this.anchor, AlignType.Right)
        --CItemAccessListMgr.Inst.DisplayDiamondAccess(false, anchor);
    else
        CUIManager.CloseUI(CUIResources.AutoDrugItemList)
        if not CDrugItemListMgr.isFood then
            --        CCommonItem drug = CItemMgr.Inst.GetById(item.ID);
            --         if (drug == null)
            --            return;
            Gac2Gas.SetHpDrugShortcut(item.ID)
        else
            if CDrugItemListMgr.isHp then
                if AutoDrugWndMgr.CurrentMpFood ~= nil then
                    AutoDrugWndMgr.SetCurrentFood(item.ID, AutoDrugWndMgr.CurrentMpFood.ID)
                else
                    AutoDrugWndMgr.SetCurrentFood(item.ID, 0)
                end
            else
                if AutoDrugWndMgr.CurrentHpFood ~= nil then
                    AutoDrugWndMgr.SetCurrentFood(AutoDrugWndMgr.CurrentHpFood.ID, item.ID)
                else
                    AutoDrugWndMgr.SetCurrentFood(0, item.ID)
                end
            end
        end
    end
end
CDrugItemList.m_GetItem_CS2LuaHook = function (this)
    if this.ListItems.Count > 0 then
        return this.ListItems[0]
    else
        L10.Game.Guide.CGuideMgr.Inst:EndCurrentPhase()
    end
    return nil
end
CDrugItemList.m_GetGuideGo_CS2LuaHook = function (this, methodName)
    if methodName == "GetItem" then
        return this:GetItem()
    else
        CLogMgr.LogError(LocalString.GetString("引导数据表填写错误"))
        return nil
    end
end
