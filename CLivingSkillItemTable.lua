-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CLivingItem = import "L10.UI.CLivingItem"
local CLivingSkillItemSection = import "L10.UI.CLivingSkillItemSection"
local CLivingSkillItemTable = import "L10.UI.CLivingSkillItemTable"
local CLivingSkillMgr = import "L10.Game.CLivingSkillMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local SkillEntry = import "L10.Game.CLivingSkillMgr+SkillEntry"
local UInt32 = import "System.UInt32"
local Vector3 = import "UnityEngine.Vector3"
CLivingSkillItemTable.m_Init_CS2LuaHook = function (this) 

    local entry = CommonDefs.ListFind(CLivingSkillMgr.Inst.entrys, typeof(SkillEntry), DelegateFactory.Predicate_SkillEntry(function (p) 
        return p.skillTypeId == CLivingSkillMgr.Inst.selectedSkill
    end))
    if entry ~= nil then
        this.sections = entry.sections
        this:InitList()
    end
end
CLivingSkillItemTable.m_RecycleAll_CS2LuaHook = function (this) 
    for i = this.table.transform.childCount - 1, 0, - 1 do
        local go = this.table.transform:GetChild(i).gameObject
        local cmp = CommonDefs.GetComponent_GameObject_Type(go, typeof(CLivingSkillItemSection))
        cmp:Recycle()
        this.pool:Recycle(go)
    end
end
CLivingSkillItemTable.m_InitList_CS2LuaHook = function (this) 
    this:RecycleAll()
    do
        local i = 0
        while i < this.sections.Count do
            local section = this.sections[i]
            --GameObject go = GameObject.Instantiate(sectionPrefab) as GameObject;
            local go = this.pool:GetFromPool(0)
            --go.name = "Section" + i;
            go.transform.parent = this.table.transform
            go.transform.localScale = Vector3.one
            go:SetActive(true)
            local cmp = CommonDefs.GetComponent_GameObject_Type(go, typeof(CLivingSkillItemSection))
            cmp.callback = this.onSelected
            cmp:Init(section)
            i = i + 1
        end
    end
    if this.table.transform.childCount <= CLivingSkillMgr.Inst.sectionIndex then
        CLivingSkillMgr.Inst.sectionIndex = 0
    end
    if CLivingSkillMgr.Inst.useItemTemplateId > 0 or CLivingSkillMgr.Inst.targetItemTemplateId > 0 then
        --说明从背包点过来的
        this:SelectDefaultItem()
    else
        this:TrySelectMaxLevelItem()
    end

    this.table:Reposition()
    this.scrollView:ResetPosition()
end
CLivingSkillItemTable.m_SelectDefaultItem_CS2LuaHook = function (this) 
    if this.table.transform.childCount > CLivingSkillMgr.Inst.sectionIndex then
        local tf = this.table.transform:GetChild(CLivingSkillMgr.Inst.sectionIndex)
        if tf ~= nil then
            local cmp = CommonDefs.GetComponent_Component_Type(tf, typeof(CLivingSkillItemSection))
            cmp:SelectItem(CLivingSkillMgr.Inst.rowIndex)
        end
    end
end
CLivingSkillItemTable.m_TrySelectMaxLevelItem_CS2LuaHook = function (this) 
    if this.sections.Count == 0 then
        return
    end
    if CLivingSkillItemTable.Patch_DefaultSelect then
        this:SelectDefaultItem()
        return
    end
    local section = this.sections[0]
    local index = 0
    do
        local i = 0
        while i < section.itemIds.Count do
            local item = section.itemIds[i]
            local data = section:GetItem(item)
            if CommonDefs.DictContains(CClientMainPlayer.Inst.SkillProp.LifeSkillGradeMap, typeof(UInt32), CLivingSkillMgr.Inst.selectedSkill) then
                local skillLevel = CommonDefs.DictGetValue(CClientMainPlayer.Inst.SkillProp.LifeSkillGradeMap, typeof(UInt32), CLivingSkillMgr.Inst.selectedSkill)
                if data.SkillLev > skillLevel then
                    break
                else
                    index = i
                end
            end
            i = i + 1
        end
    end
    local tf = this.table.transform:GetChild(0)
    if tf ~= nil then
        local cmp = CommonDefs.GetComponent_Component_Type(tf, typeof(CLivingSkillItemSection))
        cmp:SelectItem(index)
    end
end
CLivingSkillItemTable.m_GoTo_CS2LuaHook = function (this, section, row) 
    if this.table.transform.childCount > section then
        local tf = this.table.transform:GetChild(section)
        if tf ~= nil then
            local cmp = CommonDefs.GetComponent_Component_Type(tf, typeof(CLivingSkillItemSection))
            cmp:SelectItem(row)
        end
    end
end
CLivingSkillItemTable.m_UpdateSkillLevel_CS2LuaHook = function (this) 
    local cmps = CommonDefs.GetComponentsInChildren_GameObject_Type_Boolean(this.gameObject, typeof(CLivingItem), true)
    CommonDefs.EnumerableIterate(cmps, DelegateFactory.Action_object(function (___value) 
        local cmp = ___value
        cmp:RefreshMaskState()
    end))
end
