local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local Animation = import "UnityEngine.Animation"

LuaSnowManResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaSnowManResultWnd, "WinStyle", "WinStyle", GameObject)
RegistChildComponent(LuaSnowManResultWnd, "LoseStyle", "LoseStyle", GameObject)

--@endregion RegistChildComponent end

RegistClassMember(LuaSnowManResultWnd, "m_Anim")
function LuaSnowManResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.m_Anim = self.gameObject:GetComponent(typeof(Animation))
end

function LuaSnowManResultWnd:Init()
    self.WinStyle:SetActive(LuaHanJia2024Mgr.SnowManData.IsWin)
    self.LoseStyle:SetActive(not LuaHanJia2024Mgr.SnowManData.IsWin)
    if LuaHanJia2024Mgr.SnowManData.IsWin then
        self:InitWinStyle()
    else
        self:InitLossStyle()
    end
end

function LuaSnowManResultWnd:InitWinStyle()
    local trans = self.WinStyle.transform
    local label = FindChildWithType(trans, "Win_Info/Mode_Label", typeof(UILabel))
    label.text = self:GetLevelName()

    local go = FindChildWithType(trans, "Win_Info/NoReward_Label", typeof(GameObject))

    local setting = HanJia2024_Setting.GetData()
    local data = LuaHanJia2024Mgr.SnowManData

    if data.Rewarded then
        go:SetActive(true)
    else
        go:SetActive(false)
        local grid = FindChildWithType(trans, "Win_Info/RewardGrid", typeof(UIGrid))
        local tempctrl = FindChildWithType(trans, "Win_Info/ItemTemplate", typeof(GameObject))
        local rewards = setting.SnowManWndTaskRewards
        self:InitRewards(rewards, grid, tempctrl)
    end

    local normalgo = FindChildWithType(trans, "Normal_Bg", typeof(GameObject))
    local hardgo = FindChildWithType(trans, "Hard_Bg", typeof(GameObject))
    normalgo:SetActive(data.Type == 1)
    hardgo:SetActive(data.Type == 2)

    self.m_Anim:Play("snowmanresultwnd_win")
end

function LuaSnowManResultWnd:InitRewards(rewardtbl, grid, tempctrl)
    for i = 0, rewardtbl.Length - 1 do
        local data = rewardtbl[i]
        local ctrlgo = NGUITools.AddChild(grid.gameObject, tempctrl)
        self:InitReward(data, ctrlgo)
        ctrlgo:SetActive(true)
    end
    grid:Reposition()
end

function LuaSnowManResultWnd:InitReward(data, ctrlgo)
    local trans = ctrlgo.transform
    local qualitylb = FindChildWithType(trans, "QualitySprite", typeof(UISprite))
    local icon = FindChildWithType(trans, "IconTexture", typeof(CUITexture))

    local cfg = Item_Item.GetData(data.TempID)
    if cfg == nil then
        return
    end

    qualitylb.spriteName = CUICommonDef.GetItemCellBorder(cfg.NameColor)
    icon:LoadMaterial(cfg.Icon)

    UIEventListener.Get(ctrlgo).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            CItemInfoMgr.ShowLinkItemTemplateInfo(data.TempID, false, nil, AlignType.Default, 0, 0, 0, 0)
        end
    )
end

function LuaSnowManResultWnd:InitLossStyle()
    local label = FindChildWithType(self.LoseStyle.transform, "Lose_Info/Mode_Label", typeof(UILabel))
    label.text = self:GetLevelName()
    self.m_Anim:Play("common_result_lose")
end

function LuaSnowManResultWnd:GetLevelName()
    local type = LuaHanJia2024Mgr.SnowManData.Type
    local colorstring = type == 1 and "ffffff" or "fdb1f1"
    local typestring = type == 1 and LocalString.GetString("普通") or LocalString.GetString("困难")
    local gamename = LocalString.GetString("不甘心的雪人")
    return SafeStringFormat3("[%s]%s-%s[-]", colorstring, gamename, typestring)
end

--@region UIEvent

--@endregion UIEvent
