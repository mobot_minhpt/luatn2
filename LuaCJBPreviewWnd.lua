-- local UIEventListener = import "UIEventListener"
local Object = import "UnityEngine.Object"
-- local DelegateFactory = import "DelegateFactory"
local CSwitchMgr = import "L10.Engine.CSwitchMgr"
local CPlayerShopMgr = import "L10.UI.CPlayerShopMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CCJBRenderObject = import "L10.Game.CCJBRenderObject"
local CCJBMgr = import "L10.Game.CCJBMgr"
local CARMgr = import "L10.Game.CARMgr"


CLuaCJBPreviewWnd = class()
RegistClassMember(CLuaCJBPreviewWnd,"uiTexture")
RegistClassMember(CLuaCJBPreviewWnd,"ArBtn")
RegistClassMember(CLuaCJBPreviewWnd,"ShareBtn")
RegistClassMember(CLuaCJBPreviewWnd,"_showNode")
CLuaCJBPreviewWnd.m_WashedStar = 0
CLuaCJBPreviewWnd.m_Star = 0

function CLuaCJBPreviewWnd:Awake()
    self.uiTexture = self.transform:Find("Anchor/QnModelPreviewer/Model/FakeModel/Texture"):GetComponent(typeof(UITexture))
    self.ArBtn = self.transform:Find("Anchor/QnModelPreviewer/ARButton").gameObject
    self.ShareBtn = self.transform:Find("Anchor/QnModelPreviewer/ShareButton").gameObject

    self.transform:Find("Anchor/StarNode").gameObject:SetActive(false)

end

function CLuaCJBPreviewWnd:Init( )

    if self._showNode ~= nil then
        Object.Destroy(self._showNode)
    end

    if CCJBMgr.Inst.ItemSelectType == "IdShow" then
        local id = CCJBMgr.Inst.ItemSelectId
        if id == nil or id == "" then
            CUIManager.CloseUI(CUIResources.CJBPreviewWnd)
            return
        end

        self._showNode = CCJBMgr.Inst:LoadCJBRenderModel(self.gameObject, id, self.uiTexture.width, self.uiTexture.height, nil, true)
    elseif CCJBMgr.Inst.ItemSelectType == "DataShow" then
        --从包裹那里点进来
        local id = CCJBMgr.Inst.ItemSelectId
        if id then
            local star = CLuaCJBPreviewWnd.m_Star
            local washedStar = CLuaCJBPreviewWnd.m_WashedStar or 0
            local hasWashResult = washedStar>0
            if hasWashResult then
                local node = self.transform:Find("Anchor/StarNode")
                node.gameObject:SetActive(true)
                local starItem = node:Find("Star").gameObject
                starItem:SetActive(false)
                local grid1 = node:Find("StarGrid").gameObject
                local grid2 = node:Find("StarGrid2").gameObject

                local c2 = ChuanjiabaoModel_Setting.GetData().Chuanjiabao_Total_Score
                for i=1,c2 do
                    local g = NGUITools.AddChild(grid1,starItem)
                    g:SetActive(true)

                    local sprite = g:GetComponent(typeof(UISprite))
                    if i<=star then
                        sprite.spriteName = "playerAchievementView_star_1"
                    else
                        sprite.spriteName = "playerAchievementView_star_2"
                    end
                    ---------------------------------------------------------
                    local g = NGUITools.AddChild(grid2,starItem)
                    g:SetActive(true)
                    local sprite = g:GetComponent(typeof(UISprite))
                    if i<=washedStar then
                        sprite.spriteName = "playerAchievementView_star_1"
                    else
                        sprite.spriteName = "playerAchievementView_star_2"
                    end
                end
                grid1:GetComponent(typeof(UIGrid)):Reposition()
                grid2:GetComponent(typeof(UIGrid)):Reposition()
            end
        end

        self._showNode = CCJBMgr.Inst:LoadCJBRenderModelByData(self.gameObject, CCJBMgr.Inst.ItemSelectData, self.uiTexture.width, self.uiTexture.height, nil, true)
    end

    self:CheckArBtn()

    if CCJBMgr.Inst.ItemShowShareBtn and CSwitchMgr.EnableCJBShare then
        self.ShareBtn:SetActive(true)
        UIEventListener.Get(self.ShareBtn).onClick = DelegateFactory.VoidDelegate(function () 
            self:ShareCJB()
        end)
    else
        self.ShareBtn:SetActive(false)
    end
end
function CLuaCJBPreviewWnd:CheckArBtn( )
    if CARMgr.Inst.EnableAR and CARMgr.Inst:IsPlatformSupported() and not System.String.IsNullOrEmpty(CCJBMgr.Inst.ItemSelectId) then
        local appearInfoInPlayer = CCJBMgr.Inst:GetCJBItemInfo(CCJBMgr.Inst.ItemSelectId)

        local item = CItemMgr.Inst:GetById(CCJBMgr.Inst.ItemSelectId)
        if item == nil then
            local _item = CPlayerShopMgr.Inst:GetItemById(CCJBMgr.Inst.ItemSelectId)
            if _item ~= nil then
                item = _item.Item
            end
        end
        if not (item ~= nil and item.Item.OwnerId == CClientMainPlayer.Inst.BasicProp.Id) then
            self.ArBtn:SetActive(false)
            return
        end

        if appearInfoInPlayer ~= nil then
            self.ArBtn:SetActive(true)
            UIEventListener.Get(self.ArBtn).onClick = DelegateFactory.VoidDelegate(function () 
                self:ArFunction()
            end)
        else
            self.ArBtn:SetActive(false)
        end
    else
        self.ArBtn:SetActive(false)
    end
end
function CLuaCJBPreviewWnd:ShareCJB( )
    if self._showNode ~= nil then
        local showData = CommonDefs.GetComponent_GameObject_Type(self._showNode, typeof(CCJBRenderObject)).showData
        if showData ~= nil then
            showData:ShareCJB()
        end
    end
end


function CLuaCJBPreviewWnd:OnEnable()
    CUIManager.ModelLightEnabled = false

end
function CLuaCJBPreviewWnd:OnDisable()
    CUIManager.ModelLightEnabled = true

end

function CLuaCJBPreviewWnd:ArFunction()
    CARMgr.Inst:ShowChuanJiaBao(CCJBMgr.Inst.ItemSelectId)
end

function CLuaCJBPreviewWnd:OnDestroy()
    CLuaCJBPreviewWnd.m_WashedStar = 0
    CLuaCJBPreviewWnd.m_Star = 0
end