local BoxCollider = import "UnityEngine.BoxCollider"
local CChatLinkMgr = import "CChatLinkMgr"
local CChatMgr = import "L10.Game.CChatMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CIMMgr = import "L10.Game.CIMMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CPersonalSpaceDetailMoment = import "L10.UI.CPersonalSpaceDetailMoment"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CUIManager = import "L10.UI.CUIManager"
local CUITexture = import "L10.UI.CUITexture"
local CWebBrowserMgr = import "L10.Game.CWebBrowserMgr"
local DelegateFactory = import "DelegateFactory"
local Extensions = import "Extensions"
local GameObject = import "UnityEngine.GameObject"
local L10 = import "L10"
local LocalString = import "LocalString"
local NGUIText = import "NGUIText"
local NGUITools = import "NGUITools"
local Object1 = import "UnityEngine.Object"
local String = import "System.String"
local UICamera = import "UICamera"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local UIPanel = import "UIPanel"
local UIScrollView = import "UIScrollView"
local UISprite = import "UISprite"
local UITable = import "UITable"
local UITexture = import "UITexture"
local Vector2 = import "UnityEngine.Vector2"
local Vector3 = import "UnityEngine.Vector3"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local CPersonalSpaceWnd = import "L10.UI.CPersonalSpaceWnd"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local WWW = import "UnityEngine.WWW"
local CSpeechCtrlMgr = import "L10.Game.CSpeechCtrlMgr"
local CSpeechCtrlType = import "L10.Game.CSpeechCtrlType"


LuaPersonalSpaceHotTalkWnd=class()
RegistChildComponent(LuaPersonalSpaceHotTalkWnd,"qaBtn", QnAddSubAndInputButton)
RegistChildComponent(LuaPersonalSpaceHotTalkWnd,"scrollView", UIScrollView)
RegistChildComponent(LuaPersonalSpaceHotTalkWnd,"table", UITable)
RegistChildComponent(LuaPersonalSpaceHotTalkWnd,"emptyNode", GameObject)
RegistChildComponent(LuaPersonalSpaceHotTalkWnd,"templateNode", GameObject)
RegistChildComponent(LuaPersonalSpaceHotTalkWnd,"topicTable", GameObject)
RegistChildComponent(LuaPersonalSpaceHotTalkWnd,"DetailMoment", GameObject)
RegistChildComponent(LuaPersonalSpaceHotTalkWnd,"FriendsCircle", GameObject)
RegistChildComponent(LuaPersonalSpaceHotTalkWnd,"EmptyNode", GameObject)
RegistChildComponent(LuaPersonalSpaceHotTalkWnd,"sortTemplate", GameObject)

RegistClassMember(LuaPersonalSpaceHotTalkWnd, "m_Panel")
RegistClassMember(LuaPersonalSpaceHotTalkWnd, "m_TopicId")
RegistClassMember(LuaPersonalSpaceHotTalkWnd, "m_PageNum")
RegistClassMember(LuaPersonalSpaceHotTalkWnd, "m_SortType")
RegistClassMember(LuaPersonalSpaceHotTalkWnd, "sortNameTable")

function LuaPersonalSpaceHotTalkWnd:Init()
  self.templateNode:SetActive(false)
  self.m_Panel = self.scrollView.transform:GetComponent(typeof(UIPanel))
  self.DetailMoment:SetActive(false)
  self.EmptyNode:SetActive(false)
  self.FriendsCircle:SetActive(false)
  self.sortTemplate:SetActive(false)
  self.sortNameTable = {{type = 'hot',name = LocalString.GetString('热度')},{type = 'new',name = LocalString.GetString('时间')},{type = 'image',name = LocalString.GetString('带图')},{type = 'follow',name = LocalString.GetString('关注')}}
  LuaPersonalSpaceMgrReal.GetPersonalSpaceHotTalkTitle()
end

function LuaPersonalSpaceHotTalkWnd:ShowMomentDetailThroughFriendCircle(info,showTalk)
  LuaPersonalSpaceMgrReal.GetSingleMomentInfo(info.id, function (ret)
    if not self or not self.templateNode then
      return
    end
    if ret.code == 0 then
      local newItem = ret.data

      local formerNode = self.FriendsCircle
      local detailScript = self.DetailMoment:GetComponent(typeof(CCommonLuaScript))
      formerNode:SetActive(false)
      self.DetailMoment:SetActive(true)
      detailScript:Init({newItem,showTalk,formerNode})
    end
  end)

end

function LuaPersonalSpaceHotTalkWnd:InitTopicTableDetail(nodeIndex,nodeTable)
  local data = LuaPersonalSpaceMgrReal.ShowInfoTopic[nodeIndex]
  if not data and self.m_TopicId then
    self.m_SortType = 'hot'
    self.m_PageNum = 1
    self.topicTable:GetComponent(typeof(UITable)):Reposition()
    self.qaBtn.onValueChanged = DelegateFactory.Action_uint(function (value)
      self.m_PageNum = value
      LuaPersonalSpaceMgrReal.GetPersonalSpaceHotTalkInfo(self.m_TopicId,self.m_SortType,self.m_PageNum)
    end)
    self.qaBtn:SetValue(1, true)
    return
  end

  local node = nodeTable[nodeIndex]
  if not node or not data then
    return
  end

  for i,v in ipairs(nodeTable) do
    local sdata = LuaPersonalSpaceMgrReal.ShowInfoTopic[i]
    if sdata then
      local arrow = v:Find('TitleBg/Sprite')
      if nodeIndex == i then
        arrow.localEulerAngles = Vector3.zero
      else
        arrow.localEulerAngles = Vector3(0,0,180)
      end
      local picRootNode = v:Find('Root').gameObject
      if nodeIndex == i then
        picRootNode:SetActive(true)
      else
        picRootNode:SetActive(false)
      end

      local btn = v:Find('Root/pic/Sprite/bg').gameObject

      local clickFunction = function(go)
        local shareText = '<link button=' .. sdata.Name .. ','.. LuaPersonalSpaceMgrReal.HotTalkBtnName .. ','.. sdata.ID ..'>'
        if self:CheckHotTalk(sdata.Name, sdata.ID) and 
          CSpeechCtrlMgr.Inst:CheckIfNeedSpeechCtrl(CSpeechCtrlType.Type_PersonalSpace_HotTalk_Please_Moveto_Website, true) then
          return
        end
        CPersonalSpaceMgr.Inst:ShareTextToPersonalSpace(shareText,nil)
      end
      CommonDefs.AddOnClickListener(btn,DelegateFactory.Action_GameObject(clickFunction),false)

      local moreDetailBtn = v:Find('Root/pic/moreDetail').gameObject
      if sdata.Url and sdata.Url ~= '' then
        moreDetailBtn:SetActive(true)
        local clickMoreFunction = function(go)
          CWebBrowserMgr.Inst:OpenUrl(self:GenActivityUrl(sdata.Url))
        end
        CommonDefs.AddOnClickListener(moreDetailBtn,DelegateFactory.Action_GameObject(clickMoreFunction),false)
      else
        moreDetailBtn:SetActive(false)
      end
    end
  end

  local picNode = node:Find('Root/pic').gameObject

  self:SetNormalPic(picNode,data.Banner)
  self.m_TopicId = data.ID
  self.m_SortType = 'hot'
  self.m_PageNum = 1
  self.topicTable:GetComponent(typeof(UITable)):Reposition()

  self.qaBtn.onValueChanged = DelegateFactory.Action_uint(function (value)
    self.m_PageNum = value
    LuaPersonalSpaceMgrReal.GetPersonalSpaceHotTalkInfo(self.m_TopicId,self.m_SortType,self.m_PageNum)
  end)
  self.qaBtn:SetValue(1, true)
end

function LuaPersonalSpaceHotTalkWnd:GenActivityUrl(url)
  local tUrl = 'https://md-test.163.com/qnm/activity/login?redirect_url=%s&roleid=%s&skey=%s'
  if CommonDefs.IS_PUB_RELEASE then
    tUrl = 'https://api.hi.163.com/qnm/activity/login?redirect_url=%s&roleid=%s&skey=%s'
  end
  local returnUrl = SafeStringFormat3(tUrl,WWW.EscapeURL(url),CClientMainPlayer.Inst.Id,CPersonalSpaceMgr.Inst.Token)
  return returnUrl
end

function LuaPersonalSpaceHotTalkWnd:CheckHotTalk(name, id)
  return name == LocalString.GetString("仙宗风华") and id == 68
end

function LuaPersonalSpaceHotTalkWnd:InitTopicTable()
  if not LuaPersonalSpaceMgrReal.ShowInfoTopic then
    return
  end

  local topicNodeTable = {self.topicTable.transform:Find('word1'),self.topicTable.transform:Find('word2'),self.topicTable.transform:Find('word3'),self.topicTable.transform:Find('word4')}
  local defaultId = LuaPersonalSpaceMgrReal.ShowInfoTopicDefaultId
  local defaultIndex = 0
  if not defaultId and LuaPersonalSpaceMgrReal.ShowInfoTopic[1] then
     defaultId = LuaPersonalSpaceMgrReal.ShowInfoTopic[1].ID
  end
  for i,v in ipairs(topicNodeTable) do
    local data = LuaPersonalSpaceMgrReal.ShowInfoTopic[i]
    if data then
      local titleNode = v:Find('TitleBg/Label')
      local arrow = v:Find('TitleBg/Sprite')
      arrow.localEulerAngles = Vector3(0,0,180)
      local picRootNode = v:Find('Root').gameObject
      picRootNode:SetActive(false)
      local btn = v:Find('Root/pic/Sprite/bg').gameObject
      local topicBtn = v:Find('TitleBg').gameObject

      local desNode = v:Find('Root/pic/Sprite/Sprite/des')
      titleNode:GetComponent(typeof(UILabel)).text = data.Name
      desNode:GetComponent(typeof(UILabel)).text = data.Desc
      if defaultId and defaultId == data.ID then
        defaultIndex = i
      end

      UIEventListener.Get(topicBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        self:InitTopicTableDetail(i,topicNodeTable)
      end)
    end
  end

  if defaultIndex == 0 then
    self.m_TopicId = defaultId
  end
  self:InitTopicTableDetail(defaultIndex,topicNodeTable)
  LuaPersonalSpaceMgrReal.ShowInfoTopicDefaultId = nil
end

function LuaPersonalSpaceHotTalkWnd:CleanBtn(btn)
  UIEventListener.Get(btn).onClick = nil
end
function LuaPersonalSpaceHotTalkWnd:TableToArray(commonItems)
  if commonItems then
    return CommonDefs.ListToArray(CommonDefs.ListGetRange(commonItems, 1, commonItems.Count - 1))
  end
end

function LuaPersonalSpaceHotTalkWnd:SetNormalPic(node, picPath)
  local uiTex = CommonDefs.GetComponent_GameObject_Type(node, typeof(UITexture))
  if not uiTex or not picPath then
    return
  end

  CPersonalSpaceMgr.DownLoadPic(picPath, uiTex, uiTex.height, uiTex.width,
  DelegateFactory.Action(function ()
  end), true, true)
end

function LuaPersonalSpaceHotTalkWnd:SetMomentPic(node, infoArray, index)
    local info = infoArray[index]

    if System.String.IsNullOrEmpty(info.thumb) then
        return
    end

    --TODO check cache
    self:CleanBtn(node)

    --CPersonalSpaceMgr.Inst.DownLoadPortraitPic(info.thumb, node.GetComponent<UITexture>(), delegate(Texture texture)
    --{

    --});

    CPersonalSpaceMgr.DownLoadPic(info.thumb, CommonDefs.GetComponent_GameObject_Type(node, typeof(UITexture)), CPersonalSpaceMgr.MaxSmallPicHeight, CPersonalSpaceMgr.MaxSmallPicWidth, DelegateFactory.Action(function ()
        if not self or not self.templateNode then
            return
        end
        UIEventListener.Get(node).onClick = DelegateFactory.VoidDelegate(function (p)
            LuaPersonalSpaceMgrReal.InitDetailPicPanel(infoArray, index)
        end)
    end), false, true)
end
--function LuaPersonalSpaceHotTalkWnd:TableToList(tData, tType)
  --for i,v in ipairs(tData) do
    --local
    --list:Add()
  --end
--end
function LuaPersonalSpaceHotTalkWnd:SetBoxCollider(boxSprite)
  if boxSprite ~= nil then
    local boxObject = boxSprite.gameObject
    local old = CommonDefs.GetComponent_GameObject_Type(boxObject, typeof(BoxCollider))
    if old ~= nil then
      Object1.Destroy(old)
    end

    local newBox = CommonDefs.AddComponent_GameObject_Type(boxObject, typeof(BoxCollider))
    local newHeight = boxSprite.height + CPersonalSpaceWnd.ColliderMoreNum
    newBox.center = Vector3(0, math.floor(- newHeight / 2), 0)
    newBox.size = Vector3(boxSprite.width, newHeight, 0)
  end
end


function LuaPersonalSpaceHotTalkWnd:InitInfoNode(info, scrollView)
    local node = NGUITools.AddChild(self.table.gameObject,self.templateNode)
    node:SetActive(true)

    node.transform:Find("icon").gameObject:SetActive(true)
    --node.transform:Find("icon/hotsign").gameObject:SetActive(true)
    node.transform:Find("name").gameObject:SetActive(true)
    node.transform:Find("lv").gameObject:SetActive(true)
    node.transform:Find("subInfo/time").gameObject:SetActive(true)
    node.transform:Find("time").gameObject:SetActive(false)
    node.transform:Find("bgbutton").gameObject:SetActive(true)
    --node.transform.FindChild("bgbutton").GetComponent<UISprite>().ResizeCollider();
    node.transform:Find("selfbgbutton").gameObject:SetActive(false)

    CPersonalSpaceMgr.Inst:SetNodeDragScroll(node, scrollView)
    local subInfo = node.transform:Find("subInfo")
    local statusLabel = CommonDefs.GetComponent_Component_Type(node.transform:Find("status"), typeof(UILabel))
    local favorIcon = node.transform:Find("subInfo/favorbutton/favoricon").gameObject
    local favorbuttonSign = node.transform:Find("subInfo/favorbutton/favoricon/UI_aixin").gameObject
    local favorNumLabel = CommonDefs.GetComponent_Component_Type(node.transform:Find("subInfo/favorbutton/favornum"), typeof(UILabel))

    local favorbarNode = node.transform:Find("subInfo/favorbar").gameObject
    local favorbarLabel = CommonDefs.GetComponent_Component_Type(node.transform:Find("subInfo/favorbar/text"), typeof(UILabel))
    local leaveMessageNumText = CommonDefs.GetComponent_Component_Type(node.transform:Find("subInfo/leavemessagebutton/leavemessagenum"), typeof(UILabel))

    --if (totalReNew)
    --{
    if System.String.IsNullOrEmpty(info.rolename) then
        CommonDefs.GetComponent_Component_Type(node.transform:Find("name"), typeof(UILabel)).text = tostring(info.roleid)
    else
        CommonDefs.GetComponent_Component_Type(node.transform:Find("name"), typeof(UILabel)).text = info.rolename
    end

    local vipNode = node.transform:Find("name/vipicon")
    if vipNode ~= nil then
        CPersonalSpaceMgr.Inst:SetSpLevelNode(info.splevel, vipNode.gameObject)
    end

    if CClientMainPlayer.Inst:GetMyServerId() ~= info.server_id and info.server_id ~= 0 then
        CommonDefs.GetComponent_Component_Type(node.transform:Find("name"), typeof(UILabel)).text = CommonDefs.GetComponent_Component_Type(node.transform:Find("name"), typeof(UILabel)).text .. (CPersonalSpaceMgr.ServerSplitChar .. info.server_name)
    end

    local followBtn = node.transform:Find("followbutton").gameObject
    local cancelFollowBtn = node.transform:Find("cancelFollowBtn").gameObject
    if info.roleid ~= CIMMgr.PERSONAL_SPACE_HOT_POINT_ID and not CPersonalSpaceMgr.CheckAndLoadGongZhongHaoPortraitPic(info.roleid, nil, nil) and info.roleid ~= CClientMainPlayer.Inst.Id then
        UIEventListener.Get(followBtn).onClick = DelegateFactory.VoidDelegate(function (go)
          local backFunction = function(result)
            if not self or not self.templateNode then
              return
            end
            if result then
              followBtn:SetActive(false)
              cancelFollowBtn:SetActive(true)
              g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("关注玩家成功!"))
            end
          end
          LuaPersonalSpaceMgrReal.SetPlayerFollow(info.roleid,backFunction)
        end)

        UIEventListener.Get(cancelFollowBtn).onClick = DelegateFactory.VoidDelegate(function (go)
          local backFunction = function(result)
            if not self or not self.templateNode then
              return
            end
            if result then
              followBtn:SetActive(true)
              cancelFollowBtn:SetActive(false)
              g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("取消关注玩家成功!"))
            end
          end
          LuaPersonalSpaceMgrReal.CancelPlayerFollow(info.roleid,backFunction)
        end)

        if not info.isfollowing then
          followBtn:SetActive(true)
          cancelFollowBtn:SetActive(false)
        else
          followBtn:SetActive(false)
          cancelFollowBtn:SetActive(true)
        end
    else
        followBtn:SetActive(false)
        cancelFollowBtn:SetActive(false)
    end

    if info.roleid == CIMMgr.PERSONAL_SPACE_HOT_POINT_ID then
        CommonDefs.GetComponent_Component_Type(node.transform:Find("icon"), typeof(CUITexture)):LoadNPCPortrait(CPersonalSpaceMgr.Inst:GetSpaceGMIcon(), false)
        CommonDefs.GetComponent_Component_Type(node.transform:Find("lv"), typeof(UILabel)).text = ""
    elseif CPersonalSpaceMgr.CheckAndLoadGongZhongHaoPortraitPic(info.roleid, node.transform:Find("icon").gameObject, CommonDefs.GetComponent_Component_Type(node.transform:Find("lv"), typeof(UILabel))) then
    else
        local levelLabel = CommonDefs.GetComponent_Component_Type(node.transform:Find("lv"), typeof(UILabel))
        local default
        if info.xianfanstatus > 0 then
            default = L10.Game.Constants.ColorOfFeiSheng
        else
            default = NGUIText.EncodeColor24(levelLabel.color)
        end
        levelLabel.text = System.String.Format(LocalString.GetString("[c][{0}]{1}[-][/c]"), default, "lv." .. info.grade)

        --if (!string.IsNullOrEmpty(info.clazz) && !string.IsNullOrEmpty(info.gender))
        --{
        --    uint cls = uint.Parse(info.clazz);
        --    uint gender = uint.Parse(info.gender);
        --    node.transform.FindChild("icon").GetComponent<CUITexture>().LoadNPCPortrait(CUICommonDef.GetPortraitName(cls, gender));
        --}

        LuaPersonalSpaceMgrReal.LoadPortraitPic(info.photo, info.clazz, info.gender, node.transform:Find("icon").gameObject, info.expression_base)

        if info.roleid ~= CClientMainPlayer.Inst.Id then
            CPersonalSpaceMgr.Inst:addPlayerLinkBtn(node.transform:Find("icon/button").gameObject, info.roleid, EnumReportId_lua.eMengDaoMoment, info.text, nil, info.id, 0)
        end
    end

    local totalShowText = string.gsub(string.gsub(info.text, "&lt;", "<"), "&gt;", ">")
    totalShowText = totalShowText .. LuaPersonalSpaceMgrReal.GenerateForwardString(info.previousforwards)
    totalShowText = CChatMgr.Inst:FilterYangYangEmotion(totalShowText)
    statusLabel.text = SafeStringFormat3('[c][FFFFFF]%s[-][/c]',CChatLinkMgr.TranslateToNGUIText(totalShowText, false))

    UIEventListener.Get(statusLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
        local url = CommonDefs.GetComponent_GameObject_Type(statusLabel.gameObject, typeof(UILabel)):GetUrlAtPosition(UICamera.lastWorldPosition)
        if url ~= nil and CPersonalSpaceMgr.ProcessLinkClick(url) then
            return
        else
            self:ShowMomentDetailThroughFriendCircle(info, false)
        end
    end)

    UIEventListener.Get(node.transform:Find("subInfo/leavemessagebutton").gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
        self:ShowMomentDetailThroughFriendCircle(info, true)
    end)

    local deleteBtn = node.transform:Find("subInfo/deletebutton").gameObject
    if info.roleid == CClientMainPlayer.Inst.Id then
      deleteBtn:SetActive(false)
 --       deleteBtn:SetActive(true)
--        UIEventListener.Get(deleteBtn).onClick = DelegateFactory.VoidDelegate(function (p)
--            MessageWndManager.ShowConfirmMessage(CPersonalSpaceWnd.DeleteMomentAlertString, 10, false, DelegateFactory.Action(function ()
--                CPersonalSpaceMgr.Inst:DelMoment(info.id, DelegateFactory.Action_CPersonalSpace_BaseRet(function (data)
--                    if not self or not self.templateNode then
--                        return
--                    end
--                    self:DeleteMomentBack(data, info, generateType)
--                end), nil)
--            end), nil)
--        end)
    else
        deleteBtn:SetActive(false)
    end

    --}

    local bgBtn = node.transform:Find("bgbutton").gameObject
    local selfBgBtn = node.transform:Find("selfbgbutton").gameObject
    bgBtn:SetActive(true)
    selfBgBtn:SetActive(true)

    UIEventListener.Get(bgBtn).onClick = DelegateFactory.VoidDelegate(function (p)
      self:ShowMomentDetailThroughFriendCircle(info, false)
    end)

    UIEventListener.Get(selfBgBtn).onClick = DelegateFactory.VoidDelegate(function (p)
      self:ShowMomentDetailThroughFriendCircle(info, false)
    end)

    local infoTime = info.createtime / 1000
    local nowTime = CServerTimeMgr.Inst.timeStamp
    local disTime = nowTime - infoTime
    if disTime < 0 then
        disTime = 0
    end

    if disTime < 60 then
        CommonDefs.GetComponent_Component_Type(node.transform:Find("subInfo/time"), typeof(UILabel)).text = LocalString.GetString("刚刚")
        CommonDefs.GetComponent_Component_Type(node.transform:Find("time"), typeof(UILabel)).text = LocalString.GetString("刚刚")
    elseif disTime < 3600 then
        CommonDefs.GetComponent_Component_Type(node.transform:Find("subInfo/time"), typeof(UILabel)).text = math.ceil(disTime / 60) .. LocalString.GetString("分钟前")
        CommonDefs.GetComponent_Component_Type(node.transform:Find("time"), typeof(UILabel)).text = math.ceil(disTime / 60) .. LocalString.GetString("分钟前")
    elseif disTime < 86400 --[[24 * 3600]] then
        CommonDefs.GetComponent_Component_Type(node.transform:Find("subInfo/time"), typeof(UILabel)).text = math.ceil(disTime / 3600) .. LocalString.GetString("小时前")
        CommonDefs.GetComponent_Component_Type(node.transform:Find("time"), typeof(UILabel)).text = math.ceil(disTime / 3600) .. LocalString.GetString("小时前")
    else
        local infoTimeDate = CServerTimeMgr.ConvertTimeStampToZone8Time(infoTime)
        CommonDefs.GetComponent_Component_Type(node.transform:Find("subInfo/time"), typeof(UILabel)).text = CPersonalSpaceMgr.GetTimeString(infoTime)
        CommonDefs.GetComponent_Component_Type(node.transform:Find("time"), typeof(UILabel)).text = ((infoTimeDate.Month .. LocalString.GetString("月")) .. infoTimeDate.Day) .. LocalString.GetString("日")
    end

    local yShift = - statusLabel.height - 11
    local picNodeArray = CreateFromClass(MakeGenericClass(List, GameObject))
    for pic_i = 1, 6 do
        local picNode = node.transform:Find("pic" .. tostring(pic_i))
        if picNode ~= nil then
            picNode.gameObject:SetActive(false)
            CommonDefs.ListAdd(picNodeArray, typeof(GameObject), picNode.gameObject)
        end
    end

    local speNode = node.transform:Find("spepic").gameObject

    speNode:SetActive(false)

    if info.speimage ~= nil and not System.String.IsNullOrEmpty(info.speimage.url) then
        speNode:SetActive(true)
        CPersonalSpaceMgr.DownLoadPic(info.speimage.url, CommonDefs.GetComponent_GameObject_Type(speNode, typeof(UITexture)), 160, 800, DelegateFactory.Action(function ()
            if not self or not self.templateNode then
                return
            end
            UIEventListener.Get(speNode).onClick = DelegateFactory.VoidDelegate(function (p)
                CWebBrowserMgr.Inst:OpenUrl(info.speimage.jump_url)
            end)
        end), false, true)
        speNode.transform.localPosition = Vector3(speNode.transform.localPosition.x, CPersonalSpaceWnd.DefaultPicPosY + yShift, speNode.transform.localPosition.z)
        yShift = yShift - CPersonalSpaceWnd.DefaultPicHeight
    elseif info.videolist and #info.videolist > 0 then
        local videoNode = node.transform:Find('pic1').gameObject
        LuaPersonalSpaceMgrReal.SetVideoPlayPic(videoNode,100)
        local videoData = info.videolist[1]
        videoNode:SetActive(true)
        CPersonalSpaceMgr.DownLoadPic(videoData.snapshot, CommonDefs.GetComponent_GameObject_Type(videoNode, typeof(UITexture)), 160, 250, DelegateFactory.Action(function ()
            if not self.templateNode then
                return
            end
            UIEventListener.Get(videoNode).onClick = DelegateFactory.VoidDelegate(function (p)
              LuaPersonalSpaceMgrReal.MoviePlayUrl = videoData.url
              CUIManager.ShowUI(CLuaUIResources.DetailMovieShowWnd)
            end)
        end), true, true)
        videoNode.transform.localPosition = Vector3(videoNode.transform.localPosition.x, CPersonalSpaceWnd.DefaultPicPosY + yShift, videoNode.transform.localPosition.z)
        yShift = yShift - CPersonalSpaceWnd.DefaultPicHeight
    else
        local imgCount = 0
        if info.imglist then
          for i,v in pairs(info.imglist) do
            if v and v ~= '' then
              imgCount = imgCount + 1
            end
          end
        end
        if imgCount > 0 then
          for i,v in ipairs(info.imglist) do
            local imageInfo = v
            local picNode = picNodeArray[i-1]
            picNode:SetActive(true)
            self:SetMomentPic(picNode, info.imglist, i)
            if i <= 3 then
              picNode.transform.localPosition = Vector3(picNode.transform.localPosition.x, CPersonalSpaceWnd.DefaultPicPosY + yShift, picNode.transform.localPosition.z)
            else
              picNode.transform.localPosition = Vector3(picNode.transform.localPosition.x, CPersonalSpaceWnd.DefaultPicPosY + yShift - CPersonalSpaceWnd.DefaultTwoRowPicHeight, picNode.transform.localPosition.z)
            end
          end
            if imgCount <= 3 then
                yShift = yShift - CPersonalSpaceWnd.DefaultPicHeight
            else
                yShift = yShift - CPersonalSpaceWnd.DefaultPicHeight
                yShift = yShift - CPersonalSpaceWnd.DefaultTwoRowPicHeight
            end
        end
    end


    local forwardNode = node.transform:Find("forwardinfo").gameObject
    if info.forwardmoment and info.forwardmoment.id and info.forwardmoment.id > 0 then
        forwardNode:SetActive(true)

        if System.String.IsNullOrEmpty(info.forwardmoment.rolename) then
            CommonDefs.GetComponent_Component_Type(forwardNode.transform:Find("name"), typeof(UILabel)).text = tostring(info.forwardmoment.roleid)
        else
            CommonDefs.GetComponent_Component_Type(forwardNode.transform:Find("name"), typeof(UILabel)).text = info.forwardmoment.rolename
        end

        local forwardVipNode = forwardNode.transform:Find("name/vipicon")
        if forwardVipNode ~= nil then
            CPersonalSpaceMgr.Inst:SetSpLevelNode(info.forwardmoment.splevel, forwardVipNode.gameObject)
        end

        if CClientMainPlayer.Inst:GetMyServerId() ~= info.forwardmoment.server_id and info.forwardmoment.server_id ~= 0 then
            CommonDefs.GetComponent_Component_Type(forwardNode.transform:Find("name"), typeof(UILabel)).text = CommonDefs.GetComponent_Component_Type(forwardNode.transform:Find("name"), typeof(UILabel)).text .. (CPersonalSpaceMgr.ServerSplitChar .. info.forwardmoment.server_name)
        end

        if info.forwardmoment.roleid == CIMMgr.PERSONAL_SPACE_HOT_POINT_ID then
            CommonDefs.GetComponent_Component_Type(forwardNode.transform:Find("lv"), typeof(UILabel)).text = ""
            CommonDefs.GetComponent_Component_Type(forwardNode.transform:Find("icon"), typeof(CUITexture)):LoadNPCPortrait(CPersonalSpaceMgr.Inst:GetSpaceGMIcon(), false)
        elseif CPersonalSpaceMgr.CheckAndLoadGongZhongHaoPortraitPic(info.forwardmoment.roleid, forwardNode.transform:Find("icon").gameObject, CommonDefs.GetComponent_Component_Type(forwardNode.transform:Find("lv"), typeof(UILabel))) then
        else
            --forwardNode.transform.FindChild("lv").GetComponent<UILabel>().text = info.forwardmoment.grade.ToString();
            local levelLabel = CommonDefs.GetComponent_Component_Type(forwardNode.transform:Find("lv"), typeof(UILabel))
            local extern
            if info.forwardmoment.xianfanstatus > 0 then
                extern = L10.Game.Constants.ColorOfFeiSheng
            else
                extern = NGUIText.EncodeColor24(levelLabel.color)
            end
            levelLabel.text = System.String.Format(LocalString.GetString("[c][{0}]{1}[-][/c]"), extern, "lv." .. info.forwardmoment.grade)

            --if (!string.IsNullOrEmpty(info.forwardmoment.clazz) && !string.IsNullOrEmpty(info.forwardmoment.gender))
            --{
            --    uint cls = uint.Parse(info.forwardmoment.clazz);
            --    uint gender = uint.Parse(info.forwardmoment.gender);
            --    forwardNode.transform.FindChild("icon").GetComponent<CUITexture>().LoadNPCPortrait(CUICommonDef.GetPortraitName(cls, gender));
            --}

            LuaPersonalSpaceMgrReal.LoadPortraitPic(info.forwardmoment.photo, info.forwardmoment.clazz, info.forwardmoment.gender, forwardNode.transform:Find("icon").gameObject, info.forwardmoment.expression_base)

            if info.forwardmoment.roleid ~= CClientMainPlayer.Inst.Id then
              local imgArray = nil
              if info.forwardmoment and info.forwardmoment.imglist then
                img = Table2ArrayWithCount(info.forwardmoment.imglist, #info.forwardmoment.imglist, MakeArrayClass(String))
              else
                img = Table2ArrayWithCount({}, 0, MakeArrayClass(String))
              end
              CPersonalSpaceMgr.Inst:addPlayerLinkBtn(forwardNode.transform:Find("icon/button").gameObject, info.forwardmoment.roleid, EnumReportId_lua.eMengDaoMoment, info.forwardmoment.text,
              imgArray, info.forwardmoment.id, 0)
            end
        end


        local showText = string.gsub(string.gsub(info.forwardmoment.text, "&lt;", "<"), "&gt;", ">")
        showText = CChatMgr.Inst:FilterYangYangEmotion(showText)
        local forwardLabel = CommonDefs.GetComponent_Component_Type(forwardNode.transform:Find("status"), typeof(UILabel))
        CPersonalSpaceMgr.SetTextMore(showText, forwardLabel)
        UIEventListener.Get(forwardLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
            local url = CommonDefs.GetComponent_GameObject_Type(statusLabel.gameObject, typeof(UILabel)):GetUrlAtPosition(UICamera.lastWorldPosition)
            if url ~= nil and CPersonalSpaceMgr.ProcessLinkClick(url) then
                return
            else
                self:ShowMomentDetailThroughFriendCircle(info.forwardmoment, false)
            end
        end)

        --GameObject forward_picNode1 = forwardNode.transform.FindChild("pic1").gameObject;
        --GameObject forward_picNode2 = forwardNode.transform.FindChild("pic2").gameObject;
        --GameObject forward_picNode3 = forwardNode.transform.FindChild("pic3").gameObject;
        local forward_speNode = forwardNode.transform:Find("spepic").gameObject

        --GameObject[] forward_picNodeArray = new GameObject[] { forward_picNode1, forward_picNode2, forward_picNode3 };
        --forward_picNode1.SetActive(false);
        --forward_picNode2.SetActive(false);
        --forward_picNode3.SetActive(false);

        local forward_picNodeArray = CreateFromClass(MakeGenericClass(List, GameObject))
        for pic_i = 1, 6 do
            local picNode = forwardNode.transform:Find("pic" .. tostring(pic_i))
            if picNode ~= nil then
                picNode.gameObject:SetActive(false)
                CommonDefs.ListAdd(forward_picNodeArray, typeof(GameObject), picNode.gameObject)
            end
        end

        forward_speNode:SetActive(false)

        local forward_bgBtn = forwardNode.transform:Find("bg").gameObject

        UIEventListener.Get(forward_bgBtn).onClick = DelegateFactory.VoidDelegate(function (p)
            self:ShowMomentDetailThroughFriendCircle(info.forwardmoment, false)
        end)

        forwardNode.transform.localPosition = Vector3(forwardNode.transform.localPosition.x, CPersonalSpaceWnd.DefaultPicPosY + yShift, forwardNode.transform.localPosition.z)

        if info.forwardmoment.speimage ~= nil and not System.String.IsNullOrEmpty(info.forwardmoment.speimage.url) then
            forward_speNode:SetActive(true)
            CPersonalSpaceMgr.DownLoadPic(info.forwardmoment.speimage.url, CommonDefs.GetComponent_GameObject_Type(forward_speNode, typeof(UITexture)), 160, 800, DelegateFactory.Action(function ()
                if not self or not self.templateNode then
                    return
                end
                UIEventListener.Get(forward_speNode).onClick = DelegateFactory.VoidDelegate(function (p)
                    CWebBrowserMgr.Inst:OpenUrl(info.forwardmoment.speimage.jump_url)
                end)
            end), false, true)

            CommonDefs.GetComponent_GameObject_Type(forward_bgBtn, typeof(UISprite)).height = CPersonalSpaceMgr.ForwardInfoHeight2
            yShift = yShift - CPersonalSpaceMgr.ForwardInfoHeight2
        elseif info.forwardmoment.videolist and #info.forwardmoment.videolist > 0 then
          local videoNode = forwardNode.transform:Find('pic1').gameObject
          LuaPersonalSpaceMgrReal.SetVideoPlayPic(videoNode,100)
          local videoData = info.forwardmoment.videolist[1]
          videoNode:SetActive(true)
          CPersonalSpaceMgr.DownLoadPic(videoData.snapshot, CommonDefs.GetComponent_GameObject_Type(videoNode, typeof(UITexture)), 160, 250, DelegateFactory.Action(function ()
            if not self.templateNode then
              return
            end
            UIEventListener.Get(videoNode).onClick = DelegateFactory.VoidDelegate(function (p)
              LuaPersonalSpaceMgrReal.MoviePlayUrl = videoData.url
              CUIManager.ShowUI(CLuaUIResources.DetailMovieShowWnd)
            end)
          end), true, true)
          --videoNode.transform.localPosition = Vector3(videoNode.transform.localPosition.x, CPersonalSpaceWnd.DefaultPicPosY + yShift, videoNode.transform.localPosition.z)
          CommonDefs.GetComponent_GameObject_Type(forward_bgBtn, typeof(UISprite)).height = CPersonalSpaceMgr.ForwardInfoHeight2
          yShift = yShift - CPersonalSpaceMgr.ForwardInfoHeight2
        else
            if info.forwardmoment.imglist ~= nil and #info.forwardmoment.imglist > 0 then
              local imgCount = 0
              if info.forwardmoment.imglist then
                for i,v in pairs(info.forwardmoment.imglist) do
                  if v and v ~= '' then
                    imgCount = imgCount + 1
                  end
                end
              end
              if imgCount > 0 then
                for i,v in ipairs(info.forwardmoment.imglist) do
                  local imageInfo = v
                  local picNode = forward_picNodeArray[i-1]
                  picNode:SetActive(true)
                  self:SetMomentPic(picNode, info.forward_picNodeArray.imglist, i)
                end
              end

              if info.forwardmoment and info.forwardmoment.imglist and #info.forwardmoment.imglist <= 3 then
                CommonDefs.GetComponent_GameObject_Type(forward_bgBtn, typeof(UISprite)).height = CPersonalSpaceMgr.ForwardInfoHeight2
                yShift = yShift - CPersonalSpaceMgr.ForwardInfoHeight2
              else
                CommonDefs.GetComponent_GameObject_Type(forward_bgBtn, typeof(UISprite)).height = CPersonalSpaceMgr.ForwardInfoHeight2 + CPersonalSpaceWnd.DefaultTwoRowPicHeight
                yShift = yShift - CPersonalSpaceMgr.ForwardInfoHeight2
                yShift = yShift - CPersonalSpaceWnd.DefaultTwoRowPicHeight
              end
            else
              CommonDefs.GetComponent_GameObject_Type(forward_bgBtn, typeof(UISprite)).height = CPersonalSpaceMgr.ForwardInfoHeight1
              yShift = yShift - CPersonalSpaceMgr.ForwardInfoHeight1
            end
          end

        CommonDefs.GetComponent_GameObject_Type(forward_bgBtn, typeof(UISprite)):ResizeCollider()
    else
        forwardNode:SetActive(false)
    end

    subInfo.localPosition = Vector3(subInfo.localPosition.x, yShift, subInfo.localPosition.z)

    --//favor
    local favorText = ""
    favorIcon:SetActive(false)
    favorbuttonSign:SetActive(false)

    if info.zanlist ~= nil and info.zancount > 0 then
        if info.is_user_liked then
            favorIcon:SetActive(true)
        else
            favorIcon:SetActive(false)
        end

        favorNumLabel.text = tostring(info.zancount)

        do
            local zan_i = 1
            while zan_i < #info.zanlist do
                local zanInfo = info.zanlist[zan_i]
                if zan_i < CPersonalSpaceWnd.ShowZanListBarNum then
                    local name = zanInfo.rolename
                    if System.String.IsNullOrEmpty(name) then
                        name = tostring(zanInfo.roleid)
                    end
                    favorText = favorText .. CPersonalSpaceMgr.GeneratePlayerName(zanInfo.roleid, name)
                end
                if zanInfo.roleid == CClientMainPlayer.Inst.Id then
                    if info.showFavorSign then
                        favorbuttonSign:SetActive(true)
                        info.showFavorSign = false
                        --this:updateMomentCache(Table2ArrayWithCount({info}, 1, MakeArrayClass(CPersonalSpace_GetMomentItem)), true, generateType, nil, false)
                    else
                        favorbuttonSign:SetActive(false)
                    end
                end
                zan_i = zan_i + 1
            end
        end
    else
        favorNumLabel.text = "0"
    end

    local totalHeight = 0

    if info.zanlist ~= nil and #info.zanlist > 0 then
        --if generateType == CPersonalSpace_CircleGenerateType.AllHotMoments or generateType == CPersonalSpace_CircleGenerateType.HotMoments then
        if true then
            favorbarNode:SetActive(false)
        else
            favorbarNode:SetActive(true)
            favorbarLabel.text = CChatLinkMgr.TranslateToNGUIText(favorText, false)
            UIEventListener.Get(favorbarLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
                local index = favorbarLabel:GetCharacterIndexAtPosition(UICamera.lastWorldPosition, true)
                if index == 0 then
                    index = - 1
                end

                local url = favorbarLabel:GetUrlAtCharacterIndex(index)
                if url ~= nil and CPersonalSpaceMgr.ProcessLinkClick(url) then
                    return
                else
                    self:ShowMomentDetailThroughFriendCircle(info, false)
                end
            end)
            totalHeight = totalHeight + CPersonalSpaceWnd.DefaultTotalHeight
        end
    else
        favorbarNode:SetActive(false)
        --totalHeight = 0;
    end

    --//leavemessage
    if info.commentlist ~= nil and #info.commentlist > 0 then
        leaveMessageNumText.text = tostring(info.commentcount)
        --if generateType == CPersonalSpace_CircleGenerateType.AllHotMoments or generateType == CPersonalSpace_CircleGenerateType.HotMoments then
        if false then
            node.transform:Find("subInfo/messagebar").gameObject:SetActive(false)
        else
            node.transform:Find("subInfo/messagebar").gameObject:SetActive(true)
            local nodeArray = {node.transform:Find("subInfo/messagebar/1").gameObject, node.transform:Find("subInfo/messagebar/2").gameObject, node.transform:Find("subInfo/messagebar/3").gameObject}

            do
                local message_i = 1
                while message_i <= CPersonalSpaceWnd.ShowMessageListBarNum do
                    if message_i <= #info.commentlist and message_i <= #nodeArray then
                        local comment_info = info.commentlist[message_i]
                        local messageNode = nodeArray[message_i]
                        messageNode:SetActive(true)

                        local showText = CPersonalSpaceMgr.GeneratePlayerName(comment_info.roleid, comment_info.rolename)
                        if comment_info and comment_info.replyinfo and comment_info.replyinfo.roleid ~= 0 then
                            showText = showText .. (CPersonalSpaceDetailMoment.ReplayMidfix .. CPersonalSpaceMgr.GeneratePlayerName(comment_info.replyinfo.roleid, comment_info.replyinfo.rolename))
                        end

                        showText = showText .. comment_info.text
                        showText = CChatMgr.Inst:FilterYangYangEmotion(showText)
                        local textNode = messageNode.transform:Find("text")
                        messageNode.transform.localPosition = Vector3(messageNode.transform.localPosition.x, - totalHeight, messageNode.transform.localPosition.z)
                        local textNodeLabel = CommonDefs.GetComponent_Component_Type(textNode, typeof(UILabel))
                        textNodeLabel.text = CChatLinkMgr.TranslateToNGUIText(showText, false)
                        totalHeight = totalHeight + (textNodeLabel.height + CPersonalSpaceWnd.ShowMessageSpace)

                        UIEventListener.Get(textNode.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
                            local index = textNodeLabel:GetCharacterIndexAtPosition(UICamera.lastWorldPosition, true)
                            if index == 0 then
                                index = - 1
                            end

                            local url = textNodeLabel:GetUrlAtCharacterIndex(index)
                            if url ~= nil and CPersonalSpaceMgr.ProcessLinkClick(url) then
                                CPersonalSpaceMgr.Inst:SetPlayerReportDefaultInfo(EnumReportId_lua.eMengDaoMoment, info.text, nil, info.id)
                                return
                            else
                                self:ShowMomentDetailThroughFriendCircle(info, false)
                            end
                        end)
                    elseif message_i <= #nodeArray then
                        nodeArray[message_i]:SetActive(false)
                    end
                    message_i = message_i + 1
                end
            end
        end
    else
        leaveMessageNumText.text = "0"
        node.transform:Find("subInfo/messagebar").gameObject:SetActive(false)
    end

    local subInfoBgSprite = CommonDefs.GetComponent_Component_Type(node.transform:Find("subInfo/bg"), typeof(UISprite))
    subInfoBgSprite.height = totalHeight

    if totalHeight <= 0 then
        subInfoBgSprite.gameObject:SetActive(false)
    else
        subInfoBgSprite.gameObject:SetActive(true)
    end

    local bgBtnSprite = CommonDefs.GetComponent_GameObject_Type(bgBtn, typeof(UISprite))
    bgBtnSprite.height = CPersonalSpaceWnd.DefaultBgButtonHeight + totalHeight - yShift
    --bgBtnSprite.ResizeCollider();
    self:SetBoxCollider(bgBtnSprite)

    local selfbgBtnSprite = CommonDefs.GetComponent_GameObject_Type(selfBgBtn, typeof(UISprite))
    selfbgBtnSprite.height = CPersonalSpaceWnd.DefaultSelfBgButtonHeight + totalHeight - yShift
    --selfbgBtnSprite.ResizeCollider();
    self:SetBoxCollider(selfbgBtnSprite)

    local forwardBtn = node.transform:Find("subInfo/forwardbutton").gameObject
    local forwardNum = CommonDefs.GetComponent_Component_Type(node.transform:Find("subInfo/forwardbutton/num"), typeof(UILabel))
    forwardNum.text = tostring(info.forwardcount)
    UIEventListener.Get(forwardBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        if info.forwardmoment and info.forwardmoment.id and info.forwardmoment.id > 0 and info.forwardmoment.status and info.forwardmoment.status == - 1 then
            g_MessageMgr:ShowMessage("PersonalSpaceCannotForward")
        else
            --CPersonalSpaceMgr.Inst.saveForwardInfo = info
            LuaPersonalSpaceMgrReal.forwardInfo = info
            CUIManager.ShowUI(CLuaUIResources.PersonalSpaceForwardLuaWnd)
        end
    end)

    --favorbuttonSign.SetActive(false);
    UIEventListener.Get(node.transform:Find("subInfo/favorbutton").gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
        CPersonalSpaceMgr.Inst:LikeMoment(info.id, favorIcon.activeSelf, DelegateFactory.Action_CPersonalSpace_BaseRet(function (data)

            if not self or not self.templateNode then
                return
            end
            if data.code == 0 then
                --UpdateZanListBySelf(!favorIcon.activeSelf, info);
                --ReposFriendCircleList(scrollView);

                LuaPersonalSpaceMgrReal.GetSingleMomentInfo(info.id, function (ret)
                    if not self or not self.templateNode then
                        return
                    end
                    if ret.code == 0 then
                        local newItem = ret.data
                        newItem.type = generateType
                        if not favorIcon.activeSelf and newItem.is_user_liked then
                            newItem.showFavorSign = true
                        else
                            newItem.showFavorSign = false
                        end
                        info.showFavorSign = newItem.showFavorSign
                        info.zancount = newItem.zancount
                        info.is_user_liked = newItem.is_user_liked
                        self:InitInfoTable(true)
                    end
                end)
            end
        end), DelegateFactory.Action(function ()
        end))
    end)

    return node
end

function LuaPersonalSpaceHotTalkWnd:InitSortItem()
    local node = NGUITools.AddChild(self.table.gameObject,self.sortTemplate)
    node:SetActive(true)
    local sortName = self.sortNameTable
    for i,v in pairs(sortName) do
      if v.type == self.m_SortType then
        node.transform:Find('label'):GetComponent(typeof(UILabel)).text = v.name
      end
    end
    local btn = node.transform:Find('bg').gameObject
    local arrow = node.transform:Find('Sprite')
    arrow.localEulerAngles = Vector3.zero
    if btn then
      local clickFunction = function(go)
        arrow.localEulerAngles = Vector3(0,0,180)
        local rows = CreateFromClass(MakeGenericClass(List, PopupMenuItemData))
        for i,v in ipairs(sortName) do
          CommonDefs.ListAdd(rows, typeof(PopupMenuItemData), PopupMenuItemData(v.name, DelegateFactory.Action_int(function (row)
            self.m_SortType = v.type
            self.qaBtn:SetValue(1, true)
          end), false, nil))
        end
        CPopupMenuInfoMgr.ShowPopupMenu(CommonDefs.ListToArray(rows), btn.transform, CPopupMenuInfoMgr.AlignType.Bottom,1,
        DelegateFactory.Action(function()
          arrow.localEulerAngles = Vector3.zero
				end)
        ,600,true,296)
      end
      CommonDefs.AddOnClickListener(btn,DelegateFactory.Action_GameObject(clickFunction),false)
    end
end

function LuaPersonalSpaceHotTalkWnd:InitInfoTable(savePosSign)
  if LuaPersonalSpaceMgrReal.ShowInfoList then
    local savePos = self.scrollView.transform.localPosition
    local saveOffsetY = self.m_Panel.clipOffset.y

    Extensions.RemoveAllChildren(self.table.transform)
    self.FriendsCircle:SetActive(true)
    self.DetailMoment:SetActive(false)
    if LuaPersonalSpaceMgrReal.ShowInfoList and #LuaPersonalSpaceMgrReal.ShowInfoList > 0 then
      self.EmptyNode:SetActive(false)
      self:InitSortItem()
      for i,v in ipairs(LuaPersonalSpaceMgrReal.ShowInfoList) do
        local node = self:InitInfoNode(v,self.scrollView)
      end
    else
      self.EmptyNode:SetActive(true)
    end
    self.table:Reposition()
    self.scrollView:ResetPosition()

    if savePosSign then
      self.scrollView.transform.localPosition = savePos
      self.m_Panel.clipOffset = Vector2(self.m_Panel.clipOffset.x, saveOffsetY)
    end
  end
end

function LuaPersonalSpaceHotTalkWnd:InitNewInfoTable()
  self:InitInfoTable()
end

function LuaPersonalSpaceHotTalkWnd:RefreshInfo()
  self:InitInfoTable(true)
end

function LuaPersonalSpaceHotTalkWnd:AddNewMomentAndRefreshInfo()
  g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("心情发送成功"))
  self:InitInfoTable(true)
end

function LuaPersonalSpaceHotTalkWnd:OnEnable( ... )
	g_ScriptEvent:AddListener("PersonalSpaceHotTalkInfoBack", self, "InitNewInfoTable")
	g_ScriptEvent:AddListener("PersonalSpaceHotTalkTopicInfoBack", self, "InitTopicTable")
	g_ScriptEvent:AddListener("UpdatePersonalSpaceHotTalkWnd", self, "RefreshInfo")
  g_ScriptEvent:AddListener("PersonalSpaceAddNewMomentSucc", self, "AddNewMomentAndRefreshInfo")
end

function LuaPersonalSpaceHotTalkWnd:OnDisable( ... )
	g_ScriptEvent:RemoveListener("PersonalSpaceHotTalkInfoBack", self, "InitNewInfoTable")
	g_ScriptEvent:RemoveListener("PersonalSpaceHotTalkTopicInfoBack", self, "InitTopicTable")
	g_ScriptEvent:RemoveListener("UpdatePersonalSpaceHotTalkWnd", self, "RefreshInfo")
  g_ScriptEvent:RemoveListener("PersonalSpaceAddNewMomentSucc", self, "AddNewMomentAndRefreshInfo")
end

function LuaPersonalSpaceHotTalkWnd:OnDestroy()

end

return LuaPersonalSpaceHotTalkWnd
