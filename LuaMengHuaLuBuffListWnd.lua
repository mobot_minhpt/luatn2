require("common/common_include")

local UIScrollView=import "UIScrollView"
local UIWidget = import "UIWidget"
local UITable = import "UITable"
local UIRoot = import "UIRoot"
local Screen = import "UnityEngine.Screen"
local NGUIMath = import "NGUIMath"
local Mathf = import "UnityEngine.Mathf"
local Extensions = import "Extensions"
local MengHuaLu_Buff = import "L10.Game.MengHuaLu_Buff"
local CUITexture = import "L10.UI.CUITexture"

LuaMengHuaLuBuffListWnd=class()

RegistChildComponent(LuaMengHuaLuBuffListWnd, "Table", UITable)
RegistChildComponent(LuaMengHuaLuBuffListWnd, "ScrollView", UIScrollView)
RegistChildComponent(LuaMengHuaLuBuffListWnd, "Background", UIWidget)
RegistChildComponent(LuaMengHuaLuBuffListWnd, "BuffItem", GameObject)


function LuaMengHuaLuBuffListWnd:Init()
    
    self.BuffItem:SetActive(false)

    self:UpdateBuffs()
    
    self.Table:Reposition()

    local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject)
    local virtualScreenHeight = Screen.height * scale
    local contentTopPadding = Mathf.Abs(self.ScrollView.panel.topAnchor.absolute)
    local contentBottomPadding = Mathf.Abs(self.ScrollView.panel.bottomAnchor.absolute)
    local totalHeight = NGUIMath.CalculateRelativeWidgetBounds(self.Table.transform).size.y + (self.Table.padding.y * 2) + contentTopPadding + contentBottomPadding + (self.ScrollView.panel.clipSoftness.y * 2)
    local displayWndHeight = Mathf.Clamp(totalHeight, contentTopPadding + contentBottomPadding + 80, virtualScreenHeight - 100)
    self.Background:GetComponent(typeof(UIWidget)).height = Mathf.CeilToInt(displayWndHeight)
    self.ScrollView.panel:ResetAndUpdateAnchors()
    self.ScrollView:ResetPosition()
end

function LuaMengHuaLuBuffListWnd:UpdateBuffs()

    Extensions.RemoveAllChildren(self.Table.transform)

    if not LuaMengHuaLuMgr.m_BuffList then 
        CUIManager.CloseUI(CLuaUIResources.MengHuaLuBuffListWnd)
        return 
    end

    for i = 0, LuaMengHuaLuMgr.m_BuffList.Count-1 do
        local go = CUICommonDef.AddChild(self.Table.gameObject, self.BuffItem)
        local buffInfo = LuaMengHuaLuMgr.m_BuffList[i]
        local buffId = buffInfo[0]
        local lastRound = buffInfo[1]
        self:InitBuffItem(go,{
            buffId = buffId,
            lastRound = lastRound,
            })
        go:SetActive(true)
    end

end

function LuaMengHuaLuBuffListWnd:InitBuffItem(go, info)
    local Icon = go.transform:Find("BuffIcon/Icon"):GetComponent(typeof(CUITexture))
    Icon:Clear()
    local LastRoundLabel = go.transform:Find("BuffIcon/LastRoundLabel"):GetComponent(typeof(UILabel))
    local NameLabel = go.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local DescLabel = go.transform:Find("DescLabel"):GetComponent(typeof(UILabel))

    if not info then return end

    local buff = MengHuaLu_Buff.GetData(info.buffId)
    if buff then
        Icon:LoadMaterial(buff.Icon)
        if info.lastRound > 0 then
            LastRoundLabel.text = tostring(info.lastRound)
        else
            LastRoundLabel.text = nil
        end
        NameLabel.text = buff.Name
        DescLabel.text = buff.Description
    end
end

function LuaMengHuaLuBuffListWnd:Update()
    self:ClickThroughToClose()
end

function LuaMengHuaLuBuffListWnd:UpdateBabyBuffs(buffs)
    LuaMengHuaLuMgr.m_BuffList = buffs
    self:UpdateBuffs()
end

function LuaMengHuaLuBuffListWnd:OnEnable()
    g_ScriptEvent:AddListener("MengHuaLuUpdateBabyBuffs", self, "UpdateBabyBuffs")
    g_ScriptEvent:AddListener("EnterMengHuaLuNewRound", self, "EnterMengHuaLuNewRound")

end

function LuaMengHuaLuBuffListWnd:OnDisable()
    g_ScriptEvent:RemoveListener("MengHuaLuUpdateBabyBuffs", self, "UpdateBabyBuffs")
    g_ScriptEvent:RemoveListener("EnterMengHuaLuNewRound", self, "EnterMengHuaLuNewRound")
end

function LuaMengHuaLuBuffListWnd:EnterMengHuaLuNewRound()
    self:UpdateBuffs()
end

-- 进入新的一轮，buff轮次减1（废弃）
function LuaMengHuaLuBuffListWnd:UpdateBuffRound()
    for i = 0, self.BabyInfo.BuffDataList.Count-1 do
        local buffInfo = self.BabyInfo.BuffDataList[i]
        local lastRound = buffInfo[1]
        if lastRound > 0 then
            lastRound = lastRound - 1
        end
        self.BabyInfo.BuffDataList[i][1] = lastRound
    end
    self:UpdateBabyBuffs()
end

function LuaMengHuaLuBuffListWnd:ClickThroughToClose()
    if Input.GetMouseButtonDown(0) then       
        if (not CUICommonDef.IsOverUI or (UICamera.lastHit.collider and
           not CUICommonDef.IsChild(self.transform, UICamera.lastHit.collider.transform) and
            CUICommonDef.GetNearestPanelDepth(self.transform) > CUICommonDef.GetNearestPanelDepth(UICamera.lastHit.collider.transform))) then
            CUIManager.CloseUI(CLuaUIResources.MengHuaLuBuffListWnd)
        end
    end
end

return LuaMengHuaLuBuffListWnd