local MessageWndManager = import "L10.UI.MessageWndManager"
local CTooltipAlignType=import "L10.UI.CTooltip+AlignType"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local QnRadioBox = import "L10.UI.QnRadioBox"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local QnAddSubAndInputButton=import "L10.UI.QnAddSubAndInputButton"
local CItemMgr = import "L10.Game.CItemMgr"
local QnTableView=import "L10.UI.QnTableView"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local UILongPressButton = import "L10.UI.UILongPressButton"
local CItemCountUpdate = import "L10.UI.CItemCountUpdate"
local Item_Item = import "L10.Game.Item_Item"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local ServerSubmit_SubmitItem=import "L10.Game.ServerSubmit_SubmitItem"
local CUITexture = import "L10.UI.CUITexture"
local UISlider= import "UISlider"

CLuaServerSubmitWnd =class()
RegistClassMember(CLuaServerSubmitWnd,"m_TableView")
RegistClassMember(CLuaServerSubmitWnd,"m_ItemIds")
RegistClassMember(CLuaServerSubmitWnd,"m_ItemAddLookup")
RegistClassMember(CLuaServerSubmitWnd,"m_NumButton")
RegistClassMember(CLuaServerSubmitWnd,"m_SelectedTemplateId")
RegistClassMember(CLuaServerSubmitWnd,"m_BindLabel")
RegistClassMember(CLuaServerSubmitWnd,"m_NotBindLabel")
RegistClassMember(CLuaServerSubmitWnd,"m_RadioBox")
RegistClassMember(CLuaServerSubmitWnd,"m_CostLabel1")
RegistClassMember(CLuaServerSubmitWnd,"m_CostLabel2")
RegistClassMember(CLuaServerSubmitWnd,"m_Slider")
RegistClassMember(CLuaServerSubmitWnd,"m_DescLabel")
RegistClassMember(CLuaServerSubmitWnd,"m_SubmitTotalNum")
RegistClassMember(CLuaServerSubmitWnd,"m_Formula1")
RegistClassMember(CLuaServerSubmitWnd,"m_Formula2")
RegistClassMember(CLuaServerSubmitWnd,"m_SubmitButton")

CLuaServerSubmitWnd.s_Id=1
CLuaServerSubmitWnd.s_SubmitNum=0
CLuaServerSubmitWnd.s_TotalNum=0

function CLuaServerSubmitWnd:Init()
    self.m_ItemAddLookup={}
    self.m_SubmitTotalNum=0

    self.m_DescLabel = FindChild(self.transform,"DescLabel"):GetComponent(typeof(UILabel))
    self.m_Slider=FindChild(self.transform,"Slider"):GetComponent(typeof(UISlider))
    self.m_Slider.value=0

    local tipButton = FindChild(self.transform,"TipButton").gameObject
    UIEventListener.Get(tipButton).onClick = DelegateFactory.VoidDelegate(function(go)
        g_MessageMgr:ShowMessage("ServerSubmit_Tip")
    end)

    self.m_SubmitButton = FindChild(self.transform,"SubmitButton").gameObject
    UIEventListener.Get(self.m_SubmitButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnClickSubmitButton(go)
    end)
    self.m_BindLabel = FindChild(self.transform,"BindLabel"):GetComponent(typeof(UILabel))
    self.m_NotBindLabel = FindChild(self.transform,"NotBindLabel"):GetComponent(typeof(UILabel))
    self.m_RadioBox = FindChild(self.transform,"RadioBox"):GetComponent(typeof(QnRadioBox))
    self.m_CostLabel1 = FindChild(self.m_RadioBox.transform,"Cost1"):GetComponent(typeof(UILabel))
    self.m_CostLabel2 = FindChild(self.m_RadioBox.transform,"Cost2"):GetComponent(typeof(UILabel))
    self.m_CostLabel1.text="0"
    self.m_CostLabel2.text="0"
    -- self.m_RadioBox.transform:GetChild(0):GetComponent(typeof(UILabel)).text="0"
    -- self.m_RadioBox.transform:GetChild(1):GetComponent(typeof(UILabel)).text="0"

    self.m_NumButton=FindChild(self.transform,"QnIncreseAndDecreaseButton"):GetComponent(typeof(QnAddSubAndInputButton))
    self.m_NumButton:SetMinMax(0,0,1)
    self.m_NumButton.onValueChanged = DelegateFactory.Action_uint(function (value) 
        self:OnNumChanged(value)
    end)
    self.m_ItemIds={}
    local counts = {}
    local Index = {}
    local data = ServerSubmit_SubmitItem.GetData(CLuaServerSubmitWnd.s_Id)

    if data then
        self.m_SubmitTotalNum = data.DaySubmitLimit
        local array=data.Items
        for i=1,array.Length,2 do
            local id = array[i-1]
            table.insert( self.m_ItemIds, id )
            self.m_ItemAddLookup[id] = array[i]
            local bindCount,notBindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(id)
            counts[id] = bindCount+notBindCount
            Index[id] = i
        end
        self.m_Slider.value=CLuaServerSubmitWnd.s_TotalNum/data.ServerSubmitValue
        FindChild(self.m_Slider.transform,"ProgressLabel"):GetComponent(typeof(UILabel)).text=SafeStringFormat3("%d/%d",CLuaServerSubmitWnd.s_TotalNum,data.ServerSubmitValue)

        self.m_Formula1 = AllFormulas.Action_Formula[data.formulaId1] and AllFormulas.Action_Formula[data.formulaId1].Formula
        self.m_Formula2 = AllFormulas.Action_Formula[data.formulaId2] and AllFormulas.Action_Formula[data.formulaId2].Formula


        table.sort(self.m_ItemIds,function(a,b)
            if counts[a] == counts[b] then
                return Index[a] < Index[b]
            else
                return counts[a] > counts[b]
            end
        end)
    end

    self.m_TableView = FindChild(self.transform,"ScrollView"):GetComponent(typeof(QnTableView))

    self.m_TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function() return #self.m_ItemIds end,
        function(item,row)
            self:InitItem(item,row)

        end)
    self.m_TableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
        self.m_SelectedTemplateId = self.m_ItemIds[row+1]
        self:InitDetails( self.m_SelectedTemplateId )

        --数量为0 则显示获取途径
        local bindCount,notBindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(self.m_SelectedTemplateId)
        if bindCount+notBindCount==0 then
            local tf = self.m_TableView:GetItemAtRow(row).transform
            CItemAccessListMgr.Inst:ShowItemAccessInfo(self.m_SelectedTemplateId, false, tf, CTooltipAlignType.Right)
        end
    end)
    self.m_TableView:SetSelectRow(0,true)
    self.m_TableView:ReloadData(false, false)
end

function CLuaServerSubmitWnd:InitDetails(id)
    local tf = FindChild(self.transform,"SelectedItem")

    local icon=FindChild(tf,"IconTexture"):GetComponent(typeof(CUITexture))
    local nameLabel = FindChild(tf,"NameLabel"):GetComponent(typeof(UILabel))

    local item = Item_Item.GetData(id)
    if item then
        icon:LoadMaterial(item.Icon)
        nameLabel.text = item.Name
    end

    self:InitSelectedItemCount()
end

function CLuaServerSubmitWnd:OnClickSubmitButton(go)
    local point = self.m_NumButton:GetValue()*self.m_ItemAddLookup[self.m_SelectedTemplateId]
    local type = self.m_RadioBox.CurrentSelectIndex+1

    local data = ServerSubmit_SubmitItem.GetData(CLuaServerSubmitWnd.s_Id)
    if data and CLuaServerSubmitWnd.s_TotalNum + point > data.ServerSubmitValue then
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("ServerSubmit_Over_Progress"), DelegateFactory.Action(function ()
            Gac2Gas.RequestSubmitServerSubmitItem( CLuaServerSubmitWnd.s_Id, self.m_SelectedTemplateId,self.m_NumButton:GetValue(),type)
        end), nil, nil, nil, false)
    else
        Gac2Gas.RequestSubmitServerSubmitItem( CLuaServerSubmitWnd.s_Id, self.m_SelectedTemplateId,self.m_NumButton:GetValue(),type)
    end
end

function CLuaServerSubmitWnd:OnNumChanged(value)
    local point = value*self.m_ItemAddLookup[self.m_SelectedTemplateId]
    self.m_DescLabel.text=g_MessageMgr:FormatMessage("ServerSubmit_Desc", point, self.m_SubmitTotalNum-CLuaServerSubmitWnd.s_SubmitNum, self.m_SubmitTotalNum)
    local level = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Level or 0
    self.m_CostLabel1.text = self.m_Formula1 and self.m_Formula1(nil,nil,{point,level}) or "0"
    self.m_CostLabel2.text = self.m_Formula2 and self.m_Formula2(nil,nil,{point}) or "0"

    if value>0 then
        CUICommonDef.SetActive(self.m_SubmitButton,true,true)
    else
        CUICommonDef.SetActive(self.m_SubmitButton,false,true)
    end
end

function CLuaServerSubmitWnd:InitSelectedItemCount()
    local bindCount,notBindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(self.m_SelectedTemplateId)
    self.m_BindLabel.text = LocalString.GetString("绑定")..SafeStringFormat3("[c][ffffff]x%d[-][/c]",bindCount)
    self.m_NotBindLabel.text = LocalString.GetString("非绑定")..SafeStringFormat3("[c][ffffff]x%d[-][/c]",notBindCount)

    local count = bindCount+notBindCount
    local maxCount = math.floor( (self.m_SubmitTotalNum-CLuaServerSubmitWnd.s_SubmitNum)/self.m_ItemAddLookup[self.m_SelectedTemplateId])
    if count>0 then
        self.m_NumButton:SetMinMax(1,math.min(count,maxCount),1)
        self.m_NumButton:SetValue(1,true)
    else
        self.m_NumButton:SetMinMax(0,0,1)
        self.m_NumButton:SetValue(0,true)
    end

end


function CLuaServerSubmitWnd:InitItem(item,row)
    local tf = item.transform
    local go = item.gameObject

    local id = self.m_ItemIds[row+1]
    FindChild(tf,"BindSprite").gameObject:SetActive(false)
    local icon=FindChild(tf,"Icon"):GetComponent(typeof(CUITexture))
    local item = Item_Item.GetData(id)
    if item then
        icon:LoadMaterial(item.Icon)
    end
    local mask=FindChild(go.transform,"GetNode").gameObject
    local countUpdate=go:GetComponent(typeof(CItemCountUpdate))
    countUpdate.templateId=id
    
    local function OnCountChange(val)
        if val>0 then
            mask:SetActive(false)
            countUpdate.format="{0}"
        else
            mask:SetActive(true)
            countUpdate.format="[ff0000]{0}[-]"
        end
        if id==self.m_SelectedTemplateId then
            self:InitSelectedItemCount()
        end
    end
    countUpdate.onChange=DelegateFactory.Action_int(OnCountChange)
    countUpdate:UpdateCount()

    local longPressCmp = go:GetComponent(typeof(UILongPressButton))
    longPressCmp.OnLongPressDelegate = DelegateFactory.Action(function()
        CItemInfoMgr.ShowLinkItemTemplateInfo(id, false, nil, CItemInfoMgr.AlignType.Default, 0, 0, 0, 0)
    end)
end

function CLuaServerSubmitWnd:OnEnable()
    g_ScriptEvent:AddListener("ReplyServerSubmitInfo", self, "OnReplyServerSubmitInfo")

end
function CLuaServerSubmitWnd:OnDisable()
    g_ScriptEvent:RemoveListener("ReplyServerSubmitInfo", self, "OnReplyServerSubmitInfo")
end
function CLuaServerSubmitWnd:OnReplyServerSubmitInfo(id,submitNum,totalNum)
    CLuaServerSubmitWnd.s_Id = id
    CLuaServerSubmitWnd.s_SubmitNum = submitNum
    CLuaServerSubmitWnd.s_TotalNum = totalNum

    self:InitSelectedItemCount()
    self:OnNumChanged(self.m_NumButton:GetValue())

    local data = ServerSubmit_SubmitItem.GetData(CLuaServerSubmitWnd.s_Id)
    if data then
        self.m_Slider.value=CLuaServerSubmitWnd.s_TotalNum/data.ServerSubmitValue
        FindChild(self.m_Slider.transform,"ProgressLabel"):GetComponent(typeof(UILabel)).text=SafeStringFormat3("%d/%d",CLuaServerSubmitWnd.s_TotalNum,data.ServerSubmitValue)
    end
end
