local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local UISprite = import "UISprite"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"

LuaBQPSponsorListWnd=class()

RegistClassMember(LuaBQPSponsorListWnd,"TableViewDataSource")
RegistClassMember(LuaBQPSponsorListWnd,"TableView")
RegistClassMember(LuaBQPSponsorListWnd,"SponsorBtn")
RegistClassMember(LuaBQPSponsorListWnd,"HintLabel")
RegistClassMember(LuaBQPSponsorListWnd,"HelpBtn")

RegistClassMember(LuaBQPSponsorListWnd,"SponsorList")

function LuaBQPSponsorListWnd:Init()
    self.SponsorList = LuaBingQiPuMgr.m_SponsorList
    self.TableView = self.transform:Find("Anchor/Middle/TableView"):GetComponent(typeof(QnTableView))
    self.SponsorBtn = self.transform:Find("Anchor/Bottom/SponsorBtn").gameObject
    self.HintLabel =  self.transform:Find("Anchor/Bottom/HintLabel"):GetComponent(typeof(UILabel))
    self.HelpBtn = self.transform:Find("Anchor/Bottom/HelpBtn")
    self.HintLabel.text = g_MessageMgr:FormatMessage("BQP_SPONSOR_TIP")
    
    -- 我要赞助
    local onSponsor = function (go)
        self:OnSponsorButtonClick(go)
    end
    CommonDefs.AddOnClickListener(self.SponsorBtn,DelegateFactory.Action_GameObject(onSponsor),false)

    if self.HelpBtn then
		local onhelp = function(go)
			self:OnHelpBtnClick()
		end
		CommonDefs.AddOnClickListener(self.HelpBtn.gameObject,DelegateFactory.Action_GameObject(onhelp),false)
	end

    local function initItem(item,index)
        if index % 2 == 1 then
            item:SetBackgroundTexture("common_bg_mission_background_n")
        else
            item:SetBackgroundTexture("common_bg_mission_background_s")
        end
        self:InitItem(item,index)
    end

    self.TableViewDataSource=DefaultTableViewDataSource.CreateByCount(#self.SponsorList,initItem)
    self.TableView.m_DataSource=self.TableViewDataSource
    self.TableView.OnSelectAtRow=DelegateFactory.Action_int(function (row)
        self:OnSelectAtRow(row)
    end)
    self.TableView:ReloadData(true,false)

end

function LuaBQPSponsorListWnd:OnEnable()
    g_ScriptEvent:AddListener("BQPUpdateSponsorList", self, "BQPUpdateSponsorList")
end

function LuaBQPSponsorListWnd:OnDisable()
    g_ScriptEvent:RemoveListener("BQPUpdateSponsorList", self, "BQPUpdateSponsorList")
end

function LuaBQPSponsorListWnd:BQPUpdateSponsorList()
    self.SponsorList = LuaBingQiPuMgr.m_SponsorList

    self.TableViewDataSource.count=#self.SponsorList
    self.TableView:ReloadData(true,false)
end

function LuaBQPSponsorListWnd:InitItem(item, index)
    local info = self.SponsorList[index+1]

    local go = item.gameObject
    local RankLabel = go.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
    RankLabel.text=""
    local RankImage = go.transform:Find("RankLabel/RankImage"):GetComponent(typeof(UISprite))
    RankImage.spriteName=""
    local NameLabel = go.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local EquipLabel = go.transform:Find("EquipLabel"):GetComponent(typeof(UILabel))
    local SponsorLabel = go.transform:Find("SponsorLabel"):GetComponent(typeof(UILabel))

    local color = ""
    if CClientMainPlayer.Inst and info.ownerId == CClientMainPlayer.Inst.Id then
        color = "00FF60"
        NameLabel.text = SafeStringFormat3("[%s]%s[-]", color,CClientMainPlayer.Inst.Name)
    else
        color = "FFFFFF"
        NameLabel.text = SafeStringFormat3("[%s]%s[-]", color,info.ownerName)
    end

    local rank=info.rankPos
    if rank==1 then
        RankImage.spriteName="Rank_No.1"
    elseif rank==2 then
        RankImage.spriteName="Rank_No.2png"
    elseif rank==3 then
        RankImage.spriteName="Rank_No.3png"
    else
        if rank > 0 then
            RankLabel.text = SafeStringFormat3("[%s]%s[-]", color, rank)
        else
            RankLabel.text = SafeStringFormat3("[%s]%s[-]", color, "-")
        end
    end

    if String.IsNullOrEmpty(info.equipName) then
        local equip = EquipmentTemplate_Equip.GetData(info.templateId)
        EquipLabel.text = equip and SafeStringFormat3("[%s]%s[-]", color, equip.Name)
    else
        EquipLabel.text = SafeStringFormat3("[%s]%s[-]", color,info.equipName)
    end

    local invest = math.floor(info.invest / 10000)
    SponsorLabel.text = SafeStringFormat3(LocalString.GetString("[%s]%d万[-]"), color, invest)
end


function LuaBQPSponsorListWnd:OnSelectAtRow(row)
    local info = self.SponsorList[row+1]
    if not info then return end
    CPlayerInfoMgr.ShowPlayerPopupMenu(info.ownerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), CPlayerInfoMgr.AlignType.Default)
end


function LuaBQPSponsorListWnd:OnSponsorButtonClick(go)
    CUIManager.ShowUI(CLuaUIResources.BQPMySponsorWnd)
end

function LuaBQPSponsorListWnd:OnHelpBtnClick()
	g_MessageMgr:ShowMessage("BQP_SPONSOR_DESC")
end