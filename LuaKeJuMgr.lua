local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local CSocialWndMgr = import "L10.UI.CSocialWndMgr"
local CChatHelper = import "L10.UI.CChatHelper"
local EChatPanel = import "L10.Game.EChatPanel"
local CKeJuMgr = import "L10.Game.CKeJuMgr"
local CTeamMgr = import "L10.Game.CTeamMgr"

LuaKeJuMgr = class()

function LuaKeJuMgr:SendXiangShiChoiceInfo(choiceInfo, questionId)
    g_ScriptEvent:BroadcastInLua("KeJuQuestionAnswerUpdatePercentage", self:CalculatePercentage(choiceInfo),questionId)
end

function LuaKeJuMgr:SendHuiShiChoiceInfo(choiceInfo)
    g_ScriptEvent:BroadcastInLua("KeJuQuestionAnswerUpdatePercentage",self:CalculatePercentage(choiceInfo))
end

function LuaKeJuMgr:SendDianShiChoiceInfo(choiceInfo)
    g_ScriptEvent:BroadcastInLua("KeJuQuestionAnswerUpdatePercentage", self:CalculatePercentage(choiceInfo))
end

function LuaKeJuMgr:CalculatePercentage(choiceInfo)
    local arr = MsgPackImpl.unpack(choiceInfo)
    local sum = 0
    local len = arr.Count - 1
    for i = 0, len do
        sum = sum + arr[i]
    end
    for i = 0, len do
        arr[i] = (sum ~= 0) and (arr[i] / sum) or 0
    end
    return arr
end

function LuaKeJuMgr:ShowXiangshiPopupMenu(obj)
    local popupList={}
    table.insert( popupList,PopupMenuItemData(LocalString.GetString("分享至世界"), DelegateFactory.Action_int(function(index) 
        self:ShareXiangshiQuestion(EChatPanel.World)
    end),false, nil, EnumPopupMenuItemStyle.Default))
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst:IsInGuild() then
        table.insert( popupList,PopupMenuItemData(LocalString.GetString("分享至帮会"), DelegateFactory.Action_int(function(index) 
            self:ShareXiangshiQuestion(EChatPanel.Guild)
        end),false, nil, EnumPopupMenuItemStyle.Default))
    end
    if CTeamMgr.Inst:TeamExists() then
        table.insert( popupList,PopupMenuItemData(LocalString.GetString("分享至队伍"), DelegateFactory.Action_int(function(index) 
            self:ShareXiangshiQuestion(EChatPanel.Team)
        end),false, nil, EnumPopupMenuItemStyle.Default))
    end
    local array=Table2Array(popupList,MakeArrayClass(PopupMenuItemData))
    local side = CPopupMenuInfoMgr.AlignType.Right
    CPopupMenuInfoMgr.ShowPopupMenu(array,obj.transform, side, 1, nil, 600, true, 220)
end

function LuaKeJuMgr:ShareXiangshiQuestion(channel)
    local optionLabelArray = {"A.","B.","C.","D."}
    local optionText = ""
    for i = 0,CKeJuMgr.Inst.AnswerList.Count - 1 do
        local optionLabel = optionLabelArray[i + 1]
        local answer = CKeJuMgr.Inst.AnswerList[i]
        optionLabel = optionLabel .. answer
        optionText = optionText .. ((i > 0) and " " or "") .. optionLabel
    end
    local questionText = CKeJuMgr.Inst.Question
    local link = g_MessageMgr:FormatMessage("KEJU_XIANGSHI_SHARE_QUESTION", questionText, optionText)
    CChatHelper.SendMsgWithFilterOption(channel,link,0, true)
    CSocialWndMgr.ShowChatWnd();
end