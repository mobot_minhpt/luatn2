local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnTableView = import "L10.UI.QnTableView"
local CButton = import "L10.UI.CButton"
local Animation = import "UnityEngine.Animation"

LuaYaoYeManJuanStateWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaYaoYeManJuanStateWnd, "Leaves", "Leaves", QnTableView)
RegistChildComponent(LuaYaoYeManJuanStateWnd, "ExpandButton", "ExpandButton", CButton)

--@endregion RegistChildComponent end
RegistClassMember(LuaYaoYeManJuanStateWnd, "m_LeafInfos")

RegistClassMember(LuaYaoYeManJuanStateWnd, "m_Stage")
RegistClassMember(LuaYaoYeManJuanStateWnd, "m_CurLeaf")

function LuaYaoYeManJuanStateWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    UIEventListener.Get(self.ExpandButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnExpandButtonClick()
	end)

    self.Leaves.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_LeafInfos
        end,
        function(item, index)
            self:InitItem(item, index, self.m_LeafInfos[index+1])
        end
    )
end

function LuaYaoYeManJuanStateWnd:Init()
    self.m_Stage = LuaYaoYeManJuanMgr.m_Stage
    self.m_CurLeaf = LuaYaoYeManJuanMgr.m_CurLeaf
    self.m_LastLeaf = self.m_CurLeaf

    self:Refresh()
end

--@region UIEvent
function LuaYaoYeManJuanStateWnd:OnExpandButtonClick()
	LuaUtils.SetLocalRotation(self.ExpandButton.transform,0,0,0)
	CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end
--@endregion UIEvent
function LuaYaoYeManJuanStateWnd:OnEnable()
    g_ScriptEvent:AddListener("YaoYeManJuan_SyncPlayState", self, "OnYaoYeManJuan_SyncPlayState")
end

function LuaYaoYeManJuanStateWnd:OnDisable()
    g_ScriptEvent:RemoveListener("YaoYeManJuan_SyncPlayState", self, "OnYaoYeManJuan_SyncPlayState")
end

function LuaYaoYeManJuanStateWnd:InitItem(item, index, info)
    local foreground= item.transform:Find("Foreground")
    local fx        = item.transform:Find("Foreground/CUIFx01").gameObject
    local ani       = item:GetComponent(typeof(Animation))
    foreground.gameObject:SetActive(info)

    local needShowFx = self.m_LastLeaf <= index and index < self.m_CurLeaf
    fx:SetActive(needShowFx)
    if needShowFx then
        ani:Play("yaoyemanjuanstatewnd_yezi")
    end
end

function LuaYaoYeManJuanStateWnd:OnYaoYeManJuan_SyncPlayState(PlayState, EnterStateTime, NextStateTime, ExtraInfoTbl)
    self.m_Stage = PlayState
    if self.m_Stage == EnumYaoYeManJuanPlayState.Stage_1 then
        self.m_LastLeaf = self.m_CurLeaf
        self.m_CurLeaf = (ExtraInfoTbl or {0})[1]
    end

    self:Refresh()
end

function LuaYaoYeManJuanStateWnd:Refresh()
    local curLeaf = self.m_CurLeaf

    self.m_LeafInfos = {}
    for i = 1, 9 do
        local show = i <= curLeaf
        table.insert(self.m_LeafInfos, show)
    end
    
    self.Leaves:ReloadData(false, false)
end

function LuaYaoYeManJuanStateWnd:OnDestroy()
end