return
{
	{"ServerDebug", "sU"},
	{"SetSwitch", "si"},
	{"AddClientPatch", "iS"},

	{"ShakeHand", "ibI"},

	{"CreateMainPlayer", "dsIICdUUUUUUUUUUUUUU"},
	{"DetachPlayer", ""},
	{"UpdatePlayerReconnectTicket", "ss"}, -- requestOldTicket, extra
	{"UpdatePlayerReconnectTicketDone", "s"}, -- newTicket

	{"SendReconnectResult", "i"}, -- result 0:failed 1:success

	{"CreateObject", "IIU"},
	{"HideObject", "I"},

	{"SetTarget", "IUb"},
	{"ClearTarget", "I"},
	{"CharacterChangeTarget", "II"}, -- characterEngineId, targetEngineId

	{"UpdateCooldown", "II"},
	{"StoreOldCooldown", "II"},
	{"UpdateCooldownDetails", "III"},

	{"DoTeleport", "dsIs"},
	{"RequestDisconnect", "I"}, -- reason
	{"HeartBeatDone", "Idd"}, -- HeartBeatNum, serverTime, clientTime
	{"DisconnectPlayer", "b"}, -- bUniSdkLogout

	{"RequestUseSkillFail","IIIU"},
	{"RequestUseSkillSuccess","IIIII"},
	{"OnInterruptLeading", "I"},

	{"SetActiveSkillSuite2", "I"},
	{"SetCurrentActiveSkillSuite", "I"}, --suiteIdx
	{"GMRemoveSkill", "III"},  -- skillCls, orignalSkillId, skillId

	{"OpenDialog", "IIIUSI"},
	{"CloseDialog", ""},

	-- COMMENT : for antihack
	{"SendPlayerUniqueStatusInfo", "U"},

	{"PickItemFailed", "I"},
	{"SendItem", "bU"},
	{"SetItemAt", "IIs"},

	{"SetWeather", "I"},
	{"SendServerVersion", "s"},
	{"SendMapFullName", "IsI"}, -- mapTemplateId, mapFullName, mapIdx

	{"SendPlayerNewName", "ss"}, -- newName, oldName
	{"OpenRenameWnd", ""},
	{"VipLevelUp", "ii"}, -- newVipLevel, oldVipLevel

	{"QueryOtherClientInfo", ""},

	{"SyncQiangqinPlayers", "Udd"}, -- qiangqinPlayerIds, groomId, brideId
	{"NotifyStartQiangqinApply", ""}, --
	{"NotifyEndQiangqinApply", ""}, --
	{"NotifyOpenQiangqinBiaoBaiInputWnd", "sI"}, --playerName, passTime
	{"UpdateQiangqinBiaoBaiVotes", "ssdidi"},  -- groomName, brideName, playerId1, vote1, playerId2, vote2
	{"NotifyOpenQiangqinVoteWnd", "dsssdsssi"}, -- playerId1, playerName1, targetName1, msg1, playerId2, playerName2, targetName2, msg2, pastTime
	{"NotifyWeddingQiangqinApplySuccess", ""},
	{"NotifyBuyQiangqinCostItem", ""},
	{"UpdateShowHideQiangqinPlayer", "b"}, -- isHide
	{"QueryWeddingFireworksResult", "U"}, -- fireworksCount

	-- query
	{"SendQueryPlayerInfoResult_NotExist", "d"},
	{"SendQueryPlayerInfoResult_Offline", "dsIIsddssIIIIiUUdUiiisIUIIIIds"},
	{"SendQueryPlayerInfoResult_Online_Begin", "dsIIIsddssIIIIiUUdUiiisssIUIIIIds"},
	{"SendQueryPlayerInfoResult_Online_Equipment", "dIsU"},
	{"SendQueryPlayerInfoResult_Online_End", "dIIiUbU"},
	{"SendQueryLingShouInfoResult_NotExist", "s"},
	{"SendQueryLingShouInfoResult_Offline", "s"},
	{"SendQueryLingShouInfoResult", "sU"},
	{"SendQueryLingShouOverview_NotExist", "d"},
	{"SendQueryLingShouOverview_Offline", "d"},
	{"SendQueryLingShouOverviewResult", "dIU"},

	{"SendQueryMapiInfoResult_NotExist", "s"},
	{"SendQueryMapiInfoResult_Offline", "s"},
	{"SendQueryMapiInfoResult", "sUUI"},
	{"SendQueryMapiOverview_NotExist", "d"},
	{"SendQueryMapiOverview_Offline", "d"},
	{"SendQueryMapiOverviewResult", "dIUU"},

	{"SendQueryBabyInfoResult_NotExist", "s"},
	{"SendQueryBabyInfoResult", "sdIIsdIIUU"},

	{"SendQueryCityFlagResult", "db"},
	{"SendQueryCityInfoResult_NotExist", "d"},
	{"SendQueryCityInfoResult", "dUUUU"},

	{"SendInteractMenuData_Success", "dsIIidiiiiIIUI"}, --targetId, name, grade, class, gender, xiuwei, expression, expressionTxt, sticker, context, SERVER_GROUP_ID, xianfanStatus, profileInfo_U, yingLingState
	{"SendInteractMenuData_Fail", "diI"}, --targetId, context, SERVER_GROUP_ID
	{"SendPlayerName_Success", "dsi"}, -- targetId, name, context
	{"SendPlayerName_Fail", "di"}, --targetId, context
	{"UpdateLingShouSkillSlotLockState", "sI"}, --lingshouId, pos, isLocked

	{"SendQueryItemInfoResult_NotExist", "s"},
	{"SendQueryItemInfoResult", "bsU"},

	-- rank
	{"SendPlayerRank", "IUIdUsIIsII"},
	{"SendGuildRank", "IUIdsII"},
	{"SendHouseRank", "IUIdsssdd"},
	{"SendBabyRank", "IUIdsIssd"},

	--fight
	{"ShowActionDirection","Ii"},
	{"ShowActionState","IC"},
	{"ShowActionStateWithSource","ICI"}, -- engineId, as, sourceId
	{"ShowExpressionActionState", "II"},
	{"ShowSkillActionState","ICIbIiidd"},
	{"ShowSkillCtrlResult","Is"}, --engineId, name
	{"ShowSkillCtrlNum","is"}, --num, name
	{"ShowSkillRmCtrlNum","i"}, --num
	{"ShowMultiBulletFx","IIU"},
	{"UpdateFightParam","IId"},
	{"UpdateHpHurt","IIdbII"}, -- attackerEngineId, defenderEngineId, value, bFatal, tipId, tipLv
	{"UpdateMpChg","IId"}, -- attackerEngineId, defenderEngineId, value
	{"UpdateHpHurtWithType","IIdbI"}, -- attackerEngineId, defenderEngineId, value, bFatal, TextType
	{"UpdateCurrentHpFullHurt","Id"},
	{"SendBenefitCharacterId", "Id"}, -- selfEngineObjectId, benefitCharacterId
	{"ChainHealing", "IIU"}, -- attackerId, skillId, msgpack.pack(defenderIds)
	--{"SendBenefitTeamId", "Id"}, -- selfEngineObjectId, benefitTeamId
	{"ShowPreSkillEffect", "IU"}, -- engineId, msgpack.pack(args)
	{"ShowCastingAlert", "IIU"}, -- engineId, sourceId, msgpack.pack(CastingTargetExtraInfo)

	{"Reborn", "U"},
	{"SetHp", "Id"},
	{"SetCurrentHpFull", "Id"}, -- engineId, currentHpFull


	{"BeginCastSkill", "II"},
	{"EndCastSkill", "I"},
	{"BeginLightningChain", "III"},
	{"EndLightningChain", "I"},

	{"SetPlayerPkProactive", "Ii"}, -- engineId, isActive
	{"UpdatePlayerPKMode", "II"}, -- engineId, pkMode
	{"UpdatePkProtect", "III"}, -- engineId, EnumPkProtect, value
	{"UpdatePlayerPKValue", "Ii"}, -- engineId, pkValue
	{"AddPersonalChallengeTarget", "Id"}, -- challengeSrcEngineId, challengeTargetPlayerId
	{"DelPersonalChallengeTarget", "Id"}, -- challengeSrcEngineId, challengeTargetPlayerId
	{"AddSavePicSuccess", "s"}, -- picId

	-- guild
	{"SendGuildInfo", "Us"}, -- infoUD, infoType
	{"SendGuildInfoEnd", "s"}, -- infoType
	{"RequestOperationInGuildSucceed", "ss"}, -- requestType, paramStr
	{"GuildNotification", "si"}, -- power, value
	{"OpenGuildWnd", ""},
	{"OpenMarketWnd", "ii"}, -- a, b
	{"QueryPlayerGuildInfoDone", "idUbU"}, -- reason, guildId, guildInfo, bInfoExists, memberInfo
	{"TryInvitedAsGuildMember", "dss"}, -- inviterId, inviterName, guildName

	{"OpenGuildMergeWnd", ""},
	{"RequestSetGuildRightsSuccess", ""},

	--帮会宣战
	{"GC_OpenAllGuildInfoWndBegin", ""}, --
	{"GC_OpenAllGuildInfoWndAdd", "diiiSSii"}, --guildId, guildScale, memberNum, maxMemberNum, guildName, leaderName, status, fightTime
	{"GC_OpenAllGuildInfoWndEnd", "b"}, --, bHasOp
	{"GC_OpenChallengeGuildWnd", "ddU"}, -- cost, silver, timeTblUD
	{"GC_CloseChallengeGuildWnd", ""}, --
	{"GC_AcceptOrRejectSuccess", ""}, --
	{"GC_OpenFightDetailWnd", "SSiiUUddII"}, --fromGuildName, targetGuildName, fromKillScore, targetKillScore, fromPlayerDetailUD, targetPlayerDetailUD, fromOccupationScore, targetOccupationScore, fromOccupiedCount, targetOccupiedCount
	{"GC_StartFight", "ddSSib"}, --guildId, targetGuildId, guildName, targetGuildName, endTime, bSceneAllow
	{"GC_EndFight", ""},
	{"GC_ShowEffect", "i"}, --effectType

	-- teleport
	{"ConfirmTeleportPublicCopy", "IUiI"}, -- sceneTemplateId, sceneIds, showRandomButton, playerCountLimit

	-- misc
	{"UpdateServerTime", "d"},
	{"RequestPushInfo", ""},

	-- shop
	{"RequestOperationInMallSucceed", "ss"}, -- requestType, paramStr
	{"SendMarketItemInfo", "IU"}, -- itemId, marketItemInfoUD
	{"SendMarketItemInfoEnd", "s"}, -- infoType
	{"SendOneMarketItemInfo", "IU"}, -- itemId, marketItemInfoUD
	{"SendMallMarketItemLimit", "iU"}, -- regionId, limitItemsMsgPack
	{"NotifyJadeNotEnough", "d"}, --cost
	{"PreSellMarketItemDone", "III"}, -- itemId, itemCount, itemPrice
	{"ReplyAutoShangJiaItem", "U"}, --regionid_itemIdUD

    -- shop activity
    {"SendShopActivityInfo", "IIU"},      -- activityId, accumulation, giftStatusUD
    {"SendAvailableShopActivityIds", "U"},      -- availableActivityIds
    {"SendGetShopActivityGiftResult", "IIb"},    -- activityId, giftLevel(gid), result

	--buff
	{"AddBuff", "IId"},
	{"DeleteBuff", "II"},
	{"AddBuffData", "IIdd"},
	{"RmBuffData", "II"},
	{"TeamMemberAddBuffData", "dIdd"},
	{"TeamMemberRmBuffData", "dI"},
	{"QueryTeamMemberBuffReturn","IU"},
	{"QueryTargetBuffReturn","IU"},
	{"SyncTeamMemberBuffProp", "dU"},
	{"ResetTeam", "db"}, -- playerId, dontDestroyTeam

	--QA测试
	{"AutoQAShowMessageBox","bs"},
	{"AutoQASendServerLog","is"},

	{"ShowMessage", "IU"},
	{"ShowMessageInObjDelayQueue", "IIU"}, -- ObjEngineId, messageId, args
	{"ShowConfirmMessage", "sSIIIU"}, --sessionId, message, timeout, wndType, extraInfo
	{"CloseConfirmMessageWnd", "s"}, --sessionId
	-- {"ShowGamePlayConfirmMessage", "sISII"}, --sessionId, playId, message, timeout, type

	-- HuaBi
	{"SendHuaBiResult", "dIIdd"}, --exp, timePassed, percent, killPercent, score
	{"UpdateTeamConfirmInfo", "U"}, --teamInfo
	{"ShowTeamConfirmInfoWnd", ""}, --
	{"CloseTeamConfirmInfoWnd", ""}, --

	{"SyncBasicProp", "IU"},
	{"SyncFightProp", "IU"},
	{"SyncPlayScore", "II"}, -- scoreIdx, score
	{"SyncPlayProp", "IU"},
	{"SyncBuffProp", "IU"},
	{"SendSayContent", "ISds"},
	{"SendSayDialogId", "II"}, --engineId, dialogId

	-- gameplay
	{"PlayGameVideo", "s"},

	--任务
	{"UpdateTask","IU"},
	{"AcceptTask","IU"},
	{"GiveUpTask","I"},
	{"SubmitTask","I"},
	{"ReviewTask","I"},
	{"AddAcceptableTask","I"},
	{"DelAcceptableTask","I"},
	{"DelCurrentTask","I"},
	{"ClearAcceptableTasks",""},
	{"SyncTaskTimeCheck", "U"}, -- msgpack.pack(self.m_TaskTimeCheck)
	{"TaskTimeCheckChange", "Ii"}, -- taskId, self.m_TaskTimeCheck[taskId]

	{"OpenTaskDialog","IIIsU"},
	{"SyncStatus","IIC"},
	{"ApplyInterrupt","II"},

	{"NotifyLevelUp", "Ii"},

	{"StopTaskAutoExecute", ""},

	-- 组队
	{"TeamOperationResult", "issss"}, --eResultType, errStr1, errStr2, errStr3, errStr4
	{"ApplyForLeaderPermitJoinTeam", "dsdsiiiddIU"}, --notifierId, notifierName, playerId, playerName, playerClass, playerGrade, playerGender, playerXiuWeiGrade, playerXiuLianGrade, playerXianFanStatus, playerProfileInfo_U
	{"InvitePlayerJoinTeam", "dsisiiidddi"}, --inviterId, inviterName, teamId, teamName, inviterClass, inviterGrade, inviterGender, inviterXiuWeiGrade, inviterXiuLianGrade, teamLeaderId, matchingActivity
	{"InviteTeamAddMember", "idsiiidd"}, --teamId, memberId, memberName, memberClass, memberGrade, memberGender, memberXiuWeiGrade, memberXiuLianGrade
	{"InviteJoinTeamFeedBack", "dsi"}, --playerId, playerName, feedbackStatus
	{"TeamAddMember", "idsiiiddiiibIU"}, --teamdId, memberId, memberName, memberClass, memberGrade, memberGender, memberXiuWeiGrade, memberXiuLianGrade, expression, expressionTxt, sticker, bNew, xianfanStatus, profileInfo_U
	{"NotifyPlayerJoinTeam", "dId"}, --playerId, engineId, teamId
	{"NotifyPlayerLeaveTeam", "dId"}, --playerId, engineId, teamId
	{"TeamSetLeader", "iddb"}, -- teamId, newLeaderId, oldLeaderId, bNew
	{"UpdateTeamName", "s"}, -- teamName
	{"SetTeamDistributeType", "ii"}, --teamId, teamDistributeType
	{"SetTeamForbidRoll", "ib"}, --teamId, teamForbidRoll
	{"TeamDelMember", "ids"}, --teamId, memberId, typeString
	{"AcceptTeamFollow", "dU"}, -- playerId, followList
	{"StopTeamFollow", "dU"}, -- playerId, followList
	{"SetMemberVisible", "db"}, --memberId, bNew
	{"UpdateTeamMemberHp", "diii"}, -- memberId, hp, fullHp, maxHp
	{"UpdateTeamMemberMp", "diii"}, -- memberId, mp, fullMp, maxMp
	{"TeamDepart", "db"}, --leaderId, bShowMsg
	{"UpdateTeamMemberGrade", "dII"}, --memberId, grade, xianfanStatus
	{"SendTeamLeaderPos", "dSiiiII"}, --leaderId, sceneId, x, y, z, sceneOrgId, direction
	{"CheckIsPlayerInTeamResult", "db"}, -- playerId,  isInTeam
	{"ClearTeamJoinRequest", ""},
	{"SummonTeammate", "I"}, --WndType
	{"UpdateTeamMemberXiuWeiGrade", "idd"}, -- teamId, playerId, playerXiuWeiGrade
	{"UpdateTeamMemberXiuLianGrade", "idd"}, -- teamId, playerId, playerXiuLianGrade
	{"UpdateTeamMemberInfo", "idU"}, -- teamId, playerId, memberInfo_U
	{"QueryTeamSizeResult", "di"}, -- playerId, teamSize
	{"OpenTeamDialog", "I"}, -- activityId
	{"UpdateTeamMemberLocation", "dsIiii"}, --srcPlayerId, sceneId, sceneTemplateId, x, y, pIdx
	{"BroadcastAddTeamRequestMsg", "dssiis"}, -- requesterId, requesterName, activityName, startGrade, endGrade, content
	{"SetAutoAcceptTeamFollowResult", "b"}, --bAutoAccept
	{"QueryTeamsInSameSceneResult", "S"}, -- teamInfo
	{"UpdateTeamMemberName", "ds"}, -- playerId, newName
	{"PlayerRequestChangeLeader", "ds"}, -- requesterId, requesterName
	{"BroadcastSystemMsgInTeamChannel", "S"}, --  message
	{"BroadcastVoiceTranslationOnIM", "sdS"}, --  voiceId, voiceLength, translation


	--TeamRecruit
	{"QueryTeamRecruitResult", "UII"}, -- infoListUD, currentPageNum, pageNum
	{"QueryOwnTeamRecruitResult", "U"}, -- infoItemUD
	{"QueryTeamRecruitByIdAndTimeResult", "U"}, -- infoItemUD
	{"OpenTeamRecruitWnd", ""}, --
	{"TeamRecruitAddSuccess", "U"}, --
	{"TeamRecruitCancelSuccess", ""}, --

	{"SendMoney", "IU"},

	{"ResetGuaJiMemberFormationPosIdx", ""}, --
	{"UpdateGuaJiMemberFormationPosIdx", "dI"}, --memberId, formationPosIdx


	{"SyncGuaJiState", "U"}, -- guaji state
	{"UpdateGuaJiSceneTempId", "I"}, -- sceneTempId

	--AI
	{"ServerCastSkill", "Iii"}, --skillId"},
	{"SyncPlayerAppearance","IsccIdUHIs"}, --uint engineId, string name, sbyte gender, sbyte objClass, uint actionState, double dir, byte[] appearanceData, ushort level, uint titileId, string titleName
	{"SyncFakePlayerAppearance","IsccIdUHIs"}, --uint engineId, string name, sbyte gender, sbyte objClass, uint actionState, double dir, byte[] appearanceData, ushort level, uint titileId, string titleName

	--Forces
	{"SetEnemyCheckForPlay", "s"}, --playName

	--技能升级
	{"UpdateSkillInfo", "UUb"}, -- deletedSkills in msgpack, addedSkills in msgpack
	{"UpdateTianFuInfo", "II"}, --TianFuPointUsed, ExtraTianFuPoint

	{"UpdateSkillExpInfo", "U"}, --skillPrivateExp, skillId, skillExp (in msgpack.pack)
	{"UpdateSkillMoneyInfo", "U"}, -- skillId, skillMoney (as above)
	{"SkillInjectExpCriticalOp", "U"}, -- skillExp, skillInjectExp
	{"SkillInjectMoneyCriticalOp", "U"}, -- skillMoney, skillInjectMoney

	{"SwitchActiveSkillTemplateSuccess", "I"},  -- templateIdx

	{"UpdateActiveSkillAt", "II"}, -- newActiveSkillIdx, newActiveSkillId
	{"UpdateActiveSkill2At", "II"}, -- newActiveSkillIdx, newActiveSkillId
	{"UpdateSkillTemplate", "IsUUU"}, -- templateIdx,name, normalSkills, tianfuSkills, normalSkills2

	{"UpdateActiveSkillTemplateAt", "II"},
	{"UpdateActiveSkill2TemplateAt", "II"},

	{"UpdateActiveSkillTemplateAt_V2", "III"},
	{"UpdateActiveSkill2TemplateAt_V2", "III"},


	{"UpdateAvaliableSkillTemplate", "I"}, -- avaliableTemplateBS

	{"UpdateActiveSkillForAI", "IUU"}, -- aiSkillType, skillForAI, tianfuSkillSetForAI

	{"SyncDoubleJoystickSkillCls", "U"}, -- currentUseDoubleJoystickSkills

	-- Relationship
	{"SyncRelationshipProp", "IU"},
	{"AddFriendSpecialId", "d"},
	{"DelFriendSpecialId", "d"},
	{"QueryFriendlinessResult", "dd"}, -- playerId, friendliness
	{"QueryFriendlinessTblResult", "U"},
	{"AdjustRelationshipById", "disIiiiiiIIsU"}, -- targetId, adjustType, name, level, class, gender, expression, expressionTxt, sticker, xianfanStatus, serverGroupId, serverName, profileInfo_U
	{"RefreshRelationship", "U"}, --basicInfo:SaveToString()
	-- {"AddEnemy", "dsiIii"}, --targetId, name, hate, level, class, gender
	-- {"DelEnemy", "d"}, --toDelEnemyId
	{"SetInteractType", "dI"}, -- targetId, interactType
	{"SyncFriendLimit", "I"}, -- curFriendLimit
	{"ClearFriendRequest", ""}, -- curFriendLimit
	{"AdjustFriendGroup", "di"}, -- playerId, groupId
	{"AdjustFriendGroupWithMultiPlayer", "Ui"}, -- playerIdsData, groupId
	{"SetFriendGroupName", "is"}, -- groupId, name
	{"QueryFriendsNoGuildResult", "U"}, -- friendsNoGuild
	{"SetFriendOfflineTime", "dI"}, -- playerId, offlineTime
	{"UpdateFriendliness", "dd"}, -- playerId, friendliness
	{"DelMultiFriendResult", "I"}, -- count

	-- CangBaoTu
	{"RequestCheckUseCangBaoTuResult", "si"}, --itemId, EnumCheckUseCangBaoTuResult
	{"SendOpenedGumuInfo", "Isi"}, --playId, mapId, EnumGumuOpenStatus
	{"SyncConsumeEndInfo", ""},
	{"SyncTriggerEnterSceneEvent", ""},
	{"SyncInstantActionDone", ""},
	{"GuaJiTrackToDoCangBaoTuTask", "I"},
	{"AcceptNextCangBaoTuTaskConfirm","i"},
	{"CangBaoTuGainItem","I"}, --itemTemplateId
	-- 通知玩家可以使用命灯
	{"SyncPlayerCanUseLifeLight",""},

	-- 祈福守财
	{"SyncQiFuShouCaiBeginTrack", "IsIii"},  --targetNpcId, npcSceneId, npcSceneTemplateId, npcGridPosx, npcGridPosY

	-- 重置修为修炼
	{"RequestConfirmPlayerResetXiuweiPrice", "I"}, --jadePrice
	{"RequestConfirmPlayerResetXiulianPrice", "I"}, --jadePrice

	-- lingshou
	{"UpdateLingShouHp", "Iddd"},
	{"RequestAllLingShouOverviewResult", "IUi"}, -- count, overviewInfo, context
	{"RequestLingShouDetailsResult", "sUbUi"}, -- lingshouId, lingshou, isUnderFight, fightProp, context
	{"UpdateWashedLingShouDetails", "sUbU"}, -- lingshouId, lingshou, isUnderFight, fightProp
	{"UpdateLingShouName", "sIs"}, --lingshouId, engineId, name
	{"UpdateLingShouExp", "sd"}, --lingshouId, exp
	{"UpdateLingShouXiuwei", "sI"}, --lingshouId, xiuwei
	{"UpdateLingShouLevel", "sII"}, --lingshouId, engineId, level
	{"SendWashLingShouResult", "sU"}, --lingshouId, lingshouProp
	{"UpdateLingShouLifeSpan", "sI"}, --lingshouId, lifeSpan
	{"UpdateLingShouTemplateIdForSkin", "sII"}, --lingshouId, templateIdForSkin, evolveGrade
	{"UpdateLingShouCharacterTemplateIdForSkin", "III"}, --engineId, templateIdForSkin, evolveGrade
	{"UpdateLingShouWuxing", "sU"}, --lingshouId, lingshouProp
	{"UpdateLingShouSkillProp", "sII"}, --lingshouId, skillPos, newSkillId
	{"UpdateLingShouCharacterWuxing", "II"}, --engineId, wuxing
	{"UpdateLingShouCharacterQuality", "II"}, --engineId, quality
	{"UpdatePlayerLingShouMaxNumber", "II"}, --lingshouMaxNumber,IsUsedExtraLingShouSlotItem
	{"DeleteLingShou", "s"}, --lingshouId
	{"AddLingShou", "sU"}, --lingshouId, lingshou
	{"RequestOpenFreeLingShouWnd", ""},
	{"RequestSelectLingShou", "IIsII"}, --place, pos, itemId, lingshouTempId1, lingshouTempId2
	{"UpdateEvolveGrade", "II"}, --engineId, evolveGrade
	{"RequestOpenGetPiXiangWnd", ""},
	{"StopAutoWashLingShou", "s"}, --lingshouId
	{"RequestLastLingShouWashNoResult", "s"}, -- lingshouId
	{"RequestClearLingShouPixiang", "s"}, --lingshouId

	{"ConfirmBangHuiShangPiao", "I"}, -- guildSilverAdded
	{"UpdateAssistLingShouId", "s"}, -- lingshouId
	{"SyncLastLingShouId", "s"}, -- lingshouId
	{"LingShouJZQKSuccess", "ss"}, -- lingshouId, destLingShouId
	{"LingShouYXHDSuccess", "ss"}, -- lingshouId, destLingShouId

	{"UpdateLingShouMarryInfo", "sU"}, -- lingshouId, lingshouMarryInfo
	{"LingShouMarrySuccess", "ddIIISS"}, -- playerId, otherPlayerId, lingshouTemplateId, otherLingShouTemplateId, npcEngineId, msg1, msg2
	{"UpdateLingShouBabyLastWashResult", "sIIII"}, --lingshouId, babyQuality, babySkillCls1, babySkillCls2, washCount
	{"UpdateLingShouBabySkill2", "sI"}, --lingshouId, babySkillCls2
	{"UpdateLingShouBabyInfo", "IsU"}, --engineId, lingshouId, babyInfo
	{"UpdateLingShouBabyShowList", "IbIII"}, --engineId, isShowSelfBaby, babyTemplateId1, babyTemplateId2, babyTemplateId3
	{"RequestLingShouMarryPermission", "ss"}, --lingshouName, otherLingShouName
	{"ShowLingShouMarryDialog", ""}, --
	{"LingShouHeBaZiResult", "b"}, -- isSuccess
	{"LingShouDongFangFinish", "s"}, -- lingshouId
	{"LingShouEnterDongFangState", "s"}, -- lingshouId
	{"UpdateFuTiLingShouId", "s"}, -- lingshouId
	{"SyncLingShouPartnerInfo", "sU"}, -- lingshouId, partnerInfo_U
	{"SyncPlayerBindPartnerLingShouId", "s"}, -- lingshouId



	-- pay
	{"SendCheckOrderInfo", "sssU"}, -- pid, sn, create_time, extra_data
	{"DelayRetryChargeMonthCard", ""},
	{"SendQueryOrderResult", "ssi"}, -- pid, sn, result
	{"RequestCheckNoGetOrders", ""},
	{"SyncPlayerServerId", "is"}, -- serverId, extra

	-- IM
	{"SendMsgInfoToPlayer", "dS"}, -- sendId, sendName
	{"IMMessageIsEmpty", "d"}, -- SenderId
	{"FetchIMMessageSuccess", "ddISIISI"}, -- RecvId, SendId, EngineId, SendName, Class, Gender, Message, SendTime
	{"FetchIMMessageFailed", "d"}, -- SenderId
	{"SpeechRecovery", "dS"}, --targetId
	{"StartSessionWithGm", "d"}, --gmId
	{"StopSessionWithGm", ""}, --
	{"SetGmStatus", "i"}, -- gmStatus
	{"AddFriendLeader", "dsIIidd"}, --m_CharacterID, m_Name, m_Grade, m_Class, m_Gender, m_XiuWeiGrade, m_XiuLianGrade
	{"BroadcastMsgToFriendsSuccess", "US"}, --groupData, Message
	{"SetBroadcastSetting", "i"}, --setting
	{"QueryOnlineReverseFriendsResult", "UI"}, --data, context
	{"QueryMyGmIdResult", "d"}, --gmId
	{"SetBroadcastSettingByGroup", "ii"},
	{"SetLastReceiveMsgFromGmTime", "d"},

	--Vip GM
	{"StartSessionWithVipGm", "d"}, --gmId
	{"StopSessionWithVipGm", ""}, --
	{"SetVipGmStatus", "i"}, -- gmStatus

	--chat
	{"ShowLabaInput", "IsIIii"}, -- data.Id, data.ItemId, data.Place, data.Pos, limit, type
	{"PlayerUseLaba", "SdiiiSsUiiiUI"}, -- Name, Id, Class, Gender, Type, content, extraInfo, atListData, expression, expressionTxt, sticker, profileInfo_U, xianFanStatus
	{"SendChatMsgSuccess", "Ib"}, -- channelId, isVoice
	{"JoinChannel", "ISbS"}, -- channelId, channelName, bChangeChannel, ExtraInfoStr
	{"LeaveChannel", "IS"}, -- channelId, ExtraInfoStr
	{"SendChatMessage", "ISIdIIiiiSSbU"}, -- channelId, fromUserName, engineId, fromUserId, fromUserClass, fromUserGender, fromUserExpression, fromUserExpressionTxt, fromUserSticker, message, ExtraInfo, notShowTopTip, profileInfo_U
	{"BroadcastVoiceTranslation", "dIsS"}, --fromPlayerId, channelId, voiceId, translation
	{"ClearChatMessageByChannelId", "I"}, --channelId
	{"QueryGuildForbidSpeakResult", "U"},
	{"SendPoolMsgEnd", "I"},
	{"BeAtInChannel", "dsIs"},
	{"PlayerQueryTranslationResult", "sbSs"},

	{"HandleMailSuccess", "s"}, -- mailId
	{"HandleMailFail", "s"}, -- mailId
	{"DelMailSuccess", "si"}, --mailId, delReason
	{"DelMailFailed", "s"}, --mailId
	{"GetMailBaseReward", "IIIII"}, -- templateId, silver, freeSilver, yuanBao, exp
	{"QueryMailContentResult", "IU"},	-- ID, msgpack
	{"SendMailToPlayer", "UUU"}, --mailData, ExtraMailItemData, ExtraMailInfoData
	{"SyncMailContainer", "iU"}, --MAIL_MAXNUM, MailContainerData
	{"SendGlobalAnnouncementMail", "U"}, --GlobalAnnouncementMailData
	{"SyncGlobalAnnouncementMailBegin", ""},
	{"SyncGlobalAnnouncementMail", "U"}, --GlobalAnnouncementMailData
	{"SyncGlobalAnnouncementMailEnd", ""},
	{"SetAnnoucementMailReadList", "S"},
	{"DelGlobalAnnouncementMail", "I"}, -- AnnouncementMailId
	{"HandleMailNotDelSuccess", "s"}, -- mailId
	{"BatchDelMailBegin", ""},
	{"BatchDelMailEnd", ""},

	{"ShowBaolingdanDialog", "sII"},
	{"UpdateItemUseTimes", "dddII"},	--ItemData.TimesGroup, since, times, place, position

	--模糊搜索
	{"SearchPlayerFuzzySuccess", "U"}, -- resultData
	{"SearchPlayerSuccess", "U"}, --
	{"SearchPlayerFail", ""},
	{"GetPlayersNearby", "Ui"}, -- playersNearbyData, context

	{"AddEnemy", "Id"}, --srcEngineId, enemyId
	{"DelEnemy", "Id"}, --srcEngineId, enemyId
	{"ClearEnemy", "I"}, -- engineId

	{"InformOnline", "dbb"}, -- PlayerId, isOnline, bEnemy


	{"ShowHideNpc","IiiIb"},

	--光环
	{"AddAura","IIIb"},
	{"RmAura","III"},
	{"Immunity","Is"},

	{"CannotUseItem","s"},


	{"ShowBornEffect", "II"}, --engineId, expressionId
	--Pet
	{"RequestPetDetailsResult", "IdddddddIdddddddddddddddddddddddddddddddd"}, -- petEngineId, hp, pAttMin, pAttMax, mAttMin, mAttMax, pDef, mDef, leftTime, antiFire, antiThunder, antiIce, antiPoison, antiWind, antiLight, antiIllusion, antiWater, pFatal, pFatalDamage, mFatal, mFatalDamage
	{"PlayerRemovePet", "I"}, -- engineId

	{"SyncIsPetLingShouEnemy", "Ib"}, -- engineId, isEnemy
	{"UpdatePetBehaviorType", "i"}, -- petCtrlType

	--强化
	{"IntensifyConfirm","ii"},
	{"IntensifySuccess","i"},
	{"IntensifyFailed","s"},
	{"EquipmentSuitAppearanceUpdate","II"}, --engineId,suitId
	{"OpenIntesifySwitchDialog",""},
	{"IntesifySwitchSuccess",""},

	-- 交互菜单
	{"RequestInteractMenuDataSuccess","dsIIidIi"}, -- targetId, name, level, class, gender, XiuWeiGrade, teamSize, context
	{"RequestInteractMenuDataFail","di"}, -- targetId, context

	{"GetAppearancePropByIdResult", "dIU"}, -- targetId, context, resultData

	{"ShowDeathTimerWnd", "IiIIIs"}, --reliveStatus, leftDeathTime, costSilver, latency, wndType, sceneName
	--debug
	{"OnArkScriptError", "S"}, -- errorStr

	--洗词条
	{"BaptizeWordResult","sbU"}, --equipId,result,changedProperties
	{"RequestAutoBaptizeWordResult","sb"},-- equipId true/false
	{"BaptizeWordItemNotEnough","s"}, --洗练道具不足 equipId

	--任务进度
	{"UpdateRepeatableTask","IU"},
	{"CanAcceptNextRoundRepeatableTask","I"},

	{"SendFreeSilver", "Id"},

	{"AcceptEnterJuQing", "I"}, -- playid
	{"JuQingForceMakeTeam", "Iiii"}, -- playid, MultiPlayer, JuQingLevel, JuQingType
	{"SyncJuQingWaitInfo", "U"}, -- msgpack.pack(WaitInfoData)
	{"ShowJuQingEnterPrompt", "Iii"}, -- playId, JuQingLevel, JuQingType
	{"CloseJuQingWaitUI", ""},

	{"SetHpDrugShortcut", "I"}, --templateId
	{"OpenNpcShop","dIUUU"},
	{"ShowSkillActionResult", "Ii"}, --engineId, EnumSkillActionResult

	{"ShowSkillName", "II"}, --engineId, SkillId

	-- life skill
	{"QueryLifeSkillNeedInfoResult", "II"},		-- huoli guildContri
	{"UpgradeLifeSkillSuccessed", "II"},			-- skillId lv

	{"UpdateSpecialHpHurt","IIIU"}, -- fightType, attackerEngineId, defenderEngineId, args

	{"DeleteItem","s"},--玩家删除了一个物品(任务删除)

	--Gnjc
	{"QueryGnjcSignUpInfoResult", "IUUII"},			-- playIndex signUpTbl countTbl hour min
	{"QueryGnjcPlayInfoResult", "iiUiiUbI"},		-- defTotalScore defOccupyScore defTbl attTotalScore attOccupyScore attTbl
	{"ShowGnjcSignUpWnd", ""},
	{"ShowGnjcExchangeWnd", ""},
	{"UpdateOccupyHpInfo", "U"},					-- occupyNpcInfo
	{"UpdateAppearanceInGnjc", "bIIU"},				-- bEnter force crossFlag
	{"StartPlayCountDown", "Id"},					-- count time
	{"StartPlayCountDownByPlayId", "III"},			-- count time designPlayId
	{"StopPlayCountDown", ""},					
	{"ShowGnjcResultWnd", ""},
	{"UpdatePlayerForceInfo", "U"},					-- forceInfo
	{"ShowPlayStartFx", ""},						-- 播放开场特效
	{"UpdateGNJCInfo", "IU"},						-- 同步关宁数据
	{"ShowPlayResultFx", "I"},						-- 播放结束效果
	{"ShowGnjcNiXiFx", "I"},						-- 播放逆袭特效 engineId
	{"ShowGnjcOpenMsg", ""},						-- 显示关宁提示消息
	{"ShowGuanNingXunLianWin", "I"}, --index
	{"ReplyCrossGnjcPlayerServerName", "U"},		-- playerid和serverName 信息
	{"UpdatePlayerGnjcScore", "III"},				-- score exp freeSilver
	{"QueryLastGuanNingAwardInfoResult", "III"},	-- score exp freeSilver
	{"OpenGuanNingQueryLastAwardWnd", ""},

	{"PlayerTeleportEnd",""},
	{"SetMp","Id"},

	-- ServerTrade
	{"RequestStartTrade", "dII"}, -- playerId1, sellType
	{"TradeActorStartTrade", "II"}, --engineId, sellType
	{"PlayerCancelTrade", "Is"}, --playerEngineId, playerName
	{"SendTradeNewItem", "bU"}, -- isItem, item:SaveToString
	{"TradeShelfUpdateItem", "IibU"}, --engineId, pos, isItem, item:SaveToString
	{"TradeBagUpdateItem", "ibU"}, --pos, isItem, item:SaveToString
	{"TradeBagRmItem", "i"}, -- pos
	{"TradeShelfRmItem", "Ii"}, -- engineId, pos
	{"UpdatePlayerTradeStatus", "II"}, -- engineId, tradeStatus
	{"SendItemProp", "U"}, --itemProp:SaveToString
	{"ConfirmTradeSuccess", ""},
	{"UpdateTradeShelfSilver", "IU"}, --silver
	{"UpdateTradeBagSilver", "U"}, -- silver

	--Gift
	{"RequestStartGift", "dI"}, -- playerId1, playerEngineId1
	{"TradeActorStartGift", "Ib"}, -- engineId, bIsInvitor
	{"CheckCanFaceToFaceGiftFail", "UUU"}, -- msgpack.pack(condRet_1), msgpack.pack(condRet_2), msgpack.pack(condRet_3)
	{"RequestGiftLimitResult", "ddd"}, -- validGiftLimit, usedWeeklyGiftLimit, weeklyGiftLimit
	{"GetNeedGiftLimitResult", "d"}, -- NeedGiftLimit
	{"RequestGiftLimitOnShelfResult", "dddd"}, -- validGiftLimit, usedWeeklyGiftLimit, weeklyGiftLimit, NeedGiftLimit

	--日程
	{"SendDayActivityInfoNoTimeRestriction", "SI"}, -- dataStr, time
	{"SendDayActivityInfo", "SI"}, -- dataStr, time
	{"SendWeeklyInfo", "S"}, -- dataStr

	--自动拾取
	{"SetAutoPickItemResult", "iib"}, -- Type, PickOrNot, success
	{"SetAutoPickEquipResult", "iib"}, -- Color, PickOrNot, success
	{"SetAutoPickTalismanResult", "iib"}, -- Color, PickOrNot, success
	{"SetAutoPickExtraResult", "iib"}, -- Key, Value, success
	{"AutoPickSuccess", "I"}, --EngineId


	{"AttachThreaten", "I"}, --EngineId 表示这个obj对我有仇恨了
	{"DetachThreaten", "I"}, --EngineId 表示这个obj对我没仇恨了

	{"AttachThreatenReverse", "I"}, --EngineId 表示我对这个obj有仇恨了
	{"DetachThreatenReverse", "I"}, --EngineId 表示我对这个obj没仇恨了

	--自动放技能
	{"SetAutoAttackOnSelect", "I"}, -- iAutoAttack

	{"SceneTimeLeft","ii"},---lefttime lifetime
	{"CanLeaveTaskPlay","b"},
	{"KilledMonsterInGamePlay","ii"}, --killed,total

	--playershop
	{"OpenPlayerShopWindowResult","dd"},--shopId, bankruptTime
	{"RequestOpenPlayerShopSuccess", "dId"}, --shopId, openShopMoney, maintainFund
	{"PlayerShopOnShelfSuccess", "IIsII"}, --counterPos, price, itemName, itemCount, charge
	{"PlayerShopOffShelfSuccess", "I"}, --counterPos
	{"BuyItemFromPlayerShopSuccess", "IsI"}, --price, itemName, num
	{"ChangePlayerShopItemPriceSuccess", "dII"}, --shopId, counterPos, newPrice
	{"ChangePlayerShopNameSuccess", "ds"}, --shopId, strNewName
	{"ChangePlayerShopDescSuccess", "ds"}, --shopId, strNewDesc
	{"AddPlayerShopFundSuccess", "dIdd"}, --shopId, fund, maintainFund, totalFund
	{"SendPlayerShopBasicInfo", "U"}, --basicInfo)
	{"SendPlayerShopItemInfo2Gac", "sbUI"}, --itemId, isItem, itemData, collectedCount
	{"SendPlayerShopCounterInfo2Gac", "dUUUIUU"}, --shopId, counterItemsData, counterItemsPrice, itemOnShelfStatus, itemsSize, collections, leftTimeData
	{"SendPlayerShopInfoBegin", "d"}, --shopId
	{"SendPlayerShopInfoEnd", "d"}, --shopId
	{"QueryPlayerShopListResult", "IIUI"}, --indexStart, indexEnd, resultData, totalShopNum
	{"QueryTradeRecordsResult", "IdUU"}, --recordDate, income, recordsData{}{itemName, itemCount, totalCost, playerId, playerName}, buyRecord
	{"RequestAddPlayerShopCounterPosSuccess", "dII"}, --shopId, pos, needSilver
	{"RequestAddPlayerShopCounterPosFailure", "dII"}, --shopId, pos, needSilver
	{"SendPlayerShopBankruptInfo2Gac", "dsIII"}, --shopId, shopName, day, needSilver, bankruptcyProtectionDays
	{"CancelPlayerShopBankruptSuccess", "dI"},  --shopId, needSilver
	{"GetPlayerShopFundSuccess", "dddd"}, --shopId, fund, maintainFund, totalFund)
	{"QueryPlayerShopSearchedItemsResult", "IIIIUIbI"}, --itemTemplateId, preciousFlag, indexStart, indexEnd, result, totalNum, publiclyFlag, wordId
	{"QueryPlayerShopItemCollectedCountResult", "dII"}, --shopId, pos, count
	{"QueryPlayerShopItemCollectionsResult", "Ub"}, --result, publiclyFlag
	{"PlayerShopCollectItemSuccess", "dIs"}, --shopId, pos, itemId
	{"CancelPlayerShopCollectItemSuccess", "U"}, -- {shopId, pos, itemId}
	{"QueryPlayerShopOnShelfPriceResult", "IIII"}, --bagPos, basisPrice, minPrice, maxPrice
	{"QueryPlayerShopRecommendPriceResult", "IIII"}, --itemTemplate, basisPrice, minPrice, maxPrice
	{"QueryPlayerShopOnShelfNumResult", "Ub"}, --resultData, publiclyFlag
	{"ActionOpenPlayerShop", "I"}, --templateId
	{"SendPlayerShopFrozenFund", "U"}, --resultData
	{"QueryPlayerShopPubliclyItemsResult", "IIUI"}, --indexStart, indexEnd, result, count
	{"QuickBuyItemFromPlayerShopSuccess", "dsII"}, --price, itemName, num, itemTempalteId
	{"QuickBuyItemFromPlayerShopFailure", "IIs"}, --itemTempalteId, num, reason
	{"BuyItemFromPlayerShopFailed", "I"}, --reason
	{"QueryPlayerShopSearchedByWordsResult", "UIIIIIIUIb"}, --strWordIds, gradeRange, type, subType, preciousFlag, indexStart, indexEnd, msgpack.pack(result), num, publiclyFlag

	--剧情
	{"QueryJuQingCount", "i"},
	{"SyncUnLockJuQingLevel", "i"},
	{"QueryWuHuoQiQinCount", "i"},

	{"GetOnlineFriendsNotInTeam", "Ui"}, -- playersNearbyData
	{"GetOnlineFriendsNotInTeamGroup", "Ui"}, -- playersNearbyData

	{"StartConversationWithPlayer","I"},
	{"EndConversationWithPlayer","I"},

	{"UpdateEquipDuration","IId"}, -- place,pos,duration
	{"QueryRepairEquipCostResult","I"},
	{"UpdatePlayerEquipScore","I"}, --score
	{"ClearEnemyCheckForPlay", "b"},	-- clearForce

	{"AcceptNextYiTiaoLongConfirm","i"},
	{"UpdateNpcInfo", "U"},				--NpcPos
	{"QueryPlayerPosResult", "U"}, 			-- playerPos

	{"UpdateActiveSkillInfo", "IU"}, -- skillCls,all activeskills in msgpack
	{"UpdateSkillDelta","si"},

	{"OpenStoryDialog", "I"}, -- dialogId

	{"SyncActiveSkills2Gac", "U"}, -- msgpack.pack(activeSkills)
	{"SyncActiveSkills22Gac", "U"}, -- msgpack.pack(activeSkills)
	{"SyncSkillMap2Gac", "U"}, -- msgpack.pack(skillMap)
	{"SyncSkillCls2Id2Gac", "U"}, -- msgpack.pack(cls2Id)

	{"SyncExp", "d"}, -- exp
	{"DecreaseEquipDurationOnDie","I"}, --color
	{"TaskFinished","I"},
	{"UpdatePlayerFightProp","U"},
	{"QueryQianDaoInfoResult", "IIIIIIII"}, 			-- nYear, nMonth, nDay, dailyTimes, signUpTimes, totalTimes
	{"SignUpForQianDaoSuccess", "II"},				-- dailyTimes, signUpTimes,
	{"ShowQianDaoRefreshFlag", ""},
	{"ShowQianDaoWnd", ""},
	{"ItemAddRemind", "bIIs"},
	{"EquipSuccess","IIs"},
	{"BaptizeFail","s"},

	{"QueryChallengeRecordResult", "U"}, --record
	{"MPTZNewHighestRank", "III"}, --oldRank, newrank, award
	{"MPTZQueryPlayerTargetResult", "IIIIIIiiiiU"}, -- speedupPrice, idx, avgRank1, avgRank2, avgRank3, fightScore, lastIdx, lastScore, lastYinPiao, lastExp, dataUD
	{"QueryMPTZRankInfoResult", "IIU"}, -- class, maxGrade, rank data
	{"SetMPTZPrepare", "b"}, --isPrepare
	{"UpdateMPTZAISkill", "II"}, -- isCast79Skill, isCast109Skill
	{"UpdateMPTZDPS", "dsddsd"}, -- id1, name1, dps1, id2, name2, dps2

	{"QueryPlayerCapacityResult", "ddU"},  -- targetId, totalValue, packedData

	--货运任务
	{"UpdateFreightBoxStatusResult", "dsdsiibi"}, -- requesterId, requesterName, ownerId, ownerName, boxIdx, status, success, msg
	{"GetCurrentFreightItemsResult", "dSdd"}, -- ownerId, data, ExperidTime, currentTime
	{"GetCurrentFreightItemsFailed", "d"}, -- ownerId
	{"GetNextFreightItemsResult", "Sdddb"}, -- data, today5AM, today9PM, currentTime, couldAcceptTask
	{"GetNextFreightItemsFailed", ""}, --
	{"QueryGuildFreightGiveHelpCountResult", "i"}, --count

	{"UpdateYiTiaoLongCurrentTaskId", "I"},
	{"CreateNpcForYiTiaolongSuccess", "II"},
	{"GuaJiTrackToDoYiTiaoLong", "I"},
	{"UpdateHuoLiInfo", "II"},

	{"AddPlaySceneMonsterPosition","siii"},
	{"SyncPlaySceneTrackConditionDone","sU"},
	{"TrackConditionDone","sI"},

	{"UpdateEquipDisable","sC"},

	{"SyncBarrierType", "i"}, -- BarrierType

	{"AddFxToObject","II"},
	{"AddWorldPositionFx","iiiI"},
	{"RemoveWorldPositionFx", "I"}, --fxId
	{"QueryTaskHuoLiTimesResult", "U"},
	{"AddUIFx", "Iib"}, -- fxId, duration, destroyWithScene
	{"AddUIFxWithBG", "Iibb"}, -- fxId, duration, destroyWithScene, showBG
	{"RemoveUIFx", "I"}, -- fxId

	{"ShowRetributionEffect", "IIi"}, -- fxId, playerEngineId, deltaHp

	--行酒令
	{"RemoveFxFromObject","II"},

	--自动匹配
	{"TeamMatchingResult", "bi"}, -- success, cause
	{"GetMatchingTeamInfosResult", "iS"}, -- activityId
	{"GetMatchingTeamInfosFailed", ""}, --
	{"RequestJoinMatchingTeamCallback", "i"}, -- teamId
	{"GetTeamMachingActivityResult", "bi"}, -- success, activityId
	{"EnterTeamMatching", "i"}, -- activityId
	{"LeaveTeamMatching", "i"}, -- activityId

	{"SyncGuildCityId", "s"},

	{"UpdateYiTiaoLongTaskFinishedTimes","IIII"}, --taskId,round,subtimes

	{"SyncBagPlaceSize", "I"}, -- newBagPlace

	{"UpdateFightSpeed","Idd"}, -- engineId pspeed mspeed

	{"UpdateNpcShopLimitItemCount", "III"},		-- shopId, itemId, count

	-- keju
	{"ShowXiangShiWnd", "SUIII"},
	{"SendXiangShiQuestion", "SUIIIS"},
	{"SendXiangShiEnd", "IIIS"},
	{"SendHuiShiQuestion", "ISUbII"},
	{"SendHuiShiAnswer", "IU"},
	{"SendDianShiQuestion", "ISUbII"},
	{"SendDianShiAnswer", "IU"},
	{"SendTopPlayerAnswerResult", "U"},
	{"SendHuiShiTopPlayerScore", "U"},
	{"StartDianShiCountDown", "I"},
	{"SendDanMu", "sU"},
	{"UpdateHuiShiCountDownTime", "I"},
	{"UpdatePlayerHuiShiDianShiScore", "I"},
	{"UpdateDianShiGamblingPlayerIdInfo", "U"},

	--帮会修炼
	{"SendGuildPracticeAvailable", "IIdIIIIIU"}, -- Scale, SanctumNum, Silver, BasePracticeSize, AntiControlPracticeSize, EnhanceControlPracticeSize, AntiElementPracticeSize, IgnoreElementPracticeSize, msgpack.pack(PracticeArray)
	{"SendSetPracticeExp", "II"}, -- Index, Exp
	{"SendSetPracticeLevel", "II"}, -- Index, Level
	{"SendUpdatePlayerPracticeDialog", "I"}, -- Contribution
	{"OpenGuildSkillWnd", ""}, --
	{"PracticeToNextLevelNeedResources", "Iddd"}, --Index, RequiredExp, RequiredSilverAndFreeSilver, RequiredContribution


	{"ClearCastSkillCheckInPlay", ""}, --playName
	{"SetCastSkillCheckInPlay", "s"}, -- playName

	--repertory
	{"OpenRepertoryWnd", "IbI"},  -- opentype, isHaveValidDefaultZuoQi, defaultZuoQiTemplateId
	{"UpdateRiderBagInfo", "bI"},  -- isHaveValidDefaultZuoQi, defaultZuoQiTemplateId
	{"UpdateRiderExtraBagSize", "I"},  -- newExtraRiderBagSize
	{"SyncRepertoryOpen", "I"}, -- repoOpenNum
	{"SetRepertoryAt", "Is"}, -- repoIdx, itemId
	{"SyncRepertoryOpenTimes", "II"}, -- expireTimes, leftOpenTimes
	{"SyncIsKeepRepertory", "I"}, -- isKeepRepertory

	-- kaifu
	{"UpdateLoginDaysAward", "II"},			-- id days
	{"UpdateOnlineTimeAward", "II"},		-- id time
	{"UpdateLevelUpAward", "I"},			-- id

	{"JadeNoEnough",""},
	{"BaptizeEquipmentPropertySuccess","s"},
	{"UpdateSubmitEquipGetMFInfo", "dd"},
	{"OpenEquipExchangeMFWnd", "d"},

	{"YuanBaoNotEnough","II"}, -- yuanbao, type

	{"UpdateWordMatchFilter", "IISib"}, -- version id string type forceUpdate
	{"DelWordMatchFilter", "IIi"}, -- version id type

	{"EnterPlaySetData","Is"}, -- playdesignId,sceneId
	{"LeavePlayClearData",""}, -- clear playdesignId,sceneId
	{"UpdateItemCount","bsI"}, -- isitem,itemId,count
	{"RequestClientUploadUserInfo",""},
	{"YiTiaoLongNeedTeam",""}, --给引导用的消息

	{"UpdateSealData", "bIIsiiIbI"},		-- seal, , ,canbaishMinLevel,canBaiShiMaxLevel, feishengOpenStatus, feishengOpenTime


	--称号
	{"AddTitle", "IIs"}, --titleId, time, name
	{"UpdateTitle", "II"}, --titleId, time
	{"DeleteTitle", "U"}, --titleTable
	{"SetShowTitle", "IS"}, --titleId, name
	{"SetAttrTitle", "I"}, --titleId
	{"CancelShowTitle", ""},
	{"CancelAttrTitle", ""},
	{"SyncTitle", "IIS"}, --EngineId, titleId, name
	--djw COMMOENT: for challenge client
	{"UpdateDynamicTitleName", "Is"}, --titleId, newName

	--成就
	{"AddPlayerAchievement","I"},
	{"UpdatePlayerAchievementProgress","IU"},
	{"DeletePlayerAchievementProgress", "I"},
	{"PlayerGetAchievementAwardSuccess","Is"},
	{"ViewPlayerAchievementResult","IU"},


	{"UpdateItemActionTimes","bsI"},

	-- 联赛
	{"UpdateGuildLeagueHomeHp", "U"},
	{"QueryGuildLeagueInfoResult", "UUUUb"},
	{"UpdateMonsterForceInfo", "U"},
	{"NotifyGLKillStreak", "I"},		-- num
	{"OpenGLLevelInfoWnd", ""},
	{"OpenGLMatchInfoWnd", ""},
	{"QueryGLLevelInfoResult", "UI"},
	{"QueryGLMatchInfoResult", "UI"},
	{"UpdateGLBuildingPosData", "U"},
	{"UpdateGLForceGuildName", "IsIs"},		-- force guildName, force, guildName
	{"UpdateGuildGLHistoryInfo", "IIIIU"},		-- playerJoinCount, playerWinCount, guildJoinCount, guildWinCount, historyTbl_U
	{"QueryGuildPreLeagueTaskPointRankResult", "U"}, --result
	{"QueryGuildPreLeagueTaskPersonalPointResult", "I"}, --point

	{"RequestCanInlayStoneOnEquipmentResult","bsii"},
	{"RequestCanRemoveStoneOnEquipmentResult","bsIiUii"},--can, equipId, stoneItemId, costyuanbao,newItemInfo,equipNewgrade, setIndex
	{"RequestCanUpgradeEquipStoneResult","bsii"},
	{"RequestCanIntensifyEquipmentResult","biii"},
	{"GetEquipGradeByIntensifyLevelResult","sib"},--itemId,newGrade,canEquip

	{"QueryMonsterPosResult", "U"},
	{"SendXiangShiConfirmMessage", "s"},
	{"SendHuiShiConfirmMessage", "s"},
	{"ChouBingSuccess","s"}, -- equipId
	{"ResetWordSuccess","s"}, -- equipId
	{"BindItemSuccess","IIs"}, -- place,pos,equipId
	{"SplitItemSuccess","IIsII"},--place,pos,itemId,oldItemCount,newItemCount

	--ZhouMoActivity
	{"SyncZhouMoActivityOpenStatus", "I"}, --status
	{"ReplyZhouMoActivityRankInfo", "U"}, --rankData
	--djw COMMENT: for challenge client
	{"ShowZhouMoActivityCountDown", "sIII"}, --openTime, countDown, playStage, openStatus
	{"ShowZhouMoActivityBossInfo", "dUIII"}, --progress, bossInfo, countDown, enterCount, openStatus
	{"ReplyZhouMoActivityCHJPInfo", "IbI"}, --nextStage, openState, needCount
	{"SyncZhouMoActivityTeamHurt", "db"}, --teanHurt, isShow

	{"HideSuitFx", "Ib"},
	{"HideGemGroupFx", "Ib"},

	--时装
	--djw COMMENT: 0 for challenge client
	{"TakeOnFashion", "d"}, --fashionId
	--djw COMMENT: 0 for challenge client
	{"TakeOffFashion", "d"}, --fashionId
	{"DiscardFashion", "d"}, --fashionId
	{"ExpireFashion", "d"}, --fashionId
	{"AddFashion", "dIi"}, --fashionId, expireTime, appearance
	{"SyncFashion", "IIIISS"}, --engineId, headTemplateId, bodyTemplateId, weaponTemplateId, HeadFashionCustomColorInfo, BodyFashionCustomColorInfo
	{"UpdateFashion", "dII"}, --fashionId, expireTime, isExpired
	{"SetFashionHide", "II"}, --position, state
	{"CompositeTabenSuccess", ""},
	{"SetWardRobeNumLimit", "I"}, --设置衣橱上限
	{"OpenTabenFashionDialog", ""},
	{"RenewFashionSuccess", ""},
	{"SetFashionCustomColorSuccess", "dddd"}, --fashionId, seXiang, baoHeDu, mingDu

	{"RequestRecycleItemReturn","IIsI"},
	{"RequestBatchRecycleItemReturn",""},

	{"RequestBuyBackItemReturn","IIs"},

	{"ClearTradeFrozenSilver", "I"}, --idx
	{"SyncTradeFrozenSilver", "IU"}, -- idx, frozenInfo
	{"SyncRepertorySilver", "U"}, -- normalsilver, mallsilver

	-- 运营专区
	{"SyncSubmitSettingQuestionResult","S"}, --message
	{"SyncSubmitAppealQuestionResult","S"}, --message
	{"SyncEvaluateAnswerResult","S"}, --message
	{"SyncGetAnswerResult","S"}, --message
	{"SyncGetDetailResult","S"}, --message
	{"SyncGetQuestionsResult","S"}, --message
	{"SyncMaintenanceZoneAvailableResult","bb"}, --isOpen, isQuickEntranceOpen
	{"SyncMaintenanceZoneUniToken","S"}, --uniToken
	{"SyncMaintenanceZoneTags","IU"}, --star, tagData

	{"YaoQianShuPlanted",""},
	{"OpenTianShuResult","U"},

	--翅膀
	{"AddWing","II"}, --wingId ,wing deadline
	{"RemoveWing","I"},
	{"HideWing","Ib"},
	{"UpdateAppearenceWing","II"},

	{"EquipLostSoul", "sU"},
	{"EquipCancelLostSoul", "s"},
	{"UpdateLingShouOwner", "II"}, --lingshouEngineId, ownerEngineId

	-- 小精灵
	{"SendJingLingMsg","SIb"},

	-- GM流程
	{"SetQuickFileReportVipLevel","I"},
	{"SetQuickDialVipLevel","I"},

	--高昌
	{"GaoChangApplySuccess", ""},--高昌报名成功
	{"GaoChangInviteToStage1", ""},--高昌邀请玩家进入第一层
	{"GaoChangStatisticStart", ""},--高昌统计开始，进场景
	{"GaoChangStatisticStop", ""},--高昌统计开始，进场景
	{"GaoChangStatisticAddPlayer", "dsii"},--高昌统计添加玩家，playerId, name, kill, killed
	{"GaoChangStatisticUpdate", "dii"},--高昌统计更新数据，playerId, kill, killed
	{"GaoChangInviteToApply", ""},--高昌邀请玩家报名
	{"GaoChangQueryApplyInfoResult", "b"},--高昌查询报名结果

	{"ShowSceneTimeCountDown","sbi"},--开启场景倒计时窗口倒计时 参数:sceneId,show/hide,lefttime

	{"ShowFakePlayerToPlayer","IsccIdUHIsU"},-- uint engineId,string name, sbyte gender, sbyte objClass, int state, double dir, byte[] appearanceData, ushort level, uint titileId, string titleName, byte[] statusProp
	{"ShowFlagToPlayer","IIIIUd"}, -- uint engineId, uint templateId,uint actionState,uint ownerEngineId, statusProp, dir
	{"ShowBulletToPlayer","IIIIU"}, -- uint engineId, uint templateId,uint actionState,uint ownerEngineId, statusProp
	{"ShowItemToPlayer","IIbIiiibdbiibbbsb"},--uint engineId, uint templateId, bool isDropping, uint dropObjectEngineId, int equipColor, int wordCount,bool straightDrop, double originalHeight,isfake,displayColor,holeNum,isNiTian,isPrecious,pickAble,specialDisplayName, bCannotBeAutoPick
	{"ShowLingShouToPlayer","IIsIdsIdddIIIIUIUUIII"},	--uint engineId, uint templateId, string id, uint actionState, double dir, string name, uint ownerEngineId, double hp, double currentHpFull,double hpFull,uint pixiangTemplateId,uint quality,uint evolveGrade,uint pixiangEvolveGrade, statusProp, ResourceId, marryInfo, babyInfo, babyTempId1, babyTempId2, babyTempId3
	{"ShowMonsterToPlayer","IIIdUIHddsUbUUIsIdU"},--uint engineId, uint templateId, uint actionState, double dir, byte[] appearanceData, ushort level, double benefitCharacterId, double ownerPlayerId name, statusProp, bool isFake, byte[] fakeAppearData, extraData, specialTitleId, specialTitleStr, zuoqiTemplateId, driverId, passengerIdList
	{"ShowNpcToPlayer","IIIdssIIdbUUsIUdHd"}, --uint engineId, uint templateId, uint actionState, double dir, string name, string titile, uint headFx, uint fxId, double ownerPlayerId, bool isFake, byte[] fakeAppearData, byte[] statusData, portraitName, zuoqiTemplateId, passengerIdList, EmbracePlayerId, ushort level, driverId
	{"ShowPetToPlayer","IIIdIbdddsbUIIIsU"}, --uint engineId, uint templateId, uint actionState, double dir, uint ownerEngineId, bool ownerIsFemale, double hp, double currentHpFull, double hpFull, string ownerName, isShadow, appearProp, class, gender, titleId, titleName, statusProp
	{"ShowOtherPlayerToPlayer","IsccIdUHIsUib"}, --uint engineId, string name, sbyte gender, sbyte objClass, uint actionState, double dir, byte[] appearanceData, ushort level, uint titileId, string titleName, string statusProp, serverGroupId, hasFeiSheng
	{"ShowPickToPlayer","IIdIsI"}, -- engineId templateId dir actionstate name resourceId
	{"ShowTempleToPlayer","IIdI"},-- engineId templateId dir actionstate
	{"UpdateTalismanSuit","UU"},-- addedSuitIds,removedSuitIds

	{"OpenYiZhangShanHeTuResult", "IIs"}, -- comsumeYiZhangShaneHeTuId, randomIndex, showItemInfo

	{"UpdateGuildSceneId", "s"},			-- sceneId

	{"QueryZhanLongTimesResult", "U"},

	{"ResetTalismanBasicWordResult","sb"},
	{"ResetTalismanExtraWordResult","sb"},
	{"MakeXianJiaTalismanResult","IIb"},
	{"UpgradeXianJiaResult","b"},
	{"HeChengTalismanResult","b"},

	{"BeginEquipItem","IIs"},-- place pos equipId 开始装备 equip
	{"UpdatePaoShangVehicleNeedCheck", "II"},			-- duration  engineId
	{"CheckPaoShangVehicleSuccess", ""},

	--绑定手机相关
	{"BindMobileOpenWnd", "ssb"}, -- areaCode, phoneNumber, bCheckedThisMonth
	{"BindMobileDisbindPhoneSuccess", ""},
	{"BindMobileVeryfiCaptchaSuccess", "s"}, -- phoneNumber
	-- 节日签到
	{"UpdateHolidayQianDaoInfo", "U"},
	{"QueryHolidayQianDaoInfoResult", "U"},
	{"RequestHolidayQianDaoSuccess", "IIII"},			-- batchId, dailyTimes, totalTimes, validDays
	-- 保释金
	{"ShowBaoShiJinGuide", "SIIIIii"}, -- message, timeout, type, npcId, npcSscene, npcPosX, npcPosY 保释金引导
	{"BaoShiJinPayConfirmMessage", "SIII"}, -- message, timeout, type, action 保释金确认消息
	{"BaoShiJinCleanHistoryConfirmMessage", "SIII"}, -- message, timeout, type, action 保释金确认消息
	{"ShowBaoShiJinSimpleTip", "S"}, -- message 保释金Tips
	{"ShowBaoShiJinActionConfirm", "SiII"}, -- message, timeout, type, action

	{"ShowAnnouncement", ""},

	-- 请求打开运维专区页面
	{"SyncOpenMaintenanceZoneWnd", ""},

	--邀请活动
	{"ReplyInvitationRankInfo", "U"}, --rankInfo
	{"ReplyInvitationInfo", "dsIIU"}, --inviterId, inviterName, inviteNum, achieveNum, receiveTbl
	{"ReplyInvitedPlayerInfo", "U"}, --invitedPlayerData
	{"ShowInvitationRedDot", ""},

	-- 港澳台分享和邀请
	{"ReplyHmtShareInfo", "U"},

	--  外挂检测
	{"FindModuleListByKeyword", "ss"}, -- gmSessionInfo, keyword
	{"GetModuleList", "s"},
	{"FindProcessListByKeyword", "ss"},-- gmSessionInfo, keyword
	{"GetProcessList", "s"},
	{"GetTouchPosHistory", ""},
	{"QueryVerifyClientTime", "II"},
	{"SyncCheatDetect", "s"},
	{"SyncGetCheatInfo", "Is"}, -- cheatType, gmSessionInfo

	{"UpdateGuideData","U"},
	{"PlayerAboutToEnterNewRoleGamePlay",""},

	{"TrackToDoTask","I"},
	{"EquipmentSuitUpdate","I"}, --suitId

	{"UpdateIntensifySuitList","U"},

	--营销需求
	{"ReplyQnRechargeReturnData", "bbU"}, --isConfirm, isValid, userData
	{"RequestShareYangMao", ""},
	{"WaitConfirmPRA", "IIs"}, --tp, price, name

	{"RequestEquipPropertyChangedWithTempWordsReturn","sU"},--equipId,propertylist
	{"UpdateHolidayOnlineTimeInfo", "II"},			-- awardId, duration
	{"SignUpForQianDaoWithExtraSuccess", "III"},
	{"HolidayQianDaoNeedExtraConfirm", "II"},

	{"ShowBindMobileMessage", ""},

	{"RequesetCanQianZhuanShanResult","sii"},--equipId needYinPiao needYinLiang
	{"QianZhuanShanResult","sb"},--equipId result

	{"UpdatePaoShangVehicleInfo", "U"},
	{"UpdatePaoShangVehicleMapPos", "U"},
	{"UpdateHolidayOnlineTimeClosed", ""},

	--个人空间
	{"SetPersonalToken", "sbd"}, --token, bIsLogin, expiredTime
	{"SetPersonalSpaceSwitchAndHost", "bbsisb"}, --bOpenPersonalSpace, bOpenEntrance, PersonalSpaceHostClient, serverGroupId, ServerName, EnableSendFlowerOnlyAddFriendliness
	{"QueryPictureTokenResult", "IIs"}, --context, time, token

	{"UpdateGLPlayerPlatformCenterPos", "III"},		-- x y r

	{"OpenYiZhangShanHeTuV2Fail", "siI"}, -- itemId, failure, locationId
	{"OpenYiZhangShanHeTuV2Success", "sI"}, -- itemId, templateId
	{"PreCheckYiZhangV2Success", "s"}, -- itemid
	{"PreCheckYiZhangV2Fail", "si"}, -- itemid, failure


	--师徒
	{"CanPostShouTuMessage","b"},--打开NPC对话选项,客户端可以录入收徒信息, isWaiMen
	{"PlayerCanSearchShiFu","b"}, -- isWaiMen
	{"PostShouTuMessageSuccess",""}, --发布收徒信息成功
	{"SearchShiFuResultNoShiFu",""}, --没有收徒信息
	{"SearchShifuReturn","iU"}, --找到的师傅列表
	{"RemindBaiShiOnLevelUp",""}, -- 提示可以拜师
	{"RequestBeYourShiFu","dsbb"}, --XX请求成为师傅  shifuId shifuName isRuShi isWaiMen
	{"RefuseBeYourTuDi","ds"},--xx拒绝成为徒弟 tiduId tudiName
	{"AddShiFu","dU"},--添加一个师傅
	{"DeleteShiFu","d"},--删除一个师傅
	{"ChuShi","dd"},--出师 shifuId,chushi time
	{"TuDiChuShi","dd"},--徒弟出师 tudiId,chushi time
	{"AddTuDi","dU"},--添加一个徒弟
	{"DeleteTuDi","d"},--删除一个徒弟
	{"TuDiJieChuShiTu","d"},--徒弟解除师徒关系
	{"ShiFuJieChuShiTu","d"},--师傅解除师徒关系

	{"RequestBreakShiTu","bds"}, --请求解除师徒关系  -- isShiFu,playerid,name



	-- 比武
	{"QueryBiWuStageMatchInfoResult", "U"},				-- matchInfo
	{"QueryBiWuStageWatchInfoResult", "iU"},			--  stage watchInfo
	{"OpenBiWuSelectPlayerWnd", "IUUId"},				-- teamId teamInfo playerInfo duration time
	{"UpdateBiWuTeamRoundData", "UUU"},
	{"QueryBiWuSignUpInfoResult", "b"},					-- bSign
	{"UpdateBiWuSelectPlayerResult", "U"},				-- playerTbl_U
	{"QueryBiWuScoreResult", "U"},
	{"UpdateBiWuTeamSelectResult", "IU"},				-- teamId teamSelect_U
	{"QueryBiWuChampionInfoResult", "IU"},				-- stage info_U
	{"OpenBiWuChampionInfoWnd", ""},

	{"BroadcastTopMessage", "SI"}, -- msg, expiredTime
	{"CancelTopMessage", ""},

	{"OpenRedIceExchangeMFWnd", "d"},			-- leftExchangeMF
	{"OpenPurpleIceExchangeMFWnd", "d"},		-- leftExchangeMF

	{"UpdateTalismanAppearanceList","U"}, -- 法宝列表（等级 数量,高阶也计入低阶数量）
	{"SyncTalismanAppearance","Ii"}, -- engineId , (lv - 1) * 100 + grade * 10 + count

	-- 夺宝
	{"ReplyDuoBaoPlayStatus", "III"},		--status, endTime, myTotalInvestRound
	{"ReplyCurrentRoundDuoBaoInfo", "IIIIII"},		--duoBaoId,currentRound,currentProgress,myInvest,myInvestTotalRound,myInvestTotalFenshu
	{"ReplyDuoBaoOverview", "IIIIIII"},		--duoBaoId,totalRound,currentRound,currentProgress,myInvest,myInvestRound,myInvestTotalFenshu
	{"ReplyDuoBaoOverviewEnd", ""},
	{"ReplyMyDuoBaoInfo", "IIIIdsIUs"},		--duoBaoId,round,currentProgress,myInvestFenShu,winnerId, winnerName,winnerFenShu,topUsers,winnerServerName
	{"ReplyDuoBaoResult", "IIIIdsI"},		--resCount, timeStamp,duobaoId,round,winnerId,winnerName,winnerInverst
	{"ReplyDuoBaoResultEnd", ""},
	{"ReplyMyDuoBaoInfoBegin", ""},
	{"ReplyMyDuoBaoInfoEnd", ""},
	{"ReplyDuoBaoInvestResult", "IIIb"},  -- duoBaoId,round,count,result
	{"ReplyDuoBaoRewardHistoryBegin", ""},
	{"ReplyDuoBaoRewardHistory", "IIII"}, --timestamp, duoBaoId, round, invest
	{"ReplyDuoBaoRewardHistoryEnd", ""},

	-- 结婚
	{"OpenWeddingDeclareWnd", "ss"}, -- groomName, brideName
	{"NotifyBroomOpenDinner", ""},
	{"NotifyPlayerGetWeddingCert", "s"}, -- itemId
	{"ShowWeddingLuxuryEffect", ""},
	{"OpenFireworksChooseWnd", ""},
	{"ShowYangyangWeddingVideo", ""},
	{"OpenCeBaZiWnd", ""},
	{"CeBaZiWaitPartner", ""},
	{"CeBaZiWaitFail", ""},
	{"SendBaZiResult", "s"}, -- baziItemId
	{"CancelCeBaZi", ""}, --
	{"PlayerRequestEngageMe", "dss"}, -- playerId, playerName, itemName
	{"PlayerRequestEngage", ""}, -- isUseLingYu
	{"PlayerRequestOpenCeBaZiWnd", ""},
	{"GroomStartParade", ""},
	{"GroomStopParade", ""},
	{"SendWeddingGroomAndBride", "dd"}, --groomId, brideId
	{"QueryWeddingCertNamesResult", "ddss"}, -- groomId, brideId, groomName, brideName
	{"ShowParadeScreenEffect", ""},
	{"UpdateWeddingInfo", "IU"},  -- engineId, weddingInfo
	{"PlayerRequestParadeJinling", "I"}, --price
	{"PlayerRequestParadeHangzhou", "I"}, -- price
	{"PlayerRequestCancelEngage", ""},
	{"PlayerRequestSendInvitationToAllFriend", "I"}, --price
	{"PlayerRequestSendInvitationToGuild", "I"}, --price
	{"RequestStartWedding", "I"}, --price
	{"RequestConfirmPlayerDivorce", ""},
	{"RequestConfirmPlayerCancelDivorce", ""},
	{"RequestConfirmPlayerForceDivorce", "I"}, -- forcedivorcePrice
	{"RequestConfirmPlayerDoDivorceTogather", ""},
	{"NotifyBaiTianDiNpcSayMsg", "s"}, -- msgName


	{"QiangqinMemberNotHaveItem", ""}, --

	--出家
	{"QueryFriendRansomChuJia", "dsI"}, --playerId, name, silver
	{"RequestVisitChuJiaPlayerResult", "U"}, --players
	{"RequestRansomChuJiaPlayerResult", "U"}, --players
	{"RequestChuJiaHuaYuan", "Idss"}, --silver, playerId, playerName, message
	{"OpenChuJiaHuaYuanWin", ""},
	{"ChuJiaDaZuoBegin", "dI"}, --playerId, engineId
	{"ChuJiaBaiFoBegin", "dI"}, --playerId, engineId
	{"ChuJiaDaZuoEnd", "d"}, --playerId
	{"ChuJiaBaiFoEnd", "d"}, --playerId
	{"RequestChuJiaNotSingleNeedMoney", "I"}, --silver

	--奖章
	{"PlayerCanUsePresentItemReturn","sbsd"}, --itemId,result,recievername,recieverId
	{"PlayerCanGetPresentItemAwardFromLink","dddIs"}, --recieverId,senderId,sendTime,itemtemplateId,senderName
	{"QueryPresentItemRecieverNameReturn","sdsi"},--itemId,recieverId,recieverName,count

	--红包
	{"ReplyRecentHongBaoInfo", "IU"},  --channelType, hongBaoInfo
	{"ReplySendHongBaoResult", "Is"},  --status, reason
	{"ReplyHongBaoDetails", "dsIsdIIUIIiiIIUb"},  --ownerId, ownerName, sendTime, contents, totalAmount, totalNum, snatchNum, snatchData, playerClass, gender, expression, expressionTxt, snatchAmount, event, extraData_U, isVoiceType
	{"ReplyHongBaoHistoryRecord", "IIddU"}, --sendNum, snatchNum, sendSilver, snatchSilver, history_U
	{"PlayerSendHongBao", "dsIIiiiIIssIIUI"}, --playerId, playerName, class, gender, expression, expressionTxt, sticker, channelType, channelId, contents, id, amountLY, event, profileInfo_U, coverType
	{"ReplyRemainHongBaoSendTimes", "II"}, --channelType, times
	{"SnatchHongBaoSuccess", "s"}, --id

	{"PersonalSpaceAddEventSuccess", "is"}, -- addeventType, data
	{"PersonalSpaceAddPresentSuccess", "s"}, -- data
	{"PersonalSpaceSetHidePreviousNameSuccess", "sb"}, -- data, bHide

	{"SendChangeGenderCheckResult", "I"}, --needJade

	{"ShowGuildLeagueMonsterDieFX", "I"},		-- templateId

	{"SyncGuildName", "s"},

	{"BroadcastSendFlowerEffect", "dsdsIii"}, --srcId, srcName, targetId, targetName, flowerTemplateId, num, effectId
	{"NotifyAccountReplaced", ""},
	{"OpenRawGuildWnd", ""},

	{"LevelUpConfirm","U"},--confirm message list

	{"PackageOrderNeedConfirm","i"},--place  整理背包 可能会发生交易保护期的物品合并 需要玩家确认
	{"RepertoryReorderNeedConfirm","I"},-- 整理仓库可能会发生交易保护期的物品合并 需要玩家确认

	{"GuidePlayerToDestScenePos", "IiiIIs"}, --sceneTemplateId, x, y, place, pos, itemId
	{"GuidePlayerToDestPosAndInteractWithNpc", "IiiI"}, --sceneTemplateId, x, y,npcTemplateId
	--坐骑
	{"ReplyAllZuoQiOverview", "sIIU"}, --defaultZuoQiId, maxZuoQiNum, currentNum, zuoqiData
	{"RideOnZuoQiSuccess", "sIUI"},  --zuoqiId, templateId, mapiinfo, resId
	{"CastingForRideFail", "s"}, --reason
	{"SyncZuoQiTemplateId", "IIUI"}, --engineId, templateId, mapiInfo, resId
	{"RideOffZuoQiSuccess", ""},
	{"AddNewZuoQi", "sII"}, --zuoqiId, templateId, expireTime
	{"RenewZuoQiSuccess", ""},
	--djw COMMENT: for challenge client
	{"UpdateZuoQiInfo", "sIII"}, --zuoqiId, expireTime, count, resId
	{"SetDefaultZuoQiSuccess", "s"}, --zuoqiId
	{"DiscardZuoQiSuccess", "s"}, --zuoqiId
	{"InvitePassenger", "I"}, --driverEngineId
	{"UpdateZuoQiAppearance", "IIdU"}, --engineId, zuoqiTemplateId, driverId, passengerList
	{"UpdateMapiZuojuAppearance", "IsIII"}, --- engineId, zuoqiId, toushiId, maanId, tunshiId

	{"UpdateMonsterOwnerGuildId", "Id"},		--  engineId guildId
	{"UpdateGuildLeagueFlagPos", "III"},		-- flagType x y

	{"BroadcastPlayerRenameDone", "Idss"},		-- engineId, playerId, newName, oldName

	{"ReplyLoveFlowerInfo", "IIIsIII"}, --plantId, enginedId, stage, name, plantTime, growValue, lastChangeTime

	{"NotifyChargeDone", "ssIs"}, -- pid, currency, count, sn

	{"ReplyDynamicActivityInfo", "IIIU"}, -- activityType, currentCount, maxCount, ud
	{"SyncDynamicActivitySimpleInfo", "U"}, -- ud

	{"SendHuiLiuData", "IIIb"}, --level, day(今天是第几天), rewardFlag(1表示领过了，0表示没领过)
	{"GetHuiLiuRewardResult", "I"}, --rewardFlag(1领取成功， 0领取失败)
	{"SendInvitedHuiLiuPlayerInfo", "UI"}, --data
	{"InvitedPlayerHuiLiuResult", "db"}, --playerid, flag


	--师徒情 心有灵犀
	{"SendXinYouLingXiQuestion","dss"},--sessionid,title,options
	{"StopShiTuQingXYLXSession","d"},--sessionid
	{"SendPlayerShiTuQingXYLXAnswer","ddi"},--sessionid,playerid,answer
	{"SyncShiTuQingXYLXSessionInfo","diiss"},--sessionId,selfAnswer,otherAnswer,title,options

	-- 寇岛围攻
	{"SendKouDaoFightInfo","IUId"},--statType, msgpack.pack(data), playerCount, statTotal
	{"SendKouDaoCleanStatResult","Ib"},--statType, bResult
	{"SendKouDaoSceneInfo","UI"},--msgpack.pack(data), playerCount
	{"SyncKouDaoHardModeBossDie","I"},-- leftTime
	{"SyncKouDaoKillExtraMonster","I"},-- count
	{"SyncPlayerKouDaoEliteRight","I"},-- rightTag, 1->has,0->no

	-- COMMENT : for antihack
	{"AddPlayerSpecialItemInfo", "U"},

	-- 情书
	{"ShowLoveLetterWnd", "U"},							-- info_U
	{"SendTopLoveLetterInfo", "U"},						-- letterInfo_U
	{"RequestGetPlayerSendLoveLetterResult", "U"},		-- letterInfo_U
	{"RequestGetPlayerReceivedLoveLetterResult", "dU"},	--  receiveId, letterInfo_U
	{"PraiseLoveLetterSuccess", "s"},					-- letterId
	{"BroadcastLoveLetter", "U"},						-- senderName receiverName content
	{"QueryLoveLetterReceiverInfoResult", "U"},			--playerInfo_U
	{"QueryLoveLetterSenderInfoResult", "U"},			--playerInfo_U
	{"OpenLoveLetterRankWnd", "U"},						-- letterId_U
	{"RequestSendLoveLetterSuccess", ""},

	--鹊桥
	{"SendQueQiaoPoemQuestion", "SI"}, --question, countDown
	{"CloseQueQiaoRandomEventWnd", "I"}, --type   1-poem 2-bao
	{"OpenQueQiaoBaoYiBaoWnd", "SI"},  --ShowMessage, countDown

	-- pc pay
	{"OpenPcChargeWnd", "ss"}, -- pcPayTicket, pid
	{"NotifyMobileScanChargeQrWnd", "s"}, -- pid
	{"NotifyMobilePayReadme", "s"}, -- pid

	{"SetSwitchTargetSkipPetAndLingShouSuccess", "i"},
	{"SyncClientInfoToClient", "U"},

	-- 经验雨
	{"ExpRainStart", "i"}, -- isFirst
	{"ExpRainStop", ""},
	{"ShowExpRainFx", ""},

	-- 客户端外挂信息
	{"QueryClientInfo", "ss"}, -- typeName, cypher


	{"PatchPlayUpdateAlertBegin", ""},
	{"PatchPlayUpdateAlert", "U"}, --data
	{"PatchPlayUpdateAlertEnd", ""},
	{"IgnorePlayUpdateAlert", "d"}, -- curVersion
	{"ShowPlayUpdateAlert", ""},

	--结拜系统
	{"StopJieBaiSession","d"}, -- sessionid
	{"ShowJinLanPu","di"}, -- sessionid,membercount
	{"UpdateJieBaiSessionState","iiss"}, -- state,membercount,title1,title2
	{"AddMemberToJinLanPu","diisiiii"},-- playerid,class gender,name,level,index,membercount,writecount
	{"UpdateMemberIndexOnJinLanPu","di"}, --playerId,index
	{"AddPlayerToJinLanPuDone","di"},--playerId,birth

	{"SyncJieBaiTitleWhenJieBai","ss"}, --title1,title2
	{"PlayerSignForJieBai","di"}, -- playerid,index
	{"UpdateJieBaiMembers","U"},-- msgpack.pack(playerids)
	{"OpenSetJiebaiCommonTitleDialog","i"}, -- jiebai_member_count
	{"OpenSetJiebaiPersonalTitleDialog","ss"}, -- commontitleName,personalTitleName
	{"DelJieBaiMember","d"}, -- playerId

	{"CancelJieBaiConfirm","i"}, -- silver
	{"CancelJiebaiDone",""},

	-- 闯关
	{"SendChuangGuanOverview", "IUU"}, -- stage, data, chosenClassUD
	{"ReplyChuangGuanSceneInfo", "U"}, -- sceneInfoData

	{"ReplyChuangGuanRankInfoBegin", "UUI"}, -- scoreUd, selfUd, selfBestTime
	{"ReplyChuangGuanRankInfo", "IU"}, -- count, rankUd
	{"ReplyChuangGuanRankInfoEnd", ""},

	-- 帮花
	{"PlayBangHuaFirstStageVideo", "sU"},			-- videoName fakeappeapance
	{"SyncNpcFakeAppearanceDirection", "IU"},		-- engineId fakeAppearance
	{"SyncBangHuaFirstStageBossCatchInfo", "Id"},	-- engineId playerId
	{"QueryBangHuaCurStageStatusResult", "II"},		-- stage status
	{"BeginBulletTime", "I"},						-- duration
	{"QueryHugPlayerPosResult", "U"},				--
	{"ShowEnterBangHuaNextStageWnd", ""},
	{"ShowEnterBangHuaSecondSingleConfirmWnd", ""},
	{"ShowBangHuaQueryPlayDataBtn", ""},
	{"QueryBangHuaFightDataResult", "IUId"},			-- type data
	{"QueryBangHuaScenePlayerInfoResult", "IU"},	-- count data
	{"CancelShowBangHuaQueryPlayDataBtn", ""},
	{"UpdateBangHuaThirdStageBossStatus", "IIII"},		-- buffCls lv hp hpFull
	{"PlayBangHuaThirdStageVideo", "sbIIUIIU"},			-- videoName bool class gender appearance class gender appearance
	{"QueryBangHuaSecondSingleInfoResult", "U"},

	--抗性兑换
	{"OpenAntiExchangeWnd", "IIU"}, --UnDistributedAntiPoint, RestExchangeNum, msgpack.pack(skillLevel)
	{"ReplyAntiExchangeCheckResult", "II"}, --gongXun, exchangeLimit
	{"ExchangeAntiSuccess", "I"}, --UnDistributedAntiPoint
	{"ReplySubAntiPointResult", "U"}, --msgpack.pack(result)
	{"ReplyDistributeAntiResult", "IIU"}, -- consumed, remain, msgpack.pack(result)
	{"ReplyAntiExchangeTipsInfo", "IIIdIIII"}, --gongXun, maxAnti, scale, xiuWei, upperLimit, totalAnti, restAnti, returnRatio
	{"OpenAntiExchangeWnd2", "IIU"}, -- FreedomDistributeAntiPoint, canExchangeNum, msgpack.pack(skillLevel)
	{"ReplyAntiExchangeCheckResult2", "IIII"}, --gongxun, lower, upper, restExchangeNum
	{"ExchangeAntiSuccess2", "I"}, --FreedomDistributeAntiPoint
	{"ReplySubAntiPointResult2", "U"}, --msgpack.pack(result)
	{"ReplyAntiExchangeTipsInfo2", "IIdIIII"}, --gongXun, scale, xiuWei, upperLimit, totalAnti, restAnti, returnRatio

	--
	{"NotifyPushFinish", "I"}, --engineId

	{"SetYiShiAddHpContinuously", "b"},

	--transform
	{"SyncTransformResourceId", "II"},  --engineId, monsterId
	{"SetScenePassedTime", "i"}, -- passedTime

	{"PlayerContactGuildManager", "dds"}, -- guildId, managerId, managerName

	{"RestartJieBaiSession","di"},-- sessionid,membercount

	{"SyncForces", "II"},-- engineId, forceId

	{"ShowChangeHairColorWnd", "sII"}, --itemId, place, pos

	--夺魂
	{"QueryDuoHunFanPlayerNameReturn","sds"}, --itemId,targetId,targetName
	{"QueryHunPoPlayerNameReturn","sds"}, --itemId,targetId,targetName
	{"QianLiYanQueryPlayerPosReturn","dsssiiisI"},-- targetId,targetName,itemid,sceneName,sceneIdx,posX,posY,sceneId,scentempateId
	{"ZhaoHunSuccess",""}, --
	{"UpdateZhaoHunData","U"},--zhaohundata
	{"PlayerCanZhaoHun","b"}, -- can/cannt

	-- 蹴鞠
	{"SendCuJuQieCuoRequest", "d"},				-- playerId
	{"UpdateCuJuSignUpInfo", "bbIII"},			-- bool bool times totalTimes stage
	{"InvitePlayerJoinCuJuPractice", "I"},		-- interval
	{"ShowCuJuSelectPosWnd", "I"},				-- position
	{"QueryCuJuGameMatchInfoResult", "U"},		-- watchInfo
	{"UpdateCuJuPlayerPlayInfo", "UUI"},
	{"UpdateSetPositionResult", "I"},			-- position
	{"UpdateCuJuBallPosAndScore", "UU"},
	{"OpenCuJuPlayerPlayInfoWnd", "UUI"},		-- scoreInfo, playerData, winForce

	{"ShowGuoQingXianLiDaTiWnd", "IdssUI"}, --index, playerId, playerName, question, choices, answerDuration
	{"ReplyCampusCodeInfo", "I"}, --code
	{"SubmitCampusCodeSuccess", ""},
	{"SendGuoQingXianLiDaTiAnswer", "III"}, --rightAnswer, playerAnswer, showDuraton

	{"ActiveTalismanOnBody","bi"},--success index

	{"ReplyTeamDamageInfo", "U"}, --data_U

	{"ReplySetHelmetHide", "I"}, --state

	-- 拍卖
	{"SendPaiMaiPriceChanged", "sdII"}, --paimaiId, playerId, newPrice, ownPrice
	{"SendWorldPaiMaiOnShelfGoods", "IUII"},--count,data, pageNum, goodClass
	{"SendGuildPaiMaiOnShelfGoods", "IUII"},--count,data, pageNum, goodClass
	{"SendMyPaiMaiInvestGoods", "IUII"},--count,data, pageNum, goodClass
	{"SendPaiMaiGoodsByTemplateId", "IIUI"},--count,templateId,data, pageNum
	{"SendPaiMaiItemInfo", "dsbU"},--playerId,itemId,isItem,data
	{"SyncPaiMaiOnShelfSuccess", ""}, --
	{"SyncPaiMaiOffShelfResult", "b"}, --
	{"SendGuildPaimaiHistory", "IUI"}, --count,data, pageNum
	{"SendMyPaimaiOnShelfGoods", "IU"},--count,data
	{"SyncBetSuccess", "sI"},--paiMaiId, newPrice
	{"SendPaiMaiBonus", "d"},--bonus
	{"SyncGuildPaiMaiBegin", ""},
	{"SyncPaiMaiBuySuccess", "s"},
	{"SyncPaiMaiFrozenJadeDetail", "U"},
	{"SyncPaiMaiFrozenJadeInfo", "dd"},
	{"SyncPaiMaiLeftFrozenJadeCount", "d"},
	{"SyncPaiMaiWolrdOnShelfCount", "d"},
	{"SyncPaiMaiClassfiedOnShelfData", "U"},

	{"RequestBreakShiTuWithTuDi",""}, --

	{"ZhaoHunMTWCConfirm","i"},-- need money

	{"RandomCanAcceptQingYuanPlayersResult", "U"}, --data

	{"InviteVote", "sUsd"}, --myServerName, otherServerNames, mergeToServerName, voteEndTime


	{"ChaiJieFaBaoSuccess","iis"},-- place,pos,fabaId

	{"SetNickNameResult", "bds"}, --success, targetId, nickname
	{"SendCrossGatewayInfo", "ssIs"}, -- ticket, gatewayIp, gatewayPort, metaSceneName

	{"WashTalismanSuitResult","iIIII"}, -- suitIndex,selectSuitId,userSuitKey,oldWordCls,newWordCls

	-- 转职
	{"ShowClassTransferCheckResult", "UU"}, -- result, resultArgs
	{"OpenExchangeNewJueJiItemWnd", "I"}, -- wndType
	{"ExchangeNewJueJiItemSuccess", "IIIII"}, -- groupIdx, itemTempId70Forget, itemTempId100Forget, itemTempId70, itemTempId100

	--表情相关
	{"TeamMemberExpressionChange", "diiiU"}, -- playerId, expression, expressionTxt, sticker, profileInfo_U
	{"QueryValidExpressionResult", "U"}, --
	{"QueryValidExpressionTxtResult", "U"}, --
	{"QueryValidStickerResult", "U"}, --
	{"QueryValidProfileFrameResult", "U"}, --
	{"UpdatePlayerExpression", "i"}, --
	{"UpdatePlayerExpressionTxt", "i"}, --
	{"UpdatePlayerSticker", "i"}, --
	{"UpdatePlayerProfileInfo", "U"}, -- profileInfo_U
	{"UnlockPlayerExpression", "ib"}, --
	{"UnlockPlayerExpressionTxt", "ib"}, --
	{"UnlockPlayerSticker", "ib"}, --
	{"UnlockPlayerProfileFrame", "ibd"}, --


	{"RestartJinLanPu","d"}, -- sessionId
	{"RequestChaiJieFaBaoConfirm","iisiiii"}, --place,pos,needmoney,rate,itemCount,yuanshiCount

	-- 变狐
	{"SyncSheepResourceId", "II"},  --engineId, monsterId
	--- 家园
	{"CreateFurnitureFromRepository", "IIs"}, -- id, templateId, itemid
	{"ExchangeFurniture", "U"}, --- CRpcProp_ExchangeInfo
	{"RemoveFurniture", "I"}, -- packed FurnitureId
	{"MoveFurniture", "IIdddI"}, -- FurnitureId, TemplateId, X, Y, Z, Rot
	{"ScaleFurniture", "II"}, -- Id, scale
	{"ShiftFurniture", "Iddd"}, --- Id, xOffset, yOffset, zOffset
	{"MovePenzaiHuamu", "III"}, -- decorationId, scale, rot
	{"SendFurnitureAppearance", "UUUUUU"}, -- FurnitureStr, DecorationStr, ChuanjiabaoStr, dimianChuanjiabaoStr, FurnitureStr2, FurnitureStr3
	{"SendCurHouseProp", "UUUUUUUU"}, --- BasicProp, ConstructInfo, ProgressInfo, GardenInfo, furnitureInfo, furnitureInfo2, furnitureInfo3, extraInfo
	{"SendQiShuFurnitureStore", "U"}, ---
	{"AddFurniture", "IIdddIIsU"}, -- id, templateId, x, y, z, rot, EngineId, chuanjiabaoItemId
	{"SetRepositoryFurnitureCount", "II"}, -- templateId, count
	{"SendRepositoryFurnitureData", "U"},
	{"OpenFurnitureWnd", ""}, --
	{"AddDecoration", "IIII"}, --- id, templateId, parentId, slotIdx
	{"RemoveDecoration", "III"}, -- id, parentId, slotIdx
	{"AddChuanjiabaoToScene", "IsII"}, -- id, itemid, parentId, slotIdx
	{"RemoveChuanjiabaoFromScene", "III"}, -- id, parentId, slotIdx

	{"ProduceFurnitureConfirm", "IIsI"}, -- place, pos, itemid, Silver
	{"AddExtraProgressConfirm", "IIsI"}, -- place, pos, itemid, progressCount

	{"OpenPutQinggongWnd", "I"}, -- FurnitureId
	{"OpenLiuyanbanWnd", "Id"}, -- furnitureId, playerId
	{"PlayerAttachFurniture", "dII"}, -- playerId, furnitureId, slotId
	{"PlayerDetachFurniture", "dI"}, -- playerId, furnitureId
	{"SendPlayerFurnitureInteractInfo", "U"}, -- PlayerFurnitureInteractInfo
	{"CharacterAttachFurniture", "III"}, -- engineId, furnitureId, slotId
	{"CharacterDetachFurniture", "II"}, -- engineId, furnitureId
	{"SendCharacterFurnitureInteractInfo", "U"}, -- PlayerFurnitureInteractInfo
	{"SendFurnitureBedBeiziStatus", "U"}, -- FurnitureBedBeiziInfo
	{"UpdateBedBeiziStatus", "Ib"}, ---- furnitureId, bStatus

	{"SendFurnitureStolenInfo", "sU"}, -- houseId, FurnitureStolenInfo

	{"ClosestoolProduceStatusChange", "II"}, -- furnitureId, produceTime

	{"PlayerUseFurniture", "I"}, --- furnitureId
	{"PlayerPlayDrum", "Iddd"}, --- ftid, x, y, z

	{"QueryQishuFurnitureSuccess", ""},

	{"SendHouseData", "UUUUUUUdsIsssssUU"}, -- BasicProp, ConstructInfo, FurnitureInfo, ProgressInfo, GardenInfo, NewsInfo, ExtraInfo, CommunityId, CommunityName, CommunityNO, HouseName, ZuolingName, YousheName, ownerName1, ownerName2, FurnitureInfo2, FurnitureInfo3
	{"SendHouseProgressInfo", "sIU"}, -- HouseId, ProgressPos, ProgressInfo
	{"HouseBuildEnd", "sII"}, --- HouseId, ProgressPos, nextGrade
	{"UpdateHouseResource", "sddd"}, --- HouseId, food, wood, stone
	{"UpdateSelfCommunityInfo", "sdsI"}, -- -- houseId, communityId, communityName, pos
	{"UpdateHouseAttribute", "U"}, -- msg
	{"UpdateHouseWeixiuResource", "sI"}, --- HouseId, WeixiuResource
	{"WeixiuFurnitureSuccess", ""}, --
	{"SendQueryAllCommunityResult_NoCommunity", "s"}, -- initStreetName
	{"SendQueryAllCommunityResult", "UUUUsI"}, -- communityList, defaultCommunity, houseNamesInCommunity, collectedHouseInfo, lizhangHouseName, lizhangHousePos
	{"SendQueryCommunity", "UUsI"}, -- communityData, houseNames, liZhangHouseName, liZhangHousePos
	{"SendHouseIntro", "dsdssdIIUUUUsUUIIII"}, -- ownerId, ownerName, ownerId2, ownerName2, houseId, communityId, communityNO, communityStreetId, basicProp, contructProp, gardenProp, progressProp, houseName, ownerCJB, coupleCJB, furnitureCount, fangkeCount, qishu, isSpokesmanHouse
	{"ShowNoHouseIntro", "dII"}, -- communityId, communityNO, communityStreetId
	{"AddCollectedHouse", "ssII"}, -- houseId, houseId, houseName or "", communityId or 0, pos or 0
	{"RemoveCollectedHouse", "s"}, --houseId
	{"OpenCommunityWnd", ""}, --
	{"SetHouseId", "s"}, --houseId
	{"OpenSelectDiqiWnd", ""}, --
	{"RequestConfirmBuyHouse", "I"}, --silver
	{"RequestConfirmConvertHouseToContract", "I"}, --silver
	{"RequestDiqiLastOwnerNameResult", "sds"}, --itemId, playerId, playerName
	{"OpenCJBModelMakeWnd", ""}, --
	{"FriendsFitTongzhuRequirementResult", "U"}, -- friendIdList
	{"QueryTongzhuResult", "II"}, -- currentRommerNum, maxRoommerNum
	{"RequestConfirmInviteRoomer", "ds"}, -- roomerId, roomerName
	{"UpdateHouseExtraInfo", "U"}, -- extraInfo
	{"PlayerSendRoomInvite", "ds"}, -- inviterId, inviterName
	{"QueryCJBMakeIsRoomerResult", "b"}, -- isroomer
	{"QueryCJBMakeIsRoomerResult_NotAtHome", ""}, --
	{"RequestRoomerAssignableTitleInfoResult", "bIUUUU"}, -- isOwner, xiangfangGrade, roomNameList, houseTitleInfo, puppetInfoUD, friendPuppetUD
	{"VarifyHouseTitlesForHouseName", "U"}, -- alltitleUd, houseName
	{"VarifyHouseTitlesForHouseTitle", "U"}, -- alltitleUd, houseName
	{"RequestPray_NotAtHome", ""}, --

	{"UpdateHouseName", "s"}, -- newName
	{"UpdateXiangfangName", "s"}, --- newName
	{"QueryRoomerNamesResult", "U"}, -- roomerNameListUD

	{"QueryPlayerCurrentPrayWordsResult", "IUU"}, -- engineId, buffUD wordUD

	{"QueryRoomerPrivalegeInfoResult", "bdsUsU"}, -- isOwner, roomerId, roomerName, roomerInfoUD, titleStr, opInfoUD

	{"AddHouseNewsRewardSuccess", "IU"}, -- newsId, newsUD
	{"RequestGetHouseNewsRewardSuccess", "I"}, --newsId

	{"ShowQiyuanBtnFirstGoHome", ""}, --

	{"UpdateFreeMaintainFeeDay", "I"}, -- newDays
	{"AddFreeMaintainFeeDayConfirm", "IIsII"}, -- cost, days, itemid, place, pos
	{"SetFurnitureLimitStatusConfirm", "IsII"}, -- cost, itemid, place, pos
	{"SetFurnitureLimitStatus", "I"},

	-- 家园马厩
	{"ReplyAllMajiuMapiOverview", "IU"}, --- mapiCount, mapiOverview
	{"MajiuMapiStatusChanged", "sb"}, -- zuoqiId, bIsMajiuMapi
	{"SendMajiuMapiEquipList", "sU"}, -- zuoqiId, equipListUD
	{"SendMajiuMapiAppearance", "IIIsU"}, -- fid, idx, engineId, displayMapiInfoUD

	-- 家园傀儡
	{"AddHouseCPPuppetSuccess", "sIU"}, -- houseId, idx, puppetInfoUD
	{"AddFriendPuppetSuccess", "sIU"}, -- houseId, idx, puppetInfoUD
	{"SyncHousePuppetInfoToClient", "UU"}, -- puppetInfoUD, friendPuppetUD
	{"RemovePuppetSuccess", "sI"}, -- houseId, idx
	{"FriendsFitPuppetRequirementResult", "UU"}, -- friendsListUD, oldFriendListUD
	{"PuppetPlayerQueryHouseList_Result", "U"}, -- houseListUD
	{"SyncHousePuppetFightStatus", "U"}, -- puppet forces info
	{"SendPuppetData", "IbU"}, -- puppetIdx, isFriendPuppet, puppetInfo
	{"UpdateFriendPuppetNewName", "Is"}, -- engineId, newName
	{"RequestViewPuppetDetailInfo_Result", "IU"}, -- engineId, fightPropUD
	{"PuppetGossipSetSuccess", "IU"}, -- puppetIdx, puppetInfoUD
	{"RequestCreatePuppetSuccess", "IIbU"}, -- puppetIdx, engineId, isFriendPuppet, puppetInfoUD
	{"RequestPackPuppetSuccess", "IIbU"}, -- puppetIdx, engineId, isFriendPuppet, puppetInfoUD
	{"BeginSendPuppetInfoInScene", ""}, --
	{"SendPuppetInfoInScene", "II"}, -- puppetIdx, engineId
	{"RequestMovePuppetSuccess", "II"}, -- puppetIdx, engineId
	{"SyncMonsterTitle", "IIs"}, -- engineId, specialTitleId, specialTitleStr
	{"SyncFakePlayerTitle", "IIs"}, -- engineId, specialTitleId, specialTitleStr
	{"SyncFakePlayerName", "Is"}, -- engineId, name
	{"QueryPuppetFightHistoryResult", "IIU"}, -- engineId, puppetIdx, fightHistoryUD
	{"SwitchHousePuppetDifficultySuccess", "IIII"}, -- puppetIdx, difficulty, engineId, newEngineId
	{"AddFriendPuppetSuccessForNotify", ""}, --
	{"AddHouseCPPuppetSuccessForNotify", ""}, --
	{"SetPuppetHideFashionSuccess", ""}, --

	-- 名园
	{"RequestConfirmConvertHouseToMingyuan", "sII"}, -- itemid, place, pos


	-- 家园进度相关
	{"HouseProgressReduceTime", "sII"}, -- HouseId, progressPos, reducedTime
	{"AddExtraProgress", "sII"}, -- HouseId, extraProgressCount, endTime
	{"CancelHouseProgress", "sI"}, -- HouseId, progressPos
	-- 种菜相关
	{"PlantCropSuccess", "sIIIII"}, -- HouseId, gardenIdx, cropId, productId, productNum, beginTime
	{"HarvestCropSuccess", "sId"}, -- HouseId, gardenIdx, playerId
	{"StealCropSuccess", "sIIIdI"}, -- HouseId, gardenIdx, restnum, moreProductRest, playerId, stealTimes
	{"ReviveCropSuccess", "sII"}, -- HouseId, gardenIdx, newKuweiTime
	{"RemoveCropSuccess", "sI"}, -- HouseId, gardenIdx
	{"UpdateStealCropInfo", "sIIdI"}, --- houseId, updateTime, stealCount, pid, totalStealCount
	{"OpenNameMyHouseWnd", "IsI"}, --- lastRenameTime, houseName, price
	{"RequestResetChuanjiabaoMakeProgressSuccess", "d"}, --- currentLingqi

	{"UpdateCropProtectTime", "sU"}, -- houseId, gardenInfo

	--- 建筑外观变换功能
	{"ChangeBuildingAppearance", "II"}, -- furnitureId, newTemplateId

	--- 杨洋家园相关
	{"SyncYangyangHouseScene", "b"}, -- bYangyangHouseScene

	{"ChangeShineiOpenSuccess", "b"}, -- bOpenShinei

	-- 传家宝
	{"MakeCJBModelSuccess", "I"},  -- star
	{"BeginBakeChuanjiabaoModel", ""},  --
	{"QueryLastSavedCJBMakeProgress", "sIIUdUUUSI"},  -- repaireItemId, status, targetTemplateIdx, widthUD, height, colorUD, texUD, texRestoreUD, texUrl, star
	{"QueryLastSavedCJBMakeProgress_Empty", "d"},  -- lingqi
	{"ChuanjiabaoModelNameSuccess", "s"},  -- itemId
	{"SendHousePrayResult", "UUUIsId"}, -- rengeList, digeList, tiangeList, totalScore, panci, borrowLiftWordId, propMulti
	{"SetHouseChuanjiabaoInfo", "sU"}, -- houseId,
	{"QueryHouseLingqiResult", "sd"}, -- houseId, lingqi
	{"QueryChuanjiabaoInHouseResult", "sIUI"}, -- houseId, maxChuanjiabaoNum, ids, suiteIdx
	{"RequestChuanjiabaoArtistNameResult", "dsdsds"}, -- artistId1, name1, artistId2, name2, artistId3, name3
	{"RequestZhushabiArtistNameResult", "dsds"}, -- artistId1, name1, artistId2, name2
	{"RequestChuanjiabaoModelArtistNameResult", "ds"}, -- artistId1, name1
	{"QueryPlayerPrayInfoResult", "IIUUUIsIb"}, -- prayTimes, cost, rengeList, digeList, tiangeList, totalScore, panci, borrowLiftWordId, isShowTip
	{"SyncPlayerPrayWord", "U"}, -- prayWords
	{"CalculateShapeStarResult", "III"}, -- targetTemplateId, score, star
	{"CalculateShangseStarResult", "III"}, -- targetTemplateId, score, star
	{"SyncPlayerChuanjiabaoModelTextureIds", "U"}, -- customTextureIds
	{"RequestChaijieZhushabiDetailsResult", "IIsII"}, -- place, pos, itemId, silver, totalCount
	{"NotifyPlayerSelectChuanjiabaoToRepaire", "U"}, -- cjbList
	{"PlayerMakeZhushabiSuccess", "s"}, -- itemId
	{"QueryPlayerNextTimePrayInfoResult", "ddddd"}, -- qishu, prob, wordLvDiff, borrowLifeProb, propMulti

	--- 装修模板
	{"PlayerRequestLoadHouseLayoutCheckSuccess", "I"}, -- SlotIdx
	{"PlayerRequestAutoLayoutCheckSuccess", ""}, -- 

	--- 灵兽装饰物
	{"OpenExchangeLingShouFurnitureWnd", ""}, --
	{"OpenLingshouPropertyWnd", "I"}, -- furnitureId
	{"OpenFeedLingshouWnd", "I"}, -- furnitureId
	{"SendFurnitureLingshouData", "IU"}, -- fid, lingshouInfo
	{"SendFurnitureEngineId", "II"}, -- fid, engineId
	{"UpdateMonsterName", "Is"}, -- engineid, name

	{"SendHouseCompetitionData", "IIb"}, -- stage, chusaiUploadTimes
	{"SendHouseCompetitionHouseList", "U"}, -- houseListCache
	{"SyncHouseCompetitionFinalStatus", "bs"}, -- bStatus, sUrl
	{"SendHouseCompetitionVictoryHouseList", "U"},
	{"HouseCompetitionCheckRewardResult", "b"},

	{"SyncJiyuNpcTime", "U"},

	--- 马匹系统相关
	{"SyncMapiProp", "sIIU"}, ---- zuoqiId, templateId, expireTime, mapiProp
	{"SyncYumaPlayData", "U"}, --- yumaInfo
	{"ShouhuLingshouSuccess", "sIs"}, -- zuoqiId, lingshouId
	{"StopShouhuLingshouSuccess", "sIs"}, --- zuoqiId, pos, lingshouId
	{"UpdateZuoqiEquip", "sIsIs"}, --- zuoqiId, pos, equipId, bagPos, itemid
	{"SendYumaInfo", "UU"}, -- yumaInfo, mapimsg
	{"SendYumaFriendInfo", "U"}, --- qualifiedFriendList
	{"PlayerInviteYuma", "ds"}, ---- playerId, playerName
	{"PlayerDenyYumaInvitation", ""},
	{"InviteYumaSuccess",""},
	{"SendYumaWithFriendInfo", "ddUUbbI"}, --- playerId1, playerId2, zuoqi1, zuoqi2, bConfirmed1, bConfirmed2, itemcount
	{"GenerateLingguSuccess", "sII"}, --- lingfuId, quality, pos
	{"GenerateLingguFastSuccess", "U"}, --- msgpack
	{"GenerateLingguFail", ""},
	{"SendAllMapiDetailDone", ""},
	{"StartYumaSuccess", ""},
	{"UpgradeLingfuLevelSuccess","I"}, ---
	{"SendLingfuWashTempProperty", "sIU"}, --- lingfuId, washType, lingfuItemData
	{"ReplaceLingfuPropertySuccess", "sI"}, -- lingfuId, washType
	{"QianghuaLingfuSuccess", "sb"}, -- lingfuId, bIsShengjie

	{"SendHouseDisplayMapiInfo", "IU"}, --- furnitureId, displayMapiInfo
	{"SendHouseDisplayMapiBabyInfo", "IU"}, ---- furnitureId, displayMapiBabyInfo
	{"UpdateMapiQingxu", "sII"}, --- zuoqiId, engineId, qingxuzhi

	{"SendMapiPinfen", "sII"}, -- zuoqiId, pinfen, lingfuPinfen

	--
	{"BeginCameraMove", "ddddddd"}, -- x, y, z, rX, rY, rZ, maxTime
	{"CameraMoveTo", "ddddddd"}, -- x, y, z, rX, rY, rZ, t
	{"EndCameraMove", ""}, --
	{"SetCameraPinch", "dbbb"}, --- t, showWnd, affectUI

	{"BeginCameraTraceTarget", "Id"}, -- targetEngineId, duration
	{"EndCameraTraceTarget", ""},

	-- 招亲
	-- 打开初始页面
	{"OpenZhaoQinInitWnd", "III"},
	-- 发起招亲的结果
	{"SyncCallZhaoQinResult", "b"},
	-- 报名招亲的结果
	{"SyncJoinZhaoQinResult", "b"},
	-- 发起者信息
	{"SyncZhaoQinCallerInfo", "U"},
	{"SyncOneZhaoQinCallerInfo", "U"},
	-- 观战信息
	{"SyncZhaoQinGuanZhanInfo", "U"},

	{"SyncZhaoQinSuccess", "ds"},

	-- 家谱
	{"SendPlayerFamilyTreeInfo", "dUbsU"}, -- playerId, infoUd, ret, reason, extraInfo

	--装备打孔转换
	{"OpenEquipHolesTransferWnd",""},
	{"QueryCanTransferHolesSourceEquipRet","U"}, -- positionlist
	{"CanSelectTransferHoleSourceEquipRet","Is"}, --sourceEquipPos,sourceEquipId
	{"QueryCanTransferHolesTargetEquipRet","IsU"}, --sourcePos,sourceEquipId,targetPosList
	{"CanSelectTransferHoleTargetEquipRet","IsIsi"}, --sourcePos,sourceEquipId,targetPos,targetEquipId,costYuanBao
	{"TransferEquipHoleSuccess","IsIs"}, --sourcePos,sourceEquipId,targetPos,targetEquipId

	--仙家法宝转移
	{"OpenXianJiaFaBaoTransferWnd",""},
	{"QueryCanTransferXianJiaFaBaoListRet","U"}, -- fabaoPosListUD
	{"TransferXianJiaFaBaoSuccess",""},

	-- 帮会建设红利
	{"SyncGuildBuildBonus","d"},
	{"SyncSelfBuildBonus","dbI"}, -- bonus, isThisWeekTaken, playStaus

	--自动回复
	{"PlayerSetAutoReply", "b"},
	{"PlayerSetAutoReplyMsg", "s"},

	--多倍经验
	{"ReplyMultipleExpInfo", "IIIIbIIIIbI"}, --ReceiveTime, BuyCount, BuyTime, RestTime, IsDongJie, JieDongTime, BuyCount3, BuyTime3, RestTime3, IsDongJie3, JieDongTime3
	{"PlayerGetMultipleExpItem", "II"}, --type, itemTemplateId

	{"SyncOfflineTaskExp", "d"},  --exp

	{"SyncBeginZhaoQinGuanZhan", ""},
	{"SyncZhaoQinItemTargetName", "sIIs"},
	{"UpdatePlayerBelongServerSealId", "I"},			-- sealId

	-- CC语音
	{"PlayerJoinRemoteChannel", "IsSSS"}, -- channelId, channelType, retJsonStr, roomInfoStr, context
	{"PlayerLeaveRemoteChannel", "Si"}, -- remoteChannelId, reason
	{"UpdateRemoteChannelInfo", "S"}, -- roomInfoStr
	-- {"QueryCcChannelListResult", "S"}, -- channelListStr
	-- {"QueryCcChannelListResultWithBreakStr", "SSSS"}, -- zhuBoListStr
	{"QueryZhuBoListResult", "S"}, -- zhuBoListStr
	{"QueryZhuBoListResultWithBreakStr", "SSSS"}, -- zhuBoListStr
	{"SetGuildCommander", "dsb"}, -- commanderPlayerId, commanderPlayerName, bIsReconnect
	{"ResetGuildCommander", ""},
	{"QueryRemoteChannelEIdResult", "ddI"}, -- targetId, eId, context
	{"PlayerJoinCcChannel", "S"}, -- ccChannelInfo
	{"SetTeamSpeakStatusSuccess", "I"}, -- status
	{"UpdateCcFollow", "S"}, -- dataStr
	{"SyncEIdInChannelFrom", "dd"}, -- playerId, eid
	{"SyncEIdInChannelTo", "U"}, -- dataUD
	{"CcidOnline", "U"}, -- dataUD
	{"CcidAlert", "U"}, -- dataUD
	{"InviteListenGuildCommand", ""}, --
	{"SetTeamGroupCommander", "dsbi"}, -- commanderPlayerId, commanderPlayerName, bIsReconnect, idx
	{"ResetTeamGroupCommander", "i"},  -- idx
	{"InviteListenTeamGroupCommand", "i"}, --idx--
	{"NotifyNoRemoteChannel", ""}, --

	{"RemoteTeleportDeleteGatewayReconnectInfo", "s"}, -- ticket
	{"ClearTalismanSuitUserWord","I"}, --suitIndex
	{"PutInItemToItemSuccess","Is"}, -- position ,itemId

	{"PlayBaiShiVideo","sIIUIIU"}, --video name,shifu class,shifu gener,shifu appearance,tudi class tudi gender tudi appearance
	{"ClearTalismanSuitUserWordConfirm","i"},

	-- 斗魂
	{"OpenDouHunChlgTeamSetPosWnd", ""},
	{"VoteDouHunChlgTeamFailed", "s"},					-- chlgTeamId
	{"OpenDouHunSubmitItemWnd", "II"},
	{"OpenDouHunFuTiInfoWnd", ""},
	{"OpenDouHunTrainInfoWnd", ""},
	{"OpenCrossDouHunWatchWndResult", "I"},				-- stage
	{"OpenCrossDouHunScoreRankWnd", ""},
	{"OpenLocalDouHunWatchWnd", ""},

	{"CreateDouHunChlgTeamSuccess", "s"},							-- chlgTeamId
	{"QueryDouHunChlgTeamApplyInfoResult", "U"},					-- UD
	{"QueryDouHunAllChlgTeamInfoBegin", ""},
	{"QueryDouHunAllChlgTeamInfoData", "U"},						-- UD
	{"QueryDouHunAllChlgTeamInfoEnd", "sU"},						-- chlgTeamId applyTeamId
	{"QueryDouHunChlgTeamInfoByIdResult", "U"},
	{"QueryPlayerDouHunChlgTeamInfoResult", "UUbUbbbIb"},					--
	{"QueryDouHunChlgTeamVoteInfoResultBegin", ""},
	{"QueryDouHunChlgTeamVoteInfoResult", "U"},						-- chlgTeamId
	{"QueryDouHunChlgTeamVoteInfoResultEnd", "ssIU"},					-- chlgTeamId selfTeamId
	{"ClearDouHunChlgTeamApplicantSuccess", ""},
	{"RequestJoinDouHunChlgTeamSuccess", "s"},						-- chlgTeamId
	{"RequestLeaveDouHunChlgTeamSuccess", ""},
	{"KickDouHunChlgTeamMemberSuccess", "sd"},						-- chlgTeamId playerId
	{"AcceptDouHunTeamApplicantSuccess", "d"},						-- playerId
	{"CancelApplyDouHunChlgTeamSuccess", "s"},						-- chlgTeamId
	{"RefuseDouHunTeamApplicantSuccess", "ds"},						-- playerId chlgTeamId
	{"QueryDouHunChlgTeamPosInfoResult", "UUbU"},					-- UD
	{"ChlgTeamLeaderSetMemberPosSuccess", "dU"},					-- playerId pos
	{"ChlgTeamLeaderCancelMemberPosSuccess", "dU"},					-- playerId pos
	{"VoteDouHunChlgTeamSuccess", "sI"},							-- chlgTeamId voteNum
	{"QueryDouHunTrainValueResult", "U"},
	{"QueryDouHunTrainJingLiInfoResult", "Ibbbb"},					-- level hasSet enabled canEnable canSet
	{"QueryDouHunTrainQiLiInfoResult", "Isbbb"},					-- level trainType enabled canEnable canSet
	{"QueryDouHunTrainShenLiInfoResult", "IUbbbI"},					-- level PointTbl enabled canEnable canSet shenLiLv
	{"QueryLocalDouHunPlayWatchInfoResult", "UU"},
	{"QueryCrossDouHunGroupWatchInfoResult", "IIUs"},				-- level stage info
	{"QueryCrossDouHunKnockOutWatchInfoResult", "IIUs"},			-- level stage info

	{"QueryCorssDouHunCurScoreInfoResult", "IU"},					-- serverId
	{"QueryCorssDouHunHistoryScoreInfoResult", "IU"},				-- serverId
	{"QueryCorssDouHunGroupScoreInfoResult", "IU"},					-- level
	{"QueryCrossDouHunHistoryWatchInfoResult", "IIIU"},				-- index level stage
	{"OpenCrossDouHunMatchRecordWndResult", "I"},					-- matchIndex

	{"QueryDouHunPlayInfoResult", "UUI"},
	{"ShowDouHunSubstituteBtn", ""},
	{"UpdatePlayPlayerInfoToWatcher", "UU"},
	{"UpdatePlayFightInfoToWatcher", "UUb"},
	{"QueryDouHunGroupRoundMemberResult", "U"},
	{"QueryPlayerGambleChoiceResult", "IIUU"},						-- serverId key
	{"OpenCrossDouHunGroupScoreInfoWnd", "I"},						-- serverId
	{"OpenDouHunBackgroundWnd", ""},
	{"OpenDouHunScheduleWnd", ""},
	{"OpenDouHunRuleWnd", ""},
	{"RequestModifyDouHunTeamDeclarationSuccess", "s"},

	{"RecommendAutoUseBaoLingDan", "bbII"}, --needBuyBaoLingDan, needBuyNiLiuPing, BaoLingDanPrice, NiLiuPingJifen
	{"RecommendAutoUseYXJWJ", "bbII"}, --needBuyYXJWJ, needBuyNiLiuPing, YXJWJPrice, NiLiuPingJifen

	{"UpdateCommonOccupationHpInfo", "ddIdsds"}, -- npcTemplateId, competeScore, npcEngineId, competitor1Id, competitorName1, competitor2Id, competitorName2
	{"GCO_OpenOccupationDetailWnd", "dsdsUd"}, -- guildId, fromName, targetGuildId, targetName,infosUD, nextOnTickTimestamp

	--lbs
	{"SendLbsClosePlayerInfo", "U"},--userData
	{"SendLbsPlayerDetailInfo", "U"},--userData
	{"BeginSendLbsClosePlayerInfo", ""},
	{"SyncLastLbsInfo", "bU"},

	{"BecomeShiTuWithTudiConfirm","ib"}, -- jianmianli, isRuShi

	{"SendGongJianJiaYuanProcessRank", "IIUUIIsIII"}, --rank, process, myServerRankData, allServerRankData, serverRank, ServerProcess, ServerName
	{"GasGongJianJiaYuanRewardSuccess", "I"}, --num

	--道具礼包
	{"ReplyRecentDaoJuLiBaoInfo", "U"},  --libaoInfo
	{"ReplySendDaoJuLiBaoResult", "Is"},  --status, reason
	{"ReplyDaoJuLiBaoDetails", "IdsIsIIIU"},  --channelType, ownerId, ownerName, sendTime, contents, price, totalNum, snatchNum, snatchDataStr
	{"ReplyDaoJuLiBaoHistoryRecord", "IIddU"}, --sendNum, snatchNum, sendLingYu, snatchLingYu, history_U
	{"PlayerSendDaoJuLiBao", "dsIIiiiIIssIIU"}, --playerId, playerName, playerClass, gender, expression, expressionTxt, sticker, channelType, channelId, contents, id, price, number, profileInfo_U
	{"ReplyRemainDaoJuLiBaoSendTimes", "II"}, --channelType, times
	{"ReplyDaoJuLiBaoOpenState", "b"}, --openState

	{"QueryMonthPresentLimitResult", "ddd"}, -- used, vipLimit, adjustLimit

	{"MoveOutItemFormItemSuccess","III"}, --bagPos,moveOutItemTemplateId,moveOutItemCount

	{"CheckMyRightsResult", "sIb"}, -- rightName, context, success
	{"QueryOnlineMembersInMyGuildResult", "UI"}, -- dataUD, context

	{"QueryPlayerIsGhostReturn","db"}, --targetId,isGhost
	{"ReOrderItemBagSuccess","Is"}, -- pos itemId

	{"QueryMembersJieBaiPersonalTitleReturn","ssi"}, -- operation,membernameAndTitle,memberCount

	{"OpenTaskQuestionWnd","I"}, -- taskId

	-- 删除角色
	{"SyncDelRoleSuccess", ""},
	{"OpenVerifyMobileOpenWnd", "sI"},--phoneNumber,actionType
	{"OpenDeleteRoleInitWnd", ""},
	{"SyncLastRequestDeleteRoleTime", "d"},--time
	{"SyncVerifyCaptchaResult", "b"},--isOk
	{"SyncCancleDelRoleResult", "b"},--isOk

	{"MoveAllItemsToItemSuccess","Is"}, -- position,itemId
	{"TakeAllItemsOutFormItemSuccess","Is"},--position,itemId
	{"ShiMenLingShouCreated","s"}, --sceneId
	{"ShiMenLingShouPlayDone",""},

	{"ReplyVoteInfo", "ISU"},	--id, description, choices

	{"SyncScreenVoteInfo", "IU"},	--lastMonthTopId, choices
	{"SyncScreenVoteReult", "bII"},	--isOk


	--GroupIM
	{"GroupIMChangeOwner", "dd"}, --SerialNum, OwnerId
	{"BroadcastGroupIMMsg", "ddsiiSIiiiISU"}, --SerialNum, SendId, SendName, SendClass, SendGender, Message, SendTime, Expression, ExpressionTxt, Sticker, XianFanStatus, ExtraInfo, ProfileInfo_U
	{"SyncDataToPlayer", "U"}, --Data
	{"JoinGroupIM", "dII"}, --SerialNum, serverGroupId, GroupIMType
	{"LeaveGroupIM", "d"}, --SerialNum
	{"PlayerJoinGroupIM", "ddsIiiiiiIU"}, --SerialNum, playerId, playerName, level, class, gender, expression, expressionTxt, sticker, xianfanStatus, profileInfo_U
	{"PlayerLeaveGroupIM", "dd"}, --SerialNum, playerId
	{"GroupIMSetGroupName", "ds"}, --SerialNum, GroupName
	{"GroupIMSetGroupAnnouncement", "ds"}, --SerialNum, GroupAnnouncement
	{"GroupIMSetNeedApprove", "db"}, --SerialNum, bNeedApprove
	{"PlayerAddToApproveList", "dddsIiiiiiIssU"}, --SerialNum, inviterId, invitedId, invitedPlayerName, level, class, gender, expression, expressionTxt, sticker, xianfanStatus, inviterName, reason, profileInfo_U
	{"InvitePlayerJoinGroupIM", "ddss"}, --inviterId, SerialNum, GroupName, inviterName
	{"PlayerRemoveFromApproveList", "dd"}, --SerialNum, invitedId
	{"BroadcastVoiceTranslationInGroupIM", "ddsS"}, --playerId, self.m_SerialNum, voiceId, translation
	{"SetGroupIMIgnoreNewMsg", "db"}, --SerialNum, bIgnoreNewMsg
	{"GroupIMSyncRelationshipPropFrom", "ddsIiiiiiIU"}, --serialNum, playerId, name, level, class, gender, expression, expressionTxt, sticker, xianfanStatus, profileInfo_U

	--元旦
	{"ShowYuanDanCountDown", "i"}, --countDownValue
	{"ShowYuanDanHappyNewYear", ""},
	{"ShowYangYangFx", ""},
	{"NotifyYuanDanYaoQian", "I"}, --qianId
	{"ShowSubmitQiFuXiangWnd",""},
	{"ShowQiFuProgressWnd","II"}, -- myQiFuTimes,totalQiFuTimes
	{"SyncBaoWeiYuanDanZhuZiInfo", "U"}, --userdata (engineId-curVal)
	{"UpdateBaoWeiYuanDanZhuZiProgress", "II"}, --engineId, curVal

	-- 寒假活动
	{"SyncLingShouBattleGetPoint", "d"},  -- point
	{"SyncSceneLingShouBattlePoint", "bU"},  --isEnd, ud
	{"SyncLingShouBattlePersonalPoint", "d"},  -- point
	{"SyncLingShouBattleTransfromInfo", "d"},  -- monsterId
	{"SyncAccpetSecondStageTaskResult", "bI"}, --isOk, acceptTime
	{"SyncSecondStageKillMonsterNum", "dd"},--nowCount, needCount
	{"SyncHanJiaTaskStatus", "I"},--status

	{"HideDouHunSubstituteBtn", ""},

    -- 充值活动
    {"SendGiftAvailableShopActivityIds", "U"},    -- ids
    {"SendNewShopActivityIds", "U"},    -- ids

  	{"CancelGardenProgress", "sI"}, -- houseId, progressPos

  	{"PushLowLvFilterParamsToPlayer", "iisb"}, -- LowLvFilterLv, LowLvFilterVip, LowLvFilterChannels, EnableCodecCheck

  	-- 首席大弟子
  	{"SendShouXiDaDiZiBiWuWatchInfo", "UUUU"}, --player1Basic_U, player1Buff_U, player2Basic_U, player2Buff_U
  	{"ReplyShouXiDaDiZiBiWuOverView", "IIUUUUUU"}, --class, currentStage, NameTbl, stage1Data, stage2Data, stage3Data, stage4Data, stage5Data
  	{"ReplyShouXiDaDiZiBiWuStageDetails", "IIU"}, --class, currentStage, data

	{"RequestConfirmFixChuanjiabaoItem", "sI"}, -- itemId, cost

	{"SendEquipPresentName", "dss"}, -- playerId, itemId, playerName

	-- 元宵活动
	{"SyncYuanXiaoRiddle", "III"}, -- riddleTyle, riddleId, rightIndex
	{"SyncGuessRiddleResult", "Ib"}, -- riddleId, isCorrect

	--PGQY
	{"SendPGQYFightInfo","IUId"},--statType, msgpack.pack(data), playerCount, statTotal
	{"SendPGQYCleanStat","I"},--statType

	{"UpdatePvpSideName", "Ub"},
	{"UpdatePvpSideDPS", "U"},

	{"PlayerBuyMallItemResult","iIIbIs"},-- regionId, itemTemplateId, itemCount, result, faildReason, extra

	{"PlayerOpenChunJiePrayWnd",""},
	{"PlayerChunJiePraySuccess",""},
	{"PlayerChunJieKaiJingResult","iIs"}, -- type itemTemplateId itemId
	{"PlayerInputChunJieJiangPinInfoSuccess",""},

	-- 门派闯关增加队伍状态确认机制
	{"SyncChuangGuanWaitInfo","IIU"}, -- timeout, playId, msgpack.pack(WaitInfoData)
	{"SyncCloseChuanGuanWaitUI",""},

	--情人节活动
	{"StartValentineNewAskAndAnswer","IIdss"}, --questionId, state, expireTime, boyName, girlName
	{"ResetValentineAskAndAnswer", ""},
	{"SetValentineFlowerData", "U"},
	{"PreCheckGiveHelpValentineResult", "dIbIsdd"}, -- targetPlayerId, templateId, result, sceneTemplateId, sceneId, x, y
	{"SetValentineGetHelpCount", "I"}, -- ValentineGetHelpCount
	{"ValentineRequestInviteTeam", "ds"}, -- targetId, name
	{"ValentineRequestAddFriend", "ds"}, -- targetId, name

	{"QianLiYanTrackCheckSceneIndexRet","si"}, --sceneId,result

	-- 报名
	{"ShowSignUpRemindMsg", "Is"},						-- key msg
	{"RequestSignUpPlayByKeySuccess", "I"},			-- key
	{"RequestCancelSignUpPlayByKeySuccess", "I"},		-- key
	{"RequsetGetSignUpInfoByKeyResult", "Ib"},			-- key UD

	{"PlayJieBaiVideo","U"}, -- other player info list: class/gender/appearance
	{"SyncPlayerServerGroupId", "I"}, --serverGroupId
	{"NotifyYinLiangNotEnough",""},

	{"RequestOpenGuildCallUpWnd","UUU"}, -- guildMemberBasicInfoRpc
	{"PlayerBeCalledUpInGuildLeague","ids"}, -- callUpType, callerId, callerName

	{"SendPersonalHongBaoOpenState", "bdId"}, --openState, targetPlayerId, targetEngineId, validGiftLimit
	{"SetMultipleExpFxSwitch", "bI"}, --bShow, multilple
	{"SendPersonalHongBaoSuccess", ""},

	{"AddAutoClientPatch", "iS"}, --patchId, script
	{"ReplyTeamFightData", "U"}, --fightData_U

	-- 二级密码
	{"OpenSecondaryPasswordMobileVerifyWnd", "sI"}, --phoneNumber, tp
	{"OpenSecondaryPasswordVerifyWnd", ""},
	{"OpenSecondaryPasswordSetWnd", ""},
	{"ReplySecondaryPasswordProtectContent", "U"},  --protectContent_U
	{"SyncSecondaryPasswordInfo", "IU"}, --tp, info_U
	{"SecondaryPasswordVeryfiCaptchaSuccess", ""},

	-- 寇岛精英查询
	{"OpenEliteKouDaoWnd", "b"},	--hasRight
	{"AddWorldPositionFxWithDir","iiiId"},-- posX,posY,posZ,fxId,Dir

	-- 渠道删号
	{"OpenVerifyIdForDelRoleWnd", "I"}, -- actionType
	{"SyncVerifyDeleRoleIdResult", "b"}, -- isOK
	{"OpenQuDaoDeleteRoleInitWnd", ""},

	-- 手动关闭寇岛的模态二次确认
	{"ShowManualEndKouDaoConfirm", ""},
	{"PlayNewRoleGameVideoWithCP","sU"}, --videoname fakeappearance

	--青丘玩法
	{"QingQiuRequestEnterPart2", ""},
	{"QingQiuQueryScoreRankResult", "U"},
	{"QingQiuQueryFightDataRankResult", "U"},
	{"QingQiuOpenRankDialog", ""},
	{"QingQiuQueryPlayStateResult", "Id"}, -- state, startPlayTime
	{"ClearQingQiuLocalData", ""}, -- state
	{"QingQiuTeamMemberScoreUpdate", "U"}, --
	{"QingQiuAddScore", "I"}, --
	{"QingQiuQueryNpcAndMonsterLocationResult", "U"}, -- npcAndMonsterLocationDumpData

	--桃花仙占卜
	{"SetDivinationData", "Id"}, -- luckyTaskId, ExpiredTime,
	{"UpdateShiJian", "s"}, -- data
	{"SetDivinationQianWen", "I"}, -- qianWenId
	{"OpenDivinationDialog", ""},
	{"QueryDivinationLuckyTaskInfoResult", "IIIIs"}, -- luckyTaskId, taskId, needFinishTime, todayFinishTime, context
	{"DivinationAddShiJian", "I"}, -- ShiJianId

	-- 实名认证
	{"PlayerTryCertification","s"}, -- extraInfo
	{"PlayerTryCertificationResult","iS"}, -- result, extraInfo

	-- 打开内置浏览器
	{"OpenQYGS", ""},

	{"RunScript", "S"},

	{"OpenSubtileWnd", "I"}, --subtitleId


	--桃花宝匣
	{"SetTaoHuaBaoXiaGetHelpCount", "I"}, -- getHelpCount
	{"SetTaoHuaBaoXiaFlowerData", "U"},
	{"PreCheckGiveHelpTaoHuaBaoXiaResult", "dIbIsdd"}, -- targetPlayerId, templateId, result, sceneTemplateId, sceneId, x, y
	{"OpenTaoHuaCinemaShareWnd", ""}, --
	{"TaoHuaBaoXiaShowCinimaTicket", "s"}, --


	{"OpenTaskWipeGlassWnd","Ib"}, --taskId, hasSnow
	{"StartCollectPlayerVoice","Ib"}, --
	{"StopCollectPlayerVoice",""},

	{"OpenTaskVoiceQuestionWnd","IIiii"}, -- taskId, questionId,index,total,type
	{"CloseTaskVoiceQuestionWnd", ""},

	{"EmbracePlayerConfirm", "dIs"}, --playerId, expressionId, tpye

	-- 躲猫猫
	{"OpenDuoMaoMaoSignUpWnd", "bII"},			-- bool times times
	{"ShowDuoMaoMaoSkillIntroductionWnd", "b"},		-- bool
	{"SyncDuoMaoMaoFindSkillPoint", "I"},			-- times
	{"QueryDuoMaoMaoPlayInfoResult", "bbIIUU"},
	{"UpdateDuoMaoMaoLeftPlayerNum", "IIII"},

	{"OpenUrl", "s"}, --url

	{"BeginMakeGesture", "IIIb"}, -- taskId, gestureId, engineId, isLast

	{"OpenJuQingLetter",""},

	-- 精灵专家团
	{"SyncJingLingExpertRank","IU"}, -- toSendCount, rankUd
	{"SyncJingLingExpertRankEnd",""},	--
	{"SyncJingLingExpertActionResult","IIS"}, --actionType, code, message
	{"SyncJingLingExpertInitBasicInfo","U"}, -- basicInfoUd
	{"SyncJingLingExpertScore","ddI"}, --totalScore, totalConsumedScore, expertLevel
	{"SyncJingLingExpertRewardTimes","I"}, --rewardTimes
	{"SyncJingLingExpertWATimes","II"}, --rewardTimes, expireTime
	{"SyncJingLingExpertSwitch","bs"}, --isOpen, host
	{"SyncGetJingLingExpertExpireInfo",""},

	-- 拍卖红点提示方式
	{"SyncPaiMaiRemindWay","I"}, --remindWay

	{"PlayerFinishLengYueXinAllTask", "I"}, --num

	{"SyncExpressionEmbracePlayerId", "Id"}, --playerId

	{"OpenSeparateWaterWnd", "IIIII"}, -- taskId, waterA, waterB, containerA, containerB
	{"CloseSeparateWaterWnd", ""},

	{"UpdateReplaceSkillInfo", "UUb"}, --deletedSkills in msgpack, addedSkills in msgpack, bNotNotify

	{"LinearRushBegin", "IIdddd"}, -- skillId, engineId, speed, destX, destY, destZ
	{"LinearRushEnd", "II"}, -- skillId, engineId

	{"LinearPounceBegin", "IIdIdddddd"}, -- skillId, engineId, AttackerSpeed, AttackerEngineId, srcX, srcY, srcZ, destX, destY, destZ
	{"LinearPounceEnd", "II"}, -- skillId, engineId

	{"ThrowToTargetBegin", "II"}, -- skillId, engineId
	{"ThrowToTargetEnd", "II"}, -- skillId, engineId

	{"SubmitItemAwardConfirm", "IIs"}, --itemTemplateId, itemCount, itemName

	{"OpenMultipleQuestionsWnd", "I"}, -- questionId
	{"AnswerMutipleQuestions", ""},
	{"OpenDuoMaoMaoTransformWnd", ""},

	{"RequestOpenExchangeQnPoint2JadeWnd", "I"}, -- qnpoint

	{"OpenZhuJueJuQingEnterWnd", ""},
	{"AcceptZhuJueJuQingTaskSuccess", "I"}, 	--index

	-- qianshi
	{"SyncPlayOperateFurnitureStatus", "bI"}, -- isAllowToOp, furnitureFeatureModel
	{"SyncPlayFurnitureInfoToClient", "U"}, -- furnitureInfoList
	{"SyncPlayFurniturePosToClient", "U"}, -- furniturePosInfoList
	{"SyncPlayFurnitureNpcInfoToClient", "U"}, -- npcInfoList
	{"AddPlayFurnitureToClient", "IIIiii"}, -- engineId, npcTempId, furnitureTempId, x, y, dir
	{"MovePlayFurnitureToClient", "IIIiii"}, -- engineId, npcTempId, furnitureTempId, x, y, dir
	{"PackPlayFurnitureToClient", "III"}, -- engineId, npcTempId, furnitureTempId

	--Dice
	{"DiceStartPlay", ""},
	{"DiceEndBet", "U"}, --BetData
	{"DiceEndDice", "iiiUU"}, --diceResult_1, diceResult_2, diceResult_3, BetResultData, FailTimesData
	{"DicePlayResult", "b"}, --result

	{"TaskPlayCG", "Isbd"}, -- taskId, cgName, isLocal, volumPercent

	-- 帮会管理功勋
	{"AllocExtraGongXunSuccess", "iiU"}, -- remainGongXun, allocedGongXun, allocInfo_u(playerId, 本周分配的功勋列表)
	{"SyncAllocExtraGongXunStatus", "bbi"}, -- isOpen, canAlloc(有无权限), remainAllocTimes

	{"Vibrate", "I"},	--duration

	-- 周年庆门票
	{"SendZhouNianQingMenPiaoInfo", "IIIsss"},
	{"InputZhouNianQingMenPiaoInfoSuccess", ""},

	{"EnableWaterWaveMat", "b"}, -- toGray
	{"QueryMonthCardRedPointDone", "is"}, -- flag, extra
	{"AddGameVideoData","Ud"},
	{"SendGameVideoMilestoneDataBegin", ""},
	{"SendGameVideoMilestoneData", "U"},
	{"SendGameVideoMilestoneDataEnd", "U"},

	{"CloseMultipleQuestionsWnd", ""},

	--六一儿童节
	{"ShowSongReorderWnd", "sIII"}, --itemId, place, pos, songId
	{"ShowPhotoTakenWnd", ""},

    {"CastSkillJiTianZhiXie", "I"}, --engineId

    {"QosBeginRequest", "sb"}, -- phoneNumUrl, forceRetry
    {"QosSyncSuccess", "sI"}, -- id, expiretime
    {"QosSyncRemove", ""},

    {"QueryKeJuReviewInfoResult", "bi"},
    {"ShowKeJuReviewWnd", "i"},
    {"UpdateKeJuReviewRankInfo", "U"},
    {"UpdateKeJuReviewQuestionInfo", "UU"},

    {"ShowExpressionTo", "IIiiI"}, --engineId, expressionId, x, y, direction

	-- 塔防
    {"TowerDefenseSyncPlayStatusInfo", "iUs"}, -- status, playStatusDataUD, extra
    {"TowerDefenseSyncPlayPlayerInfoList", "Us"}, -- playerInfoListUD, extra
    {"TowerDefenseSyncMyPlayPlayerInfo", "Us"}, -- playerInfoUD, extra
    {"TowerDefenseAddFurniture", "IIdddIIs"}, -- id, templateId, x, y, z, rot, engineId, extra
    {"TowerDefenseRemoveFurniture", "Is"}, -- id, extra

	{"BiDongPlayerConfirm", "d"}, --driverId
	{"QiuHunPlayerConfirm", "d"}, --requesterId

	-- 表情动作外观
	{"UpdatePlayerExpressionAppearance", "iI"}, -- group, appearance
	{"UnlockExpressionAppearance", "iII"}, -- group, appearance, expireTime
	{"ExpireExpressionAppearance", "iI"}, -- group, appearance

    {"SyncUnLockExpressionGroupInfo", "s"}, --info

    {"TowerDefenseSyncMonsterHpInfoList", "Us"}, -- monsterHpInfoList, extra
    {"TowerDefenseSyncPlayerBaseHpInfo", "Us"}, -- monsterHpInfo, extra

	-- 陀螺仪玩法
	{"StartGyroCheck", "i"},  -- expireTime
	{"StopGyroCheck", ""},

	-- 用内置浏览器打开对应的活动
	{"OpenActivityWithBuiltInExplorer", "s"}, --activityName

	{"UpdatePersistPlayDataWithKey", "IU"}, -- key value
	{"UpdateTempPlayTimesWithKey", "IId"}, -- key value expiredTime

	{"PlayGameVideoWithTrackInfo", "sU"}, -- videoname, trackInfo

	-- 全民PK
	{"OpenQmpkRegisterWnd", ""},
	{"QmpkRegisterPersonalInfoSuccess", ""},
	{"OpenQmpkAllServerListWnd", "U"},
	{"ReplyQmpkSelfPersonalInfo", "dssiiiisdI"}, --fromCharacterId, fromServerName, fromCharacterName, goodAtClass, locationId, onlineTimeId, canZhiHui, mark, zhanduiId, loginCount
	{"UpdateQmpkPersonalInfoSuccess", ""},
	{"ReplyQmpkPlayerListQueryResult", "U"}, --dataUD
	{"ReplyQmpkPlayerDetailsQueryResult", "dUU"}, --targetId, fromCharacterData, currentCharacterData
	{"ReplyQmpkZhanDuiInfo", "ddUUssIIIs"}, --selfZhanDuiId, zhanduiId, memberData, chuzhanData, title, kouhao, needClass, needZhiHui, isApply, tips
	{"CreateQmpkZhanDuiSuccess", ""},
	{"ReplyQmpkZhanDuiApplyList", "IU"}, --totalPage, dataUD
	{"QmpkSetChuZhanInfoSuccess", ""},
    {"OpenQmpkBiWuChuZhanInfoWnd", "dsUU"}, --zhanduiId, zhanduiName, msgpack.pack(memberInfo), msgpack.pack(chuzhanInfo)
    {"QmpkWashXianJiaTalismanSuitSuccess", "Is"}, --suitIndex, dataStr
    {"QmpkOperateLingShouSuccess", "sU"}, --lingshouId, lingshou
    {"ReplyQmpkQieCuoTeamList", "dIIU"}, --selfQieCuoId, page, totalPage, dataUD
    {"ReplyQmpkPlayerPrayWordGroup", "IIIIUU"}, --engineId, diGeId, tianGeId, renGeId, fishGroupTblData, fishWordTblData
    {"ReplyQmpkZhanDuiList", "dIIUI"}, --selfZhanDuiId, page, totalPage, dataUD, source
    {"UpdateQmpkZhanDuiTitleAndKouHaoSuccess", "ss"}, --title, kouhao
    {"RequestJoinQmpkZhanDuiSuccess", "d"}, --zhanduiId
    {"PlayerRequestJoinQmpkZhanDui", ""},
    {"ReplyQmpkQieCuoTeam", "ddU"}, --selfQieCuoId, memberId, dataUD
    {"ReplyQmpkBiWuZhanKuang", "ssIIIIUIII"}, --attackName, defendName, attackWinCount, defendWinCount, finishRound, playRound, dataUD, selfForce, playStage, playIndex
    {"JoinQmpkFreeMatch", ""},
    {"QuitQmpkFreeMatch", ""},
    {"ReplyQmpkJiFenSaiRank", "UdIsUII"}, --rankData, zhanduiId, rank, title, memberData, JiFen, avgWinTime
    {"ReplyQmpkJiFenSaiWatchList", "U"}, --dataUD
    {"SignUpQmpkQieCuoSuccess", "d"}, --QieCuoId
    {"OpenQmpkSchedule", ""},
    {"SetQmpkZhanDuiRecruitInfoSuccess", "IIs"}, --needClass, needZhiHui, kouhao
    {"SyncQmpkLoginCount", "I"}, --count
    {"SyncQmpkOperatePlayerResourceFlag", "I"}, --flag
    {"ReplyQmpkAllMemberFightData", "IIUI"}, --playStage, playIndex, fightData, isWin
    {"QmpkBiWuPlayEnd", ""},
    {"ReplyQmpkTaoTaiSaiOverView", "I"}, --currentStage
    {"ReplyQmpkTaoTaiSaiWatchList", "IU"}, --stage, msgpack.pack(data)
    {"ReplyQmpkJingCaiData", "dIUssd"}, --playIndex, playRound, peilvData, zhanduiName1, zhanduiName2, daibi
    {"ReplyQmpkZongJueSaiOverView", "IdsUI"}, --index, zhanduiId, title, memberData, isFinish
    {"ReplyQmpkZongJueSaiWatchList", "IdsdsdI"} ,--playIndex, zhanduiId1, zhanduiTitle1, zhanduiId2, zhanduiTitle2, winZhanDuiId, start
    {"OpenQmpkBiWuOverView", "I"}, --playStage
    {"OpenQmpkJingCaiWnd", "I"}, --playStage
    {"OpenQmpkHistoryVideoWnd", ""},
    {"ReplyQmpkLingShouLingFuIndex", "IIII"}, --engineId, GeneralLingFu, SpecialLingFu, SuitLingFu
    {"ReplyQmpkMeiRiYiZhanInfo", "IUUI"}, --lianshengCount, rankData, dynamicData, isMatching
    {"JoinQmpkMeiRiYiZhan", ""},
    {"QuitQmpkMeiRiYiZhan", ""},
    {"ReplyQmpkJingCaiList", "IU"}, --playStage, listData
    {"ReplyQmpkJingCaiRecord", "U"}, --recordData
    {"ReplyQmpkDetailFightData", "IUUIss"}, --round, attackData, defendData, playStage, attackName, defendName
    {"ReplyQmpkShiLiZhiXingInfo", "IIU"}, --page, totalPage, data
    {"QmpkExcellentDataShare", "IId"}, -- playStage, tp, value

	{"StartWatchLive",""},
	{"StartWatchGameVideo","s"},
	{"EndWatchLiveOrGameVideo",""},

    {"LNT_RequestEnterInGateway", "dss"}, -- playerId, ticket, extra
    {"LNT_RequestEnterDone", "s"}, -- extra
    {"LNT_ClearTicketInfo", "ss"}, -- ticket, extra
    {"LNT_SetStatus", "is"}, -- status, extra
    {"LNT_InitPlayerPosition", "iii"}, -- posvX, posvY, posvZ

	-- 团队
	{"SyncTeamGroupChangeInTeam", "Ids"},
	{"AddTeamGroupApplicantInfo", "dU"},
	{"DelTeamGroupApplicantInfo", "d"},
	{"QueryTeamGroupApplicantInfoResult", "U"},
	{"RequestSetTeamGroupLeaderIdSuccess", "d"},
	{"RequestSetTeamGroupManagerSuccess", "d"},			-- playerId
	{"RequestCancelTeamGroupManagerSuccess", "d"},		-- playerId
	{"QueryTeamGroupTeamMemberInfoResult", "sdUU"},
	{"RequestCreateTeamGroupSuccess", ""},
	{"SetPlayerTeamGroupLeader", "d"},					-- playerId
	{"SetPlayerTeamGroupManager", ""},
	{"CancelPlayerTeamGroupManager", ""},
	{"UpdateTeamMemberOrderInTeamGroup", "IU"},			-- teamId
	{"SyncTeamGroupTeamIdInfo", "U"},
	{"ClearTeamGroupApplicantSuccess", ""},
	{"QueryTeamGroupTeamHpInfoResult", "IU"},
	{"DelTeamMemberOrderInTeamGroup", "I"},				-- teamId
	{"OpenTeamGroupWnd", ""},
	{"DisbandTeamGroupSuccess", ""},
	{"LeaveTeamGroupSuccess", ""},
	{"JoinTeamGroupSuccess", "d"},						-- playerId
	{"QueryTeamGroupMembersResult", "Ui"},
	-- {"SyncPlayerTeamGroup", "dS"},

	{"SendGameVideoSceneLeftTime","ii"},

	{"SendJuQingFail",""},--剧情失败

	--侠侣Pk
	{"XiaLvPkPlayerCancelSignUpConfirm", "s"}, -- name
	{"XialvPkSignUpStatusChanged", "b"},  ---- 是否报名
	{"XiaLvPkUpdateForceRoundData", "UUU"},
	{"XiaLvPkQueryStageMatchInfoResult", "U"},
	{"XiaLvPkQueryStageWatchInfoResult", "IU"},
	{"XiaLvPkSendWenShiQuestion", "ISUbIU"},
	{"XiaLvPkSendWenShiAnswer", "IU"},
	{"XiaLvPkSendWenShiScore", "U"},
	{"XiaLvPkOpenLearnTempSkillDialog", ""},
	{"XiaLvPkQueryTempSkillResult", "U"},
	{"XiaLvPkPlayerOpenWatchWnd", ""},
	{"XiaLvPkPlayerOpenMatchWnd", ""},
	{"XiaLvPkWenShiRoundEnd", ""},

	--跨服侠侣Pk
	{"CrossXiaLvPkPlayerCancelSignUpConfirm", "s"}, -- name
	{"CrossXialvPkSignUpStatusChanged", "b"},  ---- 是否报名
	{"CrossXiaLvPkUpdateForceRoundData", "UUU"},
	{"CrossXiaLvPkQueryStageMatchInfoResult", "U"},
	{"CrossXiaLvPkQueryStageWatchInfoResult", "IU"},
	{"CrossXiaLvPkSendWenShiQuestion", "ISUbIU"},
	{"CrossXiaLvPkSendWenShiAnswer", "IU"},
	{"CrossXiaLvPkSendWenShiScore", "U"},
	{"CrossXiaLvPkOpenLearnTempSkillDialog", ""},
	{"CrossXiaLvPkQueryTempSkillResult", "U"},
	{"CrossXiaLvPkPlayerOpenWatchWnd", ""},
	{"CrossXiaLvPkPlayerOpenMatchWnd", ""},
	{"CrossXiaLvPkWenShiRoundEnd", ""},


	-- 暑假玩法
	{"SyncCanReceiveFoodDetectItemPlayers", "iiU"}, -- onlinecnt, totalcnt, playerInfoList
	{"RequestSendFoodDetectItem", "ds"}, -- fromPlayerId, fromPlayerName
	{"RequestConfirmGetFoodDetectItemFromPlayer", "ds"}, -- fromPlayerId, fromPlayerName
	{"RequestGetFoodDetectItem", "ds"}, -- toPlayerId, toPlayerName
	{"SetCurrentDetectFoodDrug", "I"}, -- templateId
	{"PlaceFoodOnMapSuccess", ""},

	{"UpdateSubstituePlayerInfo", "dU"},

	{"SendExtraSceneTrapInfo", "U"}, -- TrapPointUD

	{"ServerTeamFollowDoMoveTo", "iidiii"}, -- x, y, speed, pathType, barrierType, regionLimit
	{"LimitRpc_SyncStatus", "Iis"}, -- engineId, limitRpcStatus, extra

	{"AddNewZuoQiConfirm", "sII"}, --itemId, place, pos
	{"AddNewFashionConfirm", "sII"}, --itemId, place, pos

	{"SyncGotSpecialFoodResult", "U"}, -- infolist

	{"OpenWnd","sU"}, --- 通用，打开指定窗口并传过来一个table参数
	{"CloseWnd", "sU"}, --通用，关闭指定窗口并传过来一个table参数

	{"DetectFoodResult", "bI"}, -- gotFood, foodId

	{"AddExtraProductConfirm", "IIsdI"},   -- place, pos, itemid, rate, durationTime
	{"AddExtraProduct", "sdII"}, -- houseId, rate, endTime, totalTime

	{"EnterHouseRoomConfirm", "I"}, ---

	{"SendOffLinePlayerHouse", "dss"}, -- targetPlayerId, houseId, houseName

	{"QueryGuanNingHistoryWeekDataResult", "UU"},
	{"UpdateGuanNingPlayerPersonalData", "dUU"},

	{"OpenTaskWipeMuBeiWnd", "I"}, -- taskId

	{"SendEnemyGuildInfoBegin", ""},
	{"SendEnemyGuildInfo", "IIIIss"},
	{"SendEnemyGuildInfoEnd", "bUd"},
	{"RequestSetEnemyGuildSuccess", "I"},
	{"RequestCancelEnemyGuildSuccess", "I"},

	{"SendCurrentHuoGuoMeterial", "U"}, --{RangeLowerLimit, RangeUpperLimit, MeterialUpperLimit, baJiao, huaJiao, guiPi}
	{"SubmitHuoGuoMeterialSuccess", "I"},

	{"SendJieBaiRank", "IUIdsUs"},
	{"QueryRankJieBaiInfoResult","sU"}, -- key,playerInfoList

	{"TryConfirmBaptizeWordResult","sUi"}, --equipId,fightpropUD,equipGrade
	{"TryConfirmResetTalismanExtraWordResult","sUi"}, --equipId, fightpropUD,equipGrade
	{"TryConfirmResetTalismanBasicWordResult","sUi"}, --equipId, fightpropUD,equipGrade

	{"EnableListenShakeDevice", ""},
	{"DisableListenShakeDevice", ""},

	{"ShowSendBangHuaHongBao", ""},
	{"ShowHuaMingShareWnd", "s"}, -- url

	{"OpenContinuousQuestionsWnd", "II"}, -- taskId, questionId
	{"CloseContinuousQuestionsWnd", ""},

	-- apphelper
	{"RequestBindRoleForAppHelper", ""},

	{"ConfirmOpenCookingWnd", "IsUb"}, -- taskId, uiPath, {cookId1, cookId2...}
	{"CookingPlayAddMeterial", "III"}, -- cookId, curProgress, totalProgress
	{"CloseCookingWnd", "b"}, -- finished
	{"QueryGuanNingRankDataResult", "U"},

	{"BeginShootBirds", "III"}, -- taskId, curCount, needCount
	{"FinishShootBirds", ""},
	{"OpenChangeGuildLeaderStatueWnd", "b"},

	{"CarnivalLotteryResult", "IU"},

	{"JXYSSendBossBenefitId", "Id"}, -- selfEngineObjectId, benefitCharacterId

	{"ApplyColorSystemSkill", "I"}, -- colorSystemSkillId
	{"CancelColorSystemSkill", "I"}, -- srcSkillId

	{"RequestZouYueForPlayer", "ds"}, -- audienceId, audienceName
	{"InviteConcert", "dsI"}, -- inviterId, inviterName, scoreId
	{"SyncExpressionSound", "IIIdb"}, -- engineId, expressionId, scoreId, beginTime, repeatSound
	{"BeginLeadSoundExpression", "II"}, -- expressionId, scoreId
	{"EndLeadSoundExpression", ""},

	{"JXYSSendTeleportRemainInfo", "IU"}, -- maxFloor, {floor => remainTimes}
	{"ShowJXYSComic", "I"}, -- comicId
	{"ShowJXYSGuideFx", "ii"}, -- x, y

	{"PlayPicture", "sIII"}, -- path, duration,width,height
	{"UpdateKeJuZhuBoInfo", "U"},

	{"OpenYinMengXiangWnd", "IU"}, -- taskId, {cookId1, cookId2}
	{"YinMengXiangAddMeterial", "III"}, -- cookId, curProgress, totalProgress
	{"OpenShakeYinMengXiangWnd", "I"}, -- taskId
	{"FinishShakeYinMengXiang", ""},

	{"MPTYSSyncStartPlayTime", "Id"}, -- startPlayTime, recovery
	{"MPTYSSyncSpeedUpTimes", "I"}, -- SpeedUpTimes
	{"MPTYSStartSelectHorse", "U"}, -- zuoqiData
	{"MPTYSOpenSignUpWnd", "b"}, -- isSignUp
	{"MPTYSSyncRankInfo", "U"}, -- playerName, zuoqiName, progress
	{"MPTYSFinishSelectHorse", "s"}, -- zuoqiId
	{"MPTYSSyncCurrentRank", "II"}, -- rank, progress
	{"MPTYSSyncSpeedUpTimesDirect", "I"}, -- SpeedUpTimes
	{"MPTYSSyncPreelectionSignInfo", "Ui"}, -- signInfo, playStatus
	{"MPTYSSyncScheduleInfo", "UIUii"}, -- groupInfo, groupId, followInfo, scheduleStatus, playStage
	{"MPTYSSyncPerspective", "di"}, -- playerId, perspective
	{"MPTYSSyncChampionInfo", "U"}, -- chanmpionInfo
	{"MPTYSShowWatchConfirm", "I"}, -- groupId

	{"OpenChaoDuRollPointWnd", "IIId"},
	{"ShowRollPointResultInChaoDuPlay", "III"},

	{"UpdateWeddingDayGiftInfo", "bdbdbd"}, -- honeyMoonGiftGot, canGetHoneyMoonGiftTime, hundredDayGiftGot, canGetHundredDayGiftTime, anniversaryGiftGot, canGetAnniversaryGiftTime
	{"OpenWeddingDayDeclareWnd", "ss"}, -- groomName, brideName
	{"WeddingDayDeclareSuccess", ""},
	{"SculptureWeddingRingSuccess", "U"},

	{"LoveQingQiuQuestion", "sI"}, -- questionContent, questionId
	{"LoveQingQiuAnswerQuestion", "ss"}, -- playerName, answerContent
	{"TaBenWeddingRingSuccess", "U"},
	{"GetGuanNingDoubleScoreSuccess", "I"},

	{"SyncAppHelperBindRoleResult", "b"}, -- isSuccess
	{"SendGuanNingMaxDataInfo", "U"},

	--本职业装备打孔转换
	{"SelfClsOpenEquipHolesTransferWnd",""},
	{"SelfClsQueryCanTransferHolesSourceEquipRet","U"}, -- positionlist
	{"SelfClsCanSelectTransferHoleSourceEquipRet","Is"}, --sourceEquipPos,sourceEquipId
	{"SelfClsQueryCanTransferHolesTargetEquipRet","IsU"}, --sourcePos,sourceEquipId,targetPosList
	{"SelfClsCanSelectTransferHoleTargetEquipRet","IsIsi"}, --sourcePos,sourceEquipId,targetPos,targetEquipId,costYuanBao
	{"SelfClsTransferEquipHoleSuccess","IsIs"}, --sourcePos,sourceEquipId,targetPos,targetEquipId

	{"RequestTargetZouYueSuccess", ""},
	{"InviteConcertSuccess", ""},


	{"NationalDayParadeQueryRankResult", "UU"},

	{"QueryYBZDPlayInfoResult", "iUiUb"}, -- defYueBingScore defTbl attYueBingScore attTbl bEnd
	{"UpdateAppearanceInYBZD", "bI"}, -- bEnter, force

	-- 常用设备库
	--{"CDVerifyBindPhoneResult", "b"}, --isSuccess

	{"SetZuoQiNumLimit", "I"}, --number

	{"SendYueGongQiFuSuccess", ""},
	{"QuerySelfYueGongQiFuResult", "U"}, -- qiFuList
	{"QueryRandomYueGongQiFuResult", "sdsIIdssd"}, -- qiFuId, senderId, senderName, senderClass, senderGender, receiverId, receiverName, qiFuContent, sendTime
	{"QueryYueGongQiFuByIdResult", "sdsIIdssd"}, -- qiFuId, senderId, senderName, senderClass, senderGender, receiverId, receiverName, qiFuContent, sendTime

	{"SendHuLuRank", "U"}, --rankUD

	{"NotifyServerGuaJiStatus", "Ibs"}, -- reason, isInGuajiStatus, extra
	-- 葫芦召唤玩法
	-- singOrderData 结构是玩家id组成的array
	{"SyncEnterHuLuSummon", "ddUI"}, -- createTime, endTime, singOrderData, npcEngineId
	{"SyncHuLuDrop", "IU"}, -- dropTimes, dropData
	{"SyncSummonPlayEnd", ""},

	{"SendHuLuOverview", "U"}, -- sceneOverviewData

	{"SetMonitorWords", "is"},

	-- 料罗湾海战
	{"LLW_SendWarshipSeatInfoToClient", "Uss"}, -- seatInfoUd, reason, extra
	{"LLW_SendWarshipBasicInfoToClient", "dddUs"}, -- hp, currentHpFull, hpFull, energy, extra
	{"LLW_SendPlayerWarshipInfoToClient", "is"}, -- seatIndex, extra
	{"LLW_SendWarshipWindInfoToClient", "sis"}, -- currentWindName, effectStatus, extra
	{"LLWT_SendGuideIndex", "is"}, -- guideIndex, extra

	-- 端午大作战2018
	{"DuanWuDZZ_SendPlayerPlayInfoToClient", "ds"}, -- score, extra
	{"DuanWuDZZ_SendPlayInfoToClient", "Uddds"}, -- playInfoUd, playTimeCount, teamScore, mvpPlayerId, extra
	{"DuanWuDZZ_SendOrderedRankListToClient", "Us"}, -- orderedRankListUd, extra

	-- 星官系统
	{"Xingguan_SyncXingguanProp", "Udds"}, -- xingguanPropUD, totalXingmangCount, usedXingmangCount, extra
	{"Xingguan_OpenWnd", "s"}, -- extra

	{"SyncFinalMaxLevelForEquip", "iis"}, -- finalMaxLevel, currentMaxLevel extra

	{"SyncPlayerChessDataToPlayer","dsIIU"}, -- playerId playerName costTime index chessmapdata
	{"PlayerSelectOneChessman","dIii"}, -- playerId playerEngineId x,y
	{"PlayerSelectOneChessmanFailed","dIii"}, -- playerId playerEngineId x,y
	{"PlayerMatchTwoChessmanSuccess","dIiiii"}, -- playerId playerEngineId x1,y1,x2,y2
	{"PlayerMatchTwoChessmanFailed","dIiiii"}, -- playerId playerEngineId x1,y1,x2,y2
	{"PlayerChessDone","dII"}, -- playerId playerEngineId costTime

	{"SyncGardenIsKuwei", "Ii"}, -- gardenIdx, isKuwei
	{"SyncHuluDefendSuccess", "I"}, -- gardenIdx
	{"SyncHuluMonsterHp", "Idd"}, -- engineId, hp, fullHp
	{"DelWeddingTaBenOnRingSuccess", "U"},
	{"OpenDelWeddingRingTaBenWnd", ""},

	{"ShowPlayFinishFx", ""},
	{"ReplyLianLianKanFinalOverView", "idUUUUUUU"}, -- round, allMoney, moneys, names, round1-round5
	{"ReplyLianLianKanFinalRoundDetails", "iU"}, -- round, data

	{"AddToAccusedTempBlacklist", "d"},
	{"ShowHuLuSummonNpcState", "II"}, --engineId, state (1表示小浮动摆动, 2表示大浮动)
	{"OpenDelWeddingRingTaBenSculptureContentWnd", ""},
	{"DelWeddingRingTaBenSculptureContentSuccess", "U"},

	-- NPC的titleName发生改变
	{"SyncNpcTitleName", "Is"}, -- engineId, title

	{"ChuHaiTanBaoSuccess", "I"}, -- id

	{"OpenHandWriteEffectWnd", "I"}, --writeId 手写界面
	{"OpenHandDrawWnd", "II"}, -- taskId, drawId 手绘界面
	{"OpenHandDrawOnlyPic", "I"}, -- drawId 手绘界面
	{"FinishHandDraw", ""},

	{"QueryHeChengGuiCostReturn", "IIsIII"}, --targetPlace, targetPos, targetEquipId, costYinLiang,costItemId,costItemCount
	{"RequestCanHeChengGuiReturn", "IIsbI"}, --targetPlace, targetPos, targetEquipId, canHeCheng or false , targetEquipNewGrade or 0
	{"RequestCanSwitchEquipWordSetReturn","IIsIb"}, --place, pos, equipId, newGrade or 0,result
	{"NotifyHeChengGuiSuccess","s"},--合成鬼成功 通知client equipId

	{"SendRelationPlayerIds","U"}, -- dict <id, relationType>

	{"RushBegin", "II"}, -- skillId, engineId
	{"RushCancel", "II"}, -- skillId, engineId

	{"OpenCommonPinTuWnd", "II"}, -- taskId, pinTuId
	{"FinishCommonPinTu", ""},

	{"SendQianKunDaiAntiScores", "bU"}, -- bRefineDone，dict<antiType,score>

	{"HeChengItemSuccess", "IsI"}, -- formulaId, itemId, score

	-- 飞升
	{"ReplyAdjustedEquipInfo", "sIIIIUU"}, --itemId, equipGrade, gemGroupId, intensifyLevel, forgeLevel, msgpack.pack(wordTbl), equip
	{"SyncQianShiExp", "d"}, --exp
	{"SyncFeiShengOpenStatus", "b"}, --status
	{"SyncXianShenLevel", "I"}, --xianshenLevel
	{"SendXianFanPropertyCompareResult", "U"}, --fightPropUD
	{"PlayerAboutToFeiSheng", ""},

	-- 财产保护
	{"SyncPropertyProtect", "bd"}, -- isMaintenanceZoneOpen, protectLeftTime


	--元旦 天降福蛋
	{"NewYearFuDanStartHatching", "d"}, -- expiredTime
	{"SyncNpcNameAndTitle","IIss"}, --npc:GetEngineId(), npc:GetTemplateId(), npc:BasicProp():GetName(), npc.m_TitleName

	-- 斗魂坛打赏
	{"OpenDouHunRewardWnd", "I"},					 -- serverid
	{"RequestDouHunServerRewardInfoResult", "U"},  -- allMemberInfoU
	{"RequestDouHunRewardRankInfoResult", "U"},

	{"ShowDanMuMessage", "s"},   -- msg
	{"ShowTeamGroupInviteWnd", "dsbs"},

	{"RefineTalismanTripodSuccess", "s"},

	{"RequestOpenBaGuaLuPosDone", "iIII"},
	{"SyncBaGuaLuZhuRuInfo", "iUI"},
	{"SyncBaGuaLuPlayStatus", "id"},
	{"SyncBaGuaLuOpenInfo", "id"},
	{"BaGuaLuFinishChouJiang", "II"},
	{"BaGuaLuShowResult", "dsI"},
	{"SyncPlayerTotalZhuRuInfo", "UU"}, -- zhuGuaPosTblUD, keGuaPosTblUD
	{"RequestEnableDouHunFuTiSuccess", "I"},

	{"OpenFeiShengQianKunDaiWnd", ""},
	{"QueryEquipForFeiShengQianKunDaiResult", "U"},

	{"SyncGridForceInHongLvZuoZhanPlay", "III"},

	--新年音乐会
	{"NewYearConcertStopTalk", ""},
	{"NewYearConcertStartQiangMai", ""},
	{"NewYearConcertQiangMaiSuccess", "d"},
	{"NewYearConcertStartLottery", ""},
	{"NewYearConcertEndLottery", ""},
	{"NewYearConcertQueryPlayStateResult", "Id"}, -- state, startPlayTime
	{"NewYearConcertSyncSceneAndQiangMaiState", "sU"}, -- sceneId, msgpack.pack(info)
	{"NewYearConcertQueryScoreRankResult", "U"}, -- rankData
	{"NewYearConcertUpdateTalkPlayerFlowerNum", "sdd"}, -- sceneId, playerId, flowerNum
	{"NewYearConcertLotteryResult", "U"}, --
	{"NewYearConcertLotteryReceived", ""}, --

	{"ChristmasQueryRankResult", "UU"}, -- rank, myrank
	{"ChristmasPutTreeSuccess", "UU"}, --playerInfo, sceneInfo
	{"UpdateQianDaoDaysAward", "II"},			-- id days

	-- 全民抢蛋
	{"RequestQMQDSignUpInfoResult", "IIb"},
	{"UpdateAppearanceInQMQD", "b"},
	{"ChangeQMQDPlayerAppearance", "Ib"},
	{"SyncEggInfoToPlayer", "II"},
	{"RequestQMQDPlayInfoResult", "U"},
	{"OnQMQDPlayRoundBegin", "I"},
	{"SyncQMQDSceneTime", "ii"},

	-- 雪球大战
	{"SignUpXueQiuSuccess", "b"}, -- bTeam
	{"CancelSignUpXueQiuSuccess", ""},
	{"SyncXueQiuPlayFightInfo", "iiiii"}, -- allcount, alivecount, killNum, score, executeScore
	{"SyncXueQiuEliteMonsterInfo", "ib"}, -- leftTime, bIsAlive
	{"SyncXueQiuPlayResult", "iii"}, -- rank, killNum, score
	{"UpdateXueQiuSkillItemSlots", "UiiIIii"}, -- idlist, 1 add or -1 del or 0, pos, skillItemId, engineId, x, y
	{"QuerySignUpXueQiuStatusResult", "bI"}, -- bSignUp, remainWeeklyRewardTimes
	{"QueryXueQiuPlayInfoResult", "Ub"}, -- resultTbl_U(id, name, killnum, score, rank, executed), bPlayEnd
	{"AddXueQiuUnsafeRegionFxs", "UIdbU"}, -- positions, fxId, dir, bNeedRemove, removePositions
	{"SyncXueQiuSafeRegionShrinkLeftTime", "i"}, -- leftTime
	{"QueryXueQiuPlayLocationInfoResult", "U"}, -- locationInfo

	{"SignUpXueQiuDoubleSuccess", "b"}, -- bTeam
	{"CancelSignUpXueQiuDoubleSuccess", ""},
	{"SyncXueQiuDoublePlayFightInfo", "iiiU"}, -- allcount, alivecount, killNum, score, executeScore
	{"SyncXueQiuDoubleEliteMonsterInfo", "ib"}, -- leftTime, bIsAlive
	{"SyncXueQiuDoublePlayResult", "UU"}, --
	{"UpdateXueQiuDoubleSkillItemSlots", "UiiIIii"}, -- idlist, 1 add or -1 del or 0, pos, skillItemId, engineId, x, y
	{"QuerySignUpXueQiuDoubleStatusResult", "bI"}, -- bSignUp, remainWeeklyRewardTimes
	{"QueryXueQiuDoublePlayInfoResult", "UbU"}, -- resultTbl_U(id, name, killnum, score, rank, executed), bPlayEnd, teamPlayInfo
	{"AddXueQiuDoubleUnsafeRegionFxs", "UIdbU"}, -- positions, fxId, dir, bNeedRemove, removePositions
	{"SyncXueQiuDoubleSafeRegionShrinkLeftTime", "i"}, -- leftTime
	{"QueryXueQiuDoublePlayLocationInfoResult", "U"}, -- locationInfo

	{"QueryXueQiuSeasonRankDataResult", "U"},
	{"QueryXueQiuSeasonRankResult", "IIUU"},
	{"XueQiuDoublePlaySyncWatching", "d"},


	{"SyncAllGridForceInHongLvZuoZhanPlay", "U"},

	{"QueryHongLvZuoZhanSignUpInfoResult", "bI"},
	{"RequestSignUpHongLvZuoZhanSuccess", "d"},
	{"RequestCancelSignUpHongLvZuoZhanSuccess", ""},

	{"OpenSelectTaskRewardItemWndOnSubmit","IIs"}, -- taskId npcEngineId selectItemsString

    {"RequestQianKunDaiRefineDoneSuccess", "s"},
    {"BuyItemFromSecretShopSuccess", ""},

    {"SendWorldPaiMaiPublicityGoods", "IUII"},--count,data, pageNum, goodClass
    {"ShowHongLvZuoZhanSkillInfoWnd", "i"},
	{"RequestAddDouHunTrainValueSuccess", "II"},

	{"SetDrunkStatus","bU"},--设置醉酒状态


    {"SetFlagLineGroundBlock", "Iiiii"},
    {"SetFlagCircleGroundBlock", "Iiii"},

    {"SyncNoSpaceOpenShop", "II"}, -- needSize, emptySpaceCount
	{"UpdateTempPlayDataWithKey", "IU"}, -- key value
	{"EraseTempPlayDataWithKey", "I"}, -- key

	{"SendNianShouPuzzleInfo", "U"},

	{"RaiseCameraView", "d"}, -- y
	{"CancelRaiseCameraView", ""},

	{"PlayerInputContactInfoByUseItemSuccess",""},

	{"BalloonReturnBeginTrack", "sIiiI"}, -- sceneId, sceneTemplateId, gridPosx, gridPosy, balloonId
	{"VoiceLovePlayStatusResult", "i"}, -- bossCount

	{"SendNSDZZPosInfo", "IU"},
	{"SendNSDZZSignUpInfo", "bb"},
	{"SendNSDZZBattleInfo", "IUU"},
	{"SendNSDZZExplodeInfo", "I"},
	{"SendNSDZZEndInfo", "bbIII"},
	{"SendNSDZZTeamInfo", "IIII"},

	{"PlayerQueryItemStockRet","Ii"}, --ItemTemplateId, Stock

	-- 取一个外挂怎么也想不到的名字,其实是查询进程信息描述
	{"SendLbsSimpleMsg", "ss"}, --processName, gmConnInfo
	{"SyncHuTianYiZhiBoStatus", "b"},

	{"ShowChristmasCardWnd", "dIIsdssdsd"}, --senderId, senderClass, senderGender, senderName, recieverId, recieverName, voiceUrl, voiceDuration, wishContent, senderTime
	{"ShowFeiShengTianJieFinishFx", "Iii"}, -- engineId, x, y

	{"AddBulletFx", "III"}, -- srcEngineId, dstEngineId, fxId
	{"PlayFakeConversation", "S"},

	{"OpenXinYiListWnd", ""},
	{"OpenXinYiMsgBoardWnd", ""},

	{"SyncBbsShareToken", "IIsSII"}, -- code, expireTime, token, ref-url, ziBingPrice, requestType

	{"UpdateGuanNingChallengeTaskInfo", "Iddbd"},
	{"QueryGuanNingChallengeTaskChoiceResult", "U"},
	{"ShowGuanNingContinueChallengeTaskWnd", "U"},

	{"TriggerBbsAutoShare", "IIs"}, -- shareType, itemTemplateId, itemInstanceId
	{"ShowGuanNingChallengeTaskChoiceRemind", ""},

	{"SyncNosUploadToken", "ISss"},

	{"SyncJXYSBossPos", "IIII"},

	{"QianLiYanXuFeiDone","Is"}, -- positionInBag, itemId

	-- 斗地主
	{"QuerySignUpOrChoosedGuildDouDiZhuPlayersResult", "bbbU"}, -- bCanSignUp, bSignUp, bFight, resultTbl_U
	{"OpenDouDiZhuWnd", "IbIbdU"}, -- index, isWatch, type, isRemote, ownerId, extradata
	{"SyncDouDiZhuPlayerInfo", "U"}, -- info
	{"SyncDouDiZhuCardsInfo", "IdIUIUb"}, -- index, playerId, handCardsCount, handCards, showCardsCount, showCards, bFaPai
	{"PlayerDouDiZhuReady", "Idb"}, -- index, playerId, ready
	{"PlayerQiangDiZhu", "IdIbId"}, -- index, playerId, status, bEnd, diZhuIndex, diZhuPlayerId
	{"PlayerMingPai", "IdIU"}, -- index, playerId, count, handCards_U
	{"PlayerChuPai", "IdIUi"}, -- index, playerId, count, showCards_U, usedTime
	{"SyncQiangDiZhuProgress", "Idbi"}, -- index, playerid, bJiaoDiZhu, leftTime
	{"SyncChuPaiProgress", "IdiI"}, -- index, playerId, leftTime, maxCardIndex
	{"SyncDuZhuScore", "ii"}, -- duzhu, power
	{"SyncDiZhuReserveCards", "IU"}, -- count, cards_U
	{"PlayerDouDiZhuTuoGuan", "Idb"}, -- index, playerId, bTuoGuan
	{"CloseDouDiZhuWnd", ""},
	{"SyncDouDiZhuBoardResult", "U"}, -- resultTbl_U
	{"DouDiZhuStartRound", ""},
	{"DouDiZhuEndRound", "Id"}, -- winnerIndex, winnerPlayerId
	{"QueryGuildDouDiZhuFightRankResult", "dsiiUIIb"}, -- playerId, name, score, rank, reusltTbl_U, stage, round, bFinalBroadcast
	{"DiZhuAddReserveCards", "IdUIU"}, -- index, playerId, reserveCards, count, handCards_U
	{"PlayerLeaveDouDiZhu", "Id"}, -- index, playerId
	{"ResetDouDiZhuRound", ""},
	{"ShowPlayerDouDiZhuShortMessage", "Idi"}, -- index, playerId, id
	{"ShowDouDiZhuRoundLeftTime", "biI"}, -- bShow, leftTime, time
	{"UpdateDouDiZhuPlayerScore", "Idi"}, -- index, playerId, score
	{"QueryGuildDouDiZhuChampionRecordResult", "U"}, -- resultTbl_U
	{"ShowDouDiZhuHandCards", "IdIU"}, -- index, playerId, handCardsCount, handCards
	{"DouDiZhuWithNpcBegin", "dII"}, -- playerId, engineId, slotIdx
	{"DouDiZhuWithNpcEnd", "d"}, -- playerId
	{"QueryDouDiZhuPlayersForWatchResult", "IU"}, -- type, resultTbl_U
	{"SignUpGuildDouDiZhuSuccess", ""},
	{"VoteGuildDouDiZhuSuccess", "d"}, -- votePlayerId
	{"CancelVoteGuildDouDiZhuSuccess", "d"}, -- votePlayerId

	{"SyncTianQiPlayData", "U"}, --- yumaInfo

	-- 纸鸢

	{"SyncZhiYuanPos", "ddddddd"}, --- playerId, posX, posY, posZ, accX accY, accZ
	{"SyncNewZhiYuanPos", "ddddddddddI"}, --- playerId, posX, posY, posZ, rolePosX, rolePosY, rolePosZ, accX accY, accZ, zhiYuanType
	{"BeginZhiYuanControl", "U"},
	{"BatchSyncZhiYuanDropInfo", "U"},
	{"SyncZhiYuanScore", "dI"},
	{"SyncZhiYuanOneDropReplaced", "sU"},
	{"EndZhiYuanControl", "II"}, --score, time
	{"SyncZhiYuanLeave", "d"}, -- playerId
	{"SyncZhiYuanFxChange", "dI"}, -- playerId, fxIdx
	{"GetPlayerHighForZhiYuan", "I"}, -- engineId,
	{"SyncZhiYuanRankData", "IUU"}, -- rankDataUd

	-- 引导技能读条
	{"ShowSkillLeadingProcess", "Ii"},	-- sourceId, remain time
	{"StopSkillLeadingProcess", "I"},	-- sourceId

	-- 仙踪山合战
	{"CrystalSyncForcePlayerInfo", "U"},
	{"CrystalSyncForcePlayInfo", "UU"},
	{"CrystalOnCrystalGenerated", "III"},
	{"CrystalSearchCrystalAuraModeResult", "III"},
	{"CrystalSyncTeammatePos", "U"},
	{"UpdateAppearanceInCrystal", "b"},
	{"CrystalSyncGuardPos", "bIIS"},

	-- 世界事件
	{"SyncWorldEventMessageIdx", "ibb"}, -- messageIdx, isPlayFx, isShowRedPoint
	{"AddWorldEventLiuXingYuFx", "U"},
	{"SyncViewedMessageIdx", "iU"},

	--支机石
	{"RequestUpgradeEquipWordCostReturn","IIsi"}, -- place, pos, equipId, itemCount
	{"UpgradeEquipWordSuccess","sII"}, --oldWordId, newWordId

	--X锤一炼
	{"RequestChuiLianEquipWordCostReturn","IIsIi"}, -- place, pos, equipId, chuilianItemTemplateId, itemCount
	{"ChuiLianEquipWordSuccess", "sU"}, --equipId,temp equip property
	{"ChuiLianEquipWordFail","s"}, --equipId

	--鉴定技能
	{"ApplyIdentifySkill", "Is"},
	{"CancelIdentifySkill", "I"},
	{"IdentifySkillWashCost", "sU"},
	{"IdentifySkillApplyFeeback", "sU"},
	{"IdentifySkillExtendCost", "sIdIi"},
	{"IdentifySkillExchangeCost", "ssdIiIi"},

	{"IdentifySkillRequestWashSkillResult", "bsibbII"},
	{"IdentifySkillRequestApplyTempSkillResult", "bs"},
	{"IdentifySkillRequestExtendSkillResult", "bsI"},
	{"IdentifySkillRequestExchangeSkillResult", "bss"},
	{"IdentifySkillSetDisableResult", "bsb"},
	{"IdentifySkillUpdateOtherPlayerEquipScoreSuccess", "ds"},

	{"ShowSkillAquisitionWnd", "I"},

	{"CloseUnlockWithKeyWnd", "I"}, -- taskId
	{"OpenUnlockWithKeyWnd", "II"}, -- taskId, countdown

	{"QueryHuiLiuDanInvitePlayersResult", "U"}, -- resultTbl_U
	{"InvitePlayerWithHuiLiuDanSuccess", ""},

	{"PlaySceneObjectAnimation", "ss"}, -- objectPath, animation

	{"MoveOutItemFormItemForOnShelfSuccess","IIIU"},

	{"ZhouNianShopQueryPackageResult", "UI"},
	{"ZhouNianShopBuyPackageSuccess", ""},
	{"ZhouNianShopStatusQueryResult", "bbI"},

	{"ZhouNianShopAlertStatusQueryResult", "bI"},


	{"CcChatBroadcastMessage", "U"},

	{"SetNextLoadingPicToBlack", "I"},

	{"SetSceneBrightness","d"}, --设置客户端场景亮度
	{"IncreaseSceneBrightness","d"}, --

	{"OpenLightingWnd", "III"}, -- taskId, subTitleId, showType

	-- 世界杯
	{"ShowWorldCupJingCaiAwardReddot", ""},
	{"SendWorldCupTodayJingCaiInfo", "IIUssI"},
	{"ReplyWorldCupTodayJingCaiRecord", "U"},
	{"ReplyWorldCupTodayJingCaiDetail", "IUs"},
	{"SendRenRenZhongCaiPiaoInfo", "sIIs"}, --itemId, place, pos, code
	{"ReplyWorldCupHomeTeamPlayInfo", "IUbIII"}, --tid, data, bAlert, groupRank, settimes, taotaiStage
	{"ReplyWorldCupAgendaInfo", "IU"}, --tid, roundData

	{"KickNextTempleGroup", "Us"}, -- engineIdTbl, animation

	-- 兵器谱
	{"QueryBQPForSubmitResult", "UU"}, -- submittedUd, thresholdUd
	{"BQPSubmitReplaceConfirm", "sI"}, -- equipId, equipType
	{"SendBingQiPuRank", "IU"}, -- mainRankType, rankUd
	{"BingQiPuQueryEquipRes", "U"}, -- detailUd
	{"SendBQPRecommendEquip", "bIU"}, -- bPreciousRank, equipType, rankUd
	{"BingQiPuOpResult", "bII"}, -- bSuccess, opType, enumReason
	{"BroadcastBQPEquipToWorld", "U"}, -- equipId
	{"SendBqpQueryShareRes", "sIIUUIb"}, -- equipId, rankPos, extraRewardIdx, praiserNameInfoU, dataString, usedPraisedTimes, bPreciousRank
	{"SyncBQPStage", "I"}, -- stage
	{"SendBQPSponsorRank", "U"}, -- rankUd
	{"SendBQPPreciousRank", "U"}, -- rankUd
	{"SendBQPHistory", "U"}, -- rankUd
	{"SyncBQPPraiseInfo", "III"}, -- expireTime, equipType,Times
	{"SyncBQPHistoryDetail", "U"}, -- dataString
	{"SyncBQPMisc", "IU"}, -- infoType, dataString

	{"PlayLocalCG", "s"}, -- cgName
	{"OpenPersonalSpaceShareWnd", ""}, -- 打开梦岛分享界面
	{"OpenLanRuoSiShareWnd", ""}, -- 打开兰若分享界面
	{"ShareLanRuoSiSuccess", ""},

	{"SyncNpcBubbleInfo", "IId"}, -- npcEngineId, blowTime, scale
	{"SendZhuoJiSignUpInfo", "bbi"}, -- isSignUp, forceOpen, rewardRemainTimes
	{"QueryZhuoJiPlayInfoResult", "U"}, -- resultTbl_U
	{"UpdateZhuoJiForceScore", "Ii"}, -- force, score
	{"ShowZhuoJiPlayResult", "Iii"}, -- force, score, rank
	{"PlayerCatchChicken", "dI"}, -- playerId, chickenEngineId
	{"PlayerReleaseChicken", "d"}, -- playerId
	{"ShowZhuoJiSkillsTip", "U"}, -- skillList_U

	{"QueryCuJuWeekRankResult", "IUU"},
	{"QueryCuJuTotalRankResult", "IUU"},
	{"RequestUseCuJuLotterySuccess", "I"},
	{"QueryCuJuCrossSignUpResult", "bII"},

	{"QueryCloseFriendInfoResult", "UU"},
	{"QueryCloseFriendListResult", "U"},

	{"DownloadNpcVoice", "I"}, -- notifyType
	{"SyncPlayerOxygen", "I"},
	{"SBCSPlayAddLinkFx", "III"},
	{"SBCSPlayRemoveLinkFx", "II"},
	{"SBCSSyncFinishInfo", "UII"},

	{"RequestMakeShenBingSuccess", ""},
	{"RequestTrainShenBingSuccess", ""},
	{"RequestBreakShenBingTrainLevelSuccess", ""},
	{"RequestRecastShenBingSuccess", ""},
	{"RequestRestoreShenBingSuccess", ""},
	{"RequestExchangeQianYuanSuccess", ""},

	{"ShowJXYSMapDetail", "b"}, -- show
	{"SyncZhiBoStatus", "bsbsssIUU"}, -- bOpen, url, bJump, zhuboName, ccid, showText, idFlag, levelDataUd, extraDataUd

	{"QueryWuCaiShaBingSignUpStatusDone", "bIb"},
	{"WuCaiShaBingShowDetail", "IIU"}, -- playerGroupId, showGroupId, progressData
	{"WuCaiShaBingShowBattleInfo", "IU"}, --playerGroupId, {groupId => progressData}
	{"WuCaiShaBingShowResult", "IIU"}, -- playerGroupId, winGroupId, {groupId => playerInfoTbl}
	{"WuCaiShaBingShowState", "IU"}, -- playerGroupId, {groupId => progressData}
	{"WuCaiShaBingSubmitMaterialDone", "I"}, -- materialId
	{"WuCaiShaBingStealMaterialDone", "I"}, -- materialId

	-- 斗魂坛点赞
	{"RequestDouHunPraiseServerListResult", "U"},
	{"RequestDouHunPraiseServerInfoResult", "sU"},
	{"RequestDouHunPraiseRankResult", "U"},
	{"RequestDouHunPraisePlayerResult", "bdsI"},

	{"SyncCheckBQPComment", "b"},

	-- 合服比拼
	{"QueryMergeBattleOverviewResult", "IIbbIssUUII"}, -- beginTime, endTime, bStart, bEnd, winner, serverName1, serverName2, data1, data2, totalScore, force
	{"QueryMergeBattleInviteHuiLiuInfoResult", "dIU"}, -- bindCode, count, resultData
	{"QueryMergeBattleBindCodePlayerNameResult", "ds"}, -- bindPlayerId, bindPlayerName
	{"QueryMergeBattleHuiLiuBindStatusResult", "bbds"}, -- canbind, hasbind, bindPlayerId, bindPlayerName
	{"MergeBattleHuiLiuPlayerBindSuccess", "ds"}, -- bindPlayerId, bindPlayerName
	{"QueryMergeBattlePersonalEventsResult", "U"}, -- finishedEvents
	{"QueryMergeBattlePersonalScoresResult", "U"}, -- groupScores
	{"ChangeServerNameToLastMergeServer", "IIssb"}, -- serverId, lastMergeServerId, sectionName, serverName, bRevert

	-- cangbaoge
	{"CBG_SendConditionsInfo", "sUs"}, -- requestType, conditionsInfoUd, extra
	{"CBG_SendAllPlayerInfo_Begin", "s"}, -- extra
	{"CBG_SendAllPlayerInfo", "Us"}, -- allPlayerInfoUd, extra
	{"CBG_SendAllPlayerInfo_End", "s"}, -- extra

	-- 2018嘉年华线上活动
	{"SyncSmallPrizeDrawIndex", "I"},
	{"SyncBigPrizeDrawIndex", "I"},
	{"SyncStampCollectProgress", "II"},
	{"SyncCarnivalTickStock", "IIU"},
	{"SyncBuyCarnivalTickDone", "I"},
	{"SyncCarnivalTicketCountdown", "bI"},
	{"SyncCarnivalPrizedLocationId", "I"},
	{"SyncCarnivalBigPrized", ""},

	-- 跨服首席大弟子
	{"CrossSXDDZChallenged", "dssI"},
	{"CrossSXDDZShowStartCountDown", "dssI"},
	{"CrossSXDDZQueryCurrentChallengeInfoResult", "U"}, --infoUD

	{"SyncQmpkPlayData", "I"}, --playStage
	{"SyncPVPCommand", "IIIds"},

	{"SyncGuiMenGuanInfo", "bIIU"},
	{"GuiMenGuanTrackToBoss", "sIII"},

	{"CheckGameAudioOpenStatus", ""},
	{"ProduceShenBingRanLiaoByMaterialSuccess", "U"},
	{"ProduceShenBingRanLiaoByRanLiaoSuccess", "U"},
	{"RequestShenBingRanSeSuccess", "UI"},

	{"RequestBindRoleForDaShen", ""},

	{"OpenScreenBroken", "ss"},
	{"RequestOpenScreenBroken", "I"}, -- taskId
	{"RequestOpenScreenBrokenWithoutExit", "Iss"}, -- taskId, hintMsg, color
	{"RequestResetShenBingRanSeSuccess", ""},

	{"RequestBindRoleForDaShenSuccess", "s"},

	{"SendCrossGuildRank", "IUIdUsIIIs"},

	-- 跨服城战
	{"SyncGuildCityWarMaterial", "dd"}, -- guildId, material
	{"ShowCityToPlayer", "UUU"}, -- basicInfo, unitInfo, repositoryInfo
	{"SendCityUnitAgentEngineIds", "sU"}, -- cityId, engineIdsUD
	{"HideCity", "s"}, -- cityId
	{"CreateCityUnit", "sII"}, -- cityId, templateId, unitId
	{"AddCityUnit", "sIIiiiI"}, -- cityId, templateId, unitId, x, y, dir, engineId
	{"MoveCityUnit", "sIiiiI"}, -- cityId, unitId, x, y, dir, engineId
	{"RemoveCityUnit", "sIb"}, -- cityId, unitId, bNoRefreshUI
	{"ClearCityUnits", "s"}, -- cityId
	{"UpgradeCityUnit", "sIII"}, -- cityId, tempalteId, unitId, engineId
	{"UpdateCityGrade", "sI"}, -- cityId, grade

	{"SendRepairCityUnitInfo", "sIIbI"},
	{"SendRepairAllCityUnitInfo", "sbI"},

	{"RequestUseCityWarLayoutTemplateCheckResult", "sbI"}, -- cityId, bSuccess, index

	{"SyncCityOccupyingProgress", "IIIds"}, -- templateId, npcEngineId, progress, guildId, guildName
	{"ShowOccupyHuFuConfirmWnd", "dIIsdII"}, -- guildId, mapId, templateId, occupyCityId, duration, currMapId, currTemplateId
	{"QueryTerritoryMapsOverviewResult", "IIUUU"}, -- guildMapId, guildTemplateId, serverInfoUD, overviewUD, biaoCheInfoUd
	{"QueryTerritoryMapDetailResult", "IIU"}, -- mapId, guildCityTemplateId, mapDetailUD
	{"ShowTerritoryCityTips", "IIsdsIsIdIIdIUbI"}, -- mapId, templateId, cityId, ownerId, ownerName, ownerServerId, ownerServerName, grade, material, unitCount, fightState, duration, playNum, playIdsUD, isPriorityMatchPeriod, priorityMatchLeftTime
	{"SendMyCityData", "IUUUU"}, -- actionType, guildPlayDataUD, basicInfoUD, unitInfoUD, miscInfoUD
	{"UpdateMirrorWarOccupyInfo", "IIIII"}, -- warIndex, force, activated, progress, occupyForce
	{"QueryMirrorWarSummaryResult", "bIUUU"}, -- finish, winForce, playResultUD, forceInfoUD, summaryInfoUD
	{"QueryMirrorWarPlayInfoResult", "IbIUUU"}, -- warIndex, finish, winForce, forceInfoUD, summaryInfoUD, playerInfoUD
	{"ShowMirrorWarMapInfo", "IUbII"}, -- myMapId, infoUD, isPriorityMatchPeriod, priorityMatchLeftTime, declaredPriorityMatchMapId
	{"UpdateMirrorWarForceInfo", "UI"}, -- guildForceInfoUD, warIndex
	{"UpdateMirrorWarRebornPoints", "U"}, -- infoUD
	{"AlterCityMissionSuccess", "s"}, -- mission
	{"QueryMirrorWarWatchInfoResult", "U"}, -- dataUD

	{"CreateBuildingObject", "IIII"},
	{"DestroyBuildingObject", "I"},

	{"SendCityWarHistory", "U"}, -- resUD
	{"SendCityWarHistoryEnd", ""},
	{"SendCityWarHistoryPlaySummary", "IUUU"}, -- winForce, playResultUD, forceInfoUD, summaryInfoUD
	{"QueryEnemyPlayerPosResult", "U"}, -- playerPos
	{"ExchangeCityWarGuildZiCaiSuccess", ""},
	{"QueryCityWarServerListResult", "UI"}, -- serverListUD, playerServerGroupId
	{"QueryCityWarServerCityListResult", "UI"}, -- cityListUD, serverId
	{"QueryMirrorWarPlayNumResult", "I"}, -- playNum
	{"QueryCityWarOccupyOpenResult", "b"}, -- bOpen
	{"QueryCityWarGuildToCityHashResult", "U"}, -- dataUD

	{"ReplyServerSubmitInfo", "U"}, --data

	-- 国庆校场
	{"GuoQingJiaoChangSyncForcePlayerInfo", "U"},
	{"GuoQingJiaoChangSyncMapInfo", "IIIU"},
	{"GuoQingJiaoChangSyncTeamPos", "U"},
	{"GuoQingJiaoChangOpenSelectTemp", "UIU"},
	{"RequestSignUpGQJCResult", "b"},
	{"ShowPlayerInfoNotInTeamGroupInGuildLeague", "U"},

	-- baby
	{"SendBaby", "U"}, -- babyUD
	{"DeleteBaby", "s"}, -- babyId
	{"UpdateBabyData", "sU"}, -- babyId, babyUD
	{"PlayerPlanBabySuccess", "s"}, -- babyId
	{"ReceiveBabyPlanAwardSuccess", "s"}, -- babyId
	{"RefreshBabyPlanSuccess", "s"}, -- babyId
	{"CheckBirthCertificateSuccess", "bU"}, -- isCouple, conditionResult
	{"InspectBabySuccess", "IU"}, -- inspectTime, inspectInfo
	{"SendInspectInfo", "IU"},  -- inspectTime, inspectInfo
	{"StartSelectDialog", ""},
	{"AcceptTaskFromBabySuccess", "s"}, -- babyId
	{"PlayerRenameBabySuccess", "s"}, -- babyId
	{"OpenNamingBabyWnd", "s"}, -- babyId
	{"SyncCarouselPassengers", "IU"}, -- {pos1, engineId1, pos2, engineId2, ...}
	{"StartRotateCarousel", "I"},
	{"StopRotateCarousel", ""},
	{"SyncPlayerPregnant", "U"},
	{"ReceiveQiYuAwardSuccess", "s"}, -- babyId
	{"QueryRestorableBabySuccess", "U"}, -- babyInfoUD
	{"ActiveBabyFashionColorSuccess", "s"}, -- babyId
	{"AddBabyFashionSuccess", "s"}, -- babyId
	{"RemoveBabyFashionSuccess", "s"}, -- babyId
	{"ActiveBabyExpressionSuccess", "s"}, -- babyId
	{"SaveBabyFashionInfoSuccess", "s"}, -- babyId
	{"PlayerEnterPregnantStatus", "Ib"}, -- taiMengId, isSingle
	{"OpenYangYuWnd", "I"}, --type
	{"QueryBabyNpcInteractSuccess", "II"}, -- type, queryType
	{"AcceptQiYuTaskFromBabySuccess", "s"}, -- babyId
	{"SyncYangYuRedPoint", "b"},
	{"QueryBabyParentsNameDone", "sss"}, -- babyId, fatherId, motherId
	{"QueryHouseBabyListDone", "dU"},
	{"PlayerFeedingBabyNpcSuccess", "s"}, -- babyId
	{"FinishedDeliveryBaby", "b"}, -- isSingleMale
	{"SendOrphanInfo", "U"}, -- babyInfoTblUD
	{"QueryBabyFollowStatusDone", "b"},
	{"OpenBabyChatWnd", "s"}, -- babyId
	{"ReplyChatMessageToBabySuccess", "sIsS"},
	{"ReplyChatMessageToBabyFail", "sIs"},
	{"SendBabyGradeAndExp", "IIII"}, -- npcEngineId, grade, exp, remainTimes
	{"SyncNpcLevelToClient", "II"}, -- npcEngineId, level
	{"QueryBabyNpcInfoDone", "U"},
	{"PushBabyChatMessage", "ssS"},
	{"ActiveBabyQiChangSuccess", "sI"}, -- babyId, newQiChangId
	{"SyncPlayerOwnBabyInfo", "II"}, -- qiChangId, grade
	{"SetShowQiChangIdSuccess", "sI"}, -- babyId, qiChangId
	{"ChangeBabyStatusSuccess", "s"}, -- babyId
	{"PlantAndHarvestInHouseSuccess", "s"}, -- babyId
	{"BabyPlantAndHarvestComeBack", "s"}, -- babyId

	-- 设置客户端自动选择技能目标时，是否把对象当做玩家处理
	{"SetIsTreatedAsPlayerForPlay", "sb"}, -- playname, bSet

	-- 倩女知道玩家基本信息
	{"SyncJingLingExpertRoleInfo", "UU"}, --basicInfoUd, questionInfoUd


	-- 二设
	{"ErSheQueryOuyuRoleResult", "IU"},
	{"ErSheRequestStartJuQingResult", "IbI"},
	{"ErSheQueryZhaoHuanRoleListResult", "U"},
	{"ErSheRequestZhaoHuanRoleResult", "IU"},
	{"ErSheRequestTouchRoleResult", "IbI"},
	{"ErSheRequestBuyGiftSuccess", "II"},
	{"ErSheShowMessage", "IU"},
	{"ErSheRoleProgressChanged", "III"},
	{"ErSheFinishedJuQing", "III"},
	{"ErSheQueryFinishedJuQingResult", "U"},
	{"ErSheGetJuQingHistoryResult", "IU"},


	{"UpdateAppearanceInPlay", "sb"},


	-- {"AmusementParkCancelTakePhoto", "I"},
	-- {"AmusementParkReadyToTakePhoto", "I"},
	{"AmusementParkTakePhoto", "s"},
	{"OpenHuaZhuZiWnd", ""},

	{"SyncYaYunResNpc", "II"}, -- npcId, count
	{"QueryCityInfoForCityWarZhanLongResult", "dU"},
	{"SetCityWarZhanLongEnemyGuildIdSuccess", "d"},
	{"OpenGrabMaterialWndForCityWarZhanLong", "sII"},

	{"PracticeLianShenSkillSuccess", "IIII"}, --skillId, type, effectSkillLv, alreadyPracticeNum
	{"ReplyCityWarPlayOpenStatus", "b"}, --status

	{"SyncMingWangChange", "II"}, -- score, lastGainTime
	{"SyncBiaoCheNpcPos", "IIIU"}, -- npcTemplateId, x, y, overviewUd

	{"RearingTestSendQuestion", "ISUII"},
	{"RearingTestSendAnswer", "IIIs"},
	{"RearingTestStop", "s"},
	{"RearingTestFinished", "II"},

	{"SyncCityWarPlayerGuildNameInfo", "U"}, --playerId2GuildName

	{"SHTYSyncStatusToPlayersInPlay", "IIU"}, --playStatus, enterStatusTime, extraData

	{"ReplySetShenBingEffectHide", "I"}, --value

	--2018 连连看
	{"UpdateLianLianKanChessData","iU"},
	{"UpdateAllPlayerScoreToClient", "U"}, -- playerId, name, socre
	{"LianLianKanPlayerSelectOneChessman","dIii"}, -- playerId playerEngineId x,y
	{"LianLianKanPlayerCancelSelectOneChessman","dIii"}, -- playerId playerEngineId x,y
	{"LianLianKanPlayerSelectOneChessmanFail","dIii"}, -- playerId playerEngineId x,y
	{"LianLianKanPlayerMatchChessSuccess","dIiiii"}, -- playerId playerEngineId x1,y1,x2,y2
	{"LianLianKanPlayerMatchChessFail","dIiiii"}, -- playerId playerEngineId x1,y1,x2,y2
	{"OpenLianLianKanQuestionWnd","sss"}, -- question, answer1 answer2
	{"CloseLianLianKanQuestionWnd",""}, --
	{"SendLianLianKanSettlementData","biU"}, --
	{"QueryLianLianKan2018RankReturn","Ubii"}, -- rankDataUD, inRank selfScore selfCostTime
	{"RequestLianLianKanBonusPoolReturn","i"}, -- bonus


	{"SendCityWarZiCaiShopInfo", "dS"}, --zicai, items

	{"SyncSetGuildBiaoCheResult","bI"}, -- bSuccess, reason
	{"SyncSetBiaoChePower","b"}, -- bHasPower
	{"SyncResetGuildBiaoCheResult","bI"}, -- bSuccess, reason
	{"UpdateGuildBiaoCheInfo","IU"}, -- actionType, biaoCheOverviewUd

	{"ReplyCityWarGuildZiCaiInfo", "dd"}, --zicai, limit

	-- 2018万圣节
	{"BaoWeiNanGuaSyncPlayInfo", "UU"},
	{"BaoWeiNanGuaResult", "bU"},

	{"SyncQueryGuildAltarResult", "UU"},
	{"SyncYaYunTaskStatusChange", "III"}, -- newStatus, reason, expireTime
	{"SetPropertyAppearanceSettingInfoResult", "III"},
	{"DoTrackToBiaoChe", "sIIII"}, -- sceneId, biaoCheEngineId, sceneTemplateId, x, y
	{"UpdateSubmitBiaoCheWnd", "U"},

	{"SyncMonsterCircleGroundBlockInfo", "Iiii"}, --engineId, radius, zMin, zMax

	{"ReplyAutoShangJiaItemMoneyAndDiscount","U"},
	{"QueryAllZhongCaoCountRet","iU"}, -- progress,data
	{"PlayerZhongCaoRet","ii"},

	{"ReplyActivatedAltarCount", "I"}, --count
	{"QueryGuildSilveForBiaoCheRes", "dIdU"}, -- guildSilver, mapId, cost, costTbl
	{"QuerySetGuildBiaoCheCostAllRes", "U"}, -- costTbl

	{"SyncMonsterSiegePlayProgressInfo", "ddIII"},
	{"CheckAcceleration", "dI"},
	{"SetMemberExtraNameSuccess", "ds"},

	{"BabyFundRequestShowEntranceResult", "IIU"},
	{"BabyFundRequestBuyFundSuccess", ""},
	{"BabyFundRequestRecvPackageSuccess", "I"},

	-- 2018 圣诞
	{"SyncChristmasTreeProgressChanged", "IIIII"}, -- level, idx, newProgress, newSubmitTimes, rewardStatus
	{"SyncChristmasTreeLevelUp", "IIIU"}, -- oriTreeLevel, newLevel, newNpcEngineId, extraInfoUd
	{"SyncChristmasTreeInfo", "U"}, -- infoUd

	--christmas
	{"ShowChristmasCardEditWnd", "IsII"}, --sourceInfo.Id, sourceInfo.ItemId, sourceInfo.Place, sourceInfo.Pos
	{"ShowEditedChristCardListWnd", ""},
	{"SendCardToSender", "ds"}, --senderId, senderName
	{"ShowExchangeCardToYinPiaoCount", ""},
	{"RequestRecieveCardFromTreeSuccess", "Id"},  --sceneId, recieverId

	{"ReplyBabyUnLockExpressionGroupInfo", "IUI"}, --babyEngineId, unlockExpressionGroupData, showStatus
	{"UnLockBabyExpression", "I"}, --expressionId

	{"SyncChristmasTreeDropItem", "I"},-- npcEngineId
	{"QueryNeteaseMemberWelfareResult", "bbbI"},
	{"RequestGetNeteaseMemberLevelAwardSuccess", ""},
	{"RequestGetNeteaseMemberConsumeFirstLvAwardSuccess", "d"},
	{"RequestGetNeteaseMemberConsumeSecondLvAwardSuccess", "d"},
	{"BatchSyncGridForceInHongLvZuoZhanPlay", "IU"},-- force, gridUd
	{"SyncChristmasStromEffect", "bII"},-- bShow, posX, posY
	{"BatchSyncGridTwinkleInHongLvZuoZhanPlay", "IU"},-- force, gridUd
	{"SyncChristmasSantaKilledInGuild", ""},
	{"SyncChristmasSnowBallLeftTime", "II"}, -- engineId, endTime
	{"BatchSyncChristmasSnowBallLeftTime", "U"}, -- {engineId1, endTime1, ..}
	{"QueryNeteaseMemberInfoResult", "UUUUUU"},
	{"UpdateNeteaseMemberQualificationUsedInfo", "b"},
	{"UseNeteaseMemberQualificationResult", "b"},
	{"OpenSetBiaoCheWnd", ""},
	{"ShowSendShengDanGiftFx", "IU"}, -- srcEngineId, dstEngineIdUd
	{"ShowSendShengTreeLevelUpFx", "IIII"}, -- srcEngineId, oldLevel, dstEngineId, newLevel

	-- 2019 元旦
	{"NewYearGuildQuizOpenNotifyWnd", "dsI"},
	{"NewYearLuckyDrawSuccess", "sI"},
	{"NewYearChallengeSyncScoreInfo", "U"},

	{"SendMonsterSiegeFightData", "IUId"}, --dataType, dataUD, playerCount, totalValue
	{"SendMonsterSiegeCopyMemberInfo", "UI"}, --dataUD, playerCount
	{"AddBuffAllData","IIU"},
	{"TeamMemberAddBuffAllData","dIU"},


	-- 家园雪景
	{"SyncWinterFunitureAtHomeStatus","sII"},
	{"UpdateFurnitureWinterStyleInfo","sII"},
	{"SyncYarkPickStr","U"},

	{"SetJieBanBabyQiChangRes","bsI"}, -- bSuccess, babyId, qiChangId
	{"SyncAdmonishTimes","IsII"}, -- expireTime, babyId, admonishTimes, enhanceTimes
	{"SyncAdmonishRes","sbIIII"}, -- babyId, isAdd, chosenPropId, newChosenProp, targetPropId, newTargetProp
	{"SyncQueryBabyOwnerPlayerId","IId"}, -- npcEngineId, actionType, ownerId
	{"SyncQueryBabyIdByEngineIdRes","Isd"}, -- npcEngineId, babyId, currentOwnerId

	{"DeletePlayerAchievement","I"},
	{"SyncAdmonishBabyFail","I"}, -- failType

	{"SyncCombatVehicleMonsterHpInfo", "Iddd"}, --engineId, hp, currentHpFull, hpFull

	-- 设置雾浓度
	{"SetFogValue", "d"}, -- fogIntensity

	-- 通知客户端开始吹气
	{"NotifyPlayerStartBlow", "I"},

	-- 2019 NewYear
	{"StopFire2019Fireworks", ""},
	{"StartFire2019Fireworks", "I"},
	{"SyncPigCoinParams", "II"},
	{"Sync2019HonorCoinInfo", "III"},


	-- 2019元宵
	{"TangYuanPlaySyncPlayInfo", "IIU"},
	{"TangYuanPlayScoreAdd", "bI"},

	{"SynMengHuaLuBuffData", "IU"},
	{"CastMengHuaLukillBegin", "II"},
	{"SyncMengHuaLuGridInfo", "U"},
	{"EnterMengHuaLuNewRound", "I"},
	{"SyncMengHuaLuPlay", "U"},
	{"SyncMengHuaLuDropItemInfo", "U"},
	{"SyncMengHuaLuShopInfo", "U"},
	{"RequestMengHuaLuSetQiChangSuccess", "U"},
	{"MengHuaLuFailed", "IIU"},
	{"CastMengHuaLukillEnd", "IIUU"},
	{"SyncMengHuaLuCharacterGold", "II"},
	{"SyncMengHuaLuSkillNum", "II"},

	{"RequestLoadErSheArPlayDataResult", "sS"},

	{"SyncMengHuaLuCharacterFightProp", "IIi"},
	{"ShowCharacterHpChange", "Ii"},
	{"QueryMengHuaLuSkillResult", "U"},

	{"SendLuckyMoneyToBabySuccess", ""},
	{"QueryBabyLuckyMoneyHistoryDone", "sIIIU"},

	{"RequestLingShouScoreInfo_Result", "sUIsUI"},

	{"ReplyValentineDaySendFlowerRank", "IsU"}, --selfSendNum, selfGuildName, rankData
	{"QueryMengHuaLuQiChangResult", "U"},
	{"RequestStartMengHuaLuResult", "I"},
	{"SyncMengHuaLuCharacterDie", "II"},

	-- 2019 Valentine
	{"AutoTakePhoto", ""},
	{"ShowValentineQYSSPromiseAndSubmitWnd", "d"}, -- partnerPlayerId
	{"SyncValentineQYSJList", "U"},
	{"SyncValentineQYSJInfo", "U"},
	{"SyncValentineQSYYInfo", "U"},

	{"OpenActivityUrl", "s"}, -- activityName

	{"SyncStarBiwuViewVictory", ""},
	{"SyncStarBiwuAudienceBasic", "UUU"},
	{"SyncStarBiwuAudienceAction", "U"},
	{"SyncQueryStarBiwuOverview", "ddIU"},

	{"UpdateYayunBiaoCheMapPos", "U"},

	{"SculptureEquipSuccess", "U"},
	{"UpdateYaYunBiaoCheMonster", "I"},

	{"UpdateMessagePushStatus", "Ib"}, -- pushId, bopen
	{"SummonBackToOwnCity", "ds"}, -- senderId, senderName
	{"SummonTrackToGuildBiaoChe", "dsIs"}, -- senderId, senderName, mapId, mapName

	{"HideNpcByWeather", "Ib"},
	{"StarBiwuAudienceReleaseResult", "dIb"},
	{"StarBiwuAudiencePackResult", "dIb"},
	{"AbandonCitySuccess", "II"}, -- mapId, templateId

	{"QuickAddBabyTiliSuccess", "s"}, -- babyId
	{"SyncStarBiwuUpdateLocationRes", "db"}, -- bSuccess
	{"SyncStarBiwuUpdateActionRes", "db"}, -- bSuccess
	{"SyncStarBiwuViewVictoryRes", "b"}, -- bOpen
	{"SyncStarBiwuViewGossipRes", "b"}, -- bOpen

	{"ExchangeLingShouPartnerSkillSuccess","ss"},

	{"QueryCityMonsterFightParamResult", "IdI"}, -- monsterId, hpFull, reason
	{"StarBiwuShowNpcSayContent", "Is"}, -- engineId, content
	{"QueryMirrorWarWatchEnableResult", "b"}, -- bEnable

	{"RequestCanReferenceStoneOnEquipmentResult", "bsi"},
	{"RequestCanSwitchEquipHoleSetReturn", "IIsIb"},

	{"PlayerStartFloat", "I"}, -- engineId
	{"PlayerFinishFloat", "I"}, -- engineId

	{"SyncTianMenShanBattleLeftReliveCount", "I"}, --reliveCount
	{"SendTianMenShanBattleFightData", "IUId"}, --dataType, dataUD, playerCount, totalValue
	{"SendTianMenShanBattleCopyMemberInfo", "UI"}, --dataUD, playerCount
	{"SyncSmartGMToken", "bsiS"}, --bSuccess, token, code, message

	{"EquipSuitFxIndexUpdate", "I"}, -- fxIndex
	{"SyncEnterStarBiwuScene", ""}, --

	{"OpenTianQiShopWnd","U"}, ---

	{"ResponseDiskPlayerData", "IbbssIIIb"}, -- 返回音乐盒数据, furnitureId, isVoice, isOn, musicId/voiceUrl, name, expireTime, playType, playLoop, stopBGM
	{"SyncDiskPlayerPlayStatus", "IbbsIb"}, -- 同步音乐盒播放状态 furnitureId, isVoice, isOn, musicId/voiceUrl, playLoop, stopBGM

	{"SyncNpcFightingHp", "Id"},

	{"SyncStarBiwuChampion", "U"}, --
	{"SyncStarBiwuChampionEnd", ""}, --

	{"QueryAntiCheatCheckInfo", ""},


	{"EnterSummonMirrorWarMineVehicle", ""},
	{"SummonMirrorWarMineVehicleSuccess", "I"},
	{"UpdateMirrorWarForceMineAndVehicleInfo", "IIU"},
	{"OpenCreateOneLevelMajorCityGuide", ""},


	{"ShowOnceFlagEffect", "Iddd"}, -- templateId, x, y, z

	{"CakeCuttingSyncStatusToPlayersInPlay", "IIU"}, --playStatus, enterStatusTime, extraData
	{"CakeCuttingClearPath", "d"}, --playerId
	{"CakeCuttingSyncDiff", "U"}, --diff_U
	{"CakeCuttingPlayClearColor", "I"}, --colorIdx
	{"CakeCuttingPlaySyncMap", "Ud"}, --map_U, playEndTime
	{"CakeCuttingPathAddNode", "ddd"}, --playerId, x, y
	{"CakeCuttingSyncPlayerInfo", "dU"}, --playerId, info_U
	{"CakeCuttingCheckInMatchingResult", "b"}, --isInMatching
	{"CakeCuttingSignUpPlayResult", "b"}, --success
	{"CakeCuttingCancelSignUpResult", "b"}, --success
	{"CakeCuttingPlayKilledByPlayer", "ddIs"}, --playerId, killByPlayerId, continueKillNum, reason
	{"CakeCuttingPlayRestartMoving", ""}, --
	{"CakeCuttingPlaySpeedUpResult", "d"}, --CDTime
	{"CakeCuttingPlayEnd", "U"}, --
	{"CakeCuttingQueryRankResult", "UU"}, --

	{"SyncQingHongFlyProgress", "I"}, -- progress
	{"SyncQingHongFlySkillStatus", "b"}, -- on/off

	{"ZNQLotteryAddTicketResult", "bs"}, -- success, lotteryId
	{"ZNQLotterySetLotteryDataResult", "bsU"}, -- success, lotteryId, lotteryData_U
	{"ZNQLotteryQueryLotteryResult", "bUUdd"}, -- success, lotteryInfos_U, lotteryResult_U, sumOfReward, sumOfCanReward
	{"ZNQLotteryQueryPrizePoolResult", "U"}, -- prizePool_U

	{"CnvLotteryAddTicketResult", "bs"}, -- success, lotteryId
	{"CnvLotterySetLotteryDataResult", "bsU"}, -- success, lotteryId, lotteryData_U
	{"CnvLotteryQueryLotteryResult", "bUUdd"}, -- success, lotteryInfos_U, lotteryResult_U, sumOfReward, sumOfCanReward
	{"CnvLotteryQueryPrizePoolResult", "U"}, -- prizePool_U

	{"SyncSelectStarBiwuChampionNpc", "I"}, -- engineId

	{"RespDiskItemCreatorName", "ds"}, -- playerId, playerName

	{"SendXianZhiPossibleInfo", "IU"}, -- regionId, possibleInfo
	{"QueryXianZhiSignUpDone", "II"}, -- curPlayIndex, signUpPlayIndex
	{"SyncPlayerXianZhiData", "U"}, -- xianzhiData
	{"LevelUpXianZhiSuccess", ""},

	{"OpenEnterWaKuangPlayWnd", "I"}, -- leftRewardTimes
	{"SyncWaKuangTaskInfo", "IIIU"}, -- round, finished, total, data_U
	{"SyncWaKuangPackageInfo", "IIU"}, -- packageSize, gridCapacity, data_U
	{"AddKuangShiToPackage", "IiiiII"}, -- engineId, x, y, idx, itemId, count
	{"RemoveKuangShiFromPackage", "iI"}, -- idx, itemId
	{"ShowWaKuangPlayResult", "IU"}, -- time, data_U
	{"UpdateWaKuangUpgradePoints", "Ib"}, -- points, canUpgrade

	{"SyncNpcHaoGanDuInfo", "IIIIIIIUU"}, --npcId, haogandu, buyNum, zengsongNum, buyTime, zengsongTime, reason, letterFlagData, miscFlagData

	{"SyncTaskPropertyToPlayer", "UU"},
	{"ReplyLingShouBabyInfoForCamara", "sII"}, --babyId, babyEngineId, lingshouEngineId

	{"ReplyEquipInfoInAttrGroup", "siiIbIIIIUU"}, --itemId, wordSetIdx, holeSetIdx, gemGroupId, feiShengAdjust, adjustEquipGrade, adjustGemGroupId, adjustIntensifyLevel, adjustForgeLevel, adjustWordUD, equip
	{"UpdateAttrGroupUnlock", "I"}, -- value
	{"UpdateAttrGroupAt", "iIU"}, -- groupIdx, eItem, groupUD
	{"RequestCanSwitchAttrGroupReturn", "ib"}, -- groupIdx, canSwitch
	{"SwitchAttrGroupSuccess", "i"}, -- groupIdx

	{"AddBulletFxWithSpeed", "IIId"}, -- srcEngineId, dstEngineId, fxId, speed
	{"SyncWorldEvent2019MessageLieFengInfo", "IIbI"}, --serverMessageIdx, viewedMessageIdx, bShowUIEffect, liefengEffectId
	{"ReplyLieFengRepairProgressInfo", "IU"},

	{"TeamAppointmentPlayerQueryRewardResult", "UU"}, --retPlayerList_U, retRewardList_U
	{"TeamAppointmentPlayerGetRewardResult", "Ib"}, --rewardId, success
	{"TeamAppointmentCheckHasRewardResult", "b"}, --hasReward

	{"PlayNormalSceneQTE", "Idb"}, -- qteId, duration

	-- 角色手绘卡牌集换玩法
	{"SyncRdcPropSlotValue", "II"}, -- slotIdx, newValue
	{"SyncRdcPropResetExpire", "I"}, -- newExpireTime
	{"SyncRdcPropShareTime", "I"}, -- shareTime
	{"SyncRdcPropAllSetReward", "I"}, -- newFlag

	{"CreateFakeBullet", "Iddddd"}, -- BulletTemplateId, SourceX, SourceY, DestX, DestY,speed

	{"ZuoQiPlayExtraAnimation", "Is"}, -- engineId, AniName

	{"SyncLeaderQueryStarBiwuAudience", "U"}, -- audienceUd
	{"SyncQueryStarBiwuTeamInfo", "U"}, -- teamUd
	{"SyncWakeupBedSuccess", "iiiib"}, -- WakeupBedRoom WakeupBedTemplateId WakeupBedX WakeupBedY status
	{"SyncLeaderRmStarBiwuAudicenDone", "ds"}, -- playerId, targetName
	{"ShowInviteToStarBiwuCombatTeamConfirm", "ds"}, -- leaderId, leaderName

	{"QueryInvitedPlayerIdsDone", "U"},
	{"InviteWatchFengXianSuccess", ""},
	{"QueryFengXianPlayerAppearanceDone", "dU"}, -- inviterId, appearanceData

	{"PlayAddLinkFx", "IIII"}, -- engineId, engineId2, fxId, duration
	{"SendXianZhiPuInfo", "UUUI"}, -- sanJiePlayerInfoTblUD, cityPlayerInfoTblUD, districtPlayerInfoTblUD, operateTimes

	{"SyncOpenHouseXuanFuEdit","I"},
	{"QueryRemainXianZhiTimeDone", "d"},
	{"UpdateTianFuSkillTemplate", "IU"}, -- templateIdx, tianfuSkills

	{"ExtendXianZhiSuccess", "d"},
	{"AllocateXianZhiSuccess", ""},

	{"CreateStarBiwuZhanDuiSuccess", ""},
	{"ReplyStarBiwuSelfPersonalInfo", "dssiiiisdI"}, --fromCharacterId, fromServerName, fromCharacterName, goodAtClass, locationId, onlineTimeId, canZhiHui, mark, zhanduiId, loginCount
	{"ReplyStarBiwuZhanDuiList", "U"}, --selfZhanDuiId, page, totalPage, dataUD, source
	{"ReplyStarBiwuZhanDuiInfo", "ddUUssIIIsI"}, --selfZhanDuiId, zhanduiId, memberData, chuzhanData, title, kouhao, needClass, needZhiHui, isApply, tips, hideFlag
	{"RequestJoinStarBiwuZhanDuiSuccess", "d"}, --zhanduiId
	{"PlayerRequestJoinStarBiwuZhanDui", ""},
	{"ReplyStarBiwuZhanDuiApplyList", "IU"}, --totalPage, dataUD
	{"ReplyStarBiwuQieCuoTeamList", "dIIU"}, --selfQieCuoId, page, totalPage, dataUD
	{"SignUpStarBiwuQieCuoSuccess", "d"}, --selfQieCuoId
	{"ReplyStarBiwuQieCuoTeam", "ddU"}, --selfQieCuoId, memberId, dataUD
	{"TriggerClientBinaryCheatCheck", ""},
	{"SyncClientBinaryIndentity", "U"}, --dataUD

	{"SyncLiangHaoInfo", "III"}, --reason, lianghaoId, expiredTime
	{"ReplyLiangHaoRenewInfo", "dIIIII"}, --targetId, lianghaoId, renewPrice, oldExpiredTime, newExpiredTime, addDay
	{"ReplyLiangHaoSellInfo", "UUIsIU"}, --buyData, jingpaiData, jingpaiEndTime, tp, nextStartTime, dingzhiData
	{"ReplyLiangHaoJingPaiInfo", "IdssIIIII"}, --lianghaoId, roleId, roleName, serverName, initPrice, currentPrice, myPrice, minPrice, maxPrice, priceStep
	{"ReplyMyLiangHaoJingPaiInfo", "U"}, --data
	{"SetLiangHaoPrivilegeSuccess", "II"}, --tp, value

	{"SendPengPengCheSignUpInfo", "bbI"}, -- isSignUp isClientRequest, todayRemainRewardTimes
	{"SyncPengPengCheBattleInfo", "U"}, -- battleInfo
	{"SyncPengPengCheBattleScore", "III"}, -- force1Score, force2Score, selfForce
	{"ShowPengPengCheResultWnd", "III"}, -- result, playScore, addedLiuYiScore
	{"UpdatePengPengCheSkillItemSlots", "UiiIIii"}, -- idlist, 1 add or -1 del or 0, pos, skillId, engineId, x, y

	{"PlaySceneObjectHide", "Is"}, -- sceneTemplateId, objectPath
	{"PlaySceneObjectShow", "Is"}, -- sceneTemplateId, objectPath

	{"UpdateDuanWuZongZiMonsterHp", "U"}, -- monsterHpInfo
	{"DuanWuZongZiPlayEnd", "I"}, -- isSuccess
	{"StarBiwuZhanDuiBeInvite", "dss"}, -- zhanduiId, zhanduiId, inviterName, zhanduiName
	{"OpenStarBiwuChuZhanInfoWnd", "dsUUI"}, --zhanduiId, zhanduiName, msgpack.pack(memberInfo), msgpack.pack(chuzhanInfo)
	{"StarBiwuSetChuZhanInfoSuccess", ""},
	{"UpdateStarBiwuZhanDuiTitleAndKouHaoSuccess", "ss"}, -- title, kouhao

	{"ZhuoXiaoGuiBegin", ""},
	{"ZhuoXiaoGuiEnd", ""},
	{"SelectXiaoGuiSuccess", "IiiIii"},

	{"ReplyStarBiWuZhanKuang", "ssIIIIUIsIIII"}, --attackName, defendName, attackWinCount, defendWinCount, finishRound, playRound, dataUD, selfForce, aliasName, stage, group, matchIdx, jieshu
	{"ReplyStarBiwuDetailFightData", "IUUIss"}, --round, attackData, defendData, playStage, attackName, defendName
	{"ReplyStarBiwuAllMemberFightData", "IIUI"}, --playStage, playIndex, fightData, isWin
	{"StarBiWuPlayEnd", ""},
	{"SyncStarBiwuFightPlayData", "I"}, --playStage
	{"StarBiwuExcellentDataShare", "IId"}, -- playStage, tp, value
	{"SyncRdcPropAllSlot", "U"}, -- allSlotData
	{"SyncRdcDataUpdate", ""}, --

	{"RequestOpenDrawHanZi", "II"},-- taskId, hanZiId

	{"SyncRdcBatchAddCard", "U"}, -- cardIdxUd

	{"SyncObjectYScale", "Id"}, -- engineId, y scale
	{"OnPlayerAddPengPengCheScore", "I"}, -- score
	{"SendJingLingKeyMsg","SIb"},
	{"QueryShenBingRestoreReturnResult", "IIUII"},

	{"PlayerStartChengZhong", "II"},
	{"PlayerEndChengZhong", ""},
	{"UseHuiLiuItemWithChoiceSuccess", ""},

	{"LoginZhouBianMallShopSuccess", "s"},

	-- 跨服明星赛新版比武流程
	{"ReplyStarBiwuJiFenSaiRank", "UdIsUII"}, --rankData, zhanduiId, rank, title, memberData, JiFen, avgWinTime
	{"ReplyStarBiwuJiFenSaiWatchList", "dU"}, --dataUD
	{"ReplyStarBiwuXiaoZuSaiSaiOverView", "IIIIIUUUUU"}, --queryType, group, round, xiaozusaiStage, queryGroup, group1Ud, group2Ud, groud3Ud, group4Ud, extraUd
	{"ReplyStarBiwuZongJueSaiOverView", "IdsUI"}, --index, zhanduiId, title, memberData, isFinish
	{"ReplyStarBiwuZongJueSaiWatchList", "IdsdsdI"} ,--playIndex, zhanduiId1, zhanduiTitle1, zhanduiId2, zhanduiTitle2, winZhanDuiId, start
	

	-- unintermitten hell
	{"SyncHellJieTuoGuoInfo", "IIIUU"},
	{"EnterHellLevel", "I"},

	{"MoveSceneObject", "sdddd"}, -- objectPath, x, y, z, duration

	{"ReplyAllSceneryPhotoAlbumUrl", "U"},

	{"ReplyMecicineBoxInfo", "sUII"}, --lastTemplateIdStr, itemDataUD, isAutoCollect, lastOneKeyUseTime
	{"SendSmartGMTokenAndUrlWitchContext", "bsiSss"}, -- bSuccess, token, code, message, question, context

	{"SyncDayAndNightChangeInfo", "IIIddI"}, --templateId, startTime, stopTime, currentDegree, finalDegree, updateNow
	
	{"HideMainPlayer", "b"},

	{"ReplyLiangHaoSurplusYuanBaoValue", "IsIII"}, --lianghaoId, context, buyPrice, expiredTime, retYuanBao
	{"OpenSceneItemInteractWnd", "IIs"},
	{"PlayItemInteractFx", "II"},
	{"EndCameraMoveImmediately", ""},
	{"StartUITrigger", "I"}, -- triggerId

	{"ReplyQmpkHandBookInfo", "IUUI"}, --jifen, dataUD1, dataUD2, isUnLock

	{"ReplyMGGJDynamicActivityInfo", "IIIU"}, -- activityType, currentCount, maxCount, ud

	-- zhongqiu2019
	{"SyncZhongQiuNpcFriendlinessInfo", "UI"}, -- npcData, currentNpcId
	{"SyncYueShenTempleInfo", "IIIUU"},
	{"EnterYueShenLevel", "I"},

	{"SendCommunityGhostData", "IUU"},

	{"OpenLiangHaoDingZhiWnd", "sIIIII"}, --itemId, place, pos, digitNum, from, to
	{"ReplyCalcLiangHaoPriceResult", "IIs"}, --lianghaoId, price, context
	{"ReplyLiangHaoDingZhiInfo", "IIIdssIIIII"}, --digitNum, toufangNum, playerNum, roleId, roleName, serverName, currentPrice, myPrice, minPrice, maxPrice, priceStep
	{"ReplyMyLiangHaoDingZhiApplyInfo", "U"}, --dataUD
	{"QueryPlayerShopSearchZhuangShiWuByColorResult", "IIIIIUI"},
	{"BuyDingZhiLiangHaoSuccess", "II"}, --lianghaoId, price

	{"SyncSpeakSettingStatus", "dd"}, -- forbidType, expiredTime
	{"CheckPlayerName",""},
	{"SyncCarnivalExpireTimeChange","db"}, -- expireTime, bResetData
	{"OpenHaoGanDuTaskQiuQianWnd", "Is"}, --taskId, eventName
	{"UpdateDynamicAreaFogDatas", "U"},

	{"SendChuanjiabaoWashResult", "sIU"},

	{"QXJTSendData", "Ibb"}, -- nUploadTimes, bRegistered, bIsOpen
	{"QXJTCheckRewardResult", "b"}, -- bCanReward

	{"SyncNpcHaoGanDuSpecialGiftInfo", "IIU"}, --npcId, generatedTimne, data

	{"SendPlayerName", "ds"}, --playerId,playerName

	{"TryOpenQiXiLiuXingYu3D", ""},
	{"TryTrackOpenQiXiLiuXingYu3D", "sIII"},
	{"StartQiXiLiuXingYu", "I"},
	{"StopQiXiLiuXingYu", ""},
	{"UseNeteaseMemberRightSuccsss", "I"},
	{"RequestNeteaseMemberHolidayGiftSuccess", "I"},

	{"BroadcastGuildMessageTopHighline", "dS"},

	{"PlayerGetPickInHell", "I"},
	{"SyncFuliWndStatus", "bU"},

	{"ZhongQiuNpcTaskResult", "I"}, -- npcTemplateId
	{"PlayerGetPickInYueShenTemple", "II"}, -- pickEngineId, progress

	{"SendDanMuRecordBegin", "s"},
	{"SendDanMuRecord", "sU"},
	{"SendDanMuRecordEnd", "s"},

	{"ClearLianShenSkillData", ""},
	{"SummonToMirrorWarPlay", "dsI"},

	{"PlayMultiPlayerVideo","sU"}, -- video name, other player info list: class/gender/appearance
	{"NationalDayTXZRSignUpPlayResult", "b"},
	{"NationalDayTXZRCancelSignUpResult", "b"},
	{"NationalDayTXZRCheckInMatchingResult", "b"},
	{"NationalDayTXZRSyncPlayerInfo", "dU"},
	{"NationalDayTXZRSyncStatusToPlayersInPlay", "IdIU"},
	{"NationalDayTXZRPlayAoeFx", "I"},	
	{"NationalDayTXZRPlayEnd", "U"},

	{"NationalDayJCYWSyncStatusToPlayersInPlay", "IdU"},
	{"NationalDayJCYWPlayEnd", "UUdd"},

	{"UpdateStarBiwuZhanDuiPublishSuccess", "I"}, -- hideFlag

	{"SendGuildDiningTableData", "IdibiUU"}, -- id, expireTime, refreshTimesDay, bFull, left, gridUD, topUD
	{"FillGuildDiningTableGridSuccess", "IiIiiibi"}, -- id, gridIdx, templateId, bindedCount, unbindCount, filledCount, bFull, eatTimes
	{"DiningOnGuildDiningTableSuccess", "i"}, -- eatTimes

	{"TargetAgreeToGroupPhoto", "dIb"}, --targetId, expressionId, isUnLockXiaoYaoExpression

	{"RotateFurniture", "IIII"}, --targetId, expressionId
	

	{"CheckLinkResult", "ssUUb"},
	{"LoadScenePhotoFilter", "bsd"},
	{"ShowSceneObjectInCopyMap", "bIII"},

	-- bind repo
	{"UpdateAllBindRepo", "U"},
	{"RequestOpenBindRepoWndResult", "III", nil, nil, "lua"},

	-- StarBiwuJingCai
	{"StarBiwuSendCurrentJingCaiItem", "UbssddIddUUUIiUd"}, -- jingcaiItemTblUd, canBet, targetDate, targetStageName, maxBetValue, currentBetPool, jingcaiPhaseNum, phaseBet, canGetReward, matchTblUd, choice2BetValueUd, singleChoiceRateUd, shutdownStatus, rightChoice, showResultUd, jingcaiquanCount
	{"StarBiwuSendJingCaiRecord", "UIdd"}, -- jingcaiRecordForClient, eachDataLen, canGetReward, jingcaiquanCount
	{"StarBiwuSendJingCaiRecordDetail", "UUUIsUdUU"}, -- jingcaiItemTblUd, selfChoiceTblUd, rightChoiceTblUd, jingcaiStage, targetDate, matchTblUd, phaseBetPool, choice2BetValueUd, showResultUd
	{"StarBiwuJingCaiBetResult", "bd"}, -- bSuccess, phaseBet
	{"StarBiwuJingCaiGetRewardResult", "bd"}, -- bSuccess, leftReward

	{"OpenUrlWithoutBuildIn", "s"}, -- url

	--unintermittent hell
	{"SyncHellTreasureBoxPos", "U"}, -- boxInfo
	{"SyncPaoShangBiaoChePosInfo", "III"},

	{"ReceiveQiChangActiveRewardSuccess", "s"},

	-- familytreetag
	{"SendPlayerFamilyTreeTags", "dsUsU"}, -- playerId, param, tagInfoUD, selfTagId, favorListUD
	{"SendPlayerFamilyTreeTagDetail", "dssdsIUb"}, -- playerId, tagId, tagName, writerId, writerName, favorNum, favorPlayerListUD, favored
	{"OpenAddPlayerFamilyTreeTagWnd", "d"}, -- playerId
	{"TagPlayerFamilyTreeSuccess", "dsssIII"}, -- playerId, tagName, tagId, opTagId, opType, favorNum, createTime
	{"FavorPlayerFamilyTreeTagSuccess", "dsbI"}, -- playerId, tagId, bFavor, favorNum

	-- halloween2019
	{"SyncDyePreGenMonster", "ddII"}, -- posX, posY, duration, fxType
	{"SyncUseTaskItemTip", "I"}, -- templateId
	{"SyncDestroyUseTaskItemTip", ""},
	{"SyncBugHuntProgress", "II"}, -- catchedCount, leftCount
	{"SyncHalloweenTaskProgress", "UI"}, -- dataUd, eachDataLen
	{"SyncDyeSceneOverView", "UI"}, -- dataUd, score
	{"SyncDyePlayScore", "I"}, -- score
	{"SyncDyePlayBossChangeColor", "d"}, -- endTime
	{"SyncDyePlayBossRefreshStatus", "d"}, -- hpThreshold
	{"SyncDyePlaySummary", "UUUUb"}, -- rankUd, statUd, rewardUd, extraUd, bGetReward
	{"SyncHalloweenCardOrder", "I"}, -- cardOrderValue
	{"SyncHalloweenCollectProgress", "III"}, -- mailId, newCount, status
	{"SyncBugHuntThunder", "U"}, -- catchedMonster
	{"SendRanSeJiBossStatus", "dd"}, -- bossHp, BossHpFull

	{"MapEditorOpenSearchNpcWnd", ""},
	{"MapEditorExportNpcDataResult", "S"},
	{"MapEditorQueryRecordResult", "U"},
	{"MapEditorCloseLoadWnd", ""},

	{"SyncBaoTuanZuoZhanApplyIcon", "b"},
	{"QueryBaoTuanZuoZhanApplyInfoResult", "bI"},
	{"ApplyBaoTuanZuoZhanSuccess", ""},
	{"CancelApplyBaoTuanZuoZhanSuccess", ""},
	{"ShowBaoTuanZuoZhanEndVideo", "UI"},
	{"ShowBaoTuanZuoZhanInfoWnd", ""},

	{"SendSinglesDayLotteryJoinInfo", "UI"}, -- joinInfo, joinedCount
	{"SendSinglesDayLotteryResult", "IUU"}, -- dayBegin, result, dateList
	{"SyncSinglesDayRMBPackageStatus", "bII"}, -- isActive, remain second, itemId

	{"SendAuthenticationQuestion", "IIIU"},
	{"SendAuthenticationCorrectChoice", "I"},
	{"FinishAuthentication", "II"},

	{"SyncManualShangJiaItem", "U"}, --regionid itemId expiretime isshow
	{"SyncQiLinDongItemCount", "U"},
	{"SyncQiLinDongHp", "i"},
	{"OpenQiLinDongPlayShop", "UIUI"},
	{"ShowCostJadeEnterQiLinDongWnd", "I"},
	{"ShowTempSkillReplaceWnd", "UI"},
	{"ShowQiLinDongResult", "II"},
	{"DestroyPlayCountWnd", ""},
	
	{"QuerySanXingBattelePlayInfoResult", "iIUU"},
	{"ShowSanXingBattleGateInteractiveMenu", "II"},

	{"SyncYingLingState", "IId"},
	{"UpdateExtraYingLingActiveSkills", "IU"},
	{"ShowYingLingTransformPannel", ""},
	{"YingLingAISkillConflict", "I"},

	{"PlaySound", "s"},
	{"PlaySoundWithDuration", "sdb"}, --eventName, duration, stopBGM
	{"SyncBaoTuanZuoZhanCurRound", "II"},
	
	{"ShowBatchMonsterDropItem", "IIIU"},
	{"PickBatchItemFailed", "s"},
	{"AutoPickBatchItemSuccess", "s"},
	{"SendDeclareMirrorWarInfo", "dsIIII"},
	{"SyncQiLinDongBossState", "bII"},

	{"SyncRescueChristmasTreeGrowthValue", "I"},

	{"SendCustomRpc", "sU"}, -- type, content
	{"TryStopTrack", ""},

	{"SyncShiFuScore", "IIII"}, -- oldScore, newScore, oldLevel, newLevel
	{"SendShiFuScoreLevel", "IIII"}, -- score, level, maxLevel, monthRewardedLevel

	{"BatchItemAutoDestroy", "s"}, -- itemId

	{"OpenChristmasGiftBattleSignUpWnd", "bII"}, --bSignUp, restAwardNum, choice
	{"ReplyChristmasGiftBattlePlayInfo", "Ub"}, --dataUD, bFinish

	{"SendMirrorWarPriorityMatchGroup", "dsIIUbbI"}, -- rivalGuildId, rivalGuildName, rivalMapId, rivalTemplateId, dataUD, inPriorityMatchGroup, isPriorityMatchPeriod, priorityMatchLeftTime
	{"QueryTerritoryOccupyAwardListResult", "IU"}, -- startTime, dataUD
	{"QueryTerritoryDayOccupyAwardResult", "iiIIIIIU"}, -- temp, dayIndex, mapId, templateId, index, totalMingWang, totalExp, hourAwardsUD
	{"QueryTerritoryWeekOccupyAwardResult", "iiIIIIIII"}, -- temp, weekIndex, mapId, templateId, index, evaluation, yinpiao, banggong, mingwang
	{"RequestTerritoryOccupyAwardSuccess", "Ii"}, -- awardType, awardIndex

	{"SyncSmartGMTokenAndHost", "ssdS"}, -- host, token, expire, refer

	{"EnterOrLeaveGameVideoScene", "b"}, -- bEnter
	{"StartSanXingBattleGuide", "I"}, --force
	{"JoinSanXingBattFreeMatch", "Ii"},
	{"QuitSanXingBattleFreeMatch", ""},
	{"SyncChristmasGiftBattleGiftNum", "U"},
	{"OpenSanXingBattleFreeMatchWnd", "bIIi"},
	{"RequestUseChuJiaMuYuSuccess", "I"},

	{"SyncSchoolAchievementStatus", "II"},
	{"SyncSchoolContribution", "II"},
	{"SyncSchoolAchievementCounter", "Id"},

	{"SyncSanXingBattleCurrentStage", "II"}, --stage, force	
	{"RequestConfirmChangePlayerName", "s"},
	{"SyncSanXingBattleJinShaScore", "II"}, --currentScore, TotalScore

	{"OneKeyBuyMallItemResult", "b"}, --result
	{"SyncSanXingFlagPosition", "U"}, --data
	{"SyncSanXingBossPosition", "IIII"}, --force, templateId, x, y

	-- 元旦帮会投票
	{"SendYuanDanGuildVoteInfo", "bU"}, -- canCreateVote, infoUD{questionId, questionContent, title, playerId, playerName, isFinish, isVoted}
	{"SendYuanDanGuildVoteQuestion", "ISsbdbdU"}, --questionId, questionContent, title, isVoted, votePlayerId, isFinish, winPlayerId, playerUD{playerId,playerName,playerVoteCount}

	{"OpenSanXingBattleReliveWnd", "II"}, --force, restTime
	{"CloseSanXingBattleReliveWnd", ""},
	{"SetSanXingDefaultRelivePosSuccess", "II"}, --force, index
	{"ChristmasGiftBattleGetGift", "I"}, --num
	{"RefuseJoinChristmasGiftBattle", ""},
	{"ItemUseCheckBagSpaceFail","ss"}, -- itemId,context
	{"PlayerEnterSanXingBattleCopy", "I"}, --force

	{"SendGuildMemberIdAndName", "U"}, -- playerId and Name userdata
	{"SendYuanDanGuildVoteCreateResult", "ISsdsbb"}, -- questionId, questionContent, title, playerId, playerName, isFinish, isVoted
	{"ShowSanXingBattlePlayFx", "s"}, --fxPath

	-- 2020寒假活动
	{"ReplyWorkManualOverview", "UIIIIUU"}, -- subTask_U, subTaskCount, finishedTaskCount, winePoint, leftOpenBottleTimes, ownManual_U, manualReward_U
	{"ReplyGetManualRewardResult", "bI"}, -- bSuccess, level
	{"ReplyBuyHanJiaManualResult", "bIU"}, -- bSuccess, manualId, ownManual_U
	{"ReplyOpenBottleResult", "bIU"}, -- bSuccess, leftOpenBottleTimes, reward_U
	{"ReplyGetHanjiaSubTaskReward", "bIIIII"}, -- bSuccess, subTaskId, status, winePoint, rewardedTaskCount, leftOpenBottleTimes
	{"SyncSubmitSnowballResult", "bIIII"}, -- bSuccess, furnitureId, currentCount, maxCount, currentStage
	{"SyncDecorateSnowmanResult", "bII"}, -- bSuccess, furnitureId, newTemplateId

	{"SendYuanDanGuildDoVoteSucc", "I"}, -- questionId

	{"SendSanXingBattleAwardInfo", "IIIII"}, --score, skillId, itemId, rank, awardNum

	{"SyncPlayerTitleInPlay", "dss"}, -- playerId, titleName, colorStr
	{"ClearPlayerTitleInPlay", ""},
	{"SyncTieChunLianPlayScore", "i"}, -- score
	{"SyncTieChunLianPlayResult", "i"}, -- score
	{"StartTrackToDynamicNpc", "IIiii"}, -- npcTemplateId, sceneTemplateId, x, y, z
	{"EnterEnjoySceneryMode", "UU"}, -- rzy, cameraLocalPositionTable

	{"SyncGameVideoTargetPos", "II"},
	{"OpenMenPaiHuTongSkillReplaceWnd", "IIU"},

	{"ShowGuideRegion","IsdU"},
	{"FinishNewbieSchoolWish", ""},

	{"NotifyPlayerChooseChristmasPickOnEnterHouse", ""}, -- 

	{"SyncQingYiXiangTongSignUpStatus", "Ib"},

	{"SyncSingletonShopOnshelfStatus", "U"}, -- onShelfUd
	{"SyncSingletonShopBuyResult", "bU"}, -- bSuccess, onShelfUd

	{"NotifyPromoCodeCheckOrder", "sssssssssIssSS"}, -- ip, pid, ch, pf, app_channel, udid, devname, goodsid, goodsinfo, goodscount, currency, sn, receiptData, receiptSign
	{"ShowPromoCodeCheckOrderWnd", "sss"}, -- pid, goodsinfo, sn

	{"SyncHuiGuiJieBanData", "U"}, -- huiGuiJieBanDataUD
	{"SendLiuShiFriends", "U"},	-- friendIdTblUD
	{"InviteFriendSuccess", "d"},
	{"SendPlayerInviters", "U"}, -- inviterIdTblUD
	{"NotifyInviterSuccess", "d"},
	{"RemoveJieBanPlayerSuccess", ""},

	{"SyncUseDecoratedSnowmanAction", "I"}, -- furnitureId

	{"SendChunJie2020SeriesTasksRewardStatus", "bb"}, -- canGetReward, rewardGot
	{"GetChunJie2020SeriesTasksRewardSuccess", ""},

	{"SendSchoolTaskOpenStatus", "b"}, -- openStatus

	{"ReplyMenPaiHuTongSkillInfo", "IU"},
	{"ReplySanXingBattleFreeMatchPlayerNum", "II"}, --totalNum, copyPlayerNum

	{"SyncHouseFurnitureColor", "IU"}, 

	{"OpenHuiGuiJieBanWnd", ""},


	-- 跨服斗地主大赛
	{"CrossDouDiZhuQueryHaiXuanRankResult", "UdU"},
	{"CrossDouDiZhuQueryJueSaiVoteListResult", "IIU"},
	{"CrossDouDiZhuRequestShowPlayInfoResult", "Udb"},

	{"SetPlayerInterestedItemsSuccess", "u"},

	-- -- 元宵放灯
	{"YuanXiaoLanternStart", "I"},
	{"YuanXiaoLanternStop", ""},
	{"YuanXiaoLanternTryOpen3D", ""},
	{"YuanXiaoLanternTryTrackTo3D", "sddd"},

	-- 元宵怼怼乐
	{"DuiDuiLeSyncPlayInfo", "dU"},
	{"DuiDuiLePlayResult", "dddU"},
	{"DuiDuiLeOpenSignUpWndResult", "dd"},

	{"ReplyPlayerXiaoYaoExpressionUnLockInfo", "db"}, 
	{"CrossDouDiZhuOpenSignUpWndResult", "U"},
	{"CrossDouDiZhuWashOutNotify", "bdd"},
	{"CrossDouDiZhuJieSuanNotify", "Idd"},
	{"CreateLuaFlag", "IIIIIII"},
	{"DestroyLuaFlag", "I"},

	{"SendQYXTBattleInfo", "IIU"}, -- stage, isEnd, battleInfoTbl
	{"SendQYXTProgressInfo", "IU"}, -- stage, progressInfo
	{"AddQYXTSubmitFlowerFx", "II"}, -- srcEngineId, dstEngineId

	{"RemoveHouseFurnitureColor", "I"}, -- fid


	{"QueryZhuBoListResultUdBegin", ""},
	{"QueryZhuBoListResultUd", "IU"},
	{"QueryZhuBoListResultUdEnd", ""},

	{"SyncJueJiStatus", "dbddbddU"}, -- memberId, hasSkill70, remainTimeSkill70, durationSkill70, hasSkill100, remainTimeSkill100, durationSkill100, extraData

	{"RepairAllBodyEquipConfirm", "I"},

	{"ReplyStarBiwuZongJueSaiMatch", "UIdId"}, -- matchDataUd, currentMatchPlayIdx, selfZhanduiId, syncType, canGetReward
	{"SyncCheckStarBiwuInJingCaiResult", "b"}, -- bInJingCai
	{"StarBiwuSendJuesaiJingCaiItem", "UbssddIddU"}, -- jingcaiItemTblUd, canBet, targetDate, targetStageName, maxBetValue, currentBetPool, jingcaiPhaseNum, phaseBet, canGetReward, matchTblUd

	{"AddSceneSyncGroupPlayerId", "sd"},
	{"DelSceneSyncGroupPlayerId", "sd"},
	{"AddObjSyncGroupPlayerId", "dd"},
	{"DelObjSyncGroupPlayerId", "dd"},


	{"QingMing2020PvpSignUpPlayResult", "b"}, --success
	{"QingMing2020PvpCheckInMatchingResult", "b"}, --isInMatching
	{"QingMing2020PvpCancelSignUpResult", "b"}, --success
	{"QingMing2020PvpQueryRankResult", "UU"}, --
	{"QingMing2020PvpSyncStatusToPlayersInPlay", "IIIU"}, --playStatus, enterStatusTime, PlayEndTime, extraData
	{"QingMing2020PvpQueryPlayInfoResult", "U"}, --

	{"QingMing2020PveSyncStatusToPlayersInPlay", "IIIIIU"}, --playStatus, enterStatusTime, roundStatus, roundStatusTime, PlayEndTime, extraData

	{"SyncRanFaJiTaskProgress", "I"}, --progress
	{"OpenRanFaJiColorSystemChooseWnd", "sII"}, --itemId, place, pos
	{"SyncRanFaJiPlayData", "UUIII"}, --peifangData, ranfajiData, takeonId, newPeiFangId, openType

	{"SendXiangShiQuestionChoiceInfo", "IU"},
	{"QueryPuJiaCunQiuQianInfoResult", "Id"},
	{"PushPuJiaCunQiuQianSuccess", "Id"},

	{"QueryAutoBaptizeTargetWordCondListResult", "IIsU"}, -- place, pos, equipId, idListUD

	{"QueryTempPlayDataInUDResult", "IUb"}, --key, ud, isNilUD

	{"ClearMailClientCache", "I"}, -- mailTemplateId
	{"QueryNeteaseMemberGouKaInfoResult", "IS"},

	-- 远程斗地主
	{"ApplyJoinRemoteDouDiZhuPlayerListAlert", ""},
	{"QueryApplyJoinRemoteDouDiZhuPlayerListResult", "U"}, -- listUD
	{"ClearApplyJoinRemoteDouDiZhuPlayerListSuccess", ""},
	{"ApprovePlayerJoinRemoteDouDiZhuSuccess", "d"}, -- applyId
	{"DisapprovePlayerJoinRemoteDouDiZhuSuccess", "d"}, -- applyId
	{"InviteRemoteDouDiZhu", "sIIdsI"}, -- containerId, gasId, serverId, inviterId, inviterName, index
	{"InviteRemoteDouDiZhuSuccess", "sIIdI"}, -- containerId, gasId, serverId, inviteeId, index
	{"QueryInvitedRemoteDouDiZhuPlayerListResult", "U"}, -- listUD

	-- 2020 六一
	{"DanZhuQuerySignUpStatusResult", "bII"},
	{"DanZhuSelectDanZhuType", "I"},
	{"DanZhuVoteEarlyEndConfirm", "IIU"},
	{"DanZhuSyncPlayInfo", "U"},
	{"DanZhuPlayResult", "IIIIib"},

	{"SyncShiTuCultivateInfo", "bdbU"}, -- isShiFu, otherPlayerId, isRuShi, cultivateInfo
	{"SyncShiTuWeekTaskInfo", "bdU"}, -- isShiFu, otherPlayerId, weekTaskInfo
	{"SyncShiTuDailyTaskInfo", "bdU"}, -- isShiFu, otherPlayerId, dailyTaskInfo


	-- 木桩
	{"SyncHouseWoodPileFightStatus", "U"}, -- fightTuples
	{"SyncPileFightProgressInfo", "IU"}, -- pileId,damageInfo
	{"AttachWoodPileToMonster", "III"}, -- monsterEngineId, pileId,templateId
	{"SendWoodPileFightProp", "IU"}, -- monsterEngineId, fightProp

	{"DiYuShiKongNoYiShiAlert", ""},
	{"AskDiYuShiKongAddBloodToFaQi", ""},
	{"SyncDiYuShiKongCopyInfo", "ddddI"}, --rongxinHp, rongxinHpFull, faqiHp, faqiHpFull, escapedXiaoGuiCount

	-- 2020 五一全民货运
	{"QueryQuanMinHuoYunInfoResult", "IbiUIIIIIIbbb"}, -- submitClass, bAssist, day, listUD, rank, rankTotal, submitNum, totalGetNum, totalFillNum, totalSubmitNum, bCanGetFullAward, bFullAwardGot, bAllProgressFull
	{"SubmitTongXinJingChipSuccess", "iiIIIII"}, -- day, place, class, count, progress, submitNum, totalSubmitNum
	{"SetQuanMinHuoYunAssistClassSuccess", "iiI"}, -- day, place, class
	{"SendQuanMinHuoYunFullAwardSuccess", "i"}, -- day
	{"QuanMinHuoYunHeChengYaoFangSuccess", "s"}, -- yaoFangItemId
	{"SyncFengHuoLingPlayValue", "IddI"}, -- class, value, maxValue, canFillNum
	{"SyncFengHuoLingPlayValueFull", ""},
	{"ShowFengHuoLingReliveWnd", "id"}, -- dieNum, costValue
	{"CloseFengHuoLingReliveWnd", ""},
	{"ShowFengHuoLingPlayResult", "IddI"}, -- class, value, maxValue, fillNum

	{"ApplyChiJiSuccess", ""},
	{"CancelApplyChiJiSuccess", ""},
	{"SetChiJiBagItemId", "Id"},
	{"SendChiJibag", "U"},
	{"SendChiJiItemExtraInfoToGac", "IUU"},
	{"SyncChiJiSceneItemInfo", "U"},
	{"ChiJiSceneAddItem", "Id"},
	{"ChiJiSceneDelItem", "I"},
	{"SyncChiJiRoundAreaInfo", "IIIIII"},
	{"QueryChiJiApplyInfoResult", "b"},
	{"SyncChiJiWarningAreaInfo", "IIIIII"},

	{"SavePictureInfoSuccess", "III"},
	{"SendPictureInfo", "IIIU"},
	{"DetachChiJiWatch", ""},
	{"AtachChiJiWatch", "I"},
	{"SyncChiJiFightProp", "IId"},
	{"UpdateTeamMemberChiJiHp", "dII"},
	{"UpdateChiJiDropBagInfo", "IU"},
	{"SendChiJiResult", "IIU"},
	{"SetActiveTempSkillSuccess", "II"},

	{"OnRushEnd", "dd"},
	{"SyncActiveTempSkill", "II"},
	{"ClearActiveTempSkill", ""},


	{"DieKeDoPossessOther", "IId"},
	{"DieKeDoPossessByOther", "IIIIb"},
	{"DieKeUnPossessOther", "II"},
	{"DieKeUnPossessByOther", "IIII"},
	{"DieKeClearPossessByOther", "I"},
	{"DieKeSyncPossess", "dbbbb"},

	{"SyncPersonalChallengeTarget", "IU"},
	{"SyncEnemy", "IU"},

	{"ButterflyPlayerReadNpcPopWnd", "I"},			-- npcIdx
	{"ButterflyUnlockMemorySuccess", "II"},			-- npcIdx, segmentIdx
	{"ButterflyUpdateUIStatus", "IIU"},				-- npcIdx, rewardTimes, resultTbl
	{"ButterflyCollectRewardSuccess", "IU"},		-- rewardIdx, itemCountTbl
	{"ButterflyPlayerFirstUnreadNpc", "I"},			-- npcIdx
	{"ButterflyPlayerCompleteBossFight", "I"},		-- npcIdx
	{"SyncChiJiLeftTeamCountInfo", "II"},

	{"JiuGeShiHunResult", "bII"},					-- bWin, hpPercent, rewardItemId
	{"JiuGeShiHunHpPercent", "I"},					-- hpPercent

	{"SyncMobileBindVerifyStatus", "I"}, --status
	{"SyncGuildStatuePolluteValueChange", "II"},

	{"SyncStopStarBiwuFight", "sdUUUI"}, -- requestName, beginTime, agreeUd, disagreeUd, choiceUd, totalCount

	{"ShowConfirmBuyProductWithTokenMoney", "sdI"}, -- pid, targetPlayerId, tokenItemId
	{"NotifyTokenMoneyConsumed", "sIsI"}, -- pid, count, tokenItemId, tokenItemTemplateId

	{"OnPlayerQingHongFlyFail", ""},
	{"LuaSetMainScene", "s"}, --metaSceneName
	{"TestGatewayRttEnd", "d"},


	{"HouseCompetitionTrySelectCardResult", "IbsI"}, -- idx, result or false, addType or EMPTY_STRING, templateId or 0
	{"HouseCompetitionTryExtraCardRewardResult", "IbsI"}, -- times, result or false, addType or EMPTY_STRING, templateId or 0
	{"BindHouseTemplate", "I"},

	{"SyncUnLockSpecialFaBaoFxLevelUp", "s"}, -- data
	{"GetSpecialFaBaoFxSuccess", "I"}, -- appearanceKey

	{"SyncSceneItemStatus", "II"}, -- sceneItemId, status

	{"CheckEarphone", "U"},

	{"SyncButterflyPickNpcResult", "IU"}, -- npcId, data

	{"PlayerOverSeaFlagChange", "I"}, -- flag
	
	{"JiuGeShiHunMonsterKillCount", "II"}, -- monsterKillCount, monsterTotalCount
	{"SyncGetLatestDaShenLogToken", ""},

	{"UpdateClientHouseSkyboxInfo", "sI"}, -- id, skyboxIdx
	{"NewHouseSkyboxUnlocked", "sI"}, -- id, skyboxIdx

	{"PlayerUpdateDayHuoLi", "II"}, -- newDayHuoLi, newExpireTime

	{"ButterflyPlayerEnterScene", "I"}, -- isUnderFight

	{"SyncAccountTransferCondition", "U"}, -- conditionUd
	{"SyncAccountBindMobile", "ss"}, -- areaCode, phoneNumber

	{"HouseScreenshotReportSuccess", ""},

	{"XYLPTryRewardResult", "Ib"}, -- groupId, bSuccess
	{"XYLPFindThread", "I"}, -- threadId
	{"XYLPQueryThreadsResult", "U"}, -- data_U

	{"YNZJQueryRankResult", "UU"}, -- 
	{"YNZJUpdateMirror", "I"}, -- 

	{"RJPMQueryRankResult", "UU"}, -- 

	{"QueryWoodPileFightHistory_Result", "IU"}, -- pileType, fightStatics
	
	{"ShowClassTransferSuccessDelayTip", ""},

	{"EnablePaiZhaoLvJing", "Ibd"}, -- id, ignoreRole, density
	{"DisablePaiZhaoLvJing", "I"}, -- id

	{"ChangeDangPuLightStage", "I"}, -- stage

	{"GlobalSpokesmanSendAvailablePawn", "U"}, -- itemsInfo

	{"HideSceneAllJiaJu", ""},
	{"ShowSceneAllJiaJu", ""},

	{"SendSkyBoxKind", "I"}, --SkyBoxKind

	{"SyncSpokesmanTCGData", "IU"}, -- composeTimes, dataUD
	{"SyncSpokesmanTCGComposeResult", "U"}, -- dataUD

	{"QuerySpokesmanContractSignResult", "b"}, -- signed
	{"SignSpokesmanContractSuccess", ""},
	{"UpdateSpokesmanContractProgress", "iI"}, -- contractId, progress
	{"QuerySpokesmanContractDetailResult", "Udb"}, -- progressList, awarded, allAwarded
	{"SendFinishSpokesmanContractAwardSuccess", "i"}, -- contractId
	{"SendFinishAllSpokesmanContractAwardSuccess", ""},

	{"SyncSpokesmanOperatorId", "ddi"}, -- designerOperatorId, fansOperatorId, isLoadFakedFurtniture
	{"SyncHuiLiuLimitRmbItemInfo", "IIU"},

	{"OpenDonateSpokesmanHouseWndResult", "I"}, -- isShowEnterHouseBtn

	{"ReplaceSkillIcon", "III"},
	{"ResetSkillIcon", "I"},

	-- 2020 七夕
	{"StartFireWorkChapter", "I"},
	{"AttachLightToCharacter", "I"}, -- characterEngineId
	{"SyncQiXiLiuXingYu", "U"}, -- kindList

	{"ReplyScoreShopGoodsRestStock", "III"}, --shopId, itemTemplateId, restStock
	{"ShowReverseSkillFx", "IIIIIIII"},
	{"ReplyDieKeVipTicketSellInfo", "UI"}, --restStockData, isBuy

	{"SyncHuiLiuFundInfo", "ddbbddUb"}, -- expiredTime, nowTime, purchased, allAwarded, finished, awarded, progressListUD, showAlert
	{"NotifyChargeHuiLiuFundDone", "d"}, -- expiredTime
	{"UpdateHuiLiuFundTaskProgress", "dIdb"}, -- expiredTime, id, progress, finished
	{"GetHuiLiuFundTaskAwardSuccess", "dIb"}, -- expiredTime, id, allAwarded

	-- 易市求购
	{"SendMyRequestBuyOrder", "U"},
	{"SendRequestBuyOrderCountByType", "IU"}, -- type, dataUD
	{"SendRequestBuyOrderList", "IIIU"}, -- itemTemplateId, page, totalPage, dataUD
	{"SendRequestBuyItemPrice", "IIII"}, -- itemTemplateId, basisPrice, minPrice, maxPrice
	{"CreateRequestBuyOrderSucc", "U"},
	{"CancelRequestBuyOrderSucc", "U"},
	{"SellItemToRequestBuyOrderSucc", "IdI"}, -- itemTemplateId, orderId, selledCount
	{"OpenRequestBuyWindowResult", "dI"}, -- shopId, itemTemplateId
	{"RequestBuyExistLowPriceItem", "III"}, -- itemTemplateId, itemCount, singlePrice
	{"SendRequestBuyOrderCount", "U"}, -- dataUD

	{"SyncAttachJingCaiButton", "bU"}, -- bShow, ccidListUd

	{"OpenCCHighlightEntrance", "U"},
	{"CloseCCHighlightEntrance", ""},


	{"SyncClassTransferFundInfo", "dddbbddUbb"}, -- endTime, taskExpireTime, nowTime, purchased, allAwarded, finished, awarded, progressListUD, showAlert, bIsTransfered
	{"NotifyBuyClassTransferFundDone", "d"}, -- taskExpireTime
	{"UpdateClassTransferFundTaskProgress", "dIdb"}, -- taskExpireTime, id, progress, finished
	{"GetClassTransferFundTaskAwardSuccess", "dIb"}, -- taskExpireTime, id, allAwarded

	{"CloseQte", "I"}, -- qteId
	{"SyncAdjustSceneLight", "I"}, -- lightValue
	{"ShowAppUpgradeNotify", "sU"}, -- notifyType, content_U

	{"SubmitJiaNianHua2020PersonalInfoSuccess", ""},
	{"ShowQiXiTianDengHpBar", "Idd"}, -- engineId, hp, hpFull
	{"RequestTaskOpenTimeCheckResult", "Ibs"}, --taskId, result, openTime
	{"SyncPvpKillInfo", "UU"}, -- playerId1, playerId2
	{"CheckSpokesmanContractAwardResult", "bb"}, -- hasAward, hasAllAward

	-- 国庆斗地主
	{"SyncQiangXiangZiSignUpInfo", "bUIII"}, -- isSignUp, remainAwards, myRank, honor, nextRefreshTime
	{"SyncQiangXiangZiSignUpResult", "b"}, -- isSignUp
	{"SyncQiangXiangZiRoundResult", "bbbIIU"}, -- isWin, isDiZhu, isPlayOver, roundChestCount, totalChestCount, extraData
	{"SyncQiangXiangZiOpenChestResult", "IIU"}, -- remainChestCount, getHonor, awards


	{"EnterHouseCompetitionTemplateShowPlay", "I"}, -- templateId
	{"QueryNeteaseMemberWelfareInfoFailed", ""},

	-- 双十一活动
	{"ClearTrolleyStatics","IUUI"},	-- credit, {red, yellow, blue}, {x, y, colour}, round
	{"ClearTrolleyResult","II"},	-- credit, rewardItemId
	{"PlayerZhongCaoRet2020","II"},	-- mallItemId, count
	{"QueryAllZhongCaoCountRet2020","iU"}, -- progress, data
	{"QueryDouble11VouchersInfoRet","UU"}, -- packageInfo, voucherInfo
	{"SendDouble11LotteryResult","IU"}, -- status, rewardInfo
	{"PlayerHitDouble11RebateReward","III"}, -- prize, payJade, returnJade

	{"Halloween2020ArrestPlaySignUpResult", "b"}, --bSuccess
	{"Halloween2020ArrestPlayCancelSignUpResult", "b"}, --bSuccess
	{"QueryHalloween2020ArrestPlayMatchInfoResult", "b"}, --bInMatching
	{"QueryHalloween2020ArrestPlayInfoResult", "iUU"}, --winForce, attackData, defendData
	{"EnterHalloween2020ArrestPlay", "i"}, --force
	{"ReplyHalloweenTerrifyAndDanShiInfo", "iiiii"}, --terrifyCount, maxTerrifyCount, level, param1, param2
	{"QueryHouseMemberAtHomeInfoResult", "dU"}, --communityId, dataUD
	{"SyncHalloween2020ArrestPlayInfo", "IIU"}, --restCount, totalCount, playData

	-- new huiliu
	{"SyncZhaoHuiFriendList", "U"}, -- friendList
	{"SyncAlreadyZhaoHuiFriendList", "U"}, -- friendList
	{"SendZhaoHuiInviteFriendSucc", "d"}, -- friendPlayerId
	{"SyncZhaoHuiTaskInfo", "dIU"}, -- friendPlayerId, expireTime, taskInfo
	{"SendGetZhaoHuiTaskRewardSucc", "dIIIb"}, -- friendPlayerId, taskId, progress, stage, isRewarded
	{"SyncHuiGuiInviterInfo", "IUU"}, -- expireTime, inviterInfo, notFriendInviterInfo
	{"SyncHuiGuiBindedTaskInfo", "dIIU"}, -- inviterPlayerId, huiguiScore, taskExpireTime, taskInfo
	{"SendHuiGuiInviteBindSucc", "d"}, -- friendPlayerId
	{"SendGetHuiGuiTaskRewardSucc", "dIIIb"}, -- friendPlayerId, taskId, progress, stage, isRewarded
	{"ShowNewZhaoHuiBtn", ""},
	{"HuiLiuShowConfirmBindWithInvitee", "dss"}, -- friendId, accountName, characterName

	{"SyncDuoMaoMaoSignUpResult", "b"},
	{"SyncPlayerDuoMaoMaoHp", "dIIb"},
	{"SyncDuoMaoMaoYaoQiHp", "III"},

	{"Wish_AddWishNotifyUI", "sb"},
	{"Wish_DelWishNotifyUI", "sb"},
	{"Wish_AddHelpNotifyUI", "sb"},
	{"Wish_QueryWishLimitResult", "bII"},

	{"LuaQueryMonthPresentLimitResult", "dddI"}, -- used, vipLimit, adjustLimit, context
	{"SyncDuoMaoMaoAddHpSkillPoint", "I"},

	{"SyncPlayReport", "ssddUUUUUU"}, -- id, alias, designPlayId, fightTime, teamInfo, playerInfo, reportInfo, roundScore, chuzhanInfoUd, extra

	{"TryStartGuide", "IIbb"}, -- phase, subPhase, bForce, bSave
	{"TryEndGuide", ""},

	{"SendAnQiIslandPlayInfoResult", "UUUbI"}, -- cannonInfo, defendPlayerInfo, attackPlayerInfo, isFinish, doubleTimes
	{"SendAnQiIslandMaxDataInfo", "U"},	-- maxDataInfo
	{"UpdatePlayerAnQiIslandScore", "III"},	-- score, exp, freeSilver
	{"ShowAnQiIslandResultWnd", ""},
	{"AnQiIslandFireCannon", "II"},	-- force, count
	{"AnQiIslandSyncCannonInfo", "UU"}, -- defInfo, attInfo
	{"UpdateAnQiIslandPlayerPersonalData", "UU"}, -- scoreInfo, achieveTbl
	{"SendAnQiIslandCurrTime", "i"}, -- time
	{"AnQiIslandSyncCannonBallAndBossPos", "UU"}, -- pickInfo, bossInfo

	{"SyncRepertoryForMallPreviewDone", ""},

	-- 2020元旦跨年分享窗口
	{"ShowYuanDanHappyNewYearShareWnd", ""},
	{"SyncShengDanXueRenXueQiuNum", "IIb"},
	{"SyncShengDanCatchXueQiuNum", "I"}, -- xueQiuNum
	{"AddFxWithROScale", "II"}, -- engineId, fxId

	{"SyncTianChengData", "IIIIIIU"}, -- level, jiuqi, state, rewardedLevel, rewardedVipLevel, levelLimit, taskUD
	{"TianChengLevelRewardResult", "IIIU"}, -- level, state, rewardedLevel, items

	-- Yuanxiao 2021
	{"TangYuan2021PlayerEnd", "IIII"}, -- gameplayTime, highScore, score, idx
	{"UpdateTangYuan2021PlayInfo", "U"}, -- count, hp, maxHp
	{"QueryTodayTangYuan2021Score_Result", "I"}, -- highScore
	{"QueryPlayerIsMatchingTangYuan2021_Result", "b"}, -- isMatching

	-- 金城池结冰
	{"SetJinChengChiFrozen", ""},
	{"SetJinChengChiIceMelt", "ii"}, -- gridPosx, gridPosy

	{"YanHuaScreenshotReportSuccess", ""},
	{"PlayYanHuaLiBao", "iiSid"}, --x, y, yanhuaStr, dir, playerId

	-- 雾效
	{"UpdateSceneFogSetting", "dddss"}, -- fogStart, fogClarity, fogClarityMaximum, heightFogColor, fogColor
	-- 完整参数雾效
	{"UpdateSceneFogSettingComplete", "bbdddddddddss"}, -- enableFog, enableFogDarken, heightFogStart, heightFogEnd, heightFogColorFalloff, heightFogColorOffset, fogStart, fogClarity, heightMinimum, fogFalloff, fogClarityMaximum, heightFogColor, fogColor

	-- HanJia2021
	{"SyncHanJiaAdventureSignUpResult", "b"}, -- isSignUp
	{"OpenHanJiaAdventureSignUpWnd", "bI"}, -- isSignUp, todayRewardTimes
	{"SyncHanJiaAdventurePlayInfo", "U"}, -- infoUD
	{"ShowHanJiaAdventureResultWnd", "IIIU"}, -- rank, aliveSeconds, monsterId, resultUD

	{"AnQiIslandPickCannonBall", "Id"}, -- pickEngineId, playerId
	{"AnQiIslandBossKilled", "I"}, -- force

	{"SendJingLingKeyMsgWithCondition","SSb"},

	-- 2021春节
	{"CareFireworkSignUpPlayResult", "b"}, -- bSuccess
	{"CareFireworkCancelSignUpResult", "b"}, -- bSuccess
	{"CareFireworkCheckInMatchingResult", "bbSI"}, -- bSuccess, bInMatching, resultStr, rewardTimes
	{"SyncCarefireWorkPlayerInfo", "dU"}, -- playerId, playerUd
	{"SyncCarefireWorkSceneInfo", "ddd"}, -- leftCount, totalCount, phase
	{"SyncCarefireWorkSummaryInfo", "U"}, -- summaryUd
	{"SyncCarefireWorkPlayerSelfInfo", "dU"}, -- playerId, playerUd

	-- 自立门派
	{"SyncSectXinWuInfo", "bU"}, -- bInSect, sectXingWuInfoRpcUD

	{"MapEditorOpenSearchMonsterWnd", ""},
	{"ReplyJianJiaCangCangFlowerInfo", "IIIIb"}, --npcEngineId, data.stage, data.time1, data.time2, bOperate
	{"ReplyJianJiaCangCangPlayInfo", "b"}, --bReceive

	{"ReplyBuyOffWorldPassResult", "bI"}, -- bSuccess, passId
	{"OffWorldCollectRewardSuccess", "UU"}, -- playInfo, rewardItems
	{"OffWorldPassNeedRedDot", "bb"}, -- bPassNeedRedDot, bPlayNeedRedDot
	{"UseOffWorldPassRMBPackageSuccess", "I"}, -- passId
	{"OffWorldPlayerQueryPlayInfoRet", "U"}, -- passId
	
	{"SoulCoreSetLevel", "I"}, --lv
	{"SoulCoreSetMana", "d"}, --mana
	{"SoulCoreUpdateDisplay", "IIIIIs"}, --DisplayCoreId, DisplayFxId, DisplayPalletId, DisplayCoreParticles, DisplayCoreEffect, reason

	{"PassiveSkillSetLevel", "III"}, --SkillType, Index, Level
	{"SyncSectInWorldMap", "U"}, -- mapUd
	{"TrackToSectEntrance", "III"}, -- entranceMapId, entrancePosX, entrancePosY

	{"PlayerFengXianSuccess", "I"}, -- skillId
	{"AddWorldPositionMultiFxWithDir", "iisi"}, -- posX, posY, fxList, dir
	
	{"ReplyInviteTudiGoinSect", ""},
	{"SyncCreateSectDone", ""},
	{"SyncTudiSectAndMingGe", "U"},
	{"ReplyJianDingMingGe", "I"},
	{"SyncPlayerMingGe", "I"},
	{"SyncToJoinSectInfo", "bU"},
	{"ReplyJoinSect", "dIdUUU"}, -- newSectId, mingGe, oldSectId, stdList, invalidList, extra
	{"ReplyQuitSect", "d"},
	{"SyncLastInviteSectTime", "U"},
	
	{"SyncSetPlayerSectId", "d"}, -- newSectId
	{"SyncSectInfoBegin", "dsd"}, -- sectId, infoType, extra
	{"SyncSectInfo", "dsdU"}, -- sectId, infoType, extra, Ud
	{"SyncSectInfoEnd", "dsdU"}, -- sectId, infoType, extra, extraUd
	{"SyncMySectTitleInfo", "UI"}, -- titleListUd, currentTitle
	{"ReplyBatchChangeSectOffice", "db"}, -- sectId, bSuccess
	{"ReplySetSectRights", "dbU"}, -- sectId, bSuccess, rightUd
	{"ReplyResignSectOffice", "b"}, -- bSuccess
	{"SyncPlayerSectSpeakStatus", "ddb"}, -- sectId, playerId, bForbidden
	{"SyncSectEvilValue", "bddb"}, -- bSuccess, sectId, evilValue, bEvil
	{"ReplySubmitSectEvilYaoGuai", "bddb"}, -- bSuccess, sectId, evilValue, bEvil
	
	{"ReplyYiAiZhiMingSendFlowerRank", "UU"}, -- selfData, rankData

	{"ShowEngineHeadFxWithDuration", "III"}, -- engineId, fxId, duration
	{"SyncTianChengHasReward", "bb"}, -- level reward, task reward

	-- 新反弹机制 告知客户端播放特效
	{"StartFlick","II"},	-- engineid, aniType

	-- 捉妖
	{"SendYaoGuai","Ub"},	-- yaoguaiUD, isOnLoad
	{"DeleteYaoGuai","s"},	-- yaoguaiId
	{"IncYaoGuaiBagSucc","I"},	-- currentSize
	{"TryCatchYaoGuai","I"},	-- monsterEngineId
	{"CatchYaoGuaiResult","bIs"},	-- isSucc, monsterEngineId, yaoguaiId
	{"SyncYaoGuaiTuJian","U"},	-- tujianData
	{"SyncZhuoYaoLevelAndExp","III"}, -- level, exp, bagSize
	{"SyncGetTuJianRewardSucc","I"}, -- yaoguaiTempId
	{"SyncGetTuJianFullQualityRewardSucc","I"}, -- yaoguaiTempId
	{"SendZhuoYaoLevelUp","II"}, -- oldLevel, newLevel

	-- 灵核获取任务
	{"AddSoulcoreLinkFxAndAniWithoutDuration", "III"}, -- engineId, engineId2, fxId
	{"RmSoulcoreLinkFxAndAni", "II"}, -- engineId, fxId
	{"SyncCondenseLeftTime", "bddIId"}, -- bPause, endTimestamp, leftTime, speed, phase, totalTime
	{"ReplySubmitCondenseItem", "bd"}, -- bOk, endTimestamp
	{"SyncCondenseWeatherChange", "Id"}, -- speed, endTimestamp
	{"ReplyPauseCondense", "bdd"}, -- bOk, endTimestamp, leftTime
	{"ReplyResumeCondense", "bd"}, -- bOk, endTimestamp
	{"SyncFinishCondense", ""},
	{"SyncFinishCarving", ""},
	{"ReplySectFaZhenAvoidWuXingInfo", "ddddbd"}, -- sectId, currentWuXing, avoidWuXing, nextCanSetTime, bFree, hasShengWang
	{"ReplySetSectFaZhenAvoidWuXing", "ddddbd"}, -- sectId, currentWuXing, avoidWuXing, nextCanSetTime, bFree, hasShengWang

	{"QuerySectFaZhenCurrentInfoResult", "bIIIIIIIdIIdddbdddIdIdI"}, 
	{"PutSoulCoreIntoFaZhenSuccess", ""},
	{"PutMonsterIntoFaZhenSuccess", "sI"}, --id, templateId
	{"QueryFaZhenLianHuaMonsterQueueInfoResult", "U"},
	{"RemoveFaZhenLianHuaMonsterSuccess", "Is"}, --index, id
	{"QuerySectPersonalInputItemInfoResult", "IUUU"},
	{"QuerySectTopInputItemInfoResult", "IU"},
	{"PutItemIntoFaZhenSuccess", "I"}, --templateId
	{"QueryLianHuaItemSubmitHistroyResult", "U"},
	{"SyncSectFaZhenPeopleInfo", "ddIIdIIII"}, -- sectId, playerId1, engineId1, time1, playerId2, engineId2, time2, isXiePai, wuxing

	-- 家园室内墙纸地板
	{"UpdateWallOrFloorPaperType", "III"},
	{"NewHouseWallFloorPaperUnlocked", "sIII"},
	{"SendItemV2", "bU"}, --isItem, itemData
	{"SyncFengHuaItemStatus", "Ibbb"},
	{"SyncTempPlayScore", "III"},

	{"PlayerContactSectManager", "dds"}, -- sectId, managerId, managerName

	{"AntiProfession_UpgradeSkillResult", "bIII"}, -- bSuccess, Index, Level, FailTimes
	{"AntiProfession_UnlockSlotResult", "bIIII"}, -- bSuccess, Index, Profession, MaxLevel, FailTimes
	{"AntiProfession_BreakLimitResult", "bII"}, -- bSuccess, Index, MaxLevel
	{"AntiProfession_SetAntiProfessionMul", "dS"}, -- expiredTime, extraStr
	{"AntiProfession_UpdateSkill", "IU"}, -- Index, skillObject_U
	{"AntiProfession_UpdateScore", "II"}, -- Profession, score
	{"AntiProfession_QueryTransferTimeResult", "d"}, -- time
	{"AntiProfession_RandomChangeScore", "U"},
	{"AntiProfession_UpdatePersistTransfer", "IS"},


	-- 藏宝阁渠道登录授权
	{"CBG_SendPlayerChannelLoginToken", "iS"}, -- code, token
	{"JiaoShanTrapAOIChange", "III"}, -- engineId, curr, total
	{"SendJiaoShanBloodstoneHp", "II"}, -- engineId, hp
	{"SendJiaoShanPlayResult", "UU"}, -- playResultInfo, playerScoreInfo
	{"ShowJiaoShanDropFx", "I"}, 	-- npcEngineId
	{"UnlockOffWorldBloodShopRet", "II"}, 	-- progress, count
	{"GetOffWorldBloodShopInfoRet", "IU"}, 	-- progress, onShelfUd

	{"RequestUseKunXianSuoResult", "ddI"}, -- playerId, targetId, result
	{"RequestUseKunXianSuoCheckResult", "dI"}, -- targetId, result

	{"SendXinFuLotteryResult", "UI"}, -- resultUD, days
	{"ShowHongBaoCanSnatchIcon", "I"}, -- channelType
	{"ShowSceneItemHighlight", "IsI"}, -- sceneId, itempath, time
	{"SyncQingMing2021TuJianInfo", "I"},
	{"ShowQingMing2021PlaySuccessWnd","II"},
	{"SyncQingMing2021PlayTime", "IIIII"},
	{"SyncQingMing2021PlayMonsterCount", "U"},

	{"ShowEyeCloseEffect", "dId"}, -- durationTime, blinkType, blinkTotalTime
	{"ShowScenePostEffect", "Ib"}, -- effectType, isStart
	{"ShowUIDangerEffect", "b"}, -- enable

	{"SMSW_QueryRankResult", "Ud"},
	{"SMSW_QueryPlayInfoResult", "Idd"},
	{"SMSW_SyncPlayState", "IIdd"},
	{"SMSW_DestroyScene", "I"},
	{"SMSW_BroadcastBossInfo", "Idd"},

	{"SyncSectHasTitleMember", "dIU"}, -- sectId, titleId, titleData
	{"SyncEnemySectOverview", "dU"}, -- sectId, ovewviewUd
	{"SyncEnemySectCaptured", "dU"}, -- enemySectId, ovewviewUd
	{"SyncSectEntranceByMapId", "U"}, -- ovewviewUd
	{"SyncSectTaskDestSceneId", "IdIs"}, -- taskId, sectId, raidMapId, sceneId

	{"SyncDynamicShopShangJiaItem", "U"}, -- dataUD

	{"SyncSectFaZhenMonsterInfo", "dIIIIIU"}, -- sectId, templateId, costTime, beginTime, isXiePai, wuxing, lingheAppearanceData
	{"ReleaseFaZhenBeiZhuoPlayer", "Id"}, -- index, playerId

	{"SendAllSectNpcData", "UUIbb"}, -- joinedUD, unjoinUD, mySectNpcId, isSectLeader, inCompetition
	{"SendMySectNpcData", "IIIbU"}, -- todayTaskTimes, npcId, npcState, isRewarded, dataUD
	{"SendNpcInviterData", "IIIIbU"}, -- myDaoYiBaseValue, myDaoYiAddonValue, npcId, inviterCount, isRewarded, inviterUD
	{"SendNpcJoinSectSucc", "I"}, -- npcId
	{"SendMySectNpcId", "I"}, -- npcId

	{"ReplySetSectOffice", "ddIb"}, -- sectId, targetPlayerId, office, bSuccess
	{"ReplyKickSectMember", "ddb"}, -- sectId, targetPlayerId, bSuccess
	{"SyncSectSkyBox", "I"}, -- skyboxId
	{"BroadcastSectMessageTopHighline", "dS"}, -- sectId, msg

	{"CaiDieSignUpResult", "b"}, -- bSuccess
    {"CaiDieShowTips", ""},
    {"SyncCaiDieDestructionInfo", "Id"}, -- destructionPrec, cursedPlayerId
    {"SyncCaiDiePlayerStoneInfo", "I"}, -- stone
    {"SyncCaiDieBossInfo", "I"}, -- bossEngineId
    {"SendCaiDiePlayResult", "IU"}, -- score, playerScoreInfo
	{"SyncCaiDieTaskInfo", "Ib"}, -- taskType, bStartTask
	
	{"SyncAddSceneSectTrapInfo", "dbU"}, -- sectId, bXiePai, TrapPointUD
	{"SyncRmSceneSectTrapInfo", "d"}, -- sectId
	{"SyncSectSetHideFromIdSearch", "db"}, -- sectId, bOpen
	{"SyncSetSectMemberSpeakStatusResult", "ddI"}, -- sectId, targetPlayerId, status
	{"SyncSectName", "ds"}, -- sectId, sectName

	{"PlayerPressQiaoqiaoban", "Id"}, -- fid, playerId
	{"SyncAddSectEntrance", "dIIIb"}, -- sectId, mapId, x, y, bXiePai
	{"SyncRmSectEntrance", "d"}, -- sectId
	{"PlayLianHuaZhuoRenFx", "II"}, -- playerEngineId, attackerEngineId

	{"ReplyCrossGnjcGroupInfoQueryResult", "IUI"}, -- group, dataUD, count
	{"ReplyCrossGnjcServerInfoQueryResult", "IU"}, -- group, dataUD
	{"RequestGetFengHuaLuStageAwardSuccess", "I"},

	{"UpdateHousePool", "sUU"}, -- houseId, poolud, warmpoolud

	{"SendMusicBoxState", "III"}, -- currentSong, currentState, progress
	{"SendMusicBoxPlayList", "U"}, -- playlist

	{"PlayBigPicture", "si"}, -- path, duration

	{"SyncCreateSectPrecheckResult", "bIdUUIIs"}, -- bSuccess, mingGe, oldSectId, allStdListUd, mismatchStdListUd, place, pos, itemId

	-- wuyi2021
	{"SyncWuYi2021FindMindData", "IbU"}, -- mindPieceCount, fullRewarded, dataUD
	{"SyncWuYi2021JXMXPlayInfo", "IUI"}, -- stage, data, bossEngineId
	{"SyncWuYi2021JXMXPlayResult", "bb"}, -- hasReward, isWin

	{"LearnGuanTianXiangSkillSuccess", "I"}, -- level

	{"ReplySectFutureWeather", "IU"}, -- hours, dataUD

	-- ErHaTCG
	{"SyncErHaTCGData", "IIIIIIIUbI"}, -- haitangCount, haitangPieceCount, curHuoLi, freeTimes, score, curMainTaskIdx, hiddenComposeCount, dataUD, canAcceptMainTask, currentMainTaskIdx
	{"SyncErHaTCGComposeResult", "IUU"}, -- hiddenComposeCount, cardUD, itemUD
	{"ErHaTCGAcceptCardTaskSucc", "II"}, -- cardId, taskState
	{"ErHaTCGAcceptScoreTaskSucc", "I"}, -- newTaskIdx
	{"ErHaTCGBuyHaiTangSucc", "I"}, -- newHaiTangCount
	{"ErHaTCGShareDoneRet", "I"}, -- hiddenComposeCount

	{"SendCheckOrderInfo_GAS3", "ssU"}, -- pid, gamesn, info_data
	{"SyncCurrentSceneSectId", "sd"}, -- sceneId, sectId

	{"InitSaiMaSequence", "U"}, -- saiMaRegionUD
	{"StartSaiMa", "IIII"},      -- baseVertialSpeed, baseHorizontalSpeed, curRegionId, startPlayTime
	{"EndSaiMa", ""},
	{"SyncSaiMaAccelerateInfo", "IIII"},       -- hitType, colour, duration, timeCount
    {"PlayerUseSaiMaJumpSkill", ""},
    {"PlayerEnterSaiMaJumpRegion", "IId"},	   -- start, size, duration
    {"PlayerLeaveSaiMaJumpRegion", "IIbd"},    -- engineId, result, bSkill, forwardSpeed

	{"TongQingYouLi_QueryInfoResult", "U"}, -- info_U
	{"TongQingYouLi_RewardResult", "IIS"}, -- rewardTimes, randomIdx, str
	
	{"CaiDieOnPlayerBeginCurse", "d"}, -- cursedPlayerId
	{"OffWorldCollectWeekReward", "U"}, -- itemsSent
	{"OffWorldGetRealCreature", "II"}, -- weaponType, wuxing
	{"SyncShengChenGangBiaoCheInfo", "IIIII"},
	

	{"ZNQZhiBo_SyncChannelInfo", "S"}, -- RemoteChannelInfo
	{"ZNQZhiBo_ShowInvition", ""},
	{"ZNQZhiBo_ZhiBoEnd", ""},

	{"SyncXiepaiExtraRewardSwitch", "dbdd"}, -- sectId, bOpen, leftTime, expCost
	-- 宗门牌匾
	{"SyncSectNameAndStd", "dsU"}, -- sectId, name, stdList

	{"SendPersonalSpaceBackGroundList", "IU"}, -- curBgId, bgList
	{"SendActivePersonalSpaceBackGround", "U"}, -- bgIds
	{"SendUsePersonalSpaceBackGround", "I"}, -- bgId

	{"ClubHouse_QueryRoom_Result", "Us"}, -- room_U, Context
	{"ClubHouse_QueryRoomList_Result", "UIs"}, -- rooms_U, Page, context
	{"ClubHouse_CreateRoom_Result", "dbU"}, -- playerId, bSuccess, data_U
	{"ClubHouse_LeaveRoom_Result", "dbU"}, -- playerId, bSuccess, data_U
	{"ClubHouse_EnterRoom_Result", "dbUS"}, -- playerId, bSuccess, data_U, Context
	{"ClubHouse_ChangeStatus_Reply", "dsb"},
	{"ClubHouse_ChangeStatus_Result", "dbU"}, -- playerId, bSuccess, data_U
	{"ClubHouse_ClearStatusAll_Result", "dbU"}, -- playerId, bSuccess, data_U
	{"ClubHouse_ForbidStatusAll_Result", "dbU"}, -- playerId, bSuccess, data_U
	{"ClubHouse_SetMic_Result", "dbU"}, -- playerId, bSuccess, data_U
	{"ClubHouse_UnsetMic_Result", "dbU"}, -- playerId, bSuccess, data_U
	{"ClubHouse_Kick_Result", "dbU"}, -- playerId, bSuccess, data_U
	{"ClubHouse_DestroyRoom_Result", "dbU"}, -- playerId, bSuccess, data_U
	{"ClubHouse_ChangeOwner_Result", "dbU"}, -- playerId, bSuccess, data_U
	{"ClubHouse_ChangePassWord_Result", "dbS"}, -- playerId, bSuccess, password
	{"ClubHouse_QueryPassWord_Result", "dbU"}, -- playerId, bSuccess, data_U
	{"ClubHouse_QueryMemberByStatus_Result", "dbUss"}, -- playerId, bSuccess, data_U, Status, Context
	{"ClubHouse_QueryAllListenersInRoom_Result_Begin", "dd"}, -- playerId, RoomId
	{"ClubHouse_QueryAllListenersInRoom_Result", "ddU"}, -- playerId, RoomId, data_U
	{"ClubHouse_QueryAllListenersInRoom_Result_End", "dd"}, -- playerId, RoomId
	{"ClubHouse_QueryMyRoomId_Result", "dS"}, -- RoomId, Context
	{"ClubHouse_QueryRank_Result", "IUd"}, -- RankType, RankData_U, MyValue

	{"ClubHouse_Notify_DelRoom", "d"}, -- RoomId
	{"ClubHouse_Notify_LeaveRoom", "ddIId"}, -- RoomId, AccId, MicCount, ListenerCount, Heat
	{"ClubHouse_Notify_EnterRoom", "UIId"}, -- msgpack.pack(member), MicCount, ListenerCount, Heat
	{"ClubHouse_Notify_MemberChange", "U"}, -- msgpack.pack(member)
	{"ClubHouse_Notify_ClearStatusAll", "ds"}, -- RoomId, Status
	{"ClubHouse_Notify_ForbidHandsUpChange", "db"}, -- RoomId, bForbid
	{"ClubHouse_Notify_SetMic", "UIId"}, -- msgpack.pack(member), MicCount, ListenerCount, Heat
	{"ClubHouse_Notify_UnsetMic", "UIId"}, -- msgpack.pack(member), MicCount, ListenerCount, Heat
	{"ClubHouse_Notify_ChangeOwner", "UU"}, -- msgpack.pack(owner), msgpack.pack(prevOwner)
	{"ClubHouse_Notify_GivePresent", "UIIIddIU"}, -- info_U, PresentId, Count, Combo, Heat, DstAccId, EffectTimes, ToRemove_U
	{"ClubHouse_Notify_Reset", ""}, -- 
	{"ClubHouse_Notify_RenameRoom", "dS"}, -- RoomId, Name
	{"ClubHouse_Notify_UpdateTitle", "dI"}, -- RoomId, TitleId
	{"ClubHouse_Notify_UpdateHeat", "dd"}, -- RoomId, Heat

	{"ClubHouse_BroadcastMessageInRoom", "ddSSSIS"}, --RoomId, SendId, AppName, SendName, content, RoomTop, Character
	{"ClubHouse_GivePresentFailed", ""}, -- 
	{"ClubHouse_Notify_ResetRoomPresent", ""}, -- 

	{"SyncSpokesmanSectRankExtra", "bd"}, -- isSigned, effectCnt
	{"ReplySignSpokesmanSect", "b"}, -- isSigned
	{"SyncSpokesmanSectSignStatus", "b"}, -- isSigned

	{"OpenPcUniSdkChargeConfirmWnd", "ss"}, -- pcPayTicket, pid
	{"NotifyMobileScanPcUniSdkChargeConfirmQrWnd", "s"}, -- pid
	{"UseTempTaBen", "II"},
	{"SyncGnjcZhanYiProgressInfo", "II"}, -- current, total
	{"SyncGnjcBossHpInfo", "dddd"}, -- attackerHp, attackerFullHp, defenderHp, defenderFullHp
	{"CloseCaiDieWaitUI", ""},

	{"SyncSetSectShowTitle", "dI"}, -- sectId, titleId
	{"CandyPlaySignUpResult", "b"}, -- bSuccess
    {"CandyPlayCancelSignUpResult", "b"}, -- bSuccess
    {"CandyPlayCheckInMatchingResult", "b"}, -- bInMatching
    {"CandyPlayStageFx", "I"}, -- stage
    {"CandyPlayOpenSelectWnd", "I"}, -- countDown
    {"CandyPlayerScoreChange", "i"}, -- deltaScore
    {"CandyPlaySyncPlayInfo", "U"}, -- playInfo
    {"CandyPlayResult", "III"}, -- babyType, rank, score

	{"EnableSpecialBuilding", "sI"}, -- houseId, buildingType

	{"SendFashionTransformState", "b"}, -- isTransformOn
	{"SyncMySectName", "s"},

	-- Duanwu2021
	{"SyncLongZhouPlaySignUpResult", "b"}, -- isSignUp
	{"SyncLongZhouPlayPlayInfo", "IU"}, -- rank, infoUD
	{"ShowLongZhouPlayResultWnd", "IIUb"}, -- rank, time, temples, isWin
	{"SyncStartLongZhouOperate", ""},
	{"SyncLongZhouOperateResult", "Ub"}, -- resultUD, isRoundOver
	{"OpenLongZhouPlaySignUpWnd", "bI"}, -- isSignUp, remainRewardTimes
	{"AddLongZhouTempleBuff", "I"}, -- buffId
	{"SyncLongZhouOnBoatStatus", "b"}, -- onBoat

	{"ShowTransferEquipStoneConfirm", "Iiisiis"},

	{"SyncSectAlterNameInfo", "dddU"}, -- sectId, lastAlterNameTime, alterNameCost, nameUsedListUd
	{"SyncTriggerEnemySectGuide", ""}, 

	{"OnSaiMaStartCountDown", ""}, 
	{"MPTYSShowStartPlayFx", ""},
	{"SyncPlayerHpInfoToWatcher", "IIIIU"},
	{"SyncSectChangeName", "s"}, -- newName
	{"SyncSectAllMemberId", "Udd"}, -- Ud, lastInviteFriendTime, lastInviteGuildTime
	{"SyncSectInfoForStone", "dUU"}, -- sectId, Ud1, Ud2
	{"ReplySectInviteImMessage", "dbIU"}, -- sectId, bOk, inviteType, invitedIds

	{"OperateFuXiDanceNeedUnLock", ""}, 
	{"UnLockFuXiDanceSuccess", ""},
	{"FuXiDanceUploadPreCheckDone", "I"}, -- index
	{"OpenFuXiDanceUploadSubJadeWnd", ""},
	{"SubJadeForFuXiDanceUploadSuccess", "I"}, -- index
	{"UpdateFuXiDanceData", "IIU"}, -- reason, index, data_U
	{"ReNameFuXiDanceMotionSuccess", "Ids"}, --index, motionId, newName
	{"DeleteFuXiDanceMotionSuccess", "Id"}, -- index, motionId
	{"ShowFuXiDanceExpressionActionState", "IId"}, -- engineId, expressionId, motionId

	{"EnableMainPlayerFootSteps", "b"}, -- bEnable
	{"SyncClientCareExpressionDone", "dd"}, -- expressionId, motionId

	-- qixi2021
	{"SendQueQiaoXianQuSignUpInfo", "bbI"}, -- isSignUp, forceOpen, todayRemainRewardTimes
	{"SendQueQiaoXianQuPartnerInfo", "UI"}, -- resultUD, partnerAwardTimes
	{"QueQiaoXianQuQuestion", "sIds"}, -- questionContent, questionId, duration, answerContent
	{"QueQiaoXianQuQuestionResult", "bb"}, -- result, timeout
	{"SyncQueQiaoXianQuPlayInfo", "IiibUb"}, -- stage, questionCount, questionFinishCount, daJianQueQiaoFinished, monsterKillDataUD, needHighlightSkill
	{"ShowQueQiaoXianQuPlayResult", "bdsIII"}, -- result, partnerPlayerId, partnerName, partnerClass, partnerGrade, partnerGender
	{"SyncYanHuaPlayInfo", "biiidiii"}, -- isOwner, visitorNum, maxVisitorNum, effectType, timeStamp, status, chapterIndex, chapterCount
	{"StartFireWorkChapterInCopy", "Id"}, -- chapterId, beginTime

	{"SyncSaiMaSpeed", "Idd"}, -- engineid, adjSpeed, mulSpeed
	{"EnableFirstPersonPerspective", "ddd"}, -- cameraHeightOffset, cameraZ, cameraY
	{"DisableFirstPersonPerspective", ""},

	{"TravestyRole_CheckSignUpResult", "bIIIIU"},
	{"TravestyRole_SignUpPlayResult", "b"},
	{"TravestyRole_CancelSignUpResult", "b"},
	{"TravestyRole_SyncPlayState", "IIIdIUSI"},


	--House Fishing
	{"UpdateHouseFishInfo", "U"}, -- fishInfo_u
	{"FishingHarvestSuccess", "Id"}, -- fishTempId, weight
	{"UpdateFishingFightState", "ddddddd"}, -- tagPos, tagSpeed, greenPos, greenSpeed, greenLen, percent, time
	{"HouseFishingFightFailed", ""}, -- 
	{"BeginFishingFight", "ddddddd"}, -- tagPos, tagSpeed, greenPos, greenSpeed, greenLen, percent, time
	{"FishingHarvestBegin", "I"}, -- fishItemTempId
	{"UpdateFishFood", "I"}, -- fishFoodTempId
	{"SetCurrentFishingWaitDuration", "II"}, -- startTime, duration
	{"StartHouseFishingGuide", "I"}, -- fishTempId
	{"SetFishPoleSuccess", "dI"}, -- playerId, fishPoleType
	{"UpdateFishBasket", "U"}, -- basket_ud
	{"PlayerFirstEnterFishingHouse", ""}, --
	{"UpdateZhengzhaiFishInfo", "IU"}, --fishId, fishUD
	{"ImproveZhengzhaiFishSuccess", "I"}, --fishId
	{"ImproveZhengzhaiFishFailed", "I"}, --fishId

	{"ShowGuanNingEnterBossFightFx", ""},
	{"SyncOlympicBossInfo", "dU"}, -- hp, mapNpcCountInfo
	{"SyncOlympicBossHp", "Id"}, -- bossEngineId, hp
	{"SyncOlympicBossStatus", "bb"}, -- isPlayStart, isBossAlive

	{"SendSeaFishingPlayInfo", "IIb"}, -- rewardTimes, lastDifficult, canEnterSpecialPlay
	{"UpdateSeaFishingPlayStageInfo", "IU"}, -- stage, params
	{"SyncSeaFishingPlayResult", "Ib"}, -- gameplayId, isWin
	{"ShowIllustrations", "II"}, -- id, duration

	{"SyncShuiGuoPaiDuiScore", "I"},
	{"SyncShuiGuoPaiDuiLianJi", "I"},
	{"ShowShuiGuoPaiDuiResultWnd", "IIUU"},
	{"SyncUnity2018UpgradeInfo", "bbssbdb"}, -- canOpenWnd, needUpgrade, upgradeUrl, packageSize, finished, finishTime, awarded
	{"SendUnity2018UpgradeAwardSuccess", ""},
	{"SetTianQiSpecialItemSkipConfirmResult", "U"},

	{"EnterGuanNingWeekendNewModePlay", ""},
	{"LeaveGuanNingWeekendNewModePlay", ""},

	{"SyncSectOpenAd", "db"}, --sectId, bOpen
	{"SyncChangeSectJoinStdPreInfo", "ddb"}, --sectId, timeStamp, bHasRight
	{"SendSectRank", "IUIdsII"},
	{"ShowLingLiRain", ""},
	{"ShutdownLingLiRain", ""},
	{"SyncSectStdRulesChangeDone", "dUU"}, -- sectId, joinStdUd, rulesUd
	{"SyncSectVoteNpcVoteId", "Id"}, -- engineId, voteId
	{"SyncSectShowLianhuaData", "U"}, -- lianhuaUd

	{"PlayerUseFuXiLaba", "SdiiiUIsUiiiUI"}, -- Name, Id, Class, Gender, Type, msgpack.pack(contents), curCombo, extraInfo, atListData, expression, expressionTxt, sticker, profileInfo_U, xianFanStatus

	{"UpdateHousePoolGridNum", "sI"}, -- houseId, poolgridnum
	{"SwitchFishingPannel", "b"}, -- isOpen
	{"MarkFishAwarded", "I"}, -- fishIdx
	{"RequestEditPoolSuccess", ""}, -- 

	{"ZuiMengLu_FindThread", "I"}, -- ThreadId
	{"ZuiMengLu_ReadThread", "I"}, -- ThreadId
	{"ZuiMengLu_TryRewardResult", "Ib"}, -- GroupId, bSuccess
	{"ZuiMengLu_FinalRewardResult", ""}, -- 

	{"ZYJ2021HMLZ_CheckSignUpResult", "b"},
	{"ZYJ2021HMLZ_SignUpPlayResult", "b"},
	{"ZYJ2021HMLZ_CancelSignUpResult", "b"},
	{"ZYJ2021HMLZ_SyncPlayState", "IU"},

	{"SyncJiaoShanCurrScore", "I"}, -- currScore

	{"SendOlympicGambleData", "IIIdU"}, -- id, choice, count, totalBet, gambleData
	
	{"SetAddFriendLvThreashold", "I"},
	{"SetIgnoreAddFriendRequest", "b"},

	{"EnableCameraDirectionBind", "dd"}, -- cameraZ, cameraY
	{"DisableCameraDirectionBind", ""},

	{"ReplyJiaNianHuaTicketSellInfo", "U"},

	{"PlayerUseFuXiLabaSuccess", "I"},

	{"SendSeaFishingPlayFishScore", "IU"}, -- finishTime, playerScoreData

	{"UpdateLingShouMaxLevel", "sII"}, --lingshouId, maxLv
	{"SetWaveStrength", "dd"}, -- height, strength

	{"YeSheng_OpenThreadWnd", "U"}, -- msgpack.pack(selectableGroupIds)
	{"SendJueDouInviteToPlayer", "sdbU"},
	{"SendGetRepairSkillInSeaFishingPlay", "I"}, -- skillId
	{"SendQieCuoInviteToPlayer", "sdbU"},

	{"SyncTempChangeSectWuxingResult", "dIId"}, -- sectId, newWuxing, oldWuXing, expireTime
	{"SyncSoulcoreBoomEffect", "ddII"}, -- sectId, targetPlayerId, x, y
	{"SyncDiscoverySectEntrance", "ddII"}, -- sectId, mapId, x, y
	{"SyncShowSectFaZhenEffect", "dI"}, -- sectId, wuxing
	{"SyncPlayerTurnToSoulCore", "dIbII"}, -- playerId, wuxing, bChange, posX, posY

	{"SendOpenExchangeLiuHeYeJingWnd", "II"}, -- currentQingLaiZhi remainTimes

	--梦岛暗恋
    {"PersonalSpaceSetSecretLove", "dbb"}, --targetId, bIsSuccess, bIsBoth
	{"PersonalSpaceCancelSecretLove", "db"},  --targetId, bIsSuccess

	{"SyncAddFakeSectEntrance", "dIIIbd"}, -- sectId, mapId, x, y, bXiePai, leftTime
	{"OpenSeaFishingShop", "dIIUUU"}, -- npcEngineId, shopId, poolGrade, itemList, limitTbl, buyedItemList
	{"BuyItemFromSeaFishingDecorationShopSuccess", "dIdII"}, -- playerId, poolGrade, shopId, itemTemplateId, count
	
	{"SyncZhengWuLuItemStatus", "Ibb"},

	--bgm音量
	{"SetBgMusicVolume", "db"}, -- sceneVoiceValue, restore
	{"SendGuildMemberInfoAfterOperation", "dUss"}, -- memberId, guildMemberInfoRpcUD, requestType, paramStr
	{"RemoveGuildMemberInfoAfterOperation", "dss"}, -- memberId, requestType, paramStr
	{"RemoveGuildMemberApplyInfoAfterOperation", "dss"}, -- applyPlayerId, requestType, paramStr

	{"SendPoolInfo", "Ub"}, -- Furniture2:SaveToString, IsMingyuan
	{"SendAllGuildsCreationAlert", "b"}, -- alertStatus
	{"TrackToDoSectSchoolTask","I"},
	{"SyncRecommendIgnoreBigData", "b"}, -- status

	-- 摘星
	{"SyncZhaiXingComponent","IU"}, --stage, components
	{"ReplyZhaiXingOpenStatus","U"},
	{"ReplyZhaiXingSuccess","I"},
	{"ReplyZhaiXingPassComponent","I"},
	{"SyncZhaiXingSpeedUp",""},
	{"SyncZhaiXingScore","Id"}, -- score, fightEndTime
	{"SyncZhaiXingSummary","III"}, -- score, timeUsed, historyBest

	{"SyncLuoChaPlayHp", "dII"},
	{"OpenLuoChaHaiShiSignUpWnd", "bIII"},
	{"SyncLuoChaHaiShiSignUpResult", "b"},

	--罗刹海市
	{"SyncLuoChaAttackerLevelInfo", "iiiii"}, --level, power, upgradeNeedPower, learnedSkillCount, CanlearnSkillCount
	{"UpdateLuoChaFortPickCount", "i"},
	{"SyncLuoChaPlayerAliveCount", "i"},
	{"SyncLuoChaDefenderPickInfo", "U"},
	{"ShowLuoChaAttackerResult", "biiiUs"}, --bwin, costTime, killCount, skillTbl,playerName
	{"ShowLuoChaDefenderResult", "biiU"}, --bwin, costTime, fortPickCount, playerInfo 
	{"SyncLuoChaDefenderLevelInfo", "iiiii"}, --level, learnedSkillCount, canLearnedSkillCount, maxPickCount, upgradeNeedPick
	{"SyncLuoChaCanLearnSkillInfo", "U"},

	-- 卷轴任务
	{"OpenScrollTaskWnd", "IIsI"}, -- openType, subtitleId, pictureKey, layout
	{"CloseScrollTaskWnd", "I"}, -- closeType
	{"ShowScrollTaskSubTitle", "IsI"}, -- subtitleId, pictureKey, layout
	{"HideScrollTaskSubTitle", "Is"}, -- subtitleId, pictureKey
	{"SetNextLoadingPicToScroll", "IsI"}, -- subtitleId, pictureKey, layout
	{"SetNextLoadingPicToCurrentScroll", ""},
	{"CancelSetNextLoadingPicToScroll", ""},
	{"ScrollTaskCaptureScreen", "s"}, -- pictureKey
	{"ScrollTaskChangeLayout", "IsI"}, -- subtitleId, pictureKey, layout
	{"WaitForShowScrollTaskSubTitle", "I"}, -- subtitleId
	{"PlayScrollTaskQTE", "Id"}, -- qteId, duration
	{"StartScrollTaskGameEvent", "Is"}, -- taskId, eventName
	{"SaveCombDataSuccess", "IIII"}, -- taskId, material, decoration, shape
	{"PlayCombPicture", "IIIIIII"}, -- taskId, material, decoration, shape, duration, width, height
	{"ForceSetBgMusic", "sbI"}, -- eventName, showLyric, lyricId
	{"StopBGMusic", ""}, -- 
	{"StartMusicRun", "I"}, -- musicId
	{"StopMusicRun", "I"}, -- musicId
	{"OpenTaskWipeScreenTearsWnd", "I"}, -- taskId
	{"StartClickScreenLightning", "I"}, -- taskId
	{"StartClickScreenLightning2", "I"}, -- taskId

	-- 竞技场
	{"SendArenaInfo", "IIIIIbIIIU"}, -- myMaxRankScore, myArenaPlayScore, weeklyBattleTimes, weeklyWinTimes, weeklyScoreRewardState, matching, matchType, weeklyGetShopScore, weeklyGetShopScoreLimit, BattleRecord
	{"SyncArenaSignupState", "bI"}, -- matching, matchType
	{"SendArenaClassRecord", "U"}, -- classrecord
	{"SendArenaScoreShopData","IIUU"}, -- shopScore, nextAutoRefreshTime, goodsLimitData, goodsData
	{"SendArenaRankInfo", "IIIIIIIU"}, -- seasonId, rankType, page, totalPage, myScore, myEquipScore, myRank, rankInfo
	{"SyncWeeklyArenaScoreRewardState", "IIII"}, -- weeklyBattleTimes, weeklyWinTimes, rewardState, currentShopScore
	{"SendArenaPlayResult", "IIiIIIIU"}, -- matchType, winType, changeRankScore, newRankScore, addShopScore, totalShopScore, winComboTimes, playerData

	{"SyncYangyangHousePool", "U"}, -- furnitureProp2

	{"YinXianWan_QueryFurnitureInfoResult", "Id"},
	{"SyncSkinColor", "II"},

	{"SetDuoMaoMaoMingYuanInfo", "b"},

	{"UpdateGamePlayStageInfo", "IIsSb"}, -- gameplayId, specifiedId, title, content, autoSwitch

	--天降宝箱
	{"TianJiangBaoXiang_SyncPlayState", "bdIIIb"}, --isPlayOpen, startTime, totalAddKeysCount, totalKeys_ChiTong, totalKeys_FeiCui, isConsume
	{"TianJiangBaoXiang_LevelOne", "bUUS"}, --isRare, keyNumTbl_U, openTimesTbl_U, context
	{"TianJiangBaoXiang_LevelTwo", "IUbdU"}, --KeyType, rewardTbl_U, canEnterBossPlay, refreshYuanBao, openTimesTbl_U
	{"TianJiangBaoXiang_Reward", "I"}, --itemTemplateId
	{"TianJiangBaoXiang_ReachLimit", ""}, 

	-- 万圣节游花车
	{"UpdateHuaCheMapPos","III"},	--huaCheEngineId, posx, posy
	{"SyncHalloweenYouHuaChePlayInfo","I"},	--stage

	{"RequestQueryTopFisher_Result","U"},	--topfisher_ud

	-- 万圣节结良缘
	{"OpenJieLiangYuanSignUpWnd", "bI"}, -- bSignUp, dailyAwardTimes
	{"SyncJieLiangYuanSignUpResult", "b"}, -- bSignUp
	{"JieLianYuanNotifyNeiGui", "d"}, -- playerId
	{"JieLiangYuanSyncPlayInfo", "IbU"}, -- stage, isNeiGui, puzzleListUD
	{"JieLiangYuanSyncVoteNeiGuiInfo", "dUdbbdibbb"}, -- expireTime, voteInfoUD, votePlayerId, voteConfirmed, voteEnd, neiGuiPlayerId, result, canGetAward, awardGot, forceOpen
	{"JieLiangYuanGetAwardSuccess", ""},

	{"Double11DSG_QueryTaskResult", "UII"}, -- msgpack.pack(taskTbl), leftFreeTimes, acceptTimes
	{"Double11KYD_SyncPlayState", "IU"}, -- playState, msgpack.pack(eventTbl)

	{"ShowCastingWnd", "dI"},
	{"RequestUsePresentItemMustBeFriend","sdi"},
	{"AddNpcFriend", "II"},
	{"DelNpcFriend", "II"},
	{"SetNpcChatGroupFinished", "III"},
	{"SetNpcChatEventFinished", "III"},
	{"SendNpcChatMsgId", "II"},

	{"SyncPrepareSendSectItemRedPackInfo", "IIUU"}, -- remainJadeLimit, remainSendTimes, lastSendItems, dynamicOnShelfItems
	{"SendSectItemPackOverview", "U"},
	{"SendSectItemPackDetail", "IdssIIIIIIU"}, -- packId, ownerId, ownerName, content, gender, class, jade, state, finishPlayerCount, totalCount, detail
	{"SendSectItemPackLotteryInfo", "dsIIIbIIU"}, -- currentLotteryPlayerId, currentLotteryPlayerName, myStatus, myQueuePos, playAnimate, animateFinishTime, animateStopPos, lotteryInfoUD
	{"SendWinSectItemPack", "dsIIb"}, -- ownerId, ownerName, itemId, itemCount, isBest
	{"SendConfirmJoinLottery", "dIdsI"}, -- sectId, packId, ownerId, ownerName, duration
	{"PlayerSendSectItemHongBao", "IdsIIiiidIs"}, -- channelId, playerId, playerName, class, gender, expression, expressionTxt, sticker, sectId, packId, contents
	{"SendSectItemRedPackHistory", "IdIdU"}, -- sendTimes, sendTotalJade, recvTimes, recvTotalJade, historyUD

	{"XinShengHuaJi_AddPitureFailed", "SU"}, -- reason, DetailInfo_U
	{"XinShengHuaJi_AddPitureSuccess", "IU"}, -- time, data_U
	{"XinShengHuaJi_DelPitureSuccess", "I"}, -- time
	{"XinShengHuaJi_QueryPictures", "U"}, -- time->data
	{"XinShengHuaJi_AddTaskPic", "IU"}, -- taskId, data_U
	{"XinShengHuaJi_ShowTaskPic", "USII"}, -- data_U, context, taskId, duration
	{"XinShengHuaJi_SetHuaJiNum", "I"}, -- num
	{"XinShengHuaJi_ReadPictureSuccess", "I"}, -- time
	{"XinShengHuaJi_Unlock", "II"}, -- choice, sn
	{"XinShengHuaJi_UnlockFailed", "IIII"}, -- choice, templateId, lackCount, needCount
	{"XinShengHuaJi_QueryRankPicResult", "dSUU"}, -- id, name, pic_U, rankData_U
	{"XinShengHuaJi_VoteResult", "dd"}, -- id, voteNum
	{"XinShengHuaJi_AddRankPicResult", "b"}, -- bSuccess
	{"RuMengJi_SetState", "II"}, -- npcChoice, state
	{"RuMengJi_QueryNpcStateResult", "U"}, -- npcChoice->state

	{"SyncSeaFishingPlayEntranceStatus", "bI"}, -- isOpen, closeTime

	{"ChangeTaskVisibility", "Ib"}, -- taskId, visibility
	{"SyncSendWeddingInvitationConfirm", "dI"}, -- price, invitationType

	{"SyncHouseGrassInfo", "sU"}, --houseId, grassChgInfoUD
	{"SyncHouseGroundInfo", "sU"}, -- houseId, groundChgInfoUD
	{"SyncHouseGrassGridNum", "sII"}, -- houdeId, grassType, count
	{"EnableHouseGroundBrushType", "sI"}, -- houseId, brushType
	{"BindHouseGroundBrushType", "sII"}, -- houseId, brushPos, brushType
	{"RequestEditYardGroundSuccess", ""}, -- 
	{"EndEditYardGroundSuccess", ""}, -- 


	{"Watch_PushPlayData", "IssU"}, -- playId, sceneId, key, data_U

	{"QueryGLCGuildPosInfoResult", "IUU"},
	{"RequestGLCExchangeGuildPosSuccess", "I"},
	{"UpdateGLCExchangePosInfo", "U"},
	{"QueryForOpenGLCWndResult", "UbU"},
	{"QueryGLCJoinCrossGuildInfoResult", "U"},

	{"QueryGLCGroupScoreInfoResult", "U"},
	{"QueryGLCGroupMatchInfoResult", "IU"},
	{"QueryGLCRankMatchInfoResult", "U"},
	{"QueryGLCServerGuildInfoResult", "UI"},
	{"QueryGLCServerMatchInfoResult", "UU"},

	{"SyncChoseNewWeddingScene", "ddIbdIb"}, -- leaderId, playerId1, choice1, bSameWithLast1, playerId2, choice2, bSameWithLast2
	{"SyncChoseNewWeddingSceneCountDown", "d"}, -- endTime
	{"SyncNewWeddingPartner", "dsII"}, -- playerId, name
	{"SyncNewWeddingSceneInfo", "IIdsds"}, -- guestCount, leftTime, groomId, groomName, brideId, brideName
	{"SyncNewWeddingStageInfo", "IU"}, -- stage, extraInfoUd
	{"SyncQueryChoseNewWeddingSceneResult", "bddU"}, -- bReview, leaderId, disccount, sceneInfoUd
	{"SyncNewWeddingCurrentChoice", "dI"}, -- senderId, choiceIdx
	{"SyncConfirmNewWeddingChoice", "IIb"}, -- senderEngineId, choiceIdx, boolean_Right
	{"SyncNewWeddingChoiceConfirm", "dI"}, -- senderId, choiceIdx
	{"SyncNewWeddingCloseConversationWnd", "d"}, -- senderId
	{"SyncPartnerSkipNewWeddingPreGame", "d"}, -- senderId
	{"CheckEnterWeddingByNewInvitationResult", "sIb"}, -- sceneId, invitationType, bCanEnter, bNeedTicket
	{"SyncZhaiXingPlayStart", "d"}, -- currentScore

	{"UpateCountDownWndGamePlayName","s"}, --

	{"WashPermanentPropSuccess","IIIUU"}, --
	{"OpenNpcChatWndSuccess", "I"},

	{"Xingguan_SwitchTemplateSuccess", "i"}, -- templateId
	{"Xingguan_SyncEffectOverview", "iUUUU"}, -- activeTemplateId, allPropListUD, statusPropListUD, paramSumListUD, xingguanIdListUD
	{"SubmitItemToGuildLeagueTrainSuccess", "U"},
	{"QueryGuildLeagueTrainRankResult", "UsI"},

	{"SyncSceneWaveFx", "si"}, -- sceneId:当前场景；fxId:1/2/3

	{"SyncPlayerYuanDan2022FuDanSignUpResult","b"}, -- ismatching
	{"QueryPlayerYuanDan2022FuDanSignUpInfoResult","db"}, -- playerId, ismatching
	{"RequestGetYuanDanXinYuanRewardsSuccess", "i"}, -- index
	{"SendYuanDan2022FuDanPlayResult", "iU"}, -- result, playResultInfoUD
	{"YuanDan2022FuDanStartFuDan", "d"}, -- duration
	{"YuanDan2022FuDanStopFuDan", ""},

	{"QueryGuildChristmas2021Card_Result", "U"}, -- allcardInGuild_ud
	{"XingYuPlayerLeaveStarRange", "I"}, -- starIdx
	{"XingYuPlayerEnterStarRange", "I"}, -- starIdx
	{"QueryPlayerIsMatchingXingyu2021_Result", "b"}, -- isMatching
	{"XingYuRankResult", "II"}, -- idx, duration
	{"QueryXingYuRank_Result", "UUU"}, -- playerId, name, duration
	{"XingyuLightStar", "I"}, -- starIdx
	{"SyncXingYuLightedStars", "U"}, -- starList_ud
	{"PutChristmasCardSuccess", "II"}, -- guildId, cardId
	{"QueryOtherChristmasCardInGuild_Failed", ""}, -- 
	{"QueryOtherChristmasCardInGuild_Result", "U"}, -- u
	{"GetChrismas2021CardGiftSuccess", "II"}, -- cardId, giftItemTempId

	{"SyncYuanDan2022PlayInfo", "iU"}, -- 副本阶段 ,playInfoUD,战况
	{"QuerySchoolRebuildingProgress_Result", "UU"}, -- progress_ud, progressPerDay_ud

	-- 兵器谱第二届
	{"BQPQuerySubmittedResult", "UUd"}, -- submittedUd, thresholdUd, threshold
	{"BQPQueryTopTenRankResult", "IIIIUU"}, -- equipmentType, equipmentSubType, rankType, eachDataLen, rankUd, selfCommittedTypesUd
	{"BQPQueryPreciousRankResult", "U"}, -- rankUd
	{"BQPSubmitEquipPrecheckDone", "sIsb"}, -- equipId, tag, discription, bPreciousRank
	{"BQPQueryPraiseLeftTimesResult", "II"}, -- leftTimes, praiseTimes
	{"BQPPraiseDone", "sI"}, -- equipId, leftTimes

	{"GlobalMatch_CheckInMatchingResult", "dIbs"},
	{"GlobalMatch_CheckInMatchingResultWithInfo", "dIbSU"},
	{"GlobalMatch_SignUpPlayResult", "Ib"},
	{"GlobalMatch_SignUpPlayResultWithInfo", "IbU"},
	{"GlobalMatch_CancelSignUpResult", "Ib"},
	{"GlobalMatch_UpdatePlayStatus", "IbbU"}, --playId, bIsPlayOpen, bIsPlayOpenMatch, ExtraInfo_U

	{"ShengXiaoCard_SyncPlayState", "IUIIdUUUIUdb"}, -- state, PlayerInfos_U, Round, SubRound, LoserId, ShowInfo_U, GuessInfo_U, PlayerIn2Profile_U, context, PreparePlayers_U, expiredTime, IsReportRank
	{"ShengXiaoCard_SyncGuaji", "db"}, -- playerId, Guaji
	{"ShengXiaoCard_SyncJoin", "dU"}, --playerId, playerProfile_U
	{"ShengXiaoCard_SyncLeave", "d"}, --playerId
	{"ShengXiaoCard_SyncPrepare", "db"}, --playerId, bPrepared
	{"ShengXiaoCard_SyncInteract", "ddI"}, --srcId, targetId, InteractType

	{"InitGuildLeagueWatchHeadWndInfo", "IU"},
	{"SyncGuildLeagueTowerCountToWatcher", "II"},
	{"SyncGuildLeagueBossHpInfoToWatcher", "U"},
	{"SetCameraRYD", "dddb"}, -- rotation, yAngle, distToPlayer, bInstant
	
	{"ShowNewWeddingPreGameRule", "I", nil, nil, "lua"},
	{"SetSceneDecorationId", "I"},

	{"SeasonRank_QueryRankResult", "IIUU"},

	{"ShangShiSendCookbookData", "IIU"}, -- ingredientPackCount, lastCookbookId, cookbookInfo
	{"ShangShiSendMyFoodIngredient", "U"}, -- ingredientInfo
	{"ShangShiSendOpenIngredientPackResult", "U"}, -- ingredientInfo
	{"ShangShiSendCookingResult", "IbI"}, -- cookbookId, isSucc, star
	{"ShangShiSendRetrieveIngredientSucc", "II"}, -- ingredientId, remainCount

	{"XinShengHuaJi_NotifyClient", "S"},

	{"SubmitSchoolRebuildMeterial_Success", ""},

	-- 寒假活动2022
	{"SyncTianShuoData", "IIIIIIIU"}, -- level, jingcui, state, rewardedLevel, rewardedVip1Level, rewardedVip2Level, rewardLevelLimit, taskUD
	{"TianShuoLevelRewardResult", "IIIIIU"}, -- level, vip1Level, vip2Level, state, rewardLevelLimit, itemsUD
	{"SyncTianShuoHasReward", "bb"}, -- hasLevelReward, hasTaskReward
	{"OpenXueJingKuangHuanSignUpWnd", "bI"}, -- bSignUp, dailyAwardTimes
	{"SyncXueJingKuangHuanSignUpResult", "b"}, -- bSignUp
	{"SyncXueJingKuangHuanPlayInfo", "U"}, -- infoUD
	{"ShowXueJingKuangHuanResultWnd", "IIII"}, -- rank, finalLength, maxLength, historyMaxLength

	{"HLPY_SyncRescueProgress", "Ib"}, -- progress, bExistsEnemy
	{"HLPY_EnterRescueRadius", "I"},
	{"HLPY_LeaveRescueRadius", ""},
	{"QueryGuildLeaguePlayerPosResult", "U"},

	{"UpdateLingShouExItemUsedTime", "dsII"}, --playerId, lingshouId, exChengzhang, exProps
	{"SyncUnLockPaiZhaoLvJing", "s"}, -- data
	
	{"ShowNewWeddingBaiTangButton", "I"}, -- nextProgress

	{"SyncMonsterAlwaysShowNameBoard", "II"}, -- engineId, isShow

	{"XZNW_RewardSuccess", "I"}, -- taskId
	{"XZNW_QueryRewardResult", "U"}, -- rewardData_U
	
	{"NewWeddingSyncFillInDiskProgress", "III"}, -- furnitureId, itemId, progress
	{"BQPQueryShareCostResult", "sd"}, -- equipId, needSilver

	{"WHCB_QueryRewardResult", "U"}, 
	{"BroadcastBQPRewardToWorld", "U"}, -- equipUd

	{"Watch_UpdateWatchStatus", "b"}, -- bIsWatching
	
	{"NewWeddingSyncEnjoyFoodResult", "IIb"}, -- furnitureId, leftTimes, bEnjoyed
	{"NewWeddingSyncXiuQiuMessage", "I"}, -- 
	{"NewWeddingHideNpc", "I"}, -- engineId
	{"NewWeddingSyncXiuQiuMovement", "II"}, -- fromEngineId, toEngineId

	{"Curling_SyncPlayState", "IId"}, -- playState, round, expiredTime
	{"Curling_SyncPlayerInfo", "U"}, -- playerInfo_U
	{"Curling_QueryRankResult", "US"}, -- rank_U, context

	{"PlayerEnterGamePlay", "IIU"}, -- designPlayId, sceneTemplateId, extraDataUD
	
	{"NewWeddingStartPlayCountDown", "Id"}, -- count time
	{"NewWeddingShowPlayStartFx", ""},
	{"NewWeddingShowPlayEndFx", ""},
	{"NewWeddingSyncYiXianQianSelfMatchInfo", "dsIdbb"}, -- targetId, targetName, round, roundLeftTime, bSigned, bCutOff
	{"NewWeddingSyncYiXianQianSceneMatchInfo", "U"}, -- matchUd
	{"SyncPartnerSkipNewWeddingYiXianQian", "d"}, -- playerId
	{"SyncPartnerSkipNewWeddingNaoDongFang", "d"}, -- playerId
	

	-- 元宵2022
	{"UpdateYuanXiaoRiddle2022Question", "IIIdd"}, -- questionCount, questionIdx, questionId, questionExpireTime, roundExpireTime
	{"UpdateYuanXiaoRiddle2022GuessResult", "Ibs"}, -- questionId, guessed, answer
	{"UpdateYuanXiaoRiddle2022OpenStatus", "IbII"}, -- questionId, opened, itemNum1, itemNum2

	{"OpenFireworkPartyWnd", "dUd"}, -- totalSubmittedNum, statusListUD, toSubmitNum
	{"SubmitItemsToFireworkPartySuccess", "ddd"}, -- submitNum, totalSubmittedNum, toSubmitNum
	{"UpdateFirworkPartyStatus", "IId"}, -- id, status, totalSubmittedNum
	{"NotifyPlayerBuyLiuGuangYiCaiFirework", "dsIsddb"}, -- buyerId, buyerName, serverId, serverName, submitNum, totalSubmittedNum, bShowServerName
	{"OpenConfessFireworkWnd", ""},
	{"AddConfessFireworkInScene", "Idsdsiiii"}, -- id, playerId, playerName, targetId, targetName, x, y, dir, height
	{"SetOffConfessFireworkSuccess", "ds"}, -- targetId, targetName
	{"FireworkPartyZhiBo_SyncChannelInfo", "IS"}, -- id, RemoteChannelInfo
	{"FireworkPartyZhiBo_ZhiBoEnd", ""},
	{"FireworkPartyStart", "Id"}, -- id, endTime
	{"FireworkPartyEnd", "I"}, -- id

	{"StartTargetAutoMove", "ddIId"}, -- vSpeed, hSpeed, targetX, targetY, targetDist
	{"EndTargetAutoMove", ""},
	{"SetTargetAutoMoveReverseDrag", "b"}, -- bReverse
	{"StartDirAutoMove", "ddI"}, -- vSpeed, hSpeed, dir
	{"EndDirAutoMove", ""},
	{"SetDirAutoMoveReverseDrag", "b"}, -- bReverse

	{"SyncTangYuan2022PlayInfo", "U"}, -- infoUD
	{"ShowTangYuan2022ResultWnd", "IIII"}, -- finishNum, score, rankInPlay, oldDayMaxScore
	{"SyncTangYuan2022RoundScore", "diI"}, -- playerId, roundScore, engineId
	{"UpdateTangYuan2022RoundScore", "Iii"}, -- engineId, roundScore, deltaScore
	{"PlayerGrabTangYuan", "dI"}, -- playerId, engineId
	{"PlayerReleaseTangYuan", "d"}, -- playerId

	-- 情人节2022
	{"ShowDriftBottleEditWnd", "IsII"}, -- itemTemplateId, itemId, place, pos
	{"ShowDriftBottleViewWnd", "bdsssddsIIiii"}, -- anonymous, receiverId, receiverName, content, voiceUrl, voiceDuration, senderId, senderName, senderClass, senderGender, senderExpression, senderExpressionTxt, senderSticker
	{"GenerateDriftBottleSuccess", ""},
	{"PutDriftBottleSuccess", ""},

	{"SnowballFight_SyncPlayState", "IdU"}, -- playState, expiredTime, forceInfos_U
	{"SnowballFight_SyncPlayerInfo", "U"}, -- playerInfo_U
	{"SnowballFight_RewardResult", "III"}, -- score, playResult, rewardItemId

	{"SyncGuildLeagueKillNumToWatcher", "II"},

	-- 贝塞尔摄影机
	{"BeginBezierCameraMove", "dddddddddddddIbb"}, -- x, y, z, rx, ry, rz, startTanx, startTany, startTanz, endTanx, endTany, endTanz, maxDuration, moveType, showMainUI, showSoundSettingBtn
	{"BezierCameraMoveTo", "dddddddddddddIbb"}, -- x, y, z, rx, ry, rz, startTanx, startTany, startTanz, endTanx, endTany, endTanz, maxDuration, moveType, showMainUI, showSoundSettingBtn
	{"EndBezierCameraMove", ""},

	{"SyncSetPackagePinnedItemTypeResult", "bI"}, -- equipmentFirst, itemType
	{"SyncPartnerEndNewWedding", "d"}, -- playerId
	{"NewWeddingSyncSendCandy", "s"}, -- candyId

	{"SendArenaBattleTeamInfo", "U"}, -- teamInfo
	{"RedFabaoSuitBeChanged","b"}, --changed
	
	{"NewWeddingSyncGuestSide","I"}, -- guestSide
	{"NewWeddingSyncBaiTangEndFx","III"}, -- groomEngineId, brideEngineId, stage
	{"NewWeddingSyncPregameRightAnswer","I"}, -- rightCount
	{"NewWeddingSyncGuestCount","I"}, -- guestCount
	{"NewWeddingSyncSignupDone","Ib"}, -- stage, bOk
	{"NewWeddingSyncSayContent","II"}, -- engineId, sayId

	{"QueryPlayerPos_JXYSEnemy", "U"},

	{"SeasonRank_ReportRankDataSuccess","IIU"}, -- rankType, seasonId, data_U
	{"SyncNewWeddingSkybox", "I"}, -- SkyBoxId

	{"BeginToricSpaceCamera1", "IIddddbbb"}, -- engineId1, engineId2, alpha, theta, phi, maxDuration, showMainUI, showSoundSettingBtn, noSway
	{"ToricSpaceCameraMoveTo1", "IIddddbbb"}, -- engineId1, engineId2, alpha, theta, phi, maxDuration, showMainUI, showSoundSettingBtn, noSway
	{"BeginToricSpaceCamera2", "ddddddddddbb"}, -- x, y, z, x2, y2, z2, alpha, theta, phi, maxDuration, showMainUI, showSoundSettingBtn
	{"ToricSpaceCameraMoveTo2", "ddddddddddbb"}, -- x, y, z, x2, y2, z2, alpha, theta, phi, maxDuration, showMainUI, showSoundSettingBtn
	{"EndToricSpaceCamera", ""},

	{"SendTerritoryWarPickBornRegionInfo", "IU"}, -- pickedRegion, regionGuildCount
	{"SyncTerritoryWarStatus", "II"}, -- seasonId, status
	{"SendTerritoryWarMapOverview", "dIIUU"}, -- myGuildId, observeType, currentWeek, guildInfo, randLevelGrids
	{"SendTerritoryWarMapDetail", "IdIIIIUUU"}, -- currentStage, myGuildId, battleStartTime, finishTime, curPosGridId, myGuildGridCount, favoriteGrids, grids, hasMonsterGrids
	{"StartTerritoryWarTeleportCasting", "I"}, -- castingTime
	{"SendTerritoryWarPlayInfo", "dIIIIIIIbdII"}, -- myGuildId, seasonId, weekCount, battleLevel, prepareFinishTime, finishTime, gridId, sceneIdx, isFighting, belongGuildId, belongForce, occupyNpcEngineId
	{"SyncTerritoryWarPlayProgress", "U"}, -- progresses
	{"SendTerritoryWarScoreInfo", "dIIIIUUUU"}, -- guildId, currentWeek, currentScore, gridCount, scoreSpeed, toMasterSpeed, fromSlaveSpeed, rankInfo, eventInfo
	{"SendTerritoryWarRelationInfo", "dbU"}, -- guildId, hasAlliance, relationInfo
	{"SendTerritoryWarScenePlayerCount", "U"}, -- scenePlayerCount
	{"SendTerritoryWarRankInfo", "dIIU"}, -- playerGuildId, playerGuildBattleLevel, battleLevel, rankInfo
	{"SendTerritoryWarFightDetail", "U"}, -- detail

	{"SyncNewWeddingSignupYiXianQianDone", ""},
	{"SyncNewWeddingNDFSceneInfo", "dd"}, -- groomId, brideId
	{"NewWeddingSyncYiXianQianSceneCutOffInfo", "dd"}, -- playerId, matchPlayerId

	{"SharePicToPersonalSpace", "SSd"},

	{"SetCameraRZY", "ddd"}, -- rzy
	{"ResetCameraRZY", ""},

	{"PlaySoundAtPos", "sdd"}, -- eventName, x, y
	{"QueryDouHunCrossGambleInfoResult", "IbUU"},
	{"OpenDouHunCrossGambleWnd", "I"},
	{"BetDouHunCrossGambleFailed", "IsI"},
	{"BetDouHunCrossGambleSuccess", "IsI"},
	{"DoubleBetDouHunCrossGambleFailed", "IsI"},
	{"DoubleBetDouHunCrossGambleSuccess", "IsI"},
	{"RequestGetDouHunCrossGambleRewardSuccess", "II"},
	{"QueryDouHunCrossServerGroupInfoResult", "U"},
	{"AddSkillAppearance", "II"},
	{"SetSkillAppearanceSuccess", "II"},
	{"QueryDouHunTrainRankResult", "IU"},
	{"QueryDouHunStageResult", "IIIU"},
	{"QueryDouHunCrossChampionGroupInfoResult", "U"},
	
	{"SyncPartnerStartNewWeddingPreGame", "d"}, -- senderId

	{"OpenQingMing2022PVEQuestionWnd", "dd"}, -- questionid, timeout(s) 
	{"CloseQingMing2022PVEQuestionWnd", ""},
	{"QingMing2022PVESyncScoreInPlay", "d"}, -- score
	{"QingMing2022PVEPlayResult","ddb"},-- score, todayMaxScore, newrecord
	{"QingMing2022SingleOpenBuffSelectionWnd","UIi"},-- tempBuffList{id=true,id=true}, tempSkillId,timeout

	{"SendGuildForeignAidInfo","bbIIUI"}, -- hasApplication, canApply, maxAidCount
	{"SendGuildForeignAidApplyInfo","bIU"}, -- gameplayContext
	{"SendGuildForeignAidPersonalInfo","dsIIbUI"}, -- guildId, guildName, aidType, canFightTime, hasInvite, infoUd
	{"SendGuildForeignAidInviteInfo","UI"},
	{"SendGuildForeignAidApplyStatusChange","dbI"}, -- guildId, isApply, gameplayContext
	{"SendGuildForeignAidPlayerBeAid","dI"}, -- guildId, gameplayContext

	-- meixianglou
	{"SyncSongPlayPressButton","dIII"}, -- playerId, buttonIdx, perform, score
	{"StartSongPlay","I"}, -- songId
	{"SongBeatArrived","III"}, -- songId, rhythmIdx, rhythmId
	{"SongPlayTeamResult","dddsIIdIdsIIdI"}, -- totalScore, leaderId, playerId1, name1, class1, gender1, score1, beatNum1, playerId2, name2, class2, gender2, score2, beatNum2
	{"SongPlaySingResult","ddIIIII"}, --playerId, score, perfect, good, ok, miss, maxContinueBeat

	{"ZhuErDan_SyncPlayState", "Id"},
	{"ZhuErDan_PlayDialog", "U"}, -- 

	{"GuildJuDianReplySceneOverview", "IIdUUUU"}, -- battleFieldId, currentStage, statusEndTime, notTakenSceneListUd, takenSceneListUd, supplySceneListUd, tagSceneListUd
	{"GuildJuDianSyncMapSceneInfo", "IIsUUUUUUI"}, --mapId, sceneIdx, sceneId, tagInfoUd, takenFlagInfoUd, notTakenFlagInfoUd, takenJuDianUd, notTakenJuDianUd, supplyNpcInfoUd, teleportCdRemain
	{"GuildJuDianSyncPlayerCountByMapId", "IIU"}, -- mapId, currentSceneIdx, playerCountUd
	{"GuildJuDianSyncTopFourGuildRank", "U"}, -- topGuildRankUd
	{"GuildJuDianSyncCurrentSceneProgress", "UU"}, -- progressUd, flagProgressUd
	{"GuildJuDianSyncCurrentScenePlayerCountRank", "UU"}, -- playerCountRankUd
	{"GuildJuDianSyncCurrentSceneJuDianProgress", "U"}, -- playerCountRankUd
	{"GuildJuDianSyncGlobalTopRank", "UU"}, -- topGuildRankUd
	{"GuildJuDianSyncNoneFightSceneIdx", ""},
	{"GuildJuDianSyncGuildMemberDetail", "U"}, -- memberDetailUd
	{"GuildJuDianSyncGuildMemberDetailEnd", "dIIIIIU"}, -- score, rank, totalCount, fightLevel, killCount, playerCount, activeBuffUd
	{"GuildJuDianSyncBattleFieldRank", "bUU"}, -- bShowFx, topPlayersUd, rankUd
	{"GuildJuDianSyncGuildName", "U"}, -- nameUd
	{"GuildJuDianSyncJoinQualification", "b"}, -- bCanJoin
	{"GuildJuDianSyncSetQualification", "bb"}, -- bCanTag, bCanKickoutPlayer
	{"GuildJuDianSyncGuildFlagAndJuDian", "dUU"}, -- guildId, takenFlagInfoUd, takenJuDianInfoUd
	{"GuildJuDianSyncFightLevelChange", "I"}, -- newLevel

	{"UpdateTradeSimulationData", "IU"}, -- isFinish, tradeSimulateInfo_U

	{"GetTicketForChangeChannelResult", "iS"}, -- result, urlWithTicket

	{"GaoChangCrossApplySuccess", ""},--跨服高昌报名成功
	{"GaoChangCrossInviteToStage1", ""},--高昌邀请玩家进入第一层
	{"GaoChangCrossInviteToApply", ""},--高昌邀请玩家报名
	{"GaoChangCrossQueryApplyInfoResult", "b"},--高昌查询报名结果

	{"ShenYao_SyncPlayState", "bbd"}, -- bIsPlayOpen, bIsPlayStart, playStartTime
	{"ShenYao_QuerySceneCountTblResult", "UI"},
	{"ShenYao_SignUpSucess", "dII"},
	{"ShenYao_QueryTeamPassResult", "dIU"},
	{"ShenYao_QueryPersonalNpcInfoResult", "U"},
	{"ShenYao_QueryUnlockLevelResult", "I"},
	{"ShenYao_QueryNpcEngineIdResult", "SII"}, -- sceneId, instId, engineId
	{"ShenYao_QueryRemainTimesResult", "IIII"},
	{"ShenYao_OpenPersonalNpcWnd", "IU"}, -- consumeItemId, msgpack.pack(tbl) 
	{"ShenYao_QueryRejectLevelResult", "I"}, -- lv
	{"ShenYao_OpenTeamShenYaoInfoWnd", "U"}, -- msgpack.pack(tbl)
	{"ShenYao_OpenSelectNpcWnd", "s"}, -- itemId

	{"SendArenaCrossServerRankInfo", "IIIIIU"}, -- seasonId, levelType, myScore, myEquipScore, myRank, rankInfo
	{"QueryWuYiManYouSanJieTaskInfoResult", "U"},
	{"SyncStarBiwuChampionInScene", "ds"}, -- zhanduiId, zhanduiName
	
	{"XinBaiProgress_QueryResult", "bUUU"},
	{"XinBaiProgress_QueryRewardResult", "U"},

	{"SetMonitorChannel", "U"},
	{"EnterDaGongHunBossStage", "I"},
	{"SyncDaGongHunJiTanNum", "I"},

	{"SyncAllPlayerTitleInPlay", "U"},
	{"RequestGuildTerritoryWarMemberTeleport", "dsII"}, -- leaderId, leaderName, gridId, sceneIdx

	{"SyncHuluBrotherSummary", "ddsd"}, -- selfUseTime, playerId, playerName, bestUseTime
	{"SyncHuluBrotherBossProgress", "I"}, -- killCount
	{"QueryRuYiHuluwaUnlockDataResult", "U"}, -- unlockData	
	{"QueryHuluwaTransformDataResult", "U"}, -- Ud
	{"SyncHuluwaActivityAwardTimes", "U"}, -- Ud
	{"SyncRollHuluwaAdventure", "II"}, -- value, leftTimes
	{"SyncHuluwaAdventureWndInfo", "IIIUb"}, -- huluwaId, currentPosId, leftTravelTimes, visitedListUd, bVisited
	{"SyncHuluwaBuffLevel", "II"}, -- 
	{"SyncHuluwaAdventurePopWnd", ""},
	{"SyncHuluwaAdventureVisitStatusChange", "b"},
	{"SyncHuluwaBossPosition", "III"}, -- 

	{"SendLianDanLuAlertInfo", "IIIIIb"}, -- p1, p2, p3, vip1, vip2, bAlert
	{"SyncLianDanLuPlayData", "IIIIIbU"}, --  p1, p2, p3, playData.vip1, playData.vip2, bAlert, receiveDataUD
	{"UnLockLianDanLuVipSuccess", "I"}, -- vip
	{"SendLianDanLuTaskData", "U"}, -- taskDataUD
	{"ReceiveLianDanLuRewardSuccess", "II"}, -- level, index

	{"SendPlayerCanJoinGuildTerritoryWar", "b"}, -- canJoin
	{"RemoteSummonGuildLeagueMainPlayBossSuccess", "U"},

	{"PlayRemoveLinkFx", "III"}, -- engineId, engineId2, fxId

	{"YaoBan_Unlock", "Id"}, -- yaobanId, unlockTime
	{"YaoBan_Enable", "I"}, -- yaobanId
	{"YaoBan_Disable", ""},	
	{"YaoBan_UpdateScore", "d"}, -- score

	{"BaiSheBianRen_SyncPlayState", "IdII"}, -- playState, expiredTime, score, maxScore
	{"BaiSheBianRen_UpdateSize", "Idb"}, -- engineId, scale, isScaleAtOnce

	{"GuanYaoJian_Unlock", "I"}, -- id	
	{"GuanYaoJian_RewardSuccess", ""},	

	{"ShakeCamera", "dddd"}, -- duration, shakeStrength, fadeInTime, fadeOutTime

	{"QWD_ShowNextFloorBuff", "UU"}, -- candidateBuffList, selectedBuffList
	{"QWD_SyncPlayResult", "IIIIIIb"}, -- difficulty1, difficulty2, difficulty3, floor, usedTime, score, bBreakRecord

	{"SendShanYeMiZongInfo", "bbUUU"}, -- finalRewarded, extraTaskRewarded, xiansuoInfo, chapterInfo, joinedCaiXiang
	{"SendShanYeMiZongCaiXiangInfo", "IIIU"}, -- caixiangId, myChoice, rightAnster, caixiangInfo

	{"SyncBeginnerGuideInfo", "U"}, -- beginnerGuideInfoU
	{"OpenBeginnerGuideInfoToPlayer", ""},
	{"CloseLoginDaysAward", ""},
	{"RequestBeginnerGuideTaskRewardSuccess", "IIb"}, -- taskId, postTaskId, todayAllFinish
	{"RequestBeginnerGuideFinalRewardSuccess", "I"}, -- rewardId

	{"ShuiManJinShan_LeaderRequestSignUp", "U"}, -- info_U
	{"ShuiManJinShan_MemberConfirmSignUp", "dI"}, -- memberId, buffIdx
	{"ShuiManJinShan_PopUpSuccessWnd", "UII"}, -- RewardItems_U, buffIdx, force
	{"ShuiManJinShan_SyncPlayState", "Id"}, -- playState, waterFactor
	{"ShuiManJinShan_CreateWave", "ddddd"}, -- x, y, duration, speed, height
	{"ShuiManJinShan_SyncProgress", "IIIIIb"}, -- id, progress, maxProgress, subProgress, maxSubProgress, pause
	{"ShuiManJinShan_SyncNpc", "U"}, -- npcEngineIds_U
	{"ShuiManJinShan_CheckPlayOpenMatchResult", "bS"}, -- bIsOpen, context

	{"SyncNavalWarCabinShipHp", "ddd"}, -- hp, currHpFull, HpFull
	{"SyncNavalWarShipVirtualHpChange", "III"}, -- totalDeltaHp, endTime, duration
	{"SyncNavalWarCannonCD", "IU"}, -- seatId, cdInfo
	{"SyncNavalWarMaterials", "U"}, -- {cannonCnts, boardCnt}
	{"SyncNavalSpecialMonsterHp", "Iddd"}, -- engineId, hp, currentHpFull, hpFull
	{"SyncNavalWarSeatInfo", "U"}, -- seatInfoU
	{"SyncNavalWarCabinFire", "I"}, -- fireNum
	{"SyncNavalWarStage", "I"}, -- stage
	{"NavalwarShipDriverLeaveRiding", "Id"}, -- frameId, poId
	{"NavalWarFireSkyShotGun", "IIddddU"}, -- attackerId, skillId, x, y, delay1, delay2, offsetsUd
	{"NavalShipStatusChange", "IU"}, -- shipId, statusInfoU
	{"NavalSyncShipStatus", "U"}, -- shipId2StatusInfoU
	{"QueryNavalPvpShipPosResult", "IUUU"}, -- bossGenTime, goldShipInfoU, huntingShipInfoU
	{"GetNavalShipNamesReturn", "U"}, -- shipId2NameU
	{"SyncNavalWarShipId2EngineId", "IU"}, -- shipId, shipId2MonsterEngineIdU
	{"OpenNavalOnEnterSelectSeatWnd", "II"},
	{"NavalWarDoCreateShipCannon", "IdIdddIU"}, -- frameId, poId, cannonId, x, y, dir, cannonFloor, launchInfoUd
	{"NavalWarShowImpMessage", "IU"},
	{"RequestNavalSailorFishingSuccess", "dU"}, -- playerId, deltaTblUd

	{"SetGuildForeignAidTypeSuccess", "dI"},
	{"SendGuildLeagueCrossGambleInfo", "UUIU"},
	{"BetGuildLeagueCrossGambleFailed", "IIII"},
	{"SendGuildLeagueCrossGambleRecord", "IUU"},
	{"SendGuildLeagueCrossGambleOpenType", "U"},
	{"SendGuildLeagueCrossGambleResult", "I"},

	{"SendShanYeMiZongNewXianSuo", "I"}, -- xiansuoId
	{"ShanYeMiZongGetChapterRewardSucc", "I"}, -- chapterId
	{"SendHuanHunShanZhuangShengSiMen", "bU"}, -- isShow
	{"SendShanYeMiZongFinalReward", "bbbbU"},
	{"ShowShanYeMiZongZhenXiong", ""},

	{"FKPP_SyncTeamInfo", "U"}, -- teamInfo_U
	{"FKPP_StartPlay", ""},
	{"FKPP_StartChangeRhythm", "I"}, -- rhythmId
	{"FKPP_EndChangeRhythm", "I"}, -- rhythmId
	{"FKPP_PlayerHitBubble", "dI"}, -- playerId, newContribution
	{"FKPP_TeamScore", "II"}, -- teamId, score
	{"FKPP_SyncResult", "IIIU"}, -- win, addPoint, dailyPoint, teamInfo_U

	{"SyncQmpkDpsInfo", "ddss"}, --attackDps, defendDps, attackNme, defendName
	
	{"SendMsgToGuildLeagueWatcher", "ISUS"},
	{"SyncPlayerHpToWatcher", "U"},
	{"SendGuildLeagueCrossExchangePosRecord", "U"},
	{"SendGuildLeagueCrossHistoryRankInfo", "IU"},

	{"SendHuanHunShanZhuangPlayTimes", "III"}, -- tiaozhanTimes, zhuzhanTimes, hardModeTimes
	{"SendHuanHunShanZhuangPlayResult", "bbIUI"}, -- isWin, isHardMode, duration, memberInfo, rewardType
	{"SendGuildLeagueCrossServerMatchRecord", "IU"},

	{"SendGnjcDianZanData", "IIUU"}, -- playId, serverGroupId, allData, myData
	{"SendGnjcZhiHuiSignUpData", "U"}, -- dataUD
	{"SendGnjcEvaluateZhiHuiAlert", "dsIII"}, -- playerId, playerName, playerClass, playerGender, showTime
	{"SyncGnjcZhiHuiInfo", "bddI"}, -- bOpen, attackZhiHuiPlayerId, defendZhiHuiPlayerId, status
	{"SyncGnjcZhiHuiFaZhenProgress", "II"}, -- currentProgress, totalProgress
	{"DianZanGnjcOtherPlayerSuccess", "IId"}, -- playId, serverGroupId, targetId
	{"CancelDianZanGnjcOtherPlayerSuccess", "IId"}, -- playId, serverGroupId, targetId
	
	{"SetQmpkAntiProfessionDataSuccess", "s"}, --str

	{"SyncStarBiwuCurrentSeasonChampionInfo", "IdsU"}, -- currentSeason, leaderId, teamName, memberInfoUd
	-- roller coaster
	{"RequestRollerCoasterRunSuccess", "dIIIU"}, -- playerId, cabinFId, cabinTId, stationFId, playerIdsUd
	{"RunRollerCoasterEnd", "d"}, -- playerId
	{"RequestCheckContinueRollerCoaster", "IU"}, -- furnitureTempId, playerIdsUd
	{"RequestRollerCoasterStopSuccess", "ddIIb"}, -- playerId, driverId, furnitureId, furnitureTempId, bAuto
	{"RequestRollerCoasterResumeSuccess", "dII"}, -- playerId, furnitureId, furnitureTempId
	{"RequestRollerCoasterSpeedUpSuccess", "ddII"}, -- playerId, driverId, furnitureId, furnitureTempId
	{"SyncRollerCoasterProgress", "dIIId"}, -- driverId, cabinFId, pathIndex, segmentPercent, speed
	{"BatchSyncRollerCoasterProgress", "U"},

	{"SyncHuanHunShanZhuangPlayStageChange", "Ib"}, -- stage, isShowFx
	{"SyncMiWuLingFogClear", "bb"}, -- isShowFx, showFog

	{"StartTeamConfirm", ""},

	{"SyncGnjcZhiHuiFaZhenPosition", "IIII"}, -- x1, y1, x2, y2
	{"StartGnjcZhiHuiGuide", ""},

	{"ShowActiveSYMZBuyHuoLi", ""},
	{"SendQmpkCreateRoleInfo", "UU"}, -- dataUD, allServerData

	{"SyncPlayerSeeSYMZPick", "bII"}, -- isSaw, pickId, pickEngineId

	{"WuLiangShenJing_SetChuZhan", "U"}, -- chuzhan_U
	{"WuLiangShenJing_SyncHuoBan", "U"}, -- huobanInfo_U
	{"WuLiangShenJing_SyncGates", "U"}, -- gates_U
	{"WuLiangShenJing_ApproachGate", "IUUU"}, -- engineId, gate_U, huoban_U, chuzhan_U
	{"WuLiangShenJing_LeaveGate", "I"}, -- engineId
	{"WuLiangShenJing_ApproachElevator", "IU"}, -- engineId, unlockTbl_U
	{"WuLiangShenJing_LeaveElevator", "I"}, -- engineId
	{"WuLiangShenJing_QueryHuoBanDataResult", "UUII"}, -- HuoBan_U, chuzhan_U, SkillScore, InheritScore
	{"WuLiangShenJing_UpgradeSkillSuc", "III"}, --huobanId, skillCls, nextLevel
	{"WuLiangShenJing_UpgradeInheritSuc", "II"}, --huobanId, nextLevel
	{"WuLiangShenJing_ResetHuoBanSkillSuc", "II"}, --huobanId, skillCls
	{"WuLiangShenJing_ResetHuoBanInheritSuc", "I"}, --huobanId
	{"WuLiangShenJing_SetSkillScore", "I"}, --score
	{"WuLiangShenJing_SetInheritScore", "I"}, --score
	{"WuLiangShenJing_QueryRecentPassResult", "IU"}, --playId, data_U
	{"WuLiangShenJing_PlaySuc", "Ub"}, --data_U
	{"WuLiangShenJing_QueryHuoBanPropResult", "IU"}, --huobanId, data_U
	{"WuLiangShenJing_QueryHuoBanPropResultWithNextLv", "IUU"}, --huobanId, data_U, dataNextLv_U

	{"SetGuildCommanderForAID", "dsb"}, -- commanderPlayerId, commanderPlayerName, bIsReconnect
	{"ResetGuildCommanderForAID", ""},

	{"ReplySetBackPendantHide", "I"}, -- value
	{"ReplyQmpkZongJueSaiOverView2", "IU"}, -- isFinish, dataUD

	{"SendShiTuShiMenHasReward", "bb"}, -- hasReward
	{"SendShiTuShiMenInfo", "bsIIIbU"}, -- isTuDi, name, level, exp, qingyi, hasWenDaoReward, playerInfo
	{"SendShiTuShiMenNews", "bU"}, -- isTuDi, news
	{"SendRecommendShiFu", "bU"}, -- succ, shifu
	{"SendRecommendTuDi", "bU"}, -- succ, tudi

	{"SFEQ_SyncStage", "I"}, -- stageId
	{"SFEQ_SyncOptionInfo", "U"}, -- optionData_U
	{"SFEQ_ChangeOptionText", "I"}, -- textId
	{"SFEQ_OnOptionCountDownTick", "I"}, -- countDown
	{"SFEQ_PlayerClickOption", "dI"}, -- playerId, optionId
	{"SFEQ_StartConfirmOption", "III"}, -- optionId, countDown, textId
	{"SFEQ_CancelConfirmOption", "II"}, -- countDown, textId
	{"SFEQ_EndSelectOption", "I"}, -- optionId
	{"SFEQ_WPFL_PlayerOpenWnd", "d"}, -- playerId
	{"SFEQ_WPFL_SyncClickOrReleaseItemResult", "Ibd"}, -- posId, bCanClick, playerId
	{"SFEQ_WPFL_PlayerCloseWnd", "d"}, -- playerId
	{"SFEQ_WPFL_PutItemSuccess", "I"}, -- posId
	{"SFEQ_WPFL_EndPlay", ""}, -- 
	{"SFEQ_XW_UpdateDistance", "I"}, -- distance
	{"SFEQ_XW_SyncPickNum", "I"}, -- pickNum
	{"SFEQ_XW_SyncTargetPos", "II"}, -- targetX, targetY
	{"SFEQ_SyncEndingResult", "IU"}, -- endingId, playerInfo_U

	{"SubmitQmpkCertificationInfoSuccess", "ss"}, -- name, id
	{"ShowQmpkCertificationInfoAlert", ""},

	{"UpdatePlayerGSNZRoleInfo", "U"},
	{"SyncGSNZPlayInfo", "I"}, -- process
	{"ShowGSNZResultWnd", "IIIU"}, -- winForce, force, playerRewardTime, playerInfosUd

	--- Ghost Teleport
	{"GhostDoTeleport", "ddsIIII"}, -- bsId, playerId, ip, port, px, py, pz
	{"UpdateGhostObjectMap", "dI"}, -- bsId, engineId
	{"GhostTeleportReplaceEngineId", "dI"}, -- bsId, engineId

	{"GhostTeleportShowFakePlayerToPlayer","dIsccIdUHIsU"},-- bsId, engineId, uint engineId,string name, sbyte gender, sbyte objClass, int state, double dir, byte[] appearanceData, ushort level, uint titileId, string titleName, byte[] statusProp
	{"GhostTeleportShowFlagToPlayer","dIIIIUdIIII"}, -- bsId, uint engineId, uint templateId,uint actionState,uint ownerEngineId, statusProp, dir, px, py, pz, lifeTime
	{"GhostTeleportShowBulletToPlayer","dIIIIU"}, -- bsId, uint engineId, uint templateId,uint actionState,uint ownerEngineId, statusProp
	{"GhostTeleportShowItemToPlayer","dIIbIiiibdbiibbbsb"},--bsId, uint engineId, uint templateId, bool isDropping, uint dropObjectEngineId, int equipColor, int wordCount,bool straightDrop, double originalHeight,isfake,displayColor,holeNum,isNiTian,isPrecious,pickAble,specialDisplayName, bCannotBeAutoPick
	{"GhostTeleportShowLingShouToPlayer","dIIsIdsIdddIIIIUIUUIII"},	--bsId, uint engineId, uint templateId, string id, uint actionState, double dir, string name, uint ownerEngineId, double hp, double currentHpFull,double hpFull,uint pixiangTemplateId,uint quality,uint evolveGrade,uint pixiangEvolveGrade, statusProp, ResourceId, marryInfo, babyInfo, babyTempId1, babyTempId2, babyTempId3
	{"GhostTeleportShowMonsterToPlayer","dIIIdUIHddsUbUUIsIdU"},--bsId, uint engineId, uint templateId, uint actionState, double dir, byte[] appearanceData, ushort level, double benefitCharacterId, double ownerPlayerId name, statusProp, bool isFake, byte[] fakeAppearData, extraData, specialTitleId, specialTitleStr, zuoqiTemplateId, driverId, passengerIdList
	{"GhostTeleportShowNpcToPlayer","dIIIdssIIdbUUsIUdHd"}, --bsId, uint engineId, uint templateId, uint actionState, double dir, string name, string titile, uint headFx, uint fxId, double ownerPlayerId, bool isFake, byte[] fakeAppearData, byte[] statusData, portraitName, zuoqiTemplateId, passengerIdList, EmbracePlayerId, ushort level, driverId
	{"GhostTeleportShowPetToPlayer","dIIIdIbdddsbUIIIsU"}, --bsId, uint engineId, uint templateId, uint actionState, double dir, uint ownerEngineId, bool ownerIsFemale, double hp, double currentHpFull, double hpFull, string ownerName, isShadow, appearProp, class, gender, titleId, titleName, statusProp
	{"GhostTeleportShowOtherPlayerToPlayer","dIsccIdUHIsUib"}, --bsId, uint engineId, string name, sbyte gender, sbyte objClass, uint actionState, double dir, byte[] appearanceData, ushort level, uint titileId, string titleName, string statusProp, serverGroupId, hasFeiSheng
	{"GhostTeleportShowPickToPlayer","dIIdIsI"}, -- bsId, engineId templateId dir actionstate name, resourceId
	{"GhostTeleportShowTempleToPlayer","dIIdI"},-- bsId, engineId templateId dir actionstate
	
	{"GhostTeleportReplaceMainPlayerEngineId", "I"}, -- engineId
	{"GhostPlayerBeginEnterScene", "sU"}, -- idsUD
	{"GhostPlayerTeleportEnd", ""}, --

	{"StopReadGacNetwork", "IsI"}, -- gasid, ip, port

	{"BeginShootBirdsInPlay", "dddd"}, -- threshold, birdLength, birdWidth, flySpeed

	{"SendQueryQmpkPlayerInfoResult_NotExist", "d"}, -- targetPlayerId
	{"SendQueryQmpkPlayerInfoResult_Offline", "d"}, -- targetPlayerId
	{"SendQueryQmpkPlayerInfoResult_Online_Begin", "dIUUU"}, -- targetPlayerId, class, qifuWordInfo, fishWordInfo, effectFishWordInfo
	{"SendQueryQmpkPlayerInfoResult_Online_Equipment", "dIsU"}, -- targetPlayerId, pos, itemId, itemInfo
	{"SendQueryQmpkPlayerInfoResult_Online_LingShou", "dsU"}, -- targetPlayerId, lingshouId, lingshouInfo
	{"SendQueryQmpkPlayerInfoResult_Online_End", "dIUU"}, -- targetPlayerId, wuxing, antiprofessionInfo, otherInfo

	{"SyncHuanHunShanZHuangYinYang", "II"}, -- yinNum, yangNum
	{"SendWuZiQiInfo", "IU"},
	
	{"SFEQ_XW_EndPlay", ""},
	{"YYS_SendInvitation", "s"},  -- playerName
	{"YYS_PlantTreeSuccess", "bs"},  -- bInvitor, playerName
	{"YYS_UpdateMissionProgress", "IIIbb"}, -- type, id, value, bFinish, bClaimReward
	{"YYS_IsFirstOpenTab", "bUU"}, -- bFirstOpen, playData, dailyMissionData

	{"SendShiMenQiuXuePuInfo", "bU"}, -- isShifu
	{"SendShiMenHanZhangJiInfo", "bU"}, -- isShifu
	{"SendChuShiGiftInfo", "dsIIII"}, -- tudiId, tudiName, level, class, gender, jiyuId
	{"ModifyChuShiJiYuSucc", "dI"}, -- tudiId, jiyuId

	{"SyncGuZhengKeyStrokes", "dIU"}, -- playerId, fid, dataUd
	{"GuZhengSheetUnlockSuccess", "I"}, -- id
	{"SyncGuZhengPlaySheet", "dIII"}, -- playerId, fid, sheetId, startTime

	{"SendHasShanYeMiZongReward", "b"}, -- hasReward

	{"StartJoystickAutoMove", "dI"}, -- changeDirSpeed, changeDirInterval
	{"EndJoystickAutoMove", ""},

	{"SyncPhysicsMove", "dU"}, -- poId, ud
	{"SyncPhysicsServerTime", "dd"}, -- serverTime, clientTime
	{"SyncPhysicsStartTime", "dd"}, -- startTime, currTime
	{"CreatePhysicsObject", "IdIU"}, -- frameId, poId, controllerType, extraUd
	{"OnHitByPhysicsTrigger", "IddI"}, -- frameId, objId, bulletId, templateId
	{"CreatePhysicsTrigger", "IdIU"}, -- frameId, id, templateId, {lifeCycle, x, y, ownerid, vx, vy, extraSync}
	{"DestroyPhysicsObjectOrTrigger", "Ibd"}, -- frameId, isObject, pId
	{"FreezePhysicsObject", "IdIU"}, -- frameId, id, triggerId, templateId, countDownInfo
	{"UnFreezePhysicsObject", "Id"}, -- frameId, id
	{"AddPhysicsEffector", "IddU"}, -- frameId, id, triggerId, dataU
	{"RmPhysicsEffector", "Idd"}, -- frameId, id, triggerId
	{"ApplyPhysicsAISteering", "IdU"}, -- frameId, id, steeringInfo
	{"SyncPhysicsSnapshot", "IU"}, -- frameId, dataUd
	{"SyncPhysicsCmdQSnapshot", "IU"}, -- frameId, dataUd
	{"TeleportPhysicsObjectInScene", "Idddd"}, -- frameId, poId, x, y, dir
	{"ApplyPhysicsWorldForce", "Iddd"}, -- frameId, poId, x, y
	{"UpdatePhysicsMulSpeed", "Idd"}, -- frameId, poId, mulSpeed
	{"SyncPhysicsCmdCD", "U"}, -- cdUd
	{"SyncPhysicsSelfSnapshot", "IU"}, -- frameId, dataUd

	{"UpdateTeamMemberExtStringForGameplay", "IdsU"}, -- gameplayId, memberId, content, extraInfo

	{"QueryCompositeExtraEquipWordCostReturn", "IIsIIII"},
	{"RequestCanCompositeExtraEquipWordReturn", "IIsb"},
	{"CompositeExtraEquipWordResult","sb"}, --targetEquipId,result
	{"TryConfirmCompositeExtraEquipWordResult", "sUi"}, -- equipId, fightpropUD, tempGrade
	{"QueryDisassembleExtraEquipCostAndItemsReturn", "IIsUIb"}, -- place, pos, equipId, getItemInfosUD, cost, bSilverOnly
	{"RequestCanForgeExtraEquipResult", "bIIsU"}, -- canForge, place, pos, equipId, itemInfoUD
	{"ForgeExtraEquipSuccess", "II"}, -- newLevel, newTotalValue
	{"RequestCanForgeBreakExtraEquipResult", "bIIs"}, -- canBreak, place, pos, equipId
	{"ForgeBreakExtraEquipSuccess", "I"}, -- newBreakLevel
	{"QueryForgeResetExtraEquipCostAndItemsReturn", "IIsUI", nil, nil, "lua"}, -- place, pos, equipId, getItemInfosUD, costYuanBao
	{"ForgeResetExtraEquipSuccess", ""},
	{"ShiftBackPendant", "iii"},
	{"SyncBackPendantAppearance", "Ii"}, -- engineId, appearance
	{"UpdateBackPendantPosTemplate", "Iiii"}, -- templateIdx, xOffset, yOffset, zOffset
	{"SwitchBackPendantPosTemplateSuccess", "I"}, -- templateIdx

	{"PlayRainyEffect", "ddd"}, -- delayDuration, duration, strength

	{"XinBaiAchv_PlayerAddAchievement", "I"}, -- achievementId
	{"XinBaiAchv_SyncAchievementInfo", "UU"}, -- sharaData, markData

	{"SFEQ_XW_SyncEndTime", "d"}, -- endTime
	{"SFEQ_SyncPlayData", "U"}, -- playData
	{"SetAtmosphere", "I"}, -- skyboxId

	{"SendTerritoryWarAllianceApplyInfo", "U"}, -- applyInfo
	{"SyncShiTuCultivateInfoNew", "bdUU"}, -- isShiFu, otherPlayerId, tudiInfo, cultivateInfo
	{"SendTerritoryWarCanDrawCommand", "Ib", nil, nil, "lua"}, -- requestType, can

	{"QiXi2022_SyncActivityInfo", "bb"}, -- bIsOpen, bIsOpenPlay

	{"UpdateSkillHintFx", "Ib"}, -- skillId, isHint

	{"XinBaiAchv_ShareAchievementFail", "I"}, -- id

	{"SendGTWRelatedPlayMonsterCount", "I"}, -- monsterCount
	{"SendGTWRelatedPlayMonsterSceneInfo", "U"}, -- sceneInfo
	{"SendCanOpenGTWRelatedPlayContributeWnd", "b"}, -- can
	{"SendGTWRelatedPlayContributeInfo", "bU"}, -- isTotal, contributeInfo
	{"SyncTerritoryWarGuildCurrentRank", "I"}, -- rank
	{"AdjustCameraDefaultZY", "dd"}, -- cameraZ, cameraY


	{"MarkNewShinningHouseFishItem", "I"}, -- itemTempId
	{"RemoveShinningHouseFishItem", "I"}, -- itemTempId
	{"SendQmpkHandbookReceiveAwardInfo", "b"}, -- canReceive

	{"SendGuildTerritoryWarPlayWeeklyResult", "IIdsIIIIIIIIIU"}, -- seasonId, weekPassed, guildId, guildName, rank, guildScore, gridCount, slaveCount, playerScore, contribution, silver, exp, mingwang, itemUd
	{"SendPlayerShareCommandPicResult", "b"}, -- isSucc

	{"ActivityPannelShowReddot", "Ib"}, -- activityId, bShowReddot
	{"YYS_OnPlayerRejectInvitation", ""},
	{"SFEQ_WPFL_ClickItemFail", "I"}, -- itemId
	{"SendGTWRelatedPlayInfo", "dIIIIIdIII"}, -- myGuildId, seasonId, weekCount, battleLevel, gridId, sceneIdx, belongGuildId, belongForce, monsterCount, pickCount

	{"RequestFixBeginnerGuideDay2FinishSuccess", "I"}, -- day

	{"SendNewShiTuWenDaoInfo", "bdIUU"}, -- isShifu, partnerPlayerId, currentStage, tudiInfo, wendaoInfo
	{"QueryDouHunCrossWaiKaRankResult", "U"},

	{"GuildJuDianReplyDailySceneOverview", "UU"}, -- monsterNpcDataUD, otherDataUD
	{"GuildJuDianReplyRankInfo", "UU"}, -- centerRankData, localRankData
	{"GuildJuDianSendCanOpenContributionWnd", "b"}, -- result
	{"GuildJuDianSendContributionWeekData", "U"}, -- dataUD
	{"GuildJuDianSendContributionSeasonData", "U"}, -- dataUD
	{"GuildJuDianShowAwardWnd", "IIIsIIIIIII"}, -- season, week, guildRank, guildName, guildScore, totoalKillCount, eval, guildContri, mingwang, exp, freeSilver
	{"GuildJuDianOpenGuideMapWnd", ""},
	{"GuildJuDianOpenLingqiSubmitWnd", "I"}, -- submited

	{"JYDSY_StartNextRound", "IUbU"}, -- roundTime, barrierAndPosInfo_U, bIsOnMove, nextMoves_U
	{"JYDSY_GameEndSyncResult", "d"}, -- winnerId
	{"JYDSY_SyncRoundTime", "I"}, -- roundTime
	{"JYDSY_SyncPlayerInfo", "U"}, -- playerInfo_U
	{"JYDSY_SyncBarrierInfo", "U"}, -- boardBarrierInfo_U
	{"JYDSY_ShowBubbleMsg", "I"}, -- msgId
	{"QueryDouHunGroupAppearanceInfoResult", "U"},

	{"SendMyShouTuQuestionInfo", "sU"}, -- question, tagList
	{"SendMyBaiShiQuestionInfo", "sU"}, -- question, tagList
	{"SendGuildForeignAidApplicationStatus", "b"}, -- isOpen

	{"SyncShiTuDuLingTaskInfo", "IiI"},

	{"PrepareSendShiTuGiftRet", "bdIIsIIsIIU"}, -- isShifu, otherPlayerId, winshItemId, wishItemCount, wishContent, myWishItemId, myWishItemCount, myWishContent, remainTimes, remainJadeLimit, dynamicOnShelfItems
	{"SendShiTuGiftSucc", ""},
	{"SetShiTuGiftWishSucc", "bdIIs"}, -- isShifu, otherPlayerId, winshItemId, wishItemCount, wishContent
	{"ShowShifuPingJiaWnd", "ds"}, -- shifuId, shifuName
	{"SendFamilyTreeShiMenInfo", "dbIU"}, -- targetPlayerId, isBestTudi, shimenLevel, bestTudiIds
	{"SendShifuAllTudiInfo", "dIU"}, -- shifuId, requestType, allTudiInfo
	{"SyncSmartGMTokenRaw", "bsiS"}, --bSuccess, token, code, message
	{"SendGTWRelatedRemainExchangeMonsterHpCount", "I"}, -- count
	{"ShowShiTuJianLaJiMsg", "Ib"},
	{"SyncShiTuJianLaJiScore", "II"},
	{"ShowShiTuZhaoMiJiStartWnd", "II"},
	{"SyncShiTuZhaoMiJiScore", "II"},
	{"SyncShiTuZhaoMiJiMonsterPos", "II"},

	{"SyncUXDynamicShopShangJiaGiftItems", "IssUIIdd"}, -- event, traceId, scm, itemInfos_U, oriTotalJade, totalJade, discount, expireTime
	{"BuyUXDynamicGiftItemsSuccess", "Is"}, -- event, traceId
	{"SendUXDynamicMallHomeItems", "bssUd"}, -- bUseRecommendItems, trace, scm, itemInfos_U, expireTime

	{"UpdateItemFlag", "sIb"}, -- itemId, flag, value

	{"UpdateShiTuQingTaskFinishedTimes","II"}, --taskId,subtimes
	{"OpenShifuInviteTudiJoinShiTuTeamWnd","I"}, --taskId
	
	{"Double11MQHX_SyncPlayState", "IddU"}, -- playState, startTime, endTime, RankTbl_U
	{"Double11MQHX_BroadCastAction", "dIU"}, -- playerId, action, data_U
	{"Double11MQHX_ShowRewardWnd", "UU"}, -- Rewards_U, RankTbl_U

	{"Double11SZTB_SyncPlayState", "IddIU"}, -- playState, EnterStateTime, NextStateTime, round, traps_U
	{"Double11SZTB_ShowRewardWnd", "bdbb"}, -- bSuccess, playDuration, bReward, bNewRecord

	{"SendDayFestivalInfo", "SI"}, -- todayFestivalForClient, time
	
	{"GJZL_SyncStage", "II"}, -- stage, countDownTime
	{"GJZL_SyncProgress", "II"}, -- progress, fullProgress

	{"SendJinLuHunYuanZhanChoosableSkillInfo", "U"}, -- data_U
	{"SyncJinLuHunYuanZhanTeamChooseResult", "U"}, -- data_U
	{"SyncJinLuHunYuanZhanAllFinalSkillInfo", "sU"}, -- reason, data_U
	{"SendJinLuHunYuanZhanPlayResult", "IUU"}, -- winForce, allSkillInfo_U, evaluateInfo_U
	{"SendXiangYaoYuQiuSiRank", "IU"}, -- selfFinishTime, rankData_U

	{"InitShuiGuoPaiDuiScore", "II"},

	{"GJZL_SyncSelectPosition", "U"}, -- position2PlayerName_U
	{"GJZL_SelectPositionSuccess", "I"}, -- position

	{"UpdateCuJuPlayerPlace", "U"}, -- place2PlayerName_U
	{"SetCuJuPlayerPlaceSuccess", "I"}, -- place

	{"SyncSectInfoBegin_Ite", "dsd"}, -- sectId, infoType, extra
	{"SyncSectInfo_Ite", "dsdU"}, -- sectId, infoType, extra, Ud
	{"SyncSectInfoEnd_Ite", "dsdU"}, -- sectId, infoType, extra, extraUd
	{"SyncSectMission_Ite", "ds"}, -- sectId, mission
	{"SyncTriggerZhuoyaoGuide_Ite", ""},
	{"SyncZhuoYaoUnlockInfo_Ite", "IdUUU"}, -- zhuoyaoLevel, exp, unlockTimesUd
	{"SyncYaoGuaiCacthInfo_Ite", "IId"}, -- yaoguaiId, catchTimes, unlockTimes

	{"Halloween2022SyncAllowToUseHongBaoCover", "II"}, -- usedJade, isAllowToUse
	{"SyncTrafficLightSignUpResult", "b"}, -- isSignUp
	{"TrafficLightPlayResult", "IIIIIbb"}, -- playerStatus, rank, totalCount, participationAwardTimes, dailyAwardTimes, isGet1stAward, isGetOnceAchieveAward
	{"SyncTrafficLightNpcState", "IIIIdd"}, -- templateId, x, y, state, totalKeepDuration, leftKeepDuration
	{"SyncTrafficLightPlayerAliveNum", "I"}, -- aliveNum
	{"SyncTrafficLightPlayerFail", "I"}, -- playerEngineId
	{"SyncStartTrafficLightNpcPerform", ""},
	{"SyncTrafficLightEnd", ""},
	{"SyncEndTrafficLightNpcPerform", "bI"}, -- isLeft, count
	{"QueryPlayerIsMatchingTrafficLight_Result", "b"}, -- isMatching
	{"QueryPlayerTrafficLightAward_Result", "IIbb"}, -- participationAwardTimes, dailyAwardTimes, isGet1stAward, isGetOnceAchieveAward
	{"ProtectCampfirePlayResult", "bbbbb"}, -- isHero, isWin, heroAward, normalOnceAward, normalDailyAward
	{"ProtectFireMonsterNewWave", "I"}, -- count
	{"UpdateProtectCampfireHp", "Idddddd"}, -- engineId, hp, currentHpFull, hpFull, mp, currentMpFull, mpfull

	{"SyncGnjcTotalScoreInfo", "ii"}, -- attackScore, defendScore
	{"SendGnjcRecommendDianZanInfo", "IIU"}, -- playId, servergroupId, data_U

	{"UpdateReddotDataWithKey", "III"}, -- key, times, expiredTime

	{"GuildCustomBuildPreTaskSyncStatus", "I"}, -- status
	{"GuildCustomBuildPreTaskSyncPlayInfo", "U"}, -- playInfoUd
	{"SyncNpcCountDownProgress", "Ibdd"}, -- engineId, bDisplay, leftTime, totalTime

	{"PlayPumpkinHeadFire", "IId"}, -- engineId, fxId, duration
	{"XiangYaoYuQiuSiPlaySuccess", "III"}, -- costTime, times, mode
	{"ReplyJinLuHunYuanZhanHotSkill", "U"}, -- hotSkillData_U
	{"EnterJinLuHunYuanZhanDieStatus", "dI"}, -- playerId, engineId
	{"ReplyJinLuHunYuanZhanOpenStatus", "b"}, -- status
	{"SyncJinLuHunYuanZhanKillInfo", "UU"}, -- attackData_U, defendData_U

	{"WorldCupJingCai_UpdateAllJingCaiData", "UUUUII"}, -- allPeriodResultData_U, allRoundResultData_U, odds_U, jingCaiData_U, groupRank, taotaiStage
	{"WorldCupJingCai_OnVoteSuccess", "IsIUUU"}, -- pid, value, num, allPeriodResultData_U, allRoundResultData_U, odds_U
	{"WorldCupJingCai_SendSelfJingCaiData", "U"}, -- jingCaiData_U
	{"WorldCupJingCai_SendPeriodAndRoundData", "UU"}, -- allPeriodResultData_U, allRoundResultData_U
	{"WorldCupJingCai_ReplyTaoTaiInfoResult", "U"}, -- taoTaiInfo_U

	{"PlayTeamGameVideoWithCheckNewer", "sIbb"}, -- videoName, playerCount, canSkip, isNewPlayer
	{"SyncTeamGameVideoSkipMemberCount", "sII"}, -- videoName, playerCount, skipCount

	{"SyncStarBiwuStatusForActivity", "bI"}, -- bOfficial, status
	{"RequestCatchYaoGuaiResult_Ite", "bIIIIIIdIdId"}, -- bSuccess, yaoguaiTempId, zhuoyaoType, quality, returnItemTemplateId, returnItemCount, bravo, addexp, level, currentExp, originalLevel, originalExp
	{"SyncTriggerZhuoyaoHuaFuGuide_Ite", ""},
	{"SyncTriggerSectXiuXingGuide_Ite", ""},
	{"SyncCleanSectApply_Ite", ""},
	{"SyncRandomWuXing_Ite", "I"}, -- newWuXing
	{"SyncSectFuWenRank_Ite", "dUdI"}, -- sectId, rankUd, effectExpireTime, todayLingfu
	{"NotifySectBoomPlayStart", ""},
	{"SyncSectBoomRank_Ite", "dUd"}, -- sectId, rankUd, score
	{"SyncSectBoomInfo_Ite", "dddd"}, -- sectId, towerHp, towerFullHp, endTimestamp
	{"SyncSectXiuxingPlayStatus_Ite", "dIb"}, -- sectId, bJoined

	{"QueryBiWuCrossMatchInfoResult", "U"},
	{"QueryBiWuCrossTeamInfoResult", "U"},	

	{"SetCrtSelectedHongBaoType", "I"},	--coverType
	{"QueryBiWuCrossGroupTeamInfoResult", "U"},
	{"ShowCrossBiWuSetHeroWnd", "UII"},
	{"SendGnjcCounterattackNpcInfo", "IIII"}, -- index, force, x, y
	{"SyncBiWuFireRegion", "Ub"},

	{"SyncStarBiwuZhanduiRenqiRank", "UU"},
	{"SyncStarBiwuZhanduiDetailRank", "dUU"},
	{"SyncSectNameAndStd_Ite", "dIs"}, -- sectId, randomId, name
	{"SyncShuiGuoPaiDuiXiGuaNum", "IIb"},
	{"RemoveBiWuFireRegion", ""},

	{"SyncBingTianShiLianData", "U"}, -- data_U
	{"ShowBingTianShiLianResult", "bII"}, -- bResult, curSelect, curDifficulty

	-- 2023元宵猜谜
	{"SyncGuessRiddleResult2023", "Ib"}, -- riddleId, isCorrect

	{"RequestCCSpeakResult", "bssb"}, -- result, steamName, extraInfo, bForce

	{"Spring2023TTSY_SyncPlayState", "IddU"}, -- playState, startTime, endTime, RankTbl_U
	{"Spring2023TTSY_ShowRewardWnd", "IIU"}, -- rank, score, rewards_U
	{"Spring2023TTSY_ShowSitDownBtn", "I"}, -- engineId
	{"Spring2023TTSY_HideSitDownBtn", "I"}, -- engineId

	{"Spring2023SLTT_ShowRewardWnd", "bIU"}, -- bSuccess, duration, rewards_U
	{"Spring2023SLTT_AddOrUpdateTrap", "IddddddIId"}, -- trapId, x, y, beginRadius, endRadius, beginTime, endTime, playerBuff, monsterBuff, delta
	{"Spring2023SLTT_RmTrap", "I"}, -- trapId

	{"Spring2023ZJNH_QueryTasksResult", "U"}, -- TaskInfo_U

	{"SyncXinYuanBiDaStatus", "II"},  -- wishId, wishStatus
	{"SyncXinYuanBiDaStatusAll", "U"},  -- WishedInfo_U
	{"SyncYuanDanQiYuanProgress", "III"}, -- fuqi, xiqi, caiqi
	{"SyncYuanDanQiYuanAwardTimes", "I"}, -- awardTimes
	{"YuanDanQiYuanPlayResult", "bbI"},  -- bWin, bAward, times

	{"SetCheckSkillSetCondForPlay", "s"},  -- playName
	{"ClearCheckSkillSetCondForPlay", ""},

	-- 2022圣诞节
	{"UpdateTurkeyMatchSnowmanHp", "IIIs"}, -- snowmanTeamId, curHp, curHpFull, maxDamagePlayerName
	{"SyncTurkeyMatchPlayStatus", "I"}, -- playStatus
	{"SyncTurkeyMatchSnowballBroke", "dd"}, -- playerId1, playerId2
	{"SyncTurkeyMatchSnowballInfo", "U"}, -- SnowballInfoData_U
	{"SyncTurkeyMatchSuperLuckyStarInfo", "U"}, -- SuperLuckyStarInfoData_U
	{"SyncTurkeyMatchSuperLuckyStarKick", "I"}, -- SuperLuckyStarEngineId
	{"SyncTurkeyMatchResult", "bIIIIbI"}, -- isWin, score, rank, selfDamage, teamTotalDamage, isFirstDamager, killSnowmanTime
	{"QueryTurkeyMatchRankData_Result", "IIdU"}, -- rankPos, firstStageScore, killSnowmanTime, rankData_U
	{"QueryPlayerTurkeyMatchInterfaceData_Result", "IIU"}, -- dailyAwardTimes, firstKillAwardTimes, firstTeamInfos_U

	-- 圣诞心意 2021圣诞玩法改进
	{"SyncChristmasKindnessPlayStage", "II"}, -- playStage, leftTime
	{"SyncChristmasKindnessGiftTimes", "III"}, -- firstStageGiftTimes, secondStageGiftTimes, thirdStageGiftTimes
	{"SyncChristmasKindnessStartUI", ""},
	{"QueryCanStartChristmasKindnessGuildPlay_Result", "b"}, -- isCanStart

	{"UpdateExpressionExpireInfo", "II"}, -- group, expiredTime

	{"SyncMNeacToken", "SSS"}, -- token, iplist, portlist
	-- 跨服高昌激斗
	{"GaoChangJiDouCrossQueryApplyInfoResult", "b"},
	{"GaoChangJiDouCrossInviteToApply", ""},
	{"GaoChangJiDouCrossApplySuccess", ""},
	{"SyncGaoChangJiDouMonsterState", "IIIbb"}, -- engineId, x, y,isAlive
	{"SyncGaoChangJiDouPickState", "IIIbb"}, -- engineId, x, y,isAlive
	{"GaoChangJiDouScoreUpdate", "dI"}, -- playerId, score
	{"GaoChangJiDouStatisticStart", ""},
	{"GaoChangJiDouStatisticStop", ""},
	{"GaoChangJiDouSelfUpdate", "dIIIIII"}, --playerId, kill, killed, help, monsterkill, baoxiang, score
	{"SyncGaoChangJiDouCrossPlayInfo", "UII"}, --rankInfo_U, monsterNum, pickNum

	{"SceneSectXiuxingPlayStart", ""},
	{"SceneSectXiuxingPlayEnd", ""},

	{"WorldCupJingCai_SendPeriodRoundStageData", "UUI"}, -- allPeriodResultData_U, allRoundResultData_U, taotaiStage
	{"WorldCupJingCai_OnReceiveJingCaiAwardSuccess", ""},

	{"SyncFrameTimeToClient", "dd"},

	{"UnlockHongBaoCover", "I"},
	
	{"SyncYaoGuaiRedAlert", "b"}, -- bShow
	{"TriggerSectGetYaoguaiGuide", ""},
	{"TriggerSectPutYaoguaiToFazhenGuide", ""},

	{"PDFY_RequestShuffleBuffResult", "IIIIU"}, -- shufflePoint, shuffleTimes, shuffleType, buffLevel, tempBuff
	{"PDFY_RequestReplaceAllBuffResult", "UI"}, -- newBuff, currShuffleType
	{"PDFY_RequestSkillLevelUpSuccess", "IIIII"}, -- id, skillLevel, skillPoint, group, hasGroupPoint
	{"PDFY_RequestEquipSkillSuccess", "I"}, -- id
	{"PDFY_RequestResetSkillResult", "II"}, -- score, skillPoint
	{"PDFY_SendMainWndData", "IIUUU"}, -- lastSeasonId, thisSeasonId, rewardData, rankData, selfData
	{"PDFY_ShowNextLevelBlessing", "UUU"}, -- candidateBlessings, selectedBlessings, extraSkills 
	{"PDFY_UpdateKilledMonsterNum", "U"}, -- updatedMonsterNum
	{"PDFY_SyncPlayResult", "bIIIIIIbI"}, -- bSuccess, difficulty1, difficulty2, difficulty3, passedLevel, usedTime, score, breakRecord, rewardScore

	{"YuanDanQiYuanOnMonsterKilled", "II"}, -- yuanqiType, deltaProgress

	{"PDFY_GetMonsterRewardSuccess", "II"}, -- monsterId, rewardId
	{"PDFY_UpdataMaxDifficulty", "I"}, -- difficulty

	-- 2023寒假雪魄历险
	{"SyncXuePoLiXianPrepareInfo", "U"}, -- data_U
	{"SyncXuePoLiXianData", "U"}, -- data_U
	{"XuePoLiXianStopPrepare", ""},
	{"ShowXuePoLiXianResult", "bII"}, -- bResult, curLevelId, costTime

	{"PDFY_QueryTempSkillResult", "U"}, -- extraSkill_U

	{"NaoYuanXiaoPlayResult","IIUUUU"}, -- teamScore, todayTotalScore, playScoreInfo, remainNumTbl, totalRewardNum, rewardItems
	{"SyncNaoYuanXiaoTeamPlayInfo", "IU"}, -- teamScore, playScoreInfo
	{"SyncNaoYuanXiaoGridStatus", "UU"}, --gridInfo,gridTexture
	{"ReturnNaoYuanXiaoScoreQuery", "I"}, --todayTotalScore
	{"ReturnNaoYuanXiaoRemainRewardTimes", "U"}, --todayRewardTimes

	{"QuerySpriteGameYWTokenResult", "SdSS"}, -- token, expiredTime, context, clentUrl

	{"ReplyXuePoLiXianBattleInfo", "U"}, -- data_U

	{"PDFY_SyncShufflePoint", "I"}, -- value
	{"PDFY_SyncSkillPoint", "I"}, -- value

	{"PlayerAcceptSectApplyResult_Ite", "b"}, -- bSuccess
	{"SceneBrightnessTransition", "dd"}, -- duration, brightness

	{"NaoYuanXiaoGridStatus","U"}, -- x, y, status
	{"NaoYuanXiaoPlayerCollisionOthers","dI"}, -- playerId,dir
	{"NaoYuanXiaoPlayerOnBadGrid","d"}, -- playerId

	{"SyncXianKeLaiPlayData", "IIIIUd"}, -- progress, vip1, vip2, resetNum, rewardInfo_U, addRate
	{"SyncXianKeLaiTaskData", "U"}, -- data_U
	{"UnLockXianKeLaiVipSuccess", "I"}, -- vip
	{"ReceiveXianKeLaiRewardSuccess", "IIU"}, -- level, index, resultTbl
	{"SyncHanHuaMiBaoData", "U"}, -- data_U

	{"SyncPickResourceIdChange", "II"}, -- engineId, resourceId
	{"SyncXiuxingBoomBornPostion", "II"}, -- x, y

	{"SendValentineYWQSInfo", "dbIIIIIIU"}, -- partnerId, partnerIsOnline, loveValue, rank, todayHuoLi, expressionTaskCount, dateTaskCount, shareMengDaoCount, playerAppearanceInfo
	{"SendValentineYWQSRankInfo", "dsIIIIIU"}, -- partnerId, partnerName, partnerClass, partnerGender, loveValue, voteCount, myRank, rankInfo
	{"SendValentineYWQSInvitation", "ds"}, -- playerId, playerName

	{"SendSkillEventToBuffFx", "IIIIIIU"}, -- engineId, mainSkillId, buffId, eventId, targetPixelX, targetPixelY, extraUd
	{"QueryGuildLeagueCrossAidEvaluteInfoResult","U"},
	{"QHLanternShowStart", "II"}, --party, endtime
	{"QHLanternShowEnd", "I"}, --party
	{"QHLanternShowZhiBo_SyncChannelInfo","IS"}, --id, zhiboInfo
	{"QHLanternShowZhiBo_ZhiBoEnd",""},
	{"NaoYuanXiaoPlayerOnGoodGrid", "II"}, --x, y

	{"RequestSetSectUseEnemyForTaskResult", "db"}, -- sectId, bUse
	{"SyncSectXiuxingReliveTime", "d"}, -- nextTime
	{"SyncSectRedAlert", "db"}, -- sectId, bHasRedAlert

	{"NaoYuanXiaoPlayersGridPos", "U"}, --x, y

	{"SyncValentineYWQSTop10", "U"}, -- rankInfo
	{"SendValentineLGTMPlayInfo", "II"}, -- remainRewardTimes, remainHardModeRewardTimes
	{"StartLGTMFlyProgress", "II"}, -- rangeStart, rangeWidth
	{"PlayerDoLGTMFlySkill", "Ib"}, -- doSkillPos, isSucc
	{"SendLGTMPlayResult", "bb"}, -- isWin, isHardMode

	{"QHLanternShowChangeView","I"},
	{"NaoYuanXiaoReturnStartPos","d"}, --playerId
	{"ShowDouHunCrossChampionWnd", "IIsssI"},

	-- 人间棋局三期
	{"GuildJuDianSyncMapPlayerCountAll", "U"}, -- playerCountList
	{"GuildJuDianSyncMapLingQuanCount", "U"}, -- lingQuanCountList
	{"GuildJuDianQueryMapLingQuanPosResult", "IU"}, -- nextRefreshTime, lingQuanPosListUd
	{"GuildJuDianSendLingQiSubmitInfoNew", "U"}, -- lingQiInfoU

	{"NaoYuanXiaoCommitFx",""},
	{"GuildJuDianQueryGuildGongFengDataResult", "U"}, -- gongfengDataU

	{"SendGTWSubmitLingQiInfo", "IIU"}, -- currentHaveLingQi, levelInfo
	{"SendGTWAltarInfo", "U"}, -- altarInfo
	{"SendGTWZhanLongTaskInfo", "IU"}, -- todayTaskCount, taskInfo

	{"OpenRenewZuoQiByItemWnd", "IIsUI"}, -- place, pos, itemId, dataUD, days
	{"NaoYuanXiaoShowUI", ""},

	{"SyncCDNKeyToClient","ss"}, -- 同步cdn鉴权key

	--属性引导
	{"PlayerPropCompositionReturn", "U"}, --
	{"PlayerQueryPropRankDataResult", "U"}, --rank,playerid,value
	{"PlayerQueryPropShareResult", "bI"}, --isShared
	{"CompareOnlinePropResult", "UU"},
	{"CompareRankPropResult", "UU"},
	{"GaoChangJiDouOnPlayerPick","dI"}, -- playerId, pickEngineId

	{"QueryMonsterPosResultNew", "U"},
	{"GuildJuDianSendZhanLongTaskInfo", "U"}, -- taskInfoU
	{"PlayerQueryExtraSkillLvResult", "U"},--systemId, value

	{"SendGTWZhanLongShopInfo", "IbU"}, -- hasScore, hasPermission, shopInfo

	{"GaoChangJiDouQueryAwardResult", "s"}, --award_s
	{"QueryOnlinePlayerSharedPropResult", "dU"}, --targetId, propType_U
	{"XianKeLaiSyncPlayState", "b"}, -- isOpen
	{"SendGuildJuDianZhanLongShopInfo", "IbU"}, -- hasScore, hasPermission, shopInfo

	--新装备再造
	{"RequestCanRecreateExtraEquipReturn", "IIsb"}, -- equipPlace, equipPos, equipId, canRecreate
	{"RecreateExtraEquipResult", "sbU"},-- equipId, isSuccess, changedTbl
	{"TryConfirmRecreateExtraEquipResult", "sUi"},-- equipId, fightpropUD, tempGrade
	{"QueryRecreateExtraEquipCostReturn", "IIsIII"}, -- equipPlace, equipPos, equipId, costYinLiang, costItemId, costItemCount

	{"SyncServerMetaSpanInfoBegin", "II"},
	{"SyncServerChunkMetaSpanInfo", "iiU"},
	{"SyncServerMetaSpanInfoEnd", "II"},

	{"SendGTWChallengeMemberInfo", "IIIIbbU"}, -- onlineMemberCount, totalMemberCount, onlineEliteCount, totalEliteCount, hasPermission, isChallenging, memberInfo
	{"SendSetGTWChallengeEliteResult", "dbIIII"}, -- targetPlayerId, isElite, onlineMemberCount, totalMemberCount, onlineEliteCount, totalEliteCount
	{"SendGTWChallengeBattleFieldInfo", "dIIIUU"}, -- myGuildId, challengeIdx, bossIdx, remainTime, bossInfo, passGuildInfo
	{"SendGTWChallengePlayInfo", "IIIb"}, -- challengeIdx, currentBossIdx, maxBossIdx, isCurrentBossDie
	{"SendGTWChallengeFightDetailInfo", "IUId"}, -- detailType, detail, playerCount, statTotal

	{"GaoChangJiDouNoTeamMember", "d"}, -- playerId
	{"GaoChangJiDouMengMianPlayers", "Ub"}, -- otherPlayerIds, isFirstEnter

	{"QingMing2023SyncLevelList", "IIUb"}, -- currentLevelIndex, currentLevelId, levelListU, bShowRule
	{"QingMing2023SyncPlayRankInfo", "U"}, -- playerRankListU
	{"QingMing2023PVPSendFinalResult", "IIIIU"}, -- rank, score, engageAward, winnerAward, playerDetailsU
	{"QingMing2023SyncSubPlayInfo", "IU"}, -- subplayId, playerSubPlayInfoU
	{"QingMing2023BurningGroundSyncFireBlocks", "bU"}, -- isReal, blocksDataU
	{"QingMing2023PlayerArriveLevel", "II"}, -- level, arrivalOrder
	{"QingMing2023TrapTriggered", "II"}, -- x, y
	{"QingMing2023MoveToStartingPoint", ""}, -- 

	{"SendQueryEquipInfoResult_Online", "sU"},

	{"Anniv2023FSC_SyncPlayResult", "IIbU"}, -- score, result, bNewRecord, extraData_U
	{"Anniv2023FSC_QueryRankDataResult", "UUU"}, -- rankData, selfData, npcData
	{"Anniv2023FSC_SyncNearByPlayers", "Ud"}, -- inviteList_U, lastInviteExpiredTime
	{"Anniv2023FSC_InviteSuccess", "d"}, -- currentInviteExpiredTime
	{"Anniv2023FSC_SendInvitation", "dsd"}, -- inviterId, inviterName, currentInviteExpiredTime
	{"Anniv2023FSC_OnInvitationRejected", "dsd"}, -- playerId, playerName, rejectExpiredTime
	{"Anniv2023FSC_SyncPlayerStatus", "II"}, -- index, status
	{"Anniv2023FSC_OnSelfUseCard", "IIUUI"}, -- usedCardId, boardCardId, handCards_U, boardCards_U, lastCollectedCard
	{"Anniv2023FSC_OnOpponentUseCard", "IIIUI"}, -- usedCardId, boardCardId, opponentCardNum, boardCards_U, lastCollectedCard
	{"Anniv2023FSC_OnSelfReplaceCard", "IIUU"}, -- giveUpCardId, newHandCardId, handCards_U, boardCards_U
	{"Anniv2023FSC_OnOpponentReplaceCard", "IIU"}, -- giveUpCardIdx, opponentCardNum, boardCards_U
	{"Anniv2023FSC_OnAddBoardCard", "IU"}, -- cardId, boardCards_U
	{"Anniv2023FSC_OnReShuffleBoardCard", "U"}, -- boardCards_U
	{"Anniv2023FSC_StartRound", "bd"}, -- bIsOnMove, countDownEndTime
	{"Anniv2023FSC_OnDealCards", "IUU"}, -- opponentCardNum, handCards_U, boardCards_U
	{"Anniv2023FSC_UpdateScore", "II"}, -- selfScore, opponentScore
	{"Anniv2023FSC_StartAddNewCombo", "UIId"}, -- cards, comboId, score, countDownEndTime
	{"Anniv2023FSC_StartSpCardEffect", "IId"}, -- cardId, effectId, countDownEndTime
	{"Anniv2023FSC_SpCardEffectEnd", "IIIU"}, -- index, cardId, effectId, extreData_U
	{"Anniv2023FSC_OpponentStartSkill", "IId"}, -- cardId, effectId, countDownEndTime
	{"Anniv2023FSC_StartReplicate", "IIUd"}, -- cardId, effectId, card2Effects_U, countDownEndTime
	{"Anniv2023FSC_StartInterrupt", "IIUd"}, -- cardId, effectId, card2Effects_U, countDownEndTime
	{"Anniv2023FSC_SyncPlayerInfo", "UU"}, -- selfInfo, opponentInfo
	{"Anniv2023FSC_SyncPrepareInfo", "UIId"}, -- specialCards_U, selfStatus, opponentStatus, countDownEndTime
	{"Anniv2023FSC_SyncCardInfo", "UUUIIUIUU"}, -- boardCards, handCards, selfSpecialCards, selfLastCard, opponentCardNum, opponentShowedCards, opponentLastCard, selfCollectedCards, selfCollectedCards
	{"Anniv2023FSC_SyncGuidingStage", "I"}, -- guidingStage
	{"Anniv2023FSC_CloseGameWnd", ""},
	{"Anniv2023FSC_SendCollectedCardsData", "IUU"}, -- queryIndex, collectedCards, collectedCombos
	{"Anniv2023FSC_QueryPlayWndDataResult", "UUUU"}, -- playData, rankData, selfData, npcData
	{"Anniv2023_QueryFestivalWndDataResult", "I"}, -- specialCardNum

	{"SyncHuiCaiShanData", "U"}, -- data_U
	{"SyncYiRenSiFindCluesData", "U"}, -- data_U
	{"OpenDaFuWengSignUpWnd", "b"}, -- bSignUp
	{"SyncDaFuWengSignUpResult", "b"}, -- bSignUp
	{"QingMing2023SyncCommonData", "IIU"}, -- subplayId, dataType, dataU
	{"DaFuWengSyncGameInfo", "U"}, -- data_U
	{"SendGuildJuDianChallengeMemberInfo", "IIIIbbU"}, -- onlineMemberCount, totalMemberCount, onlineEliteCount, totalEliteCount, hasPermission, isChallenging, memberInfo

	{"NanDuLordAcceptScheduleSuccess", "III"}, -- period, scheduleId, scheduleType
	{"NanDuLordSelectScheduleChoiceSuccess", "IIII"}, -- period, scheduleId, question, choice
	{"NanDuLordOpenWnd", "I"}, -- wndType
	{"NanDuLordFinishScheduleEvent", "I"}, -- period
	{"NanDuLordShowAttrChange", "U"}, -- details
	{"UnlockNanDuJuBaoPenItem", "I"}, -- designId 
	{"GetNanDuJuBaoPenRewardSuccess", "I"}, -- fameId 
	{"NanDuLordBecomeLordSuccess", ""},
	{"NanDuLordRequestSetLordStatueSuccess", ""},
	{"NanDuTaskBargainsSuccess", "II"}, -- bargainId, price

	{"LiYuZhangDaiFoodCookingResult", "Ib"}, -- cookbookId, isSucc
	{"SendLiYuZhangDaiFoodPositons", "U"}, -- positonsData
	{"FindLiYuZhangDaiFoodFar", "II"}, -- positonX, positonY
	{"SendQingMing2023PveRank", "IIU"}, -- myRank, finishTime, rankData
	{"QingMing2023PvePlaySuccess", "bIIIII"}, -- bWin, mode, finishTime, myRank, minTime, award
	{"DaFuWengShuYuanAnswerResult", "IIbU"}, -- questionId, answer, result, rewardInfo

	-- New Arena
	{"SendNewArenaInfo", "IIIIbIUUU"}, -- rankScore, shopScore, winCount, favCount, isPlayOpen, matchType, passportData, taskData, friendData
	{"SendPlayerArenaDetails", "dsIIIbUUU"}, -- targetId, name, gender, class, level, isFeiSheng, appearanceProp, arenaDetails, mateData
	{"SendArenaOpenInfo", "IU"}, -- matchType, openedTypeData
	{"SendNewArenaRankInfo", "IIIIbIIU"}, -- myScore, myRank, seasonId, levelType, isCross, page, totalPage, rankData
	{"SendArenaTaskInfo", "IIIUUU"}, -- currentWeek, openedWeek, activedWeek, starData, taskData, finishedWeek
	{"SetArenaActiveTaskWeekResult", "I"}, -- activedWeek
	{"SendArenaPassportInfo", "IIIII"}, -- curExp, curLevel, rewardedLevel, weekExp, weekExpLimit
	{"SendArenaPassportGiftInfo", "IIU"}, -- jadeLimit, gotLevel, friendData
	{"SendArenaPassportRankInfo", "IIU"}, -- rankType, myRank, rankData

	{"SendSetGuildJuDianChallengeEliteResult", "dbIIII"}, -- targetPlayerId, isElite, onlineMemberCount, totalMemberCount, onlineEliteCount, totalEliteCount
	{"HuiCaiShanShowPic", "UI"}, -- data_U, duration
	{"MonthCardInfoChange", "s"}, -- reason

	{"SyncRecommendQiChang", "sI"}, -- babyId, qiChangId
	{"SyncSetTargetQiChangRes", "sI"}, -- babyId, qiChangId
	{"SyncTargetQiChangInfo", "sII"}, -- babyId, targetQiChangId, recommendQiChangId
	{"SendQingMing2023PveInfo", "bbbIII"}, -- isOpen, isTeamOk, isLevelOk, award, myRank, finishTime
	{"DaFuWengYaoQianResult", ""},
	{"DaFuWengRandomEventOpenTreasure", ""},
	{"Anniv2023ZaiZai_PlaySuccess", "Ib"}, -- playId, bHasReward

	{"SendNewArenaPlayResult", "IIiIIIIIIIUUU"}, -- matchType, winType, changeRankScore, newRankScore, addShopScore, totalShopScore, winComboTimes, playStartTime, weekPassportExp, weekPassportExpLimit, teamInfo, playerData, taskData
	{"DaFuWengRandomEventBeggarSelect", "IU"}, -- select, rewardInfo

	{"SyncEndlessChallengeResult", "Ibb"}, -- bossNumber, isGetBaseDailyReward, isGetAdvancedDailyReward
	{"SyncDuanWu2023PVPVEScore", "IIIU"}, -- teamId, teamOneScore, teamTwoScore, scoreInfo_U
	{"SyncDuanWu2023PVPVESelfTeamScoreChange", "IdIII"}, -- teamScore, playerId, playerScore, playerContribution, scoreStatus
	{"SyncDuanWu2023PVPVEOtherTeamScoreChange", "II"}, -- teamScore, scoreStatus
	{"SyncDuanWu2023PVPVECompareResult", "dIdII"}, -- winnerId, winnerScore, loserId, loserScore, changeScore
	{"SyncDuanWu2023PVPVEPlayResult", "IIIIIUU"}, -- getScore, dailyScore, winTeamId, teamOneScore, teamTwoScore, teamOneInfo_U, teamTwoInfo_U

	{"SendZhanLingAlertInfo", "IIIIIb"}, -- p1, p2, p3, vip1, vip2, bAlert
	{"SyncZhanLingPlayData", "IIIIIU"}, --  p1, p2, p3, vip1, vip2, receiveDataUD
	{"UnLockZhanLingVipSuccess", "I"}, -- vip
	{"SendZhanLingTaskData", "U"}, -- taskDataUD
	{"ReceiveZhanLingRewardSuccess", "II"}, -- level, index
	{"DaFuWengSyncGameResult", "bIUUU"}, -- result, rank, wonderfulRecord_U, report_U, reward_U
	{"ReceiveDaFuWengRewardSuccess", "IIU"}, -- level, index, resultTbl
	{"UnLockDaFuWengVipSuccess", "I"}, -- vip
	{"SyncDaFuWengPlayData", "U"}, -- playData_U

	{"SyncStoreRoomItem", "Is"}, -- pos, itemId
	{"SyncStoreRoomPosCount", "III"}, -- itemcount, silvercount, jadecount
	{"DaFuWengSyncMoney", "diii"}, -- playerId, curMoney, changeValue, totalMoney
	{"DaFuWengShowMessage", "dIbU"}, -- playerId, stage, result, extraInfo_U
	{"YiRenSiFindCluesAndOpenWnd", "IU"}, -- cluesId, data_U

	{"SyncArenaPlayerJingYuanInfo", "U"}, -- playerInfoTbl
	{"SyncArenaTeamJingYuanInfo", "U"}, -- teamInfoTbl
	
	{"SyncStarBiwuLeiGuTimes", "U"}, -- timesUd
	{"Anniv2023_SetConfidantId", "d"}, -- playerId

	{"PlayerQueryCompetitionHonorRankReturn","IIUI"}, --selfRank, viewId, rankData
	{"QueryCompetitionHonorPlayerInfoResult","IUUd"}, --score, appearanceProp, honorList

	{"FenYongZhenXianShowRuleWnd", ""},
	{"FenYongZhenXianSyncGameInfo", "IU"}, -- selfForce, gameInfoU
	{"FenYongZhenXianHorseKilled", "dIII"},  -- attackerId, attackerForce, attackerClass, attackerGender
	{"FenYongZhenXianHorseCountDown", "III"},  -- engineId, horseRebornTime, horseForce
	{"FenYongZhenXianPlayerKilled", "dIIIdIII"},  -- attackerId, attackerForce, attackerClass, attackerGender, defenderId, defenderForce, defenderClass, defenderGender
	{"FenYongZhenXianReachScoreLimit", "I"}, -- score
	{"FenYongZhenXianSendPlayResult", "IIIIIUU"}, -- win, winReward, loseReward, extraReward, combo, teamInfoU, playerInfoU
	{"FenYongZhenXianShowSkillChooseWnd", "I"}, -- closeTime

	{"RecreateExtraEquipResultConfirmResult", ""},
	{"PlayerQueryCrossPropRankDataResult", "U"}, --crossRankData
	{"Anniv2023FSC_SendSpecialCardsData", "U"}, -- specialCards

	{"ArenaJYPrepareSpawn", "Ib"}, -- srcEngineId, isCenter
	{"ArenaJYSpawn", "IbU"}, -- srcEngineId, isCenter, spawnInfo
	{"QingMing2023PvpSendFriendRank", "U"},  -- rankDataU
	{"QingMing2023PvpSendTopRank", "U"},  -- rankDataU

	{"QueryCompetitionHonorChapionGroupResult", "U"}, --chapionInfosU

	--2023六一战糖果鬼玩法
	{"PlayerQueryTangGuoGuiDataResult","III"}, --TodayBaseRewardTime, TodayWinRewardTime, TodayJoinTime
	{"SyncTangGuoGuiNpcHp", "I"}, --npcLeftHp
	{"TangGuoGuiPlayResult", "bIIU"}, --isSuccess, passNpcHp, useTime, reward_U

	--2023六一捣蛋小鬼玩法
	{"PlayerQueryDaoDanXiaoGuiDataResult","IIi"}, --TodayJoinTime, TodayRewardTime, passTime
	{"SyncDaoDanXiaoGuiPlayInfo","U"}, --playInfo_U
	{"DaoDanXiaoGuiPlayResult","bIUU"}, --isSuccess, passTime, playerInfo_U, reward_U

	{"XinXiangShiChengSendLotteryInfo", "IIIIIIIIU"},  -- haveGold, haveSilver, hasBronze, numberOfDraws, needGold, needSilver, allTakeNeedGold, allTakeNeedSilver, itemsSelected
	{"XinXiangShiChengCommonResult", "U"},  -- itemsInfoU
	{"XinXiangShiChengAdvancedResult", "IU"},  -- silverCanGive, itemsInfoU
	{"Anniv2023ZaiZai_MakeTabenSuccess", "I"}, -- count


	{"SyncTianQiAutoBuyItemConfirmId", "II"},  -- idx, itemConfirmId
	{"XinXiangShiChengSendCoinSuccess", "d"},  -- friendId
	
	{"UnlockPlayerExpressionTxtWithExpiredTime", "iI"}, -- expressionTxt, expiredTime
	{"QueryValidExpressionTxtResultV2", "U"}, -- expressionTxtTblUD
	{"XinXiangShiChengSendCoinAmount", "II"}, -- gold, bronze

	{"DaoDanXiaoGuiScanResult", "b"}, --isSuccess
	{"DaoDanXiaoGuiSeeThroughResult", "b"}, --isSuccess

	{"OpenStoreRoomWnd", "bI"},  -- isValidZuoQi, zuoqiTemplateId

	{"CloseSelectConfirmUI", ""},
	{"SyncTeamSelectConfirmInfo", "bIIIU"}, -- bForceOpen, startTime, waitTimeout, gamePlayId, resultUd
	{"SyncNoTeamSelectConfirmInfo", ""},
	{"JZLYSyncStampProgress", "UI"}, -- stampProgressU, mswsTimesLeft
	{"SendQianDaoInfo_New", "IIIIII"}, -- nowTime, todayTimes, totalTimes, nextBuQianTime, usedBuQianTimes, totalBuQianTimes
	{"ShowQianDaoWnd_New", ""},
	{"ShowQianDaoRefreshFlag_New", ""},
	{"OpenNewQianDao", ""},

	{"ArenaTalentInfo", "IU"}, -- score, talentInfo
	{"SyncArenaTalentLevel", "IIII"}, -- score, talentId, subTalentId, newLevel
	{"UpdateCustomFacialData", "IIU"}, -- engineId, key, ud
	{"QueryCustomFacialDataResult", "IU"}, -- key, ud
	{"RequestSetCustomFacialDataResult", "b"}, -- success

	{"LiuYi2023FindDaoDanXiaoGui", "dII"}, --playerId, class, gender

	{"ShuJia2023WorldEventsSyncStageTwoStatus", "Id"}, -- stageTwoStatus, stageTwoStartTime

	{"VoiceMsgRecovery", "dIs"}, --playerId, channelId, voiceId
	{"UpdatePlayerProfileFrameInfo", "id"}, -- frameId, expiredTime

	{"SyncRepertoryProp", "U"}, --repoUD
	
	{"SendPaimaiMySystemItemsBegin", ""},
	{"SendPaimaiMySystemItems", "U"}, -- itemUd
	{"SendPaimaiMySystemItemsEnd", ""},
	{"SendPaimaiSystemItemByPrice", "IddddI"}, -- itemTemplateId, oneShootPrice, currentlowestPrice, takeoffPrice, price, hasCount
	{"SendSystemOnShelfItem", "IUIId"}, -- validTotalCount, infoRpcUd, pageNum, itemTemplateId, currentLowestPrice
	{"PlayerRequestBetSystemResult", "bIdII"},-- bOk, itemTemplateId, price, targetType, betCount

	-- 2023 亚运会
	{"AsianGames2023RequestQuestionsResult", "IUU"}, -- questionCurDay, questionsCache_U, questionPlayerAnswersCache_U
	{"AsianGames2023JueDouRemind", ""},
	{"AsianGames2023QueryGambleInfo_Result", "IIIddU"}, -- id, status, choice, choiceOneNum, choiceTwoNum, description
	{"AsianGames2023RequestGambleBet_Result", "Idd"}, -- id, choiceOneNum, choiceTwoNum
	{"AsianGames2023QueryGambleStatus_Result", "U"}, -- gambleStatusCache_U

	{"GmChangeWuLianRecreateStatus","b"}, --isOpen
	{"PropGuideIsOpenCrossServer","b"}, --isOpenCross

	{"HideYanjia",""},
	{"SetWeatherSystemProfile","s"}, -- profile
	{"SetCinemachineXYZ", "ddd"}, -- xRotation, yAngle, zoom
	{"PDFY_RequestMonsterAllRewardResult", "U"}, -- updatedMonsterReward_U

	{"NavalPvpGoldChange", "IiIU"}, -- shipId, delta, reason, allGoldU
	{"NavalPvpExpChange", "IiU"}, -- shipId, delta, allExpU
	{"NavalPvpShowSpecialEvent", "III"}, -- posIndex, npcEngineId, eventId
	{"NavalPvpPlayEnd", "IIIU"}, -- rank, killShip, killMonster, packInfo
	{"NavalPvpAcceptSpecialEvent", "IIb"}, -- eventId, reasonId, bAccept
	{"NavalPvpSyncHuntInfo", "U"}, -- huntingInfoU

	{"CanTransformZKWeaponResult", "sdd"}, -- equipId, needYinPiao, needYinLiang
	{"TransformZKWeaponResult", "sI"}, -- equipId, subHand

	{"SetIsAllowOpenStoreRoomFromRepo", "b"}, -- isAllow

	{"SyncStarBiwuZhengShiSaiZK_QD", "IUIU"}, -- zhanduiDataLen, zhanduiData, statusDataLen, biSaiStatusUd
	{"SyncStarBiwuReShenSaiZK_QD", "dsIU"}, -- selfZhanduiId, selfZhanduiName, dataLen, dataUd
	{"StarBiwuSendJingCaiHistory", "UUIsdUUUd"}, -- jingcaiIdxsUd, rightChoiceUd, jingcaiStage, targetDate, phaseBetPool, choice2BetValueUd, showResultUd, matchTblUd, maxBetValue

	-- 通用通行证
	{"UpdatePassDataWithId", "IUI"}, -- passId, passData_U, reason
	{"SendOpenPassInfos", "SI"}, -- openPassIds, time

	{"QueryGuildLeagueSetHeroInfoResult", "U"},
	{"OpenGuildLeagueHeroInfoWnd", "bU"},
	{"AppointGuildLeagueHeroSuccess", "db"},
	{"RequestGetGuildLeagueCrossTrainLevelRewardSuccess", "II"},
	{"SendGuildLeagueCrossTrainRankRewardInfo","U"},

	{"PDFY_ShuffleBuffFail", ""},
	{"OpenLiuYi2023DaoDanXiaoGuiRules", ""},
	{"SendGuildLeagueCrossMatchBuffInfo", "U"},

	{"GaoChangJiDouCrossCancelApplySuccess", ""},
	{"ShowZhongYuanPuDuResult", "bIIU"}, -- bResult, mode, lvIdx, extraInfo_U
	{"QueryGuildLeagueCrossZhanLingRankResult", "UII"},
	{"ReplyStarBiwuZhanDuiListBegin", ""},
	{"ReplyStarBiwuZhanDuiListEnd", "dIII"}, --selfZhanDuiId, page, totalPage, source

	{"QueryGuildLeagueCrossRewardPoolInfoResult", "III"},
	{"QueryGuildLeagueCrossZhanLingExtInfoResult", "II"},

	{"XianHaiTongXingPlayerQueryTeamInfo_Result", "dddddIIU"}, -- member1Id, member2Id, member3Id, member4Id, member5Id, vipInfo, progress, extraInfo_U
	{"XianHaiTongXingPlayerQueryTeamProgress_Result", "I"}, -- progress
	{"GaoChangCancelApplySuccess", ""},

	{"SyncQmpkBanPickInfo", "IIUUUUU"}, -- force, round, basicData, roundWinForceData, attackChuZhanMemberData, defendChuZhanMemberData, banMemberData
	{"ReplyQmpkQueryZhanDuiMemberForBanPick", "UUUUUU"}, -- memberInfoData, roundData1, roundData2, roundData3, roundData4, roundData5
	{"ReplyQmpkQueryBiWuZhanDuiInfo", "IssIUUUUUU"}, -- force, title, kouhao, round, memberInfoData, roundData1, roundData2, roundData3, roundData4, roundData5
	{"ReplyQmpkActivityPageMeedInfo", "bIIU"}, -- canReceive, huoli, totalSignUpNum, receiveData
	
	{"SyncJingYuanPlayInfo", "UUd"},  -- teamJingYuanCount, playerJingYuanCount, addJingYuanPlayerId
	{"SyncJingYuanPlayResult", "IIUUU"},  -- win, playTime, teamInfoU, playerInfoU, rewardInfoU
	{"JingYuanPlayRemind", ""},

	{"Carnival2023MonsterResult", "IbI"}, -- killMonsterNum, getAward, getPoint
	{"Carnival2023RequestPlayDataResult", "IIIIIIIi"}, -- attackBuffLevel, defBuffLevel, speedBuffLevel, skillPoint, mReminTime, bReminTime, rReminTime, Rank

	{"SendPlayerAppearance", "dsbsIIIsU"}, -- targetId, context, isSucc, name, gender, class, titleId, titleName, appearProp
	{"YYS_SyncPlayData", "UU"}, -- playData, missionData
	{"QueryJingYuanPlayInfoResult", "IIII"},  -- talentLevel, engageTimes, hasPieceCount, maxPieceCount
	{"Carnival2023BossResult", "bIIII"}, -- bWin, finishTime, myRank, minTime, award
	{"QueryCarnival2023RankResult", "IIU"}, -- myRank, finishTime, RankData_U

	{"SyncGmPushBubblePopUp", "SSSISiIU"}, -- title, content, icon_content, type, url, duration, choice, limitUd

	{"QueryCompetitionSeasonDataResult", "U"}, --seasonData

	{"UpdateFurnitureExpireNotify", "I"}, --isNotify

	{"ModifyFashionDaPeiNameSuccess_Permanent", "Is"}, -- index, name
	{"SaveFashionDaPeiContentSuccess_Permanent", "IIIII"}, -- index, headId, bodyId, weaponId, backPendantId
	{"UnLockFashionSuccess_Permanent", "I"}, -- fashionId
	{"SetWardRobeTabenNumLimit_Permanent", "I"}, -- num
	{"CompositeTabenSuccess_Permanent", ""},
	{"SetFashionHideSuccess_Permanent", "II"}, -- pos, value
	{"UnLockTeamMemberBgSuccess", "I"}, -- id
	{"UseTeamMemberBgSuccess", "I"}, -- id
	{"UnLockTeamLeaderIconSuccess", "I"}, -- id
	{"UseTeamLeaderIconSuccess", "I"}, -- id

	{"QueryGuildPinTuDataResult", "IIIUb"}, -- stage, duration, answerCountdown, listUD, bQueryFromClient
	{"QueryGuildPinTuProgressResult", "bbI"}, -- guildFinished, selfFinished, guildFinishTime
	{"OpenGuildPinTuDetailWnd", "IIIUU"}, -- stage, duration, answerCountdown, listUD, chiplistUD
	{"CloseGuildPinTuDetailWnd", ""},
	{"PlayerEnterGuildPinTu", "dII"}, -- playerId, pictureId, pinTuPlayerCount
	{"PlayerLeaveGuildPinTu", "dII"}, -- playerId, pictureId, pinTuPlayerCount
	{"GetGuildPinTuChipSuccess", "II"}, -- pictureId, chipId
	{"ReturnGuildPinTuChipSuccess", "II"}, -- pictureId, chipId
	{"UseGuildPinTuChipSuccess", "III"}, -- pictureId, chipId, place
	{"UseGuildPinTuChipFail", "III"}, -- pictureId, chipId, place
	{"AnswerGuildPinTuQuestionSuccess", "IisU"}, -- pictureId, answered, answer, placeListUD
	{"AnswerGuildPinTuQuestionFail", "Iis"}, -- pictureId, answered, answer
	{"PlayerGetGuildPinTuChip", "dII"}, -- playerId, pictureId, chipId
	{"PlayerReturnGuildPinTuChip", "dII"}, -- playerId, pictureId, chipId
	{"PlayerFillGuildPinTu", "dIIU"}, -- playerId, pictureId, place, listUD
	{"PlayerAnswerGuildPinTuQuestion", "dIUU"}, -- playerId, pictureId, placeListUD, listUD
	{"GuildPinTuPlayStart", ""},
	{"GuildPinTuPlayEnd", ""},
	{"QueryGuildPinTuPlayerDataResult", "U"}, -- playerDataList_U
	{"ProtectHangZhouSyncSceneNpcCount", "UI"},  -- sceneNpcCountU, remainChallengeTimes
	{"QueryGuoQing2023InfoResult", "bbbIbbIs"},  -- jyOpen, jyOpenMatch, jyPlayEnd, jyTimes, hzOpen, hzPlayEnd, hzTimes, hzNextTime
	{"SyncCarnivalBigTicketCount", "I"},--newCount
	{"SyncCarnivalSmallTicketCount", "I"},--newCount
	{"QuanMinKaiShu_SyncPlayData", "U"}, -- playData_U
	{"QuanMinKaiShu_GetTianShuSuccess", "II"}, -- count, newScore
	{"UnlockFacialStyle", "I"}, -- id
	{"FashionLotteryQueryTicketPriceResult", "IIId"},  -- count, price, originalPrice, discount
	{"FashionLotteryDrawResult", "U"},  -- rewardItemsTbl
	{"SyncIgnoreCheckText", "IsII"}, -- channelId, text, mode, expireTime
	{"RequestCanQuickForgeExtraEquipResult","bIIsIU"},
	{"QuickForgeExtraEquipSuccess","II"},
	{"EnableScreenEdgeDistortion", "dd"}, -- range, intensity
	{"DisableScreenEdgeDistortion", ""},
	{"StartJuQingOperaPlay", "I"}, -- id
	{"StartJuQingMaskPlay", "I"}, -- id
	{"PlayerGetBigTicketCarnivalCollect",""},
	{"PlayerGetSmallTicketCarnivalCollect",""},

	{"YaoYeManJuan_SyncPlayState", "IddU"}, -- PlayState, EnterStateTime, NextStateTime, ExtraInfo
	{"FashionLotteryBuyTicketResult", "II"},  -- count, price
	{"NotifyChargeUseWebpay", "SS"}, -- msg, url

	{"SyncCustomFurnitureTemplateNum", "sdI"}, -- houseId, playerId, newNum
	{"SyncCustomFurnitureTemplateLv", "sdII"}, -- houseId, playerId, slotIdx, lv

	{"Carnival2023BossTrapPlayerCount", "III"}, -- npcId, currentCount, needCount

	{"SyncGlobalJinglingKey", "U"}, -- jinglingKeyTbl (index, jinglingKey, ...)
	{"SyncSystemPaimaiDelay", "I"}, -- playId
	{"ZhongYuanPuDu_SyncProgress", "III"}, -- stageId, param1, param2

	-- 2023 双十一
	{"SyncDouble11MQLDResult", "bbbUU"}, -- isGetDailyAward, isGetEliteAward, isNewRecord, rankCache_U, fightData_U
	{"SyncDouble11MQLDPlayStatus", "dIIIU"}, -- playerId, status, curTime, endTime, rankCache_U
	{"Double11MQLDBroadFlyAction", "dIII"}, -- playerId, x, y, z
	{"SyncDouble11BYDLYResult", "bbbbbIU"}, -- isHero, isWin, isGetDailyAward, isGetHeroAward, isGetEliteAward, usedTime, extraInfo_U
	{"SyncDouble11BYDLYPlayInfo", "bIIIb"}, -- isHero, stage, curtime, timelimit, isChangeStage
	{"SyncDouble11BYDLYFirstStageInfo", "IU"}, -- totalTreasure, teamTreasureInfo_U
	{"SyncDouble11BYDLYHeroReliveTimes", "I"}, -- heroReliveTimes
	{"Double11ShowRechargeLotteryRewards", "bU"}, -- isBagFull, rewards_U
	{"Request2023Double11ExteriorDiscountCoupons_Result", "U"}, -- coupons_U

	{"DownloadImRecord", "SSS"}, -- url, token, context
	{"UploadImRecord", "SSS"}, -- url, token, context
	{"UploadImRecordSuccessCallback", "bSS"}, -- bSuccess, fileId, context

	{"SyncGTWZhanShenTaskInfo", "IIb"}, -- acceptedTaskId,progress,isFinish
	{"SendGTWZhanShenTaskChooseData", "IU"}, -- acceptedTaskId, taskIds
	{"BeginCameraShake", "Iddd"}, -- shakeType, amplitude, frequency, duration
	{"EndCameraShake", ""},
	{"SyncWardrobeProperty", "Us"}, -- wardrobeProp, reason
	{"CheckFashionLotteryEnsuranceInfoResult", "I"},  -- left
	{"CheckFashionLotteryDiscountInfoResult", "d"},  -- discount
	-- 横屿荡寇
	{"SendHengYuDangKouStat", "IUId"}, -- statType, data_U, playerCount, statTotal
	{"ShowManualEndHengYuDangKouConfirm", ""},
	{"SyncHengYuDangKouSceneInfo", "U"}, -- sycInfo_U
	{"SyncHengYuDangKouKillExtraMonster", "I"}, -- killCount
	{"SyncHengYuDangKouOpenBox", "U"}, -- result_U
	{"UDPConnReady", ""},

	{"StartBlackScreenFadeIn", "d"}, -- duration
	{"StartBlackScreenFadeOut", "d"}, -- duration
	{"OpenPermanentFashionSwitch", ""},

	{"UploadFacialPic", "SSS"}, -- url, token, context
	{"ReviewFacialPicBegin", "SSSi"}, -- url, pic_id, context, status
	{"ReviewFacialPicEnd", "SSSi"}, -- url, pic_id, context, status
	{"SectXiuxingUpdateChuansongmenHp", "II"}, -- engineId, hp

	{"ReplyOpenHengYuDangKouInfo", "U"}, -- replyInfo_U
	{"RequestBeginnerGuideBatchTaskRewardSuccess", "UU"}, -- tasks, todayAllFinishT
	{"EvaluateFashionSuitSuccess_Permanent", "I"}, -- suitId

	{"YaoYeManJuan_UpdateIconStatus", "b"},
	{"ShowLuoChaBossPlayResult", "III"},
	{"QueryLuoChaHaiShi2023InfoResult", "II"},

	{"RequestChangeToCbgTradeItemResult", "sb"}, -- itemId, bSuccess
	{"RequestChangeToNoneCbgTradeItemResult", "sb"}, -- itemId, bSuccess
	{"SyncFashionability", "I"}, -- fashionability
	{"UpdateXiuxingDoorAndTime", "III"}, -- totalDoors, destroyedDoors, finishTime
	{"RequestGamePlayPaiMaiBetResult", "bIdII"}, --bOk, itemTemplateId, price, targetType, betCount
	{"SendGamePlayPaiMaiOnShelfCount", "IIU"}, -- targetType, playId, countResultUd
	{"SendGamePlayPaiMaiOnShelfItem", "IIIUIIII"}, -- targetType, playId, validTotalCount, infoRpcUd, pageNum, itemTemplateId, curCount, totalCount
	{"SendGamePlayPaiMaiItemInfoByPrice", "IddddII"}, -- itemTemplateId, oneShootPrice, currentLowestPrice, takeoffPrice, price, hasCount, extraValue

	{"SendQieXianGuideScenePlayerCount", "I"}, -- playerCount

	{"UnLockFashionByItemConfirm_Permanent", "IIsII"}, -- fashionId, progress, itemId, place, pos
	{"TakeOffFashion_Permanent", "I"}, -- fashionId
	{"TakeOffFashionSuit_Permanent", "I"}, -- suitId

	{"QueryGamePlayPaiMaiHistoryResult", "IIIU"}, -- playId, pageNum, totalCount, resultTbl
	{"GuoQing2023ShowMessage", "Is"},  -- force, msg

	{"Christmas2023GetCardSuccess","Ib"},
	{"Christmas2023GetCardGiftSuccess","I"},
	{"AddFashionSuccess_Permanent", "II"}, -- fashionId, isExist
	{"UnLockFashionAlert_Permanent", "I"}, -- fashionId

	{"SendPlayerCreditScoreData", "dU"},

	{"ReplyPublishToStarBiwuFreeMember", "b"}, -- bPublish
	{"ReplyInviteStarBiwuFreeMember", "d"}, -- targetId
	{"ReplyStarBiwuFreeMemberAcceptBeInvite", "d"}, -- zhanduiId
	{"ReplyStarBiwuFreeMemberRefuseBeInvite", "d"}, -- zhanduiId
	{"SyncStarBiwuJifensaiMatchS13Begin", ""},
	{"SyncStarBiwuJifensaiMatchS13", "U"}, -- ud
	{"SyncStarBiwuJifensaiMatchS13End", "IIIdb"}, -- day, group, selfGroup, selfZhanduiId, bMatchGenerated
	{"SyncStarBiwuJifensaiMainRankS13", "IIdU"}, -- group, selfGroup, selfZhanduiId, ud
	{"SyncStarBiwuJifensaiRoundRankS13", "IIIdU"}, -- group, round, selfGroup, selfZhanduiId, ud
	{"SyncStarBiwuTaotaisaiMatchS13", "IIdUI"}, -- group, selfGroup, selfZhanduiId, ud, currentMatchPlayIdx
	{"SyncStarBiwuFreememberS13", "IIUIIbI"}, -- profession, sortMethed,memberInfoUd, page, count, bPublishSelfInfo, totalPageCount
	{"SyncStarBiwuMyInviverBegin", ""},
	{"SyncStarBiwuMyInviver", "U"},  -- ud
	{"SyncStarBiwuMyInviverEnd", "U"}, -- ud
	{"SyncStarBiwuZhanduiNameBegin", ""},
	{"SyncStarBiwuZhanduiName", "U"}, -- ud
	{"SyncStarBiwuZhanduiNameEnd", ""},
	{"SyncStarBiwuJifensaiRankS13Begin", ""}, 
	{"SyncStarBiwuJifensaiRankS13", "U"}, -- ud
	{"SyncStarBiwuJifensaiRankS13End", "IId"}, -- group, selfGroup, selfZhanduiId
	{"SyncStarBiwuActivityStatusS13", "Ibb"}, -- signUpStatus, bSignUp, bHasFight

	{"SyncBingXuePlay2023PlayData", "IIIId"},
	{"StartBingXuePlay2023PickPart", ""},

	-- 2023万圣节 糖果外卖
	{"SyncCandyDeliverySignUpResult", "b"}, -- isSignUp
	{"QueryPlayerIsMatchingCandyDelivery_Result", "b"}, -- isMatching
	{"SyncCandyDeliveryCampInfo", "bU"}, -- isAll, campInfo
	{"SendCandyDeliveryResultInfo", "U"}, -- resultInfo
	{"SendCandyDeliveryCountDown", "I"}, -- countDown
	{"ShowCandyDeliverySummonFx", "II"}, -- fromEngineId, toEngineId
	{"SyncCandyDeliveryActionType", "II"}, -- engineId, actionType
	{"SendCandyDeliveryTeamInfo", "dd"}, -- driverId, passengerId

	-- 返回当前是开服第几天
	{"SendServerOpenPassedDay", "II"}, -- days, context

	{"Christmas2023GuaGuaKaResult", "IUUU"},
	{"ShowMessageAfterTakeOnFashion_Permanent", "IIb"}, -- position, fashionId, bHide
	{"UnlockPermanentProfileFrameDuplicate", "IIsI"}, -- place, pos, itemId, frameId
	{"QueryHengYuDangKouTeamMemberInfoResult", "UU"}, -- memberInfo, extraInfo
	{"QueryHengYuDangKouTeamMemberHpInfoResult", "U"}, -- memberHpInfo

	{"StartSkillTraining", "I"}, -- TrainId
	{"SyncStarBiwuJifesanSaiGroupS13", "IId"}, -- selfGroup, groupCount, selfZhanduiId
	{"Christmas2023BingXuePlayAward", "Ib"},
	{"Christmas2023BingXuePlayChooseCircle", ""},

	{"SyncZuoQiPassengerModeChanged", "IsII"}, -- driverEngineId, zuoqiId, mode, targetZuoQiTemplateId

	{"SyncDoubleDragonPlayerBegin", "U"}, -- memberInfo, 
	{"SyncDoubleDragonUseGouzi", "dd"}, --  playerid, direction
	{"SyncDoubleDragonGetLongzhu", "di"}, -- playerid, LongzhuId
	{"SyncDoubleDragonGameEnd", ""}, 
	{"SyncDoubleDragonExit", "d"}, -- playerid
	{"SyncStarBiwuTaotaisaiGroupS13", "IId"},
	{"QuerySongQiongShen2024InfoResult", "III"},
	{"SyncSongQiongShenChooseRewardIndex", "U"},
	{"SyncDoubleDragonPickPos", "di"}, -- playerid, pos
	{"SyncDoubleDragonLongZhuInfo", "U"}, -- longzhuInfo
	{"SyncDoubleDragonSyncScore","di"}, -- playerid, score

	{"StartTextBubblePlay", "I"}, -- playId
	{"StartShowZiMu", "Id"}, -- id, duration

	-- 2024元旦
	{"SendYuanDanJinChuiReward", "U"}, -- rewards = {itemId, count}
	{"SyncYuanDanXNCGAreaInfo", "U"}, -- areaInfo = {false, false, false, false, false, false, false, false}

	{"SyncHengYuDangKouBossHp", "dd"}, -- curHp, fullHp
	{"QueryPlayerForWuZiQiResult", "IUU"},

	{"ShowPlatformToPlayer", "IIIds"}, --uint engineId, uint templateId, uint actionState, double dir, string name
	{"GhostTeleportShowPlatformToPlayer","dIIIds"}, -- bsId, uint engineId, uint templateId, uint actionState, double dir, string name

	{"BroadcastCCNotify", "dss"}, -- ccid, nickname, keyword
	{"SyncStarBiwuJingCaiWenDa", "U"}, -- CStarBiwuJingCaiWenDa_Rpc

	{"ShowActivityAlert", "IbU"}, -- activityId, bShow, dataU
	{"HanJia2024InvertedWorld_SyncNpcPos", "U"}, -- dataU
	{"HanJia2024InvertedWorld_SyncMonsterPos", "U"}, -- dataU
	{"HanJia2024InvertedWorld_SyncPlayStatus", "III"}, -- floor, count, total
	{"HanJia2024InvertedWorld_SyncYuanQiValue", "I"}, -- value
	{"HanJia2024InvertedWorld_SyncPlayOpenInfo", "bd"}, -- bStart, endTime
	{"HanJia2024InvertedWorld_SyncEntrustMissionData", "U"}, -- missionDataU
	{"HanJia2024InvertedWorld_SyncNormalMissionData", "UU"}, -- weeklyMissionU, dailyMissionU

	{"SyncShuangLongQiangZhuApplyResult", "bII"},			-- bMatching times times
	{"SyncShuangLongQiangZhuEvent", "IU"},					-- event context
	{"SyncShuangLongQiangZhuChooseRewardIndex", "U"},		-- rewardChooseIndex
	{"SyncShuangLongQiangZhuPlayerPos", "U"},				-- playerPos
	{"SetSkillTrainingActiveSkills", "IIIII"},			

	{"SyncGuideManualData", "U"}, -- guideManualDataUd
	{"HanJia2024FurySnowman_SyncPlayResult", "bbb"},	-- isHardMode, isWin, hasRewarded
	{"StartJuQingLearnKungFuPlay", "I"}, -- id

	{"ReplyHengYuDangKouRankList", "U"}, -- replyInfo_U
	{"SendHengYuDangKouItemInfo", "U"}, -- itemData

	{"ShowGlobalCastingAlert", "IIUU"},
	{"DiscardExpressionAppearance", "II"}, -- group, appearance
	{"UpdateEquipPlayerClass", "sI"},
	{"RemoveAllFxFromObject", "I"},
	{"SendHengYuDangKouPlayer", "UI"}, -- data, playerCount
	
	{"SyncStarBiwuStarScore", "d"}, -- starScore
	{"SyncStarBiwuGroupIdx", "I"}, -- groupIdx

	{"SyncHZXJTaoHuaHp", "Iddd"}, -- engineId, hp, currentHpFull, hpFull
	{"SyncHZXJTaoHuaPickStatus", "IbI"}, -- engineId, isPicking, pickTime
	{"SyncHZXJTaoHuaStatus", "Ib"}, -- engineId, isRotten
	{"SyncHZXJTaoHuaResult", "II"}, -- pretty, rotten
	{"RequestPickHZXJTaoHuaDone", "dIb"}, -- playerId, taoHuaEngineId, isRotten
	{"SyncHZXJResult", "bIIdU"}, -- bWin, pretty, rotten, process, playerInfo
	{"ExtraEquipForgeSuitUpdate", "I"},

	{"HanJia2024InvertedWorld_ShowUIFx", "I"},

	{"HuntRabbitSyncStatusToPlayersInPlay", "IIIU"}, -- playStatus, enterStatusTime, leftTime, extraData
	{"HuntRabbitReplyRingPosResult", "bii"}, -- bSuccess, x, y
	{"ShowHideMonsterName", "Ib"}, -- engineId, bShow
	{"SyncSpecialConcernCandidatePlayers", "U"},  -- playerIdTbl
	{"ExtraEquipMarketingGetScoreAwardSuccess", ""},

	{"QueryVNClientAwardResult", "U"},
	{"UpdateVNClientAwardStatus", "SI"},
	{"AppsflyerNotify", "S"},
	{"QueryDayOnlineTimeResult", "dd"},
}
