local UIBasicSprite = import "UIBasicSprite"

CLuaQMPKDamageWnd = class()
RegistClassMember(CLuaQMPKDamageWnd,"NameLabel1")
RegistClassMember(CLuaQMPKDamageWnd,"NameLabel2")
RegistClassMember(CLuaQMPKDamageWnd,"DamageLabel1")
RegistClassMember(CLuaQMPKDamageWnd,"DamageLabel2")
RegistClassMember(CLuaQMPKDamageWnd,"Button")
RegistClassMember(CLuaQMPKDamageWnd,"InfoRoot")
CLuaQMPKDamageWnd.Name1=nil
CLuaQMPKDamageWnd.Name2=nil
function CLuaQMPKDamageWnd:Awake()
    self.NameLabel1 = self.transform:Find("ShowArea/InfoRoot/BG1/NameLabel"):GetComponent(typeof(UILabel))
    self.NameLabel2 = self.transform:Find("ShowArea/InfoRoot/BG2/NameLabel"):GetComponent(typeof(UILabel))
    self.DamageLabel1 = self.transform:Find("ShowArea/InfoRoot/BG1/Label"):GetComponent(typeof(UILabel))
    self.DamageLabel2 = self.transform:Find("ShowArea/InfoRoot/BG2/Label"):GetComponent(typeof(UILabel))
    self.Button = self.transform:Find("ShowArea/ExpandButton"):GetComponent(typeof(UISprite))
    self.InfoRoot = self.transform:Find("ShowArea/InfoRoot").gameObject
    UIEventListener.Get(self.Button.gameObject).onClick=DelegateFactory.VoidDelegate(function(go)
        self:onButtonClick(go)
    end)
end

function CLuaQMPKDamageWnd:Init()
    self:RefreshName()
end

function CLuaQMPKDamageWnd:onButtonClick( go) 
    if self.InfoRoot.activeSelf then
        self.Button.flip = UIBasicSprite.Flip.Nothing
        self.InfoRoot:SetActive(false)
    else
        self.Button.flip = UIBasicSprite.Flip.Horizontally
        self.InfoRoot:SetActive(true)
    end
end
function CLuaQMPKDamageWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncQmpkDpsInfo",self,"OnSyncQmpkDpsInfo")
end
function CLuaQMPKDamageWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncQmpkDpsInfo",self,"OnSyncQmpkDpsInfo")
end
function CLuaQMPKDamageWnd:OnSyncQmpkDpsInfo(attackDps, defendDps)
    self.DamageLabel1.text = tostring(math.floor(defendDps))
    self.DamageLabel2.text = tostring(math.floor(attackDps))
    self:RefreshName()
end
function CLuaQMPKDamageWnd:RefreshName()
    if CLuaQMPKDamageWnd.Name1 and CLuaQMPKDamageWnd.Name2 then
        self.NameLabel1.text = CUICommonDef.cutDown(CLuaQMPKDamageWnd.Name2)
        self.NameLabel2.text = CUICommonDef.cutDown(CLuaQMPKDamageWnd.Name1)
    end
end
function CLuaQMPKDamageWnd:OnDestroy()
    CLuaQMPKDamageWnd.Name1 = ""
    CLuaQMPKDamageWnd.Name2 = ""
end