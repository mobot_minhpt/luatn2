local UIGrid=import "UIGrid"
local Object=import "UnityEngine.Object"
local Ease=import "DG.Tweening.Ease"
local UISoundEvents=import "SoundManager+UISoundEvents"
local Vector3 = import "UnityEngine.Vector3"

LuaShengXiaoCard = class()

RegistClassMember(LuaShengXiaoCard,"m_Selected")
RegistClassMember(LuaShengXiaoCard,"m_Node")
RegistClassMember(LuaShengXiaoCard,"m_BgSprite")
-- RegistClassMember(LuaShengXiaoCard,"m_Card")
RegistClassMember(LuaShengXiaoCard,"m_Index")
RegistClassMember(LuaShengXiaoCard,"m_Id")
RegistClassMember(LuaShengXiaoCard,"m_ShengXiao")
RegistClassMember(LuaShengXiaoCard,"m_StartDrag")
RegistClassMember(LuaShengXiaoCard,"m_CardNum")--总的牌的数量
RegistClassMember(LuaShengXiaoCard,"m_Transform")
RegistClassMember(LuaShengXiaoCard,"m_Tweener")

RegistClassMember(LuaShengXiaoCard,"m_Lookup")


function LuaShengXiaoCard:Init(tf,id,shengxiao,index,cardNum)
    self.m_Transform=tf
    self.m_Index=index
    self.m_Id = id
    self.m_ShengXiao = shengxiao
    self.m_StartDrag=false
    self.m_CardNum=cardNum
    tf.name=tostring(index)

    self.m_Selected=false
    self.m_Node=FindChild(tf,"Node").gameObject
    self.m_BgSprite=self.m_Node:GetComponent(typeof(UISprite))


    self:SetCard(shengxiao,index)

    if not CLuaDouDiZhuMgr.m_IsWatch then
        UIEventListener.Get(self.m_Node).onClick=LuaUtils.VoidDelegate(function(go)
            self:OnClick()
        end)
    end

    self:SetCardLocalPosition(self.m_Index)
end
--
function LuaShengXiaoCard:SetCard(value,index)
    local tf = self.m_Transform
	local label=FindChild(tf,"Label"):GetComponent(typeof(UILabel))
    local mark = FindChild(tf,"Mark"):GetComponent(typeof(UIWidget))

	local back=FindChild(tf,"Back"):GetComponent(typeof(UIWidget))

	local bg=FindChild(tf,"Node"):GetComponent(typeof(UIWidget))

	bg.depth=index*2
	label.depth=index*2+1
	mark.depth=index*2

	if value==0 then
		label.gameObject:SetActive(false)
		mark.gameObject:SetActive(false)
		back.depth = bg.depth+1
		-- bg.enabled = false
		back.gameObject:SetActive(true)
		return
	end
	back.gameObject:SetActive(false)
	-- bg.enabled = true

	-- local val=math.floor(card/100)
	-- local type=card%100

	local black=Color(0,0,0,1)
	local red=Color(1,0,0,1)



    label.gameObject:SetActive(true)
    mark.gameObject:SetActive(true)
    local design = ShengXiaoCard_ShengXiao.GetData(value)
    label.text = design.Name
    label.color = NGUIText.ParseColor24(design.Color, 1)

    local path = SafeStringFormat3("UI/Texture/Transparent/Material/chunjie2022_shengxiaocard_%02d.mat",value)
    mark.gameObject:GetComponent(typeof(CUITexture)):LoadMaterial(path)
end

function LuaShengXiaoCard:OnClick(  )
    if self.m_Selected then
        --复位
        LuaUtils.SetLocalPositionY(self.m_Node.transform,0)
    else
        --弹出
        LuaUtils.SetLocalPositionY(self.m_Node.transform,40)
    end
    self.m_Selected=not self.m_Selected

    SoundManager.Inst:PlayOneShot(UISoundEvents.ButtonClick, Vector3(0,0,0),nil,0)

    LuaShengXiaoCardMgr.m_SelectedCardId = self.m_Id
    --其他牌收回
    g_ScriptEvent:BroadcastInLua("ShengXiaoCard_OnClickCard", self.m_Id,self.m_Selected)
end


function LuaShengXiaoCard:SetCardLocalPosition(index)
    LuaUtils.SetLocalPositionX(self.m_Transform,self:GetCardLocalPositionX(index))
end



function LuaShengXiaoCard:GetCardLocalPositionX(index)
    local grid=self.m_Transform.parent:GetComponent(typeof(UIGrid))

    -- local maxPerLine=grid.maxPerLine
    local posX=grid.cellWidth*(index-1)

    local fx=grid.cellWidth*(self.m_CardNum-1)/2

    posX=posX-fx
    return posX
end

-- function LuaShengXiaoCard:Register()
--     LuaShengXiaoCardMgr.m_CardsLookup[self.m_Node]=self
--     LuaShengXiaoCardMgr.m_AllCards[self]=true
-- end
-- function LuaShengXiaoCard:Unregister()
--     LuaShengXiaoCardMgr.m_CardsLookup[self.m_Node]=nil
--     LuaShengXiaoCardMgr.m_AllCards[self]=nil
-- end

function LuaShengXiaoCard:SetGreyColor()
    self.m_BgSprite.color=Color(0.8,0.9,1,1)
end
function LuaShengXiaoCard:SetNormalColor()
    self.m_BgSprite.color=Color(1,1,1,1)
end
function LuaShengXiaoCard:Pop()
    LuaUtils.SetLocalPositionY(self.m_Node.transform,40)
    self.m_Selected=true
end
function LuaShengXiaoCard:TakeBack()
    LuaUtils.SetLocalPositionY(self.m_Node.transform,0)
    self.m_Selected=false
end
function LuaShengXiaoCard:MoveToNewPos()
    local posX=self:GetCardLocalPositionX(self.m_Index)
    LuaTweenUtils.DOKill(self.m_Tweener,false)
    LuaTweenUtils.TweenPositionX(self.m_Transform,posX,0.5)
end


function LuaShengXiaoCard:Disappear()
    LuaTweenUtils.DOKill(self.m_Transform,false)
    LuaTweenUtils.TweenPositionY(self.m_Transform,80,0.5)
    LuaTweenUtils.TweenAlpha(self.m_Transform,1,0,0.5)

    Object.Destroy(self.m_Transform.gameObject,0.5)
end

-- function LuaShengXiaoCard:PlayGetDiPaiEffect()
--     LuaTweenUtils.DOKill(self.m_Transform,false)
--     LuaUtils.SetLocalPositionY(self.m_Transform,180)
--     LuaTweenUtils.TweenAlpha(self.m_Transform,0,1,0.8)
--     LuaTweenUtils.TweenPositionY(self.m_Transform,0,0.8)
-- end

function LuaShengXiaoCard:PlayFaPaiEffect()
    LuaUtils.SetLocalPositionX(self.m_Transform,0)
    self:MoveToNewPos()
end

--出牌动画
function LuaShengXiaoCard.CardAppear(tf)
    LuaTweenUtils.DOKill(tf,false)
    tf.localScale=Vector3(2,2,1)
    LuaTweenUtils.TweenAlpha(tf,0,1,0.5)
    LuaTweenUtils.TweenScaleXY(tf,1,1,0.5,Ease.OutBack)
end
function LuaShengXiaoCard.SetName(tf,shengxiao)
    local label=FindChild(tf,"Label"):GetComponent(typeof(UILabel))
    if shengxiao>0 then
        local design = ShengXiaoCard_ShengXiao.GetData(shengxiao)
        label.text = design.Name
        label.color = NGUIText.ParseColor24(design.Color, 1)
    else
        label.text = nil
    end
    local mark = FindChild(tf,"Mark"):GetComponent(typeof(CUITexture))
    mark:Clear()
    if shengxiao>0 then
        local path = SafeStringFormat3("UI/Texture/Transparent/Material/chunjie2022_shengxiaocard_%02d.mat",shengxiao)
        mark:LoadMaterial(path)
    end
end
