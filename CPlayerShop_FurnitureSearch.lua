-- Auto Generated!!
local AlignType = import "L10.UI.CPopupMenuInfoMgr+AlignType"
local Boolean = import "System.Boolean"
local CClientFurnitureMgr = import "L10.Game.CClientFurnitureMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CPlayerShop_FurnitureSearch = import "L10.UI.CPlayerShop_FurnitureSearch"
local CPlayerShopMgr = import "L10.UI.CPlayerShopMgr"
local CPlayerShopPopupMenu = import "L10.UI.CPlayerShopPopupMenu"
local CPlayerShopPopupMenuInfoMgr = import "L10.UI.CPlayerShopPopupMenuInfoMgr"
local CYuanbaoMarketMgr = import "L10.UI.CYuanbaoMarketMgr"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Int32 = import "System.Int32"
local Item_Item = import "L10.Game.Item_Item"
local ItemSearchParams = import "L10.UI.ItemSearchParams"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local MsgPackImpl = import "MsgPackImpl"
local Object = import "System.Object"
local QnButton = import "L10.UI.QnButton"
local QnCheckBox = import "L10.UI.QnCheckBox"
local SearchOption = import "L10.UI.SearchOption"
local String = import "System.String"
local UILabel = import "UILabel"
local UInt32 = import "System.UInt32"

local LuaPlayerShopFurnitureSearch = {}

function LuaPlayerShopFurnitureSearch:DoSearch(id)
  local itemId = id
  GenericDelegateInvoke(LuaPlayerShopFurnitureSearch.mThis.OnSearchCallback, Table2ArrayWithCount({itemId, SearchOption.All, false}, 3, MakeArrayClass(Object)))
end

CPlayerShop_FurnitureSearch.m_Init_CS2LuaHook = function (this, publicity)
  LuaPlayerShopFurnitureSearch.mThis = this
  this:ReloadLevelData()
  this:ReloadColorData()
  this:LevelAction(this.SelectLevel)
  this.m_CheckBoxGroup.OnSelect = MakeDelegateFromCSFunction(this.OnCheckBoxGroupSelect, MakeGenericClass(Action2, QnCheckBox, Int32), this)
  this.m_CheckBoxGroup:SetSelect(0, true)

  CYuanbaoMarketMgr.FurnitureSearchParam = nil

  local qiugouBtn = this.transform:Find('QnButton').gameObject

  local onQiugouClick = function(go)
    Gac2Gas.OpenRequestBuyWindow(0)
  end
  CommonDefs.AddOnClickListener(qiugouBtn,DelegateFactory.Action_GameObject(onQiugouClick),false)
end

CPlayerShop_FurnitureSearch.m_OnEnable_CS2LuaHook = function (this)
    this.m_LevelButton.OnClick = MakeDelegateFromCSFunction(this.OnLevelButtonOnClick, MakeGenericClass(Action1, QnButton), this)
    this.m_TypeButton.OnClick = MakeDelegateFromCSFunction(this.OnTypeButtonOnClick, MakeGenericClass(Action1, QnButton), this)
    this.m_FurnitureNameButton.OnClick = MakeDelegateFromCSFunction(this.OnFurnitureNameButtonOnClick, MakeGenericClass(Action1, QnButton), this)
    this.m_SearchButton.OnClick = MakeDelegateFromCSFunction(this.OnSearchButtonOnClick, MakeGenericClass(Action1, QnButton), this)
    this.m_ColorButton.OnClick = MakeDelegateFromCSFunction(this.OnColorButtonOnClick, MakeGenericClass(Action1, QnButton), this)
    if this.m_FocusButton ~= nil then
        this.m_FocusButton.OnClick = MakeDelegateFromCSFunction(this.OnFocusButtonOnClick, MakeGenericClass(Action1, QnButton), this)
    end
    EventManager.AddListenerInternal(EnumEventType.QueryPlayerShopOnShelfNumResult, MakeDelegateFromCSFunction(this.OnQueryPlayerShopOnShelfNumResult, MakeGenericClass(Action2, MakeGenericClass(List, UInt32), Boolean), this))
    g_ScriptEvent:AddListener("SearchFurnitureItem", LuaPlayerShopFurnitureSearch, "DoSearch")
end

CPlayerShop_FurnitureSearch.m_OnDisable_CS2LuaHook = function (this)
    EventManager.RemoveListenerInternal(EnumEventType.QueryPlayerShopOnShelfNumResult, MakeDelegateFromCSFunction(this.OnQueryPlayerShopOnShelfNumResult, MakeGenericClass(Action2, MakeGenericClass(List, UInt32), Boolean), this))
    g_ScriptEvent:RemoveListener("SearchFurnitureItem", LuaPlayerShopFurnitureSearch, "DoSearch")
end

CPlayerShop_FurnitureSearch.m_OnCheckBoxGroupSelect_CS2LuaHook = function (this, checkbox, index)
    if index == 1 then
        this.m_IsColorSearch = true
        this.m_FocusButton.Visible = false
        this:ReloadColorData()
        this:ReloadFurnitureData()
    else
        this.m_IsColorSearch = false
        this.m_FocusButton.Visible = true
        this:ReloadFurnitureData()
    end


    this.m_ColorButton.Enabled = (index == 1)
    this.m_FurnitureNameButton.Enabled = (index == 0)
end
CPlayerShop_FurnitureSearch.m_ReloadColorData_CS2LuaHook = function (this)
    this.m_ColorActions = CPlayerShopMgr.Inst:ConvertOptions(this.m_ColorNames, MakeDelegateFromCSFunction(this.ColorAction, MakeGenericClass(Action1, Int32), this))
    if this.m_ColorActions.Count > 0 then
        this.SelectColor = math.min(math.max(this.SelectColor, 0), this.m_ColorActions.Count - 1)
        this.m_ColorButton.Text = this.m_ColorActions[this.SelectColor].text
    end
end
CPlayerShop_FurnitureSearch.m_ColorAction_CS2LuaHook = function (this, row)

    if this.m_ColorButton ~= nil and this.m_ColorActions ~= nil and this.m_ColorActions.Count > row then
        this.SelectColor = row
        this.m_ColorButton.Text = this.m_ColorActions[this.SelectColor].text
        this:ReloadFurnitureData()
    end
end
CPlayerShop_FurnitureSearch.m_OnColorButtonOnClick_CS2LuaHook = function (this, button)
    CPlayerShopPopupMenuInfoMgr.ShowPlayerShopPopupMenu(CommonDefs.ListToArray(this.m_ColorActions), button.transform, AlignType.Bottom, this:GetColumnCount(this.m_ColorActions.Count), nil, nil, DelegateFactory.Action(function ()
        this.m_ColorButton:SetTipStatus(false)
    end), 600,420)
end
CPlayerShop_FurnitureSearch.m_ReloadLevelData_CS2LuaHook = function (this)
    this.m_LevelActions = CPlayerShopMgr.Inst:ConvertOptions(CPlayerShopMgr.Inst:GetFurnitureLevelGroupNames(), MakeDelegateFromCSFunction(this.LevelAction, MakeGenericClass(Action1, Int32), this))

    if CYuanbaoMarketMgr.FurnitureSearchParam ~= nil then
        this.SelectLevel = CYuanbaoMarketMgr.FurnitureSearchParam.Level - 1
    end

    if this.m_LevelActions.Count > 0 then
        this.SelectLevel = math.min(math.max(this.SelectLevel, 0), this.m_LevelActions.Count - 1)
        this.m_LevelButton.Text = this.m_LevelActions[this.SelectLevel].text
    end
end
CPlayerShop_FurnitureSearch.m_ReloadTypeData_CS2LuaHook = function (this, levelName)
    this.m_TypeActions = CPlayerShopMgr.Inst:ConvertOptions(CPlayerShopMgr.Inst:GetFurnitureTypeGroupNames(), MakeDelegateFromCSFunction(this.TypeAction, MakeGenericClass(Action1, Int32), this))

    if CYuanbaoMarketMgr.FurnitureSearchParam ~= nil then
        local typeName = CClientFurnitureMgr.GetTypeNameByType(CYuanbaoMarketMgr.FurnitureSearchParam.Type)
        do
            local i = 0
            while i < this.m_TypeActions.Count do
                if typeName == this.m_TypeActions[i].text then
                    this.SelectType = i
                    break
                end
                i = i + 1
            end
        end
    end

    if this.m_TypeActions.Count > 0 then
        this.SelectType = math.min(math.max(this.SelectType, 0), this.m_TypeActions.Count - 1)
        this.m_TypeButton.Text = this.m_TypeActions[this.SelectType].text
    end
end
CPlayerShop_FurnitureSearch.m_ReloadFurnitureData_CS2LuaHook = function (this)
    if this.SelectLevel >= 0 and this.SelectLevel < this.m_LevelActions.Count and this.SelectType >= 0 and this.SelectType < this.m_TypeActions.Count then
        local furnitures = CPlayerShopMgr.Inst:SearchFurniture(this.m_LevelActions[this.SelectLevel].text, this.m_TypeActions[this.SelectType].text)
        CommonDefs.ListClear(this.m_SearchTemplateIds)
        CommonDefs.ListClear(this.m_SearchTemplateColors)
        CommonDefs.ListClear(this.m_SearchNames)

        local colorDic = CreateFromClass(MakeGenericClass(Dictionary, String, UInt32))
        CommonDefs.DictAdd(colorDic, typeof(String), "COLOR_ORANGE", typeof(UInt32), 1)
        CommonDefs.DictAdd(colorDic, typeof(String), "COLOR_BLUE", typeof(UInt32), 2)
        CommonDefs.DictAdd(colorDic, typeof(String), "COLOR_RED", typeof(UInt32), 3)
        CommonDefs.DictAdd(colorDic, typeof(String), "COLOR_PURPLE", typeof(UInt32), 4)

        CommonDefs.DictIterate(furnitures, DelegateFactory.Action_object_object(function(key,val)
            local item = Item_Item.GetData(key)
            if item then
                if item.PlayerShopPrice ~= 0 or item.ValuablesFloorPrice ~= 0 then
                    CommonDefs.ListAdd(this.m_SearchTemplateIds,typeof(UInt32), key)
                end
            end
        end))

        CommonDefs.ListSort1(this.m_SearchTemplateIds, typeof(UInt32), DelegateFactory.Comparison_uint(function (a, b)
            local data1 = Item_Item.GetData(a)
            local data2 = Item_Item.GetData(b)
            local name1 = data1.Name
            local name2 = data2.Name
            if (name1 == name2) then
                local colorIdx1 = 1
                (function ()
                    local __try_get_result
                    __try_get_result, colorIdx1 = CommonDefs.DictTryGet(colorDic, typeof(String), data1.NameColor, typeof(UInt32))
                    return __try_get_result
                end)()
                local colorIdx2 = 1
                (function ()
                    local __try_get_result
                    __try_get_result, colorIdx2 = CommonDefs.DictTryGet(colorDic, typeof(String), data2.NameColor, typeof(UInt32))
                    return __try_get_result
                end)()
                return NumberCompareTo(colorIdx1, colorIdx2)
            end
            return CommonDefs.StringCompareTo(name1, name2)
        end))
        this.m_SearchTemplateColors = CPlayerShopMgr.Inst:GetColors(this.m_SearchTemplateIds)


        do
            local i = 0
            while i < this.m_SearchTemplateIds.Count do
                CommonDefs.ListAdd(this.m_SearchNames, typeof(String), CommonDefs.DictGetValue(furnitures, typeof(UInt32), this.m_SearchTemplateIds[i]))
                i = i + 1
            end
        end

        this.m_FurnitureNameActions = CPlayerShopMgr.Inst:ConvertOptions(this.m_SearchNames, MakeDelegateFromCSFunction(this.FurnitureAction, MakeGenericClass(Action1, Int32), this))
        if this.m_FurnitureNameActions.Count > 0 then
            this.SelectFurniture = 0
            if this.m_FocusButton ~= nil then
                local focus = CPlayerShopMgr.Inst:IsFocusTemplate(this.m_SearchTemplateIds[this.SelectFurniture])
                local default
                if focus then
                    default = LocalString.GetString("取消关注")
                else
                    default = LocalString.GetString("关注此类")
                end
                this.m_FocusButton.Text = default
            end
            this.m_FurnitureNameButton.Text = this.m_FurnitureNameActions[0].text
            CommonDefs.GetComponentInChildren_Component_Type(this.m_FurnitureNameButton, typeof(UILabel)).color = this.m_SearchTemplateColors[0]
            this:QuerySellCount(this.m_SearchTemplateIds)
        else
            this.m_FurnitureNameButton.Text = LocalString.GetString("空")
            MessageWndManager.ShowOKMessage(LocalString.GetString("当前你选择的分类中没有任何装饰物,请重新选择！"), nil)
        end
    else
        this.m_FurnitureNameButton.Text = LocalString.GetString("空")
        MessageWndManager.ShowOKMessage(LocalString.GetString("当前你选择的分类中没有任何装饰物,请重新选择！"), nil)
    end
end
CPlayerShop_FurnitureSearch.m_OnTypeButtonOnClick_CS2LuaHook = function (this, button)
    CPlayerShopPopupMenuInfoMgr.ShowPlayerShopPopupMenu(CommonDefs.ListToArray(this.m_TypeActions), button.transform, AlignType.Bottom, this:GetColumnCount(this.m_TypeActions.Count), nil, nil, DelegateFactory.Action(function ()
        this.m_TypeButton:SetTipStatus(false)
    end), 600,420)
end
CPlayerShop_FurnitureSearch.m_OnFurnitureNameButtonOnClick_CS2LuaHook = function (this, button)
    CPlayerShopPopupMenuInfoMgr.ShowPlayerShopPopupMenu(CommonDefs.ListToArray(this.m_FurnitureNameActions), button.transform, AlignType.Top, this:GetColumnCount(this.m_FurnitureNameActions.Count), this.m_SearchTemplateColors, CreateFromClass(ItemSearchParams, this.m_SearchTemplateIds, false, SearchOption.All), DelegateFactory.Action(function ()
        this.m_FurnitureNameButton:SetTipStatus(false)
    end), 600,420)
end
CPlayerShop_FurnitureSearch.m_OnSearchButtonOnClick_CS2LuaHook = function (this, button)
    if this.m_IsColorSearch then
        local grade = this.SelectLevel + 1
        if this.SelectType >= 0 and this.SelectType < this.m_TypeActions.Count and this.SelectColor >= 0 and this.SelectColor < this.m_ColorValues.Count then
            local typeName = this.m_TypeActions[this.SelectType].text
            local type = CClientFurnitureMgr.GetTypeByName(typeName)
            local color = this.m_ColorValues[this.SelectColor]
            if this.OnSearchColorCallback ~= nil then
                GenericDelegateInvoke(this.OnSearchColorCallback, Table2ArrayWithCount({grade, type, color}, 3, MakeArrayClass(Object)))
            end
        end
    else
        if this.SelectFurniture < 0 or this.SelectFurniture >= this.m_SearchTemplateIds.Count then
            return
        end
        local itemId = this.m_SearchTemplateIds[this.SelectFurniture]
        GenericDelegateInvoke(this.OnSearchCallback, Table2ArrayWithCount({itemId, SearchOption.All, false}, 3, MakeArrayClass(Object)))
    end
end
CPlayerShop_FurnitureSearch.m_OnFocusButtonOnClick_CS2LuaHook = function (this, button)
    if this.SelectFurniture < 0 or this.SelectFurniture >= this.m_SearchTemplateIds.Count then
        return
    end
    local itemId = this.m_SearchTemplateIds[this.SelectFurniture]
    local allFocus = CPlayerShopMgr.Inst:GetAllFocusTemplate()
    if CommonDefs.HashSetContains(allFocus, typeof(UInt32), itemId) then
        CommonDefs.HashSetRemove(allFocus, typeof(UInt32), itemId)
        CPlayerShopMgr.Inst:SetAllFocusTemplate(allFocus)
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("取消关注成功"))
        this.m_FocusButton.Text = LocalString.GetString("关注此类")
    else
        CommonDefs.HashSetAdd(allFocus, typeof(UInt32), itemId)
        if allFocus.Count <= CPlayerShopMgr.s_FocusTemplateLimit then
            CPlayerShopMgr.Inst:SetAllFocusTemplate(allFocus)
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("关注成功"))
            this.m_FocusButton.Text = LocalString.GetString("取消关注")
        else
            g_MessageMgr:ShowMessage("PLAYER_SHOP_MAX_COLLECTIONS")
        end
    end
end
CPlayerShop_FurnitureSearch.m_LevelAction_CS2LuaHook = function (this, row)

    if this.m_LevelButton ~= nil and this.m_LevelActions ~= nil and this.m_LevelActions.Count > row then
        this.SelectLevel = row
        this.m_LevelButton.Text = this.m_LevelActions[this.SelectLevel].text
        this:ReloadTypeData(this.m_LevelActions[this.SelectLevel].text)
        this:ReloadFurnitureData()
    end
end
CPlayerShop_FurnitureSearch.m_TypeAction_CS2LuaHook = function (this, row)

    if this.m_TypeButton ~= nil and this.m_TypeActions ~= nil and this.m_TypeActions.Count > row then
        this.SelectType = row
        this.m_TypeButton.Text = this.m_TypeActions[this.SelectType].text
        this:ReloadFurnitureData()
    end
end
CPlayerShop_FurnitureSearch.m_FurnitureAction_CS2LuaHook = function (this, row)
    if this.m_FurnitureNameButton ~= nil and this.m_FurnitureNameActions ~= nil and this.m_FurnitureNameActions.Count > row then
        this.SelectFurniture = row
        if this.m_FocusButton ~= nil then
            local focus = CPlayerShopMgr.Inst:IsFocusTemplate(this.m_SearchTemplateIds[this.SelectFurniture])
            local default
            if focus then
                default = LocalString.GetString("取消关注")
            else
                default = LocalString.GetString("关注此类")
            end
            this.m_FocusButton.Text = default
        end
        this.m_FurnitureNameButton.Text = this.m_FurnitureNameActions[row].text
        CommonDefs.GetComponentInChildren_Component_Type(this.m_FurnitureNameButton, typeof(UILabel)).color = this.m_SearchTemplateColors[row]
    end
end
CPlayerShop_FurnitureSearch.m_GetColumnCount_CS2LuaHook = function (this, count)
    local columns = 1
    if count >= 6 then
        columns = 2
    end
    return columns
end
CPlayerShop_FurnitureSearch.m_QuerySellCount_CS2LuaHook = function (this, templateIds)

    if not CPlayerShop_FurnitureSearch.s_QuerySellCount then
        return
    end

    local data = CreateFromClass(MakeGenericClass(List, Object))
    local data2 = CreateFromClass(MakeGenericClass(List, Object))
    local data3 = CreateFromClass(MakeGenericClass(List, Object))
    this.m_MaxSellIndex = - 1
    this.m_MaxSellCount = 0
    do
        local i = 0
        while i < templateIds.Count do
            if i < 50 then
                CommonDefs.ListAdd(data, typeof(UInt32), templateIds[i])
            elseif i < 100 then
                CommonDefs.ListAdd(data2, typeof(UInt32), templateIds[i])
			else
                CommonDefs.ListAdd(data3, typeof(UInt32), templateIds[i])
            end
            i = i + 1
        end
    end
    Gac2Gas.QueryPlayerShopOnShelfNum(MsgPackImpl.pack(data), MsgPackImpl.pack(data2), MsgPackImpl.pack(data3), false, SearchOption_lua.All, false)
end
CPlayerShop_FurnitureSearch.m_OnQueryPlayerShopOnShelfNumResult_CS2LuaHook = function (this, selledCounts, publicity)

    if not CPlayerShop_FurnitureSearch.s_QuerySellCount then
        return
    end
    if CPlayerShopPopupMenu.IsPlayerShopPopUpMenuEnabled then
        return
    end

    if this.m_SearchTemplateIds ~= nil and this.m_SearchTemplateIds.Count > 0 then
        if publicity ~= false then
            return
        end
        do
            local i = 0
            while i < selledCounts.Count do
                local id = selledCounts[i]
                local count = selledCounts[i + 1]
                do
                    local j = 0
                    while j < this.m_SearchTemplateIds.Count do
                        if this.m_SearchTemplateIds[j] == id and this.m_MaxSellCount < count then
                            this.m_MaxSellIndex = j
                            this.m_MaxSellCount = count
                        end
                        j = j + 1
                    end
                end
                i = i + 2
            end
        end
        if this.m_MaxSellIndex >= 0 and this.m_MaxSellIndex < this.m_FurnitureNameActions.Count then
            this.SelectFurniture = this.m_MaxSellIndex
            if this.m_FocusButton ~= nil then
                local focus = CPlayerShopMgr.Inst:IsFocusTemplate(this.m_SearchTemplateIds[this.SelectFurniture])
                local default
                if focus then
                    default = LocalString.GetString("取消关注")
                else
                    default = LocalString.GetString("关注此类")
                end
                this.m_FocusButton.Text = default
            end
            this.m_FurnitureNameButton.Text = this.m_FurnitureNameActions[this.SelectFurniture].text
            CommonDefs.GetComponentInChildren_Component_Type(this.m_FurnitureNameButton, typeof(UILabel)).color = this.m_SearchTemplateColors[this.SelectFurniture]
        end
    end
end
