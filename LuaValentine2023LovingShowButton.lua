local DelegateFactory = import "DelegateFactory"

LuaValentine2023LovingShowButton = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

function LuaValentine2023LovingShowButton:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

    UIEventListener.Get(self.transform.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        LuaValentine2023Mgr:ShowYWQSExpressionWnd(go)
	end)
end

function LuaValentine2023LovingShowButton:Init()

end

--@region UIEvent

--@endregion UIEvent

