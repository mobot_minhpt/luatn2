require("3rdParty/ScriptEvent")
require("common/common_include")
local CTowerDefenseMgr=import "L10.Game.CTowerDefenseMgr"
local CMainCamera=import "L10.Engine.CMainCamera"
local LuaGameObject=import "LuaGameObject"

CLuaTowerDefenseFurniturePlaceHint=class()
RegistClassMember(CLuaTowerDefenseFurniturePlaceHint,"m_CachedPos")
RegistClassMember(CLuaTowerDefenseFurniturePlaceHint,"m_Anchor")

function CLuaTowerDefenseFurniturePlaceHint:Init()
	self.m_Anchor=LuaGameObject.GetChildNoGC(self.transform,"Anchor").gameObject
	self.m_CachedPos=Vector3(0,0,0)
    self:Update()
end

function CLuaTowerDefenseFurniturePlaceHint:OnEnable()
    --g_ScriptEvent:AddListener("TowerDefenseSyncMyPlayPlayerInfo", self, "OnTowerDefenseSyncMyPlayPlayerInfo")
end

function CLuaTowerDefenseFurniturePlaceHint:OnDisable()
    --g_ScriptEvent:RemoveListener("TowerDefenseSyncMyPlayPlayerInfo", self, "OnTowerDefenseSyncMyPlayPlayerInfo")
end
function CLuaTowerDefenseFurniturePlaceHint:OnTowerDefenseSyncMyPlayPlayerInfo()
    self:RefreshButtonState()
end

function CLuaTowerDefenseFurniturePlaceHint:Update()
    local tower = CTowerDefenseMgr.Inst.PlacePosHint
    if not tower then return end
    local RO=nil
    if tower then
        RO=tower.RO or tower.RelatedRO
    end
    if not RO then return end
    
    local pos=RO.TopAnchorPos
    local rootPos = RO.Position
    --计算
    pos = Vector3((rootPos.x+pos.x)/2,(rootPos.y+pos.y)/2,(rootPos.z+pos.z)/2)
    local screenPos = CMainCamera.Main:WorldToScreenPoint(pos)
    if self.m_CachedPos.x == screenPos.x and self.m_CachedPos.y==screenPos.y then
        return
    end
    screenPos.z = 0
    self.m_CachedPos = screenPos
    local nguiWorldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(screenPos)
    self.m_Anchor.transform.position = nguiWorldPos
end
return CLuaTowerDefenseFurniturePlaceHint
