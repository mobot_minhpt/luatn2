-- Auto Generated!!
local Byte = import "System.Byte"
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CPuppetClothesMgr = import "L10.UI.CPuppetClothesMgr"
local CPuppetFashionClothesWindow = import "L10.UI.CPuppetFashionClothesWindow"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CPuppetFashionClothesWindow.m_Init_CS2LuaHook = function(this)
    --Gac2Gas.RequestCheckFashion();
    --modelPreviewer.PreviewMainPlayer(forceShowHelmet: 1);
    this.dressList:Init()
    this:InitSwitchButtons()
    UIEventListener.Get(this.headSelectButton.gameObject).onClick = MakeDelegateFromCSFunction(this.OnSwitchButtonSelect, VoidDelegate, this)
    UIEventListener.Get(this.bodySelectButton.gameObject).onClick = MakeDelegateFromCSFunction(this.OnSwitchButtonSelect, VoidDelegate, this)
    UIEventListener.Get(this.weaponSelectButton.gameObject).onClick = MakeDelegateFromCSFunction(this.OnSwitchButtonSelect, VoidDelegate, this)
    UIEventListener.Get(this.beishiSelectButton.gameObject).onClick = MakeDelegateFromCSFunction(this.OnSwitchButtonSelect, VoidDelegate, this)
end
CPuppetFashionClothesWindow.m_InitSwitchButtons_CS2LuaHook = function(this)
    local idx = CPuppetClothesMgr.selectIdx
    if idx == 0 then
        return
    end
    local playerPuppetInfo = CommonDefs.DictGetValue(CClientHouseMgr.Inst.mCurFurnitureProp.PuppetInfo, typeof(Byte), idx)
    if playerPuppetInfo ~= nil then
        this:SetSwitchButtonSelect(this.headSelectButton, playerPuppetInfo.FashionInfo.HideHeadFashion == 0)
        this:SetSwitchButtonSelect(this.bodySelectButton, playerPuppetInfo.FashionInfo.HideBodyFashion == 0)
        this:SetSwitchButtonSelect(this.weaponSelectButton, playerPuppetInfo.FashionInfo.HideWeaponFashion == 0)
        this:SetSwitchButtonSelect(this.beishiSelectButton, playerPuppetInfo.FashionInfo.HideBackPendantFashion == 0)
    end
end
CPuppetFashionClothesWindow.m_OnFashionStatusChange_CS2LuaHook = function(this)
    this:Init()
end
CPuppetFashionClothesWindow.m_OnSwitchButtonSelect_CS2LuaHook = function(this, button)
    local idx = CPuppetClothesMgr.selectIdx
    if idx == 0 then
        return
    end
    if button == this.headSelectButton.gameObject then
        Gac2Gas.PlayerRequestSetPuppetHideFashion(
            idx,
            this:IsSwitchButtonSelected(this.headSelectButton) and 1 or 0,
            this:IsSwitchButtonSelected(this.bodySelectButton) and 0 or 1,
            this:IsSwitchButtonSelected(this.weaponSelectButton) and 0 or 1,
            this:IsSwitchButtonSelected(this.beishiSelectButton) and 0 or 1
        )
    elseif button == this.bodySelectButton.gameObject then
        Gac2Gas.PlayerRequestSetPuppetHideFashion(
            idx,
            this:IsSwitchButtonSelected(this.headSelectButton) and 0 or 1,
            this:IsSwitchButtonSelected(this.bodySelectButton) and 1 or 0,
            this:IsSwitchButtonSelected(this.weaponSelectButton) and 0 or 1,
            this:IsSwitchButtonSelected(this.beishiSelectButton) and 0 or 1
        )
    elseif button == this.weaponSelectButton.gameObject then
        Gac2Gas.PlayerRequestSetPuppetHideFashion(
            idx,
            this:IsSwitchButtonSelected(this.headSelectButton) and 0 or 1,
            this:IsSwitchButtonSelected(this.bodySelectButton) and 0 or 1,
            this:IsSwitchButtonSelected(this.weaponSelectButton) and 1 or 0,
            this:IsSwitchButtonSelected(this.beishiSelectButton) and 0 or 1
        )
    elseif button == this.beishiSelectButton.gameObject then
        Gac2Gas.PlayerRequestSetPuppetHideFashion(
            idx,
            this:IsSwitchButtonSelected(this.headSelectButton) and 0 or 1,
            this:IsSwitchButtonSelected(this.bodySelectButton) and 0 or 1,
            this:IsSwitchButtonSelected(this.weaponSelectButton) and 0 or 1,
            this:IsSwitchButtonSelected(this.beishiSelectButton) and 1 or 0
        )
    end
end
