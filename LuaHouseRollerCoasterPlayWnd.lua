local CClientFurnitureMgr=import "L10.Game.CClientFurnitureMgr"
local CHouseRollerCoasterMgr=import "L10.Game.CHouseRollerCoasterMgr"

LuaHouseRollerCoasterPlayWnd = class()

--@region RegistChildComponent: Dont Modify Manually!


--@endregion RegistChildComponent end
RegistClassMember(LuaHouseRollerCoasterPlayWnd,"m_MinSpeed")
RegistClassMember(LuaHouseRollerCoasterPlayWnd,"m_MaxSpeed")
RegistClassMember(LuaHouseRollerCoasterPlayWnd,"m_MaxSpeed")
RegistClassMember(LuaHouseRollerCoasterPlayWnd,"m_GravityParam")
RegistClassMember(LuaHouseRollerCoasterPlayWnd,"m_FrictionParam")
RegistClassMember(LuaHouseRollerCoasterPlayWnd,"m_AccParam")
--[[
    playInfo = {
        fid = 0
        path = {}
        pathIndex = 0
        speed = 0
        over = false
    }
]]
function LuaHouseRollerCoasterPlayWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    local setting = Zhuangshiwu_Setting.GetData()
    self.m_MinSpeed = setting.RollerCoastervmin
    self.m_MaxSpeed = setting.RollerCoastervmax
    self.m_FrictionParam = setting.RollerCoasteruk
    self.m_GravityParam = setting.RollerCoasterg
    self.m_AccParam = setting.RollerCoastera
end

function LuaHouseRollerCoasterPlayWnd:Init()

end

--@region UIEvent

--@endregion UIEvent

function LuaHouseRollerCoasterPlayWnd:OnEnable()
    g_ScriptEvent:AddListener("HouseRollerCoasterAccelerate", self, "OnHouseRollerCoasterAccelerate")
    CHouseRollerCoasterMgr.Inst.PlayMode = true
end
function LuaHouseRollerCoasterPlayWnd:OnDisable()
    g_ScriptEvent:RemoveListener("HouseRollerCoasterAccelerate", self, "OnHouseRollerCoasterAccelerate")
    CHouseRollerCoasterMgr.Inst.PlayMode = false
end
function LuaHouseRollerCoasterPlayWnd:OnHouseRollerCoasterAccelerate(huacheId)
    local playInfo = LuaHouseRollerCoasterMgr.GetPlayInfo(huacheId)
    if playInfo then
        playInfo.speed = playInfo.speed+self.m_AccParam
        playInfo.speed = math.min(math.max(playInfo.speed,self.m_MinSpeed),self.m_MaxSpeed)
    end
end

function LuaHouseRollerCoasterPlayWnd:Tick(playInfo)
    if not playInfo then return end
    if not playInfo.tick then playInfo.tick = 0 end
    playInfo.tick = playInfo.tick+1
    --15帧同步一次
    if playInfo.tick>15 then
        local node = playInfo.path[playInfo.pathIndex]
        if node then
            Gac2Gas.SyncRollerCoasterProgress(playInfo.cabinId,
                                playInfo.pathIndex,
                                math.floor(node.moveInfo.percent*100),
                                playInfo.speed)
        end
        playInfo.tick = 0
    end
end

function LuaHouseRollerCoasterPlayWnd:Update()
    local playerId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0

    local deltaTime = Time.deltaTime
    for k,playInfo in pairs(LuaHouseRollerCoasterMgr.s_PlayInfo) do
        if not playInfo.over then 
            local huacheFur = CClientFurnitureMgr.Inst:GetFurniture(playInfo.cabinId)
            if huacheFur then
                local tf = huacheFur.RO.transform

                local gravityForce = self.m_GravityParam
                local frictionForce = self.m_FrictionParam

                local dot = Vector3.Dot(tf.forward,Vector3.down)
                local speedAdd = 0
                if dot>0 then
                    speedAdd = gravityForce*dot*deltaTime
                else
                    speedAdd = gravityForce*dot*deltaTime
                end
                
                playInfo.speed = playInfo.speed+speedAdd
                playInfo.speed = math.min(math.max(playInfo.speed,self.m_MinSpeed),self.m_MaxSpeed)

                local distance = deltaTime*playInfo.speed
                if playInfo.pause then
                    distance = 0
                end

                playInfo.speed = playInfo.speed-distance*frictionForce

                local node = playInfo.path[playInfo.pathIndex]
                local v = LuaHouseRollerCoasterMgr.MoveOneStep(node,distance)
                if v>0 then
                    playInfo.pathIndex = playInfo.pathIndex+1
                    local nextNode = playInfo.path[playInfo.pathIndex]
                    if nextNode then
                        node = nextNode
                        LuaHouseRollerCoasterMgr.MoveOneStep(node,v)
                    else
                        -- print("play end")
                        playInfo.over = true
                    end
                end

                if playInfo.driverId ==  playerId and distance>0 then
                    self:Tick(playInfo)
                end

                local pos = node.moveInfo.currentPos
            
                local prePos = huacheFur.RO.transform.position
                local baseHeight = CHouseRollerCoasterMgr.GetBaseHeight()
                local offset = 0.46
                local newPos = Vector3(pos.x,baseHeight+pos.y+offset,pos.z)

                --暂停的时候的方向要重新算一下
                if playInfo.pause then
                    local node = playInfo.path[playInfo.pathIndex]
                    local percent = node.moveInfo.percent
                    if percent>=1 then
                        local pos = LuaHouseRollerCoasterMgr.Lerp(node,node.moveInfo.percent-0.01)
                        local newPos2 = Vector3(pos.x,baseHeight+pos.y+offset,pos.z)
                        huacheFur.RO.transform.position = newPos2
                        huacheFur.RO.transform:LookAt(newPos)
                        huacheFur.RO.transform.position = newPos
                    else
                        local pos = LuaHouseRollerCoasterMgr.Lerp(node,node.moveInfo.percent+0.01)
                        local newPos2 = Vector3(pos.x,baseHeight+pos.y+offset,pos.z)
                        huacheFur.RO.transform.position = newPos
                        huacheFur.RO.transform:LookAt(newPos2)
                    end
                else
                    huacheFur.RO.transform:LookAt(newPos)
                    huacheFur.RO.transform.position = newPos
                end
            end
        end
    end

    for k,playInfo in pairs(LuaHouseRollerCoasterMgr.s_PlayInfo) do
        if playInfo.over then
            --自动停车
            if CClientMainPlayer.Inst and playInfo.driverId==CClientMainPlayer.Inst.Id then
                if playInfo.isClose then--闭合的路径，到终点自动停车
                    Gac2Gas.RequestStopRollerCoaster(true)
                else
                    --到终点直接下车
                    local pos = CClientMainPlayer.Inst.RO.Position
                    Gac2Gas.RequestEndRollerCoaster(math.floor(pos.x),math.floor(pos.z))
                end
            end
            LuaHouseRollerCoasterMgr.ResetCabinPos(k)
            LuaHouseRollerCoasterMgr.SetPlayInfo(k,nil)
        end
    end
end