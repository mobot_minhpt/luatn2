local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UITable = import "UITable"
local UIGrid = import "UIGrid"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local UILabel = import "UILabel"
local QnButton = import "L10.UI.QnButton"
local CWarehousePackageItemCell = import "L10.UI.CWarehousePackageItemCell"
local CItemMgr = import "L10.Game.CItemMgr"
local MessageMgr = import "L10.Game.MessageMgr"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local QnTableView=import "L10.UI.QnTableView"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local EnumWarehouseItemCellLockStatus = import "L10.UI.EnumWarehouseItemCellLockStatus"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CExpandPackageMgr = import "L10.UI.CExpandPackageMgr"
local COpenEntryMgr=import "L10.Game.COpenEntryMgr"
local CGuideMgr=import "L10.Game.Guide.CGuideMgr"

LuaStoreRoomWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaStoreRoomWnd, "PageIndicatorTable", "PageIndicatorTable", UITable)
RegistChildComponent(LuaStoreRoomWnd, "ArrangeStoreRoomBtn", "ArrangeStoreRoomBtn", GameObject)
RegistChildComponent(LuaStoreRoomWnd, "WarehousePackageView", "WarehousePackageView", GameObject)
RegistChildComponent(LuaStoreRoomWnd, "RightPackageTypeBtn", "RightPackageTypeBtn", QnButton)
RegistChildComponent(LuaStoreRoomWnd, "RightPageView", "RightPageView", GameObject)
RegistChildComponent(LuaStoreRoomWnd, "RightOverlapBtn", "RightOverlapBtn", GameObject)
RegistChildComponent(LuaStoreRoomWnd, "RightArrangeBtn", "RightArrangeBtn", QnButton)
RegistChildComponent(LuaStoreRoomWnd, "RightArrangeBtnLabel", "RightArrangeBtnLabel", UILabel)
RegistChildComponent(LuaStoreRoomWnd, "TypeArrow", "TypeArrow", UISprite)
RegistChildComponent(LuaStoreRoomWnd, "RightAvailableLabel", "RightAvailableLabel", UILabel)
RegistChildComponent(LuaStoreRoomWnd, "TipBtn", "TipBtn", GameObject)
RegistChildComponent(LuaStoreRoomWnd, "TableView", "TableView", QnTableView)

--@endregion RegistChildComponent end
RegistClassMember(LuaStoreRoomWnd,"ChangeViewBtn")
function LuaStoreRoomWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	UIEventListener.Get(self.ArrangeStoreRoomBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnArrangeStoreRoomBtnClick()
	end)

	UIEventListener.Get(self.RightPackageTypeBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRightPackageTypeBtnClick(go)
	end)
	
	UIEventListener.Get(self.RightOverlapBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRightOverlapBtnClick()
	end)

	UIEventListener.Get(self.RightArrangeBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRightArrangeBtnClick()
	end)
    --@endregion EventBind end
	--self:InitRightView()
	self.RightOverlapBtn:SetActive(false)
	UIEventListener.Get(self.TipBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    g_MessageMgr:ShowMessage("StoreRoom_Rule_Tip")
	end)
	self.ChangeViewBtn = self.transform:Find("changeViewBtn").gameObject
	self.ChangeViewBtn:SetActive(IsOpenStoreRoom())
	UIEventListener.Get(self.ChangeViewBtn).onClick = DelegateFactory.VoidDelegate(function (go)
	    Gac2Gas.RequestOpenRepoFromStoreRoomWnd()
	end)
end

function LuaStoreRoomWnd:Init()
	self:InitValues()
	self:InitPageGrid()
	self:InitRightView(self.m_RightViewType)

	--手动解锁过格子的人不再触发引导
	local _guide_num =253
	if not CClientMainPlayer.Inst then
        return
    end

    local repoProp = CClientMainPlayer.Inst.RepertoryProp
    local total =  repoProp.ItemUnlockedRoomPosCount + 
        repoProp.SilverUnlockedRoomPosCount + 
        repoProp.JadeUnlockedRoomPosCount

    if not COpenEntryMgr.Inst:GetGuideRecord(_guide_num) and total <= 0 then
        CGuideMgr.Inst:StartGuide(_guide_num, 0)
    end
end

function LuaStoreRoomWnd:InitValues()
	self.m_CurPageIndex = 0
	self.m_IsPageMenue = true
	self.m_UnlockPosCount = 1
	self.EnumRightType = {
		ePackage = 0,
		eCangKu1 = 1,
		eCangKu2 = 2,
		eCangKu3 = 3,
		eXingNang = 4,
		eAnNang = 5,
	}
	self.m_RightViewType = self.EnumRightType.ePackage
	self.m_ItemLoaded = false

	self.RightViewGrid = self.RightPageView.transform:Find("Grid"):GetComponent(typeof(UIGrid))
	self.ItemCell = self.transform:Find("Templates/PackageItemPool/ItemCell")

	self.m_ActionDataSourc = DefaultItemActionDataSource.Create(
        1,
        {
            function() 
                self:PutInStoreRoom()
            end
        },
        {
            LocalString.GetString("放入")
        }
    )
	self.m_ActionDataSourcLeft = DefaultItemActionDataSource.Create(
        1,
        {
            function() 
                CItemInfoMgr.CloseItemInfoWnd()
				self:OnTakeOutFromStoreRoom()
            end
        },
        {
            LocalString.GetString("取出")
        }
    )
end

function LuaStoreRoomWnd:InitPackage()
	local packageScript = self.WarehousePackageView.transform:GetComponent(typeof(CCommonLuaScript))
	packageScript.m_LuaSelf.OnPackageItemClick = function(packagePos, itemId)
        self:OnPackageItemClick(packagePos, itemId)
    end	
	packageScript.m_LuaSelf.OnPackageLockedItemClick = function(packagePos, itemId)
		self:OnPackageLockedItemClick()
    end	

	packageScript.m_LuaSelf:Init()
	local packageOverLapBtn = packageScript.transform:Find("Offset/FastOverlapBtn")
	if packageOverLapBtn then
		packageOverLapBtn.gameObject:SetActive(false)
	end
end

function LuaStoreRoomWnd:InitCangkuXingNang(warehouseIndex)
	self.mWarehouseIndex = warehouseIndex
	self.m_ItemLoaded = false
	Extensions.RemoveAllChildren(self.RightViewGrid.transform)
	self.m_CellItems = {}
	if not CClientMainPlayer.Inst then
		return 
	end
	local carry_size = nil
    local has_zuoqi = false
    carry_size, has_zuoqi = CLuaWarehousePage.TryGetMaxZuoQiCarrySize()

	local xingnangItemCount=0
	xingnangItemCount = CLuaWarehouseMgr.GetXingNangItemCount()

	if (not has_zuoqi and xingnangItemCount==0 and warehouseIndex == 3)  then--没坐骑 没物品 行囊
		local messageformat = MessageMgr.Inst:GetMessageFormat("Set_Defult_ZuoQi", true)
        self.RightAvailableLabel.text = messageformat
        self.RightAvailableLabel.gameObject:SetActive(true)
	else
		self.RightAvailableLabel.gameObject:SetActive(false)

		for i = 1, Constants.SingeWarehouseSize, 1 do
			local go = NGUITools.AddChild(self.RightViewGrid.gameObject, self.ItemCell.gameObject)
			go:SetActive(true)
			local cell = go:GetComponent(typeof(CWarehousePackageItemCell))
			cell.OnItemClickDelegate = DelegateFactory.Action_CWarehousePackageItemCell(
				function(item_cell)
					self.m_SelectPos = i
					self:OnRightItemClick(item_cell)
				end
			)
			cell.OnItemDoubleClickDelegate = DelegateFactory.Action_CWarehousePackageItemCell(
				function(item_cell)
					self.m_SelectPos = i
					self:OnRightItemDoubleClick(item_cell)
				end
			)
			local pos_id = i + warehouseIndex * Constants.SingeWarehouseSize
            local itemId = CClientMainPlayer.Inst.RepertoryProp:GetItemAt(pos_id)
            cell:Init(itemId, CLuaWarehousePage.GetRepertoryPosStatus(pos_id))
			table.insert(self.m_CellItems,cell)

		end
		self.m_ItemLoaded = true
		self.RightViewGrid:Reposition()
	end
end

function LuaStoreRoomWnd:InitAnNang()
	Extensions.RemoveAllChildren(self.RightViewGrid.transform)
	self.m_BangCangItems = {}
	if not CClientMainPlayer.Inst then
		return 
	end
	local carry_size = nil
    local has_zuoqi = false
    carry_size, has_zuoqi = CLuaWarehousePage.TryGetMaxZuoQiCarrySize()
	local bangcangItemCount = CLuaBindRepoMgr.GetBangCangItemCount()
	if not CLuaBindRepoMgr.IsLevelMatch() or (not has_zuoqi and bangcangItemCount==0) then --没有坐骑、没有物品
		local messageformat = MessageMgr.Inst:GetMessageFormat("BDCK_STATUS_UNOPEN", true)
        self.RightAvailableLabel.text = messageformat
        self.RightAvailableLabel.gameObject:SetActive(true)
	else
		self.RightAvailableLabel.gameObject:SetActive(false)
		for i = 1, Constants.SingeWarehouseSize, 1 do
			local go = NGUITools.AddChild(self.RightViewGrid.gameObject, self.ItemCell.gameObject)
			go:SetActive(true)
			local cell = go:GetComponent(typeof(CWarehousePackageItemCell))
			cell.OnItemClickDelegate = DelegateFactory.Action_CWarehousePackageItemCell(
				function(item_cell)
					self.m_SelectPos = i
					self:OnRightItemClick(item_cell)
				end
			)
			cell.OnItemDoubleClickDelegate = DelegateFactory.Action_CWarehousePackageItemCell(
				function(item_cell)
					self.m_SelectPos = i
					self:OnRightItemDoubleClick(item_cell)
				end
			)
			table.insert(self.m_BangCangItems,cell)
		end
		self.RightViewGrid:Reposition()
		--self:OnUpdateBindRepo()
		self:RefreshBindRepoData()
	end
end

function LuaStoreRoomWnd:InitCangKu(cangkuIndex)
	self:InitCangkuXingNang(cangkuIndex)
end

function LuaStoreRoomWnd:InitRightView(index)
	self.m_RightViewType = index
	self.WarehousePackageView.gameObject:SetActive(self.m_RightViewType == self.EnumRightType.ePackage)
	self.RightPageView.gameObject:SetActive(self.m_RightViewType ~= self.EnumRightType.ePackage)
	--package
	if self.m_RightViewType == self.EnumRightType.ePackage then
		self:InitPackage()
	elseif self.m_RightViewType == self.EnumRightType.eXingNang then
		self:InitCangkuXingNang(3)
		self.RightArrangeBtn.Text = LocalString.GetString("整理行囊")
	elseif self.m_RightViewType == self.EnumRightType.eAnNang then
		self:InitAnNang()
		self.RightArrangeBtn.Text = LocalString.GetString("整理鞍囊")
	elseif self.m_RightViewType == self.EnumRightType.eCangKu1 then
		self:InitCangKu(0)
		self.RightArrangeBtn.Text = LocalString.GetString("整理仓库")
	elseif self.m_RightViewType == self.EnumRightType.eCangKu2 then
		self:InitCangKu(1)
		self.RightArrangeBtn.Text = LocalString.GetString("整理仓库")
	elseif self.m_RightViewType == self.EnumRightType.eCangKu3 then
		self:InitCangKu(2)
		self.RightArrangeBtn.Text = LocalString.GetString("整理仓库")
	end
	self:InitPageGrid()
	self.RightPackageTypeBtn.Text = self:GetRightTypeName(self.m_RightViewType)
	self.RightArrangeBtn.gameObject:SetActive(self.m_RightViewType ~= self.EnumRightType.ePackage)
end

function LuaStoreRoomWnd:InitPageGrid()
	local unlockPosCount = LuaStoreRoomMgr:GetTotalValidStoreRoomPosCount()
	self.m_UnlockPosCount = unlockPosCount
	self.m_LockedButItemPosCount = LuaStoreRoomMgr:GetLockedButHasItemPosCount()
	self.m_LastUsedPos = LuaStoreRoomMgr:GetLastUsedPos()
	local defaultPagePosCount = StoreRoom_Setting.GetData().RoomPosCountPerPage
	local maxPosCount = 75
	self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            if self.m_UnlockPosCount < defaultPagePosCount then
				local openCount = self.m_UnlockPosCount+self.m_LockedButItemPosCount
				if openCount >= defaultPagePosCount then
					return math.max(math.max(defaultPagePosCount,openCount+5),self.m_LastUsedPos)
				end
				return math.max(math.max(defaultPagePosCount,openCount),self.m_LastUsedPos)
			else
				local openCount = self.m_UnlockPosCount + 5 + self.m_LockedButItemPosCount
				openCount = math.max(openCount,self.m_LastUsedPos)
				return math.min(openCount,maxPosCount)--全部解锁增加显示五个锁定状态的格子
			end
        end,
        function(item,row)
            self:InitStoreRoomItem(item,row)
        end)
	self.TableView:ReloadData(false,false)
end

function LuaStoreRoomWnd:InitStoreRoomItem(item,row)
	local pos = row + 1
	self:InitStoreRoomItemAtRow(item,pos)							
end

function LuaStoreRoomWnd:InitStoreRoomItemAtRow(item,pos)
	local row = tonumber(pos - 1)

	if row < 0 then
		return
	end
	if not item then
		item = self.TableView:GetItemAtRow(row)
	end

	if not item then
		return
	end
	if not CClientMainPlayer.Inst then
		return
	end
	local cell = CommonDefs.GetComponent_GameObject_Type(item.gameObject, typeof(CWarehousePackageItemCell))
	local isLocked = true
	local repoProp = CClientMainPlayer.Inst.RepertoryProp

	if pos <= self.m_UnlockPosCount then
		local itemId = repoProp.StoreRoom[pos] and repoProp.StoreRoom[pos] or ""
		isLocked = false
		cell:Init(itemId, EnumWarehouseItemCellLockStatus.Unlocked)
		self:SetCellMask(cell)
	elseif repoProp.StoreRoom[pos] and (repoProp.StoreRoom[pos] ~= "") then --大月卡解锁的格子，存放了东西，但是大月卡过期了，这种时候应该允许玩家取出
		local itemId = repoProp.StoreRoom[pos] and repoProp.StoreRoom[pos] or ""
		isLocked = false
		cell:Init(itemId, EnumWarehouseItemCellLockStatus.Unlocked)
		self:SetCellMask(cell)
	else
		isLocked = true
		cell:Init("", EnumWarehouseItemCellLockStatus.Locked)
	end
	cell.OnItemClickDelegate = DelegateFactory.Action_CWarehousePackageItemCell(
		function(item_cell)
			self:StoreRoomItemClick(item_cell,isLocked,row+1)
		end
	)
	cell.OnItemDoubleClickDelegate = DelegateFactory.Action_CWarehousePackageItemCell(
		function(item_cell)
			self:OnStoreRoomItemDoubleClick(item_cell,isLocked,row+1)
		end
	)
end

function LuaStoreRoomWnd:SetCellMask(cell)
    local mask = cell.transform:Find("Mask").gameObject
	if cell.ItemId == "" then
		mask:SetActive(false)
		return
	end
	
	if self.m_RightViewType == self.EnumRightType.eAnNang and not self:CheckCanPutInSimpleRepo(cell.ItemId) then
		mask:SetActive(true)
	else
		mask:SetActive(false)
	end 
end

function LuaStoreRoomWnd:CheckCanPutInSimpleRepo(itemId)
    local item = CItemMgr.Inst:GetById(itemId)
    --TODO 读表判断该物品是否可以放入绑仓
    local bindrepo_not_allow = true
    if (item) and item.IsItem then
        bindrepo_not_allow = (BindRepo_AllowedItems.GetData(item.TemplateId) == nil)
    else
        return false
    end
    local item_should_mask = ((item and ((not item.IsBinded) or (CLuaBindRepoMgr.GetItemExpiredTime(item) > 0) or bindrepo_not_allow)))
    if (item_should_mask) then
        return false
    end
    return true
end

function LuaStoreRoomWnd:StoreRoomItemClick(item_cell,isLocked,pos)
    if isLocked then
        --打开解锁界面
        LuaStoreRoomMgr:OpenUnlockPosWnd()
    else
		if not item_cell.ItemId or item_cell.ItemId == "" then
			return
		end
		--取出Tip
		self.m_SelectPos = pos
		local item = CItemMgr.Inst:GetById(item_cell.ItemId)
		if not item then
			self.m_SelectedItemId = nil
			return
		end
		self.m_SelectedItemId = item_cell.ItemId
		--能否放入强化鞍囊
		local showAction =  true
		if self.m_RightViewType == self.EnumRightType.eAnNang and not self:CheckCanPutInSimpleRepo(self.m_SelectedItemId) then
			showAction = false
		end
		CItemInfoMgr.ShowLinkItemInfo(item_cell.ItemId, false, showAction and self.m_ActionDataSourcLeft or nil, CItemInfoMgr.AlignType.ScreenRight, 0, 0, 0, 0)
		--CItemInfoMgr.ShowLinkItemTemplateInfo(item.TemplateId, false, showAction and self.m_ActionDataSourcLeft or nil, CItemInfoMgr.AlignType.ScreenRight, 0, 0, 0, 0)
    end
end

function LuaStoreRoomWnd:OnStoreRoomItemDoubleClick(item_cell,isLocked,pos)
    if isLocked then
        return
    end
	if not item_cell.ItemId or item_cell.ItemId == "" then
		return
	end
    --Gac2Gas.MoveItemFromStoreRoom2Bag(pos,item_cell.ItemId)
	self.m_SelectPos = pos
	self.m_SelectedItemId = item_cell.ItemId
	self:OnTakeOutFromStoreRoom()
end

function LuaStoreRoomWnd:GetRightTypeName(rightType)
	local isLocked = false
	
	if rightType == self.EnumRightType.ePackage then
		return LocalString.GetString("我的包裹"),isLocked
	elseif rightType == self.EnumRightType.eXingNang then
		return LocalString.GetString("行囊"),isLocked
	elseif rightType == self.EnumRightType.eAnNang then
		return LocalString.GetString("强化鞍囊"),isLocked
	elseif rightType == self.EnumRightType.eCangKu1 then
		local startId = 1 + 0 * Constants.SingeWarehouseSize
		local status = CLuaWarehousePage.GetRepertoryPosStatus(startId)
		isLocked = status ~= EnumWarehouseItemCellLockStatus.Unlocked
		return LocalString.GetString("仓库1"),isLocked
	elseif rightType == self.EnumRightType.eCangKu2 then
		local startId = 1 + 1 * Constants.SingeWarehouseSize
		local status = CLuaWarehousePage.GetRepertoryPosStatus(startId)
		isLocked = status ~= EnumWarehouseItemCellLockStatus.Unlocked
		return LocalString.GetString("仓库2"),isLocked
	elseif rightType == self.EnumRightType.eCangKu3 then
		local startId = 1 + 2 * Constants.SingeWarehouseSize
		local status = CLuaWarehousePage.GetRepertoryPosStatus(startId)
		isLocked = status ~= EnumWarehouseItemCellLockStatus.Unlocked
		return LocalString.GetString("仓库3"),isLocked
	else
		return "",true
	end
end
--@region UIEventpage	

--整理府库
function LuaStoreRoomWnd:OnArrangeStoreRoomBtnClick()
	Gac2Gas.RequestReorderStoreRoom()
end

function LuaStoreRoomWnd:OnRightPackageTypeBtnClick(go)
	local b = NGUIMath.CalculateRelativeWidgetBounds(go.transform)
	local menus = {}

	for rightType=0,5,1 do
		local btnName,isLocked = self:GetRightTypeName(rightType)
		table.insert(menus, {text = btnName, locked = isLocked} )
	end

	CLuaWarehouseMenuMgr:ShowOrCloseMenu(self.m_RightViewType, menus, self, go.transform:TransformPoint(Vector3(b.center.x, b.center.y - b.extents.y, b.center.z)),true)
end

function LuaStoreRoomWnd:OnRightOverlapBtnClick()
end

function LuaStoreRoomWnd:OnRightArrangeBtnClick()
	--整理其他
	if self.m_RightViewType == self.EnumRightType.eXingNang then
		Gac2Gas.RepertoryReorder()
	elseif self.m_RightViewType == self.EnumRightType.eAnNang then
		Gac2Gas.SimpleItemRepoReorder()
	elseif self.m_RightViewType == self.EnumRightType.eCangKu1 then
		Gac2Gas.RepertoryReorder()
	elseif self.m_RightViewType == self.EnumRightType.eCangKu2 then
		Gac2Gas.RepertoryReorder()
	elseif self.m_RightViewType == self.EnumRightType.eCangKu3 then
		Gac2Gas.RepertoryReorder()
	end	
end

--@endregion UIEvent

---------------------------------------------PopMenu
function LuaStoreRoomWnd:OnMenuAppear()
	self.TypeArrow.flip = UIBasicSprite.Flip.Vertically
end

function LuaStoreRoomWnd:OnMenuDisappear()
	self.TypeArrow.flip = UIBasicSprite.Flip.Nothing
end

function LuaStoreRoomWnd:OnMenuItemClick(selectedIndex)
	local _,isLocked = self:GetRightTypeName(selectedIndex)
	if isLocked then
		if selectedIndex == self.EnumRightType.eCangKu2 or selectedIndex == self.EnumRightType.eCangKu3 then
			local _,isLocked2 = self:GetRightTypeName(self.EnumRightType.eCangKu2)
			local pageIndex = isLocked2 and 1 or 2
            local setting = GameSetting_Common.GetData()
            local prices = setting.RepertoryBuyPrice
            if(prices ~= nil and prices.Length >= pageIndex and pageIndex > 0)then
                MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("OPEN_REPERTORY_WITH_MONEY", prices[pageIndex - 1]), DelegateFactory.Action(function()
                    Gac2Gas.OpenNewRepertory()
                end), nil, nil, nil, false)
            end
			CUIManager.CloseUI(CUIResources.WarehouseMenu)
			return
		else
			CUIManager.CloseUI(CUIResources.WarehouseMenu)
			return
		end
	end
	self:InitRightView(selectedIndex)
	CUIManager.CloseUI(CUIResources.WarehouseMenu)
end

-------------------------------------------Package
function LuaStoreRoomWnd:OnPackageItemClick(packagePos, itemId)
	Gac2Gas.MoveItemFromBag2StoreRoom(packagePos, itemId)
end

function LuaStoreRoomWnd:OnPackageLockedItemClick()
	CExpandPackageMgr.Inst:ShowExpandPackageWnd()
end

function LuaStoreRoomWnd:OnSyncItemBagSize()
	self:InitRightView(self.m_RightViewType)
end
-------------------------------------------绑定仓库
--绑仓更新
function LuaStoreRoomWnd:OnUpdateBindRepo()
	if not CClientMainPlayer.Inst then
		return
	end

    local type = self.m_RightViewType
    if type == self.EnumRightType.eAnNang then
        local data = CClientMainPlayer.Inst.ItemProp.SimpleItems
        local items = data.Items
        local counts = data.Count
		local warehouseIndex = 5
		self.mWarehouseIndex = warehouseIndex
        -- local num = data.UsableSize
        local bangcang_start_id = (warehouseIndex - CLuaWarehouseView.m_BangCangStartId) * Constants.SingeWarehouseSize + 1--item array的长度似乎比总长度大1
        do
            local cell_id = 0
            local item_count = items and items.Length or 0
            while(cell_id < Constants.SingeWarehouseSize and bangcang_start_id < item_count)do
                local item_id = items[bangcang_start_id]
                self:InitBindRepoItem(cell_id, item_id, counts[bangcang_start_id])
                self:ModifyItemCellForBangCang(self.m_BangCangItems[cell_id + 1], cell_id)
                cell_id = cell_id + 1
                bangcang_start_id = bangcang_start_id + 1
            end

            while(cell_id < Constants.SingeWarehouseSize)do
                self.m_BangCangItems[cell_id + 1]:Init(nil, CLuaWarehousePage.GetRepertoryPosStatus(cell_id + 1 + warehouseIndex * Constants.SingeWarehouseSize))
                self:ModifyItemCellForBangCang(self.m_BangCangItems[cell_id + 1], cell_id)
                cell_id = cell_id + 1
            end
        end 
    end
end
function LuaStoreRoomWnd:InitBindRepoItem(cell_id, _templateid, _count)
    local item_cell = self.m_BangCangItems[cell_id + 1]
	local warehouseIndex = 5
	self.mWarehouseIndex = warehouseIndex
    item_cell:Init("", CLuaWarehousePage.GetRepertoryPosStatus(cell_id + 1 + warehouseIndex * Constants.SingeWarehouseSize))
    if(_templateid ~= 0)then
        local item_template = CItemMgr.Inst:GetItemTemplate(_templateid)
        local icon_texture = item_cell.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
        local bind_sprite = item_cell.transform:Find("BindSprite"):GetComponent(typeof(UISprite))
        local amount_label = item_cell.transform:Find("AmountLabel"):GetComponent(typeof(UILabel))
        local quality_sprite = item_cell.transform:Find("QualitySprite"):GetComponent(typeof(UISprite))
        icon_texture:LoadMaterial(item_template.Icon)
        bind_sprite.spriteName = Constants.ItemBindedSpriteName
        quality_sprite.spriteName = CUICommonDef.GetItemCellBorder(item_template.NameColor)
        amount_label.text = (_count > 1) and tostring(_count) or ""
    end
end
--TODO与服务器衔接,判断是否为可开启绑仓格子
function LuaStoreRoomWnd:ModifyItemCellForBangCang(_cell, local_id)
	if not CClientMainPlayer.Inst then
		return
	end
    local type = self.m_RightViewType
	local warehouseIndex = 5
    if type == self.EnumRightType.eAnNang then
        local bangcang_cellid = (warehouseIndex - CLuaWarehouseView.m_BangCangStartId) * Constants.SingeWarehouseSize + local_id
        local data = CClientMainPlayer.Inst.ItemProp.SimpleItems
        -- local bindrepo_setting = BindRepo_Setting.GetData()
        local usable_size = CLuaWarehousePage.GetUsableSize()

        if(bangcang_cellid >= usable_size and bangcang_cellid < CLuaWarehousePage.m_BangCangAvailable)then
            local label = _cell.transform:Find("TextLabel"):GetComponent(typeof(UILabel))
            label.text = "[ACF8FF]"..LocalString.GetString("可开启").."[-]"
        end
    end
end

function LuaStoreRoomWnd:ExpandBangCangSize()
	local carry_size, has_zuoqi = CLuaWarehousePage.TryGetMaxZuoQiCarrySize()
    if not has_zuoqi then
        --没有坐骑的时候 点击“可开启”按钮提示一条消息
        g_MessageMgr:ShowMessage("ADD_SIMPLE_ITEM_NO_ZUOQI")
        return
    end
    -- --TODO 服务器衔接,替换银票数值
	if not CClientMainPlayer.Inst then
		return
	end
    local cost = CLuaWarehousePage.BindRepoUnlockPrice()
    local freeSilver = CClientMainPlayer.Inst.FreeSilver
    if freeSilver<cost then
        --BindRepo_Open_NeedYinLiang
        local message = g_MessageMgr:FormatMessage("BindRepo_Open_NeedYinLiang", cost-freeSilver)
        MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function ()
            Gac2Gas.RequestBuyOneSimpleItemSlot()
        end), nil, LocalString.GetString("开启"), nil, false)
    end

    local message = g_MessageMgr:FormatMessage("BDCK_OPENLATTICE_CONFIRM", tostring(CLuaWarehousePage.BindRepoUnlockPrice()))
    MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function ()
        Gac2Gas.RequestBuyOneSimpleItemSlot()
    end), nil, LocalString.GetString("开启"), nil, false)
end

function LuaStoreRoomWnd:OnClickLockedPos(_itemCell)
	if (self.m_RightViewType == self.EnumRightType.eCangKu1 or self.m_RightViewType == self.EnumRightType.eCangKu2 or self.m_RightViewType == self.EnumRightType.eCangKu3) then
		local repoIndex = self.m_SelectPos + self.mWarehouseIndex * Constants.SingeWarehouseSize
		if not CClientMainPlayer.Inst then
			return
		end
		local minVipLevel = CClientMainPlayer.Inst:MinVipLevelToAccessRepertoryPos(repoIndex)
		g_MessageMgr:ShowMessage("VIP_UNLOCK_THE_LINE", minVipLevel)
	elseif(self.m_RightViewType == self.EnumRightType.eXingNang)then
		self:ExpandZuoQiBagSize()
	elseif(self.m_RightViewType == self.EnumRightType.eAnNang)then
		--TODO 与服务器衔接，已锁定绑仓格子提示
		g_MessageMgr:ShowMessage("BDCK_OPENLATTICE_ERROR_UNOPEN", "1", tostring(CLuaWarehousePage.BindRepoUnlockPrice()))
	end
end

function LuaStoreRoomWnd:PutInStoreRoom()
	CItemInfoMgr.CloseItemInfoWnd()
	self:OnPutInStoreRoom()
end

function LuaStoreRoomWnd:OnRightItemClick(item_cell)
	if not CClientMainPlayer.Inst then
		return
	end
	--锁住的格子
	if item_cell.LockStatus ==EnumWarehouseItemCellLockStatus.Locked then
		--注意 行囊可能会出现格子是锁住的，但是格子里有东西，这种情况格子里的东西应该允许被取出来
		if not item_cell.ItemId or item_cell.ItemId == "" then
			self:OnClickLockedPos(item_cell)
			return
		end
	end
	
	--单独处理绑仓
	if self.m_RightViewType == self.EnumRightType.eAnNang then
		local data = CClientMainPlayer.Inst.ItemProp.SimpleItems
		local usable_size = CLuaWarehousePage.GetUsableSize()
		local localIdx = self.m_SelectPos
		--TODO 与服务器衔接，判断是否为可开启绑仓格子
		if(localIdx <= usable_size)then
			if(localIdx < data.Items.Length)then
				local template_id = data.Items[localIdx]
				if(template_id ~= 0)then
					CItemInfoMgr.ShowLinkItemTemplateInfo(template_id, false, self.m_ActionDataSourc, CItemInfoMgr.AlignType.ScreenLeft, 0, 0, 0, 0)
				end
			end
		else
			self:ExpandBangCangSize()
		end
		return
	end

    local item = CItemMgr.Inst:GetById(item_cell.ItemId)
	if not item then
		self.m_SelectedItemId = nil
		return
	end
	self.m_SelectedItemId = item_cell.ItemId
	CItemInfoMgr.ShowLinkItemInfo(item_cell.ItemId, false, self.m_ActionDataSourc, CItemInfoMgr.AlignType.ScreenLeft, 0, 0, 0, 0)
end

function LuaStoreRoomWnd:OnRightItemDoubleClick(item_cell)
	if item_cell.LockStatus ==EnumWarehouseItemCellLockStatus.Locked then
		return
	end
	local item = CItemMgr.Inst:GetById(item_cell.ItemId)
	self.m_SelectedItemId = item_cell.ItemId
	self:OnPutInStoreRoom()
end

function LuaStoreRoomWnd:OnPutInStoreRoom()
	if not CClientMainPlayer.Inst then
		return
	end
	--单独处理绑仓
	if self.m_RightViewType == self.EnumRightType.eAnNang then
		local data = CClientMainPlayer.Inst.ItemProp.SimpleItems
		local usable_size = CLuaWarehousePage.GetUsableSize()
		local localIdx = self.m_SelectPos
		--TODO 与服务器衔接，判断是否为可开启绑仓格子
		if(localIdx <= usable_size)then
			if(localIdx < data.Items.Length)then
				local template_id = data.Items[localIdx]
				if(template_id ~= 0)then
					local count = data.Count[localIdx]
					Gac2Gas.MoveSimpleItemFromSimpleRepoToStoreRoom(self.m_SelectPos,template_id,count)
				end
			end
		else
			self:ExpandBangCangSize()
		end
		return
	end

	if not self.m_SelectedItemId then
		return
	end
	local pos = self.m_SelectPos
	local itemId = self.m_SelectedItemId
	local item = CItemMgr.Inst:GetById(itemId)
	if not item then
		return
	end
	local templateId = item.TemplateId
	local count = item.Item and item.Item.Count or 1

	if self.m_RightViewType == self.EnumRightType.ePackage then

	elseif self.m_RightViewType == self.EnumRightType.eXingNang then
		pos = self.m_SelectPos + self.mWarehouseIndex * Constants.SingeWarehouseSize
		Gac2Gas.MoveItemFromRepo2StoreRoom(pos,itemId)
	elseif self.m_RightViewType == self.EnumRightType.eAnNang then		
		Gac2Gas.MoveSimpleItemFromSimpleRepoToStoreRoom(pos,templateId,count)
	elseif self.m_RightViewType == self.EnumRightType.eCangKu1 then
		pos = self.m_SelectPos + self.mWarehouseIndex * Constants.SingeWarehouseSize
		Gac2Gas.MoveItemFromRepo2StoreRoom(pos,itemId)
	elseif self.m_RightViewType == self.EnumRightType.eCangKu2 then
		pos = self.m_SelectPos + self.mWarehouseIndex * Constants.SingeWarehouseSize
		Gac2Gas.MoveItemFromRepo2StoreRoom(pos,itemId)
	elseif self.m_RightViewType == self.EnumRightType.eCangKu3 then
		pos = self.m_SelectPos + self.mWarehouseIndex * Constants.SingeWarehouseSize
		Gac2Gas.MoveItemFromRepo2StoreRoom(pos,itemId)
	end	
end

function LuaStoreRoomWnd:OnTakeOutFromStoreRoom()
	if not self.m_SelectedItemId then
		return
	end
	local pos = self.m_SelectPos
	local itemId = self.m_SelectedItemId

	if self.m_RightViewType == self.EnumRightType.ePackage then
		--MoveItemFromStoreRoom2Bag()
		Gac2Gas.MoveItemFromStoreRoom2Bag(pos,itemId)
	elseif self.m_RightViewType == self.EnumRightType.eXingNang then
		Gac2Gas.MoveItemFromStoreRoom2Repo(pos,itemId)
	elseif self.m_RightViewType == self.EnumRightType.eAnNang then
		if not self:CheckCanPutInSimpleRepo(self.m_SelectedItemId) then
			return
		end
		Gac2Gas.MoveItemFromStoreRoom2SimpleRepo(pos,itemId)
	elseif self.m_RightViewType == self.EnumRightType.eCangKu1 then
		Gac2Gas.MoveItemFromStoreRoom2Repo(pos,itemId)
	elseif self.m_RightViewType == self.EnumRightType.eCangKu2 then
		Gac2Gas.MoveItemFromStoreRoom2Repo(pos,itemId)
	elseif self.m_RightViewType == self.EnumRightType.eCangKu3 then
		Gac2Gas.MoveItemFromStoreRoom2Repo(pos,itemId)
	end	
end
------------------------------------------仓库
function LuaStoreRoomWnd:OnPackageWarehouseOpen(args)
    
end
--普通仓库、行囊的更新
function LuaStoreRoomWnd:OnSetWarehouseItemAt(pos, itemId)
    if(CClientMainPlayer.Inst and self.m_ItemLoaded)then
        if( (pos > self.mWarehouseIndex * Constants.SingeWarehouseSize) and (pos <= (self.mWarehouseIndex + 1) * Constants.SingeWarehouseSize) )then
            self.m_CellItems[(pos - self.mWarehouseIndex * Constants.SingeWarehouseSize)]:Init(itemId, CLuaWarehousePage.GetRepertoryPosStatus(pos))
        end
    end
end

function LuaStoreRoomWnd:OnSendItem(args)
	
	local itemId = args[0]
	if(itemId == nil or itemId == "")then
        return
    end
	--刷新仓库
	if self.m_CellItems and next(self.m_CellItems) then
		local n = #self.m_CellItems
		for i = 1, n, 1 do
			if(self.m_CellItems[i].ItemId == itemId)then
				self.m_CellItems[i]:UpdateAmount()
				break
			end
		end
	end
    
	--刷新府库
	if CClientMainPlayer.Inst then
		local repoProp = CClientMainPlayer.Inst.RepertoryProp
		for pos = 1,self.m_UnlockPosCount,1 do
			if itemId == repoProp.StoreRoom[pos] then
				self:InitStoreRoomItemAtRow(nil,pos)
				break
			end
		end
	end
end

function LuaStoreRoomWnd:ExpandZuoQiBagSize()
	if(CClientMainPlayer.Inst)then
        --首先判断是否有坐骑
        local carry_size, has_zuoqi = CLuaWarehousePage.TryGetMaxZuoQiCarrySize()

        if has_zuoqi and carry_size==0 then
            --坐骑的格子数是0，这个时候不允许开格子
            g_MessageMgr:ShowMessage("XingNang_ZuoQi_NoSize_Unlock")
            return
        end

        local zuoqi_extra_bag_size = CClientMainPlayer.Inst.ItemProp.ExtraRiderBagSize
        local firstUnlockedIndex = zuoqi_extra_bag_size + 1
        local consumeCount = CClientMainPlayer.Inst:GetRiderExtraBagSizePrice(firstUnlockedIndex)
        if(consumeCount > 0)then
            local messageformat = MessageMgr.Inst:GetMessageFormat("XINGNANG_UNLOCK_TIPS", true)
            LuaItemInfoMgr:ShowItemConsumeWndWithQuickAccess(ZuoQi_Setting.GetData().ExtraBagConsumeItemId, consumeCount, messageformat, true, false, LocalString.GetString("解锁"), function (enough)
                Gac2Gas.RequestIncRiderExtraBagSize(1)
            end, nil, false)
        end
    end
end

function LuaStoreRoomWnd:OnSyncZuoqiExtraBagSize()
	self:InitRightView(self.m_RightViewType)
	self:RefreshBindRepoData()
end

function LuaStoreRoomWnd:RefreshBindRepoData()
	if not CClientMainPlayer.Inst then
		return
	end
    local bindrepo_setting = BindRepo_Setting.GetData()
    local data = CClientMainPlayer.Inst.ItemProp.SimpleItems
    local unlock_formula = AllFormulas.Action_Formula[tonumber(bindrepo_setting.UnlockFormula)] and AllFormulas.Action_Formula[tonumber(bindrepo_setting.UnlockFormula)].Formula or nil
    if(unlock_formula)then
        local arg = CClientMainPlayer.Inst.ItemProp.ExtraRiderBagSize-- + carry_size
        CLuaWarehousePage.m_BangCangAvailable = math.max(unlock_formula(nil, nil, {arg}), CLuaWarehousePage.GetUsableSize())
        CLuaWarehousePage.m_BangCangUsable = CLuaWarehousePage.GetUsableSize()
    end
    self:OnUpdateBindRepo()
end
-----------------------------------------府库
function LuaStoreRoomWnd:OnSyncStoreRoomPosCount(itemUnlockCount, silverUnlockCount, jadeUnlockCount)
	self:InitPageGrid()
end

function LuaStoreRoomWnd:OnSyncStoreRoomItem(pos,itemId)--IS
	if CClientMainPlayer.Inst then
		local repoProp = CClientMainPlayer.Inst.RepertoryProp
		repoProp.StoreRoom[pos] = itemId
		self:InitStoreRoomItemAtRow(nil,pos)
	end
	--close
	CUIManager.CloseUI(CIndirectUIResources.ItemInfoWnd)
    CUIManager.CloseUI(CIndirectUIResources.EquipInfoWnd)
end

function LuaStoreRoomWnd:UpdateWarehouse()
	self:Init()
end

function LuaStoreRoomWnd:OnEnable()
	if(not self.m_OnSetWarehouseItemAtEvent)then
        self.m_OnSetWarehouseItemAtEvent = DelegateFactory.Action_uint_string(
            function(pos, itemId)
                self:OnSetWarehouseItemAt(pos, itemId)
            end
        )
    end
    EventManager.AddListenerInternal(EnumEventType.SetWarehouseItemAt, self.m_OnSetWarehouseItemAtEvent)

	if(not self.m_OnSyncZuoqiExtraBagSizeEvent)then
        self.m_OnSyncZuoqiExtraBagSizeEvent = DelegateFactory.Action(
            function()
                self:OnSyncZuoqiExtraBagSize()
            end
        )
    end
    EventManager.AddListener(EnumEventType.SyncZuoqiExtraBagSize, self.m_OnSyncZuoqiExtraBagSizeEvent)

	if(not self.m_OnSyncItemBagSizeEvent)then
        self.m_OnSyncItemBagSizeEvent = DelegateFactory.Action(
            function()
                self:OnSyncItemBagSize()
            end
        )
    end
    EventManager.AddListener(EnumEventType.SyncItemBagSize, self.m_OnSyncItemBagSizeEvent)

	g_ScriptEvent:AddListener("SyncStoreRoomPosCount", self, "OnSyncStoreRoomPosCount")
	g_ScriptEvent:AddListener("OnUpdateAllBindRepo", self, "OnUpdateBindRepo")
	g_ScriptEvent:AddListener("SendItem", self, "OnSendItem")
	g_ScriptEvent:AddListener("SyncStoreRoomItem", self, "OnSyncStoreRoomItem")
	g_ScriptEvent:AddListener("OnWarehouseNumUpdate", self, "UpdateWarehouse")
end

function LuaStoreRoomWnd:OnDisable()
	CLuaWarehouseMenuMgr:CloseMenu()

	if(self.m_OnSetWarehouseItemAtEvent)then
        EventManager.RemoveListenerInternal(EnumEventType.SetWarehouseItemAt, self.m_OnSetWarehouseItemAtEvent)
    end

	if(self.m_OnSyncZuoqiExtraBagSizeEvent)then
        EventManager.RemoveListener(EnumEventType.SyncZuoqiExtraBagSize, self.m_OnSyncZuoqiExtraBagSizeEvent)
    end

	if(self.m_OnSyncItemBagSizeEvent)then
        EventManager.RemoveListener(EnumEventType.SyncItemBagSize, self.m_OnSyncItemBagSizeEvent)
    end

	g_ScriptEvent:RemoveListener("SyncStoreRoomPosCount", self, "OnSyncStoreRoomPosCount")
	g_ScriptEvent:RemoveListener("OnUpdateAllBindRepo", self, "OnUpdateBindRepo")
	g_ScriptEvent:RemoveListener("SendItem", self, "OnSendItem")
	g_ScriptEvent:RemoveListener("SyncStoreRoomItem", self, "OnSyncStoreRoomItem")
	g_ScriptEvent:RemoveListener("OnWarehouseNumUpdate", self, "UpdateWarehouse")
end
------------------------------------引导-------------------
function LuaStoreRoomWnd:GetGuideGo(methodName)
    if methodName == "GetPackageTypeBtn" then
        return self.RightPackageTypeBtn.gameObject
	elseif methodName == "GetFirstLockedBtn" then
		local unlockPosCount = LuaStoreRoomMgr:GetTotalValidStoreRoomPosCount()
		local item = self.TableView:GetItemAtRow(unlockPosCount)
		
		if item then
			return item.gameObject
		end
    end
    return nil
end