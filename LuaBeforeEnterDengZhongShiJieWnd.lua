local GameObject = import "UnityEngine.GameObject"

local QnTableView = import "L10.UI.QnTableView"
local Gac2Login = import "L10.Game.Gac2Login"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CUITexture = import "L10.UI.CUITexture"
local CLoginMgr = import "L10.Game.CLoginMgr"
local EasyTouch = import "EasyTouch"
local Animation = import "UnityEngine.Animation"
local SoundManager=import "SoundManager"
local CMainCamera = import "L10.Engine.CMainCamera"
local PlayerSettings = import "L10.Game.PlayerSettings"
local Animator = import "UnityEngine.Animator"
local CRenderScene = import "L10.Engine.Scene.CRenderScene"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local Vector3=import "UnityEngine.Vector3"
local CLoadingWnd = import "L10.UI.CLoadingWnd"
local EnumClass = import "L10.Game.EnumClass"
local UIGrid = import "UIGrid"
local Random = import "UnityEngine.Random"
local QnButton = import "L10.UI.QnButton"

LuaBeforeEnterDengZhongShiJieWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaBeforeEnterDengZhongShiJieWnd, "EllipticRoot", "EllipticRoot", GameObject)
RegistChildComponent(LuaBeforeEnterDengZhongShiJieWnd, "Pool", "Pool", GameObject)
RegistChildComponent(LuaBeforeEnterDengZhongShiJieWnd, "ScrollView", "ScrollView", GameObject)
RegistChildComponent(LuaBeforeEnterDengZhongShiJieWnd, "BackButton", "BackButton", GameObject)
RegistChildComponent(LuaBeforeEnterDengZhongShiJieWnd, "LeftBtn", "beforeenterdengzhongshijiewnd_arrow_left", GameObject)
RegistChildComponent(LuaBeforeEnterDengZhongShiJieWnd, "RightBtn", "beforeenterdengzhongshijiewnd_arrow_right", GameObject)
RegistChildComponent(LuaBeforeEnterDengZhongShiJieWnd, "IndicatorGrid", "IndicatorGrid", UIGrid)
RegistChildComponent(LuaBeforeEnterDengZhongShiJieWnd, "IndicatorTemplate", "IndicatorTemplate", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaBeforeEnterDengZhongShiJieWnd,"m_JobList")
RegistClassMember(LuaBeforeEnterDengZhongShiJieWnd,"m_JobCount")
RegistClassMember(LuaBeforeEnterDengZhongShiJieWnd,"m_JobItems")
RegistClassMember(LuaBeforeEnterDengZhongShiJieWnd,"m_A")
RegistClassMember(LuaBeforeEnterDengZhongShiJieWnd,"m_B")
RegistClassMember(LuaBeforeEnterDengZhongShiJieWnd,"m_ShowTick")
RegistClassMember(LuaBeforeEnterDengZhongShiJieWnd,"WaterBg")
RegistClassMember(LuaBeforeEnterDengZhongShiJieWnd,"m_ClickTick")
RegistClassMember(LuaBeforeEnterDengZhongShiJieWnd,"m_CloseTick")
RegistClassMember(LuaBeforeEnterDengZhongShiJieWnd,"m_ClickAngle")
function LuaBeforeEnterDengZhongShiJieWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    LuaCharacterCreation2023Mgr.m_LastCjName = nil
    self.m_JobList = {}
    self.m_JobCount = 0
    Initialization_Init.Foreach(function(k,v)
        local class = math.floor(k/100)
        if not self.m_JobList[class]  then
            self.m_JobList[class] = v.ID
            self.m_JobCount = self.m_JobCount + 1
        end     
    end)

    self.m_JobItems = {}

    for i=1,13,1 do
        local idx = i<10 and "0"..i or i
        local item = self.Pool.transform:Find("JobItem"..idx)
        table.insert(self.m_JobItems,item)
    end

    self.WaterBg = self.transform:Find("Background/beforeenterdengzhongshijiewnd_water_bg").gameObject
    self.HuDieAnimator = self.transform:Find("HuDie/hudie001").gameObject:GetComponent(typeof(Animator))

    --椭圆参数
    self.m_A = 756
    self.m_B = 305

    self.angle = 0
    self.speed = 2
    self.m_ClickAngle = 1

    if LuaCloudGameFaceMgr:IsDaShenCloudGameFace() then self.BackButton:SetActive(false) end
    UIEventListener.Get(self.BackButton).onClick = DelegateFactory.VoidDelegate(function (go)
        if CLoginMgr.Inst.m_ExistingPlayerList.Count > 0 then
            LuaCharacterCreation2023Mgr.ForceOpenDengzhongshijie = false
            LuaCharacterCreation2023Mgr.ForceInitExitingView = true
            LuaCharacterCreation2023Mgr:OpenCreatingOrExitingWnd()
            CMainCamera.Inst:SetCameraEnableStatus(false, "Enter_ChuangJue", false)
            CUIManager.CloseUI(CUIResources.BeforeEnterDengZhongShiJieWnd)
            return
        end
        if LuaCloudGameFaceMgr:IsCloudGameFace() then
            LuaCloudGameFaceMgr:OnReturnClick()
            return
        end
        CLoginMgr.Inst:Disconnect()
        LuaCharacterCreation2023Mgr:ShowCrossingFx()
        if self.m_ShowWndTick then
            UnRegisterTick(self.m_ShowWndTick)
            self.m_ShowWndTick = nil
        end
        self.m_ShowWndTick = RegisterTickOnce(function()
            CUIManager.ShowUIGroup(CUIManager.EUIModuleGroup.EServerWndUI)
            CUIManager.CloseUI(CUIResources.BeforeEnterDengZhongShiJieWnd)
            CMainCamera.Inst:SetCameraEnableStatus(true, "Enter_ChuangJue", false)
            LuaLoginMgr:HideQueueStatusTopNotice()
		    SoundManager.s_BlockBgMusic = false
        end,1000)
		
    end)
    if self.HuDieAnimator then
        self.HuDieAnimator:Play("nielian_hudie001_man01")
    end

end

function LuaBeforeEnterDengZhongShiJieWnd:OnEnable()
    g_ScriptEvent:AddListener("SendQueueStatus", self, "OnSendQueueStatus")
    local bgmevent = "event:/L10/L10_music/MUS_2023ChuangJue/ChuangJue_loop"
    SoundManager.s_BlockBgMusic = true
    self.m_Music = SoundManager.Inst:PlaySound(bgmevent,Vector3.zero, 1, nil, 0)
    if not LuaCloudGameFaceMgr:IsCloudGameFace() then
        CUIManager.ShowUI("TopNoticeCenter")
    end
    CUIManager.CloseUI("LoginQueueWnd")
    CMainCamera.Inst:SetCameraEnableStatus(false, "Enter_ChuangJue", false)
end

function LuaBeforeEnterDengZhongShiJieWnd:Update()
    SoundManager.Inst:StopBGMusic()
end
function LuaBeforeEnterDengZhongShiJieWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendQueueStatus", self, "OnSendQueueStatus")
    if self.m_ShowTick then
        UnRegisterTick(self.m_ShowTick)
        self.m_ShowTick = nil
    end
    if self.m_ClickTick then
        UnRegisterTick(self.m_ClickTick)
        self.m_ClickTick = nil
    end
    if self.m_CloseTick then
        UnRegisterTick(self.m_CloseTick)
        self.m_CloseTick = nil
    end
    if self.m_RotateAniTick then
        UnRegisterTick(self.m_RotateAniTick)
        self.m_RotateAniTick = nil
    end
    if self.m_ShowWndTick then
        UnRegisterTick(self.m_ShowWndTick)
        self.m_ShowWndTick = nil
    end
    SoundManager.Inst:StopSound(self.m_Music)
    --LuaCharacterCreation2023Mgr.ForceOpenDengzhongshijie = false
end

function LuaBeforeEnterDengZhongShiJieWnd:Init()
    if self:HandleAutoLoginByCmdLineArgs() then --qa客户端机器人自动登录逻辑
        CUIManager.CloseUI(CUIResources.BeforeEnterDengZhongShiJieWnd)
        return
    end
    if CLoginMgr.Inst and CLoginMgr.Inst.m_ExistingPlayerList.Count > 0 and not LuaCharacterCreation2023Mgr.ForceOpenDengzhongshijie then
        LuaCharacterCreation2023Mgr:OpenCreatingOrExitingWnd()
        CUIManager.CloseUI(CUIResources.BeforeEnterDengZhongShiJieWnd)
        return
    end
    
    --随机从某个职业开始显示
    self.m_StartRow = math.floor(Random.Range(0,13))
    for i=0,self.m_JobCount-1,1 do
        local item = self.m_JobItems[i+1]
        local row = i
        self:InitItem(item,row)
    end
    if self.m_ShowTick then
        UnRegisterTick(self.m_ShowTick)
        self.m_ShowTick = nil
    end
    if self.HuDieAnimator then
        self.HuDieAnimator:Play("nielian_hudie001_kuai01")
    end
    self:InitIndicators()
    self.m_ShowTick = RegisterTickOnce(function()
        local ani = self.transform:GetComponent(typeof(Animation))
        ani:Stop()
        self:InitElliptic2()
        self:RegisterJobItemEvents()
    end,2000)
    
    LuaPinchFaceMgr:ShowWndWithCache()

    --reset bgm
    LuaCharacterCreation2023Mgr.m_LastSceneName = nil
    if LuaCharacterCreation2023Mgr.BGM then
        SoundManager.Inst:StopSound(LuaCharacterCreation2023Mgr.BGM)
        LuaCharacterCreation2023Mgr.BGM = nil
    end
end

function LuaBeforeEnterDengZhongShiJieWnd:InitIndicators()
    Extensions.RemoveAllChildren(self.IndicatorGrid.transform)
	self.IndicatorTemplate:SetActive(false)
	for i=1,#self.m_JobItems,1 do
		local g = NGUITools.AddChild(self.IndicatorGrid.gameObject,self.IndicatorTemplate)
        g:SetActive(true)
        local sp = g:GetComponent(typeof(UISprite))
		sp.color = NGUIText.ParseColor24("362D59", 0)
		sp.alpha = 0.5
	end
	self.IndicatorGrid:Reposition()
end

function LuaBeforeEnterDengZhongShiJieWnd:UpdateIndicators()
    local highlightRow = 0
    local mixDist = self.m_A
    for i = 0, self.m_JobCount - 1 do
        local item = self.m_JobItems[i + 1]
        local pos = item.transform.localPosition
        if math.abs(pos.x) < mixDist and pos.y <= 0 then
            highlightRow = i
            mixDist = math.abs(pos.x)
        end
    end
    local highlightClass = (highlightRow + self.m_StartRow) % self.m_JobCount + 1
    --Indicators
	for i=1,#self.m_JobItems,1 do
		local g = self.IndicatorGrid.transform:GetChild(i-1)
		if g then
			local sp = g:GetComponent(typeof(UISprite))
			if sp then
				if i == highlightClass then
					sp.color = NGUIText.ParseColor24("7E66E4", 0)
					sp.alpha = 0.8
				else
					sp.color = NGUIText.ParseColor24("362D59", 0)
					sp.alpha = 0.5
				end
			end
		end
	end
end

function LuaBeforeEnterDengZhongShiJieWnd:HandleAutoLoginByCmdLineArgs()
    if CLoginMgr.Inst:NeedAutoLoginByCmdLineArgs() then
        if  CLoginMgr.Inst.m_ExistingPlayerList.Count == 0 then
            math.randomseed(os.time()) --不添加这一句每次运行客户端到这里得到的随机结果是一样的，导致新建角色会重名
            local classval = math.random(1, EnumToInt(EnumClass.Undefined)-1)
            LuaCharacterCreation2023Mgr:OpenCreatingOrExitingWnd(classval)
        else
            LuaCharacterCreation2023Mgr:OpenCreatingOrExitingWnd()
        end
        return true
    else
        return false
    end
end

function LuaBeforeEnterDengZhongShiJieWnd:InitElliptic2()
    local spaceAngle = 360/self.m_JobCount
    for i=0,self.m_JobCount-1,1 do
        local item = self.m_JobItems[i+1]
        local angle = -180 + spaceAngle*i

        local wpos = item.transform.position
        item.transform.parent = self.EllipticRoot.transform
        item.transform.position = wpos

        local pos = self:GetPositionOnEllipse(angle)
        item.transform.parent = self.EllipticRoot.transform
        self:SetScaleAlphaAndEnable(item,pos)
    end
    self:UpdateIndicators()
    
    CommonDefs.AddOnDragListener(self.WaterBg, DelegateFactory.Action_GameObject_Vector2(function (go, delta)
		self:OnScreenDrag(go, delta)
    end), false)
    UIEventListener.Get(self.LeftBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnRotateBtnClick(-1)
    end)
    UIEventListener.Get(self.RightBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnRotateBtnClick(1)
    end)
end

function LuaBeforeEnterDengZhongShiJieWnd:RegisterJobItemEvents()
    for i=0,self.m_JobCount-1,1 do
        local item = self.m_JobItems[i+1]
        local row = i
        local class = (row + self.m_StartRow) % self.m_JobCount + 1
        UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnSelectClass(class)
        end)
        CommonDefs.AddOnDragListener(item.gameObject, DelegateFactory.Action_GameObject_Vector2(function (go, delta)
            self:OnScreenDrag(go, delta)
        end), false)
    end
end

function LuaBeforeEnterDengZhongShiJieWnd:GetEllipticPos(position)
    local pos = Vector3(position.x,position.y,position.z)
    if(pos.x < -self.m_A) then
        pos.x = -self.m_A
    elseif pos.x > self.m_A then
        pos.x = self.m_A
    end

    local x = pos.x
    local y = pos.y
    local a = self.m_A
    local b = self.m_B
    y = b * math.sqrt(1 - ((x * x) / (a * a)))
    if pos.y < 0 then
        y = -y
    end
    pos.y = y
    local temp = math.abs(self.m_A-pos.x)
    local scale = 1-0.2*math.abs(x/self.m_A)
    return pos,scale
end

function LuaBeforeEnterDengZhongShiJieWnd:OnScreenDrag(go, delta)
    if EasyTouch.GetTouchCount() == 1 then
        if self.m_RotateAniTick then
            UnRegisterTick(self.m_RotateAniTick)
            self.m_RotateAniTick = nil
        end
        local rotationDirection = (delta.x < 0) and -1 or 1  -- 基于拖动方向决定旋转方向
        local deltaAngle = delta.x / (2*self.m_A) * 180
        for i = 0, self.m_JobCount - 1 do
            local item = self.m_JobItems[i + 1]
            local oriPos = item.transform.localPosition
            local angle = self:CalculateAngleFromVector(oriPos) -- 获取当前位置的角度
            angle = angle + deltaAngle -- 更新角度
            local pos = self:GetPositionOnEllipse(angle) -- 获取椭圆上的位置
            local scale = 1-0.2*math.abs(pos.x/self.m_A)
            self:SetScaleAlphaAndEnable(item,pos)
        end
        self:UpdateIndicators()
    end
end

function LuaBeforeEnterDengZhongShiJieWnd:SetScaleAlphaAndEnable(item,pos)
    local scale = 1-0.2*math.abs(pos.x/self.m_A)
    item.transform.localPosition = pos
    item.transform.localScale = Vector3(scale,scale,scale)
    local alpha = 1 - math.abs(pos.x/self.m_A)
    local enable = true
    if pos.y > 0 then
        alpha = 0
    elseif alpha < 0.1 then
        alpha = alpha / 0.1
    else
        alpha = 1
    end
    enable = alpha > 0
    local tex = item.transform:GetComponent(typeof(UITexture))
    local enbutton = item.transform:GetComponent(typeof(QnButton))
    enbutton.Enabled = enable
    tex.alpha = alpha
end

function LuaBeforeEnterDengZhongShiJieWnd:OnRotateBtnClick(rotationDirection)
    if self.m_RotateAniTick then
        UnRegisterTick(self.m_RotateAniTick)
        self.m_RotateAniTick = nil
    end

    self.m_RotateAniTick = RegisterTickWithDuration(function()
        for i = 0, self.m_JobCount - 1 do
            local item = self.m_JobItems[i + 1]
            local oriPos = item.transform.localPosition
            local angle = self:CalculateAngleFromVector(oriPos) -- 获取当前位置的角度
            angle = angle + self.m_ClickAngle * rotationDirection -- 更新角度
            local pos = self:GetPositionOnEllipse(angle) -- 获取椭圆上的位置
            self:SetScaleAlphaAndEnable(item,pos)
        end
        self:UpdateIndicators()
    end,5,250)
    
end

function LuaBeforeEnterDengZhongShiJieWnd:GetPositionOnEllipse(angle)
    local x = self.m_A * math.cos(math.rad(angle)) -- 椭圆方程中的x
    local y = self.m_B * math.sin(math.rad(angle)) -- 椭圆方程中的y
    return Vector3(x, y, 0)
end

function LuaBeforeEnterDengZhongShiJieWnd:CalculateAngleFromVector(vector)
    local angle = math.deg(math.atan2(vector.y / self.m_B, vector.x / self.m_A))
    return angle
end

function LuaBeforeEnterDengZhongShiJieWnd:InitItem(item,row)
    local JobIcon = item.transform:Find("Icon_Normal"):GetComponent(typeof(CUITexture))
    local HighLight = item.transform:Find("Icon_Selected"):GetComponent(typeof(CUITexture))
    local class = (row + self.m_StartRow) % self.m_JobCount + 1
    local id = self.m_JobList[class]
    local data = Initialization_Init.GetData(id)
    HighLight.gameObject:SetActive(false)
    if data then
        local icon = data.CreationNewLargeIconEmpty
        JobIcon:LoadMaterial(icon)
        HighLight:LoadMaterial(icon)
        local nameLabel = item.transform:Find("Bg/Name"):GetComponent(typeof(UILabel))
        nameLabel.text = LocalString.StrH2V(data.ClassFullName,true)
    end
end

function LuaBeforeEnterDengZhongShiJieWnd:OnSelectClass(class)
    --Indicators
	for i=1,#self.m_JobItems,1 do
		local g = self.IndicatorGrid.transform:GetChild(i-1)
		if g then
			local sp = g:GetComponent(typeof(UISprite))
			if sp then
				if i == class then
					sp.color = NGUIText.ParseColor24("7E66E4", 0)
					sp.alpha = 0.8
				else
					sp.color = NGUIText.ParseColor24("362D59", 0)
					sp.alpha = 0.5
				end
			end
		end
	end

    if self.m_ClickTick then
        UnRegisterTick(self.m_ClickTick)
        self.m_ClickTick = nil
    end
    if self.m_CloseTick then
        UnRegisterTick(self.m_CloseTick)
        self.m_CloseTick = nil
    end
    --按钮动效
    local itemIdx = class-1 - self.m_StartRow
    if itemIdx >= 0 then
        itemIdx = itemIdx+1
    else
        itemIdx = 13 + itemIdx+1
    end
    --itemIdx = itemIdx + 1
    local item = self.m_JobItems[itemIdx]
    if item then
        item:Find("Icon_Selected").gameObject:SetActive(true)
    end
    --关闭界面动效
    local ani = self.transform:GetComponent(typeof(Animation))
    ani:Play("beforeenterdengzhongshijiewnd_close")
    LuaCharacterCreation2023Mgr:OnlyPlayTimeline(class)
    self.m_ClickTick = RegisterTickOnce(function()
        LuaCharacterCreation2023Mgr:OpenCreatingOrExitingWnd(class)
    end,2000)
end

--@region UIEvent

--@endregion UIEvent

function LuaBeforeEnterDengZhongShiJieWnd:OnSendQueueStatus()
	LuaLoginMgr:ShowQueueStatusTopNotice()
end