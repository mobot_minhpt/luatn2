require("common/common_include")

local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"

CLuaSkillCastInfoItem = class()
RegistChildComponent(CLuaSkillCastInfoItem, "m_SkillIcon", CUITexture)
RegistChildComponent(CLuaSkillCastInfoItem, "m_SkillName", UILabel)
RegistChildComponent(CLuaSkillCastInfoItem, "m_TimeLbl", UILabel)


function CLuaSkillCastInfoItem:SetData(duration, skillId, isJueJi)
	local minutes = math.floor(duration/60)
	local seconds = duration % 60
	local timeStr = SafeStringFormat3(LocalString.GetString("%s分%s秒"), minutes, seconds)
	self.m_TimeLbl.text = timeStr
	local designData = Skill_AllSkills.GetData(skillId)
	self.m_SkillIcon:LoadSkillIcon(designData.SkillIcon)
	local skillData = Skill_AllSkills.GetData(skillId)
	if isJueJi then
		self.m_SkillName.text = "[FF88FF]"..designData.Name.."[-]"
	elseif skillData and skillData.Leading == 1 then
		self.m_SkillName.text = SafeStringFormat(LocalString.GetString("%s（释放中）"), designData.Name)
	else
		self.m_SkillName.text = designData.Name
	end
end

