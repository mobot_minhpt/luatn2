local CButton       = import "L10.UI.CButton"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"

LuaDuanWu2023PVPVEEnterWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaDuanWu2023PVPVEEnterWnd, "commonImageRuleView", "CommonImageRuleView", CCommonLuaScript)
RegistChildComponent(LuaDuanWu2023PVPVEEnterWnd, "textTable", "TextTable", UITable)
RegistChildComponent(LuaDuanWu2023PVPVEEnterWnd, "textTemplate", "TextTemplate", GameObject)
RegistChildComponent(LuaDuanWu2023PVPVEEnterWnd, "skillTemplate", "SkillTemplate", GameObject)
RegistChildComponent(LuaDuanWu2023PVPVEEnterWnd, "skillGrid", "SkillGrid", UIGrid)
RegistChildComponent(LuaDuanWu2023PVPVEEnterWnd, "matchButton", "MatchButton", CButton)
RegistChildComponent(LuaDuanWu2023PVPVEEnterWnd, "matching", "Matching", GameObject)
RegistChildComponent(LuaDuanWu2023PVPVEEnterWnd, "todayScore", "TodayScore", UILabel)
RegistChildComponent(LuaDuanWu2023PVPVEEnterWnd, "ruleButton", "RuleButton", GameObject)
--@endregion RegistChildComponent end

RegistClassMember(LuaDuanWu2023PVPVEEnterWnd, "isInMatching")

function LuaDuanWu2023PVPVEEnterWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	UIEventListener.Get(self.matchButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnMatchButtonClick()
	end)

	UIEventListener.Get(self.ruleButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRuleButtonClick()
	end)

	UIEventListener.Get(self.transform:Find("Anchor/StoreBtn").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnStoreButtonClick()
	end)
    --@endregion EventBind end
	self.textTemplate:SetActive(false)
	self.skillTemplate:SetActive(false)
	self.matchButton.gameObject:SetActive(false)
	self.matching:SetActive(false)
end

function LuaDuanWu2023PVPVEEnterWnd:OnEnable()
	g_ScriptEvent:AddListener("GlobalMatch_CheckInMatchingResult", self, "OnCheckInMatchingResult")
    g_ScriptEvent:AddListener("GlobalMatch_SignUpPlayResult", self, "OnSignUpPlayResult")
    g_ScriptEvent:AddListener("GlobalMatch_CancelSignUpResult", self, "OnCancelSignUpResult")
end

function LuaDuanWu2023PVPVEEnterWnd:OnDisable()
	g_ScriptEvent:RemoveListener("GlobalMatch_CheckInMatchingResult", self, "OnCheckInMatchingResult")
    g_ScriptEvent:RemoveListener("GlobalMatch_SignUpPlayResult", self, "OnSignUpPlayResult")
    g_ScriptEvent:RemoveListener("GlobalMatch_CancelSignUpResult", self, "OnCancelSignUpResult")
end

function LuaDuanWu2023PVPVEEnterWnd:OnCheckInMatchingResult(playerId, playId, isInMatching, resultStr)
    if playId == Duanwu2023_PVPVESetting.GetData().GamePlayId then
		self.isInMatching = isInMatching
        self:UpdateMatchButton()
    end
end

function LuaDuanWu2023PVPVEEnterWnd:OnSignUpPlayResult(playId, success)
	if playId == Duanwu2023_PVPVESetting.GetData().GamePlayId and success then
		self.isInMatching = true
        self:UpdateMatchButton()
    end
end

function LuaDuanWu2023PVPVEEnterWnd:OnCancelSignUpResult(playId, success)
	if playId == Duanwu2023_PVPVESetting.GetData().GamePlayId and success then
        self.isInMatching = false
		self:UpdateMatchButton()
    end
end


function LuaDuanWu2023PVPVEEnterWnd:Init()
	self:InitBaseInfo()
	self:InitSkill()
	self:InitImageRule()

	local setting = Duanwu2023_PVPVESetting.GetData()
	local player = CClientMainPlayer.Inst
	local score = tonumber(player.PlayProp:GetTempPlayStringData(EnumTempPlayDataKey_lua.eDuanWu2023PVPVEDailyScore) or 0)
	self.todayScore.text = SafeStringFormat3("%d/%d", score, setting.ScoreLimit)
	Gac2Gas.GlobalMatch_RequestCheckSignUp(setting.GamePlayId, player.Id)
end

function LuaDuanWu2023PVPVEEnterWnd:InitBaseInfo()
	Extensions.RemoveAllChildren(self.textTable.transform)
	local setting = Duanwu2023_PVPVESetting.GetData()
	local textList = {{
		LocalString.GetString("活动时间"),
		setting.ActivityTime
	},{
		LocalString.GetString("任务形式"),
		LocalString.GetString("匹配")
	},{
		LocalString.GetString("等级需求"),
		SafeStringFormat3(LocalString.GetString("%d级"), setting.LevelLimit)
	},{
		LocalString.GetString("任务描述"),
		g_MessageMgr:FormatMessage("DUANWU_2023_PVPVE_DESC")
	}}

	for i = 1, #textList do
		local child = NGUITools.AddChild(self.textTable.gameObject, self.textTemplate)
		child:SetActive(true)
		child.transform:GetComponent(typeof(UILabel)).text = textList[i][1]
		child.transform:Find("Label"):GetComponent(typeof(UILabel)).text = textList[i][2]
	end
	self.textTable:Reposition()
end

function LuaDuanWu2023PVPVEEnterWnd:InitSkill()
	Extensions.RemoveAllChildren(self.skillGrid.transform)
	local skillIds = Duanwu2023_PVPVESetting.GetData().TempSkillIds
	for i = 0, skillIds.Length - 1 do
		local child = NGUITools.AddChild(self.skillGrid.gameObject, self.skillTemplate)
		child:SetActive(true)
		local skillId = skillIds[i]
		local skillData = Skill_AllSkills.GetData(skillId)
		child.transform:Find("Mask/Icon"):GetComponent(typeof(CUITexture)):LoadSkillIcon(skillData.SkillIcon)
		child.transform:Find("Name"):GetComponent(typeof(UILabel)).text = skillData.Name
		UIEventListener.Get(child).onClick = DelegateFactory.VoidDelegate(function (go)
            CSkillInfoMgr.ShowSkillInfoWnd(skillId, true, 0, 0, nil)
        end)
	end
	self.skillGrid:Reposition()
end

function LuaDuanWu2023PVPVEEnterWnd:InitImageRule()
    local imagePaths = {
        "UI/Texture/FestivalActivity/Festival_DuanWu/DuanWu2023/Material/duanwu2023pvpveenterwnd_guide_01.mat",
        "UI/Texture/FestivalActivity/Festival_DuanWu/DuanWu2023/Material/duanwu2023pvpveenterwnd_guide_02.mat",
        "UI/Texture/FestivalActivity/Festival_DuanWu/DuanWu2023/Material/duanwu2023pvpveenterwnd_guide_03.mat",
        "UI/Texture/FestivalActivity/Festival_DuanWu/DuanWu2023/Material/duanwu2023pvpveenterwnd_guide_04.mat",
    }
    local msgs = {
        g_MessageMgr:FormatMessage("DUANWU_2023_PVPVE_ENTER_WND_GUIDE_01"),
        g_MessageMgr:FormatMessage("DUANWU_2023_PVPVE_ENTER_WND_GUIDE_02"),
        g_MessageMgr:FormatMessage("DUANWU_2023_PVPVE_ENTER_WND_GUIDE_03"),
        g_MessageMgr:FormatMessage("DUANWU_2023_PVPVE_ENTER_WND_GUIDE_04"),
    }
	self.commonImageRuleView:Init(imagePaths, msgs)
end

function LuaDuanWu2023PVPVEEnterWnd:UpdateMatchButton()
	self.matchButton.gameObject:SetActive(true)
	local yellowBtn = self.matchButton.transform:Find("YellowBtn")
	local blueBtn = self.matchButton.transform:Find("BlueBtn")
	if self.isInMatching then
		yellowBtn.gameObject:SetActive(false)
		blueBtn.gameObject:SetActive(true)
		self.matchButton.label.color = NGUIText.ParseColor24("0e3254", 0)
		self.matchButton.Text = LocalString.GetString("取消匹配")
    else
		yellowBtn.gameObject:SetActive(true)
		blueBtn.gameObject:SetActive(false)
		self.matchButton.label.color = NGUIText.ParseColor24("351B01", 0)
		self.matchButton.Text = LocalString.GetString("开始匹配")
    end
	self.matching:SetActive(self.isInMatching)
end

--@region UIEvent

function LuaDuanWu2023PVPVEEnterWnd:OnMatchButtonClick()
	local playId = Duanwu2023_PVPVESetting.GetData().GamePlayId
	if self.isInMatching then
		Gac2Gas.GlobalMatch_RequestCancelSignUp(playId)
	else
		Gac2Gas.GlobalMatch_RequestSignUp(playId)
	end
end

function LuaDuanWu2023PVPVEEnterWnd:OnRuleButtonClick()
	g_MessageMgr:ShowMessage("DUANWU_2023_PVPVE_RULE")
end

function LuaDuanWu2023PVPVEEnterWnd:OnStoreButtonClick()
	CLuaNPCShopInfoMgr.ShowScoreShop("DuanWu2023Score")
end

--@endregion UIEvent
