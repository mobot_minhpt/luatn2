local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local Color = import "UnityEngine.Color"

LuaHMLZEnterWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaHMLZEnterWnd, "Content", "Content", GameObject)
RegistChildComponent(LuaHMLZEnterWnd, "Template", "Template", GameObject)
RegistChildComponent(LuaHMLZEnterWnd, "CountLabel", "CountLabel", GameObject)
RegistChildComponent(LuaHMLZEnterWnd, "EnterBtn", "EnterBtn", GameObject)
RegistChildComponent(LuaHMLZEnterWnd, "CancelBtn", "CancelBtn", GameObject)
RegistChildComponent(LuaHMLZEnterWnd, "WaitLabel", "WaitLabel", GameObject)

--@endregion RegistChildComponent end

RegistClassMember(LuaHMLZEnterWnd, "m_State")

function LuaHMLZEnterWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaHMLZEnterWnd:Init()
    Gac2Gas.ZYJ2021HMLZ_CheckSignUp()
    self:InitView()
end

function LuaHMLZEnterWnd:InitView()     
    local content = g_MessageMgr:FormatMessage("ZYJ2021HMLZ_RULE")
    self.Template:SetActive(false)
    local table = self.Content.transform:Find("ScrollView/Table"):GetComponent(typeof(UITable))
    for p in string.gmatch(content, "<p>(.-)</p>") do
        local pObj = NGUITools.AddChild(table.gameObject, self.Template)
        pObj:SetActive(true)
        pObj.transform:Find("Label"):GetComponent(typeof(UILabel)).text = p
    end
    table:Reposition()
    -- 时间
    self.transform:Find("TimeLabel/Time"):GetComponent(typeof(UILabel)).text = g_MessageMgr:FormatMessage("2021ZYJ_HMLZ_Period")
    -- 剩余次数
    local eZYJ2021HMLZ = 178
    local count = 1 - CClientMainPlayer.Inst.PlayProp:GetPlayTimes(eZYJ2021HMLZ)
    local color = (count == 0) and Color(1, 0.3125, 0.3125, 1) or Color(1, 1, 1, 1)

    local label = self.CountLabel:GetComponent(typeof(UILabel))
    label.text = tostring(count)
    label.color = color
    UIEventListener.Get(self.EnterBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        Gac2Gas.ZYJ2021HMLZ_SignUpPlay()
    end)

    UIEventListener.Get(self.CancelBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        Gac2Gas.ZYJ2021HMLZ_CancelSignUp()
    end)
    self.m_State = false
    self:UpdateView()
end

function LuaHMLZEnterWnd:UpdateView()
    self.EnterBtn:SetActive(not self.m_State)
    self.CancelBtn:SetActive(self.m_State)
    self.WaitLabel:SetActive(self.m_State)
end

function LuaHMLZEnterWnd:OnEnable()
    g_ScriptEvent:AddListener("ZYJ2021HMLZ_StateUpdate", self, "StateUpdate")
end

function LuaHMLZEnterWnd:OnDisable()
    g_ScriptEvent:RemoveListener("ZYJ2021HMLZ_StateUpdate", self, "StateUpdate")
end

function LuaHMLZEnterWnd:StateUpdate(state)
    self.m_State = state
    self:UpdateView()
end
--@region UIEvent

--@endregion UIEvent

