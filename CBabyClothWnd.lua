-- Auto Generated!!
local Baby_Fashion = import "L10.Game.Baby_Fashion"
local Baby_RanSe = import "L10.Game.Baby_RanSe"
local Baby_Setting = import "L10.Game.Baby_Setting"
local BabyMgr = import "L10.Game.BabyMgr"
local CBabyClothWnd = import "L10.UI.CBabyClothWnd"
local CBabyFashion = import "L10.Game.CBabyFashion"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CFreightEquipSubmitMgr = import "L10.UI.CFreightEquipSubmitMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local CUITexture = import "L10.UI.CUITexture"
local DelegateFactory = import "DelegateFactory"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local Extensions = import "Extensions"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local Main = import "L10.Engine.Main"
local MessageMgr = import "L10.Game.MessageMgr"
local MessageWndManager = import "L10.UI.MessageWndManager"
local NGUITools = import "NGUITools"
local Object = import "UnityEngine.Object"
local String = import "System.String"
local Time = import "UnityEngine.Time"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local UInt32 = import "System.UInt32"
local UIPanel = import "UIPanel"
local UIScrollView = import "UIScrollView"
local UITable = import "UITable"
local Vector2 = import "UnityEngine.Vector2"
local NativeTools = import "L10.Engine.NativeTools"
CBabyClothWnd.m_GetNowUseFashionId_CS2LuaHook = function (this, dic)
    do
        local __itr_is_triger_return = nil
        local __itr_result = nil
        CommonDefs.DictIterateWithRet(dic, DelegateFactory.DictIterFunc(function (___key, ___value)
            local pair = {}
            pair.Key = ___key
            pair.Value = ___value
            if pair.Value.Actived == 1 then
                __itr_is_triger_return = true
                __itr_result = pair.Value.FashionId
                return true
            end
        end))
        if __itr_is_triger_return == true then
            return __itr_result
        end
    end
    return 0
end
CBabyClothWnd.m_DoCapture_CS2LuaHook = function (this)
    if Time.realtimeSinceStartup - this.m_LastEvaluateTime < this.m_EvaluateInterval then
        --MessageMgr.Inst.ShowMessage("RPC_TOO_FREQUENT");
        return
    end
    this.m_LastEvaluateTime = Time.realtimeSinceStartup

    this:StartCoroutine(this:delayCaptureScreen())
end
CBabyClothWnd.m_ClearTexture_CS2LuaHook = function (this)
    if this.m_CachedTexture ~= nil then
        Object.Destroy(this.m_CachedTexture)
    end
end
CBabyClothWnd.m_OpenCapturePanel_CS2LuaHook = function (this)
    this.captureShowNode:SetActive(true)
    local backBtn = this.captureShowNode.transform:Find("backBtn").gameObject
    local capBtn = this.captureShowNode.transform:Find("captureBtn").gameObject
    UIEventListener.Get(backBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        this.captureShowNode:SetActive(false)
    end)
    UIEventListener.Get(capBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        this:DoCapture()
    end)
end

CBabyClothWnd.m_Init_CS2LuaHook = function (this)
    UIEventListener.Get(this.closeBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        this:Close()
    end)

    UIEventListener.Get(this.capturePicBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        this:OpenCapturePanel()
    end)

    UIEventListener.Get(this.expressionBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        this:OpenExpressionWnd()
    end)

    BabyMgr.Inst:ResetChooseInfo()

    do
        local i = 0
        while i < this.tabNodeArray.Length do
            local index = i
            this.tabNodeArray[i].transform:Find("light").gameObject:SetActive(false)
            UIEventListener.Get(this.tabNodeArray[i]).onClick = DelegateFactory.VoidDelegate(function (p)
                this:InitData(index, false)
            end)
            i = i + 1
        end
    end

    this.babyInfo = BabyMgr.Inst.ShowBabyInfo

    BabyMgr.Inst.TempChooseHeadId = this:GetNowUseFashionId(this.babyInfo.Props.FashionData.HeadFashions)
    BabyMgr.Inst.TempChooseBodyId = this:GetNowUseFashionId(this.babyInfo.Props.FashionData.BodyFashions)
    BabyMgr.Inst.TempChooseWingId = this:GetNowUseFashionId(this.babyInfo.Props.FashionData.BackFashions)

    BabyMgr.Inst.TempChooseColorId = 0
    if CommonDefs.DictContains(this.babyInfo.Props.FashionData.BodyFashions, typeof(UInt32), BabyMgr.Inst.TempChooseBodyId) then
        BabyMgr.Inst.TempChooseColorId = CommonDefs.DictGetValue(this.babyInfo.Props.FashionData.BodyFashions, typeof(UInt32), BabyMgr.Inst.TempChooseBodyId).ColorId
    end

    this:ReshowBaby(0)

    --ShowLingshou();

    this:InitData(3, false)
end
CBabyClothWnd.m_DeleteBabyFashion_CS2LuaHook = function (this, fashionId)
    if BabyMgr.Inst.TempChooseBodyId == fashionId then
        BabyMgr.Inst.TempChooseBodyId = 0
        BabyMgr.Inst.TempChooseColorId = 0
    end
    if BabyMgr.Inst.TempChooseHeadId == fashionId then
        BabyMgr.Inst.TempChooseHeadId = 0
    end
    if BabyMgr.Inst.TempChooseWingId == fashionId then
        BabyMgr.Inst.TempChooseWingId = 0
    end

    Gac2Gas.RequestRemoveBabyFashion(this.babyInfo.Id, fashionId)
end
CBabyClothWnd.m_InitColorNodeInfo_CS2LuaHook = function (this, infoNode)
    local nowChooseBodyId = BabyMgr.Inst.TempChooseBodyId
    local fashionData = CommonDefs.DictGetValue(this.babyInfo.Props.FashionData.BodyFashions, typeof(UInt32), nowChooseBodyId)
    if fashionData == nil then
        return
    end

    local colorCount = Baby_RanSe.GetDataCount()

    local template = infoNode.transform:Find("clothItem").gameObject
    template:SetActive(false)
    local tableNode = infoNode.transform:Find("ScrollView/Table").gameObject
    local scrollView = CommonDefs.GetComponent_Component_Type(infoNode.transform:Find("ScrollView"), typeof(UIScrollView))
    local table = CommonDefs.GetComponent_Component_Type(infoNode.transform:Find("ScrollView/Table"), typeof(UITable))

    local totalRow = math.ceil(colorCount / 3)
    Extensions.RemoveAllChildren(tableNode.transform)
    local totalCount = 0
    local lockAllSign = true

    do
        local i = 0
        while i < totalRow do
            local t_node = NGUITools.AddChild(tableNode, template)
            t_node:SetActive(true)
            for j = 0, 2 do
                totalCount = totalCount + 1
                local itemNode = t_node.transform:Find("icon" .. tostring((j + 1))).gameObject
                itemNode.transform:Find("light").gameObject:SetActive(false)
                local activeSign = false
                if CommonDefs.DictContains(fashionData.ActivedColors, typeof(UInt32), totalCount) then
                    activeSign = true
                end
                local ranseInfo = Baby_RanSe.GetData(totalCount)
                if ranseInfo ~= nil then
                    itemNode:SetActive(true)
                    if activeSign then
                        itemNode.transform:Find("locksign").gameObject:SetActive(false)
                        itemNode.transform:Find("use").gameObject:SetActive(true)
                        UIEventListener.Get(itemNode.transform:Find("button").gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
                            --use color
                            this:UseColor(itemNode, ranseInfo)
                        end)
                    else
                        lockAllSign = false
                        itemNode.transform:Find("locksign").gameObject:SetActive(true)
                        itemNode.transform:Find("use").gameObject:SetActive(false)
                    end
                    if fashionData.ColorId == ranseInfo.ID then
                        this:UseColor(itemNode, ranseInfo)
                    end

                    CommonDefs.GetComponent_Component_Type(itemNode.transform:Find("frame"), typeof(CUITexture)):LoadMaterial(ranseInfo.Icon)
                else
                    itemNode:SetActive(false)
                end
            end
            i = i + 1
        end
    end
    table:Reposition()
    scrollView:ResetPosition()

    local resetBtn = infoNode.transform:Find("btn1").gameObject
    local saveBtn = infoNode.transform:Find("btn2").gameObject
    local unlockBtn = infoNode.transform:Find("btn3").gameObject
    if lockAllSign then
        unlockBtn:SetActive(false)
    else
        unlockBtn:SetActive(true)
        UIEventListener.Get(unlockBtn).onClick = DelegateFactory.VoidDelegate(function (p)
            this:UnlockColor()
        end)
    end

    resetBtn:SetActive(false)
    --UIEventListener.Get(resetBtn).onClick = p =>
    --{
    --    MessageWndManager.ShowOKCancelMessage(LocalString.GetString("是否还原外观"), () => {
    --        this.ResetBabyModify();
    --    });
    --};
    UIEventListener.Get(saveBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        MessageWndManager.ShowOKCancelMessage(LocalString.GetString("是否保存当前外观"), DelegateFactory.Action(function ()
            this:SaveBabyModify()
        end), nil, nil, nil, false)
    end)
end
CBabyClothWnd.m_UseColor_CS2LuaHook = function (this, node, ranseInfo)
    local lightNode = node.transform:Find("light").gameObject
    if lightNode.activeSelf then
        if this.nowChooseNode[0] ~= nil then
            this.nowChooseNode[0].transform:Find("light").gameObject:SetActive(false)
            this.nowChooseNode[0] = nil
        else
            lightNode:SetActive(false)
        end

        BabyMgr.Inst.TempChooseColorId = 0
    else
        if this.nowChooseNode[0] ~= nil then
            this.nowChooseNode[0].transform:Find("light").gameObject:SetActive(false)
        end
        this.nowChooseNode[0] = node
        lightNode:SetActive(true)

        BabyMgr.Inst.TempChooseColorId = ranseInfo.ID
    end

    this:ReshowBaby(0)
end
CBabyClothWnd.m_UnlockColor_CS2LuaHook = function (this)
    local nowChooseBodyId = BabyMgr.Inst.TempChooseBodyId
    local fashionData = CommonDefs.DictGetValue(this.babyInfo.Props.FashionData.BodyFashions, typeof(UInt32), nowChooseBodyId)
    if fashionData == nil then
        return
    end

    local count = 0
    CommonDefs.DictIterate(fashionData.ActivedColors, DelegateFactory.Action_object_object(function (___key, ___value)
        local pair = {}
        pair.Key = ___key
        pair.Value = ___value
        if pair.Value > 0 then
            count = count + 1
        end
    end))

    local consumeArray = Baby_Setting.GetData().RanSeItemCount
    if count > consumeArray.Length - 1 then
        return
    end

    local consumeCount = consumeArray[count]
    if consumeCount > 0 then
        local messageformat = MessageMgr.Inst:GetMessageFormat("BABY_COLOR_UNLOCK_TIPS", true)

        LuaItemInfoMgr:ShowItemConsumeWndWithQuickAccess(Baby_Setting.GetData().RanSeItemId, consumeCount, messageformat, true, false, LocalString.GetString("开启"), function (enough)
            Gac2Gas.RequestActiveBabyFashionColor(this.babyInfo.Id, BabyMgr.Inst.TempChooseBodyId)
            LuaItemInfoMgr:CloseItemConsumeWnd()
        end, nil, false)
    end
end
CBabyClothWnd.m_InitData_CS2LuaHook = function (this, index, savePosSign)
    if index == 0 then
        local nowChooseBodyId = BabyMgr.Inst.TempChooseBodyId
        local canColor = true
        if not CommonDefs.DictContains(this.babyInfo.Props.FashionData.BodyFashions, typeof(UInt32), nowChooseBodyId) then
            canColor = false
        else
            local fashionData = CommonDefs.DictGetValue(this.babyInfo.Props.FashionData.BodyFashions, typeof(UInt32), nowChooseBodyId)
            if fashionData ~= nil then
                local body_fashion = Baby_Fashion.GetData(fashionData.FashionId)
                if body_fashion ~= nil then
                    if body_fashion.CanColor == 0 then
                        canColor = false
                    end
                end
            else
                canColor = false
            end
        end

        if not canColor then
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("当前服饰不支持染色"))
            return
        end
    end

    this.nowChooseTabIndex = index
    if this.nowChooseTabNode ~= nil then
        this.nowChooseTabNode.transform:Find("light").gameObject:SetActive(false)
        this.nowChooseTabNode = nil
    end
    this.nowChooseTabNode = this.tabNodeArray[index]
    this.nowChooseTabNode.transform:Find("light").gameObject:SetActive(true)

    local chooseNode = nil
    this.nowChooseFashionId = 0
    do
        local i = 0
        while i < this.showNodeArray.Length do
            if i == index then
                this.showNodeArray[i]:SetActive(true)
                chooseNode = this.showNodeArray[i]
            else
                this.showNodeArray[i]:SetActive(false)
            end
            i = i + 1
        end
    end

    if index == 0 then
        this:InitColorNodeInfo(chooseNode)
    else
        if chooseNode ~= nil then
            this:InitClothNodeInfo(index, chooseNode, savePosSign)
        end
    end
end
CBabyClothWnd.m_InitClothNodeInfo_CS2LuaHook = function (this, index, infoNode, savePosSign)
    local maxNum = 0
    local fashionDic = nil
    local fashionPosition = 0

    if index == 1 then
        maxNum = Baby_Setting.GetData().BackFashionUpperLimit
        fashionDic = this.babyInfo.Props.FashionData.BackFashions
        fashionPosition = 2
    elseif index == 2 then
        maxNum = Baby_Setting.GetData().HeadFashionUpperLimit
        fashionDic = this.babyInfo.Props.FashionData.HeadFashions
        fashionPosition = 0
    elseif index == 3 then
        maxNum = Baby_Setting.GetData().BodyFashionUpperLimit
        fashionDic = this.babyInfo.Props.FashionData.BodyFashions
        fashionPosition = 1
    end

    local template = infoNode.transform:Find("clothItem").gameObject
    template:SetActive(false)
    local tableNode = infoNode.transform:Find("ScrollView/Table").gameObject
    local scrollView = CommonDefs.GetComponent_Component_Type(infoNode.transform:Find("ScrollView"), typeof(UIScrollView))
    local table = CommonDefs.GetComponent_Component_Type(infoNode.transform:Find("ScrollView/Table"), typeof(UITable))
    local panel = CommonDefs.GetComponent_Component_Type(scrollView, typeof(UIPanel))
    local savePos = scrollView.transform.localPosition
    local saveOffsetY = panel.clipOffset.y
    if fashionDic == nil then
        return
    end

    local fashionList = CreateFromClass(MakeGenericClass(List, CBabyFashion))
    CommonDefs.DictIterate(fashionDic, DelegateFactory.Action_object_object(function (___key, ___value)
        local pairs = {}
        pairs.Key = ___key
        pairs.Value = ___value
        CommonDefs.ListAdd(fashionList, typeof(CBabyFashion), pairs.Value)
    end))
    CommonDefs.ListSort1(fashionList, typeof(CBabyFashion), MakeDelegateFromCSFunction(this.CompareFashionItem, MakeGenericClass(Comparison, CBabyFashion), this))
    local totalCount = 0
    local addBtnSign = false

    local totalRow = math.ceil(maxNum / 3)
    Extensions.RemoveAllChildren(tableNode.transform)
    do
        local i = 0
        while i < totalRow do
            local t_node = NGUITools.AddChild(tableNode, template)
            t_node:SetActive(true)
            for j = 0, 2 do
                local itemNode = t_node.transform:Find("icon" .. tostring((j + 1))).gameObject
                itemNode.transform:Find("light").gameObject:SetActive(false)
                if totalCount < fashionList.Count then
                    this:InitFashionShowNode(itemNode, fashionList[totalCount], fashionPosition, false)
                    totalCount = totalCount + 1
                else
                    if not addBtnSign then
                        this:InitFashionShowNode(itemNode, nil, fashionPosition, false)
                        addBtnSign = true
                    else
                        this:InitFashionShowNode(itemNode, nil, fashionPosition, true)
                    end
                end
            end
            i = i + 1
        end
    end
    table:Reposition()
    scrollView:ResetPosition()

    if savePosSign then
        scrollView.transform.localPosition = savePos
        panel.clipOffset = Vector2(panel.clipOffset.x, saveOffsetY)
    end

    local resetBtn = infoNode.transform:Find("btn1").gameObject
    local saveBtn = infoNode.transform:Find("btn2").gameObject
    local deleteBtn = infoNode.transform:Find("btn3").gameObject

    UIEventListener.Get(resetBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        MessageWndManager.ShowOKCancelMessage(LocalString.GetString("是否还原外观"), DelegateFactory.Action(function ()
            this:ResetBabyModify()
        end), nil, nil, nil, false)
    end)
    UIEventListener.Get(saveBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        MessageWndManager.ShowOKCancelMessage(LocalString.GetString("是否保存当前外观"), DelegateFactory.Action(function ()
            this:SaveBabyModify()
        end), nil, nil, nil, false)
    end)
    UIEventListener.Get(deleteBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        if this.nowChooseFashionId > 0 then
            MessageWndManager.ShowOKCancelMessage(LocalString.GetString("是否删除当前选中物品"), DelegateFactory.Action(function ()
                this:DeleteBabyFashion(this.nowChooseFashionId)
            end), nil, nil, nil, false)
        end
    end)
end
CBabyClothWnd.m_InitFashionShowNode_CS2LuaHook = function (this, node, fashionInfo, fashionPosition, emptySign)
    if emptySign then
        node:SetActive(false)
    else
        node:SetActive(true)
        local btn = node.transform:Find("button").gameObject
        if fashionInfo == nil then
            node.transform:Find("addsign").gameObject:SetActive(true)
            node.transform:Find("frame").gameObject:SetActive(false)
            UIEventListener.Get(btn).onClick = DelegateFactory.VoidDelegate(function (p)
                this:ShowEmptyChooseWnd(fashionPosition)
            end)
        else
            node.transform:Find("addsign").gameObject:SetActive(false)
            node.transform:Find("frame").gameObject:SetActive(true)
            local fInfo = Baby_Fashion.GetData(fashionInfo.FashionId)
            if fInfo ~= nil then
                local itemTemplate = CItemMgr.Inst:GetItemTemplate(fInfo.ItemID)
                if itemTemplate ~= nil then
                    CommonDefs.GetComponent_Component_Type(node.transform:Find("frame"), typeof(CUITexture)):LoadMaterial(itemTemplate.Icon)
                end
            end
            local nowChooseId = 0
            if fashionPosition == 0 then
                nowChooseId = BabyMgr.Inst.TempChooseHeadId
            elseif fashionPosition == 1 then
                nowChooseId = BabyMgr.Inst.TempChooseBodyId
            elseif fashionPosition == 2 then
                nowChooseId = BabyMgr.Inst.TempChooseWingId
            end
            if nowChooseId == fashionInfo.FashionId then
                local lightNode = node.transform:Find("light").gameObject
                if this.nowChooseNode[fashionPosition] ~= nil then
                    this.nowChooseNode[fashionPosition].transform:Find("light").gameObject:SetActive(false)
                end
                this.nowChooseNode[fashionPosition] = node
                lightNode:SetActive(true)
                this.nowChooseFashionId = fInfo.ID
            end

            local fID = fInfo.ID

            local fashionData = nil
            if fashionPosition == 0 then
                if CommonDefs.DictContains(this.babyInfo.Props.FashionData.HeadFashions, typeof(UInt32), fID) then
                    fashionData = CommonDefs.DictGetValue(this.babyInfo.Props.FashionData.HeadFashions, typeof(UInt32), fID)
                end
            elseif fashionPosition == 1 then
                if CommonDefs.DictContains(this.babyInfo.Props.FashionData.BodyFashions, typeof(UInt32), fID) then
                    fashionData = CommonDefs.DictGetValue(this.babyInfo.Props.FashionData.BodyFashions, typeof(UInt32), fID)
                end
            elseif fashionPosition == 2 then
                if CommonDefs.DictContains(this.babyInfo.Props.FashionData.BackFashions, typeof(UInt32), fID) then
                    fashionData = CommonDefs.DictGetValue(this.babyInfo.Props.FashionData.BackFashions, typeof(UInt32), fID)
                end
            end

            local expireLabel = CommonDefs.GetComponent_Component_Type(node.transform:Find("expireLabel"), typeof(UILabel))
            if fashionData ~= nil and fashionData.ExpireTime > 0 then
                local fashionT = Baby_Fashion.GetData(fashionData.FashionId)
                if fashionT ~= nil and fashionT.AvailableTime > 0 then
                    expireLabel.gameObject:SetActive(true)
                    if fashionData.IsExpired == 0 then
                        local endTime = CServerTimeMgr.ConvertTimeStampToZone8Time(fashionData.ExpireTime)
                        local now = CServerTimeMgr.Inst:GetZone8Time()
                        local span = endTime:Subtract(now)
                        if span.TotalSeconds > 0 then
                            --未过期
                            if span.TotalDays >= 1 or endTime.Day ~= now.Day then
                                expireLabel.text = System.String.Format(LocalString.GetString("[00ff00]{0}天[-]"), math.floor((span.TotalDays)))
                            else
                                expireLabel.text = System.String.Format(LocalString.GetString("[00ff00]{0}[-]"), ToStringWrap(endTime, "HH:mm"))
                            end
                        end
                    else
                        expireLabel.text = LocalString.GetString("[ff0000]已过期[-]")
                    end
                else
                    expireLabel.gameObject:SetActive(false)
                end
            else
                expireLabel.gameObject:SetActive(false)
            end

            UIEventListener.Get(btn).onClick = DelegateFactory.VoidDelegate(function (p)
                this:ChooseFashionItemNode(node, fInfo, fashionPosition)
            end)
        end
    end
end
CBabyClothWnd.m_SetTempChoose_CS2LuaHook = function (this, fashionId, fashionPosition)
    this.nowChooseFashionId = fashionId
    if fashionPosition == 0 then
        BabyMgr.Inst.TempChooseHeadId = fashionId
    elseif fashionPosition == 1 then
        BabyMgr.Inst.TempChooseBodyId = fashionId
        if CommonDefs.DictContains(this.babyInfo.Props.FashionData.BodyFashions, typeof(UInt32), BabyMgr.Inst.TempChooseBodyId) then
            BabyMgr.Inst.TempChooseColorId = CommonDefs.DictGetValue(this.babyInfo.Props.FashionData.BodyFashions, typeof(UInt32), BabyMgr.Inst.TempChooseBodyId).ColorId
        else
            BabyMgr.Inst.TempChooseColorId = 0
        end
    elseif fashionPosition == 2 then
        BabyMgr.Inst.TempChooseWingId = fashionId
    end
end
CBabyClothWnd.m_RecalScore_CS2LuaHook = function (this)
    local score = 0
    local headInfo = Baby_Fashion.GetData(BabyMgr.Inst.TempChooseHeadId)
    if headInfo ~= nil then
        score = score + headInfo.Score
    end
    local bodyInfo = Baby_Fashion.GetData(BabyMgr.Inst.TempChooseBodyId)
    if bodyInfo ~= nil then
        score = score + bodyInfo.Score
    end
    local wingInfo = Baby_Fashion.GetData(BabyMgr.Inst.TempChooseWingId)
    if wingInfo ~= nil then
        score = score + wingInfo.Score
    end

    local colorInfo = Baby_RanSe.GetData(BabyMgr.Inst.TempChooseColorId)
    if colorInfo ~= nil then
        score = score + colorInfo.Score
    end


    this.scoreLabel.text = tostring(score)
end
CBabyClothWnd.m_ReshowBaby_CS2LuaHook = function (this, expressionId)

    this.babyTexture:Init(this.babyInfo, - 180, true, expressionId)

    this:RecalScore()
end
CBabyClothWnd.m_ChooseFashionItemNode_CS2LuaHook = function (this, node, fInfo, fashionPosition)
    if this.babyInfo.Props.ExtraData.ShowStatus ~= 1 and (this.babyInfo.Props.ExtraData.ShowStatus == fInfo.BabyStage + 1 or fInfo.BabyStage == 3) then
    else
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("宝宝当前展示阶段不支持此时装试穿！"))
        return
    end

    local lightNode = node.transform:Find("light").gameObject
    if lightNode.activeSelf then
        if this.nowChooseNode[fashionPosition] ~= nil then
            this.nowChooseNode[fashionPosition].transform:Find("light").gameObject:SetActive(false)
            this.nowChooseNode[fashionPosition] = nil
        else
            lightNode:SetActive(false)
        end

        this:SetTempChoose(0, fashionPosition)
    else
        if this.nowChooseNode[fashionPosition] ~= nil then
            this.nowChooseNode[fashionPosition].transform:Find("light").gameObject:SetActive(false)
        end
        this.nowChooseNode[fashionPosition] = node
        lightNode:SetActive(true)

        this:SetTempChoose(fInfo.ID, fashionPosition)
    end

    this:ReshowBaby(0)
end
CBabyClothWnd.m_CheckFashionIdValid_CS2LuaHook = function (this, data)
    if data.BabyStage == 3 or data.BabyStage + 1 == this.babyInfo.Status then
        if CommonDefs.DictContains(this.babyInfo.Props.FashionData.HeadFashions, typeof(UInt32), data.ID) or CommonDefs.DictContains(this.babyInfo.Props.FashionData.BodyFashions, typeof(UInt32), data.ID) or CommonDefs.DictContains(this.babyInfo.Props.FashionData.BackFashions, typeof(UInt32), data.ID) then
            return false
        end
        return true
    end

    return false
end
CBabyClothWnd.m_ShowEmptyChooseWnd_CS2LuaHook = function (this, chooseType)
    local dic = CreateFromClass(MakeGenericClass(Dictionary, Int32, MakeGenericClass(List, String)))
    local equipList = CreateFromClass(MakeGenericClass(List, String))
    local bagSize = CClientMainPlayer.Inst.ItemProp:GetPlaceSize(EnumItemPlace.Bag)
    do
        local i = 1
        while i <= bagSize do
            local id = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Bag, i)
            if not System.String.IsNullOrEmpty(id) then
                local equip = CItemMgr.Inst:GetById(id)
                if equip ~= nil and equip.IsItem and this:GetBabyFashionByItem(equip.TemplateId, chooseType) > 0 then
                    CommonDefs.ListAdd(equipList, typeof(String), equip.Id)
                end
            end
            i = i + 1
        end
    end
    CommonDefs.DictAdd(dic, typeof(Int32), 0, typeof(MakeGenericClass(List, String)), equipList)

    CFreightEquipSubmitMgr.OpenEquipmentChooseWnd("", dic, LocalString.GetString("包裹中的宝宝时装道具"), 0, false, LocalString.GetString("添加"), "", nil, LocalString.GetString("包裹中暂无可加入当前衣柜的时装道具"))
end
