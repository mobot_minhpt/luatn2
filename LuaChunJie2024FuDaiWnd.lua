local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaChunJie2024FuDaiWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaChunJie2024FuDaiWnd, "timeLabel1", "TimeLabel1", UILabel)
RegistChildComponent(LuaChunJie2024FuDaiWnd, "timeLabel2", "TimeLabel2", UILabel)
RegistChildComponent(LuaChunJie2024FuDaiWnd, "timeLabel3", "TimeLabel3", UILabel)
RegistChildComponent(LuaChunJie2024FuDaiWnd, "closeButton", "CloseButton", GameObject)
--@endregion RegistChildComponent end

RegistClassMember(LuaChunJie2024FuDaiWnd, "tick")
RegistClassMember(LuaChunJie2024FuDaiWnd, "fuDaiTableRoot")
RegistClassMember(LuaChunJie2024FuDaiWnd, "playerNum")
RegistClassMember(LuaChunJie2024FuDaiWnd, "iHaveAFuDai")
RegistClassMember(LuaChunJie2024FuDaiWnd, "iHaveReceived")
RegistClassMember(LuaChunJie2024FuDaiWnd, "index2PlayerId")
RegistClassMember(LuaChunJie2024FuDaiWnd, "isAllFuDaiReceived")
RegistClassMember(LuaChunJie2024FuDaiWnd, "timeRoot")
RegistClassMember(LuaChunJie2024FuDaiWnd, "endTimeStamp")

function LuaChunJie2024FuDaiWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	UIEventListener.Get(self.closeButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCloseButtonClick()
	end)
    --@endregion EventBind end

	self.timeRoot = self.transform:Find("Time").gameObject
end

function LuaChunJie2024FuDaiWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncSongQiongShenChooseRewardIndex", self, "OnSyncSongQiongShenChooseRewardIndex")
end

function LuaChunJie2024FuDaiWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncSongQiongShenChooseRewardIndex", self, "OnSyncSongQiongShenChooseRewardIndex")
end

function LuaChunJie2024FuDaiWnd:OnSyncSongQiongShenChooseRewardIndex()
	self:UpdateFuDai()
	self:UpdateTimeLabel()
end

function LuaChunJie2024FuDaiWnd:Init()
	local type = LuaChunJie2024Mgr.fuDaiRewardInfo.type
	self.transform:Find("SQS").gameObject:SetActive(type == "SQS")
	self.transform:Find("SLQZ").gameObject:SetActive(type == "SLQZ")

	self:InitFuDai()
	self:UpdateFuDai()
	self:InitLeftTime()
	self:UpdateTimeLabel()
end

function LuaChunJie2024FuDaiWnd:InitFuDai()
	local playerInfo = LuaChunJie2024Mgr.fuDaiRewardInfo.playerInfo
	self.playerNum = 0
	local myId = CClientMainPlayer.Inst.Id
	self.iHaveAFuDai = false
	for playerId, data in pairs(playerInfo) do
		self.playerNum = self.playerNum + 1
		if playerId == myId then
			self.iHaveAFuDai = true
		end
	end

	local fudaiRoot = self.transform:Find("FuDai")
	local childCount = fudaiRoot.childCount
	for i = 1, childCount do
		fudaiRoot:GetChild(i - 1).gameObject:SetActive(false)
	end

	self.fuDaiTableRoot = fudaiRoot:Find(self.playerNum > 2 and "5" or tostring(self.playerNum))
	self.fuDaiTableRoot.gameObject:SetActive(true)
	if self.playerNum > 0 then
		local tableChildCount = self.fuDaiTableRoot.childCount
		for i = 1, tableChildCount do
			local child = self.fuDaiTableRoot:GetChild(i - 1).gameObject
			child:SetActive(i <= self.playerNum)
			UIEventListener.Get(child).onClick = DelegateFactory.VoidDelegate(function (go)
				self:OnFuDaiClick(i)
			end)
		end
	end

	self.transform:Find("SQS/DoubleReward").gameObject:SetActive(self.playerNum >= ChunJie2024_SongQiongShen.GetData().DoubleRewardNeedPlayerNum)
end

function LuaChunJie2024FuDaiWnd:UpdateFuDai()
	if self.playerNum == 0 then
		self.isAllFuDaiReceived = true
		return
	end

	local rewardChooseInfo = LuaChunJie2024Mgr.fuDaiRewardInfo.rewardChooseInfo

	local myId = CClientMainPlayer.Inst.Id
	self.iHaveReceived = self.iHaveAFuDai and (rewardChooseInfo[myId] ~= nil)
	self.index2PlayerId = {}
	for playerId, index in pairs(rewardChooseInfo) do
		self.index2PlayerId[index] = playerId
	end

	local haveFuDaiCanReceive = false
	for i = 1, self.playerNum do
		local child = self.fuDaiTableRoot:GetChild(i - 1)
		local playerId = self.index2PlayerId[i]

		local received = playerId ~= nil
		local texture = child:Find("Texture")
		texture:Find("Received").gameObject:SetActive(received)
		texture:Find("NotReceived").gameObject:SetActive(not received)
		LuaUtils.SetLocalPositionZ(texture, (self.iHaveAFuDai and (not self.iHaveReceived) and (not received)) and 0 or -1)

		if received then
			local portrait = child:Find("Received/Portrait")
			local playerData = LuaChunJie2024Mgr.fuDaiRewardInfo.playerInfo[playerId]
			portrait:GetComponent(typeof(CUITexture)):LoadPortrait(CUICommonDef.GetPortraitName(tonumber(playerData[2]), tonumber(playerData[3]), -1), true)
			local nameLabel = portrait:Find("Label"):GetComponent(typeof(UILabel))
			nameLabel.text = playerData[1]
			nameLabel.color = NGUIText.ParseColor24(myId == playerId and "00FF00" or "FFFFFF", 0)
		end

		if not received then
			haveFuDaiCanReceive = true
		end
	end
	self.isAllFuDaiReceived = not haveFuDaiCanReceive
end

function LuaChunJie2024FuDaiWnd:InitLeftTime()
	self:ClearTick()
	self.endTimeStamp = CServerTimeMgr.Inst.timeStamp + 9
	self:UpdateLeftTime()
	self.tick = RegisterTick(function()
		self:UpdateLeftTime()
	end, 1000)
end

function LuaChunJie2024FuDaiWnd:UpdateLeftTime()
	local leftTime = math.floor(self.endTimeStamp - CServerTimeMgr.Inst.timeStamp)
	if leftTime < 0 then
		self:ClearTick()
		self.timeRoot:SetActive(false)

		if self.playerNum == 0 then
			CUIManager.CloseUI(CLuaUIResources.ChunJie2024FuDaiWnd)
		end
		return
	end

	self.timeLabel2.text = leftTime
end

function LuaChunJie2024FuDaiWnd:UpdateTimeLabel()
	if self.isAllFuDaiReceived and self.playerNum > 0 then
		self:ClearTick()
		self.timeRoot:SetActive(false)
		return
	end

	if self.playerNum == 0 then
		self.timeLabel1.text = LocalString.GetString("界面将自动关闭")
	elseif not self.iHaveAFuDai then
		self.timeLabel1.text = LocalString.GetString("您的领奖次数已用完，请等待队友领取")
	elseif not self.iHaveReceived then
		if self.playerNum == 1 then
			self.timeLabel1.text = LocalString.GetString("快拿走你的福袋吧！")
		else
			self.timeLabel1.text = LocalString.GetString("点击选择一个福袋")
		end
	else
		if not self.isAllFuDaiReceived then
			self.timeLabel1.text = LocalString.GetString("请等待队友领取")
		end
	end

	self.timeLabel2:ResetAndUpdateAnchors()
	self.timeLabel3:ResetAndUpdateAnchors()
end

function LuaChunJie2024FuDaiWnd:ClearTick()
	if self.tick then
		UnRegisterTick(self.tick)
		self.tick = nil
	end
end

function LuaChunJie2024FuDaiWnd:OnDestroy()
	self:ClearTick()
end

--@region UIEvent

function LuaChunJie2024FuDaiWnd:OnCloseButtonClick()
	if self.isAllFuDaiReceived then
		CUIManager.CloseUI(CLuaUIResources.ChunJie2024FuDaiWnd)
	else
		if self.iHaveAFuDai and not self.iHaveReceived then
			g_MessageMgr:ShowMessage("CHUNJIE2024_NEED_CHOOSE_FUDAI")
		else
			g_MessageMgr:ShowMessage("CHUNJIE2024_FUDAI_NEED_WAIT_TEAMMATE")
		end
	end
end

function LuaChunJie2024FuDaiWnd:OnFuDaiClick(i)
	if self.iHaveReceived then
		g_MessageMgr:ShowMessage("CHUNJIE2024_HAVE_RECEIVED_FUDAI")
		return
	end

	if not self.iHaveAFuDai then
		g_MessageMgr:ShowMessage("CHUNJIE2024_HAVE_NO_FUDAI")
		return
	end

	if self.index2PlayerId[i] then
		g_MessageMgr:ShowMessage("CHUNJIE2024_FUDAI_IS_RECEIVED")
		return
	end

	Gac2Gas.ChooseSongQiongShenReward(i)
end

--@endregion UIEvent
