local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CSortButton = import "L10.UI.CSortButton"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local QnRadioBox=import "L10.UI.QnRadioBox"
local QnTableView=import "L10.UI.QnTableView"
local Profession = import "L10.Game.Profession"
local Double = import "System.Double"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
local AlignType1 = import "CPlayerInfoMgr+AlignType"

CLuaBangHuiJingYingSettingWnd = class()

RegistClassMember(CLuaBangHuiJingYingSettingWnd,"sortRadioBox")
RegistClassMember(CLuaBangHuiJingYingSettingWnd,"tableView")
RegistClassMember(CLuaBangHuiJingYingSettingWnd,"refreshBtn")
RegistClassMember(CLuaBangHuiJingYingSettingWnd,"cancleAllBtn")
RegistClassMember(CLuaBangHuiJingYingSettingWnd,"onlineNumLabel")
RegistClassMember(CLuaBangHuiJingYingSettingWnd,"jingyingNumLabel")
RegistClassMember(CLuaBangHuiJingYingSettingWnd,"mGuildMemberInfoList")
RegistClassMember(CLuaBangHuiJingYingSettingWnd,"m_SortFlags")
RegistClassMember(CLuaBangHuiJingYingSettingWnd,"m_HasRight")
RegistClassMember(CLuaBangHuiJingYingSettingWnd,"leagueButton")
RegistClassMember(CLuaBangHuiJingYingSettingWnd,"tipButton")

function CLuaBangHuiJingYingSettingWnd:Awake()
    self.m_SortFlags={-1,-1,-1,-1,-1,-1,-1,-1}
    self.m_HasRight = false
    
    self.sortRadioBox = self.transform:Find("Anchor/TableView/Header"):GetComponent(typeof(QnRadioBox))
    self.tableView = self.transform:Find("Anchor/TableView"):GetComponent(typeof(QnTableView))
    self.refreshBtn = self.transform:Find("Anchor/TableView/Buttons/RefreshBtn").gameObject
    self.cancleAllBtn = self.transform:Find("Anchor/TableView/Buttons/CancelAllBtn").gameObject
    self.leagueButton = self.transform:Find("Anchor/TableView/Buttons/LeagueButton").gameObject
    self.tipButton = self.transform:Find("Anchor/TableView/Buttons/TipButton").gameObject
    self.onlineNumLabel = self.transform:Find("Anchor/TableView/Buttons/MemberLabel"):GetComponent(typeof(UILabel))
    self.jingyingNumLabel = self.transform:Find("Anchor/TableView/Buttons/JingYingLabel"):GetComponent(typeof(UILabel))
    self.mGuildMemberInfoList = {}

    UIEventListener.Get(self.refreshBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnClickRefreshButton(go)
    end)
    UIEventListener.Get(self.cancleAllBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnClickCancleAllButton(go)
    end)

    self.sortRadioBox.OnSelect =DelegateFactory.Action_QnButton_int(function(btn,index)
        self:OnRadioBoxSelect(btn,index)
    end)

    UIEventListener.Get(self.leagueButton).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.ShowUI(CLuaUIResources.GuildLeagueWeeklyInfoWnd)
    end)

    UIEventListener.Get(self.tipButton).onClick = DelegateFactory.VoidDelegate(function(go)
        g_MessageMgr:ShowMessage("BangHuiJingYing_CancleAllTip")
    end)

end

function CLuaBangHuiJingYingSettingWnd:OnRadioBoxSelect( btn, index) 
    local sortButton = TypeAs(btn, typeof(CSortButton))

    self.m_SortFlags[index+1] = - self.m_SortFlags[index+1]
    for i,v in ipairs(self.m_SortFlags) do
        if i~=index+1 then
            self.m_SortFlags[i] = -1
        end
    end

    sortButton:SetSortTipStatus(self.m_SortFlags[index+1] < 0)
    self:Sort(self.mGuildMemberInfoList, index)
    self.tableView:ReloadData(true, false)
end

function CLuaBangHuiJingYingSettingWnd:OnClickCancleAllButton( go) 
    if self.m_HasRight then
        local msg = g_MessageMgr:FormatMessage("BangHuiJingYing_CancleAllConfirm", nil)
        MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function () 
            Gac2Gas.RequestOperationInGuild(CClientMainPlayer.Inst.BasicProp.GuildId, "DismissAllJingYing", "", "")
        end), nil, nil, nil, false)
    else
        g_MessageMgr:ShowMessage("Guild_No_Power")
    end
end

function CLuaBangHuiJingYingSettingWnd:OnClickRefreshButton( go) 
    CLuaGuildMgr.m_KouDaoInfo=nil
    Gac2Gas.CheckMyRights("AppointJingYing", 0)
end

function CLuaBangHuiJingYingSettingWnd:Init( )
    Gac2Gas.CheckMyRights("AppointJingYing", 0)

    local function initItem(item,row)
        item:SetBackgroundTexture(row % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite)
        self:InitItem(item.transform,self.mGuildMemberInfoList[row+1],row)
    end

    self.tableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.mGuildMemberInfoList
        end,
        initItem)
    
    self.tableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
        local playerId = self.mGuildMemberInfoList[row+1].PlayerId
        local pos = Vector3(540, 0)
        pos = self.transform:TransformPoint(pos)
        CPlayerInfoMgr.ShowPlayerPopupMenu(playerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, pos, AlignType1.Default)
    end)
end

function CLuaBangHuiJingYingSettingWnd:OnEnable( )
    g_ScriptEvent:AddListener("SendGuildInfo_GuildKouDaoInfo", self, "OnSendGuildInfo_GuildKouDaoInfo")
    g_ScriptEvent:AddListener("OnQueryGuildRightsFinished", self, "OnFinishCheckRight")
    g_ScriptEvent:AddListener("RequestOperationInGuildSucceed", self, "OnRequestOperationInGuildSucceed")
end
function CLuaBangHuiJingYingSettingWnd:OnDisable( )
    g_ScriptEvent:RemoveListener("SendGuildInfo_GuildKouDaoInfo", self, "OnSendGuildInfo_GuildKouDaoInfo")
    g_ScriptEvent:RemoveListener("OnQueryGuildRightsFinished", self, "OnFinishCheckRight")
    g_ScriptEvent:RemoveListener("RequestOperationInGuildSucceed", self, "OnRequestOperationInGuildSucceed")
end

function CLuaBangHuiJingYingSettingWnd:OnFinishCheckRight(rightName, context, success)
    if rightName == "AppointJingYing" then
        self.m_HasRight = success
        CLuaGuildMgr.m_KouDaoInfo=nil
        Gac2Gas.RequestGetGuildInfo("GuildKouDaoInfo")
    end
end

function CLuaBangHuiJingYingSettingWnd:OnRequestOperationInGuildSucceed(args) 
    local requestType, paramStr=args[0] ,args[1]
    if "AppointMember" == requestType then
        if paramStr and paramStr ~= "" then
            local playerId = string.match(paramStr, "(%d+)")
			playerId = tonumber(playerId) or 0
            if CLuaGuildMgr.m_KouDaoInfo then
                local val=CLuaGuildMgr.m_KouDaoInfo[playerId]
                if val then val.Title=31 end
            end

            local parent=self.tableView.m_Grid.transform         
            for i,v in ipairs(self.mGuildMemberInfoList) do
                if v.PlayerId==playerId then
                        self:SetItemElite(parent:GetChild(i-1),i-1,true)
                    break
                end
            end

        end
    elseif "DismissMember" == requestType then
        if paramStr and paramStr ~= "" then
            local playerId = tonumber(paramStr)
            if CLuaGuildMgr.m_KouDaoInfo then
                local val=CLuaGuildMgr.m_KouDaoInfo[playerId]
                if val then val.Title=100 end
            end

            local parent=self.tableView.m_Grid.transform         
            for i,v in ipairs(self.mGuildMemberInfoList) do
                if v.PlayerId==playerId then
                        self:SetItemElite(parent:GetChild(i-1),i-1,false)
                    break
                end
            end

        end
    elseif "DismissAllJingYing" == requestType then
        --设置数据
        if CLuaGuildMgr.m_KouDaoInfo then
            for k,v in pairs(CLuaGuildMgr.m_KouDaoInfo) do
                v.Title=100
            end
        end
        --取消设置
        local parent=self.tableView.m_Grid.transform
        for i=1,parent.childCount do
            self:SetItemElite(parent:GetChild(i-1),i-1,false)
        end
    end

    if ("AppointMember" == requestType) or ("DismissMember" == requestType) or ("DismissAllJingYing" == requestType) then
        self:UpdateJingyingNum()
    end
end
function CLuaBangHuiJingYingSettingWnd:OnSendGuildInfo_GuildKouDaoInfo( ) 
    self.mGuildMemberInfoList={}
    local myId = CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.Id or 0

    if CLuaGuildMgr.m_KouDaoInfo then
        for k,v in pairs(CLuaGuildMgr.m_KouDaoInfo) do
            local job = v.Title
            if job then
                if job >= 31 then--帮众.精英
                    table.insert( self.mGuildMemberInfoList, v )
                end
            end            
        end
    end

    --默认排序
    self.sortRadioBox:ChangeTo(-1, true)
    self:Sort(self.mGuildMemberInfoList, -1)

    self.tableView:ReloadData(true, false)

    self:UpdateMemberNum()
    self:UpdateJingyingNum()
end

function CLuaBangHuiJingYingSettingWnd:UpdateMemberNum( )
    local onlineNum = 0
    local num = 0
    if not CLuaGuildMgr.m_KouDaoInfo then
        return
    end
    for k,v in pairs(CLuaGuildMgr.m_KouDaoInfo) do
        if v.IsOnline>0 then
            onlineNum=onlineNum+1
        end
        num = num + 1
    end
    self.onlineNumLabel.text = SafeStringFormat3(LocalString.GetString("帮会在线：[00ff00]%d[-]/%d"), onlineNum, num)--+m
end

function CLuaBangHuiJingYingSettingWnd:UpdateJingyingNum( )
    if self.mGuildMemberInfoList ~= nil then
        local num = 0
        local onlineNum = 0

        for i,v in ipairs(self.mGuildMemberInfoList) do
            if v.Title and v.Title <= 31 then
                num=num+1
                if v.IsOnline>0 then
                    onlineNum=onlineNum+1
                end
            end
        end

        self.jingyingNumLabel.text = SafeStringFormat3( LocalString.GetString("帮会精英：[00ff00]%d[-]/%d/%d"), onlineNum, num, Guild_Setting.GetData().JingYing_Num )
    end
end

function CLuaBangHuiJingYingSettingWnd:Sort(t,sortIndex)
    local function defaultSort(a,b)
        if a.JoinGuildLeagueTime > b.JoinGuildLeagueTime then
            return true
        elseif a.JoinGuildLeagueTime < b.JoinGuildLeagueTime then
            return false
        else
            return a.PlayerId<b.PlayerId
        end
    end
    local flag=self.m_SortFlags[sortIndex+1]
    if sortIndex==0 then--cls name
        table.sort( t, function(a,b)
            local ret=flag
            if a.Class<b.Class then
                ret = -flag
            elseif a.Class>b.Class then
                ret=flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    elseif sortIndex==1 then--level
        table.sort( t, function(a,b)
            local ret=flag
            if a.Level<b.Level then
                ret = flag
            elseif a.Level>b.Level then
                ret = -flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )

    elseif sortIndex==2 then--xiuwei
        table.sort( t, function(a,b)

            local ret=flag
            if a.XiuWei<b.XiuWei then
                ret = flag
            elseif a.XiuWei>b.XiuWei then
                ret = -flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    elseif sortIndex==3 then--xiulian
        table.sort( t, function(a,b)
            local ret=flag
            if a.XiuLian<b.XiuLian then
                ret = flag
            elseif a.XiuLian>b.XiuLian then
                ret = -flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    elseif sortIndex==4 then--equip score
        table.sort( t, function(a,b)
            local ret=flag
            if a.EquipScore<b.EquipScore then
                ret = flag
            elseif a.EquipScore>b.EquipScore then
                ret = -flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    elseif sortIndex==5 then--liansai count
        table.sort( t, function(a,b)
            local ret=flag
            if a.GuildLeagueTimes<b.GuildLeagueTimes then
                ret = flag
            elseif a.GuildLeagueTimes>b.GuildLeagueTimes then
                ret = -flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    elseif sortIndex==6 then--liansai time default
        table.sort( t, function(a,b)
            local ret=flag
            if a.JoinGuildLeagueTime < b.JoinGuildLeagueTime then
                ret = flag
            elseif a.JoinGuildLeagueTime > b.JoinGuildLeagueTime then
                ret = -flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    elseif sortIndex==7 then--jing ying
        table.sort( t, function(a,b)
            local ret=flag
            if a.Title<b.Title then
                ret = flag
            elseif a.Title>b.Title then
                ret = -flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    else
        table.sort( t, function(a,b)
                return defaultSort(a,b)
        end )
    end
end


function CLuaBangHuiJingYingSettingWnd:InitItem(transform,info,row)
    local clsSprite = transform:Find("ClassSprite"):GetComponent(typeof(UISprite))
    local nameLabel = transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local levelLabel = transform:Find("LevelLabel"):GetComponent(typeof(UILabel))
    local xiuweiLabel = transform:Find("XiuweiLabel"):GetComponent(typeof(UILabel))
    local xiulianLabel = transform:Find("XiulianLabel"):GetComponent(typeof(UILabel))
    local equipScoreLabel = transform:Find("EquipScoreLabel"):GetComponent(typeof(UILabel))
    local lianSaiCountLabel = transform:Find("LianSaiCountLabel"):GetComponent(typeof(UILabel))
    local lianSaiTimeLabel = transform:Find("LianSaiTimeLabel"):GetComponent(typeof(UILabel))
    local tickSprite = transform:Find("TickSprite"):GetComponent(typeof(UISprite))

    local playerId=info.PlayerId
    local myId = CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.Id or 0

    if playerId == myId then
        nameLabel.color = Color.green
        levelLabel.color = Color.green
        xiuweiLabel.color = Color.green
        xiulianLabel.color = Color.green
        equipScoreLabel.color = Color.green
        lianSaiCountLabel.color = Color.green
        lianSaiTimeLabel.color = Color.green
    else
        nameLabel.color = Color.white
        levelLabel.color = Color.white
        xiuweiLabel.color = Color.white
        xiulianLabel.color = Color.white
        equipScoreLabel.color = Color.white
        lianSaiCountLabel.color = Color.white
        lianSaiTimeLabel.color = Color.white
    end


    nameLabel.text = info.Name
    clsSprite.spriteName = Profession.GetIconByNumber(info.Class)

    CUICommonDef.SetActive(clsSprite.gameObject, info.IsOnline > 0, true)

    local default
    if info.XianFanStatus > 0 then
        default=SafeStringFormat3( "[c][ff7900]Lv.%d[-][/c]",info.Level )
    else
        default=SafeStringFormat3( "Lv.%d",info.Level )
    end
    levelLabel.text = default

    xiuweiLabel.text = NumberComplexToString(NumberTruncate(info.XiuWei, 1), typeof(Double), "F1")
    --一位小数显示
    xiulianLabel.text = tostring(info.XiuLian)

    equipScoreLabel.text = tostring(info.EquipScore)
    --联赛次数
    lianSaiCountLabel.text = info.GuildLeagueTimes
    --联赛时间
    local minute = (math.floor(info.JoinGuildLeagueTime / 60))
    local hour = math.floor(minute / 60)
    if hour > 0 then
        lianSaiTimeLabel.text =  SafeStringFormat3(LocalString.GetString("%d小时"), hour)
    else
        lianSaiTimeLabel.text =  SafeStringFormat3(LocalString.GetString("%d分钟"), minute)
    end
    --lianSaiTimeLabel.text = info.JoinGuildLeagueTime

    local isElite = info.Title == 31
    self:SetItemElite(transform,row,isElite)

    UIEventListener.Get(tickSprite.transform:GetChild(0).gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        if tickSprite.enabled then
            --撤销
            local str =tostring(playerId)
            Gac2Gas.RequestOperationInGuild(CClientMainPlayer.Inst.BasicProp.GuildId, "DismissMember", str, "")
        else
            local str =SafeStringFormat3( "%s,31",playerId )
            Gac2Gas.RequestOperationInGuild(CClientMainPlayer.Inst.BasicProp.GuildId, "AppointMember", str, "")
        end
    end)
end

function CLuaBangHuiJingYingSettingWnd:SetItemElite(transform,row,isElite)
    local tickSprite = transform:Find("TickSprite"):GetComponent(typeof(UISprite))
    tickSprite.enabled = isElite
end
