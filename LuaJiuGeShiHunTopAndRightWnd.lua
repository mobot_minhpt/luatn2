local UITexture = import "UITexture"

LuaJiuGeShiHunTopAndRightWnd = class()

RegistChildComponent(LuaJiuGeShiHunTopAndRightWnd, "m_Foreground","Foreground", UITexture)
RegistChildComponent(LuaJiuGeShiHunTopAndRightWnd, "m_PercentLabel","PercentLabel", UILabel)
RegistChildComponent(LuaJiuGeShiHunTopAndRightWnd, "m_RedBorder","RedBorder", GameObject)

function LuaJiuGeShiHunTopAndRightWnd:OnEnable()
    self.m_RedBorder:SetActive(false)
    self:OnUpdateHpPercent()

    g_ScriptEvent:AddListener("JiuGeShiHunHpPercentUpdate", self, "OnUpdateHpPercent")
end

function LuaJiuGeShiHunTopAndRightWnd:OnDisable()
    g_ScriptEvent:RemoveListener("JiuGeShiHunHpPercentUpdate", self, "OnUpdateHpPercent")
end

function LuaJiuGeShiHunTopAndRightWnd:OnUpdateHpPercent()
    self.m_PercentLabel.text = cs_string.Format("{0}%", DuanWu2020Mgr.m_hpPercent)
    self.m_Foreground.fillAmount = DuanWu2020Mgr.m_hpPercent / 100
    self.m_RedBorder:SetActive(DuanWu2020Mgr.m_hpPercent < 30)
end