-- Auto Generated!!
local CItemMgr = import "L10.Game.CItemMgr"
local CLivingSurpriseItem = import "L10.UI.CLivingSurpriseItem"
CLivingSurpriseItem.m_Init_CS2LuaHook = function (this, templateId) 
    this.templateId = templateId
    local template = CItemMgr.Inst:GetItemTemplate(templateId)
    this.texture:LoadMaterial(template.Icon)
end
