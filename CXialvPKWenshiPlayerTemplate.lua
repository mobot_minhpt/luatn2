-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CKeJuMgr = import "L10.Game.CKeJuMgr"
local CXialvPKWenshiPlayerTemplate = import "L10.UI.CXialvPKWenshiPlayerTemplate"
local LocalString = import "LocalString"
CXialvPKWenshiPlayerTemplate.m_Init_CS2LuaHook = function (this, playerId, playerName, playerScore, playerAnswerIdx) 

    if playerId == CClientMainPlayer.Inst.Id then
        this.m_PlayerName.text = System.String.Format("[{0}]{1}[-]", CXialvPKWenshiPlayerTemplate.rightColor, playerName)
    else
        this.m_PlayerName.text = playerName
    end

    this.m_PlayerScore.text = tostring(playerScore) .. LocalString.GetString("分")
    this.m_Answer.gameObject:SetActive(false)

    if playerAnswerIdx <= 0 then
        this:ClearPlayerAnswer()
    else
        this:UpdatePlayerAnswer(playerAnswerIdx)
    end
end
CXialvPKWenshiPlayerTemplate.m_UpdatePlayerAnswer_CS2LuaHook = function (this, answerIdx) 
    this.m_Answer.gameObject:SetActive(true)
    if answerIdx == CKeJuMgr.Inst.rightAnswerIndex then
        this.m_Answer.text = System.String.Format("[{0}]{1}[-]", CXialvPKWenshiPlayerTemplate.rightColor, this:GetSelection(answerIdx))
    else
        this.m_Answer.text = System.String.Format("[{0}]{1}[-]", CXialvPKWenshiPlayerTemplate.wrongColor, this:GetSelection(answerIdx))
    end
end
CXialvPKWenshiPlayerTemplate.m_GetSelection_CS2LuaHook = function (this, answerIdx) 
    local result = nil
    repeat
        local default = answerIdx
        if default == (1) then
            result = "A"
            break
        elseif default == (2) then
            result = "B"
            break
        elseif default == (3) then
            result = "C"
            break
        elseif default == (4) then
            result = "D"
            break
        else
            result = ""
            break
        end
    until 1
    return result
end
