local CUIFx = import "L10.UI.CUIFx"
local CButton = import "L10.UI.CButton"
local DelegateFactory = import "DelegateFactory"
local CBaseWnd = import "L10.UI.CBaseWnd"
local Extensions = import "Extensions"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"

LuaFlagonOpenWnd = class()

RegistChildComponent(LuaFlagonOpenWnd,"m_Fx","Fx", CUIFx)
RegistChildComponent(LuaFlagonOpenWnd,"m_RootGo","Root",  GameObject)
RegistChildComponent(LuaFlagonOpenWnd,"m_ItemTemplateGo","ItemTemplate", GameObject)
RegistChildComponent(LuaFlagonOpenWnd,"m_ReceiveRewardButton","ReceiveRewardButton", CButton)
RegistChildComponent(LuaFlagonOpenWnd,"m_DoItAgaintButton","DoItAgaintButton", CButton)
RegistChildComponent(LuaFlagonOpenWnd,"m_NumLabel","NumLabel", UILabel)
RegistChildComponent(LuaFlagonOpenWnd,"m_Grid","Grid", UIGrid)

RegistClassMember(LuaFlagonOpenWnd,"m_CurItems")
RegistClassMember(LuaFlagonOpenWnd,"m_OpenBottleTimes")

function LuaFlagonOpenWnd:Init()

    UIEventListener.Get(self.m_ReceiveRewardButton.gameObject).onClick = DelegateFactory.VoidDelegate(function()
        self:ReceiveReward()
    end)
    UIEventListener.Get(self.m_DoItAgaintButton.gameObject).onClick = DelegateFactory.VoidDelegate(function()
        self:DoItAgaint()
    end)
    self.m_ItemTemplateGo:SetActive(false)
    self.m_RootGo:SetActive(false)

    --Gac2Gas.RequestOpenHanJiaBottle()
end

function LuaFlagonOpenWnd:OnEnable()
    g_ScriptEvent:AddListener("HanJia2020_OnBottleOpen", self, "OnBottleOpen")
    HanJia2020Mgr.isOpenFlagonWnd = true
    g_ScriptEvent:BroadcastInLua("HanJia2020_OnBottleOpen",HanJia2020Mgr.FlagonWndData)
    HanJia2020Mgr.FlagonWndData = nil
end

function LuaFlagonOpenWnd:OnDisable()
    g_ScriptEvent:RemoveListener("HanJia2020_OnBottleOpen", self, "OnBottleOpen")
    HanJia2020Mgr.isOpenFlagonWnd = false
    self:CancelShowRootTick()
end

function LuaFlagonOpenWnd:InitItems(data)
    Extensions.RemoveAllChildren(self.m_Grid.transform)

    self.m_CurItems= {}
    self.m_ItemTemplateGo:SetActive(false)
    local len = #data
    for i = 1, len do
        local item = Item_Item.GetData(data[i].ID)
        if item then
            local go = NGUITools.AddChild(self.m_Grid.gameObject, self.m_ItemTemplateGo)
            go:SetActive(true)
            local tex = go:GetComponent(typeof(CUITexture))
            tex:LoadMaterial(item.Icon)
            local label = go.transform:Find("Label"):GetComponent(typeof(UILabel))
            label.text = data[i].Count
            local nameLabel = go.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
            nameLabel.text = item.Name
            go:SetActive(true)

            local id = data[i].ID
            UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function()
                CItemInfoMgr.ShowLinkItemTemplateInfo(id, false, nil, AlignType.Default, 0, 0, 0, 0)
            end)

        end
    end
    self.m_Grid:Reposition()
end

function LuaFlagonOpenWnd:ReceiveReward()
    self:Close()
end

function LuaFlagonOpenWnd:DoItAgaint()
    Gac2Gas.RequestOpenHanJiaBottle()
end

function LuaFlagonOpenWnd:Close()
    self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaFlagonOpenWnd:OnBottleOpen(data)
    local openBottleTimes = data[1]
    local rewardsTable = data[2]
    
    self.m_RootGo:SetActive(false)
    self.m_Fx:DestroyFx()
    self.m_Fx:LoadFx("fx/ui/prefab/UI_dagongshouce_dakaijiuhu.prefab")
    self:CancelShowRootTick()
    self.m_ShowRootTick = RegisterTickOnce(function()
        self.m_RootGo:SetActive(true)
    end, 1000)

    self.m_NumLabel.text = openBottleTimes
    self.m_OpenBottleTimes = openBottleTimes
    self.m_NumLabel.color = self.m_OpenBottleTimes == 0 and Color.red or Color.white
    self:InitItems(rewardsTable)
end

function LuaFlagonOpenWnd:CancelShowRootTick()
    if self.m_ShowRootTick then
        UnRegisterTick(self.m_ShowRootTick)
        self.m_ShowRootTick = nil
    end
end