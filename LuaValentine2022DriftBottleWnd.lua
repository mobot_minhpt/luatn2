local CUITexture = import "L10.UI.CUITexture"
local DelegateFactory  = import "DelegateFactory"
local UIInput = import "UIInput"
local GameObject = import "UnityEngine.GameObject"
local QnCheckBox = import "L10.UI.QnCheckBox"
local UILabel = import "UILabel"
local CUIFx = import "L10.UI.CUIFx"
local CIMMgr = import "L10.Game.CIMMgr"
local CPersonalSpace_ExpressionBase = import "L10.Game.CPersonalSpace_ExpressionBase"
local CExpressionMgr = import "L10.Game.CExpressionMgr"
local CCommonPlayerListMgr = import "L10.Game.CCommonPlayerListMgr"
local CCommonPlayerDisplayData = import "L10.Game.CCommonPlayerDisplayData"
local EnumCommonPlayerListUpdateType = import "L10.Game.EnumCommonPlayerListUpdateType"
local UILongPressButton = import "L10.UI.UILongPressButton"
local CRecordingInfoMgr = import "L10.UI.CRecordingInfoMgr"
local EnumRecordContext = import "L10.Game.EnumRecordContext"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EChatPanel = import "L10.Game.EChatPanel"
local CUICommonDef = import "L10.UI.CUICommonDef"
local EnumVoicePlatform = import "L10.Game.EnumVoicePlatform"
local CRecordingWnd = import "L10.UI.CRecordingWnd"
local UISpriteAnimation = import "UISpriteAnimation"
local CItemMgr = import "L10.Game.CItemMgr"
local BoxCollider = import "UnityEngine.BoxCollider"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"

LuaValentine2022DriftBottleWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaValentine2022DriftBottleWnd, "NameInput", "NameInput", UIInput)
RegistChildComponent(LuaValentine2022DriftBottleWnd, "ChoosePlayButton", "ChoosePlayButton", GameObject)
RegistChildComponent(LuaValentine2022DriftBottleWnd, "StatusText", "StatusText", UIInput)
RegistChildComponent(LuaValentine2022DriftBottleWnd, "VoiceBtn", "VoiceBtn", UILongPressButton)
RegistChildComponent(LuaValentine2022DriftBottleWnd, "SendButton", "SendButton", GameObject)
RegistChildComponent(LuaValentine2022DriftBottleWnd, "QnCheckBox", "QnCheckBox", QnCheckBox)
RegistChildComponent(LuaValentine2022DriftBottleWnd, "NoneVoiceLabel", "NoneVoiceLabel", UILabel)
RegistChildComponent(LuaValentine2022DriftBottleWnd, "BottomLabel", "BottomLabel", UILabel)
RegistChildComponent(LuaValentine2022DriftBottleWnd, "Fx", "Fx", CUIFx)
RegistChildComponent(LuaValentine2022DriftBottleWnd, "VoiceBar", "VoiceBar", GameObject)
RegistChildComponent(LuaValentine2022DriftBottleWnd, "Anchor", "Anchor", GameObject)
RegistChildComponent(LuaValentine2022DriftBottleWnd, "Icon", "Icon", CUITexture)
RegistChildComponent(LuaValentine2022DriftBottleWnd, "Sticker", "Sticker", CUITexture)
RegistChildComponent(LuaValentine2022DriftBottleWnd, "ExpressionTxt", "ExpressionTxt", CUITexture)
RegistChildComponent(LuaValentine2022DriftBottleWnd, "LoudSpeaker", "LoudSpeaker", UISpriteAnimation)
RegistChildComponent(LuaValentine2022DriftBottleWnd, "VoiceBarSprite", "VoiceBarSprite", UISprite)
RegistChildComponent(LuaValentine2022DriftBottleWnd, "VoiceLengthLabel", "VoiceLengthLabel", UILabel)
RegistChildComponent(LuaValentine2022DriftBottleWnd, "CloseButton", "CloseButton", GameObject)
--@endregion RegistChildComponent end
RegistClassMember(LuaValentine2022DriftBottleWnd, "m_IsAnonymous")
RegistClassMember(LuaValentine2022DriftBottleWnd, "m_SelectPlayerId")
RegistClassMember(LuaValentine2022DriftBottleWnd, "m_VoiceUrl")
RegistClassMember(LuaValentine2022DriftBottleWnd, "m_VoiceDuration")
RegistClassMember(LuaValentine2022DriftBottleWnd, "m_CloseWndTick")

function LuaValentine2022DriftBottleWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.ChoosePlayButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnChoosePlayButtonClick()
	end)

	UIEventListener.Get(self.SendButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSendButtonClick()
	end)

	UIEventListener.Get(self.VoiceBarSprite.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnVoiceBarSpriteClick()
	end)

	UIEventListener.Get(self.CloseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCloseButtonClick()
	end)
    --@endregion EventBind end
end

function LuaValentine2022DriftBottleWnd:Init()
	self.QnCheckBox.OnValueChanged = DelegateFactory.Action_bool(function (value)
		self:OnQnCheckBoxChanged(value)
	end)
	self.ChoosePlayButton.gameObject:SetActive(LuaValentine2022Mgr.m_IsSpecialDriftBottleItem and not LuaValentine2022Mgr.m_IsShowDriftBottle)
	self.QnCheckBox.transform:Find("Background").gameObject:SetActive(not LuaValentine2022Mgr.m_IsShowDriftBottle)
	self.QnCheckBox.transform:GetComponent(typeof(BoxCollider)).enabled = not LuaValentine2022Mgr.m_IsShowDriftBottle
	self.VoiceBtn.gameObject:SetActive(not LuaValentine2022Mgr.m_IsShowDriftBottle)
	self.VoiceBar.gameObject:SetActive(LuaValentine2022Mgr.m_IsShowDriftBottle and not System.String.IsNullOrEmpty(LuaValentine2022Mgr.m_ShowDriftBottleData.voiceUrl))
	self.NoneVoiceLabel.gameObject:SetActive(not LuaValentine2022Mgr.m_IsShowDriftBottle)
	self.SendButton.gameObject:SetActive(not LuaValentine2022Mgr.m_IsShowDriftBottle)
	self.StatusText.gameObject:GetComponent(typeof(BoxCollider)).enabled = not LuaValentine2022Mgr.m_IsShowDriftBottle
	if LuaValentine2022Mgr.m_IsShowDriftBottle then
		self.NameInput.label.text = LuaValentine2022Mgr.m_ShowDriftBottleData.receiverName
		self.StatusText.value = LuaValentine2022Mgr.m_ShowDriftBottleData.content
		self.m_VoiceUrl = LuaValentine2022Mgr.m_ShowDriftBottleData.voiceUrl
		self.VoiceLengthLabel.text = SafeStringFormat3(LocalString.GetString("%d秒"), LuaValentine2022Mgr.m_ShowDriftBottleData.voiceDuration)
		self.VoiceBarSprite:ResetAnchors()
		self.QnCheckBox:SetSelected(LuaValentine2022Mgr.m_ShowDriftBottleData.anonymous, false) 
	else
		if not LuaValentine2022Mgr.m_IsSpecialDriftBottleItem then
			self.NameInput.label.text = LocalString.GetString("有缘人")
			self.m_SelectPlayerId = 0
		end
		UIEventListener.Get(self.VoiceBtn.gameObject).onPress = DelegateFactory.BoolDelegate(function (go, isPressed)
			self:OnPress(go, isPressed)
		end)
		self.QnCheckBox:SetSelected(false, false) 
	end
	self.Fx:LoadFx("fx/ui/prefab/UI_qingrenjie_hua.prefab")
end

function LuaValentine2022DriftBottleWnd:OnEnable()
	g_ScriptEvent:AddListener("UploadWebVoiceViaPersonalSpaceFinished", self, "UploadWebVoiceViaPersonalSpaceFinished")
	g_ScriptEvent:AddListener("OnVoicePlayStart", self, "OnVoicePlayStart")
	g_ScriptEvent:AddListener("OnVoicePlayFinish", self, "OnVoicePlayFinish")
	g_ScriptEvent:AddListener("GenerateDriftBottleSuccess", self, "OnGenerateDriftBottleSuccess")
	CRecordingWnd.recordingText = LocalString.GetString("手指滑开，取消输入")
	CRecordingWnd.cancelRecordingText = LocalString.GetString("松开手指，取消输入")
end

function LuaValentine2022DriftBottleWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UploadWebVoiceViaPersonalSpaceFinished", self, "UploadWebVoiceViaPersonalSpaceFinished")
	g_ScriptEvent:RemoveListener("OnVoicePlayStart", self, "OnVoicePlayStart")
	g_ScriptEvent:RemoveListener("OnVoicePlayFinish", self, "OnVoicePlayFinish")
	g_ScriptEvent:RemoveListener("GenerateDriftBottleSuccess", self, "OnGenerateDriftBottleSuccess")
	CRecordingWnd.recordingText = LocalString.GetString("手指滑开，取消发送")
	CRecordingWnd.cancelRecordingText = LocalString.GetString("松开手指，取消发送")
	self:CancelCloseWndTick()
end

function LuaValentine2022DriftBottleWnd:OnGenerateDriftBottleSuccess()
	self.Anchor.gameObject:SetActive(false)
	self:CancelCloseWndTick()
	self.Fx:LoadFx("fx/ui/prefab/UI_qingrenjie_piaoliuping.prefab")
	self.m_CloseWndTick = RegisterTickOnce(function ()
		CUIManager.CloseUI(CLuaUIResources.Valentine2022DriftBottleWnd)
	end,5000)
end

function LuaValentine2022DriftBottleWnd:UploadWebVoiceViaPersonalSpaceFinished(argv)
	local voiceId = argv[0]
	local timeDuration = argv[1]
	self.m_VoiceDuration = timeDuration
	self.m_VoiceUrl = CLuaMusicBoxMgr:GetPartialUrl(voiceId)
	self.NoneVoiceLabel.gameObject:SetActive(not self.m_VoiceUrl)
	self.VoiceBar.gameObject:SetActive(self.m_VoiceUrl)
	self.VoiceLengthLabel.text = SafeStringFormat3(LocalString.GetString("%d秒"), timeDuration)
	self.VoiceBarSprite:ResetAnchors()
end

function LuaValentine2022DriftBottleWnd:OnVoicePlayStart()
	self.LoudSpeaker.enabled = true
end

function LuaValentine2022DriftBottleWnd:OnVoicePlayFinish()
	self.LoudSpeaker.enabled = false
end

--@region UIEvent
function LuaValentine2022DriftBottleWnd:OnCloseButtonClick()
	if LuaValentine2022Mgr.m_IsShowDriftBottle then
		CUIManager.CloseUI(CLuaUIResources.Valentine2022DriftBottleWnd)
		return
	end
	local msg = g_MessageMgr:FormatMessage("Valentine2022DriftBottleWnd_CloseWndConfirm")
	MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
		CUIManager.CloseUI(CLuaUIResources.Valentine2022DriftBottleWnd)
	end),nil,nil,nil,false)
end

function LuaValentine2022DriftBottleWnd:OnChoosePlayButtonClick()
	local player = CClientMainPlayer.Inst
    if not player then
        return
    end

    CCommonPlayerListMgr.Inst.WndTitle = LocalString.GetString("选择对象")
    CCommonPlayerListMgr.Inst.ButtonText = LocalString.GetString("选择")
    CCommonPlayerListMgr.Inst.UpdateType = EnumCommonPlayerListUpdateType.Default
    CommonDefs.ListClear(CCommonPlayerListMgr.Inst.allData)

    -- 加在线好友
    local friendlinessTbl = {}
	local playerInfoDict = {}
    CommonDefs.DictIterate(player.RelationshipProp.Friends, DelegateFactory.Action_object_object(function(k, v)
        if CIMMgr.Inst:IsSameServerFriend(k) then
            table.insert(friendlinessTbl, {k, v.Friendliness})
        end
    end))
    table.sort(friendlinessTbl, function(v1, v2) return v1[2] > v2[2] end)

    for _, info in pairs(friendlinessTbl) do
        local basicInfo = CIMMgr.Inst:GetBasicInfo(info[1])
        if basicInfo then
            local data = CreateFromClass(CCommonPlayerDisplayData, basicInfo.ID, basicInfo.Name, basicInfo.Level, basicInfo.Class, basicInfo.Gender, basicInfo.Expression, true)
            CommonDefs.ListAdd(CCommonPlayerListMgr.Inst.allData, typeof(CCommonPlayerDisplayData), data)
			playerInfoDict[basicInfo.ID] = data
        end
    end

    CCommonPlayerListMgr.Inst.OnPlayerSelected = DelegateFactory.Action_ulong(function(playerId)
		local data = playerInfoDict[playerId]
		self:OnSelectPlayer(data)
        CUIManager.CloseUI(CIndirectUIResources.CommonPlayerListWnd)
    end)
    CUIManager.ShowUI(CIndirectUIResources.CommonPlayerListWnd)
end

function LuaValentine2022DriftBottleWnd:OnSelectPlayer(data)
	self.m_SelectPlayerId = data.playerId
	self.NameInput.label.text = data.playerName
end

function LuaValentine2022DriftBottleWnd:OnPress(go, isPressed)
	if isPressed then
        CRecordingInfoMgr.ShowRecordingWnd(EnumRecordContext.Undefined, EChatPanel.Undefined, go, EnumVoicePlatform.WebBrowser, false)
    else
        CRecordingInfoMgr.CloseRecordingWnd(not go:Equals(CUICommonDef.SelectedUI))
    end
	self.NoneVoiceLabel.text = isPressed and LocalString.GetString("(松开结束)") or LocalString.GetString("(点击右侧按钮输入按钮)")
end

function LuaValentine2022DriftBottleWnd:OnSendButtonClick()
	if not self.m_SelectPlayerId then
		g_MessageMgr:ShowMessage("Valentine2022DriftBottleWnd_NoneSelectPlayer")
		return
	end
	if not self.m_VoiceUrl then 
		self.m_VoiceUrl = ""
		self.m_VoiceDuration = 0
	end
	if System.String.IsNullOrEmpty(self.StatusText.value) and System.String.IsNullOrEmpty(self.m_VoiceUrl) then
		g_MessageMgr:ShowMessage("Valentine2022DriftBottleWnd_NoneText")
        return
    end
	local itemId = Valentine_DriftBottle.GetData().EmptyDriftBottleItemID
	local default, pos, itemId = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, itemId)
	if default then
		self:MakeDriftBottle(false)
	else
		local msg = g_MessageMgr:FormatMessage("Valentine2022DriftBottleWnd_BuyItemConfirm")
		MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
			self:MakeDriftBottle(true)
		end),nil,nil,nil,false)
	end
end

function LuaValentine2022DriftBottleWnd:MakeDriftBottle(isNeedBuyItem)
	local text = self.StatusText.value
	text = CWordFilterMgr.Inst:DoFilterOnSendPersonalSpace(text, true)
	if System.String.IsNullOrEmpty(text) and System.String.IsNullOrEmpty(self.m_VoiceUrl) then 
		return 
	end
	if not text then 
		return 
	end
	Gac2Gas.RequestGenerateDriftBottle(
			LuaValentine2022Mgr.m_DriftBottleItemId, LuaValentine2022Mgr.m_DriftBottleItemPlace, LuaValentine2022Mgr.m_DriftBottleItemPos, self.m_IsAnonymous, 
			self.m_SelectPlayerId, self.NameInput.label.text, text, self.m_VoiceUrl, self.m_VoiceDuration,isNeedBuyItem)
end

function LuaValentine2022DriftBottleWnd:CancelCloseWndTick()
	if self.m_CloseWndTick then
		UnRegisterTick(self.m_CloseWndTick)
		self.m_CloseWndTick = nil
	end
end

function LuaValentine2022DriftBottleWnd:OnVoiceBarSpriteClick()
	if not self.m_VoiceUrl then return end
	CLuaMusicBoxMgr:PlayVoice(self.m_VoiceUrl, CLuaMusicBoxMgr:GetFullUrl(self.m_VoiceUrl), true)
end
--@endregion UIEvent


function LuaValentine2022DriftBottleWnd:OnQnCheckBoxChanged(value)
	self.m_IsAnonymous = value
	self.BottomLabel.text = LocalString.GetString("来自")
	local baseInfo = CPersonalSpace_ExpressionBase(0,0,0,0)
	if LuaValentine2022Mgr.m_IsShowDriftBottle then
		self.QnCheckBox.gameObject:SetActive(value)
		self.Icon:LoadNPCPortrait(CUICommonDef.GetPortraitName(LuaValentine2022Mgr.m_ShowDriftBottleData.senderClass, LuaValentine2022Mgr.m_ShowDriftBottleData.senderGender, LuaValentine2022Mgr.m_ShowDriftBottleData.senderExpression),false)
		if not value then
			baseInfo = CPersonalSpace_ExpressionBase(0,LuaValentine2022Mgr.m_ShowDriftBottleData.senderExpressionTxt,LuaValentine2022Mgr.m_ShowDriftBottleData.senderSticker,0)
			self.BottomLabel.text = SafeStringFormat3(LocalString.GetString("来自  %s"),LuaValentine2022Mgr.m_ShowDriftBottleData.senderName)
		end
	else
		if CClientMainPlayer.Inst then
			self.Icon:LoadNPCPortrait(CClientMainPlayer.Inst.PortraitName, false)
			if not value then
				baseInfo = CPersonalSpace_ExpressionBase(0,CClientMainPlayer.Inst.BasicProp.ExpressionTxt,CClientMainPlayer.Inst.BasicProp.Sticker,0)
				self.BottomLabel.text = SafeStringFormat3(LocalString.GetString("来自  %s"),CClientMainPlayer.Inst.Name)
			end
		end
	end
	CExpressionMgr.Inst:SetIconTextInfo(self.Sticker, baseInfo, CExpressionMgr.Inst.expression_extra,false)
	CExpressionMgr.Inst:SetIconTextInfo(self.ExpressionTxt, baseInfo, CExpressionMgr.Inst.expression_extra,true)
	if self.m_IsAnonymous then
		self.Icon.uiTexture = nil
		return 
	end
	CPersonalSpaceMgr.Inst:GetUserProfile(LuaValentine2022Mgr.m_IsShowDriftBottle and LuaValentine2022Mgr.m_ShowDriftBottleData.senderId or (CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0), DelegateFactory.Action_CPersonalSpace_UserProfile_Ret(function (data)
		if data.code == 0 then
			local _data = data.data
			if not System.String.IsNullOrEmpty(_data.photo) then
				self.Icon:Clear()
				self.Sticker:Clear()
				self.ExpressionTxt:Clear()
				CPersonalSpaceMgr.Inst:DownLoadPortraitPic(_data.photo, self.Icon.uiTexture, nil)
			end
		end
	end))
end