local CDuoMaoMaoMgr = import "L10.Game.CDuoMaoMaoMgr"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CClientNpc = import "L10.Game.CClientNpc"
local CHeadInfoWnd = import "L10.UI.CHeadInfoWnd"
--@desc 处理躲猫猫迭代
LuaDuoMaoMaoMgr = {}

function LuaDuoMaoMaoMgr.UpdateDuoMaoMaoHP(playerId, current, full)
    g_ScriptEvent:BroadcastInLua("UpdateGameplayHP", current, full)
end

-- 报名或者取消报名的返回
function Gas2Gac.SyncDuoMaoMaoSignUpResult(bSignUp)
    if bSignUp then
        -- 报名成功
        --CGas2Gac.RequestSignUpPlayByKeySuccess(1)
        EventManager.Broadcast(EnumEventType.SignUpHideAndSeekSuccess)
    else
        -- 取消报名
        --CGas2Gac.RequestCancelSignUpPlayByKeySuccess(1)
        EventManager.Broadcast(EnumEventType.CancelSignUpHideAndSeekSuccess)
    end
end

-- 向队伍成员同步玩家血量 playerId可能是队友  也可能是自己
function Gas2Gac.SyncPlayerDuoMaoMaoHp(playerId, curHp, fullhp, hasAddHp)
    CDuoMaoMaoMgr.Inst:UpdateGamePlayHP(playerId, curHp, fullhp, hasAddHp)
end

-- 妖气血量 会在这个rpc:ShowNpcToPlayer之后发给客户端
function Gas2Gac.SyncDuoMaoMaoYaoQiHp(npcEngineId, hp, maxHp)
    -- 将显示NPC的血量
    local obj = CClientObjectMgr.Inst:GetObject(npcEngineId)
    if obj and obj:GetType() == typeof(CClientNpc) then
        obj.showHpBar = true
        obj.Hp = hp
        obj.HpFull = maxHp
        CHeadInfoWnd.Instance:OnClientObjExcluded(npcEngineId, false)
    end
end

-- 同步玩家加血技能点数
function Gas2Gac.SyncDuoMaoMaoAddHpSkillPoint(addHpSkillPoint)
    g_ScriptEvent:BroadcastInLua("SyncDuoMaoMaoAddHpSkillPoint", addHpSkillPoint)
end
