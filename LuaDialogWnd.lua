local CClientObjectMgr=import "L10.Game.CClientObjectMgr"
local Time = import "UnityEngine.Time"
local System = import "System"
local SoundManager = import "SoundManager"
local NGUIText = import "NGUIText"
local NGUIMath = import "NGUIMath"
local MessageWndManager = import "L10.UI.MessageWndManager"
local Extensions = import "Extensions"
local Constants = import "L10.Game.Constants"
local CNPCShopInfoMgr = import "L10.UI.CNPCShopInfoMgr"
local CConversationMgr = import "L10.Game.CConversationMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CChatLinkMgr = import "CChatLinkMgr"
local CUITexture=import "L10.UI.CUITexture"
local CClientObjectRoot=import "L10.Game.CClientObjectRoot"
local LayerDefine = import "L10.Engine.LayerDefine"

CLuaDialogWnd = class()
RegistClassMember(CLuaDialogWnd,"nameLabel")
RegistClassMember(CLuaDialogWnd,"headPortrait")
RegistClassMember(CLuaDialogWnd,"descriptionScrollView")
RegistClassMember(CLuaDialogWnd,"descriptionLabel")
RegistClassMember(CLuaDialogWnd,"selectionsRoot")
RegistClassMember(CLuaDialogWnd,"selectionsScrollView")
RegistClassMember(CLuaDialogWnd,"selectionsTable")
RegistClassMember(CLuaDialogWnd,"selectionsBg")
RegistClassMember(CLuaDialogWnd,"selectionsTemplate")
RegistClassMember(CLuaDialogWnd,"selectionsScrollViewIndicator")
RegistClassMember(CLuaDialogWnd,"dynamicOptionCount")
RegistClassMember(CLuaDialogWnd,"onSelectDelegate")
RegistClassMember(CLuaDialogWnd,"m_CustomDialogActionBg")
RegistClassMember(CLuaDialogWnd,"m_DefaultDialogActionBg")
RegistClassMember(CLuaDialogWnd,"m_MessageLookup")
RegistClassMember(CLuaDialogWnd,"GmWatchedDialog")
RegistClassMember(CLuaDialogWnd,"m_SoundButton")
RegistClassMember(CLuaDialogWnd,"m_SoundSettingButton")
RegistClassMember(CLuaDialogWnd,"m_HideSelf")
RegistClassMember(CLuaDialogWnd,"m_HideOther")
RegistClassMember(CLuaDialogWnd,"selections")

CLuaDialogWnd.m_LastNpcId = 0
CLuaDialogWnd.m_LastPlayTime = 0

function CLuaDialogWnd:Awake()
    self.nameLabel = self.transform:Find("Anchor/Portrait/Name/Label"):GetComponent(typeof(UILabel))
    self.headPortrait = self.transform:Find("Anchor/Portrait"):GetComponent(typeof(CUITexture))
    self.descriptionScrollView = self.transform:Find("Anchor/Description/ScrollView"):GetComponent(typeof(UIScrollView))
    self.descriptionLabel = self.transform:Find("Anchor/Description/ScrollView/Label"):GetComponent(typeof(UILabel))

    self.selectionsRoot = self.transform:Find("Anchor/Selections").gameObject
    self.selectionsScrollView = self.transform:Find("Anchor/Selections/ScrollView"):GetComponent(typeof(UIScrollView))
    self.selectionsTable = self.transform:Find("Anchor/Selections/ScrollView/Table"):GetComponent(typeof(UITable))
    self.selectionsBg = self.transform:Find("Anchor/Selections/Background"):GetComponent(typeof(UISprite))
    self.selectionsTemplate = self.transform:Find("Anchor/Selections/ScrollView/ItemTemplate").gameObject
    self.selectionsScrollViewIndicator = self.transform:Find("Anchor/Selections/ScrollViewIndicator"):GetComponent(typeof(UIScrollViewIndicator))

    self.m_SoundButton = self.transform:Find("Anchor/Portrait/SoundButton").gameObject
    self.m_SoundSettingButton = self.transform:Find("Anchor/Portrait/SoundSettingButton").gameObject

    self.dynamicOptionCount = 0
    self.onSelectDelegate = nil
    self.m_CustomDialogActionBg = "common_btn_01_blue_highlight"
    self.m_DefaultDialogActionBg = "common_btn_01_blue"
    self.m_HideSelf = false
    self.m_HideOther = false

    self.selectionsTemplate:SetActive(false)

    UIEventListener.Get(self.descriptionLabel.gameObject).onClick  = DelegateFactory.VoidDelegate(function(go)
        CUIManager.CloseUI(CUIResources.DialogWnd)
    end)

    UIEventListener.Get(self.m_SoundButton).onClick  = DelegateFactory.VoidDelegate(function(go)
        self:OnSoundButtonClick()
    end)

    UIEventListener.Get(self.m_SoundSettingButton).onClick  = DelegateFactory.VoidDelegate(function(go)
        self:OnSoundSettingButtonClick(go, false)
    end)
    self.GmWatchedDialog =  {}
    local arr = Kefuzhuanqu_Setting.GetData().GmWatchedDialog
    for i=0,arr.Length-1 do
        self.GmWatchedDialog[arr[i]] = 1
    end
    if LuaHuLuWa2022Mgr.s_HuLuWaSwitch then
        Gac2Gas.QueryHuluwaActivityAwardTimes()
    end
end

function CLuaDialogWnd:Init( )
    local title = nil
    local content = nil
    local portrait = nil
    local selections = {}--CreateFromClass(MakeGenericClass(List, SelectionData))
    local info = CConversationMgr.Inst.DialogInfo

    if nil ~= info.dynamicSelections then
        CommonDefs.ListIterate(info.dynamicSelections, DelegateFactory.Action_object(function (selection)
            local temp = g_LuaUtil:StrSplit(selection,"#")--CommonDefs.StringSplit_ArrayChar(selection, "#")
            if #temp == 3 then
                local flag = temp[0+1]
                local taskId = math.floor(tonumber(temp[1+1] or 0))
                local display = temp[2+1]
                local template = Task_Task.GetData(taskId)
                if display == "" or display == nil then
                    display = template.Display
                end
                --refs #54028 【任务】主线任务如果当前已接但未完成不可提交，则按钮也显示为蓝色
                local taskNotFinished = false
                if CClientMainPlayer.Inst ~= nil and CommonDefs.DictContains(CClientMainPlayer.Inst.TaskProp.CurrentTasks, typeof(UInt32), template.ID) then
                    taskNotFinished = true
                end

                local selectionData = {
                    taskId = taskId,
                    text = display,
                    icon = self:GetIconByFlag(flag, template),
                    btnBg = (template.DisplayNode ~= 1 or taskNotFinished) and "common_btn_01_blue" or "common_btn_01_yellow"
                }
                table.insert( selections, selectionData )
            elseif #temp == 1 then
                local selectionData = {
                    text = selection,
                    icon = nil,
                    btnBg = "common_btn_01_blue"
                }
                table.insert( selections, selectionData )
            end
        end))
    end

    self.dynamicOptionCount = #selections

    self.selections = selections

    local npc =  CClientObjectMgr.Inst:GetObject(info.m_NpcEngineId)
    if npc ~= nil then
        title = npc.Name
        portrait = npc.PortraitName
    else
        CUIManager.CloseUI(CUIResources.DialogWnd)
        return
    end
    if info.m_DialogId ~= 0 then
        local data = Dialog_Dialog.GetData(info.m_DialogId)
        if data ~= nil then
            content = data.Message
            do
                local i = 0
                while i < data.ChoiceName.Length do
                    local selectionData = {
                        text = data.ChoiceName[i],
                        icon = self:GetActionIcon(data.ChoiceActionName[i]),
                        btnBg = self:GetActionBg(data.ChoiceActionName[i])
                    }
                    if info.m_BgColorIdx ~= 0 then
                        selectionData.btnBg = self:GetDialogActionBg(info.m_BgColorIdx)
                    end
                    if data.ChoiceConfirmMessage ~= nil and not System.String.IsNullOrEmpty(data.ChoiceConfirmMessage[i]) then
                        selectionData.confirmMessage = data.ChoiceConfirmMessage[i]
                    end
                    table.insert( selections, selectionData )
                    i = i + 1
                end
            end
        end
    else
        local data = NPC_NPC.GetData(info.m_NpcId)
        if data ~= nil then
            content = data.Message
            portrait = data.Portrait
        end
    end

    self:Adjust(selections)

    if info.m_CustomContent ~= "" then
        content = info.m_CustomContent
    end

    local npc_data = NPC_NPC.GetData(info.m_NpcId)
    if npc_data ~= nil and npc_data.BabyAppearance == 1 then
        portrait = npc.PortraitName
    end

    self:LoadPortrait(portrait)

    --string title = "武大郎NPC";
    --string content = "[00ff00]UITableView[-]是在[b]iOS开发[/b]中，展示大量内容的首选。我个人认为的原有有一下几点：UITableView的展现形式是为移动设备专门设计过的。有较好的人机交互体验。从技术角度来说UITableView具有重用和延迟加载等特性。如果使用恰当。可以获得一个App流畅的用户体验。";
    --List<string> selections = new List<string>(new string[] { "买1个烧饼", "买2个烧饼", "买3个烧饼", "买4个烧饼", "买5个烧饼", "买6个烧饼", "买一袋子烧饼", "不买烧饼" });
    --TODO: 从数据源读取对话框内容
    self:InitDialog(title, content, selections, function (dynamic, index, confirmText)
        if not (confirmText==nil or confirmText=="") then
            MessageWndManager.ShowOKCancelMessage(confirmText, DelegateFactory.Action(function ()
                Gac2Gas.SelectDialogChoice(dynamic, index + 1)
                if self.GmWatchedDialog and self.GmWatchedDialog[info.m_DialogId] then
                    PreventPluginMgr:CheckActivate("dialogWnd", {info.m_DialogId,index + 1})
                end
            end), nil, nil, nil, false)
        else
            Gac2Gas.SelectDialogChoice(dynamic, index + 1)
            if self.GmWatchedDialog and self.GmWatchedDialog[info.m_DialogId] then
                PreventPluginMgr:CheckActivate("dialogWnd", {info.m_DialogId,index + 1})
            end
        end
    end)

    --语音播放
    local npcInfo = NPC_NPC.GetData(info.m_NpcId)
    self.m_SoundButton:SetActive(npcInfo and npcInfo.Sound and npcInfo.Sound~="")
    self.m_SoundSettingButton:SetActive(npcInfo and npcInfo.Sound and npcInfo.Sound~="")
    if npcInfo ~= nil then
        local curTime = Time.realtimeSinceStartup
        --两次播放的npcId不相同或者npcId相同但时间间隔大于3s才能播放语音
        if CLuaDialogWnd.m_LastNpcId ~= info.m_NpcId or (CLuaDialogWnd.m_LastNpcId == info.m_NpcId and CLuaDialogWnd.m_LastPlayTime + 3 < curTime) then
            SoundManager.Inst:StopDialogSound()
            if not (npcInfo.Sound==nil or npcInfo.Sound=="") then

                SoundManager.Inst:StartDialogSound(npcInfo.Sound)
                CLuaDialogWnd.m_LastNpcId = info.m_NpcId
                CLuaDialogWnd.m_LastPlayTime = curTime
            end
        end
    end
end

function CLuaDialogWnd:OnSoundButtonClick()
    local npcInfo = self.m_LastNpcId and  NPC_NPC.GetData(self.m_LastNpcId) or nil
    if npcInfo then
		SoundManager.Inst:StartDialogSoundAndEnableSoundSetting(npcInfo.Sound)
    end
end

function CLuaDialogWnd:OnSoundSettingButtonClick(go, isLeft)
    if not CUIManager.IsLoaded(CLuaUIResources.MiniVolumeSettingsWnd) then
        LuaMiniVolumeSettingsMgr:OpenWnd(go, go.transform.position, UIWidget.Pivot.Left)
    else
        CUIManager.CloseUI(CLuaUIResources.MiniVolumeSettingsWnd)
    end
end

--根据文字长度调整
function CLuaDialogWnd:Adjust(selections)
    local wide = false
    for i,v in ipairs(selections) do
        if v.text and string.len( v.text )>=11*3 then
            wide=true
        end
    end
    if wide then
        self.transform:Find("Anchor/Selections").gameObject:SetActive(false)
        self.transform:Find("Anchor/Selections2").gameObject:SetActive(true)
        self.selectionsRoot = self.transform:Find("Anchor/Selections2").gameObject
        self.selectionsScrollView = self.transform:Find("Anchor/Selections2/ScrollView"):GetComponent(typeof(UIScrollView))
        self.selectionsTable = self.transform:Find("Anchor/Selections2/ScrollView/Table"):GetComponent(typeof(UITable))
        self.selectionsBg = self.transform:Find("Anchor/Selections2/Background"):GetComponent(typeof(UISprite))
        self.selectionsTemplate = self.transform:Find("Anchor/Selections2/ScrollView/ItemTemplate").gameObject
        self.selectionsScrollViewIndicator = self.transform:Find("Anchor/Selections2/ScrollViewIndicator"):GetComponent(typeof(UIScrollViewIndicator))
        self.selectionsTemplate:SetActive(false)
    end
end

function CLuaDialogWnd:GetIconByFlag( flag, template)
    if flag == "f" then
        return (template.MainTask == 1) and Constants.NPCTaskIcon_Period_Green or Constants.NPCTaskIcon_Period_Blue
    elseif flag == "a" then
        return (template.MainTask == 1) and Constants.NPCTaskIcon_QuestionMark_Green or Constants.NPCTaskIcon_QuestionMark_Blue
    elseif flag == "b" then
        return (template.MainTask == 1) and Constants.NPCTaskIcon_QuestionMark_Green or Constants.NPCTaskIcon_QuestionMark_Blue
    elseif flag == "g" then
        return self:GetActionIcon("EnterGamePlay")
    elseif flag == "i" then
        return (template.MainTask == 1) and Constants.NPCTaskIcon_Period_Green or Constants.NPCTaskIcon_Period_Blue
    elseif flag == "n" then
        return (template.MainTask == 1) and Constants.NPCTaskIcon_Period_Green or Constants.NPCTaskIcon_Period_Blue
    elseif flag == "t" then
        return (template.MainTask == 1) and Constants.NPCTaskIcon_Period_Red or Constants.NPCTaskIcon_Period_Red
    end
    return ""
end
function CLuaDialogWnd:InitDialog( npcName, content, selections, onSelectDelegate)
    --设置标题
    self.nameLabel.text = npcName
    --设置对话内容
    local logics,speakContent= string.match(content, "<popup%s%s?([^>]*)>([^<]*)</popup>")

    if not speakContent then 
        speakContent = content
    end
    self.descriptionLabel.text = SafeStringFormat3("[c][%s]%s[-][/c]", NGUIText.EncodeColor24(self.descriptionLabel.color), CChatLinkMgr.TranslateToNGUIText(speakContent, false))
    self.descriptionScrollView:ResetPosition()
    --设置对话镜头
    if logics then
        local npcId1,npcId2,alpha,theta,phi,duration = string.match(logics, "toriccamera:(%d+),(%d+),([0-9%.]+),(-?[0-9%.]+),(-?[0-9%.]+),([0-9%.]+)")
        if npcId1 and npcId2 and alpha and theta and phi and duration then
            npcId1 = tonumber(npcId1)
            npcId2 = tonumber(npcId2)
            local noSway = false
            _,_,_,_,_,_,noSway = string.match(logics, "toriccamera:(%d+),(%d+),([0-9%.]+),(-?[0-9%.]+),(-?[0-9%.]+),([0-9%.]+),(%d+)")
            if noSway and noSway == "1" then
                noSway = true
            else
                noSway = false
            end
            local Input = import "UnityEngine.Input"
            
            RegisterTickOnce(function ()
                print("Input.GetMouseButton(0)", Input.GetMouseButton(0))
                LuaDialogCameraMgr.ProcessToricCamera(npcId1,npcId2,alpha,theta,phi,duration,noSway)
            end,  100)
        end

        if CClientMainPlayer.Inst then
            if string.find(logics,"hideself") then
                self.m_HideSelf = true
                CClientObjectRoot.Inst:SetLayerVisible(false,LayerDefine.MainPlayer)
            else
                if self.m_HideSelf then
                    CClientObjectRoot.Inst:SetLayerVisible(true,LayerDefine.MainPlayer)
                end
                self.m_HideSelf = false
            end
        end
        if string.find(logics,"hideother") then
            self.m_HideOther = true
            CClientObjectRoot.Inst:SetLayerVisible(false,LayerDefine.OtherPlayer)
        else
            if self.m_HideOther then
                CClientObjectRoot.Inst:SetLayerVisible(true,LayerDefine.OtherPlayer)
            end
            self.m_HideOther = false
        end
    end
    --设置选项内容
    Extensions.RemoveAllChildren(self.selectionsTable.transform)
    if #selections == 0 then
        self.selectionsRoot:SetActive(false)
        return
    end
    local itemBounds = nil--CreateFromClass(Bounds)
    self.m_MessageLookup = {}
    for i,selection in ipairs(selections) do
        local instance = NGUITools.AddChild(self.selectionsTable.gameObject, self.selectionsTemplate)
        instance:SetActive(true)

        local textLabel = instance.transform:Find("Label"):GetComponent(typeof(UILabel))
        local icon = instance.transform:Find("Icon"):GetComponent(typeof(UISprite))
        local bgSprite = instance.transform:Find("Background"):GetComponent(typeof(UISprite))

        textLabel.text = selection.text
        icon.spriteName = selection.icon
        bgSprite.spriteName = selection.btnBg
        self.m_MessageLookup[instance] = selection.confirmMessage

        UIEventListener.Get(instance).onClick = DelegateFactory.VoidDelegate(function(go)
            self:OnSelectionClick(go)
        end)
        if i == 1 then
            itemBounds = NGUIMath.CalculateRelativeWidgetBounds(instance.transform)
        end
    end

    self.selectionsTable:Reposition()

    local b = NGUIMath.CalculateRelativeWidgetBounds(self.selectionsTable.transform)

    self.selectionsBg.height = math.ceil(math.min((itemBounds.size.y
                                + self.selectionsTable.padding.y * 2) * 6, b.size.y))
                                + math.abs(self.selectionsScrollView.panel.topAnchor.absolute)
                                + math.abs(self.selectionsScrollView.panel.bottomAnchor.absolute) + 10
    self.selectionsScrollView.panel:ResetAndUpdateAnchors()
    self.selectionsScrollView:ResetPosition()
    self.selectionsScrollViewIndicator:Layout()
    --设置选择事件的代理
    self.onSelectDelegate = onSelectDelegate
end
function CLuaDialogWnd:OnSelectionClick( go)
    local textLabel = go.transform:Find("Label"):GetComponent(typeof(UILabel))
    CNPCShopInfoMgr.LastOpenShopName = textLabel.text

    local confirmText = self.m_MessageLookup[go]--item.confirmText

    local children = self.selectionsTable:GetChildList()
    do
        local i = 0
        while i < children.Count do
            if children[i]:Equals(go.transform) then
                local taskId = self.selections[i+1].taskId
                if self.onSelectDelegate ~= nil then
                    if i < self.dynamicOptionCount then
                        self.onSelectDelegate(true, i, confirmText)
                    else
                        self.onSelectDelegate(false, i - self.dynamicOptionCount, confirmText)
                    end
                end
                --L10.CLogMgr.Log("您选择了第" + i + "个选项，即:" + go.GetComponentInChildren<UILabel>().text);
                break
            end
            i = i + 1
        end
    end
end
function CLuaDialogWnd:Update()
    if Time.frameCount % 10 == 0 then
        if CClientMainPlayer.Inst == nil or  CClientMainPlayer.Inst.IsDie then
            CUIManager.CloseUI(CUIResources.DialogWnd)
        else
            local npc = CClientObjectMgr.Inst:GetObject(CConversationMgr.Inst.DialogInfo.m_NpcEngineId)
            if npc == nil then
                CUIManager.CloseUI(CUIResources.DialogWnd)
            end
        end
    end
end

function CLuaDialogWnd:GetActionIcon(actionName)
    -- local data = Action_Action.GetData(actionName)
    -- return data and data.DialogChoiceIcon or nil
    return nil
end

function CLuaDialogWnd:GetDialogActionBg( colorIdx)
    if colorIdx == 1 then
        return self.m_CustomDialogActionBg
    end
    return self.m_DefaultDialogActionBg
end
function CLuaDialogWnd:GetActionBg( actionName)
    -- local data = Action_Action.GetData(actionName)
    -- if data ~= nil and data.DialogChoiceBg > 0 then
    --     return self.m_CustomDialogActionBg
    -- end
    return self.m_DefaultDialogActionBg
end


function CLuaDialogWnd:OnDestroy()
    if CConversationMgr.Inst.DialogInfo  then
        Gac2Gas.EndConversation();
    end
    SoundManager.Inst:StopDialogSound()
    LuaDialogCameraMgr.EndDialogCamera()

    if self.m_HideSelf and CClientObjectRoot.Inst then
        CClientObjectRoot.Inst:SetLayerVisible(true,LayerDefine.MainPlayer)
    end
    if self.m_HideOther and CClientObjectRoot.Inst then
        CClientObjectRoot.Inst:SetLayerVisible(true,LayerDefine.OtherPlayer)
    end
end
function CLuaDialogWnd:LoadPortrait(portraitName)
    self.headPortrait:LoadPortrait(portraitName, false)
end


