local UISlider = import "UISlider"
local Extensions = import "Extensions"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local PathMode = import "DG.Tweening.PathMode"
local PathType = import "DG.Tweening.PathType"
local Ease = import "DG.Tweening.Ease"
local Vector4 = import "UnityEngine.Vector4"

LuaShiTuWenDaoView  = class()

RegistChildComponent(LuaShiTuWenDaoView, "m_Grid","Grid", UIGrid)
RegistChildComponent(LuaShiTuWenDaoView, "m_Template","Template", GameObject)
RegistChildComponent(LuaShiTuWenDaoView, "m_ScorllView1","ScorllView1", UIScrollView)
RegistChildComponent(LuaShiTuWenDaoView, "m_Slider","Slider", UISlider)
RegistChildComponent(LuaShiTuWenDaoView, "m_ProgressLabel","ProgressLabel", UILabel)
RegistChildComponent(LuaShiTuWenDaoView, "m_RadioBox","RadioBox", QnRadioBox)
RegistChildComponent(LuaShiTuWenDaoView, "m_TopLabel","TopLabel", UILabel)
RegistChildComponent(LuaShiTuWenDaoView, "m_Gift","Gift", GameObject)

RegistClassMember(LuaShiTuWenDaoView,"m_Plans")
RegistClassMember(LuaShiTuWenDaoView,"m_CanBeFilledItemIds")
RegistClassMember(LuaShiTuWenDaoView,"m_CurrentStage")
RegistClassMember(LuaShiTuWenDaoView,"m_Level")
RegistClassMember(LuaShiTuWenDaoView,"m_CurTudiPlayerId")

function LuaShiTuWenDaoView:Awake()
    self.m_Template:SetActive(false)
    self.m_RadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn,index)
        self:OnTabButtonSelected(btn, index)
    end)

    local wenDaoStageName = ShiTu_Setting.GetData().WenDaoStageName
    for i = 0,self.m_RadioBox.m_RadioButtons.Length - 1 do
        self.m_RadioBox.m_RadioButtons[i].Text = wenDaoStageName[i]
        self.m_RadioBox.m_RadioButtons[i].transform:Find("Lock/Label"):GetComponent(typeof(UILabel)).text = wenDaoStageName[i]
        if String.IsNullOrEmpty(wenDaoStageName[i]) then
            self.m_RadioBox.m_RadioButtons[i].gameObject:SetActive(false)
        end
        self.m_RadioBox.m_RadioButtons[i].transform:Find("Lock").gameObject:SetActive(false)
        self.m_RadioBox.m_RadioButtons[i].transform:Find("AlertSprite").gameObject:SetActive(false)
    end

    UIEventListener.Get(self.m_Gift.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnGiftButtonClick()
	end)
end

function LuaShiTuWenDaoView:OnEnable()
    g_ScriptEvent:AddListener("SendNewShiTuWenDaoInfo", self, "OnSendNewShiTuWenDaoInfo")
    g_ScriptEvent:AddListener("ShiTuTrainingHandbookWnd_ChooseTudiPlayerId", self, "OnChooseTudiPlayerId")
    self:OnChooseTudiPlayerId()
end

function LuaShiTuWenDaoView:OnDisable()
    g_ScriptEvent:RemoveListener("SendNewShiTuWenDaoInfo", self, "OnSendNewShiTuWenDaoInfo")
    g_ScriptEvent:RemoveListener("ShiTuTrainingHandbookWnd_ChooseTudiPlayerId", self, "OnChooseTudiPlayerId")
end

function LuaShiTuWenDaoView:OnChooseTudiPlayerId()
    local playerId = LuaShiTuTrainingHandbookMgr:GetPartnerPlayerId()
    if playerId == 0 then
        return
    end
    Gac2Gas.QueryShiTuCultivateInfo(LuaShiTuTrainingHandbookMgr.wndtype.TrainingPlan, playerId)
    self.m_TopLabel.text = LuaShiTuTrainingHandbookMgr.m_IsShowForTeacher and LocalString.GetString("弟子学业目标") or LocalString.GetString("我的学业目标")
end

function LuaShiTuWenDaoView:OnSendNewShiTuWenDaoInfo()
    local curStageData = ShiTuCultivate_CultivateNewStage.GetData(LuaShiTuTrainingHandbookMgr.m_WenDaoInfo.currentStage)
    self:OnTabButtonSelected(nil,self.m_CurrentStage - 1)
    local curStage = curStageData and curStageData.StageId or LuaShiTuTrainingHandbookMgr.m_WenDaoInfo.currentStage
    for i = 0,self.m_RadioBox.m_RadioButtons.Length - 1 do
        self.m_RadioBox.m_RadioButtons[i].transform:Find("Lock").gameObject:SetActive(i >= curStage)
        self.m_RadioBox.m_RadioButtons[i].m_Label.gameObject:SetActive(i < curStage)
        self.m_RadioBox.m_RadioButtons[i].transform:GetComponent(typeof(UITexture)).alpha = (i < curStage) and 1 or 0.6
    end
    if self.m_CurTudiPlayerId ~= LuaShiTuTrainingHandbookMgr.m_WenDaoInfo.otherPlayerId then
        self.m_CurrentStage = curStage
        self.m_RadioBox:ChangeTo(self.m_CurrentStage - 1, true)
        self.m_CurTudiPlayerId = LuaShiTuTrainingHandbookMgr.m_WenDaoInfo.otherPlayerId
    end
    if LuaShiTuTrainingHandbookMgr.m_WenDaoInfo.wendaoInfo then
        for i = 0,self.m_RadioBox.m_RadioButtons.Length - 1 do
            self.m_RadioBox.m_RadioButtons[i].transform:Find("AlertSprite").gameObject:SetActive(false)
            local stage = i + 1
            local stageInfo = LuaShiTuTrainingHandbookMgr.m_WenDaoInfo.wendaoInfo[stage]
            if stageInfo then 
                for cultivateId, taskInfo in pairs(stageInfo.cultivateTasks or {}) do
                    local progress = taskInfo[1] or 0
                    local rewarded = taskInfo[2] == true
                    local cultivate = ShiTuCultivate_CultivateNew.GetData(cultivateId)
                    local isFullProgress = progress >= cultivate.Num
                    local canGetReward = not rewarded and isFullProgress and stage <= curStage
                    if canGetReward then
                        self.m_RadioBox.m_RadioButtons[i].transform:Find("AlertSprite").gameObject:SetActive(true)
                    end
                end
            end
        end
    end
    self:InitGift()
end

function LuaShiTuWenDaoView:OnTabButtonSelected(btn, index)
    local currentStage = ShiTuCultivate_CultivateNewStage.GetData(LuaShiTuTrainingHandbookMgr.m_WenDaoInfo.currentStage)
    if btn and currentStage and currentStage.StageId < (index + 1) then
        g_MessageMgr:ShowMessage("ShiTuWenDaoView_TabButton_Locked")
        --self.m_RadioBox:ChangeTo(self.m_CurrentStage - 1, false)
    end
    self.m_CurrentStage = index + 1
    LuaShiTuTrainingHandbookMgr.m_WenDaoStage = self.m_CurrentStage
    if not LuaShiTuTrainingHandbookMgr.m_WenDaoInfo.wendaoInfo then
        return
    end
    local stageInfo = LuaShiTuTrainingHandbookMgr.m_WenDaoInfo.wendaoInfo[self.m_CurrentStage]
    local list = {}
    for cultivateId, taskInfo in pairs(stageInfo and stageInfo.cultivateTasks or {}) do
        local t = {}
        local progress = taskInfo[1] or 0
        local rewarded = taskInfo[2] == true
        t.cultivate = ShiTuCultivate_CultivateNew.GetData(cultivateId)
        t.progress = progress
        self.m_Level = t.cultivate.Level
        local studentRewards = {}
        if LuaShiTuTrainingHandbookMgr.m_IsShowForTeacher then
            table.insert(studentRewards, {scorekey = EnumPlayScoreKey.QYJF, num = t.cultivate.ShifuQingyi})
            if t.cultivate.ShifuItems then
                for j = 0, t.cultivate.ShifuItems.Length - 1, 2 do
                    local itemId, num = t.cultivate.ShifuItems[j], t.cultivate.ShifuItems[j + 1]
                    table.insert(studentRewards, {item = Item_Item.GetData(itemId), num = num})
                end
            end
        else
            table.insert(studentRewards, {scorekey = EnumPlayScoreKey.QYJF, num = t.cultivate.TudiQingyi})
            table.insert(studentRewards, {scorekey = EnumPlayScoreKey.GIFT, num = t.cultivate.TudiGiftLimit})
            if t.cultivate.TudiItems then
                for j = 0, t.cultivate.TudiItems.Length - 1, 2 do
                    local itemId, num = t.cultivate.TudiItems[j], t.cultivate.TudiItems[j + 1]
                    table.insert(studentRewards, {item = Item_Item.GetData(itemId), num = num})
                end
            end
        end
        t.studentRewards = studentRewards
        t.rewarded = rewarded
        table.insert(list,t)
    end
    table.sort(list,function (a,b)
        return a.cultivate.ID < b.cultivate.ID
    end)
    self:InitItems(list)
    self:InitGift()
end

function LuaShiTuWenDaoView:InitItems(data)
    Extensions.RemoveAllChildren(self.m_Grid.transform)
    local finishedNum = 0
    for i = 1, #data do
        local t = data[i]
        local go = NGUITools.AddChild(self.m_Grid.gameObject, self.m_Template.gameObject)
        go:SetActive(true)
        local conditionLabel = go.transform:Find("Center/ConditionLabel"):GetComponent(typeof(UILabel))
        local grid = go.transform:Find("Center/Grid"):GetComponent(typeof(UIGrid))
        local rewardTemplate = go.transform:Find("Center/RewardTemplate").gameObject
        local line = go.transform:Find("Center/ConditionLabel/Line"):GetComponent(typeof(UISprite))

        rewardTemplate.gameObject:SetActive(false)
        conditionLabel.text = SafeStringFormat3(t.cultivate.Description, math.floor(t.progress) )
        line.width =  conditionLabel.width
        line.gameObject:SetActive(t.progress >= t.cultivate.Num)
        if t.progress >= t.cultivate.Num then
            finishedNum = finishedNum + 1
        end
        conditionLabel.alpha = t.progress >= t.cultivate.Num and 0.5 or 1
        local isFullProgress = t.progress >= t.cultivate.Num
        local canGetReward = not t.rewarded and isFullProgress and self.m_CurrentStage <= LuaShiTuTrainingHandbookMgr.m_WenDaoInfo.currentStage

        for j = 1, #t.studentRewards do
           
            local rewardInfo = t.studentRewards[j]
            local item = rewardInfo.item
            if rewardInfo.num > 0 then
                local go = NGUITools.AddChild(grid.gameObject, rewardTemplate)
                go:SetActive(true)
    
                local qualitySprite = go.transform:Find("QualitySprite"):GetComponent(typeof(UISprite))
                local icon = go:GetComponent(typeof(CUITexture))
                local fx = go.transform:Find("Fx"):GetComponent(typeof(CUIFx))
                local got = go.transform:Find("Got")
                local received = go.transform:Find("Received")
                local label = go.transform:Find("NumLabel"):GetComponent(typeof(UILabel))
                got.gameObject:SetActive(false)
                received.gameObject:SetActive(false)
    
                qualitySprite.spriteName = ""
                if item then 
                    icon:LoadMaterial(item.Icon) 
                    qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(item.Quality)
                end
                if rewardInfo.scorekey then icon:LoadMaterial(LuaShiTuTrainingHandbookMgr:GetPlayScore(rewardInfo.scorekey).ScoreIcon) end
                label.text = rewardInfo.num
                label.gameObject:SetActive(rewardInfo.num > 1)
                         
                received.gameObject:SetActive(t.rewarded)
                if canGetReward then
                    fx.ScrollView = self.m_ScorllView1
                    fx:LoadFx("fx/ui/prefab/UI_kuang_yellow02.prefab")
                    local b = NGUIMath.CalculateRelativeWidgetBounds(go.transform)
                    local waypoints = CUICommonDef.GetWayPoints(Vector4(b.min.x, b.max.y, b.size.x, b.size.y), 1, false)
                    fx.transform.localPosition = waypoints[0]
                    LuaTweenUtils.DOLuaLocalPath(fx.transform, waypoints, 2, PathType.Linear, PathMode.Ignore, 2, - 1, Ease.Linear)
                end
    
                UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function()
                    if canGetReward then
                        local playerId = LuaShiTuTrainingHandbookMgr:GetPartnerPlayerId()
                        Gac2Gas.ShiTuWenDaoGetTaskReward(playerId,t.cultivate.StageId,t.cultivate.ID)
                        return
                    end
                    if item and item.ID then
                        CItemInfoMgr.ShowLinkItemTemplateInfo(item.ID, false, nil, CItemInfoMgr.AlignType.Default, 0, 0, 0, 0)
                    end
                end)
            end
        end
        grid:Reposition()
    end
    self.m_Grid:Reposition()
    self.m_ProgressLabel.text = finishedNum .."/" .. (#data)
    self.m_Slider.value = finishedNum / (#data)
end

function LuaShiTuWenDaoView:InitGift()
    local fx = self.m_Gift.transform:Find("Fx"):GetComponent(typeof(CUIFx))
    local got = self.m_Gift.transform:Find("GiftGot")
    local received = self.m_Gift.transform:Find("ReceivedGift")
    local notAddGift = self.m_Gift.transform:Find("NotAddGift")
    
    got.gameObject:SetActive(false)
    received.gameObject:SetActive(false)
    notAddGift.gameObject:SetActive(false)
    
    local stageInfo = LuaShiTuTrainingHandbookMgr.m_WenDaoInfo.wendaoInfo[self.m_CurrentStage]
    if stageInfo then
        local hasGift = stageInfo.hasGift
        local rewarded = stageInfo.rewarded
        notAddGift.gameObject:SetActive(not hasGift and not rewarded)
        got.gameObject:SetActive(rewarded)
    end
end

function LuaShiTuWenDaoView:OnGiftButtonClick()
    LuaShiTuTrainingHandbookMgr:ShowWenDaoRewardWnd(self.m_Level,self.m_CurrentStage)
end