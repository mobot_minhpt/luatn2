local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local CUIManager = import "L10.UI.CUIManager"
local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"

CLuaQMPKCreateZhanDuiWnd = class()
RegistClassMember(CLuaQMPKCreateZhanDuiWnd,"m_OKBtn")
RegistClassMember(CLuaQMPKCreateZhanDuiWnd,"m_NameInput")
RegistClassMember(CLuaQMPKCreateZhanDuiWnd,"m_SloganInput")

function CLuaQMPKCreateZhanDuiWnd:Awake()
    self.m_OKBtn = self.transform:Find("Anchor/OKButton").gameObject
    self.m_NameInput = self.transform:Find("Anchor/Name/NameInput"):GetComponent(typeof(UIInput))
    self.m_SloganInput = self.transform:Find("Anchor/Slogan/SloganInput"):GetComponent(typeof(UIInput))

    UIEventListener.Get(self.m_OKBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnOkBtnClicked(go) end)
end

function CLuaQMPKCreateZhanDuiWnd:OnEnable( )
    g_ScriptEvent:AddListener("CreateQmpkZhanDuiSuccess", self, "OnCreateQmpkZhanDuiSuccess")
end
function CLuaQMPKCreateZhanDuiWnd:OnDisable( )
    g_ScriptEvent:RemoveListener("CreateQmpkZhanDuiSuccess", self, "OnCreateQmpkZhanDuiSuccess")
end
function CLuaQMPKCreateZhanDuiWnd:OnCreateQmpkZhanDuiSuccess( )
    CUIManager.ShowUI(CLuaUIResources.QMPKSelfZhanDuiWnd)
    CUIManager.CloseUI(CLuaUIResources.QMPKCreateZhanDuiWnd)
end
function CLuaQMPKCreateZhanDuiWnd:OnOkBtnClicked( go) 
    if System.String.IsNullOrEmpty(self.m_NameInput.value) then
        g_MessageMgr:ShowMessage("QMPK_Please_Input_Zhandui_Name")
        return
    end
    local nameStr = string.gsub(self.m_NameInput.value, " ", "")
    if CommonDefs.StringLength(nameStr) == 0 then
        g_MessageMgr:ShowMessage("QMPK_Please_Input_Zhandui_Name")
        return
    end
    if CUICommonDef.GetStrByteLength(nameStr) > CQuanMinPKMgr.m_MaxNameLength then
        g_MessageMgr:ShowMessage("QMPK_Zhandui_Name_Too_Long")
        return
    end
    if CUICommonDef.GetStrByteLength(self.m_SloganInput.value) > CQuanMinPKMgr.m_MaxSloganLength then
        g_MessageMgr:ShowMessage("QMPK_Slogan_Too_Long")
        return
    end
    if not CWordFilterMgr.Inst:CheckName(nameStr) or not CWordFilterMgr.Inst:CheckName(self.m_SloganInput.value) then
        g_MessageMgr:ShowMessage("QMPK_Name_Slogan_Violation")
        return
    end
    Gac2Gas.RequestCreateQmpkZhanDui(nameStr, self.m_SloganInput.value)
end

return CLuaQMPKCreateZhanDuiWnd
