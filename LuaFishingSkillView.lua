local QnCheckBox = import "L10.UI.QnCheckBox"

local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local UITable = import "UITable"
local GameObject = import "UnityEngine.GameObject"
local CUITexture = import "L10.UI.CUITexture"
local QnNewSlider = import "L10.UI.QnNewSlider"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CChatLinkMgr = import "CChatLinkMgr"
local StringBuilder = import "System.Text.StringBuilder"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local Color = import "UnityEngine.Color"
local CTrackMgr = import "L10.Game.CTrackMgr"
local Constants = import "L10.Game.Constants"

LuaFishingSkillView = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaFishingSkillView, "SkillGradeLabel", "SkillGradeLabel", UILabel)
RegistChildComponent(LuaFishingSkillView, "Table", "Table", UITable)
RegistChildComponent(LuaFishingSkillView, "Preview", "Preview", CUITexture)
RegistChildComponent(LuaFishingSkillView, "DescItem", "DescItem", GameObject)
RegistChildComponent(LuaFishingSkillView, "NextTitleItem", "NextTitleItem", GameObject)
RegistChildComponent(LuaFishingSkillView, "ChangeAppearanceBtn", "ChangeAppearanceBtn", GameObject)
RegistChildComponent(LuaFishingSkillView, "TipBtn", "TipBtn", GameObject)
RegistChildComponent(LuaFishingSkillView, "SkillDetailView", "SkillDetailView", GameObject)
RegistChildComponent(LuaFishingSkillView, "ShuLianDuSlider", "ShuLianDuSlider", QnNewSlider)
RegistChildComponent(LuaFishingSkillView, "SkillUpGradeBtn", "SkillUpGradeBtn", GameObject)
RegistChildComponent(LuaFishingSkillView, "ShuLianDuLabel", "ShuLianDuLabel", UILabel)
RegistChildComponent(LuaFishingSkillView, "GradLabel", "GradLabel", UILabel)
RegistChildComponent(LuaFishingSkillView, "MoneyCtrl", "MoneyCtrl", CCurentMoneyCtrl)
RegistChildComponent(LuaFishingSkillView, "ExpCostView", "ExpCostView", GameObject)
RegistChildComponent(LuaFishingSkillView, "Checkbox", "Checkbox", QnCheckBox)
RegistChildComponent(LuaFishingSkillView, "ExpLabel1", "ExpLabel1", GameObject)
RegistChildComponent(LuaFishingSkillView, "ExpLabel2", "ExpLabel2", GameObject)
RegistChildComponent(LuaFishingSkillView, "UpgadeView", "UpgadeView", GameObject)
RegistChildComponent(LuaFishingSkillView, "NeedExpLabel", "NeedExpLabel", UILabel)
RegistChildComponent(LuaFishingSkillView, "OwnExpLabel", "OwnExpLabel", UILabel)
RegistChildComponent(LuaFishingSkillView, "EmptyView", "EmptyView", GameObject)
RegistChildComponent(LuaFishingSkillView, "GoHomeBtn", "GoHomeBtn", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaFishingSkillView, "m_FishingSkillLevel")
RegistClassMember(LuaFishingSkillView, "m_SpeicailSkillDesc")
RegistClassMember(LuaFishingSkillView, "m_IsUseExp")
RegistClassMember(LuaFishingSkillView, "m_NextLevel")
RegistClassMember(LuaFishingSkillView, "m_NeedMoney")
RegistClassMember(LuaFishingSkillView, "m_NeedExp")
RegistClassMember(LuaFishingSkillView, "m_NeedShuliandu")
RegistClassMember(LuaFishingSkillView, "m_NeedPlayerExp")
RegistClassMember(LuaFishingSkillView, "m_SpecialExp")

function LuaFishingSkillView:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
	UIEventListener.Get(self.SkillUpGradeBtn).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSkillUpGradeBtnClick()
	end)
	UIEventListener.Get(self.TipBtn).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipBtnClick()
	end)
	UIEventListener.Get(self.GoHomeBtn).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnGoHomeBtnClick()
	end)

	self.m_SpeicailSkillDesc = {}
	self.m_IsUseExp = false
	self.Checkbox:SetSelected(false,true)
	self.ExpCostView:SetActive(false)
end

function LuaFishingSkillView:Init()
	if not CClientMainPlayer.Inst then return end
	self.MoneyCtrl:SetType(EnumMoneyType.YinPiao, EnumPlayScoreKey.NONE, true)
	self:FormatSpecialSkillDesc()
	self.DescItem:SetActive(false)
	self.NextTitleItem:SetActive(false)
	Extensions.RemoveAllChildren(self.Table.transform)
	local cls = HouseFish_Setting.GetData().FishSkillId
	self.m_CurSkillId = CClientMainPlayer.Inst.SkillProp:GetSkillIdWithDeltaByCls(cls, CClientMainPlayer.Inst.Level)
	self.m_FishingSkillLevel = CLuaDesignMgr.Skill_AllSkills_GetLevel(self.m_CurSkillId)
	self.GradLabel.text = self.m_FishingSkillLevel
	self.SkillGradeLabel.text = self.m_FishingSkillLevel
	if self.m_FishingSkillLevel == 0 then
		self.SkillDetailView:SetActive(false)
		self.EmptyView:SetActive(true)
        self.EmptyView.transform:Find("HintLabel"):GetComponent(typeof(UILabel)).text = LocalString.GetString("请前往杭州彦归朝处领取引导任务")
		return
	else
		self.SkillDetailView:SetActive(true)
		self.EmptyView:SetActive(false)
	end

	local builder = NewStringBuilderWraper(StringBuilder)
	local skillWithDelta = Skill_AllSkills.GetData(self.m_CurSkillId)
	if skillWithDelta then 
		local default = skillWithDelta:Get_Display_Internal(false,false,0)
		default = CChatLinkMgr.TranslateToNGUIText(default, false)

		local item = NGUITools.AddChild(self.Table.gameObject,self.DescItem)
		item:SetActive(true)
		local label = item:GetComponent(typeof(UILabel))
		label.enabled = true
		local lv = HouseFish_SkillUpgrade.GetData(self.m_FishingSkillLevel).Lv
		default = SafeStringFormat3("%s%s[c][00C932]%d[-][/c]",default,LocalString.GetString("需求等级"),lv)
		label.text = default

		if self.m_SpeicailSkillDesc[self.m_FishingSkillLevel] then
			local item2 = NGUITools.AddChild(self.Table.gameObject,self.DescItem)
			item2:SetActive(true)
			local label2 = item2:GetComponent(typeof(UILabel))
			label2.text = SafeStringFormat3(LocalString.GetString("[c][FFFE91]%s[-][/c]"), self.m_SpeicailSkillDesc[self.m_FishingSkillLevel])
			label2.transform:Find("TitleLabel"):GetComponent(typeof(UILabel)).text = nil
		end
	end
	local nextLevel = self.m_FishingSkillLevel + 1
	
	self.m_NextLevel = nextLevel
	if nextLevel <= HouseFish_Setting.GetData().MaxFishingSkillLevel then
		local hasNextTitle = false
		if self.m_SpeicailSkillDesc[tonumber(nextLevel)] then
			local item3 = NGUITools.AddChild(self.Table.gameObject,self.DescItem)
			item3:SetActive(true)
			local label3 = item3:GetComponent(typeof(UILabel))
			label3.text = SafeStringFormat3(LocalString.GetString("[c][FFFE91]%s[-][/c]"), self.m_SpeicailSkillDesc[tonumber(nextLevel)])
			label3.transform:Find("TitleLabel"):GetComponent(typeof(UILabel)).text = LocalString.GetString("下一级效果")
			hasNextTitle = true
		end
		local nextData = Skill_AllSkills.GetData(CLuaDesignMgr.Skill_AllSkills_GetSkillId(cls, nextLevel))
		local nextDesc = nextData:Get_Display_Internal(false,false,0)
		nextDesc = CChatLinkMgr.TranslateToNGUIText(nextDesc, false)
		local lv = HouseFish_SkillUpgrade.GetData(nextLevel).Lv
		nextDesc = SafeStringFormat3("%s%s[c][00C932]%d[-][/c]",nextDesc,LocalString.GetString("需求等级"),lv)
		local item4 = NGUITools.AddChild(self.Table.gameObject,self.DescItem)
		item4:SetActive(true)
		local label4 = item4:GetComponent(typeof(UILabel))
		label4.text = nextDesc
		local titleLabel = label4.transform:Find("TitleLabel"):GetComponent(typeof(UILabel))
		if hasNextTitle then
			titleLabel.text = nil
		else
			titleLabel.text = LocalString.GetString("下一级效果")
		end
		

		local upgrade = HouseFish_SkillUpgrade.GetData(nextLevel)
		if upgrade then
			maxScore = upgrade.Consume
		end
	end
	
	self.Table:Reposition()
	--self.m_IsUseExp = false
	self:RefreshCost()
	-- self.Checkbox:SetSelected(false,true)
	-- self.ExpCostView:SetActive(false)
	self.Checkbox.OnValueChanged = DelegateFactory.Action_bool(function (value)
		self.m_IsUseExp = value
		self.ExpCostView:SetActive(value)
        self:RefreshCost()
	end)
end

function LuaFishingSkillView:RefreshCost()
	if self.m_NextLevel <= HouseFish_Setting.GetData().MaxFishingSkillLevel then
		--money
		local data = HouseFish_SkillUpgrade.GetData(self.m_NextLevel)
		local maxScore = data.Consume
		if self.m_IsUseExp then
			self.m_NeedMoney = data.Silver
			self:InitCost(data.Silver)
			self.NeedExpLabel.text = data.Exp
			self.m_NeedExp = data.Exp
			self.ExpLabel1:SetActive(false)
			self.ExpLabel2:SetActive(true)
		else
			self.m_NeedMoney = data.OnlySilver
			maxScore = data.OnlyConsume
			self.ExpLabel1:SetActive(true)
			self.ExpLabel2:SetActive(false)
			self:InitCost(data.OnlySilver)
			self.NeedExpLabel.text = data.Exp
			self.m_NeedExp = data.Exp
		end
				
		--shuliandu
		self.m_NeedShuliandu = maxScore
		local curScore = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp:GetScoreByKey(EnumPlayScoreKey.HouseFishing) or 0
		self.ShuLianDuLabel.text = SafeStringFormat3(LocalString.GetString("%d/%d"),curScore,maxScore)
		local percent = maxScore == 0 and 0 or curScore / maxScore
		self.ShuLianDuSlider.m_Value = percent
		--exp 优先消耗专用经验
		local skillPrivateExp = CClientMainPlayer.Inst.SkillProp.SkillPrivateExp
        local exp = CClientMainPlayer.Inst.PlayProp.Exp
		self.m_SpecialExp = skillPrivateExp
		if skillPrivateExp >= self.m_NeedExp then
			self.OwnExpLabel.text = skillPrivateExp
			self:InitExpIcon(true)
			self.m_NeedPlayerExp = false
			self.m_SpecialExp = self.m_NeedExp
		elseif skillPrivateExp > 0 then
			self.OwnExpLabel.text = skillPrivateExp
			self:InitExpIcon(true)
			if skillPrivateExp + exp >= self.m_NeedExp then
				self.OwnExpLabel.color = Color.white
			else
				self.OwnExpLabel.color = Color.red
			end
			self.m_NeedPlayerExp = true
		else			
			self.OwnExpLabel.text = exp
			if skillPrivateExp + exp >= self.m_NeedExp then
				self.OwnExpLabel.color = Color.white
			else
				self.OwnExpLabel.color = Color.red
			end
			self:InitExpIcon(false)
			self.m_NeedPlayerExp = true
			self.m_SpecialExp = 0
		end	
	else
		self.UpgadeView:SetActive(false)
	end
end

function LuaFishingSkillView:OnEnable()
	g_ScriptEvent:AddListener("MainPlayerSkillPropUpdate", self, "OnMainPlayerSkillPropUpdate")
	g_ScriptEvent:AddListener("MainPlayerPlayPropUpdate",self,"OnMainPlayerPlayPropUpdate")
end

function LuaFishingSkillView:OnDisable()
	g_ScriptEvent:RemoveListener("MainPlayerSkillPropUpdate", self, "OnMainPlayerSkillPropUpdate")
	g_ScriptEvent:RemoveListener("MainPlayerPlayPropUpdate",self,"OnMainPlayerPlayPropUpdate")
end


function LuaFishingSkillView:FormatSpecialSkillDesc()
	self.m_SpeicailSkillDesc = {}
	local specialList = HouseFish_Setting.GetData().FishSkillSpecialText
	for i=0,specialList.Length-1,2 do 
		local lv = tonumber(specialList[i])
		local desc = specialList[i+1]
		desc = CChatLinkMgr.TranslateToNGUIText(desc, false)
		self.m_SpeicailSkillDesc[lv] = desc
	end
end

function LuaFishingSkillView:InitCost(cost)
	self.MoneyCtrl:SetCost(cost)
end

--@region UIEvent

function LuaFishingSkillView:OnSkillUpGradeBtnClick()
	if not self.m_NeedShuliandu or not self.m_NeedMoney then
		return
	end
	local yinpiao = CClientMainPlayer.Inst.FreeSilver
    local yinliang = CClientMainPlayer.Inst.Silver
	if yinpiao >= self.m_NeedMoney then
		yinpiao = self.m_NeedMoney
		yinliang = 0
	else
		yinpiao = yinpiao
		yinliang = self.m_NeedMoney - yinpiao
	end

	if self.m_IsUseExp then
		local msg = g_MessageMgr:FormatMessage("Upgrade_SeaFishingSkill_With_Exp",self.m_NeedShuliandu,yinpiao,yinliang,self.m_SpecialExp,self.m_NeedExp-self.m_SpecialExp)
		MessageWndManager.ShowOKCancelMessage(msg, 
        DelegateFactory.Action(function ()      
            Gac2Gas.RequestUpgradeHouseFishingSkill(2)--1 只扣银票 2 经验
        end),
        nil,
        LocalString.GetString("确定"), LocalString.GetString("取消"), false)
	else
		MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("Upgrade_SeaFishingSkill_Without_Exp",self.m_NeedShuliandu,yinpiao,yinliang), 
        DelegateFactory.Action(function ()                
            Gac2Gas.RequestUpgradeHouseFishingSkill(1)--1 只扣银票 2 经验
        end),
        nil,
        LocalString.GetString("确定"), LocalString.GetString("取消"), false)
	end
end

function LuaFishingSkillView:OnTipBtnClick()
	g_MessageMgr:ShowMessage("SeaFishing_Skill_Rule")
end

function LuaFishingSkillView:OnGoHomeBtnClick()
	CTrackMgr.Inst:FindNPC(20001409, Constants.HangZhouID, 0, 0, nil, nil)
end

function LuaFishingSkillView:InitExpIcon(isSpecial)
	local icon1 = self.OwnExpLabel.transform.parent:Find("Icon"):GetComponent(typeof(UISprite))
	local icon2 = self.NeedExpLabel.transform.parent:Find("Icon"):GetComponent(typeof(UISprite))
	icon1.spriteName = isSpecial and "common_special_exp" or "common_player_exp"
	icon2.spriteName = isSpecial and "common_special_exp" or "common_player_exp"
end

--@endregion UIEvent

function LuaFishingSkillView:OnMainPlayerSkillPropUpdate()
    self:Init() 
end

function LuaFishingSkillView:OnMainPlayerPlayPropUpdate()
    self:Init() 
end
