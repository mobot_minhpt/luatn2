-- Auto Generated!!
local Buff_Buff = import "L10.Game.Buff_Buff"
local CGuildQueenStateWnd = import "L10.UI.CGuildQueenStateWnd"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local Vector3 = import "UnityEngine.Vector3"
CGuildQueenStateWnd.m_Init_CS2LuaHook = function (this) 

    local data = Buff_Buff.GetData(this.s_BangHuaBossId)
    if data ~= nil then
        this.m_BuffIcon:LoadMaterial(data.Icon)
    end
    this.m_BuffLevel.text = System.String.Format("x{0}", 1)
    this.m_HpBar:UpdateValue(100, 100, 100)
    this.contentNode:SetActive(false)
end
CGuildQueenStateWnd.m_OnEnable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.expandButton).onClick = DelegateFactory.VoidDelegate(function (p) 
        this.expandButton.transform.localEulerAngles = Vector3(0, 0, 0)
        CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
    end)
    EventManager.AddListener(EnumEventType.HideTopAndRightTipWnd, MakeDelegateFromCSFunction(this.OnHideTopAndRightTipWnd, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.OnUpdateBangHuaThirdStageBossStatus, MakeDelegateFromCSFunction(this.OnUpdateBangHuaThirdStageBossStatus, MakeGenericClass(Action4, UInt32, UInt32, UInt32, UInt32), this))
end
CGuildQueenStateWnd.m_OnDisable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.expandButton).onClick = nil
    EventManager.RemoveListener(EnumEventType.HideTopAndRightTipWnd, MakeDelegateFromCSFunction(this.OnHideTopAndRightTipWnd, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.OnUpdateBangHuaThirdStageBossStatus, MakeDelegateFromCSFunction(this.OnUpdateBangHuaThirdStageBossStatus, MakeGenericClass(Action4, UInt32, UInt32, UInt32, UInt32), this))
    --这个界面不显示的话，那么tip界面肯定也不显示
    if CUIManager.IsLoaded(CUIResources.TopAndRightTipWnd) then
        CUIManager.CloseUI(CUIResources.TopAndRightTipWnd)
    end
end
CGuildQueenStateWnd.m_OnUpdateBangHuaThirdStageBossStatus_CS2LuaHook = function (this, buffid, bufflv, hp, hpFull) 
    local data = Buff_Buff.GetData(buffid)
    if data ~= nil then
        this.m_BuffIcon:LoadMaterial(data.Icon)
    end
    this.m_BuffLevel.text = System.String.Format("x{0}", bufflv)
    this.contentNode:SetActive(true)
    this.m_HpLabel.text = tostring(hp)
    this.m_HpBar:UpdateValue(hp, hpFull, hpFull)
end
