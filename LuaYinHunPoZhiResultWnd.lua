local DelegateFactory = import "DelegateFactory"
local ShareBoxMgr     = import "L10.UI.ShareBoxMgr"
local EShareType      = import "L10.UI.EShareType"
local CLoginMgr       = import "L10.Game.CLoginMgr"

LuaYinHunPoZhiResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaYinHunPoZhiResultWnd, "score")
RegistClassMember(LuaYinHunPoZhiResultWnd, "newRecord")
RegistClassMember(LuaYinHunPoZhiResultWnd, "maxScore")
RegistClassMember(LuaYinHunPoZhiResultWnd, "playerInfo")
RegistClassMember(LuaYinHunPoZhiResultWnd, "rankButton")
RegistClassMember(LuaYinHunPoZhiResultWnd, "shareButton")
RegistClassMember(LuaYinHunPoZhiResultWnd, "closeButton")
RegistClassMember(LuaYinHunPoZhiResultWnd, "fx")

function LuaYinHunPoZhiResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self:InitUIComponents()
    self:InitEventListener()
end

function LuaYinHunPoZhiResultWnd:InitUIComponents()
    local anchor = self.transform:Find("Anchor")
    self.score = anchor:Find("Score"):GetComponent(typeof(UILabel))
    self.newRecord = anchor:Find("Score/NewRecord").gameObject
    self.maxScore = anchor:Find("MaxScore"):GetComponent(typeof(UILabel))
    self.playerInfo = anchor:Find("PlayerInfoNode")
    self.rankButton = anchor:Find("RankButton").gameObject
    self.shareButton = anchor:Find("ShareButton").gameObject
    self.closeButton = self.transform:Find("Bg/CloseButton").gameObject
    self.fx = self.transform:Find("Bg/Fx"):GetComponent(typeof(CUIFx))
end

function LuaYinHunPoZhiResultWnd:InitEventListener()
    UIEventListener.Get(self.rankButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRankButtonClick()
	end)

    UIEventListener.Get(self.shareButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareButtonClick()
	end)
    self.shareButton.gameObject:SetActive(not CommonDefs.IS_VN_CLIENT)
end


function LuaYinHunPoZhiResultWnd:Init()
    self:InitPlayerInfo()
    self.score.text = LuaQingMing2022Mgr.YHPZInfo.score
    self.maxScore.text = LuaQingMing2022Mgr.YHPZInfo.todayMaxScore
    self.newRecord:SetActive(LuaQingMing2022Mgr.YHPZInfo.newrecord)
    self.fx:LoadFx(g_UIFxPaths.QingMingPVEFxPath)
end

function LuaYinHunPoZhiResultWnd:InitPlayerInfo()
    self.playerInfo:Find("Icon"):GetComponent(typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(CClientMainPlayer.Inst.Class, CClientMainPlayer.Inst.Gender, -1), false)
    self.playerInfo:Find("Icon/Lv"):GetComponent(typeof(UILabel)).text = tostring(CClientMainPlayer.Inst.Level)
    self.playerInfo:Find("ID"):GetComponent(typeof(UILabel)).text = tostring(CClientMainPlayer.Inst.Id)
    self.playerInfo:Find("Name"):GetComponent(typeof(UILabel)).text = CClientMainPlayer.Inst.BasicProp.Name

    local myGameServer = CLoginMgr.Inst:GetSelectedGameServer()
    self.playerInfo:Find("Server"):GetComponent(typeof(UILabel)).text = myGameServer.name
end

-- 分享时不显示几个按键
function LuaYinHunPoZhiResultWnd:SetButtonActive(active)
    self.shareButton:SetActive(active)
    self.rankButton:SetActive(active)
    self.closeButton:SetActive(active)
end

--@region UIEvent

function LuaYinHunPoZhiResultWnd:OnRankButtonClick()
    LuaPlayerRankMgr:ShowPlayerRankWnd(QingMing2022_PVESetting.GetData().RankId, nil, nil, nil, LocalString.GetString("今日最高分数"),
        g_MessageMgr:FormatMessage("YINHUNPOZHI_RANK_TIP"))
end

function LuaYinHunPoZhiResultWnd:OnShareButtonClick()
    self:SetButtonActive(false)

    CUICommonDef.CaptureScreen("screenshot", false, false, nil,
        DelegateFactory.Action_string_bytes(function(fullPath, jpgBytes)
            self:SetButtonActive(true)
            ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, fullPath)
        end), false)
end

--@endregion UIEvent

