local GameObject = import "UnityEngine.GameObject"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local Color = import "UnityEngine.Color"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local CUITexture = import "L10.UI.CUITexture"
local Extensions = import "Extensions"
local Vector3 = import "UnityEngine.Vector3"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local EShareType = import "L10.UI.EShareType"
local CChatHelper = import "L10.UI.CChatHelper"
local EChatPanel = import "L10.Game.EChatPanel"
local WWW = import "UnityEngine.WWW"

LuaHuiLiuMainWnd = class()
RegistChildComponent(LuaHuiLiuMainWnd,"closeBtn", "CloseButton", GameObject)
RegistChildComponent(LuaHuiLiuMainWnd,"tipLabel", UILabel)
RegistChildComponent(LuaHuiLiuMainWnd,"tip1Label", UILabel)
RegistChildComponent(LuaHuiLiuMainWnd,"tip2Label", UILabel)
RegistChildComponent(LuaHuiLiuMainWnd,"templateNode", GameObject)
RegistChildComponent(LuaHuiLiuMainWnd,"fNode", GameObject)
RegistChildComponent(LuaHuiLiuMainWnd,"rightBtn", GameObject)
RegistChildComponent(LuaHuiLiuMainWnd,"leftBtn", GameObject)
RegistChildComponent(LuaHuiLiuMainWnd,"shareBtn", GameObject)
RegistChildComponent(LuaHuiLiuMainWnd,"titleLabel","TitleLabel", UILabel)

RegistClassMember(LuaHuiLiuMainWnd, "nowIndex")

function LuaHuiLiuMainWnd:Close()
	CUIManager.CloseUI(CLuaUIResources.HuiLiuMainWnd)
end

function LuaHuiLiuMainWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateHuiLiuInfo", self, "InitPlayerInfo")
end

function LuaHuiLiuMainWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateHuiLiuInfo", self, "InitPlayerInfo")
end

function LuaHuiLiuMainWnd:Init()
	local onCloseClick = function(go)
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.closeBtn,DelegateFactory.Action_GameObject(onCloseClick),false)

	self.templateNode:SetActive(false)

	if LuaHuiLiuMgr.PlayerType == 1 then
		self.tip1Label.text = g_MessageMgr:FormatMessage('HuiGuiJieBanFriendTip1')
		self.tip2Label.text = g_MessageMgr:FormatMessage('HuiGuiJieBanFriendTip2')
		self.titleLabel.text = LocalString.GetString('召回好友')

		local onShareClick = function(go)
			local url, title, text,thumbnail = self:GetShareData()
			if url then
				ShareBoxMgr.OpenShareBox(EShareType.CommonUrl, 0, {url, title, text, thumbnail})
			end
		end
		CommonDefs.AddOnClickListener(self.shareBtn,DelegateFactory.Action_GameObject(onShareClick),false)
		self.shareBtn.transform:Find('text'):GetComponent(typeof(UILabel)).text = LocalString.GetString('邀请同游')
	elseif LuaHuiLiuMgr.PlayerType == 2 then
		self.tip1Label.text = g_MessageMgr:FormatMessage('HuiGuiJieBanFriendTip3')
		self.tip2Label.text = g_MessageMgr:FormatMessage('HuiGuiJieBanFriendTip4')
		self.titleLabel.text = LocalString.GetString('寻找同游')

		local onShareClick = function(go)
			local sendText = g_MessageMgr:FormatMessage('HuiGuiJieWorldText')
			CChatHelper.SendMsgWithFilterOption(EChatPanel.World, sendText, 0 ,true)
			g_MessageMgr:ShowMessage('HuiGuiJieWorldTextTip')
		end
		CommonDefs.AddOnClickListener(self.shareBtn,DelegateFactory.Action_GameObject(onShareClick),false)
		self.shareBtn.transform:Find('text'):GetComponent(typeof(UILabel)).text = LocalString.GetString('寻找同游')
	end

	self.nowIndex = 0

	local onLeftClick = function(go)
		self.nowIndex = self.nowIndex - 1
		if self.nowIndex < 0 then
			self.nowIndex = 0
		end
		self:InitPlayerInfo()
	end
	CommonDefs.AddOnClickListener(self.leftBtn,DelegateFactory.Action_GameObject(onLeftClick),false)
	local onRightClick = function(go)
		self.nowIndex = self.nowIndex + 1
		self:InitPlayerInfo()
	end
	CommonDefs.AddOnClickListener(self.rightBtn,DelegateFactory.Action_GameObject(onRightClick),false)

	self:InitPlayerInfo()
end

function LuaHuiLiuMainWnd:GetShareData()
	local setting = HuiGuiJieBan_Setting.GetData()
	local url = setting.ShareURL
	local title = setting.ShareTitle
	local text = setting.ShareText
	local image = setting.ShareImage

	return url,title,text,image
end

function LuaHuiLiuMainWnd:InitPlayerInfo()
	local itemWidth = 260
	Extensions.RemoveAllChildren(self.fNode.transform)
	self.rightBtn:SetActive(false)
	self.leftBtn:SetActive(false)

	if LuaHuiLiuMgr.PlayerInfoTable and #LuaHuiLiuMgr.PlayerInfoTable > 0 then
		self.tipLabel.gameObject:SetActive(false)

		if LuaHuiLiuMgr.PlayerType == 1 then
			for i,v in ipairs(LuaHuiLiuMgr.PlayerInfoTable) do
				if i > self.nowIndex * 4 and i <= self.nowIndex * 4 + 4 then
					local node = NGUITools.AddChild(self.fNode,self.templateNode)
					node:SetActive(true)
					node.transform.localPosition = Vector3((i-self.nowIndex*4-1) * itemWidth,0,0)
					self:InitInviteFriendNode(node,v)
				end
			end
		elseif LuaHuiLiuMgr.PlayerType == 2 then
			for i,v in ipairs(LuaHuiLiuMgr.PlayerInfoTable) do
				if i > self.nowIndex * 4 and i <= self.nowIndex * 4 + 4 then
					local node = NGUITools.AddChild(self.fNode,self.templateNode)
					node:SetActive(true)
					node.transform.localPosition = Vector3((i-self.nowIndex*4-1) * itemWidth,0,0)
					self:InitCallFriendNode(node,v)
				end
			end
		end

		if self.nowIndex > 0 then
			self.leftBtn:SetActive(true)
		else
			self.leftBtn:SetActive(false)
		end

		if self.nowIndex * 4 + 4 >= #LuaHuiLiuMgr.PlayerInfoTable then
			self.rightBtn:SetActive(false)
		else
			self.rightBtn:SetActive(true)
		end
	else
		self.tipLabel.gameObject:SetActive(true)
		if LuaHuiLiuMgr.PlayerType == 1 then
			self.tipLabel.text = g_MessageMgr:FormatMessage('HuiGuiJieBanFriendEmptyTip1')
		elseif LuaHuiLiuMgr.PlayerType == 2 then
			self.tipLabel.text = g_MessageMgr:FormatMessage('HuiGuiJieBanFriendEmptyTip2')
		end
	end

end

function LuaHuiLiuMainWnd:InitCallFriendNode(node,data)
	node.transform:Find('name'):GetComponent(typeof(UILabel)).text = data[2]
	local level = data[4]
	if data[5] > 0 then
		level = data[5]
	end
	node.transform:Find('lv'):GetComponent(typeof(UILabel)).text = 'lv.'.. level
	node.transform:Find('icon'):GetComponent(typeof(CUITexture)):LoadPortrait(CUICommonDef.GetPortraitName(data[3],data[6]),true)

	if data[5] <= 0 then
		node.transform:Find('lv'):GetComponent(typeof(UILabel)).color = Color.white
	end

	local shareBtn = node.transform:Find('btn').gameObject
	local tipNode = node.transform:Find('tipText').gameObject

  local huiLiuData = CClientMainPlayer.Inst.PlayProp.HuiGuiJieBanData
	if CommonDefs.DictContains(huiLiuData.RepliedFriends, typeof(UInt64), data[1]) then
		shareBtn:SetActive(false)
		tipNode:SetActive(true)
		tipNode:GetComponent(typeof(UILabel)).text = LocalString.GetString('已告知')
	else
		shareBtn:SetActive(true)
		tipNode:SetActive(false)
		shareBtn.transform:Find('text'):GetComponent(typeof(UILabel)).text = LocalString.GetString('邀请同游')
		local onSClick = function(go)
			Gac2Gas.RequestNotifyInviter(data[1])
		end
		CommonDefs.AddOnClickListener(shareBtn,DelegateFactory.Action_GameObject(onSClick),false)
	end

end

function LuaHuiLiuMainWnd:InitInviteFriendNode(node,data)
	node.transform:Find('name'):GetComponent(typeof(UILabel)).text = data[2]
	local level = data[4]
	if data[5] > 0 then
		level = data[5]
	end
	node.transform:Find('lv'):GetComponent(typeof(UILabel)).text = 'lv.'.. level
	node.transform:Find('icon'):GetComponent(typeof(CUITexture)):LoadPortrait(CUICommonDef.GetPortraitName(data[3],data[6]),true)

	if data[5] <= 0 then
		node.transform:Find('lv'):GetComponent(typeof(UILabel)).color = Color.white
	end

	local shareBtn = node.transform:Find('btn').gameObject
	local tipNode = node.transform:Find('tipText').gameObject

  local huiLiuData = CClientMainPlayer.Inst.PlayProp.HuiGuiJieBanData
	if CommonDefs.DictContains(huiLiuData.InvitedFriends, typeof(UInt64), data[1]) then
		shareBtn:SetActive(false)
		tipNode:SetActive(true)
		tipNode:GetComponent(typeof(UILabel)).text = LocalString.GetString('等待回复')
	else
		shareBtn:SetActive(true)
		tipNode:SetActive(false)
		shareBtn.transform:Find('text'):GetComponent(typeof(UILabel)).text = LocalString.GetString('召 回')
		local onSClick = function(go)
			Gac2Gas.RequestInviteFriend(data[1])
		end
		CommonDefs.AddOnClickListener(shareBtn,DelegateFactory.Action_GameObject(onSClick),false)
	end

end

return LuaHuiLiuMainWnd
