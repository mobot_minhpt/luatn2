-- Auto Generated!!
local CQMPKTitleTemplate = import "L10.UI.CQMPKTitleTemplate"
local Extensions = import "Extensions"
CQMPKTitleTemplate.m_Expand_CS2LuaHook = function (this, expand) 
    if expand then
        Extensions.SetLocalRotationZ(this.m_Arrow.transform, this.m_InitRotationZ + 180)
    else
        Extensions.SetLocalRotationZ(this.m_Arrow.transform, this.m_InitRotationZ)
    end
end
