require("common/common_include")

local CCommonRuleMgr = import "L10.UI.CCommonRuleMgr"
local CNationalDayMgr = import "L10.UI.CNationalDayMgr"

EnumGQJCPlayerStatus = {
    ePrepare = 1,
    eAlive = 2,
    eDie = 3,
    eEnd = 4
}

CLuaGuoQingJiaoChangMgr = {}

CLuaGuoQingJiaoChangMgr.m_SituationInfo = {}
CLuaGuoQingJiaoChangMgr.m_MainPlayerForce = -1

CLuaGuoQingJiaoChangMgr.m_RewardCandidates ={}
CLuaGuoQingJiaoChangMgr.m_RewardSelections = {}

-- state相关
CLuaGuoQingJiaoChangMgr.m_RemainTime = 0
CLuaGuoQingJiaoChangMgr.m_RemainTeam = 0
CLuaGuoQingJiaoChangMgr.m_BossState = 0

CLuaGuoQingJiaoChangMgr.m_CellInfos = nil
CLuaGuoQingJiaoChangMgr.m_MapCells = nil    -- cellId, LuaGQJCStateCell
CLuaGuoQingJiaoChangMgr.m_PlayerPosInfo = nil

CLuaGuoQingJiaoChangMgr.m_PosInfos = nil --playerId, cellId
CLuaGuoQingJiaoChangMgr.m_PlayerPosTable = nil --playerId, posGameObject

function CLuaGuoQingJiaoChangMgr.ShowCommonRule(isSignUp)
    local bSigned = isSignUp == 1
    local msg = g_MessageMgr:FormatMessage("GQJC_Rule")
    local onSignup = function ()
        Gac2Gas.RequestSignUpGQJC()
    end
    local onCancel = function ()
        Gac2Gas.RequestCancelSignUpGQJC()
    end
    CCommonRuleMgr.Instance:ShowCommonRuleWnd(LocalString.GetString("神机演武"), msg, nil, DelegateFactory.Action(onSignup), 
        DelegateFactory.Action(onCancel), bSigned, LocalString.GetString("开始匹配"), LocalString.GetString("取消匹配"), LocalString.GetString("正在匹配中"))
end

-- 报名(匹配)状态更新
function Gas2Gac.RequestSignUpGQJCResult(bSucc)
    CCommonRuleMgr.Instance:UpdateCommonRuleSign(bSucc)
end


-- 战况
function Gas2Gac.GuoQingJiaoChangSyncForcePlayerInfo(playInfoListU)
    local infoList = MsgPackImpl.unpack(playInfoListU)
    if not infoList then return end

    CLuaGuoQingJiaoChangMgr.m_SituationInfo = {}

    for i = 0, infoList.Count - 1, 1 do

        local info = infoList[i]
        if info.Count >= 11 then
            local playerId = info[0]
            local force = info[1]
            local class = info[2]
            local gender = info[3]
            local name = info[4]
            local grade = info[5]
            local isFeiSheng = info[6]
            local status = info[7]
            local killNum = info[8]
            local reliveNum = info[9]
            local clearNum = info[10]

            if playerId == CClientMainPlayer.Inst.Id then
                CLuaGuoQingJiaoChangMgr.m_MainPlayerForce = force
            end

            table.insert(CLuaGuoQingJiaoChangMgr.m_SituationInfo, {
                playerId = playerId,
                force = force,
                class = class,
                gender = gender,
                name = name,
                grade = grade,
                isFeiSheng = isFeiSheng,
                status = status,
                killNum = killNum,
                reliveNum = reliveNum,
                clearNum = clearNum,
            })
        end
    end

    if CUIManager.IsLoaded("GQJCSituationWnd") then
        g_ScriptEvent:BroadcastInLua("UpdateGQJCSituation", CLuaGuoQingJiaoChangMgr.m_SituationInfo)
    else
        CUIManager.ShowUI("GQJCSituationWnd")
    end
end

-- ShrinkState 0, 1, 2 NoShrink, Shrinking, Shrinked
-- BossState 0, 1, 2 NoActive, Activating, Actived
function Gas2Gac.GuoQingJiaoChangSyncMapInfo(remainTime, remainTeam, bossState, cellStateListU)

    CLuaGuoQingJiaoChangMgr.m_RemainTime = remainTime
    CLuaGuoQingJiaoChangMgr.m_RemainTeam = remainTeam
    CLuaGuoQingJiaoChangMgr.m_BossState = bossState
    CLuaGuoQingJiaoChangMgr.m_CellInfos = {}

    local cellInfo = MsgPackImpl.unpack(cellStateListU)
    if not cellInfo then return end

    for i = 0, cellInfo.Count - 1, 3 do
        local cellId = cellInfo[i]
        local cleared = cellInfo[i + 1]
        local shrinkState = cellInfo[i + 2]
        table.insert(CLuaGuoQingJiaoChangMgr.m_CellInfos, {
            cellId = cellId, 
            cleared = cleared, -- 1代表已经通关
            shrinkState = shrinkState,
            })
    end

    g_ScriptEvent:BroadcastInLua("UpdateGQJCState", CLuaGuoQingJiaoChangMgr.m_RemainTime, CLuaGuoQingJiaoChangMgr.m_RemainTeam,
            CLuaGuoQingJiaoChangMgr.m_BossState, CLuaGuoQingJiaoChangMgr.m_CellInfos)

end

function Gas2Gac.GuoQingJiaoChangSyncTeamPos(posListU)
    local posList = MsgPackImpl.unpack(posListU)
    if not posList then return end

    local playerPosInfos = {}
    -- 包括自己的
    for i = 0, posList.Count - 1, 2 do
        local playerId = posList[i]
        local cellId = posList[i + 1]
        table.insert(playerPosInfos, {
            playerId = playerId,
            cellId = cellId,
            })
    end
    g_ScriptEvent:BroadcastInLua("UpdateGQJCPosition", playerPosInfos)
end

-- 通关奖励
function Gas2Gac.GuoQingJiaoChangOpenSelectTemp(curSkillListU, curDrug, candidateTempListU)
    CLuaGuoQingJiaoChangMgr.m_RewardCandidates = {}
    CLuaGuoQingJiaoChangMgr.m_RewardSelections = {}

    local curSkillList = MsgPackImpl.unpack(curSkillListU)
    if not curSkillList then return end

    -- 已选
    for i = 0, curSkillList.Count - 1 do
        table.insert(CLuaGuoQingJiaoChangMgr.m_RewardSelections, curSkillList[i])
    end
    table.insert(CLuaGuoQingJiaoChangMgr.m_RewardSelections, curDrug)

    -- 待选
    local cdtTempList = MsgPackImpl.unpack(candidateTempListU)
    if not cdtTempList then return end
    for i = 0, cdtTempList.Count - 1 do
        table.insert(CLuaGuoQingJiaoChangMgr.m_RewardCandidates, cdtTempList[i])
    end

    if CUIManager.IsLoaded("GQJCSituationWnd") then
        g_ScriptEvent:BroadcastInLua("UpdateGQJCReward", CLuaGuoQingJiaoChangMgr.m_RewardCandidates, CLuaGuoQingJiaoChangMgr.m_RewardSelections)
    else
        CUIManager.ShowUI("GQJCChooseRewardWnd")
    end

   
end


g_UpdateAppearanceInPlayCallBack = {}
function Gas2Gac.UpdateAppearanceInPlay(playName, bEnter)
    if g_UpdateAppearanceInPlayCallBack[playName] then
        g_UpdateAppearanceInPlayCallBack[playName](bEnter)
    end
end

g_UpdateAppearanceInPlayCallBack["GuoQingJiaoChang"] = function(bEnter)
    -- todo
    CNationalDayMgr.Inst:UpdatGQJCPlayerShowIcon(bEnter)
end
