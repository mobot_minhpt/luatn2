local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CScene = import "L10.Game.CScene"
local PlayerPrefs = import "UnityEngine.PlayerPrefs"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaLiuYi2022Mgr = class()
LuaLiuYi2022Mgr.teamInfo = nil
LuaLiuYi2022Mgr.resultInfo = nil

function LuaLiuYi2022Mgr:IsInGameplay()
    if CScene.MainScene then
        local gamePlayId = CScene.MainScene.GamePlayDesignId
        if gamePlayId == DuanWu_Setting.GetData().BubbleGameplayId then
            return true
        end
    end
    return false
end

function LuaLiuYi2022Mgr:ShowResultWnd(info)
    self.resultInfo = info
    CUIManager.ShowUI(CLuaUIResources.LiuYi2022PopoResultWnd)
end