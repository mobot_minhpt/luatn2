-- Auto Generated!!
local CItemChosenWnd = import "L10.UI.CItemChosenWnd"
local CUICommonDef = import "L10.UI.CUICommonDef"
local DelegateFactory = import "DelegateFactory"
local EnumQualityType = import "L10.Game.EnumQualityType"
local Gac2Gas = import "L10.Game.Gac2Gas"
local Item_Item = import "L10.Game.Item_Item"
local ItemChosenMgr = import "L10.UI.ItemChosenMgr"
local MessageWndManager = import "L10.UI.MessageWndManager"
CItemChosenWnd.m_Init_CS2LuaHook = function (this) 
    this.iconTexture_Left:Clear()
    this.qualitySprite_Left.spriteName = CUICommonDef.GetItemCellBorder(EnumQualityType.White)
    this.amountLabel_Left.text = ""
    this.bindSprite_Left.gameObject:SetActive(false)

    local itemLeft = Item_Item.GetData(ItemChosenMgr.Inst.LeftItem.TemplateId)
    this.iconTexture_Left:LoadMaterial(itemLeft.Icon)
    this.qualitySprite_Left.spriteName = CUICommonDef.GetItemCellBorder(itemLeft, nil, false)
    this.nameLabel_Left.text = itemLeft.Name

    this.iconTexture_Right:Clear()
    this.qualitySprite_Right.spriteName = CUICommonDef.GetItemCellBorder(EnumQualityType.White)
    this.amountLabel_Right.text = ""
    this.bindSprite_Right.gameObject:SetActive(false)

    local itemRight = Item_Item.GetData(ItemChosenMgr.Inst.RightItem.TemplateId)
    this.iconTexture_Right:LoadMaterial(itemRight.Icon)
    this.qualitySprite_Right.spriteName = CUICommonDef.GetItemCellBorder(itemRight, nil, false)
    this.nameLabel_Right.text = itemRight.Name
end
CItemChosenWnd.m_OnLeftItemClick_CS2LuaHook = function (this, go) 
    local message = g_MessageMgr:FormatMessage("CHOOSE_LEFT_ITEM_COMFIRM")
    MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function () 
        Gac2Gas.PlayerSubmitTaskWithSelectedItem(ItemChosenMgr.Inst.TaskId, ItemChosenMgr.Inst.NpcId, ItemChosenMgr.Inst.LeftItem.TemplateId)
        this:Close()
    end), nil, nil, nil, false)
end
CItemChosenWnd.m_OnRightItemClick_CS2LuaHook = function (this, go) 
    local message = g_MessageMgr:FormatMessage("CHOOSE_RIGHT_ITEM_COMFIRM")
    MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function () 
        Gac2Gas.PlayerSubmitTaskWithSelectedItem(ItemChosenMgr.Inst.TaskId, ItemChosenMgr.Inst.NpcId, ItemChosenMgr.Inst.RightItem.TemplateId)
        this:Close()
    end), nil, nil, nil, false)
end



