local DelegateFactory  = import "DelegateFactory"
local NGUIText = import "NGUIText"
local UIGrid = import "UIGrid"
local EPropStatus = import "L10.Game.EPropStatus"
local CStatusMgr = import "L10.Game.CStatusMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"

LuaXueJingKuangHuanStateWnd = class()

RegistClassMember(LuaXueJingKuangHuanStateWnd, "m_ExpandButton")
RegistClassMember(LuaXueJingKuangHuanStateWnd, "m_Grid")
RegistClassMember(LuaXueJingKuangHuanStateWnd, "m_Template")
RegistClassMember(LuaXueJingKuangHuanStateWnd, "m_NormalColor")
RegistClassMember(LuaXueJingKuangHuanStateWnd, "m_MyselfColor")
RegistClassMember(LuaXueJingKuangHuanStateWnd, "m_CachedPos")

function LuaXueJingKuangHuanStateWnd:Awake()
    self.m_ExpandButton = self.transform:Find("Anchor/Tip/ExpandButton").gameObject
    self.m_Grid = self.transform:Find("Anchor/Tip/BG/Grid"):GetComponent(typeof(UIGrid))
    self.m_Template = self.transform:Find("Anchor/Tip/BG/Template").gameObject

    self.m_Template:SetActive(false)
    UIEventListener.Get(self.m_ExpandButton).onClick = DelegateFactory.VoidDelegate(function() self:OnExpandButtonClick() end)
    self.m_NormalColor = NGUIText.ParseColor24("FFFFFF", 0)
    self.m_MyselfColor = NGUIText.ParseColor24("00FF60", 0)
    self.m_CachedPos = {x=0,z=0}
end

function LuaXueJingKuangHuanStateWnd:Init()
    self:UpdateData(LuaHanJiaMgr.m_XueJingKuangHuanPlayInfo or {}) --为避免断线重连时序问题，这里进行数据缓存
end

function LuaXueJingKuangHuanStateWnd:OnDestroy()
    LuaHanJiaMgr.m_XueJingKuangHuanPlayInfo = {} --关闭窗口清除数据
end

function LuaXueJingKuangHuanStateWnd:UpdateData(playInfoTbl)
    local n = self.m_Grid.transform.childCount
    for i=0,n-1 do
        self.m_Grid.transform:GetChild(i).gameObject:SetActive(false)
    end
    
    local index = 0
    for __, info in pairs(playInfoTbl) do
        local child = nil
        if index<n then
            child = self.m_Grid.transform:GetChild(index).gameObject
        else
            child = CUICommonDef.AddChild(self.m_Grid.gameObject, self.m_Template)
        end
        index = index + 1
        child:SetActive(true)
        local rankIcon = child.transform:Find("RankIcon"):GetComponent(typeof(UISprite))
        local rankLabel = child.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
        local nameLabel = child.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
        local valueLabel = child.transform:Find("ValueLabel"):GetComponent(typeof(UILabel))
        if info.rank<4 then
            rankIcon.spriteName = info.rank==1 and g_sprites.Common_Huangguan_01 or (info.rank==2 and g_sprites.Common_Huangguan_02 or g_sprites.Common_Huangguan_03)
            rankLabel.text = ""
        else
            rankIcon.spriteName = ""
            rankLabel.text = tostring(info.rank)
        end
        nameLabel.text = info.playerName
        valueLabel.text = tostring(info.length)
        rankLabel.color = info.isMe and self.m_MyselfColor or self.m_NormalColor
        nameLabel.color = info.isMe and self.m_MyselfColor or self.m_NormalColor
        valueLabel.color = info.isMe and self.m_MyselfColor or self.m_NormalColor
    end
    self.m_Grid:Reposition()
end

function LuaXueJingKuangHuanStateWnd:OnEnable()
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:AddListener("PlayerDragJoyStick", self, "OnPlayerDragJoyStick")
    g_ScriptEvent:AddListener("PlayerDragJoyStickComplete", self, "OnPlayerDragJoyStickComplete")
    g_ScriptEvent:AddListener("OnSyncXueJingKuangHuanPlayInfo", self, "OnSyncXueJingKuangHuanPlayInfo")
end

function LuaXueJingKuangHuanStateWnd:OnDisable()
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:RemoveListener("PlayerDragJoyStick", self, "OnPlayerDragJoyStick")
    g_ScriptEvent:RemoveListener("PlayerDragJoyStickComplete", self, "OnPlayerDragJoyStickComplete")
    g_ScriptEvent:RemoveListener("OnSyncXueJingKuangHuanPlayInfo", self, "OnSyncXueJingKuangHuanPlayInfo")
end

function LuaXueJingKuangHuanStateWnd:OnExpandButtonClick()
    self.m_ExpandButton.transform.localEulerAngles = Vector3(0, 0, 0)
    CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end

function LuaXueJingKuangHuanStateWnd:OnHideTopAndRightTipWnd()
    self.m_ExpandButton.transform.localEulerAngles = Vector3(0, 0, 180)
end

function LuaXueJingKuangHuanStateWnd:IsInXueJingKuangHuanMoving()
    return CStatusMgr.Inst:GetStatus(CClientMainPlayer.Inst, EPropStatus.GetId("XueJingKuangHuanMoving")) == 1
end

function LuaXueJingKuangHuanStateWnd:OnPlayerDragJoyStick(args)
    if self:IsInXueJingKuangHuanMoving() then
        local dir = args[0]
        if  math.abs(self.m_CachedPos.x - dir.x) > 0.0001 or math.abs(self.m_CachedPos.z - dir.z) > 0.0001 then
            self.m_CachedPos.x = dir.x
            self.m_CachedPos.z = dir.z
            LuaHanJiaMgr:ChangeDirInXueJingKuangHuanMoving(self.m_CachedPos.x, self.m_CachedPos.z)
        end
        
    end
end

function LuaXueJingKuangHuanStateWnd:OnPlayerDragJoyStickComplete()
    if self:IsInXueJingKuangHuanMoving() then
        self.m_CachedPos.x = 0
        self.m_CachedPos.z = 0
        LuaHanJiaMgr:ChangeDirInXueJingKuangHuanMoving(self.m_CachedPos.x, self.m_CachedPos.z)
    end
end

function LuaXueJingKuangHuanStateWnd:OnSyncXueJingKuangHuanPlayInfo(playInfoTbl)
    self:UpdateData(playInfoTbl)
end
