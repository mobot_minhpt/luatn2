local Animation=import "UnityEngine.Animation"
local GameObject = import "UnityEngine.GameObject"
local Ease = import "DG.Tweening.Ease"

LuaHaiZhanPickWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaHaiZhanPickWnd, "Slider", "Slider", UISlider)
RegistChildComponent(LuaHaiZhanPickWnd, "Target", "Target", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaHaiZhanPickWnd, "m_Tick")

function LuaHaiZhanPickWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.Target.transform.localPosition = Vector3(-250,0,0)
    local tweener = LuaTweenUtils.TweenPositionX(self.Target.transform,250,1.2)
    LuaTweenUtils.SetLoops(tweener,-1,true)
    LuaTweenUtils.SetEase(tweener,Ease.InOutSine)
end

function LuaHaiZhanPickWnd:Init()
    local v = 0.2+math.random()/1.67--0.2~0.8之间
    self.Slider.value = v

    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick = false
    end
    self.transform:Find("Bottom/Slider/Success_Fx").gameObject:SetActive(false)
    self.transform:Find("Bottom/Slider/Fail_Fx").gameObject:SetActive(false)

end

--@region UIEvent

--@endregion UIEvent

function LuaHaiZhanPickWnd:OnEnable()
    g_ScriptEvent:AddListener("HaiZhanPickMat", self, "OnHaiZhanPickMat")
end
function LuaHaiZhanPickWnd:OnDisable()
    g_ScriptEvent:RemoveListener("HaiZhanPickMat", self, "OnHaiZhanPickMat")
end
function LuaHaiZhanPickWnd:OnHaiZhanPickMat()
    --延迟关闭界面
    self.m_Tick = RegisterTickOnce(function()
        CUIManager.CloseUI(CLuaUIResources.HaiZhanPickWnd)
        self.m_Tick = nil
    end,500)

    local x = self.Target.transform.localPosition.x
    x = (x+250)/500

    local v = self.Slider.value
    if x>v-0.13 and x<v+0.13 then
        --success
        Gac2Gas.RequestNavalSailorFishing(true)

        local go = self.transform:Find("Bottom/Slider/Success_Fx").gameObject
        go:SetActive(true)
        go:GetComponent(typeof(Animation)):Play("haizhanpickwnd_qte")
    else
        -- Gac2Gas.RequestNavalSailorFishing(false)
        LuaHaiZhanPickResultWnd.s_Info = nil
        CUIManager.ShowUI(CLuaUIResources.HaiZhanPickResultWnd)

        local go = self.transform:Find("Bottom/Slider/Fail_Fx").gameObject
        go:SetActive(true)
        go:GetComponent(typeof(Animation)):Play("haizhanpickwnd_qte")
    end

    LuaTweenUtils.DOKill(self.Target.transform,false)

end