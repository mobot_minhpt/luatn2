local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnRadioBox = import "L10.UI.QnRadioBox"

LuaGuildTerritorialWarsDrawColorWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaGuildTerritorialWarsDrawColorWnd, "RadioBox", "RadioBox", QnRadioBox)

--@endregion RegistChildComponent end

function LuaGuildTerritorialWarsDrawColorWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaGuildTerritorialWarsDrawColorWnd:Init()
    self.RadioBox:ChangeTo(LuaGuildTerritorialWarsMgr.m_DrawColorIndex - 1, false)
    for i = 1, self.RadioBox.m_RadioButtons.Length do
        local btn = self.RadioBox.m_RadioButtons[i - 1]
        btn.transform:Find("Highlighted").gameObject:SetActive(i == LuaGuildTerritorialWarsMgr.m_DrawColorIndex)
    end
    self.RadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn,index)
        self:OnRadioBoxSelect(btn,index)
    end)
end

--@region UIEvent
function LuaGuildTerritorialWarsDrawColorWnd:OnRadioBoxSelect(btn,index)
    LuaGuildTerritorialWarsMgr.m_DrawColorIndex = index + 1
    for i = 1, self.RadioBox.m_RadioButtons.Length do
        local btn = self.RadioBox.m_RadioButtons[i - 1]
        btn.transform:Find("Highlighted").gameObject:SetActive(i == LuaGuildTerritorialWarsMgr.m_DrawColorIndex)
    end
    CUIManager.CloseUI(CLuaUIResources.GuildTerritorialWarsDrawColorWnd)
    g_ScriptEvent:BroadcastInLua("OnGuildTerritorialWarsDrawColorSelect")
    local data = GuildTerritoryWar_Setting.GetData().CommandToolBrushColor[LuaGuildTerritorialWarsMgr.m_DrawColorIndex - 1]
    g_MessageMgr:ShowMessage("GuildTerritorialWarsDraw_ChangeColor", data[0])
end
--@endregion UIEvent

