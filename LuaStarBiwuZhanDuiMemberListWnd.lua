local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local Profession = import "L10.Game.Profession"

CLuaStarBiwuZhanDuiMemberListWnd = class()
CLuaStarBiwuZhanDuiMemberListWnd.Path = "ui/starbiwu/LuaStarBiwuZhanDuiMemberListWnd"
RegistClassMember(CLuaStarBiwuZhanDuiMemberListWnd,"LeftArrow")
RegistClassMember(CLuaStarBiwuZhanDuiMemberListWnd,"RightArrow")
RegistClassMember(CLuaStarBiwuZhanDuiMemberListWnd,"RequestButton")
RegistClassMember(CLuaStarBiwuZhanDuiMemberListWnd,"MemberItemPrefab")
RegistClassMember(CLuaStarBiwuZhanDuiMemberListWnd,"MemberRoot")
RegistClassMember(CLuaStarBiwuZhanDuiMemberListWnd,"SloganLabel")
RegistClassMember(CLuaStarBiwuZhanDuiMemberListWnd,"RequestLabel")
RegistClassMember(CLuaStarBiwuZhanDuiMemberListWnd,"NameLabel")
RegistClassMember(CLuaStarBiwuZhanDuiMemberListWnd,"m_RecruitInfoBtn")
RegistClassMember(CLuaStarBiwuZhanDuiMemberListWnd,"memberList")
RegistClassMember(CLuaStarBiwuZhanDuiMemberListWnd,"m_HasApplied")

function CLuaStarBiwuZhanDuiMemberListWnd:Awake()
    self.LeftArrow = self.transform:Find("ShowArea/LeftArrow").gameObject
    self.RightArrow = self.transform:Find("ShowArea/RightArrow").gameObject
    self.RequestButton = self.transform:Find("ShowArea/RequestButton").gameObject
    self.MemberItemPrefab = self.transform:Find("ShowArea/MemberInfoItem").gameObject
    self.MemberRoot = self.transform:Find("ShowArea/MemberRoot")
    self.SloganLabel = self.transform:Find("ShowArea/TopArea/SloganLabel"):GetComponent(typeof(UILabel))
    self.RequestLabel = self.transform:Find("ShowArea/RequestButton/Label"):GetComponent(typeof(UILabel))
    self.NameLabel = self.transform:Find("Wnd_Bg_Secondary_1/TitleLabel"):GetComponent(typeof(UILabel))
    self.m_RecruitInfoBtn = self.transform:Find("ShowArea/RecruitButton").gameObject
    self.memberList = {}
    self.m_HasApplied = false
end

-- Auto Generated!!
function CLuaStarBiwuZhanDuiMemberListWnd:RequestJoinStarBiwuZhanDuiSuccess(zhanduiId)
    if zhanduiId ~= CLuaStarBiwuMgr.m_ZhanduiTable[CLuaStarBiwuMgr.m_CurrentZhanDuiIndex].m_Id then
        return
    end
    self.RequestLabel.text = LocalString.GetString("取消申请")
    self.m_HasApplied = true
    CLuaStarBiwuMgr.m_ZhanduiTable[CLuaStarBiwuMgr.m_CurrentZhanDuiIndex].m_HasApplied = 1
end
function CLuaStarBiwuZhanDuiMemberListWnd:Init( )
    self:ShowZhanDui(CLuaStarBiwuMgr.m_CurrentZhanDuiIndex)
    if self.MemberRoot.childCount == 0 then
        self:initMembers()
    end
    if CLuaStarBiwuMgr.m_HasZhanDui then
        self.RequestButton:SetActive(false)
    end
end
function CLuaStarBiwuZhanDuiMemberListWnd:ShowZhanDui( index)
    if CLuaStarBiwuMgr.m_ShowZhanDuiMemberType == 0 then
        if index <= 0 or index > #CLuaStarBiwuMgr.m_ZhanduiTable then
            CUIManager.CloseUI(CLuaUIResources.StarBiwuZhanDuiMemberListWnd)
            return
        end
        Gac2Gas.QueryStarBiwuZhanDuiInfoByZhanDuiId(CLuaStarBiwuMgr.m_ZhanduiTable[index].m_Id)
        self.RightArrow:SetActive(index ~= #CLuaStarBiwuMgr.m_ZhanduiTable)
        self.LeftArrow:SetActive(index ~= 1)
    elseif CLuaStarBiwuMgr.m_ShowZhanDuiMemberType == 1 then
        Gac2Gas.QueryStarBiwuZhanDuiInfoByZhanDuiId(CLuaStarBiwuMgr.m_CurrentZhanduiId)
        self.LeftArrow:SetActive(false)
        self.RightArrow:SetActive(false)
    elseif CLuaStarBiwuMgr.m_ShowZhanDuiMemberType == 2 then
      if index <= 0 or index > #CLuaStarBiwuMgr.m_ScoreRankList then
          CUIManager.CloseUI(CLuaUIResources.StarBiwuZhanDuiMemberListWnd)
          return
      end
      Gac2Gas.QueryStarBiwuZhanDuiInfoByZhanDuiId(CLuaStarBiwuMgr.m_ScoreRankList[index].m_Id)
      self.RightArrow:SetActive(index ~= #CLuaStarBiwuMgr.m_ScoreRankList)
      self.LeftArrow:SetActive(index ~= 1)
    end
end
function CLuaStarBiwuZhanDuiMemberListWnd:initMembers( )
    self.memberList ={}
    self.MemberItemPrefab:SetActive(false)
    do
        local i = 0
        while i < StarBiWuShow_Setting.GetData().Max_ZhanDui_MemberNum do
            local go = NGUITools.AddChild(self.MemberRoot.gameObject, self.MemberItemPrefab)
            go:SetActive(false)
            local trans = go.transform
            self:SetItemSelected(trans,false)
            table.insert( self.memberList,trans )
            i = i + 1
        end
        self.MemberRoot:GetComponent(typeof(UIGrid)):Reposition()
    end
end
function CLuaStarBiwuZhanDuiMemberListWnd:Start( )
    UIEventListener.Get(self.RequestButton).onClick = DelegateFactory.VoidDelegate(function(go) self:onRequestClick(go) end)
    UIEventListener.Get(self.LeftArrow).onClick = DelegateFactory.VoidDelegate(function(go) self:onLeftArrowClick(go) end)
    UIEventListener.Get(self.RightArrow).onClick = DelegateFactory.VoidDelegate(function(go) self:onRightArrowClick(go) end)
    UIEventListener.Get(self.m_RecruitInfoBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnRecruitInfoBtnClicked(go) end)
end
function CLuaStarBiwuZhanDuiMemberListWnd:OnEnable( )
    g_ScriptEvent:AddListener("ReplyStarBiwuZhanDuiInfo", self, "OnReplyZhanDuiInfo")
    g_ScriptEvent:AddListener("RequestJoinStarBiwuZhanDuiSuccess", self, "RequestJoinStarBiwuZhanDuiSuccess")
end
function CLuaStarBiwuZhanDuiMemberListWnd:OnDisable( )
    g_ScriptEvent:RemoveListener("ReplyStarBiwuZhanDuiInfo", self, "OnReplyZhanDuiInfo")
    g_ScriptEvent:RemoveListener("RequestJoinStarBiwuZhanDuiSuccess", self, "RequestJoinStarBiwuZhanDuiSuccess")
end
function CLuaStarBiwuZhanDuiMemberListWnd:OnRecruitInfoBtnClicked( go)
    CUIManager.ShowUI(CLuaUIResources.QMPKRecruitWnd)
end
function CLuaStarBiwuZhanDuiMemberListWnd:OnReplyZhanDuiInfo( )
  if CLuaStarBiwuMgr.m_ShowZhanDuiMemberType == 0 then
    self.m_HasApplied = CLuaStarBiwuMgr.m_ZhanduiTable[CLuaStarBiwuMgr.m_CurrentZhanDuiIndex].m_HasApplied > 0
    if self.m_HasApplied then
        self.RequestLabel.text = LocalString.GetString("取消申请")
    else
        self.RequestLabel.text = LocalString.GetString("申请")
    end
  else
    self.RequestButton.gameObject:SetActive(false)
  end

    local maxCnt = StarBiWuShow_Setting.GetData().Max_ZhanDui_MemberNum
    do
        local i = 1
        while i <= maxCnt do
            self.memberList[i].gameObject:SetActive(true)
            if i <= #CLuaStarBiwuMgr.m_MemberInfoTable then
                self:InitMemberItem(self.memberList[i], CLuaStarBiwuMgr.m_MemberInfoTable[i], i - 1, nil)
                local id = CLuaStarBiwuMgr.m_MemberInfoTable[i].m_Id
                UIEventListener.Get(self.memberList[i].gameObject).onClick=DelegateFactory.VoidDelegate(function(go)
                    self:OnItemClicked(id, go)
                end)
            else
                self:InitMemberItem(self.memberList[i], nil, i - 1, nil)
            end
            i = i + 1
        end
    end
    self.SloganLabel.text = CLuaStarBiwuMgr.m_CurrentZhanduiSlogan
    self.NameLabel.text = CLuaStarBiwuMgr.m_CurrentZhanduiName
    if #CLuaStarBiwuMgr.m_MemberInfoTable >= maxCnt then
        self.RequestButton:SetActive(false)
    end
end
function CLuaStarBiwuZhanDuiMemberListWnd:onRequestClick( go)
    if not self.m_HasApplied then
        if CLuaStarBiwuMgr.m_ZhanduiTable and CLuaStarBiwuMgr.m_CurrentZhanDuiIndex and CLuaStarBiwuMgr.m_ZhanduiTable[CLuaStarBiwuMgr.m_CurrentZhanDuiIndex] then
            Gac2Gas.RequestJoinStarBiwuZhanDui(CLuaStarBiwuMgr.m_ZhanduiTable[CLuaStarBiwuMgr.m_CurrentZhanDuiIndex].m_Id, "")
        end
    else
        self.RequestLabel.text = LocalString.GetString("申请")
        Gac2Gas.RequestCancelJoinStarBiwuZhanDui(CLuaStarBiwuMgr.m_ZhanduiTable[CLuaStarBiwuMgr.m_CurrentZhanDuiIndex].m_Id)
        self.m_HasApplied = false
        CLuaStarBiwuMgr.m_ZhanduiTable[CLuaStarBiwuMgr.m_CurrentZhanDuiIndex].m_HasApplied = 0
        --EventManager.BroadcastInternalForLua(EnumEventType.QMPKRequestCancelJoinZhanDui, {CQuanMinPKMgr.Inst.m_ZhanDuiList[CQuanMinPKMgr.Inst.m_CurrentZhanDuiIndex].m_Id})
        g_ScriptEvent:BroadcastInLua("StarBiwuCancelJoinZhanDuiByPlayer", CLuaStarBiwuMgr.m_ZhanduiTable[CLuaStarBiwuMgr.m_CurrentZhanDuiIndex].m_Id)
    end
end

function CLuaStarBiwuZhanDuiMemberListWnd:OnItemClicked( id, go)
    if id <= 0 then
        return
    end
    CPlayerInfoMgr.ShowPlayerPopupMenu(math.floor(id), EnumPlayerInfoContext.QMPK, EChatPanel.Undefined, nil, nil, go.transform.position, CPlayerInfoMgr.AlignType.Right)
    do
        local i = 0 local cnt = #self.memberList
        while i < cnt do
            self:SetItemSelected(self.memberList[i+1],self.memberList[i+1].gameObject == go)
            i = i + 1
        end
    end
end

function CLuaStarBiwuZhanDuiMemberListWnd:onLeftArrowClick( go)
    CLuaStarBiwuMgr.m_CurrentZhanDuiIndex = CLuaStarBiwuMgr.m_CurrentZhanDuiIndex - 1
    self:ShowZhanDui(CLuaStarBiwuMgr.m_CurrentZhanDuiIndex)
end
function CLuaStarBiwuZhanDuiMemberListWnd:onRightArrowClick( go)
    CLuaStarBiwuMgr.m_CurrentZhanDuiIndex = CLuaStarBiwuMgr.m_CurrentZhanDuiIndex + 1
    self:ShowZhanDui(CLuaStarBiwuMgr.m_CurrentZhanDuiIndex)
end

function CLuaStarBiwuZhanDuiMemberListWnd:SetItemSelected(transform,state)
    local m_SelectedObj = transform:Find("InfoRoot/Selected").gameObject
    m_SelectedObj:SetActive(state)
end

function CLuaStarBiwuZhanDuiMemberListWnd:InitMemberItem(transform, member, index, chuZhanInfo)
    local IconTexture = transform:Find("InfoRoot/Icon"):GetComponent(typeof(CUITexture))
    local LevelLabel = transform:Find("InfoRoot/LevelBG/LevelLabel"):GetComponent(typeof(UILabel))
    local NameLabel = transform:Find("InfoRoot/NameLabel"):GetComponent(typeof(UILabel))
    local LeaderMark = transform:Find("InfoRoot/LeaderMark").gameObject
    local ClazzMarkSprite = transform:Find("InfoRoot/ClazzSprite"):GetComponent(typeof(UISprite))
    --local m_GroupObj = transform:Find("InfoRoot/ChuZhanSprite1").gameObject
    --local m_ThreeVThreeObj = transform:Find("InfoRoot/ChuZhanSprite2").gameObject
    --local m_SigleObj = transform:Find("InfoRoot/ChuZhanSprite3").gameObject
    --local m_SigleIndexLabel = transform:Find("InfoRoot/ChuZhanSprite3/BG/Label"):GetComponent(typeof(UILabel))
    local m_SelectedObj = transform:Find("InfoRoot/Selected").gameObject
    local m_NoMemberObj = transform:Find("NoMember").gameObject
    local m_InfoRoot = transform:Find("InfoRoot").gameObject

    local m_PlayerId=0

    if not member or member.m_Id == 0 then
        m_NoMemberObj:SetActive(true)
        m_NoMemberObj:GetComponent(typeof(UILabel)).text = not member and LocalString.GetString("空缺") or LocalString.GetString("隐藏")
        m_InfoRoot:SetActive(false)
        m_PlayerId = 0
        return
    end
    m_NoMemberObj:SetActive(false)
    m_InfoRoot:SetActive(true)
    m_SelectedObj:SetActive(false)
    m_PlayerId = member.m_Id
    IconTexture:LoadNPCPortrait(CUICommonDef.GetPortraitName(member.m_Class, member.m_Gender, -1), false)
    LevelLabel.text = System.String.Format("lv.{0}", member.m_Grade)
    NameLabel.text = member.m_Name
    LeaderMark:SetActive(index == 0)
    ClazzMarkSprite.spriteName = Profession.GetIcon(member.m_Class)
    if CClientMainPlayer.Inst ~= nil and m_PlayerId == CClientMainPlayer.Inst.Id then
        NameLabel.color = Color(16 / 255, 140/ 255, 0)
    else
        NameLabel.color = Color.white
    end
    if chuZhanInfo == nil or chuZhanInfo.Length < 5 then
        return
    end
end
