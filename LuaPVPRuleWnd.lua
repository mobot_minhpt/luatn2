local LocalString = import "LocalString"
local CommonDefs = import "L10.Game.CommonDefs"
local StringBuilder = import "System.Text.StringBuilder"

LuaPVPRuleWnd = class()
RegistClassMember(LuaPVPRuleWnd,"scrollView")
RegistClassMember(LuaPVPRuleWnd,"contentLabel")

function LuaPVPRuleWnd:Init()
    self.scrollView = self.transform:Find("ScrollView"):GetComponent(typeof(UIScrollView))
    self.contentLabel = self.transform:Find("ScrollView/ContentLabel"):GetComponent(typeof(UILabel))

    local content = MenPaiTiaoZhan_Setting.GetData().AwardDescription
    local ranges = MenPaiTiaoZhan_Setting.GetData().Award1
    self.contentLabel.text = nil
    local find = false
    if ranges.Length > 0 then
        do
            local i = 0
            while i < ranges.Length do
                if ranges[i][0] <= LuaMPTZMgr.SelfRankId and LuaMPTZMgr.SelfRankId <= ranges[i][1] then
                    find = true
                    if ranges[i][0] == ranges[i][1] then
                        local builder = NewStringBuilderWraper(StringBuilder)
                        builder:Append(LocalString.GetString("第"))
                        CommonDefs.StringBuilder_Append_StringBuilder_UInt32(builder, ranges[i][0])
                        builder:Append(LocalString.GetString("名"))
                        self.contentLabel.text = g_MessageMgr:Format(content, ToStringWrap(builder))
                    else
                        local builder = NewStringBuilderWraper(StringBuilder)
                        builder:Append(LocalString.GetString("第"))
                        CommonDefs.StringBuilder_Append_StringBuilder_UInt32(builder, ranges[i][0])
                        builder:Append(LocalString.GetString("名至第"))
                        CommonDefs.StringBuilder_Append_StringBuilder_UInt32(builder, ranges[i][1])
                        builder:Append(LocalString.GetString("名"))
                        self.contentLabel.text = g_MessageMgr:Format(content, ToStringWrap(builder))
                    end
                    break
                end
                i = i + 1
            end
        end
    end
    if not find then
        self.contentLabel.text = g_MessageMgr:Format(content, "")
    end
    self.scrollView:ResetPosition()
end
