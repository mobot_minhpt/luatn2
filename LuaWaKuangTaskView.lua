require("common/common_include")

local CButton = import "L10.UI.CButton"
local CUITexture = import "L10.UI.CUITexture"
local CScene = import "L10.Game.CScene"
local CUIFx = import "L10.UI.CUIFx"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local Vector3 = import "UnityEngine.Vector3"
local Ease = import "DG.Tweening.Ease"
local UIWidget = import "UIWidget"
local UIRoot=import "UIRoot"
local Screen=import "UnityEngine.Screen"
local NGUIMath=import "NGUIMath"
local Mathf = import "UnityEngine.Mathf"
local UITable = import "UITable"
local CGamePlayMgr = import "L10.Game.CGamePlayMgr"

LuaWaKuangTaskView = class()

RegistChildComponent(LuaWaKuangTaskView, "TaskName", UILabel)
RegistChildComponent(LuaWaKuangTaskView, "RemainTimeLabel", UILabel)
RegistChildComponent(LuaWaKuangTaskView, "TaskInfo", UILabel)
RegistChildComponent(LuaWaKuangTaskView, "LeaveBtn", CButton)
RegistChildComponent(LuaWaKuangTaskView, "StoneTable", UITable)
RegistChildComponent(LuaWaKuangTaskView, "StoneTemplate", GameObject)
RegistChildComponent(LuaWaKuangTaskView, "UIFx", CUIFx) -- 用于显示任务更新

RegistClassMember(LuaWaKuangTaskView, "Background")

function LuaWaKuangTaskView:Awake()
	self.TaskName.text = nil
	self.TaskInfo.text = nil
	self.StoneTemplate:SetActive(false)
	self.UIFx:DestroyFx()
end

function LuaWaKuangTaskView:Init()
	self.Background = self.transform:GetComponent(typeof(UIWidget))

	self:Refresh()

	CommonDefs.AddOnClickListener(self.LeaveBtn.gameObject, DelegateFactory.Action_GameObject(function (go)
		CGamePlayMgr.Inst:LeavePlay()
	end), false)
end

function LuaWaKuangTaskView:Refresh()

	local setting = WuYi_WaKuangSetting.GetData()
	local gameplay = Gameplay_Gameplay.GetData(setting.GamePlayId)
	if LuaWuYiMgr.m_TotalRound ~= 0 then
		self.TaskName.text = SafeStringFormat3("%s(%d/%d)", gameplay.Name, LuaWuYiMgr.m_CurrentRound, LuaWuYiMgr.m_TotalRound)
	else
		self.TaskName.text = gameplay.Name
	end

	self.TaskInfo.text = LocalString.GetString("根据矿监的要求提交足够数量的矿石。")

	CUICommonDef.ClearTransform(self.StoneTable.transform)

	local isNew = true and LuaWuYiMgr.m_StoneInfos and #LuaWuYiMgr.m_StoneInfos ~= 0

	if LuaWuYiMgr.m_StoneInfos then
		
		for i = 1, #LuaWuYiMgr.m_StoneInfos do
			local go = NGUITools.AddChild(self.StoneTable.gameObject, self.StoneTemplate)
			self:InitStoneItem(go, LuaWuYiMgr.m_StoneInfos[i])
			go:SetActive(true)

			isNew = isNew and (LuaWuYiMgr.m_StoneInfos[i].got == 0)
		end
	end

	self.StoneTable:Reposition()
	-- 自适应
	local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject)
	local virtualScreenHeight = Screen.height * scale
	local totalHeight = NGUIMath.CalculateRelativeWidgetBounds(self.StoneTable.transform).size.y + (self.StoneTable.padding.y * 2) + 200
	self.Background.height = Mathf.CeilToInt(totalHeight)
	self.LeaveBtn:GetComponent(typeof(UISprite)):ResetAndUpdateAnchors()

	if isNew then
		self:TaskDemandRefresh()
	end
end

-- 任务栏矿石需求刷新，流光特效
function LuaWaKuangTaskView:TaskDemandRefresh()
	self.UIFx:DestroyFx()
	self.UIFx:LoadFx(CUIFxPaths.CommonYellowLineFxPath)
	local Background = self.transform:GetComponent(typeof(UISprite))

	LuaTweenUtils.DOKill(self.UIFx.FxRoot, false)
	local path = {Vector3(Background.width/2,0,0),Vector3(-Background.width/2,0,0),Vector3(-Background.width/2,-Background.height,0),Vector3(Background.width/2,-Background.height,0),Vector3(Background.width/2,0,0)}
	local array = Table2Array(path, MakeArrayClass(Vector3))
    LuaUtils.SetLocalPosition(self.UIFx.FxRoot,Background.width/2,0,0)
    LuaTweenUtils.DOLocalPathOnce(self.UIFx.FxRoot, array, 3, Ease.Linear)
    LuaUtils.SetLocalPosition(self.UIFx.transform, 0, 0, 0)
end

function LuaWaKuangTaskView:InitStoneItem(go, info)
	local stoneLabel = go.transform:GetComponent(typeof(UILabel))
	local stoneTexture = go.transform:Find("StoneTexture"):GetComponent(typeof(CUITexture))
	local kuangShi = WuYi_KuangShi.GetData(info.monsterId)
	local monster = Monster_Monster.GetData(info.monsterId)
	stoneTexture:LoadMaterial(kuangShi.Icon)
	stoneLabel.text = SafeStringFormat3("%s(%d/%d)", monster.Name, info.got, info.need)
end

function LuaWaKuangTaskView:OnSceneRemainTimeUpdate(args)
	if CScene.MainScene then
        if CScene.MainScene.ShowTimeCountDown then
            self.RemainTimeLabel.text = SafeStringFormat3(LocalString.GetString("计时 [00FF00]%s[-]"), self:GetRemainTimeText(CScene.MainScene.ShowTime))
        else
            self.RemainTimeLabel.text = nil
        end
    else
        self.RemainTimeLabel.text = nil
    end
end

function LuaWaKuangTaskView:GetRemainTimeText(totalSeconds)
    if totalSeconds < 0 then
        totalSeconds = 0
    end
    if totalSeconds >= 3600 then
        return SafeStringFormat3("%02d:%02d:%02d", math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
    else
        return SafeStringFormat3("%02d:%02d", math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end


function LuaWaKuangTaskView:OnCanLeaveGamePlay()
	if CScene.MainScene then
        self.LeaveBtn.gameObject:SetActive(CScene.MainScene.ShowLeavePlayButton)
    end
end

function LuaWaKuangTaskView:OnShowTimeCountDown()
	--[[if CScene.MainScene then
        self.RemainTimeLabel.enabled = CScene.MainScene.ShowTimeCountDown or CScene.MainScene.ShowMonsterCountDown
    end--]]
end

-- todo 断线重连
function LuaWaKuangTaskView:OnMainPlayerCreated()
	-- body
end

function LuaWaKuangTaskView:OnEnable()
	g_ScriptEvent:AddListener("SynWaKuangTaskView", self, "Refresh")
	g_ScriptEvent:AddListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
	g_ScriptEvent:AddListener("CanLeaveGamePlay", self, "OnCanLeaveGamePlay")
	g_ScriptEvent:AddListener("ShowTimeCountDown", self, "OnShowTimeCountDown")
	g_ScriptEvent:AddListener("MainPlayerCreated", self, "OnMainPlayerCreated")
	g_ScriptEvent:AddListener("TaskDemandRefresh", self, "TaskDemandRefresh")
end

function LuaWaKuangTaskView:OnDisable()
	g_ScriptEvent:RemoveListener("SynWaKuangTaskView", self, "Refresh")
	g_ScriptEvent:RemoveListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
	g_ScriptEvent:RemoveListener("CanLeaveGamePlay", self, "OnCanLeaveGamePlay")
	g_ScriptEvent:RemoveListener("ShowTimeCountDown", self, "OnShowTimeCountDown")
	g_ScriptEvent:RemoveListener("MainPlayerCreated", self, "OnMainPlayerCreated")
	g_ScriptEvent:RemoveListener("TaskDemandRefresh", self, "TaskDemandRefresh")
end

return LuaWaKuangTaskView
