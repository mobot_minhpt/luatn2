local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"

LuaJuDianBattleDailyBtn = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaJuDianBattleDailyBtn, "Btn", "Btn", GameObject)
RegistChildComponent(LuaJuDianBattleDailyBtn, "CountLabel", "CountLabel", UILabel)

--@endregion RegistChildComponent end

function LuaJuDianBattleDailyBtn:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.Btn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBtnClick()
	end)


    --@endregion EventBind end
end

function LuaJuDianBattleDailyBtn:Init()
end

function LuaJuDianBattleDailyBtn:OnData()
	local data = LuaJuDianBattleMgr.SceneMonsterData

	local count = 0
	for mapId, list in pairs(data) do
		count = count + #list
	end

	local sp = self.transform:Find("Sprite").gameObject
	self.Btn:SetActive(count ~= 0)
	self.CountLabel.gameObject:SetActive(count ~= 0)
	sp:SetActive(count ~= 0)

	self.CountLabel.text = SafeStringFormat3(LocalString.GetString("余%s"), tostring(count))
end

function LuaJuDianBattleDailyBtn:OnEnable()
	Gac2Gas.GuildJuDianQuerySceneOverview()
    g_ScriptEvent:AddListener("JuDianBattle_SyncDailySceneData", self, "OnData")
end

function LuaJuDianBattleDailyBtn:OnDisable()
    g_ScriptEvent:RemoveListener("JuDianBattle_SyncDailySceneData", self, "OnData")
end


--@region UIEvent

function LuaJuDianBattleDailyBtn:OnBtnClick()
	CUIManager.ShowUI(CLuaUIResources.JuDianBattleDailySceneViewWnd)
end

--@endregion UIEvent

