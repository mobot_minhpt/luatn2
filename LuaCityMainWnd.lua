require("common/common_include")

local LuaUtils = import "LuaUtils"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local NGUITools = import "NGUITools"
local Vector3 = import "UnityEngine.Vector3"
local CommonDefs = import "L10.Game.CommonDefs"
local QnTabView = import "L10.UI.QnTabView"
local UITable = import "UITable"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local UISprite = import "UISprite"
local Extensions = import "Extensions"
local CScene = import "L10.Game.CScene"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CGuildMgr = import "L10.Game.CGuildMgr"

CLuaCityMainWnd = class()
CLuaCityMainWnd.Path = "ui/citywar/LuaCityMainWnd"

RegistClassMember(CLuaCityMainWnd, "m_TabView")
RegistClassMember(CLuaCityMainWnd, "m_CurrentTabIndex")
RegistClassMember(CLuaCityMainWnd, "m_bRuleInited")
RegistClassMember(CLuaCityMainWnd, "m_bMapInited")
RegistClassMember(CLuaCityMainWnd, "m_LineObj")
RegistClassMember(CLuaCityMainWnd, "m_LineArrowObj")
RegistClassMember(CLuaCityMainWnd, "m_CircleObj")
RegistClassMember(CLuaCityMainWnd, "m_RegionObjTable")
RegistClassMember(CLuaCityMainWnd, "m_HistoryDataTable")
RegistClassMember(CLuaCityMainWnd, "m_RouteTable")
RegistClassMember(CLuaCityMainWnd, "m_LineRootTrans")

-- Draw Path
RegistClassMember(CLuaCityMainWnd, "m_MapId2CircleObjTable")
RegistClassMember(CLuaCityMainWnd, "m_MapId2LineFlagTable")

function CLuaCityMainWnd:Init( ... )
	self:SyncMyCityData(CLuaCityWarMgr.MyCityGuildPlayData, CLuaCityWarMgr.MyCityBasicInfo, CLuaCityWarMgr.MyCityUnitInfo, CLuaCityWarMgr.MyCityMiscInfo)
	self:AlterCityMissionSuccess(CLuaCityWarMgr.MyCityGuildPlayData.Mission)

	if CLuaCityWarMgr.MyCityWndOpenActionType and CLuaCityWarMgr.MyCityWndOpenActionType == EnumQueryCityDataAction.eJoinYaYun then
		--此时GongFengView的代码尚未实例化
		CLuaCityWarMgr.MyCityWndJumpToGongFeng = true
		self.m_TabView:ChangeTo(2)
	else
		self.m_TabView:ChangeTo(0)
	end
end

function CLuaCityMainWnd:Awake( ... )
	self.m_bRuleInited = false
	self.m_bMapInited = false
	self.m_HistoryDataTable = {}
	self.m_RouteTable = {}

	self.m_TabView = self.transform:Find("Anchor"):GetComponent(typeof(QnTabView))
	self.m_TabView.OnSelect = CommonDefs.CombineListner_Action_QnTabButton_int(self.m_TabView.OnSelect, (DelegateFactory.Action_QnTabButton_int(function (button, index)
		self.m_CurrentTabIndex = index
		if index == 1 then
        	if not self.m_bRuleInited then
	        	self:InitRuleWnd()
    		end
        elseif index == 3 then
        	if not self.m_bMapInited then
        		self:InitMap()
        	end
        end
    end)), true)

    UIEventListener.Get(self.transform:Find("Anchor/Info/BasicPropertyFrame/MapBtn").gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
    	CLuaCityWarMgr:OpenPrimaryMap()
    end)

	UIEventListener.Get(self.transform:Find("Anchor/Info/BtnGoHome").gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
    	Gac2Gas.RequestBackToOwnCity()
    end)

    UIEventListener.Get(self.transform:Find("Anchor/Info/Announce/ChangeNameBtn").gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
    	CLuaCommonAnnounceMgr.ShowWnd(LocalString.GetString("城战通告修改"), LocalString.GetString("请输入城战通告"), function (ret)
    		self:ChangeAnnounce(ret)
    	end, 200, LocalString.GetString("1~100个汉字"), CLuaCityWarMgr.MyCityGuildPlayData.Mission)
    end)

	UIEventListener.Get(self.transform:Find("Anchor/Info/BtnContribution").gameObject).onClick = LuaUtils.VoidDelegate(function (...)
		CUIManager.ShowUI(CLuaUIResources.CityWarContributionWnd)
	end)
	UIEventListener.Get(self.transform:Find("Anchor/Info/BtnManage").gameObject).onClick = LuaUtils.VoidDelegate(function (...)
		CUIManager.ShowUI(CUIResources.CityWarSoldierWnd)
	end)
	Gac2Gas.CheckMyRights("CityBuild", 0)
	UIEventListener.Get(self.transform:Find("Anchor/Info/BtnAbandon").gameObject).onClick = LuaUtils.VoidDelegate(function (...)
		if not CGuildMgr.Inst.CanCityBuild then
			g_MessageMgr:ShowMessage("Guild_No_Power")
			Gac2Gas.CheckMyRights("CityBuild", 0)
			return
		end

		local content = g_MessageMgr:FormatMessage("KFCZ_ABANDON_CITY_CONFIRM", math.ceil(CityWar_Setting.GetData().ReOccupyCityCD / 60))
		MessageWndManager.ShowDelayOKCancelMessage(content, DelegateFactory.Action(function ()
			Gac2Gas.RequestAbandonCity()
		end), nil, 5)
	end)
end

function CLuaCityMainWnd:ChangeAnnounce(announce)
    local length = CUICommonDef.GetStrByteLength(announce)
    if length < 2 or length > 200 then
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("城战通告长度必须在1-100个汉字之内，请重新输入"))
        return
    end

    if not CWordFilterMgr.Inst:CheckGuildMotto(announce) then
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("城战通告中包含违规内容，请修改"))
        return
    end

    local len = CUICommonDef.GetStrByteLength(announce)
    if (LocalString.GetString("城战通告修改") == announce) or len < 2 or len > 200 then
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("城战通告长度必须在1-100个汉字之内，请重新输入"))
        return
    end

    if CommonDefs.StringLength(announce) > 50 then
        local guildMission1 = CommonDefs.StringSubstring2(announce, 0, 50)
        local guildMission2 = CommonDefs.StringSubstring1(announce, 50)
        Gac2Gas.RequestAlterCityMission(CClientMainPlayer.Inst.BasicProp.GuildId, guildMission1)
        Gac2Gas.RequestAlterCityMissionEnd(CClientMainPlayer.Inst.BasicProp.GuildId, guildMission2)
    else
        Gac2Gas.RequestAlterCityMissionEnd(CClientMainPlayer.Inst.BasicProp.GuildId, announce)
    end
end

function CLuaCityMainWnd:InitRuleWnd( ... )
	local templateObj = self.transform:Find("Anchor/Rule/Template").gameObject
	templateObj:SetActive(false)
	local paraObj = self.transform:Find("Anchor/Rule/Template/Item").gameObject
	paraObj:SetActive(false)
	local rootTable = self.transform:Find("Anchor/Rule/Scroll View/Table"):GetComponent(typeof(UITable))

 	local content = g_MessageMgr:FormatMessage("KFCZ_RULE_INTERFACE_MSG_SIMPLIFIED_VERSION")
 	local index = 1
 	while true do
 		local prevTag, _ = string.find(content, "<title>")
 		local nextTag, _ = string.find(content, "<title>", prevTag + 1)
 		local curContent = content
 		if nextTag then
 			curContent = string.sub(content, prevTag, nextTag - 1)
 			content = string.sub(content, nextTag)
 		end

 		local obj = NGUITools.AddChild(rootTable.gameObject, templateObj)
 		obj:SetActive(true)

 		local title = string.match(curContent, "<title>(.*)</title>")
 		obj.transform:Find("Title"):GetComponent(typeof(UILabel)).text = title

 		local pTable = obj.transform:Find("Table"):GetComponent(typeof(UITable))
 		for p in string.gmatch(curContent, "<p>(.-)</p>") do
 			local pObj = NGUITools.AddChild(pTable.gameObject, paraObj)
 			pObj:SetActive(true)
 			pObj.transform:Find("Label"):GetComponent(typeof(UILabel)).text = p
 		end
 		pTable:Reposition()

 		if not nextTag then break end
 	end
 	rootTable:Reposition()


 	self.m_bRuleInited = true
end

function CLuaCityMainWnd:SyncMyCityData(guildPlayData, basicInfo, unitInfo, miscInfo)
	local basicRootTrans = self.transform:Find("Anchor/Info/BasicPropertyFrame")
	basicRootTrans:Find("City/ValueLabel"):GetComponent(typeof(UILabel)).text = CityWar_MapCity.GetData(basicInfo.TemplateId).Name
	basicRootTrans:Find("Region/ValueLabel"):GetComponent(typeof(UILabel)).text = CityWar_Map.GetData(basicInfo.MapId).Name
	basicRootTrans:Find("MainCity/ValueLabel"):GetComponent(typeof(UILabel)).text = CityWar_MapCity.GetData(CityWar_Map.GetData(basicInfo.MapId).MajorCity).Name
	basicRootTrans:Find("Grade/ValueLabel"):GetComponent(typeof(UILabel)).text = basicInfo.Grade
	basicRootTrans:Find("Guild/ValueLabel"):GetComponent(typeof(UILabel)).text = basicInfo.OwnerName
	basicRootTrans:Find("Asset/ValueLabel"):GetComponent(typeof(UILabel)).text = guildPlayData.Material.."/"..CityWar_WareHouseGrade.GetData(miscInfo.WareHouseGrade).Material

	local nameTable = {"Repertory", "Gate", "Wall", "Cannon", "Tower", "ControlTower", "Bomb", "ZhongPao", "Trap", "TanSheBan"}
	local countTable = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
	CommonDefs.DictIterate(unitInfo.Units, DelegateFactory.Action_object_object(function (key, value)
		local curType = CLuaCityWarMgr.UnitDesignData[value.TemplateId].Type
		if curType == 24 then
			countTable[1] = value.TemplateId % 100
		elseif curType == 3 then
			countTable[2] = countTable[2] + 1
		elseif curType == 1 then
			countTable[3] = countTable[3] + 1
		elseif curType == 4 then
			countTable[4] = countTable[4] + 1
		elseif curType >= 15 and curType <= 22 then
			countTable[5] = countTable[5] + 1
		elseif curType >= 9 and curType <= 14 then
			countTable[6] = countTable[6] + 1
		elseif curType == 6 then
			countTable[7] = countTable[7] + 1
		elseif curType == 5 then
			countTable[8] = countTable[8] + 1
		elseif curType == 8 then
			countTable[9] = countTable[9] + 1
		elseif curType == 7 then
			countTable[10] = countTable[10] + 1
		end
	end))

	for i = 1, #nameTable do
		basicRootTrans:Find(nameTable[i].."/ValueLabel"):GetComponent(typeof(UILabel)).text = countTable[i]
	end
end

function CLuaCityMainWnd:AlterCityMissionSuccess(announce)
	CLuaCityWarMgr.MyCityGuildPlayData.Mission = announce
	if announce == "" then announce = LocalString.GetString("点击编辑按钮输入通告") end
	self.transform:Find("Anchor/Info/Announce/QnInputField/Label"):GetComponent(typeof(UILabel)).text = announce
	CLuaCommonAnnounceMgr.CloseWnd()
end

function CLuaCityMainWnd:InitMap( ... )
	self.m_LineRootTrans = self.transform:Find("Anchor/Path/MapRoot/LineRoot")
	self.m_LineObj = self.transform:Find("Anchor/Path/MapRoot/Line").gameObject
	self.m_LineArrowObj = self.transform:Find("Anchor/Path/MapRoot/Arrow").gameObject
	self.m_CircleObj = self.transform:Find("Anchor/Path/MapRoot/Circle").gameObject
	self.m_CircleObj:SetActive(false)
	self.m_LineObj:SetActive(false)
	self.m_LineArrowObj:SetActive(false)

	local guildMapId = tonumber(CLuaCityWarMgr.MyCityGuildPlayData.CityMapId)
	local isMajorCity = CLuaCityWarMgr:IsMajorCity(CLuaCityWarMgr.MyCityGuildPlayData.CityTemplateId)
	local myMapId = 0
    if CScene.MainScene then
        myMapId = CScene.MainScene.SceneTemplateId
    end


	local templateObj = self.transform:Find("Anchor/Path/MapRoot/Center").gameObject
	local nameTable = {"ChiTian", "QingTian", "XuanTian", "SuTian"}
	local rootTransTable = {}
	self.m_RegionObjTable = {}
	for i = 1, #nameTable do
		table.insert(rootTransTable, self.transform:Find("Anchor/Path/MapRoot/"..nameTable[i]):GetComponent(typeof(UITable)))
	end

	local mapIdTable = {}
	CityWar_Map.ForeachKey(function(key)
		table.insert(mapIdTable, key)
	end)
	table.sort(mapIdTable)

	for _, mapId in ipairs(mapIdTable) do
		local obj
		local map = CityWar_Map.GetData(mapId)
		if map.Zone == 0 then
			obj = templateObj
		else
			obj = NGUITools.AddChild(rootTransTable[map.Zone].gameObject, templateObj)
		end
		obj:SetActive(true)
		obj.transform:Find("GuildFlag").gameObject:SetActive(false)
		obj.transform:Find("LocationFlag").gameObject:SetActive(false)
		obj.transform:Find("Level"):GetComponent(typeof(UILabel)).text = "lv."..map.Level
		self.m_RegionObjTable[mapId] = obj

		local guildFlagObj = obj.transform:Find("GuildFlag").gameObject
		guildFlagObj:SetActive(mapId == guildMapId)
		if mapId == guildMapId and not isMajorCity then
			guildFlagObj:GetComponent(typeof(UISprite)).spriteName = "citywarprimarymapwnd_chengchiweizhi_02"
		end
		obj.transform:Find("LocationFlag").gameObject:SetActive(mapId == myMapId)
	end

	for i = 1, 4 do
		rootTransTable[i]:Reposition()
	end

	UIEventListener.Get(self.transform:Find("Anchor/Path/HistoryBtn").gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
		CUIManager.ShowUI(CLuaUIResources.CityWarHistoryWnd)
	end)

	Gac2Gas.QueryCityWarHistory()

	self.m_bMapInited = true
end

function CLuaCityMainWnd:DrawLine(mapId1, mapId2)
	-- Destroy Circle and store line flag
	for _, v in pairs({mapId1, mapId2}) do
		if self.m_MapId2CircleObjTable[v] then
			GameObject.Destroy(self.m_MapId2CircleObjTable[v])
			self.m_MapId2CircleObjTable[v] = nil
		end
		self.m_MapId2LineFlagTable[v] = true
	end

	local pos1, pos2 = self.m_LineRootTrans:InverseTransformPoint(self.m_RegionObjTable[mapId1].transform.position), self.m_LineRootTrans:InverseTransformPoint(self.m_RegionObjTable[mapId2].transform.position)
	local pos = Vector3((pos1.x + pos2.x)/2, (pos1.y + pos2.y)/2, 0)
	local obj = NGUITools.AddChild(self.m_LineRootTrans.gameObject, self.m_LineObj)
	obj:SetActive(true)
	obj.transform.localPosition = pos
	local delta = Vector3((pos2.x - pos1.x), (pos2.y - pos1.y), 0)
	obj:GetComponent(typeof(UISprite)).width = Vector3.Magnitude(delta)
	local angle = math.atan2(delta.y, delta.x) / math.pi * 180
	Extensions.SetLocalRotationZ(obj.transform, angle)
	return pos2, angle
end

function CLuaCityMainWnd:DrawCircle(mapId)
	-- donot draw circle if line exsit
	if self.m_MapId2LineFlagTable[mapId] then return end
	local obj = NGUITools.AddChild(self.m_LineRootTrans.gameObject, self.m_CircleObj)
	obj:SetActive(true)
	obj.transform.localPosition = self.m_LineRootTrans:InverseTransformPoint(self.m_RegionObjTable[mapId].transform.position)
end

function CLuaCityMainWnd:SendCityWarHistory(dataUD)
	local list = MsgPackImpl.unpack(dataUD)
	if not list then return end
	for i = 0, list.Count - 1, 7 do
		local writeTime = list[i]
		local eventType = list[i + 1]
		local eventStr = list[i + 2]
		local rivalGuildName = list[i + 3]
		local hasPlaySummary = list[i + 4]
		local mapId = list[i + 5]
		local templateId = list[i + 6]

		table.insert(self.m_HistoryDataTable, {MapId = mapId, Time=writeTime, EventType=eventType, EventStr=eventStr, GuildName=rivalGuildName, HasResult=hasPlaySummary>=1})
	end
end

function CLuaCityMainWnd:SendCityWarHistoryEnd()
	table.sort(self.m_HistoryDataTable, function (a, b)
		return a.Time < b.Time
	end)
	for _, v in ipairs(self.m_HistoryDataTable) do
		if v.EventType == 1 then
			table.insert(self.m_RouteTable, v.MapId)
		end
	end

	CLuaCityWarHistoryWnd.HistoryDataTable = self.m_HistoryDataTable
	Extensions.RemoveAllChildren(self.m_LineRootTrans)
	self.m_MapId2LineFlagTable = {}
	self.m_MapId2CircleObjTable = {}

	for i = 2, #self.m_RouteTable do
		local bPreNeighbor = CLuaCityWarMgr:IsNeighbor(self.m_RouteTable[i - 1], self.m_RouteTable[i])
		local bPostNeighbor = i ~= #self.m_RouteTable and CLuaCityWarMgr:IsNeighbor(self.m_RouteTable[i], self.m_RouteTable[i + 1])

		if bPreNeighbor then
			local endPos, angle = self:DrawLine(self.m_RouteTable[i - 1], self.m_RouteTable[i])
			if not bPostNeighbor then
				self.m_LineArrowObj:SetActive(true)
				self.m_LineArrowObj.transform.localPosition = endPos
				Extensions.SetLocalRotationZ(self.m_LineArrowObj.transform, angle - 90)
			end
		else
			self:DrawCircle(self.m_RouteTable[i - 1])
		end
	end
end

function CLuaCityMainWnd:OnEnable( ... )
	g_ScriptEvent:AddListener("AlterCityMissionSuccess", self, "AlterCityMissionSuccess")
	g_ScriptEvent:AddListener("SendCityWarHistoryEnd", self, "SendCityWarHistoryEnd")
	g_ScriptEvent:AddListener("SendCityWarHistory", self, "SendCityWarHistory")
end

function CLuaCityMainWnd:OnDisable( ... )
	g_ScriptEvent:RemoveListener("AlterCityMissionSuccess", self, "AlterCityMissionSuccess")
	g_ScriptEvent:RemoveListener("SendCityWarHistoryEnd", self, "SendCityWarHistoryEnd")
	g_ScriptEvent:RemoveListener("SendCityWarHistory", self, "SendCityWarHistory")
end

function CLuaCityMainWnd:OnDestroy( ... )
	CLuaCityWarHistoryWnd.HistoryDataTable = {}
end

function CLuaCityMainWnd:GetGuideGo( methodName)
    if methodName == "GetReturnCityButton" then
		return self.transform:Find("Anchor/Info/BtnGoHome").gameObject
    else
        return nil
    end
end

function CLuaCityMainWnd:GetTabIndex()
    return self.m_TabView.CurrentSelectTab
end
