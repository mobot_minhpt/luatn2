local CButton = import "L10.UI.CButton"
local CIMMgr = import "L10.Game.CIMMgr"
local UIInput = import "UIInput"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local Constants = import "L10.Game.Constants"
local MessageWndManager = import "L10.UI.MessageWndManager"
local Money = import "L10.Game.Money"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local QnCheckBox = import "L10.UI.QnCheckBox"
local CItemMgr = import "L10.Game.CItemMgr"

LuaYuanXiao2023QiYuanWnd = class()

RegistClassMember(LuaYuanXiao2023QiYuanWnd, "m_Grid")
RegistClassMember(LuaYuanXiao2023QiYuanWnd, "m_SelectBtn")
RegistClassMember(LuaYuanXiao2023QiYuanWnd, "m_NameLabel")
RegistClassMember(LuaYuanXiao2023QiYuanWnd, "m_BuyBtn")
RegistClassMember(LuaYuanXiao2023QiYuanWnd, "m_StatusText")
RegistClassMember(LuaYuanXiao2023QiYuanWnd, "m_CheckBoxLabel")
RegistClassMember(LuaYuanXiao2023QiYuanWnd, "m_CheckBox")

RegistClassMember(LuaYuanXiao2023QiYuanWnd, "m_SelectedTargetPlayerId")
RegistClassMember(LuaYuanXiao2023QiYuanWnd, "m_SelectedTargetFangDengIndex")
RegistClassMember(LuaYuanXiao2023QiYuanWnd, "m_FangDengList")
RegistClassMember(LuaYuanXiao2023QiYuanWnd, "m_VoucherItemId")
RegistClassMember(LuaYuanXiao2023QiYuanWnd, "m_VoucherCount")
RegistClassMember(LuaYuanXiao2023QiYuanWnd, "m_LingYuFangdengLabel")
RegistClassMember(LuaYuanXiao2023QiYuanWnd, "m_LingYuFangdengCount")

function LuaYuanXiao2023QiYuanWnd:Init()
    self.m_Grid = self.transform:Find("Anchor/CenterRoot/Grid"):GetComponent(typeof(UIGrid))
    self.m_SelectBtn = self.transform:Find("Anchor/TopRoot/SelectBtn"):GetComponent(typeof(CButton))
    self.m_NameLabel = self.transform:Find("Anchor/TopRoot/NameLabel"):GetComponent(typeof(UILabel))
    self.m_BuyBtn = self.transform:Find("Anchor/BuyBtn"):GetComponent(typeof(CButton))
    self.m_StatusText = self.transform:Find("Anchor/TopRoot/StatusText"):GetComponent(typeof(UIInput))
    self.m_StatusText.enablelMultiline = false
    self.m_CheckBoxLabel = self.transform:Find("Anchor/BottomRoot/CheckBox/CheckBoxLabel"):GetComponent(typeof(UILabel))
    self.m_CheckBox = self.transform:Find("Anchor/BottomRoot/CheckBox"):GetComponent(typeof(QnCheckBox))

    UIEventListener.Get(self.m_SelectBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function()
        LuaYuanXiao2023Mgr:ShowPlayListWnd()
    end)
    UIEventListener.Get(self.m_BuyBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function()
        self:OnBuyButtonClick()
    end)

    self.m_VoucherItemId = YuanXiao_LanternSetting.GetData().CouponTempId
    local bindCount, notBindCount
    bindCount, notBindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(self.m_VoucherItemId)
    local sumCount = bindCount + notBindCount
    self.m_VoucherCount = sumCount
    --sumCount = 10
    self.m_CheckBoxLabel.text = SafeStringFormat3(LocalString.GetString("消耗包裹中的祈愿灯抵扣券（%d张）"), sumCount)

    self.m_FangDengList = {}
    for i = 1,3 do
        local go = self.m_Grid.transform:GetChild(i - 1).gameObject
        self:InitFangDeng(go, i)
        go:SetActive(true)
    end
    self.m_Grid:Reposition()
    self:SelectFangDeng()

    self.m_CheckBox.OnValueChanged = DelegateFactory.Action_bool(
		function(select)
            if select then
                local n = self.m_LingYuFangdengCount - (self.m_VoucherCount * 100000)
                if n<0 then
                    n=0
                end
                self.m_LingYuFangdengLabel.text = tostring(n)
            else
                self.m_LingYuFangdengLabel.text = tostring(self.m_LingYuFangdengCount)
            end
        end
    )

    if sumCount>0 then
        self.m_CheckBox:SetSelected(true, false)
    else
        self.m_CheckBox:SetSelected(false, true)
        self.m_CheckBox.Enabled = false
    end
end

function LuaYuanXiao2023QiYuanWnd:OnEnable()
    g_ScriptEvent:AddListener("YuanXiao2020_SelectPlayer", self, "OnSelectPlayer")
end

function LuaYuanXiao2023QiYuanWnd:OnDisable()
    g_ScriptEvent:RemoveListener("YuanXiao2020_SelectPlayer", self, "OnSelectPlayer")
end

function LuaYuanXiao2023QiYuanWnd:InitFangDeng(go, index)
    local t = {}
    t.Bg = go.transform:Find("Bg"):GetComponent(typeof(CUITexture))
    t.NameLabel = go.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    t.MoneyIcon = go.transform:Find("MoneyIcon"):GetComponent(typeof(UISprite))
    t.PriceLabel = go.transform:Find("MoneyIcon/PriceLabel"):GetComponent(typeof(UILabel))
    t.Selected = go.transform:Find("Selected"):GetComponent(typeof(UITexture))
    t.Index = index
    local lanternType = YuanXiao_LanternType.GetData(t.Index)
    t.NameLabel.pivot = UIWidget.Pivot.TopLeft
    t.NameLabel.width = 350
    t.NameLabel.height = 40
    t.NameLabel.fontSize = 30
    LuaUtils.SetLocalPosition(t.NameLabel.transform, -557, -34, 0)
    local friendness = index == 1 and 520 or (index == 2 and 2333 or 10000)
    t.NameLabel.text = lanternType.Name.." ("..LocalString.GetString("友好度").."+"..friendness..")"
    t.MoneyIcon.spriteName = Money.GetIconName((lanternType.PriceType == 1) and EnumMoneyType.LingYu or EnumMoneyType.YinLiang, EnumPlayScoreKey.NONE)
    t.PriceLabel.text = lanternType.Price
    -- 储存银两放灯的信息
    if lanternType.PriceType ~= 1 then
        self.m_LingYuFangdengLabel = t.PriceLabel
        self.m_LingYuFangdengCount = lanternType.Price
    end
    -- 选中某款放灯
    UIEventListener.Get(t.Bg.gameObject).onClick = DelegateFactory.VoidDelegate(function()
        self:SelectFangDeng(t)
    end)
    t.isJade = lanternType.PriceType == 1
    table.insert(self.m_FangDengList,t)
    return t
end


function LuaYuanXiao2023QiYuanWnd:SelectFangDeng(t)
    self.m_SelectedTargetFangDengIndex = (t == nil) and 1 or t.Index
    for i = 1,3 do
        self.m_FangDengList[i].Selected.enabled = (self.m_SelectedTargetFangDengIndex == self.m_FangDengList[i].Index)
    end
end

function LuaYuanXiao2023QiYuanWnd:OnSelectPlayer(playerId)
    self:ShowNameLabel(playerId)
end

function LuaYuanXiao2023QiYuanWnd:ShowNameLabel(playerId)
    local player = CClientMainPlayer.Inst
    if not player then
        return
    end

    if playerId == nil and self.m_SelectedTargetPlayerId == nil then
        self.m_NameLabel.alpha = 0.3
        self.m_NameLabel.text = LocalString.GetString("尚未添加对象")
        return
    end

    self.m_NameLabel.alpha = 1.0

    if playerId == player.Id then
        self.m_NameLabel.text = player.Name
    else
        local basicInfo = CIMMgr.Inst:GetBasicInfo(playerId)
        if basicInfo then
            self.m_NameLabel.text = basicInfo.Name
        end
    end
    self.m_SelectedTargetPlayerId = playerId
end

function LuaYuanXiao2023QiYuanWnd:CheckMoneyEnough()
    local player = CClientMainPlayer.Inst
    if not player then
        return false
    end
    local needJade = tonumber(self.m_FangDengList[self.m_SelectedTargetFangDengIndex].PriceLabel.text)
    local isJade = self.m_FangDengList[self.m_SelectedTargetFangDengIndex].isJade
    if isJade and (not CShopMallMgr.TryCheckEnoughJade(needJade, 0)) then
        return false
    elseif not isJade then
        if needJade > player.Silver then
            local txt = g_MessageMgr:FormatMessage("NotEnough_Silver_QuickBuy")
            MessageWndManager.ShowOKCancelMessage(txt, DelegateFactory.Action(function ()
                CShopMallMgr.ShowLinyuShoppingMall(Constants.SilverItemId)
            end), nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
            return false
        end
    end
    return true
end

function LuaYuanXiao2023QiYuanWnd:OnBuyButtonClick()
    if not self.m_SelectedTargetPlayerId then
        g_MessageMgr:ShowMessage("YuanXiao2020_FangDeng_Not_Select_Target")
        return
    end

    local wishMsg = CUICommonDef.Trim(self.m_StatusText.value)
    if wishMsg == "" then
        g_MessageMgr:ShowMessage("YuanXiao2020_FangDeng_Not_Input_Wish")
        return
    end

    local ret = CWordFilterMgr.Inst:DoFilterOnSendWithoutVoiceCheck(wishMsg, false)
    if not ret then return end
    if ret.msg == nil or ret.shouldBeIgnore then
        g_MessageMgr:ShowMessage("YuanXiao2020_Speech_Violation")
        return
    end

    if not self:CheckMoneyEnough() then
        return
    end

    local isJade = self.m_FangDengList[self.m_SelectedTargetFangDengIndex].isJade
    local selectTargetName = self.m_NameLabel.text
    local needJade = tonumber(self.m_FangDengList[self.m_SelectedTargetFangDengIndex].PriceLabel.text)
    local isUseVoucher = (self.m_CheckBox.Selected and not isJade) and 1 or 0
    local voucherStr = ""
    if isUseVoucher == 1 then
        local n = self.m_VoucherCount
        if n >10 then
            n = 10
        end
        voucherStr = SafeStringFormat3(LocalString.GetString("[FFFFFF]%d张[FA800A]祈愿灯抵扣券[-]和[-]"), n)
    end

    local message = g_MessageMgr:FormatMessage("YuanXiao2020_FangDeng_Confirm",
            voucherStr..needJade,isJade and LocalString.GetString("灵玉") or LocalString.GetString("银两"), selectTargetName)
    MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function()
        LuaYuanXiao2023Mgr:RequestFangDeng(self.m_SelectedTargetPlayerId, selectTargetName, self.m_SelectedTargetFangDengIndex, isUseVoucher, wishMsg)
    end), nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
end