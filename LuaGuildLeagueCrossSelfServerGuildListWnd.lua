
LuaGuildLeagueCrossSelfServerGuildListWnd = class()

RegistClassMember(LuaGuildLeagueCrossSelfServerGuildListWnd, "m_ItemList")

function LuaGuildLeagueCrossSelfServerGuildListWnd:Awake()
    self.m_ItemList = self.transform:Find("Anchor/List")
end

function LuaGuildLeagueCrossSelfServerGuildListWnd:Init()
    self:OnQueryGLCJoinCrossGuildInfoSuccess({})
    LuaGuildLeagueCrossMgr:QueryGLCJoinCrossGuildInfo()
end

function LuaGuildLeagueCrossSelfServerGuildListWnd:OnEnable()
    g_ScriptEvent:AddListener("OnQueryGLCJoinCrossGuildInfoSuccess", self, "OnQueryGLCJoinCrossGuildInfoSuccess")
end

function LuaGuildLeagueCrossSelfServerGuildListWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnQueryGLCJoinCrossGuildInfoSuccess", self, "OnQueryGLCJoinCrossGuildInfoSuccess")
end

function LuaGuildLeagueCrossSelfServerGuildListWnd:OnQueryGLCJoinCrossGuildInfoSuccess(guildTbl)
    local n = self.m_ItemList.childCount
    for i=0, n-1 do
        local child = self.m_ItemList:GetChild(i).gameObject
        if i>=#guildTbl then
            child:SetActive(false)
        else
            child:SetActive(true)
            local isMyGuild = LuaGuildLeagueCrossMgr:IsMyGuild(guildTbl[i+1].guildId)
            child.transform:Find("NameLabel"):GetComponent(typeof(UILabel)).text = SafeStringFormat3("%s%s", isMyGuild and "[79ff7c]" or "", guildTbl[i+1].guildName)
            child.transform:Find("IDLabel"):GetComponent(typeof(UILabel)).text = "ID "..tostring(guildTbl[i+1].guildId)
            child.transform:Find("RankLabel"):GetComponent(typeof(UILabel)).text= guildTbl[i+1].rankStr
        end
    end
end

