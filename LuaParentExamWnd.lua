require("common/common_include")

local CKeJuOptionTemplate = import "L10.UI.CKeJuOptionTemplate"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local UIGrid = import "UIGrid"
local BoxCollider = import "UnityEngine.BoxCollider"
local Vector3 = import "UnityEngine.Vector3"
local Baby_Setting = import "L10.Game.Baby_Setting"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaParentExamWnd = class()

RegistClassMember(LuaParentExamWnd, "Question")
RegistClassMember(LuaParentExamWnd, "NumberLabel")
RegistClassMember(LuaParentExamWnd, "CountDownLabel")
RegistClassMember(LuaParentExamWnd, "TopicLabel")
RegistClassMember(LuaParentExamWnd, "TopicScrollview")
RegistClassMember(LuaParentExamWnd, "OptionGrid")
RegistClassMember(LuaParentExamWnd, "OptionTemplate")
RegistClassMember(LuaParentExamWnd, "InstructionLabel")
RegistClassMember(LuaParentExamWnd, "CloseButton")

RegistClassMember(LuaParentExamWnd, "AnswerList")
RegistClassMember(LuaParentExamWnd, "SelectedIndex")
RegistClassMember(LuaParentExamWnd, "QuestionEndTime")
RegistClassMember(LuaParentExamWnd, "CurrentTime")
RegistClassMember(LuaParentExamWnd, "CountDownTick")

function LuaParentExamWnd:Init()
	self:InitClassMembers()
	self:InitValues()
end

function LuaParentExamWnd:InitClassMembers()
	self.Question = self.transform:Find("Anchor/Question").gameObject
	self.NumberLabel = self.transform:Find("Anchor/Question/NumberLabel"):GetComponent(typeof(UILabel))
	self.CountDownLabel = self.transform:Find("Anchor/Question/CountDownLabel"):GetComponent(typeof(UILabel))

	self.TopicLabel = self.transform:Find("Anchor/Question/TopicScrollview/TopicLabel"):GetComponent(typeof(UILabel))
	self.TopicScrollview = self.transform:Find("Anchor/Question/TopicScrollview"):GetComponent(typeof(CUIRestrictScrollView))
	self.OptionGrid = self.transform:Find("Anchor/Question/OptionGrid"):GetComponent(typeof(UIGrid))
	self.OptionTemplate = self.transform:Find("Anchor/Question/OptionTemplate").gameObject
	self.OptionTemplate:SetActive(false)

	self.InstructionLabel = self.transform:Find("Anchor/InstructionLabel"):GetComponent(typeof(UILabel))
	self.CloseButton = self.transform:Find("Wnd_Bg_Secondary_3/CloseButton").gameObject
end

function LuaParentExamWnd:InitValues()

	self.AnswerList = {}
	self.SelectedIndex = 0

	self.QuestionEndTime = CServerTimeMgr.Inst.timeStamp + LuaBabyMgr.m_QuestionCountDown
	if self.CountDownTick ~= nil then
      UnRegisterTick(self.CountDownTick)
      self.CountDownTick=nil
    end

    local onCloseButtonClick = function (go)
    	self:TryToStopTest(go)
    end
    CommonDefs.AddOnClickListener(self.CloseButton,DelegateFactory.Action_GameObject(onCloseButtonClick),false)

	self:InitQuestion()
end

function LuaParentExamWnd:InitQuestion()
	CUICommonDef.ClearTransform(self.OptionGrid.transform)

	for i = 1, #LuaBabyMgr.m_AnswerList, 1 do
		local go = NGUITools.AddChild(self.OptionGrid.gameObject, self.OptionTemplate)
		self:InitOption(go, i, LuaBabyMgr.m_AnswerList[i])
		go:SetActive(true)
		table.insert(self.AnswerList, go)
	end
	
	self.OptionGrid:Reposition()

	self.TopicLabel.text = LuaBabyMgr.m_Question
	self.TopicScrollview:ResetPosition()

	self.NumberLabel.text = ""
	local setting = Baby_Setting.GetData()
	local totalQuestCount = setting.RearingTest_QuestionNum

	if LuaBabyMgr.m_CurrentQuestionIndex > 0 and LuaBabyMgr.m_CurrentQuestionIndex <= totalQuestCount then
		self.NumberLabel.text = SafeStringFormat3(LocalString.GetString("第%d/%d题"), LuaBabyMgr.m_CurrentQuestionIndex, totalQuestCount)
	end

	self.InstructionLabel.text = SafeStringFormat3(LocalString.GetString("[ACF8FF]得分超过%d分通过测试。[-]当前得分[FFFF00]%d[-]分。"), setting.RearingTest_PassScore, LuaBabyMgr.m_CurrentScore)

	local currentTime = self.QuestionEndTime - CServerTimeMgr.Inst.timeStamp - 1
	self.CountDownLabel.text = SafeStringFormat3(LocalString.GetString("%d秒"), math.ceil(currentTime))

	self.CountDownTick = RegisterTick(function ()
		local currentTime = self.QuestionEndTime - CServerTimeMgr.Inst.timeStamp - 1
		if currentTime >= 0 then
			self.CountDownLabel.text = SafeStringFormat3(LocalString.GetString("%d秒"), math.ceil(currentTime))
		end
	end, 300)
end

function LuaParentExamWnd:InitOption(go, index, answer)
	local optionLabel = ""
	if index == 1 then
		optionLabel = "A."
	elseif index == 2 then
		optionLabel = "B."
	elseif index == 3 then
		optionLabel = "C."
	elseif index == 4 then
		optionLabel = "D."
	end

	local answerOption = SafeStringFormat3("%s%s", optionLabel, answer)
	local optionScrpit = go.transform:GetComponent(typeof(CKeJuOptionTemplate))
	optionScrpit:Init(answerOption)

	local box = go.transform:GetComponent(typeof(BoxCollider))
	local sprite = go.transform:GetComponent(typeof(UISprite))

	box.size = Vector3(sprite.width, sprite.height, 1)

	local onAnswerClick = function (go)
		self:OnAnswerClick(go, index)
	end
	CommonDefs.AddOnClickListener(go,DelegateFactory.Action_GameObject(onAnswerClick), false)
end

function LuaParentExamWnd:OnAnswerClick(go, index)
	for i = 1, #self.AnswerList, 1 do
		local box = self.AnswerList[i]:GetComponent(typeof(BoxCollider))
		box.size = Vector3.one

		if go == self.AnswerList[i] then
			self.SelectedIndex = i
			Gac2Gas.RearingTestSubmitAnswer(LuaBabyMgr.m_CurrentQuestionIndex, self.SelectedIndex)
		end
	end
end

function LuaParentExamWnd:OnEnable()
	g_ScriptEvent:AddListener("RearingTestAnswerUpdate", self, "OnQuestionAnswerUpdate")
end

function LuaParentExamWnd:OnDisable()
	g_ScriptEvent:RemoveListener("RearingTestAnswerUpdate", self, "OnQuestionAnswerUpdate")
end

function LuaParentExamWnd:OnDestroy()
	if self.CountDownTick ~= nil then
      UnRegisterTick(self.CountDownTick)
      self.CountDownTick=nil
    end
end

function LuaParentExamWnd:OnQuestionAnswerUpdate()
	for  i = 1, #self.AnswerList, 1 do
		if i ~= LuaBabyMgr.m_RightAnswerIndex and i == self.SelectedIndex then -- 玩家选择了
			local option = self.AnswerList[i]:GetComponent(typeof(CKeJuOptionTemplate))
			if option then
				option:SetResult(false)
			end
		elseif i ~= LuaBabyMgr.m_RightAnswerIndex and i ~= self.SelectedIndex then
			local box = self.AnswerList[i]:GetComponent(typeof(BoxCollider))
			box.size = Vector3.one
		else
			local box = self.AnswerList[i]:GetComponent(typeof(BoxCollider))
			box.size = Vector3.one
			local option = self.AnswerList[i]:GetComponent(typeof(CKeJuOptionTemplate))
			if option then
				option:SetResult(true)
			end
		end
	end
end

function LuaParentExamWnd:TryToStopTest(go)
	local msg = g_MessageMgr:FormatMessage("Rearing_Test_Stop_Test_Confirm")
	MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function () 
		Gac2Gas.RearingTestStopTest()
		CUIManager.CloseUI(CLuaUIResources.ParentExamWnd)
	end), nil, nil, nil, false)
end

return LuaParentExamWnd