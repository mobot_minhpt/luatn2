-- Auto Generated!!
local CGuildHongbaoSetting = import "L10.UI.CGuildHongbaoSetting"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
--local HongBao_Play = import "L10.Game.HongBao_Play"
local Int32 = import "System.Int32"
local NGUIMath = import "NGUIMath"
local QnCheckBox = import "L10.UI.QnCheckBox"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
CGuildHongbaoSetting.m_Init_CS2LuaHook = function (this) 
    HongBao_Play.Foreach(function (key, data) 
        if data.Status == 0 then
            CommonDefs.ListAdd(this.m_Ids, typeof(UInt32), key)
            CommonDefs.ListAdd(this.m_Names, typeof(String), data.Name)
        end
    end)
    this.m_CheckBoxGroup:InitWithOptions(CommonDefs.ListToArray(this.m_Names), false, false)
    this.m_Background.height = math.floor((NGUIMath.CalculateRelativeWidgetBounds(this.m_Title.transform).size.y + NGUIMath.CalculateRelativeWidgetBounds(this.m_CheckBoxGroup.transform).size.y)) + CGuildHongbaoSetting.s_TotalPadding
    this.m_CheckBoxGroup.OnSelect = MakeDelegateFromCSFunction(this.OnSelected, MakeGenericClass(Action2, QnCheckBox, Int32), this)
    UIEventListener.Get(this.m_TipSprite.gameObject).onClick = DelegateFactory.VoidDelegate(function (go) 
        g_MessageMgr:ShowMessage("GUILD_HONGBAO_TARGET_TIP")
    end)
    this.m_Inited = true
end
