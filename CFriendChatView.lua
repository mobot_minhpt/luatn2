-- Auto Generated!!
local BoolDelegate = import "UIEventListener+BoolDelegate"
local CChatHelper = import "L10.UI.CChatHelper"
local CChatInputMgr = import "L10.UI.CChatInputMgr"
local CChatListBaseItem = import "L10.UI.CChatListBaseItem"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CFriendChatView = import "L10.UI.CFriendChatView"
local CFriendView = import "L10.UI.CFriendView"
local CIMMgr = import "L10.Game.CIMMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local Constants = import "L10.Game.Constants"
local CRecordingInfoMgr = import "L10.UI.CRecordingInfoMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CVoiceMsg = import "L10.Game.CVoiceMsg"
local DelegateFactory = import "DelegateFactory"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumEventType = import "EnumEventType"
local EnumVoicePlatform = import "L10.Game.EnumVoicePlatform"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local Gac2Gas = import "L10.Game.Gac2Gas"
local L10 = import "L10"
local LoadPicMgr = import "L10.Game.LoadPicMgr"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local OnClippingMoved = import "UIPanel+OnClippingMoved"
local String = import "System.String"
local System = import "System"
local UIEventListener = import "UIEventListener"
local UInt64 = import "System.UInt64"
local UIPanel = import "UIPanel"
local VoicePlayInfo = import "L10.Game.VoicePlayInfo"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local UILabel = import "UILabel"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local LuaTweenUtils = import "LuaTweenUtils"
local EnumNPCChatType = import "L10.Game.EnumNPCChatType"
local EnumNPCChatMsgLocation = import "L10.Game.EnumNPCChatMsgLocation"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local CChatListSystemIMItem = import "L10.UI.CChatListSystemIMItem"
CFriendChatView.m_Init_CS2LuaHook = function (this, groupId, isInBroadcastStatus, oppositeId, oppositeName, showHintRoot) 
    this.chatRoot:SetActive(not showHintRoot)
    this.chatHintRoot:SetActive(showHintRoot)
    if showHintRoot then
        return
    end

    this.curGroupId = groupId
    this.isInGroupSendStatus = isInBroadcastStatus
    this.friendChatHeader:SetActive(not isInBroadcastStatus)
    this.groupSendHeader:SetActive(isInBroadcastStatus)

    --bug fix 从邮件切换时这里会导致消息不能及时刷新
    --if (this.oppositeId == oppositeId)
    --    return;
    this.oppositeId = oppositeId
    this.oppositeName = oppositeName
    CFriendView.ChatOppositeId = oppositeId
    CLuaFriendChatView:Init(this)
    if not isInBroadcastStatus then
        if not CIMMgr.Inst:IsLoaded(oppositeId) then
            CIMMgr.Inst:LoadMsgs(oppositeId)
        end
        CIMMgr.Inst:FetchIMMsg(oppositeId)

        if oppositeId == CIMMgr.IM_SYSTEM_ID then
            this.oppsiteNameLabel.text = nil
            this.friendlinessLabel.gameObject:SetActive(false)
            this.groupSendMsgBtn:SetActive(CIMMgr.Inst:GroupExists(this.curGroupId))
            this.chatInput.gameObject:SetActive(false)
            this.hintLabel.gameObject:SetActive(true)
            this.gmBtn:SetActive(false)
        elseif LuaNPCChatMgr:IsChatNpc(oppositeId) then
            local displayName = oppositeName
            this.friendlinessLabel.gameObject:SetActive(false)
            this.oppsiteNameLabel.text = System.String.Format(LocalString.GetString("正在与{0}聊天"), displayName)
            this.groupSendMsgBtn:SetActive(false)
            this.gmBtn:SetActive(false)
        else
            local displayName = oppositeName
            if CIMMgr.Inst:IsMyFriend(this.oppositeId) then
                local basicInfo = CIMMgr.Inst:GetBasicInfo(this.oppositeId)
                if basicInfo ~= nil and not System.String.IsNullOrEmpty(basicInfo.NickName.StringData) then
                    displayName = System.String.Format("{0} - {1}", basicInfo.Name, basicInfo.NickName.StringData)
                end
            end
            local gmFlag = this.oppositeId == CIMMgr.IM_GM_ID and CIMMgr.Inst:GMReady()
            this.gmBtn:SetActive(gmFlag)
            this.oppsiteNameLabel.text = System.String.Format(LocalString.GetString("正在与{0}聊天"), displayName)
            this.friendlinessLabel.gameObject:SetActive(true)
            this.friendlinessLabel.text = System.String.Format(LocalString.GetString("[c][ACF8FF]友好度[-][/c] {0}"), CIMMgr.Inst:GetFriendliness(oppositeId))
            this.groupSendMsgBtn:SetActive(CIMMgr.Inst:GroupExists(this.curGroupId) and not gmFlag)
            Gac2Gas.QueryFriendliness(oppositeId)

            if LuaCreditScoreMgr:OpenFriendTalkReminder() then
                LuaCreditScoreMgr:PrepareLocalData()
                
                local syncTimeStamp = LuaCreditScoreMgr.m_otherCreditTimeData[oppositeId] and LuaCreditScoreMgr.m_otherCreditTimeData[oppositeId] or 0
                local isToday = false
                if syncTimeStamp == 0 then
                    isToday = false
                else
                    local beginTime =  CServerTimeMgr.ConvertTimeStampToZone8Time(syncTimeStamp)
                    isToday = (CServerTimeMgr.Inst:DayDiff(beginTime, CServerTimeMgr.Inst:GetZone8Time()) == 0)
                end
                if not isToday then
                    LuaCreditScoreMgr:RecordCheckCreditScoreTime(oppositeId, CServerTimeMgr.Inst.timeStamp)
                    LuaCreditScoreMgr:RequestPlayerCreditScoreData(oppositeId)
                end
            end
            
            this.chatInput.gameObject:SetActive(true)
            this.hintLabel.gameObject:SetActive(false)
        end
        Extensions.SetLocalPositionX(this.m_MovableButtonTable.transform, this.groupSendMsgBtn.activeSelf and this.m_LeftMovableButtonPos or this.m_RightMovableButtonPos)
        CLuaFriendChatView:SetSettingBtnPos()
    else
        this.groupNameLabel.text = CIMMgr.Inst:GetFriendGroupName(this.curGroupId)
        this.groupMemberNumLabel.text = System.String.Format(LocalString.GetString("共{0}人"), CIMMgr.Inst:GetFriendsByGroup(this.curGroupId).Count)
        this.chatInput.gameObject:SetActive(true)
        this.hintLabel.gameObject:SetActive(false)
        this.gmBtn:SetActive(false)
    end
    CLuaFriendChatView:UpdateSyncMsgBtnDisplay()
    this.m_MovableButtonTable:Reposition()

    this.tableView:Clear()
    this.tableView.dataSource = this
    CommonDefs.ListClear(this.allData)
    this.hasNewMsg = false

    this:SetNewMsgButtonVisible(false)
    CRecordingInfoMgr.CloseRecordingWnd(true)
    this.updateUUID = 0
    if not isInBroadcastStatus then
        this:UpdateChatMessages(true)
    else
        this:UpdateBroadcastMessages(true)
    end
    this:UpdateInputType()
end
CFriendChatView.m_UpdateInputType_CS2LuaHook = function (this) 
    this.keyboardInput:SetActive(not CFriendChatView.useVoiceInput)
    this.voiceButton.gameObject:SetActive(CFriendChatView.useVoiceInput)
    this.voiceButton.Text = LocalString.GetString("按下发言")
    this.switchButton:SetBackgroundSprite(CFriendChatView.useVoiceInput and Constants.ChatInput_KeyboardInputIcon or Constants.ChatInput_VoiceInputIcon)
end
CFriendChatView.m_CellForRowAtIndex_CS2LuaHook = function (this, index) 

    if index < 0 and index >= this.allData.Count then
        return nil
    end
    local cellIdentifier = nil
    local template = nil
    local channel = EChatPanel.Friend

    local data = this.allData[index]
    data.text = CLuaHouseMgr.TryFoldQishuMessage(data.text,data.uuid)

    local isOpp = false
    if data.usedForDisplayTime then
        cellIdentifier = "TimeSplitCell"
        template = this.timesplitTemplate
    elseif data.channel == EChatPanel.System then
        cellIdentifier = "SystemChannelCell"
        template = this.systemChannelTemplate
    elseif channel == EChatPanel.Friend and this.oppositeId == CIMMgr.IM_SYSTEM_ID and not this.isInGroupSendStatus then
        cellIdentifier = "SystemIMCell"
        template = this.systemIMTemplate
    elseif data.senderId == 0 then
        cellIdentifier = "SystemChannelCell"
        template = this.systemChannelTemplate
    elseif data.NPCChatType ~= EnumToInt(EnumNPCChatType.None) then
        cellIdentifier,template = CLuaFriendChatView:GetNPCChatGameObject(data)
    elseif data.isOpposite then
        if data.isCheat and (index == this.allData.Count - 1 or CLuaFriendChatView.m_LastShowTipTime == 0) then
           CLuaFriendChatView:ShowAntiFraudTip()
        end
        if data.isPic then
            cellIdentifier = "OppositePicTemplate"
            template = this.oppositePicTemplate
        elseif data.voiceAchievementId > 0 then
            cellIdentifier = "OppositeVoiceAchievementItem"
            template = this.oppositeVoiceAchievementItem
            isOpp = true
        else
            cellIdentifier = "OppositeTemplate"
            template = this.oppositeTemplate
        end
    else
        if data.isPic then
            cellIdentifier = "SelfPicTemplate"
            template = this.selfPicTemplate
        elseif data.voiceAchievementId > 0 then
            cellIdentifier = "SelfsiteVoiceAchievementItem"
            template = this.selfVoiceAchievementItem
            isOpp = true
        else
            cellIdentifier = "SelfTemplate"
            template = this.selfTemplate
        end
    end

    local cell = this.tableView:DequeueReusableCellWithIdentifier(cellIdentifier)
    if cell == nil then
        cell = this.tableView:AllocNewCellWithIdentifier(template, cellIdentifier)
    end

    if data.voiceAchievementId > 0 then
        local chatListItem = CommonDefs.GetComponent_GameObject_Type(cell, typeof(CChatListBaseItem))
        chatListItem:Init(this.allData[index])
        chatListItem.onReLayoutDelegate = MakeDelegateFromCSFunction(this.OnChatListItemReLayout, MakeGenericClass(Action1, CChatListBaseItem), this)
        local luascrip = CommonDefs.GetComponent_GameObject_Type(cell, typeof(CCommonLuaScript))
        if luascrip then
            luascrip.m_LuaSelf:InitItem(data,isOpp)
            return cell
        end
        
    end
    if data.NPCChatType == EnumToInt(EnumNPCChatType.None) then       
        local chatListItem = CommonDefs.GetComponent_GameObject_Type(cell, typeof(CChatListBaseItem))
        chatListItem:Init(data)
        chatListItem.onReLayoutDelegate = MakeDelegateFromCSFunction(this.OnChatListItemReLayout, MakeGenericClass(Action1, CChatListBaseItem), this)
    else
        CLuaFriendChatView:InitNPCChatItem(cell,data)
    end
    return cell
end
CFriendChatView.m_AppendChatMessages_CS2LuaHook = function (this, messages, forceDisplay) 


    if messages == nil or messages.Length == 0 then
        this.deleteMsgBtn:SetActive(this.allData.Count > 0 and this.oppositeId ~= CIMMgr.IM_SYSTEM_ID)
        this.deleteGroupMsgBtn:SetActive(this.deleteMsgBtn.activeSelf)
        this.m_MovableButtonTable:Reposition()
        return
    end


    this.deleteMsgBtn:SetActive(true and this.oppositeId ~= CIMMgr.IM_SYSTEM_ID and not LuaNPCChatMgr:IsChatNpc(this.oppositeId))
    this.deleteGroupMsgBtn:SetActive(this.deleteMsgBtn.activeSelf)
    this.m_MovableButtonTable:Reposition()


    local lastChildIsVisible = (this:NumberOfRows() == 0 or (this:NumberOfRows() > 0 and this.tableView:IsVisible(this:NumberOfRows() - 1)))

    if lastChildIsVisible or forceDisplay then
        this:SetNewMsgButtonVisible(false)
    else
        for i = messages.Length - 1, 0, - 1 do
            if not messages[i].usedForDisplayTime then
                this.unreadCountOfCurChannel = this.unreadCountOfCurChannel + 1
            end
        end
        this:SetNewMsgButtonVisible(true)
    end
    CommonDefs.ListAddRange(this.allData, messages)

    if CommonDefs.IsInWifiNetwork then
        for i = messages.Length - 1, 0, - 1 do
            this:PreDownloadVoice(messages[i])
            --预先下载声音文件
        end
    end
    if lastChildIsVisible or forceDisplay then
        --从底部加载
        this.tableView:LoadDataFromTail()
    else
        --原位置刷新
        this.tableView:UpdateIndexes(0)
    end
end

CFriendChatView.m_Start_CS2LuaHook = function (this) 
    UIEventListener.Get(this.switchButton.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.switchButton.gameObject).onClick, MakeDelegateFromCSFunction(this.OnSwitchButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.voiceButton.gameObject).onPress = CommonDefs.CombineListner_BoolDelegate(UIEventListener.Get(this.voiceButton.gameObject).onPress, MakeDelegateFromCSFunction(this.OnVoiceButtonPress, BoolDelegate, this), true)
    UIEventListener.Get(this.emoticonButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.emoticonButton).onClick, MakeDelegateFromCSFunction(this.OnEmoticonButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.newMsgButton.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.newMsgButton.gameObject).onClick, MakeDelegateFromCSFunction(this.OnNewMsgButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.groupSendMsgBtn.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.groupSendMsgBtn.gameObject).onClick, MakeDelegateFromCSFunction(this.OnGroupSendMsgButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.deleteMsgBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.deleteMsgBtn).onClick, MakeDelegateFromCSFunction(this.OnDeleteMsgButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.deleteGroupMsgBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.deleteGroupMsgBtn).onClick, MakeDelegateFromCSFunction(this.OnDeleteGroupMsgButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.gmBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.gmBtn).onClick, MakeDelegateFromCSFunction(this.OnGMBtnClick, VoidDelegate, this), true)
    this.chatInput.OnSend = CommonDefs.CombineListner_Action_string(this.chatInput.OnSend, MakeDelegateFromCSFunction(this.OnSend, MakeGenericClass(Action1, String), this), true)
    CommonDefs.GetComponent_Component_Type(this.tableView.scrollView, typeof(UIPanel)).onClipMove = CommonDefs.CombineListner_OnClippingMoved(CommonDefs.GetComponent_Component_Type(this.tableView.scrollView, typeof(UIPanel)).onClipMove, MakeDelegateFromCSFunction(this.OnClipMove, OnClippingMoved, this), true)
end
CFriendChatView.m_OnEnable_CS2LuaHook = function (this) 

    CLuaFriendChatView:OnEnable(this)
    EventManager.AddListenerInternal(EnumEventType.RecvIMMsg, MakeDelegateFromCSFunction(this.OnRecvIMMsg, MakeGenericClass(Action1, UInt64), this))
    EventManager.AddListenerInternal(EnumEventType.OnFriendlinessUpdate, MakeDelegateFromCSFunction(this.UpdateFriendliness, MakeGenericClass(Action1, UInt64), this))
    EventManager.AddListenerInternal(EnumEventType.OnWithdrawPlayerIMMsg, MakeDelegateFromCSFunction(this.OnWithdrawPlayerMsg, MakeGenericClass(Action1, UInt64), this))
    EventManager.AddListener(EnumEventType.BroadcastMsgToFriendsSuccess, MakeDelegateFromCSFunction(this.OnBroadcastMsgToFriendsSuccess, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.ClearIMMsg, MakeDelegateFromCSFunction(this.OnClearIMMsg, MakeGenericClass(Action1, UInt64), this))
    EventManager.AddListenerInternal(EnumEventType.DeleteIMMsg, MakeDelegateFromCSFunction(this.OnDeleteIMMsg, MakeGenericClass(Action1, UInt64), this))
    EventManager.AddListener(EnumEventType.ClearBroadcastMsg, MakeDelegateFromCSFunction(this.OnClearBroadcastMsg, Action0, this))

end
CFriendChatView.m_OnDisable_CS2LuaHook = function (this) 
    CLuaFriendChatView:OnDisable()
    CFriendView.ChatOppositeId = System.UInt64.MaxValue

    EventManager.RemoveListenerInternal(EnumEventType.RecvIMMsg, MakeDelegateFromCSFunction(this.OnRecvIMMsg, MakeGenericClass(Action1, UInt64), this))
    EventManager.RemoveListenerInternal(EnumEventType.OnFriendlinessUpdate, MakeDelegateFromCSFunction(this.UpdateFriendliness, MakeGenericClass(Action1, UInt64), this))
    EventManager.RemoveListenerInternal(EnumEventType.OnWithdrawPlayerIMMsg, MakeDelegateFromCSFunction(this.OnWithdrawPlayerMsg, MakeGenericClass(Action1, UInt64), this))
    EventManager.RemoveListener(EnumEventType.BroadcastMsgToFriendsSuccess, MakeDelegateFromCSFunction(this.OnBroadcastMsgToFriendsSuccess, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.ClearIMMsg, MakeDelegateFromCSFunction(this.OnClearIMMsg, MakeGenericClass(Action1, UInt64), this))
    EventManager.RemoveListenerInternal(EnumEventType.DeleteIMMsg, MakeDelegateFromCSFunction(this.OnDeleteIMMsg, MakeGenericClass(Action1, UInt64), this))
    EventManager.RemoveListener(EnumEventType.ClearBroadcastMsg, MakeDelegateFromCSFunction(this.OnClearBroadcastMsg, Action0, this))
end

CFriendChatView.m_OnWithdrawPlayerMsg_CS2LuaHook = function (this, playerId) 

    if (CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.Id == playerId) or (playerId == this.oppositeId) then
        if this.chatRoot.activeSelf and not this.isInGroupSendStatus then
            this:Init(this.curGroupId, this.isInGroupSendStatus, this.oppositeId, this.oppositeName, false)
        end
    end
end
CFriendChatView.m_OnSend_WithFilterOption_CS2LuaHook = function (this, message, notDoFilter) 

    if this.isInGroupSendStatus then
        if CIMMgr.Inst:GroupExists(this.curGroupId) then
            CIMMgr.Inst:BroadcastMsgToFriends(Table2ArrayWithCount({this.curGroupId}, 1, MakeArrayClass(System.Int32)), message, notDoFilter)
        end
    else
        CChatHelper.SendMsgWithFilterOption(EChatPanel.Friend, message, this.oppositeId, notDoFilter)
    end
    this.chatInput:Clear()
end
CFriendChatView.m_OnVoiceButtonPress_CS2LuaHook = function (this, go, pressed) 

    if pressed then
        CRecordingInfoMgr.ShowFriendRecordingWnd(this.oppositeId, this.isInGroupSendStatus and this.curGroupId or 0, go)
        this.voiceButton.Text = LocalString.GetString("松开结束")
    else
        CRecordingInfoMgr.CloseRecordingWnd(not go:Equals(CUICommonDef.SelectedUI))
        this.voiceButton.Text = LocalString.GetString("按下发言")
    end
end
CFriendChatView.m_OnEmoticonButtonClick_CS2LuaHook = function (this, go) 

    if this.isInGroupSendStatus and CIMMgr.Inst:GroupExists(this.curGroupId) then
        CChatInputMgr.ShowChatInputWnd(CChatInputMgr.EParentType.FriendGroup, this, go, EChatPanel.Friend, 0, 0)
    else
        if LoadPicMgr.Inst:CheckSendFriendPicAvaliable(this.oppositeId) then
            CChatInputMgr.ShowChatInputWnd(CChatInputMgr.EParentType.Friend, this, go, EChatPanel.Friend, this.oppositeId, 0)
        else
            CChatInputMgr.ShowChatInputWnd(CChatInputMgr.EParentType.FriendNormal, this, go, EChatPanel.Friend, this.oppositeId, 0)
        end
    end
end
CFriendChatView.m_PreDownloadVoice_CS2LuaHook = function (this, data) 

    local voiceMsg = CVoiceMsg.Parse(data.text)
    if voiceMsg ~= nil then
        if not System.IO.File.Exists(L10.Game.HTTPHelper.CACHE_DIR .. voiceMsg.VoiceId) then
            L10.Game.HTTPHelper.DownloadVoice(CreateFromClass(VoicePlayInfo, voiceMsg.VoiceId, EnumVoicePlatform.Default, nil), nil)
        end
    end
end
CFriendChatView.m_SetNewMsgButtonVisible_CS2LuaHook = function (this, visible) 

    if this.oppositeId == CIMMgr.IM_GM_ID then
        this.deleteMsgBtn:SetActive(false)
        this.m_MovableButtonTable:Reposition()
    end

    if visible then
        if not this.newMsgButton.gameObject.activeSelf then
            this.newMsgButton.gameObject:SetActive(true)
        end
        this.newMsgButton.Text = tostring(this.unreadCountOfCurChannel) .. LocalString.GetString("条未读消息")
    else
        this.unreadCountOfCurChannel = 0
        if this.newMsgButton.gameObject.activeSelf then
            this.newMsgButton.gameObject:SetActive(false)
        end
    end
end
CFriendChatView.m_OnNewMsgButtonClick_CS2LuaHook = function (this, go) 

    this:SetNewMsgButtonVisible(false)
    this.tableView:LoadDataFromTail()
end
CFriendChatView.m_OnDeleteMsgButtonClick_CS2LuaHook = function (this, go) 

    if this.oppositeId == CIMMgr.IM_SYSTEM_ID then
        return
    end

    if CIMMgr.Inst:GetIMMsgsByID(this.oppositeId).Count == 0 then
        return
    end

    local msg = g_MessageMgr:FormatMessage("Clear_IM_Message_Confirm_Text", this.oppositeName)
    local playerId = this.oppositeId
    MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function () 
        CIMMgr.Inst:ClearMsgs(playerId)
    end), nil, LocalString.GetString("清空"), nil, false)
end
CFriendChatView.m_OnDeleteGroupMsgButtonClick_CS2LuaHook = function (this, go) 

    --清空群发消息
    local msg = LocalString.GetString("是否清空群发消息")
    -- MessageMgr.Inst.FormatMessage("Clear_Broadcast_Message_Confirm_Text");
    MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function () 
        CIMMgr.Inst:ClearBroadcastMsgs()
    end), nil, LocalString.GetString("清空"), nil, false)
end
CFriendChatView.m_OnGMBtnClick_CS2LuaHook = function (this, go) 
    if CIMMgr.Inst:GMReady() then
        Gac2Gas.QueryMyGmId()
    end
end
CFriendChatView.m_OnClipMove_CS2LuaHook = function (this, panel) 

    local lastChildIsVisible = (this:NumberOfRows() == 0 or (this:NumberOfRows() > 0 and this.tableView:IsVisible(this:NumberOfRows() - 1)))

    if lastChildIsVisible then
        this:SetNewMsgButtonVisible(false)
    end
end
CFriendChatView.m_UpdateFriendliness_CS2LuaHook = function (this, playerId) 

    if playerId == this.oppositeId then
        this.friendlinessLabel.text = System.String.Format(LocalString.GetString("[c][ACF8FF]友好度[-][/c] {0}"), CIMMgr.Inst:GetFriendliness(this.oppositeId))
    end
end
CFriendChatView.m_OnChatListItemReLayout_CS2LuaHook = function (this, baseItem) 

    if baseItem == nil or this == nil or not this.enabled then
        return
    end
    this.tableView:UpdateLayout(baseItem.transform)
end

CLuaFriendChatView = {}
CLuaFriendChatView.m_AntiFraudTip = nil
CLuaFriendChatView.m_View = nil
CLuaFriendChatView.m_SettingBtn = nil
CLuaFriendChatView.m_Interval = 0 --聊天诈骗提醒message触发CD
CLuaFriendChatView.m_LastShowTipTime = 0 -- 上次显示防诈骗悬浮窗的时间
CLuaFriendChatView.m_BeforeY = 85
CLuaFriendChatView.m_AfterY = 0
CLuaFriendChatView.m_TweenInterval = 0.5
CLuaFriendChatView.m_ContentBGHeight = {700,600,510}    -- ContentBG面板的Height,无选项时700,<=2个选项600,>=2 且 <=4 510
-- NPC聊天相关
CLuaFriendChatView.NPCChatPanel = nil   -- NPCGroup中输入面板
CLuaFriendChatView.NPCChatPanelScript = nil 
CLuaFriendChatView.ChatInput = nil  --原输入面板
CLuaFriendChatView.ContentBg = nil
CLuaFriendChatView.ScrollView = nil
-- 一些新增Templates
CLuaFriendChatView.NPCSendItem = nil
CLuaFriendChatView.NPCVoiceItem = nil
CLuaFriendChatView.OppsiteVideoItem = nil
CLuaFriendChatView.EventSplitItem = nil
CLuaFriendChatView.NPCPicItem = nil

function CLuaFriendChatView:Init(view)
    self.m_View = view
    self.m_AntiFraudTip:SetActive(false)
    self.m_LastShowTipTime = 0
    self.NPCChatPanel:SetActive(false)
    self.ChatInput:SetActive(true)
    if not LuaNPCChatMgr:EnableNpcChat() then return end
    local isNpc = LuaNPCChatMgr:IsChatNpc(self.m_View.oppositeId)
    LuaNPCChatMgr:OnChangeFriendChatView(isNpc,self.m_View.oppositeId,self.m_View.oppositeName)
    self:CheckNPCChatStatus(true)
end

function CLuaFriendChatView:GetNPCChatGameObject(data)
    if data.usedForDisplayStorySplit ~= EnumToInt(EnumNPCChatMsgLocation.Default) or data.NPCChatType == EnumToInt(EnumNPCChatType.AddFriendTip) then
        return "EventSplitItem",self.EventSplitItem
    elseif data.NPCChatType == EnumToInt(EnumNPCChatType.Text) then
        if data.isOpposite then
            return "OppositeTemplate",self.m_View.oppositeTemplate
        else
            return "SelfTemplate",self.m_View.selfTemplate
        end
    elseif data.NPCChatType == EnumToInt(EnumNPCChatType.Pic) then
        return "NPCPicItem",self.NPCPicItem
    elseif data.NPCChatType == EnumToInt(EnumNPCChatType.Video) then
        return "OppsiteVideoItem",self.OppsiteVideoItem
    elseif data.NPCChatType == EnumToInt(EnumNPCChatType.Voice) then
        return "NPCVoiceItem",self.NPCVoiceItem
    elseif data.NPCChatType == EnumToInt(EnumNPCChatType.Event) then
        return "NPCSendItem",self.NPCSendItem
    end
end

function CLuaFriendChatView:InitNPCChatItem(cell,data)
    if data.usedForDisplayStorySplit ~= EnumToInt(EnumNPCChatMsgLocation.Default) then 
        local sc = cell.transform:GetComponent(typeof(CCommonLuaScript))
        if sc then
            local LuaScript = sc.m_LuaSelf
            LuaScript:Init(data)
        end
        return
    end
    if data.usedForDisplayTime or data.NPCChatType == EnumToInt(EnumNPCChatType.Text) then
        local chatListItem = CommonDefs.GetComponent_GameObject_Type(cell, typeof(CChatListBaseItem))
        chatListItem:Init(data)
        chatListItem.onReLayoutDelegate = MakeDelegateFromCSFunction(self.m_View.OnChatListItemReLayout, MakeGenericClass(Action1, CChatListBaseItem), self.m_View)
    else
        local sc = cell.transform:GetComponent(typeof(CCommonLuaScript))
        if sc then 
            local LuaScript = sc.m_LuaSelf
            LuaScript:Init(data)
        end
        self:OnUpdateFriendChatListItemReLayout(cell.transform)
    end
end

function CLuaFriendChatView:ShowAntiFraudTip()
    local nowTime = CServerTimeMgr.Inst.timeStamp 
    local ShowInfo = self.m_AntiFraudTip.transform:Find("Info").gameObject
    if self.m_LastShowTipTime == 0 or nowTime - self.m_LastShowTipTime >= self.m_Interval then
        self.m_LastShowTipTime = nowTime
        Extensions.SetLocalPositionY(ShowInfo.transform, self.m_BeforeY)
        self.m_AntiFraudTip:SetActive(true)
        LuaTweenUtils.DOKill(ShowInfo.transform, false)
        local ShowTween = LuaTweenUtils.DOLocalMoveY(ShowInfo.transform,self.m_AfterY,self.m_TweenInterval,false)
    end
end

function CLuaFriendChatView:ShowSettingWnd()
    CUIManager.ShowUI(CLuaUIResources.FriendChatSettingWnd)
end

function CLuaFriendChatView:SetSettingBtnPos()
    if not self.m_View then return end
    local gmFlag = self.m_View.oppositeId == CIMMgr.IM_GM_ID and CIMMgr.Inst:GMReady()

    self.m_SettingBtn:SetActive(not gmFlag and not LuaNPCChatMgr:IsChatNpc(self.m_View.oppositeId))
end

function CLuaFriendChatView:UpdateSyncMsgBtnDisplay()
    if not self.m_View then return end
    if not CLuaIMMgr:IsEnableSyncImRecord() or not LuaWelfareMgr:HasBigMonthCard() then
        self.m_UploadBtn:SetActive(false)
        self.m_DownloadBtn:SetActive(false)
        self.m_HintUploadBtn:SetActive(false)
        self.m_HintDownloadBtn:SetActive(false)
        self:StopCheckSyncRecordTick()
    else
        local visible = not self.m_View.isInGroupSendStatus
        self.m_UploadBtn:SetActive(visible)
        self.m_DownloadBtn:SetActive(visible)
        self.m_HintUploadBtn:SetActive(visible)
        self.m_HintDownloadBtn:SetActive(visible)
        if visible then
            self:StartCheckSyncRecordTick()
        else
            self:StopCheckSyncRecordTick()
        end
    end
end

function CLuaFriendChatView:OnUpdateFriendChatListItemReLayout(CellTransform)
    if not CellTransform then return end
    self.m_View.tableView:UpdateLayout(CellTransform)
end

function CLuaFriendChatView:OnUpdateNPCChatStatus()
    self:CheckNPCChatStatus(false)
end

function CLuaFriendChatView:CheckNPCChatStatus(autoresponse)
    if not LuaNPCChatMgr.Id2ChatStatus or not LuaNPCChatMgr.Id2MessageNeedFinish then
        self.NPCChatPanel:SetActive(false)
        self.ChatInput:SetActive(true)
        self:ResizeContentBGHeight(EnumNPCChatStatus.FreeChat)
        return 
    end
    local status = LuaNPCChatMgr.Id2ChatStatus[self.m_View.oppositeId]
    local msgid = LuaNPCChatMgr.Id2MessageNeedFinish[self.m_View.oppositeId]
    if not LuaNPCChatMgr:IsChatNpc(self.m_View.oppositeId) or not status or status == EnumNPCChatStatus.FreeChat then
        self.NPCChatPanel:SetActive(false)
        self.ChatInput:SetActive(true)
    elseif status == EnumNPCChatStatus.WaitForNPC then
        self.NPCChatPanel:SetActive(true)
        self.ChatInput:SetActive(false)
        self.NPCChatPanelScript.m_LuaSelf:ShowWaitForNPCInput()
        if autoresponse then
            LuaNPCChatMgr:WaitForNPC(msgid,self.m_View.oppositeId)
        end
    elseif status == EnumNPCChatStatus.WaitForFinishEvent then
        self.NPCChatPanel:SetActive(true)
        self.ChatInput:SetActive(false)
        self.NPCChatPanelScript.m_LuaSelf:ShowWaitForPlayerFinishEvent(msgid,self.m_View.oppositeName)
    elseif status == EnumNPCChatStatus.WaitForPlayer then
        self.NPCChatPanel:SetActive(true)
        self.ChatInput:SetActive(false)
        self.NPCChatPanelScript.m_LuaSelf:ShowOptions(msgid,self.m_View.oppositeId)
    end
    self:ResizeContentBGHeight(status)
end

function CLuaFriendChatView:ResizeContentBGHeight(status)
    local ctrl = self.ContentBg:GetComponent(typeof(UIWidget))
    if status and status == EnumNPCChatStatus.WaitForPlayer then
        local opcount = 0
        local ops = self.NPCChatPanelScript.m_LuaSelf.m_OptionsList
        if ops then
            opcount = #ops
        end
        if opcount == 0 then
            ctrl.height = self.m_ContentBGHeight[1]
        elseif opcount <= 2 then
            ctrl.height = self.m_ContentBGHeight[2]
        elseif opcount > 2 then
            ctrl.height = self.m_ContentBGHeight[3]
        end
    else
        ctrl.height = self.m_ContentBGHeight[1]
    end
    self.ScrollView.panel:ResetAndUpdateAnchors()
end

function CLuaFriendChatView:OnEnable(view)
    self.m_View = view
    self.m_Interval = GameSetting_Common.GetData().ShowAntiFraudTipCD
    -- 聊天设置按钮
    self.m_SettingBtn = self.m_View.transform:Find("Chat/FriendChatHeader/ButtonTable/SettingBtn").gameObject
    self.m_UploadBtn = self.m_View.transform:Find("Chat/FriendChatHeader/ButtonTable/UploadBtn").gameObject
    self.m_DownloadBtn = self.m_View.transform:Find("Chat/FriendChatHeader/ButtonTable/DownloadBtn").gameObject
    self.m_HintUploadBtn = self.m_View.transform:Find("Hint/UploadBtn").gameObject
    self.m_HintDownloadBtn = self.m_View.transform:Find("Hint/DownloadBtn").gameObject
    -- NPC聊天Panel
    self.NPCChatPanel = self.m_View.transform:Find("Chat/NPCChatPanel").gameObject
    self.NPCChatPanel:SetActive(false)
    self.NPCChatPanelScript = self.NPCChatPanel:GetComponent(typeof(CCommonLuaScript))
    self.ContentBg = self.m_View.transform:Find("Chat/ContentBg").gameObject
    self.ChatInput = self.m_View.transform:Find("Chat/ChatInput").gameObject
    self.ScrollView = self.m_View.transform:Find("Chat/ScrollView"):GetComponent(typeof(UIScrollView))
    -- 新增Templates
    self.NPCSendItem = self.m_View.transform:Find("Chat/Templates/NPCSendItem").gameObject
    self.NPCVoiceItem = self.m_View.transform:Find("Chat/Templates/NPCVoiceItem").gameObject
    self.OppsiteVideoItem = self.m_View.transform:Find("Chat/Templates/OppsiteVideoItem").gameObject
    self.EventSplitItem = self.m_View.transform:Find("Chat/Templates/EventSplitItem").gameObject
    self.NPCPicItem = self.m_View.transform:Find("Chat/Templates/NPCPicItem").gameObject

    local settingBtn = self.m_View.transform:Find("Hint/SettingBtn").gameObject
    UIEventListener.Get(self.m_SettingBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:ShowSettingWnd()
    end)
    UIEventListener.Get(settingBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:ShowSettingWnd()
    end)

    UIEventListener.Get(self.m_UploadBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnUploadButtonClick(go)
    end)
    UIEventListener.Get(self.m_HintUploadBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnUploadButtonClick(go)
    end)
    UIEventListener.Get(self.m_DownloadBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnDownloadButtonClick(go)
    end)
    UIEventListener.Get(self.m_HintDownloadBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnDownloadButtonClick(go)
    end)

    -- 防诈骗悬浮窗
    self.m_AntiFraudTip = self.m_View.transform:Find("Chat/FriendChatHeader/AntiFraudTip").gameObject
    local AntiFraudCloseBtn = self.m_AntiFraudTip.transform:Find("Info/CloseButton").gameObject
    local AntiFraudText = self.m_AntiFraudTip.transform:Find("Info/Label"):GetComponent(typeof(UILabel))
    AntiFraudText.text = g_MessageMgr:FormatMessage("AntiFraud_Tip")
    UIEventListener.Get(AntiFraudCloseBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        local ShowInfo = self.m_AntiFraudTip.transform:Find("Info").gameObject
        Extensions.SetLocalPositionY(ShowInfo.transform, self.m_AfterY)
        LuaTweenUtils.DOKill(ShowInfo.transform, false)
        local ShowTween = LuaTweenUtils.DOLocalMoveY(ShowInfo.transform,self.m_BeforeY,self.m_TweenInterval,false)
        LuaTweenUtils.OnComplete(ShowTween, function ()
            self.m_AntiFraudTip:SetActive(false)
        end)
    end)

    g_ScriptEvent:AddListener("UpdateFriendChatListItemReLayout", self, "OnUpdateFriendChatListItemReLayout")
    g_ScriptEvent:AddListener("UpdateNPCChatStatus",self,"OnUpdateNPCChatStatus")
    g_ScriptEvent:AddListener("UpdateIMMsg",self,"OnUpdateIMMsg")
    g_ScriptEvent:AddListener("QiShuFoldExpand",self,"OnQiShuFoldExpand")
    g_ScriptEvent:AddListener("GetPlayerCreditScore",self,"OnGetPlayerCreditScore")

    self:UpdateSyncMsgBtnDisplay()
    view.m_MovableButtonTable:Reposition()
end

function CLuaFriendChatView:OnDisable()
    self.m_View = nil
    LuaNPCChatMgr:OnChangeFriendChatView(false,0,"")
    g_ScriptEvent:RemoveListener("UpdateFriendChatListItemReLayout", self, "OnUpdateFriendChatListItemReLayout")
    g_ScriptEvent:RemoveListener("UpdateNPCChatStatus",self,"OnUpdateNPCChatStatus")
    g_ScriptEvent:RemoveListener("UpdateIMMsg",self,"OnUpdateIMMsg")
    g_ScriptEvent:RemoveListener("QiShuFoldExpand",self,"OnQiShuFoldExpand")
    g_ScriptEvent:RemoveListener("GetPlayerCreditScore",self,"OnGetPlayerCreditScore")

    self:StopCheckSyncRecordTick()
end

function CLuaFriendChatView:OnGetPlayerCreditScore(pid, creditScore)
    if self.m_View.oppositeId == pid then
        if creditScore < CreditScore_Setting.GetData().FriendChatNoticeScore then
            g_MessageMgr:ShowMessage("CreditScore_LowNotice_FriendChat", self.m_View.oppositeName)
        end
    end
end


function CLuaFriendChatView:OnQiShuFoldExpand(uuid,isExpand)
    local imMsg = CIMMgr.Inst:GetIMMsgByUUID(uuid)
    local index = 0
    if imMsg and self.m_View then
        for i=0, self.m_View.allData.Count - 1 do
            local data = self.m_View.allData[i]
            if data.uuid == uuid and not data.usedForDisplayTime then
                data.text = CLuaHouseMgr.TryFoldQishuMessage(imMsg.Content,uuid,isExpand)
                self.m_View.allData[i] = data
                index = i
                break
            end
        end
    end

    if self.m_View.tableView then
        --self.m_View.tableView:Clear()
        self.m_View.tableView:LoadData(0, false)
        self.m_View.tableView.SelectedIndex = index
    end
end

function CLuaFriendChatView:OnUpdateIMMsg(args)
    local uuid = tonumber(args[0])
    local imMsg = CIMMgr.Inst:GetIMMsgByUUID(uuid)
    if imMsg and self.m_View then
        for i=0, self.m_View.allData.Count - 1 do
            local data = self.m_View.allData[i]
            if data.uuid == uuid and not data.usedForDisplayTime then
                data.text = CChatLinkMgr.TranslateToNGUIText(imMsg.Content, false)
                self.m_View.allData[i] = data
                break
            end
        end
    end
end

---------------------------------------------
-- Sync IM Record
---------------------------------------------
function CLuaFriendChatView:OnUploadButtonClick(go)
    CLuaIMMgr:TryUploadImRecord()
end

function CLuaFriendChatView:OnDownloadButtonClick(go)
    CLuaIMMgr:TryDownloadImRecord()
end

function CLuaFriendChatView:StartCheckSyncRecordTick()
    self:StopCheckSyncRecordTick()
    self.m_CheckSyncRecordTick = RegisterTick(function()
        self:DoCheckSyncRecord()
    end, 100)
end

function CLuaFriendChatView:StopCheckSyncRecordTick()
    if self.m_CheckSyncRecordTick then
        UnRegisterTick(self.m_CheckSyncRecordTick)
        self.m_CheckSyncRecordTick = nil
    end
end

function CLuaFriendChatView:DoCheckSyncRecord()
    local uploadBtn = nil
    local downloadBtn = nil
    if self.m_UploadBtn.activeInHierarchy then
        uploadBtn = self.m_UploadBtn
        downloadBtn = self.m_DownloadBtn
    elseif self.m_HintUploadBtn.activeInHierarchy then
        uploadBtn = self.m_HintUploadBtn
        downloadBtn = self.m_HintDownloadBtn
    end
    local uploadResultIcon = uploadBtn.transform:Find("Result"):GetComponent(typeof(UISprite))
    local uploadProgressWidget = uploadBtn.transform:Find("Progress"):GetComponent(typeof(UIWidget))
    local downloadResultIcon = downloadBtn.transform:Find("Result"):GetComponent(typeof(UISprite))
    local downloadProgressWidget = downloadBtn.transform:Find("Progress"):GetComponent(typeof(UIWidget))
    local successSpriteName = "common_log_01"
    local failedSpriteName = "common_log_03"

    uploadResultIcon.gameObject:SetActive(false)
    uploadProgressWidget.fillAmount = 0
    if CLuaIMMgr.m_UploadImRecordStatus == EnumIMSyncImRecordStatus.eSuccess then
        uploadResultIcon.gameObject:SetActive(true)
        uploadResultIcon.spriteName = successSpriteName
    elseif CLuaIMMgr.m_UploadImRecordStatus == EnumIMSyncImRecordStatus.eFailed then
        uploadResultIcon.gameObject:SetActive(true)
        uploadResultIcon.spriteName = failedSpriteName
    elseif CLuaIMMgr.m_UploadImRecordStatus == EnumIMSyncImRecordStatus.eStarted then
        uploadProgressWidget.fillAmount = CLuaIMMgr.m_UploadImRecordProgress
    end

    downloadResultIcon.gameObject:SetActive(false)
    downloadProgressWidget.fillAmount = 0
    if CLuaIMMgr.m_DownloadImRecordStatus == EnumIMSyncImRecordStatus.eSuccess then
        downloadResultIcon.gameObject:SetActive(true)
        downloadResultIcon.spriteName = successSpriteName
    elseif CLuaIMMgr.m_DownloadImRecordStatus == EnumIMSyncImRecordStatus.eFailed then
        downloadResultIcon.gameObject:SetActive(true)
        downloadResultIcon.spriteName = failedSpriteName
    elseif CLuaIMMgr.m_DownloadImRecordStatus == EnumIMSyncImRecordStatus.eStarted then
        downloadProgressWidget.fillAmount = CLuaIMMgr.m_DownloadImRecordProgress
    end
end