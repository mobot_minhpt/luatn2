local CScene = import "L10.Game.CScene"
local Utility = import "L10.Engine.Utility"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CClientNpc = import "L10.Game.CClientNpc"
local CTrackMgr = import "L10.Game.CTrackMgr"

LuaChristmas2023Mgr = {}

-- ===== 主界面 =====

LuaChristmas2023Mgr.m_IsMainWndOpened = false -- 是否已经打开过主界面

function LuaChristmas2023Mgr:TrackToXueHua()
    local guildid = Constants.GuildSceneId
    local hangzhouid = Constants.HangZhouID
    local sceneid = CScene.MainScene and CScene.MainScene.SceneTemplateId or 0
    if sceneid == guildid or sceneid == hangzhouid then
        local npcidlist = nil
        if sceneid == guildid then
            npcidlist = Christmas2023_Setting.GetData().GuildXueHuaNpcId
        elseif sceneid == hangzhouid then
            npcidlist = Christmas2023_Setting.GetData().HangZhouXueHuaNpcId
        end
        if not npcidlist then return end
        local npciddict = {}
        for i = 0, npcidlist.Length - 1 do
            npciddict[npcidlist[i]] = true
        end
        if not CClientMainPlayer.Inst then return end
        local playerpos = CClientMainPlayer.Inst.RO.Position
        local MinPos = nil
        local MinNpcId = nil
        local MinDist = 1e100
        CClientObjectMgr.Inst:ForeachObject(DelegateFactory.Action_CClientObject(function(obj)
            if obj and TypeIs(obj, typeof(CClientNpc)) and npciddict[obj.TemplateId] then
                local npcpos = obj.RO.transform.position
                local distance = Vector3.Distance(playerpos, npcpos)
                if not MinPos or distance < MinDist then
                    MinPos = Utility.PixelPos2GridPos(obj.Pos)
                    MinNpcId = obj.TemplateId
                    MinDist = distance
                end
            end
        end))
        if MinPos and MinNpcId then
            CTrackMgr.Inst:FindNPC(MinNpcId, sceneid, MinPos.x, MinPos.y, nil, nil)
        end
    else
        local npcidlist = Christmas2023_Setting.GetData().HangZhouXueHuaNpcId
        if not npcidlist or npcidlist.Length < 1 then return end
        CTrackMgr.Inst:FindNPC(npcidlist[0], hangzhouid, 0, 0, nil, nil)
    end
end

-- ===== 刮刮卡 =====

LuaChristmas2023Mgr.m_IsPaid = false -- 是否是付费刮刮卡

-- 打开刮刮卡界面
-- isPaid 是否是付费刮刮卡
function LuaChristmas2023Mgr:OpenChristmas2023GuaGuaCardWnd(isPaid)
    self.m_IsPaid = isPaid
    if CUIManager.IsLoaded(CLuaUIResources.Christmas2023GuaGuaCardWnd) then
        CUIManager.CloseUI(CLuaUIResources.Christmas2023GuaGuaCardWnd)
    end
    CUIManager.ShowUI(CLuaUIResources.Christmas2023GuaGuaCardWnd)
end
-- 刮刮卡结果
-- Gas2Gac.Christmas2023GuaGuaKaResult
function LuaChristmas2023Mgr:Christmas2023GuaGuaKaResult(upPattern, snowIds_U, rewardTbl_U, playerGetItemIds_U)
    local snowlist = MsgPackImpl.unpack(snowIds_U)
    local rewardlist = MsgPackImpl.unpack(rewardTbl_U)
    local getitemlist = MsgPackImpl.unpack(playerGetItemIds_U)
    g_ScriptEvent:BroadcastInLua("OnChristmas2023GuaGuaKaResult", upPattern, snowlist, rewardlist, getitemlist)
end
-- 刮刮卡大奖播放完毕
function LuaChristmas2023Mgr:Christmas2023GuaGuaKaDaJiangEnd()
    g_ScriptEvent:BroadcastInLua("OnChristmas2023GuaGuaKaDaJiangEnd")
end

-- ===== 雪花絮语 =====

LuaChristmas2023Mgr.EnumSelectGift = { -- 礼物类型
    HuaLi = 0,
    XinLi = 1,
}
LuaChristmas2023Mgr.m_SelectGift = -1    -- 选择的礼物类型
LuaChristmas2023Mgr.m_SelectItemId = -1  -- 礼物itemid
LuaChristmas2023Mgr.m_SelectTexture = "" -- 礼物图标
LuaChristmas2023Mgr.m_SelectPrice = 0    -- 礼物总价
LuaChristmas2023Mgr.m_SelectCount = 0    -- 礼物个数

-- LuaChristmas2023XueHuaXuYuSendGiftWnd 中的相关对象
LuaChristmas2023Mgr.m_IconTexture = nil
LuaChristmas2023Mgr.m_CountLabel = nil
LuaChristmas2023Mgr.m_NoGift = nil
LuaChristmas2023Mgr.m_Highlight = nil
-- LuaChristmas2023XueHuaXuYuGetGiftWnd 中的相关对象
LuaChristmas2023Mgr.m_PlayerImage = nil

LuaChristmas2023Mgr.m_posx = nil              -- 雪花x坐标
LuaChristmas2023Mgr.m_posy = nil              -- 雪花y坐标
LuaChristmas2023Mgr.m_WishTable = nil         -- 展示的雪花祝福信息
LuaChristmas2023Mgr.PlayerId2MengDaoInfo = {} -- 玩家信息数据

-- 送出祝福
function LuaChristmas2023Mgr:SendGift(posx, posy)
    if posx and posy then -- 手动传入坐标
        self.m_posx = posx
        self.m_posy = posy
    elseif CClientMainPlayer.Inst and CClientMainPlayer.Inst.Target then -- 获取对话的NPC坐标
        local gridpos = Utility.PixelPos2GridPos(CClientMainPlayer.Inst.Target.Pos)
        self.m_posx = gridpos.x
        self.m_posy = gridpos.y
    end
    if CUIManager.IsLoaded(CLuaUIResources.Christmas2023XueHuaXuYuSendGiftWnd) then
        CUIManager.CloseUI(CLuaUIResources.Christmas2023XueHuaXuYuSendGiftWnd)
    end
    CUIManager.ShowUI(CLuaUIResources.Christmas2023XueHuaXuYuSendGiftWnd)
end
-- 放入礼物
function LuaChristmas2023Mgr:PutGift()
    if self.m_SelectGift < 0 then
        self.m_NoGift:SetActive(true)
        self.m_Highlight:SetActive(false)
        self.m_IconTexture:LoadMaterial("")
        self.m_CountLabel.gameObject:SetActive(false)
        return
    end
    self.m_IconTexture:LoadMaterial(self.m_SelectTexture)
    if self.m_SelectCount > 1 then
        self.m_CountLabel.text = self.m_SelectCount
        self.m_CountLabel.gameObject:SetActive(true)
    else
        self.m_CountLabel.gameObject:SetActive(false)
    end
    self.m_NoGift:SetActive(false)
    self.m_Highlight:SetActive(true)
end
-- 查询祝福
function LuaChristmas2023Mgr:QueryGift(posx, posy)
    if posx and posy then -- 手动传入坐标
        self.m_posx = posx
        self.m_posy = posy
    elseif CClientMainPlayer.Inst and CClientMainPlayer.Inst.Target then -- 获取对话的NPC坐标
        local gridpos = Utility.PixelPos2GridPos(CClientMainPlayer.Inst.Target.Pos)
        self.m_posx = gridpos.x
        self.m_posy = gridpos.y
    end
    if CScene.MainScene and CScene.MainScene.SceneTemplateId == Constants.HangZhouID then
        Gac2Gas.QueryHangZhouChristmas2021Card(self.m_posx, self.m_posy) -- 杭州主城查询
    else
        Gac2Gas.QueryGuildChristmas2021Card(self.m_posx, self.m_posy)    -- 帮会查询
    end
end
-- 加载玩家头像
function LuaChristmas2023Mgr:LoadPlayerImage(info)
    if info and self.m_PlayerImage then
        if info.Photo then
            CPersonalSpaceMgr.Inst:DownLoadPortraitPic(info.Photo, self.m_PlayerImage.transform:GetComponent(typeof(UITexture)), nil)
        elseif info.Cls and info.Gender then
            self.m_PlayerImage:LoadNPCPortrait(CUICommonDef.GetPortraitName(info.Cls, info.Gender, -1), false)
        end
    end
end
-- 获取到祝福数据，打开获取祝福窗口
-- Gac2Gas.QueryGuildChristmas2021Card / QueryHangZhouChristmas2021Card
-- Gas2Gac.QueryGuildChristmas2021Card_Result
function LuaChristmas2023Mgr:SyncGuildChristmas2021CardResult(allcard_ud)
    local MakeCardTable = function(info)
        local card = {}
        card.Id = info[0]
        card.GuildId = info[1]
        card.PlayerId = info[2]
        card.Msg = info[3]
        card.Gifts = info[4]
        card.Sended = info[5]
        card.SendPlayerId = info[6]
        card.PlayerName = info[7]
        return card
    end
    self.m_WishTable = nil
    local MinGiftLeft = 998244353 -- 优先展示还有礼物并且剩余最少的祝福
    if allcard_ud and allcard_ud.Length > 0 then
        local list = MsgPackImpl.unpack(allcard_ud)
        if list and list.Count > 0 then
            for i = 0, list.Count - 1, 1 do
                local card = MakeCardTable(list[i])
                if card.Gifts and card.Gifts.Count >= 2 and card.Sended and card.Sended.Count >= 2 then
                    local left = card.Gifts[0] + card.Gifts[1] - (card.Sended[0] + card.Sended[1]) -- 剩余个数
                    if left > 0 and left < MinGiftLeft then
                        self.m_WishTable = card
                        MinGiftLeft = left
                    end
                end
            end
            -- 否则随机一个展示
            if not self.m_WishTable then
                self.m_WishTable = MakeCardTable(list[math.random(0, list.Count - 1)])
            end
        end
    end
    if not self.m_WishTable then -- 没有祝福，直接打开送礼界面
        self:SendGift(self.m_posx, self.m_posy)
        return
    end
    -- print("PlayerId", self.m_WishTable.PlayerId)
    -- print("PlayerName", self.m_WishTable.PlayerName)
    -- print("CardId", self.m_WishTable.Id)
    -- print("Count", MinGiftLeft)
    local playerid = self.m_WishTable.PlayerId and self.m_WishTable.PlayerId or nil
    -- 已经读取过玩家数据
    if playerid and self.PlayerId2MengDaoInfo[playerid] then
        self.m_WishTable.PlayerInfo = self.PlayerId2MengDaoInfo[playerid]
    end

    if CUIManager.IsLoaded(CLuaUIResources.Christmas2023XueHuaXuYuGetGiftWnd) then
        CUIManager.CloseUI(CLuaUIResources.Christmas2023XueHuaXuYuGetGiftWnd)
    end
    CUIManager.ShowUI(CLuaUIResources.Christmas2023XueHuaXuYuGetGiftWnd)

    -- 通过playerid获取玩家数据（用于获取头像）
    if playerid and not self.PlayerId2MengDaoInfo[playerid] then
        CPersonalSpaceMgr.Inst:GetUserProfile(playerid, DelegateFactory.Action_CPersonalSpace_UserProfile_Ret(function(ret)
            if ret.code ~= 0 or CClientMainPlayer.Inst == nil then
                return
            end
            local playerInfo = {}
            playerInfo.RoleName = ret.data.rolename
            playerInfo.Photo = ret.data.photo
            playerInfo.Cls = ret.data.clazz
            playerInfo.Gender = ret.data.gender
            self.PlayerId2MengDaoInfo[playerid] = playerInfo
            self:LoadPlayerImage(playerInfo)
        end), nil)
    end
end

-- ===== 冰雪大世界 =====

LuaChristmas2023Mgr.EnumStage = { -- 冰雪大世界模式
    Boss = 1, -- boss模式
    Pick = 2, -- 采集模式
}
LuaChristmas2023Mgr.m_SceneInfo = nil -- 冰雪大世界当前的进度
LuaChristmas2023Mgr.m_LastScore = 0   -- 冰雪大世界结算分数
LuaChristmas2023Mgr.m_IsAward = false -- 冰雪大世界是否领到奖

-- 是否在冰雪大世界副本中
function LuaChristmas2023Mgr:IsInGamePlay()
    if CScene.MainScene then
        return CScene.MainScene.GamePlayDesignId == Christmas2023_BingXuePlay.GetData().GameplayId
    end
    return false
end
-- 冰雪大世界同步数据
-- Gas2Gac.SyncBingXuePlay2023PlayData
function LuaChristmas2023Mgr:SyncBingXuePlay2023PlayData(stage, bossHp, Score, leftPickTime, bossFullHp)
    LuaChristmas2023Mgr.m_SceneInfo = {
        Stage = stage,
        BossHp = bossHp,
        Score = Score,
        LeftPickTime = leftPickTime,
        BossFullHp = bossFullHp,
    }
    g_ScriptEvent:BroadcastInLua("OnSyncBingXuePlay2023PlayData")
end
-- 冰雪大世界开始采集模式
-- Gas2Gac.StartBingXuePlay2023PickPart
function LuaChristmas2023Mgr:StartBingXuePlay2023PickPart()
    g_ScriptEvent:BroadcastInLua("OnStartBingXuePlay2023PickPart")
end
-- 冰雪大世界进入结算环节
-- Gas2Gac.Christmas2023BingXuePlayAward
function LuaChristmas2023Mgr:Christmas2023BingXuePlayAward(currentScore, isaward)
    self.m_LastScore = currentScore
    self.m_IsAward = isaward
    CUIManager.ShowUI(CLuaUIResources.TurkeyMatchResultWnd)
end
-- 冰雪大世界开始选人
-- Gas2Gac.Christmas2023BingXuePlayChooseCircle
function LuaChristmas2023Mgr:Christmas2023BingXuePlayChooseCircle()
    -- 打开选人窗口
    if not CUIManager.IsLoaded(CLuaUIResources.Christmas2023BingXuePlayChooseWnd) then
        CUIManager.ShowUI(CLuaUIResources.Christmas2023BingXuePlayChooseWnd)
    else
        -- 由于倒计时偏差，上次窗口可能还在，此时重开窗口刷新倒计时
        CUIManager.CloseUI(CLuaUIResources.Christmas2023BingXuePlayChooseWnd)
        CUIManager.ShowUI(CLuaUIResources.Christmas2023BingXuePlayChooseWnd)
    end
end

-- ===== 给自己的礼物 =====

LuaChristmas2023Mgr.m_CardId = -1        -- 给自己的礼物获取到的卡片id
LuaChristmas2023Mgr.m_IsFirstGet = false -- 给自己的礼物是否第一次获取到卡片
LuaChristmas2023Mgr.m_UpWndCount = 0     -- 用于判断当前是否在给自己的礼物主界面

-- 给自己的礼物获取卡片成功
-- Gac2Gas.Christmas2023CarftCard
-- Gas2Gac.Christmas2023GetCardSuccess
function LuaChristmas2023Mgr:Christmas2023GetCardSuccess(cardId, isFirstGet)
    self.m_CardId = cardId
    self.m_IsFirstGet = isFirstGet
    CUIManager.ShowUI(CLuaUIResources.Christmas2023GiftForSelfResultWnd)
end
-- 给自己的礼物获取礼物成功
-- Gac2Gas.Christmas2023GetCardGift
-- Gas2Gac.Christmas2023GetCardGiftSuccess
function LuaChristmas2023Mgr:Christmas2023GetCardGiftSuccess(cardId)
    g_ScriptEvent:BroadcastInLua("OnChristmas2023GetCardGiftSuccess", cardId)
end
-- 主界面上压了一个界面
function LuaChristmas2023Mgr:IncUpWndCount()
    LuaChristmas2023Mgr.m_UpWndCount = LuaChristmas2023Mgr.m_UpWndCount + 1
end
-- 主界面上压的界面关闭了
function LuaChristmas2023Mgr:DecUpWndCount()
    LuaChristmas2023Mgr.m_UpWndCount = LuaChristmas2023Mgr.m_UpWndCount - 1
    -- 回到了主界面
    if LuaChristmas2023Mgr.m_UpWndCount <= 0 then
        g_ScriptEvent:BroadcastInLua("OnChristmas2023UpdateLightInfo")
    end
end