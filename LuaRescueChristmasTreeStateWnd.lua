local Vector3 = import "UnityEngine.Vector3"
local UILabel = import "UILabel"
local UISlider = import "UISlider"
local UISprite = import "UISprite"
local Color = import "UnityEngine.Color"
local CUITexture = import "L10.UI.CUITexture"

CLuaRescueChristmasTreeStateWnd = class()

RegistClassMember(CLuaRescueChristmasTreeStateWnd,"m_Slider")
RegistClassMember(CLuaRescueChristmasTreeStateWnd,"m_LeftTree")
RegistClassMember(CLuaRescueChristmasTreeStateWnd,"m_RightTree")
RegistClassMember(CLuaRescueChristmasTreeStateWnd,"m_ExpandBtn")
RegistClassMember(CLuaRescueChristmasTreeStateWnd,"m_TreeLevelLabel")
RegistClassMember(CLuaRescueChristmasTreeStateWnd,"m_LastGrowthValue")
RegistClassMember(CLuaRescueChristmasTreeStateWnd,"m_TreeLevelData")
RegistClassMember(CLuaRescueChristmasTreeStateWnd,"m_MaxLevel")

function CLuaRescueChristmasTreeStateWnd:Awake()
    self.m_Slider = self.transform:Find("Anchor/Slider"):GetComponent(typeof(UISlider))
    self.m_LeftTree = self.transform:Find("Anchor/LeftTree")
    self.m_RightTree = self.transform:Find("Anchor/RightTree")
    self.m_TreeLevelLabel = self.transform:Find("Anchor/Slider/Label"):GetComponent(typeof(UILabel))
    self.m_ExpandBtn = self.transform:Find("Anchor/Tip/ExpandButton").gameObject
    self.m_LastGrowthValue = ShengDan_RescueTree.GetData().InitGrowthValue

    UIEventListener.Get(self.m_ExpandBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        self.m_ExpandBtn.transform.localEulerAngles = Vector3(0, 0, 0)
        CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
    end)

end

function CLuaRescueChristmasTreeStateWnd:Init()   
    if CLuaRescueChristmasTreeMgr.curState == 2 then
        self:InitValueIcon(CLuaRescueChristmasTreeMgr.yuanDanIcons)
    else
        self:InitValueIcon(CLuaRescueChristmasTreeMgr.shengDanIcons)
    end
    Extensions.SetLocalPositionZ(self.m_RightTree, - 1)
    self.m_TreeLevelData = {}

    self.m_MaxLevel = 0
    for value,state in string.gmatch(ShengDan_RescueTree.GetData().GrowthValue2NpcId, "(%d+),(%d+)") do
        self.m_MaxLevel = self.m_MaxLevel + 1
        self.m_TreeLevelData[self.m_MaxLevel]=tonumber(value)
    end

    self:OnRefreshState(ShengDan_RescueTree.GetData().InitGrowthValue)
end

function CLuaRescueChristmasTreeStateWnd:InitValueIcon(icons)
    self.m_LeftTree:GetComponent(typeof(CUITexture)):LoadMaterial(icons[1])
    self.m_RightTree:GetComponent(typeof(CUITexture)):LoadMaterial(icons[2])
end

function CLuaRescueChristmasTreeStateWnd:OnEnable()
    g_ScriptEvent:AddListener("UpdateChristmasTreeState", self, "OnRefreshState")
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
end

function CLuaRescueChristmasTreeStateWnd:OnDisable()
    g_ScriptEvent:RemoveListener("UpdateChristmasTreeState", self, "OnRefreshState")
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
end
------------------call back
function CLuaRescueChristmasTreeStateWnd:OnRefreshState(value)
    self.m_Slider.value = value/self.m_TreeLevelData[self.m_MaxLevel]
    
    if value >= self.m_TreeLevelData[6] then
        self.m_TreeLevelLabel.text = "lv.6"
        Extensions.SetLocalPositionZ(self.m_RightTree, 0)
    elseif value >= self.m_TreeLevelData[5] then
        self.m_TreeLevelLabel.text = "lv.5"
    elseif value >= self.m_TreeLevelData[4] then
        self.m_TreeLevelLabel.text = "lv.4"
    elseif value >= self.m_TreeLevelData[3] then
        self.m_TreeLevelLabel.text = "lv.3"
    elseif value >= self.m_TreeLevelData[2] then
        self.m_TreeLevelLabel.text = "lv.2"
    elseif value >= self.m_TreeLevelData[1] then
        self.m_TreeLevelLabel.text = "lv.1"
    end

    if value <=0 then
        Extensions.SetLocalPositionZ(self.m_LeftTree, - 1)
    else
        Extensions.SetLocalPositionZ(self.m_LeftTree, 0)
    end

    if value < self.m_LastGrowthValue then
        self.m_Slider.transform:Find("Foreground"):GetComponent(typeof(UISprite)).color = Color.red
    else
        self.m_Slider.transform:Find("Foreground"):GetComponent(typeof(UISprite)).color = NGUIText.ParseColor("29DC71", 0)
    end
    self.m_LastGrowthValue = value
end

function CLuaRescueChristmasTreeStateWnd:OnHideTopAndRightTipWnd( )
    self.m_ExpandBtn.transform.localEulerAngles = Vector3(0, 0, 180)
end
