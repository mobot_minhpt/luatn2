local CCommonLuaWnd = import "L10.UI.CCommonLuaWnd"
local WndType = import "L10.UI.WndType"

--中秋月灵石合成界面
CLuaZQYueLingShiHeChengWnd = class()

RegistClassMember(CLuaZQYueLingShiHeChengWnd, "delayTick")

function CLuaZQYueLingShiHeChengWnd:OnEnable()
    if self.delayTick then 
		UnRegisterTick(self.delayTick)
		self.delayTick = nil
	end
	self.delayTick = RegisterTickOnce(function()
		local wnd = self.gameObject:GetComponent(typeof(CCommonLuaWnd))
        wnd.TypeOfWnd = WndType.TooltipWithBlackBg
	end,5 * 1000)
end

function CLuaZQYueLingShiHeChengWnd:OnDisable()
    if self.delayTick then 
		UnRegisterTick(self.delayTick)
		self.delayTick = nil
	end
end