local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CTipParagraphItem = import "L10.UI.CTipParagraphItem"
local MessageMgr = import "L10.Game.MessageMgr"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local CQnReturnAwardTemplate = import "L10.UI.CQnReturnAwardTemplate"
local CItemMgr = import "L10.Game.CItemMgr"

LuaQingMing2023PVEWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaQingMing2023PVEWnd, "RuleTemplate", GameObject)
RegistChildComponent(LuaQingMing2023PVEWnd, "RuleTabel", UITable)
RegistChildComponent(LuaQingMing2023PVEWnd, "EasyBtn", GameObject)
RegistChildComponent(LuaQingMing2023PVEWnd, "DifficultBtn", GameObject)
RegistChildComponent(LuaQingMing2023PVEWnd, "JoinBtn", GameObject)
RegistChildComponent(LuaQingMing2023PVEWnd, "RankBtn", GameObject)
RegistChildComponent(LuaQingMing2023PVEWnd, "TimeLimit", UITexture)
RegistChildComponent(LuaQingMing2023PVEWnd, "PersonLimit", UITexture)
RegistChildComponent(LuaQingMing2023PVEWnd, "LvLimit", UITexture)
RegistChildComponent(LuaQingMing2023PVEWnd, "ItemCell", CQnReturnAwardTemplate)
RegistChildComponent(LuaQingMing2023PVEWnd, "RewardTipLabel", UILabel)
RegistChildComponent(LuaQingMing2023PVEWnd, "RankLabel", UILabel)

--@endregion RegistChildComponent end

function LuaQingMing2023PVEWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    UIEventListener.Get(self.EasyBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnDifficultBtnClick(false)
    end)
    UIEventListener.Get(self.DifficultBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnDifficultBtnClick(true)
    end)
    UIEventListener.Get(self.JoinBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnJoinBtnClick()
    end)
    UIEventListener.Get(self.RankBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnRankBtnClick()
    end)
end

function LuaQingMing2023PVEWnd:Init()
    local setData = QingMing2023_PVESetting.GetData()
    self:OnDifficultBtnClick(LuaQingMing2023Mgr.isPVESelectDifficult)
    self:InitPLayInfo(setData)
    self:InitAwards(setData)
    self:InitRuleDesc()
end

function LuaQingMing2023PVEWnd:InitPLayInfo(setData)
    self.timeLabel = self.TimeLimit.transform:Find("TimeLimitLabel"):GetComponent(typeof(UILabel))
    self.timeLabel.text = setData.PlayTimeString
    self.playerNumLabel = self.PersonLimit.transform:Find("PersonLimitLabel"):GetComponent(typeof(UILabel))
    self.playerNumLabel.text = SafeStringFormat3(LocalString.GetString("%d人组队"), setData.PlayerNum)
    self.lvLabel = self.LvLimit.transform:Find("LvLimitLabel"):GetComponent(typeof(UILabel))
    self.lvLabel.text = SafeStringFormat3(LocalString.GetString("≥%d级"), setData.LevelLimit)
end

function LuaQingMing2023PVEWnd:InitRuleDesc()
    self.RuleTemplate:SetActive(false)
    Extensions.RemoveAllChildren(self.RuleTabel.transform)
	local ruleMessage = MessageMgr.Inst:FormatMessage("QINGMING2023_PVE_PLAY_TIPS", {})
	local ruleTipInfo = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(ruleMessage)
    if ruleTipInfo == nil then return end
	for i = 0, ruleTipInfo.paragraphs.Count - 1 do
		local template = CUICommonDef.AddChild(self.RuleTabel.gameObject, self.RuleTemplate)
		template:GetComponent(typeof(CTipParagraphItem)):Init(ruleTipInfo.paragraphs[i], 0xffffffff)
		template:SetActive(true)
	end
	self.RuleTabel:Reposition()
end

function LuaQingMing2023PVEWnd:InitAwards(setData)
    self.easyItem = self.EasyBtn.transform:Find("ItemCell"):GetComponent(typeof(CQnReturnAwardTemplate))
    self.easyItem:Init(CItemMgr.Inst:GetItemTemplate(setData.EasyAwardItemId), 1)
    self.difficultItem = self.DifficultBtn.transform:Find("ItemCell"):GetComponent(typeof(CQnReturnAwardTemplate))
    self.difficultItem:Init(CItemMgr.Inst:GetItemTemplate(setData.HardAwardItemId), 1)
    self.ItemCell:Init(CItemMgr.Inst:GetItemTemplate(setData.ExtraAwardItemId), 1)
end

function LuaQingMing2023PVEWnd:OnSendQingMing2023PveInfo(isOpen, isTeamOk, isLevelOk, award, myRank, finishTime)
    self.TimeLimit.color = isOpen and NGUIText.ParseColor24("ffffff", 0) or NGUIText.ParseColor24("ff4646", 0)
    self.timeLabel.color = isOpen and NGUIText.ParseColor24("ffffff", 0) or NGUIText.ParseColor24("ff4646", 0)
    self.PersonLimit.color = isTeamOk and NGUIText.ParseColor24("ffffff", 0) or NGUIText.ParseColor24("ff4646", 0)
    self.playerNumLabel.color = isTeamOk and NGUIText.ParseColor24("ffffff", 0) or NGUIText.ParseColor24("ff4646", 0)
    self.LvLimit.color = isLevelOk and NGUIText.ParseColor24("ffffff", 0) or NGUIText.ParseColor24("ff4646", 0)
    self.lvLabel.color = isLevelOk and NGUIText.ParseColor24("ffffff", 0) or NGUIText.ParseColor24("ff4646", 0)

    self.easyItem.transform:Find("GetMask").gameObject:SetActive(award == 1)
    self.difficultItem.transform:Find("GetMask").gameObject:SetActive(award == 2)
    CUICommonDef.SetActive(self.easyItem.gameObject, award ~= 2, true)
    CUICommonDef.SetActive(self.difficultItem.gameObject, award ~= 1, true)
    self.RewardTipLabel.text = award == 0 and LocalString.GetString("每日首次通关可得奖励") or LocalString.GetString("今日已通关")
    self.RankLabel.text = SafeStringFormat3(LocalString.GetString("当前排名：%s"), myRank > 0 and tostring(myRank) or LocalString.GetString("未上榜"))
end

--@region UIEvent
function LuaQingMing2023PVEWnd:OnDifficultBtnClick(isDifficult)
    LuaQingMing2023Mgr.isPVESelectDifficult = isDifficult
    self.EasyBtn.transform:Find("selectbg/mask").gameObject:SetActive(not isDifficult)
    self.EasyBtn.transform:Find("HighLight").gameObject:SetActive(not isDifficult)
    self.DifficultBtn.transform:Find("selectbg/mask").gameObject:SetActive(isDifficult)
    self.DifficultBtn.transform:Find("HighLight").gameObject:SetActive(isDifficult)
end

function LuaQingMing2023PVEWnd:OnJoinBtnClick()
    Gac2Gas.EnterQingMing2023PvePlay(LuaQingMing2023Mgr.isPVESelectDifficult and 2 or 1)
end

function LuaQingMing2023PVEWnd:OnRankBtnClick()
    Gac2Gas.QueryQingMing2023PveRank()
end
--@endregion UIEvent
function LuaQingMing2023PVEWnd:OnEnable()
    Gac2Gas.QueryQingMing2023PveInfo()
    g_ScriptEvent:AddListener("SendQingMing2023PveInfo", self, "OnSendQingMing2023PveInfo")
end

function LuaQingMing2023PVEWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendQingMing2023PveInfo", self, "OnSendQingMing2023PveInfo")
end

