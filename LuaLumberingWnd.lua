require("common/common_include")

local UISlider = import "UISlider"
local Time = import "UnityEngine.Time"
local TweenRotation = import "TweenRotation"
local Ease = import "DG.Tweening.Ease"
local UISprite = import "UISprite"
local CUIFx = import "L10.UI.CUIFx"
local Vector3 = import "UnityEngine.Vector3"
local CUIManager = import "L10.UI.CUIManager"
local Screen = import "UnityEngine.Screen"
local Random = import "UnityEngine.Random"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local Mathf = import "UnityEngine.Mathf"
local Constants = import "L10.Game.Constants"

LuaLumberingWnd = class()

RegistChildComponent(LuaLumberingWnd, "CountDownLabel", UILabel)
RegistChildComponent(LuaLumberingWnd, "ScoreLabel", UILabel)
RegistChildComponent(LuaLumberingWnd, "Progress", UISlider)
RegistChildComponent(LuaLumberingWnd, "Axe", GameObject)
RegistChildComponent(LuaLumberingWnd, "AxeTexture", GameObject)
RegistChildComponent(LuaLumberingWnd, "UsefulPeriod", UISprite)
RegistChildComponent(LuaLumberingWnd, "FallingLeaveFx", CUIFx)
RegistChildComponent(LuaLumberingWnd, "Branch", GameObject)
RegistChildComponent(LuaLumberingWnd, "BranchRoot", GameObject)
RegistChildComponent(LuaLumberingWnd, "ResultFx", CUIFx)
RegistChildComponent(LuaLumberingWnd, "Fail", GameObject)
RegistChildComponent(LuaLumberingWnd, "GuideMask", GameObject)
RegistChildComponent(LuaLumberingWnd, "UsefulPeriodGuidePos", GameObject)

RegistClassMember(LuaLumberingWnd, "m_AxeNormalRotation")

RegistClassMember(LuaLumberingWnd, "m_CountDownTick")
RegistClassMember(LuaLumberingWnd, "m_TimeCounter")
RegistClassMember(LuaLumberingWnd, "m_LumberingTotaltime")

RegistClassMember(LuaLumberingWnd, "m_ShuZhiGot")
RegistClassMember(LuaLumberingWnd, "m_ShuZhiNeed")

RegistClassMember(LuaLumberingWnd, "m_LumberingLoopTime")
RegistClassMember(LuaLumberingWnd, "m_LumberingLoopTimeCounter")
RegistClassMember(LuaLumberingWnd, "m_LumberingStartLoop")
RegistClassMember(LuaLumberingWnd, "m_ValidPeriod")
RegistClassMember(LuaLumberingWnd, "m_ValidPeriodStart")
RegistClassMember(LuaLumberingWnd, "m_ValidPeriodWidth")

RegistClassMember(LuaLumberingWnd, "m_BranchGeneratePercent")

RegistClassMember(LuaLumberingWnd, "m_GameEnd")

function LuaLumberingWnd:Init()
	local setting = ZhuJueJuQing_Setting.GetData()
	self.m_LumberingTotaltime = setting.LumberingTotaltime
	self.m_TimeCounter = 0

	self.m_ShuZhiGot = 0
	self.m_ShuZhiNeed = setting.LumberingNeedCount

	self.m_LumberingLoopTime = setting.LumberingLoopTime
	self.m_LumberingLoopTimeCounter = 0
	self.m_LumberingStartLoop = false
	self.m_ValidPeriod = setting.LumberingValidPercent
	self.m_ValidPeriodStart = setting.LumberingValidPercent
	self.m_ValidPeriodWidth = 1 - self.m_ValidPeriod

	self.m_BranchGeneratePercent = {}
	for i = 0, setting.LumberingShuZhiPercent.Length-1 do
		local percentMin = 0
		if i > 0 then
			percentMin = setting.LumberingShuZhiPercent[i-1]
		end
		table.insert(self.m_BranchGeneratePercent, percentMin + setting.LumberingShuZhiPercent[i])
	end

	self.m_AxeNormalRotation = self.Axe:GetComponent(typeof(TweenRotation))
	self.CountDownLabel.text = self:ParseTimeText(self.m_LumberingTotaltime)

	self.m_GameEnd = false
	self.UsefulPeriod.width = self.Progress.backgroundWidget.width * (1 - self.m_ValidPeriod)
	self.Branch:SetActive(false)
	self.Fail:SetActive(false)

	self:TryStartGameWithGuide()
end


function LuaLumberingWnd:BeginGame()
	-- 倒计时
	self:DestroyCountDownTick()
	self.m_CountDownTick = RegisterTick(function ()
		self.m_TimeCounter = self.m_TimeCounter + 1
		local leftTime = self.m_LumberingTotaltime - self.m_TimeCounter
		if leftTime < 0 then
			self:EndGame()
			self:DestroyCountDownTick()
		else
			self.CountDownLabel.text = self:ParseTimeText(leftTime)
		end
	end, 1000)

	CommonDefs.AddOnClickListener(self.AxeTexture, DelegateFactory.Action_GameObject(function (go)
        self:OnAxeClicked(go)
    end), false)

	self.m_GameEnd = false
	self:StartLoop()
	self:UpdateScore()
end

function LuaLumberingWnd:StartLoop()
	self.m_LumberingLoopTimeCounter = 0
	self.m_LumberingStartLoop = true
end

function LuaLumberingWnd:ParseTimeText(totalSeconds)
    if totalSeconds < 0 then
        totalSeconds = 0
    end
    if totalSeconds >= 3600 then
        return SafeStringFormat3("%02d:%02d:%02d", math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds	 % 60)
    else
        return SafeStringFormat3("%02d:%02d", math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end


function LuaLumberingWnd:OnAxeClicked(go)
	if not self.m_LumberingStartLoop or self.m_GameEnd then return end

	self.m_AxeNormalRotation.enabled = false
	self.m_LumberingStartLoop = false

	local tweener = LuaTweenUtils.TweenRotationZ(self.Axe.transform, 20, 0.3)
	LuaTweenUtils.SetEase(tweener, Ease.OutBack)
	LuaTweenUtils.OnComplete(tweener, function ()

		-- 检查是否有得分
		if self.Progress.value >= self.m_ValidPeriodStart and self.Progress.value <= self.m_ValidPeriodStart + self.m_ValidPeriodWidth then
			self:GainScore()
		else
			g_MessageMgr:ShowMessage("FAMU_NOT_VALID_PERIOD")
		end

		local resetTweener = LuaTweenUtils.TweenRotationZ(self.Axe.transform, -30, 0.3)
		LuaTweenUtils.SetEase(resetTweener, Ease.Linear)
		LuaTweenUtils.OnComplete(resetTweener, function ()
			self.m_AxeNormalRotation.enabled = true
			self.m_LumberingStartLoop = true
			self:ChangeValidPeriod()
		end)
		LuaTweenUtils.SetDelay(resetTweener, 0.5)
	end)
end


function LuaLumberingWnd:ChangeValidPeriod()
	if self.m_GameEnd then return end
	self.m_ValidPeriodStart = Random.Range(0, self.m_ValidPeriod)
	-- [-325, 325]
	local x = Mathf.Lerp(-325, 325, self.m_ValidPeriodStart + self.m_ValidPeriodWidth)
	LuaTweenUtils.TweenPositionX(self.UsefulPeriod.transform, x, 0)
end


function LuaLumberingWnd:GainScore()
	local branchNum = self:GetBranchNum()
	self.m_ShuZhiGot = self.m_ShuZhiGot + branchNum
	self.FallingLeaveFx:DestroyFx()
	self.FallingLeaveFx:LoadFx("Fx/UI/Prefab/UI_kanshuluoye.prefab")

	self:GenerateBranch(branchNum)
	self:UpdateScore()
end

function LuaLumberingWnd:GenerateBranch(num)
	for i = 1, num do
		self:EmitBranch()
	end
end

function LuaLumberingWnd:GetBranchNum()
	local random = Random.Range(0, 1)
	if random <= self.m_BranchGeneratePercent[1] then
		return 1
	elseif random > self.m_BranchGeneratePercent[1] and random <= self.m_BranchGeneratePercent[2] then
		return 2
	else
		return 3
	end
end

function LuaLumberingWnd:EmitBranch()
	local pos = Vector3.zero
	-- 是否打开外置聊天
	local percent = 1

	if CommonDefs.IsPCGameMode() then
		local CWinSocialWndMgr = import "L10.UI.CWinSocialWndMgr"
		if CWinSocialWndMgr.Inst.m_IsWinSocialWndOpened then
			percent = 1 - Constants.WinSocialWndRatio
		end
	end

	pos = CUIManager.UIMainCamera:ScreenToWorldPoint(Vector3(Random.Range(0, Screen.width * percent), Screen.height, 0))
	local branch = GameObject.Instantiate(self.Branch, pos, self.BranchRoot.transform.rotation)
	if branch then
		branch.transform.parent = self.BranchRoot.transform
		branch.transform.localScale = Vector3.one
		branch:SetActive(true)
	end
end

function LuaLumberingWnd:UpdateScore()
	self.ScoreLabel.text = SafeStringFormat3(LocalString.GetString("获得树枝（%s/%s）"), tostring(self.m_ShuZhiGot), tostring(self.m_ShuZhiNeed))
	if self.m_ShuZhiGot >= self.m_ShuZhiNeed then
		self:EndGame()
	end
end

function LuaLumberingWnd:EndGame()
	if self.m_GameEnd then return end

	self.m_GameEnd = true

	if self.m_ShuZhiGot >= self.m_ShuZhiNeed then
		self.ResultFx:DestroyFx()
		self.ResultFx:LoadFx(CUIFxPaths.TaskFinishedFxPath)
		Gac2Gas.FinishEventTask(LuaZhuJueJuQingMgr.m_LumberingTaskId, "Lumbering")
	else
		self.Fail:SetActive(true)
	end
	RegisterTickOnce(function ()
		CUIManager.CloseUI(CLuaUIResources.LumberingWnd)
	end, 2000)
end

function LuaLumberingWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateLumberingScore", self, "UpdateScore")
end

function LuaLumberingWnd:OnDisable()
	self:DestroyCountDownTick()
	g_ScriptEvent:RemoveListener("UpdateLumberingScore", self, "UpdateScore")
end

function LuaLumberingWnd:Update()
	if self.m_GameEnd then
		return
	end

	if self.m_LumberingStartLoop then
		self.m_LumberingLoopTimeCounter = self.m_LumberingLoopTimeCounter + Time.deltaTime
		if self.m_LumberingLoopTimeCounter > self.m_LumberingLoopTime then
			self.m_LumberingLoopTimeCounter = 0
		end
		self.Progress.value = self.m_LumberingLoopTimeCounter / self.m_LumberingLoopTime
	end
end

function LuaLumberingWnd:DestroyCountDownTick()
	self.m_TimeCounter = 0
	if self.m_CountDownTick then
		UnRegisterTick(self.m_CountDownTick)
		self.m_CountDownTick = nil
	end
end

function LuaLumberingWnd:TryStartGameWithGuide()
	local should_guide = CLuaGuideMgr.TryTriggerGuide(78)
	self.GuideMask:SetActive(should_guide)
	if(should_guide)then
		CommonDefs.AddOnClickListener(self.GuideMask, DelegateFactory.Action_GameObject(function (go)
			if(L10.Game.Guide.CGuideMgr.Inst:IsInPhase(78))then
				L10.Game.Guide.CGuideMgr.Inst:EndCurrentPhase()
			end
			self.GuideMask:SetActive(false)
			self:BeginGame()
		end), false)
	else
		self:BeginGame()
	end
end

function LuaLumberingWnd:GetGuideGo(methodName)
	if(methodName == "GetHighlightBar")then
		return self.UsefulPeriodGuidePos
	end
	return nil
end

return LuaLumberingWnd
