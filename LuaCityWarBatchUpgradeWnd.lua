require("common/common_include")
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local UILabel = import "UILabel"
local UIEventListener = import "UIEventListener"
local LuaUtils = import "LuaUtils"
local CQMPKTitleTemplate = import "L10.UI.CQMPKTitleTemplate"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local AlignType = import "L10.UI.CPopupMenuInfoMgr+AlignType"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"
local CCityWarMgr = import "L10.Game.CCityWarMgr"
local QnCheckBox = import "L10.UI.QnCheckBox"

CLuaCityWarBatchUpgradeWnd = class()
CLuaCityWarBatchUpgradeWnd.Path = "ui/citywar/LuaCityWarBatchUpgradeWnd"

RegistClassMember(CLuaCityWarBatchUpgradeWnd, "m_CurrentLevel")
RegistClassMember(CLuaCityWarBatchUpgradeWnd, "m_CurrentName")
RegistClassMember(CLuaCityWarBatchUpgradeWnd, "m_MoneyCtrl")
RegistClassMember(CLuaCityWarBatchUpgradeWnd, "m_ButtonLabel")
RegistClassMember(CLuaCityWarBatchUpgradeWnd, "m_TextLabel")
RegistClassMember(CLuaCityWarBatchUpgradeWnd, "m_CostLabel")
RegistClassMember(CLuaCityWarBatchUpgradeWnd, "m_CostValue")
RegistClassMember(CLuaCityWarBatchUpgradeWnd, "m_BatchCostValue")
RegistClassMember(CLuaCityWarBatchUpgradeWnd, "m_BatchList")
RegistClassMember(CLuaCityWarBatchUpgradeWnd, "m_TargetLevel")
RegistClassMember(CLuaCityWarBatchUpgradeWnd, "m_GradeSelector")
RegistClassMember(CLuaCityWarBatchUpgradeWnd, "m_SingleCheckBox")
RegistClassMember(CLuaCityWarBatchUpgradeWnd, "m_SingleCheckBoxLabel")
RegistClassMember(CLuaCityWarBatchUpgradeWnd, "m_BatchCheckBox")
RegistClassMember(CLuaCityWarBatchUpgradeWnd, "m_BatchCheckBoxLabel")
RegistClassMember(CLuaCityWarBatchUpgradeWnd, "m_IsBatch")
RegistClassMember(CLuaCityWarBatchUpgradeWnd, "m_BatchType")

function CLuaCityWarBatchUpgradeWnd:Init( ... )
	local unitDData = CityWar_Unit.GetData(CLuaCityWarMgr.CurrentUpgradeUnit.TemplateId)
	local nextLevelDData = CityWar_Unit.GetData(CLuaCityWarMgr.CurrentUpgradeUnit.TemplateId + 1)
	self.m_CurrentLevel = CLuaCityWarMgr.CurrentUpgradeUnit.TemplateId % 100
	self.m_CurrentName = unitDData.Name
	self.m_MoneyCtrl = self.transform:Find("Anchor/QnCostAndOwnMoney"):GetComponent(typeof(CCurentMoneyCtrl))
	self.m_MoneyCtrl:SetType(15, 0, true)
	self.m_ButtonLabel = self.transform:Find("Anchor/OkButton/Label"):GetComponent(typeof(UILabel))
	self.m_TextLabel = self.transform:Find("Anchor/TextLabel"):GetComponent(typeof(UILabel))
	self.m_CostLabel = self.transform:Find("Anchor/QnCostAndOwnMoney/Cost/Label"):GetComponent(typeof(UILabel))
	self.m_GradeSelector = self.transform:Find("Anchor/Grade"):GetComponent(typeof(CQMPKTitleTemplate))
	

	local popupMenuItemTable = {}
	local gradeTable = {}
	for i = 1, 5 do
		if i ~= self.m_CurrentLevel then
			table.insert(gradeTable, i)
			table.insert(popupMenuItemTable, PopupMenuItemData(SafeStringFormat3(LocalString.GetString("%s级"), tostring(i)), DelegateFactory.Action_int(function ( index )
				self:InitLevel(gradeTable[index + 1])
			end), false, nil, EnumPopupMenuItemStyle.Default))
		end
	end
	local popupMenuItemArray = Table2Array(popupMenuItemTable, MakeArrayClass(PopupMenuItemData))
	UIEventListener.Get(self.m_GradeSelector.gameObject).onClick = LuaUtils.VoidDelegate(function (go)
		self.m_GradeSelector:Expand(true)
		CPopupMenuInfoMgr.ShowPopupMenu(popupMenuItemArray, go.transform, AlignType.Right, 1, DelegateFactory.Action(function ( ... )
				self.m_GradeSelector:Expand(false)
			end), 600, true, 296)
	end)

	self:InitCheckBox()
	if nextLevelDData then
		self:InitLevel(self.m_CurrentLevel + 1)
	else
		self:InitLevel(self.m_CurrentLevel - 1)
	end
	self.m_SingleCheckBox:SetSelected(true, false)

	UIEventListener.Get(self.transform:Find("Anchor/OkButton").gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
		self:OnButtonClick()
	end)
end

function CLuaCityWarBatchUpgradeWnd:OnButtonClick( ... )
	if self.m_IsBatch then
		if self.m_BatchCostValue > 0 and CLuaCityWarMgr.CurrentMaterial < self.m_BatchCostValue then
			g_MessageMgr:ShowMessage("UPGRADE_CITY_UNIT_MATERIAL_NOT_ENOUGH")
			return
		else
			CCityWarMgr.Inst.CurUnit = nil
			CLuaCityWarMgr.CurrentUpgradeUnit.Moving = false
			Gac2Gas.RequestBatchUpgradeCityUnit(CLuaCityWarMgr.CurrentCityId, self.m_TargetLevel, self.m_BatchCostValue > 0, self.m_BatchType)
		end
	else
		if self.m_CostValue > 0 and CLuaCityWarMgr.CurrentMaterial < self.m_CostValue then
			g_MessageMgr:ShowMessage("UPGRADE_CITY_UNIT_MATERIAL_NOT_ENOUGH")
			return
		else
			CCityWarMgr.Inst.CurUnit = nil
			CLuaCityWarMgr.CurrentUpgradeUnit.Moving = false
			Gac2Gas.UpgradeCityUnit(CLuaCityWarMgr.CurrentCityId, CLuaCityWarMgr.CurrentUpgradeUnit.TemplateId, CLuaCityWarMgr.CurrentUpgradeUnit.ID, self.m_TargetLevel)
		end
	end

	CUIManager.CloseUI(CLuaUIResources.CityWarBatchUpgradeWnd)
end

function CLuaCityWarBatchUpgradeWnd:InitCheckBox( ... )
	self.m_SingleCheckBox = self.transform:Find("Anchor/Single"):GetComponent(typeof(QnCheckBox))
	self.m_SingleCheckBoxLabel = self.m_SingleCheckBox.transform:Find("Label"):GetComponent(typeof(UILabel))
	self.m_BatchCheckBox = self.transform:Find("Anchor/Batch"):GetComponent(typeof(QnCheckBox))
	self.m_BatchCheckBoxLabel = self.m_BatchCheckBox.transform:Find("Label"):GetComponent(typeof(UILabel))
	self.m_SingleCheckBox.OnValueChanged = DelegateFactory.Action_bool(function ( val )
		if val then
			self.m_BatchCheckBox:SetSelected(false, true)
			self.m_IsBatch = false
			self:InitLevel(self.m_TargetLevel)
		else
			self.m_SingleCheckBox:SetSelected(true, true)
		end
	end)
	self.m_BatchCheckBox.OnValueChanged = DelegateFactory.Action_bool(function (val)
		if val then
			self.m_SingleCheckBox:SetSelected(false, true)
			self.m_IsBatch = true
			self:InitLevel(self.m_TargetLevel)
		else
			self.m_BatchCheckBox:SetSelected(true, true)
		end
	end)
end

function CLuaCityWarBatchUpgradeWnd:InitLevel(level)
	self.m_TargetLevel = level
	self.m_GradeSelector:Init(SafeStringFormat3(LocalString.GetString("%s级"), tostring(level)))
	local opTxt = level > self.m_CurrentLevel and LocalString.GetString("升级") or LocalString.GetString("降级")
	local adjTxt = level > self.m_CurrentLevel and LocalString.GetString("低于") or LocalString.GetString("高于")
	self.m_TextLabel.text = SafeStringFormat3(LocalString.GetString("%s此%s至"), opTxt, self.m_CurrentName)
	self.m_ButtonLabel.text = opTxt
	self.m_CostLabel.text = level > self.m_CurrentLevel and LocalString.GetString("消耗") or LocalString.GetString("返还")
	self.m_SingleCheckBoxLabel.text = SafeStringFormat3(LocalString.GetString("%s此%s"), opTxt, self.m_CurrentName)

	local targetTemplateId = math.floor(CLuaCityWarMgr.CurrentUpgradeUnit.TemplateId / 100) * 100 + level
	local targetDData = CityWar_Unit.GetData(targetTemplateId)
	if not targetDData then return end

	local batchType = CityWar_UnitTypeName.GetData(targetDData.Type).BatchType
	self.m_BatchType = batchType
	local batchTypeNameDData = CityWar_BatchTypeName.GetData(batchType)
	local batchName = batchTypeNameDData and batchTypeNameDData.BatchName or ""
	self.m_BatchCheckBoxLabel.text = SafeStringFormat3(LocalString.GetString("%s所有%s%d级的%s"), opTxt, adjTxt, level, batchName)

	self.m_CostValue = targetDData.Material - CityWar_Unit.GetData(CLuaCityWarMgr.CurrentUpgradeUnit.TemplateId).Material

	self.m_BatchCostValue = 0
	self.m_BatchList = {}
	CommonDefs.DictIterate(CCityWarMgr.Inst.UnitList, DelegateFactory.Action_object_object(function (___key, ___value) 
		local unit = ___value
		local curLevel = unit.TemplateId % 100
        if (curLevel < level and self.m_CurrentLevel < level) or (curLevel > level and self.m_CurrentLevel > level) then
        	local enumCurDData = CityWar_Unit.GetData(unit.TemplateId)
        	local enumNextDData = CityWar_Unit.GetData(math.floor(unit.TemplateId/100)*100 + level)
        	local enumTypeDData = CityWar_UnitTypeName.GetData(enumCurDData.Type)
        	if enumCurDData and enumNextDData and enumTypeDData and enumTypeDData.BatchType == batchType then
        		self.m_BatchCostValue = self.m_BatchCostValue + enumNextDData.Material - enumCurDData.Material
        		self.m_BatchList[unit.ID] = unit.TemplateId
        	end
        end
    end))

	local cost = self.m_IsBatch and self.m_BatchCostValue or self.m_CostValue
    self.m_MoneyCtrl:SetCost(math.abs(cost))
end
