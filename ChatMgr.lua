local CChatLinkMgr=import "CChatLinkMgr"
local CChatMgr = import "L10.Game.CChatMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local CMiniChatMgr = import "L10.UI.CMiniChatMgr"
local CMiniChatMgrChatType = import "L10.UI.CMiniChatMgr+ChatType"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local LinkSource = import "CChatLinkMgr.LinkSource"
local Object = import "System.Object"
local Utility = import "L10.Engine.Utility"
local DelegateFactory = import "DelegateFactory"
local EnumLabaType = import "L10.Game.EnumLabaType"
local CommonDefs = import "L10.Game.CommonDefs"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local CCMiniAPIMgr = import "L10.Game.CCMiniAPIMgr"
local EnumCCMiniStreamType = import "L10.Game.EnumCCMiniStreamType"
local CCCChatMgr = import "L10.Game.CCCChatMgr"
local CJoinCCChanelContext = import "L10.Game.CJoinCCChanelContext"
local CLabaMsg = import "L10.Game.CLabaMsg"
local CProfileObject = import "L10.Game.CProfileObject"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CLoudSpeakerNoticeMgr = import "L10.UI.CLoudSpeakerNoticeMgr"
local CSocialWndMgr = import "L10.UI.CSocialWndMgr"

LuaChatMgr = class()

LuaChatMgr.m_GuildAidCommanderInfo = {}
--@region 聊天过滤

LuaChatMgr.FilterWords = nil

CChatMgr.m_hookPlayerUseFuxiLaba = function(this, name, id, clazz, gender, type, content, combo, extraInfo, toUserName, toUserId, fromUserExpression, fromUserExpressionTxt, fromUserSticker, profileInfo, xianFanStatus)
	if content == nil then
		return
	end
	for i = 0, content.Count - 1 do
		local labaMsg = CLabaMsg()
		local data = LaBa_FuXiLaBa.GetData(tonumber(content[i]))
		labaMsg.fromUserName = name
		labaMsg.fromUserId = id
		labaMsg.fromUserClass = clazz
		labaMsg.fromUserGender = gender
		labaMsg.type = type
		labaMsg.message = data.Content
		labaMsg.subType = 4- data.Type
		labaMsg.fromUserExpression = fromUserExpression
		labaMsg.fromUserExpressionTxt = fromUserExpressionTxt
		labaMsg.fromUserSticker = fromUserSticker
		labaMsg.isHongBao = false
		labaMsg.isPic = false
		labaMsg.timeStamp = CServerTimeMgr.Inst.timeStamp
		labaMsg.sendTime = CServerTimeMgr.Inst.timeStamp
		labaMsg.extraInfo = toUserName

		if profileInfo == nil then
			profileInfo = CProfileObject()
		end
		labaMsg.profileInfo = profileInfo
		this:DoAddMsg(labaMsg)

		if i == ( content.Count - 1) then
			CLoudSpeakerNoticeMgr.ShowComboMessage(labaMsg, combo)
			if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == toUserId and combo == 1 then
				this.m_FuxiLabaTimeStamp = CServerTimeMgr.Inst.timeStamp;
				this.m_FuxiLabaMsg = labaMsg
				if LuaCommonSideDialogMgr.m_IsOpen then
					CMiniChatMgr.NotifySenderId = labaMsg.fromUserId
					CMiniChatMgr.NotifySenderName = labaMsg.fromUserName
					local notifyInfo = LuaCommonSideDialogMgr:GetNotifyInfo("Quwei_Laba_Tip_Wnd_Type_".. tostring(4 - labaMsg.subType), "commonsidedialog_diban_05", "chat_background_9_1_" .. tostring(labaMsg.subType), "chat_background_9_2",nil,"chat_background_9_3_" .. tostring(labaMsg.subType))
					LuaCommonSideDialogMgr:ShowMiniChatNotifyWnd("PlayerUseFuxiLaba",
						LuaCommonSideDialogMgr:GetBtnInfo(LocalString.GetString("查看"), function() 
							CSocialWndMgr.ShowChatWnd(EChatPanel.World)
						end, true),notifyInfo)
				else
					CUIManager.ShowUI("QuweiLabaTipWnd")
				end
			end
		end
	end
end

function LuaChatMgr.LoadFilterWords()
	LuaChatMgr.FilterWords = {}
	if CClientMainPlayer.Inst==nil then return end

	local data = CClientMainPlayer.Inst.PlayProp.PersistPlayData
	if not CommonDefs.DictContains_LuaCall(data, EnumPersistPlayDataKey_lua.eMonitorChannel) then
		return
	end
	
	local vbin = CommonDefs.DictGetValue_LuaCall(data,EnumPersistPlayDataKey_lua.eMonitorChannel)
	local data = vbin.Data
	local lst = MsgPackImpl.unpack(data)
	if lst == nil then return end
	local count = lst.Count
	for i=0,count-1 do
		local str = lst[i]
		local len = string.len( str )
		if len >= 2 then
			local enabled = string.sub(str,len) == "1"
			local word = string.sub(str,1,len-1)
			LuaChatMgr.FilterWords[i+1]={
				Enabled = enabled,
				Word = word
			}
		else
			LuaChatMgr.FilterWords[i+1]={
				Enabled = false,
				Word = ""
			}
		end
	end
end

function LuaChatMgr.SaveFilterWords(datas)
	if CClientMainPlayer.Inst == nil then return end
	local ignore = false
	local list = CreateFromClass(MakeGenericClass(List, Object))
	for i=1,#datas do
		local data = datas[i]
		local wd = string.gsub(data.Word,"%s+","")--移除所有空格
		if wd then
			wd = CWordFilterMgr.Inst:DoFilterOnSendPersonalSpace(wd,false);
		end
		if wd then
			if CommonDefs.StringLength(wd) > 3 then
				wd = CommonDefs.StringSubstring2(wd,0,3)
			end
		end
		if wd and not System.String.IsNullOrEmpty(wd) then
			local enabledstr = data.Enabled and "1" or "0"
			local str = wd..enabledstr
			CommonDefs.ListAdd_LuaCall(list,str)
		else
			ignore = true
		end
	end
	local idata = MsgPackImpl.pack(list)
	Gac2Gas.SetMonitorChannel(idata)
	if ignore then
		g_MessageMgr:DisplayMessage("Monitor_Channel_Violation")
	end
end

--@endregion

function LuaChatMgr.PlayerInteractionPermitted(isCrossServerPlayer, playerPopupMenuInfo)
	return not (isCrossServerPlayer and (
		playerPopupMenuInfo.channel == EChatPanel.CC or playerPopupMenuInfo.channel == EChatPanel.VIP))
end

function LuaChatMgr.PlayerJoinTeamOrTeamGroupPermitted(isCrossServerPlayer, playerPopupMenuInfo)
	return not (isCrossServerPlayer and playerPopupMenuInfo.channel == EChatPanel.CC)
end

--点击图片链接的日志处理
function LuaChatMgr.OnExecuteImageAction(url, actionName, paramsStr, envContext)
	if envContext and CommonDefs.DictGetValue_LuaCall(envContext, "imgsource") == "jingling" then
		local dict = CreateFromClass(MakeGenericClass(Dictionary, cs_string, Object))
		CommonDefs.DictAdd_LuaCall(dict,"methodName", tostring(actionName))
		CommonDefs.DictAdd_LuaCall(dict,"params", LuaChatMgr.TruncateString(tostring(paramsStr)))
		if CommonDefs.DictContains_LuaCall(envContext, "keyword") then
			CommonDefs.DictAdd_LuaCall(dict,"keyword",  CommonDefs.DictGetValue_LuaCall(envContext, "keyword"))
		end
		CommonDefs.DictAdd_LuaCall(dict,"isimagelink", "1")
		Gac2Gas.RequestRecordClientLog("JingLingLinkClick", MsgPackImpl.pack(dict))
	end
end

function LuaChatMgr.RequestCheckLink(source, method, params_U, env_U)
	--在发起验证前就记录一下，避免遗漏, 登录界面不记录，此时没法上传日志

	if source == LinkSource.Mail then
		local dict = CreateFromClass(MakeGenericClass(Dictionary, cs_string, Object))
		CommonDefs.DictAdd_LuaCall(dict,"methodName", tostring(method))
		CommonDefs.DictAdd_LuaCall(dict,"params", MsgPackImpl.unpack(params_U)) --备注：params内容过多时会导致dict超过rpc的参数256字节限制，导致rpc发送失败
		Gac2Gas.RequestRecordClientLog("MailLinkClick", MsgPackImpl.pack(dict))
	elseif source == LinkSource.Announcement then
		local dict = CreateFromClass(MakeGenericClass(Dictionary, cs_string, Object))
		CommonDefs.DictAdd_LuaCall(dict,"methodName", tostring(method))
		CommonDefs.DictAdd_LuaCall(dict,"params", MsgPackImpl.unpack(params_U)) --备注：params内容过多时会导致dict超过rpc的参数256字节限制，导致rpc发送失败
		Gac2Gas.RequestRecordClientLog("AnnouncementLinkClick", MsgPackImpl.pack(dict))
	elseif source == LinkSource.JingLing then
		local dict = CreateFromClass(MakeGenericClass(Dictionary, cs_string, Object))
		CommonDefs.DictAdd_LuaCall(dict,"methodName", tostring(method))
		CommonDefs.DictAdd_LuaCall(dict,"params", MsgPackImpl.unpack(params_U)) --备注：params内容过多时会导致dict超过rpc的参数256字节限制，导致rpc发送失败
		local envDict =  env_U and MsgPackImpl.unpack(env_U) or nil
		if envDict and CommonDefs.DictContains_LuaCall(envDict, "keyword") then
			CommonDefs.DictAdd_LuaCall(dict,"keyword",  CommonDefs.DictGetValue_LuaCall(envDict, "keyword"))
		end
		Gac2Gas.RequestRecordClientLog("JingLingLinkClick", MsgPackImpl.pack(dict))
	end
	Gac2Gas.RequestCheckLink(source, method, params_U, env_U)
end
--为了避免RequestRecordClientLog的参数超过256字节限制，这里对某些字符串长度（目前主要是潜在的url）做限制，暂时限定为100字节
function LuaChatMgr.TruncateString(input)
	if input == nil or input == "" then
		return input
	end
	local array = Utility.BreakString(input, 2, 100, nil)
	if array[1] ~= "" then
		return array[0].."..."
	else
		return array[0]
	end
end

--@region 外援收听帮会自由发言

function LuaChatMgr:GuildAIDCommanderExists()
	return  self.m_GuildAidCommanderInfo.commanderId and self.m_GuildAidCommanderInfo.commanderId>0 or false
end

function LuaChatMgr:SetGuildAIDCCListen()
	if not CCMiniAPIMgr.Inst.IsEngineVersionEnabled then
		g_MessageMgr:ShowMessage("NEED_UPDATE_CLIENT");
		return
	end

	if not CCMiniAPIMgr.Inst:CheckWindowsPlatformCCMiniPath() then
		return
	end

	CCMiniAPIMgr.Inst:StopCCMiniCapture()

	if CCMiniAPIMgr.Inst:IsInGuildAidStream() then
		self:LeaveAIDRemoteChannel()
	else
		self:JoinAIDRemoteChannel(CJoinCCChanelContext.GuildAID)
	end
end

function LuaChatMgr:UpdateGuildAIDCommander(commanderId, commanderName)
	self.m_GuildAidCommanderInfo.commanderId = commanderId
	self.m_GuildAidCommanderInfo.commanderName = commanderName
	g_ScriptEvent:BroadcastInLua("OnGuildAIDCommanderUpdate")
end

function LuaChatMgr:JoinAIDRemoteChannel(context)
	CCCChatMgr.Inst:JoinRemoteCCChannel(EnumCCMiniStreamType.eGuildAid, nil, context)
end

function LuaChatMgr:LeaveAIDRemoteChannel()
	CCCChatMgr.Inst:LeaveRemoteCCChannel(EnumCCMiniStreamType.eGuildAid)
end

function LuaChatMgr:SetGuildCommanderForAID(commanderPlayerId, commanderPlayerName, bIsReconnect)
    self:UpdateGuildAIDCommander(commanderPlayerId, commanderPlayerName)
end

function LuaChatMgr:ResetGuildCommanderForAID()
    self:UpdateGuildAIDCommander(0,"")
end

--@endregion

--@region 对特定文本在满足要求的情况下不做敏感词检查
function LuaChatMgr:SyncIgnoreCheckText(channelId, text, mode, expireTime)
    CChatMgr.Inst:SyncIgnoreCheckText(channelId, text, mode, expireTime)
end
--@endregion

--@region 弹幕特殊关注
LuaChatMgr.m_DanMuAttentionPlayerMaxCount = 5
LuaChatMgr.m_DanMuAttentionPlayerList = {}
function LuaChatMgr:UpdateDanMuAttentionPlayer(playerList)
	self.m_DanMuAttentionPlayerList = {}
	for i = 1, #playerList do
		table.insert(self.m_DanMuAttentionPlayerList, playerList[i].playerId)
	end
end

function Gas2Gac.CheckLinkResult(source, method, params_U, env_U, checkResult)
	CChatLinkMgr.OnReturnCheckLinkResult(source, method, params_U, env_U, checkResult)
end

function Gas2Gac.BroadcastGuildMessageTopHighline(guildId, message)
	CChatMgr.Inst:AddGuildBroadcastMsg(message)
end

function Gas2Gac.BeAtInChannel(sourcePlayerId, sourcePlayerName, channelId, channelName)
	CMiniChatMgr.ShowGuildPlayerNotify(sourcePlayerId, sourcePlayerName)
	--For Flash Window In PC Client
	EventManager.BroadcastInternalForLua(EnumEventType.FlashPCWindow, {})
end

function Gas2Gac.ShowLabaInput(id, itemId, place, pos, limit, type)
	local title = ""
	local chatType = CMiniChatMgrChatType.Undefined

	if type == EnumToInt(EnumLabaType.DaLaba) then
		title = LocalString.GetString("大喇叭")
		chatType = CMiniChatMgrChatType.BigLoudSpeaker
	elseif type == EnumToInt(EnumLabaType.XiaoLaba) then
		title = LocalString.GetString("小喇叭")
		chatType = CMiniChatMgrChatType.SmallLoudSpeaker
	elseif type == EnumToInt(EnumLabaType.NYHTLaBa) then
		title = LocalString.GetString("凝音海棠喇叭")
		chatType = CMiniChatMgrChatType.NYHT
	elseif type == EnumToInt(EnumLabaType.FuXiLaBa) then
		CUIManager.ShowUI(CLuaUIResources.QuweiLabaChooseWnd)
		return
	elseif type == EnumToInt(EnumLabaType.WeddingMaleLaBa) or type == EnumToInt(EnumLabaType.WeddingFemaleLaBa) then
		title = LocalString.GetString("婚礼喇叭")
		chatType = CMiniChatMgrChatType.WeddingLaBa
	elseif type == EnumToInt(EnumLabaType.QingJiuLaBa) then
		LuaMeiXiangLouMgr.m_LaBaId = id
    	LuaMeiXiangLouMgr.m_LaBaItemId = itemId
		LuaMeiXiangLouMgr.m_LaBaPlace = place
		LuaMeiXiangLouMgr.m_LaBaPos = pos
		CUIManager.ShowUI(CLuaUIResources.QingJiuLaBaWnd)
		return
	end

	CMiniChatMgr.Show(title, limit, chatType, DelegateFactory.Action_MiniChatCallBackInfo(function (data)
		local packInfo = CreateFromClass(MakeGenericClass(List, Object))
		if data.notifyPlayers then
			for i = 0, data.notifyPlayers.Count-1 do
				CommonDefs.ListAdd_LuaCall(packInfo, data.notifyPlayers[i].playerId)
				CommonDefs.ListAdd_LuaCall(packInfo, data.notifyPlayers[i].playerName)
			end
		end
		CChatMgr.Inst:UseLaba(id, itemId, place, pos, data.msg, packInfo)
	end))
end