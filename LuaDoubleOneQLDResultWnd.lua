local GameObject = import "UnityEngine.GameObject"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local CUITexture = import "L10.UI.CUITexture"
local Vector3 = import "UnityEngine.Vector3"
local CItemMgr = import "L10.Game.CItemMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType=import "L10.UI.CItemInfoMgr+AlignType"

LuaDoubleOneQLDResultWnd = class()
RegistChildComponent(LuaDoubleOneQLDResultWnd,"closeBtn", GameObject)
RegistChildComponent(LuaDoubleOneQLDResultWnd,"winNode", GameObject)
RegistChildComponent(LuaDoubleOneQLDResultWnd,"failNode", GameObject)
RegistChildComponent(LuaDoubleOneQLDResultWnd,"templateNode", GameObject)

RegistClassMember(LuaDoubleOneQLDResultWnd, "maxChooseNum")

function LuaDoubleOneQLDResultWnd:Close()
	CUIManager.CloseUI("DoubleOneQLDResultWnd")
end

function LuaDoubleOneQLDResultWnd:OnEnable()
	--g_ScriptEvent:AddListener("", self, "")
end

function LuaDoubleOneQLDResultWnd:OnDisable()
	--g_ScriptEvent:RemoveListener("", self, "")
end

function LuaDoubleOneQLDResultWnd:Init()
	local onCloseClick = function(go)
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.closeBtn,DelegateFactory.Action_GameObject(onCloseClick),false)

	self.templateNode:SetActive(false)
	--LuaDoubleOne2019Mgr.QLDResult = false
	--LuaDoubleOne2019Mgr.QLDResultLayer = layer + 1
	local layerName = {
		LocalString.GetString('一'),
		LocalString.GetString('二'),
		LocalString.GetString('三'),
		LocalString.GetString('四'),
		LocalString.GetString('五')
	}

	if LuaDoubleOne2019Mgr.QLDResult then
		self.winNode:SetActive(true)
		self.failNode:SetActive(false)
		self.winNode.transform:Find('text'):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString('成功通关第%s层!'),layerName[LuaDoubleOne2019Mgr.QLDResultLayer])
		self:InitItemInfo(self.winNode.transform:Find('fatherNode').gameObject)
	else
		self.winNode:SetActive(false)
		self.failNode:SetActive(true)
		self.failNode.transform:Find('text'):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString('第%s层挑战失败!'),layerName[LuaDoubleOne2019Mgr.QLDResultLayer])
		self:InitItemInfo(self.failNode.transform:Find('fatherNode').gameObject)
	end
end

function LuaDoubleOneQLDResultWnd:InitItemInfo(node)
	CUICommonDef.ClearTransform(node.transform)

	local itemWidth = 100

  if LuaDoubleOne2019Mgr.QLDItem then
		for i,v in ipairs(LuaDoubleOne2019Mgr.QLDItem) do
			local itemId = v[1]
			local go = NGUITools.AddChild(node, self.templateNode)
			go:SetActive(true)
			if i <= 10 then
				go.transform.localPosition = Vector3((i-1)*itemWidth,0,0)
			else
				go.transform.localPosition = Vector3((i-11)*itemWidth,-itemWidth,0)
			end
			local item = CItemMgr.Inst:GetItemTemplate(itemId)
			local clickNode = go.transform:Find('IconTexture').gameObject
			clickNode:GetComponent(typeof(CUITexture)):LoadMaterial(item.Icon)
			local onClick = function(go)
				CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType.Default, 0, 0, 0, 0)
			end
			CommonDefs.AddOnClickListener(clickNode,DelegateFactory.Action_GameObject(onClick),false)
		end
	end
end

return LuaDoubleOneQLDResultWnd
