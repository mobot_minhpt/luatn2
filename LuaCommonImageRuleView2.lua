local CUICenterOnChild=import "L10.UI.CUICenterOnChild"
local CUITexture = import "L10.UI.CUITexture"

LuaCommonImageRuleView2 = class()

RegistChildComponent(LuaCommonImageRuleView2, "Template", "Template", GameObject)
RegistChildComponent(LuaCommonImageRuleView2, "IndicatorGrid", "IndicatorGrid", UIGrid)
RegistChildComponent(LuaCommonImageRuleView2, "IndicatorTemplate", "IndicatorTemplate", GameObject)
RegistChildComponent(LuaCommonImageRuleView2, "ImageRoot", "ImageRoot", GameObject)
RegistChildComponent(LuaCommonImageRuleView2, "NextArrow", "NextArrow", GameObject)
RegistChildComponent(LuaCommonImageRuleView2, "LastArrow", "LastArrow", GameObject)
RegistChildComponent(LuaCommonImageRuleView2, "DescLabel", "DescLabel", UILabel)

RegistClassMember(LuaCommonImageRuleView2, "m_PicList")
RegistClassMember(LuaCommonImageRuleView2, "m_MessageList")
RegistClassMember(LuaCommonImageRuleView2, "m_CurrentSelectedIndex")

function LuaCommonImageRuleView2:Awake()

    self.Template:SetActive(false)
    self.IndicatorTemplate:SetActive(false)


    
    UIEventListener.Get(self.LastArrow).onClick = DelegateFactory.VoidDelegate(function(g)
        self:UpdatePage( -1)
    end)
    UIEventListener.Get(self.NextArrow).onClick = DelegateFactory.VoidDelegate(function(g)
        self:UpdatePage(1)
    end)
end

function LuaCommonImageRuleView2:UpdatePage(offset)
    self.m_CurrentSelectedIndex = self.m_CurrentSelectedIndex + offset

    if self.m_CurrentSelectedIndex < 1 then
        self.m_CurrentSelectedIndex = #self.m_PicList
    end
    if self.m_CurrentSelectedIndex > #self.m_PicList then
        self.m_CurrentSelectedIndex = 1
    end

    local centerOnChild=self.ImageRoot:GetComponent(typeof(CUICenterOnChild))
    centerOnChild:CenterOnInstant(self.ImageRoot.transform:GetChild(self.m_CurrentSelectedIndex-1))
end
function LuaCommonImageRuleView2:Init(imagePaths, msgs)
    self.m_PicList = imagePaths
    self.m_MessageList = msgs

    Extensions.RemoveAllChildren(self.ImageRoot.transform)
    for i, v in ipairs(self.m_PicList) do
        local go = CUICommonDef.AddChild(self.ImageRoot.gameObject, self.Template)
        go:SetActive(true)

        local texture = go:GetComponent(typeof(CUITexture))
        texture:LoadMaterial(v)
    end
    self.m_CurrentSelectedIndex = 1

    Extensions.RemoveAllChildren(self.IndicatorGrid.transform)
    for i, v in ipairs(self.m_PicList) do
        local go = CUICommonDef.AddChild(self.IndicatorGrid.gameObject, self.IndicatorTemplate)
        go:SetActive(true)
    end
    self.IndicatorGrid:Reposition()

    local centerOnChild=self.ImageRoot:GetComponent(typeof(CUICenterOnChild))

    centerOnChild.onCenter=LuaUtils.OnCenterCallback(function(go)
        if not go then return end
        local index=go.transform:GetSiblingIndex()
        self.m_CurrentSelectedIndex = index+1

        for i=1,self.IndicatorGrid.transform.childCount do
            local child=self.IndicatorGrid.transform:GetChild(i-1)--:GetComponent(typeof(UISprite))
            child:Find("Normal").gameObject:SetActive(i ~= self.m_CurrentSelectedIndex)
            child:Find("Highlight").gameObject:SetActive(i == self.m_CurrentSelectedIndex)
        end

        self.DescLabel.text = self.m_MessageList[self.m_CurrentSelectedIndex]
    end)

    centerOnChild:CenterOnInstant(self.ImageRoot.transform:GetChild(0))
end
