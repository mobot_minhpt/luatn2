local CGuideWnd=import "L10.UI.CGuideWnd"
local CLoginMgr=import "L10.Game.CLoginMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local GameSetting_Client_Wapper=import "L10.Game.GameSetting_Client_Wapper"
local CHouseMainWnd=import "L10.UI.CHouseMainWnd"
local CTaskAndTeamWnd = import "L10.UI.CTaskAndTeamWnd"
local CFriendWnd=import "L10.UI.CFriendWnd"
local Ease = import "DG.Tweening.Ease"
local CMenuGuideMgr=import "L10.UI.CMenuGuideMgr"
local EnumGender=import "L10.Game.EnumGender"
local EnumClass=import "L10.Game.EnumClass"
local CQuanMinPKMgr=import "L10.Game.CQuanMinPKMgr"
local Item_Item=import "L10.Game.Item_Item"
local NGUIMath = import "NGUIMath"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CGuideMgr=import "L10.Game.Guide.CGuideMgr"
local BabyMgr=import "L10.Game.BabyMgr"
local MouseInputHandler=import "L10.Engine.MouseInputHandler"
local TriggerOnceGuideType=import "L10.Game.Guide.TriggerOnceGuideType"
local CCityWarMgr=import "L10.Game.CCityWarMgr"
local COpenEntryMgr=import "L10.Game.COpenEntryMgr"
local CCityWarUnit=import "L10.Game.CCityWarUnit"
local CCommonLuaWnd=import "L10.UI.CCommonLuaWnd"
local CGuildMgr=import "L10.Game.CGuildMgr"
local CSwitchMgr=import "L10.Engine.CSwitchMgr"
local CBaseWnd=import "L10.UI.CBaseWnd"
local CClientHouseMgr=import "L10.Game.CClientHouseMgr"
local CReliveWnd=import "L10.UI.CReliveWnd"
local CHousePopupMenu=import "L10.UI.CHousePopupMenu"
local CUITexture=import "L10.UI.CUITexture"
local CMainPlayerWnd=import "L10.UI.CMainPlayerWnd"
local CSkillButtonBoardWnd=import "L10.UI.CSkillButtonBoardWnd"
local CScene=import "L10.Game.CScene"
local CHouseCompetitionMgr = import "L10.Game.CHouseCompetitionMgr"
local MainPlayerFrame=import "L10.UI.MainPlayerFrame"
local CTaskMgr = import "L10.Game.CTaskMgr"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
-- local CEquipWordAutoBaptizeWnd=import "L10.UI.CEquipWordAutoBaptizeWnd"
local CMiniMapPopWnd=import "L10.UI.CMiniMapPopWnd"
local UIWidget = import "UIWidget"
local MonoBehaviour = import "UnityEngine.MonoBehaviour"
local BoxCollider = import "UnityEngine.BoxCollider"
local CTopAndRightTipWnd = import "L10.UI.CTopAndRightTipWnd"
local CGamePlayConfirmWnd=import "L10.UI.CGamePlayConfirmWnd"
local CSocialWnd=import "L10.UI.CSocialWnd"
local CThumbnailChatWnd = import "L10.UI.CThumbnailChatWnd"
local CGuanNingMgr = import "L10.Game.CGuanNingMgr"

require "ui/guide/LuaGuideAction"

--id管理
EnumGuideKey = {
    TaskGuide = 4,--任务寻路引导
    GuideUseBagItem = 5,
    Get3Skill = 7,
    UpgradeSkill = 8,
    Get6Skill = 9,
    GuideUseFood = 17,
    GuideZuDui = 19,
    FirstEnterGuanNing = 23,
    EquipTipGuide = 32,
    ZuoQi = 33,
    EquipIntensify = 38,--装备打造
    GroupIM = 40,
    HouseDecorate = 43,
    GetMiaoPu3 = 45,
    QingHongFly = 67,
    QieXian = 76,--切线引导
    BoilMedicineWnd = 79,
    TaoQuanWnd = 80,
    FamilyTreeWndTags = 81,
    EquipAutoBaptizeWnd = 82,
    BindRepo = 83,--绑仓
    MiniMapNpcFilter = 84,--大地图npc筛选
    DiZiLineFilter = 85,        --笛子界面滑动提示
    DiZiClickFilter = 86,       --笛子界面点击提示
    YingLingGetBianShenSkill = 87, --影灵获得变身技能
    SanxingGame1 = 88,
    SanxingGame2 = 89,
    SanxingGame3 = 90,
    SanxingGame4 = 91,
    JinShaJingMap = 92,
    YanChiXiaWorkWnd = 93,
    LearnAnimal  = 94,      --模仿动物技能按钮提示
    KillDaWu = 95,      --  杀死大巫
    PourWineWnd = 96, -- 倒酒玩法
    RemoteDouDiZhu = 97,
    -- RanFaBtn = 98, -- 外观界面染发剂提示
    TiaoXiangPlayWnd = 99,--调香玩法-百和调香
    HouseCompetition2020 = 101,--家园设计大赛2020
    ButterflyCrisis = 102,--
    ButterflyCrisis2 = 103,--
    SpokesmanHireContractWndGuide = 104,--代言人雇佣契约
    SpokesmanDonate = 105,--代言人家园众筹
    MakeClothesPattern = 106,--制衣制作图案
    MakeClothes = 107,--制衣放置图案
    TiaoXiangPlayWndHYNX = 108,--调香玩法-红艳凝香
    TiaoXiangPlayWnd3 = 109,--百和调香22029452
    TiaoXiangPlayWnd4 = 110,--红艳凝香22022948
    WanShengJieSkybox = 111,
    Huahunshimen1 = 112,
    Huahunshimen2 = 113,
    Huahunshimen3 = 114,
    Huahunshimen4 = 115,
    Huahunshimen5 = 116,
    Diekeshimen1 = 117,
    Diekeshimen2 = 118,
    WatchWuXingMingGe = 119,
    XianZhiSkill = 120,--仙职技能
    SoulCoreCondenseJiasu = 121, -- 灵核凝结加速
    ZhuoYaoMainWnd = 122,
    SoulCoreWnd = 123,
    RefineMonsterGuide = 124,
    WuXingAvoidSetting = 125,
    GoToZongMen = 126,
    AntiProfessionLevelUpSecurity = 128,
    MusicBoxPopControlWnd = 129,
    FindMusicBox = 130,
    SWBtn1 = 131,
    SWBtn2 = 132,
    HaMaoEntry = 133,--哈猫按钮引导
    HaoYiXingCard = 134,
    InviteNpcJoinSectGuide = 136,
    FirstEnterZongMen = 137,
    InviteNpcAlert = 138,
    GetPersonalSpaceNewBg = 139,
    EnemySectGuide = 141,
    GamePlayConfirmChat = 142,
    TransformFashion = 143,     --首次穿变身时装引导
    AntiProfessionGuide = 144,     --首次打开克符主界面引导
    FuxiDanceGuide1 = 145,     --首次自定义伏羲动作解锁引导
    FuxiDanceGuide2 = 146,     --首次自定义伏羲动作面板引导
    ReCommandZongMen = 148,

    LingShouLevelLimit = 152,--灵兽等级上限
    
    NewJianZhu_Level4 = 157,
    NewJianZhu_Level5 = 158,
    SeaFishingGuide = 159,--海钓搏斗
    HaoYiXingCardsWndDiscussionButton = 161,
    ZhuXianAchievementWndDiscussionButton = 162,
    WeddingHelpAnswerGuide = 163, -- 婚礼助答按钮引导
    WeddingInviteOpenGuide = 164,
    WeddingInviteTipGuide = 165,

    ShengXiaoCard_RuleAlert = 166,--玩法说明
    ShengXiaoCard_Guide1 = 167,
    ShengXiaoCard_Guide2 = 168,
    ShengXiaoCard_Guide3 = 169,
    ShengXiaoCard_Guide4 = 170,
    ShengXiaoCard_Guide5 = 171,
    ShengXiaoCard_Guide6 = 172,
    ShengXiaoCard_Guide7 = 173,
    ShengXiaoCard_Guide8 = 174,
    XinShengHuaJiDrawGuide = 175,
    -- 176
    BusinessPegionTickGuide = 177, --首次打开倒卖白鸽票界面引导
    GuildTerritorialWarsMap = 178,
    WuYi2022AcceptTaskWnd = 179,
    RuYiSkillGuide = 180,--巧夺如意
    HuLuWaAdventureGuide = 181,--葫芦娃三界历险
    BusinessGuide = 182,--倒卖引导
    GuanNingCommanderSignUp = 183, -- 关宁报名指挥
    GuanNingCommander = 184, -- 成为关宁战场指挥
    GuanNingNotCommander = 185, -- 没有成为关宁战场指挥
    GuanNingThumbsUp = 186, -- 关宁左侧点赞
    QianYingChuWenGuide = 187, -- 倩影初闻引导
    XinBaiGuide = 188, -- 新白引导
    QMPKJiXiangWuTypeGuide = 190, -- 全民争霸吉祥物类型切换引导
    GuildTerritorialWarGuideMap = 191,--天地棋局地图引导
    -- GuildFashionHeadAlpha = 192,--头饰开关引导
    GuildTerritorialWarPeripheryPlay = 194,--天地棋局地图引导
    GuildTerritorialWarPeripheryPlay2 = 195,
    -- BeiShiPosAdptGuide = 196, --背饰位置设置引导
    WorldCupGJZLZhongFengSkillGuide = 201,
    WorldCupGJZLBianFengSkillGuide = 202,
    WorldCupGJZLZhongChangSkillGuide = 203,
    ZhuoYaoCaptureView = 204,
    ZhuoYaoHuaFuWnd = 206,
    ZongMenPracticeGuide = 208, --宗派修行
    GuanNingCounterattackWeak = 209, -- 关宁英魂助战弱势方引导
    GuanNingCounterattackStrong = 210, -- 关宁英魂助战强势方引导
    NewZhuoYaoCaptureView = 211,
    ZhuoYaoHuaFuResult = 212,
    GaoChangJiDouIntro = 214, -- 高昌激斗模式介绍
	GuildLeagueCrossTrainShop = 215, --跨服联赛培养活动兑换商店指引
    FashionPreviewWndDoExpressionButton = 216,
    ThumbnailExpressionViewFashionSpecialExpressionButton = 217,
    HanJia2023BattlePassIntrodutionGuide = 218, --寒假2023通行证介绍引导
    HanJia2023BattlePassResetGuide = 219, --寒假2023通行证重置引导

    HaiZhan_1 = 220,
    HaiZhan_2 = 221,
    HaiZhan_3 = 222,
    HaiZhan_4 = 223,
    HaiZhan_5 = 224,
    HaiZhan_6 = 225,
    HaiZhan_7 = 226,
    HaiZhan_8 = 227,

    HanJia2023BattlePassUnlockVip1Guide = 228, --寒假2023解锁Vip1的动效(伪造,走引导方式)
    HanJia2023BattlePassUnlockVip2Guide = 229, --寒假2023解锁Vip2的动效(伪造,走引导方式)
    HanJia2023BattlePassOldServerIntrodutionGuide = 230, --寒假2023通行证老服介绍引导
    HanJia2023SnowAdventureSingleGuide = 231, --寒假2023雪魄历险单人引导
    HanJia2023SnowAdventureSingleInGameGuide = 232, --寒假2023雪魄历险玩法中单人引导

    TeamRecruitInTaskAndTeamGuide = 236, -- 队伍招募引导-任务栏界面队伍招募按钮
    TeamRecruitInTaskWndGuide = 237, -- 队伍招募引导-队伍界面更多操作按钮
    PengDaoSelectDifficultyWnd = 238, -- 蓬岛挑战引导
    PengDaoDevelopWnd = 239, -- 蓬岛强化引导
    SkillPrivateSilverGuide = 241, -- 修为专有银票引导
    MuKeGuide = 242, --木刻水印引导
    
    NanDuMapGuide = 244, --南都地图引导
    NanDuScheduleGuide = 245, --南都日程引导
    
    FSC_Guide1 = 246, -- 周年庆2023四时戏新手牌局引导
    FSC_Guide2 = 247,
    FSC_Guide3 = 248,
    FSC_Guide4 = 249,
    FSC_Guide5 = 250,
    FSC_Guide6 = 251,
    FSC_Guide7 = 252,

    ClubHousePrivateRoomPasswordSet = 255, -- 茶室私密房间密码设置引导
    SettingGuide = 256, 
    ChooseUserType = 257,
    AppearanceSetting = 258,
    AppearanceGuide1 = 259,
    AppearanceGuide2 = 260,

    AppearanceDaPei = 261,
    AppearanceTaBen = 262,
    AppearanceSkillAppearance = 264,
    AppearanceUnlockFashion = 265,
    
    BabySetTargetQiChang = 266,
    GuanNingRightThumbsUp = 275, -- 关宁右侧点赞
}

EnumGuideExtKey = {
    OldUser = 13,--老玩家标记
    -- SetUserType = 14,--是否做过新老玩家的判断
}

CLuaGuideMgr = {}

CLuaGuideMgr.EquipIntensifyIds = {
    [23030034]=true,
    [23030214]=true,
    [23030394]=true,
}

if rawget(_G, "CLuaGuideMgr") then
    g_ScriptEvent:RemoveListener("GuideEnd", CLuaGuideMgr, "OnGuideEnd")
end
g_ScriptEvent:AddListener("GuideEnd", CLuaGuideMgr, "OnGuideEnd")

function CLuaGuideMgr:OnGuideEnd(args)
    local phase = args and args[0]
    if phase then
        Gac2Gas.OnClientGuideEvent(phase)
    end
end

function CLuaGuideMgr.EnableGuide()
    if CQuanMinPKMgr.Inst.m_IsQuanMinPKServer then
        return false
    end
    return true
end

CLuaGuideMgr.m_GetGuideGoLookup={
    ["Joystick"] = true,
    -- ["SkillButtonBoard"] = true,
    -- ["TaskAndTeam"] = true,
    -- ["MainPlayerFrame"] = true,
    -- ["PackageWnd"] = true,
    -- ["EquipInfoWnd"] = true,
    ["ItemInfoWnd"] = true,
    ["TopRightMenu"] = true,
    ["RightMenuWnd"] =true,
    ["StoryWnd"] =true,
    -- ["EquipmentProcessWnd"] =true,
    ["SkillWnd"] =true,
    -- ["GuildWnd"] =true,
    -- ["ThumbnailChatWindow"] =true,
    -- ["FriendWnd"] =true,
    ["SettingWnd"] =true,
    ["AutoDrugWnd"] =true,
    ["AutoDrugItemList"] =true,
    -- ["AutoPickupWnd"] =true,
    ["TeamWnd"] =true,
    ["TeamMatchWnd"] =true,
    ["EquipHandIntensifyWnd"] =true,
    ["TianFuSkillWnd"] =true,
    ["TeamQuickJoinWnd"] =true,
    -- ["MainPlayerWnd"] =true,
    ["YuanBaoMarketWnd"] =true,
    ["PlayerShop_Info"] =true,
    ["VehicleMainWnd"] =true,
    ["PersonalSpaceWnd"] =true,
    -- ["SocialWnd"] =true,
    -- ["HouseMainWnd"] =true,
    -- ["HouseMinimap"] =true,
    -- ["FurnitureRightTopWnd"] =true,
    -- ["HousePopupMenu"] =true,
    ["PlantWnd"] =true,
    -- ["FurnitureBigTypeWnd"] =true,
    -- ["FurnitureSmallTypeWnd"] =true,
    ["BottomRightActionBoard"] =true,
    ["PopupMenu"] =true,
    -- ["SkillInfoWnd"] =true,
    ["XingGuanMainWnd"]=true,
    ["MessageBox"]=true,
}
CUIManager.m_GetGuideGoCallback=function(module,go,moduleName,methodName)
    if CLuaGuideMgr.m_GetGuideGoLookup[moduleName] then
        local wnd = go:GetComponent(typeof(CBaseWnd))
        if wnd then
            return wnd:GetGuideGo(methodName)
        end
    elseif moduleName == "ReliveWnd" then
        local wnd = go:GetComponent(typeof(CReliveWnd))
        if wnd then
            if methodName == "GetCityWarButton" then
                return wnd.relivePointBtn.gameObject
            end
        end
    elseif moduleName == "HousePopupMenu" then
        return CLuaGuideMgr.GetHousePopupMenuGuideGo(go, methodName)
    elseif moduleName == "PackageWnd" then
        return CLuaGuideMgr.GetPackageWndGuideGo(go,methodName)
    elseif moduleName == "AppearancePreviewWnd" then
        return CLuaGuideMgr.GetAppearancePreviewWndGuideGo(go,methodName)
    elseif moduleName == "MainPlayerWnd" then
        return CLuaGuideMgr.GetMainPlayerWndGuideGo(go,methodName)
    elseif moduleName == "SkillButtonBoard" then
        return CLuaGuideMgr.GetSkillButtonBoardGuideGo(go,methodName)
    elseif moduleName == "MainPlayerFrame" then
        return CLuaGuideMgr.GetMainPlayerFrameGuideGo(go,methodName)
    elseif moduleName == "EquipWordAutoBaptizeWnd" then
        return nil
        -- return CLuaGuideMgr.GetEquipWordAutoBaptizeWndGuideGo(go,methodName)
    elseif moduleName == "MiniMap" then
        return CLuaMiniMap:GetGuideGo(go,methodName)
    elseif moduleName == "MiniMapPopWnd" then
        return CLuaGuideMgr.GetMiniMapPopWndGuideGo(go,methodName)
    elseif moduleName == "YanChiXiaWorkWnd" then
        return CLuaGuideMgr.GetYanChiXiaWorkWndGuideGo(go, methodName)
    elseif moduleName == "TaskAndTeam" then
        return CLuaGuideMgr.GetTaskAndTeamWndGuideGo(go,methodName)
    elseif moduleName == "FriendWnd" then
        return CLuaGuideMgr.GetFriendWndGuideGo(go,methodName)
    elseif moduleName == "HouseMainWnd" then
        return CLuaGuideMgr.GetHouseMainWndGuideGo(go,methodName)
    elseif moduleName == "TopAndRightTipWnd" then
        return CLuaGuideMgr.GetTopAndRightTipWndGuideGo(go, methodName)
    elseif moduleName == "DialogWnd" then
        return CLuaGuideMgr.GetDialogWndGuideGo(go, methodName)
    elseif moduleName == "GamePlayConfirmWnd" then
        return CLuaGuideMgr.GetGamePlayConfirmWndGuideGo(go, methodName)
    elseif moduleName == "SocialWnd" then
        return CLuaGuideMgr.GetSocialWndGuideGo(go, methodName)
    elseif moduleName == "SoulCoreWnd" then
        return CLuaGuideMgr.GetSoulCoreWndGuideGo(go, methodName)
    elseif moduleName == "ThumbnailChatWindow" then
        return CLuaGuideMgr.GetThumbnailChatWindowGuideGo(go, methodName)
    else
        local wnd=go:GetComponent(typeof(CCommonLuaWnd))
        if wnd then
          return wnd.m_LuaScript:GetGuideGo(methodName)
        end
    end
	return nil
end
function CLuaGuideMgr.GetThumbnailChatWindowGuideGo(go,methodName)
    local wnd = go:GetComponent(typeof(CThumbnailChatWnd))
    if methodName == "GetFriendButton" then
        return wnd:GetFriendButton()
    end
    if methodName=="GetBubbleButton" then
        return wnd:GetBubbleButton()
    end
    if methodName=="GetExpressionButton" then
        return wnd:GetExpressionButton()
    end

    if methodName == "GetOpenUmbrellaExpression" then
        return wnd:GetOpenUmbrellaExpression()
    end

    if methodName == "GetFashionSpecialExpressionButton" then
        return wnd:GetFashionSpecialExpressionButton()
    end

    if methodName == "GetFuxiExpressionTab" then
        return wnd.transform:Find("Anchor/Expressions/btns/TabBtn2").gameObject
    end
    if methodName == "GetFuxiExpressionAddBtn" then
        local gridtrans = wnd.transform:Find("Anchor/Expressions/ScrollView/Grid")
        if gridtrans then
            if gridtrans.childCount > 0 then
                local trans = gridtrans:GetChild(0)
                if trans and trans.name == "1" then 
                    return trans.gameObject
                end
            end
        end
    end

    return nil
end
function CLuaGuideMgr.GetSoulCoreWndGuideGo(go,methodName)
    local wnd=go:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
    if wnd then
        if methodName=="GetUnlockButton" then
            local viewWnd = wnd.m_AntiProView:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
            if viewWnd then
                return viewWnd:GetGuideGo(methodName)
            end
        end
    end
    return nil
end

function CLuaGuideMgr.GetSocialWndGuideGo(go, methodName)
    local wnd = go:GetComponent(typeof(CSocialWnd))
    if methodName == "GetCCTab" then
        return wnd:GetCCTab()
    elseif methodName=="GetOpenZhuboListButton" then
        return wnd:GetOpenZhuboListButton()
    elseif methodName =="GetCloseButton" then
        return wnd.closeButton
    elseif methodName == "GetCurrentTab" then  -- 友方频道
        return wnd:GetCurrentTab()
    elseif methodName == "GetChatRoomSetBtn" then  -- 茶室私密房间密码设置
        return wnd:GetChatRoomSetBtn()
    end
    return nil
end

function CLuaGuideMgr.GetGamePlayConfirmWndGuideGo(go,methodName)
    local wnd = go:GetComponent(typeof(CGamePlayConfirmWnd))
    if methodName == "GetChatButton" then
        return wnd.transform:Find("Anchor/CommonChatEntranceButton").gameObject
    else
        return nil
    end
end
function CLuaGuideMgr.GetTopAndRightTipWndGuideGo(go, methodName)
    local wnd = go:GetComponent(typeof(CTopAndRightTipWnd))
    if methodName == "GetShopBtn" then
        return wnd.shopButton
    else
        return nil
    end
end

function CLuaGuideMgr.GetTaskAndTeamWndGuideGo(go,methodName)
    local wnd = go:GetComponent(typeof(CTaskAndTeamWnd))
    if wnd then
        if methodName=="GetMissionButton" then
            return wnd.taskTab.gameObject;
        elseif methodName=="GetFirstMission" then
            if wnd.taskListBoard.gameObject.activeSelf then
                local ret = wnd.taskListBoard:GetFirstTaskGO()
                if not ret then
                    CGuideMgr.Inst:EndCurrentPhase()
                end
                return ret
            end
        elseif methodName=="GetTeamTab" then
            return wnd.teamTab.gameObject
        elseif methodName=="GetGuildTerritorialWarsPlayViewMapBtn" then
            if CUIManager.IsLoaded(CLuaUIResources.GuildTerritorialWarsTopRightWnd) then
                return CUIManager.instance:GetGuideGo("GuildTerritorialWarsTopRightWnd.GetGuildTerritorialWarsPlayViewMapBtn()")
            elseif CUIManager.IsLoaded(CLuaUIResources.GuildTerritorialWarsPeripheryAlertWnd) then
                return CUIManager.instance:GetGuideGo("GuildTerritorialWarsPeripheryAlertWnd.GetGuildTerritorialWarsPlayViewMapBtn()")
            end
            return wnd.taskListBoard.transform:Find("GuildTerritorialWarsView/MapBtn").gameObject
        elseif methodName == "GetGuanNingCommanderSignUpBtn" then
            return wnd.taskListBoard.transform:Find("GuanNingTaskView/CountDown/LeaveBtn").gameObject
        elseif methodName == "GetTeamRecruitButton" then
            return wnd.teamListBoard.teamRecruitBtn.gameObject
        end
    end
    return nil
end
function CLuaGuideMgr.GetFriendWndGuideGo(go,methodName)
    local wnd = go:GetComponent(typeof(CFriendWnd))
    if wnd then
        if methodName=="GetSearchTab" then
            return wnd.friendView:GetSearchTab()
        elseif methodName=="GetAddFriendButton" then
            return wnd.friendView:GetSearchView():GetAddFriendButton()
        elseif methodName=="GetSearchButton" then
            return wnd.friendView:GetSearchView():GetSearchButton()
        elseif methodName=="GetInputRegion" then
            return wnd.friendView:GetSearchView():GetInputRegion()
        elseif methodName=="Get1stTab" then
            return wnd.tabBar:GetTabGoByIndex(0)
        elseif methodName=="Get2ndTab" then
            return wnd.tabBar:GetTabGoByIndex(1)
        elseif methodName=="Get3rdTab" then
            return wnd.tabBar:GetTabGoByIndex(2)
        elseif methodName=="GetSpaceButton" then
            return wnd.friendView:GetSpaceButton()
        elseif methodName=="GetAddGroupIMButton" then
            if not CSwitchMgr.EnableGroupIM then
                if CGuideMgr.Inst:IsInPhase(EnumGuideKey.GroupIM) then
                    CGuideMgr.Inst:EndCurrentPhase()
                    return nil
                end
            end
            return wnd.groupIMView:GetAddGroupIMButton()
        end
    end
end
function CLuaGuideMgr.GetHouseMainWndGuideGo(go,methodName)
    local wnd = go:GetComponent(typeof(CHouseMainWnd))
    if wnd then
        if methodName=="Get1stTab" then
            return wnd.tabView:GetTabGameObject(0)
        elseif methodName=="GetGoHomeButton" then
            return wnd.transform:Find("Anchor/Info/BtnGoHome").gameObject
        end
    end
end

function  CLuaGuideMgr.GetMiniMapPopWndGuideGo( go,methodName )
    local wnd = go:GetComponent(typeof(CMiniMapPopWnd))
    if wnd then
        if methodName=="GetNpcFilterButton" then
            return wnd.m_NpcInfoButton
        elseif methodName == 'GetSanxingGuideNode' then
            return wnd.transform:Find('Contents/BigMap/TablePanel/SanxingGamePanel/TipButton').gameObject
        elseif methodName == "GetJinShaJingMap" then
            return wnd.transform:Find('Contents/BigMap/WorldMap/jinshajing').gameObject
        elseif methodName == "GetBackToMenPaiButton" then
            return CLuaMiniMapPopWnd.m_BackToMenPaiButton
        elseif methodName == "GetWorldMapButton" then
            return wnd.m_SwitchButton.gameObject
        elseif methodName == "GetCloseBtn" then
            return wnd.m_CloseButton
        elseif methodName == "GetEnemyMenPaiButton" then
            if CLuaMiniMapPopWnd.m_EnemyMenPaiButton then
                return CLuaMiniMapPopWnd.m_EnemyMenPaiButton
            elseif CLuaMiniMapPopWnd.m_ZongMenButtonInited or (CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.SectId == 0) then
                COpenEntryMgr.Inst:SaveGuidePhase(0, 0)
                CGuideMgr.Inst:EndCurrentPhase()
                return nil
            end
        elseif methodName == "GetCounterattackNpc" then
            if CGuanNingMgr.Inst.inGnjc then
                return CLuaGuanNingMgr.m_CounterattackPopMapIcon[1] or CLuaGuanNingMgr.m_CounterattackPopMapIcon[2]
            else
                return nil
            end
        elseif methodName == "GetNanDuFanHuaButton" then
            return wnd.transform:Find('Contents/BigMap/WorldMap/nandufanhua').gameObject
        end
    end
    return nil
end

-- function CLuaGuideMgr.GetEquipWordAutoBaptizeWndGuideGo(go,methodName)
--     local wnd = go:GetComponent(typeof(CEquipWordAutoBaptizeWnd))
--     if wnd then
--         if methodName == "GetTargetButton" then
--             return wnd.targetBtn
--         end
--     end
-- end

function CLuaGuideMgr.GetMainPlayerFrameGuideGo(go,methodName)
    local wnd = go:GetComponent(typeof(MainPlayerFrame))
    if wnd then
        if methodName == "GetPortraitGo" then
            return wnd.portraitButton
        elseif methodName == "GetEnhanceButton" then
            if wnd.enhanceBtn.activeSelf then
                return wnd.enhanceBtn
            end
        end
    end
end

function CLuaGuideMgr.GetSkillButtonBoardGuideGo(go,methodName)
    -- print("GetSkillButtonBoardGuideGo")
    local wnd = go:GetComponent(typeof(CSkillButtonBoardWnd))
    if wnd then
        if methodName == "GetHpRecoverButton" then
            return wnd:GetHpRecoverButton()
        elseif methodName == "GetSwitchTargetButton" then
            return wnd:GetSwitchTargetButton()
        elseif methodName == "Get1stButton" then
            return wnd:Get1stButton()
        elseif methodName == "Get1stButton2" then
            return wnd:Get1stButton2()
        elseif methodName == "Get2ndButton" then
            return wnd:Get2ndButton()
        elseif methodName == "Get2ndButton2" then
            return wnd:Get2ndButton2()
        elseif methodName == "Get3rdButton" then
            return wnd:Get3rdButton()
        elseif methodName == "GetSwitchGo" then
            return wnd:GetSwitchGo()
        elseif methodName == "GetSwitchSkillButton" then
            return wnd:GetSwitchSkillButton()
        elseif methodName == "GetJuejiIcon" then
            return wnd:GetJuejiIcon()
        elseif methodName == "Get1stAdditionalButton" then
            return wnd.additionalButtonInfos[5].gameObject
        elseif methodName == "Get3rdButton2" then
            if wnd.additionalButtonInfos.Length >= 3 then
                return wnd.additionalButtonInfos[2].gameObject
            else
                return nil
            end
        elseif methodName == "Get4thButton2" then
            if wnd.additionalButtonInfos.Length >= 4 then
                return wnd.additionalButtonInfos[3].gameObject
            else
                return nil
            end
        elseif methodName == "Get5thButton2" then
            if wnd.additionalButtonInfos.Length >= 5 then
                return wnd.additionalButtonInfos[4].gameObject
            else
                return nil
            end
        elseif methodName == "Get6thButton2" then
            if wnd.additionalButtonInfos.Length >= 8 then
                return wnd.additionalButtonInfos[7].gameObject
            else
                return nil
            end
        elseif methodName == "GetSeaFishingButton" then
            local luawnd = go:GetComponent(typeof(CCommonLuaScript))
            if luawnd then
                return luawnd.m_LuaSelf:GetGuideGo(methodName)
            else
                return nil
            end
        elseif methodName == "GetWeddingHelpAnswerButton" then
            local luawnd = go:GetComponent(typeof(CCommonLuaScript))
            if luawnd then
                return luawnd.m_LuaSelf:GetGuideGo(methodName)
            else
                return nil
            end
        elseif methodName == "GetFaZhenBtn" then
            local luawnd = go:GetComponent(typeof(CCommonLuaScript))
            if luawnd then
                return luawnd.m_LuaSelf:GetGuideGo(methodName)
            else
                return nil
            end
        else
            return nil
        end
    end
end
function CLuaGuideMgr.GetMainPlayerWndGuideGo(go,methodName)
    local wnd = go:GetComponent(typeof(CMainPlayerWnd))
    if wnd then
        if methodName == "GetScoreTab" then
            return wnd.tabBar:GetTabGoByIndex(1)
        elseif methodName == "GetGuanNingExchangeButton" then
            local tf = wnd.scoreView.transform:Find("ScrollView/Table")
            for i=1,tf.childCount do
                local child = tf:GetChild(i-1)
                local label = child:Find("DisplayNameLabel"):GetComponent(typeof(UILabel))
                if label.text == LocalString.GetString("关宁校场积分") then
                    return child.gameObject
                end
            end
        elseif methodName == "GetXingGuanButton" then
            return wnd.playerPropertyView:GetXingGuanButton()
        elseif methodName == "GetAchievementTab" then
            return wnd.tabBar:GetTabGoByIndex(2)
        elseif methodName =="GetNpcHaoGanDuButton" then
            return wnd.achieveView.transform:Find("HaoGanDuButton").gameObject
        elseif methodName =="GetWuXingButton" then
            return wnd.playerPropertyView.transform:Find("TopPanel/WuXingBtn").gameObject
        elseif methodName =="GetSoulCoreButton" then
            return wnd.playerPropertyView.transform:Find("BtnsPanel/SoulCoreBtn").gameObject
        elseif methodName =="GetPropertyName" then
            local luaScrip = wnd.playerPropertyView.basicPropertyView.gameObject:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
            return luaScrip.Attack.transform:Find("GuideGo").gameObject
        end
    end
    return nil
end
function CLuaGuideMgr.GetYanChiXiaWorkWndGuideGo(go, methodName)
    local wnd = go:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
    if wnd then
        if methodName == "GetJiuQiIcon" then
            local go1 = wnd.JiuQiArea
            if go1 then
                local widget = go1:GetComponent(typeof(UIWidget))
                if widget and not go1:GetComponent(typeof(BoxCollider)) then
                    local collider = go1:AddComponent(typeof(BoxCollider))
                    collider.size = Vector3(widget.width,widget.height)
                end
            end
            return go1
        elseif methodName == "GetShouCeArea" then
            local go1 = wnd.JiuQiArea
            if go1 then
                local widget = go1:GetComponent(typeof(UIWidget))
                if widget then
                    local col1 = go1:GetComponent(typeof(BoxCollider))
                    if col1 then
                        MonoBehaviour.Destroy(col1)
                    end
                end
            end
            local go2 = wnd.TabArea
            if go2 then
                local widget = go2:GetComponent(typeof(UIWidget))
                if widget then
                    widget.depth = 16
                    if not go2:GetComponent(typeof(BoxCollider)) then
                        local collider = go2:AddComponent(typeof(BoxCollider))
                        collider.size = Vector3(widget.width,widget.height)
                    end
                end
            end
            return go2
        elseif methodName == "GetTaskArea" then
            local go2 = wnd.TabArea
            if go2 then
                local widget = go2:GetComponent(typeof(UIWidget))
                if widget then
                    widget.depth = 1
                    local col2 = go2:GetComponent(typeof(BoxCollider))
                    if col2 then
                        MonoBehaviour.Destroy(col2)
                    end
                end
            end
            local go3 = wnd.TaskArea
            if go3 then
                local widget = go3:GetComponent(typeof(UIWidget))
                if widget and not go3:GetComponent(typeof(BoxCollider)) then
                    local collider = go3:AddComponent(typeof(BoxCollider))
                    collider.size = Vector3(widget.width,widget.height)
                end
            end
            return go3
        elseif methodName == "GetFlagonIcon" then
            local go3 = wnd.TaskArea
            if go3 then
                local widget = go3:GetComponent(typeof(UIWidget))
                if widget then
                    local col3 = go3:GetComponent(typeof(BoxCollider))
                    if col3 then
                        MonoBehaviour.Destroy(col3)
                    end
                end
            end
            local go4 = wnd.FlagonArea
            if go4 then
                local widget = go4:GetComponent(typeof(UIWidget))
                if widget and not go4:GetComponent(typeof(BoxCollider)) then
                    local collider = go4:AddComponent(typeof(BoxCollider))
                    collider.size = Vector3(widget.width,widget.height)
                end
            end
            return go4
        elseif methodName == "GetExplanationBtn" then
            local go4 = wnd.FlagonArea
            if go4 then
                local widget = go4:GetComponent(typeof(UIWidget))
                if widget then
                    local col4 = go4:GetComponent(typeof(BoxCollider))
                    if col4 then
                        MonoBehaviour.Destroy(col4)
                    end
                end
            end
            local go5 = wnd.ExplanationIcon.gameObject
            return go5
        end
    end
    return nil
end
function CLuaGuideMgr.GetHousePopupMenuGuideGo(go, methodName)
    local wnd = go:GetComponent(typeof(CHousePopupMenu))
    if wnd then
        if methodName == "GetPlantButton" then
            for i=1,wnd.buttons.Length do
                local btn = wnd.buttons[i-1]
                local texture = btn.transform:Find("Mask/Icon"):GetComponent(typeof(CUITexture))
                if texture.lastPath == "UI/Texture/Transparent/Material/housepopupmenu_zhongzhi.mat" then
                    return btn
                end
            end
            return nil
        elseif methodName == "GetEnterButton" then
            for i=1,wnd.buttons.Length do
                local btn = wnd.buttons[i-1]
                local texture = btn.transform:Find("Mask/Icon"):GetComponent(typeof(CUITexture))
                if texture.lastPath == "UI/Texture/Transparent/Material/chengzhanwnd_jinchucheng.mat" then
                    return btn
                end
            end
            return nil
        elseif methodName == "GetFeedButton" then
            for i=1,wnd.buttons.Length do
                local btn = wnd.buttons[i-1]
                local texture = btn.transform:Find("Mask/Icon"):GetComponent(typeof(CUITexture))
                if texture.lastPath == "UI/Texture/Transparent/Material/babyclothwnd_weishi.mat" then
                    return btn
                end
            end
            return nil
        end
    end
end
function CLuaGuideMgr.GetPackageWndGuideGo(go, methodName)
    local wnd = go:GetComponent(typeof(CCommonLuaScript))
    if wnd then
        local packageView = wnd.m_LuaSelf.packageView

        local setFullyVisible = function(itemCellGo)
            if itemCellGo == nil then
                return
            end
            local panel = packageView.scrollView.panel
            local worldCorners = panel.worldCorners
            local childBounds = NGUIMath.CalculateAbsoluteWidgetBounds(itemCellGo.transform)
            if childBounds.min.y < worldCorners[0].y or childBounds.max.y > worldCorners[2].y then
                local b = NGUIMath.CalculateRelativeWidgetBounds(packageView.scrollView.transform, itemCellGo.transform)
                local dstY = panel.clipOffset.y + panel.baseClipRegion.y + panel.baseClipRegion.w * 0.5 - panel.clipSoftness.y
                local srcY = b.max.y
                packageView.scrollView:MoveRelative(Vector3(0,dstY - srcY,0))
            end
        end
        local tryEndGuide = function()
            if CClientMainPlayer.Inst ~= nil then
                local n = CClientMainPlayer.Inst.ItemProp:GetPlaceMaxSize(EnumItemPlace.Bag)
                if n == packageView.itemCells.Count then -- IsFullLoaded
                    if packageView.itemCells.Count > 0 then--如果真的没有这个物件
                        CGuideMgr.Inst:EndCurrentPhase()
                    end
                end
            end
        end

        if methodName == "GetEquipItem" then
            if packageView.curPlace == EnumItemPlace.Bag then
                for i=1,packageView.itemCells.Count do
                    local itemCell = packageView.itemCells[i-1]
                    local item = CItemMgr.Inst:GetById(itemCell.ItemId)
                    --性别要吻合
                    if item ~= nil and item.IsEquip and item.Equip.MainPlayerIsFit then
                        local rtn = itemCell.gameObject
                        setFullyVisible(rtn)
                        return rtn
                    end
                end
                tryEndGuide()
            end
            return nil
        elseif methodName == "GetLingShouItem" then
            if packageView.curPlace == EnumItemPlace.Bag then
                for i=1,packageView.itemCells.Count do
                    local itemCell = packageView.itemCells[i-1]
                    local item = CItemMgr.Inst:GetById(itemCell.ItemId)
                    if item ~= nil and item.IsItem and item.Item.Type == EnumItemType_lua.Pet and (item.TemplateId == 21000771 or item.TemplateId == 21000485) then
                        local rtn = itemCell.gameObject
                        setFullyVisible(rtn)
                        return rtn
                    end
                end
                tryEndGuide()
            end
            return nil
        elseif methodName == "GetZuoQiItem" then
            if packageView.curPlace == EnumItemPlace.Bag then
                for i=1,packageView.itemCells.Count do
                    local itemCell = packageView.itemCells[i-1]
                    local item = CItemMgr.Inst:GetById(itemCell.ItemId)
                    if item ~= nil and item.IsItem and item.Item.Type == EnumItemType_lua.Mount and CGuideMgr.CanZuoQiItemTriggerGuide(item) then
                        local rtn = itemCell.gameObject
                        setFullyVisible(rtn)
                        return rtn
                    end
                end
                tryEndGuide()
            end
            return nil
        elseif methodName == "GetWushanshiItem" then
            if packageView.curPlace == EnumItemPlace.Bag then
                for i=1,packageView.itemCells.Count do
                    local itemCell = packageView.itemCells[i-1]
                    local item = CItemMgr.Inst:GetById(itemCell.ItemId)
                    if item ~= nil and item.IsItem and item.Item.Type == EnumItemType_lua.WushanStone then
                        local rtn = itemCell.gameObject
                        setFullyVisible(rtn)
                        return rtn
                    end
                end
                --如果找不到 然后逻辑到这里 判断一下是不是正在异步加载 如果已经加载完了 还找不到 说明没有这个东西 结束引导
                tryEndGuide()
            end
            return nil
        elseif methodName == "GetDrugItem" then
            if packageView.curPlace == EnumItemPlace.Bag then
                if not packageView.clearupBtnRoot.activeSelf then
                    for i=1,packageView.itemCells.Count do
                        local itemCell = packageView.itemCells[i-1]
                        local item = CItemMgr.Inst:GetById(itemCell.ItemId)
                        if item ~= nil and item.IsItem and item.Item.Type == EnumItemType_lua.Drug then
                            local rtn = itemCell.gameObject
                            setFullyVisible(rtn)
                            return rtn
                        end
                    end
                    tryEndGuide()
                end
            end
            return nil
        elseif methodName == "GetFoodItem" then
            if packageView.curPlace == EnumItemPlace.Bag then
                for i=1,packageView.itemCells.Count do
                    local itemCell = packageView.itemCells[i-1]
                    local item = CItemMgr.Inst:GetById(itemCell.ItemId)
                    if item ~= nil and item.IsItem and item.Item.Type == EnumItemType_lua.Food then
                        local rtn = itemCell.gameObject
                        setFullyVisible(rtn)
                        return rtn
                    end
                end
                tryEndGuide()
            end
            return nil
        elseif methodName == "YaoGuaiWarehouseBtn" then
            return wnd.m_LuaSelf:YaoGuaiWarehouseBtn()
        elseif methodName == "GetSettingButton" then
            return packageView.settingBtn
        elseif methodName == "GetRedFoodItem" then
            if packageView.curPlace == EnumItemPlace.Bag then
                for i=1,packageView.itemCells.Count do
                    local itemCell = packageView.itemCells[i-1]
                    local item = CItemMgr.Inst:GetById(itemCell.ItemId)
                    if item ~= nil and item.IsItem and item.Item.Type == EnumItemType_lua.Food then
                        local data = Item_Item.GetData(item.Item.TemplateId)
                        if data ~= nil and data.CDGroup == 3 then
                            local rtn = itemCell.gameObject
                            setFullyVisible(rtn)
                            return rtn
                        end
                    end
                end
                tryEndGuide()
            end
            return nil
        elseif methodName == "GetBlueFoodItem" then
            if packageView.curPlace == EnumItemPlace.Bag then
                for i=1,packageView.itemCells.Count do
                    local itemCell = packageView.itemCells[i-1]
                    local item = CItemMgr.Inst:GetById(itemCell.ItemId)
                    if item ~= nil and item.IsItem and item.Item.Type == EnumItemType_lua.Food then
                        local data = Item_Item.GetData(item.Item.TemplateId)
                        if data ~= nil and data.CDGroup == 2 then
                            local rtn = itemCell.gameObject
                            setFullyVisible(rtn)
                            return rtn
                        end
                    end
                end
                tryEndGuide()
            end
            return nil
        elseif methodName == "GetZhanKuangWeaponItem" then
            for i=1,packageView.itemCells.Count do
                local itemCell = packageView.itemCells[i-1]
                local item = CItemMgr.Inst:GetById(itemCell.ItemId)
                --性别要吻合
                if item ~= nil and item.IsEquip and item.TemplateId == EquipBaptize_Setting.GetData().WarriorTransformGuideWeapon then
                    local rtn = itemCell.gameObject
                    setFullyVisible(rtn)
                    return rtn
                end
            end
            tryEndGuide()
            return nil
        elseif methodName == "GetZhuoYaoBtn" then
            return wnd.m_LuaSelf.m_YaoGuaiWarehouseBtn
        elseif methodName == "GetAppearanceBtn" then
            return packageView.playerInfoView.equipmentView.appearanceBtn
        elseif methodName == "GetPropertyTabBtn" then
            return packageView.playerInfoView.tabBar:GetTabGoByIndex(1)
        elseif methodName == "GetPropertyName" then
            local LuaSelf = packageView.playerInfoView.propertyView.gameObject:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
            return LuaSelf.Attack.transform:Find("GuideGo").gameObject
        elseif methodName == "GetYinPiaoExpandButton" then
            return packageView.moneyInfoView.yinpiaoArrow.gameObject
        end
    end
end

function CLuaGuideMgr.GetAppearancePreviewWndGuideGo(go,methodName)
  local wnd = go:GetComponent(typeof(CCommonLuaScript))
  if wnd then
    if methodName == "GetSettingBtn" then
        return wnd.m_LuaSelf.settingBtn.gameObject
    end

    local fashionView = wnd.m_LuaSelf.fashionView:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf

    if methodName == "GetRanFaBtn" then
        if fashionView then
            return fashionView.ranFaBtn
        else
            return nil
        end
    elseif methodName == "GetSWBtn1" or methodName == "GetSWBtn2" then
        if fashionView then
            return fashionView.swShowNode
        else
            return nil
        end
    elseif methodName == "GetHeadAlphaBtn" then
        return fashionView.headAlphaPreviewBtn.gameObject
    elseif methodName == "GetSkillAppearanceBtn" then
        return go.transform:Find("Panel/shopRoot/switchBg/luminousSwitchView/skillGroupPreviewBtn").gameObject
    elseif methodName == "GetSkillAppearanceItem" then
        local view = go.transform:Find("Panel/shopRoot/luminousView"):GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
        if view then
            return view:GetSkillAppearanceItem()
        end
        return nil
    end
  end
end

function CLuaGuideMgr.GetDialogWndGuideGo(go,methodName)
    if methodName == "GetInviteNpc2SectGo" then
        local wnd = go:GetComponent(typeof(CCommonLuaScript))
        if wnd then
            local tableList = {"Anchor/Selections/ScrollView/Table","Anchor/Selections2/ScrollView/Table"}
            for _,name in pairs(tableList) do
                local table = wnd.m_LuaSelf.transform:Find(name)
                for i=0,table.childCount-1,1 do
                    local tf = table:GetChild(i)
                    local label = tf:Find("Label"):GetComponent(typeof(UILabel))
                    if label and label.text == LocalString.GetString("邀请贤士加入宗派") then
                        return tf.gameObject
                    end
                end
            end    
        end
    end  
    return nil
end

function CCityWarUnit.m_OnMouseDownNotSelected_Lua(this)
    --是否在自己城里
    if this.Type==3 then
        if COpenEntryMgr.Inst:GetGuideRecord(55) then
            return
        end
        if CCityWarMgr.Inst.PlaceMode then
            return
        end
        if CLuaCityWarMgr.IsOwnCurrentCity() then
            -- if not COpenEntryMgr.Inst:GetTriggeredState(TriggerOnceGuideType.FirstSelectChengMen) then
                -- COpenEntryMgr.Inst:SetTriggeredState(TriggerOnceGuideType.FirstSelectChengMen)
                g_MessageMgr:ShowMessage("FirstSelectChengMen")
            -- end
        end
    end
end
function CCityWarUnit.m_OnLongPressed_Lua(this)
    if this.Type==3 then
        if COpenEntryMgr.Inst:GetGuideRecord(55) then
            return
        end
        if CCityWarMgr.Inst.PlaceMode then
            return
        end
        if CLuaCityWarMgr.IsOwnCurrentCity()  then
            local triggered = CGuideMgr.Inst:StartGuide(55, 0)
            if triggered then
                COpenEntryMgr.Inst:SetGuideRecord(55)
            end
        end
    end
end

function CGuideMgr.m_OnFinishTask_Lua(taskId)
    if taskId==22203558 then
        CLuaGuideMgr.TryTriggerSettingGuide()
    end
end

function CGuideMgr.m_OnAcceptTask_Lua(taskId)
    if taskId==22130310 then
        if COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.JinShaJingMap) then
            return
        end
        --触发引导打开大地图
        CLuaMiniMapPopWnd.m_ShowWorldMap = true--直接打开世界地图
        CUIManager.ShowUI(CUIResources.MiniMapPopWnd)

        local triggered = CGuideMgr.Inst:StartGuide(EnumGuideKey.JinShaJingMap, 0)
        if triggered then
            COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.JinShaJingMap)
        end
    elseif taskId==22203400 then
        --新老玩家选择
        CLuaGuideMgr.TryTriggerUserTypeChoose()
    else
        --切线引导
        CLuaGuideMgr.TryTriggerQieXianGuide(taskId)
    end
end
function CLuaGuideMgr.TryTriggerQieXianGuide(taskId)
    if COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.QieXian) then
        return
    end
    local trigger=false
    local sceneTemplateId = CScene.MainScene.SceneTemplateId
    Guide_QieXianGuide.Foreach(function(key,value)
        if taskId==key and value.MapID==sceneTemplateId then
            trigger=true
        end
    end)
    if trigger then
        Gac2Gas.QueryQieXianGuideScenePlayerCount()
    end
end

function CLuaGuideMgr.SendQieXianGuideScenePlayerCount(playerCount)
    if playerCount>50 then--本分线人数＞50
        local triggered = CGuideMgr.Inst:StartGuide(EnumGuideKey.QieXian, 0)
        if triggered then
            COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.QieXian)
        end
    end
end


--触发npc过滤引导
function CLuaGuideMgr.TryTriggerNpcFilterGuide()
    if not CGuideMgr.Inst:IsInPhase(EnumGuideKey.JinShaJingMap) then
        CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.MiniMapNpcFilter)
    end
end

function CLuaGuideMgr.TryTriggerUserTypeChoose()
    if not CClientMainPlayer.Inst then return end
    --看看有没有做过新老玩家判断
    if not COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.ChooseUserType) then
    -- if CClientMainPlayer.Inst.Level==1  then
            --新老玩家选择
            -- local firstTaskId = GameSetting_Client_Wapper.Inst.NewPlayerFirstTaskId
            if CLoginMgr.ExistOldUser then-- and not CClientMainPlayer.Inst.TaskProp:IsMainPlayerTaskFinished(firstTaskId) then
                -- CUIManager.ShowUI(CLuaUIResources.GuideChooseWnd)
                CGuideMgr.Inst:StartGuide(EnumGuideKey.ChooseUserType, 0)
                COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.ChooseUserType)
                return
            end
        -- end
    end
    CLuaGuideMgr.TryTriggerTaskGuide()
end
--任务寻路引导
function CLuaGuideMgr.TryTriggerTaskGuide()
    CGuideMgr.Inst:StartGuide(EnumGuideKey.TaskGuide, 0)

end

function CLuaGuideMgr.TryTriggerSettingGuide()
    local triggered = CGuideMgr.Inst:StartGuide(EnumGuideKey.SettingGuide, 0)
    if triggered then
        COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.SettingGuide)
    end
end

CGuideMgr.m_hookOnGuideProcessFinish = function(this,phase)
    if phase==EnumGuideKey.SettingGuide then
        --设置引导结束，如果是pc，则直接触发任务寻路
        --手机上的话，需要触发手指操控引导
        if not CommonDefs.IsPCGameMode() then
            CLuaGuideMgr.ShowFingerGuide1()
        end
    end
end

function CGuideMgr.m_OnMainPlayerCreated_Lua(this)
    if CQuanMinPKMgr.Inst.m_IsQuanMinPKServer then
        --引导逻辑会CloseAllPopupViews，所以把逻辑放后面
        CLuaEntryUIMgr.OnMainPlayerCreated()
        return
    end
    if not CClientMainPlayer.Inst then return end

    local curLevel = CClientMainPlayer.Inst.MaxLevel
    if curLevel>=31 then--尝试触发坐骑引导 改31是为了不和一条龙引导冲突
        CGuideMgr.Inst:TryTriggerFirstGetZuoQiGuide()
    end

    local sceneTemplateId = CScene.MainScene.SceneTemplateId

    if sceneTemplateId == 16100012 then--FirstEnterGuanNing
        if not COpenEntryMgr.Inst:GetGuideRecord(23) then
            CUIManager.instance:CloseAllPopupViews()
            local triggered = CGuideMgr.Inst:StartGuide(23, 0)
            if triggered then
                COpenEntryMgr.Inst:SetGuideRecord(23)
            end
        end
    elseif sceneTemplateId == 16100050 then--FirstEnterGuildLeagueViceBattle
        if not COpenEntryMgr.Inst:GetGuideRecord(28) then
            CUIManager.instance:CloseAllPopupViews()
            local triggered = CGuideMgr.Inst:StartGuide(28, 0)
            if triggered then
                COpenEntryMgr.Inst:SetGuideRecord(28)
            end
        end
    elseif sceneTemplateId == 16100049 then--FirstEnterGuildLeagueMainBattle
        if not COpenEntryMgr.Inst:GetGuideRecord(29) then
            CUIManager.instance:CloseAllPopupViews()
            local triggered = CGuideMgr.Inst:StartGuide(29, 0)
            if triggered then
                COpenEntryMgr.Inst:SetGuideRecord(29)
            end
        end
    elseif sceneTemplateId == 16101788 then
        if not LuaZongMenMgr.m_ZongMenProp then
            if LuaZongMenMgr.m_IsOpen then
                Gac2Gas.GetSectXinWuInfo()
            end
        elseif CClientMainPlayer.Inst and LuaZongMenMgr:IsMySectScene() and LuaZongMenMgr.m_ZongMenProp.LeaderId == CClientMainPlayer.Inst.Id then
            CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.ReCommandZongMen)
        end   
    end
    --FirstEnterHouse
    if CClientHouseMgr.Inst:IsCurHouseOwner() and not CLuaHouseMinimap.s_IsHouseCompetitionGuide then
        if not COpenEntryMgr.Inst:GetGuideRecord(43) then
            CUIManager.instance:CloseAllPopupViews()
            local triggered = CGuideMgr.Inst:StartGuide(43, 1)
            if triggered then
                COpenEntryMgr.Inst:SetGuideRecord(43)
            end
        end
    end

    --CheckGetMiaoPu3
    --FirstEnterTowerDefense
    CLuaGuideMgr.TryTriggerBabyGuide()
    --CLuaGuideMgr.TryTriggerWuJianDiYuGuide()
    CLuaGuideMgr.TryTriggerNewSceneGuide()

    --引导逻辑会CloseAllPopupViews，所以把逻辑放后面
    CLuaEntryUIMgr.OnMainPlayerCreated()
end
function CLuaGuideMgr.TryTriggerNewSceneGuide()
    if not CGuideMgr.Inst.s_EnablePrefToolBox then return end
    if not CLuaGuideMgr.EnableGuide() then return end
    if COpenEntryMgr.Inst:GetTriggeredState(TriggerOnceGuideType.PerfSet) then--这是老玩家
        if not COpenEntryMgr.Inst:GetTriggeredState(TriggerOnceGuideType.PerfSet2) then
            CUIManager.ShowUI(CUIResources.PerfToolBox)
            COpenEntryMgr.Inst:SetTriggeredState(TriggerOnceGuideType.PerfSet2)
        end
    end
end
function CGuideMgr.m_OnMainPlayerLevelChange_Lua(this)
    if CQuanMinPKMgr.Inst.m_IsQuanMinPKServer then
        return
    end
    if not CClientMainPlayer.Inst then return end
    --
    local curLevel = CClientMainPlayer.Inst.MaxLevel
    --帮会引导 20
    --一条龙 30
    --玩家商店 40
    --个人空间 35
    --直播 28
    --家园 65
    --baby 65+封印


    if curLevel>=31 then--尝试触发坐骑引导
        if CGuideMgr.Inst:TryTriggerFirstGetZuoQiGuide() then return end
    end


    CLuaGuideMgr.TryTriggerBabyGuide()
end

function CGuideMgr.m_OnGetNewSkill_Lua(this,newSkillId)
    CLuaGuideMgr.OnGetNewSkill(newSkillId)
end

function CLuaGuideMgr.TryTriggerCityWarPlaceGuide()
    if not CLuaGuideMgr.EnableGuide() then return end
    --是否引导过
    if COpenEntryMgr.Inst:GetGuideRecord(54) then
        return
    end
    --判断玩家职位
    if not CGuildMgr.Inst.CanCityBuild then
        return
    end

    --判断玩家位置是否在自己城池内
    if CLuaCityWarMgr:IsMainPlayerInOwnCity() then
        local triggered = CGuideMgr.Inst:StartGuide(54, 0)
        if triggered then
            COpenEntryMgr.Inst:SetGuideRecord(54)
        end
    end
end


function CLuaGuideMgr.TryTriggerCityWarSecondaryMapGuide()
    if not CLuaGuideMgr.EnableGuide() then return end
    if COpenEntryMgr.Inst:GetGuideRecord(64) then
        return
    end
    local triggered = CGuideMgr.Inst:StartGuide(64, 0)
    if triggered then
        COpenEntryMgr.Inst:SetGuideRecord(64)
    end
end

function CLuaGuideMgr.TryTriggerCityWarChallengeGuide()
    if not CLuaGuideMgr.EnableGuide() then return end
    if COpenEntryMgr.Inst:GetGuideRecord(65) then
        return
    end
    local triggered = CGuideMgr.Inst:StartGuide(65, 0)
    if triggered then
        COpenEntryMgr.Inst:SetGuideRecord(65)
    end
end

CReliveWnd.m_TryTriggerCityWarReliveGuide=function(wnd)
    if CCityWarMgr.Inst:IsInMirrorWarScene() then
        if COpenEntryMgr.Inst:GetGuideRecord(66) then
            return
        end
        local triggered = CGuideMgr.Inst:StartGuide(66, 0)
        if triggered then
            COpenEntryMgr.Inst:SetGuideRecord(66)
        end
    end
end

--养育引导
function CLuaGuideMgr.TryTriggerBabyGuide()
    if not CLuaGuideMgr.EnableGuide() then return end

    if not CSwitchMgr.EnableYangYu then
        return
    end

    if CClientMainPlayer.Inst.IsInGamePlay then
        return
    end

    if COpenEntryMgr.Inst:GetGuideRecord(60) then
        return
    end
    local level = CClientMainPlayer.Inst and  CClientMainPlayer.Inst.MaxLevel or 0
    if level>=65 then
        local sealId = CClientMainPlayer.Inst.PlayProp.SealId
        if sealId>2 then
            COpenEntryMgr.Inst:SetOpenData(COpenEntryMgr.OpenType.Baby)
            local triggered = CGuideMgr.Inst:StartGuide(60, 0)
            if triggered then
                COpenEntryMgr.Inst:SetGuideRecord(60)
            end
        end
    end
end
--宝宝升级到幼儿阶段的引导
function CLuaGuideMgr.TryTriggerBabyUpgradeGuide(babyId)
    if not CLuaGuideMgr.EnableGuide() then return end
    if not CSwitchMgr.EnableYangYu then
        return
    end
    local baby = BabyMgr.Inst:GetBabyById(babyId)
    if baby and baby.Grade>=11 then
        if COpenEntryMgr.Inst:GetGuideRecord(62) then
            return
        end
        local triggered = CGuideMgr.Inst:StartGuide(62, 0)
        if triggered then
            COpenEntryMgr.Inst:SetGuideRecord(62)
        end
    else
        if CGuideMgr.Inst:IsInPhase(62) then
            CGuideMgr.Inst:EndCurrentPhase()
        end
    end
end

function MouseInputHandler.m_OnSelectBaby_Lua(this,baby)
    if COpenEntryMgr.Inst:GetGuideRecord(63) then
        return
    end
    if baby.TemplateId==20021220 or baby.TemplateId==20021221 then
        COpenEntryMgr.Inst:SetGuideRecord(63)
        g_MessageMgr:ShowMessage("FirstSelectBaby")
    end
end
function CLuaGuideMgr.OnInteractWithBaby(babyIteractType)
    if not CLuaGuideMgr.EnableGuide() then return end
    if babyIteractType~=4 then
        return
    end

    if COpenEntryMgr.Inst:GetGuideRecord(61) then
        return
    end
    local triggered = CGuideMgr.Inst:StartGuide(61, 0)
    if triggered then
        COpenEntryMgr.Inst:SetGuideRecord(61)
    end
end

--轻鸿玉佩引导 每次今玩法触发
function CLuaGuideMgr.TriggerQingHongFlySkillGuide(blink)
    if not CLuaGuideMgr.EnableGuide() then return end
    if not blink then
        if CGuideMgr.Inst:IsInPhase(EnumGuideKey.QingHongFly) then
            CGuideMgr.Inst:EndCurrentPhase()
        end
        return
    end
    --引导过程中可能再次触发
    if blink then
        if CGuideMgr.Inst:IsInPhase(EnumGuideKey.QingHongFly) then
            return
        end
    end
    if LuaQingHongFlyMgr.m_TriggerGuide2 then return end
    if blink then
        LuaQingHongFlyMgr.m_TriggerGuide2 = true
        CGuideMgr.Inst:StartGuide(EnumGuideKey.QingHongFly, 0)
    end
end

function CLuaGuideMgr.TriggerCookingPlayGuide()
    if not CLuaGuideMgr.EnableGuide() then return end
	if COpenEntryMgr.Inst:GetGuideRecord(70) then
		return
	end
	local triggered = CGuideMgr.Inst:StartGuide(70, 0)
    if triggered then
	    COpenEntryMgr.Inst:SetGuideRecord(70)
    end
end


function CLuaGuideMgr.CheckOpenHouseCompetition()
    if not CLuaGuideMgr.EnableGuide() then return end
    if CClientHouseMgr.Inst:IsPlayerInHouse() then
        if CHouseCompetitionMgr.Inst:IsInHouseCompetitionFirstStage() then

            if COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.HouseCompetition2020) then
                return
            end
            CLuaGuideMgr.UnlockHouseCompetitionMenuItem()
	        COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.HouseCompetition2020)
        end
    end
end

function CLuaGuideMgr.UnlockHouseCompetitionMenuItem()
    if not CLuaGuideMgr.EnableGuide() then return end
    CLuaHouseMinimap.s_IsHouseCompetitionGuide = true
    local button = CUIManager.instance:GetGuideGo("HouseMinimap.GetHouseCompetitionButton()")
    if button ~= nil then
        CGuideMgr.Inst.inForceGuide = true
        CUIManager.blockAllEvent = true
        CUIManager.instance:HideOverlayUI()

        CMenuGuideMgr.PlayUnlockMenuItemEffect(LocalString.GetString("家园设计大赛"), LocalString.GetString("盛大开启"), button.transform, 2, DelegateFactory.Action_Vector3(function (initialWorldPos)
            local function callback()
                CUIManager.instance:ShowAll()
                CGuideMgr.Inst.inForceGuide = false
                CUIManager.blockAllEvent = false
                CLuaHouseMinimap.s_IsHouseCompetitionGuide = false
                --刷新按钮显示
                g_ScriptEvent:BroadcastInLua("OnShowQiyuanBtnFirstGoHome")
            end

            if CUIManager.IsLoaded(CUIResources.HouseMinimap) then
                local gameObject = CUIManager.instance:GetGameObject(CUIResources.HouseMinimap)
                local Grid = CommonDefs.GetComponent_Component_Type(gameObject.transform:Find("Anchor/Tip"), typeof(UIGrid))
                if gameObject.activeSelf then
                    --算出一个位置
                    button.transform.parent = Grid.transform
                    local count = Grid.transform.childCount
                    button.transform:SetSiblingIndex(0)

                    local posTo = Grid.transform:TransformPoint(CreateFromClass(Vector3, -Grid.cellWidth * (count - 2), 0))

                    button.gameObject:SetActive(true)
                    button.transform.position = initialWorldPos
                    button.transform.localScale = Vector3.one
                    local tweener = CommonDefs.SetEase_Tweener(LuaTweenUtils.DOMove(button.transform, posTo, 1, false), Ease.InOutCirc)
                    CommonDefs.OnComplete_Tweener(tweener, DelegateFactory.TweenCallback(function ()
                        callback()
                        Grid:Reposition()
                    end))
                else
                    callback()
                end
            else
                callback()
            end
        end))
    end
end

--第一次进入无间地狱弹出
function CLuaGuideMgr.TryTriggerWuJianDiYuGuide()
    if not CLuaGuideMgr.EnableGuide() then return end
    --if CScene.MainScene then
    local gamePlayId=CScene.MainScene.GamePlayDesignId
    if gamePlayId == LuaWuJianDiYuMgr.gamePlayId then--无间地狱
        if COpenEntryMgr.Inst:GetGuideRecord(74) then
            return
        end
        COpenEntryMgr.Inst:SetGuideRecord(74)
        LuaWuJianDiYuMgr:ShowRuleWnd()
    end
    --end
end

function CLuaGuideMgr.TryTriggerZhuoGuiGuide()
    if not CLuaGuideMgr.EnableGuide() then return end
    if COpenEntryMgr.Inst:GetGuideRecord(71) then
        return false
    end
    local triggered = CGuideMgr.Inst:StartGuide(71, 0)
    if triggered then
        COpenEntryMgr.Inst:SetGuideRecord(71)
    end
    return true
end

function CLuaGuideMgr.TryTriggerXiaoxieZombieJumpSkillGuide(skillId, additionSkillButtonInfo)
    if not CLuaGuideMgr.EnableGuide() then return end
    if not skillId or skillId ~= ZhuJueJuQing_Setting.GetData().XiaoxieZombieJumpSkillId
        or not additionSkillButtonInfo or not additionSkillButtonInfo.GuideCircle or additionSkillButtonInfo.GuideCircle.gameObject.activeSelf then return end

    local taskId = ZhuJueJuQing_Setting.GetData().XiaoxieZombieJumpTaskId
    local taskData = taskId and CTaskMgr.Inst:IsInProgress(taskId) and Task_Task.GetData(taskId)
    local gamePlayId = taskData and taskData.GamePlayId
    if gamePlayId and CScene.MainScene.GamePlayDesignId == gamePlayId then
        additionSkillButtonInfo.GuideCircle.transform.localPosition = additionSkillButtonInfo.transform.localPosition
        additionSkillButtonInfo.GuideCircle.gameObject:SetActive(true)
    end
end

-- function Gas2Gac.PlayerAboutToEnterNewRoleGamePlay()
--     print("PlayerAboutToEnterNewRoleGamePlay")
--     CGuideMgr.Inst:StartGuide(1, 0)
-- end

function CLuaGuideMgr.TryTriggerGuanNingXunLianGuide()
end

function CLuaGuideMgr.TryTriggerTalismanExchageGuide()
    if not CLuaGuideMgr.EnableGuide() then return end
    if COpenEntryMgr.Inst:GetGuideRecord(100)then
        return false
    end
    COpenEntryMgr.Inst:SetGuideRecord(100)
    return true
end

function CLuaGuideMgr.TryTriggerFallingDownTreasureGuide()
    if not CLuaGuideMgr.EnableGuide() then return end
    if COpenEntryMgr.Inst:GetGuideRecord(77)then
        return false
    end
    local triggered = CGuideMgr.Inst:StartGuide(77, 0)
    if triggered then
        COpenEntryMgr.Inst:SetGuideRecord(77)
        return true
    end
    return false
end

CLuaGuideMgr.skillWndGuides = {7,8,9,21,30,37,52}
function CLuaGuideMgr.TryTriggerYingLingGetBianShenSkillGuide()
    if not CLuaGuideMgr.EnableGuide() then return end
    for _,guideLogic in pairs(CLuaGuideMgr.skillWndGuides) do
        if CGuideMgr.Inst:IsInPhase(guideLogic) then
            return false
        end
    end

    if COpenEntryMgr.Inst:GetGuideRecord(87)then
        return false
    end
    local triggered = CGuideMgr.Inst:StartGuide(87, 0)
    if triggered then
        COpenEntryMgr.Inst:SetGuideRecord(87)
        return true
    end
    return false
end

function CLuaGuideMgr.TryTriggerGuide(_guide_num)
    if not CLuaGuideMgr.EnableGuide() then return end
    if COpenEntryMgr.Inst:GetGuideRecord(_guide_num)then
        return false
    end
    local triggered = CGuideMgr.Inst:StartGuide(_guide_num, 0)
    if triggered then
        COpenEntryMgr.Inst:SetGuideRecord(_guide_num)
        return true
    end
    return false
end

function CLuaGuideMgr.TryTriggerSanxingGuide1()
    if not CLuaGuideMgr.EnableGuide() then return end
    if COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.SanxingGame3) then
        return false
    end
    LuaSanxingGamePlayMgr.InGuidePha1 = true
    CGuideMgr.Inst:StartGuide(EnumGuideKey.SanxingGame1, 0)
    return true
end
function CLuaGuideMgr.TryTriggerSanxingGuide1_1()
    if not CLuaGuideMgr.EnableGuide() then return end
    if COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.SanxingGame4) then
        return false
    end
    LuaSanxingGamePlayMgr.InGuidePha2 = true
    CGuideMgr.Inst:StartGuide(EnumGuideKey.SanxingGame1, 0)
    return true
end
function CLuaGuideMgr.CheckSanxingGuide2()
    if COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.SanxingGame2) then
        return true
    end
    return false
end
function CLuaGuideMgr.TryTriggerSanxingGuide2()
    if not CLuaGuideMgr.EnableGuide() then return false end
    if COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.SanxingGame3) then
        return false
    end
    LuaSanxingGamePlayMgr.InGuidePha1_2 = true
    CGuideMgr.Inst:StartGuide(EnumGuideKey.SanxingGame2, 0)
    return true
end
function CLuaGuideMgr.TryTriggerSanxingGuide2_1()
    if COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.SanxingGame4) then
        return false
    end
    LuaSanxingGamePlayMgr.InGuidePha2_2 = true
    CGuideMgr.Inst:StartGuide(EnumGuideKey.SanxingGame2, 0)
    return true
end
function CLuaGuideMgr.TryTriggerSanxingGuide3()
    if COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.SanxingGame3) then
        return false
    end
    CGuideMgr.Inst:StartGuide(EnumGuideKey.SanxingGame3, 0)
    return true
end
function CLuaGuideMgr.TryTriggerSanxingGuide4()
    if COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.SanxingGame4) then
        return false
    end
    CGuideMgr.Inst:StartGuide(EnumGuideKey.SanxingGame4, 0)
    return true
end


function CLuaGuideMgr.Get3rdSkillClass()
    if CClientMainPlayer.Inst then
        if CClientMainPlayer.Inst.Class  == EnumClass.YingLing then
            local gender = CClientMainPlayer.Inst.AppearanceGender
            if gender == EnumGender.Male then
                return 911002
            elseif gender == EnumGender.Female then
                return 911003
            end
        else
            return COpenEntryMgr.Inst:Get3rdSkillClass()
        end
    end
    return 0
end
function CLuaGuideMgr.Get6thSkillClass()
    if CClientMainPlayer.Inst then
        if CClientMainPlayer.Inst.Class  == EnumClass.YingLing then
            local gender = CClientMainPlayer.Inst.AppearanceGender
            if gender == EnumGender.Male then
                return 911004
            elseif gender == EnumGender.Female then
                return 911004
            elseif gender == EnumGender.Monster then
                return 911004
            end
        else
            return COpenEntryMgr.Inst:Get6thSkillClass()
        end
    end
    return 0
end

function CLuaGuideMgr.OnGetNewSkill(newSkillId)
    if not CLuaGuideMgr.EnableGuide() then return end
    if CClientMainPlayer.Inst.Class == EnumClass.YingLing then
        local gender = CClientMainPlayer.Inst.AppearanceGender
        if CClientMainPlayer.Inst.Level>=5 then
            local trigger = false
            if gender == EnumGender.Male and newSkillId==91100201 then
                trigger = true
            elseif gender == EnumGender.Female and newSkillId==91100301 then
                trigger = true
            end
            if trigger and not COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.Get3Skill)then
                local triggered = CGuideMgr.Inst:StartGuide(EnumGuideKey.Get3Skill, 1)
                if triggered then
                    COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.Get3Skill)
                end
            end
        end
        if CClientMainPlayer.Inst.Level>=10 then
            local trigger = false
            if gender == EnumGender.Male and newSkillId==91100401 then
                trigger = true
            elseif gender == EnumGender.Female and newSkillId==91100401 then
                trigger = true
            end

            if trigger and not COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.Get6Skill)then
                local triggered = CGuideMgr.Inst:StartGuide(EnumGuideKey.Get6Skill, 1)
                if triggered then
                    COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.Get6Skill)
                end
            end
        end
    else
        local count = 0
        CommonDefs.DictIterate(CClientMainPlayer.Inst.SkillProp.SkillMap, DelegateFactory.Action_object_object(function (___key, ___value)
            if ___key > 0 and not Skill_TempSkill.Exists(___key) then
                count = count + 1
            end
        end))

        if count==3 then
            if not COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.Get3Skill)then
                local triggered = CGuideMgr.Inst:StartGuide(EnumGuideKey.Get3Skill, 1)
                if triggered then
                    COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.Get3Skill)
                end
            end
        elseif count==6 then
            if not COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.Get6Skill)then
                local triggered = CGuideMgr.Inst:StartGuide(EnumGuideKey.Get6Skill, 1)
                if triggered then
                    COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.Get6Skill)
                end
            end
        end
    end
end


function CLuaGuideMgr.TryTriggerRemoteDouDiZhuGuide()
    if not CLuaGuideMgr.EnableGuide() then return end
    if COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.RemoteDouDiZhu) then
        return false
    end
    local triggered = CGuideMgr.Inst:StartGuide(EnumGuideKey.RemoteDouDiZhu, 0)
    if triggered then
        COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.RemoteDouDiZhu)
        return true
    end
    return false
end
function CLuaGuideMgr.TryEndRemoteDouDiZhuGuide()
    if CGuideMgr.Inst:IsInPhase(EnumGuideKey.RemoteDouDiZhu) then
        CGuideMgr.Inst:EndCurrentPhase()
    end
end
function CLuaGuideMgr.TryTriggerButterflyCrisis1Guide()
    if not CLuaGuideMgr.EnableGuide() then return end
    if COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.ButterflyCrisis) then
        return false
    end
    local triggered = CGuideMgr.Inst:StartGuide(EnumGuideKey.ButterflyCrisis, 0)
    if triggered then
        COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.ButterflyCrisis)
    end
end

function CLuaGuideMgr.TryTriggerButterflyCrisis2Guide()
    if not CLuaGuideMgr.EnableGuide() then return end
    if COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.ButterflyCrisis2) then
        return false
    end
    local triggered = CGuideMgr.Inst:StartGuide(EnumGuideKey.ButterflyCrisis2, 0)
    if triggered then
        COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.ButterflyCrisis2)
    end
end

CGuideMgr.m_hookNextSubPhase = function(this)
    if this.mPhase == EnumGuideKey.GuideZuDui and this.mSubphase == 0 then
        Gac2Gas.QuerySpokesmanContractSign(true)
    end
    this:TryExecuteNextSubPhase()
end


function CLuaGuideMgr.NeedWanShengJieSkyboxGuide()
    if CGuideMgr.Inst:IsInPhase(EnumGuideKey.HouseDecorate) or CGuideMgr.Inst:IsInPhase(EnumGuideKey.GetMiaoPu3)then
        return false
    end
    if not CLuaGuideMgr.EnableGuide() then return false end
    --期间
    local skyBoxData = House_SkyBoxKind.GetData(10)
    local inBuyDuration = CLuaHouseMgr.IsSkyBoxInBuyDuration(skyBoxData)
    if not inBuyDuration then return false end

    if COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.WanShengJieSkybox) then
        return false
    end
    return true
end

function CLuaGuideMgr.TryXianZhiSkillGuide()
    if CLuaXianzhiMgr.GetXianZhiSKillId()>0 then
        local triggered = CGuideMgr.Inst:StartGuide(EnumGuideKey.XianZhiSkill, 0)
        if triggered then
		    COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.XianZhiSkill)
        end
    end
end

function CLuaGuideMgr.TryRuYiSkillGuide()
    local triggered = CGuideMgr.Inst:StartGuide(EnumGuideKey.RuYiSkillGuide, 0)
    if triggered then
	    COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.RuYiSkillGuide)
    end
end

function CLuaGuideMgr.TryHuLuWaAdventureGuide()
    if COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.HuLuWaAdventureGuide) then
        return
    end

    CGuideMgr.Inst:StartGuide(EnumGuideKey.HuLuWaAdventureGuide, 0)
end

function Gas2Gac.TryStartGuide(phase, subPhase, bForce, bSave)
	if not bForce and not CLuaGuideMgr.EnableGuide() then
		return
	end
	if not bForce and COpenEntryMgr.Inst:GetGuideRecord(phase) then
		return
	end
	local triggered = CGuideMgr.Inst:StartGuide(phase, subPhase)
    if triggered then
        if bSave then
            COpenEntryMgr.Inst:SetGuideRecord(phase)
        end
    end
end

function Gas2Gac.TryEndGuide()
    -- 清除离开副本时，可能未终止的引导
    CGuideMgr.Inst:EndOpenGuide()
end


function CLuaGuideMgr.TryTriggerGamePlayConfirmChatGuide()
    if not CLuaGuideMgr.EnableGuide() then return end
    if COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.GamePlayConfirmChat) then
        return
    end
    local triggered = CGuideMgr.Inst:StartGuide(EnumGuideKey.GamePlayConfirmChat, 0)
    if triggered then
        COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.GamePlayConfirmChat)
    end
end

function CLuaGuideMgr.TryTriggerLingShouLevelLimitGuide()
    if not CLuaGuideMgr.EnableGuide() then return end
    if COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.LingShouLevelLimit) then
        return
    end
    local triggered = CGuideMgr.Inst:StartGuide(EnumGuideKey.LingShouLevelLimit, 0)
    if triggered then
        COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.LingShouLevelLimit)
    end
end

function CLuaGuideMgr.TryTriggerGuanNingCommanderSignUpGuide()
    if CGuideMgr.Inst:IsInPhase(EnumGuideKey.FirstEnterGuanNing) then -- 防止顶掉第一次进关宁的引导
        return
    end
    if not CLuaGuideMgr.EnableGuide() then return end
    if COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.GuanNingCommanderSignUp) then
        return
    end
    local triggered = CGuideMgr.Inst:StartGuide(EnumGuideKey.GuanNingCommanderSignUp, 0)
    if triggered then
        COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.GuanNingCommanderSignUp)
    end
end

function CLuaGuideMgr.TryTriggerGuanNingCommanderGuide(isCommander)
    if CGuideMgr.Inst:IsInPhase(EnumGuideKey.FirstEnterGuanNing) or CGuideMgr.Inst:IsInPhase(EnumGuideKey.GuanNingCommanderSignUp) then
        return
    end
    if not CLuaGuideMgr.EnableGuide() then return end
    if isCommander then
        if COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.GuanNingCommander) then
            return
        end
        local triggered = CGuideMgr.Inst:StartGuide(EnumGuideKey.GuanNingCommander, 0)
        if triggered then
            COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.GuanNingCommander)
        end
    else
        if COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.GuanNingNotCommander) then
            return
        end
        local triggered = CGuideMgr.Inst:StartGuide(EnumGuideKey.GuanNingNotCommander, 0)
        if triggered then
            COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.GuanNingNotCommander)
        end
    end
end

function CLuaGuideMgr.TryTriggerGuanNingThumbsUpGuide()
    if not CLuaGuideMgr.EnableGuide() then return end
    if COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.GuanNingThumbsUp) or COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.GuanNingRightThumbsUp) then
        return
    end

    if CLuaGuanNingMgr:GetMyForce() == 0 then
        local triggered = CGuideMgr.Inst:StartGuide(EnumGuideKey.GuanNingThumbsUp, 0)
        if triggered then
            COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.GuanNingThumbsUp)
        end
    else
        local triggered = CGuideMgr.Inst:StartGuide(EnumGuideKey.GuanNingRightThumbsUp, 0)
        if triggered then
            COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.GuanNingRightThumbsUp)
        end
    end
end

function CLuaGuideMgr.TryTriggerGuanNingCounterattackWeak()
    if CGuideMgr.Inst:IsInPhase(EnumGuideKey.FirstEnterGuanNing) then
        return
    end
    if not CLuaGuideMgr.EnableGuide() then return end
    if COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.GuanNingCounterattackWeak) then
        return
    end
    local triggered = CGuideMgr.Inst:StartGuide(EnumGuideKey.GuanNingCounterattackWeak, 0)
    if triggered then
        COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.GuanNingCounterattackWeak)
    end
end

function CLuaGuideMgr.TryTriggerGuanNingCounterattackStrong()
    if CGuideMgr.Inst:IsInPhase(EnumGuideKey.FirstEnterGuanNing) then
        return
    end
    if not CLuaGuideMgr.EnableGuide() then return end
    if COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.GuanNingCounterattackStrong) then
        return
    end
    local triggered = CGuideMgr.Inst:StartGuide(EnumGuideKey.GuanNingCounterattackStrong, 0)
    if triggered then
        COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.GuanNingCounterattackStrong)
    end
end


local remindBaiShiInfo = {}
function CLuaGuideMgr.RemindBaiShiOnLevelUp()
    if CClientMainPlayer.Inst then
		local id = CClientMainPlayer.Inst.Id
		local curTime = Time.realtimeSinceStartup
		--30分钟内不重复触发
		if remindBaiShiInfo[id] and curTime - remindBaiShiInfo[id] < 1800 then
            return
		end
		remindBaiShiInfo[id] = Time.realtimeSinceStartup
		CUIManager.ShowUI(CUIResources.ShiTuAskWnd)
	end
end

function CLuaGuideMgr.TryTriggerEquipTipGuide(taskId)
    if not CLuaGuideMgr.EnableGuide() then return end
    if taskId and taskId==22100012 then
        CGuideMgr.Inst:StartGuide(EnumGuideKey.EquipTipGuide, 0)
    end
end

function CLuaGuideMgr.SendJuQingFail()
    -- if CGuidelineMgr.Inst:NeedShowEnhanceButton() then
    --     CGuideMgr.Inst:TriggerFirstJuQingFail()
    -- else
        CUIManager.ShowUI(CLuaUIResources.FailGuideWnd)
    -- end
end


CLuaGuideMgr.s_HideFingerGuideTick = nil
function CLuaGuideMgr.ShowFingerGuide1()
    CGuideWnd.Instance:ShowBubble(Vector3(0,200),g_MessageMgr:FormatMessage("Guide_Text_Finger1"))
    CGuideWnd.Instance:ShowFx("Fx/Vfx/Prefab/Dynamic/ui_guild_finger1.prefab",400,0)
    if CLuaGuideMgr.s_HideFingerGuideTick then
        UnRegisterTick(CLuaGuideMgr.s_HideFingerGuideTick)
    end
    CLuaGuideMgr.s_HideFingerGuideTick = RegisterTickOnce(function()
        CGuideWnd.Instance:HideBubble()
        CGuideWnd.Instance:HideFx()
        CLuaGuideMgr.s_HideFingerGuideTick = nil
        CLuaGuideMgr.ShowFingerGuide2()
        g_ScriptEvent:RemoveListener("MainCameraAngleChanged", CLuaGuideMgr, "OnMainCameraAngleChanged")
    end,5000)

    --等1秒钟再监听消息
    RegisterTickOnce(function()
        g_ScriptEvent:RemoveListener("MainCameraAngleChanged", CLuaGuideMgr, "OnMainCameraAngleChanged")
        g_ScriptEvent:AddListener("MainCameraAngleChanged", CLuaGuideMgr, "OnMainCameraAngleChanged")
    end,1000)
end

function CLuaGuideMgr:OnMainCameraAngleChanged()
    --结束引导
    if CLuaGuideMgr.s_HideFingerGuideTick then
        UnRegisterTick(CLuaGuideMgr.s_HideFingerGuideTick)
        CLuaGuideMgr.s_HideFingerGuideTick = nil
    end

    CGuideWnd.Instance:HideBubble()
    CGuideWnd.Instance:HideFx()

    g_ScriptEvent:RemoveListener("MainCameraAngleChanged", CLuaGuideMgr, "OnMainCameraAngleChanged")
    CLuaGuideMgr.ShowFingerGuide2()
end

function CLuaGuideMgr.ShowFingerGuide2()
    CGuideWnd.Instance:ShowBubble(Vector3(0,200),g_MessageMgr:FormatMessage("Guide_Text_Finger2"))
    CGuideWnd.Instance:ShowFx("Fx/Vfx/Prefab/Dynamic/ui_guild_finger2.prefab",400,0)
    if CLuaGuideMgr.s_HideFingerGuideTick then
        UnRegisterTick(CLuaGuideMgr.s_HideFingerGuideTick)
    end
    --最多5秒钟结束
    RegisterTickOnce(function()
        CGuideWnd.Instance:HideBubble()
        CGuideWnd.Instance:HideFx()
        CLuaGuideMgr.s_HideFingerGuideTick = nil
        g_ScriptEvent:RemoveListener("OnPinch", CLuaGuideMgr, "OnPinch")
    end,5000)

    --等1秒钟再监听消息
    RegisterTickOnce(function()
        g_ScriptEvent:RemoveListener("OnPinch", CLuaGuideMgr, "OnPinch")
        g_ScriptEvent:AddListener("OnPinch", CLuaGuideMgr, "OnPinch")
    end,1000)
end
function CLuaGuideMgr:OnPinch()
    --结束引导
    if CLuaGuideMgr.s_HideFingerGuideTick then
        UnRegisterTick(CLuaGuideMgr.s_HideFingerGuideTick)
        CLuaGuideMgr.s_HideFingerGuideTick = nil
    end

    CGuideWnd.Instance:HideBubble()
    CGuideWnd.Instance:HideFx()

    g_ScriptEvent:RemoveListener("OnPinch", CLuaGuideMgr, "OnPinch")
end

function CLuaGuideMgr.TriggerBabySetTargetQiChangGuide()
    if not CLuaGuideMgr.EnableGuide() then return end
    local selectedBaby = BabyMgr.Inst:GetSelectedBaby()
    local isOwnBaby = selectedBaby.OwnerId == CClientMainPlayer.Inst.Id
    if LuaBabyMgr.isAllQiChangActive(selectedBaby) then return end
    
    local jieBanQiChangId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp.JieBanQiChangId or 0
    local jieBanQiChangQuality = Baby_QiChang.GetData(jieBanQiChangId) and Baby_QiChang.GetData(jieBanQiChangId).Quality or 0
    local selectedBaby = BabyMgr.Inst:GetSelectedBaby()

    local EnumBabyStatus = {
        eBorn = 1,		-- 婴儿阶段
        eChild = 2,		-- 幼儿阶段
        eYoung = 3,		-- 少年阶段
    }

    -- 从一个符合引导条件的宝切换到一个不符合条件的宝的时候 需要退出一下引导
    if jieBanQiChangQuality >= 6 or selectedBaby.Grade < 11 or selectedBaby.Status < EnumBabyStatus.eYoung or not isOwnBaby and (CGuideMgr.Inst:IsInPhase(EnumGuideKey.BabySetTargetQiChang)) then
        --CGuideMgr.Inst:EndCurrentPhase()
        local bubble = CGuideWnd.Instance:GetGuideBubble()
        if (bubble ~= nil) then
            bubble:Hide()
        end
        return
    end

    if jieBanQiChangQuality < 6 and selectedBaby.Grade >= 11 and selectedBaby.Status == EnumBabyStatus.eYoung and isOwnBaby and (CGuideMgr.Inst:IsInPhase(EnumGuideKey.BabySetTargetQiChang)) then
        local bubble = CGuideWnd.Instance:GetGuideBubble()
        if (bubble ~= nil) then
            bubble:Show()
        end
        return
    end

    if COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.BabySetTargetQiChang) then return end
    if jieBanQiChangQuality < 6 and selectedBaby.Grade >= 11 and selectedBaby.Status == EnumBabyStatus.eYoung and isOwnBaby then
        local triggered = CGuideMgr.Inst:StartGuide(EnumGuideKey.BabySetTargetQiChang, 0)
        if triggered then
            COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.BabySetTargetQiChang)
        end
    end
end