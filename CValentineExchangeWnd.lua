-- Auto Generated!!
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CValentineExchangeWnd = import "L10.UI.CValentineExchangeWnd"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local Gac2Gas = import "L10.Game.Gac2Gas"
local Int32 = import "System.Int32"
local Item_Item = import "L10.Game.Item_Item"
local LocalString = import "LocalString"
local Object = import "System.Object"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local Valentine_Flowers = import "L10.Game.Valentine_Flowers"
local Valentine_Setting = import "L10.Game.Valentine_Setting"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CValentineExchangeWnd.m_OnEnable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.m_ExchangeBtn.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_ExchangeBtn.gameObject).onClick, MakeDelegateFromCSFunction(this.OnExchangeBtnClick, VoidDelegate, this), true)
    UIEventListener.Get(this.m_HelpBtn.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_HelpBtn.gameObject).onClick, MakeDelegateFromCSFunction(this.OnHelpBtnClick, VoidDelegate, this), true)
    UIEventListener.Get(this.m_CloseBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_CloseBtn).onClick, MakeDelegateFromCSFunction(this.OnCloseBtnClick, VoidDelegate, this), true)
    EventManager.AddListenerInternal(EnumEventType.SetValentineFlowerData, MakeDelegateFromCSFunction(this.SetValentineFlowerData, MakeGenericClass(Action1, MakeGenericClass(Dictionary, String, Object)), this))
    EventManager.AddListenerInternal(EnumEventType.SetValentineGetHelpCount, MakeDelegateFromCSFunction(this.SetValentineGetHelpCount, MakeGenericClass(Action1, UInt32), this))
end
CValentineExchangeWnd.m_OnDisable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.m_ExchangeBtn.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_ExchangeBtn.gameObject).onClick, MakeDelegateFromCSFunction(this.OnExchangeBtnClick, VoidDelegate, this), false)
    UIEventListener.Get(this.m_HelpBtn.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_HelpBtn.gameObject).onClick, MakeDelegateFromCSFunction(this.OnHelpBtnClick, VoidDelegate, this), false)
    UIEventListener.Get(this.m_CloseBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_CloseBtn).onClick, MakeDelegateFromCSFunction(this.OnCloseBtnClick, VoidDelegate, this), false)
    EventManager.RemoveListenerInternal(EnumEventType.SetValentineFlowerData, MakeDelegateFromCSFunction(this.SetValentineFlowerData, MakeGenericClass(Action1, MakeGenericClass(Dictionary, String, Object)), this))
    EventManager.RemoveListenerInternal(EnumEventType.SetValentineGetHelpCount, MakeDelegateFromCSFunction(this.SetValentineGetHelpCount, MakeGenericClass(Action1, UInt32), this))
end
CValentineExchangeWnd.m_Init_CS2LuaHook = function (this) 
    Gac2Gas.QueryValentineFlowerData()
    Gac2Gas.QueryValentineGetHelpCount()
    local rewardItemId = Valentine_Setting.GetData().RewardItemID
    local rewardData = Item_Item.GetData(rewardItemId)
    if rewardData == nil then
        return
    end
    --m_RewardTex.LoadMaterial(rewardData.Icon);
    this.m_RewardName = rewardData.Name
    UIEventListener.Get(this.m_RewardTex.gameObject).onClick = DelegateFactory.VoidDelegate(function (obj) 
        CItemInfoMgr.ShowLinkItemTemplateInfo(rewardItemId, false, nil, AlignType.Default, 0, 0, 0, 0)
    end)
    local index = 0
    Valentine_Flowers.ForeachKey(DelegateFactory.Action_object(function (key) 
        CommonDefs.ListAdd(this.m_FlowerItem, typeof(UInt32), key)
        local data = Item_Item.GetData(key)
        if data ~= nil then
            this.m_FlowerTex[index]:LoadMaterial(data.Icon)
            UIEventListener.Get(this.m_FlowerTex[index].gameObject).onClick = DelegateFactory.VoidDelegate(function (obj) 
                CItemInfoMgr.ShowLinkItemTemplateInfo(key, false, nil, AlignType.Default, 0, 0, 0, 0)
            end)
            this.m_FlowerLabel[index].text = data.Name
        end
        index = index + 1
    end))
end
CValentineExchangeWnd.m_SetValentineFlowerData_CS2LuaHook = function (this, dict) 
    local maxReward = Int32.MaxValue
    do
        local i = 0 local cnt = this.m_FlowerItem.Count local num
        while i < cnt do
            if dict ~= nil and CommonDefs.DictContains(dict, typeof(String), tostring(this.m_FlowerItem[i])) then
                num = math.floor(tonumber(CommonDefs.DictGetValue(dict, typeof(String), tostring(this.m_FlowerItem[i])) or 0))
            else
                num = 0
            end
            maxReward = math.min(num, maxReward)
            if num <= 0 then
                this.m_CountLabel[i].text = "[FF0000]0[-]/1"
            else
                this.m_CountLabel[i].text = num .. "/1"
            end
            if maxReward <= 0 then
                this.m_RewardLabel.text = this.m_RewardName .. "[FF0000]x0[-]"
                this.m_ExchangeBtn.Enabled = false
                --m_RewardFxTex.SetActive(false);
                Extensions.SetLocalPositionZ(this.m_RewardTex.transform, - 1)
            else
                this.m_ExchangeBtn.Enabled = true
                this.m_RewardLabel.text = (this.m_RewardName .. "x") .. maxReward
                --m_RewardFxTex.SetActive(true);
                Extensions.SetLocalPositionZ(this.m_RewardTex.transform, 0)
            end
            i = i + 1
        end
    end
end
CValentineExchangeWnd.m_SetValentineGetHelpCount_CS2LuaHook = function (this, cnt) 
    this.m_HelpLabel.text = ((LocalString.GetString("求助 ") .. cnt) .. "/") .. CValentineExchangeWnd.s_TotalHelpCount
    if cnt >= CValentineExchangeWnd.s_TotalHelpCount then
        this.m_HelpBtn.Enabled = false
    else
        this.m_HelpBtn.Enabled = true
    end
end
