local CUITexture = import "L10.UI.CUITexture"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local Extensions = import "Extensions"
local CButton = import "L10.UI.CButton"
local EnumGender = import "L10.Game.EnumGender"
local UISprite = import "UISprite"
local Color = import "UnityEngine.Color"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"

LuaAppearanceClosetLuminousSubview = class()

RegistChildComponent(LuaAppearanceClosetLuminousSubview,"m_ClosetLuminousItem", "CommonClosetItemCell", GameObject)
RegistChildComponent(LuaAppearanceClosetLuminousSubview,"m_SwitchButton", "CommonHeaderSwitch", CButton)
RegistChildComponent(LuaAppearanceClosetLuminousSubview,"m_ItemDisplay", "AppearanceCommonButtonDisplayView", CCommonLuaScript)
RegistChildComponent(LuaAppearanceClosetLuminousSubview,"m_ContentTable", "ContentTable", UITable)
RegistChildComponent(LuaAppearanceClosetLuminousSubview,"m_ContentScrollView", "ContentScrollView", UIScrollView)

RegistClassMember(LuaAppearanceClosetLuminousSubview, "m_AllIntensifySuitAppearances") --所有强化套装外观
RegistClassMember(LuaAppearanceClosetLuminousSubview, "m_AllGemGroupAppearances") --所有石之灵外观 同时最多只有一个， 石之灵只有开关，不需要应用和去除
RegistClassMember(LuaAppearanceClosetLuminousSubview, "m_AllSkillAppearances") --所有技能外观，极少获得， 在全部中不做处理，仅在衣橱使用
RegistClassMember(LuaAppearanceClosetLuminousSubview, "m_SelectedDataId")
RegistClassMember(LuaAppearanceClosetLuminousSubview, "m_PreviewType")

function LuaAppearanceClosetLuminousSubview:Awake()
end

--type取值1 强化套装 2 石之灵  3 技能
function LuaAppearanceClosetLuminousSubview:Init(type)
    --组件共用，每次需要重新关联响应方法
    self.m_SwitchButton.gameObject:SetActive(true)
    UIEventListener.Get(self.m_SwitchButton.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) self:OnSwitchButtonClick() end)
    
    self.m_PreviewType = type
    self:SetDefaultSelection()
    self:LoadData()
end

function LuaAppearanceClosetLuminousSubview:LoadData()
    if self.m_PreviewType == EnumAppearancePreviewLuminousType.eIntensifySuit then
        self.m_AllIntensifySuitAppearances = LuaAppearancePreviewMgr:GetAllIntensifySuitInfo(true)
    elseif self.m_PreviewType == EnumAppearancePreviewLuminousType.eGemGroup then
        self.m_AllGemGroupAppearances = LuaAppearancePreviewMgr:GetAllGemGroupInfo()
    elseif self.m_PreviewType == EnumAppearancePreviewLuminousType.eSkillAppearance then
        self.m_AllSkillAppearances = LuaAppearancePreviewMgr:GetAllSkillAppearInfo()
    end

    Extensions.RemoveAllChildren(self.m_ContentTable.transform)
    self.m_ContentTable.gameObject:SetActive(true)

    if self.m_PreviewType == EnumAppearancePreviewLuminousType.eIntensifySuit then
        for i = 1, #self.m_AllIntensifySuitAppearances do
            local child = CUICommonDef.AddChild(self.m_ContentTable.gameObject, self.m_ClosetLuminousItem)
            child:SetActive(true)
            self:InitItem(child, self.m_AllIntensifySuitAppearances[i])
        end
    elseif self.m_PreviewType == EnumAppearancePreviewLuminousType.eGemGroup then
        for i = 1, #self.m_AllGemGroupAppearances do
            local child = CUICommonDef.AddChild(self.m_ContentTable.gameObject, self.m_ClosetLuminousItem)
            child:SetActive(true)
            self:InitItem(child, self.m_AllGemGroupAppearances[i])
        end
    elseif self.m_PreviewType == EnumAppearancePreviewLuminousType.eSkillAppearance then
        for i = 1, #self.m_AllSkillAppearances do
            local child = CUICommonDef.AddChild(self.m_ContentTable.gameObject, self.m_ClosetLuminousItem)
            child:SetActive(true)
            self:InitItem(child, self.m_AllSkillAppearances[i])
        end
    end
    self.m_ContentTable:Reposition()
    self.m_ContentScrollView:ResetPosition()
    self:InitSelection()
end

function LuaAppearanceClosetLuminousSubview:SetDefaultSelection()
    if self.m_PreviewType == EnumAppearancePreviewLuminousType.eIntensifySuit then
        self.m_SelectedDataId = LuaAppearancePreviewMgr:IsShowIntensitySuit() and LuaAppearancePreviewMgr:GetCurrentInUseIntensitySuit() or 0
    elseif self.m_PreviewType == EnumAppearancePreviewLuminousType.eGemGroup then
        self.m_SelectedDataId = LuaAppearancePreviewMgr:IsShowGemGroup() and LuaAppearancePreviewMgr:GetCurrentInUseGemGroup() or 0
    elseif self.m_PreviewType == EnumAppearancePreviewLuminousType.eSkillAppearance then
        self.m_SelectedDataId = LuaAppearancePreviewMgr:IsShowSkillAppear() and LuaAppearancePreviewMgr:GetCurrentSkillAppearId() or 0
    end
end

function LuaAppearanceClosetLuminousSubview:InitSelection()
    if self.m_SelectedDataId == 0 then
        self:OnItemClick(nil)
        return
    end

    local childCount = self.m_ContentTable.transform.childCount
    for i=0, childCount-1 do
        local childGo = self.m_ContentTable.transform:GetChild(i).gameObject
        if self.m_PreviewType == EnumAppearancePreviewLuminousType.eIntensifySuit then
            local appearanceData = self.m_AllIntensifySuitAppearances[i+1]
            if appearanceData.fxId == self.m_SelectedDataId then
                self:OnItemClick(childGo)
                break
            end
        elseif self.m_PreviewType == EnumAppearancePreviewLuminousType.eGemGroup then
            local appearanceData = self.m_AllGemGroupAppearances[i+1]
            if appearanceData.id == self.m_SelectedDataId then
                self:OnItemClick(childGo)
                break
            end
        elseif self.m_PreviewType == EnumAppearancePreviewLuminousType.eSkillAppearance then
            local appearanceData = self.m_AllSkillAppearances[i+1]
            if appearanceData.id == self.m_SelectedDataId then
                self:OnItemClick(childGo)
                break
            end
        end
    end
end

function LuaAppearanceClosetLuminousSubview:InitItem(itemGo, appearanceData)
    local iconTexture = itemGo.transform:Find("Item"):GetComponent(typeof(CUITexture))
    local cornerGo = itemGo.transform:Find("Item/Corner").gameObject
    local nameLabel = itemGo.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local conditionLabel = itemGo.transform:Find("ConditionLabel"):GetComponent(typeof(UILabel))
    iconTexture:LoadMaterial(appearanceData.icon)
    nameLabel.text = appearanceData.name
    conditionLabel.text = appearanceData.condition

    if self.m_PreviewType == EnumAppearancePreviewLuminousType.eIntensifySuit then
        itemGo:GetComponent(typeof(CButton)).Selected = (self.m_SelectedDataId and self.m_SelectedDataId==appearanceData.fxId or false)
        cornerGo:SetActive(LuaAppearancePreviewMgr:IsCurrentInUseIntensitySuit(appearanceData.fxId))

    elseif self.m_PreviewType == EnumAppearancePreviewLuminousType.eGemGroup then
        itemGo:GetComponent(typeof(CButton)).Selected = (self.m_SelectedDataId and self.m_SelectedDataId==appearanceData.id or false)
        cornerGo:SetActive(LuaAppearancePreviewMgr:MainPlayerHasGemGroup())
        
    elseif self.m_PreviewType == EnumAppearancePreviewLuminousType.eSkillAppearance then
        itemGo:GetComponent(typeof(CButton)).Selected = (self.m_SelectedDataId and self.m_SelectedDataId==appearanceData.id or false)
        cornerGo:SetActive(LuaAppearancePreviewMgr:IsCurrentInUseSkillAppear(appearanceData.id))

    end
    
    UIEventListener.Get(itemGo).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnItemClick(go)
    end)
end

function LuaAppearanceClosetLuminousSubview:OnItemClick(go)
    local childCount = self.m_ContentTable.transform.childCount
    for i=0, childCount-1 do
        local childGo = self.m_ContentTable.transform:GetChild(i).gameObject
        if childGo == go then
            childGo.transform:GetComponent(typeof(CButton)).Selected = true
            if self.m_PreviewType == EnumAppearancePreviewLuminousType.eIntensifySuit then
                self.m_SelectedDataId = self.m_AllIntensifySuitAppearances[i+1].fxId
                if not LuaAppearancePreviewMgr:IsShowIntensitySuit() then
                    g_MessageMgr:ShowMessage("Appearance_Preview_Hide_Itensify_Suit_Tip")
                end
            elseif self.m_PreviewType == EnumAppearancePreviewLuminousType.eGemGroup then
                self.m_SelectedDataId = self.m_AllGemGroupAppearances[i+1].id
                if not LuaAppearancePreviewMgr:IsShowGemGroup() then
                    g_MessageMgr:ShowMessage("Appearance_Preview_Hide_Gem_Group_Tip")
                end 
            elseif self.m_PreviewType == EnumAppearancePreviewLuminousType.eSkillAppearance then
                self.m_SelectedDataId = self.m_AllSkillAppearances[i+1].id
                if not LuaAppearancePreviewMgr:IsShowSkillAppear() then
                    g_MessageMgr:ShowMessage("Appearance_Preview_Hide_Skill_Appearance_Tip")
                end 
            end
        else
            childGo.transform:GetComponent(typeof(CButton)).Selected = false
        end
    end
    self:UpdateButtonsDisplay()
    if self.m_PreviewType == EnumAppearancePreviewLuminousType.eIntensifySuit then
        g_ScriptEvent:BroadcastInLua("RequestPreviewMainPlayerIntensitySuit", self.m_SelectedDataId)
    elseif self.m_PreviewType == EnumAppearancePreviewLuminousType.eGemGroup then
        g_ScriptEvent:BroadcastInLua("RequestPreviewMainPlayerGemGroup", self.m_SelectedDataId)
    elseif self.m_PreviewType == EnumAppearancePreviewLuminousType.eSkillAppearance then
        g_ScriptEvent:BroadcastInLua("RequestPreviewMainPlayerSkillAppearance", self.m_SelectedDataId)
    end
end

function LuaAppearanceClosetLuminousSubview:UpdateButtonsDisplay()
    self.m_ItemDisplay.gameObject:SetActive(true)
    if CClientMainPlayer.Inst then
        local buttonTbl = {}
        if self.m_PreviewType == EnumAppearancePreviewLuminousType.eIntensifySuit then
            local appearanceData = self:GetCurrentSelectedSuitData()
            if appearanceData then
                local exist = LuaAppearancePreviewMgr:MainPlayerHasIntensitySuit(appearanceData.fxId)
                local inUse = LuaAppearancePreviewMgr:IsCurrentInUseIntensitySuit(appearanceData.fxId)
                if exist and not inUse then
                    table.insert(buttonTbl, {text=LocalString.GetString("更换"), isYellow=false, action=function(go) self:OnApplyButtonClick() end})
                elseif exist and inUse then
                    table.insert(buttonTbl, {text=LocalString.GetString("脱下"), isYellow=false, action=function(go) self:OnRemoveButtonClick() end})
                end
            else
                self.m_ItemDisplay.gameObject:SetActive(false)
            end
            self.m_SwitchButton.Text = LocalString.GetString("强化套装")
            self.m_SwitchButton.Selected = LuaAppearancePreviewMgr:IsShowIntensitySuit()
        elseif self.m_PreviewType == EnumAppearancePreviewLuminousType.eGemGroup then
            --石之灵只有开关，无需Apply和Remove
            self.m_SwitchButton.Text = LocalString.GetString("石之灵")
            self.m_SwitchButton.Selected = LuaAppearancePreviewMgr:IsShowGemGroup()
        elseif self.m_PreviewType == EnumAppearancePreviewLuminousType.eSkillAppearance then
            local appearanceData = self:GetCurrentSelectedSkillAppearData()
            if appearanceData then
                local exist = LuaAppearancePreviewMgr:MainPlayerHasSkillAppear(appearanceData.id)
                local inUse = LuaAppearancePreviewMgr:IsCurrentInUseSkillAppear(appearanceData.id)
                if exist and not inUse then
                    table.insert(buttonTbl, {text=LocalString.GetString("更换"), isYellow=false, action=function(go) self:OnApplyButtonClick() end})
                elseif exist and inUse then
                    table.insert(buttonTbl, {text=LocalString.GetString("脱下"), isYellow=false, action=function(go) self:OnRemoveButtonClick() end})
                end
            else
                self.m_ItemDisplay.gameObject:SetActive(false)
            end
            self.m_SwitchButton.Text = LocalString.GetString("技能外观")
            self.m_SwitchButton.Selected = LuaAppearancePreviewMgr:IsShowSkillAppear()
        end
        self.m_ItemDisplay:Init(buttonTbl)
    else
        self.m_SwitchButton.Text = ""
        self.m_ItemDisplay.gameObject:SetActive(false)
    end
end

function LuaAppearanceClosetLuminousSubview:GetCurrentSelectedSuitData()
    local id = self.m_SelectedDataId and self.m_SelectedDataId or 0
    if id>0 then
        local appearanceData = nil
        for i=1,#self.m_AllIntensifySuitAppearances do
            if self.m_AllIntensifySuitAppearances[i].fxId == self.m_SelectedDataId then
                return self.m_AllIntensifySuitAppearances[i]
            end
        end
    end
    return nil
end

function LuaAppearanceClosetLuminousSubview:GetCurrentSelectedSkillAppearData()
    local id = self.m_SelectedDataId and self.m_SelectedDataId or 0
    if id>0 then
        local appearanceData = nil
        for i=1,#self.m_AllSkillAppearances do
            if self.m_AllSkillAppearances[i].id == self.m_SelectedDataId then
                return self.m_AllSkillAppearances[i]
            end
        end
    end
    return nil
end

function LuaAppearanceClosetLuminousSubview:GetCurrentSelectedGemGroupData()
    local id = self.m_SelectedDataId and self.m_SelectedDataId or 0
    if id>0 then
        local appearanceData = nil
        for i=1,#self.m_AllGemGroupAppearances do
            if self.m_AllGemGroupAppearances[i].id == self.m_SelectedDataId then
                return self.m_AllGemGroupAppearances[i]
            end
        end
    end
    return nil
end

function LuaAppearanceClosetLuminousSubview:OnApplyButtonClick()
    if self.m_PreviewType == EnumAppearancePreviewLuminousType.eIntensifySuit then
        LuaAppearancePreviewMgr:RequestSetIntensitySuit(self.m_SelectedDataId)
    elseif self.m_PreviewType == EnumAppearancePreviewLuminousType.eGemGroup then
        --不应该进入该分支
    elseif self.m_PreviewType == EnumAppearancePreviewLuminousType.eSkillAppearance then
        LuaAppearancePreviewMgr:RequestSetSkillAppear(self.m_SelectedDataId)
    end
end

function LuaAppearanceClosetLuminousSubview:OnRemoveButtonClick()
    if self.m_PreviewType == EnumAppearancePreviewLuminousType.eIntensifySuit then
        LuaAppearancePreviewMgr:RequestSetIntensitySuit(0)
    elseif self.m_PreviewType == EnumAppearancePreviewLuminousType.eSkillAppearance then
        LuaAppearancePreviewMgr:RequestSetSkillAppear(0)
    end
end

function LuaAppearanceClosetLuminousSubview:OnSwitchButtonClick()
    self.m_SwitchButton.Selected = not self.m_SwitchButton.Selected
    if self.m_PreviewType == EnumAppearancePreviewLuminousType.eIntensifySuit then
        local bShow = LuaAppearancePreviewMgr:IsShowIntensitySuit()
        LuaAppearancePreviewMgr:ChangeIntensitySuitVisibility(not bShow)
    elseif self.m_PreviewType == EnumAppearancePreviewLuminousType.eGemGroup then
        local bShow = LuaAppearancePreviewMgr:IsShowGemGroup()
        LuaAppearancePreviewMgr:ChangeGemGroupVisibility(not bShow)
    elseif self.m_PreviewType == EnumAppearancePreviewLuminousType.eSkillAppearance then
        local bShow = LuaAppearancePreviewMgr:IsShowSkillAppear()
        LuaAppearancePreviewMgr:ChangeSkillAppearVisibility(not bShow)
    end
end

function  LuaAppearanceClosetLuminousSubview:OnEnable()
    g_ScriptEvent:AddListener("EquipmentSuitUpdate",self,"FxUpdate")
    g_ScriptEvent:AddListener("EquipmentGemGroupUpdate",self,"GemgroupFxUpdate")
	g_ScriptEvent:AddListener("OnSyncMainPlayerAppearanceProp",self,"SyncMainPlayerAppearancePropUpdate")
    g_ScriptEvent:AddListener("AppearancePropertySettingInfoReturn",self,"OnAppearancePropertySettingInfoReturn") --开关切换
    g_ScriptEvent:AddListener("SetSkillAppearanceSuccess",self,"OnSetSkillAppearanceSuccess")
end

function  LuaAppearanceClosetLuminousSubview:OnDisable()
    g_ScriptEvent:RemoveListener("EquipmentSuitUpdate",self,"FxUpdate")
    g_ScriptEvent:RemoveListener("EquipmentGemGroupUpdate",self,"GemgroupFxUpdate")
    g_ScriptEvent:RemoveListener("OnSyncMainPlayerAppearanceProp",self,"SyncMainPlayerAppearancePropUpdate")
    g_ScriptEvent:RemoveListener("AppearancePropertySettingInfoReturn",self,"OnAppearancePropertySettingInfoReturn")
    g_ScriptEvent:RemoveListener("SetSkillAppearanceSuccess",self,"OnSetSkillAppearanceSuccess")
    if self.m_PreviewType == EnumAppearanceFashionUnlockType.eSkillAppearance then
        CLuaFightingSpiritMgr:ClearOpenWndParam()
    end
end


function LuaAppearanceClosetLuminousSubview:FxUpdate(args)
    local obj = args[0]
    if obj and TypeIs(obj, typeof(CClientMainPlayer)) then
        self:SetDefaultSelection()
		self:LoadData()
	end
end

function LuaAppearanceClosetLuminousSubview:GemgroupFxUpdate(args)
    local obj = args[0]
    if obj and TypeIs(obj, typeof(CClientMainPlayer)) then
        self:SetDefaultSelection()
        self:LoadData()
    end
end

function LuaAppearanceClosetLuminousSubview:SyncMainPlayerAppearancePropUpdate()
    self:InitSelection()
end

function LuaAppearanceClosetLuminousSubview:OnAppearancePropertySettingInfoReturn(args)
    self:SetDefaultSelection()
    self:InitSelection()
end

function LuaAppearanceClosetLuminousSubview:OnSetSkillAppearanceSuccess(appearId, expiredTime)
    self:SetDefaultSelection()
    self:LoadData()
end