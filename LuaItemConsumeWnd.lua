local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CItemMgr = import "L10.Game.CItemMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local NGUITextAlignment = import "NGUIText+Alignment"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CTooltipAlignType = import "L10.UI.CTooltip+AlignType"
local CUITexture = import "L10.UI.CUITexture"
local CButton = import "L10.UI.CButton"
local UIWidget = import "UIWidget"

LuaItemConsumeWnd = class()
RegistClassMember(LuaItemConsumeWnd, "m_DescLabel") --描述文本
RegistClassMember(LuaItemConsumeWnd, "m_NumLabel")  --数量文本
RegistClassMember(LuaItemConsumeWnd, "m_IconTexture") --道具图标
RegistClassMember(LuaItemConsumeWnd, "m_DisableIcon") --数量不满足时的遮罩
RegistClassMember(LuaItemConsumeWnd, "m_CancelButton") --取消按钮
RegistClassMember(LuaItemConsumeWnd, "m_OKButton") --确认按钮
RegistClassMember(LuaItemConsumeWnd, "m_WidgetBg") --背景版
RegistClassMember(LuaItemConsumeWnd, "m_WidgetAnchor") --布局容器

RegistClassMember(LuaItemConsumeWnd, "m_CurInfo")


function LuaItemConsumeWnd:Awake()
    self.m_DescLabel = self.transform:Find("Content/DescLabel"):GetComponent(typeof(UILabel))
    self.m_NumLabel = self.transform:Find("Content/Container/CountLabel"):GetComponent(typeof(UILabel))
    self.m_IconTexture = self.transform:Find("Content/Container/Icon"):GetComponent(typeof(CUITexture))
    self.m_DisableIcon = self.transform:Find("Content/Container/DisableIcon").gameObject
    self.m_CancelButton = self.transform:Find("Content/Container/CancelButton"):GetComponent(typeof(CButton))
    self.m_OKButton = self.transform:Find("Content/Container/OKButton"):GetComponent(typeof(CButton))
    self.m_WidgetBg = self.transform:Find("Wnd_Bg_Box"):GetComponent(typeof(UIWidget))
    self.m_WidgetAnchor = self.transform:Find("Content/Container"):GetComponent(typeof(UIWidget))

    self.m_CurInfo = nil

    CommonDefs.AddOnClickListener(self.m_IconTexture.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnItemClick(go) end), false)
    CommonDefs.AddOnClickListener(self.m_OKButton.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnOKButtonClick(go) end), false)
    CommonDefs.AddOnClickListener(self.m_CancelButton.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnCancelButtonClick(go) end), false)
end

function LuaItemConsumeWnd:Init()
    self.m_CurInfo = LuaItemInfoMgr:GetCurrentConsumeItemInfo()

    self.m_DescLabel.text = self.m_CurInfo.m_Desc
    self.m_OKButton.Text = self.m_CurInfo.m_OKText
    self.m_CancelButton.Text = self.m_CurInfo.m_CancelText
    local item = CItemMgr.Inst:GetItemTemplate(self.m_CurInfo.m_TemplateId)
    if item then
        self.m_IconTexture:LoadMaterial(item.Icon)
    else
        self.m_IconTexture:Clear()
    end

    self:UpdateCount()
end

function LuaItemConsumeWnd:OnEnable()
    g_ScriptEvent:AddListener("SendItem", self, "OnSendItem")
    g_ScriptEvent:AddListener("SetItemAt", self, "OnSetItemAt")
end

function LuaItemConsumeWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendItem", self, "OnSendItem")
    g_ScriptEvent:RemoveListener("SetItemAt", self, "OnSetItemAt")
end

function LuaItemConsumeWnd:OnSendItem(args)
    local item = args and args[0] and CItemMgr.Inst:GetById(args[0]) or nil
    if item and item.TemplateId == self.m_CurInfo.m_TemplateId then
        self:UpdateCount()
    end
end

function LuaItemConsumeWnd:OnSetItemAt(args)
    self:UpdateCount()
end

function LuaItemConsumeWnd:UpdateCount()
    local count = self:GetItemCount()
    if count < self.m_CurInfo.m_ConsumeCount then
        self.m_NumLabel.text = SafeStringFormat3("[c][ff0000]%d[-][/c]/%d", count, self.m_CurInfo.m_ConsumeCount)
        self.m_DisableIcon:SetActive(true)
    else
        self.m_NumLabel.text = SafeStringFormat3("%d/%d", count, self.m_CurInfo.m_ConsumeCount)
        self.m_DisableIcon:SetActive(false)
    end

    if self.m_CurInfo.m_ShowCountInDesc then
        self.m_DescLabel.text = cs_string.Format(self.m_CurInfo.m_Desc, self.m_NumLabel.text)
    end

    self:Layout()
end

function LuaItemConsumeWnd:Layout()
    if self.m_DescLabel.height <= self.m_DescLabel.fontSize + math.ceil(self.m_DescLabel.floatSpacingY) then
        self.m_DescLabel.alignment = NGUITextAlignment.Center
    else
        self.m_DescLabel.alignment = NGUITextAlignment.Left
    end

    self.m_WidgetAnchor:ResetAndUpdateAnchors()
    self.m_WidgetBg:ResetAndUpdateAnchors()
end

function LuaItemConsumeWnd:OnOKButtonClick(go)
    if self.m_CurInfo.m_OKFunc then
        local count = self:GetItemCount()
        local enough = count >= self.m_CurInfo.m_ConsumeCount

        if not enough and self.m_CurInfo.m_ShowAccessWhenClickIcon and self.m_CurInfo.m_ShowAccessWhenClickOkIfNotEnough then
            CItemAccessListMgr.Inst:ShowItemAccessInfo(self.m_CurInfo.m_TemplateId, enough, self.m_IconTexture.transform, CTooltipAlignType.Right)
        else
            self.m_CurInfo.m_OKFunc(enough)
            if self.m_CurInfo.m_CloseWhenClickOKButton then
                LuaItemInfoMgr:CloseItemConsumeWnd()
            end
        end
    end
end

function LuaItemConsumeWnd:OnCancelButtonClick(go)
    if self.m_CurInfo.m_CancelFunc then
        local count = self:GetItemCount()
        self.m_CurInfo.m_CancelFunc(count>=self.m_CurInfo.m_ConsumeCount) 
    end
    LuaItemInfoMgr:CloseItemConsumeWnd()
end

function LuaItemConsumeWnd:OnItemClick(go)
    if self.m_CurInfo.m_ShowAccessWhenClickIcon then
        local count = self:GetItemCount()
        CItemAccessListMgr.Inst:ShowItemAccessInfo(self.m_CurInfo.m_TemplateId, count >= self.m_CurInfo.m_ConsumeCount, go.transform,  CTooltipAlignType.Right)
    else
        CItemInfoMgr.ShowLinkItemTemplateInfo(self.m_CurInfo.m_TemplateId)
    end
end

function LuaItemConsumeWnd:GetItemCount()
    local count = 0 
    if CClientMainPlayer.Inst then
        local bagSize = CClientMainPlayer.Inst.ItemProp:GetPlaceSize(EnumItemPlace.Bag)
        for i = 1, bagSize, 1 do
            local id = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Bag, i)
            local item = CItemMgr.Inst:GetById(id)
            if item and item.TemplateId == self.m_CurInfo.m_TemplateId then
                if item.IsItem then
                    if item.Item and not item.Item:IsExpire() then
                        count = count + item.Amount
                    end
                else
                    count = count + item.Amount
                end
            end
        end
    end
    return count
end