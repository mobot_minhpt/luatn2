-- Auto Generated!!
local CHideAndSeekRankItem = import "L10.UI.CHideAndSeekRankItem"
local LocalString = import "LocalString"
local Profession = import "L10.Game.Profession"
CHideAndSeekRankItem.m_Init_CS2LuaHook = function (this, rank) 
    if rank.m_Rank <= 0 then
        this.m_RankLabel.text = LocalString.GetString("未上榜")
    else
        this.m_RankLabel.text = tostring(rank.m_Rank)
    end

    this.m_ClassSprite.spriteName = Profession.GetIcon(rank.m_Clazz)
    this.m_NameLabel.text = rank.m_Name
    this.m_ScoreLabel.text = tostring(rank.m_Score)
    this.gameObject:SetActive(true)
end
