require("common/common_include")


local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local UILabel = import "UILabel"
local CButton = import "L10.UI.CButton"
local UIInput = import "UIInput"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local MessageWndManager = import "L10.UI.MessageWndManager"
local System = import "System"
local CUICommonDef = import "L10.UI.CUICommonDef"
local Color = import "UnityEngine.Color"
--local LocalString = import "LocalString"

CLuaBindDetailWnd=class()
RegistChildComponent(CLuaBindDetailWnd, "m_DoBtn", CButton)
RegistChildComponent(CLuaBindDetailWnd, "QnInputField", UIInput)
RegistChildComponent(CLuaBindDetailWnd, "bindLabel", UILabel)
RegistChildComponent(CLuaBindDetailWnd, "tipsLabel", UILabel)

RegistClassMember(CLuaBindDetailWnd,"canbind")
RegistClassMember(CLuaBindDetailWnd,"hasbind")
RegistClassMember(CLuaBindDetailWnd,"lastInput")
function CLuaBindDetailWnd:Awake()


	self.lastInput=self.QnInputField.value
	self.bindLabel.gameObject:SetActive(false)
	CommonDefs.AddOnClickListener(self.QnInputField.gameObject, DelegateFactory.Action_GameObject(function (go)
		if self.QnInputField.value==LocalString.GetString("请输入邀请码") then 
			self.QnInputField.value=""
		end
	end), false)


	CommonDefs.AddOnClickListener(self.m_DoBtn.gameObject, DelegateFactory.Action_GameObject(function (go)

		local content = self.QnInputField.value
		if self.canbind==true and self.hasbind==false then
			if not content  then
	        MessageWndManager.ShowOKMessage(LocalString.GetString("请先输入邀请码"), nil)
			elseif	string.len(content)<9 then
				MessageWndManager.ShowOKMessage(LocalString.GetString("该邀请码无效，请重新输入"), nil)
			elseif self.bindLabel.text=="" then
				MessageWndManager.ShowOKMessage(LocalString.GetString("该邀请码无效，请重新输入"), nil)
			elseif self.bindLabel.text==CClientMainPlayer.Inst.Name then
				MessageWndManager.ShowOKMessage(LocalString.GetString("不能绑定自己的邀请码哦"), nil)
			elseif self.bindLabel.text~="" then
				content = tonumber(content)
				MessageWndManager.ShowOKCancelMessage(System.String.Format(LocalString.GetString("[c]确定要绑定[-][FFED5F]{0}[-]的邀请码吗？[/c]"), self.bindLabel.text), DelegateFactory.Action(function () 
                Gac2Gas.MergeBattleHuiLiuPlayerRequestBind(content)
            	end), nil, nil, nil, false)
				
			end
		end
	end), false)
	self:DefaultInit()

end

function CLuaBindDetailWnd:Init()

	
	
	
end

function CLuaBindDetailWnd:Update()
	if self.lastInput~=self.QnInputField.value then
		self.lastInput=self.QnInputField.value
		self.bindLabel.text= bindPlayerName
		self.bindLabel.gameObject:SetActive(false)
		local content = self.QnInputField.value
		
		if string.len(content)>8 then 
			content = tonumber(content or 0)
			Gac2Gas.QueryMergeBattleBindCodePlayerName(content)
		end
	end
	
	
	
end

function CLuaBindDetailWnd:OnEnable()

	g_ScriptEvent:AddListener("QueryMergeBattleHuiLiuBindStatusResult", self, "SetbindStatus")
	g_ScriptEvent:AddListener("QueryMergeBattleBindCodePlayerNameResult", self, "SetbindName")
	g_ScriptEvent:AddListener("MergeBattleHuiLiuPlayerBindSuccess", self, "OnBindSuccess")
	Gac2Gas.QueryMergeBattleHuiLiuBindStatus()

end

function CLuaBindDetailWnd:OnDisable()
	g_ScriptEvent:RemoveListener("QueryMergeBattleHuiLiuBindStatusResult", self, "SetbindStatus")
	g_ScriptEvent:RemoveListener("QueryMergeBattleBindCodePlayerNameResult", self, "SetbindName")
	g_ScriptEvent:RemoveListener("MergeBattleHuiLiuPlayerBindSuccess", self, "OnBindSuccess")
end

function CLuaBindDetailWnd:SetbindName(bindPlayerId,bindPlayerName)
	self.bindLabel.text= bindPlayerName
	self.bindLabel.gameObject:SetActive(true)
end


function CLuaBindDetailWnd:SetbindStatus(canBind, hasBind,bindPlayerId, bindPlayerName)
	self.canbind=canBind
	self.hasbind=hasBind
	local label = CommonDefs.GetComponent_Component_Type(self.m_DoBtn.gameObject.transform:Find("Label"), typeof(UILabel))
	if hasBind==true then
		label.text=LocalString.GetString("已绑定")
		CUICommonDef.SetActive(self.m_DoBtn.gameObject, false, true)
		self.tipsLabel.gameObject:SetActive(false)
		if bindPlayerName and bindPlayerName~="" and bindPlayerId then
			self.bindLabel.text= bindPlayerName
			self.bindLabel.gameObject:SetActive(true)
		end
	else
		if canBind==true then
			self.tipsLabel.text=LocalString.GetString("您是回流玩家，可以绑定邀请码")
			CUICommonDef.SetActive(self.m_DoBtn.gameObject, true, true)
			label.text=LocalString.GetString("绑定邀请码")
			self.tipsLabel.color=Color.green
			self.tipsLabel.gameObject:SetActive(true)
		else
			self.tipsLabel.text=LocalString.GetString("您不是回流玩家，无法绑定邀请码")
			self.tipsLabel.color=Color.red
			label.text=LocalString.GetString("无法绑定")
			CUICommonDef.SetActive(self.m_DoBtn.gameObject, false, true)
			self.tipsLabel.gameObject:SetActive(true)	
		end
		
	end
end

function CLuaBindDetailWnd:OnBindSuccess()

	self.hasbind=true
	local label = CommonDefs.GetComponent_Component_Type(self.m_DoBtn.gameObject.transform:Find("Label"), typeof(UILabel))
	label.text=LocalString.GetString("已绑定")
	CUICommonDef.SetActive(self.m_DoBtn.gameObject, false, true)
	self.tipsLabel.gameObject:SetActive(false)
end

function CLuaBindDetailWnd:SetBindSuccessStatus()
	self.hasbind=true
	local label = CommonDefs.GetComponent_Component_Type(self.m_DoBtn.gameObject.transform:Find("Label"), typeof(UILabel))
	label.text=LocalString.GetString("已绑定")
	CUICommonDef.SetActive(self.m_DoBtn.gameObject, false, true)
	self.tipsLabel.gameObject:SetActive(false)
end

function CLuaBindDetailWnd:DefaultInit()
	self.tipsLabel.gameObject:SetActive(false)
	self.bindLabel.gameObject:SetActive(false)
	local label = CommonDefs.GetComponent_Component_Type(self.m_DoBtn.gameObject.transform:Find("Label"), typeof(UILabel))
	label.text=LocalString.GetString("绑定邀请码")
	CUICommonDef.SetActive(self.m_DoBtn.gameObject, false, true)
end
