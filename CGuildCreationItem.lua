-- Auto Generated!!
local CGuildCreationItem = import "L10.UI.CGuildCreationItem"
local Guild_Setting = import "L10.Game.Guild_Setting"
CGuildCreationItem.m_UpdateData_CS2LuaHook = function (this, itemData) 
    --Dictionary<string, object> responseNum_obj = CDesignData.DesignData["Guild_Setting"]["response_build_number"] as Dictionary<string, object>;
    -- System.Convert.ToUInt32(responseNum_obj["Value"]);
    --Dictionary<string, object> responseNum_obj = CDesignData.DesignData["Guild_Setting"]["response_build_number"] as Dictionary<string, object>;
    local responseNum = Guild_Setting.GetData().response_build_number
    -- System.Convert.ToUInt32(responseNum_obj["Value"]);

    this.m_LabelId.Text = tostring(itemData.GuildId)
    this.m_LabelName.Text = itemData.Name
    this.m_LabelFounder.Text = itemData.FounderName
    this.m_LabelResponse.Text = (itemData.ResponseNum .. "/") .. responseNum

    local hour = math.floor(itemData.TimeLeft / 3600)
    local min = math.floor((itemData.TimeLeft - hour * 3600) / 60)
    local sec = itemData.TimeLeft - hour * 3600 - min * 60
    this.m_LabelTimeLeft.Text = (((this:Get2bitString(hour) .. ":") .. this:Get2bitString(min)) .. ":") .. this:Get2bitString(sec)

    this.m_TimeLeft = itemData.TimeLeft
end
CGuildCreationItem.m_CountDown_CS2LuaHook = function (this) 
    if this.m_TimeLeft <= 0 then
        return
    end
    this.m_TimeLeft = this.m_TimeLeft - 1

    local timeLeft = this.m_TimeLeft
    local hour = math.floor(timeLeft / 3600)
    local min = math.floor((timeLeft - hour * 3600) / 60)
    local sec = timeLeft - hour * 3600 - min * 60
    this.m_LabelTimeLeft.Text = (((this:Get2bitString(hour) .. ":") .. this:Get2bitString(min)) .. ":") .. this:Get2bitString(sec)
end
CGuildCreationItem.m_Get2bitString_CS2LuaHook = function (this, v) 
    if v < 10 then
        return "0" .. v
    else
        return "" .. v
    end
end
