local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CButton = import "L10.UI.CButton"
local CTaskMgr = import "L10.Game.CTaskMgr"

LuaLiuRuShiTaskFailWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaLiuRuShiTaskFailWnd, "DescLabel", "DescLabel", UILabel)
RegistChildComponent(LuaLiuRuShiTaskFailWnd, "CancelBtn", "CancelBtn", CButton)
RegistChildComponent(LuaLiuRuShiTaskFailWnd, "RestartBtn", "RestartBtn", CButton)

--@endregion RegistChildComponent end

function LuaLiuRuShiTaskFailWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    UIEventListener.Get(self.RestartBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnRestartBtnClick()
    end)
end

function LuaLiuRuShiTaskFailWnd:Init()
    local descText = LuaLiuRuShiMgr.m_TaskFailData.m_FailDescText
    if not System.String.IsNullOrEmpty(descText) then self.DescLabel.text = descText end
    local cancelBtnText = LuaLiuRuShiMgr.m_TaskFailData.m_CancelBtnText
    if not System.String.IsNullOrEmpty(cancelBtnText) then self.CancelBtn.Text = cancelBtnText end
    local restartBtnText = LuaLiuRuShiMgr.m_TaskFailData.m_RestartBtnText
    if not System.String.IsNullOrEmpty(restartBtnText) then self.RestartBtn.Text = restartBtnText end
end

--@region UIEvent
function LuaLiuRuShiTaskFailWnd:OnRestartBtnClick()
    local taskId = LuaLiuRuShiMgr.m_TaskFailData.m_RestartTaskId
    if taskId then CTaskMgr.Inst:TrackToDoTask(taskId) end
    CUIManager.CloseUI(CLuaUIResources.LiuRuShiTaskFailWnd)
end
--@endregion UIEvent

