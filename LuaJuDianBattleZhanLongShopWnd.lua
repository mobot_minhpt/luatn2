local CItemMgr = import "L10.Game.CItemMgr"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local QnTableView = import "L10.UI.QnTableView"
local UILabel = import "UILabel"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local CChatLinkMgr = import "CChatLinkMgr"
local CUITexture = import "L10.UI.CUITexture"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local CItem = import "L10.Game.CItem"

LuaJuDianBattleZhanLongShopWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaJuDianBattleZhanLongShopWnd, "BuyBtn", "BuyBtn", GameObject)
RegistChildComponent(LuaJuDianBattleZhanLongShopWnd, "GoodsGrid", "GoodsGrid", QnTableView)
RegistChildComponent(LuaJuDianBattleZhanLongShopWnd, "NameLabel", "NameLabel", UILabel)
RegistChildComponent(LuaJuDianBattleZhanLongShopWnd, "TypeLabel", "TypeLabel", UILabel)
RegistChildComponent(LuaJuDianBattleZhanLongShopWnd, "QnIncreseAndDecreaseButton", "QnIncreseAndDecreaseButton", QnAddSubAndInputButton)
RegistChildComponent(LuaJuDianBattleZhanLongShopWnd, "LevelLabel", "LevelLabel", UILabel)
RegistChildComponent(LuaJuDianBattleZhanLongShopWnd, "Icon", "Icon", CUITexture)
RegistChildComponent(LuaJuDianBattleZhanLongShopWnd, "DescScrollView", "DescScrollView", CUIRestrictScrollView)
RegistChildComponent(LuaJuDianBattleZhanLongShopWnd, "DescLabel", "DescLabel", UILabel)
RegistChildComponent(LuaJuDianBattleZhanLongShopWnd, "OwnedNumLabel", "OwnedNumLabel", UILabel)
RegistChildComponent(LuaJuDianBattleZhanLongShopWnd, "QnCostAndOwnMoneyCostLabel", "QnCostAndOwnMoneyCostLabel", UILabel)
RegistChildComponent(LuaJuDianBattleZhanLongShopWnd, "QnCostAndOwnMoneyOwnLabel", "QnCostAndOwnMoneyOwnLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaJuDianBattleZhanLongShopWnd,"m_GoodsList")
RegistClassMember(LuaJuDianBattleZhanLongShopWnd,"m_SelectedItemIndex")

function LuaJuDianBattleZhanLongShopWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.BuyBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBuyBtnClick()
	end)

    --@endregion EventBind end
	self.QnIncreseAndDecreaseButton.onValueChanged = DelegateFactory.Action_uint(function(val)
        self:OnValueChanged(val)
    end)
end

function LuaJuDianBattleZhanLongShopWnd:Init()
	self.GoodsGrid.m_DataSource = DefaultTableViewDataSource.Create(
        function() 
			return self.m_GoodsList and #self.m_GoodsList or 0 
		end,
        function(item,index) 
			self:InitItem(item.transform, index) 
		end)
    self.GoodsGrid.OnSelectAtRow = DelegateFactory.Action_int(function(row)
        self:OnSelectAtRow(row)
    end)
    self:OnJuDianZhanLongShopInfo()
end

function LuaJuDianBattleZhanLongShopWnd:InitItem(transform, index) 
	local icon = transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local nameLabel = transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local priceLabel = transform:Find("PriceLabel"):GetComponent(typeof(UILabel))
    --local moneySprite = transform:Find("MoneySprite"):GetComponent(typeof(UISprite))
    --local needSprite = transform:Find("NeedSprite"):GetComponent(typeof(UISprite))
    local disableSprite = transform:Find("Icon/DisableSprite"):GetComponent(typeof(UISprite))
    local levelLabel = transform:Find("LevelLabel"):GetComponent(typeof(UILabel))
    local numLimitLabel = transform:Find("NumLimitLabel"):GetComponent(typeof(UILabel))
    local globalSellOutTag = transform:Find("GlobalSellOutTag").gameObject
    local lockSprite = transform:Find("LockSprite"):GetComponent(typeof(UISprite))
    local unlock = transform:Find("Unlock").gameObject
    local milestoneLabelLabel = transform:Find("Unlock/MilestoneLabelLabel"):GetComponent(typeof(UILabel))

    local data = self.m_GoodsList[index + 1]

	nameLabel.text = nil
    levelLabel.text = nil
	milestoneLabelLabel.text = ""
	disableSprite.enabled = false
    icon:Clear()
    globalSellOutTag:SetActive(data.remainCount == 0)
    lockSprite.enabled = false
    unlock:SetActive(false)
    icon.alpha = 1

	local templateId = data.itemId
	local item = CItemMgr.Inst:GetItemTemplate(templateId)
	if item ~= nil then
		nameLabel.text = item.Name
		icon:LoadMaterial(item.Icon)
	end

	priceLabel.text = data.needScore
	numLimitLabel.text = SafeStringFormat3(LocalString.GetString("本周剩余%d"), data.remainCount)
    
end

function LuaJuDianBattleZhanLongShopWnd:OnSelectAtRow(row) 
	self.m_SelectedItemIndex = row
	self:UpdateDescription()
    self:RefreshInputMinMax()
end

function LuaJuDianBattleZhanLongShopWnd:UpdateDescription()
    local data = self.m_GoodsList[self.m_SelectedItemIndex + 1]
    self.LevelLabel.text = ""
    local item = CItemMgr.Inst:GetItemTemplate(data.itemId)
    self.NameLabel.text = item.Name
    local desc = CItem.GetItemDescription(item.ID,true)
    self.Icon:LoadMaterial(item.Icon)
    self.DescLabel.text = CChatLinkMgr.TranslateToNGUIText(desc, false)
    self.DescScrollView:ResetPosition()
    self.QnIncreseAndDecreaseButton:SetValue(1, true)
    local typedata = Item_Type.GetData(item.Type)
    self.TypeLabel.text = ""
    if typedata ~= nil then
        local itemType = typedata.Name
        self.TypeLabel.text = itemType
    end
    self.QnCostAndOwnMoneyCostLabel.text = 1
    self.OwnedNumLabel.text = SafeStringFormat3(LocalString.GetString("已拥有:%d"), data.buyCount) 

end

function LuaJuDianBattleZhanLongShopWnd:RefreshInputMinMax()
    local data = self.m_GoodsList[self.m_SelectedItemIndex + 1]
    local cost = self.QnIncreseAndDecreaseButton:GetValue() * data.needScore
    self.QnCostAndOwnMoneyCostLabel.text = tostring(cost)
    self.QnIncreseAndDecreaseButton:SetMinMax(1, 1, 1)
end

function LuaJuDianBattleZhanLongShopWnd:OnEnable()
    g_ScriptEvent:AddListener("SendGuildJuDianZhanLongShopInfo", self, "OnJuDianZhanLongShopInfo")
end

function LuaJuDianBattleZhanLongShopWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendGuildJuDianZhanLongShopInfo", self, "OnJuDianZhanLongShopInfo")
end

function LuaJuDianBattleZhanLongShopWnd:OnJuDianZhanLongShopInfo()
    self.m_GoodsList = {}
    if LuaJuDianBattleMgr.m_ZhanLongShopInfo and LuaJuDianBattleMgr.m_ZhanLongShopInfo.shopInfo then
        for itemId, goodsInfo in pairs(LuaJuDianBattleMgr.m_ZhanLongShopInfo.shopInfo) do
            table.insert(self.m_GoodsList,{itemId = itemId, needScore = goodsInfo.needScore, buyCount = goodsInfo.buyCount, remainCount = goodsInfo.remainCount})
        end
    end
    table.sort(self.m_GoodsList,function (a,b)
        return a.itemId < b.itemId
    end)
    self.GoodsGrid:ReloadData(false, false)
    self.GoodsGrid:SetSelectRow(0, true)
    self.QnCostAndOwnMoneyOwnLabel.text = LuaJuDianBattleMgr.m_ZhanLongShopInfo and LuaJuDianBattleMgr.m_ZhanLongShopInfo.hasScore or ""
end
--@region UIEvent

function LuaJuDianBattleZhanLongShopWnd:OnBuyBtnClick()
    local data = self.m_GoodsList[self.m_SelectedItemIndex + 1]
    Gac2Gas.BuyGuildJuDianZhanLongShopItem(data.itemId)
end
function LuaJuDianBattleZhanLongShopWnd:OnValueChanged(val)
    local data = self.m_GoodsList[self.m_SelectedItemIndex + 1]
    self.QnCostAndOwnMoneyCostLabel.text = val * data.needScore
end


--@endregion UIEvent

