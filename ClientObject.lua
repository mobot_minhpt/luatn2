local LuaTweenUtils = import "LuaTweenUtils"
local CClientNpc = import "L10.Game.CClientNpc"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CScene=import "L10.Game.CScene"
local CLingShouPartnerInfoType = import "L10.Game.CLingShouPartnerInfoType"
local CLingShouMgr=import "L10.Game.CLingShouMgr"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local CCppBuff = import "L10.Game.CCppBuff"
local CClientObject = import "L10.Game.CClientObject"
local CTeamMgr = import "L10.Game.CTeamMgr"
local EnumActionState = import "L10.Game.EnumActionState"
local CClientMonster = import "L10.Game.CClientMonster"
local CClientOtherPlayer = import "L10.Game.CClientOtherPlayer"
local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"
local CPostEffect = import "L10.Engine.CPostEffect"
local CCommonUIFxWnd = import "L10.UI.CCommonUIFxWnd"
local CExpressionActionMgr = import "L10.Game.CExpressionActionMgr"
local CPUBGMgr = import "L10.Game.CPUBGMgr"
local CEffectMgr = import "L10.Game.CEffectMgr"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local CMoveScalingEffect = import "L10.Game.CMoveScalingEffect"
local EKickOutType = import "L10.Engine.EKickOutType"
local EWeaponVisibleReason = import "L10.Engine.EWeaponVisibleReason"

LuaClientObject = {}

function LuaClientObject:SyncMonsterAlwaysShowNameBoard(engineId, isShow)
	local obj = CClientObjectMgr.Inst:GetObject(engineId)
	if not obj then return end
	if TypeIs(obj, typeof(CClientMonster)) then
		obj:UpdateHeadInfoVisibilityByServer(isShow>0)
	end
end

CClientObject.RegisterStatusLuaMethod("OnEnter_YingJiu2022",function(this,EventId)
	-- print("CClientObject OnEnter_YingJiu2022")
	if this==CClientMainPlayer.Inst then
		LuaHLPYHelpWnd.s_YingJiu2022Status = true
		if not CUIManager.IsLoaded(CLuaUIResources.HLPYHelpWnd) then
			CUIManager.ShowUI(CLuaUIResources.HLPYHelpWnd)
		end
		g_ScriptEvent:BroadcastInLua("OnEnter_YingJiu2022")
	end
	this:ShowActionState(EnumActionState.Casting,{0})
	if LuaHLPYHelpWnd.s_RescueTargetEngineId>0 then
		local obj = CClientObjectMgr.Inst:GetObject(LuaHLPYHelpWnd.s_RescueTargetEngineId)
		if obj then
			this:LookAt(obj.Pos)
		end
	end
end)

CClientObject.RegisterStatusLuaMethod("OnLeave_YingJiu2022",function(this,EventId)
	-- print("CClientObject OnLeave_YingJiu2022",this)
	if this==CClientMainPlayer.Inst then
		LuaHLPYHelpWnd.s_YingJiu2022Status = false
    	g_ScriptEvent:BroadcastInLua("OnLeave_YingJiu2022")
	end
	this:ShowActionState(EnumActionState.Idle,{})
end)

CClientObject.RegisterStatusLuaMethod("OnEnter_Embracing",function(this,EventId)
	if this == CClientMainPlayer.Inst then
		if CScene.MainScene then
			local gamePlayId = CScene.MainScene.GamePlayDesignId
			if WinterOlympic_CurlingSetting.GetData().PlayDesignId == gamePlayId and CUIManager.IsLoaded(CLuaUIResources.CurlingOlympicWinterGamesPlayWnd) then
				g_ScriptEvent:BroadcastInLua("CurlingOlympicWinterGamesOnEnterEmbracing")
			end
		end
	end
end)

CClientObject.RegisterStatusLuaMethod("OnLeave_Embracing",function(this,EventId)
	if this == CClientMainPlayer.Inst then
		if CUIManager.IsLoaded(CLuaUIResources.CurlingOlympicWinterGamesPlayWnd) then
			g_ScriptEvent:BroadcastInLua("CurlingOlympicWinterGamesOnLeaveEmbracing")
		end
	end
end)

CClientObject.RegisterStatusLuaMethod("OnEnter_WuYiReverseTransform",function(this,EventId)
	if CClientMainPlayer.Inst and this == CClientMainPlayer.Inst then
		local objs = CClientPlayerMgr.Inst.m_Id2VisiblePlayer
		local buffs = CClientMainPlayer.Inst.BuffProp.Buffs
		CommonDefs.DictIterate(buffs, DelegateFactory.Action_object_object(function (___key, ___value) 
            if WuYi_ReverseTransform.Exists(___key) then
				local data = WuYi_ReverseTransform.GetData(___key)
				if data then
					CClientOtherPlayer.s_OtherStatusFemaleMonsterId = data.FemaleMonsterId
					CClientOtherPlayer.s_OtherStatusMaleMonsterId = data.MaleMonsterId
				end
			end
        end))
		CommonDefs.DictIterate(objs, DelegateFactory.Action_object_object(function (___key, obj) 
            obj:RefreshAppearance()
        end))
	end
end)

CClientObject.RegisterStatusLuaMethod("OnLeave_WuYiReverseTransform",function(this,EventId)
	if this == CClientMainPlayer.Inst then
		local objs = CClientPlayerMgr.Inst.m_Id2VisiblePlayer
		CClientOtherPlayer.s_OtherStatusFemaleMonsterId = 0
		CClientOtherPlayer.s_OtherStatusMaleMonsterId = 0
		CommonDefs.DictIterate(objs, DelegateFactory.Action_object_object(function (___key, obj) 
            obj:RefreshAppearance()
        end))
	end
end)

CClientObject.RegisterStatusLuaMethod("OnLeave_Embraced",function(this,EventId)
	if TypeIs(this, typeof(CClientNpc)) then
		if CScene.MainScene then
			local gamePlayId = CScene.MainScene.GamePlayDesignId
			if gamePlayId == WinterOlympic_CurlingSetting.GetData().PlayDesignId then
				local data = NPC_NPC.GetData(this.TemplateId)
				if data then
					this.RO.Scale = data.Scale
				end
			end
		end
	end
end)

CClientObject.RegisterStatusLuaMethod("OnEnter_SouCha2022",function(this,EventId)
	if CClientMainPlayer.Inst and this == CClientMainPlayer.Inst then
		--CPostEffect.EnableWaterWaveMat(true)
		--CPostEffect.LeftWaveDuration = 15
		--EventManager.BroadcastInternal(EnumEventType.OnLoadCommonUIFx,{"Fx/UI/Prefab/UI_Shanye_Tansuo.prefab"})
		CCommonUIFxWnd.Instance:LoadUIFxAssignedPath("Fx/UI/Prefab/UI_Shanye_Tansuo.prefab", 2)
	end
end)

CClientObject.RegisterStatusLuaMethod("OnLeave_SouCha2022",function(this,EventId)
	if CClientMainPlayer.Inst and this == CClientMainPlayer.Inst then
		--CPostEffect.DisableWaterWaveMat(true)
		--EventManager.BroadcastInternal(EnumEventType.OnDestroyCommonUIFx, {})
		CCommonUIFxWnd.Instance:DestroyUIFxAssignedPath(2)
	end
end)

CClientObject.RegisterStatusLuaMethod("OnEnter_DuLing",function(this,EventId)
	if CClientMainPlayer.Inst and this == CClientMainPlayer.Inst then
		LuaShiTuMgr:ShowShiTuDuLingTaskWnd()
	end
end)

CClientObject.RegisterStatusLuaMethod("OnLeave_DuLing",function(this,EventId)
	if CClientMainPlayer.Inst and this == CClientMainPlayer.Inst then
		LuaShiTuMgr:CloseShiTuDuLingTaskWnd()
	end
end)

CClientObject.RegisterStatusLuaMethod("OnEnter_JinLuHunYuanZhanDead",function(this,EventId)
	CPUBGMgr.Inst:OnEnterChiJiPreDie(this)
end)

CClientObject.RegisterStatusLuaMethod("OnLeave_JinLuHunYuanZhanDead",function(this,EventId)
	CPUBGMgr.Inst:OnLeaveChiJiPreDie(this)
end)

CClientObject.RegisterStatusLuaMethod("OnEnter_TurkeyMatchPushSnowball",function(this,EventId)
	if not this.RO:ContainFx("TurkeySnowball") then
		local snowBallFxId = 88804278
		local fx = CEffectMgr.Inst:AddObjectFX(snowBallFxId, this.RO, 0, this.RO.Scale, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
		this.RO:AddFX("TurkeySnowball", fx)
	end
end)

CClientObject.RegisterStatusLuaMethod("OnLeave_TurkeyMatchPushSnowball",function(this,EventId)
	this.RO:RemoveFX("TurkeySnowball")
end)

CClientObject.RegisterStatusLuaMethod("OnEnter_SnowballHit",function(this,EventId)
	this.RO:SetKickOutStatus(true, EKickOutType.ParabolaWithoutRotate, 0.5, -1, -100)
	this.RO:Preview("die01", 1)
end)

CClientObject.RegisterStatusLuaMethod("OnLeave_SnowballHit",function(this,EventId)
	this.RO:SetKickOutStatus(false, EKickOutType.ParabolaWithoutRotate, 0.5, -1, -100)
end)

CClientObject.RegisterStatusLuaMethod("OnEnter_SeaMonsterHiding",function(this, EventId)
	this.RO:DoAni("stand02")
end)

CClientObject.RegisterStatusLuaMethod("OnEnter_MoveProgress",function(this, EventId)
	this.RO:SetKickOutStatus(true, EKickOutType.Line, 0.5)
end)

CClientObject.RegisterStatusLuaMethod("OnLeave_MoveProgress",function(this, EventId)
	--this.RO:SetKickOutStatus(false, EKickOutType.Line)
end)

CClientObject.RegisterStatusLuaMethod("OnEnter_FlyProgress",function(this, EventId)
	this.RO:SetKickOutStatus(true, EKickOutType.ParabolaWithoutRotate, 0.5, -1, -100)
end)

CClientObject.RegisterStatusLuaMethod("OnLeave_FlyProgress",function(this, EventId)
	--this.RO:SetKickOutStatus(false, EKickOutType.ParabolaWithoutRotate, 0.5, -1, -20)
end)

CClientObject.RegisterStatusLuaMethod("OnLeave_JiaHeFly",function(this, EventId)
	LuaValentine2023Mgr:LeaveJiaHeFly()
end)

CClientObject.RegisterStatusLuaMethod("OnEnter_NavalPhoenixCatch",function(this, EventId)
	local fx = CEffectMgr.Inst:AddObjectFX(88804522, this.RO, 1, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
	this.RO:AddFX(88804522, fx)
	this.RO:DoAni("qinghongfly04",true,0,1,0.5,true,1)
	local y = this.RO.Position.y
	LuaTweenUtils.SetDelay(LuaTweenUtils.DOMoveY(this.RO.transform,y+30,2.5,false),0.5)
end)
CClientObject.RegisterStatusLuaMethod("OnLeave_NavalPhoenixCatch",function(this, EventId)
	this.RO:RemoveFX(88804522)
end)
CClientObject.RegisterStatusLuaMethod("OnEnter_NavalPhoenixCatchEnd",function(this, EventId)
	LuaTweenUtils.DOKill(this.RO.transform,false)
	LuaTweenUtils.DOMoveY(this.RO.transform,this.WorldPos.y,0.6,false)
end)
CClientObject.RegisterStatusLuaMethod("OnLeave_NavalPhoenixCatchEnd",function(this, EventId)
	LuaTweenUtils.DOKill(this.RO.transform,true)
	this:ShowActionState(EnumActionState.Idle,{})
end)
CClientObject.RegisterStatusLuaMethod("OnEnter_RollerCoasterRun",function(this, EventId)
	this:UpdateSnowFootPrint()
end)
CClientObject.RegisterStatusLuaMethod("OnLeave_RollerCoasterRun",function(this, EventId)
	this:UpdateSnowFootPrint()
end)

CClientObject.RegisterStatusLuaMethod("OnEnter_QingMingPush",function(this, EventId)
	this.RO:SetKickOutStatus(true, EKickOutType.Line, QingMing2023_PVPSetting.GetData().PushBackTime)
end)

CClientObject.RegisterStatusLuaMethod("OnLeave_QingMingPush",function(this, EventId)
	this.RO:SetKickOutStatus(false, EKickOutType.Line, 0.5)
end)

CClientObject.RegisterStatusLuaMethod("OnEnter_NewWeddingBaiTang", function(this, EventId)
	if not this:IsInVehicle() then
		this.RO:SetWeaponVisible(EWeaponVisibleReason.BaiTianDi, false)
	end
end)

CClientObject.RegisterStatusLuaMethod("OnLeave_NewWeddingBaiTang", function(this, EventId)
	if not this:IsInVehicle() then
		this.RO:SetWeaponVisible(EWeaponVisibleReason.BaiTianDi, true)
	end
end)

CClientObject.RegisterStatusLuaMethod("OnEnter_JumpBack", function(this, EventId)
	this.RO:SetKickOutStatus(true, EKickOutType.ParabolaWithoutRotate, tonumber(LiuYi2023_BossFight.GetData("JumpDuration").Value), -1, tonumber(LiuYi2023_BossFight.GetData("JumpGravity").Value))
end)

CClientObject.RegisterStatusLuaMethod("OnLeave_JumpBack", function(this, EventId)
	this.RO:SetKickOutStatus(false, EKickOutType.ParabolaWithoutRotate, tonumber(LiuYi2023_BossFight.GetData("JumpDuration").Value), -1, tonumber(LiuYi2023_BossFight.GetData("JumpGravity").Value))
end)

CClientObject.RegisterStatusLuaMethod("OnEnter_TravelSnowInjury", function(this, EventId)
	local clip = CreateFromClass(MakeGenericClass(Dictionary, String, String))
    clip:Clear()
    CommonDefs.DictAdd(clip, typeof(String), "walk02", typeof(String), "injuredwalk01")
    CommonDefs.DictAdd(clip, typeof(String), "run02", typeof(String), "injuredwalk01")
	CommonDefs.DictAdd(clip, typeof(String), "stand02", typeof(String), "injuredstand01")
    this.RO.m_AnimationChangeCashDic = clip
end)

CClientObject.RegisterStatusLuaMethod("OnLeave_TravelSnowInjury", function(this, EventId)
	local clip = CreateFromClass(MakeGenericClass(Dictionary, String, String))
    clip:Clear()
    this.RO.m_AnimationChangeCashDic = clip
end)

CClientObject.RegisterStatusLuaMethod("OnEnter_NavalWarIncreaseDamage",function(this,EventId)
	g_ScriptEvent:BroadcastInLua("NavalWarStatusChange",this.EngineId)
end)
CClientObject.RegisterStatusLuaMethod("OnLeave_NavalWarIncreaseDamage",function(this,EventId)
	g_ScriptEvent:BroadcastInLua("NavalWarStatusChange",this.EngineId)
end)
CClientObject.RegisterStatusLuaMethod("OnEnter_NavalWarModerate",function(this,EventId)
	g_ScriptEvent:BroadcastInLua("NavalWarStatusChange",this.EngineId)
end)
CClientObject.RegisterStatusLuaMethod("OnLeave_NavalWarModerate",function(this,EventId)
	g_ScriptEvent:BroadcastInLua("NavalWarStatusChange",this.EngineId)
end)
CClientObject.RegisterStatusLuaMethod("OnEnter_NavalWarNavalBurning",function(this,EventId)
	g_ScriptEvent:BroadcastInLua("NavalWarStatusChange",this.EngineId)
end)
CClientObject.RegisterStatusLuaMethod("OnLeave_NavalWarNavalBurning",function(this,EventId)
	g_ScriptEvent:BroadcastInLua("NavalWarStatusChange",this.EngineId)
end)

function Gas2Gac.SyncLingShouPartnerInfo(lingshouId, partnerInfoU)
	local partnerInfo = CLingShouPartnerInfoType()
	partnerInfo:LoadFromString(partnerInfoU)

	local d = CLingShouMgr.Inst:GetLingShouDetails(lingshouId)
	if d then
		d.data.Props.PartnerInfo = partnerInfo
	end
	g_ScriptEvent:BroadcastInLua("SyncLingShouPartnerInfo",lingshouId,partnerInfo)
end

function Gas2Gac.SyncPlayerBindPartnerLingShouId(lingshouId)
	if CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp then
		CClientMainPlayer.Inst.ItemProp.BindedPartenerLingShouId = lingshouId
		g_ScriptEvent:BroadcastInLua("UpdateJieBanLingShouId")
		EventManager.BroadcastInternalForLua(EnumEventType.UpdateJieBanLingShouId, {lingshouId})
	end
end

function Gas2Gac.AddBuffAllData(engineId, buffId, buffUD)
	local obj = CClientObjectMgr.Inst:GetObject(engineId);
	if not obj then return end
	local buffObj = CCppBuff()
	buffObj:LoadFromString(buffUD)
	if not buffObj then return end
	obj.BuffProp.Buffs[buffId] = buffObj;
	EventManager.BroadcastInternalForLua(EnumEventType.OnBuffInfoUpdate,{engineId})
end

function Gas2Gac.TeamMemberAddBuffAllData(playerId, buffId, buffUD)
	local buffData = CCppBuff()
	buffData:LoadFromString(buffUD)
	CTeamMgr.Inst:AddTeamMemberBuff(playerId, buffId, buffData)
end