local AlignType = import "L10.UI.CTooltip+AlignType"
local QnTableView=import "L10.UI.QnTableView"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local UILabel = import "UILabel"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CPackageItemCell = import "L10.UI.CPackageItemCell"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local AlignType2 = import "L10.UI.CItemInfoMgr+AlignType"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CButton = import "L10.UI.CButton"

LuaCommonItemChooseWnd=class()
RegistClassMember(LuaCommonItemChooseWnd,"m_CommitBtn")
RegistClassMember(LuaCommonItemChooseWnd,"m_CommitBtnLabel")
RegistClassMember(LuaCommonItemChooseWnd,"m_VoidLabel")
RegistClassMember(LuaCommonItemChooseWnd,"m_TableView")
RegistClassMember(LuaCommonItemChooseWnd,"m_TitleLabel")
RegistClassMember(LuaCommonItemChooseWnd,"m_DescLabel")
RegistClassMember(LuaCommonItemChooseWnd,"m_CloseBtn")
--当前选中对象 一个Table
--单选时 永远只保存一个对象
RegistClassMember(LuaCommonItemChooseWnd,"m_CurrentItemIds")
--展示的ItemList
RegistClassMember(LuaCommonItemChooseWnd,"m_ItemList")
--是否支持物品多选
RegistClassMember(LuaCommonItemChooseWnd,"m_EnableMultiSelect")
--是否支持物品多选

--获取途径
RegistClassMember(LuaCommonItemChooseWnd,"m_AccessTableView")
RegistClassMember(LuaCommonItemChooseWnd,"m_AccessItemTemplateIds")

function LuaCommonItemChooseWnd:InitComponents()
    self.m_TableView = self.transform:Find("Anchor/Offset/TableView"):GetComponent(typeof(QnTableView))
    self.m_AccessTableView = self.transform:Find("Anchor/Offset/AccessTableView"):GetComponent(typeof(QnTableView))
    self.m_CommitBtn = self.transform:Find("Anchor/Offset/CommitBtn"):GetComponent(typeof(CButton))
    self.m_CommitBtnLabel = self.transform:Find("Anchor/Offset/CommitBtn/Label"):GetComponent(typeof(UILabel))
    self.m_VoidLabel = self.transform:Find("Anchor/Offset/Bg/VoidLabel"):GetComponent(typeof(UILabel))

    self.m_TitleLabel = self.transform:Find("Anchor/Offset/Bg/TitleLabel"):GetComponent(typeof(UILabel))
    self.m_DescLabel = self.transform:Find("Anchor/Offset/DescLabel"):GetComponent(typeof(UILabel))
    self.m_CloseBtn = self.transform:Find("Anchor/Offset/CloseBtn").gameObject
    self.m_TipBtn = self.transform:Find("Anchor/Offset/TipBtn").gameObject
end

function LuaCommonItemChooseWnd:Init()
    self:InitComponents()

    UIEventListener.Get(self.m_CommitBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
        self:OnCommit()
    end)

    UIEventListener.Get(self.m_CloseBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        self:OnClose()
    end)

    if LuaCommonItemChooseMgr.m_TipMsg then
        self.m_TipBtn:SetActive(true)
        UIEventListener.Get(self.m_TipBtn).onClick = DelegateFactory.VoidDelegate(function (p)
            g_MessageMgr:ShowMessage(LuaCommonItemChooseMgr.m_TipMsg)
        end)
    else
        self.m_TipBtn:SetActive(false)
    end

    self.m_EnableMultiSelect = LuaCommonItemChooseMgr.m_EnableMultiSelect
    self.m_TitleLabel.text = LuaCommonItemChooseMgr.m_TitleStr
    self.m_CommitBtnLabel.text = LuaCommonItemChooseMgr.m_BtnStr
    self.m_DescLabel.text = LuaCommonItemChooseMgr.m_DescStr
    self.m_VoidLabel.text = LuaCommonItemChooseMgr.m_VoidStr
    self.m_AccessItemTemplateIds = LuaCommonItemChooseMgr.m_AccessItemTemplateIds

    self.m_TableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
        local itemId = self.m_ItemList[row+1]
        if not self.m_EnableMultiSelect then
            local commonItem = CItemMgr.Inst:GetById(itemId)
            CItemInfoMgr.ShowLinkItemInfo(commonItem, true, nil, AlignType2.Default, 0, 0, 0, 0, 0)
        end
        local tableItem = self.m_TableView:GetItemAtRow(row)
        local itemCell = tableItem.transform:GetComponent(typeof(CPackageItemCell))

        if self.m_CurrentItemIds[itemId] == nil then
            -- 不支持多选时，取消之前选中的物品
            if not self.m_EnableMultiSelect then
                for i=1,#self.m_ItemList do
                    if self.m_CurrentItemIds[self.m_ItemList[i]] ~= nil then
                        local tableItem = self.m_TableView:GetItemAtRow(i-1)
                        local itemCell = tableItem.transform:GetComponent(typeof(CPackageItemCell))
                        if itemCell~= nil then
                            itemCell.Selected = false
                        end
                    end 
                end
                self.m_CurrentItemIds = {}
            end
            self.m_CurrentItemIds[itemId] = 1
        else
            -- 之前选中了自己
            self.m_CurrentItemIds[itemId] = nil
        end

        if itemCell~= nil then
            itemCell.Selected = self.m_CurrentItemIds[itemId]
        end

        self:OnItemClick()
    end)
    
    self:InitStatus()
    self:InitAccessTable()
end

function LuaCommonItemChooseWnd:InitStatus()
    self.m_ItemList = {}
    -- 一个Set
    if self.m_CurrentItemIds == nil then
        self.m_CurrentItemIds = {}
    end
    local newCurrentItemsIds = {}
    for k, v in pairs(self.m_CurrentItemIds) do
        local commonItem = CItemMgr.Inst:GetById(k)
        if commonItem and commonItem.Amount > 0 then
            newCurrentItemsIds[k] = v
        end
    end
    self.m_CurrentItemIds = newCurrentItemsIds

    local count = CClientMainPlayer.Inst.ItemProp:GetPlaceMaxSize(EnumItemPlace.Bag)
    -- 遍历包裹里的物品
    for i=0, count-1 do
        local itemId = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Bag, i + 1)
        -- 确定合适的物品
        if LuaCommonItemChooseMgr.m_ItemSelectFunc then
            if LuaCommonItemChooseMgr.m_ItemSelectFunc(itemId) then
                table.insert(self.m_ItemList, itemId)
            end
        else
            table.insert(self.m_ItemList, itemId)
        end
    end
    
    if #self.m_ItemList == 0 then
        if #self.m_AccessItemTemplateIds == 0 then
            self.m_VoidLabel.gameObject:SetActive(true)
        else
            self.m_AccessTableView.gameObject:SetActive(true)
        end
        self.m_CommitBtn.Enabled = false
        self.m_DescLabel.text = ""
    else
        self.m_VoidLabel.gameObject:SetActive(false)
        self.m_AccessTableView.gameObject:SetActive(false)
        self.m_CommitBtn.Enabled = true
    end

    if LuaCommonItemChooseMgr.m_ItemSort then
        table.sort(self.m_ItemList, LuaCommonItemChooseMgr.m_ItemSort)
    end

    self.m_TableView.m_DataSource=DefaultTableViewDataSource.Create(
        function()
            return #self.m_ItemList
        end,
        
        function(item,index)
            self:InitItem(item.transform, index)
        end)

    self.m_TableView:ReloadData(true, false)

    for i=1,#self.m_ItemList do
        local tableItem = self.m_TableView:GetItemAtRow(i-1)
        local itemCell = tableItem.transform:GetComponent(typeof(CPackageItemCell))
        if itemCell~= nil then
            itemCell.Selected = self.m_CurrentItemIds[self.m_ItemList[i]] ~= nil
        end
    end
end

function LuaCommonItemChooseWnd:InitAccessTable()
    -- self.m_AccessTableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
    --     local templateId = self.m_AccessItemTemplateIds[row+1]

    --     CItemAccessListMgr.Inst:ShowItemAccessInfo(templateId, true, nil, AlignType.Right)
    -- end)

    self.m_AccessTableView.m_DataSource=DefaultTableViewDataSource.Create(
        function()
            return #self.m_AccessItemTemplateIds
        end,
        
        function(item,index)
            self:InitAccessItem(item.transform, index)
        end)

    self.m_AccessTableView:ReloadData(true, false)
end

function LuaCommonItemChooseWnd:InitItem(transform, index)
    local itemCell = transform:GetComponent(typeof(CPackageItemCell))
    local itemId = self.m_ItemList[index+1]
    if itemCell ~= nil then
        itemCell:Init(itemId, false, false, false, nil)
        itemCell.Selected = false
        if self.m_EnableMultiSelect then
            itemCell.OnItemLongPressDelegate = DelegateFactory.Action_CPackageItemCell(
                function(cell)
                    local commonItem = CItemMgr.Inst:GetById(itemId)
                    CItemInfoMgr.ShowLinkItemInfo(commonItem, true, nil, AlignType2.Default, 0, 0, 0, 0, 0)
                end
            )
        end
    end
    if LuaCommonItemChooseMgr.m_ItemInit then
        LuaCommonItemChooseMgr.m_ItemInit(itemId, itemCell)
    end
end

function LuaCommonItemChooseWnd:InitAccessItem(transform, index)
    local itemCell = transform:GetComponent(typeof(CPackageItemCell))
    local icon = transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    local quality = transform:Find("QualitySprite"):GetComponent(typeof(UISprite))

    local templateId = self.m_AccessItemTemplateIds[index+1]
    local itemData = Item_Item.GetData(templateId)

    icon:LoadMaterial(itemData.Icon)
    quality.spriteName = CUICommonDef.GetItemCellBorder(itemData.NameColor)
    itemCell.OnItemLongPressDelegate = DelegateFactory.Action_CPackageItemCell(function(cell)
        CItemInfoMgr.ShowLinkItemTemplateInfo(templateId, false, nil, AlignType2.Default, 0, 0, 0, 0)
    end)
    itemCell.OnItemClickDelegate = DelegateFactory.Action_CPackageItemCell(function(cell)
        CItemAccessListMgr.Inst:ShowItemAccessInfo(templateId, true, nil, AlignType.Top)
    end)
end


function LuaCommonItemChooseWnd:OnCommit()
    local items = {}
    for k, v in pairs(self.m_CurrentItemIds) do
        table.insert(items, k)
    end
    if LuaCommonItemChooseMgr.m_CommitFunc then
        LuaCommonItemChooseMgr.m_CommitFunc(items)
    end
end

function LuaCommonItemChooseWnd:OnClose()
    LuaCommonItemChooseMgr:Reset()
    CUIManager.CloseUI(CLuaUIResources.CommonItemChooseWnd)
end

function LuaCommonItemChooseWnd:OnItemClick()
    local items = {}
    for k, v in pairs(self.m_CurrentItemIds) do
        table.insert(items, k)
    end
    if LuaCommonItemChooseMgr.m_ItemClickFunc then
        LuaCommonItemChooseMgr.m_ItemClickFunc(self.m_DescLabel, items)
    end
end

function LuaCommonItemChooseWnd:SendItem(args)
    local itemId = args[0]
    if LuaCommonItemChooseMgr.m_ItemSelectFunc then
        if CItemMgr.Inst:GetById(itemId) == nil or LuaCommonItemChooseMgr.m_ItemSelectFunc(itemId) then
            self:InitStatus()
        end
    else
        self:InitStatus()
    end
end

function LuaCommonItemChooseWnd:SetItemAt(place, position, oldItemId, newItemId)
	if LuaCommonItemChooseMgr.m_ItemSelectFunc then
        if CItemMgr.Inst:GetById(oldItemId) == nil or CItemMgr.Inst:GetById(newItemId) or LuaCommonItemChooseMgr.m_ItemSelectFunc(oldItemId) or LuaCommonItemChooseMgr.m_ItemSelectFunc(newItemId) then
            self:InitStatus()
        end
    else
        self:InitStatus()
    end
end

function LuaCommonItemChooseWnd:OnEnable()
	g_ScriptEvent:AddListener("SendItem", self, "SendItem")
	g_ScriptEvent:AddListener("SetItemAt", self, "SetItemAt")
end

function LuaCommonItemChooseWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SendItem", self, "SendItem")
    g_ScriptEvent:RemoveListener("SetItemAt", self, "SetItemAt")
end
