local UIWidget = import "UIWidget"
local UISlider = import "UISlider"
local CUIGameObjectPool = import "L10.UI.CUIGameObjectPool"
local CButton = import "L10.UI.CButton"
local QnButton = import "L10.UI.QnButton"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"

LuaFootStepPlayWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaFootStepPlayWnd, "ScreenArea", "ScreenArea", UIWidget)
RegistChildComponent(LuaFootStepPlayWnd, "TimeLeft", "TimeLeft", GameObject)
RegistChildComponent(LuaFootStepPlayWnd, "TimeLeftLabel", "TimeLeftLabel", UILabel)
RegistChildComponent(LuaFootStepPlayWnd, "WrongFx", "WrongFx", GameObject)
RegistChildComponent(LuaFootStepPlayWnd, "ProgressBar", "ProgressBar", UISlider)
RegistChildComponent(LuaFootStepPlayWnd, "FootStepRoot", "FootStepRoot", GameObject)
RegistChildComponent(LuaFootStepPlayWnd, "Pool", "Pool", CUIGameObjectPool)
RegistChildComponent(LuaFootStepPlayWnd, "CloseButton", "CloseButton", CButton)
RegistChildComponent(LuaFootStepPlayWnd, "Result", "Result", GameObject)
RegistChildComponent(LuaFootStepPlayWnd, "StartBtn", "StartBtn", QnButton)
RegistChildComponent(LuaFootStepPlayWnd, "ShareBtn", "ShareBtn", CButton)

--@endregion RegistChildComponent end

RegistClassMember(LuaFootStepPlayWnd,"m_CreatStepTick")
RegistClassMember(LuaFootStepPlayWnd,"m_IsFirstLongStep")
RegistClassMember(LuaFootStepPlayWnd,"m_TimeTick")
RegistClassMember(LuaFootStepPlayWnd,"m_TotalPlayTime")
RegistClassMember(LuaFootStepPlayWnd,"m_LeftTime")
RegistClassMember(LuaFootStepPlayWnd,"m_CurrentProgress")

RegistClassMember(LuaFootStepPlayWnd,"m_CommonStepHeight")
RegistClassMember(LuaFootStepPlayWnd,"m_PlayId")
RegistClassMember(LuaFootStepPlayWnd,"m_ScreenHeight")
RegistClassMember(LuaFootStepPlayWnd,"m_TargetStep")
RegistClassMember(LuaFootStepPlayWnd,"m_StepMoveTime")
RegistClassMember(LuaFootStepPlayWnd,"m_Speed")
RegistClassMember(LuaFootStepPlayWnd,"m_StepIntervalRange")
RegistClassMember(LuaFootStepPlayWnd,"m_StepOffsetRange")
RegistClassMember(LuaFootStepPlayWnd,"m_LongPressRange")
RegistClassMember(LuaFootStepPlayWnd,"m_LongPressProbability")

RegistClassMember(LuaFootStepPlayWnd,"m_Id2FootStep")
RegistClassMember(LuaFootStepPlayWnd,"m_CurCreateFootIndex")
RegistClassMember(LuaFootStepPlayWnd,"m_CurCreateFootHeight")
RegistClassMember(LuaFootStepPlayWnd,"m_CurCreateFootTime")
RegistClassMember(LuaFootStepPlayWnd,"m_CurCreateFootType")

RegistClassMember(LuaFootStepPlayWnd,"m_CanOperateStepIndex")

RegistClassMember(LuaFootStepPlayWnd,"m_IsResult")
RegistClassMember(LuaFootStepPlayWnd,"m_CancleClick")
function LuaFootStepPlayWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.CloseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCloseButtonClick()
	end)


	
	UIEventListener.Get(self.StartBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnStartClick()
	end)


	
	UIEventListener.Get(self.ShareBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareBtnClick()
	end)


    --@endregion EventBind end

	self.m_Id2FootStep = nil
	self.m_CreatStepTick = nil
	self.m_CancleClick = nil
	self.m_IsFirstLongStep = true
	self.m_TimeTick = nil
	self.m_CurrentProgress = 0
	self.m_LeftTime = 0
	self.m_PlayId = 0
	self.m_Speed = 0 
	self.m_ScreenHeight = 0
	self.m_TargetStep = 0 
	self.m_StepMoveTime = 0
	self.m_StepIntervalRange = nil
	self.m_StepOffsetRange = nil
	self.m_LongPressRange = nil
	self.m_TotalPlayTime = 0
	self.m_IsResult = false
	self.m_LongPressProbability = 0

	self.m_CommonStepHeight = self.Pool.transform:Find("FootStepNode/CommonNode/Normal"):GetComponent(typeof(UIWidget)).height
end

function LuaFootStepPlayWnd:Init()
	self.m_PlayId = LuaZhuJueJuQingMgr.m_FootStepPlayId
	local data = ZhuJueJuQing_FootStepPlay.GetData(self.m_PlayId)
	if not data then return end
	self.m_IsFirstLongStep = true
	self.m_TargetStep = data.TargetCount
	self.m_TotalPlayTime = data.PlayTime
	self.m_LeftTime = data.PlayTime
	self.m_StepMoveTime = data.MoveTime
	self.m_StepIntervalRange = {data.IntervalRange[0], data.IntervalRange[1]}
	self.m_StepOffsetRange = {data.OffsetRange[0], data.OffsetRange[1]}
	self.m_LongPressRange = {data.LongPressRange[0], data.LongPressRange[1]}
	self.m_LongPressProbability = data.LongPressProbability
	self.ScreenArea:GetComponent(typeof(UIWidget)):ResetAndUpdateAnchors()
	self.m_ScreenHeight = self.ScreenArea.height
	self.m_Speed = self.m_ScreenHeight / self.m_StepMoveTime
	self.Result.gameObject:SetActive(false)
	self.ShareBtn.gameObject:SetActive(false)
	self.StartBtn.gameObject:SetActive(true)
	self.TimeLeftLabel.text = self:GetRemainTimeText(self.m_LeftTime)
	self.ProgressBar.value = 0
	self.m_CanOperateStepIndex = 0
	self.WrongFx.gameObject:SetActive(false)

	self:InitFirstShowStep()
end

-- 初始化几个脚印
function LuaFootStepPlayWnd:InitFirstShowStep()
	self.m_Id2FootStep = {}
	self.m_CurCreateFootIndex = 0
	self.m_CurCreateFootHeight = 0
	self.m_CurCreateFootTime = 0
	self.m_CurCreateFootType = 0 -- 左脚
	local startHight = self.StartBtn.transform.localPosition.y - self.m_CommonStepHeight / 2
	local duration = math.abs(self.m_ScreenHeight / 2 - startHight) / self.m_Speed
	self.m_CurCreateFootTime = self.m_CurCreateFootTime + (duration * 1000)
	self:CreateFootStep(1,self.m_CurCreateFootTime,false)
	while self.m_CurCreateFootTime >= 0 do
		local itemHeightMoveTime = (self.m_CurCreateFootHeight / self.m_Speed) * 1000
		local randomIntervalTime = math.random(self.m_StepIntervalRange[1] * 1000, self.m_StepIntervalRange[2] * 1000)
		self.m_CurCreateFootTime = self.m_CurCreateFootTime - itemHeightMoveTime - randomIntervalTime
		self.m_CurCreateFootIndex = self.m_CurCreateFootIndex + 1
		self:CreateFootStep(self.m_CurCreateFootIndex,self.m_CurCreateFootTime,false)		
	end
end

function LuaFootStepPlayWnd:CreateFootStep(index, fastForwardTime, isAutoMove)
	local footStep = self.Pool:GetFromPool(0)
	footStep.gameObject.transform.parent = self.FootStepRoot.transform
	footStep.gameObject.transform.localScale = Vector3.one
	footStep.gameObject:SetActive(true)
	local startPos = {}
	local endPos = {}
	local leftOrRight = 0
	local isLongPress = false
	local totalLongPressTime = 0
	local longStepHeight = 0
	local isFirstLongStep = false
	local curItemHgight = self.m_CommonStepHeight
	if index == 1 then
		self.m_CurCreateFootType = 0
		startPos.x = self.StartBtn.transform.localPosition.x
		startPos.y = self.StartBtn.transform.localPosition.y - curItemHgight / 2
	else
		self.m_CurCreateFootType = self.m_CurCreateFootType == 0 and 1 or 0
		local xoffset = math.random(self.m_StepOffsetRange[1], self.m_StepOffsetRange[2])
		if self.m_CurCreateFootType == 0 then xoffset = -1 * xoffset end
		startPos.x = xoffset
		startPos.y = self.m_ScreenHeight / 2 - (fastForwardTime / 1000 * self.m_Speed)
		local randomLongPress = math.random(0, 100)
		isLongPress = randomLongPress <= (self.m_LongPressProbability * 100) and true or false
		if isLongPress then
			if self.m_IsFirstLongStep then 
				isFirstLongStep = true  
				self.m_IsFirstLongStep = false
			else isFirstLongStep = false end
			totalLongPressTime = math.random(self.m_LongPressRange[1] * 1000, self.m_LongPressRange[2] * 1000)
			totalLongPressTime = totalLongPressTime / 1000
			longStepHeight = totalLongPressTime * self.m_Speed
			curItemHgight = longStepHeight
		end
	end
	self.m_CurCreateFootIndex = index
	self.m_CurCreateFootHeight = curItemHgight
	leftOrRight = self.m_CurCreateFootType
	endPos.x = startPos.x
	endPos.y =  - (self.m_ScreenHeight / 2)
	self.m_Id2FootStep[index] = footStep.gameObject
	local luafunc = footStep:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
	luafunc:Init(index,startPos, endPos, self.m_Speed, leftOrRight,
			isLongPress, isFirstLongStep, longStepHeight, totalLongPressTime,
			function (go) self:OnRecycle( go, index) end,
			function (go) self:OnMiss( go, index) end,
			function (go) self:OnSuccess( go, index ) end,isAutoMove)
	if self.m_CanOperateStepIndex == index then
		luafunc:SetCanClickStatus()
	end
end

function LuaFootStepPlayWnd:GetRemainTimeText(totalSeconds)
	if totalSeconds < 0 then
        totalSeconds = 0
    end
    if totalSeconds >= 3600 then
        return SafeStringFormat3("%02d:%02d:%02d", math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
    elseif totalSeconds > 10 then
        return SafeStringFormat3("%02d:%02d", math.floor(totalSeconds / 60), totalSeconds % 60)
	else
		return SafeStringFormat3("[FF0000]%02d:%02d[-]", math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end

function LuaFootStepPlayWnd:OnMiss( go,index )
	if index ~= self.m_CanOperateStepIndex then return end
	self.WrongFx.gameObject:SetActive(true)
	self.m_CanOperateStepIndex = self.m_CanOperateStepIndex + 1
	self.ProgressBar.value = self.m_CurrentProgress / self.m_TargetStep
	if self.m_Id2FootStep and self.m_Id2FootStep[self.m_CanOperateStepIndex] then
		local luafunc = self.m_Id2FootStep[self.m_CanOperateStepIndex]:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
		luafunc:SetCanClickStatus()	
	end
end

function LuaFootStepPlayWnd:OnSuccess( go ,index)
	if index ~= self.m_CanOperateStepIndex then return end
	self.WrongFx.gameObject:SetActive(false)
	self.m_CanOperateStepIndex = self.m_CanOperateStepIndex + 1
	self.m_CurrentProgress = self.m_CurrentProgress + 1
	self.ProgressBar.value = self.m_CurrentProgress / self.m_TargetStep
	if self.m_CurrentProgress >= self.m_TargetStep then
		self:OnReachTarget()
		return
	end
	if self.m_Id2FootStep and self.m_Id2FootStep[self.m_CanOperateStepIndex] then
		local luafunc = self.m_Id2FootStep[self.m_CanOperateStepIndex]:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
		luafunc:SetCanClickStatus()	
	end
end

function LuaFootStepPlayWnd:OnRecycle(go,index)
	self.Pool:Recycle(go.gameObject)
	if self.m_Id2FootStep and self.m_Id2FootStep[index] then
		self.m_Id2FootStep[index] = nil
	end
end
function LuaFootStepPlayWnd:StartCreateStepTick()
	if self.m_CreatStepTick then UnRegisterTick(self.m_CreatStepTick)  self.m_CreatStepTick = nil end
	local randomIntervalTime = math.random(self.m_StepIntervalRange[1] * 1000, self.m_StepIntervalRange[2] * 1000)
	self.m_CurCreateFootTime = self.m_CurCreateFootTime - randomIntervalTime
	self.m_CreatStepTick = RegisterTickOnce(function()
		if self.m_IsResult then return end
		local itemHeightMoveTime = (self.m_CurCreateFootHeight / self.m_Speed) * 1000
		self.m_CurCreateFootIndex = self.m_CurCreateFootIndex + 1
		self:CreateFootStep(self.m_CurCreateFootIndex, - itemHeightMoveTime, true)
		self.m_CurCreateFootTime = - itemHeightMoveTime
		self:StartCreateStepTick()
	end,( -1 * self.m_CurCreateFootTime))
end

function LuaFootStepPlayWnd:StartTimeLeftTick()
	if self.m_TimeTick then UnRegisterTick(self.m_TimeTick)  self.m_TimeTick = nil end
	self.m_TimeTick = RegisterTick(function()
		self.m_LeftTime = self.m_LeftTime - 1
		self.TimeLeftLabel.text = self:GetRemainTimeText(self.m_LeftTime)
		if self.m_LeftTime <= 0 then
			self:OnTimeOut()
		end
	end,1000)
end

function LuaFootStepPlayWnd:OnTimeOut()
	g_MessageMgr:ShowMessage("FootStepPlayTimeOut")
	self:ResetPlay()
end

function LuaFootStepPlayWnd:OnReachTarget()
	self.m_IsResult = true
	self:ClearTick()
	if self.m_Id2FootStep then
		for k,v in pairs(self.m_Id2FootStep) do
			if not v then return end
			local luafunc = v.gameObject:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
			luafunc:Stop()
		end
	end
	for i = self.FootStepRoot.transform.childCount - 1,0,-1 do
		local child = self.FootStepRoot.transform:GetChild(i)
		self.Pool:Recycle(child.gameObject)
	end
	self.m_Id2FootStep = nil
	self.Result.gameObject:SetActive(true)
	local fxPath = CUIFxPaths.WanChengFxPath
	EventManager.BroadcastInternalForLua(EnumEventType.OnLoadCommonUIFx, {fxPath})
	Gac2Gas.FinishEventTask(self.m_PlayId,"chasesteps")
	--self.ShareBtn.gameObject:SetActive(true)
	if self.m_CancleClick then UnRegisterTick(self.m_CancleClick) self.m_CancleClick = nil end
    self.m_CancleClick = RegisterTickOnce(function()
        CUIManager.CloseUI(CLuaUIResources.FootStepPlayWnd)
    end, 1000)
end

function LuaFootStepPlayWnd:ResetPlay()
	self.m_IsResult = true
	self:ClearTick()
	if self.m_Id2FootStep then
		for k,v in pairs(self.m_Id2FootStep) do
			if not v then return end
			local luafunc = v.gameObject:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
			luafunc:Stop()
		end
	end
	for i = self.FootStepRoot.transform.childCount - 1,0,-1 do
		local child = self.FootStepRoot.transform:GetChild(i)
		self.Pool:Recycle(child.gameObject)
	end
	
	self.m_Id2FootStep = nil
	self.ProgressBar.value = 0
	self.m_CanOperateStepIndex = 0
	self.m_CurrentProgress = 0
	self.m_IsFirstLongStep = true
	self.m_LeftTime = self.m_TotalPlayTime
	self.TimeLeftLabel.text = self:GetRemainTimeText(self.m_LeftTime)
	self.Result.gameObject:SetActive(false)
	self.ShareBtn.gameObject:SetActive(false)
	self.StartBtn.gameObject:SetActive(true)
	self.WrongFx.gameObject:SetActive(false)
	self:InitFirstShowStep()
end
--@region UIEvent

function LuaFootStepPlayWnd:OnCloseButtonClick()
	g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("LEAVE_COPY_CONFIRM"), function ()
		Gac2Gas.RequestLeavePlay()
		CUIManager.CloseUI(CLuaUIResources.FootStepPlayWnd)
	end,nil, nil, nil, false)
end

function LuaFootStepPlayWnd:OnStartClick()
	self.StartBtn.gameObject:SetActive(false)
	self.m_CanOperateStepIndex = 1
	if self.m_Id2FootStep then
		for k,v in pairs(self.m_Id2FootStep) do
			local luafunc = v.gameObject:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
			luafunc:StartMove()
			if k == 1 then
				luafunc:SetCanClickStatus()
			end
		end
	end
	self.m_IsResult = false
	self:StartCreateStepTick()
	self:StartTimeLeftTick()
end



function LuaFootStepPlayWnd:OnShareBtnClick()
	local excludeRoot = self.transform:Find("ScreenArea/ExcludeRoot").gameObject
	CUICommonDef.CaptureScreenUIAndShare(CLuaUIResources.FootStepPlayWnd,excludeRoot)
end


--@endregion UIEvent

function LuaFootStepPlayWnd:ClearTick()
	if self.m_TimeTick then UnRegisterTick(self.m_TimeTick)  self.m_TimeTick = nil end
	if self.m_CreatStepTick then UnRegisterTick(self.m_CreatStepTick)  self.m_CreatStepTick = nil end
	if self.m_CancleClick then UnRegisterTick(self.m_CancleClick) self.m_CancleClick = nil end
end

function LuaFootStepPlayWnd:OnDisable()
	self:ClearTick()
end