-- Auto Generated!!
local CAuditLingyuDetailWnd = import "L10.UI.CAuditLingyuDetailWnd"
local CAuditLingyuItem = import "L10.UI.CAuditLingyuItem"
local CAuditLingyuMgr = import "L10.Game.CAuditLingyuMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local Object = import "UnityEngine.Object"
local Vector3 = import "UnityEngine.Vector3"
CAuditLingyuDetailWnd.m_show_CS2LuaHook = function (this) 
    this.itemPrefab:SetActive(false)
    this:removeOldItems()
    local detailList = CAuditLingyuMgr.Instance.DetailList
    do
        local i = 0
        while i < detailList.Count do
            local go = CommonDefs.Object_Instantiate(this.itemPrefab)
            go.transform.parent = this.grid.transform
            go.transform.localScale = Vector3.one
            go:SetActive(true)
            local item = CommonDefs.GetComponent_GameObject_Type(go, typeof(CAuditLingyuItem))
            local data = detailList[i]
            item:SetData(data.Amount, data.StartTime, data.EndTime)
            i = i + 1
        end
    end
    this.grid:Reposition()
    this.scrollView:InvalidateBounds()
    this.scrollView:SetDragAmount(0, 0, false)
end
CAuditLingyuDetailWnd.m_removeOldItems_CS2LuaHook = function (this) 
    local l = this.grid:GetChildList()
    do
        local i = 0
        while i < l.Count do
            Object.Destroy(l[i])
            i = i + 1
        end
    end
end
