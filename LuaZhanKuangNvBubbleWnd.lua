local CUIGameObjectPool = import "L10.UI.CUIGameObjectPool"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local UIWidget = import "UIWidget"
local GameObject = import "UnityEngine.GameObject"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local CEffectMgr = import "L10.Game.CEffectMgr"
local CPos = import "L10.Engine.CPos"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"

LuaZhanKuangNvBubbleWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaZhanKuangNvBubbleWnd, "ScreenArea", "ScreenArea", UIWidget)
RegistChildComponent(LuaZhanKuangNvBubbleWnd, "BubbleRoot", "BubbleRoot", GameObject)
RegistChildComponent(LuaZhanKuangNvBubbleWnd, "Bubble", "Bubble", UILabel)
RegistChildComponent(LuaZhanKuangNvBubbleWnd, "Pool", "Pool", CUIGameObjectPool)

--@endregion RegistChildComponent end
RegistClassMember(LuaZhanKuangNvBubbleWnd,"m_Id")
RegistClassMember(LuaZhanKuangNvBubbleWnd,"m_BubbleData")
RegistClassMember(LuaZhanKuangNvBubbleWnd,"m_ScreenWidth")
RegistClassMember(LuaZhanKuangNvBubbleWnd,"m_ScreenHight")
RegistClassMember(LuaZhanKuangNvBubbleWnd,"m_CurClickNum")
RegistClassMember(LuaZhanKuangNvBubbleWnd,"m_CreateBubbleTick")
RegistClassMember(LuaZhanKuangNvBubbleWnd,"m_TextNeedNum")
RegistClassMember(LuaZhanKuangNvBubbleWnd,"m_ShakeFx")
RegistClassMember(LuaZhanKuangNvBubbleWnd,"m_BubbleList")
function LuaZhanKuangNvBubbleWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.m_ScreenWidth = 0
    self.m_ScreenHight = 0
    self.m_CurClickNum = 0
    self.m_BubbleList = nil
    self.m_Id = 0
    self.m_BubbleData = nil
    self.m_ShakeFx = nil
    self.m_CreateBubbleTick = nil
    self.m_TextNeedNum = 0
end

function LuaZhanKuangNvBubbleWnd:Init()
    self.m_Id = LuaZhuJueJuQingMgr.m_BubblePlayId
    self.m_BubbleData = ZhuJueJuQing_BubblePlay.GetData(self.m_Id)
    if not self.m_BubbleData then
        return
    end
    self.m_TextNeedNum = self.m_BubbleData.NeedCount
    self.m_BubbleList = {}
    for i = 0,self.m_BubbleData.BubbleIds.Length - 1 do
        table.insert(self.m_BubbleList,self.m_BubbleData.BubbleIds[i])
    end
    if self.m_CreateBubbleTick then 
        UnRegisterTick(self.m_CreateBubbleTick)
        self.m_CreateBubbleTick = nil
    end
    local createInterval = self.m_BubbleData.CreateInterval
    self.m_CreateBubbleTick = RegisterTick(function()
        self:CreateNewBubble()
    end, createInterval * 1000)
end

function LuaZhanKuangNvBubbleWnd:CreateNewBubble()
    if not self.m_BubbleData then return end
    self.m_ScreenWidth = self.ScreenArea.width
    self.m_ScreenHight = self.ScreenArea.height
    local textDataIndex = math.random(1,#self.m_BubbleList)
    local textData = ZhuJueJuQing_Bubble.GetData(self.m_BubbleList[textDataIndex])
    local halfsize = textData.BubbleSize / 2
    local x,y = 0,0
    local halfWidth = self.m_ScreenWidth / 2
    local halfHight = self.m_ScreenHight / 2
    local direction = math.random(1,4)
    if direction == 1 then 
        x = math.random(-1 * (halfWidth - halfsize),halfWidth - halfsize)
        y = halfHight - halfsize
    elseif direction == 2 then 
        x = math.random(-1 * (halfWidth - halfsize),halfWidth - halfsize)
        y = -1 * (halfHight - halfsize)
    elseif direction == 3 then
        x = halfWidth - halfsize
        y = math.random(-1 * (halfHight - halfsize),halfHight - halfsize)
    else
        x = -1 * (halfWidth - halfsize)
        y = math.random(-1 * (halfHight - halfsize),halfHight - halfsize)
    end
    local targetPos = {x = x, y = y}
    local moveTime = self.m_BubbleData.MoveTime
    local waitTime = self.m_BubbleData.ClickTime
    local bubble = self.Pool:GetFromPool(0)
    bubble.gameObject.transform.parent = self.BubbleRoot.transform
    bubble.gameObject.transform.localScale = Vector3.one
    bubble.gameObject:SetActive(true)
    bubble.gameObject:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf:Init(
        textData.Text,textData.BubbleSize,targetPos,moveTime,waitTime,function (go)
        self:OnBubbleReachScreenEdge()
        end,function (go,status)
        self:OnBubbleClick(go,status)
        end,function (go)
        self:OnBubbleDisappear(go)
    end)
end

-- 震屏
function LuaZhanKuangNvBubbleWnd:OnBubbleReachScreenEdge()
    if not self.m_BubbleData then return end
    if not CClientMainPlayer.Inst then return end
    local fxId =  self.m_BubbleData.ReachFx
    if self.ShakeFx then
		self.ShakeFx:Destroy()
	end
    local pos = CPos(CClientMainPlayer.Inst.Pos.x + 800, CClientMainPlayer.Inst.Pos.y + 800)
	self.ShakeFx = CEffectMgr.Inst:AddWorldPositionFX(fxId, pos, 0, 0,1,-1,EnumWarnFXType.None,nil,0,0, nil)
end

function LuaZhanKuangNvBubbleWnd:OnBubbleClick(go,status)
    if status == 2 then
       self.m_CurClickNum = self.m_CurClickNum + 1 
       go:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf:ShowBubbleClickDisappear()
    else
        g_MessageMgr:ShowMessage("BubblePlay_NeedClickHighLightBubble")
    end
    self:CheckClickNum()
end

function LuaZhanKuangNvBubbleWnd:OnBubbleDisappear(go)
    go.gameObject:SetActive(false)
    self.Pool:Recycle(go.gameObject)
end

function LuaZhanKuangNvBubbleWnd:CheckClickNum()
    if self.m_CurClickNum >= self.m_TextNeedNum then
        Gac2Gas.FinishTextBubblePlay(self.m_Id)
        CUIManager.CloseUI(CLuaUIResources.ZhanKuangNvBubbleWnd)
    end
end

function LuaZhanKuangNvBubbleWnd:OnDisable()
    if self.m_CreateBubbleTick then 
        UnRegisterTick(self.m_CreateBubbleTick)
        self.m_CreateBubbleTick = nil
    end
    if self.ShakeFx then
		self.ShakeFx:Destroy()
        self.ShakeFx = nil
	end
end


--@region UIEvent

--@endregion UIEvent

