local UIEventListener             = import "UIEventListener"
local LocalString                 = import "LocalString"
local L10                         = import "L10"
local EnumPlayerInfoContext       = import "L10.Game.EnumPlayerInfoContext"
local CPlayerInfoMgr              = import "CPlayerInfoMgr"
local CPersonalSpaceMgr           = import "L10.Game.CPersonalSpaceMgr"
local CommonDefs                  = import "L10.Game.CommonDefs"
local CUIResources                = import "L10.UI.CUIResources"
local CUIManager                  = import "L10.UI.CUIManager"
local PopupMenuItemData           = import "L10.UI.PopupMenuItemData"
local CPlayerShopPopupMenuInfoMgr = import "L10.UI.CPlayerShopPopupMenuInfoMgr"
local CPopupMenuInfoMgrAlignType  = import "L10.UI.CPopupMenuInfoMgr+AlignType"
local EnumPopupMenuItemStyle      = import "L10.UI.EnumPopupMenuItemStyle"
local QnTipButton                 = import "L10.UI.QnTipButton"
local CVoiceMsg                   = import "L10.Game.CVoiceMsg"
local CVoiceMgr                   = import "L10.Game.CVoiceMgr"
local CHongBaoMsg                 = import "L10.Game.CHongBaoMsg"

CLuaPlayerReportMgr = {}
CLuaPlayerReportMgr.m_PersonalSpaceInfo = {} -- 重构自 refs  #70334 个人空间举报接入客户端实现 （看提交历史savetime目前无用，剔除）
CLuaPlayerReportMgr.m_ExpertTeamReportInfo = {} --重构自 refs #85797 [精灵专家团]增加举报log类型
CLuaPlayerReportMgr.m_DefaultReportId = 1
CLuaPlayerReportMgr.m_DefaultReportReason = ""

function CLuaPlayerReportMgr:SetPersonalSpaceReportInfo(defaultReportId, defaultReportReason, url, id)
    self.m_DefaultReportId = defaultReportId
    self.m_DefaultReportReason = defaultReportReason
    self.m_PersonalSpaceInfo = {}
    self.m_ExpertTeamReportInfo.content = defaultReportReason
    self.m_PersonalSpaceInfo.url = url and url or ""
    self.m_PersonalSpaceInfo.id = id and id or 0
end

function CLuaPlayerReportMgr:SetExpertTeamReportInfo(content, qid, aid)
    self.DefaultReportId = 1
    self.m_DefaultReportReason = ""
    self.m_ExpertTeamReportInfo = {}
    self.m_ExpertTeamReportInfo.content = content
    self.m_ExpertTeamReportInfo.qid = qid
    self.m_ExpertTeamReportInfo.aid = aid
end

function CLuaPlayerReportMgr:Reset()
    self.m_PersonalSpaceInfo = {}
    self.m_ExpertTeamReportInfo = {}
    self.m_DefaultReportId = 1
    self.m_DefaultReportReason = ""
end


CLuaPlayerReportWnd = class()

RegistClassMember(CLuaPlayerReportWnd, "okBtn")
RegistClassMember(CLuaPlayerReportWnd, "cancelBtn")
RegistClassMember(CLuaPlayerReportWnd, "selectBtn")
RegistClassMember(CLuaPlayerReportWnd, "selectLabel")
RegistClassMember(CLuaPlayerReportWnd, "playerInfoLabel")
RegistClassMember(CLuaPlayerReportWnd, "roomInfoRoot")
RegistClassMember(CLuaPlayerReportWnd, "roomInfoLabel")
RegistClassMember(CLuaPlayerReportWnd, "reasonInput")
RegistClassMember(CLuaPlayerReportWnd, "titleLabel")

RegistClassMember(CLuaPlayerReportWnd, "reportInfo")
RegistClassMember(CLuaPlayerReportWnd, "selectId")
RegistClassMember(CLuaPlayerReportWnd, "popupMenuArray")

function CLuaPlayerReportWnd:Close()
	CUIManager.CloseUI(CUIResources.PlayerReportWnd)
end

function CLuaPlayerReportWnd:Awake()
    self.okBtn = self.transform:Find("Anchor/Offset/OKButton").gameObject
    self.cancelBtn = self.transform:Find("Anchor/Offset/CancelButton").gameObject
    self.playerInfoLabel = self.transform:Find("Anchor/Offset/NameLabel/ValueLabel"):GetComponent(typeof(UILabel))
    self.roomInfoRoot = self.transform:Find("Anchor/Offset/ChatRoomLabel").gameObject
    self.roomInfoLabel = self.transform:Find("Anchor/Offset/ChatRoomLabel/ValueLabel"):GetComponent(typeof(UILabel))
    self.reasonInput = self.transform:Find("Anchor/Offset/Input"):GetComponent(typeof(UIInput))
    self.selectBtn = self.transform:Find("Anchor/Offset/Reason/QnButton"):GetComponent(typeof(QnTipButton))
    self.selectLabel = self.transform:Find("Anchor/Offset/Reason/QnButton/Label"):GetComponent(typeof(UILabel))
    self.titleLabel = self.transform:Find("Wnd_Bg_Secondary_2/TitleLabel"):GetComponent(typeof(UILabel))
    self.reportInfo = nil
end

function CLuaPlayerReportWnd:Start( )
    UIEventListener.Get(self.cancelBtn).onClick = DelegateFactory.VoidDelegate(function(g)
        self:OnCancelButtonClick(g)
    end)

    UIEventListener.Get(self.okBtn).onClick = DelegateFactory.VoidDelegate(function(g)
        self:OnOkButtonClick(g)
    end)

    self.selectBtn.OnClick = DelegateFactory.Action_QnButton(function (qnButton)
		self:OnSelectButtonClick()
	end)
end

-- Auto Generated!!
function CLuaPlayerReportWnd:Init( )
    local accId, roomId, isMyGamePlayer, playerName = self:GetChatRoomInfo()
    self:InitReportInfo(isMyGamePlayer)

    if accId > 0 then
        self.playerInfoLabel.text = playerName
        self.roomInfoRoot:SetActive(true)
        self.roomInfoLabel.text = tostring(roomId)
    else
        self.playerInfoLabel.text = LuaPlayerReportMgr.m_PlayerReportInfo.playerName
        self.roomInfoRoot:SetActive(false)
    end

    self:InitSelect()
    self.titleLabel.text = LuaPlayerReportMgr.m_PlayerReportInfo.customTitle
    self.reasonInput.characterLimit = 50
end

function CLuaPlayerReportWnd:InitReportInfo(isMyGamePlayer)
    local selectAction = DelegateFactory.Action_int(function (index)
        self.selectId = index + 1
        self.selectLabel.text = self.reportInfo[index + 1].type
	end)

    local tbl = {}
    self.reportInfo = {}
    local isInSameClubHouse = LuaClubHouseMgr:ShowClubHouseReportItem() -- 举报对象是否在同一茶室
    Report_Report.Foreach(function (id, data)
        if not isMyGamePlayer and data.OnlyVisibleInMyGame > 0 then return end
        if not CPersonalSpaceMgr.OpenPersonalSpace and data.Condition == "mengdao" then return end
        if not isInSameClubHouse and data.Condition == "clubhouse" then return end

        local item = PopupMenuItemData(data.Type, selectAction, false, nil, EnumPopupMenuItemStyle.Default)
        table.insert(tbl, item)
        table.insert(self.reportInfo, {id = id, type = data.Type})
    end)
    self.popupMenuArray = Table2Array(tbl, MakeArrayClass(PopupMenuItemData))
end

-- 初始化选择
function CLuaPlayerReportWnd:InitSelect()
    if LuaPlayerReportMgr.m_PlayerReportInfo.playerInfoContext:Equals(EnumPlayerInfoContext.PlotDiscussion) then
        CLuaPlayerReportMgr.m_DefaultReportId = EnumReportId_lua.ePlotDiscuss
    end

    if CLuaPlayerReportMgr.m_DefaultReportId then
        for i = 1, #self.reportInfo do
            if self.reportInfo[i].id == CLuaPlayerReportMgr.m_DefaultReportId then
                self.selectId = i
                self.selectLabel.text = self.reportInfo[i].type
                self.reasonInput.value = CLuaPlayerReportMgr.m_DefaultReportReason
                return
            end
        end
    end

    self.selectId = 1
    self.selectLabel.text = self.reportInfo[1].type
    self.reasonInput.value = nil
end

function CLuaPlayerReportWnd:OnDestroy()
    CLuaPlayerReportMgr:Reset()
end

--#region UIEvent

function CLuaPlayerReportWnd:OnSelectButtonClick()
    if not self.popupMenuArray then return end

    CPlayerShopPopupMenuInfoMgr.ShowPlayerShopPopupMenu(self.popupMenuArray, self.selectBtn.transform,
        CPopupMenuInfoMgrAlignType.Bottom, 2, nil, nil, DelegateFactory.Action(function ()
            self.selectBtn:SetTipStatus(false)
        end), 600, 450)
end

function CLuaPlayerReportWnd:OnCancelButtonClick(go)
    self:Close()
end

function CLuaPlayerReportWnd:OnOkButtonClick(go)
    local reportReason = self.reasonInput.value == nil and "" or StringTrim(self.reasonInput.value)
    local reportId = self.reportInfo[self.selectId].id

    if Report_Report.GetData(reportId).NeedReasonInput > 0 and CommonDefs.StringLength(reportReason) == 0 then
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("请填写举报原因"))
        return
    end

    if LuaPlayerReportMgr.m_PlayerReportInfo.playerInfoContext:Equals(EnumPlayerInfoContext.PlotDiscussion) then
        LuaPlotDiscussionMgr:RequestComplaint(LuaPlayerReportMgr.m_PlayerReportInfo.playerId, reportReason)
        self:Close()
        return
    end

    -- 梦岛
    local url = ""
    local r_id = 0
    if CLuaPlayerReportMgr.m_DefaultReportId and LuaPlayerReportMgr.m_PlayerReportInfo.playerInfoContext:Equals(EnumPlayerInfoContext.PersonalSpace) then
        local defaultSelectIndex = 0
        for index, data in pairs(self.reportInfo) do
            if data.id == CLuaPlayerReportMgr.m_DefaultReportId then
                defaultSelectIndex = index - 1
            end
        end
        if defaultSelectIndex == self.selectId then
            url = CLuaPlayerReportMgr.m_PersonalSpaceInfo.url
            r_id = CLuaPlayerReportMgr.m_PersonalSpaceInfo.id
        end
    end

    --被举报者Id,举报方式,违规类型,举报原因,被举报的发言内容截取1,被举报的发言内容截取2,历史言论1_1,历史言论1_2,历史言论2_1,历史言论2_2,历史言论3_1,历史言论3_2
    local chatMsgs = {}
    local reportMsg = LuaPlayerReportMgr.m_PlayerReportInfo.reportMsg
    chatMsgs[1] = reportMsg and reportMsg or ""
    local msgs = LuaPlayerReportMgr.m_PlayerReportInfo.nearestMsgs
    chatMsgs[2] = (msgs and msgs.Length > 0) and msgs[0] or ""
    chatMsgs[3] = (msgs and msgs.Length > 1) and msgs[1] or ""
    chatMsgs[4] = (msgs and msgs.Length > 2) and msgs[2] or ""
    self:ParseChatMsgs(chatMsgs)

    -- 精灵专家团
    local expertTeamReportInfo =  CLuaPlayerReportMgr.m_ExpertTeamReportInfo
    local expertTeamContent = expertTeamReportInfo and expertTeamReportInfo.context or reportReason
    local expertTeamQid = expertTeamReportInfo and expertTeamReportInfo.qid or 0
    local expertTeamAid = expertTeamReportInfo and expertTeamReportInfo.aid or 0

    local accId, roomId = self:GetChatRoomInfo()

    Gac2Gas.RequestAccuse(LuaPlayerReportMgr.m_PlayerReportInfo.playerId, LuaPlayerReportMgr.m_PlayerReportInfo.context .. LocalString.GetString("举报"),
        self.reportInfo[self.selectId].id, reportReason, url, r_id,
        chatMsgs[1][0], chatMsgs[1][1], chatMsgs[2][0], chatMsgs[2][1], chatMsgs[3][0], chatMsgs[3][1], chatMsgs[4][0], chatMsgs[4][1], 0,
        expertTeamContent, expertTeamQid, expertTeamAid, accId, roomId)
    self:Close()
end

--#endregion UIEvent

--这里假定了举报需要来自交互菜单
function CLuaPlayerReportWnd:GetChatRoomInfo()
    local accId = 0
    local roomId = 0
    local isMyGamePlayer = true
    local playerName = ""
    if CPlayerInfoMgr.PopupMenuInfo.context == EnumPlayerInfoContext.ChatRoom then
        local extraInfoDict = CPlayerInfoMgr.PopupMenuInfo.extraInfo
        local appName = CommonDefs.DictGetValue_LuaCall(extraInfoDict, "AppName")
        accId = tonumber(CommonDefs.DictGetValue_LuaCall(extraInfoDict, "AccId"))
        local member = LuaClubHouseMgr:GetMember(accId)
        roomId = member and member.RoomId or 0
        isMyGamePlayer = LuaClubHouseMgr:IsMyGame(appName)
        playerName = member and member.PlayerName or ""
    end
    return accId, roomId, isMyGamePlayer, playerName
end

-- 聊天信息解析(语音、红包)
function CLuaPlayerReportWnd:ParseChatMsgs(chatMsgs)
    for i = 1, #chatMsgs do
        local msg = ""
        if chatMsgs[i] ~= nil and chatMsgs[i] ~= "" then
            local voiceMsg = CVoiceMsg.Parse(chatMsgs[i])
            if voiceMsg then -- 语音
                local extraText = System.String.IsNullOrEmpty(voiceMsg.ExtraText) and "" or voiceMsg.ExtraText
                local text = System.String.IsNullOrEmpty(voiceMsg.Text) and CVoiceMgr.Inst:GetVoiceText(voiceMsg.VoiceId) or voiceMsg.Text
                msg = SafeStringFormat3("%s%s", extraText, text)
            else
                local hongBaoMsg = CHongBaoMsg.Parse(chatMsgs[i])
                if hongBaoMsg then -- 红包
                    msg = hongBaoMsg.Text
                else
                    msg = chatMsgs[i]
                end
            end
        end
        chatMsgs[i] = L10.Engine.Utility.BreakString(msg, 2, 255, nil)
    end
end
