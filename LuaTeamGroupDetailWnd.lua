require("3rdParty/ScriptEvent")
require("common/common_include")
require("ui/teamgroup/LuaTeamGroupTeamInfo")
require("ui/teamgroup/LuaTeamGroupPlayerInfoItem")
local UISprite = import "UISprite"
local UIWidget = import "UIWidget"
local UIRoot = import "UIRoot"
local MessageMgr  = import "L10.Game.MessageMgr"
local BoxCollider = import "UnityEngine.BoxCollider"
local MonoBehaviour = import "UnityEngine.MonoBehaviour"
local UICamera = import "UICamera"
local Physics = import "UnityEngine.Physics"
local UIGrid = import "UIGrid"
local Gac2Gas = import "L10.Game.Gac2Gas"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIRes = import "L10.UI.CUIResources"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CUIManager = import "L10.UI.CUIManager"
local Vector3 = import "UnityEngine.Vector3"
local CTeamGroupMgr = import "L10.Game.CTeamGroupMgr"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local AlignType = import "L10.UI.CPopupMenuInfoMgr+AlignType"
local EnumPosition = import "L10.Game.CTeamGroupMgr.EnumTeamGroupMemberPosition"
local ClientMainPlayer = import"L10.Game.CClientMainPlayer"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local CChatHelper = import "L10.UI.CChatHelper"
local CTickMgr = import "L10.Engine.CTickMgr"
local GamePlay_GamePlay = import "L10.Game.Gameplay_Gameplay"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CTeamMgr = import "L10.Game.CTeamMgr"
local CCityWarMgr = import "L10.Game.CCityWarMgr"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"
local CGuildMgr = import "L10.Game.CGuildMgr"
local CButton = import "L10.UI.CButton"
local Array_p = MakeArrayClass(PopupMenuItemData)

local GroupIndex = 1
local TeamTable = {}
local TabTable = {}
local PrefixTable= nil

CLuaTeamGroupDetailWnd=class()

RegistClassMember(CLuaTeamGroupDetailWnd,"TeamContainer")
RegistClassMember(CLuaTeamGroupDetailWnd,"TeamPrefab")
RegistClassMember(CLuaTeamGroupDetailWnd,"TeamTabPrefab")
RegistClassMember(CLuaTeamGroupDetailWnd,"TeamTabGrid")
RegistClassMember(CLuaTeamGroupDetailWnd,"LeaveGroupBtn")
RegistClassMember(CLuaTeamGroupDetailWnd,"DismissGroupBtn")
RegistClassMember(CLuaTeamGroupDetailWnd,"MoreInfoBtn")
RegistClassMember(CLuaTeamGroupDetailWnd,"PrepareConfirmBtn")
RegistClassMember(CLuaTeamGroupDetailWnd,"CancelFollowBtn")
RegistClassMember(CLuaTeamGroupDetailWnd,"AskFollowBtn")
RegistClassMember(CLuaTeamGroupDetailWnd,"CancelEditBtn")
RegistClassMember(CLuaTeamGroupDetailWnd,"NormalBottom")
RegistClassMember(CLuaTeamGroupDetailWnd,"EditingBottom")
RegistClassMember(CLuaTeamGroupDetailWnd,"QuickJoinBtn")
RegistClassMember(CLuaTeamGroupDetailWnd,"ShowMatchBtn")
RegistClassMember(CLuaTeamGroupDetailWnd,"AddBtn")
RegistClassMember(CLuaTeamGroupDetailWnd,"TargetLabel")
RegistClassMember(CLuaTeamGroupDetailWnd,"ManagerInfoRoot")

RegistClassMember(CLuaTeamGroupDetailWnd,"focusTeamIndex")
RegistClassMember(CLuaTeamGroupDetailWnd,"Editing")

CLuaTeamGroupDetailWnd.focusPlayerID = -1

function CLuaTeamGroupDetailWnd:OnEnable()
	self:RefreshTeamShow()
	self:RefreshTarget()
	self:RefreshButtons()
end

function CLuaTeamGroupDetailWnd:Awake()
	CLuaTeamGroupDetailWnd.focusPlayerID = -1
	self.focusTeamIndex = 0
	self.Editing = false

	self:InitTeams()
	self:InitTabs()
	local onEditTeamclk = function(go)
		self:OnEditTeam()
	end
	CommonDefs.AddOnClickListener(self.EditTeamBtn,DelegateFactory.Action_GameObject(onEditTeamclk),false)

	local cancelEdit = function(go)
		self.Editing = false
		self:RefreshTeamShow()

	end
	CommonDefs.AddOnClickListener(self.CancelEditBtn,DelegateFactory.Action_GameObject(cancelEdit),false)
	local leaveclick = function(go)
		local confirm = function()
			Gac2Gas.RequestLeaveTeamGroup()
		end
		MessageWndManager.ShowOKCancelMessage(LocalString.GetString("确定离开团队吗?"),DelegateFactory.Action(confirm))
	end
	CommonDefs.AddOnClickListener(self.LeaveGroupBtn,DelegateFactory.Action_GameObject(leaveclick),false)
	local dismissClick = function(go)
		local confirm = function()
			Gac2Gas.RequestDisbandTeamGroup()
		end
		MessageWndManager.ShowOKCancelMessage(LocalString.GetString("确定解散团队吗?"),DelegateFactory.Action(confirm))
	end
	CommonDefs.AddOnClickListener(self.DismissGroupBtn,DelegateFactory.Action_GameObject(dismissClick),false)
	local prepareConfirm = function(go)
		Gac2Gas.RequestTeamGroupConfirm()
	end
	CommonDefs.AddOnClickListener(self.PrepareConfirmBtn,DelegateFactory.Action_GameObject(prepareConfirm),false)
	local startFollow = function(go)
		Gac2Gas.RequestStartTeamGroupFollow()
	end
	CommonDefs.AddOnClickListener(self.AskFollowBtn,DelegateFactory.Action_GameObject(startFollow),false)
	local cancelFollow = function(go)
		Gac2Gas.RequestCancelTeamGroupFollow()
	end
	CommonDefs.AddOnClickListener(self.CancelFollowBtn,DelegateFactory.Action_GameObject(cancelFollow),false)
	local quickJoin = function(go)
		CTeamGroupMgr.Instance:StartQuickJoin()
	end
	CommonDefs.AddOnClickListener(self.QuickJoinBtn,DelegateFactory.Action_GameObject(quickJoin),false)
	local showMatchWnd = function(go)
		CTeamGroupMgr.Instance:ShowMatchWnd()
	end

	CommonDefs.AddOnClickListener(self.ShowMatchBtn,DelegateFactory.Action_GameObject(showMatchWnd),false)
	local onTeamPosClk = function(go)
		CUIManager.ShowUI(CUIRes.TeamManagerWnd)
	end
	CommonDefs.AddOnClickListener(self.TeamPostionBtn,DelegateFactory.Action_GameObject(onTeamPosClk),false)
	local showInvitation = function(go)
		local inviteFriend = function(index)
			CTeamMgr.Inst:RequestFriendsNotInTeam(1)
		end
		local inviteNearby = function(index)
			CTeamMgr.Inst:RequestGetPlayersNearby(1)
		end
		local friend = PopupMenuItemData(LocalString.GetString("好友"),DelegateFactory.Action_int(inviteFriend),false,nil)
		local nearby = PopupMenuItemData(LocalString.GetString("附近"),DelegateFactory.Action_int(inviteNearby),false,nil)
		local ot={}
		ot[1] = friend
		ot[2] = nearby
		CPopupMenuInfoMgr.ShowPopupMenu(Table2Array(ot, Array_p),go.transform,AlignType.Bottom,1,nil,600,true,296)
	end
	CommonDefs.AddOnClickListener(self.AddBtn,DelegateFactory.Action_GameObject(showInvitation),false)
	local queryHp = function()
		Gac2Gas.QueryTeamGroupTeamHpInfo(GroupIndex)
	end

	self.HpTick = CTickMgr.Register(DelegateFactory.Action(queryHp),1000)
	local queryMemberInfo = function()
		Gac2Gas.QueryTeamGroupTeamMemberInfo()
	end
	self.MemberInfoTick = CTickMgr.Register(DelegateFactory.Action(queryMemberInfo),10000)
	queryMemberInfo()
	queryHp()
	g_ScriptEvent:AddListener("TeamGroupMemberInfoReceived", self, "RefreshTeamShow")
	g_ScriptEvent:AddListener("TeamGroupTargetChange", self, "RefreshTarget")
	g_ScriptEvent:AddListener("TeamGroupSelfPositionChange", self, "RefreshButtons")
end

function CLuaTeamGroupDetailWnd:OnDestroy()
	g_ScriptEvent:RemoveListener("TeamGroupMemberInfoReceived", self, "RefreshTeamShow")
	g_ScriptEvent:RemoveListener("TeamGroupTargetChange", self, "RefreshTarget")
	g_ScriptEvent:RemoveListener("TeamGroupSelfPositionChange", self, "RefreshButtons")
	UnRegisterTick(self.MemberInfoTick)
	UnRegisterTick(self.HpTick)
end


function CLuaTeamGroupDetailWnd:InitTeams()
	local widget = self.TeamContainer:GetComponent(typeof(UIWidget))
	local teamWidth = self.TeamPrefab:GetComponent(typeof(UIWidget)).width
	local go = widget.gameObject
	for i = 1,6 do
		local team = CUICommonDef.AddChild(go,self.TeamPrefab)
		local TeamItem = CLuaTeamGroupTeamInfo:new()
		TeamItem.m_OnItemPress = self.OnMemberPress
		TeamItem.m_OnItemDrag = self.OnMemberDrag
		TeamItem.m_OnTitlePress = self.OnTeamPress
		TeamItem.m_OnTitleDrag = self.OnTeamDrag
		TeamItem.m_Parent = self

		TeamItem:AttachToGo(team)
		TeamTable[i] = TeamItem
		local pos = Vector3.zero
		pos.x = -widget.width/2 + widget.width/6*(i-1 + 0.5)
		team.transform.localPosition = pos
	end
	self.TeamDragItem = CLuaTeamGroupTeamInfo:new()
	self.TeamDragItem:AttachToGo(self.TeamPrefab)


	local memberdragtrans = CUICommonDef.TraverseFindChild("PlayerInfoPrefab",self.TeamPrefab.transform)
	local boxcollider = memberdragtrans.gameObject:GetComponent(typeof(BoxCollider))
	MonoBehaviour.Destroy(boxcollider)
	self.TeamPrefab:SetActive(false)
	local memberDraggo = CUICommonDef.AddChild(self.TeamContainer,memberdragtrans.gameObject)
	self.MemberDragItem = CLuaTeamGroupPlayerInfoItem:new()
	self.MemberDragItem:AttachToGo(memberDraggo)
	memberDraggo:SetActive(false)
	boxcollider = memberDraggo:GetComponent(typeof(BoxCollider))
	MonoBehaviour.Destroy(boxcollider)
end
function CLuaTeamGroupDetailWnd:InitTabs()
	local OnTabClick = function(go)
		for i = 1,5 do
			if go == TabTable[i] then
				if GroupIndex ~= i then
					GroupIndex = i

					self:RefreshTeamShow()
				end
				break
			end
		end
	end

	local Grid = self.TeamTabGrid:GetComponent(typeof(UIGrid))
	local gridGO = Grid.gameObject
	if PrefixTable== nil then
		PrefixTable= {LocalString.GetString("甲"),LocalString.GetString("乙"),LocalString.GetString("丙"),LocalString.GetString("丁"),LocalString.GetString("戊")}
	end
	for i =1,5 do
		local team = CUICommonDef.AddChild(gridGO,self.TeamTabPrefab)
		team:SetActive(true)
		team:GetComponent(typeof(CButton)).Text = SafeStringFormat3(LocalString.GetString("%s组"),PrefixTable[i])
		TabTable[i] = team
		CommonDefs.AddOnClickListener(team,DelegateFactory.Action_GameObject(OnTabClick),false)
	end
	self.TeamTabPrefab:SetActive(false)
	Grid:Reposition()
end



function CLuaTeamGroupDetailWnd:OnMemberPress(go,flag)
	if flag then
		CLuaTeamGroupDetailWnd.focusPlayerID,self.focusTeamIndex = self:MemberGOToPlayerID(go)
		if not self.Editing then
			return
		end
		local goParent = go.transform.parent
		local worldPos = goParent:TransformPoint(go.transform.localPosition)
		self.MemberDragItem.m_gameObject:SetActive(true)
		self.MemberDragItem.m_gameObject.transform.localPosition = self.TeamContainer.transform:InverseTransformPoint(worldPos)
		self.MemberDragItem:RefreshInfo(CLuaTeamGroupDetailWnd.focusPlayerID)
	else
		if self.Editing then
			self.MemberDragItem.m_gameObject:SetActive(false)
			local currentPos = UICamera.currentTouch.pos
			local ray = UICamera.currentCamera:ScreenPointToRay(Vector3(currentPos.x,currentPos.y,0))
			local hits = Physics.RaycastAll(ray, 10,  UICamera.currentCamera.cullingMask)
			local hit,id,teamIndex = self:JudgeClickTarget()
			if hit then
				if id > 0 then
					if id ~= CLuaTeamGroupDetailWnd.focusPlayerID then
						Gac2Gas.RequestExchangeTeamGroupMember(CLuaTeamGroupDetailWnd.focusPlayerID,id)
					end
				else
					if teamIndex ~= self.focusTeamIndex then
						Gac2Gas.RequestMoveTeamGroupMember(CLuaTeamGroupDetailWnd.focusPlayerID,teamIndex)
					end
				end
			end
		else
			self:OnMemberSelect(go)
		end
	end
end
function CLuaTeamGroupDetailWnd:OnMemberDrag(go,delta)
	if not self.Editing then
		return
	end
	local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject);
	local trans = self.MemberDragItem.m_gameObject.transform
	local pos = trans.localPosition
	pos.x = pos.x + delta.x*scale
	pos.y = pos.y + delta.y*scale
	trans.localPosition = pos
	self:JudgeTabHit()
end
function CLuaTeamGroupDetailWnd:OnTeamPress(go,flag)
	if not self.Editing then
		return
	end
	if flag then
		self.focusTeamIndex = self:TeamGOToIndex(go.transform.parent.gameObject)
		if CTeamGroupMgr.Instance:GetTeamGroupUnit(self.focusTeamIndex) == nil then
			return
		end
		local goParent = go.transform.parent.parent
		local worldPos = goParent:TransformPoint(go.transform.parent.localPosition)
		self.TeamDragItem.m_gameObject:SetActive(true)
		self.TeamDragItem.m_gameObject.transform.localPosition = self.TeamContainer.transform:InverseTransformPoint(worldPos)
		self.TeamDragItem:RefreshInfo(self.focusTeamIndex)
	else
		self.TeamDragItem.m_gameObject:SetActive(false)
		if CTeamGroupMgr.Instance:GetTeamGroupUnit(self.focusTeamIndex) == nil then
			return
		end
		local currentPos = UICamera.currentTouch.pos
		local ray = UICamera.currentCamera:ScreenPointToRay(Vector3(currentPos.x,currentPos.y,0))
		local hits = Physics.RaycastAll(ray, 10,  UICamera.currentCamera.cullingMask)
		local hit,id,teamIndex = self:JudgeClickTarget()
		if hit then
			if teamIndex ~= self.focusTeamIndex then
				self:DoTeamExchange(self.focusTeamIndex,teamIndex)
			end
		end
	end
end
function CLuaTeamGroupDetailWnd:OnTeamDrag(go,delta)
	if not self.Editing then
		return
	end
	if CTeamGroupMgr.Instance:GetTeamGroupUnit(self.focusTeamIndex) == nil then
		return
	end
	local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject);
	local trans = self.TeamDragItem.m_gameObject.transform
	local pos = trans.localPosition
	pos.x = pos.x + delta.x*scale
	pos.y = pos.y + delta.y*scale
	trans.localPosition = pos
	self:JudgeTabHit()
end

function CLuaTeamGroupDetailWnd:MemberGOToPlayerID(go)
	for k,v in pairs(TeamTable) do

		local id = v:GameObjToPlayerID(go)
		if id ~= nil then
			return id,v.m_Index
		end
    end
	return nil,nil
end

function CLuaTeamGroupDetailWnd:TeamGOToIndex(go)
	for k,v in pairs(TeamTable) do
		if v.m_gameObject == go then
			return v.m_Index
		end
	end
	return 1
end

function CLuaTeamGroupDetailWnd:RefreshTeamShow()
	-- 切换队伍
    local changeViewBtn = self.transform:Find("TopArea/changeViewBtn").gameObject
    if CTeamMgr.Inst:TeamExists()then
        changeViewBtn:SetActive(true)
        UIEventListener.Get(changeViewBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
            CUIManager.ShowUI(CUIResources.TeamWnd)
            CUIManager.CloseUI(CUIResources.TeamGroupWnd)
        end)
    else
        changeViewBtn:SetActive(false)
    end

	self.NormalBottom:SetActive(not self.Editing)
	self.EditingBottom:SetActive(self.Editing)
	for k,v in pairs(TeamTable) do
		v:RefreshInfo(k+(GroupIndex-1)*6)
	end
	for k,v in pairs(TabTable) do
		local sprite = v:GetComponent(typeof(UISprite))
		if GroupIndex == k then
			v:GetComponent(typeof(CButton)).Selected = true
		else
			v:GetComponent(typeof(CButton)).Selected = false
		end
	end
end
function CLuaTeamGroupDetailWnd:RefreshButtons()
	local Pos = CTeamGroupMgr.Instance:GetPlayerPos(ClientMainPlayer.Inst.Id)
	local isManager = (Pos == EnumPosition.Leader or Pos == EnumPosition.Manager)
	local isLeader = (Pos == EnumPosition.Leader)
	self.ManagerInfoRoot:SetActive(isManager)
	self.PrepareConfirmBtn:SetActive(isLeader)
	self.CancelFollowBtn:SetActive(isLeader)
	self.AskFollowBtn:SetActive(isLeader)

end
function CLuaTeamGroupDetailWnd:DoTeamExchange(index1,index2)
	local startTeamID = 0
	local startTeam = CTeamGroupMgr.Instance:GetTeamGroupUnit(index1)
	if startTeam ~= nil then
		startTeamID = startTeam.TeamId
	end
	local targetTeamID=0
	local targetTeam = CTeamGroupMgr.Instance:GetTeamGroupUnit(index2)
	if targetTeam ~= nil then
		targetTeamID = targetTeam.TeamId
	end
	Gac2Gas.RequestExchangeTeamInTeamGroup(startTeamID,index1,targetTeamID,index2)
end
function CLuaTeamGroupDetailWnd:JudgeTabHit()
	local currentPos = UICamera.currentTouch.pos
	local ray = UICamera.currentCamera:ScreenPointToRay(Vector3(currentPos.x,currentPos.y,0))
	local hits = Physics.RaycastAll(ray, 10,  UICamera.currentCamera.cullingMask)
	for i =0,hits.Length -1 do
		if string.find(hits[i].collider.gameObject.name ,"TeamTabButton")~= nil then
			for k,v in pairs(TabTable) do
				if v == hits[i].collider.gameObject then
					if k~= GroupIndex then
						GroupIndex = k
						self:RefreshTeamShow()
						Gac2Gas.QueryTeamGroupTeamHpInfo(GroupIndex)
						return
					end
				end
			end
		end
	end
end
function CLuaTeamGroupDetailWnd:JudgeClickTarget()
	local currentPos = UICamera.currentTouch.pos
	local ray = UICamera.currentCamera:ScreenPointToRay(Vector3(currentPos.x,currentPos.y,0))
	local hits = Physics.RaycastAll(ray, 10,  UICamera.currentCamera.cullingMask)
	for i =0,hits.Length -1 do
		if string.find(hits[i].collider.gameObject.name ,"PlayerInfoPrefab")~= nil then
			local id,index = self:MemberGOToPlayerID(hits[i].collider.gameObject)
			return true,id,index
		end
	end
	for i =0,hits.Length -1 do
		if string.find(hits[i].collider.gameObject.name ,"TitleSprite")~= nil then
			return true,-1,self:TeamGOToIndex(hits[i].collider.gameObject.transform.parent.gameObject)
		end
	end
	return false,-1,-1
end
function CLuaTeamGroupDetailWnd:OnEditTeam()
	local pos = CTeamGroupMgr.Instance:GetPlayerPos(ClientMainPlayer.Inst.Id)
	if pos == EnumPosition.Leader or pos == EnumPosition.Manager then
		self.Editing = true
		self:RefreshTeamShow()
	else
		MessageMgr.Inst:ShowMessage("TEAMGROUP_NO_PERMISSION",{})
	end
end
function CLuaTeamGroupDetailWnd:RefreshTarget()
	local play = GamePlay_GamePlay.GetData(CTeamGroupMgr.Instance.selectedActivityId)
	local str = SafeStringFormat3(LocalString.GetString("%s  等级：%d-%d"),play.Name,CTeamGroupMgr.Instance.MinLevel,CTeamGroupMgr.Instance.MaxLevel)
	self.TargetLabel.text = str
end

function CLuaTeamGroupDetailWnd:ViewPlayer(index)
	local data = CTeamGroupMgr.Instance:GetMemberInfo(CLuaTeamGroupDetailWnd.focusPlayerID)
	CPlayerInfoMgr.ShowPlayerInfoWnd(CLuaTeamGroupDetailWnd.focusPlayerID, data.Name)
end
function CLuaTeamGroupDetailWnd:SendPlayerMsg(index)
	local data = CTeamGroupMgr.Instance:GetMemberInfo(CLuaTeamGroupDetailWnd.focusPlayerID)
	CChatHelper.ShowFriendWnd(CLuaTeamGroupDetailWnd.focusPlayerID, data.Name)
end
function CLuaTeamGroupDetailWnd:SetAsLeader(index)
	local confirm = function()
		Gac2Gas.RequestSetTeamGroupLeaderId(CLuaTeamGroupDetailWnd.focusPlayerID)
	end
	MessageWndManager.ShowOKCancelMessage(LocalString.GetString("确定转让团长吗?"),DelegateFactory.Action(confirm))

end
function CLuaTeamGroupDetailWnd:SetAsManager(index)
	local confirm = function()
		Gac2Gas.RequestSetTeamGroupManager(CLuaTeamGroupDetailWnd.focusPlayerID)
	end
	local info = CTeamGroupMgr.Instance:GetMemberInfo(CLuaTeamGroupDetailWnd.focusPlayerID)
	MessageWndManager.ShowOKCancelMessage(SafeStringFormat3(LocalString.GetString("确定任命%s为管理员吗?"),info.Name),DelegateFactory.Action(confirm))
end
function CLuaTeamGroupDetailWnd:KickPlayer(index)
	Gac2Gas.RequestKickTeamGroupMember(CLuaTeamGroupDetailWnd.focusPlayerID)
end

function CLuaTeamGroupDetailWnd:RemoveManager(index)
	local confirm = function()
		Gac2Gas.RequestCancelTeamGroupManager(CLuaTeamGroupDetailWnd.focusPlayerID)
	end
	local info = CTeamGroupMgr.Instance:GetMemberInfo(CLuaTeamGroupDetailWnd.focusPlayerID)
	MessageWndManager.ShowOKCancelMessage(SafeStringFormat3(LocalString.GetString("确定撤销%s的管理员权限吗?"),info.Name),DelegateFactory.Action(confirm))
end
function CLuaTeamGroupDetailWnd:SetAsTeamLeader(index)
	Gac2Gas.RequestSetTeamLeaderInGroup(CLuaTeamGroupDetailWnd.focusPlayerID)
end

local viewPlayer= nil
local sendMessage = nil
local setAsGroupLeader = nil
local setAsManager = nil
local removeAsManager= nil
local kickPlayer = nil
local setAsTeamLeader = nil

function CLuaTeamGroupDetailWnd:OnMemberSelect(go)
	if viewPlayer == nil then
		viewPlayer = PopupMenuItemData(LocalString.GetString("查看信息"),DelegateFactory.Action_int(CLuaTeamGroupDetailWnd.ViewPlayer),false,nil)
		sendMessage = PopupMenuItemData(LocalString.GetString("发送信息"),DelegateFactory.Action_int(CLuaTeamGroupDetailWnd.SendPlayerMsg),false,nil)
		setAsGroupLeader = PopupMenuItemData(LocalString.GetString("转让团长"),DelegateFactory.Action_int(CLuaTeamGroupDetailWnd.SetAsLeader),false,nil)
		setAsManager = PopupMenuItemData(LocalString.GetString("任命管理员"),DelegateFactory.Action_int(CLuaTeamGroupDetailWnd.SetAsManager),false,nil)
		removeAsManager= PopupMenuItemData(LocalString.GetString("撤销管理员"),DelegateFactory.Action_int(CLuaTeamGroupDetailWnd.RemoveManager),false,nil)
		kickPlayer = PopupMenuItemData(LocalString.GetString("请离个人"),DelegateFactory.Action_int(CLuaTeamGroupDetailWnd.KickPlayer),false,nil)
		setAsTeamLeader = PopupMenuItemData(LocalString.GetString("设为队长"),DelegateFactory.Action_int(CLuaTeamGroupDetailWnd.SetAsTeamLeader),false,nil)
	end
	if CLuaTeamGroupDetailWnd.focusPlayerID == ClientMainPlayer.Inst.Id or CLuaTeamGroupDetailWnd.focusPlayerID < 0 then
		return
	end

	local selfPos = CTeamGroupMgr.Instance:GetPlayerPos(ClientMainPlayer.Inst.Id)
	local targetPos = CTeamGroupMgr.Instance:GetPlayerPos(CLuaTeamGroupDetailWnd.focusPlayerID)
	local ot={}
	ot[1] = viewPlayer
	ot[2] = sendMessage
	if selfPos == EnumPosition.Leader then
		ot[3] = setAsGroupLeader
		if targetPos == EnumPosition.Manager then
			ot[4] = removeAsManager
		else
			ot[4] = setAsManager
		end
		ot[5] = kickPlayer
		if not CTeamGroupMgr.Instance:IsTeamLeader(CLuaTeamGroupDetailWnd.focusPlayerID) then
			ot[6] = setAsTeamLeader
		end
	elseif selfPos == EnumPosition.Manager then
		if CTeamGroupMgr.Instance:IsTeamLeader(CLuaTeamGroupDetailWnd.focusPlayerID) then
			ot[3] = kickPlayer
		else
			if targetPos == EnumPosition.Manager then
				ot[3] = setAsTeamLeader
			else
				ot[3] = setAsTeamLeader
				ot[4] = kickPlayer
			end
		end

	end

	-- 城战管理
	if CGuildMgr.Inst.CanCityBuild and CCityWarMgr.Inst:IsInCityWarScene() then
		CLuaCityWarSoldierWnd.FistPlayerId = CLuaTeamGroupDetailWnd.focusPlayerID
		local cityWarManageAction = PopupMenuItemData(LocalString.GetString("城战管理"),DelegateFactory.Action_int(function (...)
			CUIManager.ShowUI(CLuaUIResources.CityWarSoldierWnd)
		end),false,nil,EnumPopupMenuItemStyle.Orange)
		table.insert(ot, cityWarManageAction)
	end

	CPopupMenuInfoMgr.ShowPopupMenu(Table2Array(ot, Array_p),go.transform,AlignType.Right,1,nil,600,true,296)
end

return CLuaTeamGroupDetailWnd
