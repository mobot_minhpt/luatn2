local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local Color = import "UnityEngine.Color"
local DelegateFactory = import "DelegateFactory"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Constants = import "L10.Game.Constants"
local UISprite = import "UISprite"

LuaCakeCuttingFightInfoWnd = class()
RegistChildComponent(LuaCakeCuttingFightInfoWnd,"closeBtn", GameObject)
RegistChildComponent(LuaCakeCuttingFightInfoWnd,"selfInfo", GameObject)
RegistChildComponent(LuaCakeCuttingFightInfoWnd,"templateNode", GameObject)
RegistChildComponent(LuaCakeCuttingFightInfoWnd,"table", UITable)
RegistChildComponent(LuaCakeCuttingFightInfoWnd,"scrollView", UIScrollView)

RegistClassMember(LuaCakeCuttingFightInfoWnd, "colorIndexTable")

function LuaCakeCuttingFightInfoWnd:Close()
	CUIManager.CloseUI(CLuaUIResources.CakeCuttingFightInfoWnd)
end

function LuaCakeCuttingFightInfoWnd:Split(str, pat)
   local t = {}  -- NOTE: use {n = 0} in Lua-5.0
   local fpat = "(.-)" .. pat
   local last_end = 1
   local s, e, cap = str:find(fpat, 1)
   while s do
      if s ~= 1 or cap ~= "" then
         table.insert(t,cap)
      end
      last_end = e+1
      s, e, cap = str:find(fpat, last_end)
   end
   if last_end <= #str then
      cap = str:sub(last_end)
      table.insert(t, cap)
   end
   return t
end

function LuaCakeCuttingFightInfoWnd:OnEnable()
	--g_ScriptEvent:AddListener("LotteryAddSucc", self, "AddLotterySucc")
end

function LuaCakeCuttingFightInfoWnd:OnDisable()
	--g_ScriptEvent:RemoveListener("LotteryAddSucc", self, "AddLotterySucc")
end

function LuaCakeCuttingFightInfoWnd:InitNode(node,playerInfo,rank,gridNum,maxGridNum)
	local maxWidth = 234
	local totalPixelNum = LuaCakeCuttingMgr.MapHeight * LuaCakeCuttingMgr.MapWidth

	local bg1 = node.transform:Find('bg1')
	local bg2 = node.transform:Find('bg2')
	if bg1 and bg2 then
		if rank % 2 == 0 then
			bg1.gameObject:SetActive(true)
			bg2.gameObject:SetActive(false)
		else
			bg1.gameObject:SetActive(false)
			bg2.gameObject:SetActive(true)
		end
	end

	node.transform:Find('name'):GetComponent(typeof(UILabel)).text = playerInfo.Name
	--playerInfo.KillNum
	node.transform:Find('info/num'):GetComponent(typeof(UILabel)).text = SafeStringFormat3('%.2f%%', gridNum * 100 / totalPixelNum)
	node.transform:Find('num'):GetComponent(typeof(UILabel)).text = playerInfo.KillNum
	node.transform:Find('info/percent'):GetComponent(typeof(UISprite)).width = gridNum / maxGridNum * maxWidth
	if self.colorIndexTable[playerInfo.ColorIdx] then
		node.transform:Find('info/percent'):GetComponent(typeof(UISprite)).color = self.colorIndexTable[playerInfo.ColorIdx]
	end
	if rank > 3 then
		node.transform:Find('rank'):GetComponent(typeof(UILabel)).text = rank
	else
		local rankSprite = {Constants.RankFirstSpriteName,Constants.RankSecondSpriteName,Constants.RankThirdSpriteName}
		node.transform:Find('rank'):GetComponent(typeof(UILabel)).text = ''
		node.transform:Find('rank/spe').gameObject:SetActive(true)
		node.transform:Find('rank/spe'):GetComponent(typeof(UISprite)).spriteName = rankSprite[rank]
	end
end

function LuaCakeCuttingFightInfoWnd:Init()
	local onCloseClick = function(go)
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.closeBtn,DelegateFactory.Action_GameObject(onCloseClick),false)

	self.colorIndexTable = {}
	for i = 1,10 do
		local colorString = ZhouNianQing_CakeCuttingColor.GetData(i).Color
		local colorTable = self:Split(colorString,',')
		self.colorIndexTable[i] = Color(colorTable[1]/255,colorTable[2]/255,colorTable[3]/255)
	end

	self.templateNode:SetActive(false)

	if not LuaCakeCuttingMgr.PlayerFightInfoTable then
		local maxValue = 1
		if LuaCakeCuttingMgr.PlayerRankTable and #LuaCakeCuttingMgr.PlayerRankTable > 0 then
			maxValue = LuaCakeCuttingMgr.PlayerRankTable[1].value
			for i,v in ipairs(LuaCakeCuttingMgr.PlayerRankTable) do
				local node = NGUITools.AddChild(self.table.gameObject, self.templateNode)
				node:SetActive(true)
				local playerInfo = LuaCakeCuttingMgr.PlayerInfo[v.playerId]
				self:InitNode(node,playerInfo,i,v.value,maxValue)
				if v.playerId == CClientMainPlayer.Inst.Id then
					self:InitNode(self.selfInfo,playerInfo,i,v.value,maxValue)
				end
			end
		end
	else
		local maxValue = 1
		maxValue = LuaCakeCuttingMgr.PlayerFightInfoTable[1].Area

		for i,v in ipairs(LuaCakeCuttingMgr.PlayerFightInfoTable) do
			local playerInfo = v
			if playerInfo.Rank > 0 then
				local node = NGUITools.AddChild(self.table.gameObject, self.templateNode)
				node:SetActive(true)
				self:InitNode(node,playerInfo,i,v.Area,maxValue)
			end
			if v.PlayerId == CClientMainPlayer.Inst.Id then
				self:InitNode(self.selfInfo,playerInfo,i,v.Area,maxValue)
			end
		end

		LuaCakeCuttingMgr.PlayerFightInfoTable = nil
	end

  self.table:Reposition()
  self.scrollView:ResetPosition()

end

return LuaCakeCuttingFightInfoWnd
