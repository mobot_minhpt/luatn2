local MessageWndManager = import "L10.UI.MessageWndManager"
local CMessageBoxPage = import "L10.UI.CMessageBoxPage"
local CChatLinkMgr = import "CChatLinkMgr"

LuaMessageBoxTimeLimitWndMgr = {}
LuaMessageBoxTimeLimitWndMgr.pagesMaxCount = 1

--自动关闭的模态窗口，窗口允许多个，有优先级
LuaMessageBoxTimeLimitWnd = class()
RegistChildComponent(LuaMessageBoxTimeLimitWnd,"m_PageTemplate","PageTemplate", GameObject)
RegistChildComponent(LuaMessageBoxTimeLimitWnd,"m_Grid","Grid", UIGrid)

RegistClassMember(LuaMessageBoxTimeLimitWnd, "m_Pages")
RegistClassMember(LuaMessageBoxTimeLimitWnd, "m_closePageTick")
RegistClassMember(LuaMessageBoxTimeLimitWnd, "m_ClosePageTime")

function LuaMessageBoxTimeLimitWnd:Awake()
    self.m_Pages = {}
    self.m_PageTemplate:SetActive(false)
end

function LuaMessageBoxTimeLimitWnd:OnDisable()
    self:CancelClosePageTick()
end

function LuaMessageBoxTimeLimitWnd:Init()
    local info = MessageWndManager.MessageBoxInfoWithTimelimitAndPriority

    for i = 1, #self.m_Pages do
        if (self.m_Pages[i].page.messageId == info.id) then
            return
        end
    end

    self:RebuildPages(info)
end

function LuaMessageBoxTimeLimitWnd:CancelClosePageTick()
    if self.m_closePageTick then
        UnRegisterTick(self.m_closePageTick)
        self.m_closePageTick = nil
    end
end

function LuaMessageBoxTimeLimitWnd:RebuildPages(info)

    --相同文字的直接删掉旧的
    for i = #self.m_Pages, 1, -1 do
        if (self.m_Pages[i].page.textLabel.text == CChatLinkMgr.TranslateToNGUIText(info.text)) then
            local page = self.m_Pages[i].page
            local go = page.gameObject
            go.transform.parent = nil
            GameObject.Destroy(go)
            table.remove(self.m_Pages, i)
        end
    end

    --限制消息提示窗的数量
    local tempPages = {}
    local removePagesCount = #self.m_Pages - LuaMessageBoxTimeLimitWndMgr.pagesMaxCount + 1
    for i = 1, #self.m_Pages do
        local pageTable = self.m_Pages[i]
        if i > removePagesCount then
            table.insert(tempPages, pageTable)
        else
            local go = pageTable.page.gameObject
            go.transform.parent = nil
            GameObject.Destroy(go)
        end
    end
    self.m_Pages = tempPages

    self:AddNewPage(info)
    self:UpdateClosePageTick()
    self:SortPages()
end

function LuaMessageBoxTimeLimitWnd:AddNewPage(info)
    local go = NGUITools.AddChild(self.m_Grid.gameObject, self.m_PageTemplate)
    go:SetActive(true)
    local page = go:GetComponent(typeof(CMessageBoxPage))
    if page then
        local t = {page = page, priority = info.priority, closeTime = os.time() + info.aliveDuration}
        page:Init(info)
        page.onClosePage = DelegateFactory.Action_CMessageBoxPage(function(page)
            self:OnPageClose(page)
        end)
        table.insert(self.m_Pages, t)
    end
    self.m_Grid:Reposition()
end

function LuaMessageBoxTimeLimitWnd:SortPages()
    table.sort(self.m_Pages, function(a, b)
        return a.priority < b.priority
    end)
    for i = 1, #self.m_Pages do
        self.m_Pages[i].page:ResetDepth(i)
    end
end

function LuaMessageBoxTimeLimitWnd:UpdateClosePageTick()
    self:CancelClosePageTick()
    if #self.m_Pages == 0 then
        return
    end
    self.m_ClosePageTime = uint.MaxValue
    local closePage
    for i = #self.m_Pages, 1 do
        if self.m_Pages[i].closeTime < self.m_ClosePageTime then
            self.m_ClosePageTime = self.m_Pages[i].closeTime
            closePage = self.m_Pages[i].page
        end
    end
    self.m_closePageTick = RegisterTickOnce(function ()
        self:OnPageClose(closePage)
    end, (self.m_ClosePageTime - os.time()) * 1000 )
end

function LuaMessageBoxTimeLimitWnd:OnPageClose(page)
    for i = #self.m_Pages, 1, -1 do
        if self.m_Pages[i].page == page then
            table.remove(self.m_Pages, i)
        end
    end
    self:UpdateClosePageTick()
    local go = page.gameObject
    go.transform.parent = nil
    GameObject.Destroy(go)
    if #self.m_Pages == 0 then CUIManager.CloseUI(CLuaUIResources.MessageBoxTimeLimitWnd) end
end