require("common/common_include")

local CBaseWnd = import "L10.UI.CBaseWnd"
local DelegateFactory = import "DelegateFactory"
local CommonDefs = import "L10.Game.CommonDefs"
local LocalString = import "LocalString"
local CUICommonDef = import "L10.UI.CUICommonDef"
local UIScrollView = import "UIScrollView"
local UITable = import "UITable"
local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local UISprite = import "UISprite"
local CButton = import "L10.UI.CButton"
local CChatHelper = import "L10.UI.CChatHelper"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
local CPlayerInfoMgrAlignType = import "CPlayerInfoMgr+AlignType"

LuaFriendStatusWnd = class()
RegistClassMember(LuaFriendStatusWnd,"m_closeBtn")
RegistClassMember(LuaFriendStatusWnd,"m_ScrollView")
RegistClassMember(LuaFriendStatusWnd,"m_Table")
RegistClassMember(LuaFriendStatusWnd,"m_ItemTemplate")

function LuaFriendStatusWnd:Awake()
    
end

function LuaFriendStatusWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaFriendStatusWnd:Init()
	self.m_closeBtn = self.transform:Find("Wnd_Bg_Secondary_1/CloseButton").gameObject
	CommonDefs.AddOnClickListener(self.m_closeBtn, DelegateFactory.Action_GameObject(function(go) self:Close() end), false)
	
	self.m_ScrollView = self.transform:Find("Anchor/ScrollView"):GetComponent(typeof(UIScrollView))
	self.m_Table = self.transform:Find("Anchor/ScrollView/Table"):GetComponent(typeof(UITable))
	self.m_ItemTemplate = self.transform:Find("Anchor/ScrollView/Pool/Item").gameObject

	self.m_ItemTemplate:SetActive(false)
	CUICommonDef.ClearTransform(self.m_Table.transform)

	Gac2Gas.QueryCloseFriendInfo()
end

function LuaFriendStatusWnd:OnEnable()
	g_ScriptEvent:AddListener("OnReceiveCloseFriendInfoResult", self, "OnReceiveCloseFriendInfoResult")
	g_ScriptEvent:AddListener("MainPlayerRelationship_AddBlack", self, "MainPlayerRelationship_AddBlack")

end

function LuaFriendStatusWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnReceiveCloseFriendInfoResult", self, "OnReceiveCloseFriendInfoResult")
	g_ScriptEvent:RemoveListener("MainPlayerRelationship_AddBlack", self, "MainPlayerRelationship_AddBlack")
end


function LuaFriendStatusWnd:OnReceiveCloseFriendInfoResult(dataTbl)
	-- playerId, displayName, gender, class, level, xianfanStatus, isOnline, hours, serverName
	if not dataTbl then return end
	CUICommonDef.ClearTransform(self.m_Table.transform)
	for _,data in pairs(dataTbl) do
		local go = CUICommonDef.AddChild(self.m_Table.gameObject, self.m_ItemTemplate)
		go:SetActive(true)

		local portrait = go.transform:Find("Border/Portrait"):GetComponent(typeof(CUITexture))
		local portraitBorder = go.transform:Find("Border").gameObject
		local levelLabel = go.transform:Find("LevelLabel"):GetComponent(typeof(UILabel))
		local nameLabel = go.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
		local onlineStatusSprite = go.transform:Find("InfoLabel/Sprite"):GetComponent(typeof(UISprite))
		local infoLabel = go.transform:Find("InfoLabel"):GetComponent(typeof(UILabel))
		local button = go.transform:Find("OpButton"):GetComponent(typeof(CButton))

		portrait:LoadPortrait(CUICommonDef.GetPortraitName(data.class, data.gender, -1), false)
		levelLabel.text = CUICommonDef.GetColoredLevelString(data.xianfanStatus > 0, data.level, levelLabel.color)
		nameLabel.text = data.displayName
		if data.isOnline then
			CUICommonDef.SetActive(portrait.gameObject, true, false)
			onlineStatusSprite.spriteName = "loginwnd_severstate_smooth"
			local str = ""
			if data.serverName and data.serverName~= "" and (not data.isSameServer) then
				str = SafeStringFormat3(LocalString.GetString("活跃在%s "), data.serverName)
			end
			if data.hours and data.hours > 0 then
				if data.hours > 24 then
					str = str..SafeStringFormat3(LocalString.GetString("在线%d天"), math.ceil(data.hours/24))
				else
					str = str..SafeStringFormat3(LocalString.GetString("在线%d小时"), data.hours)
				end
			else
				str = str..LocalString.GetString("在线")
			end
			infoLabel.text = str
			
		else
			CUICommonDef.SetActive(portrait.gameObject, false, false)
			onlineStatusSprite.spriteName = "loginwnd_severstate_uphold"
			local str = ""
			if data.serverName and data.serverName~= "" and (not data.isSameServer) then
				str = SafeStringFormat3(LocalString.GetString("活跃在%s "), data.serverName)
			end
			if data.hours and data.hours > 0 then
				if data.hours > 24 then
					str = str..SafeStringFormat3(LocalString.GetString("离线%d天"), math.ceil(data.hours/24))
				else
					str = str..SafeStringFormat3(LocalString.GetString("离线%d小时"), data.hours)
				end
			else
				str = str..LocalString.GetString("离线")
			end
			infoLabel.text = str
		end

		local needInvite = false
		if data.isSameServer then
			if data.isOnline then
				needInvite = false
				button.Text = LocalString.GetString("发送消息")
			else
				needInvite = true
				button.Text = LocalString.GetString("邀请上线")
			end
		else
			if data.sourcePlayerIsOnline then
				needInvite = false
				button.Text = LocalString.GetString("发送消息")
			else
				needInvite = true
				button.Text = LocalString.GetString("邀请上线")
			end
		end

		CommonDefs.AddOnClickListener(button.gameObject, DelegateFactory.Action_GameObject(function(go) self:DoChat(data, needInvite) end), false)
		CommonDefs.AddOnClickListener(portraitBorder, DelegateFactory.Action_GameObject(function(go) self:OnPlayerClick(data.playerId) end), false)
	end

	self.m_Table:Reposition()
	self.m_ScrollView:ResetPosition()
end

function LuaFriendStatusWnd:MainPlayerRelationship_AddBlack(args)
	local playerId = args[0]
	if not playerId or playerId == 0 then return end
	--加黑名单做个刷新操作
	Gac2Gas.QueryCloseFriendInfo()
end

function LuaFriendStatusWnd:DoChat(data, needInvite)
	if needInvite then
		--本服离线玩家或跨服玩家 邀请上线
		local infoStr = SafeStringFormat3("PlayerId=%s,SourcePlayerId=%s,PlayerName=%s,Gender=%s,Class=%s,Level=%s,ServerName=%s", 
										data.playerId, data.sourcePlayerId or "", data.name, data.gender, data.class, data.level, data.serverName)
		Gac2Gas.InviteCloseFriendHuiLiu(data.playerId, infoStr)
	else
		--本服在线玩家 发送消息
		CChatHelper.ShowFriendWnd(data.playerId, data.name)
		self:Close()
	end
end

function LuaFriendStatusWnd:OnPlayerClick(playerId)
	CPlayerInfoMgr.ShowPlayerPopupMenu(playerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), CPlayerInfoMgrAlignType.Default)
end

return LuaFriendStatusWnd