local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"

LuaSanxingTipWnd = class()
RegistChildComponent(LuaSanxingTipWnd,"closeBtn", GameObject)
RegistChildComponent(LuaSanxingTipWnd,"pha1Btn", GameObject)
RegistChildComponent(LuaSanxingTipWnd,"pha2Btn", GameObject)
RegistChildComponent(LuaSanxingTipWnd,"pha3Btn", GameObject)
RegistChildComponent(LuaSanxingTipWnd,"pha1Node", GameObject)
RegistChildComponent(LuaSanxingTipWnd,"pha2Node", GameObject)
RegistChildComponent(LuaSanxingTipWnd,"pha3Node", GameObject)
RegistChildComponent(LuaSanxingTipWnd,"guideNode", GameObject)

RegistClassMember(LuaSanxingTipWnd, "lightNode")

function LuaSanxingTipWnd:Close()
	CUIManager.CloseUI(CLuaUIResources.SanxingTipWnd)
end

function LuaSanxingTipWnd:OnEnable()
	--g_ScriptEvent:AddListener("", self, "")
end

function LuaSanxingTipWnd:OnDisable()
	--g_ScriptEvent:RemoveListener("", self, "")
end

function LuaSanxingTipWnd:Init()
	if LuaSanxingGamePlayMgr.InGuidePha1 then
		CLuaGuideMgr.TryTriggerSanxingGuide3()
	elseif LuaSanxingGamePlayMgr.InGuidePha2 then
		CLuaGuideMgr.TryTriggerSanxingGuide4()
	end

	local onCloseClick = function(go)
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.closeBtn,DelegateFactory.Action_GameObject(onCloseClick),false)

  self.lightNode = nil

  local phaBtnTable = {self.pha1Btn,self.pha2Btn,self.pha3Btn}
  local phaNodeTable = {self.pha1Node,self.pha2Node,self.pha3Node}
	local splits = g_LuaUtil:StrSplit(SanXingBattle_Setting.GetData().FlagItem_Info,";")
	local splits1 = g_LuaUtil:StrSplit(splits[1],",")
	local splits2 = g_LuaUtil:StrSplit(splits[2],",")
	self.pha1Node.transform:Find('tipText'):GetComponent(typeof(UILabel)).text = g_MessageMgr:FormatMessage('SanxingTip1',
	SanXingBattle_Setting.GetData().Trigger_Pve_Score)
	self.pha2Node.transform:Find('tipText'):GetComponent(typeof(UILabel)).text = g_MessageMgr:FormatMessage('SanxingTip2')
	self.pha3Node.transform:Find('tipText'):GetComponent(typeof(UILabel)).text = g_MessageMgr:FormatMessage('SanxingTip3',
	SanXingBattle_Setting.GetData().JinSha_Win_Score,
	splits1[7],
	splits2[7]
	)

  for i,v in ipairs(phaBtnTable) do
    v.transform:Find('highlight').gameObject:SetActive(false)
		local index = i
    local onBtnClick = function(go)
      self:ShowPanel(index)
    end
    CommonDefs.AddOnClickListener(v,DelegateFactory.Action_GameObject(onBtnClick),false)
  end
  for i,v in ipairs(phaNodeTable) do
    v:SetActive(false)
  end
	self:ShowPanel(1)
end

function LuaSanxingTipWnd:ShowPanel(index)
  local phaBtnTable = {self.pha1Btn,self.pha2Btn,self.pha3Btn}
  local phaNodeTable = {self.pha1Node,self.pha2Node,self.pha3Node}
  for i,v in ipairs(phaBtnTable) do
    if i == index then
			if self.lightNode then
				self.lightNode:SetActive(false)
			end
      self.lightNode = v.transform:Find('highlight').gameObject
      self.lightNode:SetActive(true)
    end
  end
  for i,v in ipairs(phaNodeTable) do
    if i == index then
      v:SetActive(true)
    else
      v:SetActive(false)
    end
  end
end


return LuaSanxingTipWnd
