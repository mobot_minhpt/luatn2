-- Auto Generated!!
local BabyMgr = import "L10.Game.BabyMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CClientOtherPlayer = import "L10.Game.CClientOtherPlayer"
local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"
local CDuoHunMgr = import "L10.Game.CDuoHunMgr"
local CFamilyTreeMgr = import "L10.Game.CFamilyTreeMgr"
local CFightingSpiritMgr = import "L10.Game.CFightingSpiritMgr"
-- local CGuildJobChangeModel = import "L10.UI.CGuildJobChangeModel"
local CGuildMgr = import "L10.Game.CGuildMgr"
local CIMMgr = import "L10.Game.CIMMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"
local CScene = import "L10.Game.CScene"
local CTeamGroupMgr = import "L10.Game.CTeamGroupMgr"
local CTeamMgr = import "L10.Game.CTeamMgr"
local CTradeMessageBox = import "L10.UI.CTradeMessageBox"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local CWeddingMgr = import "L10.Game.CWeddingMgr"
local CZuoQiMgr = import "L10.Game.CZuoQiMgr"
local DelegateFactory = import "DelegateFactory"
local DuoHun_Setting = import "L10.Game.DuoHun_Setting"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
--local Gac2Gas = import "L10.Game.Gac2Gas"
local GameSetting_Common = import "L10.Game.GameSetting_Common"
local GuildDefine = import "L10.Game.GuildDefine"
local L10 = import "L10"
local Mall_LingYuMall = import "L10.Game.Mall_LingYuMall"
local MessageMgr = import "L10.Game.MessageMgr"
local MessageWndManager = import "L10.UI.MessageWndManager"
local PopupMenuItemData = import "CPlayerInfoMgr+PopupMenuItemData"
local PopupMenuItemData2 = import "L10.UI.PopupMenuItemData"
local PublicMap_PublicMap = import "L10.Game.PublicMap_PublicMap"
local QnMoneyCostMesssageMgr = import "L10.UI.QnMoneyCostMesssageMgr"
local System = import "System"
local UInt64 = import "System.UInt64"
local Vector2 = import "UnityEngine.Vector2"
local Vector3 = import "UnityEngine.Vector3"
local ZuoQi_ZuoQi = import "L10.Game.ZuoQi_ZuoQi"
local EnumMenuItemStyle = import "CPlayerInfoMgr+EnumMenuItemStyle"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CCommonSendFlowerMgr = import "L10.UI.CCommonSendFlowerMgr"
local EnumSendFlowerType = import "L10.UI.CCommonSendFlowerMgr+EnumSendFlowerType"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local CPopupMenuInfoMgrAlignType = import  "L10.UI.CPopupMenuInfoMgr+AlignType"
local CChatHelper = import "L10.UI.CChatHelper"
local Constants = import "L10.Game.Constants"
local Object = import "System.Object"
local CChatMgr = import "L10.Game.CChatMgr"

CPlayerInfoMgr.m_ShowTeamMemberPopupMenu_CS2LuaHook = function (memberId, anchorTrans, alignType)
    local info = CPlayerInfoMgr.info
    info.memberId = memberId
    info.anchorTrans = anchorTrans
    info.alignType = alignType
    Gac2Gas.QueryInteractMenuData(memberId, EnumPlayerInfoContext_lua.Team)
end
CPlayerInfoMgr.m_ShowTeamMemberPopupMenuAfterQuery_CS2LuaHook = function (this, memberId, online, serverGroupId)
    if CPlayerInfoMgr.info.memberId ~= memberId or not online then
        return
    end
    local info = CPlayerInfoMgr.info
    CTeamMgr.Inst:ShowTeamMemberPopupMenu(info.memberId, info.anchorTrans, info.alignType, serverGroupId)
end
CPlayerInfoMgr.m_ShowPlayerPopupMenuFromPersonalSpace_CS2LuaHook = function (playerId, context, channel, reportMsg, nearestMsgs, worldPos, alignType)

    --主角不在线或者点击了主角自身时，不弹出交互菜单
    if CClientMainPlayer.Inst == nil or CClientMainPlayer.Inst.Id == playerId or playerId == 0 or CIMMgr.Inst:IsSpecialPlayerID(playerId) then
        return
    end

    CPersonalSpaceMgr.Inst:GetUserProfile(playerId, DelegateFactory.Action_CPersonalSpace_UserProfile_Ret(function (ret)
        if ret.code ~= 0 or CClientMainPlayer.Inst == nil then
            return
        end
        if ret.data.server_id == CClientMainPlayer.Inst:GetMyServerId() then
            CPlayerInfoMgr.ShowPlayerPopupMenu(playerId, context, channel, reportMsg, nearestMsgs, worldPos, alignType)
            return
		elseif g_PopupMenuMgr.m_NeedQueryCrossServerFriendPopupMenu then
			g_PopupMenuMgr:SetCrossServerFriendInfo(playerId, worldPos, alignType,
                ret.data.rolename,
                ret.data.grade,
                ret.data.xianfanstatus,
                ret.data.clazz,
                ret.data.gender,
                ret.data.server_name,
                ret.data.guild.name,
                ret.data.signature,
                ret.data.expression_base.expression,
                ret.data.expression_base.expression_txt,
                ret.data.expression_base.frame,
                ret.data.lianghao.id,
                ret.data.lianghao.hideicon,
                ret.data.lianghao.expiredtime)
            CPlayerInfoMgr.ShowPlayerPopupMenu(playerId, EnumPlayerInfoContext.CrossServerFriend, channel, reportMsg, nearestMsgs, worldPos, alignType)
			return
        else
            -- 基本信息
            g_PopupMenuMgr:ShowCrossServerFriendPopupMenu(playerId, worldPos, alignType,
                ret.data.rolename,
                ret.data.grade,
                ret.data.xianfanstatus,
                ret.data.clazz,
                ret.data.gender,
                ret.data.server_name,
                ret.data.guild.name,
                ret.data.signature,
                ret.data.expression_base.expression,
                ret.data.expression_base.expression_txt,
                ret.data.expression_base.frame,
                ret.data.lianghao.id,
                ret.data.lianghao.hideicon,
                ret.data.lianghao.expiredtime)
        end
    end), nil)
end

CPlayerInfoMgr.m_CheckCanLookAtInfo_CS2LuaHook = function (playerId)
    if CClientPlayerMgr.Inst ~= nil and CClientMainPlayer.Inst.IsCrossServerPlayer then
        local obj = CClientPlayerMgr.Inst:GetPlayer(playerId)
        if (TypeIs(obj, typeof(CClientOtherPlayer))) and ((TypeAs(obj, typeof(CClientOtherPlayer))).ServerId ~= CClientMainPlayer.Inst:GetMyServerId()) then
            g_MessageMgr:ShowMessage("CROSS_SERVER_NOT_USE_THE_FEATURE")
            return false
        end
    end
    return true
end

CPlayerInfoMgr.m_OnQueryIsPlayerGhostReturn_CS2LuaHook = function (playerId, isGhost)
    if playerId ~= CDuoHunMgr.Inst.queryPlayerId then
        return
    end
    CDuoHunMgr.Inst.isQueryPlayerGhostInFoeSelectWnd = true
    if isGhost then
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("ALREADY_HUNFEIPOSAN"), DelegateFactory.Action(function ()
            CPlayerInfoMgr.OnConfirmSetDuoHunFan(playerId)
        end), nil, nil, nil, false)
    else
        CPlayerInfoMgr.OnConfirmSetDuoHunFan(playerId)
    end
end
CPlayerInfoMgr.m_OnConfirmSetDuoHunFan_CS2LuaHook = function (playerId)
    local hasDuoHunFan = false
    local bagSize = CClientMainPlayer.Inst.ItemProp:GetPlaceSize(EnumItemPlace.Bag)
    do
        local i = 1
        while i <= bagSize do
            local id = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Bag, i)
            if not System.String.IsNullOrEmpty(id) then
                local itemcell = CItemMgr.Inst:GetById(id)
                if itemcell ~= nil and itemcell.TemplateId == DuoHun_Setting.GetData().DuoHunItemId then
                    local targetId = itemcell.Item:GetDuoHunPlayerId()
                    if targetId == 0 then
                        Gac2Gas.SetDuoHunFan(EnumItemPlace_lua.Bag, i, playerId)
                        hasDuoHunFan = true
                        break
                    end
                end
            end
            i = i + 1
        end
    end
    if not hasDuoHunFan then
        local item = Mall_LingYuMall.GetData(DuoHun_Setting.GetData().DuoHunItemId)
        if item == nil then
            return
        end
        QnMoneyCostMesssageMgr.ShowMoneyCostMessageBox(EnumMoneyType.LingYu, g_MessageMgr:FormatMessage("BUY_DUOHUNFAN"), item.Jade, DelegateFactory.Action(function ()
            Gac2Gas.BuyAndSetDuoHunFan(playerId)
        end), nil, false, true, EnumPlayScoreKey.NONE, false)
    end
end

CPlayerInfoMgr.m_ShowPlayerPopupMenuAfterQuery_CS2LuaHook = function (this, playerId, level, portraitName, expressionTxt, profileInfo, displayName, xiuweiGrade, context, online, serverGroupId, isInXianShenStatus, isSuccess)

    if not isSuccess then
        if context == EnumPlayerInfoContext.MSKK then
            g_MessageMgr:ShowMessage("MSKK_ACCOUNT_NOT_VALID")
            return
        end
    end

    if context == EnumPlayerInfoContext.ChatRoom then
        LuaPlayerInfoMgr:ShowChatRoomPopupMenu(playerId, level, portraitName, expressionTxt, profileInfo, displayName, xiuweiGrade, context, online, serverGroupId, isInXianShenStatus, isSuccess)
        return
    end

	if context == EnumPlayerInfoContext.CrossServerFriend then
		g_PopupMenuMgr:ShowCrossServerFriendPopupMenuAfterQuery(playerId, level, portraitName, expressionTxt, profileInfo, displayName, xiuweiGrade, context, online, serverGroupId, isInXianShenStatus, isSuccess)
		return
	end
   
    -- 据点战特殊处理 会弹出自身的交互菜单
    if not LuaJuDianBattleMgr.NeedShowPlayerJuDianInfo then
        --主角不在线或者点击了主角自身时，不弹出交互菜单
        if CClientMainPlayer.Inst == nil or CClientMainPlayer.Inst.Id == playerId or playerId == 0 or CIMMgr.Inst:IsSpecialPlayerID(playerId) then
            return
        end
    elseif CClientMainPlayer.Inst == nil then
        return
    end

    local isCrossServerPlayer = (CClientMainPlayer.Inst:GetMyServerId() ~= serverGroupId)

    local actionPairs = CreateFromClass(MakeGenericClass(List, PopupMenuItemData))

    local obj = CClientPlayerMgr.Inst:GetPlayer(playerId)
    local otherPlayer = nil
    if obj and TypeIs(obj, typeof(CClientOtherPlayer)) then
        otherPlayer = TypeAs(obj, typeof(CClientOtherPlayer))
    end
    --注意！新加的action没有特殊需求一律往后放
    if isSuccess and LuaChatMgr.PlayerInteractionPermitted(isCrossServerPlayer, CPlayerInfoMgr.PopupMenuInfo) then
        CommonDefs.ListAdd_LuaCall(actionPairs, LuaPlayerInfoMgr:GetMenuItem_SendMsg(playerId, displayName))
    end
	if isSuccess then
		CommonDefs.ListAdd_LuaCall(actionPairs, LuaPlayerInfoMgr:GetMenuItem_LookatInfo(playerId, displayName))
	end
    if isSuccess and online and LuaChatMgr.PlayerJoinTeamOrTeamGroupPermitted(isCrossServerPlayer, CPlayerInfoMgr.PopupMenuInfo) then
        CommonDefs.ListAdd_LuaCall(actionPairs, LuaPlayerInfoMgr:GetMenuItem_InviteToJoinTeam(playerId, displayName))
        CommonDefs.ListAdd_LuaCall(actionPairs, LuaPlayerInfoMgr:GetMenuItem_ApplyToJoinTeam(playerId, displayName))
        if CTeamGroupMgr.Instance:InTeamGroup() then
            CommonDefs.ListAdd_LuaCall(actionPairs, LuaPlayerInfoMgr:GetMenuItem_InviteToTeamGroup(playerId, displayName))
        end
        if otherPlayer == nil or otherPlayer.TeamGroupLeaderId ~= 0 then
            CommonDefs.ListAdd_LuaCall(actionPairs, LuaPlayerInfoMgr:GetMenuItem_ApplyToTeamGroup(playerId, displayName))
        end
    end

    repeat
        local default = context
        if default == EnumPlayerInfoContext.FriendInFriendWnd then
            CommonDefs.ListAdd_LuaCall(actionPairs, LuaPlayerInfoMgr:GetMenuItem_DelFriend(playerId, displayName))
            LuaPlayerInfoMgr:AppendQlcjAction(playerId, displayName, isCrossServerPlayer, actionPairs, isSuccess)
            break
        elseif default == EnumPlayerInfoContext.EnemyInFriendWnd then
            CommonDefs.ListAdd_LuaCall(actionPairs, LuaPlayerInfoMgr:GetMenuItem_DelEnemy(playerId, displayName))
            break
        elseif default == EnumPlayerInfoContext.RecentInFriendWnd then
            LuaPlayerInfoMgr:AppendAddOrDelFriendAction(playerId, displayName, actionPairs, isSuccess)
            CommonDefs.ListAdd_LuaCall(actionPairs, LuaPlayerInfoMgr:GetMenuItem_DelRecent(playerId, displayName))
            LuaPlayerInfoMgr:AppendQlcjAction(playerId, displayName, isCrossServerPlayer, actionPairs, isSuccess)
            break
        elseif default == EnumPlayerInfoContext.RecentInteractionInFriendWnd then
            LuaPlayerInfoMgr:AppendAddOrDelFriendAction(playerId, displayName, actionPairs, isSuccess)
            CommonDefs.ListAdd_LuaCall(actionPairs, LuaPlayerInfoMgr:GetMenuItem_DelRecentInteraction(playerId, displayName))
            LuaPlayerInfoMgr:AppendQlcjAction(playerId, displayName, isCrossServerPlayer, actionPairs, isSuccess)
            break
        elseif default == EnumPlayerInfoContext.ChatLink or default == EnumPlayerInfoContext.PlayerInChannel then
            if LuaChatMgr.PlayerInteractionPermitted(isCrossServerPlayer, CPlayerInfoMgr.PopupMenuInfo) then
                LuaPlayerInfoMgr:AppendAddOrDelFriendAction(playerId, displayName, actionPairs, isSuccess)
            end
            LuaPlayerInfoMgr:AppendQlcjAction(playerId, displayName, isCrossServerPlayer, actionPairs, isSuccess)
            break
        elseif default == EnumPlayerInfoContext.CurrentTarget then
            LuaPlayerInfoMgr:AppendAddOrDelFriendAction(playerId, displayName, actionPairs, isSuccess)
            if not isCrossServerPlayer then
                local sceneInfo = PublicMap_PublicMap.GetData(CScene.MainScene.SceneTemplateId)
                local allowPK = true
                if sceneInfo ~= nil then
                    allowPK = sceneInfo.AllowPK
                end

                if isSuccess and CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.BasicProp.Level >= 30 and level >= 30 and allowPK then
                    CommonDefs.ListAdd_LuaCall(actionPairs, LuaPlayerInfoMgr:GetMenuItem_RequestChallenge(playerId, displayName))
                end
				if isSuccess then
					CommonDefs.ListAdd_LuaCall(actionPairs, LuaPlayerInfoMgr:GetMenuItem_FtfTransaction(playerId, displayName))
				end
            end
			if isSuccess then
				CommonDefs.ListAdd_LuaCall(actionPairs, LuaPlayerInfoMgr:GetMenuItem_QieCuo(playerId, displayName))
			end
            if not isCrossServerPlayer then
                --如果是仇敌增加设置夺魂幡
                local isEnemy = false
                local enemies = CIMMgr.Inst.Enemies
                CommonDefs.EnumerableIterate(enemies, DelegateFactory.Action_object(function (___value)
                    local id = ___value
                    if id == playerId then
                        isEnemy = true
                    end
                end))
                if isSuccess and isEnemy then
                    CommonDefs.ListAdd_LuaCall(actionPairs, LuaPlayerInfoMgr:GetMenuItem_SetDuoHun(playerId, displayName))
                end
            end
            break
        elseif default == EnumPlayerInfoContext.RankList then
            LuaPlayerInfoMgr:AppendAddOrDelFriendAction(playerId, displayName, actionPairs, isSuccess)
            break
        elseif default == EnumPlayerInfoContext.LBS then
            LuaPlayerInfoMgr:AppendAddOrDelFriendAction(playerId, displayName, actionPairs, isSuccess)
            break
        elseif default == EnumPlayerInfoContext.GuildMember then
            LuaPlayerInfoMgr:AppendAddOrDelFriendAction(playerId, displayName, actionPairs, isSuccess)
            if not isCrossServerPlayer then
                if CGuildMgr.Inst:IsMeCoreMember() and CGuildMgr.Inst:IsBangZhu(playerId) then
                    CommonDefs.ListAdd_LuaCall(actionPairs, LuaPlayerInfoMgr:GetMenuItem_QuitGuild(playerId ,displayName))
                end
                LuaPlayerInfoMgr:AppendQlcjAction(playerId, displayName, isCrossServerPlayer, actionPairs, isSuccess)
            end
            break
        elseif default == EnumPlayerInfoContext.GuildApplyMember then
            LuaPlayerInfoMgr:AppendAddOrDelFriendAction(playerId, displayName, actionPairs, isSuccess)
            break
        elseif default == EnumPlayerInfoContext.MPTZ then
            LuaPlayerInfoMgr:AppendAddOrDelFriendAction(playerId, displayName, actionPairs, isSuccess)
            break
        elseif default == EnumPlayerInfoContext.FamilyTree then
            LuaPlayerInfoMgr:AppendAddOrDelFriendAction(playerId, displayName, actionPairs, isSuccess)
            if isSuccess and not isCrossServerPlayer then
                if CWeddingMgr.Inst:IsMyHusband(playerId) or CWeddingMgr.Inst:IsMyWife(playerId) then
                    CommonDefs.ListAdd_LuaCall(actionPairs, LuaPlayerInfoMgr:GetMenuItem_QlcjSkill(playerId, displayName))
                end
            end
            break
        elseif default == EnumPlayerInfoContext.DouhunSelfTeam then
            LuaPlayerInfoMgr:AppendAddOrDelFriendAction(playerId, displayName, actionPairs, isSuccess)
            if not isCrossServerPlayer then
                if CFightingSpiritMgr.Instance:IsLeader() then
                    CommonDefs.ListAdd_LuaCall(actionPairs, LuaPlayerInfoMgr:GetMenuItem_KickDouhunMember(playerId, displayName))
                end
                CommonDefs.ListAdd_LuaCall(actionPairs, LuaPlayerInfoMgr:GetMenuItem_LeaveDouhunTeam(playerId, displayName))
            end
            break
        elseif default == EnumPlayerInfoContext.DouhunTeam then
            LuaPlayerInfoMgr:AppendAddOrDelFriendAction(playerId, displayName, actionPairs, isSuccess)
            break
        elseif default == EnumPlayerInfoContext.QMPK then
            LuaPlayerInfoMgr:AppendAddOrDelFriendAction(playerId, displayName, actionPairs, isSuccess)
            break
        elseif default == EnumPlayerInfoContext.QMPKSelfTeam then
            LuaPlayerInfoMgr:AppendAddOrDelFriendAction(playerId, displayName, actionPairs, isSuccess)
            CommonDefs.ListAdd_LuaCall(actionPairs, LuaPlayerInfoMgr:GetMenuItem_QmpkKickMember(playerId, displayName))
            if isSuccess and CQuanMinPKMgr.Inst.m_IsTeamLeader then
                CommonDefs.ListAdd_LuaCall(actionPairs, LuaPlayerInfoMgr:GetMenuItem_QmpkTransferLeader(playerId, displayName))
            end
            break
        elseif default == EnumPlayerInfoContext.TeamGroup then
            if CTeamGroupMgr.Instance:GetPlayerPos(playerId) == CTeamGroupMgr.EnumTeamGroupMemberPosition.Manager and CTeamGroupMgr.Instance:GetPlayerPos(CClientMainPlayer.Inst.Id) == CTeamGroupMgr.EnumTeamGroupMemberPosition.Leader then
                CommonDefs.ListAdd_LuaCall(actionPairs, LuaPlayerInfoMgr:GetMenuItem_RemoveManager(playerId, displayName))
            end
            break
        elseif default == EnumPlayerInfoContext.XianZhi then
            if CLuaXianzhiMgr.CanBeXianZhiManaged(playerId) then
                local item = PopupMenuItemData(LocalString.GetString("仙职管理"), DelegateFactory.Action_ulong_string(function ( ... )
                    CLuaXianzhiMgr.OpenPlayerXianZhiManage(playerId)
                end), EnumMenuItemStyle.Orange, nil)
                CommonDefs.ListAdd_LuaCall(actionPairs, item)
            end
            break
        elseif default == EnumPlayerInfoContext.PersonalSpace then
            if isSuccess and not CIMMgr.Inst:IsMyFriend(playerId) then
                if profileInfo.LiangHaoId>0 and CServerTimeMgr.Inst.timeStamp<profileInfo.LiangHaoExpiredTime and CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.Id ~= playerId then
                    CommonDefs.ListAdd_LuaCall(actionPairs, LuaPlayerInfoMgr:GetMenuItem_AddFriend(playerId, displayName))
                end
            end
            break
        elseif default == EnumPlayerInfoContext.StarBiwu then
            LuaPlayerInfoMgr:AppendAddOrDelFriendAction(playerId, displayName, actionPairs, isSuccess)
            if CLuaStarBiwuMgr.m_IsTeamLeader then
              local kickPlayer = PopupMenuItemData(LocalString.GetString("请离队员"), DelegateFactory.Action_ulong_string(function ( playerId, playerName )
                local msg = MessageMgr.Inst:FormatMessage("Qmpk_KickoffMember", playerName)
                MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function()
                  Gac2Gas.RequestRemoveStarBiwuZhanDuiMember(playerId)
                end), nil, nil, nil, false)
              end), EnumMenuItemStyle.Default, nil)
              CommonDefs.ListAdd_LuaCall(actionPairs, kickPlayer)

              local transferLeader = PopupMenuItemData(LocalString.GetString("转让队长"), DelegateFactory.Action_ulong_string(function ( playerId, playerName )
                local msg = MessageMgr.Inst:FormatMessage("Qmpk_ChangeTeamLeader", playerName)
                MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function()
                  Gac2Gas.StarBiwuTransferCombatTeam(playerId)
                end), nil, nil, nil, false)
              end), EnumMenuItemStyle.Default, nil)
              CommonDefs.ListAdd_LuaCall(actionPairs, transferLeader)
            end
            break
        elseif default == EnumPlayerInfoContext.ShenYaoPassingRecord then
            LuaPlayerInfoMgr:AppendAddOrDelFriendAction(playerId, displayName, actionPairs, isSuccess)
            break
        elseif default == EnumPlayerInfoContext.XHTXTeamLeader then
            LuaPlayerInfoMgr:AppendAddOrDelFriendAction(playerId, displayName, actionPairs, isSuccess)
            break
        end
    until 1
    
    -- 蜃妖通关记录里去掉拉黑和举报
    if context ~= EnumPlayerInfoContext.ShenYaoPassingRecord then
        LuaPlayerInfoMgr:AppendBlackListActions(playerId, displayName, isCrossServerPlayer, actionPairs, isSuccess)

        if isSuccess then
            CommonDefs.ListAdd_LuaCall(actionPairs, LuaPlayerInfoMgr:GetMenuItem_Report(playerId, displayName))
        end
        --所有都加“举报”，已删除角色不加
    end
    if not isCrossServerPlayer then
        if isSuccess and context == EnumPlayerInfoContext.GuildApplyMember then
            CommonDefs.ListAdd_LuaCall(actionPairs, LuaPlayerInfoMgr:GetMenuItem_AcceptApplyGuild(playerId, displayName))
        end
    end
    if isSuccess and CPersonalSpaceMgr.OpenPersonalSpaceEntrance then
        CommonDefs.ListAdd_LuaCall(actionPairs, LuaPlayerInfoMgr:GetMenuItem_PersonalSpace(playerId, displayName))
        --#66748 梦岛在交互菜单中放在最后一位
    end
    if isSuccess and CPersonalSpaceMgr.EnableSendFlowerSign then
        CommonDefs.ListAdd_LuaCall(actionPairs, LuaPlayerInfoMgr:GetMenuItem_SendFlower(playerId, displayName))
    end
    if not isCrossServerPlayer then
        local zqData = ZuoQi_ZuoQi.GetData(CClientMainPlayer.Inst.AppearanceProp.ZuoQiTemplateId)
        if isSuccess and zqData ~= nil then
            local coride = zqData.CoRide
            if CZuoQiMgr.IsMapiTemplateId(zqData.ID) and not System.String.IsNullOrEmpty(CClientMainPlayer.Inst.BasicProp.CurrentZuoQiId) then
                local zqdata = CZuoQiMgr.Inst:GetZuoQiById(CClientMainPlayer.Inst.BasicProp.CurrentZuoQiId)
                if zqdata ~= nil and zqdata.mapiInfo ~= nil and zqdata.mapiInfo.IsDoubleRide > 0 then
                    coride = 1
                else
                    coride = 0
                end
            end
            if coride > 0 and zqData.HideCoRideMenu ~= 1 then
                CommonDefs.ListAdd_LuaCall(actionPairs, LuaPlayerInfoMgr:GetMenuItem_InviteCoride(playerId, displayName))
            end
        end
        if isSuccess and CFamilyTreeMgr.IsOpen() and CClientMainPlayer.Inst.MaxLevel >= 30 and not CQuanMinPKMgr.Inst.m_IsQuanMinPKServer then
            CommonDefs.ListAdd_LuaCall(actionPairs, LuaPlayerInfoMgr:GetMenuItem_FamilyTree(playerId, displayName))
        end
    end
    if isSuccess and CQuanMinPKMgr.Inst.m_IsQuanMinPKServer then
        CommonDefs.ListAdd_LuaCall(actionPairs, LuaPlayerInfoMgr:GetMenuItem_QMPKConfigLearn(playerId, displayName))
    end

    if context == EnumPlayerInfoContext.GuildMember then
        if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.Id ~= playerId then
            CommonDefs.ListAdd_LuaCall(actionPairs, LuaPlayerInfoMgr:GetMenuItem_ChangeGuildTitle(playerId, displayName))
            CommonDefs.ListAdd_LuaCall(actionPairs, LuaPlayerInfoMgr:GetMenuItem_KickGuildMember(playerId, displayName))
            if CGuildMgr.Inst.IsForbiddenToSpeakOpen then
                if CGuildMgr.Inst:IsForbiddenToSpeak(playerId) then
                    CommonDefs.ListAdd_LuaCall(actionPairs, LuaPlayerInfoMgr:GetMenuItem_EnableGuildSpeak(playerId, displayName))
                else
                    CommonDefs.ListAdd_LuaCall(actionPairs, LuaPlayerInfoMgr:GetMenuItem_ForbidGuildSpeak(playerId, displayName))
                end
            end
        end
    end
    LuaPlayerInfoMgr:AddZongMenPopupMenuItemData(context, playerId, actionPairs, isSuccess)
    LuaPlayerInfoMgr:AddShiTuMenuItemData(context, playerId, actionPairs, isSuccess)
    LuaPlayerInfoMgr:AddGuildExternalAidPopupMenuItemData(context, playerId, actionPairs, isSuccess)
    LuaPlayerInfoMgr:AddPropertyCompareItem(context, playerId, actionPairs, isSuccess)
    LuaPlayerInfoMgr:AddArenaItemData(context, playerId, actionPairs, isSuccess)
    LuaPlayerInfoMgr:AddXHTXTeamLeaderItemData(context, playerId, actionPairs, isSuccess)
    LuaPlayerInfoMgr:AddStarBiWuFreePlayerInvitedItemData(context, playerId, actionPairs, isSuccess)
    if isSuccess and CQuanMinPKMgr.Inst.m_IsQuanMinPKServer then
        CommonDefs.ListAdd_LuaCall(actionPairs, LuaPlayerInfoMgr:GetMenuItem_QmpkPersonalInfo(playerId, displayName))
    end

    if context == EnumPlayerInfoContext.PlayerInChannel then
        if isSuccess and CPlayerInfoMgr.PopupMenuInfo.channel == EChatPanel.Guild then
            CommonDefs.ListAdd_LuaCall(actionPairs, LuaPlayerInfoMgr:GetMenuItem_ShowGuildMember(playerId, displayName))
        end
    end

    if actionPairs == nil then
        return
    end

    if isSuccess and not isCrossServerPlayer and BabyMgr.Inst:IsReadyToGiveBirth(playerId) then
        CommonDefs.ListAdd_LuaCall(actionPairs, LuaPlayerInfoMgr:GetMenuItem_DeliverBaby(playerId,displayName))
    end

    if LuaJuDianBattleMgr:IsInGameplay() and LuaJuDianBattleMgr.NeedShowPlayerJuDianInfo and CClientMainPlayer.Inst then
        if otherPlayer then
            CommonDefs.ListAdd_LuaCall(actionPairs, LuaPlayerInfoMgr:GetMenuItem_JuDianDivorcePlayer (playerId,displayName))
        end
        -- 自己只有加buff
        CommonDefs.ListAdd_LuaCall(actionPairs, LuaPlayerInfoMgr:GetMenuItem_JuDianAddBuff(playerId,displayName)) 
    end

    if CIMMgr.Inst:IsMyFriend(playerId) and CChatMgr.m_QuickChatOpen then --快捷聊天
        CommonDefs.ListAdd_LuaCall(actionPairs, LuaPlayerInfoMgr:GetMenuItem_QuickChat(playerId, displayName))
    end

    this:ShowPlayerPopupMenuAfterQuery_2(playerId, level, portraitName, expressionTxt, profileInfo, displayName, xiuweiGrade, context, online, serverGroupId, isInXianShenStatus, actionPairs)
end

CPlayerInfoMgr.m_ShowPlayerInfoWnd_CS2LuaHook = function (playerId, playerName)
    --主角不在线或者点击了主角自身时，不弹出查看信息窗口
    if CClientMainPlayer.Inst == nil or CClientMainPlayer.Inst.Id == playerId then
        return
    end
    CPlayerInfoMgr.PlayerInfo = nil
    CPlayerInfoMgr.PlayerId = playerId
    CPlayerInfoMgr.PlayerName = playerName
    CUIManager.ShowUI(CUIResources.PlayerInfoWnd)
end

LuaPlayerInfoMgr = {}

function LuaPlayerInfoMgr:AddGuildExternalAidPopupMenuItemData(context, playerId, actionPairs, isSuccess)
    if context == EnumPlayerInfoContext.GuildExternalAid then
        local data = PopupMenuItemData(LocalString.GetString("请离外援"), DelegateFactory.Action_ulong_string(function ( ... )
            local msg = g_MessageMgr:FormatMessage("GuildForeignAidDeletePlayer_Confirm")
            MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
                LuaGuildExternalAidMgr:GuildForeignAidDeletePlayer(playerId, LuaGuildExternalAidMgr.m_ForeignAidInfo.m_Context)
            end),nil,LocalString.GetString("确认"),LocalString.GetString("取消"),false)
        end), EnumMenuItemStyle.Default, nil)
        CommonDefs.ListAdd_LuaCall(actionPairs, data)
    end
end

function LuaPlayerInfoMgr:AddZongMenPopupMenuItemData(context, playerId, actionPairs, isSuccess)
    if context == EnumPlayerInfoContext.ZongMenMember then
        local extraInfoDict = CPlayerInfoMgr.PopupMenuInfo.extraInfo
        local acceptSectApplyId = CommonDefs.DictGetValue_LuaCall(extraInfoDict, "RequestAcceptSectApplyId")
        if acceptSectApplyId and acceptSectApplyId > 0 then
            local requestAcceptSectApply = PopupMenuItemData(LocalString.GetString("接受申请"),  DelegateFactory.Action_ulong_string(function ( ...) 
                Gac2Gas.RequestAcceptSectApply_Ite(playerId)
            end), EnumMenuItemStyle.Orange, nil)
            CommonDefs.ListAdd_LuaCall(actionPairs, requestAcceptSectApply)
            return
        end
        local changeWeiJie = PopupMenuItemData(LocalString.GetString("变更位阶"), DelegateFactory.Action_ulong_string(function ( ... )
            LuaZongMenMgr:ChangeWeiJie(playerId)
        end), EnumMenuItemStyle.Default, nil)
        local kickSectMember = PopupMenuItemData(LocalString.GetString("逐出宗派"), DelegateFactory.Action_ulong_string(function (playerId, playerName )
            local msg = g_MessageMgr:FormatMessage("ZongMen_Kick",playerName)
            local okAction = DelegateFactory.Action(function () 
                if CClientMainPlayer.Inst then
                    Gac2Gas.RequestKickSectMember(CClientMainPlayer.Inst.BasicProp.SectId, playerId)
                end
            end)
            MessageWndManager.ShowOKCancelMessage(msg,okAction,nil,LocalString.GetString("确定"), LocalString.GetString("取消"),false)
        end), EnumMenuItemStyle.Default, nil)
        local bForbidden = LuaZongMenMgr.m_PlayerId2Forbidden[playerId]
        local setSectMemberSpeakStatus = PopupMenuItemData(not bForbidden and LocalString.GetString("宗派禁言") or LocalString.GetString("禁言解封"), DelegateFactory.Action_ulong_string(function ( ... )
            if CClientMainPlayer.Inst then
                Gac2Gas.RequestSetSectMemberSpeakStatus(CClientMainPlayer.Inst.BasicProp.SectId, playerId, bForbidden and 0 or 1)
            end
        end), EnumMenuItemStyle.Default, nil)
        CommonDefs.ListAdd_LuaCall(actionPairs, changeWeiJie)
        CommonDefs.ListAdd_LuaCall(actionPairs, kickSectMember)
        CommonDefs.ListAdd_LuaCall(actionPairs, setSectMemberSpeakStatus)
    end
end

function LuaPlayerInfoMgr:AddShiTuMenuItemData(context, playerId, actionPairs, isSuccess)
    if context == EnumPlayerInfoContext.ShiTu then
        local extraInfoDict = CPlayerInfoMgr.PopupMenuInfo.extraInfo
        local baiShiId = CommonDefs.DictGetValue_LuaCall(extraInfoDict, "BaiShiId")
        local shouTuId = CommonDefs.DictGetValue_LuaCall(extraInfoDict, "ShouTuId")
        local tuDiId = CommonDefs.DictGetValue_LuaCall(extraInfoDict, "TuDiId")
        local tuDiName = CommonDefs.DictGetValue_LuaCall(extraInfoDict, "TuDiName")
        local shiFuId = CommonDefs.DictGetValue_LuaCall(extraInfoDict, "ShiFuId")
        local waiMen = CommonDefs.DictGetValue_LuaCall(extraInfoDict, "waiMen")
        if baiShiId and baiShiId > 0 then
            local baishi = PopupMenuItemData(LocalString.GetString("拜师"), DelegateFactory.Action_ulong_string(function ( ... )
                local t = {[baiShiId] = waiMen}
                Gac2Gas.RequestBaiShiToMultiPlayers(g_MessagePack.pack(t))
                for i = 1, #LuaShiTuMgr.m_RecommendShiFuList do
                    if LuaShiTuMgr.m_RecommendShiFuList[i].playerId == baiShiId then
                        LuaShiTuMgr.m_RecommendShiFuList[i].isApplied = true
                    end
                end
                if LuaShiTuMgr.m_BestMatchRecommendShiFu.playerId == baiShiId then
                    LuaShiTuMgr.m_BestMatchRecommendShiFu.isApplied = true
                end
                g_ScriptEvent:BroadcastInLua("OnSendRecommendShiFu")
            end), EnumMenuItemStyle.Orange, nil)
            CommonDefs.ListAdd_LuaCall(actionPairs, baishi)
        end
        if shouTuId and shouTuId > 0 then
            local shoutu = PopupMenuItemData(LocalString.GetString("收徒"), DelegateFactory.Action_ulong_string(function ( ... )
                local t = {[shouTuId] = waiMen}
                Gac2Gas.RequestShouTuToMultiPlayers(g_MessagePack.pack(t))
                for i = 1, #LuaShiTuMgr.m_RecommendTuDiList do
                    if LuaShiTuMgr.m_RecommendTuDiList[i].playerId == shouTuId then
                        LuaShiTuMgr.m_RecommendTuDiList[i].isApplied = true
                    end
                end
                if LuaShiTuMgr.m_BestMatchRecommendTuDi.playerId == baiShiId then
                    LuaShiTuMgr.m_BestMatchRecommendTuDi.isApplied = true
                end
                g_ScriptEvent:BroadcastInLua("OnSendRecommendTuDi")
            end), EnumMenuItemStyle.Orange, nil)
            CommonDefs.ListAdd_LuaCall(actionPairs, shoutu)
        end
        if tuDiId and tuDiId > 0 then
            local likui = PopupMenuItemData(LocalString.GetString("礼赠弟子"), DelegateFactory.Action_ulong_string(function ( ... )
                LuaShiTuMgr:OpenShiTuGiftGivingWnd(tuDiId, false)
            end), EnumMenuItemStyle.Default, nil)
            CommonDefs.ListAdd_LuaCall(actionPairs, likui)
            local zhuchu = PopupMenuItemData(LocalString.GetString("逐出师门"), DelegateFactory.Action_ulong_string(function ( ... )
                local message = g_MessageMgr:FormatMessage("BreakShiTu_Confirm", tuDiName)
                MessageWndManager.ShowDelayOKCancelMessage(message, DelegateFactory.Action(function () 
                    Gac2Gas.BreakShiTuRelationship(false, tuDiId)
                end), nil, 5)
            end), EnumMenuItemStyle.Default, nil)
            CommonDefs.ListAdd_LuaCall(actionPairs, zhuchu)
        end
        if shiFuId and shiFuId > 0 then
            local likui = PopupMenuItemData(LocalString.GetString("礼馈师恩"), DelegateFactory.Action_ulong_string(function ( ... )
                LuaShiTuMgr:OpenShiTuGiftGivingWnd(shiFuId, true)
            end), EnumMenuItemStyle.Default, nil)
            CommonDefs.ListAdd_LuaCall(actionPairs, likui)
        end
    end
end

function LuaPlayerInfoMgr:ShowChatRoomPopupMenu(playerId, level, portraitName, expressionTxt, profileInfo, displayName, xiuweiGrade, context, online, serverGroupId, isInXianShenStatus, isSuccess)
    
    if context ~= EnumPlayerInfoContext.ChatRoom then
        return
    end

    local extraInfoDict = CPlayerInfoMgr.PopupMenuInfo.extraInfo
    local appName = CommonDefs.DictGetValue_LuaCall(extraInfoDict, "AppName")
    local accid = tonumber(CommonDefs.DictGetValue_LuaCall(extraInfoDict, "AccId"))
    local isMyGame = LuaClubHouseMgr:IsMyGame(appName)

    local actionPairs = CreateFromClass(MakeGenericClass(List, PopupMenuItemData))
    if isMyGame then
        --主角不在线或者点击了主角自身时，不弹出交互菜单
        if CClientMainPlayer.Inst == nil or CClientMainPlayer.Inst.Id == playerId or playerId == 0 or CIMMgr.Inst:IsSpecialPlayerID(playerId) then
            return
        end

        local isCrossServerPlayer = (CClientMainPlayer.Inst:GetMyServerId() ~= serverGroupId)
        local hasLiangHao = profileInfo.LiangHaoId>0 and CServerTimeMgr.Inst.timeStamp<profileInfo.LiangHaoExpiredTime
        local canSendMsg = not isCrossServerPlayer or hasLiangHao
        if isSuccess and canSendMsg and LuaChatMgr.PlayerInteractionPermitted(isCrossServerPlayer, CPlayerInfoMgr.PopupMenuInfo) then
            CommonDefs.ListAdd_LuaCall(actionPairs, LuaPlayerInfoMgr:GetMenuItem_SendMsg(playerId, displayName))
        end
		if isSuccess then
			CommonDefs.ListAdd_LuaCall(actionPairs, LuaPlayerInfoMgr:GetMenuItem_LookatInfo(playerId, displayName))
		end
        if isCrossServerPlayer then
            if hasLiangHao then
                if isSuccess and not CIMMgr.Inst:IsMyFriend(playerId) then
                    CommonDefs.ListAdd_LuaCall(actionPairs, LuaPlayerInfoMgr:GetMenuItem_AddFriend(playerId, displayName))
                end
            end
        else
            LuaPlayerInfoMgr:AppendAddOrDelFriendAction(playerId, displayName, actionPairs, isSuccess)
            LuaPlayerInfoMgr:AppendBlackListActions(playerId, displayName, isCrossServerPlayer, actionPairs, isSuccess)
        end
        
		if isSuccess then
			CommonDefs.ListAdd_LuaCall(actionPairs, LuaPlayerInfoMgr:GetMenuItem_Report(playerId, displayName))
		end
        if isSuccess and CPersonalSpaceMgr.OpenPersonalSpaceEntrance then
            CommonDefs.ListAdd_LuaCall(actionPairs, LuaPlayerInfoMgr:GetMenuItem_PersonalSpace(playerId, displayName))
        end
        if isSuccess and not isCrossServerPlayer and CFamilyTreeMgr.IsOpen() and CClientMainPlayer.Inst.MaxLevel >= 30 then
            CommonDefs.ListAdd_LuaCall(actionPairs, LuaPlayerInfoMgr:GetMenuItem_FamilyTree(playerId, displayName))
        end
    else
		if isSuccess then
			CommonDefs.ListAdd_LuaCall(actionPairs, LuaPlayerInfoMgr:GetMenuItem_Report(playerId, displayName))
		end
    end

    local myself = LuaClubHouseMgr:GetMyselfInfo()
    local member = LuaClubHouseMgr:GetMember(accid) --重新获取一次，避免信息刷新问题
    if member == nil then
        -- 茶室排行榜使用
        CPlayerInfoMgr.Inst:ShowPlayerPopupMenuAfterQuery_2(playerId, level, portraitName, expressionTxt, profileInfo, displayName, xiuweiGrade, context, online, serverGroupId, isInXianShenStatus, actionPairs)
        return
    end

    if myself.Character == EnumCHCharacterType.Owner then
        
        if LuaClubHouseMgr:IsAudience(member) then--房主操作听众：请离房间
            CommonDefs.ListAdd_LuaCall(actionPairs, self:GetMenuItem_KickoutRoom(accid))

        elseif LuaClubHouseMgr:IsBroadcaster(member) then--房主操作主播：踢下麦位、禁言或取消禁言、转移房主、请离房间
            CommonDefs.ListAdd_LuaCall(actionPairs, self:GetMenuItem_KickoutMic(accid))
            if LuaClubHouseMgr:IsForbidden(member) then
                CommonDefs.ListAdd_LuaCall(actionPairs, self:GetMenuItem_CancelForbidMic(accid))
            else
                CommonDefs.ListAdd_LuaCall(actionPairs, self:GetMenuItem_ForbidMic(accid))
            end
            CommonDefs.ListAdd_LuaCall(actionPairs, self:GetMenuItem_ChangeOwner(accid))
            CommonDefs.ListAdd_LuaCall(actionPairs, self:GetMenuItem_KickoutRoom(accid))
        end
    end

    if CIMMgr.Inst:IsMyFriend(playerId) and CChatMgr.m_QuickChatOpen then --快捷聊天
        CommonDefs.ListAdd_LuaCall(actionPairs, LuaPlayerInfoMgr:GetMenuItem_QuickChat(playerId, displayName))
    end

    CPlayerInfoMgr.Inst:ShowPlayerPopupMenuAfterQuery_2(playerId, member.Level, member.Portrait, expressionTxt, profileInfo, member.PlayerName, xiuweiGrade, context, online, serverGroupId, isInXianShenStatus, actionPairs)
end


--[[function LuaPlayerInfoMgr:GetMenuItem_InviteMic(accid)
    return PopupMenuItemData(LocalString.GetString("抱上麦位"), DelegateFactory.Action_ulong_string(function (playerId, playerName )
            local msg = g_MessageMgr:FormatMessage("CLUBHOUSE_INVITE_MIC_CONFIRM",playerName)
            local action = DelegateFactory.Action(function () 
                LuaClubHouseMgr:SetMicEnabled(accid, true)
            end)
            MessageWndManager.ShowOKCancelMessage(msg,action,nil,nil,nil,false)
        end), EnumMenuItemStyle.Default, nil)
end]]

function LuaPlayerInfoMgr:GetMenuItem_KickoutRoom(accid)
    return PopupMenuItemData(LocalString.GetString("请离茶室"), DelegateFactory.Action_ulong_string(function (playerId, playerName )
            local msg = g_MessageMgr:FormatMessage("CLUBHOUSE_KICK_OUT_ROOM_CONFIRM",playerName)
            local action = DelegateFactory.Action(function () 
                LuaClubHouseMgr:KickoutRoom(accid)
            end)
            MessageWndManager.ShowOKCancelMessage(msg,action,nil,nil,nil,false)
        end), EnumMenuItemStyle.Default, nil)
end

function LuaPlayerInfoMgr:GetMenuItem_KickoutMic(accid)
    return PopupMenuItemData(LocalString.GetString("踢下麦位"), DelegateFactory.Action_ulong_string(function (playerId, playerName )
            local msg = g_MessageMgr:FormatMessage("CLUBHOUSE_KICK_OUT_MIC_CONFIRM",playerName)
            local action = DelegateFactory.Action(function () 
                LuaClubHouseMgr:SetMicEnabled(accid, false)
            end)
            MessageWndManager.ShowOKCancelMessage(msg,action,nil,nil,nil,false)
        end), EnumMenuItemStyle.Default, nil)
end

function LuaPlayerInfoMgr:GetMenuItem_ForbidMic(accid)
    return PopupMenuItemData(LocalString.GetString("静音麦位"), DelegateFactory.Action_ulong_string(function (playerId, playerName )
            local msg = g_MessageMgr:FormatMessage("CLUBHOUSE_FORBID_MIC_CONFIRM",playerName)
            local action = DelegateFactory.Action(function () 
                LuaClubHouseMgr:SetForbidStatus(accid, true)
            end)
            MessageWndManager.ShowOKCancelMessage(msg,action,nil,nil,nil,false)
        end), EnumMenuItemStyle.Default, nil)
end

function LuaPlayerInfoMgr:GetMenuItem_CancelForbidMic(accid)
    return PopupMenuItemData(LocalString.GetString("取消静音"), DelegateFactory.Action_ulong_string(function (playerId, playerName )
            local msg = g_MessageMgr:FormatMessage("CLUBHOUSE_CANCEL_FORBID_MIC_CONFIRM",playerName)
            local action = DelegateFactory.Action(function () 
                LuaClubHouseMgr:SetForbidStatus(accid, false)
            end)
            MessageWndManager.ShowOKCancelMessage(msg,action,nil,nil,nil,false)
        end), EnumMenuItemStyle.Default, nil)
end

function LuaPlayerInfoMgr:GetMenuItem_ChangeOwner(accid)
    return PopupMenuItemData(LocalString.GetString("转移室主"), DelegateFactory.Action_ulong_string(function (playerId, playerName )
            local msg = g_MessageMgr:FormatMessage("CLUBHOUSE_CHANGE_OWNER_CONFIRM",playerName)
            local action = DelegateFactory.Action(function () 
                LuaClubHouseMgr:ChangeOwner(accid)
            end)
            MessageWndManager.ShowOKCancelMessage(msg,action,nil,nil,nil,false)
        end), EnumMenuItemStyle.Default, nil)
end
function LuaPlayerInfoMgr:AddPropertyCompareItem(context, playerId, actionPairs, isSuccess)
    if CQuanMinPKMgr.Inst.m_IsQuanMinPKServer then
        return
    end
    if LuaPlayerPropertyMgr.PropertyGuide and LuaPlayerPropertyMgr.PlayerCompareData and context == EnumPlayerInfoContext.Undefined then
        local data = PopupMenuItemData(LocalString.GetString("属性对比"), DelegateFactory.Action_ulong_string(function ( ... )
            LuaPlayerPropertyMgr:ShowPlayerPropertyCompareWnd(LuaPlayerPropertyMgr.PropertyGuideID,LuaPlayerPropertyMgr.PlayerCompareData)
        end), EnumMenuItemStyle.Default, nil)
        CommonDefs.ListAdd_LuaCall(actionPairs, data)
    elseif LuaPlayerPropertyMgr.PropertyGuide and context == EnumPlayerInfoContext.CurrentTarget then
        local data = PopupMenuItemData(LocalString.GetString("属性对比"), DelegateFactory.Action_ulong_string(function ( ... )
            LuaPlayerPropertyMgr:ShowPlayerPropertyCompareOnlineWnd(playerId)
        end), EnumMenuItemStyle.Default, nil)
        CommonDefs.ListAdd_LuaCall(actionPairs, data)
    end
end

function LuaPlayerInfoMgr:AddArenaItemData(context, playerId, actionPairs, isSuccess)
    if context == EnumPlayerInfoContext.Arena then
        local data = PopupMenuItemData(LocalString.GetString("竞技数据"), DelegateFactory.Action_ulong_string(function ( ... )
            LuaArenaMgr:ShowPlayerInfoWnd(playerId)
        end), EnumMenuItemStyle.Default, nil)
        CommonDefs.ListAdd_LuaCall(actionPairs, data)
    end
end

function LuaPlayerInfoMgr:AddXHTXTeamLeaderItemData(context, playerId, actionPairs, isSuccess)
    if context == EnumPlayerInfoContext.XHTXTeamLeader then
        local kickPlayer = PopupMenuItemData(LocalString.GetString("请离成员"), DelegateFactory.Action_ulong_string(function ( playerId, playerName )
        local msg = g_MessageMgr:FormatMessage("SHUJIA2023_XHTX_KICKOFFMEMBER_MAKESURE", playerName)
        MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function()
            Gac2Gas.XianHaiTongXingLeaveTeam(playerId, true)
        end), nil, nil, nil, false)
        end), EnumMenuItemStyle.Orange, nil)
        CommonDefs.ListAdd_LuaCall(actionPairs, kickPlayer)
    end
end

function LuaPlayerInfoMgr:AddStarBiWuFreePlayerInvitedItemData(context, playerId, actionPairs, isSuccess)
    if context == EnumPlayerInfoContext.StarBiWuFreePlayer then
        local invitedPlayer = PopupMenuItemData(LocalString.GetString("邀请入战队"), DelegateFactory.Action_ulong_string(function ( playerId, playerName )
            Gac2Gas.RequestInviteStarBiwuFreeMember(playerId)
        end), EnumMenuItemStyle.Orange, nil)
        CommonDefs.ListAdd_LuaCall(actionPairs, invitedPlayer)
    end
end

----
-- 一些便利方法，用于简化实现action组合
----

function LuaPlayerInfoMgr:AppendAddOrDelFriendAction(playerId, playerName, actionPairs, isSuccess)
    if CIMMgr.Inst:IsMyFriend(playerId) then
        CommonDefs.ListAdd_LuaCall(actionPairs, self:GetMenuItem_DelFriend(playerId, playerName))
    elseif isSuccess and CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.Id ~= playerId then
        CommonDefs.ListAdd_LuaCall(actionPairs, self:GetMenuItem_AddFriend(playerId, playerName))
    end
end

function LuaPlayerInfoMgr:AppendBlackListActions(playerId, playerName, isCrossServerPlayer, actionPairs, isSuccess)
    if L10.Engine.CSwitchMgr.BlackListEnabled and LuaChatMgr.PlayerInteractionPermitted(isCrossServerPlayer, CPlayerInfoMgr.PopupMenuInfo) then
        if CIMMgr.Inst:IsInBlackList(playerId, true) then
            CommonDefs.ListAdd_LuaCall(actionPairs, self:GetMenuItem_DelBlacklist(playerId, playerName))
        elseif isSuccess then
            CommonDefs.ListAdd_LuaCall(actionPairs, self:GetMenuItem_AddBlacklist(playerId, playerName))
        end
    end
end

function LuaPlayerInfoMgr:AppendQlcjAction(playerId, playerName, isCrossServerPlayer, actionPairs, isSuccess)
    if isSuccess and not isCrossServerPlayer then
        if CWeddingMgr.Inst:IsMyHusband(playerId) or CWeddingMgr.Inst:IsMyWife(playerId) then
            CommonDefs.ListAdd_LuaCall(actionPairs, self:GetMenuItem_QlcjSkill(playerId, playerName))
        end
    end
end

----
-- 原C# Action搬运
----

function LuaPlayerInfoMgr:GetMenuItem_SendMsg(playerId, playerName)
    return PopupMenuItemData(LocalString.GetString("发送信息"), DelegateFactory.Action_ulong_string(function (playerId, playerName)
        CChatHelper.ShowFriendWnd(playerId, playerName)
    end), EnumMenuItemStyle.Default, nil)
end
function LuaPlayerInfoMgr:GetMenuItem_AddFriend(playerId, playerName)
    return PopupMenuItemData(LocalString.GetString("添加好友"), DelegateFactory.Action_ulong_string(function (playerId, playerName)
        Gac2Gas.AddFriend(playerId, "AddFrindAction")
    end), EnumMenuItemStyle.Default, nil)
end
function LuaPlayerInfoMgr:GetMenuItem_DelFriend(playerId, playerName)
    return PopupMenuItemData(LocalString.GetString("删除好友"), DelegateFactory.Action_ulong_string(function (playerId, playerName)
        local msg = LocalString.GetString("是否删除好友 ")..playerName
        local action = DelegateFactory.Action(function () 
            
            if CIMMgr.Inst:GetFriendliness(playerId) >= GameSetting_Common.GetData().Friendly_Degree then
                local str = g_MessageMgr:FormatMessage("CONFIRM_DELETE_SINGLE_CLOSEFRIEND", playerName)
                MessageWndManager.ShowDelayOKCancelMessage(str, DelegateFactory.Action(function ()
                    Gac2Gas.DelFriend(playerId)
                end), nil, 3)
            else
                Gac2Gas.DelFriend(playerId)
            end
        
        end)
        MessageWndManager.ShowOKCancelMessage(msg,action,nil,nil,nil,false)
    end), EnumMenuItemStyle.Default, nil)
end
function LuaPlayerInfoMgr:GetMenuItem_AddBlacklist(playerId, playerName)
    return PopupMenuItemData(LocalString.GetString("加入黑名单"), DelegateFactory.Action_ulong_string(function (playerId, playerName)
        Gac2Gas.AddBlacklist(playerId)
    end), EnumMenuItemStyle.Default, nil)
end
function LuaPlayerInfoMgr:GetMenuItem_DelBlacklist(playerId, playerName)
    return PopupMenuItemData(LocalString.GetString("从黑名单删除"), DelegateFactory.Action_ulong_string(function (playerId, playerName)
        Gac2Gas.DelBlacklist(playerId)
    end), EnumMenuItemStyle.Default, nil)
end
function LuaPlayerInfoMgr:GetMenuItem_DelEnemy(playerId, playerName)
    return PopupMenuItemData(LocalString.GetString("删除仇敌"), DelegateFactory.Action_ulong_string(function (playerId, playerName)
        local msg = g_MessageMgr:FormatMessage("Friend_Wnd_Confirm_Delete_Enemy", playerName)
        local action = DelegateFactory.Action(function ()  
            Gac2Gas.DelEnemy(playerId)
        end)
        MessageWndManager.ShowOKCancelMessage(msg,action,nil,nil,nil,false)

    end), EnumMenuItemStyle.Default, nil)
end
function LuaPlayerInfoMgr:GetMenuItem_InviteToJoinTeam(playerId, playerName)
    return PopupMenuItemData(LocalString.GetString("邀请入队"), DelegateFactory.Action_ulong_string(function (playerId, playerName)
        CTeamMgr.Inst:RequestInviteToTeam(playerId)
    end), EnumMenuItemStyle.Default, nil)
end
function LuaPlayerInfoMgr:GetMenuItem_ApplyToJoinTeam(playerId, playerName)
    return PopupMenuItemData(LocalString.GetString("申请入队"), DelegateFactory.Action_ulong_string(function (playerId, playerName)
        CTeamMgr.Inst:RequestJoinTeam(playerId)
    end), EnumMenuItemStyle.Default, nil)
end
function LuaPlayerInfoMgr:GetMenuItem_LookatInfo(playerId, playerName)
    return PopupMenuItemData(LocalString.GetString("查看信息"), DelegateFactory.Action_ulong_string(function (playerId, playerName)
        CPlayerInfoMgr.ShowPlayerInfoWnd(playerId, playerName)
    end), EnumMenuItemStyle.Default, nil)
end
function LuaPlayerInfoMgr:GetMenuItem_DelRecent(playerId, playerName)
    return PopupMenuItemData(LocalString.GetString("移出列表"), DelegateFactory.Action_ulong_string(function (playerId, playerName)
        Gac2Gas.DelRecent(playerId)
        CIMMgr.Inst:ClearNewIMMsgCount(playerId)
    end), EnumMenuItemStyle.Default, nil)
end
function LuaPlayerInfoMgr:GetMenuItem_DelRecentInteraction(playerId, playerName)
    return PopupMenuItemData(LocalString.GetString("移出列表"), DelegateFactory.Action_ulong_string(function (playerId, playerName)
        Gac2Gas.DelInteract(playerId)
    end), EnumMenuItemStyle.Default, nil)
end
function LuaPlayerInfoMgr:GetMenuItem_Report(playerId, playerName)
    return PopupMenuItemData(LocalString.GetString("举报"), DelegateFactory.Action_ulong_string(function (playerId, playerName)
        LuaPlayerReportMgr:ShowDefaultReportWnd(playerId, playerName)
    end), EnumMenuItemStyle.Default, nil)
end
function LuaPlayerInfoMgr:GetMenuItem_RequestChallenge(playerId, playerName)
    return PopupMenuItemData(LocalString.GetString("个人宣战"), DelegateFactory.Action_ulong_string(function (playerId, playerName)
        local msg = g_MessageMgr:FormatMessage("PERSONAL_CHALLENGE_CONFIRM", playerName)
        MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function ()
            Gac2Gas.RequestPersonalChallenge(playerId)
        end), nil, nil, nil, false)
    end), EnumMenuItemStyle.Default, nil)
end
function LuaPlayerInfoMgr:GetMenuItem_QieCuo(playerId, playerName)
    local showStr = LocalString.GetString("请求切磋")
    if CommonDefs.IsPlayEnabled("ChaQiJueDou") then
        showStr = LocalString.GetString("切磋论武")
    end
    return PopupMenuItemData(showStr, DelegateFactory.Action_ulong_string(function (playerId, playerName)
        if CommonDefs.IsPlayEnabled("ChaQiJueDou") then
            self.m_QieCuoPlayerId = playerId
            CUIManager.ShowUI(CLuaUIResources.QieCuoPopupMenuWnd)
        else
            Gac2Gas.RequestStartQieCuo(playerId)
        end
    end), EnumMenuItemStyle.Default, nil)
end
function LuaPlayerInfoMgr:GetMenuItem_FtfTransaction(playerId, playerName)
    return PopupMenuItemData(LocalString.GetString("请求交易"), DelegateFactory.Action_ulong_string(function (playerId, playerName)
        if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.Target ~= nil and playerId ~= 0 then
            --MessageWndManager.ShowOKCancelMessage("当面交易只能够出售或购买珍品，请选择你的交易目的：",
            --    () =>
            --    {
            --        CTradeMgr.Inst.targetEngineId = CClientMainPlayer.Inst.Target.EngineId;
            --        Gac2Gas.RequestStartTrade(playerId, (uint)EnumTradeSellType.Precious);
            --    },
            --    () =>
            --    {
            --        CTradeMgr.Inst.targetEngineId = CClientMainPlayer.Inst.Target.EngineId;
            --        Gac2Gas.RequestStartTrade(playerId, (uint)EnumTradeSellType.Silver);
            --    }, "我要出售", "我要购买", true);
            CTradeMessageBox.playerId = playerId
            CUIManager.ShowUI(CUIResources.TradeMessageBox)
        else
            g_MessageMgr:ShowMessage("OTHER_PLAYER_LEAVE")
        end
    end), EnumMenuItemStyle.Default, nil)
end
function LuaPlayerInfoMgr:GetMenuItem_QuitGuild(playerId, playerName)
    return PopupMenuItemData(LocalString.GetString("辞职"), DelegateFactory.Action_ulong_string(function (playerId, playerName)
        if CGuildMgr.Inst:IsNeiWuFuOfficer() then
            local t={}
            local item=PopupMenuItemData2(CGuildMgr.Inst:GetMyTitle(),DelegateFactory.Action_int(function(index) CGuildMgr.Inst:TryResign() end),false,nil)
            table.insert(t, item)
            local item=PopupMenuItemData2(Guild_Setting.GetData().NeiWuFuOfficerName,DelegateFactory.Action_int(function(index) CGuildMgr.Inst:TryResignNeiWuFu() end),false,nil)
            table.insert(t, item)
            local array=Table2Array(t,MakeArrayClass(PopupMenuItemData2))
            CPopupMenuInfoMgr.ShowPopupMenu(array, CPlayerInfoMgr.PopupMenuInfo.worldPos, CPopupMenuInfoMgrAlignType.Top)
        else
            CGuildMgr.Inst:TryResign()
        end
    end), EnumMenuItemStyle.Default, nil)
end
function LuaPlayerInfoMgr:GetMenuItem_AcceptApplyGuild(playerId, playerName)
    return PopupMenuItemData(LocalString.GetString("接受申请"), DelegateFactory.Action_ulong_string(function (playerId, playerName)
        if CClientMainPlayer.Inst ~= nil then
            Gac2Gas.RequestOperationInGuild(CClientMainPlayer.Inst.BasicProp.GuildId, "AcceptApplyAsMember", tostring(playerId), "")
        end
    end), EnumMenuItemStyle.Orange, nil)
end
function LuaPlayerInfoMgr:GetMenuItem_PersonalSpace(playerId, playerName)
    return PopupMenuItemData(LocalString.GetString("梦岛"), DelegateFactory.Action_ulong_string(function (playerId, playerName)
        CPersonalSpaceMgr.Inst:OpenPersonalSpaceWnd(playerId)
    end), EnumMenuItemStyle.Default, Constants.FriendPopupMenuPersonalSpaceImageName)
end
function LuaPlayerInfoMgr:GetMenuItem_InviteCoride(playerId, playerName)
    return PopupMenuItemData(LocalString.GetString("邀请同骑"), DelegateFactory.Action_ulong_string(function (playerId, playerName)
        CZuoQiMgr.Inst:InviteCoride(playerId)
    end), EnumMenuItemStyle.Default, nil)
end
function LuaPlayerInfoMgr:GetMenuItem_FamilyTree(playerId, playerName)
    return PopupMenuItemData(LocalString.GetString("查看家谱"), DelegateFactory.Action_ulong_string(function (playerId, playerName)
        CFamilyTreeMgr.Instance:SendFamilyTreeRequest(playerId)
    end), EnumMenuItemStyle.Default, nil)
end
function LuaPlayerInfoMgr:GetMenuItem_QMPKConfigLearn(playerId, playerName)
    return PopupMenuItemData(LocalString.GetString("配置学习"), DelegateFactory.Action_ulong_string(function (playerId, playerName)
        CLuaQMPKMgr.ShowPlayerConfigWnd(playerId, playerName)
    end), EnumMenuItemStyle.Default, nil)
end
function LuaPlayerInfoMgr:GetMenuItem_InviteToTeamGroup(playerId, playerName)
    return PopupMenuItemData(LocalString.GetString("邀请入团"), DelegateFactory.Action_ulong_string(function (playerId, playerName)
        Gac2Gas.InvitePlayerJoinTeamGroup(playerId)
    end), EnumMenuItemStyle.Default, nil)
end
function LuaPlayerInfoMgr:GetMenuItem_ApplyToTeamGroup(playerId, playerName)
    return PopupMenuItemData(LocalString.GetString("申请入团"), DelegateFactory.Action_ulong_string(function (playerId, playerName)
        Gac2Gas.RequestJoinTeamGroup(playerId)
    end), EnumMenuItemStyle.Default, nil)
end
function LuaPlayerInfoMgr:GetMenuItem_SetDuoHun(playerId, playerName)
    return PopupMenuItemData(LocalString.GetString("设置夺魂幡"), DelegateFactory.Action_ulong_string(function (playerId, playerName)
        CDuoHunMgr.Inst.isQueryPlayerGhostInFoeSelectWnd = false
        CDuoHunMgr.Inst.queryPlayerId = playerId
        Gac2Gas.QueryPlayerIsGhost(playerId)
    end), EnumMenuItemStyle.Default, nil)
end
function LuaPlayerInfoMgr:GetMenuItem_QlcjSkill(playerId, playerName)
    return PopupMenuItemData(LocalString.GetString("千里婵娟"), DelegateFactory.Action_ulong_string(function (playerId, playerName)
        if CClientMainPlayer.Inst == nil then
            return
        end
        local skillId = CWeddingMgr.Inst:GetTeleportSkillId()
        if skillId == 0 then
            return
        end
        if CClientMainPlayer.Inst:CheckSkillCDAndAlive(skillId) then
            CClientMainPlayer.Inst:TryCastSkill(skillId, true, Vector2.zero)
        else
            local clientRemainTime = CClientMainPlayer.Inst.CooldownProp:GetClientRemainTime(skillId)
            local serverRemainTime = CClientMainPlayer.Inst.CooldownProp:GetServerRemainTime(skillId)
            local remainTime = round2(math.max(clientRemainTime, serverRemainTime) / 1000, 1)
            if remainTime ~= 0 then
                g_MessageMgr:ShowMessage("SKILL_IN_COOLDOWN", remainTime)
            end
        end
    end), EnumMenuItemStyle.Default, nil)
end
function LuaPlayerInfoMgr:GetMenuItem_KickDouhunMember(playerId, playerName)
    return PopupMenuItemData(LocalString.GetString("请离队员"), DelegateFactory.Action_ulong_string(function (playerId, playerName)
        Gac2Gas.KickDouHunChlgTeamMember(playerId)
    end), EnumMenuItemStyle.Default, nil)
end
function LuaPlayerInfoMgr:GetMenuItem_LeaveDouhunTeam(playerId, playerName)
    return PopupMenuItemData(LocalString.GetString("退出战队"), DelegateFactory.Action_ulong_string(function (playerId, playerName)
        Gac2Gas.RequestLeaveDouHunChlgTeam()
    end), EnumMenuItemStyle.Default, nil)
end
function LuaPlayerInfoMgr:GetMenuItem_ChangeGuildTitle(playerId, playerName)
    return PopupMenuItemData(LocalString.GetString("变更职位"), DelegateFactory.Action_ulong_string(function (playerId, playerName)
        if CClientMainPlayer.Inst ~= nil then
            if CommonDefs.DictContains(CGuildMgr.Inst.m_GuildMemberInfo.Infos, typeof(UInt64), playerId) and CommonDefs.DictContains(CGuildMgr.Inst.m_GuildMemberInfo.DynamicInfos, typeof(UInt64), playerId) then
                local info = CommonDefs.DictGetValue(CGuildMgr.Inst.m_GuildMemberInfo.Infos, typeof(UInt64), playerId)
                --CGuildMemberDynamicInfoRpc_Item memberDynamicInfo = CGuildMgr.Inst.m_GuildMemberInfo.DynamicInfos[playerId];
                -- CGuildJobChangeModel.ShowWnd(playerId, playerName, GuildDefine.GetOfficeName(info.Title))
                CLuaGuildJobChangeWnd.ShowWnd(playerId, playerName, GuildDefine.GetOfficeName(info.Title))
            end
        end
    end), EnumMenuItemStyle.Default, nil)
end
function LuaPlayerInfoMgr:GetMenuItem_KickGuildMember(playerId, playerName)
    return PopupMenuItemData(LocalString.GetString("逐出帮会"), DelegateFactory.Action_ulong_string(function (playerId, playerName)
        if CClientMainPlayer.Inst ~= nil then
            local message = g_MessageMgr:FormatMessage("Guild_Expel_Confirm", playerName)
            MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function ()
                Gac2Gas.RequestOperationInGuild(CClientMainPlayer.Inst.BasicProp.GuildId, "ExpelMember", tostring(playerId), "")
            end), nil, nil, nil, false)
        end
    end), EnumMenuItemStyle.Default, nil)
end
function LuaPlayerInfoMgr:GetMenuItem_ForbidGuildSpeak(playerId, playerName)
    return PopupMenuItemData(LocalString.GetString("帮会禁言"), DelegateFactory.Action_ulong_string(function (playerId, playerName)
        Gac2Gas.RequestForbidGuildSpeak(playerId)
    end), EnumMenuItemStyle.Default, nil)
end
function LuaPlayerInfoMgr:GetMenuItem_EnableGuildSpeak(playerId, playerName)
    return PopupMenuItemData(LocalString.GetString("解除禁言"), DelegateFactory.Action_ulong_string(function (playerId, playerName)
        Gac2Gas.RequestEnableGuildSpeak(playerId)
    end), EnumMenuItemStyle.Default, nil)
end
function LuaPlayerInfoMgr:GetMenuItem_QmpkPersonalInfo(playerId, playerName)
    return PopupMenuItemData(LocalString.GetString("个人名片"), DelegateFactory.Action_ulong_string(function (playerId, playerName)
        Gac2Gas.RequestQueryQmpkPlayerDetails(playerId)
    end), EnumMenuItemStyle.Default, nil)
end
function LuaPlayerInfoMgr:GetMenuItem_QmpkKickMember(playerId, playerName)
    return PopupMenuItemData(LocalString.GetString("请离队员"), DelegateFactory.Action_ulong_string(function (playerId, playerName)
        local msg = g_MessageMgr:FormatMessage("Qmpk_KickoffMember", playerName)
        local action = DelegateFactory.Action(function ()  
            Gac2Gas.RequestRemoveQmpkZhanDuiMember(playerId)
        end)
        MessageWndManager.ShowOKCancelMessage(msg,action,nil,nil,nil,false)
    end), EnumMenuItemStyle.Default, nil)
end
function LuaPlayerInfoMgr:GetMenuItem_RemoveManager(playerId, playerName)
    return PopupMenuItemData(LocalString.GetString("撤销管理员"), DelegateFactory.Action_ulong_string(function (playerId, playerName)
        local msg = System.String.Format(LocalString.GetString("确定撤销{0}的管理员权限吗?"), playerName)
        local action = DelegateFactory.Action(function ()  
            Gac2Gas.RequestCancelTeamGroupManager(playerId)
        end)
        MessageWndManager.ShowOKCancelMessage(msg,action,nil,nil,nil,false)
    end), EnumMenuItemStyle.Default, nil)
end
function LuaPlayerInfoMgr:GetMenuItem_SendFlower(playerId, playerName)
    return PopupMenuItemData(LocalString.GetString("送花"), DelegateFactory.Action_ulong_string(function (playerId, playerName)
        CCommonSendFlowerMgr.Inst:ShowSendFlowerWnd(playerId, playerName, EnumSendFlowerType.PlayerInfoMenu, 0)
    end), EnumMenuItemStyle.Default, nil)
end
function LuaPlayerInfoMgr:GetMenuItem_QmpkTransferLeader(playerId, playerName)
    return PopupMenuItemData(LocalString.GetString("转让队长"), DelegateFactory.Action_ulong_string(function (playerId, playerName)
        local msg = g_MessageMgr:FormatMessage("Qmpk_ChangeTeamLeader", playerName)
        local action = DelegateFactory.Action(function ()  
            Gac2Gas.RequestTransferQmpkZhanDuiLeader(playerId)
        end)
        MessageWndManager.ShowOKCancelMessage(msg,action,nil,nil,nil,false)
    end), EnumMenuItemStyle.Default, nil)
end
function LuaPlayerInfoMgr:GetMenuItem_ShowGuildMember(playerId, playerName)
    return PopupMenuItemData(LocalString.GetString("帮会成员"), DelegateFactory.Action_ulong_string(function (playerId, playerName)
        if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.BasicProp.GuildId > 0 then
            CGuildMgr.Inst.GuildMainWndTabIndex = 1
            CGuildMgr.Inst.m_SelectMemberId = playerId
            CUIManager.ShowUI(CUIResources.GuildMainWnd)
        end
    end), EnumMenuItemStyle.Default, nil)
end
function LuaPlayerInfoMgr:GetMenuItem_DeliverBaby(playerId, playerName)
    return PopupMenuItemData(LocalString.GetString("接生"), DelegateFactory.Action_ulong_string(function (playerId, playerName)
        Gac2Gas.RequestHelpDeliveryBaby(playerId)
    end), EnumMenuItemStyle.Default, nil)
end
function LuaPlayerInfoMgr:GetMenuItem_FlagDuel(playerId, playerName)
    return PopupMenuItemData(LocalString.GetString("插旗论武"), DelegateFactory.Action_ulong_string(function (playerId, playerName)
        --主角不在线或者点击了主角自身时，不弹出查看信息窗口
        if CClientMainPlayer.Inst == nil or CClientMainPlayer.Inst.Id == playerId then
            return
        end
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("JUE_DOU_HUOLI_CONFIRM", playerName), function()
            Gac2Gas.RequestJueDou(playerId)
        end, nil, nil, nil, false)
    end), EnumMenuItemStyle.Default, nil)
end

function LuaPlayerInfoMgr:GetMenuItem_FlagDuel(playerId, playerName)
    return PopupMenuItemData(LocalString.GetString("插旗论武"), DelegateFactory.Action_ulong_string(function (playerId, playerName)
        --主角不在线或者点击了主角自身时，不弹出查看信息窗口
        if CClientMainPlayer.Inst == nil or CClientMainPlayer.Inst.Id == playerId then
            return
        end
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("JUE_DOU_HUOLI_CONFIRM", playerName), function()
            Gac2Gas.RequestJueDou(playerId)
        end, nil, nil, nil, false)
    end), EnumMenuItemStyle.Default, nil)
end

function LuaPlayerInfoMgr:GetMenuItem_JuDianDivorcePlayer(playerId, playerName)
    return PopupMenuItemData(LocalString.GetString("逐出据点战"), DelegateFactory.Action_ulong_string(function (playerId, playerName)
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("JuDian_Divorce_Player_Confirm", playerName), function()
            -- 逐出
            Gac2Gas.GuildJuDianRequestKickoutPlayer(playerId)
        end, nil, nil, nil, false)
    end), EnumMenuItemStyle.Default, nil)
end

function LuaPlayerInfoMgr:GetMenuItem_JuDianAddBuff(playerId, playerName)
    return PopupMenuItemData(LocalString.GetString("赋予加成"), DelegateFactory.Action_ulong_string(function (playerId, playerName)
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("JuDian_Add_Buff_Confirm", playerName), function()
            -- 加buff
            Gac2Gas.GuildJuDianRequestApplyBuff(playerId, 0)
        end, nil, nil, nil, false)
    end), EnumMenuItemStyle.Orange, nil)
end

function LuaPlayerInfoMgr:GetMenuItem_QuickChat(playerId, playerName)
    return PopupMenuItemData(LocalString.GetString("快捷聊天"), DelegateFactory.Action_ulong_string(function (playerId, playerName)
	    g_ScriptEvent:BroadcastInLua("SetQuickChatTarget", CPlayerInfoMgr.PopupMenuInfo)
    end), EnumMenuItemStyle.Default, nil)
end

LuaPlayerInfoMgr.ShowApperance = false
LuaPlayerInfoMgr.PlayerApperanceProp = nil
function LuaPlayerInfoMgr:ShowPlayerApperanceInfo(id,name,class,gender,apperanceProp)
    self.PlayerApperanceProp = {}
    self.PlayerApperanceProp.id = id
    self.PlayerApperanceProp.name = name
    self.PlayerApperanceProp.cls = class
    self.PlayerApperanceProp.gender = gender
    self.PlayerApperanceProp.appearanceProp = apperanceProp
    self.ShowApperance = true
    if IsOpenNewPlayerAppearance() then
        CUIManager.ShowUI(CLuaUIResources.NewPlayerAppearanceInfoWnd)
    else
        CUIManager.ShowUI(CUIResources.PlayerAppearanceInfoWnd)
    end
end