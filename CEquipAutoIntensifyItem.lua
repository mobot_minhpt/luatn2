-- Auto Generated!!
local CEquipAutoIntensifyItem = import "L10.UI.CEquipAutoIntensifyItem"
local CEquipmentProcessMgr = import "L10.Game.CEquipmentProcessMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local EquipIntensify_Intensify = import "L10.Game.EquipIntensify_Intensify"
local LocalString = import "LocalString"
CEquipAutoIntensifyItem.m_UpdateData_CS2LuaHook = function (this, m_Info) 
    this.m_Info = m_Info
    local equip = CItemMgr.Inst:GetById(CEquipmentProcessMgr.Inst.SelectEquipment.itemId)

    local config = EquipIntensify_Intensify.GetData(m_Info.Level)


    this.LevelLabel.text = System.String.Format(LocalString.GetString("{0}级"), m_Info.Level)

    this.m_MinGoal = 0
    if m_Info.addValue > 0 then
        do
            local i = 0
            while i < config.Range.Length do
                if config.Range[i] > m_Info.addValue then
                    this.m_MinGoal = config.Range[i]
                    break
                end
                i = i + 1
            end
        end
        if this.m_MinGoal == 0 then
            this.m_MinGoal = config.Range[0]
        end
    else
        this.m_MinGoal = config.Range[0]
    end
    this.slider:SetMinMax(config.Range[0], config.Range[config.Range.Length - 1])
    local goal = math.ceil((math.max(10, equip.Equip:GetIntensifyValue(config.Level) * 5) / 10 + config.Range[config.Range.Length - 1] * 0.5))


    --AutoIntensifyGoalButton.SetValue(System.Math.Max(goal, m_MinGoal));

    local addValueStr = "?"
    if m_Info.addValue > 0 then
        addValueStr = tostring(m_Info.addValue)

        if m_Info.addValue == m_Info.minAddValue then
            this.slider:SetMinValue()
        else
            this.slider:SetValue(m_Info.addValue)
        end

        this.slider.foregroundWidget.alpha = 1
        --slider.HideThumb();
    else
        --slider.ShowThumb();
        --slider.SetValue(m_Info.maxAddValue);
        this.slider.valueLabel.text = System.String.Format(LocalString.GetString("{0}% 未强化"), m_Info.maxAddValue)
        this.slider.foregroundWidget.alpha = 0.2
    end


    this:UpdateAppearance()
    this:UpdateColor()
end
