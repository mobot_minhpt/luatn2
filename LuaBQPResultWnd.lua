local LuaGameObject=import "LuaGameObject"
local CButton = import "L10.UI.CButton"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"
local EquipmentTemplate_Equip=import "L10.Game.EquipmentTemplate_Equip"

LuaBQPResultWnd=class()

RegistClassMember(LuaBQPResultWnd,"TimeSelectBtn")
RegistClassMember(LuaBQPResultWnd,"TypeSelectBtn")
RegistClassMember(LuaBQPResultWnd,"SubtypeSelectBtn")

RegistClassMember(LuaBQPResultWnd,"TableViewDataSource")
RegistClassMember(LuaBQPResultWnd,"TableView")

RegistClassMember(LuaBQPResultWnd,"m_ResultInfo")
RegistClassMember(LuaBQPResultWnd,"m_SelectedTime")
RegistClassMember(LuaBQPResultWnd,"m_SelectedType")
RegistClassMember(LuaBQPResultWnd,"m_SelectedSubtype")
RegistClassMember(LuaBQPResultWnd,"m_MenuList1")
RegistClassMember(LuaBQPResultWnd,"m_MenuList2")
RegistClassMember(LuaBQPResultWnd,"m_MenuList3")
RegistClassMember(LuaBQPResultWnd,"m_EquipTypes")
RegistClassMember(LuaBQPResultWnd,"m_WeaponTypes")



function LuaBQPResultWnd:CreateMenu(menulist,callback,menus)
	for i=1,#menus do
		local item = PopupMenuItemData(menus[i], DelegateFactory.Action_int(callback), false, nil, EnumPopupMenuItemStyle.Light)
		CommonDefs.ListAdd(menulist, typeof(PopupMenuItemData), item)
	end
end

function LuaBQPResultWnd:Init()

	self.m_SelectedTime = 0--当前届，1或者2
    self.m_SelectedType = 0--不是武器大类，只是index
    self.m_SelectedSubtype = 0--武器小类
	self.m_ResultInfo = {}

	self.TimeSelectBtn = self.transform:Find("ShowArea/SelectionRoot/TimeSelectBtn"):GetComponent(typeof(CButton))
	self.TypeSelectBtn = self.transform:Find("ShowArea/SelectionRoot/TypeSelectBtn"):GetComponent(typeof(CButton))
	self.SubtypeSelectBtn = self.transform:Find("ShowArea/SelectionRoot/SubtypeSelectBtn"):GetComponent(typeof(CButton))
	self.TableView = self.transform:Find("ShowArea/Content/TableView"):GetComponent(typeof(QnTableView))

	-- 第几届
	local onTime = function (go)
		self:OnTimeSelectClick(go)
	end
	CommonDefs.AddOnClickListener(self.TimeSelectBtn.gameObject,DelegateFactory.Action_GameObject(onTime),false)
	

	-- 大类别
	local onType = function (go)
		self:OnTypeSelectClick(go)
	end
	CommonDefs.AddOnClickListener(self.TypeSelectBtn.gameObject,DelegateFactory.Action_GameObject(onType),false)
	

	-- 小类别
	local onSubtype = function (go)
		self:OnSubtypeSelectionClick(go)
	end
	CommonDefs.AddOnClickListener(self.SubtypeSelectBtn.gameObject,DelegateFactory.Action_GameObject(onSubtype),false)

	self.m_MenuList1 = CreateFromClass(MakeGenericClass(List, PopupMenuItemData))
	self.m_MenuList2 = CreateFromClass(MakeGenericClass(List, PopupMenuItemData))
	self.m_MenuList3 = CreateFromClass(MakeGenericClass(List, PopupMenuItemData))

	local cb = function (idx) 
		self:OnSelectedTime(idx)
	end
	self:CreateMenu(self.m_MenuList1,cb,{LocalString.GetString("第1届"),LocalString.GetString("第2届")})

	
	local cb2 = function (idx) 
		self:OnSelectedType(idx)
	end
	local cb3 = function (idx)
		self:OnSelectedSubtype(idx)
	end

	self.m_EquipTypes={}
	table.insert(self.m_EquipTypes,1)--武器
	self.m_WeaponTypes={0}
	local menu2 = {LocalString.GetString("武器")}
	local menu3 = {LocalString.GetString("全部类型")}
	BingQiPu_EquipType.Foreach(function (key,value)
		if value.FstTab ~= 1 then
			table.insert(self.m_EquipTypes,value.FstTab)--其他大类
			table.insert(menu2,value.SecTabName)
		else
			table.insert(self.m_WeaponTypes,value.SecTab)--小类
			table.insert(menu3,value.SecTabName)
		end
	end)
	self:CreateMenu(self.m_MenuList2,cb2,menu2)
	self:CreateMenu(self.m_MenuList3,cb3,menu3)


	local function initItem(item,index)
		if index % 2 == 1 then
            item:SetBackgroundTexture("common_bg_mission_background_n")
        else
            item:SetBackgroundTexture("common_bg_mission_background_s")
        end
    	self:InitItem(item, index)
    end
    self.TableViewDataSource=DefaultTableViewDataSource.CreateByCount(#self.m_ResultInfo,initItem)
    self.TableView.m_DataSource=self.TableViewDataSource
    self.TableView.OnSelectAtRow=DelegateFactory.Action_int(function (row)
        self:OnSelectAtRow(row)
    end)
    
    self:OnSelectedTime(1)--默认第二届
    self:OnSelectedType(0)
end


function LuaBQPResultWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateBQPResultList", self, "UpdateBQPResultList")
end

function LuaBQPResultWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateBQPResultList", self, "UpdateBQPResultList")
end

function LuaBQPResultWnd:UpdateBQPResultList(infos)
	self.m_ResultInfo = infos
	self.TableViewDataSource.count=#self.m_ResultInfo
	self.TableView:ReloadData(true,false)
end

function LuaBQPResultWnd:InitItem(item, index)
	local info = self.m_ResultInfo[index + 1]
	local tf=item.transform

	local rankLabel=LuaGameObject.GetChildNoGC(tf,"RankLabel").label
    rankLabel.text=""
    local rankSprite=LuaGameObject.GetChildNoGC(tf,"RankImage").sprite
    rankSprite.spriteName=""

    local equipNameLabel=LuaGameObject.GetChildNoGC(tf,"EquipNameLabel").label
    equipNameLabel.text=""
    local ownerLabel=LuaGameObject.GetChildNoGC(tf,"OwnerNameLabel").label
    ownerLabel.text=""
    local scoreLabel=LuaGameObject.GetChildNoGC(tf,"ScoreLabel").label
    scoreLabel.text=""

    local rank=info.rankPos
    if rank==1 then
        rankSprite.spriteName="Rank_No.1"
    elseif rank==2 then
        rankSprite.spriteName="Rank_No.2png"
    elseif rank==3 then
        rankSprite.spriteName="Rank_No.3png"
    else
        rankLabel.text=tostring(rank)
    end

	if String.IsNullOrEmpty(info.equipName) then
    	local template = EquipmentTemplate_Equip.GetData(info.templateId)
    	equipNameLabel.text = template.Name
	else
		equipNameLabel.text = info.equipName
	end
    ownerLabel.text = info.ownerName
    scoreLabel.text = tostring(info.praise)

end

function LuaBQPResultWnd:OnSelectAtRow(row)
	local info = self.m_ResultInfo[row+1]
	Gac2Gas.QueryBingQiPuHistoryDetail(self.m_SelectedTime, info.historyId)
end

function LuaBQPResultWnd:OnTimeSelectClick(go)
	local menus = CommonDefs.ListToArray(self.m_MenuList1)
	CPopupMenuInfoMgr.ShowPopupMenu(menus, go.transform, CPopupMenuInfoMgr.AlignType.Bottom, 1, nil, 350, true, 330)
end

function LuaBQPResultWnd:OnTypeSelectClick(go)
	local menus = CommonDefs.ListToArray(self.m_MenuList2)
	CPopupMenuInfoMgr.ShowPopupMenu(menus, go.transform, CPopupMenuInfoMgr.AlignType.Bottom, 1, nil, 350, true, 330)
end


function LuaBQPResultWnd:OnSubtypeSelectionClick(go)
	local menus = CommonDefs.ListToArray(self.m_MenuList3)
	CPopupMenuInfoMgr.ShowPopupMenu(menus, go.transform, CPopupMenuInfoMgr.AlignType.Bottom, 2, nil, 350, true, 330)
end

-- 选择第几届
function LuaBQPResultWnd:OnSelectedTime(idx)
	if self.m_SelectedTime == idx+1 then return end
	self.TableView:Clear()
	self.m_SelectedTime = idx + 1
	self.TimeSelectBtn.Text = self.m_MenuList1[idx].text
	self:RequireData()
end

-- 选择大类
function LuaBQPResultWnd:OnSelectedType(idx)
	if self.m_SelectedType == idx + 1 then return end
	self.m_SelectedType = idx + 1
	self.TypeSelectBtn.Text = self.m_MenuList2[idx].text
	
	self.SubtypeSelectBtn.gameObject:SetActive(self.m_SelectedType == 1)
	if self.m_SelectedType == 1 then --武器榜
		self.m_SelectedSubtype = -1
		self:OnSelectedSubtype(0)
	else
		self.TableView:Clear()
		self:RequireData()
	end
end

-- 选择小类
function LuaBQPResultWnd:OnSelectedSubtype(idx)
	local wtype = self.m_WeaponTypes[idx+1]
	if wtype == self.m_SelectedSubtype then return end
	self.TableView:Clear()
	self.m_SelectedSubtype = wtype
	self.SubtypeSelectBtn.Text = self.m_MenuList3[idx].text
	self:RequireData()
end

function LuaBQPResultWnd:RequireData()
	local selectIndex = self.m_SelectedType
	local subtype = self.m_SelectedSubtype
	if selectIndex == nil or subtype == nil then 
		return
	end

	if selectIndex <= 0 or subtype < 0 then 
		return
	end

	if self.m_SelectedTime == 0 then 
		self.m_SelectedTime = 2
	end
	
	if subtype > 0 or selectIndex > 1 then--非全部武器
		local equipType = self.m_EquipTypes[selectIndex]
		local id = LuaBingQiPuMgr.GetBQPEquipTypeWithEquipType(equipType,subtype)
		if id == 0 then return end
		Gac2Gas.QueryBQPHistory(self.m_SelectedTime, id, false)
	else
		Gac2Gas.QueryBQPHistory(self.m_SelectedTime, 0, false)
	end

end
