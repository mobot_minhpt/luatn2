require("common/common_include")
require("ui/lingshou/LuaLingShouBabySkillSlot")
require("ui/lingshou/LuaLingShouMgr")

local LuaGameObject=import "LuaGameObject"
local LingShouBaby_BabyTemplate=import "L10.Game.LingShouBaby_BabyTemplate"

CLuaLingShouOtherBabyWnd=class()
RegistClassMember(CLuaLingShouOtherBabyWnd,"m_NameLabel")
RegistClassMember(CLuaLingShouOtherBabyWnd,"m_LevelLabel")

RegistClassMember(CLuaLingShouOtherBabyWnd,"m_ModelTexture")
RegistClassMember(CLuaLingShouOtherBabyWnd,"m_FatherTexture")
RegistClassMember(CLuaLingShouOtherBabyWnd,"m_MotherTexture")
RegistClassMember(CLuaLingShouOtherBabyWnd,"m_PinzhiSprite")
RegistClassMember(CLuaLingShouOtherBabyWnd,"m_ZhandouliLabel")
RegistClassMember(CLuaLingShouOtherBabyWnd,"m_SkillSlot1")
RegistClassMember(CLuaLingShouOtherBabyWnd,"m_SkillSlot2")

RegistClassMember(CLuaLingShouOtherBabyWnd,"m_FatherTemplateId")
RegistClassMember(CLuaLingShouOtherBabyWnd,"m_MotherTemplateId")
RegistClassMember(CLuaLingShouOtherBabyWnd,"m_AdjHpLabel")

function CLuaLingShouOtherBabyWnd:Init()
    local g = LuaGameObject.GetChildNoGC(self.transform,"CloseButton").gameObject
    UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.CloseUI(CUIResources.LingShouOtherBabyWnd)
    end)

    self.m_ModelTexture=LuaGameObject.GetChildNoGC(self.transform,"ModelTexture").lingShouBabyTextureLoader
    self.m_FatherTexture=LuaGameObject.GetChildNoGC(self.transform,"FatherModelTexture").lingShouTextureLoader
    self.m_MotherTexture=LuaGameObject.GetChildNoGC(self.transform,"MotherModelTexture").lingShouTextureLoader
    --战斗力
    self.m_ZhandouliLabel=LuaGameObject.GetChildNoGC(self.transform,"ZhandouliLabel").label
    self.m_ZhandouliLabel.text=" "
    --品质
    self.m_PinzhiSprite=LuaGameObject.GetChildNoGC(self.transform,"PinZhiSprite").sprite
    self.m_SkillSlot1=CLuaLingShouBabySkillSlot:new()
    self.m_SkillSlot1:Init(LuaGameObject.GetChildNoGC(self.transform,"Skill1").transform)
    self.m_SkillSlot2=CLuaLingShouBabySkillSlot:new()
    self.m_SkillSlot2:Init(LuaGameObject.GetChildNoGC(self.transform,"Skill2").transform)

    self.m_NameLabel=LuaGameObject.GetChildNoGC(self.transform,"NameLabel").label
    self.m_LevelLabel=LuaGameObject.GetChildNoGC(self.transform,"LevelLabel").label

    self.m_AdjHpLabel=LuaGameObject.GetChildNoGC(self.transform,"AdjHpLabel").label
    
    self:InitBabyInfo()

end

function CLuaLingShouOtherBabyWnd:InitBabyInfo()
    local details=CLuaLingShouMgr.m_TempLingShouInfo
    if details~=nil then
        local marryInfo=details.data.Props.MarryInfo

        if marryInfo.IsFather>0 then
            self.m_FatherTemplateId=details.data.TemplateId
            self.m_MotherTemplateId=marryInfo.PartnerTemplateId
        else
            self.m_FatherTemplateId=marryInfo.PartnerTemplateId
            self.m_MotherTemplateId=details.data.TemplateId
        end

        local babyInfo = details.data.Props.Baby

        if babyInfo.BornTime>0 then
            local templateData=LingShouBaby_BabyTemplate.GetData(babyInfo.TemplateId)
            if templateData then
                self.m_NameLabel.text=templateData.Name
            end
            self.m_LevelLabel.text=SafeStringFormat3( "Lv.%d", babyInfo.Level )
            self.m_ModelTexture:Init(babyInfo.TemplateId)
            self.m_MotherTexture:Init(self.m_MotherTemplateId,0,0)
            self.m_FatherTexture:Init(self.m_FatherTemplateId,0,0)
            self.m_ZhandouliLabel.text=tostring(CLuaLingShouMgr.BabyZhandouliFormula(babyInfo.Level,babyInfo.Quality,babyInfo.SkillList))
            
            local skillId1=CLuaLingShouMgr.ProcessSkillId(babyInfo.Quality,babyInfo.SkillList[1])
            local skillId2=CLuaLingShouMgr.ProcessSkillId(babyInfo.Quality,babyInfo.SkillList[2])
            self.m_SkillSlot1:InitData(skillId1)
            self.m_SkillSlot2:InitData(skillId2)

            self.m_PinzhiSprite.spriteName=CLuaLingShouMgr.GetBabyQualitySprite(babyInfo.Quality)
        end

        local adjhp=CLuaLingShouMgr.GetLSBBAdjHp(babyInfo.Level,babyInfo.Quality)
        self.m_AdjHpLabel.text=LocalString.GetString("父母气血").."+"..tostring(adjhp)
    end
end

function CLuaLingShouOtherBabyWnd:OnEnable()
    --接到灵兽宝宝信息数据
    -- g_ScriptEvent:AddListener("MainPlayerCreated", self, "OnUpdateExp")
end

function CLuaLingShouOtherBabyWnd:OnDisable()
    -- g_ScriptEvent:RemoveListener("MainPlayerPlayPropUpdate", self, "OnUpdateExp")
end

return CLuaLingShouOtherBabyWnd
