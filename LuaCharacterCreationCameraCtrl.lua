require("common/common_include")

local GameObject = import "UnityEngine.GameObject"
local Camera = import "UnityEngine.Camera"
local DepthTextureMode = import "UnityEngine.DepthTextureMode"
local Vector3 = import "UnityEngine.Vector3"
local Quaternion = import "UnityEngine.Quaternion"
local Time = import "UnityEngine.Time"
local Mathf = import "UnityEngine.Mathf"
local GestureRecognizer = import "L10.Engine.GestureRecognizer"
local GestureType = import "L10.Engine.GestureType"
local LoginModelRoot = import "L10.UI.LoginModelRoot"
local Shader = import "UnityEngine.Shader"
local CLoginCamera = import "L10.Engine.CLoginCamera"
local DelegateFactory = import "DelegateFactory"
local CommonDefs = import "L10.Game.CommonDefs"

CLuaCharacterCreationCameraCtrl = class()

RegistClassMember(CLuaCharacterCreationCameraCtrl, "m_Camera")
RegistClassMember(CLuaCharacterCreationCameraCtrl, "m_LoginCamera")
RegistClassMember(CLuaCharacterCreationCameraCtrl, "m_ModelRootScaleY")
RegistClassMember(CLuaCharacterCreationCameraCtrl, "m_FollowRO")

RegistClassMember(CLuaCharacterCreationCameraCtrl, "m_DelegatePinchIn")
RegistClassMember(CLuaCharacterCreationCameraCtrl, "m_DelegatePinchOut")

RegistClassMember(CLuaCharacterCreationCameraCtrl, "m_RZY")
RegistClassMember(CLuaCharacterCreationCameraCtrl, "m_DeltaHeightFactor")
RegistClassMember(CLuaCharacterCreationCameraCtrl, "m_DeltaHeight")
RegistClassMember(CLuaCharacterCreationCameraCtrl, "m_CameraFieldOfView")
RegistClassMember(CLuaCharacterCreationCameraCtrl, "m_T")
RegistClassMember(CLuaCharacterCreationCameraCtrl, "m_EnablePinch")

RegistChildComponent(CLuaCharacterCreationCameraCtrl, "m_NearestRZY", Transform)
RegistChildComponent(CLuaCharacterCreationCameraCtrl, "m_FarthestRZY", Transform)
RegistChildComponent(CLuaCharacterCreationCameraCtrl, "m_TargetRZY", Transform)
RegistChildComponent(CLuaCharacterCreationCameraCtrl, "m_DeltaHeightParam", Transform)
RegistChildComponent(CLuaCharacterCreationCameraCtrl, "m_Speed", Transform)
RegistChildComponent(CLuaCharacterCreationCameraCtrl, "m_FieldOfViewParam", Transform)

function CLuaCharacterCreationCameraCtrl:Awake()
	local cameraObj = GameObject.Find("SceneRoot/Camera")
	self.m_Camera = cameraObj:GetComponent(typeof(Camera))
	self.m_Camera.enabled = true
	self.m_Camera.allowHDR = false
	self.m_LoginCamera = cameraObj:GetComponent(typeof(CLoginCamera))
	if self.m_LoginCamera == nil then
		self.m_LoginCamera = cameraObj:AddComponent(typeof(CLoginCamera))
	end

	self.m_ModelRootScaleY = (LoginModelRoot.Inst ~= nil) and LoginModelRoot.Inst.transform.localScale.y or 1.0
	self.m_DeltaHeightFactor = self.m_ModelRootScaleY * self.m_DeltaHeightParam.localScale.y

	-- 初始在Farthest
	local farthestRZY = self.m_FarthestRZY.localPosition
	self.m_RZY = Vector3(farthestRZY.x, farthestRZY.y, farthestRZY.z)
	self.m_DeltaHeight = 0
	self.m_CameraFieldOfView = self.m_Camera.fieldOfView or self.m_FieldOfViewParam.localPosition.y

	self.m_T = 1
	self:UpdateTargetRZY()
	self.m_EnablePinch = false
end

function CLuaCharacterCreationCameraCtrl:SetFollowRO(followRO)
	self.m_FollowRO = followRO
end

function CLuaCharacterCreationCameraCtrl:SetDeltaHeightFactor(deltaHeightFactor)
	self.m_DeltaHeightFactor = self.m_ModelRootScaleY * deltaHeightFactor
end

function CLuaCharacterCreationCameraCtrl:UpdateEyeAnchor(leftOffsetX, leftOffsetY, leftOffsetZ, rightOffsetX, rightOffsetY, rightOffsetZ)
	self.m_LoginCamera.rightEyeAnchor.transform.localPosition = Vector3(rightOffsetX, rightOffsetY, rightOffsetZ)
	self.m_LoginCamera.leftEyeAnchor.transform.localPosition = Vector3(leftOffsetX, leftOffsetY, leftOffsetZ)
end

function CLuaCharacterCreationCameraCtrl:OnEnable()
	self.m_DelegatePinchIn = DelegateFactory.Action_GenericGesture(function(gesture) self:OnPinchIn(gesture) end)
	self.m_DelegatePinchOut = DelegateFactory.Action_GenericGesture(function(gesture) self:OnPinchOut(gesture) end)
	GestureRecognizer.AddGestureListener(GestureType.OnPinchIn, self.m_DelegatePinchIn)
	GestureRecognizer.AddGestureListener(GestureType.OnPinchOut, self.m_DelegatePinchOut)
end

function CLuaCharacterCreationCameraCtrl:OnDisable()
	GestureRecognizer.RemoveGestureListener(GestureType.OnPinchIn, self.m_DelegatePinchIn)
	GestureRecognizer.RemoveGestureListener(GestureType.OnPinchOut, self.m_DelegatePinchOut)
end

function CLuaCharacterCreationCameraCtrl:IsEnablePinch()
	if not self.m_EnablePinch then
		return
	end

	return true
end

function CLuaCharacterCreationCameraCtrl:OnPinchIn(gesture)
	if not self:IsEnablePinch() then
		return
	end
	
	self.m_T = math.min(math.max(self.m_T + gesture.deltaPinch / 400, -1), 1)
	self:UpdateTargetRZY()
end

function CLuaCharacterCreationCameraCtrl:OnPinchOut(gesture)
	if not self:IsEnablePinch() then
		return
	end
	
	self.m_T = math.min(math.max(self.m_T - gesture.deltaPinch / 400, -1), 1)
	self:UpdateTargetRZY()
end

function CLuaCharacterCreationCameraCtrl:UpdateTargetRZY()
	-- 摄像机距离变化时，调整飞升材质的高光反射强度
	Shader.SetGlobalFloat("_CameraDist", self.m_T)

	local nearestRZY = self.m_NearestRZY.localPosition
	local farthestRZY = self.m_FarthestRZY.localPosition
	local ratio = (self.m_T + 1) / 2
	local far = CommonDefs.op_Multiply_Single_Vector3(ratio, farthestRZY)
	local near = CommonDefs.op_Multiply_Single_Vector3(1 - ratio, nearestRZY)
	local targetRZY = CommonDefs.op_Addition_Vector3_Vector3(far, near)
	targetRZY.x = math.min(math.max(targetRZY.x, nearestRZY.x), farthestRZY.x)
	targetRZY.y = math.min(math.max(targetRZY.y, nearestRZY.y), farthestRZY.y)
	targetRZY.z = math.min(math.max(targetRZY.z, nearestRZY.z), farthestRZY.z)
	self.m_TargetRZY.localPosition = targetRZY

	-- 同时更新camera
	self.m_LoginCamera.targetRZY = targetRZY
end

function CLuaCharacterCreationCameraCtrl:ForwardToFace()
	self.m_T = -1
	self:UpdateTargetRZY()
	self.m_EnablePinch = false
end

function CLuaCharacterCreationCameraCtrl:BackwardToBody()
	self.m_T = 1
	self:UpdateTargetRZY()
	self.m_EnablePinch = false
end

function CLuaCharacterCreationCameraCtrl:Update()
	local targetRZY = self.m_TargetRZY.localPosition
	local rzyDist = Vector3.Distance(self.m_RZY, targetRZY)
	local rzySpeed = self.m_Speed.localPosition.z
	local rzyTimeReverse = rzySpeed / rzyDist
	if rzyDist > 0.01 then
		self.m_RZY.x = self.m_RZY.x % 360
		targetRZY.x = targetRZY.x % 360
		self.m_RZY = Vector3.MoveTowards(self.m_RZY, targetRZY, rzySpeed * Time.deltaTime)
	end

	local ratio = (self.m_T - 1) / 2
	local targetDeltaHeight = ratio * self.m_DeltaHeightFactor
	local heightDist = math.abs(targetDeltaHeight - self.m_DeltaHeight)
	if heightDist > 0.01 then
		local hSpeed = math.min(math.max(rzyDist > 0.01 and (heightDist * rzyTimeReverse) or self.m_Speed.localPosition.y, 0.5), self.m_Speed.localPosition.y)
		self.m_DeltaHeight = Mathf.MoveTowards(self.m_DeltaHeight, targetDeltaHeight, hSpeed * Time.deltaTime)
	end

	local farthestFov = self.m_FieldOfViewParam.localPosition.y
	local nearestFov = self.m_FieldOfViewParam.localPosition.x
	local targetFieldOfView = farthestFov + ratio * (farthestFov - nearestFov)
	local deltaFieldOfView = math.abs(targetFieldOfView - self.m_CameraFieldOfView)
	if deltaFieldOfView > 0.01 then
		local fSpeed = math.min(math.max(rzyDist > 0.01 and (deltaFieldOfView * rzyTimeReverse) or self.m_Speed.localPosition.x, 10), self.m_Speed.localPosition.x)
		self.m_CameraFieldOfView = Mathf.MoveTowards(self.m_CameraFieldOfView, targetFieldOfView, fSpeed * Time.deltaTime)
	end

	if self.m_FollowRO ~= nil then
		local roPos = self.m_FollowRO.transform.position
		local lookAtPos = Vector3(roPos.x, roPos.y, roPos.z)
		lookAtPos.y = lookAtPos.y + self.m_ModelRootScaleY * self.m_DeltaHeightParam.localPosition.y
		lookAtPos.y = lookAtPos.y + self.m_DeltaHeight

		local euler = Quaternion.Euler(0, self.m_RZY.x, 0)
		local dir = Vector3(0, self.m_RZY.z, self.m_RZY.y)
		local delta = CommonDefs.op_Multiply_Quaternion_Vector3(euler, dir)
		local pos = CommonDefs.op_Addition_Vector3_Vector3(lookAtPos, delta)
		if self.m_Camera and self.m_Camera.transform ~= nil then
			self.m_Camera.transform.position = pos
			self.m_Camera.transform:LookAt(lookAtPos)
			if math.abs(self.m_Camera.fieldOfView - self.m_CameraFieldOfView) > 0.01 then
				self.m_Camera.fieldOfView = self.m_CameraFieldOfView
			end
		end
	end
end

return CLuaCharacterCreationCameraCtrl
