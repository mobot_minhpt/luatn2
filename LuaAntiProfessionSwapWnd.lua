local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnTableView = import "L10.UI.QnTableView"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local UILabel = import "UILabel"
local QnButton = import "L10.UI.QnButton"
local CUITexture = import "L10.UI.CUITexture"
local Profession = import "L10.Game.Profession"
local EnumClass = import "L10.Game.EnumClass"
local MsgPackImpl = import "MsgPackImpl"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaAntiProfessionSwapWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaAntiProfessionSwapWnd, "table", "table", QnTableView)
RegistChildComponent(LuaAntiProfessionSwapWnd, "OriAnti", "OriAnti", QnButton)
RegistChildComponent(LuaAntiProfessionSwapWnd, "TarAnti", "TarAnti", QnButton)
RegistChildComponent(LuaAntiProfessionSwapWnd, "QnCostAndOwnMoney", "QnCostAndOwnMoney", CCurentMoneyCtrl)
RegistChildComponent(LuaAntiProfessionSwapWnd, "FreeTip", "FreeTip", UILabel)
RegistChildComponent(LuaAntiProfessionSwapWnd, "TimeLab", "TimeLab", UILabel)
RegistChildComponent(LuaAntiProfessionSwapWnd, "OpBtn", "OpBtn", QnButton)
RegistChildComponent(LuaAntiProfessionSwapWnd, "TipBtn", "TipBtn", QnButton)

--@endregion RegistChildComponent end

RegistClassMember(LuaAntiProfessionSwapWnd, "m_SelectedSlot")
RegistClassMember(LuaAntiProfessionSwapWnd, "m_OriAntiSlot")
RegistClassMember(LuaAntiProfessionSwapWnd, "m_TarAntiSlot")
RegistClassMember(LuaAntiProfessionSwapWnd, "m_OriAntiIndex")
RegistClassMember(LuaAntiProfessionSwapWnd, "m_TarAntiIndex")
RegistClassMember(LuaAntiProfessionSwapWnd, "m_Infos")

function LuaAntiProfessionSwapWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.m_SelectedSlot = 1
    -- m_OriAntiSlot 1
    -- m_TarAntiSlot 2
end

function LuaAntiProfessionSwapWnd:Init()
    self.QnCostAndOwnMoney:SetType(EnumMoneyType_lua.YuanBao, 0, false)
    self.OpBtn.OnClick = DelegateFactory.Action_QnButton(function (btn)
        if self.m_OriAntiSlot ~= nil and self.m_TarAntiSlot ~= nil and self.m_OriAntiIndex ~= nil and self.m_TarAntiIndex ~= nil then
            Gac2Gas.AntiProfession_Transfer(self.m_OriAntiIndex, self.m_TarAntiIndex)
            self:ClearSlot()
            self:InitIcons()
        end
    end)

    self.TipBtn.OnClick = DelegateFactory.Action_QnButton(function (btn)
        g_MessageMgr:ShowMessage("ANTIPROFESSION_SWAPVIEW_TIP")
    end)

    self.OriAnti.OnClick = DelegateFactory.Action_QnButton(function (btn)
        self:OnClickSlot(1)
    end)

    self.TarAnti.OnClick = DelegateFactory.Action_QnButton(function (btn)
        self:OnClickSlot(2)
    end)

    self:RefreshCost()

    self:ClearSlot()
    self:InitIcons()
end

--@region UIEvent

--@endregion UIEvent

function LuaAntiProfessionSwapWnd:OnEnable()
    g_ScriptEvent:AddListener("AntiProfessionUpdateSkill", self, "RefreshIcons")
    g_ScriptEvent:AddListener("AntiProfessionUpdateScore", self, "OnAntiProfessionUpdateScore")
    g_ScriptEvent:AddListener("TransferAntiProfession", self, "RefreshCost")
end

function LuaAntiProfessionSwapWnd:OnDisable()
    g_ScriptEvent:RemoveListener("AntiProfessionUpdateSkill", self, "RefreshIcons")
    g_ScriptEvent:RemoveListener("AntiProfessionUpdateScore", self, "OnAntiProfessionUpdateScore")
    g_ScriptEvent:RemoveListener("TransferAntiProfession", self, "RefreshCost")
end

function LuaAntiProfessionSwapWnd:OnAntiProfessionUpdateScore(profession, score)
    self:InitSlotInfo(self.OriAnti.gameObject, self.m_OriAntiSlot, self.m_SelectedSlot == 1)
    self:InitSlotInfo(self.TarAnti.gameObject, self.m_TarAntiSlot, self.m_SelectedSlot == 2)
end

function LuaAntiProfessionSwapWnd:RefreshCost()
    local lastTransferData = CommonDefs.DictGetValue(CClientMainPlayer.Inst.PlayProp.PersistPlayData, typeof(UInt32), EnumPersistPlayDataKey_lua.eTransferTime)
    local lastTransferTime = lastTransferData and MsgPackImpl.unpack(lastTransferData.Data) or nil

    local transferTimes = 0
    local remainTime = 0
    if lastTransferTime then
        local freezeDuration = tonumber(ProfessionTransfer_Setting.GetData().TransferInterval) * 24 * 3600
        local offset = freezeDuration - (CServerTimeMgr.Inst.timeStamp - lastTransferTime)
        if offset > 0 then
            local timesData = CommonDefs.DictGetValue(CClientMainPlayer.Inst.PlayProp.PersistPlayData, typeof(UInt32), EnumPersistPlayDataKey_lua.eTransferAntiProfession)
            transferTimes = timesData and MsgPackImpl.unpack(timesData.Data) or nil
            transferTimes = transferTimes and tonumber(transferTimes) or 0
            remainTime = offset
        end
    end

    if remainTime > 0 and transferTimes == 0 then
        self.FreeTip.gameObject:SetActive(true)
        self.QnCostAndOwnMoney.gameObject:SetActive(false)
        self.FreeTip.text = SafeStringFormat3(LocalString.GetString("当前拥有%d次免费转移机会"), 1)
        if remainTime > 24 * 3600 then
            self.TimeLab.text = SafeStringFormat3(LocalString.GetString("%d天"), math.floor(remainTime / 24 / 3600))
        else
            self.TimeLab.text = SafeStringFormat3(LocalString.GetString("%d小时"), math.floor(remainTime / 3600))
        end
    else
        self.FreeTip.gameObject:SetActive(false)
        if self.m_OriAntiSlot ~= nil and self.m_TarAntiSlot ~= nil and self.m_OriAntiIndex ~= nil and self.m_TarAntiIndex ~= nil then
            self.QnCostAndOwnMoney.gameObject:SetActive(true)
            self.QnCostAndOwnMoney:SetCost(self:CalTransferYuanBao(self.m_OriAntiIndex, self.m_TarAntiIndex, self.m_OriAntiSlot.CurLevel, self.m_TarAntiSlot.CurLevel))
        else
            self.QnCostAndOwnMoney.gameObject:SetActive(false)
        end
    end
end

function LuaAntiProfessionSwapWnd:ClearSlot()
    self.m_SelectedSlot = 1
    self.m_OriAntiSlot = nil
    self.m_TarAntiSlot = nil
    self.m_OriAntiIndex = nil
    self.m_TarAntiIndex = nil
    self:InitSlotInfo(self.OriAnti.gameObject, nil, false)
    self:InitSlotInfo(self.TarAnti.gameObject, nil, false)
    self.table:ReloadData(true, false)

end

function LuaAntiProfessionSwapWnd:OnClickSlot(SlotId)
    self.m_SelectedSlot = SlotId

    self:InitSlotInfo(self.OriAnti.gameObject, self.m_OriAntiSlot, self.m_SelectedSlot == 1)
    self:InitSlotInfo(self.TarAnti.gameObject, self.m_TarAntiSlot, self.m_SelectedSlot == 2)
end

function LuaAntiProfessionSwapWnd:SetSlot(index, info)
    local profession = info.Profession
    if (self.m_OriAntiSlot or {}).Profession == info.Profession or (self.m_TarAntiSlot or {}).Profession == info.Profession then
        return
    end

    if self.m_SelectedSlot == 1 then
        if self.m_TarAntiSlot == nil then
            self.m_SelectedSlot = 2
        end
        self.m_OriAntiSlot = info
        self.m_OriAntiIndex = index
        self:InitSlotInfo(self.OriAnti.gameObject, info, self.m_SelectedSlot == 1)
        self:InitSlotInfo(self.TarAnti.gameObject, self.m_TarAntiSlot, self.m_SelectedSlot == 2)
    elseif self.m_SelectedSlot == 2 then
        if self.m_OriAntiSlot == nil then
            self.m_SelectedSlot = 1
        end
        self.m_TarAntiSlot = info
        self.m_TarAntiIndex = index
        self:InitSlotInfo(self.TarAnti.gameObject, info, self.m_SelectedSlot == 2)
        self:InitSlotInfo(self.OriAnti.gameObject, self.m_OriAntiSlot, self.m_SelectedSlot == 1)
    end

    self:RefreshCost()
end

function LuaAntiProfessionSwapWnd:InitSlotInfo(slot, info, selected)
    local selectedBg= slot.transform:Find("SelectedBg").gameObject
    local icon      = slot.transform:Find("Target/Icon"):GetComponent(typeof(CUITexture))
    local nameLab   = slot.transform:Find("Target/NameLab"):GetComponent(typeof(UILabel))
    local lvLab     = slot.transform:Find("Target/LvLab"):GetComponent(typeof(UILabel))
    local score     = slot.transform:Find("Score").gameObject
    selectedBg:SetActive(selected)

    if info ~= nil then
        local profession= info.Profession
        local lv = info.CurLevel
        local proClass = CommonDefs.ConvertIntToEnum(typeof(EnumClass), profession)
        local proName = Profession.GetFullName(proClass)
        icon.gameObject:SetActive(true)
        icon:LoadMaterial(Profession.GetLargeIcon(proClass))
        nameLab.text = SafeStringFormat3(LocalString.GetString("克制%s"), proName)
        lvLab.gameObject:SetActive(true)
        lvLab.text = SafeStringFormat3("lv.%d", lv)
    else 
        icon.gameObject:SetActive(false)
        nameLab.text = ""
        lvLab.gameObject:SetActive(false)
    end

    self:InitScore(score, info)
end

function LuaAntiProfessionSwapWnd:InitScore(item, info)
    local icon      = item.transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local numLab    = item.transform:Find("NumLab"):GetComponent(typeof(UILabel))
    if info ~= nil then
        local profession = info.Profession
        icon.gameObject:SetActive(true)
        icon:LoadMaterial(LuaZongMenMgr:GetProfessionScoreIconPath(info.Profession))
        local ownScore = CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.SkillProp.AntiProfessionScore, profession) and CClientMainPlayer.Inst.SkillProp.AntiProfessionScore[profession] or 0
        numLab.text = SafeStringFormat3("%d", ownScore)
    else
        icon.gameObject:SetActive(false)
        numLab.text =  ""
    end
end

function LuaAntiProfessionSwapWnd:InitIcons()
    self.table.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_Infos
        end,
        function(item, index)
            self:InitItem(item, self.m_Infos[index + 1])
        end
    )

    self.table.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        local info = self.m_Infos[row + 1]
        if (self.m_OriAntiSlot or {}).Profession == info.Profession or (self.m_TarAntiSlot or {}).Profession == info.Profession then
            g_MessageMgr:ShowMessage("ANTIPROFESSION_SWAPVIEW_ALREADYSELECTED")
            return
        end
        self:SetSlot(row + 1, info)
        self.table:ReloadData(true, false)
    end)

    self:RefreshIcons()
end

function LuaAntiProfessionSwapWnd:RefreshIcons()
    self.m_Infos = {}

    CommonDefs.DictIterate(CClientMainPlayer.Inst.SkillProp.AntiProfessionSkill, DelegateFactory.Action_object_object(function(___key, ___value)
        table.insert(self.m_Infos, ___value)
    end))
    
    self.table:ReloadData(true, false)
end

function LuaAntiProfessionSwapWnd:InitItem(item, info)
    local btn       = item:GetComponent(typeof(QnButton))
    local icon      = item.transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local selected  = item.transform:Find("Selected").gameObject
    local lvLab     = item.transform:Find("LvLab"):GetComponent(typeof(UILabel))

    local profession = info.Profession
    local proClass = CommonDefs.ConvertIntToEnum(typeof(EnumClass), profession)

    selected:SetActive((self.m_OriAntiSlot or {}).Profession == info.Profession or (self.m_TarAntiSlot or {}).Profession == info.Profession)
    icon:LoadMaterial(Profession.GetLargeIcon(proClass))
    lvLab.text = SafeStringFormat3("lv.%d", info.CurLevel)
end

function LuaAntiProfessionSwapWnd:CalTransferYuanBao(Index1, Index2, Level1, Level2)
	local base1 = 0
	if Level1 > 0 then
		base1 = SoulCore_AntiProfessionLevel.GetData(Level1).TransferBaseYuanBao 
	end

	local base2 = 0
	if Level2 > 0 then
		base2 = SoulCore_AntiProfessionLevel.GetData(Level2).TransferBaseYuanBao
	end

	local slotRate1 = SoulCore_AntiProfessionSlot.GetData(Index1).TransferYuanBaoRate
	local slotRate2 = SoulCore_AntiProfessionSlot.GetData(Index2).TransferYuanBaoRate

	return math.floor(math.max(base1 * slotRate1, base2 * slotRate2))
end
