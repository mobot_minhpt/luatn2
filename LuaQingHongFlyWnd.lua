local CGuideMgr=import "L10.Game.Guide.CGuideMgr"
local UISlider = import "UISlider"
local CUIFx =import "L10.UI.CUIFx"
local NGUIText = import "NGUIText"
local Time = import "UnityEngine.Time"
local CGuideBubbleDisplay=import "L10.UI.CGuideBubbleDisplay"
local UIAnchor=import "UIAnchor"

LuaQingHongFlyWnd = class()

RegistChildComponent(LuaQingHongFlyWnd, "FlyingStateSlider", UISlider)
RegistChildComponent(LuaQingHongFlyWnd, "ShineFx", CUIFx)
RegistChildComponent(LuaQingHongFlyWnd, "PeriodSpriteTop", UISprite)
RegistChildComponent(LuaQingHongFlyWnd, "Foreground", UISprite)
RegistChildComponent(LuaQingHongFlyWnd, "ThumbFx", CUIFx)

RegistClassMember(LuaQingHongFlyWnd, "CurrentProgress")
RegistClassMember(LuaQingHongFlyWnd, "TimeInterval")
RegistClassMember(LuaQingHongFlyWnd, "LastUpdateTime")
RegistClassMember(LuaQingHongFlyWnd, "UpdatePercent")

RegistClassMember(LuaQingHongFlyWnd, "m_GuideTick")


function LuaQingHongFlyWnd:Init()
	LuaQingHongFlyMgr.m_FlyFail = false
	self.ShineFx:DestroyFx()
	self.ThumbFx:DestroyFx()
	self.TimeInterval = 0.025
	self.LastUpdateTime = 0

	local setting = ZhuSiDong_Setting.GetData()

	self.UpdatePercent = setting.ReduceFlyPointPerSec * self.TimeInterval

	self:InitProgressPeriod()
	self:UpdateQingHongFlyProgress()

	--持续显示5秒钟
	if not LuaQingHongFlyMgr.m_TriggerGuide2 then
		LuaQingHongFlyMgr.m_TriggerGuide2 = true
		local text =g_MessageMgr:FormatMessage("Guide_QingHongFly")
		local cmp=self.transform:Find("Bubble"):GetComponent(typeof(CGuideBubbleDisplay))
		cmp:InitPosition(text,"QingHongFlyWnd.GetCenter()","top",UIAnchor.Side.Bottom)
		self.m_GuideTick = RegisterTickOnce(function()
			cmp.gameObject:SetActive(false)
			self.m_GuideTick = nil
			--结束当前引导
			CGuideMgr.Inst:EndCurrentPhase()
		end,5000)
	else
		self.transform:Find("Bubble").gameObject:SetActive(false)
	end
end

function LuaQingHongFlyWnd:InitProgressPeriod()
	local setting = ZhuSiDong_Setting.GetData()
	local full = setting.FlyPointFull
	local low = setting.FlyPointHitRange[0]
	local high = setting.FlyPointHitRange[1]

	-- period length
	local periodLength = (high - low) / full * self.FlyingStateSlider.backgroundWidget.width
	--self.PeriodSpriteTop.width = periodLength
	--self.PeriodSpriteBottom.width = periodLength

end

function LuaQingHongFlyWnd:UpdateQingHongFlyProgress()
	local setting = ZhuSiDong_Setting.GetData()
	local full = setting.FlyPointFull
	local low = setting.FlyPointHitRange[0]
	local high = setting.FlyPointHitRange[1]

	self.CurrentProgress = LuaQingHongFlyMgr.m_FlyProgress

	if LuaQingHongFlyMgr.m_FlyProgress < low or LuaQingHongFlyMgr.m_FlyProgress > high then
		-- 调整颜色为黄色 ffea60
		-- 预警 qinghongyupei_jindutiaoyujing_fx01, ThumbFx qinghongyupei_tishi_fx02
		self.Foreground.color = NGUIText.ParseColor24("ffea60", 0)
		self.ShineFx:DestroyFx()
		self.ShineFx:LoadFx("fx/ui/prefab/qinghongyupei_jindutiaoyujing_fx01.prefab")
		self.ThumbFx:DestroyFx()
		self.ThumbFx:LoadFx("fx/ui/prefab/qinghongyupei_tishi_fx02.prefab")
	else
		-- 调整进度条颜色为绿色 00ffa9
		--ThumbFx qinghongyupei_jindutiaoyujing_fx02
		self.Foreground.color = NGUIText.ParseColor24("00ffa9", 0)
		self.ShineFx:DestroyFx()
		self.ThumbFx:DestroyFx()
		self.ThumbFx:LoadFx("fx/ui/prefab/qinghongyupei_jindutiaoyujing_fx02.prefab")
	end

	local percent = LuaQingHongFlyMgr.m_FlyProgress / full
	self.FlyingStateSlider.value = percent
end

function LuaQingHongFlyWnd:Update()
	if Time.realtimeSinceStartup - self.LastUpdateTime > self.TimeInterval then
		self.LastUpdateTime = Time.realtimeSinceStartup
		self.CurrentProgress = self.CurrentProgress - self.UpdatePercent
		local setting = ZhuSiDong_Setting.GetData()
		local full = setting.FlyPointFull

		local percent = self.CurrentProgress / full
		self.FlyingStateSlider.value = percent
	end
end

function LuaQingHongFlyWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateQingHongFlyProgress", self, "UpdateQingHongFlyProgress")
end

function LuaQingHongFlyWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateQingHongFlyProgress", self, "UpdateQingHongFlyProgress")
end

function LuaQingHongFlyWnd:OnDestroy()
	if self.m_GuideTick then
        UnRegisterTick(self.m_GuideTick)
		self.m_GuideTick=nil
	end

	--如果没失败 则结束引导
	if not LuaQingHongFlyMgr.m_FlyFail then
		if CGuideMgr.Inst:IsInPhase(EnumGuideKey.QingHongFly) then
			CGuideMgr.Inst:EndCurrentPhase()
		end
	end
end

function  LuaQingHongFlyWnd:GetGuideGo(methodName)
	if methodName=="GetCenter" then
		return self.FlyingStateSlider.transform:Find("GuideTarget").gameObject
	end
	return nil
end
