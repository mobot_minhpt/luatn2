

local CUIFx = import "L10.UI.CUIFx"

local CButtonTexture = import "L10.UI.CButtonTexture"
local UIPanel = import "UIPanel"
local QnRichLabel = import "QnRichLabel"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local CButton = import "L10.UI.CButton"
local QnButton = import "L10.UI.QnButton"
local UITable = import "UITable"
local GameObject = import "UnityEngine.GameObject"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local NGUITools = import "NGUITools"
local NGUIText = import "NGUIText"
local CChatLinkMgr = import "CChatLinkMgr"
local StringBuilder = import "System.Text.StringBuilder"
local Regex = import "System.Text.RegularExpressions.Regex"
local GameSetting_Common_Wapper = import "L10.Game.GameSetting_Common_Wapper"
local Item_Item = import "L10.Game.Item_Item"
local IdPartition = import "L10.Game.IdPartition"
local EquipmentTemplate_Equip=import "L10.Game.EquipmentTemplate_Equip"
local CEquipment = import "L10.Game.CEquipment"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"

LuaTongQingYouLiWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaTongQingYouLiWnd, "ActivityTimeLabel", "ActivityTimeLabel", UILabel)
RegistChildComponent(LuaTongQingYouLiWnd, "QianDaoLabel", "QianDaoLabel", UILabel)
RegistChildComponent(LuaTongQingYouLiWnd, "ChongZhiLabel", "ChongZhiLabel", UILabel)
RegistChildComponent(LuaTongQingYouLiWnd, "GoToBtn", "GoToBtn", CButton)
RegistChildComponent(LuaTongQingYouLiWnd, "ShareLabel", "ShareLabel", UILabel)
RegistChildComponent(LuaTongQingYouLiWnd, "ShareBtn", "ShareBtn", CButton)
RegistChildComponent(LuaTongQingYouLiWnd, "YvYueLabel", "YvYueLabel", UILabel)
RegistChildComponent(LuaTongQingYouLiWnd, "YvYueBtn", "YvYueBtn", CButton)
RegistChildComponent(LuaTongQingYouLiWnd, "HongBaoTipBtn", "HongBaoTipBtn", QnButton)
RegistChildComponent(LuaTongQingYouLiWnd, "HongBaoLabel", "HongBaoLabel", UILabel)
RegistChildComponent(LuaTongQingYouLiWnd, "AwardIteamTable", "AwardIteamTable", GameObject)
RegistChildComponent(LuaTongQingYouLiWnd, "AwardIteamTable1", "AwardIteamTable1", GameObject)
RegistChildComponent(LuaTongQingYouLiWnd, "QiFuPointTipBtn", "QiFuPointTipBtn", QnButton)
RegistChildComponent(LuaTongQingYouLiWnd, "QiFuPointLabel", "QiFuPointLabel", UILabel)
RegistChildComponent(LuaTongQingYouLiWnd, "QiFuPointItemTemplate", "QiFuPointItemTemplate", GameObject)
RegistChildComponent(LuaTongQingYouLiWnd, "QiFuPointTable", "QiFuPointTable", UITable)
RegistChildComponent(LuaTongQingYouLiWnd, "QiFuBtn", "QiFuBtn", CButtonTexture)
RegistChildComponent(LuaTongQingYouLiWnd, "QiFuLeftTimesLabel", "QiFuLeftTimesLabel", UILabel)
RegistChildComponent(LuaTongQingYouLiWnd, "ChongZhiComplate", "ChongZhiComplate", GameObject)
RegistChildComponent(LuaTongQingYouLiWnd, "ShareComplate", "ShareComplate", GameObject)
RegistChildComponent(LuaTongQingYouLiWnd, "YvYueComplate", "YvYueComplate", GameObject)
RegistChildComponent(LuaTongQingYouLiWnd, "LuckyPlayer", "LuckyPlayer", GameObject)
RegistChildComponent(LuaTongQingYouLiWnd, "LuckyPlayerPanel", "LuckyPlayerPanel", UIPanel)
RegistChildComponent(LuaTongQingYouLiWnd, "LuckyPlayerLabel", "LuckyPlayerLabel", QnRichLabel)
RegistChildComponent(LuaTongQingYouLiWnd, "Fx", "Fx", CUIFx)
RegistChildComponent(LuaTongQingYouLiWnd, "Alert", "Alert", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaTongQingYouLiWnd, "m_RewardCount")	--抽奖次数
RegistClassMember(LuaTongQingYouLiWnd, "m_CountDownTick")
RegistClassMember(LuaTongQingYouLiWnd, "m_Charge") --累计充值
RegistClassMember(LuaTongQingYouLiWnd, "m_MilestoneItemsList") -- 祈福值奖励
RegistClassMember(LuaTongQingYouLiWnd, "m_MilestonePointList") -- 对应祈福值点数
RegistClassMember(LuaTongQingYouLiWnd, "m_TotalQiFuPoint") -- 累计祈福值
RegistClassMember(LuaTongQingYouLiWnd, "m_RewardPlayers") -- 获奖玩家信息
RegistClassMember(LuaTongQingYouLiWnd, "m_QiFuPoint")	-- 祈福值
RegistClassMember(LuaTongQingYouLiWnd, "m_StartTime")	-- 活动开始时间

function LuaTongQingYouLiWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.GoToBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnGoToBtnClick()
	end)


	
	UIEventListener.Get(self.ShareBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareBtnClick()
	end)


	
	UIEventListener.Get(self.YvYueBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnYvYueBtnClick()
	end)


	
	UIEventListener.Get(self.HongBaoTipBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnHongBaoTipBtnClick()
	end)


	
	UIEventListener.Get(self.QiFuPointTipBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnQiFuPointTipBtnClick()
	end)


	
	UIEventListener.Get(self.QiFuBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnQiFuBtnClick()
	end)


    --@endregion EventBind end
end

function LuaTongQingYouLiWnd:Init()
	self.Alert = FindChild(self.YvYueBtn.transform,"Alert").gameObject
	Gac2Gas.TongQingYouLi_QueryInfo()	--打开界面调用，返回界面所需数据
	-- 活动开始时间
	local startTime = ZhouNianQing_TongQingYouLiSettings.GetData().Tongqingyouli_StartTime
	self.m_StartTime = CServerTimeMgr.Inst:GetTimeStampByStr(startTime)
	-- 显示信息
	self:ShowInfo()
	self:UpdateInfo()

	if self.m_CountDownTick then 
		UnRegisterTick(self.m_CountDownTick)
		self.m_CountDownTick = nil
	end

	self.m_CountDownTick = RegisterTick(function() self:UpdateInfo() end, 2000)
end

-- 更新服务器端显示的数据
function LuaTongQingYouLiWnd:UpdateInfo()
	Gac2Gas.TongQingYouLi_QueryInfo()
end
-- 显示信息
function LuaTongQingYouLiWnd:ShowInfo()
	-- Label信息
	self.QianDaoLabel.text = g_MessageMgr:FormatMessage("TongQingYouLi_QiandaoTip")	-- 每日签到获得祈福次数
	self.ShareLabel.text = g_MessageMgr:FormatMessage("TongQingYouLi_ShareTip")	-- 分享获得祈福次数
	self.YvYueLabel.text = g_MessageMgr:FormatMessage("TongQingYouLi_AppointmentTip")	-- 预约获得祈福次数
	self.ActivityTimeLabel.text = g_MessageMgr:FormatMessage("TongQingYouLi_Time")		-- 活动时间

	
	-- 奖池珍贵物品
	local itemsArr =  g_LuaUtil:StrSplit(ZhouNianQing_TongQingYouLiSettings.GetData().AwardItems,';')
	local count = 1
	for i,v in ipairs(itemsArr) do
		if v then
			local dataInfo = g_LuaUtil:StrSplit(v,",")
			if dataInfo and #dataInfo == 2 then
				local itemId = tonumber(dataInfo[1])	--物品ID
				local itemNum = tonumber(dataInfo[2])	--物品数量
				if count <= 5 then	-- 前5个
					local childObj = self.AwardIteamTable.transform:GetChild(count - 1)
					childObj.gameObject:SetActive(true)
					local itemTemplate = childObj:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
					itemTemplate:Init(itemId,itemNum, false)
				else	-- 后5个
					local childObj = self.AwardIteamTable1.transform:GetChild(count - 6)
					childObj.gameObject:SetActive(true)
					local itemTemplate = childObj:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
					itemTemplate:Init(itemId,itemNum, false)
				end
			end
			
		end
		count = count + 1 
	end


	-- 累计祈福值物品
	self.m_MilestoneItemsList = {}
	self.m_MilestonePointList = {}
	Extensions.RemoveAllChildren(self.QiFuPointTable.transform)
	local milestoneItemsArr = g_LuaUtil:StrSplit(ZhouNianQing_TongQingYouLiSettings.GetData().MilestoneItems,';')
	for i,v in ipairs(milestoneItemsArr) do
		if v then
			local dataInfo = g_LuaUtil:StrSplit(v,",")
			if dataInfo and #dataInfo == 2 then
				local itemId = tonumber(dataInfo[1])
				local qiFuPoint = tonumber(dataInfo[2])
				local itemTemplateObj = CUICommonDef.AddChild(self.QiFuPointTable.gameObject, self.QiFuPointItemTemplate)
				table.insert(self.m_MilestoneItemsList,itemTemplateObj)
				table.insert(self.m_MilestonePointList,qiFuPoint)
				itemTemplateObj:SetActive(true)
				local itemTemplate = itemTemplateObj.transform:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
				itemTemplate:Init(itemId,qiFuPoint)
			end
		end
	end

	--累计祈福值
	local currTime = CServerTimeMgr.Inst.timeStamp
	local day = math.floor((currTime - self.m_StartTime) / (3600 * 24) ) 	-- 天数
	local interval = math.floor((currTime - self.m_StartTime - day * 3600 * 24) / 1200 ) -- 距离当前凌晨0点的时间（每20分钟）
	local qiFuPoint = 0
	
	if self.m_StartTime < CServerTimeMgr.Inst.timeStamp then	-- 活动开始
		-- 每天增加的数值
		local minRandom = (3472 + 5556 + 11806) * 8 * 3
		local maxRandom = (3486 + 5578 + 11854) * 8 * 3
		for i = 1, day, 1 do
			qiFuPoint = qiFuPoint + NGUITools.RandomRange(minRandom,maxRandom)
		end
		-- 距离当天凌晨增加的数值
		for i=1,interval,1 do -- 遍历所有时间间隔
			local time = i % (24*3)	 -- 所处的时间段
			if time >= 0 and time <= 8*3 then -- 0-8时，每20分钟在(3472,3486)中随机增加
				qiFuPoint = qiFuPoint + NGUITools.RandomRange(3472,3486)
			else 
				if time > 8*3 and time <= 16*3 then  -- 8 - 16时,每20分钟在(5556,5578)中随机增加
					qiFuPoint = qiFuPoint + NGUITools.RandomRange(5556,5578)
				else								-- 16 - 24时,每20分钟在(11806,11854)中随机增加
					qiFuPoint = qiFuPoint + NGUITools.RandomRange(11806,11854)
				end
			end
		end
	else	-- 活动未开始 
		qiFuPoint = 0
		LuaZhouNianQing2021Mgr.qiFuPoint = 0
	end 
	
	-- 如果计算出比当前显示的祈福数值大，更新（防止玩家在同一小时内多次打开界面导致随机计算的祈福值减小）
	if LuaZhouNianQing2021Mgr.qiFuPoint < qiFuPoint then
		LuaZhouNianQing2021Mgr.qiFuPoint = qiFuPoint
	end

	self.QiFuPointLabel.text = LuaZhouNianQing2021Mgr.qiFuPoint
	-- 里程碑奖励达成
	local length = #self.m_MilestoneItemsList
	for i=1, length, 1 do
		if LuaZhouNianQing2021Mgr.qiFuPoint >= tonumber(self.m_MilestonePointList[i]) * 10000 then -- 如果累计祈福值大于所需祈福值，显示勾号
			local itemTemplate = self.m_MilestoneItemsList[i].transform:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
			itemTemplate:ReachQiFuPoint()
		else 
			break
		end
	end

	-- 幸运玩家滚动条
	self.m_RewardPlayers = {}
	self.m_RewardPlayers = LuaZhouNianQing2021Mgr.rewardPlayers
	if self.m_RewardPlayers == nil or self.m_RewardPlayers.Count == 0 then	--当前没有幸运玩家
		self.LuckyPlayer.gameObject:SetActive(false)
	else
		self:PlayLuckyPlayerMessage()	-- 展示幸运玩家
	end

end

-- 更新信息
function LuaTongQingYouLiWnd:OnUpdateTongQingYouLiInfo(info)
	-- 剩余祈福次数
	self.m_RewardCount = info["RewadCount"]
	self.QiFuLeftTimesLabel.text = info.RewadCount..LocalString.GetString("次")

	-- 更新祈福按钮状态
	self:SetQiFuBtuFx()

	-- 累计送出现金红包
	local totalReward = info["TotalReward"]
	self.HongBaoLabel.text = totalReward


	-- 累计充值
	self.m_Charge = info["ChargeAmount"]
	self.ChongZhiLabel.text = g_MessageMgr:FormatMessage("TongQingYouLi_ChargeMoneyTip", self.m_Charge) -- 累充获得祈福次数
	if self.m_Charge >= 90 then				-- 充值达到上限
		self.GoToBtn.gameObject:SetActive(false)
		self.ChongZhiComplate:SetActive(true)
	else
		self.GoToBtn.gameObject:SetActive(true)	-- 充值未达到上限
		self.ChongZhiComplate:SetActive(false)
	end

	-- 分享
	if LuaZhouNianQing2021Mgr.Share == 0 then
		self.ShareBtn.gameObject:SetActive(true)
		self.ShareComplate:SetActive(false)
	else
		self.ShareBtn.gameObject:SetActive(false)
		self.ShareComplate:SetActive(true)
	end

	-- 预约
	if LuaZhouNianQing2021Mgr.Appointment  == 0 then
		self.YvYueBtn.gameObject:SetActive(true)
		self.YvYueComplate:SetActive(false)
		local zhiBoTimeStamp = CServerTimeMgr.Inst:GetTimeStampByStr(ZhouNianQing_ZhiBoSettings.GetData().ZhiBoStartTime)
		if CServerTimeMgr.Inst.timeStamp >= zhiBoTimeStamp then -- 过了直播时间将按钮置灰
			self.Alert:SetActive(false)
			self.YvYueBtn.Enabled = false
		else
			self.YvYueBtn.Enabled = true
		end
	else
		self.YvYueBtn.gameObject:SetActive(false)
		self.YvYueComplate:SetActive(true)
	end

end

--@region UIEvent
-- 前往充值
function LuaTongQingYouLiWnd:OnGoToBtnClick()
	-- 展示充值界面
	CShopMallMgr.ShowChargeWnd()
end

-- 分享
function LuaTongQingYouLiWnd:OnShareBtnClick()
	CUICommonDef.CaptureScreenAndShare()
end

-- 预约
function LuaTongQingYouLiWnd:OnYvYueBtnClick()
	CUIManager.ShowUI(CLuaUIResources.ZNQZhiBoYaoQingWnd)
end

-- 累计送出现金红包相关规则
function LuaTongQingYouLiWnd:OnHongBaoTipBtnClick()
	g_MessageMgr:ShowMessage("TongQingYouLi_HongbaoIntroduction")
end

-- 累计祈福值相关规则
function LuaTongQingYouLiWnd:OnQiFuPointTipBtnClick()
	g_MessageMgr:ShowMessage("TongQingYouLi_QifuzhiIntroduction")
end


-- 祈福
function LuaTongQingYouLiWnd:OnQiFuBtnClick()
	Gac2Gas.TongQingYouLi_GetReward()
end

-- 展示幸运玩家滚动条
function LuaTongQingYouLiWnd:PlayLuckyPlayerMessage()
	if self.m_RewardPlayers == nil then return end

	self.LuckyPlayer.gameObject:SetActive(true)
	local sb = NewStringBuilderWraper(StringBuilder)	-- 需要显示的消息
	for i=0, self.m_RewardPlayers.Count - 1 , 1 do
		local playerId = self.m_RewardPlayers[i].Id
		local playerName = self.m_RewardPlayers[i].Name
		local templateId = self.m_RewardPlayers[i].ItemTemplateId
		local serverName = self.m_RewardPlayers[i].ServerName
	
		if i ~= 0 then
			sb:Append(LocalString.GetString("、恭喜"))
		else 
			sb:Append(LocalString.GetString("恭喜"))
		end
		-- 玩家所在服
		if serverName then
			sb:Append(serverName)
		end
		-- 玩家名称
		if playerName then
			sb:Append("[");
			sb:Append(CChatLinkMgr.PlayerLinkColor);
			sb:Append("]");
			if Regex.IsMatch(playerName, "^[0-9a-fA-F]+$") then
				sb:Append("[ ");--当角色名称为纯数字时，NGUI的颜色标记块[]可能会发生副作用，添加空格避免这种情况
			else
				sb:Append("[");
			end
			sb:Append(playerName);
			sb:Append("][-]");
		end
		-- 玩家抽中道具
		sb:Append(LocalString.GetString("抽中"))
		if IdPartition.IdIsItem(templateId) then
			local itemdata = Item_Item.GetData(templateId)
			sb:Append(System.String.Format("[c][{0}]{1}[-][/c]",
						NGUIText.EncodeColor24(GameSetting_Common_Wapper.Inst:GetColor(itemdata.NameColor)),  --物品颜色
						itemdata.Name));
		else
			local equip = EquipmentTemplate_Equip.GetData(templateId);
			if equip then
				sb:Append(System.String.Format("[c][{0}]{1}[-][/c]",
						NGUIText.EncodeColor24(CEquipment:GetColor(templateId)),
						equip.Name))
			end
		end		
	end
	local script = CommonDefs.GetComponent_GameObject_Type(self.LuckyPlayer, typeof(CCommonLuaScript))
	script.m_LuaSelf:InitWithMsg(sb:ToString())
end

function LuaTongQingYouLiWnd:OnDestroy()
	if self.m_CountDownTick then
		UnRegisterTick(self.m_CountDownTick)
		self.m_CountDownTick = nil
	end
end
-- 祈福按钮特效
function LuaTongQingYouLiWnd:SetQiFuBtuFx()
	self.Fx:DestroyFx()
	if self.m_RewardCount > 0 then
		self.Fx:LoadFx("fx/ui/prefab/UI_zhounianqingdian_qifu.prefab")
	end
end

function LuaTongQingYouLiWnd:OnPersonalSpaceShareFinished()
	Gac2Gas.TongQingYouLi_Share()
end

-- 监听
function LuaTongQingYouLiWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateTongQingYouLiInfo", self, "OnUpdateTongQingYouLiInfo")
	g_ScriptEvent:AddListener("PersonalSpaceShareFinished", self, "OnPersonalSpaceShareFinished")
end
-- 解除监听
function LuaTongQingYouLiWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateTongQingYouLiInfo", self, "OnUpdateTongQingYouLiInfo")
	g_ScriptEvent:RemoveListener("PersonalSpaceShareFinished", self, "OnPersonalSpaceShareFinished")
end

--@endregion UIEvent


