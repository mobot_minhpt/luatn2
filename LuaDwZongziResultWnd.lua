local GameObject			= import "UnityEngine.GameObject"

local UIEventListener		= import "UIEventListener"
local DelegateFactory		= import "DelegateFactory"

local CItemInfoMgr			= import "L10.UI.CItemInfoMgr"
local CUITexture			= import "L10.UI.CUITexture"
local AlignType				= import "L10.UI.CItemInfoMgr+AlignType"


--粽子结算界面
CLuaDwZongziResultWnd = class()
CLuaDwZongziResultWnd.IsWin = 0
--注册View组件
RegistChildComponent(CLuaDwZongziResultWnd, "Item1",	GameObject)
RegistChildComponent(CLuaDwZongziResultWnd, "Item2",	GameObject)
RegistChildComponent(CLuaDwZongziResultWnd, "Win",		GameObject)
RegistChildComponent(CLuaDwZongziResultWnd, "Faild",	GameObject)

--注册变量
RegistClassMember(CLuaDwZongziResultWnd, "delayTick")

--[[
    @desc: 初始化
    author:CodeGize
    time:2019-04-16 21:48:04
    @return:
]]
function CLuaDwZongziResultWnd:Init()

	if CLuaDwZongziResultWnd.IsWin == 1 then
		self:ShowWin()
	else
		self:ShowFaild()
	end



end

--[[
    @desc: 关闭
    author:CodeGize
    time:2019-04-16 21:48:13
    @return:
]]
function CLuaDwZongziResultWnd:OnDisable()

end

--[[
    @desc:显示胜利 
    author:CodeGize
    time:2019-04-16 21:50:52
    @return:
]]
function CLuaDwZongziResultWnd:ShowWin()
	self.Win:SetActive(true)
	self.Faild:SetActive(false)

	local dwsetting = DuanWu_Setting.GetData()
	if dwsetting == nil then return end
	local awards = dwsetting.ZongZi2019RewardTempIds
	
	if awards == nil or awards.Length <= 0 then return end

	local id1 = awards[0]
	local id2 = awards[1]

	self:SetItem(self.Item1,id1)
	self:SetItem(self.Item2,id2)
end

--[[
    @desc: 显示失败
    author:CodeGize
    time:2019-04-16 21:51:01
    @return:
]]
function CLuaDwZongziResultWnd:ShowFaild( )
	self.Win:SetActive(false)
	self.Faild:SetActive(true)
end

--[[
    @desc: 显示道具
    author:CodeGize
    time:2019-04-17 13:43:01
    --@ctrl: 显示控件
	--@itemId: 道具配置ID
    @return:
]]
function CLuaDwZongziResultWnd:SetItem(ctrl,itemId)
	local item = Item_Item.GetData(itemId)
	if item == nil then return end

	local icon = ctrl.transform:Find("Texture"):GetComponent(typeof(CUITexture))

	icon:LoadMaterial(item.Icon)
	local itemclick = function(go)
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType.Default, 0, 0, 0, 0) 
	end
	UIEventListener.Get(ctrl.gameObject).onClick = DelegateFactory.VoidDelegate(itemclick)
end

