-- Auto Generated!!
local CLogMgr = import "L10.CLogMgr"
local CTeamMgr = import "L10.Game.CTeamMgr"
local CTeamQuickJoinInfoMgr = import "L10.UI.CTeamQuickJoinInfoMgr"
local CTeamQuickJoinWnd = import "L10.UI.CTeamQuickJoinWnd"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local L10 = import "L10"
local LocalString = import "LocalString"
local UInt32 = import "System.UInt32"
CTeamQuickJoinWnd.m_Init_CS2LuaHook = function (this) 

    if CTeamMgr.Inst:TeamExists() then
        if not CUIManager.IsLoaded(CUIResources.TeamWnd) then
            CUIManager.ShowUI(CUIResources.TeamWnd)
        end
        this:Close()
        return
    end
    CTeamQuickJoinInfoMgr.Inst:Clear()
    this.masterView.OnActivityClick = MakeDelegateFromCSFunction(this.OnActivityClick, MakeGenericClass(Action1, UInt32), this)
    this.masterView:Init(CTeamQuickJoinInfoMgr.Inst.DefaultActivityId)
end
CTeamQuickJoinWnd.m_OnTeamInfoChanged_CS2LuaHook = function (this) 

    if CTeamMgr.Inst:TeamExists() then
        this:Close()
        if not CUIManager.IsLoaded(CUIResources.TeamWnd) then
            CUIManager.ShowUI(CUIResources.TeamWnd)
        end
    end
end
CTeamQuickJoinWnd.m_GetTargetButton_CS2LuaHook = function (this) 
    if CTeamMgr.Inst:TeamExists() then
        --如果有队伍就中断引导
        L10.Game.Guide.CGuideMgr.Inst:EndCurrentPhase()
        return nil
    end
    --return infoTabContent.GetTargetButton();
    return nil
end
CTeamQuickJoinWnd.m_GetGuideGo_CS2LuaHook = function (this, methodName) 
    if methodName == "GetYiTiaoLong" then
        return this.masterView:GetYiTiaoLong()
    elseif methodName == "GetAutoMatchButton" then
        return this.detailView:GetAutoMatchButton()
    else
        CLogMgr.LogError(LocalString.GetString("引导数据表填写错误 ") .. methodName)
        return nil
    end
end

