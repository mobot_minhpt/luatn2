require("3rdParty/ScriptEvent")
require("common/common_include")

local CUITexture = import "L10.UI.CUITexture"
local CUICommonDef = import "L10.UI.CUICommonDef"
local DelegateFactory = import "DelegateFactory"
local CUIManager = import "L10.UI.CUIManager"
local CZhuJueJuQingMgr = import "L10.Game.CZhuJueJuQingMgr"
local CUIRes = import "L10.UI.CUIResources"


CLuaJuqingPicWnd=class()
RegistClassMember(CLuaJuqingPicWnd,"MainTexture")


CLuaJuqingPicWnd.s_Width=0
CLuaJuqingPicWnd.s_Height=0

function CLuaJuqingPicWnd:Init()
	local tex = self.MainTexture:GetComponent(typeof(CUITexture))
	local cb = function(flag)
		self:resize()
	end
	if LuaScrollTaskMgr.m_PictureCombData then
		LuaScrollTaskMgr:GenCombTex(tex.texture, LuaScrollTaskMgr.m_PictureCombData)
	elseif not System.String.IsNullOrEmpty(CZhuJueJuQingMgr.Instance.PicTexturePath) then
		tex:LoadMaterial(CZhuJueJuQingMgr.Instance.PicTexturePath,false,DelegateFactory.Action_bool(cb))
	end
	local hide = function()
		CUIManager.CloseUI(CUIRes.JuqingPicWnd)
	end
	CUICommonDef.DelayDoSth(CZhuJueJuQingMgr.Instance.PicDuduration*1000,DelegateFactory.Action(hide))
end
function CLuaJuqingPicWnd:resize()
	local cuitex = self.MainTexture:GetComponent(typeof(CUITexture))
	local uiTex = cuitex.texture
	local mat = cuitex.material
	local tex = mat.mainTexture
	uiTex.width = tex.width
	uiTex.height = tex.height

	local picName = CZhuJueJuQingMgr.Instance.PicTexturePath
	if picName=="UI/Texture/Transparent/Material/erhaliandongjuqing_jinnang_01.mat" then
		uiTex.width = 512
		uiTex.height = 282
	elseif picName=="UI/Texture/Transparent/Material/zhaixinliuqianchou_1.mat" then
		uiTex.width = 367
		uiTex.height = 870
	elseif picName=="UI/Texture/Transparent/Material/zhaixinliuqianchou_2.mat" then
		uiTex.width = 367
		uiTex.height = 870
	end
	if CLuaJuqingPicWnd.s_Width>0 then
		uiTex.width = CLuaJuqingPicWnd.s_Width
	end
	if CLuaJuqingPicWnd.s_Height>0 then
		uiTex.width = CLuaJuqingPicWnd.s_Height
	end
end

function CLuaJuqingPicWnd:OnDestroy()
	LuaScrollTaskMgr.m_PictureCombData = nil
end
