local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CCarnivalLotteryMgr = import "L10.Game.CCarnivalLotteryMgr"
local CQnReturnAwardTemplate = import "L10.UI.CQnReturnAwardTemplate"
local CItemMgr = import "L10.Game.CItemMgr"
local Animation = import "UnityEngine.Animation"

LuaChunJie2023ShouLieEnterWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaChunJie2023ShouLieEnterWnd, "StartBtn", GameObject)
RegistChildComponent(LuaChunJie2023ShouLieEnterWnd, "RuleBtn", GameObject)

RegistChildComponent(LuaChunJie2023ShouLieEnterWnd, "TabBtn1", GameObject)
RegistChildComponent(LuaChunJie2023ShouLieEnterWnd, "TabBtn2", GameObject)

RegistChildComponent(LuaChunJie2023ShouLieEnterWnd, "DescTitle", UILabel)
RegistChildComponent(LuaChunJie2023ShouLieEnterWnd, "DescLabel", UILabel)
RegistChildComponent(LuaChunJie2023ShouLieEnterWnd, "TimeLabel", UILabel)
RegistChildComponent(LuaChunJie2023ShouLieEnterWnd, "RewardGrid", UIGrid)
RegistChildComponent(LuaChunJie2023ShouLieEnterWnd, "ItemCell1", CQnReturnAwardTemplate)
RegistChildComponent(LuaChunJie2023ShouLieEnterWnd, "ItemCell2", CQnReturnAwardTemplate)

RegistClassMember(LuaChunJie2023ShouLieEnterWnd, "isDifficult")
RegistClassMember(LuaChunJie2023ShouLieEnterWnd, "animation")

--@endregion RegistChildComponent end

function LuaChunJie2023ShouLieEnterWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    UIEventListener.Get(self.StartBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnStartBtnClick()
    end)
    UIEventListener.Get(self.RuleBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnRuleBtnClick()
    end)
    UIEventListener.Get(self.TabBtn1).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnTabsClick(false)
    end)
    UIEventListener.Get(self.TabBtn2).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnTabsClick(true)
    end)
end

function LuaChunJie2023ShouLieEnterWnd:Init()
    self.animation = self.transform:GetComponent(typeof(Animation))
    self.animation:Play("chunjie2023shoulieenterwnd_show")
    self.isDifficult = false
    self:InitInfo()
end

function LuaChunJie2023ShouLieEnterWnd:InitInfo()
    local data = ChunJie_SLTTSetting.GetData()
    self.TabBtn1.transform:Find("highlight").gameObject:SetActive(not self.isDifficult)
    self.TabBtn2.transform:Find("highlight").gameObject:SetActive(self.isDifficult)
    if self.isDifficult then
        self.DescTitle.text = LocalString.GetString("英雄模式")
        self.DescLabel.text = data.DifficultPlayDescString
        self.ItemCell1.gameObject:SetActive(true)
    else
        self.DescTitle.text = LocalString.GetString("普通模式")
        self.DescLabel.text = data.NormalPlayDescString
        self.ItemCell1.gameObject:SetActive(false)
    end
    -- 活动次数
    local hasPlayTime = CCarnivalLotteryMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eSpring2023SLTT_DailyRewardTimes)
    local limitTime = data.DailyRewardTimesLimit
    self.TimeLabel.text = SafeStringFormat3(LocalString.GetString("今日奖励次数%d/%d"), hasPlayTime, limitTime)
    -- 奖励内容
    self.ItemCell1:Init(CItemMgr.Inst:GetItemTemplate(data.DifficultRewardItemId[0]), 1)
    self.ItemCell2:Init(CItemMgr.Inst:GetItemTemplate(data.DailyRewardItemId[0]), 1)
    self.RewardGrid:Reposition()
    local isFirstWin = CCarnivalLotteryMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eSpring2023SLTT_DifficultRewardTimes) == 1
    self.ItemCell1.transform:Find("mask").gameObject:SetActive(isFirstWin)
    local isRewardAll = hasPlayTime == limitTime
    self.ItemCell2.transform:Find("mask").gameObject:SetActive(isRewardAll)
end

--@region UIEvent
function LuaChunJie2023ShouLieEnterWnd:OnTabsClick(isDifficult)
    if self.isDifficult == isDifficult then return end
    self.isDifficult = isDifficult
    self:InitInfo()
    if isDifficult then
        self.animation:Play("chunjie2023shoulieenterwnd_switchtohero")
    else
        self.animation:Play("chunjie2023shoulieenterwnd_switchtonormal")
    end
end

function LuaChunJie2023ShouLieEnterWnd:OnRuleBtnClick()
    g_MessageMgr:ShowMessage("ChunJie2023_ShouLieTaoTie_TaskDes")
end

function LuaChunJie2023ShouLieEnterWnd:OnStartBtnClick()
    Gac2Gas.Spring2023SLTT_EnterGame(self.isDifficult)
end
--@endregion UIEvent

