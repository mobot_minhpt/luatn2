local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local QnButton = import "L10.UI.QnButton"
local QnTableView=import "L10.UI.QnTableView"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local UIGrid = import "UIGrid"
local CUITexture = import "L10.UI.CUITexture"
local CommonDefs = import "L10.Game.CommonDefs"
local Item_Item = import "L10.Game.Item_Item"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr=import "L10.UI.CItemInfoMgr"

CLuaQingMingJieZiXiaoChouWnd = class()

RegistChildComponent(CLuaQingMingJieZiXiaoChouWnd, "RuleButton", QnButton)
RegistChildComponent(CLuaQingMingJieZiXiaoChouWnd, "ChallengeButton", QnButton)
RegistChildComponent(CLuaQingMingJieZiXiaoChouWnd, "ChallengeLabel", UILabel)
RegistChildComponent(CLuaQingMingJieZiXiaoChouWnd, "TableView", QnTableView)
RegistChildComponent(CLuaQingMingJieZiXiaoChouWnd, "AwardItem", GameObject)

RegistClassMember(CLuaQingMingJieZiXiaoChouWnd, "m_AwardItemList")
RegistClassMember(CLuaQingMingJieZiXiaoChouWnd, "m_AwardInfoList")
RegistClassMember(CLuaQingMingJieZiXiaoChouWnd, "m_PlayData")

function CLuaQingMingJieZiXiaoChouWnd:OnEnable()
    g_ScriptEvent:AddListener("UpdateTempPlayDataWithKey", self, "OnUpdateTempPlayDataWithKey")
    g_ScriptEvent:AddListener("QueryTempPlayDataInUDResult", self, "OnQueryTempPlayDataInUDResult")
end

function CLuaQingMingJieZiXiaoChouWnd:OnDisable()
    CLuaQingMing2020Mgr.NeedAlert = true
    g_ScriptEvent:RemoveListener("UpdateTempPlayDataWithKey", self, "OnUpdateTempPlayDataWithKey")
    g_ScriptEvent:RemoveListener("QueryTempPlayDataInUDResult", self, "OnQueryTempPlayDataInUDResult")
end

function CLuaQingMingJieZiXiaoChouWnd:Awake()
    self.m_AwardItemList = {}
    self.m_AwardInfoList = {{},{},{},{},{}}
    self.m_PlayData = {}
    UIEventListener.Get(self.RuleButton.gameObject).onClick  =DelegateFactory.VoidDelegate(function(go)
        g_MessageMgr:ShowMessage("QingMing2020_JZXC_Rule")
    end)

    UIEventListener.Get(self.ChallengeButton.gameObject).onClick  =DelegateFactory.VoidDelegate(function(go)
        Gac2Gas.QingMing2020PveEnterPlay()
    end)

    self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function() 
            return #self.m_AwardInfoList
        end,
        function(item,row)
            self:InitItem(item,row)
        end)
    
end

function CLuaQingMingJieZiXiaoChouWnd:Init()
    self.m_AwardItemList = {}
    self.m_AwardInfoList = {{},{},{},{},{}}
    self.m_PlayData = {}

    local res = {}
    for part in string.gmatch(QingMing2019_QingMing2020Pve.GetData().RoundToAwardItem, "([^;]+);?") do
        if part then
            local ary = {}
            local index = 0
            local round = 0
            for val in string.gmatch(part, "(%d+),?") do
                index = index + 1
                val =  tonumber(val)
                if index == 1 then
                    round = val
                else
                    table.insert(self.m_AwardInfoList[round],val) 
                end
            end
        end
    end

    self.TableView:ReloadData(false, false)
    Gac2Gas.QueryTempPlayDataInUD(EnumTempPlayDataKey_lua.eQingMing2020PveData)
end

function CLuaQingMingJieZiXiaoChouWnd:InitItem(item, row)
    local info = self.m_AwardInfoList[row + 1]
    if info and next(info) then
        local passLabel = item.transform:Find("PassLabel"):GetComponent(typeof(UILabel))
        local awardGrid = item.transform:Find("AwardGrid"):GetComponent(typeof(UIGrid))
        local getButton = item.transform:Find("GetButton"):GetComponent(typeof(QnButton))
        local alert = item.transform:Find("GetButton/Alert").gameObject
        local enableSprite = item.transform:Find("GetButton/Enable").gameObject
        local disableSprite = item.transform:Find("GetButton/Disable").gameObject
        local unFinishLabel = item.transform:Find("UnFinishLabel").gameObject

        passLabel.text = CLuaQingMing2020Mgr.PassName[row + 1]

        Extensions.RemoveAllChildren(awardGrid.transform)
        for i,awardId in ipairs(info) do
            local award = NGUITools.AddChild(awardGrid.gameObject,self.AwardItem)
            award:SetActive(true)
            local texture = award.transform:Find("Icon"):GetComponent(typeof(CUITexture))
            local iconName = Item_Item.GetData(awardId).Icon
            texture:LoadMaterial(iconName)
            UIEventListener.Get(award).onClick = DelegateFactory.VoidDelegate(function(go)
                CItemInfoMgr.ShowLinkItemTemplateInfo(awardId,false,nil,AlignType.Default, 0, 0, 0, 0)
            end)
        end
        
        awardGrid:Reposition()
        table.insert(self.m_AwardItemList,item)

        UIEventListener.Get(getButton.gameObject).onClick  =DelegateFactory.VoidDelegate(function(go)
            Gac2Gas.QingMing2020PveRequestReward(row+1)
        end)
    end
end

function CLuaQingMingJieZiXiaoChouWnd:OnUpdateTempPlayDataWithKey(argv)

    if argv[0] ~= EnumTempPlayDataKey_lua.eQingMing2020PveData then return end
    Gac2Gas.QueryTempPlayDataInUD(EnumTempPlayDataKey_lua.eQingMing2020PveData)
end

function CLuaQingMingJieZiXiaoChouWnd:OnQueryTempPlayDataInUDResult(key, ud, data)
    if not data then return end
    
	for k, v in pairs(CLuaQingMing2020Mgr.EnumQingMing2020PvePlayDataKey) do
        if CommonDefs.DictContains(data, typeof(String), tostring(v)) then
            self.m_PlayData[k] = data[v]
		end
    end
    
    self:RefreshView()
end

function CLuaQingMingJieZiXiaoChouWnd:RefreshView()
    local passKey = "ePassRound_"
    local rewardKey = "eRewardRound_"
    for r = 1,5,1 do
        local pk = passKey..r
        local rk = rewardKey..r
        if not self.m_PlayData[pk] then
            self:SetItemStatus(r,"notPass")
        elseif not self.m_PlayData[rk] then
            self:SetItemStatus(r,"reward")
        else
            self:SetItemStatus(r,"hasReward")
        end
    end
    local playTimes = self.m_PlayData["ePlayTimes"] or 0
    self.ChallengeLabel.text = SafeStringFormat3(LocalString.GetString("挑战(%d/%d)"), playTimes, 3)   
    self.ChallengeButton.Enabled = playTimes < 3
end

function CLuaQingMingJieZiXiaoChouWnd:SetItemStatus(round,status)
    local item = self.m_AwardItemList[round]

    local getButton = item.transform:Find("GetButton"):GetComponent(typeof(QnButton))
    local alert = item.transform:Find("GetButton/Alert").gameObject
    local enableSprite = item.transform:Find("GetButton/Enable").gameObject
    local disableSprite = item.transform:Find("GetButton/Disable").gameObject
    local unFinishLabel = item.transform:Find("UnFinishLabel").gameObject

    if status == "notPass" then--未通过
        getButton.gameObject:SetActive(false)
    elseif status == "reward" then--领取
        getButton.gameObject:SetActive(true)
        enableSprite:SetActive(true)
        disableSprite:SetActive(false)
        getButton.Enabled = true
        alert:SetActive(CLuaQingMing2020Mgr.NeedAlert)
    elseif status == "hasReward" then--已领取
        getButton.gameObject:SetActive(true)
        enableSprite:SetActive(false)
        disableSprite:SetActive(true)
        getButton.Enabled = false
        alert:SetActive(false)
    end
end
