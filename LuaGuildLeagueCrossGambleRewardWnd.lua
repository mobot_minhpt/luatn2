local DelegateFactory  = import "DelegateFactory"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local EShareType = import "L10.UI.EShareType"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CUITexture = import "L10.UI.CUITexture"
local CUIFx = import "L10.UI.CUIFx"
local CBaseWnd = import "L10.UI.CBaseWnd"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"

LuaGuildLeagueCrossGambleRewardWnd = class()
RegistChildComponent(LuaGuildLeagueCrossGambleRewardWnd, "m_RewardLabel", "RewardLabel", UILabel)
RegistChildComponent(LuaGuildLeagueCrossGambleRewardWnd, "m_Buttons", "Buttons", GameObject)
RegistChildComponent(LuaGuildLeagueCrossGambleRewardWnd, "m_ShareButton", "ShareButton", GameObject)
RegistChildComponent(LuaGuildLeagueCrossGambleRewardWnd, "m_CloseButton", "CloseButton", GameObject)

function LuaGuildLeagueCrossGambleRewardWnd:Awake()
    UIEventListener.Get(self.m_ShareButton).onClick = DelegateFactory.VoidDelegate(function() self:OnShareButtonClick() end)
    UIEventListener.Get(self.m_CloseButton).onClick = DelegateFactory.VoidDelegate(function() self:OnCloseButtonClick() end)
end

function LuaGuildLeagueCrossGambleRewardWnd:Init()
    self.m_RewardLabel.text = tostring(LuaGuildLeagueCrossMgr.m_GambleGetRewardResult.rewardSilver)
    self.m_RewardLabel.effectColor = self.m_RewardLabel.color
end

function LuaGuildLeagueCrossGambleRewardWnd:OnShareButtonClick()
    local channels = CommonDefs.IS_CN_CLIENT and EnumInGameShareChannelDefine.eGuildLeagueCrossGambleRewardShare or EnumInGameShareChannelDefine.eGuild
    local shareText = g_MessageMgr:FormatMessage("GLC_SHARE_GAMBLE_REWARD_TEXT", tostring(LuaGuildLeagueCrossMgr.m_GambleGetRewardResult.rewardSilver))
    LuaInGameShareMgr.Share(channels, function()
        return shareText
    end, self.m_ShareButton.transform, CPopupMenuInfoMgr.AlignType.Top, nil, self.m_Buttons)
end

function LuaGuildLeagueCrossGambleRewardWnd:OnCloseButtonClick()
    self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

