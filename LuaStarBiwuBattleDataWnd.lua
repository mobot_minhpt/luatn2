local CPlayerInfoMgr = import "CPlayerInfoMgr"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local Skill_AllSkills=import "L10.Game.Skill_AllSkills"
local Profession=import "L10.Game.Profession"
local CUITexture=import "L10.UI.CUITexture"
local UIGrid=import "UIGrid"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local CGameVideoMgr=import "L10.Game.CGameVideoMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"


CLuaStarBiwuBattleDataWnd=class()
RegistChildComponent(CLuaStarBiwuBattleDataWnd,"m_DescriptionTemplate","DescriptionTemplate", GameObject)
RegistChildComponent(CLuaStarBiwuBattleDataWnd,"m_FengHuoView","FengHuoView", GameObject)
RegistChildComponent(CLuaStarBiwuBattleDataWnd,"m_NameLabel1","NameLabel1",UILabel)
RegistChildComponent(CLuaStarBiwuBattleDataWnd,"m_NameLabel2","NameLabel2",UILabel)
RegistChildComponent(CLuaStarBiwuBattleDataWnd,"m_ScoreLabel1","ScoreLabel1",UILabel)
RegistChildComponent(CLuaStarBiwuBattleDataWnd,"m_ScoreLabel2","ScoreLabel2",UILabel)
RegistChildComponent(CLuaStarBiwuBattleDataWnd,"m_MemberGrid1","MemberGrid1",UIGrid)
RegistChildComponent(CLuaStarBiwuBattleDataWnd,"m_MemberGrid2","MemberGrid2",UIGrid)
RegistChildComponent(CLuaStarBiwuBattleDataWnd,"m_MemberTemplate","MemberTemplate", GameObject)
RegistChildComponent(CLuaStarBiwuBattleDataWnd,"m_ScrollView","ScrollView",UIScrollView)
RegistChildComponent(CLuaStarBiwuBattleDataWnd,"m_Table","Table", UITable)
RegistChildComponent(CLuaStarBiwuBattleDataWnd,"m_TimeLabel","TimeLabel",UILabel)
RegistChildComponent(CLuaStarBiwuBattleDataWnd,"m_NoneReportDataLabel","NoneReportDataLabel",UILabel)
RegistChildComponent(CLuaStarBiwuBattleDataWnd,"m_CheerLabel1","CheerLabel1",UILabel)
RegistChildComponent(CLuaStarBiwuBattleDataWnd,"m_CheerLabel2","CheerLabel2",UILabel)
RegistChildComponent(CLuaStarBiwuBattleDataWnd,"m_FengHuoInfoLabel","FengHuoInfoLabel",UILabel)
RegistChildComponent(CLuaStarBiwuBattleDataWnd,"m_ShareFengHuoViewButton","ShareFengHuoViewButton", GameObject)

RegistClassMember(CLuaStarBiwuBattleDataWnd,"m_SkillRoot")
RegistClassMember(CLuaStarBiwuBattleDataWnd,"m_BaseRoot")
RegistClassMember(CLuaStarBiwuBattleDataWnd,"m_PlayPopupData")
RegistClassMember(CLuaStarBiwuBattleDataWnd,"m_DataPopupData")
RegistClassMember(CLuaStarBiwuBattleDataWnd,"m_PlayNames")
RegistClassMember(CLuaStarBiwuBattleDataWnd,"m_DataTypeNames")

RegistClassMember(CLuaStarBiwuBattleDataWnd,"m_PlaySelectButton")
RegistClassMember(CLuaStarBiwuBattleDataWnd,"m_DataTypeSelectButton")

RegistClassMember(CLuaStarBiwuBattleDataWnd,"m_PlayIndex")
RegistClassMember(CLuaStarBiwuBattleDataWnd,"m_DataTypeIndex")
RegistClassMember(CLuaStarBiwuBattleDataWnd,"m_SkillItemTemplate")
RegistClassMember(CLuaStarBiwuBattleDataWnd,"m_DataItemTemplate")

RegistClassMember(CLuaStarBiwuBattleDataWnd,"m_MainPlayerId")
RegistClassMember(CLuaStarBiwuBattleDataWnd,"m_FengHuoData")

function CLuaStarBiwuBattleDataWnd:Init()
    self.m_SkillRoot = self.transform:Find("Anchor/MainView/Skill")
    self.m_BaseRoot = self.transform:Find("Anchor/MainView/Base")
    self.m_SkillRoot.gameObject:SetActive(false)
    self.m_BaseRoot.gameObject:SetActive(false)
    self.m_SkillItemTemplate=FindChild(self.m_SkillRoot,"ItemTemplate").gameObject
    self.m_SkillItemTemplate:SetActive(false)
    self.m_DataItemTemplate=FindChild(self.m_BaseRoot,"ItemTemplate").gameObject
    self.m_DataItemTemplate:SetActive(false)
    self.m_FengHuoView:SetActive(false)
    self.m_DataItemTemplate:SetActive(false)
    self.m_MemberTemplate:SetActive(false)
    self.m_DescriptionTemplate:SetActive(false)
    self.m_NoneReportDataLabel.gameObject:SetActive(false)
    self.m_DataTypeSelectButton=FindChild(self.transform,"Button2").gameObject

    if CClientMainPlayer.Inst then
        self.m_MainPlayerId=CClientMainPlayer.Inst.Id
    end
    UIEventListener.Get(self.m_ShareFengHuoViewButton).onClick=DelegateFactory.VoidDelegate(function(go)
        self:ShareFengHuoView()
    end)


    self.m_PlayNames={
        LocalString.GetString("第一局：团队赛"),
        LocalString.GetString("第二局：单人赛"),
        LocalString.GetString("第三局：三人赛"),
        LocalString.GetString("第四局：团队赛"),
        LocalString.GetString("第五局：单人赛")
    }
    if CLuaStarBiwuMgr.m_BattleDataWnd_Season and CLuaStarBiwuMgr.m_BattleDataWnd_Season >= 11 then
        self.m_PlayNames={
            LocalString.GetString("第一局：团队赛"),
            LocalString.GetString("第二局：单人赛"),
            LocalString.GetString("第三局：双人赛"),
            LocalString.GetString("第四局：三人赛"),
            LocalString.GetString("第五局：团队赛"),
            LocalString.GetString("第六局：单人赛")
        }  
    end
    local popuplist={}
    for i,v in ipairs(self.m_PlayNames) do
        local data = PopupMenuItemData(v, DelegateFactory.Action_int(function(index) self:OnPlaySelected(index) end), false, nil, EnumPopupMenuItemStyle.Light)
        table.insert( popuplist,data )
    end
    self.m_PlayPopupData=Table2Array(popuplist,MakeArrayClass(PopupMenuItemData))
    self.m_PlaySelectButton=FindChild(self.transform,"Button1").gameObject
    UIEventListener.Get(self.m_PlaySelectButton).onClick=DelegateFactory.VoidDelegate(function(go)
        CPopupMenuInfoMgr.ShowPopupMenu(self.m_PlayPopupData, go.transform, CPopupMenuInfoMgr.AlignType.Bottom, 1, DelegateFactory.Action(function ()
            -- self.m_ClassTitle:Expand(false)
        end), 600, true, 296)
    end)

    self.m_DataTypeNames={LocalString.GetString("详细数据"),LocalString.GetString("技能使用")}
    local label=self.m_DataTypeSelectButton.transform:Find("Label"):GetComponent(typeof(UILabel))
    label.text=self.m_DataTypeNames[1]
    local alias = CLuaStarBiwuMgr.m_BattleDataWnd_AliasName
    if alias and not System.String.IsNullOrEmpty(alias) and not string.find(alias, "Star_JiFen") then
        table.insert(self.m_DataTypeNames,1, LocalString.GetString("烽火战报"))
        self:OnDataTypeSelected(0)
    end
    local popuplist2={}
    for i,v in ipairs(self.m_DataTypeNames) do
        local data = PopupMenuItemData(v, DelegateFactory.Action_int(function(index) self:OnDataTypeSelected(index) end), false, nil, EnumPopupMenuItemStyle.Light)
        table.insert( popuplist2,data )
    end
    self.m_DataPopupData=Table2Array(popuplist2,MakeArrayClass(PopupMenuItemData))
    UIEventListener.Get(self.m_DataTypeSelectButton).onClick=DelegateFactory.VoidDelegate(function(go)
        CPopupMenuInfoMgr.ShowPopupMenu(self.m_DataPopupData, go.transform, CPopupMenuInfoMgr.AlignType.Bottom, 1, DelegateFactory.Action(function ()
            -- self.m_ClassTitle:Expand(false)
        end), 600, true, 296)
    end)
    self.m_DataTypeIndex=1
    self:OnPlaySelected(0)
end
function CLuaStarBiwuBattleDataWnd:OnPlaySelected(index)
    local label=self.m_PlaySelectButton.transform:Find("Label"):GetComponent(typeof(UILabel))
    label.text=self.m_PlayNames[index+1]

    self.m_PlayIndex=index+1
    if self.m_DataTypeNames[self.m_DataTypeIndex] == LocalString.GetString("烽火战报") then
        self:LoadStarBiwuFengHuoView()
        return
    end

    --看看有没有数据
    if  CLuaStarBiwuMgr.m_FightData[index+1] then
        self:InitList()
    else
        --清掉界面
        self:InitList()
        if CGameVideoMgr.Inst:IsLiveMode() or CGameVideoMgr.Inst:IsVideoMode() then
        else
            Gac2Gas.QueryStarBiwuDetailFightData(index+1)
        end
    end
end

function CLuaStarBiwuBattleDataWnd:OnDataTypeSelected(index)
    local label=self.m_DataTypeSelectButton.transform:Find("Label"):GetComponent(typeof(UILabel))
    label.text=self.m_DataTypeNames[index+1]

    --看看有没有数据
    self.m_DataTypeIndex=index+1
    local isFengHuoView = self.m_DataTypeNames[self.m_DataTypeIndex] == LocalString.GetString("烽火战报")
    self.transform:Find("Anchor/MainView").gameObject:SetActive(not isFengHuoView)
    if not isFengHuoView then
        self:InitList()
        self.m_FengHuoView:SetActive(false)
    else
        self:LoadStarBiwuFengHuoView()
        if CLuaStarBiwuMgr.m_BattleDataWnd_AliasName and not System.String.IsNullOrEmpty(CLuaStarBiwuMgr.m_BattleDataWnd_AliasName) then
            Gac2Gas.QueryPlayReport(CLuaStarBiwuMgr.m_BattleDataWnd_AliasName)
        end
    end
end

function CLuaStarBiwuBattleDataWnd:OnEnable()
    g_ScriptEvent:AddListener("ReplyStarBiwuDetailFightData", self, "ReplyStarBiwuDetailFightData")
    g_ScriptEvent:AddListener("LoadStarBiwuFengHuoData", self, "OnLoadFengHuoData")
end

function CLuaStarBiwuBattleDataWnd:OnDisable()
    g_ScriptEvent:RemoveListener("ReplyStarBiwuDetailFightData", self, "ReplyStarBiwuDetailFightData")
    g_ScriptEvent:RemoveListener("LoadStarBiwuFengHuoData", self, "OnLoadFengHuoData")
end

function CLuaStarBiwuBattleDataWnd:ReplyStarBiwuDetailFightData(round, attackData, defendData)
    -- if round==self.m_PlayIndex then

    -- end
    local data=CLuaStarBiwuMgr.m_FightData[self.m_PlayIndex]
    if not data then return end

    self:InitList()
end
function CLuaStarBiwuBattleDataWnd:InitList()
    self.m_NoneReportDataLabel.gameObject:SetActive(false)
    self.transform:Find("Anchor/MainView").gameObject:SetActive(true)
    local grid1=nil
    local grid2=nil
    local itemTemplate=nil
    local teamNameLabel1=nil
    local teamNameLabel2=nil
    local isType1 = self.m_DataTypeNames[self.m_DataTypeIndex] == LocalString.GetString("详细数据")
    local isType2 = self.m_DataTypeNames[self.m_DataTypeIndex] == LocalString.GetString("技能使用")
    if isType1 then
        self.m_BaseRoot.gameObject:SetActive(true)
        self.m_SkillRoot.gameObject:SetActive(false)
        grid1=FindChild(self.m_BaseRoot,"Grid1")
        grid2=FindChild(self.m_BaseRoot,"Grid2")
        itemTemplate=FindChild(self.m_BaseRoot,"ItemTemplate").gameObject
        teamNameLabel1=FindChild(self.m_BaseRoot,"TeamNameLabel1"):GetComponent(typeof(UILabel))
        teamNameLabel2=FindChild(self.m_BaseRoot,"TeamNameLabel2"):GetComponent(typeof(UILabel))
    elseif isType2 then
        self.m_BaseRoot.gameObject:SetActive(false)
        self.m_SkillRoot.gameObject:SetActive(true)
        grid1=FindChild(self.m_SkillRoot,"Grid1")
        grid2=FindChild(self.m_SkillRoot,"Grid2")
        itemTemplate=FindChild(self.m_SkillRoot,"ItemTemplate").gameObject
        teamNameLabel1=FindChild(self.m_SkillRoot,"TeamNameLabel1"):GetComponent(typeof(UILabel))
        teamNameLabel2=FindChild(self.m_SkillRoot,"TeamNameLabel2"):GetComponent(typeof(UILabel))
    end
    if teamNameLabel1 ~= nil then
        teamNameLabel1.text=nil
    end
    if teamNameLabel2 ~= nil then
        teamNameLabel2.text=nil
    end

    if grid1 ~= nil then
        CUICommonDef.ClearTransform(grid1)
    end
    if grid2 ~= nil then
        CUICommonDef.ClearTransform(grid2)
    end

    local data=CLuaStarBiwuMgr.m_FightData[self.m_PlayIndex]
    if not data then return end
    local data1=data[1]
    local data2=data[2]
    if teamNameLabel1 ~= nil then
        teamNameLabel1.text=data.name1
    end
    if teamNameLabel2 ~= nil then
        teamNameLabel2.text=data.name2
    end
    if isType1 then
        --详细数据
        local count=math.max(5,#data1)
        for i=1,count do
            local go=NGUITools.AddChild(grid1.gameObject,itemTemplate)
            go:SetActive(true)
            self:InitDataItem(go.transform,data1[i],i)
        end

        count=math.max(5,#data2)
        for i=1,count do
            local go=NGUITools.AddChild(grid2.gameObject,itemTemplate)
            go:SetActive(true)
            self:InitDataItem(go.transform,data2[i],i)
        end


    elseif isType2 then
        --技能使用
        local count=math.max(5,#data1)
        for i=1,count do
            local go=NGUITools.AddChild(grid1.gameObject,itemTemplate)
            go:SetActive(true)
            self:InitSkillItem(go.transform,data1[i],i)
        end

        count=math.max(5,#data2)
        for i=1,count do
            local go=NGUITools.AddChild(grid2.gameObject,itemTemplate)
            go:SetActive(true)
            self:InitSkillItem(go.transform,data2[i],i)
        end

    end
    if grid1 ~= nil then
        grid1:GetComponent(typeof(UIGrid)):Reposition()
    end
    if grid2 ~= nil then
        grid2:GetComponent(typeof(UIGrid)):Reposition()
    end
end
function  CLuaStarBiwuBattleDataWnd:InitDataItem(tf,itemData,index)
    local bg=tf:Find("Bg").gameObject
    if index % 2==0 then
        bg:SetActive(true)
    else
        bg:SetActive(false)
    end

    local labelsTf=tf:Find("Labels")
    local nameLabel=tf:Find("NameLabel"):GetComponent(typeof(UILabel))
    local classSprite=tf:Find("ClassSprite"):GetComponent(typeof(UISprite))
    local btn=tf:Find("Button").gameObject

    if itemData then
        local fightdata=itemData.fightdata
        for i=1,8 do
            labelsTf:GetChild(i-1):GetComponent(typeof(UILabel)).text=tostring(fightdata[i])
        end
        nameLabel.text=itemData.name
        classSprite.spriteName=Profession.GetIconByNumber(itemData.clazz)

        if self.m_MainPlayerId==itemData.playerId then
            btn:SetActive(false)
        else
            btn:SetActive(true)
            UIEventListener.Get(btn).onClick=DelegateFactory.VoidDelegate(function(go)
                CPlayerInfoMgr.ShowPlayerInfoWnd(itemData.playerId, itemData.name)
            end)
        end
    else
        nameLabel.text=nil
        classSprite.spriteName=nil
        labelsTf.gameObject:SetActive(false)
        btn:SetActive(false)
    end
end
function CLuaStarBiwuBattleDataWnd:InitSkillItem(tf,itemData,index)
    local bg=tf:Find("Bg").gameObject
    if index % 2==0 then
        bg:SetActive(true)
    else
        bg:SetActive(false)
    end

    local labelsTf=tf:Find("Labels")
    local iconsTf=tf:Find("Icons")
    local nameLabel=tf:Find("NameLabel"):GetComponent(typeof(UILabel))
    local classSprite=tf:Find("ClassSprite"):GetComponent(typeof(UISprite))
    local btn=tf:Find("Button").gameObject

    if itemData then
        for i=1,8 do
            local skillcls=itemData.skilldata[i*2-1]
            local label=labelsTf:GetChild(i-1):GetComponent(typeof(UILabel))
            label.text=""
            if skillcls then
                local skillId=itemData.skilldata[i*2-1]*100+1
                local skillData=Skill_AllSkills.GetData(skillId)
                if skillData then
                    iconsTf:GetChild(i-1):GetComponent(typeof(CUITexture)):LoadSkillIcon(skillData.SkillIcon)
                    label.text=tostring(itemData.skilldata[i*2] or 0)
                else
                    label.text=""
                end
            end
        end
        nameLabel.text=itemData.name
        classSprite.spriteName=Profession.GetIconByNumber(itemData.clazz)

        if self.m_MainPlayerId==itemData.playerId then
            btn:SetActive(false)
        else
            btn:SetActive(true)
            UIEventListener.Get(btn).onClick=DelegateFactory.VoidDelegate(function(go)
                CPlayerInfoMgr.ShowPlayerInfoWnd(itemData.playerId, itemData.name)
            end)
        end
    else
        nameLabel.text=nil
        classSprite.spriteName=nil
        iconsTf.gameObject:SetActive(false)
        labelsTf.gameObject:SetActive(false)
        btn:SetActive(false)
    end
end

function CLuaStarBiwuBattleDataWnd:OnLoadFengHuoData(data)
    self.m_FengHuoData = data
    self:OnPlaySelected(self.m_PlayIndex and (self.m_PlayIndex - 1) or 0)
end

function CLuaStarBiwuBattleDataWnd:LoadStarBiwuFengHuoView()
    self.m_FengHuoView:SetActive(false)
    local data = self.m_FengHuoData
    Extensions.RemoveAllChildren(self.m_MemberGrid1.transform)
    Extensions.RemoveAllChildren(self.m_MemberGrid2.transform)
    Extensions.RemoveAllChildren(self.m_Table.transform)

    if not data then return end
    local round = self.m_PlayIndex
    if round > data.reportDataCount then
        self.m_NoneReportDataLabel.gameObject:SetActive(true)
        self.m_NoneReportDataLabel.text = PlayReport_Setting.GetData("NoneReportData").Value
        return
    end
    self.m_NoneReportDataLabel.gameObject:SetActive(false)
    self.m_FengHuoView:SetActive(true)
    self:ShowZhuWeiInfo(data, round)
    local leftTeamName = data.leftTeamName
    local rightTeamName = data.rightTeamName
    local leftTeamScore = data.scoreInfo[round][1]
    local rightTeamScore = data.scoreInfo[round][0]
    local time = CServerTimeMgr.ConvertTimeStampToZone8Time(data.fightTime)
    local leftTeamMemberClasses = {}
    local rightTeamMemberClasses = {}
    local report = data.reportInfo and data.reportInfo[round] or nil
    local roundInfo = data.roundInfo[round]
    if roundInfo and #roundInfo > 0 then
        for _, info in pairs(roundInfo) do
            local playerId = info.playerId
            local force = info.force
            if data.playerInfo[playerId] then
                local class = data.playerInfo[playerId].playerClass
                table.insert(force == 1 and leftTeamMemberClasses or rightTeamMemberClasses, class)
            end
        end
    end
    self.m_NameLabel1.text = leftTeamName
    self.m_NameLabel2.text = rightTeamName
    self.m_ScoreLabel1.text = leftTeamScore
    self.m_ScoreLabel2.text = rightTeamScore
    self.m_TimeLabel.text = ToStringWrap(time, "yyyy-MM-dd")
    self:ShowFengHuoInfoLabel()
    for i = 1,#leftTeamMemberClasses do
        local go = NGUITools.AddChild(self.m_MemberGrid1.gameObject, self.m_MemberTemplate)
        go:SetActive(true)
        local sprite = go:GetComponent(typeof(UISprite))
        local class = leftTeamMemberClasses[i]
        sprite.spriteName = Profession.GetIconByNumber(class)
    end
    self.m_MemberGrid1:Reposition()
    for i = #rightTeamMemberClasses,1, -1 do
        local go = NGUITools.AddChild(self.m_MemberGrid2.gameObject, self.m_MemberTemplate)
        go:SetActive(true)
        local sprite = go:GetComponent(typeof(UISprite))
        local class = rightTeamMemberClasses[i]
        sprite.spriteName = Profession.GetIconByNumber(class)
    end
    self.m_MemberGrid2:Reposition()
    local go = NGUITools.AddChild(self.m_Table.gameObject, self.m_DescriptionTemplate)
    go:SetActive(true)
    local msg = ""
    if report and #report > 0 then
        for i = 1, #report do
            local reportId = report[i].reportId
            msg = msg .. self:GetReportMsg(data, reportId, report[i].argList)
        end
    end
    go:GetComponent(typeof(UILabel)).text = CUICommonDef.TranslateToNGUIText(msg)
    self.m_Table:Reposition()
    self.m_ScrollView:ResetPosition()
end

function CLuaStarBiwuBattleDataWnd:ShowFengHuoInfoLabel()
    local alias = CLuaStarBiwuMgr.m_BattleDataWnd_AliasName
    if not alias then self.m_FengHuoInfoLabel.text = "" end
    local textArray1 = {LocalString.GetString("甲组"),LocalString.GetString("乙组"),LocalString.GetString("丙组"),LocalString.GetString("丁组")}
    local textArray2 = {
        LocalString.GetString("第一轮"),LocalString.GetString("第二轮"),LocalString.GetString("第三轮"),
        LocalString.GetString("第四轮"),LocalString.GetString("第五轮"),LocalString.GetString("第六轮"),
        LocalString.GetString("第七轮"),LocalString.GetString("第八轮"),LocalString.GetString("第九轮"),
        LocalString.GetString("第十轮"),LocalString.GetString("第十一轮"),LocalString.GetString("第十二轮")
    }
    local textArray3 = {
        LocalString.GetString("胜者组第一轮"),LocalString.GetString("胜者组第一轮"),LocalString.GetString("胜者组第一轮"),
        LocalString.GetString("胜者组第一轮"),LocalString.GetString("败者组第一轮"),LocalString.GetString("败者组第一轮"),
        LocalString.GetString("胜者组第二轮"),LocalString.GetString("胜者组第二轮"),LocalString.GetString("败者组第二轮"),
        LocalString.GetString("败者组第二轮"),LocalString.GetString("四强总决赛"),LocalString.GetString("四强总决赛")
        ,LocalString.GetString("四强总决赛"),LocalString.GetString("总决赛")
    }
    local s1,s2,s3 = "","",""
    local seasonId = 10
    local arr = g_LuaUtil:StrSplit(alias,LocalString.GetString("_"))
    if string.find(alias, "Star_XiaoZu") then
        s2 = LocalString.GetString("小组赛")
        if arr[6] then seasonId = tonumber(arr[6]) end
        if #arr >=4 then
            local group = tonumber(arr[3])
            local matchIdx = tonumber(arr[4])
            s3 = textArray1[group] .. textArray2[matchIdx]
        end
    elseif string.find(alias, "Star_TaoTai") then
        s2 = LocalString.GetString("")
        if arr[4] then seasonId = tonumber(arr[4]) end
        if #arr >=3 then
            local matchIdx = tonumber(arr[3])
            s3 = textArray3[matchIdx]
        end
    end
    local data = StarBiWuShow_AutoPatch.GetDataBySubKey("Season",seasonId)
    if data then
        s1 = data.Desc
    end
    self.m_FengHuoInfoLabel.text = SafeStringFormat3(LocalString.GetString("%s%s%s"),s1,s2,s3)
end

function CLuaStarBiwuBattleDataWnd:ShareFengHuoView()
    CUICommonDef.CaptureScreenAndShare()
end

function CLuaStarBiwuBattleDataWnd:ShowZhuWeiInfo(data, round)
    self.m_CheerLabel1.gameObject:SetActive(false)
    self.m_CheerLabel2.gameObject:SetActive(false)
    if not data or not data.zhuweiInfo[round] then return end
    local leftVal = data.zhuweiInfo[round][0]
    local rightVal = data.zhuweiInfo[round][1]
    self.m_CheerLabel1.text = leftVal
    self.m_CheerLabel2.text = rightVal
    self.m_CheerLabel1.gameObject:SetActive(true)
    self.m_CheerLabel2.gameObject:SetActive(true)
end

function CLuaStarBiwuBattleDataWnd:GetReportMsg(data, reportId, argList)
    local report = PlayReport_Report.GetData(reportId)
    local msg = report.Content
    local d_ArgList = g_LuaUtil:StrSplit(report.ArgList,";")
    local leftTeamColor = SafeStringFormat3("[%s]",NGUIText.EncodeColor24(self.m_NameLabel1.color))
    local rightTeamColor =  SafeStringFormat3("[%s]",NGUIText.EncodeColor24(self.m_NameLabel2.color))
    local classNameTable = {LocalString.GetString("射手"),LocalString.GetString("甲士"), LocalString.GetString("刀客"),
                            LocalString.GetString("侠客"), LocalString.GetString("方士"), LocalString.GetString("医师"),
                            LocalString.GetString("魅者"), LocalString.GetString("异人"), LocalString.GetString("偃师"),
                            LocalString.GetString("画魂"), LocalString.GetString("影灵"), LocalString.GetString("蝶客")}
    local params = {}

    local round = self.m_PlayIndex
    local allMembers = {}
    if data.roundInfo and data.roundInfo[round] and #data.roundInfo[round] > 0 then
        for k, data in pairs(data.roundInfo[round]) do
            local force = data.force
            if not allMembers[force] then
                allMembers[force] = {}
            end
            table.insert(allMembers[force], data.playerId)
        end
    end

    for i = 0 ,argList.Count - 1 do
        if string.find(tostring(d_ArgList[i+1]),"t") then
            local teamid = argList[i]
            local color = data.teamInfo[teamid].teamForce == 1 and leftTeamColor or rightTeamColor
            table.insert(params,SafeStringFormat3("%s%s[ffffff]",color, data.teamInfo[teamid].teamName))
        elseif string.find(tostring(d_ArgList[i+1]),"p") then
            local playerid = argList[i]
            local color = data.playerInfo[playerid].playerForce == 1 and leftTeamColor or rightTeamColor
            local class = classNameTable[data.playerInfo[playerid].playerClass]
            table.insert(params,SafeStringFormat3("%s%s(%s)[ffffff]",color, data.playerInfo[playerid].name, class))
        elseif string.find(tostring(d_ArgList[i+1]),"m") then
            if data.teamInfo[argList[i]] then
                local force = data.teamInfo[argList[i]].teamForce
                local members = allMembers[force]
                local msg = ""
                if members then
                    for j = 1,#members do
                        local playerid = members[j]
                        local color = data.playerInfo[playerid].playerForce == 1 and leftTeamColor or rightTeamColor
                        local class = classNameTable[data.playerInfo[playerid].playerClass]
                        local name = SafeStringFormat3("%s%s(%s)",color, data.playerInfo[playerid].name, class)
                        if j < (#members - 1) then
                            msg = msg .. SafeStringFormat3("%s%s",name, LocalString.GetString("、"))
                        elseif j == (#members - 1) then
                            msg = msg .. SafeStringFormat3("%s%s",name, LocalString.GetString("和"))
                        else
                            msg = msg ..name .."[ffffff]"
                        end
                    end
                end
                table.insert(params, msg)
            end
        else
            table.insert(params, type(argList[i]) == "number" and math.modf( argList[i]) or argList[i])
        end
    end
    if argList.Count == 1 then
        msg = SafeStringFormat3(msg, params[1])
    elseif argList.Count == 2 then
        msg = SafeStringFormat3(msg, params[1], params[2])
    elseif argList.Count == 3 then
        msg = SafeStringFormat3(msg, params[1], params[2], params[3])
    elseif argList.Count == 4 then
        msg = SafeStringFormat3(msg, params[1], params[2], params[3], params[4])
    elseif argList.Count == 5 then
        msg = SafeStringFormat3(msg, params[1], params[2], params[3], params[4], params[5])
    elseif argList.Count == 6 then
        msg = SafeStringFormat3(msg, params[1], params[2], params[3], params[4], params[5], params[6])
    end
    msg = string.gsub(msg, LocalString.GetString('(%d+)秒'),function (d)
        local a,b = math.modf(d / 60)
        local aStr = a > 0 and SafeStringFormat3(LocalString.GetString("%d分"),a) or ""
        local bStr =(b > 0 or a == 0) and SafeStringFormat3(LocalString.GetString("%d秒"),b * 60) or ""
        return aStr .. bStr
    end)
    return string.gsub(msg,"\\n","#r")
end

return CLuaStarBiwuBattleDataWnd
