local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Vector3 = import "UnityEngine.Vector3"
local SoundManager = import "SoundManager"
local CForcesMgr = import "L10.Game.CForcesMgr"
local CTooltip = import "L10.UI.CTooltip"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CItemConsumeWndMgr = import "L10.UI.CItemConsumeWndMgr"
local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local PlayerSettings = import "L10.Game.PlayerSettings"
local CClientFurnitureMgr = import "L10.Game.CClientFurnitureMgr"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"

ZhongYuanJie2022Mgr = {}

ZhongYuanJie2022Mgr.IsMDJSOpen = true --明灯寄思开关

--@region 瑶筝

ZhongYuanJie2022Mgr.YaoZhengDatas = {}
ZhongYuanJie2022Mgr.YaoZhengTick = nil
ZhongYuanJie2022Mgr.MusicDoc = nil
ZhongYuanJie2022Mgr.MusicDocId = 0
ZhongYuanJie2022Mgr.MusicDocStime = 0
ZhongYuanJie2022Mgr.MusicDocPid = 0
ZhongYuanJie2022Mgr.MusicDocEid = 0
ZhongYuanJie2022Mgr.EnableSoundMsgShowed = false
ZhongYuanJie2022Mgr.MusicDocNeedPlay = false

function ZhongYuanJie2022Mgr.OnDisconnect()
    ZhongYuanJie2022Mgr.StopMusicDoc(true)
end

function ZhongYuanJie2022Mgr.OnMainPlayerCreated()
    if ZhongYuanJie2022Mgr.MusicDocNeedPlay then --延迟播放时，在角色创建的时候播放
        ZhongYuanJie2022Mgr.MusicDocNeedPlay = false
        ZhongYuanJie2022Mgr.PlayMusicDoc()
    end
end

function ZhongYuanJie2022Mgr.OnSoundEnableChanged()
    local setting = PlayerSettings.SoundEnabled
    if setting then
        if ZhongYuanJie2022Mgr.MusicDocId > 0 then
            ZhongYuanJie2022Mgr.PlayMusicDoc()
        end
    else
        ZhongYuanJie2022Mgr.PauseMusicDoc()
    end
end

function ZhongYuanJie2022Mgr:OnPlayerDetach(playerId, furnitureId)
    local player = CClientPlayerMgr.Inst:GetPlayer(playerId)
    if player and player.EngineId == ZhongYuanJie2022Mgr.MusicDocEid then
        ZhongYuanJie2022Mgr.StopMusicDoc(true)
    end
end

function ZhongYuanJie2022Mgr:OnClientObjDestroy(args)
    local id = args[0]
    if id == ZhongYuanJie2022Mgr.MusicDocEid then
        ZhongYuanJie2022Mgr.StopMusicDoc(true)
    end
end

function ZhongYuanJie2022Mgr.DelListener()
    g_ScriptEvent:RemoveListener("OnSoundEnableChanged", ZhongYuanJie2022Mgr, "OnSoundEnableChanged")
    g_ScriptEvent:RemoveListener("MainPlayerCreated", ZhongYuanJie2022Mgr, "OnMainPlayerCreated")
    g_ScriptEvent:RemoveListener("GasDisconnect", ZhongYuanJie2022Mgr, "OnDisconnect")
    g_ScriptEvent:RemoveListener("ClientObjDestroy", ZhongYuanJie2022Mgr, "OnClientObjDestroy")
    g_ScriptEvent:RemoveListener("PlayerDetachFurniture", ZhongYuanJie2022Mgr, "OnPlayerDetach")
end

function ZhongYuanJie2022Mgr.AddListener()
    ZhongYuanJie2022Mgr.DelListener()
    g_ScriptEvent:AddListener("OnSoundEnableChanged", ZhongYuanJie2022Mgr, "OnSoundEnableChanged")
    g_ScriptEvent:AddListener("MainPlayerCreated", ZhongYuanJie2022Mgr, "OnMainPlayerCreated")
    g_ScriptEvent:AddListener("GasDisconnect", ZhongYuanJie2022Mgr, "OnDisconnect")
    g_ScriptEvent:AddListener("ClientObjDestroy", ZhongYuanJie2022Mgr, "OnClientObjDestroy")
    g_ScriptEvent:AddListener("PlayerDetachFurniture", ZhongYuanJie2022Mgr, "OnPlayerDetach")
end
ZhongYuanJie2022Mgr.AddListener()

function ZhongYuanJie2022Mgr.NeedShowEnableSoundMsg(playerId)
    if ZhongYuanJie2022Mgr.EnableSoundMsgShowed then
        return false
    end

    if not CClientMainPlayer.Inst or playerId == CClientMainPlayer.Inst.Id then
        return false
    end

    local soundenable = PlayerSettings.SoundEnabled
    if soundenable then
        return false
    end
    return true
end

function ZhongYuanJie2022Mgr.ShowEnableSoundMsg(playerId, func)
    local needshow = ZhongYuanJie2022Mgr.NeedShowEnableSoundMsg(playerId)
    if not needshow then
        if func then
            func()
        end
        return
    end

    local player = CClientPlayerMgr.Inst:GetPlayer(playerId)
    local pname = player and player.Name or LocalString.GetString("有人")
    local msg = g_MessageMgr:FormatMessage("YAOZHENG_OPENAUDIO_ATTENTION", pname)
    local okfunc = function()
        PlayerSettings.SoundEnabled = true
    end
    g_MessageMgr:ShowOkCancelMessage(msg, okfunc, nil, nil, nil, true)
    ZhongYuanJie2022Mgr.EnableSoundMsgShowed = true
end

function ZhongYuanJie2022Mgr.PlayYaoZheng(playerId, furnitureId, dataUd)
    if not CClientMainPlayer.Inst or playerId == CClientMainPlayer.Inst.Id then
        return
    end

    local player = CClientPlayerMgr.Inst:GetPlayer(playerId)
    if not player then
        return
    end
    SoundManager.Inst:ChangeBgMusicVolume("exp_yaozheng", 0, false)
    ZhongYuanJie2022Mgr.MusicDocEid = player.EngineId

    ZhongYuanJie2022Mgr.ShowEnableSoundMsg(playerId)

    local datas = dataUd.Length == 0 and {} or g_MessagePack.unpack(dataUd)
    for i = 1, #datas, 3 do
        local data = {}
        data.interval = datas[i]
        data.scale = datas[i + 1]
        data.gap = datas[i + 2]
        if #ZhongYuanJie2022Mgr.YaoZhengDatas <= 0 then
            data.gap = 0
        end
        table.insert(ZhongYuanJie2022Mgr.YaoZhengDatas, data)
    end
    ZhongYuanJie2022Mgr.PlayYaoZhengNext()
end

function ZhongYuanJie2022Mgr.DelayPlayYaoZheng(gap)
    if gap <= 0 then
        ZhongYuanJie2022Mgr.PlayYaoZhengNext()
    else
        RegisterTickOnce(ZhongYuanJie2022Mgr.PlayYaoZhengNext, gap)
    end
end

function ZhongYuanJie2022Mgr.PlayYaoZhengNext()
    if #ZhongYuanJie2022Mgr.YaoZhengDatas > 0 then
        local data = ZhongYuanJie2022Mgr.YaoZhengDatas[1]
        table.remove(ZhongYuanJie2022Mgr.YaoZhengDatas, 1)
        ZhongYuanJie2022Mgr.PlayYaoZhengSound(data.interval, data.scale)

        if #ZhongYuanJie2022Mgr.YaoZhengDatas > 0 then
            data = ZhongYuanJie2022Mgr.YaoZhengDatas[1]
            ZhongYuanJie2022Mgr.DelayPlayYaoZheng(data.gap)
        end
    end
end

function ZhongYuanJie2022Mgr.PlayYaoZhengSound(interval, scale)
    InstrumentMgr.PlaySound(interval, scale)
end

function ZhongYuanJie2022Mgr.ProcessGuZhengPlayDoc(playerId, fid, sheetId, startTime)
    if startTime > 0 then
        ZhongYuanJie2022Mgr.MusicDocId = sheetId
        ZhongYuanJie2022Mgr.MusicDocStime = startTime
        ZhongYuanJie2022Mgr.MusicDocPid = playerId
        
        if CClientMainPlayer.Inst then --主角存在才能播放乐谱
            ZhongYuanJie2022Mgr.ShowEnableSoundMsg(playerId, ZhongYuanJie2022Mgr.PlayMusicDoc)
            if playerId == CClientMainPlayer.Inst.Id then
                ZhongYuanJie2022Mgr.AddCameraPoint(18) --增加180s的镜头
            end
        else
            ZhongYuanJie2022Mgr.MusicDocNeedPlay = true --延迟播放
        end
    else
        ZhongYuanJie2022Mgr.StopMusicDoc(true) --服务器要求停止乐谱
    end
end

function ZhongYuanJie2022Mgr.PauseMusicDoc()
    if ZhongYuanJie2022Mgr.MusicDoc then
        SoundManager.Inst:StopSound(ZhongYuanJie2022Mgr.MusicDoc)
    end
    ZhongYuanJie2022Mgr.MusicDoc = nil
end

function ZhongYuanJie2022Mgr.StopMusicDoc(tf)
    ZhongYuanJie2022Mgr.PauseMusicDoc()

    if tf then
        ZhongYuanJie2022Mgr.MusicDocId = 0
        ZhongYuanJie2022Mgr.MusicDocStime = 0
        ZhongYuanJie2022Mgr.MusicDocPid = 0
        ZhongYuanJie2022Mgr.MusicDocEid = 0
        ZhongYuanJie2022Mgr.MusicDocNeedPlay = false
        SoundManager.Inst:ChangeBgMusicVolume("exp_yaozheng", 1, true)
    end
end

function ZhongYuanJie2022Mgr.PlayMusicDoc()
    if not PlayerSettings.SoundEnabled then
        return
    end

    local player = CClientPlayerMgr.Inst:GetPlayer(ZhongYuanJie2022Mgr.MusicDocPid)
    if not player then
        return
    end
    SoundManager.Inst:ChangeBgMusicVolume("exp_yaozheng", 0, false)
    local pos = player and player.WorldPos or Vector3.zero
    local cfg = ZhongYuanJie_YaoZhengDoc.GetData(ZhongYuanJie2022Mgr.MusicDocId)
    local path = cfg.MusicDoc
    local now = CServerTimeMgr.Inst.timeStamp
    local time = math.floor((now - ZhongYuanJie2022Mgr.MusicDocStime) * 1000)

    ZhongYuanJie2022Mgr.StopMusicDoc(false)
    ZhongYuanJie2022Mgr.MusicDoc = SoundManager.Inst:PlaySound(path, pos, 1, nil, time, false)
    ZhongYuanJie2022Mgr.MusicDocEid = player.EngineId
end

function ZhongYuanJie2022Mgr.GetCameraMoveDur()
    return 10
end

function ZhongYuanJie2022Mgr.AddCameraPoint(count)
    if CClientMainPlayer.Inst == nil then
        return
    end
    local eid = CClientMainPlayer.Inst.EngineId
    local datas = ZhongYuanJie_Setting2022.GetData().CameraArgs
    if datas == nil then
        return
    end

    local dur = ZhongYuanJie2022Mgr.GetCameraMoveDur()
    for i = 1, count do
        if CameraFollow.Inst.IsToricSpaceCamera then
            local cindex = math.random(datas.Length / 3)
            local index = (cindex - 1) * 3
            CameraFollow.Inst:ToricSpaceCameraMoveTo(eid, eid, datas[index], datas[index + 1], datas[index + 2], dur,true)
        else
            CameraFollow.Inst:BeginToricSpaceCamera(eid, eid, datas[0], datas[1], datas[2], 0, true,false,true)
            CameraFollow.Inst:ToricSpaceCameraMoveTo(eid, eid, datas[3], datas[4], datas[5], dur,true)
        end
    end
end

function ZhongYuanJie2022Mgr.GuZhengSheetUnlockSuccess(id)
    g_ScriptEvent:BroadcastInLua("GuZhengSheetUnlockSuccess", id)
end

function ZhongYuanJie2022Mgr.ReqPlayMusicDoc(fid, id)
    Gac2Gas.RequestPlayGuZhengSheet(fid, id)
end

function ZhongYuanJie2022Mgr.ShowItemConsumeWnd(id)
    local cfg = ZhongYuanJie_YaoZhengDoc.GetData(id)
    local cost = cfg.Cost
    local itemid = ZhongYuanJie_Setting2022.GetData().MusicScoresItemId --21021316
    CItemConsumeWndMgr.ShowItemConsumeWnd(
        itemid,
        cost,
        g_MessageMgr:FormatMessage("YUEPU_UNLOCK_CONFIRM", cfg.MusicName),
        LocalString.GetString("解锁"),
        DelegateFactory.Action(
            function()
                Gac2Gas.RequestUnlockGuZhengSheet(id)
            end
        ),
        DelegateFactory.Action(
            function()
                CItemAccessListMgr.Inst:ShowItemAccessInfo(itemid, true, nil, CTooltip.AlignType.Right)
            end
        ),
        nil
    )
end
--@endregion

--@region 古树凝珠

--[[
    @desc: 已获得的古树凝珠奖励次数
    author:Codee
    time:2022-05-27 11:02:35
    @return:
]]
function ZhongYuanJie2022Mgr.GetGSNZCount()
    local count = CClientMainPlayer.Inst.PlayProp:GetPlayTimes(245)
    return count
end

ZhongYuanJie2022Mgr.IsGSNZOpen = true --古树凝珠开关
ZhongYuanJie2022Mgr.GSNZGamePlayId = 51102934
ZhongYuanJie2022Mgr.GSNZType = -1 --1表示游魂 0表示鬼差
ZhongYuanJie2022Mgr.GSNZResultData = nil
ZhongYuanJie2022Mgr.GSNZProcess = 0

function ZhongYuanJie2022Mgr.UpdateActivity()
    local onclick = function()
        g_MessageMgr:ShowMessage("GSNZ_RULE_SIMPLE")
    end

    local msgname = ZhongYuanJie2022Mgr.GSNZType == 1 and "GSNZ_TASK_DESC_YOUHUN" or "GSNZ_TASK_DESC_GUICHAI"
    local desc = g_MessageMgr:FormatMessage(msgname, "%s")
    LuaCommonCountDownViewMgr:SetInfo(desc, true, true, nil, LocalString.GetString("规则"), nil, onclick,nil)
end

-- 同步所有玩家所扮演的角色
-- role: 1 ~ 10
-- @param dataU			List: {playerId, role, playerId, role, ...}
function ZhongYuanJie2022Mgr.UpdatePlayerGSNZRoleInfo(dataU)
    local MainPlayerId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
    if MainPlayerId ~= 0 then
        local bFind, force = CommonDefs.DictTryGet_LuaCall(CForcesMgr.Inst.m_PlayForceInfo, MainPlayerId)
        if bFind == true then
            ZhongYuanJie2022Mgr.GSNZType = force
        else
            local allList = dataU.Length == 0 and {} or g_MessagePack.unpack(dataU)
            for i = 1, #allList, 2 do
                local playerId = allList[i]
                local role = allList[i + 1]
                if playerId == CClientMainPlayer.Inst.Id then
                    if i > 10 then
                        ZhongYuanJie2022Mgr.GSNZType = 0 --鬼差
                    else
                        ZhongYuanJie2022Mgr.GSNZType = 1 --游魂
                    end
                end
            end
        end
        ZhongYuanJie2022Mgr.UpdateActivity()
    end
end

function ZhongYuanJie2022Mgr.SyncGSNZPlayInfo(value)
    ZhongYuanJie2022Mgr.GSNZProcess = value
    g_ScriptEvent:BroadcastInLua("OnSyncGSNZProcess")
end

function ZhongYuanJie2022Mgr.ShowGSNZResultWnd(winForce, force, rewardTime, playerInfosUd)
    ZhongYuanJie2022Mgr.GSNZResultData = {}
    ZhongYuanJie2022Mgr.GSNZResultData.Force = force
    ZhongYuanJie2022Mgr.GSNZResultData.IsWin = winForce == force
    ZhongYuanJie2022Mgr.GSNZResultData.WinType = winForce
    ZhongYuanJie2022Mgr.GSNZResultData.RewardTimes = rewardTime
    ZhongYuanJie2022Mgr.GSNZResultData.PlayerDatas = {}
    local allList = playerInfosUd.Length == 0 and {} or g_MessagePack.unpack(playerInfosUd)
    for i = 1, #allList, 7 do
        local data = {}
        data.PlayerId = allList[i]
        data.Name = allList[i + 1]
        data.Job = allList[i + 2]
        data.Role = allList[i + 3]
        data.Side = allList[i + 4]
        data.Killnum = allList[i + 5]
        data.Picknum = allList[i + 6]
        table.insert(ZhongYuanJie2022Mgr.GSNZResultData.PlayerDatas, data)
    end
    CUIManager.ShowUI(CLuaUIResources.GSNZResultWnd)
end

--@endregion

--@region 余愿梦记

ZhongYuanJie2022Mgr.IsYYMJOpen = true --余愿梦记开关
ZhongYuanJie2022Mgr.YYMJCondID = 0
ZhongYuanJie2022Mgr.YYMJTaskID = 0
function ZhongYuanJie2022Mgr.OpenYuYuanMengJi(taskId, conditionId)
    ZhongYuanJie2022Mgr.YYMJTaskID = taskId
    ZhongYuanJie2022Mgr.YYMJCondID = 0
    if conditionId then
        ZhongYuanJie2022Mgr.YYMJCondID = tonumber(conditionId)
    end
    CUIManager.ShowUI(CLuaUIResources.YuYuanMengJiWnd)
end

--@endregion
