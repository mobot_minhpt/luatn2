local UILabel = import "UILabel"
local NGUITools = import "NGUITools"
local CChatLinkMgr = import "CChatLinkMgr"


CLuaStarBiwuAgendaWnd = class()
RegistClassMember(CLuaStarBiwuAgendaWnd,"m_RulesTable")
RegistClassMember(CLuaStarBiwuAgendaWnd,"m_RuleItem")
RegistClassMember(CLuaStarBiwuAgendaWnd,"m_TimeLabel")

function CLuaStarBiwuAgendaWnd:Awake()
    self.m_RulesTable = self.transform:Find("ShowArea/RuleRoot/Scrollview/Table"):GetComponent(typeof(UITable))
    self.m_RuleItem = self.transform:Find("ShowArea/RuleRoot/Item").gameObject
    self.m_TimeLabel = {}
    self.m_TimeLabel[1]=FindChild(self.transform,"Time1"):GetComponent(typeof(UILabel))
    self.m_TimeLabel[2]=FindChild(self.transform,"Time2"):GetComponent(typeof(UILabel))
    self.m_TimeLabel[3]=FindChild(self.transform,"Time3"):GetComponent(typeof(UILabel))
    self.m_TimeLabel[4]=FindChild(self.transform,"Time4"):GetComponent(typeof(UILabel))
    self.m_TimeLabel[5]=FindChild(self.transform,"Time5"):GetComponent(typeof(UILabel))
    self.m_TimeLabel[6]=FindChild(self.transform,"Time6"):GetComponent(typeof(UILabel))
    self.m_TimeLabel[7]=FindChild(self.transform,"Time7"):GetComponent(typeof(UILabel))
    self.m_TimeLabel[8]=FindChild(self.transform,"Time8"):GetComponent(typeof(UILabel))
    self.m_TimeLabel[9]=FindChild(self.transform,"Time9"):GetComponent(typeof(UILabel))
end

function CLuaStarBiwuAgendaWnd:Init( )
    self.m_RuleItem:SetActive(false)
    local isQuDao = CLuaStarBiwuMgr.ShowWndIsQuDao
    local setting = StarBiWuShow_Setting.GetData()
    if isQuDao then setting = StarBiWuShow_QudaoSetting.GetData() end
    if nil == setting then
        CUIManager.CloseUI(CLuaUIResources.StarBiwuAgendaWnd)
        return
    end

    
    if isQuDao then -- 渠道服赛制
        self.transform:Find("Wnd_Bg_Primary_Tab/Tabs/Tab2").gameObject:SetActive(false)
        do
            local i = 0 local cnt = setting.Saizhi_Rule.Length
            while i < cnt do
                local go = NGUITools.AddChild(self.m_RulesTable.gameObject, self.m_RuleItem)
                if go ~= nil then
                    go:SetActive(true)
                    go.transform:Find("Sprite/Title"):GetComponent(typeof(UILabel)).text= CChatLinkMgr.TranslateToNGUIText(setting.Saizhi_Rule[i][0], false)
                    go.transform:Find("Content"):GetComponent(typeof(UILabel)).text= CChatLinkMgr.TranslateToNGUIText(setting.Saizhi_Rule[i][1], false)
                end
                i = i + 1
            end
        end
        self.m_RulesTable:Reposition()
    else
        for i,v in ipairs(self.m_TimeLabel) do
            v.text=CChatLinkMgr.TranslateToNGUIText(setting.Saicheng_Rule[i-1], false)
        end
        do
            local i = 0 local cnt = setting.Saizhi_Rule.Length
            while i < cnt do
                local go = NGUITools.AddChild(self.m_RulesTable.gameObject, self.m_RuleItem)
                if go ~= nil then
                    go:SetActive(true)
                    go.transform:Find("Sprite/Title"):GetComponent(typeof(UILabel)).text= CChatLinkMgr.TranslateToNGUIText(setting.Saizhi_Rule[i][0], false)
                    go.transform:Find("Content"):GetComponent(typeof(UILabel)).text= CChatLinkMgr.TranslateToNGUIText(setting.Saizhi_Rule[i][1], false)
                end
                i = i + 1
            end
        end
        self.m_RulesTable:Reposition()
    end

end

