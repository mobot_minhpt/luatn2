local CUIFx = import "L10.UI.CUIFx"

local UILabel = import "UILabel"

local QnTableView = import "L10.UI.QnTableView"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UITabBar = import "L10.UI.UITabBar"
local GameObject = import "UnityEngine.GameObject"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local CUITexture = import "L10.UI.CUITexture"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr=import "L10.UI.CItemInfoMgr"
local PathMode = import "DG.Tweening.PathMode"
local PathType = import "DG.Tweening.PathType"
local Ease = import "DG.Tweening.Ease"
local NGUIMath = import "NGUIMath"
local Vector4 = import "UnityEngine.Vector4"
local MsgPackImpl = import "MsgPackImpl"
local CommonDefs = import "L10.Game.CommonDefs"
local Object = import "System.Object"
local Profession = import "L10.Game.Profession"
local CMainPlayerInfoMgr = import "L10.UI.CMainPlayerInfoMgr"

LuaChooseMusicWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaChooseMusicWnd, "DifficultyTabBar", "DifficultyTabBar", UITabBar)
RegistChildComponent(LuaChooseMusicWnd, "RankBtn", "RankBtn", GameObject)
RegistChildComponent(LuaChooseMusicWnd, "BeginPlayBtn", "BeginPlayBtn", GameObject)
RegistChildComponent(LuaChooseMusicWnd, "TableView", "TableView", QnTableView)
RegistChildComponent(LuaChooseMusicWnd, "ModelTitleLabel", "ModelTitleLabel", UILabel)
RegistChildComponent(LuaChooseMusicWnd, "BgFx", "BgFx", CUIFx)

--@endregion RegistChildComponent end
RegistClassMember(LuaChooseMusicWnd,"m_MusicList")
RegistClassMember(LuaChooseMusicWnd,"m_SelectDifficulty")
RegistClassMember(LuaChooseMusicWnd,"m_SelectSongId")
function LuaChooseMusicWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	if CommonDefs.IS_VN_CLIENT then
		self.ModelTitleLabel.spacingX = 0
	end

	UIEventListener.Get(self.RankBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnAchivementBtnClick()
	end)

    --@endregion EventBind end
	UIEventListener.Get(self.BeginPlayBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    --self:OnBeginPlayBtnClick()
		self:OnRankBtnClick()
	end)
	self.DifficultyTabBar.OnTabChange = CommonDefs.CombineListner_Action_GameObject_int(self.DifficultyTabBar.OnTabChange, DelegateFactory.Action_GameObject_int(function(go, index)
        self:OnDifficultyTabChange(go, index)
    end), true)

	self.m_MusicList = {}
	self.m_EasyList = {}
	self.m_NormalList = {}
	self.m_HardList = {}

	MeiXiangLou_SongInfo.Foreach(function(songId,data)
		local t = {}
		t.SongId = data.SongId
		t.SongIdx = data.SongIdx
		t.Name = data.Name
		t.PassScore = data.PassScore
		if data.Difficulty == LocalString.GetString("简单") then
			table.insert(self.m_EasyList,t)
		elseif data.Difficulty == LocalString.GetString("中等") then
			table.insert(self.m_NormalList,t)
		elseif data.Difficulty == LocalString.GetString("困难") then
			table.insert(self.m_HardList,t)
		end
	end)

	table.sort(self.m_EasyList,function(a,b)
		return a.SongId < b.SongId
	end)
	table.sort(self.m_NormalList,function(a,b)
		return a.SongId < b.SongId
	end)
	table.sort(self.m_MusicList,function(a,b)
		return a.SongId < b.SongId
	end)

	self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            --return #self.m_MusicList / 3
			if self.m_SelectDifficulty == 0 then
				return #self.m_EasyList
			elseif self.m_SelectDifficulty == 1 then
				return #self.m_NormalList
			elseif self.m_SelectDifficulty == 2 then
				return #self.m_HardList
			end
        end,
        function(item,row)
            self:InitItem(item,row)
        end)
	self.TableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
		self:OnSelectAtRow(row)
	end)
	
	self.m_SelectDifficulty = 0
	self.m_SelectSongId = nil
	self:Init()
end

function LuaChooseMusicWnd:Init()
	self.BgFx:LoadFx("Fx/UI/Prefab/UI_qsq_fenwei.prefab")

	self.ModelTitleLabel.text = LuaMeiXiangLouMgr.m_PlayModelIndex == 0 and LocalString.GetString("单人") or LocalString.GetString("双人")
	--print("here init label",LuaMeiXiangLouMgr.m_PlayModelIndex)
	self.DifficultyTabBar:ChangeTab(0,false)
	--self.TableView:SetSelectRow(0,true)
	self.TableView:ReloadData(false,false)
end

function LuaChooseMusicWnd:OnEnable()
	self:Init()
	g_ScriptEvent:AddListener("SendItem",self,"OnMainPlayerPlayPropUpdate")
end
function LuaChooseMusicWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SendItem",self,"OnMainPlayerPlayPropUpdate")
end
function LuaChooseMusicWnd:OnMainPlayerPlayPropUpdate()
	self.TableView:ReloadData(true,false)
end

function LuaChooseMusicWnd:InitItem(item,row)
	if not LuaMeiXiangLouMgr.m_PlayModelIndex then
		return
	end
	local bgTexture = item.transform:GetComponent(typeof(CUITexture))
	local nameLabel = item.transform:Find("Label"):GetComponent(typeof(UILabel))
	local songInfo --= self.m_MusicList[row*3+1]
	if self.m_SelectDifficulty == 0 then
		songInfo = self.m_EasyList[row+1]
	elseif self.m_SelectDifficulty == 1 then
		songInfo = self.m_NormalList[row+1]
	elseif self.m_SelectDifficulty == 2 then
		songInfo = self.m_HardList[row+1]
	end
	if not songInfo then
		return
	end

	local id = songInfo.SongId
	local data = MeiXiangLou_SongInfo.GetData(id)
	--区分单双人
	if data.OperatorNumber ~= LuaMeiXiangLouMgr.m_PlayModelIndex+1 then
		item.gameObject:SetActive(false)
		return
	end
	nameLabel.text = songInfo.Name
	local path = SafeStringFormat("UI/Texture/Transparent/Material/musicplaywnd_xuanqu_0%d.mat",self.m_SelectDifficulty+1)
	bgTexture:LoadMaterial(path)
	--reward
	local rewardIcon = item.transform:Find("RewardIcon"):GetComponent(typeof(CUITexture))
	local chengHao = item.transform:Find("Chenghao"):GetComponent(typeof(CUITexture))
	local rewardFx = rewardIcon.transform:Find("RewardFx"):GetComponent(typeof(CUIFx))
	local rewardId = data.Reward
	local idata = Item_Item.GetData(rewardId)
	if idata then
		rewardIcon:LoadMaterial(idata.Icon)
	end

	local rewardStatus = self:GetRewardStatus(id,LuaMeiXiangLouMgr.m_PlayModelIndex+1)--0不能领 1待领取 2已经领取
	if rewardStatus == 2 then
		local score = self:GetMeiXiangLouSongPlayRecScore(id, nil)
		local tetxureName = "UI/Texture/Transparent/Material/musicplaywnd_guoshou.mat"
		if score >= data.GodlikeScore then
			tetxureName = "UI/Texture/Transparent/Material/musicplaywnd_qinsheng.mat"
		end
		chengHao:LoadMaterial(tetxureName)
		chengHao.gameObject:SetActive(true)
		rewardIcon.gameObject:SetActive(false)
	else
		chengHao.gameObject:SetActive(false)
		rewardIcon.gameObject:SetActive(true)
	end
	--can award
	rewardFx:DestroyFx()
	LuaTweenUtils.DOKill(rewardFx.transform, true)

	if rewardStatus == 1 then
		rewardFx:LoadFx("fx/ui/prefab/UI_kuang_yellow.prefab")
		rewardFx.transform.localPosition = Vector3(0,0,0)
		local b = NGUIMath.CalculateRelativeWidgetBounds(rewardFx.transform)
		local waypoints = CUICommonDef.GetWayPoints(Vector4(b.min.x, b.max.y, b.size.x, b.size.y), 1, true)
		rewardFx.transform.localPosition = waypoints[0]
		LuaTweenUtils.DOLuaLocalPath(rewardFx.transform, waypoints, 2, PathType.Linear, PathMode.Ignore, 2, - 1, Ease.Linear)
		UIEventListener.Get(rewardIcon.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
			Gac2Gas.RequestGetSongGoodAward(id, LuaMeiXiangLouMgr.m_PlayModelIndex+1)
		end)
	else
		UIEventListener.Get(rewardIcon.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
			CItemInfoMgr.ShowLinkItemTemplateInfo(rewardId,false,nil,AlignType.Default, 0, 0, 0, 0)
		end)
	end
end
function LuaChooseMusicWnd:OnSelectAtRow(row)
	local songInfo
	if self.m_SelectDifficulty == 0 then
		songInfo = self.m_EasyList[row+1]
	elseif self.m_SelectDifficulty == 1 then
		songInfo = self.m_NormalList[row+1]
	elseif self.m_SelectDifficulty == 2 then
		songInfo = self.m_HardList[row+1]
	end
	if not songInfo then
		return
	end
	self.m_SelectSongId = songInfo.SongId
	self:OnBeginPlayBtnClick()
end
--@region UIEvent

function LuaChooseMusicWnd:OnRankBtnClick()
	LuaMeiXiangLouMgr.OpenMusicPlayRankWnd()
end

function LuaChooseMusicWnd:OnBeginPlayBtnClick()
	if not self.m_SelectSongId then
		return
	end

	local isTeam = LuaMeiXiangLouMgr.m_PlayModelIndex == 1

	Gac2Gas.RequestEnterMeiXiangLou(isTeam,self.m_SelectSongId)
end

--@endregion UIEvent
function LuaChooseMusicWnd:OnAchivementBtnClick()
	--CMainPlayerInfoMgr.ShowMainPlayerWnd(2)
	LuaAchievementMgr:ShowDetailWndByAchievementId(61000667)
end

function LuaChooseMusicWnd:OnDifficultyTabChange(go, index)
	self.m_SelectDifficulty = index
	for i=0,self.DifficultyTabBar.tabButtons.Length-1,1 do 
		local tab = self.DifficultyTabBar:GetTabGoByIndex(i)
		local highlight = tab.transform:Find("Highlight").gameObject
		if i == index then 
			highlight:SetActive(true)
		else
			highlight:SetActive(false)
		end
	end
	self.TableView:ReloadData(true,false)
end

function LuaChooseMusicWnd:IsSongPlayOnceAwardGetted(songId, playType)
	if not CClientMainPlayer.Inst then
		return false
	end
	local playProp = CClientMainPlayer.Inst.PlayProp
	local songRec = CommonDefs.DictGetValue(CClientMainPlayer.Inst.PlayProp.PersistPlayData, typeof(UInt32), EnumPersistPlayDataKey_lua.eMeiXiangLouOnceAward)
	
	local songRecSet = songRec and MsgPackImpl.unpack(songRec.Data)

	if not songRecSet then 
		return false 
	end
	local idx = songId - 1 
	if songRecSet.Count > idx then
		return songRecSet[idx] == 1
	end
	return false
end

function LuaChooseMusicWnd:GetMeiXiangLouSongPlayRecScore(songId, playType)
	if not CClientMainPlayer.Inst then
		return 0
	end
	local songRec = CommonDefs.DictGetValue(CClientMainPlayer.Inst.PlayProp.PersistPlayData, typeof(UInt32), EnumPersistPlayDataKey_lua.eMeiXiangLou)
	local songRecSet = songRec and MsgPackImpl.unpack(songRec.Data)
	
	if not songRecSet then 
		return 0
	end

	local data = MeiXiangLou_SongInfo.GetData(songId)
	for sid=0,songRecSet.Count-1,1 do
		if sid +1 == songId then
			--区分单双人
			if data.OperatorNumber == 2 then
				return math.floor(songRecSet[sid] / 10)
			else
				return songRecSet[sid]
			end
		end
	end

	return 0
end

function LuaChooseMusicWnd:GetRewardStatus(songId,playType)--0不能领 1待领取 2已经领取
	local score = self:GetMeiXiangLouSongPlayRecScore(songId, playType)
	local songInfo = MeiXiangLou_SongInfo.GetData(songId)

	if self:IsSongPlayOnceAwardGetted(songId, playType) then
		return 2
	end
	if score >= songInfo.ProfessionalScore then
		return 1
	else
		return 0
	end
end