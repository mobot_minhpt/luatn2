local CPopupMenuInfoMgrAlignType = import "L10.UI.CPopupMenuInfoMgr+AlignType"
local CPersonalSpaceMgr=import "L10.Game.CPersonalSpaceMgr"
local CPlayerInfoMgr=import "CPlayerInfoMgr"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local CChatHelper=import "L10.UI.CChatHelper"
local CUITexture=import "L10.UI.CUITexture"
local CJingLingWebImageMgr=import "L10.Game.CJingLingWebImageMgr"
local Profession=import "L10.Game.Profession"
local Initialization_Init=import "L10.Game.Initialization_Init"
local QnAddSubAndInputButton=import "L10.UI.QnAddSubAndInputButton"
local QnTabView=import "L10.UI.QnTabView"
local Main = import "L10.Engine.Main"
local Json = import "L10.Game.Utils.Json"
local CHTTPForm = import "L10.Game.CHTTPForm"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"
CLuaQMPKStarWnd=class()
RegistClassMember(CLuaQMPKStarWnd, "m_TipButton")
RegistClassMember(CLuaQMPKStarWnd, "m_ShiLiZhiXingButton")
RegistClassMember(CLuaQMPKStarWnd, "m_RenQiZhiXingButton")
RegistClassMember(CLuaQMPKStarWnd, "m_ItemTemplate")
RegistClassMember(CLuaQMPKStarWnd, "m_ShiLiZhiXingTipLabel")
RegistClassMember(CLuaQMPKStarWnd, "m_RenQiZhiXingTipLabel")

RegistClassMember(CLuaQMPKStarWnd, "m_RenQiZhiXingData")
RegistClassMember(CLuaQMPKStarWnd, "m_RenQiZhiXingTotalPage")

function CLuaQMPKStarWnd:Init()
    self.m_RenQiZhiXingTotalPage=0

    self.m_RenQiZhiXingData=nil--人气之星数据

    self.m_ItemTemplate=FindChild(self.transform,"ItemTemplate").gameObject
    
    self.m_ItemTemplate:SetActive(false)
    Gac2Gas.RequestQmpkShiLiZhiXingInfo(1)
    --请求人气之星

    local tabView=FindChild(self.transform,"RadioBox"):GetComponent(typeof(QnTabView))
    tabView.OnSelect=DelegateFactory.Action_QnTabButton_int(function(btn,index)
        if index==0 then
            -- self.m_ShiLiZhiXingTipLabel.text=g_MessageMgr:FormatMessage("Qmpk_NO_QMZX")
        elseif index==1 then
            -- self.m_RenQiZhiXingTipLabel.text=g_MessageMgr:FormatMessage("Qmpk_NO_QMZX")
            if not self.m_RenQiZhiXingData then
                self:RequestRenQiZhiXing()
            end
        end
    end)

    local content1=FindChild(self.transform,"Content1")
    self.m_ShiLiZhiXingButton=content1:Find("QnIncreseAndDecreaseButton"):GetComponent(typeof(QnAddSubAndInputButton))
    self.m_ShiLiZhiXingButton:SetMinMax(0,0,1)

    self.m_ShiLiZhiXingTipLabel=FindChild(content1,"TipLabel"):GetComponent(typeof(UILabel))
    self.m_ShiLiZhiXingTipLabel.text=nil

    self.m_ShiLiZhiXingButton.onValueChanged = DelegateFactory.Action_uint(function (value) 
        if CLuaQMPKMgr.m_ShiLiZhiXingInfo[value] then
            self:InitShiLiZhiXingList(value,CLuaQMPKMgr.m_ShiLiZhiXingTotalPage,CLuaQMPKMgr.m_ShiLiZhiXingInfo[value])
        else
            Gac2Gas.RequestQmpkShiLiZhiXingInfo(value)
        end
    end)
    local content2=FindChild(self.transform,"Content2")
    self.m_RenQiZhiXingButton=content2:Find("QnIncreseAndDecreaseButton"):GetComponent(typeof(QnAddSubAndInputButton))
    self.m_RenQiZhiXingButton:SetMinMax(0,0,1)
    self.m_RenQiZhiXingTipLabel=FindChild(content2,"TipLabel"):GetComponent(typeof(UILabel))
    self.m_RenQiZhiXingTipLabel.text=nil
    self.m_RenQiZhiXingButton.onValueChanged = DelegateFactory.Action_uint(function (value) 
            self:InitRenQiZhiXingList(value,self.m_RenQiZhiXingTotalPage)
    end)


    self.m_TipButton=FindChild(self.transform,"TipButton").gameObject
    UIEventListener.Get(self.m_TipButton).onClick=DelegateFactory.VoidDelegate(function(go)
        g_MessageMgr:ShowMessage("QMPK_Star_Tip")
    end)

    self.m_ShiLiZhiXingTipLabel.text = g_MessageMgr:FormatMessage("Qmpk_NO_QMZX")
    self.m_RenQiZhiXingTipLabel.text = g_MessageMgr:FormatMessage("Qmpk_NO_QMZX")
end

function CLuaQMPKStarWnd:OnEnable()
    g_ScriptEvent:AddListener("ReplyQmpkShiLiZhiXingInfo", self, "OnReplyQmpkShiLiZhiXingInfo")
    
end
function CLuaQMPKStarWnd:OnDisable()
    g_ScriptEvent:RemoveListener("ReplyQmpkShiLiZhiXingInfo", self, "OnReplyQmpkShiLiZhiXingInfo")
end

function CLuaQMPKStarWnd:OnReplyQmpkShiLiZhiXingInfo(page, totalPage, playerInfo)
    if totalPage>0 then
        self.m_ShiLiZhiXingButton:SetMinMax(1,totalPage,1)
    else
        self.m_ShiLiZhiXingButton:SetMinMax(0,0,1)
    end

    -- for i,v in ipairs(playerInfo) do
    --     print(i,v)
    -- end
    self:InitShiLiZhiXingList(page,totalPage,playerInfo)
end

function CLuaQMPKStarWnd:InitShiLiZhiXingList(page,totalPage,playerInfo)
    self.m_ShiLiZhiXingButton:OverrideText(SafeStringFormat3( "%d/%d",page,math.max(totalPage,1) ))

    local content1=FindChild(self.transform,"Content1")
    local grid=FindChild(content1,"Grid")
    CUICommonDef.ClearTransform(grid)

    if #playerInfo == 0 then
        self.m_ShiLiZhiXingTipLabel.text=g_MessageMgr:FormatMessage("Qmpk_NO_QMZX")
    else
        self.m_ShiLiZhiXingTipLabel.text=nil
    end

    for i,v in ipairs(playerInfo) do
        local go=NGUITools.AddChild(grid.gameObject,self.m_ItemTemplate)
        go:SetActive(true)
        self:InitShiLiZhiXingItem(go.transform,v)
    end
    grid:GetComponent(typeof(UIGrid)):Reposition()
end

function CLuaQMPKStarWnd:InitShiLiZhiXingItem(transform,info)
    local icon=transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local shili=transform:Find("shili").gameObject
    local renqi=transform:Find("renqi").gameObject
    shili:SetActive(true)
    renqi:SetActive(false)

    --默认头像
    local initData=Initialization_Init.GetData(info.fromProfession*100+info.fromGender)
    if initData then
        icon:LoadPortrait(initData.ResName,false)
    end

    local serverNameLabel=transform:Find("ServerNameLabel"):GetComponent(typeof(UILabel))
    serverNameLabel.text=info.serverName

    local nameLabel1=transform:Find("NameLabel1"):GetComponent(typeof(UILabel))
    nameLabel1.text=info.fromCharacterName
    local nameLabel2=transform:Find("NameLabel2"):GetComponent(typeof(UILabel))
    nameLabel2.text=info.name

    local classSprite1=transform:Find("ClassSprite1"):GetComponent(typeof(UISprite))
    local classSprite2=transform:Find("ClassSprite2"):GetComponent(typeof(UISprite))

    classSprite1.spriteName=Profession.GetIconByNumber(info.fromProfession)
    classSprite2.spriteName=Profession.GetIconByNumber(info.class)

    UIEventListener.Get(transform.gameObject).onClick=DelegateFactory.VoidDelegate(function(p)
        local isme=false
        if CClientMainPlayer.Inst then
            local myid = CClientMainPlayer.Inst.Id
            if myid==info.fromCharacterId or myid==info.pid then
                isme=true
            end
        end
        if not isme then
            self:OnClickShiLiZhiXing(p)
        end
    end)
end
function CLuaQMPKStarWnd:InitRenQiZhiXingItem(transform,info)
    local icon=transform:Find("Icon"):GetComponent(typeof(CUITexture))

    local shili=transform:Find("shili").gameObject
    local renqi=transform:Find("renqi").gameObject
    shili:SetActive(false)
    renqi:SetActive(true)

    --默认头像
    local initData=Initialization_Init.GetData(info.class*100+info.gender)
    if initData then
        icon:LoadPortrait(initData.ResName,false)
    end

    local serverNameLabel=transform:Find("ServerNameLabel"):GetComponent(typeof(UILabel))
    serverNameLabel.text=info.serverName

    local nameLabel1=transform:Find("NameLabel1"):GetComponent(typeof(UILabel))
    nameLabel1.text=info.fromCharacterName
    local nameLabel2=transform:Find("NameLabel2"):GetComponent(typeof(UILabel))
    nameLabel2.text=info.name



    local classSprite1=transform:Find("ClassSprite1"):GetComponent(typeof(UISprite))
    local classSprite2=transform:Find("ClassSprite2"):GetComponent(typeof(UISprite))

    classSprite1.spriteName=Profession.GetIconByNumber(info.fromProfession)
    classSprite2.spriteName=Profession.GetIconByNumber(info.class)

    UIEventListener.Get(transform.gameObject).onClick=DelegateFactory.VoidDelegate(function(p)

        self:OnClickRenQiZhiXing(p)
    end)

    if info.fromCharacterId==info.pid then
        nameLabel2.text=nil
        classSprite2.spriteName=nil
    end
end



function CLuaQMPKStarWnd:OnDestroy()
    CLuaQMPKMgr.m_ShiLiZhiXingInfo={}
    if self.m_UrlCoroutine ~= nil then
        Main.Inst:StopCoroutine(self.m_UrlCoroutine)
        self.m_UrlCoroutine=nil
	end
end

function CLuaQMPKStarWnd:InitRenQiZhiXingList(page,totalPage)
    -- print(page,totalPage,#self.m_RenQiZhiXingData)
    self.m_RenQiZhiXingButton:OverrideText(SafeStringFormat3( "%d/%d",page,math.max(totalPage,1) ))

    if not self.m_RenQiZhiXingData then
        self.m_RenQiZhiXingTipLabel.text=g_MessageMgr:FormatMessage("Qmpk_NO_QMZX")
    end

    local content1=FindChild(self.transform,"Content2")
    local grid=FindChild(content1,"Grid")
    CUICommonDef.ClearTransform(grid)

    if self.m_RenQiZhiXingData then
        local startIndex=(page-1)*5+1
        local endIndex=math.min((page)*5,#self.m_RenQiZhiXingData)
        for i=startIndex,endIndex do
            local info=self.m_RenQiZhiXingData[i]
            local go=NGUITools.AddChild(grid.gameObject,self.m_ItemTemplate)
            go:SetActive(true)
            self:InitRenQiZhiXingItem(go.transform,info)
        end
    else
        -- self:RequestRenQiZhiXing()
    end
    grid:GetComponent(typeof(UIGrid)):Reposition()

end
function CLuaQMPKStarWnd:OnClickRenQiZhiXing(go)
    local index=go.transform:GetSiblingIndex()

    local page= self.m_RenQiZhiXingButton.m_CurrentValue
    page=math.max(page,1)
    if self.m_RenQiZhiXingData then
        local row= (page-1)*5+index+1
        local data=self.m_RenQiZhiXingData[row]
        local isme=false
        if CClientMainPlayer.Inst then
            local myid = CClientMainPlayer.Inst.Id
            if myid==data.fromCharacterId or myid==data.pid then
                isme=true
            end
        end
        if data and not isme then
            local t={}
            local action1 = PopupMenuItemData(LocalString.GetString("发送消息"),DelegateFactory.Action_int(function(index)
                CChatHelper.ShowFriendWnd(data.fromCharacterId, data.fromCharacterName)
            end),false,nil)
            table.insert(t, action1)
            local action2 = PopupMenuItemData(LocalString.GetString("原服角色"),DelegateFactory.Action_int(function(index)
                CPlayerInfoMgr.ShowPlayerInfoWnd(data.fromCharacterId, data.fromCharacterName)
            end),false,nil)
            table.insert(t, action2)
            local action3 = PopupMenuItemData(LocalString.GetString("添加好友"),DelegateFactory.Action_int(function(index)
                Gac2Gas.AddFriend(data.fromCharacterId, "AddFrindAction")
                
            end),false,nil)
            table.insert(t, action3)
            local action4 = PopupMenuItemData(LocalString.GetString("查看梦岛"),DelegateFactory.Action_int(function(index)
                CPersonalSpaceMgr.Inst:OpenPersonalSpaceWnd(data.fromCharacterId)
            end),false,nil)
            table.insert(t, action4)
            if data.pid~=data.fromCharacterId then
                local action5 = PopupMenuItemData(LocalString.GetString("参赛角色"),DelegateFactory.Action_int(function(index)
                    CPlayerInfoMgr.ShowPlayerInfoWnd(data.pid, data.name)
                end),false,nil)
                table.insert(t, action5)
                local action6 = PopupMenuItemData(LocalString.GetString("配置学习"),DelegateFactory.Action_int(function(index)
                    CLuaQMPKMgr.ShowPlayerConfigWnd(data.pid, data.name)
                end),false,nil)
                if CQuanMinPKMgr.Inst.m_IsQuanMinPKServer then
                    table.insert(t, action6)
                end
            end
            local array=Table2Array(t,MakeArrayClass(PopupMenuItemData))
            CPopupMenuInfoMgr.ShowPopupMenu(array, go.transform, CPopupMenuInfoMgrAlignType.Right)
        end
    end
end

function CLuaQMPKStarWnd:OnClickShiLiZhiXing(go)
    local index=go.transform:GetSiblingIndex()
    local page= self.m_ShiLiZhiXingButton.m_CurrentValue
    page=math.max(page,1)

    if CLuaQMPKMgr.m_ShiLiZhiXingInfo[page] then
        local data=CLuaQMPKMgr.m_ShiLiZhiXingInfo[page][index+1]
        if data then
            local t={}
            local action1 = PopupMenuItemData(LocalString.GetString("发送消息"),DelegateFactory.Action_int(function(index)
                CChatHelper.ShowFriendWnd(data.fromCharacterId, data.fromCharacterName)
            end),false,nil)
            table.insert(t, action1)
            local action2 = PopupMenuItemData(LocalString.GetString("原服角色"),DelegateFactory.Action_int(function(index)
                CPlayerInfoMgr.ShowPlayerInfoWnd(data.fromCharacterId, data.fromCharacterName)
            end),false,nil)
            table.insert(t, action2)
            local action3 = PopupMenuItemData(LocalString.GetString("添加好友"),DelegateFactory.Action_int(function(index)
                Gac2Gas.AddFriend(data.fromCharacterId, "AddFrindAction")
            end),false,nil)
            table.insert(t, action3)
            local action4 = PopupMenuItemData(LocalString.GetString("查看梦岛"),DelegateFactory.Action_int(function(index)
                CPersonalSpaceMgr.Inst:OpenPersonalSpaceWnd(data.fromCharacterId)
            end),false,nil)
            table.insert(t, action4)
            local action5 = PopupMenuItemData(LocalString.GetString("参赛角色"),DelegateFactory.Action_int(function(index)
                CPlayerInfoMgr.ShowPlayerInfoWnd(data.pid, data.name)
            end),false,nil)
            table.insert(t, action5)
            local action6 = PopupMenuItemData(LocalString.GetString("配置学习"),DelegateFactory.Action_int(function(index)
                CLuaQMPKMgr.ShowPlayerConfigWnd(data.pid, data.name)
            end),false,nil)
            if CQuanMinPKMgr.Inst.m_IsQuanMinPKServer then
                table.insert(t, action6)
            end
            local array=Table2Array(t,MakeArrayClass(PopupMenuItemData))
            CPopupMenuInfoMgr.ShowPopupMenu(array, go.transform, CPopupMenuInfoMgrAlignType.Right)
        end
    end
end

function CLuaQMPKStarWnd:RequestRenQiZhiXing()
	if self.m_UrlCoroutine ~= nil then
        Main.Inst:StopCoroutine(self.m_UrlCoroutine)
        self.m_UrlCoroutine=nil
    end
    -- local url=CLuaQMPKMgr.m_ShareUrl.."shareImg/getMdDetailRank"
    local url = CPersonalSpaceMgr.BASE_URL.."/qnm/activity/qmpk/rank"
    -- local url = "http://192.168.131.156:88/qnm/activity/qmpk/rank"

    local form = CHTTPForm()
    local skey = CPersonalSpaceMgr.Inst.Token
    local roleid=CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
    form:AddField("roleid", roleid)
    form:AddField("skey", skey)

    self.m_UrlCoroutine = --Main.Inst:StartCoroutine(HTTPHelper.GetUrl(url, DelegateFactory.Action_bool_string(function (success, ret) 
        Main.Inst:StartCoroutine(CPersonalSpaceMgr.Inst:DoPost(url, form, DelegateFactory.Action_bool_string(function (success, ret)
            -- print(success,ret)
        if success then
            self.m_RenQiZhiXingData={}
            local json = Json.Deserialize(ret)

            local list = CommonDefs.DictGetValue_LuaCall(json,"data")
            if list then
                for i=1,list.Count do
                    local dict=list[i-1]
                    local rank = CommonDefs.DictGetValue_LuaCall(dict,"rank")
                    if rank then
                        rank=tonumber(rank)
                        local original=CommonDefs.DictGetValue_LuaCall(dict,"origin")
                        local current=CommonDefs.DictGetValue_LuaCall(dict,"current")
                        local info={
                            fromCharacterId = tonumber(CommonDefs.DictGetValue_LuaCall(original,"roleid") or 0),
                            serverName = CommonDefs.DictGetValue_LuaCall(original,"servername") or "nil",
                            fromProfession = tonumber(CommonDefs.DictGetValue_LuaCall(original,"careerid") or 0),
                            fromGender = tonumber(CommonDefs.DictGetValue_LuaCall(original,"gender") or 0),
                            fromCharacterName = CommonDefs.DictGetValue_LuaCall(original,"rolename") or "nil",

                            pid = tonumber(CommonDefs.DictGetValue_LuaCall(current,"roleid") or 0),
                            class = tonumber(CommonDefs.DictGetValue_LuaCall(current,"careerid") or 0),
                            gender = tonumber(CommonDefs.DictGetValue_LuaCall(current,"gender") or 0),
                            name = CommonDefs.DictGetValue_LuaCall(current,"rolename") or "nil",
                            avatar = CommonDefs.DictGetValue_LuaCall(original,"avatar") or "nil",
                        }

                        table.insert( self.m_RenQiZhiXingData,info )
                    end
                end
            end

            local totalPage=math.ceil( #self.m_RenQiZhiXingData/5 )
            self.m_RenQiZhiXingTotalPage=totalPage
            if totalPage>0 then
                self.m_RenQiZhiXingButton:SetMinMax(1,totalPage,1)
            else
                self.m_RenQiZhiXingButton:SetMinMax(0,0,1)
            end
            --有数据 不显示tip
            if #self.m_RenQiZhiXingData>0 then
                self.m_RenQiZhiXingTipLabel.text=nil
            else
                self.m_RenQiZhiXingData=nil
            end

            self:InitRenQiZhiXingList(1,totalPage)
        end
        self.m_UrlCoroutine=nil
	end)))
end


return CLuaQMPKStarWnd
