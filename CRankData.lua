-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CFeiShengMgr = import "L10.Game.CFeiShengMgr"
local CJieBaiRankInfoMgr = import "L10.UI.CJieBaiRankInfoMgr"
local CJieBaiRankPlayerInfo = import "L10.UI.CJieBaiRankPlayerInfo"
local CommonDefs = import "L10.Game.CommonDefs"
local CRankData = import "L10.UI.CRankData"
local CRankItemData = import "L10.UI.CRankItemData"
local CRankMainCategory = import "L10.UI.CRankMainCategory"
local CRankSubCategory = import "L10.UI.CRankSubCategory"
local CSwitchMgr = import "L10.Engine.CSwitchMgr"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local Debug = import "UnityEngine.Debug"
local DelegateFactory = import "DelegateFactory"
local EnumClass = import "L10.Game.EnumClass"
local EnumEventType = import "EnumEventType"
local EnumGender = import "L10.Game.EnumGender"
local EnumRankSheet = import "L10.UI.EnumRankSheet"
local EventManager = import "EventManager"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local MengHuaLu_Setting = import "L10.Game.MengHuaLu_Setting"
local MsgPackImpl = import "MsgPackImpl"
local Object = import "System.Object"
--local Rank_Baby = import "L10.Game.Rank_Baby"
--local Rank_Guild = import "L10.Game.Rank_Guild"
--local Rank_House = import "L10.Game.Rank_House"
--local Rank_JieBai = import "L10.Game.Rank_JieBai"
--local Rank_LingShou = import "L10.Game.Rank_LingShou"
local Rank_Mapi = import "L10.Game.Rank_Mapi"
local Rank_Order = import "L10.Game.Rank_Order"
local Rank_Player = import "L10.Game.Rank_Player"
local Rank_PlayerShop = import "L10.Game.Rank_PlayerShop"
local RankPlayerBaseInfo = import "L10.UI.RankPlayerBaseInfo"
local String = import "System.String"
local UInt32 = import "System.UInt32"
local CSpeechCtrlMgr = import "L10.Game.CSpeechCtrlMgr"
local CSpeechCtrlType = import "L10.Game.CSpeechCtrlType"

--local Rank_Activity = import "L10.Game.Rank_Activity"
CRankData.m_Init_CS2LuaHook = function (this) 
    CommonDefs.ListClear(this.allCategories)
    Rank_Activity.Foreach(function (key, data) 
		if data.Status ~= 3 then
			this:AddData(key, data.MainCategory, data.SubCategory, data.TabName, data.HeaderName, data.HeaderName2)
		end
	end)

    Rank_Player.Foreach(DelegateFactory.Action_object_object(function (rankId, player) 
        this:AddRankPlayerData(rankId, player)
    end))

    Rank_Guild.Foreach(function (rankId, guild) 
        if guild.Status ~= 3 and guild.RankType ~= 1 then
            this:AddData(rankId, guild.MainCategory, guild.SubCategory, guild.TabName, guild.HeaderName, guild.HeaderName2)
        end
    end)

    Rank_LingShou.Foreach(function (rankId, lingshou) 
        if lingshou.Status ~= 3 then
            this:AddData(rankId, lingshou.MainCategory, lingshou.SubCategory, lingshou.TabName, lingshou.HeaderName, lingshou.HeaderName2)
        end
    end)
    Rank_House.Foreach(function (rankId, house) 
        if house.Status ~= 3 then
            this:AddData(rankId, house.MainCategory, house.SubCategory, house.TabName, house.HeaderName, house.HeaderName2)
        end
    end)
    Rank_JieBai.Foreach(function (key, data) 
        if data.Status ~= 3 then
            this:AddData(key, data.MainCategory, data.SubCategory, data.TabName, data.HeaderName, data.HeaderName2)
        end
    end)
    if CSwitchMgr.EnableYangYu then
        Rank_Baby.Foreach(function (key, data) 
            if data.Status ~= 3 then
                this:AddData(key, data.MainCategory, data.SubCategory, data.TabName, data.HeaderName, data.HeaderName2)
            end
        end)
    end

    Rank_Sect.Foreach(function(key, data)
        if data.Status ~=3 then
            this:AddData(key, data.MainCategory, data.SubCategory, data.TabName, data.HeaderName, data.HeaderName2)
        end
    end)

    this:SortSubCategory()
end
CRankData.m_AddRankPlayerData_CS2LuaHook = function (this, rankId, player) 
    if player.Status ~= 3 and player.RankType == 0 then
        if not CFeiShengMgr.Inst.IsOpen and player.FanShenNotDisplay == 1 then
            return
        end
        this:AddData(rankId, player.MainCategory, player.SubCategory, player.TabName, player.HeaderName, player.HeaderName2)
    end
end
CRankData.m_SortSubCategory_CS2LuaHook = function (this) 
    do
        local i = 0
        while i < this.allCategories.Count do
            local list = this.allCategories[i].children
            CommonDefs.ListSort1(list, typeof(CRankSubCategory), DelegateFactory.Comparison_CRankSubCategory(function (cat1, cat2) 
                return NumberCompareTo(cat1.order, cat2.order)
                --按order升序排列
            end))
            i = i + 1
        end
    end
end
CRankData.m_AddData_CS2LuaHook = function (this, rankId, mainCatName, SubCatName, tabName, headerName, headerName2) 
    --对rank_order进行缓存，避免无谓的getdata
    local order = System.Int32.MaxValue
    --order升序排列，默认取最大值排最后
    if not (function () 
        local __try_get_result
        __try_get_result, order = CommonDefs.DictTryGet(this.subCategoryName2OrderDict, typeof(String), SubCatName, typeof(Int32))
        return __try_get_result
    end)() then
        local rankOrder = Rank_Order.GetData(SubCatName)
        order = (rankOrder ~= nil) and rankOrder.Order or System.Int32.MaxValue
        CommonDefs.DictAdd(this.subCategoryName2OrderDict, typeof(String), SubCatName, typeof(Int32), order)
    end
    --没有开启飞升的服务器，“凡身等级”就改为“等级”显示
    if not CFeiShengMgr.Inst.IsOpen then
        if SubCatName == LocalString.GetString("凡身等级") then
            SubCatName = LocalString.GetString("等级")
        end
        if headerName == LocalString.GetString("凡身等级") then
            headerName = LocalString.GetString("等级")
        end
    end

    local data = CRankItemData(rankId, mainCatName, SubCatName, tabName, headerName, headerName2)
    do
        local i = 0
        while i < this.allCategories.Count do
            if this.allCategories[i].name == mainCatName then
                local subCategory = this.allCategories[i]:GetSubCategory(SubCatName)
                if subCategory ~= nil then
                    --副类型已经存在
                    if not subCategory:Contains(rankId) then
                        CommonDefs.ListAdd(subCategory.children, typeof(CRankItemData), data)
                    end
                    return
                else
                    --新建一个副类型
                    subCategory = CreateFromClass(CRankSubCategory, SubCatName, order)
                    CommonDefs.ListAdd(subCategory.children, typeof(CRankItemData), data)
                    CommonDefs.ListAdd(this.allCategories[i].children, typeof(CRankSubCategory), subCategory)
                end
                return
            end
            i = i + 1
        end
    end

    --如果不属于任何主类，就新建一个主类
    local mainCat = CreateFromClass(CRankMainCategory, mainCatName)
    local subCat = CreateFromClass(CRankSubCategory, SubCatName, order)
    CommonDefs.ListAdd(subCat.children, typeof(CRankItemData), data)
    CommonDefs.ListAdd(mainCat.children, typeof(CRankSubCategory), subCat)
    CommonDefs.ListAdd(this.allCategories, typeof(CRankMainCategory), mainCat)
end
CRankData.m_GetRankSheetKey_CS2LuaHook = function (sheet) 
    repeat
        local default = sheet
        if default == (EnumRankSheet.ePlayer) then
            return "Player"
        elseif default == (EnumRankSheet.eGuild) then
            return "Guild"
        elseif default == (EnumRankSheet.eLingShou) then
            return "LingShou"
        elseif default == (EnumRankSheet.ePlayerShop) then
            return "PlayerShop"
        elseif default == (EnumRankSheet.eHouse) then
            return "House"
        elseif default == (EnumRankSheet.eJieBai) then
            return "JieBai"
        elseif default == (EnumRankSheet.eMapi) then
            return "Mapi"
        elseif default == EnumRankSheet.eBaby then
            return "Baby"
        elseif default == EnumRankSheet.eSect then
            return "Sect"
        else
            return ""
        end
    until 1
end
--#region 界面数据结构
CRankMainCategory.m_GetSubCategory_CS2LuaHook = function (this, subName) 
    do
        local i = 0
        while i < this.children.Count do
            if this.children[i].name == subName then
                return this.children[i]
            end
            i = i + 1
        end
    end
    return nil
end
CRankSubCategory.m_Contains_CS2LuaHook = function (this, rankId) 
    do
        local i = 0
        while i < this.children.Count do
            if this.children[i].rankId == rankId then
                return true
            end
            i = i + 1
        end
    end
    return false
end
--#endregion
CLuaRankData = {}
CLuaRankData.m_CurRankId = 0


Gas2Gac.SendGuildRank = function (rankId, rankData, guildRank, guildValue, guildName, memberCount, guildScale) 
    CLuaRankData.m_CurRankId = rankId
    local rank_guild = Rank_Guild.GetData(rankId)
    CommonDefs.ListClear(CRankData.Inst.RankList)
    if rank_guild ~= nil then
        if rankData.Length > 0 then
            local list = TypeAs(MsgPackImpl.unpack(rankData), typeof(MakeGenericClass(List, Object)))
            local count = math.floor(list.Count / 5)
            do
                local i = 0
                while i < count do
                    local playerId = math.floor(tonumber(list[i * 5] or 0))
                    --这里的playerId就是帮会id
                    local value = tonumber(list[i * 5 + 1])
                    local gname = tostring(list[i * 5 + 2])
                    local member = math.floor(tonumber(list[i * 5 + 3] or 0))
                    local scale = math.floor(tonumber(list[i * 5 + 4] or 0))

                    local RankGuidUnit = RankPlayerBaseInfo(rankId, playerId, "", gname, scale, i + 1, CommonDefs.ConvertIntToEnum(typeof(EnumClass), 0), member, value, EnumRankSheet.eGuild, "", "", "", "", "", "", 0, 0, "", "", nil, nil, 0, "")
                    CommonDefs.ListAdd(CRankData.Inst.RankList, typeof(RankPlayerBaseInfo), RankGuidUnit)
                    i = i + 1
                end
            end
        end
        CRankData.Inst.MainPlayerRankInfo = RankPlayerBaseInfo(rankId, 0, "", guildName, guildScale, guildRank, CommonDefs.ConvertIntToEnum(typeof(EnumClass), 0), memberCount, guildValue, EnumRankSheet.eGuild, "", "", "", "", "", "", 0, 0, "", "", nil, nil, 0, "")
        EventManager.Broadcast(EnumEventType.OnRankDataReady)
    end
end

function Gas2Gac.SendSectRank(rankId, rankData, sectRank, sectValue, sectName, memberCount, sectlevel)
    CLuaRankData.m_CurRankId = rankId
    local rank = Rank_Sect.GetData(rankId)
    CommonDefs.ListClear(CRankData.Inst.RankList)
    if rank ~= nil then
        if rankData.Length > 0 then
            local list = TypeAs(MsgPackImpl.unpack(rankData), typeof(MakeGenericClass(List, Object)))
            for i = 0, list.Count - 1, 5 do
                -- body
                local id = math.floor(tonumber(list[i] or 0))
                local value = math.floor(tonumber(list[i + 1] or 0))
                local name = tostring(list[i + 2])
                local count = math.floor(tonumber(list[i + 3] or 0))
                local level = math.floor(tonumber(list[i + 4] or 0))
                local rank = math.floor(i / 5) + 1
                local RankSectUnit = RankPlayerBaseInfo(rankId, id, "", name, level, rank, CommonDefs.ConvertIntToEnum(typeof(EnumClass), 0), count, value, EnumRankSheet.eSect, "", "", "", "", "", "", 0, 0, "", "", nil, nil, 0, "")
                CommonDefs.ListAdd(CRankData.Inst.RankList, typeof(RankPlayerBaseInfo), RankSectUnit)
            end
        end
        CRankData.Inst.MainPlayerRankInfo = RankPlayerBaseInfo(rankId, 0, "",sectName, sectlevel, sectRank, CommonDefs.ConvertIntToEnum(typeof(EnumClass), 0), memberCount, sectValue, EnumRankSheet.eSect, "", "", "", "", "", "", 0, 0, "", "", nil, nil, 0, "")
        EventManager.Broadcast(EnumEventType.OnRankDataReady)
    end
end
Gas2Gac.SendCrossGuildRank = function (rankId, rankData, guildRank, guildValue, guildExtra, guildName, memberCount, guildScale, ServerGroupId, ServerName)
    CLuaRankData.m_CurRankId = rankId
    if rankData.Length > 0 then
        local list = TypeAs(MsgPackImpl.unpack(rankData), typeof(MakeGenericClass(List, Object)))
        if list ~= nil then
            CommonDefs.ListClear(CRankData.Inst.RankList)
            do
                local i = 0 local count = list.Count
                while i < count do
                    local playerId = math.floor(tonumber(list[i] or 0))
                    --这里的playerId就是帮会id
                    local value = tonumber(list[i + 1])
                    local extra = TypeAs(list[i + 2], typeof(MakeArrayClass(System.Byte)))
                    local gname = tostring(list[i + 3])
                    local member = math.floor(tonumber(list[i + 4] or 0))
                    local scale = math.floor(tonumber(list[i + 5] or 0))
                    local serverGroupId = math.floor(tonumber(list[i + 6] or 0))
                    local serverName = tostring(list[i + 7])

                    local RankGuidUnit = RankPlayerBaseInfo(rankId, playerId, "", gname, scale, math.floor(i / 8) + 1, CommonDefs.ConvertIntToEnum(typeof(EnumClass), 0), member, value, EnumRankSheet.eCrossGuild, "", "", "", "", "", "", 0, 0, "", "", nil, extra, serverGroupId, serverName)
                    CommonDefs.ListAdd(CRankData.Inst.RankList, typeof(RankPlayerBaseInfo), RankGuidUnit)
                    i = i + 8
                end
            end
        end
        CRankData.Inst.MainPlayerRankInfo = RankPlayerBaseInfo(rankId, 0, "", guildName, guildScale, guildRank, CommonDefs.ConvertIntToEnum(typeof(EnumClass), 0), memberCount, guildValue, EnumRankSheet.eCrossGuild, "", "", "", "", "", "", 0, 0, "", "", nil, guildExtra, ServerGroupId, ServerName)
        EventManager.Broadcast(EnumEventType.OnRankDataReady)
    end
end
Gas2Gac.SendHouseRank = function (rankId, rankData, houseRank, houseValue, houseName, ownerName1, ownerName2, ownerId1, ownerId2) 
    CLuaRankData.m_CurRankId = rankId
    local rank_house = Rank_House.GetData(rankId)
    CommonDefs.ListClear(CRankData.Inst.RankList)
    if rank_house ~= nil then
        if rankData.Length > 0 then
            local list = TypeAs(MsgPackImpl.unpack(rankData), typeof(MakeGenericClass(List, Object)))
            local segNum = 7
            local count = math.floor(list.Count / segNum)
            do
                local i = 0
                while i < count do
                    local houseId = tostring(list[i * segNum])
                    local value = tonumber(list[i * segNum + 1])
                    local house_name = tostring(list[i * segNum + 2])
                    local owner_name1 = tostring(list[i * segNum + 3])
                    local owner_name2 = tostring(list[i * segNum + 4])
                    local owner_id1 = tonumber(list[i * segNum + 5])
                    local owner_id2 = tonumber(list[i * segNum + 6])
                    local rankHouseUnit = RankPlayerBaseInfo(0, 0, "", "", 0, i + 1, CommonDefs.ConvertIntToEnum(typeof(EnumClass), 0), 0, value, EnumRankSheet.eHouse, "", "", house_name, houseId, owner_name1, owner_name2, math.floor(owner_id1), math.floor(owner_id2), "", "", nil, nil, 0, "")
                    CommonDefs.ListAdd(CRankData.Inst.RankList, typeof(RankPlayerBaseInfo), rankHouseUnit)
                    i = i + 1
                end
            end
        end
        CRankData.Inst.MainPlayerRankInfo = RankPlayerBaseInfo(0, 0, "", "", 0, houseRank, CommonDefs.ConvertIntToEnum(typeof(EnumClass), 0), 0, houseValue, EnumRankSheet.eHouse, "", "", houseName, "", ownerName1, ownerName2, math.floor(ownerId1), math.floor(ownerId2), "", "", nil, nil, 0, "")
        EventManager.Broadcast(EnumEventType.OnRankDataReady)
    end
end
Gas2Gac.SendPlayerRank = function (rankId, rankData, playerRank, playerValue, playerExtra, playerName, playerGrade, playerClass, guildName, playerStage, countryCode)
    CLuaRankData.m_CurRankId = rankId
    local lingshouId = ""
    local lingshouName = ""
    local playershopId = ""
    local playershopName = ""
    local babyLevel = ""
    local babyQiChang = ""
    local isLingshouRank = Rank_LingShou.Exists(rankId)
    local isPlayerShopRank = Rank_PlayerShop.Exists(rankId)
    local isMapiRank = Rank_Mapi.Exists(rankId)
    local isMengHuaLu = MengHuaLu_Setting.GetData().WeekMaxLayerRankId == rankId
    if isLingshouRank or isMapiRank then
        local extraList = MsgPackImpl.unpack(playerExtra)
        if extraList ~= nil then
            local list = TypeAs(extraList, typeof(MakeGenericClass(List, Object)))
            if list ~= nil and list.Count > 0 then
                lingshouId = tostring(list[0])
                lingshouName = tostring(list[1])
            end
        end
    end
    if isPlayerShopRank then
        local extraList = MsgPackImpl.unpack(playerExtra)
        if extraList ~= nil then
            local list = TypeAs(extraList, typeof(MakeGenericClass(List, Object)))
            if list ~= nil and list.Count > 0 then
                playershopId = tostring(list[0])
                playershopName = CSpeechCtrlMgr.Inst:CheckIfNeedSpeechCtrl(CSpeechCtrlType.Type_PlayerShop_Name, false) and LocalString.GetString("玩家商店") or tostring(list[1])
            end
        end
    end

    if isMengHuaLu then
        local extraList = MsgPackImpl.unpack(playerExtra)
        if extraList ~= nil then
            local list = TypeAs(extraList, typeof(MakeGenericClass(List, Object)))
            if list ~= nil and list.Count > 0 then
                babyLevel = tostring(list[0])
                babyQiChang = tostring(list[1])
            end
        end
    end

    local rank_player = Rank_Player.GetData(rankId)
    if isLingshouRank then
        CRankData.Inst.MainPlayerRankInfo = RankPlayerBaseInfo(rankId, CClientMainPlayer.Inst.Id, playerName, guildName, playerGrade, playerRank, CommonDefs.ConvertIntToEnum(typeof(EnumClass), playerClass), 0, playerValue, EnumRankSheet.eLingShou, lingshouName, lingshouId, "", "", "", "", 0, 0, "", "", nil, nil, 0, "", countryCode)
    elseif isPlayerShopRank then
        CRankData.Inst.MainPlayerRankInfo = RankPlayerBaseInfo(rankId, CClientMainPlayer.Inst.Id, playerName, guildName, playerGrade, playerRank, CommonDefs.ConvertIntToEnum(typeof(EnumClass), playerClass), 0, playerValue, EnumRankSheet.ePlayerShop, playershopName, playershopId, "", "", "", "", 0, 0, "", "", nil, nil, 0, "", countryCode)
    elseif isMapiRank then
        CRankData.Inst.MainPlayerRankInfo = RankPlayerBaseInfo(rankId, CClientMainPlayer.Inst.Id, playerName, guildName, playerGrade, playerRank, CommonDefs.ConvertIntToEnum(typeof(EnumClass), playerClass), 0, playerValue, EnumRankSheet.eMapi, lingshouName, lingshouId, "", "", "", "", 0, 0, "", "", nil, nil, 0, "", countryCode)
    elseif isMengHuaLu then
        CRankData.Inst.MainPlayerRankInfo = RankPlayerBaseInfo(rankId, CClientMainPlayer.Inst.Id, playerName, guildName, playerGrade, playerRank, CommonDefs.ConvertIntToEnum(typeof(EnumClass), playerClass), 0, playerValue, EnumRankSheet.eBaby, babyLevel, babyQiChang, "", "", "", "", 0, 0, "", "", nil, nil, 0, "", countryCode)
    else
        if rank_player and rank_player.IsMultiLevel == 1 then
            local showValueFormula = AllFormulas.Rank_Player[rankId] and AllFormulas.Rank_Player[rankId].ShowValueFormula
            playerValue = showValueFormula and showValueFormula(nil, nil, { playerValue }) or playerValue
        end
        CRankData.Inst.MainPlayerRankInfo = RankPlayerBaseInfo(rankId, CClientMainPlayer.Inst.Id, playerName, guildName, playerGrade, playerRank, CommonDefs.ConvertIntToEnum(typeof(EnumClass), playerClass), 0, playerValue, EnumRankSheet.ePlayer, lingshouName, lingshouId, "", "", "", "", 0, 0, "", "", nil, playerExtra, 0, "", countryCode)
    end

    local entitySize = 9
    CommonDefs.ListClear(CRankData.Inst.RankList)
    if rank_player ~= nil then
        if rankData.Length > 0 then
            local list = TypeAs(MsgPackImpl.unpack(rankData), typeof(MakeGenericClass(List, Object)))
            local count = math.floor(list.Count / entitySize)
            local isMultiLevel = rank_player.IsMultiLevel or 0
            local showValueFormula = AllFormulas.Rank_Player[rankId] and AllFormulas.Rank_Player[rankId].ShowValueFormula
            local tempValue = { 0 }
            do
                local i = 0
                while i < count do
                    local playerId = math.floor(tonumber(list[i * entitySize] or 0))
                    local value = tonumber(list[i * entitySize + 1])
                    local extra = TypeAs(list[i * entitySize + 2], typeof(MakeArrayClass(System.Byte)))
                    local name = tostring(list[i * entitySize + 3])
                    local grade = math.floor(tonumber(list[i * entitySize + 4] or 0))
                    local cls = math.floor(tonumber(list[i * entitySize + 5] or 0))
                    local gname = tostring(list[i * entitySize + 6])
                    local stage = math.floor(tonumber(list[i * entitySize + 7] or 0))
                    local subCountryCode = math.floor(tonumber(list[i * entitySize + 8] or 0))

                    local lsName = ""
                    local lsId = ""

                    if isMultiLevel == 1 and showValueFormula then
                        tempValue[1] = value
                        value = showValueFormula(nil, nil, tempValue)
                    end

                    if isLingshouRank or isPlayerShopRank or isMapiRank or isMengHuaLu then
                        local extraList = MsgPackImpl.unpack(extra)
                        if extraList ~= nil then
                            local sllist = TypeAs(extraList, typeof(MakeGenericClass(List, Object)))
                            lsId = tostring(sllist[0])
                            lsName = tostring(sllist[1])
                        end
                    end

                    local RankDataUnit = nil
                    if isLingshouRank then
                        RankDataUnit = RankPlayerBaseInfo(rankId, playerId, name, gname, grade, i + 1, CommonDefs.ConvertIntToEnum(typeof(EnumClass), cls), 0, value, EnumRankSheet.eLingShou, lsName, lsId, "", "", "", "", 0, 0, "", "", nil, nil, 0, "", subCountryCode)
                    elseif isPlayerShopRank then
                        RankDataUnit = RankPlayerBaseInfo(rankId, playerId, name, gname, grade, i + 1, CommonDefs.ConvertIntToEnum(typeof(EnumClass), cls), 0, value, EnumRankSheet.ePlayerShop, lsName, lsId, "", "", "", "", 0, 0, "", "", nil, nil, 0, "", subCountryCode)
                    elseif isMapiRank then
                        RankDataUnit = RankPlayerBaseInfo(rankId, playerId, name, gname, grade, i + 1, CommonDefs.ConvertIntToEnum(typeof(EnumClass), cls), 0, value, EnumRankSheet.eMapi, lsName, lsId, "", "", "", "", 0, 0, "", "", nil, nil, 0, "", subCountryCode)
                    elseif isMengHuaLu then
                        RankDataUnit = RankPlayerBaseInfo(rankId, playerId, name, gname, grade, i + 1, CommonDefs.ConvertIntToEnum(typeof(EnumClass), cls), 0, value, EnumRankSheet.eBaby, lsId, lsName, "", "", "", "", 0, 0, "", "", nil, nil, 0, "", subCountryCode)
                    else
                        RankDataUnit = RankPlayerBaseInfo(rankId, playerId, name, gname, grade, i + 1, CommonDefs.ConvertIntToEnum(typeof(EnumClass), cls), 0, value, EnumRankSheet.ePlayer, lsName, lsId, "", "", "", "", 0, 0, "", "", nil, extra, 0, "", subCountryCode)
                    end
                    CommonDefs.ListAdd(CRankData.Inst.RankList, typeof(RankPlayerBaseInfo), RankDataUnit)
                    i = i + 1
                end
            end
        end
        local pExtra = MsgPackImpl.unpack(playerExtra)
        EventManager.Broadcast(EnumEventType.OnRankDataReady)
    else
        Debug.Log("rank not found " .. rankId)
    end
end
Gas2Gac.SendJieBaiRank = function (rankId, rankData, jieBaiRank, jieBaiValue, jieBaiTitle, jieBaiPlayerClsListUD, playerjiebaiKey) 
    CLuaRankData.m_CurRankId = rankId
    local rank_jiebai = Rank_JieBai.GetData(rankId)
    CommonDefs.ListClear(CRankData.Inst.RankList)
    if rank_jiebai ~= nil then
        if rankData.Length > 0 then
            local list = TypeAs(MsgPackImpl.unpack(rankData), typeof(MakeGenericClass(List, Object)))
            local segNum = 4
            local count = math.floor(list.Count / segNum)
            do
                local i = 0
                while i < count do
                    local jieBaiKey = tostring(list[i * segNum])
                    local value = tonumber(list[i * segNum + 1])
                    local title = tostring(list[i * segNum + 2])
                    local clsList = TypeAs(list[i * segNum + 3], typeof(MakeGenericClass(List, Object)))
                    local classList = CreateFromClass(MakeGenericClass(List, UInt32))
                    if clsList ~= nil then
                        do
                            local j = 0
                            while j < clsList.Count do
                                CommonDefs.ListAdd(classList, typeof(UInt32), System.UInt32.Parse(ToStringWrap(clsList[j])))
                                j = j + 1
                            end
                        end
                    end
                    local rankJiebaiUnit = RankPlayerBaseInfo(0, 0, "", "", 0, i + 1, CommonDefs.ConvertIntToEnum(typeof(EnumClass), 0), 0, value, EnumRankSheet.eJieBai, "", "", "", "", "", "", 0, 0, jieBaiKey, title, classList, nil, 0, "")
                    CommonDefs.ListAdd(CRankData.Inst.RankList, typeof(RankPlayerBaseInfo), rankJiebaiUnit)
                    i = i + 1
                end
            end
        end

        local playerClsList = TypeAs(MsgPackImpl.unpack(jieBaiPlayerClsListUD), typeof(MakeGenericClass(List, Object)))
        local playerClass = CreateFromClass(MakeGenericClass(List, UInt32))
        if playerClsList ~= nil then
            do
                local i = 0
                while i < playerClsList.Count do
                    CommonDefs.ListAdd(playerClass, typeof(UInt32), System.UInt32.Parse(ToStringWrap(playerClsList[i])))
                    i = i + 1
                end
            end
        end
        CRankData.Inst.MainPlayerRankInfo = RankPlayerBaseInfo(rankId, 0, "", "", 0, jieBaiRank, CommonDefs.ConvertIntToEnum(typeof(EnumClass), 0), 0, jieBaiValue, EnumRankSheet.eJieBai, "", "", "", "", "", "", 0, 0, playerjiebaiKey, jieBaiTitle, playerClass, nil, 0, "")
        EventManager.Broadcast(EnumEventType.OnRankDataReady)
    end
end
Gas2Gac.QueryRankJieBaiInfoResult = function (jieBaiKey, playerInfoListUD) 
    local list = TypeAs(MsgPackImpl.unpack(playerInfoListUD), typeof(MakeGenericClass(List, Object)))
    CommonDefs.ListClear(CJieBaiRankInfoMgr.jiebaiInfo)
    do
        local i = 0
        while i < list.Count do
            local info = TypeAs(list[i], typeof(MakeGenericClass(List, Object)))
            if info ~= nil and info.Count == 7 then
                local index = System.Int32.Parse(ToStringWrap(info[0]))
                local id = System.UInt64.Parse(ToStringWrap(info[1]))
                local name = ToStringWrap(info[2])
                local cls = CommonDefs.ConvertIntToEnum(typeof(EnumClass), System.Int32.Parse(ToStringWrap(info[3])))
                local gender = CommonDefs.ConvertIntToEnum(typeof(EnumGender), System.Int32.Parse(ToStringWrap(info[4])))
                local grade = System.Int32.Parse(ToStringWrap(info[5]))
                local personalTitle = ToStringWrap(info[6])
                CommonDefs.ListAdd(CJieBaiRankInfoMgr.jiebaiInfo, typeof(CJieBaiRankPlayerInfo), CreateFromClass(CJieBaiRankPlayerInfo, index, id, name, cls, gender, grade, personalTitle))
            end
            i = i + 1
        end
    end
    CommonDefs.ListSort1(CJieBaiRankInfoMgr.jiebaiInfo, typeof(CJieBaiRankPlayerInfo), DelegateFactory.Comparison_CJieBaiRankPlayerInfo(function (data1, data2) 
        return NumberCompareTo(data1.index, data2.index)
    end))
    CUIManager.ShowUI(CUIResources.JieBaiRankInfoWnd)
end
Gas2Gac.SendBabyRank = function (rankId, rankData, babyRank, babyValue, babyName, babyGrade, ownerName, guildName, ownerId) 
    --Debug.Log("SendBabyRank " + rankId.ToString());
    CLuaRankData.m_CurRankId = rankId
    local rankBaby = Rank_Baby.GetData(rankId)
    CommonDefs.ListClear(CRankData.Inst.RankList)
    if rankBaby ~= nil then
        if rankData.Length > 0 then
            local list = TypeAs(MsgPackImpl.unpack(rankData), typeof(MakeGenericClass(List, Object)))
            local segNum = 7
            local count = math.floor(list.Count / segNum)
            do
                local i = 0
                while i < count do
                    local babyId = tostring(list[i * segNum])
                    local value = tonumber(list[i * segNum + 1])
                    local name = tostring(list[i * segNum + 2])

                    local grade = tonumber(list[i * segNum + 3])
                    local owner = tostring(list[i * segNum + 4])
                    local guild = tostring(list[i * segNum + 5])
                    local _ownerId = math.floor(tonumber(list[i * segNum + 6] or 0))
                    local babyInfo = RankPlayerBaseInfo(0, _ownerId, "", "", 0, i + 1, CommonDefs.ConvertIntToEnum(typeof(EnumClass), 0), 0, value, EnumRankSheet.eBaby, "", "", "", "", "", "", 0, 0, "", "", nil, nil, 0, "")
                    babyInfo.data = InitializeList(CreateFromClass(MakeGenericClass(List, Object)), Object, owner, name, guild, value)
                    CommonDefs.ListAdd(CRankData.Inst.RankList, typeof(RankPlayerBaseInfo), babyInfo)
                    i = i + 1
                end
            end
        end

        CRankData.Inst.MainPlayerRankInfo = RankPlayerBaseInfo(rankId, 0, "", "", 0, babyRank, CommonDefs.ConvertIntToEnum(typeof(EnumClass), 0), 0, babyValue, EnumRankSheet.eBaby, "", "", "", "", "", "", 0, 0, "", "", nil, nil, 0, "")
        if babyGrade > 0 then
            CRankData.Inst.MainPlayerRankInfo.data = InitializeList(CreateFromClass(MakeGenericClass(List, Object)), Object, ownerName, babyName, guildName, babyValue)
        end
        EventManager.Broadcast(EnumEventType.OnRankDataReady)
    end
end
