local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local UISprite = import "UISprite"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CUICommonDef = import "L10.UI.CUICommonDef"
local EnumClass = import "L10.Game.EnumClass"
local Profession = import "L10.Game.Profession"

LuaQieCuoMessageBox = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaQieCuoMessageBox, "OkButton", "OkButton", GameObject)
RegistChildComponent(LuaQieCuoMessageBox, "CancelButton", "CancelButton", GameObject)
RegistChildComponent(LuaQieCuoMessageBox, "TextLabel", "TextLabel", UILabel)
RegistChildComponent(LuaQieCuoMessageBox, "HeadTex", "HeadTex", CUITexture)
RegistChildComponent(LuaQieCuoMessageBox, "LvLb", "LvLb", UILabel)
RegistChildComponent(LuaQieCuoMessageBox, "PowerLb", "PowerLb", UILabel)
RegistChildComponent(LuaQieCuoMessageBox, "JobIcon1", "JobIcon1", UISprite)
RegistChildComponent(LuaQieCuoMessageBox, "JobIcon2", "JobIcon2", UISprite)
RegistChildComponent(LuaQieCuoMessageBox, "JobIcon3", "JobIcon3", UISprite)
RegistChildComponent(LuaQieCuoMessageBox, "JobIcon4", "JobIcon4", UISprite)
RegistChildComponent(LuaQieCuoMessageBox, "JobIcon5", "JobIcon5", UISprite)
RegistChildComponent(LuaQieCuoMessageBox, "Jobs", "Jobs", GameObject)
RegistChildComponent(LuaQieCuoMessageBox, "CancelBtnLb", "CancelBtnLb", UILabel)

--@endregion RegistChildComponent end

function LuaQieCuoMessageBox:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.OkButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOkButtonClick(go)
	end)

	UIEventListener.Get(self.CancelButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCancelButtonClick(go)
	end)

    --@endregion EventBind end
end

function LuaQieCuoMessageBox:Init()
	local data = LuaFlagDuelMgr.QieCuoMessageInfo
	if data == nil then return end

	local msg = data.Msg
	if data.IsNml then
		self.LvLb.text = "lv."..tostring(data.PlayerLv)
	else
		self.LvLb.text = g_MessageMgr:DecorateTextWithFeiShengColor("lv."..tostring(data.PlayerLv))
	end
	self.PowerLb.text = tostring(data.Power)
	self.HeadTex:LoadNPCPortrait(CUICommonDef.GetPortraitName(data.PlayerClass, data.PlayerGender,0),false)

	local teams = data.Teams
    if teams and #teams > 0 then
		local jobs = {self.JobIcon1,self.JobIcon2,self.JobIcon3,self.JobIcon4,self.JobIcon5}
		local tcount = #teams

		for i=1,tcount do
			jobs[i].gameObject:SetActive(true)
			local mjob = teams[i]
			jobs[i].spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), mjob))
		end

		for i=tcount+1,5 do
			jobs[i].gameObject:SetActive(false)
		end

		self.Jobs:SetActive(true)
	else
		self.Jobs:SetActive(false)
    end
	self.TextLabel.text = msg--g_MessageMgr:FormatMessage("QIECUO_CONFIRM",name)
	
	local btntxt = self.CancelBtnLb.text
	local timeout = data.TimeOut
	if self.m_TimeOutTick then
		UnRegisterTick(self.m_TimeOutTick)
		self.m_TimeOutTick = nil
	end
	self.m_TimeOutTick = RegisterTick(function()
		self.CancelBtnLb.text = SafeStringFormat3("%s(%d)",btntxt,data.TimeOut)
		if data.TimeOut <= 0 then
			UnRegisterTick(self.m_TimeOutTick)
			self.m_TimeOutTick = nil
			local data = LuaFlagDuelMgr.QieCuoMessageInfo
			if data  then 
				Gac2Gas.SendQieCuoInviteResult(data.PlayerID,3)
			end
			CUIManager.CloseUI(CLuaUIResources.QieCuoMessageBox)
		else
			data.TimeOut = data.TimeOut - 1
		end
	end,1000)
end

function LuaQieCuoMessageBox:OnDisable()
	if self.m_TimeOutTick then
		UnRegisterTick(self.m_TimeOutTick)
		self.m_TimeOutTick = nil
	end
end

--@region UIEvent

function LuaQieCuoMessageBox:OnOkButtonClick(go)
	local data = LuaFlagDuelMgr.QieCuoMessageInfo
	if data  then 
		Gac2Gas.SendQieCuoInviteResult(data.PlayerID,1)
	end
	CUIManager.CloseUI(CLuaUIResources.QieCuoMessageBox)
end

function LuaQieCuoMessageBox:OnCancelButtonClick(go)
	local data = LuaFlagDuelMgr.QieCuoMessageInfo
	if data  then 
		Gac2Gas.SendQieCuoInviteResult(data.PlayerID,2)
	end
	CUIManager.CloseUI(CLuaUIResources.QieCuoMessageBox)
end


--@endregion UIEvent

