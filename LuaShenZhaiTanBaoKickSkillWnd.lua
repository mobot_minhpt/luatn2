local Vector2 = import "UnityEngine.Vector2"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UISlider = import "UISlider"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local UILongPressButton = import "L10.UI.UILongPressButton"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CClientNpc = import "L10.Game.CClientNpc"
local UIPanel = import "UIPanel"
local PlayerSettings = import "L10.Game.PlayerSettings"

LuaShenZhaiTanBaoKickSkillWnd = class()

RegistChildComponent(LuaShenZhaiTanBaoKickSkillWnd, "KickButton", "KickButton", UILongPressButton)
RegistChildComponent(LuaShenZhaiTanBaoKickSkillWnd, "ProgressBar", "ProgressBar", UISlider)
RegistChildComponent(LuaShenZhaiTanBaoKickSkillWnd, "ResetButton", "ResetButton", GameObject)
RegistChildComponent(LuaShenZhaiTanBaoKickSkillWnd, "SwitchTargetButton", "SwitchTargetButton", GameObject)
RegistChildComponent(LuaShenZhaiTanBaoKickSkillWnd, "HpRecoverButton", "HpRecoverButton", GameObject)

function LuaShenZhaiTanBaoKickSkillWnd:Awake()
    self.KickButton.OnLongPressDelegate = DelegateFactory.Action(function ()
        self:TrySetKickTarget()
        if self:CheckKickCondition() then
            self.curSliderValue = 0
            self.isAdditive = true
            self.isInPressState = true 
        end
    end)
    self.KickButton.OnLongPressEndDelegate = DelegateFactory.Action(function ()
        if self:CheckKickCondition() then
            Gac2Gas.Double11SZTB_PushCart(self.maxPushForce * self.curSliderValue)
        end
        self.isInPressState = false
        self.ProgressBar.value = 0
    end)
    
    UIEventListener.Get(self.ResetButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        self:OnResetButtonClick()
    end)

    UIEventListener.Get(self.SwitchTargetButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        self:OnSwitchTargetButtonClick()
    end)

    self.ProgressBar.value = 0
end

function LuaShenZhaiTanBaoKickSkillWnd:Init()
    local SZTBConfigData = Double11_SZTB.GetData()

    self.progressBarMaxRange = 0.35
    self.maxPushForce = SZTBConfigData.PushPowerNumber / self.progressBarMaxRange
    self.addSliderPerFrame = 0.01
    self.curSliderValue = 0
    self.isAdditive = true
    self.isInPressState = false

    self.ballNpcIds = {}
    for i = 1, SZTBConfigData.CartInfos.Length do
        local NpcId = SZTBConfigData.CartInfos[i-1][0]
        self.ballNpcIds[NpcId] = true
    end
end

function LuaShenZhaiTanBaoKickSkillWnd:Update()
    if self.isInPressState then
        if self.isAdditive then
            local nextValue = self.curSliderValue + self.addSliderPerFrame
            if nextValue > self.progressBarMaxRange then
                self.curSliderValue = self.curSliderValue - self.addSliderPerFrame
                self.isAdditive = false
            else
                self.curSliderValue = nextValue
            end
        else
            local nextValue = self.curSliderValue - self.addSliderPerFrame
            if nextValue < 0 then
                self.curSliderValue = self.curSliderValue + self.addSliderPerFrame
                self.isAdditive = true
            else
                self.curSliderValue = nextValue
            end
        end
        self.ProgressBar.value = self.curSliderValue
    end
end

function LuaShenZhaiTanBaoKickSkillWnd:TrySetKickTarget()
    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer and mainPlayer.Target then
        if TypeIs(mainPlayer.Target, typeof(CClientNpc)) then
            if mainPlayer.Target.TemplateId then
                if self.ballNpcIds[mainPlayer.Target.TemplateId] then
                else
                    self:OnSwitchTargetButtonClick()
                end
            else
                self:OnSwitchTargetButtonClick()
            end
        else
            self:OnSwitchTargetButtonClick()
        end
    else
        self:OnSwitchTargetButtonClick()
    end
end

function LuaShenZhaiTanBaoKickSkillWnd:CheckKickCondition()
    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer and mainPlayer.Target then
        if TypeIs(mainPlayer.Target, typeof(CClientNpc)) then
            if mainPlayer.Target.TemplateId then
                return self.ballNpcIds[mainPlayer.Target.TemplateId]
            else
                return false
            end
        else
            return false    
        end
    end
    return false
end

function LuaShenZhaiTanBaoKickSkillWnd:OnResetButtonClick()
    if CClientMainPlayer.Inst then
        local resetSkillId = Double11_SZTB.GetData().ResetCartSkillId
        
        if CClientMainPlayer.Inst:CheckSkillCDAndAlive(resetSkillId) then
            CClientMainPlayer.Inst:TryCastSkill(resetSkillId, true, Vector2.zero)
        else
            local clientRemainTime = CClientMainPlayer.Inst.CooldownProp:GetClientRemainTime(resetSkillId)
            local serverRemainTime = CClientMainPlayer.Inst.CooldownProp:GetServerRemainTime(resetSkillId)
            local remainTime = round2(math.max(clientRemainTime, serverRemainTime) / 1000, 1)
            if remainTime ~= 0 then
                g_MessageMgr:ShowMessage("SKILL_IN_COOLDOWN", remainTime)
            end
        end
    end
end

function LuaShenZhaiTanBaoKickSkillWnd:OnSwitchTargetButtonClick()
    if CClientMainPlayer.Inst then
        local mainPlayerObj = CClientMainPlayer.Inst
        local mainPlayerPosition = mainPlayerObj and mainPlayerObj.RO.Position
        local curSelectedTarget = CClientMainPlayer.Inst.Target
        if curSelectedTarget and TypeIs(curSelectedTarget, typeof(CClientNpc)) then
            if curSelectedTarget.TemplateId then
                if self.ballNpcIds[curSelectedTarget.TemplateId] then
                else
                    curSelectedTarget = nil
                end
            else
                curSelectedTarget = nil
            end
        end
        local minDistance = 99999999
        local minDistanceTarget = nil
        CClientObjectMgr.Inst:ForeachObject(DelegateFactory.Action_CClientObject(function(obj)
            if obj and TypeIs(obj, typeof(CClientNpc)) and self.ballNpcIds[obj.TemplateId] then
                local otherPlayerPosition = obj.RO.Position
                local distance = Vector3.Distance(otherPlayerPosition, mainPlayerPosition)
                if curSelectedTarget then
                    --之前是有target的
                    if curSelectedTarget ~= obj then
                        --判断一下是不是同一个gameObject, 不是的话, 再来算距离
                        if minDistance > distance then
                            minDistance = distance
                            minDistanceTarget = obj
                        end
                    end
                else
                    --之前没有target
                    if minDistance > distance then
                        minDistance = distance
                        minDistanceTarget = obj
                    end
                end
            end
        end))
        if minDistanceTarget then
            CClientMainPlayer.Inst.Target = minDistanceTarget
        end
    end
end

function LuaShenZhaiTanBaoKickSkillWnd:SetSkillIcon(comp, skillId)
    if skillId then
        local skillData = Skill_AllSkills.GetData(skillId)
        if skillData then
            comp:LoadSkillIcon(skillData.SkillIcon)
        end
    end
end

function LuaShenZhaiTanBaoKickSkillWnd:OnEnable()
    self.OnRightMenuChanged = DelegateFactory.Action(function()
        local panel = self.transform:GetComponent(typeof(UIPanel))
        if PlayerSettings.RightMenuExpanded then
            panel.alpha = 0
        else
            panel.alpha = 1
        end
    end)
    EventManager.AddListenerInternal(EnumEventType.OnRightMenuWndExpandedStatusChanged, self.OnRightMenuChanged)
    CUIManager.instance:HideUI(InitializeList(CreateFromClass(MakeGenericClass(List, String)), String, "", "SkillButtonBoard"))
end

function LuaShenZhaiTanBaoKickSkillWnd:OnDisable()
    EventManager.RemoveListenerInternal(EnumEventType.OnRightMenuWndExpandedStatusChanged, self.OnRightMenuChanged)
    CUIManager.instance:ShowUI(InitializeList(CreateFromClass(MakeGenericClass(List, String)), String, "", "SkillButtonBoard"))
end
