local bServer = IsRunningServerCode()

if bServer then
	function CPhysicsShip:GetObject()
		local obj = g_PhysicsMgr:GetObjByPhysicsObjId(self.m_POId)
		if obj and obj.m_EngineObject then
			return obj
		end
	end
else
	function CPhysicsShip:GetObject()
        return self.m_ClientPhysicsObjectHandler
	end
end

--[-[
--	obj:PhysicsApplyLocalForce(forcex, forcey)
--	obj:PhysicsApplyTorque(torque)
--	obj:GetPhysicsPixelPosv()
--	obj:PhysicsApplyWorldForce(forcex, forcey)
--]-]

function CPhysicsShip:Ctor(poId, templateId)
	self.m_POId = poId
	self.m_TemplateId = templateId
	self.m_CmdQ = {}
	self.m_LastMoveCmd = nil
	self.m_RotationTarget = 0
	self.m_TargetSpeed = 0
	self.m_MulSpeed = 1
	self.m_CmdMulSpeed = 1
	self.m_MaxW = 90 * PHYSICS_DEG_TO_RAD
	self.m_OriSpeed = 1.2

    self.m_TimeStepInSecond = 0.05
	self.m_IsFreeze = false
	-- server: { triggerId = {templateId, force, pixelX, pixelY} }
	self.m_EffectorInfo = {}
	self.m_SpeedCmd2CDEnd = {}

	self:LoadDesignData()
end

function CPhysicsShip:LoadDesignData()
	local objDesign = GetPhysicsObjectDesignData(self.m_TemplateId)
	local shapeDesign = GetPhysicsShapeDesignData(self.m_TemplateId)
	if not (objDesign and shapeDesign) then return end

	self.m_LinearDamping = shapeDesign.linearDamping
	self.m_AngularDamping = shapeDesign.angularDamping
	self.m_MaxW = objDesign.w * PHYSICS_DEG_TO_RAD
	self.m_OriSpeed = objDesign.speed
	InitPhysicsObject_M_I(self, shapeDesign)
end

function CPhysicsShip:FadeMoveCmd(who, to, stepAutoTF)
	if who < to then
        who = who + stepAutoTF * self.m_TimeStepInSecond
        if who > to then
            who = to
		end
    elseif who > to then
        who = who - stepAutoTF * self.m_TimeStepInSecond
        if who < to then
            who = to
        end
	end
	return who
end


function CPhysicsShip:Step(currframeId)
	self:ProcessSpeedCmd(currframeId)
    if not self:IsFreeze() then
		local obj = self:GetObject()
		if not obj then return end

		local mulSpeed = math.max(0, self.m_MulSpeed) * self.m_CmdMulSpeed
		local force = self.m_TargetSpeed * mulSpeed * self.m_Mass * self.m_LinearDamping
		local torque = self.m_MaxW * self.m_RotationTarget * self.m_I * self.m_AngularDamping

        local ret = obj:PhysicsApplyLocalForce(force, 0)
        ret = ret and obj:PhysicsApplyTorque(torque)

		if EnablePhysicsShipOutputLog then
			local str = string.format("CPhysicsShip:Step,,%s,,%s,,%s,||,%s,,%s,||,%s,,%s,||,%s,,%s\n",
				currframeId, force, torque,
				self.m_TargetSpeed, self.m_RotationTarget,
				self.m_Mass, self.m_I,
				self.m_LinearDamping, self.m_AngularDamping)
			local x, y, anglex, angley, vx, vy, angle, speed, w = obj:GetPhysicsAIParams()
			local str2 = string.format(",,%s,,%s,||,%s,,%s,,%s,,%s,,%s\n",
				x, y, vx, vy, angle, speed, w)

			local str3 = string.format("%s,%s,%s,%s\n",currframeId,x,y,angle)

			local logPath = "K:/L10Mainland/artist/"--H:/L10/
			if bServer then
				local file = io.open(logPath.."PhysicsShip_server.log", "a")
				file:write(str)
				file:write(str2)
				file:close()

				local file2 = io.open(logPath.."PhysicsShip_s.txt", "a")
				file2:write(str3)
				file2:close()
			else
				local File =import "System.IO.File"
				File.AppendAllText(logPath.."PhysicsShip_client.log", str);
				File.AppendAllText(logPath.."PhysicsShip_client.log", str2);

				File.AppendAllText(logPath.."PhysicsShip_c.txt", str3);
			end
		end
        --漩涡
		self:ProcessEffector()
    end
end

function CPhysicsShip:CheckSpeedCmdCD(frameId, speedCmdId)
	if self.m_IsFreeze then return false end
	local cdEndFrame = self.m_SpeedCmd2CDEnd[speedCmdId]
	if cdEndFrame then
		return frameId > cdEndFrame
	end
	return true
end

function CPhysicsShip:ProcessSpeedCmd(currframeId)
	if not self.m_SpeedCmdEndTime then return end
	local cmdId, endTime = self.m_SpeedCmdEndTime[1], self.m_SpeedCmdEndTime[2]
	if currframeId >= endTime then
		self:EndSpeedCmd(cmdId)
	end
end

function CPhysicsShip:SetMulSpeed(mulSpeed)
	self.m_MulSpeed = mulSpeed
end

function CPhysicsShip:EndSpeedCmd(cmdId)
	local cmdDesign = GetPhysicsSpeedCmdDesignData(cmdId)
	if cmdDesign.recoverOriSpeed ~= -1 then
		self.m_TargetSpeed = self.m_OriSpeed * cmdDesign.recoverOriSpeed
	end

	if cmdDesign.recoverMulSpeed ~= -1 then
		self.m_CmdMulSpeed = cmdDesign.recoverMulSpeed
	end
	self.m_SpeedCmdEndTime = nil
end

function CPhysicsShip:ProcessEffector()
	if not (self.m_EffectorInfo and next(self.m_EffectorInfo)) then return end

	local obj = self:GetObject()
	if not obj then return end

	local objX, objY = obj:GetPhysicsPixelPosv()
	if not (objX and objY) then return end

	for triggerId, info in pairs(self.m_EffectorInfo) do
		local triggerX, triggerY = info[3], info[4] -- pixel pos
		if triggerX and triggerY then
			local forcex = triggerX - objX
			local forcey = triggerY - objY
			local dist = math.sqrt(forcex * forcex + forcey * forcey)
			if dist > 0 then
				forcex = forcex / dist * info[2]
				forcey = forcey / dist * info[2]

				obj:PhysicsApplyWorldForce(forcex, forcey)
			else
				forcex = info[2]
				forcey = 0
				obj:PhysicsApplyWorldForce(forcex, forcey)
			end
		end
	end
end
--templateId, force, triggerX, triggerY
function CPhysicsShip:AddEffector(triggerId,templateId,force,triggerX,triggerY)
	self.m_EffectorInfo[triggerId] = {
		templateId or 0,
		force or 0,
		triggerX or 0,
		triggerY or 0
	}
end
function CPhysicsShip:RemoveEffector(triggerId)
	self.m_EffectorInfo[triggerId] = nil
end

function CPhysicsShip:SetFreeze(bFreeze, freezeTemplateId, countDownInfo)
	local obj = self:GetObject()
	if not obj then return end

	self.m_IsFreeze = bFreeze
	self.m_FreezeTemplateId = freezeTemplateId
	self.m_FreezeCountDown = countDownInfo 

	return obj:SetPhysicsObjectFreeze(bFreeze)
end

function CPhysicsShip:IsFreeze()
	return self.m_IsFreeze
end

function CPhysicsShip:PostTeleportObjectInScene()
	self.m_CmdQ = {}
	self.m_LastMoveCmd = nil
	self.m_RotationTarget = 0
	self.m_TargetSpeed = 0
	self.m_CmdMulSpeed = 1
  
	self.m_EffectorInfo = {}
end

function CPhysicsShip:OnDriverLeave()
	self.m_CmdQ = {}
	self.m_LastMoveCmd = nil
	self.m_RotationTarget = 0
	self.m_TargetSpeed = 0
	self.m_CmdMulSpeed = 1
end

function CPhysicsShip:GetControllerType()
	return EnumPhysicsController.ePhysicsShip
end

function CPhysicsShip:GetSnapShotDataTbl()
	local res = {
		self.m_IsFreeze and 1 or 0,
		self.m_LastMoveCmd and self.m_LastMoveCmd[1] or 0,
		self.m_RotationTarget,
		self.m_TargetSpeed,
		self.m_EffectorInfo,
		self.m_TemplateId,
		self.m_FreezeTemplateId or 0,
		self.m_CmdMulSpeed,
		self.m_MulSpeed,
		self.m_SpeedCmdEndTime,
		self.m_SpeedCmd2CDEnd,
	}
	return res
end

--解析t，并塞到snapshot中
function CPhysicsShip.ParseSnapShotDataTbl(snapshot,t)
	snapshot.isFreeze = t[1]>0
	snapshot.inputType = t[2]
	snapshot.rotationTarget = t[3]
	snapshot.targetSpeed = t[4]
	snapshot.effectorInfo = t[5]
	snapshot.templateId = t[6]
	snapshot.freezeTemplateId = t[7]
	snapshot.cmdMulSpeed = t[8]
	snapshot.mulSpeed = t[9]
	snapshot.speedCmdEndTime = t[10]
	snapshot.speedCmd2CDEnd = t[11]
end

--for client only
function CPhysicsShip:SyncServerSnapshot(shot)
	if bServer then return end

	self.m_IsFreeze = shot.isFreeze or false
	self.m_FreezeTemplateId = shot.freezeTemplateId or 0
	self.m_InputType = shot.inputType
	self.m_RotationTarget = shot.rotationTarget
	self.m_TargetSpeed = shot.targetSpeed
	self.m_CmdMulSpeed = shot.cmdMulSpeed
	self.m_MulSpeed = shot.mulSpeed
	self.m_EffectorInfo = {}
	if next(shot.effectorInfo) then
		for k,v in pairs(shot.effectorInfo) do
			self.m_EffectorInfo[k] = { v[1],v[2],v[3],v[4] }
		end
	end
	if shot.speedCmdEndTime then
		self.m_SpeedCmdEndTime = {}
		self.m_SpeedCmdEndTime[1] = shot.speedCmdEndTime[1]
		self.m_SpeedCmdEndTime[2] = shot.speedCmdEndTime[2]
	else
		self.m_SpeedCmdEndTime = nil
	end

	self.m_SpeedCmd2CDEnd = {}
	if next(shot.speedCmd2CDEnd) then
		for k, v in pairs(shot.speedCmd2CDEnd) do
			self.m_SpeedCmd2CDEnd[k] = v
		end
	end
end
function CPhysicsShip:RecordSnapshot(shot)
	local effectorInfo = {}
	if next(self.m_EffectorInfo) then
		for k,v in pairs(self.m_EffectorInfo) do
			effectorInfo[k] = {
				v[1],
				v[2],
				v[3],
				v[4]
			}
		end
	end
	shot.isFreeze = self.m_IsFreeze
	shot.freezeTemplateId = self.m_FreezeTemplateId
	shot.inputType = self.m_InputType
	shot.rotationTarget = self.m_RotationTarget
	shot.targetSpeed = self.m_TargetSpeed
	shot.cmdMulSpeed = self.m_CmdMulSpeed
	shot.mulSpeed = self.m_MulSpeed
	shot.effectorInfo = effectorInfo
	if self.m_SpeedCmdEndTime then
		shot.speedCmdEndTime = {}
		shot.speedCmdEndTime[1] = self.m_SpeedCmdEndTime[1]
		shot.speedCmdEndTime[2] = self.m_SpeedCmdEndTime[2]
	else
		shot.speedCmdEndTime = nil
	end
	local speedCmd2CDEnd = {}
	if next(self.m_SpeedCmd2CDEnd) then
		for k, v in pairs(self.m_SpeedCmd2CDEnd) do
			speedCmd2CDEnd[k] = v
		end
	end
	shot.speedCmd2CDEnd = speedCmd2CDEnd
end
