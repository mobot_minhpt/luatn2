local GameObject				= import "UnityEngine.GameObject"
local UILabel				    = import "UILabel"
local UITexture                 = import "UITexture"
local CommonDefs				= import "L10.Game.CommonDefs"
local DelegateFactory			= import "DelegateFactory"
local CUIManager                = import "L10.UI.CUIManager"

CLuaDwZongziTopRightMenu = class()

--注册View组件
RegistChildComponent(CLuaDwZongziTopRightMenu, "ExpandButton",		GameObject)
RegistChildComponent(CLuaDwZongziTopRightMenu, "Content",		    GameObject)
RegistChildComponent(CLuaDwZongziTopRightMenu, "Hp1",		        UITexture)
RegistChildComponent(CLuaDwZongziTopRightMenu, "HpLabel1",		    UILabel)
RegistChildComponent(CLuaDwZongziTopRightMenu, "Hp2",		        UITexture)
RegistChildComponent(CLuaDwZongziTopRightMenu, "HpLabel2",		    UILabel)
--RegistChildComponent(CLuaDwZongziTopRightMenu, "Fx2",		        CUIFx)

--注册变量
--RegistClassMember(CLuaDwZongziTopRightMenu,"m_enable")

--[[
    @desc: 初始化
    author:CodeGize
    time:2019-04-16 11:22:58
    @return:
]]
function CLuaDwZongziTopRightMenu:Init()
    
end

function CLuaDwZongziTopRightMenu:OnExpandButtonClick()
	self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 0)
	self.Content:SetActive(false)
    CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end

function CLuaDwZongziTopRightMenu:OnHideTopAndRightTipWnd()
	self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 180)
	self.Content:SetActive(true)
end

function CLuaDwZongziTopRightMenu:OnEnable()
    g_ScriptEvent:AddListener("OnProcessMonsterHP",self,"ProcessInfo")
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    local btnclick = function(go)
        self:OnExpandButtonClick()
    end

    CommonDefs.AddOnClickListener(self.ExpandButton, DelegateFactory.Action_GameObject(btnclick), false)
end

function CLuaDwZongziTopRightMenu:OnDisable( )
	g_ScriptEvent:RemoveListener("OnProcessMonsterHP",self,"ProcessInfo")
	g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
end
--[[ 
    @desc:更新界面 
    author:CodeGize
    time:2019-04-16 14:02:40
    --@mid:
	--@hp:
	--@hpmax: 
    @return:
]]
function CLuaDwZongziTopRightMenu:InitValue(mid,hp,hpmax)
    local dwsetting = DuanWu_Setting.GetData()
    if dwsetting == nil then return end
    local mids = dwsetting.ZongZiSyncMonsterTempId
    if mids == nil then return end

    if mids[0] == mid then 
        self.Hp1.fillAmount = hp / hpmax
        self.HpLabel1.text = tostring(math.ceil(hp))
    elseif mids[1] == mid then
        self.Hp2.fillAmount = hp / hpmax
        self.HpLabel2.text = tostring(math.ceil(hp))
    end
end

--[[
    @desc: 处理数据，并调用InitValue进行更  新
    author:Code Gize
    autho1:CodeGize
    time:2019-04-16 14:09:10
    --@info: 服务器数据，list，每3个item一组，分别表示为mid,hp,hpmax
    @return:
]]
function CLuaDwZongziTopRightMenu:ProcessInfo(info)
    local cps = MsgPackImpl.unpack(info)
    local ct = cps.Count
    if ct >= 3 then
        local mid1 = cps[0]
        local mhp1 = cps[1]
        local mhpmax1 = cps[2]
        self:InitValue(mid1,mhp1,mhpmax1)
    end
    if ct >= 6 then
        local mid2 = cps[3]
        local mhp2 = cps[4]
        local mhpmax2 = cps[5]
        self:InitValue(mid2,mhp2,mhpmax2)
    end
end
