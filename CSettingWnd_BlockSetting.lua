-- Auto Generated!!
local CommonDefs = import "L10.Game.CommonDefs"
local CPaiZhaoMgr = import "CPaiZhaoMgr"
local CSettingWnd_BlockSetting = import "L10.UI.CSettingWnd_BlockSetting"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Int32 = import "System.Int32"
local QnCheckBox = import "L10.UI.QnCheckBox"

CSettingWnd_BlockSetting.m_Init_CS2LuaHook = function (this)
    local titleList = CommonDefs.ListCreate(typeof(cs_string))
    CommonDefs.ListAdd_LuaCall(titleList, LocalString.GetString("显示结拜"))
    CommonDefs.ListAdd_LuaCall(titleList, LocalString.GetString("显示好友"))
    CommonDefs.ListAdd_LuaCall(titleList, LocalString.GetString("显示帮会成员"))
    CommonDefs.ListAdd_LuaCall(titleList, LocalString.GetString("显示侠侣"))
    CommonDefs.ListAdd_LuaCall(titleList, LocalString.GetString("显示路人"))
    CommonDefs.ListAdd_LuaCall(titleList, LocalString.GetString("拒绝与他人合影"))
    this.titleArray = CommonDefs.ListToArray(titleList)
    this.m_Root.gameObject:SetActive(true)
    this.m_DetailSettingsGroup:InitWithOptions(this.titleArray, true, false)
    this:LoadData()
end

CSettingWnd_BlockSetting.m_OnEnable_CS2LuaHook = function (this) 
    this.m_DetailSettingsGroup.OnSelect = CommonDefs.CombineListner_Action_QnCheckBox_int(this.m_DetailSettingsGroup.OnSelect, MakeDelegateFromCSFunction(this.OnSetDetail, MakeGenericClass(Action2, QnCheckBox, Int32), this), true)
    EventManager.AddListener(EnumEventType.ReloadSettings, MakeDelegateFromCSFunction(this.LoadData, Action0, this))
end
CSettingWnd_BlockSetting.m_OnDisable_CS2LuaHook = function (this) 

    this.m_DetailSettingsGroup.OnSelect = CommonDefs.CombineListner_Action_QnCheckBox_int(this.m_DetailSettingsGroup.OnSelect, MakeDelegateFromCSFunction(this.OnSetDetail, MakeGenericClass(Action2, QnCheckBox, Int32), this), false)
    EventManager.RemoveListener(EnumEventType.ReloadSettings, MakeDelegateFromCSFunction(this.LoadData, Action0, this))
end
CSettingWnd_BlockSetting.m_OnSetDetail_CS2LuaHook = function (this, checkBox, index) 
    local indexToSettingName =
    {
        [0] = "ShowBrother",
        [1] = "ShowFriend",
        [2] = "ShowGuildMember",
        [3] = "ShowPartner",
        [4] = "ShowOther",
    }
    local enable = this.m_DetailSettingsGroup[index].Selected
    if index == 5 then
        -- 设置是否拒绝与他人合影
        CClientMainPlayer.Inst.RelationshipProp.SettingInfo:SetBit(EnumPropertyRelationshipSettingBit.RefuseGroupPhoto, enable)
        Gac2Gas.RequestSetNotAllowExpressionAction("photo", enable)
    else
        CPaiZhaoMgr.Instance:setShowType(this:indexToType(index), enable)
        CLuaPlayerSettings.Set(indexToSettingName[index], enable)
    end 
end
CSettingWnd_BlockSetting.m_LoadData_CS2LuaHook = function (this) 
    do
        local i = 0
        while i < this.titleArray.Length do
            if i == 5 then
                this.m_DetailSettingsGroup[i].Selected = CClientMainPlayer.Inst.RelationshipProp.SettingInfo:GetBit(EnumPropertyRelationshipSettingBit.RefuseGroupPhoto)
            else
                this.m_DetailSettingsGroup[i].Selected = CPaiZhaoMgr.Instance:CanTypeShow(this:indexToType(i))
            end
            i = i + 1
        end
    end
end
CSettingWnd_BlockSetting.m_SetSelected_CS2LuaHook = function (this, selected) 
    this.m_Root:SetActive(selected)
    if selected then
        this:LoadData()
    end
end
CSettingWnd_BlockSetting.m_indexToType_CS2LuaHook = function (this, index) 
    repeat
        local default = index
        if default == 0 then
            return EnumPaizhaoRelation_lua.eBrother
        elseif default == 1 then
            return EnumPaizhaoRelation_lua.eFriend
        elseif default == 2 then
            return EnumPaizhaoRelation_lua.eGuildMember
        elseif default == 3 then
            return EnumPaizhaoRelation_lua.ePartner
        elseif default == 4 then
            return EnumPaizhaoRelation_lua.eOther
        end
    until 1
    return EnumPaizhaoRelation_lua.eAll
end

