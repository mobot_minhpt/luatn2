local QnButton = import "L10.UI.QnButton"
local UITable = import "UITable"
local Extensions = import "Extensions"
local MessageMgr = import "L10.Game.MessageMgr"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local CTipParagraphItem = import "L10.UI.CTipParagraphItem"

CLuaMengGuiJieSignWnd = class()

RegistChildComponent(CLuaMengGuiJieSignWnd, "SignButton", QnButton)
RegistChildComponent(CLuaMengGuiJieSignWnd, "SkillTipButton", QnButton)
RegistChildComponent(CLuaMengGuiJieSignWnd, "RuleTable", UITable)
RegistChildComponent(CLuaMengGuiJieSignWnd, "ParagraphTemplate", GameObject)

function CLuaMengGuiJieSignWnd:Init()
    self.SignButton.OnClick = DelegateFactory.Action_QnButton(function ()
        Gac2Gas.SignUpHalloween2020ArrestPlay()
    end)

    self.SkillTipButton.OnClick = DelegateFactory.Action_QnButton(function ()
        CUIManager.ShowUI(CLuaUIResources.MengGuiJieSkillTipWnd)
    end)

    Extensions.RemoveAllChildren(self.RuleTable.transform)
    local ruleMessage = MessageMgr.Inst:FormatMessage("HALLOWEEN2020_MENGGUIJIE_SIGNRULE", {})
    local ruleTipInfo = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(ruleMessage)
    if ruleTipInfo ~= nil then
        for i = 0, ruleTipInfo.paragraphs.Count - 1 do
            local rule = CUICommonDef.AddChild(self.RuleTable.gameObject, self.ParagraphTemplate):GetComponent(typeof(CTipParagraphItem))
            rule.gameObject:SetActive(true)
            rule:Init(ruleTipInfo.paragraphs[i], 0xffffffff)
        end
    end
    self.RuleTable:Reposition()

    Gac2Gas.QueryHalloween2020ArrestPlayMatchInfo()
end
