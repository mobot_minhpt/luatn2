local DelegateFactory        = import "DelegateFactory"
local CButton                = import "L10.UI.CButton"
local QnSelectableButton     = import "L10.UI.QnSelectableButton"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local PopupMenuItemData      = import "L10.UI.PopupMenuItemData"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"
local CPopupMenuInfoMgr      = import "L10.UI.CPopupMenuInfoMgr"
local AlignType              = import "L10.UI.CPopupMenuInfoMgr+AlignType"
local Profession             = import "L10.Game.Profession"
local StringBuilder          = import "System.Text.StringBuilder"
local CWordFilterMgr         = import "L10.Game.CWordFilterMgr"
local CTeamMgr               = import "L10.Game.CTeamMgr"

LuaTeamRecruitWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaTeamRecruitWnd, "targetMatchButton")
RegistClassMember(LuaTeamRecruitWnd, "targetMatchArrow")
RegistClassMember(LuaTeamRecruitWnd, "targetArray")
RegistClassMember(LuaTeamRecruitWnd, "targetMatchIndex")
RegistClassMember(LuaTeamRecruitWnd, "levelMatchSelectaleButton")
RegistClassMember(LuaTeamRecruitWnd, "classMatchSelectaleButton")
RegistClassMember(LuaTeamRecruitWnd, "equipScoreMatchSelectaleButton")
RegistClassMember(LuaTeamRecruitWnd, "table")
RegistClassMember(LuaTeamRecruitWnd, "template")
RegistClassMember(LuaTeamRecruitWnd, "typeTemplate")
RegistClassMember(LuaTeamRecruitWnd, "searchInput")
RegistClassMember(LuaTeamRecruitWnd, "addSubButton")
RegistClassMember(LuaTeamRecruitWnd, "postRecruitButton")
RegistClassMember(LuaTeamRecruitWnd, "myRecruitButton")
RegistClassMember(LuaTeamRecruitWnd, "playerName")

function LuaTeamRecruitWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

    local info = self.transform:Find("Info")
    self.targetMatchButton = info:Find("Match/Target/Button"):GetComponent(typeof(CButton))
    self.targetMatchArrow = info:Find("Match/Target/Button/Arrow"):GetComponent(typeof(UIBasicSprite))
    UIEventListener.Get(self.targetMatchButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTargetMatchButtonClick(go)
	end)
    self.targetMatchButton.Text = "-"

    self.levelMatchSelectaleButton = info:Find("Match/Level/Sprite"):GetComponent(typeof(QnSelectableButton))
    self.levelMatchSelectaleButton.OnButtonSelected = DelegateFactory.Action_bool(function (selected)
	    self:OnLevelMatchButtonSelected(selected)
	end)
    self.classMatchSelectaleButton = info:Find("Match/Class/Sprite"):GetComponent(typeof(QnSelectableButton))
    self.classMatchSelectaleButton.OnButtonSelected = DelegateFactory.Action_bool(function (selected)
	    self:OnClassMatchButtonSelected(selected)
	end)
    self.equipScoreMatchSelectaleButton = info:Find("Match/EquipScore/Sprite"):GetComponent(typeof(QnSelectableButton))
    self.equipScoreMatchSelectaleButton.OnButtonSelected = DelegateFactory.Action_bool(function (selected)
	    self:OnEquipScoreMatchButtonSelected(selected)
	end)

    self.template = info:Find("Template").gameObject
    self.typeTemplate = info:Find("TypeTemplate").gameObject
    self.table = info:Find("Table"):GetComponent(typeof(UITable))
    self.template:SetActive(false)
    self.typeTemplate:SetActive(false)

    self.searchInput = info:Find("Bottom/Search/Input"):GetComponent(typeof(UIInput))
    self.searchInput.characterLimit = 7
    UIEventListener.Get(info:Find("Bottom/Search/Button").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSearchButtonClick()
	end)

    self.addSubButton = info:Find("Bottom/AddSubButton"):GetComponent(typeof(QnAddSubAndInputButton))
    self.addSubButton.onValueChanged = DelegateFactory.Action_uint(function (value)
        self:OnAddSubButtonValueChanged(value)
    end)

    self.postRecruitButton = info:Find("Bottom/PostRecruitButton").gameObject
    UIEventListener.Get(self.postRecruitButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnPostRecruitButtonClick()
	end)
    self.postRecruitButton:SetActive(false)
    self.myRecruitButton = info:Find("Bottom/MyRecruitButton").gameObject
    UIEventListener.Get(self.myRecruitButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnMyRecruitButtonClick()
	end)
    self.myRecruitButton:SetActive(false)
    UIEventListener.Get(info:Find("Bottom/RefreshButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRefreshButtonClick()
	end)
end

function LuaTeamRecruitWnd:OnEnable()
    g_ScriptEvent:AddListener("TeamRecruitSearchInfo", self, "OnTeamRecruitSearchInfo")
    g_ScriptEvent:AddListener("TeamRecruitSelfInfo", self, "OnTeamRecruitSelfInfo")
    g_ScriptEvent:AddListener("TeamRecruitSendSucc", self, "OnTeamRecruitSendSucc")
end

function LuaTeamRecruitWnd:OnDisable()
    g_ScriptEvent:RemoveListener("TeamRecruitSearchInfo", self, "OnTeamRecruitSearchInfo")
    g_ScriptEvent:RemoveListener("TeamRecruitSelfInfo", self, "OnTeamRecruitSelfInfo")
    g_ScriptEvent:RemoveListener("TeamRecruitSendSucc", self, "OnTeamRecruitSendSucc")
end

function LuaTeamRecruitWnd:OnTeamRecruitSearchInfo()
    self:UpdateInfo()
end

function LuaTeamRecruitWnd:OnTeamRecruitSelfInfo()
    self.myRecruitButton:SetActive(LuaTeamRecruitMgr.myData.id ~= nil)
    self.postRecruitButton:SetActive(LuaTeamRecruitMgr.myData.id == nil)
end

function LuaTeamRecruitWnd:OnTeamRecruitSendSucc()
    self:QueryInfo(self.addSubButton:GetValue() - 1)
end

function LuaTeamRecruitWnd:Init()
    self:UpdateInfo()
    Gac2Gas.QueryMyTeamRecruit()
end

function LuaTeamRecruitWnd:UpdateInfo()
    self:UpdateTable()
    self:UpdatePageNum()
end

function LuaTeamRecruitWnd:UpdateTable()
    self:AddChild(self.table.transform, self.template, #LuaTeamRecruitMgr.searchList)
    for id, data in ipairs(LuaTeamRecruitMgr.searchList) do
        local child = self.table.transform:GetChild(id - 1)

        child:Find("Target"):GetComponent(typeof(UILabel)).text = TeamRecruit_Type.GetData(data.target).Name
        child:Find("Level"):GetComponent(typeof(UILabel)).text = SafeStringFormat3("%d-%d", data.gardeStart, data.gardeEnd)
        child:Find("Name"):GetComponent(typeof(UILabel)).text = data.name
        local typeTable = child:Find("TypeTable"):GetComponent(typeof(UITable))
        self:AddChild(typeTable.transform, self.typeTemplate, #data.typeList)
        for i = 1, #data.typeList do
            local typeData = data.typeList[i]
            if typeData[1] > 0 then
                local typeChild = typeTable.transform:GetChild(i - 1)

                local sprite = typeChild:GetComponent(typeof(UISprite))
                if typeData[2] > 0 then
                    sprite.width = 60
                    sprite.spriteName = Profession.GetIconByNumber(typeData[2])
                else
                    sprite.width = 70
                    sprite.spriteName = TeamRecruit_Feature.GetData(typeData[1]).Icon
                end
            end
        end
        typeTable:Reposition()

        local msg = child:Find("Message"):GetComponent(typeof(UILabel))
        if System.String.IsNullOrEmpty(data.msg) then
            msg.text = ""
        else
            local sb = NewStringBuilderWraper(StringBuilder, tostring(data.msg))
            local str = sb.Length > 9 and SafeStringFormat3("%s...", sb:ToString(0, 9)) or sb:ToString()
            msg.text = str
        end

        child:GetComponent(typeof(QnButton)):SetBackgroundTexture(id % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite)
        UIEventListener.Get(child.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            LuaTeamRecruitMgr.dataForDetailWnd = data
            CUIManager.ShowUI(CLuaUIResources.TeamRecruitDetailWnd)
        end)
    end
    self.table:Reposition()
end

function LuaTeamRecruitWnd:AddChild(parent, template, num)
    local childCount = parent.childCount
    if childCount < num then
        for i = childCount, num do
            NGUITools.AddChild(parent.gameObject, template)
        end
    end

    childCount = parent.childCount
    for i = 0, childCount - 1 do
        parent:GetChild(i).gameObject:SetActive(i < num)
    end
end

function LuaTeamRecruitWnd:UpdatePageNum()
    self.addSubButton:SetMinMax(1, LuaTeamRecruitMgr.searchTotalPageNum, 1)
    self.addSubButton:SetValue(LuaTeamRecruitMgr.searchCurrentPageNum, false)
    self.addSubButton:OverrideText(SafeStringFormat3("%d/%d", LuaTeamRecruitMgr.searchCurrentPageNum, LuaTeamRecruitMgr.searchTotalPageNum))
end

-- 请求信息
function LuaTeamRecruitWnd:QueryInfo(page)
    local target = self.targetMatchIndex or 0
    local isByGrade = self.levelMatchSelectaleButton:isSeleted()
    local isByClass = self.classMatchSelectaleButton:isSeleted()
    local isByEquipScore = self.equipScoreMatchSelectaleButton:isSeleted()
    Gac2Gas.RequestQueryTeamRecruit(page or 0, target, self.playerName or "", isByGrade, isByClass, isByEquipScore)
end

function LuaTeamRecruitWnd:OnDestroy()
    LuaTeamRecruitMgr.searchList = {}
end

--@region UIEvent

function LuaTeamRecruitWnd:OnLevelMatchButtonSelected(selected)
    self:QueryInfo()
end

function LuaTeamRecruitWnd:OnClassMatchButtonSelected(selected)
    self:QueryInfo()
end

function LuaTeamRecruitWnd:OnEquipScoreMatchButtonSelected(selected)
    self:QueryInfo()
end

function LuaTeamRecruitWnd:OnTargetMatchButtonClick(go)
    if not self.targetArray then
        local tbl = {}
        local count = TeamRecruit_Type.GetDataCount()
        for i = 1, count do
            local name = TeamRecruit_Type.GetData(i).Name
            table.insert(tbl, PopupMenuItemData(name, DelegateFactory.Action_int(function (_)
                self.targetMatchIndex = i
                self.targetMatchButton.Text = name
                self:QueryInfo()
            end), false, nil, EnumPopupMenuItemStyle.Default))
        end
        self.targetArray = Table2Array(tbl, MakeArrayClass(PopupMenuItemData))
    end

    self.targetMatchArrow.flip = UIBasicSprite.Flip.Vertically
    CPopupMenuInfoMgr.ShowPopupMenu(self.targetArray, go.transform, AlignType.Bottom, 1, DelegateFactory.Action(function ( ... )
        self.targetMatchArrow.flip = UIBasicSprite.Flip.Nothing
    end), 300, true, 296)
end

function LuaTeamRecruitWnd:OnSearchButtonClick()
    local ret = CWordFilterMgr.Inst:DoFilterOnSend(self.searchInput.value, nil, nil, true)
    local msg = ret.msg

	if msg == nil or ret.shouldBeIgnore then
		return
	end

    self.playerName = msg
    self:QueryInfo()
end

function LuaTeamRecruitWnd:OnPostRecruitButtonClick()
    if not CTeamMgr.Inst:TeamExists() then
        g_MessageMgr:ShowOkCancelMessage(LocalString.GetString("你当前没有队伍，是否要创建队伍？"), function()
            CTeamMgr.Inst:RequestCreateTeam()
            CUIManager.ShowUI(CUIResources.TeamWnd)
        end, nil, nil, nil, false)
    else
        CUIManager.ShowUI(CLuaUIResources.TeamRecruitPostWnd)
    end
end

function LuaTeamRecruitWnd:OnMyRecruitButtonClick()
    LuaTeamRecruitMgr.dataForDetailWnd = LuaTeamRecruitMgr.myData
    CUIManager.ShowUI(CLuaUIResources.TeamRecruitDetailWnd)
end

function LuaTeamRecruitWnd:OnRefreshButtonClick()
    self:QueryInfo(self.addSubButton:GetValue() - 1)
end

function LuaTeamRecruitWnd:OnAddSubButtonValueChanged(value)
    self.addSubButton:OverrideText(SafeStringFormat3("%d/%d", value, LuaTeamRecruitMgr.searchTotalPageNum))
    self:QueryInfo(value - 1)
end

--@endregion UIEvent
