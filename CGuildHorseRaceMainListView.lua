-- Auto Generated!!
local CGuildHorseRaceMainGroupTemplate = import "L10.UI.CGuildHorseRaceMainGroupTemplate"
local CGuildHorseRaceMainListView = import "L10.UI.CGuildHorseRaceMainListView"
local CGuildHorseRaceMgr = import "L10.Game.CGuildHorseRaceMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local Extensions = import "Extensions"
local GameObject = import "UnityEngine.GameObject"
local GuildHorseRaceMainGroupInfo = import "L10.Game.CGuildHorseRaceMgr+GuildHorseRaceMainGroupInfo"
local LocalString = import "LocalString"
local NGUITools = import "NGUITools"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CGuildHorseRaceMainListView.m_Init_CS2LuaHook = function (this, list, init) 
    Extensions.RemoveAllChildren(this.table.transform)
    this.groupTemplate:SetActive(false)
    CommonDefs.ListClear(this.groupList)
    if init then
        this.selectGroupIndex = - 1
        this.selectPlayerId = 0
    end

    CommonDefs.ListSort1(list, typeof(GuildHorseRaceMainGroupInfo), DelegateFactory.Comparison_GuildHorseRaceMainGroupInfo(function (data1, data2) 
        local first = - 1
        if data1.order then
            return first
        elseif data2.order then
            return - first
        elseif data1:HasMyGuildMember() then
            return first
        elseif data2:HasMyGuildMember() then
            return - first
        else
            return NumberCompareTo(data1.groupIndex, data2.groupIndex)
        end
    end))

    do
        local i = 0
        while i < list.Count do
            local instance = NGUITools.AddChild(this.table.gameObject, this.groupTemplate)
            instance:SetActive(true)
            local template = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CGuildHorseRaceMainGroupTemplate))
            if template ~= nil then
                template:Init(list[i], this.selectPlayerId)
                template.OnPlayerInThisGroupSelect = CommonDefs.CombineListner_Action_GameObject(template.OnPlayerInThisGroupSelect, MakeDelegateFromCSFunction(this.OnPlayerSelectInGroup, MakeGenericClass(Action1, GameObject), this), true)
                local button = CommonDefs.GetComponent_GameObject_Type(instance, typeof(QnSelectableButton))
                if button ~= nil then
                    CommonDefs.ListAdd(this.groupList, typeof(QnSelectableButton), button)
                    button:SetSelected(this.selectGroupIndex == i, false)
                    UIEventListener.Get(instance).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(instance).onClick, MakeDelegateFromCSFunction(this.OnGroupSelect, VoidDelegate, this), true)
                end
            end
            i = i + 1
        end
    end
    this.table:Reposition()
    this.scrollView:ResetPosition()
    this:UpdateWatchButton()
end
CGuildHorseRaceMainListView.m_OnGroupSelect_CS2LuaHook = function (this, go) 
    do
        local i = 0
        while i < this.groupList.Count do
            this.groupList[i]:SetSelected(go == this.groupList[i].gameObject, false)
            if go == this.groupList[i].gameObject then
                this.selectGroupIndex = i
            end
            i = i + 1
        end
    end
    this:UpdateWatchButton()
end
CGuildHorseRaceMainListView.m_OnPlayerSelectInGroup_CS2LuaHook = function (this, go) 
    do
        local i = 0
        while i < this.groupList.Count do
            if go ~= this.groupList[i].gameObject then
                local template = CommonDefs.GetComponent_Component_Type(this.groupList[i], typeof(CGuildHorseRaceMainGroupTemplate))
                if template ~= nil then
                    template:UnSelectAll()
                end
            end
            i = i + 1
        end
    end
    local group = CommonDefs.GetComponent_GameObject_Type(go, typeof(CGuildHorseRaceMainGroupTemplate))
    if group ~= nil then
        this.selectPlayerId = group.selectPlayerId
    end
end
CGuildHorseRaceMainListView.m_IsSelectedGroupOrdered_CS2LuaHook = function (this) 
    local result = false
    if this.selectGroupIndex >= 0 and this.groupList.Count > this.selectGroupIndex then
        local group = CommonDefs.GetComponent_Component_Type(this.groupList[this.selectGroupIndex], typeof(CGuildHorseRaceMainGroupTemplate)).info
        result = group.order
    else
        this.selectGroupIndex = 0
    end
    return result
end
CGuildHorseRaceMainListView.m_UpdateWatchButton_CS2LuaHook = function (this) 
    repeat
        local default = CGuildHorseRaceMgr.Inst.mainStatus
        if default == (1) then
            if this:IsSelectedGroupOrdered() then
                this.watchBtnLabel.text = LocalString.GetString("取消预约")
            else
                this.watchBtnLabel.text = LocalString.GetString("观赛预约")
            end
            if CGuildHorseRaceMgr.Inst:InPreelection() then
                this.watchButton.Enabled = true
            else
                this.watchButton.Enabled = false
            end
            break
        elseif default == (4) then
            if this:IsSelectedGroupOrdered() then
                this.watchBtnLabel.text = LocalString.GetString("取消预约")
            else
                this.watchBtnLabel.text = LocalString.GetString("观赛预约")
            end
            if CGuildHorseRaceMgr.Inst:InFinals() then
                this.watchButton.Enabled = true
            else
                this.watchButton.Enabled = false
            end
            break
        elseif default == (2) or default == (5) then
            this.watchBtnLabel.text = LocalString.GetString("观看比赛")
            this.watchButton.Enabled = true
            break
        elseif default == (3) or default == (6) then
            this.watchBtnLabel.text = LocalString.GetString("观看比赛")
            this.watchButton.Enabled = false
            break
        else
            break
        end
    until 1
end
CGuildHorseRaceMainListView.m_OnWatchButtonClick_CS2LuaHook = function (this, go) 
    if this.selectGroupIndex >= 0 and this.selectGroupIndex < this.groupList.Count then
        local index = CommonDefs.GetComponent_Component_Type(this.groupList[this.selectGroupIndex], typeof(CGuildHorseRaceMainGroupTemplate)).info.groupIndex
        repeat
            local default = CGuildHorseRaceMgr.Inst.mainStatus
            if default == (1) or default == (4) then
                if this:IsSelectedGroupOrdered() then
                    Gac2Gas.MPTYSRequestReserveGroup(index, 2)
                else
                    Gac2Gas.MPTYSRequestReserveGroup(index, 1)
                end
                break
            elseif default == (2) or default == (5) then
                Gac2Gas.MPTYSRequestReserveGroup(index, 3)
                break
            elseif default == (3) or default == (6) then
                this.watchBtnLabel.text = LocalString.GetString("观看比赛")
                this.watchButton.Enabled = false
                break
            else
                break
            end
        until 1
    else
        g_MessageMgr:ShowMessage("GUILDHORSERACE_SELECT_GROUP_TO_ORDER")
    end
end
