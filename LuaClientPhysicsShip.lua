local PlayerSettings = import "L10.Game.PlayerSettings"
local Rigidbody2D=import "UnityEngine.Rigidbody2D"
local WeatherContext = import "CTT3.Weather.WeatherContext"
local PeriodType=import "CTT3.Weather.PeriodType"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local LayerDefine=import "L10.Engine.LayerDefine"
local CRenderObject = import "L10.Engine.CRenderObject"
local CClientObjectRoot=import "L10.Game.CClientObjectRoot"
local CPhysicsTrigger=import "L10.Engine.CPhysicsTrigger"
local CPhysicsWorld=import "L10.Engine.CPhysicsWorld"
local CPhysicsObjectMgr=import "L10.Engine.CPhysicsObjectMgr"
local CPhysicsMgr=import "L10.Engine.CPhysicsMgr"
local CBoatAnimator=import "L10.Engine.CBoatAnimator"
local GameObject=import "UnityEngine.GameObject"
local Component=import "UnityEngine.Component"
local Mathf=import "UnityEngine.Mathf"
local BoxCollider = import "UnityEngine.BoxCollider"
local PrimitiveType=import "UnityEngine.PrimitiveType"
local Vector3=import "UnityEngine.Vector3"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local CEffectMgr = import "L10.Game.CEffectMgr"
local CBox2dObject = import "L10.Engine.CBox2dObject"
local Vector2=import "UnityEngine.Vector2"
local Rigidbody=import "UnityEngine.Rigidbody"
local ForceMode=import "UnityEngine.ForceMode"
local ForceMode2D=import "UnityEngine.ForceMode2D"
local Physics2D=import "UnityEngine.Physics2D"

local FrameInputPacket=import "L10.Engine.FrameInputPacket"
local FrameStatusPacket=import "L10.Engine.FrameStatusPacket"
local FrameLuaPacket=import "L10.Engine.FrameLuaPacket"

LuaClientPhysicsShip = class()
RegistClassMember(LuaClientPhysicsShip, "m_Core")
RegistClassMember(LuaClientPhysicsShip, "m_PhysicsObject")
RegistClassMember(LuaClientPhysicsShip, "m_PhysicsObjectId")
RegistClassMember(LuaClientPhysicsShip, "m_InCollision")
RegistClassMember(LuaClientPhysicsShip, "m_Mass")
RegistClassMember(LuaClientPhysicsShip, "m_FreezeTemplateIds")
RegistClassMember(LuaClientPhysicsShip, "m_IsHideHud")
RegistClassMember(LuaClientPhysicsShip, "m_ShipDamagedFX")
RegistClassMember(LuaClientPhysicsShip, "m_Force")
RegistClassMember(LuaClientPhysicsShip, "m_TemplateId")
RegistClassMember(LuaClientPhysicsShip, "m_Snapshot")
RegistClassMember(LuaClientPhysicsShip, "m_Box2dObject")

LuaClientPhysicsShip.s_SnapshotPool = {}
LuaClientPhysicsShip.s_SnapshotSize = 50--最多保存50个快照

function LuaClientPhysicsShip:Ctor(physicsObjectId,physicsObject)
    self.m_Snapshot = {}
    self.m_PhysicsObjectId = physicsObjectId
    self.m_PhysicsObject = physicsObject
    self.m_InCollision = false
    self.m_FreezeTemplateIds = {}--所有的锁定特效
    g_PhysicsObjectHandlers[self.m_PhysicsObjectId] = self
    self.m_IsHideHud = false

end
function LuaClientPhysicsShip:SetForce(force)
    self.m_Force = force
    if self.m_Box2dObject then
        self.m_Box2dObject.force = force
    end
end

function LuaClientPhysicsShip:Init(table)
    local objectTemplateId,controllerType = table.objectTemplateId,table.controllerType
    self.m_TemplateId = objectTemplateId
    local shapeId = Physics_Object.GetData(objectTemplateId).shape

    local objDesign = NavalWar_Ship.GetData(objectTemplateId)
    self.m_ShipDamagedFX = objDesign.FireFXID


    self.m_PhysicsObject.m_ControllerType = controllerType

    if controllerType==0 then
        print("error")
        return
    end


    if controllerType == EnumPhysicsController.ePhysicsShip then
        self.m_Core = CPhysicsShip:new(self.m_PhysicsObjectId,objectTemplateId)
        self.m_Core.m_ClientPhysicsObjectHandler = self
    elseif controllerType == EnumPhysicsController.ePhysicsAIShip then
        self.m_Core = CPhysicsAIShip:new(self.m_PhysicsObjectId,objectTemplateId)
        self.m_Core.m_ClientPhysicsObjectHandler = self
    end
    if not self.m_Core then 

        return 
    end


    local rb2d = self.m_PhysicsObject.box2dRb
    self.m_Mass = rb2d.mass

    self.m_Box2dObject = rb2d.gameObject:AddComponent(typeof(CBox2dObject))
    --玩家船的force使用的是shipId，其他是monster的force
    local engineId = math.abs(self.m_PhysicsObjectId)
    if LuaHaiZhanMgr.s_EngineId2ShipId[engineId] then
        self:SetForce(LuaHaiZhanMgr.s_EngineId2ShipId[engineId])
    else
        local clientObject = CClientObjectMgr.Inst:GetObject(engineId)
        if clientObject then--CClientMonster
            self:SetForce(clientObject.Forces)
        end
    end

    if LuaHaiZhanMgr.s_EnableDebugShape then
        local designData = Physics_ObjectShape.GetData(shapeId)
        local go = GameObject.CreatePrimitive(PrimitiveType.Cube)
        go.transform.parent = self.m_PhysicsObject.transform
        go.transform.localPosition = Vector3(0,0,0)
        go.transform.localScale = Vector3(designData.radius * 2 + designData.height,1,designData.radius * 2)
        go.transform.localEulerAngles = Vector3(0,90,0)
        Component.DestroyImmediate(go:GetComponent(typeof(BoxCollider)))
    end
end
local moveFxs = {88804162,88804171}--水纹特效、拖尾特效
function LuaClientPhysicsShip:SetMoveEffectActive(show)
    local ro = self.m_PhysicsObject.RO
    if ro then
        if not show then
            for index, value in ipairs(moveFxs) do
                local fx = ro:GetFxById(value)
                if fx then fx:Destroy() end
            end
        else
            for index, value in ipairs(moveFxs) do
                local fx = ro:GetFxById(value)
                if not fx then
                    local fx = CEffectMgr.Inst:AddObjectFX(value, ro, 0, 1, 1.0, nil, false, EnumWarnFXType.None, Vector3(0,0,0), Vector3.zero, nil)
                    ro:AddFX(value,fx)
                end
            end
        end
    end
end

function LuaClientPhysicsShip:RefreshHurtFx(hp,hpfull)
    --战损特效
    local percent = hp/hpfull
    local ro = self.m_PhysicsObject.RO
    if not ro then return end
    local key = self.m_ShipDamagedFX
    if percent<0.4 then
        local fx = ro:GetFxById(key)
        if not fx then
            fx = CEffectMgr.Inst:AddObjectFX(key, ro, 0, 1, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
            ro:AddFX(key, fx)
        end
    elseif percent>0.7 then
        local fx = ro:GetFxById(key)
        if fx then 
            fx:Destroy()
        end
    end
end

function LuaClientPhysicsShip:OnDestroy()
    g_PhysicsObjectHandlers[self.m_PhysicsObjectId] = nil

    for k,v in pairs(self.m_Snapshot) do
        table.insert(LuaClientPhysicsShip.s_SnapshotPool,v)
        self.m_Snapshot[k] = nil
    end
end

function LuaClientPhysicsShip:Step()
    if not self.m_Core then return end

    if self.m_PhysicsObject.m_CurrentPackets.Count>0 then
        --如果有快照和其他指令包混在一起，不处理快照了。简化一下逻辑。
        local processSnapshot = self.m_PhysicsObject.m_CurrentPackets.Count==1
        for i=1,self.m_PhysicsObject.m_CurrentPackets.Count do
            local pkt = self.m_PhysicsObject.m_CurrentPackets[i-1]
            if TypeIs(pkt, typeof(FrameStatusPacket)) then
				local speedCmdId = pkt.speedCmd
				local cmdDesign = Physics_SpeedCmd.GetData(speedCmdId)
				local objectDesign = Physics_Object.GetData(self.m_Core.m_TemplateId)

				if cmdDesign.durationFrame > 0 then
					local currframeId = self.m_PhysicsObject:GetFrameId()
					if self.m_Core.m_SpeedCmdEndTime then
						self.m_Core:EndSpeedCmd(self.m_Core.m_SpeedCmdEndTime[1])
					end
					self.m_Core.m_SpeedCmdEndTime = { speedCmdId, currframeId + cmdDesign.durationFrame }
				end
				if cmdDesign.startForce > 0 then
					-- local currframeId = self.m_PhysicsObject:GetFrameId()
					--print(debug.traceback("ApplyLocalForce:".. i .. "," .. currframeId .. "," .. cmdDesign.startForce))
					self:PhysicsApplyLocalForce(cmdDesign.startForce, 0)
				end
				if cmdDesign.oriSpeed ~= -1 then
					self.m_Core.m_TargetSpeed = objectDesign.speed * cmdDesign.oriSpeed
				end
				if cmdDesign.mulSpeed ~= -1 then
					self.m_Core.m_CmdMulSpeed = cmdDesign.mulSpeed
				end
            elseif TypeIs(pkt, typeof(FrameInputPacket)) then
                self.m_Core.m_InputType = pkt.inputType
                if self.m_Core.m_InputType == EnumPhysicsCmdType.eMoveLeft then
                    self.m_Core.m_RotationTarget = 0.25*pkt.level
                elseif self.m_Core.m_InputType == EnumPhysicsCmdType.eMoveRight then
                    self.m_Core.m_RotationTarget = -0.25*pkt.level
                elseif self.m_Core.m_InputType == EnumPhysicsCmdType.eNone then
                    self.m_Core.m_RotationTarget = 0
                end
            elseif TypeIs(pkt,typeof(FrameLuaPacket)) then
                -- if pkt.table.isSteering then
                if pkt.type == EnumLuaPacketType.eEffector then
                    local t = pkt.table
                    if t.add then
                        --增加漩涡
                        self.m_Core:AddEffector(t.triggerId,0,t.force,t.triggerX,t.triggerY)
                    else
                        --移除漩涡
                        self.m_Core:RemoveEffector(t.triggerId)
                    end
                elseif pkt.type == EnumLuaPacketType.eSteering then
                    -- print("set steering")
                    self.m_Core:AddSteer(pkt.table.steeringInfo)
                elseif pkt.type == EnumLuaPacketType.eFreeze then
                    local tamplateId = pkt.table.templateId
                    if tamplateId and tamplateId>0 then
                        self.m_FreezeTemplateIds[tamplateId] = true
                    end
                    local isFreeze = pkt.table.isFreeze
                    if isFreeze then
                        self.m_Core:SetFreeze(isFreeze,tamplateId,pkt.table.countDownInfo)
                        --做个动画
                        if self.m_PhysicsObject and self.m_PhysicsObject.RO and self.m_PhysicsObject.RO.m_MainObject then
                            local animator = self.m_PhysicsObject.RO.m_MainObject:GetComponent(typeof(CBoatAnimator))
                            if animator then
                                animator:ApplyFreezeForce()
                            end
                        end
                    else
                        self.m_Core:SetFreeze(isFreeze,tamplateId)
                    end
                elseif pkt.type == EnumLuaPacketType.eTeleport then
                    local rb = self.m_PhysicsObject.box2dRb
                    rb.velocity = Vector2(0,0)
                    rb.angularVelocity = 0
                    -- rb.position = Vector2(pkt.table.x,pkt.table.y)
                    -- rb.rotation = pkt.table.dir
                    rb.transform.position = Vector3(pkt.table.x,pkt.table.y,0)
                    rb.transform.localEulerAngles = Vector3(0,0,pkt.table.dir)

                    --将力和力矩清0
                    rb:Sleep()
                    rb:WakeUp()
                    if self.m_Core.PostTeleportObjectInScene then
                        self.m_Core:PostTeleportObjectInScene()
                    end
                    self.m_PhysicsObject:SyncTransformImmediate()
                elseif pkt.type == EnumLuaPacketType.eWorldForce then
                    self:PhysicsApplyWorldForce(pkt.table.x,pkt.table.y)
                elseif pkt.type == EnumLuaPacketType.eSnapshot then
                    if processSnapshot then
                        self:SyncServerSnapshot(pkt.table.snapshot)
                    end
                elseif pkt.type == EnumLuaPacketType.eDriverLeaveRiding then
                    self.m_Core:OnDriverLeave()
                elseif pkt.type == EnumLuaPacketType.eMulSpeed then
                    self.m_Core:SetMulSpeed(pkt.table.mulSpeed)
                elseif pkt.type == EnumLuaPacketType.eSendBullet then
                    if UNITY_EDITOR then BeginSample("CreateBullet") end
                    self:CreateBullet(pkt.table)
                    if UNITY_EDITOR then EndSample() end
                end
            end
        end
    end
    if UNITY_EDITOR then BeginSample("Core Step") end
    self.m_Core:Step(self.m_PhysicsObject:GetFrameId())
    if UNITY_EDITOR then EndSample() end

    self:UpdateFx()
end

function LuaClientPhysicsShip:UpdateFx()
    if self.m_Core.m_CmdMulSpeed and self.m_Core.m_CmdMulSpeed>1 then
        --加速
        if PlayerSettings.QuickSettingMode>0 then
            local ro = self.m_PhysicsObject.RO
            local fx = ro:GetFxById(88804153)
            if not fx then
                local fx = CEffectMgr.Inst:AddObjectFX(88804153, ro, 0, 1, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
                ro:AddFX(88804153, fx)
            end
        end
    else
        --移除加速特效
        local ro = self.m_PhysicsObject.RO
        local fx = ro:GetFxById(88804153)
        if fx then 
            fx:Destroy()
        end
    end

    if self.m_PhysicsObject.m_ControllerType==EnumPhysicsController.ePhysicsShip then
        if self.m_Core.m_IsFreeze then
            local ro = self.m_PhysicsObject.RO
            local key = self.m_Core.m_FreezeTemplateId
            if key>0 then
                local fx = ro:GetFxById(key)
                if not fx then
                    fx = CEffectMgr.Inst:AddObjectFX(key, ro, 0, 1, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
                    ro:AddFX(key, fx)
                end
            else
                --如果特效是0，那就应该全部移除
                for key, _ in pairs(self.m_FreezeTemplateIds) do
                    local fx = ro:GetFxById(key)
                    if fx then 
                        fx:Destroy()
                    end
                end
            end
        else
            local ro = self.m_PhysicsObject.RO
            for key, _ in pairs(self.m_FreezeTemplateIds) do
                local fx = ro:GetFxById(key)
                if fx then 
                    fx:Destroy()
                end
            end
        end
    end

    --亮灯特效
    if self.m_TemplateId==10000001 then
        if WeatherContext.period==PeriodType.Night then
            if PlayerSettings.QuickSettingMode>0 then
                local ro = self.m_PhysicsObject.RO
                local fx = ro:GetFxById(88804186)
                if not fx then
                    local fx = CEffectMgr.Inst:AddObjectFX(88804186, ro, 0, 1, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
                    ro:AddFX(88804186, fx)
                end
            end
        else
            local ro = self.m_PhysicsObject.RO
            local fx = ro:GetFxById(88804186)
            if fx then 
                fx:Destroy()
            end
        end
    end
end

function LuaClientPhysicsShip.CanShowBullet(poId,x, y)
    if not CClientMainPlayer.Inst then return false end

    local myPOId = CClientMainPlayer.Inst.AppearanceProp.DriverId
    --自己的子弹必然创建
    if myPOId == poId then return true end

    local fps = 1/Time.unscaledDeltaTime
    if fps<10 then--小于10帧
        --如果是卡顿状态，其他船不创建子弹
        return false
    end
    local myPo = g_PhysicsObjectHandlers[myPOId]
    local po = g_PhysicsObjectHandlers[poId]
    if myPo and po then
        local myPosX,myPosY,myPosZ = LuaTransformAccess.GetPosition(myPo.m_PhysicsObject.RO.transform)
        local dis = Vector2_Distance(myPosX,myPosZ,x,y)
        if fps<15 and dis>10 then--小于15帧，距离大于10米不创建
            return false
        elseif fps<20 and dis>20 then--距离大于20米不创建
            return false
        elseif fps<30 and dis>30 then--距离大于30米不创建
            return false
        end
    end
    return true
end

function LuaClientPhysicsShip:CreateBullet(t)
    --回滚可能也会造成卡顿，这里也拦一次
    if not LuaClientPhysicsShip.CanShowBullet(self.m_PhysicsObjectId,t.x, t.y) then return end

    local bulletId = t.bulletId
    --已经创建
    local po = CPhysicsObjectMgr.Inst:GetTrigger(bulletId)
    if po then return end

    local serverFrameId = self.m_PhysicsObject:GetFrameId()

    local shipData = NavalWar_Ship.GetData(self.m_Core.m_TemplateId)
    local right = self.m_PhysicsObject.RO.transform.right

    --点乘
    local val = right.x*t.vx+right.z*t.vy
    local bone
    if val>0 then--right side
        bone = shipData.LeftCannonBone[t.floorId-1][t.iter-1]
    else
        bone = shipData.RightCannonBone[t.floorId-1][t.iter-1]
    end
    local boneTf = self.m_PhysicsObject.RO:GetSlotTransform(bone,false)
    local x,y,z = LuaTransformAccess.GetPosition(boneTf)
    t.positionY = y
    t.position = Vector3(x,y,z)

    local templateId = t.templateId
    local vx = t.vx
    local vy = t.vy
    local lifeCycle = t.lifeCycle
    t.deadFrame = serverFrameId + lifeCycle
    

    local physicsObject = LuaClientPhysicsBullet.CreateTrigger(bulletId,self.m_PhysicsObjectId)
    local go = physicsObject.gameObject
    --发射子弹的船
    -- local player = CPhysicsObjectMgr.Inst:GetPhysicsObject(self.m_PhysicsObjectId)
    -- local pos = player.transform.position

    local baseHeight = CPhysicsWorld.Inst:GetBaseHeight()

    local posx,posy = LuaRigidbody2DAccess.GetRelativePoint(self.m_PhysicsObject.box2dRb,t.offset,0)
    local tf = go.transform
    LuaTransformAccess.SetLocalPosition(tf, posx, baseHeight, posy)
    LuaTransformAccess.SetLocalScale(tf, 1,1,1)
    LuaTransformAccess.LookAt(tf, posx+vx*100,baseHeight,posy+vy*100)

    --trigger的坐标是准确的
    physicsObject:SetFrameId(serverFrameId)
    physicsObject:InitBullet(bulletId)

    local shapeId = Physics_Trigger.GetData(templateId).shape
    LuaClientPhysicsBullet.CreatePhysicsBullet(physicsObject,shapeId)

    local ro = LuaClientPhysicsBullet.CreateRenderObject(tostring(bulletId))
    physicsObject.RO = ro
    LuaTransformAccess.SetLocalPosition(ro.transform,x,y,z)
    LuaTransformAccess.LookAt(ro.transform, t.x+vx*100,baseHeight,t.y+vy*100)

    -- if UNITY_EDITOR then
    --     local DebugTrajectory = import "DebugTrajectory"
    --     ro.gameObject:AddComponent(typeof(DebugTrajectory))
    -- end

    local luaObject = LuaClientPhysicsBullet:new(bulletId,templateId,physicsObject)
    physicsObject:InitLuaSelf(luaObject)
    t.force = self.m_Force
    luaObject:Init(t)

    local triggerData = Physics_Trigger.GetData(templateId)
    if triggerData and triggerData.FX then
        for i=1,triggerData.FX.Length do
            local fxId = triggerData.FX[i-1]
            local fx = CEffectMgr.Inst:AddObjectFX(fxId, ro, 0, 1, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
            ro:AddFX(fxId, fx)
            if fx then 
                fx.CacheCount = LuaClientPhysicsBullet.s_BulletCacheCount
                fx.LifeTime = LuaClientPhysicsBullet.s_BulletCacheLifeTime
            end
        end
    end

    --炮口特效，音效在炮口上，必须显示
    local dir = 90-ro.Direction
    if triggerData and triggerData.GunFX then
        for i=1,triggerData.GunFX.Length do
            local fxId = triggerData.GunFX[i-1]
            local fx = CEffectMgr.Inst:AddWorldPositionFX(fxId, t.position, dir,0,1,-1,EnumWarnFXType.None,nil,0,0, nil)
            if fx then 
                fx.CacheCount = LuaClientPhysicsBullet.s_BulletCacheCount
                fx.LifeTime = LuaClientPhysicsBullet.s_BulletCacheLifeTime
            end
        end
    end
end

function LuaClientPhysicsShip:Snapshot()

    if not self.m_Core then return end

    local rb = self.m_PhysicsObject.box2dRb
    local frameId = self.m_PhysicsObject:GetFrameId()
    -- rb.transform.position = Vector3(x,y,0)
    -- rb.transform.localEulerAngles = Vector3(0,0,dir)

    -- local position = rb.position
    -- local rotation = rb.rotation
    -- local velocity = rb.velocity
    -- local angularVelocity = rb.angularVelocity

    local shot = table.remove(LuaClientPhysicsShip.s_SnapshotPool)
    if not shot then shot = {} end
    
    shot.x,shot.y,shot.angle,shot.linearVelocityX,shot.linearVelocityY,shot.angularVelocity = self.m_PhysicsObject:GetSnapshotParams()
    shot.simulated = rb.simulated

    if self.m_Core.__class == CPhysicsShip then
        self.m_Core:RecordSnapshot(shot)
    elseif self.m_Core.__class == CPhysicsAIShip then
        self.m_Core:RecordSnapshot(shot)
    end

    self.m_Snapshot[frameId] = shot
    local size = LuaClientPhysicsShip.s_SnapshotSize

    for k,v in pairs(self.m_Snapshot) do
        if k<frameId-size then
            table.insert(LuaClientPhysicsShip.s_SnapshotPool,v)
            
            self.m_Snapshot[k] = nil
        end
    end
end

function LuaClientPhysicsShip:CompareSnapshot(frameId,shot)
    local history = self.m_Snapshot[frameId]
    if history then
        local angleDiff = math.abs(history.angle-shot.angle)
        local xDiff = math.abs(history.x-shot.x)
        local yDiff = math.abs(history.y-shot.y)
        if xDiff>0.001 or yDiff>0.001 or angleDiff>0.1 then
if UNITY_EDITOR then
            local xd = history.x-shot.x
            local yd = history.y-shot.y
            local dis = math.sqrt( xd*xd+yd*yd )
            local isai = self.m_Core.__class == CPhysicsAIShip and "ai" or "player"

            print("pos no sync,", frameId,isai, self.m_Core.m_POId, self.m_Core.m_TemplateId, dis,angleDiff)
end
			return false
        end
        if self.m_PhysicsObject.m_ControllerType==EnumPhysicsController.ePhysicsShip then
            if history.isFreeze~=shot.isFreeze then
                return false
            end
        end
        if history.angle~=shot.angle then
            -- print("angle no sync",history.angle,shot.angle)
        end
    else
        local minFrame = 4294967295
        for k,v in pairs(self.m_Snapshot) do
            if k<minFrame then
                minFrame = k
            end
        end

        -- print("no snapshot:",frameId,"min frame:",minFrame)
        --没有历史镜像包
		return false
    end
	return true
end

function LuaClientPhysicsShip:RollbackTo(frameId)
    if not self.m_Core then return end
    -- print("rollbackto",frameId,self.m_Core.m_POId)
    --快照包可以删掉了
    for k,v in pairs(self.m_Snapshot) do
        if k>frameId then
            table.insert(LuaClientPhysicsShip.s_SnapshotPool,v)

            self.m_Snapshot[k] = nil
        end
    end
    local shot = self.m_Snapshot[frameId]
    if shot then
        self:SyncServerSnapshot(shot)
    end
end
function LuaClientPhysicsShip:SyncServerSnapshot(shot)
    if not self.m_Core then return end
    local rb = self.m_PhysicsObject.box2dRb
    if rb then
        self.m_PhysicsObject:SetSnapshotParams(shot.x,shot.y,shot.angle,shot.linearVelocityX,shot.linearVelocityY,shot.angularVelocity)
        if shot.simulated~=nil then--服务器没发就不管
            if rb.simulated~=shot.simulated then
                rb.simulated = shot.simulated
            end
        end
    end

    if self.m_PhysicsObject.m_ControllerType == EnumPhysicsController.ePhysicsShip then
        if shot.freezeTemplateId and shot.freezeTemplateId>0 then
            self.m_FreezeTemplateIds[shot.freezeTemplateId] = true
        end
    end
    self.m_Core:SyncServerSnapshot(shot)
end


--callback for core lua object
function LuaClientPhysicsShip:PhysicsApplyLocalForce(forcex,forcey)
    if forcex~=0 or forcey~=0 then
        self.m_PhysicsObject.box2dRb:AddRelativeForce(Vector2(forcex,forcey))
    end
    return true
end
function LuaClientPhysicsShip:PhysicsApplyWorldForce(forcex,forcey)
    if forcex~=0 or forcey~=0 then
        self.m_PhysicsObject.box2dRb:AddForce(Vector2(forcex,forcey))
    end
    return true
end
function LuaClientPhysicsShip:PhysicsApplyTorque(torque)
    if torque~=0 then
        self.m_PhysicsObject.box2dRb:AddTorque(torque)
    end
    return true
end
function LuaClientPhysicsShip:GetPhysicsPixelPosv()
    local pos = self.m_PhysicsObject.box2dRb.position
    return math.floor(pos.x*64),math.floor(pos.y*64)
end
function LuaClientPhysicsShip:SetPhysicsObjectFreeze(bFreeze)
    local rb = self.m_PhysicsObject.box2dRb
    if not bFreeze then
        local pos = rb.position
        local rot = rb.rotation
        rb:WakeUp()
        rb.mass=self.m_Mass
        rb.position = pos
        rb.rotation = rot
    else
        rb.mass = 1000000--设置一个超大的数值
        rb:Sleep()
    end
end
function LuaClientPhysicsShip:GetPhysicsAIParams()
    return self.m_PhysicsObject:GetPhysicsAIParams()
end
function LuaClientPhysicsShip:SetPhysicsMoveDeltaPosition(dx, dy)
    -- print("SetPhysicsMoveDeltaPosition",dx,dy)
	local invTimeDelta = 1.0 / self.m_Core.m_TimeStepInSecond 
    LuaRigidbody2DAccess.SetVelocity(self.m_PhysicsObject.box2dRb,dx * invTimeDelta, dy * invTimeDelta)
	self.m_PhysicsObject.box2dRb.drag = 0.0
end
function LuaClientPhysicsShip:ApplyPhysicsAngularImpulse(impulse)
    -- print("ApplyPhysicsAngularImpulse",impulse)
	if impulse~=0 then
		self.m_PhysicsObject.box2dRb.angularDrag = 0;
        self.m_PhysicsObject.box2dRb:AddTorque(impulse, ForceMode2D.Impulse)
    end
    return true
end
function LuaClientPhysicsShip:SetPhysicsDirection(rad)
    self.m_PhysicsObject.box2dRb.rotation = rad * PHYSICS_RAD_TO_DEG
end
function LuaClientPhysicsShip:GetPhysicsPosv()
    -- local pos = self.m_PhysicsObject.box2dRb.position
    -- return pos.x,pos.y
    return LuaRigidbody2DAccess.GetPosition(self.m_PhysicsObject.box2dRb)
end
function LuaClientPhysicsShip:GetPhysicsForwardVector()
    local rad = self.m_PhysicsObject.box2dRb.rotation * PHYSICS_DEG_TO_RAD
	return math.cos(rad), math.sin(rad)
end
function LuaClientPhysicsShip:GetPhysicsVelocity()
    -- local velocity = self.m_PhysicsObject.box2dRb.velocity
    -- return velocity.x,velocity.y
    return LuaRigidbody2DAccess.GetVelocity(self.m_PhysicsObject.box2dRb)
end
function LuaClientPhysicsShip:GetNearestObstacle(minDistanceToCollision)
    local ret,a,b,c,d = self.m_PhysicsObject:GetNearestObstacleParams(minDistanceToCollision)
    if ret then
        return a,b,c,d
    end
end
function LuaClientPhysicsShip:PhysicsPredictFuturePosition(deltaTime, offsetX, offsetY)
    local x, y = self:GetPhysicsPosv()
	if not (x and y) then return end
	local velocity = self.m_PhysicsObject.box2dRb.velocity
	local speed = velocity.magnitude
	local forwardX, forwardY = self:GetPhysicsForwardVector()
	if not (speed and forwardX and forwardY) then return x, y end

	local tx, ty = x + deltaTime * forwardX * speed, y + deltaTime * forwardY * speed
	if offsetX and offsetY then
		-- { x = tx, y = ty } + offset.x * forward + offset.y * { x = -forward.y, y = forward.x }
		tx = tx + forwardX * offsetX - forwardY * offsetY
		ty = ty + forwardY * offsetX + forwardX * offsetY
	end
	return tx, ty
end


-- ai steering callback
function LuaClientPhysicsShip:SteeringDoAni(steering, aniName)
    if not self.m_PhysicsObject.RO then return end
    self.m_PhysicsObject.RO:DoAni(aniName,false,0,1,0.15,true,1)
end
function LuaClientPhysicsShip:SteeringSetActive(steering, bActive)
    local rb = self.m_PhysicsObject.box2dRb
    rb.simulated = bActive
end
function LuaClientPhysicsShip:SteeringSetDiveStatus(steering,bActive)
    --print("SteeringSetDiveStatus",bActive)
    --潜水 隐藏姓名板
    self.m_IsHideHud = bActive

    --潜水，隐藏船轨迹特效
    self:SetMoveEffectActive(not bActive)
end
-- function LuaClientPhysicsShip:SteeringSendAIEvent()
-- end
function LuaClientPhysicsShip:SteeringSetPhysicsPos(steering)
	local x, y, dir = steering.x, steering.y, steering.dir or 0
	if not (x and y) then return end
	-- return self:SetPhysicsPos(x, y, dir) > 0
    local rb = self.m_PhysicsObject.box2dRb
    -- rb.velocity = Vector2(0,0)
    -- rb.angularVelocity = 0
    rb.transform.position = Vector3(x,y,0)
    rb.transform.localEulerAngles = Vector3(0,0,dir)
    self.m_PhysicsObject:SyncTransformImmediate()
end
function LuaClientPhysicsShip:SteeringSetToTargetFuturePos(steering, deltaTime)
	local targetPOId = steering.poId
	local target = GetObjByPhysicsObjId(targetPOId)
	if not (target) then return end
	deltaTime = deltaTime or 1
	local x, y = target:PhysicsPredictFuturePosition(deltaTime, 0, 0.5)
	local dir = 0
	local rb = self.m_PhysicsObject.box2dRb

    --simulated为false的时候，无法设置rb的position和rotation。
    --这个时候可以设置transform的位置和角度
    rb.transform.position = Vector3(x,y,0)
    rb.transform.localEulerAngles = Vector3(0,0,dir)
    self.m_PhysicsObject:SyncTransformImmediate()
end
function LuaClientPhysicsShip:SteeringAddWorldFx(steering,fxId)
    if not self.m_PhysicsObject.RO then return end
    local pos = self.m_PhysicsObject.RO.Position
    CEffectMgr.Inst:AddWorldPositionFX(fxId, pos, 0,0,1,-1,EnumWarnFXType.None,nil,0,0, nil)
end
function LuaClientPhysicsShip:SteeringAddObjectFx(steering,fxId)
    if not self.m_PhysicsObject.RO then return end
    local ro = self.m_PhysicsObject.RO
    local fx = CEffectMgr.Inst:AddObjectFX(fxId, ro, 0, 1, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
    ro:AddFX(fxId, fx)
end
function LuaClientPhysicsShip:SteeringSetMaxSpeed(steering, maxSpeed)
    self.m_Core:SetMaxSpeed(maxSpeed)
end
function LuaClientPhysicsShip:SteeringSetMaxW(steering, maxW)
    self.m_Core:SetMaxW(maxW)
end
-- function LuaClientPhysicsShip:SteeringAddBuff()
-- end
-- function LuaClientPhysicsShip:SteeringRmBuff()
-- end
