local DelegateFactory  = import "DelegateFactory"
local CUITexture = import "L10.UI.CUITexture"
local UISprite = import "UISprite"
local UILabel = import "UILabel"
local CCommonLuaWnd=import "L10.UI.CCommonLuaWnd"

LuaYaoGuaiInfoWnd = class()

RegistChildComponent(LuaYaoGuaiInfoWnd, "Portrait", "Portrait", CUITexture)
RegistChildComponent(LuaYaoGuaiInfoWnd, "QualitySprite", "QualitySprite", UISprite)
RegistChildComponent(LuaYaoGuaiInfoWnd, "NameLabel", "NameLabel", UILabel)
RegistChildComponent(LuaYaoGuaiInfoWnd, "LevelLabel", "LevelLabel", UILabel)
RegistChildComponent(LuaYaoGuaiInfoWnd, "LabelTemplate", "LabelTemplate", GameObject)
RegistChildComponent(LuaYaoGuaiInfoWnd, "LabelTable", "LabelTable", UITable)
RegistChildComponent(LuaYaoGuaiInfoWnd, "LabelBg", "LabelBg", UISprite)
RegistChildComponent(LuaYaoGuaiInfoWnd, "BtnTemplate", "BtnTemplate", GameObject)
RegistChildComponent(LuaYaoGuaiInfoWnd, "BtnTable", "BtnTable", UITable)
RegistChildComponent(LuaYaoGuaiInfoWnd, "BtnBg", "BtnBg", UISprite)

RegistClassMember(LuaYaoGuaiInfoWnd, "m_QualityBorderColorList")
RegistClassMember(LuaYaoGuaiInfoWnd, "m_Wnd")

function LuaYaoGuaiInfoWnd:Init()
    self.m_QualityBorderColorList = {}
    self.m_Wnd=self.gameObject:GetComponent(typeof(CCommonLuaWnd))
    local list = g_LuaUtil:StrSplit(ZhuoYao_Setting.GetData("QualityBorderColor").Value, ";")
    for _,s in pairs(list) do
        local k2v = g_LuaUtil:StrSplit(s, ",")
        table.insert(self.m_QualityBorderColorList, k2v[1])
        table.insert(self.m_QualityBorderColorList, k2v[2])
    end
    
    local data = LuaZhuoYaoMgr.m_TipData
    local tujianId = data.TemplateId
    local tujiandata = ZhuoYao_TuJian.GetData(tujianId)
    local name = tujiandata.Name
    local icon = tujiandata.Icon
    local quality = data.Quality
    local borderSpriteName = LuaZhuoYaoMgr:GetYaoGuaiQualityBorderColor(tujiandata, data.Quality)
    local level = data.Level
    
    self.Portrait:LoadNPCPortrait(icon)
    self.QualitySprite.spriteName = borderSpriteName
    self.NameLabel.text = name
    self.LevelLabel.text = SafeStringFormat3(LocalString.GetString("%d级"),level)

    self:InitBtns()
    self:InitLabels()
end

function LuaYaoGuaiInfoWnd:InitBtns()
    self.BtnTemplate:SetActive(false)

    if LuaZhuoYaoMgr.m_TipBtnActions == nil or #LuaZhuoYaoMgr.m_TipBtnActions == 0 then
        self.BtnBg.gameObject:SetActive(false)
        return
    end

    for i=1,#LuaZhuoYaoMgr.m_TipBtnActions do
        local pair = LuaZhuoYaoMgr.m_TipBtnActions[i]
        local instance = CUICommonDef.AddChild(self.BtnTable.gameObject, self.BtnTemplate)
        instance:SetActive(true)
        local btnLabel = instance.transform:Find("Label"):GetComponent(typeof(UILabel))
        btnLabel.text = pair.Key
        if pair.Value ~= nil then
            UIEventListener.Get(instance).onClick = DelegateFactory.VoidDelegate(function (go)
                invoke(pair.Value)
            end)
        end
    end

    self.BtnTable:Reposition()
    self.BtnBg.gameObject:SetActive(true)
    local b = NGUIMath.CalculateRelativeWidgetBounds(self.BtnBg.transform, self.BtnTable.transform)
    self.BtnBg.height = math.floor((b.size.y + 40))
    --self.BtnBg.transform.localPosition = b.center
end

function LuaYaoGuaiInfoWnd:InitLabels()
    self.LabelTemplate:SetActive(false)

    if LuaZhuoYaoMgr.m_TipLabels == nil or #LuaZhuoYaoMgr.m_TipLabels == 0 then
        return
    end

    for i=1,#LuaZhuoYaoMgr.m_TipLabels do
        local pair = LuaZhuoYaoMgr.m_TipLabels[i]
        local instance = CUICommonDef.AddChild(self.LabelTable.gameObject, self.LabelTemplate)
        instance:SetActive(true)
        local titleLabel = instance:GetComponent(typeof(UILabel))
        local contentLabel = instance.transform:Find("Label"):GetComponent(typeof(UILabel))
        titleLabel.text = pair.Key
        contentLabel.text = pair.Value
    end

    self.LabelTable:Reposition()
    self.LabelBg.gameObject:SetActive(true)
    local b = NGUIMath.CalculateRelativeWidgetBounds(self.LabelBg.transform, self.LabelTable.transform)
    self.LabelBg.height = math.floor((b.size.y + 250))
end

function LuaYaoGuaiInfoWnd:Update( )
    self.m_Wnd:ClickThroughToClose()
end
