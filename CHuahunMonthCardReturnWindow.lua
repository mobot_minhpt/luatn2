-- Auto Generated!!
local Boolean = import "System.Boolean"
local CChargeWnd = import "L10.UI.CChargeWnd"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CFileTools = import "CFileTools"
local CHuahunMonthCardReturnWindow = import "L10.UI.CHuahunMonthCardReturnWindow"
local CommonDefs = import "L10.Game.CommonDefs"
local CPropertyItem = import "L10.Game.CPropertyItem"
local CTickMgr = import "L10.Engine.CTickMgr"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EShareType = import "L10.UI.EShareType"
local ETickType = import "L10.Engine.ETickType"
local EventManager = import "EventManager"
local Gac2Gas = import "L10.Game.Gac2Gas"
local HTTPHelper = import "L10.Game.HTTPHelper"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local Main = import "L10.Engine.Main"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local System = import "System"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local ClientAction = import "L10.UI.ClientAction"
local CLoginMgr = import "L10.Game.CLoginMgr"
local ShareMgr=import "ShareMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
CHuahunMonthCardReturnWindow.m_Init_CS2LuaHook = function (this)
    LuaProfessionTransferMgr.openShareBoxOnce = false
    
    --titleLabel.text = "";
    this.leftdaysLabel.text = ""
    this.slider.value = 0
    this.buttonLabel.text = LocalString.GetString("领   取")
    this.shareBtnLabel.text = LocalString.GetString("分   享")
    this.getBtn.Enabled = false
    this.cardInfo = CClientMainPlayer.Inst.ItemProp.ZhuanZhiReturn
    --titleLabel.text = MessageMgr.Inst.FormatMessage("MonthCard_FuLi_Tip");
    this.transform:Find("Title"):GetComponent(typeof(UILabel)).text = g_MessageMgr:FormatMessage("PROFESSIONTRANSFER_MONTH_CARD_RETURN_TITLE")
    this.transform:Find("tip"):GetComponent(typeof(UILabel)).text = g_MessageMgr:FormatMessage("PROFESSIONTRANSFER_MONTH_CARD_RETURN_TIP")

    local haveTransfer = LuaProfessionTransferMgr.TransferInDuration4Return()
    this.shareBtn:SetActive(haveTransfer)
    this.getBtn.gameObject:SetActive(haveTransfer)
    this.transform:Find("GoTransferButton").gameObject:SetActive((not haveTransfer))

    if haveTransfer then
        if this.cardInfo ~= nil and this.cardInfo.ZhuanZhiTime ~= 0 then
            CUICommonDef.SetActive(this.shareBtn, this.cardInfo.Shared == 0, true)
            local leftDay = CChargeWnd.HuahunMonthCardLeftDay(this.cardInfo)
            if leftDay > 0 then
                this:InitMonthCard(leftDay)
                return
            end
            this.slider.gameObject:SetActive(true)
        end
    else
        this.slider.gameObject:SetActive(false)
    end
end
CHuahunMonthCardReturnWindow.m_InitMonthCard_CS2LuaHook = function (this, leftDay)
    local hasGetToday = CPropertyItem.IsSamePeriod(this.cardInfo.LastReturnTime, "d")

    if leftDay > 0 then
        this.getBtn.gameObject:SetActive(true)
        this.leftdaysLabel.text = System.String.Format(LocalString.GetString("剩余{0}/30天"), leftDay)
        this.getBtn.Enabled = not hasGetToday
        this.alertButton1.enabled = not hasGetToday
    else
        return
    end
    this.slider.value = leftDay / 30
end

CHuahunMonthCardReturnWindow.m_OnEnable_CS2LuaHook = function (this)
    this:Init()
    EventManager.AddListener(EnumEventType.ItemPropUpdated, MakeDelegateFromCSFunction(this.Init, Action0, this))
    UIEventListener.Get(this.getBtn.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.getBtn.gameObject).onClick, MakeDelegateFromCSFunction(this.OnGetBtnClick, VoidDelegate, this), true)
    UIEventListener.Get(this.shareBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.shareBtn).onClick, MakeDelegateFromCSFunction(this.OnShareBtnClick, VoidDelegate, this), true)
    UIEventListener.Get(this.transform:Find("GoTransferButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        local action = ProfessionTransfer_Setting.GetData().FindTransferNPC
        if action then
            ClientAction.DoAction(action)
            CUIManager.CloseUI("WelfareWnd")
            if CUIManager.IsLoaded(CLuaUIResources.ProfessionTransferMainWnd) then
                CUIManager.CloseUI(CLuaUIResources.ProfessionTransferMainWnd)
            end
        end
    end)
    EventManager.AddListenerInternal(EnumEventType.ShareFinished, MakeDelegateFromCSFunction(this.ShareFinished, MakeGenericClass(Action2, Int32, Boolean), this))
end
CHuahunMonthCardReturnWindow.m_ShareFinished_CS2LuaHook = function (this, code, finish)
    --发消息给服务器，报告分享成功
    if finish then
        --Gac2Gas.RequestZhouNianQingShareAward((uint)nowChooseIndex);
        Gac2Gas.ShareZhuanZhiReturn()
    end
end
CHuahunMonthCardReturnWindow.m_Share2Web_CS2LuaHook = function (this)
--    if CClientMainPlayer.Inst == nil then
--        return
--    end
--
--    local roleid = CClientMainPlayer.Inst.Id
--    local rolename = CClientMainPlayer.Inst.Name
--    local grade = CClientMainPlayer.Inst.Level
--
--    local totalGameRegion = CLoginMgr.Inst.AllGameRegions
--    local myGameServer = CLoginMgr.Inst:GetSelectedGameServer()
--    local server = myGameServer.name
--    do
--        local i = 0
--        while i < totalGameRegion.Count do
--            local gameRegion = totalGameRegion[i]
--            do
--                local j = 0
--                while j < gameRegion.servers.Count do
--                    local gameServer = gameRegion.servers[j]
--                    if gameServer.id == myGameServer.id then
--                        server = server .. ("-" .. gameRegion.name)
--                        break
--                    end
--                    j = j + 1
--                end
--            end
--            i = i + 1
--        end
--    end
--
--    local profession = EnumToInt(CClientMainPlayer.Inst.Class)
--    local gender = EnumToInt(CClientMainPlayer.Inst.Gender)
--    local xiuwei = math.floor(CClientMainPlayer.Inst.BasicProp.XiuWeiGrade)
--
--
--    local md5 = System.Security.Cryptography.MD5.Create()
--    local token = ((CHuahunMonthCardReturnWindow.s_CipherCode .. tostring(CClientMainPlayer.Inst.Id)) .. CClientMainPlayer.Inst.Level) .. EnumToInt(CClientMainPlayer.Inst.Class)
--    local inputBytes = System.Text.Encoding.ASCII:GetBytes(token)
--    local hash = md5:ComputeHash(inputBytes)
--
--    local sb = NewStringBuilderWraper(StringBuilder)
--    sb:Append((NumberComplexToString(hash[0], typeof(Byte), "x2") .. NumberComplexToString(hash[1], typeof(Byte), "x2")) .. NumberComplexToString(hash[2], typeof(Byte), "x2"))
--
--    local url = CommonDefs.String_Format_String_ArrayObject(CHuahunMonthCardReturnWindow.GetShareUrl(), {
--        roleid,
--        WWW.EscapeURL(rolename),
--        grade,
--        WWW.EscapeURL(server),
--        profession,
--        gender,
--        xiuwei,
--        ToStringWrap(sb)
--    })

--    Main.Inst:StartCoroutine(HTTPHelper.GetUrl(url, DelegateFactory.Action_bool_string(function (success, text)
--        if success then
--            this:ShareZhuanzhiImage(text)
--        end
--    end)))

    Gac2Gas.ShareZhuanZhiReturn()
    CUICommonDef.SetActive(this.shareBtn, this.cardInfo.Shared == 0, true)
end
CHuahunMonthCardReturnWindow.m_ShareZhuanzhiImage_CS2LuaHook = function (this, text)
  --    local dic = TypeAs(L10.Game.Utils.Json.Deserialize(text), typeof(MakeGenericClass(Dictionary, String, Object)))
  --    local code = 0
  --    if CommonDefs.DictContains(dic, typeof(String), "code") then
  --        code = math.floor(tonumber(CommonDefs.DictGetValue(dic, typeof(String), "code") or 0))
  --    end
  --    if code == 1 then
  --        if CommonDefs.DictContains(dic, typeof(String), "imgurl") then
  --            local imgurl = tostring(CommonDefs.DictGetValue(dic, typeof(String), "imgurl"))
  --
  --            if this.m_PicCoroutine ~= nil then
  --                Main.Inst:StopCoroutine(this.m_PicCoroutine)
  --                this.m_PicCoroutine = nil
  --            end
  --
  --            this.m_PicCoroutine = Main.Inst:StartCoroutine(HTTPHelper.DownloadImage(imgurl, DelegateFactory.Action_bool_byte_Array(function (picResult, picData)
  --                local filename = Utility.persistentDataPath .. "/HuahunZhuanzhiShare.jpg"
  --                System.IO.File.WriteAllBytes(filename, picData)
  --                CFileTools.SetFileNoBackup(filename)
  --
  --                CTickMgr.Register(DelegateFactory.Action(function ()
  --                    ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, {filename})
  --                end), 1000, ETickType.Once)
  --            end)))
  --        end
  --    end

  local imgurl = text

  if this.m_PicCoroutine ~= nil then
    Main.Inst:StopCoroutine(this.m_PicCoroutine)
    this.m_PicCoroutine = nil
  end
    if LuaProfessionTransferMgr.m_waitShareBoxCloseTick ~= nil then
        UnRegisterTick(LuaProfessionTransferMgr.m_waitShareBoxCloseTick)
        LuaProfessionTransferMgr.m_waitShareBoxCloseTick = nil
    end

  this.m_PicCoroutine = Main.Inst:StartCoroutine(HTTPHelper.DownloadImage(imgurl, DelegateFactory.Action_bool_byte_Array(function (picResult, picData)
    local filename = Utility.persistentDataPath .. "/HuahunZhuanzhiShare.jpg"
    System.IO.File.WriteAllBytes(filename, picData)
    CFileTools.SetFileNoBackup(filename)

    CTickMgr.Register(DelegateFactory.Action(function ()
        LuaProfessionTransferMgr.openShareBoxOnce = true  
      ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, {filename})
    end), 1000, ETickType.Once)

      LuaProfessionTransferMgr.m_waitShareBoxCloseTick = RegisterTick(function ()
          if LuaProfessionTransferMgr.openShareBoxOnce and (not CUIManager.IsLoaded(CUIResources.ShareBox3)) and (not CUIManager.IsLoading(CUIResources.ShareBox3)) then
              LuaProfessionTransferMgr.openShareBoxOnce = false
              Gac2Gas.ShareZhuanZhiReturn()
          end
      end, 100)
  end)))
end
