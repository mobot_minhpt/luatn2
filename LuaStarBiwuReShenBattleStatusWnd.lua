local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local QnTableView = import "L10.UI.QnTableView"
local Constants = import "L10.Game.Constants"

LuaStarBiwuReShenBattleStatusWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaStarBiwuReShenBattleStatusWnd, "TipButton", "TipButton", GameObject)
RegistChildComponent(LuaStarBiwuReShenBattleStatusWnd, "TimeLabel", "TimeLabel", UILabel)
RegistChildComponent(LuaStarBiwuReShenBattleStatusWnd, "YellowButton", "YellowButton", GameObject)
RegistChildComponent(LuaStarBiwuReShenBattleStatusWnd, "BlueButton", "BlueButton", GameObject)
RegistChildComponent(LuaStarBiwuReShenBattleStatusWnd, "QnTableView", "QnTableView", QnTableView)

--@endregion RegistChildComponent end
RegistClassMember(LuaStarBiwuReShenBattleStatusWnd,"m_List")

function LuaStarBiwuReShenBattleStatusWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)


	
	UIEventListener.Get(self.YellowButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnYellowButtonClick()
	end)


	
	UIEventListener.Get(self.BlueButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBlueButtonClick()
	end)


    --@endregion EventBind end
end

function LuaStarBiwuReShenBattleStatusWnd:Init()
	self.TimeLabel.text = g_MessageMgr:FormatMessage("StarBiwuReShenBattleStatusWnd_TimeLabel")
	self.QnTableView.m_DataSource = DefaultTableViewDataSource.Create(
        function() 
            return self.m_List and #self.m_List or 0
        end,
        function(item, index)
            self:ItemAt(item, index)
        end)
	self:OnSyncStarBiwuReShenSaiZK()
end

function LuaStarBiwuReShenBattleStatusWnd:ItemAt(item, index)
	local data = self.m_List[index + 1]

	local timesLabel = item.transform:Find("TimesLabel"):GetComponent(typeof(UILabel))
	local teamItem1 = item.transform:Find("TeamItem1")
	local nameLabel1 = item.transform:Find("TeamItem1/NameLabel1"):GetComponent(typeof(UILabel))
	local waitLabel1 = item.transform:Find("TeamItem1/WaitLabel"):GetComponent(typeof(UILabel))
	local teamItem2 = item.transform:Find("TeamItem2")
	local nameLabel2 = item.transform:Find("TeamItem2/NameLabel1"):GetComponent(typeof(UILabel))
	local waitLabel2 = item.transform:Find("TeamItem2/WaitLabel"):GetComponent(typeof(UILabel))
	local resultSprite1 = item.transform:Find("ResultSprite1"):GetComponent(typeof(UISprite))
	local resultSprite2 = item.transform:Find("ResultSprite2"):GetComponent(typeof(UISprite))
	local infoButton = item.transform:Find("InfoButton")
	local startTimeLabel = item.transform:Find("StartTimeLabel"):GetComponent(typeof(UILabel))

	timesLabel.text = SafeStringFormat3(LocalString.GetString("第%d轮"), index + 1)
	nameLabel1.text = data.selfZhanduiName
	nameLabel1.gameObject:SetActive(data.zhanduiName ~= nil)
	waitLabel1.gameObject:SetActive(data.zhanduiName == nil)
	nameLabel2.text = data.zhanduiName
	nameLabel2.gameObject:SetActive(data.zhanduiName ~= nil)
	waitLabel2.gameObject:SetActive(data.zhanduiName == nil)
	local isWin = data.status == EnumStarBiwuFightCommonResult.eWinByFight or data.status == EnumStarBiwuFightCommonResult.eWinByEnemyQuit
	local isLose = data.status == EnumStarBiwuFightCommonResult.eLossByFight or data.status == EnumStarBiwuFightCommonResult.eLossByQuit
	local isPin =  data.status == EnumStarBiwuFightCommonResult.eEvenBothQuit 
	resultSprite1.gameObject:SetActive(isWin or isLose or isPin)
	resultSprite2.gameObject:SetActive(isWin or isLose or isPin)
	resultSprite1.spriteName = isWin and Constants.WinSprite or (isLose and Constants.LoseSprite or "common_fight_result_tie")
	resultSprite2.spriteName = isWin and Constants.LoseSprite or (isLose and Constants.WinSprite or "common_fight_result_tie")
	infoButton.gameObject:SetActive(data.zhanduiId ~= nil)
	UIEventListener.Get(infoButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
		if data.zhanduiId then
			CLuaStarBiwuMgr:ShowZhanDuiMemberWnd(0, data.zhanduiId, 1) 
		end
	end)
	startTimeLabel.text = StarBiWuShow_QudaoSetting.GetData().ReShenSai_StartMatch_Time[index]
end

function LuaStarBiwuReShenBattleStatusWnd:OnEnable()
	g_ScriptEvent:AddListener("OnSyncStarBiwuReShenSaiZK", self, "OnSyncStarBiwuReShenSaiZK")
end

function LuaStarBiwuReShenBattleStatusWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnSyncStarBiwuReShenSaiZK", self, "OnSyncStarBiwuReShenSaiZK")
end

function LuaStarBiwuReShenBattleStatusWnd:OnSyncStarBiwuReShenSaiZK()
	self.m_List = CLuaStarBiwuMgr.m_ReShenSaiZkDataList
	local isInBattle = false
	for round, t in pairs(self.m_List) do
		print(round,t.status)
		if t.status == EnumStarBiwuFightCommonResult.eNoResultYet then
			isInBattle = true
		end
	end
	self.YellowButton.gameObject:SetActive(isInBattle)
	self.BlueButton.gameObject:SetActive(not isInBattle)
	self.QnTableView:ReloadData(true, true)
end

--@region UIEvent

function LuaStarBiwuReShenBattleStatusWnd:OnTipButtonClick()
	g_MessageMgr:ShowMessage("StarBiwuReShenBattleStatusWnd_ReadMe")
end

function LuaStarBiwuReShenBattleStatusWnd:OnYellowButtonClick()
	Gac2Gas.RequestEnterStarBiwuPrepare_QD()
end

function LuaStarBiwuReShenBattleStatusWnd:OnBlueButtonClick()
	g_MessageMgr:ShowMessage("StarBiwuReShenBattleStatusWnd_IsNotInMatching")
end

--@endregion UIEvent

