-- Auto Generated!!
local CNationalDayMgr = import "L10.UI.CNationalDayMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local MsgPackImpl = import "MsgPackImpl"
local Object = import "System.Object"
local UISprite = import "UISprite"
local XianZongShan_Group = import "L10.Game.XianZongShan_Group"


CNationalDayMgr.m_SetForceIcon_CS2LuaHook = function (this, node, force) 
    local groupInfo = XianZongShan_Group.GetData(force)
    if groupInfo ~= nil then
        local ct = CommonDefs.GetComponent_GameObject_Type(node, typeof(UISprite))
        if ct ~= nil then
            ct.spriteName = groupInfo.Icon
            ct:MakePixelPerfect()
        end
    end
end
Gas2Gac.ShowGuoQingXianLiDaTiWnd = function (index, playerId, playerName, question, choices_U, answerDuration) 
    local choices = TypeAs(MsgPackImpl.unpack(choices_U), typeof(MakeGenericClass(List, Object)))
    CNationalDayMgr.Inst.m_Index = index
    CNationalDayMgr.Inst.m_PlayerId = playerId
    CNationalDayMgr.Inst.m_PlayerName = playerName
    CNationalDayMgr.Inst.m_Question = question
    CNationalDayMgr.Inst.m_Options = choices
    CNationalDayMgr.Inst.m_AnswerDuration = answerDuration
    CNationalDayMgr.Inst:ShowUI()
end
