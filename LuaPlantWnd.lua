local CPlantWnd = import "L10.UI.CPlantWnd"
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemMgr = import "L10.Game.CItemMgr"
local Vector3 = import "UnityEngine.Vector3"

CPlantWnd.m_OnSelectAtRow_CS2LuaHook = function(this, index)
    this.m_CurPlantInfo = this.m_Type2PlantDict[this.m_CurType][index]
    this.m_PlantName.text = this.m_CurPlantInfo.m_Name
    this.m_GrowTime.text = this:GetTimeString(this.m_CurPlantInfo.m_GrowTime)
    this.m_GardenLevel.text = String.Format(LocalString.GetString("{0}级"), this.m_CurPlantInfo.m_GardenLevel)
    this.m_GardenLevel.color = (this.m_CurPlantInfo.m_GardenLevel > this.MyGardenLevel) and Color.red or
                                   NGUIText.ParseColor24("DFFFFF", 0)
    this.m_CharacterXiuwei.text = ToStringWrap(this.m_CurPlantInfo.m_HarvestXiuwei)
    this.m_CharacterXiuwei.color = (this.m_CurPlantInfo.m_HarvestXiuwei > this.MyXiuwei) and Color.red or
                                       NGUIText.ParseColor24("DFFFFF", 0)
    -- 格式更改 取消换行
    this.m_DisplayLabel.text = "[F7FF7B]" .. string.gsub(this.m_CurPlantInfo.m_Display, '#r', '')
    -- local typeStr = {LocalString.GetString("粮食"), LocalString.GetString("木材"),LocalString.GetString("石材"),LocalString.GetString("资源")}
    --this.m_DisplayLabel.text = SafeStringFormat3(LocalString.GetString("[F7FF7B]品质越高，可兑换的%s越多。推荐离线种植。"), typeStr[this.m_CurType+1])

    local rawInfo = this.m_CurPlantInfo.m_ExtraProduct
    local productsInfo = {}
    local productsSet = {}
    local excludeProduct = {}
    excludeProduct["21003756"] = 1
    excludeProduct["21003757"] = 1
    excludeProduct["21003758"] = 1

    if rawInfo then
        for i=0, rawInfo.Length-1 do
            if i%3==0 then
                local p = rawInfo[i]
                if productsSet[p] == nil and excludeProduct[p]==nil then
                    productsSet[p] = 1
                    table.insert(productsInfo, p)
                end
            end
        end
    end

    local initItem = function(item, index)
        local transform = item.transform
        local icon = transform:Find("Icon"):GetComponent(typeof(CUITexture))
        local id = CommonDefs.Convert_ToUInt32(productsInfo[index+1])
        local template = Item_Item.GetData(id)
        if template then
            local item = CItemMgr.Inst:GetById(id)
            icon:LoadMaterial(template.Icon)
        end
    end
    this.m_ProductTable.OnSelectAtRow=DelegateFactory.Action_int(function(row)
        local id = productsInfo[row+1]
        CItemInfoMgr.ShowLinkItemTemplateInfo(id, false, nil, AlignType.Default, 0, 0, 0, 0)
    end)
    local dataSource = DefaultTableViewDataSource.CreateByCount(#productsInfo, initItem)
    this.m_ProductTable.m_DataSource = dataSource
    this.m_ProductTable:ReloadData(true, false)

    local constructInfo = CClientHouseMgr.Inst.mConstructProp
    if constructInfo then
        local grade = constructInfo.CangkuGrade
        local data = House_CangkuUpgrade.GetData(grade)
        if data then
            this.m_FoodLabel.text = SafeStringFormat3("%d/%d", constructInfo.Food, data.MaxFood)
            this.m_FoodSlider.value = constructInfo.Food / data.MaxFood
            this.m_WoodLabel.text = SafeStringFormat3("%d/%d", constructInfo.Wood, data.MaxWood)
            this.m_WoodSlider.value = constructInfo.Wood / data.MaxWood
            this.m_StoneLabel.text = SafeStringFormat3("%d/%d", constructInfo.Stone, data.MaxStone)
            this.m_StoneSlider.value = constructInfo.Stone / data.MaxStone
        end
    end

    this.m_ConsumeItem.gameObject:SetActive(this.m_CurPlantInfo.m_ConsumeItem ~= nil)
    if this.m_CurPlantInfo.m_ConsumeItem then
        local data = Item_Item.GetData(this.m_CurPlantInfo.m_ConsumeItem[0])
        if data then
            this.m_ConsumeItem.text = data.Name
        end
    end

    local types = {}
    if this.m_CurPlantInfo.m_ProductRate then
        for i = 0, 2 do
            this.m_ProductIcon[i].gameObject:SetActive(false)
            if this.m_CurPlantInfo.m_ProductRate[i] ~= 0 then
                table.insert(types, i)
            end
        end
    end

    for i = 0, 2 do
        this.m_ProductIcon[i].gameObject:SetActive(false)
    end

    for i = 1, #types do
        local type = types[i]
        this.m_ProductIcon[i - 1].gameObject:SetActive(true)
        this.m_ProductIcon[i - 1].spriteName = CPlantWnd.m_ProductIconList[type]
    end

    if #types == 0 then
        this.m_ProductRate.text = "0"
    else
        this.m_ProductRate.text = ToStringWrap(this.m_CurPlantInfo.m_ProductRate[types[1]])
    end

    local pos = Vector3(646, 70, 0)
    local move = Vector3(46 * (3 - #types), 0, 0)
    this.m_ProductRate.transform.localPosition = CommonDefs.op_Subtraction_Vector3_Vector3(pos, move)

    this.m_CurPlantId = this.m_CurPlantInfo.m_Id
end
