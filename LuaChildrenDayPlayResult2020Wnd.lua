local GameObject = import "UnityEngine.GameObject"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local CUITexture = import "L10.UI.CUITexture"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CLoginMgr = import "L10.Game.CLoginMgr"

LuaChildrenDayPlayResult2020Wnd = class()
RegistChildComponent(LuaChildrenDayPlayResult2020Wnd,"closeBtn", GameObject)
RegistChildComponent(LuaChildrenDayPlayResult2020Wnd,"shareBtn", GameObject)
RegistChildComponent(LuaChildrenDayPlayResult2020Wnd,"checkBtn", GameObject)
RegistChildComponent(LuaChildrenDayPlayResult2020Wnd,"playerInfoNode", GameObject)
RegistChildComponent(LuaChildrenDayPlayResult2020Wnd,"winNode", GameObject)
RegistChildComponent(LuaChildrenDayPlayResult2020Wnd,"loseNode", GameObject)

RegistClassMember(LuaChildrenDayPlayResult2020Wnd, "maxChooseNum")

function LuaChildrenDayPlayResult2020Wnd:Close()
	CUIManager.CloseUI(CLuaUIResources.ChildrenDayPlayResult2020Wnd)
end

function LuaChildrenDayPlayResult2020Wnd:OnEnable()
	--g_ScriptEvent:AddListener("", self, "")
end

function LuaChildrenDayPlayResult2020Wnd:OnDisable()
	--g_ScriptEvent:RemoveListener("", self, "")
end

function LuaChildrenDayPlayResult2020Wnd:Init()
	local onCloseClick = function(go)
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.closeBtn,DelegateFactory.Action_GameObject(onCloseClick),false)

	if CommonDefs.IS_VN_CLIENT then 
		self.shareBtn:SetActive(false)
	else
		local onShareClick = function(go)
			CUICommonDef.CaptureScreenAndShare()
		end
		CommonDefs.AddOnClickListener(self.shareBtn,DelegateFactory.Action_GameObject(onShareClick),false)
	end
	local onCheckClick = function(go)
		LuaChildrenDay2020Mgr.OpenRankWnd()
	end
	CommonDefs.AddOnClickListener(self.checkBtn,DelegateFactory.Action_GameObject(onCheckClick),false)

	CommonDefs.GetComponent_Component_Type(self.playerInfoNode.transform:Find("name"), typeof(UILabel)).text = CClientMainPlayer.Inst.BasicProp.Name
	local myGameServer = CLoginMgr.Inst:GetSelectedGameServer()
	CommonDefs.GetComponent_Component_Type(self.playerInfoNode.transform:Find("server"), typeof(UILabel)).text = myGameServer.name
	CommonDefs.GetComponent_Component_Type(self.playerInfoNode.transform:Find("ID"), typeof(UILabel)).text = tostring(CClientMainPlayer.Inst.Id)
	CommonDefs.GetComponent_Component_Type(self.playerInfoNode.transform:Find("icon"), typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(CClientMainPlayer.Inst.Class, CClientMainPlayer.Inst.Gender, -1), false)
	CommonDefs.GetComponent_Component_Type(self.playerInfoNode.transform:Find("icon/lv"), typeof(UILabel)).text = tostring(CClientMainPlayer.Inst.Level)

	self:InitResultShow()
end

function LuaChildrenDayPlayResult2020Wnd:InitResultPanel(node)
	local killLabel = node.transform:Find('scoreLabel'):GetComponent(typeof(UILabel))
	local tanzhuLabel = node.transform:Find('bonusLabel'):GetComponent(typeof(UILabel))
	local rankLabel = node.transform:Find('rank'):GetComponent(typeof(UILabel))
	local gameTypeLabel = node.transform:Find('label'):GetComponent(typeof(UILabel))
	local danzhuPic = node.transform:Find('win_bg/ball'):GetComponent(typeof(CUITexture))
	killLabel.text = LuaChildrenDay2020Mgr.gameResultKillCount
	tanzhuLabel.text = math.abs(LuaChildrenDay2020Mgr.gameResultLiveCount)
	rankLabel.text = LuaChildrenDay2020Mgr.gameResultRank
	local gameTypeStringTable = {LocalString.GetString('弹珠争霸正式比赛'),LocalString.GetString('弹珠争霸练习赛')}
	gameTypeLabel.text = gameTypeStringTable[LuaChildrenDay2020Mgr.gameResultType]

	local danzhuString = {'UI/Texture/Transparent/Material/childrenday2020_shui.mat','UI/Texture/Transparent/Material/childrenday2020_hu.mat','UI/Texture/Transparent/Material/childrenday2020_huo.mat'}
	danzhuPic:LoadMaterial(danzhuString[LuaChildrenDay2020Mgr.gameResultDanzhuType])
end

function LuaChildrenDayPlayResult2020Wnd:InitResultShow()
--LuaChildrenDay2020Mgr.gameResultType = gameType
--LuaChildrenDay2020Mgr.gameResultRank = rank
--LuaChildrenDay2020Mgr.gameResultKillCount = killCount
--LuaChildrenDay2020Mgr.gameResultLiveCount = liveCount
--LuaChildrenDay2020Mgr.gameResultbWashOut = bWashOut
	if LuaChildrenDay2020Mgr.gameResultLiveCount >= 0 then
		self.winNode:SetActive(true)
		self.loseNode:SetActive(false)
		self:InitResultPanel(self.winNode)
	else
		self.winNode:SetActive(false)
		self.loseNode:SetActive(true)
		self:InitResultPanel(self.loseNode)
	end
end

function LuaChildrenDayPlayResult2020Wnd:OnDestroy()

end

return LuaChildrenDayPlayResult2020Wnd
