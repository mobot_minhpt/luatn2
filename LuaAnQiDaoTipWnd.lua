local CTipParagraphItem = import "L10.UI.CTipParagraphItem"
local UITable = import "UITable"
local Extensions = import "Extensions"
local MessageMgr = import "L10.Game.MessageMgr"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"

CLuaAnQiDaoTipWnd = class()

RegistClassMember(CLuaAnQiDaoTipWnd, "m_Table")
RegistClassMember(CLuaAnQiDaoTipWnd, "m_ParagraphTemplate")

function CLuaAnQiDaoTipWnd:Awake()
    self:InitComponents()
end

function CLuaAnQiDaoTipWnd:InitComponents()
    self.m_Table = self.transform:Find("Rot/Background/ContentScrollView/Table"):GetComponent(typeof(UITable))
    self.m_ParagraphTemplate = self.transform:Find("Rot/Background/Templates/ParagraphTemplate").gameObject
end

function CLuaAnQiDaoTipWnd:Init()
    Extensions.RemoveAllChildren(self.m_Table.transform)
    local ruleMessage = MessageMgr.Inst:FormatMessage("ANQIISLAND_README", {})
    local ruleTipInfo = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(ruleMessage)
    if ruleTipInfo ~= nil then
        for i = 0, ruleTipInfo.paragraphs.Count - 1 do
            local rule = CUICommonDef.AddChild(self.m_Table.gameObject, self.m_ParagraphTemplate):GetComponent(typeof(CTipParagraphItem))
            rule.gameObject:SetActive(true)
            rule:Init(ruleTipInfo.paragraphs[i], 0xffffffff)
        end
    end
    self.m_Table:Reposition()
end
