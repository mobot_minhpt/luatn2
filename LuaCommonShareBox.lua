local EChatPanel = import "L10.Game.EChatPanel"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local QnInput=import "L10.UI.QnInput"
local UITable = import "UITable"
local GameObject = import "UnityEngine.GameObject"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local CChatHelper = import "L10.UI.CChatHelper"
local CCommonPlayerListMgr = import "L10.Game.CCommonPlayerListMgr"
local CIndirectUIResources = import "L10.UI.CIndirectUIResources"

LuaCommonShareBox = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaCommonShareBox, "ItemLabel", "ItemLabel", UILabel)
RegistChildComponent(LuaCommonShareBox, "WishInput", "WishInput", QnInput)
RegistChildComponent(LuaCommonShareBox, "Table", "Table", UITable)
RegistChildComponent(LuaCommonShareBox, "ShareFriendButton", "ShareFriendButton", GameObject)
RegistChildComponent(LuaCommonShareBox, "ShareTeamButton", "ShareTeamButton", GameObject)
RegistChildComponent(LuaCommonShareBox, "ShareSectButton", "ShareSectButton", GameObject)
RegistChildComponent(LuaCommonShareBox, "ShareGuildButton", "ShareGuildButton", GameObject)

--@endregion RegistChildComponent end

function LuaCommonShareBox:Init()
	local enum2btn = {}
	enum2btn[EChatPanel.Friend] = self.ShareFriendButton
	enum2btn[EChatPanel.Team]   = self.ShareTeamButton
	enum2btn[EChatPanel.Sect]   = self.ShareSectButton
	enum2btn[EChatPanel.Guild]  = self.ShareGuildButton
	for enum, btn in pairs(enum2btn) do
		btn:SetActive(false)
		UIEventListener.Get(btn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
			if enum == EChatPanel.Friend then
				CCommonPlayerListMgr.Inst:ShowFriendsToShareJingLingAnswer(DelegateFactory.Action_ulong(function (playerId) 
					self:Share(enum, playerId)
					CUIManager.CloseUI(CIndirectUIResources.CommonPlayerListWnd)
				end))
			else
				self:Share(enum, 0)
			end
		end)
	end
	for _,enum in pairs(CLuaShareMgr.m_ShareChannels) do
		local btn = enum2btn[enum]
		if btn then
			btn:SetActive(true)
		end
	end
	self.Table:Reposition()
	self.ItemLabel.text = CUICommonDef.TranslateToNGUIText(CLuaShareMgr.m_TopLabelText)
	self.WishInput.OnValueChanged = DelegateFactory.Action_uint(function (value) 
        self:OnInputValueChanged(value)
    end)
end

function LuaCommonShareBox:OnInputValueChanged(value)
	local maxInputLen = 30
	if value.Length > maxInputLen * 2 then
		self.WishInput.Text = CommonDefs.StringSubstring2(value, 0,  maxInputLen)
	end
end

function LuaCommonShareBox:Share(channel,playerId)
	local inputText = self.WishInput.Text and self.WishInput.Text or ""
	if inputText:len() > 0 then
		local ret = CWordFilterMgr.Inst:DoFilterOnSendWithoutVoiceCheck(inputText,true)
		if not ret.msg then return end
		inputText = ret.msg
	end
	local msg = CLuaShareMgr.m_ShareMsg .. inputText
	CChatHelper.SendMsgWithFilterOption(channel, msg, playerId, true)
	CUIManager.CloseUI(CLuaUIResources.CommonShareBox)
end