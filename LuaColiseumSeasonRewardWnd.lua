local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local UILabel = import "UILabel"
local DelegateFactory  = import "DelegateFactory"
local UITabBar = import "L10.UI.UITabBar"
local GameObject = import "UnityEngine.GameObject"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local Constants = import "L10.Game.Constants"

LuaColiseumSeasonRewardWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaColiseumSeasonRewardWnd, "Tabs", "Tabs", UITabBar)
RegistChildComponent(LuaColiseumSeasonRewardWnd, "DanView1", "DanView1", GameObject)
RegistChildComponent(LuaColiseumSeasonRewardWnd, "DanView2", "DanView2", GameObject)
RegistChildComponent(LuaColiseumSeasonRewardWnd, "DanView3", "DanView3", GameObject)
RegistChildComponent(LuaColiseumSeasonRewardWnd, "DanView4", "DanView4", GameObject)
RegistChildComponent(LuaColiseumSeasonRewardWnd, "DanView5", "DanView5", GameObject)
RegistChildComponent(LuaColiseumSeasonRewardWnd, "DanRewardView", "DanRewardView", GameObject)
RegistChildComponent(LuaColiseumSeasonRewardWnd, "RankRewardView", "RankRewardView", GameObject)
RegistChildComponent(LuaColiseumSeasonRewardWnd, "DanRewardViewReadMeLabel", "DanRewardViewReadMeLabel", UILabel)
RegistChildComponent(LuaColiseumSeasonRewardWnd, "RankRewardViewReadMeLabel", "RankRewardViewReadMeLabel", UILabel)
RegistChildComponent(LuaColiseumSeasonRewardWnd, "TableView", "TableView", QnAdvanceGridView)

--@endregion RegistChildComponent end
RegistClassMember(LuaColiseumSeasonRewardWnd,"m_IsInitRankRewardView")
RegistClassMember(LuaColiseumSeasonRewardWnd,"m_ColiseumSeasonRankRewardsData")
RegistClassMember(LuaColiseumSeasonRewardWnd,"m_CurDan")
RegistClassMember(LuaColiseumSeasonRewardWnd,"m_IsInCurrentSeason")

function LuaColiseumSeasonRewardWnd:Awake()
    --@region EventBind: Dont Modify Manually!


	self.Tabs.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
	    self:OnTabsTabChange(index)
	end)


    --@endregion EventBind end
end

function LuaColiseumSeasonRewardWnd:Init()
	self.m_IsInCurrentSeason = LuaColiseumMgr:IsInCurrentSeason()
	self.m_CurDan = LuaColiseumMgr:GetDan(LuaColiseumMgr.m_Score)
	local danViewList = {self.DanView1,self.DanView2,self.DanView3,self.DanView4,self.DanView5}
	for index, danView in pairs(danViewList) do
		self:InitDanView(index, danView)
	end
	self:InitRankRewardView()
	self.DanRewardViewReadMeLabel.text = CUICommonDef.TranslateToNGUIText(g_MessageMgr:FormatMessage("ColiseumSeasonRewardWnd_DanRewardView_ReadMe"))
	self.RankRewardViewReadMeLabel.text = CUICommonDef.TranslateToNGUIText(g_MessageMgr:FormatMessage(self.m_IsInCurrentSeason and "ColiseumSeasonRewardWnd_RankRewardView_ReadMe" or "ColiseumSeasonRewardWnd_RankRewardView_NotInSeason"))
	self.Tabs:ChangeTab(0, false)
end

function LuaColiseumSeasonRewardWnd:InitRankRewardView()
	self.m_ColiseumSeasonRankRewardsData = {}
	Arena_RankInfo.ForeachKey(function (key)
		table.insert(self.m_ColiseumSeasonRankRewardsData,Arena_RankInfo.GetData(key))
	end)
	self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function() 
            return self:NumOfRow()
        end,
        function(item,index) self:InitItem(item,index) end
    )
end

function LuaColiseumSeasonRewardWnd:NumOfRow()
	return #self.m_ColiseumSeasonRankRewardsData
end

function LuaColiseumSeasonRewardWnd:InitItem(item,index)
	local data = self.m_ColiseumSeasonRankRewardsData[index + 1]

	--排名
	local minRank,maxRank = data.MinRank,data.MaxRank

	local myIndex = -1

	local rankLabel = item.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
	local rankImage = item.transform:Find("RankLabel/RankImage"):GetComponent(typeof(UISprite))
	local mySpite = item.transform:Find("RankLabel/MySpite").gameObject
	local table = item.transform:Find("Table"):GetComponent(typeof(UITable))
	local iconTextureTemplate = item.transform:Find("IconTextureTemplate").gameObject

	rankImage.spriteName = self:GetRankImageByRankIndex(index + 1)
	rankLabel.enabled = (index >= 3)
	rankLabel.text = SafeStringFormat3("%d~%d",minRank,maxRank)
	iconTextureTemplate.gameObject:SetActive(false)
	mySpite.gameObject:SetActive(myIndex == index)

	Extensions.RemoveAllChildren(table.transform)
	local seasonReward = CommonDefs.IS_VN_CLIENT and data.SeasonReward_VN or data.SeasonReward
	for i = 0, seasonReward.Length - 1,2 do
		local rewardItemId = tonumber(seasonReward[i])
		local rewardText = seasonReward[i + 1]
		rewardText = (rewardText == "1") and "" or rewardText
		local obj = NGUITools.AddChild(table.gameObject,iconTextureTemplate.gameObject)
		obj:SetActive(true)
		obj.transform:Find("Label"):GetComponent(typeof(UILabel)).text = CUICommonDef.TranslateToNGUIText(rewardText)
		local itemData = Item_Item.GetData(rewardItemId)
		obj:GetComponent(typeof(CUITexture)):LoadMaterial(itemData.Icon)
		UIEventListener.Get(obj.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
			CItemInfoMgr.ShowLinkItemTemplateInfo(rewardItemId)
		end)
	end
	table:Reposition()
end

function LuaColiseumSeasonRewardWnd:GetRankImageByRankIndex(index)
	if index == 1 then
		return Constants.RankFirstSpriteName
	elseif index == 2 then
		return Constants.RankSecondSpriteName
	elseif index == 3 then
		return Constants.RankThirdSpriteName
	else
		return nil
	end
end

function LuaColiseumSeasonRewardWnd:InitDanView(index, danView)
	local mySprite = danView.transform:Find("MySpite").gameObject
	local bg = danView.transform:Find("Bg").gameObject
	local scoreAreaLabel = danView.transform:Find("ScoreAreaLabel"):GetComponent(typeof(UILabel))
	local danLabel = danView.transform:Find("DanLabel"):GetComponent(typeof(UILabel))
	
	local danInfoData = Arena_DanInfo.GetData(index)

	--段位名称文字和颜色
	local danLabelText, danLabelColor = danInfoData.DanName,danInfoData.DanColor

	danLabel.text = danLabelText
	danLabel.color =  NGUIText.ParseColor24(danLabelColor, 0)
	mySprite:SetActive(self.m_CurDan == index)
	bg:SetActive(self.m_CurDan ~= index)
	
	if danView ~= self.DanView1 then
		local reward1 = danView.transform:Find("Reward1"):GetComponent(typeof(CUITexture))
		local receivedReward1 = reward1.transform:Find("ReceivedReward1").gameObject
		local rewardNumLabel1 = reward1.transform:Find("NumLabel"):GetComponent(typeof(UILabel))
		local reward2 = danView.transform:Find("Reward2"):GetComponent(typeof(CUITexture))
		local receivedReward2 = reward2.transform:Find("ReceivedReward2").gameObject
		local rewardNumLabel2 = reward2.transform:Find("NumLabel"):GetComponent(typeof(UILabel)) 

		--该段位奖励道具id
		local rewardItemId1,rewardItemId2 = 0,0
		local rewardText1,rewardText2 = "",""
		if danInfoData.DanReward then
			for i = 0,danInfoData.DanReward.Length - 1 ,2 do
				if i == 0 then
					rewardItemId1 = tonumber(danInfoData.DanReward[i])
					rewardText1 = (danInfoData.DanReward[i + 1] == "1") and "" or danInfoData.DanReward[i + 1]
				elseif i == 2 then
					rewardItemId2 = tonumber(danInfoData.DanReward[i])
					rewardText2 = (danInfoData.DanReward[i + 1] == "1") and "" or danInfoData.DanReward[i + 1]
				end
			end
		end

		local rewardItemData1,rewardItemData2 = Item_Item.GetData(rewardItemId1),Item_Item.GetData(rewardItemId2)
		--是否领取奖励道具
		local isReceivedReward = true
		if danInfoData then
			isReceivedReward = LuaColiseumMgr.m_MaxRankScore >= danInfoData.Score
		end

		reward1.gameObject:SetActive(rewardItemData1 ~= nil)
		if rewardItemData1 then
			reward1:LoadMaterial(rewardItemData1.Icon)
		end
		rewardNumLabel1.text = rewardText1
		reward2.gameObject:SetActive(rewardItemData2 ~= nil)
		if rewardItemData2 then
			reward2:LoadMaterial(rewardItemData2.Icon)
		end
		rewardNumLabel2.text = rewardText2
		receivedReward1:SetActive(isReceivedReward)
		receivedReward2:SetActive(isReceivedReward)

		UIEventListener.Get(reward1.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
			CItemInfoMgr.ShowLinkItemTemplateInfo(rewardItemId1)
		end)
		UIEventListener.Get(reward2.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
			CItemInfoMgr.ShowLinkItemTemplateInfo(rewardItemId2)
		end)
	end
end

--@region UIEvent

function LuaColiseumSeasonRewardWnd:OnTabsTabChange(index)
	self.DanRewardView:SetActive(index == 0)
	self.RankRewardView:SetActive(index == 1)
	if index == 1 and not self.m_IsInitRankRewardView then
		self.TableView:ReloadData(true, true)
		self.m_IsInitRankRewardView = true
	end
end

--@endregion UIEvent

