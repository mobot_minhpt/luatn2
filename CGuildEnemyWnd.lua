-- Auto Generated!!
local CGuildEnemyMgr = import "L10.Game.CGuildEnemyMgr"
local CGuildEnemyWnd = import "L10.UI.CGuildEnemyWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local Extensions = import "Extensions"
local LocalString = import "LocalString"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local VoidDelegate = import "UIEventListener+VoidDelegate"
--帮会敌对界面
CGuildEnemyWnd.m_Init_CS2LuaHook = function (this) 
    this.idArrow.enabled = false
    this.numArrow.enabled = false
    this.levelArrow.enabled = false

    this:OnGuildEnemyInfoUpdate()

    CGuildEnemyMgr.Inst:FilterGuildInfo(nil)

    this:OnNoGuildSelect()

    this.listView.OnSelected = MakeDelegateFromCSFunction(this.OnGuildSelect, MakeGenericClass(Action1, UInt32), this)
    this.searchView.OnSearch = MakeDelegateFromCSFunction(this.OnSearch, MakeGenericClass(Action1, String), this)

    this.listView:Init()
end
CGuildEnemyWnd.m_Start_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeButton).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.idButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.idButton).onClick, MakeDelegateFromCSFunction(this.OnIdButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.numButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.numButton).onClick, MakeDelegateFromCSFunction(this.OnNumButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.levelButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.levelButton).onClick, MakeDelegateFromCSFunction(this.OnLevelButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.setEnemyBtn.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.setEnemyBtn.gameObject).onClick, MakeDelegateFromCSFunction(this.OnSetEnemyButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.tipsButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.tipsButton).onClick, MakeDelegateFromCSFunction(this.OnTipsButtonClick, VoidDelegate, this), true)
end
CGuildEnemyWnd.m_OnGuildEnemyInfoUpdate_CS2LuaHook = function (this) 
    this.enemyNumLabel.text = System.String.Format("{0}/{1}", CGuildEnemyMgr.Inst.CurrentEnemyCount, CGuildEnemyMgr.Inst.MaxEnemyCount)
    local default
    if CGuildEnemyMgr.Inst:IsEnemyGuild(this.curGuildId) then
        default = LocalString.GetString("取消敌对")
    else
        default = LocalString.GetString("重点敌对")
    end
    this.setEnemyBtn.Text = default
    this.setEnemyBtn.Enabled = CGuildEnemyMgr.Inst.HasRightForSettingGuildEnemy
end
CGuildEnemyWnd.m_OnGuildSelect_CS2LuaHook = function (this, guildId) 
    this.curGuildId = guildId
    local default
    if CGuildEnemyMgr.Inst:IsEnemyGuild(guildId) then
        default = LocalString.GetString("取消敌对")
    else
        default = LocalString.GetString("重点敌对")
    end
    this.setEnemyBtn.Text = default
    this.setEnemyBtn.Enabled = CGuildEnemyMgr.Inst.HasRightForSettingGuildEnemy
    this.setEnemyBtn.gameObject:SetActive(true)
end
CGuildEnemyWnd.m_OnIdButtonClick_CS2LuaHook = function (this, go) 

    if not this.idArrow.enabled then
        this.idArrow.enabled = true
    elseif this.idArrow.transform.localRotation.z == 0 then
        Extensions.SetLocalRotationZ(this.idArrow.transform, 180)
    else
        Extensions.SetLocalRotationZ(this.idArrow.transform, 0)
    end
    this.numArrow.enabled = false
    this.levelArrow.enabled = false
    CGuildEnemyMgr.Inst:SortGuildListById(this.idArrow.transform.localRotation.z == 0)
    this.listView:Init()
    this:OnNoGuildSelect()
end
CGuildEnemyWnd.m_OnNumButtonClick_CS2LuaHook = function (this, go) 
    if not this.numArrow.enabled then
        this.numArrow.enabled = true
    elseif this.numArrow.transform.localRotation.z == 0 then
        Extensions.SetLocalRotationZ(this.numArrow.transform, 180)
    else
        Extensions.SetLocalRotationZ(this.numArrow.transform, 0)
    end
    this.idArrow.enabled = false
    this.levelArrow.enabled = false
    CGuildEnemyMgr.Inst:SortGuildListByNum(this.numArrow.transform.localRotation.z == 0)
    this.listView:Init()
    this:OnNoGuildSelect()
end
CGuildEnemyWnd.m_OnLevelButtonClick_CS2LuaHook = function (this, go) 
    if not this.levelArrow.enabled then
        this.levelArrow.enabled = true
    elseif this.levelArrow.transform.localRotation.z == 0 then
        Extensions.SetLocalRotationZ(this.levelArrow.transform, 180)
    else
        Extensions.SetLocalRotationZ(this.levelArrow.transform, 0)
    end
    this.idArrow.enabled = false
    this.numArrow.enabled = false
    CGuildEnemyMgr.Inst:SortGuildListByLevel(this.levelArrow.transform.localRotation.z == 0)
    this.listView:Init()
    this:OnNoGuildSelect()
end
CGuildEnemyWnd.m_OnSearch_CS2LuaHook = function (this, text) 

    CGuildEnemyMgr.Inst:FilterGuildInfo(text)
    this.listView:Init()
    this:OnNoGuildSelect()
end
CGuildEnemyWnd.m_OnSetEnemyButtonClick_CS2LuaHook = function (this, go) 
    if this.curGuildId > 0 then
        CGuildEnemyMgr.Inst:RequestSetOrCancelEnemyGuild(this.curGuildId)
    end
end
