local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CHTTPForm = import "L10.Game.CHTTPForm"
local ClientHttp = import "L10.Game.ClientHttp"
local JingLingFromDefine = import "L10.Game.JingLingFromDefine"
local HTTPHelper = import "L10.Game.HTTPHelper"
local Json = import "L10.Game.Utils.Json"
local String = import "System.String"
local Object = import "System.Object"
local CJingLingMgr = import "L10.Game.CJingLingMgr"
local CUIWebTexture = import "L10.UI.CUIWebTexture"
local UILabel = import "UILabel"
local CWebBrowserMgr = import "L10.Game.CWebBrowserMgr"
local CChatLinkActions = import "CChatLinkActions"
local CChatLinkMgr = import "CChatLinkMgr"
local QnTableView = import "L10.UI.QnTableView"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local UISprite = import "UISprite"

LuaJingLingNewHotspotView = class()
-- image video
RegistClassMember(LuaJingLingNewHotspotView, "m_ImageWebTexTbl")
RegistClassMember(LuaJingLingNewHotspotView, "m_ImageLabelTbl")
RegistClassMember(LuaJingLingNewHotspotView, "m_ImageVideoFlagTbl")

-- toolbar 
RegistClassMember(LuaJingLingNewHotspotView, "m_ToolbarWebTexTbl")

-- recommend
RegistClassMember(LuaJingLingNewHotspotView, "m_RecommendLabelTbl")

-- banner
RegistClassMember(LuaJingLingNewHotspotView, "m_BannerWebTex")
RegistClassMember(LuaJingLingNewHotspotView, "m_BannerLabel")
RegistClassMember(LuaJingLingNewHotspotView, "m_BannerIndicatorGridView")

-- data    (cover封面，title标题，type视频为0， video视频地址，keywordType非视频类型（EXTERNAL_URL外链， QUESTION_KEYWORD提问，CONTENT_DETAIL打开图片）, keyword相应值)
RegistClassMember(LuaJingLingNewHotspotView, "m_BannerDataTbl")
RegistClassMember(LuaJingLingNewHotspotView, "m_ToolbarDataTbl")
RegistClassMember(LuaJingLingNewHotspotView, "m_RecommendDataTbl")
RegistClassMember(LuaJingLingNewHotspotView, "m_ImageDataTbl")

-- banner id
RegistClassMember(LuaJingLingNewHotspotView, "m_BannerIndex")
RegistClassMember(LuaJingLingNewHotspotView, "m_BannerTick")
RegistClassMember(LuaJingLingNewHotspotView, "m_BannerIndicatorTbl")  -- banner indicator table(UISprite)
RegistClassMember(LuaJingLingNewHotspotView, "m_BannerDragDelta")

-- 记录当前拿token的原因
RegistClassMember(LuaJingLingNewHotspotView, "m_RefreshRecommend")
RegistClassMember(LuaJingLingNewHotspotView, "m_LogClick")

-- 位置标识
-- 1(banner)  2(toolbar)
-- 4(image)   3(recommend)
RegistClassMember(LuaJingLingNewHotspotView, "m_PositionValue")
RegistClassMember(LuaJingLingNewHotspotView, "m_MethodValue") -- 点击记录

function LuaJingLingNewHotspotView:Awake()
    self.m_ImageWebTexTbl = {}
    self.m_ImageLabelTbl = {}
    self.m_ToolbarWebTexTbl = {}
    self.m_RecommendLabelTbl = {}
    self.m_ImageVideoFlagTbl = {}
    self.m_BannerIndex = 0
    self.m_PositionValue = {"BANNER", "TOOLBAR", "RECOMMEND_FOR_YOU", "hotnews"}
    self.m_MethodValue = {"hp_banner_", "hp_toolbar_", "hp_recommend_", "hp_hotnews_"}
    
    -- init banner
    local rootTrans = self.transform:Find("Offset")
    local bannerTrans = rootTrans:Find("Banner")
    self.m_BannerWebTex = bannerTrans:Find("Tex"):GetComponent(typeof(CUIWebTexture))
    self.m_BannerLabel = bannerTrans:Find("Label"):GetComponent(typeof(UILabel))
    CommonDefs.AddOnClickListener(self.m_BannerWebTex.gameObject, DelegateFactory.Action_GameObject(function(go)
        if self.m_BannerDataTbl then
            self:OnContentClick(go, self.m_BannerDataTbl[self.m_BannerIndex], 1, self.m_BannerIndex)
        end
    end), false)
    
    CommonDefs.AddOnDragListener(self.m_BannerWebTex.gameObject, DelegateFactory.Action_GameObject_Vector2(function(go, delta)
        self.m_BannerDragDelta = self.m_BannerDragDelta + delta.x
    end), false)
    UIEventListener.Get(self.m_BannerWebTex.gameObject).onDragEnd = DelegateFactory.VoidDelegate(function(go)
        if self.m_BannerDragDelta < 0 then
            self:GotoNextBanner()
        else
            self:GotoLastBanner()
        end
    end)
    UIEventListener.Get(self.m_BannerWebTex.gameObject).onDragStart = DelegateFactory.VoidDelegate(function(go)
        self.m_BannerDragDelta = 0
    end)

    self:RefreshBannerTick()
    CommonDefs.AddOnClickListener(bannerTrans:Find("NextArrow").gameObject, DelegateFactory.Action_GameObject(function(go)
        self:GotoNextBanner()
    end), false)
    CommonDefs.AddOnClickListener(bannerTrans:Find("LastArrow").gameObject, DelegateFactory.Action_GameObject(function(go)
        self:GotoLastBanner()
    end), false)
    self.m_BannerIndicatorGridView = bannerTrans:Find("Indicators/Grid"):GetComponent(typeof(QnTableView))

    -- init toolbar
    local toolbarTrans = rootTrans:Find("Toolbar")
    for i = 1, 4 do
        self.m_ToolbarWebTexTbl[i] = toolbarTrans:Find("Tex" .. i):GetComponent(typeof(CUIWebTexture))
        CommonDefs.AddOnClickListener(self.m_ToolbarWebTexTbl[i].gameObject, DelegateFactory.Action_GameObject(function(go)
            self:OnContentClick(go, self.m_ToolbarDataTbl[i], 2, i)
        end), false)
    end
    
    -- init image video
    local imageTrans = rootTrans:Find("Image")
    for i = 1, 4 do
        self.m_ImageWebTexTbl[i] = imageTrans:Find("Tex" .. i):GetComponent(typeof(CUIWebTexture))
        self.m_ImageLabelTbl[i] = self.m_ImageWebTexTbl[i].transform:Find("Label"):GetComponent(typeof(UILabel))
        self.m_ImageVideoFlagTbl[i] = self.m_ImageWebTexTbl[i].transform:Find("VideoPlay").gameObject
        CommonDefs.AddOnClickListener(self.m_ImageWebTexTbl[i].gameObject, DelegateFactory.Action_GameObject(function(go)
            self:OnContentClick(go, self.m_ImageDataTbl[i], 4, i)
        end), false)
    end
    
    -- init recommend
    local recommendTrans = rootTrans:Find("Recommend")
    for i = 1, 6 do
        self.m_RecommendLabelTbl[i] = recommendTrans:Find("Grid/LinkLabel" .. i):GetComponent(typeof(UILabel))
        CommonDefs.AddOnClickListener(self.m_RecommendLabelTbl[i].gameObject, DelegateFactory.Action_GameObject(function(go)
            self:OnContentClick(go, self.m_RecommendDataTbl[i], 3, i)
        end), false)
    end
    CommonDefs.AddOnClickListener(recommendTrans:Find("Refresh/RefreshBtn").gameObject, DelegateFactory.Action_GameObject(function(go) self:OnRefreshRecommend(go) end), false)
end

function LuaJingLingNewHotspotView:OnDestroy()
    if self.m_BannerTick then
        UnRegisterTick(self.m_BannerTick)
    end
end

function LuaJingLingNewHotspotView:GotoNextBanner()
    self:NextBannerIndex()
    self:ShowBanner()
    self:RefreshBannerTick()
end

function LuaJingLingNewHotspotView:GotoLastBanner()
    self:LastBannerIndex()
    self:ShowBanner()
    self:RefreshBannerTick()
end

function LuaJingLingNewHotspotView:RefreshBannerTick()
    if self.m_BannerTick then
        UnRegisterTick(self.m_BannerTick)
    end
    -- Tick 3s换图
    self.m_BannerTick = RegisterTick(function()
        self:GotoNextBanner()
    end, 3000)
end

function LuaJingLingNewHotspotView:LastBannerIndex()
    if self.m_BannerDataTbl then
        local cnt = #self.m_BannerDataTbl
        if cnt > 0 then
            self.m_BannerIndex = self.m_BannerIndex - 1
            if self.m_BannerIndex <= 0 then
                self.m_BannerIndex = cnt
            end
        end
    end
end

function LuaJingLingNewHotspotView:NextBannerIndex()
    if self.m_BannerDataTbl then
        local cnt = #self.m_BannerDataTbl
        if cnt > 0 then
            self.m_BannerIndex = self.m_BannerIndex + 1
            if self.m_BannerIndex > cnt then
                self.m_BannerIndex = 1
            end
        end
    end
end

function LuaJingLingNewHotspotView:ShowBanner()
    if self.m_BannerIndex > 0 and self.m_BannerDataTbl and self.m_BannerIndex <= #self.m_BannerDataTbl then
        self.m_BannerWebTex:Init(self.m_BannerDataTbl[self.m_BannerIndex].cover)
        self.m_BannerLabel.text = CChatLinkMgr.TranslateToNGUIText(self.m_BannerDataTbl[self.m_BannerIndex].title)
        self:RefreshBannerIndicators()
    end
end

function LuaJingLingNewHotspotView:InitBannerIndicators()
    local len = #self.m_BannerDataTbl
    self.m_BannerIndicatorTbl = {}
    self.m_BannerIndicatorGridView.m_DataSource = DefaultTableViewDataSource.CreateByCount(len, function(item, index)
        self.m_BannerIndicatorTbl[index + 1] = item.gameObject:GetComponent(typeof(UISprite))
    end)
    self.m_BannerIndicatorGridView:Clear()
    self.m_BannerIndicatorGridView:ReloadData(true, true)
end

function LuaJingLingNewHotspotView:RefreshBannerIndicators()
    if not self.m_BannerIndicatorTbl then return end
    for k, v in pairs(self.m_BannerIndicatorTbl) do
        v.color = k == self.m_BannerIndex and Color.white or Color.gray
    end
end

function LuaJingLingNewHotspotView:OnContentClick(go, webData, pos, index)
    -- 视频
    if webData.type == 0 then
        LuaJingLingMgr.PlayRemoteVideo(webData.video)
    else
        if webData.keywordType == "EXTERNAL_URL" then
            CWebBrowserMgr.Inst:OpenUrl(webData.keyword)
        elseif webData.keywordType == "QUESTION_KEYWORD" then
            CChatLinkActions.QueryJingLing(webData.keyword)
        elseif webData.keywordType == "CONTENT_DETAIL" then
            CChatLinkActions.ShowWebImage(webData.keyword)
        end
    end
    self:LogClick(pos, index, webData.contentId)
end

function LuaJingLingNewHotspotView:OnRefreshRecommend(go)
    if not LuaJingLingMgr:GameYWTokenExpired() then
        self:QueryDetailByPosition(LuaJingLingMgr.m_GameYWToken, LuaJingLingMgr.m_GameYWClientUrl, "FRONT_PAGE", "RECOMMEND_FOR_YOU", "false")
    else
        self.m_RefreshRecommend = true
        LuaJingLingMgr:QuerySpriteGameYWToken()
    end
end


function LuaJingLingNewHotspotView:OnEnable()
    -- 这个界面是在JingLingWnd下的，主界面打开时会请求Token
    g_ScriptEvent:AddListener("QuerySpriteGameYWTokenResult", self, "OnQuerySpriteGameYWTokenResult")

    if not LuaJingLingMgr:GameYWTokenExpired() then
        self:OnQuerySpriteGameYWTokenResult(LuaJingLingMgr.m_GameYWToken, LuaJingLingMgr.m_GameYWTokenExpiredTime, nil, LuaJingLingMgr.m_GameYWClientUrl)
    end
end

function LuaJingLingNewHotspotView:OnDisable()
    g_ScriptEvent:RemoveListener("QuerySpriteGameYWTokenResult", self, "OnQuerySpriteGameYWTokenResult")
end

function LuaJingLingNewHotspotView:OnQuerySpriteGameYWTokenResult(token, expiredTime, context, clientUrl)
    -- 刷新推荐
    if self.m_RefreshRecommend then
        self:QueryDetailByPosition(token, clientUrl, "FRONT_PAGE", "RECOMMEND_FOR_YOU", "false")
        self.m_RefreshRecommend = false
        return
    end
    
    -- 记录点击拿的token就不实时反馈了
    if self.m_LogClick then
        self.m_LogClick = false
        return
    end
    
    local posValue = {"BANNER", "TOOLBAR", "RECOMMEND_FOR_YOU", "hotnews"}
    for _, v in ipairs(self.m_PositionValue) do
        self:QueryDetailByPosition(token, clientUrl, "FRONT_PAGE", v, "true")
    end
end

-- 记录点击事件
function LuaJingLingNewHotspotView:LogClick(pos, index, contentId)
    if not LuaJingLingMgr:GameYWTokenExpired() then
        self:QueryPosition(LuaJingLingMgr.m_GameYWToken, LuaJingLingMgr.m_GameYWClientUrl, pos, index, contentId)
    else
        self.m_LogClick = true
        LuaJingLingMgr:QuerySpriteGameYWToken()
    end
end

function LuaJingLingNewHotspotView:QueryPosition(token, clientUrl, pos, index, contentId)
    local method = self.m_MethodValue[pos] .. tostring(index)
    --local positionValue = self.m_PositionValue[pos]
    
    local url = clientUrl .. "/sprite/api/l10/content/get"
    local form = CreateFromClass(CHTTPForm)
    form:AddField("loginFrom", CJingLingMgr.Inst.ExistsUnreadMsgs and JingLingFromDefine.Push or JingLingFromDefine.Sprite)
    form:AddField("method", method)
    form:AddField("contentId", contentId)
    --form:AddField("value", positionValue)
    form.m_UseJsonPost = true
    local dic = CreateFromClass(MakeGenericClass(Dictionary, String, String))
    CommonDefs.DictAdd_LuaCall(dic, "token", token)
    CommonDefs.DictAdd_LuaCall(dic, "token-type", "sprite")
    local request = ClientHttp(url, form, dic)
    HTTPHelper.GetResponse(request, nil, false)
end

function LuaJingLingNewHotspotView:QueryDetailByPosition(token, clientUrl, position, positionValue, top)
    local url = clientUrl .. "/sprite/api/l10/content/getByPosition"
    local form = CreateFromClass(CHTTPForm)
    form:AddField("loginFrom", CJingLingMgr.Inst.ExistsUnreadMsgs and JingLingFromDefine.Push or JingLingFromDefine.Sprite)
    form:AddField("method", "first")
    form:AddField("lastIndex", 0)
    -- TODO:暂时给个
    form:AddField("pageSize", 10)
    form:AddField("position", position)
    form:AddField("positionValue", positionValue)
    form:AddField("top", top)
    form.m_UseJsonPost = true
    local dic = CreateFromClass(MakeGenericClass(Dictionary, String, String))
    CommonDefs.DictAdd_LuaCall(dic, "token", token)
    CommonDefs.DictAdd_LuaCall(dic, "token-type", "sprite")
    local request = ClientHttp(url, form, dic)
    HTTPHelper.GetResponse(request, DelegateFactory.Action_bool_string(function(ret, text)
        if ret then
            local dict = TypeAs(Json.Deserialize(text), typeof(MakeGenericClass(Dictionary, String, Object)))
            if dict then
                local data = CommonDefs.DictGetValue(dict, typeof(String), "data")
                if data then
                    self:RefreshDetail(data, positionValue)
                end
            end
        end
    end), false)
end

function LuaJingLingNewHotspotView:RefreshDetail(data, positionValue)
    local contentList = CommonDefs.DictGetValue(data, typeof(String), "contentList")
    if not contentList then
        return
    end

    local cnt = contentList.Count
    if positionValue == self.m_PositionValue[1] then
        self.m_BannerDataTbl = {}
        for i = 1, cnt do
            local content = contentList[i - 1]
            self.m_BannerDataTbl[i] = self:GetWebData(content)
        end
        self:InitBannerIndicators()
        self.m_BannerIndex = 1
        self:ShowBanner()
    elseif positionValue == self.m_PositionValue[2] then
        self.m_ToolbarDataTbl = {}
        for  i = 1, 4 do
            if i > cnt then
                self.m_ToolbarWebTexTbl[i]:Init(nil)
            else
                local content = contentList[i - 1]
                self.m_ToolbarDataTbl[i] = self:GetWebData(content)
                self.m_ToolbarWebTexTbl[i]:Init(self.m_ToolbarDataTbl[i].cover)
            end
        end
    elseif positionValue == self.m_PositionValue[3] then
        self.m_RecommendDataTbl = {}
        for i = 1, 6 do
            if i > cnt then
                self.m_RecommendLabelTbl[i].text = nil
            else
                local content = contentList[i - 1]
                self.m_RecommendDataTbl[i] = self:GetWebData(content)
                self.m_RecommendLabelTbl[i].text = CChatLinkMgr.TranslateToNGUIText(self.m_RecommendDataTbl[i].title)
            end
        end
    elseif positionValue == self.m_PositionValue[4] then
        self.m_ImageDataTbl = {}
        for i = 1, 4 do
            if i > cnt then
                self.m_ImageWebTexTbl[i]:Init(nil)
                self.m_ImageLabelTbl[i].text = nil
                self.m_ImageVideoFlagTbl[i]:SetActive(false)
            else
                local content = contentList[i - 1]
                self.m_ImageDataTbl[i] = self:GetWebData(content)
                self.m_ImageWebTexTbl[i]:Init(self.m_ImageDataTbl[i].cover)
                self.m_ImageLabelTbl[i].text = CChatLinkMgr.TranslateToNGUIText(self.m_ImageDataTbl[i].title)
                self.m_ImageVideoFlagTbl[i]:SetActive(self.m_ImageDataTbl[i].type == 0)
            end
        end
    end
end

function LuaJingLingNewHotspotView:GetWebData(content)
    return {cover = self:GetContentValue(content, "cover"),
            title = self:GetContentValue(content, "title"),
            video = self:GetContentValue(content, "video"),
            type = self:GetContentValue(content, "type"),
            keyword = self:GetContentValue(content, "keyword"),
            keywordType = self:GetContentValue(content, "keywordType"),
            contentId = self:GetContentValue(content, "id")}
end

function LuaJingLingNewHotspotView:GetContentValue(content, key)
    local data = CommonDefs.DictGetValue(content, typeof(String), "data")
    if data then
        return CommonDefs.DictGetValue(data, typeof(String), key)
    end
end