


local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"

LuaOffWorldPlay2Game2Wnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaOffWorldPlay2Game2Wnd, "CloseBtn", "CloseBtn", GameObject)
RegistChildComponent(LuaOffWorldPlay2Game2Wnd, "PosNodeFather", "PosNodeFather", GameObject)
RegistChildComponent(LuaOffWorldPlay2Game2Wnd, "fill", "fill", GameObject)
RegistChildComponent(LuaOffWorldPlay2Game2Wnd, "empty", "empty", GameObject)
RegistChildComponent(LuaOffWorldPlay2Game2Wnd, "TipTextNode", "TipTextNode", UILabel)
RegistChildComponent(LuaOffWorldPlay2Game2Wnd, "fxNode", "fxNode", GameObject)

--@endregion RegistChildComponent end

function LuaOffWorldPlay2Game2Wnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

RegistClassMember(LuaOffWorldPlay2Game2Wnd,"correctNum")
RegistClassMember(LuaOffWorldPlay2Game2Wnd,"clickNodeTable")
RegistClassMember(LuaOffWorldPlay2Game2Wnd,"emptyNodeTable")
RegistClassMember(LuaOffWorldPlay2Game2Wnd,"fillNodeTable")
RegistClassMember(LuaOffWorldPlay2Game2Wnd,"m_Tick")

function LuaOffWorldPlay2Game2Wnd:Init()
	local onCloseClick = function(go)
    MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("CUSTOM_STRING2",LocalString.GetString("确认退出吗?")), DelegateFactory.Action(function ()
      CUIManager.CloseUI(CLuaUIResources.OffWorldPlay2Game2Wnd)
    end), nil, nil, nil, false)
	end
	CommonDefs.AddOnClickListener(self.CloseBtn,DelegateFactory.Action_GameObject(onCloseClick),false)

  self:UpdateLabel()
  self:InitClickNode()

end

--@region UIEvent

--@endregion UIEvent

function LuaOffWorldPlay2Game2Wnd:UpdateLabel()
  if not self.correctNum then
    self.correctNum = 0
  end
  self.TipTextNode.text = SafeStringFormat3(LocalString.GetString('按顺序点击散落的经文 %s/%s'),self.correctNum,8)
end

function LuaOffWorldPlay2Game2Wnd:InitClickNode()
  self.clickNodeTable = {
    self.PosNodeFather.transform:Find('1').gameObject,
    self.PosNodeFather.transform:Find('2').gameObject,
    self.PosNodeFather.transform:Find('3').gameObject,
    self.PosNodeFather.transform:Find('4').gameObject,
    self.PosNodeFather.transform:Find('5').gameObject,
    self.PosNodeFather.transform:Find('6').gameObject,
    self.PosNodeFather.transform:Find('7').gameObject,
    self.PosNodeFather.transform:Find('8').gameObject,
  }
  self.emptyNodeTable = {
    self.empty.transform:Find('Circle01').gameObject,
    self.empty.transform:Find('Circle02').gameObject,
    self.empty.transform:Find('Circle03').gameObject,
    self.empty.transform:Find('Circle04').gameObject,
    self.empty.transform:Find('Circle05').gameObject,
    self.empty.transform:Find('Circle06').gameObject,
    self.empty.transform:Find('Circle07').gameObject,
    self.empty.transform:Find('Circle08').gameObject,
  }
  self.fillNodeTable = {
    self.fill.transform:Find('Circle01').gameObject,
    self.fill.transform:Find('Circle02').gameObject,
    self.fill.transform:Find('Circle03').gameObject,
    self.fill.transform:Find('Circle04').gameObject,
    self.fill.transform:Find('Circle05').gameObject,
    self.fill.transform:Find('Circle06').gameObject,
    self.fill.transform:Find('Circle07').gameObject,
    self.fill.transform:Find('Circle08').gameObject,
  }

  for i,v in ipairs(self.emptyNodeTable) do
    v:SetActive(true)
  end
  for i,v in ipairs(self.fillNodeTable) do
    v:SetActive(false)
  end

  for i,v in ipairs(self.clickNodeTable) do
    local onClick = function(go)
      if not self.correctNum then
        self.correctNum = 0
      end
      if self.correctNum == i - 1 then
        self.correctNum = self.correctNum + 1
        self:DoCorrectChoose(i)
			else
				g_MessageMgr:ShowMessage('CaiDie_WrongWord_Click')
      end
    end
    CommonDefs.AddOnClickListener(v,DelegateFactory.Action_GameObject(onClick),false)

    v.transform.localEulerAngles = Vector3(0,0,math.random(-5, 5))
    local tweenposition = v:GetComponent(typeof(TweenPosition))
    tweenposition.to = Vector3(tweenposition.from.x + math.random(-10, 10),tweenposition.from.y + math.random(-10, 10),tweenposition.from.z)
  end
end

function LuaOffWorldPlay2Game2Wnd:DoCorrectChoose(index)
  self:UpdateLabel()
  self.emptyNodeTable[index]:SetActive(false)
  self.fillNodeTable[index]:SetActive(true)
  self.clickNodeTable[index]:SetActive(false)
  if index == #self.clickNodeTable then
    self:DoSucc()
  end
end

function LuaOffWorldPlay2Game2Wnd:DoSucc()
	local fxTime = 2000
	if self.m_Tick then
    UnRegisterTick(self.m_Tick)
	end

	self.m_Tick = RegisterTickWithDuration(function ()
		Gac2Gas.CaiDieTaskFinish(4)
    CUIManager.CloseUI(CLuaUIResources.OffWorldPlay2Game2Wnd)
	end,fxTime,fxTime)

	self.fxNode:GetComponent(typeof(CUIFx)):LoadFx('fx/ui/prefab/UI_caidiezhen_chaoduwancheng.prefab')
end

function LuaOffWorldPlay2Game2Wnd:OnDestroy()
	if self.m_Tick then
    UnRegisterTick(self.m_Tick)
	end
  self.m_Tick = nil
end
