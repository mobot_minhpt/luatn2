local UISprite = import "UISprite"

local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CItemMgr = import "L10.Game.CItemMgr"
local QnCheckBox = import "L10.UI.QnCheckBox"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local Object = import "System.Object"
local UIEventListener = import "UIEventListener"
local UInt64 = import "System.UInt64"
local String = import "System.String"

LuaQuweiLabaWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaQuweiLabaWnd, "QnCheckBox", "QnCheckBox", QnCheckBox)
RegistChildComponent(LuaQuweiLabaWnd, "LingYuCount", "LingYuCount", UILabel)
RegistChildComponent(LuaQuweiLabaWnd, "TimeProgress", "TimeProgress", UISprite)
RegistChildComponent(LuaQuweiLabaWnd, "LianJiBtn", "LianJiBtn", GameObject)
RegistChildComponent(LuaQuweiLabaWnd, "LaBaCountLabel", "LaBaCountLabel", UILabel)
RegistChildComponent(LuaQuweiLabaWnd, "CloseBtn", "CloseBtn", GameObject)
RegistChildComponent(LuaQuweiLabaWnd, "QnCostAndOwnMoney", "QnCostAndOwnMoney", CCurentMoneyCtrl)
RegistChildComponent(LuaQuweiLabaWnd, "Offset", "Offset", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaQuweiLabaWnd,"m_TimeLimit")
RegistClassMember(LuaQuweiLabaWnd,"m_CurTime")
RegistClassMember(LuaQuweiLabaWnd,"m_TimeThreshold")
RegistClassMember(LuaQuweiLabaWnd,"m_CountThreshold")
RegistClassMember(LuaQuweiLabaWnd,"m_LaBaItemId")
RegistClassMember(LuaQuweiLabaWnd,"m_LaBaCost")
-- 倒计数tick
RegistClassMember(LuaQuweiLabaWnd,"m_TimeTick")
-- 是否使用灵玉
RegistClassMember(LuaQuweiLabaWnd,"m_IsLingYuUse")
-- 喇叭数量
RegistClassMember(LuaQuweiLabaWnd,"m_LaBaCount")
-- 灵玉数量
RegistClassMember(LuaQuweiLabaWnd,"m_LingYuCount")
-- 缓存发送tick
RegistClassMember(LuaQuweiLabaWnd,"m_CacheSendTick")
RegistClassMember(LuaQuweiLabaWnd,"m_PlayerData")
RegistClassMember(LuaQuweiLabaWnd,"m_CanSendRpc")

function LuaQuweiLabaWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.LianJiBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLianJiBtnClick()
	end)
	
	UIEventListener.Get(self.CloseBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCloseBtnClick()
	end)


    --@endregion EventBind end
end

function LuaQuweiLabaWnd:DesignData()
    self.m_TimeLimit = LaBa_Setting.GetData().LaBaCountdownTime * 1000
    self.m_TimeThreshold = LaBa_Setting.GetData().LaBaTimeThreshold
    self.m_CountThreshold = LaBa_Setting.GetData().LaBaCountThreshold
    self.m_LaBaItemId = LaBa_Setting.GetData().LaBaItemID
    self.m_LaBaCost = Mall_LingYuMall.GetData(self.m_LaBaItemId).Jade
end

function LuaQuweiLabaWnd:Init()
    self:DesignData()
    -- 倒计时
    self.TimeProgress.fillAmount = 0

    self.m_CurTime = 0
    local interval = 30

    if self.m_TimeTick then
        invoke(self.m_TimeTick)
        self.m_TimeTick = nil
    end

    if self.m_TimeTick == nil then
        self.m_TimeTick = RegisterTick(function()
            if self.m_CurTime == nil or self.m_CurTime >= self.m_TimeLimit then
                self.TimeProgress.fillAmount = 1
                self:OnClose()
            else
                self.TimeProgress.fillAmount = self.m_CurTime / self.m_TimeLimit
                self.m_CurTime = self.m_CurTime + interval
            end
        end, interval)
    end

    -- 物品数量
    self.m_LaBaCount = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, self.m_LaBaItemId)
    self.LaBaCountLabel.text = tostring(self.m_LaBaCount)
    self.m_CacheSendTick = nil

    -- 灵玉数量
    self.QnCostAndOwnMoney:SetType(EnumMoneyType.LingYu, EnumPlayScoreKey.NONE, true)

    -- 灵玉模式
    self.QnCheckBox.OnValueChanged = DelegateFactory.Action_bool(
        function(select)
            self.m_IsLingYuUse = select
        end
    )
    self.QnCheckBox.Selected = true
    self.m_IsLingYuUse = true

    self.LingYuCount.text = self.m_LaBaCost

    -- 玩家数据
    local info = CreateFromClass(MakeGenericClass(List, Object))
    CommonDefs.ListAdd(info, typeof(UInt64), LuaQuweiLabaMgr.m_PlayerId)
    CommonDefs.ListAdd(info, typeof(String), LuaQuweiLabaMgr.m_PlayerName)

    self.m_PlayerData = MsgPackImpl.pack(info)
    self.m_CanSendRpc = false
    -- 第一次发送信息
    Gac2Gas.UseFuXiLaba(self.m_LaBaItemId, self.m_PlayerData, LuaQuweiLabaMgr.m_Type, 1, false)
end

function LuaQuweiLabaWnd:OnClose()
    if self.m_TimeTick ~= nil then
        invoke(self.m_TimeTick)
        self.m_TimeTick = nil
    end
    CUIManager.CloseUI(CLuaUIResources.QuweiLabaWnd)
end

function LuaQuweiLabaWnd:CheckLingYuCondition()
    if self.m_IsLingYuUse then
        return self.QnCostAndOwnMoney.own >= self.m_LaBaCost
    end
    return false
end

function LuaQuweiLabaWnd:CheckItemCondition()
    local realCount = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, self.m_LaBaItemId)
    return realCount > 0
end

function LuaQuweiLabaWnd:UpdateAndGetItemCount()
    local realCount = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, self.m_LaBaItemId)
    self.LaBaCountLabel.text = tostring(realCount)
end

function LuaQuweiLabaWnd:UseFuXiLabaSuccess(combo)
    self.m_CanSendRpc = true
    if combo == 0 then
        self:OnClose()
    end
end

function LuaQuweiLabaWnd:OnDisable()
    if self.m_TimeTick ~= nil then
        invoke(self.m_TimeTick)
        self.m_TimeTick = nil
    end
	g_ScriptEvent:RemoveListener("PlayerUseFuXiLabaSuccess", self, "UseFuXiLabaSuccess")
    g_ScriptEvent:RemoveListener("SendItem", self, "UpdateAndGetItemCount")
    g_ScriptEvent:RemoveListener("SetItemAt", self, "UpdateAndGetItemCount")
end

function LuaQuweiLabaWnd:OnEnable()
    g_ScriptEvent:AddListener("PlayerUseFuXiLabaSuccess", self, "UseFuXiLabaSuccess")
    g_ScriptEvent:AddListener("SendItem", self, "UpdateAndGetItemCount")
    g_ScriptEvent:AddListener("SetItemAt", self, "UpdateAndGetItemCount")
end

--@region UIEvent

function LuaQuweiLabaWnd:OnLianJiBtnClick()
    self.m_CurTime = 0

    -- 无可用喇叭
    if (not self:CheckItemCondition()) and (not self:CheckLingYuCondition()) then
        if self.m_IsLingYuUse then
            g_MessageMgr:ShowMessage("FUXILABA_FAILED_JADE_NOT_ENOUGH")
        else
            g_MessageMgr:ShowMessage("FUXILABA_FAILED_ITEM_NOT_ENOUGH")
        end
    elseif self.m_CanSendRpc then
        self.m_CanSendRpc = false
        local useLingYu = (not self:CheckItemCondition()) and self:CheckLingYuCondition()
        Gac2Gas.UseFuXiLaba(self.m_LaBaItemId, self.m_PlayerData, LuaQuweiLabaMgr.m_Type, 1, useLingYu)
    else
        g_MessageMgr:ShowMessage("FUXILABA_WAITING_RESPONSE_RPC")
    end
end

function LuaQuweiLabaWnd:OnCloseBtnClick()
    self:OnClose()
end

--@endregion UIEvent
