LuaSpokesmanDialogWnd = class()

RegistChildComponent(LuaSpokesmanDialogWnd, "m_TextLabel","TextLabel",UILabel)
RegistChildComponent(LuaSpokesmanDialogWnd, "m_NameLabel","NameLabel",UILabel)
RegistChildComponent(LuaSpokesmanDialogWnd, "m_SeeContractButton","SeeContractButton",GameObject)
RegistChildComponent(LuaSpokesmanDialogWnd, "m_WaitButton","WaitButton",GameObject)
RegistChildComponent(LuaSpokesmanDialogWnd, "m_SignContractButton","SignContractButton",GameObject)

function LuaSpokesmanDialogWnd:Init()
    self.m_TextLabel.text = g_MessageMgr:FormatMessage(CLuaSpokesmanMgr.m_SpokesmanDialogWndMessage)
    self.m_SignContractButton:SetActive(CLuaSpokesmanMgr.m_SpokesmanDialogWndState == 0)
    self.m_WaitButton:SetActive(CLuaSpokesmanMgr.m_SpokesmanDialogWndState == 0)
    self.m_SeeContractButton:SetActive(CLuaSpokesmanMgr.m_SpokesmanDialogWndState == 1)
    self.m_NameLabel.text = Spokesman_Setting.GetData().TaskScheduleTabName
    UIEventListener.Get(self.m_WaitButton).onClick = DelegateFactory.VoidDelegate(function (p)
        CUIManager.CloseUI(CLuaUIResources.SpokesmanDialogWnd)
    end)
    UIEventListener.Get(self.m_SignContractButton).onClick = DelegateFactory.VoidDelegate(function (p)
        CUIManager.CloseUI(CLuaUIResources.SpokesmanDialogWnd)
        Gac2Gas.RequestSignSpokesmanContract()
    end)
    UIEventListener.Get(self.m_SeeContractButton).onClick = DelegateFactory.VoidDelegate(function (p)
        CUIManager.CloseUI(CLuaUIResources.SpokesmanDialogWnd)
        CUIManager.ShowUI(CLuaUIResources.SpokesmanHireContractWnd)
    end)
end
