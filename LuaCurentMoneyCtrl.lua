local QnButton = import "L10.UI.QnButton"
local CServerTimeMgr=import "L10.Game.CServerTimeMgr"
local Money = import "L10.Game.Money"
local MessageWndManager = import "L10.UI.MessageWndManager"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local DelegateFactory = import "DelegateFactory"
local CUIResources = import "L10.UI.CUIResources"
local CUIManager = import "L10.UI.CUIManager"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local Constants = import "L10.Game.Constants"
local CommonDefs = import "L10.Game.CommonDefs"
local CFeiShengMgr = import "L10.Game.CFeiShengMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CCityWarMgr = import "L10.Game.CCityWarMgr"
local BabyMgr = import "L10.Game.BabyMgr"
local CChargeWnd = import "L10.UI.CChargeWnd"
local CGetMoneyMgr=import "L10.UI.CGetMoneyMgr"

CLuaCurentMoneyCtrl = class()
RegistClassMember(CLuaCurentMoneyCtrl,"m_Type")
RegistClassMember(CLuaCurentMoneyCtrl,"m_ScoreType")
RegistClassMember(CLuaCurentMoneyCtrl,"m_bForceChangeYinpiao2Yinliang")

RegistChildComponent(CLuaCurentMoneyCtrl,"m_OwnLabel","Own",UILabel)
RegistChildComponent(CLuaCurentMoneyCtrl,"m_CostLabel","Cost",UILabel)
RegistChildComponent(CLuaCurentMoneyCtrl,"m_AddMoneyButton","AddBtn",QnButton)
RegistChildComponent(CLuaCurentMoneyCtrl,"m_BindLingYuSprite","BindLingYu",UISprite)
RegistChildComponent(CLuaCurentMoneyCtrl,"m_OwnIcon","OwnIcon",UISprite)
RegistChildComponent(CLuaCurentMoneyCtrl,"m_CostIcon","CostIcon",UISprite)

RegistClassMember(CLuaCurentMoneyCtrl,"m_AutoSetType")
RegistClassMember(CLuaCurentMoneyCtrl,"updateAction")
RegistClassMember(CLuaCurentMoneyCtrl,"cost")
RegistClassMember(CLuaCurentMoneyCtrl,"own")
RegistClassMember(CLuaCurentMoneyCtrl,"m_IsTempPlayScore")
RegistClassMember(CLuaCurentMoneyCtrl,"m_TempPlayScoreType")

function CLuaCurentMoneyCtrl:Awake()
    self.m_Type = EnumMoneyType.Undefined
    self.m_ScoreType = EnumPlayScoreKey.NONE
    self.m_bForceChangeYinpiao2Yinliang = true
    -- self.m_OwnLabel = self.transform:Find("Own"):GetComponent(typeof(UILabel))
    -- self.m_CostLabel = self.transform:Find("Cost"):GetComponent(typeof(UILabel))
    -- self.m_AddMoneyButton = self.transform:Find("Own/AddBtn"):GetComponent(typeof(L10.UI.QnButton))
    -- self.m_BindLingYuSprite = self.transform:Find(""):GetComponent(typeof(UISprite))
    -- self.m_OwnIcon = self.transform:Find("Own/Sprite"):GetComponent(typeof(UISprite))
    -- self.m_CostIcon = self.transform:Find("Cost/Sprite"):GetComponent(typeof(UISprite))
    self.m_AutoSetType = false
    self.updateAction = nil
    self.cost = 0
    self.own = 0

    if self.m_AddMoneyButton ~= nil then
        self.m_AddMoneyButton.OnClick = DelegateFactory.Action_QnButton(function(btn)
            self:AddMoney(btn)
        end)
    end
end

function CLuaCurentMoneyCtrl:UpdateTempPlayScore()
    if CClientMainPlayer.Inst then
        local info = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayScore,self.m_TempPlayScoreType)
        if info then
            self:OnSyncTempPlayScore(self.m_TempPlayScoreType,info.Score,info.ExpiredTime)
        else
            self:OnSyncTempPlayScore(self.m_TempPlayScoreType,0)
        end
    else
        self:OnSyncTempPlayScore(self.m_TempPlayScoreType,0)
    end
end
function CLuaCurentMoneyCtrl:InitTempPlayScore(tempScoreType)
    self.m_Type = EnumMoneyType.Undefined
    self.m_ScoreType = EnumPlayScoreKey.NONE

    self.m_IsTempPlayScore = true
    self.m_TempPlayScoreType = tempScoreType

    self:UpdateTempPlayScore()

    if self.m_AddMoneyButton then
        self.m_AddMoneyButton.gameObject:SetActive(false)
    end

    if self.m_OwnIcon then
        self.m_OwnIcon.spriteName = nil
    end
    if self.m_CostIcon then
        self.m_CostIcon.spriteName = nil
    end

    if self.updateAction ~= nil then
        self.updateAction()
    end
end

function CLuaCurentMoneyCtrl:SetType( type, scoreType, forceChangeYinpiao2Yinliang) 
    self.m_IsTempPlayScore = false

    self.m_Type = type
    self.m_ScoreType = scoreType
    self.m_bForceChangeYinpiao2Yinliang = forceChangeYinpiao2Yinliang
    
    if type == EnumMoneyType.Score then
        if self.m_AddMoneyButton ~= nil and scoreType ~= EnumPlayScoreKey.YuMa then
            self.m_AddMoneyButton.gameObject:SetActive(false)
        end

        self:OnMainPlayerPlayPropUpdate()
    elseif type == EnumMoneyType.GuildContri or type == EnumMoneyType.MingWang then
        self:OnMainPlayerPlayPropUpdate()
    else
        self:OnUpdateMoney()
    end
    self:UpdateIcon(type)
end

function CLuaCurentMoneyCtrl:UpdateIcon( type) 
    if self.m_OwnIcon ~= nil then
        self.m_OwnIcon.spriteName = Money.GetIconName(type, EnumPlayScoreKey.NONE)
    end
    if self.m_CostIcon ~= nil then
        self.m_CostIcon.spriteName = Money.GetIconName(type, EnumPlayScoreKey.NONE)
    end


    if self.m_BindLingYuSprite ~= nil then
        self.m_BindLingYuSprite.spriteName = Money.GetIconName(EnumMoneyType.LingYu_OnlyBind, EnumPlayScoreKey.NONE)
        local bindCount = CClientMainPlayer.Inst.BindJade
        self.m_BindLingYuSprite.gameObject:SetActive(type == EnumMoneyType.LingYu_WithBind and bindCount > 0)
    end
end
function CLuaCurentMoneyCtrl:OnEnable( )
    if self.m_AutoSetType then
        self:SetType(self.m_Type, self.m_ScoreType, self.m_bForceChangeYinpiao2Yinliang)
    else
        self:OnUpdateMoney()
        self:OnMainPlayerPlayPropUpdate()
    end
    g_ScriptEvent:AddListener("MainPlayerUpdateMoney",self,"OnUpdateMoney")
    g_ScriptEvent:AddListener("MainPlayerPlayPropUpdate",self,"OnMainPlayerPlayPropUpdate")
    g_ScriptEvent:AddListener("OnSyncYumaPlayData",self,"OnMainPlayerPlayPropUpdate")
    g_ScriptEvent:AddListener("SyncXingguanProp",self,"OnMainPlayerPlayPropUpdate")
    g_ScriptEvent:AddListener("OnSyncTianQiPlayData",self,"OnMainPlayerPlayPropUpdate")
    g_ScriptEvent:AddListener("OnSyncMingWangData",self,"OnMainPlayerPlayPropUpdate")
    g_ScriptEvent:AddListener("SyncTempPlayScore",self,"OnSyncTempPlayScore")
    g_ScriptEvent:AddListener("UpdateTempPlayDataWithKey", self, "OnUpdateTempPlayDataWithKey")

end
function CLuaCurentMoneyCtrl:OnDisable( )
    g_ScriptEvent:RemoveListener("MainPlayerUpdateMoney",self,"OnUpdateMoney")
    g_ScriptEvent:RemoveListener("MainPlayerPlayPropUpdate",self,"OnMainPlayerPlayPropUpdate")
    g_ScriptEvent:RemoveListener("OnSyncYumaPlayData",self,"OnMainPlayerPlayPropUpdate")
    g_ScriptEvent:RemoveListener("SyncXingguanProp",self,"OnMainPlayerPlayPropUpdate")
    g_ScriptEvent:RemoveListener("OnSyncTianQiPlayData",self,"OnMainPlayerPlayPropUpdate")
    g_ScriptEvent:RemoveListener("OnSyncMingWangData",self,"OnMainPlayerPlayPropUpdate")
    g_ScriptEvent:RemoveListener("SyncTempPlayScore",self,"OnSyncTempPlayScore")
    g_ScriptEvent:RemoveListener("UpdateTempPlayDataWithKey",self,"OnUpdateTempPlayDataWithKey")
end

function CLuaCurentMoneyCtrl:OnUpdateTempPlayDataWithKey()
    if self.m_IsTempPlayScore and self.m_TempPlayScoreType == EnumTempPlayScoreKey_lua["NanDuLordMoney"] then
        local lordData = {}
        if CClientMainPlayer.Inst and CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, LuaYiRenSiMgr.LordTempPlayDataKey) then
            local playData = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, LuaYiRenSiMgr.LordTempPlayDataKey)
            if playData.ExpiredTime > CServerTimeMgr.Inst.timeStamp and playData.Data and playData.Data.Data then
                local data = g_MessagePack.unpack(playData.Data.Data)
                lordData = data[1]
            end
        end
        local moneyCnt = lordData[LuaYiRenSiMgr.LordPropertyKey.moneyIndex] and lordData[LuaYiRenSiMgr.LordPropertyKey.moneyIndex] or 0
        self.own = moneyCnt
        self.m_OwnLabel.text = self:IsEnough() and tostring(self.own) or SafeStringFormat3("[c][ff0000]%d[-][/c]", self.own)
    end
end

function CLuaCurentMoneyCtrl:OnSyncTempPlayScore(key,score,expiredTime)
    --过期
    if expiredTime and expiredTime<CServerTimeMgr.Inst.timeStamp then score=0 end
    if self.m_IsTempPlayScore and self.m_TempPlayScoreType == key then
        if self.m_TempPlayScoreType == EnumTempPlayScoreKey_lua["NanDuLordMoney"] then
            local lordData = {}
            if CClientMainPlayer.Inst and CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, LuaYiRenSiMgr.LordTempPlayDataKey) then
                local playData = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, LuaYiRenSiMgr.LordTempPlayDataKey)
                if playData.ExpiredTime > CServerTimeMgr.Inst.timeStamp and playData.Data and playData.Data.Data then
                    local data = g_MessagePack.unpack(playData.Data.Data)
                    lordData = data[1]
                end
            end
            local moneyCnt = lordData[LuaYiRenSiMgr.LordPropertyKey.moneyIndex] and lordData[LuaYiRenSiMgr.LordPropertyKey.moneyIndex] or 0
            self.own = moneyCnt
            self.m_OwnLabel.text = self:IsEnough() and tostring(self.own) or SafeStringFormat3("[c][ff0000]%d[-][/c]", self.own)
        else
            self.own = score
            self.m_OwnLabel.text = self:IsEnough() and tostring(self.own) or SafeStringFormat3("[c][ff0000]%d[-][/c]", self.own) 
        end
    end
end

function CLuaCurentMoneyCtrl:AddMoney( button) 
    if self.m_Type == EnumMoneyType.LingYu or self.m_Type == EnumMoneyType.LingYu_WithBind then
        CShopMallMgr.ShowChargeWnd()
    elseif self.m_Type == EnumMoneyType.LingYu_OnlyBind then
        g_MessageMgr:ShowMessage("BINDJADEGETTIP")
    elseif self.m_Type == EnumMoneyType.YuanBao then
        CChargeWnd.OpenWndGettingYuanbao()
    elseif self.m_Type == EnumMoneyType.YinLiang or (self.m_Type == EnumMoneyType.YinPiao and self.m_bForceChangeYinpiao2Yinliang) then
        --灵玉商城购买
        if not self:IsEnough() and self.cost > 0 then
            local txt = g_MessageMgr:FormatMessage("NotEnough_Silver_QuickBuy")
            MessageWndManager.ShowOKCancelMessage(txt, DelegateFactory.Action(function () 
                CShopMallMgr.ShowLinyuShoppingMall(Constants.SilverItemId)
            end), nil, nil, nil, false)
        else
            CShopMallMgr.ShowLinyuShoppingMall(Constants.SilverItemId)
        end
    elseif (self.m_Type == EnumMoneyType.YinPiao and (not self.m_bForceChangeYinpiao2Yinliang)) then
        CShopMallMgr.ShowYuanbaoShoppingMall(21001068)
    elseif self.m_Type == EnumMoneyType.GuildContri then
        g_MessageMgr:ShowMessage("MAPI_SHOP_BANGGONG_GET")
    elseif self.m_Type == EnumMoneyType.Score and self.m_ScoreType == EnumPlayScoreKey.YuMa then
        g_MessageMgr:ShowMessage("MAPI_JIFEN_GET")
    elseif self.m_Type == EnumMoneyType.Score and self.m_ScoreType == EnumPlayScoreKey.TianQiJiFen then
        g_MessageMgr:ShowMessage("TIANQI_JIFEN_GET")
    elseif self.m_Type == EnumMoneyType.Score and self.m_ScoreType == EnumPlayScoreKey.FishingScore then
        g_MessageMgr:ShowMessage("FishingScore_JIFEN_GET")
    elseif self.m_Type == EnumMoneyType.Xingmang then
        CUIManager.ShowUI(CUIResources.XingGuanShengJiWnd)
    elseif self.m_Type == EnumMoneyType.CityWarMaterial then
        g_MessageMgr:ShowMessage("City_War_Add_Material")
    elseif self.m_Type == EnumMoneyType.TiLi then
        g_MessageMgr:ShowMessage("YangYu_Add_TiLi")
    elseif self.m_Type == EnumMoneyType.MingWang then
        g_MessageMgr:ShowMessage("MingWang_Shop_Add_Tip")
    elseif self.m_Type == EnumMoneyType.NanDuTongBao then
        CUIManager.ShowUI(CLuaUIResources.NanDuFanHuaExchangeMoneyWnd)
    else
        if self.m_IsTempPlayScore and self.m_TempPlayScoreType == EnumTempPlayScoreKey_lua["HanJiaScore"] then
            g_MessageMgr:ShowMessage("HanJia_Shop_Add_Tip")
        else
            g_MessageMgr:ShowMessage("FUNCTION_NOT_OPEN")
        end
    end
end
function CLuaCurentMoneyCtrl:SetCost( cost) 
    self.cost = cost
    if self.m_CostLabel ~= nil then
        self.m_CostLabel.text = tostring(cost)
    end

    if self.m_IsTempPlayScore then
        self:UpdateTempPlayScore()
    else
        if self.m_Type == EnumMoneyType.YinPiao then
            self:UpdateYinPiaoDisplay()
        elseif self.m_Type == EnumMoneyType.YinLiang then
            self:UpdateYinLiangDisplay()
        elseif self.m_Type == EnumMoneyType.GuildContri or self.m_Type == EnumMoneyType.Score then
            self:OnMainPlayerPlayPropUpdate()
        end
    end
end
function CLuaCurentMoneyCtrl:UpdateYinPiaoDisplay( )
    if CClientMainPlayer.Inst ~= nil and self.m_OwnLabel ~= nil then
        --有银票的时候显示银票的图标
        if CClientMainPlayer.Inst.FreeSilver > 0 or not self.m_bForceChangeYinpiao2Yinliang then
            if self.m_OwnIcon ~= nil then
                self.m_OwnIcon.spriteName = Money.GetIconName(EnumMoneyType.YinPiao, EnumPlayScoreKey.NONE)
            end
            if self.m_CostIcon ~= nil then
                self.m_CostIcon.spriteName = self.m_OwnIcon.spriteName
            end
        else
            if self.m_OwnIcon ~= nil then
                self.m_OwnIcon.spriteName = Money.GetIconName(EnumMoneyType.YinLiang, EnumPlayScoreKey.NONE)
            end
            if self.m_CostIcon ~= nil then
                self.m_CostIcon.spriteName = self.m_OwnIcon.spriteName
            end
        end
        --moneyEnough = (CClientMainPlayer.Inst.FreeSilver + CClientMainPlayer.Inst.Silver) >= cost;
        --m_OwnLabel.color = moneyEnough ? Color.white : Color.red;
        --m_OwnLabel

        self.own = CClientMainPlayer.Inst.FreeSilver + CClientMainPlayer.Inst.Silver
        if not self.m_bForceChangeYinpiao2Yinliang then
            self.own = CClientMainPlayer.Inst.FreeSilver
        end
        self.m_OwnLabel.text = self:IsEnough() and tostring(self.own) or System.String.Format("[c][ff0000]{0}[-][/c]", self.own)
    end
end
function CLuaCurentMoneyCtrl:GetCost()
    return self.cost
end
function CLuaCurentMoneyCtrl:GetOwn()
    return self.own
end

function CLuaCurentMoneyCtrl:IsEnough()
    if self.m_Type==EnumMoneyType.YinPiao and not self.m_bForceChangeYinpiao2Yinliang then
        if CClientMainPlayer.Inst then
            return CClientMainPlayer.Inst.FreeSilver+CClientMainPlayer.Inst.Silver>self.cost
        end
    end
    return self.own>=self.cost
end

function CLuaCurentMoneyCtrl:UpdateYinLiangDisplay( )
    if CClientMainPlayer.Inst ~= nil and self.m_Type == EnumMoneyType.YinLiang then
        self.own = CClientMainPlayer.Inst.Silver
        if self.m_CostLabel ~= nil and self:GetCost() > 0 then
            self.m_OwnLabel.text = self:IsEnough() and tostring(self.own) or System.String.Format("[c][ff0000]{0}[-][/c]", self.own)
        else
            self.m_OwnLabel.text = tostring(self.own)
        end
    end
end
function CLuaCurentMoneyCtrl:OnMainPlayerPlayPropUpdate( )
    if self.m_IsTempPlayScore then return end
    if CClientMainPlayer.Inst == nil then
        return
    end

    if self.m_Type == EnumMoneyType.GuildContri then
        if self.m_OwnIcon ~= nil then
            self.m_OwnIcon.spriteName = Money.GetIconName(EnumMoneyType.GuildContri, EnumPlayScoreKey.NONE)
        end
        if self.m_CostIcon ~= nil then
            self.m_CostIcon.spriteName = self.m_OwnIcon.spriteName
        end

        self.own = CClientMainPlayer.Inst.PlayProp.GuildData.Contribution
        self.m_OwnLabel.text = self:IsEnough() and tostring(self.own) or System.String.Format("[c][ff0000]{0}[-][/c]", self.own)
    end
    if self.m_Type == EnumMoneyType.Huoli then
        if self.m_OwnIcon ~= nil then
            self.m_OwnIcon.spriteName = Money.GetIconName(EnumMoneyType.Huoli, EnumPlayScoreKey.NONE)
        end
        if self.m_CostIcon ~= nil then
            self.m_CostIcon.spriteName = self.m_OwnIcon.spriteName
        end

        self.own = CClientMainPlayer.Inst.PlayProp.HuoLi
        self.m_OwnLabel.text = self:IsEnough() and tostring(self.own) or System.String.Format("[c][ff0000]{0}[-][/c]", self.own)
    elseif self.m_Type == EnumMoneyType.Score and self.m_ScoreType == EnumPlayScoreKey.GXZ then
        if self.m_OwnIcon ~= nil then
            self.m_OwnIcon.spriteName = Money.GetIconName(EnumMoneyType.Score, EnumPlayScoreKey.GXZ)
        end
        if self.m_CostIcon ~= nil then
            self.m_CostIcon.spriteName = self.m_OwnIcon.spriteName
        end

        self.own = CClientMainPlayer.Inst.PlayProp.GuildData.GuildGongXun
        self.m_OwnLabel.text = self:IsEnough() and tostring(self.own) or System.String.Format("[c][ff0000]{0}[-][/c]", self.own)
    elseif self.m_Type == EnumMoneyType.Score then
        if self.m_OwnIcon ~= nil then
            self.m_OwnIcon.spriteName = Money.GetIconName(self.m_Type, self.m_ScoreType)
        end
        if self.m_CostIcon ~= nil then
            self.m_CostIcon.spriteName = self.m_OwnIcon.spriteName
        end

        self.own = CClientMainPlayer.Inst.PlayProp:GetScoreByKey(self.m_ScoreType)
        self.m_OwnLabel.text = self:IsEnough() and tostring(self.own) or System.String.Format("[c][ff0000]{0}[-][/c]", self.own)
    elseif self.m_Type == EnumMoneyType.Xingmang then
        if self.m_OwnIcon ~= nil then
            self.m_OwnIcon.spriteName = Money.GetIconName(self.m_Type, self.m_ScoreType)
        end
        if self.m_CostIcon ~= nil then
            self.m_CostIcon.spriteName = self.m_OwnIcon.spriteName
        end
        local left = CFeiShengMgr.Inst.m_TotalXingmangCount - CFeiShengMgr.Inst.m_UsedXingmangCount
        self.own = math.floor(left)
        self.m_OwnLabel.text = tostring(self.own)
    elseif self.m_Type == EnumMoneyType.CityWarMaterial then
        if self.m_OwnIcon ~= nil then
            self.m_OwnIcon.spriteName = Money.GetIconName(self.m_Type, EnumPlayScoreKey.NONE)
        end
        if self.m_CostIcon ~= nil then
            self.m_CostIcon.spriteName = self.m_OwnIcon.spriteName
        end
        self.own = CCityWarMgr.Inst:GetCurCityMaterial()
        self.m_OwnLabel.text = tostring(self.own)
    elseif self.m_Type == EnumMoneyType.MingWang then
        if self.m_OwnIcon ~= nil then
            self.m_OwnIcon.spriteName = Money.GetIconName(self.m_Type, EnumPlayScoreKey.NONE)
        end
        if self.m_CostIcon ~= nil then
            self.m_CostIcon.spriteName = self.m_OwnIcon.spriteName
        end
        self.own = CClientMainPlayer.Inst.PlayProp.MingWangData.Score
        self.m_OwnLabel.text = tostring(self.own)

    elseif self.m_Type == EnumMoneyType.TiLi then
        if self.m_OwnIcon ~= nil then
            self.m_OwnIcon.spriteName = Money.GetIconName(self.m_Type, EnumPlayScoreKey.NONE)
        end
        if self.m_CostIcon ~= nil then
            self.m_CostIcon.spriteName = self.m_OwnIcon.spriteName
        end
    end

    if self.updateAction ~= nil then
        self.updateAction()
    end
end
function CLuaCurentMoneyCtrl:OnUpdateMoney( )
    if self.m_IsTempPlayScore then return end
    if CClientMainPlayer.Inst ~= nil and self.m_OwnLabel ~= nil then
        if self.m_Type == EnumMoneyType.YinLiang then
            self.own = CClientMainPlayer.Inst.Silver
            self.m_OwnLabel.text = tostring(CClientMainPlayer.Inst.Silver)
            self:UpdateYinLiangDisplay()
        elseif self.m_Type == EnumMoneyType.YinPiao then
            --m_OwnLabel.text = (CClientMainPlayer.Inst.FreeSilver + CClientMainPlayer.Inst.Silver).ToString();
            --有银票的时候显示银票的图标
            self:UpdateYinPiaoDisplay()
        elseif self.m_Type == EnumMoneyType.YuanBao then
            self.own = CClientMainPlayer.Inst.YuanBao
            self.m_OwnLabel.text = tostring(CClientMainPlayer.Inst.YuanBao)
        elseif self.m_Type == EnumMoneyType.LingYu then
            self.own = CClientMainPlayer.Inst.Jade
            self.m_OwnLabel.text = tostring(CClientMainPlayer.Inst.Jade)
        elseif self.m_Type == EnumMoneyType.LingYu_OnlyBind then
            self.own = CClientMainPlayer.Inst.BindJade
            self.m_OwnLabel.text = tostring(self.own)
        elseif self.m_Type == EnumMoneyType.LingYu_WithBind then
            self.own = CClientMainPlayer.Inst.Jade + CClientMainPlayer.Inst.BindJade
            self.m_OwnLabel.text = tostring(self.own)
            self:UpdateIcon(self.m_Type)
        elseif self.m_Type == EnumMoneyType.CityWarMaterial then
            self.own = CCityWarMgr.Inst:GetCurCityMaterial()
            self.m_OwnLabel.text = tostring(self.own)
            self:UpdateIcon(self.m_Type)
        elseif self.m_Type == EnumMoneyType.TiLi then
            self.own = BabyMgr.Inst:GetSelectedBabyTiLi()
            self.m_OwnLabel.text = tostring(self.own)
            self:UpdateIcon(self.m_Type)
        elseif self.m_Type == EnumMoneyType.NanDuTongBao then
            local lordData = {}
            if CClientMainPlayer.Inst and CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, LuaYiRenSiMgr.LordTempPlayDataKey) then
                local playData = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, LuaYiRenSiMgr.LordTempPlayDataKey)
                if playData.ExpiredTime > CServerTimeMgr.Inst.timeStamp and playData.Data and playData.Data.Data then
                    local data = g_MessagePack.unpack(playData.Data.Data)
                    lordData = data[1]
                end
            end
            local moneyCnt = lordData[LuaYiRenSiMgr.LordPropertyKey.moneyIndex] and lordData[LuaYiRenSiMgr.LordPropertyKey.moneyIndex] or 0
            self.own = moneyCnt
            self.m_OwnLabel.text = tostring(self.own)
            --这个不是sprite里的, 直接在prefab里贴好texture吧
            --self:UpdateIcon(self.m_Type)
        end
        if self.updateAction ~= nil then
            self.updateAction()
        end
    end
end

