-- Auto Generated!!
local CGuildPreLeagueInfoItem = import "L10.UI.CGuildPreLeagueInfoItem"
local CommonDefs = import "L10.Game.CommonDefs"
local Constants = import "L10.Game.Constants"
local EnumClass = import "L10.Game.EnumClass"
local GuildDefine = import "L10.Game.GuildDefine"
local Profession = import "L10.Game.Profession"
CGuildPreLeagueInfoItem.m_Init_CS2LuaHook = function (this, info, index) 
    if index % 2 == 0 then
        this:SetBackgroundTexture(Constants.EvenBgSprite)
    else
        this:SetBackgroundTexture(Constants.OddBgSpirite)
    end

    this.clsSprite.spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), info.cls))


    this.nameLabel.text = info.name
    this.titleLabel.text = GuildDefine.GetOfficeName(info.title)
    this.levelLabel.text = System.String.Format("Lv.{0}", info.grade)

    this.contributionLabel.text = System.String.Format("{0}/{1}/{2}", info.contribution, info.contributionWeek, info.contributionTotal)
    this.pointLabel.text = tostring(info.memberPoint)
end
