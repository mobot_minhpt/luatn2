local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local CGuildMgr = import "L10.Game.CGuildMgr"
local QnTabView = import "L10.UI.QnTabView"


CLuaGuildMainWnd = class()
RegistClassMember(CLuaGuildMainWnd,"mTitle")
RegistClassMember(CLuaGuildMainWnd,"tabBar")

function CLuaGuildMainWnd:Awake()
    self.mTitle = self.transform:Find("Wnd_Bg_Primary_Tab/TitleLabel"):GetComponent(typeof(UILabel))
    self.tabBar = self.transform:Find("Offset/QnTabView"):GetComponent(typeof(QnTabView))
end

function CLuaGuildMainWnd:OnEnable( )
    g_ScriptEvent:AddListener("RequestOperationInGuildSucceed", self, "OnRequestOperationInGuildSucceed")

    Gac2Gas.RequestUpdateServerTime()
    --这个rpc之前被实现在了lua下，这里直接手工处理到这，原来的CS部分有改动转lua时，请保留下面的内容
    Gac2Gas.QueryCityWarPlayOpenStatus()
    --end
end
function CLuaGuildMainWnd:OnDisable( )
    g_ScriptEvent:RemoveListener("RequestOperationInGuildSucceed", self, "OnRequestOperationInGuildSucceed")
end
function CLuaGuildMainWnd:OnRequestOperationInGuildSucceed( args ) 
    local requestType, paramStr = args[0], args[1]
    if (("LeaveGuild") == requestType) then
        CUIManager.CloseUI(CUIResources.GuildMainWnd)
    end
end

function CLuaGuildMainWnd:Init( )
    self.tabBar.OnSelect = (DelegateFactory.Action_QnTabButton_int(function (button, index) 
        if CommonDefs.IS_VN_CLIENT then
            self.mTitle.text = string.gsub(button.Text, "\n", " ")
        else
            self.mTitle.text = LocalString.GetString("帮会") .. string.gsub(button.Text, "\n", "")
        end

        --#region Guide
        EventManager.BroadcastInternalForLua(EnumEventType.Guide_ChangeView, {"GuildMainWnd"})
        --#endregion

        if index == 1 then
            if CGuildMgr.Inst.m_SelectMemberId > 0 then
                g_ScriptEvent:BroadcastInLua("TrySelectGuildMember")
            end
        end
    end))
    self.tabBar:ChangeTo(CGuildMgr.Inst.GuildMainWndTabIndex)
end
function CLuaGuildMainWnd:GetGuideGo( methodName) 
    if methodName == "Get3rdTab" then
        return self.tabBar:GetTabGameObject(2)
    elseif methodName == "Get4thTab" then
        return self.tabBar:GetTabGameObject(3)
    end
    return nil
end
function CLuaGuildMainWnd:GetTabIndex( ) 
    return self.tabBar.CurrentSelectTab
end

function CLuaGuildMainWnd:OnDestroy()
    CLuaGuildMgr.m_ExtraNames={}
    CLuaGuildMgr.m_GuildRightsDic=nil
end