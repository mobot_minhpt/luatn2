-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CDuoBaoInfo = import "L10.UI.CDuoBaoInfo"
local CDuoBaoMgr = import "L10.UI.CDuoBaoMgr"
local CDuoBaoResultInfo = import "L10.UI.CDuoBaoResultInfo"
local CItemMgr = import "L10.Game.CItemMgr"
local CMyDuoBaoInfo = import "L10.UI.CMyDuoBaoInfo"
local CommonDefs = import "L10.Game.CommonDefs"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local DelegateFactory = import "DelegateFactory"
local EGlobalDuoBaoStatus = import "L10.UI.EGlobalDuoBaoStatus"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Int32 = import "System.Int32"
local MsgPackImpl = import "MsgPackImpl"
local Object = import "System.Object"
local StringIntKeyValuePair = import "L10.Game.StringIntKeyValuePair"
local UInt32 = import "System.UInt32"
--local YinLiangDuoBao_Config = import "L10.Game.YinLiangDuoBao_Config"
--全局的DuoBao状态,未开始，进行中，开始

CDuoBaoMgr.m_ShowWnd_CS2LuaHook = function (this) 
    if CDuoBaoMgr.s_EnableDuoBao then
        CUIManager.ShowUI(CUIResources.DuoBaoWnd)
    end
end
CDuoBaoMgr.m_InitData_CS2LuaHook = function (this) 
    this:ClearData()
    YinLiangDuoBao_Config.Foreach(function (k, v) 
        if v.IsActive <= 0 then
            return
        end
        local info = CreateFromClass(CDuoBaoInfo)
        info.ID = v.ID
        info.Type = v.Type
        info.ItemID = v.ItemID
        info.ItemCount = v.ItemCount

        local itemName = ""
        local itemData = CItemMgr.Inst:GetItemTemplate(info.ItemID)
        if itemData ~= nil then
            if CommonDefs.IS_VN_CLIENT then
                itemName = System.String.Format("{0} x{1}", itemData.Name, info.ItemCount)
            else
                itemName = System.String.Format("{0}x{1}", itemData.Name, info.ItemCount)
            end
        end
        info.ItemName = itemName
        info.IsCrossServer = v.IsKuaFu
        info.Category = v.Buy_Type
        info.Need_FenShu = v.Need_FenShu
        info.Buy_Max = v.Buy_Max
        info.Fenshu_Min = v.Fenshu_min
        info.GroupCount = CDuoBaoMgr.s_UseServerGroupCount and 0 or v.Count
        info.SortWeight = v.Weight
        info.CurrentGroupIndex = 0
        if not CommonDefs.DictContains(this.m_Category2DuoBaoInfos, typeof(Int32), info.Category) then
            CommonDefs.DictAdd(this.m_Category2DuoBaoInfos, typeof(Int32), info.Category, typeof(MakeGenericClass(List, CDuoBaoInfo)), CreateFromClass(MakeGenericClass(List, CDuoBaoInfo)))
        end
        CommonDefs.ListAdd(CommonDefs.DictGetValue(this.m_Category2DuoBaoInfos, typeof(Int32), info.Category), typeof(CDuoBaoInfo), info)
        CommonDefs.DictSet(this.m_Id2DuoBaoInfos, typeof(UInt32), info.ID, typeof(CDuoBaoInfo), info)
    end)
    CommonDefs.DictIterate(this.m_Category2DuoBaoInfos, DelegateFactory.Action_object_object(function (___key, ___value) 
        local kv = {}
        kv.Key = ___key
        kv.Value = ___value
        CommonDefs.ListSort1(kv.Value, typeof(CDuoBaoInfo), DelegateFactory.Comparison_CDuoBaoInfo(function (a, b) 
            return NumberCompareTo(a.SortWeight, b.SortWeight)
        end))
    end))
end
CDuoBaoMgr.m_ClearData_CS2LuaHook = function (this) 
    this.EndTimeStamp = 0
    this.GlobalDuoBaoStatus = EGlobalDuoBaoStatus.NotStarted
    CommonDefs.DictClear(this.m_Category2DuoBaoInfos)
    CommonDefs.DictClear(this.m_Id2DuoBaoInfos)
    CommonDefs.ListClear(this.ret)
end
CDuoBaoMgr.m_GetYinLiang_PerFenShu_CDuoBaoInfo_CS2LuaHook = function (this, info) 

    if info == nil then
        return this:MoneyUnit(false)
    end
    local isLingYu = this:IsLingYuDuoBao(info)
    return this:MoneyUnit(isLingYu) * math.max(1, info.Fenshu_Min)
end
CDuoBaoMgr.m_GetDuoBaoInfosByCategory_CS2LuaHook = function (this, category) 
    CommonDefs.ListClear(this.ret)
    if CommonDefs.DictContains(this.m_Category2DuoBaoInfos, typeof(Int32), category) then
        do
            local i = 0
            while i < CommonDefs.DictGetValue(this.m_Category2DuoBaoInfos, typeof(Int32), category).Count do
                if CommonDefs.DictGetValue(this.m_Category2DuoBaoInfos, typeof(Int32), category)[i].GroupCount > 0 then
                    CommonDefs.ListAdd(this.ret, typeof(CDuoBaoInfo), CommonDefs.DictGetValue(this.m_Category2DuoBaoInfos, typeof(Int32), category)[i])
                end
                i = i + 1
            end
        end
    end
    return this.ret
end
Gas2Gac.ReplyCurrentRoundDuoBaoInfo = function (duoBaoId, currentRound, currentProgress, myInvest, myInvestTotalRound, myInvestTotalFenshu) 
    local info = CDuoBaoMgr.Inst:GetDuoBaoInfoById(duoBaoId)
    if info ~= nil then
        info.CurrentGroupIndex = currentRound
        info.CurrentGroupProgress = currentProgress
        info.MyInvestInCurrentGroup = myInvest
        info.MyInvestInTotalGroup = myInvestTotalRound
        info.MyInvestInTotalFenShu = myInvestTotalFenshu
    end
    EventManager.BroadcastInternalForLua(EnumEventType.ReplyCurrentRoundDuoBaoInfo, {duoBaoId})
end
Gas2Gac.ReplyDuoBaoOverview = function (duoBaoId, totalRound, currentRound, currentProgress, myInvest, myInvestTotalRound, myInvestTotalFenshu) 
    local info = CDuoBaoMgr.Inst:GetDuoBaoInfoById(duoBaoId)
    if info ~= nil then
        info.GroupCount = totalRound
        info.CurrentGroupIndex = currentRound
        info.CurrentGroupProgress = currentProgress

        info.MyInvestInCurrentGroup = myInvest
        info.MyInvestInTotalGroup = myInvestTotalRound
        info.MyInvestInTotalFenShu = myInvestTotalFenshu
    end
end
Gas2Gac.ReplyMyDuoBaoInfo = function (duoBaoId, round, currentProgress, myInvestFenShu, winnerId, winnerName, winnerFenShu, topUsers, winnerServerName)
    local myInfo = CreateFromClass(CMyDuoBaoInfo)
    myInfo.ID = duoBaoId
    myInfo.GroupIndex = round
    myInfo.CurrentGroupProgress = currentProgress
    myInfo.MyInvestInFenShu = myInvestFenShu
    myInfo.WinnerId = math.floor(tonumber(winnerId or 0))
    myInfo.WinnerName = winnerName
    myInfo.WinnerInvestInFenShu = winnerFenShu
    myInfo.TopInvestUsers = CreateFromClass(MakeGenericClass(List, StringIntKeyValuePair))
    myInfo.Servername = winnerServerName
    local info = CDuoBaoMgr.Inst:GetDuoBaoInfoById(duoBaoId)
    local topInvesters = TypeAs(MsgPackImpl.unpack(topUsers), typeof(MakeGenericClass(List, Object)))
    do
        local i = 0
        while i < topInvesters.Count do
            local userInfo = TypeAs(topInvesters[i], typeof(MakeGenericClass(List, Object)))
            local userName = ToStringWrap(userInfo[0])
            local investFenshu = math.floor(tonumber(userInfo[1] or 0))

            local userServerName = userInfo[2] or LocalString.GetString("跨服")
            CommonDefs.ListAdd(myInfo.TopInvestUsers, typeof(StringIntKeyValuePair), CreateFromClass(StringIntKeyValuePair,
                    (info.IsCrossServer ~= 0) and SafeStringFormat3("%s(%s)", userName, userServerName) or userName, investFenshu))
            i = i + 1
        end
    end
    myInfo.ItemID = info.ItemID
    myInfo.ItemCount = info.ItemCount
    myInfo.Need_FenShu = info.Need_FenShu
    myInfo.ItemName = info.ItemName
    myInfo.SortWeight = info.SortWeight
    myInfo.IsCrossServer = info.IsCrossServer
    CommonDefs.ListAdd(CDuoBaoMgr.Inst.MyDuoBaoInfos, typeof(CMyDuoBaoInfo), myInfo)
end
Gas2Gac.ReplyMyDuoBaoInfoEnd = function () 
    --排序
    --排序
    CommonDefs.ListSort1(CDuoBaoMgr.Inst.MyDuoBaoInfos, typeof(CMyDuoBaoInfo), DelegateFactory.Comparison_CMyDuoBaoInfo(function (a, b) 
        if a.ID ~= b.ID then
            return NumberCompareTo(a.SortWeight, b.SortWeight)
        end
        return NumberCompareTo(a.GroupIndex, b.GroupIndex)
    end))
    EventManager.Broadcast(EnumEventType.ReplyMyDuoBaoInfoEnd)
end
Gas2Gac.ReplyDuoBaoResult = function (resCount, timeStamp, duobaoId, round, winnerId, winnerName, winnerInverst) 
    CDuoBaoMgr.Inst.TotalResultCount = resCount

    local resultInfo = CreateFromClass(CDuoBaoResultInfo)
    local dateTime = CServerTimeMgr.ConvertTimeStampToZone8Time(timeStamp)
    resultInfo.TimeString = ToStringWrap(dateTime, "yyyy-MM-dd")
    resultInfo.ID = duobaoId
    resultInfo.GroupIndex = round
    resultInfo.WinnerId = math.floor(tonumber(winnerId or 0))
    resultInfo.WinnerName = winnerName
    resultInfo.WinnerInvest = winnerInverst

    local info = CDuoBaoMgr.Inst:GetDuoBaoInfoById(duobaoId)
    resultInfo.ItemID = info.ItemID
    resultInfo.ItemCount = info.ItemCount
    CommonDefs.ListAdd(CDuoBaoMgr.Inst.DuobaoResultInfos, typeof(CDuoBaoResultInfo), resultInfo)
end
Gas2Gac.ReplyDuoBaoInvestResult = function (duoBaoId, round, count, isSuccess) 
    local info = CDuoBaoMgr.Inst:GetDuoBaoInfoById(duoBaoId)
    if info ~= nil then
        if isSuccess then
            if CDuoBaoMgr.Inst:IsLingYuDuoBao(info) then
                g_MessageMgr:ShowMessage("message_yinliangduobao_touru_tishi2", count * CDuoBaoMgr.Inst:GetYinLiang_PerFenShu(info))
            else
                g_MessageMgr:ShowMessage("message_yinliangduobao_touru_tishi", count * CDuoBaoMgr.Inst:GetYinLiang_PerFenShu(info))
            end
        end
        --刷新界面
        CDuoBaoMgr.Inst:QueryDuoBaoInfoOverView()
        CDuoBaoMgr.Inst:QueryCurrentDuoBaoStatus()
    end
end
Gas2Gac.ReplyDuoBaoRewardHistory = function (timestamp, duoBaoId, round, invest) 
    local resultInfo = CreateFromClass(CDuoBaoResultInfo)
    local dateTime = CServerTimeMgr.ConvertTimeStampToZone8Time(timestamp)
    resultInfo.TimeString = ToStringWrap(dateTime, "yyyy-MM-dd")
    resultInfo.ID = duoBaoId
    resultInfo.GroupIndex = round
    resultInfo.WinnerId = CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.Id or 0
    local default
    if CClientMainPlayer.Inst ~= nil then
        default = CClientMainPlayer.Inst.Name
    else
        default = ""
    end
    resultInfo.WinnerName = default
    resultInfo.WinnerInvest = invest

    local info = CDuoBaoMgr.Inst:GetDuoBaoInfoById(duoBaoId)
    resultInfo.ItemID = info.ItemID
    resultInfo.ItemCount = info.ItemCount

    CommonDefs.ListAdd(CDuoBaoMgr.Inst.MyDuobaoHistoryInfos, typeof(CDuoBaoResultInfo), resultInfo)
end
