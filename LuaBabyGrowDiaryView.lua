require("common/common_include")

local UIVerticalLabel = import "UIVerticalLabel"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local UIGrid = import "UIGrid"
local BabyMgr = import "L10.Game.BabyMgr"
local Baby_Diary = import "L10.Game.Baby_Diary"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local NGUIText = import "NGUIText"
local AlignType = import "L10.UI.CTooltip+AlignType"

LuaBabyGrowDiaryView = class()

RegistClassMember(LuaBabyGrowDiaryView, "PageButton")
RegistClassMember(LuaBabyGrowDiaryView, "DiaryGrid")
RegistClassMember(LuaBabyGrowDiaryView, "DiaryTemplate")

RegistClassMember(LuaBabyGrowDiaryView, "DiaryInfos")
RegistClassMember(LuaBabyGrowDiaryView, "DiaryNumPerPage")
RegistClassMember(LuaBabyGrowDiaryView, "DiaryMaxPage")

RegistClassMember(LuaBabyGrowDiaryView, "SelectedBaby")
RegistClassMember(LuaBabyGrowDiaryView, "CurrentPage")

local numString = {LocalString.GetString("一"),LocalString.GetString("二"),LocalString.GetString("三"),LocalString.GetString("四"),LocalString.GetString("五"),
LocalString.GetString("六"),LocalString.GetString("七"),LocalString.GetString("八"),LocalString.GetString("九"),LocalString.GetString("十"),}

local GetNumString = function(num)
	num = tonumber(num)
	if num > 0 and num <= 10 then
		return numString[num]
	elseif num > 10 and num <= 19 then
		return numString[10] .. numString[num%10]
	elseif num >= 20 then
		if num%10 == 0  then
			return numString[math.floor(num/10)] .. numString[10]
		else
			return numString[math.floor(num/10)] .. numString[10] .. numString[num%10]
		end
	end
	return LocalString.GetString("零")
end

local GetYearString = function(year)
	local first = math.floor(year / 1000)
	local second = math.floor((year - first* 1000) / 100)
	local third = math.floor((year - first* 1000 - second * 100) / 10)
	local forth = year%10
	return GetNumString(first) .. GetNumString(second) .. GetNumString(third) .. GetNumString(forth)
end

local GetDateString = function(date)
	if CommonDefs.IS_VN_CLIENT then
		return SafeStringFormat3("%02d/%02d/%s",date.Day,date.Month,date.Year)
	end
	return SafeStringFormat3(LocalString.GetString("%s年%s月%s日"), GetYearString(date.Year), GetNumString(date.Month), GetNumString(date.Day))
end

function LuaBabyGrowDiaryView:Init()
	self:InitClassMembers()
	self:InitValues()
end

function LuaBabyGrowDiaryView:InitClassMembers()
	self.PageButton = self.transform:Find("PageButton"):GetComponent(typeof(QnAddSubAndInputButton))
	self.DiaryGrid = self.transform:Find("DiaryGrid"):GetComponent(typeof(UIGrid))
	self.DiaryTemplate = self.transform:Find("DiaryTemplate").gameObject
	self.DiaryTemplate:SetActive(false)

end

function LuaBabyGrowDiaryView:InitValues()

	self.DiaryInfos= {}
	self.DiaryNumPerPage = 4
	self.CurrentPage = 1

	local selectedBaby = BabyMgr.Inst:GetSelectedBaby()
	if not selectedBaby then
		CUIManager.CloseUI(CLuaUIResources.BabyGrowDiaryWnd)
		return
	end

	self.SelectedBaby = selectedBaby
	self:UpdateDiaries()
end

function LuaBabyGrowDiaryView:UpdateDiaries()
	local diaryDict = self.SelectedBaby.Props.ScheduleData.DiaryData
	if not diaryDict then return end

	self.DiaryInfos= {}
	CommonDefs.DictIterate(diaryDict, DelegateFactory.Action_object_object(function (___key, ___value)
		table.insert(self.DiaryInfos, {
			index = ___key,
			value = ___value,
			})
    end))

    -- 将未领取的任务提到前面，剩下的按照时间排序
    local compare = function (a, b)
    	if a.value.Accepted == 0 and b.value.Accepted == 1 then
    		return true
    	elseif a.value.Accepted == 1 and b.value.Accepted == 0 then
    		return false
    	else
    		return a.value.FinishTime < b.value.FinishTime
    	end
    end
    table.sort(self.DiaryInfos, compare)
	
	self.DiaryMaxPage = math.ceil(#self.DiaryInfos / self.DiaryNumPerPage)
	if self.DiaryMaxPage == 0 then
		self.PageButton:SetMinMax(0, 0, 1)
	else
		self.PageButton:SetMinMax(1, self.DiaryMaxPage, 1)
		local onPageChange = function (value)
			self:OnPageChange(value)
		end
		self.PageButton:SetNumberInputAlignType(AlignType.Right)
		self.PageButton.onValueChanged  = DelegateFactory.Action_uint(onPageChange)
		self.PageButton.ImmediateCallBackWhenInput = false

		self.PageButton:SetValue(self.CurrentPage, true)
	end
end

function LuaBabyGrowDiaryView:OnPageChange(value)

	local txt = SafeStringFormat3(LocalString.GetString("%s/%s"), tostring(value), tostring(self.DiaryMaxPage))
	self.PageButton:OverrideText(txt)
	self.CurrentPage = value

	CUICommonDef.ClearTransform(self.DiaryGrid.transform)

	local low = (value - 1) * self.DiaryNumPerPage + 1
	local high = (value - 1) * self.DiaryNumPerPage + 4
	if high > #self.DiaryInfos then
		high = #self.DiaryInfos
	end
	for i = low, high, 1 do
		local go = NGUITools.AddChild(self.DiaryGrid.gameObject, self.DiaryTemplate)
    	go.name =  SafeStringFormat3("%02d", value * self.DiaryNumPerPage - i)
    	self:InitDiaryTemplate(go, self.DiaryInfos[i])
        go:SetActive(true)
	end
	self.DiaryGrid:Reposition()

end

function LuaBabyGrowDiaryView:InitDiaryTemplate(go, diaryInfo)
	local Title = nil
	local Content = nil
	if CommonDefs.IS_VN_CLIENT then
		Title = go.transform:Find("TitleLabel"):GetComponent(typeof(UILabel))
		Title.color = NGUIText.ParseColor24("3B2009", 0)
		Content = go.transform:Find("ContentLabel"):GetComponent(typeof(UILabel))
	else
		Title = go.transform:Find("Title"):GetComponent(typeof(UIVerticalLabel))
		Title.fontColor = NGUIText.ParseColor24("3B2009", 0)
		Content = go.transform:Find("Content"):GetComponent(typeof(UIVerticalLabel))
	end
	
	local AcceptButton = go.transform:Find("AcceptButton").gameObject
	local AcceptedStatus = go.transform:Find("AcceptedStatus").gameObject

	local date = CServerTimeMgr.ConvertTimeStampToZone8Time(diaryInfo.value.FinishTime)
	Title.text = GetDateString(date)

	Title.gameObject:SetActive(true)
	Content.gameObject:SetActive(true)
	
	
	local diary = Baby_Diary.GetData(diaryInfo.value.DiaryId)
	if not diary then return end

	local AcceptButton = go.transform:Find("AcceptButton").gameObject
	local AcceptedStatus = go.transform:Find("AcceptedStatus").gameObject
	AcceptButton:SetActive(diaryInfo.value.Accepted == 0)
	AcceptedStatus:SetActive(diaryInfo.value.Accepted == 1)

	-- 处理显示第几段描述
	local pos = 1
	if diaryInfo.value.Accepted == 1 then
		-- 已领取
		pos = diaryInfo.value.TaskPos
	else
		-- 未领取
		local diaryTaskPos = self.SelectedBaby.Props.TaskData.DiaryTaskPos
		if CommonDefs.DictContains_LuaCall(diaryTaskPos, diaryInfo.value.DiaryId) then
			pos = diaryTaskPos[diaryInfo.value.DiaryId] + 1
		end
	end

	if pos > diary.Desc.Length or pos < 1 then
		pos = 1
	end
	if CommonDefs.IS_VN_CLIENT then
		Content.text = string.gsub(diary.Desc[pos-1], "#r", "")
	else
		Content.text = diary.Desc[pos-1]
	end


	-- 增加点击事件
	local onAcceptTask = function (go)
    	self:OnAcceptTask(go, diaryInfo.index)
    end
    CommonDefs.AddOnClickListener(AcceptButton, DelegateFactory.Action_GameObject(onAcceptTask),false)
end

function LuaBabyGrowDiaryView:OnAcceptTask(go, index)

	local selectedBaby = BabyMgr.Inst:GetSelectedBaby()
	if not selectedBaby then
		CUIManager.CloseUI(CLuaUIResources.BabyGrowDiaryWnd)
		return
	end
	Gac2Gas.RequestAcceptTaskFromBaby(selectedBaby.Id, index)
end

function LuaBabyGrowDiaryView:OnEnable()
	g_ScriptEvent:AddListener("AcceptTaskFromBabySuccess", self, "AcceptTaskFromBabySuccess")
end

function LuaBabyGrowDiaryView:OnDisable()
	g_ScriptEvent:RemoveListener("AcceptTaskFromBabySuccess", self, "AcceptTaskFromBabySuccess")
end

function LuaBabyGrowDiaryView:AcceptTaskFromBabySuccess(babyId)
	if babyId == self.SelectedBaby.Id then
		self:UpdateDiaries()
	end
end

return LuaBabyGrowDiaryView
