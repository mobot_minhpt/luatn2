local UITexture = import "UITexture"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local UISprite = import "UISprite"
local QnTableView = import "L10.UI.QnTableView"
local GameObject = import "UnityEngine.GameObject"
local GestureRecognizer = import "L10.Engine.GestureRecognizer"
local GestureType = import "L10.Engine.GestureType"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local Screen = import "UnityEngine.Screen"
local EasyTouch = import "EasyTouch"
local Profession = import "L10.Game.Profession"
local EnumBodyPosition = import "L10.Game.EnumBodyPosition"
local CTalismanMgr = import "L10.Game.CTalismanMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local ShowType = import "L10.UI.CItemInfoMgr+ShowType"
local CItemMgr = import "L10.Game.CItemMgr"
local LinkItemInfo = import "L10.UI.CItemInfoMgr+LinkItemInfo"
local Vector2 = import "UnityEngine.Vector2"
local Camera = import "UnityEngine.Camera"
local PlayerSettings = import "L10.Game.PlayerSettings"
local RenderTexture = import "UnityEngine.RenderTexture"
local TextureWrapMode = import "UnityEngine.TextureWrapMode"
local FilterMode = import "UnityEngine.FilterMode"
local RenderTextureFormat = import "UnityEngine.RenderTextureFormat"
local RenderTextureReadWrite = import "UnityEngine.RenderTextureReadWrite"
local CSharpResourceLoader = import "L10.Game.CSharpResourceLoader"
local Vector3 = import "UnityEngine.Vector3"
local Quaternion = import "UnityEngine.Quaternion"
local LayerDefine = import "L10.Engine.LayerDefine"
local EnumClass = import "L10.Game.EnumClass"
local EnumGender = import "L10.Game.EnumGender"
local Object = import "UnityEngine.Object"
local UITableTween = import "L10.UI.UITableTween"

-- EnumAppearanceType
EnumAppearanceType = {
    Fashion = 1,
    Wing = 2,
    Luminous = 3,
    Talisman = 4,
    HairColor = 5,
    SkillAppear = 6
}

LuaNewPlayerAppearanceInfoWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaNewPlayerAppearanceInfoWnd, "NameLabel", "NameLabel", UILabel)
RegistChildComponent(LuaNewPlayerAppearanceInfoWnd, "clazz", "clazz", UISprite)
RegistChildComponent(LuaNewPlayerAppearanceInfoWnd, "scrollView", "scrollView", QnTableView)
RegistChildComponent(LuaNewPlayerAppearanceInfoWnd, "modelTexture", "modelTexture", UITexture)

--@endregion RegistChildComponent end
RegistClassMember(LuaNewPlayerAppearanceInfoWnd, "defaultModelLocalPos")
RegistClassMember(LuaNewPlayerAppearanceInfoWnd, "maxPreviewScale")
RegistClassMember(LuaNewPlayerAppearanceInfoWnd, "maxPreviewPos")
RegistClassMember(LuaNewPlayerAppearanceInfoWnd, "localModelScale")
RegistClassMember(LuaNewPlayerAppearanceInfoWnd, "identifier")
RegistClassMember(LuaNewPlayerAppearanceInfoWnd, "m_AppearanceInfos")
RegistClassMember(LuaNewPlayerAppearanceInfoWnd, "m_Info")
RegistClassMember(LuaNewPlayerAppearanceInfoWnd, "m_Rot")
RegistClassMember(LuaNewPlayerAppearanceInfoWnd, "m_PlayerGender")
RegistClassMember(LuaNewPlayerAppearanceInfoWnd, "m_PlayerCls")
RegistClassMember(LuaNewPlayerAppearanceInfoWnd, "m_NoneLab")
RegistClassMember(LuaNewPlayerAppearanceInfoWnd, "m_Cam")

function LuaNewPlayerAppearanceInfoWnd:Awake()
    self.m_NoneLab = self.transform:Find("Appearances/Label"):GetComponent(typeof(UILabel))
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.m_Rot = 180

    CommonDefs.AddOnDragListener(self.modelTexture.gameObject, DelegateFactory.Action_GameObject_Vector2(function (go, delta)
		self:OnScreenDrag(go, delta)
    end), false)
    
    
    self.scrollView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_AppearanceInfos
        end,
        function(item, index)
            self:InitItem(item, self.m_AppearanceInfos[index+1])
        end
    )

    self.scrollView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        self:OnSelectAtRow(row)
    end)

    self.scrollView.transform:Find("Grid"):GetComponent(typeof(UITableTween)).enabled = true
    self.identifier = SafeStringFormat3("__%s__", tostring(self.modelTexture.gameObject:GetInstanceID()))
    self:InitModelTransformData()
end

function LuaNewPlayerAppearanceInfoWnd:Init()
    LuaAppearancePreviewMgr.InitTalismanData()
    LuaAppearancePreviewMgr.InitRanFaJiData()

    if LuaPlayerInfoMgr.ShowApperance and LuaPlayerInfoMgr.PlayerApperanceProp then
        self.m_Info = LuaPlayerInfoMgr.PlayerApperanceProp
    elseif CPlayerInfoMgr.PlayerInfo then
        self.m_Info = CPlayerInfoMgr.PlayerInfo
    end
    if self.m_Info then
        self.m_PlayerGender = self.m_Info.gender
        self.m_PlayerCls = self.m_Info.cls
        self.NameLabel.text = self.m_Info.name
        self.clazz.spriteName = Profession.GetIconByNumber(self.m_Info.cls)
    end

    self:InitModel()
    self:InitAppearances()
end

function LuaNewPlayerAppearanceInfoWnd:InitModelTransformData()
    self.defaultModelLocalPos = Vector3.zero
    self.defaultModelLocalPos.x = -0.6
    self.defaultModelLocalPos.y = -1
    self.defaultModelLocalPos.z = 5
    self.localModelScale = 1 
    self.m_Rot = 180
    self.maxPreviewScale = 1.5
end

function LuaNewPlayerAppearanceInfoWnd:RefreshUIModelPreviewListener()
    if self.maxPreviewPos then
        LuaUIModelPreviewMgr:AddModelPreviewScaleTransformListener(self.identifier,
        self.defaultModelLocalPos,self.localModelScale,
        self.maxPreviewScale,self.maxPreviewPos,
        CLuaUIResources.NewPlayerAppearanceInfoWnd,true)
    end
end

function LuaNewPlayerAppearanceInfoWnd:SetModelTransformData(class,gender,ro)
    if class == EnumClass.YingLing and gender == EnumGender.Monster then 
        self.maxPreviewScale = 2
    else
        self.maxPreviewScale = 4
    end
    if ro:IsAllFinished() then
        local Headtransform = ro:GetSlotTransform("Head",false)
        self.maxPreviewPos = Vector3.zero
        self.maxPreviewPos.y = -1 * Headtransform.localPosition.y
        self.maxPreviewPos.x = self.defaultModelLocalPos.x / self.maxPreviewScale
        self.maxPreviewPos.z = self.defaultModelLocalPos.z / self.maxPreviewScale
        self:RefreshUIModelPreviewListener()
    else
        ro:AddLoadAllFinishedListener(DelegateFactory.Action_CRenderObject(function(renderObject)
            local Headtransform = renderObject:GetSlotTransform("Head",false)
            self.maxPreviewPos = Vector3.zero
            self.maxPreviewPos.y = -1 * Headtransform.localPosition.y
            self.maxPreviewPos.x = self.defaultModelLocalPos.x / self.maxPreviewScale
            self.maxPreviewPos.z = self.defaultModelLocalPos.z / self.maxPreviewScale
            self:RefreshUIModelPreviewListener()
        end))
    end
end

function LuaNewPlayerAppearanceInfoWnd:OnEnable()
    if not CommonDefs.IsInMobileDevice() then
        g_ScriptEvent:AddListener("OnWinScreenChangeAfterResolutionCheck", self, "UpdateModelTexture")
    else
        g_ScriptEvent:AddListener("OnScreenChange", self, "UpdateModelTexture")
    end
    self:RefreshUIModelPreviewListener()
end

function LuaNewPlayerAppearanceInfoWnd:OnDisable()
    if not CommonDefs.IsInMobileDevice() then
        g_ScriptEvent:RemoveListener("OnWinScreenChangeAfterResolutionCheck", self, "UpdateModelTexture")
    else
        g_ScriptEvent:RemoveListener("OnScreenChange", self, "UpdateModelTexture")
    end
    LuaPlayerInfoMgr.ShowApperance = false
    LuaUIModelPreviewMgr:RemoveModelPreviewScaleTransformListener(self.identifier)
end

--@region UIEvent
function LuaNewPlayerAppearanceInfoWnd:OnScreenDrag(go, delta)
    local rot = -delta.x / Screen.width * 360
    self.m_Rot = self.m_Rot + rot
    self:SetModelRot(self.m_Rot)
end
--@endregion UIEvent
function LuaNewPlayerAppearanceInfoWnd:InitAppearances()
    if not self.m_Info then
        return
    end
    local appearanceProp = self.m_Info.appearanceProp
    self.m_AppearanceInfos = {}
    -- 头部时装
    if appearanceProp.HideHeadFashionEffect ~= 1 and appearanceProp.HeadFashionId ~= 0 then
        table.insert(self.m_AppearanceInfos, {type = EnumAppearanceType.Fashion, info = {FashionId = appearanceProp.HeadFashionId, FashionPosition = 0}})
    end
    -- 身体时装
    if appearanceProp.HideBodyFashionEffect ~= 1 and appearanceProp.BodyFashionId ~= 0 then
        table.insert(self.m_AppearanceInfos, {type = EnumAppearanceType.Fashion, info = {FashionId = appearanceProp.BodyFashionId, FashionPosition = 1}})
    end
    -- 武器拓本
    if appearanceProp.HideWeaponFashionEffect ~= 1 and appearanceProp.WeaponFashionId ~= 0 then
        table.insert(self.m_AppearanceInfos, {type = EnumAppearanceType.Fashion, info = {FashionId = appearanceProp.WeaponFashionId, FashionPosition = 2}})
    end
    -- 背饰时装
    if appearanceProp.HideBackPendantFashionEffect ~= 1 and appearanceProp.BackPendantFashionId ~= 0 then
        table.insert(self.m_AppearanceInfos, {type = EnumAppearanceType.Fashion, info = {FashionId = appearanceProp.BackPendantFashionId, FashionPosition = 3}})
    end
    -- 翅膀
    if appearanceProp.HideWing ~= 1 and appearanceProp.Wing ~= 0 then
        table.insert(self.m_AppearanceInfos, {type = EnumAppearanceType.Wing, info = {WingId = appearanceProp.Wing}})
    end
    -- 强化光效
    if appearanceProp.HideSuitIdToOtherPlayer ~= 1 and appearanceProp.IntesifySuitId ~= 0 then
        local designId = appearanceProp.IntesifySuitId % 10000
        local fxIndex = math.floor(appearanceProp.IntesifySuitId / 10000)
        local suit = EquipIntensify_Suit.GetData(designId)
        local colorName = fxIndex == 1 and suit.SecondColorName or suit.ColorName
        if colorName and colorName ~= "" then
            table.insert(self.m_AppearanceInfos, {type = EnumAppearanceType.Luminous, info = {SuitId = appearanceProp.IntesifySuitId, IsGemGroupFx = false}})
        end
    end
    -- 石之灵光效
    if appearanceProp.HideGemFxToOtherPlayer ~= 1 then
        local haveGemFx = false
        local equipmentGemFx = appearanceProp.EquipmentGemFx
        if equipmentGemFx then
            for i = 0, equipmentGemFx.Length - 1 do
                local gemFx = equipmentGemFx[i]
                if gemFx == 1 or gemFx == 2 or gemFx == 3 then
                    haveGemFx = true
                    break
                end
            end
        end
        if haveGemFx then
            table.insert(self.m_AppearanceInfos, {type = EnumAppearanceType.Luminous, info = {IsGemGroupFx = true}})
        end
    end
    -- 法宝外观
    if appearanceProp.TalismanAppearance ~= 0 then
        table.insert(self.m_AppearanceInfos, {type = EnumAppearanceType.Talisman, info = {TalismanAppearance = appearanceProp.TalismanAppearance}})
    end
    -- 发色
    if appearanceProp.AdvancedHairColor ~= "" or appearanceProp.AdvancedHairColorId ~= 0 then
        table.insert(self.m_AppearanceInfos, {type = EnumAppearanceType.HairColor, info = {ColorRGB = appearanceProp.AdvancedHairColor, haircolorId = appearanceProp.AdvancedHairColorId}})
    end
    -- 技能外观
    if appearanceProp.SkillAppearId ~= 0 and appearanceProp.SkillAppearExpiredTime > CServerTimeMgr.Inst.timeStamp  then
        table.insert(self.m_AppearanceInfos, {type = EnumAppearanceType.SkillAppear, info = {SkillAppearId = appearanceProp.SkillAppearId}})
    end

    self.m_NoneLab.gameObject:SetActive(#self.m_AppearanceInfos == 0)
    self.scrollView:ReloadData(true, false)
end

-- Init
function LuaNewPlayerAppearanceInfoWnd:InitItem(item, appearanceInfo)
    local type = appearanceInfo.type
    local info = appearanceInfo.info
    if type == EnumAppearanceType.Fashion then
        self:InitFashionItem(item, info)
    elseif type == EnumAppearanceType.Wing then
        self:InitWingItem(item, info)
    elseif type == EnumAppearanceType.Luminous then
        self:InitLuminousItem(item, info)
    elseif type == EnumAppearanceType.Talisman then
        self:InitTalismanItem(item, info)
    elseif type == EnumAppearanceType.HairColor then
        self:InitHairColorItem(item, info)
    elseif type == EnumAppearanceType.SkillAppear then
        self:InitSkillAppearItem(item, info)
    end
end

function LuaNewPlayerAppearanceInfoWnd:InitWingItem(item, info)
    local normal = item.transform:Find("normal"):GetComponent(typeof(CUITexture))
    local nameLabel = item.transform:Find("nameLabel"):GetComponent(typeof(UILabel))
    local typeLabel = item.transform:Find("typeLabel"):GetComponent(typeof(UILabel))

    -- 翅膀
    local wingId = info.WingId
    local wing = Wing_Wing.GetData(wingId)
    if self.m_PlayerGender == EnumGender_lua.Male then
        normal:LoadMaterial(wing.Icon)
    else
        normal:LoadMaterial(wing.FemaleIcon)
    end
    nameLabel.text = wing.Name
    typeLabel.text = LocalString.GetString("翅膀")
end

function LuaNewPlayerAppearanceInfoWnd:InitFashionItem(item, info)
    local normal = item.transform:Find("normal"):GetComponent(typeof(CUITexture))
    local nameLabel = item.transform:Find("nameLabel"):GetComponent(typeof(UILabel))
    local typeLabel = item.transform:Find("typeLabel"):GetComponent(typeof(UILabel))

    -- 时装
    local basicFashionId = info.FashionId 
    local fashion = Fashion_Fashion.GetData(basicFashionId)
    local taben = EquipmentTemplate_Equip.GetData(basicFashionId)

    if not fashion then
        if taben then
            local tabenType = CommonDefs.ConvertIntToEnum(typeof(EnumBodyPosition), taben.Type)
            if tabenType == EnumBodyPosition.Casque then
                fashion = Fashion_Fashion.GetData((Fashion_Setting.GetData("Taben_Head_Template_Id").Value))
            elseif tabenType == EnumBodyPosition.Armour then
                fashion = Fashion_Fashion.GetData((Fashion_Setting.GetData("Taben_Body_Template_Id").Value))
            elseif tabenType == EnumBodyPosition.Weapon then
                fashion = Fashion_Fashion.GetData(45999997)
            elseif tabenType == EnumBodyPosition.BackPendant then
                fashion = Fashion_Fashion.GetData((Fashion_Setting.GetData("Taben_BackPendant_Template_Id").Value))
            else
                return
            end
        else
            return
        end
    end

    if not taben then
        normal:LoadMaterial(fashion.Icon)
        nameLabel.text = fashion.Name
    else
        normal:LoadMaterial(taben.Icon)
        nameLabel.text = SafeStringFormat3(LocalString.GetString("%s·拓本"), taben.Name)
    end
    local position = info.FashionPosition
    if position == 0 then
        typeLabel.text = LocalString.GetString("头部")
    elseif position == 1 then
        typeLabel.text = LocalString.GetString("身体")
    elseif position == 2 then
        typeLabel.text = LocalString.GetString("武器")
    elseif position == 3 then
        typeLabel.text = LocalString.GetString("背饰")
    end

end

function LuaNewPlayerAppearanceInfoWnd:InitLuminousItem(item, info)
    local normal = item.transform:Find("normal"):GetComponent(typeof(CUITexture))
    local nameLabel = item.transform:Find("nameLabel"):GetComponent(typeof(UILabel))
    local typeLabel = item.transform:Find("typeLabel"):GetComponent(typeof(UILabel))

    -- 光效
    local suitId = info.SuitId
    local isGemGroupFx = info.IsGemGroupFx
    if not isGemGroupFx then
        -- 强化光效
        local designId = suitId % 10000
        local fxIndex = math.floor(suitId / 10000)
        local suit = EquipIntensify_Suit.GetData(designId)
        local colorName = suit.ColorName
        local suitFX = suit.SuitFX
        local icon = suit.ColorIcon
        if fxIndex == 1 then
            colorName = suit.SecondColorName
            suitFX = suit.SecondSuitFX
            icon = suit.SecondColorIcon
        end
        normal:LoadMaterial(icon)
        nameLabel.text = colorName
    else
        -- 石之灵光效
        nameLabel.text = LocalString.GetString("石之灵光效")
        local icon = LuaAppearancePreviewMgr.GemGroupIcon
        normal:LoadMaterial(icon)
    end
    typeLabel.text = LocalString.GetString("光效")
end

function LuaNewPlayerAppearanceInfoWnd:InitTalismanItem(item, info)
    local normal = item.transform:Find("normal"):GetComponent(typeof(CUITexture))
    local nameLabel = item.transform:Find("nameLabel"):GetComponent(typeof(UILabel))
    local typeLabel = item.transform:Find("typeLabel"):GetComponent(typeof(UILabel))

    -- 法宝
    local talismanId = info.TalismanAppearance
    local talismanFxId = info.TalismanAppearance
    talismanFxId = CTalismanMgr.Inst:FindTalismanFx(talismanId)

    local fx = Talisman_FXOrder.GetData(talismanFxId)
    local icon --=default默认
    local itFx = ItemGet_TalismanFX.GetData(talismanFxId)

    if fx then
        nameLabel.text = NGUIText.StripSymbols(CChatLinkMgr.TranslateToNGUIText(fx.Description,false))
    elseif itFx then
        nameLabel.text = NGUIText.StripSymbols(CChatLinkMgr.TranslateToNGUIText(itFx.Name,false))
    end

    icon = LuaAppearancePreviewMgr.GetTalismanAppearanceIconUseJob(talismanId, talismanFxId, self.m_PlayerCls)

    normal:LoadMaterial(icon)
    typeLabel.text = LocalString.GetString("法宝")
end

function LuaNewPlayerAppearanceInfoWnd:InitSkillAppearItem(item, info)
    local normal = item.transform:Find("normal"):GetComponent(typeof(CUITexture))
    local nameLabel = item.transform:Find("nameLabel"):GetComponent(typeof(UILabel))
    local typeLabel = item.transform:Find("typeLabel"):GetComponent(typeof(UILabel))

    -- 技能外观
    local skillAppearId = info.SkillAppearId
    local data = SkillAppearance_SkillAppearance.GetData(skillAppearId)
    normal:LoadMaterial(data.Icon)
    nameLabel.text = data.Name
    typeLabel.text = LocalString.GetString("技能")
end

function LuaNewPlayerAppearanceInfoWnd:InitHairColorItem(item, info)
    local hairColor = item.transform:Find("normal"):GetComponent(typeof(CUITexture))
    local nameLabel = item.transform:Find("nameLabel"):GetComponent(typeof(UILabel))
    local typeLabel = item.transform:Find("typeLabel"):GetComponent(typeof(UILabel))

    -- 发色
    local colorRGB = info.ColorRGB
    local id = info.haircolorId

    if colorRGB and colorRGB ~= "" then
        id = LuaAppearancePreviewMgr.GetRanFaJiIdByColorRGB(colorRGB)
    end

    local designData = RanFaJi_RanFaJi.GetData(id)
    nameLabel.text = SafeStringFormat3(LocalString.GetString("%s染发剂"), designData.ColorName)
    typeLabel.text = LocalString.GetString("发色")
    hairColor:LoadMaterial(designData.Icon)
end

-- OnSelect
function LuaNewPlayerAppearanceInfoWnd:OnSelectAtRow(row)
    local appearanceInfo = self.m_AppearanceInfos[row+1]
    local type = appearanceInfo.type
    local info = appearanceInfo.info

    if type == EnumAppearanceType.Fashion then
        self:OnSelectFashionItem(info)
    elseif type == EnumAppearanceType.Wing then
        self:OnSelectWingItem(info)
    elseif type == EnumAppearanceType.Luminous then
        self:OnSelectLuminousItem(info)
    elseif type == EnumAppearanceType.Talisman then
        self:OnSelectTalismanItem(info)
    elseif type == EnumAppearanceType.HairColor then
        self:OnSelectHairColorItem(info)
    elseif type == EnumAppearanceType.SkillAppear then
        self:OnSelectSkillAppearItem(info)
    end
end

function LuaNewPlayerAppearanceInfoWnd:OnSelectWingItem(info)
    local wingId = info.WingId
    local wing = Wing_Wing.GetData(wingId)
    if self.m_PlayerGender == EnumGender_lua.Male then
        if wing.ItemId ~= 0 then
            CItemInfoMgr.ShowLinkItemTemplateInfo(wing.ItemId, false, nil, AlignType.Default, 0, 0, 0, 0)
        end
    else
        if wing.FemaleItemId ~= 0 then
            CItemInfoMgr.ShowLinkItemTemplateInfo(wing.FemaleItemId, false, nil, AlignType.Default, 0, 0, 0, 0)
        end
    end
end

function LuaNewPlayerAppearanceInfoWnd:OnSelectFashionItem(info)
    local basicFashionId = info.FashionId 
    local fashion = Fashion_Fashion.GetData(basicFashionId)
    local taben = EquipmentTemplate_Equip.GetData(basicFashionId)

    if fashion then
        LuaFashionInfoMgr.ShowFashionTemplateInfo(basicFashionId, AlignType.Default, 0, 0, 0, 0)
    elseif taben then
        self:ShowTabenInfo(taben.ID)
    end
end

function LuaNewPlayerAppearanceInfoWnd:ShowTabenInfo(templateId)
    if CItemMgr.Inst:GetEquipTemplate(templateId) ~= nil then
        CItemInfoMgr.showType = ShowType.Link
        CItemInfoMgr.linkItemInfo = LinkItemInfo(templateId, false, nil)
        CItemInfoMgr.linkItemInfo.alignType = AlignType.Default
        CItemInfoMgr.linkItemInfo.targetSize = Vector2(0, 0)
        CItemInfoMgr.linkItemInfo.targetCenterPos = Vector3(0, 0)
        CItemInfoMgr.linkItemInfo.isBind = false
        CItemInfoMgr.linkItemExtraUD = nil
        CUIManager.ShowUI(CIndirectUIResources.EquipInfoWnd)
    end
end

function LuaNewPlayerAppearanceInfoWnd:OnSelectLuminousItem(info)
    local suitId = info.SuitId
    local isGemGroupFx = info.IsGemGroupFx

    if not isGemGroupFx then
        local designId = suitId % 10000
        local fxIndex = math.floor(suitId / 10000)
        local suit = EquipIntensify_Suit.GetData(designId)
        local itemId = fxIndex == 1 and suit.SecondItemID or suit.ItemID
        if itemId ~= 0 then
            CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType.Default, 0, 0, 0, 0)
        end
    else
        CItemInfoMgr.ShowLinkItemTemplateInfo(21052281, false, nil, AlignType.Default, 0, 0, 0, 0)
    end
end

function LuaNewPlayerAppearanceInfoWnd:OnSelectTalismanItem(info)
    local talismanId = info.TalismanAppearance
    local talismanFxId = info.TalismanAppearance

    talismanFxId = CTalismanMgr.Inst:FindTalismanFx(talismanId)

    local fx = Talisman_FXOrder.GetData(talismanFxId)
    local itFx = ItemGet_TalismanFX.GetData(talismanFxId)

    if fx then
        if fx.ItemId ~= 0 then
            CItemInfoMgr.ShowLinkItemTemplateInfo(fx.ItemId, false, nil, AlignType.Default, 0, 0, 0, 0)
        end
    elseif itFx then
        if itFx.ItemID ~= 0 then
            CItemInfoMgr.ShowLinkItemTemplateInfo(itFx.ItemID, false, nil, AlignType.Default, 0, 0, 0, 0)
        end
    end
end

function LuaNewPlayerAppearanceInfoWnd:OnSelectHairColorItem(info)
    local colorRGB = info.ColorRGB
    local id = LuaAppearancePreviewMgr.GetRanFaJiIdByColorRGB(colorRGB)
    if info.haircolorId ~= 0 then
        id = info.haircolorId
    end
    local itemId = RanFaJi_RanFaJi.GetData(id).RanFaJiItemID
    CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType.Default, 0, 0, 0, 0)
end

function LuaNewPlayerAppearanceInfoWnd:OnSelectSkillAppearItem(info)
    local skillAppearId = info.SkillAppearId
    local data = SkillAppearance_SkillAppearance.GetData(skillAppearId)
    if data.ItemID ~= 0 then
        CItemInfoMgr.ShowLinkItemTemplateInfo(data.ItemID, false, nil, AlignType.Default, 0, 0, 0, 0)
    end
end

function LuaNewPlayerAppearanceInfoWnd:InitModel()
    if self.m_Info == nil then
        return
    end

    self.m_Rot = 180
    CUIManager.CreateModelTexture(self.identifier, LuaDefaultModelTextureLoader.Create(function (ro)
        self.m_Cam = CUIManager.instance.transform:Find(self.identifier):GetComponent(typeof(Camera))
        self.m_Cam.farClipPlane = 50
        self.modelTexture.mainTexture = self:UpdateModelTexture()
        self:CreateModel(ro)
    end), self.m_Rot, self.defaultModelLocalPos.x, self.defaultModelLocalPos.y, self.defaultModelLocalPos.z, false, true, 1.0, true, false)

    self:SetModelRot(self.m_Rot)
end

function LuaNewPlayerAppearanceInfoWnd:CreateModel(renderObj)
    if self.m_Info == nil then
        return
    end

    local appearanceData = self.m_Info.appearanceProp
    if appearanceData ~= nil then
        appearanceData.ResourceId = 0
        CClientMainPlayer.LoadResource(renderObj, appearanceData, true, 1, 0, false, appearanceData.ResourceId, false, 0, false, nil)

        if appearanceData.ResourceId ~= 0 then 
            local scale = 1
            local data = Transform_Transform.GetData(appearanceData.ResourceId)
            if data then
                scale = data.PreviewScale
            end
            renderObj.Scale = scale
        else
            self:SetModelTransformData(appearanceData.Class,appearanceData.Gender,renderObj)
        end
    end

    CSharpResourceLoader.Inst:LoadGameObject("Levels/Dynamic/yichu_shangchengbg.prefab", DelegateFactory.Action_GameObject(function(obj)
        local go = GameObject.Instantiate(obj, Vector3.zero, Quaternion.identity)
        NGUITools.SetLayer(go, LayerDefine.ModelForNGUI_3D)
        go.transform.parent = renderObj.transform.parent
        go.transform.localPosition = Vector3(45.65, 0, -131.7)
        go.transform.localRotation = Quaternion.identity
        go.transform.localScale = Vector3.one
        go.transform.parent = go.transform.parent.parent
    end))
end

function LuaNewPlayerAppearanceInfoWnd:SetModelRot(rotY)
    CUIManager.SetModelRotation(self.identifier, rotY)
end

function LuaNewPlayerAppearanceInfoWnd:UpdateModelTexture()
    if self.m_Cam == nil then
        return
    end
    local rt = self.m_Cam.targetTexture
    if rt then
        Object.Destroy(rt)
    end

    local screenHeight = PlayerSettings.s_EnableHighResolutionLimit and PlayerSettings.s_HighResolutionLimit or CommonDefs.GameScreenHeight
    local screenWidth = PlayerSettings.s_EnableHighResolutionLimit and (CUIManager.ScreenWidthScale * screenHeight * Screen.width / Screen.height) or CommonDefs.GameScreenWidth

    if screenHeight < 720 then
        local ratio = 720 / screenHeight
        screenHeight = 720
        screenWidth = screenWidth * ratio
    end

    rt = RenderTexture(screenWidth, screenHeight, 24, RenderTextureFormat.ARGB32, RenderTextureReadWrite.Default)
    rt.wrapMode = TextureWrapMode.Clamp
    rt.filterMode = FilterMode.Bilinear
    rt.anisoLevel = 2
    rt.name = self.m_Cam.name
    self.m_Cam.targetTexture = rt

    if self.modelTexture then
        self.modelTexture.mainTexture = rt
    end

    self.m_Cam:Render()
    return rt
end

function LuaNewPlayerAppearanceInfoWnd:OnDestroy()
    self.modelTexture.mainTexture = nil
    CUIManager.DestroyModelTexture(self.identifier)
end
