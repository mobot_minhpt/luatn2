local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CClientObjectMgr=import "L10.Game.CClientObjectMgr"
LuaDaFuWongPopMapWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaDaFuWongPopMapWnd, "PlayerNode", "PlayerNode", GameObject)
RegistChildComponent(LuaDaFuWongPopMapWnd, "LandNode", "LandNode", GameObject)
RegistChildComponent(LuaDaFuWongPopMapWnd, "PlayerTemplate", "PlayerTemplate", GameObject)
RegistChildComponent(LuaDaFuWongPopMapWnd, "LandTemplate", "LandTemplate", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaDaFuWongPopMapWnd, "m_PlayerView")
RegistClassMember(LuaDaFuWongPopMapWnd, "m_LandView")
RegistClassMember(LuaDaFuWongPopMapWnd, "m_headIconName")
RegistClassMember(LuaDaFuWongPopMapWnd, "m_setNum")
RegistClassMember(LuaDaFuWongPopMapWnd, "m_ColorList")
RegistClassMember(LuaDaFuWongPopMapWnd, "m_MoveTick")
RegistClassMember(LuaDaFuWongPopMapWnd, "m_MaxMoveTime")
function LuaDaFuWongPopMapWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.m_PlayerView = nil
	self.PlayerTemplate.gameObject:SetActive(false)
	self.LandTemplate.gameObject:SetActive(false)
    self.m_headIconName = {"dafuwongpopmapwnd_daditu_touxiangkuang_y","dafuwongpopmapwnd_daditu_touxiangkuang_r","dafuwongpopmapwnd_daditu_touxiangkuang_p","dafuwongpopmapwnd_daditu_touxiangkuang_g"}
	self.m_ColorList = {"F6D05E","E75656","A55DC9","3BB86D"}
	self.m_ColorList[0] = "8C9FBA"
	self.m_MaxMoveTime = DaFuWeng_Countdown.GetData(EnumDaFuWengStage.RoundMove).Time + 1
	self.m_MoveTick = nil
	self.m_setNum = 5
end
function LuaDaFuWongPopMapWnd:Init()
	self:InitLandDot()
	self:OnStageUpdate(LuaDaFuWongMgr.CurStage)
end
-- function LuaDaFuWongPopMapWnd:Update()
-- 	if not self.m_PlayerView then self:InitPlayerViewList() return end
-- 	for k,v in pairs(self.m_PlayerView) do
-- 		self:SetPlayerViewPos(k,v)
-- 	end
-- end

function LuaDaFuWongPopMapWnd:InitPlayerViewList()
	if not LuaDaFuWongMgr.PlayerInfo then return end
	self.m_PlayerView = {}
	Extensions.RemoveAllChildren(self.PlayerNode.transform)
	for k,v in pairs(LuaDaFuWongMgr.PlayerInfo) do
		local go = CUICommonDef.AddChild(self.PlayerNode.gameObject,self.PlayerTemplate)
		go.gameObject:SetActive(true)
		self:SetHeadDepth(go,v.round)
        go.transform:Find("Anchor/PlayerHeadTemplate/BG"):GetComponent(typeof(CUITexture)):LoadMaterial(self:GetHeadBgPath(v.round))
        local headIcon = go.transform:Find("Anchor/PlayerHeadTemplate/Portrait")
        headIcon:GetComponent(typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(v.class, v.gender, -1), false)
        UIEventListener.Get(headIcon.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnHeadIconClick(k)
        end)
		self.m_PlayerView[k] = go
		self:SetPlayerViewPos(k,go,false)
	end
	
end
function LuaDaFuWongPopMapWnd:SetHeadDepth(go,index)
	local depth = {30,35,30}
	local widgetList = CommonDefs.GetComponentsInChildren_Component_Type(go.transform,typeof(UIWidget))
    for i = 0,widgetList.Length - 1 do
        widgetList[i].depth = depth[i + 1] + (index - 1) * 10
    end
end

function LuaDaFuWongPopMapWnd:InitLandDot()
	if not LuaDaFuWongMgr.LandInfo then return end
	self.m_LandView = {}
	Extensions.RemoveAllChildren(self.LandNode.transform)
	for k,v in pairs(LuaDaFuWongMgr.LandInfo) do
		local go = CUICommonDef.AddChild(self.LandNode.gameObject,self.LandTemplate)
		go.gameObject:SetActive(true)
		local x,y = self:GetMapPos(v.movePos[0],v.movePos[1])
		LuaUtils.SetLocalPosition(go.transform, x, y, 0)
		local num = go.transform:Find("Label"):GetComponent(typeof(UILabel))
		local dot = go.transform:Find("dian"):GetComponent(typeof(UITexture))
		local shadow = go.transform:Find("dian_youying").gameObject
		if k % self.m_setNum == 0 then 
			num.text = k
		else
			num.text = nil
		end
		if v.Owner and v.Owner ~= 0 and LuaDaFuWongMgr.PlayerInfo and LuaDaFuWongMgr.PlayerInfo[v.Owner] then
			shadow.gameObject:SetActive(true)
			dot.color = NGUIText.ParseColor24(self.m_ColorList[LuaDaFuWongMgr.PlayerInfo[v.Owner].round], 0)
		else
			shadow.gameObject:SetActive(false)
			dot.color = NGUIText.ParseColor24(self.m_ColorList[0], 0)
		end
		self.m_LandView[k] = {go = go,dot = dot,shadow = shadow}
	end
end

function LuaDaFuWongPopMapWnd:GetHeadBgPath(index)
    return SafeStringFormat3("UI/Texture/FestivalActivity/Festival_DaFuWong/Material/%s.mat",self.m_headIconName[index])
end
function LuaDaFuWongPopMapWnd:SetPlayerViewPos(id,go,isMove)
	if not LuaDaFuWongMgr.PlayerInfo then return end
	local obj = CClientObjectMgr.Inst and CClientObjectMgr.Inst:GetObject(LuaDaFuWongMgr.PlayerInfo[id].fakePlayerId)
	local landId = LuaDaFuWongMgr.PlayerInfo[id].Pos
	local landPos = LuaDaFuWongMgr.LandInfo[landId] and LuaDaFuWongMgr.LandInfo[landId].movePos
	if obj and isMove then
		local landId = LuaDaFuWongMgr.PlayerInfo[id].Pos
		local landPos = LuaDaFuWongMgr.LandInfo[landId] and LuaDaFuWongMgr.LandInfo[landId].movePos
		local pixelPos =  Utility.WorldPos2PixelPos(obj.RO:GetPosition())
		local x,y = self:GetMapPos(pixelPos.x,pixelPos.y)
		go.gameObject:SetActive(true)
		LuaUtils.SetLocalPosition(go.transform, x, y, 0)
	elseif LuaDaFuWongMgr.PlayerInfo[id].IsOut then
		go.gameObject:SetActive(false)
	else
		if landPos then
			local x,y = self:GetMapPos(landPos[0],landPos[1])
			go.gameObject:SetActive(true)
			LuaUtils.SetLocalPosition(go.transform, x, y, 0)
		else
			go.gameObject:SetActive(false)
		end
	end
end

function LuaDaFuWongPopMapWnd:GetMapPos(x,y)
	return (x - 7232)* -0.119,(y - 5792)  * -0.1127
end

function LuaDaFuWongPopMapWnd:OnHeadIconClick(id)
    if LuaDaFuWongMgr.CanMoveCameraStage and LuaDaFuWongMgr.CanMoveCameraStage[LuaDaFuWongMgr.CurStage] then
        local obj = CClientObjectMgr.Inst and CClientObjectMgr.Inst:GetObject(LuaDaFuWongMgr.PlayerInfo[id].fakePlayerId)
        local landId = LuaDaFuWongMgr.PlayerInfo[id].Pos
        local landPos = LuaDaFuWongMgr.LandInfo[landId] and LuaDaFuWongMgr.LandInfo[landId].movePos
        local targetPos = nil
        if obj then
            targetPos = obj.RO:GetPosition()
        else
            targetPos = Utility.PixelPos2WorldPos(landPos[0], landPos[1])
        end
		LuaDaFuWongMgr:OnDragCamera({x = 0,y = 0})
        if targetPos ~= nil and LuaDaFuWongMgr.DragCameraRo then
            LuaDaFuWongMgr:MoveROToTarget(LuaDaFuWongMgr.DragCameraRo,targetPos,nil)
            CUIManager.CloseUI(CLuaUIResources.DaFuWongPopMapWnd)
        end
    end
end

function LuaDaFuWongPopMapWnd:OnLandInfoUpdate(landId)
	if not self.m_LandView or not self.m_LandView[landId] then return end
	local ownerId = LuaDaFuWongMgr.LandInfo[landId] and LuaDaFuWongMgr.LandInfo[landId].Owner or 0
	local view = self.m_LandView[landId]
	if ownerId ~= 0 and LuaDaFuWongMgr.PlayerInfo and LuaDaFuWongMgr.PlayerInfo[ownerId] then
		view.shadow.gameObject:SetActive(true)
		view.dot.color = NGUIText.ParseColor24(self.m_ColorList[LuaDaFuWongMgr.PlayerInfo[ownerId].round], 0)
	else
		view.shadow.gameObject:SetActive(false)
		view.dot.color = NGUIText.ParseColor24(self.m_ColorList[0], 0)
	end
end
function LuaDaFuWongPopMapWnd:OnStageUpdate(curStage)
	if not self.m_LandView then self:InitLandDot() end
	if not self.m_PlayerView then self:InitPlayerViewList() return end
	
	if self.m_MoveTick then UnRegisterTick(self.m_MoveTick) self.m_MoveTick = nil end
	if self.m_PlayerView then
		for k,v in pairs(self.m_PlayerView) do
			self:SetPlayerViewPos(k,v,LuaDaFuWongMgr.CanMoveCameraStage and (not LuaDaFuWongMgr.CanMoveCameraStage[EnumDaFuWengStage.RoundMove]))
			if curStage == EnumDaFuWengStage.RoundMove and k == LuaDaFuWongMgr.CurPlayerRound then
				self.m_MoveTick = RegisterTickWithDuration(function()
					self:SetPlayerViewPos(k,v,LuaDaFuWongMgr.CanMoveCameraStage and (not LuaDaFuWongMgr.CanMoveCameraStage[EnumDaFuWengStage.RoundMove]))
				end,200,self.m_MaxMoveTime * 1000)
			end
		end
	end
end
function LuaDaFuWongPopMapWnd:OnEnable()
	g_ScriptEvent:AddListener("OnDaFuWongLandInfoUpdate",self,"OnLandInfoUpdate")
	g_ScriptEvent:AddListener("OnDaFuWengStageUpdate",self,"OnStageUpdate")
end

function LuaDaFuWongPopMapWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnDaFuWongLandInfoUpdate",self,"OnLandInfoUpdate")
	g_ScriptEvent:RemoveListener("OnDaFuWengStageUpdate",self,"OnStageUpdate")
	if self.m_MoveTick then UnRegisterTick(self.m_MoveTick) self.m_MoveTick = nil end
end

--@region UIEvent

--@endregion UIEvent

