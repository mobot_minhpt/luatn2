local UISprite = import "UISprite"
local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"
local Profession = import "L10.Game.Profession"
local Animation = import "UnityEngine.Animation"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local CTooltip = import "L10.UI.CTooltip"
local CRankData = import "L10.UI.CRankData"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaSnowAdventureSingleStageMainWnd = class()
LuaSnowAdventureSingleStageMainWnd.FirstOpenStageId = nil
--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaSnowAdventureSingleStageMainWnd, "EasyModeButton", "EasyModeButton", GameObject)
RegistChildComponent(LuaSnowAdventureSingleStageMainWnd, "Difficulty", "Difficulty", GameObject)
RegistChildComponent(LuaSnowAdventureSingleStageMainWnd, "Rewards", "Rewards", GameObject)
RegistChildComponent(LuaSnowAdventureSingleStageMainWnd, "RewardButton", "RewardButton", GameObject)
RegistChildComponent(LuaSnowAdventureSingleStageMainWnd, "progressSprite", "progressSprite", UITexture)
RegistChildComponent(LuaSnowAdventureSingleStageMainWnd, "RewardName", "RewardName", UILabel)
RegistChildComponent(LuaSnowAdventureSingleStageMainWnd, "RewardTips", "RewardTips", UILabel)
RegistChildComponent(LuaSnowAdventureSingleStageMainWnd, "Route", "Route", GameObject)
RegistChildComponent(LuaSnowAdventureSingleStageMainWnd, "EasyRoute", "EasyRoute", GameObject)
RegistChildComponent(LuaSnowAdventureSingleStageMainWnd, "NormalRoute", "NormalRoute", GameObject)
RegistChildComponent(LuaSnowAdventureSingleStageMainWnd, "HardRoute", "HardRoute", GameObject)
RegistChildComponent(LuaSnowAdventureSingleStageMainWnd, "RemainLabel", "RemainLabel", UILabel)
RegistChildComponent(LuaSnowAdventureSingleStageMainWnd, "RuleButton", "RuleButton", GameObject)
RegistChildComponent(LuaSnowAdventureSingleStageMainWnd, "AdventureName", "AdventureName", UILabel)
RegistChildComponent(LuaSnowAdventureSingleStageMainWnd, "RankButton", "RankButton", GameObject)
RegistChildComponent(LuaSnowAdventureSingleStageMainWnd, "RankLabel", "RankLabel", UILabel)
RegistChildComponent(LuaSnowAdventureSingleStageMainWnd, "SnowAdventureStageDetailWnd", "SnowAdventureStageDetailWnd", GameObject)
RegistChildComponent(LuaSnowAdventureSingleStageMainWnd, "ChangeSnowManButton", "ChangeSnowManButton", GameObject)
RegistChildComponent(LuaSnowAdventureSingleStageMainWnd, "SnowManProIcon", "SnowManProIcon", UISprite)
RegistChildComponent(LuaSnowAdventureSingleStageMainWnd, "SnowManLevel", "SnowManLevel", UILabel)
RegistChildComponent(LuaSnowAdventureSingleStageMainWnd, "SnowManTexture", "SnowManTexture", CUITexture)
RegistChildComponent(LuaSnowAdventureSingleStageMainWnd, "NormalModeButton", "NormalModeButton", GameObject)
RegistChildComponent(LuaSnowAdventureSingleStageMainWnd, "HardModeButton", "HardModeButton", GameObject)

--@endregion RegistChildComponent end


function LuaSnowAdventureSingleStageMainWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    if CommonDefs.IS_VN_CLIENT then
        if LocalString.language == "vn" then
            self.transform:Find("AdventureName/snowadventuresinglestagemainwnd_logo_bg").localPosition = Vector3(-111, -36, 0)
        else
            self.transform:Find("AdventureName/snowadventuresinglestagemainwnd_logo_bg").localPosition = Vector3(-33, -36, 0)
        end
    end
    
    self:InitWndData()
    self:RefreshConstUI()
    self:RefreshVariableUI()
    self:InitUIEvent()
end

function LuaSnowAdventureSingleStageMainWnd:Init()
    Gac2Gas.RequestXuePoLiXianData()
    CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.HanJia2023SnowAdventureSingleGuide)
end

function LuaSnowAdventureSingleStageMainWnd:OnEnable()
    g_ScriptEvent:AddListener("HanJia2023_SyncXuePoLiXianData", self, "OnSyncXuePoLiXianData")
    g_ScriptEvent:AddListener("SelectSnowAdventureStage", self, "OnSelectSnowAdventureStage")
    g_ScriptEvent:AddListener("HanJia2023_EnterPlayScene", self, "OnEnterScene")
    g_ScriptEvent:AddListener("CloseSnowAdventureStageDetailWnd", self, "OnCloseSnowAdventureStageDetailWnd")
    g_ScriptEvent:AddListener("OnRankDataReady", self, "OnRankDataReady")

    if LuaSnowAdventureSingleStageMainWnd.FirstOpenStageId then
        local routeID = math.floor(LuaSnowAdventureSingleStageMainWnd.FirstOpenStageId / 100)
        self.selectRouteId = routeID
        self:RefreshRoute(self.selectRouteId)
        g_ScriptEvent:BroadcastInLua("SelectSnowAdventureStage", LuaSnowAdventureSingleStageMainWnd.FirstOpenStageId)
        LuaSnowAdventureSingleStageMainWnd.FirstOpenStageId = nil
    end
end

function LuaSnowAdventureSingleStageMainWnd:OnDisable()
    g_ScriptEvent:RemoveListener("HanJia2023_SyncXuePoLiXianData", self, "OnSyncXuePoLiXianData")
    g_ScriptEvent:RemoveListener("SelectSnowAdventureStage", self, "OnSelectSnowAdventureStage")
    g_ScriptEvent:RemoveListener("HanJia2023_EnterPlayScene", self, "OnEnterScene")
    g_ScriptEvent:RemoveListener("CloseSnowAdventureStageDetailWnd", self, "OnCloseSnowAdventureStageDetailWnd")
    g_ScriptEvent:RemoveListener("OnRankDataReady", self, "OnRankDataReady")

    for i = 1, 4 do
        local len = #self.stageRouteScriptList[i]
        if len ~= 0 then
            for subIndex = 1, len do
                self.stageRouteScriptList[i][subIndex]:RemoveListener()
            end
        end
    end
end

function LuaSnowAdventureSingleStageMainWnd:OnEnterScene()
    CUIManager.CloseUI(CLuaUIResources.SnowAdventureSingleStageMainWnd)
end

function LuaSnowAdventureSingleStageMainWnd:InitWndData()
    self.timeRewardItemId = HanJia2023_XuePoLiXianSetting.GetData().SuccessNumRewardItemId
    self.selectRouteId = 1
    self.rewardTimePerRound = HanJia2023_XuePoLiXianSetting.GetData().SuccessNumNeed
    self.snowAdventureData = LuaHanJia2023Mgr.snowAdventureData

    self.stageDetailScript = LuaSnowAdventureStageDetailWnd:new()
    self.stageDetailScript:Init(self.SnowAdventureStageDetailWnd)
    
    self.stageRouteScriptList = {{}, {}, {}, {}}

    self.initEventOnce = false
    
    self.wndAnimation = self.transform:GetComponent(typeof(Animation))

    local rankId = HanJia2023_XuePoLiXianSetting.GetData().SingleModeRankIdList
    local rankIdList = g_LuaUtil:StrSplit(rankId,",")
    self.rankIdList = {}
    for i = 1, #rankIdList do
        table.insert(self.rankIdList, tonumber(rankIdList[i]))
    end
end

function LuaSnowAdventureSingleStageMainWnd:RefreshConstUI()
    self:InitOneItem(self.RewardButton, self.timeRewardItemId)
end

function LuaSnowAdventureSingleStageMainWnd:RefreshVariableUI()
    if LuaHanJia2023Mgr.snowAdventureData == nil then
        return
    end
    self.snowAdventureData = LuaHanJia2023Mgr.snowAdventureData
    
    --刷新难度按钮
    local modeButtonList = {self.EasyModeButton, self.NormalModeButton, self.HardModeButton}
    for i = 1, 3 do
        local isCurModeOpen = self.snowAdventureData.unlockStageInfo[i] > 0
        local modeButton = modeButtonList[i]
        modeButton.transform:Find("Label").gameObject:SetActive(isCurModeOpen)
        modeButton.transform:Find("Lock").gameObject:SetActive(not isCurModeOpen)
        modeButton.transform:Find("IsSelected").gameObject:SetActive(self.selectRouteId == i)
    end
    Gac2Gas.QueryRank(self.rankIdList[self.selectRouteId])

    --刷新中间的路线
    self:RefreshRoute(self.selectRouteId)
    --刷新成功通关次数
    local realSuccessNum = self.snowAdventureData.successNum % self.rewardTimePerRound
    self.RewardTips.text =  g_MessageMgr:FormatMessage("SnowAdventure_GetRewardTips", realSuccessNum, self.rewardTimePerRound)
    self.progressSprite.fillAmount = realSuccessNum / self.rewardTimePerRound
    
    --刷新剩余参与次数
    self.RemainLabel.text = g_MessageMgr:FormatMessage("SnowAdventure_RemainPlayNum", self.snowAdventureData.playNum)
    
    --刷新雪人
    local snowmanType = self.snowAdventureData.lastSnowmanType
    local snowmanLevel = self.snowAdventureData.snowmanInfo[snowmanType]
    local snowmanConfig = HanJia2023_XuePoLiXianSnowman.GetData(snowmanType)
    self.SnowManTexture:LoadMaterial(snowmanConfig.Pic)
    self.SnowManLevel.text = SafeStringFormat3(LocalString.GetString("%s级"), tostring(snowmanLevel))
    self.SnowManProIcon.spriteName = Profession.GetIconByNumber(snowmanConfig.Career)
    
end

function LuaSnowAdventureSingleStageMainWnd:OnRankDataReady()
    local myRankInfo = CRankData.Inst.MainPlayerRankInfo
    local rank = myRankInfo.Rank
    if rank == 0 then
        self.RankLabel.text = LocalString.GetString("当前 未上榜")
    else
        self.RankLabel.text = LocalString.GetString("当前 第") .. rank .. LocalString.GetString("名")
    end
end

function LuaSnowAdventureSingleStageMainWnd:RefreshRoute(routeId)
    local routeList = {self.EasyRoute, self.NormalRoute, self.HardRoute}
    for i = 1, 3 do
        routeList[i]:SetActive(i == routeId)
    end
    Gac2Gas.QueryRank(self.rankIdList[routeId])
    --刷新难度按钮
    local modeButtonList = {self.EasyModeButton, self.NormalModeButton, self.HardModeButton}
    for i = 1, 3 do
        local modeButton = modeButtonList[i]
        modeButton.transform:Find("IsSelected").gameObject:SetActive(self.selectRouteId == i)
    end
    
    local routeObj = routeList[routeId]
    local childCount = routeObj.transform.childCount
    for i = 1, childCount do
        local childRoot = routeObj.transform:Find(tostring(i))
        --这个点对应的stageIndex
        local stageId = routeId * 100 + i
        --当前路线已解锁的最大StageId
        local curRouteMaxUnlockStageId = self.snowAdventureData.unlockStageInfo[routeId]
        --当前这个点 的前序点是否挑战过
        local preStagePass = true
        if i == 1 then
            if routeId == 1 then
                preStagePass = true
            else    
                --检查上一routeId的有没有全解锁, 绕过了找上一route的最大stageId
                local preRouteId = routeId-1
                for k = 1, 100 do
                    local preStageId = preRouteId*100 + k
                    if self.snowAdventureData.singleInfo[preStageId] ~= nil then
                        if self.snowAdventureData.singleInfo[preStageId] == false then
                            preStagePass = false
                            break
                        end
                    else
                        break
                    end
                end
            end
        else    
            preStagePass = self.snowAdventureData.singleInfo[stageId-1]
        end
        
        local stageScript = nil
        if #self.stageRouteScriptList[routeId] < i then
            stageScript = LuaSnowAdventureStageTemplate:new()
            stageScript:Init(childRoot)
            table.insert(self.stageRouteScriptList[routeId], stageScript)
        else    
            stageScript = self.stageRouteScriptList[routeId][i]
        end
        stageScript:RefreshData(stageId, curRouteMaxUnlockStageId, preStagePass, self.snowAdventureData.nextSingleChallengeStageId)
    end
end

function LuaSnowAdventureSingleStageMainWnd:InitUIEvent()
    if self.snowAdventureData == nil then
        return
    end
    self.initEventOnce = true

    local modeButtonList = {self.EasyModeButton, self.NormalModeButton, self.HardModeButton}
    for i = 1, 3 do
        local isCurModeOpen = self.snowAdventureData.unlockStageInfo[i] > 0
        local modeButton = modeButtonList[i]
        UIEventListener.Get(modeButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
            if isCurModeOpen then
                if self.selectRouteId ~= i then
                    self.selectRouteId = i
                    self:RefreshRoute(self.selectRouteId)
                end
            else
                local startTime = HanJia2023_XuePoLiXianSetting.GetData().StartTime
                startTime = startTime .. " 00:00:00"
                local startTimeStamp = CServerTimeMgr.Inst:GetTimeStampByStr(startTime)
                local curTimeStamp = CServerTimeMgr.Inst.timeStamp
                local SECONDSPERDAY = 24*60*60
                local dayDiff = math.floor((curTimeStamp-startTimeStamp)/SECONDSPERDAY)
                local stageId = 201
                if i == 3 then
                    stageId = 301
                end
                local unlockDayFromStart = HanJia2023_XuePoLiXianLevel.GetData(stageId).UnlockNeedDay-1
                
                g_MessageMgr:ShowMessage("SnowAdventure_CurrentRouteIsLocked", unlockDayFromStart-dayDiff)
            end
        end)
    end

    UIEventListener.Get(self.RuleButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        g_MessageMgr:ShowMessage("SnowAdventure_SinglePlayTips")
    end)

    UIEventListener.Get(self.ChangeSnowManButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        LuaSnowAdventureSnowManWnd.ShowCountdownPattern = false
        CUIManager.ShowUI(CLuaUIResources.SnowAdventureSnowManWnd)
    end)

    UIEventListener.Get(self.RankButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        LuaSnowAdventureSingleStageRankWnd.InitStageType = self.selectRouteId
        CUIManager.ShowUI(CLuaUIResources.SnowAdventureSingleStageRankWnd)
    end)
    
    UIEventListener.Get(self.RemainLabel.transform:Find("PlusBtn").gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        local addTimeItemId = 21051948
        CItemAccessListMgr.Inst:ShowItemAccessInfo(addTimeItemId, true, self.RemainLabel.transform, CTooltip.AlignType.Right)
    end)
    
end

function LuaSnowAdventureSingleStageMainWnd:OnSyncXuePoLiXianData()
    self:RefreshVariableUI()

    if self.initEventOnce == false then
        self:InitUIEvent()
    end
end

function LuaSnowAdventureSingleStageMainWnd:OnSelectSnowAdventureStage(selectedStageId)
    -- open or refresh
    if (not self.SnowAdventureStageDetailWnd.activeInHierarchy) or (self.SnowAdventureStageDetailWnd.transform:GetComponent(typeof(UIPanel)).alpha == 0) then
        self.wndAnimation:Play("snowadventuresinglestagemainwnd_qiehuan")
    end
    self.transform:Find("CloseButton").gameObject:SetActive(false)
    self.stageDetailScript:RefreshData(selectedStageId)
end

function LuaSnowAdventureSingleStageMainWnd:OnCloseSnowAdventureStageDetailWnd()
    --self.transform:Find("Texture_Bg/RightBottom").gameObject:SetActive(true)
    self.wndAnimation:Play("snowadventuresinglestagemainwnd_qiehuanhui")
    self.transform:Find("CloseButton").gameObject:SetActive(true)
end

function LuaSnowAdventureSingleStageMainWnd:InitOneItem(curItem, itemID)
    local texture = curItem.transform:Find("Item"):GetComponent(typeof(CUITexture))
    local ItemData = Item_Item.GetData(itemID)
    if ItemData then texture:LoadMaterial(ItemData.Icon) end
    UIEventListener.Get(curItem).onClick = DelegateFactory.VoidDelegate(function (_)
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemID, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
    end)
end

function LuaSnowAdventureSingleStageMainWnd:GetGuideGo(methodName)
    if methodName == "GetChangeSnowManButton" then
        return self.ChangeSnowManButton
    elseif methodName == "GetEasyRouteFirstStage" then
        return self.EasyRoute.transform:Find("1/circleBg_Normal").gameObject
    elseif methodName == "GetChallengeButton" then
        return self.SnowAdventureStageDetailWnd.transform:Find("ChallengeButton").gameObject
    end
    return nil
end


--@region UIEvent

--@endregion UIEvent

