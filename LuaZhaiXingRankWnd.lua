local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local CPlayerInfoMgr=import "CPlayerInfoMgr"
local CRankData=import "L10.UI.CRankData"
local EnumPlayerInfoContext=import "L10.Game.EnumPlayerInfoContext"
local EChatPanel=import "L10.Game.EChatPanel"
local AlignType=import "CPlayerInfoMgr+AlignType"
local Profession = import "L10.Game.Profession"

LuaZhaiXingRankWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaZhaiXingRankWnd, "m_MyRankLabel", "MyRankLabel", UILabel)
RegistChildComponent(LuaZhaiXingRankWnd, "m_MyCareerIcon", "MyCareerIcon", UISprite)
RegistChildComponent(LuaZhaiXingRankWnd, "m_MyNameLabel", "MyNameLabel", UILabel)
RegistChildComponent(LuaZhaiXingRankWnd, "m_MyCostTimeLabel", "MyCostTimeLabel", UILabel)
RegistChildComponent(LuaZhaiXingRankWnd, "m_MyMaxScoreLabel", "MyMaxScoreLabel", UILabel)
RegistChildComponent(LuaZhaiXingRankWnd, "m_TableView", "TableView", QnTableView)

--@endregion RegistChildComponent end
RegistClassMember(LuaZhaiXingRankWnd, "m_RankList")

function LuaZhaiXingRankWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaZhaiXingRankWnd:Init()
    if CClientMainPlayer.Inst then
        self.m_MyNameLabel.text = CClientMainPlayer.Inst.Name
    end
    self.m_RankList={}
    self.m_TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_RankList
        end,
        function(item,index)
            item:SetBackgroundTexture(index % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite)

            self:InitItem(item,index,self.m_RankList[index+1])
        end
    )
    self.m_TableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        local data = self.m_RankList[row+1]
        if data then
            CPlayerInfoMgr.ShowPlayerPopupMenu(data.PlayerIndex, EnumPlayerInfoContext.Undefined,EChatPanel.Undefined,"",nil,Vector3.zero,AlignType.Default)
        end
    end)

    Gac2Gas.QueryRank(41000226)
end

--@region UIEvent

--@endregion UIEvent

function LuaZhaiXingRankWnd:InitItem(item,index,info)
    local tf=item.transform
    local maxCostTimeLabel=tf:Find("MaxCostTimeLabel"):GetComponent(typeof(UILabel))
    maxCostTimeLabel.text=""
    local maxScoreLabel=tf:Find("MaxScoreLabel"):GetComponent(typeof(UILabel))
    maxScoreLabel.text=""
    local iconSprite=tf:Find("CareerIcon"):GetComponent(typeof(UISprite))
    iconSprite.spriteName=""
    local nameLabel=tf:Find("NameLabel"):GetComponent(typeof(UILabel))
    nameLabel.text=""
    local rankLabel=tf:Find("RankLabel"):GetComponent(typeof(UILabel))
    rankLabel.text=""
    local rankSprite=tf:Find("RankLabel/RankImage"):GetComponent(typeof(UISprite))
    rankSprite.spriteName=""
    local rank=info.Rank
    if rank==1 then
        rankSprite.spriteName="Rank_No.1"
    elseif rank==2 then
        rankSprite.spriteName="Rank_No.2png"
    elseif rank==3 then
        rankSprite.spriteName="Rank_No.3png"
    else
        rankLabel.text=tostring(rank)
    end
    
    local extraUD = info.extraData
	local extra = extraUD and MsgPackImpl.unpack(extraUD)
	local totalSeconds = extra[0]

    iconSprite.spriteName = Profession.GetIcon(info.Job)
    nameLabel.text = info.Name
    maxCostTimeLabel.text = self:GetRemainTimeText(totalSeconds)
    maxScoreLabel.text = info.Value
end

function LuaZhaiXingRankWnd:OnEnable()
    g_ScriptEvent:AddListener("OnRankDataReady", self, "OnRankDataReady")
end

function LuaZhaiXingRankWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnRankDataReady", self, "OnRankDataReady")
end

function LuaZhaiXingRankWnd:OnRankDataReady()
    local myInfo = CRankData.Inst.MainPlayerRankInfo
    if myInfo == nil or myInfo.rankId ~= 41000226 then
        return
    end
    self.m_MyCareerIcon.spriteName =  Profession.GetIcon(CClientMainPlayer.Inst.Class)
    self.m_MyRankLabel.text = myInfo.Rank > 0 and myInfo.Rank or LocalString.GetString("未上榜")
    self.m_MyNameLabel.text = myInfo.Name
    if CClientMainPlayer.Inst then
        self.m_MyNameLabel.text = CClientMainPlayer.Inst.Name
    end
    
    local extraUD = myInfo.extraData
	local extra = extraUD and MsgPackImpl.unpack(extraUD)
	local totalSeconds = extra[0]

    self.m_MyCostTimeLabel.text = self:GetRemainTimeText(totalSeconds > 0 and totalSeconds or 0)
    self.m_MyMaxScoreLabel.text = myInfo.Value > 0 and myInfo.Value or 0
    
    self.m_RankList = {}
    for i=1,CRankData.Inst.RankList.Count do
        table.insert(self.m_RankList, CRankData.Inst.RankList[i-1])
    end
    self.m_TableView:ReloadData(true,false)
end

function LuaZhaiXingRankWnd:GetRemainTimeText(totalSeconds)
    if totalSeconds < 0 then
        totalSeconds = 0
    end
    if totalSeconds >= 3600 then
        return SafeStringFormat3(LocalString.GetString("%02d:%02d:%02d"), math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
    else
        return SafeStringFormat3(LocalString.GetString("%02d:%02d"), math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end
