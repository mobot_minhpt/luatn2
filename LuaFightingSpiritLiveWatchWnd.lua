local UIRoot = import "UIRoot"
local Extensions = import "Extensions"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local CXialvPKMgr = import "L10.Game.CXialvPKMgr"
local CScene = import "L10.Game.CScene"
local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"
local CQMJJMgr = import "L10.Game.CQMJJMgr"
local Constants = import "L10.Game.Constants"
local CMainCamera = import "L10.Engine.CMainCamera"
local CGameVideoMgr = import "L10.Game.CGameVideoMgr"
local CGamePlayMgr = import "L10.Game.CGamePlayMgr"
local CFightingSpiritPlayerInfo = import "L10.UI.CFightingSpiritPlayerInfo"
local CFightingSpiritLiveWatchWndMgr = import "L10.UI.CFightingSpiritLiveWatchWndMgr"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CBiWuDaHuiMgr = import "L10.Game.CBiWuDaHuiMgr"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local CFightingSpiritLiveWatch_TeamInfo=import "L10.UI.CFightingSpiritLiveWatch_TeamInfo"
local CButton=import "L10.UI.CButton"
local CCommonLuaScript=import "L10.UI.CCommonLuaScript"
local PlayerPrefs = import "UnityEngine.PlayerPrefs"
local CFightingSpiritMgr = import "L10.Game.CFightingSpiritMgr"
local EnumCommonForce = import "L10.UI.EnumCommonForce"
local UILongPressButton = import "L10.UI.UILongPressButton"

require "ui/common/guanzhan/LuaFightingSpiritLiveWatch_Top"


CLuaFightingSpiritLiveWatchWnd = class()
RegistClassMember(CLuaFightingSpiritLiveWatchWnd,"m_TopCtrl")
RegistClassMember(CLuaFightingSpiritLiveWatchWnd,"m_LeftTeamCtrl")
RegistClassMember(CLuaFightingSpiritLiveWatchWnd,"m_RightTeamCtrl")
RegistClassMember(CLuaFightingSpiritLiveWatchWnd,"m_ZhanKuanButton")
RegistClassMember(CLuaFightingSpiritLiveWatchWnd,"m_LeaveButton")
RegistClassMember(CLuaFightingSpiritLiveWatchWnd,"m_RemainTimeLabel")
RegistClassMember(CLuaFightingSpiritLiveWatchWnd,"m_PlayerInfos1")
RegistClassMember(CLuaFightingSpiritLiveWatchWnd,"m_PlayerInfos2")
RegistClassMember(CLuaFightingSpiritLiveWatchWnd,"m_LeftForce2ServerName")
RegistClassMember(CLuaFightingSpiritLiveWatchWnd,"m_RightForce2ServerName")
RegistClassMember(CLuaFightingSpiritLiveWatchWnd,"m_OptionBtn")
RegistClassMember(CLuaFightingSpiritLiveWatchWnd,"m_ShowOptionBtn")
RegistClassMember(CLuaFightingSpiritLiveWatchWnd,"m_JingCaiButton")
RegistClassMember(CLuaFightingSpiritLiveWatchWnd,"m_IsHideSelf")

function CLuaFightingSpiritLiveWatchWnd:Awake( )
    self.m_ZhanKuanButton = self.transform:Find("TopLeft/ZhanKuanBtn").gameObject
    if UIRoot.EnableIPhoneXAdaptation then
        local x = self.m_ZhanKuanButton.transform.localPosition.x
        Extensions.SetLocalPositionX(self.m_ZhanKuanButton.transform, x - UIRoot.VirtualIPhoneXWidthMargin)
    end

    local script=self.transform:Find("Top"):GetComponent(typeof(CCommonLuaScript))
    script:Awake()
    self.m_TopCtrl =script.m_LuaSelf

    self.m_LeftTeamCtrl = self.transform:Find("Anchor1/Left"):GetComponent(typeof(CFightingSpiritLiveWatch_TeamInfo))
    self.m_RightTeamCtrl = self.transform:Find("Anchor2/Right"):GetComponent(typeof(CFightingSpiritLiveWatch_TeamInfo))
    self.m_LeaveButton = self.transform:Find("TopRight/LeaveButton"):GetComponent(typeof(CButton))
    self.m_RemainTimeLabel = self.transform:Find("Top/RemainTimeLabel"):GetComponent(typeof(UILabel))

    --录像
    self.m_JingCaiButton=FindChild(self.transform,"JingCaiButton").gameObject
    self.m_JingCaiButton:SetActive(false)

    if CQuanMinPKMgr.Inst.IsInQuanMinPKWatch then
        if CLuaQMPKMgr.m_PlayStage == EnumQmpkPlayStage.eJiFenSai
        or CLuaQMPKMgr.m_PlayStage == EnumQmpkPlayStage.eTaoTaiSai
        or CLuaQMPKMgr.m_PlayStage == EnumQmpkPlayStage.eZongJueSai then
            if CGameVideoMgr.Inst:IsLiveMode() then
                self.m_JingCaiButton:SetActive(true)
                UIEventListener.Get(self.m_JingCaiButton).onClick=DelegateFactory.VoidDelegate(function(go)
                    CLuaQMPKMgr.m_PlayIndex=0
                    CUIManager.ShowUI(CLuaUIResources.QMPKJingCaiContentWnd)
                end)
            end
        end
    end

    self.m_OptionBtn = self.transform:Find("BottomRight/OptionBtn").gameObject
    self.m_ShowOptionBtn=false
    --切磋场景不显示
    local sceneId = CScene.MainScene and CScene.MainScene.SceneTemplateId or 0
    if sceneId == Constants.QieCuoSceneTemplateId or sceneId == Constants.QieCuoWatchSceneTemplateId then
        self.m_OptionBtn:SetActive(false)
    else
        self.m_ShowOptionBtn=true
        self.m_OptionBtn:SetActive(true)
        -- if CGameVideoMgr.Inst:IsVideoMode() then
        --     optionBtn.transform.localPosition = Vector3(-60,160,0)
        -- else
        --     optionBtn.transform.localPosition = Vector3(-60,60+40,0)
        -- end

        local pos = self.transform:InverseTransformPoint(self.m_OptionBtn.transform.position)
        UIEventListener.Get(self.m_OptionBtn).onClick = DelegateFactory.VoidDelegate(function(go)
            self:ShowDlg(go,pos)

        end)
    end
end

function CLuaFightingSpiritLiveWatchWnd:ShowDlg(go,pos)
    local textArray = {LocalString.GetString("屏蔽伤害数字"),LocalString.GetString("屏蔽自身")}
    if not CLuaFightingSpiritMgr.m_ShowLeiGuView then
        table.insert(textArray,LocalString.GetString("屏蔽聊天框"))
    end
    table.insert(textArray,LocalString.GetString("屏蔽弹幕"))
    table.insert(textArray,LocalString.GetString("屏蔽擂鼓效果"))
    CLuaOptionMgr.ShowDlg(
        function() return #textArray end,
        function(index)
            return textArray[index]
        end,
        function(index)
            if textArray[index] == LocalString.GetString("屏蔽伤害数字") then
                return CLuaGameVideoMgr.IsHideHpHurtText()
            elseif textArray[index] == LocalString.GetString("屏蔽自身") then
                return self:IsHideSelf()
            elseif textArray[index] == LocalString.GetString("屏蔽聊天框") then
                return self:IsHideThumbnailChatWnd()
            elseif textArray[index] == LocalString.GetString("屏蔽弹幕") then
                return PlayerPrefs.GetInt("LeiGuView_HideDanMu") and PlayerPrefs.GetInt("LeiGuView_HideDanMu") > 0
            elseif textArray[index] == LocalString.GetString("屏蔽擂鼓效果") then
                return PlayerPrefs.GetInt("LeiGuView_HideLeiGuVFx") and PlayerPrefs.GetInt("LeiGuView_HideLeiGuVFx") > 0
            end
        end,
        function(index,val)
            if textArray[index] == LocalString.GetString("屏蔽伤害数字") then
                CLuaGameVideoMgr.HideHpHurtText(val)
            elseif textArray[index] == LocalString.GetString("屏蔽自身") then
                self:ShowOrHideSelf(val)
            elseif textArray[index] == LocalString.GetString("屏蔽聊天框") then
                self:ShowOrHideThumbnailChatWnd(not val)
            elseif textArray[index] == LocalString.GetString("屏蔽弹幕") then
                PlayerPrefs.SetInt("LeiGuView_HideDanMu", val and 1 or 0)
            elseif textArray[index] == LocalString.GetString("屏蔽擂鼓效果") then
                PlayerPrefs.SetInt("LeiGuView_HideLeiGuVFx", val and 1 or 0)
            end
        end, self.transform:TransformPoint(Vector3(pos.x-100,pos.y+150+40,0))
    )
end

function CLuaFightingSpiritLiveWatchWnd:IsHideSelf()
    return PlayerPrefs.GetInt("FightingSpiritLiveWatchWnd_HideSelf") > 0
end

function CLuaFightingSpiritLiveWatchWnd:IsHideThumbnailChatWnd()
    return PlayerPrefs.GetInt("FightingSpiritLiveWatchWnd_HideThumbnailChatWnd") > 0
end

function CLuaFightingSpiritLiveWatchWnd:ShowOrHideSelf(val)
    local mainPlayer = CClientMainPlayer.Inst
    if not mainPlayer then return end
    mainPlayer.Visible = not val
    for i = 0, mainPlayer.PetEngineIds.Count - 1 do
        local engineId = mainPlayer.PetEngineIds[i]
        local obj = CClientObjectMgr.Inst:GetObject(engineId)
        if obj then
            obj.Visible = not val
        end
    end
    if mainPlayer.CurrentLingShou then
        mainPlayer.CurrentLingShou.Visible = not val
        mainPlayer.CurrentLingShou:SetAllBabyVisible(not val)
    end
    PlayerPrefs.SetInt("FightingSpiritLiveWatchWnd_HideSelf", val and 1 or 0)
end

function CLuaFightingSpiritLiveWatchWnd:ShowOrHideThumbnailChatWnd(val)
    g_ScriptEvent:BroadcastInLua("ShowOrHideThumbnailChatWnd", val)
    PlayerPrefs.SetInt("FightingSpiritLiveWatchWnd_HideThumbnailChatWnd", val and 0 or 1)
end

function CLuaFightingSpiritLiveWatchWnd:Init( )
    self.m_PlayerInfos1 = CreateFromClass(MakeGenericClass(List, CFightingSpiritPlayerInfo))
    self.m_PlayerInfos2 = CreateFromClass(MakeGenericClass(List, CFightingSpiritPlayerInfo))

    local info1=CFightingSpiritLiveWatchWndMgr.LeftForce2ServerName
    local info2=CFightingSpiritLiveWatchWndMgr.RightForce2ServerName
    local leftInfo=info1
    local rightInfo=info2
    --force1 攻击 蓝方 左侧

    --只处理全民pk，其他玩法都有特殊处理           跨服明星赛又抄了一遍                     斗魂坛也加下
    if CQuanMinPKMgr.Inst.IsInQuanMinPKWatch or CLuaStarBiwuMgr:IsInStarBiwuWatch() or CFightingSpiritMgr.Instance:IsDouhunFighting() then
        if info1.Force~= EnumCommonForce.eAttack then
            leftInfo=info2
            rightInfo=info1
        end
    end
    self.m_LeftForce2ServerName={
        Force = leftInfo.Force,
        ServerName = leftInfo.ServerName,
        FutiLv = leftInfo.FutiLv,
        ShenLiLv = leftInfo.ShenLiLv,
        FutiInfos = leftInfo.FutiInfos
    }
    self.m_RightForce2ServerName={
        Force = rightInfo.Force,
        ServerName = rightInfo.ServerName,
        FutiLv = rightInfo.FutiLv,
        ShenLiLv = rightInfo.ShenLiLv,
        FutiInfos = rightInfo.FutiInfos
    }

    CommonDefs.ListClear(self.m_PlayerInfos1)
    CommonDefs.ListClear(self.m_PlayerInfos2)
    do
        local i = 0
        while i < CFightingSpiritLiveWatchWndMgr.PlayerInfos.Count do
            if CFightingSpiritLiveWatchWndMgr.PlayerInfos[i].Force == self.m_LeftForce2ServerName.Force then
                CommonDefs.ListAdd(self.m_PlayerInfos1, typeof(CFightingSpiritPlayerInfo), CFightingSpiritLiveWatchWndMgr.PlayerInfos[i])
            else
                CommonDefs.ListAdd(self.m_PlayerInfos2, typeof(CFightingSpiritPlayerInfo), CFightingSpiritLiveWatchWndMgr.PlayerInfos[i])
            end
            i = i + 1
        end
    end

    local leftServerName = self.m_LeftForce2ServerName.ServerName
    if self.m_LeftForce2ServerName.FutiLv > 0 then
        leftServerName = System.String.Format(LocalString.GetString("({0}级附体){1}"), self.m_LeftForce2ServerName.FutiLv, self.m_LeftForce2ServerName.ServerName)
    end
    local rightServerName = self.m_RightForce2ServerName.ServerName
    if self.m_RightForce2ServerName.FutiLv > 0 then
        rightServerName = System.String.Format(LocalString.GetString("{0}({1}级附体)"), self.m_RightForce2ServerName.ServerName, self.m_RightForce2ServerName.FutiLv)
    end

    self.m_TopCtrl:Init(leftServerName, rightServerName, self.m_LeftForce2ServerName.ShenLiLv, self.m_LeftForce2ServerName.FutiInfos, self.m_RightForce2ServerName.ShenLiLv, self.m_RightForce2ServerName.FutiInfos)
    self.m_LeftTeamCtrl:Init(self.m_PlayerInfos1, self.m_LeftForce2ServerName.ServerName, self.m_LeftForce2ServerName.FutiLv == - 1, true)
    self.m_RightTeamCtrl:Init(self.m_PlayerInfos2, self.m_RightForce2ServerName.ServerName, self.m_RightForce2ServerName.FutiLv == - 1, false)
    self:GetDiekePossessingData()
    if CScene.MainScene ~= nil then
        local sceneId = CScene.MainScene.SceneTemplateId
        self.m_ZhanKuanButton.gameObject:SetActive(sceneId ~= Constants.QieCuoSceneTemplateId and sceneId ~= Constants.QieCuoWatchSceneTemplateId)
        if sceneId == Constants.DouHunCrossSceneTemplateId or sceneId == Constants.DouHunLocalSceneTemplateId or sceneId == Constants.QmpkSceneTemplateId then
            CUIManager.ShowUI(CUIResources.DanmuWnd)
            g_ScriptEvent:BroadcastInLua("ShowDanmuEntry",true)
        end
        if CBiWuDaHuiMgr.Inst.inBiWu then
            CUIManager.ShowUI(CUIResources.DanmuWnd)
            g_ScriptEvent:BroadcastInLua("ShowDanmuEntry",true)
        end
    end

    self:UpdateKillNum(self.m_PlayerInfos1, self.m_PlayerInfos2)
end

function CLuaFightingSpiritLiveWatchWnd:UpdateKillNum(leftInfos, rightInfos)
    if not CLuaStarBiwuMgr:IsInStarBiwuWatch() then return end
    local killNum1,killNum2 = 0,0
    for i = 0,leftInfos.Count - 1 do
        killNum1 = killNum1 + leftInfos[i].KillNum
    end
    for i = 0,rightInfos.Count - 1 do
        killNum2 = killNum2 + rightInfos[i].KillNum
    end
    self.m_TopCtrl:UpdateKillNum(killNum1, killNum2)
end

function CLuaFightingSpiritLiveWatchWnd:OnEnable( )
    local height = DouHunTan_Setting.GetData().WatchCameraHeight
    self:SetWatchCameraWatchHeight(height)
    UIEventListener.Get(self.m_ZhanKuanButton).onClick =DelegateFactory.VoidDelegate(function(go) self:OnZhanKuanButtonClick(go) end)
    UIEventListener.Get(self.m_LeaveButton.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) self:OnLeaveButtonClick(go) end)
    self:ShowOrHideSelf(self:IsHideSelf())
    self:ShowOrHideThumbnailChatWnd(not self:IsHideThumbnailChatWnd())
    CUIManager.ShowUI(CLuaUIResources.CommonKillInfoTopWnd)
    CUIManager.CloseUI(CLuaUIResources.ScheduleWnd)
    g_ScriptEvent:AddListener("UpdateDouHunFightInfoToWatcher", self, "OnUpdateDouHunFightInfoToWatcher")
    g_ScriptEvent:AddListener("SceneRemainTimeUpdate", self, "OnRemainTimeUpdate")
    g_ScriptEvent:AddListener("UpdateSubstituePlayerInfo", self, "OnUpdateSubstituePlayerInfo")
    g_ScriptEvent:AddListener("ShowVideoProgress", self, "OnShowVideoProgress")
    g_ScriptEvent:AddListener("LingShouUpdateStatus",self,"OnLingShouUpdateStatus")
end
function CLuaFightingSpiritLiveWatchWnd:OnDisable( )
    UIEventListener.Get(self.m_ZhanKuanButton).onClick = nil
    UIEventListener.Get(self.m_LeaveButton.gameObject).onClick = nil
    self:SetWatchCameraWatchHeight(0)
    --self:ShowOrHideSelf(true)
    g_ScriptEvent:BroadcastInLua("ShowOrHideThumbnailChatWnd", true)
    g_ScriptEvent:RemoveListener("UpdateDouHunFightInfoToWatcher", self, "OnUpdateDouHunFightInfoToWatcher")
    g_ScriptEvent:RemoveListener("SceneRemainTimeUpdate", self, "OnRemainTimeUpdate")
    g_ScriptEvent:RemoveListener("UpdateSubstituePlayerInfo", self, "OnUpdateSubstituePlayerInfo")
    g_ScriptEvent:RemoveListener("ShowVideoProgress", self, "OnShowVideoProgress")
    g_ScriptEvent:RemoveListener("LingShouUpdateStatus",self,"OnLingShouUpdateStatus")
    CUIManager.CloseUI("OptionDlg")
    CUIManager.CloseUI(CLuaUIResources.CommonKillInfoTopWnd)
end

function CLuaFightingSpiritLiveWatchWnd:GetDiekePossessingData()
    local leftTeam = self.m_LeftTeamCtrl.m_PlayerInfos
    local rightTeam = self.m_RightTeamCtrl.m_PlayerInfos
    for i = 0,leftTeam.Count - 1 do
        local info = leftTeam[i]
        local item = self.m_LeftTeamCtrl.m_TeamTable:GetItemAtRow(i)
        self:UpdateItemDiekePossessingSign(info, item)
    end
    for i = 0,rightTeam.Count - 1 do
        local info = rightTeam[i]
        local item = self.m_RightTeamCtrl.m_TeamTable:GetItemAtRow(i)
        self:UpdateItemDiekePossessingSign(info, item)
    end
end

function CLuaFightingSpiritLiveWatchWnd:UpdateItemDiekePossessingSign(info, item)
    if not item.m_PlayerInfo or info.PlayerId ~= item.m_PlayerInfo.PlayerId then return end
    local data = CLuaFightingSpiritMgr.m_DiekePossessingData[info.PlayerId]
    if not data then return end
    local possessTarget,expiredTime,aggressivePossessing,positivePossessing,aggressivePossessNum,positivePossessNum = data[0],data[1],data[2],data[3],data[4],data[5]
    --item.m_PotraitTexture:LoadNPCPortrait(aggressivePossessing and Constants.DiekeAggressivePossessingPortrait or (positivePossessing and Constants.DiekePositivePossessingPortrait or CUICommonDef.GetPortraitName(info.Class, info.Gender)))
    item:UpdateDiekePossessByOtherIcon(aggressivePossessNum,positivePossessNum)
    CLuaFightingSpiritMgr.m_DiekePossessingData[info.PlayerId] = nil
end

function CLuaFightingSpiritLiveWatchWnd:OnLingShouUpdateStatus()
    self:ShowOrHideSelf(self:IsHideSelf())
end

function CLuaFightingSpiritLiveWatchWnd:OnShowVideoProgress(show)
    if self.m_ShowOptionBtn then
        self.m_OptionBtn:SetActive(not show)
    end
end
function CLuaFightingSpiritLiveWatchWnd:OnUpdateSubstituePlayerInfo( args )
    local playerId=args[0]
    local destplayerInfo=args[1]

    local oldInfo = self:GetPlayerInfo(playerId)
    if oldInfo ~= nil then
        local index = - 1
        do
            local i = 0
            while i < self.m_PlayerInfos1.Count do
                if self.m_PlayerInfos1[i] == oldInfo then
                    index = i
                    break
                end
                i = i + 1
            end
        end
        --int index = m_PlayerInfos1.IndexOf(oldInfo);
        if index >= 0 and index < self.m_PlayerInfos1.Count then
            local newInfo = CFightingSpiritLiveWatchWndMgr.ToFightingSpiritPlayerInfo(destplayerInfo)
            newInfo.IsYuanHu = true
            self.m_PlayerInfos1[index] = newInfo
            self.m_LeftTeamCtrl:UpdateYuanZuData(newInfo, index, true)
        else
            --index = m_PlayerInfos2.IndexOf(oldInfo);
            do
                local i = 0
                while i < self.m_PlayerInfos2.Count do
                    if self.m_PlayerInfos2[i] == oldInfo then
                        index = i
                        break
                    end
                    i = i + 1
                end
            end


            if index >= 0 and index < self.m_PlayerInfos2.Count then
                local newInfo = CFightingSpiritLiveWatchWndMgr.ToFightingSpiritPlayerInfo(destplayerInfo)
                newInfo.IsYuanHu = true
                self.m_PlayerInfos2[index] = newInfo
                self.m_RightTeamCtrl:UpdateYuanZuData(newInfo, index, false)
            end
        end
    end
    self:GetDiekePossessingData()
    self:UpdateKillNum(self.m_PlayerInfos1, self.m_PlayerInfos2)
end
function CLuaFightingSpiritLiveWatchWnd:OnZhanKuanButtonClick( go)
    if CScene.MainScene ~= nil then
        if CBiWuDaHuiMgr.Inst.inBiWu then
            if CGameVideoMgr.Inst:IsInGameVideoScene() then
                CGameVideoMgr.Inst:QueryGameVideoBiWuTeamRoundData()
            else
                Gac2Gas.QueryBiWuTeamRoundData()
            end
        elseif CQMJJMgr.Inst.inQMJJ then
            Gac2Gas.QueryQMJJTeamRoundData()
        elseif CScene.MainScene.SceneTemplateId == Constants.DouHunLocalSceneTemplateId or CScene.MainScene.SceneTemplateId == Constants.DouHunCrossSceneTemplateId then
            Gac2Gas.QueryDouHunPlayInfo()
        elseif CScene.MainScene.SceneTemplateId == Constants.DouHunWatchSceneTemplateId then
            CGameVideoMgr.Inst:QueryDouHunPlayInfo()
        elseif CXialvPKMgr.Inst.InXiaLvPK then
            if CXialvPKMgr.Inst.InXiaLvPkCross then
                Gac2Gas.CrossXiaLvPkQueryForceRoundData()
            elseif CXialvPKMgr.Inst.InXiaLvPkGuanZhan then
                CUIManager.ShowUI("XialvPKResultWnd")
            else
                Gac2Gas.XiaLvPkQueryForceRoundData()
            end
        elseif CQuanMinPKMgr.Inst.IsInQuanMinPKWatch then
            if CGameVideoMgr.Inst:IsInGameVideoScene() then
                -- CGameVideoMgr.Inst:QueryGameVideoQMPKTeamRoundData()
                CLuaGameVideoMgr.QueryGameVideoQMPKTeamRoundData()
            else
                -- CQuanMinPKMgr.Inst.m_IsGameVideoBattleStatus = false
                CLuaQMPKMgr.m_IsGameVideoBattleStatus=false
                CUIManager.ShowUI(CLuaUIResources.QMPKCurrentBattleStatusWnd)
            end
        elseif CLuaStarBiwuMgr.IsInStarBiwuWatch() then
          if CGameVideoMgr.Inst:IsInGameVideoScene() then
            CLuaStarBiwuMgr.m_IsGameVideoBattleStatus = true
            CUIManager.ShowUI(CLuaUIResources.StarBiwuCurrentBattleStatusWnd)
          end
        end
    end
end
function CLuaFightingSpiritLiveWatchWnd:OnLeaveButtonClick( button)
    CGamePlayMgr.Inst:LeavePlay()
end
function CLuaFightingSpiritLiveWatchWnd:OnRemainTimeUpdate( args )
    local leftTime=args[0]
    if CScene.MainScene ~= nil then
        self.m_RemainTimeLabel.text = self:GetRemainTimeText(CScene.MainScene.ShowTime)
    else
        self.m_RemainTimeLabel.text = ""
    end
end
function CLuaFightingSpiritLiveWatchWnd:GetRemainTimeText( totalSeconds)
    if totalSeconds < 0 then
        totalSeconds = 0
    end
    if totalSeconds >= 3600 then
        return System.String.Format("[ACF9FF]{0:00}:{1:00}:{2:00}[-]", math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
    else
        return System.String.Format("[ACF9FF]{0:00}:{1:00}[-]", math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end

function CLuaFightingSpiritLiveWatchWnd:OnUpdateLocalPlayInfos(info)
    for i = 0, self.m_PlayerInfos2.Count - 1 do
        if self.m_PlayerInfos2[i].PlayerId == info.PlayerId then
            self.m_PlayerInfos2[i] = info
        end
    end

    for i = 0, self.m_PlayerInfos1.Count - 1 do
        if self.m_PlayerInfos1[i].PlayerId == info.PlayerId then
            self.m_PlayerInfos1[i] = info
        end
    end

    self:UpdateKillNum(self.m_PlayerInfos1, self.m_PlayerInfos2)
end

function CLuaFightingSpiritLiveWatchWnd:OnUpdateDouHunFightInfoToWatcher(args)
    local fightinfos=args[0]
    local  dpsInfos=args[1]
    local isGM=args[2]
    --根据洪磊描述，更新时先清空下血量
    self:ResetAllHpAndMpToZero()
    for i=1,fightinfos.Count do
        local data = fightinfos[i-1]
        if data ~= nil then
            local playerId = math.floor(tonumber(data[0] or 0))
            local info = self:GetPlayerInfo(playerId)
            if info then
                info.KillNum = math.floor(tonumber(data[1] or 0))
                info.Dps = math.floor(tonumber(data[2] or 0))

                local hpInfo = data[3]
                info.Hp = math.floor(tonumber(hpInfo[1] or 0))
                info.Mp = math.floor(tonumber(hpInfo[4] or 0))

                info.HpFull = math.floor(tonumber(hpInfo[3] or 0))
                info.MpFull = math.floor(tonumber(hpInfo[6] or 0))

                self:OnUpdateLocalPlayInfos(info)
                local juejidata = data[4]
                if juejidata ~= nil then
                    for k=1,juejidata.Count do
                        local juejidata2 = juejidata[k-1]
                        if juejidata2 ~= nil then
                            local skillId = math.floor(tonumber(juejidata2[0] or 0))
                            local juejiInfo = self:GetJueJiInfo(info, skillId)
                            if juejiInfo ~= nil then
                                juejiInfo.CD = tonumber(juejidata2[1])
                                juejiInfo.TotalCD = tonumber(juejidata2[2])
                            end
                        end
                    end
                end
            end
        end
    end


    --获取dps信息
    local total1 = 0 local total2 = 0
    local killNum1,killNum2=0,0
    local hasKillNum=false
    if dpsInfos.Count >= 2 then
        for i=1,dpsInfos.Count do
            local data = dpsInfos[i-1]
            if data ~= nil and data.Count >= 2 then
                local force = data[0]
                if force == EnumToInt(self.m_LeftForce2ServerName.Force) then
                    total1 = math.floor(tonumber(data[1] or 0))
                    if data.Count>=3 then
                        killNum1 = tonumber(data[2])
                        hasKillNum=true
                    end
                else
                    total2 = math.floor(tonumber(data[1] or 0))
                    if data.Count>=3 then
                        killNum2 = tonumber(data[2])
                    end
                end
            end
        end
    end

    self.m_LeftTeamCtrl:UpdateData(self.m_PlayerInfos1, total1, isGM)
    self.m_RightTeamCtrl:UpdateData(self.m_PlayerInfos2, total2, isGM)
    self.m_TopCtrl:UpdateHpInfo(total1, total2)
    self:GetDiekePossessingData()
    if hasKillNum then
        self.m_TopCtrl:UpdateKillNum(killNum1,killNum2)
    end

    --update hp
    for i=1,fightinfos.Count do
        local data = fightinfos[i-1]
        if data ~= nil then
            local hpInfo = data[3]
            local engineId = math.floor(tonumber(hpInfo[0] or 0))

            local obj = CClientObjectMgr.Inst:GetObject(engineId)
            if obj ~= nil then
                obj.Hp = math.floor(tonumber(hpInfo[1] or 0))
                obj.CurrentHpFull = math.floor(tonumber(hpInfo[2] or 0))
                obj.HpFull = math.floor(tonumber(hpInfo[3] or 0))
                obj.Mp = math.floor(tonumber(hpInfo[4] or 0))
                obj.CurrentMpFull = math.floor(tonumber(hpInfo[5] or 0))
                obj.MpFull = math.floor(tonumber(hpInfo[6] or 0))

                EventManager.BroadcastInternalForLua(EnumEventType.HpUpdate, {engineId})
                EventManager.BroadcastInternalForLua(EnumEventType.MpUpdate, {engineId})
            end
        end
    end
end

function CLuaFightingSpiritLiveWatchWnd:GetPlayerInfo( playerId)
    do
        local i = 0
        while i < self.m_PlayerInfos1.Count do
            if self.m_PlayerInfos1[i].PlayerId == playerId then
                return self.m_PlayerInfos1[i]
            end
            i = i + 1
        end
    end

    do
        local i = 0
        while i < self.m_PlayerInfos2.Count do
            if self.m_PlayerInfos2[i].PlayerId == playerId then
                return self.m_PlayerInfos2[i]
            end
            i = i + 1
        end
    end
    return nil
end
function CLuaFightingSpiritLiveWatchWnd:ResetAllHpAndMpToZero( )
    do
        local i = 0
        while i < self.m_PlayerInfos1.Count do
            self.m_PlayerInfos1[i].Mp = 0 self.m_PlayerInfos1[i].Hp = self.m_PlayerInfos1[i].Mp
            i = i + 1
        end
    end

    do
        local i = 0
        while i < self.m_PlayerInfos2.Count do
            self.m_PlayerInfos2[i].Mp = 0 self.m_PlayerInfos2[i].Hp = self.m_PlayerInfos2[i].Mp
            i = i + 1
        end
    end
end
function CLuaFightingSpiritLiveWatchWnd:GetJueJiInfo( playerInfo, juejiId)
    if playerInfo.JueJiInfos ~= nil then
        do
            local i = 0
            while i < playerInfo.JueJiInfos.Count do
                if playerInfo.JueJiInfos[i].SkillId == juejiId then
                    return playerInfo.JueJiInfos[i]
                end
                i = i + 1
            end
        end
    end
    return nil
end
function CLuaFightingSpiritLiveWatchWnd:SetWatchCameraWatchHeight( height)
    CMainCamera.Inst:SetMainCameraYPos(height)
    if CClientMainPlayer.Inst ~= nil then
        CameraFollow.Inst:SetFollowObj(nil, nil)
    end
end



-- return CLuaFightingSpiritLiveWatchWnd
