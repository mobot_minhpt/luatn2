-- Auto Generated!!
local CChuangGuanRankItem = import "L10.UI.CChuangGuanRankItem"
local CUICommonDef = import "L10.UI.CUICommonDef"
local LocalString = import "LocalString"
CChuangGuanRankItem.m_SetRankInfo_CS2LuaHook = function (this, totalRank, name, timeList, totalTime, self, rankList) 

    this.RankLabel.text = this:rankToString(totalRank)
    this.NameLabel.text = name

    do
        local i = 0
        while i < this.LabelArray.Length do
            local time = 0
            if timeList ~= nil and timeList.Count > i then
                time = timeList[i]
            end
            local rank = 0
            if rankList ~= nil and rankList.Count > i then
                rank = rankList[i]
            end

            this:showTimeStr(this.LabelArray[i], time, rank, self)
            i = i + 1
        end
    end

    if totalTime <= 0 then
        this.TotalTimeLabel.text = LocalString.GetString("暂无")
    else
        this.TotalTimeLabel.text = CUICommonDef.SecondsToTimeString(totalTime, true)
    end
end
CChuangGuanRankItem.m_showTimeStr_CS2LuaHook = function (this, label, time, rank, self) 
    local timestr = this:timeToString(time)
    if not self then
        label.text = timestr
    else
        if time > 0 then
            label.text = System.String.Format(CChuangGuanRankItem.normalFormat, timestr, this:rankToString(rank))
        else
            label.text = System.String.Format(CChuangGuanRankItem.emptyFormat, timestr)
        end
    end
end
CChuangGuanRankItem.m_rankToString_CS2LuaHook = function (this, rank) 
    if rank > 0 then
        return tostring(rank)
    else
        return LocalString.GetString("未上榜")
    end
end
CChuangGuanRankItem.m_timeToString_CS2LuaHook = function (this, time) 
    if time > 0 then
        return CUICommonDef.SecondsToTimeString(time, true)
    else
        return LocalString.GetString("暂无")
    end
end
