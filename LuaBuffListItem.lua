local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CPropertyBuff = import "L10.Game.CPropertyBuff"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CClientChuanJiaBaoMgr = import "L10.Game.CClientChuanJiaBaoMgr"
local CChatLinkMgr = import "CChatLinkMgr"
local Profession = import "L10.Game.Profession"
local EnumClass = import "L10.Game.EnumClass"
local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"

CLuaBuffListItem = class()
RegistClassMember(CLuaBuffListItem,"icon")
RegistClassMember(CLuaBuffListItem,"nameLabel")
RegistClassMember(CLuaBuffListItem,"remainTimeLabel")
RegistClassMember(CLuaBuffListItem,"descLabel")
RegistClassMember(CLuaBuffListItem,"descBg")
RegistClassMember(CLuaBuffListItem,"lastUpdateTime")
RegistClassMember(CLuaBuffListItem,"buffId")
RegistClassMember(CLuaBuffListItem,"objId")
RegistClassMember(CLuaBuffListItem,"needShowTime")
RegistClassMember(CLuaBuffListItem,"OnDescLabelChanged")
RegistClassMember(CLuaBuffListItem,"prayStartTime")

local nameColor = "FFED5F"
local valueColor = "E8D0AA"

CLuaBuffListItem.QMPKPrayBuffId = 64162610
CLuaBuffListItem.QMPKLingfuBuffId = 642225
CLuaBuffListItem.HuTongBuffId = 643985

function CLuaBuffListItem:Awake()
    self.icon = self.transform:Find("BuffIcon/Icon"):GetComponent(typeof(CUITexture))
    self.nameLabel = self.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    self.remainTimeLabel = self.transform:Find("RemainTimeLabel"):GetComponent(typeof(UILabel))
    self.descLabel = self.transform:Find("DescLabel"):GetComponent(typeof(UILabel))
    self.descBg = self.transform:Find("DesBg"):GetComponent(typeof(UIWidget))
    self.lastUpdateTime = 0
    self.buffId = 0
    self.objId = 0
    self.needShowTime = true
    self.OnDescLabelChanged = nil
    self.prayStartTime = 0

end

function CLuaBuffListItem:Init( buffId, objId)
    self.icon:Clear()
    self.buffId = buffId
    self.objId = objId
    self.nameLabel.text = nil
    self.remainTimeLabel.text = nil
    self.descLabel.text = nil

    local buff = Buff_Buff.GetData(buffId)
    if buff ~= nil and buff.NeedDisplay > 0 then
        self.icon:LoadMaterial(buff.Icon)
        
        if math.floor(buffId / 100) == GameplayItem_YinXianWan.GetData().SunburnBuff  then
            -- 隐仙湾晒伤名称特殊处理
            local buffLv = buffId % 100
            self.nameLabel.text = SafeStringFormat3("[c][%s]%s[-][/c]", nameColor, GameplayItem_YinXianWan.GetData().SunburnBuffName[buffLv - 1])
        elseif CQuanMinPKMgr.Inst.m_IsQuanMinPKServer and LuaZongMenMgr:IsAntiProfessionBuff(buffId) then
            self.nameLabel.text = SafeStringFormat3("[c][%s]%s(%s)[-][/c]", nameColor, buff.Name, LocalString.GetString("全民争霸赛专用"))
        elseif buff.Level == 1 and not Buff_Buff.Exists(buff.ID + 1) then
            self.nameLabel.text = SafeStringFormat3("[c][%s]%s[-][/c]", nameColor, buff.Name)
        else
            self.nameLabel.text = SafeStringFormat3(LocalString.GetString("[c][%s]%s[-][%s](%d级)[-][/c]"), nameColor, buff.Name, valueColor, buff.Level)
        end
        self.remainTimeLabel.text = ""
        if CLuaHouseMgr.IsHouseBuff(buffId) then
            self:InitHouseBuff(buff, objId)
        elseif math.floor(buffId / 100) == math.floor(CLuaBuffListItem.QMPKPrayBuffId / 100) then
            Gac2Gas.QueryQmpkPlayerPrayWordGroup(objId)
        elseif math.floor(buffId / 100) == CLuaBuffListItem.QMPKLingfuBuffId then
            Gac2Gas.QueryQmpkLingShouLingFuIndex(objId)
        elseif math.floor(buffId / 100) == CLuaBuffListItem.HuTongBuffId then
            self.descLabel.text = CPropertyBuff.GetBuffDisplay(buffId, objId)
            Gac2Gas.QueryMenPaiHuTongSkillInfo(objId)
        elseif LuaZongMenMgr:IsAntiProfessionBuff(buffId) then
            self:InitAntiProfessionBuff(buffId, objId)
        else
            self.descLabel.text = CPropertyBuff.GetBuffDisplay(buffId, objId)
        end
        self.needShowTime = (buff.TimeHidden == 0)
    end
    self.descBg.height = math.floor(math.abs(self.descLabel.transform.localPosition.y - self.descLabel.localSize.y - 4))
    self.lastUpdateTime = 0
end


function CLuaBuffListItem:OnEnable()
    g_ScriptEvent:AddListener("OnQueryOtherPlayerPrayInfoResult",self,"OnQueryOtherPlayerPrayResult")
    g_ScriptEvent:AddListener("QMPKPrayBuffWordResult",self,"InitQMPKBuff")
    g_ScriptEvent:AddListener("ReplyQmpkLingShouLingFuIndex",self,"OnReplyQmpkLingShouLingFuIndex")
    g_ScriptEvent:AddListener("SanxingMenPaiHuTongUpdate",self,"OnSanxingMenPaiHuTongUpdate")
end
function CLuaBuffListItem:OnDisable()
    g_ScriptEvent:RemoveListener("OnQueryOtherPlayerPrayInfoResult",self,"OnQueryOtherPlayerPrayResult")
    g_ScriptEvent:RemoveListener("QMPKPrayBuffWordResult",self,"InitQMPKBuff")
    g_ScriptEvent:RemoveListener("ReplyQmpkLingShouLingFuIndex",self,"OnReplyQmpkLingShouLingFuIndex")
    g_ScriptEvent:RemoveListener("SanxingMenPaiHuTongUpdate",self,"OnSanxingMenPaiHuTongUpdate")
end

function CLuaBuffListItem:OnSanxingMenPaiHuTongUpdate(engineId, info)
  if self.objId == engineId and math.floor(self.buffId / 100) == CLuaBuffListItem.HuTongBuffId and info then
    local oriText = self.descLabel.text
    local skillText = info[1]
    for i,v in ipairs(info) do
      if i > 1 then
        skillText = SafeStringFormat3('%s,%s',skillText,v)
      end
    end
    self.descLabel.text = oriText .. skillText
  end
end

function CLuaBuffListItem:OnReplyQmpkLingShouLingFuIndex( engineId, generalLingFu, specialLingFu, suitLingFu)
    if self.objId == engineId and math.floor(self.buffId / 100) == CLuaBuffListItem.QMPKLingfuBuffId then
        local buff = Buff_Buff.GetData(self.buffId)
        if buff == nil then
            return
        end

        local t = {}

        if buff.Display and buff.Display~="" then
            table.insert( t, CPropertyBuff.GetBuffDisplay(self.buffId, engineId))
        end

        local lingshou = CClientObjectMgr.Inst:GetObject(engineId)

        local data = QuanMinPK_LingShou.GetData(lingshou.TemplateId)
        local word1 = data.GeneralLingFu[generalLingFu - 1]
        local word2 = data.SpecialLingFu[specialLingFu - 1]
        local word3 = data.SuitLingFu[suitLingFu - 1]

        local words = {}
        table.insert( words,word1 )
        table.insert( words,word2 )
        table.insert( words,word3 )
        for i,v in ipairs(words) do
            for idx = 0, v.Length - 1 do
                local wd = Word_Word.GetData(v[idx])
                if wd ~= nil then
                    table.insert( t, SafeStringFormat3(LocalString.GetString("【%s】%s"), wd.Name, wd.Description))
                end
            end
        end

        local str = table.concat( t, "\n" )

        self.descLabel.text = CChatLinkMgr.TranslateToNGUIText(str, false)
        self.needShowTime = (buff.TimeHidden == 0)
        self.descBg.height = math.floor(math.abs(self.descLabel.transform.localPosition.y - self.descLabel.localSize.y - 4))
        self.lastUpdateTime = 0
        if self.OnDescLabelChanged ~= nil then
            self.OnDescLabelChanged()
        end
    end
end
function CLuaBuffListItem:InitQMPKBuff( engineId, di, tian, ren, fishGroupTbl, fishWordTbl)
    if self.objId == engineId and math.floor(self.buffId / 100) == math.floor(CLuaBuffListItem.QMPKPrayBuffId / 100) then
        local buff = Buff_Buff.GetData(self.buffId)
        if buff == nil then
            return
        end

        local t = {}
        table.insert( t, CPropertyBuff.GetBuffDisplay(self.buffId, engineId))
        table.insert( t, SafeStringFormat3(LocalString.GetString("[c][%s]传家之宝[-]"), nameColor))

        local words = {}
        local type = {di, tian, ren}
        for i,v in ipairs(type) do
            local prayData = QuanMinPK_Pray.GetData(v)
            if prayData then
                local word = prayData.Words
                for i=1,word.Length do
                    table.insert( words,word[i-1] )
                end
            end
        end

        local color = CClientChuanJiaBaoMgr.Inst:GetChuanjiaboBuffColor(3)
        for i,v in ipairs(words) do
            local wd = Word_Word.GetData(words[i])
            if wd then
                table.insert( t, (SafeStringFormat3(LocalString.GetString("[c][%s]【%s】%s[-]"), color, wd.Name, wd.Description)))
            end
        end

        table.insert( t,"\r")
        table.insert( t,SafeStringFormat3(LocalString.GetString("[c][%s]佑宅福鱼[-][/c]"), nameColor) )
    
        for i=1,#fishWordTbl do
            local wd = Word_Word.GetData(fishWordTbl[i])
            if wd ~= nil then
                table.insert( t,wd.Description )
            end
        end
        if #fishWordTbl == 0 then
            table.insert( t,LocalString.GetString("暂无") )
        end

        self.descLabel.text = CChatLinkMgr.TranslateToNGUIText(table.concat( t, "\n" ), false)
        self.needShowTime = (buff.TimeHidden == 0)
        self.descBg.height = math.floor(math.abs(self.descLabel.transform.localPosition.y - self.descLabel.localSize.y - 4))
        self.lastUpdateTime = 0
        if self.OnDescLabelChanged ~= nil then
            self.OnDescLabelChanged()
        end
    end
end

function CLuaBuffListItem:InitHouseBuff( buff, objId)
    self.prayStartTime = 0
    if CClientMainPlayer.Inst == nil then
        return
    end
    if CClientMainPlayer.Inst.EngineId == objId then
        local prayInfo = CClientMainPlayer.Inst.PlayProp.HousePrayWords
        local words = {}
        CommonDefs.DictIterate(prayInfo.Words, DelegateFactory.Action_object_object(function (___key, ___value)
            table.insert( words,___key )
        end))
        local wordkedZhenZhaiFishWords = {}
        CommonDefs.DictIterate(prayInfo.WorkedZhengzhaiFishWord, DelegateFactory.Action_object_object(function (___key, ___value)
            table.insert( wordkedZhenZhaiFishWords,___key )
        end))

        self:InitHousePrayBuff(prayInfo.Score, words, wordkedZhenZhaiFishWords)

        self.prayStartTime = CServerTimeMgr.ConvertTimeStampToZone8Time(prayInfo.StartTime)
    else
        Gac2Gas.QueryPlayerCurrentPrayWords(objId)
    end
end

function CLuaBuffListItem:InitAntiProfessionBuff(buffId, objId)
    local effectsTable = nil
    if CClientMainPlayer.Inst.EngineId == objId then
        effectsTable = {}
        CommonDefs.DictIterate(CClientMainPlayer.Inst.SkillProp.AntiProfessionMul, DelegateFactory.Action_object_object(function(___key, ___value)
            if ___value ~= 0 then
                local proClass = CommonDefs.ConvertIntToEnum(typeof(EnumClass), ___key)
                local name = Profession.GetFullName(proClass)
                local mul = SoulCore_AntiProfessionLevel.GetData(___value).AntiProfessionMul
                local desc = SafeStringFormat3(LocalString.GetString("对%s伤害 +%.1f%%"), name , mul * 100)
                table.insert(effectsTable, {desc = desc, key = ___key, value = mul})
            end
        end))
    else
        local obj = CClientObjectMgr.Inst:GetObject(self.objId)
        if obj ~= nil and CommonDefs.DictContains(obj.BuffProp.Buffs, typeof(UInt32), self.buffId) then
            local buffObj = CommonDefs.DictGetValue(obj.BuffProp.Buffs, typeof(UInt32), self.buffId)

            if buffObj then
                if CommonDefs.DictContains(buffObj.EffectValues, typeof(UInt32), 13010007) then
                    local effects = CommonDefs.DictGetValue(buffObj.EffectValues, typeof(UInt32), 13010007)
                    effectsTable = {}
                    CommonDefs.DictIterate(effects.Values, DelegateFactory.Action_object_object(function (___key, ___value)
                        local data = SoulCore_AntiProfessionBuff.GetData(___key)
                        if data ~= nil and ___value ~= 0 then
                            local desc = SafeStringFormat3(data.Name, SafeStringFormat3(" +%.1f%%", ___value * 100))
                            table.insert(effectsTable, {desc = desc, key = ___key, value = ___value})
                        end
                    end))
                end
            end
        end
    end


    if effectsTable ~= nil then
        table.sort(effectsTable, function(a , b)
            if a.value > b.value then
                return true
            elseif a.value == b.value then
                return a.key < b.key
            end
            return false
        end)

        local rowsTable = {}
        for i = 1, #effectsTable do
            table.insert(rowsTable, effectsTable[i].desc)
        end

        local str = table.concat(rowsTable, "\n")
        self.descLabel.text = CChatLinkMgr.TranslateToNGUIText(str, false)
    end
end

function CLuaBuffListItem:InitSunBurn()
    -- body
end

function CLuaBuffListItem:OnQueryOtherPlayerPrayResult( engineId, info)
    if self.objId == engineId and (CLuaHouseMgr.IsHouseBuff(self.buffId)) then
        local words = {}
        CommonDefs.DictIterate(info.Words, DelegateFactory.Action_object_object(function (___key, ___value)
            table.insert( words,___key )
        end))
        local wordkedZhenZhaiFishWords = {}
        CommonDefs.DictIterate(info.WorkedZhengzhaiFishWord, DelegateFactory.Action_object_object(function (___key, ___value)
            table.insert( wordkedZhenZhaiFishWords,___key )
        end))

        self:InitHousePrayBuff(info.Score, words, wordkedZhenZhaiFishWords)
        self.prayStartTime = CServerTimeMgr.ConvertTimeStampToZone8Time(info.StartTime)
    end
end
function CLuaBuffListItem:InitHousePrayBuff( score, words,wordkedZhenZhaiFishWords)
    local buff = Buff_Buff.GetData(self.buffId)
    if buff == nil then
        return
    end

    local t = {}
    table.insert( t,CPropertyBuff.GetBuffDisplay(self.buffId, self.objId))
    table.insert( t,SafeStringFormat3(LocalString.GetString("[c][%s]传家之宝(评分:%s)[-]"), nameColor, score) )

    local color = CClientChuanJiaBaoMgr.Inst:GetChuanjiaboBuffColor(#words)
    for i=1,#words do
        local wd = Word_Word.GetData(words[i])
        if wd ~= nil then
            table.insert( t,SafeStringFormat3(LocalString.GetString("[c][%s]【%s】%s[-]"), color, wd.Name, wd.Description) )
        end
    end

    table.insert( t,"\r")
    table.insert( t,SafeStringFormat3(LocalString.GetString("[c][%s]佑宅福鱼[-][/c]"), nameColor) )

    for i=1,#wordkedZhenZhaiFishWords do
        local wd = Word_Word.GetData(wordkedZhenZhaiFishWords[i])
        if wd ~= nil then
            table.insert( t,wd.Description )
        end
    end
    if #wordkedZhenZhaiFishWords == 0 then
        table.insert( t,LocalString.GetString("暂无") )
    end

    local str = table.concat( t, "\n" )

    self.descLabel.text = CChatLinkMgr.TranslateToNGUIText(str, false)
    self.needShowTime = (buff.TimeHidden == 0)
    self.descBg.height = math.floor(math.abs(self.descLabel.transform.localPosition.y - self.descLabel.localSize.y - 4))
    self.lastUpdateTime = 0

    if self.OnDescLabelChanged ~= nil then
        self.OnDescLabelChanged()
    end
end

function CLuaBuffListItem:Update( )
    if CQuanMinPKMgr.Inst.m_IsQuanMinPKServer and LuaZongMenMgr:IsAntiProfessionBuff(self.buffId) then
        self.remainTimeLabel.text = SafeStringFormat3(LocalString.GetString("[c][%s]剩余时间:[-][%s]永久[-][/c]"), nameColor, valueColor)
        return
    end
    if CLuaHouseMgr.IsHouseBuff(self.buffId) then
        if Time.realtimeSinceStartup - self.lastUpdateTime >= 1 then
            self.lastUpdateTime = Time.realtimeSinceStartup
            if not self.needShowTime then
                local str = self.remainTimeLabel.text
                if str and str~="" then
                    self.remainTimeLabel.text = ""
                end
                return
            end
            if self.prayStartTime~=0 then
                local ts = CommonDefs.op_Subtraction_DateTime_DateTime(CServerTimeMgr.Inst:GetZone8Time(), self.prayStartTime)
                if ts.TotalSeconds >= 86400 or ts.TotalSeconds < 0 then
                    self.remainTimeLabel.text = SafeStringFormat3(LocalString.GetString("[c][%s]剩余时间:[-][%s]0分0秒[-][/c]"), nameColor, valueColor)
                else
                    local totalLeftSeconds = 86400 - math.floor(ts.TotalSeconds)
                    if totalLeftSeconds >= 86400 then
                        self.remainTimeLabel.text = SafeStringFormat3(LocalString.GetString("[c][%s]剩余时间:[-][%s]%d天%d小时[-][/c]"), nameColor, valueColor, math.floor(totalLeftSeconds / (86400)), math.floor((totalLeftSeconds % (86400)) / 3600))
                    elseif totalLeftSeconds >= 3600 then
                        self.remainTimeLabel.text = SafeStringFormat3(LocalString.GetString("[c][%s]剩余时间:[-][%s]%d小时%d分[-][/c]"), nameColor, valueColor, math.floor(totalLeftSeconds / (3600)), math.floor((totalLeftSeconds % 3600) / 60))
                    else
                        self.remainTimeLabel.text = SafeStringFormat3(LocalString.GetString("[c][%s]剩余时间:[-][%s]%d分%d秒[-][/c]"), nameColor, valueColor, math.floor(totalLeftSeconds / 60), totalLeftSeconds % 60)
                    end
                end
            end
        end
        return
    end
    if Time.realtimeSinceStartup - self.lastUpdateTime >= 1 then
        self.lastUpdateTime = Time.realtimeSinceStartup
        if not self.needShowTime then
            local str = self.remainTimeLabel.text
            if str and str~="" then
                self.remainTimeLabel.text = ""
            end
            return
        end

        local obj = CClientObjectMgr.Inst:GetObject(self.objId)
        if obj ~= nil and CommonDefs.DictContains(obj.BuffProp.Buffs, typeof(UInt32), self.buffId) then
            local b = CommonDefs.DictGetValue(obj.BuffProp.Buffs, typeof(UInt32), self.buffId)

            if b.EndTime < 0 then
                self.remainTimeLabel.text = SafeStringFormat3(LocalString.GetString("[c][%s]剩余时间:[-][%s]永久[-][/c]"), nameColor, valueColor)
            else
                local ts = CommonDefs.op_Subtraction_DateTime_DateTime(CServerTimeMgr.Inst:GetZone8Time(), b.receivedTime)
                local buff = Buff_Buff.GetData(self.buffId)
                if ts.TotalSeconds >= b.EndTime or ts.TotalSeconds < 0 then
                    self.remainTimeLabel.text = SafeStringFormat3(LocalString.GetString("[c][%s]剩余时间:[-][%s]0分0秒[-][/c]"), nameColor, valueColor)
                else
                    local totalLeftSeconds = math.floor((b.EndTime - ts.TotalSeconds))
                    if totalLeftSeconds >= 86400 then
                        self.remainTimeLabel.text = SafeStringFormat3(LocalString.GetString("[c][%s]剩余时间:[-][%s]%d天%d小时[-][/c]"), nameColor, valueColor, math.floor(totalLeftSeconds / 86400), math.floor((totalLeftSeconds % 86400) / 3600))
                    elseif totalLeftSeconds >= 3600 then
                        self.remainTimeLabel.text = SafeStringFormat3(LocalString.GetString("[c][%s]剩余时间:[-][%s]%d小时%d分[-][/c]"), nameColor, valueColor, math.floor(totalLeftSeconds / 3600), math.floor((totalLeftSeconds % 3600) / 60))
                    else
                        self.remainTimeLabel.text = SafeStringFormat3(LocalString.GetString("[c][%s]剩余时间:[-][%s]%d分%d秒[-][/c]"), nameColor, valueColor, math.floor(totalLeftSeconds / 60), totalLeftSeconds % 60)
                    end
                end
            end
        else
            self.remainTimeLabel.text = SafeStringFormat3(LocalString.GetString("[c][%s]剩余时间:[-][%s]0分0秒[-][/c]"), nameColor, valueColor)
        end
    end
end
