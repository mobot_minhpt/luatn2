local CButton = import "L10.UI.CButton"
local UIScrollView = import "UIScrollView"
local UIGrid = import "UIGrid"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CUITexture = import "L10.UI.CUITexture"
local Extensions = import "Extensions"
local CIMMgr = import "L10.Game.CIMMgr"
local FriendItemData = import "FriendItemData"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
local Constants = import "L10.Game.Constants"
local CItemMgr = import "L10.Game.CItemMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CChatHelper = import "L10.UI.CChatHelper"

LuaHuiLiuFuliWindow = class()

RegistChildComponent(LuaHuiLiuFuliWindow, "FriendView", GameObject)
RegistChildComponent(LuaHuiLiuFuliWindow, "StrangerView", GameObject)
RegistChildComponent(LuaHuiLiuFuliWindow, "BackView", GameObject)

--RegistClassMember(LuaHuiLiuFuliWindow, "FundInfos")


function LuaHuiLiuFuliWindow:Init()
  if LuaHuiLiuMgr.InviterInfo and LuaHuiLiuMgr.InviterInfo.inviter then
    self:InitInviterView()
  elseif LuaHuiLiuMgr.InviterInfo and LuaHuiLiuMgr.InviterInfo.task then
    self:InitBackView()
  end
end

function LuaHuiLiuFuliWindow:OnEnable()
    g_ScriptEvent:AddListener("UpdateWelfareNewHuiLiu", self, "Init")

    --
end

function LuaHuiLiuFuliWindow:OnDisable()
    g_ScriptEvent:RemoveListener("UpdateWelfareNewHuiLiu", self, "Init")
    self:CancelTaskTick()
    self:CancelRecieveTick()
end

function LuaHuiLiuFuliWindow:InitInviterNode(node, playerId, playerInfo, firstClick)
  node.transform:Find('text'):GetComponent(typeof(UILabel)).text = ''
  node.transform:Find('NameLabel'):GetComponent(typeof(UILabel)).text = playerInfo.name
  local playerNodeBtn = node.transform:Find('Border/Portrait').gameObject
  --CUICommonDef.GetPortraitName(CClientMainPlayer.Inst.Class, CClientMainPlayer.Inst.Gender, -1)
  playerNodeBtn:GetComponent(typeof(CUITexture)):LoadNPCPortrait(playerInfo.portraitName, false)
  node.transform:Find('LevelLabel'):GetComponent(typeof(UILabel)).text = playerInfo.isInXianShenStatus and SafeStringFormat3("[c][%s]%d[-][/c]", Constants.ColorOfFeiSheng, playerInfo.level) or SafeStringFormat3("%d", playerInfo.level)
  local onPlayerIconClick = function(go)
    CPlayerInfoMgr.ShowPlayerPopupMenu(playerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), CPlayerInfoMgr.AlignType.Default)
  end
  CommonDefs.AddOnClickListener(playerNodeBtn,DelegateFactory.Action_GameObject(onPlayerIconClick),false)

  local opBtn = node
  local cbutton = node:GetComponent(typeof(CButton))
  local onNodeClick = function(go)
    if not self.cButtonSelect then
      self.cButtonSelect = cbutton
    elseif self.cButtonSelect then
      self.cButtonSelect.Selected = false
    end
    cbutton.Selected = true
    self.cButtonSelect = cbutton
    self.selectHuiLiuPlayerId = playerId
    self.selectHuiLiuPlayerName = playerInfo.name
  end

  CommonDefs.AddOnClickListener(opBtn,DelegateFactory.Action_GameObject(onNodeClick),false)

  if firstClick then
    onNodeClick()
  end
end

function LuaHuiLiuFuliWindow:InitInviterView()
  self.FriendView:SetActive(true)
  self.StrangerView:SetActive(false)
  self.BackView:SetActive(false)

  local _scrollView = self.FriendView.transform:Find('ScrollView'):GetComponent(typeof(UIScrollView))
  local _table = self.FriendView.transform:Find('ScrollView/Grid'):GetComponent(typeof(UIGrid))
  local _templateNode = self.FriendView.transform:Find('TemplateNode').gameObject
  _templateNode:SetActive(false)

	self.cButtonSelect = nil
	Extensions.RemoveAllChildren(_table.transform)

  for i,v in ipairs(LuaHuiLiuMgr.InviterInfo.inviter) do
    local node = NGUITools.AddChild(_table.gameObject,_templateNode)
    node:SetActive(true)

    local playerId = tonumber(v)

    local playerBasicInfo = CIMMgr.Inst:GetBasicInfo(playerId)
    local playerInfo = CreateFromClass(FriendItemData, playerBasicInfo)

    if playerInfo then
      local firstClick = false
      if i == 1 then
        firstClick = true
      end

      self:InitInviterNode(node,playerId,playerInfo,firstClick)
    end
  end

  for i,v in ipairs(LuaHuiLiuMgr.InviterInfo.inviter2) do
    local node = NGUITools.AddChild(_table.gameObject,_templateNode)
    node:SetActive(true)

    local playerId = tonumber(v[1])

		--table.insert(LuaHuiLiuMgr.InviterInfo.inviter2,{playerId, name, class, gender, level, xianshen})
    local playerInfo = {name = v[2],
    portraitName = CUICommonDef.GetPortraitName(tonumber(v[3]), tonumber(v[4]), -1),
    level = tonumber(v[5]),
    isInXianShenStatus = v[6],}

    self:InitInviterNode(node,playerId,playerInfo,false)
  end

	_scrollView:ResetPosition()
	_table:Reposition()

  local submitBtn = self.FriendView.transform:Find('Button').gameObject
  local onSubmitClick = function(go)
    if self.selectHuiLiuPlayerId and self.selectHuiLiuPlayerName then
      local msg2 = SafeStringFormat3(LocalString.GetString("绑定后不可修改，是否确认绑定[c][FFED5F]%s[-]作为召回人？"), self.selectHuiLiuPlayerName)
      MessageWndManager.ShowOKCancelMessage(msg2, DelegateFactory.Action(function ()
        Gac2Gas.RequestHuiGuiInviteBind(self.selectHuiLiuPlayerId)
      end), nil, nil, nil, false)
    else
      g_MessageMgr:ShowMessage('CUSTOM_STRING2',LocalString.GetString('请选中一位玩家作为召回人'))
    end
  end
  CommonDefs.AddOnClickListener(submitBtn,DelegateFactory.Action_GameObject(onSubmitClick),false)

	local bonusInfo = g_LuaUtil:StrSplit(HuiLiuNew_Setting.GetData().ZhaoHuiTaskRewards,',')
	Extensions.RemoveAllChildren(self.FriendView.transform:Find('ItemFather'))
	self.FriendView.transform:Find('Item').gameObject:SetActive(false)
	self:InitItemTableShow(bonusInfo,self.FriendView.transform:Find('ItemFather').gameObject,self.FriendView.transform:Find('Item').gameObject,128)

  self.TimeHintLabel = self.FriendView.transform:Find('time'):GetComponent(typeof(UILabel))
  self:CancelTaskTick()
  self:CancelRecieveTick()
  self:ReceiveCountDown()
  self.ReceiveCountDownTick = RegisterTick(function ()
    self:ReceiveCountDown()
  end, 1000)
end

function LuaHuiLiuFuliWindow:InitItemTableShow(rewardInfo,rewardFatherNode,itemTemplate,distance)
	local count = 0
	local length = #rewardInfo
	for i=1,length,2 do
		local itemTemplateId = tonumber(rewardInfo[i])
		local itemData = CItemMgr.Inst:GetItemTemplate(itemTemplateId)
		local itemNum = tonumber(rewardInfo[i+1])
		local rewardNode = NGUITools.AddChild(rewardFatherNode,itemTemplate)
		rewardNode:SetActive(true)
		rewardNode.transform.localPosition = Vector3(count * distance,0,0)
    if itemNum and itemNum > 1 then
      rewardNode.transform:Find('Normal/DescLabel'):GetComponent(typeof(UILabel)).text = itemNum
    else
      rewardNode.transform:Find('Normal/DescLabel'):GetComponent(typeof(UILabel)).text = ''
    end
		rewardNode.transform:Find('Normal/IconTexture'):GetComponent(typeof(CUITexture)):LoadMaterial(itemData.Icon)

		UIEventListener.Get(rewardNode).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(rewardNode).onClick, DelegateFactory.VoidDelegate(function (go)
			CItemInfoMgr.ShowLinkItemTemplateInfo(itemTemplateId, false, nil, AlignType.Default, 0, 0, 0, 0)
		end), true)
		count = count + 1
	end
end

function LuaHuiLiuFuliWindow:InitPlayerIconNode(node,playerInfo,playerId)
  node.transform:Find('NameLabel'):GetComponent(typeof(UILabel)).text = playerInfo.name
  local playerNodeBtn = node.transform:Find('Border/Portrait').gameObject
  playerNodeBtn:GetComponent(typeof(CUITexture)):LoadNPCPortrait(playerInfo.portraitName, false)
  node.transform:Find('LevelLabel'):GetComponent(typeof(UILabel)).text = playerInfo.isInXianShenStatus and SafeStringFormat3("[c][%s]%d[-][/c]", Constants.ColorOfFeiSheng, playerInfo.level) or SafeStringFormat3("%d", playerInfo.level)
  local onPlayerIconClick = function(go)
    CPlayerInfoMgr.ShowPlayerPopupMenu(playerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), CPlayerInfoMgr.AlignType.Default)
  end
  CommonDefs.AddOnClickListener(playerNodeBtn,DelegateFactory.Action_GameObject(onPlayerIconClick),false)

end

function LuaHuiLiuFuliWindow:UpdateTaskInfoSingleNode(node,v)
	local taskId = v[1]
	local progress = v[2]
	local isRewarded = v[3]
	local stage = v[4] or 1

	local taskInfo = HuiLiuNew_HuiGuiTask.GetData(taskId)
	if taskInfo then
		node.transform:Find('text1'):GetComponent(typeof(UILabel)).text = taskInfo.Name
		local targetTable = g_LuaUtil:StrSplit(taskInfo.Target,',')
		local rewardTable = g_LuaUtil:StrSplit(taskInfo.Reward,';')

		local targetNum = tonumber(targetTable[stage])
		local rewardInfo = g_LuaUtil:StrSplit(rewardTable[stage],',')

		local percent = progress/targetNum
		if percent > 1 then
			percent = 1
		end

		if isRewarded then
			node.transform:Find('status/OpButton').gameObject:SetActive(false)
			node.transform:Find('status/done').gameObject:SetActive(true)
			node.transform:Find('status/doing').gameObject:SetActive(false)
		else
			if percent >= 1 then
				local receiveBtn = node.transform:Find('status/OpButton').gameObject
				receiveBtn:SetActive(true)
				node.transform:Find('status/done').gameObject:SetActive(false)
				node.transform:Find('status/doing').gameObject:SetActive(false)

				local onClick = function(go)
					Gac2Gas.RequestGetHuiGuiTaskReward(LuaHuiLiuMgr.InviterInfo.inviterId,taskId)
				end
				CommonDefs.AddOnClickListener(receiveBtn,DelegateFactory.Action_GameObject(onClick),false)
			else
				node.transform:Find('status/OpButton').gameObject:SetActive(false)
				node.transform:Find('status/done').gameObject:SetActive(false)
				node.transform:Find('status/doing').gameObject:SetActive(true)
        local leftTime = LuaHuiLiuMgr.InviterInfo.taskExpireTime - CServerTimeMgr.Inst.timeStamp
        if leftTime <= 0 then
          node.transform:Find('status/doing'):GetComponent(typeof(UILabel)).text = LocalString.GetString('未完成')
        else
          node.transform:Find('status/doing'):GetComponent(typeof(UILabel)).text = LocalString.GetString('进行中')
        end
			end
		end

		node.transform:Find('text2'):GetComponent(typeof(UILabel)).text = progress .. '/' .. targetNum
		node.transform:Find('slider'):GetComponent(typeof(UISprite)).width = percent * 295

		local itemTemplate = node.transform:Find('Item1').gameObject
		local rewardFatherNode = node.transform:Find('ItemNode').gameObject
		itemTemplate:SetActive(false)

		local count = 0
		local length = #rewardInfo
		for i=1,length,2 do
			local itemTemplateId = tonumber(rewardInfo[i])
			local itemData = CItemMgr.Inst:GetItemTemplate(itemTemplateId)
			local itemNum = tonumber(rewardInfo[i+1])
			local rewardNode = NGUITools.AddChild(rewardFatherNode,itemTemplate)
			rewardNode:SetActive(true)
			rewardNode.transform.localPosition = Vector3(count * 128,0,0)
			rewardNode.transform:Find('Normal/DescLabel'):GetComponent(typeof(UILabel)).text = itemNum
			rewardNode.transform:Find('Normal/IconTexture'):GetComponent(typeof(CUITexture)):LoadMaterial(itemData.Icon)

			UIEventListener.Get(rewardNode).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(rewardNode).onClick, DelegateFactory.VoidDelegate(function (go)
				CItemInfoMgr.ShowLinkItemTemplateInfo(itemTemplateId, false, nil, AlignType.Default, 0, 0, 0, 0)
			end), true)
			count = count + 1
		end

	end
end

function LuaHuiLiuFuliWindow:InitTaskInfo()
  local _scrollView = self.BackView.transform:Find('ScrollView'):GetComponent(typeof(UIScrollView))
  local _table = self.BackView.transform:Find('ScrollView/Grid'):GetComponent(typeof(UIGrid))
  local _templateNode = self.BackView.transform:Find('TemplateNode').gameObject
  _templateNode:SetActive(false)

	Extensions.RemoveAllChildren(_table.transform)

	for i,v in ipairs(LuaHuiLiuMgr.InviterInfo.task) do
		local node = NGUITools.AddChild(_table.gameObject,_templateNode)
		node:SetActive(true)
		self:UpdateTaskInfoSingleNode(node,v)
	end
	_scrollView:ResetPosition()
	_table:Reposition()
end

function LuaHuiLiuFuliWindow:InitBackView()
  self.FriendView:SetActive(false)
  self.StrangerView:SetActive(false)
  self.BackView:SetActive(true)

  if not CClientMainPlayer.Inst then
    self.BackView:SetActive(false)
    return
  end

  local playerId = LuaHuiLiuMgr.InviterInfo.inviterId
  local playerInfo = CreateFromClass(FriendItemData, CIMMgr.Inst:GetBasicInfo(playerId))
  self:InitPlayerIconNode(self.BackView.transform:Find('LeftInfo/info1').gameObject,playerInfo,playerId)
  self:InitPlayerIconNode(self.BackView.transform:Find('LeftInfo/info2').gameObject,{name = CClientMainPlayer.Inst.Name,isInXianShenStatus = CClientMainPlayer.Inst.IsInXianShenStatus,
  level = CClientMainPlayer.Inst.Level,portraitName = CClientMainPlayer.Inst.PortraitName},CClientMainPlayer.Inst.Id)
  local chatBtn = self.BackView.transform:Find('LeftInfo/info1/Button').gameObject
  local onChatClick = function(go)
		CChatHelper.ShowFriendWnd(playerId, playerInfo.name)
  end
  CommonDefs.AddOnClickListener(chatBtn,DelegateFactory.Action_GameObject(onChatClick),false)

  self:InitTaskInfo()

  local shopBtn = self.BackView.transform:Find('LeftInfo/Button').gameObject
  local onShopClick = function(go)
    Gac2Gas.RequestOpenHuiGuiShop()
  end
  CommonDefs.AddOnClickListener(shopBtn,DelegateFactory.Action_GameObject(onShopClick),false)

  local tipBtn = self.BackView.transform:Find('TipBtn').gameObject
  local onTipClick = function(go)
    g_MessageMgr:ShowMessage('HuiGuiBonusTip')
  end
  CommonDefs.AddOnClickListener(tipBtn,DelegateFactory.Action_GameObject(onTipClick),false)

  self.BackView.transform:Find('LeftInfo/score'):GetComponent(typeof(UILabel)).text = LocalString.GetString('回归积分 ') .. LuaHuiLiuMgr.InviterInfo.huiguiScore
  self.TaskTimeLabel = self.BackView.transform:Find('time'):GetComponent(typeof(UILabel))
  self:CancelTaskTick()
  self:CancelRecieveTick()
  self:TaskCountDown()
  self.TaskCountDownTick = RegisterTick(function ()
    self:TaskCountDown()
  end, 1000)
end

--@desc 领取倒计时相关处理
function LuaHuiLiuFuliWindow:ReceiveCountDown()
    -- 计算领取倒计时
    if LuaHuiLiuMgr.InviterInfo and LuaHuiLiuMgr.InviterInfo.expireTime then
      local leftTime = LuaHuiLiuMgr.InviterInfo.expireTime - CServerTimeMgr.Inst.timeStamp
      if leftTime > 0 then
        self.TimeHintLabel.text = SafeStringFormat3(LocalString.GetString("剩余时间 %s"), self:ParseTime(leftTime))
      else
        self.TimeHintLabel.text = LocalString.GetString("已超时")
        self:CancelRecieveTick()
      end
    end
end

--@desc 购买倒计时相关处理
function LuaHuiLiuFuliWindow:TaskCountDown()
    -- 计算购买倒计时
    if LuaHuiLiuMgr.InviterInfo and LuaHuiLiuMgr.InviterInfo.taskExpireTime then
      local leftTime = LuaHuiLiuMgr.InviterInfo.taskExpireTime - CServerTimeMgr.Inst.timeStamp
      if leftTime > 0 then
        self.TaskTimeLabel.text = SafeStringFormat3(LocalString.GetString("任务时限 %s"), self:ParseTime(leftTime))
      else
        self.TaskTimeLabel.text = LocalString.GetString("已超时")
        self:CancelTaskTick()
      end
    end
end

function LuaHuiLiuFuliWindow:CancelTaskTick()
  if self.TaskCountDownTick then
    UnRegisterTick(self.TaskCountDownTick)
    self.TaskCountDownTick = nil
  end
end

function LuaHuiLiuFuliWindow:CancelRecieveTick()
  if self.ReceiveCountDownTick then
    UnRegisterTick(self.ReceiveCountDownTick)
    self.ReceiveCountDownTick = nil
  end
end

--@desc 处理修改时间格式
function LuaHuiLiuFuliWindow:ParseTime(leftTime)
    if leftTime <= 0 then
        return LocalString.GetString("已过期")
    end

    if leftTime < 24 * 3600 then
        local hour = math.floor(leftTime / 3600)
        local minute = math.floor(leftTime % 3600 / 60)
        return SafeStringFormat3("%d:%d", hour, minute)
    else
        local days = math.floor(leftTime / (24 * 3600))
        local hour = math.floor(leftTime % (24 * 3600) / 3600)
        return SafeStringFormat3(LocalString.GetString("%d天%d小时"), days, hour)
    end
end


return LuaHuiLiuFuliWindow
