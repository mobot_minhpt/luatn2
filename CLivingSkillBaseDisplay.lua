-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CLivingSkillBaseDisplay = import "L10.UI.CLivingSkillBaseDisplay"
local CLivingSkillMgr = import "L10.Game.CLivingSkillMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local LifeSkill_Skill = import "L10.Game.LifeSkill_Skill"
local LifeSkill_SkillType = import "L10.Game.LifeSkill_SkillType"
local LocalString = import "LocalString"
local UInt32 = import "System.UInt32"
CLivingSkillBaseDisplay.m_Init_CS2LuaHook = function (this) 
    if CClientMainPlayer.Inst == nil then
        return
    end

    local skillLevel = 0
    if CommonDefs.DictContains(CClientMainPlayer.Inst.SkillProp.LifeSkillGradeMap, typeof(UInt32), CLivingSkillMgr.Inst.selectedSkill) then
        skillLevel = CommonDefs.DictGetValue(CClientMainPlayer.Inst.SkillProp.LifeSkillGradeMap, typeof(UInt32), CLivingSkillMgr.Inst.selectedSkill)
    end
    local data = LifeSkill_SkillType.GetData(CLivingSkillMgr.Inst.selectedSkill)
    this.skillNameLabel.text = (data.Name .. "    Lv.") .. tostring(skillLevel)
    this.skillDescLabel.text = data.Intro

    this.icon:LoadSkillIcon(data.SkillIcon)

    local lv = CClientMainPlayer.Inst.MaxLevel
    local selectedSkill = CLivingSkillMgr.Inst.selectedSkill
    local maxLifeSkillLv = 1
    for i = CLivingSkillMgr.MaxLifeSkillLevel, 1, - 1 do
        local skillId = (selectedSkill * 100 + i)
        local skillData = LifeSkill_Skill.GetData(skillId)
        if skillData ~= nil then
            if skillData.GraLimit <= lv then
                maxLifeSkillLv = i
                break
            end
        end
    end
    this.maxLvLabel.text = System.String.Format(LocalString.GetString("等级上限lv.{0}"), maxLifeSkillLv)
end
