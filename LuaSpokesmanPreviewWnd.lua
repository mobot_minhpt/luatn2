local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local UIPanel = import "UIPanel"
local AMPlayer = import "L10.Game.CutScene.AMPlayer"
local CUICommonDef = import "L10.UI.CUICommonDef"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local EShareType = import "L10.UI.EShareType"
local PlayerSettings = import "L10.Game.PlayerSettings"
local ShareMgr = import "ShareMgr"

LuaSpokesmanPreviewWnd = class()

RegistChildComponent(LuaSpokesmanPreviewWnd, "m_Texture","Texture",UITexture)
RegistChildComponent(LuaSpokesmanPreviewWnd, "m_ShareButton","ShareButton",GameObject)
RegistChildComponent(LuaSpokesmanPreviewWnd, "m_PlayButton","PlayButton",UISprite)
RegistChildComponent(LuaSpokesmanPreviewWnd, "m_CloseButton","CloseButton",GameObject)

RegistClassMember(LuaSpokesmanPreviewWnd, "m_Data")
RegistClassMember(LuaSpokesmanPreviewWnd, "m_Sound")
function LuaSpokesmanPreviewWnd:Init()
    self.m_Data = SpokesmanTCG_TCG.GetData(CLuaSpokesmanMgr.m_SpokesmanPreviewWndPreviewCardID)
    local url = ShareMgr.GetWebImagePath(self.m_Data.BigIcon)
    CPersonalSpaceMgr.DownLoadPic(url, self.m_Texture, 1080, 1920, DelegateFactory.Action(function ()

    end), false, true)
    self.m_PlayButton.gameObject:SetActive(self.m_Data.Type ~= 1)
    self.m_PlayButton.transform:Find("Sprite").gameObject:SetActive(self.m_Data.Type ~= 2)
    self.m_PlayButton.spriteName = self.m_Data.Type == 2 and "common_button_broadcast" or "common_btn_05"
    UIEventListener.Get(self.m_ShareButton).onClick = DelegateFactory.VoidDelegate(function (p)
        self:Share()
    end)
    UIEventListener.Get(self.m_PlayButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
        self:Play()
    end)
    self.gameObject:GetComponent(typeof(UIPanel)).IgnoreIphoneXMargin = true
end

function LuaSpokesmanPreviewWnd:OnDisable()
    if self.m_Sound then
        SoundManager.Inst:StopSound(self.m_Sound)
        self.m_Sound = nil
    end
end

function LuaSpokesmanPreviewWnd:Play()
    if self.m_Data.Type == 2 then
        if PlayerSettings.VolumeSetting == 0 then
            PlayerSettings.VolumeSetting = 1
        end
        PlayerSettings.SoundEnabled = true
        if self.m_Sound~=nil then
            SoundManager.Inst:StopSound(self.m_Sound)
            self.m_Sound = nil
        end
        self.m_Sound = SoundManager.Inst:PlayOneShot(self.m_Data.Audio, Vector3.zero, nil, 0, -1)
    elseif self.m_Data.Type == 3 then
        AMPlayer.Inst:PlayCG(self.m_Data.Url, DelegateFactory.Action(function () end), 1)
    end
end

function LuaSpokesmanPreviewWnd:Share()
    if not CClientMainPlayer.Inst then return end
    self.m_ShareButton:SetActive(false)
    self.m_PlayButton.gameObject:SetActive(false)
    self.m_CloseButton:SetActive(false)
    CUICommonDef.CaptureScreen(
            "screenshot",
            true,
            false,
            self.m_ShareButton,
            DelegateFactory.Action_string_bytes(
                    function(fullPath, jpgBytes)
                        self.m_ShareButton:SetActive(true)
                        self.m_PlayButton.gameObject:SetActive(self.m_Data.Type ~= 1)
                        self.m_CloseButton:SetActive(true)
                        if CLuaShareMgr.IsOpenShare() then
                            ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, fullPath)
                        else
                            CLuaShareMgr.SimpleShareLocalImage2PersonalSpace(fullPath)
                        end
                    end
            ),
            false
    )
end
