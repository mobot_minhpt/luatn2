local CBaseWnd = import "L10.UI.CBaseWnd"
local CFamilyTreeMgr = import "L10.Game.CFamilyTreeMgr"
local FamilyRelationShip = import "L10.Game.FamilyRelationShip"
local CUICommonDef = import "L10.UI.CUICommonDef"
local EnumQualityType = import "L10.Game.EnumQualityType"
local UILabel = import "UILabel"
local UISprite = import "UISprite"
local CUITexture=import "L10.UI.CUITexture"
local QnCheckBox = import "L10.UI.QnCheckBox"
local UIEventListener = import "UIEventListener"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CTooltipAlignType = import "L10.UI.CTooltip+AlignType"
local CItemMgr = import "L10.Game.CItemMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local Mall_LingYuMall = import "L10.Game.Mall_LingYuMall"
local FamilyTree_Setting = import "L10.Game.FamilyTree_Setting"
local Item_Item = import "L10.Game.Item_Item"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local PlayerPrefs = import "UnityEngine.PlayerPrefs"
local CShopMallMgr = import "L10.UI.CShopMallMgr"

LuaDelTagWnd=class()

RegistClassMember(LuaDelTagWnd,"CancelButton")
RegistClassMember(LuaDelTagWnd,"OKButton")
RegistClassMember(LuaDelTagWnd,"DesLabel")
RegistClassMember(LuaDelTagWnd,"AutoLingyuBox")
RegistClassMember(LuaDelTagWnd,"selfMember")
RegistClassMember(LuaDelTagWnd,"CostItemId")
RegistClassMember(LuaDelTagWnd,"CostAndOwnMoney")
RegistClassMember(LuaDelTagWnd,"ItemBtn")
RegistClassMember(LuaDelTagWnd,"ItemCount")
RegistClassMember(LuaDelTagWnd,"ItemMask")
RegistClassMember(LuaDelTagWnd,"itemIcon")
RegistClassMember(LuaDelTagWnd,"itemQuality")
RegistClassMember(LuaDelTagWnd,"ItemData")
RegistClassMember(LuaDelTagWnd,"AutoCost")
RegistClassMember(LuaDelTagWnd,"UseLingyu")

function LuaDelTagWnd:Awake()
    self.CostItemId = FamilyTree_Setting.GetData().TagCostItemId
    self.CancelButton = self.transform:Find("Anchor/Grid/CancelButton").gameObject
    self.OKButton = self.transform:Find("Anchor/Grid/OKButton").gameObject
    self.DesLabel = self.transform:Find("Anchor/DesLabel"):GetComponent(typeof(UILabel))
    self.selfMember = CFamilyTreeMgr.Instance:GetSingleMember(FamilyRelationShip.Self)
    self.DesLabel.text = g_MessageMgr:FormatMessage("Del_Tag_Confirm",luaFamilyMgr.m_TagIndexData[luaFamilyMgr.DelTagId].tagName)
    self.ItemBtn = self.transform:Find("Anchor/Item").gameObject
    self.ItemMask = self.transform:Find("Anchor/Item/Acquire").gameObject
    self.ItemCount = self.transform:Find("Anchor/Item/Count"):GetComponent(typeof(UILabel))
    self.itemIcon = self.transform:Find("Anchor/Item/itemIcon"):GetComponent(typeof(CUITexture))
    self.itemQuality = self.transform:Find("Anchor/Item/Quality"):GetComponent(typeof(UISprite))
    
    self.CostAndOwnMoney = self.transform:Find("Anchor/QnCostAndOwnMoney"):GetComponent(typeof(CCurentMoneyCtrl))
    self.CostAndOwnMoney:SetType(EnumMoneyType.LingYu,EnumPlayScoreKey.NONE,true)
    self.CostAndOwnMoney:SetCost(Mall_LingYuMall.GetData(self.CostItemId).Jade)
    
    self.AutoLingyuBox = self.transform:Find("Anchor/AutoLingyuBox"):GetComponent(typeof(QnCheckBox))
    
    local LocalStr = "LuaDelTagWnd_CostLingyu"
    if PlayerPrefs.GetInt(CClientMainPlayer.Inst.Id..LocalStr) ~= 1 then
        self.AutoLingyuBox:SetSelected(false, false)
        self.AutoCost = false
    else
        self.AutoLingyuBox:SetSelected(true, false)
        self.AutoCost = true
    end

    self.AutoLingyuBox.OnValueChanged = DelegateFactory.Action_bool(function(value)     --  是否消耗灵玉
        self.AutoCost = value
        if  value then
            PlayerPrefs.SetInt(CClientMainPlayer.Inst.Id..LocalStr,1)
        else
            PlayerPrefs.SetInt(CClientMainPlayer.Inst.Id..LocalStr,0)
        end
        self:RefreshCostType()
    end)

    self:InitItemBtn()

    UIEventListener.Get(self.CancelButton).onClick = DelegateFactory.VoidDelegate(function (p) 
        self:Close()
    end)
    UIEventListener.Get(self.OKButton).onClick = DelegateFactory.VoidDelegate(function (p) 
        if self.UseLingyu then
            if not CShopMallMgr.TryCheckEnoughJade(Mall_LingYuMall.GetData(self.CostItemId).Jade, 0) then
                return
            end
        else
            if CItemMgr.Inst:GetItemCount(self.CostItemId) < 1 then
                g_MessageMgr:ShowMessage("TAG_FAMILY_TREE_ITEM_NOT_ENOUGH")
                return
            end
        end

        if luaFamilyMgr.DelTagId ~= "" then
            Gac2Gas.TryTagPlayerFamilyTree(CFamilyTreeMgr.Instance.CenterPlayerID,"", luaFamilyMgr.DelTagId, EnumFamilyTreeTagOperationType.eDel, self.AutoCost) -- body
        end
        self:Close()
    end)
end

function LuaDelTagWnd:OnEnable()
	g_ScriptEvent:AddListener("SendItem", self, "RefreshCostType")
	g_ScriptEvent:AddListener("SetItemAt", self, "RefreshCostType")
end

function LuaDelTagWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SendItem", self, "RefreshCostType")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "RefreshCostType")
end

function LuaDelTagWnd:RefreshCostType()
    if self.AutoLingyuBox.Selected and CItemMgr.Inst:GetItemCount(self.CostItemId) == 0 then
        self.ItemBtn:SetActive(false)
        self.CostAndOwnMoney.gameObject:SetActive(true)
        self.UseLingyu = true
    else
        self.ItemBtn:SetActive(true)
        self.CostAndOwnMoney.gameObject:SetActive(false)
        self.UseLingyu = false
    end
    if CItemMgr.Inst:GetItemCount(self.CostItemId) == 0 then
        self.ItemMask:SetActive(true)
        self.ItemCount.text = SafeStringFormat3("[c][FF0000]%s[-][/c]/1",CItemMgr.Inst:GetItemCount(self.CostItemId))
    else
        self.ItemMask:SetActive(false)
        self.ItemCount.text = SafeStringFormat3("[c][FFFFFF]%s[-][/c]/1",CItemMgr.Inst:GetItemCount(self.CostItemId))
    end
end

function LuaDelTagWnd:InitItemBtn()
    self.itemData = Item_Item.GetData(self.CostItemId)
    self.itemIcon:LoadMaterial(self.itemData.Icon)
    self.itemQuality.spriteName = CUICommonDef.GetItemCellBorder(EnumQualityType.White)
    
    UIEventListener.Get(self.ItemBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        CItemInfoMgr.ShowLinkItemTemplateInfo(self.CostItemId, false, nil, AlignType.Right, 0, 0, 0, 0)
    end)
    UIEventListener.Get(self.ItemMask).onClick = DelegateFactory.VoidDelegate(function (p) 
        CItemAccessListMgr.Inst:ShowItemAccessInfo(self.CostItemId, true,  self.ItemMask.transform, CTooltipAlignType.Right)
    end)
    self:RefreshCostType()
end

function LuaDelTagWnd:Close()
	self.transform:GetComponent(typeof(CBaseWnd)):Close()
end
