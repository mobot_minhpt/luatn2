local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local UISearchView=import "L10.UI.UISearchView"
local QnTableView = import "L10.UI.QnTableView"
local GameObject = import "UnityEngine.GameObject"
local CButton = import "L10.UI.CButton"
local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local Profession=import "L10.Game.Profession"
local EnumClass = import "L10.Game.EnumClass"
local CNumberKeyBoardMgr = import "CNumberKeyBoardMgr"
local CTooltip=import "L10.UI.CTooltip"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaStarBiwuZhanDuiSearchView = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaStarBiwuZhanDuiSearchView, "Consist", "Consist", UILabel)
RegistChildComponent(LuaStarBiwuZhanDuiSearchView, "Score", "Score", UILabel)
RegistChildComponent(LuaStarBiwuZhanDuiSearchView, "Capacity", "Capacity", UILabel)
RegistChildComponent(LuaStarBiwuZhanDuiSearchView, "ListView", "ListView", QnTableView)
RegistChildComponent(LuaStarBiwuZhanDuiSearchView, "BottomView", "BottomView", GameObject)
RegistChildComponent(LuaStarBiwuZhanDuiSearchView, "Empty", "Empty", UILabel)
RegistChildComponent(LuaStarBiwuZhanDuiSearchView, "QnIncreseAndDecreaseButton", "QnIncreseAndDecreaseButton", GameObject)
RegistChildComponent(LuaStarBiwuZhanDuiSearchView, "Create", "Create", CButton)
RegistChildComponent(LuaStarBiwuZhanDuiSearchView, "NormalRoot", "NormalRoot", GameObject)
RegistChildComponent(LuaStarBiwuZhanDuiSearchView, "FreePlayerBtn", "FreePlayerBtn", CButton)
RegistChildComponent(LuaStarBiwuZhanDuiSearchView, "OutOfTime", "OutOfTime", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaStarBiwuZhanDuiSearchView,"m_SearchBtn")
RegistClassMember(LuaStarBiwuZhanDuiSearchView,"m_ZhanDuiBtnText")
RegistClassMember(LuaStarBiwuZhanDuiSearchView,"m_IncreseaBtn")
RegistClassMember(LuaStarBiwuZhanDuiSearchView,"m_DecreaseBtn")
RegistClassMember(LuaStarBiwuZhanDuiSearchView,"m_PageLabel")
RegistClassMember(LuaStarBiwuZhanDuiSearchView,"m_AdvSearchRoot")

RegistClassMember(LuaStarBiwuZhanDuiSearchView,"m_OnePageTotalShowTeam")
RegistClassMember(LuaStarBiwuZhanDuiSearchView,"m_CurrentPage")
RegistClassMember(LuaStarBiwuZhanDuiSearchView,"m_HasZhanDui")
RegistClassMember(LuaStarBiwuZhanDuiSearchView,"m_Source")
RegistClassMember(LuaStarBiwuZhanDuiSearchView,"m_EndStamp")
RegistClassMember(LuaStarBiwuZhanDuiSearchView,"m_TotalPage")
RegistClassMember(LuaStarBiwuZhanDuiSearchView,"m_TableViewDataSource")
RegistClassMember(LuaStarBiwuZhanDuiSearchView,"m_ZhanDuiListHead")
RegistClassMember(LuaStarBiwuZhanDuiSearchView,"m_ZhanduiObj2IdTable")
function LuaStarBiwuZhanDuiSearchView:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.Create.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCreateClick()
	end)


	
	UIEventListener.Get(self.FreePlayerBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnFreePlayerBtnClick()
	end)
	self.m_SearchBtn = self.NormalRoot.transform:Find("SearchView"):GetComponent(typeof(UISearchView))
    self.m_ZhanDuiBtnText = self.Create.transform:Find("Label"):GetComponent(typeof(UILabel))
    self.m_IncreseaBtn = self.QnIncreseAndDecreaseButton.transform:Find("IncreaseButton"):GetComponent(typeof(QnButton))
    self.m_DecreaseBtn = self.QnIncreseAndDecreaseButton.transform:Find("DecreaseButton"):GetComponent(typeof(QnButton))
	self.m_PageLabel =  self.QnIncreseAndDecreaseButton.transform:Find("Label"):GetComponent(typeof(UILabel))
	self.m_AdvSearchRoot = self.BottomView.transform:Find("AdvRoot").gameObject

	UIEventListener.Get(self.m_PageLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) 
		self:OnClickPageLabel(go)
	end)

	UIEventListener.Get(self.m_IncreseaBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) 
		self:OnIncreaseBtnClicked(go) 
	end)
    UIEventListener.Get(self.m_DecreaseBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) 
		self:OnDecreaseBtnClicked(go) 
	end)
    --@endregion EventBind end
    self.OutOfTime.gameObject:SetActive(false)
	self.m_ZhanDuiListHead = {}
    self.m_ZhanDuiListHead[1] = self.Consist
    self.m_ZhanDuiListHead[2] = self.Capacity
    self.m_ZhanDuiListHead[3] = self.Score

	self.m_ZhanduiObj2IdTable = nil
	self.m_SortType = 0
	self.m_CurrentPage = 1
	self.m_HasZhanDui = false
	self.m_Source = 0
	self.m_TotalPage = 1
	self.m_OnePageTotalShowTeam = 7
    self:CheckEndTime()
	self:InitView()
end

function LuaStarBiwuZhanDuiSearchView:CheckEndTime()
    local time = StarBiWuShow_Setting.GetData().Create_ZhanDui_End_Time
    self.m_EndStamp = CServerTimeMgr.Inst:GetTimeStampByStr(time)
end

function LuaStarBiwuZhanDuiSearchView:InitView()
    local function initItem(item,row)
        local index = self.m_OnePageTotalShowTeam * (self.m_CurrentPage - 1) + row + 1
        if index <= #CLuaStarBiwuMgr.m_ZhanduiTable then
            self:InitZhanDuiItem(item, index, row%2==0)
        else
            item.gameObject:SetActive(false)
        end
    end
    local count = self.m_OnePageTotalShowTeam

    local dataSource=DefaultTableViewDataSource.CreateByCount(count,initItem)
    self.ListView.m_DataSource = dataSource

    self.m_SearchBtn.OnSearch =DelegateFactory.Action_string(function(str) self:OnSearchBtnClicked(str) end)

end

function LuaStarBiwuZhanDuiSearchView:InitListHead()
    self.m_SortType = 0
    self.m_ZhanDuiListHead[1].text = LocalString.GetString("职业组成")
    self.m_ZhanDuiListHead[2].text = LocalString.GetString("总战力")
    self.m_ZhanDuiListHead[3].text = LocalString.GetString("明星值")
    for k,v in pairs(self.m_ZhanDuiListHead) do
        UIEventListener.Get(v.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) 
            self:OnListHeaderClick(k)
        end)
    end
end

function LuaStarBiwuZhanDuiSearchView:SetZhanDuiListPage(page)
	self.m_CurrentPage = math.max(1,math.min(page,self.m_TotalPage))
    self.ListView:ReloadData(true, false)
    self.m_PageLabel.text = SafeStringFormat3( "%d/%d",self.m_CurrentPage,self.m_TotalPage )
    if self.m_CurrentPage <= 1 then
        self.m_DecreaseBtn.Enabled = false
    else
        self.m_DecreaseBtn.Enabled = true
    end
    if self.m_CurrentPage >= self.m_TotalPage then
        self.m_IncreseaBtn.Enabled = false
    else
        self.m_IncreseaBtn.Enabled = true
    end
end

function LuaStarBiwuZhanDuiSearchView:ReplyStarBiwuZhanDuiList(selfZhanDuiId, page, totalPage, source)
    self.m_Source = source
    self.m_HasZhanDui = selfZhanDuiId > 0
    self.ListView.gameObject:SetActive(true)
    self:InitListHead()

    if self.m_CurrentPage <= 1 then
        self.m_DecreaseBtn.Enabled = false
    else
        self.m_DecreaseBtn.Enabled = true
    end
    if self.m_CurrentPage >= totalPage then
        self.m_IncreseaBtn.Enabled = false
    else
        self.m_IncreseaBtn.Enabled = true
    end
    totalPage = math.ceil( #CLuaStarBiwuMgr.m_ZhanduiTable / self.m_OnePageTotalShowTeam )
    if totalPage == 0 then
        totalPage = 1
    end
    self.m_TotalPage = totalPage
    self.m_ZhanDuiBtnText.text = self.m_HasZhanDui and LocalString.GetString("我的战队") or LocalString.GetString("创建战队") 
	self.ListView:ReloadData(true, false)
    self.ListView.OnSelectAtRow = DelegateFactory.Action_int(function(index) self:OnSelectAtRow(index) end)
    self.m_PageLabel.text = SafeStringFormat3( "%d/%d",page,totalPage )

    self.m_AdvSearchRoot:SetActive(source == 2)
    self.NormalRoot:SetActive(source == 1)
    if  #CLuaStarBiwuMgr.m_ZhanduiTable > 0 then
        self.Empty.gameObject:SetActive(false)
    else
        self.Empty.gameObject:SetActive(true)
    end
    local isOutOfData = CServerTimeMgr.Inst.timeStamp >= self.m_EndStamp
    self.FreePlayerBtn.gameObject:SetActive(not isOutOfData)
    self.OutOfTime.gameObject:SetActive(isOutOfData)
    self.Create.gameObject:SetActive(not isOutOfData)
end

function LuaStarBiwuZhanDuiSearchView:OnSelectAtRow( index)
    local zhanduiId = 0
    index = self.m_OnePageTotalShowTeam * (self.m_CurrentPage - 1) + index + 1
    if CLuaStarBiwuMgr.m_ZhanduiTable and (index <= #CLuaStarBiwuMgr.m_ZhanduiTable) then
        zhanduiId = CLuaStarBiwuMgr.m_ZhanduiTable[index].m_Id
    end
    CLuaStarBiwuMgr:ShowZhanDuiMemberWnd(index, zhanduiId, 0)
end

function LuaStarBiwuZhanDuiSearchView:InitZhanDuiItem(tableItem, index, isOdd)
    tableItem.gameObject:SetActive(true)
    local zhanDui = CLuaStarBiwuMgr.m_ZhanduiTable[index]
    --local bRetainFlag = CLuaStarBiwuMgr.m_ZhanduiFlagTable[index] > 0
    local transform=tableItem.transform
    local m_NameLabel = transform:Find("Name"):GetComponent(typeof(UILabel))
    local m_SloganLabel = transform:Find("Slogan"):GetComponent(typeof(UILabel))
    local m_MemberGrid = transform:Find("MemberGrid"):GetComponent(typeof(UIGrid))
    local m_Capacity = transform:Find("Capacity"):GetComponent(typeof(UILabel))
	local m_Score = transform:Find("Score"):GetComponent(typeof(UILabel))
    local m_MemberObj = transform:Find("Member").gameObject
    m_MemberObj:SetActive(false)

    --transform:Find("RetainFlag").gameObject:SetActive(bRetainFlag)

    if isOdd then
        tableItem:SetBackgroundTexture(g_sprites.EvenBgSprite)
    else
        tableItem:SetBackgroundTexture(g_sprites.OddBgSpirite)
    end
    m_NameLabel.text = zhanDui.m_Name
    m_SloganLabel.text = zhanDui.m_Slogan
    m_Capacity.text = tostring(zhanDui.m_Capacity)
	m_Score.text = tostring(zhanDui.m_StarScore)
    Extensions.RemoveAllChildren(m_MemberGrid.transform)
    do
        local i = 0 local len = zhanDui.m_Member and zhanDui.m_Member.Length or 0
        while i < len do
            local go = NGUITools.AddChild(m_MemberGrid.gameObject, m_MemberObj)
            go:SetActive(true)
            if zhanDui.m_Member[i] > 0 then
                go:GetComponent(typeof(UISprite)).spriteName= Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), zhanDui.m_Member[i]))
                go.transform:Find("Sprite").gameObject:SetActive(false)
            end
            i = i + 1
        end
    end
    m_MemberGrid:Reposition()

    self:UpdateApplyStatus(transform, zhanDui)
end

function LuaStarBiwuZhanDuiSearchView:OnListHeaderClick(index)
    if not self.m_ZhanDuiListHead or not self.m_ZhanDuiListHead[index] then return end
    self.m_ZhanDuiListHead[1].text = LocalString.GetString("职业组成")
    self.m_ZhanDuiListHead[2].text = LocalString.GetString("总战力")
    self.m_ZhanDuiListHead[3].text = LocalString.GetString("明星值")
    if math.abs(self.m_SortType) == index then
        self.m_SortType = -1 * self.m_SortType
    else
        self.m_SortType = index
    end
    if self.m_SortType > 0 then
        self.m_ZhanDuiListHead[index].text = self.m_ZhanDuiListHead[index].text .. LocalString.GetString("▼")
    elseif self.m_SortType < 0 then
        self.m_ZhanDuiListHead[index].text = self.m_ZhanDuiListHead[index].text .. LocalString.GetString("▲")
    end
    if index == 2 and self.m_SortType ~= 0 then
        table.sort(CLuaStarBiwuMgr.m_ZhanduiTable,function(a,b)
            if self.m_SortType > 0 then
                return a.m_Capacity > b.m_Capacity
            else
                return a.m_Capacity < b.m_Capacity
            end
        end)
    end
    if index == 1 and self.m_SortType ~= 0 then
        table.sort(CLuaStarBiwuMgr.m_ZhanduiTable,function(a,b)
            if self.m_SortType > 0 then
                return a.m_Member.Length > b.m_Member.Length
            else
                return a.m_Member.Length < b.m_Member.Length
            end
        end)
    end

	if index == 3 and self.m_SortType ~= 0 then
        table.sort(CLuaStarBiwuMgr.m_ZhanduiTable,function(a,b)
            if self.m_SortType > 0 then
                return a.m_StarScore > b.m_StarScore
            else
                return a.m_StarScore < b.m_StarScore
            end
        end)
    end

    self.ListView:ReloadData(true, false)
end

function LuaStarBiwuZhanDuiSearchView:UpdateApplyStatus(trans, zhanDui)
	local applyObj = trans:Find("Apply").gameObject
	local applyLabel = applyObj.transform:Find("ApplyName"):GetComponent(typeof(UILabel))
	local fullObj = trans:Find("FullTip").gameObject
	local hasAppliedObj = trans:Find("Apply/Checked").gameObject
	if self.m_HasZhanDui or CServerTimeMgr.Inst.timeStamp >= self.m_EndStamp then
		applyObj.gameObject:SetActive(false)
		fullObj.gameObject:SetActive(false)
		return 
	end
	if not self.m_ZhanduiObj2IdTable then self.m_ZhanduiObj2IdTable = {} end
	self.m_ZhanduiObj2IdTable[trans] = zhanDui
	local bFull = zhanDui.m_Member.Length >= StarBiWuShow_Setting.GetData().Max_ZhanDui_MemberNum
	fullObj:SetActive(bFull)
	applyObj:SetActive(not bFull)
	hasAppliedObj:SetActive(false)
	local bHasApplied = zhanDui.m_HasApplied > 0
	if not bFull then
	  	hasAppliedObj:SetActive(bHasApplied)
	  	applyLabel.text = bHasApplied and LocalString.GetString("取消申请") or LocalString.GetString("申请")
	end
	UIEventListener.Get(applyObj).onClick = LuaUtils.VoidDelegate(function()
	  	if not bHasApplied then
			Gac2Gas.RequestJoinStarBiwuZhanDui(zhanDui.m_Id, "")
	  	else
			Gac2Gas.RequestCancelJoinStarBiwuZhanDui(zhanDui.m_Id)
			applyLabel.text = LocalString.GetString("申请")
			hasAppliedObj:SetActive(false)
			zhanDui.m_HasApplied = 0
			bHasApplied = false
	  	end
	end)
end

function LuaStarBiwuZhanDuiSearchView:RequestJoinStarBiwuZhanDuiSuccess(zhanduiId)
	for k, v in pairs(self.m_ZhanduiObj2IdTable) do
	  	if v.m_Id == zhanduiId then
			v.m_HasApplied = 1
			self:UpdateApplyStatus(k, v)
	  	end
	end
end
  
function LuaStarBiwuZhanDuiSearchView:StarBiwuCancelJoinZhanDuiByPlayer(zhanduiId)
	for k, v in pairs(self.m_ZhanduiObj2IdTable) do
	  	if v.m_Id == zhanduiId then
			v.m_HasApplied = 0
			self:UpdateApplyStatus(k, v)
	  	end
	end
end
--@region UIEvent

function LuaStarBiwuZhanDuiSearchView:OnCreateClick()
	if self.m_HasZhanDui then
		g_ScriptEvent:BroadcastInLua("OnStarBiWuZhanDuiPlatformChangeTab", 1)
    else
        CUIManager.ShowUI(CLuaUIResources.StarBiwuCreateZhanDuiWnd)
    end
end

function LuaStarBiwuZhanDuiSearchView:OnFreePlayerBtnClick()
	CUIManager.ShowUI(CLuaUIResources.StarBiWuFreePlayerWnd)
end


function LuaStarBiwuZhanDuiSearchView:OnClickPageLabel(go)
    local min=0
    local max=math.max(1,self.m_TotalPage)

    local val=max
    local num=0
    while val>=1 do
        num = num+1
        val = val/10
    end

    CNumberKeyBoardMgr.ShowNumberKeyBoardWithRange(min, max, self.m_CurrentPage, num, DelegateFactory.Action_int(function (val)
        self.m_PageLabel.text = tostring(val)
    end), DelegateFactory.Action_int(function (val)
        local page=math.max(1,val)
        page=math.min(self.m_TotalPage,page)
        self.m_PageLabel.text = SafeStringFormat3("%d/%d", page, self.m_TotalPage)
        self:SetZhanDuiListPage(page)
    end), self.m_PageLabel, CTooltip.AlignType.Top, true)
end

function LuaStarBiwuZhanDuiSearchView:OnIncreaseBtnClicked( go)
    if self.m_CurrentPage >= self.m_TotalPage then
        return
    end
    self:SetZhanDuiListPage(self.m_CurrentPage + 1)
end
function LuaStarBiwuZhanDuiSearchView:OnDecreaseBtnClicked( go)
    if self.m_CurrentPage <= 1 then
        return
    end
    self:SetZhanDuiListPage(self.m_CurrentPage - 1)
end

function LuaStarBiwuZhanDuiSearchView:OnSearchBtnClicked(value)
    local id=tonumber(value)
    if not id then
        g_MessageMgr:ShowMessage("QMPK_Search_Zhandui_Only_By_Id")
        return
    end
    Gac2Gas.QueryStarBiwuZhanDuiInfoByMemberId(id)
end

--@endregion UIEvent

function LuaStarBiwuZhanDuiSearchView:OnEnable()
    CLuaStarBiwuMgr:RequestQueryStarBiwuZhanDuiList(1)
	g_ScriptEvent:BroadcastInLua("OnStarBiWuZhanDuiPlatformWndChange", LocalString.GetString("战队平台"))
	g_ScriptEvent:AddListener("ReplyStarBiwuZhanDuiList", self, "ReplyStarBiwuZhanDuiList")
	g_ScriptEvent:AddListener("RequestJoinStarBiwuZhanDuiSuccess", self, "RequestJoinStarBiwuZhanDuiSuccess")
    g_ScriptEvent:AddListener("StarBiwuCancelJoinZhanDuiByPlayer", self, "StarBiwuCancelJoinZhanDuiByPlayer")
end

function LuaStarBiwuZhanDuiSearchView:OnDisable()
	g_ScriptEvent:RemoveListener("ReplyStarBiwuZhanDuiList", self, "ReplyStarBiwuZhanDuiList")
	g_ScriptEvent:RemoveListener("RequestJoinStarBiwuZhanDuiSuccess", self, "RequestJoinStarBiwuZhanDuiSuccess")
    g_ScriptEvent:RemoveListener("StarBiwuCancelJoinZhanDuiByPlayer", self, "StarBiwuCancelJoinZhanDuiByPlayer")
end
