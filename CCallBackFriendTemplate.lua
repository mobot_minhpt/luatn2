-- Auto Generated!!
local CCallBackFriendTemplate = import "L10.UI.CCallBackFriendTemplate"
local CUICommonDef = import "L10.UI.CUICommonDef"
local EnumCallBackStatus = import "L10.Game.EnumCallBackStatus"
local LocalString = import "LocalString"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CCallBackFriendTemplate.m_Init_CS2LuaHook = function (this, info) 
    this.playerInfo = info
    this.playerName.text = info.playerName
    this.iconTexture:LoadNPCPortrait(CUICommonDef.GetPortraitName(info.playerClass, info.playerGender, -1), false)

    UIEventListener.Get(this.inviteButton).onClick = MakeDelegateFromCSFunction(this.OnButtonClick, VoidDelegate, this)

    this:InitStatus()
end
CCallBackFriendTemplate.m_InitStatus_CS2LuaHook = function (this) 
    repeat
        local default = this.playerInfo.status
        if default == EnumCallBackStatus.notInvite then
            this.inviteButton:SetActive(true)
            this.invitedLabel.gameObject:SetActive(false)
            break
        elseif default == EnumCallBackStatus.waitForBack then
            this.inviteButton:SetActive(false)
            this.invitedLabel.gameObject:SetActive(true)
            this.invitedLabel.text = LocalString.GetString("等待回复")
            break
        elseif default == EnumCallBackStatus.inviteSuccess then
            this.inviteButton:SetActive(false)
            this.invitedLabel.gameObject:SetActive(true)
            this.invitedLabel.text = LocalString.GetString("已召回")
            break
        else
            break
        end
    until 1
end
