local UITable = import "UITable"
local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local CButton = import "L10.UI.CButton"
local Extensions = import "Extensions"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local CTipParagraphItem = import "L10.UI.CTipParagraphItem"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"

LuaLuoSpecialZongMenWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaLuoSpecialZongMenWnd, "ActiveTimeLabel", "ActiveTimeLabel", UILabel)
RegistChildComponent(LuaLuoSpecialZongMenWnd, "ScrollView", "ScrollView", CUIRestrictScrollView)
RegistChildComponent(LuaLuoSpecialZongMenWnd, "JoinBtn", "JoinBtn", CButton)
RegistChildComponent(LuaLuoSpecialZongMenWnd, "RankBtn", "RankBtn", CButton)
RegistChildComponent(LuaLuoSpecialZongMenWnd, "JoinBtnLabel", "JoinBtnLabel", UILabel)
RegistChildComponent(LuaLuoSpecialZongMenWnd, "StatusLabel", "StatusLabel", UILabel)
RegistChildComponent(LuaLuoSpecialZongMenWnd, "TextTemplate", "TextTemplate", GameObject)
RegistChildComponent(LuaLuoSpecialZongMenWnd, "TextTable", "TextTable", UITable)
RegistChildComponent(LuaLuoSpecialZongMenWnd, "RewardGrid", "RewardGrid", GameObject)
--@endregion RegistChildComponent end
RegistClassMember(LuaLuoSpecialZongMenWnd,"m_AlreadyJoined")
RegistClassMember(LuaLuoSpecialZongMenWnd,"m_HasInited")

function LuaLuoSpecialZongMenWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.JoinBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnJoinBtnClick()
	end)


	
	UIEventListener.Get(self.RankBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRankBtnClick()
	end)


    --@endregion EventBind end
end

function LuaLuoSpecialZongMenWnd:Init()
    Gac2Gas.QuerySpokesmanSectSignStatus()

    self.m_HasInited = false
	self:InitTextTable()

    local reward = Menpai_Spokesman.GetData().SpokesmanReward
    -- 初始化奖励
    for i=1, 3 do
        local itemId = reward[i-1]
        local data = Item_Item.GetData(itemId)
        local icon = self.RewardGrid.transform:Find(tostring(i).. "/IconTexture"):GetComponent(typeof(CUITexture))

        if data then
            UIEventListener.Get(icon.gameObject).onClick = DelegateFactory.VoidDelegate(function()
                CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType.Default, 0, 0, 0, 0)
            end)
            icon:LoadMaterial(data.Icon)
        end
    end

    -- 初始化时间
    self.ActiveTimeLabel.text = Menpai_Spokesman.GetData().SpokesmanActivityTime

    -- 初始化文本
    self.StatusLabel.text = g_MessageMgr:FormatMessage("LuoYunXi_Special_Sect_Already_Jion_Tip")
end

function LuaLuoSpecialZongMenWnd:InitStatus(isJoined)
    self.m_HasInited = true
    self.m_AlreadyJoined = isJoined
    self.StatusLabel.gameObject:SetActive(isJoined)
    if isJoined then
        self.JoinBtnLabel.text = LocalString.GetString("取消报名")
    else
        self.JoinBtnLabel.text = LocalString.GetString("我要报名")
    end
end

function LuaLuoSpecialZongMenWnd:InitTextTable()
    self.TextTemplate:SetActive(false)
    Extensions.RemoveAllChildren(self.TextTable.transform)
    local msg = g_MessageMgr:FormatMessage("LuoYunXi_Special_Sect_Introduction")
    local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(msg)
    if info == nil then
        return
    end
    for i = 0, info.paragraphs.Count - 1 do
        local paragraphGo = NGUITools.AddChild(self.TextTable.gameObject, self.TextTemplate)
        paragraphGo:SetActive(true)
        CommonDefs.GetComponent_GameObject_Type(paragraphGo, typeof(CTipParagraphItem))
                :Init(info.paragraphs[i], 4294967295)
    end
    self.ScrollView:ResetPosition()
    self.TextTable:Reposition()
end

function LuaLuoSpecialZongMenWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncSpokesmanSectSignStatus", self, "InitStatus")
end

function LuaLuoSpecialZongMenWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncSpokesmanSectSignStatus", self, "InitStatus")
end

--@region UIEvent

function LuaLuoSpecialZongMenWnd:OnJoinBtnClick()
    -- 请求rpc
    if not self.m_AlreadyJoined and self.m_HasInited then
        Gac2Gas.RequestSignSpokesmanSect()
    elseif self.m_HasInited then
        Gac2Gas.RequestCancleSignSpokesmanSect()
    end
end

function LuaLuoSpecialZongMenWnd:OnRankBtnClick()
    CUIManager.ShowUI(CLuaUIResources.LuoSpecialZongMenRankWnd)
end

--@endregion UIEvent

