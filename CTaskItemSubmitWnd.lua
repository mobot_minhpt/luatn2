-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CConversationMgr = import "L10.Game.CConversationMgr"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CTaskDialogInfo = import "L10.Game.CConversationMgr+CTaskDialogInfo"
local CTaskItemSubmitWnd = import "L10.UI.CTaskItemSubmitWnd"
local CTaskSubmitItem = import "L10.UI.CTaskSubmitItem"
local CTooltip = import "L10.UI.CTooltip"
local DelegateFactory = import "DelegateFactory"
local Extensions = import "Extensions"
local MessageWndManager = import "L10.UI.MessageWndManager"
local NGUIMath = import "NGUIMath"
local NGUITools = import "NGUITools"
local Task_Task = import "L10.Game.Task_Task"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CTaskItemSubmitWnd.m_LoadData_CS2LuaHook = function (this, items) 
    if items == nil then
        return
    end
    Extensions.RemoveAllChildren(this.itemsGrid.transform)
    CommonDefs.ListClear(this.candidates)
    this.selectedItemId = nil
    do
        local i = 0
        while i < items.Count do
            local obj = NGUITools.AddChild(this.itemsGrid.gameObject, this.itemTemplate)
            obj:SetActive(true)
            local submitItem = CommonDefs.GetComponent_GameObject_Type(obj, typeof(CTaskSubmitItem))
            submitItem:Init(items[i].item, items[i].count, items[i].pos)
            CommonDefs.ListAdd(this.candidates, typeof(CTaskSubmitItem), submitItem)
            UIEventListener.Get(obj).onClick = MakeDelegateFromCSFunction(this.OnItemClick, VoidDelegate, this)
            i = i + 1
        end
    end
    this.itemsGrid:Reposition()
    this.itemsScrollView:ResetPosition()
    if this.candidates.Count > 0 then
        this:OnItemClick(this.candidates[0].gameObject)
    end
end
CTaskItemSubmitWnd.m_OnItemClick_CS2LuaHook = function (this, go) 
    do
        local i = 0
        while i < this.candidates.Count do
            if this.candidates[i].gameObject:Equals(go) then
                this.candidates[i].Selected = true
                this.selectedItemId = this.candidates[i].ItemId
                this.selectedItemPlace = this.candidates[i].Place

                local b1 = NGUIMath.CalculateRelativeWidgetBounds(this.background.transform)
                local b2 = NGUIMath.CalculateRelativeWidgetBounds(this.background.transform, this.closeBtn.transform)
                local height = b1.size.y
                local worldCenterY = this.background.transform:TransformPoint(b1.center).y
                CommonDefs.Bounds_Encapsulate_Bounds(b1, b2)
                local width = b1.size.x
                local worldCenterX = this.background.transform:TransformPoint(b1.center).x

                CItemInfoMgr.ShowLinkItemInfo(this.selectedItemId, false, nil, CItemInfoMgr.AlignType.Right, worldCenterX, worldCenterY, width, height)
            else
                this.candidates[i].Selected = false
            end
            i = i + 1
        end
    end
end
CTaskItemSubmitWnd.m_OnSubmitButtonClick_CS2LuaHook = function (this, go) 

    local item = CItemMgr.Inst:GetById(this.selectedItemId)
    if item ~= nil and item.IsEquip then
        if item.Equip.IsBlueEquipment or item.Equip.IsRedOrPurpleEquipment then
            local content = g_MessageMgr:FormatMessage("SUBMIT_EQUIP_CONFIRM", item.ColoredName)
            MessageWndManager.ShowOKCancelMessage(content, DelegateFactory.Action(function () 
                this:SubmitEquipment()
            end), nil, nil, nil, false)
        else
            this:SubmitEquipment()
        end
    end
end
CTaskItemSubmitWnd.m_OnBuyButtonClick_CS2LuaHook = function (this, go) 
    this.dialogInfo = TypeAs(CConversationMgr.Inst.BaseInfo, typeof(CTaskDialogInfo))
    if not Task_Task.Exists(this.dialogInfo.m_TaskId) then
        return
    end
    if CClientMainPlayer.Inst == nil then
        return
    end
    local taskTemplate = Task_Task.GetData(this.dialogInfo.m_TaskId)
    do
        local i = 0
        while i < taskTemplate.TaskLocations.Length do
            local loc = taskTemplate.TaskLocations[i]
            if loc.destEvent == TaskTrackDestEvent_lua.BuyItem then
                CItemAccessListMgr.Inst:ShowNPCShopAccessInfo(loc.sceneTemplateId, loc.InteractWithNpcId, loc.targetX, loc.targetY, this.buyBtn.transform, CTooltip.AlignType.Top)
                break
            elseif loc.destEvent == TaskTrackDestEvent_lua.BuyItemFromPlayerShop then
                local submitItem = taskTemplate.SubmitItemInfo
                local level = math.max(math.max(submitItem.maxGrade, submitItem.minGrade), 1)
                local colorId = math.max(1, submitItem.coloId)
                local equipType = math.max(1, submitItem.equipType)
                local subtype = math.max(1, submitItem.subtype)
                CItemAccessListMgr.Inst:ShowPlayerShopAccessInfo(level, colorId, equipType, subtype, this.buyBtn.transform, CTooltip.AlignType.Top)
                break
            end
            i = i + 1
        end
    end
end
