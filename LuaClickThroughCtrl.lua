require("common/common_include")

local CUICommonDef = import "L10.UI.CUICommonDef"
local UICamera = import "UICamera"

CLuaClickThroughCtrl = class()

function CLuaClickThroughCtrl:Update()
    self:ClickThroughToClose()
end

function CLuaClickThroughCtrl:ClickThroughToClose()
    if Input.GetMouseButtonDown(0) then
        if (not CUICommonDef.IsOverUI or (UICamera.lastHit.collider and
           not CUICommonDef.IsChild(self.transform, UICamera.lastHit.collider.transform) and
            CUICommonDef.GetNearestPanelDepth(self.transform) > CUICommonDef.GetNearestPanelDepth(UICamera.lastHit.collider.transform))) then
            self.gameObject:SetActive(false)
        end
    end
end

return CLuaClickThroughCtrl
