local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"

LuaHanJia2024MainWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaHanJia2024MainWnd, "TimeLabel", "TimeLabel", UILabel)
RegistChildComponent(LuaHanJia2024MainWnd, "SnowManTimeLabel", "SnowManTimeLabel", UILabel)
RegistChildComponent(LuaHanJia2024MainWnd, "UpDownWorldTimeLabel", "UpDownWorldTimeLabel", UILabel)
RegistChildComponent(LuaHanJia2024MainWnd, "DuiXueRen", "DuiXueRen", GameObject)
RegistChildComponent(LuaHanJia2024MainWnd, "Passport", "Passport", GameObject)
RegistChildComponent(LuaHanJia2024MainWnd, "SnowMan", "SnowMan", GameObject)
RegistChildComponent(LuaHanJia2024MainWnd, "UpDownWorld", "UpDownWorld", GameObject)
RegistChildComponent(LuaHanJia2024MainWnd, "SnowManShop", "SnowManShop", GameObject)

--@endregion RegistChildComponent end

function LuaHanJia2024MainWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	UIEventListener.Get(self.DuiXueRen.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnDuiXueRenClick()
	end)

	UIEventListener.Get(self.Passport.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnPassportClick()
	end)

	UIEventListener.Get(self.SnowMan.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSnowManClick()
	end)

	UIEventListener.Get(self.UpDownWorld.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnUpDownWorldClick()
	end)

	UIEventListener.Get(self.SnowManShop.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSnowManShopClick()
	end)

    --@endregion EventBind end
end

function LuaHanJia2024MainWnd:Init()
	local setting = HanJia2024_Setting.GetData()
	self.TimeLabel.text = setting.MainWndShowTime
	self.SnowManTimeLabel.text = setting.MainWndSnowManTime
	--self.UpDownWorldTimeLabel.text = setting.MainWndUpDownWorldTime
end

--@region UIEvent

function LuaHanJia2024MainWnd:OnDuiXueRenClick()
	g_MessageMgr:ShowMessage("HANJIA2024MAINEND_DUIXUEREN_TIP")
end

function LuaHanJia2024MainWnd:OnPassportClick()
	local setting = HanJia2024_UpDownWorldSetting.GetData()
	local passid = setting.PassId
	LuaCommonPassportMgr:ShowPassportWnd(passid)
end

function LuaHanJia2024MainWnd:OnSnowManClick()
	LuaHanJia2024Mgr:OpenSnowManEnterWnd()
end

function LuaHanJia2024MainWnd:OnUpDownWorldClick()
	LuaHanJia2024Mgr:ReqUpDownWorldData()
end

function LuaHanJia2024MainWnd:OnSnowManShopClick()
	local setting = HanJia2024_Setting.GetData()
	Gac2Gas.OpenShop(setting.SnowManShopID)
end

--@endregion UIEvent

