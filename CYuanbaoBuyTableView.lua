-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CellIndex = import "L10.UI.UITableView+CellIndex"
local CellIndexType = import "L10.UI.UITableView+CellIndexType"
local CFreightMgr = import "L10.Game.CFreightMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CYuanbaoBuySectionTemplate = import "L10.UI.CYuanbaoBuySectionTemplate"
local CYuanbaoBuyTableView = import "L10.UI.CYuanbaoBuyTableView"
local CYuanbaoMarketMgr = import "L10.UI.CYuanbaoMarketMgr"
local EnumGuildFreightStatus = import "L10.Game.EnumGuildFreightStatus"
local ItemGet_item = import "L10.Game.ItemGet_item"
--local Mall_MarketDefine = import "L10.Game.Mall_MarketDefine"
--local Mall_YuanBaoMarket = import "L10.Game.Mall_YuanBaoMarket"
local NeedTag = import "L10.UI.CYuanbaoBuyTableView+NeedTag"
local Section = import "L10.UI.CYuanbaoBuyTableView+Section"
local String = import "System.String"
local UInt32 = import "System.UInt32"
CYuanbaoBuyTableView.m_Init_CS2LuaHook = function (this) 

    CommonDefs.ListClear(this.sections)
    Mall_MarketDefine.Foreach(function (key, data) 
        local contains = false
        local index = 0
        do
            local i = 0
            while i < this.sections.Count do
                if this.sections[i].Category == data.Category then
                    contains = true
                    index = i
                    break
                end
                i = i + 1
            end
        end
        if not contains then
            if not this:IsEmptyCategory(data.Category) then
                local rowList = CreateFromClass(MakeGenericClass(List, String))
                local section = CreateFromClass(Section)
                section.Category = data.Category
                section.Name = data.Name
                section.RowList = rowList
                CommonDefs.ListAdd(this.sections, typeof(Section), section)
            end
        else
            CommonDefs.ListAdd(this.sections[index].RowList, typeof(String), data.Name)
        end
    end)
    this:LoadData(CreateFromClass(CellIndex, 0, 0, CellIndexType.Section), true)
    this:SetSectionSelected(CreateFromClass(CellIndex, CYuanbaoMarketMgr.SelectMarketIndex, 0, CellIndexType.Section))
end
CYuanbaoBuyTableView.m_IsEmptyCategory_CS2LuaHook = function (this, category) 
    if category < 5 then
        return false
    end
    local count = 0
    Mall_YuanBaoMarket.Foreach(function (k, v) 
        if v.Status == 0 and v.Category == category then
            count = count + 1
        end
    end)
    return count == 0
end
CYuanbaoBuyTableView.m_SetSelected_CS2LuaHook = function (this, sectionIdx, rowIdx) 
    local index = nil
    if rowIdx == 0 then
        index = CreateFromClass(CellIndex, sectionIdx, rowIdx, CellIndexType.Section)
        this:SetSectionSelected(index)
    else
        index = CreateFromClass(CellIndex, sectionIdx, rowIdx, CellIndexType.Row)
        this:SetRowSelected(index)
    end
end
CYuanbaoBuyTableView.m_NumberOfRowsInSection_CS2LuaHook = function (this, section) 
    --throw new System.NotImplementedException();
    if this.sections[section].RowList ~= nil then
        return this.sections[section].RowList.Count
    else
        return 0
    end
end
CYuanbaoBuyTableView.m_RefreshCellForRowAtIndex_CS2LuaHook = function (this, cell, index) 

    local needList = CreateFromClass(MakeGenericClass(List, NeedTag))
    --货运任务购买物品
    if CommonDefs.ListContains(CClientMainPlayer.Inst.TaskProp.CurrentTaskList, typeof(UInt32), CFreightMgr.Inst.taskId) and not CommonDefs.DictGetValue(CClientMainPlayer.Inst.TaskProp.CurrentTasks, typeof(UInt32), CFreightMgr.Inst.taskId).IsFailed then
        do
            local i = 0
            while i < CFreightMgr.Inst.cargoList.Count do
                local item = ItemGet_item.GetData(CFreightMgr.Inst.cargoList[i].templateId)
                if item ~= nil and (CFreightMgr.Inst.cargoList[i].status == EnumGuildFreightStatus.eNotFilled or CFreightMgr.Inst.cargoList[i].status == EnumGuildFreightStatus.eNotFilledNeedHelp) then
                    local yuanbaoItem = Mall_YuanBaoMarket.GetData(item.ID)
                    if yuanbaoItem ~= nil then
                        local tag = CreateFromClass(NeedTag)
                        tag.Category = yuanbaoItem.Category - 1
                        tag.subCategory = yuanbaoItem.Category2 - 1
                        CommonDefs.ListAdd(needList, typeof(NeedTag), tag)
                    end
                end
                i = i + 1
            end
        end
    end
    --throw new System.NotImplementedException();
    if index.type == CellIndexType.Section then
        do
            local i = 0
            while i < needList.Count do
                if needList[i].Category == index.section then
                    CommonDefs.GetComponentInChildren_GameObject_Type(cell, typeof(CYuanbaoBuySectionTemplate)):Init(this.sections[index.section].Name, true)
                    return
                end
                i = i + 1
            end
        end
        CommonDefs.GetComponentInChildren_GameObject_Type(cell, typeof(CYuanbaoBuySectionTemplate)):Init(this.sections[index.section].Name, false)
    else
        do
            local i = 0
            while i < needList.Count do
                if needList[i].Category == index.section and needList[i].subCategory == index.row then
                    CommonDefs.GetComponentInChildren_GameObject_Type(cell, typeof(CYuanbaoBuySectionTemplate)):Init(this.sections[index.section].RowList[index.row], true)
                    return
                end
                i = i + 1
            end
        end
        CommonDefs.GetComponentInChildren_GameObject_Type(cell, typeof(CYuanbaoBuySectionTemplate)):Init(this.sections[index.section].RowList[index.row], false)
    end
end
