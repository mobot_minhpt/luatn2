require("3rdParty/ScriptEvent")
require("common/common_include")

local Gac2Gas = import "L10.Game.Gac2Gas"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CUIRes = import "L10.UI.CUIResources"
local CUIManager = import "L10.UI.CUIManager"
local MessageMgr = import "L10.Game.MessageMgr"

CLuaWeekendFightOpenWnd=class()
RegistClassMember(CLuaWeekendFightOpenWnd,"CloseBtn")
RegistClassMember(CLuaWeekendFightOpenWnd,"btn1")
RegistClassMember(CLuaWeekendFightOpenWnd,"btn2")
RegistClassMember(CLuaWeekendFightOpenWnd,"btn3")

function CLuaWeekendFightOpenWnd:Init()
	local onCloseClick = function(go)
		CUIManager.CloseUI(CUIRes.WeekendFightOpenWnd)
	end
	CommonDefs.AddOnClickListener(self.CloseBtn,DelegateFactory.Action_GameObject(onCloseClick),false)
	local onbtn1Click = function(go)
		MessageMgr.Inst:ShowMessage("XianZongShan_Rule",{})
	end
	CommonDefs.AddOnClickListener(self.btn1,DelegateFactory.Action_GameObject(onbtn1Click),false)
	local onbtn2Click = function(go)
		Gac2Gas.CrystalRequestEnterPrepare()
	end
	CommonDefs.AddOnClickListener(self.btn2,DelegateFactory.Action_GameObject(onbtn2Click),false)
	local onbtn3Click = function(go)
			CUIManager.ShowUI(CUIRes.WeekendFightRankWnd)
	end
	CommonDefs.AddOnClickListener(self.btn3,DelegateFactory.Action_GameObject(onbtn3Click),false)
end

return CLuaWeekendFightOpenWnd
