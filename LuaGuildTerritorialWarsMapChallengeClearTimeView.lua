local GameObject = import "UnityEngine.GameObject"
local UIGrid = import "UIGrid"
local CGuildMgr=import "L10.Game.CGuildMgr"

LuaGuildTerritorialWarsMapChallengeClearTimeView = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaGuildTerritorialWarsMapChallengeClearTimeView, "My", "My", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsMapChallengeClearTimeView, "Grid", "Grid", UIGrid)
RegistChildComponent(LuaGuildTerritorialWarsMapChallengeClearTimeView, "Template", "Template", GameObject)

--@endregion RegistChildComponent end

function LuaGuildTerritorialWarsMapChallengeClearTimeView:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaGuildTerritorialWarsMapChallengeClearTimeView:Start()
    self.Template.gameObject:SetActive(false)
end

function LuaGuildTerritorialWarsMapChallengeClearTimeView:InitTemplate(item, name, time)
    item.gameObject:SetActive(true)
    local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local timeLabel = item.transform:Find("TimeLabel"):GetComponent(typeof(UILabel))
    nameLabel.text = name
    timeLabel.text = LocalString.GetString("—")
    if time then
        timeLabel.text = SafeStringFormat3("%02d:%02d:%02d",  math.floor(time / 3600), math.floor((time % 3600) / 60), time % 60)
    end
end

function LuaGuildTerritorialWarsMapChallengeClearTimeView:OnEnable()
    g_ScriptEvent:AddListener("SendGTWChallengeBattleFieldInfo", self, "OnSendGTWChallengeBattleFieldInfo")
end

function LuaGuildTerritorialWarsMapChallengeClearTimeView:OnDisable()
    g_ScriptEvent:RemoveListener("SendGTWChallengeBattleFieldInfo", self, "OnSendGTWChallengeBattleFieldInfo")
end

function LuaGuildTerritorialWarsMapChallengeClearTimeView:OnSendGTWChallengeBattleFieldInfo()
    local t = LuaGuildTerritorialWarsMgr.m_ChallengeBattleFieldInfo
    local myGuildName = (CClientMainPlayer.Inst and CClientMainPlayer.Inst:IsInGuild()) and CGuildMgr.Inst.m_GuildName or LocalString.GetString("—")
    self:InitTemplate(self.My, myGuildName, nil)
    for k, v in pairs(t.passGuildInfo) do
        print(k,v)
    end
    if CClientMainPlayer.Inst and t.passGuildInfo[CClientMainPlayer.Inst.BasicProp.GuildId] then
        local guildInfo = t.passGuildInfo[CClientMainPlayer.Inst.BasicProp.GuildId]
        print(guildInfo.passTime)
        self:InitTemplate(self.My, myGuildName, guildInfo.passTime)
    end
    local passGuildInfo = t.passGuildInfo
    Extensions.RemoveAllChildren(self.Grid.transform)
    for guildId, info in pairs(passGuildInfo) do
        local guildName = info.guildName
        local passTime = info.passTime
        local obj = NGUITools.AddChild(self.Grid.gameObject,self.Template.gameObject)
        print(passTime)
        self:InitItem(obj, guildName,  passTime)
    end
    Extensions.RemoveAllChildren(self.Grid.transform)
    self.Grid:Reposition()
end
--@region UIEvent

--@endregion UIEvent

