local UITable = import "UITable"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local UIProgressBar = import "UIProgressBar"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
LuaShuangshiyi2023ChargeRewardWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaShuangshiyi2023ChargeRewardWnd, "ChargeText", "ChargeText", UILabel)
RegistChildComponent(LuaShuangshiyi2023ChargeRewardWnd, "RuleButton", "RuleButton", GameObject)
RegistChildComponent(LuaShuangshiyi2023ChargeRewardWnd, "Table", "Table", UITable)
RegistChildComponent(LuaShuangshiyi2023ChargeRewardWnd, "ChargeGiftItem", "ChargeGiftItem", GameObject)

--@endregion RegistChildComponent end

function LuaShuangshiyi2023ChargeRewardWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self:InitWndData()
    self:RefreshConstUI()
    self:RefreshVariableUI()
    self:InitUIEvent()
end

function LuaShuangshiyi2023ChargeRewardWnd:Init()

end

--@region UIEvent

--@endregion UIEvent

function LuaShuangshiyi2023ChargeRewardWnd:InitWndData()
    self.rechargeData = {}
    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer and CommonDefs.DictContains_LuaCall(mainPlayer.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eDouble11RechargeData) then
        local playDataU = CommonDefs.DictGetValue_LuaCall(mainPlayer.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eDouble11RechargeData)
        local list = g_MessagePack.unpack(playDataU.Data.Data)
        self.rechargeData = list
    end
    
    self.chargeRewardData = {}
    local rawData = Double11_RechargeLottery.GetData().PriceRewardItems
    local chargeList = g_LuaUtil:StrSplit(rawData, ";")
    for i = 1, #chargeList do
        if chargeList[i] ~= "" then
            local itemList = g_LuaUtil:StrSplit(chargeList[i], ",")
            local chargeRequireNumber = tonumber(itemList[1])
            local itemTbl = {}
            for j = 2, #itemList, 2 do
                local itemId = tonumber(itemList[j])
                local itemCnt = tonumber(itemList[j+1])
                table.insert(itemTbl, {itemId = itemId, itemCnt = itemCnt})
            end
            table.insert(self.chargeRewardData, {chargeRequire = chargeRequireNumber, rewardItemTbl = itemTbl})
        end
    end
    
    self.chargeItemList = {}
end

function LuaShuangshiyi2023ChargeRewardWnd:RefreshConstUI()
    self.ChargeText.text = g_MessageMgr:FormatMessage("Double11_2023RechargeReward", 
            self.rechargeData[LuaShuangshiyi2023Mgr.KeyTbl.ePrice] and self.rechargeData[LuaShuangshiyi2023Mgr.KeyTbl.ePrice] or 0)
end

function LuaShuangshiyi2023ChargeRewardWnd:RefreshVariableUI()
    --refresh data
    self.rechargeData = {}
    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer and CommonDefs.DictContains_LuaCall(mainPlayer.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eDouble11RechargeData) then
        local playDataU = CommonDefs.DictGetValue_LuaCall(mainPlayer.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eDouble11RechargeData)
        local list = g_MessagePack.unpack(playDataU.Data.Data)
        self.rechargeData = list
    end

    self.ChargeGiftItem:SetActive(false)
    local rewardGotTbl = self.rechargeData[LuaShuangshiyi2023Mgr.KeyTbl.eRewardStatus] and self.rechargeData[LuaShuangshiyi2023Mgr.KeyTbl.eRewardStatus] or {}
    local isNewObject = false
    for i = 1, #self.chargeRewardData do
        local chargeData = self.chargeRewardData[i]
        local formerChargeData = self.chargeRewardData[i-1] and self.chargeRewardData[i-1] or nil
        local templateItem = nil
        if self.chargeItemList[i] then
            templateItem = self.chargeItemList[i]
        else
            isNewObject = true
            templateItem = CommonDefs.Object_Instantiate(self.ChargeGiftItem)
            templateItem:SetActive(true)
            templateItem.transform.parent = self.Table.transform
            table.insert(self.chargeItemList, templateItem)
        end
        self:InitChargeItem(templateItem, formerChargeData, chargeData, rewardGotTbl[i] and rewardGotTbl[i] or false, i)
    end
    if isNewObject then
        self.Table:Reposition()
    end
end

function LuaShuangshiyi2023ChargeRewardWnd:InitChargeItem(chargeItemObj, formerChargeData, chargeData, isGot, chargeId)
    local chargeNumber = self.rechargeData[LuaShuangshiyi2023Mgr.KeyTbl.ePrice] and self.rechargeData[LuaShuangshiyi2023Mgr.KeyTbl.ePrice] or 0
    
    --refresh charge value
    local hideIndex = Double11_RechargeLottery.GetData().PriceRewardHideIndex
    if chargeId >= hideIndex then
        if formerChargeData == nil then
            chargeItemObj.transform:Find("Label"):GetComponent(typeof(UILabel)).text = chargeData.chargeRequire .. LocalString.GetString("元")
        else
            if chargeNumber >= formerChargeData.chargeRequire then
                chargeItemObj.transform:Find("Label"):GetComponent(typeof(UILabel)).text = chargeData.chargeRequire .. LocalString.GetString("元")
            else
                --用?代替数位
                local str = ""
                local tmpNumber = chargeData.chargeRequire
                while tmpNumber > 0 do
                    str = "?" .. str
                    tmpNumber = math.floor(tmpNumber / 10)
                end
                chargeItemObj.transform:Find("Label"):GetComponent(typeof(UILabel)).text = str .. LocalString.GetString("元")
            end
        end
    else
        chargeItemObj.transform:Find("Label"):GetComponent(typeof(UILabel)).text = chargeData.chargeRequire .. LocalString.GetString("元")
    end

    --refresh award list
    local awardTable = chargeItemObj.transform:Find("GiftTable"):GetComponent(typeof(UITable))
    local awardItemTemplate = chargeItemObj.transform:Find("GiftTemplate").gameObject
    awardItemTemplate:SetActive(false)
    Extensions.RemoveAllChildren(awardTable.transform)
    for k, v in pairs(chargeData.rewardItemTbl) do
        local itemId = v.itemId
        local itemCnt = v.itemCnt
        local templateItem = CommonDefs.Object_Instantiate(awardItemTemplate)
        templateItem:SetActive(true)
        templateItem.transform.parent = awardTable.transform
        templateItem.transform.localScale = Vector3.one
        local ItemData = Item_Item.GetData(itemId)
        templateItem.transform:Find("QualitySprite"):GetComponent(typeof(UISprite)).spriteName = CUICommonDef.GetItemCellBorder(ItemData.NameColor)
        local textureComp = templateItem.transform:GetComponent(typeof(CUITexture))
        textureComp:LoadMaterial(ItemData.Icon)
        templateItem.transform:Find("Label"):GetComponent(typeof(UILabel)).text = itemCnt > 1 and itemCnt or "" 
        templateItem.transform:Find("Get").gameObject:SetActive(isGot)

        UIEventListener.Get(templateItem).onClick = DelegateFactory.VoidDelegate(function (_)
            CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
        end)
    end
    awardTable:Reposition()
    
    --refresh slider
    local alreadyRecTransform = chargeItemObj.transform:Find("AlreadyRec")
    local recButtonTransform = chargeItemObj.transform:Find("QnButton")
    local chargeProgressTransform = chargeItemObj.transform:Find("QnProgressBar")

    if isGot then
        --已获得
        alreadyRecTransform.gameObject:SetActive(true)
        recButtonTransform.gameObject:SetActive(false)
        chargeProgressTransform.gameObject:SetActive(false)
    else
        if chargeNumber >= chargeData.chargeRequire then
            alreadyRecTransform.gameObject:SetActive(false)
            recButtonTransform.gameObject:SetActive(true)
            chargeProgressTransform.gameObject:SetActive(false)
            UIEventListener.Get(recButtonTransform.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
                Gac2Gas.Request2023Double11RechargeRewardById(chargeId)
            end)
        else
            alreadyRecTransform.gameObject:SetActive(false)
            recButtonTransform.gameObject:SetActive(false)
            chargeProgressTransform.gameObject:SetActive(true)
            chargeProgressTransform:GetComponent(typeof(UIProgressBar)).value = chargeNumber / chargeData.chargeRequire
        end
    end
    
end

function LuaShuangshiyi2023ChargeRewardWnd:OnEnable()
    g_ScriptEvent:AddListener("UpdateTempPlayDataWithKey", self, "RefreshVariableUI")
end 

function LuaShuangshiyi2023ChargeRewardWnd:OnDisable()
    g_ScriptEvent:RemoveListener("UpdateTempPlayDataWithKey", self, "RefreshVariableUI")
end

function LuaShuangshiyi2023ChargeRewardWnd:InitUIEvent()
    UIEventListener.Get(self.RuleButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        g_MessageMgr:ShowMessage("Double11_2023RechargeReward_Rule")
    end)
end
