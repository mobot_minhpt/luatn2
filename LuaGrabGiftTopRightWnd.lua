local Vector3 = import "UnityEngine.Vector3"
local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
CLuaGrabGiftTopRightWnd = class()

RegistClassMember(CLuaGrabGiftTopRightWnd,"m_PlayerInfoView")
RegistClassMember(CLuaGrabGiftTopRightWnd,"m_ExpandBtn")
RegistClassMember(CLuaGrabGiftTopRightWnd,"m_InfoPanel")


function CLuaGrabGiftTopRightWnd:Awake()
    --self.m_PlayerInfoView
    local player1 = self.transform:Find("Anchor/InfoView/Player1")
    local player2 = self.transform:Find("Anchor/InfoView/Player2")
    local player3 = self.transform:Find("Anchor/InfoView/Player3")
    local player4 = self.transform:Find("Anchor/InfoView/Player4")
    self.m_ExpandBtn = self.transform:Find("Anchor/Tip/ExpandButton").gameObject
    self.m_PlayerInfoView = {player1,player2,player3,player4}
    self.m_InfoPanel = self.transform:Find("Anchor/InfoView").gameObject

    UIEventListener.Get(self.m_ExpandBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        self.m_ExpandBtn.transform.localEulerAngles = Vector3(0, 0, 0)
        CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
    end)
end

function CLuaGrabGiftTopRightWnd:Init()
    self.m_InfoPanel:SetActive(false)   
end

function CLuaGrabGiftTopRightWnd:OnRefreshPlayerInfo()
    if not self.m_InfoPanel.activeSelf then
        self.m_InfoPanel:SetActive(true)
    end

    local mainPlayerNameLabel = self.m_PlayerInfoView[4]:Find("NameLabel"):GetComponent(typeof(UILabel))
    local mainPlayerGiftLabel = self.m_PlayerInfoView[4]:Find("GiftNumLabel"):GetComponent(typeof(UILabel))
    local mainPlayerRankLabel = self.m_PlayerInfoView[4]:Find("RankLabel"):GetComponent(typeof(UILabel))
    local mainPlayerRankTexture = self.m_PlayerInfoView[4]:Find("RankLabel/Texture"):GetComponent(typeof(CUITexture))
    local isInitMainPlayer = false
    local mainPlayerData
    for rank,data in ipairs(LuaGrabChristmasGiftMgr.playerInfoData) do
        if rank <= 3 then
            local nameLabel =  self.m_PlayerInfoView[rank]:Find("NameLabel"):GetComponent(typeof(UILabel))
            local giftNumLabel = self.m_PlayerInfoView[rank]:Find("GiftNumLabel"):GetComponent(typeof(UILabel))
            nameLabel.text = data.playerName
            giftNumLabel.text = "x"..data.giftNum
            if CClientMainPlayer.IsPlayerId(data.playerId) then
                data.rank = rank
                mainPlayerData = data
                isInitMainPlayer = true
            end
        else
            if isInitMainPlayer then
                break
            elseif CClientMainPlayer.IsPlayerId(data.playerId) then
                data.rank = rank
                mainPlayerData = data
                isInitMainPlayer = true
            end
        end
    end

    local path = "UI/Texture/Transparent/Material/"
    local huangGuan = {"grabchristmasgiftrankwnd_huangguang_01.mat","grabchristmasgiftrankwnd_huangguang_02.mat","grabchristmasgiftrankwnd_huangguang_03.mat"}
    
    if mainPlayerData then
        local rank = mainPlayerData.rank
        mainPlayerNameLabel.text = mainPlayerData.playerName
        mainPlayerGiftLabel.text = "x"..mainPlayerData.giftNum
        mainPlayerRankLabel.text = rank
        if rank >3 then
            mainPlayerRankTexture.gameObject:SetActive(false)
        else
            mainPlayerRankTexture.gameObject:SetActive(true)
            mainPlayerRankTexture:LoadMaterial(path..huangGuan[rank])
        end
    end
end

function CLuaGrabGiftTopRightWnd:OnEnable()
    g_ScriptEvent:AddListener("RefreshGiftBattlePlayerInfoView", self, "OnRefreshPlayerInfo")
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
end

function CLuaGrabGiftTopRightWnd:OnDisable()
    g_ScriptEvent:RemoveListener("RefreshGiftBattlePlayerInfoView", self, "OnRefreshPlayerInfo")
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
end

function CLuaGrabGiftTopRightWnd:OnHideTopAndRightTipWnd( )
    self.m_ExpandBtn.transform.localEulerAngles = Vector3(0, 0, 180)
end
