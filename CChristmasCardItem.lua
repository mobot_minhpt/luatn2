-- Auto Generated!!
local CChristmasCardItem = import "L10.UI.CChristmasCardItem"
local LocalString = import "LocalString"
local Regex = import "System.Text.RegularExpressions.Regex"
CChristmasCardItem.m_UpdateData_CS2LuaHook = function (this, name, content) 
    this.m_PlayerNameLabel.text = System.String.Format(LocalString.GetString("致:{0}"), name)
    content = Regex.Replace(content, "\\p{Cs}", "")
    if CommonDefs.StringLength(content) > CChristmasCardItem.s_MaxContentLength then
        content = CommonDefs.StringSubstring2(content, 0, 8) .. "..."
    end
    this.m_WishContentLabel.text = content
end
