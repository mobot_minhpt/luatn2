local CPostEffectMgr = import "L10.Engine.CPostEffectMgr"
local Time = import "UnityEngine.Time"
local SoundManager = import "SoundManager"
local NGUIText = import "NGUIText"
local CUITexture = import "L10.UI.CUITexture"
local Constants = import "L10.Game.Constants"
local Color = import "UnityEngine.Color"
local CLoadingWnd = import "L10.UI.CLoadingWnd"
local CJuQingDialogMgr = import "L10.UI.CJuQingDialogMgr"
local CConversationMgr = import "L10.Game.CConversationMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CChatLinkMgr = import "CChatLinkMgr"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local CClientNpc = import "L10.Game.CClientNpc"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CGuideMgr = import "L10.Game.Guide.CGuideMgr"
local CPostProcessingMgr = import "L10.Engine.PostProcessing.CPostProcessingMgr"
local UIRoot = import "UIRoot"
local CThumbnailChatWnd = import "L10.UI.CThumbnailChatWnd"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"

--Dialog_TaskDialog显示的是这个对话框，TaskDialog只显示task发起的一些对话，是不支持选项的，只有juqingdialog才支持选项
CLuaJuQingDialogWnd = class()
RegistClassMember(CLuaJuQingDialogWnd, "nameLabel")
RegistClassMember(CLuaJuQingDialogWnd, "headPortrait")
RegistClassMember(CLuaJuQingDialogWnd, "conversation")
RegistClassMember(CLuaJuQingDialogWnd, "AnchorNode")
RegistClassMember(CLuaJuQingDialogWnd, "AnchorBigNode")
RegistClassMember(CLuaJuQingDialogWnd, "startTime")
RegistClassMember(CLuaJuQingDialogWnd, "duration")
RegistClassMember(CLuaJuQingDialogWnd, "curAIDialogId")
RegistClassMember(CLuaJuQingDialogWnd, "useBigShow")
RegistClassMember(CLuaJuQingDialogWnd, "dialog")
RegistClassMember(CLuaJuQingDialogWnd, "m_ContentIndex")
RegistClassMember(CLuaJuQingDialogWnd, "m_ShowChoices")
RegistClassMember(CLuaJuQingDialogWnd, "m_ChoiceDisplayParagraph")
RegistClassMember(CLuaJuQingDialogWnd, "m_ContainsBigNpcPortrait")--对话中是否包含半身立绘，即含有bigNpc:附带不小于0的数字
RegistClassMember(CLuaJuQingDialogWnd, "m_CancelSoundPlayAction")
RegistClassMember(CLuaJuQingDialogWnd, "m_NormalSoundButton")
RegistClassMember(CLuaJuQingDialogWnd, "m_BigShowSoundLeftButton")
RegistClassMember(CLuaJuQingDialogWnd, "m_BigShowSoundRightButton")
RegistClassMember(CLuaJuQingDialogWnd, "m_NormalSoundSettingButton")
RegistClassMember(CLuaJuQingDialogWnd, "m_BigShowSoundSettingLeftButton")
RegistClassMember(CLuaJuQingDialogWnd, "m_BigShowSoundSettingRightButton")
RegistClassMember(CLuaJuQingDialogWnd, "m_CrosswordViewScript")
RegistClassMember(CLuaJuQingDialogWnd, "m_IsToricBegin")
RegistClassMember(CLuaJuQingDialogWnd, "m_Npc2Expression")
RegistClassMember(CLuaJuQingDialogWnd, "m_AdaptationOffset") -- 安全区适配偏移量

function CLuaJuQingDialogWnd:EnableBigNpcMode()
    -- 根据策划的需求：只要对话中有一句bigNpc不为-1，才开启虚化
    if CPostProcessingMgr.s_NewPostProcessingOpen then
        local postEffectCtrl = CPostProcessingMgr.Instance.MainController
        if self.m_ContainsBigNpcPortrait and postEffectCtrl then
            postEffectCtrl.DepthOfFieldEnable = true
        end
    else
        if self.m_ContainsBigNpcPortrait and CPostEffectMgr.Instance then
            CPostEffectMgr.Instance.DepthOfFieldEnable = true
        end
    end
    -- CClientObjectRoot.Inst.gameObject:SetActive(false)
    self.useBigShow = true
    CameraFollow.Inst.m_IsBigNpcTalk = true
    CameraFollow.Inst:SetUIVisiable(false)
    local List_String = MakeGenericClass(List, cs_string)
    CUIManager.SetPopViewsVisiable(false, Table2List({"JuQingDialogWnd","ShowPictureWnd","BusinessMainWnd"}, List_String),"juqing_dialog")
end
function CLuaJuQingDialogWnd:DisableBigNpcMode()
    if CPostProcessingMgr.s_NewPostProcessingOpen then
        local postEffectCtrl = CPostProcessingMgr.Instance.MainController
        if self.m_ContainsBigNpcPortrait and postEffectCtrl then
            postEffectCtrl.DepthOfFieldEnable = false
        end
    else
        if self.m_ContainsBigNpcPortrait and CPostEffectMgr.Instance then
            CPostEffectMgr.Instance.DepthOfFieldEnable = false
        end
    end
    -- CClientObjectRoot.Inst.gameObject:SetActive(true)
    self.useBigShow = false
    if not CUIManager.IsLoaded(CUIResources.ScreenCaptureWnd) then
        --ai相机模式下 不显示界面
        CameraFollow.Inst.m_IsBigNpcTalk = false
        if not CameraFollow.Inst.IsAICamera and not CameraFollow.Inst.IsBezierCamera and not CameraFollow.Inst.IsToricSpaceCamera then
            CameraFollow.Inst:SetUIVisiable(true)
            CUIManager.SetPopViewsVisiable(true, nil,"juqing_dialog")
        else
            CUIManager.ShowUIByChangeLayer(CUIResources.HeadInfoWnd, false, false)
            CUIManager.ShowUIByChangeLayer(CUIResources.CommonUIFxWnd, false, false)
        end
    end
end

function CLuaJuQingDialogWnd:Awake()
    self.m_CrosswordViewScript = self.transform:Find("DialogCrosswordView"):GetComponent(typeof(CCommonLuaScript))
    self.nameLabel = self.transform:Find("Anchor/Portrait/Name/Label"):GetComponent(typeof(UILabel))
    self.headPortrait = self.transform:Find("Anchor/Portrait"):GetComponent(typeof(L10.UI.CUITexture))
    self.conversation = self.transform:Find("Anchor/Description/ScrollView/Label"):GetComponent(typeof(UILabel))
    self.AnchorNode = self.transform:Find("Anchor").gameObject
    self.AnchorBigNode = self.transform:Find("AnchorBig").gameObject
    self.startTime = -1
    self.duration = 0
    self.curAIDialogId = 0
    self.useBigShow = false
    self.dialog = {}
    self.m_ContentIndex = 0
    self.m_IsToricBegin = false
    self.m_Npc2Expression = {}

    self.m_NormalSoundButton = self.transform:Find("Anchor/SoundButton").gameObject
    self.m_BigShowSoundLeftButton = self.transform:Find("AnchorBig/SoundButtonLeft").gameObject
    self.m_BigShowSoundRightButton = self.transform:Find("AnchorBig/SoundButtonRight").gameObject
    self.m_NormalSoundSettingButton = self.transform:Find("Anchor/SoundSettingButton").gameObject
    self.m_BigShowSoundSettingLeftButton = self.transform:Find("AnchorBig/SoundSettingButtonLeft").gameObject
    self.m_BigShowSoundSettingRightButton = self.transform:Find("AnchorBig/SoundSettingButtonRight").gameObject

    self.m_CrosswordViewScript.gameObject:SetActive(false)

    UIEventListener.Get(self.m_NormalSoundButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnSoundButtonClick(go)
    end)
    UIEventListener.Get(self.m_BigShowSoundLeftButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnSoundButtonClick(go)
    end)
    UIEventListener.Get(self.m_BigShowSoundRightButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnSoundButtonClick(go)
    end)
    UIEventListener.Get(self.m_NormalSoundSettingButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnSoundSettingButtonClick(go, false)
    end)
    UIEventListener.Get(self.m_BigShowSoundSettingLeftButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnSoundSettingButtonClick(go, false)
    end)
    UIEventListener.Get(self.m_BigShowSoundSettingRightButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnSoundSettingButtonClick(go, true)
    end)
end

function CLuaJuQingDialogWnd:IPhoneXAdaptation()
    if UIRoot.EnableIPhoneXAdaptation then
        self.m_AdaptationOffset = CThumbnailChatWnd.GetHeightOffsetForIphoneX()
    elseif CommonDefs.IsAndroidPlatform() then
        self.m_AdaptationOffset = 40
    else
        self.m_AdaptationOffset = 0
    end
    if self.m_AdaptationOffset > 0 then
        local w = self.AnchorNode.transform:Find("BottomInput"):GetComponent(typeof(UIWidget))
        if w then
            w.bottomAnchor.absolute = 9 + self.m_AdaptationOffset - 17
            w.topAnchor.absolute = 11 + self.m_AdaptationOffset - 17
            w:ResetAndUpdateAnchors()
        end
        w = self.AnchorBigNode.transform:Find("BottomInput"):GetComponent(typeof(UIWidget))
        if w then
            w.bottomAnchor.absolute = -8 + self.m_AdaptationOffset
            w.topAnchor.absolute = -6 + self.m_AdaptationOffset
            w:ResetAndUpdateAnchors()
        end
    end
end

function CLuaJuQingDialogWnd:OnEnable()
    self:IPhoneXAdaptation()
    g_ScriptEvent:AddListener("ClientObjCreate", self, "OnClientObjCreate")
end

function CLuaJuQingDialogWnd:OnDisable()
    g_ScriptEvent:RemoveListener("ClientObjCreate", self, "OnClientObjCreate")
end

function CLuaJuQingDialogWnd:OnClientObjCreate(args)
    local engineId = args[0]
    local obj = CClientObjectMgr.Inst:GetObject(engineId)
    if obj then
        if TypeIs(obj, typeof(CClientNpc)) and self.m_Npc2Expression[obj.TemplateId] then
            obj:ShowExpressionActionState(self.m_Npc2Expression[obj.TemplateId])
        elseif TypeIs(obj, typeof(CClientMainPlayer)) and self.m_Npc2Expression[0] then
            obj:ShowExpressionActionState(self.m_Npc2Expression[0])
        end
    end
end

function CLuaJuQingDialogWnd:SetSoundButtonVisibility(isBigShow, paragraph)
    local isNewBewbiePlay = CGuideMgr.Inst and CGuideMgr.Inst:IsNewbiePlayId() or false
    local hasSound = paragraph and paragraph.fmodEvent ~= nil and paragraph.fmodEvent ~= ""
    local isInCrosswordGame = paragraph and paragraph.crosswordGameId ~= nil
    self.m_NormalSoundButton:SetActive(not isBigShow and hasSound and not isInCrosswordGame)
    self.m_BigShowSoundLeftButton:SetActive(isBigShow and hasSound and cs_string.IsNullOrEmpty(paragraph.otherPortrait) and not isInCrosswordGame)
    self.m_BigShowSoundRightButton:SetActive(isBigShow and hasSound and (not cs_string.IsNullOrEmpty(paragraph.otherPortrait)) and not isInCrosswordGame)
    self.m_NormalSoundSettingButton:SetActive(not isBigShow and hasSound and not isInCrosswordGame and not isNewBewbiePlay)
    self.m_BigShowSoundSettingLeftButton:SetActive(isBigShow and hasSound and cs_string.IsNullOrEmpty(paragraph.otherPortrait) and not isInCrosswordGame and not isNewBewbiePlay)
    self.m_BigShowSoundSettingRightButton:SetActive(isBigShow and hasSound and (not cs_string.IsNullOrEmpty(paragraph.otherPortrait)) and not isInCrosswordGame and not isNewBewbiePlay)
end

function CLuaJuQingDialogWnd:OnSoundButtonClick()
    --juqingdialogwnd这里，m_ContentIndex指向的是下一句对话，这和taskdialogwnd处理的不太一样
    local paragraph = self.dialog and self.m_ContentIndex and self.dialog[self.m_ContentIndex] or nil
    if paragraph and paragraph.fmodEvent and paragraph.fmodEvent ~= "" then
        SoundManager.Inst:StartDialogSoundAndEnableSoundSetting(paragraph.fmodEvent)
    end
end

function CLuaJuQingDialogWnd:OnSoundSettingButtonClick(go, isLeft)
    if not CUIManager.IsLoaded(CLuaUIResources.MiniVolumeSettingsWnd) then
        LuaMiniVolumeSettingsMgr:OpenWnd(go, go.transform.position, UIWidget.Pivot.Left)
    else
        CUIManager.CloseUI(CLuaUIResources.MiniVolumeSettingsWnd)
    end
end

function CLuaJuQingDialogWnd:GetPortraitName(_type, id, emotionId)
    --填-1则不显示
    if id == -1 then return end
    local name = ""
    local portrait = ""
    if _type == 1 then
        if id == 0 then
            --主角自己的头像
            if CClientMainPlayer.Inst ~= nil then
                name = CClientMainPlayer.Inst.Name
                portrait = CClientMainPlayer.Inst.PortraitName
            --使用默认头像
            end
        else
            --从Player表读取其他角色头像
            local data = Initialization_Init.GetData(id)
            if data ~= nil then
                name = data.Name
                portrait = data.ResName
            end
        end
    elseif _type >= 2 then
        --从NPC表读取头像
        local curNpcId = CJuQingDialogMgr.Inst.TargetNPCId
        local npcId = id == 0 and curNpcId or id

        local data = NPC_NPC.GetData(npcId)
        if data ~= nil then
            name = data.Name
            if _type == 2 then
                portrait = data.Portrait
            elseif _type == 3 then
                portrait = data.BigPortrait
            end
        end
    end

    local mainplayerUseMonsterPortrait = false
    if (_type == 1 and id == 0 and CClientMainPlayer.Inst and CClientMainPlayer.Inst.AppearanceProp.ResourceId > 0) then
        local data = Transform_Transform.GetData(CClientMainPlayer.Inst.AppearanceProp.ResourceId)
        mainplayerUseMonsterPortrait = (data and data.UseMonsterPortrait > 0 or false)
    end
    if mainplayerUseMonsterPortrait then
        -- do nothing 对于主角变身的情况不显示表情头像
    elseif portrait ~= Constants.DefaultNPCPortrait and emotionId > 0 then
        portrait = SafeStringFormat3("%s_%02d", portrait, emotionId)
    end
    return name, portrait
end

function CLuaJuQingDialogWnd:Init()
    self.m_ShowChoices = false
    UIEventListener.Get(self.transform:Find("_BgMask_").gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnBgMaskClick(go)
    end)

    if CConversationMgr.s_EnableChangeBgAlpha then
        local bgTransform = self.transform:Find("Anchor/Background")
        if bgTransform ~= nil then
            CommonDefs.GetComponent_Component_Type(bgTransform, typeof(UIWidget)).alpha = CConversationMgr.s_BgAlpha
        end
    end
    self:SetSoundButtonVisibility(false, nil)
    if not CJuQingDialogMgr.Inst.IsSerialDialog then
        self.curAIDialogId = 0
        self.nameLabel.text = CJuQingDialogMgr.Inst.SpeakerName
        self.conversation.text = SafeStringFormat3("[c][%s]%s[-][/c]", NGUIText.EncodeColor24(self.conversation.color), CChatLinkMgr.TranslateToNGUIText(CJuQingDialogMgr.Inst.Content, false))
        self.duration = CJuQingDialogMgr.Inst.Duration
        self:LoadPortrait(CJuQingDialogMgr.Inst.PortraitName)
        self.startTime = Time.realtimeSinceStartup
        self.AnchorNode:SetActive(true)
        self.AnchorNode.transform:Find("BottomInput").gameObject:SetActive(false)
        self.AnchorBigNode:SetActive(false)
        self:InitAnimatedIcon()
        return
    end

    local text = CJuQingDialogMgr.Inst.Content
    CJuQingDialogMgr.Inst:ClearDialogContent()
    self.curAIDialogId = CJuQingDialogMgr.Inst.AIDialogId
    self:InitAnimatedIcon()
    local oldLen = 0
    if self.dialog then
        oldLen = #self.dialog
    else
        self.dialog = {}
    end

    self:CheckContainsBigNpcPortrait(text)
    if string.find(text, "bigNpc") then
        self:EnableBigNpcMode()
    end

    for item1, item2, item3, item4 in string.gmatch(text, "<popup%s(%w+):(-?%d+,?%d*)%s?([^>]*)>([^<]*)</popup>") do
        local p = self:Parse(item1, item2, item3, item4)
        table.insert(self.dialog, p)
    end

    if Dialog_TaskDialog.Exists(self.curAIDialogId) then
        local designData = Dialog_TaskDialog.GetData(self.curAIDialogId)
        if designData and designData.ChoiceDisplay and designData.ChoiceDisplay ~= "" then --如果选项有自定义的显示，则按这个自定义显示来显示
            local item1, item2, item3, item4 = string.match(designData.ChoiceDisplay, "<popup%s(%w+):(-?%d+,?%d*)%s?([^>]*)>([^<]*)</popup>")
            local p = self:Parse(item1, item2, item3, item4)
            self.m_ChoiceDisplayParagraph = p
        end
    end

    if oldLen <= 0 then
        self:ShowContent()
    end
end

function CLuaJuQingDialogWnd:CheckContainsBigNpcPortrait(text)
    if string.find(text, "bigNpc:%d+") then
        self.m_ContainsBigNpcPortrait = true
    else
        self.m_ContainsBigNpcPortrait = false
    end
end

function CLuaJuQingDialogWnd:InitAnimatedIcon()
    local iconGo = self.AnchorNode.transform:Find("Description/Sprite").gameObject
    if self.curAIDialogId > 0 then
        iconGo:SetActive(true)
    else
        iconGo:SetActive(false)
    end
end

function CLuaJuQingDialogWnd:Parse(item1, item2, item3, item4)
    local p = {
        playerType = 0,
        emotionId = 0,
        writeId = 0,
        otherPlayerType = 0,
        otherEmotionId = 0,
        dialogBgType = 0,
        choices = nil,
        expression = {},
    }
    if (item1 == "player") then
        p.playerType = 1
    elseif (item1 == "npc") then
        p.playerType = 2
    elseif (item1 == "bigNpc") then
        p.playerType = 3
    end

    p.name = nil
    p.portrait = Constants.DefaultNPCPortrait
    p.emotionId = 0
    p.fmodEvent = nil
    p.crosswordGameId = nil
    p.toricCameraParams = nil

    local id, emotionId = 0, 0
    local firstTalk = item2
    if (string.find(firstTalk, ",", 1, true) ~= nil) then
        id, emotionId = string.match(firstTalk, "(%d+),(%d+)")
        id, emotionId = tonumber(id or 0), tonumber(emotionId or 0)
        p.emotionId = emotionId
    else
        id = tonumber(firstTalk)
    end

    local extraInfo = item3
    if not (extraInfo == nil or extraInfo == "") then
        local fmodEvent = string.match(extraInfo, "audio:(event:.+)")
        if fmodEvent then
            p.fmodEvent = fmodEvent
        end

        local emotionId = string.match(extraInfo, "emotion:(%d+)")
        if emotionId then
            p.emotionId = math.floor(tonumber(emotionId))
        end

        local writeId = string.match(extraInfo, "write:(%d+)")
        if writeId then
            p.writeId = math.floor(tonumber(writeId))
        end

        local crosswordGameId = string.match(extraInfo, "CrosswordGame:(%d+)")
        if crosswordGameId then
            p.crosswordGameId = math.floor(tonumber(crosswordGameId))
        end

        local npcId1, npcId2, alpha, theta, phi, duration = string.match(extraInfo, "toriccamera:(%d+),(%d+),([0-9%.]+),(-?[0-9%.]+),(-?[0-9%.]+),([0-9%.]+)")
        if npcId1 and npcId2 and alpha and theta and phi and duration then
            local toricCameraParams = {}
            toricCameraParams.npcId1 = tonumber(npcId1)
            toricCameraParams.npcId2 = tonumber(npcId2)
            toricCameraParams.alpha = alpha
            toricCameraParams.theta = theta
            toricCameraParams.phi = phi
            toricCameraParams.duration = duration

            local noSway = false
            _,_,_,_,_,_,noSway = string.match(extraInfo, "toriccamera:(%d+),(%d+),([0-9%.]+),(-?[0-9%.]+),(-?[0-9%.]+),([0-9%.]+),(%d+)")
            print(extraInfo,noSway)
            if noSway and noSway == "1" then
                noSway = true
            else
                noSway = false
            end
            toricCameraParams.noSway = noSway

            p.toricCameraParams = toricCameraParams
        end

        local expressionStr = string.match(extraInfo, "expression:([^%s]+)")
        if expressionStr then
            for expressionId, npcStr in string.gmatch(expressionStr, "(%d+),([^;]+)") do
                for npcId in string.gmatch(npcStr, "%d+") do
                    p.expression[math.floor(tonumber(npcId))] = math.floor(tonumber(expressionId))
                end
            end
        end

        local p1, p2 = string.match(extraInfo, "other:(%w+):(%d+,?%d*)")
        if p1 then
            if p1 == "player" then
                p.otherPlayerType = 1
            elseif p1 == "npc" then
                p.otherPlayerType = 2
            elseif p1 == "bigNpc" then
                p.otherPlayerType = 3
            end
            local other_id, otherEmotionId = 0, 0
            local otherTalk = p2
            if (string.find(otherTalk, ",", 1, true) ~= nil) then
                other_id, otherEmotionId = string.match(otherTalk, "(%d+),(%d+)")
                other_id, otherEmotionId = tonumber(other_id or 0), tonumber(otherEmotionId or 0)
                p.otherEmotionId = otherEmotionId
            else
                other_id = tonumber(otherTalk)
            end

            local other_name, other_portrait = self:GetPortraitName(p.otherPlayerType, other_id, otherEmotionId or 0)
            p.otherPortrait = other_portrait
            p.otherName = other_name
        end
    end
    --设置头像及名称
    local name, portrait = self:GetPortraitName(p.playerType, id, p.emotionId)
    p.portrait = portrait
    p.name = name

    --设置内容
    p.content = item4

    return p
end

function CLuaJuQingDialogWnd:ShowBigNpc(paragraph)
    self.AnchorNode:SetActive(false)
    self.AnchorBigNode:SetActive(true)

    local portrait = self.AnchorBigNode.transform:Find("Portrait"):GetComponent(typeof(CUITexture))
    if paragraph.portrait then
        portrait:LoadBigNPCPortrait(paragraph.portrait, false)
    else
        portrait:Clear()
    end

    local nameLabel = self.AnchorBigNode.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local nameLabel2 = self.AnchorBigNode.transform:Find("NameLabel2"):GetComponent(typeof(UILabel))
    local portrait2 = self.AnchorBigNode.transform:Find("Portrait2"):GetComponent(typeof(CUITexture))
    local portrait3 = self.AnchorBigNode.transform:Find("Portrait3"):GetComponent(typeof(CUITexture))
    local contentLabel = self.AnchorBigNode.transform:Find("ContentLabel"):GetComponent(typeof(UILabel))

    if paragraph.otherPortrait == nil or paragraph.otherPortrait == "" then
        portrait.color = Color.white
        self.AnchorBigNode.transform:Find("Portrait2").gameObject:SetActive(false)

        if paragraph.name then
            nameLabel.gameObject:SetActive(true)
            nameLabel.text = paragraph.name
        else
            nameLabel.gameObject:SetActive(false)
            nameLabel.text = nil
        end

        nameLabel2.gameObject:SetActive(false)
        portrait2.gameObject:SetActive(false)
        portrait3.gameObject:SetActive(false)
    else
        portrait.color = Color(0.22, 0.22, 0.22, 0.86)--半透明
        nameLabel.gameObject:SetActive(false)
        if paragraph.otherName then
            nameLabel2.gameObject:SetActive(true)
            nameLabel2.text = paragraph.otherName
        else
            nameLabel2.gameObject:SetActive(false)
            nameLabel2.text = nil
        end

        if paragraph.otherPlayerType == 3 then --bignpc
            portrait2.gameObject:SetActive(false)
            portrait3.gameObject:SetActive(true)
            portrait3:LoadBigNPCPortrait(paragraph.otherPortrait, false)
        else
            portrait2.gameObject:SetActive(true)
            portrait3.gameObject:SetActive(false)
            portrait2:LoadNPCPortrait(paragraph.otherPortrait, false)
        end
    end

    if paragraph.content then
        contentLabel.text = SafeStringFormat3("[c][%s]%s[-][/c]", NGUIText.EncodeColor24(self.conversation.color), CChatLinkMgr.TranslateToNGUIText(paragraph.content, false))
    else
        contentLabel.text = nil
    end
    self:ProcessFmodEventAndCrossword(paragraph)
    local template = self.AnchorBigNode.transform:Find("ItemTemplate").gameObject
    template:SetActive(false)
end

function CLuaJuQingDialogWnd:ProcessFmodEventAndCrossword(paragraph)
    local voiceCallBack = nil
    if paragraph.fmodEvent then
        voiceCallBack = function()
            self.m_CancelSoundPlayAction = CLoadingWnd.Inst:PushDelayAction(DelegateFactory.Action(function()
                SoundManager.Inst:StartDialogSound(paragraph.fmodEvent)
            end))
        end
    end
    if paragraph.crosswordGameId then
        self:InitCrosswordGame(paragraph.crosswordGameId, function()
            if voiceCallBack then
                voiceCallBack()
            end
            paragraph.crosswordGameId = nil
            self:SetSoundButtonVisibility(self.useBigShow, paragraph)
        end)
    else
        self.m_CrosswordViewScript.gameObject:SetActive(false)
        if voiceCallBack then
            voiceCallBack()
        end
    end
end

function CLuaJuQingDialogWnd:ProcessToricCamera(paragraph)
    local toric = paragraph.toricCameraParams
    if toric then
        --是否还在进行上个镜头的切换
        LuaDialogCameraMgr.ProcessToricCamera(toric.npcId1, toric.npcId2, toric.alpha, toric.theta, toric.phi, toric.duration, toric.noSway)
    end
end

function CLuaJuQingDialogWnd:ShowContent()
    if self.dialog == nil or self.m_ContentIndex >= #self.dialog then
        --有选项的时候
        if Dialog_TaskDialog.Exists(self.curAIDialogId) then
            local data = Dialog_TaskDialog.GetData(self.curAIDialogId)
            if data.ChoiceName and data.ChoiceName.Length > 0 then
                self.m_ShowChoices = true
                local choices = {}
                for i = 1, data.ChoiceName.Length do
                    local item = {
                        text = data.ChoiceName[i - 1],
                    }
                    if data.ChoiceConfirmMessage ~= nil and data.ChoiceConfirmMessage[i - 1] and data.ChoiceConfirmMessage[i - 1] ~= "" then
                        item.confirmMessage = data.ChoiceConfirmMessage[i - 1]
                    end
                    table.insert(choices, item)
                end
                local itemTemplate = self.AnchorBigNode.transform:Find("ItemTemplate").gameObject
                itemTemplate:SetActive(false)

                local selectionsTf = self.AnchorBigNode.transform:Find("Selections")
                for i, v in ipairs(choices) do
                    local go = NGUITools.AddChild(selectionsTf.gameObject, itemTemplate)
                    go:SetActive(true)
                    local label = go.transform:Find("Label"):GetComponent(typeof(UILabel))

                    --处理icon的逻辑
                    local text = v.text
                    if string.sub(text, 1, 5) == "<icon" then
                        local a, b = string.match(text, "<icon%s(.+)/>(.*)")
                        label.text = b
                        local icon = go.transform:Find("Icon"):GetComponent(typeof(CUITexture))
                        icon:LoadMaterial(a)
                    else
                        label.text = text
                    end

                    UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function(go)
                        if CLuaJuQingDialogMgr.m_ClientDialogCallback then
                            CLuaJuQingDialogMgr.m_ClientDialogCallback(i)
                        else
                            Gac2Gas.SayDialogEnd(CLuaJuQingDialogMgr.m_EngineId, self.curAIDialogId, i)
                        end
                        CUIManager.CloseUI(CUIResources.JuQingDialogWnd)
                    end)
                end
                selectionsTf:GetComponent(typeof(UIGrid)):Reposition()

                if self.m_ChoiceDisplayParagraph then
                    --取选项内容
                    self:ShowBigNpc(self.m_ChoiceDisplayParagraph)
                else
                    --显示右侧头像 都是双人对话，找到某一个对话的右侧信息
                    local rightInfo = nil
                    for i = #self.dialog, 1, -1 do
                        local item = self.dialog[i]
                        if item.otherPortrait and item.otherPortrait ~= "" then
                            rightInfo = {
                                otherPortrait = item.otherPortrait,
                                -- otherName = item.otherName,
                                otherPlayerType = item.otherPlayerType,
                            }
                            break
                        end
                    end
                    if rightInfo then
                        self:ShowBigNpc(rightInfo)
                    else
                        self:ShowBigNpc({})
                    end
                end
                -- 屏蔽输入框
                self.AnchorBigNode.transform:Find("BottomInput").gameObject:SetActive(false)
            end
        end

        if not self.m_ShowChoices then
            self:OnFinishDialog()
            CUIManager.CloseUI(CUIResources.JuQingDialogWnd)
        end
        return
    end

    local paragraph = self.dialog[self.m_ContentIndex + 1]
    self:SetSoundButtonVisibility(self.useBigShow, paragraph)
    if self.useBigShow then
        self:ShowBigNpc(paragraph)
    else
        self.AnchorNode:SetActive(true)
        self.AnchorBigNode:SetActive(false)

        self.nameLabel.text = paragraph.name
        self:LoadPortrait(paragraph.portrait)
        self.conversation.text = SafeStringFormat3("[c][%s]%s[-][/c]", NGUIText.EncodeColor24(self.conversation.color), CChatLinkMgr.TranslateToNGUIText(paragraph.content, false))

        self:ProcessFmodEventAndCrossword(paragraph)
    end
    self:ProcessToricCamera(paragraph)
    self:ProcessExpression(paragraph)
    self:ProcessBottomInput(paragraph)
    self.m_ContentIndex = self.m_ContentIndex + 1
end

function CLuaJuQingDialogWnd:OnFinishDialog()
    if CJuQingDialogMgr.Inst.IsSerialDialog then
        if self.dialog == nil or self.m_ContentIndex >= #self.dialog then
            --对话结束时才发送rpc，主要是解决断线重连问题
            --这样如果中途关闭窗口（断线和顶号AI端进行了处理），AI可能进行不下去
            if Dialog_TaskDialog.Exists(self.curAIDialogId) then
                if not CLuaJuQingDialogMgr.m_ClientDialogCallback then
                    Gac2Gas.SayDialogEnd(CLuaJuQingDialogMgr.m_EngineId, self.curAIDialogId, 0)
                end
            end
        end
    end
end

function CLuaJuQingDialogWnd:LoadPortrait(portraitName)
    self.headPortrait:LoadPortrait(portraitName, false)
end

function CLuaJuQingDialogWnd:OnDestroy()
    if self.useBigShow then
        self:DisableBigNpcMode()
    end
    CLuaJuQingDialogMgr.m_EngineId = 0
    CLuaJuQingDialogMgr.m_ClientDialogCallback = nil
    self:StopSound()
    LuaDialogCameraMgr.EndDialogCamera()
end

function CLuaJuQingDialogWnd:Update()
    if CJuQingDialogMgr.Inst.IsSerialDialog then
        return
    end
    if Time.realtimeSinceStartup - self.startTime >= self.duration then
        CUIManager.CloseUI(CUIResources.JuQingDialogWnd)
    end
end

function CLuaJuQingDialogWnd:OnBgMaskClick(go)
    if not CJuQingDialogMgr.Inst.IsSerialDialog then
        return
    end
    if self.m_ContentIndex <= #self.dialog and self.dialog[self.m_ContentIndex].writeId ~= 0 then
        return
    end
    if not self.m_ShowChoices then
        self:ShowContent()
    end
end

function CLuaJuQingDialogWnd:StopSound()
    if self.m_CancelSoundPlayAction then
        invoke(self.m_CancelSoundPlayAction)
        self.m_CancelSoundPlayAction = nil
    end
    SoundManager.Inst:StopDialogSound()
end

function CLuaJuQingDialogWnd:InitCrosswordGame(gameId, callback)
    self.m_CrosswordViewScript:Awake()
    local script = self.m_CrosswordViewScript.m_LuaSelf
    script.gameObject:SetActive(true)
    script:InitGame(gameId, function()
        callback()
    end)
end

function CLuaJuQingDialogWnd:ProcessExpression(paragraph)
    if next(paragraph.expression) ~= nil then
        for k, v in pairs(paragraph.expression) do
            self.m_Npc2Expression[k] = v
        end
        CClientObjectMgr.Inst:ForeachObject(DelegateFactory.Action_CClientObject(function(obj)
            if TypeIs(obj, typeof(CClientNpc)) and paragraph.expression[obj.TemplateId] then
                obj:ShowExpressionActionState(paragraph.expression[obj.TemplateId])
            elseif TypeIs(obj, typeof(CClientMainPlayer)) and paragraph.expression[0] then
                obj:ShowExpressionActionState(paragraph.expression[0])
            end
        end))
    end
end

function CLuaJuQingDialogWnd:ProcessBottomInput(paragraph)
    local writeId = paragraph.writeId
    local writeData = ZhuJueJuQing_Write.GetData(writeId)
    local parent = self.useBigShow and self.AnchorBigNode.transform or self.AnchorNode.transform
    local bottomInput = parent:Find("BottomInput")
    local contentLabel
    if self.useBigShow then
        contentLabel = self.AnchorBigNode.transform:Find("ContentLabel"):GetComponent(typeof(UILabel))
        contentLabel.pivot = UIWidget.Pivot.Left
    end

    if not writeData then
        bottomInput.gameObject:SetActive(false)
    elseif writeData.NeedInput == 0 then
        bottomInput.gameObject:SetActive(false)
        CLuaHandWriteEffectMgr.ShowHandWriteEffect(writeId, function()
            self:ShowContent()
        end)
    elseif writeData.NeedInput == 1 then
        bottomInput.gameObject:SetActive(true)
        UIEventListener.Get(bottomInput:Find("SendButton").gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            self:OnInputOkButtonClick(writeId)
        end)
        local chatInput = bottomInput:Find("ChatInput"):GetComponent(typeof(UIInput))
        chatInput.value = ""
        chatInput.onReturnKeyPressed = DelegateFactory.Action_GameObject(function(go)
            self:OnInputOkButtonClick(writeId)
        end)
        if self.useBigShow then contentLabel.pivot = UIWidget.Pivot.TopLeft end
    end

    -- 输入框出现时，正文提示输入的内容也要适配安全区
    if self.useBigShow then
        LuaUtils.SetLocalPositionY(contentLabel.transform, writeData and writeData.NeedInput == 1 and 19 + self.m_AdaptationOffset or -53)
    end
end

function CLuaJuQingDialogWnd:OnInputOkButtonClick(writeId)
    local writeData = ZhuJueJuQing_Write.GetData(writeId)
    if writeData then
        local parent = self.useBigShow and self.AnchorBigNode.transform or self.AnchorNode.transform
        local chatInput = parent:Find("BottomInput/ChatInput"):GetComponent(typeof(UIInput))
        local inputStr = StringTrim(chatInput.value)

        -- 替换所有空格, 。等等
        string.gsub(inputStr, LocalString.GetString(" "), "")
        string.gsub(inputStr, LocalString.GetString("，"), "")
        string.gsub(inputStr, LocalString.GetString("。"), "")

        local ret = CWordFilterMgr.Inst:DoFilterOnSend(inputStr, nil, nil, false)
        if ret.msg == nil or ret.shouldBeIgnore then
            CUIManager.ShowUIByChangeLayer(CUIResources.MiddleNoticeCenter, false, false)
            g_MessageMgr:ShowMessage("Speech_Violation")
            return
        end

        local needStr = StringTrim(writeData.Text)
        if (string.find(inputStr, needStr, 1, true) ~= nil) then
            CLuaHandWriteEffectMgr.ShowHandWriteEffect(writeId, function()
                self:ShowContent()
            end)
        else
            g_MessageMgr:ShowMessage("INPUT_NOT_MATCH")
        end
    end
end
