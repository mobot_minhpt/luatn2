local UILabel = import "UILabel"
local UIGrid = import "UIGrid"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
LuaShuangshiyi2023DiscountBuyFashionWnd = class()
LuaShuangshiyi2023DiscountBuyFashionWnd.couponId = nil
LuaShuangshiyi2023DiscountBuyFashionWnd.shoppingIdList = nil

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaShuangshiyi2023DiscountBuyFashionWnd, "Tips", "Tips", UILabel)
RegistChildComponent(LuaShuangshiyi2023DiscountBuyFashionWnd, "grid", "grid", UIGrid)
RegistChildComponent(LuaShuangshiyi2023DiscountBuyFashionWnd, "shoppingMallItem", "shoppingMallItem", GameObject)
RegistChildComponent(LuaShuangshiyi2023DiscountBuyFashionWnd, "SellRoot", "SellRoot", GameObject)
RegistChildComponent(LuaShuangshiyi2023DiscountBuyFashionWnd, "buyButton", "buyButton", GameObject)
RegistChildComponent(LuaShuangshiyi2023DiscountBuyFashionWnd, "qnCostAndOwnMoney", "qnCostAndOwnMoney", CCurentMoneyCtrl)
RegistChildComponent(LuaShuangshiyi2023DiscountBuyFashionWnd, "OriginalPrice", "OriginalPrice", GameObject)
RegistChildComponent(LuaShuangshiyi2023DiscountBuyFashionWnd, "PriceLabel", "PriceLabel", UILabel)
RegistChildComponent(LuaShuangshiyi2023DiscountBuyFashionWnd, "TitleLabel", "TitleLabel", GameObject)

--@endregion RegistChildComponent end

function LuaShuangshiyi2023DiscountBuyFashionWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self:InitWndData()
    self:InitUIEvent()
    self:RefreshConstUI()
end

function LuaShuangshiyi2023DiscountBuyFashionWnd:Init()

end

--@region UIEvent

--@endregion UIEvent

function LuaShuangshiyi2023DiscountBuyFashionWnd:InitWndData()
    self.couponCfgData = Double11_ExteriorDiscountCoupons.GetData(LuaShuangshiyi2023DiscountBuyFashionWnd.couponId)
    self.selectFashionId = nil
    self.isBoughtAll = true
end 

function LuaShuangshiyi2023DiscountBuyFashionWnd:InitUIEvent()
    UIEventListener.Get(self.buyButton).onClick = DelegateFactory.VoidDelegate(function (_)
        if self.isBoughtAll then
            g_MessageMgr:ShowMessage("Double11_2023Discount_No_Available_Item_Tips")
            return
        end
        
        if self.selectFashionId then
            Gac2Gas.Request2023Double11BuyExteriorByDiscountCoupons(self.selectFashionId, LuaShuangshiyi2023DiscountBuyFashionWnd.couponId)
            CUIManager.CloseUI("Shuangshiyi2023DiscountBuyFashionWnd")
        else
            g_MessageMgr:ShowMessage("Double11_2023Discount_Select_No_Item_Tips")
        end
    end)
end 

function LuaShuangshiyi2023DiscountBuyFashionWnd:RefreshConstUI()
    self.Tips.text = g_MessageMgr:FormatMessage("Double11_2023Discount_Buy_Fashion_Tips", self.couponCfgData.Name)
    self.OriginalPrice:SetActive(false)
    self.qnCostAndOwnMoney:SetType(EnumMoneyType.LingYu_WithBind, EnumPlayScoreKey.NONE, true)
    self.qnCostAndOwnMoney:SetCost(0)


    local boughtList = {}
    if CClientMainPlayer.Inst and CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eDouble11ExteriorDiscountCouponsData) then
        local playDataU = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eDouble11ExteriorDiscountCouponsData)
        local list = g_MessagePack.unpack(playDataU.Data.Data)
        boughtList = list[4] and list[4] or {}
    end
    
    local tbl = {}
    for i = 1, #LuaShuangshiyi2023DiscountBuyFashionWnd.shoppingIdList do
        local shopMallItemId = LuaShuangshiyi2023DiscountBuyFashionWnd.shoppingIdList[i]
        if boughtList[shopMallItemId] then
        else
            table.insert(tbl, shopMallItemId)
            self.isBoughtAll = false
        end
    end
    
    self.fashionObjList = {}
    for i = 1, #tbl do
        local shopMallItemId = tbl[i]
        local fashionCfgData = Mall_LingYuMall.GetData(shopMallItemId)
        local itemCfgData = Item_Item.GetData(shopMallItemId)
        
        local templateItem = CommonDefs.Object_Instantiate(self.shoppingMallItem)
        templateItem:SetActive(true)
        templateItem.transform.parent = self.grid.transform
        templateItem.transform.localScale = Vector3.one
        
        local texComp = templateItem.transform:Find("Texture"):GetComponent(typeof(CUITexture))
        local nameComp = templateItem.transform:Find("Name"):GetComponent(typeof(UILabel))
        local priceComp = templateItem.transform:Find("Price"):GetComponent(typeof(UILabel))
        texComp:LoadMaterial(itemCfgData.Icon)
        nameComp.text = itemCfgData.Name
        priceComp.text = math.floor(fashionCfgData.Jade * self.couponCfgData.Discount + 0.5)

        UIEventListener.Get(templateItem).onClick = DelegateFactory.VoidDelegate(function (_)
            self.qnCostAndOwnMoney:SetCost(math.floor(fashionCfgData.Jade * self.couponCfgData.Discount + 0.5))
            self.PriceLabel.text = fashionCfgData.Jade
            self.selectFashionId = shopMallItemId
            self.OriginalPrice:SetActive(true)

            if self.fashionObjList then
                for k, v in pairs(self.fashionObjList) do
                    v.transform:Find("IsSelected").gameObject:SetActive(k == shopMallItemId)
                end
            end
        end)
        
        self.fashionObjList[shopMallItemId] = templateItem

        if i == 1 then
            self.qnCostAndOwnMoney:SetCost(math.floor(fashionCfgData.Jade * self.couponCfgData.Discount + 0.5))
            self.PriceLabel.text = fashionCfgData.Jade
            self.selectFashionId = shopMallItemId
            self.OriginalPrice:SetActive(true)

            if self.fashionObjList then
                for k, v in pairs(self.fashionObjList) do
                    v.transform:Find("IsSelected").gameObject:SetActive(k == shopMallItemId)
                end
            end
        end
    end
    
    self.grid:Reposition()
end 
