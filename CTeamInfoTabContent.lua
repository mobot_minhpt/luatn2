-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CMiniChatMgr = import "L10.UI.CMiniChatMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local CTeamFollowMgr = import "L10.Game.CTeamFollowMgr"
local CTeamGroupMgr = import "L10.Game.CTeamGroupMgr"
local CTeamInfoTabContent = import "L10.UI.Team.CTeamInfoTabContent"
local CTeamMatchMgr = import "L10.Game.CTeamMatchMgr"
local CTeamMatchWnd = import "L10.UI.CTeamMatchWnd"
local CTeamMember = import "L10.Game.CTeamMember"
local CTeamMemberItem = import "L10.UI.Team.CTeamMemberItem"
local CTeamMgr = import "L10.Game.CTeamMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local TeamMatch_Activities = import "L10.Game.TeamMatch_Activities"
local UIEventListener = import "UIEventListener"
local UInt64 = import "System.UInt64"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CTeamInfoTabContent.m_RefreshMember_CS2LuaHook = function (this, data) 
    if data == nil then
        return
    end
    CommonDefs.ListIterate(this.memberItems, DelegateFactory.Action_object(function (___value) 
        local item = ___value
        if item.memberData ~= nil and item.memberData.BasicProp.Id == data.BasicProp.Id then
            item:Init(data, false)
        end
    end))
end
CTeamInfoTabContent.m_RefreshTeamMatchVisuality_CS2LuaHook = function (this) 
    if CClientMainPlayer.Inst ~= nil then
        --仅队长显示
        this.teamMatchNode.gameObject:SetActive(CTeamMgr.Inst:TeamExists() and CTeamMgr.Inst:MainPlayerIsTeamLeader())
    end
end
CTeamInfoTabContent.m_Start_CS2LuaHook = function (this) 
    UIEventListener.Get(this.targetBtn).onClick = MakeDelegateFromCSFunction(this.OnTargetButtonClick, VoidDelegate, this)
    UIEventListener.Get(this.targetBg).onClick = MakeDelegateFromCSFunction(this.OnTargetButtonClick, VoidDelegate, this)
    --UIEventListener.Get(autoMatchBtn).onClick = this.OnAutoMatchButtonClick;
    UIEventListener.Get(this.callBtn).onClick = MakeDelegateFromCSFunction(this.OnCallButtonClick, VoidDelegate, this)
    UIEventListener.Get(this.inviteBtn).onClick = MakeDelegateFromCSFunction(this.OnInviteButtonClick, VoidDelegate, this)
    UIEventListener.Get(this.leaveBtn).onClick = MakeDelegateFromCSFunction(this.OnLeaveButtonClick, VoidDelegate, this)
    UIEventListener.Get(this.leaderFollowBtn).onClick = MakeDelegateFromCSFunction(this.OnLeaderFollowButtonClick, VoidDelegate, this)
    UIEventListener.Get(this.cancleFollowBtn).onClick = MakeDelegateFromCSFunction(this.OnCancleFollowButtonClick, VoidDelegate, this)
    UIEventListener.Get(this.createBtn).onClick = MakeDelegateFromCSFunction(this.OnCreateButtonClick, VoidDelegate, this)
    UIEventListener.Get(this.quickJoinTeamBtn).onClick = MakeDelegateFromCSFunction(this.OnQuickJoinTeamButtonClick, VoidDelegate, this)
    UIEventListener.Get(this.moreOptBtn).onClick = MakeDelegateFromCSFunction(this.OnMoreOption, VoidDelegate, this)
    UIEventListener.Get(this.showFightDataBtn).onClick = MakeDelegateFromCSFunction(this.ShowFightData, VoidDelegate, this)
end
CTeamInfoTabContent.m_OnEnable_CS2LuaHook = function (this) 
    this.matchLabel.text = CTeamMatchMgr.Inst:GetMatchDesc()
    this.callBtn:SetActive(CTeamMgr.Inst:MainPlayerIsTeamLeader())
    --Refresh();
    --this.LoadMembersData(CTeamMgr.Inst.GetTeamMembers());
    EventManager.AddListener(EnumEventType.RefreshTeamMatch, MakeDelegateFromCSFunction(this.RefreshTeamMatch, Action0, this))
    EventManager.AddListener(EnumEventType.TeamInfoChange, MakeDelegateFromCSFunction(this.OnTeamInfoChange, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.TeamMemberInfoChange, MakeDelegateFromCSFunction(this.OnTeamMemberInfoChange, MakeGenericClass(Action1, UInt64), this))

    CommonDefs.ListIterate(this.memberItems, DelegateFactory.Action_object(function (___value) 
        local item = ___value
        item:RefreshAppearance()
    end))
end
CTeamInfoTabContent.m_OnTeamMemberInfoChange_CS2LuaHook = function (this, memberId) 
    this.callBtn:SetActive(CTeamMgr.Inst:MainPlayerIsTeamLeader())
    if CClientMainPlayer.Inst ~= nil then
        this:RefreshCancelFollowButtonState()
    end
end
CTeamInfoTabContent.m_OnMoreOption_CS2LuaHook = function (this, go) 
    local openTeamRecruit = PopupMenuItemData(LocalString.GetString("查看招募"), DelegateFactory.Action_int(function (_)
        LuaTeamRecruitMgr:OpenTeamRecruitWnd()
    end), false, nil)
    local openTeamGroup = PopupMenuItemData(LocalString.GetString("打开团队"), MakeDelegateFromCSFunction(this.OnOpenTeamGroup, MakeGenericClass(Action1, Int32), this), false, nil)
    local items = nil

    if CTeamMgr.Inst:MainPlayerIsTeamLeader() then
        items = CreateFromClass(MakeArrayClass(PopupMenuItemData), 4)
        items[0] = openTeamRecruit
        local depart = PopupMenuItemData(LocalString.GetString("解散队伍"), MakeDelegateFromCSFunction(this.OnDepartTeam, MakeGenericClass(Action1, Int32), this), false, nil)
        items[1] = depart
        if not CTeamGroupMgr.Instance:InTeamGroup() then
            local createTeamGroup = PopupMenuItemData(LocalString.GetString("创建团队"), MakeDelegateFromCSFunction(this.OnCreateTeamGroup, MakeGenericClass(Action1, Int32), this), false, nil)
            items[2] = createTeamGroup
        else
            items[2] = openTeamGroup
        end

        local confirm = PopupMenuItemData(LocalString.GetString("准备确认"), DelegateFactory.Action_int(function()
            Gac2Gas.StartTeamConfirm()
        end), false, nil)
        items[3] = confirm
    else
        items = CreateFromClass(MakeArrayClass(PopupMenuItemData), 2)
        items[0] = openTeamRecruit
        items[1] = openTeamGroup
    end
    CPopupMenuInfoMgr.ShowPopupMenu(items, this.moreOptBtn.transform, CPopupMenuInfoMgr.AlignType.Top)
end
CTeamInfoTabContent.m_OnTargetButtonClick_CS2LuaHook = function (this, go) 
    --目标按钮点击事件响应
    --目标按钮点击事件响应
    if CTeamMatchMgr.Inst.IsTeamMatching then
        g_MessageMgr:ShowMessage("CannotOpenTeamMatch")
    else
        CTeamMatchWnd.ShowType = CTeamMatchWnd.EnumShowType.EnumTeamMatch
        CUIManager.ShowUI(CUIResources.TeamMatchWnd)
    end
end
CTeamInfoTabContent.m_OnCallButtonClick_CS2LuaHook = function (this, go) 

    if not CTeamMatchMgr.Inst.HasMatchTarget then
        g_MessageMgr:ShowMessage("Choose_Target")
        return
    end
    local rows = CreateFromClass(MakeGenericClass(List, PopupMenuItemData))
    CommonDefs.ListAdd(rows, typeof(PopupMenuItemData), PopupMenuItemData(LocalString.GetString("帮会频道"), DelegateFactory.Action_int(function (row) 
        local hasGuild = (CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst:IsInGuild())
        if not hasGuild then
            g_MessageMgr:ShowMessage("NOT_IN_ANY_GUILD")
            return
        end
        CMiniChatMgr.Show(LocalString.GetString("一键喊话-帮会频道"), 30, CMiniChatMgr.ChatType.TeamBroadcast, DelegateFactory.Action_MiniChatCallBackInfo(function (data) 
            CTeamMatchMgr.Inst:BroadcastAddTeamRequestMsg(1, data.msg, CTeamMatchMgr.Inst.selectedActivityId, CTeamMatchMgr.Inst.IncludeNewbie)
        end))
    end), false, nil))
    local activity = TeamMatch_Activities.GetData(CTeamMatchMgr.Inst.selectedActivityId)
    if activity ~= nil and activity.GuildActivity ~= 1 then
        CommonDefs.ListAdd(rows, typeof(PopupMenuItemData), PopupMenuItemData(LocalString.GetString("队伍频道"), DelegateFactory.Action_int(function (row) 
            CMiniChatMgr.Show(LocalString.GetString("一键喊话-队伍频道"), 30, CMiniChatMgr.ChatType.TeamBroadcast, DelegateFactory.Action_MiniChatCallBackInfo(function (data) 
                CTeamMatchMgr.Inst:BroadcastAddTeamRequestMsg(0, data.msg, CTeamMatchMgr.Inst.selectedActivityId, CTeamMatchMgr.Inst.IncludeNewbie)
            end))
        end), false, nil))
    end
    CPopupMenuInfoMgr.ShowPopupMenu(CommonDefs.ListToArray(rows), this.callBtn.transform, CPopupMenuInfoMgr.AlignType.Bottom)
end
CTeamInfoTabContent.m_OnInviteButtonClick_CS2LuaHook = function (this, go) 
    --邀请按钮点击事件响应
    --邀请按钮点击事件响应
    local rows = CreateFromClass(MakeGenericClass(List, PopupMenuItemData))
    CommonDefs.ListAdd(rows, typeof(PopupMenuItemData), PopupMenuItemData(LocalString.GetString("好友"), DelegateFactory.Action_int(function (row) 
        CTeamMgr.Inst:RequestFriendsNotInTeam(0)
    end), false, nil))
    CommonDefs.ListAdd(rows, typeof(PopupMenuItemData), PopupMenuItemData(LocalString.GetString("附近的人"), DelegateFactory.Action_int(function (row) 
        CTeamMgr.Inst:RequestGetPlayersNearby(0)
    end), false, nil))
    CPopupMenuInfoMgr.ShowPopupMenu(CommonDefs.ListToArray(rows), this.inviteBtn.transform, CPopupMenuInfoMgr.AlignType.Bottom)
end
CTeamInfoTabContent.m_OnLeaveButtonClick_CS2LuaHook = function (this, go)
    local str = ""
    if CTeamGroupMgr.Instance:InTeamGroup() then
        str = g_MessageMgr:FormatMessage("Leave_Team_And_Group_Confirm")
    else
        str = g_MessageMgr:FormatMessage("Leave_Team_Confirm")
    end

    MessageWndManager.ShowOKCancelMessage(str, DelegateFactory.Action(function () 
        --离开队伍按钮点击事件响应
        CTeamMgr.Inst:RequestLeaveTeam()
    end), nil, nil, nil, false)
end
CTeamInfoTabContent.m_OnLeaderFollowButtonClick_CS2LuaHook = function (this, go) 
    --我是队长
    --我是队长
    if CTeamMgr.Inst:MainPlayerIsTeamLeader() then
        CTeamFollowMgr.Inst:RequestSummonTeammate()
    end
end
CTeamInfoTabContent.m_OnCancleFollowButtonClick_CS2LuaHook = function (this, go) 
    if CTeamMgr.Inst:MainPlayerIsTeamLeader() then
        CTeamFollowMgr.Inst:RequestCancelTeamFollow()
    else
        --队员跟随中，则取消跟随
        if CTeamFollowMgr.Inst:IsMainPlayerLeadingTeamOrTeamFollowing() then
            CTeamFollowMgr.Inst:RequestCancelTeamFollow()
        else
            CTeamFollowMgr.Inst:RequestTeamFollowOnNotFollowing()
        end
    end
end
CTeamInfoTabContent.m_RefreshCancelFollowButtonState_CS2LuaHook = function (this) 
    if CTeamMgr.Inst:MainPlayerIsTeamLeader() then
        local follow = CTeamFollowMgr.Inst:IsMainPlayerLeadingTeamOrTeamFollowing()
        if follow then
            CUICommonDef.SetActive(this.cancleFollowBtn, true, true)
        else
            CUICommonDef.SetActive(this.cancleFollowBtn, false, true)
        end
    else
        CUICommonDef.SetActive(this.cancleFollowBtn, true, true)
    end

    if CTeamMgr.Inst:MainPlayerIsTeamLeader() then
        this.followLabel.text = LocalString.GetString("取消跟随")
    else
        if CTeamFollowMgr.Inst:IsMainPlayerLeadingTeamOrTeamFollowing() then
            this.followLabel.text = LocalString.GetString("取消跟随")
        else
            this.followLabel.text = LocalString.GetString("组队跟随")
        end
    end
end
CTeamInfoTabContent.m_LoadMembersData_CS2LuaHook = function (this, members) 

    local teamBg = CTeamMgr.Inst:GetSpecialTeamBg()
    this.specialBg:LoadMaterial(teamBg)
    this.specialBg.gameObject:SetActive(true)

    CommonDefs.ListClear(this.memberItems)
    Extensions.RemoveAllChildren(this.grid.transform)

    local teamMemberList = CreateFromClass(MakeGenericClass(List, CTeamMember))
    CommonDefs.ListAddRange(teamMemberList, members)

    for i = 0, 4 do
        local instance = CUICommonDef.AddChild(this.grid.gameObject, this.itemTemplate)
        instance:SetActive(true)
        local item = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CTeamMemberItem))
        if teamMemberList.Count > i then
            item:Init(teamMemberList[i], true)
        else
            item:Init(nil, true)
        end
        item.clickCallback = MakeDelegateFromCSFunction(this.OnMemberItemClick, MakeGenericClass(Action1, CTeamMemberItem), this)
        instance:SetActive(true)
        CommonDefs.ListAdd(this.memberItems, typeof(CTeamMemberItem), item)
    end

    this.grid:Reposition()
    this.scrollView:ResetPosition()
end
CTeamInfoTabContent.m_Refresh_CS2LuaHook = function (this) 
    this:RefreshCancelFollowButtonState()

    local hasTeam = CTeamMgr.Inst:TeamExists()
    this.createBtn:SetActive(not hasTeam)
    this.quickJoinTeamBtn:SetActive(not hasTeam)
    this.leaveBtn:SetActive(hasTeam)
    this.leaderFollowBtn:SetActive(hasTeam)
    this.cancleFollowBtn:SetActive(hasTeam)
    --解散队伍按钮

    if CTeamMgr.Inst:MainPlayerIsTeamLeader() then
        --      departBtn.SetActive(true);
        this.leaderFollowBtn:SetActive(true)
        --     moreOptBtn.SetActive(true);
    else
        --     departBtn.SetActive(false);
        this.leaderFollowBtn:SetActive(false)
        --    moreOptBtn.SetActive(false);
    end
end
