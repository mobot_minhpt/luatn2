local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local UISlider = import "UISlider"
local QnTableView = import "L10.UI.QnTableView"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local UILongPressButton = import "L10.UI.UILongPressButton"
local CItemCountUpdate = import "L10.UI.CItemCountUpdate"
local CUITexture = import "L10.UI.CUITexture"
local UISprite = import "UISprite"
local CItemMgr = import "L10.Game.CItemMgr"
local AlignType1 = import "L10.UI.CTooltip+AlignType"
local CItemAccessListMgr=import "L10.UI.CItemAccessListMgr"

LuaStoreRoomConstructWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaStoreRoomConstructWnd, "IncreseAndDecreaseButton", "IncreseAndDecreaseButton", QnAddSubAndInputButton)
RegistChildComponent(LuaStoreRoomConstructWnd, "SelectedItem", "SelectedItem", GameObject)
RegistChildComponent(LuaStoreRoomConstructWnd, "TipButton", "TipButton", GameObject)
RegistChildComponent(LuaStoreRoomConstructWnd, "SubmitButton", "SubmitButton", GameObject)
RegistChildComponent(LuaStoreRoomConstructWnd, "BindLabel", "BindLabel", UILabel)
RegistChildComponent(LuaStoreRoomConstructWnd, "NotBindLabel", "NotBindLabel", UILabel)
RegistChildComponent(LuaStoreRoomConstructWnd, "Slider", "Slider", UISlider)
RegistChildComponent(LuaStoreRoomConstructWnd, "TableView", "TableView", QnTableView)
RegistChildComponent(LuaStoreRoomConstructWnd, "DescLabel", "DescLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaStoreRoomConstructWnd, "m_TaskItems")
RegistClassMember(LuaStoreRoomConstructWnd, "m_CurSelectedItemId")
RegistClassMember(LuaStoreRoomConstructWnd, "m_CurValuePerItem")
RegistClassMember(LuaStoreRoomConstructWnd, "m_SubmitCount")
RegistClassMember(LuaStoreRoomConstructWnd, "m_CurProgress")
RegistClassMember(LuaStoreRoomConstructWnd, "m_MaxTaskProcess")

function LuaStoreRoomConstructWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)


	
	UIEventListener.Get(self.SubmitButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSubmitButtonClick()
	end)


    --@endregion EventBind end
	self:InitClassMember()
end

function LuaStoreRoomConstructWnd:Init()
	self:InitValues()
	self:InitProgress()

	self.IncreseAndDecreaseButton.onValueChanged = DelegateFactory.Action_uint(function (value) 
        self:OnNumChanged(value)
    end)

	self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_TaskItems
        end,
        function(item,row)
            self:InitItem(item,row)
        end)

	self.TableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
		self:OnSelectAtRow(row)
	end)
	self.TableView:ReloadData(false,false)
	self.TableView:SetSelectRow(0,true)
	CUIManager.CloseUI(CUIResources.ItemAccessListWnd)
end

function LuaStoreRoomConstructWnd:InitClassMember()
	self.m_IconTexture = self.SelectedItem.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
	self.m_NameLabel = self.SelectedItem.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
	self.m_QualitySprite = self.SelectedItem.transform:Find("QualitySprite"):GetComponent(typeof(UISprite))
	self.m_BindSprite = self.SelectedItem.transform:Find("BindSprite"):GetComponent(typeof(UISprite))
end

function LuaStoreRoomConstructWnd:InitValues()
	self.m_CurSelectedItemId = nil
	self.m_CurValuePerItem = 0
	self.m_SubmitCount = 0
	self.m_CurProgress = 0
	self.m_MaxTaskProcess = StoreRoom_Setting.GetData().MaxTaskProcess

	self.m_TaskItems = {}
	StoreRoom_TaskItem.Foreach(function(k,v)
		local t = {}
		t.templateId = v.ItemTemplateId
		t.value = v.Value
		table.insert(self.m_TaskItems,t)
	end)
end

function LuaStoreRoomConstructWnd:InitItem(item,row)
	local tf = item.transform
    local go = item.gameObject

	local info = self.m_TaskItems[row+1]
    local id = info.templateId

    FindChild(tf,"BindSprite").gameObject:SetActive(false)
    local icon=FindChild(tf,"Icon"):GetComponent(typeof(CUITexture))
    local data = Item_Item.GetData(id)
    if data then
        icon:LoadMaterial(data.Icon)
    end
    local mask=FindChild(go.transform,"GetNode").gameObject
    local countUpdate=go:GetComponent(typeof(CItemCountUpdate))
    countUpdate.templateId=id
    
    local function OnCountChange(val)
        if val>0 then
            mask:SetActive(false)
            countUpdate.format="{0}"
        else
            mask:SetActive(true)
            countUpdate.format="[ff0000]{0}[-]"
        end
        if id==self.m_CurSelectedItemId then
            self:InitSelectedItemCount()
        end
    end
    countUpdate.onChange=DelegateFactory.Action_int(OnCountChange)
    countUpdate:UpdateCount()

    local longPressCmp = go:GetComponent(typeof(UILongPressButton))
    longPressCmp.OnLongPressDelegate = DelegateFactory.Action(function()
        CItemInfoMgr.ShowLinkItemTemplateInfo(id, false, nil, CItemInfoMgr.AlignType.Default, 0, 0, 0, 0)
    end)
end

function LuaStoreRoomConstructWnd:OnSelectAtRow(row)
	local info = self.m_TaskItems[row+1]
    local id = info.templateId
	local value = info.value

	self.m_CurSelectedItemId = id
	self.m_CurValuePerItem = value

	local data = Item_Item.GetData(id)
	if data then
		self.m_IconTexture:LoadMaterial(data.Icon)
		self.m_NameLabel.text = data.Name
		self.m_BindSprite.gameObject:SetActive(false)
		self.m_QualitySprite.spriteName = CUICommonDef.GetItemCellBorder(data.NameColor)
		self:InitSelectedItemCount()
	end

	local item = self.TableView:GetItemAtRow(row)
	local countUpdate = item.gameObject:GetComponent(typeof(CItemCountUpdate))
	if countUpdate.count<=0 then
		CItemAccessListMgr.Inst:ShowItemAccessInfo(self.m_CurSelectedItemId, true, nil, AlignType1.Right)
	end
	
end

function LuaStoreRoomConstructWnd:InitSelectedItemCount()
	if not self.m_CurSelectedItemId then
		return
	end
	local bindCount,notBindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(self.m_CurSelectedItemId)
    self.BindLabel.text = LocalString.GetString("绑定")..SafeStringFormat3("[c][ffffff]x%d[-][/c]",bindCount)
    self.NotBindLabel.text = LocalString.GetString("非绑定")..SafeStringFormat3("[c][ffffff]x%d[-][/c]",notBindCount)

    local count = bindCount+notBindCount
    local maxCount = self.m_CurValuePerItem ~= 0 and ((self.m_MaxTaskProcess - self.m_CurProgress) / self.m_CurValuePerItem) or count
    --可超额提交 不足1个的按照一个计算
	maxCount = math.ceil(maxCount)
	if count>0 then
        self.IncreseAndDecreaseButton:SetMinMax(1,math.min(count,maxCount),1)
        self.IncreseAndDecreaseButton:SetValue(1,true)
    else
        self.IncreseAndDecreaseButton:SetMinMax(0,0,1)
        self.IncreseAndDecreaseButton:SetValue(0,true)
    end
end

function LuaStoreRoomConstructWnd:OnNumChanged(value)
	local progressAdd = value * self.m_CurValuePerItem
    self.DescLabel.text = SafeStringFormat3(LocalString.GetString("本次提交可增加[c][00ffff]%s[-][/c]进度"),progressAdd)
	self.m_SubmitCount = value
end

function LuaStoreRoomConstructWnd:InitProgress()
	if CommonDefs.DictContains(CClientMainPlayer.Inst.PlayProp.PersistPlayData, typeof(UInt32), EnumPersistPlayDataKey_lua.eStoreRoomTaskProgress) then
		local data = CClientMainPlayer.Inst.PlayProp.PersistPlayData[EnumPersistPlayDataKey_lua.eStoreRoomTaskProgress].Data
		local p = data and MsgPackImpl.unpack(data) or 0

		if p and p>=0 then
			self.m_CurProgress = p
		end
	end
	--init slider
	self.Slider.value = self.m_CurProgress / self.m_MaxTaskProcess
	local ProgressLabel = self.Slider.transform:Find("ProgressLabel"):GetComponent(typeof(UILabel))
	ProgressLabel.text = SafeStringFormat3("%d/%d",self.m_CurProgress , self.m_MaxTaskProcess)
	self:InitSelectedItemCount()
end

function LuaStoreRoomConstructWnd:OnUpdatePersistPlayDataWithKey()
	--完成建设关闭界面
	if CommonDefs.DictContains(CClientMainPlayer.Inst.PlayProp.PersistPlayData, typeof(UInt32), EnumPersistPlayDataKey_lua.eStoreRoomTaskProgress) then
		local data = CClientMainPlayer.Inst.PlayProp.PersistPlayData[EnumPersistPlayDataKey_lua.eStoreRoomTaskProgress].Data
		local p = data and MsgPackImpl.unpack(data) or 0

		if p and p>=0 then
			self.m_CurProgress = p
		end
		if self.m_CurProgress >= self.m_MaxTaskProcess then
			CUIManager.CloseUI(CLuaUIResources.StoreRoomConstructWnd)
			return
		end
	end
	--init slider
	self.Slider.value = self.m_CurProgress / self.m_MaxTaskProcess
	local ProgressLabel = self.Slider.transform:Find("ProgressLabel"):GetComponent(typeof(UILabel))
	ProgressLabel.text = SafeStringFormat3("%d/%d",self.m_CurProgress , self.m_MaxTaskProcess)
	self:InitSelectedItemCount()
end

function LuaStoreRoomConstructWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdatePersistPlayDataWithKey", self, "OnUpdatePersistPlayDataWithKey")
end

function LuaStoreRoomConstructWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdatePersistPlayDataWithKey", self, "OnUpdatePersistPlayDataWithKey")
end
--@region UIEvent

function LuaStoreRoomConstructWnd:OnTipButtonClick()
	g_MessageMgr:ShowMessage("StoreRoom_Construct_Rule")
end

function LuaStoreRoomConstructWnd:OnSubmitButtonClick()
	if not self.m_CurSelectedItemId or self.m_SubmitCount <= 0 then
		return
	end
	Gac2Gas.RequestSubmitStoreRoomTaskItem(self.m_CurSelectedItemId, self.m_SubmitCount)
end

--@endregion UIEvent

