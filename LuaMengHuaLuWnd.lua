require("common/common_include")

local CUIFx = import "L10.UI.CUIFx"
local LuaTweenUtils = import "LuaTweenUtils"
local Vector3 = import "UnityEngine.Vector3"
local CBabyModelTextureLoader = import "L10.UI.CBabyModelTextureLoader"
local UIGrid = import "UIGrid"
local MengHuaLu_Setting = import "L10.Game.MengHuaLu_Setting"
local UITable = import "UITable"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local MengHuaLu_QiChang = import "L10.Game.MengHuaLu_QiChang"
local MengHuaLu_Buff = import "L10.Game.MengHuaLu_Buff"
local MengHuaLu_Skill = import "L10.Game.MengHuaLu_Skill"
local CUITexture = import "L10.UI.CUITexture"
local CPortraitSocialWndMgr = import "L10.UI.CPortraitSocialWndMgr"
local LuaUtils = import "LuaUtils"
local QnTableView = import "L10.UI.QnTableView"
local PlayerSettings = import "L10.Game.PlayerSettings"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local Time = import "UnityEngine.Time"
local Extensions = import "Extensions"
local MessageMgr = import "L10.Game.MessageMgr"
local QnButton = import "L10.UI.QnButton"
local Baby_QiChang = import "L10.Game.Baby_QiChang"
local CEffectMgr = import "L10.Game.CEffectMgr"
local CPos = import "L10.Engine.CPos"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local Ease = import "DG.Tweening.Ease"

LuaMengHuaLuWnd = class()

RegistChildComponent(LuaMengHuaLuWnd, "Fx", CUIFx) -- 用于给策划测试UI特效

-- 气场
RegistChildComponent(LuaMengHuaLuWnd, "QiChangGrid", UIGrid)
RegistChildComponent(LuaMengHuaLuWnd, "QiChangItem", GameObject)

-- 宝宝数据
RegistChildComponent(LuaMengHuaLuWnd, "BabyTexture", CBabyModelTextureLoader)
RegistChildComponent(LuaMengHuaLuWnd, "BabyTalk", UILabel)
RegistChildComponent(LuaMengHuaLuWnd, "BabyAttackValueLabel", UILabel)
RegistChildComponent(LuaMengHuaLuWnd, "BabyHPValueLabel", UILabel)
RegistChildComponent(LuaMengHuaLuWnd, "BabyMPValueLabel", UILabel)
RegistChildComponent(LuaMengHuaLuWnd, "BabyMoneyValueLabel", UILabel)
RegistChildComponent(LuaMengHuaLuWnd, "BabyFx", CUIFx)
RegistChildComponent(LuaMengHuaLuWnd, "BabyDamageNotice", CCommonLuaScript)

-- 宝宝buff
RegistChildComponent(LuaMengHuaLuWnd, "BuffGrid", UIGrid)
RegistChildComponent(LuaMengHuaLuWnd, "BuffItem", GameObject)

-- 迷宫数据
RegistChildComponent(LuaMengHuaLuWnd, "MazeTable", UITable)
RegistChildComponent(LuaMengHuaLuWnd, "MazeItem", GameObject)

-- 右边数据
RegistChildComponent(LuaMengHuaLuWnd, "MoreInfo", GameObject)
RegistChildComponent(LuaMengHuaLuWnd, "ChatBtn", GameObject)
RegistChildComponent(LuaMengHuaLuWnd, "SkillBtn", GameObject)
RegistChildComponent(LuaMengHuaLuWnd, "SkillCountLabel", UILabel)
RegistChildComponent(LuaMengHuaLuWnd, "MengJingTitleLabel", UILabel)
RegistChildComponent(LuaMengHuaLuWnd, "RoundLabel", UILabel)

-- 技能数据
RegistChildComponent(LuaMengHuaLuWnd, "QiChang", GameObject)
RegistChildComponent(LuaMengHuaLuWnd, "LeftBottomView", GameObject)
RegistChildComponent(LuaMengHuaLuWnd, "SkillView", GameObject)
RegistChildComponent(LuaMengHuaLuWnd, "SkillHintLabel", GameObject)
RegistChildComponent(LuaMengHuaLuWnd, "SkillTableView", QnTableView)
RegistChildComponent(LuaMengHuaLuWnd, "SkillMask", GameObject)

RegistChildComponent(LuaMengHuaLuWnd, "FaliCostLabel", UILabel)
RegistChildComponent(LuaMengHuaLuWnd, "FaliOwnLabel", UILabel)
RegistChildComponent(LuaMengHuaLuWnd, "FaliAddBtn", GameObject)
RegistChildComponent(LuaMengHuaLuWnd, "UseSkillBtn", QnButton)
RegistChildComponent(LuaMengHuaLuWnd, "CancelSkillBtn", QnButton)
RegistChildComponent(LuaMengHuaLuWnd, "HideSkillBtn", GameObject)

RegistChildComponent(LuaMengHuaLuWnd, "AttackIcon", GameObject)
RegistChildComponent(LuaMengHuaLuWnd, "HPIcon", GameObject)
RegistChildComponent(LuaMengHuaLuWnd, "MPIcon", GameObject)
RegistChildComponent(LuaMengHuaLuWnd, "MoneyIcon", GameObject)
RegistChildComponent(LuaMengHuaLuWnd, "SkillIcon", GameObject)


RegistClassMember(LuaMengHuaLuWnd, "SelectedBaby")
RegistClassMember(LuaMengHuaLuWnd, "ActionExecuter")

-- 技能界面打开相关参数
RegistClassMember(LuaMengHuaLuWnd, "SkillShowX")
RegistClassMember(LuaMengHuaLuWnd, "SkillHideX")
RegistClassMember(LuaMengHuaLuWnd, "QiChangShowY")
RegistClassMember(LuaMengHuaLuWnd, "QiChangHideY")
RegistClassMember(LuaMengHuaLuWnd, "BabyShowY")
RegistClassMember(LuaMengHuaLuWnd, "BabyHideY")
RegistClassMember(LuaMengHuaLuWnd, "JiNengInterval")
RegistClassMember(LuaMengHuaLuWnd, "LastJiNengTime")

-- 技能数据
RegistClassMember(LuaMengHuaLuWnd, "TableViewDataSource")
RegistClassMember(LuaMengHuaLuWnd, "SkillInfos")
RegistClassMember(LuaMengHuaLuWnd, "SelectedSkillIndex")

-- 执行动画相关
RegistClassMember(LuaMengHuaLuWnd, "StartTime")
RegistClassMember(LuaMengHuaLuWnd, "IntervalTime")
RegistClassMember(LuaMengHuaLuWnd, "ActionQueue")
RegistClassMember(LuaMengHuaLuWnd, "TweenList")
-- 震屏相关
RegistClassMember(LuaMengHuaLuWnd, "ShakeFx")

-- 存储的数据
RegistClassMember(LuaMengHuaLuWnd, "BabyInfo")


function LuaMengHuaLuWnd:Init()
	self:InitClassMembers()
	self:InitValues()
end

function LuaMengHuaLuWnd:InitClassMembers()
	self.Fx:DestroyFx()
end

function LuaMengHuaLuWnd:InitValues()

	self.QiChangItem:SetActive(false)
	self.BuffItem:SetActive(false)
	self.SkillInfos = {}
	self.BabyTalk.alpha = 0
	self.SelectedSkillIndex = -1

	self.SkillShowX = -710
	self.SkillHideX = -1225
	self.QiChangShowY = 0
	self.QiChangHideY = 380
	self.BabyShowY = 0
	self.BabyHideY = -730
	self.StartTime = Time.realtimeSinceStartup
	self.IntervalTime = 0.8
	self.ActionQueue = {}
	self.ShakeFx = nil
	self.JiNengInterval = 0.3
	self.LastJiNengTime = 0
	self.TweenList = {}


	local onChatBtnClicked = function (go)
		self:OnChatBtnClicked(go)
	end
	CommonDefs.AddOnClickListener(self.ChatBtn, DelegateFactory.Action_GameObject(onChatBtnClicked), false)

	local onSkillBtnClicked = function (go)
		self:OnSkillBtnClicked(go)
	end
	CommonDefs.AddOnClickListener(self.SkillBtn, DelegateFactory.Action_GameObject(onSkillBtnClicked), false)

	local onMoreInfoClicked = function (go)
		self:OnMoreInfoClicked(go)
	end
	CommonDefs.AddOnClickListener(self.MoreInfo, DelegateFactory.Action_GameObject(onMoreInfoClicked), false)

	local onFaliAddBtnClicked = function (go)
		self:OnFaliAddBtnClicked(go)
	end
	CommonDefs.AddOnClickListener(self.FaliAddBtn, DelegateFactory.Action_GameObject(onFaliAddBtnClicked), false)

	local onUseSkillBtnClicked = function (go)
		self:OnUseSkillBtnClicked(go)
	end
	CommonDefs.AddOnClickListener(self.UseSkillBtn.gameObject, DelegateFactory.Action_GameObject(onUseSkillBtnClicked), false)

	local onCancelSkillBtnClicked = function (go)
		self:OnCancelSkillBtnClicked(go)
	end
	CommonDefs.AddOnClickListener(self.CancelSkillBtn.gameObject, DelegateFactory.Action_GameObject(onCancelSkillBtnClicked), false)

	local onHideSkillBtnClicked = function (go)
		self:OnHideSkillBtnClicked(go)
	end
	CommonDefs.AddOnClickListener(self.HideSkillBtn, DelegateFactory.Action_GameObject(onHideSkillBtnClicked), false)

	self:InitMazeGrid()
	self.BabyDamageNotice:Init({})
	self:HideSkill(false)
	PlayerSettings.MengHuaLuSkillExpanded = false

	local initItem = function (item, index)
		self:InitSkillItem(item, index)
	end
	self.TableViewDataSource = DefaultTableViewDataSource.Create(
		function ()
			return #self.SkillInfos
		end, initItem)
	self.SkillTableView.m_DataSource=self.TableViewDataSource
	self.SkillTableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        self:OnSelectAtRow(row)
    end)
    self.SkillTableView:ReloadData(true,false)

    -- 加载完成
    Gac2Gas.RequestStartMengHuaLu()
end

-- 初始化格子
function LuaMengHuaLuWnd:InitMazeGrid()

	CUICommonDef.ClearTransform(self.MazeTable.transform)

	local setting = MengHuaLu_Setting.GetData()

	for i = 1, setting.GridCount do
		local go = NGUITools.AddChild(self.MazeTable.gameObject, self.MazeItem)
		go:SetActive(true)
		go.name = tostring(i)
		local luaScript = go.transform:GetComponent(typeof(CCommonLuaScript))
		if luaScript then
			luaScript.m_LuaSelf:Init(false, i)
		else
		end
	end
	self.MazeItem:SetActive(false)
	self.MazeTable:Reposition()

end

-- 打开聊天界面
function LuaMengHuaLuWnd:OnChatBtnClicked()
	-- 打开聊天界面
	if CPortraitSocialWndMgr.Inst:IsPortraitSocialWndOpened() then
		CPortraitSocialWndMgr.Inst:ClosePortraitSocialWnd()
	end

	-- pc版本
	local bIsWinSocialWndOpened = false
	if CommonDefs.IsPCGameMode() then
		local CWinSocialWndMgr = import "L10.UI.CWinSocialWndMgr"
		bIsWinSocialWndOpened = CWinSocialWndMgr.Inst.m_IsWinSocialWndOpened
	end

	if not bIsWinSocialWndOpened then
		CUIManager.ShowUI(CUIResources.SocialWnd)
	end

end

-- 打开提示
function LuaMengHuaLuWnd:OnMoreInfoClicked(go)
	MessageMgr.Inst:ShowMessage("MENGHUALU_MORE_INFO", {})
end


---------------------- 气场 -----------------------
function LuaMengHuaLuWnd:UpdateQiChang()

	CUICommonDef.ClearTransform(self.QiChangGrid.transform)
	if not self.BabyInfo then return end

	local setting = MengHuaLu_Setting.GetData()
	local activeQiChangList = self.BabyInfo.AvtiveQiChangList

	for i = 1, setting.MaxActiveQiChangNum do
		local go = NGUITools.AddChild(self.QiChangGrid.gameObject, self.QiChangItem)
		local qichangId = 0
		if activeQiChangList.Count >= i then
			qichangId = activeQiChangList[i-1]
		end
		self:InitQiChangItem(go, qichangId)
		go:SetActive(true)
	end
	self.QiChangGrid:Reposition()
end

function LuaMengHuaLuWnd:InitQiChangItem(go, qichangId)
	local QiChangLabel = go.transform:Find("QiChangLabel"):GetComponent(typeof(UILabel))
	local AddSprite = go.transform:Find("AddSprite").gameObject

	local qichang = MengHuaLu_QiChang.GetData(qichangId)
	if qichang then
		QiChangLabel.gameObject:SetActive(true)
		AddSprite:SetActive(false)
		QiChangLabel.text = self:GetQiChangName(qichang)
	else
		QiChangLabel.gameObject:SetActive(false)
		AddSprite:SetActive(true)
		QiChangLabel.text = nil
	end

	local onAddClicked = function (gameObject)
		CUIManager.ShowUI(CLuaUIResources.MengHuaLuQiChangWnd)
	end
	CommonDefs.AddOnClickListener(QiChangLabel.gameObject, DelegateFactory.Action_GameObject(onAddClicked), false)
	CommonDefs.AddOnClickListener(AddSprite, DelegateFactory.Action_GameObject(onAddClicked), false)
end

function LuaMengHuaLuWnd:GetQiChangName(qichang)
	local name = qichang.MaleName
	if self.BabyInfo.AppearanceData[1] == 1 then
		name = qichang.FemaleName
	end

	local babyQiChang = Baby_QiChang.GetData(qichang.QiChangID)
	return SafeStringFormat3("[%s]%s[-]", LuaBabyMgr.GetQiChangNameColor(babyQiChang.Quality-1), name)
end

-- 设置气场成功后的更新
function LuaMengHuaLuWnd:MengHuaLuSetQiChangSuccess(list)
	self.BabyInfo.AvtiveQiChangList = list
	self:UpdateQiChang()
end

---------------------- 气场 end -----------------------


---------------------- buff -----------------------
function LuaMengHuaLuWnd:UpdateBabyBuffs()
	CUICommonDef.ClearTransform(self.BuffGrid.transform)

	for i = 0, self.BabyInfo.BuffDataList.Count-1 do
		local buffInfo = self.BabyInfo.BuffDataList[i]
		local go = NGUITools.AddChild(self.BuffGrid.gameObject, self.BuffItem)
		local buffId = buffInfo[0]
		local lastRound = buffInfo[1]
		self:InitBuffItem(go, {
            buffId = buffId,
            lastRound = lastRound,
        })
        go:SetActive(true)
	end
	self.BuffGrid:Reposition()
end

function LuaMengHuaLuWnd:InitBuffItem(go, info)
	local Icon = go.transform:Find("Icon"):GetComponent(typeof(CUITexture))
	local ActionRound = go.transform:Find("ActionRound"):GetComponent(typeof(UILabel))

	local buff = MengHuaLu_Buff.GetData(info.buffId)
	Icon:LoadMaterial(buff.Icon)
	ActionRound.gameObject:SetActive(info.lastRound > 0)
	if info.lastRound >= 0 then
		ActionRound.text = tostring(info.lastRound)
	else
		ActionRound.text = nil
	end

	local onBuffClicked = function (go)
		LuaMengHuaLuMgr.ShowBuffList(self.BabyInfo.BuffDataList)
	end
	CommonDefs.AddOnClickListener(go, DelegateFactory.Action_GameObject(onBuffClicked), false)
end

-- 更新buff
function LuaMengHuaLuWnd:UpdateBuffs(buffs)
	self.BabyInfo.BuffDataList = buffs
	self:UpdateBabyBuffs()
end

-- 进入新的一轮，buff轮次减1
function LuaMengHuaLuWnd:UpdateBuffRound()
	for i = 0, self.BabyInfo.BuffDataList.Count-1 do
		local buffInfo = self.BabyInfo.BuffDataList[i]
		local lastRound = buffInfo[1]
		if lastRound > 0 then
			lastRound = lastRound - 1
		end
		self.BabyInfo.BuffDataList[i][1] = lastRound
	end
	self:UpdateBabyBuffs()
end


---------------------- buff end -----------------------

---------------------- 技能显示相关 -----------------------

function LuaMengHuaLuWnd:OnSkillBtnClicked(go)
	if Time.realtimeSinceStartup - self.LastJiNengTime > self.JiNengInterval then
		self.LastJiNengTime = Time.realtimeSinceStartup
		PlayerSettings.MengHuaLuSkillExpanded = not PlayerSettings.MengHuaLuSkillExpanded
		if PlayerSettings.MengHuaLuSkillExpanded then
			self:ShowSkill(true)
			self:HideQiChang(true)
			self:HideBaby(true)
		else
			self:HideSkill(true)
			self:ShowQiChang(true)
			self:ShowBaby(true)
		end
	end
end

function LuaMengHuaLuWnd:ShowSkill(animated)
	-- 请求已有技能
	Gac2Gas.QueryMengHuaLuSkill()
	if animated then
		LuaTweenUtils.DOLocalMoveX(self.SkillView.transform, self.SkillShowX, 0.2, false)
		self.SkillView.gameObject:SetActive(true)
    else
        Extensions.SetLocalPositionX(self.SkillView.transform, self.SkillShowX)
        self.SkillView.gameObject:SetActive(true)
    end
end

function LuaMengHuaLuWnd:HideSkill(animated)
	if animated then
        local tween = LuaTweenUtils.DOLocalMoveX(self.SkillView.transform, self.SkillHideX, 0.2, false)
        LuaTweenUtils.OnComplete(tween, function ()
        	self.SkillView.gameObject:SetActive(false)
        end)
    else
        Extensions.SetLocalPositionX(self.SkillView.transform, self.SkillHideX)
        self.SkillView.gameObject:SetActive(false)
    end
end

function LuaMengHuaLuWnd:ShowQiChang(animated)
	if animated then
		LuaTweenUtils.DOLocalMoveY(self.QiChang.transform, self.QiChangShowY, 0.2, false)
		self.QiChang.gameObject:SetActive(true)
    else
        Extensions.SetLocalPositionY(self.QiChang.transform, self.QiChangShowY)
        LuaTweenUtils.TweenAlpha(self.QiChang.transform, 0, 1, 0)
        self.QiChang.gameObject:SetActive(true)
    end
end

function LuaMengHuaLuWnd:HideQiChang(animated)
	if animated then
		local tween = LuaTweenUtils.DOLocalMoveY(self.QiChang.transform, self.QiChangHideY, 0.2, false)
		LuaTweenUtils.OnComplete(tween, function ()
        	self.QiChang.gameObject:SetActive(false)
        end)
    else
        Extensions.SetLocalPositionY(self.QiChang.transform, self.QiChangHideY)
        self.QiChang.gameObject:SetActive(false)
    end
end

function LuaMengHuaLuWnd:ShowBaby(animated)
	if animated then
		LuaTweenUtils.DOLocalMoveY(self.LeftBottomView.transform, self.BabyShowY, 0.2, false)
		self.BabyFx:DestroyFx()
		self.LeftBottomView.gameObject:SetActive(true)
    else
        Extensions.SetLocalPositionY(self.LeftBottomView.transform, self.BabyShowY)
        self.BabyFx:DestroyFx()
        self.LeftBottomView.gameObject:SetActive(true)
    end
end

function LuaMengHuaLuWnd:HideBaby(animated)
	if animated then
		local tween = LuaTweenUtils.DOLocalMoveY(self.LeftBottomView.transform, self.BabyHideY, 0.2, false)
		LuaTweenUtils.OnComplete(tween, function ()
			self.LeftBottomView.gameObject:SetActive(false)
		end)
    else
        Extensions.SetLocalPositionY(self.LeftBottomView.transform, self.BabyHideY)
        self.LeftBottomView.gameObject:SetActive(false)
    end
end

function LuaMengHuaLuWnd:MengHuaLuHideSkill()
	self:HideSkill(false)
	self:ShowBaby(false)
	self:ShowQiChang(false)
end

function LuaMengHuaLuWnd:OnFaliAddBtnClicked(go)
	MessageMgr.Inst:ShowMessage("MENGHUALU_ADD_FALI", {})
end

function LuaMengHuaLuWnd:UpdateSkillCount()
	self.SkillCountLabel.text = tostring(self.BabyInfo.Skills)
end

function LuaMengHuaLuWnd:SyncMengHuaLuSkillNum(count)
	local current = CommonDefs.Convert_ToUInt32(self.SkillCountLabel.text)
	if count > current then
		self:ShowPropChange(self.SkillIcon)
	end
	self.SkillCountLabel.text = tostring(count)
	
	-- 如果打开技能界面，重新请求一下技能
	if PlayerSettings.MengHuaLuSkillExpanded then
		Gac2Gas.QueryMengHuaLuSkill()
	end
end

---------------------- 技能显示相关 end -----------------------

---------------------- 技能释放相关 -----------------------

function LuaMengHuaLuWnd:OnSelectAtRow(row)
	self.SelectedSkillIndex = row
	self:UpdateSkillFaliCost()
	local info = self.SkillInfos[self.SelectedSkillIndex+1]
	LuaMengHuaLuMgr.ShowSkillTip(info)
end

function LuaMengHuaLuWnd:UpdateSkillFaliCost()
	local info = self.SkillInfos[self.SelectedSkillIndex+1]
	if not info then return end
	self.FaliCostLabel.text = tostring(info.comsume)
	self.UseSkillBtn.Enabled = info.comsume <= self.BabyInfo.Mp
end

function LuaMengHuaLuWnd:InitSkillItem(item, index)
	local info = self.SkillInfos[index+1]
	local Icon = item.transform:Find("Icon"):GetComponent(typeof(CUITexture))
	local SkillNameLabel = item.transform:Find("SkillNameLabel"):GetComponent(typeof(UILabel))
	local MPConsume = item.transform:Find("MPConsume"):GetComponent(typeof(UILabel))
	local NumLabel = item.transform:Find("NumLabel"):GetComponent(typeof(UILabel))

	local skill = MengHuaLu_Skill.GetData(info.skillId)
	Icon:Clear()
	Icon:LoadMaterial(skill.Icon)
	SkillNameLabel.text = skill.Name
	MPConsume.text = info.comsume
	NumLabel.text = info.num
end

function LuaMengHuaLuWnd:UpdateSkills(skillList)
	self.SkillInfos = skillList
	--self.TableViewDataSource.count=#self.SkillInfos
	self.SelectedSkillIndex = -1
    self.SkillTableView:ReloadData(true,false)
    self.SkillHintLabel:SetActive(#self.SkillInfos == 0)

end

function LuaMengHuaLuWnd:OnUseSkillBtnClicked(go)
	if self.SelectedSkillIndex == -1 then
		MessageMgr.Inst:ShowMessage("MENGHUALU_SELECT_SKILL_FIRST", {})
		return
	end
	LuaMengHuaLuMgr.CloseSkillTip()
	local info = self.SkillInfos[self.SelectedSkillIndex+1]
	if not info then return end
	local skill = MengHuaLu_Skill.GetData(info.skillId)
	if skill.TargetType == "other" then -- 需要选择对象
		-- 展示黑屏
		if not LuaMengHuaLuMgr.HasSkillTarget(info.skillId) then
			MessageMgr.Inst:ShowMessage("MENGHUA_NO_SKILL_TARGET", {})
			return
		end
		self.SkillMask:SetActive(true)
		g_ScriptEvent:BroadcastInLua("ShowCanBeSkillAt", info.skillId)
	else -- 不需要选择对象，直接发技能
		-- todo 增加震屏
		if skill.ShockRange ~= 0 then
			self:ShakeCamera()
		end
		Gac2Gas.RequestUseMengHuaLuSkill(info.skillId, 0)
		-- 显示宝宝的界面
		self:HideSkill(false)
		self:ShowBaby(false)
		self:ShowQiChang(false)
	end
	
end

function LuaMengHuaLuWnd:ShakeCamera()
	local shakeFxId = 88200004
	if self.ShakeFx then
		self.ShakeFx:Destroy()
	end
	local pos = CPos(CClientMainPlayer.Inst.Pos.x + 800, CClientMainPlayer.Inst.Pos.y + 800)
	self.ShakeFx = CEffectMgr.Inst:AddWorldPositionFX(shakeFxId, pos, 0, 0,1,-1,EnumWarnFXType.None,nil,0,0, nil)
end

function LuaMengHuaLuWnd:OnHideSkillBtnClicked(go)
	PlayerSettings.MengHuaLuSkillExpanded = false
	self:HideSkill(true)
	self:ShowBaby(true)
	self:ShowQiChang(true)
end

function LuaMengHuaLuWnd:OnCancelSkillBtnClicked()
	self.SkillMask:SetActive(false)
	g_ScriptEvent:BroadcastInLua("CancelCanBeSkillAt")
end

---------------------- 技能释放相关 end -----------------------



---------------------- 宝宝显示相关 -----------------------

-- 整体更新宝宝信息
function LuaMengHuaLuWnd:UpdateBaby(babyInfo)
	self.BabyInfo = babyInfo
	self:UpdateBabyTexture(0, 0)
	self.BabyFx:DestroyFx()
	self:UpdateBabyProps()
	self:UpdateBabyBuffs()

	self:UpdateQiChang()
	self:UpdateSkillCount()
	self:UpdateMengHuaLuLayer()
end

-- 更新宝宝外观（包括动作和特效）
function LuaMengHuaLuWnd:UpdateBabyTexture(expressionId, fxId)
	-- local appearanceData = {status, gender, hairColor, skinColor, hairstyle, grade, headId, bodyId, colorId, backId, qiChangId}

	local setting = MengHuaLu_Setting.GetData()
	local status = self.BabyInfo.AppearanceData[0]
	local gender = self.BabyInfo.AppearanceData[1]
	local hairColor = self.BabyInfo.AppearanceData[2]
	local skinColor = self.BabyInfo.AppearanceData[3]
	local hairstyle = self.BabyInfo.AppearanceData[4]

	local hairFashionId = 0
	local bodyFashionId = 0
	local colorId = 0
	local backId = 0
	local qiChangId = 0

	if self.BabyInfo.AppearanceData.Count > 6 then
		hairFashionId = self.BabyInfo.AppearanceData[6]
		bodyFashionId = self.BabyInfo.AppearanceData[7]
		colorId = self.BabyInfo.AppearanceData[8]
		backId = self.BabyInfo.AppearanceData[9]
		qiChangId = self.BabyInfo.AppearanceData[10]
	end

	if expressionId == 0 then
		local setting = MengHuaLu_Setting.GetData()
		expressionId = setting.BabyNormalExpression
	end
	-- (status, gender, hairColor, skinColor, hairstyle, headId, bodyId, colorId, wingId, qiChangId)
	local appearance = LuaBabyMgr.GetBabyAppearance(status, gender, hairColor, skinColor, hairstyle, hairFashionId, bodyFashionId, colorId, backId, qiChangId)
	local setting = MengHuaLu_Setting.GetData()

	self.BabyTexture:Init(appearance, setting.BabyDirection, expressionId, fxId)
end

-- 更新宝宝血量
function LuaMengHuaLuWnd:ShowBabyHpChange(grid, hpChange)
	if grid == 0 then
		self:UpdateBabyProps()
	end
end

-- 更新宝宝属性
function LuaMengHuaLuWnd:ShowBabyPropsChange(grid, propKey, propValue)
	if grid == 0 and self.BabyInfo then
		if propKey == 1 then
			-- 攻击
			if propValue > self.BabyInfo.Attack then
				self.BabyInfo.Attack = propValue
				self:ShowPropChange(self.AttackIcon)
			else
				self.BabyInfo.Attack = propValue
				self:UpdateBabyProps()
			end
		elseif propKey == 5 then
			-- 生命
			if propValue > self.BabyInfo.Hp then
				self.BabyInfo.Hp = propValue
				self:ShowPropChange(self.HPIcon)
			else
				self.BabyInfo.Hp = propValue
				self:UpdateBabyProps()
			end
		elseif propKey == 6 then
			-- 法力
			if propValue > self.BabyInfo.Mp then
				self.BabyInfo.Mp = propValue
				self:ShowPropChange(self.MPIcon)
			else
				self.BabyInfo.Mp = propValue
				self:UpdateBabyProps()
			end
		end
	end
end

function LuaMengHuaLuWnd:ResetBabyPropIcon()
	self:DestroyTweenList()
	self.AttackIcon.transform.localScale = Vector3(1, 1, 1)
	self.HPIcon.transform.localScale = Vector3(1, 1, 1)
	self.MPIcon.transform.localScale = Vector3(1, 1, 1)
	self.MoneyIcon.transform.localScale = Vector3(1, 1, 1)
end

function LuaMengHuaLuWnd:ShowPropChange(target)
	local tweener1 = LuaTweenUtils.TweenScale(target.transform, Vector3(1, 1, 1), Vector3(1.5, 1.5, 1.5), 0.2)
	LuaTweenUtils.SetEase(tweener1, Ease.InOutQuint)
	LuaTweenUtils.OnComplete(tweener1, function ()
		local tweener2 = LuaTweenUtils.TweenScale(target.transform, Vector3(1.5, 1.5, 1.5), Vector3(1, 1, 1), 0.2)
		self:UpdateBabyProps()
		table.insert(self.TweenList, tweener2)
	end)
	table.insert(self.TweenList, tweener1)
end

function LuaMengHuaLuWnd:UpdateBabyProps()
	self.BabyHPValueLabel.text = tostring(self.BabyInfo.Hp)
	self.BabyMPValueLabel.text = tostring(self.BabyInfo.Mp)
	self.FaliOwnLabel.text = tostring(self.BabyInfo.Mp)
	self.BabyAttackValueLabel.text = tostring(self.BabyInfo.Attack)
	self.BabyMoneyValueLabel.text = tostring(self.BabyInfo.Gold)
end

-- 更新宝宝的金币
function LuaMengHuaLuWnd:UpdateBabyGold(grid, gold)
	if grid == 0 then
		if gold > self.BabyInfo.Gold then
			self:ShowPropChange(self.MoneyIcon)
			self.BabyInfo.Gold = gold
		else
			self.BabyInfo.Gold = gold
			self:UpdateBabyProps()
		end
	end
end

---------------------- 宝宝显示相关 end -----------------------




---------------------- 宝宝动画相关 -----------------------

function LuaMengHuaLuWnd:AddAction(action)
	if action.SrcGridId == 0 or action.DestGridId == 0 then
		table.insert(self.ActionQueue, action)
	end
end

function LuaMengHuaLuWnd:GetActionToExecute()
	if self.ActionQueue and #self.ActionQueue > 0 then
		return table.remove(self.ActionQueue, 1)
	end
	return nil
end

function LuaMengHuaLuWnd:ExecuteAction(action)
	local SrcGridId = action.SrcGridId
	local DestGridId = action.DestGridId
	local SkillId = action.SkillId

	self:ExecuteBabyAction(SrcGridId, DestGridId, SkillId)
end

function LuaMengHuaLuWnd:ExecuteBabyAction(srcGridId, destGridId, skillId)
	local skill = MengHuaLu_Skill.GetData(skillId)
	if srcGridId == 0 then
		local attackfx = skill.MaleAttackFX
		if self.BabyInfo.AppearanceData[1] == 1 then
			attackfx = skill.FemaleAttackFX
		end
		self:UpdateBabyTexture(skill.CastAnimationID, attackfx)

		if not System.String.IsNullOrEmpty(skill.CastTalk) then
			self:ShowBabyTalk(skill.CastTalk)
			self:HideBabyTalk(true)
		end
	elseif destGridId == 0 then
		self:UpdateBabyTexture(skill.SufferAnimationID, skill.SufferFX)
		self.BabyFx:DestroyFx()
		self.BabyFx:LoadFx("fx/ui/prefab/UI_baobaoshouji01.prefab")
		self:HideBabyTalk(false)
	end
end

function LuaMengHuaLuWnd:ShowBabyTalk(str)
	if not System.String.IsNullOrEmpty(str) then
		self.BabyTalk.text = str
		LuaTweenUtils.TweenAlpha(self.BabyTalk.transform, 0, 1, 0.3)
		LuaTweenUtils.TweenPositionY(self.BabyTalk.transform, 76, 0.3)
	end
end

function LuaMengHuaLuWnd:HideBabyTalk(animated)
	if animated then
		local tween = LuaTweenUtils.TweenAlpha(self.BabyTalk.transform, 1, 0, 0.5)
		LuaTweenUtils.SetDelay(tween, 1.8)
		LuaUtils.SetLocalPositionY(self.BabyTalk.transform, 56)
	else
		LuaTweenUtils.TweenAlpha(self.BabyTalk.transform, 1, 0, 0.0)
		LuaUtils.SetLocalPositionY(self.BabyTalk.transform, 56)
	end
	
end


function LuaMengHuaLuWnd:UpdateMengHuaLuLayer()
	self.MengJingTitleLabel.text = SafeStringFormat3(LocalString.GetString("梦境%s"), Extensions.ToChinese(LuaMengHuaLuMgr.m_Layer))
end

function LuaMengHuaLuWnd:OnEnable()
	g_ScriptEvent:AddListener("MengHuaLuUpdateBaby", self, "UpdateBaby")
	g_ScriptEvent:AddListener("QueryMengHuaLuSkillResult", self, "UpdateSkills")
	g_ScriptEvent:AddListener("ShowCharacterHpChange", self, "ShowBabyHpChange")
	g_ScriptEvent:AddListener("SyncMengHuaLuCharacterFightProp", self, "ShowBabyPropsChange")
	g_ScriptEvent:AddListener("SyncMengHuaLuCharacterGold", self, "UpdateBabyGold")
	g_ScriptEvent:AddListener("MengHuaLuUpdateBabyBuffs", self, "UpdateBuffs")
	g_ScriptEvent:AddListener("RequestMengHuaLuSetQiChangSuccess", self, "MengHuaLuSetQiChangSuccess")
	g_ScriptEvent:AddListener("EnterMengHuaLuNewRound", self, "EnterMengHuaLuNewRound")
	g_ScriptEvent:AddListener("AddBabyAction", self, "AddAction")
	g_ScriptEvent:AddListener("SyncMengHuaLuSkillNum", self, "SyncMengHuaLuSkillNum")
	g_ScriptEvent:AddListener("MengHuaLuHideSkillView", self, "MengHuaLuHideSkill")
	g_ScriptEvent:AddListener("MengHuaLuEnterNewLayer", self, "ResetBabyPropIcon")
end

function LuaMengHuaLuWnd:OnDisable()
	g_ScriptEvent:RemoveListener("MengHuaLuUpdateBaby", self, "UpdateBaby")
	g_ScriptEvent:RemoveListener("QueryMengHuaLuSkillResult", self, "UpdateSkills")
	g_ScriptEvent:RemoveListener("ShowCharacterHpChange", self, "ShowBabyHpChange")
	g_ScriptEvent:RemoveListener("SyncMengHuaLuCharacterFightProp", self, "ShowBabyPropsChange")
	g_ScriptEvent:RemoveListener("SyncMengHuaLuCharacterGold", self, "UpdateBabyGold")
	g_ScriptEvent:RemoveListener("MengHuaLuUpdateBabyBuffs", self, "UpdateBuffs")
	g_ScriptEvent:RemoveListener("RequestMengHuaLuSetQiChangSuccess", self, "MengHuaLuSetQiChangSuccess")
	g_ScriptEvent:RemoveListener("EnterMengHuaLuNewRound", self, "EnterMengHuaLuNewRound")
	g_ScriptEvent:RemoveListener("AddBabyAction", self, "AddAction")
	g_ScriptEvent:RemoveListener("SyncMengHuaLuSkillNum", self, "SyncMengHuaLuSkillNum")
	g_ScriptEvent:RemoveListener("MengHuaLuHideSkillView", self, "MengHuaLuHideSkill")
	g_ScriptEvent:RemoveListener("MengHuaLuEnterNewLayer", self, "ResetBabyPropIcon")
end

function LuaMengHuaLuWnd:DestroyTweenList()
	if not self.TweenList or #self.TweenList <= 0 then return end
	for i = 1, #self.TweenList, 1 do
		LuaTweenUtils.Kill(self.TweenList[i], false)
	end
end

function LuaMengHuaLuWnd:EnterMengHuaLuNewRound(round)
	self.RoundLabel.text = SafeStringFormat3(LocalString.GetString("第%s回合"), tostring(round))
	--self:UpdateBuffRound()
end


function LuaMengHuaLuWnd:Update()
	if not self.StartTime then
		self.StartTime = Time.realtimeSinceStartup
	end
	if not self.IntervalTime then
		self.IntervalTime = 0.8
	end
	if Time.realtimeSinceStartup - self.StartTime > self.IntervalTime then
		local action = self:GetActionToExecute()
		if action then
			self.StartTime = Time.realtimeSinceStartup
			self.IntervalTime = action.Interval
			self:ExecuteAction(action)
		end
	end
end

return LuaMengHuaLuWnd
