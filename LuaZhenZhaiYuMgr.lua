LuaZhenZhaiYuMgr = {}

LuaZhenZhaiYuMgr.m_SelectedFishButtons = {
    LocalString.GetString("培养"),
    LocalString.GetString("喂食"),
    LocalString.GetString("取消关联"),
    LocalString.GetString("收至包裹"),
}

LuaZhenZhaiYuMgr.FrishCreelType = {
    ["Put"] = 1,
    ["Attach"] = 2,
    ["Clear"] = 3,
    ["Inherit"] = 4,
}

LuaZhenZhaiYuMgr.BagType = {
    ["Fish"] = 1,
    ["Other"] = 2,
}

--镇宅鱼在鱼缸里的速度范围
LuaZhenZhaiYuMgr.m_ZhenZhaiYuMinVelocity = 2
LuaZhenZhaiYuMgr.m_ZhenZhaiYuMaxVelocity = 3
--大鱼的速度范围扩大倍数
LuaZhenZhaiYuMgr.m_BigFishVelocityScale = 1.5
--1——5 越大越靠前 大鱼放最后一层 小鱼放在前三层
LuaZhenZhaiYuMgr.m_SmallFishLayerIndex1 = 3
LuaZhenZhaiYuMgr.m_SmallFishLayerIndex2 = 5
LuaZhenZhaiYuMgr.m_BigFishLayerIndex = 1

LuaZhenZhaiYuMgr.m_LastPackageIndex = 0

LuaZhenZhaiYuMgr.m_FishInFishJarScale = 150
function LuaZhenZhaiYuMgr.OpenFeedWnd(itemId)
    LuaZhenZhaiYuMgr.m_FeedFishItemId = itemId
    CUIManager.ShowUI(CLuaUIResources.FeedZhenZhaiYuWnd)
end

function LuaZhenZhaiYuMgr.OpenBringUpWnd(itemId)
    LuaZhenZhaiYuMgr.m_FeedFishItemId = itemId
    CUIManager.ShowUI(CLuaUIResources.BringUpZhenZhaiYuWnd)
end
function LuaZhenZhaiYuMgr.OpenChooseFabaoWnd(itemId)
    LuaZhenZhaiYuMgr.m_FeedFishItemId = itemId
    CUIManager.ShowUI(CLuaUIResources.ChooseFabao2GongFengWnd)
end

function LuaZhenZhaiYuMgr.OpenChooseAttachedZhenzhaiyu(fabaoId,suitWords,attachedFishs)
    LuaZhenZhaiYuMgr.m_FishCreelOpenType = LuaZhenZhaiYuMgr.FrishCreelType.Attach
    LuaZhenZhaiYuMgr.m_AttachedFabaoId = fabaoId
    LuaZhenZhaiYuMgr.m_AttachSuitWords = suitWords
    LuaZhenZhaiYuMgr.m_AttachedFishs = attachedFishs
    CUIManager.ShowUI(CLuaUIResources.FishCreelWnd)
end

function LuaZhenZhaiYuMgr.OpenFishCreelForClearing()
    LuaZhenZhaiYuMgr.m_FishCreelOpenType = LuaZhenZhaiYuMgr.FrishCreelType.Clear
    CUIManager.ShowUI(CLuaUIResources.FishCreelWnd)
end

function LuaZhenZhaiYuMgr.OpenFishCreelForInheriting()
    LuaZhenZhaiYuMgr.m_FishCreelOpenType = LuaZhenZhaiYuMgr.FrishCreelType.Inherit
    CUIManager.ShowUI(CLuaUIResources.FishCreelWnd)
end

function LuaZhenZhaiYuMgr.OpenClearZhenZhaiYuWordWnd(consumeItemPos,consumeItemId)
    LuaZhenZhaiYuMgr.m_ClearConsumeItemPos = consumeItemPos
    LuaZhenZhaiYuMgr.m_ClearConsumeItemId = consumeItemId
    CUIManager.ShowUI(CLuaUIResources.ClearZhenZhaiYuWordWnd)
end
function LuaZhenZhaiYuMgr.OpenInheritZhenZhaiYuWordWnd(consumeItemPos,consumeItemId)
    LuaZhenZhaiYuMgr.m_InheritConsumeItemPos = consumeItemPos
    LuaZhenZhaiYuMgr.m_InheritConsumeItemId = consumeItemId
    CUIManager.ShowUI(CLuaUIResources.InheritZhenZhaiYuWordWnd)
end


function LuaZhenZhaiYuMgr.InitFish2TalismanWords()
    if not LuaZhenZhaiYuMgr.m_Fish2TalismanWords then
        LuaZhenZhaiYuMgr.m_Fish2TalismanWords = {}
        HouseFish_WordMatch.Foreach(function(k,v)
            if not LuaZhenZhaiYuMgr.m_Fish2TalismanWords[v.FishWord] then
                LuaZhenZhaiYuMgr.m_Fish2TalismanWords[v.FishWord] = {}
            end
            table.insert(LuaZhenZhaiYuMgr.m_Fish2TalismanWords[v.FishWord],k)
        end)
    end
end

function LuaZhenZhaiYuMgr.IsZhenZhaiFish(itemId)
    local fishData = HouseFish_AllFishes.GetData(itemId)
    if fishData then
        if fishData.IsZhenZhai == 1 then
            return true
        end
    end
    return false

end

function LuaZhenZhaiYuMgr.CheckCanFangSheng(templateId)
    local fishData = HouseFish_AllFishes.GetData(templateId)
    if fishData and fishData.IsZhenZhai == 1 then
        return true
    end
    return false
end