local CItemInfoMgr = import "L10.UI.CItemInfoMgr"

LuaReceiveGiftPackSettingWnd = class()


function LuaReceiveGiftPackSettingWnd:Awake()
    
end

function LuaReceiveGiftPackSettingWnd:OnEnable()
    g_ScriptEvent:AddListener("ItemPropUpdated", self, "Init")
end

function LuaReceiveGiftPackSettingWnd:OnDisable()
    g_ScriptEvent:RemoveListener("ItemPropUpdated", self, "Init")
end

function LuaReceiveGiftPackSettingWnd:Init()
    if not CClientMainPlayer.Inst then CUIManager.CloseUI(CLuaUIResources.ReceiveGiftPackSettingWnd) 
    else
        local default = CClientMainPlayer.Inst.ItemProp.BigMonthCardInfo.DefaultItemId
        local hasFeiSheng = CClientMainPlayer.Inst.HasFeiSheng
        local i = 1
        Charge_BigMonthCardItem.Foreach(function(k, v)
            local option = self.transform:Find("SettingPanel/Grid/Option"..i)
            local str = SafeStringFormat3(LocalString.GetString("购买%s期间都领[00FF00][%s][-]"), Charge_Charge.GetData(LuaWelfareMgr.BigMonthCardId).Description01, Item_Item.GetData(k).Name)
            local needFeiSheng = v.NeedFeiSheng == 1
            if needFeiSheng then
                str = str..LocalString.GetString("（飞升特权）")
            end
            local label = option:Find("text"):GetComponent(typeof(UILabel))
            label.text = str
            UIEventListener.Get(label.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
                CItemInfoMgr.ShowLinkItemTemplateInfo(k, false, nil, CItemInfoMgr.AlignType.ScreenRight, 0, 0, 0, 0)
            end)
            option:Find("click/node").gameObject:SetActive(default == k)
            UIEventListener.Get(option:Find("click").gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
                if default ~= k then
                    if needFeiSheng and not hasFeiSheng then
                        g_MessageMgr:ShowMessage("BMC_Cannot_Get_ZiBei")
                    else
                        Gac2Gas.RequestSetBigMonthCardDefaultItemId(k)
                    end
                else
                    Gac2Gas.RequestSetBigMonthCardDefaultItemId(0)
                end
            end)
            i = i + 1
        end)
    end
end
