local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnTableView = import "L10.UI.QnTableView"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CButton = import "L10.UI.CButton"
local EnumChineseDigits = import "L10.Game.EnumChineseDigits"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CGuideMgr = import "L10.Game.Guide.CGuideMgr"
local Rect = import "UnityEngine.Rect"

LuaBusinessLotteryWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaBusinessLotteryWnd, "TableView", "TableView", QnTableView)
RegistChildComponent(LuaBusinessLotteryWnd, "SmallRewardNotice", "SmallRewardNotice", GameObject)
RegistChildComponent(LuaBusinessLotteryWnd, "BigRewardNotice", "BigRewardNotice", GameObject)
RegistChildComponent(LuaBusinessLotteryWnd, "TipBtn", "TipBtn", GameObject)
RegistChildComponent(LuaBusinessLotteryWnd, "CurNotice", "CurNotice", GameObject)
RegistChildComponent(LuaBusinessLotteryWnd, "BuyOp", "BuyOp", GameObject)
RegistChildComponent(LuaBusinessLotteryWnd, "StateAndOp", "StateAndOp", GameObject)
RegistChildComponent(LuaBusinessLotteryWnd, "SelectedWordLab", "SelectedWordLab", UILabel)
RegistChildComponent(LuaBusinessLotteryWnd, "ResetBtn", "ResetBtn", GameObject)
RegistChildComponent(LuaBusinessLotteryWnd, "RewardTip", "RewardTip", UILabel)
RegistChildComponent(LuaBusinessLotteryWnd, "OpBtn", "OpBtn", CButton)
RegistChildComponent(LuaBusinessLotteryWnd, "Rewarded", "Rewarded", GameObject)
RegistChildComponent(LuaBusinessLotteryWnd, "failed", "failed", GameObject)
RegistChildComponent(LuaBusinessLotteryWnd, "Date", "Date", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaBusinessLotteryWnd, "m_Labs")
RegistClassMember(LuaBusinessLotteryWnd, "m_SelectedWord")
RegistClassMember(LuaBusinessLotteryWnd, "m_BuyBtn")
RegistClassMember(LuaBusinessLotteryWnd, "m_CurAnimSelect")
RegistClassMember(LuaBusinessLotteryWnd, "m_CurWordLabs")

function LuaBusinessLotteryWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self:InitComponents()

    UIEventListener.Get(self.TipBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        g_MessageMgr:ShowMessage("BUSINESS_PEGIONTICK_TIP")
    end)

    UIEventListener.Get(self.m_BuyBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnBuyBtnClick()
    end)

    UIEventListener.Get(self.OpBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnOpBtnClick()
    end)

    UIEventListener.Get(self.ResetBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:ResetSelected()
    end)

    self.m_Labs = {}
    local surNameStr = LocalString.GetString("赵;钱;孙;李;周;吴;郑;王;冯;陈;褚;卫;蒋;沈;韩;杨;")
    local splits = g_LuaUtil:StrSplit(surNameStr, ";")
    for i = 1, #splits do
        if splits[i] ~= "" then
            table.insert(self.m_Labs, splits[i])
        end
    end
    
    self.m_SelectedWord = {}
    for i = 1, #LuaBusinessMgr.m_OwnTicketTab do
        self.m_SelectedWord[i] = LuaBusinessMgr.m_OwnTicketTab[i]
    end
    self.m_CurAnimSelect = 0

    self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_Labs
        end,
        function(item, index)
            self:InitItem(item, self.m_Labs[index + 1], index + 1)
        end
    )

    self.TableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        if CGuideMgr.Inst:IsInPhase(EnumGuideKey.BusinessPegionTickGuide) then
            CGuideMgr.Inst:TriggerNextStep()
        end

        if #self.m_SelectedWord < 4 then
            for i = 1, #self.m_SelectedWord do
                if self.m_SelectedWord[i] == row + 1 then
                    return
                end
            end
            table.insert(self.m_SelectedWord, row + 1)
            self.m_CurAnimSelect = #self.m_SelectedWord
            self.TableView:ReloadData(true, false)
            self:RefreshSelected()
        else
            self.m_CurAnimSelect = 0
        end
    end)

    self.Date.text = math.ceil(LuaBusinessMgr.m_CurDay / 3) * 3
end

function LuaBusinessLotteryWnd:Init()
    self:RefreshState()
    self:RefreshSelected()
    self.TableView:ReloadData(true, false)
end

function LuaBusinessLotteryWnd:InitComponents()
    self.m_BuyBtn = self.BuyOp.transform:Find("BuyBtn").gameObject

    self.m_CurWordLabs = {}
    for i = 1, 4 do
        self.m_CurWordLabs[i] = self.CurNotice.transform:Find("CurWordLab" .. i):GetComponent(typeof(UITexture))
    end
end

--@region UIEvent

--@endregion UIEvent
function LuaBusinessLotteryWnd:OnEnable()
    g_ScriptEvent:AddListener("OnBusinessDataUpdate", self, "OnBusinessDataUpdate")
end

function LuaBusinessLotteryWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnBusinessDataUpdate", self, "OnBusinessDataUpdate")
end

function LuaBusinessLotteryWnd:RefreshRewardWord()
    if #LuaBusinessMgr.m_CurTicketTab ~= 4 then
        return
    end
    for i = 1, 4 do 
        local index = LuaBusinessMgr.m_CurTicketTab[i]
        if index then
            self:SetLabByIndex(self.m_CurWordLabs[i], index)
        end
    end
end

function LuaBusinessLotteryWnd:SetLabByIndex(lab, index)
    index = index - 1
    local xIndex = math.floor(index % 4)
    local yIndex = math.floor(index / 4)
    local newRect = Rect(0.205 + xIndex * 0.165, 0.615 - yIndex * 0.165, 0.165, 0.165)
    lab.uvRect = newRect
end

function LuaBusinessLotteryWnd:RefreshSelected()
    local word = ""
    for i = 1, 4 do 
        local index = self.m_SelectedWord[i]
        if index then
            word = word .. self.m_Labs[index]
        end
    end
    self.SelectedWordLab.text = word
end

function LuaBusinessLotteryWnd:ResetSelected()
    self.m_SelectedWord = {}
    self:RefreshSelected()
    self.TableView:ReloadData(true, false)
end

function LuaBusinessLotteryWnd:InitItem(item, info, index)
    local lab       = item.transform:Find("Lab"):GetComponent(typeof(UITexture))
    local selected  = item.transform:Find("Selected"):GetComponent(typeof(UITexture))
    local seqLab    = item.transform:Find("Selected/SeqLab"):GetComponent(typeof(UILabel))

    local isFind = nil
    for i = 1, 4 do 
        local ind = self.m_SelectedWord[i]
        if ind == index then
            isFind = i 
            break
        end
    end
    
    self:SetLabByIndex(lab, index)
    selected.gameObject:SetActive(isFind)
    if isFind then
        seqLab.text = EnumChineseDigits.GetDigit(isFind)
    end
    if self.m_CurAnimSelect == isFind then
        LuaTweenUtils.TweenAlpha(selected, 0, 1, 0.5,function()end)
        self.m_CurAnimSelect = 0
    end
end

function LuaBusinessLotteryWnd:OnBusinessDataUpdate()
    self:RefreshState()
end
-- 1 未开奖未购买  2 未开奖已购买 3 开奖日未购买 4 开奖日未中奖 5 开奖日小奖 6 开奖日大奖
function LuaBusinessLotteryWnd:RefreshState()
    local state = LuaBusinessMgr.m_TicketState
    
    if state == 1 then
        self:NotTheLotteryNoBuy()
    elseif state == 2 then
        self:NotTheLotteryPurchased()
    elseif state == 3 then
        self:TheLotteryNoBuy()
    elseif state == 4 then
        self:TheLotteryLosing()
    elseif state == 5 then
        self:TheLotterySmallReward()
    elseif state == 6 then
        self:TheLotteryBigReward()
    end
end

function LuaBusinessLotteryWnd:OnBuyBtnClick()
    if #self.m_SelectedWord ~= 4 then
        return
    end

    local msg
    if LuaBusinessMgr.m_OwnMoney < 200 then
        msg = g_MessageMgr:FormatMessage("BUSINESS_BUYPEGIONTICK_MAKESURE_DEBT")
    else
        msg = g_MessageMgr:FormatMessage("BUSINESS_BUYPEGIONTICK_MAKESURE")
    end

    MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function()
        local pegionTick = LuaBusinessMgr:PackPegionTickTab(self.m_SelectedWord)
        Gac2Gas.TradeSimulateBuyPegionTick(pegionTick)
    end),nil,LocalString.GetString("确定"),LocalString.GetString("取消"),false)
end

function LuaBusinessLotteryWnd:OnOpBtnClick()
    local state = LuaBusinessMgr.m_TicketState

    if state == 2 then
        CUIManager.CloseUI(CLuaUIResources.BusinessLotteryWnd)
    elseif state >= 5 then
        Gac2Gas.PegionTickAward()
    end
end

-- 未开奖日未购买
function LuaBusinessLotteryWnd:NotTheLotteryNoBuy()
    self:SwitchState(1)
    self:SwitchBuyState(false, false)
    self:RefreshRwardState(1)

    CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.BusinessPegionTickGuide)
end

-- 未开奖日已购买
function LuaBusinessLotteryWnd:NotTheLotteryPurchased()
    self:SwitchState(1)
    self:SwitchBuyState(false, true)
    self:RefreshRwardState(1)
end

-- 开奖日未购买
function LuaBusinessLotteryWnd:TheLotteryNoBuy()
    self:SwitchState(2)
    self:SwitchBuyState(true, false)
    self:RefreshRewardWord()
    self:RefreshRwardState(3)
end

-- 开奖日未中奖
function LuaBusinessLotteryWnd:TheLotteryLosing()
    self:SwitchState(2)
    self:SwitchBuyState(true, true)
    self:RefreshRewardWord()
    self:RefreshRwardState(3)
end

-- 开奖日小奖
function LuaBusinessLotteryWnd:TheLotterySmallReward()
    self:SwitchState(3)
    self:SwitchBuyState(true, true)
    self:RefreshRewardWord()
    self:RefreshRwardState(LuaBusinessMgr.m_TicketRewarded and 4 or 2)
end

-- 开奖日大奖
function LuaBusinessLotteryWnd:TheLotteryBigReward()
    self:SwitchState(4)
    self:SwitchBuyState(true, true)
    self:RefreshRewardWord()
    self:RefreshRwardState(LuaBusinessMgr.m_TicketRewarded and 4 or 2)
end

-- 1 normal 2 未中奖 3 小奖 4 大奖
function LuaBusinessLotteryWnd:SwitchState(type)
    self.SmallRewardNotice:SetActive(type == 3)
    self.BigRewardNotice:SetActive(type == 4)
    self.failed:SetActive(type == 2)
    self.CurNotice:SetActive(type ~= 1)
end

-- bBuy是否已购买
function LuaBusinessLotteryWnd:SwitchBuyState(isRewardDay, bBuy)
    self.BuyOp:SetActive(not bBuy and not isRewardDay)
    self.StateAndOp:SetActive(bBuy or isRewardDay)
end

-- 刷新中奖状态 1 静待开奖 2 收下奖金 3 下次开奖 4 已领奖
function LuaBusinessLotteryWnd:RefreshRwardState(state)
    self.OpBtn.Text = state == 1 and LocalString.GetString("静待开奖") or LocalString.GetString("收下奖金")
    self.OpBtn.gameObject:SetActive(state == 1 or state == 2)
    self.RewardTip.gameObject:SetActive(state == 3)
    self.Rewarded:SetActive(state == 4)
end

function LuaBusinessLotteryWnd:GetGuideGo(methodName)
    if methodName == "GetSelectArea" then
		return self.TableView.gameObject
    elseif methodName == "GetResetBtn" then
        return self.ResetBtn
	end
	return nil
end
