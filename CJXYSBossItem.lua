-- Auto Generated!!
local AlignType = import "L10.UI.CTooltip+AlignType"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CJXYSBossItem = import "L10.UI.CJXYSBossItem"
local CJXYSMgr = import "L10.Game.CJXYSMgr"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local StringBuilder = import "System.Text.StringBuilder"

CJXYSBossItem.m_Init_CS2LuaHook = function (this, info, teleportRemains) 
    this.mInfo = info
    this.hasNeedItem = false
    this.teleporterLabel.text = System.String.Format(LocalString.GetString("{0}层传送点"), info.TeleporterFloor)

    -- Step 1：显示为蓝色
    this.goToBtn.background.spriteName = CJXYSBossItem.BUTTON_BG_BLUE
    this.goToBtn.Text = LocalString.GetString("前往")
    -- Step 2：更新
    -- 第一层传送的特殊处理
    if info.NeedItemId == 0 and info.TeleporterFloor == CJXYSMgr.FLOOR_NO_NEED_ITEM_TELEPORT then
        this.hasNeedItem = true
        this.goToBtn.background.spriteName = CJXYSBossItem.BUTTON_BG_ORANGE
    end

    if info.NeedItemId ~= 0 then
        if teleportRemains > 0 then
            this.hasNeedItem = true
            this.goToBtn.background.spriteName = CJXYSBossItem.BUTTON_BG_ORANGE
        end
        this.goToBtn.Text = System.String.Format(LocalString.GetString("前往({0})"), teleportRemains)
    end

    if this.mInfo.Count == 0 or this.mInfo.AppearFloor.Count == 0 then
        if CJXYSMgr.RemainMinutes <= 0 then
            --CJXYSMgr.RemainMinutes = 0
            this.siteLabel.text = LocalString.GetString("BOSS正在刷新")
        else
            this.isNeedUpdate = true
            this.siteLabel.text = System.String.Format(LocalString.GetString("[c][ffc000]{0}[-][/c]分钟后出现BOSS"), CJXYSMgr.RemainMinutes)
        end 
    else
        this.isNeedUpdate = false
        local appearStrBuilder = NewStringBuilderWraper(StringBuilder)
        appearStrBuilder:Append(System.String.Format(LocalString.GetString("第[c][ffed5f]{0}[-][/c]层[c][ffed5f]{1}[-][/c]个"), this.mInfo.AppearFloor[0], this.mInfo.AppearBossCount[0]))
        do
            local i = 1
            while i < this.mInfo.AppearFloor.Count do
                appearStrBuilder:Append(", " .. System.String.Format(LocalString.GetString("第[c][ffed5f]{0}[-][/c]层[c][ffed5f]{1}[-][/c]个"), this.mInfo.AppearFloor[i], this.mInfo.AppearBossCount[i]))
                i = i + 1
            end
        end
        this.siteLabel.text = System.String.Format(LocalString.GetString("BOSS: {0}"), ToStringWrap(appearStrBuilder))
    end
end
CJXYSBossItem.m_OnGotoBtnClicked_CS2LuaHook = function (this, go) 

    if this.mInfo.TeleporterFloor == CJXYSMgr.FLOOR_NO_NEED_ITEM_TELEPORT then
        Gac2Gas.RequestEnterJXYSPlay(CJXYSMgr.FLOOR_NO_NEED_ITEM_TELEPORT)
        return
    end

    if this.hasNeedItem then
        MessageWndManager.ShowOKCancelMessage(System.String.Format(LocalString.GetString("是否前往怨憎洞{0}层"), this.mInfo.TeleporterFloor), DelegateFactory.Action(function () 
            Gac2Gas.RequestEnterJXYSPlay(this.mInfo.TeleporterFloor)
        end), nil, nil, nil, false)
    else
        CItemAccessListMgr.Inst:ShowItemAccessInfo(this.mInfo.NeedItemId, true, this.transform, AlignType.Right)
    end
end
CJXYSBossItem.m_hookUpdate = function (this)
    if not this.isNeedUpdate then return end
    if CJXYSMgr.RemainMinutes <= 0 then
        this.siteLabel.text = LocalString.GetString("BOSS正在刷新")
    else
        this.siteLabel.text = System.String.Format(LocalString.GetString("[c][ffc000]{0}[-][/c]分钟后出现BOSS"), CJXYSMgr.RemainMinutes)
    end
end
