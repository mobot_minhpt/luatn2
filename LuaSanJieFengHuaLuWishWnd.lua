


local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"

LuaSanJieFengHuaLuWishWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaSanJieFengHuaLuWishWnd, "TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaSanJieFengHuaLuWishWnd, "CancelBtn", "CancelBtn", GameObject)
RegistChildComponent(LuaSanJieFengHuaLuWishWnd, "OKBtn", "OKBtn", GameObject)
RegistChildComponent(LuaSanJieFengHuaLuWishWnd, "CostLabel", "CostLabel", UILabel)
RegistChildComponent(LuaSanJieFengHuaLuWishWnd, "OwnLabel", "OwnLabel", UILabel)

--@endregion RegistChildComponent end

function LuaSanJieFengHuaLuWishWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.CancelBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCancelBtnClick()
	end)

	UIEventListener.Get(self.OKBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOKBtnClick()
	end)

    --@endregion EventBind end
end

function LuaSanJieFengHuaLuWishWnd:Init()
	local data = WorldEvent_FengHuaLu.GetData(LuaWorldEventMgr2021.WishID)
	self.TitleLabel.text = g_MessageMgr:FormatMessage("FengHuaLu_Wish_Confirm_Tip",(data.WishProbability*100).."%")
	self.CostLabel.text = data.WishNeedHuoLi
	local own = CClientMainPlayer.Inst.PlayProp.HuoLi
	if own >= data.WishNeedHuoLi then
		self.OwnLabel.text = own
	else
		self.OwnLabel.text = System.String.Format("[c][ff0000]{0}[-][/c]", own)
	end
end

--@region UIEvent

function LuaSanJieFengHuaLuWishWnd:OnCancelBtnClick()
	LuaWorldEventMgr2021.CloseFengHuaLuWishWnd()
end

function LuaSanJieFengHuaLuWishWnd:OnOKBtnClick()
	local own = CClientMainPlayer.Inst.PlayProp.HuoLi
	local data = WorldEvent_FengHuaLu.GetData(LuaWorldEventMgr2021.WishID)
	if own >= data.WishNeedHuoLi then
		LuaWorldEventMgr2021.RequestWish()
	else
		g_MessageMgr:ShowMessage("HUOLI_NO_ENOUGH",data.WishNeedHuoLi)
	end
 	LuaWorldEventMgr2021.CloseFengHuaLuWishWnd()
end

--@endregion UIEvent

