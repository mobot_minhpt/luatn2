local Animation=import "UnityEngine.Animation"
local GameObject = import "UnityEngine.GameObject"

LuaHaiZhanPickResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaHaiZhanPickResultWnd, "Success", "Success", GameObject)
RegistChildComponent(LuaHaiZhanPickResultWnd, "Fail", "Fail", GameObject)
RegistChildComponent(LuaHaiZhanPickResultWnd, "Mat1Label", "Mat1Label", UILabel)
RegistChildComponent(LuaHaiZhanPickResultWnd, "Mat2Label", "Mat2Label", UILabel)
RegistChildComponent(LuaHaiZhanPickResultWnd, "Grid", "Grid", UIGrid)

--@endregion RegistChildComponent end

LuaHaiZhanPickResultWnd.s_Info = nil

function LuaHaiZhanPickResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaHaiZhanPickResultWnd:Init()
    if LuaHaiZhanPickResultWnd.s_Info then
        self.transform:Find("Anchor"):GetComponent(typeof(Animation)):Play("haizhanpickresultwnd_success")
        self.Success:SetActive(true)
        self.Fail:SetActive(false)

        local cnt1,cnt2 = 0,0
        for index, value in ipairs(LuaHaiZhanPickResultWnd.s_Info) do
            local type, id, count = value[1],value[2],value[3]
            if type==1 then--cannon
                cnt1 = cnt1+count
            elseif type==2 then--board
                cnt2 = cnt2+count
            end
        end

        self.Mat1Label.gameObject:SetActive(false)
        self.Mat2Label.gameObject:SetActive(false)
        if cnt1>0 then
            self.Mat1Label.text = "+"..tostring(cnt1)
            self.Mat1Label.gameObject:SetActive(true)
        end
        if cnt2>0 then
            self.Mat2Label.text = "+"..tostring(cnt2)
            self.Mat2Label.gameObject:SetActive(true)
        end
        self.Grid:Reposition()
    else
        self.transform:Find("Anchor"):GetComponent(typeof(Animation)):Play("haizhanpickresultwnd_fail")
        self.Success:SetActive(false)
        self.Fail:SetActive(true)
    end

    RegisterTickOnce(function()
        CUIManager.CloseUI(CLuaUIResources.HaiZhanPickResultWnd)
    end,2000)
end

--@region UIEvent

--@endregion UIEvent


function LuaHaiZhanPickResultWnd:OnDestroy()
    LuaHaiZhanPickResultWnd.s_Info = nil
end