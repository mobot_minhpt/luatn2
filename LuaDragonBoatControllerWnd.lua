

local GameObject = import "UnityEngine.GameObject"
local CUITexture = import "L10.UI.CUITexture"
local UISlider = import "UISlider"
local UISprite = import "UISprite"
local CClientObjectMgr=import "L10.Game.CClientObjectMgr"
local CMainCamera = import "L10.Engine.CMainCamera"
local CClientOtherPlayer = import "L10.Game.CClientOtherPlayer"

LuaDragonBoatControllerWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaDragonBoatControllerWnd, "MyIcon", "MyIcon", CUITexture)
RegistChildComponent(LuaDragonBoatControllerWnd, "FriendIcon", "FriendIcon", CUITexture)
RegistChildComponent(LuaDragonBoatControllerWnd, "HighlightSection1", "HighlightSection1", GameObject)
RegistChildComponent(LuaDragonBoatControllerWnd, "HighlightSection2", "HighlightSection2", GameObject)
RegistChildComponent(LuaDragonBoatControllerWnd, "HighlightSection3", "HighlightSection3", GameObject)
RegistChildComponent(LuaDragonBoatControllerWnd, "RedHighlight", "RedHighlight", GameObject)
RegistChildComponent(LuaDragonBoatControllerWnd, "CurStateSlider", "CurStateSlider", UISlider)
RegistChildComponent(LuaDragonBoatControllerWnd, "CurStateSprite", "CurStateSprite", UISprite)
RegistChildComponent(LuaDragonBoatControllerWnd, "SliderForegroundSprite", "SliderForegroundSprite", UISprite)
RegistChildComponent(LuaDragonBoatControllerWnd, "CurState", "CurState", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaDragonBoatControllerWnd,"m_MyIconRadius")
RegistClassMember(LuaDragonBoatControllerWnd,"m_FriendIconRadius")
RegistClassMember(LuaDragonBoatControllerWnd,"m_HighlightSprites")
RegistClassMember(LuaDragonBoatControllerWnd,"m_UpdateMyPointerTick")
RegistClassMember(LuaDragonBoatControllerWnd,"m_UpdateFriendPointerTick")
RegistClassMember(LuaDragonBoatControllerWnd,"m_HidePointersTick")
RegistClassMember(LuaDragonBoatControllerWnd,"m_OperatDuration")
RegistClassMember(LuaDragonBoatControllerWnd,"m_OperatInterval")
RegistClassMember(LuaDragonBoatControllerWnd,"m_IsInitMyIcon")
RegistClassMember(LuaDragonBoatControllerWnd,"m_IsInitFriendIcon")
RegistClassMember(LuaDragonBoatControllerWnd,"m_CenterOfCirclePosY")
function LuaDragonBoatControllerWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaDragonBoatControllerWnd:Init()
	self.m_CenterOfCirclePosY = 60
	self.m_MyIconRadius = self.MyIcon.transform.localPosition.y - self.m_CenterOfCirclePosY
	self.m_FriendIconRadius = self.FriendIcon.transform.localPosition.y - self.m_CenterOfCirclePosY
	self.m_HighlightSprites = {self.HighlightSection1, self.HighlightSection2, self.HighlightSection3}
	for i,highlightSprite in pairs(self.m_HighlightSprites) do
		highlightSprite:SetActive(false)
	end
	self.RedHighlight:SetActive(false)
	self.CurState:SetActive(false)
	self.MyIcon.gameObject:SetActive(false)
	self.FriendIcon.gameObject:SetActive(false)
	self.m_OperatDuration = Duanwu2021_LongZhouSetting.GetData().OperatDuration
	self.m_OperatInterval = Duanwu2021_LongZhouSetting.GetData().OperatInterval
end 

--@region UIEvent

--@endregion UIEvent
function LuaDragonBoatControllerWnd:OnEnable()
	g_ScriptEvent:AddListener("OnSyncStartLongZhouOperate", self, "OnSyncStartLongZhouOperate")
	g_ScriptEvent:AddListener("OnSyncLongZhouOperateResult", self, "OnSyncLongZhouOperateResult")
	g_ScriptEvent:AddListener("OnAddLongZhouTempleBuff", self, "OnAddLongZhouTempleBuff")
end

function LuaDragonBoatControllerWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnSyncStartLongZhouOperate", self, "OnSyncStartLongZhouOperate")
	g_ScriptEvent:RemoveListener("OnSyncLongZhouOperateResult", self, "OnSyncLongZhouOperateResult")
	g_ScriptEvent:RemoveListener("OnAddLongZhouTempleBuff", self, "OnAddLongZhouTempleBuff")
	self:CancelUpdateMyPointerTick()
	self:CancelUpdateFriendPointerTick()
	self:CancelHidePointersTick()
	LuaTweenUtils.DOKill(self.MyIcon.transform, false)
	LuaTweenUtils.DOKill(self.FriendIcon.transform, false)
	LuaTweenUtils.DOKill(self.CurState.transform, false)
end

function LuaDragonBoatControllerWnd:OnAddLongZhouTempleBuff(buffId)
	local data = Duanwu2021_LongZhouSetting.GetData()
	self.CurStateSlider.value = 1
	self.CurState:SetActive(true)
	self.CurState.transform:Find("CurStateSprite/Up").gameObject:SetActive(buffId == data.SpeedUpBuffId)
	self.CurState.transform:Find("CurStateSprite/Down").gameObject:SetActive(buffId == data.SpeedDownBuffId)
	self.CurState.transform:Find("CurStateSprite/Wait").gameObject:SetActive(buffId == data.PauseBuffId)
	local endByTime = 10
	for i = 0,data.BuffEndTime.Length - 1 do
		if data.BuffEndTime[i][0] == buffId then
			endByTime = data.BuffEndTime[i][1]
		end
	end
	LuaTweenUtils.DOKill(self.CurState.transform, false)
	local tween = LuaTweenUtils.TweenFloat(1, 0, endByTime, function ( val )
		self.CurStateSlider.value = val
	end)
	LuaTweenUtils.OnComplete(tween, function ()
		self.CurState:SetActive(false)
	end)
	LuaTweenUtils.SetTarget(tween, self.CurState.transform)
end

function LuaDragonBoatControllerWnd:OnSyncStartLongZhouOperate()
	self.RedHighlight:SetActive(false)
	for i,highlightSprite in pairs(self.m_HighlightSprites) do
		highlightSprite:SetActive(false)
	end
	self:ShowPointerAni(true)
	self:ShowPointerAni(false)
end

function LuaDragonBoatControllerWnd:ShowPointerAni(isMy)
	if isMy then
		self:CancelUpdateMyPointerTick()
	else
		self:CancelUpdateFriendPointerTick()
	end
	local icon = isMy and self.MyIcon or self.FriendIcon
	LuaTweenUtils.DOKill(icon.transform, false)
	local tween = LuaTweenUtils.TweenFloat(0, 1, self.m_OperatDuration, function ( val )
		self:ResetObj(icon.gameObject, val, isMy and self.m_MyIconRadius or self.m_FriendIconRadius)
	end)
	LuaTweenUtils.OnComplete(tween, function ()
		icon.gameObject:SetActive(false)
		if isMy then
			self:CancelUpdateMyPointerTick()
			self.m_UpdateMyPointerTick = RegisterTickOnce(function()
				self:ShowPointerAni(isMy)
			end, self.m_OperatInterval * 1000)
		else
			self:CancelUpdateFriendPointerTick()
			self.m_UpdateFriendPointerTick = RegisterTickOnce(function()
				self:ShowPointerAni(isMy)
			end, self.m_OperatInterval * 1000)
		end
	end)
	LuaTweenUtils.SetTarget(tween, icon.transform)
end

function LuaDragonBoatControllerWnd:OnSyncLongZhouOperateResult(myValue,friendValue ,isRoundOver)
	if myValue then
		LuaTweenUtils.DOKill(self.MyIcon.transform, false)
		self:CancelUpdateMyPointerTick()
		self:ResetObj(self.MyIcon.gameObject, myValue / self.m_OperatDuration / 1000, self.m_MyIconRadius)
	end
	if friendValue then
		LuaTweenUtils.DOKill(self.FriendIcon.transform, false)
		self:CancelUpdateFriendPointerTick()
		self:ResetObj(self.FriendIcon.gameObject, friendValue / self.m_OperatDuration / 1000, self.m_FriendIconRadius)
	end
	self.RedHighlight:SetActive(isRoundOver)
	if myValue and friendValue then
		self:ShowHighlight(myValue / self.m_OperatDuration / 1000, friendValue / self.m_OperatDuration / 1000)
		self:CancelHidePointersTick()
		self.m_HidePointersTick = RegisterTickOnce(function()
			self.MyIcon.gameObject:SetActive(false)
			self.FriendIcon.gameObject:SetActive(false)
		end, 500)
	end
end

function LuaDragonBoatControllerWnd:CancelUpdateMyPointerTick()
	if self.m_UpdateMyPointerTick then
		UnRegisterTick(self.m_UpdateMyPointerTick)
		self.m_UpdateMyPointerTick = nil
	end
end

function LuaDragonBoatControllerWnd:CancelUpdateFriendPointerTick()
	if self.m_UpdateFriendPointerTick then
		UnRegisterTick(self.m_UpdateFriendPointerTick)
		self.m_UpdateFriendPointerTick = nil
	end
end

function LuaDragonBoatControllerWnd:CancelHidePointersTick()
	if self.m_HidePointersTick then
		UnRegisterTick(self.m_HidePointersTick)
		self.m_HidePointersTick = nil
	end
end

function LuaDragonBoatControllerWnd:Update()
	if not CClientMainPlayer.Inst then return end
	local driverRO = CClientMainPlayer.Inst.VehicleRO and CClientMainPlayer.Inst.VehicleRO.DriverRO or nil
	self:InitIcons()
	if driverRO then
		local pos = driverRO.Position
		local leaderWorldPos = Vector3(pos.x, pos.y + 1, pos.z)
		local viewPos = CMainCamera.Main:WorldToViewportPoint(leaderWorldPos)
		local isInView = viewPos.z > 0 and viewPos.x > 0 and viewPos.x < 1 and viewPos.y > 0 and viewPos.y < 1
		local screenPos = isInView and CMainCamera.Main:WorldToScreenPoint(leaderWorldPos) or Vector3(0,3000,0)
		screenPos.z = 0
		local worldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(screenPos)
		self.CurState.transform.position = worldPos
	end
end

function LuaDragonBoatControllerWnd:ResetObj(obj,value, radius)
	local angle = math.lerp(0.0174533*135, 0.0174533*45, value)
	local rotationZ = math.lerp(45, -45, value)
	LuaUtils.SetLocalRotation(obj.transform.transform,0,0,rotationZ)
	obj.transform.localPosition = Vector3(radius * math.cos(angle), radius * math.sin(angle) + self.m_CenterOfCirclePosY, 0)
	obj:SetActive(value > 0 and ((obj == self.MyIcon.gameObject and self.m_IsInitMyIcon) or (obj == self.FriendIcon.gameObject and self.m_IsInitFriendIcon) ))
end

function LuaDragonBoatControllerWnd:ShowHighlight(myValue, friendValue)
	local mySection, friendSection = self:GetSection(myValue), self:GetSection(friendValue)
	for i,highlightSprite in pairs(self.m_HighlightSprites) do
		highlightSprite:SetActive(mySection == friendSection and i == mySection)
	end
	self.RedHighlight:SetActive(mySection ~= friendSection)
end

function LuaDragonBoatControllerWnd:GetSection(value)
	return value <= 1/3 and 1 or (value <= 2/3 and 2 or 3)
end

function LuaDragonBoatControllerWnd:InitIcons()
	if not CClientMainPlayer.Inst then
		return
	end
	local driverRO = CClientMainPlayer.Inst.VehicleRO and CClientMainPlayer.Inst.VehicleRO.DriverRO or nil
	if not self.m_IsInitMyIcon then
		local myPortrait = CUICommonDef.GetPortraitName(CClientMainPlayer.Inst.Class, CClientMainPlayer.Inst.Gender, -1)
		self.MyIcon:LoadNPCPortrait(myPortrait, false)
		self.m_IsInitMyIcon = true
	end
	self.m_IsInitFriendIcon = false
	if driverRO and CClientMainPlayer.Inst.RO then	
		local passengerList = CClientMainPlayer.Inst.VehicleRO.PassengerList
		for i = 0, passengerList.Count - 1 do
			local ro = passengerList[i]
			if ro ~= CClientMainPlayer.Inst.RO then
				local co = CClientObjectMgr.Inst:GetObject(ro)
				if co then
					local otherPlayer = TypeAs(co, typeof(CClientOtherPlayer))
					if otherPlayer then
						local friendClass, friendGender = otherPlayer.Class, otherPlayer.Gender
						local friendPortrait = CUICommonDef.GetPortraitName(friendClass, friendGender, -1)
						self.FriendIcon:LoadNPCPortrait(friendPortrait, false)
						self.m_IsInitFriendIcon = true
					end
				end
			end
		end
	end
end
