local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local UISlider = import "UISlider"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local Item_Item = import "L10.Game.Item_Item"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType2 = import "L10.UI.CItemInfoMgr+AlignType"

LuaTrainNpcFavorWnd = class()

RegistChildComponent(LuaTrainNpcFavorWnd, "NpcLeaveView", "NpcLeaveView", GameObject)
RegistChildComponent(LuaTrainNpcFavorWnd, "NpcInvitingView", "NpcInvitingView", GameObject)
RegistChildComponent(LuaTrainNpcFavorWnd, "Buttons", "Buttons", GameObject)
RegistChildComponent(LuaTrainNpcFavorWnd, "NpcJoinedView", "NpcJoinedView", GameObject)
RegistChildComponent(LuaTrainNpcFavorWnd, "NpcName", "NpcName", UILabel)
RegistChildComponent(LuaTrainNpcFavorWnd, "NpcIcon", "NpcIcon", CUITexture)
RegistChildComponent(LuaTrainNpcFavorWnd, "InceaseFavorBtn", "InceaseFavorBtn", GameObject)
RegistChildComponent(LuaTrainNpcFavorWnd, "BanishBtn", "BanishBtn", GameObject)
RegistChildComponent(LuaTrainNpcFavorWnd, "TipBtn", "TipBtn", GameObject)
RegistChildComponent(LuaTrainNpcFavorWnd, "LeaveReasonLabel", "LeaveReasonLabel", UILabel)
RegistChildComponent(LuaTrainNpcFavorWnd, "InviteOtherBtn", "InviteOtherBtn", GameObject)
RegistChildComponent(LuaTrainNpcFavorWnd, "TargtLabel", "TargtLabel", UILabel)
RegistChildComponent(LuaTrainNpcFavorWnd, "StatusLabel", "StatusLabel", UILabel)
RegistChildComponent(LuaTrainNpcFavorWnd, "HotBtn", "HotBtn", GameObject)
RegistChildComponent(LuaTrainNpcFavorWnd, "RestTimeLabel", "RestTimeLabel", UILabel)
RegistChildComponent(LuaTrainNpcFavorWnd, "InivatingFavorSlider", "InivatingFavorSlider", UISlider)
RegistChildComponent(LuaTrainNpcFavorWnd, "InvitingFavorLabel", "InvitingFavorLabel", UILabel)
RegistChildComponent(LuaTrainNpcFavorWnd, "FavorTexture", "FavorTexture", CUITexture)
RegistChildComponent(LuaTrainNpcFavorWnd, "CurFavorLabel", "CurFavorLabel", UILabel)
RegistChildComponent(LuaTrainNpcFavorWnd, "FavorDesLabel", "FavorDesLabel", UILabel)
RegistChildComponent(LuaTrainNpcFavorWnd, "InvitedFavorSlider", "InvitedFavorSlider", UISlider)
RegistChildComponent(LuaTrainNpcFavorWnd, "InvitedFavorLabel", "InvitedFavorLabel", UILabel)
RegistChildComponent(LuaTrainNpcFavorWnd, "DayDecreaseLabel", "DayDecreaseLabel", UILabel)
RegistChildComponent(LuaTrainNpcFavorWnd, "ShowBuffBtn", "ShowBuffBtn", GameObject)
RegistChildComponent(LuaTrainNpcFavorWnd, "ExchangeBtn", "ExchangeBtn", GameObject)
RegistChildComponent(LuaTrainNpcFavorWnd, "RewardButton", "RewardButton", GameObject)
RegistChildComponent(LuaTrainNpcFavorWnd, "RewardItemIconTexture", "RewardItemIconTexture", CUITexture)
RegistChildComponent(LuaTrainNpcFavorWnd, "RewardItemQualitySprite", "RewardItemQualitySprite", UISprite)
RegistChildComponent(LuaTrainNpcFavorWnd, "RewardItemCell", "RewardItemCell", GameObject)
RegistChildComponent(LuaTrainNpcFavorWnd, "RewardItemLabel", "RewardItemLabel", UILabel)

function LuaTrainNpcFavorWnd:Awake()
    UIEventListener.Get(self.TipBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        g_MessageMgr:ShowMessage("InviteNpc_AddFriendless_Tip")
    end)
    UIEventListener.Get(self.BanishBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnBanishBtnClick()
    end)
    UIEventListener.Get(self.InceaseFavorBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        if LuaInviteNpcMgr.MyFavorInfo and LuaInviteNpcMgr.MyFavorInfo.id then
            local npcId = LuaInviteNpcMgr.MyFavorInfo.id
            Gac2Gas.RequestAcceptSectNpcFriendTask(npcId)
        end
    end)
    UIEventListener.Get(self.ShowBuffBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnShowBuffBtnClick()
    end)
    UIEventListener.Get(self.ExchangeBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnExchangeBtnClick()
    end)
    UIEventListener.Get(self.RewardButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRewardButtonClick()
	end)
end

function LuaTrainNpcFavorWnd:Init()
    self.RewardButton.gameObject:SetActive(not LuaInviteNpcMgr.MyFavorInfo.isRewarded)
    local invite = SectInviteNpc_NPC.GetData(LuaInviteNpcMgr.MyFavorInfo.id)
    local npc = NPC_NPC.GetData(LuaInviteNpcMgr.MyFavorInfo.id)
    self.NpcName.text = LocalString.StrH2V(invite.Name,true)
    self.NpcIcon:LoadNPCPortrait(npc.Portrait)
    local sectInviteNpcData = SectInviteNpc_NPC.GetData(LuaInviteNpcMgr.MyFavorInfo.id)
    local data = Item_Item.GetData(sectInviteNpcData and sectInviteNpcData.DailyTaskRewardItemId or 0)
    if data then
        self.RewardItemIconTexture:LoadMaterial(data.Icon)
        self.RewardItemQualitySprite.spriteName = CUICommonDef.GetItemCellBorder(data, nil, false)
        UIEventListener.Get(self.RewardItemCell.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnRewardItemCellClick(sectInviteNpcData and sectInviteNpcData.DailyTaskRewardItemId or 0)
        end)
    end
    self.RewardItemCell.gameObject:SetActive(LuaInviteNpcMgr.TrainWndType ~= 0 and data)
    if LuaInviteNpcMgr.TrainWndType == 0 then
        self:InitNpcLeaveView()
    elseif LuaInviteNpcMgr.TrainWndType == 1 then
        self:InitNpcInvitingView()
    elseif LuaInviteNpcMgr.TrainWndType == 2 then
        self:InitNpcJoinedView()
    end
end

function LuaTrainNpcFavorWnd:InitNpcLeaveView()
    self.NpcLeaveView:SetActive(true)
    self.NpcInvitingView:SetActive(false)
    self.NpcJoinedView:SetActive(false)
    self.Buttons:SetActive(false)

    UIEventListener.Get(self.InviteOtherBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        LuaInviteNpcMgr.ShowInviteNpcWnd()
        CUIManager.CloseUI(CLuaUIResources.TrainNpcFavorWnd)
    end)
end

function LuaTrainNpcFavorWnd:InitNpcInvitingView()
    local info = LuaInviteNpcMgr.MyFavorInfo

    self.NpcLeaveView:SetActive(false)
    self.NpcInvitingView:SetActive(true)
    self.NpcJoinedView:SetActive(false)
    self.Buttons:SetActive(true)

    local btnLabel = self.InceaseFavorBtn.transform:Find("FavorBtnLabel"):GetComponent(typeof(UILabel))
    btnLabel.text = SafeStringFormat3(LocalString.GetString("增加青睐值(%d/%d)"),info.taskTimes,SectInviteNpc_Setting.GetData().FriendlinessTaskLimitPerDay)
    self.RewardItemLabel.text = info.taskTimes == SectInviteNpc_Setting.GetData().FriendlinessTaskLimitPerDay and LocalString.GetString("已获得") or LocalString.GetString("完成奖励")
    UIEventListener.Get(self.HotBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        local data = SectInviteNpc_NPC.GetData(info.id)
        LuaInviteNpcMgr.NpcInfo = {
            npcId = info.id,
            inviterCount = info.inviterCount,
            grade = data.Grade,
            name = data.Name,
            sceneTemplateId = data.PosInScene[0],
        }
        Gac2Gas.QuerySectInvitingNpcData(info.id)
    end)
    self.HotBtn.transform:GetComponent(typeof(UISprite)).spriteName = LuaInviteNpcMgr.GetHotSpriteName(info.inviterCount)

    local invite = SectInviteNpc_NPC.GetData(info.id)
    local grade = invite.Grade
    local level = SectInviteNpc_Setting.GetData().InviteLevelLimit
    local target = LuaInviteNpcMgr.GetFriendLinessByLevel(level,grade)
    self.TargtLabel.text = SafeStringFormat3(LocalString.GetString("青睐值达到%d"),target)
    local restTime = info.expireTime - CServerTimeMgr.Inst.timeStamp
    if restTime < 0 then restTime = 0 end
    local day = math.floor(restTime / (3600*24))
    local hour = math.floor(restTime % (3600*24) / 3600)
    if day == 0 and hour == 0 then
        local min = math.floor(restTime / 60) + 1
        self.RestTimeLabel.text = SafeStringFormat3(LocalString.GetString("剩余时间%d分钟"),min)
    else
        self.RestTimeLabel.text = SafeStringFormat3(LocalString.GetString("剩余时间%d天%d小时"),day,hour)
    end
    
    self.InivatingFavorSlider.value = info.friendliness / target
    self.InvitingFavorLabel.text = SafeStringFormat3("%d/%d",info.friendliness,target)
end

function LuaTrainNpcFavorWnd:InitNpcJoinedView()
    local info = LuaInviteNpcMgr.MyFavorInfo
    self.NpcLeaveView:SetActive(false)
    self.NpcInvitingView:SetActive(false)
    self.NpcJoinedView:SetActive(true)
    self.Buttons:SetActive(true)

    local btnLabel = self.InceaseFavorBtn.transform:Find("FavorBtnLabel"):GetComponent(typeof(UILabel))
    btnLabel.text = SafeStringFormat3(LocalString.GetString("青睐值培养(%d/%d)"),info.taskTimes,SectInviteNpc_Setting.GetData().FriendlinessTaskLimitPerDay)
    self.RewardItemLabel.text = info.taskTimes == SectInviteNpc_Setting.GetData().FriendlinessTaskLimitPerDay and LocalString.GetString("已获得") or LocalString.GetString("完成奖励")
    local invite = SectInviteNpc_NPC.GetData(info.id)
    self.CurFavorLabel.text = info.friendliness

    local target = LuaInviteNpcMgr.GetFriendLinessByLevel(LuaInviteNpcMgr.MaxFriendLinessLvel,invite.Grade)
    self.InvitedFavorSlider.value = info.friendliness / target
    self.InvitedFavorLabel.text = SafeStringFormat3("%d/%d",info.friendliness,target)
    local level = LuaInviteNpcMgr.GetLevelByfriendliness(info.friendliness,invite.Grade)
    local restTime = info.aboutLeaveTime - CServerTimeMgr.Inst.timeStamp
    local day = math.floor(restTime / (3600*24)) + 1
    local tip = SectInviteNpc_Level.GetData(level).Tips
    local next = 0
    if level < LuaInviteNpcMgr.MaxFriendLinessLvel then
        next = LuaInviteNpcMgr.GetFriendLinessByLevel(level+1,invite.Grade)
    end
    self.FavorDesLabel.text = SafeStringFormat3(tip,next,day)

    local decrease = SectInviteNpc_Level.GetData(level).MaintainFriendlinessPerDay
    self.DayDecreaseLabel.text = -decrease
    self.FavorTexture:LoadMaterial(SectInviteNpc_Level.GetData(level).Icon)
end

function LuaTrainNpcFavorWnd:OnBanishBtnClick()
    if LuaInviteNpcMgr.MyFavorInfo and LuaInviteNpcMgr.MyFavorInfo.id then
        local npcId = LuaInviteNpcMgr.MyFavorInfo.id
        local npcName = NPC_NPC.GetData(npcId).Name
        local msg = ""
        if LuaInviteNpcMgr.TrainWndType == 1 then
            msg = g_MessageMgr:FormatMessage("Sect_Banish_InvitingNpc", npcName)
        elseif LuaInviteNpcMgr.TrainWndType == 2 then
            msg = g_MessageMgr:FormatMessage("Sect_Banish_JoinedNpc", npcName)
        end
        MessageWndManager.ShowOKCancelMessage(msg, 
            DelegateFactory.Action(function ()
                Gac2Gas.RequestNpcLeaveSect(npcId)
                CUIManager.CloseUI(CLuaUIResources.TrainNpcFavorWnd)
            end),nil,
            LocalString.GetString("确定"), LocalString.GetString("取消"), false)
    end
end

function LuaTrainNpcFavorWnd:OnShowBuffBtnClick()
    local npcId = LuaInviteNpcMgr.MyFavorInfo.id
    local data = SectInviteNpc_NPC.GetData(npcId)
    LuaInviteNpcMgr:ShowNpcInvitedEnchantAbilityInfoWnd(data.EnchantID)
end

function LuaTrainNpcFavorWnd:OnExchangeBtnClick()
    Gac2Gas.RequestOpenExchangeLiuHeYeJingWnd()
end

function LuaTrainNpcFavorWnd:OnRewardButtonClick()
    self.RewardButton.gameObject:SetActive(false)
    Gac2Gas.RequestGetSectNpcInviteReward(LuaInviteNpcMgr.MyFavorInfo.id)
end

function LuaTrainNpcFavorWnd:OnRewardItemCellClick(templateId)
    CItemInfoMgr.ShowLinkItemTemplateInfo(templateId, false, nil, AlignType2.Default, 0, 0, 0, 0)
end
