-- Auto Generated!!
local CItemMgr = import "L10.Game.CItemMgr"
local CQingQiuGiftWnd = import "L10.UI.CQingQiuGiftWnd"
local CQingQiuMgr = import "L10.UI.CQingQiuMgr"
local TaoHuaBaoXia_GameSetting = import "L10.Game.TaoHuaBaoXia_GameSetting"
local TaoHuaBaoXia_JiangPin = import "L10.Game.TaoHuaBaoXia_JiangPin"
CQingQiuGiftWnd.m_Init_CS2LuaHook = function (this) 
    this.item = CItemMgr.Inst:GetById(CQingQiuMgr.Inst.m_CurGiftId)
    this.jiangping = TaoHuaBaoXia_JiangPin.GetData(this.item.TemplateId)
    if this.jiangping == nil then
        return
    end

    this.TitleLabel.text = this.jiangping.Title
    this.DesLabel.text = this.jiangping.Description
    this.m_IconTex:LoadMaterial(this.jiangping.Icon)
    local setting = TaoHuaBaoXia_GameSetting.GetData()

    this.PhoneInput.characterLimit = math.floor(setting.PhoneLenLimit / 4)
    this.AddrInput.characterLimit = math.floor(setting.AddrLenLimit / 4)
    this.NameInput.characterLimit = math.floor(setting.NameLenLimit / 4)
end
CQingQiuGiftWnd.m_onConfirmClick_CS2LuaHook = function (this, go) 

    local phone = ""
    local name = ""
    local addr = ""
    local setting = TaoHuaBaoXia_GameSetting.GetData()

    addr = this.AddrInput.value
    addr = StringTrim(addr)
    if CommonDefs.StringLength(addr) < math.floor(setting.AddrLenMin / 4) then
        g_MessageMgr:ShowMessage("ADDR_LENGTH_NOT_ENOUGH")
        return
    end

    phone = this.PhoneInput.value
    phone = StringTrim(phone)
    if CommonDefs.StringLength(phone) < math.floor(setting.PhoneLenMin / 4) then
        g_MessageMgr:ShowMessage("PHONE_LENGTH_NOT_ENOUGH")
        return
    end

    name = this.NameInput.value
    name = StringTrim(name)
    if System.String.IsNullOrEmpty(name) then
        g_MessageMgr:ShowMessage("NAME_CANNOT_BE_EMPTY")
        return
    end
    local log = System.String.Format("Name:{0};Phone{1};Address:{2}", name, phone, addr)
    Gac2Gas.TaoHuaBaoXiaLog(log)
end
