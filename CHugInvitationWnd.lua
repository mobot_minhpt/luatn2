-- Auto Generated!!
local BabyMgr = import "L10.Game.BabyMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CClientNpc = import "L10.Game.CClientNpc"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"
local CHugInvitationItem = import "L10.UI.CHugInvitationItem"
local CHugInvitationWnd = import "L10.UI.CHugInvitationWnd"
local CNearbyPlayerWndMgr = import "L10.UI.CNearbyPlayerWndMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local DelegateFactory = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local HugInvitationItemData = import "L10.UI.CHugInvitationWnd+HugInvitationItemData"
local Lua = import "L10.Engine.Lua"
local Object = import "System.Object"
local Quaternion = import "UnityEngine.Quaternion"
local UInt64 = import "System.UInt64"
local Vector3 = import "UnityEngine.Vector3"
CHugInvitationWnd.m_Init_CS2LuaHook = function (this) 
    this:ClearData()
    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer == nil then
        return
    end
    --show = Expression_Show.GetData(CExpressionActionMgr.InviteExpressionActionId);
    --if (show != null && show.InterfaceMessage != null)
    --{
    --    hintLabel.text = show.InterfaceMessage;
    --    this.RefreshData();
    --}
    this.hintLabel.text = CNearbyPlayerWndMgr.s_HintContent
    this:RefreshData()
end
CHugInvitationWnd.m_RefreshData_CS2LuaHook = function (this) 
    local hugInvitations = CreateFromClass(MakeGenericClass(List, HugInvitationItemData))
    local counter = 0
    if CNearbyPlayerWndMgr.s_BabyExpressionType == 0 then
        CommonDefs.DictIterate(CClientPlayerMgr.Inst.m_Id2VisiblePlayer, DelegateFactory.Action_object_object(function (___key, ___value) 
            local item = {}
            item.Key = ___key
            item.Value = ___value
            if counter >= CHugInvitationWnd.MAX_HUG_INVITATION then
                return
            end
            counter = counter + 1
            local invitation = CreateFromClass(HugInvitationItemData, item.Key, item.Value.Level, item.Value.Name, item.Value.PortraitName, CNearbyPlayerWndMgr.s_ButtonContent)
            CommonDefs.ListAdd(hugInvitations, typeof(HugInvitationItemData), invitation)
        end))
    else
        local objs = CClientObjectMgr.Inst:GetAllObjects()
        local table = Lua.Inst:GetLuaTable("LuaBabyMgr")
        if table["m_ExpressionBabyEngineId"] == nil then
            return
        end

        local expressionBabyEngineId = math.floor(tonumber(table["m_ExpressionBabyEngineId"] or 0))

        CommonDefs.EnumerableIterate(objs, DelegateFactory.Action_object(function (___value) 
            local item = ___value
            local continue
            repeat
                if counter >= CHugInvitationWnd.MAX_HUG_INVITATION then
                    return
                end
                if item.EngineId == expressionBabyEngineId then
                    continue = true
                    break
                end


                if TypeIs(item, typeof(CClientNpc)) then
                    local npc = TypeAs(item, typeof(CClientNpc))
                    if BabyMgr.Inst:IsBaby(npc.TemplateId) then
                        if BabyMgr.Inst:GetBabyShowStatus(npc) == CNearbyPlayerWndMgr.s_BabyExpressionType then
                            local invitation = CreateFromClass(HugInvitationItemData, item.EngineId, item.Level, item.Name, item.PortraitName, CNearbyPlayerWndMgr.s_ButtonContent)
                            CommonDefs.ListAdd(hugInvitations, typeof(HugInvitationItemData), invitation)
                            counter = counter + 1
                        end
                    end
                end
                continue = true
            until 1
            if not continue then
                return
            end
        end))
    end

    this:LoadData(hugInvitations)
end
CHugInvitationWnd.m_LoadData_CS2LuaHook = function (this, itemInfos) 
    this:ClearData()
    do
        local i = 0
        while i < itemInfos.Count do
            local instance = TypeAs(GameObject.Instantiate(this.itemTemplate, Vector3.zero, Quaternion.identity), typeof(GameObject))
            instance.transform.parent = this.table.transform
            instance.transform.localScale = Vector3.one
            local item = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CHugInvitationItem))
            item:Init(itemInfos[i].playerId, itemInfos[i].name, itemInfos[i].portraitName, itemInfos[i].level, itemInfos[i].buttonText)
            item.OnOpButtonClickDelegate = MakeDelegateFromCSFunction(this.DoHug, MakeGenericClass(Action1, UInt64), this)
            instance:SetActive(true)
            i = i + 1
        end
    end

    this.table:Reposition()
    this.scrollView:ResetPosition()
end
CHugInvitationWnd.m_DoHug_CS2LuaHook = function (this, playId) 
    if CNearbyPlayerWndMgr.s_BabyExpressionType == 0 then
        if CClientMainPlayer.Inst == nil then
            return
        end
        local targetToHug = CClientPlayerMgr.Inst:GetPlayer(playId)
        if targetToHug ~= nil then
            --CClientMainPlayer.Inst.Target = targetToHug;
            --//PlayerSettings.SetExpressionPlayed(CExpressionActionMgr.ExpressionActionID);
            --CExpressionActionMgr.Inst.DoExpressionAction();
            --CUIManager.CloseUI(CUIResources.HugInvitationWnd);
            if CNearbyPlayerWndMgr.s_ChooseAction ~= nil then
                GenericDelegateInvoke(CNearbyPlayerWndMgr.s_ChooseAction, Table2ArrayWithCount({playId}, 1, MakeArrayClass(Object)))
            end
            CUIManager.CloseUI(CUIResources.HugInvitationWnd)
        else
            g_MessageMgr:ShowMessage("HUG_TARGET_FARAWAY")
            this:RefreshData()
        end
    else
        if CNearbyPlayerWndMgr.s_ChooseAction ~= nil then
            GenericDelegateInvoke(CNearbyPlayerWndMgr.s_ChooseAction, Table2ArrayWithCount({playId}, 1, MakeArrayClass(Object)))
        end
        CUIManager.CloseUI(CUIResources.HugInvitationWnd)
    end
end

CNearbyPlayerWndMgr.m_ShowWnd_CS2LuaHook = function (hint, button, action, babyExpressionType) 
    CNearbyPlayerWndMgr.s_HintContent = hint
    CNearbyPlayerWndMgr.s_ButtonContent = button
    CNearbyPlayerWndMgr.s_ChooseAction = action
    CNearbyPlayerWndMgr.s_BabyExpressionType = babyExpressionType
    CUIManager.ShowUI(CUIResources.HugInvitationWnd)
end
