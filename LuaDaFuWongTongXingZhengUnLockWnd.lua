local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UITable = import "UITable"
local QnButton = import "L10.UI.QnButton"
local UILabel = import "UILabel"
local CPayMgr = import "L10.Game.CPayMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"
local Item_Item = import "L10.Game.Item_Item"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
LuaDaFuWongTongXingZhengUnLockWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaDaFuWongTongXingZhengUnLockWnd, "ItemCell", "ItemCell", GameObject)
RegistChildComponent(LuaDaFuWongTongXingZhengUnLockWnd, "Table", "Table", UITable)
RegistChildComponent(LuaDaFuWongTongXingZhengUnLockWnd, "BuyBtn", "BuyBtn", QnButton)
RegistChildComponent(LuaDaFuWongTongXingZhengUnLockWnd, "Title1", "Title1", GameObject)
RegistChildComponent(LuaDaFuWongTongXingZhengUnLockWnd, "Title2", "Title2", GameObject)
RegistChildComponent(LuaDaFuWongTongXingZhengUnLockWnd, "Title3", "Title3", GameObject)
RegistChildComponent(LuaDaFuWongTongXingZhengUnLockWnd, "PriceLabel", "PriceLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaDaFuWongTongXingZhengUnLockWnd, "m_ScrollView")
RegistClassMember(LuaDaFuWongTongXingZhengUnLockWnd, "m_Tick")
function LuaDaFuWongTongXingZhengUnLockWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.BuyBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBuyBtnClick()
	end)


    --@endregion EventBind end
	self.ItemCell.gameObject:SetActive(false)
	self.m_ScrollView = self.transform:Find("Anchor/QnTableView/ScrollView"):GetComponent(typeof(CUIRestrictScrollView))
	self.m_ScrollView.enabled = false
	self.m_Tick = nil
end

function LuaDaFuWongTongXingZhengUnLockWnd:Init()
	self.PriceLabel.text = DaFuWeng_Setting.GetData().VipOriPrice
	self.BuyBtn.transform:Find("Label"):GetComponent(typeof(UILabel)).text = DaFuWeng_Setting.GetData().VipBuyPrice
	self:InitTable()
	self.m_Tick = RegisterTickOnce(function() self.m_ScrollView.enabled = true end,1000)
end
function LuaDaFuWongTongXingZhengUnLockWnd:InitTable()
	Extensions.RemoveAllChildren(self.Table.transform)
	if not LuaDaFuWongMgr.TongXingZhengInfo then
		self.Title1.transform:Find("Label"):GetComponent(typeof(UILabel)).text = nil
		Gac2Gas.RequestDaFuWengPlayData()
	else
		self:InitFirstTiltleLabel()
	end
	self.Title1.transform:Find("AddNum"):GetComponent(typeof(UILabel)).text = DaFuWeng_Setting.GetData().VipAddValue
	self.Title1.gameObject:SetActive(true)
	self:InitTitle(self.Title2,DaFuWeng_Setting.GetData().UnLockVipShowAward1)
	self:InitTitle(self.Title3,DaFuWeng_Setting.GetData().UnLockVipShowAward2)
	self.Title1.transform:SetParent(self.Table.transform)
	self.Title2.transform:SetParent(self.Table.transform)
	self.Title3.transform:SetParent(self.Table.transform)
	self.Table:Reposition()
	self.Title2.transform:Find("Label"):GetComponent(typeof(UILabel)).text = g_MessageMgr:FormatMessage("DaFuWeng_TongXingZhengUnlock_Title2")
	self.Title3.transform:Find("Label"):GetComponent(typeof(UILabel)).text = g_MessageMgr:FormatMessage("DaFuWeng_TongXingZhengUnlock_Title3")
end

function LuaDaFuWongTongXingZhengUnLockWnd:InitTitle(title,data)
	if not title or not data then return end
	title.gameObject:SetActive(true)
	local tbl = title.transform:Find("Table"):GetComponent(typeof(UITable))
	Extensions.RemoveAllChildren(tbl.transform)
	for i = 0,data.Length - 1 do
		local go = CUICommonDef.AddChild(tbl.gameObject,self.ItemCell)
		go.gameObject:SetActive(true)
		self:InitItemCell(go,data[i])
	end
	tbl:Reposition()
end

function LuaDaFuWongTongXingZhengUnLockWnd:InitItemCell(go,itemdata)
    if not go or not itemdata then return end
    local texture = go.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    local ItemData = Item_Item.GetData(itemdata[0])
    
    local amountLabel = go.transform:Find("AmountLabel"):GetComponent(typeof(UILabel))
    amountLabel.gameObject:SetActive(itemdata[1] > 1)
    amountLabel.text = itemdata[1]
    if ItemData then texture:LoadMaterial(ItemData.Icon) end

    UIEventListener.Get(go.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemdata[0], false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
	end)
end

function LuaDaFuWongTongXingZhengUnLockWnd:InitFirstTiltleLabel()
	local curExp = LuaDaFuWongMgr.TongXingZhengInfo.progress
	local curLv = LuaDaFuWongMgr.TongXingZhengInfo.lv
	local addExp = DaFuWeng_Setting.GetData().VipAddValue
	local resExp = curExp + addExp
	local lv,exp = LuaDaFuWongMgr:GetTongXingZhengLevelAndProgress(resExp)
	self.Title1.transform:Find("Label"):GetComponent(typeof(UILabel)).text = g_MessageMgr:FormatMessage("DaFuWeng_UnLock_TongXingZheng_Title",lv-curLv)
end
--@region UIEvent

function LuaDaFuWongTongXingZhengUnLockWnd:OnBuyBtnClick()
	CPayMgr.Inst:BuyProduct(CPayMgr.Inst:PIDConverter(DaFuWeng_Setting.GetData().VipPID), 0)
end

--@endregion UIEvent

function LuaDaFuWongTongXingZhengUnLockWnd:OnEnable()
	g_ScriptEvent:AddListener("OnTongXingZhengDataUpdate",self,"InitFirstTiltleLabel")
end

function LuaDaFuWongTongXingZhengUnLockWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnTongXingZhengDataUpdate",self,"InitFirstTiltleLabel")
	if self.m_Tick then UnRegisterTick(self.m_Tick) self.m_Tick = nil end
end