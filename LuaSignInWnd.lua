require("3rdParty/ScriptEvent")
require("common/common_include")

local SignScrollView = import "L10.UI.SignScrollView"
local CSigninMgr = import "L10.Game.CSigninMgr"

LuaSignInWnd = class()


RegistChildComponent(LuaSignInWnd, "SignScrollView", SignScrollView)
RegistChildComponent(LuaSignInWnd, "DaShenButton", GameObject)
RegistChildComponent(LuaSignInWnd, "RemainRetroactiveLabel", UILabel)
RegistChildComponent(LuaSignInWnd, "DayCountHint", UILabel)
RegistChildComponent(LuaSignInWnd, "DayCountLabel", UILabel)
RegistChildComponent(LuaSignInWnd, "RetroactivButton", GameObject)

RegistClassMember(LuaSignInWnd, "MaxRetroactiveTimes")

function LuaSignInWnd:Init()
	
	self.DaShenButton:SetActive(LuaWelfareMgr.EnableDaShenEntry())

	self.MaxRetroactiveTimes = 5
	CommonDefs.AddOnClickListener(self.DaShenButton, DelegateFactory.Action_GameObject(function(go) self:OnDaShenButtonClick(go) end), false)
	CommonDefs.AddOnClickListener(self.RetroactivButton, DelegateFactory.Action_GameObject(function(go) self:OnRetroactivButtonClicked(go) end), false)
end


function LuaSignInWnd:OnEnable()
	Gac2Gas.QueryQianDaoInfo()
	g_ScriptEvent:AddListener("OnSigninInfoUpdate", self, "SigninInfoUpdate")
end


function LuaSignInWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnSigninInfoUpdate", self, "SigninInfoUpdate")

	if LuaWelfareMgr.SignInWndOpenContext == EnumBabyFeature.MainUI then
		CUIManager.ShowUI(CUIResources.WelfareWnd)
		LuaWelfareMgr.SignInWndOpenContext = nil
	end

	LuaWelfareMgr.CheckMainWelfare = false
end

function LuaSignInWnd:SigninInfoUpdate()
	self:InitInfo()
end

function LuaSignInWnd:InitInfo()
	local leftTimes = CSigninMgr.Inst.retroactiveTotalTimes - CSigninMgr.Inst.retroactiveUsedTimes
	self.RemainRetroactiveLabel.text = SafeStringFormat3(LocalString.GetString("剩余补签次数 %s"), tostring(leftTimes))
	self.DayCountHint.text = LocalString.GetString("本月累计签到")
	self.DayCountLabel.text = tostring(CSigninMgr.Inst.signedDays)
	self.SignScrollView:Init()
end

function LuaSignInWnd:OnDaShenButtonClick(go)
    LuaWelfareMgr.OpenDaShenWelfareWnd()
end

function LuaSignInWnd:OnRetroactivButtonClicked(go)
	local leftTimes = CSigninMgr.Inst.retroactiveTotalTimes - CSigninMgr.Inst.retroactiveUsedTimes
    if leftTimes == 0 then
        if CSigninMgr.Inst.retroactiveTotalTimes < self.MaxRetroactiveTimes then
            g_MessageMgr:ShowMessage("QIAN_DAO_NOT_EXTRA_TIMES_RECHARGE", self.MaxRetroactiveTimes)
            return
        else
            g_MessageMgr:ShowMessage("QIAN_DAO_NOT_EXTRA_TIMES", self.MaxRetroactiveTimes)
            return
        end
    end
    Gac2Gas.SignUpForQianDaoWithExtraTimes()
end

return LuaSignInWnd