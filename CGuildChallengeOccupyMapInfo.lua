-- Auto Generated!!
local CGuildChallengeMgr = import "L10.Game.CGuildChallengeMgr"
local CGuildChallengeOccupyMapInfo = import "L10.UI.CGuildChallengeOccupyMapInfo"
local LocalString = import "LocalString"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local UIWidget = import "UIWidget"
CGuildChallengeOccupyMapInfo.m_Init_CS2LuaHook = function (this, index) 
    this.info = CGuildChallengeMgr.Inst.occupationList[index]
    local mapIcon = this.transform:Find("MapTex"):GetComponent(typeof(UIWidget))
    mapIcon.enabled = this.info.competeScore < 100 and this.info.competeScore > 0
    this.redFlagIcon.enabled = this.info.competeScore == 0
    this.blueFlagIcon.enabled = this.info.competeScore == 100
    this.fenxian.text = System.String.Format(LocalString.GetString("分线{0}"), this.info.sceneIndex)
    this.blueProgress.value = this.info.competeScore / 100

    UIEventListener.Get(this.gameObject).onClick = MakeDelegateFromCSFunction(this.OnMapClick, VoidDelegate, this)
end
