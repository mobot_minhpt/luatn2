-- Auto Generated!!
local CGuildHorseRaceMainPlayerTemplate = import "L10.UI.CGuildHorseRaceMainPlayerTemplate"
local CGuildHorseRaceMgr = import "L10.Game.CGuildHorseRaceMgr"
local CGuildHorseRaceWatchMemberList = import "L10.UI.CGuildHorseRaceWatchMemberList"
local CommonDefs = import "L10.Game.CommonDefs"
local Extensions = import "Extensions"
local LocalString = import "LocalString"
local NGUITools = import "NGUITools"
local Object = import "System.Object"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CGuildHorseRaceWatchMemberList.m_Init_CS2LuaHook = function (this) 
    Extensions.RemoveAllChildren(this.grid.transform)
    this.memberTemplate:SetActive(false)
    CommonDefs.ListClear(this.playerList)
    this.groupLabel.text = System.String.Format(LocalString.GetString("{0}第{1}组"), " ", CGuildHorseRaceMgr.Inst.watchGroupInfo.groupIndex)
    do
        local i = 0
        while i < CGuildHorseRaceMgr.Inst.watchGroupInfo.playerList.Count do
            local info = CGuildHorseRaceMgr.Inst.watchGroupInfo.playerList[i]
            local instance = NGUITools.AddChild(this.grid.gameObject, this.memberTemplate)
            instance:SetActive(true)
            local template = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CGuildHorseRaceMainPlayerTemplate))
            if template ~= nil then
                template:Init(info)
                CommonDefs.ListAdd(this.playerList, typeof(CGuildHorseRaceMainPlayerTemplate), template)
                UIEventListener.Get(instance).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(instance).onClick, MakeDelegateFromCSFunction(this.OnPlayerSelect, VoidDelegate, this), true)
            end
            i = i + 1
        end
    end
    this.grid:Reposition()
    this.scrollview:ResetPosition()
end
CGuildHorseRaceWatchMemberList.m_UpdateWatchInfo_CS2LuaHook = function (this) 
    Extensions.RemoveAllChildren(this.grid.transform)
    CommonDefs.ListClear(this.playerList)
    this.groupLabel.text = System.String.Format(LocalString.GetString("{0}第{1}组"), " ", CGuildHorseRaceMgr.Inst.watchGroupInfo.groupIndex)
    do
        local i = 0
        while i < CGuildHorseRaceMgr.Inst.watchGroupInfo.playerList.Count do
            local info = CGuildHorseRaceMgr.Inst.watchGroupInfo.playerList[i]
            local instance = NGUITools.AddChild(this.grid.gameObject, this.memberTemplate)
            instance:SetActive(true)
            local template = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CGuildHorseRaceMainPlayerTemplate))
            if template ~= nil then
                template:Init(info)
                CommonDefs.ListAdd(this.playerList, typeof(CGuildHorseRaceMainPlayerTemplate), template)
                UIEventListener.Get(instance).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(instance).onClick, MakeDelegateFromCSFunction(this.OnPlayerSelect, VoidDelegate, this), true)
                if info.playerId == this.selectPlayerId then
                    template:SetSelect(true)
                end
            end
            i = i + 1
        end
    end
    this.grid:Reposition()
    this.scrollview:ResetPosition()
end
CGuildHorseRaceWatchMemberList.m_OnPlayerSelect_CS2LuaHook = function (this, go) 
    do
        local i = 0
        while i < this.playerList.Count do
            this.playerList[i]:SetSelect(go == this.playerList[i].gameObject)
            if go == this.playerList[i].gameObject then
                this.selectPlayerIndex = i
                this.selectPlayerId = this.playerList[i].info.playerId
                if this.OnPlayerToWatchSelect ~= nil then
                    GenericDelegateInvoke(this.OnPlayerToWatchSelect, Table2ArrayWithCount({this.selectPlayerId}, 1, MakeArrayClass(Object)))
                end
            end
            i = i + 1
        end
    end
end
