local CHousePopupMenu = import "L10.UI.CHousePopupMenu"
local GameObject = import "UnityEngine.GameObject"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local Physics = import "UnityEngine.Physics"
local Tags = import "L10.Game.Tags"
local GameObject = import "UnityEngine.GameObject"
local CClientObjectRoot=import "L10.Game.CClientObjectRoot"
local CMainCamera = import "L10.Engine.CMainCamera"

LuaMultipleFurnishPopupMenu = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaMultipleFurnishPopupMenu, "ArcButtons", "ArcButtons", CHousePopupMenu)
RegistChildComponent(LuaMultipleFurnishPopupMenu, "MoveBtn", "MoveBtn", GameObject)

--@endregion RegistChildComponent end

function LuaMultipleFurnishPopupMenu:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    UIEventListener.Get(self.MoveBtn).onPress = DelegateFactory.BoolDelegate(function(go,state)
        self.m_IsMoving = state
    end)
end

function LuaMultipleFurnishPopupMenu:Init()
    self.m_MultipleRoot = CLuaHouseMgr.GetMultipleRoot()
    if not self.m_MultipleRoot or not self.m_MultipleRoot.mChildList or self.m_MultipleRoot.mChildList.Count ==0 then
        CUIManager.CloseUI(CLuaUIResources.MultipleFurnishPopupMenu)
        return
    end
    self.m_MultipleRoot:InitTransformPosition()
    self.ArcButtons:Init()
end

function LuaMultipleFurnishPopupMenu:Update()
    if not self.m_IsMoving then
        return
    end
    
    if Input.GetMouseButton(0) then
        local ray = CMainCamera.Main:ScreenPointToRay(Input.mousePosition)
        local dist = CMainCamera.Main.farClipPlane - CMainCamera.Main.nearClipPlane
        local hits = Physics.RaycastAll(ray, dist, CMainCamera.Main.cullingMask)
        local pos
        local minDis = 0
        local fur = nil
        local hitDis = CMainCamera.Main.farClipPlane
        for i=0,hits.Length-1 do
            local hitData = hits[i]
            if hitData.collider:CompareTag(Tags.Ground) then
                if hitDis > hitData.distance then
                    hit = hitData
                    hitDis = hitData.distance
                end
            end
        end
    
        if hit then
            local y = CLuaHouseMgr.MultipleRoot.transform.position.y
            if self.m_MultipleRoot then
                self.m_MultipleRoot:SetRootPosition(Vector3(math.floor(hit.point.x)+0.5,y,math.floor(hit.point.z)+0.5),false)
            end
        end
    end

end


--@region UIEvent

--@endregion UIEvent

