local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"

LuaOffWorldPha1PicWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaOffWorldPha1PicWnd, "CloseButton", "CloseButton", GameObject)
RegistChildComponent(LuaOffWorldPha1PicWnd, "pic", "pic", GameObject)
RegistChildComponent(LuaOffWorldPha1PicWnd, "TipTextNode", "TipTextNode", GameObject)

--@endregion RegistChildComponent end

function LuaOffWorldPha1PicWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaOffWorldPha1PicWnd:Init()
  local onCloseClick = function(go)
    self:Close()
  end
  CommonDefs.AddOnClickListener(self.CloseButton,DelegateFactory.Action_GameObject(onCloseClick),false)

  local index = LuaOffWorldPassMgr.ShowPicIndex
  local data = OffWorldPass_ParallelWorldReel.GetData(index)
  self.TipTextNode:GetComponent(typeof(UILabel)).text = data.description
  self.pic:GetComponent(typeof(CUITexture)):LoadMaterial(data.picture)
end

function LuaOffWorldPha1PicWnd:Close()
  CUIManager.CloseUI(CLuaUIResources.OffWorldPha1PicWnd)
end
--@region UIEvent

--@endregion UIEvent
