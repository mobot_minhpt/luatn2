require("common/common_include")
local LuaGameObject=import "LuaGameObject"
local EPlayerFightProp=import "L10.Game.EPlayerFightProp"
local CTooltip=import "L10.UI.CTooltip"
local MessageMgr=import "L10.Game.MessageMgr"
local NGUITools=import "NGUITools"
local NGUIMath=import "NGUIMath"
local Color=import "UnityEngine.Color"
--仙凡属性 基础属性 详细属性
CLuaXianFanCompareDetailPropView=class()

RegistClassMember(CLuaXianFanCompareDetailPropView,"m_DataTable")--
RegistClassMember(CLuaXianFanCompareDetailPropView,"m_Table")
RegistClassMember(CLuaXianFanCompareDetailPropView,"m_ItemTemplate")
RegistClassMember(CLuaXianFanCompareDetailPropView,"m_SectionTemplate")
RegistClassMember(CLuaXianFanCompareDetailPropView,"m_FightProp")
RegistClassMember(CLuaXianFanCompareDetailPropView,"m_IsLeft")

function CLuaXianFanCompareDetailPropView:Init(tf,fightProp,left)
    self.m_IsLeft=left
    self.m_FightProp=fightProp
    self.m_ItemTemplate=LuaGameObject.GetChildNoGC(tf,"ItemTemplate").gameObject
    self.m_ItemTemplate:SetActive(false)
    self.m_SectionTemplate=LuaGameObject.GetChildNoGC(tf,"SectionTemplate").gameObject
    self.m_SectionTemplate:SetActive(false)

    self.m_DataTable={}
    self.m_Table=LuaGameObject.GetChildNoGC(tf,"Table").table
    
    -- local kt={
    --     {LocalString.GetString("当前经验"),0,"FightProp_Tips_Exp"},
    --     {LocalString.GetString("升级经验"),0,"FightProp_Tips_Exp_ShengJi"},
    --     {LocalString.GetString("修为"),0,"FightProp_Tips_XiuWei"},
    --     {LocalString.GetString("修炼"),0,"FightProp_Tips_XiuLian"},
    --     {LocalString.GetString("活力"),0,"FightProp_Tips_HuoLi"},
    --     {LocalString.GetString("PK值"),0,"FightProp_Tips_PK"},
    --     {LocalString.GetString("幸运值"),EPlayerFightProp.MF,"FightProp_Tips_MF"},
    -- }

    -- table.insert( self.m_DataTable,{LocalString.GetString("基础"),kt} )
    local kt2={
        {LocalString.GetString("当前气血"),EPlayerFightProp.Hp,"FightProp_Tips_Hp"},
        {LocalString.GetString("当前气血上限"),EPlayerFightProp.CurrentHpFull,"FightProp_Tips_CurrentHpFull"},
        {LocalString.GetString("最大气血上限"),EPlayerFightProp.HpFull,"FightProp_Tips_HpFull"},
        {LocalString.GetString("当前法力"),EPlayerFightProp.Mp,"FightProp_Tips_Mp"},
        {LocalString.GetString("当前法力上限"),EPlayerFightProp.CurrentMpFull,"FightProp_Tips_CurrentMpFull"},
        {LocalString.GetString("最大法力上限"),EPlayerFightProp.MpFull,"FightProp_Tips_MpFull"},
    }
    table.insert( self.m_DataTable,{LocalString.GetString("续航"),kt2} )
    
    local kt3={
        {LocalString.GetString("最小物攻"),EPlayerFightProp.pAttMin,"FightProp_Tips_pAttMin"},
        {LocalString.GetString("最大物攻"),EPlayerFightProp.pAttMax,"FightProp_Tips_pAttMax"},
        {LocalString.GetString("最小法攻"),EPlayerFightProp.mAttMin,"FightProp_Tips_mAttMin"},
        {LocalString.GetString("最大法攻"),EPlayerFightProp.mAttMax,"FightProp_Tips_mAttMax"},
        {LocalString.GetString("物攻速度"),EPlayerFightProp.pSpeed,"FightProp_Tips_pSpeed"},

        {LocalString.GetString("法攻速度"),EPlayerFightProp.mSpeed,"FightProp_Tips_mSpeed"},
        {LocalString.GetString("物理命中"),EPlayerFightProp.pHit,"FightProp_Tips_pHit"},
        {LocalString.GetString("法术命中"),EPlayerFightProp.mHit,"FightProp_Tips_mHit"},
        {LocalString.GetString("物理致命"),EPlayerFightProp.pFatal,"FightProp_Tips_pFatal"},
        {LocalString.GetString("物理致命伤害"),EPlayerFightProp.pFatalDamage,"FightProp_Tips_pFatalDamage"},

        {LocalString.GetString("法术致命"),EPlayerFightProp.mFatal,"FightProp_Tips_mFatal"},
        -- {LocalString.GetString("法术致命"),2087,"FightProp_Tips_mFatal"},
        {LocalString.GetString("法术致命伤害"),EPlayerFightProp.mFatalDamage,"FightProp_Tips_mFatalDamage"},
        {LocalString.GetString("破盾"),EPlayerFightProp.AntiBlock,"FightProp_Tips_AntiBlock"},
    }
    table.insert( self.m_DataTable,{LocalString.GetString("攻击"),kt3} )
    
    --防御 8项
    local kt4={
        {LocalString.GetString("物理防御"),EPlayerFightProp.pDef,"FightProp_Tips_pDef"},
        {LocalString.GetString("法术防御"),EPlayerFightProp.mDef,"FightProp_Tips_mDef"},
        {LocalString.GetString("物理躲避"),EPlayerFightProp.pMiss,"FightProp_Tips_pMiss"},
        {LocalString.GetString("法术躲避"),EPlayerFightProp.mMiss,"FightProp_Tips_mMiss"},
        {LocalString.GetString("格挡"),EPlayerFightProp.Block,"FightProp_Tips_Block"},
        {LocalString.GetString("格挡伤害减免"),EPlayerFightProp.BlockDamage,"FightProp_Tips_BlockDamage"},
        {LocalString.GetString("抗物理致命"),EPlayerFightProp.AntipFatal,"FightProp_Tips_AntipFatal"},
        {LocalString.GetString("抗法术致命"),EPlayerFightProp.AntimFatal,"FightProp_Tips_AntimFatal"},
        {LocalString.GetString("物理减伤比例"),EPlayerFightProp.MulpHurt,"FightProp_Tips_MulpHurt"},
        {LocalString.GetString("法术减伤比例"),EPlayerFightProp.MulmHurt,"FightProp_Tips_MulmHurt"},
        {LocalString.GetString("物理受伤减少"),EPlayerFightProp.AdjpHurt,"FightProp_Tips_AdjpHurt"},
        {LocalString.GetString("法术受伤减少"),EPlayerFightProp.AdjmHurt,"FightProp_Tips_AdjmHurt"},
    }
    table.insert( self.m_DataTable,{LocalString.GetString("防御"),kt4} )
    

    --抗性 26项
    local kt5={
        {LocalString.GetString("眩晕抗性"),EPlayerFightProp.AntiDizzy,"FightProp_Tips_AntiDizzy"},
        {LocalString.GetString("睡眠抗性"),EPlayerFightProp.AntiSleep,"FightProp_Tips_AntiSleep"},
        {LocalString.GetString("混乱抗性"),EPlayerFightProp.AntiChaos,"FightProp_Tips_AntiChaos"},
        {LocalString.GetString("定身抗性"),EPlayerFightProp.AntiBind,"FightProp_Tips_AntiBind"},
        {LocalString.GetString("沉默抗性"),EPlayerFightProp.AntiSilence,"FightProp_Tips_AntiSilence"},
        {LocalString.GetString("束缚抗性"),EPlayerFightProp.AntiTie,"FightProp_Tips_AntiTie"},
        {LocalString.GetString("石化抗性"),EPlayerFightProp.AntiPetrify,"FightProp_Tips_AntiPetrify"},
        {LocalString.GetString("变狐抗性"),EPlayerFightProp.AntiBianHu,"FightProp_Tips_AntiBianHu"},
        {LocalString.GetString("冰冻抗性"),EPlayerFightProp.AntiFreeze,"FightProp_Tips_AntiFreeze"},
        {LocalString.GetString("眩晕抵当"), EPlayerFightProp.DizzyTimeChange, "FightProp_Tips_DizzyTimeChange"},
        {LocalString.GetString("睡眠抵当"), EPlayerFightProp.SleepTimeChange, "FightProp_Tips_SleepTimeChange"},
        {LocalString.GetString("混乱抵当"), EPlayerFightProp.ChaosTimeChange, "FightProp_Tips_ChaosTimeChange"},
        {LocalString.GetString("定身抵当"), EPlayerFightProp.BindTimeChange, "FightProp_Tips_BindTimeChange"},
        {LocalString.GetString("沉默抵当"), EPlayerFightProp.SilenceTimeChange, "FightProp_Tips_SilenceTimeChange"},
        {LocalString.GetString("束缚抵当"), EPlayerFightProp.TieTimeChange, "FightProp_Tips_TieTimeChange"},
        {LocalString.GetString("石化抵当"), EPlayerFightProp.PetrifyTimeChange, "FightProp_Tips_PetrifyTimeChange"},
        {LocalString.GetString("火抗性"),EPlayerFightProp.AntiFire,"FightProp_Tips_AntiFire"},
        {LocalString.GetString("电抗性"),EPlayerFightProp.AntiThunder,"FightProp_Tips_AntiThunder"},
        {LocalString.GetString("风抗性"),EPlayerFightProp.AntiWind,"FightProp_Tips_AntiWind"},
        {LocalString.GetString("冰抗性"),EPlayerFightProp.AntiIce,"FightProp_Tips_AntiIce"},
        {LocalString.GetString("毒抗性"),EPlayerFightProp.AntiPoison,"FightProp_Tips_AntiPoison"},
        {LocalString.GetString("光抗性"),EPlayerFightProp.AntiLight,"FightProp_Tips_AntiLight"},
        {LocalString.GetString("幻抗性"),EPlayerFightProp.AntiIllusion,"FightProp_Tips_AntiIllusion"},
        {LocalString.GetString("水抗性"),EPlayerFightProp.AntiWater,"FightProp_Tips_AntiWater"},
        {LocalString.GetString("抗物理致命伤害"),EPlayerFightProp.AntipFatalDamage,"FightProp_Tips_AntipFatalDamage"},
        {LocalString.GetString("抗法术致命伤害"),EPlayerFightProp.AntimFatalDamage,"FightProp_Tips_AntimFatalDamage"},
    }
    table.insert( self.m_DataTable,{LocalString.GetString("抗性"),kt5} )
    
    local kt6={
        {LocalString.GetString("强眩晕"),EPlayerFightProp.EnhanceDizzy,"FightProp_Tips_EnhanceDizzy"},
        {LocalString.GetString("强睡眠"),EPlayerFightProp.EnhanceSleep,"FightProp_Tips_EnhanceSleep"},
        {LocalString.GetString("强混乱"),EPlayerFightProp.EnhanceChaos,"FightProp_Tips_EnhanceChaos"},
        {LocalString.GetString("强定身"),EPlayerFightProp.EnhanceBind,"FightProp_Tips_EnhanceBind"},
        {LocalString.GetString("强沉默"),EPlayerFightProp.EnhanceSilence,"FightProp_Tips_EnhanceSilence"},
        {LocalString.GetString("强束缚"),EPlayerFightProp.EnhanceTie,"FightProp_Tips_EnhanceTie"},
        {LocalString.GetString("强变狐"),EPlayerFightProp.EnhanceBianHu,"FightProp_Tips_EnhanceBianHu"},
        {LocalString.GetString("强石化"),EPlayerFightProp.EnhancePetrify,"FightProp_Tips_EnhancePetrify"},
    }        
    table.insert( self.m_DataTable,{LocalString.GetString("加强"),kt6} )
    
    local kt7={
        {LocalString.GetString("忽视火抗"),EPlayerFightProp.IgnoreAntiFire,"FightProp_Tips_IgnoreAntiFire"},
        {LocalString.GetString("忽视电抗"),EPlayerFightProp.IgnoreAntiThunder,"FightProp_Tips_IgnoreAntiThunder"},
        {LocalString.GetString("忽视风抗"),EPlayerFightProp.IgnoreAntiWind,"FightProp_Tips_IgnoreAntiWind"},
        {LocalString.GetString("忽视冰抗"),EPlayerFightProp.IgnoreAntiIce,"FightProp_Tips_IgnoreAntiIce"},
        {LocalString.GetString("忽视毒抗"),EPlayerFightProp.IgnoreAntiPoison,"FightProp_Tips_IgnoreAntiPoison"},

        {LocalString.GetString("忽视光抗"),EPlayerFightProp.IgnoreAntiLight,"FightProp_Tips_IgnoreAntiLight"},
        {LocalString.GetString("忽视幻抗"),EPlayerFightProp.IgnoreAntiIllusion,"FightProp_Tips_IgnoreAntiIllusion"},
        {LocalString.GetString("忽视水抗"),EPlayerFightProp.IgnoreAntiWater,"FightProp_Tips_IgnoreAntiWater"},
        {LocalString.GetString("忽视格挡伤害减免"),EPlayerFightProp.AntiBlockDamage,"FightProp_Tips_AntiBlockDamage"},
    }
    table.insert( self.m_DataTable,{LocalString.GetString("忽视"),kt7} )
    
    local kt8={
        {LocalString.GetString("隐身"),EPlayerFightProp.Invisible,"FightProp_Tips_Invisible"},
        {LocalString.GetString("反隐身"),EPlayerFightProp.TrueSight,"FightProp_Tips_TrueSight"},
    }
    table.insert( self.m_DataTable,{LocalString.GetString("其他"),kt8} )
    
    CUICommonDef.ClearTransform(self.m_Table.transform)
    local go=self.m_Table.gameObject
    for i,v in ipairs(self.m_DataTable) do
        local go=NGUITools.AddChild(go,self.m_SectionTemplate)
        go:SetActive(true)
        self:InitSection(go.transform,v[1],v[2])
    end
    self.m_Table:Reposition()
end
function CLuaXianFanCompareDetailPropView:InitSection(tf,sectionName,t)
    local sectionNameLabel=LuaGameObject.GetChildNoGC(tf,"SectionNameLabel").label
    local grid=LuaGameObject.GetChildNoGC(tf,"Grid").grid
    local gridGo=grid.gameObject
    sectionNameLabel.text=sectionName

    local color=Color(172/255,248/255,1,1)
    if self.m_IsLeft then
        color=Color(246/255,193/255,1,1)
    end
    for i,v in ipairs(t) do
        local go=NGUITools.AddChild(gridGo,self.m_ItemTemplate)
        go:SetActive(true)
        
        local nameLabel=LuaGameObject.GetChildNoGC(go.transform,"NameLabel").label
        local valueLabel=LuaGameObject.GetChildNoGC(go.transform,"ValueLabel").label
        nameLabel.text=v[1]
        nameLabel.color=color
        valueLabel.text=self:GetParam(v[2])--self:GetParam(Convert.ToUInt32(v[2]))
        
        CommonDefs.AddOnClickListener(nameLabel.gameObject,DelegateFactory.Action_GameObject(function(go)
            CTooltip.Show(MessageMgr.Inst:FormatMessage(v[3],{}), go.transform, CTooltip.AlignType.Top);
        end),false)
    end
    grid:Reposition()

    local bg=LuaGameObject.GetLuaGameObjectNoGC(tf).sprite
    local b = NGUIMath.CalculateRelativeWidgetBounds(bg.transform, grid.transform);
    bg.height = math.abs(b.min.y) + 20
end
function CLuaXianFanCompareDetailPropView:GetParam(type)
    local val=self.m_FightProp:GetParam(type)
    if type == EPlayerFightProp.mSpeed or type == EPlayerFightProp.pSpeed then
        if val<0.000001 then
            val=1
        end
        return SafeStringFormat3("%0.2f",val)
    elseif  type == EPlayerFightProp.pFatalDamage or type == EPlayerFightProp.mFatalDamage
         or type == EPlayerFightProp.AntipFatalDamage or type == EPlayerFightProp.AntimFatalDamage then
        return SafeStringFormat3("%0.1f",math.floor(val*1000)/10).."%"
    elseif type==EPlayerFightProp.MulpHurt or type==EPlayerFightProp.MulmHurt then
        val=-val
        if math.abs(val)<0.000001 then
            return "0"
        else
            return SafeStringFormat3( "%0.1f",val*100 ).."%"
        end
    end
    return tostring(math.floor(val))
end
return CLuaXianFanCompareDetailPropView
