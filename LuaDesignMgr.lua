--lua下面不允许使用策划表类的扩展属性，下面是一些包装函数

local CChatLinkMgr = import "CChatLinkMgr"
local CSkillMgr=import "L10.Game.CSkillMgr"
CLuaDesignMgr = {}

function CLuaDesignMgr.LifeSkill_Skill_GetFeiShengModifyLevel(skillId,playerLevel)
	local level = skillId % 100
    local rtn = 0
    for i = level, 1, - 1 do
        local data = LifeSkill_Skill.GetData(math.floor(skillId / 100) * 100 + i)
        if data ~= nil then
            if playerLevel >= data.GraLimit then
                rtn = i
                break
            end
        end
    end
    return rtn
end

function CLuaDesignMgr.Practice_Practice_GetFeiShengModifyLevel(skillId, playerLevel) 
	local level = skillId % 100
    local rtn = 0
    for i = level, 1, - 1 do
        local data = Practice_Practice.GetData(math.floor(skillId / 100) * 100 + i)
        if data ~= nil then
            if playerLevel >= data.PlayerLearnLv then
                rtn = i
                break
            end
        end
    end
    return rtn
end

function CLuaDesignMgr.Item_Item_GetAdjustedGradeCheck(this)
    if CClientMainPlayer.Inst == nil then
        return this.GradeCheck
    end
    local feishengData = CClientMainPlayer.Inst.PlayProp.FeiShengData
    if feishengData.FeiShengStage == 1 then
        return this.GradeCheckFS1
    else
        return this.GradeCheck
    end
end

--todo
function CLuaDesignMgr.Skill_AllSkills_GetAdjustedExperience(this)

end
function CLuaDesignMgr.Skill_AllSkills_GetAdjustedMoney(this)
end
function CLuaDesignMgr.Skill_AllSkills_GetAdjustedPreSkills(this)
end
function CLuaDesignMgr.Skill_AllSkills_GetAttackModeDisplay(this)
    if this.AttMode == "p" then
        return LocalString.GetString("物理攻击")
    elseif this.AttMode == "m" then
        return LocalString.GetString("法术攻击")
    else
        return LocalString.GetString("辅助攻击")
    end
end

function CLuaDesignMgr.Skill_AllSkills_GetCDString(this)

end
function CLuaDesignMgr.Skill_AllSkills_GetECategory(this)
end
function CLuaDesignMgr.Skill_AllSkills_GetEKind(this)
end

function CLuaDesignMgr.Skill_AllSkills_GetClass(skillId)
    return math.floor(skillId/100)
end
function CLuaDesignMgr.Skill_AllSkills_GetLevel(skillId)
    return skillId % 100
end
function CLuaDesignMgr.Skill_AllSkills_GetGetNextLevelSkillId(this)
end
function CLuaDesignMgr.Skill_AllSkills_GetSkillId(skillClassId,skillLevel)
    return skillClassId*100+skillLevel
end

function CLuaDesignMgr.Skill_AllSkills_IsOneLevelSkill(skillClass)
    return skillClass%100==0
end

function CLuaDesignMgr.Skill_AllSkills_GetSkillRange(this)
end
function CLuaDesignMgr.Skill_AllSkills_GetIsIdentifySkill(this)
    return CSkillMgr.Inst:IsIdentifySkill(math.floor(this.ID / 100))
end

function CLuaDesignMgr.Skill_AllSkills_GetIsPassiveSkill(this)
    return this.Category == 0--SkillCategory.Passive
end

function CLuaDesignMgr.Skill_AllSkills_GetScopeType(this)
end
function CLuaDesignMgr.Skill_AllSkills_GetSeriesIndex(this)
    return tonumber(this.SeriesName)
end
function CLuaDesignMgr.Skill_AllSkills_GetSkillClass(this)
    return math.floor(this.ID/100)
end

function CLuaDesignMgr.Skill_AllSkills_GetTranslatedDisplay(this)
    return CChatLinkMgr.TranslateToNGUIText(this.Display)
end