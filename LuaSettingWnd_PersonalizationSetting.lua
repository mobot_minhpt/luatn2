local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UICamera = import "UICamera"
local CChatLinkMgr = import "CChatLinkMgr"
local PlayerSettings = import "L10.Game.PlayerSettings"

LuaSettingWnd_PersonalizationSetting = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistClassMember(LuaSettingWnd_PersonalizationSetting, "m_OpenBtn")
RegistClassMember(LuaSettingWnd_PersonalizationSetting, "m_DescLabel")

--@endregion RegistChildComponent end

function LuaSettingWnd_PersonalizationSetting:Awake()
    self.m_OpenBtn = self.transform:Find("PersonalizationSetting/OpenBtn"):GetComponent(typeof(UISprite))
    self.m_DescLabel = self.transform:Find("PersonalizationSetting/DescLabel"):GetComponent(typeof(UILabel))
    UIEventListener.Get(self.m_OpenBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnOpenBtnClick()
    end)
    UIEventListener.Get(self.m_DescLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnDescLabelClick()
    end)
end

function LuaSettingWnd_PersonalizationSetting:SetOpenBtnSelected(isSeleted)
    self.m_OpenBtn.spriteName = isSeleted and "common_thumb_open" or "common_thumb_close"
end

--@region UIEvent
function LuaSettingWnd_PersonalizationSetting:OnOpenBtnClick()
    if PlayerSettings.ProductRecommendation then
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("PlayerSettings_ProductRecommendation_Close_Confirm"), function()
            PlayerSettings.ProductRecommendation = false
            self:SetOpenBtnSelected(false)
        end, nil, LocalString.GetString("确认关闭"), LocalString.GetString("我再想想"), false)
    else
        PlayerSettings.ProductRecommendation = true
        self:SetOpenBtnSelected(true)
    end
end

function LuaSettingWnd_PersonalizationSetting:OnDescLabelClick()
    local url = self.m_DescLabel:GetUrlAtPosition(UICamera.lastWorldPosition)
    if url then
        CChatLinkMgr.ProcessLinkClick(url, nil)
    end
end
--@endregion UIEvent

function LuaSettingWnd_PersonalizationSetting:OnEnable()
    self:SetOpenBtnSelected(PlayerSettings.ProductRecommendation)
end