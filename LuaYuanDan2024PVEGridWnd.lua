local GameObject = import "UnityEngine.GameObject"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"

LuaYuanDan2024PVEGridWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaYuanDan2024PVEGridWnd, "grid", "grid", GameObject)
RegistChildComponent(LuaYuanDan2024PVEGridWnd, "Template", "Template", GameObject)
RegistChildComponent(LuaYuanDan2024PVEGridWnd, "ExpandButton", "ExpandButton", GameObject)

--@endregion RegistChildComponent end

RegistClassMember(LuaYuanDan2024PVEGridWnd, "m_gridState")
RegistClassMember(LuaYuanDan2024PVEGridWnd, "m_gridGoList")

function LuaYuanDan2024PVEGridWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.ExpandButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnExpandButtonClick()
	end)


    --@endregion EventBind end
end

function LuaYuanDan2024PVEGridWnd:Init()
	--队伍初始进入在左上角，右下角固定为boss图标
	self.m_gridState = {false, false, false, false, false, false, false, false, false}
	self.m_gridGoList = {}
	for i = 1, 9 do
		local go = NGUITools.AddChild(self.grid, self.Template.gameObject)
		self.m_gridGoList[i] = go
		self:setGridGoState(go, i == 9, self.m_gridState[i])
	end
end

function LuaYuanDan2024PVEGridWnd:setGridGoState(go, isBoss, isReach)
	local Boss = go.transform:Find("Boss").gameObject
	local Reach = go.transform:Find("Reach").gameObject
	local NotReach = go.transform:Find("NotReach").gameObject
	Boss:SetActive(isBoss)
	Reach:SetActive(isReach)
	NotReach:SetActive(not isReach)
	go:SetActive(true)
end

function LuaYuanDan2024PVEGridWnd:onSyncYuanDanXNCGAreaInfo(areaInfo)
	local gridState = g_MessagePack.unpack(areaInfo)
	for i = 1, 8 do
		if self.m_gridState[i] ~= gridState[i] then
			self.m_gridState[i] = gridState[i]
			self:setGridGoState(self.m_gridGoList[i], false, self.m_gridState[i])
		end
	end
end

function LuaYuanDan2024PVEGridWnd:OnEnable()
	g_ScriptEvent:AddListener("SyncYuanDanXNCGAreaInfo", self, "onSyncYuanDanXNCGAreaInfo")
	g_ScriptEvent:AddListener("HideTopAndRightTipWnd",self,"OnHideTopAndRightTipWnd")
end

function LuaYuanDan2024PVEGridWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SyncYuanDanXNCGAreaInfo",self,"onSyncYuanDanXNCGAreaInfo")
	g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd",self,"OnHideTopAndRightTipWnd")
end

function LuaYuanDan2024PVEGridWnd:OnHideTopAndRightTipWnd()
	self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 180)
end

--@region UIEvent

function LuaYuanDan2024PVEGridWnd:OnExpandButtonClick()
	self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 0)
	CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end


--@endregion UIEvent

