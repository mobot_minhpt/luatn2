local CMainCamera = import "L10.Engine.CMainCamera"
local CClientObjectMgr=import "L10.Game.CClientObjectMgr"
local GameObject = import "UnityEngine.GameObject"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"

LuaHaiZhanPickUpWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaHaiZhanPickUpWnd, "PickUpBtn", "PickUpBtn", GameObject)
RegistChildComponent(LuaHaiZhanPickUpWnd, "ButtonLabel", "ButtonLabel", UILabel)

--@endregion RegistChildComponent end

function LuaHaiZhanPickUpWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    UIEventListener.Get(self.PickUpBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        Gac2Gas.RequestPick(LuaHaiZhanMgr.s_PickEngineId)
    end)

    self:Update()
end

function LuaHaiZhanPickUpWnd:Init()

end

--@region UIEvent

--@endregion UIEvent

function LuaHaiZhanPickUpWnd:Update()
    if LuaHaiZhanMgr.s_PickEngineId==0 then return end
    local obj = CClientObjectMgr.Inst:GetObject(LuaHaiZhanMgr.s_PickEngineId)
    if not obj then return end

    if obj.TemplateId == 37000363 then
        self.ButtonLabel.text = LocalString.GetString("修复")
    elseif obj.TemplateId == 37000364 then
        self.ButtonLabel.text = LocalString.GetString("灭火")
    end

    if not obj.RO.m_TopAnchorTransform then return end
    
    local pos = obj.RO.m_TopAnchorTransform.position
    -- local newpos = Vector3(pos.x,pos.y+1,pos.z)
    local viewPos = CMainCamera.Main:WorldToViewportPoint(pos)
    
    local screenPos
    if viewPos and viewPos.z and viewPos.z > 0 and viewPos.x > 0 and viewPos.x < 1 and viewPos.y > 0 and viewPos.y < 1 then
        screenPos = CMainCamera.Main:WorldToScreenPoint(pos)
    else  
        screenPos = CommonDefs.op_Multiply_Vector3_Single(Vector3.up, 3000)
    end
    screenPos.z = 0

    local topWorldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(screenPos)

    self.transform.position = topWorldPos
end
