--朱尔旦 接点心 LuaFallingDownTreasureWnd
local CUIFx = import "L10.UI.CUIFx"
local Screen=import "UnityEngine.Screen"

local Object = import "System.Object"

LuaZhuErDanJieDianXinWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaZhuErDanJieDianXinWnd, "Template1", "Template1", GameObject)
RegistChildComponent(LuaZhuErDanJieDianXinWnd, "Template2", "Template2", GameObject)
RegistChildComponent(LuaZhuErDanJieDianXinWnd, "CountDownLabel", "CountDownLabel", UILabel)
RegistChildComponent(LuaZhuErDanJieDianXinWnd, "ScoreLabel", "ScoreLabel", UILabel)
RegistChildComponent(LuaZhuErDanJieDianXinWnd, "Success", "Success", GameObject)
RegistChildComponent(LuaZhuErDanJieDianXinWnd, "Fail", "Fail", GameObject)
RegistChildComponent(LuaZhuErDanJieDianXinWnd, "ResultFx", "ResultFx", CUIFx)
RegistChildComponent(LuaZhuErDanJieDianXinWnd, "ToyRoot", "ToyRoot", GameObject)
RegistChildComponent(LuaZhuErDanJieDianXinWnd, "PlayerTexture", "PlayerTexture", CUITexture)
RegistChildComponent(LuaZhuErDanJieDianXinWnd, "LanZi", "LanZi", CUITexture)
RegistChildComponent(LuaZhuErDanJieDianXinWnd, "TempTexture", "TempTexture", GameObject)

--@endregion RegistChildComponent end

RegistClassMember(LuaZhuErDanJieDianXinWnd, "m_CountDownTick")
RegistClassMember(LuaZhuErDanJieDianXinWnd, "m_TimeCounter")
RegistClassMember(LuaZhuErDanJieDianXinWnd, "m_EmitIntervalTime")
RegistClassMember(LuaZhuErDanJieDianXinWnd, "m_EmitTimestamp")

RegistClassMember(LuaZhuErDanJieDianXinWnd, "m_TotalTime")
RegistClassMember(LuaZhuErDanJieDianXinWnd, "m_ToyPassScore")
RegistClassMember(LuaZhuErDanJieDianXinWnd, "m_GeneratePercent")
RegistClassMember(LuaZhuErDanJieDianXinWnd, "m_GenerateScore")
RegistClassMember(LuaZhuErDanJieDianXinWnd, "m_GameScore")

RegistClassMember(LuaZhuErDanJieDianXinWnd, "m_IsFinished")
RegistClassMember(LuaZhuErDanJieDianXinWnd, "m_Tick")

function LuaZhuErDanJieDianXinWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
	self.Template1:SetActive(false)
	self.Template2:SetActive(false)

	self.m_IsFinished = false
end

function LuaZhuErDanJieDianXinWnd:Init()
	self.m_EmitIntervalTime = 0.5
	self.m_EmitTimestamp = 0

	self.m_GameScore = 0

	local setting = ZhuJueJuQing_ZhuErDanSetting.GetData()
	self.m_TotalTime = setting.FallingToyTotalTime
	self.m_ToyPassScore = setting.FallingToyPassScore
	self.m_GeneratePercent = {setting.FallingToyGeneratePercent[0],setting.FallingToyGeneratePercent[1]}
	self.m_GenerateScore = {setting.FallingToyScore[0],setting.FallingToyScore[1]}


	self.ResultFx:DestroyFx()
	self:DestroyCountDownTick()
	self:BeginGame()
end

--@region UIEvent

--@endregion UIEvent

function LuaZhuErDanJieDianXinWnd:OnEnable()
	g_ScriptEvent:AddListener("ZhuErDanJieDianXinAddScore", self, "AddScore")
end

function LuaZhuErDanJieDianXinWnd:OnDisable()
	self:DestroyCountDownTick()
	g_ScriptEvent:RemoveListener("ZhuErDanJieDianXinAddScore", self, "AddScore")
end

function LuaZhuErDanJieDianXinWnd:AddScore(score)
	if self.m_Tick then UnRegisterTick(self.m_Tick) self.m_Tick=nil end
	if score<0 then
		self.PlayerTexture:LoadMaterial("UI/Texture/Transparent/Material/zhuerdandadishuwnd_tou_01.mat")
		self.LanZi:LoadMaterial("UI/Texture/Transparent/Material/zhuerdanjiedianxinwnd_normal.mat")
		self.m_Tick = RegisterTickOnce(function()
			--恢复
			self.PlayerTexture:LoadMaterial("UI/Texture/Transparent/Material/zhuerdandadishuwnd_tou_02.mat")
			self.m_Tick = nil
		end,1000)
	else
		self.PlayerTexture:LoadMaterial("UI/Texture/Transparent/Material/zhuerdandadishuwnd_tou_02.mat")
		self.LanZi:LoadMaterial("UI/Texture/Transparent/Material/zhuerdanjiedianxinwnd_hit.mat")
		self.m_Tick = RegisterTickOnce(function()
			self.LanZi:LoadMaterial("UI/Texture/Transparent/Material/zhuerdanjiedianxinwnd_normal.mat")
		end,1000)
	end
	self.m_GameScore = self.m_GameScore+score
	self.ScoreLabel.text = tostring(self.m_GameScore)

	if self.m_GameScore >= self.m_ToyPassScore then
		self:EndGame()
	end
end

function LuaZhuErDanJieDianXinWnd:ParseTimeText(totalSeconds)
    if totalSeconds < 0 then
        totalSeconds = 0
    end
    if totalSeconds >= 3600 then
        return SafeStringFormat3("%02d:%02d:%02d", math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
    else
        return SafeStringFormat3("%02d:%02d", math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end
function LuaZhuErDanJieDianXinWnd:DestroyCountDownTick()
	self.m_TimeCounter = 0
	if self.m_CountDownTick then
		UnRegisterTick(self.m_CountDownTick)
		self.m_CountDownTick = nil
	end
end

function LuaZhuErDanJieDianXinWnd:BeginGame()
	self.m_GameScore = 0

	-- 倒计时
	self:DestroyCountDownTick()
	self.m_CountDownTick = RegisterTick(function ()
		self.m_TimeCounter = self.m_TimeCounter + 1
		local leftTime = self.m_TotalTime - self.m_TimeCounter
		if leftTime < 0 then
			self:EndGame()
			self:DestroyCountDownTick()
		else
			self.CountDownLabel.text = self:ParseTimeText(leftTime)
		end
	end, 1000)

	self.PlayerTexture.gameObject:SetActive(true)
	-- self.FrozenPlayerTexture.gameObject:SetActive(false)
	CommonDefs.AddOnDragListener(self.PlayerTexture.gameObject, DelegateFactory.Action_GameObject_Vector2(function (go, delta)
        self:OnPlayerDrag(go, delta)
    end),false)

	-- LuaZhuJueJuQingMgr.m_FallingDownTreasureEnd = false
	self.Success:SetActive(false)
	self.Fail:SetActive(false)

	self:UpdateScore()
	
end
function LuaZhuErDanJieDianXinWnd:UpdateScore()
	self.ScoreLabel.text = tostring(self.m_GameScore)
	if self.m_GameScore >= self.m_ToyPassScore then
		self:EndGame()
	end
end

function LuaZhuErDanJieDianXinWnd:OnPlayerDrag(go, delta)
	local currentPos = UICamera.currentTouch.pos
	self.TempTexture.transform.position = UICamera.currentCamera:ScreenToWorldPoint(Vector3(currentPos.x, currentPos.y, 0))
	local x = Mathf.Clamp(self.TempTexture.transform.localPosition.x, -838, 838)
	self.PlayerTexture.transform.localPosition = Vector3(x, 59, 0)
end

function LuaZhuErDanJieDianXinWnd:Update()
	if self.m_IsFinished then return end
	self.m_EmitTimestamp = self.m_EmitTimestamp + Time.deltaTime
	if self.m_EmitTimestamp > self.m_EmitIntervalTime then
		self.m_EmitTimestamp = 0
		self:EmitToy()
	end
end

function LuaZhuErDanJieDianXinWnd:EmitToy()
	local pos = Vector3.zero
	pos = CUIManager.UIMainCamera:ScreenToWorldPoint(Vector3(math.random()*Screen.width, Screen.height, 0))
	local template, score = self:GetToyTemplate()
	local toy = GameObject.Instantiate(template, pos, self.ToyRoot.transform.rotation)
	if toy then
		if toy.transform:GetComponent(typeof(CCommonLuaScript)) then
			toy.transform:GetComponent(typeof(CCommonLuaScript)):Init({score})
		end
		toy.transform.parent = self.ToyRoot.transform
		toy.transform.localScale = Vector3.one
		toy:SetActive(true)
	end
end

function LuaZhuErDanJieDianXinWnd:GetToyTemplate()
	local random = math.random()
	if random < self.m_GeneratePercent[1] then
		return self.Template1, self.m_GenerateScore[1]
	else
		return self.Template2, self.m_GenerateScore[2]
	end
end

function LuaZhuErDanJieDianXinWnd:EndGame()
	self.m_IsFinished = true

	if self.m_GameScore >= self.m_ToyPassScore then
		self.Success:SetActive(true)
		self.ResultFx:LoadFx("Fx/UI/Prefab/UI_pengpengche_shengli.prefab")
		local empty = CreateFromClass(MakeGenericClass(List, Object))
		empty = MsgPackImpl.pack(empty)
		Gac2Gas.FinishClientTaskEvent(22120570,"ZhuErDan_JieDianXin",empty)

		g_ScriptEvent:BroadcastInLua("ZhuErDan_JieDianXin_Finish",true)

	else
		self.Fail:SetActive(true)
		self.ResultFx:LoadFx("Fx/UI/Prefab/UI_pengpengche_shibai.prefab")
		
		g_ScriptEvent:BroadcastInLua("ZhuErDan_JieDianXin_Finish",false)
	end
	RegisterTickOnce(function ()
		CUIManager.CloseUI(CLuaUIResources.ZhuErDanJieDianXinWnd)
	end, 5000)
end


function LuaZhuErDanJieDianXinWnd:OnDestroy()
    self:DestroyCountDownTick()
end