-- Auto Generated!!
local CItemMgr = import "L10.Game.CItemMgr"
local CLingShouItem = import "L10.UI.CLingShouItem"
local CLingShouMgr = import "L10.Game.CLingShouMgr"
local CLingShouMorphWnd = import "L10.UI.CLingShouMorphWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local DelegateFactory = import "DelegateFactory"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local Int32 = import "System.Int32"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CLingShouMorphWnd.m_Awake_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeBtn).onClick = MakeDelegateFromCSFunction(this.OnClickCloseButton, VoidDelegate, this)
    UIEventListener.Get(this.morphBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        --判断是否出战
        -- 
        if CLingShouMgr.Inst:IsOnBattle(CLingShouMgr.Inst.selectedLingShou) then
            g_MessageMgr:ShowMessage("LINGSHOU_CHUZHAN")
        else
            if CLingShouMorphWnd.selectedItem ~= nil then
                if CLingShouMorphWnd.selectedItem.Item.ExtraData.Data ~= nil and CLingShouMorphWnd.selectedItem.Item.ExtraData.Data.varbinary ~= nil then
                    local raw = CLingShouMorphWnd.selectedItem.Item.ExtraData.Data.varbinary.StringData
                    local pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, CLingShouMorphWnd.selectedItem.Id)
                    local selectedLingShou = CLingShouMgr.Inst.selectedLingShou
                    if selectedLingShou and selectedLingShou~="" then
                        Gac2Gas.SetLingShouPixiang(selectedLingShou, EnumItemPlace_lua.Bag, pos, CLingShouMorphWnd.selectedItem.Id)
                        CUIManager.CloseUI(CUIResources.LingShouMorphWnd)
                    end
                end
            end
        end
    end)
end
CLingShouMorphWnd.m_Start_CS2LuaHook = function (this) 
    this.gridView.m_DataSource = this
    this.gridView.OnSelectAtRow = MakeDelegateFromCSFunction(this.UpdateLingShou, MakeGenericClass(Action1, Int32), this)

    CLingShouMgr.Inst:RequestLingShouList()
end
CLingShouMorphWnd.m_UpdateLingShou_CS2LuaHook = function (this, row) 
    local list = CLingShouMgr.Inst:GetLingShouOverviewList()
    if row < list.Count then
        CLingShouMgr.Inst.selectedLingShou = list[row].id

        local lingShouId = list[row].id

        CLingShouMgr.Inst:RequestLingShouDetails(lingShouId)
    end
end
CLingShouMorphWnd.m_GetAllLingShouOverview_CS2LuaHook = function (this) 
    local list = CLingShouMgr.Inst:GetLingShouOverviewList()
    --初始化列表
    this.gridView:ReloadData(false, false)
    this.gridView:SetSelectRow(CLingShouMgr.Inst:GetBattleIndex(), true)
end
CLingShouMorphWnd.m_GetLingShouDetails_CS2LuaHook = function (this, lingShouId, details) 
    if lingShouId == CLingShouMgr.Inst.selectedLingShou then
        --更新界面
        if details ~= nil then
            this.textureLoaderFrom:Init(details)
            --根据易容丹的参数确定
            if CLingShouMorphWnd.selectedItem.Item.ExtraData.Data ~= nil and CLingShouMorphWnd.selectedItem.Item.ExtraData.Data.varbinary ~= nil then
                local raw = CLingShouMorphWnd.selectedItem.Item.ExtraData.Data.varbinary.StringData
                if not System.String.IsNullOrEmpty(raw) then
                    local splits = CommonDefs.StringSplit_ArrayChar(raw, ":")
                    local templateId = math.floor(tonumber(splits[0] or 0))
                    local evolveGrade = math.floor(tonumber(splits[1] or 0))
                    this.textureLoaderTo:Init(templateId, 0, evolveGrade)
                end
            end
        else
            this.textureLoaderFrom:InitNull()
        end
    end
end
CLingShouMorphWnd.m_ItemAt_CS2LuaHook = function (this, view, row) 
    local list = CLingShouMgr.Inst:GetLingShouOverviewList()

    if row < list.Count then
        local item = TypeAs(this.gridView:GetFromPool(0), typeof(CLingShouItem))
        if item ~= nil then
            item:Init(list[row])
            return item
        end
    end
    return nil
end
