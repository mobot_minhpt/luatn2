local Color             = import "UnityEngine.Color"
local CUITexture        = import "L10.UI.CUITexture"
local CUIManager        = import "L10.UI.CUIManager"
local UILabel           = import "UILabel"
local UITexture         = import "UITexture"
local SoundManager      = import "SoundManager"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CChatLinkMgr      = import "CChatLinkMgr"

CLuaBatDialogWnd = class()

RegistChildComponent(CLuaBatDialogWnd, "NpcName",		UILabel)
RegistChildComponent(CLuaBatDialogWnd, "Dialog",		UILabel)
RegistChildComponent(CLuaBatDialogWnd, "Icon",		    CUITexture)
RegistChildComponent(CLuaBatDialogWnd, "BgTx",		    UITexture)

RegistClassMember(CLuaBatDialogWnd, "delayTick")

CLuaBatDialogWnd.BatDialogID = -1
CLuaBatDialogWnd.PortraitName = nil
CLuaBatDialogWnd.Name = nil

--[[
    @desc: 显示对话框
    author:CodeGize
    time:2019-04-29 09:57:23
    --@id: batdialog id
    @return:
]]
function CLuaBatDialogWnd.ShowWnd(id)
    CLuaBatDialogWnd.BatDialogID = id
    if CUIManager.IsLoaded(CLuaUIResources.BatDialogWnd) then
        CUIManager.CloseUI(CLuaUIResources.BatDialogWnd)
    end
    CUIManager.ShowUI(CLuaUIResources.BatDialogWnd)
end

function CLuaBatDialogWnd:Awake()
    self.gameObject:GetComponent(typeof(UIPanel)).depth = 1030
end

function CLuaBatDialogWnd:Init()
    local key = CLuaBatDialogWnd.BatDialogID
    if key < 0 then return end
    
    local data = Dialog_BatDialog.GetData(key)
    if data == nil then return end
    if CLuaBatDialogWnd.Name and CLuaBatDialogWnd.PortraitName then
        self:Display(CLuaBatDialogWnd.Name, CLuaBatDialogWnd.PortraitName, data.Dialog, data.Type)
        CLuaBatDialogWnd.Name, CLuaBatDialogWnd.PortraitName = nil, nil
    elseif data.Type == 3 then
		local player = CClientMainPlayer.Inst
		self:Display(player.Name,player.PortraitName,data.Dialog,3)
	else
		self:Display(data.Name,data.Icon,data.Dialog,data.Type)
	end
    self:InitAudio(data.Audio)
    
    local func = function()
        CUIManager.CloseUI(CLuaUIResources.BatDialogWnd)
    end
    if self.delayTick then 
        UnRegisterTick(self.delayTick)
    end
    self.delayTick = RegisterTickOnce(func,data.Time * 1000)    
end

function CLuaBatDialogWnd:Display(name,icon,dialog,type)
    self.NpcName.text = name
    self.Icon:LoadNPCPortrait(icon,false)
    self.Dialog.text = CChatLinkMgr.TranslateToNGUIText(dialog)
    if type == 1 or type == 3 then--npc或者主角
        self.NpcName.color = Color(171.0/255.0,215.0/255.0,1.0)
        self.BgTx.color = Color(79.0/255.0,135.0/255.0,184.0/255.0,1.0)
    elseif type == 2 then--怪物
        self.NpcName.color = Color(1.0,105.0/255.0,80.0/255.0,1.0)
        self.BgTx.color = Color(184.0/255.0,79.0/255.0,105.0/255.0,1.0)
    end
end

function CLuaBatDialogWnd:InitAudio(audio)
    if System.String.IsNullOrEmpty(audio) then
        return
    end
    SoundManager.Inst:StartDialogSound(audio)
end

function CLuaBatDialogWnd:OnDisable()
	if self.delayTick then 
		UnRegisterTick(self.delayTick)
		self.delayTick = nil
    end
end


