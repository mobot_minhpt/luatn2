require("common/common_include")
local CCommonPlayerListMgr = import "L10.Game.CCommonPlayerListMgr"
local CCommonPlayerDisplayData = import "L10.Game.CCommonPlayerDisplayData"
local CIndirectUIResources = import "L10.UI.CIndirectUIResources"
local EnumCommonPlayerListUpdateType = import "L10.Game.EnumCommonPlayerListUpdateType"
local CIMMgr = import "L10.Game.CIMMgr"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local UIInput = import "UIInput"
local CUIFx = import "L10.UI.CUIFx"
local Vector3 = import "UnityEngine.Vector3"
local Utility = import "L10.Engine.Utility"

CLuaQiXiLiuXingQiYuanWnd = class()

RegistClassMember(CLuaQiXiLiuXingQiYuanWnd, "m_NameLabel")
RegistClassMember(CLuaQiXiLiuXingQiYuanWnd, "m_CostNumLabel")
RegistClassMember(CLuaQiXiLiuXingQiYuanWnd, "m_OwnNumLabel")
RegistClassMember(CLuaQiXiLiuXingQiYuanWnd, "m_StatusText")
RegistClassMember(CLuaQiXiLiuXingQiYuanWnd, "m_AddBtnObj")
RegistClassMember(CLuaQiXiLiuXingQiYuanWnd, "m_SelectBtnObj")
RegistClassMember(CLuaQiXiLiuXingQiYuanWnd, "m_StartBtnObj")
RegistClassMember(CLuaQiXiLiuXingQiYuanWnd, "m_LiuXingYuObj1")
RegistClassMember(CLuaQiXiLiuXingQiYuanWnd, "m_LiuXingYuObj2")
RegistClassMember(CLuaQiXiLiuXingQiYuanWnd, "m_LiuXingYuObj3")
RegistClassMember(CLuaQiXiLiuXingQiYuanWnd, "m_SelectIndex")
RegistClassMember(CLuaQiXiLiuXingQiYuanWnd, "m_SelectedTargetName")
RegistClassMember(CLuaQiXiLiuXingQiYuanWnd, "m_SelectedTargetId")
RegistClassMember(CLuaQiXiLiuXingQiYuanWnd, "m_Fx")

function CLuaQiXiLiuXingQiYuanWnd:Awake()
	self.m_NameLabel = self.transform:Find("Anchor/Name"):GetComponent(typeof(UILabel))
	self.m_CostNumLabel = self.transform:Find("Anchor/Cost/Num"):GetComponent(typeof(UILabel))
	self.m_OwnNumLabel = self.transform:Find("Anchor/Own/Num"):GetComponent(typeof(UILabel))
	self.m_StatusText = self.transform:Find("Anchor/StatusText"):GetComponent(typeof(UIInput))
	self.m_AddBtnObj = self.transform:Find("Anchor/Own/AddBtn").gameObject
	self.m_SelectBtnObj = self.transform:Find("Anchor/SelectBtn").gameObject
	self.m_StartBtnObj = self.transform:Find("Anchor/StartBtn").gameObject
	self.m_LiuXingYuObj1 = self.transform:Find("Anchor/LiuXingYu1").gameObject
	self.m_LiuXingYuObj2 = self.transform:Find("Anchor/LiuXingYu2").gameObject
	self.m_LiuXingYuObj3 = self.transform:Find("Anchor/LiuXingYu3").gameObject
	self.m_Fx = self.transform:Find("Fx"):GetComponent(typeof(CUIFx))
	self.m_Fx:LoadFx("Fx/UI/Prefab/UI_qixi_liuxingyu.prefab")

	local selectedObj = self.m_LiuXingYuObj1.transform:Find("Selected").gameObject
	selectedObj:SetActive(true)
	self.m_SelectIndex = 1

	CommonDefs.AddOnClickListener(self.m_SelectBtnObj, DelegateFactory.Action_GameObject(function(gameObject)
		self:OnSelectBtnClicked(gameObject)
	end), false)

	CommonDefs.AddOnClickListener(self.m_StartBtnObj, DelegateFactory.Action_GameObject(function(gameObject)
		self:OnStartBtnClicked(gameObject)
	end), false)

	CommonDefs.AddOnClickListener(self.m_AddBtnObj, DelegateFactory.Action_GameObject(function(gameObject)
		CShopMallMgr.ShowChargeWnd()
	end), false)

	for i = 1, 3 do
		local liuXingYuObj = self["m_LiuXingYuObj" .. i]

		local jadeNum = QiXi_QiXi2019.GetData().LiuXingYuNeedJade[i-1]
		local jadeNumLabel = liuXingYuObj.transform:Find("Num"):GetComponent(typeof(UILabel))
		jadeNumLabel.text = tostring(jadeNum)

		local liuXingYuName = QiXi_QiXi2019.GetData().LiuXingYuName[i-1]
		local liuXingYuNameLabel = liuXingYuObj.transform:Find("Label"):GetComponent(typeof(UILabel))
		liuXingYuNameLabel.text = liuXingYuName

		CommonDefs.AddOnClickListener(liuXingYuObj, DelegateFactory.Action_GameObject(function(gameObject)
			self:OnLiuXingYuBtnClicked(gameObject)
		end), false)
	end

	self.m_OwnNumLabel.text = tostring(CClientMainPlayer.Inst.Jade)
	local jadeNumLabel = self.m_LiuXingYuObj1.transform:Find("Num"):GetComponent(typeof(UILabel))
	self.m_CostNumLabel.text = jadeNumLabel.text

	self.m_SelectedTargetName = nil
	self.m_SelectedTargetId = nil

	local bg = self.transform:Find("Bg")
	bg.localScale = Vector3(0.95, 0.95, 1)
	LuaTweenUtils.DOScale(bg, Vector3(1, 1, 1), 0.5)
	
	local anchor = self.transform:Find("Anchor")
	anchor.localScale = Vector3(0.95, 0.95, 1)
	LuaTweenUtils.DOScale(anchor, Vector3(1, 1, 1), 0.5)
end

function CLuaQiXiLiuXingQiYuanWnd:OnLiuXingYuBtnClicked(gameObject)
	for i = 1, 3 do
		local liuXingYuObj = self["m_LiuXingYuObj" .. i]
		local selectedObj = liuXingYuObj.transform:Find("Selected").gameObject
		if liuXingYuObj == gameObject then
			selectedObj:SetActive(true)
			self.m_SelectIndex = i

			local jadeNumLabel = liuXingYuObj.transform:Find("Num"):GetComponent(typeof(UILabel))
			self.m_CostNumLabel.text = jadeNumLabel.text
		else
			selectedObj:SetActive(false)
		end
	end
end

function CLuaQiXiLiuXingQiYuanWnd:OnSelectBtnClicked(gameObject)
	if not CClientMainPlayer.Inst then
		return
	end

	CCommonPlayerListMgr.Inst.WndTitle = LocalString.GetString("选择祈愿对象")
	CCommonPlayerListMgr.Inst.ButtonText = LocalString.GetString("选择")
	CCommonPlayerListMgr.Inst.UpdateType = EnumCommonPlayerListUpdateType.Default
	CommonDefs.ListClear(CCommonPlayerListMgr.Inst.allData)

	-- 先加自己
	local player = CClientMainPlayer.Inst
	local data = CreateFromClass(CCommonPlayerDisplayData, player.Id, player.Name, player.Level, player.Class, player.Gender, player.BasicProp.Expression, true)
	CommonDefs.ListAdd(CCommonPlayerListMgr.Inst.allData, typeof(CCommonPlayerDisplayData), data)

	-- 再加在线好友
	local friendlinessTbl = {}
	CommonDefs.DictIterate(CClientMainPlayer.Inst.RelationshipProp.Friends, DelegateFactory.Action_object_object(function(k, v)
		if CIMMgr.Inst:IsOnline(k) and CIMMgr.Inst:IsSameServerFriend(k) then
			table.insert(friendlinessTbl, {k, v.Friendliness})
		end
	end))
	table.sort(friendlinessTbl, function(v1, v2) return v1[2] > v2[2] end)

	for _, info in pairs(friendlinessTbl) do
		local basicInfo = CIMMgr.Inst:GetBasicInfo(info[1])
		if basicInfo then
			local data = CreateFromClass(CCommonPlayerDisplayData, basicInfo.ID, basicInfo.Name, basicInfo.Level, basicInfo.Class, basicInfo.Gender, basicInfo.Expression, true)
			CommonDefs.ListAdd(CCommonPlayerListMgr.Inst.allData, typeof(CCommonPlayerDisplayData), data)
		end
	end

	CCommonPlayerListMgr.Inst.OnPlayerSelected = DelegateFactory.Action_ulong(function(playerId)
		if playerId == CClientMainPlayer.Inst.Id then
			self.m_NameLabel.text = CClientMainPlayer.Inst.Name
		else
			local basicInfo = CIMMgr.Inst:GetBasicInfo(playerId)
			if basicInfo then
				self.m_NameLabel.text = basicInfo.Name
			end
		end
		self.m_SelectedTargetName = self.m_NameLabel.text
		self.m_SelectedTargetId = playerId
		CUIManager.CloseUI(CIndirectUIResources.CommonPlayerListWnd)
	end)
	CUIManager.ShowUI(CIndirectUIResources.CommonPlayerListWnd)
end

function CLuaQiXiLiuXingQiYuanWnd:OnStartBtnClicked(gameObject)
	if not self.m_SelectedTargetName then
		g_MessageMgr:ShowMessage("QIXI2019_LIUXINGYU_NOT_SELECT_TARGET")
		return
	end

	local wishMsg = CUICommonDef.Trim(self.m_StatusText.value)
	if wishMsg == "" then
		g_MessageMgr:ShowMessage("QIXI2019_LIUXINGYU_NOT_INPUT_WISH")
		return
	end

	local ret = CWordFilterMgr.Inst:DoFilterOnSendWithoutVoiceCheck(wishMsg, false)
    if ret.msg == nil or ret.shouldBeIgnore then
		g_MessageMgr:ShowMessage("Speech_Violation")
		return
    end

    local needJade = tonumber(self.m_CostNumLabel.text)
	if not CShopMallMgr.TryCheckEnoughJade(needJade, 0) then
		return
	end

	local liuXingYuName = QiXi_QiXi2019.GetData().LiuXingYuName[self.m_SelectIndex-1]
	local message = g_MessageMgr:FormatMessage("QIXI2019_START_LIUXINGYU_CONFIRM_MSG" .. self.m_SelectIndex, needJade, self.m_SelectedTargetName)
	MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function()
		local msgArray = Utility.BreakString(wishMsg, 2, 255, nil)
		Gac2Gas.RequestStartQiXiLiuXingYu(self.m_SelectedTargetId, self.m_SelectIndex, self.m_SelectedTargetName, msgArray[0], msgArray[1])
	end), nil)
end

function CLuaQiXiLiuXingQiYuanWnd:UpdateJadeInfo()
	self.m_OwnNumLabel.text = tostring(CClientMainPlayer.Inst.Jade)
end

function CLuaQiXiLiuXingQiYuanWnd:OnEnable()
	g_ScriptEvent:AddListener("MainPlayerUpdateMoney", self, "UpdateJadeInfo")

	if CLuaQiXiMgr.m_InputTempText ~= "" then
		self.m_StatusText.value = CLuaQiXiMgr.m_InputTempText
	end
end

function CLuaQiXiLiuXingQiYuanWnd:OnDisable()
	g_ScriptEvent:RemoveListener("MainPlayerUpdateMoney", self, "UpdateJadeInfo")

	CLuaQiXiMgr.m_InputTempText = CUICommonDef.Trim(self.m_StatusText.value)
end
