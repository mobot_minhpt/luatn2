local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local UISprite = import "UISprite"
local QnTabView = import "L10.UI.QnTabView"

LuaQiShuAlert = class()

--@region RegistChildComponent: Dont Modify Manually!


--@endregion RegistChildComponent end
RegistClassMember(LuaQiShuAlert,"m_Sprite")
RegistClassMember(LuaQiShuAlert,"m_QnTabView")
RegistClassMember(LuaQiShuAlert,"m_IsAlert")
function LuaQiShuAlert:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.m_Sprite = self.transform:GetComponent(typeof(UISprite))

    if CClientHouseMgr.Inst.mCurFurnitureProp3 and CClientHouseMgr.Inst.mCurFurnitureProp3.IsFurnitureExpireNotify then
        self.m_IsAlert = CClientHouseMgr.Inst.mCurFurnitureProp3.IsFurnitureExpireNotify == 1
    else
        self.m_IsAlert = false
    end
    self.m_Sprite.enabled = self.m_IsAlert 

    self.m_QnTabView = self.transform.parent:GetComponent(typeof(QnTabView))
    if self.m_QnTabView then
        local tab = self.m_QnTabView.m_TabButtons[2]
        if tab then
            UIEventListener.Get(tab.gameObject).onClick = DelegateFactory.VoidDelegate(function (go) 
                if self.m_IsAlert  then
                    self.m_IsAlert = false
                    Gac2Gas.SetFurnitureExpireNotifyChecked()
                end
            end)
        end
    end
end

function LuaQiShuAlert:OnEnable()
    g_ScriptEvent:AddListener("RefreshFurnitureQishuAlert",self,"OnRefreshFurnitureQishuAlert")
end

function LuaQiShuAlert:OnDisable()
    g_ScriptEvent:RemoveListener("RefreshFurnitureQishuAlert",self,"OnRefreshFurnitureQishuAlert")
end

function LuaQiShuAlert:OnRefreshFurnitureQishuAlert(bShow)
    if self.m_Sprite then
        self.m_IsAlert  = bShow
        self.m_Sprite.enabled = bShow
    end
end
--@region UIEvent

--@endregion UIEvent

