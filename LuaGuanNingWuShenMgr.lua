
CLuaGuanNingWuShenMgr={}
CLuaGuanNingWuShenMgr.taskId=0
CLuaGuanNingWuShenMgr.needValue=0
CLuaGuanNingWuShenMgr.curValue=0
CLuaGuanNingWuShenMgr.bFinish =0
CLuaGuanNingWuShenMgr.awardScore =0

CLuaGuanNingWuShenMgr.s_ShowGuanNingChallengeTaskChoiceRemind=false


function CLuaGuanNingWuShenMgr.FormatNum(num)
    local n = tonumber(num)
    if not n then return nil end
    local val=math.floor( n )
    if val<1000000 then
        return tostring(val)
    else
        return tostring(math.floor(val/1000)/10)..LocalString.GetString("万")
    end
    return n
end
