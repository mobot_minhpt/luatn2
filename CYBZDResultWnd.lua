-- Auto Generated!!
local Constants = import "L10.Game.Constants"
local CYBZDResultItem = import "L10.UI.CYBZDResultItem"
local CYBZDResultWnd = import "L10.UI.CYBZDResultWnd"
local CZhongQiuMgr = import "L10.Game.CZhongQiuMgr"
local Extensions = import "Extensions"
local Gac2Gas = import "L10.Game.Gac2Gas"
CYBZDResultWnd.m_Init_CS2LuaHook = function (this) 
    this.m_AttackTeamName.text = this.ATTACK_TEAM_NAME
    this.m_DefenderTeamName.text = this.DEFENDER_TEAM_NAME
    Gac2Gas.QueryYBZDPlayInfo()
    this.m_AttackTable.m_DataSource = this
    this.m_DefenceTable.m_DataSource = this
    this:UpdateYBZDPlayInfo()
end
CYBZDResultWnd.m_UpdateYBZDPlayInfo_CS2LuaHook = function (this) 
    this.m_AttackScore.text = tostring(CZhongQiuMgr.Inst.YBZD_ATTACKER_SCORE)
    this.m_DefenceScore.text = tostring(CZhongQiuMgr.Inst.YBZD_DEFENDER_SCORE)

    if CZhongQiuMgr.Inst.YBZS_IS_END then
        -- Step 1 显示结果图片
        if CZhongQiuMgr.Inst.YBZD_ATTACKER_SCORE > CZhongQiuMgr.Inst.YBZD_DEFENDER_SCORE then
            this.m_AttackResult.gameObject:SetActive(true)
            this.m_AttackResult.spriteName = Constants.WinSprite
            this.m_AttackResultBG.gameObject:SetActive(true)
            Extensions.SetLocalPositionZ(this.m_AttackResultBG.transform, 0)

            this.m_DefenceResult.gameObject:SetActive(true)
            this.m_DefenceResult.spriteName = Constants.LoseSprite
            this.m_DefenceResultBG.gameObject:SetActive(true)
            Extensions.SetLocalPositionZ(this.m_DefenceResultBG.transform, - 1)
        elseif CZhongQiuMgr.Inst.YBZD_ATTACKER_SCORE < CZhongQiuMgr.Inst.YBZD_DEFENDER_SCORE then
            this.m_DefenceResult.gameObject:SetActive(true)
            this.m_DefenceResult.spriteName = Constants.WinSprite
            this.m_DefenceResultBG.gameObject:SetActive(true)
            Extensions.SetLocalPositionZ(this.m_DefenceResultBG.transform, 0)

            this.m_AttackResult.gameObject:SetActive(true)
            this.m_AttackResult.spriteName = Constants.LoseSprite
            this.m_AttackResultBG.gameObject:SetActive(true)
            Extensions.SetLocalPositionZ(this.m_AttackResultBG.transform, - 1)
        else
            this.m_DefenceResult.gameObject:SetActive(true)
            this.m_DefenceResult.spriteName = this.TIE_SPRITE_NAME
            this.m_DefenceResultBG.gameObject:SetActive(true)
            Extensions.SetLocalPositionZ(this.m_DefenceResultBG.transform, - 1)

            this.m_AttackResult.gameObject:SetActive(true)
            this.m_AttackResult.spriteName = this.TIE_SPRITE_NAME
            this.m_AttackResultBG.gameObject:SetActive(true)
            Extensions.SetLocalPositionZ(this.m_AttackResultBG.transform, - 1)
        end
    else
        this.m_DefenceResult.gameObject:SetActive(false)
        this.m_DefenceResultBG.gameObject:SetActive(false)
        this.m_AttackResult.gameObject:SetActive(false)
        this.m_AttackResultBG.gameObject:SetActive(false)
    end

    this.m_AttackTable:ReloadData(false, false)
    this.m_DefenceTable:ReloadData(false, false)
end
CYBZDResultWnd.m_ItemAt_CS2LuaHook = function (this, view, row) 
    if view == this.m_AttackTable then
        if CZhongQiuMgr.Inst.YBZDAttackers ~= nil and row < CZhongQiuMgr.Inst.YBZDAttackers.Count then
            local item = TypeAs(this.m_AttackTable:GetFromPool(0), typeof(CYBZDResultItem))
            local info = CZhongQiuMgr.Inst.YBZDAttackers[row]
            item:Init(info, row)
            return item
        end
    end
    if view == this.m_DefenceTable then
        if CZhongQiuMgr.Inst.YBZDDefenders ~= nil and row < CZhongQiuMgr.Inst.YBZDDefenders.Count then
            local item = TypeAs(this.m_DefenceTable:GetFromPool(0), typeof(CYBZDResultItem))
            local info = CZhongQiuMgr.Inst.YBZDDefenders[row]
            item:Init(info, row)
            return item
        end
    end
    return nil
end
CYBZDResultWnd.m_NumberOfRows_CS2LuaHook = function (this, view) 
    if view == this.m_AttackTable and CZhongQiuMgr.Inst.YBZDAttackers ~= nil then
        return CZhongQiuMgr.Inst.YBZDAttackers.Count
    end
    if view == this.m_DefenceTable and CZhongQiuMgr.Inst.YBZDDefenders ~= nil then
        return CZhongQiuMgr.Inst.YBZDDefenders.Count
    end
    return 0
end
