-- Auto Generated!!
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CCommonItem = import "L10.Game.CCommonItem"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CXianjiaUpgradeDetailView = import "L10.UI.CXianjiaUpgradeDetailView"
local CXianjiaUpgradeMgr = import "L10.UI.CXianjiaUpgradeMgr"
local DelegateFactory = import "DelegateFactory"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EnumQualityType = import "L10.Game.EnumQualityType"
local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"
local Gac2Gas = import "L10.Game.Gac2Gas"
local IdPartition = import "L10.Game.IdPartition"
local Int32 = import "System.Int32"
local MessageWndManager = import "L10.UI.MessageWndManager"
local QualityColor = import "L10.Game.QualityColor"
local Talisman_Setting = import "L10.Game.Talisman_Setting"
local Talisman_XianJia = import "L10.Game.Talisman_XianJia"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CXianjiaUpgradeDetailView.m_OnXianjiaSelect_CS2LuaHook = function (this, info) 

    if this.selectUpgradeTalisman == info then
        return
    end
    this.selectUpgradeTalisman = info
    this.iconTexture.material = nil
    this.qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(EnumQualityType.White)
    this.nameLabel.text = ""
    this.resultIcon.material = nil
    this.resultQuality.spriteName = CUICommonDef.GetItemCellBorder(EnumQualityType.White)
    this.resultName.text = ""
    this.leftRawMat:Init(nil)
    this.rightRawMat:Init(nil)
    CXianjiaUpgradeMgr.leftRawMatId = ""
    CXianjiaUpgradeMgr.rightRawMatId = ""
    this.moneyCtrl:SetCost(0)
    if info == nil then
        return
    end
    local talisman = CItemMgr.Inst:GetById(info.itemId)
    if IdPartition.IdIsTalisman(info.templateId) and talisman ~= nil and talisman.Equip.IsFairyTalisman then
        this.iconTexture:LoadMaterial(talisman.Icon)
        this.qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(talisman.Equip.QualityType)
        this.nameLabel.text = talisman.Name
        this.nameLabel.color = talisman.Equip.DisplayColor

        local nextLevelTemplateId = info.templateId + 1
        local xianjia = Talisman_XianJia.GetData(info.templateId)
        local nextXianjia = Talisman_XianJia.GetData(nextLevelTemplateId)
        this.leftRawMat.OnRawMatSelect = MakeDelegateFromCSFunction(this.OnLeftRawSelect, MakeGenericClass(Action1, CCommonItem), this)
        this.rightRawMat.OnRawMatSelect = MakeDelegateFromCSFunction(this.OnRightRawSelect, MakeGenericClass(Action1, CCommonItem), this)
        this.leftRawMat:Init(info)
        this.rightRawMat:Init(info)
        this.moneyCtrl:SetCost(CommonDefs.DictGetValue(Talisman_Setting.GetData().XianJiaUpgradeSetting, typeof(Int32), xianjia.Grade).costYinLiang)
        if xianjia ~= nil and nextXianjia ~= nil and xianjia.Position == nextXianjia.Position and xianjia.Grade + 1 == nextXianjia.Grade then
            this.nextTalisman = EquipmentTemplate_Equip.GetData(nextLevelTemplateId)
            this.resultIcon:LoadMaterial(this.nextTalisman.Icon)
            this.resultQuality.spriteName = CUICommonDef.GetItemCellBorder(CommonDefs.ConvertIntToEnum(typeof(EnumQualityType), this.nextTalisman.Color))
            this.resultName.text = this.nextTalisman.Name
            this.resultName.color = QualityColor.GetRGBValue(CommonDefs.ConvertIntToEnum(typeof(EnumQualityType), this.nextTalisman.Color))
        end
        UIEventListener.Get(this.resultGo).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.resultGo).onClick, MakeDelegateFromCSFunction(this.OnResultGoClick, VoidDelegate, this), true)
    end
end
CXianjiaUpgradeDetailView.m_OnMainRawMatClick_CS2LuaHook = function (this, go) 

    if this.selectUpgradeTalisman ~= nil then
        local item = CItemMgr.Inst:GetById(this.selectUpgradeTalisman.itemId)
        CItemInfoMgr.ShowLinkItemInfo(item, false, nil, AlignType.Default, 0, 0, 0, 0, 0)
    end
end
CXianjiaUpgradeDetailView.m_OnUpgradeButtonClick_CS2LuaHook = function (this, go) 

    if this.selectUpgradeTalisman == nil then
        g_MessageMgr:ShowMessage("Talisman_Upgrade_No_Target")
        return
    end
    local xianjia = Talisman_XianJia.GetData(this.selectUpgradeTalisman.templateId)
    if System.String.IsNullOrEmpty(CXianjiaUpgradeMgr.leftRawMatId) or System.String.IsNullOrEmpty(CXianjiaUpgradeMgr.rightRawMatId) then
        g_MessageMgr:ShowMessage("Talisman_Upgrade_No_Item")
    elseif CommonDefs.DictGetValue(Talisman_Setting.GetData().XianJiaUpgradeSetting, typeof(Int32), xianjia.Grade).costYinLiang > CClientMainPlayer.Inst.Silver then
        g_MessageMgr:ShowMessage("NotEnough_Silver")
    else
        local target = CItemMgr.Inst:GetById(this.selectUpgradeTalisman.itemId)
        local left = CItemMgr.Inst:GetById(CXianjiaUpgradeMgr.leftRawMatId)
        local right = CItemMgr.Inst:GetById(CXianjiaUpgradeMgr.rightRawMatId)
        local msgName = "Talisman_Upgrade_Confirm"
        --当至少1个原料仙家法宝有孔，而要升阶的仙家法宝没孔时，弹出二次确认提示，并在升阶后根据材料仙家法宝的打孔情况返还相关绑定的金刚钻原石
        if left.Equip.Hole > 0 or right.Equip.Hole > 0 then
            if target.Equip.Hole == 0 then
                msgName = "XianJia_FaBao_Update_OneHole_Confirm"
            else
                msgName = "XianJia_FaBao_Update_TwoHole_Confirm"
            end
        end
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage(msgName), DelegateFactory.Action(function () 
            local leftPos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, CXianjiaUpgradeMgr.leftRawMatId)
            local rightPos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, CXianjiaUpgradeMgr.rightRawMatId)
            Gac2Gas.RequestUpgradeXianJiaBegin()
            Gac2Gas.RequestUpgradeXianJiaPutIn(EnumToInt(this.selectUpgradeTalisman.place), this.selectUpgradeTalisman.pos, true)
            Gac2Gas.RequestUpgradeXianJiaPutIn(EnumItemPlace_lua.Bag, leftPos, false)
            Gac2Gas.RequestUpgradeXianJiaPutIn(EnumItemPlace_lua.Bag, rightPos, false)
            Gac2Gas.RequestUpgradeXianJiaEnd()
        end), nil, nil, nil, false)
    end
end
