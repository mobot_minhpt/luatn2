-- Auto Generated!!
local CDrugHMPItemCell = import "L10.UI.CDrugHMPItemCell"
local Color = import "UnityEngine.Color"
CDrugHMPItemCell.m_InitItemCell_CS2LuaHook = function (this, icon, amount) 
    this.Icon:LoadMaterial(icon)
    this.Amount.text = tostring(amount)
    if amount == 0 then
        this.Amount.color = Color.red
    else
        this.Amount.color = Color.green
    end
end
