require("common/common_include")

local UILabel = import "UILabel"
local UIScrollView = import "UIScrollView"
local LocalString = import "LocalString"
local Extensions = import "Extensions"
local CUICommonDef = import "L10.UI.CUICommonDef"
local UITable = import "UITable"

CLuaWorldCupXiaoZuSaiView=class()

RegistClassMember(CLuaWorldCupXiaoZuSaiView, "m_ScorllView")
RegistClassMember(CLuaWorldCupXiaoZuSaiView, "m_Table")
RegistClassMember(CLuaWorldCupXiaoZuSaiView, "m_GroupInfo")
RegistClassMember(CLuaWorldCupXiaoZuSaiView, "m_PlayInfo")

function CLuaWorldCupXiaoZuSaiView:Init(tf)
    self.m_ScorllView = tf.transform:Find("ScrollView"):GetComponent(typeof(UIScrollView))
    self.m_Table = tf.transform:Find("ScrollView/Table"):GetComponent(typeof(UITable))
    self.m_GroupInfo = tf.transform:Find("ScrollView/GroupInfo").gameObject
    self.m_PlayInfo = tf.transform:Find("ScrollView/GroupInfo/PlayList/PlayInfo").gameObject

    self.m_GroupInfo:SetActive(false)
    self.m_PlayInfo:SetActive(false)
end

function CLuaWorldCupXiaoZuSaiView:Refresh()
    -- 清空内容
    Extensions.RemoveAllChildren(self.m_Table.transform)

    local GroupNameTbl = {"A", "B", "C", "D", "E", "F", "G", "H",}
    for i=1, 8 do
        local go = CUICommonDef.AddChild(self.m_Table.gameObject, self.m_GroupInfo)
        go:SetActive(true)
        local GroupName = go.transform:Find("GroupName"):GetComponent(typeof(UILabel))
        GroupName.text = SafeStringFormat(LocalString.GetString("%s组赛事"), GroupNameTbl[i])
        local PlayList = go.transform:Find("PlayList").gameObject
        for j=1, 6 do
            local go2 = CUICommonDef.AddChild(PlayList, self.m_PlayInfo)
            go2:SetActive(true)

            local rid = CLuaWorldCupMgr.m_GroupPlayRoundTbl[i][j]
            local PlayInfo = CLuaWorldCupMgr.GetPlayInfoById(rid)
            local tid1 = PlayInfo.Team1
            local tid2 = PlayInfo.Team2

            local TeamInfo1 = CLuaWorldCupMgr.GetTeamInfoById(tid1)
            local TeamInfo2 = CLuaWorldCupMgr.GetTeamInfoById(tid2)

            local Country1Name = go2.transform:Find("Country1Name"):GetComponent(typeof(UILabel))
            local Country2Name = go2.transform:Find("Country2Name"):GetComponent(typeof(UILabel))
            local PlayTime = go2.transform:Find("PlayTime"):GetComponent(typeof(UILabel))
            PlayTime.text = LocalString.GetString(PlayInfo.Time)

            Country1Name.text = LocalString.GetString(TeamInfo1.Name)
            Country2Name.text = LocalString.GetString(TeamInfo2.Name)

            if tid1 == CLuaWorldCupMgr.m_HomeTeamId then
                Country1Name.color = Color.green
            end

            if tid2 == CLuaWorldCupMgr.m_HomeTeamId then
                Country2Name.color = Color.green
            end

            LuaUtils.SetLocalPosition(go2.transform, 0, 175-80*(j-1), 0)
        end
    end

    self.m_Table:Reposition()
    self.m_ScorllView:ResetPosition()
end

function CLuaWorldCupXiaoZuSaiView:OnSelected()
    self:Refresh()
end

function CLuaWorldCupXiaoZuSaiView:ReplyWorldCupTodayJingCaiRecord(data)
    self:Refresh(data)
end

return CLuaWorldCupXiaoZuSaiView
