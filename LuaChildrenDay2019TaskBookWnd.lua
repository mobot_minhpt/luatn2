local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CUIFx = import "L10.UI.CUIFx"
local CUITexture = import "L10.UI.CUITexture"
local UITexture = import "UITexture"
local Extensions = import "Extensions"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local GameObject = import "UnityEngine.GameObject"
local NGUITools = import "NGUITools"
local LocalString = import "LocalString"
local Vector3 = import "UnityEngine.Vector3"
local UILabel = import "UILabel"
local NGUIText = import "NGUIText"
local CItemMgr = import "L10.Game.CItemMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local NGUIMath = import "NGUIMath"
local CBabyModelTextureLoader = import "L10.UI.CBabyModelTextureLoader"

LuaChildrenDay2019TaskBookWnd=class()
RegistChildComponent(LuaChildrenDay2019TaskBookWnd,"closeBtn", GameObject)
RegistChildComponent(LuaChildrenDay2019TaskBookWnd,"addFNode", GameObject)
RegistChildComponent(LuaChildrenDay2019TaskBookWnd,"templateNode", GameObject)
RegistChildComponent(LuaChildrenDay2019TaskBookWnd,"slider", UISlider)
RegistChildComponent(LuaChildrenDay2019TaskBookWnd,"bonusNode", GameObject)
RegistChildComponent(LuaChildrenDay2019TaskBookWnd,"tipText", UILabel)
RegistChildComponent(LuaChildrenDay2019TaskBookWnd,"bonusFNode", GameObject)
RegistChildComponent(LuaChildrenDay2019TaskBookWnd,"scoreLabel", UILabel)
RegistChildComponent(LuaChildrenDay2019TaskBookWnd,"showTexture", UITexture)
RegistChildComponent(LuaChildrenDay2019TaskBookWnd,"babyTexture", CBabyModelTextureLoader)

--[[
  LuaChildrenDay2019Mgr.TaskTable = {}
  local taskInfo = list[2]
  for i = 0, taskInfo.Count - 1, 5 do
    local _table = {}
    _table.taskid = tonumber(taskInfo[i])
    _table.level = tonumber(taskInfo[i + 1])
    _table.value = tonumber(taskInfo[i + 2])
    _table.finished = tonumber(taskInfo[i + 3])
    _table.cansubmit = tonumber(taskInfo[i + 4])
    table.insert(LuaChildrenDay2019Mgr.TaskTable,_table)
  end

  LuaChildrenDay2019Mgr.RewardTable = {}
  local rewardInfo = list[3]
  for i = 0, rewardInfo.Count - 1, 3 do
    local _table = {}
    _table.rewardid = tonumber(rewardInfo[i])
    _table.recved = tonumber(rewardInfo[i + 1])
    _table.canRecv = tonumber(rewardInfo[i + 2])
    table.insert(LuaChildrenDay2019Mgr.RewardTable,_table)
  end
  --]]
--RegistClassMember(LuaChildrenDay2019TaskBookWnd, "m_")
function LuaChildrenDay2019TaskBookWnd:Split(str, pat)
   local t = {}  -- NOTE: use {n = 0} in Lua-5.0
   local fpat = "(.-)" .. pat
   local last_end = 1
   local s, e, cap = str:find(fpat, 1)
   while s do
      if s ~= 1 or cap ~= "" then
         table.insert(t,cap)
      end
      last_end = e+1
      s, e, cap = str:find(fpat, last_end)
   end
   if last_end <= #str then
      cap = str:sub(last_end)
      table.insert(t, cap)
   end
   return t
end

function LuaChildrenDay2019TaskBookWnd:RefreshTask(taskId,taskLevel)
  local itemTemplateId = LiuYi2019_TaskBookSetting.GetData().RefreshConsumeItemId
  local itemCNum = LiuYi2019_TaskBookSetting.GetData().RefreshConsumeCount

  local count = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, itemTemplateId)

  if count >= itemCNum then
      if taskLevel >= 4 then
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("LIUYI_TASKREF_ITEM"),
        DelegateFactory.Action(function ()
          Gac2Gas.LiuYiFamilyTimeRefreshTask(LuaChildrenDay2019Mgr.TaskItemId,taskId)
        end),nil, LocalString.GetString("刷新"), nil, false)
      else
        Gac2Gas.LiuYiFamilyTimeRefreshTask(LuaChildrenDay2019Mgr.TaskItemId,taskId)
      end
  else
    MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("LIUYI_TASKREF_YINLIANG"),
    DelegateFactory.Action(function ()
      Gac2Gas.LiuYiFamilyTimeRefreshTask(LuaChildrenDay2019Mgr.TaskItemId,taskId)
    end),nil, LocalString.GetString("刷新"), nil, false)
  end


end

function LuaChildrenDay2019TaskBookWnd:InitShowNode()
  Extensions.RemoveAllChildren(self.addFNode.transform)

  if not LuaChildrenDay2019Mgr.TaskInfoTable then
    return
  end

  if not CClientMainPlayer.Inst then
    return
  end

  self.scoreLabel.text = LuaChildrenDay2019Mgr.score

  local nodeWidth = 246
  local totalCount = #LuaChildrenDay2019Mgr.TaskInfoTable
  for i,v in ipairs(LuaChildrenDay2019Mgr.TaskInfoTable) do
    local node = NGUITools.AddChild(self.addFNode,self.templateNode)
    node:SetActive(true)
    node.transform.localPosition = Vector3(nodeWidth * (i - totalCount / 2 - 0.5),0,0)
    local taskId = v.taskid
    local taskLevel = v.level
    local taskInfo = LiuYi2019_TaskBook.GetData(taskId)
    if taskInfo then
      local npcIcon = taskInfo.Icon
      local totalNumTable = self:Split(taskInfo.LevelRequire,',')
      local levelScoreTable = self:Split(taskInfo.LevelScore,',')

      local totalNum = totalNumTable[taskLevel]
      local nowNum = v.value
      if tonumber(nowNum) >= tonumber(totalNum) then
        nowNum = totalNum
      end

      local comPercent = nowNum / totalNum
      node.transform:Find("icon/circle"):GetComponent(typeof(UITexture)).fillAmount = comPercent

      if tonumber(totalNum) > 10000 then
        totalNum = totalNum / 10000 .. LocalString.GetString('万')
      end

      if tonumber(nowNum) > 10000 then
        nowNum = nowNum / 10000 .. LocalString.GetString('万')
      end

      local numLabel = node.transform:Find("icon/num"):GetComponent(typeof(UILabel))
      numLabel.text = nowNum .. '/' .. totalNum

      local desc = SafeStringFormat3(taskInfo.Description,totalNum)
      local score = levelScoreTable[taskLevel]
      local descLabel = node.transform:Find("tipText"):GetComponent(typeof(UILabel))
      desc = string.gsub(desc,'#r','\n')
      descLabel.text = desc
      local detailLabelBtn = node.transform:Find("detailText").gameObject
      CommonDefs.AddOnClickListener(detailLabelBtn, DelegateFactory.Action_GameObject(function(go)
        g_MessageMgr:ShowMessage(taskInfo.GetPatchMessage)
      end), false)
      local detailLabel = detailLabelBtn:GetComponent(typeof(UILabel))

      local colorTable = {'c7701c','00a40f','519fff','ff5050','c000c0'}
      if colorTable[taskLevel] then
        local descColor = NGUIText.ParseColor24(colorTable[taskLevel], 0)
        descLabel.color = descColor
        detailLabel.color = descColor
        node.transform:Find("icon/circle"):GetComponent(typeof(UITexture)).color = descColor
      end
      node.transform:Find("bonusText"):GetComponent(typeof(UILabel)).text = '+' .. score
      local npcIconNode = node.transform:Find('icon/Mask/icon'):GetComponent(typeof(CUITexture))
      npcIconNode:LoadMaterial(npcIcon, false)

      local refreshBtn = node.transform:Find('refreshBtn').gameObject
      local refreshBtn2 = node.transform:Find('refreshBtn2').gameObject
      local submitBtn = node.transform:Find('submitBtn').gameObject
      local finishNode = node.transform:Find('finishNode').gameObject

      local bg = node.transform:Find('bg').gameObject
      local bgFinish = node.transform:Find('bgFinish').gameObject

      if v.finished == 1 then
        refreshBtn:SetActive(false)
        refreshBtn2:SetActive(false)
        submitBtn:SetActive(false)
        finishNode:SetActive(true)
        bg:SetActive(false)
        bgFinish:SetActive(true)
      elseif v.cansubmit == 1 then
        refreshBtn:SetActive(true)
        refreshBtn2:SetActive(false)
        submitBtn:SetActive(true)
        finishNode:SetActive(false)

        CommonDefs.AddOnClickListener(submitBtn, DelegateFactory.Action_GameObject(function(go)
          MessageWndManager.ShowOKCancelMessage(LocalString.GetString("#R每人只能完成5个亲子任务，提交后将无法刷新，请谨慎选择，#Y是否继续提交？#n"),
          DelegateFactory.Action(function ()
            Gac2Gas.LiuYiFamilyTimeFinishTask(LuaChildrenDay2019Mgr.TaskItemId,taskId)
          end), DelegateFactory.Action(function ()
            self:RefreshTask(taskId,taskLevel)
          end), LocalString.GetString("提交"), LocalString.GetString("刷新"), true)
        end), false)

        local fxNode = submitBtn.transform:Find('fx'):GetComponent(typeof(CUIFx))
        fxNode:DestroyFx()
        fxNode:LoadFx("Fx/UI/Prefab/UI_kuang_blue02.prefab")
        local path = {
          Vector3(fxNode.transform.localPosition.x - 50,fxNode.transform.localPosition.y,fxNode.transform.localPosition.z),
          Vector3(fxNode.transform.localPosition.x,fxNode.transform.localPosition.y + 42,fxNode.transform.localPosition.z),
          Vector3(fxNode.transform.localPosition.x + 50,fxNode.transform.localPosition.y,fxNode.transform.localPosition.z),
          Vector3(fxNode.transform.localPosition.x,fxNode.transform.localPosition.y - 42,fxNode.transform.localPosition.z),
          Vector3(fxNode.transform.localPosition.x - 50,fxNode.transform.localPosition.y,fxNode.transform.localPosition.z),
        }
        local m_FxPath = Table2Array(path, MakeArrayClass(Vector3))
        LuaTweenUtils.DODefaultLocalPath(fxNode.transform, m_FxPath , 2)

        CommonDefs.AddOnClickListener(refreshBtn, DelegateFactory.Action_GameObject(function(go)
          self:RefreshTask(taskId,taskLevel)
        end), false)
        bg:SetActive(true)
        bgFinish:SetActive(false)
      else
        refreshBtn:SetActive(false)
        refreshBtn2:SetActive(true)
        submitBtn:SetActive(false)
        finishNode:SetActive(false)
        CommonDefs.AddOnClickListener(refreshBtn2, DelegateFactory.Action_GameObject(function(go)
          self:RefreshTask(taskId,taskLevel)
        end), false)
        bg:SetActive(true)
        bgFinish:SetActive(false)
      end
    end
  end
end

function LuaChildrenDay2019TaskBookWnd:InitBonusNode()
  Extensions.RemoveAllChildren(self.bonusFNode.transform)
  if LuaChildrenDay2019Mgr.TaskRewardTable then
    local maxData = LiuYi2019_TaskBookReward.GetData(LiuYi2019_TaskBookReward.GetDataCount())
    local maxValue = maxData.Score

    local totalCount = 0
    local maxCount = 0
    for i,v in ipairs(LuaChildrenDay2019Mgr.TaskRewardTable) do
      totalCount = totalCount + 1
      if v.recved == 1 or v.canRecv == 1 then
        maxCount = totalCount
      end
    end
    totalCount = totalCount - 1
    maxCount = maxCount - 1
    if maxCount < 0 then
      maxCount = 0
    end

    self.slider.value = maxCount / totalCount
    local maxLength = 540
    for i,v in pairs(LuaChildrenDay2019Mgr.TaskRewardTable) do
      local data = LiuYi2019_TaskBookReward.GetData(v.rewardid)
      if data then
        local node = NGUITools.AddChild(self.bonusFNode,self.bonusNode)
        node:SetActive(true)

        local closeNode = node.transform:Find("close").gameObject
        local openNode = node.transform:Find("open").gameObject

        --if v.canRecv == 1 then
          --node.transform:Find("close").gameObject:SetActive(false)
          --node.transform:Find("open").gameObject:SetActive(true)
        if v.recved == 1 then
          closeNode:SetActive(false)
          openNode:SetActive(true)
        else
          closeNode:SetActive(true)
          openNode:SetActive(false)
          local itemId = data.ItemID
          local btn = closeNode
          local b1 = NGUIMath.CalculateRelativeWidgetBounds(btn.transform)
          local height = b1.size.y
          local width = b1.size.x

          CommonDefs.AddOnClickListener(btn, DelegateFactory.Action_GameObject(function(go)
            --CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, CItemInfoMgr.AlignType.Top, btn.transform.position.x, btn.transform.position.y, width, height)
            CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, CItemInfoMgr.AlignType.Default, 0,0,0,0)
          end), false)
        end
        node.transform.localPosition = Vector3(0,maxLength * (i-1) / totalCount, 0)
        node.transform:Find("num"):GetComponent(typeof(UILabel)).text = data.Score
      end
    end
  end
end

function LuaChildrenDay2019TaskBookWnd:Init()
  self.templateNode:SetActive(false)
  self.bonusNode:SetActive(false)
  self:InitShowNode()
  self:InitBonusNode()

  if LuaChildrenDay2019Mgr.ShowBabyInfo then
    local babyAppearance = LuaBabyMgr.GetBabyAppearance(
    LuaChildrenDay2019Mgr.ShowBabyInfo.status,
    LuaChildrenDay2019Mgr.ShowBabyInfo.gender,
    LuaChildrenDay2019Mgr.ShowBabyInfo.hairColor,
    LuaChildrenDay2019Mgr.ShowBabyInfo.skinColor,
    LuaChildrenDay2019Mgr.ShowBabyInfo.hairstyle,
    LuaChildrenDay2019Mgr.ShowBabyInfo.headId,
    LuaChildrenDay2019Mgr.ShowBabyInfo.bodyId,
    LuaChildrenDay2019Mgr.ShowBabyInfo.colorId,
    LuaChildrenDay2019Mgr.ShowBabyInfo.wingId,
    LuaChildrenDay2019Mgr.ShowBabyInfo.qiChangId)
    self.babyTexture:Init(babyAppearance,-180,0,0)
  else
    local defaultBabyAppearance = LiuYi2019_TaskBookSetting.GetData().BabyAppearance
    local appearanceTable = self:Split(defaultBabyAppearance,';')
    for i,v in pairs(appearanceTable) do
      appearanceTable[i] = tonumber(v)
    end

    local babyAppearance = LuaBabyMgr.GetBabyAppearance(appearanceTable[1],appearanceTable[2],appearanceTable[3],
    appearanceTable[4],appearanceTable[5],appearanceTable[6],appearanceTable[7],
    appearanceTable[8],appearanceTable[9],appearanceTable[10],appearanceTable[11])
    self.babyTexture:Init(babyAppearance,-180,0,0)
  end
end

function LuaChildrenDay2019TaskBookWnd:OnDestroy()

end

return LuaChildrenDay2019TaskBookWnd
