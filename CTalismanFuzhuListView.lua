-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CTalismanFuzhuListView = import "L10.UI.CTalismanFuzhuListView"
local CTalismanMenuMgr = import "L10.UI.CTalismanMenuMgr"
local Extensions = import "Extensions"
local NGUITools = import "NGUITools"
local Object = import "System.Object"
local Talisman_Setting = import "L10.Game.Talisman_Setting"
local UILabel = import "UILabel"
CTalismanFuzhuListView.m_Init_CS2LuaHook = function (this, index) 
    this:InitDesc()
    this.selectSheetIndex = index
    this.sheet2Lock.enabled = not CTalismanMenuMgr.talismanMenu2Open
    this.sheet1:SetSelected(index == 1, false)
    this.sheet2:SetSelected(index == 2, false)
    this.sheet3:SetSelected(index == 3, false)
    this.sheet3.gameObject:SetActive(CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.MultipleColumTalismanNum  >= 3)
    if this.OnSheetSelect ~= nil then
        GenericDelegateInvoke(this.OnSheetSelect, Table2ArrayWithCount({index}, 1, MakeArrayClass(Object)))
    end
end
CTalismanFuzhuListView.m_InitDesc_CS2LuaHook = function (this) 
    Extensions.RemoveAllChildren(this.descTable.transform)
    this.descTemplate:SetActive(false)
    local list = Talisman_Setting.GetData().Talisman_ZhiHuan_Info
    do
        local i = 0
        while i < list.Length do
            this:AddDesc(list[i])
            i = i + 1
        end
    end
    this.descTable:Reposition()
end
CTalismanFuzhuListView.m_AddDesc_CS2LuaHook = function (this, message) 
    local instance = NGUITools.AddChild(this.descTable.gameObject, this.descTemplate)
    instance:SetActive(true)
    local label = CommonDefs.GetComponentInChildren_GameObject_Type(instance, typeof(UILabel))
    if label ~= nil then
        label.text = message
    end
end
CTalismanFuzhuListView.m_OnSheet2Click_CS2LuaHook = function (this, button) 
    if CTalismanMenuMgr.talismanMenu2Open then
        this:Init(2)
    else
        if CClientMainPlayer.Inst ~= nil then
            g_MessageMgr:ShowMessage("Open_FaBao_ZuHe2_Fail_Tips", Talisman_Setting.GetData().SecondaryTalismanXiuWei, CClientMainPlayer.Inst.BasicProp.XiuWeiGrade)
        end
        this.sheet2:SetSelected(false, false)
    end
end
