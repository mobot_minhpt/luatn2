local CFurnitureColorType=import "L10.Game.CFurnitureColorType"
local CSwitchMgr=import "L10.Engine.CSwitchMgr"
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local CFurnitureProp2=import "L10.Game.CFurnitureProp2"
local CFurnitureProp3=import "L10.Game.CFurnitureProp3"
local CTrackMgr=import "L10.Game.CTrackMgr"
local CZuoanFurnitureMgr = import "L10.UI.CZuoanFurnitureMgr"
local CClientFurnitureMgr=import "L10.Game.CClientFurnitureMgr"
local CPos = import "L10.Engine.CPos"
local Utility=import "L10.Engine.Utility"
local CFurnitureAppearanceArray = import "L10.Game.CFurnitureAppearanceArray"


local CChuanjiabaoAppearance = import "L10.Game.CChuanjiabaoAppearance"
local CDecorationAppearance = import "L10.Game.CDecorationAppearance"
local CFurnitureAppearance = import "L10.Game.CFurnitureAppearance"
local CRenderScene = import "L10.Engine.Scene.CRenderScene"
local FurnitureScene = import "L10.Game.FurnitureScene"
local CServerTimeMgr=import "L10.Game.CServerTimeMgr"
local CClientFurniture = import "L10.Game.CClientFurniture"
local MeshFilter = import "UnityEngine.MeshFilter"
local CoreScene=import "L10.Engine.Scene.CoreScene"
local CScene = import "L10.Game.CScene"
local CItemMgr = import "L10.Game.CItemMgr"


local Object = import "System.Object"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"

CLuaClientFurnitureMgr = {}
CLuaClientFurnitureMgr.m_SpecialBuildingLookup = nil
CLuaClientFurnitureMgr.m_SpecialBuildingRelationLookup = nil
CLuaClientFurnitureMgr.m_SpecialBuildingRelationLookupNew = nil
function CLuaClientFurnitureMgr.InitSpecialBuilding()
    if CLuaClientFurnitureMgr.m_SpecialBuildingLookup then return end
    CLuaClientFurnitureMgr.m_SpecialBuildingLookup = {}
    CLuaClientFurnitureMgr.m_SpecialBuildingRelationLookup = {}
    CLuaClientFurnitureMgr.m_SpecialBuildingRelationLookupNew = {}
    House_SpecialBuilding.Foreach(function(k,v)
        CLuaClientFurnitureMgr.m_SpecialBuildingLookup[v.PreZhuangshiwuId] = k
        CLuaClientFurnitureMgr.m_SpecialBuildingLookup[v.ZhuangshiwuId] = k

        CLuaClientFurnitureMgr.m_SpecialBuildingRelationLookup[v.PreZhuangshiwuId] = v.ZhuangshiwuId
        CLuaClientFurnitureMgr.m_SpecialBuildingRelationLookupNew[v.ZhuangshiwuId] = v.PreZhuangshiwuId
    end)
end


CLuaClientFurnitureMgr.m_ProcessedFurnitureId = 0

CLuaClientFurnitureMgr.m_ZhuangshiwuIds = {}
function CLuaClientFurnitureMgr.ClearZhuangshiwuIdCache()
    CLuaClientFurnitureMgr.m_ZhuangshiwuIds = {}
end

CLuaClientFurnitureMgr.m_FestivalZhuangshiwuIds = nil

function CLuaClientFurnitureMgr.InitFestival()
    if CLuaClientFurnitureMgr.m_FestivalZhuangshiwuIds then return end
    local cache = {}
    Zhuangshiwu_Festival.Foreach(function(k,v)
        local startTime = CServerTimeMgr.Inst:GetTimeStampByStr(v.StartTime)
        local endTime = CServerTimeMgr.Inst:GetTimeStampByStr(v.EndTime)
        local ids = v.ZSWIds
        for i=1,ids.Length do
            local key = ids[i-1]
            if not cache[key] then
                cache[key] = {}
            end
            table.insert( cache[key],  {startTime,endTime})
        end
    end)
    CLuaClientFurnitureMgr.m_FestivalZhuangshiwuIds = cache
end


function CLuaClientFurnitureMgr.GetIdsByType(bigTypeId)
    
    if CLuaClientFurnitureMgr.m_ZhuangshiwuIds[bigTypeId] then
        return CLuaClientFurnitureMgr.m_ZhuangshiwuIds[bigTypeId]
    else
        CLuaClientFurnitureMgr.InitFestival()

        local t = CLuaClientFurnitureMgr.m_ZhuangshiwuIds
        local lookup = {}
        local now = CServerTimeMgr.Inst.timeStamp

        Zhuangshiwu_Zhuangshiwu.Foreach(function (tid, data) 
            if data.DisplaySwitch == 0 and data.Status ~= 3 then
                local furnishTemplate = data.FurnishTemplate

                local passcheck = false
                local festivalInfo = CLuaClientFurnitureMgr.m_FestivalZhuangshiwuIds[furnishTemplate]
                if festivalInfo then
                    for i,v in ipairs(festivalInfo) do
                        if v[1]<=now and now<=v[2] then
                            passcheck = true
                        end
                    end
                else
                    passcheck = true
                end
                if passcheck then
                    local type = data.Type
                    if not t[type] then t[type]={} end
                    if not lookup[type] then lookup[type] = {} end

                    if not lookup[type][furnishTemplate] then
                        table.insert( t[type], furnishTemplate )
                        lookup[type][furnishTemplate] = true
                    end
                end
            end
        end)

        -- 家园增加天空盒
        --[[local skyType = EnumFurnitureType_lua.eSkyBox
        House_SkyBoxKind.Foreach(function (kind, data)
            if not t[skyType] then t[skyType]={} end
            if not lookup[skyType] then lookup[skyType] = {} end

            if not lookup[skyType][kind] and data.Status ~= 3 then
                table.insert( t[skyType], kind )
                lookup[skyType][kind] = true
            end
        end)--]]
        
        CLuaClientFurnitureMgr.m_ZhuangshiwuIds = t
        if not CLuaClientFurnitureMgr.m_ZhuangshiwuIds[bigTypeId] then return {} end
        return CLuaClientFurnitureMgr.m_ZhuangshiwuIds[bigTypeId]
    end
end


function CLuaClientFurnitureMgr.CheckLocation(data)--Zhuangshiwu_Zhuangshiwu
    local sceneType = CClientHouseMgr.Inst:GetCurSceneType()
    local Res = true
    if sceneType == EnumHouseSceneType_lua.eRoom then
        if data.Location == EnumFurnitureLocation_lua.eYardLand or data.Location == EnumFurnitureLocation_lua.eXiangfangLand then
            Res = false
        end
    elseif sceneType == EnumHouseSceneType_lua.eYard then
        if data.Location == EnumFurnitureLocation_lua.eRoomLand or data.Location == EnumFurnitureLocation_lua.eXiangfangLand then
            Res = false
        end
    elseif sceneType == EnumHouseSceneType_lua.eXiangfang then
        if data.Location == EnumFurnitureLocation_lua.eRoomLand or data.Location == EnumFurnitureLocation_lua.eYardLand or data.Location == EnumFurnitureLocation_lua.eDeskOrLand then
            Res = false
        end
    end
    if data.Location == EnumFurnitureLocation_lua.eDesk then
        if CZuoanFurnitureMgr.Inst:GetCurParentFurnitureId() == 0 then
            Res = false
        end
    end

    if not Res then
        g_MessageMgr:ShowMessage("Zhuangshiwu_Must_Placed_At", Zhuangshiwu_Location.GetData(data.Location).Location)
        return false
    end

    return true
end

function CLuaClientFurnitureMgr.PackAllFurnitureByTemplateId(templateId)
    local remove = {}
    CommonDefs.DictIterate(CClientFurnitureMgr.Inst.WoodPileClientObjectList, DelegateFactory.Action_object_object(function(engineId,pileId)
        local fur = CommonDefs.DictGetValue_LuaCall(CClientFurnitureMgr.Inst.WoodPileFurnitureList,pileId)
        if fur.TemplateId==templateId then
            table.insert( remove,engineId )
        end
    end))
    if #remove>0 then
        for i,v in ipairs(remove) do
            Gac2Gas.RequestPackWoodPile(v)
        end
        return
    end

    local data = Zhuangshiwu_Zhuangshiwu.GetData(templateId)
    if not data then return end
    if data.SubType == EnumFurnitureSubType_lua.eRollerCoasterCabin then
        CommonDefs.DictIterate(CClientFurnitureMgr.Inst.FurnitureList, DelegateFactory.Action_object_object(function (k,fur) 
            if fur.TemplateId == templateId then
                local parentId = fur.ParentId
                if parentId~=0 then--装在花车上
                    local parentFur = CClientFurnitureMgr.Inst:GetFurniture(parentId)
                    if parentFur then
                        for i=1,parentFur.Children.Length do
                            if parentFur.Children[i-1] == fur.ID then
                                Gac2Gas.ExchangeFurniture(EnumFurniturePlace_lua.eDecoration, parentId, 
                                    EnumFurniturePlace_lua.eRepository, i, CClientMainPlayer.Inst.Id, tostring(fur.ID))
                                break
                            end
                        end
                    end
                else
                    Gac2Gas.ExchangeFurniture(EnumFurniturePlace_lua.eFurniture, fur.ID, 
                        EnumFurniturePlace_lua.eRepository, 0, CClientMainPlayer.Inst.Id, "")
                end
            end
        end))

        return
    end


    CClientFurnitureMgr.Inst:PackAllFurnitureByTemplateId(templateId)
end

local FurnitureScaleIntervalMax = 0
local FurnitureScaleIntervalMin = 0
function GetScaleUI8ToFloat(scale)
    local res=1
    if FurnitureScaleIntervalMax==0 then
        local scaleInterval = Zhuangshiwu_Setting.GetData().FurnitureScaleInterval
        FurnitureScaleIntervalMax,FurnitureScaleIntervalMin=scaleInterval[0],scaleInterval[1]
    end
    local max,min= FurnitureScaleIntervalMax,FurnitureScaleIntervalMin
    if scale<=127 then
        res = (1-min)*scale/127+min
    else
        res = (max-1)*(scale-127)/127+1
    end
    return res
end

function CLuaClientFurnitureMgr.AddFurnitureByAppearance(appearance, bIsStolen) 
    if not (appearance.X == 0 and appearance.Y == 0 and appearance.Z == 0 and appearance.Rot == 0) then
        local ax,ay,az = appearance.X,appearance.Y,appearance.Z
        ax = math.min(math.max(ax, -256), 256)
        ay = math.min(math.max(ay, -256), 256)
        az = math.min(math.max(az, -256), 256)

        local scale = GetScaleUI8ToFloat(appearance.Scale)
        local huamuScale = GetScaleUI8ToFloat(appearance.HuamuScale)
        local huamuRot = (appearance.HuamuRot / 255) * 360

        local fid = appearance.Id
        local chuanjiabaoId = CommonDefs.DictGetValue_LuaCall(CClientHouseMgr.Inst.mFurnitureId2ChuanjiabaoId, fid)

        local fur = CClientFurnitureMgr.Inst:AddFurniture(
            fid, 
            appearance.TemplateId, 
            ax, ay, az, appearance.Rot, 
            chuanjiabaoId, 
            appearance.WinterStyle > 0, 
            appearance.EngineId, 
            appearance.Scale, 
            bIsStolen, 
            appearance.HuamuTemplateId, 
            huamuRot, 
            huamuScale)
        local arx,ary,arz = appearance.RX,appearance.RY,appearance.RZ
        if arx ~= 0 or ary ~= 0 or arz ~= 0 then
            --旋转
            local rx = arx / 256 * 360
            local ry = ary / 256 * 360
            local rz = arz / 256 * 360
            fur.ServerRX = arx
            fur.ServerRY = ary
            fur.ServerRZ = arz
            fur.RO.transform.localEulerAngles = Vector3(rx, ry, rz)
        end
        return fur
    end
    return nil
end

function Gas2Gac.OpenPutQinggongWnd(furnitureId)
    local fur = CClientFurnitureMgr.Inst:GetFurniture(furnitureId)
    if fur then
        local RO = fur.RO
        local pos = CPos(RO.Position.x, RO.Position.z)
        pos = Utility.GridPos2PixelPos(pos)
        CTrackMgr.Inst:Track(pos, Utility.Grid2Pixel(5), DelegateFactory.Action(function()
            CZuoanFurnitureMgr.Inst:SetCurParentFurnitureId(fur.ID)
            CUIManager.ShowUI(CUIResources.UseZuoanWnd)
            g_ScriptEvent:BroadcastInLua("OnUseZuoanWnd")
            CClientFurnitureMgr.Inst:ClearCurFurniture()
        end),nil,true)
    end
end


function Gas2Gac.SendPoolInfo(furniture2_U, isMingyuan)
    CClientFurnitureMgr.Inst.m_IsMingYuan = isMingyuan

    local furniture2 = CFurnitureProp2()
    furniture2:LoadFromString(furniture2_U)

    CClientFurnitureMgr.Inst.m_Pool = furniture2.Pool
    CClientFurnitureMgr.Inst.m_WarmPool = furniture2.WarmPool
    --等LuaPoolMgr.EnterJiaYuan()之后加载水池
end

function Gas2Gac.SendFurnitureAppearance(furnitureStr, decorationStr, chuanjiabaoStr, dimianChuanjiabaoStr, furnitureStr2, furnitureStr3) 
    LuaPoolMgr.SetPoolDataDirty()
    -- print("SendFurnitureAppearance")
    local furnitureArray = CFurnitureAppearanceArray()
    furnitureArray:LoadFromString(furnitureStr)
    local decorationArray = CFurnitureAppearanceArray()
    decorationArray:LoadFromString(decorationStr)
    local chuanjiabaoArray = CFurnitureAppearanceArray()
    chuanjiabaoArray:LoadFromString(chuanjiabaoStr)

    local furniture2 = CFurnitureProp2()
    furniture2:LoadFromString(furnitureStr2)

    local inst = CClientFurnitureMgr.Inst
    inst:ClearData()


    if CClientHouseMgr.Inst then
        if not CClientHouseMgr.Inst.mCurFurnitureProp3 then
            local furniture3 = CFurnitureProp3()
            furniture3:LoadFromString(furnitureStr3)
            CClientHouseMgr.Inst.mCurFurnitureProp3 = furniture3

            CClientHouseMgr.Inst:ClearGrassEditData()
            CClientHouseMgr.Inst:RefreshGrass()
            CClientHouseMgr.Inst:ClearGroundEditData()
            CClientHouseMgr.Inst:UpdateGround()
        end
    end

    --初始化变量，尝试取水池数据
    local basicProp = CClientHouseMgr.Inst.mCurBasicProp
    if basicProp then
        CClientFurnitureMgr.Inst.m_IsMingYuan = basicProp.IsMingyuan==1
    end
    local curFurnitureProp2 = CClientHouseMgr.Inst.mCurFurnitureProp2
    if curFurnitureProp2 then
        CClientFurnitureMgr.Inst.m_Pool = curFurnitureProp2.Pool
        CClientFurnitureMgr.Inst.m_WarmPool = curFurnitureProp2.WarmPool
    end
    --躲猫猫 初始化变量，尝试取水池数据
    if CScene.MainScene then
        local gamePlayId = CScene.MainScene.SceneTemplateId
        if gamePlayId == 16100468 or gamePlayId == 16100469 then
            if furniture2 then
                CClientFurnitureMgr.Inst.m_Pool = furniture2.Pool
                CClientFurnitureMgr.Inst.m_WarmPool = furniture2.WarmPool
           end
        end
    end

    CommonDefs.DictClear(CClientHouseMgr.Inst.mFurnitureId2ChuanjiabaoId)
    local dimianChuanjiabaoInfo = MsgPackImpl.unpack(dimianChuanjiabaoStr)

    --传家宝
    if dimianChuanjiabaoInfo ~= nil then
        for i=1,dimianChuanjiabaoInfo.Count,2 do
            local fid = math.floor(tonumber(dimianChuanjiabaoInfo[i-1] or 0))
            local itemid = tostring(dimianChuanjiabaoInfo[i])
            if not CommonDefs.DictContains(CClientHouseMgr.Inst.mFurnitureId2ChuanjiabaoId, typeof(UInt32), fid) then
                CommonDefs.DictAdd(CClientHouseMgr.Inst.mFurnitureId2ChuanjiabaoId, typeof(UInt32), fid, typeof(String), itemid)
            end
        end
    end

    --场景被分割成6格一块 提高效率
    local SceneMapGridWidth = 6
    local SceneMapGridHeight = 6

    local sceneType = CClientHouseMgr.Inst:GetCurSceneType()
    local notInYard = (sceneType ~= EnumHouseSceneType_lua.eYard)

    local kuozhangLvl = 1
    if not notInYard then
        kuozhangLvl = CClientFurnitureMgr.Inst:GetKuoZhangLv()
    end
    inst.mFurnitureScene = CreateFromClass(FurnitureScene)
    inst.mFurnitureScene:Init(SceneMapGridWidth, SceneMapGridHeight, CreateFromClass(MakeGenericClass(List, String)), not notInYard, kuozhangLvl)

    local mgr = CClientHouseMgr.Inst
    if mgr ~= nil and mgr.mCurConstruectProp ~= nil and CRenderScene.Inst ~= nil and CClientHouseMgr.Inst:IsPlayerInYard() then
        if kuozhangLvl<6 then--最高级别就不需要设置动态障碍了
            inst:UpdateHouseKuozhangBarrier(kuozhangLvl, true)
        else
            inst:HideSceneModels(kuozhangLvl)
        end
    end

    local furnitureStolenTime = House_Setting.GetData().FurnitureStolenTime
    local set = {}
    if CClientHouseMgr.Inst.mCurFurnitureProp ~= nil and CClientHouseMgr.Inst.mCurFurnitureProp.StolenFurnitureList ~= nil then
        CommonDefs.DictIterate(CClientHouseMgr.Inst.mCurFurnitureProp.StolenFurnitureList, DelegateFactory.Action_object_object(function (___key, ___value) 
            local data = ___value
            if data.Id ~= 0 and data.StolenTime ~= 0 and (math.floor(CServerTimeMgr.Inst.timeStamp) - data.StolenTime < furnitureStolenTime) then
                set[data.Id] = true
            end
        end))
    end
	local yardGuashiAppear = {}
    --应该先把温泉水池摆好，不然后面的装饰物的高度就不对了


    local appearanceList = {}

    CommonDefs.DictIterate(furnitureArray.Array, DelegateFactory.Action_object_object(function (___key, ___value) 
        local appearance = CFurnitureAppearance()
        appearance:LoadFromString(___value.Data)
		local ftid = appearance.TemplateId
		local designData = Zhuangshiwu_Zhuangshiwu.GetData(ftid)
		if not notInYard and designData and designData.Location == EnumFurnitureLocation_lua.eYardFenceAndRoomWall then
			table.insert(yardGuashiAppear, appearance)
		else
            if appearance.TemplateId==9448 then
                table.insert(appearanceList, 1,appearance )
            else
                table.insert(appearanceList, appearance)
            end
		end
    end))

    for i,appearance in ipairs(appearanceList) do
        local fid = appearance.Id
        local fur = CLuaClientFurnitureMgr.AddFurnitureByAppearance(appearance, set[fid] or false)
        if fur then
            local dic = furniture2 and furniture2.FurnitureColor
            if dic and CommonDefs.DictContains_LuaCall(dic,fid) then
                local color = dic[fid]
                local r,g,b = color.R/255,color.G/255,color.B/255
                fur.RO:AddLoadAllFinishedListener(DelegateFactory.Action_CRenderObject(function(ro)
                    fur:SetColor(r,g,b)
                end))
            end
        end
    end
	for _, appearance in ipairs(yardGuashiAppear) do
		local fid = appearance.Id
		CLuaClientFurnitureMgr.AddFurnitureByAppearance(appearance, set[fid] or false)
	end

    --清供
    for i=1,decorationArray.Array.Count do
        local appearance = CDecorationAppearance()
        appearance:LoadFromString(CommonDefs.DictGetValue(decorationArray.Array, typeof(UInt32), i).Data)

        if not (appearance.ParentId == 0 or appearance.SlotIndex == 0) then
            local scale = GetScaleUI8ToFloat(appearance.HuamuScale)
            local rot = (appearance.HuamuRot / 255) * 360
            CClientFurnitureMgr.Inst:AddDecoration(appearance.Id, appearance.TemplateId, appearance.ParentId, (appearance.SlotIndex) - 1, set[appearance.Id] or false, appearance.HuamuTemplateId, rot, scale)
        end
    end
    --传家宝
    for i=1,chuanjiabaoArray.Array.Count do
        local appearance = CChuanjiabaoAppearance()
        appearance:LoadFromString(CommonDefs.DictGetValue(chuanjiabaoArray.Array, typeof(UInt32), i).Data)
        if not (appearance.ParentId == 0 or appearance.SlotIndex == 0) then
            CClientFurnitureMgr.inst:AddChuanjiabaoToScene(appearance.Id, appearance.ItemId, appearance.ParentId, (appearance.SlotIndex) - 1)
        end
    end

    --预览家园装饰物模板hxy
    if CScene.MainScene then
        local sceneTemplateId = CScene.MainScene.SceneTemplateId
        local gamePlayId = CScene.MainScene.GamePlayDesignId
        if sceneTemplateId == 16101558 and gamePlayId == 51101739 then
            if CLuaZswTemplateMgr.CurPreviewInHouseId then
                local templateId = CLuaZswTemplateMgr.CurPreviewInHouseId
                local zswId = HouseCompetition_HouseTemplate.GetData(templateId).ZswID
                local tData = Zhuangshiwu_ZswTemplate.GetData(zswId)
                if tData then
                    --如果是风风的超高城堡 镜头上仰
                    if templateId == 21041082 then
                        CameraFollow.Inst.MaxSpHeight = 5
                    else
                        CameraFollow.Inst.MaxSpHeight = 0
                    end

                    for i=0,tData.DefaultComponents.Length-1,1 do
                        local info = {}
                        info.id = tData.DefaultComponents[i]
                        info.isEnough = true
                        info.isFilled = true
                        table.insert(CLuaZswTemplateMgr.CurTemplateRoTbl,info)
                    end
                    CLuaZswTemplateMgr.CurPreviewInHouseFur = CLuaClientFurnitureMgr.CreateFurniture(zswId, zswId, "")
                    local x = tData.HousePreview[0]
                    local z = tData.HousePreview[1]
                    local rot = tData.HousePreview[2]
                    CLuaZswTemplateMgr.CurPreviewInHouseFur:SetPosition(Vector3(x,0,z))
                    CLuaZswTemplateMgr.CurPreviewInHouseFur.RO.transform.eulerAngles = Vector3(0, rot, 0)
                    CLuaZswTemplateMgr.CurPreviewInHouseFur.Selected = true
                end
            end
        end
    end
end


function Gas2Gac.SyncHouseFurnitureColor(fid, colorUD)
    local color = CFurnitureColorType()
    color:LoadFromString(colorUD)
    local fur = CClientFurnitureMgr.Inst:GetFurniture(fid)
    if fur then
        fur:SetColor(color.R/255,color.G/255,color.B/255)
        local dic = CClientHouseMgr.Inst.mCurFurnitureProp2.FurnitureColor
        CommonDefs.DictSet_LuaCall(dic,fid,color)
    end
	g_ScriptEvent:BroadcastInLua("SyncHouseFurnitureColor", fid, color.R,color.G,color.B)

end

function Gas2Gac.RemoveHouseFurnitureColor(fid)
    if CClientHouseMgr.Inst then
        local dic = CClientHouseMgr.Inst.mCurFurnitureProp2.FurnitureColor
        CommonDefs.DictRemove_LuaCall(dic,fid)
        g_ScriptEvent:BroadcastInLua("RemoveHouseFurnitureColor", fid)
    end
end


function Gas2Gac.SetRepositoryFurnitureCount(templateId,count)
    CClientFurnitureMgr.Inst:SetRepositoryFurnitureCount(templateId, count)
	g_ScriptEvent:BroadcastInLua("SetRepositoryFurnitureCount", templateId, count)
end

--装饰物模板
function CLuaClientFurnitureMgr.StopTemplate()

    local curFur = CClientFurnitureMgr.Inst.CurFurniture
    if not curFur then return end
    local data = Zhuangshiwu_Zhuangshiwu.GetData(curFur.TemplateId)
    if data.Type == EnumFurnitureType_lua.eZswTemplate then
        --如果是装饰物模板的话，还需要删掉临时创建的组成fur
        curFur:Destroy()       
    end
end
--从C#挪出来
function CLuaClientFurnitureMgr.CreateFurnitureFromRepository(id, templateId, chuanjiabaoId)

    if CClientFurnitureMgr.Inst.CurFurniture and not CommonDefs.DictContains(CClientFurnitureMgr.Inst.FurnitureList, typeof(UInt32), CClientFurnitureMgr.Inst.CurFurniture.ID) then
        CClientFurnitureMgr.Inst.CurFurniture:Destroy()
        CClientFurnitureMgr.Inst.CurFurniture = nil

        CUIManager.CloseUI(CUIResources.HousePopupMenu)
    end

    local fur
    local vec = CClientFurnitureMgr.Inst.mVec3Continued
    if CClientFurnitureMgr.Inst.mbIsContinued and not (vec.x==0 and vec.y==0 and vec.z==0) and templateId ~= Zhuangshiwu_Setting.GetData().DimianChuanjiabaoId then
        fur = CClientFurnitureMgr.Inst:CreateFurnitureContinued(templateId, id, chuanjiabaoId)
    else
        fur = CLuaClientFurnitureMgr.CreateFurniture(templateId, id, chuanjiabaoId)  
    end

    fur.ID = id
    fur.Selected = true
    fur.Moving = true
    CClientFurnitureMgr.Inst.CurFurniture = fur
    if CClientFurnitureMgr.Inst.mbIsContinued and CClientFurnitureMgr.Inst:GetRepositoryFurnitureCount(templateId) > 0 then
        fur:ShowActionPopupMenu()
        CClientFurnitureMgr.Inst.mVec3Continued = Vector3.zero
    end
end

CLuaClientFurnitureMgr.m_LatColorGridOverlap = false
function CLuaClientFurnitureMgr.CheckCanMoveFurniture(fur)
    local sceneType = CClientHouseMgr.Inst:GetCurSceneType()
    local notInYard = (sceneType ~= EnumHouseSceneType_lua.eYard)

    if not notInYard and CClientHouseMgr.Inst.mCurConstruectProp then
        local zhengwuGrade = CClientHouseMgr.Inst.mCurConstruectProp.ZhengwuGrade
        if CClientFurnitureMgr.Inst.m_CachedZhengWuGrade ~= zhengwuGrade then
            CClientFurnitureMgr.Inst.m_CachedZhengWuGrade = zhengwuGrade
            local data = House_ZhengwuUpgrade.GetData(zhengwuGrade)
            if data then
                CClientFurnitureMgr.Inst.m_CachedKuoZhangLv = data.KuozhangLevel
            end
        end
    end

    if not CClientFurnitureMgr.Inst.mFurnitureScene then
        return false
    end

    if fur.RO then
        --其他摆放限制
        local bCanMoveAllChild = true
        local gridPos = {}
        local colorGridOverlap = false
        --模板是否全部可以摆放todo
        if fur:GetFurnitureType(fur.TemplateId) == EnumFurnitureType_lua.eZswTemplate then
            local mapWidth = CoreScene.MainCoreScene.WidthInGrid
            local mapHeight = CoreScene.MainCoreScene.HeightInGrid
            for i = 0, fur.Children.Length - 1 ,1 do
                if fur.Children[i] > 0 then
                    local fid = fur.Children[i]
                    local childFur = CClientFurnitureMgr.Inst:GetFurniture(fid)
                    if childFur then
                        bCanMoveAllChild = CClientFurnitureMgr.Inst.mFurnitureScene:CanMoveFurniture(childFur.ID, childFur.TemplateId, math.floor(childFur.Position.x), math.floor(childFur.Position.z), math.floor((childFur.RO.transform.eulerAngles.y + 360.0) % 360.0), CClientFurnitureMgr.GetScaleFloatToUI8(childFur.RO.Scale), notInYard, CClientFurnitureMgr.Inst.m_CachedKuoZhangLv) 
                        local coloGridGo = childFur.mColorGrid
                        if coloGridGo then
                            local coloMesh = coloGridGo:GetComponent(typeof(MeshFilter))
                            if coloMesh then
                                local pos = coloGridGo.transform.position
                                local x,y,z = pos.x, pos.y, pos.z
                                if gridPos[x] then
                                    if gridPos[x][z] then
                                        colorGridOverlap = true
                                        if(colorGridOverlap and not CLuaClientFurnitureMgr.m_LatColorGridOverlap)then
                                            g_MessageMgr:ShowMessage("ZswTemplate_Overlap_CannotPut")
                                            CLuaClientFurnitureMgr.m_LatColorGridOverlap = true
                                        end
                                        return false
                                        --不能摆放了
                                    else
                                        gridPos[x][z] = true
                                    end
                                else
                                    gridPos[x] = {}
                                    gridPos[x][z] = true
                                end
                            end
                            
                        end
                        local x = childFur.Position.x
                        local z = childFur.Position.z
                        if x<0 or x>mapWidth or z<0 or z>mapHeight then
                            return false
                        end
                        
                    end

                    if not bCanMoveAllChild then return false end
                end
            end
            if not colorGridOverlap then
                CLuaClientFurnitureMgr.m_LatColorGridOverlap = false
            end

            return bCanMoveAllChild
        else
            CLuaClientFurnitureMgr.m_LatColorGridOverlap = false
            return CClientFurnitureMgr.Inst.mFurnitureScene:CanMoveFurniture(fur.ID, fur.TemplateId, math.floor(fur.Position.x), math.floor(fur.Position.z), math.floor((fur.RO.transform.eulerAngles.y + 360.0) % 360.0), CClientFurnitureMgr.GetScaleFloatToUI8(fur.RO.Scale), notInYard, CClientFurnitureMgr.Inst.m_CachedKuoZhangLv)
        end
    end
end

function CLuaClientFurnitureMgr.CreateFurniture(templateId, id, chuanjiabaoItemId)
    print("CreateFurniture",templateId, id, chuanjiabaoItemId)
    local isWinter = CClientFurnitureMgr.Inst:IsDefaultFreeWinterStyleFurniture(templateId)

    local pos = CClientMainPlayer.Inst.WorldPos
    local posx = math.floor(pos.x)
    local posz = math.floor(pos.z)

    local offsetY = 0
    local fur = CClientFurniture.Create(id, templateId, posx, posz, CClientFurnitureMgr.DefaultRot, chuanjiabaoItemId,isWinter,0,offsetY,0,0,127,false)
    if not fur then
        return nil
    end

    local data = Zhuangshiwu_Zhuangshiwu.GetData(templateId)
    local dimianQinggongScale = Zhuangshiwu_Setting.GetData().DimianQinggongScale
    if fur.ParentId == 0 and CZuoanFurnitureMgr.Inst:GetCurParentFurnitureId() == 0 and data.Location == EnumFurnitureLocation_lua.eDeskOrLand then
        fur.RO.Scale = fur.RO.Scale *dimianQinggongScale
    end

    if data.Type == EnumFurnitureType_lua.eZswTemplate then--模板 
        local zswTemplateData = Zhuangshiwu_ZswTemplate.GetData(templateId)
        if zswTemplateData then
            --根据模板填充情况进行摆放
            for index = 1,#CLuaZswTemplateMgr.CurTemplateRoTbl,1 do
                local roInfo = CLuaZswTemplateMgr.CurTemplateRoTbl[index]
                local cslot = index - 1
                local fillId = roInfo.id
                local isFilled = roInfo.isFilled
                if isFilled and fillId and roInfo.isEnough then
                    --local childId = SafeStringFormat3("%d%003d",id,cslot)
                    local childId = SafeStringFormat3("%d%003d",fillId,cslot)
                    childId = tonumber(childId)
                    local componentFur
                    componentFur = CClientFurnitureMgr.Inst:CreateFurniture(fillId, childId, "")
                    componentFur:SetParentFurnitureRoot(true)
                    --CClientFurnitureMgr.Inst.FurnitureList:Add(childId, componentFur)
                    CommonDefs.DictAdd(CClientFurnitureMgr.Inst.FurnitureList, typeof(UInt32), childId, typeof(CClientFurniture), componentFur)
                    componentFur.ParentId = id
                    componentFur.SlotIndex = cslot
                    fur.Children[cslot] = childId
                end
            end
        end
    end
    return fur
end

function CLuaClientFurnitureMgr.CreateFurnitureFromRepositoryContinued(fur)
    local data = Zhuangshiwu_Zhuangshiwu.GetData(fur.TemplateId)
    if not data then return end

    local numlimit = CLuaClientFurnitureMgr.GetRepositoryNumLimit(CClientHouseMgr.Inst:GetCurHouseGrade(), data.Type, data.SubType)
    local curNum = CLuaClientFurnitureMgr.GetPlacedFurnitureCountByTemplateId(fur.TemplateId)
    if numlimit > curNum and CClientFurnitureMgr.Inst:GetRepositoryFurnitureCount(fur.TemplateId)>0 then
        Gac2Gas.CreateFurnitureFromRepository(fur.TemplateId, "")
    end
end
function CLuaClientFurnitureMgr.GetRepositoryNumLimit(houseGrade,type,subtype)
    if type==12 then--EnumFurnitureType.eJianzhu
        if subtype==12 then--EnumFurnitureSubType.eZhengwu
            return 1
        elseif subtype==15 then--EnumFurnitureSubType.eMiaopu
            return 6
        elseif subtype==13 then--EnumFurnitureSubType.eCangku
            return 1
        end
    end
    return 9999
end

function CLuaClientFurnitureMgr.GetPlacedFurnitureCount(sceneType) 
    if CClientHouseMgr.Inst.mFurnitureProp == nil or CClientHouseMgr.Inst.mBasicProp == nil then
        return 0
    end
    local furProp = CClientHouseMgr.Inst.mFurnitureProp
    local furProp2 = CClientHouseMgr.Inst.mFurnitureProp2
    local curSceneType = CClientHouseMgr.Inst:GetCurSceneType()
    local dimianChuanjiabaoId = Zhuangshiwu_Setting.GetData().DimianChuanjiabaoId

    if curSceneType == sceneType then
        local count = 0
        CommonDefs.DictIterate(CClientFurnitureMgr.Inst.FurnitureList, DelegateFactory.Action_object_object(function (___key, ___value) 
            local tid = ___value.TemplateId
            local data = LoadFurnitureDesignData(tid)
            if data ~= nil and data.Type ~= EnumFurnitureType_lua.eJianzhu 
                and data.Type ~= EnumFurnitureType_lua.eLingshou 
                and data.Type ~= EnumFurnitureType_lua.ePuppet 
                and data.Type ~= EnumFurnitureType_lua.eMapi 
                and tid ~= dimianChuanjiabaoId then
                count = count + 1
            end
            -- if CommonDefs.DictContains(furProp.FurnitureHuamuData, typeof(UInt32), ___key) and CommonDefs.DictGetValue(furProp.FurnitureHuamuData, typeof(UInt32), ___key).HuamuTemplateId ~= 0 then
            --     count = count + 1
            -- end
        end))
        return count
        --原先这里对不上：服务器放传家宝不收限制，但是客户端又算进去了；后来策划统一传家宝不算限制
    else
        if sceneType == EnumHouseSceneType_lua.eYard then
            local count = 0
            local addCount = DelegateFactory.Action_object_object(function (___key, ___value) 
                local tid = ___value.FurnitureTemplateId
                local data = LoadFurnitureDesignData(tid)
                if data ~= nil and data.Type ~= EnumFurnitureType_lua.eJianzhu 
                    and data.Type ~= EnumFurnitureType_lua.eLingshou 
                    and data.Type ~= EnumFurnitureType_lua.ePuppet 
                    and data.Type ~= EnumFurnitureType_lua.eMapi 
                    and tid ~= dimianChuanjiabaoId then
                    count = count + 1
                end
                -- if CommonDefs.DictContains(furProp.FurnitureHuamuData, typeof(UInt32), ___key) and CommonDefs.DictGetValue(furProp.FurnitureHuamuData, typeof(UInt32), ___key).HuamuTemplateId ~= 0 then
                --     count = count + 1
                -- end
            end)
            CommonDefs.DictIterate(furProp.YardFurnitureData, addCount)
            CommonDefs.DictIterate(furProp2.YardFurnitureData, addCount)

            return (count + furProp.YardDecorationData.Count+furProp2.YardDecorationData.Count)
        elseif sceneType == EnumHouseSceneType_lua.eRoom then
            local huamuCount = 0
            local furnitureCount = 0
            local addCount = DelegateFactory.Action_object_object(function (___key, ___value) 
                -- if CommonDefs.DictContains(furProp.FurnitureHuamuData, typeof(UInt32), ___key) and CommonDefs.DictGetValue(furProp.FurnitureHuamuData, typeof(UInt32), ___key).HuamuTemplateId ~= 0 then
                --     huamuCount = huamuCount + 1
                -- end
                if ___value.FurnitureTemplateId ~= dimianChuanjiabaoId then
                    furnitureCount = furnitureCount + 1
                end
            end)

            CommonDefs.DictIterate(furProp.RoomFurnitureData, addCount)
            CommonDefs.DictIterate(furProp2.RoomFurnitureData, addCount)

            return (furnitureCount + furProp.RoomDecorationData.Count+furProp2.RoomDecorationData.Count)
            --花木类的特殊处理看起来不对，但是目前花木也不能放入室内，所以先注释掉了
        elseif sceneType == EnumHouseSceneType_lua.eXiangfang then
            local huamuCount = 0
            local furnitureCount = 0
            local addCount = DelegateFactory.Action_object_object(function (___key, ___value) 
                -- if CommonDefs.DictContains(furProp.FurnitureHuamuData, typeof(UInt32), ___key) and CommonDefs.DictGetValue(furProp.FurnitureHuamuData, typeof(UInt32), ___key).HuamuTemplateId ~= 0 then
                --     huamuCount = huamuCount + 1
                -- end
                if ___value.FurnitureTemplateId ~= dimianChuanjiabaoId then
                    furnitureCount = furnitureCount + 1
                end
            end)
            CommonDefs.DictIterate(furProp.XiangfangFurnitureData, addCount)
            CommonDefs.DictIterate(furProp2.XiangfangFurnitureData, addCount)
            return (furnitureCount + furProp.XiangfangDecorationData.Count + furProp2.XiangfangDecorationData.Count)
        end
    end
    return 0
end

function CLuaClientFurnitureMgr.GetPlacedShineiFurnitureCount(bShinei) 
    if bShinei then
        return CLuaClientFurnitureMgr.GetPlacedFurnitureCount(EnumHouseSceneType_lua.eRoom) 
            + CLuaClientFurnitureMgr.GetPlacedFurnitureCount(EnumHouseSceneType_lua.eXiangfang)
    else
        return CLuaClientFurnitureMgr.GetPlacedFurnitureCount(EnumHouseSceneType_lua.eYard)
    end
end

function CLuaClientFurnitureMgr.GetTotalPlacedFurnitureCount()
    return CLuaClientFurnitureMgr.GetPlacedFurnitureCount(EnumHouseSceneType_lua.eYard) 
        + CLuaClientFurnitureMgr.GetPlacedFurnitureCount(EnumHouseSceneType_lua.eRoom) 
        + CLuaClientFurnitureMgr.GetPlacedFurnitureCount(EnumHouseSceneType_lua.eXiangfang)
end

CLuaClientFurnitureMgr.s_WoodPileId1 = 9127
CLuaClientFurnitureMgr.s_WoodPileId2 = 9128
function CLuaClientFurnitureMgr.GetPlacedFurnitureCountByTemplateId(templateId) 
    if CClientHouseMgr.Inst.mFurnitureProp == nil then
        return 0
    end

    local prop = CClientHouseMgr.Inst.mFurnitureProp
    local prop2 = CClientHouseMgr.Inst.mFurnitureProp2
    local count = 0

    --木桩数量不计入
    -- CommonDefs.DictIterate(CClientFurnitureMgr.Inst.WoodPileFurnitureList, DelegateFactory.Action_object_object(function (___key, ___value) 
    --     count = count + 1
    -- end))

    local sceneType = CClientHouseMgr.Inst:GetCurSceneType()

    local huamuProp = nil
    local addCount = DelegateFactory.Action_object_object(function (___key, ___value) 
        if ___value.FurnitureTemplateId == templateId then
            count = count + 1
        end
        -- if ___value.HuamuTemplateId == templateId then
        --     count = count + 1
        -- end
    end)

    if sceneType == EnumHouseSceneType_lua.eRoom then
        CommonDefs.DictIterate(prop.YardDecorationData, addCount)
        CommonDefs.DictIterate(prop.YardFurnitureData, addCount)
        CommonDefs.DictIterate(prop.XiangfangDecorationData, addCount)
        CommonDefs.DictIterate(prop.XiangfangFurnitureData, addCount)

        CommonDefs.DictIterate(prop2.YardDecorationData, addCount)
        CommonDefs.DictIterate(prop2.YardFurnitureData, addCount)
        CommonDefs.DictIterate(prop2.XiangfangDecorationData, addCount)
        CommonDefs.DictIterate(prop2.XiangfangFurnitureData, addCount)
    elseif sceneType == EnumHouseSceneType_lua.eYard then
        CommonDefs.DictIterate(prop.RoomDecorationData, addCount)
        CommonDefs.DictIterate(prop.RoomFurnitureData, addCount)
        CommonDefs.DictIterate(prop.XiangfangDecorationData, addCount)
        CommonDefs.DictIterate(prop.XiangfangFurnitureData, addCount)

        CommonDefs.DictIterate(prop2.RoomDecorationData, addCount)
        CommonDefs.DictIterate(prop2.RoomFurnitureData, addCount)
        CommonDefs.DictIterate(prop2.XiangfangDecorationData, addCount)
        CommonDefs.DictIterate(prop2.XiangfangFurnitureData, addCount)
    elseif sceneType == EnumHouseSceneType_lua.eXiangfang then
        CommonDefs.DictIterate(prop.RoomDecorationData, addCount)
        CommonDefs.DictIterate(prop.RoomFurnitureData, addCount)
        CommonDefs.DictIterate(prop.YardDecorationData, addCount)
        CommonDefs.DictIterate(prop.YardFurnitureData, addCount)

        CommonDefs.DictIterate(prop2.RoomDecorationData, addCount)
        CommonDefs.DictIterate(prop2.RoomFurnitureData, addCount)
        CommonDefs.DictIterate(prop2.YardDecorationData, addCount)
        CommonDefs.DictIterate(prop2.YardFurnitureData, addCount)
    end

    CommonDefs.DictIterate(CClientFurnitureMgr.Inst.FurnitureList, DelegateFactory.Action_object_object(function (___key, ___value) 
        if ___value.TemplateId == templateId then
            count = count + 1
        end
        -- if ___value.PenzaiHuamuTid == templateId then
        --     count = count + 1
        -- end
    end))

    return count
end

function CLuaClientFurnitureMgr.GetPlacedFurnitureCounts() 
    local ret = {}
    if CClientHouseMgr.Inst.mFurnitureProp == nil then
        return ret
    end

    local prop = CClientHouseMgr.Inst.mFurnitureProp
    local prop2 = CClientHouseMgr.Inst.mFurnitureProp
    local count = 0

    local sceneType = CClientHouseMgr.Inst:GetCurSceneType()

    local huamuProp = nil

    local addCount = DelegateFactory.Action_object_object(function (___key, ___value) 
        local tid = ___value.FurnitureTemplateId
        if not ret[tid] then ret[tid] = 0 end
        ret[tid] = ret[tid] + 1

        -- local tid = ___value.HuamuTemplateId
        -- if not ret[tid] then ret[tid] = 0 end
        -- ret[tid] = ret[tid] + 1
    end)

    if sceneType == EnumHouseSceneType_lua.eRoom then
        CommonDefs.DictIterate(prop.YardDecorationData, addCount)
        CommonDefs.DictIterate(prop.YardFurnitureData, addCount)
        CommonDefs.DictIterate(prop.XiangfangDecorationData, addCount)
        CommonDefs.DictIterate(prop.XiangfangFurnitureData, addCount)

        CommonDefs.DictIterate(prop2.YardDecorationData, addCount)
        CommonDefs.DictIterate(prop2.YardFurnitureData, addCount)
        CommonDefs.DictIterate(prop2.XiangfangDecorationData, addCount)
        CommonDefs.DictIterate(prop2.XiangfangFurnitureData, addCount)
    elseif sceneType == EnumHouseSceneType_lua.eYard then
        CommonDefs.DictIterate(prop.RoomDecorationData, addCount)
        CommonDefs.DictIterate(prop.RoomFurnitureData, addCount)
        CommonDefs.DictIterate(prop.XiangfangDecorationData, addCount)
        CommonDefs.DictIterate(prop.XiangfangFurnitureData, addCount)

        CommonDefs.DictIterate(prop2.RoomDecorationData, addCount)
        CommonDefs.DictIterate(prop2.RoomFurnitureData, addCount)
        CommonDefs.DictIterate(prop2.XiangfangDecorationData, addCount)
        CommonDefs.DictIterate(prop2.XiangfangFurnitureData, addCount)
    elseif sceneType == EnumHouseSceneType_lua.eXiangfang then
        CommonDefs.DictIterate(prop.RoomDecorationData, addCount)
        CommonDefs.DictIterate(prop.RoomFurnitureData, addCount)
        CommonDefs.DictIterate(prop.YardDecorationData, addCount)
        CommonDefs.DictIterate(prop.YardFurnitureData, addCount)

        CommonDefs.DictIterate(prop2.RoomDecorationData, addCount)
        CommonDefs.DictIterate(prop2.RoomFurnitureData, addCount)
        CommonDefs.DictIterate(prop2.YardDecorationData, addCount)
        CommonDefs.DictIterate(prop2.YardFurnitureData, addCount)
    end

    CommonDefs.DictIterate(CClientFurnitureMgr.Inst.FurnitureList, DelegateFactory.Action_object_object(function (___key, ___value) 
        local tid = ___value.TemplateId
        if not ret[tid] then ret[tid] = 0 end
        ret[tid] = ret[tid] + 1

        -- local tid = ___value.PenzaiHuamuTid
        -- if not ret[tid] then ret[tid] = 0 end
        -- ret[tid] = ret[tid] + 1
    end))
    return ret
end


function CLuaClientFurnitureMgr.GetQishuFurnitureCountByTemplateId(templateId) 
    local mgr = CClientHouseMgr.Inst
    if mgr == nil or mgr.mFurnitureProp == nil then
        return 0
    end
    local count = 0
    CommonDefs.DictIterate(mgr.mFurnitureProp.QiShuFurnitureData, DelegateFactory.Action_object_object(function (itemid, ___value) 
        local item = CItemMgr.Inst:GetById(itemid)
        if item ~= nil then
            local itemdata = Item_Item.GetData(item.TemplateId)
            if itemdata ~= nil then
                local zswtid = CClientFurnitureMgr.GetZhuangshiwuIdFromItemData(itemdata)
                if zswtid == templateId then
                    count = count + 1
                end
            end
        end
    end))

    return count
end

local cachedHouseGrade = 0
local cachedIsMingYuan = false
local cachedHouseMaxNum = 0
local cachedPoolGrade = 0
local cachedPoolMaxNum = 0
function CLuaClientFurnitureMgr.GetMaxNum()
    local maxNum = 0
    if not CClientHouseMgr.Inst then return 0 end
    if not CClientHouseMgr.Inst.mConstructProp then return 0 end
    if not CClientHouseMgr.Inst.mBasicProp then return 0 end

    local isMingYuan = CClientHouseMgr.Inst.mBasicProp.IsMingyuan==1
    local curHouseGrade = CClientHouseMgr.Inst:GetCurHouseGrade()
    if cachedHouseGrade~=curHouseGrade or cachedIsMingYuan~=isMingYuan then
        local data = Zhuangshiwu_Limit.GetData(curHouseGrade)
        if data then
            cachedIsMingYuan = isMingYuan
            cachedHouseGrade = curHouseGrade
            cachedHouseMaxNum = isMingYuan and data.MingyuanZhuangShiWuRepositoryPlacedNum or data.ZhuangShiWuRepositoryPlacedNum
        else
            cachedIsMingYuan = false
            cachedHouseGrade = 0
            cachedHouseMaxNum = 0
        end
    end
    maxNum = cachedHouseMaxNum

    local poolGrade = CClientHouseMgr.Inst.mConstructProp.PoolGrade
    if cachedPoolGrade~=poolGrade then
        local pooldata = House_PoolUpgrade.GetData(poolGrade)
        if pooldata then
            cachedPoolMaxNum = pooldata.ZhuangshiwuNum
            cachedPoolGrade = poolGrade
        else
            cachedPoolGrade = 0
            cachedPoolMaxNum = 0
        end
    end
    maxNum = maxNum + cachedPoolMaxNum
    return maxNum
end

function CLuaClientFurnitureMgr.GetPenzhaiHuamuCountByTemplateId(templateId) 
    local count = 0
    local zswdata = Zhuangshiwu_Zhuangshiwu.GetData(templateId)
    if zswdata.Type == EnumFurnitureType_lua.eHuamu and CClientHouseMgr.Inst.mFurnitureProp ~= nil then
        CommonDefs.DictIterate(CClientHouseMgr.Inst.mFurnitureProp.YardDecorationData, DelegateFactory.Action_object_object(function (___key, ___value) 
            if ___value.HuamuTemplateId == templateId then
                count = count + 1
            end
        end))

        CommonDefs.DictIterate(CClientHouseMgr.Inst.mFurnitureProp.RoomDecorationData, DelegateFactory.Action_object_object(function (___key, ___value) 
            if ___value.HuamuTemplateId == templateId then
                count = count + 1
            end
        end))

        CommonDefs.DictIterate(CClientHouseMgr.Inst.mFurnitureProp.XiangfangDecorationData, DelegateFactory.Action_object_object(function (___key, ___value) 
            if ___value.HuamuTemplateId == templateId then
                count = count + 1
            end
        end))

        CommonDefs.DictIterate(CClientHouseMgr.Inst.mFurnitureProp.FurnitureHuamuData, DelegateFactory.Action_object_object(function (___key, ___value) 
            if ___value.HuamuTemplateId == templateId then
                count = count + 1
            end
        end))
    end
    return count
end

function CLuaClientFurnitureMgr.AutoPlaceFurniture()

    if not CClientHouseMgr.Inst then return end

    if not CRenderScene.Inst then return end
    --普通家园 128
    --名园室外 256
    local RandomSizeX = CRenderScene.Inst.MapWidth
    local RandomSizeZ = CRenderScene.Inst.MapHeight
    --CommonDefs.EmptyDictionaryMsgPack,

    local sceneType = CClientHouseMgr.Inst:GetCurSceneType()
    local notInYard = (sceneType ~= EnumHouseSceneType_lua.eYard)

    local isMingyuan = CClientHouseMgr.Inst.mBasicProp.IsMingyuan~=0

    local cachedKuoZhangLv = 999
    if not notInYard and CClientHouseMgr.Inst.mCurConstruectProp then
        local zhengwuGrade = CClientHouseMgr.Inst.mCurConstruectProp.ZhengwuGrade
        local data = House_ZhengwuUpgrade.GetData(zhengwuGrade)
        cachedKuoZhangLv = data.KuozhangLevel
    end
    local furnitureScene = CClientFurnitureMgr.Inst.mFurnitureScene

    local bSetLimitStatus = CClientHouseMgr.Inst.mConstructProp.FurnitureLimitStatus > 0
    local maxNum = CLuaClientFurnitureMgr.GetMaxNum()
    local canAddNum = 0

    if bSetLimitStatus then
        local curNum = notInYard and CLuaClientFurnitureMgr.GetPlacedShineiFurnitureCount(true) or CLuaClientFurnitureMgr.GetPlacedShineiFurnitureCount(false)
        canAddNum = maxNum - curNum
    else
        local curNum = CLuaClientFurnitureMgr.GetTotalPlacedFurnitureCount()
        canAddNum = maxNum - curNum
    end

    local cnt = 0
    local huamus = {}
    local xiaojings = {}
    local others = {}

    local function checkLocation(data)
        local Res = true
        if sceneType == EnumHouseSceneType_lua.eRoom then
            if data.Location == EnumFurnitureLocation_lua.eYardLand or data.Location == EnumFurnitureLocation_lua.eXiangfangLand then
                Res = false
            end
        elseif sceneType == EnumHouseSceneType_lua.eYard then
            if data.Location == EnumFurnitureLocation_lua.eRoomLand or data.Location == EnumFurnitureLocation_lua.eXiangfangLand then
                Res = false
            end
        elseif sceneType == EnumHouseSceneType_lua.eXiangfang then
            if data.Location == EnumFurnitureLocation_lua.eRoomLand or data.Location == EnumFurnitureLocation_lua.eYardLand or data.Location == EnumFurnitureLocation_lua.eDeskOrLand then
                Res = false
            end
        end
        if data.Location == EnumFurnitureLocation_lua.eDesk then
            Res = false
        end
        return Res
    end
    local houseGrade = CClientHouseMgr.Inst:GetCurHouseGrade()

    CommonDefs.DictIterate(CClientHouseMgr.Inst.mFurnitureProp.RepositoryData,DelegateFactory.Action_object_object(function (templateId, count)
        local data = Zhuangshiwu_Zhuangshiwu.GetData(templateId)
        if data and data.DisplaySwitch == 0 and data.Status ~= 3 and data.ResName and data.ResName~="" and data.ResName~="0" and houseGrade>=data.HouseGrade then
            if data.SubType==8 then--qinggong
                --decoration
            elseif data.SubType==11 then--huamu
                if checkLocation(data) then
                    huamus[templateId] = count
                end
            elseif data.SubType==7 then--eGuashi
            elseif data.SubType==9 then--eChuanjiabao
            elseif data.SubType==10 then--eXiaojing
                if checkLocation(data) and not CLuaHouseWoodPileMgr.IsWoodPile(templateId) then
                    xiaojings[templateId] = count
                end
            elseif data.SubType==18 then--eLingshou
            elseif data.SubType==19 then--ePuppet
            elseif data.SubType==20 then--eMapi
            elseif data.SubType==21 then--eSkyBox
            elseif data.SubType==22 then--eZswTemplate
            else
                if checkLocation(data) then
                    others[templateId] = count
                end
            end
        end
        
    end))
    local BatchNumber = 5
    local FurnitureList = CreateFromClass(MakeGenericClass(List, Object))
    local count = 0
    local MaxTryCount = 100
    g_FurnitureScene:ClearTempFurnitures()



    local function requestAdd(tbl)
        for templateId,cnt in pairs(tbl) do
            if canAddNum<=0 then
                break
            end
            for i=1,cnt do
                if canAddNum<=0 then
                    break
                end
                if count == BatchNumber then
                    Gac2Gas.PlayerRequestAutoHouseLayout(MsgPackImpl.pack(FurnitureList), false)
                    count = 0
                    CommonDefs.ListClear(FurnitureList)
                end
    
                --最多尝试50次
                -- local success = false
                local rot_choices = {0,90,180,270}
                for i=1,MaxTryCount do
                    local x = math.random( 0,RandomSizeX )
                    local z = math.random( 0,RandomSizeZ )

                    --先过滤一下 比如室内可能会摆到房子外面
                    local origBarrier = EnumToInt(CoreScene.MainCoreScene:GetOriginalBarrierv(x, z))
                    if origBarrier==0 then
                        local y = 0
                        local rot = rot_choices[math.random( 1,4 )]--1,2,3,4
                        local extraOption = CommonDefs.EmptyDictionaryMsgPack
                        local rx,ry,rz = 0,0,0
                        local scale = 127
                        local chuanjiabaoId = ""
                        if FurnitureScene_CanMoveTempFurniture(templateId,x,z,rot, scale, notInYard,cachedKuoZhangLv) then
                            -- print(templateId, x, y, z, rot, true, chuanjiabaoId, extraOption, rx, ry, rz, scale)
                            FurnitureScene_AddTempFurniture(templateId, x, y, z, rot, scale)
        
                            CommonDefs.ListAdd(FurnitureList, typeof(UInt32), templateId)
                            CommonDefs.ListAdd(FurnitureList, typeof(Single), x)--x
                            CommonDefs.ListAdd(FurnitureList, typeof(Single), 0)--y
                            CommonDefs.ListAdd(FurnitureList, typeof(Single), z)--z
                            CommonDefs.ListAdd(FurnitureList, typeof(UInt32), rot)--rot
                            CommonDefs.ListAdd(FurnitureList, typeof(UInt32), scale)--ui8scale
                            CommonDefs.ListAdd(FurnitureList, typeof(Boolean), false)--iswinter
                
                            CommonDefs.ListAdd(FurnitureList, typeof(UInt32), 0)--rx
                            CommonDefs.ListAdd(FurnitureList, typeof(UInt32), 0)--ry
                            CommonDefs.ListAdd(FurnitureList, typeof(UInt32), 0)--rz
        
        
                            canAddNum = canAddNum-1
                            -- success = true
                            count = count + 1
                            break
                        end
                    end
                end
                -- if not success then
                --     print("fail")
                -- end
            end
        end
    end

    --先摆花木
    if not notInYard then
        requestAdd(huamus)
    end
    if not notInYard then
        requestAdd(xiaojings)
    end
    requestAdd(others)

    g_FurnitureScene:ClearTempFurnitures()
end


function Gas2Gac.CharacterAttachFurniture(engineId,furnitureId,slotIdx)
    local fur = CClientFurnitureMgr.Inst:GetFurniture(furnitureId)
    if fur then
        fur:AddFurnitureInteractPlayer(-engineId, slotIdx - 1)
    end
end
function Gas2Gac.CharacterDetachFurniture(engineId,furnitureId)
    local fur = CClientFurnitureMgr.Inst:GetFurniture(furnitureId)
    if fur then
        fur:RemoveFurnitureInteractPlayer(-engineId)
    end
end
function Gas2Gac.PlayerAttachFurniture(playerId,furnitureId, slotIdx)
    CClientFurnitureMgr.Inst:PlayerAttachFurniture(playerId, furnitureId, slotIdx)
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id==playerId then
        --显示跷跷板图标
        g_ScriptEvent:BroadcastInLua("MainPlayerAttachFurniture", furnitureId,slotIdx)

        local fur = CClientFurnitureMgr.Inst:GetFurniture(furnitureId)
        if fur then
            local type = fur:GetSubType()
            if type == EnumFurnitureSubType_lua.eRollerCoasterCabin then
                if fur.ParentId>0 then
                    CUIManager.ShowUI(CLuaUIResources.HouseRollerCoasterWnd)
                end
            end
        end
    end
    g_ScriptEvent:BroadcastInLua("PlayerAttachFurniture", playerId,furnitureId, slotIdx)
end

function Gas2Gac.PlayerDetachFurniture(playerId,furnitureId)
    CClientFurnitureMgr.Inst:PlayerDetachFurniture(playerId, furnitureId)
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id==playerId then
        g_ScriptEvent:BroadcastInLua("MainPlayerDetachFurniture", furnitureId)
        CUIManager.CloseUI(CLuaUIResources.HouseRollerCoasterWnd)
    end

    g_ScriptEvent:BroadcastInLua("PlayerDetachFurniture", playerId,furnitureId)
end



function Gas2Gac.SendPlayerFurnitureInteractInfo(playerFurnitureInteractInfo)
    local furnitureInteractInfo = TypeAs(MsgPackImpl.unpack(playerFurnitureInteractInfo), typeof(MakeGenericClass(List, Object)))
    if furnitureInteractInfo and furnitureInteractInfo.Count > 0 and (furnitureInteractInfo.Count % 3) == 0 then
        do
            local i = 0
            while i < furnitureInteractInfo.Count do
                local fid = math.floor(tonumber(furnitureInteractInfo[i + 0] or 0))
                local slotIdx = math.floor(tonumber(furnitureInteractInfo[i + 1] or 0))
                local pid = tonumber(furnitureInteractInfo[i + 2])

                local fur = CClientFurnitureMgr.inst:GetFurniture(fid)
                if fur then
                    fur:AddFurnitureInteractPlayer(pid, slotIdx - 1)
                end
                i = i + 3
            end
        end
    end
end
function Gas2Gac.SendCharacterFurnitureInteractInfo(playerFurnitureInteractInfo)
    local furnitureInteractInfo = TypeAs(MsgPackImpl.unpack(playerFurnitureInteractInfo), typeof(MakeGenericClass(List, Object)))
    if furnitureInteractInfo and furnitureInteractInfo.Count > 0 and (furnitureInteractInfo.Count % 3) == 0 then
        if CSwitchMgr.IsEnableFixNpcFurnitureInteract then
            CClientFurnitureMgr.Inst:ResetInteractiveCache()
        end
        do
            local i = 0
            while i < furnitureInteractInfo.Count do
                local fid = math.floor(tonumber(furnitureInteractInfo[i + 0] or 0))
                local slotIdx = math.floor(tonumber(furnitureInteractInfo[i + 1] or 0))
                local engineId = math.floor(tonumber(furnitureInteractInfo[i + 2] or 0))

                local fur = CClientFurnitureMgr.inst:GetFurniture(fid)
                if fur ~= nil then
                    fur:AddFurnitureInteractPlayer(-engineId, slotIdx - 1)
                else
                    if CSwitchMgr.IsEnableFixNpcFurnitureInteract then
                        CClientFurnitureMgr.Inst:AddInteractiveCache(fid, slotIdx - 1, engineId)
                    end
                end
                i = i + 3
            end
        end
    end
end

function Gas2Gac.ChangeBuildingAppearance(furnitureId,targetTemplateId)
    local fur = CClientFurnitureMgr.inst:GetFurniture(furnitureId)
    if fur then
        local tid = fur.TemplateId
        fur:ChangeBuildingAppearance(targetTemplateId)
        if CClientFurnitureMgr.inst.mFurnitureScene  then
            CClientFurnitureMgr.inst.mFurnitureScene:MoveFurniture(furnitureId, 
                tid, math.floor(fur.ServerPos.x), math.floor(fur.ServerPos.z), fur.ServerRotateY, fur.ServerScale,
                targetTemplateId, math.floor(fur.ServerPos.x), math.floor(fur.ServerPos.z), fur.ServerRotateY, fur.ServerScale);
        end

        fur.IsWinter = CClientHouseMgr.IsWinterStyle()
        fur:RefreshWinterSkin()

	    g_ScriptEvent:BroadcastInLua("ChangeBuildingAppearance", furnitureId,targetTemplateId)
    end
end

CLuaClientFurnitureMgr.ShowTableType = nil
