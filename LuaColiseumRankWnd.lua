local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnTipButton = import "L10.UI.QnTipButton"
local UILabel = import "UILabel"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local UISprite = import "UISprite"
local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local Constants = import "L10.Game.Constants"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local AlignType = import "CPlayerInfoMgr+AlignType"
local EnumClass = import "L10.Game.EnumClass"
local Profession = import "L10.Game.Profession"
local CLoginMgr = import "L10.Game.CLoginMgr"
local CPopupMenuInfoMgrAlignType = import "L10.UI.CPopupMenuInfoMgr+AlignType"
local CPlayerShopPopupMenuInfoMgr = import "L10.UI.CPlayerShopPopupMenuInfoMgr"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local CBiWuDaHuiMgr = import "L10.Game.CBiWuDaHuiMgr"

LuaColiseumRankWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaColiseumRankWnd, "MyRankLabel", "MyRankLabel", UILabel)
RegistChildComponent(LuaColiseumRankWnd, "RankImage", "RankImage", UISprite)
RegistChildComponent(LuaColiseumRankWnd, "MyNameLabel", "MyNameLabel", UILabel)
RegistChildComponent(LuaColiseumRankWnd, "MyLevelLabel", "MyLevelLabel", UILabel)
RegistChildComponent(LuaColiseumRankWnd, "MyEquipScoreLabel", "MyEquipScoreLabel", UILabel)
RegistChildComponent(LuaColiseumRankWnd, "MyScoreLabel", "MyScoreLabel", UILabel)
RegistChildComponent(LuaColiseumRankWnd, "TableView", "TableView", QnAdvanceGridView)
RegistChildComponent(LuaColiseumRankWnd, "QnIncreseAndDecreaseButton", "QnIncreseAndDecreaseButton", QnAddSubAndInputButton)
RegistChildComponent(LuaColiseumRankWnd, "MainPlayerClassSprite", "MainPlayerClassSprite", UISprite)
RegistChildComponent(LuaColiseumRankWnd, "MyServerNameLabel", "MyServerNameLabel", UILabel)
RegistChildComponent(LuaColiseumRankWnd, "TimeLabel", "TimeLabel", UILabel)
RegistChildComponent(LuaColiseumRankWnd, "SeasonButton", "SeasonButton", QnTipButton)
RegistChildComponent(LuaColiseumRankWnd, "IsKuaFuButton", "IsKuaFuButton", QnTipButton)
RegistChildComponent(LuaColiseumRankWnd, "RankTypeButton", "RankTypeButton", QnTipButton)
RegistChildComponent(LuaColiseumRankWnd, "LevelStageButton", "LevelStageButton", QnTipButton)
RegistChildComponent(LuaColiseumRankWnd, "Page", "Page", GameObject)
RegistChildComponent(LuaColiseumRankWnd, "KuaFuBottomLabel", "KuaFuBottomLabel", UILabel)
--@endregion RegistChildComponent end
RegistClassMember(LuaColiseumRankWnd,"m_RanksData")
RegistClassMember(LuaColiseumRankWnd,"m_PageIndex")
RegistClassMember(LuaColiseumRankWnd,"m_SeasonIndex")
RegistClassMember(LuaColiseumRankWnd,"m_SeasonActions")
RegistClassMember(LuaColiseumRankWnd,"m_IsKuaFu")
RegistClassMember(LuaColiseumRankWnd,"m_IsKuaFuActions")
RegistClassMember(LuaColiseumRankWnd,"m_RankTypeIndex")
RegistClassMember(LuaColiseumRankWnd,"m_RankTypeActions")
RegistClassMember(LuaColiseumRankWnd,"m_LevelStageIndex")
RegistClassMember(LuaColiseumRankWnd,"m_LevelStageActions")
RegistClassMember(LuaColiseumRankWnd,"m_ArenaMyMaxRank")
function LuaColiseumRankWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	CommonDefs.AddOnClickListener(self.SeasonButton.gameObject,DelegateFactory.Action_GameObject(function (go)
	    self:OnSeasonButtonClick()
	end),false)

	CommonDefs.AddOnClickListener(self.IsKuaFuButton.gameObject,DelegateFactory.Action_GameObject(function (go)
	    self:OnIsKuaFuButtonClick()
	end),false)

	CommonDefs.AddOnClickListener(self.RankTypeButton.gameObject,DelegateFactory.Action_GameObject(function (go)
	    self:OnRankTypeButtonClick()
	end),false)

	CommonDefs.AddOnClickListener(self.LevelStageButton.gameObject,DelegateFactory.Action_GameObject(function (go)
	    self:OnLevelStageButtonClick()
	end),false)
    --@endregion EventBind end
end

function LuaColiseumRankWnd:Init()
	--self.m_MaxRankCount = 100
	self.m_RanksData = LuaColiseumMgr.m_RankData
	self.TimeLabel.text = LuaColiseumMgr:GetCurrentSeasonText()
	self.m_ArenaMyMaxRank = Arena_Setting.GetData().ArenaMyMaxRank
	self.KuaFuBottomLabel.text = SafeStringFormat3(LocalString.GetString("跨服榜单仅显示前%d名"),Arena_Setting.GetData().ArenaKuafuRankPlayerCount)
	self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function() 
            return self:NumOfRow()
        end,
        function(item,index) self:InitItem(item,index) end
    )
	self.TableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        local data = self.m_RanksData[row + 1]
		local playerId = data.playerId 
		CPlayerInfoMgr.ShowPlayerPopupMenu(playerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
    end)
	self.QnIncreseAndDecreaseButton.onValueChanged = DelegateFactory.Action_uint(function(v)
		self.m_PageIndex = v - 1
		self:OnPageChanged()
    end)
	self.QnIncreseAndDecreaseButton:SetMinMax(1, LuaColiseumMgr.m_RankWndMaxPage, 1)
	-- self.QnIncreseAndDecreaseButton:SetValue(1, true)
	self:InitMainPlayerInfo()
	self:InitQnTipButtons()
end

function LuaColiseumRankWnd:InitQnTipButtons()
	self:InitSeasonButton()
	self:InitIsKuaFuButton()
	self:InitRankTypeButton()
	self:InitLevelStageButton()
	self.RankTypeButton.gameObject:SetActive(not self.m_IsKuaFu)
	self.LevelStageButton.gameObject:SetActive(self.m_IsKuaFu)
	self.Page.gameObject:SetActive(self.m_IsKuaFu)
	self.m_PageIndex = 0
	self:OnPageChanged()
end

function LuaColiseumRankWnd:InitSeasonButton()
	self.m_SeasonActions = {}
	local seasonIdArray = {}
	local currentSeasonId = LuaArenaMgr:GetCurrentSeasonId()
	LuaArenaMgr:GetSeasonInfoClass().ForeachKey(function(key)
		table.insert(seasonIdArray,key)
	end)
	table.sort(seasonIdArray,function(a,b)
		return a < b
	end)
	for i = 2,#seasonIdArray do
		local key = seasonIdArray[i]
		local text = SafeStringFormat3(LocalString.GetString("第%d赛季"),key)
		local id = key
		table.insert(self.m_SeasonActions,PopupMenuItemData(text, DelegateFactory.Action_int(function (index) 
			self:OnSelectSeason(id)
			self.SeasonButton.Text = text
		end), false, nil))
		if currentSeasonId == key then
			self.m_SeasonIndex = key
			self.SeasonButton.Text = text
		end
	end
end

function LuaColiseumRankWnd:InitIsKuaFuButton()
	self.m_IsKuaFuActions = {}
	local kuaFuBtnTextArr = {LocalString.GetString("本服排名"),LocalString.GetString("跨服排名")}
	table.insert(self.m_IsKuaFuActions,PopupMenuItemData(kuaFuBtnTextArr[1], DelegateFactory.Action_int(function (index) 
		self:OnSelectIsKuaFu(false)
		self.IsKuaFuButton.Text = kuaFuBtnTextArr[1]
	end), false, nil))
	table.insert(self.m_IsKuaFuActions,PopupMenuItemData(kuaFuBtnTextArr[2], DelegateFactory.Action_int(function (index) 
		self:OnSelectIsKuaFu(true)
		self.IsKuaFuButton.Text = kuaFuBtnTextArr[2]
	end), false, nil))
	self.m_IsKuaFu = false
	self.IsKuaFuButton.Text = kuaFuBtnTextArr[1]
end

function LuaColiseumRankWnd:InitRankTypeButton()
	self.m_RankTypeActions = {}
	local rankTypeTextArr = {LocalString.GetString("一对一"),LocalString.GetString("二对二"),LocalString.GetString("三对三"),LocalString.GetString("四对四"),LocalString.GetString("五对五")}
	for i = 1,#rankTypeTextArr do
		local rankTypeIndex = i
		local text = rankTypeTextArr[i]
		table.insert(self.m_RankTypeActions,PopupMenuItemData(text, DelegateFactory.Action_int(function (index) 
			self:OnSelectRankType(rankTypeIndex)
			self.RankTypeButton.Text = text
		end), false, nil))
		if i == 1 then
			self.m_RankTypeIndex = i
			self.RankTypeButton.Text = text
		end
	end
	table.remove(self.m_RankTypeActions, 4)
end

function LuaColiseumRankWnd:InitLevelStageButton()
	self.m_LevelStageActions = {}
	local nameArr = {
        LocalString.GetString("新锐组"),
        LocalString.GetString("英武组"),
        LocalString.GetString("神勇组"),
        LocalString.GetString("天罡组"),
        LocalString.GetString("天元组")
    }
	local mystage = CBiWuDaHuiMgr.Inst:GetMyStage()
	mystage = mystage == 0 and 1 or mystage
	for i = 1,#nameArr do
		local stage = i
		local text = nameArr[i]
		table.insert(self.m_LevelStageActions,PopupMenuItemData(text, DelegateFactory.Action_int(function (index) 
			self:OnSelectLevelStage(stage)
			self.LevelStageButton.Text = text
		end), false, nil))
	end
	self.m_LevelStageIndex = mystage
	self.LevelStageButton.Text = nameArr[mystage]
end

function LuaColiseumRankWnd:OnEnable()
	g_ScriptEvent:AddListener("OnSendArenaRankInfo", self, "OnSendArenaRankInfo")
end

function LuaColiseumRankWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnSendArenaRankInfo", self, "OnSendArenaRankInfo")
end

function LuaColiseumRankWnd:OnSendArenaRankInfo()
	self.m_RanksData = LuaColiseumMgr.m_RankData
	self.TableView:ReloadData(true,true)
	self.MyScoreLabel.text = LuaColiseumMgr.m_RankWndMyScore
	self.RankImage.spriteName = self:GetRankImageByRankIndex(tonumber(LuaColiseumMgr.m_RankWndMyRank))
	self.MyRankLabel.enabled = (LuaColiseumMgr.m_RankWndMyRank > 3) or (LuaColiseumMgr.m_RankWndMyRank == 0)
	self.MyRankLabel.text = (LuaColiseumMgr.m_RankWndMyRank == 0) and LocalString.GetString("未上榜") or LuaColiseumMgr.m_RankWndMyRank 
	self.QnIncreseAndDecreaseButton:SetMinMax(1, LuaColiseumMgr.m_RankWndMaxPage, 1)
	self.QnIncreseAndDecreaseButton:SetValue(self.m_PageIndex + 1,false)
	self.QnIncreseAndDecreaseButton.ValueInput:ForceSetText(SafeStringFormat3("%d/%d",math.min(self.m_PageIndex + 1,LuaColiseumMgr.m_RankWndMaxPage),LuaColiseumMgr.m_RankWndMaxPage), true)
	self.Page.gameObject:SetActive(not self.m_IsKuaFu)
end

function LuaColiseumRankWnd:OnPageChanged()
	self.QnIncreseAndDecreaseButton.ValueInput:ForceSetText(SafeStringFormat3("%d/%d",math.min(self.m_PageIndex + 1,LuaColiseumMgr.m_RankWndMaxPage),LuaColiseumMgr.m_RankWndMaxPage), true)
	self.Page.gameObject:SetActive(not self.m_IsKuaFu)
	self.KuaFuBottomLabel.gameObject:SetActive(self.m_IsKuaFu)
	self.m_RanksData = {}
	self.TableView:ReloadData(true,true)
	if self.m_IsKuaFu then
		Gac2Gas.RequestArenaCrossServerRankInfo(self.m_SeasonIndex, self.m_LevelStageIndex)
		return
	end
	Gac2Gas.RequestArenaRankInfo(self.m_RankTypeIndex,self.m_PageIndex + 1,self.m_SeasonIndex)
end

function LuaColiseumRankWnd:NumOfRow()
	return #self.m_RanksData
end

function LuaColiseumRankWnd:InitMainPlayerInfo()
	self.RankImage.spriteName = ""
	self.MyRankLabel.text = LocalString.GetString("未上榜")
	self.MyScoreLabel.text = 0
	if not CClientMainPlayer.Inst then return end
	self.MyNameLabel.text = CClientMainPlayer.Inst.Name
	self.MyLevelLabel.text = SafeStringFormat3("Lv.%d",CClientMainPlayer.Inst.Level)
	local hasFeiSheng = CClientMainPlayer.Inst and CClientMainPlayer.Inst.HasFeiSheng or false
	self.MyLevelLabel.color = hasFeiSheng and NGUIText.ParseColor24("fe7900", 0) or Color.white
	self.MyEquipScoreLabel.text = CClientMainPlayer.Inst.EquipScore
	self.MainPlayerClassSprite.spriteName = Profession.GetIcon(CClientMainPlayer.Inst.Class)
	local gameServer = CLoginMgr.Inst:GetSelectedGameServer()
    self.MyServerNameLabel.text = gameServer and gameServer.name or ""
end

function LuaColiseumRankWnd:InitItem(item,index)
	local tf = item.transform
	local data = self.m_RanksData[index + 1]

	local rank = data.rank
	local playerName = data.playerName 
	local level = data.playerLevel
	local equipScore = data.playerEquipScore
	local hasFeiSheng = data.hasFeiSheng
	local score = data.playerScore
	local class = data.playerClass
	local serverName = data.serverName

	local rankLabel = tf:Find("RankLabel"):GetComponent(typeof(UILabel))
	local rankImage = tf:Find("RankLabel/RankImage"):GetComponent(typeof(UISprite))
	local levelLabel = tf:Find("LevelLabel"):GetComponent(typeof(UILabel))
	local classSprite = tf:Find("Career"):GetComponent(typeof(UISprite))
	rankLabel.text = rank
	if rank > self.m_ArenaMyMaxRank then
		rankLabel.text = self.m_ArenaMyMaxRank .. "+"
	end
	rankLabel.enabled = rank > 3
	rankImage.spriteName = self:GetRankImageByRankIndex(tonumber(rank))
	classSprite.spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), class))
	levelLabel.text = SafeStringFormat3("Lv.%d",level)
	levelLabel.color = hasFeiSheng and NGUIText.ParseColor24("fe7900", 0) or Color.white
	tf:Find("NameLabel"):GetComponent(typeof(UILabel)).text = playerName
	tf:Find("EquipScoreLabel"):GetComponent(typeof(UILabel)).text = equipScore
	tf:Find("ScoreLabel"):GetComponent(typeof(UILabel)).text = score
	tf:Find("ServerNameLabel"):GetComponent(typeof(UILabel)).text = serverName
	item:SetBackgroundTexture((index % 2 == 1) and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite)
end

function LuaColiseumRankWnd:GetRankImageByRankIndex(index)
	if index == 1 then
		return Constants.RankFirstSpriteName
	elseif index == 2 then
		return Constants.RankSecondSpriteName
	elseif index == 3 then
		return Constants.RankThirdSpriteName
	else
		return nil
	end
end

--@region UIEvent

function LuaColiseumRankWnd:OnSelectSeason(index)
	self.m_SeasonIndex = index
	self:OnPageChanged()
end

function LuaColiseumRankWnd:OnSelectIsKuaFu(isKuaFu)
	self.m_IsKuaFu = isKuaFu
	self.RankTypeButton.gameObject:SetActive(not self.m_IsKuaFu)
	self.LevelStageButton.gameObject:SetActive(self.m_IsKuaFu)
	self.KuaFuBottomLabel.gameObject:SetActive(not self.m_IsKuaFu)
	self:OnPageChanged()
end

function LuaColiseumRankWnd:OnSelectRankType(rankTypeIndex)
	self.m_RankTypeIndex = rankTypeIndex
	self:OnPageChanged()
end

function LuaColiseumRankWnd:OnSelectLevelStage(stage)
	self.m_LevelStageIndex = stage
	self:OnPageChanged()
end

function LuaColiseumRankWnd:OnSeasonButtonClick()
	CPlayerShopPopupMenuInfoMgr.ShowPlayerShopPopupMenu(Table2Array(self.m_SeasonActions, MakeArrayClass(PopupMenuItemData)), self.SeasonButton.transform, CPopupMenuInfoMgrAlignType.Bottom, 1, nil, nil, DelegateFactory.Action(function ()
        self.SeasonButton:SetTipStatus(false)
    end), 600,300)
end

function LuaColiseumRankWnd:OnIsKuaFuButtonClick()
	CPlayerShopPopupMenuInfoMgr.ShowPlayerShopPopupMenu(Table2Array(self.m_IsKuaFuActions, MakeArrayClass(PopupMenuItemData)), self.IsKuaFuButton.transform, CPopupMenuInfoMgrAlignType.Bottom, 1, nil, nil, DelegateFactory.Action(function ()
        self.IsKuaFuButton:SetTipStatus(false)
    end), 600,300)
end

function LuaColiseumRankWnd:OnRankTypeButtonClick()
	CPlayerShopPopupMenuInfoMgr.ShowPlayerShopPopupMenu(Table2Array(self.m_RankTypeActions, MakeArrayClass(PopupMenuItemData)), self.RankTypeButton.transform, CPopupMenuInfoMgrAlignType.Bottom, 1, nil, nil, DelegateFactory.Action(function ()
        self.RankTypeButton:SetTipStatus(false)
    end), 600,300)
end

function LuaColiseumRankWnd:OnLevelStageButtonClick()
	CPlayerShopPopupMenuInfoMgr.ShowPlayerShopPopupMenu(Table2Array(self.m_LevelStageActions, MakeArrayClass(PopupMenuItemData)), self.LevelStageButton.transform, CPopupMenuInfoMgrAlignType.Bottom, 1, nil, nil, DelegateFactory.Action(function ()
        self.LevelStageButton:SetTipStatus(false)
    end), 600,300)
end
--@endregion UIEvent

