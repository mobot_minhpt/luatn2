local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local UIGrid = import "UIGrid"
local UIEventListener = import "UIEventListener"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local Item_Item = import "L10.Game.Item_Item"
local GameObject = import "UnityEngine.GameObject"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUITexture = import "L10.UI.CUITexture"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
local CPlayerInfoMgrAlignType = import "CPlayerInfoMgr+AlignType"
local Animation = import "UnityEngine.Animation"

LuaQueQiaoXianQuHistoryTeammateWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaQueQiaoXianQuHistoryTeammateWnd, "TopTitle", "TopTitle", UILabel)
RegistChildComponent(LuaQueQiaoXianQuHistoryTeammateWnd, "AwardItem", "AwardItem", GameObject)
RegistChildComponent(LuaQueQiaoXianQuHistoryTeammateWnd, "TableView", "TableView", GameObject)
RegistChildComponent(LuaQueQiaoXianQuHistoryTeammateWnd, "templateNode", "templateNode", GameObject)
RegistChildComponent(LuaQueQiaoXianQuHistoryTeammateWnd, "TeammateGrid", "TeammateGrid", UIGrid)
RegistChildComponent(LuaQueQiaoXianQuHistoryTeammateWnd, "TeammateScrollView", "TeammateScrollView", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaQueQiaoXianQuHistoryTeammateWnd, "m_PartenerInfo")

function LuaQueQiaoXianQuHistoryTeammateWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaQueQiaoXianQuHistoryTeammateWnd:Init()
    self.TopTitle.text = g_MessageMgr:FormatMessage("2021QiXi_QueQiaoXianQu_Partner")
    if not LuaQiXi2021Mgr.lunarCalender then LuaQiXi2021Mgr.InitData() end
    self.m_PartenerInfo = LuaQiXi2021Mgr.partnerInfo
    CUICommonDef.ClearTransform(self.TeammateGrid.transform)
    self:InitAwardItem()
    self:InitTeammateTable()
    
    self.closeBtn = self.transform:Find("Panel/CloseButton").gameObject
    local animComp = self.transform:GetComponent(typeof(Animation))
    self.clickClose = false
    UIEventListener.Get(self.closeBtn).onClick =  DelegateFactory.VoidDelegate(function(_)
        if self.clickClose == false then
            self.clickClose = true
            animComp:Play("qixi2023queqiaoxianquhistoryteammatewnd_01")
            RegisterTickOnce(function()
                CUIManager.CloseUI(CLuaUIResources.QiXi2023QueQiaoXianQuHistoryTeammateWnd)
            end, 300)
        end
    end)
end

function LuaQueQiaoXianQuHistoryTeammateWnd:InitTeammateTable()
    if not self.m_PartenerInfo then return end
    local dataTable = self.m_PartenerInfo
    for _,data in ipairs(dataTable) do
        local go = CUICommonDef.AddChild(self.TeammateGrid.gameObject, self.templateNode)
        go:SetActive(true)
        self:InitPlayer(go,data)
    end
    self.TeammateGrid:Reposition()
end

function LuaQueQiaoXianQuHistoryTeammateWnd:InitAwardItem()
    local ItemId = QiXi_QiXi2021.GetData().AdditionalAwardItemId
    local item = Item_Item.GetData(ItemId)
    local ItemIcon = self.AwardItem.transform:Find("AwardItemCell/IconTexture")
    ItemIcon:GetComponent(typeof(CUITexture)):LoadMaterial(item.Icon)
    UIEventListener.Get(ItemIcon.gameObject).onClick =  DelegateFactory.VoidDelegate(
        function(go)
            CItemInfoMgr.ShowLinkItemTemplateInfo(ItemId)
        end
    )

    local GetAward = LuaQiXi2021Mgr.awardTimes >= 1

    local DisableSprite = self.AwardItem.transform:Find("DisableSprite").gameObject
    local Label = self.AwardItem.transform:Find("Label"):GetComponent(typeof(UILabel))
    if GetAward then
        DisableSprite:SetActive(true)
        Label.text = SafeStringFormat3( LocalString.GetString("[c][00FF60FF]已获得[-][c]"))
    else
        DisableSprite:SetActive(false)
        Label.text = SafeStringFormat3( LocalString.GetString("[c][FFFFFFB2]暂未获得[-][c]"))
    end
end

function LuaQueQiaoXianQuHistoryTeammateWnd:InitPlayer(go, data)
    if not go or not data then return end
    local player = go.transform:Find("icon").gameObject
    local portrait = go.transform:Find("icon"):GetComponent(typeof(CUITexture))
    local nameLabel = go.transform:Find("name"):GetComponent(typeof(UILabel))
    local dateLabel = go.transform:Find("dateText"):GetComponent(typeof(UILabel))
    nameLabel.text = data.name
    portrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(data.class, data.gender, -1), false)

    local date = os.date("*t",data.time)
    local key = SafeStringFormat3("%d-%02d-%02d",date.year,date.month,date.day)
    if self.m_PartenerInfo and CommonDefs.DictContains_LuaCall(LuaQiXi2021Mgr.lunarCalender,key) then
        dateLabel.text = CommonDefs.DictGetValue_LuaCall(LuaQiXi2021Mgr.lunarCalender,key)
    else 
        dateLabel.text = ""
    end


    CommonDefs.AddOnClickListener(player, DelegateFactory.Action_GameObject(function(go) self:OnPlayerClick(data.playerId) end), false)
end


function LuaQueQiaoXianQuHistoryTeammateWnd:OnPlayerClick(playerId)
    CPlayerInfoMgr.ShowPlayerPopupMenu(playerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), CPlayerInfoMgrAlignType.Default)
end


--@region UIEvent

--@endregion UIEvent

