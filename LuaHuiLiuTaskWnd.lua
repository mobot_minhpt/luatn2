local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local Color = import "UnityEngine.Color"
local LocalString = import "LocalString"
local CUITexture = import "L10.UI.CUITexture"
local Extensions = import "Extensions"
local Vector3 = import "UnityEngine.Vector3"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local DelegateFactory = import "DelegateFactory"
local UIEventListener = import "UIEventListener"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"

LuaHuiLiuTaskWnd = class()
RegistChildComponent(LuaHuiLiuTaskWnd,"closeBtn", "CloseButton", GameObject)
RegistChildComponent(LuaHuiLiuTaskWnd,"taskTemplate", GameObject)
RegistChildComponent(LuaHuiLiuTaskWnd,"taskFNode", GameObject)
RegistChildComponent(LuaHuiLiuTaskWnd,"taskLabel", UILabel)
RegistChildComponent(LuaHuiLiuTaskWnd,"bonus1", GameObject)
RegistChildComponent(LuaHuiLiuTaskWnd,"bonus2", GameObject)
RegistChildComponent(LuaHuiLiuTaskWnd,"bonus3", GameObject)
RegistChildComponent(LuaHuiLiuTaskWnd,"bonus4", GameObject)
RegistChildComponent(LuaHuiLiuTaskWnd,"curScoreLabel", UILabel)
RegistChildComponent(LuaHuiLiuTaskWnd,"tongyouBtn", GameObject)
RegistChildComponent(LuaHuiLiuTaskWnd,"fabuBtn", GameObject)
RegistChildComponent(LuaHuiLiuTaskWnd,"tipBtn", GameObject)
RegistChildComponent(LuaHuiLiuTaskWnd,"title1", GameObject)
RegistChildComponent(LuaHuiLiuTaskWnd,"title2", UILabel)
RegistChildComponent(LuaHuiLiuTaskWnd,"title3", UILabel)
RegistChildComponent(LuaHuiLiuTaskWnd,"tipText", UILabel)

--RegistClassMember(LuaHuiLiuTaskWnd, "maxChooseNum")

function LuaHuiLiuTaskWnd:Close()
	CUIManager.CloseUI(CLuaUIResources.HuiLiuTaskWnd)
end

function LuaHuiLiuTaskWnd:OnEnable()
	g_ScriptEvent:AddListener("RemoveHuiLiuPlayer", self, "Init")
end

function LuaHuiLiuTaskWnd:OnDisable()
	g_ScriptEvent:RemoveListener("RemoveHuiLiuPlayer", self, "Init")
end

function LuaHuiLiuTaskWnd:Init()
	local onCloseClick = function(go)
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.closeBtn,DelegateFactory.Action_GameObject(onCloseClick),false)

	local onTipClick = function(go)
		g_MessageMgr:ShowMessage('HuiGuiJieBanTip')
	end
	CommonDefs.AddOnClickListener(self.tipBtn,DelegateFactory.Action_GameObject(onTipClick),false)

	self.tipText.text = g_MessageMgr:FormatMessage('HuiGuiJieBanTimeTip')

  self.taskTemplate:SetActive(false)

  local huiLiuData = CClientMainPlayer.Inst.PlayProp.HuiGuiJieBanData
  local taskCount = HuiGuiJieBan_Task.GetDataCount()
  local taskNumTable = {}

  for i=1,taskCount do
    if CommonDefs.DictContains(huiLiuData.DailyTask, typeof(UInt16), i) then
      local taskCompleteNum = CommonDefs.DictGetValue(huiLiuData.DailyTask, typeof(UInt16), i)
      table.insert(taskNumTable,taskCompleteNum)
    else
      table.insert(taskNumTable,0)
    end
  end

	self.curScoreLabel.text = huiLiuData.TotalScore

  self:InitTask(taskCount,taskNumTable)

	self:InitBonus()

	if huiLiuData.JieBanPlayerId > 0 then
		self.title1:SetActive(true)
		self.title2.gameObject:SetActive(false)
		self.title3.gameObject:SetActive(false)
		local onTongyouClick = function(go)
			CUIManager.ShowUI(CLuaUIResources.HuiLiuFriendWnd)
		end
		CommonDefs.AddOnClickListener(self.tongyouBtn,DelegateFactory.Action_GameObject(onTongyouClick),false)
	else
		if huiLiuData.PlayerMark == 0 then -- active
			self.title1:SetActive(false)
			self.title2.gameObject:SetActive(true)
			self.title2.text = g_MessageMgr:FormatMessage('HuiGuiJieBanActivePlayer')
			self.title3.gameObject:SetActive(false)
		else -- liushi
			self.title1:SetActive(false)
			self.title2.gameObject:SetActive(false)
			self.title3.gameObject:SetActive(true)
			self.title3.text = g_MessageMgr:FormatMessage('HuiGuiJieBanNonActivePlayer')
		end
		local onTongyouClick = function(go)
			g_MessageMgr:ShowMessage('HuiGuiJieBanNoTongyouTip')
		end
		CommonDefs.AddOnClickListener(self.tongyouBtn,DelegateFactory.Action_GameObject(onTongyouClick),false)
	end

	if huiLiuData.PlayerMark == 0 then -- active
		self.fabuBtn.transform:Find('text'):GetComponent(typeof(UILabel)).text = LocalString.GetString('召回好友')
		local onClick = function(go)
			Gac2Gas.RequestLiuShiFriends()
		end
		CommonDefs.AddOnClickListener(self.fabuBtn,DelegateFactory.Action_GameObject(onClick),false)
	else -- liushi
		self.fabuBtn.transform:Find('text'):GetComponent(typeof(UILabel)).text = LocalString.GetString('发布归来')
		local onClick = function(go)
			Gac2Gas.RequestPlayerInviters()
		end
		CommonDefs.AddOnClickListener(self.fabuBtn,DelegateFactory.Action_GameObject(onClick),false)
	end
end

function LuaHuiLiuTaskWnd:InitTask(taskCount,dataTable)
  local completeNum = 0
  local taskWidth = 245
  local huiLiuData = CClientMainPlayer.Inst.PlayProp.HuiGuiJieBanData

	Extensions.RemoveAllChildren(self.taskFNode.transform)

  for i,v in ipairs(dataTable) do
    local taskData = HuiGuiJieBan_Task.GetData(i)
    local totalNum = taskData.DailyTimes

    local node = NGUITools.AddChild(self.taskFNode,self.taskTemplate)
    node:SetActive(true)
    node.transform.localPosition = Vector3((i-1) * taskWidth,0,0)
    node.transform:Find('name'):GetComponent(typeof(UILabel)).text = taskData.TaskDesc
    node.transform:Find('tipText'):GetComponent(typeof(UILabel)).text = taskData.Score .. LocalString.GetString('积分')

    if v >= totalNum then
      completeNum = completeNum + 1
			node.transform:Find('tipText1'):GetComponent(typeof(UILabel)).text = LocalString.GetString('已完成')
			node.transform:Find('tipText1'):GetComponent(typeof(UILabel)).color = Color.green
		else
			if huiLiuData.JieBanPlayerId > 0 then
				node.transform:Find('tipText1'):GetComponent(typeof(UILabel)).text = v .. '/' .. taskData.DailyTimes
			else
				node.transform:Find('tipText1'):GetComponent(typeof(UILabel)).text = LocalString.GetString('暂无同游')
				node.transform:Find('tipText1'):GetComponent(typeof(UILabel)).color = Color.red
			end
    end
  end

  self.taskLabel.text = completeNum .. '/' .. taskCount
end

function LuaHuiLiuTaskWnd:InitBonus()
  local huiLiuData = CClientMainPlayer.Inst.PlayProp.HuiGuiJieBanData

	local data1Table = g_LuaUtil:StrSplit(HuiGuiJieBan_Reward.GetData(1).Items,";")
	self:InitSingleBonus(1,self.bonus1,data1Table[1],huiLiuData.RewardFlag:GetBit(1))

	local data2Table = g_LuaUtil:StrSplit(HuiGuiJieBan_Reward.GetData(2).Items,";")
	self:InitSingleBonus(1,self.bonus2,data2Table[1],huiLiuData.RewardFlag:GetBit(2))
	self:InitSingleBonus(2,self.bonus2,data2Table[2],huiLiuData.RewardFlag:GetBit(2))

	local data3Table = g_LuaUtil:StrSplit(HuiGuiJieBan_Reward.GetData(3).Items,";")
	self:InitSingleBonus(1,self.bonus3,data3Table[1],huiLiuData.RewardFlag:GetBit(3))
	self:InitSingleBonus(2,self.bonus3,data3Table[2],huiLiuData.RewardFlag:GetBit(3))
	self:InitSingleBonus(3,self.bonus3,data3Table[3],huiLiuData.RewardFlag:GetBit(3))

	local data4Table = g_LuaUtil:StrSplit(HuiGuiJieBan_Reward.GetData(4).Items,";")
	self:InitSingleBonus(1,self.bonus4,data4Table[1],huiLiuData.RewardFlag:GetBit(4))
	self:InitSingleBonus(2,self.bonus4,data4Table[2],huiLiuData.RewardFlag:GetBit(4))
	self:InitSingleBonus(3,self.bonus4,data4Table[3],huiLiuData.RewardFlag:GetBit(4))
end

function LuaHuiLiuTaskWnd:InitSingleBonus(index,fNode,data,getState)
	local bonusIcon = fNode.transform:Find('gift' .. index .. '/IconTexture'):GetComponent(typeof(CUITexture))
	local getNode = fNode.transform:Find('gift' .. index .. '/GameObject').gameObject
	local getNodeC = fNode.transform:Find('gift' .. index .. '/GameObject/wancheng').gameObject
	local getNodeL = fNode.transform:Find('gift' .. index .. '/GameObject/loc').gameObject
	local clickNode = fNode.transform:Find('gift' .. index).gameObject
	getNode:SetActive(true)
	if getState then
		getNodeC:SetActive(true)
		getNodeL:SetActive(false)
	else
		getNodeC:SetActive(false)
		getNodeL:SetActive(true)
	end

	local dataTable = g_LuaUtil:StrSplit(data,",")
	local itemId = tonumber(dataTable[1])
	local itemNum = tonumber(dataTable[2])

	local itemData = CItemMgr.Inst:GetItemTemplate(itemId)
	bonusIcon:LoadMaterial(itemData.Icon)

	UIEventListener.Get(clickNode).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(clickNode).onClick, DelegateFactory.VoidDelegate(function (go)
		CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType.Default, 0, 0, 0, 0)
	end), true)
end

return LuaHuiLiuTaskWnd
