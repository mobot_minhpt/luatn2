local CChatMgr = import "L10.Game.CChatMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CIMMgr = import "L10.Game.CIMMgr"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local CUIManager = import "L10.UI.CUIManager"
local CUITexture = import "L10.UI.CUITexture"
local DelegateFactory = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local LocalString = import "LocalString"
local Time = import "UnityEngine.Time"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local UIInput = import "UIInput"
local CChatInput = import "L10.UI.CChatInput"
local CChatInputMgrEParentType = import "L10.UI.CChatInputMgr+EParentType"
local CChatLinkMgr = import "CChatLinkMgr"
local CChatInputMgr = import "L10.UI.CChatInputMgr"
local EChatPanel = import "L10.Game.EChatPanel"

LuaPersonalSpaceForwardWnd=class()
RegistChildComponent(LuaPersonalSpaceForwardWnd,"backBtn", GameObject)
RegistChildComponent(LuaPersonalSpaceForwardWnd,"emotionBtn", GameObject)
RegistChildComponent(LuaPersonalSpaceForwardWnd,"statusInput", CChatInput)
RegistChildComponent(LuaPersonalSpaceForwardWnd,"fakeStatusLabel", UILabel)
RegistChildComponent(LuaPersonalSpaceForwardWnd,"forwardIcon", GameObject)
RegistChildComponent(LuaPersonalSpaceForwardWnd,"forwardName", UILabel)
RegistChildComponent(LuaPersonalSpaceForwardWnd,"forwardLv", UILabel)
RegistChildComponent(LuaPersonalSpaceForwardWnd,"forwardStatus", UILabel)

RegistClassMember(LuaPersonalSpaceForwardWnd, "defaultInputString")
RegistClassMember(LuaPersonalSpaceForwardWnd, "MaxStringNum")
RegistClassMember(LuaPersonalSpaceForwardWnd, "forwardInfo")
RegistClassMember(LuaPersonalSpaceForwardWnd, "totalInfo")
RegistClassMember(LuaPersonalSpaceForwardWnd, "statusInputScript")
RegistClassMember(LuaPersonalSpaceForwardWnd, "lastSendTime")
RegistClassMember(LuaPersonalSpaceForwardWnd, "m_DefaultChatInputListener")
RegistClassMember(LuaPersonalSpaceForwardWnd, "submitBtn")

function LuaPersonalSpaceForwardWnd:CloseWnd()
  CUIManager.CloseUI(CLuaUIResources.PersonalSpaceForwardLuaWnd)
end

function LuaPersonalSpaceForwardWnd:Init()
  UIEventListener.Get(self.backBtn).onClick = DelegateFactory.VoidDelegate(function (p)
    self:CloseWnd()
  end)
  self.MaxStringNum = 32
  self.defaultInputString = SafeStringFormat3(LocalString.GetString("转发说点什么吧...(最多%s个字)"),self.MaxStringNum)
  self.totalInfo = LuaPersonalSpaceMgrReal.forwardInfo
  self.lastSendTime = 0

  self.forwardInfo = self.totalInfo.forwardmoment
  if not self.forwardInfo or not self.forwardInfo.id or self.forwardInfo.id <= 0 then
    self.forwardInfo.roleid = self.totalInfo.roleid
    self.forwardInfo.rolename = self.totalInfo.rolename
    self.forwardInfo.clazz = self.totalInfo.clazz
    self.forwardInfo.gender = self.totalInfo.gender
    self.forwardInfo.text = self.totalInfo.text
    self.forwardInfo.imglist = self.totalInfo.imglist
    self.forwardInfo.splevel = self.totalInfo.splevel
    self.forwardInfo.photo = self.totalInfo.photo
  end

  self.statusInputScript = self.statusInput.transform:Find('StatusText'):GetComponent(typeof(UIInput))
  self.statusInputScript.transform:Find('Label'):GetComponent(typeof(UILabel)).text = self.defaultInputString
  self.submitBtn = self.statusInput.transform:Find('SendMsgButton').gameObject

  if not self.forwardInfo then
    self:CloseWNd()
  end

  if not self.m_DefaultChatInputListener then
    self.m_DefaultChatInputListener = LuaPersonalSpaceMgrReal.GetChatListener(self.statusInput)
  end

  self.statusInput.OnSend = DelegateFactory.Action_string(function (msg)
    self:OnSubmitBtnClick(msg)
  end)

  UIEventListener.Get(self.emotionBtn).onClick = DelegateFactory.VoidDelegate(function (p)
    self:OnEmotionButtonClick()
  end)

  if self.forwardInfo.rolename or self.forwardInfo.rolename ~= '' then
    self.forwardName.text = self.forwardInfo.rolename
  else
    self.forwarName.text = self.forwardInfo.roleid
  end

  LuaPersonalSpaceMgrReal.SetTextMore(self.forwardInfo.text,self.forwardStatus)

  if self.forwardInfo.roleid == CIMMgr.PERSONAL_SPACE_HOT_POINT_ID then
    self.forwardLv.text = ''
    self.forwardIcon:GetComponent(typeof(CUITexture)):LoadNPCPortrait(LuaPersonalSpaceMgrReal.GetSpaceGMIcon(), false)
  elseif CPersonalSpaceMgr.CheckAndLoadGongZhongHaoPortraitPic(self.forwardInfo.roleid,self.forwardIcon,self.forwardLv) then
  else
    self.forwardLv.text = self.forwardInfo.grade
    LuaPersonalSpaceMgrReal.LoadPortraitPic(self.forwardInfo.photo, self.forwardInfo.clazz, self.forwardInfo.gender, self.forwardIcon, self.forwardInfo.expression_base)
  end

  if self.forwardInfo.roleid ~= CClientMainPlayer.Inst.Id then
    CPersonalSpaceMgr.Inst:addPlayerLinkBtn(self.forwardIcon.transform:Find("button").gameObject, self.forwardInfo.roleid, EnumReportId_lua.eMengDaoMoment, self.forwardInfo.text, nil, self.totalInfo.id, 0)
  end

  self:OnChangeText()
end

function LuaPersonalSpaceForwardWnd:OnChangeText()
  local trueValue = self.statusInputScript.value
  if not trueValue or trueValue == '' then
    trueValue = self.defaultInputString
  end

  local showText = ''
  local recentforwards = self.totalInfo.previousforwards
  if recentforwards then
    for i,v in ipairs(recentforwards) do
      if v and v.roleinfo then
        showText = ((CPersonalSpaceMgr.ForwardDivideString .. CPersonalSpaceMgr.GeneratePlayerName(v.roleinfo.roleid, v.roleinfo.rolename)) .. v.text) .. showText
      end
    end
  end

  --if self.totalInfo.id > 0 then
  --  showText = showText .. ((CPersonalSpaceMgr.ForwardDivideString .. CPersonalSpaceMgr.GeneratePlayerName(self.totalInfo.roleid, self.totalInfo.rolename)) .. self.totalInfo.text)
  --end
  showText = CChatMgr.Inst:FilterYangYangEmotion(showText)
  self.fakeStatusLabel.text = (("[c][00000000]" .. trueValue) .. "[-][/c]") .. CChatLinkMgr.TranslateToNGUIText(showText, false)
end

function LuaPersonalSpaceForwardWnd:OnEmotionButtonClick(go)
  CChatInputMgr.ShowChatInputWnd(CChatInputMgrEParentType.PersonalSpace, self.m_DefaultChatInputListener, go, EChatPanel.Undefined, 0, 0)
end

function LuaPersonalSpaceForwardWnd:OnSubmitBtnClick(msg)
  local msg = msg
  if System.String.IsNullOrEmpty(msg) then
    g_MessageMgr:ShowMessage("ENTER_TEXT_TIP")
    return
  end

  if Time.realtimeSinceStartup - self.lastSendTime < 1 then
    return
  end
  self.lastSendTime = Time.realtimeSinceStartup

  CPersonalSpaceMgr.Inst:ForwardMoment(msg, self.totalInfo.id, DelegateFactory.Action_string_CPersonalSpace_AdvRet(function (textAfterFilter, data)
    if data.code == 0 then
      EventManager.BroadcastInternalForLua(EnumEventType.PersonalSpaceAddNewMoment, {textAfterFilter, data})
      g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString(data.msg))
    else
      g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString(data.msg))
    end
  end), DelegateFactory.Action(function ()
    g_MessageMgr:ShowMessage("INGAME_MENGDAO_SHARE_FAILED")
  end))
  --CommonDefs.GetComponent_GameObject_Type(self.submitBtn, typeof(CButton)).enabled = false
  --self.submitBtn:SetActive(false)
  LuaPersonalSpaceMgrReal.forwardInfo = nil
  self:CloseWnd()
end

function LuaPersonalSpaceForwardWnd:OnEnable()
  --g_ScriptEvent:AddListener("UpdateTaoQuanInfo", self, "UpdateInfo")
end

function LuaPersonalSpaceForwardWnd:OnDisable()
  --g_ScriptEvent:RemoveListener("UpdateTaoQuanInfo", self, "UpdateInfo")
end

function LuaPersonalSpaceForwardWnd:OnDestroy()

end

return LuaPersonalSpaceForwardWnd
