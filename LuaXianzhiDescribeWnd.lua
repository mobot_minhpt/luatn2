local GameObject  = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local UITable = import "UITable"
local CUITexture = import "L10.UI.CUITexture"
local CChatLinkMgr = import "CChatLinkMgr"

CLuaXianzhiDescribeWnd = class()
RegistChildComponent(CLuaXianzhiDescribeWnd, "Item", GameObject)
RegistChildComponent(CLuaXianzhiDescribeWnd, "Table", UITable)
RegistChildComponent(CLuaXianzhiDescribeWnd, "DesTxt1", UILabel)
RegistChildComponent(CLuaXianzhiDescribeWnd, "DesTxt2", UILabel)

--初始化
function CLuaXianzhiDescribeWnd:Init()
	local data = CLuaXianzhiMgr.SelectData
	if data == nil then return end
	
	local titleTypeData = XianZhi_TitleType.GetData(data.TitleType)
	if titleTypeData == nil then return end

	local addr = CLuaXianzhiMgr.GetXianZhiLocationByRegID()
	self:InitItem(self.Item,data.TitleName,titleTypeData.Icon,addr)
	self:InitDes(titleTypeData.Des,data.Des)
end

function CLuaXianzhiDescribeWnd:InitDes(des1,des2)
	self.DesTxt1.text = CChatLinkMgr.TranslateToNGUIText(des1);
	self.DesTxt2.text = CChatLinkMgr.TranslateToNGUIText(des2)
	self.Table:Reposition()
end

function CLuaXianzhiDescribeWnd:InitItem(itemgo,xzname,xzicon,addr) 
	local icon = itemgo.transform:Find("icon"):GetComponent(typeof(CUITexture))
	local name = itemgo.transform:Find("name"):GetComponent(typeof(UILabel))
	local addrname = itemgo.transform:Find("addr"):GetComponent(typeof(UILabel))
	icon:LoadMaterial("UI/Texture/Skill/Material/"..xzicon..".mat")
	name.text = xzname
	addrname.text = addr
end

return CLuaXianzhiDescribeWnd
