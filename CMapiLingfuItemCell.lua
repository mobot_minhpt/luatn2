-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CIndirectUIResources = import "L10.UI.CIndirectUIResources"
local CItemMgr = import "L10.Game.CItemMgr"
local CLingfuBaptizeWnd = import "L10.UI.CLingfuBaptizeWnd"
local CLingfuQianghuaWnd = import "L10.UI.CLingfuQianghuaWnd"
local CLingfuView = import "L10.UI.CLingfuView"
local CMapiLingfuItemCell = import "L10.UI.CMapiLingfuItemCell"
local CommonDefs = import "L10.Game.CommonDefs"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local CZuoQiEquipProp = import "L10.Game.CZuoQiEquipProp"
local CZuoQiMgr = import "L10.Game.CZuoQiMgr"
local DelegateFactory = import "DelegateFactory"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EnumLingfuBaptizeType = import "L10.UI.EnumLingfuBaptizeType"
local Gac2Gas = import "L10.Game.Gac2Gas"
local Item_Item = import "L10.Game.Item_Item"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local String = import "System.String"
local StringActionKeyValuePair = import "L10.Game.StringActionKeyValuePair"
local ZuoQi_Setting = import "L10.Game.ZuoQi_Setting"
CMapiLingfuItemCell.m_Init_String_CCommonItem_Int32_Action_Action_Boolean_CS2LuaHook = function (this, zqId, item, openHoleNum, act, checkAct, bReclaimMode) 
    this.zuoqiId = zqId
    this.lingfuId = item.Id
    this.selectSprite.gameObject:SetActive(false)
    this.checkBox.gameObject:SetActive(bReclaimMode)

    local itemdata = Item_Item.GetData(item.TemplateId)
    if itemdata ~= nil and itemdata.Icon ~= nil then
        this.texture:LoadMaterial(itemdata.Icon)
        this.qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(itemdata, item, false)
        local equipPos = CZuoQiMgr.GetEquipHole(item.TemplateId)
        this.disableSprite:SetActive(openHoleNum < equipPos)
        this.canPutOn = openHoleNum >= equipPos
    end

    this.checkBox:SetSelected(false, true)

    this.onClickAction = act
    this.onCheckBoxValueChanged = checkAct
end
CMapiLingfuItemCell.m_RequestFixLingfu_CS2LuaHook = function (this, equipId) 

    local item = CItemMgr.Inst:GetById(equipId)
    if item ~= nil then
        local data = item.Item.LingfuItemInfo
        if data ~= nil then
            local setting = ZuoQi_Setting.GetData()
            local fullDuration = setting.LingfuDurability[data.Quality - 1]
            local duration = data.Duration
            local silverNeed = math.floor(math.floor((fullDuration - duration) * setting.DurabilityFixCost))
            if silverNeed > 0 then
                local itemdata = Item_Item.GetData(item.TemplateId)
                local msg = g_MessageMgr:FormatMessage("Mapi_Fix_Lingfu_Confirm", silverNeed, itemdata.Name)
                MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function () 
                    Gac2Gas.RequestFixMapiLingfu(equipId, "")
                end), nil, nil, nil, false)
            else
                g_MessageMgr:ShowMessage("Mapi_Fix_Lingfu_Already_Full")
            end
        end
    end
    CUIManager.CloseUI(CIndirectUIResources.ItemInfoWnd)
end
CMapiLingfuItemCell.m_PutOnLingfuEquip_CS2LuaHook = function (this) 
    if this.lingfuId ~= nil then
        if CClientMainPlayer.Inst == nil or CLingfuView.sCurZuoQiId == nil then
            return
        end

        if not CZuoQiMgr.Inst:CheckMapiNotInMajiu(CLingfuView.sCurZuoQiId) then
            return
        end

        local zqdata = CZuoQiMgr.Inst:GetZuoQiById(CLingfuView.sCurZuoQiId)
        if zqdata == nil or zqdata.mapiInfo == nil then
            return
        end

        local item = CItemMgr.Inst:GetById(this.lingfuId)
        if item == nil then
            return
        end

        local lingfuPos = CZuoQiMgr.GetLingfuPosByTemplateId(item.TemplateId)

        local bagPos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, this.lingfuId)
        if bagPos > 0 then
            if this.canPutOn then
                local equip = nil
                if (function () 
                    local __try_get_result
                    __try_get_result, equip = CommonDefs.DictTryGet(CClientMainPlayer.Inst.ItemProp.ZuoQiEquip, typeof(String), CLingfuView.sCurZuoQiId, typeof(CZuoQiEquipProp))
                    return __try_get_result
                end)() and not System.String.IsNullOrEmpty(equip.Equip[lingfuPos]) then
                    local msg = g_MessageMgr:FormatMessage("Mapi_PutOn_Equip_Pos_Not_Available")
                    MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function () 
                        Gac2Gas.RequestPutOnZuoqiEquip(this.zuoqiId, this.lingfuId, bagPos)
                    end), nil, nil, nil, false)
                else
                    Gac2Gas.RequestPutOnZuoqiEquip(this.zuoqiId, this.lingfuId, bagPos)
                end

                CUIManager.CloseUI(CIndirectUIResources.ItemInfoWnd)
            else
                g_MessageMgr:ShowMessage("Hole_Is_Not_Available")
            end
        end
    end
end
CMapiLingfuItemCell.m_ReclaimLingfuItem_CS2LuaHook = function (this, equipId) 
    local item = CItemMgr.Inst:GetById(equipId)
    if item.Item.LingfuItemInfo ~= nil then
        if item.Item.LingfuItemInfo.Quality >= 3 then
            local data = Item_Item.GetData(item.TemplateId)
            local msg = g_MessageMgr:FormatMessage("Mapi_Reclaim_Lingfu_Confirm", data.Name)
            MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function () 
                Gac2Gas.RequestReclaimLingfuItem(equipId)
            end), nil, nil, nil, false)
        else
            Gac2Gas.RequestReclaimLingfuItem(equipId)
        end
    end
    CUIManager.CloseUI(CIndirectUIResources.ItemInfoWnd)
end
CMapiLingfuItemCell.m_RequestQianghuaLingfu_CS2LuaHook = function (this, equipId) 
    CUIManager.CloseUI(CIndirectUIResources.ItemInfoWnd)

    CLingfuQianghuaWnd.sQianghuaLingfuId = equipId
    CUIManager.ShowUI(CUIResources.LingfuQianghuaWnd)
end
CMapiLingfuItemCell.m_GetActionPairs_CS2LuaHook = function (this, itemId, templateId) 
    local actionPairs = CreateFromClass(MakeGenericClass(List, StringActionKeyValuePair))
    local zhuangbeiAction = CreateFromClass(StringActionKeyValuePair, LocalString.GetString("装备"), MakeDelegateFromCSFunction(this.PutOnLingfuEquip, Action0, this))
    local xilianCitiaoAction = CreateFromClass(StringActionKeyValuePair, LocalString.GetString("洗炼词条"), DelegateFactory.Action(function () 
        CLingfuBaptizeWnd.sCurBaptizeLingfuId = itemId
        CLingfuBaptizeWnd.sType = EnumLingfuBaptizeType.eWord
        CUIManager.CloseUI(CIndirectUIResources.ItemInfoWnd)
        CUIManager.ShowUI(CUIResources.LingfuBaptizeWnd)
    end))
    local xilianTaozhuangAction = CreateFromClass(StringActionKeyValuePair, LocalString.GetString("洗炼套装"), DelegateFactory.Action(function () 
        CLingfuBaptizeWnd.sCurBaptizeLingfuId = itemId
        CLingfuBaptizeWnd.sType = EnumLingfuBaptizeType.eTaozhuang
        CUIManager.CloseUI(CIndirectUIResources.ItemInfoWnd)
        CUIManager.ShowUI(CUIResources.LingfuBaptizeWnd)
    end))
    local huishouAction = CreateFromClass(StringActionKeyValuePair, LocalString.GetString("熔炼"), DelegateFactory.Action(function () 
        this:ReclaimLingfuItem(itemId)
    end))
    local fixAction = CreateFromClass(StringActionKeyValuePair, LocalString.GetString("修复"), DelegateFactory.Action(function () 
        this:RequestFixLingfu(itemId)
    end))

    CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), zhuangbeiAction)

    local item = CItemMgr.Inst:GetById(itemId)
    if item ~= nil and (CZuoQiMgr.GetLingfuPosByTemplateId(item.TemplateId) == 1 or CZuoQiMgr.GetLingfuPosByTemplateId(item.TemplateId) == 3 or CZuoQiMgr.GetLingfuPosByTemplateId(item.TemplateId) == 5) then
        CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), xilianCitiaoAction)
    end
    CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), xilianTaozhuangAction)
    CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), huishouAction)
    CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), fixAction)

    if item ~= nil and item.Item.LingfuItemInfo ~= nil and item.Item.LingfuItemInfo.Quality >= 3 then
        local qianghuaAction = CreateFromClass(StringActionKeyValuePair, LocalString.GetString("强化"), DelegateFactory.Action(function () 
            this:RequestQianghuaLingfu(itemId)
        end))
        CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), qianghuaAction)
    end

    return actionPairs
end
