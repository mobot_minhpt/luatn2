local CMainPlayerEquipmentFrame = import "L10.UI.MainPlayer.CMainPlayerEquipmentFrame"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local MessageWndManager = import "L10.UI.MessageWndManager"
local StringActionKeyValuePair = import "L10.Game.StringActionKeyValuePair"

CMainPlayerEquipmentFrame.m_OnFix_CS2LuaHook = function(this)
end

CMainPlayerEquipmentFrame.m_InitFixAction_CS2LuaHook = function(this, actionPair)
    local fixOneItemAction = CreateFromClass(StringActionKeyValuePair, LocalString.GetString("修理单件"), DelegateFactory.Action(function ()
        Gac2Gas.RequestRepairEquip(EnumToInt(EnumItemPlace.Body), this.SelectedPos)
    end))
    CommonDefs.ListAdd(actionPair.DetailActionPairs, typeof(StringActionKeyValuePair), fixOneItemAction)
    local fixAllItemAction = CreateFromClass(StringActionKeyValuePair, LocalString.GetString("修理全部"), DelegateFactory.Action(function ()
        Gac2Gas.RequestRepairAllBodyEquip(false)
    end))
    CommonDefs.ListAdd(actionPair.DetailActionPairs, typeof(StringActionKeyValuePair), fixAllItemAction)
end

function Gas2Gac.RepairAllBodyEquipConfirm(needFreeSilver)
    local txt = g_MessageMgr:FormatMessage("Repaire_AllBodyEquip_Confirm", needFreeSilver)
    MessageWndManager.ShowOKCancelMessage(txt, DelegateFactory.Action(function ()
        Gac2Gas.RequestRepairAllBodyEquip(true)
    end), nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
end