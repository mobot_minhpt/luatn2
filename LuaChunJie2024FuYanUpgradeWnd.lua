
LuaChunJie2024FuYanUpgradeWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaChunJie2024FuYanUpgradeWnd, "roundTime", "RoundTime", UILabel)
--@endregion RegistChildComponent end

RegistClassMember(LuaChunJie2024FuYanUpgradeWnd, "propertyTemplate")
RegistClassMember(LuaChunJie2024FuYanUpgradeWnd, "levelUpRoot")
RegistClassMember(LuaChunJie2024FuYanUpgradeWnd, "levelMaxRoot")

RegistClassMember(LuaChunJie2024FuYanUpgradeWnd, "level")

function LuaChunJie2024FuYanUpgradeWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

    local anchor = self.transform:Find("Anchor")
    local beiShiGo = anchor:Find("Left/BeiShi").gameObject
    UIEventListener.Get(beiShiGo).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBeiShiClick()
	end)

    self.levelUpRoot = anchor:Find("Right/LevelUp")
    self.levelMaxRoot = anchor:Find("Right/LevelMax")
    self.propertyTemplate = anchor:Find("Right/PropertyTemplate").gameObject
    self.propertyTemplate:SetActive(false)
end

function LuaChunJie2024FuYanUpgradeWnd:Init()
    local settingData = ChunJie2024_FuYanSetting.GetData()
	self.roundTime.text = SafeStringFormat3(LocalString.GetString("[FF5050]今天[-] %s结束"), settingData.CaiMaiEndTime)

    self.level = 4
    self:UpdateLevel()
end

function LuaChunJie2024FuYanUpgradeWnd:UpdateLevel()
    local isLevelMax = self.level == 4
    self.levelUpRoot.gameObject:SetActive(not isLevelMax)
    self.levelMaxRoot.gameObject:SetActive(isLevelMax)

    local root = isLevelMax and self.levelMaxRoot or self.levelUpRoot
    local currentEffectGrid = root:Find("CurrentEffect/Grid"):GetComponent(typeof(UIGrid))
    Extensions.RemoveAllChildren(currentEffectGrid.transform)
    local effectList = self:GetEffectList(self.level)
    for _, data in ipairs(effectList) do
        local child = NGUITools.AddChild(currentEffectGrid.gameObject, self.propertyTemplate)
        child:SetActive(true)

        child.transform:Find("Name"):GetComponent(typeof(UILabel)).text = data.name
        child.transform:Find("Value"):GetComponent(typeof(UILabel)).text = SafeStringFormat3("+%d", data.value)
        child.transform:Find("Change").gameObject:SetActive(false)
    end
    currentEffectGrid:Reposition()

    if not isLevelMax then
        self.levelUpRoot:Find("Cost"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("福宴采买金10"))

        local tipButton = self.levelUpRoot:Find("MoneyNeed/Sprite").gameObject
        UIEventListener.Get(tipButton).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnTipButtonClick()
        end)

        local nextEffectGrid = root:Find("NextEffect/Grid"):GetComponent(typeof(UIGrid))
        Extensions.RemoveAllChildren(nextEffectGrid.transform)
        local nextEffectList = self:GetEffectList(self.level + 1)
        for _, data in ipairs(nextEffectList) do
            local child = NGUITools.AddChild(nextEffectGrid.gameObject, self.propertyTemplate)
            child:SetActive(true)
            child.transform:Find("Name"):GetComponent(typeof(UILabel)).text = data.name
            child.transform:Find("Value"):GetComponent(typeof(UILabel)).text = data.value

            local beforeValue = 0
            for __, data1 in ipairs(effectList) do
                if data1.name == data.name then
                    beforeValue = data1.value
                    break
                end
            end
            local changeValue = data.value - beforeValue
            local change = child.transform:Find("Change")
            change.gameObject:SetActive(changeValue > 0)
            if changeValue > 0 then
                change:GetComponent(typeof(UILabel)).text = changeValue
            end
        end
        nextEffectGrid:Reposition()

        UIEventListener.Get(self.levelUpRoot:Find("Button").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnUpgradeButtonClick()
        end)
    end
end

function LuaChunJie2024FuYanUpgradeWnd:GetEffectList(level)
    local list = {}
    local effectStr = ChunJie2024_FuYanUpdate.GetData(level).Effect
    for name, value in string.gmatch(effectStr, "([^,;]+),(%d+)") do
        table.insert(list, {name = name, value = value})
    end
    return list
end

--@region UIEvent

function LuaChunJie2024FuYanUpgradeWnd:OnBeiShiClick()
    LuaChunJie2024Mgr:ShowModelPreviewWnd("MaxLevelFuYan")
end

function LuaChunJie2024FuYanUpgradeWnd:OnTipButtonClick()
    g_MessageMgr:ShowMessage("CHUNJIE2024_FUYAN_UPGRADE_MONEY_TIP")
end

function LuaChunJie2024FuYanUpgradeWnd:OnUpgradeButtonClick()
end

--@endregion UIEvent
