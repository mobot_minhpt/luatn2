local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"

LuaHalloween2023OrderBattleTopRightWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistClassMember(LuaHalloween2023OrderBattleTopRightWnd, "m_rightBtn")
RegistClassMember(LuaHalloween2023OrderBattleTopRightWnd, "m_RankTable")
RegistClassMember(LuaHalloween2023OrderBattleTopRightWnd, "m_RankTemplate")

RegistClassMember(LuaHalloween2023OrderBattleTopRightWnd, "m_RankItemList")
--@endregion RegistChildComponent end

function LuaHalloween2023OrderBattleTopRightWnd:Awake()
    self.m_rightBtn = self.transform:Find("Anchor/Tip/rightBtn").gameObject
    self.m_RankTable = self.transform:Find("Anchor/tipPanel/RankTable"):GetComponent(typeof(UITable))
    self.m_RankTemplate = self.transform:Find("Anchor/tipPanel/RankTemplate").gameObject
    UIEventListener.Get(self.m_rightBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnRightBtnClick()
    end)
end

function LuaHalloween2023OrderBattleTopRightWnd:Init()
    self.m_RankTemplate:SetActive(false)
    self:InitRank()
end

function LuaHalloween2023OrderBattleTopRightWnd:InitRank()
    self.m_RankItemList = {}
    Extensions.RemoveAllChildren(self.m_RankTable.transform)
    self:OnSyncCandyDeliveryCampInfo()
end

function LuaHalloween2023OrderBattleTopRightWnd:InitOneRankItem(item, rank, info)
    local otherRoot = item.transform:Find("Other")
    local myselfRoot = item.transform:Find("Myself") 
    otherRoot.gameObject:SetActive(not info.myTeam)
    myselfRoot.gameObject:SetActive(info.myTeam)
    local showRoot = info.myTeam and myselfRoot or otherRoot
    local zuoQiData = LuaHalloween2023Mgr.m_CandyDeliveryGameSetData.candyForceSet[info.zuoQiId]
    if zuoQiData == nil then return end
    local bgColor = NGUIText.ParseColor24(zuoQiData.Color,0)
    showRoot.transform:Find("Backgroud"):GetComponent(typeof(UITexture)).color = bgColor
    showRoot.transform:Find("ForceName"):GetComponent(typeof(UILabel)).text = zuoQiData.Name
    showRoot.transform:Find("RankImage").gameObject:SetActive(rank == 1)
    showRoot.transform:Find("RankLabel"):GetComponent(typeof(UILabel)).text = rank > 1 and rank or ""
    showRoot.transform:Find("ScoreLabel"):GetComponent(typeof(UILabel)).text = info.score
end

function LuaHalloween2023OrderBattleTopRightWnd:OnSyncCandyDeliveryCampInfo()
    local campInfo = LuaHalloween2023Mgr.CandyDeliveryCampRank
    local rank = 0
    local score = -1
    for i = 1, #campInfo do
        local item = self.m_RankItemList[i]
        if item == nil then 
            item = NGUITools.AddChild(self.m_RankTable.gameObject, self.m_RankTemplate)
            table.insert(self.m_RankItemList, item)
            item:SetActive(true)
        end
        if campInfo[i].score ~= score then
            rank = i
            score = campInfo[i].score
        end
        self:InitOneRankItem(item, rank, campInfo[i])
    end
    self.m_RankTable:Reposition()
end

--@region UIEvent
function LuaHalloween2023OrderBattleTopRightWnd:OnRightBtnClick()
    self.m_rightBtn.transform.localEulerAngles = Vector3(0, 0, 0)
	CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end

function LuaHalloween2023OrderBattleTopRightWnd:OnHideTopAndRightTipWnd()
    self.m_rightBtn.transform.localEulerAngles = Vector3(0, 0, -180)
end

--@endregion UIEvent

function LuaHalloween2023OrderBattleTopRightWnd:OnEnable()
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd",self,"OnHideTopAndRightTipWnd")
    g_ScriptEvent:AddListener("SyncCandyDeliveryCampInfo",self,"OnSyncCandyDeliveryCampInfo")
end

function LuaHalloween2023OrderBattleTopRightWnd:OnDisable()
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd",self,"OnHideTopAndRightTipWnd")
    g_ScriptEvent:RemoveListener("SyncCandyDeliveryCampInfo",self,"OnSyncCandyDeliveryCampInfo")
end
--@endregion UIEvent