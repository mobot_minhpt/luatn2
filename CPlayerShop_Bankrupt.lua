-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CPlayerShop_Bankrupt = import "L10.UI.CPlayerShop_Bankrupt"
local CPlayerShopData = import "L10.UI.CPlayerShopData"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
CPlayerShop_Bankrupt.m_Init_CS2LuaHook = function (this)    local formatMsg = g_MessageMgr:FormatMessage("PLAYERSHOP_GO_BROKE_TIP", CPlayerShop_Bankrupt.InBankruptDay, CPlayerShop_Bankrupt.NeedSilverForBankrupt, CPlayerShop_Bankrupt.BankruptProtectedDay)
    this.m_TipLabel.text = formatMsg
    this.m_MoneyCtrl:SetCost(CPlayerShop_Bankrupt.NeedSilverForBankrupt)
end
CPlayerShop_Bankrupt.m_StopBankruptButtonClick_CS2LuaHook = function (this, button)
    if CPlayerShop_Bankrupt.NeedSilverForBankrupt > CClientMainPlayer.Inst.Silver then
        MessageWndManager.ShowOKMessage(LocalString.GetString("你的银两不够，不能赎回"), nil)
    else
        local format = System.String.Format(LocalString.GetString("你确定要花费{0}赎回你的商店吗？"), CPlayerShop_Bankrupt.NeedSilverForBankrupt)
        MessageWndManager.ShowOKCancelMessage(format, DelegateFactory.Action(function () 
            Gac2Gas.CancelPlayerShopBankrupt(CPlayerShopData.Main.PlayerShopId, CPlayerShop_Bankrupt.InBankruptDay, CPlayerShop_Bankrupt.NeedSilverForBankrupt)
        end), nil, nil, nil, false)
    end
end
