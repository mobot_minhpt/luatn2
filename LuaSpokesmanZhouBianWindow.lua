local Extensions = import "Extensions"
local CUITexture = import "L10.UI.CUITexture"
local UIGrid = import "UIGrid"
local CUICenterOnChild=import "L10.UI.CUICenterOnChild"
local UIScrollView = import "UIScrollView"
local CommonDefs = import "L10.Game.CommonDefs"
local UIPanel = import "UIPanel"
local Screen=import "UnityEngine.Screen"
local UILabel = import "UILabel"
local QnTableItem = import "L10.UI.QnTableItem"
local DelegateFactory = import "DelegateFactory"
local CButton = import "L10.UI.CButton"
local DateTime = import "System.DateTime"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaSpokesmanZhouBianWindow = class()

RegistChildComponent(LuaSpokesmanZhouBianWindow, "m_Indicator", "Indicator", GameObject)
RegistChildComponent(LuaSpokesmanZhouBianWindow, "m_Indicators", "Indicators", UIGrid)
RegistChildComponent(LuaSpokesmanZhouBianWindow, "m_BoxItem", "BoxItem", GameObject)
RegistChildComponent(LuaSpokesmanZhouBianWindow, "m_BoxGrid", "BoxGrid", UIGrid)
RegistChildComponent(LuaSpokesmanZhouBianWindow, "m_BoxScrollView", "BoxScrollView", UIScrollView)
RegistChildComponent(LuaSpokesmanZhouBianWindow, "m_PreBtn", "PreBtn", GameObject)
RegistChildComponent(LuaSpokesmanZhouBianWindow, "m_NextBtn", "NextBtn", GameObject)
RegistChildComponent(LuaSpokesmanZhouBianWindow, "m_SuitBlindBox", "SuitBlindBox", GameObject)
RegistChildComponent(LuaSpokesmanZhouBianWindow, "m_DollItem", "DollItem", GameObject)
RegistChildComponent(LuaSpokesmanZhouBianWindow, "m_DollGrid", "DollGrid", UIGrid)
RegistChildComponent(LuaSpokesmanZhouBianWindow, "m_BoxContainer", "BoxContainer", GameObject)
RegistChildComponent(LuaSpokesmanZhouBianWindow, "m_BuyButton", "BuyButton", CButton)
RegistChildComponent(LuaSpokesmanZhouBianWindow, "m_TipButton", "TipButton", GameObject)
RegistChildComponent(LuaSpokesmanZhouBianWindow, "m_TipLabel", "TipLabel", UILabel)
RegistChildComponent(LuaSpokesmanZhouBianWindow, "m_SingleBlindBox", "SingleBlindBox", GameObject)

RegistClassMember(LuaSpokesmanZhouBianWindow, "m_CurSigleBoxIndex")
RegistClassMember(LuaSpokesmanZhouBianWindow, "m_DragOffset")
RegistClassMember(LuaSpokesmanZhouBianWindow, "m_BoxList")
RegistClassMember(LuaSpokesmanZhouBianWindow, "m_SelectTableItem")
RegistClassMember(LuaSpokesmanZhouBianWindow, "m_SelectGoodId")

function LuaSpokesmanZhouBianWindow:Init()
    self.m_Indicator:SetActive(false)
    self.m_BoxList = {}--单个盲盒
    self.m_SelectTableItem = {}
    self.m_CurSigleBoxIndex = -1
    self.m_BoxItem:SetActive(false)
    self.m_DollItem:SetActive(false)

    Spokesman_Zhoubian.ForeachKey(function (id)
        local data = Spokesman_Zhoubian.GetData(id)
        if data.Type == 1 then--单个盲盒
            self:InitSigleBlindBox(id)
        elseif data.Type == 2 then--整套盲盒
            self:InitSuitBlindBox(id)
        elseif data.Type == 3 then--应援娃娃
            self:AddDoll(id)
        end
    end)

    self.m_DollGrid:Reposition()
    local now = CServerTimeMgr.Inst:GetZone8Time()
    local timestr = Spokesman_Setting.GetData().ZhouBianMallOpenTime
    local year, month, day, hour, min= string.match(timestr, "(%d+)/(%d+)/(%d+)  (%d+):(%d+):(%d+)")  
    local startTime = CreateFromClass(DateTime, year, month, day, hour, min, 0)
    local buttonText = self.m_BuyButton.transform:Find("Label"):GetComponent(typeof(UILabel))
    if DateTime.Compare(now, startTime) >= 0 then      
        self.m_BuyButton.Enabled = true
        buttonText.text = LocalString.GetString("购买")
    else
        self.m_BuyButton.Enabled = false
        buttonText.text = SafeStringFormat3(LocalString.GetString("%d月%d日%d:%d开售"),month,day,hour,min)
    end

    UIEventListener.Get(self.m_BoxContainer).onDrag = DelegateFactory.VectorDelegate(function(g,delta)
        self:OnDrag(g,delta)
    end)
    UIEventListener.Get(self.m_BoxContainer).onDragEnd = DelegateFactory.VoidDelegate(function(go)
        self:OnSwitchPic()
    end)

    UIEventListener.Get(self.m_BuyButton.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        if not self.m_SelectGoodId then
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("请先选择一件商品"))
        else

            Gac2Gas.RequestBuyJiaNianHua2020Ticket(self.m_SelectGoodId)
        end
    end)
    UIEventListener.Get(self.m_TipButton).onClick = DelegateFactory.VoidDelegate(function(go)
        g_MessageMgr:ShowMessage("Spokesman_Zhoubian_Tip")
    end)
end

function LuaSpokesmanZhouBianWindow:InitSigleBlindBox(id)
    Extensions.RemoveAllChildren(self.m_BoxGrid.transform)

    local data = Spokesman_Zhoubian.GetData(id)
    local boxIcons = data.Icon
    for i=0,boxIcons.Length -1 , 1 do
        local go=NGUITools.AddChild(self.m_BoxGrid.gameObject,self.m_BoxItem)
        go:SetActive(true)
        local icon = boxIcons[i]
        self:InitSignleBoxTxture(go,icon)
        table.insert(self.m_BoxList,data.ID)
    end

    self.m_BoxGrid:Reposition()

    --indicator
    Extensions.RemoveAllChildren(self.m_Indicators.transform)
    for i,v in ipairs(self.m_BoxList) do
        local go=NGUITools.AddChild(self.m_Indicators.gameObject,self.m_Indicator)
        go:SetActive(true)
    end
    self.m_Indicators:Reposition()

    local centerOnChild=self.m_BoxGrid.transform:GetComponent(typeof(CUICenterOnChild))
    centerOnChild.onCenter=LuaUtils.OnCenterCallback(function(go)
        local index = go.transform:GetSiblingIndex()
        for i=1,self.m_Indicators.transform.childCount do
            local sprite=self.m_Indicators.transform:GetChild(i-1):GetComponent(typeof(UISprite))
            if i==index+1 then
                sprite.color=Color.white
            else
                sprite.color=Color(0.3,0.5,0.8)
            end
        end    
    end)
    centerOnChild:CenterOnInstant(self.m_BoxGrid.transform:GetChild(0))

    local tableItem = self.m_BoxContainer.transform:GetComponent(typeof(QnTableItem))
    table.insert(self.m_SelectTableItem,tableItem)
    tableItem.OnSelected = DelegateFactory.Action_GameObject(function (go)
        self:OnTableSelected(1)
        self.m_SelectGoodId = data.GoodsId
    end)

    UIEventListener.Get(self.m_PreBtn).onClick  =DelegateFactory.VoidDelegate(function(go)
        self:TureToLastPic()
    end)
    UIEventListener.Get(self.m_NextBtn).onClick  =DelegateFactory.VoidDelegate(function(go)
        self:TureToNextPic()
    end)

    local nameLabel = self.m_SingleBlindBox.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local descLabel = self.m_SingleBlindBox.transform:Find("DescLabel"):GetComponent(typeof(UILabel))
    local priceLabel = self.m_SingleBlindBox.transform:Find("CountLabel"):GetComponent(typeof(UILabel))

    nameLabel.text = data.GoodsName
    descLabel.text = data.Des
    priceLabel.text = data.Price
end

function LuaSpokesmanZhouBianWindow:InitSuitBlindBox(id)
    local data = Spokesman_Zhoubian.GetData(id)
    if not data then return end
    local icon = self.m_SuitBlindBox.transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local nameLabel = self.m_SuitBlindBox.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local descLabel = self.m_SuitBlindBox.transform:Find("DescLabel"):GetComponent(typeof(UILabel))
    local priceLabel = self.m_SuitBlindBox.transform:Find("CountLabel"):GetComponent(typeof(UILabel))

    icon:LoadMaterial(data.Icon[0])
    nameLabel.text = data.GoodsName
    descLabel.text = data.Des
    priceLabel.text = data.Price

    local tableItem = self.m_SuitBlindBox.transform:GetComponent(typeof(QnTableItem))
    table.insert(self.m_SelectTableItem,tableItem)
    tableItem.OnSelected = DelegateFactory.Action_GameObject(function (go)
        self:OnTableSelected(2)
        self.m_SelectGoodId = data.GoodsId
    end)
end

function LuaSpokesmanZhouBianWindow:AddDoll(id)
    local data = Spokesman_Zhoubian.GetData(id)
    if not data then return end

    local dollItem = NGUITools.AddChild(self.m_DollGrid.gameObject,self.m_DollItem)
    dollItem:SetActive(true)

    local icon = dollItem.transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local texture = dollItem.transform:Find("Icon"):GetComponent(typeof(UITexture))
    local nameLabel = dollItem.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local descLabel = dollItem.transform:Find("DescLabel"):GetComponent(typeof(UILabel))
    local priceLabel = dollItem.transform:Find("CountLabel"):GetComponent(typeof(UILabel))

    local iconPath = data.Icon[0]
    if iconPath == "UI/Texture/Transparent/Material/welfarewnd_dieke_taozhuang.mat" then
        texture.width = 228
        texture.height = 196
    end

    icon:LoadMaterial(data.Icon[0])
    nameLabel.text = data.GoodsName
    descLabel.text = data.Des
    priceLabel.text = data.Price

    local tableItem = dollItem.transform:GetComponent(typeof(QnTableItem))
    table.insert(self.m_SelectTableItem,tableItem)
    local index = #self.m_SelectTableItem
    tableItem.OnSelected = DelegateFactory.Action_GameObject(function (go)
        self:OnTableSelected(index)
        self.m_SelectGoodId = data.GoodsId
    end)
end

function LuaSpokesmanZhouBianWindow:InitSignleBoxTxture(go,iconPath)
    local icon = go.transform:Find("Icon"):GetComponent(typeof(CUITexture))
    icon:LoadMaterial(iconPath)
end

function LuaSpokesmanZhouBianWindow:OnTableSelected(index)
    for i,item in ipairs(self.m_SelectTableItem) do
        if i == index then
            --item:SetSelected(true,false)
        else
            item:SetSelected(false,false)
        end
    end
end

function LuaSpokesmanZhouBianWindow:TureToLastPic()
    local index
    local offsetAll = CommonDefs.GetComponent_Component_Type(self.m_BoxScrollView.transform, typeof(UIPanel)).width
    if self.m_CurSigleBoxIndex == 0 or self.m_CurSigleBoxIndex == -1 then--第一张 应该返回最后一张
        self.m_BoxScrollView:SetDragAmount(0,0,false)
        index = #self.m_BoxList-1
        self.m_CurSigleBoxIndex = index
        self.m_BoxScrollView:SetDragAmount(offsetAll,0,false)
    else
        self.m_CurSigleBoxIndex = self.m_CurSigleBoxIndex - 1
        index = self.m_CurSigleBoxIndex
        local perWidth = self.m_BoxGrid.cellWidth
        local offset = math.min(index * (perWidth) / (self.m_BoxGrid.transform.childCount * (perWidth) - offsetAll), 1)
        self.m_BoxScrollView:SetDragAmount(offset,0,false)
    end
    for i=1,self.m_Indicators.transform.childCount do
        local sprite=self.m_Indicators.transform:GetChild(i-1):GetComponent(typeof(UISprite))
        if i==index+1 then
            sprite.color=Color.white
        else
            sprite.color=Color(0.3,0.5,0.8)
        end
    end
end

function LuaSpokesmanZhouBianWindow:TureToNextPic()
    local index 
    if self.m_CurSigleBoxIndex == #self.m_BoxList-1 then--最后一张 应该往回了
        self.m_BoxScrollView:SetDragAmount(0,0,false)
        self.m_CurSigleBoxIndex = 0
        index = 0
    else
        self.m_CurSigleBoxIndex = self.m_CurSigleBoxIndex + 1
        if self.m_CurSigleBoxIndex == 0 then
            self.m_CurSigleBoxIndex = 1
        end
        local offset = CommonDefs.GetComponent_Component_Type(self.m_BoxScrollView.transform, typeof(UIPanel)).width
        index = self.m_CurSigleBoxIndex
        local perWidth = self.m_BoxGrid.cellWidth
        offset = math.min(index * (perWidth) / (self.m_BoxGrid.transform.childCount * (perWidth) - offset), 1)
        self.m_BoxScrollView:SetDragAmount(offset,0,false)
    end

    for i=1,self.m_Indicators.transform.childCount do
        local sprite=self.m_Indicators.transform:GetChild(i-1):GetComponent(typeof(UISprite))
        if i==index+1 then
            sprite.color=Color.white
        else
            sprite.color=Color(0.3,0.5,0.8)
        end
    end
end

function LuaSpokesmanZhouBianWindow:OnDrag(g,delta)
    self.m_DragOffset = delta.x/Screen.width*360
end

function LuaSpokesmanZhouBianWindow:OnSwitchPic()
    if self.m_DragOffset < 0 then
        self:TureToNextPic()
    elseif self.m_DragOffset > 0 then
        self:TureToLastPic()
    end
    self.m_DragOffset = 0
end

return LuaSpokesmanZhouBianWindow
