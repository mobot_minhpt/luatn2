local CommonDefs = import "L10.Game.CommonDefs"
local CTrackMgr = import "L10.Game.CTrackMgr"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local CPos = import "L10.Engine.CPos"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local RuntimePlatform = import "UnityEngine.RuntimePlatform"
local Application = import "UnityEngine.Application"
local MouseInputHandler = import "L10.Engine.MouseInputHandler"

CLuaScreenCaptureMgr = {}
function CLuaScreenCaptureMgr:Init()
  g_ScriptEvent:AddListener("StartLoadScene", self, "StartLoadScene")
end
CLuaScreenCaptureMgr:Init()
function CLuaScreenCaptureMgr:StartLoadScene()
  if not CLuaScreenCaptureMgr.IsRecording then return end
  g_ScriptEvent:RemoveListener("AICameraMoveEnd", self, "AICameraMoveEnd")
  CLuaScreenCaptureMgr.IsRecording = false
  if CommonDefs.DEVICE_PLATFORM == "ios" then
    local ReplayManager = import "ReplayManager"
    ReplayManager.StopRecording(true)
  elseif CommonDefs.DEVICE_PLATFORM == "ad" then
    local CAndroidScreenRecorder = import "CAndroidScreenRecorder"
    CAndroidScreenRecorder.StopRecord()
  end
end

CLuaScreenCaptureMgr.CurrentBabyId = 0
CLuaScreenCaptureMgr.CurrentBabyEngineId = 0
CLuaScreenCaptureMgr.CurrentLingshouEngineId = 0
CLuaScreenCaptureMgr.TrackSuccessCallBack = nil

CLuaScreenCaptureMgr.CustomPhotoCameraPos = nil--自定义的风景相册位置

function CLuaScreenCaptureMgr:ClearCurrentData()
  CLuaScreenCaptureMgr.CurrentBabyId = nil
  CLuaScreenCaptureMgr.CurrentBabyEngineId = 0
  CLuaScreenCaptureMgr.CurrentLingshouEngineId = 0
end

function CLuaScreenCaptureMgr:RideOff()
  if CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.CurrentZuoQiId and CClientMainPlayer.Inst.BasicProp.CurrentZuoQiId ~= "" then
    Gac2Gas.RequestRideOffZuoQi(CClientMainPlayer.Inst.BasicProp.CurrentZuoQiId)
  end
end

function CLuaScreenCaptureMgr:DoTrack(sceneTemplateId, posX, posY, notNeedRideOffVehicle, successFunc)
  CTrackMgr.Inst:Track(nil, sceneTemplateId, CPos(posX * 64, posY * 64), 0, DelegateFactory.Action(function()
    if successFunc then
      if notNeedRideOffVehicle < 1 and CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.CurrentZuoQiId and CClientMainPlayer.Inst.BasicProp.CurrentZuoQiId ~= "" then
        Gac2Gas.RequestRideOffZuoQi(CClientMainPlayer.Inst.BasicProp.CurrentZuoQiId)
        g_ScriptEvent:AddListener("OnRideOnOffVehicle", self, "OnRideOnOffVehicle")
        CLuaScreenCaptureMgr.TrackSuccessCallBack = successFunc
      else
        successFunc()
      end
    end
  end), nil, nil, nil, true)
end

function CLuaScreenCaptureMgr:OnRideOnOffVehicle()
  g_ScriptEvent:RemoveListener("OnRideOnOffVehicle", self, "OnRideOnOffVehicle")
  if CLuaScreenCaptureMgr.TrackSuccessCallBack and CClientMainPlayer.Inst and (not CClientMainPlayer.Inst.BasicProp.CurrentZuoQiId or CClientMainPlayer.Inst.BasicProp.CurrentZuoQiId == "") then
    CLuaScreenCaptureMgr.TrackSuccessCallBack()
  end
  CLuaScreenCaptureMgr.TrackSuccessCallBack = nil
end

--Record Video
CLuaScreenCaptureMgr.CheckRecordingTick = nil
CLuaScreenCaptureMgr.IsRecording = false
function CLuaScreenCaptureMgr:DoRecordVideo(id, designData, bDoRecord)
  if bDoRecord then
    if CommonDefs.DEVICE_PLATFORM == "ios" then
      self:IOSDoRecordVideo(id, designData)
    elseif CommonDefs.DEVICE_PLATFORM == "ad" then
      self:AndroidRecordVideo(id, designData)
    elseif CommonDefs.DEVICE_PLATFORM == "pc" then
      if Application.platform == RuntimePlatform.WindowsEditor then
        CUIManager.CloseUI(CUIResources.ScreenCaptureWnd)
        self:DoTrack(designData.SceneTemplateId, designData.ScenePos[0], designData.ScenePos[1], designData.NotNeedRideOffVehicle, function()
          self:DoAICamera(id, designData)
        end)
        return
      end
      g_MessageMgr:ShowMessage("CUSTOM_STRING1", LocalString.GetString("请使用手机录制视频"))
    end
    return
  end
  self:DoTrack(designData.SceneTemplateId, designData.ScenePos[0], designData.ScenePos[1], designData.NotNeedRideOffVehicle, nil)
end

function CLuaScreenCaptureMgr:IOSDoRecordVideo(id, designData)
  local ReplayManager = import "ReplayManager"
  if not ReplayManager.IsSupportReplay() then
    g_MessageMgr:ShowMessage("ReplayKit_Cannot_Begin")
    return
  end
  if ReplayManager.IsRecording() then
    return
  end

  CUIManager.CloseUI(CUIResources.ScreenCaptureWnd)
  self:DoTrack(designData.SceneTemplateId, designData.ScenePos[0], designData.ScenePos[1], designData.NotNeedRideOffVehicle, function()
    ReplayManager.StartRecording()
    if CLuaScreenCaptureMgr.CheckRecordingTick then
      UnRegisterTick(CLuaScreenCaptureMgr.CheckRecordingTick)
    end
    CLuaScreenCaptureMgr.CheckRecordingTick = RegisterTick(function ()
      if ReplayManager.IsRecording() then
        UnRegisterTick(CLuaScreenCaptureMgr.CheckRecordingTick)
        CLuaScreenCaptureMgr.CheckRecordingTick = nil
        self:DoAICamera(id, designData)
      end
    end, 100)
  end)
end

function CLuaScreenCaptureMgr:AndroidRecordVideo(id, designData)
  local CAndroidScreenRecorder = import "CAndroidScreenRecorder"
  if not CAndroidScreenRecorder.IsEngineSupport() then
    g_MessageMgr:ShowMessage("CUSTOM_STRING1", LocalString.GetString("引擎版本低，请下载最新客户端体验"))
    return
  end
  if not CAndroidScreenRecorder.IsSystemSupport() then
    g_MessageMgr:ShowMessage("CUSTOM_STRING1", LocalString.GetString("手机系统版本低，请升级手机系统"))
    return
  end
  CUIManager.CloseUI(CUIResources.ScreenCaptureWnd)
  self:DoTrack(designData.SceneTemplateId, designData.ScenePos[0], designData.ScenePos[1], designData.NotNeedRideOffVehicle, function()
    CAndroidScreenRecorder.StartRecord()
    if CLuaScreenCaptureMgr.CheckRecordingTick then
      UnRegisterTick(CLuaScreenCaptureMgr.CheckRecordingTick)
    end
    CLuaScreenCaptureMgr.CheckRecordingTick = RegisterTick(function ()
      if CAndroidScreenRecorder.IsRecording() then
        UnRegisterTick(CLuaScreenCaptureMgr.CheckRecordingTick)
        CLuaScreenCaptureMgr.CheckRecordingTick = nil
        self:DoAICamera(id, designData)
      end
    end, 100)
  end)
end

function CLuaScreenCaptureMgr:DoAICamera(id, designData)
  CLuaScreenCaptureWnd.ScenePhotoId = id
  g_ScriptEvent:RemoveListener("AICameraMoveEnd", self, "AICameraMoveEnd")
  g_ScriptEvent:AddListener("AICameraMoveEnd", self, "AICameraMoveEnd")
  CLuaScreenCaptureMgr.IsRecording = true
  MouseInputHandler.Inst.ForbidClickMove = true
  CameraFollow.Inst:BeginAICamera(Vector3(designData.CameraPath[0], designData.CameraPath[1], designData.CameraPath[2]), Vector3(designData.CameraPath[3], designData.CameraPath[4], designData.CameraPath[5]), designData.CameraPath[6], false)
  for i = 7, designData.CameraPath.Length - 1, 7 do
    CameraFollow.Inst:AICameraMoveTo(Vector3(designData.CameraPath[i + 0], designData.CameraPath[i + 1], designData.CameraPath[i + 2]), Vector3(designData.CameraPath[i + 3], designData.CameraPath[i + 4], designData.CameraPath[i + 5]), designData.CameraPath[i + 6])
  end
  self:DoAnimation(designData.AnimationName)
end

-- eg: umbrella01_start,1;umbrella01_loop,10;
function CLuaScreenCaptureMgr:DoAnimation(animationList)
  local expressionId = tonumber(animationList)
  if expressionId then
    Gac2Gas.RequestExpressionAction(expressionId)
    return
  end

  if not CClientMainPlayer.Inst or not CClientMainPlayer.Inst.RO then return end
  local splitIndex = string.find(animationList, ";")
  if not splitIndex then return end
  local curAnimation = string.sub(animationList, 1, splitIndex - 1)
  local nextAnimationList = string.sub(animationList, splitIndex + 1)
  local curSplitIndex = string.find(curAnimation, ",")
  if not curSplitIndex then return end
  local aniName = string.sub(curAnimation, 1, curSplitIndex - 1)
  local nextAniTime = tonumber(string.sub(curAnimation, curSplitIndex + 1))
  CClientMainPlayer.Inst.RO:DoAni(aniName, true, 0, 1.0, 0.15, true, 1.0)
  if nextAnimationList then
    RegisterTickOnce(function()
      self:DoAnimation(nextAnimationList)
    end, nextAniTime * 1000)
  end
end

function CLuaScreenCaptureMgr:AICameraMoveEnd()
   if not CLuaScreenCaptureMgr.IsRecording then return end
   MouseInputHandler.Inst.ForbidClickMove = false
   CameraFollow.Inst:EndAICamera()
   g_ScriptEvent:RemoveListener("AICameraMoveEnd", self, "AICameraMoveEnd")
   CLuaScreenCaptureMgr.IsRecording = false
   if CommonDefs.DEVICE_PLATFORM == "ios" then
     local ReplayManager = import "ReplayManager"
     ReplayManager.StopRecording(true)
   elseif CommonDefs.DEVICE_PLATFORM == "ad" then
     local CAndroidScreenRecorder = import "CAndroidScreenRecorder"
     CAndroidScreenRecorder.StopRecord()
   end
   Gac2Gas.SaveSceneryPhotoAlbumUrl(CLuaScreenCaptureWnd.ScenePhotoId, "save")
end
-- End Record Video

function Gas2Gac.ReplyLingShouBabyInfoForCamara(babyId, babyEngineId, lingshouEngineId)
    CLuaScreenCaptureMgr.CurrentBabyId = babyId
    CLuaScreenCaptureMgr.CurrentBabyEngineId = babyEngineId
    CLuaScreenCaptureMgr.CurrentLingshouEngineId = lingshouEngineId
end

CLuaScreenCaptureMgr.AlbumTable = {}
CLuaScreenCaptureMgr.AlbumCount = 0
function Gas2Gac.ReplyAllSceneryPhotoAlbumUrl(dataUD)
    CLuaScreenCaptureMgr.AlbumTable = {}
    CLuaScreenCaptureMgr.AlbumCount = 0
    local data = MsgPackImpl.unpack(dataUD)
    if data and data.Count >= 2 then
      for i = 0, data.Count - 1, 2 do
        CLuaScreenCaptureMgr.AlbumTable[data[i]] = data[i +1]
        CLuaScreenCaptureMgr.AlbumCount = CLuaScreenCaptureMgr.AlbumCount + 1
      end
    end
    g_ScriptEvent:BroadcastInLua("ReplyAllSceneryPhotoAlbumUrl")
end

function Gas2Gac.TargetAgreeToGroupPhoto(targetId, expressionId, bUnLockXiaoYaoExpression)
	 g_ScriptEvent:BroadcastInLua("TargetAgreeToGroupPhoto", targetId, bUnLockXiaoYaoExpression)
end
