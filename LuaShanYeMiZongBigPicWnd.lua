local CUITexture = import "L10.UI.CUITexture"
local Screen = import "UnityEngine.Screen"
local UITexture = import "UITexture"
local UILabel = import "UILabel"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local ShareMgr = import "ShareMgr"

LuaShanYeMiZongBigPicWnd = class()
LuaShanYeMiZongBigPicWnd.s_Pic = nil
LuaShanYeMiZongBigPicWnd.s_Desc = nil

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaShanYeMiZongBigPicWnd, "PicTexture", "PicTexture", CUITexture)
RegistChildComponent(LuaShanYeMiZongBigPicWnd, "DescLabel", "DescLabel", UILabel)

--@endregion RegistChildComponent end

function LuaShanYeMiZongBigPicWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaShanYeMiZongBigPicWnd:Init()
    self.DescLabel.text = LuaShanYeMiZongBigPicWnd.s_Desc or ""
    CPersonalSpaceMgr.DownLoadPic(
        ShareMgr.GetWebImagePath(LuaShanYeMiZongBigPicWnd.s_Pic), 
        self.PicTexture.texture, 1125, 2436, 
        DelegateFactory.Action(function() 
            self.width = 2436
            self.height = 1125
            --self.PicTexture:GetComponent(typeof(UITexture)):SetAnchor(self.gameObject, 0, 0, 0, 0)  
        end), 
        true, true
    )
    
    -- Init时一定已经AddBgMask 
    --local bgMaskTex = self.transform:Find("_BgMask_").gameObject:AddComponent(typeof(CUITexture))
    --CPersonalSpaceMgr.DownLoadPic(ShareMgr.GetWebImagePath(LuaShanYeMiZongBigPicWnd.s_Pic), bgMaskTex.texture, Screen.height, Screen.width, 
    --    DelegateFactory.Action(function() end), true, true)
end

--@region UIEvent

--@endregion UIEvent

