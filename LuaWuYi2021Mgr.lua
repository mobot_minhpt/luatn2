local CScene=import "L10.Game.CScene"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local CGuideMgr=import "L10.Game.Guide.CGuideMgr"
local Guide_Logic = import "L10.Game.Guide_Logic"

LuaWuYi2021Mgr = class()
LuaWuYi2021Mgr.m_HasReward = nil
LuaWuYi2021Mgr.m_IsWin = nil
LuaWuYi2021Mgr.m_Stage = nil
LuaWuYi2021Mgr.m_MonsterList = nil
LuaWuYi2021Mgr.m_BossEngineId = nil

function LuaWuYi2021Mgr:ShowJXMXReward(hasReward, isWin)
    self.m_HasReward = hasReward
    self.m_IsWin = isWin
    if self.m_IsWin then
        CUIManager.ShowUI(CLuaUIResources.WuYi2021MingXingResultWnd)
    else
    end
end

function LuaWuYi2021Mgr:SyncWuYi2021JXMXPlayInfo(stage, list, bossEngineId)
    self.m_Stage = stage
    self.m_MonsterList = list
    self.m_BossEngineId = bossEngineId
    g_ScriptEvent:BroadcastInLua("SyncWuYi2021JXMXPlayInfo", stage, list, bossEngineId)
end

function LuaWuYi2021Mgr:Reset()
    self.m_Stage = nil
    self.m_MonsterList = nil
    self.m_BossEngineId = nil
end

function LuaWuYi2021Mgr:SetSceneMode()
    if CScene.MainScene then
        local gamePlayId = CScene.MainScene.GamePlayDesignId
        if gamePlayId == WuYi2021_Setting.GetData().GameplayId then
            CScene.MainScene.ShowLeavePlayButton = true
            CScene.MainScene.ShowTimeCountDown = true
            CScene.MainScene.ShowMonsterCountDown = false
        end
    end
end

function LuaWuYi2021Mgr:ShowBossHighlight()
	-- 当前的场景相机
	local rotate = CameraFollow.Inst.rotateObj
	CameraFollow.Inst:BeginAICamera(rotate.position, rotate.eulerAngles, 3600, false)
	-- 计算新的相机位置
    local cameraData = WuYi2021_Setting.GetData().JXMXCameraData
    local time = WuYi2021_Setting.GetData().JXMXCameraTime
		
	CameraFollow.Inst:AICameraMoveTo(Vector3(cameraData[0], cameraData[1], cameraData[2]),
        Vector3(cameraData[3], cameraData[4], cameraData[5]), time[0])

	RegisterTickOnce(function()
        if CameraFollow.Inst then
		    CameraFollow.Inst:EndAICamera()
        end
	end, time[1]*1000)
end

function LuaWuYi2021Mgr:ShowBtnHighLight()
    local logic = Guide_Logic.GetData(135)
    if logic then
        CGuideMgr.Inst:TriggerGuide(logic)
    end
end
