-- Auto Generated!!
local Boolean = import "System.Boolean"
local CButton = import "L10.UI.CButton"
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CRoommateProp = import "L10.Game.CRoommateProp"
local CXiangfangPrivalegeManageWnd = import "L10.UI.CXiangfangPrivalegeManageWnd"
local DelegateFactory = import "DelegateFactory"
local Double = import "System.Double"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Gac2Gas = import "L10.Game.Gac2Gas"
local GameObject = import "UnityEngine.GameObject"
local Int32 = import "System.Int32"
local MessageWndManager = import "L10.UI.MessageWndManager"
local MsgPackImpl = import "MsgPackImpl"
local Object = import "System.Object"
local QnButton = import "L10.UI.QnButton"
local QnCheckBox = import "L10.UI.QnCheckBox"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local UInt64 = import "System.UInt64"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CXiangfangPrivalegeManageWnd.m_ForbidChangeCheckbox_CS2LuaHook = function (this, btn) 
    local box = CommonDefs.GetComponent_GameObject_Type(btn.gameObject, typeof(QnCheckBox))
    if box ~= nil then
        g_MessageMgr:ShowMessage("FANGKE_PRIVALEGE_NOT_EDITABLE")
        box.Selected = this.DefaultCheckboxStatus
    end
end
CXiangfangPrivalegeManageWnd.m_Init_CS2LuaHook = function (this) 

    this.RoomerIds = CreateFromClass(MakeGenericClass(List, UInt64))
    CommonDefs.DictIterate(CClientHouseMgr.Inst.mExtraProp.Roommates, DelegateFactory.Action_object_object(function (___key, ___value) 
        local pair = {}
        pair.Key = ___key
        pair.Value = ___value
        CommonDefs.ListAdd(this.RoomerIds, typeof(UInt64), pair.Key)
    end))
    CommonDefs.ListSort1(this.RoomerIds, typeof(UInt64), DelegateFactory.Comparison_ulong(function (id1, id2) 
        if CommonDefs.DictContains(CClientHouseMgr.Inst.mExtraProp.Roommates, typeof(UInt64), id1) and CommonDefs.DictContains(CClientHouseMgr.Inst.mExtraProp.Roommates, typeof(UInt64), id2) then
            local p1 = CommonDefs.DictGetValue(CClientHouseMgr.Inst.mExtraProp.Roommates, typeof(UInt64), id1)
            local p2 = CommonDefs.DictGetValue(CClientHouseMgr.Inst.mExtraProp.Roommates, typeof(UInt64), id2)
            return (p1.StartTime - p2.StartTime)
        end
        return 0
    end))

    this:InitWidget()
    if this.RoomerIds.Count > 0 then
        CommonDefs.GetComponent_GameObject_Type(this.roomerBtn[0], typeof(CButton)).Selected = true
        this.CurrentSelectedId = this.RoomerIds[0]
        Gac2Gas.QueryRoomerPrivalegeInfo(this.CurrentSelectedId)
    end
    this:SetForbidChange()

    do
        local i = 0
        while i < this.RoomerIds.Count do
            CommonDefs.GetComponent_GameObject_Type(this.roomerBtn[i], typeof(CButton)).Enabled = true
            i = i + 1
        end
    end
    for i = this.RoomerIds.Count, 3 do
        CommonDefs.GetComponent_GameObject_Type(this.roomerBtn[i], typeof(CButton)).Enabled = false
    end
end
CXiangfangPrivalegeManageWnd.m_InitWidget_CS2LuaHook = function (this) 
    --plantAndHarvest.SetSelected(true, true);
    --addResourceProgressAndSpeedup.SetSelected(true, true);
    --stealCrop.SetSelected(true, true);
    --xiangfangDecorate.SetSelected(true, true);

    --enterZhengwu.SetSelected(true, true);
    --yardDecorate.SetSelected(false, true);
    --zhengwuDecorate.SetSelected(false, true);
    --houseConstruct.SetSelected(false, true);


    this:ClearAllOp()

    this.roomerInfo:Clear()
end
CXiangfangPrivalegeManageWnd.m_SetUneditableCheckBox_CS2LuaHook = function (this) 
    this:ClearForbidChange()
    this.plantAndHarvest:SetSelected(true, true)
    this.addResourceProgressAndSpeedup:SetSelected(true, true)
    this.stealCrop:SetSelected(true, true)
    this.xiangfangDecorate:SetSelected(true, true)
    this:SetForbidChange()
    this.DefaultCheckboxStatus = true
end
CXiangfangPrivalegeManageWnd.m_SetForbidChange_CS2LuaHook = function (this) 
    this.plantAndHarvest.OnClick = MakeDelegateFromCSFunction(this.ForbidChangeCheckbox, MakeGenericClass(Action1, QnButton), this)
    this.addResourceProgressAndSpeedup.OnClick = MakeDelegateFromCSFunction(this.ForbidChangeCheckbox, MakeGenericClass(Action1, QnButton), this)
    this.stealCrop.OnClick = MakeDelegateFromCSFunction(this.ForbidChangeCheckbox, MakeGenericClass(Action1, QnButton), this)
    this.xiangfangDecorate.OnClick = MakeDelegateFromCSFunction(this.ForbidChangeCheckbox, MakeGenericClass(Action1, QnButton), this)
end
CXiangfangPrivalegeManageWnd.m_ClearForbidChange_CS2LuaHook = function (this) 
    this.plantAndHarvest.OnClick = nil
    this.addResourceProgressAndSpeedup.OnClick = nil
    this.stealCrop.OnClick = nil
    this.xiangfangDecorate.OnClick = nil
end
CXiangfangPrivalegeManageWnd.m_ClearEditableOp_CS2LuaHook = function (this) 
    this.enterZhengwu:SetSelected(false, true)
    this.yardDecorate:SetSelected(false, true)
    this.zhengwuDecorate:SetSelected(false, true)
    this.houseConstruct:SetSelected(false, true)
    this.lsdfCheckBox:SetSelected(false, true)
    this.exchangeTalisman:SetSelected(false, true)
end
CXiangfangPrivalegeManageWnd.m_ClearAllOp_CS2LuaHook = function (this) 
    this:ClearForbidChange()
    this.plantAndHarvest:SetSelected(false, true)
    this.addResourceProgressAndSpeedup:SetSelected(false, true)
    this.stealCrop:SetSelected(false, true)
    this.xiangfangDecorate:SetSelected(false, true)
    this:SetForbidChange()

    this:ClearEditableOp()
    this.DefaultCheckboxStatus = false
end
CXiangfangPrivalegeManageWnd.m_ForbidChangeCheckboxByRoomer_CS2LuaHook = function (this, btn) 
    local box = CommonDefs.GetComponent_GameObject_Type(btn.gameObject, typeof(QnCheckBox))
    if box ~= nil then
        g_MessageMgr:ShowMessage("FANGKE_PRIVALEGE_NOT_EDITABLE_BY_ROOMER")
        if this.SelectMap ~= nil and CommonDefs.DictContains(this.SelectMap, typeof(GameObject), btn.gameObject) then
            box.Selected = CommonDefs.DictGetValue(this.SelectMap, typeof(GameObject), btn.gameObject)
        end
    end
end
CXiangfangPrivalegeManageWnd.m_OnQueryRoomerPrivalegeInfoResult_CS2LuaHook = function (this, isOwner, roomerId, roomerName, prop, titleStr, opInfo) 
    this.IsOwner = isOwner
    if roomerId == this.CurrentSelectedId then
        this:ClearEditableOp()
        this:SetUneditableCheckBox()
        this.roomerInfo:SetFriendInfo(roomerName, prop.Grade, prop.Class, prop.Gender, titleStr)
        this.SelectMap = CreateFromClass(MakeGenericClass(Dictionary, GameObject, Boolean))
        if this.IsOwner then
            this.enterZhengwu.OnClick = nil
            this.zhengwuDecorate.OnClick = nil
            this.yardDecorate.OnClick = nil
            this.houseConstruct.OnClick = nil
            this.lsdfCheckBox.OnClick = nil
            this.exchangeTalisman.OnClick = nil
        else
            this.enterZhengwu.OnClick = MakeDelegateFromCSFunction(this.ForbidChangeCheckboxByRoomer, MakeGenericClass(Action1, QnButton), this)
            this.zhengwuDecorate.OnClick = MakeDelegateFromCSFunction(this.ForbidChangeCheckboxByRoomer, MakeGenericClass(Action1, QnButton), this)
            this.yardDecorate.OnClick = MakeDelegateFromCSFunction(this.ForbidChangeCheckboxByRoomer, MakeGenericClass(Action1, QnButton), this)
            this.houseConstruct.OnClick = MakeDelegateFromCSFunction(this.ForbidChangeCheckboxByRoomer, MakeGenericClass(Action1, QnButton), this)
            this.lsdfCheckBox.OnClick = MakeDelegateFromCSFunction(this.ForbidChangeCheckboxByRoomer, MakeGenericClass(Action1, QnButton), this)
            this.exchangeTalisman.OnClick = MakeDelegateFromCSFunction(this.ForbidChangeCheckboxByRoomer, MakeGenericClass(Action1, QnButton), this)
        end
        CommonDefs.DictAdd(this.SelectMap, typeof(GameObject), this.enterZhengwu.gameObject, typeof(Boolean), false)
        CommonDefs.DictAdd(this.SelectMap, typeof(GameObject), this.zhengwuDecorate.gameObject, typeof(Boolean), false)
        CommonDefs.DictAdd(this.SelectMap, typeof(GameObject), this.yardDecorate.gameObject, typeof(Boolean), false)
        CommonDefs.DictAdd(this.SelectMap, typeof(GameObject), this.houseConstruct.gameObject, typeof(Boolean), false)
        CommonDefs.DictAdd(this.SelectMap, typeof(GameObject), this.lsdfCheckBox.gameObject, typeof(Boolean), false)
        CommonDefs.DictAdd(this.SelectMap, typeof(GameObject), this.exchangeTalisman.gameObject, typeof(Boolean), false)
        do
            local i = 0
            while i < opInfo.Count do
                local idx = math.floor(tonumber(opInfo[i] or 0))
                local v = math.floor(tonumber(opInfo[i + 1] or 0))
                if idx == EHouseEditableOperation_lua.EnterZhengwu and v ~= 0 then
                    this.enterZhengwu:SetSelected(true, true)
                    CommonDefs.DictSet(this.SelectMap, typeof(GameObject), this.enterZhengwu.gameObject, typeof(Boolean), true)
                elseif idx == EHouseEditableOperation_lua.OpRoomFurniture and v ~= 0 then
                    this.zhengwuDecorate:SetSelected(true, true)
                    CommonDefs.DictSet(this.SelectMap, typeof(GameObject), this.zhengwuDecorate.gameObject, typeof(Boolean), true)
                elseif idx == EHouseEditableOperation_lua.OpYardFurniture and v ~= 0 then
                    this.yardDecorate:SetSelected(true, true)
                    CommonDefs.DictSet(this.SelectMap, typeof(GameObject), this.yardDecorate.gameObject, typeof(Boolean), true)
                elseif idx == EHouseEditableOperation_lua.UpgradeBuilding and v ~= 0 then
                    this.houseConstruct:SetSelected(true, true)
                    CommonDefs.DictSet(this.SelectMap, typeof(GameObject), this.houseConstruct.gameObject, typeof(Boolean), true)
                elseif idx == EHouseEditableOperation_lua.LingshouDongfang and v ~= 0 then
                    this.lsdfCheckBox:SetSelected(true, true)
                    CommonDefs.DictSet(this.SelectMap, typeof(GameObject), this.lsdfCheckBox.gameObject, typeof(Boolean), true)
                elseif idx == EHouseEditableOperation_lua.ExchangeTalisman and v ~= 0 then
                    this.exchangeTalisman:SetSelected(true, true)
                    CommonDefs.DictSet(this.SelectMap, typeof(GameObject), this.exchangeTalisman.gameObject, typeof(Boolean), true)
                end
                i = i + 2
            end
        end
        this.lastPrivalegeResult = opInfo
    end
end
CXiangfangPrivalegeManageWnd.m_IsEditNotSaved_CS2LuaHook = function (this) 
    if this.lastPrivalegeResult ~= nil then
        do
            local i = 0
            while i < this.lastPrivalegeResult.Count do
                local idx = math.floor(tonumber(this.lastPrivalegeResult[i] or 0))
                local v = math.floor(tonumber(this.lastPrivalegeResult[i + 1] or 0))
                if idx == EHouseEditableOperation_lua.EnterZhengwu then
                    if this.enterZhengwu.Selected ~= (v == 1) then
                        return true
                    end
                end
                if idx == EHouseEditableOperation_lua.OpRoomFurniture then
                    if this.zhengwuDecorate.Selected ~= (v == 1) then
                        return true
                    end
                end
                if idx == EHouseEditableOperation_lua.OpYardFurniture then
                    if this.yardDecorate.Selected ~= (v == 1) then
                        return true
                    end
                end
                if idx == EHouseEditableOperation_lua.UpgradeBuilding then
                    if this.houseConstruct.Selected ~= (v == 1) then
                        return true
                    end
                end
                if idx == EHouseEditableOperation_lua.LingshouDongfang then
                    if this.lsdfCheckBox.Selected ~= (v == 1) then
                        return true
                    end
                end
                if idx == EHouseEditableOperation_lua.ExchangeTalisman then
                    if this.exchangeTalisman.Selected ~= (v == 1) then
                        return true
                    end
                end
                i = i + 2
            end
        end
    end
    return false
end
CXiangfangPrivalegeManageWnd.m_RestoreToDefaultPrivalege_CS2LuaHook = function (this, go) 
    if this.CurrentSelectedId == 0 then
        g_MessageMgr:ShowMessage("HAVE_NO_FANGKE")
        return
    end
    if not this.IsOwner then
        g_MessageMgr:ShowMessage("FANGKE_PRIVALEGE_NOT_EDITABLE_BY_ROOMER")
        return
    end
    if this.RoomerIds.Count > 0 and CommonDefs.ListContains(this.RoomerIds, typeof(UInt64), this.CurrentSelectedId) then
        this:ClearEditableOp()
        this.enterZhengwu.Selected = true
        this:SaveRoomerPrivalege(go)
    else
        g_MessageMgr:ShowMessage("SELECT_FANGKE_BEFORE_SET")
    end
end
CXiangfangPrivalegeManageWnd.m_SaveRoomerPrivalege_CS2LuaHook = function (this, go) 
    if this.CurrentSelectedId == 0 then
        g_MessageMgr:ShowMessage("HAVE_NO_FANGKE")
        return
    end
    if not this.IsOwner then
        g_MessageMgr:ShowMessage("FANGKE_PRIVALEGE_NOT_EDITABLE_BY_ROOMER")
        return
    end

    if CommonDefs.ListContains(this.RoomerIds, typeof(UInt64), this.CurrentSelectedId) then
        local list = CreateFromClass(MakeGenericClass(List, Object))
        CommonDefs.ListAdd(list, typeof(UInt32), EHouseEditableOperation_lua.EnterZhengwu)
        if this.enterZhengwu.Selected then
            CommonDefs.ListAdd(list, typeof(Int32), 1)
        else
            CommonDefs.ListAdd(list, typeof(Int32), 0)
        end
        CommonDefs.ListAdd(list, typeof(UInt32), EHouseEditableOperation_lua.OpRoomFurniture)
        if this.zhengwuDecorate.Selected then
            CommonDefs.ListAdd(list, typeof(Int32), 1)
        else
            CommonDefs.ListAdd(list, typeof(Int32), 0)
        end
        CommonDefs.ListAdd(list, typeof(UInt32), EHouseEditableOperation_lua.OpYardFurniture)
        if this.yardDecorate.Selected then
            CommonDefs.ListAdd(list, typeof(Int32), 1)
        else
            CommonDefs.ListAdd(list, typeof(Int32), 0)
        end
        CommonDefs.ListAdd(list, typeof(UInt32), EHouseEditableOperation_lua.UpgradeBuilding)
        if this.houseConstruct.Selected then
            CommonDefs.ListAdd(list, typeof(Int32), 1)
        else
            CommonDefs.ListAdd(list, typeof(Int32), 0)
        end
        CommonDefs.ListAdd(list, typeof(UInt32), EHouseEditableOperation_lua.LingshouDongfang)
        if this.lsdfCheckBox.Selected then
            CommonDefs.ListAdd(list, typeof(Int32), 1)
        else
            CommonDefs.ListAdd(list, typeof(Int32), 0)
        end
        CommonDefs.ListAdd(list, typeof(UInt32), EHouseEditableOperation_lua.ExchangeTalisman)
        if this.exchangeTalisman.Selected then
            CommonDefs.ListAdd(list, typeof(Int32), 1)
        else
            CommonDefs.ListAdd(list, typeof(Int32), 0)
        end
        Gac2Gas.RequestSetRoomerPrivalege(this.CurrentSelectedId, MsgPackImpl.pack(list))
    else
        g_MessageMgr:ShowMessage("SELECT_FANGKE_BEFORE_SET")
    end
end
CXiangfangPrivalegeManageWnd.m_OnClickRoomerBtn_CS2LuaHook = function (this, go) 
    do
        local idx = 0
        while idx < this.roomerBtn.Length do
            if this.roomerBtn[idx] == go then
                if idx < this.RoomerIds.Count then
                    this.CurrentSelectedId = this.RoomerIds[idx]
                    Gac2Gas.QueryRoomerPrivalegeInfo(this.CurrentSelectedId)
                    do
                        local i = 0
                        while i < this.roomerBtn.Length do
                            if i ~= idx then
                                CommonDefs.GetComponent_GameObject_Type(this.roomerBtn[i], typeof(CButton)).Selected = false
                            end
                            i = i + 1
                        end
                    end
                    CommonDefs.GetComponent_GameObject_Type(this.roomerBtn[idx], typeof(CButton)).Selected = true
                else
                    g_MessageMgr:ShowMessage("SELECT_FANGKE_EMPTY")
                end
            end
            idx = idx + 1
        end
    end
end
CXiangfangPrivalegeManageWnd.m_OnEnable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeButton).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.save).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.save).onClick, MakeDelegateFromCSFunction(this.SaveRoomerPrivalege, VoidDelegate, this), true)
    UIEventListener.Get(this.revert).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.revert).onClick, MakeDelegateFromCSFunction(this.RestoreToDefaultPrivalege, VoidDelegate, this), true)
    EventManager.AddListenerInternal(EnumEventType.QueryRoomerPrivalegeInfoResult, MakeDelegateFromCSFunction(this.OnQueryRoomerPrivalegeInfoResult, MakeGenericClass(Action6, Boolean, Double, String, CRoommateProp, String, MakeGenericClass(List, Object)), this))
    do
        local i = 0
        while i < this.roomerBtn.Length do
            UIEventListener.Get(this.roomerBtn[i]).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.roomerBtn[i]).onClick, MakeDelegateFromCSFunction(this.OnClickRoomerBtn, VoidDelegate, this), true)
            i = i + 1
        end
    end
end
CXiangfangPrivalegeManageWnd.m_OnDisable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeButton).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.save).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.save).onClick, MakeDelegateFromCSFunction(this.SaveRoomerPrivalege, VoidDelegate, this), false)
    UIEventListener.Get(this.revert).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.revert).onClick, MakeDelegateFromCSFunction(this.RestoreToDefaultPrivalege, VoidDelegate, this), false)
    EventManager.RemoveListenerInternal(EnumEventType.QueryRoomerPrivalegeInfoResult, MakeDelegateFromCSFunction(this.OnQueryRoomerPrivalegeInfoResult, MakeGenericClass(Action6, Boolean, Double, String, CRoommateProp, String, MakeGenericClass(List, Object)), this))
    do
        local i = 0
        while i < this.roomerBtn.Length do
            UIEventListener.Get(this.roomerBtn[i]).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.roomerBtn[i]).onClick, MakeDelegateFromCSFunction(this.OnClickRoomerBtn, VoidDelegate, this), false)
            i = i + 1
        end
    end
end
CXiangfangPrivalegeManageWnd.m_OnCloseButtonClick_CS2LuaHook = function (this, go) 
    if this:IsEditNotSaved() then
        local msg = g_MessageMgr:FormatMessage("FANGKE_PRIVAGE_LEAVE_WITHOUT_SAVE")
        MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function () 
            this:SaveRoomerPrivalege(nil)
            this:Close()
        end), DelegateFactory.Action(function () 
            this:Close()
        end), nil, nil, false)
    else
        this:Close()
    end
end
