-- Auto Generated!!
local CPuppetExpressionActionItem = import "L10.UI.CPuppetExpressionActionItem"
local CPuppetExpressionActionView = import "L10.UI.CPuppetExpressionActionView"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local DelegateFactory = import "DelegateFactory"
local Expression_Show = import "L10.Game.Expression_Show"
local LocalString = import "LocalString"
CPuppetExpressionActionView.m_OnItemClick_CS2LuaHook = function (this, row) 
    if row >= 0 and row < this.mDataSource.Count then
        this.selectActionID = this.dataSource[row].ID
        local show = this.dataSource[row]
        if show ~= nil then
            if show.Status == 0 and not this:CheckIsLocked(show) then
                this.applyButton.Text = LocalString.GetString("进行动作")
            else
                this.applyButton.Text = LocalString.GetString("获取")
            end

            --if (show.Message != null)
            --{
            --    hintLabel.gameObject.SetActive(true);
            --    hintLabel.text = show.Message;
            --}
            --else
            --{
            --    hintLabel.gameObject.SetActive(false);
            --}
        end
    end
end
CPuppetExpressionActionView.m_ItemAt_CS2LuaHook = function (this, view, row) 
    if row < this.dataSource.Count then
        local item = TypeAs(view:GetFromPool(0), typeof(CPuppetExpressionActionItem))
        local expressionId = this.dataSource[row].ID
        local isInNeed = false
        item:Init(expressionId, isInNeed)
        item.OnItemLongPressed = DelegateFactory.Action(function () 
            view:SetSelectRow(row, view)
        end)
        return item
    end
    return nil
end
CPuppetExpressionActionView.m_OnApplyButtonClicked_CS2LuaHook = function (this, button) 
    local expressionId = this.selectActionID
    local show = Expression_Show.GetData(expressionId)
    if show ~= nil then
        CUIManager.CloseUI(CUIResources.PuppetInteractWnd)
        Gac2Gas.RequestShowExpressionWithPuppet(show.ID)
    end
end
CPuppetExpressionActionView.m_CheckIsLocked_CS2LuaHook = function (this, show) 
    if show == nil then
        return false
    end
    --if (show.LockGroup != 0 && CExpressionActionMgr.Inst.UnLockGroupInfo[show.LockGroup] == 0)
    --    return true;
    return false
end
