local UIInput = import "UIInput"
local UIToggle = import "UIToggle"
local CButton = import "L10.UI.CButton"
local GameObject = import "UnityEngine.GameObject"
local CLoginMgr = import "L10.Game.CLoginMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CItemMgr = import "L10.Game.CItemMgr"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local EnumGender = import "L10.Game.EnumGender"
local Constants = import "L10.Game.Constants"
local CFuxiFaceDNAMgr = import "L10.Game.CFuxiFaceDNAMgr"
local CFacialMgr = import "L10.Game.CFacialMgr"
local CLoadingWnd = import "L10.UI.CLoadingWnd"
local EnumClass = import "L10.Game.EnumClass"

LuaPinchFaceDisplayView = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaPinchFaceDisplayView, "DisplayView", "DisplayView", GameObject)
RegistChildComponent(LuaPinchFaceDisplayView, "CreatePlayerView", "CreatePlayerView", GameObject)
RegistChildComponent(LuaPinchFaceDisplayView, "Input", "Input", UIInput)
RegistChildComponent(LuaPinchFaceDisplayView, "RandomButton", "RandomButton", GameObject)
RegistChildComponent(LuaPinchFaceDisplayView, "Checkbox", "Checkbox", UIToggle)
RegistChildComponent(LuaPinchFaceDisplayView, "Evaluation", "Evaluation", GameObject)
RegistChildComponent(LuaPinchFaceDisplayView, "BackDisplayViewButton", "BackDisplayViewButton", CButton)
RegistChildComponent(LuaPinchFaceDisplayView, "FinishModifyBtnPos", "FinishModifyBtnPos", GameObject)
RegistChildComponent(LuaPinchFaceDisplayView, "ShowCreatePlayerViewBtnPos", "ShowCreatePlayerViewBtnPos", GameObject)
RegistChildComponent(LuaPinchFaceDisplayView, "EnterGameButtonPos", "EnterGameButtonPos", GameObject)
RegistChildComponent(LuaPinchFaceDisplayView, "ShareBtn", "ShareBtn", CButton)

--@endregion RegistChildComponent end
RegistClassMember(LuaPinchFaceDisplayView, "m_ShowCreatePlayerViewBtn")
RegistClassMember(LuaPinchFaceDisplayView, "m_EnterGameButton")
RegistClassMember(LuaPinchFaceDisplayView, "m_UploadBtn")
RegistClassMember(LuaPinchFaceDisplayView, "m_QRCodeForCloudGame")

function LuaPinchFaceDisplayView:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.RandomButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRandomButtonClick()
	end)


	
	UIEventListener.Get(self.BackDisplayViewButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBackDisplayViewButtonClick()
	end)


    --@endregion EventBind end
    UIEventListener.Get(self.ShareBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareBtnClick()
	end)
    self.ShareBtn.gameObject:SetActive(not CommonDefs.IS_VN_CLIENT)
    self.m_UploadBtn = self.transform:Find("DisplayView/Right/UploadBtn").gameObject
    self.m_UploadBtn:SetActive(not LuaCloudGameFaceMgr:IsCloudGameFace())
    UIEventListener.Get(self.m_UploadBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnUploadBtnClick()
	end)

    self:InitCommonBtns()
end

function LuaPinchFaceDisplayView:InitCommonBtns()
    local btn = self.ShowCreatePlayerViewBtnPos.transform:Find("CommonButton")
    if LuaCloudGameFaceMgr:IsDaShenCloudGameFace() then self.ShowCreatePlayerViewBtnPos.gameObject:SetActive(false) end
    UIEventListener.Get(btn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShowCreatePlayerViewBtnClick()
	end)
    self.m_ShowCreatePlayerViewBtn = btn:GetComponent(typeof(CButton))

    btn = self.FinishModifyBtnPos.transform:Find("CommonButton")
    UIEventListener.Get(btn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnFinishModifyBtnClick()
	end)
    btn.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("完成")
	
    btn = self.EnterGameButtonPos.transform:Find("CommonButton")
    UIEventListener.Get(btn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnEnterGameButtonClick()
	end)
    btn.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("进入游戏")
    self.m_EnterGameButton = btn:GetComponent(typeof(CButton))

    self.m_QRCodeForCloudGame = self.DisplayView.transform:Find("Right/QRCodeForCloudGame"):GetComponent(typeof(UITexture))
    self.m_QRCodeForCloudGame.gameObject:SetActive(false)
end

function LuaPinchFaceDisplayView:Start()
    self.Checkbox.value = false
    self.FinishModifyBtnPos.gameObject:SetActive(LuaPinchFaceMgr.m_IsInModifyMode)
    self.m_ShowCreatePlayerViewBtn.gameObject:SetActive(LuaPinchFaceMgr.m_IsInCreateMode)
end

function LuaPinchFaceDisplayView:ShowView()
    self:InitEvaluation()
    self.CreatePlayerView.gameObject:SetActive(false)
    self.DisplayView.gameObject:SetActive(false)
    self.Evaluation.gameObject:SetActive(false)
    if LuaCloudGameFaceMgr:IsCloudGameFace() then
        self.m_ShowCreatePlayerViewBtn.Text = LocalString.GetString("返回前世镜")
    else
        self.m_ShowCreatePlayerViewBtn.Text = LuaPinchFaceMgr.m_IsOffline and LocalString.GetString("完成") or LocalString.GetString("创建角色")
    end
    LuaPinchFaceMgr.m_CanCacheOfflineData = true
	LuaPinchFaceMgr:CacheOfflineData()
end

function LuaPinchFaceDisplayView:HandleAutoLoginByCmdLineArgs()
    if CLoginMgr.Inst:NeedAutoLoginByCmdLineArgs() then
        CLoginMgr.Inst:ClearAutoLoginByCmdLineArgs()
        if  CLoginMgr.Inst.m_ExistingPlayerList.Count == 0 then
            math.randomseed(os.time()) --不添加这一句每次运行客户端到这里得到的随机结果是一样的，导致新建角色会重名
            local classval = math.random(1, EnumToInt(EnumClass.Undefined)-1)
            local genderval = math.random(0, 1)
            local charStr = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
            local suffix = ""
            for i=1,6 do
                local idx = math.random(1, #charStr)
                suffix = suffix..string.sub(charStr, idx, idx)
            end
            local name = "whlaa"..tostring(classval*100+genderval)..suffix
            self.Input.value = name
            self:OnEnterGameButtonClick()
        end
        return true
    end
    return false
end

function LuaPinchFaceDisplayView:PlayMovie(useMovieCapture, enableRotate)
    LuaPinchFaceMgr:PlayMovie(useMovieCapture, function()  
        self.Evaluation.gameObject:SetActive(true)
        RegisterTickOnce(function ()
            if self.DisplayView then
                self.DisplayView.gameObject:SetActive(true)
            end
        end,100)
    end, function()
        self.Evaluation.gameObject:SetActive(false)
        self.DisplayView.gameObject:SetActive(false)
    end,enableRotate)
end

function LuaPinchFaceDisplayView:InitEvaluation()
    local evaluationCommentData = LuaPinchFaceMgr:AnalyzeCurEvaluation()
    if evaluationCommentData == nil then
        self.Evaluation.gameObject:SetActive(false)
        return
    end
    local male = self.Evaluation.transform:Find("Male")
    local female = self.Evaluation.transform:Find("Famle")
    male.gameObject:SetActive(LuaPinchFaceMgr.m_CurEnumGender == EnumGender.Male)
    female.gameObject:SetActive(LuaPinchFaceMgr.m_CurEnumGender ~= EnumGender.Male)
    local root = LuaPinchFaceMgr.m_CurEnumGender == EnumGender.Male and male or female
    local titleLable = root:Find("TitleLable")
    local label01 = titleLable.transform:Find("Label01"):GetComponent(typeof(UILabel))
    local label02 = titleLable.transform:Find("Label02"):GetComponent(typeof(UILabel))
    local label03 = titleLable.transform:Find("Label03"):GetComponent(typeof(UILabel))
    local label04 = titleLable.transform:Find("Label04"):GetComponent(typeof(UILabel))
    label01.text = evaluationCommentData.Evaluation[0]
    label02.text = evaluationCommentData.Evaluation[1]
    label03.text = evaluationCommentData.Evaluation[2]
    label04.text = evaluationCommentData.Evaluation[3]
    local poemLable = root:Find("PoemLable")
    local des1 = poemLable.transform:Find("Label01"):GetComponent(typeof(UILabel))
    local des2 = poemLable.transform:Find("Label02"):GetComponent(typeof(UILabel))
    des1.text = evaluationCommentData.Description[0]
    des2.text = evaluationCommentData.Description[1]
    self.Evaluation.gameObject:SetActive(true)
end

function LuaPinchFaceDisplayView:OnEnable()
    self:ShowView()
    g_ScriptEvent:AddListener("OnShowPinchFaceDisplayView_PlayMovie", self, "OnPlayMovie")
    g_ScriptEvent:AddListener("EnterPreLoginMode", self, "OnEnterPreLoginMode")
    g_ScriptEvent:AddListener("PreLoginModeCharacterExists", self, "OnPreLoginModeCharacterExists")
end

function LuaPinchFaceDisplayView:OnDisable()
    self:CancelEnterGameForCreateRoleTick()
    g_ScriptEvent:RemoveListener("OnShowPinchFaceDisplayView_PlayMovie", self, "OnPlayMovie")
    g_ScriptEvent:RemoveListener("EnterPreLoginMode", self, "OnEnterPreLoginMode")
    g_ScriptEvent:RemoveListener("PreLoginModeCharacterExists", self, "OnPreLoginModeCharacterExists")
    LuaPinchFaceMgr.m_IsEnableRotateRole = true
end

function LuaPinchFaceDisplayView:OnEnterPreLoginMode()
    if CLoginMgr.Inst.PreLoginModeCharacterExists then
        self:OnPreLoginModeCharacterExists()
    end
end

function LuaPinchFaceDisplayView:OnPreLoginModeCharacterExists()
    self.m_EnterGameButton.Text = LocalString.GetString("预约")
    LuaPinchFaceMgr:ClearOfflineData()
end

function LuaPinchFaceDisplayView:OnPlayMovie()
    if self:HandleAutoLoginByCmdLineArgs() then return end
    self:PlayMovie(false, true)
end

function LuaPinchFaceDisplayView:CheckNameValid(nameStr, showSign)
    if CommonDefs.IsSeaMultiLang() then
        local length = CommonDefs.StringLength(nameStr)
        if length < Constants.SeaMinCharLimitForPlayerName or length > Constants.SeaMaxCharLimitForPlayerName then
            if showSign then
                g_MessageMgr:ShowMessage("Sea_Login_Create_Role_Failed_Name_Length_Not_Fit", Constants.SeaMinCharLimitForPlayerName, Constants.SeaMaxCharLimitForPlayerName)
            end
            return false
        end
    else
        local length = CUICommonDef.GetStrByteLength(nameStr)
        if length < Constants.MinCharLimitForPlayerName or CUICommonDef.GetStrByteLength(nameStr) > Constants.MaxCharLimitForPlayerName then
            if showSign then
                g_MessageMgr:ShowMessage("Login_Create_Role_Failed_Name_Length_Not_Fit")
            end
            return false
        end
    end
	if System.Text.RegularExpressions.Regex.IsMatch(nameStr, "^[0-9]+$") then
		if showSign then
			g_MessageMgr:ShowMessage("Login_Create_Role_Failed_Name_Cannot_Be_All_Numbers")
		end
		return false
	end
	if not CWordFilterMgr.Inst:CheckName(nameStr) then
		if showSign then
			g_MessageMgr:ShowMessage("Name_Violation")
		end
		return false
	end

	return true
end

function LuaPinchFaceDisplayView:OnInputChange()
    local str = self.Input.value
    if CommonDefs.IsSeaMultiLang() then
        if CommonDefs.StringLength(str) > Constants.SeaMaxCharLimitForPlayerName then
            repeat
                str = CommonDefs.StringSubstring2(str, 0, CommonDefs.StringLength(str) - 1)
            until not (CommonDefs.StringLength(str) > Constants.SeaMaxCharLimitForPlayerName)
            self.Input.value = str
        end
    else
        if CUICommonDef.GetStrByteLength(str) > Constants.MaxCharLimitForPlayerName then
            repeat
                str = CommonDefs.StringSubstring2(str, 0, CommonDefs.StringLength(str) - 1)
            until not (CUICommonDef.GetStrByteLength(str) > Constants.MaxCharLimitForPlayerName)
            self.Input.value = str
        end
    end
end

function LuaPinchFaceDisplayView:RegisterEnterGameForCreateRoleTick( )
    self:CancelEnterGameForCreateRoleTick()
    self.m_EnterGameForCreateRoleTick = RegisterTickOnce(function ( ... )
		self.m_EnterGameButton.Enabled = true
	end, 3000)
end

function LuaPinchFaceDisplayView:CancelEnterGameForCreateRoleTick()
	if self.m_EnterGameForCreateRoleTick then
        UnRegisterTick(self.m_EnterGameForCreateRoleTick)
        self.m_EnterGameForCreateRoleTick = nil
    end
end

function LuaPinchFaceDisplayView:GetAboutToExpireModifyItem()
    local items = PinchFace_Setting.GetData().PinchFaceModifyItems
    local itemId = items[0]
    local itemSet = {}
    for i = 0, items.Length - 1 do
        itemSet[items[i]] = true
    end
    local minTimeStamp = 0
    if CClientMainPlayer.Inst then
        local bagSize = CClientMainPlayer.Inst.ItemProp:GetPlaceSize(EnumItemPlace.Bag)
        for i = 1, bagSize, 1 do
            local id = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Bag, i)
            local item = CItemMgr.Inst:GetById(id)
            if item and item.Item and not item.Item:IsExpire() and itemSet[item.TemplateId] then
                itemId = item.TemplateId
            end
        end
    end
    return itemId
end

function LuaPinchFaceDisplayView:FinishCloudFace()
    CFacialMgr.GetFacialDataQRCodeInHub(LuaPinchFaceMgr.m_RO, EnumToInt(LuaPinchFaceMgr.m_CurEnumClass), EnumToInt(LuaPinchFaceMgr.m_CurEnumGender), LuaPinchFaceMgr.m_HairId, LuaPinchFaceMgr.m_HeadId, nil, DelegateFactory.Action_Texture(function (texture)
        self.DisplayView.transform:Find("Left").gameObject:SetActive(false)
        self.ShareBtn.gameObject:SetActive(false)
        self.ShowCreatePlayerViewBtnPos:SetActive(false)
        self.m_QRCodeForCloudGame.gameObject:SetActive(true)
        self.m_QRCodeForCloudGame.mainTexture = texture
        LuaCloudGameFaceMgr:FinishCloudFace()
        RegisterTickOnce(function()
            self.DisplayView.transform:Find("Left").gameObject:SetActive(true)
            self.ShareBtn.gameObject:SetActive(true)
            self.ShowCreatePlayerViewBtnPos:SetActive(true)
            self.m_QRCodeForCloudGame.gameObject:SetActive(false)
            if self.m_QRCodeForCloudGame.mainTexture then
                GameObject.Destroy(self.m_QRCodeForCloudGame.mainTexture)
                self.m_QRCodeForCloudGame.mainTexture = nil
            end
        end, 2000)
    end))
end

--@region UIEvent

function LuaPinchFaceDisplayView:OnFinishModifyBtnClick()
    local msg = g_MessageMgr:FormatMessage("PinchFace_FinishModifyBtnClick_ConFirm")
    local itemId = self:GetAboutToExpireModifyItem()
    local isFacialDataChanged = LuaPinchFaceMgr:IsFacialDataChanged()
    local isChanged = isFacialDataChanged or (LuaPinchFaceMgr.m_HasCustomFacialData and not LuaPinchFaceMgr.m_UseCurFacialData)
    if not isChanged and LuaPinchFaceMgr.m_PreselectionIndex == LuaPinchFaceMgr.m_InitialPreselectionIndex then
        g_MessageMgr:ShowMessage("PinchFace_OldPlayer_HasNoChange")
        return
    end

    -- 老用户完成时候也上传捏脸站
    if isChanged then
        local name = ""
        if CClientMainPlayer.Inst then
            name = CClientMainPlayer.Inst.Name
        end
        LuaPinchFaceMgr:UploadWork(name)
    end

    LuaItemInfoMgr:ShowItemConsumeWndWithQuickAccess(itemId, 1, msg, true, true, LocalString.GetString("确定"), function (enough)
        CLoginMgr.Inst:SetFacialData(isFacialDataChanged and LuaPinchFaceMgr.m_RO.FaceDnaData or nil, isFacialDataChanged and LuaPinchFaceMgr.m_RO.FacialPartData or nil)
        local xianfanStatus = LuaPinchFaceMgr.m_IsModifyXianSheng and 1 or 0
        local checksum = CLoginMgr.Inst.CheckSumFacialData
        local FacialData = CLoginMgr.Inst.FacialData
        local facialId = LuaPinchFaceMgr.m_PreselectionIndex + 1 
        local initialFacialId = LuaPinchFaceMgr.m_IsModifyXianSheng and 2 or 1
        facialId = (facialId == initialFacialId) and 0 or facialId
        local hairId = LuaPinchFaceMgr.m_HairId and LuaPinchFaceMgr.m_HairId or 0
        LuaPinchFaceMgr:ClearOfflineData()
        Gac2Gas.RequestSetCustomFacialData(xianfanStatus, EnumToInt(LuaPinchFaceMgr.m_CurEnumGender), hairId, facialId, checksum, FacialData[0], FacialData[1], FacialData[2], FacialData[3], FacialData[4], FacialData[5])
    end, nil, false)
end


function LuaPinchFaceDisplayView:OnShowCreatePlayerViewBtnClick()
    if LuaCloudGameFaceMgr:IsCloudGameFace() then
        self:FinishCloudFace()
        return
    end
    if LuaPinchFaceMgr.m_IsOffline then
        LuaPinchFaceMgr.m_CanCacheOfflineData = true
	    LuaPinchFaceMgr:CacheOfflineData()
        CUIManager.CloseUI(CLuaUIResources.PinchFaceWnd)
        g_MessageMgr:ShowMessage("PinchFace_OfflineData_Save")
        return
    end
    self.CreatePlayerView.gameObject:SetActive(true)
    self.DisplayView.gameObject:SetActive(false)
end

function LuaPinchFaceDisplayView:OnBackDisplayViewButtonClick()
    self.CreatePlayerView.gameObject:SetActive(false)
    self.DisplayView.gameObject:SetActive(true)
end


function LuaPinchFaceDisplayView:OnRandomButtonClick()
    local name = nil
	if not LuaPinchFaceMgr.m_CurEnumGender then return end
    for i=1,5 do --最多尝试随机五次名字
        name = CLoginMgr.Inst:GetRandomName(LuaPinchFaceMgr.m_CurEnumGender, self.Checkbox.value)
        if self:CheckNameValid(name,false) then
            break
        end
    end
    self.Input.value = name
	self:OnInputChange()
    self:DoRandomAnimation(self.RandomButton.gameObject, 0)
end

function LuaPinchFaceDisplayView:DoRandomAnimation(go, times)
	local v = go.transform.eulerAngles
	v.z = v.z - 180
	LuaTweenUtils.OnComplete(LuaTweenUtils.TweenRotationZ(go.transform, v.z, 0.05), function ()
		if times<6 then
			self:DoRandomAnimation(go, times+1)
		end
	end)
end

function LuaPinchFaceDisplayView:OnEnterGameButtonClick()
    local name = self.Input.value
	if not self:CheckNameValid(name,true) then
		return
	end
	if not LuaPinchFaceMgr.m_RO then return end
	CLoginMgr.Inst.Name = name
    CLoginMgr.Inst.HeadId = LuaPinchFaceMgr.m_HeadId
	CLoginMgr.Inst.BodyId = 0
    CLoginMgr.Inst.Class = LuaPinchFaceMgr.m_CurEnumClass
    CLoginMgr.Inst.Gender = LuaPinchFaceMgr.m_CurEnumGender
    local isChanged = LuaPinchFaceMgr:IsFacialDataChanged() 
	CLoginMgr.Inst:SetFacialData(isChanged and LuaPinchFaceMgr.m_RO.FaceDnaData or nil, isChanged and LuaPinchFaceMgr.m_RO.FacialPartData or nil)
    local facialId = LuaPinchFaceMgr.m_PreselectionIndex + 1
    CLoginMgr.Inst.FacialId = (facialId == 1) and 0 or facialId
    local hairId = LuaPinchFaceMgr.m_HairId and LuaPinchFaceMgr.m_HairId or 0
    CLoginMgr.Inst:SetLuoZhuangHairId(hairId, 0) 	
    if CLoginMgr.Inst:CheckLoginConnection() then
        LuaPinchFaceMgr:ClearOfflineData()
    else
        LuaPinchFaceMgr.m_CanCacheOfflineData = true
        LuaPinchFaceMgr:CacheOfflineData()
    end
	CLoginMgr.Inst:RequestCreatePlayerWithFacialData()
    -- 进入游戏前上传捏脸站作品
    if isChanged then
        LuaPinchFaceMgr:UploadWork(name)
    end
    
	EventManager.Broadcast(EnumEventType.PlayerLogin)
    CLoginMgr.Inst.IsCreateRole = true
	self.m_EnterGameButton.Enabled = false
	self:RegisterEnterGameForCreateRoleTick()

    if CommonDefs.IS_VN_CLIENT then
        LuaSEASdkMgr:AppsflyerNotify("NRU_role", SafeStringFormat3("{}"))
        LuaSEASdkMgr:FaceBookTracking("FB_NRU_role", "")
    end
end

function LuaPinchFaceDisplayView:OnShareBtnClick()
    CFuxiFaceDNAMgr.Inst:TryReportApplyLog(CFuxiFaceDNAMgr.Inst.m_IsRandom and "RandomShare" or "PhotoShare")
    LuaPinchFaceMgr:ShowShareWnd()
end

function LuaPinchFaceDisplayView:OnUploadBtnClick()
    CUIManager.ShowUI(CLuaUIResources.PinchFaceHubUploadWnd)
end
--@endregion UIEvent

