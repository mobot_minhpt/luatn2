local CUIFx = import "L10.UI.CUIFx"
local CButton = import "L10.UI.CButton"
local UIProgressBar = import "UIProgressBar"
local CUITexture = import "L10.UI.CUITexture"
local GameObject = import "UnityEngine.GameObject"
local DelegateFactory  = import "DelegateFactory"
local UITabBar = import "L10.UI.UITabBar"
local UILabel = import "UILabel"
local QnTabButton = import "L10.UI.QnTabButton"

LuaCrossServerColiseumMainWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaCrossServerColiseumMainWnd, "Tabs", "Tabs", UITabBar)
RegistChildComponent(LuaCrossServerColiseumMainWnd, "TimeInfoLabel", "TimeInfoLabel", UILabel)
RegistChildComponent(LuaCrossServerColiseumMainWnd, "DanLabel", "DanLabel", UILabel)
RegistChildComponent(LuaCrossServerColiseumMainWnd, "DanSprites", "DanSprites", GameObject)
RegistChildComponent(LuaCrossServerColiseumMainWnd, "ScoreLabel", "ScoreLabel", UILabel)
RegistChildComponent(LuaCrossServerColiseumMainWnd, "Progress", "Progress", UIProgressBar)
RegistChildComponent(LuaCrossServerColiseumMainWnd, "DanTex", "DanTex", CUITexture)
RegistChildComponent(LuaCrossServerColiseumMainWnd, "ProgressLabel", "ProgressLabel", UILabel)
RegistChildComponent(LuaCrossServerColiseumMainWnd, "WinRateLabel", "WinRateLabel", UILabel)
RegistChildComponent(LuaCrossServerColiseumMainWnd, "WinNumLabel", "WinNumLabel", UILabel)
RegistChildComponent(LuaCrossServerColiseumMainWnd, "LoseNumLabel", "LoseNumLabel", UILabel)
RegistChildComponent(LuaCrossServerColiseumMainWnd, "WinningStreakLabel", "WinningStreakLabel", UILabel)
RegistChildComponent(LuaCrossServerColiseumMainWnd, "ShowRecordsButton", "ShowRecordsButton", GameObject)
RegistChildComponent(LuaCrossServerColiseumMainWnd, "RankButton", "RankButton", GameObject)
RegistChildComponent(LuaCrossServerColiseumMainWnd, "ShopButton", "ShopButton", GameObject)
RegistChildComponent(LuaCrossServerColiseumMainWnd, "RankLabel", "RankLabel", UILabel)
RegistChildComponent(LuaCrossServerColiseumMainWnd, "ShopScoreLabel", "ShopScoreLabel", UILabel)
RegistChildComponent(LuaCrossServerColiseumMainWnd, "WeeklyRewardButton", "WeeklyRewardButton", GameObject)
RegistChildComponent(LuaCrossServerColiseumMainWnd, "SeasonRewardButton", "SeasonRewardButton", GameObject)
RegistChildComponent(LuaCrossServerColiseumMainWnd, "StartGameButton", "StartGameButton", CButton)
RegistChildComponent(LuaCrossServerColiseumMainWnd, "CanceButton", "CanceButton", GameObject)
RegistChildComponent(LuaCrossServerColiseumMainWnd, "ReadMeButton", "ReadMeButton", GameObject)
RegistChildComponent(LuaCrossServerColiseumMainWnd, "Bg", "Bg", CUITexture)
RegistChildComponent(LuaCrossServerColiseumMainWnd, "NotGetWeeklyRewardRoot", "NotGetWeeklyRewardRoot", GameObject)
RegistChildComponent(LuaCrossServerColiseumMainWnd, "NotGetWeeklyRewardRootFx", "NotGetWeeklyRewardRootFx", CUIFx)
RegistChildComponent(LuaCrossServerColiseumMainWnd, "ReceivedWeeklyRewardRoot", "ReceivedWeeklyRewardRoot", GameObject)
RegistChildComponent(LuaCrossServerColiseumMainWnd, "StartGameView", "StartGameView", GameObject)
RegistChildComponent(LuaCrossServerColiseumMainWnd, "TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaCrossServerColiseumMainWnd, "WeeklyGetScoreLabel", "WeeklyGetScoreLabel", UILabel)
RegistChildComponent(LuaCrossServerColiseumMainWnd, "ShowRecordsLabel", "ShowRecordsLabel", GameObject)
RegistChildComponent(LuaCrossServerColiseumMainWnd, "LevelNameLabel", "LevelNameLabel", UILabel)
--@endregion RegistChildComponent end
RegistClassMember(LuaCrossServerColiseumMainWnd,"m_TabIndex")
RegistClassMember(LuaCrossServerColiseumMainWnd,"m_IsInCurrentSeason")
function LuaCrossServerColiseumMainWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.ShowRecordsButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShowRecordsButtonClick()
	end)


	
	UIEventListener.Get(self.RankButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRankButtonClick()
	end)


	
	UIEventListener.Get(self.ShopButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShopButtonClick()
	end)


	
	UIEventListener.Get(self.WeeklyRewardButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnWeeklyRewardButtonClick()
	end)


	
	UIEventListener.Get(self.SeasonRewardButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSeasonRewardButtonClick()
	end)


	
	UIEventListener.Get(self.CanceButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCanceButtonClick()
	end)


	
	UIEventListener.Get(self.ReadMeButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnReadMeButtonClick()
	end)


    UIEventListener.Get(self.StartGameButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnStartGameButtonClick()
	end)

    --@endregion EventBind end
end

function LuaCrossServerColiseumMainWnd:Init()
    self:InitLevelNameLabel()
    self.m_IsInCurrentSeason = LuaColiseumMgr:IsInCurrentSeason()
    self.WeeklyRewardButton.gameObject:SetActive(self.m_IsInCurrentSeason)
    self.DanLabel.color = Color.white
    self.WeeklyGetScoreLabel.gameObject:SetActive(false)
    self.TimeInfoLabel.text = g_MessageMgr:FormatMessage(self.m_IsInCurrentSeason and "CrossServerColiseumMainWnd_TimeInfo" or "CrossServerColiseumMainWnd_NotInSeason")
    self.Tabs.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
	    self:OnTabsTabChange(index)
	end)
    self.Tabs.transform:GetChild(0):GetComponent(typeof(CUITexture)):LoadMaterial("UI/Texture/Transparent/Material/coliseummainwnd_tab_n.mat")
    self.Tabs:ChangeTab(LuaColiseumMgr.m_LastGameType - 1, false)
    self.NotGetWeeklyRewardRoot:SetActive(false)
    self.ReceivedWeeklyRewardRoot:SetActive(false)
    Gac2Gas.RequestArenaInfo()
end

function LuaCrossServerColiseumMainWnd:OnEnable()
    g_ScriptEvent:AddListener("OnSendArenaInfo", self, "OnSendArenaInfo")
end

function LuaCrossServerColiseumMainWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnSendArenaInfo", self, "OnSendArenaInfo")
end

function LuaCrossServerColiseumMainWnd:OnSendArenaInfo()
    local index = self.m_TabIndex + 1
    local data = LuaColiseumMgr.m_BattleData[index]
    if not data then return end
    local score = data.myRankScore
    local winTimes = data.winTimes
    local loseTimes = data.lostTimes
    local winningStreakNum = data.winCombo
    local rank = data.myRank
    local shopScore = LuaColiseumMgr.m_ArenaPlayScore
    local isMatching = LuaColiseumMgr.m_IsMatching
    self.StartGameButton.gameObject:SetActive((isMatching and LuaColiseumMgr.m_MatchType ~= index) or (not isMatching))
    self.CanceButton:SetActive(isMatching and (LuaColiseumMgr.m_MatchType == index))
    local rewardState = LuaColiseumMgr.m_WeeklyScoreRewardState
    self.WeeklyGetScoreLabel.gameObject:SetActive(self.m_IsInCurrentSeason)
    self.WeeklyGetScoreLabel.text = SafeStringFormat3("%s/%s",LuaColiseumMgr.m_WeeklyGetShopScore,LuaColiseumMgr.m_WeeklyGetShopScoreLimit)
    self:InitRwardsView(rewardState == 2,rewardState == 1)
    self:InitMainInfoView(score,winTimes,loseTimes,winningStreakNum,rank,shopScore)
end

function LuaCrossServerColiseumMainWnd:InitMainInfoView(score,winTimes,loseTimes,winningStreakNum,rank,shopScore)
    local setting = Arena_Setting.GetData()
    
    --段位
    local dan = LuaColiseumMgr:GetDan(score)
    LuaColiseumMgr.m_Score = score
    local danInfoData = Arena_DanInfo.GetData(dan)

    --段位名称文字和颜色
    local danLabelText, danLabelColor = danInfoData.DanName,danInfoData.DanColor

    --段位->大段位图标 
    local danTextPaths = {
        "UI/Texture/Transparent/Material/coliseummainwnd_dan_01.mat",
        "UI/Texture/Transparent/Material/coliseummainwnd_dan_02.mat",
        "UI/Texture/Transparent/Material/coliseummainwnd_dan_03.mat",
        "UI/Texture/Transparent/Material/coliseummainwnd_dan_04.mat",
        "UI/Texture/Transparent/Material/coliseummainwnd_dan_05.mat"
    }

    --进度条数值, 小段位点数
	local processValue, showDanSpritesNum = LuaColiseumMgr:GetProcessValueAndDanSpritesNum(score)

    --段位->小段位文字 映射
    local smallDanTexts = {LocalString.GetString("一段"),LocalString.GetString("二段"),LocalString.GetString("三段"),LocalString.GetString("四段"),LocalString.GetString("五段")}
    local smallDan = smallDanTexts[showDanSpritesNum] and showDanSpritesNum or 1
    self.ScoreLabel.text = score
    self.DanLabel.color = NGUIText.ParseColor24(danLabelColor, 0)
    self.DanLabel.text = CUICommonDef.TranslateToNGUIText(SafeStringFormat3(LocalString.GetString("%s·%s"),danLabelText,smallDanTexts[smallDan]))
    self.DanTex:LoadMaterial(danTextPaths[dan])
    local childCount = self.DanSprites.transform.childCount
    for i = 0, childCount - 1 do
        local child = self.DanSprites.transform:GetChild(i)
        child.transform:Find("Sprite"):GetComponent(typeof(UITexture)).enabled = i < showDanSpritesNum
    end
    self.Progress.value = processValue / 100
    self.ProgressLabel.text = SafeStringFormat3("%d/%d",processValue, 100)
    self.WinNumLabel.text = winTimes
    self.LoseNumLabel.text = loseTimes
    local battleNum = winTimes + loseTimes
    self.WinRateLabel.text = (battleNum == 0) and "-" or (math.floor(winTimes / (battleNum) * 100) .."%")
    self.WinningStreakLabel.text =  SafeStringFormat3(LocalString.GetString("%d连胜"),winningStreakNum)
    self.WinningStreakLabel.gameObject:SetActive(winningStreakNum > 1)
    self.RankLabel.text = (rank or rank <= 1000) and rank or LocalString.GetString("未上榜")
    self.ShopScoreLabel.text = shopScore
end

function LuaCrossServerColiseumMainWnd:InitRwardsView(isReceivedWeeklyReward,canGetWeeklyReward)
    self.NotGetWeeklyRewardRoot:SetActive(canGetWeeklyReward)
    self.ReceivedWeeklyRewardRoot:SetActive(isReceivedWeeklyReward)
end

function LuaCrossServerColiseumMainWnd:InitLevelNameLabel()
    self.LevelNameLabel.text = LuaColiseumMgr:GetLevelName()
end

--@region UIEvent
function LuaCrossServerColiseumMainWnd:OnTabsTabChange(index)
    local text = self.Tabs.transform:GetChild(index):GetComponent(typeof(QnTabButton)).Text
    local btnTexture = self.Tabs.transform:GetChild(index):GetComponent(typeof(CUITexture))
    btnTexture:LoadMaterial("UI/Texture/Transparent/Material/coliseummainwnd_tab_s.mat")
    if self.m_TabIndex then
        local btnTexture = self.Tabs.transform:GetChild(self.m_TabIndex):GetComponent(typeof(CUITexture))
        btnTexture:LoadMaterial("UI/Texture/Transparent/Material/coliseummainwnd_tab_n.mat")
    end
    self.StartGameButton.Text = SafeStringFormat3("%s%s",text, LocalString.GetString("匹配"))
    self.m_TabIndex = index
    self:OnSendArenaInfo()
    self.ShowRecordsLabel.gameObject:SetActive(index == 0)
    self.Bg:LoadMaterial(index == 0 and "UI/Texture/Transparent/Material/coliseummainwnd_bg_01.mat" or "UI/Texture/Transparent/Material/coliseummainwnd_bg_03.mat")
end


function LuaCrossServerColiseumMainWnd:OnShowRecordsButtonClick()
    CUIManager.ShowUI(CLuaUIResources.ColiseumRecordsTipWnd)
end


function LuaCrossServerColiseumMainWnd:OnRankButtonClick()
    LuaColiseumMgr.m_RankWndType = self.m_TabIndex + 1
    CUIManager.ShowUI(CLuaUIResources.ColiseumRankWnd)
end

function LuaCrossServerColiseumMainWnd:OnShopButtonClick()
    CUIManager.CloseUI(CLuaUIResources.ColiseumShopWnd)
    CUIManager.ShowUI(CLuaUIResources.ColiseumShopWnd)
end


function LuaCrossServerColiseumMainWnd:OnWeeklyRewardButtonClick()
    CUIManager.ShowUI(CLuaUIResources.ColiseumWeeklyRewardInfoWnd)
end

function LuaCrossServerColiseumMainWnd:OnSeasonRewardButtonClick()
    CUIManager.ShowUI(CLuaUIResources.ColiseumSeasonRewardWnd)
end


function LuaCrossServerColiseumMainWnd:OnStartGameButtonClick()
    if not self.m_IsInCurrentSeason then
        local key = "CrossServerColiseumMainWnd_NotInCurrentSeasonAlert" 
        local curIndex = CLuaPlayerSettings.Get(key, 0)
        local curSeasonId = LuaArenaMgr:GetCurrentSeasonId()
        if curIndex ~= curSeasonId then
            CLuaPlayerSettings.Set(key, curSeasonId)
            g_MessageMgr:ShowMessage("CrossServerColiseumMainWnd_NotInCurrentSeasonAlert")
        end
    end
    Gac2Gas.RequestSignupArena(self.m_TabIndex + 1)
end


function LuaCrossServerColiseumMainWnd:OnCanceButtonClick()
    Gac2Gas.RequestCancelSignupArena()
end


function LuaCrossServerColiseumMainWnd:OnReadMeButtonClick()
    g_MessageMgr:ShowMessage("CrossServerColiseumMainWnd_ReadMe")
end


--@endregion UIEvent
