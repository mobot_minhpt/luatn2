local msgpack = require("3rdParty/MessagePack/MessagePack_V33")
g_MessagePack = {}
function g_MessagePack.unpack(data)
	if data == nil or data.Length == 0 then
		return nil
	end
	local v = CommonDefs.GetLuaStringBuffer(data)

	if CommonDefs.IS_VN_CLIENT then
		local tmpList = msgpack.unpack(v)
		if tmpList then
			for i = 1, #tmpList  do
				local tmpValue = tmpList[i]
				if type(tmpValue) == "string" then
					tmpList[i] = LocalString.TranslateAndFormatText(tmpValue)
				end
			end
			return tmpList
		else
			return msgpack.unpack(v)
		end
	else
		return msgpack.unpack(v)	
	end
end

function g_MessagePack.unpack_string(data)
	if data == nil or #data == 0 then
		return nil
	end
	return msgpack.unpack(data)
end

--直接传入一个table
function g_MessagePack.pack(t)
	local v = msgpack.pack( t )
	local len = string.len( v )
	local t = {}
	local bytes = CreateFromClass(MakeArrayClass(Byte),len)
	for i=1,len do
		bytes[i-1] = string.byte(v,i)
	end
	return bytes
end

--默认是double，但是解析float总是出错，暂时这么处理
function g_MessagePack.pack_use_float(t)
	msgpack.set_number'float'
	local ret = g_MessagePack.pack(t)
	msgpack.set_number'double'
	return ret
end
function g_MessagePack.unpack_use_float(t)
	msgpack.set_number'float'
	local ret = g_MessagePack.unpack(t)
	msgpack.set_number'double'
	return ret
end

local _empty = nil
function g_MessagePack.empty()
	if not _empty then
		_empty = g_MessagePack.pack({})
	end
	return _empty
end
