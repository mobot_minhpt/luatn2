-- Auto Generated!!
local CThumbnailExpressionItem = import "L10.UI.CThumbnailExpressionItem"
local DelegateFactory = import "DelegateFactory"
local UILongPressButton = import "L10.UI.UILongPressButton"
CThumbnailExpressionItem.m_Init_CS2LuaHook = function (this, info, isInNeed) 
    this.expressionInfo = info
    this.icon:LoadMaterial(info:GetIcon())
    this.nameLabel.text = info:GetName()
    this.inNeed:SetActive(isInNeed)
    local isLock = info:GetIsLock()
    this.disableBg:SetActive(isLock)
    this.accessLabel:SetActive(isLock)
    if not this.transform then return end
    local validityPeriodLabel = this.transform:Find("ValidityPeriodLabel"):GetComponent(typeof(UILabel))
    local data = info:WhichToShow()
    local text, isShowLabel = LuaExpressionMgr:GetValidityPeriodStr(data)
    validityPeriodLabel.text = text
    validityPeriodLabel.gameObject:SetActive(isShowLabel)

    CommonDefs.GetComponent_Component_Type(this, typeof(UILongPressButton)).OnLongPressDelegate = DelegateFactory.Action(function () 
        if this.OnItemLongPressed ~= nil then
            invoke(this.OnItemLongPressed)
        end
    end)
    this:RefreshAlert()
end
CThumbnailExpressionItem.m_IsOpenUmbrella_CS2LuaHook = function (this) 
    if this.expressionInfo ~= nil then
        return this.expressionInfo:IsOpenUmbrella()
    end
    return false
end
CThumbnailExpressionItem.m_UpdateExpression_CS2LuaHook = function (this, expressionId) 

    this.icon:LoadMaterial(this.expressionInfo:GetIcon())
    this.nameLabel.text = this.expressionInfo:GetName()
    this:RefreshAlert()

    -- 刷新是否解锁
    local isLock = this.expressionInfo:GetIsLock()
    this.disableBg:SetActive(isLock)
    this.accessLabel:SetActive(isLock)
end
