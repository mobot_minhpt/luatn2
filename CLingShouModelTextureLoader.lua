-- Auto Generated!!
local CClientLingShou = import "L10.Game.CClientLingShou"
local CClientNpc = import "L10.Game.CClientNpc"
local CLingShouBaseMgr = import "L10.Game.CLingShouBaseMgr"
local CLingShouModelTextureLoader = import "L10.UI.CLingShouModelTextureLoader"
local CLogMgr = import "L10.CLogMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CUIManager = import "L10.UI.CUIManager"
local LocalString = import "LocalString"
local Pick_Pick = import "L10.Game.Pick_Pick"

CLingShouModelTextureLoader.m_Init_UInt32_UInt32_Int32_CS2LuaHook = function (this, templateId, piXiangTemplateId, evolveGrade) 
    local Name
    local resName
    local resId
    local scale
    local FX
    local fxColor
    local fxScale = 1
    local yirongFx
    if evolveGrade <= 0 then
        CLogMgr.Log(LocalString.GetString("evolveGrade 不对：") .. evolveGrade)
        evolveGrade = 1
    end
    local default
    default, Name, resName, resId, scale, FX, fxColor, fxScale, yirongFx = CLingShouBaseMgr.GetLingShouModel(templateId, piXiangTemplateId, evolveGrade, true)
    if default then
        this.texture.mainTexture = this:CreateTexture(Name, resName, resId, scale, FX, fxColor, fxScale * 0.6, yirongFx)
    end
end
CLingShouModelTextureLoader.m_Init_LingShouDetails_CS2LuaHook = function (this, details) 
    if details == nil then
        return
    end
    local marryInfo = details.data.Props.MarryInfo
    this.m_IsInDongFang = marryInfo.IsInDongfang > 0
    this.m_IsLeave = (marryInfo.LeaveStartTime + marryInfo.LeaveDuration) > CServerTimeMgr.Inst.timeStamp

    if this.m_IsInDongFang or this.m_IsLeave then
        this.texture.mainTexture = CUIManager.CreateModelTexture(System.String.Format("__{0}__", this:GetInstanceID()), this, - 145, 0.05, -1, 4.66, false, true, 1)
        return
    end


    if details.data.Props.TemplateIdForSkin > 0 then
        this:Init(details.data.TemplateId, details.data.Props.TemplateIdForSkin, details.data.Props.EvolveGradeForSkin)
    else
        this:Init(details.data.TemplateId, 0, details.data.Props.EvolveGrade)
    end
end
CLingShouModelTextureLoader.m_Init_CLingShouProp_UInt32_CS2LuaHook = function (this, prop, templateId) 
    --if (prop.TemplateIdForSkin!=0&&prop.EvolveGradeForSkin!=0)
    --    Init(prop.TemplateIdForSkin, prop.EvolveGradeForSkin);
    --else
    --    Init(templateId, prop.EvolveGrade);
    --if (prop.TemplateIdForSkin!=0&&prop.EvolveGradeForSkin!=0)
    --    Init(prop.TemplateIdForSkin, prop.EvolveGradeForSkin);
    --else
    --    Init(templateId, prop.EvolveGrade);
    if prop.TemplateIdForSkin > 0 then
        this:Init(templateId, prop.TemplateIdForSkin, prop.EvolveGradeForSkin)
    else
        this:Init(templateId, 0, prop.EvolveGrade)
    end
end
CLingShouModelTextureLoader.m_CreateTexture_CS2LuaHook = function (this, Name, resName, resId, scale, FX, fxColor, fxScale, yirongFx) 
    this.Name = this.name
    this.resName = resName
    this.resId = resId
    this.scale = scale
    this.FX = FX
    this.fxColor = fxColor
    this.fxScale = fxScale
    this.yirongFx = yirongFx
    return CUIManager.CreateModelTexture(System.String.Format("__{0}__", this:GetInstanceID()), this, - 145, 0.05, -1, 4.66, false, true, 1)
end
CLingShouModelTextureLoader.m_Load_CS2LuaHook = function (this, renderObj) 
    if this.m_IsInDongFang then
        --jyshanchahua_02
        CClientNpc.LoadResource(renderObj, 20002673, false)
        renderObj.Scale = CLingShouModelTextureLoader.Scale1
        renderObj:DoAni("shake", true, 0, 1, 0, true, 1)
    elseif this.m_IsLeave then
        --lnpc157
        local pick = Pick_Pick.GetData(37000054)
        if pick ~= nil then
            renderObj:LoadMain(pick.PrefabPath, nil, false, false)
            renderObj.Scale = CLingShouModelTextureLoader.Scale2
        end
    else
        CClientLingShou.LoadResource(renderObj, this.Name, this.resName, this.resId, this.scale, this.FX, this.fxColor, this.fxScale, true, this.yirongFx)
    end
end
