local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local Transform = import "UnityEngine.Transform"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local QnTableView = import "L10.UI.QnTableView"
local CUITexture = import "L10.UI.CUITexture"
local CUICommonDef = import "L10.UI.CUICommonDef"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"

LuaShuiManJinShanResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaShuiManJinShanResultWnd, "Anchor", "Anchor", GameObject)
RegistChildComponent(LuaShuiManJinShanResultWnd, "Force1Bg", "Force1Bg", GameObject)
RegistChildComponent(LuaShuiManJinShanResultWnd, "Force2Bg", "Force2Bg", GameObject)
RegistChildComponent(LuaShuiManJinShanResultWnd, "ForceName", "ForceName", UILabel)
RegistChildComponent(LuaShuiManJinShanResultWnd, "RewardLab", "RewardLab", UILabel)
RegistChildComponent(LuaShuiManJinShanResultWnd, "RewardTable", "RewardTable", QnTableView)
RegistChildComponent(LuaShuiManJinShanResultWnd, "UnlockSkillLab", "UnlockSkillLab", UILabel)
RegistChildComponent(LuaShuiManJinShanResultWnd, "Icon", "Icon", CUITexture)
RegistChildComponent(LuaShuiManJinShanResultWnd, "SkillName", "SkillName", UILabel)
RegistChildComponent(LuaShuiManJinShanResultWnd, "SkillDesc", "SkillDesc", UILabel)
RegistChildComponent(LuaShuiManJinShanResultWnd, "AllUnlockTipLab", "AllUnlockTipLab", GameObject)
RegistChildComponent(LuaShuiManJinShanResultWnd, "UnlockSkill", "UnlockSkill", GameObject)
RegistChildComponent(LuaShuiManJinShanResultWnd, "Skill", "Skill", GameObject)
RegistChildComponent(LuaShuiManJinShanResultWnd, "ForceName2", "ForceName2", UILabel)
RegistChildComponent(LuaShuiManJinShanResultWnd, "RewardTable2", "RewardTable2", QnTableView)
RegistChildComponent(LuaShuiManJinShanResultWnd, "UnlockSkill2", "UnlockSkill2", GameObject)
RegistChildComponent(LuaShuiManJinShanResultWnd, "AllUnlockTipLab2", "AllUnlockTipLab2", GameObject)
RegistChildComponent(LuaShuiManJinShanResultWnd, "Skill2", "Skill2", GameObject)
RegistChildComponent(LuaShuiManJinShanResultWnd, "SkillName2", "SkillName2", UILabel)
RegistChildComponent(LuaShuiManJinShanResultWnd, "SkillDesc2", "SkillDesc2", UILabel)
RegistChildComponent(LuaShuiManJinShanResultWnd, "Icon2", "Icon2", CUITexture)
RegistChildComponent(LuaShuiManJinShanResultWnd, "Anchor2", "Anchor2", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaShuiManJinShanResultWnd, "m_CurForce")
RegistClassMember(LuaShuiManJinShanResultWnd, "m_RewardItems")
RegistClassMember(LuaShuiManJinShanResultWnd, "m_CurUnlockIdx")
RegistClassMember(LuaShuiManJinShanResultWnd, "m_BuffList")

function LuaShuiManJinShanResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.m_CurForce = LuaShuiManJinShanMgr.m_CurForce or 1001
    self.m_RewardItems = LuaShuiManJinShanMgr.m_RewardItems
    self.m_CurUnlockIdx = LuaShuiManJinShanMgr.m_CurUnlockIdx

    local curTable = self.m_CurForce == 1001 and self.RewardTable or self.RewardTable2

    curTable.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_RewardItems
        end,
        function(item, index)
            self:InitItem(item, self.m_RewardItems[index + 1])
        end
    )

    curTable.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        local info = self.m_RewardItems[row + 1]
        CItemInfoMgr.ShowLinkItemTemplateInfo(info[1], false, nil, AlignType.Default, 0, 0, 0, 0)
    end)

    self:SwitchForce(self.m_CurForce)
end

function LuaShuiManJinShanResultWnd:Init()
    local curUnlockSkill = self.m_CurForce == 1001 and self.UnlockSkill or self.UnlockSkill2
    local curAllUnlockTipLab = self.m_CurForce == 1001 and self.AllUnlockTipLab or self.AllUnlockTipLab2
    local curIcon = self.m_CurForce == 1001 and self.Icon or self.Icon2
    local curSkillName = self.m_CurForce == 1001 and self.SkillName or self.SkillName2
    local curSkillDesc = self.m_CurForce == 1001 and self.SkillDesc or self.SkillDesc2
    local curRewardTable = self.m_CurForce == 1001 and self.RewardTable or self.RewardTable2
    local curSkill = self.m_CurForce == 1001 and self.Skill or self.Skill2

    local isAllUnlock = self.m_CurUnlockIdx and self.m_CurUnlockIdx ~= 0 
    curUnlockSkill:SetActive(isAllUnlock)
    curAllUnlockTipLab.gameObject:SetActive(not isAllUnlock)

    if isAllUnlock then
        local buffData = Buff_Buff.GetData(self.m_BuffList[self.m_CurUnlockIdx])
        if buffData then
            curIcon:LoadMaterial(buffData.Icon)
            curSkillName.text = buffData.Name
            curSkillDesc.text = CUICommonDef.TranslateToNGUIText(buffData.Display)
        end
    else
        for i = 0, curSkill.transform.childCount - 1 do
            local skill = curSkill.transform:GetChild(i)
            local icon = skill:Find("Icon"):GetComponent(typeof(CUITexture))
            local buffData = Buff_Buff.GetData(self.m_BuffList[i+1])
            if buffData then
                icon:LoadMaterial(buffData.Icon)
            end
        end
    end
    curRewardTable:ReloadData(true, false)
end

function LuaShuiManJinShanResultWnd:OnDestroy()
    LuaShuiManJinShanMgr:Clear()
end

--@region UIEvent

--@endregion UIEvent

function LuaShuiManJinShanResultWnd:InitItem(item, info)
    local icon      = item.transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local border    = item.transform:Find("Border"):GetComponent(typeof(UISprite))
    local numLab    = item.transform:Find("Num"):GetComponent(typeof(UILabel))

    local itemId = info[1]
    local num = info[2]
    local itemData = Item_Item.GetData(itemId)

    icon:LoadMaterial(itemData.Icon)
    border.spriteName = CUICommonDef.GetItemCellBorder(itemData.NameColor)
    numLab.text = num
end

function LuaShuiManJinShanResultWnd:SwitchForce(force)
    local data = XinBaiLianDong_ShuiManJinShan.GetData(force)
    if data then
        self.m_BuffList = {}
        for i = 0, data.BuffList.Length - 1 do
            table.insert(self.m_BuffList, data.BuffList[i])
        end
    end
    self.Force1Bg:SetActive(force == 1001)
    self.Force2Bg:SetActive(force == 1002)
    self.Anchor:SetActive(force == 1001)
    self.Anchor2:SetActive(force == 1002)
end
