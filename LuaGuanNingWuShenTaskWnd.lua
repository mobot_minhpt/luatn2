require("common/common_include")
local Gac2Gas=import "L10.Game.Gac2Gas"
local UILabel=import "UILabel"
local UIGrid=import "UIGrid"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local CChatLinkMgr=import "CChatLinkMgr"
local QnSelectableButton=import "L10.UI.QnSelectableButton"
local MessageMgr=import "L10.Game.MessageMgr"

--武神任务
CLuaGuanNingWuShenTaskWnd=class()
RegistClassMember(CLuaGuanNingWuShenTaskWnd,"m_RemainTimeLabel")
RegistClassMember(CLuaGuanNingWuShenTaskWnd,"m_ItemTemplate")
RegistClassMember(CLuaGuanNingWuShenTaskWnd,"m_Grid")
RegistClassMember(CLuaGuanNingWuShenTaskWnd,"m_AcceptButton")
RegistClassMember(CLuaGuanNingWuShenTaskWnd,"m_TaskId")
RegistClassMember(CLuaGuanNingWuShenTaskWnd,"m_Items")

function CLuaGuanNingWuShenTaskWnd:Init()
    self.m_Items={}
    self.m_ItemTemplate=FindChild(self.transform,"ItemTemplate").gameObject
    self.m_ItemTemplate:SetActive(false)
    self.m_Grid=FindChild(self.transform,"Grid"):GetComponent(typeof(UIGrid))
    Gac2Gas.QueryGuanNingChallengeTaskChoice()

    self.m_AcceptButton=FindChild(self.transform,"AcceptButton").gameObject

    self.m_TaskId=0
    CommonDefs.AddOnClickListener(self.m_AcceptButton,DelegateFactory.Action_GameObject(function(go)
        if self.m_TaskId==0 then
            --提示“请先选择一个任务哦”
            MessageMgr.Inst:ShowMessage("CUSTOM_STRING2", LocalString.GetString("请先选择一个任务哦"))
        else
            Gac2Gas.SendGuanNingChallengeTaskChoice(self.m_TaskId)
            CUIManager.CloseUI(CUIResources.GuanNingWuShenTaskWnd)
        end
    end),false)
end
function CLuaGuanNingWuShenTaskWnd:OnEnable()
    g_ScriptEvent:AddListener("QueryGuanNingChallengeTaskChoiceResult", self, "OnQueryGuanNingChallengeTaskChoiceResult")

end
function CLuaGuanNingWuShenTaskWnd:OnDisable()
    g_ScriptEvent:RemoveListener("QueryGuanNingChallengeTaskChoiceResult", self, "OnQueryGuanNingChallengeTaskChoiceResult")

end

function CLuaGuanNingWuShenTaskWnd:OnQueryGuanNingChallengeTaskChoiceResult(info)
    for i=1,#info do
        local go=NGUITools.AddChild(self.m_Grid.gameObject,self.m_ItemTemplate)
        go:SetActive(true)
        local item=info[i]
        self:InitItem(go.transform,item.taskId,item.taskType,item.needValue,item.awardScore)
        local cmp=go:GetComponent(typeof(QnSelectableButton))
        table.insert( self.m_Items,cmp )

        cmp.OnClick=DelegateFactory.Action_QnButton(function(qnButton)
            self.m_TaskId=item.taskId
            for i,v in ipairs(self.m_Items) do
                if v~=qnButton then
                    v:SetSelected(false,false)
                end
            end
        end)
    end
    self.m_Grid:Reposition()
end

function CLuaGuanNingWuShenTaskWnd:InitItem(tf,taskId,taskType,needValue,awardScore)
    local LabelType=typeof(UILabel)
    local nameLabel=FindChild(tf,"NameLabel"):GetComponent(LabelType)
    local descLabel=FindChild(tf,"DescLabel"):GetComponent(LabelType)
    local typeLabel=FindChild(tf,"TypeLabel"):GetComponent(LabelType)
    local scoreLabel=FindChild(tf,"ScoreLabel"):GetComponent(LabelType)

    local lv=CClientMainPlayer.Inst and CClientMainPlayer.Inst.Level
    local designData=GuanNing_ChallengeTask.GetData(taskId)
    if designData then
        nameLabel.text= designData.TaskName
        descLabel.text=CChatLinkMgr.TranslateToNGUIText(SafeStringFormat3(designData.TaskDesc,CLuaGuanNingWuShenMgr.FormatNum(needValue)),false)
        scoreLabel.text=tostring(awardScore)
    else
        nameLabel.text=nil
        descLabel.text=nil
        scoreLabel.text=nil
    end

    local c=nil
    if taskType==1 then
        typeLabel.text=LocalString.GetString("普通")
        --改颜色
        c=Color(0.275,0.44,0.86)
    elseif taskType==2 then
        typeLabel.text=LocalString.GetString("职业")
        c=Color(0.94,0.3,0.32)
    elseif taskType==3 then
        typeLabel.text=LocalString.GetString("挑战")
        c=Color(0.95,0.35,0.95)
    end
    if c then
        typeLabel.color=c
        nameLabel.color=c
    end
end
return CLuaGuanNingWuShenTaskWnd
