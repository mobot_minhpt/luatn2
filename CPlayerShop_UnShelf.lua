-- Auto Generated!!
local CCJBMgr = import "L10.Game.CCJBMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CEquipment = import "L10.Game.CEquipment"
local CItem = import "L10.Game.CItem"
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CPlayerShop_UnShelf = import "L10.UI.CPlayerShop_UnShelf"
local CPlayerShopItemData = import "L10.UI.CPlayerShopItemData"
local CPlayerShopMgr = import "L10.UI.CPlayerShopMgr"
local EnumEventType = import "EnumEventType"
local EnumItemFlag = import "L10.Game.EnumItemFlag"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EventManager = import "EventManager"
local LocalString = import "LocalString"
local OnShelfStatus = import "L10.UI.OnShelfStatus"
local QnButton = import "L10.UI.QnButton"
local String = import "System.String"
local StringActionKeyValuePair = import "L10.Game.StringActionKeyValuePair"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local ETickType = import "L10.Engine.ETickType"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
CPlayerShop_UnShelf.m_Init_CS2LuaHook = function (this) 
    this.m_IsReshelf = false
    this.m_CachedData = CPlayerShopMgr.m_ShelfContext
    this.m_SearchResultTable.m_DataSource = this
    if this.m_CachedData ~= nil then
        local item = this.m_CachedData.Item
        if item ~= nil then
            if item.IsPrecious then
                this.m_ReShelfButton.Visible = (this.m_CachedData.Status == OnShelfStatus.OffShelf)
            else
                this.m_ReShelfButton.Visible = true
            end
            this.m_SingleItemPriceLabel.text = tostring(this.m_CachedData.Price)
            this.m_TotalPriceLabel.text = tostring((this.m_CachedData.Count * this.m_CachedData.Price))

            this.m_NameLabel.text = item.Name
            this.m_LevelLabel.text = System.String.Format(LocalString.GetString("[ff0000]{0}[-]级"), item.Grade)
            if item.IsItem then
                this.m_NameLabel.color = CItem.GetColor(item.TemplateId)
                this.m_ItemInfoLabel:SetContent(item.Item.Description)
            else
                this.m_NameLabel.color = CEquipment.GetColor(item.TemplateId)
                this.m_ItemInfoLabel:SetContent(item.Equip:GetDescription())
            end
            this.m_ItemTexture:LoadMaterial(item.Icon)
            this.m_TaxInput.text = "0"

            this.m_SearchResultTable.gameObject:SetActive(true)
            --如果是盗版装备,则只传盗版装备Id
            if item.IsEquip and item.Equip:FlagIsSet(EnumItemFlag.ISFAKE) then
                this:SearchOtherSells(CPlayerShopMgr.Inst:ToAliasTemplateId(item.TemplateId))
            else
                this:SearchOtherSells(item.TemplateId)
            end
            if this.m_OffShelfTimeLabel ~= nil then
                if this.m_CachedData.Status == OnShelfStatus.OffShelf then
                    this.m_OffShelfTimeLabel.gameObject:SetActive(false)
                else
                    this.m_OffShelfTimeLabel.gameObject:SetActive(true)
                    local interval = CServerTimeMgr.Inst.timeStamp - this.m_CachedData.TimeStamp
                    this.m_LeftTime = this.m_CachedData.LeftTime >= math.floor(interval) and this.m_CachedData.LeftTime-math.floor(interval) or 0
                    local hour = math.floor(this.m_LeftTime / 3600)
                    local min= math.floor((this.m_LeftTime - hour*3600) / 60)
                    local sec = this.m_LeftTime - hour*3600 - min*60
                    this.m_OffShelfTimeLabel.text = SafeStringFormat3(LocalString.GetString("%02d:%02d:%02d后自动下架"),hour,min,sec)
                    if not this.m_LeftTimeTick then
                        this.m_LeftTimeTick = RegisterTick(function()
                            this.m_LeftTime = this.m_LeftTime > 0 and this.m_LeftTime - 1 or 0
                            local h = math.floor(this.m_LeftTime / 3600)
                            local m= math.floor((this.m_LeftTime - h*3600) / 60)
                            local s = this.m_LeftTime - h*3600 - m*60
                            this.m_OffShelfTimeLabel.text = SafeStringFormat3(LocalString.GetString("%02d:%02d:%02d后自动下架"),h,m,s)
                            if this.m_LeftTime == 0 then
                                UnRegisterTick(this.m_LeftTimeTick)
                                this.m_LeftTimeTick = nil
                            end
                        end,1000,ETickType.Loop)
                    end
                end
            end
        end
    end
end
CPlayerShop_UnShelf.m_OnEnable_CS2LuaHook = function (this) 
    EventManager.AddListenerInternal(EnumEventType.PlayerShopOffShelfSuccess, MakeDelegateFromCSFunction(this.OnUnShelfSuccess, MakeGenericClass(Action1, UInt32), this))
    EventManager.AddListenerInternal(EnumEventType.PlayerShopOnShelfSuccess, MakeDelegateFromCSFunction(this.OnPlayerShopOnShelfSuccess, MakeGenericClass(Action3, String, UInt32, UInt32), this))
    EventManager.AddListenerInternal(EnumEventType.QueryPlayerShopSearchedItemsResult, MakeDelegateFromCSFunction(this.OnRecieveSearchResult, MakeGenericClass(Action4, MakeGenericClass(List, CPlayerShopItemData), UInt32, UInt32, UInt32), this))
    UIEventListener.Get(this.m_CloseButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_CloseButton).onClick, MakeDelegateFromCSFunction(this.OnClose, VoidDelegate, this), true)
    UIEventListener.Get(this.m_ItemTexture.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_ItemTexture.gameObject).onClick, MakeDelegateFromCSFunction(this.OnClickItemTexture, VoidDelegate, this), true)
    this.m_SubmitButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_SubmitButton.OnClick, MakeDelegateFromCSFunction(this.OnSubmit, MakeGenericClass(Action1, QnButton), this), true)
    this.m_ReShelfButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_ReShelfButton.OnClick, MakeDelegateFromCSFunction(this.OnReShelf, MakeGenericClass(Action1, QnButton), this), true)
end
CPlayerShop_UnShelf.m_OnDisable_CS2LuaHook = function (this) 
    EventManager.RemoveListenerInternal(EnumEventType.PlayerShopOffShelfSuccess, MakeDelegateFromCSFunction(this.OnUnShelfSuccess, MakeGenericClass(Action1, UInt32), this))
    EventManager.RemoveListenerInternal(EnumEventType.PlayerShopOnShelfSuccess, MakeDelegateFromCSFunction(this.OnPlayerShopOnShelfSuccess, MakeGenericClass(Action3, String, UInt32, UInt32), this))
    EventManager.RemoveListenerInternal(EnumEventType.QueryPlayerShopSearchedItemsResult, MakeDelegateFromCSFunction(this.OnRecieveSearchResult, MakeGenericClass(Action4, MakeGenericClass(List, CPlayerShopItemData), UInt32, UInt32, UInt32), this))
    UIEventListener.Get(this.m_CloseButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_CloseButton).onClick, MakeDelegateFromCSFunction(this.OnClose, VoidDelegate, this), false)
    UIEventListener.Get(this.m_ItemTexture.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_ItemTexture.gameObject).onClick, MakeDelegateFromCSFunction(this.OnClickItemTexture, VoidDelegate, this), false)
    this.m_SubmitButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_SubmitButton.OnClick, MakeDelegateFromCSFunction(this.OnSubmit, MakeGenericClass(Action1, QnButton), this), false)
    this.m_ReShelfButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_ReShelfButton.OnClick, MakeDelegateFromCSFunction(this.OnReShelf, MakeGenericClass(Action1, QnButton), this), false)
    if this.m_LeftTimeTick then
        UnRegisterTick(this.m_LeftTimeTick)
        this.m_LeftTimeTick = nil
    end
end
CPlayerShop_UnShelf.m_OnUnShelfSuccess_CS2LuaHook = function (this, counterPos) 
    if this.m_IsReshelf then
        this.m_IsReshelf = false
        --更新下在包裹中的位置
        CPlayerShopMgr.CloseUnShelfWnd()
        local packagePlace = CClientMainPlayer.Inst.ItemProp:GetItemPos(EnumItemPlace.Bag, this.m_CachedData.Item.Id)
        if packagePlace ~= 0 then
            this.m_CachedData.PackagePlace = packagePlace
            CPlayerShopMgr.ShowShelfWnd(this.m_CachedData)
        else
            g_MessageMgr:ShowMessage("PLAYERSHOP_RESHELF_FAILED")
        end
    else
        CPlayerShopMgr.CloseUnShelfWnd()
    end
end
CPlayerShop_UnShelf.m_OnReview_CS2LuaHook = function (this) 
    if this.m_CachedData ~= nil then
        local item = this.m_CachedData.Item

        if item ~= nil and item.IsItem then
            if item.Item.Type == EnumItemType_lua.UnEvaluateChuanjiabao or item.Item.Type == EnumItemType_lua.Chuanjiabao then
                CCJBMgr.Inst:OpenTipShow(item.Id, false)
            end
        end
    end
end
CPlayerShop_UnShelf.m_GetActionPairs_CS2LuaHook = function (this, itemId, templateId) 
    local actionPairs = CreateFromClass(MakeGenericClass(List, StringActionKeyValuePair))
    local reviewAction = CreateFromClass(StringActionKeyValuePair, LocalString.GetString("预览"), MakeDelegateFromCSFunction(this.OnReview, Action0, this))

    local item = CItemMgr.Inst:GetById(itemId)
    if item == nil then
        local _item = CPlayerShopMgr.Inst:GetItemById(itemId)
        if _item ~= nil then
            item = _item.Item
        end
    end

    if item ~= nil and item.IsItem then
        if item.Item.Type == EnumItemType_lua.UnEvaluateChuanjiabao or item.Item.Type == EnumItemType_lua.Chuanjiabao then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), reviewAction)
        end
    end

    return actionPairs
end
CPlayerShop_UnShelf.m_SearchOtherSells_CS2LuaHook = function (this, templateId)    --清空数据再查询
    CommonDefs.ListClear(this.m_SearchResultData)
    this.m_SearchResultTable:ReloadData(false, false)
    if this.m_CachedData.Item.IsPrecious then
        Gac2Gas.QueryPlayerShopSearchedItems(templateId, SearchOption_lua.Valueable, 1, 15, false, 0)
    else
        Gac2Gas.QueryPlayerShopSearchedItems(templateId, SearchOption_lua.All, 1, 15, false, 0)
    end
end
CPlayerShop_UnShelf.m_OnRecieveSearchResult_CS2LuaHook = function (this, data, totalCount, indexStart, indexEnd)
    CommonDefs.ListClear(this.m_SearchResultData)
    --去掉自己当前上架的物品
    do
        local i = 0
        while i < data.Count do
            if data[i].OwnerId ~= CClientMainPlayer.Inst.Id then
                CommonDefs.ListAdd(this.m_SearchResultData, typeof(CPlayerShopItemData), data[i])
                if this.m_SearchResultData.Count >= 4 then
                    break
                end
            end
            i = i + 1
        end
    end
    this.m_SearchResultTable:ReloadData(true, false)
    this.m_NoShopItemLabel:SetActive(this.m_SearchResultData.Count == 0)
end
