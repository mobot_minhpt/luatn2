require("common/common_include")
require("ui/biwudahui/LuaBWDHGameVideoMgr")
local LuaGameObject=import "LuaGameObject"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local Profession=import "L10.Game.Profession"
local EnumClass=import "L10.Game.EnumClass"
local Enum=import "System.Enum"

CLuaBWDHGameVideoMemberWnd=class()
RegistClassMember(CLuaBWDHGameVideoMemberWnd,"m_TableViewDataSource")
RegistClassMember(CLuaBWDHGameVideoMemberWnd,"m_TableView")
RegistClassMember(CLuaBWDHGameVideoMemberWnd,"m_MemberInfos")
RegistClassMember(CLuaBWDHGameVideoMemberWnd,"m_LeaderId")


function CLuaBWDHGameVideoMemberWnd:Init()
    local g = LuaGameObject.GetChildNoGC(self.transform,"CloseButton").gameObject
    UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.CloseUI(CUIResources.BWDHGameVideoMemberWnd)
    end)

    local function InitItem(item,index)
        if index % 2 == 1 then
            item:SetBackgroundTexture(Constants.NewEvenBgSprite)
        else
            item:SetBackgroundTexture(Constants.NewOddBgSprite)
        end
        self:InitItem(item,index)
    end
    self:InitMemberInfos()
    self.m_TableView=LuaGameObject.GetLuaGameObjectNoGC(self.transform).tableView
    self.m_TableViewDataSource=DefaultTableViewDataSource.CreateByCount(#self.m_MemberInfos,InitItem)
    self.m_TableView.m_DataSource=self.m_TableViewDataSource
    -- self.m_TableView.OnSelectAtRow=DelegateFactory.Action_int(function (row)
    --     print("select ",row)
    -- end)
    self.m_TableView:ReloadData(true,false)

end
function CLuaBWDHGameVideoMemberWnd:InitMemberInfos()
    self.m_MemberInfos={}
    self.m_LeaderId=0
    if CLuaBWDHGameVideoMgr.m_TeamInfo then
        self.m_LeaderId=CLuaBWDHGameVideoMgr.m_TeamInfo.Leader
        local players= CLuaBWDHGameVideoMgr.m_TeamInfo.players
        if players then
            local len=players.Length
            for i=1,len do
                table.insert( self.m_MemberInfos,players[i-1])
            end
        end
    end
end


function CLuaBWDHGameVideoMemberWnd:InitItem(item,index)
    local tf=item.transform

    local nameLabel=LuaGameObject.GetChildNoGC(tf,"NameLabel").label
    local levelLabel=LuaGameObject.GetChildNoGC(tf,"LevelLabel").label
    local clsSprite=LuaGameObject.GetChildNoGC(tf,"ClsSprite").sprite
    local leaderSprite=LuaGameObject.GetChildNoGC(tf,"LeaderSprite").sprite

    -- if index==0 then
    --     leaderSprite.gameObject:SetActive(true)
    -- else
        leaderSprite.gameObject:SetActive(false)
    -- end

    local data=self.m_MemberInfos[index+1]
    if data then
        nameLabel.text=data.Name
        levelLabel.text="Lv."..tostring(data.Level)
        clsSprite.spriteName = Profession.GetIcon(Enum.Parse(typeof(EnumClass), tostring(data.Class)))
        if data.Id==self.m_LeaderId then
            leaderSprite.gameObject:SetActive(true)
        end
    else
        nameLabel.text=" "
        levelLabel.text=" "
    end
end

return CLuaBWDHGameVideoMemberWnd
