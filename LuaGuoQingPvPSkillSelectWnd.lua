local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local Input = import "UnityEngine.Input"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"
local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local SoundManager = import "SoundManager"
local CUIManager = import "L10.UI.CUIManager"
local CUICommonDef = import "L10.UI.CUICommonDef"
local NGUITools = import "NGUITools"
local LocalString = import "LocalString"
local MsgPackImpl = import "MsgPackImpl"
local CScene = import "L10.Game.CScene"
local Profession = import "L10.Game.Profession"
local EnumClass = import "L10.Game.EnumClass"
local CMiddleNoticeMgr = import "L10.UI.CMiddleNoticeMgr"

LuaGuoQingPvPSkillSelectWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaGuoQingPvPSkillSelectWnd, "ItemPanel", "ItemPanel", GameObject)
RegistChildComponent(LuaGuoQingPvPSkillSelectWnd, "teammate1", "teammate1", GameObject)
RegistChildComponent(LuaGuoQingPvPSkillSelectWnd, "skillicon", "skillicon", GameObject)
RegistChildComponent(LuaGuoQingPvPSkillSelectWnd, "teammate2", "teammate2", GameObject)
RegistChildComponent(LuaGuoQingPvPSkillSelectWnd, "SkillShowItem", "SkillShowItem", GameObject)
RegistChildComponent(LuaGuoQingPvPSkillSelectWnd, "CommonSkillItem", "CommonSkillItem", GameObject)
RegistChildComponent(LuaGuoQingPvPSkillSelectWnd, "SpecialSkillItem", "SpecialSkillItem", GameObject)
RegistChildComponent(LuaGuoQingPvPSkillSelectWnd, "m_FollowFingerIcon", "CloneItem", GameObject)
RegistChildComponent(LuaGuoQingPvPSkillSelectWnd, "RandomButton", "RandomButton", GameObject)
RegistChildComponent(LuaGuoQingPvPSkillSelectWnd, "ConfirmButton", "ConfirmButton", GameObject)
RegistChildComponent(LuaGuoQingPvPSkillSelectWnd, "Skills", "Skills", GameObject)

--@endregion RegistChildComponent end

RegistClassMember(LuaGuoQingPvPSkillSelectWnd,"m_IsDragging")
RegistClassMember(LuaGuoQingPvPSkillSelectWnd,"TeamMatesIcon")
RegistClassMember(LuaGuoQingPvPSkillSelectWnd,"m_SkillButtons")
RegistClassMember(LuaGuoQingPvPSkillSelectWnd,"m_SkillId")
RegistClassMember(LuaGuoQingPvPSkillSelectWnd,"m_SkillIdList")
RegistClassMember(LuaGuoQingPvPSkillSelectWnd,"m_SkillqualityList")
RegistClassMember(LuaGuoQingPvPSkillSelectWnd,"m_SelectedSkill")
RegistClassMember(LuaGuoQingPvPSkillSelectWnd,"m_SelectedSkillObj")
RegistClassMember(LuaGuoQingPvPSkillSelectWnd,"m_SrcSkillIndex")
RegistClassMember(LuaGuoQingPvPSkillSelectWnd,"m_LastDragPos")

RegistClassMember(LuaGuoQingPvPSkillSelectWnd,"m_LastHighlight")
RegistClassMember(LuaGuoQingPvPSkillSelectWnd,"InitSelectInfo")




function LuaGuoQingPvPSkillSelectWnd:Awake()
    self.m_IsDragging = false
    self.m_LastHighlight = nil
    self.m_SkillButtons = {}
    self.m_SkillIdList = {}
    self.m_SkillqualityList = {}
    self.m_SelectedSkill = {nil,nil,nil,nil,nil,nil,nil}
    self.m_SelectedSkillObj = {nil,nil,nil,nil,nil,nil,nil}
    
    self.SkillShowItem:SetActive(false)
    self.teammate1:SetActive(false)
    self.teammate2:SetActive(false)
    self.skillicon:SetActive(false)
    self.InitSelectInfo = false
    UIEventListener.Get(self.RandomButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRandomButtonClick(go)
	end)
    UIEventListener.Get(self.ConfirmButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnConfirmButtonClick(go)
	end)
    self:SkillShow()
    self:TeamMatesInit()
    self.InitSelectInfo = false
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaGuoQingPvPSkillSelectWnd:Init()
    for i = 1,7 do
        local buttonname = "Button"..tostring(i)
        local ibutton = FindChild(self.Skills.transform,buttonname).gameObject
        UIEventListener.Get(ibutton).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnSkillButtonClick(go)
        end)
    end
end
function LuaGuoQingPvPSkillSelectWnd:OnEnable()
    g_ScriptEvent:AddListener("OnJinLuHunYuanZhanTeamChooseResult", self, "TeamMatesUpdate")
    g_ScriptEvent:AddListener("GuoQing2022PvPSelect_Skill_OnDragComplete", self, "ReplaceSkill")
end

function LuaGuoQingPvPSkillSelectWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnJinLuHunYuanZhanTeamChooseResult", self, "TeamMatesUpdate")
    g_ScriptEvent:RemoveListener("GuoQing2022PvPSelect_Skill_OnDragComplete", self, "ReplaceSkill")
end

function LuaGuoQingPvPSkillSelectWnd:Update()
    if CScene.MainScene then
        if CScene.MainScene.ShowTimeCountDown then
            local buttontext = self.ConfirmButton.transform:Find("Label"):GetComponent(typeof(UILabel))
            local remaintime = CScene.MainScene.ShowTime
            buttontext.text = LocalString.GetString("确定(")..tostring(remaintime)..")"
        end
    end
    if nil == self.m_LastDragPos then
        self.m_LastDragPos = Vector3.zero
    end
    if not self.m_IsDragging then
        return
    end
    if Input.GetMouseButton(0) then
        if self.m_LastDragPos ~= Input.mousePosition then
            CSkillInfoMgr.CloseSkillInfoWnd()
        end
        self.m_FollowFingerIcon.transform.position = CUIManager.UIMainCamera:ScreenToWorldPoint(Input.mousePosition)
        self.m_LastDragPos = Input.mousePosition

        if not Input.GetMouseButtonUp(0) then
            return
        end
    end

    self.m_FollowFingerIcon.gameObject:SetActive(false)
    local hoveredObject = CUICommonDef.SelectedUIWithRacast
    if hoveredObject then 
        for i = 1,7 do
            local name = "Button"..tostring(i)
            if hoveredObject.name == name then
                local data = {self.m_SkillId, FindChild(hoveredObject.transform,"Icon").gameObject , self.m_SrcSkillIndex}
                g_ScriptEvent:BroadcastInLua("GuoQing2022PvPSelect_Skill_OnDragComplete",data)
                break

            end
        end
        
    end
    self.m_IsDragging = false
    self.m_SkillId = {0,UInt32.MaxValue}
    self.m_SrcSkillIndex = 1

end

--@region UIEvent
function  LuaGuoQingPvPSkillSelectWnd:OnSkillButtonClick(go)
    for i = 1,7 do
        local buttonname = "Button"..tostring(i)
        local ibutton = FindChild(self.Skills.transform,buttonname).gameObject
        if ibutton == go then
            if self.m_SelectedSkill[i] ~= nil then
                CSkillInfoMgr.ShowSkillInfoWnd(self.m_SelectedSkill[i][2], true, 0, 0, nil)
            end
            break
        end
    end
end

function  LuaGuoQingPvPSkillSelectWnd:SendSelectInfo()
    local info = {}
    for i = 1,7 do
        if self.m_SelectedSkill[i] == nil then
            table.insert(info,0)
            table.insert(info,0)
        else 
            table.insert(info,self.m_SelectedSkill[i][1])
            table.insert(info,self.m_SelectedSkill[i][2])
        end
    end
    Gac2Gas.ChooseJinLuHunYuanZhanSkill(g_MessagePack.pack(info))
end

function LuaGuoQingPvPSkillSelectWnd:OnConfirmButtonClick(go)
    self:SendSelectInfo()
end

function  LuaGuoQingPvPSkillSelectWnd:RandomSelect()
    local commondata = {}
    local specialdata = {}
    local commonskillcnt = 5
    local specialskillcnt = 2
    local cnt = 0
    for profession, skillicon in pairs(LuaGuoQingPvPMgr.ChoosableSkillInfoData) 
    do
        for j = 1,5 do
            cnt = cnt + 1
            local data = {}
            data.button = self.m_SkillButtons[cnt].button
            data.srcSkillId = {profession,self.m_SkillButtons[cnt].iconid}
            table.insert(commondata,data)
        end
        for j = 6,7 do
            cnt = cnt + 1
            local data = {}
            data.button = self.m_SkillButtons[cnt].button
            data.srcSkillId = {profession,self.m_SkillButtons[cnt].iconid}
            table.insert(specialdata,data)
        end
    end
    for i = 1,7 do
        if self.m_SelectedSkillObj[i] ~= nil then
            local oldchosenbg = FindChild(self.m_SelectedSkillObj[i].transform,"ChosenBg").gameObject
            oldchosenbg:SetActive(false)
        end
    end
    local commonskillLength = #commondata
    for i = 1,commonskillcnt do
        local randomdata = math.random(1,commonskillLength)
        local skilbutton = commondata[randomdata].button
        local srcSkillId = commondata[randomdata].srcSkillId
        
        self.m_SelectedSkillObj[i] = skilbutton
        local chosenbg = FindChild(skilbutton.transform,"ChosenBg").gameObject
        chosenbg:SetActive(true)

        local buttonname = "Button"..tostring(i)
        local ibutton = FindChild(self.Skills.transform,buttonname).gameObject
        local buttonicon = FindChild(ibutton.transform,"Icon").gameObject

        local skilldata = Skill_AllSkills.GetData(srcSkillId[2])
        if skilldata ~= nil then
            buttonicon:GetComponent(typeof(CUITexture)):LoadSkillIcon(skilldata.SkillIcon)
        else
            buttonicon:GetComponent(typeof(CUITexture)):Clear()
        end
        

        self.m_SelectedSkill[i] = srcSkillId
        commondata[randomdata] = commondata[commonskillLength]
        commonskillLength = commonskillLength - 1
    end
    
    local specialskillLength = #specialdata
    for i = 1,specialskillcnt do
        local randomdata = math.random(1,specialskillLength)
        local skilbutton = specialdata[randomdata].button
        local srcSkillId = specialdata[randomdata].srcSkillId

        local buttonname = "Button"..tostring(i+5)
        
        local ibutton = FindChild(self.Skills.transform,buttonname).gameObject
        local buttonicon = FindChild(ibutton.transform,"Icon").gameObject

        local skilldata = Skill_AllSkills.GetData(srcSkillId[2])
        if skilldata ~= nil then
            buttonicon:GetComponent(typeof(CUITexture)):LoadSkillIcon(skilldata.SkillIcon)
        else
            buttonicon:GetComponent(typeof(CUITexture)):Clear()
        end


        self.m_SelectedSkillObj[i+5] = skilbutton
        local chosenbg = FindChild(skilbutton.transform,"ChosenBg").gameObject
        chosenbg:SetActive(true)

        self.m_SelectedSkill[i+5] = srcSkillId
        specialdata[randomdata] = specialdata[specialskillLength]
        specialskillLength = specialskillLength - 1
    end

end


function LuaGuoQingPvPSkillSelectWnd:OnRandomButtonClick(go)
    if self.InitSelectInfo == true then --?
        self:RandomSelect()
        self:SendSelectInfo()
    end
end
function LuaGuoQingPvPSkillSelectWnd:SelectInfoUpdate()
 
    local myplayerid = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
    if LuaGuoQingPvPMgr.TeamSkillInfoData ~= nil then
        for i=1, #LuaGuoQingPvPMgr.TeamSkillInfoData, 12 do   
            local playerid = LuaGuoQingPvPMgr.TeamSkillInfoData[i]
            if playerid == myplayerid then 
                for j = 1,7 do  
                    local icon = nil
                    local buttonname = "Button"..tostring(j)
                    
                    local ibutton = FindChild(self.Skills.transform,buttonname).gameObject
                    local buttonicon = FindChild(ibutton.transform,"Icon").gameObject
                    if LuaGuoQingPvPMgr.TeamSkillInfoData[i+4+j] ~= 0 then
                        icon = LuaGuoQingPvPMgr.TeamSkillInfoData[i+4+j]
                        local skillicon = Skill_AllSkills.GetData(icon)

                        if skillicon ~= nil then
                            buttonicon:GetComponent(typeof(CUITexture)):LoadSkillIcon(skillicon.SkillIcon)
                        else
                            print("skillicon is nil!"..tostring(icon))
                        end            
                    else
                        buttonicon:GetComponent(typeof(CUITexture)):Clear()
                    end

                    
                end
                break 
            end

        end 
    end
    self.InitSelectInfo = true
end
function LuaGuoQingPvPSkillSelectWnd:TeamMatesUpdate()
    if self.InitSelectInfo == false then
        self:SelectInfoUpdate()
    end
    local teammate = {self.teammate1,self.teammate2}
    local myplayerid = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0

    local t = 0
    if LuaGuoQingPvPMgr.TeamSkillInfoData ~= nil then
        for i=1, #LuaGuoQingPvPMgr.TeamSkillInfoData, 12 do
            
            local playerid = LuaGuoQingPvPMgr.TeamSkillInfoData[i]
            local name = LuaGuoQingPvPMgr.TeamSkillInfoData[i+1]
            local class = LuaGuoQingPvPMgr.TeamSkillInfoData[i+2]
            local gender = LuaGuoQingPvPMgr.TeamSkillInfoData[i+3]
            local playerheadicon = CUICommonDef.GetPortraitName(class, gender)

            if playerid ~= myplayerid then
                t = t + 1
                teammate[t].gameObject:SetActive(true)
                local Info = teammate[t].transform:Find("Info").gameObject
                local nametextobj = Info.transform:Find("name"):GetComponent(typeof(UILabel))
                nametextobj.text = name     
                for j = 1,7 do  
                    local icon = nil
                    if LuaGuoQingPvPMgr.TeamSkillInfoData[i+4+j] ~= 0 then
                        icon = LuaGuoQingPvPMgr.TeamSkillInfoData[i+4+j]
                        local child = FindChild(self.TeamMatesIcon[t][j].transform,"Icon"):GetComponent(typeof(CUITexture))
                        --print(icon)
                        local skillicon = Skill_AllSkills.GetData(icon)

                        if skillicon ~= nil then
                            child:LoadSkillIcon(skillicon.SkillIcon)
                        else
                            print("skillicon is nil!"..tostring(icon))
                        end            
                    else
                        FindChild(self.TeamMatesIcon[t][j].transform,"Icon"):GetComponent(typeof(CUITexture)):Clear()
                    end

                    
                end 
                local headicon = FindChild(teammate[t].transform:Find("Info").transform,"Icon").gameObject:GetComponent(typeof(CUITexture))

                headicon:LoadNPCPortrait(playerheadicon)

            end

        end 
    end
end

function LuaGuoQingPvPSkillSelectWnd:TeamMatesInit()

    local teammate = {self.teammate1,self.teammate2}
    local t = 0
    self.TeamMatesIcon = { {},{} }
    for i = 1,2 do
        t = t + 1
        local commonskillgrid = teammate[t].transform:Find("CommonSkillGrid").gameObject
        local specialskillgrid = teammate[t].transform:Find("SpecialSkillGrid").gameObject
        for j = 1,5 do  
            local go=NGUITools.AddChild(commonskillgrid,self.skillicon)
            go:SetActive(true)
            local icon = nil
            local try = go.transform:Find("Icon")
            FindChild(go.transform,"Icon"):GetComponent(typeof(CUITexture)):Clear()
            table.insert(self.TeamMatesIcon[t], go)
            
        end 
        commonskillgrid:GetComponent(typeof(UIGrid)):Reposition()
        for j = 5,6 do
            local go=NGUITools.AddChild(specialskillgrid,self.skillicon)
            go:SetActive(true)
            local icon = nil
            FindChild(go.transform,"Icon"):GetComponent(typeof(CUITexture)):Clear()
            table.insert(self.TeamMatesIcon[t], go)
        end
        specialskillgrid:GetComponent(typeof(UIGrid)):Reposition()
    end
    

end

function LuaGuoQingPvPSkillSelectWnd:InitSkillButton(go,iconid)
    
    local t = {}
    t.button = go
    t.iconid = iconid
    t.activeSkillIcon = go.transform:Find("SkillIcon"):GetComponent(typeof(CUITexture))
    UIEventListener.Get(go.gameObject).onDragStart = DelegateFactory.VoidDelegate(function (x)
        self:OnDragIconStart(x)
    end)
    UIEventListener.Get(go.gameObject).onClick = DelegateFactory.VoidDelegate(function (x)
        self:OnSkillIconClick(x)
    end)

    t.activeSkillColorIcon = go.transform:Find("SkillIcon"):GetComponent(typeof(UISprite))
    local skillicon = Skill_AllSkills.GetData(iconid)
    if skillicon ~= nil then
        t.activeSkillIcon:LoadSkillIcon(skillicon.SkillIcon)
    else
        print("skillicon is nil!"..tostring(iconid))
    end
    
    local CD = go.transform:Find("CD").gameObject
    local CDLabel = CD.transform:Find("CDLabel"):GetComponent(typeof(UILabel))
    if skillicon ~= nil then
        local cdnumber = math.floor(skillicon.CD)
        CDLabel.text = SafeStringFormat3(LocalString.GetString("%d秒"),cdnumber)
    else
        CDLabel.text =  ""
    end
    return t
end

function LuaGuoQingPvPSkillSelectWnd:F_CD(skillid)
    local skillobj = Skill_AllSkills.GetData(skillid)
    if skillobj ~= nil then
        return math.floor(skillobj.CD)
    else
        return 1000 --极大数,排序用
    end

end


function LuaGuoQingPvPSkillSelectWnd:SkillShow()
    for profession, skillicon in pairs(LuaGuoQingPvPMgr.ChoosableSkillInfoData ) 
    do
        local CommonSkillData = {}
        for i = 1,5 do
            local now = {}
            now.iconid = skillicon[i]
            now.CD = self:F_CD(skillicon[i])
            table.insert(CommonSkillData,now)
        end
        table.sort(CommonSkillData, function(a, b)
            return a.CD < b.CD
        end)
        
        local SpecialSkillData = {}
        for i = 6,7 do
            local now = {}
            now.iconid = skillicon[i]
            now.CD = self:F_CD(skillicon[i])
            table.insert(SpecialSkillData,now)
        end
        table.sort(SpecialSkillData, function(a, b)
            return a.CD < b.CD
        end)

        local instance = NGUITools.AddChild(self.ItemPanel,self.SkillShowItem)
        instance:SetActive(true)
        local instanceTexture = instance.transform:Find("Texture").gameObject
        local instancetest = instanceTexture.transform:Find("text"):GetComponent(typeof(UILabel))
        
        instancetest.text = Profession.GetFullName(CommonDefs.ConvertIntToEnum(typeof(EnumClass),profession))


        local CommonSkill = instance.transform:Find("CommonSkill").gameObject
        for j = 1,5 do
            local icon = CommonSkillData[j].iconid --skillicon[j] 
            local go=NGUITools.AddChild(CommonSkill,self.CommonSkillItem)
            go:SetActive(true)
            local t = self:InitSkillButton(go,icon)
            table.insert(self.m_SkillButtons, t)
            table.insert(self.m_SkillIdList, {profession,icon})
            table.insert(self.m_SkillqualityList, 0)
            
            
        end

        local SpecialSkill = instance.transform:Find("SpecialSkill").gameObject
        for j = 6,7 do
            local icon = SpecialSkillData[j-5].iconid --skillicon[j] 
            local go=NGUITools.AddChild(SpecialSkill,self.SpecialSkillItem)
            go:SetActive(true)
            local t = self:InitSkillButton(go,icon)
            table.insert(self.m_SkillButtons, t)
            table.insert(self.m_SkillIdList, {profession,icon})
            table.insert(self.m_SkillqualityList,1)
        end
    end
    self.ItemPanel:GetComponent(typeof(UIGrid)):Reposition()
end

function LuaGuoQingPvPSkillSelectWnd:OnDragIconStart(go)
    
    for i = 1,#self.m_SkillButtons do
        if self.m_SkillButtons[i].button.gameObject == go then
            if CClientMainPlayer.Inst ~= nil then
                self.m_SkillId = self.m_SkillIdList[i]
                if self.m_SkillId[2] == SkillButtonSkillType_lua.NoSkill then --?
                    return
                end
                local data =  Skill_AllSkills.GetData(self.m_SkillId[2])
                self.m_SrcSkillIndex = i
                self.m_FollowFingerIcon.gameObject:SetActive(true)
                local tex = self.m_FollowFingerIcon.transform:Find("SkillIcon"):GetComponent(typeof(CUITexture))

                if data ~= nil then
                    tex:LoadSkillIcon(data.SkillIcon)
                else
                    print("skillicon is nil!"..tostring(self.m_SkillId[2]))

                end

                
                self.m_IsDragging = true
                self.m_LastDragPos = Input.mousePosition

                if self.m_LastHighlight ~= nil then
                    self.m_LastHighlight.transform:Find("SelectedBg").gameObject:SetActive(false)
                end
                self.m_LastHighlight = go
                go.transform:Find("SelectedBg").gameObject:SetActive(true)

            end
            break
        end
    end
    SoundManager.Inst:PlayOneShot(SoundManager.UISoundEvents.Skill_Switch, Vector3.zero)
end



function LuaGuoQingPvPSkillSelectWnd:ReplaceSkill(data)

    local srcSkillId, go, srcActiveSkillIndex = data[1], data[2], data[3]
    local mainPlayer = CClientMainPlayer.Inst
    if srcSkillId[2] == SkillButtonSkillType_lua.NoSkill or mainPlayer == nil or Skill_AllSkills.GetData(srcSkillId[2]) == nil then
        return
    end

    local quality = self.m_SkillqualityList[srcActiveSkillIndex]
    local skilbutton = self.m_SkillButtons[srcActiveSkillIndex].button
    local chosenbg = FindChild(skilbutton.transform,"ChosenBg").gameObject

    

    local pbutton = go.transform.parent.parent.gameObject
    local ischosen = false
    


    for i = 1,7 do
        local name = "Button"..tostring(i)
        if name == pbutton.name then


            if i < 6 then
                if quality == 0 then
                    for j = 1,5 do --check chongfu
                        if j ~= i and self.m_SelectedSkill[j] ~= nil then
                            if self.m_SelectedSkill[j][2]== srcSkillId[2] then
                                g_MessageMgr:ShowMessage("JinLuHunYuanZhan_Choose_Skill_Repeat_Error")
                                return
                            end
                        end
                    end

                    self.m_SelectedSkill[i] = srcSkillId
                    local skilldata = Skill_AllSkills.GetData(srcSkillId[2])
                    if skilldata ~= nil then
                        go.gameObject:GetComponent(typeof(CUITexture)):LoadSkillIcon(skilldata.SkillIcon)
                    else
                        go.gameObject:GetComponent(typeof(CUITexture)):Clear()
                    end
                    
                    if self.m_SelectedSkillObj[i] ~= nil then
                        local oldchosenbg = FindChild(self.m_SelectedSkillObj[i].transform,"ChosenBg").gameObject
                        oldchosenbg:SetActive(false)
                    end
                    self.m_SelectedSkillObj[i] = skilbutton
                    chosenbg:SetActive(true)
                    ischosen = true
                else
                    g_MessageMgr:ShowMessage("JinLuHunYuanZhan_Choose_Skill_Not_Match")
                end
            else
                if quality == 1 then
                    for j = 6,7 do --check chongfu
                        if j ~= i and self.m_SelectedSkill[j] ~= nil then
                            if self.m_SelectedSkill[j][2]== srcSkillId[2] then
                                g_MessageMgr:ShowMessage("JinLuHunYuanZhan_Choose_Skill_Repeat_Error")
                                return
                            end
                        end
                    end

                    self.m_SelectedSkill[i] = srcSkillId
                    local skilldata = Skill_AllSkills.GetData(srcSkillId[2])
                    if skilldata ~= nil then
                        go.gameObject:GetComponent(typeof(CUITexture)):LoadSkillIcon(skilldata.SkillIcon)
                    else
                        go.gameObject:GetComponent(typeof(CUITexture)):Clear()
                    end
                    if self.m_SelectedSkillObj[i] ~= nil then
                        local oldchosenbg = FindChild(self.m_SelectedSkillObj[i].transform,"ChosenBg").gameObject
                        oldchosenbg:SetActive(false)
                    end
                    self.m_SelectedSkillObj[i] = skilbutton
                    chosenbg:SetActive(true)
                    ischosen = true
                else
                    g_MessageMgr:ShowMessage("JinLuHunYuanZhan_Choose_Skill_Not_Match")
                end
            end
            break
        end
    end
    if ischosen then
        self:SendSelectInfo()    
    end
    
end


function LuaGuoQingPvPSkillSelectWnd:OnSkillIconClick(go)
    local mainPlayer = CClientMainPlayer.Inst
    if not mainPlayer then
        return
    end
    for i = 1,#self.m_SkillButtons do
        if self.m_SkillButtons[i].button.gameObject == go then
            local skillId = self.m_SkillIdList[i][2]
            
            if skillId == SkillButtonSkillType_lua.NoSkill then
                return
            end
            if self.m_LastHighlight ~= nil then
                self.m_LastHighlight.transform:Find("SelectedBg").gameObject:SetActive(false)
            end
            self.m_LastHighlight = go
            go.transform:Find("SelectedBg").gameObject:SetActive(true)
            CSkillInfoMgr.ShowSkillInfoWnd(skillId, true, 0, 0, nil)
            break
        end
    end
end

--@endregion UIEvent

