-- Auto Generated!!
local CftfTApplyListTpl = import "L10.UI.CftfTApplyListTpl"
CftfTApplyListTpl.m_Init_CS2LuaHook = function (this, value, id, eId, isFriend, title, color) 
    do
        local i = 0
        while i < value.Length and i < this.column.Length do
            this.column[i].text = value[i]
            i = i + 1
        end
    end

    this.playerId = id
    this.engingId = eId
    this.typeLabel.text = title
    this.typeLabel.color = color
end
