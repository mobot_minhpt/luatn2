local CPlayerInfoMgr=import "CPlayerInfoMgr"
local Profession = import "L10.Game.Profession"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
local AlignType=import "CPlayerInfoMgr+AlignType"
local CUICommonDef = import "L10.UI.CUICommonDef"
local Color = import "UnityEngine.Color"

CLuaDouDiZhuGuildChooseWnd=class()
RegistClassMember(CLuaDouDiZhuGuildChooseWnd,"m_ConfirmButton")
RegistClassMember(CLuaDouDiZhuGuildChooseWnd,"m_TableViewDataSource")
RegistClassMember(CLuaDouDiZhuGuildChooseWnd,"m_TableView")
RegistClassMember(CLuaDouDiZhuGuildChooseWnd,"m_MoreInfo")
RegistClassMember(CLuaDouDiZhuGuildChooseWnd,"m_NoResultLabel")
RegistClassMember(CLuaDouDiZhuGuildChooseWnd,"m_SelectedLabel")
RegistClassMember(CLuaDouDiZhuGuildChooseWnd,"m_TimeLabel")
RegistClassMember(CLuaDouDiZhuGuildChooseWnd,"m_MyPraiseLabel")
RegistClassMember(CLuaDouDiZhuGuildChooseWnd,"m_FightOrNotLabel")

RegistClassMember(CLuaDouDiZhuGuildChooseWnd, "m_SelectedPlayers")

function CLuaDouDiZhuGuildChooseWnd:Init()

    self.m_MyPraiseLabel = self.transform:Find("Anchor/Static/MyPraiseLabel"):GetComponent(typeof(UILabel))
    self.m_FightOrNotLabel = self.transform:Find("Anchor/Static/FightOrNotLabel"):GetComponent(typeof(UILabel))

    local function InitItem(item,index)
        if index % 2 == 1 then
            item:SetBackgroundTexture("common_textbg_02_dark")
        else
            item:SetBackgroundTexture("common_textbg_02_light")
        end
        self:InitItem(item,index)
    end
    self.m_TableView=FindChild(self.transform,"TableView"):GetComponent(typeof(QnTableView))
    self.m_TableViewDataSource=DefaultTableViewDataSource.CreateByCount(#CLuaDouDiZhuMgr.m_GuildSignupPlayerInfo,InitItem)
    self.m_TableView.m_DataSource=self.m_TableViewDataSource
    self.m_TableView.OnSelectAtRow=DelegateFactory.Action_int(function (row)
        self:OnSelectAtRow(row)
    end)
    self.m_TableView:ReloadData(true,false)

    UIEventListener.Get(self.m_MoreInfo).onClick = LuaUtils.VoidDelegate(function(go)
            g_MessageMgr:ShowMessage("GUILD_DOUDIZHU_TIPS")
        end)
    UIEventListener.Get(self.m_ConfirmButton.gameObject).onClick = LuaUtils.VoidDelegate(function (go)
        self:OnConfirmButtonClicked(go)
    end)
    self:UpdateCandidates()
    self:UpdateButtonStatus()
    
    self.m_TimeLabel.text = SafeStringFormat3(LocalString.GetString("确认出战时间：%s"), DouDiZhu_Setting.GetData().ChoosePlayerTime)
    if CLuaDouDiZhuMgr.m_ShowFight then
        self.m_TimeLabel.color = Color.red
    end
end

function CLuaDouDiZhuGuildChooseWnd:OnEnable()
    g_ScriptEvent:AddListener("OnUpdateDouDiZhuChooseInfo", self, "OnUpdateDouDiZhuChooseInfo")
    g_ScriptEvent:AddListener("VoteForGuildDouDiZhuSuccess", self, "UpdatePraiseSuccess")
    g_ScriptEvent:AddListener("CancelVoteForGuildDouDiZhuSuccess", self, "UpdateDispraiseSuccess")
end

function CLuaDouDiZhuGuildChooseWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnUpdateDouDiZhuChooseInfo", self, "OnUpdateDouDiZhuChooseInfo")
    g_ScriptEvent:RemoveListener("VoteForGuildDouDiZhuSuccess", self, "UpdatePraiseSuccess")
    g_ScriptEvent:RemoveListener("CancelVoteForGuildDouDiZhuSuccess", self, "UpdateDispraiseSuccess")
end

function  CLuaDouDiZhuGuildChooseWnd:OnUpdateDouDiZhuChooseInfo(args)
    self.m_TableView:ReloadData(true,false)
    self:UpdateCandidates()
    self:UpdateButtonStatus()
end

-- 更新被点赞的信息
function CLuaDouDiZhuGuildChooseWnd:UpdatePraiseSuccess(votePlayerId)
    for k, v in ipairs(CLuaDouDiZhuMgr.m_GuildSignupPlayerInfo) do
        if v.id == votePlayerId then
            local item = self.m_TableView:GetItemAtRow(k-1)
            if item then
                v.bVote = true
                v.voteNum = v.voteNum + 1
                local tf=item.transform
                local praiseNumLabel=FindChild(tf,"PraiseNumLabel"):GetComponent(typeof(UILabel))
                local praiseButton=FindChild(tf,"PraiseButton").gameObject
                local praisedIcon = FindChild(praiseButton.transform,"PraisedIcon").gameObject
                local praiseFx = FindChild(praisedIcon.transform,"UI_aixin").gameObject
                praiseNumLabel.text=tostring(v.voteNum)
                praisedIcon:SetActive(true)
                praiseFx:SetActive(true)
                CLuaDouDiZhuMgr.m_PraiseNum = CLuaDouDiZhuMgr.m_PraiseNum + 1
                local color = "FF0000"
                if CLuaDouDiZhuMgr.m_PraiseNum >= 3 then
                    color = "00FF00"
                end
                self.m_MyPraiseLabel.text = SafeStringFormat3(LocalString.GetString("[ACF8FF]点赞([-][%s]%d[-][ACF8FF]/3)[-]"), color, CLuaDouDiZhuMgr.m_PraiseNum)
                local onDispraise = function (go)
                    Gac2Gas.RequestCancelVoteGuildDouDiZhu(v.id)
                end
                CommonDefs.AddOnClickListener(praiseButton,DelegateFactory.Action_GameObject(onDispraise),false)
            end
        end
    end
end

-- 更新被取消被点赞的信息
function CLuaDouDiZhuGuildChooseWnd:UpdateDispraiseSuccess(votePlayerId)
    for k, v in ipairs(CLuaDouDiZhuMgr.m_GuildSignupPlayerInfo) do
        if v.id == votePlayerId then
            local item = self.m_TableView:GetItemAtRow(k-1)
            if item then
                v.bVote = false
                v.voteNum = v.voteNum - 1
                local tf=item.transform
                local praiseNumLabel=FindChild(tf,"PraiseNumLabel"):GetComponent(typeof(UILabel))
                local praiseButton=FindChild(tf,"PraiseButton").gameObject
                local praisedIcon = FindChild(praiseButton.transform,"PraisedIcon").gameObject
                local praiseFx = FindChild(praisedIcon.transform,"UI_aixin").gameObject
                praiseNumLabel.text=tostring(v.voteNum)
                praisedIcon:SetActive(false)
                praiseFx:SetActive(false)
                CLuaDouDiZhuMgr.m_PraiseNum = CLuaDouDiZhuMgr.m_PraiseNum - 1
                local color = "FF0000"
                if CLuaDouDiZhuMgr.m_PraiseNum >= 3 then
                    color = "00FF00"
                end
                self.m_MyPraiseLabel.text = SafeStringFormat3(LocalString.GetString("[ACF8FF]点赞([-][%s]%d[-][ACF8FF]/3)[-]"), color, CLuaDouDiZhuMgr.m_PraiseNum)
                local onPraise = function (go)
                    Gac2Gas.RequestVoteGuildDouDiZhu(v.id)
                end
                CommonDefs.AddOnClickListener(praiseButton,DelegateFactory.Action_GameObject(onPraise),false)
            end
        end
    end
end

function CLuaDouDiZhuGuildChooseWnd:UpdateCandidates()
    if CLuaDouDiZhuMgr.m_ShowFight then
        self.m_MyPraiseLabel.gameObject:SetActive(false)
        self.m_FightOrNotLabel.gameObject:SetActive(true)
        self.m_FightOrNotLabel.text = SafeStringFormat3(LocalString.GetString("[ACF8FF]出战[-](%d)"), CLuaDouDiZhuMgr.m_FightNum)
    else
        self.m_FightOrNotLabel.gameObject:SetActive(false)
        self.m_MyPraiseLabel.gameObject:SetActive(true)
        local color = "FF0000"
        if CLuaDouDiZhuMgr.m_PraiseNum >= 3 then
            color = "00FF00"
        end
        self.m_MyPraiseLabel.text = SafeStringFormat3(LocalString.GetString("[ACF8FF]点赞([-][%s]%d[-][ACF8FF]/3)[-]"), color, CLuaDouDiZhuMgr.m_PraiseNum)
    end

    if #CLuaDouDiZhuMgr.m_GuildSignupPlayerInfo > 0 then
        self.m_NoResultLabel.gameObject:SetActive(false)
    else
        CUICommonDef.SetActive(self.m_NoResultLabel.gameObject, true, true)
    end
end

-- 更新按钮
function CLuaDouDiZhuGuildChooseWnd:UpdateButtonStatus()
    self.m_ConfirmButton.Enabled = CLuaDouDiZhuMgr.m_CanSignUp
    if CLuaDouDiZhuMgr.m_SignUp then
        self.m_ConfirmButton.Text = LocalString.GetString("已报名")
        self.m_ConfirmButton.Enabled = false
    end
end

function CLuaDouDiZhuGuildChooseWnd:OnSelectAtRow(row)
    local data=CLuaDouDiZhuMgr.m_GuildSignupPlayerInfo[row+1]
    if data then
        CPlayerInfoMgr.ShowPlayerPopupMenu(data.id, EnumPlayerInfoContext.Undefined,EChatPanel.Undefined,"",nil,Vector3.zero,AlignType.Default)
    end
end

function CLuaDouDiZhuGuildChooseWnd:InitItem(item,index)

    local info=CLuaDouDiZhuMgr.m_GuildSignupPlayerInfo[index+1]
    local tf=item.transform
    local nameLabel=FindChild(tf,"PlayerNameLabel"):GetComponent(typeof(UILabel))
    local clsSprite=FindChild(tf,"ClsSprite"):GetComponent(typeof(UISprite))
    local fightTimesLabel=FindChild(tf,"FightTimesLabel"):GetComponent(typeof(UILabel))
    local bestRankLabel=FindChild(tf,"BestRankLabel"):GetComponent(typeof(UILabel))
    local praiseNumLabel=FindChild(tf,"PraiseNumLabel"):GetComponent(typeof(UILabel))
    local isFight=FindChild(tf,"IsFight").gameObject
    local praiseButton=FindChild(tf,"PraiseButton").gameObject

    nameLabel.text=info.name
    clsSprite.spriteName=Profession.GetIconByNumber(info.class)
    fightTimesLabel.text=tostring(info.fightTimes)
    bestRankLabel.text=tostring(info.maxRankRecord)
    praiseNumLabel.text=tostring(info.voteNum)

    if CLuaDouDiZhuMgr.m_ShowFight then -- 是否显示出战玩家
        praiseButton:SetActive(false)
        isFight:SetActive(info.bFight)
    else
        isFight:SetActive(false)
        local praisedIcon = FindChild(praiseButton.transform,"PraisedIcon").gameObject
        local praiseFx = FindChild(praisedIcon.transform,"UI_aixin").gameObject
        if info.bVote then
            praisedIcon:SetActive(true)
            praiseFx:SetActive(false)
            local onDispraise = function (go)
                Gac2Gas.RequestCancelVoteGuildDouDiZhu(info.id)
            end
            CommonDefs.AddOnClickListener(praiseButton,DelegateFactory.Action_GameObject(onDispraise),false)
        else
            praisedIcon:SetActive(false)
            local onPraise = function (go)
                Gac2Gas.RequestVoteGuildDouDiZhu(info.id)
            end
            CommonDefs.AddOnClickListener(praiseButton,DelegateFactory.Action_GameObject(onPraise),false)
        end
    end
end



function CLuaDouDiZhuGuildChooseWnd:OnConfirmButtonClicked(go)
    Gac2Gas.RequestSignUpGuildDouDiZhu()
end


return CLuaDouDiZhuGuildChooseWnd
