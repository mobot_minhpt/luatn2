-- Auto Generated!!
local CChatLinkMgr = import "CChatLinkMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CLingshouOverviewDetailView = import "L10.UI.CLingshouOverviewDetailView"
local Color = import "UnityEngine.Color"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"
local ItemGet_item = import "L10.Game.ItemGet_item"
local LingShou_LingShou = import "L10.Game.LingShou_LingShou"
local LingShou_TypeTemplate = import "L10.Game.LingShou_TypeTemplate"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CLingshouOverviewDetailView.m_Init_CS2LuaHook = function (this, lingshouId) 
    this.gradeLabel.text = ""
    this.growLabel.text = ""
    this.talentLabel.text = ""
    this.descLabel.text = ""
    this.acquireDesc.text = ""
    this.shenshouSkill:SetActive(false)
    this.models:Init(lingshouId)
    this.lingshou = LingShou_LingShou.GetData(lingshouId)
    if this.lingshou == nil then
        return
    end
    this.gradeLabel.text = tostring(this.lingshou.Grade)
    this.gradeLabel.color = CClientMainPlayer.Inst.MaxLevel < this.lingshou.Grade and Color.red or Color.green
    repeat
        local default = this.lingshou.ShenShou
        if default == 0 then
            this.growLabel.text = this.grow0
            break
        elseif default == 1 then
            this.growLabel.text = this.grow1
            break
        elseif default == 2 then
            this.growLabel.text = this.grow2
            break
        else
            this.growLabel.text = "????~????"
            break
        end
    until 1
    local template = LingShou_TypeTemplate.GetData(this.lingshou.TypeTemplate)
    if template == nil then
        return
    end
    if this.lingshou.ShenShou == 0 then
        this.talentLabel.text = CChatLinkMgr.TranslateToNGUIText(template.ZizhiRange, false)
    else
        this.talentLabel.text = CChatLinkMgr.TranslateToNGUIText(template.ShenShouZizhiRange, false)
    end
    this.descLabel.text = CChatLinkMgr.TranslateToNGUIText(template.TypeDes, false)
    this:InitAcquire()
    this:InitSkill()
end
CLingshouOverviewDetailView.m_InitSkill_CS2LuaHook = function (this) 
    if this.lingshou ~= nil and this.lingshou.BornSkill > 0 then
        local skill = Skill_AllSkills.GetData(this.lingshou.BornSkill)
        if skill == nil then
            return
        end
        this.shenshouSkill:SetActive(true)
        this.skillIconTexture:LoadSkillIcon(skill.SkillIcon)
        UIEventListener.Get(this.shenshouSkill).onClick = MakeDelegateFromCSFunction(this.OnSkillClick, VoidDelegate, this)
    end
end
CLingshouOverviewDetailView.m_OnSkillClick_CS2LuaHook = function (this, go) 
    if this.lingshou ~= nil and this.lingshou.BornSkill > 0 then
        local skill = Skill_AllSkills.GetData(this.lingshou.BornSkill)
        if skill == nil then
            return
        end
        CSkillInfoMgr.ShowSkillInfoWnd(this.lingshou.BornSkill, true, 0, 0, nil)
    end
end
CLingshouOverviewDetailView.m_InitAcquire_CS2LuaHook = function (this) 
    local item = ItemGet_item.GetData(this.lingshou.ID)
    if item ~= nil and not System.String.IsNullOrEmpty(item.Message) then
        this.acquireDesc.text = g_MessageMgr:FormatMessage(item.Message)
    end
    --scrollView.ResetPosition();
end
