require("common/common_include")
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local LuaGameObject=import "LuaGameObject"
-- local File=import "System.IO.File"
local ShareMgr=import "ShareMgr"
local CChatMgr=import "L10.Game.CChatMgr"
local EShareType=import "L10.UI.EShareType"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
--ref to PlayerMainStoryShareWnd
CLuaShare2PersonalSpaceWnd=class()
-- RegistClassMember(CLuaShareImageToMengDaoWnd,"m_TitleLabel")
RegistClassMember(CLuaShare2PersonalSpaceWnd,"m_ShareButton")
RegistClassMember(CLuaShare2PersonalSpaceWnd,"m_Texture")
RegistClassMember(CLuaShare2PersonalSpaceWnd,"m_Input")

function CLuaShare2PersonalSpaceWnd:Init()
    self.m_ShareButton=LuaGameObject.GetChildNoGC(self.transform,"ShareButton").gameObject
    self.m_Texture=LuaGameObject.GetChildNoGC(self.transform,"Texture").texture
    self.m_Input=LuaGameObject.GetChildNoGC(self.transform,"Input").input
    CUICommonDef.LoadLocalFile2Texture(self.m_Texture,ShareMgr.Share2PersonalSpaceFile,1024,576)

    local function OnClick(go)
        self:Share()
    end
    CommonDefs.AddOnClickListener(self.m_ShareButton,DelegateFactory.Action_GameObject(OnClick),false)
end

function CLuaShare2PersonalSpaceWnd:Share()
    local shareText=self.m_Input.value
    shareText=CChatMgr.Inst:FilterYangYangEmotion(shareText)

    shareText = CWordFilterMgr.Inst:DoFilterOnSendPersonalSpace(shareText, true)
    if shareText then
        if ShareBoxMgr.ShareType == EShareType.ShareTranspareImage and ShareBoxMgr.TopicName and ShareBoxMgr.TopicId > 0 then
            shareText =	SafeStringFormat3("<link button=%s,PSHotTalk,%s>", ShareBoxMgr.TopicName, ShareBoxMgr.TopicId).. shareText
        end
        ShareBoxMgr.TopicName = nil
        ShareBoxMgr.TopicId = 0
        ShareMgr.Share2PersonalSpace(shareText, true)
    end
end

-- function CLuaShare2PersonalSpaceWnd:OnEnable()
--     -- g_ScriptEvent:AddListener("SendCurrentHuoGuoMeterial", self, "OnSendCurrentHuoGuoMeterial")
-- end

-- function CLuaShare2PersonalSpaceWnd:OnDisable()
--     -- g_ScriptEvent:RemoveListener("SendCurrentHuoGuoMeterial", self, "OnSendCurrentHuoGuoMeterial")
-- end

return CLuaShare2PersonalSpaceWnd
