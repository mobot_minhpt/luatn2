local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CUITexture = import "L10.UI.CUITexture"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local CClientMonster = import "L10.Game.CClientMonster"
local CConversationMgr = import "L10.Game.CConversationMgr"

LuaLiuRuShiHuanZhuangWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaLiuRuShiHuanZhuangWnd, "ModelTexture", "ModelTexture", CUITexture)
RegistChildComponent(LuaLiuRuShiHuanZhuangWnd, "ChoiceTable", "ChoiceTable", GameObject)
RegistChildComponent(LuaLiuRuShiHuanZhuangWnd, "ConfirmBtn", "ConfirmBtn", GameObject)

RegistClassMember(LuaLiuRuShiHuanZhuangWnd, "m_ModelRO")
RegistClassMember(LuaLiuRuShiHuanZhuangWnd, "m_MonsterIdTbl")
RegistClassMember(LuaLiuRuShiHuanZhuangWnd, "m_ChoiceList")
RegistClassMember(LuaLiuRuShiHuanZhuangWnd, "m_ChoiceIndex")
--@endregion RegistChildComponent end

function LuaLiuRuShiHuanZhuangWnd:Awake()
    UIEventListener.Get(self.ConfirmBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnConfirmBtnClick()
    end)
end

function LuaLiuRuShiHuanZhuangWnd:Init()
    self:InitChoices()
    self:InitModel()
end

function LuaLiuRuShiHuanZhuangWnd:InitModel()
    local modelTextureLoader = LuaDefaultModelTextureLoader.Create(function(ro)
        self.m_ModelRO = ro
	    CClientMonster.LoadResourceIntoRO(self.m_ModelRO, self.m_MonsterIdTbl[1], true)
    end)
	self.ModelTexture.mainTexture = CUIManager.CreateModelTexture("__LiuRuShiHuanZhuang__", modelTextureLoader, 180, 0.05, -1, 4.66, false, true, 1, true, false)
end

function LuaLiuRuShiHuanZhuangWnd:InitChoices()
    self.m_ChoiceList = {}
    self.m_MonsterIdTbl = {}
    for i = 1, self.ChoiceTable.transform.childCount do
        local choice = self.ChoiceTable.transform:GetChild(i - 1)
        local data = NanDuFanHua_LiuRuShiHuanZhuang.GetData(i)
        choice.transform:Find("Icon"):GetComponent(typeof(CUITexture)):LoadMaterial(data.IconPath)
        choice.transform:Find("Desc"):GetComponent(typeof(UILabel)).text = data.Description
        table.insert(self.m_MonsterIdTbl, data.MonsterId)
        table.insert(self.m_ChoiceList, choice)
        UIEventListener.Get(choice.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnChoiceClick(i)
        end)
    end
    self:OnChoiceClick(1)
end

--@region UIEvent
function LuaLiuRuShiHuanZhuangWnd:OnChoiceClick(index)
    self.m_ChoiceIndex = index
    for i = 1, #self.m_ChoiceList do
        self.m_ChoiceList[i].transform:Find("Normal").gameObject:SetActive(i ~= index)
        self.m_ChoiceList[i].transform:Find("HighLight").gameObject:SetActive(i == index)
        local color = i == index and "6C634A" or "B9C5CD"
        self.m_ChoiceList[i].transform:Find("Desc"):GetComponent(typeof(UILabel)).color = NGUIText.ParseColor24(color, 0)
    end
    if self.m_ModelRO then
	    CClientMonster.LoadResourceIntoRO(self.m_ModelRO, self.m_MonsterIdTbl[index], true)
    end
end

function LuaLiuRuShiHuanZhuangWnd:OnConfirmBtnClick()
    local data = NanDuFanHua_LiuRuShiHuanZhuang.GetData(self.m_ChoiceIndex)
    if data.Correct == 1 then
        Gac2Gas.RequestLiuRuShiHuanZhuang(LuaLiuRuShiMgr.m_HuanZhuangTaskId, self.m_ChoiceIndex)
    else
        local dialogId = data.DialogId
        local dialogMsg = Dialog_TaskDialog.GetData(dialogId).Dialog
        local taskFailText = data.TaskFailText
        CConversationMgr.Inst:OpenStoryDialog(dialogMsg, DelegateFactory.Action(function ()
            LuaLiuRuShiMgr:ShowLiuRuShiTaskFailWnd(LuaLiuRuShiMgr.m_HuanZhuangTaskId, taskFailText)
        end))
    end
    CUIManager.CloseUI(CLuaUIResources.LiuRuShiHuanZhuangWnd)
end

--@endregion UIEvent
function LuaLiuRuShiHuanZhuangWnd:OnDisable()
	self.ModelTexture.mainTexture = nil
	if self.m_ModelRO then
		self.m_ModelRO:Destroy()
		self.m_ModelRO = nil
	end
	CUIManager.DestroyModelTexture("__LiuRuShiHuanZhuang__")
end