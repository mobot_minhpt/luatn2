local CButton = import "L10.UI.CButton"
local Animator = import "UnityEngine.Animator"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"
LuaHwMuTouRenEnterWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaHwMuTouRenEnterWnd, "TimeLabel", "TimeLabel", UILabel)
RegistChildComponent(LuaHwMuTouRenEnterWnd, "RuleBtn", "RuleBtn", GameObject)
RegistChildComponent(LuaHwMuTouRenEnterWnd, "StartBtn", "StartBtn", GameObject)
RegistChildComponent(LuaHwMuTouRenEnterWnd, "ItemCell1", "ItemCell1", GameObject)
RegistChildComponent(LuaHwMuTouRenEnterWnd, "ItemCell2", "ItemCell2", GameObject)
RegistChildComponent(LuaHwMuTouRenEnterWnd, "ItemCell3", "ItemCell3", GameObject)
RegistChildComponent(LuaHwMuTouRenEnterWnd, "DescLabel", "DescLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaHwMuTouRenEnterWnd, "m_MatchStatus")    -- 匹配状态 0不在匹配，1在匹配中
RegistClassMember(LuaHwMuTouRenEnterWnd, "m_ItemCell4")
function LuaHwMuTouRenEnterWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    UIEventListener.Get(self.RuleBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRuleBtnClick()
	end)

    UIEventListener.Get(self.StartBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnStartBtnClick()
	end)
    self.m_ItemCell4 = self.transform:Find("Content/Reward/ItemCell4").gameObject
end

function LuaHwMuTouRenEnterWnd:Init()
	self.TimeLabel.text = Halloween2022_Setting.GetData().TrafficPlayTimeString
    self.DescLabel.text = Halloween2022_Setting.GetData().TrafficPlayDescString
    self.transform:Find("Content/FormLabel").gameObject:GetComponent(typeof(UILabel)).text = LocalString.GetString("多人玩法")
    self.transform:Find("Content/LevelLabel").gameObject:GetComponent(typeof(UILabel)).text = LocalString.GetString("50级")

    self:InitAwardItem()
    self.TimeLabel.color = Color.red
    self.StartBtn:SetActive(false)
    Gac2Gas.QueryPlayerIsMatchingTrafficLight()
    Gac2Gas.QueryPlayerTrafficLightAward()
end

function LuaHwMuTouRenEnterWnd:InitAwardItem()
    self:InitOneItem(self.ItemCell1,Halloween2022_Setting.GetData().Traffic30mAward)
    self:InitOneItem(self.ItemCell2,Halloween2022_Setting.GetData().TrafficDailyAward)
    self:InitOneItem(self.ItemCell3,Halloween2022_Setting.GetData().Traffic1stPlayerAward)
    self:InitOneItem(self.m_ItemCell4,Halloween2022_Setting.GetData().TrafficFirstAward)
end

function LuaHwMuTouRenEnterWnd:InitOneItem(curItem, itemID)
    local texture = curItem.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    local ItemData = Item_Item.GetData(itemID)
    curItem.transform:Find("Received").gameObject:SetActive(false)
    if ItemData then texture:LoadMaterial(ItemData.Icon) end

    CommonDefs.AddOnClickListener(
        curItem,
        DelegateFactory.Action_GameObject(
            function(go)
                CItemInfoMgr.ShowLinkItemTemplateInfo(itemID, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
            end
        ),
        false
    )
end

function LuaHwMuTouRenEnterWnd:UpdateMatchBtn(isMatching)
    self.m_MatchStatus = isMatching
    self.TimeLabel.color = Color.white
    self.StartBtn:SetActive(true)
    local BtnLabel = self.StartBtn.transform:Find("Label"):GetComponent(typeof(UILabel))
    local MatchLabel = self.StartBtn.transform:Find("StartLabel"):GetComponent(typeof(UILabel))
    if not self.m_MatchStatus then -- 不在匹配
        self.StartBtn:GetComponent(typeof(UISprite)).spriteName = "common_btn_01_yellow"
        BtnLabel.text = LocalString.GetString("开始匹配")
        MatchLabel.gameObject:SetActive(false)
    elseif self.m_MatchStatus then -- 匹配中
        self.StartBtn:GetComponent(typeof(UISprite)).spriteName = "common_btn_01_blue"
        BtnLabel.text = LocalString.GetString("取消匹配")
        MatchLabel.gameObject:SetActive(true)
    end
end
-- 奖励图标显示状态
function LuaHwMuTouRenEnterWnd:UpdateAwardStatus()
    self.ItemCell1.transform:Find("Received").gameObject:SetActive(LuaHalloween2022Mgr.PlayerTrafficLightAwardResult.participationAwardTimes >= Halloween2022_Setting.GetData().Traffic30mAwardTimes)
    self.ItemCell2.transform:Find("Received").gameObject:SetActive(LuaHalloween2022Mgr.PlayerTrafficLightAwardResult.dailyAwardTimes >= Halloween2022_Setting.GetData().TrafficDailyAwardTimes)
    self.ItemCell3.transform:Find("Received").gameObject:SetActive(LuaHalloween2022Mgr.PlayerTrafficLightAwardResult.isGet1stAward)
    self.m_ItemCell4.transform:Find("Received").gameObject:SetActive(LuaHalloween2022Mgr.PlayerTrafficLightAwardResult.isGetOnceAchieveAward)
end
--@region UIEvent

function LuaHwMuTouRenEnterWnd:OnRuleBtnClick()
    g_MessageMgr:ShowMessage("Halloween2022_Trafficplay_TaskDes")
end

function LuaHwMuTouRenEnterWnd:OnStartBtnClick()
    if not self.m_MatchStatus then -- 不在匹配，发开始匹配RPC
        Gac2Gas.RequestSignUpTrafficLight()
    elseif self.m_MatchStatus then  -- 在匹配中，发取消匹配RPC
        Gac2Gas.RequestCancelSignTrafficLight()
    end
    --Gac2Gas.QueryPlayerIsMatchingTrafficLight()
end

--@endregion UIEvent
function LuaHwMuTouRenEnterWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncPlayerTrafficMatchStatus", self, "UpdateMatchBtn")
    g_ScriptEvent:AddListener("SyncPlayerTrafficLightAward", self, "UpdateAwardStatus")
end

function LuaHwMuTouRenEnterWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncPlayerTrafficMatchStatus", self, "UpdateMatchBtn")
    g_ScriptEvent:RemoveListener("SyncPlayerTrafficLightAward", self, "UpdateAwardStatus")
end
