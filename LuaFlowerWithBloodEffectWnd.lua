local CChatLinkMgr=import "CChatLinkMgr"

local CBaseWnd = import "L10.UI.CBaseWnd"
local UIPanel = import "UIPanel"
local CUIFx = import "L10.UI.CUIFx"
local UILabel = import "UILabel"
local Camera = import "UnityEngine.Camera"
local CUICommonDef = import "L10.UI.CUICommonDef"
local Subtitle_Subtitle = import "L10.Game.Subtitle_Subtitle"
local Time = import "UnityEngine.Time"
local CommonDefs = import "L10.Game.CommonDefs"
local CUITexture = import "L10.UI.CUITexture"
local UITweener = import "UITweener"
local UIRoot = import "UIRoot"
local Screen = import "UnityEngine.Screen"

LuaFlowerWithBloodEffectWnd = class()
RegistClassMember(LuaFlowerWithBloodEffectWnd,"m_CloseButton")
RegistClassMember(LuaFlowerWithBloodEffectWnd,"m_FlowerTexture")
RegistClassMember(LuaFlowerWithBloodEffectWnd,"m_GlowwormFx")
RegistClassMember(LuaFlowerWithBloodEffectWnd,"m_GlowwormTweener")
RegistClassMember(LuaFlowerWithBloodEffectWnd,"m_FlowerCenter")
RegistClassMember(LuaFlowerWithBloodEffectWnd,"m_Bg")
RegistClassMember(LuaFlowerWithBloodEffectWnd,"m_DisplayCamera")
RegistClassMember(LuaFlowerWithBloodEffectWnd,"m_SubtitleLabel")

--字幕
RegistClassMember(LuaFlowerWithBloodEffectWnd,"m_NextSubtitleId")
RegistClassMember(LuaFlowerWithBloodEffectWnd,"m_AliveDuration")
RegistClassMember(LuaFlowerWithBloodEffectWnd,"m_FadeInTime")
RegistClassMember(LuaFlowerWithBloodEffectWnd,"m_FadeOutTime")
RegistClassMember(LuaFlowerWithBloodEffectWnd,"m_StartTime")

RegistClassMember(LuaFlowerWithBloodEffectWnd,"m_RoseTbl")
RegistClassMember(LuaFlowerWithBloodEffectWnd,"m_ReachLastRose")


--完成进度标记
RegistClassMember(LuaFlowerWithBloodEffectWnd,"m_SubtitleComplete")
RegistClassMember(LuaFlowerWithBloodEffectWnd,"m_NeedClose")
RegistClassMember(LuaFlowerWithBloodEffectWnd,"m_GlowwormShow")

RegistClassMember(LuaFlowerWithBloodEffectWnd,"m_DelayCloseTime")
RegistClassMember(LuaFlowerWithBloodEffectWnd,"m_RoseIndex")
RegistClassMember(LuaFlowerWithBloodEffectWnd,"m_CloseTick")

function LuaFlowerWithBloodEffectWnd:Awake()
    self.gameObject:GetComponent(typeof(UIPanel)).IgnoreIphoneXMargin = true
end

function LuaFlowerWithBloodEffectWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaFlowerWithBloodEffectWnd:Init()
	self.m_CloseButton = self.transform:Find("Camera/Panel/CloseButton").gameObject
	self.m_FlowerTexture = self.transform:Find("Camera/Panel/TweenAlpha/FlowerTexture"):GetComponent(typeof(CUITexture))
	self.m_GlowwormFx = self.transform:Find("Camera/Panel/TweenAlpha/FlowerCenter/GlowwormFx"):GetComponent(typeof(CUIFx))
	self.m_GlowwormTweener = self.m_GlowwormFx.transform:GetComponent(typeof(UITweener))
	self.m_FlowerCenter = self.transform:Find("Camera/Panel/TweenAlpha/FlowerCenter").transform
	self.m_Bg = self.transform:Find("Camera/Panel/BlackBg").gameObject
	self.m_DisplayCamera = self.transform:Find("Camera"):GetComponent(typeof(Camera))
	self.m_DisplayCamera.rect = CUICommonDef.GetCurrentCameraRect()
	self.m_SubtitleLabel = self.transform:Find("Camera/Panel/SubtitleLabel"):GetComponent(typeof(UILabel))


	self.m_RoseTbl = {}
	for i=1,4 do
		table.insert(self.m_RoseTbl, SafeStringFormat3("UI/Texture/Transparent/Material/rose_%d.mat", i))
	end
	self.m_RoseIndex = 1
	self.m_ReachLastRose = false

	self.m_FlowerTexture:Clear()
	self.m_GlowwormTweener.enabled = false

	CommonDefs.AddOnClickListener(self.m_CloseButton, DelegateFactory.Action_GameObject(function(go) self:Close() end), false)

    self:CancelCloseTick()

    self.m_SubtitleComplete = false
    self.m_NeedClose = false
    self.m_GlowwormShow = false

    self.m_DelayCloseTime = 1

	self.m_AliveDuration = 0
	self.m_FadeInTime = 0
	self.m_FadeOutTime = 0
	self.m_SubtitleLabel.text = nil
	self.m_SubtitleLabel.alpha = 1
	self.m_NextSubtitleId = CLuaTaskMgr.m_FloweWithBloodEffectSubtitleId
	if not Subtitle_Subtitle.Exists(self.m_NextSubtitleId) then
		self.m_SubtitleComplete = true
	end
	self.m_StartTime = Time.realtimeSinceStartup
	self:ShowSubtitle()
end

function LuaFlowerWithBloodEffectWnd:RegisterCloseTick()
	self:CancelCloseTick()
	self.m_CloseTick = RegisterTickOnce(function ( ... )
		self.m_CloseTick = nil
		self:OnFinished()
		end, 1000 * self.m_DelayCloseTime)
end

function LuaFlowerWithBloodEffectWnd:CancelCloseTick()
	if self.m_CloseTick then
		UnRegisterTick(self.m_CloseTick)
		self.m_CloseTick = nil
	end
end

function LuaFlowerWithBloodEffectWnd:OnFinished()
	CLuaTaskMgr.CompleteFloweWithBloodEffectTask()
	self:Close()
end

function LuaFlowerWithBloodEffectWnd:WaitForSecondsAndClose()
	self:RegisterCloseTick()
end

function LuaFlowerWithBloodEffectWnd:OnDisable()
	self:CancelCloseTick()
	self.m_NeedClose = false
end

function LuaFlowerWithBloodEffectWnd:Update()

	if self.m_SubtitleComplete then
		if not self.m_NeedClose then
			self.m_NeedClose = true
			self:WaitForSecondsAndClose()
		end
		return
	end

	if self.m_SubtitleComplete then
		return
	end

	local time = Time.realtimeSinceStartup - self.m_StartTime
	if time <= self.m_FadeInTime then
		-- fade in
		local alpha = time / self.m_FadeInTime
		self.m_SubtitleLabel.alpha = alpha
		if not self.m_ReachLastRose then
			self.m_FlowerTexture.texture.alpha = alpha
		end

	elseif self.m_AliveDuration - time <= self.m_FadeOutTime and time <= self.m_AliveDuration then
		-- fade out
		local alpha = (self.m_AliveDuration - time) / self.m_FadeOutTime
		self.m_SubtitleLabel.alpha = alpha
		if self.m_RoseIndex<#self.m_RoseTbl then
			self.m_FlowerTexture.texture.alpha = alpha
		end
	elseif time > self.m_AliveDuration or time < 0 then
		if Subtitle_Subtitle.Exists(self.m_NextSubtitleId) then
			self:ShowSubtitle()
		else
			self.m_SubtitleComplete = true
		end
	else
		self.m_SubtitleLabel.alpha = 1
		self.m_FlowerTexture.texture.alpha = 1
	end
end

function LuaFlowerWithBloodEffectWnd:ShowSubtitle()
	if Subtitle_Subtitle.Exists(self.m_NextSubtitleId) then
		local subtitle = Subtitle_Subtitle.GetData(self.m_NextSubtitleId)
		self.m_AliveDuration = subtitle.AliveDuration
		self.m_FadeInTime = subtitle.FadeInTime
		self.m_FadeOutTime = subtitle.FadeOutTime
		self.m_SubtitleLabel.text = CChatLinkMgr.TranslateToNGUIText(subtitle.Content, false)
		self.m_SubtitleLabel.alpha = 0
		self.m_NextSubtitleId = subtitle.Next
		self.m_StartTime = Time.realtimeSinceStartup

		if self.m_RoseTbl[self.m_RoseIndex] then
			self.m_FlowerTexture:LoadMaterial(self.m_RoseTbl[self.m_RoseIndex])
			self.m_FlowerTexture.texture.alpha = 0
		    self.m_RoseIndex = self.m_RoseIndex + 1
		else
			self.m_ReachLastRose = true
			--播放特效
			self:ShowGlowwormFx(self.m_FadeInTime, self.m_FadeOutTime, self.m_AliveDuration)
		end

	end
end

function LuaFlowerWithBloodEffectWnd:ShowGlowwormFx(fadeInTime, fadeOutTime, aliveDuration)
	if self.m_GlowwormShow then return end
	self.m_GlowwormShow = true
	self.m_GlowwormTweener.duration = fadeInTime
	self.m_GlowwormTweener.enabled = true
	self.m_GlowwormFx:LoadFx("fx/ui/prefab/ui_yinghuochong.prefab")
	local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject)
    local virtualScreenWidth = Screen.width * scale
    local enterX = -virtualScreenWidth * 0.5 - 100
    local exitX = virtualScreenWidth + 100

    self.m_FlowerCenter.localPosition = self.m_FlowerTexture.transform.localPosition
    Extensions.SetLocalPositionX(self.m_FlowerCenter, enterX)
	local fadeInTween = LuaTweenUtils.TweenPositionX(self.m_FlowerCenter, self.m_FlowerTexture.transform.localPosition.x, fadeInTime)
	local fadeOutTween = LuaTweenUtils.TweenPositionX(self.m_FlowerCenter, exitX, fadeOutTime + self.m_DelayCloseTime)

	LuaTweenUtils.OnComplete(fadeInTween, function ( ... )

		self.m_GlowwormTweener.enabled = false
	end)

	LuaTweenUtils.OnStart(fadeOutTween, function ( ... )

		self.m_GlowwormTweener.enabled = true
	end)
	LuaTweenUtils.SetDelay(fadeOutTween, aliveDuration - fadeOutTime + fadeInTime)
end
