
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local CScheduleMgr=import "L10.Game.CScheduleMgr"
local UISlider=import "UISlider"
local CSkillInfoMgr=import "L10.UI.CSkillInfoMgr"

CLuaScheduleHuoliView=class()
RegistClassMember(CLuaScheduleHuoliView,"huoliStar1")
RegistClassMember(CLuaScheduleHuoliView,"huoliStar2")
RegistClassMember(CLuaScheduleHuoliView,"huoliStar3")
RegistClassMember(CLuaScheduleHuoliView,"totalHuoliLabel")
RegistClassMember(CLuaScheduleHuoliView,"totalHuoliSlider")

function CLuaScheduleHuoliView:Init(tf)
    self.huoliStar1=FindChild(tf,"Star1"):GetComponent(typeof(UISprite))
    self.huoliStar2=FindChild(tf,"Star2"):GetComponent(typeof(UISprite))
    self.huoliStar3=FindChild(tf,"Star3"):GetComponent(typeof(UISprite))
    
    local huoliRegion=FindChild(tf,"HuoliRegion").gameObject
    UIEventListener.Get(huoliRegion).onClick=LuaUtils.VoidDelegate(function(go)
        CUIManager.ShowUI(CUIResources.ScheduleHuoliTip)
    end)

    self.totalHuoliSlider=FindChild(tf,"TotalHuoLiSlider"):GetComponent(typeof(UISlider))
    self.totalHuoliLabel=FindChild(tf,"TotalHuoliLabel"):GetComponent(typeof(UILabel))
    
    local useHuoliBtn=FindChild(tf,"UseHuoliBtn").gameObject
    UIEventListener.Get(useHuoliBtn).onClick=LuaUtils.VoidDelegate(function(go)
        CUICommonDef.CallMethodConsiderCrossServer(LuaUtils.Action(function()
            CSkillInfoMgr.ShowLivingSkillWnd()
        end),{})
    end)
    
end

function CLuaScheduleHuoliView:UpdateHuoli()
    if CClientMainPlayer.Inst and  CClientMainPlayer.Inst.PlayProp then
        self.totalHuoliLabel.text = SafeStringFormat3("%d/%d", CClientMainPlayer.Inst.PlayProp.HuoLi, CLuaScheduleMgr.GetMaxHuoLi());
        self.totalHuoliSlider.value = CClientMainPlayer.Inst.PlayProp.HuoLi / CLuaScheduleMgr.GetMaxHuoLi()
        local maxHuoli = 0;
        local huoli = 0;
        local infos = CScheduleMgr.Inst:GetCanJoinSchedules()
        local processedSchedule={}
        for i=1,infos.Count do
            local item=infos[i-1]

            local info = Task_Schedule.GetData(item.activityId)
            if CLuaScheduleMgr.ScheduleInfoFilter(item, info) and CLuaScheduleMgr.ScheduleInfoFilter2(item, info) then
	            if info.Type~="" and processedSchedule[info.Type] then
	                --nothing
	            else
	                processedSchedule[info.Type]=true
	                local curHuoli =CLuaScheduleMgr.GetHuoli(info.HuoLi)
	                if info.Type == "NewBieSchoolTask" then
						curHuoli = NewbieSchoolTask_Setting.GetData().HuoLiAward
					end
	                local all = math.floor(curHuoli * CLuaScheduleMgr.GetDailyTimes(info))

	                maxHuoli =maxHuoli+ all
	                if all>0 then
	                    local have = 0
	                    have = math.floor(math.min(item.FinishedTimes, CLuaScheduleMgr.GetDailyTimes(info)) * curHuoli)
	                    huoli =huoli+ have
	                end
	            end
	        end
        end

        local val = huoli / maxHuoli
        local _1_3=1.0/3.0
        if val > _1_3*2 then
            self.huoliStar1.fillAmount = 1
            self.huoliStar2.fillAmount = 1
            self.huoliStar3.fillAmount = (val - _1_3 * 2) / _1_3
        elseif val>_1_3 then
            self.huoliStar1.fillAmount = 1
            self.huoliStar3.fillAmount = 0
            self.huoliStar2.fillAmount = (val - _1_3) / _1_3
        elseif val>0 then
            self.huoliStar1.fillAmount = val / _1_3
            self.huoliStar2.fillAmount = 0;
            self.huoliStar3.fillAmount = 0;
        else
            self.huoliStar1.fillAmount = 0;
            self.huoliStar2.fillAmount = 0;
            self.huoliStar3.fillAmount = 0;
        end
    end

end

return CLuaScheduleHuoliView