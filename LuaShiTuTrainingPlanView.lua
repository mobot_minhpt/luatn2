local UISlider = import "UISlider"
local Extensions = import "Extensions"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local MessageWndManager = import "L10.UI.MessageWndManager"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CItemMgr = import "L10.Game.CItemMgr"

LuaShiTuTrainingPlanView  = class()

RegistChildComponent(LuaShiTuTrainingPlanView, "m_Grid","Grid", UIGrid)
RegistChildComponent(LuaShiTuTrainingPlanView, "m_Template","Template", GameObject)

RegistClassMember(LuaShiTuTrainingPlanView,"m_Plans")
RegistClassMember(LuaShiTuTrainingPlanView,"m_CanBeFilledItemIds")

function LuaShiTuTrainingPlanView:Awake()
    self.m_Template:SetActive(false)
end

function LuaShiTuTrainingPlanView:OnEnable()
    g_ScriptEvent:AddListener("ShiTuTrainingHandbook_SyncShiTuCultivateInfo", self, "OnSyncShiTuCultivateInfo")
    Gac2Gas.QueryShiTuCultivateInfo(LuaShiTuTrainingHandbookMgr.wndtype.TrainingPlan, LuaShiTuTrainingHandbookMgr:GetPartnerPlayerId())
end

function LuaShiTuTrainingPlanView:OnDisable()
    g_ScriptEvent:RemoveListener("ShiTuTrainingHandbook_SyncShiTuCultivateInfo", self, "OnSyncShiTuCultivateInfo")
end

function LuaShiTuTrainingPlanView:InitItemIds()
    self.m_CanBeFilledItemIds =  CreateFromClass(MakeGenericClass(List, cs_string))
    local bagSize = CClientMainPlayer.Inst.ItemProp:GetPlaceSize(EnumItemPlace.Bag)
    for i = 1,bagSize do
        local id = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Bag, i)
        if not cs_string.IsNullOrEmpty(id) then
            local precious = CItemMgr.Inst:GetById(id)
            if precious ~= nil then
                if precious.IsEquip and precious.Equip:IsAllowFace2FaceGift() then
                    CommonDefs.ListAdd(self.m_CanBeFilledItemIds, typeof(cs_string), precious.Id)
                elseif precious.IsItem and precious.Item:IsAllowFace2FaceGift() and not precious.Item:IsExpire() then
                    CommonDefs.ListAdd(self.m_CanBeFilledItemIds, typeof(cs_string), precious.Id)
                end
            end
        end
    end
end

function LuaShiTuTrainingPlanView:OnSyncShiTuCultivateInfo(data)
    self:InitItemIds()
    for i = 1, #data do
        local t = data[i]
        local cultivate = ShiTuCultivate_Cultivate.GetData(data[i].cultivateId)
        t.cultivate = cultivate
        local teacherRewards = {}
        table.insert(teacherRewards, {scorekey = EnumPlayScoreKey.QYJF, num = cultivate.ShifuQingyi})
        if cultivate.ShifuItems then
            for j = 0, cultivate.ShifuItems.Length - 1, 2 do
                local itemId, num = cultivate.ShifuItems[j], cultivate.ShifuItems[j + 1]
                table.insert(teacherRewards, {item = Item_Item.GetData(itemId), num = num})
            end
        end
        t.teacherRewards = teacherRewards
        local studentRewards = {}
        table.insert(studentRewards, {scorekey = EnumPlayScoreKey.QYJF, num = cultivate.TudiQingyi})
        table.insert(studentRewards, {scorekey = EnumPlayScoreKey.GIFT, num = cultivate.TudiGiftLimit})
        if cultivate.TudiItems then
            for j = 0, cultivate.TudiItems.Length - 1, 2 do
                local itemId, num = cultivate.TudiItems[j], cultivate.TudiItems[j + 1]
                table.insert(studentRewards, {item = Item_Item.GetData(itemId), num = num})
            end
        end
        t.studentRewards = studentRewards
    end
    table.sort(data,function (a,b)
        return a.cultivate.ID < b.cultivate.ID
    end)
    self:InitPlans(data)
end

function LuaShiTuTrainingPlanView:InitPlans(data)
    Extensions.RemoveAllChildren(self.m_Grid.transform)
    self.m_Plans = {}
    for i = 1, #data do
        local go = NGUITools.AddChild(self.m_Grid.gameObject, self.m_Template)
        go:SetActive(true)
        local plan = self:InitComponent(go)
        self:InitPlan(plan, data[i])
        table.insert(self.m_Plans, plan)
    end
    self.m_Grid:Reposition()
end

function LuaShiTuTrainingPlanView:InitComponent(go)
    local plan = {}
    plan.conditionLabel = go.transform:Find("Top/ConditionLabel"):GetComponent(typeof(UILabel))
    plan.slider = go.transform:Find("Top/Slider"):GetComponent(typeof(UISlider))
    plan.slider.value = 0
    plan.progressLabel = go.transform:Find("Top/Slider/ProgressLabel"):GetComponent(typeof(UILabel))
    plan.teacherGrid = go.transform:Find("Bottom/TeacherGrid"):GetComponent(typeof(UIGrid))
    plan.studentGrid = go.transform:Find("Bottom/StudentGrid"):GetComponent(typeof(UIGrid))
    plan.rewardTemplate = go.transform:Find("Bottom/RewardTemplate").gameObject
    plan.rewardTemplate:SetActive(false)
    plan.studentExtraRewardTemplate = go.transform:Find("Bottom/StudentExtraRewardTemplate").gameObject
    plan.studentExtraRewardTemplate:SetActive(false)
    plan.finishedLabel = go.transform:Find("Top/FinishedLabel"):GetComponent(typeof(UILabel))
    Extensions.RemoveAllChildren(plan.teacherGrid.transform)
    Extensions.RemoveAllChildren(plan.studentGrid.transform)
    return plan
end

function LuaShiTuTrainingPlanView:InitPlan(plan, data)
    plan.conditionLabel.text = SafeStringFormat3(data.cultivate.Description, data.cultivate.Num)
    local progressValue = (data.cultivate.Num >= 0) and (data.currentValue / data.cultivate.Num) or 0
    plan.slider.value = progressValue
    plan.finishedLabel.enabled = progressValue == 1
    plan.progressLabel.text = SafeStringFormat3("%d/%d",data.currentValue ,data.cultivate.Num)
    self:InitRewards(plan, data)
end

function LuaShiTuTrainingPlanView:InitRewards(plan, data)
    Extensions.RemoveAllChildren(plan.teacherGrid.transform)
    Extensions.RemoveAllChildren(plan.studentGrid.transform)
    for i = 1, #data.teacherRewards do
        self:InitReward(plan.teacherGrid, data.teacherRewards[i], plan)
    end
    for i = 1, #data.studentRewards do
        self:InitReward(plan.studentGrid, data.studentRewards[i], plan)
    end

    self:InitExtraReward(plan, data)

    plan.teacherGrid:Reposition()
    plan.studentGrid:Reposition()
end

function LuaShiTuTrainingPlanView:InitExtraReward(plan, data)
    local studentExtraReward = NGUITools.AddChild(plan.studentGrid.gameObject, plan.studentExtraRewardTemplate)
    studentExtraReward.transform:Find("SelectSprite").gameObject:SetActive(false)
    studentExtraReward:SetActive(true)
    local studentExtraRewardIcon = studentExtraReward.transform:Find("Reward"):GetComponent(typeof(CUITexture))
    local addRewardSprite = studentExtraReward.transform:Find("AddRewardSprite").gameObject
    local numlablel = studentExtraReward.transform:Find("NumLabel"):GetComponent(typeof(UILabel))
    numlablel.gameObject:SetActive(false)
    addRewardSprite:SetActive(false)
    studentExtraRewardIcon.gameObject:SetActive(true)
    if data.itemProp or (data.jade > 0) then
        numlablel.gameObject:SetActive(true)
        numlablel.text = (data.jade > 0) and data.jade or data.itemAmount
        numlablel.gameObject:SetActive(tonumber(numlablel.text) ~= 1)
        UIEventListener.Get(studentExtraReward).onClick = DelegateFactory.VoidDelegate(function()
            self:TakeOutItem(studentExtraReward, data)
        end)
        if data.icon then
            studentExtraRewardIcon:LoadMaterial(data.icon)
        else
            studentExtraRewardIcon:LoadMaterial("ui/texture/transparent/material/welfarewnd_lingyu.mat")
        end
    else
        addRewardSprite:SetActive(true)
        studentExtraRewardIcon.gameObject:SetActive(false)
        UIEventListener.Get(studentExtraReward).onClick = DelegateFactory.VoidDelegate(function()
            self:AddExtraReward(studentExtraReward,data)
        end)
    end
end

function LuaShiTuTrainingPlanView:InitReward(grid, data, plan)
    if (data.num == 0) and data.scorekey then return end
    local item = data.item
    local go = NGUITools.AddChild(grid.gameObject, plan.rewardTemplate)

    local icon = go:GetComponent(typeof(CUITexture))
    if item then icon:LoadMaterial(item.Icon) end
    if data.scorekey then icon:LoadMaterial(LuaShiTuTrainingHandbookMgr:GetPlayScore(data.scorekey).ScoreIcon) end

    local label = go.transform:Find("NumLabel"):GetComponent(typeof(UILabel))
    if data.num > 0 then
        label.text = data.num
		label.gameObject:SetActive(data.num > 1)
    else
        label.gameObject:SetActive(false)
    end
    if item and item.ID then
        UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function()
            CItemInfoMgr.ShowLinkItemTemplateInfo(item.ID, false, nil, CItemInfoMgr.AlignType.Default, 0, 0, 0, 0)
        end)
    end
    go:SetActive(true)
end

function LuaShiTuTrainingPlanView:TakeOutItem(go, data)
    if LuaShiTuTrainingHandbookMgr:IsShowForTeacher() then
        go.transform:Find("SelectSprite").gameObject:SetActive(true)
        local t = {}
        local itemdata = PopupMenuItemData(LocalString.GetString("取出道具"),DelegateFactory.Action_int(function (idx)
            MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("ShiTuTrainingPlan_TakeOutItem_Confirm"),DelegateFactory.Action(function()
                Gac2Gas.ShiTuCultivateShiFuAddItem(LuaShiTuTrainingHandbookMgr:GetTudiPlayerId(), data.cultivateId, 0, 0, 0,"")
            end),nil,LocalString.GetString("确认"),LocalString.GetString("取消"),false)
        end),false,nil)
        table.insert(t, itemdata)
        local array = Table2Array(t,MakeArrayClass(PopupMenuItemData))
        CPopupMenuInfoMgr.ShowPopupMenu(array, go.transform, CPopupMenuInfoMgr.AlignType.Left,1,DelegateFactory.Action(function()
            go.transform:Find("SelectSprite").gameObject:SetActive(false)
        end),600,true,240)
        return
    end
    if data.itemId and (not data.isItem) then
        CItemInfoMgr.ShowLinkItemInfo(data.itemId, false, nil, CItemInfoMgr.AlignType.Default, 0, 0, 0, 0)
    elseif data.templateId and data.isItem then
        CItemInfoMgr.ShowLinkItemTemplateInfo(data.templateId, false, nil, CItemInfoMgr.AlignType.Default, 0, 0, 0, 0)
    end
end

function LuaShiTuTrainingPlanView:AddExtraReward(go, data)
    if LuaShiTuTrainingHandbookMgr:IsShowForTeacher() then
        go.transform:Find("SelectSprite").gameObject:SetActive(true)
        local t = {}
        table.insert(t, PopupMenuItemData(LocalString.GetString("填充道具"),DelegateFactory.Action_int(function (idx)
            self:AddExtraItem(idx, data)
        end),false,nil))
        if not CommonDefs.IS_VN_CLIENT then
            table.insert(t, PopupMenuItemData(LocalString.GetString("填充灵玉"),DelegateFactory.Action_int(function (idx)
                self:AddExtraLingYu(idx, data)
            end),false,nil))
        end
        local array = Table2Array(t,MakeArrayClass(PopupMenuItemData))
        CPopupMenuInfoMgr.ShowPopupMenu(array, go.transform, CPopupMenuInfoMgr.AlignType.Left,1,DelegateFactory.Action(function( ... )
           go.transform:Find("SelectSprite").gameObject:SetActive(false)
        end),600,true,240)
        return
    end

    g_MessageMgr:ShowMessage("ShiTuTrainingPlan_Student_AddExtraReward_Remind")
end

function LuaShiTuTrainingPlanView:AddExtraItem(idx, data)
    LuaItemChooseWndMgr:ShowItemWithDetailMessageChooseWnd(self.m_CanBeFilledItemIds,function(itemId)
        self:ChooseItem(itemId, data)
    end, LocalString.GetString("选择填充道具"),LocalString.GetString("填充"), function()
        g_MessageMgr:ShowMessage("ShiTuTrainingPlan_ChooseItem_Explanation")
    end)
end

function LuaShiTuTrainingPlanView:AddExtraLingYu(idx, data)
    CLuaNumberInputMgr.ShowNumInputBox(0, 1000, 1, function (val)
        Gac2Gas.ShiTuCultivateShiFuAddItem(LuaShiTuTrainingHandbookMgr:GetTudiPlayerId(), data.cultivateId, val, EnumItemPlace.Bag, 0, "")
    end, LocalString.GetString("请输入填充的灵玉数量"), -1)
end

function LuaShiTuTrainingPlanView:ChooseItem(itemId, data)
    if itemId then
        local pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, itemId)
        Gac2Gas.ShiTuCultivateShiFuAddItem(LuaShiTuTrainingHandbookMgr:GetTudiPlayerId(), data.cultivateId, 0, EnumItemPlace.Bag, pos, itemId)
    else
        g_MessageMgr:ShowMessage("ShiTuTrainingPlan_ChooseItem_NoneItem")
    end
end
