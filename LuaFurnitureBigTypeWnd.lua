local COpenEntryMgr=import "L10.Game.COpenEntryMgr"
local UIRoot = import "UIRoot"
local CGuideMgr = import "L10.Game.Guide.CGuideMgr"
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local CClientFurnitureMgr = import "L10.Game.CClientFurnitureMgr"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"


CLuaFurnitureBigTypeWnd = class()
RegistClassMember(CLuaFurnitureBigTypeWnd,"m_TableView")
RegistClassMember(CLuaFurnitureBigTypeWnd,"m_TypeIds")
RegistClassMember(CLuaFurnitureBigTypeWnd,"m_IsInHouse")

function CLuaFurnitureBigTypeWnd:Awake()
    self.m_TypeIds = {}
    self.m_TableView = self.transform:Find("Anchor/Tab/TypeList"):GetComponent(typeof(QnTableView))
    local sceneType = CClientHouseMgr.Inst:GetCurSceneType()
    self.m_IsInHouse = sceneType == EnumHouseSceneType_lua.eXiangfang or sceneType == EnumHouseSceneType_lua.eRoom
    self.m_TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function() return #self.m_TypeIds end,
        function(item,row) 
            local typeId = self.m_TypeIds[row+1]
            if not CLuaHouseMgr.CheckSceneType(typeId,self.m_IsInHouse) then
                item.gameObject:SetActive(false)
                return
            end
            if not self.m_IsInHouse and typeId == EnumFurnitureType_lua.eWallPaper or not CLuaHouseMgr.s_WallPaperSwitchOn and typeId == EnumFurnitureType_lua.eWallPaper then
                item.gameObject:SetActive(false)
                return
            end
            if not CClientFurnitureMgr.s_PoolSwicthOn and typeId == EnumFurnitureType_lua.ePoolFurniture then
                item.gameObject:SetActive(false)
                return
            end
            local icon = Zhuangshiwu_TypeIcon.GetData(typeId)
            if icon == nil then
                return
            end
            local iconTexture = item.transform:Find("Icon"):GetComponent(typeof(CUITexture))
            iconTexture:LoadMaterial(icon.Icon)
            local nameLabel = item.transform:Find("Type/Label"):GetComponent(typeof(UILabel))
            nameLabel.text = CClientFurnitureMgr.GetTypeNameByType(typeId)
        end
    )

    self.m_TableView.OnSelectAtRow = DelegateFactory.Action_int(function(idx)
        g_ScriptEvent:BroadcastInLua("OnFurnitureTypeClicked",self.m_TypeIds[idx+1])
        CClientFurnitureMgr.Inst:ClearCurFurniture()
    end)

end

function CLuaFurnitureBigTypeWnd:OnEnable()
    g_ScriptEvent:AddListener("OnUseZuoanWnd",self,"OnUseZuoanWnd")
    g_ScriptEvent:AddListener("OnUseHuapenWnd",self,"OnUseHuapenWnd")
    g_ScriptEvent:AddListener("OnUseHouseRollerCoasterUseZhanTaiWnd",self,"OnUseHouseRollerCoasterUseZhanTaiWnd")
end
function CLuaFurnitureBigTypeWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnUseZuoanWnd",self,"OnUseZuoanWnd")
    g_ScriptEvent:RemoveListener("OnUseHuapenWnd",self,"OnUseHuapenWnd")
    g_ScriptEvent:RemoveListener("OnUseHouseRollerCoasterUseZhanTaiWnd",self,"OnUseHouseRollerCoasterUseZhanTaiWnd")
end

function CLuaFurnitureBigTypeWnd:OnUseZuoanWnd( )
    for i,v in ipairs(self.m_TypeIds) do
        if v==EnumFurnitureType_lua.eQinggong then
            self.m_TableView:SetSelectRow(i-1,true)
            break
        end
    end
end
function CLuaFurnitureBigTypeWnd:OnUseHuapenWnd( )
    for i,v in ipairs(self.m_TypeIds) do
        if v==EnumFurnitureType_lua.eHuamu then
            self.m_TableView:SetSelectRow(i-1,true)
            break
        end
    end
end
function CLuaFurnitureBigTypeWnd:OnUseHouseRollerCoasterUseZhanTaiWnd()
    for i,v in ipairs(self.m_TypeIds) do
        if v==EnumFurnitureType_lua.eXiaojing then
            CLuaFurnitureSmallTypeWnd.SetInitTheme(LocalString.GetString("夏趣"))
            self.m_TableView:SetSelectRow(i-1,true)
            break
        end
    end
end

function CLuaFurnitureBigTypeWnd:Init( )
    self:IPhoneXAdaptation()

    self.m_TypeIds = {}
    for i=1,CClientFurnitureMgr.MaxTypeCount do
        if not CClientFurnitureMgr.NeedShowBigType(i) then
        else
            table.insert( self.m_TypeIds,i )
        end
    end
    -- 加一个优先级排序
    table.sort(self.m_TypeIds,function (a,b)
        local info1 = Zhuangshiwu_TypeIcon.GetData(a)
        local info2 = Zhuangshiwu_TypeIcon.GetData(b)
        if not info1 or not info2 then return a < b end
        local p1 = info1.PriorityLevel
        local p2 = info2.PriorityLevel
        if p1 ~= p2 then
            return p1 < p2
        else
            return a < b
        end
    end)


    self.m_TableView:ReloadData(true,false)
    if CLuaGuideMgr.NeedWanShengJieSkyboxGuide() then
        local idx = nil
        for i,v in ipairs(self.m_TypeIds) do
            if v==17 then
                idx = i
                break
            end
        end
        if idx then
            self.m_TableView:SetSelectRow(idx-1,true)
            local triggered = CGuideMgr.Inst:StartGuide(EnumGuideKey.WanShengJieSkybox, 0)
            if triggered then
                COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.WanShengJieSkybox)
            end
        end
    else
        self.m_TableView:SetSelectRow(0,true)
    end
end

function CLuaFurnitureBigTypeWnd:IPhoneXAdaptation( )
    if UIRoot.EnableIPhoneXAdaptation then
        local w = CommonDefs.GetComponent_Component_Type(self.transform:Find("Anchor"), typeof(UIWidget))
        w.leftAnchor.absolute = -75 + math.floor(UIRoot.VirtualIPhoneXWidthMargin)
        w.rightAnchor.absolute = -75 + math.floor(UIRoot.VirtualIPhoneXWidthMargin)
        w:ResetAndUpdateAnchors()
    end
end
function CLuaFurnitureBigTypeWnd:GetGuideGo( methodName) 
    if methodName == "GetZuoJuType" then
        return self:GetZuoJuType()
    elseif methodName == "GetBuildingType" then
        return self:GetBuildingType()
    end
    return nil
end
function CLuaFurnitureBigTypeWnd:GetZuoJuType( )
    for i,v in ipairs(self.m_TypeIds) do
        if v==4 then
            return self.m_TableView:GetItemAtRow(i-1).gameObject
        end
    end
    return nil
end
function CLuaFurnitureBigTypeWnd:GetBuildingType( )
    if CGuideMgr.Inst:IsInPhase(EnumGuideKey.GetMiaoPu3) then
        --是否是主人 可能在别人家 在别人家的时候不中断引导
        if not CClientHouseMgr.Inst:IsCurHouseOwner() then
            return nil
        end
        if CClientFurnitureMgr.Inst:IsMiaopu3Placed() or not CClientFurnitureMgr.Inst:IsMiaopu3Exist() then
            CGuideMgr.Inst:EndCurrentPhase()
            return nil
        end
    end
    for i,v in ipairs(self.m_TypeIds) do
        if v==EnumFurnitureType_lua.eJianzhu then
            local ret = self.m_TableView:GetItemAtRow(i-1).gameObject
            CUICommonDef.SetFullyVisible(ret, self.m_TableView.m_ScrollView)
            return ret
        end
    end

    return nil
end

