-- Auto Generated!!
local AlignType = import "CPlayerInfoMgr+AlignType"
local CChatHelper = import "L10.UI.CChatHelper"
local CommonDefs = import "L10.Game.CommonDefs"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local CShiTuChooseShifuWnd = import "L10.UI.CShiTuChooseShifuWnd"
local CShiTuMgr = import "L10.Game.CShiTuMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUITexture = import "L10.UI.CUITexture"
local DelegateFactory = import "DelegateFactory"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumClass = import "L10.Game.EnumClass"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local Profession = import "L10.Game.Profession"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local UISprite = import "UISprite"
local Vector3 = import "UnityEngine.Vector3"
CShiTuChooseShifuWnd.m_setFunction_CS2LuaHook = function (this, node, data) 

    node:SetActive(true)
    CommonDefs.GetComponent_Component_Type(node.transform:Find("Name/text"), typeof(UILabel)).text = data.name
    CommonDefs.GetComponent_Component_Type(node.transform:Find("Name/JobIcon"), typeof(UISprite)).spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), (data._class)))
   
    local lvLabel = CommonDefs.GetComponent_Component_Type(node.transform:Find("lv"), typeof(UILabel))
    lvLabel.text = "lv." .. data.level
    lvLabel.color = data.hasFeiSheng and NGUIText.ParseColor24("fe7900", 0) or Color.white
    local iconbutton = node.transform:Find("icon/button").gameObject
    local button = node.transform:Find("button").gameObject

    UIEventListener.Get(iconbutton).onClick = DelegateFactory.VoidDelegate(function (p) 
        CPlayerInfoMgr.ShowPlayerPopupMenu(data.id, EnumPlayerInfoContext.ChatLink, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
    end)

    UIEventListener.Get(button).onClick = DelegateFactory.VoidDelegate(function (p) 
        Gac2Gas.RequestBaiShiToPlayer((data.id))
        --this.Close();
        CChatHelper.ShowFriendWnd(data.id, data.name)
    end)

    CommonDefs.GetComponent_Component_Type(node.transform:Find("icon"), typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(data._class, data.gender, -1), false)
end
CShiTuChooseShifuWnd.m_Init_CS2LuaHook = function (this) 

    UIEventListener.Get(this.closeBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        this:Close()
    end)

    if CShiTuMgr.Inst.ShiFuList.Count <= 0 then
        this:Close()
        return
    end

    local count = 0
    do
        local __itr_is_triger_return = nil
        local __itr_result = nil
        CommonDefs.ListIterateWithRet(CShiTuMgr.Inst.ShiFuList, DelegateFactory.SingleValIterFunc(function (___value) 
            local data = ___value
            if count >= this.nodeArray.Length then
                __itr_is_triger_return = true
                return true
            end
            this:setFunction(this.nodeArray[count], data)
            count = count + 1
        end))
        if __itr_is_triger_return == true then
            return
        end
    end
end
