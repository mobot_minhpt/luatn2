require("common/common_include")

local Profession = import "L10.Game.Profession"
local CUITexture = import "L10.UI.CUITexture"
local CUIFx = import "L10.UI.CUIFx"

CLuaWuCaiShaBingResultWnd = class()

function CLuaWuCaiShaBingResultWnd:Init()
	local playerGroupId = LuaWuCaiShaBingMgr.m_PlayerGroupId
	local winGroupId = LuaWuCaiShaBingMgr.m_WinGroupId
	local groupPlayerInfo = LuaWuCaiShaBingMgr.m_GroupPlayerInfo

	local teamNameTbl = {"RedTeam", "YellowTeam", "BlueTeam"}
	for groupId = 1, 3 do
		local teamName = teamNameTbl[groupId]
		local teamGo = self.transform:Find("Anchor/" .. teamName).gameObject
		local teamSprite = teamGo.transform:Find("Title/Sprite").gameObject
		local nameTransform = teamGo.transform:Find("Title/Label")
		local bg = teamGo.transform:Find("Bg"):GetComponent(typeof(UISprite))
		if groupId ~= playerGroupId then
			teamSprite:SetActive(false)
			nameTransform.localPosition = Vector3(0, 0, 0)
			bg.spriteName = "common_textbg_01_light"
		else
			teamSprite:SetActive(true)
			nameTransform.localPosition = Vector3(23, 0, 0)
			bg.spriteName = "common_select_01"
		end

		local shaBingName = teamGo.transform:Find("ShaBing/Label"):GetComponent(typeof(UILabel))
		local texture = teamGo.transform:Find("Texture"):GetComponent(typeof(CUITexture))
		local fxNode = teamGo.transform:Find("FxNode"):GetComponent(typeof(CUIFx))
		if groupId == winGroupId then
			shaBingName.text = LocalString.GetString("美味沙冰")
			texture:LoadMaterial("UI/Texture/Transparent/Material/wucaishabing_2.mat", false, nil)
			fxNode:LoadFx(CLuaUIFxPaths.MeiWeiShaBingFx)
		else
			shaBingName.text = LocalString.GetString("黑暗沙冰")
			texture:LoadMaterial("UI/Texture/Transparent/Material/wucaishabing_1.mat", false, nil)
			fxNode:LoadFx(CLuaUIFxPaths.HeiAnShaBingFx)
		end

		local playerInfoTbl = groupPlayerInfo[groupId]
		for idx = 1, 3 do
			local classSprite = teamGo.transform:Find("PlayerInfo" .. tostring(idx) .. "/Class"):GetComponent(typeof(UISprite))
			local nameLabel = teamGo.transform:Find("PlayerInfo" .. tostring(idx) .. "/Name"):GetComponent(typeof(UILabel))

			local i = (idx - 1) * 3 + 1
			if playerInfoTbl[i] and playerInfoTbl[i + 1] and playerInfoTbl[i + 2] then
				classSprite.spriteName = Profession.GetIconByNumber(playerInfoTbl[i])
				if playerInfoTbl[i + 2] == CClientMainPlayer.Inst.Id then
					nameLabel.text = SafeStringFormat3("[63FF8B]%s[-]", playerInfoTbl[i + 1])
				else
					nameLabel.text = SafeStringFormat3("[FFFFFF]%s[-]", playerInfoTbl[i + 1])
				end
				classSprite.gameObject:SetActive(true)
				nameLabel.gameObject:SetActive(true)
			else
				classSprite.gameObject:SetActive(false)
				nameLabel.gameObject:SetActive(false)
			end
		end
	end

	local detailBtn = self.transform:Find("Anchor/DetailButton").gameObject
	CommonDefs.AddOnClickListener(detailBtn, DelegateFactory.Action_GameObject(function()
		Gac2Gas.RequestQueryWCSBBattleInfo()
	end), false)
end

return CLuaWuCaiShaBingResultWnd