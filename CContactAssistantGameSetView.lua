-- Auto Generated!!
local CContactAssistantGameSetView = import "L10.UI.CContactAssistantGameSetView"
local CContactAssistantMgr = import "L10.Game.CContactAssistantMgr"
local ENUM_AVATAR_RESULT = import "ENUM_AVATAR_RESULT"
local LocalString = import "LocalString"
local NativeHandle = import "NativeHandle"
local UIInput = import "UIInput"
CContactAssistantGameSetView.m_SetDefaultValue_CS2LuaHook = function (this, titlePrefix, title, content, extraInfo) 
    this.m_DefaultTitlePrefix = titlePrefix
    this.m_DefaultTitleForSubmission = title
    this.m_DefaultContentForSubmission = content
    this.m_DefaultExtraInfoForSubmission = extraInfo
end
CContactAssistantGameSetView.m_init_CS2LuaHook = function (this) 
    --titleLabel.text = "";
    --titleLabel.text = "";
    CommonDefs.GetComponent_Component_Type(this.titleLabel.parent, typeof(UIInput)).value = this.m_DefaultTitleForSubmission
    --desLabel.text = "";
    CommonDefs.GetComponent_Component_Type(this.desLabel.parent, typeof(UIInput)).value = this.m_DefaultContentForSubmission
    this.nameLabel.text = LocalString.GetString("非必填")
    CommonDefs.GetComponent_Component_Type(this.nameLabel.parent, typeof(UIInput)).value = ""
    this.phoneLabel.text = LocalString.GetString("非必填")
    CommonDefs.GetComponent_Component_Type(this.phoneLabel.parent, typeof(UIInput)).value = ""

    this.m_Native = CreateFromClass(NativeHandle)
    this.m_texture = nil
    this.uploadPicTexture.mainTexture = nil
end
CContactAssistantGameSetView.m_OnAvaterCallBack_CS2LuaHook = function (this, strResult) 
    if (strResult == ToStringWrap(ENUM_AVATAR_RESULT.eResult_Success)) then
        -- 成功
        this:StartCoroutine(this:LoadTexture("upload_image.png"))
    elseif (strResult == ToStringWrap(ENUM_AVATAR_RESULT.eResult_Cancel)) then
        -- 取消
    elseif (strResult == ToStringWrap(ENUM_AVATAR_RESULT.eResult_Failed)) then
        -- 失败
    end
end
CContactAssistantGameSetView.m_SubmitInfoToGM_CS2LuaHook = function (this, go) 
    local titleText = StringTrim(this.titleLabel.text)
    local desText = StringTrim(this.desLabel.text)
    if not System.String.IsNullOrEmpty(this.m_DefaultTitlePrefix) then
        titleText = this.m_DefaultTitlePrefix .. titleText
    end
    if not System.String.IsNullOrEmpty(this.m_DefaultExtraInfoForSubmission) then
        desText = System.String.Format("{0}{1}{2}", desText, CContactAssistantMgr.JingLingQuickSubmitToGMExtraInfoPrefix, this.m_DefaultExtraInfoForSubmission)
    end
    CContactAssistantMgr.Inst:SubmitInfoToGM(titleText, desText, this.nameLabel.text, this.phoneLabel.text, this.m_texture)
end
