local UITabBar = import "L10.UI.UITabBar"
local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local Profession = import "L10.Game.Profession"
local EnumClass = import "L10.Game.EnumClass"

LuaGuardShiMenRankWnd = class()

RegistChildComponent(LuaGuardShiMenRankWnd, "Tabs", "Tabs", UITabBar)
RegistChildComponent(LuaGuardShiMenRankWnd, "MyRankLabel", "MyRankLabel", UILabel)
RegistChildComponent(LuaGuardShiMenRankWnd, "MyNameLabel", "MyNameLabel", UILabel)
RegistChildComponent(LuaGuardShiMenRankWnd, "MyClassLabel", "MyClassLabel", UILabel)
RegistChildComponent(LuaGuardShiMenRankWnd, "MyScoreLabel", "MyScoreLabel", UILabel)
RegistChildComponent(LuaGuardShiMenRankWnd, "TableView", "TableView", QnAdvanceGridView)
RegistChildComponent(LuaGuardShiMenRankWnd, "NoneRankLabel", "NoneRankLabel", UILabel)
RegistChildComponent(LuaGuardShiMenRankWnd, "MyRankImage", "MyRankImage", UISprite)

RegistClassMember(LuaGuardShiMenRankWnd, "m_RankData")
RegistClassMember(LuaGuardShiMenRankWnd, "m_MyRank")
RegistClassMember(LuaGuardShiMenRankWnd, "m_RankSpriteName")
RegistClassMember(LuaGuardShiMenRankWnd, "m_ClassNameTable")
RegistClassMember(LuaGuardShiMenRankWnd, "m_TabIndex")

function LuaGuardShiMenRankWnd:Awake()
	self.Tabs.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
	    self:OnTabsTabChange(index)
	end)
	self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return self:NumberOfRows()
        end,
        function(item, index)
            return self:ItemAt(item,index)
        end)
    local callback = DelegateFactory.Action_int(function(row) self:OnSelectAtRow(row) end)
	self.TableView.OnSelectAtRow = callback
end

function LuaGuardShiMenRankWnd:Init()
	self.m_RankSpriteName = {"Rank_No.1","Rank_No.2png","Rank_No.3png"}
	self.m_ClassNameTable = {}
	Initialization_Init.Foreach(function(k,v)
        if v.Status == 0 then
            self.m_ClassNameTable[v.Class] = Profession.GetFullName(CommonDefs.ConvertIntToEnum(typeof(EnumClass), v.Class))
        end
    end)
	local arr ={69,89,109,129,999}
	for index, level in pairs(arr) do
		if CClientMainPlayer.Inst and CClientMainPlayer.Inst.MaxLevel <= level then
			self.m_TabIndex = index - 1
			self.Tabs:ChangeTab(index - 1, false)
			break
		end
	end
	for index, rankData in pairs(LuaZongMenMgr.m_SMSWRankData) do
		for _,data in pairs(rankData) do
			if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == data.PlayerId then
				self.m_TabIndex = index - 1
				self.Tabs:ChangeTab(index - 1, false)
				break
			end
		end
	end
end

--@region UIEvent

function LuaGuardShiMenRankWnd:OnTabsTabChange(index)
	self.m_RankData = LuaZongMenMgr.m_SMSWRankData[index + 1]
	self.NoneRankLabel.gameObject:SetActive(self:NumberOfRows() == 0)
	table.sort(self.m_RankData, function(a, b) 
		return a.Rank < b.Rank
	end)
	self.TableView:ReloadData(true, true)
	self.m_MyRank = nil
	for _,data in pairs(self.m_RankData) do
		if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == data.PlayerId then
			self.m_MyRank = data
			break
		end
	end
	self.MyRankLabel.text = self.m_MyRank and self.m_MyRank.Rank > 0 and self.m_MyRank.Rank or (self.m_TabIndex == index and LocalString.GetString("未上榜") or "")
	self.MyNameLabel.text = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Name
	self.MyClassLabel.text = self.m_ClassNameTable[CClientMainPlayer.Inst and EnumToInt(CClientMainPlayer.Inst.Class) or 0]
	self.MyScoreLabel.text = math.floor(self.m_MyRank and self.m_MyRank.Score or LuaZongMenMgr.m_SMSW_MyRankScore)
	self.MyRankImage.spriteName = self.m_RankSpriteName[self.m_MyRank and self.m_MyRank.Rank or 0]
end

function LuaGuardShiMenRankWnd:NumberOfRows()
	return self.m_RankData and #self.m_RankData or 0
end

function LuaGuardShiMenRankWnd:ItemAt(item,index)
	if not self.m_RankData then return end
	local info = self.m_RankData[index+1]
	self:InitItem(item.transform,info)
	local default = index % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite
    item:SetBackgroundTexture(default)
end

function LuaGuardShiMenRankWnd:OnSelectAtRow(row)

end
--@endregion

function LuaGuardShiMenRankWnd:InitItem(transform, info)
	transform:Find("RankLabel"):GetComponent(typeof(UILabel)).text = info.Rank
	transform:Find("RankLabel/RankImage"):GetComponent(typeof(UISprite)).spriteName = self.m_RankSpriteName[info.Rank]
	transform:Find("NameLabel"):GetComponent(typeof(UILabel)).text = info.Name
	transform:Find("ScoreLabel"):GetComponent(typeof(UILabel)).text = math.floor(info.Score)
	transform:Find("ClassLabel"):GetComponent(typeof(UILabel)).text = self.m_ClassNameTable[info.Class]
end