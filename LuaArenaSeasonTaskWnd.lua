local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local EnumChineseDigits = import "L10.Game.EnumChineseDigits"

LuaArenaSeasonTaskWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaArenaSeasonTaskWnd, "tabTableView", "TabTableView", QnAdvanceGridView)
RegistChildComponent(LuaArenaSeasonTaskWnd, "tipButton", "TipButton", GameObject)
RegistChildComponent(LuaArenaSeasonTaskWnd, "progress_Label", "Progress", UILabel)
RegistChildComponent(LuaArenaSeasonTaskWnd, "progress_Grid", "Grid", UIGrid)
RegistChildComponent(LuaArenaSeasonTaskWnd, "progress_Template", "Template", GameObject)
RegistChildComponent(LuaArenaSeasonTaskWnd, "desc", "Desc", UILabel)
RegistChildComponent(LuaArenaSeasonTaskWnd, "startButton", "StartButton", GameObject)
RegistChildComponent(LuaArenaSeasonTaskWnd, "taskGrid", "TaskGrid", UIGrid)
RegistChildComponent(LuaArenaSeasonTaskWnd, "taskTemplate", "TaskTemplate", GameObject)
RegistChildComponent(LuaArenaSeasonTaskWnd, "activedWeekTip", "ActivedWeekTip", UILabel)
RegistChildComponent(LuaArenaSeasonTaskWnd, "activeState", "Select", GameObject)

--@endregion RegistChildComponent end

RegistClassMember(LuaArenaSeasonTaskWnd, "openedWeek")
RegistClassMember(LuaArenaSeasonTaskWnd, "currentWeek")
RegistClassMember(LuaArenaSeasonTaskWnd, "activedWeek")
RegistClassMember(LuaArenaSeasonTaskWnd, "finishedWeeks")
RegistClassMember(LuaArenaSeasonTaskWnd, "weekStarDict")

RegistClassMember(LuaArenaSeasonTaskWnd, "starAppearTick")
RegistClassMember(LuaArenaSeasonTaskWnd, "appearedRewardGo")
RegistClassMember(LuaArenaSeasonTaskWnd, "starData")

function LuaArenaSeasonTaskWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.tipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)

	UIEventListener.Get(self.startButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnStartButtonClick()
	end)

    --@endregion EventBind end
	self.progress_Template:SetActive(false)
	self.taskTemplate:SetActive(false)
	self.activeState:SetActive(false)
	self.activedWeekTip.text = ""
	self.progress_Label.text = ""

	UIEventListener.Get(self.transform:Find("Anchor/ActiveState").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnActiveButtonClick()
	end)
end

function LuaArenaSeasonTaskWnd:OnEnable()
    g_ScriptEvent:AddListener("SendArenaTaskInfo", self, "OnSendArenaTaskInfo")
    g_ScriptEvent:AddListener("SetArenaActiveTaskWeekResult", self, "OnSetArenaActiveTaskWeekResult")
end

function LuaArenaSeasonTaskWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendArenaTaskInfo", self, "OnSendArenaTaskInfo")
    g_ScriptEvent:RemoveListener("SetArenaActiveTaskWeekResult", self, "OnSetArenaActiveTaskWeekResult")
end

function LuaArenaSeasonTaskWnd:OnSendArenaTaskInfo(currentWeek, openedWeek, activedWeek, starData, taskData, finishedWeekData)
	self.activedWeek = activedWeek
	self.openedWeek = openedWeek
	finishedWeekData = g_MessagePack.unpack(finishedWeekData)
	self.finishedWeeks = {}
	for _, week in pairs(finishedWeekData) do
		self.finishedWeeks[week] = true
	end
	self.tabTableView:ReloadData(false, true)

	if activedWeek > 0 then
		self.activedWeekTip.text = SafeStringFormat3(LocalString.GetString("当前[FFFF00]第%s周[-]生效"), EnumChineseDigits.GetDigit(activedWeek))
	else
		self.activedWeekTip.text = ""
	end
	if self.currentWeek == 0 then
		self.currentWeek = currentWeek
		self.tabTableView:ReloadData(false, false)
		self.tabTableView:SetSelectRow(currentWeek - 1, true)

		if self.openedWeek > 5 then
			self.tabTableView:ScrollToRow(currentWeek - 1)
		end
	end
	if self.currentWeek == currentWeek then
		self.currentWeek = currentWeek
		self.activeState:SetActive(currentWeek == activedWeek)

		self:UpdateProgress(g_MessagePack.unpack(starData))
		self:UpdateTask(g_MessagePack.unpack(taskData))
	end
end

function LuaArenaSeasonTaskWnd:OnSetArenaActiveTaskWeekResult(activedWeek)
	self.activedWeek = activedWeek
	self.activeState:SetActive(self.currentWeek == activedWeek)
	if activedWeek > 0 then
		self.activedWeekTip.text = SafeStringFormat3(LocalString.GetString("当前[FFFF00]第%s周[-]生效"), EnumChineseDigits.GetDigit(activedWeek))
		g_ScriptEvent:BroadcastInLua("ArenaTaskDataUpdate", activedWeek, self.starData)
	else
		self.activedWeekTip.text = ""
	end
end

function LuaArenaSeasonTaskWnd:UpdateProgress(starData)
	self.starData = starData
	self:ClearTick()

	local currentStar = starData.currentStar
	local totalStar = starData.totalStar
	self.progress_Label.text = SafeStringFormat3(LocalString.GetString("完成进度 %d/%d"), currentStar, totalStar)
	self:AddChild(self.progress_Grid.gameObject, self.progress_Template, totalStar)
	local weekStar = self.weekStarDict[self.currentWeek] or {}
	for i = 1, totalStar do
		local child = self.progress_Grid.transform:GetChild(i - 1)
		local normal = child:Find("Normal")
		local highlight = child:Find("Highlight")
		local scale = weekStar[i] and 1.2 or 1
		if i > currentStar then
			normal.gameObject:SetActive(true)
			highlight.gameObject:SetActive(false)
			LuaUtils.SetLocalScale(normal, scale, scale, 1)
		else
			normal.gameObject:SetActive(false)
			highlight.gameObject:SetActive(true)
			LuaUtils.SetLocalScale(highlight, scale, scale, 1)
		end

		local rewardLabel = child:Find("Reward"):GetComponent(typeof(UILabel))
		local sprite = child:Find("Reward/Sprite"):GetComponent(typeof(UIWidget))
		if weekStar[i] then
			rewardLabel.text = weekStar[i]
			sprite:ResetAndUpdateAnchors()
		end
		rewardLabel.gameObject:SetActive(weekStar[i] and i > currentStar)

		if weekStar[i] and i <= currentStar then
			UIEventListener.Get(highlight.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
				self:OnBigStarClick(rewardLabel.gameObject)
			end)
		else
			UIEventListener.Get(highlight.gameObject).onClick = nil
		end
	end
	self.progress_Grid:Reposition()
end

function LuaArenaSeasonTaskWnd:UpdateTask(taskData)
	local taskTbl = {}
	for pos, data in pairs(taskData) do
		table.insert(taskTbl, {
			pos = pos,
			id = data.id,
			value = data.value,
			stage = data.stage,
			star = data.star,
			isFinished = data.isFinished,
		})
	end
	table.sort(taskTbl, function (a, b)
		return a.pos < b.pos
	end)

	self:AddChild(self.taskGrid.gameObject, self.taskTemplate, #taskTbl)
	for i = 1, #taskTbl do
		local data = taskTbl[i]
		local designData = Arena_Task.GetData(data.id)
		local child = self.taskGrid.transform:GetChild(i - 1)
		child:Find("Name"):GetComponent(typeof(UILabel)).text = designData.Name
		local grid = child:Find("Grid")
		for j = 1, 3 do
			local star = grid:GetChild(j - 1)
			star:Find("Highlight").gameObject:SetActive(j <= data.star)
			star:Find("Normal").gameObject:SetActive(j > data.star)
		end

		local progressLabel = child:Find("Progress"):GetComponent(typeof(UILabel))
		local str = ""
		local total = 0
		if data.stage == 1 then
			total = designData.Num1
		elseif data.stage == 2 then
			total = designData.Num2
		elseif data.stage == 3 then
			total = designData.Num3
		end
		if not data.isFinished then
			str = SafeStringFormat3(LocalString.GetString("（%d/%d）"), data.value, total)
		end
		progressLabel.text = SafeStringFormat3("%s%s", string.gsub(designData.Desc, "%%s", tostring(total)), str)

		local slider = child:Find("Slider"):GetComponent(typeof(UISlider))
		slider.gameObject:SetActive(not data.isFinished)
		if not data.isFinished then
			slider.value = math.min(1, data.value / total)
		end
	end
	self.taskGrid:Reposition()
end

function LuaArenaSeasonTaskWnd:Init()
	self.activedWeek = 0
	self:InitTabTableView()
	self.desc.text = g_MessageMgr:FormatMessage("ARENA_TASK_DESC")

	self.weekStarDict = LuaArenaMgr:GetWeekStarDict()
end

function LuaArenaSeasonTaskWnd:InitTabTableView()
	self.currentWeek = 0
	self.openedWeek = 0
    self.tabTableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return self.openedWeek
        end,
        function(item, index)
            self:InitTabItem(item, index)
        end
    )

	self.tabTableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
		if self.currentWeek ~= row + 1 then
			self.currentWeek = row + 1
			Gac2Gas.QueryArenaTaskInfo(self.currentWeek)
		end
	end)
	Gac2Gas.QueryArenaTaskInfo(self.currentWeek)
end

function LuaArenaSeasonTaskWnd:InitTabItem(item, index)
	item.Text = SafeStringFormat3(LocalString.GetString("第%s周"), EnumChineseDigits.GetDigit(index + 1))
	if self.activedWeek == index + 1 then
		item.m_NormalSpirte = "common_tab_02_normal_yellow"
		item.m_HighLightSprite = "common_tab_02_highlight_yellow"
		item.m_NormalFontColor = NGUIText.ParseColor32("FFFFFFFF", 0)
		item.m_HighLightFontColor = NGUIText.ParseColor32("FFFFFFFF", 0)
	else
		item.m_NormalSpirte = "common_tab_02_normal"
		item.m_HighLightSprite = "common_tab_02_highlight"
		item.m_NormalFontColor = NGUIText.ParseColor32("B2E7F4FF", 0)
		item.m_HighLightFontColor = NGUIText.ParseColor32("0E3254C5", 0)
	end
	local isFinished = self.finishedWeeks[index + 1] or false
	LuaUtils.SetLocalPositionX(item.m_Label.transform, isFinished and -15 or 0)
	local isSelected = self.currentWeek == index + 1
	item.transform:Find("CheckMark").gameObject:SetActive(isFinished and isSelected)
	item.transform:Find("CheckMark_bg").gameObject:SetActive(isFinished and not isSelected)
end

function LuaArenaSeasonTaskWnd:AddChild(parent, template, num)
    local childCount = parent.transform.childCount
    if childCount < num then
        for i = childCount + 1, num do
            NGUITools.AddChild(parent, template)
        end
    end

    childCount = parent.transform.childCount
    for i = 0, childCount - 1 do
        parent.transform:GetChild(i).gameObject:SetActive(i < num)
    end
end

function LuaArenaSeasonTaskWnd:ClearTick()
	if self.appearedRewardGo then
		self.appearedRewardGo:SetActive(false)
		self.appearedRewardGo = nil
	end
	if self.starAppearTick then
		UnRegisterTick(self.starAppearTick)
		self.starAppearTick = nil
	end
end

function LuaArenaSeasonTaskWnd:OnDestroy()
	self:ClearTick()
end

--@region UIEvent

function LuaArenaSeasonTaskWnd:OnTipButtonClick()
	g_MessageMgr:ShowMessage("ARENA_TASK_TIP")
end

function LuaArenaSeasonTaskWnd:OnStartButtonClick()
	if self.currentWeek == 0 then return end

	local action = function()
		g_ScriptEvent:BroadcastInLua("ArenaEnterModeSelect")
		CUIManager.CloseUI(CLuaUIResources.ArenaSeasonTaskWnd)
	end

	if not self.finishedWeeks[self.currentWeek] and self.currentWeek ~= self.activedWeek then
		g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("ARENA_TASK_START_CONFIRM", self.currentWeek, self.activedWeek), function ()
			Gac2Gas.RequestArenaSetActiveWeekTask(self.currentWeek, true)
			action()
		end, function()
			action()
		end, LocalString.GetString("生效"), LocalString.GetString("不生效"), false)
	else
		action()
	end
end

function LuaArenaSeasonTaskWnd:OnActiveButtonClick()
	if self.currentWeek > 0 and self.currentWeek ~= self.activedWeek then
		Gac2Gas.RequestArenaSetActiveWeekTask(self.currentWeek, true)
	end
end

function LuaArenaSeasonTaskWnd:OnBigStarClick(rewardGO)
	if self.appearedRewardGo == rewardGO then return end
	self:ClearTick()

	self.appearedRewardGo = rewardGO
	rewardGO:SetActive(true)
	self.starAppearTick = RegisterTickOnce(function()
		self:ClearTick()
	end, 5000)
end

--@endregion UIEvent

