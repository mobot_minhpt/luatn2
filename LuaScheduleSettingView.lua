local QnTableView = import "L10.UI.QnTableView"
local PlayerPrefs = import "UnityEngine.PlayerPrefs"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local CLocalPushMgr = import "L10.Game.CLocalPushMgr"

LuaScheduleSettingView = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaScheduleSettingView, "QnTableView", "QnTableView", QnTableView)

--@endregion RegistChildComponent end
RegistClassMember(LuaScheduleSettingView,"m_List")

function LuaScheduleSettingView:Awake()
    self.QnTableView.m_DataSource = DefaultTableViewDataSource.Create(
        function() 
            return self.m_List and #self.m_List or 0
        end,
        function(item, index)
            self:ItemAt(item,index)
        end
    )
    self:Init()
end

function LuaScheduleSettingView:Init()
    if not CClientMainPlayer.Inst then return end
    self.m_List = {}
    Message_Push.Foreach(function (k, v) 
        if v.MinLevel <= CClientMainPlayer.Inst.Level then
            table.insert(self.m_List, {pushId = k, index = v.SortIndex})
        else
            --未达到等级的都设置为不推送
            PlayerPrefs.SetInt(v.name, 0)
        end
    end)
    table.sort(self.m_List,function (a,b)
        return a.index < b.index
    end)
    self.QnTableView:ReloadData(false, false)
end

function LuaScheduleSettingView:ItemAt(item,index)
    local pushId = self.m_List[index + 1].pushId
    local data = Message_Push.GetData(pushId)

    local activityNameLabel = item.transform:Find("ActivityNameLabel"):GetComponent(typeof(UILabel))
    local repeatTimeLabel = item.transform:Find("RepeatTimeLabel"):GetComponent(typeof(UILabel))
    local timeLabel = item.transform:Find("TimeLabel"):GetComponent(typeof(UILabel))
    local activityModeLabel = item.transform:Find("ActivityModeLabel"):GetComponent(typeof(UILabel))
    local qnSelectableButton = item.transform:Find("QnSelectableButton"):GetComponent(typeof(QnSelectableButton))

    local pushName = data.name
    qnSelectableButton.OnButtonSelected = DelegateFactory.Action_bool(function (selected)
	    self:OnSelected(pushName, selected)
	end)
    local enablePush = PlayerPrefs.GetInt( pushName, data.DefaultOpen) > 0
    activityNameLabel.text = data.ActivityName
    repeatTimeLabel.text = data.ActivityCycle
    timeLabel.text = data.ActivityTime
    activityModeLabel.text = data.ActivityMode
    qnSelectableButton:SetSelected(enablePush, true)
end

function LuaScheduleSettingView:OnSelected(pushName, selected)
    PlayerPrefs.SetInt(pushName, selected and 1 or 0)
    CLocalPushMgr.StartLocalPushService();
end

--@region UIEvent

--@endregion UIEvent

