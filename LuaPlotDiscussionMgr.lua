local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local CWebBrowserMgr = import "L10.Game.CWebBrowserMgr"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local AlignType2 = import "CPlayerInfoMgr+AlignType"
local EChatPanel = import "L10.Game.EChatPanel"
local EPropStatus = import "L10.Game.EPropStatus"
local CStatusMgr = import "L10.Game.CStatusMgr"

LuaPlotDiscussionMgr = {}
LuaPlotDiscussionMgr.m_Title = ""
LuaPlotDiscussionMgr.m_CardRes = ""
LuaPlotDiscussionMgr.m_CardId = 0
LuaPlotDiscussionMgr.m_AchievementName = ""
LuaPlotDiscussionMgr.m_HaoYiXingData = nil
LuaPlotDiscussionMgr.m_YaoGuiHuData = nil
LuaPlotDiscussionMgr.m_IsLike = false
LuaPlotDiscussionMgr.m_LikeNum = 0
LuaPlotDiscussionMgr.m_CommentsData = {}
LuaPlotDiscussionMgr.m_NewCommentsData = {}
LuaPlotDiscussionMgr.m_ShowAlertIds = {}
LuaPlotDiscussionMgr.m_IsOpen = true --开关
LuaPlotDiscussionMgr.m_TickDict = {}
-- LuaPlotDiscussionMgr.m_BaseUrl = "http://192.168.131.156:88/"
LuaPlotDiscussionMgr.m_CardId2Like = {}
LuaPlotDiscussionMgr.m_CardId2LikeNum = {}
LuaPlotDiscussionMgr.m_NewSendCommentDataInfo = {}
LuaPlotDiscussionMgr.m_ShowCardWndType = 1
LuaPlotDiscussionMgr.m_ShowCardOwnerTexture = false
LuaPlotDiscussionMgr.m_ShowSuiPianTexture = false
LuaPlotDiscussionMgr.m_ShowCardOwnerTextureLocalPositionZ = 0
LuaPlotDiscussionMgr.m_IsEnable = false --网站组是否支持
LuaPlotDiscussionMgr.m_CacheEnableTime = 0

function LuaPlotDiscussionMgr:OnMainPlayerCreated()
    self.m_CacheEnableTime = 0
end

function LuaPlotDiscussionMgr:IsOpen()
    return self.m_IsOpen and CClientMainPlayer.Inst and CClientMainPlayer.Inst.MaxLevel >= 8 and self.m_IsEnable
end

function LuaPlotDiscussionMgr:GetBaseUrl()
    if CommonDefs.IS_HMT_CLIENT then
        if CommonDefs.IS_PUB_RELEASE then
            return "http://l10hmtxxak-island.tms.easebar.com/"
        else
            return "http://35.221.207.238:81/"
        end
    end
    return CWebBrowserMgr.s_ApiUrl
end

function LuaPlotDiscussionMgr:ShowHaoYiXingPlotDiscussion(t,isLike,likeNum,showOwnerTexture,showSuiPianTexture)
    self.m_Title = t.tcgdata.Name
    self.m_CardRes = t.tcgdata.Res
    self.m_CardId = t.tcgdata.ID
    self.m_AchievementName = ""
    self.m_HaoYiXingData = t
    self.m_YaoGuiHuData = nil
    self.m_IsLike = isLike
    self.m_LikeNum = likeNum
    self.m_ShowCardOwnerTexture = showOwnerTexture
    self.m_ShowSuiPianTexture = showSuiPianTexture
    self.m_ShowCardOwnerTextureLocalPositionZ = 0
    CUIManager.CloseUI(CLuaUIResources.PlotDiscussionWnd)
    CUIManager.ShowUI(CLuaUIResources.PlotDiscussionWnd)
end

function LuaPlotDiscussionMgr:ShowYaoGuiHuPlotDiscussion(design,isLike,likeNum,showCardOwnerTextureLocalPositionZ)
    self.m_Title = design.Name
    self.m_CardRes = design.Picture
    local groupId = design.GroupID
    self.m_CardId = groupId
    local achievement = Achievement_Achievement.GetDataBySubKey("Group", groupId)
    if achievement then
        self.m_AchievementName = achievement.Name
    end
    self.m_HaoYiXingData = nil
    self.m_YaoGuiHuData = design
    self.m_IsLike = isLike
    self.m_LikeNum = likeNum
    self.m_ShowCardOwnerTexture = true
    self.m_ShowSuiPianTexture = false
    self.m_ShowCardOwnerTextureLocalPositionZ = showCardOwnerTextureLocalPositionZ
    CUIManager.CloseUI(CLuaUIResources.PlotDiscussionWnd)
    CUIManager.ShowUI(CLuaUIResources.PlotDiscussionWnd)
end

function LuaPlotDiscussionMgr:ShowSendPlotDiscussionWnd(sortBy, pageSize)
    self.m_NewSendCommentDataInfo = {sortBy = sortBy,pageSize = pageSize}
    CUIManager.ShowUI(CLuaUIResources.SendPlotDiscussionWnd)
end

function LuaPlotDiscussionMgr:PushData(url, dataTable, backFunction)
    if not self:IsOpen() then
        return 
    end
    LuaPersonalSpaceMgrReal.PushData(url, dataTable, backFunction)
end

function LuaPlotDiscussionMgr:CheckIsEnable()
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.MaxLevel >= 8 and CPersonalSpaceMgr.OpenPersonalSpace and CClientMainPlayer.Inst and (CStatusMgr.Inst:GetStatus(CClientMainPlayer.Inst, EPropStatus.GetId("Isolation")) ~= 1) and not cs_string.IsNullOrEmpty(CPersonalSpaceMgr.Inst.Token)  then
        if self.m_IsEnable and self.m_CacheEnableTime > CServerTimeMgr.Inst.timeStamp then
            g_ScriptEvent:BroadcastInLua("OnPlotDiscussionEnable")
            return
        end
        local url = self:GetBaseUrl() .. "qnm/card/is_enable"
        local dataTable = {}
        LuaPersonalSpaceMgrReal.PushData(url, dataTable, function (tData)
            self.m_IsEnable = false 
            if tData.code == 0 and tData.data and tData.data.isEnable then
                self.m_IsEnable = true
                self.m_CacheEnableTime = CServerTimeMgr.Inst.timeStamp + 600
                g_ScriptEvent:BroadcastInLua("OnPlotDiscussionEnable")
            end
        end)
    end
end

function LuaPlotDiscussionMgr:CancelTick(tickKey)
    local tick = self.m_TickDict[tickKey]
    if tick then
        UnRegisterTick(tick)
        self.m_TickDict[tickKey] = nil
        tick = nil
    end
end

function LuaPlotDiscussionMgr:StartTick(func,duration,tickKey)
    if not self:IsOpen() then
        return 
    end
    func()
    self:CancelTick(tickKey)
    local tick = RegisterTick(function ()
        if not CUIManager.IsLoaded(CLuaUIResources.ZhuXianAchievementWnd) and 
            not CUIManager.IsLoaded(CLuaUIResources.HaoYiXingCardsWnd) and
            not CUIManager.IsLoaded(CLuaUIResources.PlotCardsWnd) and 
            not CUIManager.IsLoaded(CLuaUIResources.PlotDiscussionWnd) and
            not CUIManager.IsLoaded(CUIResources.MainPlayerWnd) and
            not CUIManager.IsLoaded(CLuaUIResources.ScheduleWnd) then
            self:CancelTick(tickKey)
            return
        end
        func()
    end,duration)
    self.m_TickDict[tickKey] = tick
end

function LuaPlotDiscussionMgr:RequestRedDotInfo()
    local dataTable = {}
    self:StartTick(function (tData)
        self:PushData(self:GetBaseUrl() .. "qnm/card/red_dot", dataTable, function (tData)
            self:OnRedDotInfo(tData)
        end)
    end,30000,"RedDotInfo")
end

function LuaPlotDiscussionMgr:OnRedDotInfo(tData)
    self.m_ShowAlertIds = {}
    --print("LuaPlotDiscussionMgr:OnRedDotInfo", tData)
    g_ScriptEvent:BroadcastInLua("OnPlotCommentShowRedAlert",0, 0,true)
    if tData and tData.code and tData.code == 0 and tData.data and #tData.data > 0 then
        --print("LuaPlotDiscussionMgr:OnRedDotInfo Success", tData)
        for i = 1,#tData.data do
            local id = tData.data[i]
            self.m_ShowAlertIds[id] = true
            --print(id > 10000 and id or 0, id < 10000 and id or 0)
            g_ScriptEvent:BroadcastInLua("OnPlotCommentShowRedAlert",id > 10000 and id or 0, id < 10000  and id or 0,false)
        end
    end
end

function LuaPlotDiscussionMgr:RequestCardsLikeInfo(cardIdsList)
    self.m_CardId2Like = {}
    self.m_CardId2LikeNum = {}
    local dataTable = {}
    local card_ids = ""
    for i,id in pairs(cardIdsList) do
        if i == 1 then
            card_ids = tostring(id)
        else
            card_ids = SafeStringFormat3(LocalString.GetString("%s,%d"), card_ids, id)
        end
    end
    dataTable.card_ids = card_ids
    self:PushData(self:GetBaseUrl() .. "qnm/card/list", dataTable, function (tData)
        self:OnRequestCardsLikeInfo(tData)
    end)
end

function LuaPlotDiscussionMgr:OnRequestCardsLikeInfo(tData)
    --print("LuaPlotDiscussionMgr:OnRequestCardsLikeInfo", tData)
    if tData and tData.code and tData.code == 0 and tData.data then
        --print("LuaPlotDiscussionMgr:OnRequestCardsLikeInfo Success")
        local newCardId2Like = {}
        local newCardId2LikeNum = {}
        for i = 1,#tData.data do
            local data = tData.data[i]
            local id = data.cardId
            newCardId2Like[id] = data.isLiked
            newCardId2LikeNum[id] = data.likeCount
        end
        self.m_IsLike = newCardId2Like[self.m_CardId]
        self.m_LikeNum = newCardId2LikeNum[self.m_CardId]
        self.m_CardId2Like = newCardId2Like
        self.m_CardId2LikeNum = newCardId2LikeNum
        g_ScriptEvent:BroadcastInLua("OnPlotRequestCardsLikeInfo")
    end
end

function LuaPlotDiscussionMgr:RequestGiveCardThumbsUp(cardId)
    --print("RequestGiveCardThumbsUp")
    local dataTable = {}
    dataTable.card_id = cardId
    self:PushData(self:GetBaseUrl() .. "qnm/card/like", dataTable, function (tData)
        self:OnRequestGiveCardThumbsUp(tData,cardId)
    end)
end

function LuaPlotDiscussionMgr:OnRequestGiveCardThumbsUp(tData, cardId)
    --print("LuaPlotDiscussionMgr:OnRequestGiveCardThumbsUp", tData, cardId)
    if tData and tData.code and tData.code == 0 and tData.data and tData.data.isOk then
        --print("LuaPlotDiscussionMgr:OnRequestGiveCardThumbsUp Success")
        self.m_CardId2Like[cardId] = true
        g_ScriptEvent:BroadcastInLua("OnPlotDiscussionCardFavorited",cardId, true)
    end
end

function LuaPlotDiscussionMgr:RequestCancelGiveCardThumbsUp(cardId)
    --print("RequestCancelGiveCardThumbsUp")
    local dataTable = {}
    dataTable.card_id = cardId
    self:PushData(self:GetBaseUrl() .. "qnm/card/cancel_like", dataTable, function (tData)
        self:OnRequestCancelGiveCardThumbsUp(tData,cardId)
    end)
end

function LuaPlotDiscussionMgr:OnRequestCancelGiveCardThumbsUp(tData, cardId)
    --print("LuaPlotDiscussionMgr:OnRequestCancelGiveCardThumbsUp", tData, cardId)
    if tData and tData.code and tData.code == 0 and tData.data and tData.data.isOk then
        --print("LuaPlotDiscussionMgr:OnRequestCancelGiveCardThumbsUp Success")
        self.m_CardId2Like[cardId] = false
        g_ScriptEvent:BroadcastInLua("OnPlotDiscussionCardFavorited",cardId, false)
    end
end

function LuaPlotDiscussionMgr:RequestCardCommentsList(cardId, sortBy, page, pageSize)
    --print(cardId)
    local dataTable = {}
    dataTable.card_id = cardId
    dataTable.sort_by = sortBy
    dataTable.page = page
    dataTable.pageSize = pageSize
    self:PushData(self:GetBaseUrl() .. "qnm/card/notion/list", dataTable, function (tData)
        self:OnRequestCardCommentsList(tData, page)
    end)
end

function LuaPlotDiscussionMgr:OnRequestCardCommentsList(tData, page)
    --print("LuaPlotDiscussionMgr:OnRequestCardCommentsList", tData, page)
    if tData and tData.code and tData.code == 0 and tData.data then
        self.m_IsLike = tData.data.card.isLiked
        self.m_LikeNum = tData.data.card.likeCount
        local commentCount = tData.data.count
        self.m_CommentsData = {}
        for i = 1,#tData.data.list do
            local data = tData.data.list[i]
            --print(i,data.roleId,data.clazz,data.gender,data.roleName)
            local t = {
                id = data.id, class = data.clazz, gender = data.gender, playerName = data.roleName,
                lv = data.grade, msg = data.text, playerId = data.roleId, hasFeiSheng = data.xianfanstatus > 0,
                isMyFavor = data.isLiked, createTime = data.createTime, favorPlayInfo = {}, msgs = {},
                likeCount = data.likeCount, messageNum = data.commentCount
            }
            --print(t.id,t.class,t.gender,t.playerName,t.lv,t.msg,t.playerId,t.hasFeiSheng,t.isMyFavor,t.createTime,t.likeCount,t.messageNum)

            table.insert(self.m_CommentsData,t)
        end
        ---print("LuaPlotDiscussionMgr:OnRequestCardCommentsList Success",self.m_IsLike,self.m_LikeNum,commentCount,#tData.data.list)
        g_ScriptEvent:BroadcastInLua("OnPlotCardCommentsList",commentCount, page)
    end
end

function LuaPlotDiscussionMgr:RequestCardCommentLikeUsers(commentId)
    local dataTable = {}
    --print(commentId)
    dataTable.notionId = commentId
    self:PushData(self:GetBaseUrl() .. "qnm/card/notion/show", dataTable, function (tData)
        self:OnRequestCardCommentLikeUsers(tData,commentId)
    end)
end

function LuaPlotDiscussionMgr:OnRequestCardCommentLikeUsers(tData,commentId)
    --print("LuaPlotDiscussionMgr:OnRequestCardCommentLikeUsers", tData,commentId)
    if tData and tData.code and tData.code == 0 and tData.data then
        --print("LuaPlotDiscussionMgr:OnRequestCardCommentLikeUsers Success")
        for i = 1,#self.m_CommentsData  do
            local data = self.m_CommentsData[i]
            if data.id == commentId then
                self.m_CommentsData[i].favorPlayInfo = {}
                --print(#tData.data.likeUsers)
                for j = 1,#tData.data.likeUsers do
                    local likeUserData = tData.data.likeUsers[j]
                    --print(likeUserData.roleId, likeUserData.roleName)
                    table.insert(self.m_CommentsData[i].favorPlayInfo,{playerId = likeUserData.roleId, playerName = likeUserData.roleName})
                end
                --print(#self.m_CommentsData[i].favorPlayInfo)
                g_ScriptEvent:BroadcastInLua("OnPlotCommentFavorPlayInfoUpdate", commentId, tData.data.isLiked, tData.data.likeCount)
                break
            end
        end
    end
end

function LuaPlotDiscussionMgr:RequestDeleteCardComment(commentId)
    local dataTable = {}
    dataTable.notionId = commentId
    self:PushData(self:GetBaseUrl() .. "qnm/card/notion/del", dataTable, function (tData)
        self:OnRequestDeleteCardComment(tData,commentId)
    end)
end

function LuaPlotDiscussionMgr:OnRequestDeleteCardComment(tData, commentId)
    --print("LuaPlotDiscussionMgr:OnRequestDeleteCardComment", tData, commentId,tData.code)
    if tData and tData.code and tData.code == 0 and tData.data and tData.data.isOk then
        --print("LuaPlotDiscussionMgr:OnRequestDeleteCardComment Success")
        g_ScriptEvent:BroadcastInLua("OnPlotDeleteCardComment")
    end
end

function LuaPlotDiscussionMgr:RequestAddCardComment(cardId, text)
    --print(text)
    local dataTable = {}
    dataTable.card_id = cardId
    dataTable.text = text
    self:PushData(self:GetBaseUrl() .. "qnm/card/notion/add", dataTable, function (tData)
        self:OnRequestAddCardComment(tData)
    end)
end

function LuaPlotDiscussionMgr:OnRequestAddCardComment(tData)
    --print("LuaPlotDiscussionMgr:OnRequestAddCardComment", tData)
    if tData and tData.code and tData.code == 0 and tData.data then
        local id = tData.data.id
        --print("LuaPlotDiscussionMgr:OnRequestAddCardComment Success", id)
        g_ScriptEvent:BroadcastInLua("OnPlotAddCardComment")
    end
end

function LuaPlotDiscussionMgr:RequestCommentDetailMessages(commentId, page, pageSize)
    local dataTable = {}
    dataTable.notionId = commentId
    dataTable.page = page
    dataTable.pageSize = pageSize
    --print(commentId, page, pageSize)
    self:PushData(self:GetBaseUrl() .. "qnm/card/notion/comment/list", dataTable, function (tData)
        self:OnRequestCommentDetailMessages(tData,commentId, page)
    end)
end

function LuaPlotDiscussionMgr:OnRequestCommentDetailMessages(tData, commentId, page)
    --print("LuaPlotDiscussionMgr:OnRequestCommentDetailMessages", tData, commentId, page)
    if tData and tData.code and tData.code == 0 and tData.data then
        --print("LuaPlotDiscussionMgr:OnRequestCommentDetailMessages Success")
        for i = 1,#self.m_CommentsData  do
            local data = self.m_CommentsData[i]
            --print(data.id , commentId,data.id == commentId,#tData.data.list)
            if data.id == commentId then
                self.m_CommentsData[i].msgs = {}
                for j = 1,#tData.data.list do
                    local msgdata = tData.data.list[j]
                    local index = page + j - 1
                    --print(msgdata.replyRoleId,msgdata.replyRoleName)
                    self.m_CommentsData[i].msgs[index] = {
                        id = msgdata.id, msg = msgdata.text, playerName = msgdata.roleName, playerId = msgdata.roleId,
                        replyPlayerName = msgdata.replyRoleName, replyPlayerId = msgdata.replyRoleId
                    }
                end
                self.m_CommentsData[i].messageNum = #tData.data.list
                g_ScriptEvent:BroadcastInLua("OnPlotCommentMessagesUpdate",commentId)
                break
            end
        end
    end
end

function LuaPlotDiscussionMgr:RequestAddCommentDetailMessage(commentId, text, replayCommentMessageId)
    --print(commentId, text, replayCommentMessageId)
    local dataTable = {}
    dataTable.notionId = commentId
    dataTable.text = text
    if replayCommentMessageId then
        dataTable.replyCommentId = replayCommentMessageId
    end
    self:PushData(self:GetBaseUrl() .. "qnm/card/notion/comment/add", dataTable, function (tData)
        self:OnRequestAddCommentDetailMessage(tData,commentId)
    end)
end

function LuaPlotDiscussionMgr:OnRequestAddCommentDetailMessage(tData,commentId)
    --print("LuaPlotDiscussionMgr:OnRequestAddCommentDetailMessage",tData,commentId,tData.code)
    if tData and tData.code and tData.code == 0 then
        --print("LuaPlotDiscussionMgr:OnRequestAddCommentDetailMessage Success")
        local msgsCount = 1
        for i = 1,#self.m_CommentsData  do
            local data = self.m_CommentsData[i]
            if data.id == commentId then
                msgsCount = #self.m_CommentsData[i].msgs
                break
            end
        end
        g_ScriptEvent:BroadcastInLua("OnPlotAddCommentDetailMessage",commentId, msgsCount * 2)
    end
end

function LuaPlotDiscussionMgr:RequestDeleteCommentDetailMessage(commentId, notionId)
    local dataTable = {}
    dataTable.commentId = commentId
    self:PushData(self:GetBaseUrl() .. "qnm/card/notion/comment/del", dataTable, function (tData)
        self:OnRequestDeleteCommentDetailMessage(tData,commentId, notionId)
    end)
end

function LuaPlotDiscussionMgr:OnRequestDeleteCommentDetailMessage(tData,commentId, notionId)
    --print("LuaPlotDiscussionMgr:OnRequestDeleteCommentDetailMessage", tData, commentId,tData.code,tData.data)
    if tData and tData.code and tData.code == 0 and tData.data and tData.data.isOk then
        --print("LuaPlotDiscussionMgr:OnRequestDeleteCommentDetailMessage Success")
        g_ScriptEvent:BroadcastInLua("OnPlotDeleteCommentDetailMessage",notionId)
    end
end

function LuaPlotDiscussionMgr:RequestGiveCommentThumbsUp(commentId)
    --print("RequestGiveCommentThumbsUp")
    local dataTable = {}
    dataTable.notionId = commentId
    self:PushData(self:GetBaseUrl() .. "qnm/card/notion/like", dataTable, function (tData)
        self:OnPlotRequestGiveCommentThumbsUp(tData,commentId)
    end)
end

function LuaPlotDiscussionMgr:OnPlotRequestGiveCommentThumbsUp(tData,commentId)
    --print("LuaPlotDiscussionMgr:OnPlotRequestGiveCommentThumbsUp", tData, commentId,tData.code,tData.data)
    if tData and tData.code and tData.code == 0 and tData.data and tData.data.isOk then
        --print("LuaPlotDiscussionMgr:OnPlotRequestGiveCommentThumbsUp Success")
        for i = 1,#LuaPlotDiscussionMgr.m_CommentsData do
            local t = LuaPlotDiscussionMgr.m_CommentsData[i]
            if t.id ==  commentId then
                LuaPlotDiscussionMgr.m_CommentsData[i].isMyFavor = true
                break
            end
        end
        g_ScriptEvent:BroadcastInLua("OnPlotRequestGiveCommentThumbsUp",commentId, true)
    end
end

function LuaPlotDiscussionMgr:CancelRequestGiveCommentThumbsUp(commentId)
    --print("CancelRequestGiveCommentThumbsUp")
    local dataTable = {}
    dataTable.notionId = commentId
    self:PushData(self:GetBaseUrl() .. "qnm/card/notion/cancel_like", dataTable, function (tData)
        self:OnCancelRequestGiveCommentThumbsUp(tData,commentId)
    end)
end

function LuaPlotDiscussionMgr:OnCancelRequestGiveCommentThumbsUp(tData,commentId)
    --print("LuaPlotDiscussionMgr:OnCancelRequestGiveCommentThumbsUp", tData, commentId)
    if tData and tData.code and tData.code == 0 and tData.data and tData.data.isOk then
        --print("LuaPlotDiscussionMgr:OnCancelRequestGiveCommentThumbsUp Success")
        for i = 1,#LuaPlotDiscussionMgr.m_CommentsData do
            local t = LuaPlotDiscussionMgr.m_CommentsData[i]
            if t.id ==  commentId then
                LuaPlotDiscussionMgr.m_CommentsData[i].isMyFavor = false
                break
            end
        end
        g_ScriptEvent:BroadcastInLua("OnPlotRequestGiveCommentThumbsUp",commentId, false)
    end
end

function LuaPlotDiscussionMgr:RequestDelAllNewNotification()
    local dataTable = {}
    dataTable.card_id  = self.m_CardId
    self:PushData(self:GetBaseUrl() .. "qnm/card/notification/del_all", dataTable, function (tData)
        self:OnRequestDelAllNewNotification(tData)
    end)
end

function LuaPlotDiscussionMgr:OnRequestDelAllNewNotification(tData)
    --print("LuaPlotDiscussionMgr:OnRequestReadAllCards", tData,tData.code,tData.data)
    if tData and tData.code and tData.code == 0 and tData.data and tData.data.isOk then
        --print("LuaPlotDiscussionMgr:OnRequestReadAllCards Success")
        self.m_ShowAlertIds[self.m_CardId] = false
        local dataTable = {}
        self:PushData(self:GetBaseUrl() .. "qnm/card/red_dot", dataTable, function (tData)
            self:OnRedDotInfo(tData)
        end)
        g_ScriptEvent:BroadcastInLua("OnPlotRequestReadAllCards")
    end
end

function LuaPlotDiscussionMgr:RequestForNotificationNum()
    local dataTable = {}
    dataTable.card_id = self.m_CardId
    --print(self.m_CardId)
    if not CClientMainPlayer.Inst then return end
    self:PushData(self:GetBaseUrl() .. "qnm/card/notification/new_num", dataTable, function (tData)
        self:OnRequestForNotificationNum(tData)
    end)
end

function LuaPlotDiscussionMgr:OnRequestForNotificationNum(tData)
    --print(tData,tData.code,tData.data)
    if tData.code == 0 and tData.data then
        --print(tData.data.count)
        g_ScriptEvent:BroadcastInLua("OnPlotNotificationNum",tData.data.count)
    end
end

function LuaPlotDiscussionMgr:RequestAllNewNotification()
    --print( self.m_CardId)
    local dataTable = {}
    --dataTable.type = 0
    dataTable.page = 1
    dataTable.pageSize = 40
    dataTable.card_id = self.m_CardId
    self:PushData(self:GetBaseUrl() .. "qnm/card/notification/list", dataTable, function (tData)
        self:OnRequestAllNewNotification(tData)
    end)
end

function LuaPlotDiscussionMgr:OnRequestAllNewNotification(tData)
    --print("LuaPlotDiscussionMgr:OnRequestAllNotification", tData,tData.code)
    if tData and tData.code and tData.code == 0 and tData.data and tData.data.list then
        self.m_NewCommentsData = {}
        --print(#tData.data.list)
        for i = 1,#tData.data.list do
            local data = tData.data.list[i]
            --print(data.type)
            if data.notionComment then
                --print(data.notionComment.text)
            end
            if data.notion then
                --print(data.notion.text)
            end
            table.insert(self.m_NewCommentsData,{
                class = data.notifier.clazz, gender = data.notifier.gender, playerName = data.notifier.roleName,id = data.notion and data.notion.id or 0,commentId = data.notionComment and data.notionComment.id or 0,
                lv = data.notifier.grade, hasFeiSheng = data.notifier.xianfanstatus > 0,isFavor = data.type == 1,roleId = data.notifier.roleId,
                msg = data.text,comment = (data.type == 3 and data.notionComment) and data.notionComment.text or (data.notion and data.notion.text or "")
            })
        end
        --print("LuaPlotDiscussionMgr:OnRequestAllNotification Success",tData.data.card.cardId , self.m_CardId,#self.m_NewCommentsData)
        g_ScriptEvent:BroadcastInLua("OnPlotAllNewNotification")
        self.m_ShowAlertIds[self.m_CardId] = false
        local dataTable = {}
        self:PushData(self:GetBaseUrl() .. "qnm/card/red_dot", dataTable, function (tData)
            self:OnRedDotInfo(tData)
        end)
        g_ScriptEvent:BroadcastInLua("OnPlotRequestReadAllCards")
    end
end

function LuaPlotDiscussionMgr:RequestComplaint(targetId, reason)
    local dataTable = {}
    dataTable.targetid = targetId
    local commentId = self.m_ComplaintInfo.commentId
    dataTable.resouce_type = commentId == 0 and "card_notion" or "card_notion_comment"
    dataTable.resouce_id = commentId == 0 and self.m_ComplaintInfo.notionId or commentId
    dataTable.content = self.m_ComplaintInfo.content
    dataTable.reason = reason
    self:PushData(self:GetBaseUrl() .. "qnm/complain/add", dataTable, function (tData)
        g_MessageMgr:ShowMessage("Report_Player_Success")		
    end)
end

LuaPlotDiscussionMgr.m_ComplaintInfo = {}
function LuaPlotDiscussionMgr:ShowComplaintPopupMenu(playerId, notionId, commentId, content)
    self.m_ComplaintInfo = {notionId = notionId, commentId = commentId, content = content}
	CPlayerInfoMgr.ShowPlayerPopupMenu(playerId, EnumPlayerInfoContext.PlotDiscussion, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType2.Default)
end
