local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CJingLingMgr = import "L10.Game.CJingLingMgr"

LuaJingLingErrorCorrectionWnd = class()

RegistClassMember(LuaJingLingErrorCorrectionWnd,"questionInput")
RegistClassMember(LuaJingLingErrorCorrectionWnd,"correctionInput")
RegistClassMember(LuaJingLingErrorCorrectionWnd,"submitBtn")
RegistClassMember(LuaJingLingErrorCorrectionWnd,"cancelBtn")

RegistClassMember(LuaJingLingErrorCorrectionWnd,"maxLength")

function LuaJingLingErrorCorrectionWnd:Awake()
    self.maxLength = 85
	self.questionInput = self.transform:Find("Anchor/QuestionInput"):GetComponent(typeof(UIInput))
	self.correctionInput = self.transform:Find("Anchor/CorrectionInput"):GetComponent(typeof(UIInput))
	self.submitBtn = self.transform:Find("Anchor/OKButton").gameObject
	self.cancelBtn = self.transform:Find("Anchor/CancelButton").gameObject
end

function LuaJingLingErrorCorrectionWnd:Init()
    self.questionInput.characterLimit = self.maxLength
    self.correctionInput.characterLimit = self.maxLength
    self.questionInput.value = CJingLingMgr.Inst.RelatedErrorCorrectionMsg.relatedQuestion
    self.correctionInput.value = ""
    UIEventListener.Get(self.submitBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnSubmitBtnClick()
    end)
    UIEventListener.Get(self.cancelBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnCancelBtnClick()
    end)
end

--@region UIEvent
function LuaJingLingErrorCorrectionWnd:OnSubmitBtnClick()
    local question = StringTrim(self.questionInput.value)
    local correction = StringTrim(self.correctionInput.value)
    if System.String.IsNullOrEmpty(question) then
        g_MessageMgr:ShowMessage("JINGLING_JIUCUO_NO_TITLE")
        return
    end
    if System.String.IsNullOrEmpty(correction) then
        g_MessageMgr:ShowMessage("JINGLING_JIUCUO_NO_CONTENT")
        return
    end
    Gac2Gas.RequestLogCorrectJingLing(question, correction, CJingLingMgr.Inst.RelatedErrorCorrectionMsg.QuestionKey)
    CUIManager.CloseUI(CUIResources.JingLingErrorCorrectionWnd)
end

function LuaJingLingErrorCorrectionWnd:OnCancelBtnClick()
    CUIManager.CloseUI(CUIResources.JingLingErrorCorrectionWnd)
end
--@endregion UIEvent

