require("3rdParty/ScriptEvent")
require("common/common_include")

local Gac2Gas = import "L10.Game.Gac2Gas"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local Color = import "UnityEngine.Color"
local UICamera = import "UICamera"
local Physics = import "UnityEngine.Physics"
local Vector3 = import "UnityEngine.Vector3"
local CUIFx = import "L10.UI.CUIFx"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local UIRoot = import "UIRoot"
local CUITexture = import "L10.UI.CUITexture"
local UITable = import "UITable"
local UIScrollView = import "UIScrollView"
local CButton = import "L10.UI.CButton"
local Extensions = import "Extensions"
local MessageWndManager = import "L10.UI.MessageWndManager"
local UIDragScrollView = import "UIDragScrollView"
local CItemMgr = import "L10.Game.CItemMgr"


CLuaZhouNianQingPintuWnd=class()
RegistClassMember(CLuaZhouNianQingPintuWnd,"ShardParentDict")
RegistClassMember(CLuaZhouNianQingPintuWnd,"ShardGrid")
RegistClassMember(CLuaZhouNianQingPintuWnd,"shardPrefab")
RegistClassMember(CLuaZhouNianQingPintuWnd,"TargetParent")
RegistClassMember(CLuaZhouNianQingPintuWnd,"HintLabel")
RegistClassMember(CLuaZhouNianQingPintuWnd,"CloseBtn")
RegistClassMember(CLuaZhouNianQingPintuWnd,"Finished")
RegistClassMember(CLuaZhouNianQingPintuWnd,"DragGrid")
RegistClassMember(CLuaZhouNianQingPintuWnd,"DragTable")
RegistClassMember(CLuaZhouNianQingPintuWnd,"DragMoveNode")
RegistClassMember(CLuaZhouNianQingPintuWnd,"StartBtn")
RegistClassMember(CLuaZhouNianQingPintuWnd,"EmptyNode")
RegistClassMember(CLuaZhouNianQingPintuWnd,"FxNode")

RegistClassMember(CLuaZhouNianQingPintuWnd,"m_GuildPinTuInfo")
RegistClassMember(CLuaZhouNianQingPintuWnd,"m_PinTuItems")
RegistClassMember(CLuaZhouNianQingPintuWnd,"m_PackagePinTuList")

function CLuaZhouNianQingPintuWnd:Awake()
	local onCloseClick = function(go)
		CUIManager.CloseUI(CUIResources.ZhouNianQingPintuWnd)
	end
	CommonDefs.AddOnClickListener(self.CloseBtn,DelegateFactory.Action_GameObject(onCloseClick),false)
end

function CLuaZhouNianQingPintuWnd:Init()
	self.Finished = false
	self.HintLabel:SetActive(false)
	self.shardPrefab:SetActive(false)
	self.DragGrid:SetActive(false)
	self.DragMoveNode:SetActive(false)
	self.ShardParentDict={}
	self.m_GuildPinTuInfo={}
	self.m_PinTuItems={}
	self.m_PackagePinTuList={}
	
	self.m_GuildPinTuInfo = LuaGetGlobal("ZhouNianQing_PinTuInfo")
	local setting = ZhouNianQing_GuildParty.GetData()
	local pintuMaxNum = setting.FragmentNum
	
	self:GetPackagePintuItem()
	self:showShards()

	if self:GetFilledPinTu() == pintuMaxNum then
		self.StartBtn:GetComponent(typeof(CButton)).Enabled = true
		local onSubmitClick = function(go)
			MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("ZNQ_PINTU_OPEN_CONFIRM"), DelegateFactory.Action(function () 
				Gac2Gas.GuildPartyRequestOpenPlay()
			end), nil, nil, nil, false)
		end
		CommonDefs.AddOnClickListener(self.StartBtn,DelegateFactory.Action_GameObject(onSubmitClick),false)
		self.FxNode:GetComponent(typeof(CUIFx)):LoadFx(CUIFxPaths.NianshouPintuFx)
	else
		self.StartBtn:GetComponent(typeof(CButton)).Enabled = false
	end
end

function CLuaZhouNianQingPintuWnd:showShards()
	
	local trans = self.TargetParent.transform
	
	local dict = {}
	if trans.childCount ~= #self.m_PinTuItems then
		CUIManager.CloseUI(CUIResources.ZhouNianQingPintuWnd)
		return
	end
	
	for i =1, #self.m_PinTuItems do
		local parent = trans:GetChild(i-1)
		Extensions.RemoveAllChildren(parent)
		local go = self:generateShard(parent.gameObject, self.m_PinTuItems[i], i)
	end
	
	
	local tableParent = self.DragTable
	Extensions.RemoveAllChildren(tableParent.transform)
	for i = 1, #self.m_PackagePinTuList do
		local go = self:generateDrag(tableParent, self.m_PackagePinTuList[i], i)
	end
	
	if #self.m_PackagePinTuList == 0 then
		self.EmptyNode:SetActive(true)
	else
		self.EmptyNode:SetActive(false)
	end
	
	self.DragTable:GetComponent(typeof(UITable)):Reposition()
	self.DragTable.transform.parent:GetComponent(typeof(UIScrollView)):ResetPosition()
	
end

function CLuaZhouNianQingPintuWnd:generateShard(fatherNode, itemInfo, index)
	local go = CUICommonDef.AddChild(fatherNode,self.shardPrefab)
	go.name = tostring(itemInfo.ID)
	go:SetActive(true)
	local cuitexture = go:GetComponent(typeof(CUITexture))
	cuitexture:LoadMaterial(itemInfo.Icon,false,nil)
	
	local exist = false
	if self.m_GuildPinTuInfo[index-1] == 1 then
		exist = true
	end
	
	if not exist then
		cuitexture.color = Color(1,1,1,0.2)
	end
	
	return go
end

local deltaTrigger = 10
local totalMove = 0
local goNodeScale = 0.8
local goNodeDragScale = 1
local finishSign = false

function CLuaZhouNianQingPintuWnd:generateDrag(fatherNode, itemInfo, index)
	local parent = fatherNode
	local goFather = CUICommonDef.AddChild(parent,self.DragGrid)
	goFather.name = tostring(index)
	goFather:SetActive(true)
	local go = goFather.transform:Find("node").gameObject
	go.transform.localScale = Vector3(goNodeScale,goNodeScale,1)
	local cuitexture = go:GetComponent(typeof(CUITexture))
	cuitexture:LoadMaterial(itemInfo.Icon,false,nil)
	
	local startMove = function(_info)
		if not self.DragMoveNode.activeSelf then
			self.DragMoveNode:SetActive(true)
			local cuitexture = self.DragMoveNode:GetComponent(typeof(CUITexture))
			cuitexture:LoadMaterial(_info.Icon,false,nil)
			self.DragMoveNode.transform.position = go.transform.position
			--go:SetActive(false)
			local cuitexture = go:GetComponent(typeof(CUITexture))
			cuitexture.color = Color(1,1,1,0)
			local dragScrollView = go:GetComponent(typeof(UIDragScrollView))
			dragScrollView.enabled = false
		end
	end
	
	local endMove = function()
		totalMove = 0
		if self.DragMoveNode.activeSelf then
			self.DragMoveNode:SetActive(false)
			local cuitexture = go:GetComponent(typeof(CUITexture))
			cuitexture.color = Color(1,1,1,1)
			local dragScrollView = go:GetComponent(typeof(UIDragScrollView))
			dragScrollView.enabled = true
		end
	end
	
	local onDrag = function(go,delta)
		if self.Finished then 
			return 
		end
		
		totalMove = totalMove + delta.x
		if totalMove > deltaTrigger then
			startMove(itemInfo)
			local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject);
			
			local _trans = go.transform
			local _pos = _trans.localPosition
			_pos.x = _pos.x + delta.x*scale
			_pos.y = _pos.y + delta.y*scale
			_trans.localPosition = _pos
			
			local trans = self.DragMoveNode.transform
			trans.position = _trans.position
		end
	end
	local onPress = function(go,flag)
		endMove()
		if self.Finished then 
			return
		end
		if not flag then
			local currentPos = UICamera.currentTouch.pos
			local ray = UICamera.currentCamera:ScreenPointToRay(Vector3(currentPos.x,currentPos.y,0))
			local hits = Physics.RaycastAll(ray, 99999,  UICamera.currentCamera.cullingMask)
			
			for i =0,hits.Length -1 do
				if hits[i].collider.gameObject.transform.parent and hits[i].collider.gameObject.transform.parent.parent == self.TargetParent.transform  then
					self:judgeFinish(itemInfo.ID, hits[i].collider.gameObject, go)
					--self.HintLabel:SetActive(false)
					--return
				end
			end
			if not finishSign then
				go.transform.localPosition = Vector3.zero
			end
			--self:judgeEmpty()
		end
	end
	CommonDefs.AddOnDragListener(go,DelegateFactory.Action_GameObject_Vector2(onDrag),false)
	CommonDefs.AddOnPressListener(go,DelegateFactory.Action_GameObject_bool(onPress),false)	
	return goFather
end

function CLuaZhouNianQingPintuWnd:judgeEmpty()
	local notempty = false
	for i = 0,self.TargetParent.transform.childCount-1 do
		local trans = self.TargetParent.transform:GetChild(i)
		if trans.childCount > 0 then
			notempty = true
			break
		end
	end
	--self.HintLabel:SetActive(not notempty)
end

function CLuaZhouNianQingPintuWnd:judgeFinish(templateId, node, moveNode)
	if tonumber(node.name) ~= templateId then
		--moveNode.transform.localPosition = Vector3.zero
		finishSign = false
		return
	end
	local cuitexture = node:GetComponent(typeof(CUITexture))
	if cuitexture.color.a == 1 then
		--moveNode.transform.localPosition = Vector3.zero
		g_MessageMgr:ShowMessage("PINTU_FULL_MESSAGE")
		finishSign = false
		return
	end
	finishSign = true
	moveNode:SetActive(false)
	self.Finished=true
	Gac2Gas.GuildPartyRequestCommitFragment(EnumItemPlace_lua.Bag,CLuaZhouNianQingMgr.GetPackagePintuPos(templateId))
end


-- 初始化拼图所需itemId，玩家所拥有的拼图item
function CLuaZhouNianQingPintuWnd:GetPackagePintuItem()
	-- body
	local setting = ZhouNianQing_GuildParty.GetData()
	local pintuStr = setting.FragmentList
	local ids = g_LuaUtil:StrSplit(pintuStr, ",")
	for k, v in ipairs(ids) do
		local itemTId = tonumber(v)
		local item = CItemMgr.Inst:GetItemTemplate(itemTId)
		table.insert(self.m_PinTuItems, item)
		local count = CItemMgr.Inst:GetItemCount(itemTId)
		if count > 0 then
			table.insert(self.m_PackagePinTuList, item)
		end
	end
end

-- 获得已经填满的拼图数量
function CLuaZhouNianQingPintuWnd:GetFilledPinTu()
	local count = 0

	if self.m_GuildPinTuInfo then
		for i=0, self.m_GuildPinTuInfo.Count-1 do
			if self.m_GuildPinTuInfo[i] == 1 then
				count = count + 1
			end
		end
	end
	return count
end

return CLuaZhouNianQingPintuWnd
