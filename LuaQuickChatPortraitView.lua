local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local CUITexture=import "L10.UI.CUITexture"
local CExpressionMgr = import "L10.Game.CExpressionMgr"
local CChatMgr = import "L10.Game.CChatMgr"
local UICamera = import "UICamera"

LuaQuickChatPortraitView = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaQuickChatPortraitView, "PortraitGrid", UIGrid)
RegistChildComponent(LuaQuickChatPortraitView, "PortraitTemplate", GameObject)

RegistClassMember(LuaQuickChatPortraitView, "m_IsInOperation")
RegistClassMember(LuaQuickChatPortraitView, "m_QuickChatPortraitList")
RegistClassMember(LuaQuickChatPortraitView, "m_LongPressTick")    -- 长按计时
--@endregion RegistChildComponent end

function LuaQuickChatPortraitView:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaQuickChatPortraitView:Init()
    self.m_IsInOperation = false
    self.PortraitTemplate:SetActive(false)
    self.m_QuickChatPortraitList = {}
    self.m_LongPressInterval = 1000
	Extensions.RemoveAllChildren(self.PortraitGrid.transform)
    for i = 1, 4 do
        local index = i
        local go = NGUITools.AddChild(self.PortraitGrid.gameObject, self.PortraitTemplate)
        local cell = go:GetComponent(typeof(UIWidget))
        table.insert(self.m_QuickChatPortraitList, {cell = cell})
        self:SetPortraitEmpty(index)

        UIEventListener.Get(cell.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnPortraitClick(index)
        end)
        UIEventListener.Get(cell.gameObject).onPress = DelegateFactory.BoolDelegate(function(go, ispressed)
            self:OnPortraitPress(go, ispressed)
        end)
        local delBtn = cell.transform:Find("DelBtn").gameObject
        UIEventListener.Get(delBtn).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnDelBtnClick(index)
        end)
    end
end

function LuaQuickChatPortraitView:Update()
    if Input.GetMouseButtonDown(0) and self.m_IsInOperation then
        local isShow = false
        if CUICommonDef.IsOverUI then   -- 点击在UI上
            for i = 1, #self.m_QuickChatPortraitList do
                local corners = self.m_QuickChatPortraitList[i].cell.worldCorners
                local lastPos = UICamera.lastWorldPosition;
                if not (lastPos.x < corners[0].x or lastPos.x > corners[2].x or lastPos.y < corners[0].y or lastPos.y > corners[2].y) then
                    isShow = true       -- 点击在头像上
                    break
                end
            end
        end
        if not isShow then
            self.m_IsInOperation = false
            self:UpdateOperationStatus()
        end
    end
end

function LuaQuickChatPortraitView:SetPortraitEmpty(index)
    local cell = self.m_QuickChatPortraitList[index].cell
    self.m_QuickChatPortraitList[index].info = nil
    cell.transform:Find("PortraitRoot").gameObject:SetActive(false)
    cell.transform:Find("Empty").gameObject:SetActive(true)
    cell.transform:Find("DelBtn").gameObject:SetActive(false)
    cell.gameObject:SetActive(self.m_IsInOperation)
end

function LuaQuickChatPortraitView:SetQuickChatTarget()
    local popupMenuInfo = CPlayerInfoMgr.PopupMenuInfo
    local index = self:GetPortraitEmptyIndex(popupMenuInfo.playerId)
    if index == -1 then return end
    self.m_QuickChatPortraitList[index].info = {}
    self.m_QuickChatPortraitList[index].info.playerId = popupMenuInfo.playerId
    self.m_QuickChatPortraitList[index].info.name = popupMenuInfo.displayName
    self.m_QuickChatPortraitList[index].info.portraitName = popupMenuInfo.portraitName
    self.m_QuickChatPortraitList[index].info.profileFrame = popupMenuInfo.profileFrame
    self.m_QuickChatPortraitList[index].info.expressionTxt = popupMenuInfo.expressionTxt
    self.m_QuickChatPortraitList[index].info.level = popupMenuInfo.level
    self:UpdatePortrait(index)

    local cell = self.m_QuickChatPortraitList[index].cell
    cell.transform:Find("PortraitRoot").gameObject:SetActive(true)
    cell.transform:Find("Empty").gameObject:SetActive(false)
    cell.transform:Find("DelBtn").gameObject:SetActive(false)
    cell.gameObject:SetActive(true)

    self:OpenQuickChatWnd(popupMenuInfo.playerId, popupMenuInfo.displayName)
end

function LuaQuickChatPortraitView:UpdatePortrait(index)
    local info = self.m_QuickChatPortraitList[index].info
    local cell = self.m_QuickChatPortraitList[index].cell
    if info then
        local texture = cell.transform:Find("PortraitRoot/Portrait"):GetComponent(typeof(CUITexture))
        local profileFrame = cell.transform:Find("PortraitRoot/ProfileFrame"):GetComponent(typeof(CUITexture))
        local expressionTxt = cell.transform:Find("PortraitRoot/ExpressionText"):GetComponent(typeof(CUITexture))
        texture:LoadNPCPortrait(info.portraitName, false)
        if profileFrame ~= nil and CExpressionMgr.EnableProfileFrame then
            profileFrame:LoadMaterial(CUICommonDef.GetProfileFramePath(info.profileFrame))
        end
        expressionTxt:LoadMaterial(CUICommonDef.GetExpressionTxtPath(info.expressionTxt))
        cell.transform:Find("PortraitRoot/LvLabel"):GetComponent(typeof(UILabel)).text = info.level
    else
        self:SetPortraitEmpty(index)
    end
end

function LuaQuickChatPortraitView:GetPortraitEmptyIndex(playerId)
    for i = 1, #self.m_QuickChatPortraitList do
        if self.m_QuickChatPortraitList[i].info then
            if self.m_QuickChatPortraitList[i].info.playerId == playerId then return i end
        else
            return i
        end
    end
    g_MessageMgr:ShowMessage("QuickChat_Target_Full")   -- 目标已满且不是目标
    return -1
end

function LuaQuickChatPortraitView:OpenQuickChatWnd(playerId, playerName)
    CChatMgr.Inst.m_QuickChatOppositeId = playerId
    CChatMgr.Inst.m_QuickChatOppositeName = playerName
    CUIManager.ShowUI(CLuaUIResources.QuickChatWnd)
end

function LuaQuickChatPortraitView:UpdateOperationStatus()
    for i = 1, #self.m_QuickChatPortraitList do
        local data = self.m_QuickChatPortraitList[i]
        if data and data.info then
            data.cell.transform:Find("DelBtn").gameObject:SetActive(self.m_IsInOperation)
        else
            data.cell.gameObject:SetActive(self.m_IsInOperation)
        end
    end
end

--@region UIEvent
function LuaQuickChatPortraitView:OnPortraitClick(index)
    if self.m_IsInOperation then return end
    local data = self.m_QuickChatPortraitList[index]
    if data and data.info then
        self:OpenQuickChatWnd(data.info.playerId, data.info.name)
    end
end

function LuaQuickChatPortraitView:OnDelBtnClick(index)
    for i = index, #self.m_QuickChatPortraitList do
        if i == #self.m_QuickChatPortraitList then
            self.m_QuickChatPortraitList[i].info = nil
        else
            self.m_QuickChatPortraitList[i].info =  self.m_QuickChatPortraitList[i + 1].info
        end
        self:UpdatePortrait(i)
    end
end

function LuaQuickChatPortraitView:OnPortraitPress(go, ispressed)
    if self.m_IsInOperation then return end
    if self.m_LongPressTick then
        UnRegisterTick(self.m_LongPressTick)
        self.m_LongPressTick = nil
    end
    if ispressed then
        self.m_IsInOperation = false
        self.m_LongPressTick = RegisterTickOnce(function()
            self.m_IsInOperation = true -- 触发长按操作
            self:UpdateOperationStatus()
        end, self.m_LongPressInterval)
    end
end

--@endregion UIEvent
function LuaQuickChatPortraitView:OnEnable()
    self:Init()
    g_ScriptEvent:AddListener("SetQuickChatTarget", self, "SetQuickChatTarget")
end

function LuaQuickChatPortraitView:OnDisable()
    g_ScriptEvent:RemoveListener("SetQuickChatTarget", self, "SetQuickChatTarget")
end
