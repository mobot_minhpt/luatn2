-- Auto Generated!!
local CYuanDanMgr = import "L10.Game.CYuanDanMgr"
local CYuanDanQiFuProgressWnd = import "L10.UI.CYuanDanQiFuProgressWnd"
local LocalString = import "LocalString"
local YuanDan_Setting = import "L10.Game.YuanDan_Setting"
CYuanDanQiFuProgressWnd.m_Init_CS2LuaHook = function (this) 
    this.myQiFuTimesLabel.text = tostring(CYuanDanMgr.Inst.MyQiFuTimes)
    this.totalQiFuTimesProgressBar.value = 0
    if this.dots ~= nil and this.dots.Length == 3 then
        do
            local i = 0
            while i < this.dots.Length do
                this.dots[i]:Init(false, nil)
                i = i + 1
            end
        end
        local values = YuanDan_Setting.GetData().QiFuTimesToPlayExpRain
        if values ~= nil and values.Length == 3 and values[0] < values[1] and values[1] < values[2] then
            this.totalQiFuTimesProgressBar.value = CYuanDanMgr.Inst.TotalQiFuTimes * 1 / values[2]
            for i = 0, 2 do
                this.dots[i]:Init(CYuanDanMgr.Inst.TotalQiFuTimes >= values[i], System.String.Format(LocalString.GetString("{0}万"), math.floor(values[i] / 10000)))
            end
        end
    end
end
