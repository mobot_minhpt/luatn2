local CUITexture = import "L10.UI.CUITexture"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local Extensions = import "Extensions"
local CButton = import "L10.UI.CButton"
local EnumGender = import "L10.Game.EnumGender"
local UISprite = import "UISprite"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"

LuaAppearanceClosetRanFaJiSubview = class()

RegistChildComponent(LuaAppearanceClosetRanFaJiSubview,"m_ClosetRanFaJiItem", "CommonClosetItemCell", GameObject)
RegistChildComponent(LuaAppearanceClosetRanFaJiSubview,"m_ItemDisplay", "AppearanceCommonButtonDisplayView", CCommonLuaScript)
RegistChildComponent(LuaAppearanceClosetRanFaJiSubview,"m_ContentTable", "ContentTable", UITable)
RegistChildComponent(LuaAppearanceClosetRanFaJiSubview,"m_ContentScrollView", "ContentScrollView", UIScrollView)

RegistClassMember(LuaAppearanceClosetRanFaJiSubview, "m_MyRanFaJis")
RegistClassMember(LuaAppearanceClosetRanFaJiSubview, "m_SelectedDataId")

function LuaAppearanceClosetRanFaJiSubview:Awake()
end

function LuaAppearanceClosetRanFaJiSubview:Init()
    Extensions.RemoveAllChildren(self.m_ContentTable.transform)
    CLuaRanFaMgr:RequestSyncRanFaJiPlayData(EnumSyncRanFaJiType.eDefault)

    self.m_ItemDisplay.gameObject:SetActive(true)
    local buttonTbl = {}
    table.insert(buttonTbl, {text=LocalString.GetString("配方"), isYellow=false, action=function(go) self:OnRecipeButtonClick() end})
    table.insert(buttonTbl, {text=LocalString.GetString("发色库"), isYellow=false, action=function(go) self:OnLibraryButtonClick() end})
    self.m_ItemDisplay:Init(buttonTbl)
end

function LuaAppearanceClosetRanFaJiSubview:LoadData()
    self.m_MyRanFaJis = LuaAppearancePreviewMgr:GetAllRanFaJiInfo(true)
    Extensions.RemoveAllChildren(self.m_ContentTable.transform)
    self.m_ContentTable.gameObject:SetActive(true)

    for i = 1, # self.m_MyRanFaJis do
        local child = CUICommonDef.AddChild(self.m_ContentTable.gameObject, self.m_ClosetRanFaJiItem)
        child:SetActive(true)
        self:InitItem(child, self.m_MyRanFaJis[i])
    end
    self.m_ContentTable:Reposition()
    self.m_ContentScrollView:ResetPosition()
    self:InitSelection()
end

function LuaAppearanceClosetRanFaJiSubview:SetDefaultSelection()
    self.m_SelectedDataId = LuaAppearancePreviewMgr:GetCurrentInUseRanFaJi()
end

function LuaAppearanceClosetRanFaJiSubview:InitSelection()
    if self.m_SelectedDataId == 0 then
        self:OnItemClick(nil)
        return
    end

    local childCount = self.m_ContentTable.transform.childCount
    for i=0, childCount-1 do
        local childGo = self.m_ContentTable.transform:GetChild(i).gameObject
        local appearanceData = self.m_MyRanFaJis[i+1]
        if appearanceData.id == self.m_SelectedDataId then
            self:OnItemClick(childGo)
            break
        end
    end
end

function LuaAppearanceClosetRanFaJiSubview:InitItem(itemGo, appearanceData)
    local iconTexture = itemGo.transform:Find("Item"):GetComponent(typeof(CUITexture))
    local cornerGo = itemGo.transform:Find("Item/Corner").gameObject
    local nameLabel = itemGo.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local conditionLabel = itemGo.transform:Find("ConditionLabel"):GetComponent(typeof(UILabel))
    iconTexture:LoadMaterial(appearanceData.icon)
    itemGo:GetComponent(typeof(CButton)).Selected = (self.m_SelectedDataId and self.m_SelectedDataId==appearanceData.id or false)
    cornerGo:SetActive(LuaAppearancePreviewMgr:IsCurrentInUseRanFaJi(appearanceData.id))
    nameLabel.text = appearanceData.name
    conditionLabel.text = ""

    UIEventListener.Get(itemGo).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnItemClick(go, true)
    end)
end

function LuaAppearanceClosetRanFaJiSubview:OnItemClick(go, isClick)
    local childCount = self.m_ContentTable.transform.childCount
    for i=0, childCount-1 do
        local childGo = self.m_ContentTable.transform:GetChild(i).gameObject
        if childGo == go then
            childGo.transform:GetComponent(typeof(CButton)).Selected = true
            self.m_SelectedDataId = self.m_MyRanFaJis[i+1].id
            if isClick then
                self:ShowPopupMenu(go, self.m_MyRanFaJis[i+1])
            end
        else
            childGo.transform:GetComponent(typeof(CButton)).Selected = false
        end
    end
    g_ScriptEvent:BroadcastInLua("RequestPreviewMainPlayerRanFaJi", self.m_SelectedDataId)
end

function LuaAppearanceClosetRanFaJiSubview:ShowPopupMenu(go, appearanceData)
    --染色/褪色
    local takeOnMenu = g_PopupMenuMgr:CreateOneDefaultPopupMenuItem(LocalString.GetString("染色"), function() LuaAppearancePreviewMgr:RequestTakeOnRanFaJi(appearanceData.id) end, false)
    local takeOffMenu = g_PopupMenuMgr:CreateOneDefaultPopupMenuItem(LocalString.GetString("褪色"), function() LuaAppearancePreviewMgr:RequestTakeOffRanFaJi(appearanceData.id) end, false)                                                                                                                                                                                                   

    local tbl = {}
    local inUse = LuaAppearancePreviewMgr:IsCurrentInUseRanFaJi(appearanceData.id)
    if inUse then
        table.insert(tbl, takeOffMenu)
    else
        table.insert(tbl, takeOnMenu)
    end
    g_PopupMenuMgr:ShowPopupMenuOnBottom(tbl, go.transform)
end

function LuaAppearanceClosetRanFaJiSubview:OnRecipeButtonClick()
    LuaAppearancePreviewMgr:RequestOpenRanFaJiRecipe()
end

function LuaAppearanceClosetRanFaJiSubview:OnLibraryButtonClick()
    LuaAppearancePreviewMgr:RequestOpenRanFaJiLibrary()
end

function  LuaAppearanceClosetRanFaJiSubview:OnEnable()
    g_ScriptEvent:AddListener("OnSyncRanFaJiPlayData", self, "OnSyncRanFaJiPlayData")
end

function  LuaAppearanceClosetRanFaJiSubview:OnDisable()
    g_ScriptEvent:RemoveListener("OnSyncRanFaJiPlayData", self, "OnSyncRanFaJiPlayData")
end

function LuaAppearanceClosetRanFaJiSubview:OnSyncRanFaJiPlayData(...)
    print("OnSyncRanFaJiPlayData")
    self:SetDefaultSelection()
    self:LoadData()
end
