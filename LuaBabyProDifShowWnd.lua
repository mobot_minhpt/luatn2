require("common/common_include")

local CPlayerProDifShowItem = import "CPlayerProDifShowItem"
local Vector3 = import "UnityEngine.Vector3"
local Extensions = import "Extensions"

LuaBabyProDifShowWnd = class()

RegistClassMember(LuaBabyProDifShowWnd, "Dis")
RegistClassMember(LuaBabyProDifShowWnd, "TitleHeight")
RegistClassMember(LuaBabyProDifShowWnd, "TemplateNode")
RegistClassMember(LuaBabyProDifShowWnd, "FatherNode")
RegistClassMember(LuaBabyProDifShowWnd, "BgNode")
RegistClassMember(LuaBabyProDifShowWnd, "Title")


function LuaBabyProDifShowWnd:Init()
	self:InitClassMembers()
	self:InitValues()
end

function LuaBabyProDifShowWnd:InitClassMembers()
	self.TemplateNode = self.transform:Find("Item").gameObject
	self.TemplateNode:SetActive(false)
	self.FatherNode = self.transform:Find("BasicPropertyFrame").gameObject
	self.BgNode = self.transform:Find("Bg"):GetComponent(typeof(UISprite))
	self.Title = self.transform:Find("Title").gameObject
	self.Title:SetActive(false)
end

function LuaBabyProDifShowWnd:InitValues()
	self.Dis = 70
	self.TitleHeight = 80
	self:UpdatePropDiffs()
end

function LuaBabyProDifShowWnd:UpdatePropDiffs()

	Extensions.RemoveAllChildren(self.FatherNode.transform)

	local propDiffList = LuaBabyMgr.m_BabyPropDiffList
	local totalCount = propDiffList.Count
	local half = propDiffList.Count / 2


	self.Title.transform.localPosition = Vector3(0, half * self.Dis, 0)
	self.Title:SetActive(true)

	if propDiffList and propDiffList.Count > 0 then
		
		for i = 0, propDiffList.Count-1 do
			local go = NGUITools.AddChild(self.FatherNode, self.TemplateNode)
			go:SetActive(true)
			local info = propDiffList[i]
			local script = go:GetComponent(typeof(CPlayerProDifShowItem))
			local pos = Vector3(0, (half - i - 0.5) * self.Dis - self.TitleHeight / 2, 0)
			script:init(info, pos, i + 1, totalCount)
		end

		self.BgNode.height = math.floor(self.Dis * totalCount) + 10 + self.TitleHeight
	end
	
end

function LuaBabyProDifShowWnd:Update()
	self:ClickThroughToClose()
end

function LuaBabyProDifShowWnd:ClickThroughToClose()
    if Input.GetMouseButtonDown(0) then       
        if (not CUICommonDef.IsOverUI or (UICamera.lastHit.collider and
           not CUICommonDef.IsChild(self.transform, UICamera.lastHit.collider.transform) and
            CUICommonDef.GetNearestPanelDepth(self.transform) > CUICommonDef.GetNearestPanelDepth(UICamera.lastHit.collider.transform))) then
            CUIManager.CloseUI(CLuaUIResources.BabyProDifShowWnd)
        end
    end
end

function LuaBabyProDifShowWnd:OnEnable()
	--g_ScriptEvent:AddListener("UpdateBabyPlanAward", self, "UpdateGains")
end

function LuaBabyProDifShowWnd:OnDisable()
	--g_ScriptEvent:RemoveListener("UpdateBabyPlanAward", self, "UpdateGains")
end

return LuaBabyProDifShowWnd
