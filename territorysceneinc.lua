CTerritoryScene = class()

RegistClassMember(CTerritoryScene, "m_Grid")
RegistClassMember(CTerritoryScene, "m_GridWidth")
RegistClassMember(CTerritoryScene, "m_GridHeight")

RegistClassMember(CTerritoryScene, "m_BarrierListCache")
RegistClassMember(CTerritoryScene, "m_BarrierMapCache")

require "common/citywar/territoryscene"
