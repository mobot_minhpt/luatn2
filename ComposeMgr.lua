g_ComposeMgr = {}

g_ComposeMgr.ConsumeItemsCache = nil
g_ComposeMgr.m_ExchangeId = 0
g_ComposeMgr.m_ComposeSuccessMsg = LocalString.GetString("合成成功")
g_ComposeMgr.m_ComposeLackOfItemMsg = LocalString.GetString("道具不足，无法合成")

--- 检测是否可供合成
function g_ComposeMgr.CheckCanCompose(TemplateId)
    local canCompose = false
    local resKey = ""
    local wndTitle = ""
    local i, j
    g_ComposeMgr.ConsumeItemsCache = {}
    Exchange_Setting.Foreach(function(key, value)
        local arr = value.Value
        for i = 0, arr.Length - 1 do
            local groupId = arr[i]
            local data = Exchange_Exchange.GetData(groupId)
            if data.ConsumeItems.Length < 4 then
                local consumeItems = data.ConsumeItems
                local consumeItem = consumeItems[0][0]
                g_ComposeMgr.ConsumeItemsCache[consumeItem] = {["Key"] = value.Key,["WndTitle"] = value.WndTitle}
            end
        end
    end)

    local t = g_ComposeMgr.ConsumeItemsCache[TemplateId]
    if t then
        canCompose = true
        resKey =  t["Key"]
        wndTitle =  t["WndTitle"]
    end
    return canCompose, resKey, wndTitle
end


