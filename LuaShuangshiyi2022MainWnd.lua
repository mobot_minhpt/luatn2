local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Animator = import "UnityEngine.Animator"
local CServerTimeMgr  = import "L10.Game.CServerTimeMgr"
local CScheduleMgr = import "L10.Game.CScheduleMgr"
local CLoginMgr = import "L10.Game.CLoginMgr"
local Animation = import "UnityEngine.Animation"
local CTaskMgr = import "L10.Game.CTaskMgr"

LuaShuangshiyi2022MainWnd = class()


function LuaShuangshiyi2022MainWnd:Awake()
    self:InitUIComponent()
    self:InitUIEvent()
    self:InitUIData()
end

function LuaShuangshiyi2022MainWnd:Init()
    self:RefreshButtonListShow()
    CScheduleMgr.Inst:RequestActivity()
end

function LuaShuangshiyi2022MainWnd:InitUIComponent()
    self.mengQuanHengXingEntrance = self.transform:Find("MengQuanHengXingEntrance")
    --self.mengQuanHengXingHighlight = self.mengQuanHengXingEntrance:Find("Highlight")
    self.mengQuanHengXingRedDot = self.mengQuanHengXingEntrance:Find("RedDot")
    self.mengQuanHengXingOpenTimeLabel = self.mengQuanHengXingEntrance:Find("Hint"):GetComponent(typeof(UILabel))
    self.shenZhaiTanBaoEntrance = self.transform:Find("ShenZhaiTanBaoEntrance")
    self.shenZhaiTanBaoRedDot = self.shenZhaiTanBaoEntrance:Find("RedDot")

    --self.shenZhaiTanBaoHighlight = self.shenZhaiTanBaoEntrance:Find("Highlight")
    self.shenZhaiTanBaoOpenTimeLabel = self.shenZhaiTanBaoEntrance:Find("Hint"):GetComponent(typeof(UILabel))
    self.zhongCaoEntrance = self.transform:Find("ZhongCaoEntrance")
    self.lotteryEntrance = self.transform:Find("LotteryEntrance")
    self.bonusEntrance = self.transform:Find("BonusEntrance")
    self.mysteryEntrance = self.transform:Find("MysteryEntrance")
    
    self.mysteryQuestionMark = self.mysteryEntrance:Find("Item/QuestionMark")
    self.mysteryDog = self.mysteryEntrance:Find("Item/Dog/Sad"):GetComponent(typeof(CUITexture))

    self.animation = self.transform:GetComponent(typeof(Animation))
end 

function LuaShuangshiyi2022MainWnd:InitUIEvent()
    UIEventListener.Get(self.mengQuanHengXingEntrance.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        local mengQuanHengXingActivityId = 42030254
        if self:CheckScheduleOpen(mengQuanHengXingActivityId, true, true) then
            if CScheduleMgr.Inst:IsCanJoinSchedule(mengQuanHengXingActivityId, true) then
                --在活动时间里
                CUIManager.ShowUI(CLuaUIResources.MengQuanHengXingSignUpWnd)
                LuaActivityRedDotMgr:OnRedDotClicked(9)
            else
                local scheduleData = Task_Schedule.GetData(mengQuanHengXingActivityId)
                CLuaScheduleMgr.selectedScheduleInfo = {
                    activityId = mengQuanHengXingActivityId,
                    taskId = scheduleData.TaskID[0],
                    TotalTimes = 0,
                    DailyTimes = 0,
                    ActivityTime = scheduleData.ExternTip,
                    ShowJoinBtn = false
                }
                CLuaScheduleInfoWnd.s_UseCustomInfo = true
                CUIManager.ShowUI(CLuaUIResources.ScheduleInfoWnd)
                g_MessageMgr:ShowMessage("DOUBLE_11_MQHX_NOT_OPEN")
            end
        end
    end)
    
    UIEventListener.Get(self.shenZhaiTanBaoEntrance.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        local shenZhaiTanBaoActivityId = 42030252
        if self:CheckScheduleOpen(shenZhaiTanBaoActivityId, true, true) then
            if CScheduleMgr.Inst:IsCanJoinSchedule(shenZhaiTanBaoActivityId, true) then
                --在活动时间里
                LuaActivityRedDotMgr:OnRedDotClicked(8)
                CUIManager.ShowUI(CLuaUIResources.ShenZhaiTanBaoSignUpWnd)
            else
                local scheduleData = Task_Schedule.GetData(shenZhaiTanBaoActivityId)
                CLuaScheduleMgr.selectedScheduleInfo = {
                    activityId = shenZhaiTanBaoActivityId,
                    taskId = scheduleData.TaskID[0],
                    TotalTimes = 0,
                    DailyTimes = 0,
                    ActivityTime = scheduleData.ExternTip,
                    ShowJoinBtn = false
                }
                CLuaScheduleInfoWnd.s_UseCustomInfo = true
                CUIManager.ShowUI(CLuaUIResources.ScheduleInfoWnd)
                g_MessageMgr:ShowMessage("Double11SZTB_FAILED_TIME_LIMIT")
            end
        end
    end)
    
    UIEventListener.Get(self.zhongCaoEntrance.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        local zhongCaoActivityId = 42030253
        local baCaoActivityId = 42030257
        local scheduleData = Task_Schedule.GetData(zhongCaoActivityId)
        local taskData = Task_Task.GetData(scheduleData.TaskID[0])
        local playerLv = CClientMainPlayer.Inst and CClientMainPlayer.Inst.MaxLevel or 0
        --把等级拨出来先判断, 不然会弹多条msg
        if taskData and playerLv < taskData.Level then
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", SafeStringFormat3(LocalString.GetString("参加%s玩法需%s级"), scheduleData.TaskName, tostring(taskData.Level)))
        else
            if self:CheckScheduleOpen(zhongCaoActivityId, false, false) then
                --种草task打开了
                if CTaskMgr.Inst:CheckTaskTime(22121166) then
                    CLuaScheduleMgr:ShowScheduleInfo(zhongCaoActivityId)
                else
                    if CScheduleMgr.Inst:IsCanJoinSchedule(zhongCaoActivityId, true) then
                        --当前时间段可以参加种草活动
                        CLuaScheduleMgr:ShowScheduleInfo(zhongCaoActivityId)
                    else
                        --gapTime
                        g_MessageMgr:ShowMessage("DoubleOne2022_Zhongcao_Gap")
                    end
                end
            elseif self:CheckScheduleOpen(baCaoActivityId, false, false) then
                --拔草task打开了
                if CTaskMgr.Inst:CheckTaskTime(22121174) then
                    CLuaScheduleMgr:ShowScheduleInfo(baCaoActivityId)
                else
                    if CScheduleMgr.Inst:IsCanJoinSchedule(baCaoActivityId, true) then
                        --当前时间段可以参加拔草活动
                        CLuaScheduleMgr:ShowScheduleInfo(baCaoActivityId)
                    else
                        --gapTime
                        g_MessageMgr:ShowMessage("DoubleOne2022_Zhongcao_Gap")
                    end
                end
            else
                local baCaoEndTime = CServerTimeMgr.Inst:GetTimeStampByStr(Double11_Setting.GetData().BaCaoDeadLine)
                local isBaCaoEnd = CServerTimeMgr.Inst.timeStamp >= baCaoEndTime
                if isBaCaoEnd then
                    g_MessageMgr:ShowMessage("PLAY_NOT_OPEN")
                else
                    g_MessageMgr:ShowMessage("DoubleOne2022_Zhongcao_Gap")
                end
            end
        end
    end)
    
    UIEventListener.Get(self.lotteryEntrance.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        local lotteryActivityId = 42030256
        if self:CheckScheduleOpen(lotteryActivityId, true, true) then
            CLuaScheduleMgr:ShowScheduleInfo(lotteryActivityId)
        end
    end)
    
    UIEventListener.Get(self.bonusEntrance.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        local bonusActivityId = 42030255
        if self:CheckScheduleOpen(bonusActivityId, true, true) then
            CLuaScheduleMgr:ShowScheduleInfo(42030255)
        end
    end)
    
    UIEventListener.Get(self.mysteryEntrance.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        local mysteryActivityId = 42030251
        if self:CheckScheduleOpen(mysteryActivityId, true, true) then
            CLuaScheduleMgr:ShowScheduleInfo(mysteryActivityId)
        end
    end)
end 

function LuaShuangshiyi2022MainWnd:RefreshButtonListShow()
    local mengQuanHengXingActivityId = 42030254
    local showMengQuanButton = self:CheckScheduleOpen(mengQuanHengXingActivityId, false, false)
    self.mengQuanHengXingEntrance:Find("Type").gameObject:SetActive(showMengQuanButton)
    self.mengQuanHengXingEntrance:Find("Name").gameObject:SetActive(showMengQuanButton)
    self.mengQuanHengXingEntrance:Find("Hint").gameObject:SetActive(showMengQuanButton)
    self.mengQuanHengXingEntrance:GetComponent(typeof(QnButton)).Enabled = showMengQuanButton
    self.mengQuanHengXingEntrance:GetComponent(typeof(UITexture)).enabled = showMengQuanButton

    local shenZhaiTanBaoActivityId = 42030252
    local showShenZhaiButton = self:CheckScheduleOpen(shenZhaiTanBaoActivityId, false, false)
    self.shenZhaiTanBaoEntrance:Find("Type").gameObject:SetActive(showShenZhaiButton)
    self.shenZhaiTanBaoEntrance:Find("Name").gameObject:SetActive(showShenZhaiButton)
    self.shenZhaiTanBaoEntrance:Find("Hint").gameObject:SetActive(showShenZhaiButton)
    self.shenZhaiTanBaoEntrance:GetComponent(typeof(QnButton)).Enabled = showShenZhaiButton
    self.shenZhaiTanBaoEntrance:GetComponent(typeof(UITexture)).enabled = showShenZhaiButton

    self:RefreshWebPayPattern()

    local lotteryActivityId = 42030256
    local showLotteryButton = self:CheckScheduleOpen(lotteryActivityId, false, false)
    self.lotteryEntrance:Find("Label").gameObject:SetActive(showLotteryButton)
    self.lotteryEntrance:Find("LotteryEntrance (1)").gameObject:SetActive(showLotteryButton)
    self.lotteryEntrance:GetComponent(typeof(QnButton)).Enabled = showLotteryButton

    local bonusActivityId = 42030255
    local showBonusButton = self:CheckScheduleOpen(bonusActivityId, false, false)
    self.bonusEntrance:Find("Label").gameObject:SetActive(showBonusButton)
    self.bonusEntrance:Find("MysteryEntrance (2)").gameObject:SetActive(showBonusButton)
    self.bonusEntrance:GetComponent(typeof(QnButton)).Enabled = showBonusButton
    
    local zhongCaoActivityId = 42030253
    local baCaoActivityId = 42030257
    local scheduleData = Task_Schedule.GetData(zhongCaoActivityId)
    local taskData = Task_Task.GetData(scheduleData.TaskID[0])
    local playerLv = CClientMainPlayer.Inst and CClientMainPlayer.Inst.MaxLevel or 0
    local showZhongCaoButton = false
    --把等级拨出来先判断, 不然后续很麻烦
    if taskData and playerLv >= taskData.Level then
        showZhongCaoButton = self:CheckScheduleOpen(zhongCaoActivityId, false, false)
        showZhongCaoButton = showZhongCaoButton or self:CheckScheduleOpen(baCaoActivityId, false, false)
        local baCaoEndTime = CServerTimeMgr.Inst:GetTimeStampByStr(Double11_Setting.GetData().BaCaoDeadLine)
        local isBaCaoEnd = CServerTimeMgr.Inst.timeStamp >= baCaoEndTime
        showZhongCaoButton = showZhongCaoButton or (not isBaCaoEnd)
    end
    self.zhongCaoEntrance:Find("Label").gameObject:SetActive(showZhongCaoButton)
    self.zhongCaoEntrance:Find("MysteryEntrance (3)").gameObject:SetActive(showZhongCaoButton)
    self.zhongCaoEntrance:GetComponent(typeof(QnButton)).Enabled = showZhongCaoButton
end

function LuaShuangshiyi2022MainWnd:CheckScheduleOpen(activityId, showLevelMsg, showTaskNotOpenMsg)
    local scheduleData = Task_Schedule.GetData(activityId)
    local taskData = Task_Task.GetData(scheduleData.TaskID[0])
    local playerLv = CClientMainPlayer.Inst and CClientMainPlayer.Inst.MaxLevel or 0
    local edouble11JieRiId = 19

    if taskData and playerLv < taskData.Level then
        if showLevelMsg then
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", SafeStringFormat3(LocalString.GetString("参加%s玩法需%s级"), scheduleData.TaskName, tostring(taskData.Level)))
        end
        return false
    elseif taskData and playerLv >= taskData.Level then
        --判断完等级之后, 再根据服务器数据判断一下这个玩法有没有开启吧..
        local openFestivalTasks = CLuaScheduleMgr.m_OpenFestivalTasks[edouble11JieRiId] or {}
        local taskId = scheduleData.TaskID[0]
        for i = 1, #openFestivalTasks do
            if openFestivalTasks[i] == taskId then
                return true
            end
        end
        --这个活动没开启
        if showTaskNotOpenMsg then
            g_MessageMgr:ShowMessage("PLAY_NOT_OPEN")
        end
        return false
    else
        --这个是保险起见, 一般不会跑到这里
        if showTaskNotOpenMsg then
            g_MessageMgr:ShowMessage("PLAY_NOT_OPEN")
        end
        return false
    end
end

function LuaShuangshiyi2022MainWnd:InitUIData()
    local SZTBConfigData = Double11_SZTB.GetData()
    local MQHXConfigData = Double11_MQHX.GetData()
    self.SZTBGameplayId = SZTBConfigData.PlayDesignId
    self.MQHXGameplayId = MQHXConfigData.PlayDesignId

    self.questionDogMaterialPath = "UI/Texture/Transparent/Material/shuangshiyi2022mainwnd_btn_shenmirenwu_noraml_01.mat"
    self.webPayDogMaterialPath = "UI/Texture/Transparent/Material/shuangshiyi2022mainwnd_btn_shenmirenwu_done.mat"
    
    self:OnUpdatePlayState(self.MQHXGameplayId)
    self:OnUpdatePlayState(self.SZTBGameplayId)
    
    self:RefreshWebPayPattern()
    
    local SettingConfigData = Double11_Setting.GetData()
    self.shenZhaiTanBaoOpenTimeLabel.text = LocalString.GetString(SettingConfigData.SZTBMainWnd_Time)
    self.mengQuanHengXingOpenTimeLabel.text = LocalString.GetString(SettingConfigData.MQHXMainWnd_Time)

    self:UpdateRedDot()
end

function LuaShuangshiyi2022MainWnd:OnEnable()
    g_ScriptEvent:AddListener("GlobalMatch_UpdatePlayStatus", self, "OnUpdatePlayState")
    g_ScriptEvent:AddListener("UpdateTempPlayTimesWithKey", self, "OnUpdateTempPlayTimesWithKey")
    g_ScriptEvent:AddListener("ShowUIPreDraw", self, "OnShowUIPreDraw")
    g_ScriptEvent:AddListener("UpdateActivityRedDot", self, "OnUpdateActivityRedDot")
    g_ScriptEvent:AddListener("Double11MQHX_OpenSignUpWnd", self, "OnPlayFadeOutAnimation")
end

function LuaShuangshiyi2022MainWnd:OnDisable()
    g_ScriptEvent:RemoveListener("GlobalMatch_UpdatePlayStatus", self, "OnUpdatePlayState")
    g_ScriptEvent:RemoveListener("UpdateTempPlayTimesWithKey", self, "OnUpdateTempPlayTimesWithKey")
    g_ScriptEvent:RemoveListener("ShowUIPreDraw", self, "OnShowUIPreDraw")
    g_ScriptEvent:RemoveListener("UpdateActivityRedDot", self, "OnUpdateActivityRedDot")
    g_ScriptEvent:RemoveListener("Double11MQHX_OpenSignUpWnd", self, "OnPlayFadeOutAnimation")
end

function LuaShuangshiyi2022MainWnd:OnPlayFadeOutAnimation()
    self.animation:Play("shuangshiyi2022mainwnd_mengquan")
end

function LuaShuangshiyi2022MainWnd:OnUpdatePlayState(playId, bIsPlayOpen, bIsPlayOpenMatch, ExtraInfo_U)
    if playId == self.SZTBGameplayId then
        local SZTBOpen = LuaGlobalMatchMgr[self.SZTBGameplayId] and
                (LuaGlobalMatchMgr[self.SZTBGameplayId].isPlayOpen and LuaGlobalMatchMgr[self.SZTBGameplayId].isPlayOpenMatch) or
                false
        --self.shenZhaiTanBaoHighlight.gameObject:SetActive(SZTBOpen)
    elseif playId == self.MQHXGameplayId then
        local MQHXOpen = LuaGlobalMatchMgr[self.MQHXGameplayId] and
                (LuaGlobalMatchMgr[self.MQHXGameplayId].isPlayOpen and LuaGlobalMatchMgr[self.MQHXGameplayId].isPlayOpenMatch) or
                false
        --self.mengQuanHengXingHighlight.gameObject:SetActive(MQHXOpen)
    end
end

function LuaShuangshiyi2022MainWnd:OnUpdateTempPlayTimesWithKey(args)
    local key = args[0]
    if key == EnumPlayTimesKey_lua.eDouble11WebPayZhongCaoRewardTimes then
        self:RefreshWebPayPattern()
    end
end 

function LuaShuangshiyi2022MainWnd:RefreshWebPayPattern()
    local neteaseOffical = (CommonDefs.IS_CN_CLIENT and CLoginMgr.Inst:IsNetEaseOfficialLogin())
    local mainplayer = CClientMainPlayer.Inst
    self.isOfficialClient = false
    if neteaseOffical and mainplayer then
        self.isOfficialClient = true
    end

    if self.isOfficialClient then
        local webPayTimes = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eDouble11WebPayZhongCaoRewardTimes) or 0
        self.mysteryQuestionMark.gameObject:SetActive(webPayTimes == 0)
        if webPayTimes > 0 then
            self.mysteryDog:LoadMaterial(self.webPayDogMaterialPath)
        else
            self.mysteryDog:LoadMaterial(self.questionDogMaterialPath)
        end

        local mysteryActivityId = 42030251
        local showMysteryButton = self:CheckScheduleOpen(mysteryActivityId, false, false)
        self.mysteryEntrance:Find("Label").gameObject:SetActive(showMysteryButton)
        self.mysteryEntrance:Find("MysteryEntranceBg").gameObject:SetActive(showMysteryButton)
        self.mysteryEntrance:GetComponent(typeof(QnButton)).Enabled = showMysteryButton
    else
        self.mysteryDog:LoadMaterial(self.webPayDogMaterialPath)
        self.mysteryQuestionMark.gameObject:SetActive(false)
        self.mysteryEntrance:Find("Label").gameObject:SetActive(false)
        self.mysteryEntrance:Find("MysteryEntranceBg").gameObject:SetActive(false)
        self.mysteryEntrance:GetComponent(typeof(QnButton)).Enabled = false
    end
end 

function LuaShuangshiyi2022MainWnd:OnShowUIPreDraw(args)
    local show, name = args[0], args[1]
    if name == "MengQuanHengXingSignUpWnd" and show == false and CUIManager.instance then
        --特殊处理一下，当关闭猛犬横行报名界面的时候, 播放一下主界面的动效, 让各个元素回到初始位置
        self.animation:Play("shuangshiyi2022mainwnd_show")
    end
end

function LuaShuangshiyi2022MainWnd:OnUpdateActivityRedDot(hasChanged)
    if hasChanged then
        self:UpdateRedDot()
    end
end

function LuaShuangshiyi2022MainWnd:UpdateRedDot()
    self.mengQuanHengXingRedDot.gameObject:SetActive(LuaActivityRedDotMgr:IsRedDot(9))
    self.shenZhaiTanBaoRedDot.gameObject:SetActive(LuaActivityRedDotMgr:IsRedDot(8))
end 
