
CPhysicsAIShip = class()
RegistClassMember(CPhysicsAIShip, "m_POId")
RegistClassMember(CPhysicsAIShip, "m_TemplateId")
RegistClassMember(CPhysicsAIShip, "m_TimeStepInSecond")
RegistClassMember(CPhysicsAIShip, "m_Mass") -- ?
RegistClassMember(CPhysicsAIShip, "m_I") -- ?

RegistClassMember(CPhysicsAIShip, "m_MaxSpeed")
RegistClassMember(CPhysicsAIShip, "m_Speed")
RegistClassMember(CPhysicsAIShip, "m_MulSpeed")
-- rate - it'll be used as a multiplier for the speed
-- at which the velocity is interpolated. A rate
-- of 1 means that we interpolate across 1 second; a rate of 5 means
-- we do it five times as fast.
RegistClassMember(CPhysicsAIShip, "m_AccelerationRate")
RegistClassMember(CPhysicsAIShip, "m_DecelerationRate")
RegistClassMember(CPhysicsAIShip, "m_MaxW")
RegistClassMember(CPhysicsAIShip, "m_CanMove")

-- ai params
RegistClassMember(CPhysicsAIShip, "m_Pos")
RegistClassMember(CPhysicsAIShip, "m_Velocity")
RegistClassMember(CPhysicsAIShip, "m_Forward")
RegistClassMember(CPhysicsAIShip, "m_CurrentSpeed")
-- in degree because read from box2d, need align to client
RegistClassMember(CPhysicsAIShip, "m_CurrentOrientation")
-- in degree because read from box2d, need align to client
RegistClassMember(CPhysicsAIShip, "m_CurrentAngularVelocity")
-- in radius because calculate by lua
RegistClassMember(CPhysicsAIShip, "m_TargetOrientation")
RegistClassMember(CPhysicsAIShip, "m_TargetSpeed")

RegistClassMember(CPhysicsAIShip, "m_LastForce")

RegistClassMember(CPhysicsAIShip, "m_bAvoidCollision")
RegistClassMember(CPhysicsAIShip, "m_AC_MinTimeToCollision")
RegistClassMember(CPhysicsAIShip, "m_Steering")

-- server only
RegistClassMember(CPhysicsAIShip, "m_CanFire")

-- client only
RegistClassMember(CPhysicsAIShip, "m_ClientPhysicsObjectHandler")


EnumPhysicsAICmd = {
	ePursuit = 1,
	eSurround = 2,
	eTargetPos = 3,
	eConvey = 4,
	eForward = 5,
	eStayStill = 6,
	eTurnInPlace = 7,
	eTurnClockwiseInPlace = 8,
	eFollow = 9,
}

EnumPhysicsAICmdFuncName = {
	[1] = "SteerToPursuit",
	[2] = "SteerToSurrounding",
	[3] = "SteerToTargetPos",
	[4] = "SteerToConvoy",
	[5] = "SteerToForward",
	[6] = "SteerToStayStill",
	[7] = "SteerToTurnInPlace",
	[8] = "SteerToTurnClockwiseInPlace",
	[9] = "SteerToFollow",
}

for _, i in pairs(EnumPhysicsAICmd) do
	assert(EnumPhysicsAICmdFuncName[i])
end

SteerEnterLeaveFunNameMap = {
	["DoAni"] = "SteeringDoAni",
	["SetActive"] = "SteeringSetActive",
	["SetDiveStatus"] = "SteeringSetDiveStatus",
	["AIEvent"] = "SteeringSendAIEvent",
	["SetPos"] = "SteeringSetPhysicsPos",
	["SetToTargetFuturePos"] = "SteeringSetToTargetFuturePos",
	["AddWorldFx"] = "SteeringAddWorldFx",
	["AddObjectFx"] = "SteeringAddObjectFx",
	["SetMaxSpeed"] = "SteeringSetMaxSpeed",
	["SetMaxW"] = "SteeringSetMaxW",
	["AddBuff"] = "SteeringAddBuff",
	["RmBuff"] = "SteeringRmBuff",
}

EnablePhysicsAIShipOutputLog = false

PhysicsAIShipWThreshold = 0.17

require("common/haizhan/PhysicsAIShip")


