local QnButton = import "L10.UI.QnButton"

local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"

local DelegateFactory  = import "DelegateFactory"

local CClientHouseMgr=import "L10.Game.CClientHouseMgr"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local Color = import "UnityEngine.Color"
local MessageWndManager = import "L10.UI.MessageWndManager"
local Constants = import "L10.Game.Constants"

LuaHouseUnlockJianZhuWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaHouseUnlockJianZhuWnd, "HintLabel", "HintLabel", UILabel)
RegistChildComponent(LuaHouseUnlockJianZhuWnd, "PreviewTexture", "PreviewTexture", CUITexture)
RegistChildComponent(LuaHouseUnlockJianZhuWnd, "UnlockButton", "UnlockButton", QnButton)
RegistChildComponent(LuaHouseUnlockJianZhuWnd, "OwnMoneyLabel", "OwnMoneyLabel", UILabel)
RegistChildComponent(LuaHouseUnlockJianZhuWnd, "OwnFoodLabel", "OwnFoodLabel", UILabel)
RegistChildComponent(LuaHouseUnlockJianZhuWnd, "OwnWoodLabel", "OwnWoodLabel", UILabel)
RegistChildComponent(LuaHouseUnlockJianZhuWnd, "OwnStoneLabel", "OwnStoneLabel", UILabel)
RegistChildComponent(LuaHouseUnlockJianZhuWnd, "CostFoodLabel", "CostFoodLabel", UILabel)
RegistChildComponent(LuaHouseUnlockJianZhuWnd, "CostWoodLabel", "CostWoodLabel", UILabel)
RegistChildComponent(LuaHouseUnlockJianZhuWnd, "CostStoneLabel", "CostStoneLabel", UILabel)
RegistChildComponent(LuaHouseUnlockJianZhuWnd, "CostMoneyLabel", "CostMoneyLabel", UILabel)
RegistChildComponent(LuaHouseUnlockJianZhuWnd, "AddFoodBtn", "AddFoodBtn", QnButton)
RegistChildComponent(LuaHouseUnlockJianZhuWnd, "AddWoodBtn", "AddWoodBtn", QnButton)
RegistChildComponent(LuaHouseUnlockJianZhuWnd, "AddStoneBtn", "AddStoneBtn", QnButton)
RegistChildComponent(LuaHouseUnlockJianZhuWnd, "AddMoneyBtn", "AddMoneyBtn", QnButton)

--@endregion RegistChildComponent end

RegistClassMember(LuaHouseUnlockJianZhuWnd, "m_Key")
RegistClassMember(LuaHouseUnlockJianZhuWnd,"m_IsZiCaiEnough")
RegistClassMember(LuaHouseUnlockJianZhuWnd,"m_IsMoneyEnough")

LuaHouseUnlockJianZhuWnd.s_ZhuangshiwuId = 0

function LuaHouseUnlockJianZhuWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end

end

function LuaHouseUnlockJianZhuWnd:Start()

    self.UnlockButton.OnClick= DelegateFactory.Action_QnButton(function(btn)
        self:OnUnlockButtonClicked(nil)
    end)

    self.AddFoodBtn.OnClick = DelegateFactory.Action_QnButton(function(btn)
        self:OnGetButtonClicked(btn)
    end)
    self.AddWoodBtn.OnClick = DelegateFactory.Action_QnButton(function(btn)
        self:OnGetButtonClicked(btn)
    end)
    self.AddStoneBtn.OnClick = DelegateFactory.Action_QnButton(function(btn)
        self:OnGetButtonClicked(btn)
    end)

    self.AddMoneyBtn.OnClick = DelegateFactory.Action_QnButton(function(btn)
        local msg = g_MessageMgr:FormatMessage("NotEnough_Silver_QuickBuy")
        MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function()
			CShopMallMgr.ShowLinyuShoppingMall(Constants.SilverItemId)
		end),nil,LocalString.GetString("确定"),LocalString.GetString("取消"),false)
    end)
end

function LuaHouseUnlockJianZhuWnd:Init()
    local data = Zhuangshiwu_Zhuangshiwu.GetData(LuaHouseUnlockJianZhuWnd.s_ZhuangshiwuId)
    if not data then return end

    local colorStr = "ffcf0e"
    self.HintLabel.text = SafeStringFormat3(LocalString.GetString("解锁[%s]%s[-]效果"), colorStr,data.Name)

    local food = CClientHouseMgr.Inst.mConstructProp.Food
    local stone = CClientHouseMgr.Inst.mConstructProp.Stone
    local wood = CClientHouseMgr.Inst.mConstructProp.Wood
    self.OwnFoodLabel.text = tostring(food)
    self.OwnStoneLabel.text = tostring(stone)
    self.OwnWoodLabel.text = tostring(wood)
    self.OwnMoneyLabel.text = tostring(CClientMainPlayer.Inst.Silver)

    local find=false
    self.m_Key=0
    House_SpecialBuilding.Foreach(function(key,info)
        if info.PreZhuangshiwuId==LuaHouseUnlockJianZhuWnd.s_ZhuangshiwuId then
            find=true
            self.m_Key = key
        end
    end)

    if not find then return end

    local specialBuildingData = House_SpecialBuilding.GetData(self.m_Key)

    --icon显示新的建筑外观
    local newData = Zhuangshiwu_Zhuangshiwu.GetData(specialBuildingData.ZhuangshiwuId)
    if newData then
        self.PreviewTexture:LoadMaterial(newData.Icon)
    end

    self.CostWoodLabel.text = tostring(specialBuildingData.Price[0])
    self.CostStoneLabel.text = tostring(specialBuildingData.Price[1])
    self.CostFoodLabel.text = tostring(specialBuildingData.Price[2])
    self.CostMoneyLabel.text = tostring(specialBuildingData.YinLiangPrice)
    
    self:UpdateButtonStatus()
end

--@region UIEvent

--@endregion UIEvent
function LuaHouseUnlockJianZhuWnd:OnUnlockButtonClicked(go)
    if not self.m_IsZiCaiEnough then
        local msg = g_MessageMgr:FormatMessage("SpecialBuilding_Unlock_ZiCaiNotEnough")
        MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function()
            CShopMallMgr.ShowLinyuShoppingMall(21003558)
		end),nil,LocalString.GetString("确定"),LocalString.GetString("取消"),false)
        return
    end
    if not self.m_IsMoneyEnough then
        local msg = g_MessageMgr:FormatMessage("SILVER_NOT_ENOUGH")
        MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function()
            CShopMallMgr.ShowLinyuShoppingMall(Constants.SilverItemId)
		end),nil,LocalString.GetString("确定"),LocalString.GetString("取消"),false)
        return
    end

    local data = Zhuangshiwu_Zhuangshiwu.GetData(LuaHouseUnlockJianZhuWnd.s_ZhuangshiwuId)
    local name = data and data.Name or ""
    local msg = g_MessageMgr:FormatMessage("BUY_HOUSE_JIANZHU_CONFIRM",self.CostWoodLabel.text,self.CostMoneyLabel.text,name)
    MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function()
        Gac2Gas.RequestBuySpecialBuildingType(self.m_Key)
    end),nil,LocalString.GetString("确定"),LocalString.GetString("取消"),false)
end

function LuaHouseUnlockJianZhuWnd:UpdateButtonStatus()
    self.m_IsZiCaiEnough = true
    self.m_IsMoneyEnough = true
    local specialBuildingData = House_SpecialBuilding.GetData(self.m_Key)

    local food = CClientHouseMgr.Inst.mConstructProp.Food
    local stone = CClientHouseMgr.Inst.mConstructProp.Stone
    local wood = CClientHouseMgr.Inst.mConstructProp.Wood
    local money = CClientMainPlayer.Inst.Silver

    self.OwnFoodLabel.color = Color.white
    if food < specialBuildingData.Price[2] then
        self.OwnFoodLabel.color = Color.red
        self.m_IsZiCaiEnough = false
    end

    self.OwnStoneLabel.color = Color.white
    if stone < specialBuildingData.Price[1] then
        self.OwnStoneLabel.color = Color.red
        self.m_IsZiCaiEnough = false
    end

    self.OwnWoodLabel.color = Color.white
    if wood < specialBuildingData.Price[0] then
        self.OwnWoodLabel.color = Color.red
        self.m_IsZiCaiEnough = false
    end

    self.OwnMoneyLabel.color = Color.white
    if money < specialBuildingData.YinLiangPrice then
        self.OwnMoneyLabel.color = Color.red
        self.m_IsMoneyEnough = false
    end

    self.UnlockButton.Enabled = self.m_IsZiCaiEnough and self.m_IsMoneyEnough
end

function LuaHouseUnlockJianZhuWnd:OnEnable()
    g_ScriptEvent:AddListener("MainPlayerUpdateMoney", self, "OnMainPlayerUpdateMoney")
    g_ScriptEvent:AddListener("OnUpdateHouseResource", self, "OnUpdateHouseResource")
    g_ScriptEvent:AddListener("EnableSpecialBuilding", self, "OnEnableSpecialBuilding")
    
end

function LuaHouseUnlockJianZhuWnd:OnDisable()
    g_ScriptEvent:RemoveListener("MainPlayerUpdateMoney", self, "OnMainPlayerUpdateMoney")
    g_ScriptEvent:RemoveListener("OnUpdateHouseResource", self, "OnUpdateHouseResource")
    g_ScriptEvent:RemoveListener("EnableSpecialBuilding", self, "OnEnableSpecialBuilding")
end
function LuaHouseUnlockJianZhuWnd:OnEnableSpecialBuilding(houseId,buildingType)
    CUIManager.CloseUI(CLuaUIResources.HouseUnlockJianZhuWnd)
    CUIManager.CloseUI(CIndirectUIResources.ItemInfoWnd)
end
function LuaHouseUnlockJianZhuWnd:OnMainPlayerUpdateMoney()
    self:Init()
end

function LuaHouseUnlockJianZhuWnd:OnUpdateHouseResource(args)
    self:Init()
end
function LuaHouseUnlockJianZhuWnd:OnGetButtonClicked(go)
    -- 选中五石仓
    CShopMallMgr.ShowLinyuShoppingMall(21003558)
end
