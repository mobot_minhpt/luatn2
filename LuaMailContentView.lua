local UICamera = import "UICamera"
local StringBuilder = import "System.Text.StringBuilder"
local String = import "System.String"
local NGUITools = import "NGUITools"
local LocalString = import "LocalString"
local Extensions = import "Extensions"
local DelegateFactory = import "DelegateFactory"
local CommonDefs = import "L10.Game.CommonDefs"
local CMailMgr = import "L10.Game.CMailMgr"
local CJingLingWebImageMgr = import "L10.Game.CJingLingWebImageMgr"
local CChatLinkMgr = import "CChatLinkMgr"
local LinkSource = import "CChatLinkMgr.LinkSource"
local Mathf = import "UnityEngine.Mathf"
local Object = import "System.Object"
local IdPartition = import "L10.Game.IdPartition"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local EnumQualityType = import "L10.Game.EnumQualityType"
local ClientAction = import "L10.UI.ClientAction"
local CButton = import "L10.UI.CButton"

CLuaMailContentView = class()
RegistClassMember(CLuaMailContentView,"contentRoot")
RegistClassMember(CLuaMailContentView,"hintRoot")
RegistClassMember(CLuaMailContentView,"mailContentScrollView")
RegistClassMember(CLuaMailContentView,"mailContentTable")
RegistClassMember(CLuaMailContentView,"banner")
RegistClassMember(CLuaMailContentView,"contentTextLabel")
RegistClassMember(CLuaMailContentView,"itemsScrollView")
RegistClassMember(CLuaMailContentView,"itemsTable")
RegistClassMember(CLuaMailContentView,"itemTemplate")
RegistClassMember(CLuaMailContentView,"receiveAwardsBtn")
RegistClassMember(CLuaMailContentView,"deleteMailBtn")
RegistClassMember(CLuaMailContentView,"titleLabel")
RegistClassMember(CLuaMailContentView,"senderLabel")
RegistClassMember(CLuaMailContentView,"dateLabel")
RegistClassMember(CLuaMailContentView,"curMailId")
RegistClassMember(CLuaMailContentView,"curBannerUrl")
RegistClassMember(CLuaMailContentView,"maxBannerWidth")

RegistClassMember(CLuaMailContentView,"curMailId")
RegistClassMember(CLuaMailContentView,"curBannerUrl")
RegistClassMember(CLuaMailContentView,"maxBannerWidth")

RegistClassMember(CLuaMailContentView,"m_AwardItemTbl")
RegistClassMember(CLuaMailContentView,"m_NeedChooseAwardItemNum")
RegistClassMember(CLuaMailContentView,"m_IsANeedChooseMail")


function CLuaMailContentView:Awake()
    self.contentRoot = self.transform:Find("Content").gameObject
    self.hintRoot = self.transform:Find("Hint").gameObject
    self.mailContentScrollView = self.transform:Find("Content/ScrollView"):GetComponent(typeof(UIScrollView))
    self.mailContentTable = self.transform:Find("Content/ScrollView/Table"):GetComponent(typeof(UITable))
    self.banner = self.transform:Find("Content/ScrollView/Table/Texture"):GetComponent(typeof(UITexture))
    self.contentTextLabel = self.transform:Find("Content/ScrollView/Table/ContentLabel"):GetComponent(typeof(UILabel))
    self.itemsScrollView = self.transform:Find("Content/ItemsScrollView"):GetComponent(typeof(UIScrollView))
    self.itemsTable = self.transform:Find("Content/ItemsScrollView/Table"):GetComponent(typeof(UITable))
    self.itemTemplate = self.transform:Find("Content/ItemsScrollView/ItemCell").gameObject
    self.receiveAwardsBtn = self.transform:Find("Content/ReceiveAwardsButton"):GetComponent(typeof(CButton))
    self.deleteMailBtn = self.transform:Find("Content/DeleteMailButton"):GetComponent(typeof(CButton))
    self.titleLabel = self.transform:Find("Content/TitleLabel"):GetComponent(typeof(UILabel))
    self.senderLabel = self.transform:Find("Content/SenderLabel"):GetComponent(typeof(UILabel))
    self.dateLabel = self.transform:Find("Content/DateLabel"):GetComponent(typeof(UILabel))
    self.curMailId = nil
    self.curBannerUrl = nil
    self.maxBannerWidth = 850
    self.m_AwardItemTbl = nil
    self.m_NeedChooseAwardItemNum = 0
    self.m_IsANeedChooseMail = false
end

function CLuaMailContentView:Start()
    CommonDefs.AddOnClickListener(self.receiveAwardsBtn.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnAcceptAwardsBtnClick(go) end), false)
    CommonDefs.AddOnClickListener(self.deleteMailBtn.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnDeleteMailBtnClick(go) end), false)
    CommonDefs.AddOnClickListener(self.contentTextLabel.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnContentTextLabelClick(go) end), false)
    CommonDefs.AddOnClickListener(self.banner.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnBannerClick(go) end), false)
end

-- Auto Generated!!
function CLuaMailContentView:Init(args)
    local mailId = args and args[0] or 0
    local mail = CMailMgr.Inst:GetMail(mailId) --L10.Game.CMail
    if mail == nil then
        self.hintRoot:SetActive(true)
        self.contentRoot:SetActive(false)
    else
        self.hintRoot:SetActive(false)
        self.contentRoot:SetActive(true)
        self:ShowMailContent(mail)
    end
end

function CLuaMailContentView:DisableOperationOnFakeMail()
    return true
end

function CLuaMailContentView:ShowMailContent( mail)

    self.curMailId = mail.MailId
    mail.bIsRead = true
    --显示邮件内容
    self.titleLabel.text = mail.Title
    self.senderLabel.text = LocalString.GetString("发送者: ") .. mail.SenderNameOrDefault
    self.dateLabel.text = mail.FormatSendTime
    self:InitBannerAndContent(mail.Content)
    --显示物品列表
    Extensions.RemoveAllChildren(self.itemsTable.transform)
    self.m_AwardItemTbl = {}
    self.m_IsANeedChooseMail = (mail.Template.SelectableNum>0)
    self.m_NeedChooseAwardItemNum = mail.Template.SelectableNum

    local hasReceived = mail.CanAcceptAwards and mail.IsHandled ~= 0
    local n = 0
    for i=0, mail.ItemList.Count-1 do
        local item = mail.ItemList[i]
        if not self.m_IsANeedChooseMail or item.Selectable then --不是N选M的邮件，或者是N选M的邮件并且道具是可选的，则添加该道具到附件列表

            local obj = NGUITools.AddChild(self.itemsTable.gameObject, self.itemTemplate)
            obj:SetActive(true)
            self:InitMailAwardItem(obj, item, hasReceived)
            table.insert(self.m_AwardItemTbl, {go = obj, itemId = item.ItemId, templateId = item.TemplateId, isFake = item.IsFake, selectable = item.Selectable})
            if item.Selectable then
                n = n+1
            end
        end
    end
    --设置附件title
    local attachLabel = self.transform:Find("Content/ItemHeaderLabel"):GetComponent(typeof(UILabel))
    if mail.Template.SelectableNum<1 then
        attachLabel.text = LocalString.GetString("相关附件")
    else
        attachLabel.text = SafeStringFormat3(LocalString.GetString("相关附件 [c]%s(%s选%s)[-][/c]"), 
                                            "[FBC701]", CUICommonDef.IntToChinese(n), CUICommonDef.IntToChinese(mail.Template.SelectableNum))
    end

    self:InitButtons(mail)

    self.itemsTable:Reposition()
    self.itemsScrollView:ResetPosition()

    --排序
    self.mailContentTable:Reposition()
    self.mailContentScrollView:ResetPosition()

    --TODO 判断一下有没有奖励
    Gac2Gas.GetMailBaseReward(mail.TemplateId)
end

function CLuaMailContentView:InitMailAwardItem(go, info, hasReceived)
    local iconTexture = go.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    local amountLabel = go.transform:Find("AmountLabel"):GetComponent(typeof(UILabel))
    local qualitySprite = go.transform:Find("QualitySprite"):GetComponent(typeof(UISprite))
    local selectedSprite = go.transform:Find("SelectedSprite").gameObject

    selectedSprite:SetActive(hasReceived)--隐藏选中状态

    local itemId = info.ItemId
    if itemId == nil then itemId = "" end
    local itemTemplateId = info.TemplateId
    local isFake = info.IsFake
    iconTexture:Clear()
    amountLabel.text = ""
    qualitySprite.spriteName = CUICommonDef.GetItemCellBorder("")

    if IdPartition.IdIsItem(info.TemplateId) then
        local item = Item_Item.GetData(info.TemplateId)
        if item then
            iconTexture:LoadMaterial(item.Icon)
            amountLabel.text = (info.Num>1 and tostring(info.Num) or "")
            qualitySprite.spriteName = CUICommonDef.GetItemCellBorder("")
        end
    elseif IdPartition.IdIsEquip(info.TemplateId) then
        local equip = EquipmentTemplate_Equip.GetData(info.TemplateId)
        if equip then
            iconTexture:LoadMaterial(equip.Icon)
            amountLabel.text = (info.Num>1 and tostring(info.Num) or "")
            qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(CommonDefs.ConvertIntToEnum(typeof(EnumQualityType), equip.Color))
        end
    end

    CommonDefs.AddOnClickListener(go, DelegateFactory.Action_GameObject(function(go) self:OnAwardItemClick(go) end), false)
end

function CLuaMailContentView:OnAwardItemClick(go)

    for _,data in pairs(self.m_AwardItemTbl) do
        if data.go == go then
            CItemInfoMgr.ShowLinkItemTemplateInfo(data.templateId, data.isFake)
            if self.m_IsANeedChooseMail then
                data.lastSelectedTime = Time.realtimeSinceStartup
                go.transform:Find("SelectedSprite").gameObject:SetActive(true)
                self:HandleMailAwardItemChosen()
            end
            break
        end
    end
end

function CLuaMailContentView:HandleMailAwardItemChosen( )
    --检查选中数量有没有超出限定
    local earliestGo = nil
    local earliestSelectedTime = Time.realtimeSinceStartup + 1 --比当前时间多1s
    local cnt = 0
    for _,data in pairs(self.m_AwardItemTbl) do
        if data.go.transform:Find("SelectedSprite").gameObject.activeSelf then
            cnt = cnt + 1
            if data.lastSelectedTime < earliestSelectedTime then
                earliestSelectedTime = data.lastSelectedTime
                earliestGo = data.go
            end
        end
    end
    --如果选中的数量超过限定数量，把最早选中的道具取消选中
    if cnt > self.m_NeedChooseAwardItemNum and earliestGo then
        earliestGo.transform:Find("SelectedSprite").gameObject:SetActive(false)
    end
end

function CLuaMailContentView:GetCurrentSelectedAwardItems()
    if not self.m_NeedChooseAwardItemNum then return "" end --非N选一情况的邮件，不需要传递道具ID信息
    local idStr = ""
    for _,data in pairs(self.m_AwardItemTbl) do
        if data.go.transform:Find("SelectedSprite").gameObject.activeSelf then
            if idStr~="" then
                idStr = idStr..","..tostring(data.templateId)
            else
                idStr = idStr..tostring(data.templateId)
            end
        end
    end
    return idStr
end

function CLuaMailContentView:InitButtons(mail)
    --批量删除模式下领取奖励和删除邮件的按钮不可见
    if LuaMailMgr:GetBatchDeleteMode() then
        self.receiveAwardsBtn.gameObject:SetActive(false)
        self.deleteMailBtn.gameObject:SetActive(false)
        return
    else
        self.receiveAwardsBtn.gameObject:SetActive(true)
        self.deleteMailBtn.gameObject:SetActive(true)
    end

    if self:DisableOperationOnFakeMail() then
        self.receiveAwardsBtn.gameObject:SetActive(not mail.IsFake)
        self.deleteMailBtn.gameObject:SetActive(not mail.IsFake)
    end

    if mail.CanAcceptAwards and mail.IsHandled == 0 then
        self.receiveAwardsBtn.Text = LocalString.GetString("领取奖励")
        self.receiveAwardsBtn.Enabled = true
        self.deleteMailBtn.Enabled = false
        self.deleteMailBtn.gameObject:SetActive(false) --不可用时隐藏
    elseif mail.CannotDelFromClient>0 then
        self.receiveAwardsBtn.Text = LocalString.GetString("奖励详情")
        self.receiveAwardsBtn.Enabled = true
        self.deleteMailBtn.Enabled = false
        self.deleteMailBtn.gameObject:SetActive(false) --不可用时隐藏
    else
        self.receiveAwardsBtn.Text = LocalString.GetString("领取奖励")
        self.receiveAwardsBtn.Enabled = false
        self.receiveAwardsBtn.gameObject:SetActive(false) --不可用时隐藏
        self.deleteMailBtn.Enabled = true
    end
end

function CLuaMailContentView:Replace(text, url, action)
    local width = 0
    local height = 0

    local parameters = CommonDefs.StringSplit_ArrayChar_StringSplitOptions(url, Table2ArrayWithCount({","}, 1, MakeArrayClass(String)), System.StringSplitOptions.RemoveEmptyEntries)

    if parameters.Length == 3 then
        self.curBannerUrl = parameters[0]
        width = tonumber(parameters[1]) or 100
        height = tonumber(parameters[2]) or 100

        local displayWidth = Mathf.Clamp(width, 100, self.maxBannerWidth)
        local displayHeight = Mathf.Max(1, Mathf.RoundToInt(height * (displayWidth / width)))
        self.banner.width = displayWidth
        self.banner.height = displayHeight
        local tex = CJingLingWebImageMgr.Inst:LoadImageByUrl(self.curBannerUrl)
        if tex ==nil then
            CJingLingWebImageMgr.Inst:DownloadImage(self.curBannerUrl)
        else
            self:LoadImage(tex)
        end
    else
        return text
    end
    return ""
end

function CLuaMailContentView:ReplaceContent(text,_id)
  local id = tonumber(_id)
  if id and id > 0 then
    local data = FestivalGift_MailContent.GetData(id)
    if data then
      return CChatLinkMgr.TranslateToNGUIText(data.MailContent, false)
    end
  end
  return ''
end

function CLuaMailContentView:InitBannerAndContent(origin)
    self.curBannerUrl = nil
    local result = origin
    if not System.String.IsNullOrEmpty(origin) then
        result = string.gsub(result, "(<banner=([^%s]-)>)", function (text, url)
            return self:Replace(text, url, nil)
        end)

        result = string.gsub(result, "(<banner=([^%s]-)(%s+)action=([^%s]-)>)", function (text, url,blankspace,action)
            return self:Replace(text, url, action)
        end)

        result = string.gsub(result, "(<content=([^%s]-)>)", function (text, id)
            return self:ReplaceContent(text, id)
        end)
    end

    SAFE_CALL(function()
        local bChanged, replacedContent = CLuaMailContentReplaceMgr:GetReplacedContent(self.curMailId, result)
        if bChanged then
            result = replacedContent
        end
    end)

    self.contentTextLabel.text = result
    self.banner.gameObject:SetActive(not System.String.IsNullOrEmpty(self.curBannerUrl))
    if self.banner.gameObject.activeSelf then
        local bgTransform = self.banner.transform:GetChild(0)
        if bgTransform then
            bgTransform:GetComponent(typeof(UIWidget)):ResetAndUpdateAnchors()
        end
    end
end

function CLuaMailContentView:OnAcceptAwardsBtnClick( go)

    local mail = CMailMgr.Inst:GetMail(self.curMailId)

    if mail == nil then
        return
    end
    if mail.CanAcceptAwards and mail.IsHandled == 0 then
        --请求领取奖励
        local idStr = self:GetCurrentSelectedAwardItems()
        if self.m_IsANeedChooseMail and idStr == "" then
            g_MessageMgr:ShowMessage("Mail_Need_Choose_Award_Item")
            return
        end
        CMailMgr.Inst:HandleMail(mail.MailId, idStr)
        self.receiveAwardsBtn.Enabled = false -- 领奖的时候先暂时禁用领奖按钮，避免重复点击，待领奖结果返回后再恢复状态
    elseif mail.CannotDelFromClient>0 then
        self:ExecuteClientAction(mail)
        -- 无需按钮禁用
    end
end

function CLuaMailContentView:OnDeleteMailBtnClick(go)
    local mail = CMailMgr.Inst:GetMail(self.curMailId)

    if mail == nil then
        return
    end

    if not (mail.CanAcceptAwards and mail.IsHandled == 0) and mail.CannotDelFromClient<1  then
         --删除邮件
        Gac2Gas.RequestDelMail(mail.MailId)
    end
    self.deleteMailBtn.Enabled = false --删除的时候先暂时禁用删除按钮，避免重复点击，待删除结果返回后再恢复状态
end

function CLuaMailContentView:ExecuteClientAction(mail)
    local actionStr = mail.ClientAction
    if actionStr == nil or actionStr == "" then return end
    ClientAction.DoAction(actionStr)
end

function CLuaMailContentView:OnContentTextLabelClick( go)

    local url = self.contentTextLabel:GetUrlAtPosition(UICamera.lastWorldPosition)
    if url ~= nil then
        local dict = CreateFromClass(MakeGenericClass(Dictionary, cs_string, Object))
        CommonDefs.DictAdd_LuaCall(dict, "context", "mail")
        CChatLinkMgr.ProcessLinkClick(url, LinkSource.Mail, dict)
    end
end

function CLuaMailContentView:OnBannerClick( go)

end

function CLuaMailContentView:OnEnable( )

    --监听领取奖励是否成功
    g_ScriptEvent:AddListener("MailAcceptAwardsDone", self, "OnAcceptAwardsDone")
    g_ScriptEvent:AddListener("OnMailInfoRequestDone", self, "OnMailInfoRequestDone")
    g_ScriptEvent:AddListener("HandleMailNotDelSuccess", self, "HandleMailNotDelSuccess")
    --监邮件删除回调
    g_ScriptEvent:AddListener("MailDeleteDone", self, "OnDeleteDone")
    g_ScriptEvent:AddListener("OnJingLingWebImageReady", self, "OnImageReady")
    g_ScriptEvent:AddListener("MailBatchDeleteModeChanged", self, "MailBatchDeleteModeChanged")
end
function CLuaMailContentView:OnDisable( )

    --监听是否有新邮件到来
    g_ScriptEvent:RemoveListener("MailAcceptAwardsDone", self, "OnAcceptAwardsDone")
    g_ScriptEvent:RemoveListener("OnMailInfoRequestDone", self, "OnMailInfoRequestDone")
    g_ScriptEvent:RemoveListener("HandleMailNotDelSuccess", self, "HandleMailNotDelSuccess")
    --监邮件删除回调
    g_ScriptEvent:RemoveListener("MailDeleteDone", self, "OnDeleteDone")
    g_ScriptEvent:RemoveListener("OnJingLingWebImageReady", self, "OnImageReady")
    g_ScriptEvent:RemoveListener("MailBatchDeleteModeChanged", self, "MailBatchDeleteModeChanged")
end
function CLuaMailContentView:OnAcceptAwardsDone(args)
    local success = args[0]
    local mailId = args[1]
    local mail = CMailMgr.Inst:GetMail(mailId)
    if mail ~= nil and mail.MailId == mailId then
        self:ShowMailContent(mail)
    end
end
function CLuaMailContentView:OnMailInfoRequestDone(args)
    local templateId = args[0]
    local info = args[1]
    local mail = CMailMgr.Inst:GetMail(self.curMailId)
    if mail ~= nil and mail.TemplateId == templateId then
        local builder = NewStringBuilderWraper(StringBuilder)
        builder:Append(mail.Content)
        builder:Append("\n\n")
        if info.Exp > 0 then
            builder:Append(LocalString.GetString("\n经验:"))
            CommonDefs.StringBuilder_Append_StringBuilder_UInt32(builder, info.Exp)
        end
        if info.FreeSilver > 0 then
            builder:Append(LocalString.GetString("\n银票:"))
            CommonDefs.StringBuilder_Append_StringBuilder_UInt32(builder, info.FreeSilver)
        end
        if info.Silver > 0 then
            builder:Append(LocalString.GetString("\n银两:"))
            CommonDefs.StringBuilder_Append_StringBuilder_UInt32(builder, info.Silver)
        end
        if info.YuanBao > 0 then
            builder:Append(LocalString.GetString("\n元宝:"))
            CommonDefs.StringBuilder_Append_StringBuilder_UInt32(builder, info.YuanBao)
        end
        self:InitBannerAndContent(ToStringWrap(builder))
        --排序
        self.mailContentTable:Reposition()
        self.mailContentScrollView:ResetPosition()
    end
end

function CLuaMailContentView:HandleMailNotDelSuccess(mailId)
    if self.curMailId~=mailId then return end
    local mail = CMailMgr.Inst:GetMail(self.curMailId)
    self:ShowMailContent(mail)
end

function CLuaMailContentView:OnDeleteDone(args)
    local success = args[0]
    local mailId = args[1]
    local mail = CMailMgr.Inst:GetMail(self.curMailId)
    if mail ~= nil and mail.MailId == mailId then
        self:InitButtons(mail)
    end
end
function CLuaMailContentView:OnImageReady(args)
    local url = args[0]
    if self.curBannerUrl == url then
        local tex = CJingLingWebImageMgr.Inst:LoadImageByUrl(url)
        self:LoadImage(tex)
    end
end

function CLuaMailContentView:LoadImage(tex)
    if self.banner.mainTexture ~= nil then
        GameObject.Destroy(self.banner.mainTexture)
    end
    self.banner.mainTexture = tex
end

function CLuaMailContentView:MailBatchDeleteModeChanged( )
    local mail = CMailMgr.Inst:GetMail(self.curMailId)

    if mail == nil then
        return
    end
    self:InitButtons(mail)
end
