-- Auto Generated!!
local CSelectExpressionItem = import "L10.UI.CSelectExpressionItem"
local Expression_Show = import "L10.Game.Expression_Show"
CSelectExpressionItem.m_Init_CS2LuaHook = function (this, info) 
    this.actionInfo = info
    local expressionAction = Expression_Show.GetData(this.actionInfo:GetID())
    if expressionAction ~= nil then
        this.icon:LoadMaterial(this.actionInfo:GetIcon())
        this.nameLabel.text = this.actionInfo:GetName()
    end
end
