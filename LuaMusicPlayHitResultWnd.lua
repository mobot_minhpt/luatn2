local CUIFx = import "L10.UI.CUIFx"
local UILabel = import "UILabel"

local GameObject = import "UnityEngine.GameObject"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CUICommonDef = import "L10.UI.CUICommonDef"

LuaMusicPlayHitResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaMusicPlayHitResultWnd, "QingFx", "QingFx", CUIFx)
RegistChildComponent(LuaMusicPlayHitResultWnd, "SongName", "SongName", UILabel)
RegistChildComponent(LuaMusicPlayHitResultWnd, "SongDifficulty", "SongDifficulty", UILabel)
RegistChildComponent(LuaMusicPlayHitResultWnd, "TotalScore", "TotalScore", UILabel)
RegistChildComponent(LuaMusicPlayHitResultWnd, "FinishLevelLabel", "FinishLevelLabel", UILabel)
RegistChildComponent(LuaMusicPlayHitResultWnd, "SinglePlayerModel", "SinglePlayerModel", GameObject)
RegistChildComponent(LuaMusicPlayHitResultWnd, "TwoPlayerModel", "TwoPlayerModel", GameObject)
RegistChildComponent(LuaMusicPlayHitResultWnd, "RankBtn", "RankBtn", GameObject)
RegistChildComponent(LuaMusicPlayHitResultWnd, "ShareBtn", "ShareBtn", GameObject)
RegistChildComponent(LuaMusicPlayHitResultWnd, "ComboNumLabel", "ComboNumLabel", UILabel)
RegistChildComponent(LuaMusicPlayHitResultWnd, "PerfectCount", "PerfectCount", UILabel)
RegistChildComponent(LuaMusicPlayHitResultWnd, "NiceCount", "NiceCount", UILabel)
RegistChildComponent(LuaMusicPlayHitResultWnd, "GoodCount", "GoodCount", UILabel)
RegistChildComponent(LuaMusicPlayHitResultWnd, "MissCount", "MissCount", UILabel)
RegistChildComponent(LuaMusicPlayHitResultWnd, "Player1", "Player1", GameObject)
RegistChildComponent(LuaMusicPlayHitResultWnd, "Player2", "Player2", GameObject)
RegistChildComponent(LuaMusicPlayHitResultWnd, "PerfectComboTag", "PerfectComboTag", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaMusicPlayHitResultWnd,"m_CurModel")
RegistClassMember(LuaMusicPlayHitResultWnd,"m_CurDifficulty")
function LuaMusicPlayHitResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.RankBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRankBtnClick()
	end)

	UIEventListener.Get(self.ShareBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareBtnClick()
	end)

    --@endregion EventBind end
end

function LuaMusicPlayHitResultWnd:OnDisable()
	LuaMeiXiangLouMgr.m_TwoPlayerResultData = nil
	LuaMeiXiangLouMgr.m_SingleResultData = nil
end

function LuaMusicPlayHitResultWnd:Init()
	self.QingFx:LoadFx("Fx/UI/Prefab/UI_qsq_qing.prefab")

	self.m_CurModel = LuaMeiXiangLouMgr.m_CurPlayModel
    self.m_CurDifficulty= LuaMeiXiangLouMgr.m_CurPlayDifficulty

	if LuaMeiXiangLouMgr.m_TwoPlayerResultData and next(LuaMeiXiangLouMgr.m_TwoPlayerResultData) then--单人
		self:InitTwoPlayerView()
	else
		self:InitSingleView()
	end
	self:InitSongInfoView()
	--分享按钮屏蔽vn hmt
	self.ShareBtn.gameObject:SetActive(CommonDefs.IS_CN_CLIENT)
end

function LuaMusicPlayHitResultWnd:InitSongInfoView()
	local songId = LuaMeiXiangLouMgr.m_CurPlaySongId
	local songData = MeiXiangLou_SongInfo.GetData(songId)
	self.SongName.text = SafeStringFormat3(LocalString.GetString("《%s》"),songData.Name)
	
	local difName = songData.Difficulty
    if difName == LocalString.GetString("简单") then
        difName = LocalString.GetString("入门")
    elseif difName == LocalString.GetString("中等") then
        difName = LocalString.GetString("高手")
    elseif difName == LocalString.GetString("困难") then
        difName = LocalString.GetString("挑战")
    end
	self.SongDifficulty.text = difName
end

function LuaMusicPlayHitResultWnd:InitSingleView()
	self.SinglePlayerModel:SetActive(true)
	self.TwoPlayerModel:SetActive(false)
	local singledata = LuaMeiXiangLouMgr.m_SingleResultData
	self.TotalScore.text = SafeStringFormat3(LocalString.GetString("总分 %d"),LuaMeiXiangLouMgr.m_SingleResultData[2])
	self.ComboNumLabel.text = SafeStringFormat3(LocalString.GetString("连击  [c][fff7a3]%d[-][/c]"),singledata[7])
	self.PerfectComboTag:SetActive(singledata[6]==0)
	self.PerfectCount.text = singledata[3]
	self.NiceCount.text = singledata[4]
	self.GoodCount.text = singledata[5]
	self.MissCount.text = singledata[6]

	local score = tonumber(LuaMeiXiangLouMgr.m_SingleResultData[2])
	self:InitLevelTitle(score)
end

function LuaMusicPlayHitResultWnd:InitTwoPlayerView()
	self.SinglePlayerModel:SetActive(false)
	self.TwoPlayerModel:SetActive(true)
	local data = LuaMeiXiangLouMgr.m_TwoPlayerResultData
	self.TotalScore.text = SafeStringFormat3(LocalString.GetString("总分 %d"),data[1])

	local myPlayer = CClientMainPlayer.Inst
	
	local id1 = data[2]
	local namelabel1 = self.Player1.transform:Find("PlayerName"):GetComponent(typeof(UILabel))
	local scoreComboLabel1 = self.Player1.transform:Find("ScoreComboLabel"):GetComponent(typeof(UILabel))
	local perfectComboTag1 = self.Player1.transform:Find("ComboLevelLabel")
	local icon1 = self.Player1.transform:Find("Icon"):GetComponent(typeof(CUITexture))

	scoreComboLabel1.text = SafeStringFormat3(LocalString.GetString("得分 %d 连击 %d"),data[6],data[7])

	local id2 = data[5]
	local namelabel2 = self.Player2.transform:Find("PlayerName"):GetComponent(typeof(UILabel))
	local scoreComboLabel2 = self.Player2.transform:Find("ScoreComboLabel"):GetComponent(typeof(UILabel))
	local perfectComboTag2 = self.Player2.transform:Find("ComboLevelLabel")
	local icon2 = self.Player2.transform:Find("Icon"):GetComponent(typeof(CUITexture))
	scoreComboLabel2.text = SafeStringFormat3(LocalString.GetString("得分 %d 连击 %d"),data[12],data[13])

	namelabel1.text = data[3]
	namelabel2.text = data[9]
	icon1:LoadNPCPortrait(CUICommonDef.GetPortraitName(data[4], data[5], -1), false)
	icon2:LoadNPCPortrait(CUICommonDef.GetPortraitName(data[10], data[11], -1), false)

	--完美连击
	local songInfo = MeiXiangLou_SongInfo.GetData(LuaMeiXiangLouMgr.m_CurPlaySongId)
	if songInfo then
		local leaderBeatNum,otherBeatNum=0,0
		for idx = songInfo.StartId,songInfo.EndId,1 do
			local info = MeiXiangLou_Song.GetData(idx)	
			for cStr in string.gmatch(info.Buttons, "([^;]+)") do
                for btnzhStr in string.gmatch(cStr, "([^,]+)") do
                    if info.Operator == 1 then
						leaderBeatNum = leaderBeatNum + 1
					elseif info.Operator == 2 then
						otherBeatNum = otherBeatNum + 1
					end
                end   
            end
		end
		perfectComboTag1.gameObject:SetActive(data[7]==leaderBeatNum)
		perfectComboTag2.gameObject:SetActive(data[13]==otherBeatNum)
	else
		perfectComboTag1.gameObject:SetActive(false)
		perfectComboTag2.gameObject:SetActive(false)
	end

	local score = tonumber(data[1])
	self:InitLevelTitle(score)

end

function LuaMusicPlayHitResultWnd:InitLevelTitle(score)
	local title = ""
	local data = MeiXiangLou_SongInfo.GetData(LuaMeiXiangLouMgr.m_CurPlaySongId)
	if not data then return end
	if score >= data.GodlikeScore then
		title = LocalString.GetString("琴圣")
	elseif score >= data.ProfessionalScore then
		title = LocalString.GetString("国手")
	elseif score >= data.CheafScore then
		title = LocalString.GetString("首席")
	elseif score >= data.WorkerScore then
		title = LocalString.GetString("匠人")	
	elseif score >= data.StudentScore then
		title = LocalString.GetString("学徒")	
	else
		title = LocalString.GetString("外行")	
	end
	self.FinishLevelLabel.text = title
end

--@region UIEvent

function LuaMusicPlayHitResultWnd:OnRankBtnClick()
	LuaMeiXiangLouMgr.OpenMusicPlayRankWnd(LuaMeiXiangLouMgr.m_CurPlaySongId)
end

function LuaMusicPlayHitResultWnd:OnShareBtnClick()
	CUICommonDef.CaptureScreenAndShare()    
end


--@endregion UIEvent

