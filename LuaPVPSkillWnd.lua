local CPVPSkillPreferenceTopView = import "L10.UI.CPVPSkillPreferenceTopView"
local CPVPSkillPreferenceBottomView = import "L10.UI.CPVPSkillPreferenceBottomView"

LuaPVPSkillWnd = class()
RegistClassMember(LuaPVPSkillWnd,"m_TopView")
RegistClassMember(LuaPVPSkillWnd,"m_BottomView")

function LuaPVPSkillWnd:Init()
    self.m_TopView = self.transform:Find("Anchor/TopView"):GetComponent(typeof(CPVPSkillPreferenceTopView))
    self.m_BottomView = self.transform:Find("Anchor/BottomView"):GetComponent(typeof(CPVPSkillPreferenceBottomView))

    self.m_TopView.OnSkillClassSelect = DelegateFactory.Action_uint(function (skillClassId)
        self:OnSkillClassSelect(skillClassId)
    end)
    self.m_TopView.OnDragComplete = DelegateFactory.Action_uint_GameObject(function (skillId, destGo)
        self:OnDragComplete(skillId, destGo)
    end)

    self.m_TopView:Init(0)
	
end

function LuaPVPSkillWnd:OnEnable( ... )
    -- body
end

function LuaPVPSkillWnd:OnDisable( ... )
    -- body
end

function LuaPVPSkillWnd:OnSkillClassSelect(skillClassId)
    -- body
end

function LuaPVPSkillWnd:OnDragComplete(skillId, destGo)
    if self.m_BottomView then
        self.m_BottomView:ReplaceSkill(skillId, destGo, 0)
    end
end

