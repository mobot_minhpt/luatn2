require("common/common_include")

local UILabel = import "UILabel"
local GameObject  = import "UnityEngine.GameObject"
local CItemMgr = import "L10.Game.CItemMgr"
local CUITexture = import "L10.UI.CUITexture"

CLuaBaoWeiNanGuaResultWnd = class()

RegistChildComponent(CLuaBaoWeiNanGuaResultWnd, "succPanel", GameObject)
RegistChildComponent(CLuaBaoWeiNanGuaResultWnd, "closeBtn", GameObject)
RegistChildComponent(CLuaBaoWeiNanGuaResultWnd, "failPanel", GameObject)
RegistChildComponent(CLuaBaoWeiNanGuaResultWnd, "succItemNode", GameObject)
RegistChildComponent(CLuaBaoWeiNanGuaResultWnd, "succItemNameLabel", UILabel)
RegistChildComponent(CLuaBaoWeiNanGuaResultWnd, "succSlider", UISlider)
RegistChildComponent(CLuaBaoWeiNanGuaResultWnd, "succNum", UILabel)

function CLuaBaoWeiNanGuaResultWnd:Init()
  UIEventListener.Get(self.closeBtn).onClick = LuaUtils.VoidDelegate(function (go)
      CUIManager.CloseUI("BaoWeiNanGuaResultWnd")
  end)

  if LuaBaoWeiNanGuaMgr.result then
    self.succPanel:SetActive(true)
    self.succSlider.value = LuaBaoWeiNanGuaMgr.resultPercent / 100
    self.succNum.text = LuaBaoWeiNanGuaMgr.resultPercent

    local itemInfo = CItemMgr.Inst:GetItemTemplate(LuaBaoWeiNanGuaMgr.resultItem)
    if itemInfo then
      self.succItemNameLabel.text = itemInfo.Name
      self.succItemNode:GetComponent(typeof(CUITexture)):LoadMaterial(itemInfo.Icon)
    end
    self.failPanel:SetActive(false)
  else
    self.succPanel:SetActive(false)
    self.failPanel:SetActive(true)
  end
end

return CLuaBaoWeiNanGuaResultWnd
