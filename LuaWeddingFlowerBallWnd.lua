local DelegateFactory = import "DelegateFactory"
local CServerTimeMgr  = import "L10.Game.CServerTimeMgr"
local Ease            = import "DG.Tweening.Ease"

LuaWeddingFlowerBallWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaWeddingFlowerBallWnd, "name")
RegistClassMember(LuaWeddingFlowerBallWnd, "inviteButton")
RegistClassMember(LuaWeddingFlowerBallWnd, "dissButton")
RegistClassMember(LuaWeddingFlowerBallWnd, "supportButton")
RegistClassMember(LuaWeddingFlowerBallWnd, "closeButton")
RegistClassMember(LuaWeddingFlowerBallWnd, "closeLabel")

RegistClassMember(LuaWeddingFlowerBallWnd, "tick")
RegistClassMember(LuaWeddingFlowerBallWnd, "endTimeStamp")
RegistClassMember(LuaWeddingFlowerBallWnd, "curBubbleNum")
RegistClassMember(LuaWeddingFlowerBallWnd, "word")
RegistClassMember(LuaWeddingFlowerBallWnd, "bubbleTable")


function LuaWeddingFlowerBallWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self:InitUIComponents()
    self:InitEventListener()
    self:InitActive()
end

-- 初始化UI组件
function LuaWeddingFlowerBallWnd:InitUIComponents()
    local anchor = self.transform:Find("Anchor")
    self.name = anchor:Find("Name"):GetComponent(typeof(UILabel))
    self.word = anchor:Find("Panel"):Find("Word")
    self.bubbleTable = anchor:Find("Panel"):Find("BubbleTable")

    self.inviteButton = anchor:Find("Grid"):Find("InviteButton").gameObject
    self.dissButton = anchor:Find("Grid"):Find("DissButton").gameObject
    self.supportButton = anchor:Find("Grid"):Find("SupportButton").gameObject

    self.closeButton = self.transform:Find("BgWnd"):Find("CloseButton").gameObject
    self.closeLabel = self.transform:Find("BgWnd"):Find("CloseLabel"):GetComponent(typeof(UILabel))
end

-- 初始化按键响应
function LuaWeddingFlowerBallWnd:InitEventListener()
	UIEventListener.Get(self.inviteButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnInviteButtonClick()
	end)

    UIEventListener.Get(self.dissButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnDissButtonClick()
	end)

    UIEventListener.Get(self.supportButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSupportButtonClick()
	end)

    UIEventListener.Get(self.closeButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCloseButtonClick()
	end)
end

-- 初始化active
function LuaWeddingFlowerBallWnd:InitActive()
    self.closeButton:SetActive(false)
    self.word.gameObject:SetActive(false)
end


function LuaWeddingFlowerBallWnd:OnEnable()
    g_ScriptEvent:AddListener("NewWeddingSyncXiuQiuMessage", self, "OnXiuQiuMessage")
end

function LuaWeddingFlowerBallWnd:OnDisable()
    g_ScriptEvent:RemoveListener("NewWeddingSyncXiuQiuMessage", self, "OnXiuQiuMessage")
end

function LuaWeddingFlowerBallWnd:OnXiuQiuMessage(msgId)
    if msgId == 1 then
        self:CreateBubble("Invite")
    elseif msgId == 2 then
        self:CreateBubble("Diss")
    elseif msgId == 3 then
        self:CreateBubble("Support")
    end
end


function LuaWeddingFlowerBallWnd:Init()
    self:StartTick()
    self.curBubbleNum = 0
    Extensions.RemoveAllChildren(self.bubbleTable)
    self:InitAvatar()
end

-- 开始计时器
function LuaWeddingFlowerBallWnd:StartTick()
	self:ClearTick()
    self.endTimeStamp = CServerTimeMgr.Inst.timeStamp + 30
    self:UpdateCloseLabel()
	self.tick = RegisterTick(function()
        self:UpdateCloseLabel()
	end, 1000)
end

-- 更新倒计时显示
function LuaWeddingFlowerBallWnd:UpdateCloseLabel()
    local count = math.floor(self.endTimeStamp - CServerTimeMgr.Inst.timeStamp)
    if count <= 0 then
        CUIManager.CloseUI(CLuaUIResources.WeddingFlowerBallWnd)
        return
    end
    self.closeLabel.text = SafeStringFormat3(LocalString.GetString("%d秒"), count)
end

-- 创建一个气泡
function LuaWeddingFlowerBallWnd:CreateBubble(type)
    if self.curBubbleNum >= 10 then return end

    local child = nil
    if self.bubbleTable.childCount > self.curBubbleNum then
        child = self.bubbleTable:GetChild(self.curBubbleNum)
    else
        child = CUICommonDef.AddChild(self.bubbleTable.gameObject, self.word.gameObject).transform
    end

    child.gameObject:SetActive(true)
    self:SetBubbleDepth(child)
    self:SetBubblePos(child)
    self:SetBubbleWord(child, type)

    self.curBubbleNum = self.curBubbleNum + 1
    local tweenPosY = LuaTweenUtils.TweenPositionY(child, 270, 2)
    LuaTweenUtils.OnComplete(LuaTweenUtils.SetEase(tweenPosY, Ease.Linear), function()
        self:OnTweenFinish(child)
    end)
end

-- 设置气泡x坐标
function LuaWeddingFlowerBallWnd:SetBubblePos(trans)
    local pos = trans.localPosition
    pos.x = math.random(-272, 16)
    pos.y = -155
    trans.localPosition = pos
end

-- 设置气泡深度
function LuaWeddingFlowerBallWnd:SetBubbleDepth(wordTrans)
    local id = wordTrans:GetSiblingIndex()

    local word = wordTrans:GetComponent(typeof(UIWidget))
    word.depth = word.depth % 100 + id * 100

    for i = 1, wordTrans.childCount do
        local child = wordTrans:GetChild(i - 1)
        local widget = child:GetComponent(typeof(UIWidget))
        if widget then
            widget.depth = widget.depth % 100 + id * 100
        end
    end
end

-- 设置气泡显示
function LuaWeddingFlowerBallWnd:SetBubbleWord(wordTrans, type)
    local choice = nil
    if type == "Invite" then
        choice = WeddingIteration_Setting.GetData().FlowerBallInviteWord
    elseif type == "Diss" then
        choice = WeddingIteration_Setting.GetData().FlowerBallDissWord
    elseif type == "Support" then
        choice = WeddingIteration_Setting.GetData().FlowerBallSupportWord
    end

    choice = LuaWeddingIterationMgr:Array2Tbl(choice)
    if not choice then return end

    local wordLabel = wordTrans:GetComponent(typeof(UILabel))
    local id = math.random(1, #choice)
    LuaWeddingIterationMgr:SetBubbleWord(wordLabel, choice[id])
end

function LuaWeddingFlowerBallWnd:OnTweenFinish(child)
    if not child then return end
    child.transform:SetSiblingIndex(self.bubbleTable.childCount - 1)
    child.gameObject:SetActive(false)
    self.curBubbleNum = self.curBubbleNum - 1

    for i = 1, self.curBubbleNum do
        self:SetBubbleDepth(self.bubbleTable:GetChild(i - 1))
    end
end

-- 初始化头像
function LuaWeddingFlowerBallWnd:InitAvatar()
    local info = LuaWeddingIterationMgr.flowerBallInfo

    local mat = CUICommonDef.GetPortraitName(info.playerClass, info.playerGender, 6)
    local avatar = self.transform:Find("Anchor"):Find("Avatar"):GetComponent(typeof(CUITexture))
    avatar:LoadNPCPortrait(mat)

    self.name.text = info.playerName
end

-- 清除计时器
function LuaWeddingFlowerBallWnd:ClearTick()
	if self.tick then
		UnRegisterTick(self.tick)
		self.tick = nil
	end
end

function LuaWeddingFlowerBallWnd:OnDestroy()
    self:ClearTick()
end

--@region UIEvent

function LuaWeddingFlowerBallWnd:OnInviteButtonClick()
    Gac2Gas.NewWeddingRequestXiuQiuDate()
end

function LuaWeddingFlowerBallWnd:OnDissButtonClick()
    Gac2Gas.NewWeddingRequestXiuQiuDiss()
end

function LuaWeddingFlowerBallWnd:OnSupportButtonClick()
    Gac2Gas.NewWeddingRequestXiuQiuSupport()
end

function LuaWeddingFlowerBallWnd:OnCloseButtonClick()
    CUIManager.CloseUI(CLuaUIResources.WeddingFlowerBallWnd)
end

--@endregion UIEvent

