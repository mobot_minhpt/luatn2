local CThumbnailChatWnd = import "L10.UI.CThumbnailChatWnd"

LuaThumbnailChatWnd = {}
LuaThumbnailChatWnd.m_Wnd = nil

function LuaThumbnailChatWnd:ShowOrHideWnd(isShow)
    if LuaThumbnailChatWnd.m_Wnd then
        LuaThumbnailChatWnd.m_Wnd.background.transform.parent.gameObject:SetActive(isShow)
    end
end

CThumbnailChatWnd.m_hookOnEnable = function(this)
    this:IPhoneXAdaptation()
    this.container.alpha = 1.0
    EventManager.AddListenerInternal(EnumEventType.RecvChatMsg, MakeDelegateFromCSFunction(this.OnRecvChatMsg, MakeGenericClass(Action1, UInt32), this))
    EventManager.AddListenerInternal(EnumEventType.JoyStickDragging, MakeDelegateFromCSFunction(this.OnJoyStickDragging, MakeGenericClass(Action2, Vector3, Boolean), this))
    EventManager.AddListenerInternal(EnumEventType.OnWithdrawPlayerChatMsg, MakeDelegateFromCSFunction(this.OnWithdrawPlayerMsg, MakeGenericClass(Action2, UInt64, MakeGenericClass(HashSet, String)), this))
    EventManager.AddListenerInternal(EnumEventType.OnWithdrawPlayerChatVoiceMsg, MakeDelegateFromCSFunction(this.OnWithdrawPlayerChatVoiceMsg, MakeGenericClass(Action3, UInt64, UInt32, String), this))
    EventManager.AddListenerInternal(EnumEventType.MainPlayerCreated, MakeDelegateFromCSFunction(this.OnMainPlayerCreated, Action0, this))
    LuaThumbnailChatWnd.m_Wnd = this
    g_ScriptEvent:AddListener("ShowOrHideThumbnailChatWnd",LuaThumbnailChatWnd,"ShowOrHideWnd")
end

CThumbnailChatWnd.m_hookOnDisable = function(this)
    EventManager.RemoveListenerInternal(EnumEventType.RecvChatMsg, MakeDelegateFromCSFunction(this.OnRecvChatMsg, MakeGenericClass(Action1, UInt32), this))
    EventManager.RemoveListenerInternal(EnumEventType.JoyStickDragging, MakeDelegateFromCSFunction(this.OnJoyStickDragging, MakeGenericClass(Action2, Vector3, Boolean), this))
    EventManager.RemoveListenerInternal(EnumEventType.OnWithdrawPlayerChatMsg, MakeDelegateFromCSFunction(this.OnWithdrawPlayerMsg, MakeGenericClass(Action2, UInt64, MakeGenericClass(HashSet, String)), this))
    EventManager.RemoveListenerInternal(EnumEventType.OnWithdrawPlayerChatVoiceMsg, MakeDelegateFromCSFunction(this.OnWithdrawPlayerChatVoiceMsg, MakeGenericClass(Action3, UInt64, UInt32, String), this))
    EventManager.RemoveListenerInternal(EnumEventType.MainPlayerCreated, MakeDelegateFromCSFunction(this.OnMainPlayerCreated, Action0, this))
    LuaThumbnailChatWnd.m_Wnd = nil
    g_ScriptEvent:RemoveListener("ShowOrHideThumbnailChatWnd",LuaThumbnailChatWnd,"ShowOrHideWnd")
end