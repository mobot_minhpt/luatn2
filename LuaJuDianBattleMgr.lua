local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CScene = import "L10.Game.CScene"
local PlayerPrefs = import "UnityEngine.PlayerPrefs"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"

LuaJuDianBattleMgr = class()
LuaJuDianBattleMgr.InfoWndTab = 0
LuaJuDianBattleMgr.PlayingShowMode = 0

LuaJuDianBattleMgr.JuDianProgressData = nil
LuaJuDianBattleMgr.JuDianPeopleData = nil

LuaJuDianBattleMgr.SceneFlagData = nil
LuaJuDianBattleMgr.SceneJuDianData = nil

LuaJuDianBattleMgr.GuildRankData = nil

-- 显示据点相关按钮
LuaJuDianBattleMgr.NeedShowPlayerJuDianInfo = nil

-- 战意等级
LuaJuDianBattleMgr.ZhanYiLevel = 0

-- 是否是结算界面
LuaJuDianBattleMgr.IsResultShow = false
LuaJuDianBattleMgr.TopList = nil
LuaJuDianBattleMgr.GuildList = nil

-- 具体占领信息
LuaJuDianBattleMgr.DetailWndGuildId = 0
LuaJuDianBattleMgr.DetailWndGuildName = ""
-- 是否在据点内
LuaJuDianBattleMgr.IsInsideJuDian = false

-- 外围玩法怪物数据
LuaJuDianBattleMgr.SceneMonsterData = nil

-- 是否可以打开据点贡献
LuaJuDianBattleMgr.CanOpenContributionWnd = false

-- 据点站结果界面
LuaJuDianBattleMgr.ResultData = nil

-- 赛季信息
LuaJuDianBattleMgr.SeasonData = nil

-- 战意图标等级
JuDianZhanYiTexture = {
    Level1 = "UI/Texture/Transparent/Material/judianbattle_zhanyi_01.mat",
    Level2 = "UI/Texture/Transparent/Material/judianbattle_zhanyi_02.mat",
    Level3 = "UI/Texture/Transparent/Material/judianbattle_zhanyi_03.mat",
}

JuDianColor = {
	Battle = Color(191/255,126/255,32/255),
	Self = Color(0/255,178/255,67/255),
	Enemy = Color(152/255,0/255,0/255),
}

JuDianTransparentColor = {
	Battle = Color(191/255,126/255,32/255,40/255),
	Self = Color(0/255,178/255,67/255, 40/255),
	Enemy = Color(152/255,0/255,0/255,40/255),
}

JuDianLabelColor = {
	Battle = Color(1,254/255,145/255),
	Self = Color(0,1,96/255),
	Enemy = Color(1,80/255,80/255),
}

EnumJuDianInfoWndTab = {
    Rank = 0,
	SelfGuild = 1,
}

EnumJuDianPlayStage = {
	eInit           = 0,
	eSignUp         = 1,
	eAfterMatch     = 2,
	ePrepare        = 3,
	eInFight        = 4,
	eFightEnd       = 5,
	eSummary        = 6,
	eCleanUp        = 7,
}

EnumJuDianPlayingShowMode = {
	Nothing = 0,
    -- 玩法开始后且玩家在据点战副本的非占领场景时，显示前4名帮会及自己帮会的占领分数据
    GuildScore = 1,
    -- 玩法开始后且玩家在某个占领场景时，但不在某个占领点时，显示该分线各占领点的信息
    JuDianOverview = 2,
    -- 玩法开始后且玩家在某个占领点时，显示该占领点的占领进度、人数排名前3的帮会及自己帮会
    JuDianDetail = 3,
}

LuaJuDianBattleMgr.m_GamePlayId = nil
function LuaJuDianBattleMgr:IsInGameplay()
    if self.m_GamePlayId == nil then
        self.m_GamePlayId = GuildOccupationWar_Setting.GetData().GamePlayID
    end
    if CScene.MainScene then
        local gamePlayId = CScene.MainScene.GamePlayDesignId
        if gamePlayId == self.m_GamePlayId then
            return true
        end
    end
    return false
end

LuaJuDianBattleMgr.m_DailyGamePlayId = nil
-- 外围玩法
function LuaJuDianBattleMgr:IsInDailyGameplay()
    if self.m_DailyGamePlayId == nil then
        self.m_DailyGamePlayId = GuildOccupationWar_DailySetting.GetData().GamePlayId
    end
    if CScene.MainScene then
        local gamePlayId = CScene.MainScene.GamePlayDesignId
        if gamePlayId == self.m_DailyGamePlayId then
            return true
        end
    end
    return false
end

-- 引导任务特殊处理
function LuaJuDianBattleMgr:IsInTaskGameplay()
    if CScene.MainScene then
        local gamePlayId = CScene.MainScene.GamePlayDesignId
        if gamePlayId == 51102965 or gamePlayId == 51102966 then
            return true
        end
    end
    return false
end

function LuaJuDianBattleMgr:SetPlayingShowMode(mode)
    self.PlayingShowMode = mode
end

function LuaJuDianBattleMgr:IsInJuDian()
    if CScene.MainScene then
        if CScene.MainScene.SceneTemplateId == 16102358 then
            -- 打开图片引导
            self:OpenImageGuide()
            return true
        end
    end
    return false
end

function LuaJuDianBattleMgr:GetMapId()
    if CScene.MainScene then
        return CScene.MainScene.SceneTemplateId
    end
    return 0
end

function LuaJuDianBattleMgr:GetSelfGuildId()
    local selfGuildId = 0
    if CClientMainPlayer.Inst then
        selfGuildId = CClientMainPlayer.Inst.BasicProp.GuildId
    end
    return selfGuildId
end

-- 显示战意等级提升
function LuaJuDianBattleMgr:ShowZhanYiLevelUp(level)
    self.ZhanYiLevel = level
    CUIManager.ShowUI("JuDianBattleLevelUpTipWnd")
end

-- 显示结束战果
function LuaJuDianBattleMgr:ShowResultWnd(topList, guildList)
    self.IsResultShow = true
    self.TopList = topList
    self.GuildList = guildList
    self:OpenInfoWnd(EnumJuDianInfoWndTab.Rank)
end

-- 打开战况界面的特定Tab
function LuaJuDianBattleMgr:OpenInfoWnd(tab)
    self.InfoWndTab = 0
    if tab == EnumJuDianInfoWndTab.SelfGuild or tab == EnumJuDianInfoWndTab.Rank then
        self.InfoWndTab = tab
    end
    if not CUIManager.IsLoaded(CLuaUIResources.JuDianBattleInfoWnd) then
        CUIManager.ShowUI("JuDianBattleInfoWnd")
    end
end

-- 打开帮会占领详情
function LuaJuDianBattleMgr:OpenGuildDetailWnd(guildId, guildName)
    self.DetailWndGuildId = guildId
    self.DetailWndGuildName = guildName
    CUIManager.ShowUI("JuDianBattleOccupationDetailWnd")
end

-- 打开世界地图
function LuaJuDianBattleMgr:OpenWorldMap()
    CLuaMiniMapPopWnd.m_ShowWorldMap = true
    CUIManager.ShowUI(CUIResources.MiniMapPopWnd)
end

-- 请求传回本服
function LuaJuDianBattleMgr:RequestBackSelfSever()
    g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("JuDian_Back_BenFu_Confirm"), function()
        Gac2Gas.GuildJuDianRequestBackToOwnServer()
    end, nil, nil, nil, false)
end

-- 每周打开一次引导
function LuaJuDianBattleMgr:OpenImageGuide()
    local now = CServerTimeMgr.Inst:GetZone8Time()
    local curDay = now.DayOfYear
    local lastShowDay = PlayerPrefs.GetInt("JuDianBattle_ShowImageRule_Day")
    if curDay - lastShowDay > 6 or curDay < lastShowDay or lastShowDay == 0 then
        PlayerPrefs.SetInt("JuDianBattle_ShowImageRule_Day",curDay)
        LuaCommonTextImageRuleMgr:ShowOnlyImageWnd(2)
    end
end

-- 打开入口界面
function LuaJuDianBattleMgr:OpenGuildWarsEnterWnd()
    if CClientMainPlayer.Inst ~= nil then
        if CClientMainPlayer.Inst.IsCrossServerPlayer then
            g_MessageMgr:ShowMessage("CROSS_SERVER_NOT_USE_THE_FEATURE")
        else
            CUIManager.ShowUI(CLuaUIResources.GuildWarsEnterWnd)
        end
    end
end

-- 同步外围玩法数据
function LuaJuDianBattleMgr:SyncDailySceneData(data)
    self.SceneMonsterData = data
    g_ScriptEvent:BroadcastInLua("JuDianBattle_SyncDailySceneData")
end

-- 分组排行信息
LuaJuDianBattleMgr.RankData = nil

-- 打开分组排行信息
function LuaJuDianBattleMgr:OpenRankWnd(centerData, localData)
    local rankData = {}
    if centerData then
        for k, list in ipairs(centerData) do
            list.IsCrossServer = true
            table.insert(rankData, list) 
        end
    end

    if localData then
        for k, list in ipairs(localData) do
            list.IsCrossServer = false
            table.insert(rankData, list) 
        end
    end

    if #rankData > 0 then
        self.RankData = rankData
        CUIManager.ShowUI(CLuaUIResources.JuDianBattleRankWnd)
    else
        g_MessageMgr:ShowMessage("JuDian_Battle_No_Rank_Data")
    end
end

-- 灵气/灵泉提交
function LuaJuDianBattleMgr:OpenLingqiSubmitWnd(count)
	if CClientMainPlayer.Inst ~= nil then
        local itemId = GuildOccupationWar_Setting.GetData().LingQuanItemId
        local lingQuanCount = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, itemId)
        if lingQuanCount <= 0 then
            g_MessageMgr:ShowMessage("Lack_Of_Ling_Quan_Item")
        else
            CLuaNumberInputMgr.ShowNumInputBox(1, lingQuanCount, 1, function (v)
                -- 提交灵气
                Gac2Gas.GuildJuDianSubmitLingQuanInFight(v)
            end, LocalString.GetString("请输入提交的数量"), -1)
        end
	end
end

-- 右上角显示
function LuaJuDianBattleMgr:OnMainPlayerCreated(playId)
    if self:IsInGameplay() then
        if not CUIManager.IsLoaded(CLuaUIResources.JuDianBattlePlayingWnd) then
            CUIManager.ShowUI(CLuaUIResources.JuDianBattlePlayingWnd)
        end
    end
    if self:IsInDailyGameplay() or self:IsInGameplay() or self:IsInJuDian() then
        if not CUIManager.IsLoaded(CLuaUIResources.JuDianBattleDailyPlayingWnd) then
            CUIManager.ShowUI(CLuaUIResources.JuDianBattleDailyPlayingWnd)
        end
    end
end

LuaJuDianBattleMgr.m_ZhanLongShopInfo = {}
-- 战龙商店
function LuaJuDianBattleMgr:SendGuildJuDianZhanLongShopInfo(hasScore, hasPermission, shopInfo)
    self.m_ZhanLongShopInfo = {hasScore = hasScore, hasPermission = hasPermission, shopInfo = shopInfo}
    if CUIManager.IsLoaded(CLuaUIResources.JuDianBattleZhanLongShopWnd) then
        g_ScriptEvent:BroadcastInLua("SendGuildJuDianZhanLongShopInfo")
        return
    end
    CUIManager.ShowUI(CLuaUIResources.JuDianBattleZhanLongShopWnd)
end

LuaJuDianBattleMgr.m_ZhanLongTaskIDList = nil
function LuaJuDianBattleMgr:GetZhanLongTaskIDList()
    if self.m_ZhanLongTaskIDList == nil then
        self.m_ZhanLongTaskIDList = {}
        GuildOccupationWar_ZhanLong.Foreach(function (key, data)
            table.insert(self.m_ZhanLongTaskIDList, key)
        end)
    end
    return self.m_ZhanLongTaskIDList
end

LuaJuDianBattleMgr.m_ChallengeMemberInfo = {}
LuaJuDianBattleMgr.m_IsChallenging = false --棋局挑战是否开启
LuaJuDianBattleMgr.m_FilterOnlineAndJingYing = false --是否只显示在线和精英
function LuaJuDianBattleMgr:SendGuildJuDianChallengeMemberInfo(onlineMemberCount, totalMemberCount, onlineEliteCount, totalEliteCount, hasPermission, isChallenging, memberInfo)
    print("SendGuildJuDianChallengeMemberInfo", onlineMemberCount, totalMemberCount, onlineEliteCount, totalEliteCount, hasPermission, isChallenging)
    memberInfo = g_MessagePack.unpack(memberInfo)
    local list = {}
    for i = 1, #memberInfo, 13  do
        local playerId = memberInfo[i]
        local playername = memberInfo[i + 1]
        local level = memberInfo[i + 2]
        local feisheng = memberInfo[i + 3]
        local xiuwei = memberInfo[i + 4]
        local xiulian = memberInfo[i + 5]
        local equipScore = memberInfo[i + 6]
        local playtime = memberInfo[i + 7]
        local score = memberInfo[i + 8]
        local isElite = memberInfo[i + 9]
        -- local isForeignAid = memberInfo[i + 10] -- 复用这个字段，改成是否入场
        local isForeignAid = false
        local isInPlay = memberInfo[i + 10] -- 是否入场
        local isOnline = memberInfo[i + 11]
        local class = memberInfo[i + 12]
        table.insert(list, {
            playerId = playerId, playername = playername, level  = level, feisheng = feisheng, xiuwei = xiuwei, xiulian = xiulian,
            equipScore = equipScore, playtime = playtime, score = score, isElite = isElite, isForeignAid = isForeignAid, isOnline = isOnline, class = class,
            isInPlay = isInPlay
        })
    end
    -- if not hasPermission then
    --     return
    -- end
    self.m_IsChallenging = isChallenging
    self.m_ChallengeMemberInfo = {onlineMemberCount = onlineMemberCount, totalMemberCount = totalMemberCount, onlineEliteCount = onlineEliteCount, totalEliteCount = totalEliteCount, hasPermission = hasPermission, list = list}
    if CUIManager.IsLoaded(CLuaUIResources.JuDianBattleJingYingSettingWnd) then
        g_ScriptEvent:BroadcastInLua("SendGuildJuDianChallengeMemberInfo", onlineMemberCount, totalMemberCount, onlineEliteCount, totalEliteCount, hasPermission, list)
    else
        CUIManager.ShowUI(CLuaUIResources.JuDianBattleJingYingSettingWnd)
    end
end
function LuaJuDianBattleMgr:SendSetGuildJuDianChallengeEliteResult(targetPlayerId, isElite, onlineMemberCount, totalMemberCount, onlineEliteCount, totalEliteCount)
    self.m_ChallengeMemberInfo.onlineMemberCount = onlineMemberCount
    self.m_ChallengeMemberInfo.totalMemberCount = totalMemberCount
    self.m_ChallengeMemberInfo.onlineEliteCount = onlineEliteCount
    self.m_ChallengeMemberInfo.totalEliteCount = totalEliteCount
    for i = 1, #self.m_ChallengeMemberInfo.list do
		if self.m_ChallengeMemberInfo.list[i].playerId == targetPlayerId then
			self.m_ChallengeMemberInfo.list[i].isElite = isElite
		end
	end
    g_ScriptEvent:BroadcastInLua("SendSetGuildJuDianChallengeEliteResult", targetPlayerId, isElite, onlineMemberCount, totalMemberCount, onlineEliteCount, totalEliteCount)
end