local GameObject			= import "UnityEngine.GameObject"
local Vector3               = import "UnityEngine.Vector3"

local UITable				= import "UITable"
local UILabel				= import "UILabel"
local UITexture             = import "UITexture"
local UIScrollView			= import "UIScrollView"
local UIScrollViewIndicator = import "UIScrollViewIndicator"
local CTipTitleItem			= import "CTipTitleItem"
local LocalString           = import "LocalString"

local DelegateFactory		= import "DelegateFactory"
local Extensions			= import "Extensions"

local CUITexture                    = import "L10.UI.CUITexture"
local CUIManager			        = import "L10.UI.CUIManager"
local CUICommonDef			        = import "L10.UI.CUICommonDef"
local CMessageTipMgr		        = import "L10.UI.CMessageTipMgr"
local CTipParagraphItem		        = import "L10.UI.CTipParagraphItem"
local QnTableView                   = import "L10.UI.QnTableView"
local DefaultTableViewDataSource    = import "L10.UI.DefaultTableViewDataSource"

local CommonDefs			= import "L10.Game.CommonDefs"
local MessageMgr			= import "L10.Game.MessageMgr"
local CTaskMgr              = import "L10.Game.CTaskMgr"
local CTrackMgr             = import "L10.Game.CTrackMgr"

--中秋月邀伴赏月界面
CLuaZQYaoBanWnd = class()

RegistChildComponent(CLuaZQYaoBanWnd, "Table",				UITable)
RegistChildComponent(CLuaZQYaoBanWnd, "TitleTemplate",		GameObject)
RegistChildComponent(CLuaZQYaoBanWnd, "ParagraphTemplate",	GameObject)
RegistChildComponent(CLuaZQYaoBanWnd, "ScrollView",		    UIScrollView)
RegistChildComponent(CLuaZQYaoBanWnd, "Indicator",			UIScrollViewIndicator)
RegistChildComponent(CLuaZQYaoBanWnd, "EnterBtn",			GameObject)
RegistChildComponent(CLuaZQYaoBanWnd, "Npcs",               QnTableView)
--RegistChildComponent(CLuaZQYaoBanWnd, "NPCTable",			UITable)
--RegistChildComponent(CLuaZQYaoBanWnd, "NPCScrollView",		UIScrollView)
--RegistChildComponent(CLuaZQYaoBanWnd, "NPCTemplate",		GameObject)
RegistChildComponent(CLuaZQYaoBanWnd, "NPCSelect",		    GameObject)


RegistClassMember(CLuaZQYaoBanWnd, "NpcDatas")
--邀伴赏月状态：0-未领取任务；1-已领取任务
RegistClassMember(CLuaZQYaoBanWnd, "SelectNpcID")

function CLuaZQYaoBanWnd:Init()
    self.NpcDatas = {}
    self.SelectNpcID = 0

    self:ParseRuleText()
    self:InitNpcs()
    self.EnterBtn:SetActive(false)

    Gac2Gas.RequestZhongQiuNpcFriendlinessData()
end

function CLuaZQYaoBanWnd:OnEnable()
    local submitClick = function ( go )
		self:OnEnterBtnClick()
    end
    CommonDefs.AddOnClickListener(self.EnterBtn, DelegateFactory.Action_GameObject(submitClick), false)
    
    g_ScriptEvent:AddListener("OnSyncZQFriendliness",   self,   "OnGetFriendInfo")
    g_ScriptEvent:AddListener("OnGetZQNPCSelect",       self,   "OnGetZQNpcSelect")
end

function CLuaZQYaoBanWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnSyncZQFriendliness",    self,   "OnGetFriendInfo")
    g_ScriptEvent:RemoveListener("OnGetZQNPCSelect",        self,   "OnGetZQNpcSelect")
end

--[[
    @desc: 领取任务的回调
    author:CodeGize
    time:2019-07-02 16:22:45
    --@npcid: 
    @return:
]]
function CLuaZQYaoBanWnd:OnGetZQNpcSelect(npcid)
    self:SetSelectNpc(npcid)
    local data = self.NpcDatas[self.SelectNpcID]
    if data ~= nil then

        CTaskMgr.Inst:TrackToDoTask(data.TempData.TaskId)
        CUIManager.CloseUI(CLuaUIResources.ZQYaoBanShangYueWnd)
    end
    --self:InitEnterBtnText()
end


--[[
    @desc: 获得npc好感度回调
    author:CodeGize
    time:2019-07-02 16:21:26
    --@list:npc好感度列表
	--@selectnpc: 当前任务的npc
    @return:
]]
function CLuaZQYaoBanWnd:OnGetFriendInfo(list,selectnpc)
    self:SetSelectNpc(selectnpc)
    self:InitEnterBtnText()
    self.EnterBtn:SetActive(true)

    for i=1, list.Count, 2 do
		local npcTemplateId = list[i-1]
        local friendliness = list[i]
        self:SetNpcCtrlFriendValue(npcTemplateId,friendliness)
    end
end

function CLuaZQYaoBanWnd:SetSelectNpc(npcid)
    if npcid == nil or npcid == 0 then return end
    
    local data = self.NpcDatas[npcid]
    local trans = data.Ctrl.transform
    
    if self.SelectNpcID <=0 then
        self.Npcs:ScrollToRow(data.Index)
    end

    self.SelectNpcID = npcid
    
    trans:Find("ValueBg").gameObject:SetActive(true)
    trans:Find("frame_normal").gameObject:SetActive(false)
    trans:Find("frame_select").gameObject:SetActive(true)
    trans.localScale = Vector3(1.1,1.1,1.1)

    self.NPCSelect:SetActive(true)
    self.NPCSelect.transform.parent = trans
    self.NPCSelect.transform.localPosition = Vector3(0,-140,0)    
    
    local btn = trans:Find("AddBtn").gameObject
    btn:SetActive(true)
    local btnclick = function (go)
        self:OnNpcCtrlBtnClick(npcid)
    end
    CommonDefs.AddOnClickListener(btn,DelegateFactory.Action_GameObject(btnclick), false)
end

function CLuaZQYaoBanWnd:InitEnterBtnText()
    local btntext = self.EnterBtn.transform:Find("EnterBtnLabel"):GetComponent(typeof(UILabel))
    if self.SelectNpcID <=0 then 
        btntext.text= LocalString.GetString("领取任务")
    else
        btntext.text= LocalString.GetString("前往任务")
    end
end

--[[
    @desc: NPC头像旁按钮事件
    author:CodeGize
    time:2019-07-02 16:27:53
    --@npcid: 
    @return:
]]
function CLuaZQYaoBanWnd:OnNpcCtrlBtnClick(npcid)
    local data = self.NpcDatas[npcid]
    local ctrl = data.Ctrl
    local trans = ctrl.transform
    local posx = trans.position.x

    local dir = 0
    if trans.localPosition.x > 0 then
        dir = -1
    else
        dir = 1 
    end

    CLuaZQSelectionToolTip.Show(npcid,posx,dir)
end

--[[
    @desc: 领取任务/前往任务的按钮事件
    author:CodeGize
    time:2019-07-02 16:27:24
    @return:
]]
function CLuaZQYaoBanWnd:OnEnterBtnClick()
    if self.SelectNpcID <=0 then
        Gac2Gas.RequestZhongQiuNpcFriendlinessTask()
    else
        local data = self.NpcDatas[self.SelectNpcID]
        if data ~= nil then

            local location = CTaskMgr.Inst:GetAcceptTaskTrackPos(data.TempData.TaskId)
            CTrackMgr.Inst:FindNPC(self.SelectNpcID,location.sceneTemplateId,location.targetX,location.targetY,nil,nil)
            CUIManager.CloseUI(CLuaUIResources.ZQYaoBanShangYueWnd)
        end
    end
end


--@region 初始化控件

function CLuaZQYaoBanWnd:InitNpcs()
    local indexnpcs = {}
    local i = 0
    ZhongQiu2019_NpcFriendliness.Foreach(function (id, data) 
        self.NpcDatas[id] = {TempData = {SubmitItems = data.SubmitItems,MaxFriendliness = data.MaxFriendliness,TaskId = data.TaskId}}
        self.NpcDatas[id].Index = i
        indexnpcs[i] = id
        i = i + 1
    end)

    local initItem = function (item, index)
        local npcid = indexnpcs[index]
        self:AddNpc(item,npcid)
    end
    self.Npcs.m_DataSource = DefaultTableViewDataSource.CreateByCount(i, initItem)
    self.Npcs:ReloadData(true,true)
end

function CLuaZQYaoBanWnd:AddNpc(ctrlgo,npcid)
    self.NpcDatas[npcid].Ctrl = ctrlgo
    self:InitNpcCtrl(ctrlgo,npcid)
end

function CLuaZQYaoBanWnd:InitNpcCtrl(ctrlgo,npcid)
    local npc = NPC_NPC.GetData(npcid)
    if npc == nil then return end
    local trans = ctrlgo.transform
    local icon = trans:Find("Icon"):GetComponent(typeof(CUITexture))
    icon:LoadNPCPortrait(npc.Portrait,false)
    
    local btn = trans:Find("AddBtn").gameObject
    btn:SetActive(false)
    self:SetNpcCtrlFriendValue(npcid,0)
end

function CLuaZQYaoBanWnd:ParseRuleText()
	Extensions.RemoveAllChildren(self.Table.transform)
	local msg =	MessageMgr.Inst:FormatMessage("ZQYaoBanShangYue_Rule",{});
    if System.String.IsNullOrEmpty(msg) then
        return
    end

    local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(msg)

    if info == nil then
        return
    end

    if info.titleVisible then
        local titleGo = CUICommonDef.AddChild(self.Table.gameObject, self.TitleTemplate)
        titleGo:SetActive(true)
        CommonDefs.GetComponent_GameObject_Type(titleGo, typeof(CTipTitleItem)):Init(info.title)
    end

    do
        local i = 0
        while i < info.paragraphs.Count do
            local paragraphGo = CUICommonDef.AddChild(self.Table.gameObject, self.ParagraphTemplate)
            paragraphGo:SetActive(true)
			local tip = CommonDefs.GetComponent_GameObject_Type(paragraphGo, typeof(CTipParagraphItem))
            tip:Init(info.paragraphs[i], 4294967295)
            i = i + 1
        end
    end

    self.ScrollView:ResetPosition()
    self.Indicator:Layout()
    self.Table:Reposition()
end

--@endregion

function CLuaZQYaoBanWnd:SetNpcCtrlFriendValue(npcid,value)
    local data = self.NpcDatas[npcid]
    if data == nil then 

        return 
    end
    local max = data.TempData.MaxFriendliness
    local ctrl = data.Ctrl.transform
    local completego = ctrl:Find("CompletedSprite").gameObject
    local valuebg = ctrl:Find("ValueBg")
    if value == max then
        completego:SetActive(true)
        valuebg.gameObject:SetActive(false)
        local btn = ctrl:Find("AddBtn").gameObject
        btn:SetActive(false)
        if npcid == self.SelectNpcID then
            CUIManager.CloseUI("ZQSelectionToolTip")
        end
    else
        completego:SetActive(false)
        
        valuebg.gameObject:SetActive(true)
        local progress = valuebg:Find("ValueProgress"):GetComponent(typeof(UITexture))
        progress.fillAmount = value / max

        local valuelb = valuebg:Find("ValueLabel"):GetComponent(typeof(UILabel))
        valuelb.text = tostring(value).."/"..tostring(max)
    end

end

