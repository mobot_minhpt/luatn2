-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CHideAndSeekRank = import "L10.UI.CHideAndSeekRank"
local CHideAndSeekRankItem = import "L10.UI.CHideAndSeekRankItem"
local CHideAndSeekRankWnd = import "L10.UI.CHideAndSeekRankWnd"
local CRankData = import "L10.UI.CRankData"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local NGUITools = import "NGUITools"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local CRankMgr = import "L10.Game.CRankMgr"

CHideAndSeekRankWnd.m_OnEnable_CS2LuaHook = function (this) 
    EventManager.AddListener(EnumEventType.OnRankDataReady, MakeDelegateFromCSFunction(this.OnGetRankInfo, Action0, this))
    UIEventListener.Get(this.m_CloseBtn).onClick = MakeDelegateFromCSFunction(this.OnClickCloseButton, VoidDelegate, this)
    this.m_RankTemplate:SetActive(false)
    this.m_MyRankItem.gameObject:SetActive(false)
end
CHideAndSeekRankWnd.m_OnGetRankInfo_CS2LuaHook = function (this) 
    if CLuaRankData.m_CurRankId ~= CRankMgr.HIDE_AND_SEEK_RANK_ID then return end
    local PlayerRankInfo = CRankData.Inst.MainPlayerRankInfo
    if PlayerRankInfo ~= nil then
        this.m_MyRankItem.gameObject:SetActive(true)
        local item = (function () 
            local __localValue = CreateFromClass(CHideAndSeekRank)
            __localValue.m_Clazz = CClientMainPlayer.Inst.Class
            __localValue.m_Name = CClientMainPlayer.Inst.Name
            __localValue.m_Rank = PlayerRankInfo.Rank
            __localValue.m_Score = math.floor(PlayerRankInfo.Value)
            return __localValue
        end)()
        this.m_MyRankItem:Init(item)
    end

    do
        local i = 0
        while i < CRankData.Inst.RankList.Count do
            local go = NGUITools.AddChild(this.m_RankGrid.gameObject, this.m_RankTemplate)
            local item = CommonDefs.GetComponent_GameObject_Type(go, typeof(CHideAndSeekRankItem))
            if item ~= nil then
                local info = CRankData.Inst.RankList[i]
                local rank = (function () 
                    local __localValue = CreateFromClass(CHideAndSeekRank)
                    __localValue.m_Clazz = info.Job
                    __localValue.m_Name = info.Name
                    __localValue.m_Rank = info.Rank
                    __localValue.m_Score = math.floor(info.Value)
                    return __localValue
                end)()
                item:Init(rank)
            end
            i = i + 1
        end
    end
    this.m_RankGrid:Reposition()
end
