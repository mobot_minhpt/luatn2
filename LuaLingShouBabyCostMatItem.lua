require("3rdParty/ScriptEvent")
require("common/common_include")
local LuaGameObject=import "LuaGameObject"
local CItemMgr=import "L10.Game.CItemMgr"
local CItemAccessListMgr=import "L10.UI.CItemAccessListMgr"
local CItemInfoMgr=import "L10.UI.CItemInfoMgr"
local AlignType=import "L10.UI.CItemInfoMgr+AlignType"


CLuaLingShouBabyCostMatItem=class()
RegistClassMember(CLuaLingShouBabyCostMatItem,"m_Icon")
RegistClassMember(CLuaLingShouBabyCostMatItem,"m_Mask")
RegistClassMember(CLuaLingShouBabyCostMatItem,"m_ItemCountUpdate")
RegistClassMember(CLuaLingShouBabyCostMatItem,"m_CostTemplateId")
RegistClassMember(CLuaLingShouBabyCostMatItem,"m_NeedCount")
-- RegistClassMember(CLuaLingShouBabyCostMatItem,"m_Cost")
function CLuaLingShouBabyCostMatItem:Init( tf,templateId,needCount)
    self.m_NeedCount=needCount
    self.m_CostTemplateId=templateId
    self.m_ItemCountUpdate=LuaGameObject.GetLuaGameObjectNoGC(tf).itemCountUpdate
    self.m_Icon=LuaGameObject.GetChildNoGC(tf,"MatIcon").cTexture
    self.m_Mask=LuaGameObject.GetChildNoGC(tf,"Mask").gameObject

    local template = CItemMgr.Inst:GetItemTemplate(self.m_CostTemplateId)
    if template then
        self.m_Icon:LoadMaterial(template.Icon)
    else
        self.m_Icon:Clear()
    end

    self.m_ItemCountUpdate.format=SafeStringFormat3("{0}/%d",self.m_NeedCount)
    local function OnCountChange(val)
        -- print(val.." "..self.m_NeedCount)
        if val>=self.m_NeedCount then
            self.m_ItemCountUpdate.format=SafeStringFormat3("{0}/%d",self.m_NeedCount)
            self.m_Mask:SetActive(false)
        else
            self.m_ItemCountUpdate.format=SafeStringFormat3("[ff0000]{0}[-]/%d",self.m_NeedCount)
            self.m_Mask:SetActive(true)
        end
    end
    self.m_ItemCountUpdate.onChange=DelegateFactory.Action_int(OnCountChange)

    CommonDefs.AddOnClickListener(tf.gameObject,DelegateFactory.Action_GameObject(function(go)
        if self.m_ItemCountUpdate.count>=self.m_NeedCount then
            CItemInfoMgr.ShowLinkItemTemplateInfo(self.m_CostTemplateId,false,nil,AlignType.ScreenRight,0,0,0,0)
        else
            CItemAccessListMgr.Inst:ShowItemAccessInfo(self.m_CostTemplateId, false, go.transform);
        end
    end),false)

    self.m_ItemCountUpdate.templateId=self.m_CostTemplateId

    self.m_ItemCountUpdate:UpdateCount()
end
function CLuaLingShouBabyCostMatItem:IsEnough()
    if self.m_ItemCountUpdate.count>=self.m_NeedCount then
        return true
    else
        return false
    end
end

return CLuaLingShouBabyCostMatItem
