-- Auto Generated!!
local CGCGuildListView = import "L10.UI.CGCGuildListView"
local CGCGuildTemplate = import "L10.UI.CGCGuildTemplate"
local CGuildChallengeMgr = import "L10.Game.CGuildChallengeMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local Extensions = import "Extensions"
local NGUITools = import "NGUITools"
local Object = import "System.Object"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CGCGuildListView.m_Init_CS2LuaHook = function (this) 
    this.searchGuild = false
    Extensions.RemoveAllChildren(this.table.transform)
    this.guildTemplate:SetActive(false)
    CommonDefs.ListClear(this.buttonList)
    this.selectIndex = - 1
    do
        local i = 0
        while i < CGuildChallengeMgr.Inst.guildList.Count do
            local instance = NGUITools.AddChild(this.table.gameObject, this.guildTemplate)
            instance:SetActive(true)
            local template = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CGCGuildTemplate))
            if template ~= nil then
                template:Init(CGuildChallengeMgr.Inst.guildList[i])
            end
            local button = CommonDefs.GetComponent_GameObject_Type(instance, typeof(QnSelectableButton))
            if button ~= nil then
                button:SetSelected(false, false)
                CommonDefs.ListAdd(this.buttonList, typeof(QnSelectableButton), button)
                button:SetBackgroundTexture(i % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite)
                UIEventListener.Get(instance).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(instance).onClick, MakeDelegateFromCSFunction(this.OnButtonClick, VoidDelegate, this), true)
            end
            i = i + 1
        end
    end
    this.table:Reposition()
    this.scrollview:ResetPosition()
end
CGCGuildListView.m_InitSearchGuild_CS2LuaHook = function (this) 
    this.searchGuild = true
    Extensions.RemoveAllChildren(this.table.transform)
    this.guildTemplate:SetActive(false)
    CommonDefs.ListClear(this.buttonList)
    this.selectIndex = - 1
    do
        local i = 0
        while i < CGuildChallengeMgr.Inst.searchGuildList.Count do
            local instance = NGUITools.AddChild(this.table.gameObject, this.guildTemplate)
            instance:SetActive(true)
            local template = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CGCGuildTemplate))
            if template ~= nil then
                template:Init(CGuildChallengeMgr.Inst.searchGuildList[i])
            end
            local button = CommonDefs.GetComponent_GameObject_Type(instance, typeof(QnSelectableButton))
            if button ~= nil then
                button:SetSelected(false, false)
                CommonDefs.ListAdd(this.buttonList, typeof(QnSelectableButton), button)
                button:SetBackgroundTexture(i % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite)
                UIEventListener.Get(instance).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(instance).onClick, MakeDelegateFromCSFunction(this.OnButtonClick, VoidDelegate, this), true)
            end
            i = i + 1
        end
    end
    this.table:Reposition()
    this.scrollview:ResetPosition()
end
CGCGuildListView.m_OnButtonClick_CS2LuaHook = function (this, go) 
    do
        local i = 0
        while i < this.buttonList.Count do
            this.buttonList[i]:SetSelected(go == this.buttonList[i].gameObject, false)
            if go == this.buttonList[i].gameObject then
                this.selectIndex = i
                if this.onGuildSelect ~= nil then
                    GenericDelegateInvoke(this.onGuildSelect, Table2ArrayWithCount({i}, 1, MakeArrayClass(Object)))
                end
            end
            i = i + 1
        end
    end
end
