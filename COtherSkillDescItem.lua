-- Auto Generated!!
local CChatLinkMgr = import "CChatLinkMgr"
local COtherSkillDescItem = import "L10.UI.COtherSkillDescItem"
local NGUIText = import "NGUIText"
COtherSkillDescItem.m_Init_CS2LuaHook = function (this, title, content) 
    this.titleLabel.text = title
    this.contentLabel.text = System.String.Format("[c][{0}]{1}[-][/c]", NGUIText.EncodeColor24(this.contentLabel.color), CChatLinkMgr.TranslateToNGUIText(content, false))
end
