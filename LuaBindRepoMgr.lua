--绑仓
local CItemMgr=import "L10.Game.CItemMgr"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local CZuoQiMgr=import "L10.Game.CZuoQiMgr"
local CServerTimeMgr=import "L10.Game.CServerTimeMgr"

CLuaBindRepoMgr = {}


--获取绑仓的物品数量
function CLuaBindRepoMgr.GetBangCangItemCount()
    if not CClientMainPlayer.Inst then return 0 end

    local data = CClientMainPlayer.Inst.ItemProp.SimpleItems
    local items = data.Items

    local count = 0
    for i=0,items.Length-1 do
        if items[i]>0 then
            count=count+1
        end
    end
    return count
end

--绑仓开放的数量
function CLuaBindRepoMgr.GetBindRepoUsableSize()
    local bindrepo_setting = BindRepo_Setting.GetData()
    local ret = 0
    if CClientMainPlayer.Inst then
        local data = CClientMainPlayer.Inst.ItemProp.SimpleItems
        ret = (data.UsableSize and data.UsableSize or 0) + bindrepo_setting.FreeSlotNum
    else
        ret = bindrepo_setting.FreeSlotNum
    end
    return ret
end

--是否有没过期的坐骑
function CLuaBindRepoMgr.HasAvailableZuoQi()
    local has_zuoqi = false
    CommonDefs.ListIterate(CZuoQiMgr.Inst.zuoqis, DelegateFactory.Action_object(function (___value) 
        local zuoqi = ___value
        if(zuoqi.expTime <= 0 or zuoqi.expTime >= CServerTimeMgr.Inst.timeStamp)then
            has_zuoqi = true
        end
    end))
    return has_zuoqi
end

function CLuaBindRepoMgr.CanPlaceInBindRepo(itemId)
    if not CLuaBindRepoMgr.HasAvailableZuoQi() then
        return false
    end
    local commonItem = CItemMgr.Inst:GetById(itemId)
    if commonItem and BindRepo_AllowedItems.Exists(commonItem.TemplateId) and commonItem.IsBinded and CLuaBindRepoMgr.GetItemExpiredTime(commonItem) == 0 then
        local simpleItems = CClientMainPlayer.Inst.ItemProp.SimpleItems
        local items = simpleItems.Items
        -- local counts = simpleItems.Count
        local size = CLuaBindRepoMgr.GetBindRepoUsableSize()--绑仓可以用的格子
        local count=0
        for i=1,size do
            if items[i]>0 then
                count=count+1
            end
        end
        if count<size then
            return true
        end

        --考虑堆叠
        local designData = Item_Item.GetData(commonItem.TemplateId)
        local overlapCount = designData and designData.Overlap or 0

        local leftCount = commonItem.Item.Count
        for i=1,size do
            local templateId = items[i]
            if templateId == commonItem.TemplateId then
                local count = simpleItems.Count[i]
                local delta = math.min(overlapCount - count,leftCount)
                leftCount = leftCount - delta
                if leftCount <= 0 then
                    return true
                end
            end
        end
    end
    return false
end

function CLuaBindRepoMgr.IsLevelMatch()
    local levelMatch = false
    if CClientMainPlayer.Inst then
		levelMatch = CClientMainPlayer.Inst.MaxLevel>=69
    end
    return levelMatch
end

function CLuaBindRepoMgr.GetItemExpiredTime(item)
    if not (item and item.IsItem) then
        return 0
    end

    return item.Item:GetRealExpiredTime()
end
