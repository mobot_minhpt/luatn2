require("common/common_include")

local UIScrollView=import "UIScrollView"
local UIWidget = import "UIWidget"
local UITable = import "UITable"
local UIRoot=import "UIRoot"
local Screen=import "UnityEngine.Screen"
local NGUIMath=import "NGUIMath"
local Mathf = import "UnityEngine.Mathf"
local Baby_Skill = import "L10.Game.Baby_Skill"
local CChatLinkMgr = import "CChatLinkMgr"

LuaBabySkillTip=class()

RegistChildComponent(LuaBabySkillTip, "SkillNameLabel", UILabel)
RegistChildComponent(LuaBabySkillTip, "SkillDescLabel", UILabel)
RegistChildComponent(LuaBabySkillTip, "UnlockDescLabel", UILabel)
RegistChildComponent(LuaBabySkillTip, "Table", UITable)
RegistChildComponent(LuaBabySkillTip, "ScrollView", UIScrollView)
RegistChildComponent(LuaBabySkillTip, "Background", GameObject)

RegistClassMember(LuaBabySkillTip, "SelectedSkill")

function LuaBabySkillTip:Init()

    local skill =  Baby_Skill.GetData(LuaBabyMgr.m_SelectedSkillId)
    if not skill  then
        CUIManager.CloseUI(CLuaUIResources.BabySkillTip)
        return
    end
    self.SelectedSkill = skill

    self.SkillNameLabel.text = skill.Name
    self.SkillDescLabel.text = CChatLinkMgr.TranslateToNGUIText(skill.Description, false)
    self.UnlockDescLabel.text = skill.Unlock

    
    self.Table:Reposition()

    local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject)
    local virtualScreenHeight = Screen.height * scale
    local contentTopPadding = Mathf.Abs(self.ScrollView.panel.topAnchor.absolute)
    local contentBottomPadding = Mathf.Abs(self.ScrollView.panel.bottomAnchor.absolute)
    local totalHeight = NGUIMath.CalculateRelativeWidgetBounds(self.Table.transform).size.y + (self.Table.padding.y * 2) + contentTopPadding + contentBottomPadding + (self.ScrollView.panel.clipSoftness.y * 2)
    local displayWndHeight = Mathf.Clamp(totalHeight, contentTopPadding + contentBottomPadding + 80, virtualScreenHeight - 100)
    self.Background:GetComponent(typeof(UIWidget)).height = Mathf.CeilToInt(displayWndHeight)
    self.ScrollView.panel:ResetAndUpdateAnchors()
    self.ScrollView:ResetPosition()
end


function LuaBabySkillTip:Update()
    self:ClickThroughToClose()
end

function LuaBabySkillTip:ClickThroughToClose()
    if Input.GetMouseButtonDown(0) then       
        if (not CUICommonDef.IsOverUI or (UICamera.lastHit.collider and
           not CUICommonDef.IsChild(self.transform, UICamera.lastHit.collider.transform) and
            CUICommonDef.GetNearestPanelDepth(self.transform) > CUICommonDef.GetNearestPanelDepth(UICamera.lastHit.collider.transform))) then
            CUIManager.CloseUI(CLuaUIResources.BabySkillTip)
        end
    end
end

return LuaBabySkillTip
