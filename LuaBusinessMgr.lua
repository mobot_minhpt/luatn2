local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CGuideMgr = import "L10.Game.Guide.CGuideMgr"

LuaBusinessMgr = {}

LuaBusinessMgr.m_GameMode = 0
LuaBusinessMgr.m_ResultGameMode = 0
LuaBusinessMgr.m_IsStoryMode = false
LuaBusinessMgr.m_CurDay = 0
LuaBusinessMgr.m_MaxDay = 0
-- 是否到访过
LuaBusinessMgr.m_Cities = {}
LuaBusinessMgr.m_CurCity = 0
-- 类型、市场价格
LuaBusinessMgr.m_CurCityGoods = {}
-- 类型、买进价格、数量
LuaBusinessMgr.m_Inventory = {}
LuaBusinessMgr.m_CurCityGoodsType2Price = {}
LuaBusinessMgr.m_CurCityGoodsSet = {}
LuaBusinessMgr.m_CurCitySpecialGoods = {}
LuaBusinessMgr.m_SpecialGoods2City = nil
LuaBusinessMgr.m_CurBuyGoodsRow = 0
LuaBusinessMgr.m_CurSellGoodsRow = 0
LuaBusinessMgr.m_CurInventoryAmount = 0
LuaBusinessMgr.m_InventorySize = 0
LuaBusinessMgr.m_CarriageLevel = 0
LuaBusinessMgr.m_OwnMoney = 0
LuaBusinessMgr.m_DailyCost = 0
-- 日期、编号
LuaBusinessMgr.m_News = {}
-- 投资、贷款
LuaBusinessMgr.m_Debt = 0
LuaBusinessMgr.m_Investment = 0
-- 白鸽票
LuaBusinessMgr.m_TicketState = 0
LuaBusinessMgr.m_TicketRewarded = false
LuaBusinessMgr.m_CurTicketTab = {}
LuaBusinessMgr.m_OwnTicketTab = {}
LuaBusinessMgr.m_LotteryCost = 0

LuaBusinessMgr.m_Health = 0
LuaBusinessMgr.m_IsSleep = 0
-- 剧情相关
LuaBusinessMgr.m_ParrotNum = 0
LuaBusinessMgr.m_HaveCanEnd = 0
LuaBusinessMgr.m_ForceCity = 0
LuaBusinessMgr.m_IsOpenScenarioInvestment = false
LuaBusinessMgr.m_IsOpenScenarioPegion = false
-- 状态控制
LuaBusinessMgr.m_BusinessIsBuy = 0
LuaBusinessMgr.m_BusinessIslevelUp = 0
LuaBusinessMgr.m_BusinessCurGoods = 0

function LuaBusinessMgr:RefreshGoodsInfo()
    self.m_CurCityGoods = {}
    self.m_CurCityGoodsSet = {}
    self.m_CurCityGoodsType2Price = {}
    self.m_Inventory = {}

    if CClientMainPlayer.Inst then
		local tradeInfo = CClientMainPlayer.Inst.PlayProp.TradeSimulateInfo
        local worldMarket = tradeInfo.WorldMarket
        CommonDefs.DictIterate(worldMarket.SellItems, DelegateFactory.Action_object_object(function(___key, ___value)
            table.insert(self.m_CurCityGoods, {type = ___key, price = ___value, onSale = true, special = (self:GetCityOfSpecialGoods(___key) and 1 or 0)})
            self.m_CurCityGoodsSet[___key] = true
            self.m_CurCityGoodsType2Price[___key] = ___value
        end))
        table.sort(self.m_CurCityGoods, function(goods1, goods2)
            if goods1.special ~= goods2.special then
                return goods1.special > goods2.special
            else
                return false
            end
        end)

        local extraPriceDay = worldMarket.ExtraPriceDay
        local extraPriceItemId = worldMarket.ExtraPriceItemId
        local extraRise = worldMarket.ExtraRise

        CommonDefs.DictIterate(worldMarket.Price, DelegateFactory.Action_object_object(function(___key, ___value)
            if self.m_CurCityGoodsType2Price[___key] == nil then
                if ___key == extraPriceItemId then
                    local goodsData = Business_Goods.GetData(___key)
                    self.m_CurCityGoodsType2Price[___key] = math.floor(extraRise == 1 and ___value * goodsData.ExtraHighPrice or ___value * goodsData.ExtraLowPrice)
                else
                    self.m_CurCityGoodsType2Price[___key] = ___value
                end
            end
        end))

        local repository = tradeInfo.Repository
        CommonDefs.DictIterate(repository.Items, DelegateFactory.Action_object_object(function(___key, ___value)
            table.insert(self.m_Inventory, {type = ___key, price = math.floor(repository.AvgPrice[___key]), num = ___value, special = (self:GetCityOfSpecialGoods(___key) and 1 or 0)})
            if not self.m_CurCityGoodsSet[___key] then
                table.insert(self.m_CurCityGoods, {type = ___key, price = self.m_CurCityGoodsType2Price[___key], onSale = false})
            end
        end))
        table.sort(self.m_Inventory, function(goods1, goods2)
            if goods1.special ~= goods2.special then
                return goods1.special > goods2.special
            else
                return false
            end
        end)
	end

    if CGuideMgr.Inst:IsInPhase(EnumGuideKey.BusinessGuide) then
        local guideGoodsId = 1
        local guideCurCityGoods = {}

        for i = 1, #self.m_CurCityGoods do
            local info = self.m_CurCityGoods[i]
            if info.type == guideGoodsId then
                table.insert(guideCurCityGoods, info)
                break
            end
        end

        for i = 1, #self.m_CurCityGoods do
            local info = self.m_CurCityGoods[i]
            if info.type ~= guideGoodsId then
                table.insert(guideCurCityGoods, info)
            end
        end

        self.m_CurCityGoods = guideCurCityGoods
    end
end

function LuaBusinessMgr:UpdateTradeSimulationData(isFinish, tradeSimulationUD)
    if CClientMainPlayer.Inst then
        local tradeInfo = CClientMainPlayer.Inst.PlayProp.TradeSimulateInfo
        tradeInfo:LoadFromString(tradeSimulationUD)
    end

    self:RefreshAll()

    if isFinish == 1 then
        CUIManager.CloseUI(CLuaUIResources.BusinessMainWnd)
        if self.m_Health > 0 and self:GetEndingMessageAndState() then
            CUIManager.ShowUI(CLuaUIResources.BusinessResultWnd)
        end
    end
    g_ScriptEvent:BroadcastInLua("OnBusinessDataUpdate")
end

function LuaBusinessMgr:RefreshAll()
    self:RefreshState()
    self:RefreshGoodsInfo()
end

function LuaBusinessMgr:RefreshState()
    if CClientMainPlayer.Inst then
		local tradeInfo = CClientMainPlayer.Inst.PlayProp.TradeSimulateInfo
        local worldMarket = tradeInfo.WorldMarket
        local repository = tradeInfo.Repository
        self.m_GameMode = tradeInfo.PlayType
        self.m_ResultGameMode = self.m_GameMode ~= 0 and self.m_GameMode or self.m_ResultGameMode
        self.m_IsStoryMode = self.m_GameMode == 5

        if self.m_GameMode == 4 then
            self.m_IsOpenScenarioInvestment = false
            self.m_IsOpenScenarioPegion = true
        elseif self.m_IsStoryMode then
            self.m_IsOpenScenarioInvestment = tradeInfo.IsOpenScenarioInvestment == 1
            self.m_IsOpenScenarioPegion = tradeInfo.IsOpenScenarioPegion == 1
        else
            self.m_IsOpenScenarioInvestment = true
            self.m_IsOpenScenarioPegion = true
        end
        
        if self.m_GameMode == 0 then
            return
        end
        self.m_Cities = {}
        for i = 1, Business_City.GetDataCount() do
            self.m_Cities[i] = tradeInfo.CityAccessed:GetBit(i)
        end
        self.m_CurCity = worldMarket.CurrentCity > 0 and worldMarket.CurrentCity or 1
        self.m_CurDay = worldMarket.CurrentDay

        self.m_OwnMoney = repository.Money
        self.m_CarriageLevel = repository.Level
        self.m_Investment = repository.Deposit
        self.m_Debt = repository.Credit
        self.m_Health = repository.Hp
        self.m_IsSleep = repository.IsSleep
        self.m_ParrotNum = repository.BirdNum

        local modeData = Business_Type.GetData(self.m_GameMode)
        self.m_MaxDay = modeData.Days

        local horseData = Business_Horse.GetData(self.m_CarriageLevel)
        self.m_InventorySize = horseData.Warehouse
        self.m_DailyCost = self:GetDailyCost()

        self.m_News = {}
        if self.m_IsStoryMode then
            local storyData = Business_Wangcheng.GetData(self.m_CurDay)
            if storyData and storyData.TopInform ~= "" then
                table.insert(self.m_News, {type = 1, value = storyData.TopInform})
            end
        end
        for i=0,worldMarket.Inform.Length-1,1 do
            local newsId = worldMarket.Inform[i]
            if newsId ~= 0 then
                table.insert(self.m_News, {type = 2, value = newsId})
            end
        end

        self.m_CurCitySpecialGoods = {}
        local cityData = Business_City.GetData(self.m_CurCity)
        if cityData.SpecialGoods then
            for i = 0, cityData.SpecialGoods.Length - 1 do 
                self.m_CurCitySpecialGoods[cityData.SpecialGoods[i]] = true
            end
        end

        self:RefreshLotteryRewardState(repository.PegionTick, repository.PegionBuyDay, repository.LastPegionAwardTick)
    end
end

function LuaBusinessMgr:GetCurInventoryAmount()
    local amount = 0

    for k, v in ipairs(self.m_Inventory) do 
        amount = amount + v.num
    end

    return amount
end

function LuaBusinessMgr:GetOwnGoodsNum(type)
    for k, v in ipairs(self.m_Inventory) do 
        if v.type == type then
            return v.num
        end
    end

    return 0
end

function LuaBusinessMgr:GetDailyCost()
    local horseData = Business_Horse.GetData(self.m_CarriageLevel)
    local horseDailyCost = horseData.NeedMoney

    local ParrotDailyCost = 0
    if self.m_IsStoryMode then
        local storyData = Business_Wangcheng.GetData(self.m_CurDay)

        ParrotDailyCost = storyData and storyData.ParrotNeedMoney * self.m_ParrotNum or 0
    end

    return horseDailyCost + ParrotDailyCost
end

function LuaBusinessMgr:GetEndingMessageAndState()
    local endings = {}
    Business_Ending.ForeachKey(function (key)
        local data = Business_Ending.GetData(key)
        if data.Type == self.m_ResultGameMode then
            table.insert(endings, data)
        end
    end)

    local endingMsg, state
    for k, v in pairs(endings) do
        local first = v.TypeName[0]
        local second = v.TypeName[1]

        if first == "-" then
            if self.m_OwnMoney <= tonumber(second) then
                endingMsg = v.Message
                state = v.EndingPicture
                break
            end
        elseif second == "+" then
            if self.m_OwnMoney >= tonumber(first) then
                endingMsg = v.Message
                state = v.EndingPicture
                break
            end
        else
            if  tonumber(first) <= self.m_OwnMoney and self.m_OwnMoney <= tonumber(second) then
                endingMsg = v.Message
                state = v.EndingPicture
                break
            end
        end
    end

    return endingMsg, state
end

function LuaBusinessMgr:GetLimitCityId()
    if self.m_IsStoryMode then
        local storyData = Business_Wangcheng.GetData(self.m_CurDay + 1) --前往城市时未下一天
        return storyData and storyData.CityID or 0
    end

    return 0
end

function LuaBusinessMgr:GetCityOfSpecialGoods(type)
    if not self.m_SpecialGoods2City then
        self.m_SpecialGoods2City = {}
        Business_City.Foreach(function (key)
            local cityData = Business_City.GetData(key)
            if cityData.SpecialGoods then
                for i = 0, cityData.SpecialGoods.Length - 1 do 
                    self.m_SpecialGoods2City[cityData.SpecialGoods[i]] = key
                end
            end
        end)
    end

    return self.m_SpecialGoods2City[type]
end

function LuaBusinessMgr:ClearAll()
    self.m_CurCityGoods = {}
    self.m_CurCityGoodsSet = {}
    self.m_Inventory = {}
    self.m_CurCityGoodsType2Price = {}
    self.m_CurCitySpecialGoods = {}
    self.m_News = {}

    self.m_SpecialGoods2City = nil
end

-- 1 未开奖未购买  2 未开奖已购买 3 开奖日未购买 4 开奖日未中奖 5 开奖日小奖 6 开奖日大奖
function LuaBusinessMgr:RefreshLotteryRewardState(ticket, buyDay, targetTicket)
    local ticketTab = self:ParsePegionTick(ticket)
    local targetTicketTab = self:ParsePegionTick(targetTicket)
    self.m_OwnTicketTab = ticketTab
    self.m_CurTicketTab = targetTicketTab

    local state = 0
    if self.m_CurDay % 3 == 0 then
        -- 开奖日
        local offset = self.m_CurDay - buyDay
        local isRewarded = buyDay > 999
        if offset < 3 or isRewarded then
            -- 已购买
            if ticket == targetTicket then
                state = 6
            elseif self:TabValEqual(ticketTab, targetTicketTab) then
                state = 5
            else
                state = 4
            end
            self.m_TicketRewarded = isRewarded
        else
            -- 未购买
            state = 3
        end
    else 
        -- 未开奖日
        if ticket ~= 0 then
            state = 2
        else
            state = 1
        end
    end

    self.m_TicketState = state
end

function LuaBusinessMgr:ParsePegionTick(pegionTick)
    if pegionTick == 0 then
        return {}
    end
    local result = {}
    for i = 1, 4 do
        local mask = math.pow(2, 4 * i)
        local mask2 = math.pow(2, 4 * (i - 1))
        local left = math.floor(pegionTick / mask) * mask
        local curValue = math.floor((pegionTick - left) / mask2) + 1
        table.insert(result, curValue)
    end
    return result
end

function LuaBusinessMgr:PackPegionTickTab(pegionTickTab)
    if #pegionTickTab < 4 then
        return
    end

    local pegionTick = 0
    for i = 1, #pegionTickTab do
        local curValue = (pegionTickTab[i] - 1) * math.pow(2, 4 * (i - 1))
        pegionTick = pegionTick + curValue
    end

    return pegionTick
end

function LuaBusinessMgr:TabValEqual(tab1, tab2)
    if #tab1 ~= #tab2 then
        return false
    end
    
    local result = true
    for i = 1, #tab1 do
        local find = false
        for j = 1, #tab2 do
            if tab1[i] == tab2[j] then
                find = true
            end
        end
        result = result and find
    end

    return result
end

function LuaBusinessMgr:TryOpenBusinessWnd()
    if CClientMainPlayer.Inst == nil then
		return
	end
	local taskId1 = 22120727 -- 天价鹌鹑
    local taskId2 = 22120847 -- 王成致富
    local taskId3 = 22120850 -- 东山再起

    if not CClientMainPlayer.Inst.TaskProp:IsMainPlayerTaskFinished(taskId1) then
        g_MessageMgr:ShowMessage("BUSINESS_NEEDFINISHTASK")
    elseif not CClientMainPlayer.Inst.TaskProp:IsMainPlayerTaskFinished(taskId2) then
        local tradeInfo = CClientMainPlayer.Inst.PlayProp.TradeSimulateInfo
        if tradeInfo.PlayType == 0 then
            if tradeInfo.ScenarioPlayOpened == 1 then
                if CommonDefs.ListContains(CClientMainPlayer.Inst.TaskProp.CurrentTaskList, typeof(UInt32), taskId3) then
                    g_MessageMgr:ShowMessage("WangCheng_Business_CannotOpenTradeSimulation")
                    return
                end
                MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("WangCheng_Business_AddMoney"),DelegateFactory.Action(function()
                    local freeSilver = CClientMainPlayer.Inst.FreeSilver
                    local silver = CClientMainPlayer.Inst.Silver
                    local totalSilver = freeSilver + silver
                    if totalSilver < Business_Setting.GetData().TaskReopenCost then 
                        g_MessageMgr:ShowMessage("NO_MONEY_REOPEN_SCENARIO")
                        return 
                    end
                    Gac2Gas.StartTradeSimulationScenario()
                    CUIManager.ShowUI(CLuaUIResources.BusinessMainWnd)
                end),nil,LocalString.GetString("确定"),LocalString.GetString("取消"),false)
            else
                Gac2Gas.StartTradeSimulationScenario()
                CUIManager.ShowUI(CLuaUIResources.BusinessMainWnd)
            end
        elseif tradeInfo.IsSealed == 1 then
            g_MessageMgr:ShowMessage("WangCheng_Business_CannotOpenTradeSimulation")
        else
            CUIManager.ShowUI(CLuaUIResources.BusinessMainWnd)
        end
    else
        CUIManager.ShowUI(CLuaUIResources.BusinessMainWnd)
    end
end