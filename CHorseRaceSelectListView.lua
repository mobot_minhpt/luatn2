-- Auto Generated!!
local CHorseRaceSelectListView = import "L10.UI.CHorseRaceSelectListView"
local CHorseRaceSelectMapiTemplate = import "L10.UI.CHorseRaceSelectMapiTemplate"
local CommonDefs = import "L10.Game.CommonDefs"
local CZuoQiMgr = import "L10.Game.CZuoQiMgr"
local Extensions = import "Extensions"
local NGUITools = import "NGUITools"
local Object = import "System.Object"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CHorseRaceSelectListView.m_Init_CS2LuaHook = function (this) 
    CommonDefs.ListClear(this.list)
    Extensions.RemoveAllChildren(this.grid.transform)
    this.mapiTemplate:SetActive(false)
    do
        local i = 0
        while i < CZuoQiMgr.Inst.zuoqis.Count do
            local data = CZuoQiMgr.Inst.zuoqis[i]
			if CZuoQiMgr.IsMapiTemplateId(data.templateId) then
				local instance = NGUITools.AddChild(this.grid.gameObject, this.mapiTemplate)
				instance:SetActive(true)
				local template = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CHorseRaceSelectMapiTemplate))
				if template ~= nil then
					template:Init(data)
					CommonDefs.ListAdd(this.list, typeof(CHorseRaceSelectMapiTemplate), template)
					UIEventListener.Get(instance).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(instance).onClick, MakeDelegateFromCSFunction(this.OnMapClicked, VoidDelegate, this), true)
				end
			end
            i = i + 1
        end
    end
    this.grid:Reposition()
    this.scrollview:ResetPosition()
    if this.list.Count > 0 then
        this:OnMapClicked(this.list[0].gameObject)
    end
end
CHorseRaceSelectListView.m_OnMapClicked_CS2LuaHook = function (this, go) 
    do
        local i = 0
        while i < this.list.Count do
            this.list[i]:Select(this.list[i].gameObject == go)
            if this.list[i].gameObject == go and this.onMapiSelect ~= nil then
                GenericDelegateInvoke(this.onMapiSelect, Table2ArrayWithCount({this.list[i].data}, 1, MakeArrayClass(Object)))
            end
            i = i + 1
        end
    end
end
