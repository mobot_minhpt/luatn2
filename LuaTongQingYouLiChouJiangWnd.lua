local QnCurve = import "L10.UI.QnCurve"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local UITable = import "UITable"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local Random = import "UnityEngine.Random"
local IdPartition = import "L10.Game.IdPartition"
local CItem = import "L10.Game.CItem"
local NGUIText = import "NGUIText"
local CEquipment = import "L10.Game.CEquipment"
local CButton = import "L10.UI.CButton"
local Item_Item = import "L10.Game.Item_Item"
local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"

LuaTongQingYouLiChouJiangWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaTongQingYouLiChouJiangWnd, "ChouJiangBtn", "ChouJiangBtn", CButton)
RegistChildComponent(LuaTongQingYouLiChouJiangWnd, "CloseButton", "CloseButton", GameObject)
RegistChildComponent(LuaTongQingYouLiChouJiangWnd, "QiFuLeftTimesLabel", "QiFuLeftTimesLabel", UILabel)
RegistChildComponent(LuaTongQingYouLiChouJiangWnd, "Table", "Table", UITable)
RegistChildComponent(LuaTongQingYouLiChouJiangWnd, "QiFuItemTemplate", "QiFuItemTemplate", CCommonLuaScript)
RegistChildComponent(LuaTongQingYouLiChouJiangWnd, "Curve", "Curve", QnCurve)

--@endregion RegistChildComponent end

RegistClassMember(LuaTongQingYouLiChouJiangWnd,"selectedTemplateId")
RegistClassMember(LuaTongQingYouLiChouJiangWnd,"selectedCount")
RegistClassMember(LuaTongQingYouLiChouJiangWnd,"speedFactor")
RegistClassMember(LuaTongQingYouLiChouJiangWnd,"items")
RegistClassMember(LuaTongQingYouLiChouJiangWnd,"MaxItemCount")


function LuaTongQingYouLiChouJiangWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.ChouJiangBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnChouJiangBtnClick()
	end)


	
	UIEventListener.Get(self.CloseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCloseButtonClick()
	end)


    --@endregion EventBind end
end

function LuaTongQingYouLiChouJiangWnd:OnDisable()
	if self.m_tick then
		UnRegisterTick(self.m_tick)
		self.m_tick = nil
	end
end

function LuaTongQingYouLiChouJiangWnd:Init()
	self.speedFactor = 0.64
	self.MaxItemCount = 15
	self.QiFuItemTemplate.gameObject:SetActive(false)
	self.ChouJiangBtn.gameObject:SetActive(true)
	self.selectedTemplateId = 0
	self.selectedCount = 0

	self:InitQiFuItemCells()
	self:RefreshChouJiangBtn()
	self:OnQiFuReturnResult(LuaZhouNianQing2021Mgr.itemInfos,LuaZhouNianQing2021Mgr.selectdIndex)

end

function LuaTongQingYouLiChouJiangWnd:OnQiFuReturnResult(itemInfos,selectedIndex)

	if System.String.IsNullOrEmpty(itemInfos) then return end

	local arr = g_LuaUtil:StrSplitAdv(itemInfos,",")
	local len = #arr
	local halflen = len/2
	if len == 0 or math.fmod(len,2)~=0 then return end
	if selectedIndex < 0 or selectedIndex >= halflen then return end
	for i=1,self.MaxItemCount do
		local itemctrl = self.items[i]
		if i*2 <=len then 
			local id = tonumber(arr[i*2-1])
			local count = tonumber(arr[i*2])

			if i == selectedIndex + 1 then
				self.selectedTemplateId = id
				self.selectedCount = count
			end
			itemctrl:Init(id,count, true)
			itemctrl:Selected(false)
		else
		end
	end

	self:RefreshChouJiangBtn()
	self.ChouJiangBtn.Enabled = false
	self.CloseButton:SetActive(false)
	self:DoLotteryAnimation(selectedIndex)
end

RegistClassMember(LuaTongQingYouLiChouJiangWnd,"m_tick")

function LuaTongQingYouLiChouJiangWnd:DoLotteryAnimation(selectedIndex)
	local totalSteps = 0
	local threshold = self.MaxItemCount / 3
	if selectedIndex < threshold and Random.value >= 0.5 then
		totalSteps = self.MaxItemCount * 2 + selectedIndex + 1;
	else
		totalSteps = self.MaxItemCount * 1 + selectedIndex + 1;
	end

	local curstep = 0
	self:AddAnimTick(curstep,totalSteps,selectedIndex)
end

function LuaTongQingYouLiChouJiangWnd:AddAnimTick(curstep,totalSteps,selectedIndex)

	if curstep == totalSteps then 
		local rindex = selectedIndex + 1
		if rindex > 0 and selectedIndex < #self.items then
			self.items[rindex]:ShowBoom()
		end
		self:DisplayObtainQiFuItemMessage()
		self.selectedTemplateId = 0
		self.selectedCount = 0
		self:RefreshChouJiangBtn()
		self.CloseButton:SetActive(true)
	else
		local delay = self.Curve.Curve:Evaluate(curstep / totalSteps) * self.speedFactor
		if delay <=0.1 then delay = 0.1 end

		if self.m_tick then
			UnRegisterTick(self.m_tick)
			self.m_tick = nil
		end

		self.m_tick = RegisterTickOnce(function()

			for i=1,self.MaxItemCount do
				self.items[i]:Selected(i-1 == math.fmod(curstep,self.MaxItemCount))
			end

			curstep = curstep+1
			self:AddAnimTick(curstep,totalSteps,selectedIndex)
		end,delay*1000)
	end
end

function LuaTongQingYouLiChouJiangWnd:DisplayObtainQiFuItemMessage()
	if IdPartition.IdIsItem(self.selectedTemplateId) then
		local item = Item_Item.GetData(self.selectedTemplateId)
		if item then
			local colorstr = CItem.GetColorString(self.selectedTemplateId)
			if self.selectedCount > 1 then
				g_MessageMgr:ShowMessage("OBTAIN_COMMON_ITEM",colorstr,item.Name .. "x" .. self.selectedCount)
			else
				g_MessageMgr:ShowMessage("OBTAIN_COMMON_ITEM",colorstr,item.Name)
			end
		end
	else
		local equip = EquipmentTemplate_Equip.GetData(self.selectedTemplateId);
		if equip then 
			local colorstr = NGUIText.EncodeColor24(CEquipment.GetColor(self.selectedTemplateId))
			g_MessageMgr:ShowMessage("OBTAIN_COMMON_ITEM",colorstr,equip.Name)
		end
	end
end

function LuaTongQingYouLiChouJiangWnd:RefreshChouJiangBtn( )
	local count = LuaZhouNianQing2021Mgr.chouJiangTimes
	self.QiFuLeftTimesLabel.text = SafeStringFormat3(LocalString.GetString("%d次"),count)
	self.ChouJiangBtn.Enabled = count > 0
end

function LuaTongQingYouLiChouJiangWnd:InitQiFuItemCells( )
	if self.items then return end
	self.items = {}
	for i=1,self.MaxItemCount do
		local item = NGUITools.AddChild(self.Table.gameObject,self.QiFuItemTemplate.gameObject)
		item:SetActive(true)
		local itemTemplate = item.transform:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
		itemTemplate:Init(0,0)
		self.items[i] = itemTemplate
	end
	self.Table:Reposition()
end

function LuaTongQingYouLiChouJiangWnd:OnEnable()
	self.CloseButton:SetActive(true)
end

--@region UIEvent

function LuaTongQingYouLiChouJiangWnd:OnChouJiangBtnClick()
	LuaZhouNianQing2021Mgr.RequestChouJiang()
end

function LuaTongQingYouLiChouJiangWnd:OnCloseButtonClick()
	CUIManager.CloseUI(CLuaUIResources.TongQingYouLiChouJiangWnd)
end

--@endregion UIEvent

