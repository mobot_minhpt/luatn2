local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local Quaternion = import "UnityEngine.Quaternion"
local CUIManager = import "L10.UI.CUIManager"
local CFashionMgr = import "L10.Game.CFashionMgr"

LuaOffWorldPassReward2Wnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaOffWorldPassReward2Wnd, "CloseBtn", "CloseBtn", GameObject)
RegistChildComponent(LuaOffWorldPassReward2Wnd, "weapon", "weapon", GameObject)
RegistChildComponent(LuaOffWorldPassReward2Wnd, "ShareBtn", "ShareBtn", GameObject)

--@endregion RegistChildComponent end

function LuaOffWorldPassReward2Wnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

RegistClassMember(LuaOffWorldPassReward2Wnd,"m_ModelTextureLoader")
RegistClassMember(LuaOffWorldPassReward2Wnd,"m_ModelName")
RegistClassMember(LuaOffWorldPassReward2Wnd,"weaponRo")
RegistClassMember(LuaOffWorldPassReward2Wnd,"m_ShareTick")
RegistClassMember(LuaOffWorldPassReward2Wnd, "swShowArgs")

function LuaOffWorldPassReward2Wnd:SetOpBtnVisible(sign)
  self.CloseBtn:SetActive(sign)
  self.ShareBtn:SetActive(sign)
end

function LuaOffWorldPassReward2Wnd:Init()
  local onCloseClick = function(go)
    CUIManager.CloseUI(CLuaUIResources.OffWorldPassReward2Wnd)
  end

  CommonDefs.AddOnClickListener(self.CloseBtn,DelegateFactory.Action_GameObject(onCloseClick),false)

  local onShareClick = function(go)
    self:SetOpBtnVisible(false)
    if self.m_ShareTick then
      UnRegisterTick(self.m_ShareTick)
      self.m_ShareTick = nil
    end
    self.m_ShareTick = RegisterTickWithDuration(function ()
      self:SetOpBtnVisible(true)
    end, 1000, 1000)
    CUICommonDef.CaptureScreenAndShare()
  end
  CommonDefs.AddOnClickListener(self.ShareBtn,DelegateFactory.Action_GameObject(onShareClick),false)

	--LuaOffWorldPassMgr.WDCreate = {weaponType, wuxing}
  self:InitSWModel({weaponType = LuaOffWorldPassMgr.WDCreate[1], wuxing = LuaOffWorldPassMgr.WDCreate[2]})
end

function LuaOffWorldPassReward2Wnd:InitSWModel(data)
  local equipmentId = nil
  local weaponTexture = nil
  OffWorldPass_Fashion.Foreach(function(i,v)
    if v.type == data.weaponType and v.wuxing == data.wuxing then
      equipmentId = v.equipmentId
      weaponTexture = v.weaponTexture
    end
  end)

  if not equipmentId or not weaponTexture then
    return
  end

  local equipData = EquipmentTemplate_Equip.GetData(equipmentId)
  local m_FakeModel = self.weapon:GetComponent(typeof(CUITexture))
  m_FakeModel:LoadMaterial(weaponTexture)

  -- self.m_ModelTextureLoader = LuaDefaultModelTextureLoader.Create(function (ro)
  --   self:LoadModel(ro, equipData.Prefab)
  -- end)
  
  -- self.m_ModelName = "_OffWorldPassWeaponGetPreview_"
  -- self.swShowArgs = LuaOffWorldPassMgr.GetWeaponShowArgs(data)
  -- --m_FakeModel.mainTexture = CUIManager.CreateModelTexture(self.m_ModelName, self.m_ModelTextureLoader,90,0,-0.3,3.04,false,true,1,true)
  -- m_FakeModel.mainTexture = CUIManager.CreateModelTexture(self.m_ModelName, self.m_ModelTextureLoader,self.swShowArgs[1],self.swShowArgs[2],self.swShowArgs[3],self.swShowArgs[4],false,true,1,true)
	-- local onDrag = function(go,delta)
	-- 	self.weaponRo.transform:Rotate(Vector3.right,(delta.x+delta.y+delta.z))
	-- end
	-- CommonDefs.AddOnDragListener(self.weapon,DelegateFactory.Action_GameObject_Vector2(onDrag),false)
end

function LuaOffWorldPassReward2Wnd:LoadModel(ro, prefabname)
    ro:LoadMain(prefabname, nil, false, false)
		--ro.transform.localRotation = Quaternion.Euler(0, 90, -90)
		ro.transform.localRotation = Quaternion.Euler(self.swShowArgs[5],self.swShowArgs[6],self.swShowArgs[7])
    self.weaponRo = ro
end
--@region UIEvent

--@endregion UIEvent

function LuaOffWorldPassReward2Wnd:OnDestroy()
	CUIManager.DestroyModelTexture(self.m_ModelName)
  if LuaOffWorldPassMgr.ShowFashionId then
    if LuaOffWorldPassMgr.CheckSWWeapon(LuaOffWorldPassMgr.ShowFashionId) then
      msg = g_MessageMgr:FormatMessage('GET_NEW_SW_WEAPON_FASHION')
      MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function ()
        LuaOffWorldPassMgr.FirstOpenClothesWnd = true
        LuaAppearancePreviewMgr:CloseAppearanceWnd()
        CFashionMgr.Inst:ShowAppearanceWnd(LuaOffWorldPassMgr.ShowFashionId)
      end), nil, nil, nil, false)
    end
  end
  if self.m_ShareTick then
    UnRegisterTick(self.m_ShareTick)
    self.m_ShareTick = nil
  end
end
