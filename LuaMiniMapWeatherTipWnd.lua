local CommonDefs = import "L10.Game.CommonDefs"
local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local CWeatherMgr = import "L10.Game.CWeatherMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Color = import "UnityEngine.Color"
local NGUIMath = import "NGUIMath"
local CChatLinkMgr = import "CChatLinkMgr"
local GameObject = import "UnityEngine.GameObject"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaMiniMapWeatherTipWnd = class()

RegistChildComponent(LuaMiniMapWeatherTipWnd, "Icon", "Icon", CUITexture)
RegistChildComponent(LuaMiniMapWeatherTipWnd, "UpBg1", "UpBg1", UITexture)
RegistChildComponent(LuaMiniMapWeatherTipWnd, "UpBg2", "UpBg2", UITexture)
RegistChildComponent(LuaMiniMapWeatherTipWnd, "DownBg1", "DownBg1", UITexture)
RegistChildComponent(LuaMiniMapWeatherTipWnd, "DownBg2", "DownBg2", UITexture)
RegistChildComponent(LuaMiniMapWeatherTipWnd, "Name", "Name", UILabel)
RegistChildComponent(LuaMiniMapWeatherTipWnd, "Desc", "Desc", UILabel)
RegistChildComponent(LuaMiniMapWeatherTipWnd, "EffectLab1", "EffectLab1", UILabel)
RegistChildComponent(LuaMiniMapWeatherTipWnd, "UpEffect", "UpEffect", UISprite)
RegistChildComponent(LuaMiniMapWeatherTipWnd, "DownEffect", "DownEffect", UISprite)

RegistClassMember(LuaMiniMapWeatherTipWnd, "m_Bg")
RegistClassMember(LuaMiniMapWeatherTipWnd, "m_Effect2")
RegistClassMember(LuaMiniMapWeatherTipWnd, "m_EffectLab2")
RegistClassMember(LuaMiniMapWeatherTipWnd, "m_Grid")
RegistClassMember(LuaMiniMapWeatherTipWnd, "m_Template")
RegistClassMember(LuaMiniMapWeatherTipWnd, "m_Effects")
RegistClassMember(LuaMiniMapWeatherTipWnd, "m_WeatherInfos")

function LuaMiniMapWeatherTipWnd:Awake()
    self.m_Bg           = self.transform:Find("Anchor/Bg"):GetComponent(typeof(UISprite))

    self.m_Effect2      = self.transform:Find("Anchor/Bottom/Effect2"):GetComponent(typeof(UIWidget))
    self.m_EffectLab2   = self.transform:Find("Anchor/Bottom/Effect2/EffectLab2"):GetComponent(typeof(UILabel))
    self.m_Grid         = self.transform:Find("Anchor/Bottom/Effect2/Grid"):GetComponent(typeof(UIGrid))
    self.m_Template     = self.transform:Find("Anchor/Bottom/Effect2/Template").gameObject
    self.m_Effects      = self.transform:Find("Anchor/Bottom").gameObject

    self.m_WeatherInfos = {}
    self.m_Template:SetActive(false)
end

function LuaMiniMapWeatherTipWnd:Init()
    self:LoadWeather()

    local isInMySect = LuaZongMenMgr:IsMySectScene()
    if isInMySect then
        if self:GetGuanTianXiangLevel() > 0 then
            Gac2Gas:QuerySectFutureWeather()
        else
            self.m_Effect2.gameObject:SetActive(true)
            self.m_EffectLab2.text = LocalString.GetString("需习得观天象技能")

            local b1 = NGUIMath.CalculateRelativeWidgetBounds(self.EffectLab1.transform)
            local b2 = NGUIMath.CalculateRelativeWidgetBounds(self.m_Effect2.transform)
            self.m_Bg.height = b1.size.y + b2.size.y + 185
        end
    else 
        local b1 = NGUIMath.CalculateRelativeWidgetBounds(self.EffectLab1.transform)
        self.m_Bg.height = b1.size.y + 225
    end
end

--@region UIEvent

--@endregion

function LuaMiniMapWeatherTipWnd:OnEnable()
    g_ScriptEvent:AddListener("WeatherIsChanged", self, "OnWeatherIsChanged")
    g_ScriptEvent:AddListener("ReplySectFutureWeather", self, "OnReplySectFutureWeather")
end

function LuaMiniMapWeatherTipWnd:OnDisable()
    g_ScriptEvent:RemoveListener("WeatherIsChanged", self, "OnWeatherIsChanged")
    g_ScriptEvent:RemoveListener("ReplySectFutureWeather", self, "OnReplySectFutureWeather")
end

function LuaMiniMapWeatherTipWnd:OnWeatherIsChanged()
    self:LoadWeather()

    local isInMySect = LuaZongMenMgr:IsMySectScene()
    if isInMySect and self:GetGuanTianXiangLevel() > 0 then
        Gac2Gas:QuerySectFutureWeather()
    end
end

function LuaMiniMapWeatherTipWnd:GetGuanTianXiangLevel()
    local clsExists = CommonDefs.DictContains(CClientMainPlayer.Inst.PlayProp.PersistPlayData, typeof(UInt32), EnumPersistPlayDataKey_lua.eGuanTianXiangSkillData)
    local playData = clsExists and CClientMainPlayer.Inst.PlayProp.PersistPlayData[EnumPersistPlayDataKey_lua.eGuanTianXiangSkillData] or nil
    local skillLevel = playData and tonumber(playData.StringData) or 0

    return skillLevel
end

function LuaMiniMapWeatherTipWnd:OnReplySectFutureWeather(hours, weatherInfos)
    self.m_Effect2.gameObject:SetActive(true)

    local hour = hours % 24
    local day = (hours - hour) / 24
    
    local timeStr = ""
    
    if day ~= 0 then
        timeStr = timeStr .. SafeStringFormat3(LocalString.GetString("%d天"), day)
    end

    if hour ~= 0 then
        timeStr = timeStr .. SafeStringFormat3(LocalString.GetString("%d小时"), hour)
    end

    if #weatherInfos == 0 then
        self.m_EffectLab2.text = SafeStringFormat3(LocalString.GetString("未来%s无特殊天象"), timeStr)
    else
        self.m_EffectLab2.text = SafeStringFormat3(LocalString.GetString("未来%s特殊天象："), timeStr)
    end

    self:InitFutureWeather(weatherInfos)
end

function LuaMiniMapWeatherTipWnd:InitFutureWeather(weatherInfos)
    local offset = #weatherInfos - #self.m_WeatherInfos

    if offset > 0 then
        for i = 1, offset do
            local go = NGUITools.AddChild(self.m_Grid.gameObject, self.m_Template)
            table.insert(self.m_WeatherInfos, go)
        end
    elseif offset < 0 then
        for i = 1, -offset do
            local startIndex = #weatherInfos
            local endIndex = #self.m_WeatherInfos
            for i = startIndex, endIndex do
                GameObject.Destroy(self.m_WeatherInfos[i])
                self.m_WeatherInfos[i] = nil
            end
        end
    end

    for i = 1, #self.m_WeatherInfos do
        self:InitItem(self.m_WeatherInfos[i], weatherInfos[i])
    end

    self.m_Grid:Reposition()
    local b1 = NGUIMath.CalculateRelativeWidgetBounds(self.EffectLab1.transform)
    local b2 = NGUIMath.CalculateRelativeWidgetBounds(self.m_Effect2.transform)
    self.m_Bg.height = b1.size.y + b2.size.y + 235
end

function LuaMiniMapWeatherTipWnd:InitItem(item, info)
    local icon = item.transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local name = item.transform:Find("Name"):GetComponent(typeof(UILabel))
    local time = item.transform:Find("Time"):GetComponent(typeof(UILabel))

    local data = SoulCore_ZongPaiWeather.GetData(info.weatherId)
    if data ~= nil then
        local iconPath = SafeStringFormat3("UI/Texture/Transparent/Material/%s.mat", data.Icon)
        icon:LoadMaterial(iconPath)
        name.text = data.Name
    end

    local now = CServerTimeMgr.Inst:GetZone8Time()
    local startTime = CServerTimeMgr.ConvertTimeStampToZone8Time(info.startTime)
    local endTimeTime = CServerTimeMgr.ConvertTimeStampToZone8Time(info.endTime)

    local toStartTimeDay = CommonDefs.op_Subtraction_DateTime_DateTime(startTime, now).Days

    local dayStr = ""
    dayStr = SafeStringFormat3(LocalString.GetString("%.d月%.d日"), startTime.Month, startTime.Day)
    time.text = SafeStringFormat3(LocalString.GetString("%s %.f时-%.f时"), dayStr, startTime.Hour, endTimeTime.Hour)

    item:SetActive(true)
end

function LuaMiniMapWeatherTipWnd:LoadWeather()
    local currentWeathers = ((CWeatherMgr.Inst or {}).m_CurrentWeather or {}).Weathers
    if currentWeathers ~= nil and currentWeathers.Length > 0 then
        --宗门天气同一时间应只会存在一种
        local currentWeather = currentWeathers[0]

        local data = SoulCore_ZongPaiWeather.GetData(EnumToInt(currentWeather))
        if data ~= nil then
            local icon = SafeStringFormat3("UI/Texture/Transparent/Material/%s.mat", data.Icon)
            local type = data.Type

            self.Icon:LoadMaterial(icon)
            self.Name.text = data.Name
            self.Desc.text = data.TypeDescription

            local skillData = Menpai_GuanTianXiang.GetData(self:GetGuanTianXiangLevel())
            local effectStr = SafeStringFormat3("%.f%%", SoulCore_Settings.GetData().WeatherImpactLianHuaFactor * 100)
            self.EffectLab1.text = CChatLinkMgr.TranslateToNGUIText(SafeStringFormat3(data.Description, effectStr))

            local hasBg2 = true
            local isUpBg = false
            if type == 1 then
                hasBg2 = false
                isUpBg = true

                self.DownEffect.gameObject:SetActive(false)
                self.Desc.color = Color.white
            elseif type == 2 then
                hasBg2 = false
                isUpBg = true

                local mainPlayer = (CClientMainPlayer.Inst or {})
                local wuXing = (mainPlayer.BasicProp or {}).MingGe
                self.DownEffect.gameObject:SetActive(wuXing ~= nil and CommonDefs.ListContains(data.ImpactList, typeof(UInt32), wuXing))
                self.Desc.color = Color.white
            elseif type == 3 then
                hasBg2 = true
                isUpBg = false
                
                self.DownEffect.gameObject:SetActive(true)
                self.Desc.color = Color.red
            end

            self.UpBg1.gameObject:SetActive(isUpBg)
            self.UpBg2.gameObject:SetActive(hasBg2 and isUpBg)
            self.DownBg1.gameObject:SetActive(not isUpBg)
            self.DownBg2.gameObject:SetActive(hasBg2 and not isUpBg)

        end

    end

    self.m_Effect2:UpdateAnchors()
end

function LuaMiniMapWeatherTipWnd:DaysOfYear(year)
    if (year % 4 == 0 and year % 100 ~= 0) or year % 400 == 0 then
        return 366
    end
    return 365
end
