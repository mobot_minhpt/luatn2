-- Auto Generated!!
local CommonDefs = import "L10.Game.CommonDefs"
local CSecondPwdContentItem = import "L10.UI.CSecondPwdContentItem"
local CSecondPwdContentWnd = import "L10.UI.CSecondPwdContentWnd"
local DelegateFactory = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local MsgPackImpl = import "MsgPackImpl"
local Object = import "System.Object"
local SecondaryPassword_ProtectContent = import "L10.Game.SecondaryPassword_ProtectContent"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local Vector3 = import "UnityEngine.Vector3"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CSecondPwdContentWnd.m_Init_CS2LuaHook = function (this) 
    Gac2Gas.RequestSecondaryPasswordProtectContent()

    local keyList = CreateFromClass(MakeGenericClass(List, UInt32))
    local dataList = CreateFromClass(MakeGenericClass(List, SecondaryPassword_ProtectContent))
    SecondaryPassword_ProtectContent.Foreach(DelegateFactory.Action_object_object(function (key, val) 
        CommonDefs.ListAdd(keyList, typeof(UInt32), key)
    end))
    CommonDefs.ListIterate(keyList, DelegateFactory.Action_object(function (___value) 
        local item = ___value
        local data = SecondaryPassword_ProtectContent.GetData(item)
        CommonDefs.ListAdd(dataList, typeof(SecondaryPassword_ProtectContent), data)
    end))
    CommonDefs.ListSort1(dataList, typeof(SecondaryPassword_ProtectContent), MakeDelegateFromCSFunction(this.SortFunc, MakeGenericClass(Comparison, SecondaryPassword_ProtectContent), this))

    CommonDefs.ListIterate(dataList, DelegateFactory.Action_object(function (___value) 
        local item = ___value
        local go = TypeAs(CommonDefs.Object_Instantiate(this.ItemPrefab), typeof(GameObject))
        go:SetActive(true)
        go.transform.parent = this.grid.transform
        go.transform.localScale = Vector3.one
        local cmp = CommonDefs.GetComponent_GameObject_Type(go, typeof(CSecondPwdContentItem))
        cmp:Init(item.ID, item.ShowName)
        CommonDefs.ListAdd(this.cmpList, typeof(CSecondPwdContentItem), cmp)
    end))
    this.grid:Reposition()
end
CSecondPwdContentWnd.m_Awake_CS2LuaHook = function (this) 
    this.ItemPrefab:SetActive(false)

    UIEventListener.Get(this.closeButton).onClick = MakeDelegateFromCSFunction(this.OnClickCloseButton, VoidDelegate, this)
    UIEventListener.Get(this.cancelButton).onClick = MakeDelegateFromCSFunction(this.OnClickCancelButton, VoidDelegate, this)
    UIEventListener.Get(this.confirmButton).onClick = MakeDelegateFromCSFunction(this.OnClickConfirmButton, VoidDelegate, this)
    UIEventListener.Get(this.tipBtn).onClick = MakeDelegateFromCSFunction(this.OnClickTipButton, VoidDelegate, this)
end
CSecondPwdContentWnd.m_OnClickConfirmButton_CS2LuaHook = function (this, go) 
    local list = CreateFromClass(MakeGenericClass(List, Object))
    do
        local i = 0
        while i < this.cmpList.Count do
            if this.cmpList[i]:GetValue() then
                CommonDefs.ListAdd(list, typeof(UInt32), this.cmpList[i].ID)
            end
            i = i + 1
        end
    end
    Gac2Gas.UpdateSecondaryPasswordProtectContent(MsgPackImpl.pack(list))
end
CSecondPwdContentWnd.m_OnReplySecondaryPasswordProtectContent_CS2LuaHook = function (this, list) 
    do
        local i = 0
        while i < this.cmpList.Count do
            if CommonDefs.ListContains(list, typeof(UInt32), this.cmpList[i].ID) then
                this.cmpList[i]:SetValue(true)
            else
                this.cmpList[i]:SetValue(false)
            end
            i = i + 1
        end
    end
end
