-- 个位为花色，百位为大小，按数字大小排列
EnumDouDiZhuCard = {
	Hei_3 = 301,
	Hong_3 = 302,
	Mei_3 = 303,
	Fang_3 = 304,

	Hei_4 = 401,
	Hong_4 = 402,
	Mei_4 = 403,
	Fang_4 = 404,

	Hei_5 = 501,
	Hong_5 = 502,
	Mei_5 = 503,
	Fang_5 = 504,

	Hei_6 = 601,
	Hong_6 = 602,
	Mei_6 = 603,
	Fang_6 = 604,

	Hei_7 = 701,
	Hong_7 = 702,
	Mei_7 = 703,
	Fang_7 = 704,

	Hei_8 = 801,
	Hong_8 = 802,
	Mei_8 = 803,
	Fang_8 = 804,

	Hei_9 = 901,
	Hong_9 = 902,
	Mei_9 = 903,
	Fang_9 = 904,

	Hei_10 = 1001,
	Hong_10 = 1002,
	Mei_10 = 1003,
	Fang_10 = 1004,

	Hei_J = 1101,
	Hong_J = 1102,
	Mei_J = 1103,
	Fang_J = 1104,

	Hei_Q = 1201,
	Hong_Q = 1202,
	Mei_Q = 1203,
	Fang_Q = 1204,

	Hei_K = 1301,
	Hong_K = 1302,
	Mei_K = 1303,
	Fang_K = 1304,

	Hei_A = 1401,
	Hong_A = 1402,
	Mei_A = 1403,
	Fang_A = 1404,

	Hei_2 = 2001,
	Hong_2 = 2002,
	Mei_2 = 2003,
	Fang_2 = 2004,

	BlackJoker = 10000,
	RedJoker = 20000,
}

EnumDouDiZhuCardName = {}
for k, v in pairs(EnumDouDiZhuCard) do
	EnumDouDiZhuCardName[v] = k
end

EnumDouDiZhuCardsType = {
	eNone = 0,
	eDanZhang = 1,
	eDuiZi= 2,
	eShunZi = 3,
	eShuangShun = 4,
	e3BuDai = 5,
	e3Dai1 = 6,
	e3Dai2 = 7,
	eFeiJi = 8,
	eFeiJiDai1 = 9,
	eFeiJiDai2 = 10,
	e4Dai2 = 11,
	e4Dai2Dui = 12,
	e4FeiJi = 13,
	e4FeiJiDai2 = 14,
	e4FeiJiDai2Dui = 15,
	eZhaDan = 16,
	eHuoJian = 17,
}

EnumDouDiZhuBoardStatus = {
	ePrepare = 1,
	eFaPai = 2,
	eQiangDiZhu = 3,
	eChuPai = 4,
	eEnd = 5,
}

EnumQiangDiZhuStatus = {
	eWait = 0,
	eBuJiao = 1,
	eBuQiang = 2,
	eJiaoDiZhu = 3,
	eQiangDiZhu = 4,
}

CDouDiZhuBoard = class()

RegistClassMember(CDouDiZhuBoard, "m_IndexInfo")
RegistClassMember(CDouDiZhuBoard, "m_ReserveCards")
RegistClassMember(CDouDiZhuBoard, "m_DefaultDiZhuCard")

RegistClassMember(CDouDiZhuBoard, "m_CandidateDiZhuIndex")
RegistClassMember(CDouDiZhuBoard, "m_QiangDiZhuIndex")
RegistClassMember(CDouDiZhuBoard, "m_DiZhuIndex")

RegistClassMember(CDouDiZhuBoard, "m_MaxCardIndex")
RegistClassMember(CDouDiZhuBoard, "m_ChuPaiIndex")

RegistClassMember(CDouDiZhuBoard, "m_DuZhuScore")
RegistClassMember(CDouDiZhuBoard, "m_DuZhuPower")

RegistClassMember(CDouDiZhuBoard, "m_RoundPlayerCount")
RegistClassMember(CDouDiZhuBoard, "m_ReserveCardCount")

function CDouDiZhuBoard:Ctor()
	self.m_RoundPlayerCount =3
	self.m_ReserveCardCount =3

	self.m_IndexInfo = {}
	self:ResetRoundData()
end

function CDouDiZhuBoard:ResetRoundData()
	for index, info in pairs(self.m_IndexInfo) do
		self:SetIndexInfo(index, info.id, info.name, info.class, info.gender, info.grade, 
			info.score, info.ready, EnumQiangDiZhuStatus.eWait, 0, false, false,
			0, {}, {}, false, 0, false, 0)
	end

	self.m_ReserveCards = {}
	self.m_DefaultDiZhuCard = 0
	self.m_CandidateDiZhuIndex = 0
	self.m_QiangDiZhuIndex = 0
	self.m_DiZhuIndex = 0
	self.m_MaxCardIndex = 0
	self.m_ChuPaiIndex = 0
	self.m_DuZhuScore = 0
	self.m_DuZhuPower = 1
end

function CDouDiZhuBoard:InitIndexInfo(index, id, name, class, gender, grade, score)
	self:SetIndexInfo(index, id, name, class, gender, grade,
		score, false, EnumQiangDiZhuStatus.eWait, 0, false, false,
		0, {}, {}, false, 0, false, 0)
end

function CDouDiZhuBoard:SetIndexInfo(index, id, name, class, gender, grade,
	score, ready, qiangDiZhuStatus, qiangDiZhuTimes, isDiZhu, mingPai,
	handCardsCount, handCards, showCards, pass, chuPaiTimes, tuoGuan, usedTime)
	self.m_IndexInfo[index] = {
		index = index,
		id = id,
		name = name,
		class = class,
		gender = gender,
		grade = grade,
		score = score,
		ready = ready,
		qiangDiZhuStatus = qiangDiZhuStatus, 
		qiangDiZhuTimes = qiangDiZhuTimes,
		isDiZhu = isDiZhu,
		mingPai = mingPai,
		handCardsCount = handCardsCount,
		handCards = handCards,
		showCards = showCards,
		pass = pass,
		chuPaiTimes = chuPaiTimes,
		tuoGuan = tuoGuan,
		usedTime = usedTime,
	}
end

function CDouDiZhuBoard:GetIndexInfo(index)
	return self.m_IndexInfo[index]
end

function CDouDiZhuBoard:GetIndexInfoArray(index)
	local info = self.m_IndexInfo[index]
	if not info then return end

	local tbl = {}
	table.insert(tbl, info.index)
	table.insert(tbl, info.id)
	table.insert(tbl, info.name)
	table.insert(tbl, info.class)
	table.insert(tbl, info.gender)
	table.insert(tbl, info.grade)
	table.insert(tbl, info.score)
	table.insert(tbl, info.ready)
	table.insert(tbl, info.qiangDiZhuStatus)
	table.insert(tbl, info.qiangDiZhuTimes)
	table.insert(tbl, info.isDiZhu)
	table.insert(tbl, info.mingPai)
	table.insert(tbl, info.pass)
	table.insert(tbl, info.chuPaiTimes)
	table.insert(tbl, info.tuoGuan)
	table.insert(tbl, info.usedTime)

	return tbl
end

function CDouDiZhuBoard:SetQiangDiZhuIndex(index)
	self.m_QiangDiZhuIndex = index
end

function CDouDiZhuBoard:GetQiangDiZhuInfo()
	return self.m_IndexInfo[self.m_QiangDiZhuIndex]
end

function CDouDiZhuBoard:SetCandidateDiZhuIndex(index)
	self.m_CandidateDiZhuIndex = index
end

function CDouDiZhuBoard:GetCandidateDiZhuInfo()
	return self.m_IndexInfo[self.m_CandidateDiZhuIndex]
end

function CDouDiZhuBoard:SetDiZhuIndex(index)
	local info = self.m_IndexInfo[index]
	if not info then return end

	info.isDiZhu = true
	self.m_DiZhuIndex = index
end

function CDouDiZhuBoard:DiZhuAddReserveCards()
	local diZhuInfo = self:GetDiZhuInfo()
	if not diZhuInfo then return end
	for _, card in ipairs(self.m_ReserveCards) do
		table.insert(diZhuInfo.handCards, card)
	end
	diZhuInfo.handCardsCount = diZhuInfo.handCardsCount + #self.m_ReserveCards
	table.sort(diZhuInfo.handCards)
end

function CDouDiZhuBoard:GetDiZhuInfo()
	return self.m_IndexInfo[self.m_DiZhuIndex]
end

function CDouDiZhuBoard:SetChuPaiIndex(index)
	self.m_ChuPaiIndex = index
end

function CDouDiZhuBoard:GetChuPaiInfo()
	return self.m_IndexInfo[self.m_ChuPaiIndex]
end

function CDouDiZhuBoard:SetMaxCardIndex(index)
	self.m_MaxCardIndex = index
end

function CDouDiZhuBoard:DoQiangDiZhu(index, status)
	local info = self.m_IndexInfo[index]
	if not info then return end
	
	info.qiangDiZhuStatus = status
	if status >= EnumQiangDiZhuStatus.eJiaoDiZhu then
		info.qiangDiZhuTimes = info.qiangDiZhuTimes + 1
	end

	return true
end

function CDouDiZhuBoard:DoChuPai(index, tbl)
	if not tbl then return end
	local info = self.m_IndexInfo[index]
	if not info then return end

	-- 扣除手牌
	info.handCardsCount = info.handCardsCount - #tbl
	for _, card in ipairs(tbl) do
		local found, index = self:FindValueOfArray(info.handCards, card)
		if found then
			table.remove(info.handCards, index)
		end
	end
	-- 更新出的牌
	info.showCards = tbl 
	-- 更新出牌次数
	if #tbl > 0 then
		info.chuPaiTimes = info.chuPaiTimes + 1
	end

	return true
end
function CDouDiZhuBoard:IsMustChuPai(index)
	if index == self.m_MaxCardIndex then
		return true
	else
		return false
	end
end
function CDouDiZhuBoard:IsChuPaiValid(index, tbl)
	local info = self.m_IndexInfo[index]
	if not info or self.m_ChuPaiIndex ~= index then
		return false
	end
	
	-- 检查有效性
	if tbl then
		for _, card in ipairs(tbl) do
			if not EnumDouDiZhuCardName[card] or not self:FindValueOfArray(info.handCards, card) then
				return false
			end
		end
	end

	if self.m_MaxCardIndex > 0 and index ~= self.m_MaxCardIndex then
		-- 牌面有最大牌，检查牌型，比大小
		-- 不出牌则直接返回
		if not tbl or #tbl <= 0 then
			return true
		end
		local maxCardInfo = self.m_IndexInfo[self.m_MaxCardIndex]
		if maxCardInfo then
			local bGreater, bValid = self:CardsGreaterThan(maxCardInfo.showCards, tbl)
			if not bValid then
				return false, "DOUDIZHU_CHUPAI_NOT_VALID"
			elseif not bGreater then
				return false, "DOUDIZHU_CHUPAI_NOT_GREATER"
			end
		end
	else
		-- 自己最大
		if not tbl or #tbl <= 0 then
			return false, "DOUDIZHU_MUST_CHUPAI"
		end
		local cardsType = self:ParseCardsType(tbl)
		if not cardsType or cardsType == EnumDouDiZhuCardsType.eNone then
			return false, "DOUDIZHU_CHUPAI_NOT_VALID"
		end
	end

	return true
end

function CDouDiZhuBoard:GetHintChuPaiCards(index)
	local info = self.m_IndexInfo[index]
	if not info then return false end

	if index == self.m_MaxCardIndex then
		-- 选最小牌
		return self:GetMinOptimalCards(info.handCards)
	else
		-- 选大过当前最大的最小牌
		local maxInfo = self.m_IndexInfo[self.m_MaxCardIndex]
		return self:HasGreaterCardsThan(maxInfo and maxInfo.showCards or {}, info.handCards)
	end
end

function CDouDiZhuBoard:IsAllReady()
	local count = 0
	for index, info in pairs(self.m_IndexInfo) do
		if not info.ready then
			return false
		end
		count = count + 1
	end
	if count < self.m_RoundPlayerCount then
		return false
	end

	return true
end

--------------------------------------------------------------------------------
-- 牌型及大小比较相关
--------------------------------------------------------------------------------
function CDouDiZhuBoard:CardEquals(a, b)
	return math.floor(a / 100) == math.floor(b / 100)
end

function CDouDiZhuBoard:GetCardDigit(card)
	return math.floor(card / 100)
end

function CDouDiZhuBoard:FindValueOfArray(array, value)
	if not array then return end
	for i, val in ipairs(array) do
		if val == value then
			return true, i
		end
	end
end

function CDouDiZhuBoard:FindGreaterValueOfArray(array, value)
	if not array then return end
	for i, val in ipairs(array) do
		if val > value then
			return true, i
		end
	end
end

-- 需考虑重复
function CDouDiZhuBoard:FindGreaterShunZiOfArray(array, maxValue, num)
	if not array then return end

	local minValue = maxValue - num + 1
	table.sort(array)

	local begin
	for i, val in ipairs(array) do
		if val > minValue then
			begin = i
			break
		end
	end
	if not begin then return end

	local last
	local count = 0
	for i = begin, #array do
		local val = array[i]
		if not last or last + 1 == val then
			count = count + 1
			if count >= num then
				return true, begin, i
			end
		elseif last ~= val then
			count = 1
			begin = i
		end
		last = val
	end
end

-- digitList已排序, 不去重
function CDouDiZhuBoard:IsDigitContinuous(digitList)
	local last
	for _, digit in ipairs(digitList) do
		if last and last + 1 ~= digit then
			return false
		end
		last = digit
	end
	return true
end

-- digitList 已排序，找第一个不连续数
function CDouDiZhuBoard:GetSplitDigit(digitList)
	for i = 1, #digitList do
		local pre = digitList[i - 1]
		local post = digitList[i + 1]
		local curr = digitList[i]
		if not ((pre and pre + 1 == curr) or (post and curr + 1 == post)) then
			return i, curr
		end
	end
	return 1, digitList[1]
end

-- digitList 已排序
function CDouDiZhuBoard:InsertDigitSorted(digitList, digit, count)
	local insertIndex
	for i = 1, #digitList do
		if digitList[i] > digit then
			insertIndex = i
			break
		end
	end
	if not insertIndex then
		insertIndex = #digitList + 1
	end
	for i = 1, count do
		table.insert(digitList, insertIndex, digit)
	end
end

function CDouDiZhuBoard:SplitCardsPattern(pattern, num, toNum)
	if not pattern or not pattern[num] then return end
	local index, digit = self:GetSplitDigit(pattern[num])
	table.remove(pattern[num], index)
	if #(pattern[num]) <= 0 then
		pattern[num] = nil
	end

	pattern[toNum] = pattern[toNum] or {}
	pattern[num - toNum] = pattern[num - toNum] or {}
	self:InsertDigitSorted(pattern[toNum], digit, 1)
	self:InsertDigitSorted(pattern[num - toNum], digit, 1)
end

function CDouDiZhuBoard:ParseCardsPattern(tbl)
	if not tbl or #tbl <= 0 then
		return
	end

	-- 统计每个数字有几张牌
	local digitNumTbl = {}
	for i = 1, #tbl do
		local cardDigit = self:GetCardDigit(tbl[i])
		digitNumTbl[cardDigit] = digitNumTbl[cardDigit] and digitNumTbl[cardDigit] + 1 or 1
	end

	-- 按照张数优先，大小其次排序
	local digitNumList = {}
	for digit, num in pairs(digitNumTbl) do
		table.insert(digitNumList, {digit = digit, num = num})
	end
	table.sort(digitNumList, function(a, b)
		if a.num == b.num then
			return a.digit < b.digit
		else
			return a.num > b.num
		end
	end)

	-- 每个张数，对应的数字按序插入
	-- 例33344456 {[3] = {3,4},  [1] = {5,6}}
	local resTbl = {}
	for _, info in ipairs(digitNumList) do
		resTbl[info.num] = resTbl[info.num] or {}
		table.insert(resTbl[info.num], info.digit)
	end

	return resTbl
end

function CDouDiZhuBoard:DeepCopyPattern(pattern)
	local res = {}
	for i, digits in pairs(pattern) do
		res[i] = {}
		for j, digit in ipairs(digits) do
			table.insert(res[i], digit)
		end
	end
	return res
end

function CDouDiZhuBoard:ParseOneCards(tbl, pattern)
	if not tbl or not pattern then return end
	local oneCards = pattern[1]
	if not oneCards or #oneCards <= 0 then return end
	if #oneCards == 1 then
		-- 单张
		return EnumDouDiZhuCardsType.eDanZhang, oneCards[#oneCards], 1
	elseif #oneCards == 2 and oneCards[1] == self:GetCardDigit(EnumDouDiZhuCard.BlackJoker) and oneCards[2] == self:GetCardDigit(EnumDouDiZhuCard.RedJoker) then
		-- 火箭
		return EnumDouDiZhuCardsType.eHuoJian, oneCards[#oneCards], 1
	elseif #oneCards >= 5 and self:IsDigitContinuous(oneCards) then
		-- 顺子
		return EnumDouDiZhuCardsType.eShunZi, oneCards[#oneCards], #oneCards
	end
end

function CDouDiZhuBoard:ParseTwoCards(tbl, pattern)
	if not tbl or not pattern then return end
	local twoCards = pattern[2]
	local oneCards = pattern[1]
	if not twoCards or #twoCards <= 0 then return end
	if oneCards and #oneCards > 0 then
		-- 不能有单张
		return
	end
	if #twoCards == 1 then
		-- 对子
		return EnumDouDiZhuCardsType.eDuiZi, twoCards[#twoCards], 1
	elseif #twoCards >= 3 and self:IsDigitContinuous(twoCards) then
		-- 双顺
		return EnumDouDiZhuCardsType.eShuangShun, twoCards[#twoCards], #twoCards
	end
end

function CDouDiZhuBoard:ParseThreeCards(tbl, pattern)
	if not tbl or not pattern then return end
	local threeCards = pattern[3]
	local twoCards = pattern[2]
	local oneCards = pattern[1]
	if not threeCards or #threeCards <= 0 then return end

	if #threeCards == 1 then
		-- 3张连牌，3不带，3带1,3带2
		if #tbl == 5 then
			-- 3带2
			if oneCards and #oneCards > 0 then
				return
			end
			return EnumDouDiZhuCardsType.e3Dai2, threeCards[#threeCards], 1
		elseif #tbl == 4 then
			-- 3带1
			return EnumDouDiZhuCardsType.e3Dai1, threeCards[#threeCards], 1
		elseif #tbl == 3 then
			-- 3不带
			return EnumDouDiZhuCardsType.e3BuDai, threeCards[#threeCards], 1
		end
	else
		if #threeCards * 3 == #tbl then 
			-- 只有3张的
			if self:IsDigitContinuous(threeCards) then
				-- 飞机
				return EnumDouDiZhuCardsType.eFeiJi, threeCards[#threeCards], #threeCards
			end
		elseif #threeCards == #tbl - #threeCards * 3  then
			if self:IsDigitContinuous(threeCards) then
				-- 飞机带1
				return EnumDouDiZhuCardsType.eFeiJiDai1, threeCards[#threeCards], #threeCards
			end
		elseif #threeCards * 2 == #tbl - #threeCards * 3 then
			if (not oneCards or #oneCards <= 0) and self:IsDigitContinuous(threeCards) then
				-- 飞机带2
				return EnumDouDiZhuCardsType.eFeiJiDai2, threeCards[#threeCards], #threeCards
			end
		end

		if #tbl >= 12 and #threeCards > 3 and #threeCards * 2 > #tbl - #threeCards * 3 then
			-- 3个连以上，至少12张牌，拆一个三张再试
			local copyPattern = self:DeepCopyPattern(pattern)
			self:SplitCardsPattern(copyPattern, 3, 2)
			return self:ParseThreeCards(tbl, copyPattern)
		end
	end
end

function CDouDiZhuBoard:ParseFourCards(tbl, pattern)
	if not tbl or not pattern then return end
	local fourCards = pattern[4]
	local threeCards = pattern[3]
	local twoCards = pattern[2]
	local oneCards = pattern[1]
	if not fourCards or #fourCards <= 0 then return end
	if #fourCards == 1 then
		-- 4张连牌，炸弹，4带2，4带2对
		if #tbl == 8 and (not oneCards or #oneCards <= 0) and (not threeCards or #threeCards <= 0) then
			-- 8张，4带2对，不能有单张或3张
			return EnumDouDiZhuCardsType.e4Dai2Dui, fourCards[#fourCards], 1
		elseif #tbl == 6 then
			-- 6张，4带2
			return EnumDouDiZhuCardsType.e4Dai2, fourCards[#fourCards], 1
		elseif #tbl == 4 then
			-- 只有4张，炸弹
			return EnumDouDiZhuCardsType.eZhaDan, fourCards[#fourCards], 1
		end
	else
		if #fourCards * 4 == #tbl then
			-- 只有4张的
			if self:IsDigitContinuous(fourCards) then
				-- 4飞机
				return EnumDouDiZhuCardsType.e4FeiJi, fourCards[#fourCards], #fourCards
			end
		elseif #fourCards * 2 == #tbl - #fourCards * 4 then
			if self:IsDigitContinuous(fourCards) then
				-- 4飞机带2
				return EnumDouDiZhuCardsType.e4FeiJiDai2, fourCards[#fourCards], #fourCards
			end
		elseif #fourCards * 4 == #tbl - #fourCards * 4 then
			if (not oneCards or #oneCards <= 0) and (not threeCards or #threeCards <= 0) and self:IsDigitContinuous(fourCards) then
				-- 4飞机带2对，不能有单张或3张
				return EnumDouDiZhuCardsType.e4FeiJiDai2Dui, fourCards[#fourCards], #fourCards
			end
		end
	end

	if #tbl >= 8 then
		-- 拆一个4张再试，先拆3,1不行再拆2,2
		local copyPattern = self:DeepCopyPattern(pattern)
		self:SplitCardsPattern(copyPattern, 4, 3)
		local cardsType, maxMainDigit, num = self:ParseAllCards(tbl, copyPattern)
		if cardsType and cardsType ~= EnumDouDiZhuCardsType.eNone then
			return cardsType, maxMainDigit, num
		else
			local copyPattern = self:DeepCopyPattern(pattern)
			self:SplitCardsPattern(copyPattern, 4, 2)
			return self:ParseAllCards(tbl, copyPattern)
		end
	end
end

function CDouDiZhuBoard:ParseAllCards(tbl, pattern)
	if not tbl or not pattern then return end
	local fourCards = pattern[4]
	local threeCards = pattern[3]
	local twoCards = pattern[2]
	local oneCards = pattern[1]

	local hasFourCards = fourCards and #fourCards > 0
	local hasThreeCards = threeCards and #threeCards > 0
	local hasTwoCards = twoCards and #twoCards > 0

	if not hasFourCards and not hasThreeCards and not hasTwoCards then
		return self:ParseOneCards(tbl, pattern)
	elseif not hasFourCards and not hasThreeCards then
		return self:ParseTwoCards(tbl, pattern)
	elseif not hasFourCards then
		return self:ParseThreeCards(tbl, pattern)
	else
		return self:ParseFourCards(tbl, pattern)
	end
end

function CDouDiZhuBoard:ParseCardsType(tbl)
	if not tbl or #tbl <= 0 then
		return EnumDouDiZhuCardsType.eNone
	end

	local pattern = self:ParseCardsPattern(tbl)
	if not pattern or not next(pattern) then
		return EnumDouDiZhuCardsType.eNone
	end

	local cardsType, maxMainDigit, num = self:ParseAllCards(tbl, pattern)
	cardsType = cardsType or EnumDouDiZhuCardsType.eNone

	return cardsType, maxMainDigit, num
end

-- 返回是否比前一个大，是否合法
function CDouDiZhuBoard:CardsGreaterThan(oldTbl, newTbl)
	local oldType, oldMaxMainDigit, oldNum = self:ParseCardsType(oldTbl)
	local newType, newMaxMainDigit, newNum = self:ParseCardsType(newTbl)

	-- 不合法处理
	if newType == EnumDouDiZhuCardsType.eNone then
		return false, false
	elseif oldType == EnumDouDiZhuCardsType.eNone then
		return true, true
	end

	-- 火箭
	if oldType == EnumDouDiZhuCardsType.eHuoJian then
		return false, false
	elseif newType == EnumDouDiZhuCardsType.eHuoJian then
		return true, true
	end

	-- 炸弹
	if oldType == EnumDouDiZhuCardsType.eZhaDan then
		if newType == EnumDouDiZhuCardsType.eZhaDan then
			return newMaxMainDigit > oldMaxMainDigit, true
		else
			return false, false
		end
	elseif newType == EnumDouDiZhuCardsType.eZhaDan then
		return true, true
	end

	-- 其余数量必须一致
	if #newTbl ~= #oldTbl or newNum ~= oldNum then
		return false, false
	end

	-- 类型必须一致
	if newType ~= oldType then
		return false, false
	end

	return newMaxMainDigit > oldMaxMainDigit, true
end

function CDouDiZhuBoard:InsertCardsWithDigit(tbl, handCards, digit, count)
	if not tbl then return end
	for i, card in ipairs(handCards) do
		if self:GetCardDigit(card) == digit and not self:FindValueOfArray(tbl, card) then
			if count > 0 then
				table.insert(tbl, card)
				count = count - 1
			else
				break
			end
		end
	end
end

-- 手牌有没有炸弹或者火箭
function CDouDiZhuBoard:HasZhaDanOrHuoJianGreaterThan(handCards, maxMainDigit)
	if not handCards or #handCards < 2 then return false end
	local pattern = self:ParseCardsPattern(handCards)
	local fourCards = pattern[4]
	local oneCards = pattern[1]
	local resTbl = {}
	if fourCards and #fourCards > 0 then
		local found, index = self:FindGreaterValueOfArray(fourCards, maxMainDigit)
		if found then
			self:InsertCardsWithDigit(resTbl, handCards, fourCards[index], 4)
			return true, resTbl
		end
	end

	if oneCards and #oneCards > 1 then
		local resBlackJoker, indexBlackJoker = self:FindValueOfArray(oneCards, self:GetCardDigit(EnumDouDiZhuCard.BlackJoker))
		local resRedJoker, indexRedJoker = self:FindValueOfArray(oneCards, self:GetCardDigit(EnumDouDiZhuCard.RedJoker))
		if resBlackJoker and resRedJoker then
			self:InsertCardsWithDigit(resTbl, handCards, oneCards[indexBlackJoker], 1)
			self:InsertCardsWithDigit(resTbl, handCards, oneCards[indexRedJoker], 1)
			return true, resTbl
		end
	end

	return false
end

-- 手上有没有大的单张
function CDouDiZhuBoard:HasDanZhangGreaterThan(handCards, maxMainDigit)
	if not handCards or #handCards < 1 then return false end
	local pattern = self:ParseCardsPattern(handCards)
	local threeCards = pattern[3]
	local twoCards = pattern[2]
	local oneCards = pattern[1]

	local resTbl = {}
	if oneCards and #oneCards > 0 then
		-- 排除火箭
		local digitBlackJoker = self:GetCardDigit(EnumDouDiZhuCard.BlackJoker)
		local digitRedJoker = self:GetCardDigit(EnumDouDiZhuCard.RedJoker)
		local resBlackJoker, indexBlackJoker = self:FindValueOfArray(oneCards, digitBlackJoker)
		local resRedJoker, indexRedJoker = self:FindValueOfArray(oneCards, digitRedJoker)
		local hasHuoJian = resBlackJoker and resRedJoker
		local found, index = self:FindGreaterValueOfArray(oneCards, maxMainDigit)
		if found then
			if not (hasHuoJian and (oneCards[index] == digitBlackJoker or oneCards[index] == digitRedJoker)) then
				self:InsertCardsWithDigit(resTbl, handCards, oneCards[index], 1)
				return true, resTbl
			end
		end
	end
	if twoCards and #twoCards > 0 then
		local found, index = self:FindGreaterValueOfArray(twoCards, maxMainDigit)
		if found then
			self:InsertCardsWithDigit(resTbl, handCards, twoCards[index], 1)
			return true, resTbl
		end
	end
	if threeCards and #threeCards > 0 then
		local found, index = self:FindGreaterValueOfArray(threeCards, maxMainDigit)
		if found then
			self:InsertCardsWithDigit(resTbl, handCards, threeCards[index], 1)
			return true, resTbl
		end
	end

	return false
end

function CDouDiZhuBoard:HasDuiZiGreaterThan(handCards, maxMainDigit)
	if not handCards or #handCards < 2 then return false end
	local pattern = self:ParseCardsPattern(handCards)
	local threeCards = pattern[3]
	local twoCards = pattern[2]

	local resTbl = {}
	if twoCards and #twoCards > 0 then
		local found, index = self:FindGreaterValueOfArray(twoCards, maxMainDigit)
		if found then
			self:InsertCardsWithDigit(resTbl, handCards, twoCards[index], 2)
			return true, resTbl
		end
	end
	if threeCards and #threeCards > 0 then
		local found, index = self:FindGreaterValueOfArray(threeCards, maxMainDigit)
		if found then
			self:InsertCardsWithDigit(resTbl, handCards, threeCards[index], 2)
			return true, resTbl
		end
	end

	return false
end

function CDouDiZhuBoard:HasShunZiGreaterThan(handCards, maxMainDigit, num)
	if not handCards or #handCards < num then return false end
	local pattern = self:ParseCardsPattern(handCards)
	local threeCards = pattern[3]
	local twoCards = pattern[2]
	local oneCards = pattern[1]

	local resTbl = {}
	local found = false
	local beginIndex, endIndex
	if oneCards and #oneCards > 0 then
		found, beginIndex, endIndex = self:FindGreaterShunZiOfArray(oneCards, maxMainDigit, num)
	end

	if not found then
		-- 除炸弹全部拆分
		if threeCards and #threeCards > 0 then
			for i = 1, #threeCards do
				self:SplitCardsPattern(pattern, 3, 1)
			end
		end
		if twoCards and #twoCards > 0 then
			for i = 1, #twoCards do
				self:SplitCardsPattern(pattern, 2, 1)
			end
		end

		oneCards = pattern[1]
		if oneCards and #oneCards > 0 then
			found, beginIndex, endIndex = self:FindGreaterShunZiOfArray(oneCards, maxMainDigit, num)
		end
	end

	if found then
		for i = beginIndex, endIndex do
			if i == beginIndex or oneCards[i] ~= oneCards[i - 1] then
				self:InsertCardsWithDigit(resTbl, handCards, oneCards[i], 1) 
			end
		end
		return true, resTbl
	end

	return false
end

function CDouDiZhuBoard:HasShuangShunGreaterThan(handCards, maxMainDigit, num)
	if not handCards or #handCards < num * 2 then return false end
	local pattern = self:ParseCardsPattern(handCards)
	local threeCards = pattern[3]
	local twoCards = pattern[2]

	local resTbl = {}
	local found = false
	local beginIndex, endIndex
	if twoCards and #twoCards > 0 then
		found, beginIndex, endIndex = self:FindGreaterShunZiOfArray(twoCards, maxMainDigit, num)
	end

	if not found then
		-- 除炸弹全部拆分
		if threeCards and #threeCards > 0 then
			for i = 1, #threeCards do
				self:SplitCardsPattern(pattern, 3, 2)
			end
		end

		twoCards = pattern[2]
		if twoCards and #twoCards > 0 then
			found, beginIndex, endIndex = self:FindGreaterShunZiOfArray(twoCards, maxMainDigit, num)
		end
	end

	if found then
		for i = beginIndex, endIndex do
			if i == beginIndex or twoCards[i] ~= twoCards[i - 1] then
				self:InsertCardsWithDigit(resTbl, handCards, twoCards[i], 2) 
			end
		end
		return true, resTbl
	end

	return false
end

function CDouDiZhuBoard:Has3BuDaiGreaterThan(handCards, maxMainDigit)
	if not handCards or #handCards < 3 then return false end
	local pattern = self:ParseCardsPattern(handCards)
	local threeCards = pattern[3]

	local resTbl = {}
	if threeCards and #threeCards > 0 then
		local found, index = self:FindGreaterValueOfArray(threeCards, maxMainDigit)
		if found then
			self:InsertCardsWithDigit(resTbl, handCards, threeCards[index], 3)
			return true, resTbl
		end
	end

	return false
end

function CDouDiZhuBoard:Has3Dai1GreaterThan(handCards, maxMainDigit)
	if not handCards or #handCards < 4 then return false end
	local pattern = self:ParseCardsPattern(handCards)
	local threeCards = pattern[3]
	local twoCards = pattern[2]
	local oneCards = pattern[1]

	local resTbl = {}
	if threeCards and #threeCards > 0 then
		local found, index = self:FindGreaterValueOfArray(threeCards, maxMainDigit)
		if found then
			self:InsertCardsWithDigit(resTbl, handCards, threeCards[index], 3)
			table.remove(threeCards, index)
			if #threeCards <= 0 then
				pattern[3] = nil
				threeCards = nil
			end

			local digit = oneCards and oneCards[1]
			if not digit then
				digit = twoCards and twoCards[1]
			end
			if not digit then
				digit = threeCards and threeCards[1]
			end
			if digit then
				self:InsertCardsWithDigit(resTbl, handCards, digit, 1)
				return true, resTbl
			end
		end
	end

	return false
end

function CDouDiZhuBoard:Has3Dai2GreaterThan(handCards, maxMainDigit)
	if not handCards or #handCards < 5 then return false end
	local pattern = self:ParseCardsPattern(handCards)
	local threeCards = pattern[3]
	local twoCards = pattern[2]

	local resTbl = {}
	if threeCards and #threeCards > 0 then
		local found, index = self:FindGreaterValueOfArray(threeCards, maxMainDigit)
		if found then
			self:InsertCardsWithDigit(resTbl, handCards, threeCards[index], 3)
			table.remove(threeCards, index)
			if #threeCards <= 0 then
				pattern[3] = nil
				threeCards = nil
			end

			local digit = twoCards and twoCards[1]
			if not digit then
				digit = threeCards and threeCards[1]
			end
			if digit then
				self:InsertCardsWithDigit(resTbl, handCards, digit, 2)
				return true, resTbl
			end
		end
	end

	return false
end

function CDouDiZhuBoard:HasFeiJiGreaterThan(handCards, maxMainDigit, num)
	if not handCards or #handCards < num * 3 then return false end
	local pattern = self:ParseCardsPattern(handCards)
	local threeCards = pattern[3]

	local resTbl = {}
	if threeCards and #threeCards > 0 then
		local found, beginIndex, endIndex = self:FindGreaterShunZiOfArray(threeCards, maxMainDigit, num)
		if found then
			for i = beginIndex, endIndex do
				if i == beginIndex or threeCards[i] ~= threeCards[i - 1] then
					self:InsertCardsWithDigit(resTbl, handCards, threeCards[i], 3) 
				end
			end
			return true, resTbl
		end
	end

	return false
end

function CDouDiZhuBoard:HasFeiJiDai1GreaterThan(handCards, maxMainDigit, num)
	if not handCards or #handCards < num * 4 then return false end
	local pattern = self:ParseCardsPattern(handCards)
	local threeCards = pattern[3]
	local twoCards = pattern[2]
	local oneCards = pattern[1]

	local resTbl = {}
	if threeCards and #threeCards > 0 then
		local found, beginIndex, endIndex = self:FindGreaterShunZiOfArray(threeCards, maxMainDigit, num)
		if found then
			local rmList = {}
			for i = beginIndex, endIndex do
				if i == beginIndex or threeCards[i] ~= threeCards[i - 1] then
					self:InsertCardsWithDigit(resTbl, handCards, threeCards[i], 3) 
					table.insert(rmList, threeCards[i])
				end
			end
			for _, digit in ipairs(rmList) do
				local found1, index1 = self:FindValueOfArray(threeCards, digit)
				if found1 then
					table.remove(threeCards, index1)
					if #threeCards <= 0 then
						pattern[3] = nil
						threeCards = nil
					end
				end
			end

			local digitNum = 0
			if digitNum < num and oneCards and #oneCards > 0 then
				for i, digit in ipairs(oneCards) do
					if digitNum >= num then break end
					digitNum = digitNum + 1
					self:InsertCardsWithDigit(resTbl, handCards, digit, 1)
				end
			end
			if digitNum < num and twoCards and #twoCards > 0 then
				for i, digit in ipairs(twoCards) do
					if digitNum >= num then break end
					local cnt = 0
					for j = 1, 2 do
						if digitNum >= num then break end
						cnt = cnt + 1
						digitNum = digitNum + 1
					end
					self:InsertCardsWithDigit(resTbl, handCards, digit, cnt)
				end
			end
			if digitNum < num and threeCards and #threeCards > 0 then
				for i, digit in ipairs(threeCards) do
					if digitNum >= num then break end
					local cnt = 0
					for j = 1, 3 do
						if digitNum >= num then break end
						cnt = cnt + 1
						digitNum = digitNum + 1
					end
					self:InsertCardsWithDigit(resTbl, handCards, digit, cnt)
				end
			end

			if digitNum >= num then
				return true, resTbl
			end
		end
	end

	return false
end

function CDouDiZhuBoard:HasFeiJiDai2GreaterThan(handCards, maxMainDigit, num)
	if not handCards or #handCards < num * 5 then return false end
	local pattern = self:ParseCardsPattern(handCards)
	local threeCards = pattern[3]
	local twoCards = pattern[2]

	local resTbl = {}
	if threeCards and #threeCards > 0 then
		local found, beginIndex, endIndex = self:FindGreaterShunZiOfArray(threeCards, maxMainDigit, num)
		if found then
			local rmList = {}
			for i = beginIndex, endIndex do
				if i == beginIndex or threeCards[i] ~= threeCards[i - 1] then
					self:InsertCardsWithDigit(resTbl, handCards, threeCards[i], 3) 
					table.insert(rmList, threeCards[i])
				end
			end
			for _, digit in ipairs(rmList) do
				local found1, index1 = self:FindValueOfArray(threeCards, digit)
				if found1 then
					table.remove(threeCards, index1)
					if #threeCards <= 0 then
						pattern[3] = nil
						threeCards = nil
					end
				end
			end


			local pairNum = 0
			if pairNum < num and twoCards and #twoCards > 0 then
				for i, digit in ipairs(twoCards) do
					if pairNum >= num then break end
					pairNum = pairNum + 1
					self:InsertCardsWithDigit(resTbl, handCards, digit, 2)
				end
			end
			if pairNum < num and threeCards and #threeCards > 0 then
				for i, digit in ipairs(threeCards) do
					if pairNum >= num then break end
					pairNum = pairNum + 1
					self:InsertCardsWithDigit(resTbl, handCards, digit, 2)
				end
			end

			if pairNum >= num then
				return true, resTbl
			end
		end
	end

	return false
end

function CDouDiZhuBoard:GetMinOptimalCards(handCards)
	if not handCards or #handCards <= 0 then return false end

	local pattern = self:ParseCardsPattern(handCards)
	local fourCards = pattern[4]
	local threeCards = pattern[3]
	local twoCards = pattern[2]
	local oneCards = pattern[1]

	local resTbl = {}
	-- 火箭最后出
	local hasHuoJian
	local digitBlackJoker = self:GetCardDigit(EnumDouDiZhuCard.BlackJoker)
	local digitRedJoker = self:GetCardDigit(EnumDouDiZhuCard.RedJoker)
	if oneCards and #oneCards > 0 then
		local resBlackJoker, indexBlackJoker = self:FindValueOfArray(oneCards, digitBlackJoker)
		local resRedJoker, indexRedJoker = self:FindValueOfArray(oneCards, digitRedJoker)
		hasHuoJian = resBlackJoker and resRedJoker
		if not (hasHuoJian and (oneCards[1] == digitBlackJoker or oneCards[1] == digitRedJoker)) then
			self:InsertCardsWithDigit(resTbl, handCards, oneCards[1], 1)
			return true, resTbl
		end
	end
	if twoCards and #twoCards > 0 then
		self:InsertCardsWithDigit(resTbl, handCards, twoCards[1], 2)
		return true, resTbl
	end
	if threeCards and #threeCards > 0 then
		self:InsertCardsWithDigit(resTbl, handCards, threeCards[1], 3)
		return true, resTbl
	end
	if fourCards and #fourCards > 0 then
		self:InsertCardsWithDigit(resTbl, handCards, fourCards[1], 4)
		return true, resTbl
	end

	if hasHuoJian then
		local resBlackJoker, indexBlackJoker = self:FindValueOfArray(oneCards, digitBlackJoker)
		local resRedJoker, indexRedJoker = self:FindValueOfArray(oneCards, digitRedJoker)
		if resBlackJoker then
			self:InsertCardsWithDigit(resTbl, handCards, oneCards[indexBlackJoker], 1)
		end
		if resRedJoker then
			self:InsertCardsWithDigit(resTbl, handCards, oneCards[indexRedJoker], 1)
		end
		if #resTbl > 0 then
			return true, resTbl
		end
	end

	return false
end

function CDouDiZhuBoard:HasGreaterCardsThan(tbl, handCards)
	if not handCards then return false end

	local cardsType, maxMainDigit, num = self:ParseCardsType(tbl)

	local res = false
	local resTbl

	if cardsType == EnumDouDiZhuCardsType.eNone then
		return self:GetMinOptimalCards(handCards)
	end

	-- 火箭
	if cardsType == EnumDouDiZhuCardsType.eHuoJian then
		return false
	end
	-- 炸弹
	if cardsType == EnumDouDiZhuCardsType.eZhaDan then
		return self:HasZhaDanOrHuoJianGreaterThan(handCards, maxMainDigit)
	end

	-- 后续要么出一样牌型，要么出炸弹或火箭，上家有四张以上则直接炸弹
	if cardsType == EnumDouDiZhuCardsType.eDanZhang then
		-- 单张
		res, resTbl = self:HasDanZhangGreaterThan(handCards, maxMainDigit)
	elseif cardsType == EnumDouDiZhuCardsType.eDuiZi then
		-- 对子
		res, resTbl = self:HasDuiZiGreaterThan(handCards, maxMainDigit)
	elseif cardsType == EnumDouDiZhuCardsType.eShunZi then
		-- 顺子
		res, resTbl = self:HasShunZiGreaterThan(handCards, maxMainDigit, num)
	elseif cardsType == EnumDouDiZhuCardsType.eShuangShun then
		-- 双顺
		res, resTbl = self:HasShuangShunGreaterThan(handCards, maxMainDigit, num)
	elseif cardsType == EnumDouDiZhuCardsType.e3BuDai then
		-- 三不带
		res, resTbl = self:Has3BuDaiGreaterThan(handCards, maxMainDigit)
	elseif cardsType == EnumDouDiZhuCardsType.e3Dai1 then
		-- 三带一
		res, resTbl = self:Has3Dai1GreaterThan(handCards, maxMainDigit)
	elseif cardsType == EnumDouDiZhuCardsType.e3Dai2 then
		-- 三带二
		res, resTbl = self:Has3Dai2GreaterThan(handCards, maxMainDigit)
	elseif cardsType == EnumDouDiZhuCardsType.eFeiJi then
		-- 飞机
		res, resTbl = self:HasFeiJiGreaterThan(handCards, maxMainDigit, num)
	elseif cardsType == EnumDouDiZhuCardsType.eFeiJiDai1 then
		-- 飞机带1张
		res, resTbl = self:HasFeiJiDai1GreaterThan(handCards, maxMainDigit, num)
	elseif cardsType == EnumDouDiZhuCardsType.eFeiJiDai2 then
		-- 飞机带对
		res, resTbl = self:HasFeiJiDai2GreaterThan(handCards, maxMainDigit, num)
	end

	-- 没有一样牌型，或者上家为四张以上牌，炸弹或火箭
	if not res then
		res, resTbl = self:HasZhaDanOrHuoJianGreaterThan(handCards, 0)
	end

	return res, resTbl
end
