-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CDuoMaoMaoMgr = import "L10.Game.CDuoMaoMaoMgr"
local CFurnitureBigTypeItem = import "L10.UI.CFurnitureBigTypeItem"
local CHideAndSeekBigTypeWnd = import "L10.UI.CHideAndSeekBigTypeWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local NGUITools = import "NGUITools"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local Zhuangshiwu_Zhuangshiwu = import "L10.Game.Zhuangshiwu_Zhuangshiwu"
CHideAndSeekBigTypeWnd.m_Init_CS2LuaHook = function (this) 

    -- 设置初始化选项
    local initType = nil
    local initTypeId = 0

    local monsterId = CClientMainPlayer.Inst.AppearanceProp.ResourceId
    if CommonDefs.DictContains(CDuoMaoMaoMgr.Inst.MonsterId2Furniture, typeof(UInt32), monsterId) then
        local zhuangshiwuId = CommonDefs.DictGetValue(CDuoMaoMaoMgr.Inst.MonsterId2Furniture, typeof(UInt32), monsterId)
        local zhuangshiwu = Zhuangshiwu_Zhuangshiwu.GetData(zhuangshiwuId)
        initTypeId = zhuangshiwu.Type
    end

    CommonDefs.ListClear(this.bigTypeItemList)
    Extensions.RemoveAllChildren(this.table.transform)
    this.FurnitureBigTypeTemplate:SetActive(false)

    CommonDefs.EnumerableIterate(CDuoMaoMaoMgr.Inst.Furnitures.Keys, DelegateFactory.Action_object(function (___value) 
        local i = ___value
        local go = NGUITools.AddChild(this.table.gameObject, this.FurnitureBigTypeTemplate)
        go:SetActive(true)

        local template = CommonDefs.GetComponent_GameObject_Type(go, typeof(CFurnitureBigTypeItem))
        template:Init(i)
        UIEventListener.Get(template.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(template.gameObject).onClick, MakeDelegateFromCSFunction(this.OnClickTypeButton, VoidDelegate, this), true)

        CommonDefs.ListAdd(this.bigTypeItemList, typeof(CFurnitureBigTypeItem), template)

        if initType == nil then
            initType = template
        end
        if i == initTypeId then
            initType = template
        end
    end))


    this.table:Reposition()
    this.scrollView:ResetPosition()
    initType:SetSelected(true)
end
CHideAndSeekBigTypeWnd.m_OnClickTypeButton_CS2LuaHook = function (this, but) 
    local selectedTypeId = - 1
    do
        local i = 0
        while i < this.bigTypeItemList.Count do
            if but == this.bigTypeItemList[i].gameObject then
                this.bigTypeItemList[i]:SetSelected(true)
                selectedTypeId = this.bigTypeItemList[i]:GetTypeId()
            else
                this.bigTypeItemList[i]:SetSelected(false)
            end
            i = i + 1
        end
    end
    if selectedTypeId ~= - 1 then
        EventManager.BroadcastInternalForLua(EnumEventType.OnTransformTypeClicked, {selectedTypeId})
    end
end
