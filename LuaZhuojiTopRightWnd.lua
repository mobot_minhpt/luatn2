require("common/common_include")

local CTopAndRightTipWnd=import "L10.UI.CTopAndRightTipWnd"
local Time=import "UnityEngine.Time"
local CForcesMgr = import "L10.Game.CForcesMgr"
local UILabel = import "UILabel"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CUIFx = import "L10.UI.CUIFx"
local LiuYi_ZhuoJiSetting = import "L10.Game.LiuYi_ZhuoJiSetting"

CLuaZhuojiTopRightWnd=class()
RegistClassMember(CLuaZhuojiTopRightWnd,"m_ExpandButton")
RegistClassMember(CLuaZhuojiTopRightWnd,"m_ScoreLabels")
RegistClassMember(CLuaZhuojiTopRightWnd,"m_OwnerSprites")
RegistClassMember(CLuaZhuojiTopRightWnd,"m_UIFxs")

RegistClassMember(CLuaZhuojiTopRightWnd, "m_Tick")
RegistClassMember(CLuaZhuojiTopRightWnd, "m_AccuTime")


function CLuaZhuojiTopRightWnd:Init()
    self.m_ExpandButton=FindChild(self.transform,"ExpandButton").gameObject
	UIEventListener.Get(self.m_ExpandButton).onClick=LuaUtils.VoidDelegate(function(go)
        LuaUtils.SetLocalRotation(self.m_ExpandButton.transform,0,0,0)
        CTopAndRightTipWnd.showPackage = false
        CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
    end)

	self.m_ScoreLabels = {}
	self.m_OwnerSprites = {}
    self.m_UIFxs = {}
	local prefix = {"Red", "Yellow", "Blue", "Green"}
	for i = 1, 4 do
		local spriteName = prefix[i] .. "OwnerSprite"
		local spriteGO = FindChild(self.transform, spriteName).gameObject
		self.m_OwnerSprites[i] = spriteGO
		
		local labelName = prefix[i] .. "Label"
		local scoreGO = FindChild(self.transform, labelName).gameObject
        local scoreLabel = CommonDefs.GetComponent_GameObject_Type(scoreGO, typeof(UILabel))
		self.m_ScoreLabels[i] = scoreLabel

        local fxGO = FindChild(scoreGO.transform, "Fx").gameObject
        self.m_UIFxs[i] = CommonDefs.GetComponent_GameObject_Type(fxGO, typeof(CUIFx))
	end
    self:UpdateValue()
end

function CLuaZhuojiTopRightWnd:UpdateValue()
    local pid = 0
    if CClientMainPlayer.Inst then
        pid = CClientMainPlayer.Inst.Id
    end

    for i = 1, 4 do
        self.m_OwnerSprites[i]:SetActive(false)
        if pid ~= 0 then
            local force = CForcesMgr.Inst:GetPlayerForce(pid)
            if force == i then
                self.m_OwnerSprites[i]:SetActive(true)
            end
        end
        local res = CLuaLiuYiMgr.ZhuojiResult[i]
        local score = 0
        if res and res.score then
            score = res.score
        end
        self.m_ScoreLabels[i].text = score .. ""
    end
end

function CLuaZhuojiTopRightWnd:OnEnable()
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:AddListener("LiuyiZhuoji_UpdateZhuoJiForceScore", self, "OnLiuyiZhuoji_UpdateZhuoJiForceScore")
    
end
function CLuaZhuojiTopRightWnd:OnDisable()
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:RemoveListener("LiuyiZhuoji_UpdateZhuoJiForceScore", self, "OnLiuyiZhuoji_UpdateZhuoJiForceScore")

    if CUIManager.IsLoaded(CUIResources.TopAndRightTipWnd) then
        CUIManager.CloseUI(CUIResources.TopAndRightTipWnd)
    end
end

function CLuaZhuojiTopRightWnd:OnHideTopAndRightTipWnd()
    self.m_ExpandButton.transform.localEulerAngles = Vector3(0, 0, 180)
end

function CLuaZhuojiTopRightWnd:OnLiuyiZhuoji_UpdateZhuoJiForceScore(force, origScore, score)
    local myforce = 0
    if CClientMainPlayer.Inst then
        myforce = CForcesMgr.Inst:GetPlayerForce(CClientMainPlayer.Inst.Id)
    end
    if myforce > 0 and force == myforce then
        self:ShowScoreFx(origScore, score)
        self:RegisterUpdateValueTick(origScore, score)
    else
        self:UpdateValue()        
    end
end

function CLuaZhuojiTopRightWnd:ShowScoreFx(origScore, score)
    local pid = 0
    if CClientMainPlayer.Inst then
        pid = CClientMainPlayer.Inst.Id
    end
    if pid > 0 then
        local force = CForcesMgr.Inst:GetPlayerForce(pid)
        if force > 0 and force <= 4 then
            self.m_UIFxs[force]:DestroyFx()

            local setting = LiuYi_ZhuoJiSetting.GetData()
            local fxname = setting.ScoreBoardFx
            if origScore > score then
                fxname = setting.ScoreBoardSubtractFx
            end
            self.m_UIFxs[force]:LoadFx(fxname)
        end
    end
end 

function CLuaZhuojiTopRightWnd:RegisterUpdateValueTick(origScore, targetScore)
    self:DestroyTick()

    self.m_AccuTime = 0
    self.m_Tick = RegisterTick(function() self:OnTick(origScore, targetScore) end, 33) 
end


function CLuaZhuojiTopRightWnd:OnTick(origScore, targetScore)
    local totalTime = 2
    self.m_AccuTime = (self.m_AccuTime or 0) + Time.deltaTime
    self.m_AccuTime = math.min(self.m_AccuTime, totalTime)
    local curScore = self.m_AccuTime * (targetScore - origScore) / totalTime + origScore
    curScore = math.floor(curScore + 0.5)

    local pid = 0
    if CClientMainPlayer.Inst then
        pid = CClientMainPlayer.Inst.Id
    end
    if pid > 0 then
        local force = CForcesMgr.Inst:GetPlayerForce(pid)
        if force > 0 and force <= 4 then
            self.m_ScoreLabels[force].text = curScore .. ""
        end
    end
end

function CLuaZhuojiTopRightWnd:DestroyTick()
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
    end
end

function CLuaZhuojiTopRightWnd:OnDestroy()
    self:DestroyTick()
end

return CLuaZhuojiTopRightWnd
