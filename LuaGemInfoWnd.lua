local GameSetting_Common_Wapper = import "L10.Game.GameSetting_Common_Wapper"
local Color = import "UnityEngine.Color"
local CItemMgr = import "L10.Game.CItemMgr"
local CEquipmentProcessMgr = import "L10.Game.CEquipmentProcessMgr"
local CCommonLuaWnd=import "L10.UI.CCommonLuaWnd"

CLuaGemInfoWnd = class()
RegistClassMember(CLuaGemInfoWnd,"iconTexture")
RegistClassMember(CLuaGemInfoWnd,"nameLabel")
RegistClassMember(CLuaGemInfoWnd,"levelLabel")
RegistClassMember(CLuaGemInfoWnd,"descLabel")
RegistClassMember(CLuaGemInfoWnd,"background")
RegistClassMember(CLuaGemInfoWnd,"buttonArea")
RegistClassMember(CLuaGemInfoWnd,"upgradeBtn")
RegistClassMember(CLuaGemInfoWnd,"putoffBtn")
RegistClassMember(CLuaGemInfoWnd,"m_Wnd")
RegistClassMember(CLuaGemInfoWnd,"m_ShareMark")
CLuaGemInfoWnd.GemId = 0
CLuaGemInfoWnd.IsReference = false
function CLuaGemInfoWnd:Awake()
    self.iconTexture = self.transform:Find("Background/Header/GemCell/Icon"):GetComponent(typeof(CUITexture))
    self.nameLabel = self.transform:Find("Background/Header/ItemNameLabel"):GetComponent(typeof(UILabel))
    self.levelLabel = self.transform:Find("Background/Header/Level"):GetComponent(typeof(UILabel))
    self.descLabel = self.transform:Find("Background/Desc"):GetComponent(typeof(UILabel))
    self.background = self.transform:Find("Background"):GetComponent(typeof(UISprite))
    self.buttonArea = self.transform:Find("ButtonsBg").gameObject
    self.upgradeBtn = self.transform:Find("ButtonsBg/Upgrade").gameObject
    self.putoffBtn = self.transform:Find("ButtonsBg/Putoff").gameObject
    self.m_Wnd=self.gameObject:GetComponent(typeof(CCommonLuaWnd))

    self.m_ShareMark = FindChild(self.transform,"ShareMarkSprite").gameObject
    self.m_ShareMark:SetActive(CLuaGemInfoWnd.IsReference)

    UIEventListener.Get(self.upgradeBtn).onClick=DelegateFactory.VoidDelegate(function(go)
        self:OnUpgradeBtnClick(go)
    end)
    UIEventListener.Get(self.putoffBtn).onClick=DelegateFactory.VoidDelegate(function(go)
        self:OnPutoffBtnClick(go)
    end)
end

function CLuaGemInfoWnd:Init( )
    self.iconTexture.material = nil
    self.nameLabel.text = ""
    self.levelLabel.text = ""
    self.descLabel.text = ""
    self.buttonArea:SetActive(CLuaEquipMgr.showButtons)
    local jewel = Jewel_Jewel.GetData(CLuaGemInfoWnd.GemId)
    local item = CItemMgr.Inst:GetItemTemplate(CLuaGemInfoWnd.GemId)
    if jewel == nil or item == nil then
        return
    end

    self.iconTexture:LoadMaterial(item.Icon)
    self.nameLabel.text = jewel.JewelName
    self.nameLabel.color = GameSetting_Common_Wapper.Inst:GetColor(item.NameColor)
    self.levelLabel.text = SafeStringFormat3(LocalString.GetString("%d级"), jewel.JewelLevel)

    local info = CEquipmentProcessMgr.Inst.SelectEquipment
    local equip = CItemMgr.Inst:GetById(info.itemId)
    if equip == nil or not equip.IsEquip then
        return
    end

    local tpl = CItemMgr.Inst:GetItemTemplate(CLuaGemInfoWnd.GemId)
    local id = 0
    if equip.Equip.IsWeapon then
        id = Jewel_Jewel.GetData(tpl.ID).onWeapon
    elseif equip.Equip.IsArmor then
        id = Jewel_Jewel.GetData(tpl.ID).onArmor
    elseif equip.Equip.IsJewelry then
        id = Jewel_Jewel.GetData(tpl.ID).onJewelry
    elseif equip.Equip.IsFaBao then
        id = Jewel_Jewel.GetData(tpl.ID).onFaBao
    end

    if id > 0 then
        local word = Word_Word.GetData(id)
        self.descLabel.text = word.Description
        self.descLabel.color = Color.yellow
    end

    self.background.height = - math.floor(self.descLabel.transform.localPosition.y) + self.descLabel.height + 28
end


function CLuaGemInfoWnd:Update()
    if self.m_Wnd then
        self.m_Wnd:ClickThroughToClose()
    end
end

function CLuaGemInfoWnd:OnUpgradeBtnClick(go)
    if CLuaEquipMgr.UpgradeAction then
        CLuaEquipMgr.UpgradeAction()
    end
end
function CLuaGemInfoWnd:OnPutoffBtnClick(go)
    if CLuaEquipMgr.PutoffAction then
        CLuaEquipMgr.PutoffAction()
    end
end
function CLuaGemInfoWnd:OnDestroy()
    CLuaGemInfoWnd.GemId = 0
    CLuaGemInfoWnd.IsReference = false
end