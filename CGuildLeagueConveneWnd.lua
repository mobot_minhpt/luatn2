-- Auto Generated!!
-- local CGuildJobChangeModel = import "L10.UI.CGuildJobChangeModel"
local CGuildLeagueConveneItem = import "L10.UI.CGuildLeagueConveneItem"
local CGuildLeagueConveneSortMgr = import "L10.Game.CGuildLeagueConveneSortMgr"
local CGuildLeagueConveneWnd = import "L10.UI.CGuildLeagueConveneWnd"
local CGuildLeagueMgr = import "L10.Game.CGuildLeagueMgr"
local CGuildMemberBasicInfoRpc_Item = import "L10.Game.CGuildMemberBasicInfoRpc_Item"
local CGuildMgr = import "L10.Game.CGuildMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CSortButton = import "L10.UI.CSortButton"
local DelegateFactory = import "DelegateFactory"
local ESortType = import "L10.Game.CGuildLeagueConveneSortMgr+ESortType"
local Gac2Gas = import "L10.Game.Gac2Gas"
local GuildDefine = import "L10.Game.GuildDefine"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local QnButton = import "L10.UI.QnButton"
local UIEventListener = import "UIEventListener"
local UInt64 = import "System.UInt64"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CGuildLeagueConveneWnd.m_Init_CS2LuaHook = function (this) 
    if CGuildLeagueMgr.Inst.inGuildLeagueMainScene then
        this.titleLabel.text = LocalString.GetString("主战场征召")
        this.numLabel.text = System.String.Format(LocalString.GetString("主战场人数：{0}/{1}"), CGuildMgr.Inst.m_MainSceneNum, CGuildMgr.Inst.m_MainSceneMaxNum)
    elseif CGuildLeagueMgr.Inst.inGuildLeagueViceScene then
        this.titleLabel.text = LocalString.GetString("副战场征召")
        this.numLabel.text = System.String.Format(LocalString.GetString("副战场人数：{0}/{1}"), CGuildMgr.Inst.m_ViceSceneNum, CGuildMgr.Inst.m_ViceSceneMaxNum)
    end

    if CGuildMgr.Inst.m_GuildMemberBasicInfoRpc ~= nil then
        if this.mList ~= nil then
            CommonDefs.ListClear(this.mList)
        else
            this.mList = CreateFromClass(MakeGenericClass(List, CGuildMemberBasicInfoRpc_Item))
        end
        CommonDefs.DictIterate(CGuildMgr.Inst.m_GuildMemberBasicInfoRpc.Infos, DelegateFactory.Action_object_object(function (___key, ___value) 
            local item = {}
            item.Key = ___key
            item.Value = ___value
            CommonDefs.ListAdd(this.mList, typeof(CGuildMemberBasicInfoRpc_Item), item.Value)
        end))

        if CGuildMgr.Inst.m_GuildAidMemberBasicInfoRpc then
            CommonDefs.DictIterate(CGuildMgr.Inst.m_GuildAidMemberBasicInfoRpc.Infos, DelegateFactory.Action_object_object(function (___key, ___value) 
                CommonDefs.ListAdd(this.mList, typeof(CGuildMemberBasicInfoRpc_Item), ___value)
            end))
        end
    else
        this.mList = nil
    end

    this.changeRankBtn:SetActive(true)
    this.foreignAidBtn:SetActive(false)

    --默认排序
    this.sortRadioBox:ChangeTo(- 1, true)
    this:Sort(this.mList, CGuildLeagueConveneSortMgr.ESortType.Default)

    this.tableView.m_DataSource = this
    this.tableView:ReloadData(true, false)
    if this.mList.Count > 0 then
        this.tableView:SetSelectRow(0, true)
    else
        this.tableView:SetSelectRow(- 1, true)
    end
end
CGuildLeagueConveneWnd.m_Awake_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeButton).onClick = MakeDelegateFromCSFunction(this.OnClickCloseButton, VoidDelegate, this)

    UIEventListener.Get(this.refreshBtn).onClick = MakeDelegateFromCSFunction(this.OnClickRefreshButton, VoidDelegate, this)
    UIEventListener.Get(this.changeRankBtn).onClick = MakeDelegateFromCSFunction(this.OnClickChangeRankButton, VoidDelegate, this)
    UIEventListener.Get(this.conveneBtn).onClick = MakeDelegateFromCSFunction(this.OnClickConveneAllButton, VoidDelegate, this)
    UIEventListener.Get(this.foreignAidBtn).onClick = MakeDelegateFromCSFunction(this.OnClickForeignAidButton, VoidDelegate, this)

    this.sortRadioBox.OnSelect = MakeDelegateFromCSFunction(this.OnRadioBoxSelect, MakeGenericClass(Action2, QnButton, Int32), this)
end
CGuildLeagueConveneWnd.m_OnRadioBoxSelect_CS2LuaHook = function (this, btn, index) 
    local sortButton = TypeAs(btn, typeof(CSortButton))

    CGuildLeagueConveneSortMgr.m_GuildLeagueConveneSortFlags[index] = - CGuildLeagueConveneSortMgr.m_GuildLeagueConveneSortFlags[index]
    do
        local i = 0
        while i < CGuildLeagueConveneSortMgr.m_GuildLeagueConveneSortFlags.Length do
            if i ~= index then
                CGuildLeagueConveneSortMgr.m_GuildLeagueConveneSortFlags[i] = - 1
            end
            i = i + 1
        end
    end
    sortButton:SetSortTipStatus(CGuildLeagueConveneSortMgr.m_GuildLeagueConveneSortFlags[index] < 0)
    this:Sort(this.mList, CommonDefs.ConvertIntToEnum(typeof(ESortType), (index)))
    this.tableView:ReloadData(true, false)
    this.tableView:SetSelectRow(0, true)
end
CGuildLeagueConveneWnd.m_Sort_CS2LuaHook = function (this, list, type) 
    --Debug.Log("sort " + type.ToString());
    if list == nil then
        return
    end
    repeat
        local default = type
        if default == CGuildLeagueConveneSortMgr.ESortType.ClsName then
            CommonDefs.ListSort1(list, typeof(CGuildMemberBasicInfoRpc_Item), MakeDelegateFromCSFunction(CGuildLeagueConveneSortMgr.CompareClsName, MakeGenericClass(Comparison, CGuildMemberBasicInfoRpc_Item), CGuildLeagueConveneSortMgr))
            break
        elseif default == CGuildLeagueConveneSortMgr.ESortType.Level then
            CommonDefs.ListSort1(list, typeof(CGuildMemberBasicInfoRpc_Item), MakeDelegateFromCSFunction(CGuildLeagueConveneSortMgr.CompareLevel, MakeGenericClass(Comparison, CGuildMemberBasicInfoRpc_Item), CGuildLeagueConveneSortMgr))
            break
        elseif default == CGuildLeagueConveneSortMgr.ESortType.Title then
            CommonDefs.ListSort1(list, typeof(CGuildMemberBasicInfoRpc_Item), MakeDelegateFromCSFunction(CGuildLeagueConveneSortMgr.CompareTitle, MakeGenericClass(Comparison, CGuildMemberBasicInfoRpc_Item), CGuildLeagueConveneSortMgr))
            break
        elseif default == CGuildLeagueConveneSortMgr.ESortType.Xiulian then
            CommonDefs.ListSort1(list, typeof(CGuildMemberBasicInfoRpc_Item), MakeDelegateFromCSFunction(CGuildLeagueConveneSortMgr.CompareXiulian, MakeGenericClass(Comparison, CGuildMemberBasicInfoRpc_Item), CGuildLeagueConveneSortMgr))
            break
        elseif default == CGuildLeagueConveneSortMgr.ESortType.Xiuwei then
            CommonDefs.ListSort1(list, typeof(CGuildMemberBasicInfoRpc_Item), MakeDelegateFromCSFunction(CGuildLeagueConveneSortMgr.CompareXiuwei, MakeGenericClass(Comparison, CGuildMemberBasicInfoRpc_Item), CGuildLeagueConveneSortMgr))
            break
        elseif default == CGuildLeagueConveneSortMgr.ESortType.EquipScore then
            CommonDefs.ListSort1(list, typeof(CGuildMemberBasicInfoRpc_Item), MakeDelegateFromCSFunction(CGuildLeagueConveneSortMgr.CompareEquipScore, MakeGenericClass(Comparison, CGuildMemberBasicInfoRpc_Item), CGuildLeagueConveneSortMgr))
            break
        elseif default == CGuildLeagueConveneSortMgr.ESortType.ConveneState then
            CommonDefs.ListSort1(list, typeof(CGuildMemberBasicInfoRpc_Item), MakeDelegateFromCSFunction(CGuildLeagueConveneSortMgr.CompareConvene, MakeGenericClass(Comparison, CGuildMemberBasicInfoRpc_Item), CGuildLeagueConveneSortMgr))
            break
        else
            CommonDefs.ListSort1(list, typeof(CGuildMemberBasicInfoRpc_Item), MakeDelegateFromCSFunction(CGuildLeagueConveneSortMgr.CompareDefault, MakeGenericClass(Comparison, CGuildMemberBasicInfoRpc_Item), CGuildLeagueConveneSortMgr))
            break
        end
    until 1
end
CGuildLeagueConveneWnd.m_OnClickChangeRankButton_CS2LuaHook = function (this, go) 
    if this.mList == nil or (this.mList ~= nil and this.mList.Count == 0) or this.tableView.currentSelectRow == - 1 then
        g_MessageMgr:ShowMessage("NOT_SELECT_VALID_OBJECT")
        return
    end

    if this.mList ~= nil and this.mList.Count > 0 then
        if this.tableView.currentSelectRow >= 0 then
            local info = this.mList[this.tableView.currentSelectRow]
            CLuaGuildJobChangeWnd.ShowWnd(info.PlayerId, info.Name, GuildDefine.GetOfficeName(info.Title))
        end
    end
end
CGuildLeagueConveneWnd.m_OnClickConveneAllButton_CS2LuaHook = function (this, go) 
    if this.mList == nil or (this.mList ~= nil and this.mList.Count == 0) or this.tableView.currentSelectRow == - 1 then
        g_MessageMgr:ShowMessage("NOT_SELECT_VALID_OBJECT")
        return
    end

    CommonDefs.ListClear(CGuildLeagueConveneSortMgr.convenedPlayerIds)
    do
        local i = 0
        while i < this.mList.Count do
            if CGuildLeagueMgr.Inst:CanBeConvened(this.mList[i]) then
                CommonDefs.ListAdd(CGuildLeagueConveneSortMgr.convenedPlayerIds, typeof(UInt64), this.mList[i].PlayerId)
            end
            i = i + 1
        end
    end
    this.tableView:ReloadData(true, false)
    this.tableView:SetSelectRow(0, true)

    if CGuildLeagueMgr.Inst.inGuildLeagueMainScene then
        for i=0, CGuildLeagueConveneSortMgr.convenedPlayerIds.Count-1 do
            Gac2Gas.RequestCallUpOnePlayer(CGuildLeagueConveneSortMgr.convenedPlayerIds[i], EnumGuildLeagueCallUpType_lua.eMain)
        end
    elseif CGuildLeagueMgr.Inst.inGuildLeagueViceScene then
        for i=0, CGuildLeagueConveneSortMgr.convenedPlayerIds.Count-1 do
            Gac2Gas.RequestCallUpOnePlayer(CGuildLeagueConveneSortMgr.convenedPlayerIds[i], EnumGuildLeagueCallUpType_lua.eVice)
        end
    end
end
CGuildLeagueConveneWnd.m_OnClickForeignAidButton_CS2LuaHook = function (this, go) 
    if this.mList == nil or (this.mList ~= nil and this.mList.Count == 0) or this.tableView.currentSelectRow == - 1 then
        g_MessageMgr:ShowMessage("NOT_SELECT_VALID_OBJECT")
        return
    end

    local info = this.mList[this.tableView.currentSelectRow]
    local isForeignAidMember = CGuildLeagueMgr.Inst:IsForeignAidMember(info)
    if isForeignAidMember then
        --和策划沟通过只有跨服联赛涉及，所以这里写固定的上下文，后续如果有其他玩法复用，记得修改
        LuaGuildExternalAidMgr:ShowGuildExternalAidWnd(EnumExternalAidGamePlay.eGuildLeagueCross, info.PlayerId)
    end
end
CGuildLeagueConveneWnd.m_OnRequestOperationInGuildSucceed_CS2LuaHook = function (this, operationType, paramStr) 
    if (("AppointMember") == operationType) or (("DismissMember") == operationType) or (("NeiWuFuOperation") == operationType) then
        --变更职位之后 刷新一下
        Gac2Gas.RequestOpenGuildCallUpWnd()
    end
end
CGuildLeagueConveneWnd.m_NumberOfRows_CS2LuaHook = function (this, view) 
    if this.mList ~= nil then
        return this.mList.Count
    end
    return 0
end
CGuildLeagueConveneWnd.m_ItemAt_CS2LuaHook = function (this, view, row) 
    if this.mList ~= nil and row < this.mList.Count then
        local cmp = TypeAs(view:GetFromPool(0), typeof(CGuildLeagueConveneItem))
        if cmp ~= nil then
            cmp:Init(this.mList[row])
        end
        return cmp
    end
    return nil
end
CGuildLeagueConveneWnd.m_OnSelectAtRow_CS2LuaHook = function (this, row) 
    if this.mList ~= nil and row < this.mList.Count then
        local info = this.mList[row]
        local isForeignAidMember = CGuildLeagueMgr.Inst:IsForeignAidMember(info)
        this.changeRankBtn:SetActive(not isForeignAidMember)
        this.foreignAidBtn:SetActive(isForeignAidMember)
    end
end