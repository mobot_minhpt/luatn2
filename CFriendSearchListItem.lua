-- Auto Generated!!
local CExpressionMgr = import "L10.Game.CExpressionMgr"
local CFriendSearchListItem = import "L10.UI.CFriendSearchListItem"
local CommonDefs = import "L10.Game.CommonDefs"
local CUICommonDef = import "L10.UI.CUICommonDef"
local L10 = import "L10"
local LocalString = import "LocalString"
local NGUIText = import "NGUIText"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CFriendSearchListItem.m_Init_CS2LuaHook = function (this, online, playerId, playerName, portraitName, expressionTxt, profileFrame, level, isMyFriend, isInXianShenStatus) 
    this.PlayerId = playerId
    this.PlayerName = playerName
    this.nameLabel.text = playerName
    this.portrait:LoadNPCPortrait(portraitName, not online)
    if this.profileFrame ~= nil and CExpressionMgr.EnableProfileFrame then
        local framePath = CUICommonDef.GetProfileFramePath(profileFrame)
        this.profileFrame:LoadMaterial(framePath)
    end
    this.expressionTxt:LoadMaterial(CUICommonDef.GetExpressionTxtPath(expressionTxt))
    local default
    if isInXianShenStatus then
        default = L10.Game.Constants.ColorOfFeiSheng
    else
        default = NGUIText.EncodeColor24(this.levelLabel.color)
    end
    this.levelLabel.text = System.String.Format(LocalString.GetString("[c][{0}]{1}[-][/c]"), default, level)
    this.IsMyFriend = isMyFriend
end
CFriendSearchListItem.m_Start_CS2LuaHook = function (this) 
    UIEventListener.Get(this.portrait.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.portrait.gameObject).onClick, MakeDelegateFromCSFunction(this.OnPortraitClick, VoidDelegate, this), true)
    UIEventListener.Get(this.addBtnSprite.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.addBtnSprite.gameObject).onClick, MakeDelegateFromCSFunction(this.OnAddFriendButtonClick, VoidDelegate, this), true)
end
