local Title_Title = import "L10.Game.Title_Title"
local StringBuilder = import "System.Text.StringBuilder"
local LocalString = import "LocalString"
local ETickType = import "L10.Engine.ETickType"
local DelegateFactory = import "DelegateFactory"
local CTickMgr = import "L10.Engine.CTickMgr"
local CNewAchievementGetWnd = import "L10.UI.CNewAchievementGetWnd"
local Achievement_Group = import "L10.Game.Achievement_Group"
local Achievement_Achievement = import "L10.Game.Achievement_Achievement"
local CAchievementMgr=import "L10.Game.CAchievementMgr"
local CUIResources = import "L10.UI.CUIResources"
local CCommonUITween = import "L10.UI.CCommonUITween"

CLuaNewAchievementGetWnd = class()

RegistClassMember(CLuaNewAchievementGetWnd,"achievementName")
RegistClassMember(CLuaNewAchievementGetWnd,"awardTable")
RegistClassMember(CLuaNewAchievementGetWnd,"awardLabel")
RegistClassMember(CLuaNewAchievementGetWnd,"awardTemplate")
RegistClassMember(CLuaNewAchievementGetWnd,"achievementGo")
RegistClassMember(CLuaNewAchievementGetWnd,"achievementDesc")
RegistClassMember(CLuaNewAchievementGetWnd,"cancel")
RegistClassMember(CLuaNewAchievementGetWnd,"closeBtn")

function CLuaNewAchievementGetWnd:Awake()
    self.achievementName = self.transform:Find("Anchor/Offset/Background/Label"):GetComponent(typeof(UILabel))
    self.awardTable = self.transform:Find("Anchor/Offset/Table"):GetComponent(typeof(UITable))
    self.awardLabel = self.transform:Find("Anchor/Offset/Label").gameObject
    self.awardLabel:SetActive(false)
    self.awardTemplate = self.transform:Find("Anchor/Offset/AwardTemplate").gameObject
    self.awardTemplate:SetActive(false)
    self.achievementGo = self.transform:Find("Anchor/Offset/Background/Tip").gameObject
    self.achievementDesc = self.transform:Find("Anchor/Offset/Background/DescriptionLabel"):GetComponent(typeof(UILabel))
    self.closeBtn = self.transform:Find("Anchor/Offset/Background/CloseButton").gameObject
end

function CLuaNewAchievementGetWnd:Init( )
    self.achievementName.text = ""
    self.achievementDesc.text = ""
    Extensions.RemoveAllChildren(self.awardTable.transform)

    local achievement = Achievement_Achievement.GetData(CNewAchievementGetWnd.AchievementID)
    if CNewAchievementGetWnd.isNewAchievement then
        UnRegisterTick(self.cancel)
        self.cancel = CTickMgr.Register(DelegateFactory.Action(function () 
            self:OnCloseBtnClick()
        end), 8000, ETickType.Once)
    end
    if achievement ~= nil then
        self.achievementName.text = achievement.Name

        --label类型需要clamp content， maxlines = 1
        self.achievementDesc.text = achievement.RequirementDisplay
        local fit = true
        local outerText = ""
        fit, outerText = self.achievementDesc:Wrap(achievement.RequirementDisplay)
        if not fit then
            self.achievementDesc.text = CommonDefs.StringSubstring2(outerText, 0, CommonDefs.StringLength(outerText) - 1) .. "..."
        end

        NGUITools.AddChild(self.awardTable.gameObject, self.awardLabel):SetActive(true)
        if achievement.ExpReward ~= nil and achievement.ExpReward > 0 then
            local go = NGUITools.AddChild(self.awardTable.gameObject, self.awardTemplate)
            go:SetActive(true)
            go.transform:Find("Label"):GetComponent(typeof(UILabel)).text = achievement.ExpReward
            go.transform:Find("Sprite"):GetComponent(typeof(UISprite)).spriteName = "common_player_exp"
        end
        if achievement.SilverReward ~= nil and achievement.SilverReward > 0 then
            local go = NGUITools.AddChild(self.awardTable.gameObject, self.awardTemplate)
            go:SetActive(true)
            go.transform:Find("Label"):GetComponent(typeof(UILabel)).text = achievement.SilverReward
            go.transform:Find("Sprite"):GetComponent(typeof(UISprite)).spriteName = "packagewnd_yinliang"
        end
        if achievement.FreeSilverReward ~= nil and achievement.FreeSilverReward > 0 then
            local go = NGUITools.AddChild(self.awardTable.gameObject, self.awardTemplate)
            go:SetActive(true)
            go.transform:Find("Label"):GetComponent(typeof(UILabel)).text = achievement.FreeSilverReward
            go.transform:Find("Sprite"):GetComponent(typeof(UISprite)).spriteName = "packagewnd_yinpiao"
        end
        if achievement.TitleReward ~= nil and achievement.TitleReward > 0 then
            local title = Title_Title.GetData(achievement.TitleReward)
            local go = NGUITools.AddChild(self.awardTable.gameObject, self.awardTemplate)
            go:SetActive(true)
            go.transform:Find("Label"):GetComponent(typeof(UILabel)).text = title.Name
            go.transform:Find("Sprite"):GetComponent(typeof(UISprite)).spriteName = "packagewnd_chenghao"
        end
        if achievement.YuanBaoReward ~= nil and achievement.YuanBaoReward > 0 then
            local go = NGUITools.AddChild(self.awardTable.gameObject, self.awardTemplate)
            go:SetActive(true)
            go.transform:Find("Label"):GetComponent(typeof(UILabel)).text = achievement.YuanBaoReward
            go.transform:Find("Sprite"):GetComponent(typeof(UISprite)).spriteName = "packagewnd_yuanbao"
        end
        if achievement.YueLiReward ~= nil and achievement.YueLiReward > 0 then
            local go = NGUITools.AddChild(self.awardTable.gameObject, self.awardTemplate)
            go:SetActive(true)
            go.transform:Find("Label"):GetComponent(typeof(UILabel)).text = achievement.YueLiReward
            go.transform:Find("Sprite"):GetComponent(typeof(UISprite)).spriteName = "talismanfxexchangewnd_dou"
        end
        self.awardTable:Reposition()
    end
end

function CLuaNewAchievementGetWnd:OnEnable()
    UIEventListener.Get(self.achievementGo).onClick = DelegateFactory.VoidDelegate(function(_go)
        self:OnAchievementClick()
    end)
    UIEventListener.Get(self.closeBtn).onClick = DelegateFactory.VoidDelegate(function(_go)
        self:OnCloseBtnClick()
    end)
end

function CLuaNewAchievementGetWnd:OnDestroy()
    UnRegisterTick(self.cancel)
    self.cancel = nil
end

function CLuaNewAchievementGetWnd.CollectZhuXianAchievementGroups()
    local tabName = LocalString.GetString("妖鬼狐")
    local subTabName = LocalString.GetString("妖鬼狐")
    local group_list = {}
    if CommonDefs.DictContains(CAchievementMgr.Inst.m_TabNameList, typeof(String), tabName) then
        local tabInfo = CommonDefs.DictGetValue(CAchievementMgr.Inst.m_TabNameList, typeof(String), tabName)
        if CommonDefs.DictContains(tabInfo.subTabName, typeof(String), subTabName) then
            local subTabInfo = CommonDefs.DictGetValue(tabInfo.subTabName, typeof(String), subTabName)
            local groupId = subTabInfo.groupId

            CommonDefs.ListIterate(groupId,DelegateFactory.Action_object(function(k)
                table.insert( group_list,k )
            end))
        else
            local groupId = tabInfo.groupId
            CommonDefs.ListIterate(groupId,DelegateFactory.Action_object(function(k)
                table.insert( group_list,k )
            end))
        end
    end
    return group_list
end

function CLuaNewAchievementGetWnd:OnAchievementClick() 
    local a = Achievement_Achievement.GetData(CNewAchievementGetWnd.AchievementID)
    if a ~= nil then
        local group = Achievement_Group.GetData(a.Group)
        if group == nil then
            return
        end
        local bFound = false
        local ZhuXianGroupTable = self.CollectZhuXianAchievementGroups()
        for k, v in ipairs(ZhuXianGroupTable) do
            if(v == a.Group)then
                CLuaZhuXianAchievementWnd.AppointedGroupId = a.Group
                CUIManager.ShowUI(CLuaUIResources.ZhuXianAchievementWnd)
                bFound = true
                break
            end
        end
        if not bFound then
            LuaAchievementMgr:ShowDetailWndByAchievementId(CNewAchievementGetWnd.AchievementID)
        end
    end
end

function CLuaNewAchievementGetWnd:OnCloseBtnClick()
    self.transform:Find("Anchor"):GetComponent(typeof(CCommonUITween)):PlayDisapperAnimation(DelegateFactory.Action(function()
        CUIManager.CloseUI(CUIResources.NewAchievementGetWnd)
    end))
end