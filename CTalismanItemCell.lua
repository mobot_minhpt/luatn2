-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CItemMgr = import "L10.Game.CItemMgr"
local CTalismanItemCell = import "L10.UI.CTalismanItemCell"
local CUICommonDef = import "L10.UI.CUICommonDef"
local EnumQualityType = import "L10.Game.EnumQualityType"
CTalismanItemCell.m_Init_CS2LuaHook = function (this, itemId) 

    this.item = CItemMgr.Inst:GetById(itemId)
    this.iconTexture.material = nil
    this.bindSprite.enabled = false
    this.disableSprite.enabled = false
    this.qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(EnumQualityType.White)
    this.selectSprite.enabled = false

    if this.item ~= nil and CClientMainPlayer.Inst ~= nil and this.item.IsEquip then
        this.iconTexture:LoadMaterial(this.item.Icon)
        this.bindSprite.enabled = true
        this.bindSprite.spriteName = this.item.BindOrEquipCornerMark
        this.disableSprite.enabled = not this.item.MainPlayerIsFit
        this.qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(this.item.Equip.QualityType)
    end
end
