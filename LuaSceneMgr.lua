local CPostEffect = import "L10.Engine.CPostEffect"
local Tags=import "L10.Game.Tags"
local MeshRenderer=import "UnityEngine.MeshRenderer"
local CRenderScene=import "L10.Engine.Scene.CRenderScene"
local CEffectMgr = import "L10.Game.CEffectMgr"
local EnumWarnFXType =import "L10.Game.EnumWarnFXType"
local CScene=import "L10.Game.CScene"
local Utility = import "L10.Engine.Utility"
local CPos = import "L10.Engine.CPos"
local Animator = import "UnityEngine.Animator"
local Animation = import "UnityEngine.Animation"
local PlayMode = import "UnityEngine.PlayMode"
local ShaderEx = import "ShaderEx"
local Main = import "L10.Engine.Main"
local CWater = import "L10.Engine.CWater"
local CMainCamera = import "L10.Engine.CMainCamera"
local CWaveEquation = import "L10.Engine.CWaveEquation"
local CoreScene=import "L10.Engine.Scene.CoreScene"
local CClientFurnitureMgr=import "L10.Game.CClientFurnitureMgr"
local CObjectFX = import "L10.Game.CObjectFX"
local CWeatherMgr=import "L10.Game.CWeatherMgr"
local EWeather=import "L10.Game.EWeather"
local CPostEffectMgr=import "L10.Engine.CPostEffectMgr"
local CPostProcessingMgr = import "L10.Engine.PostProcessing.CPostProcessingMgr"
local PlayerSettings=import "L10.Game.PlayerSettings"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local CSharpResourceLoader= import "L10.Game.CSharpResourceLoader"
local EnumHeatWaveMode = import "L10.Engine.PostProcessing.EnumHeatWaveMode"
local AMPlayer = import "L10.Game.CutScene.AMPlayer"
local CWallAndFloorPaperChanger = import "L10.Game.CWallAndFloorPaperChanger"

CLuaSceneMgr = {}
CLuaSceneMgr.m_Tick = nil
CLuaSceneMgr.m_DangPuSceneTemplateId = nil
CLuaSceneMgr.enableWaveEquationWater = false
CLuaSceneMgr.m_EnableJiaYuanNodeHiding = {} -- cached
CLuaSceneMgr.m_IsEnabledOlympicWinterGamesWater = false --冬奥会期间金沙镜开启冰面
if Main.Inst and Main.Inst.EngineVersion < ShaderEx.s_MinVersion and Main.Inst.EngineVersion ~= - 1 then
    CLuaSceneMgr.enableWaveEquationWater = false
end

function CLuaSceneMgr.EnterJinShaJing()
    CLuaSceneMgr:Leave()
    CLuaSceneMgr.m_Tick = RegisterTick(function()
        CLuaSceneMgr.OnJinShaJingMainPlayerMoveStepped()
    end,1000)
    CLuaSceneMgr:EnterOlympicWinterGamesJinShaJing()
end

function CLuaSceneMgr.EnterSect()
    CLuaSceneMgr:PlaySceneObjectAnimation("OtherObjects/Fx/liuyuxinmo/liuyuxinmo", "liuyuxinmo_None")
end

function CLuaSceneMgr:EnterOlympicWinterGamesJinShaJing()
    if not self.m_IsEnabledOlympicWinterGamesWater then return end
    if UNITY_EDITOR then return end
    if CScene.MainScene then
        local sceneTemplateId=CScene.MainScene.SceneTemplateId
        if sceneTemplateId ~= 16102152 then return end
        local isNight = CScene.MainScene.IsNightScene
        if isNight then return end
        CSharpResourceLoader.Inst:LoadMaterial("assets/res/water/jinshajing/ice_01.mat", DelegateFactory.Action_Material(function(mat)
            local obj = CRenderScene.Inst.transform:Find("OtherObjects/Water/Shui")
            if obj then
                obj.gameObject:GetComponent(typeof(MeshRenderer)).sharedMaterial = mat
            end
        end))
    end
end

function CLuaSceneMgr:Leave()
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
    end
    self:LeaveDangPu()
    LuaPoolMgr.LeaveJiaYuan()
    CLuaHouseFishMgr:LeaveJiaYuan()
    LuaSceneInteractiveMgr:LeaveScene()

    if CPostProcessingMgr.s_NewPostProcessingOpen then
        local postEffectCtrl = CPostProcessingMgr.Instance.MainController
        postEffectCtrl:SetHeatWaveMode(EnumHeatWaveMode.UnderWater ,false)
    else
        CPostEffectMgr.Instance:SetUnderWaterDistortEffect(false)
    end

    g_ScriptEvent:RemoveListener("MainCameraAngleChanged",self,"OnMainCameraAngleChanged")
    g_ScriptEvent:RemoveListener("UpdateHouseCeilingVisibility",self,"OnUpdateHouseCeilingVisibility")
    g_ScriptEvent:RemoveListener("MainPlayerMoveStepped",self,"OnMainPlayerMoveStepped")
    g_ScriptEvent:RemoveListener("OnSyncDayAndNightChangeInfo",self,"OnSyncDayAndNightChangeInfo")
    CLuaSceneMgr.m_SceneName = nil
    CLuaSceneMgr.m_IsNewJiayuansn = nil
    CLuaSceneMgr.m_CameraBox = nil
end

CLuaSceneMgr.m_DangPuFengLingPos = CPos(66,42)
CLuaSceneMgr.m_DangPuDoorPos = CPos(67,35)
CLuaSceneMgr.m_SceneName = nil--场景名字cache
CLuaSceneMgr.m_IsNewJiayuansn = nil--是否是新的家园室内场景
CLuaSceneMgr.m_CameraBox = nil

local CClientHouseMgr=import "L10.Game.CClientHouseMgr"
function CLuaSceneMgr:EnterScene()
    --清空数据
    LuaPoolMgr.SetPoolDataDirty()
    CWallAndFloorPaperChanger.OnClearPaper()
    
    if CRenderScene.Inst then
        CRenderScene.Inst:InitScenesItem()
    end
    CLuaSceneMgr.m_SceneName = CoreScene.MainCoreScene.SceneName
    local sceneName = CLuaSceneMgr.m_SceneName
    CLuaSceneMgr.m_IsNewJiayuansn = StringStartWith(sceneName,"jiayuanptsn") or StringStartWith(sceneName,"jiayuanmysn")

    self:EnterDangPu()
    self:EnterQingQiu()
    LuaChristmas2021Mgr.OnEnterXingYuPlay()
    
    CLuaSceneMgr.m_CameraBox = nil
    if CLuaSceneMgr.m_IsNewJiayuansn then
        g_ScriptEvent:AddListener("MainCameraAngleChanged",self,"OnMainCameraAngleChanged")
        g_ScriptEvent:AddListener("UpdateHouseCeilingVisibility",self,"OnUpdateHouseCeilingVisibility")
        g_ScriptEvent:AddListener("MainPlayerMoveStepped",self,"OnMainPlayerMoveStepped")
        CLuaHouseMgr.GetDefaultWallFloorPaper()
        CLuaHouseMgr.SetDesignDataRoomPapers()
        CWallAndFloorPaperChanger.OnClearPaper()
        CLuaHouseMgr.SetWallPaper()
        CLuaHouseMgr.SetFloorPaper()
        CLuaSceneMgr.InitCameraBox()
        CLuaSceneMgr.OnMainCameraAngleChangedInJiaYuanShiNei( )
    end
    g_ScriptEvent:AddListener("OnSyncDayAndNightChangeInfo",self,"OnSyncDayAndNightChangeInfo")
    LuaPoolMgr.EnterJiaYuan()
    if CRenderScene.IsDisableCombinedGenreateTerrain(CLuaSceneMgr.m_SceneName) then
        LuaPoolMgr.GenTerrainFromPoolMark()
    end
    local isHaidi = sceneName == "haidi"
    if sceneName == "jiayuan02new" or sceneName == "jiayuan01new" or isHaidi then
        CLuaHouseFishMgr:Init(sceneName)
    end

    if CPostProcessingMgr.s_NewPostProcessingOpen then
        local postEffectCtrl = CPostProcessingMgr.Instance.MainController
        postEffectCtrl:SetHeatWaveMode(EnumHeatWaveMode.UnderWater ,isHaidi)
    else
        CPostEffectMgr.Instance:SetUnderWaterDistortEffect(isHaidi)
    end
    
    if CClientHouseMgr.Inst:IsPlayerInYard() then
        local waterNode = CRenderScene.Inst.transform:Find("OtherObjects/Water/Water_in")
        if waterNode then
            waterNode.tag = Tags.Ground
        end
    end

    CObjectFX.s_UnimportantEffectCount = 0
end


--缓存一下包围盒信息
function CLuaSceneMgr.InitCameraBox()
    CLuaSceneMgr.m_CameraBox = {}
    local boxnode = CRenderScene.Inst.transform:Find("OtherObjects/CameraBox")
    if boxnode then
        local mrs = boxnode.gameObject:GetComponentsInChildren(typeof(MeshRenderer),true)
        for i=1,mrs.Length do
            local mr = mrs[i-1]
            local center = mr.bounds.center
            local extents = mr.bounds.extents
            local b = {
                center = {center.x,center.y,center.z},
                extents = {extents.x,extents.y,extents.z},
            }
            table.insert( CLuaSceneMgr.m_CameraBox,b )
        end
    end
end
function CLuaSceneMgr:OnMainPlayerMoveStepped()
    CLuaSceneMgr.OnMainCameraAngleChangedInJiaYuanShiNei( )
end
function CLuaSceneMgr:OnMainCameraAngleChanged()
    CLuaSceneMgr.OnMainCameraAngleChangedInJiaYuanShiNei( )
end
function CLuaSceneMgr:OnUpdateHouseCeilingVisibility()
    CLuaSceneMgr.OnMainCameraAngleChangedInJiaYuanShiNei( )
end
function CLuaSceneMgr:OnSyncDayAndNightChangeInfo()
    self:EnterOlympicWinterGamesJinShaJing()
end

function CLuaSceneMgr.OnEnterJiaYuanShiNei()
    CLuaSceneMgr.OnMainCameraAngleChangedInJiaYuanShiNei()
end
function CLuaSceneMgr.OnMainCameraAngleChangedInJiaYuanShiNei( )
    if not CoreScene.MainCoreScene then return end
    if not CScene.MainScene then return end
    if not CLuaSceneMgr.m_CameraBox then return end
    if AMPlayer.Inst.IsPlaying then
        return
    end 

    local sceneTemplateId = CScene.MainScene.SceneTemplateId
    if sceneTemplateId>0 and CLuaSceneMgr.m_EnableJiaYuanNodeHiding[sceneTemplateId] == nil then 
        CLuaSceneMgr.m_EnableJiaYuanNodeHiding[sceneTemplateId] = PublicMap_PublicMap.GetData(sceneTemplateId).DisableJiaYuanNodeHiding == 0
    end
    if not CLuaSceneMgr.m_EnableJiaYuanNodeHiding[sceneTemplateId] then return end

    local sceneName = CoreScene.MainCoreScene.SceneName
    if CLuaSceneMgr.m_IsNewJiayuansn then
        local mode = CClientFurnitureMgr.Inst.FurnishMode
        if CRenderScene.Inst then
            local node = CRenderScene.Inst.transform:Find("Models/wuding")
            if not node then
                node = CRenderScene.Inst.transform:Find("Models/"..sceneName.."_wuding")
            end
            local node2 = CRenderScene.Inst.transform:Find("OtherObjects/PutHide")

            if node or node2 then
                local pos = CMainCamera.Inst.transform.position
                local function contains(b,p)
                    local min = {
                        x = b.center[1]-b.extents[1],
                        y = b.center[2]-b.extents[2],
                        z = b.center[3]-b.extents[3],
                    }
                    local max = {
                        x = b.center[1]+b.extents[1],
                        y = b.center[2]+b.extents[2],
                        z = b.center[3]+b.extents[3],
                    }
                    if p.x < min.x or p.y < min.y or p.z < min.z or p.x > max.x or p.y > max.y or p.z > max.z then
                        return false
                    end
                    return true
                end

                local inbounds = false
                for i,b in ipairs(CLuaSceneMgr.m_CameraBox) do
                    if contains(b,pos) then
                        inbounds = true
                        break
                    end
                end

                if inbounds then
                    if node then node.gameObject:SetActive(true) end
                    if node2 then node2.gameObject:SetActive(true) end
                else
                    if node then node.gameObject:SetActive(false) end
                    if node2 then node2.gameObject:SetActive(false) end
                end
            end
        end
    end
end

function CLuaSceneMgr:LeaveDangPu()
    if self:IsEnterDangPu() then
        g_ScriptEvent:RemoveListener("PlayerShakeDevice", self, "OnShakingInDangPu")
    end
    self.m_DangPuSceneTemplateId = nil
end

function CLuaSceneMgr:IsEnterDangPu()
    return self.m_DangPuSceneTemplateId ~= nil
end

function CLuaSceneMgr:EnterDangPu()
    if not CScene.MainScene or not self:IsDangPu() then return end
    self:LeaveDangPu()
    self.m_DangPuSceneTemplateId = CScene.MainScene.SceneTemplateId
    g_ScriptEvent:AddListener("PlayerShakeDevice", self, "OnShakingInDangPu")
    self.m_Tick = RegisterTick(function()
        self:OnDangPuMainPlayerMoveStepped()
    end,1000)
    local obj = CRenderScene.Inst.transform:Find("OtherObjects/Fx/dangpu_fengling_jinfen")
    if obj then
        obj.gameObject:SetActive(false)
    end
end

function CLuaSceneMgr:EnterQingQiu()
    if not self.enableWaveEquationWater or not CScene.MainScene then return end
    if 16000026 ~= CScene.MainScene.SceneTemplateId then return end
    local water = self:GetSceneComponent("OtherObjects/Water/Water",typeof(CWater))
    if not water then return end
    if CMainCamera.Main and CMainCamera.Inst:GetCameraEnableStatus() then
        CWaveEquation.InitWaveEquation(water.gameObject, 512, 0.9, 0.894, 4, Vector3(-3.6, 0, -3.8),'assets/res/scenes/models/qingqiu/materials/waveequation.mat')
    end
end

function CLuaSceneMgr:IsDangPu()
    if not CScene.MainScene then return false end
    return CScene.MainScene.SceneName == "dangpu"
end

function CLuaSceneMgr:GetSceneName()
    if not CScene.MainScene then return nil end
    return CScene.MainScene.SceneName
end

function CLuaSceneMgr:OnShakingInDangPu()
    local player = CClientMainPlayer.Inst
    local pos = Utility.PixelPos2GridPos(player.Pos)

    if (CPos.Distance_Grid(self.m_DangPuFengLingPos, pos)) < 16 then
        SoundManager.Inst:PlayOneShot(ZhuJueJuQing_Setting.GetData().WindChimesSound_ShakeDivece, Vector3.zero, nil, 0, -1)
        self:ShakeWindChimes(true)
    end
end

function CLuaSceneMgr:OnDangPuMainPlayerMoveStepped()
    local player = CClientMainPlayer.Inst
    if not player then return end
    local pos = Utility.PixelPos2GridPos(player.Pos)
    if (CPos.Distance_Grid(self.m_DangPuFengLingPos, pos)) < ZhuJueJuQing_Setting.GetData().WindChimes_Distance then
        SoundManager.Inst:PlayOneShot(ZhuJueJuQing_Setting.GetData().WindChimesSound_ShakeDivece, Vector3.zero, nil, 0, -1)
        self:ShakeWindChimes(false)
    end

    local animator = self:GetSceneComponent("Models/Models_NOClip/jz_dangpu_012",typeof(Animator))
    if animator then
        animator:Play(CPos.Distance_Grid(self.m_DangPuDoorPos, pos) < 9 and "jz_dangpu_012_kaimen_donghua" or "jz_dangpu_012_kaimen_donghua02")
    end
end

--当铺风铃晃动
function CLuaSceneMgr:ShakeWindChimes(isEffect)
    if not self:IsDangPu() then return end
    local animator = self:GetSceneComponent("Models/Others/dj_jiaju_025_002",typeof(Animator))
    if not animator then return end
    local info = animator:GetCurrentAnimatorStateInfo(0)
    if info.normalizedTime >= 1.0 then
        animator:Play("dj_jiaju_025_002_fengling_donghua",-1,0)
    end
    if isEffect then
        local obj = CRenderScene.Inst.transform:Find("OtherObjects/Fx/dangpu_fengling_jinfen")
        if obj then
            obj.gameObject:SetActive(false)
            obj.gameObject:SetActive(true)
        end
    end
end

-- 88801367 金沙-白木林蝴蝶特效
-- 88801368 金沙-乌木林萤火特效
function CLuaSceneMgr.OnJinShaJingMainPlayerMoveStepped()
    local player = CClientMainPlayer.Inst
    local pos = player.Pos
    local roadInfo = CoreScene.MainCoreScene:GetRoadInfo(pos.x/64,pos.y/64)
    local iswumulin = bit.band(roadInfo, bit.lshift(1, ERoadInfo.eRI_UNUSED6))
    local atNight = CRenderScene.Inst and CRenderScene.Inst.AtNight
    if iswumulin>0 and atNight then
        if not player.RO:ContainFx("JinShaJingFx1") then
            local fx = CEffectMgr.Inst:AddObjectFX(88801368,player.RO,0,1,1,nil,false,EnumWarnFXType.None, Vector3.zero, Vector3.zero)
            player.RO:AddFX("JinShaJingFx1", fx)
        end
    else
        player.RO:RemoveFX("JinShaJingFx1")

        local isbaimulin = bit.band(roadInfo, bit.lshift(1, ERoadInfo.eRI_PLAY2))
        if isbaimulin>0 and not atNight then
            if not player.RO:ContainFx("JinShaJingFx2") then
                local fx = CEffectMgr.Inst:AddObjectFX(88801367,player.RO,0,1,1,nil,false,EnumWarnFXType.None, Vector3.zero, Vector3.zero)
                player.RO:AddFX("JinShaJingFx2", fx)
            end
        else
            player.RO:RemoveFX("JinShaJingFx2")
        end
    end
end

function CRenderScene.m_hookDoSceneAni(this, path, aniName)
    CLuaSceneMgr:PlaySceneObjectAnimation(path, aniName)
end

function CRenderScene.m_hookDoSceneAni(this, path, aniName)
    CLuaSceneMgr:PlaySceneObjectAnimation(path, aniName)
end

function CLuaSceneMgr:GetSceneComponent(path,componentType)
    local component = nil
    if CRenderScene.Inst then
        local root = CRenderScene.Inst.transform:Find(path)
        if root then
            component = root.gameObject:GetComponent(componentType)
        end
    end
    return component
end

function CLuaSceneMgr:PlaySceneObjectAnimation(path, aniName)
    if not CRenderScene.Inst then return end
    local root = CRenderScene.Inst.transform:Find(path)
    if root then
        local animator = self:GetSceneComponent(path,typeof(Animator))
        if animator then
            animator:Play(aniName, -1, 0)
        else
            local animation = root.gameObject:GetComponent(typeof(Animation))
            if animation then
                animation:Play(aniName, PlayMode.StopAll)
            end
        end
    end
end

function CLuaSceneMgr:ShowOrHideSceneObject(sceneTemplateId, objectPath, isShow)
    if CScene.MainScene and CScene.MainScene.SceneTemplateId ~= sceneTemplateId then
        return
    end

    local obj = CRenderScene.Inst.transform:Find(objectPath)
    if obj then
        obj.gameObject:SetActive(false)
        obj.gameObject:SetActive(isShow)
    end
end

------------------------rpc-----------------
function Gas2Gac.PlaySceneObjectHide(sceneTemplateId, objectPath)
    CLuaSceneMgr:ShowOrHideSceneObject(sceneTemplateId, objectPath, false)
end

function Gas2Gac.PlaySceneObjectShow(sceneTemplateId, objectPath)
    CLuaSceneMgr:ShowOrHideSceneObject(sceneTemplateId, objectPath, true)
end

function Gas2Gac.SetWeather(weather)
    CWeatherMgr.Inst:SetWeather(CommonDefs.ConvertIntToEnum(typeof(EWeather),weather),false)
end

-- 场景滤镜扭曲 scneeId:当前场景；
function Gas2Gac.SyncSceneWaveFx(sceneId,waveType)
    if CScene.MainScene and CScene.MainScene.SceneId ~= sceneId then
        return
    end

    if CPostProcessingMgr.s_NewPostProcessingOpen then
        local postEffectCtrl = CPostProcessingMgr.Instance.MainController

        if waveType == 0 then
            --取消第三种效果
            postEffectCtrl:SetHeatWaveMode(EnumHeatWaveMode.HeatwaveEffect ,false)
            return 
        end
        postEffectCtrl:EnableWaterWave(waveType ~= 1)
        if waveType == 2 then
            EventManager.BroadcastInternalForLua(EnumEventType.OnLoadCommonUIFx, {CUIFxPaths.JinZiFx})
        end
        postEffectCtrl:SetHeatWaveMode(EnumHeatWaveMode.HeatwaveEffect ,waveType == 3)
    else
        if waveType == 0 then
            --取消第三种效果
            CPostEffectMgr.Instance.HeatwaveEffectEnable = false
            return 
        end
        CPostEffect.EnableWaterWaveMat(waveType ~= 1)
        if waveType == 2 then
            EventManager.BroadcastInternalForLua(EnumEventType.OnLoadCommonUIFx, {CUIFxPaths.JinZiFx})
        end
        CPostEffectMgr.Instance.HeatwaveEffectEnable = waveType == 3
    end
end
