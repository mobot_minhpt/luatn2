local QnButton = import "L10.UI.QnButton"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local QnTabView = import "L10.UI.QnTabView"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"
local Animator = import "UnityEngine.Animator"
local CServerTimeMgr=import "L10.Game.CServerTimeMgr"
LuaHwFireDefenceEnterWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaHwFireDefenceEnterWnd, "TimeLabel", "TimeLabel", UILabel)
RegistChildComponent(LuaHwFireDefenceEnterWnd, "RuleBtn", "RuleBtn", QnButton)
RegistChildComponent(LuaHwFireDefenceEnterWnd, "StartBtn", "StartBtn", QnButton)
RegistChildComponent(LuaHwFireDefenceEnterWnd, "Tabs", "Tabs", QnTabView)
RegistChildComponent(LuaHwFireDefenceEnterWnd, "DescLabel", "DescLabel", UILabel)
RegistChildComponent(LuaHwFireDefenceEnterWnd, "ItemCell1", "ItemCell1", GameObject)
RegistChildComponent(LuaHwFireDefenceEnterWnd, "ItemCell2", "ItemCell2", GameObject)
RegistChildComponent(LuaHwFireDefenceEnterWnd, "NormalStyle", "NormalStyle", GameObject)
RegistChildComponent(LuaHwFireDefenceEnterWnd, "HardStyle", "HardStyle", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaHwFireDefenceEnterWnd, "m_PlayType")   -- 玩法类型
RegistClassMember(LuaHwFireDefenceEnterWnd, "m_AwardList")
RegistClassMember(LuaHwFireDefenceEnterWnd, "m_NormalBtn")  -- 普通模式按钮
RegistClassMember(LuaHwFireDefenceEnterWnd, "m_HeroBtn")    -- 英雄模式按钮
RegistClassMember(LuaHwFireDefenceEnterWnd, "m_FirstOpenWnd")   -- 首次打开界面
RegistClassMember(LuaHwFireDefenceEnterWnd, "m_DelayAnimTick")
RegistClassMember(LuaHwFireDefenceEnterWnd, 'm_Anim')
function LuaHwFireDefenceEnterWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
	UIEventListener.Get(self.RuleBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRuleBtnClick()
	end)

	UIEventListener.Get(self.StartBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnStartBtnClick()
	end)

	-- self.Tabs.OnSelect = DelegateFactory.Action_QnTabButton_int(function(btn, index)
	--     self:OnTabsSelected(index)
	-- end)
    self.m_NormalBtn = self.transform:Find("Tabs/TabBtn1").gameObject
    self.m_HeroBtn = self.transform:Find("Tabs (1)/TabBtn2").gameObject

    UIEventListener.Get(self.m_NormalBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTabsSelected(0)
	end)
    UIEventListener.Get(self.m_HeroBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTabsSelected(1)
	end)
    self.m_AwardList = {false,false,false}  -- 1: 普通每日 2：英雄首通 3：普通首通
    self.m_FirstOpenWnd = true
    self.m_DelayAnimTick = nil

    local CloseBtn = self.transform:Find("CloseButton").gameObject
    UIEventListener.Get(CloseBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        if LuaHalloween2022Mgr.m_OpenFireDeferenceWndFromMain then
	        CUIManager.ShowUI(CLuaUIResources.Halloween2022MainWnd)
        end
        CUIManager.CloseUI(CLuaUIResources.HwFireDefenceEnterWnd)
	end)
    if LuaHalloween2022Mgr.m_OpenFireDeferenceWndFromMain then g_ScriptEvent:BroadcastInLua("OnHwFireDefenceEnterWndOpen") end
        
    self.m_anim = self.gameObject:GetComponent(typeof(Animator))
    --self.m_anim.enabled = false
end

function LuaHwFireDefenceEnterWnd:DoStartAnim()
    --self.m_anim.enabled = true
    if LuaHalloween2022Mgr.m_OpenFireDeferenceWndFromMain then
        self.m_anim:Play("hwfiredefenceenterwnd_shown",0,0)
    else
        self.m_anim:Play("hwfiredefenceenterwnd_shown",0,0.08)
    end
end

function LuaHwFireDefenceEnterWnd:Init()
    self:DoStartAnim()
    self.TimeLabel.text = Halloween2022_Setting.GetData().CampfirePlayTimeString
    self.DescLabel.text = ""
    self.ItemCell1:SetActive(false)
    self.ItemCell2:SetActive(false)
    self.NormalStyle:SetActive(false)
    self.HardStyle:SetActive(false)
    if not CClientMainPlayer.Inst then return end
    -- EnumPersistPlayDataKey_Lua.eProtectCampfire
    -- EnumTempPlayDataKey_lua.eProtectCampfire
    local playDataU = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eProtectCampfire)
    local PersistPlayDataU = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.PersistPlayData,EnumPersistPlayDataKey_lua.eProtectCampfire)

    if playDataU then
        self.m_AwardList[1] = tonumber(playDataU.Data.StringData) == 1 and (tonumber(playDataU.ExpiredTime) > CServerTimeMgr.Inst.timeStamp)
    end
    if PersistPlayDataU then
        local dataInfo = MsgPackImpl.unpack(PersistPlayDataU.Data)
        if dataInfo.Count >= 3 then
            self.m_AwardList[2] = dataInfo[1]
            self.m_AwardList[3] = dataInfo[2]
        elseif dataInfo.Count >= 2 then
            self.m_AwardList[2] = dataInfo[2]
        end
    end
    if self.m_AwardList[1] then
        self:OnTabsSelected(1)
    else
        self:OnTabsSelected(0)
    end

end
-- 初始化模式
function LuaHwFireDefenceEnterWnd:InitMode()
    if self.m_PlayType == 0 and not self.m_FirstOpenWnd then
        self.m_anim:SetFloat("PlaySpeed", 1.0)     -- 用于动画倒播，动效新做动画后目前不需要了
        self.m_anim:Play("hwfiredefenceenterwnd_qiehuan_1",0,0)
    elseif self.m_PlayType == 1 then
        if self.m_DelayAnimTick then
            UnRegisterTick(self.m_DelayAnimTick)
            self.m_DelayAnimTick = nil
        end
        if self.m_FirstOpenWnd then
            if LuaHalloween2022Mgr.m_OpenFireDeferenceWndFromMain then
                self.m_DelayAnimTick = RegisterTickOnce(function()
                    self.m_anim:Play("hwfiredefenceenterwnd_qiehuan",0,0)    
                end,270)
            else
                self.m_anim:Play("hwfiredefenceenterwnd_qiehuan",0,0.05)    
            end
        else
            self.m_anim:SetFloat("PlaySpeed", 1.0)
            self.m_anim:Play("hwfiredefenceenterwnd_qiehuan",0,0)
        end
    end
    if self.m_PlayType == 0 then
        self.DescLabel.text = Halloween2022_Setting.GetData().CampfirePlayDescString
    elseif self.m_PlayType == 1 then
        self.DescLabel.text = Halloween2022_Setting.GetData().CampfirePlayDescStringHero
    end
    self.NormalStyle:SetActive(self.m_PlayType == 0)
    self.HardStyle:SetActive(self.m_PlayType == 1)
    self.transform:Find("Tabs").gameObject:SetActive(self.m_PlayType == 1)
    self.transform:Find("Tabs (1)").gameObject:SetActive(self.m_PlayType == 0)
    self:InitAwardItem()
    self:InitAwardItemStatus()
    self.m_FirstOpenWnd = false
end

function LuaHwFireDefenceEnterWnd:InitAwardItem()
    if self.m_PlayType == 0 then    -- 普通模式
        self.ItemCell1:SetActive(true)
        self.ItemCell2:SetActive(true)
        self.ItemCell1.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("每日")
        self.ItemCell2.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("首通")
        self.ItemCell1.transform:Find("Label/Sprite"):GetComponent(typeof(UISprite)).spriteName = "common_item01"
        self.ItemCell2.transform:Find("Label/Sprite"):GetComponent(typeof(UISprite)).spriteName = "common_item03"
        self:InitOneItem(self.ItemCell1,Halloween2022_Setting.GetData().NormalCampfireDailyAward)
        self:InitOneItem(self.ItemCell2,Halloween2022_Setting.GetData().NormalCampfireOnceAward)
    elseif self.m_PlayType == 1 then    -- 英雄模式
        self.ItemCell1:SetActive(true)
        self.ItemCell2:SetActive(false)
        self.ItemCell1.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("首通")
        self.ItemCell1.transform:Find("Label/Sprite"):GetComponent(typeof(UISprite)).spriteName = "common_item03"
        self:InitOneItem(self.ItemCell1,Halloween2022_Setting.GetData().HeroCampfireOnceAward)
    end
end
-- 更新玩家获得奖励的情况
function LuaHwFireDefenceEnterWnd:InitAwardItemStatus()
    if self.m_PlayType == 0 then    -- 普通模式
        self.ItemCell1.transform:Find("mask").gameObject:SetActive(self.m_AwardList[1])
        self.ItemCell2.transform:Find("mask").gameObject:SetActive(self.m_AwardList[3])
    elseif self.m_PlayType == 1 then -- 困难模式
        self.ItemCell1.transform:Find("mask").gameObject:SetActive(self.m_AwardList[2])
    end
end

function LuaHwFireDefenceEnterWnd:InitOneItem(curItem, itemID)
    local texture = curItem.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    local ItemData = Item_Item.GetData(itemID)

    if ItemData then texture:LoadMaterial(ItemData.Icon) end

    CommonDefs.AddOnClickListener(
        curItem,
        DelegateFactory.Action_GameObject(
            function(go)
                CItemInfoMgr.ShowLinkItemTemplateInfo(itemID, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
            end
        ),
        false
    )
end
--@region UIEvent

function LuaHwFireDefenceEnterWnd:OnRuleBtnClick()
	g_MessageMgr:ShowMessage("Halloween2022_ProtectCampfire_TaskDes")
end

function LuaHwFireDefenceEnterWnd:OnStartBtnClick()
	-- 发RPC
    Gac2Gas.RequestStartProtectFire(self.m_PlayType) 
    if self.m_PlayType == 0 then
        -- 普通模式请求
        --print("QueryNormalMode")
    elseif self.m_PlayType == 1 then
        -- 英雄模式请求
        --print("QueryHeroMode")
    end
end

function LuaHwFireDefenceEnterWnd:OnTabsSelected(index)
    self.m_PlayType = index
    self:InitMode()
end

function LuaHwFireDefenceEnterWnd:OnDisable()
    if self.m_DelayAnimTick then
        UnRegisterTick(self.m_DelayAnimTick)
        self.m_DelayAnimTick = nil
    end
end
--@endregion UIEvent
