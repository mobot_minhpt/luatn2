local CGuideMgr=import "L10.Game.Guide.CGuideMgr"
local UITabBar = import "L10.UI.UITabBar"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local QnTabView = import "L10.UI.QnTabView"
local UIDefaultTableView=import "L10.UI.UIDefaultTableView"
local UIDefaultTableViewCell=import "L10.UI.UIDefaultTableViewCell"
local CellIndexType=import "L10.UI.UITableView+CellIndexType"
local CellIndex=import "L10.UI.UITableView+CellIndex"
local QnModelPreviewer = import "L10.UI.QnModelPreviewer"
local UIScrollViewIndicator = import "UIScrollViewIndicator"
local CUIModelPreviewMgr = import "L10.Game.CUIModelPreviewMgr"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local CButton = import "L10.UI.CButton"
local CMainCamera = import "L10.Engine.CMainCamera"
local CUITexture = import "L10.UI.CUITexture"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local EShareType = import "L10.UI.EShareType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local CBaseWnd = import "L10.UI.CBaseWnd"
local EnumYingLingState = import "L10.Game.EnumYingLingState"
local COpenEntryMgr = import "L10.Game.COpenEntryMgr"
local CCharacterRenderMgr = import "L10.Engine.CCharacterRenderMgr"

LuaAppearanceWnd = class()

RegistChildComponent(LuaAppearanceWnd, "m_LeftArea", "LeftArea", GameObject)
RegistChildComponent(LuaAppearanceWnd, "m_RightArea", "RightArea", GameObject)
RegistChildComponent(LuaAppearanceWnd, "m_ShareRoot", "ShareRoot", GameObject)
RegistChildComponent(LuaAppearanceWnd, "m_BeiShiSettingView", "AppearanceBeiShiSettingView", CCommonLuaScript)
RegistChildComponent(LuaAppearanceWnd, "m_FashionabilityLabel", "FashionabilityLabel", UILabel)
RegistChildComponent(LuaAppearanceWnd, "m_TabBar", "TabBar", UITabBar)
RegistChildComponent(LuaAppearanceWnd, "m_ModelPreviewMgr", "ModelPreviewMgr", CUIModelPreviewMgr)
RegistChildComponent(LuaAppearanceWnd, "m_FashionPartTable", "FashionPartTable", UITable)
RegistChildComponent(LuaAppearanceWnd, "m_DayNightSwitch", "DayNightSwitchButton", CButton)
RegistChildComponent(LuaAppearanceWnd, "m_XianFanSwitch", "XianFanSwitchButton", CButton)
RegistChildComponent(LuaAppearanceWnd, "m_SettingButton", "SettingButton", GameObject)
RegistChildComponent(LuaAppearanceWnd, "m_AdditionButton", "AdditionButton", GameObject)
RegistChildComponent(LuaAppearanceWnd, "m_ResetButton", "ResetButton", GameObject)
RegistChildComponent(LuaAppearanceWnd, "m_FashionPreviewButtons", "FashionPreviewButtons", UITable)
RegistChildComponent(LuaAppearanceWnd, "m_AddtionRoot", "AdditionRoot", GameObject)
RegistChildComponent(LuaAppearanceWnd, "m_ShareButton", "ShareButton", GameObject)
RegistChildComponent(LuaAppearanceWnd, "m_TabenButton", "TabenButton", GameObject)
RegistChildComponent(LuaAppearanceWnd, "m_HideUIButton", "HideUIButton", GameObject)
RegistChildComponent(LuaAppearanceWnd, "m_DayNightSwitch2", "DayNightSwitchButton2", CButton) --用于隐藏UI时切换白天夜晚
RegistChildComponent(LuaAppearanceWnd, "m_ShowUIButton", "ShowUIButton", GameObject)
RegistChildComponent(LuaAppearanceWnd, "m_GroupTableView", "GroupTableView", UIDefaultTableView)
RegistChildComponent(LuaAppearanceWnd, "m_DefaultHideViewsRoot", "DefaultHideViewsRoot", Transform)
RegistChildComponent(LuaAppearanceWnd, "m_SubviewRoot", "SubviewRoot", Transform)
RegistChildComponent(LuaAppearanceWnd, "m_Templates", "Templates", GameObject)
RegistChildComponent(LuaAppearanceWnd, "m_ScrollViewBg", "ScrollViewBg", UIWidget)
RegistChildComponent(LuaAppearanceWnd, "m_ScrollView", "ContentScrollView", UIScrollView)
RegistChildComponent(LuaAppearanceWnd, "m_ScrollViewIndicator", "ScrollViewIndicator", UIScrollViewIndicator)
RegistChildComponent(LuaAppearanceWnd,"m_ContentTable", "ContentTable", UITable)
RegistChildComponent(LuaAppearanceWnd,"m_ContentGrid", "ContentGrid", UIGrid)
RegistChildComponent(LuaAppearanceWnd,"m_FashionContentGrid", "FashionContentGrid", UIGrid)
RegistChildComponent(LuaAppearanceWnd,"m_SharePlayerInfo", "SharePlayerInfo", GameObject)
RegistChildComponent(LuaAppearanceWnd,"m_ShareScrollView", "ShareScrollView", UIScrollView)
RegistChildComponent(LuaAppearanceWnd,"m_ShareTable", "ShareTable", UITable)
RegistChildComponent(LuaAppearanceWnd,"m_ShareFashionItem", "ShareFashionItem", GameObject)

RegistClassMember(LuaAppearanceWnd, "m_DarkCorner")

RegistClassMember(LuaAppearanceWnd, "m_TakeOffButton") --一件脱下 暂时不用
RegistClassMember(LuaAppearanceWnd, "m_ExpressionButton") --表情动作，套装可能会携带
RegistClassMember(LuaAppearanceWnd, "m_TransformButton")    --变身，套装可能会携带
RegistClassMember(LuaAppearanceWnd, "m_EvaluateButton")     --评价，套装需要显示

RegistClassMember(LuaAppearanceWnd, "m_PreviewTransformOn")             --是否开启了时装变身
RegistClassMember(LuaAppearanceWnd, "m_PreviewTransformMonsterId")      --时装变身对应的monsterID
RegistClassMember(LuaAppearanceWnd, "m_PreviewTransformExpressionId")   --时装变身对应的表情动作
RegistClassMember(LuaAppearanceWnd, "m_PreviewSpecialExpressionId")     --时装非变身对应的表情动作

RegistClassMember(LuaAppearanceWnd, "m_AllMenuInfo")
RegistClassMember(LuaAppearanceWnd, "m_AllSubviews")
RegistClassMember(LuaAppearanceWnd, "m_CurMenuInfo")
RegistClassMember(LuaAppearanceWnd, "m_CurPreview")
RegistClassMember(LuaAppearanceWnd, "m_LastActiveSubview")
RegistClassMember(LuaAppearanceWnd, "m_NeedHideRO") --是否需要隐藏角色
RegistClassMember(LuaAppearanceWnd, "m_DisableTransform") --是否需要禁止变身（用于非时装相关的tab）

RegistClassMember(LuaAppearanceWnd, "m_ModelScreenXOffset")
RegistClassMember(LuaAppearanceWnd, "m_ContentDefaultOffset")       --外观列表的初始y坐标
RegistClassMember(LuaAppearanceWnd, "m_ContentHeight")              --外观列表的初始最大高度
RegistClassMember(LuaAppearanceWnd, "m_ContentHeaderHeight")        --外观列表的header高度（部分列表需要显示header）
RegistClassMember(LuaAppearanceWnd, "m_ContentSmallFooterHeight")   --外观列表的小footer高度（部分列表用到）
RegistClassMember(LuaAppearanceWnd, "m_ContentBigFooterHeight")     --外观列表的大footer高度(部分列表用到)

RegistClassMember(LuaAppearanceWnd, "m_BackPendantPosData")     --外观列表的大footer高度(部分列表用到)

RegistClassMember(LuaAppearanceWnd, "m_CharacterModelRestored")     --角色参数是否已经还原，盛放的接口，为了暂时界面和捏脸的冲突

function LuaAppearanceWnd:Awake()
    self.m_ShareButton:SetActive(not CommonDefs.IS_VN_CLIENT)
    self.m_AddtionRoot.transform:GetComponent(typeof(UISprite)).width = CommonDefs.IS_VN_CLIENT and 200 or 296
    
    self.m_ModelScreenXOffset = 0.35 -- 取值0-1,0.5表示居中
    self.m_ContentDefaultOffset = 380 -- 外观列表的初始y坐标
    self.m_ContentHeight = 903 -- 外观列表的初始最大高度
    self.m_ContentHeaderHeight = 65 -- 外观列表的header高度(部分列表需要显示header)
    self.m_ContentSmallFooterHeight = 80 -- 外观列表的小footer高度(部分列表用到)
    self.m_ContentBigFooterHeight = 294 -- 外观列表的大footer高度(部分列表用到)

    self.m_BackPendantPosData = CreateFromClass(MakeArrayClass(SByte),4)

    self.m_Templates:SetActive(false)
    --通用的itemcell中不常用的节点默认隐藏，subview处理就可以简化
    local commonItemCell =  self.m_Templates.transform:Find("CommonItemCell")
    commonItemCell:Find("Disabled").gameObject:SetActive(false)
    commonItemCell:Find("Locked").gameObject:SetActive(false)
    commonItemCell:Find("Corner").gameObject:SetActive(false)
    commonItemCell:Find("CornerNew").gameObject:SetActive(false)
    commonItemCell:Find("Selected").gameObject:SetActive(false)
    commonItemCell:Find("TryOnIcon").gameObject:SetActive(false)
    local commonClosetItemCell = self.m_Templates.transform:Find("CommonClosetItemCell")
    commonClosetItemCell:Find("Item/Add").gameObject:SetActive(false)
    local commonClosetSingleLineItemCell = self.m_Templates.transform:Find("CommonClosetSingleLineItemCell")
    commonClosetSingleLineItemCell:Find("Item/Add").gameObject:SetActive(false)
    commonClosetSingleLineItemCell:Find("Item/Disabled").gameObject:SetActive(false)
    self.m_DarkCorner = self.m_ModelPreviewMgr.transform:Find("DarkCorner").gameObject
    self.m_AddtionRoot:SetActive(false)
    self.m_ShowUIButton:SetActive(false)
    self.m_DayNightSwitch2.gameObject:SetActive(false)
    self.m_ShareRoot:SetActive(false)
    self.m_BeiShiSettingView.gameObject:SetActive(false)
    self.m_ShareFashionItem:SetActive(false)
    self.m_TabBar.OnTabChange = CommonDefs.CombineListner_Action_GameObject_int(self.m_TabBar.OnTabChange, DelegateFactory.Action_GameObject_int(function(go, index)
        self:OnTabChange(go, index)
    end), true)

    local childCount = self.m_FashionPreviewButtons.transform.childCount
    for i=0,childCount-1 do
        self.m_FashionPreviewButtons.transform:GetChild(i).gameObject:SetActive(false)
    end
    self.m_TakeOffButton = self.m_FashionPreviewButtons.transform:Find("TakeOffButton").gameObject
    self.m_ExpressionButton = self.m_FashionPreviewButtons.transform:Find("ExpressionButton").gameObject
    self.m_TransformButton = self.m_FashionPreviewButtons.transform:Find("TransformButton").gameObject
    self.m_EvaluateButton = self.m_FashionPreviewButtons.transform:Find("EvaluateButton").gameObject
    UIEventListener.Get(self.m_FashionabilityLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) self:OnFashionabilityClick() end)
    UIEventListener.Get(self.m_DayNightSwitch.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) self:OnDayNightSwitchClick() end)
    UIEventListener.Get(self.m_DayNightSwitch2.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) self:OnDayNightSwitchClick() end)
    UIEventListener.Get(self.m_XianFanSwitch.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) self:OnXianFanSwitchClick() end)
    UIEventListener.Get(self.m_SettingButton).onClick = DelegateFactory.VoidDelegate(function(go) self:OnSettingButtonClick() end)
    UIEventListener.Get(self.m_AdditionButton).onClick = DelegateFactory.VoidDelegate(function(go) self:OnAddtionButtonClick() end)
    UIEventListener.Get(self.m_ResetButton).onClick = DelegateFactory.VoidDelegate(function(go) self:OnResetButtonClick() end)
    UIEventListener.Get(self.m_TakeOffButton).onClick = DelegateFactory.VoidDelegate(function(go) self:OnTakeOffButtonClick() end)
    UIEventListener.Get(self.m_ExpressionButton).onClick = DelegateFactory.VoidDelegate(function(go) self:OnExpressionButtonClick() end)
    UIEventListener.Get(self.m_TransformButton).onClick = DelegateFactory.VoidDelegate(function(go) self:OnTransformButtonClick() end)
    UIEventListener.Get(self.m_EvaluateButton).onClick = DelegateFactory.VoidDelegate(function(go) self:OnEvaluateButtonClick() end)

    --addition
    UIEventListener.Get(self.m_ShareButton).onClick = DelegateFactory.VoidDelegate(function(go) self:OnShareButtonClick() end)
    UIEventListener.Get(self.m_TabenButton).onClick = DelegateFactory.VoidDelegate(function(go) self:OnTabenButtonClick() end)
    UIEventListener.Get(self.m_HideUIButton).onClick = DelegateFactory.VoidDelegate(function(go) self:OnHideUIButtonClick() end)
    UIEventListener.Get(self.m_ShowUIButton).onClick = DelegateFactory.VoidDelegate(function(go) self:OnShowUIButtonClick() end)

    self.m_GroupTableView:Init(
        function() return self:NumberOfSectionsInTableView() end,
        function(section) return self:NumberOfRowsInSection(section) end,
        function(cell,index) self:RefreshCellForRowAtIndex(cell, index) end,
        function(index,expanded) self:OnSectionClicked(index, expanded) end,
        function(index) self:OnRowSelected(index) end
    )
    self:GenerateMenuInfo()
    self:CollectAllSubviews()
end

function LuaAppearanceWnd:CollectAllSubviews()
   self.m_AllSubviews = {
        "AppearanceFashionSuitSubview",
        "AppearanceFashionSubview",
        "AppearanceWingSubview",
        "AppearanceLuminousSubview",
        "AppearanceTalismanSubview",
        "AppearanceRanFaJiSubview",
        "AppearanceProfileFrameSubview",
        "AppearanceUmbrellaSubview",
        "AppearanceTeamLeaderIconSubview",
        "AppearanceTeamMemberBgSubview",
        "AppearanceClosetMyOutfitSubview",
        "AppearanceClosetFashionSubview",
        "AppearanceClosetWingSubview",
        "AppearanceClosetLuminousSubview",
        "AppearanceClosetTalismanSubview",
        "AppearanceClosetFashionTabenSubview",
        "AppearanceClosetRanFaJiSubview",
        "AppearanceClosetProfileFrameSubview",
        "AppearanceClosetUmbrellaSubview",
        "AppearanceClosetTeamLeaderIconSubview",
        "AppearanceClosetTeamMemberBgSubview",
        "AppearanceAnimationSubview",
    }
end

function LuaAppearanceWnd:GenerateMenuInfo()

    local defaultOffsetY = self.m_ContentDefaultOffset
    local offsetYWithHeader = self.m_ContentDefaultOffset - self.m_ContentHeaderHeight
    local height = self.m_ContentHeight
    local heightWithSmallFooter = self.m_ContentHeight - self.m_ContentSmallFooterHeight
    local heightWithBigFooter = self.m_ContentHeight - self.m_ContentBigFooterHeight
    local heightWithHeader = self.m_ContentHeight - self.m_ContentHeaderHeight
    local heightWithHeaderAndSmallFooter = self.m_ContentHeight - self.m_ContentHeaderHeight - self.m_ContentSmallFooterHeight
    local heightWithHeaderAndBigFooter = self.m_ContentHeight - self.m_ContentHeaderHeight - self.m_ContentBigFooterHeight

    local allMenu = {
        {
            title = LocalString.GetString("时装"),
            action = function() end,
            rows = {
                {title=LocalString.GetString("套装"), action=function() self:UpdatePreviewFashionSetting(true, true, false) self:ShowSubview("AppearanceFashionSuitSubview", false, offsetYWithHeader, heightWithHeaderAndBigFooter) end},
                {title=LocalString.GetString("头部"), action=function() self:UpdatePreviewFashionSetting(true, true, false) self:ShowSubview("AppearanceFashionSubview", false, offsetYWithHeader, heightWithHeaderAndBigFooter, EnumAppearanceFashionPosition.eHead) end},
                {title=LocalString.GetString("身体"), action=function() self:UpdatePreviewFashionSetting(true, true, false) self:ShowSubview("AppearanceFashionSubview", false, offsetYWithHeader, heightWithHeaderAndBigFooter, EnumAppearanceFashionPosition.eBody) end},
                {title=LocalString.GetString("背饰"), action=function() self:UpdatePreviewFashionSetting(true, true, false) self:ShowSubview("AppearanceFashionSubview", false, offsetYWithHeader, heightWithHeaderAndBigFooter, EnumAppearanceFashionPosition.eBeiShi) end},
            }
        },
        {
            title = LocalString.GetString("翅膀"),
            action = function() self:UpdatePreviewFashionSetting(false, false, true) self:ShowSubview("AppearanceWingSubview", false, offsetYWithHeader, heightWithHeaderAndBigFooter) end,
            rows = {}
        },
        {
            title = LocalString.GetString("光效"),
            action = function() end,
            rows = {
                {title=LocalString.GetString("强化套装"), action=function() self:UpdatePreviewFashionSetting(false, false, true) self:ShowSubview("AppearanceLuminousSubview", false, offsetYWithHeader, heightWithHeaderAndBigFooter, EnumAppearancePreviewLuminousType.eIntensifySuit) end},
                {title=LocalString.GetString("石之灵"), action=function() self:UpdatePreviewFashionSetting(false, false, true) self:ShowSubview("AppearanceLuminousSubview", false, offsetYWithHeader, heightWithHeaderAndBigFooter, EnumAppearancePreviewLuminousType.eGemGroup) end},
                #LuaAppearancePreviewMgr:GetAllSkillAppearInfo()>0 and
                    {title=LocalString.GetString("技能"), action=function() self:UpdatePreviewFashionSetting(false, false, true) self:ShowSubview("AppearanceLuminousSubview", false, offsetYWithHeader, heightWithHeaderAndBigFooter, EnumAppearancePreviewLuminousType.eSkillAppearance) end}
                     or nil,
            }
        },
        {
            title = LocalString.GetString("法宝"),
            action = function() self:UpdatePreviewFashionSetting(false, false, true) self:ShowSubview("AppearanceTalismanSubview", false, offsetYWithHeader, heightWithHeaderAndBigFooter) end,
            rows = {}
        },
        {
            title = LocalString.GetString("个性化"),
            action = function() end,
            rows = {
                {title=LocalString.GetString("高级染发"), action=function() self:UpdatePreviewFashionSetting(false, false, true) self:ShowSubview("AppearanceRanFaJiSubview", false, defaultOffsetY, heightWithBigFooter) end},
                {title=LocalString.GetString("头像框"), action=function() self:UpdatePreviewFashionSetting(false, false, false) self:ShowSubview("AppearanceProfileFrameSubview", true, defaultOffsetY, heightWithBigFooter) end},
                {title=LocalString.GetString("伞面"), action=function() self:UpdatePreviewFashionSetting(false, false, true) self:ShowSubview("AppearanceUmbrellaSubview", false, defaultOffsetY, heightWithBigFooter) end},
                {title=LocalString.GetString("队标"), action=function() self:UpdatePreviewFashionSetting(false, false, false) self:ShowSubview("AppearanceTeamLeaderIconSubview", true, defaultOffsetY, heightWithBigFooter) end},
                {title=LocalString.GetString("队伍背景"), action=function() self:UpdatePreviewFashionSetting(false, false, false) self:ShowSubview("AppearanceTeamMemberBgSubview", true, defaultOffsetY, heightWithBigFooter) end},
                --还没投放, 先注释掉
                --{title=LocalString.GetString("动作外观"), action=function() self:UpdatePreviewFashionSetting(false, false, false) self:ShowSubview("AppearanceAnimationSubview", true, defaultOffsetY, heightWithBigFooter) end},
            },
        },
    }

    local closetMenu = {
        {
            title = LocalString.GetString("我的搭配"),
            action = function() self:UpdatePreviewFashionSetting(true, false, false) self:ShowSubview("AppearanceClosetMyOutfitSubview", false, defaultOffsetY, height) end,
            rows = {},
        },
        {
            title = LocalString.GetString("时装"),
            action = function() end,
            rows = {
                {title=LocalString.GetString("头部"), action=function() self:UpdatePreviewFashionSetting(true, false, false) self:ShowSubview("AppearanceClosetFashionSubview", false, offsetYWithHeader, heightWithHeader, EnumAppearanceFashionPosition.eHead) end},
                {title=LocalString.GetString("身体"), action=function() self:UpdatePreviewFashionSetting(true, false, false) self:ShowSubview("AppearanceClosetFashionSubview", false, offsetYWithHeader, heightWithHeader, EnumAppearanceFashionPosition.eBody) end},
                {title=LocalString.GetString("背饰"), action=function() self:UpdatePreviewFashionSetting(true, false, false) self:ShowSubview("AppearanceClosetFashionSubview", false, offsetYWithHeader, heightWithHeader, EnumAppearanceFashionPosition.eBeiShi) end},
            }
        },
        {
            title = LocalString.GetString("翅膀"),
            action = function() self:UpdatePreviewFashionSetting(false, false, true) self:ShowSubview("AppearanceClosetWingSubview", false, offsetYWithHeader, heightWithHeaderAndSmallFooter) end,
            rows = {}
        },
        {
            title = LocalString.GetString("光效"),
            action = function() end,
            rows = {
                {title=LocalString.GetString("强化套装"), action=function() self:UpdatePreviewFashionSetting(false, false, true) self:ShowSubview("AppearanceClosetLuminousSubview", false, offsetYWithHeader, heightWithHeaderAndSmallFooter, EnumAppearancePreviewLuminousType.eIntensifySuit) end},
                {title=LocalString.GetString("石之灵"), action=function() self:UpdatePreviewFashionSetting(false, false, true) self:ShowSubview("AppearanceClosetLuminousSubview", false, offsetYWithHeader, heightWithHeaderAndSmallFooter, EnumAppearancePreviewLuminousType.eGemGroup) end},
                #LuaAppearancePreviewMgr:GetAllSkillAppearInfo()>0 and
                    {title=LocalString.GetString("技能"), action=function() self:UpdatePreviewFashionSetting(false, false, true) self:ShowSubview("AppearanceClosetLuminousSubview", false, offsetYWithHeader, heightWithHeaderAndSmallFooter, EnumAppearancePreviewLuminousType.eSkillAppearance) end}
                     or nil,
            }
        },
        {
            title = LocalString.GetString("法宝"),
            action = function() self:UpdatePreviewFashionSetting(false, false, true) self:ShowSubview("AppearanceClosetTalismanSubview", false, offsetYWithHeader, heightWithHeaderAndSmallFooter) end,
            rows = {}
        },
        {
            title = LocalString.GetString("个性化"),
            action = function() end,
            rows = {
                {title=LocalString.GetString("拓本"), action=function() self:UpdatePreviewFashionSetting(false, false, false) self:ShowSubview("AppearanceClosetFashionTabenSubview", false, offsetYWithHeader, heightWithHeaderAndSmallFooter) end},
                {title=LocalString.GetString("高级染发"), action=function() self:UpdatePreviewFashionSetting(false, false, true) self:ShowSubview("AppearanceClosetRanFaJiSubview", false, defaultOffsetY, heightWithSmallFooter) end},
                {title=LocalString.GetString("头像框"), action=function() self:UpdatePreviewFashionSetting(false, false, false) self:ShowSubview("AppearanceClosetProfileFrameSubview", true, defaultOffsetY, heightWithSmallFooter) end},
                {title=LocalString.GetString("伞面"), action=function() self:UpdatePreviewFashionSetting(false, false, true) self:ShowSubview("AppearanceClosetUmbrellaSubview", false, defaultOffsetY, heightWithSmallFooter) end},
                {title=LocalString.GetString("队标"), action=function() self:UpdatePreviewFashionSetting(false, false, false) self:ShowSubview("AppearanceClosetTeamLeaderIconSubview", true, defaultOffsetY, heightWithSmallFooter) end},
                {title=LocalString.GetString("队伍背景"), action=function() self:UpdatePreviewFashionSetting(false, false, false) self:ShowSubview("AppearanceClosetTeamMemberBgSubview", true, defaultOffsetY, heightWithSmallFooter) end},
                --还没投放, 先注释掉
                --{title=LocalString.GetString("动作外观"), action=function() self:UpdatePreviewFashionSetting(false, false, false) self:ShowSubview("AppearanceAnimationSubview", true, defaultOffsetY, heightWithSmallFooter) end},
            }
        },
    }
    self.m_AllMenuInfo = {closetMenu, allMenu}
end

function LuaAppearanceWnd:Init()
    --伞的过期时间处理，没想好放哪儿，暂时先放这里 
    Gac2Gas.RequestCheckExpressionAppearance()
    self:UpdateFashionability()
    self:InitModelPreviewer()
    self:InitDefaultSelection()
    self:UpdateFashionIcon()     
    
    --重置
    LuaAppearancePreviewMgr.m_DefaultWndTabIndex = -1
    LuaAppearancePreviewMgr.m_DefaultWndSection = -1
    LuaAppearancePreviewMgr.m_DefaultWndRow = -1
    --如果是使用道具获得技能外观，打开界面时技能引导可能会被tab引导给吞掉/解锁永久/拓本引导也有类似问题，这里加个限制
    if not CGuideMgr.Inst:IsInPhase(EnumGuideKey.AppearanceSkillAppearance) 
        and not CGuideMgr.Inst:IsInPhase(EnumGuideKey.AppearanceTaBen)
        and not CGuideMgr.Inst:IsInPhase(EnumGuideKey.AppearanceUnlockFashion) then 
        --根据tab触发不同的引导
        if self.m_TabBar.SelectedIndex==0 then --衣橱
            local triggered = CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.AppearanceGuide1)
            if triggered then
                COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.AppearanceGuide2)
            end
        else
            local triggered = CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.AppearanceGuide2)
            if triggered then
                COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.AppearanceGuide1)
            end
        end
    end

    if LuaAppearancePreviewMgr.IsBeiShiSetting then
        self:OpenBeiShiSetting()
    end
end

function LuaAppearanceWnd:InitDefaultSelection()
    local defaultTabIndex = LuaAppearancePreviewMgr.m_DefaultWndTabIndex
    local defaultSection = LuaAppearancePreviewMgr.m_DefaultWndSection
    local defaultRow = LuaAppearancePreviewMgr.m_DefaultWndRow
    if defaultTabIndex>-1 then
        print("AppearanceWnd InitDefaultSelection", defaultTabIndex, defaultSection, defaultRow)
        self.m_TabBar:ChangeTab(defaultTabIndex, false)
        self.m_GroupTableView:LoadData( CellIndex(defaultSection>-1 and defaultSection or 0, 
                                                    defaultRow>-1 and defaultRow or 0,
                                                    defaultRow>-1 and CellIndexType.Row or CellIndexType.Section), true) 
        return
    end
    -- 1. 如果玩家有时装（排除拓本），打开界面默认定位到 衣橱-时装-第一个有时装的子页面（头部、身体或背饰）
    -- 2. 如果玩家没有时装（排除拓本），打开界面默认定位到 所有-时装-套装
    local tabIndex = 1
    local cellIndex = CellIndex(0, 0, CellIndexType.Section) --默认选中 所有-时装-套装 
    local mainplayer = CClientMainPlayer.Inst
    if mainplayer then
        if LuaAppearancePreviewMgr:MyFashionExists(EnumAppearanceFashionPosition.eHead) then
            tabIndex = 0
            cellIndex = CellIndex(1, 0, CellIndexType.Row)
            bFound = true
        elseif LuaAppearancePreviewMgr:MyFashionExists(EnumAppearanceFashionPosition.eBody) then
            tabIndex = 0
            cellIndex = CellIndex(1, 1, CellIndexType.Row)
            bFound = true
        elseif LuaAppearancePreviewMgr:MyFashionExists(EnumAppearanceFashionPosition.eBeiShi) then
            tabIndex = 0
            cellIndex = CellIndex(1, 2, CellIndexType.Row)
            bFound = true
        end
    end
    self.m_TabBar:ChangeTab(tabIndex, false)
    self.m_GroupTableView:LoadData(cellIndex, true) 
end

function LuaAppearanceWnd:IsFashionSuitSubviewSelected()
    return  self.m_TabBar.SelectedIndex==1 and
        self.m_GroupTableView.SelectedIndex and self.m_GroupTableView.SelectedIndex.section==0 and self.m_GroupTableView.SelectedIndex.row==0 or false
end

function LuaAppearanceWnd:UpdateFashionability()
    self.m_FashionabilityLabel.text = SafeStringFormat3(LocalString.GetString("风尚度 %d"), CClientMainPlayer.Inst and CClientMainPlayer.Inst.WardrobeProp.Fashionability or 0)
end

function LuaAppearanceWnd:OnTabChange(go, index)
    self.m_CurMenuInfo = self.m_AllMenuInfo[index+1]
    if index == 0 then
        --衣橱——默认选中时装-头部
        self.m_GroupTableView:LoadData(CellIndex(1, 0, CellIndexType.Row), true)
    else
        --所有——默认选中新品推荐section
        self.m_GroupTableView:LoadData(CellIndex(0, 0, CellIndexType.Section), true)
    end
end

-- group table view
function LuaAppearanceWnd:NumberOfSectionsInTableView()
    return #self.m_CurMenuInfo
end

function LuaAppearanceWnd:NumberOfRowsInSection(section)
    return #self.m_CurMenuInfo[section+1].rows
end

function LuaAppearanceWnd:RefreshCellForRowAtIndex(cell, cellIndex)
    local viewCell = cell:GetComponent(typeof(UIDefaultTableViewCell))
    local title = ""
    if cellIndex.type == CellIndexType.Section then
        title = self.m_CurMenuInfo[cellIndex.section+1].title
    else
        title = self.m_CurMenuInfo[cellIndex.section+1].rows[cellIndex.row+1].title
    end
    viewCell.m_Button.Text = title
end

function LuaAppearanceWnd:OnSectionClicked(cellIndex, expanded)
    local menuItem = self.m_CurMenuInfo[cellIndex.section+1]
    print("OnSectionClicked", menuItem.title)
    if #menuItem.rows == 0 then
        menuItem.action()
    end
    if menuItem.title==LocalString.GetString("我的搭配") then
        CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.AppearanceDaPei)
    end
end

function LuaAppearanceWnd:OnRowSelected(cellIndex)
    local menuItem = self.m_CurMenuInfo[cellIndex.section+1].rows[cellIndex.row+1]
    print("OnRowSelected", menuItem.title)
    menuItem.action()

    if menuItem.title==LocalString.GetString("拓本") then
        CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.AppearanceTaBen)
    elseif menuItem.title==LocalString.GetString("技能") then
        CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.AppearanceSkillAppearance)
    elseif self.m_TabBar.SelectedIndex==0 and cellIndex.section==1 and self:GetFirstCanUnlockFashionItem()~=nil then                                                                                                                    
        CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.AppearanceUnlockFashion)
    end
end

function LuaAppearanceWnd:OnEnable()
    CMainCamera.Inst:SetCameraEnableStatus(false, "appearancewnd", false)
    g_ScriptEvent:AddListener("OnSyncFashionability",self,"OnSyncFashionability")
    g_ScriptEvent:AddListener("OnSendFashionTransformState",self,"OnSendFashionTransformState")
    g_ScriptEvent:AddListener("OnEvaluateFashionSuitSuccess_Permanent",self,"OnEvaluateFashionSuitSuccess_Permanent")
    g_ScriptEvent:AddListener("MainPlayerAppearanceUpdate",self,"OnMainPlayerAppearanceUpdate")
    g_ScriptEvent:AddListener("OnSyncMainPlayerAppearanceProp",self,"OnSyncMainPlayerAppearanceProp")
    g_ScriptEvent:AddListener("RequestPreviewMainPlayerFashionOutfit", self, "OnRequestPreviewMainPlayerFashionOutfit")
    g_ScriptEvent:AddListener("RequestPreviewMainPlayerFashion", self, "OnRequestPreviewMainPlayerFashion")
    g_ScriptEvent:AddListener("RequestPreviewMainPlayerFashionSuit", self, "OnRequestPreviewMainPlayerFashionSuit")
    g_ScriptEvent:AddListener("RequestPreviewMainPlayerWing", self, "OnRequestPreviewMainPlayerWing")
    g_ScriptEvent:AddListener("RequestPreviewMainPlayerIntensitySuit", self, "OnRequestPreviewMainPlayerIntensitySuit")
    g_ScriptEvent:AddListener("RequestPreviewMainPlayerGemGroup", self, "OnRequestPreviewMainPlayerGemGroup")
    g_ScriptEvent:AddListener("RequestPreviewMainPlayerTalismanAppearance", self, "OnRequestPreviewMainPlayerTalismanAppearance")
    g_ScriptEvent:AddListener("RequestPreviewMainPlayerUmbrella", self, "OnRequestPreviewMainPlayerUmbrella")
    g_ScriptEvent:AddListener("RequestPreviewMainPlayerRanFaJi", self, "OnRequestPreviewMainPlayerRanFaJi")

    g_ScriptEvent:AddListener("OpenBeiShiSetting",self,"OpenBeiShiSetting")
    g_ScriptEvent:AddListener("CloseBeiShiSetting",self,"CloseBeiShiSetting")
    g_ScriptEvent:AddListener("OnBeiShiSetPosChange",self,"OnBeiShiSetPosChange")

    g_ScriptEvent:AddListener("OnRequestOpenAppearanceSettingWnd",self,"OnRequestOpenAppearanceSettingWnd")
end

function LuaAppearanceWnd:OnDisable()
    CMainCamera.Inst:SetCameraEnableStatus(true, "appearancewnd", false)
    g_ScriptEvent:RemoveListener("OnSyncFashionability",self,"OnSyncFashionability")
    g_ScriptEvent:RemoveListener("OnSendFashionTransformState",self,"OnSendFashionTransformState")
    g_ScriptEvent:RemoveListener("OnEvaluateFashionSuitSuccess_Permanent",self,"OnEvaluateFashionSuitSuccess_Permanent")
    g_ScriptEvent:RemoveListener("MainPlayerAppearanceUpdate",self,"OnMainPlayerAppearanceUpdate")
    g_ScriptEvent:RemoveListener("OnSyncMainPlayerAppearanceProp",self,"OnSyncMainPlayerAppearanceProp")
    g_ScriptEvent:RemoveListener("RequestPreviewMainPlayerFashionOutfit", self, "OnRequestPreviewMainPlayerFashionOutfit")
    g_ScriptEvent:RemoveListener("RequestPreviewMainPlayerFashion", self, "OnRequestPreviewMainPlayerFashion")
    g_ScriptEvent:RemoveListener("RequestPreviewMainPlayerFashionSuit", self, "OnRequestPreviewMainPlayerFashionSuit")
    g_ScriptEvent:RemoveListener("RequestPreviewMainPlayerWing", self, "OnRequestPreviewMainPlayerWing")
    g_ScriptEvent:RemoveListener("RequestPreviewMainPlayerIntensitySuit", self, "OnRequestPreviewMainPlayerIntensitySuit")
    g_ScriptEvent:RemoveListener("RequestPreviewMainPlayerGemGroup", self, "OnRequestPreviewMainPlayerGemGroup")
    g_ScriptEvent:RemoveListener("RequestPreviewMainPlayerTalismanAppearance", self, "OnRequestPreviewMainPlayerTalismanAppearance")
    g_ScriptEvent:RemoveListener("RequestPreviewMainPlayerUmbrella", self, "OnRequestPreviewMainPlayerUmbrella")
    g_ScriptEvent:RemoveListener("RequestPreviewMainPlayerRanFaJi", self, "OnRequestPreviewMainPlayerRanFaJi")

    g_ScriptEvent:RemoveListener("OpenBeiShiSetting",self,"OpenBeiShiSetting")
    g_ScriptEvent:RemoveListener("CloseBeiShiSetting",self,"CloseBeiShiSetting")
    g_ScriptEvent:RemoveListener("OnBeiShiSetPosChange",self,"OnBeiShiSetPosChange")

    g_ScriptEvent:RemoveListener("OnRequestOpenAppearanceSettingWnd",self,"OnRequestOpenAppearanceSettingWnd")
end

function LuaAppearanceWnd:OnDestroy()
    self:ClearPreviewFashionInfo() --关闭窗口时清除预览信息
    if not self.m_CharacterModelRestored then
        self.m_CharacterModelRestored = true
        self:UpdateCharacterModelByDayNight(false, true)
    end
end

function LuaAppearanceWnd:OnSyncFashionability(value)
    self:UpdateFashionability()
end

function LuaAppearanceWnd:OnSendFashionTransformState()
    if not self:HasPreviewFashionHeadOrBody() then
        self:OnSyncMainPlayerAppearanceProp()
    end
end

function LuaAppearanceWnd:OnEvaluateFashionSuitSuccess_Permanent(suitId)
    self:UpdateFashionPreviewButtons()
end

function LuaAppearanceWnd:OnMainPlayerAppearanceUpdate()
    self:OnSyncMainPlayerAppearanceProp()
end

--仙凡切换
function LuaAppearanceWnd:OnSyncMainPlayerAppearanceProp()
    self:OnSyncMainPlayerAppearancePropWithForceReloadOption(true)
end

function LuaAppearanceWnd:OnSyncMainPlayerAppearancePropWithForceReloadOption(bForceReload)
    self:UpdateXianFanStatus()
    self:UpdateFashionPreviewButtons()
    --TODO 如何处理预览和仙凡切换的冲突
    local fakeAppear = LuaAppearancePreviewMgr:GetPreviewFashionInfo(self:GetPreviewHeadId(), self:GetPreviewBodyId(), self:GetPreviewWeaponId(), self:GetPreviewBackPendantId(), self.m_PreviewTransformOn)
    if self.m_DisableTransform then
        fakeAppear.ResourceId = 0
    end
    self.m_ModelPreviewMgr:PreviewMainPlayer(fakeAppear, 0, bForceReload)
    self:UpdateFashionIcon()
end

function LuaAppearanceWnd:OnRequestPreviewMainPlayerFashionOutfit(headId, bodyId, weaponId, backPendantId)
    LuaAppearancePreviewMgr.m_PreviewHeadFashionId = headId
    LuaAppearancePreviewMgr.m_PreviewBodyFashionId = bodyId
    LuaAppearancePreviewMgr.m_PreviewWeaponFashionId = weaponId
    LuaAppearancePreviewMgr.m_PreviewBackPendantFashionId = backPendantId
    local entiretyHeadId = LuaAppearancePreviewMgr:GetEntiretyHead(bodyId)
    if entiretyHeadId ~= 0 then
        LuaAppearancePreviewMgr.m_PreviewIsEntiretyHead = true
        LuaAppearancePreviewMgr.m_PreviewHeadFashionId = entiretyHeadId
    end   
    self:PreviewAllFashion()
end

function LuaAppearanceWnd:OnRequestPreviewMainPlayerFashion(pos, fashionId)
    if pos == EnumAppearanceFashionPosition.eHead then
        LuaAppearancePreviewMgr.m_PreviewHeadFashionId = fashionId
    elseif pos == EnumAppearanceFashionPosition.eBody then
        LuaAppearancePreviewMgr.m_PreviewBodyFashionId = fashionId
        local headId = LuaAppearancePreviewMgr:GetEntiretyHead(fashionId)
        if headId ~= 0 then
            LuaAppearancePreviewMgr.m_PreviewIsEntiretyHead = true
            LuaAppearancePreviewMgr.m_PreviewHeadFashionId = headId
        elseif LuaAppearancePreviewMgr.m_PreviewIsEntiretyHead then
            LuaAppearancePreviewMgr.m_PreviewIsEntiretyHead = false
            LuaAppearancePreviewMgr.m_PreviewHeadFashionId = 0
        end
    elseif pos == EnumAppearanceFashionPosition.eBeiShi then
        LuaAppearancePreviewMgr.m_PreviewBackPendantFashionId = fashionId
    else
        return
    end
    self:PreviewAllFashion()
end

function LuaAppearanceWnd:OnRequestPreviewMainPlayerFashionSuit(suitId, headId, bodyId)
    LuaAppearancePreviewMgr.m_PreviewHeadFashionId = headId
    LuaAppearancePreviewMgr.m_PreviewBodyFashionId = bodyId
    LuaAppearancePreviewMgr.m_PreviewFashionSuitId = suitId
    LuaAppearancePreviewMgr.m_PreviewIsEntiretyHead = LuaAppearancePreviewMgr:GetEntiretyHead(bodyId)>0
    self:PreviewAllFashion()
end

function LuaAppearanceWnd:OnRequestPreviewMainPlayerWing(wingId)
    local fakeAppear = CClientMainPlayer.Inst.AppearanceProp:Clone()
    fakeAppear.Wing = wingId
    fakeAppear.HideWing = 0
    fakeAppear.ResourceId = 0
    self.m_ModelPreviewMgr:PreviewMainPlayer(fakeAppear, 0, false)
end

function LuaAppearanceWnd:OnRequestPreviewMainPlayerIntensitySuit(suitFXId)
    local fakeAppear = CClientMainPlayer.Inst.AppearanceProp:Clone()
    fakeAppear.IntesifySuitId = suitFXId
    fakeAppear.HideSuitIdToOtherPlayer = 0
    fakeAppear.ResourceId = 0
    self.m_ModelPreviewMgr:PreviewMainPlayer(fakeAppear, 0, false)
end

function LuaAppearanceWnd:OnRequestPreviewMainPlayerGemGroup(id)
    local fakeAppear = CClientMainPlayer.Inst.AppearanceProp:Clone()
    fakeAppear.HideGemFxToOtherPlayer = (id==0 and 1 or 0)
    fakeAppear.ResourceId = 0
    self.m_ModelPreviewMgr:PreviewMainPlayer(fakeAppear, 0, false)
end

function LuaAppearanceWnd:OnRequestPreviewMainPlayerTalismanAppearance(talismanId)
    local fakeAppear = CClientMainPlayer.Inst.AppearanceProp:Clone()
    fakeAppear.TalismanAppearance = talismanId
    fakeAppear.HideTalismanAppearance = 0
    fakeAppear.ResourceId = 0
    self.m_ModelPreviewMgr:PreviewMainPlayer(fakeAppear, 0, false)
end

function LuaAppearanceWnd:OnRequestPreviewMainPlayerUmbrella(expressionId)
    self.m_ModelPreviewMgr:PreviewMainPlayerExpression(0, expressionId)
end

function LuaAppearanceWnd:OnRequestPreviewMainPlayerRanFaJi(ranfajiId)
    if ranfajiId>0 and CLuaRanFaMgr:GetMainPlayerVisibleHeadFashion()>0 then
        g_MessageMgr:ShowMessage("Appearance_Preview_RanFa_Need_Take_Off_Head_Fashion_Tip")
    else
        local fakeAppear = CClientMainPlayer.Inst.AppearanceProp:Clone()
        local designData = RanFaJi_RanFaJi.GetData(ranfajiId)

        if designData then
            fakeAppear.AdvancedHairColor = designData.ColorRGB
            fakeAppear.AdvancedHairColorId = ranfajiId
        end
        fakeAppear.ResourceId = 0
        self.m_ModelPreviewMgr:PreviewMainPlayer(fakeAppear, 0, false)
    end
end

--model preview
function LuaAppearanceWnd:UpdateXianFanStatus()
    local appearance = CClientMainPlayer.Inst.AppearanceProp
    local isFashionSupportXianShen = CClientMainPlayer.IsFashionSupportFeiSheng(appearance.HeadFashionId, appearance.BodyFashionId, appearance.HideHeadFashionEffect, appearance.HideBodyFashionEffect)
    self.m_XianFanSwitch.Selected = appearance.FeiShengAppearanceXianFanStatus>0
    self.m_XianFanSwitch.gameObject:SetActive(CClientMainPlayer.Inst and CClientMainPlayer.Inst.HasFeiSheng or false)
end

function LuaAppearanceWnd:InitModelPreviewer()
    self.m_ModelPreviewMgr:SetPreviewTexture(self.m_ModelPreviewMgr.transform:Find("Texture"):GetComponent(typeof(UITexture)))
    self.m_ModelPreviewMgr:SetScreenXOffset(self.m_ModelScreenXOffset) -- 取值0-1,0.5表示居中
    local mainplayer = CClientMainPlayer.Inst
    if mainplayer then
        local fakeAppear = mainplayer.AppearanceProp:Clone()
        self.m_ModelPreviewMgr:PreviewMainPlayer(fakeAppear, 0, true) --首次进入时，默认显示场景外观，如果已经变身就显示变身效果，之后根据具体的选项来决定实际外观
    end
    self.m_DayNightSwitch.Selected = true
    self.m_DayNightSwitch2.Selected = true
    self.m_DarkCorner:SetActive(false) --dark corner 只白天显示
    self.m_ModelPreviewMgr:ChangeDayNight(false) --默认晚上
    self:UpdateCharacterModelByDayNight(false, false)
    self:UpdateXianFanStatus()
end

function LuaAppearanceWnd:UpdateCharacterModelByDayNight(toDay, bRestoreToDefault)
    if bRestoreToDefault then
        CCharacterRenderMgr.Inst:RejectCharacterModel("AppearanceWnd")
    else
        CCharacterRenderMgr.Inst:ApplyCharacterModel("AppearanceWnd",toDay and "yichu" or "yichu_night")
    end
end
     
function LuaAppearanceWnd:OnFashionabilityClick()
    LuaAppearancePreviewMgr:ShowFashionabilityRankWnd()
end

function LuaAppearanceWnd:OnDayNightSwitchClick()
    self.m_DayNightSwitch.Selected = not self.m_DayNightSwitch.Selected
    self.m_DayNightSwitch2.Selected = self.m_DayNightSwitch.Selected
    self.m_DarkCorner:SetActive(not self.m_DayNightSwitch.Selected) --dark corner 只白天显示
    self.m_ModelPreviewMgr:ChangeDayNight(not self.m_DayNightSwitch.Selected)
    self:UpdateCharacterModelByDayNight(not self.m_DayNightSwitch.Selected, false)
end

function LuaAppearanceWnd:OnXianFanSwitchClick()
    self.m_XianFanSwitch.Selected = not self.m_XianFanSwitch.Selected
    LuaAppearancePreviewMgr:RequestChangeXianFanAppearance(self.m_XianFanSwitch.Selected)
end

function LuaAppearanceWnd:OnSettingButtonClick()
    LuaAppearancePreviewMgr:ShowSettingWnd(self.m_SettingButton.transform.position, self.m_SettingButton.transform:GetComponent(typeof(UIWidget)).width)
end

function LuaAppearanceWnd:OnAddtionButtonClick()
    self.m_AddtionRoot:SetActive(not self.m_AddtionRoot.activeSelf)
end

function LuaAppearanceWnd:OnResetButtonClick()
    self.m_ModelPreviewMgr:ResetCamera(false)
end

function LuaAppearanceWnd:OnTakeOffButtonClick()
    --DO Nothing
end

function LuaAppearanceWnd:OnExpressionButtonClick()
    local expressionId = self:GetCurrentFashionExpressionId()
    --monster变身时没有表情动作，需要借助ShowExpressionAction来播放, 这里必须走一次Preivew以便于清除一些状态
    local fakeAppear = LuaAppearancePreviewMgr:GetPreviewFashionInfo(self:GetPreviewHeadId(), self:GetPreviewBodyId(), self:GetPreviewWeaponId(), self:GetPreviewBackPendantId(), self.m_PreviewTransformOn)
    self.m_ModelPreviewMgr:PreviewMainPlayer(fakeAppear, 0, true)
    CClientMainPlayer.ShowExpressionAction(self.m_ModelPreviewMgr.RO, CClientMainPlayer.Inst.Gender, expressionId)
    local fashionData = Fashion_Fashion.GetData(self:GetCurrentBodyId())
    if fashionData and fashionData.SpecialExpressionShotAdjust~=0 then
        self.m_ModelPreviewMgr:SetZoomFactor(fashionData.SpecialExpressionShotAdjust)
    end
end

function LuaAppearanceWnd:OnTransformButtonClick()
    if not self:HasPreviewFashionHeadOrBody() then
        Gac2Gas.DoFashionTransform(not CClientMainPlayer.Inst.IsTransformOn)
        return
    end
    self.m_PreviewTransformOn = not self.m_PreviewTransformOn
    self.m_TransformButton:GetComponent(typeof(CButton)).Selected = self.m_PreviewTransformOn
    self.m_ExpressionButton:SetActive(not self:MainPlayerIsYingLingMonsterState() and self:GetCurrentFashionExpressionId()>0)
    self.m_FashionPreviewButtons:Reposition()
    local fakeAppear = LuaAppearancePreviewMgr:GetPreviewFashionInfo(self:GetPreviewHeadId(), self:GetPreviewBodyId(), self:GetPreviewWeaponId(), self:GetPreviewBackPendantId(), self.m_PreviewTransformOn)
    self.m_ModelPreviewMgr:PreviewMainPlayer(fakeAppear, 0, true)
end

function LuaAppearanceWnd:OnEvaluateButtonClick()
    if LuaAppearancePreviewMgr.m_PreviewFashionSuitId and LuaAppearancePreviewMgr.m_PreviewFashionSuitId > 0 then
        LuaAppearancePreviewMgr:ShowFashionEvaluationWnd(LuaAppearancePreviewMgr.m_PreviewFashionSuitId)
    end
end

function LuaAppearanceWnd:OnShareButtonClick()
    self.m_LeftArea:SetActive(false)
    self.m_RightArea:SetActive(false)
    self.m_ShareRoot:SetActive(true)
    self.m_ModelPreviewMgr:SetROVisibility(true)
    --布局
    self.m_ShareTable:Reposition()
    self.m_ShareScrollView:ResetPosition()
    local ok = xpcall(function() end, function(err) print("error occured", err) end) --加个xpcall避免分享报错阻塞界面
    CUICommonDef.CaptureScreen(
        "screenshot",
        false,
        false,
        nil,
        DelegateFactory.Action_string_bytes(
            function(fullPath, jpgBytes)
                ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, fullPath)
                self.m_LeftArea:SetActive(true)
                self.m_RightArea:SetActive(true)
                self.m_ShareRoot:SetActive(false)
                self.m_ModelPreviewMgr:SetROVisibility(not self.m_NeedHideRO)
            end
        ),
        false
    )
end

function LuaAppearanceWnd:OnTabenButtonClick()
    CUIManager.ShowUI("AppearanceFashionTabenWnd")
end

function LuaAppearanceWnd:OnHideUIButtonClick()
    self.m_LeftArea:SetActive(false)
    self.m_RightArea:SetActive(false)
    self.m_ShowUIButton:SetActive(true)
    self.m_DayNightSwitch2.gameObject:SetActive(true)
    self.m_ModelPreviewMgr:SetScreenXOffset(0.5)
end

function LuaAppearanceWnd:OnShowUIButtonClick()
    self.m_LeftArea:SetActive(true)
    self.m_RightArea:SetActive(true)
    self.m_ShowUIButton:SetActive(false)
    self.m_DayNightSwitch2.gameObject:SetActive(false)
    self.m_ModelPreviewMgr:SetScreenXOffset(self.m_ModelScreenXOffset)
end
--仿照原AppearancePreviewWnd的功能来处理背饰设置相关内容
function LuaAppearanceWnd:OpenBeiShiSetting()
    LuaAppearancePreviewMgr.IsBeiShiSetting = false
    self.m_LeftArea:SetActive(false)
    self.m_RightArea:SetActive(false)
    self.m_BeiShiSettingView.gameObject:SetActive(true)
    self.m_BeiShiSettingView:Init({})
    self.m_ModelPreviewMgr:SetROVisibility(true) --背饰调整期间强行显示模型
    self.m_ModelPreviewMgr:RotateToBack()
end
function LuaAppearanceWnd:CloseBeiShiSetting()
    LuaAppearancePreviewMgr.IsBeiShiSetting = false
    self.m_LeftArea:SetActive(true)
    self.m_RightArea:SetActive(true)
    self.m_BeiShiSettingView.gameObject:SetActive(false)
    self.m_ModelPreviewMgr:SetROVisibility(not self.m_NeedHideRO)
end
function LuaAppearanceWnd:OnBeiShiSetPosChange(x, y, z)
    local fxOffsetPos = LuaAppearancePreviewMgr:GetBeiShiPos(x, y, z)
    self.m_BackPendantPosData[1] = math.floor(x*255-128)
    self.m_BackPendantPosData[2] = math.floor(y*255-128)
    self.m_BackPendantPosData[3] = math.floor(z*255-128)
    self.m_ModelPreviewMgr:SetBackPendantPos(fxOffsetPos, self.m_BackPendantPosData)
end

function LuaAppearanceWnd:OnRequestOpenAppearanceSettingWnd()
    self:OnSettingButtonClick()
end

function LuaAppearanceWnd:GetCurrentHeadId()
    return LuaAppearancePreviewMgr.m_PreviewHeadFashionId>0 and LuaAppearancePreviewMgr.m_PreviewHeadFashionId or 
        (CClientMainPlayer.Inst and CClientMainPlayer.Inst.AppearanceProp.HeadFashionId or 0)
end

function LuaAppearanceWnd:GetCurrentBodyId()
    return LuaAppearancePreviewMgr.m_PreviewBodyFashionId>0 and LuaAppearancePreviewMgr.m_PreviewBodyFashionId or 
        (CClientMainPlayer.Inst and CClientMainPlayer.Inst.AppearanceProp.BodyFashionId or 0)
end

function LuaAppearanceWnd:GetCurrentWeaponId()
    return LuaAppearancePreviewMgr.m_PreviewWeaponFashionId>0 and LuaAppearancePreviewMgr.m_PreviewWeaponFashionId or
        (CClientMainPlayer.Inst and CClientMainPlayer.Inst.AppearanceProp.WeaponFashionId or 0)
end

function LuaAppearanceWnd:GetCurrentBackPendantId()
    return LuaAppearancePreviewMgr.m_PreviewBackPendantFashionId>0 and LuaAppearancePreviewMgr.m_PreviewBackPendantFashionId or
        (CClientMainPlayer.Inst and CClientMainPlayer.Inst.AppearanceProp.BackPendantFashionId or 0)
end

function LuaAppearanceWnd:GetPreviewHeadId()
    return LuaAppearancePreviewMgr.m_PreviewHeadFashionId
end

function LuaAppearanceWnd:GetPreviewBodyId()
    return LuaAppearancePreviewMgr.m_PreviewBodyFashionId
end

function LuaAppearanceWnd:GetPreviewWeaponId()
    return LuaAppearancePreviewMgr.m_PreviewWeaponFashionId
end

function LuaAppearanceWnd:GetPreviewBackPendantId()
    return LuaAppearancePreviewMgr.m_PreviewBackPendantFashionId
end

function LuaAppearanceWnd:HasPreviewFashionHeadOrBody()
    return LuaAppearancePreviewMgr.m_PreviewHeadFashionId>0 or LuaAppearancePreviewMgr.m_PreviewBodyFashionId>0
end

function LuaAppearanceWnd:GetCurrentFashionExpressionId()
    return self.m_PreviewTransformOn and self.m_PreviewTransformExpressionId or self.m_PreviewSpecialExpressionId
end

function LuaAppearanceWnd:ClearPreviewFashionInfo()
    LuaAppearancePreviewMgr.m_PreviewHeadFashionId = 0
    LuaAppearancePreviewMgr.m_PreviewBodyFashionId = 0
    LuaAppearancePreviewMgr.m_PreviewWeaponFashionId = 0
    LuaAppearancePreviewMgr.m_PreviewBackPendantFashionId = 0
    LuaAppearancePreviewMgr.m_PreviewFashionSuitId = 0
    LuaAppearancePreviewMgr.m_PreviewIsEntiretyHead = false
    self.m_PreviewTransformMonsterId = 0
    self.m_PreviewTransformExpressionId = 0
    self.m_PreviewSpecialExpressionId = 0
    self:UpdateFashionPreviewButtons()
end

function LuaAppearanceWnd:MainPlayerIsYingLingMonsterState()
    return CClientMainPlayer.Inst and CClientMainPlayer.Inst.AppearanceProp.YingLingState==EnumToInt(EnumYingLingState.eMonster)
end

function LuaAppearanceWnd:UpdateFashionPreviewButtons()
    self.m_TakeOffButton:SetActive(false) --功能暂时不开放
    local mainplayer = CClientMainPlayer.Inst
    if mainplayer==nil then return end
    if not self:HasPreviewFashionHeadOrBody() then
        self.m_PreviewTransformOn = CClientMainPlayer.Inst.IsTransformOn
    else
        self.m_PreviewTransformOn = false
    end
    local headData = Fashion_Fashion.GetData(self:GetCurrentHeadId())
    local bodyData = Fashion_Fashion.GetData(self:GetCurrentBodyId())
    self.m_PreviewTransformMonsterId, self.m_PreviewTransformExpressionId, self.m_PreviewSpecialExpressionId = LuaAppearancePreviewMgr:GetFashionSuitTransformAndExpressionInfo(headData, bodyData)
    self.m_TransformButton:SetActive(self.m_PreviewTransformMonsterId>0)
    self.m_TransformButton:GetComponent(typeof(CButton)).Selected = self.m_PreviewTransformOn
    self.m_ExpressionButton:SetActive((self.m_PreviewTransformOn and self.m_PreviewTransformExpressionId>0) or self.m_PreviewSpecialExpressionId>0)
    if self:MainPlayerIsYingLingMonsterState() then
        self.m_ExpressionButton:SetActive(false)
    end
    self.m_EvaluateButton:SetActive(self:IsFashionSuitSubviewSelected() and LuaAppearancePreviewMgr.m_PreviewFashionSuitId>0 and not LuaAppearancePreviewMgr:IsFashionSuitEvaluated(LuaAppearancePreviewMgr.m_PreviewFashionSuitId))
    
    self.m_FashionPreviewButtons:Reposition()
end

-- 仿照CFashionPreviewTextureLoader
function LuaAppearanceWnd:PreviewAllFashion()
    self:UpdateFashionPreviewButtons() --这里会更新变身信息

    local mainplayer = CClientMainPlayer.Inst
    if mainplayer==nil then return end

    local fakeAppear = LuaAppearancePreviewMgr:GetPreviewFashionInfo(self:GetPreviewHeadId(), self:GetPreviewBodyId(), self:GetPreviewWeaponId(), self:GetPreviewBackPendantId(), self.m_PreviewTransformOn)
    fakeAppear.ResourceId = 0 --预览时不显示变身状态
    self.m_ModelPreviewMgr:PreviewMainPlayer(fakeAppear, 0, false)
    self:InitShareContent()
end

function LuaAppearanceWnd:ResetAppearance()
    self:OnSyncMainPlayerAppearancePropWithForceReloadOption(false)
end

function LuaAppearanceWnd:UpdatePreviewFashionSetting(bShowFashionPreviewButtons, bKeepFashionPreviewInfo, bDisableTransform)
    self.m_FashionPreviewButtons.gameObject:SetActive(bShowFashionPreviewButtons)
    self.m_DisableTransform = bDisableTransform
    if not bKeepFashionPreviewInfo then
        self:ClearPreviewFashionInfo()
        self:ResetAppearance()
    else
        self:UpdateFashionPreviewButtons()
    end
end
--入口已经去掉，该方法作废
-- function LuaAppearanceWnd:ShowPinchFace()     
--     --大放老师的CharacterModel存在时序问题，捏脸界面也用到了，暂时进行绕过处理，在打开捏脸界面前先把衣橱的角色参数还原                                        
--     self.m_CharacterModelRestored = true
--     self:UpdateCharacterModelByDayNight(false, true)
--     LuaPinchFaceMgr:OpenPinchFaceModifyWnd()
--     self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
-- end

function LuaAppearanceWnd:ShowSubview(subviewName, hideRO, bgPosY, bgHeight, ...)    
    local childCount = self.m_DefaultHideViewsRoot.childCount
    for i=0,childCount-1 do
        self.m_DefaultHideViewsRoot:GetChild(i).gameObject:SetActive(false)
    end

    self.m_ContentTable.gameObject:SetActive(false)
    self.m_ContentGrid.gameObject:SetActive(false)
    self.m_FashionContentGrid.gameObject:SetActive(false)

    -- 是否隐藏预览的角色
    self.m_NeedHideRO = hideRO
    self.m_ModelPreviewMgr:SetROVisibility(not hideRO)

    --根据不同subview自适应scrollview范围
    local pos = self.m_ScrollViewBg.transform.localPosition
    pos.y = bgPosY
    self.m_ScrollViewBg.transform.localPosition = pos
    self.m_ScrollViewBg.height = bgHeight
    self.m_ScrollView:GetComponent(typeof(UIPanel)):ResetAndUpdateAnchors()
    self.m_ScrollViewIndicator:Layout()

    --从打伞页面切到其他页面时，重置外观，去掉开伞 以下代码通过UpdatePreviewFashionSetting中的ResetAppearance可以替代
    -- if self.m_LastActiveSubview  and self.m_LastActiveSubview == "AppearanceUmbrellaSubview" or self.m_LastActiveSubview == "AppearanceClosetUmbrellaSubview" then
    --     self:ResetAppearance()
    -- end
    -- self.m_LastActiveSubview = subviewName

    for i=1,#self.m_AllSubviews do
        local subviewGo = self.m_SubviewRoot:Find(self.m_AllSubviews[i]).gameObject
        if subviewName==self.m_AllSubviews[i] then
            subviewGo:SetActive(true)
            subviewGo:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf:Init(...)
        else
            subviewGo:SetActive(false)
        end
    end
end

--时装图标更新

function LuaAppearanceWnd:UpdateFashionIcon()
    local mainplayer = CClientMainPlayer.Inst
    
    local headId = (mainplayer and mainplayer.AppearanceProp.HeadFashionId or 0)
    local bodyId = (mainplayer and mainplayer.AppearanceProp.BodyFashionId or 0)
    local weaponId = (mainplayer and mainplayer.AppearanceProp.WeaponFashionId or 0)
    local backPendantId = (mainplayer and mainplayer.AppearanceProp.BackPendantFashionId or 0)
    local fashionIdTbl = {headId, bodyId, weaponId, backPendantId}
    local hideHead = mainplayer and mainplayer.AppearanceProp.HideHeadFashionEffect>0 or false
    local hideBody = mainplayer and mainplayer.AppearanceProp.HideBodyFashionEffect>0 or false
    local hideWeapon = mainplayer and mainplayer.AppearanceProp.HideWeaponFashionEffect>0 or false
    local hideBackPendant = mainplayer and mainplayer.AppearanceProp.HideBackPendantFashionEffect>0 or false
    local hideTbl ={hideHead, hideBody, hideWeapon, hideBackPendant}

    local childCount = self.m_FashionPartTable.transform.childCount
    for i=0,childCount-1 do
        local child = self.m_FashionPartTable.transform:GetChild(i)
        child:Find("Icon"):GetComponent(typeof(CUITexture)):LoadMaterial(LuaAppearancePreviewMgr:GetFashionIconById(i, fashionIdTbl[i+1])) 
        child:Find("DisableSprite").gameObject:SetActive(hideTbl[i+1])
    end

    self:InitShareContent()
end

function LuaAppearanceWnd:InitShareContent()
    local mainplayer = CClientMainPlayer.Inst
    if mainplayer==nil then return end
    self.m_SharePlayerInfo.transform:Find("Icon"):GetComponent(typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(mainplayer.Class, mainplayer.Gender, -1), false)
    self.m_SharePlayerInfo.transform:Find("Name/Label"):GetComponent(typeof(UILabel)).text = mainplayer.Name
    self.m_SharePlayerInfo.transform:Find("Icon/LevelLabel"):GetComponent(typeof(UILabel)).text = tostring(mainplayer.Level)
    self.m_SharePlayerInfo.transform:Find("ID/Label"):GetComponent(typeof(UILabel)).text = tostring(mainplayer.Id)
    self.m_SharePlayerInfo.transform:Find("Server/Label"):GetComponent(typeof(UILabel)).text = mainplayer:GetMyServerName()

    local fashionTbl = {}
    local headId = self:GetCurrentHeadId()
    local bodyId = self:GetCurrentBodyId()
    local weaponId = self:GetCurrentWeaponId()
    local backPendantId = self:GetCurrentBackPendantId()
    
    local hideHead =  mainplayer and mainplayer.AppearanceProp.HideHeadFashionEffect>0 or false
    local hideBody = mainplayer and mainplayer.AppearanceProp.HideBodyFashionEffect>0 or false
    local hideWeapon = mainplayer and mainplayer.AppearanceProp.HideWeaponFashionEffect>0 or false
    local hideBackPendant = mainplayer and mainplayer.AppearanceProp.HideBackPendantFashionEffect>0 or false

    local tmpTbl = {}
    if LuaAppearancePreviewMgr.m_PreviewHeadFashionId>0 or not hideHead then table.insert(tmpTbl, {id=headId,pos=EnumAppearanceFashionPosition.eHead}) end --预览状态，或者非预览状态下没有隐藏，则显示
    if LuaAppearancePreviewMgr.m_PreviewBodyFashionId>0 or not hideBody then table.insert(tmpTbl, {id=bodyId,pos=EnumAppearanceFashionPosition.eBody}) end
    if LuaAppearancePreviewMgr.m_PreviewWeaponFashionId>0 or not hideWeapon then table.insert(tmpTbl, {id=weaponId,pos=EnumAppearanceFashionPosition.eWeapon}) end
    if LuaAppearancePreviewMgr.m_PreviewBackPendantFashionId>0 or not hideBackPendant then table.insert(tmpTbl, {id=backPendantId,pos=EnumAppearanceFashionPosition.eBeiShi}) end
    for _,data in pairs(tmpTbl) do
        local fashionId = data.id
        local pos = data.pos
        if fashionId>0 then
            local isTaben = EquipmentTemplate_Equip.Exists(fashionId)
            local quality = isTaben and 0 or Fashion_Fashion.GetData(fashionId).Quality
            table.insert(fashionTbl, {id=fashionId, icon=LuaAppearancePreviewMgr:GetFashionIconById(pos, fashionId),
                         positionName=LuaAppearancePreviewMgr:GetFashionPositionName(pos),
                         name=LuaAppearancePreviewMgr:GetFashionNameById(fashionId), qualitySprite=LuaAppearancePreviewMgr:GetFashionQualityBorder(quality),
                         isTry=not LuaAppearancePreviewMgr:IsCurrentInUseFashion(pos, fashionId) })
        end
    end

    local childCount = self.m_ShareTable.transform.childCount
    for i=0,childCount-1 do
        self.m_ShareTable.transform:GetChild(i).gameObject:SetActive(false)
    end

    for i = 1, #fashionTbl do
        local child = i<=childCount and self.m_ShareTable.transform:GetChild(i-1).gameObject or CUICommonDef.AddChild(self.m_ShareTable.gameObject, self.m_ShareFashionItem)
        child:SetActive(true)
        child.transform:Find("Item"):GetComponent(typeof(CUITexture)):LoadMaterial(fashionTbl[i].icon)
        child.transform:Find("NameLabel"):GetComponent(typeof(UILabel)).text = fashionTbl[i].name
        child.transform:Find("Item/Border"):GetComponent(typeof(UISprite)).spriteName = fashionTbl[i].qualitySprite
        child.transform:Find("PositionLabel"):GetComponent(typeof(UILabel)).text = fashionTbl[i].positionName
        child.transform:Find("Try").gameObject:SetActive(fashionTbl[i].isTry)
    end
    --排序放在分享的时候进行，避免deactive状态reposition计算不正常
end

function LuaAppearanceWnd:GetFirstCanUnlockFashionItem()
    local subView = self.m_SubviewRoot:Find("AppearanceClosetFashionSubview")
    if subView and subView.gameObject.activeSelf then
        local go = subView:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf:GetGuideGoByAppearanceWnd()
        return go
    end
    return nil
end


function LuaAppearanceWnd:GetGuideGo(methodName)
    local selectedIndex = self.m_GroupTableView.SelectedIndex
    local row = selectedIndex and selectedIndex.row or -1
    local section = selectedIndex and selectedIndex.section or -1
    local tab = self.m_TabBar.SelectedIndex
    if methodName == "GetTab0" then
        return self.m_TabBar.transform:GetChild(0).gameObject
    elseif methodName=="GetTab1" then
        return self.m_TabBar.transform:GetChild(1).gameObject
    elseif methodName=="GetSettingButton" then
        return self.m_SettingButton.gameObject
    elseif methodName=="GetPreviewItem" then
        return self.m_FashionContentGrid.transform:GetChild(0).gameObject
    elseif methodName=="GetGetButton" then
        return self.m_FashionContentGrid.transform:GetChild(0).gameObject
    elseif methodName=="GetDaPeiItem" then--搭配
        if tab==0 and section==0 and CGuideMgr.Inst:IsInPhase(EnumGuideKey.AppearanceDaPei) then
            return self.m_ContentTable.transform:GetChild(0).gameObject
        end
    elseif methodName=="GetSaveDaPeiButton" then
        if tab==0 and section==0 and CGuideMgr.Inst:IsInPhase(EnumGuideKey.AppearanceDaPei) then
            return self.m_SubviewRoot:Find("AppearanceClosetMyOutfitSubview/SaveButton").gameObject
        end
    elseif methodName=="GetTaBenItem" then--拓本
        if tab==0 and section==5 and row==0 and CGuideMgr.Inst:IsInPhase(EnumGuideKey.AppearanceTaBen) then
            return self.m_ContentTable.transform:GetChild(0).gameObject
        end
    elseif methodName=="GetFirstFashionUnlockItem" then--解锁时装的时候，获取第一个可解锁
        if tab==0 and section==1 and CGuideMgr.Inst:IsInPhase(EnumGuideKey.AppearanceUnlockFashion) then
            return self:GetFirstCanUnlockFashionItem()
        end
    elseif methodName=="GetSkillAppearanceItem" then--技能外观
        if tab==0 and section==3 and row==2 and CGuideMgr.Inst:IsInPhase(EnumGuideKey.AppearanceSkillAppearance) then
            return self.m_ContentTable.transform:GetChild(0).gameObject
        end
    elseif methodName=="GetSkillAppearanceSwitchButton" then--
        if tab==0 and section==3 and row==2 and CGuideMgr.Inst:IsInPhase(EnumGuideKey.AppearanceSkillAppearance) then
            return self.m_DefaultHideViewsRoot:Find("CommonHeaderSwitch").gameObject
        end
    end
    return nil
end

