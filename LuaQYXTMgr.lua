local CScene = import "L10.Game.CScene"

CLuaQYXTMgr = class()

CLuaQYXTMgr.signUpStatus = false
CLuaQYXTMgr.remainTimes = 0
CLuaQYXTMgr.lastStage = 0
CLuaQYXTMgr.curSatge = 1
CLuaQYXTMgr.isEnd = 0
CLuaQYXTMgr.battleInfoTblUD = nil
CLuaQYXTMgr.RefreshResultWndNotShow = false
CLuaQYXTMgr.SignUpWndNotShow = false
CLuaQYXTMgr.ForceOpen = false

function CLuaQYXTMgr.IsInPlay()
    if CScene.MainScene then
        local gamePlayId = CScene.MainScene.GamePlayDesignId
        if gamePlayId == 51101414 then
            return true
        end
    end
    return false
end
