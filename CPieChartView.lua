-- Auto Generated!!
local CLogMgr = import "L10.CLogMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CPieChartAnnotation = import "L10.UI.CPieChartAnnotation"
local CPieChartView = import "L10.UI.CPieChartView"
local CUICommonDef = import "L10.UI.CUICommonDef"
local Extensions = import "Extensions"
local GameObject = import "UnityEngine.GameObject"
local GuildMemberExportMgr = import "L10.Game.GuildMemberExportMgr"
local System = import "System"
local Texture2D = import "UnityEngine.Texture2D"
local TextureFormat = import "UnityEngine.TextureFormat"
CPieChartView.m_Init_CS2LuaHook = function (this, values, annotations) 
    if values == nil or values.Length == 0 or values.Length ~= annotations.Length then
        return
    end

    if this.tex == nil then
        this.tex = CreateFromClass(Texture2D, 256, 256, TextureFormat.RGBA32, false)
        this.texture.mainTexture = this.tex
    end

    --int[] indexes = new int[values.Length];
    --for(int i=0;i<indexes.Length;++i)
    --    indexes[i] = i;

    --System.Array.Sort(indexes, (i,j)=>{
    --    return values[i].CompareTo(values[j]);
    --});

    --System.Array.Sort(values, (v1, v2) =>
    --{
    --    return v1.CompareTo(v2);
    --});

    local originValues = CreateFromClass(MakeArrayClass(System.Single), values.Length)
    CommonDefs.Array_Copy_Array_Array_Int32(values, originValues, values.Length)

    local totalVal = values[0]
    do
        local i = 1
        while i < values.Length do
            if values[i] < 0 then
                CLogMgr.LogError("values cannot be negative!")
                return
            end
            local tmp = totalVal
            totalVal = totalVal + values[i]
            values[i] = values[i] + tmp
            i = i + 1
        end
    end

    if totalVal == 0 then
        do
            local i = 0
            while i < values.Length do
                values[i] = i + 1
                i = i + 1
            end
        end
        totalVal = values.Length
    end

    do
        local i = 0
        while i < values.Length do
            values[i] = (values[i] / totalVal) * math.pi * 2
            i = i + 1
        end
    end

    GuildMemberExportMgr.Inst:GeneratePieTexture(values, this.tex)

    Extensions.RemoveAllChildren(this.table.transform)
    do
        local i = 0
        while i < values.Length do
            local instance = CUICommonDef.AddChild(this.table.gameObject, this.annotationTemplate)
            instance:SetActive(true)
            local annotation = System.String.Format("{0}({1})", annotations[i], originValues[i])
            CommonDefs.GetComponent_GameObject_Type(instance, typeof(CPieChartAnnotation)):Init(GuildMemberExportMgr.Inst:GetChartColor(i), annotation)
            i = i + 1
        end
    end
    this.table:Reposition()
end
CPieChartView.m_OnDestroy_CS2LuaHook = function (this) 
    if this.texture.mainTexture ~= nil then
        GameObject.Destroy(this.texture.mainTexture)
        this.texture.mainTexture = nil
    end
end
