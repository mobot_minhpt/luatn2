-- Auto Generated!!
local CFriendGroupMenu = import "L10.UI.CFriendGroupMenu"
local CFriendGroupMenuMgr = import "L10.UI.CFriendGroupMenuMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local Extensions = import "Extensions"
local GameObject = import "UnityEngine.GameObject"
local NGUIMath = import "NGUIMath"
local NGUITools = import "NGUITools"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local UIPanel = import "UIPanel"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CFriendGroupMenu.m_LoadItems_CS2LuaHook = function (this) 

    if CFriendGroupMenuMgr.Inst.Texts == nil or CFriendGroupMenuMgr.Inst.Texts.Length == 0 then
        CFriendGroupMenuMgr.Inst:Close()
        return
    end
    Extensions.RemoveAllChildren(this.table.transform)
    this.buttons = CreateFromClass(MakeArrayClass(GameObject), CFriendGroupMenuMgr.Inst.Texts.Length)
    do
        local i = 0
        while i < CFriendGroupMenuMgr.Inst.Texts.Length do
            local instance = NGUITools.AddChild(this.table.gameObject, this.itemTemplate)
            instance:SetActive(true)
            --GetComponentInChildren要求active
            CommonDefs.GetComponentInChildren_GameObject_Type(instance, typeof(UILabel)).text = CFriendGroupMenuMgr.Inst.Texts[i]
            UIEventListener.Get(instance).onClick = MakeDelegateFromCSFunction(this.OnMenuItemClick, VoidDelegate, this)
            this.buttons[i] = instance
            i = i + 1
        end
    end
    this.table:Reposition()
    local b = NGUIMath.CalculateRelativeWidgetBounds(this.table.transform)
    this.background.height = math.floor(math.min(this.m_MaxBgHeight, (b.size.y + this.table.padding.y * 2 + this.m_VerticalGap * 2)))
    CommonDefs.GetComponent_Component_Type(this.scrollView, typeof(UIPanel)):ResetAndUpdateAnchors()
    this.scrollView:ResetPosition()
end
CFriendGroupMenu.m_OnMenuItemClick_CS2LuaHook = function (this, go) 
    do
        local i = 0
        while i < this.buttons.Length do
            if this.buttons[i]:Equals(go) then
                if CFriendGroupMenuMgr.Inst.Listener ~= nil then
                    CFriendGroupMenuMgr.Inst.Listener:OnMenuItemClick(i)
                end
                CFriendGroupMenuMgr.Inst:Close()
                break
            end
            i = i + 1
        end
    end
end
CFriendGroupMenu.m_OnEnable_CS2LuaHook = function (this) 
    if CFriendGroupMenuMgr.Inst.Listener ~= nil then
        CFriendGroupMenuMgr.Inst.Listener:OnMenuAppear()
    end
end
CFriendGroupMenu.m_OnDisable_CS2LuaHook = function (this) 
    if CFriendGroupMenuMgr.Inst.Listener ~= nil then
        CFriendGroupMenuMgr.Inst.Listener:OnMenuDisappear()
    end
end

CFriendGroupMenuMgr.m_ShowOrCloseMenu_CS2LuaHook = function (this, defaultSelectedIndex, texts, listener) 

    if CUIManager.IsLoaded(CUIResources.FriendGroupMenu) then
        CUIManager.CloseUI(CUIResources.FriendGroupMenu)
        return
    end

    this.m_Texts = CommonDefs.ListToArray(texts)
    this.m_Listener = listener
    CUIManager.ShowUI(CUIResources.FriendGroupMenu)
end
