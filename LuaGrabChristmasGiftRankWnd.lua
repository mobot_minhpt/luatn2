local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local UIGrid = import "UIGrid"

CLuaGrabChristmasGiftRankWnd = class()

RegistClassMember(CLuaGrabChristmasGiftRankWnd,"TableView")
RegistClassMember(CLuaGrabChristmasGiftRankWnd,"AwardItem")
RegistClassMember(CLuaGrabChristmasGiftRankWnd,"MyRankItem")
RegistClassMember(CLuaGrabChristmasGiftRankWnd,"dressHat")
RegistClassMember(CLuaGrabChristmasGiftRankWnd,"AwardRankId")

function CLuaGrabChristmasGiftRankWnd:Awake()
    self.TableView = self.transform:Find("TableView"):GetComponent(typeof(QnAdvanceGridView))
    self.AwardItem = self.transform:Find("Award").gameObject
    self.MyRankItem = self.transform:Find("MainPlayerInfo").gameObject
end

function CLuaGrabChristmasGiftRankWnd:Init()
    local data = ShengDan_GiftBattle.GetData()
    self.dressHat = {}
    self.AwardRankId = {}
    for i=0,data.TransformIcon.Length-1,2 do
        local index = tonumber(data.TransformIcon[i])
        self.dressHat[index] = data.TransformIcon[i+1]
    end
    for _,highRank,mailId in string.gmatch(data.AwardMailId, "(%d+),(%d+),(%d+)") do
        local t = {}
        t.highRank = tonumber(highRank)
        t.mailId = tonumber(mailId)
        table.insert(self.AwardRankId,t)
    end

    if LuaGrabChristmasGiftMgr.RefreshRankWndNotShow == true then
        LuaGrabChristmasGiftMgr.RefreshRankWndNotShow = false
        if not LuaGrabChristmasGiftMgr.RankDataTable or not next (LuaGrabChristmasGiftMgr.RankDataTable) then
            return
        end
        self.MyRankItem:SetActive(true)
        if next(LuaGrabChristmasGiftMgr.RankDataTable) then  
            self.TableView.m_DataSource = DefaultTableViewDataSource.Create(function()   
                return #LuaGrabChristmasGiftMgr.RankDataTable
            end,
            function(item, index)
                self:InitItem(item.gameObject, index+1)
            end)
            self.TableView:ReloadData(true, false)
        end
        self:InitItemInfo(self.MyRankItem, LuaGrabChristmasGiftMgr.MyDataIndex, LuaGrabChristmasGiftMgr.MyData)
    end
end

function CLuaGrabChristmasGiftRankWnd:OnRefreshRankView()
    self.TableView:ReloadData(true, false)
    self:InitItemInfo(self.MyRankItem, LuaGrabChristmasGiftMgr.MyDataIndex, LuaGrabChristmasGiftMgr.MyData)
end


function CLuaGrabChristmasGiftRankWnd:InitItem(obj, index)
    if not index then
        return
    end
    local data = LuaGrabChristmasGiftMgr.RankDataTable[index]
    if not data then
        return
    end

    if index%2 == 0 then
        obj.transform:Find("bg1").gameObject:SetActive(true)
        obj.transform:Find("bg2").gameObject:SetActive(false)
    else
        obj.transform:Find("bg1").gameObject:SetActive(false)
        obj.transform:Find("bg2").gameObject:SetActive(true)
    end
     
    self:InitItemInfo(obj, index, data)
end

function CLuaGrabChristmasGiftRankWnd:InitItemInfo(obj, index, data)
    if not (obj and index and data) then
        return
    end
    local rank = index
    obj.transform:Find("RankLabel"):GetComponent(typeof(UILabel)).text = rank
    local path = "UI/Texture/Transparent/Material/"
    local huangGuan = {"grabchristmasgiftrankwnd_huangguang_01.mat","grabchristmasgiftrankwnd_huangguang_02.mat","grabchristmasgiftrankwnd_huangguang_03.mat"}
    local rankTexture = obj.transform:Find("RankLabel/Texture"):GetComponent(typeof(CUITexture))
    if rank == 1 or rank == 2 or rank == 3 then
        rankTexture:LoadMaterial(path..huangGuan[rank])
        rankTexture.gameObject:SetActive(true)
    else
        rankTexture.gameObject:SetActive(false)
    end
    --dress
    local dress = obj.transform:Find("Dress"):GetComponent(typeof(CUITexture))
    dress:LoadMaterial(self.dressHat[data.choice])
    --dress:LoadMaterial
    obj.transform:Find("NameLabel"):GetComponent(typeof(UILabel)).text = data.playerName
    obj.transform:Find("CountLabel"):GetComponent(typeof(UILabel)).text = data.giftNum

    local mailId
    local mailRank = data.rank
    for _,v in pairs(self.AwardRankId) do
        if mailRank <= v.highRank then
            mailId = v.mailId
            break
        end
    end

    local awardGrid = obj.transform:Find("AwardGrid").gameObject
    Extensions.RemoveAllChildren(awardGrid.transform)
    if data.cangGetAward then
        if mailId then      
            for awardId,count,_ in string.gmatch(Mail_Mail.GetData(mailId).Items, "(%d+),(%d+),(%d+)") do
                local award = NGUITools.AddChild(awardGrid,self.AwardItem)
                award:SetActive(true)
                local iconName = Item_Item.GetData(tonumber(awardId)).Icon
                local texture = award.transform:Find("Texture"):GetComponent(typeof(CUITexture))
                texture:LoadMaterial(iconName)
                UIEventListener.Get(texture.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
                    CItemInfoMgr.ShowLinkItemTemplateInfo(tonumber(awardId))
                end)
                award.transform:Find("Label"):GetComponent(typeof(UILabel)).text = tonumber(count)
            end
        end
        awardGrid.transform:GetComponent(typeof(UIGrid)):Reposition()
    end
end

function CLuaGrabChristmasGiftRankWnd:OnEnable()
    g_ScriptEvent:AddListener("RefreshGiftBattleRankView", self, "OnRefreshRankView")
end

function CLuaGrabChristmasGiftRankWnd:OnDisable()
    g_ScriptEvent:RemoveListener("RefreshGiftBattleRankView", self, "OnRefreshRankView")
end
