-- Auto Generated!!
local AlignType = import "L10.UI.CPopupMenuInfoMgr+AlignType"
local Boolean = import "System.Boolean"
local CommonDefs = import "L10.Game.CommonDefs"
local CPlayerShop_TalismanSearch = import "L10.UI.CPlayerShop_TalismanSearch"
local CPlayerShopMgr = import "L10.UI.CPlayerShopMgr"
local CPlayerShopPopupMenu = import "L10.UI.CPlayerShopPopupMenu"
local CPlayerShopPopupMenuInfoMgr = import "L10.UI.CPlayerShopPopupMenuInfoMgr"
local CYuanbaoMarketMgr = import "L10.UI.CYuanbaoMarketMgr"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"
local EventManager = import "EventManager"
local Int32 = import "System.Int32"
local ItemSearchParams = import "L10.UI.ItemSearchParams"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local MsgPackImpl = import "MsgPackImpl"
local Object = import "System.Object"
local QnButton = import "L10.UI.QnButton"
local SearchOption = import "L10.UI.SearchOption"
local String = import "System.String"
local UILabel = import "UILabel"
local UInt32 = import "System.UInt32"
CPlayerShop_TalismanSearch.m_Init_CS2LuaHook = function (this, publicity) 
    this.IsPublicity = publicity
    this:ReloadLevelData()
    this:LevelAction(this.SelectLevel)

    CYuanbaoMarketMgr.EquipSearchParams = nil
end
CPlayerShop_TalismanSearch.m_OnEnable_CS2LuaHook = function (this) 
    this.m_LevelButton.OnClick = MakeDelegateFromCSFunction(this.OnLevelButtonOnClick, MakeGenericClass(Action1, QnButton), this)
    this.m_TypeButton.OnClick = MakeDelegateFromCSFunction(this.OnTypeButtonOnClick, MakeGenericClass(Action1, QnButton), this)
    this.m_ColorButton.OnClick = MakeDelegateFromCSFunction(this.OnColorButtonOnClick, MakeGenericClass(Action1, QnButton), this)
    this.m_EquipNameButton.OnClick = MakeDelegateFromCSFunction(this.OnEquipNameButtonOnClick, MakeGenericClass(Action1, QnButton), this)
    this.m_SearchButton.OnClick = MakeDelegateFromCSFunction(this.OnSearchButtonOnClick, MakeGenericClass(Action1, QnButton), this)
    if this.m_FocusButton ~= nil then
        this.m_FocusButton.OnClick = MakeDelegateFromCSFunction(this.OnFocusButtonOnClick, MakeGenericClass(Action1, QnButton), this)
    end
    EventManager.AddListenerInternal(EnumEventType.QueryPlayerShopOnShelfNumResult, MakeDelegateFromCSFunction(this.OnQueryPlayerShopOnShelfNumResult, MakeGenericClass(Action2, MakeGenericClass(List, UInt32), Boolean), this))
end
CPlayerShop_TalismanSearch.m_ReloadLevelData_CS2LuaHook = function (this)
    this.m_LevelActions = CPlayerShopMgr.Inst:ConvertOptions(CPlayerShopMgr.Inst:GetTalismanLevelGroupNames(this.IsPublicity), MakeDelegateFromCSFunction(this.LevelAction, MakeGenericClass(Action1, Int32), this))

    if CYuanbaoMarketMgr.EquipSearchParams ~= nil then
        local level = CYuanbaoMarketMgr.EquipSearchParams.Level
        if level <= 30 then
            this.SelectLevel = 0
        elseif level <= 60 then
            this.SelectLevel = 1
        elseif level <= 90 then
            this.SelectLevel = 2
        elseif level <= 120 then
            this.SelectLevel = 3
        else
            this.SelectLevel = 4
        end
    end

    if this.m_LevelActions.Count > 0 then
        this.SelectLevel = math.min(math.max(this.SelectLevel, 0), this.m_LevelActions.Count - 1)
        this.m_LevelButton.Text = this.m_LevelActions[this.SelectLevel].text
    end
end
CPlayerShop_TalismanSearch.m_ReloadTypeData_CS2LuaHook = function (this, levelName, colorName)
    this.m_TypeActions = CPlayerShopMgr.Inst:ConvertOptions(CPlayerShopMgr.Inst:GetTalismanTypeGroupNames(levelName, colorName, this.IsPublicity), MakeDelegateFromCSFunction(this.TypeAction, MakeGenericClass(Action1, Int32), this))

    if CYuanbaoMarketMgr.EquipSearchParams ~= nil then
        local typeName = CPlayerShopMgr.Inst:GetEquipTypeName(CYuanbaoMarketMgr.EquipSearchParams.Type)
        do
            local i = 0
            while i < this.m_TypeActions.Count do
                if this.m_TypeActions[i].text == typeName then
                    this.SelectType = i
                    break
                end
                i = i + 1
            end
        end
    end

    if this.m_TypeActions.Count > 0 then
        this.SelectType = math.min(math.max(this.SelectType, 0), this.m_TypeActions.Count - 1)
        this.m_TypeButton.Text = this.m_TypeActions[this.SelectType].text
    end
end
CPlayerShop_TalismanSearch.m_ReloadColorData_CS2LuaHook = function (this, levelName) 
    this.m_ColorActions = CPlayerShopMgr.Inst:ConvertOptions(CPlayerShopMgr.Inst:GetTalismanColorGroupNames(levelName, this.IsPublicity), MakeDelegateFromCSFunction(this.ColorAction, MakeGenericClass(Action1, Int32), this))

    if CYuanbaoMarketMgr.EquipSearchParams ~= nil then
        local colorName = CPlayerShopMgr.Inst:GetEquipColorName(CYuanbaoMarketMgr.EquipSearchParams.Color, false)
        do
            local i = 0
            while i < this.m_ColorActions.Count do
                if this.m_ColorActions[i].text == colorName then
                    this.SelectColor = i
                    break
                end
                i = i + 1
            end
        end
    end

    if this.m_ColorActions.Count > 0 then
        this.SelectColor = math.min(math.max(this.SelectColor, 0), this.m_ColorActions.Count - 1)
        this.m_ColorButton.Text = this.m_ColorActions[this.SelectColor].text
    end
end
CPlayerShop_TalismanSearch.m_ReloadEquipData_CS2LuaHook = function (this)
    if this.SelectLevel >= 0 and this.SelectLevel < this.m_LevelActions.Count and this.SelectType >= 0 and this.SelectType < this.m_TypeActions.Count and this.SelectColor >= 0 and this.SelectColor < this.m_ColorActions.Count then
        local weapons = CPlayerShopMgr.Inst:SearchTalisman(this.m_LevelActions[this.SelectLevel].text, this.m_TypeActions[this.SelectType].text, this.m_ColorActions[this.SelectColor].text)
        CommonDefs.ListClear(this.m_SearchTemplateIds)
        CommonDefs.ListClear(this.m_SearchTemplateColors)
        CommonDefs.ListClear(this.m_SearchNames)

        CommonDefs.ListAddRange(this.m_SearchTemplateIds, weapons.Keys)
        CommonDefs.ListSort1(this.m_SearchTemplateIds, typeof(UInt32), DelegateFactory.Comparison_uint(function (a, b) 
            local data1 = EquipmentTemplate_Equip.GetData(a)
            local data2 = EquipmentTemplate_Equip.GetData(b)
            if data1 == nil or data2 == nil then
                return 0
            end
            return NumberCompareTo(data1.Grade, data2.Grade)
        end))
        this.m_SearchTemplateColors = CPlayerShopMgr.Inst:GetColors(this.m_SearchTemplateIds)
        do
            local i = 0
            while i < this.m_SearchTemplateIds.Count do
                CommonDefs.ListAdd(this.m_SearchNames, typeof(String), CommonDefs.DictGetValue(weapons, typeof(UInt32), this.m_SearchTemplateIds[i]))
                i = i + 1
            end
        end

        this.m_EquipNameActions = CPlayerShopMgr.Inst:ConvertOptions(this.m_SearchNames, MakeDelegateFromCSFunction(this.EquipAction, MakeGenericClass(Action1, Int32), this))
        if this.m_EquipNameActions.Count > 0 then
            this.SelectEquip = 0
            if this.m_FocusButton ~= nil then
                local focus = CPlayerShopMgr.Inst:IsFocusTemplate(this.m_SearchTemplateIds[this.SelectEquip])
                local default
                if focus then
                    default = LocalString.GetString("取消关注")
                else
                    default = LocalString.GetString("关注此类")
                end
                this.m_FocusButton.Text = default
            end
            this.m_EquipNameButton.Text = this.m_EquipNameActions[0].text
            CommonDefs.GetComponentInChildren_Component_Type(this.m_EquipNameButton, typeof(UILabel)).color = this.m_SearchTemplateColors[0]
            this:QuerySellCount(this.m_SearchTemplateIds)
        else
            this.m_EquipNameButton.Text = LocalString.GetString("空")
            MessageWndManager.ShowOKMessage(LocalString.GetString("当前你选择的分类中没有任何法宝,请重新选择！"), nil)
        end
    else
        this.m_EquipNameButton.Text = LocalString.GetString("空")
        MessageWndManager.ShowOKMessage(LocalString.GetString("当前你选择的分类中没有任何法宝,请重新选择！"), nil)
    end
end
CPlayerShop_TalismanSearch.m_OnTypeButtonOnClick_CS2LuaHook = function (this, button) 
    CPlayerShopPopupMenuInfoMgr.ShowPlayerShopPopupMenu(CommonDefs.ListToArray(this.m_TypeActions), button.transform, AlignType.Bottom, this:GetColumnCount(this.m_TypeActions.Count), nil, nil, DelegateFactory.Action(function () 
        this.m_TypeButton:SetTipStatus(false)
    end), 600,420)
end
CPlayerShop_TalismanSearch.m_OnEquipNameButtonOnClick_CS2LuaHook = function (this, button) 
    CPlayerShopPopupMenuInfoMgr.ShowPlayerShopPopupMenu(CommonDefs.ListToArray(this.m_EquipNameActions), button.transform, AlignType.Top, this:GetColumnCount(this.m_EquipNameActions.Count), this.m_SearchTemplateColors, CreateFromClass(ItemSearchParams, this.m_SearchTemplateIds, this.IsPublicity, SearchOption.All), DelegateFactory.Action(function () 
        this.m_EquipNameButton:SetTipStatus(false)
    end), 600,420)
end
CPlayerShop_TalismanSearch.m_OnColorButtonOnClick_CS2LuaHook = function (this, button) 
    CPlayerShopPopupMenuInfoMgr.ShowPlayerShopPopupMenu(CommonDefs.ListToArray(this.m_ColorActions), button.transform, AlignType.Bottom, this:GetColumnCount(this.m_ColorActions.Count), nil, nil, DelegateFactory.Action(function () 
        this.m_ColorButton:SetTipStatus(false)
    end), 600,420)
end
CPlayerShop_TalismanSearch.m_OnFocusButtonOnClick_CS2LuaHook = function (this, button) 
    if this.SelectEquip < 0 or this.SelectEquip >= this.m_SearchTemplateIds.Count then
        return
    end
    local itemId = this.m_SearchTemplateIds[this.SelectEquip]
    local allFocus = CPlayerShopMgr.Inst:GetAllFocusTemplate()
    if CommonDefs.HashSetContains(allFocus, typeof(UInt32), itemId) then
        CommonDefs.HashSetRemove(allFocus, typeof(UInt32), itemId)
        CPlayerShopMgr.Inst:SetAllFocusTemplate(allFocus)
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("取消关注成功"))
        this.m_FocusButton.Text = LocalString.GetString("关注此类")
    else
        CommonDefs.HashSetAdd(allFocus, typeof(UInt32), itemId)
        if allFocus.Count <= CPlayerShopMgr.s_FocusTemplateLimit then
            CPlayerShopMgr.Inst:SetAllFocusTemplate(allFocus)
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("关注成功"))
            this.m_FocusButton.Text = LocalString.GetString("取消关注")
        else
            g_MessageMgr:ShowMessage("PLAYER_SHOP_MAX_COLLECTIONS")
        end
    end
end
CPlayerShop_TalismanSearch.m_LevelAction_CS2LuaHook = function (this, row) 

    if this.m_LevelButton ~= nil and this.m_LevelActions ~= nil and this.m_LevelActions.Count > row then
        this.SelectLevel = row
        this.m_LevelButton.Text = this.m_LevelActions[this.SelectLevel].text
        this:ReloadColorData(this.m_LevelActions[this.SelectLevel].text)
        this:ReloadTypeData(this.m_LevelActions[this.SelectLevel].text, this.m_ColorActions[this.SelectColor].text)
        this:ReloadEquipData()
    end
end
CPlayerShop_TalismanSearch.m_TypeAction_CS2LuaHook = function (this, row) 

    if this.m_TypeButton ~= nil and this.m_TypeActions ~= nil and this.m_TypeActions.Count > row then
        this.SelectType = row
        this.m_TypeButton.Text = this.m_TypeActions[this.SelectType].text
        this:ReloadEquipData()
    end
end
CPlayerShop_TalismanSearch.m_ColorAction_CS2LuaHook = function (this, row) 

    if this.m_ColorButton ~= nil and this.m_ColorActions ~= nil and this.m_ColorActions.Count > row then
        this.SelectColor = row
        this.m_ColorButton.Text = this.m_ColorActions[this.SelectColor].text
        this:ReloadTypeData(this.m_LevelActions[this.SelectLevel].text, this.m_ColorActions[this.SelectColor].text)
        this:ReloadEquipData()
    end
end
CPlayerShop_TalismanSearch.m_EquipAction_CS2LuaHook = function (this, row) 

    if this.m_EquipNameButton ~= nil and this.m_EquipNameActions ~= nil and this.m_EquipNameActions.Count > row then
        this.SelectEquip = row
        if this.m_FocusButton ~= nil then
            local focus = CPlayerShopMgr.Inst:IsFocusTemplate(this.m_SearchTemplateIds[this.SelectEquip])
            local default
            if focus then
                default = LocalString.GetString("取消关注")
            else
                default = LocalString.GetString("关注此类")
            end
            this.m_FocusButton.Text = default
        end
        this.m_EquipNameButton.Text = this.m_EquipNameActions[row].text
        CommonDefs.GetComponentInChildren_Component_Type(this.m_EquipNameButton, typeof(UILabel)).color = this.m_SearchTemplateColors[row]
    end
end
CPlayerShop_TalismanSearch.m_GetColumnCount_CS2LuaHook = function (this, count) 
    local columns = 1
    if count >= 6 then
        columns = 2
    end
    return columns
end
CPlayerShop_TalismanSearch.m_QuerySellCount_CS2LuaHook = function (this, templateIds) 

    if not CPlayerShop_TalismanSearch.s_QuerySellCount then
        return
    end

    local data = CreateFromClass(MakeGenericClass(List, Object))
    local data2 = CreateFromClass(MakeGenericClass(List, Object))
    local data3 = CreateFromClass(MakeGenericClass(List, Object))
    this.m_MaxSellIndex = - 1
    this.m_MaxSellCount = 0
    do
        local i = 0
        while i < templateIds.Count do
            if i < 50 then
                CommonDefs.ListAdd(data, typeof(UInt32), templateIds[i])
            elseif i < 100 then
                CommonDefs.ListAdd(data2, typeof(UInt32), templateIds[i])
			else
                CommonDefs.ListAdd(data3, typeof(UInt32), templateIds[i])
            end
            i = i + 1
        end
    end
    Gac2Gas.QueryPlayerShopOnShelfNum(MsgPackImpl.pack(data), MsgPackImpl.pack(data2), MsgPackImpl.pack(data3), this.IsPublicity, SearchOption_lua.All, false)
end
CPlayerShop_TalismanSearch.m_OnQueryPlayerShopOnShelfNumResult_CS2LuaHook = function (this, selledCounts, publicity) 

    if not CPlayerShop_TalismanSearch.s_QuerySellCount then
        return
    end
    if CPlayerShopPopupMenu.IsPlayerShopPopUpMenuEnabled then
        return
    end

    if this.m_SearchTemplateIds ~= nil and this.m_SearchTemplateIds.Count > 0 then
        if publicity ~= this.IsPublicity then
            return
        end
        do
            local i = 0
            while i < selledCounts.Count do
                local id = selledCounts[i]
                local count = selledCounts[i + 1]
                do
                    local j = 0
                    while j < this.m_SearchTemplateIds.Count do
                        if this.m_SearchTemplateIds[j] == id and this.m_MaxSellCount < count then
                            this.m_MaxSellIndex = j
                            this.m_MaxSellCount = count
                        end
                        j = j + 1
                    end
                end
                i = i + 2
            end
        end
        if this.m_MaxSellIndex >= 0 and this.m_MaxSellIndex < this.m_EquipNameActions.Count then
            this.SelectEquip = this.m_MaxSellIndex
            this.m_EquipNameButton.Text = this.m_EquipNameActions[this.SelectEquip].text
            CommonDefs.GetComponentInChildren_Component_Type(this.m_EquipNameButton, typeof(UILabel)).color = this.m_SearchTemplateColors[this.SelectEquip]
        end
    end
end
