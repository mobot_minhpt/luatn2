local UIGrid = import "UIGrid"
local GameObject = import "UnityEngine.GameObject"
local UISlider = import "UISlider"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local DateTime = import "System.DateTime"
local RestrictDirection = import "L10.UI.CUIRestrictScrollView+RestrictDirection"

LuaScheduleStarBiwuPageProgress = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaScheduleStarBiwuPageProgress, "PhaseGrid", "PhaseGrid", UIGrid)
RegistChildComponent(LuaScheduleStarBiwuPageProgress, "PhaseTemplate", "PhaseTemplate", GameObject)
RegistChildComponent(LuaScheduleStarBiwuPageProgress, "ProgressBar", "ProgressBar", UISlider)

--@endregion RegistChildComponent end
RegistClassMember(LuaScheduleStarBiwuPageProgress, "m_PhasesCal")
RegistClassMember(LuaScheduleStarBiwuPageProgress, "m_ProgressNodeInfos")
RegistClassMember(LuaScheduleStarBiwuPageProgress, "m_ProgressNodes")
RegistClassMember(LuaScheduleStarBiwuPageProgress, "m_SliderFore")
RegistClassMember(LuaScheduleStarBiwuPageProgress, "m_SliderBack")
RegistClassMember(LuaScheduleStarBiwuPageProgress, "m_WhiteColor")
RegistClassMember(LuaScheduleStarBiwuPageProgress, "m_YellowColor")
RegistClassMember(LuaScheduleStarBiwuPageProgress, "m_GrayColor")
RegistClassMember(LuaScheduleStarBiwuPageProgress, "m_SpriteColor")
RegistClassMember(LuaScheduleStarBiwuPageProgress, "m_CurTitle")
RegistClassMember(LuaScheduleStarBiwuPageProgress, "m_ProgressScrollView")
RegistClassMember(LuaScheduleStarBiwuPageProgress, "m_IsQuDao")

function LuaScheduleStarBiwuPageProgress:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.m_SliderFore = self.ProgressBar.transform:Find("Foreground"):GetComponent(typeof(UISprite))
    self.m_SliderBack = self.ProgressBar.transform:Find("Background"):GetComponent(typeof(UISprite))
    self.m_ProgressScrollView = self.transform:Find("ProgressScrollView"):GetComponent(typeof(UIScrollView))
    self.m_IsQuDao = self.transform.parent.name == "QuDaoView"
    self.m_WhiteColor = NGUIText.ParseColor24(self.m_IsQuDao and "FFDDC8" or "fff6b8", 0)
    self.m_YellowColor = NGUIText.ParseColor24(self.m_IsQuDao and "FF888F" or "ffd74b", 0)
    self.m_GrayColor = NGUIText.ParseColor24(self.m_IsQuDao and "B36B71" or "a98e75", 0)
    self.m_SpriteColor = NGUIText.ParseColor24(self.m_IsQuDao and "FAAFC4" or "fff481", 0)
    self.m_SpriteLow = 18
    self.m_SpriteHigh = 28
end

function LuaScheduleStarBiwuPageProgress:Start()
    local now = CServerTimeMgr.Inst:GetZone8Time()
    local formatted_date = tonumber(self:FormatDate(now))
    local curPhaseNode = 1
    local lastData = nil
    local last_data_date = nil
    self.m_PhasesCal = {}
    self.m_ProgressNodeInfos = {}
    local scheduleDataList = {}
    StarBiWuShow_Schedule.Foreach(function (key, data)
        if (CLuaStarBiwuMgr.m_SchedulePageQnDaoViewSwitchState == 2) and (data.IsGuanfu > 0) then
            table.insert(scheduleDataList, StarBiWuShow_Schedule.GetData(key))
        elseif (CLuaStarBiwuMgr.m_SchedulePageQnDaoViewSwitchState == 1) and (data.IsGuanfu == 0) then
            table.insert(scheduleDataList, StarBiWuShow_Schedule.GetData(key))
        end
    end)
    for _, data in pairs(scheduleDataList) do
        self.m_PhasesCal[data.ProgressNode] = true
        local progressNode = data.ProgressNode
        local isInProgress = false
        local isPassed = false
        local data_date = nil
        if(data.Date and data.Date ~= "")then
            data_date = tonumber(self:FormatDataDate(tostring(data.Date)))
            if(formatted_date == data_date)then
                isInProgress = true
                curPhaseNode = progressNode
            elseif last_data_date and last_data_date < formatted_date and formatted_date < data_date then
                curPhaseNode = progressNode - 1
            end
            isPassed = formatted_date > data_date
            lastData = data
            last_data_date = data_date
        end
        if progressNode > 0 then
            if isPassed then
                curPhaseNode = progressNode
            end
            if not self.m_ProgressNodeInfos[progressNode] then
                self.m_ProgressNodeInfos[progressNode] = {NodeTitle = data.NodeTitle, NodeTime = data.NodeTime, DateTime = data.Date ~= "" and DateTime.Parse(tostring(data.Date)) or nil}
            end
            if isInProgress then
                self.m_CurTitle = data.NodeTitle
                --print(progressNode,self.m_CurTitle)
                self.m_ProgressNodeInfos[progressNode].InPorgress = true
            end
            if isPassed then
                self.m_ProgressNodeInfos[progressNode].Passed = true
            end
        end
    end

    local subProgress = 0
    local curInfo = self.m_ProgressNodeInfos[curPhaseNode]
    local nextInfo = self.m_ProgressNodeInfos[curPhaseNode + 1]

    if curInfo and curInfo.Passed and nextInfo then
        local curDate = curInfo.DateTime
        local nextDate = nextInfo.DateTime
        if curDate and nextDate then
            subProgress = CServerTimeMgr.Inst:DayDiff(now, curDate) / CServerTimeMgr.Inst:DayDiff(nextDate, curDate)
        end
    end

    self:InitPhases(curPhaseNode, subProgress)
end

function LuaScheduleStarBiwuPageProgress:InitPhases(curPhaseNode, subProgress)
    self.PhaseTemplate:SetActive(false)
    local nodeNum = #self.m_PhasesCal
    self.m_ProgressNodes = {}
    Extensions.RemoveAllChildren(self.PhaseGrid.transform)
    for i = 1, nodeNum do
        local go = NGUITools.AddChild(self.PhaseGrid.gameObject, self.PhaseTemplate)
        go:SetActive(true)
        table.insert(self.m_ProgressNodes, go.transform)
    end
    local sliderWidth = (nodeNum - 1) * 200
    self.m_SliderFore.width = sliderWidth
    self.m_SliderBack.width = sliderWidth
    for i = 1, #self.m_ProgressNodes do
        self:InitPhase(self.m_ProgressNodes[i], self.m_ProgressNodeInfos[i], i == nodeNum)
    end
    self.ProgressBar.value = (curPhaseNode - 1 + subProgress) / (#self.m_ProgressNodes - 1)
    self.m_ProgressScrollView.gameObject:GetComponent(typeof(CUIRestrictScrollView)).restrictDirection = nodeNum == 3 and RestrictDirection.Center or RestrictDirection.Left
    if nodeNum ~= 3 then
        self.m_ProgressScrollView:SetDragAmount(self.ProgressBar.value,0,false)
        self.PhaseGrid:Reposition()
    else
        self.PhaseGrid:Reposition()
        self.m_ProgressScrollView:RestrictWithinBounds(true)
        self.m_ProgressScrollView:SetDragAmount(0.5,0,false)
    end
    local curNode = self.m_ProgressNodes[curPhaseNode]
    if curNode then
        CUICommonDef.SetFullyVisible(curNode.gameObject, self.Progress)
    end
end

function LuaScheduleStarBiwuPageProgress:InitPhase(item, info, isLast)
    local nameLab   = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local timeLab   = item.transform:Find("TimeLabel"):GetComponent(typeof(UILabel))
    local stateIcon = item.transform:Find("StateIcon"):GetComponent(typeof(UISprite))
    local cupIcon   = item.transform:Find("CupIcon").gameObject

    nameLab.text = info.NodeTitle
    timeLab.text = info.NodeTime
    local labColor = nil
    local iconWidth = nil
    local iconColor = nil
    if info.InPorgress then
        -- 进行中
        labColor = self.m_YellowColor
        iconWidth = self.m_SpriteHigh
        iconColor = self.m_SpriteColor
    elseif info.Passed then
        -- 已完成
        labColor = self.m_GrayColor
        iconWidth = self.m_SpriteLow
        iconColor = self.m_SpriteColor
    else
        -- 未开始
        labColor = self.m_WhiteColor
        iconWidth = self.m_SpriteLow
        iconColor = self.m_GrayColor
    end

    nameLab.color = labColor
    timeLab.color = labColor
    stateIcon.color = iconColor
    stateIcon.width = iconWidth
    stateIcon.height = iconWidth
    cupIcon:SetActive(isLast)
end

function LuaScheduleStarBiwuPageProgress:FormatDate(data)
    local dataTime = CreateFromClass(DateTime, data.Year, data.Month, data.Day, 0, 0, 0)
    local span = dataTime:Subtract(CServerTimeMgr.UtcStart)
    local return_val = span.TotalSeconds
    return return_val
end

function LuaScheduleStarBiwuPageProgress:FormatDataDate(_date)
    if(_date and _date ~= "")then
        local return_val = ""
        local words = {}
        for w in string.gmatch(_date, "%d+") do
            table.insert(words, w)
        end
        local dataTime = CreateFromClass(DateTime, words[1], words[2], words[3], 0, 0, 0)
        return_val = self:FormatDate(dataTime)
        return return_val
    end
end

--@region UIEvent

--@endregion UIEvent

