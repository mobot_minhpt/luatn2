require("3rdParty/ScriptEvent")
require("common/common_include")
local CTowerDefenseMgr=import "L10.Game.CTowerDefenseMgr"
local CMainCamera=import "L10.Engine.CMainCamera"
local Gac2Gas=import "L10.Game.Gac2Gas"
local LuaGameObject=import "LuaGameObject"
local EnumFurnitureFeatureStatus = import "L10.Game.EnumFurnitureFeatureStatus"

CLuaTowerDefensePopupMenu=class()
RegistClassMember(CLuaTowerDefensePopupMenu,"m_Button1")
RegistClassMember(CLuaTowerDefensePopupMenu,"m_Button2")
RegistClassMember(CLuaTowerDefensePopupMenu,"m_Button3")
RegistClassMember(CLuaTowerDefensePopupMenu,"m_Button4")
RegistClassMember(CLuaTowerDefensePopupMenu,"m_RotateButton")
RegistClassMember(CLuaTowerDefensePopupMenu,"m_DeleteButton")
RegistClassMember(CLuaTowerDefensePopupMenu,"table")

RegistClassMember(CLuaTowerDefensePopupMenu,"m_CachedPos")
RegistClassMember(CLuaTowerDefensePopupMenu,"m_Anchor")
RegistClassMember(CLuaTowerDefensePopupMenu,"m_LastPos")


function CLuaTowerDefensePopupMenu:Init()
    self.m_Anchor=LuaGameObject.GetChildNoGC(self.transform,"Anchor").gameObject
    self.m_CachedPos=Vector3(0,0,0)
    if CTowerDefenseMgr.Inst.CurTower==nil then
        CUIManager.CloseUI("TowerDefensePopupMenu")
        return
    end
	self.table = LuaGameObject.GetChildNoGC(self.transform, "table").table

    self.m_Button1=LuaGameObject.GetChildNoGC(self.transform,"Button1").gameObject
    UIEventListener.Get(self.m_Button1).onClick = DelegateFactory.VoidDelegate(function(go)
        if CTowerDefenseMgr.Inst.CurTower~=nil then
            CTowerDefenseMgr.Inst.CurTower:RequestPlaceTower()
            if(CTowerDefenseMgr.Inst.type == 1) then
                CTowerDefenseMgr.Inst.CurTower=nil
            end
        end
        CUIManager.CloseUI("TowerDefensePopupMenu")
    end)
    self.m_Button2=LuaGameObject.GetChildNoGC(self.transform,"Button2").gameObject
    UIEventListener.Get(self.m_Button2).onClick = DelegateFactory.VoidDelegate(function(go)
        if CTowerDefenseMgr.Inst.CurTower~=nil and (CTowerDefenseMgr.Inst.type == 1 or CTowerDefenseMgr.Inst.CurTower.EngineId == 0) then
            CTowerDefenseMgr.Inst:RemoveTower(CTowerDefenseMgr.Inst.CurTower)
            CTowerDefenseMgr.Inst.CurTower=nil
        end
        if CTowerDefenseMgr.Inst.CurTower ~= nil and CTowerDefenseMgr.Inst.type == 2 and CTowerDefenseMgr.Inst.CurTower.EngineId > 0 then
            CTowerDefenseMgr.Inst:CancelMoveTower()
        end
        CUIManager.CloseUI("TowerDefensePopupMenu") 
    end)

    self.m_Button3=LuaGameObject.GetChildNoGC(self.transform,"Button3").gameObject--回收
    UIEventListener.Get(self.m_Button3).onClick = DelegateFactory.VoidDelegate(function(go)
        if CTowerDefenseMgr.Inst.CurTower then
            Gac2Gas.TowerDefenseRemoveTower(CTowerDefenseMgr.Inst.CurTower.ID," ")

            CTowerDefenseMgr.Inst.CurTower=nil
            CUIManager.CloseUI("TowerDefensePopupMenu") 
        end
    end)
    self.m_Button4=LuaGameObject.GetChildNoGC(self.transform,"Button4").gameObject--升级
    UIEventListener.Get(self.m_Button4).onClick = DelegateFactory.VoidDelegate(function(go)
        if CTowerDefenseMgr.Inst.CurTower then
            Gac2Gas.TowerDefenseUpgradeTower(CTowerDefenseMgr.Inst.CurTower.ID," ")
            
            CTowerDefenseMgr.Inst.CurTower=nil
            CUIManager.CloseUI("TowerDefensePopupMenu") 
        end
    end)

	self.m_RotateButton = LuaGameObject.GetChildNoGC(self.transform, "RotateButton").gameObject--旋转
    UIEventListener.Get(self.m_RotateButton).onClick = DelegateFactory.VoidDelegate(function(go)
        if CTowerDefenseMgr.Inst.CurTower and CTowerDefenseMgr.Inst.type == 2 then
            CTowerDefenseMgr.Inst.CurTower:RotateTower()
        end
    end)
	
	self.m_DeleteButton = LuaGameObject.GetChildNoGC(self.transform, "DeleteButton").gameObject--删除家具
    UIEventListener.Get(self.m_DeleteButton).onClick = DelegateFactory.VoidDelegate(function(go)
        if CTowerDefenseMgr.Inst.CurTower and CTowerDefenseMgr.Inst.type == 2 then
            CTowerDefenseMgr.Inst:RequestDeleteTower()
        end
    end)

    self:RefreshButtonState()
    self:Update()
end

function CLuaTowerDefensePopupMenu:RefreshButtonState()
    if CTowerDefenseMgr.Inst.CurTower==nil then
        return
    end
    if CTowerDefenseMgr.Inst.CurTower.EngineId==0 or CTowerDefenseMgr.Inst.type == 2 then 
        self.m_Button1:SetActive(true)
        self.m_Button2:SetActive(true)
		if CTowerDefenseMgr.Inst.type == 2 then
			self.m_RotateButton:SetActive(CTowerDefenseMgr.Inst.placeFurnitureMode == EnumFurnitureFeatureStatus.FurnitureFreeOperation)
			self.m_DeleteButton:SetActive(CTowerDefenseMgr.Inst.CurTower.EngineId ~= 0)
		else
			self.m_RotateButton:SetActive(false)
			self.m_DeleteButton:SetActive(false)
		end
        self.m_Button3:SetActive(false)
        self.m_Button4:SetActive(false)
    else--
        self.m_Button1:SetActive(false)
        self.m_Button2:SetActive(false)
		self.m_Button3:SetActive(true)
		self.m_RotateButton:SetActive(false)
		self.m_DeleteButton:SetActive(false)
        if CTowerDefenseMgr.Inst.CurTower.UpgradeCost==0 then--不能升级
            self.m_Button4:SetActive(false)
        --钱不够
        elseif CTowerDefenseMgr.Inst.CurTower.UpgradeCost>0 and CTowerDefenseMgr.Inst.CurTower.UpgradeCost>CTowerDefenseMgr.Inst.Gold then
            self.m_Button4:SetActive(true)
            CUICommonDef.SetActive(self.m_Button4,false,true)
        else
            self.m_Button4:SetActive(true)
            CUICommonDef.SetActive(self.m_Button4,true,true)
        end

        --回收费用
        LuaGameObject.GetChildNoGC(self.m_Button3.transform,"Label").label.text="+"..tostring(CTowerDefenseMgr.Inst.CurTower.RecyclePrice)
        --升级费用
        LuaGameObject.GetChildNoGC(self.m_Button4.transform,"Label").label.text="-"..tostring(CTowerDefenseMgr.Inst.CurTower.UpgradeCost)
    end
	self.table:Reposition()
end

function CLuaTowerDefensePopupMenu:OnEnable()
    g_ScriptEvent:AddListener("TowerDefenseSyncMyPlayPlayerInfo", self, "OnTowerDefenseSyncMyPlayPlayerInfo")
    -- self:UpdatePos()
end

function CLuaTowerDefensePopupMenu:OnDisable()
    g_ScriptEvent:RemoveListener("TowerDefenseSyncMyPlayPlayerInfo", self, "OnTowerDefenseSyncMyPlayPlayerInfo")
end
function CLuaTowerDefensePopupMenu:OnTowerDefenseSyncMyPlayPlayerInfo()
    self:RefreshButtonState()
end

function CLuaTowerDefensePopupMenu:Update()
    local tower = CTowerDefenseMgr.Inst.CurTower
    if not tower then return end
    local RO=nil
    if tower then
        RO=tower.RO or tower.RelatedRO
    end
    if not RO then return end
    
    local pos=RO.TopAnchorPos
    local rootPos = RO.Position
    --计算
    pos = Vector3((rootPos.x+pos.x)/2,(rootPos.y+pos.y)/2,(rootPos.z+pos.z)/2)
    local screenPos = CMainCamera.Main:WorldToScreenPoint(pos)
    if self.m_CachedPos.x == screenPos.x and self.m_CachedPos.y==screenPos.y then
        return
    end
    screenPos.z = 0
    self.m_CachedPos = screenPos
    local nguiWorldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(screenPos)
    self.m_Anchor.transform.position = nguiWorldPos
end
return CLuaTowerDefensePopupMenu