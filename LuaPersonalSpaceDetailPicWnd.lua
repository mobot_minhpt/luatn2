local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CUIManager = import "L10.UI.CUIManager"
local UITexture = import "UITexture"
local Screen = import "UnityEngine.Screen"
local GameObject = import "UnityEngine.GameObject"
local Object1 = import "UnityEngine.Object"
local CPersonalSpaceWnd = import "L10.UI.CPersonalSpaceWnd"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"

LuaPersonalSpaceDetailPicWnd=class()
RegistChildComponent(LuaPersonalSpaceDetailPicWnd,"picNode", GameObject)
RegistChildComponent(LuaPersonalSpaceDetailPicWnd,"smallPicNode", GameObject)
RegistChildComponent(LuaPersonalSpaceDetailPicWnd,"leftBtn", GameObject)
RegistChildComponent(LuaPersonalSpaceDetailPicWnd,"rightBtn", GameObject)

--RegistClassMember(LuaPersonalSpaceDetailPicWnd, "cameraNode")
function LuaPersonalSpaceDetailPicWnd:InitPanel(index)
  local picNode = self.picNode
  local smallPicNode = self.smallPicNode
  local infoArray = LuaPersonalSpaceMgrReal.ShowDetailImgTable

  if index > #infoArray or index <= 0 then
    local mainTexture = CommonDefs.GetComponent_GameObject_Type(picNode, typeof(UITexture)).mainTexture
    CommonDefs.GetComponent_GameObject_Type(picNode, typeof(UITexture)).mainTexture = nil
    Object1.Destroy(mainTexture)
    CUIManager.CloseUI(CLuaUIResources.PersonalSpaceDetailPicWnd)
    return
  end

  local leftButton = self.leftBtn
  local rightButton = self.rightBtn
  if index > 1 then
    leftButton:SetActive(true)
    UIEventListener.Get(leftButton).onClick = DelegateFactory.VoidDelegate(function (p)
      self:InitPanel(index - 1)
    end)
  else
    UIEventListener.Get(leftButton).onClick = nil
    leftButton:SetActive(false)
  end
  if index < #infoArray then
    rightButton:SetActive(true)
    UIEventListener.Get(rightButton).onClick = DelegateFactory.VoidDelegate(function (p)
      self:InitPanel(index + 1)
    end)
  else
    UIEventListener.Get(rightButton).onClick = nil
    rightButton:SetActive(false)
  end

  UIEventListener.Get(picNode).onDrag = DelegateFactory.VectorDelegate(function (go, delta)
    local deltaVal = - delta.x / Screen.width * 360
    if deltaVal > CPersonalSpaceWnd.PicTriggerDis then
      if rightButton.activeSelf then
        self:InitPanel(index + 1)
      end
    elseif deltaVal < - CPersonalSpaceWnd.PicTriggerDis then
      if leftButton.activeSelf then
        self:InitPanel(index - 1)
      end
    end
  end)


  local info = infoArray[index]
  if info and info.pic then
    CPersonalSpaceMgr.DownLoadPic(info.pic, CommonDefs.GetComponent_GameObject_Type(picNode, typeof(UITexture)), CPersonalSpaceMgr.MaxPicHeight, CPersonalSpaceMgr.MaxPicWidth, DelegateFactory.Action(function ()
      if not self or not self.picNode then
        return
      end
      picNode:SetActive(true)
      CommonDefs.GetComponent_GameObject_Type(picNode, typeof(UITexture)):ResizeCollider()
    end), false, true)
  end
end

function LuaPersonalSpaceDetailPicWnd:Init()
  local index = LuaPersonalSpaceMgrReal.ShowDetailImgIndex
  self:InitPanel(index)
end

function LuaPersonalSpaceDetailPicWnd:OnDestroy()
  LuaPersonalSpaceMgrReal.ShowDetailImgTable = nil
  LuaPersonalSpaceMgrReal.ShowDetailImgIndex = nil
  local mainTexture = CommonDefs.GetComponent_GameObject_Type(self.picNode, typeof(UITexture)).mainTexture
  CommonDefs.GetComponent_GameObject_Type(self.picNode, typeof(UITexture)).mainTexture = nil
  Object1.Destroy(mainTexture)
end

return LuaPersonalSpaceDetailPicWnd
