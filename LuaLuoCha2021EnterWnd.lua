local UITable = import "UITable"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local UILabel = import "UILabel"
local Extensions = import "Extensions"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local CTipParagraphItem = import "L10.UI.CTipParagraphItem"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CItemInfoMgr=import "L10.UI.CItemInfoMgr"
local AlignType2 = import "L10.UI.CItemInfoMgr+AlignType"


LuaLuoCha2021EnterWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaLuoCha2021EnterWnd, "JoinBtn", "JoinBtn", GameObject)
RegistChildComponent(LuaLuoCha2021EnterWnd, "TextTable", "TextTable", UITable)
RegistChildComponent(LuaLuoCha2021EnterWnd, "TextTemplate", "TextTemplate", GameObject)
RegistChildComponent(LuaLuoCha2021EnterWnd, "ScrollView", "ScrollView", CUIRestrictScrollView)
RegistChildComponent(LuaLuoCha2021EnterWnd, "MatchingLabel", "MatchingLabel", UILabel)
RegistChildComponent(LuaLuoCha2021EnterWnd, "JoinBtnLabel", "JoinBtnLabel", UILabel)
RegistChildComponent(LuaLuoCha2021EnterWnd, "TimeLabel", "TimeLabel", UILabel)
RegistChildComponent(LuaLuoCha2021EnterWnd, "RewardInfo", "RewardInfo", GameObject)
RegistChildComponent(LuaLuoCha2021EnterWnd, "CountLabel", "CountLabel", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaLuoCha2021EnterWnd,"m_IsMatching")

function LuaLuoCha2021EnterWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.JoinBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnJoinBtnClick()
	end)


    --@endregion EventBind end
end

function LuaLuoCha2021EnterWnd:Init()
    self:InitTextTable()
    self:UpdateMatching(LuaLuoCha2021Mgr.m_IsSignUp)
    local rewardInfo = {LuaLuoCha2021Mgr.m_FailReward, LuaLuoCha2021Mgr.m_RenLeiSuccessReward, LuaLuoCha2021Mgr.m_LuoChaSuccessReward}
    self:InitRewardInfo(rewardInfo)
end

function LuaLuoCha2021EnterWnd:InitTextTable()
    local setting = LuoChaHaiShi_Setting.GetData()
    self.TimeLabel.text = setting.OpenTimeDescription

    self.TextTemplate:SetActive(false)
    Extensions.RemoveAllChildren(self.TextTable.transform)
    local msg = g_MessageMgr:FormatMessage("LuoChaHaiShi_Rule_Introduction")
    
    local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(msg)
    if info == nil then
        return
    end
    for i = 0, info.paragraphs.Count - 1 do
        local paragraphGo = NGUITools.AddChild(self.TextTable.gameObject, self.TextTemplate)
        paragraphGo:SetActive(true)
        CommonDefs.GetComponent_GameObject_Type(paragraphGo, typeof(CTipParagraphItem))
                :Init(info.paragraphs[i], 4294967295)
    end
    self.ScrollView:ResetPosition()
    self.TextTable:Reposition()
end

function LuaLuoCha2021EnterWnd:InitRewardInfo(rewardInfo)
    -- 奖励物品配表
    local setting = LuoChaHaiShi_Setting.GetData()
    local mailIdList = {setting.FailedRewardMailId, setting.PlayerSuccessRewardMailId, setting.LuoChaSuccessRewardMailId}

    for i=1, 3 do
        local mailId = mailIdList[i]
        local itemIdList = {}
        if mailId~= nil then
            local mailData = Mail_Mail.GetData(mailId)
            local rawItems = mailData.Items

            local itemList = {}
            for templateId, count, _ in string.gmatch(rawItems, "(%d+),(%d+),(%d+);") do
                table.insert(itemIdList, tonumber(templateId))
                count = tonumber(count)
            end

            local itemId = itemIdList[1]
            local iconTexture = self.RewardInfo.transform:Find(tostring(i).. "/IconTexture"):GetComponent(typeof(CUITexture))
            UIEventListener.Get(iconTexture.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
                CItemInfoMgr.ShowLinkItemTemplateInfo(itemId,false, nil, AlignType2.Default, 0, 0, 0, 0)
            end)

            local itemData = Item_Item.GetData(itemId)
            if itemData then
                iconTexture:LoadMaterial(itemData.Icon)
            end
        end

        local hasRewarded = rewardInfo[i] > 0
        local item = self.RewardInfo.transform:Find(tostring(i)).gameObject
        local hasRewardIcon = item.transform:Find("HasReward").gameObject
        local texture = item.transform:Find("Texture").gameObject
        hasRewardIcon:SetActive(hasRewarded)
        texture:SetActive(hasRewarded)
    end
end

function LuaLuoCha2021EnterWnd:UpdateMatching(hasSigned)
    self.m_IsMatching = hasSigned
    self.MatchingLabel.gameObject:SetActive(hasSigned)

    local btnSprite = self.JoinBtn.transform:GetComponent(typeof(UISprite))
    if hasSigned then
        self.JoinBtnLabel.text = LocalString.GetString("取消报名")
        btnSprite.spriteName = "common_btn_01_blue"
    else
        self.JoinBtnLabel.text = LocalString.GetString("报名参加")
        btnSprite.spriteName = "common_btn_01_yellow"
    end

end

function LuaLuoCha2021EnterWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncLuoChaHaiShiSignUpResult", self, "UpdateMatching")
end

function LuaLuoCha2021EnterWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncLuoChaHaiShiSignUpResult", self, "UpdateMatching")
end

--@region UIEvent

function LuaLuoCha2021EnterWnd:OnJoinBtnClick()
    if self.m_IsMatching then
        Gac2Gas.RequestCancelSignUpLuoChaHaiShi()
    else
        Gac2Gas.RequestSignUpLuoChaHaiShi()
    end
end


--@endregion UIEvent

