local GameObject                = import "UnityEngine.GameObject"
local Vector4                   = import "UnityEngine.Vector4"
local CUIManager                = import "L10.UI.CUIManager"
local CUICommonDef              = import "L10.UI.CUICommonDef"
local CItemInfoMgr				= import "L10.UI.CItemInfoMgr"
local AlignType					= import "L10.UI.CItemInfoMgr+AlignType"
local QnTableView               = import "L10.UI.QnTableView"
local QnButton                  = import "L10.UI.QnButton"
local DefaultTableViewDataSource= import "L10.UI.DefaultTableViewDataSource"
local UILabel                   = import "UILabel"
local DelegateFactory		    = import "DelegateFactory"
local UIEventListener		    = import "UIEventListener"
local LocalString               = import "LocalString"
local LuaGameObject             = import "LuaGameObject"
local LuaTweenUtils             = import "LuaTweenUtils"
local NGUIMath                  = import "NGUIMath"
local PathMode                  = import "DG.Tweening.PathMode"
local PathType                  = import "DG.Tweening.PathType"
local Ease                      = import "DG.Tweening.Ease"
local CTickMgr                  = import "L10.Engine.CTickMgr"
local ETickType                 = import "L10.Engine.ETickType"
local CZuoQiMgr                 = import "L10.Game.CZuoQiMgr"

--天朔卷轴活动主界面奖励页签
LuaTsjzMainRewardTab = class()

RegistChildComponent(LuaTsjzMainRewardTab, "NextLabel",		    UILabel)
RegistChildComponent(LuaTsjzMainRewardTab, "TimeLb",		    UILabel)
RegistChildComponent(LuaTsjzMainRewardTab, "ItemCell1",		    GameObject)
RegistChildComponent(LuaTsjzMainRewardTab, "ItemCell2",		    GameObject)
RegistChildComponent(LuaTsjzMainRewardTab, "ItemCell3",		    GameObject)
RegistChildComponent(LuaTsjzMainRewardTab, "AdvPassImg2",		GameObject)
RegistChildComponent(LuaTsjzMainRewardTab, "AdvPassImg3",		GameObject)
RegistChildComponent(LuaTsjzMainRewardTab, "OneKeyBtn",		    GameObject)
RegistChildComponent(LuaTsjzMainRewardTab, "ShopBtn",		    GameObject)
RegistChildComponent(LuaTsjzMainRewardTab, "QnTableView1",		QnTableView)
RegistChildComponent(LuaTsjzMainRewardTab, "Sprite2",           GameObject)
RegistChildComponent(LuaTsjzMainRewardTab, "Sprite3",           GameObject)
RegistChildComponent(LuaTsjzMainRewardTab, "RideItem",          GameObject)

RegistClassMember(LuaTsjzMainRewardTab,  "CurImpLv")
RegistClassMember(LuaTsjzMainRewardTab,  "SelectedItem")
RegistClassMember(LuaTsjzMainRewardTab,  "Isdirty")

function LuaTsjzMainRewardTab:Awake()
    self.CurImpLv = -1
    self.SelectedItem = nil
    UIEventListener.Get(self.OneKeyBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnOneKeyBtnClick()
    end)

    UIEventListener.Get(self.ShopBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnShopBtnClick()
    end)

    UIEventListener.Get(self.Sprite2).onClick = DelegateFactory.VoidDelegate(function(go)
        local passtype = LuaHanJiaMgr.TcjhMainData.PassType
        if passtype ~= 1 then
            CUIManager.ShowUI(CLuaUIResources.TsjzBuyAdvPassWnd)
        end
    end)

    UIEventListener.Get(self.Sprite3).onClick = DelegateFactory.VoidDelegate(function(go)
        local passtype = LuaHanJiaMgr.TcjhMainData.PassType
        if passtype ~= 2 then
            CUIManager.ShowUI(CLuaUIResources.TsjzBuyAdvPassWnd)
        end
    end)

    local ridedeslabel = LuaGameObject.GetChildNoGC(self.RideItem.transform,"RideDesLabel").label
    local setting = HanJia2022_TianShuoSetting.GetData()
    local zuoqiID = setting.ZuoQiID
    local item = Item_Item.GetData(zuoqiID)
    if item then
        ridedeslabel.text = SafeStringFormat3(setting.ZuoQiDes,item.Name)
    else
        ridedeslabel.text = ""
    end
    UIEventListener.Get(self.RideItem).onClick = DelegateFactory.VoidDelegate(function(go)
        CZuoQiMgr.Inst:ShowVehiclePreview(setting.VehicleID)
    end)

    self.QnTableView1.m_ScrollView.horizontalScrollBar.OnChangeValue = DelegateFactory.Action_float(function(value)
        self:OnScroll(value)
    end)

    g_ScriptEvent:AddListener("OnTsjzDataChange", self, "OnTsjzDataChange")
    g_ScriptEvent:AddListener("OnTsjzPassTypeChange", self, "OnTsjzPassTypeChange")
    self:OnTsjzDataChange()
end

function LuaTsjzMainRewardTab:OnDestroy()
    g_ScriptEvent:RemoveListener("OnTsjzDataChange", self, "OnTsjzDataChange")
    g_ScriptEvent:RemoveListener("OnTsjzPassTypeChange", self, "OnTsjzPassTypeChange")
end

function LuaTsjzMainRewardTab:OnTsjzPassTypeChange(type)
    if type == 1 or type == 3 then
        local go = LuaGameObject.GetChildNoGC(self.Sprite2.transform,"Fxs").gameObject
        go:SetActive(true)
    end
    if type == 2 or type == 3 then
        local go = LuaGameObject.GetChildNoGC(self.Sprite3.transform,"Fxs").gameObject
        go:SetActive(true)
    end
end

function LuaTsjzMainRewardTab:OnScroll(value)--ok
    local data = LuaHanJiaMgr.TcjhMainData

    local vdelta = 5.2
    local impcount = 5
    local index = math.ceil((data.CfgMaxLv - vdelta) * value+vdelta)
    local implv = (math.floor((index - 1) / impcount) + 1) * impcount 

    if implv == self.CurImpLv then return end
    self.CurImpLv = implv

    self:RefreshImpItems()
end

function LuaTsjzMainRewardTab:HaveImpReward(implv,data)--ok
    if data.RewardNmlLv < implv then return true end
    if (data.PassType == 1 or data.PassType == 3) and data.RewardAdvLv < implv then return true end
    if (data.PassType == 2 or data.PassType == 3) and data.RewardAdvLv2 < implv then return true end
    return false
end

function LuaTsjzMainRewardTab:RefreshImpItems()--ok
    local implv = self.CurImpLv
    local data = LuaHanJiaMgr.TcjhMainData
    local nmlitems,advitems,advitems2 = self:GetLvItems(implv)

    local cell1 = self.ItemCell1.transform
    local cell2 = self.ItemCell2.transform
    local cell3 = self.ItemCell3.transform

    local haveimpreward = self:HaveImpReward(implv,data)
    if haveimpreward then
        self.NextLabel.text = SafeStringFormat3(LocalString.GetString("%d级可领"),implv)
    else
        self.NextLabel.text = LocalString.GetString("已领取")
    end

    local rewardlv1 = data.RewardNmlLv
    local rewardlv2 = -1
    local rewardlv3 = -1
    local passType = data.PassType
    if  passType == 1 or passType == 3 then 
        rewardlv2 = data.RewardAdvLv
    end
    if  passType == 2 or passType == 3 then 
        rewardlv3 = data.RewardAdvLv2
    end
    
    if nmlitems == nil or implv > data.MaxLv then
        self:FillItemCell(cell1,nil,implv,rewardlv1,false)
    else
        self:FillItemCell(cell1,nmlitems[1],implv,rewardlv1,false)
    end

    if advitems == nil then 
        self:FillItemCell(cell2,nil,implv,rewardlv2,false)
    else
        self:FillItemCell(cell2,advitems[1],implv,rewardlv2,false)
    end

    if advitems2 == nil then 
        self:FillItemCell(cell3,nil,implv,rewardlv3,false)
    else
        self:FillItemCell(cell3,advitems2[1],implv,rewardlv3,false)
    end
end

function LuaTsjzMainRewardTab:OnEnable()--ok
    if self.Isdirty then
        self:OnTsjzDataChange()
    end
end

function LuaTsjzMainRewardTab:OnDisable()--ok
    if self.m_DelayTick ~= nil then
        invoke(self.m_DelayTick)
    end
end

function LuaTsjzMainRewardTab:OnTsjzDataChange()
    if not self.gameObject.activeSelf then
        self. Isdirty = true
        return 
    end
    self.Isdirty = false
    local data = LuaHanJiaMgr.TcjhMainData

    local lv = data.Lv
    local lvmax = data.CfgMaxLv
    
    local passtype = data.PassType --0 免费通行证, 1 高级通行证, 2 高级通行证豪华版
    --self.AdvLockImg:SetActive(passtype == 0)
    CUICommonDef.SetGrey(self.AdvPassImg2,passtype ~=1 and passtype ~=3)
    CUICommonDef.SetGrey(self.AdvPassImg3,passtype ~=2 and passtype ~=3)
    local lockgo2 = LuaGameObject.GetChildNoGC(self.Sprite2.transform,"LockedSprite").gameObject
    local lockgo3 = LuaGameObject.GetChildNoGC(self.Sprite3.transform,"LockedSprite").gameObject
    lockgo2:SetActive(passtype ~= 1 and passtype ~= 3)
    lockgo3:SetActive(passtype ~= 2 and passtype ~= 3)

    local setting = HanJia2022_TianShuoSetting.GetData()
    local year, month, day, hour, min = string.match(setting.BeginTime, "(%d+)-(%d+)-(%d+) (%d+):(%d+)")
    local  year2, month2, day2, hour2, min2 =string.match(setting.FinishTime, "(%d+)-(%d+)-(%d+) (%d+):(%d+)")
    self.TimeLb.text = SafeStringFormat3(LocalString.GetString("活动时间：%d年%d月%d日-%d月%d日"),year,month,day,month2,day2)

    local initfunc = function(item,index)
        self:FillContentItem(item,index)
    end
    self.QnTableView1.m_DataSource = DefaultTableViewDataSource.CreateByCount(lvmax,initfunc)
    self.QnTableView1:ReloadData(true, true)
    self.QnTableView1.m_ScrollView:InvalidateBounds()

    if self.m_DelayTick ~= nil then
        invoke(self.m_DelayTick)
    end
    self.m_DelayTick = CTickMgr.RegisterUpdate(DelegateFactory.Action_uint(function (delta) 
        self.QnTableView1:ScrollToRow(lv)
    end), ETickType.Once)

    self:RefreshImpItems()

    local havereward = lv > data.RewardNmlLv or (passtype ~= 0 and lv > data.RewardAdvLv)
    local btn = self.OneKeyBtn:GetComponent(typeof(QnButton))
    btn.Enabled = havereward
end

function LuaTsjzMainRewardTab:FillContentItem(item,index)--ok
    local lv = index + 1

    local curlv = LuaHanJiaMgr.TcjhMainData.Lv

    local lvlbstr = "Lv1/Label"
    if lv > curlv then 
        lvlbstr = "Lv2/Label"
    end
    local lvlb = item.transform:Find(lvlbstr):GetComponent(typeof(UILabel))
    lvlb.text = SafeStringFormat3(LocalString.GetString("%s级"), tostring(lv))
    
    local selectgo = LuaGameObject.GetChildNoGC(item.transform,"SelectImg").gameObject
    selectgo:SetActive(curlv == lv)

    local lvbg1 = LuaGameObject.GetChildNoGC(item.transform,"Lv1").gameObject
    local lvbg2 = LuaGameObject.GetChildNoGC(item.transform,"Lv2").gameObject
    lvbg1:SetActive(lv <= curlv)
    lvbg2:SetActive(lv > curlv)

    local lv1 = LuaHanJiaMgr.TcjhMainData.RewardNmlLv
    local lv2 = LuaHanJiaMgr.TcjhMainData.RewardAdvLv
    local lv3 = LuaHanJiaMgr.TcjhMainData.RewardAdvLv2

    local passtype = LuaHanJiaMgr.TcjhMainData.PassType
    if passtype ==0 then
        lv2 = -1
        lv3 = -1
    elseif passtype == 1 then
        lv3 = -1
    elseif passtype == 2 then
        lv2 =-1
    end
    local nmlitems,advitems,advitems2 = self:GetLvItems(lv)
    if lv1 <= LuaHanJiaMgr.TcjhMainData.MaxLv then
        self:FillItems(item,nmlitems,lv,lv1,1)
    end
    self:FillItems(item,advitems,lv,lv2,2)
    self:FillItems(item,advitems2,lv,lv3,3)
end

function LuaTsjzMainRewardTab:FillItems(item,datas,lv,rewardlv,index)--ok
    local cell = item.transform:Find("Item"..index.."/ItemCell")
    if datas == nil then 
        self:FillItemCell(cell,nil,lv,rewardlv,true)
    else
        self:FillItemCell(cell,datas[1],lv,rewardlv,true)
    end    
end

function LuaTsjzMainRewardTab:FillItemCell(cell,data,lv,rewardlv,inscroll)--ok
    if data == nil then 
        cell.gameObject:SetActive(false)
    else
        local icon = LuaGameObject.GetChildNoGC(cell,"IconTexture").cTexture
        local bindgo = LuaGameObject.GetChildNoGC(cell,"BindSprite").gameObject
        local countlb = LuaGameObject.GetChildNoGC(cell,"AmountLabel").label
        local checkgo = LuaGameObject.GetChildNoGC(cell,"ClearupCheckbox").gameObject
        local lockgo = LuaGameObject.GetChildNoGC(cell,"LockedSprite").gameObject
        local fx = LuaGameObject.GetChildNoGC(cell,"Fx").uiFx

        local itemid = data.ItemID
        local count = data.Count
        local isBind = data.IsBind
        local curlv = LuaHanJiaMgr.TcjhMainData.Lv
        
        local itemcfg = Item_Item.GetData(itemid)

        if itemcfg then
            icon:LoadMaterial(itemcfg.Icon)
        end

        bindgo:SetActive(tonumber(isBind) == 1)

        
        checkgo:SetActive(lv <= rewardlv)
        lockgo:SetActive(lv > curlv or rewardlv < 0)

        if inscroll then
            fx.ScrollView = self.QnTableView1.m_ScrollView
        end

        local canreward = lv > rewardlv and lv <= curlv and rewardlv >= 0
        if canreward then 
            local b = NGUIMath.CalculateRelativeWidgetBounds(cell)
            local waypoints = CUICommonDef.GetWayPoints(Vector4(b.min.x, b.max.y, b.size.x, b.size.y), 1, false)
            fx.transform.localPosition = waypoints[0]
            fx:LoadFx("fx/ui/prefab/UI_kuang_yellow02.prefab")
            LuaTweenUtils.DOLuaLocalPath(fx.transform, waypoints, 2, PathType.Linear, PathMode.Ignore, 2, - 1, Ease.Linear)
        else
            fx:DestroyFx()
        end
        
        UIEventListener.Get(cell.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            if canreward then
                --Gac2Gas.RequestGetTianChengLevelReward(lv)
                LuaHanJiaMgr.RequireTsjzLevelReward(lv)
            else
                CItemInfoMgr.ShowLinkItemTemplateInfo(itemid, false, nil, AlignType.Default, 0, 0, 0, 0) 
            end
            if self.SelectedItem ~= go then
                if self.SelectedItem then
                    self:SelectItem(self.SelectedItem,false)
                end
                self.SelectedItem = go
                self:SelectItem(self.SelectedItem,true)
            end
        end)

        if tonumber(count) <= 1 then
            countlb.text = ""
        else
            countlb.text = count
        end

        cell.gameObject:SetActive(true)
    end
end

function LuaTsjzMainRewardTab:SelectItem(itemgo,selected)--ok
    local selectgo = LuaGameObject.GetChildNoGC(itemgo.transform,"SelectedSprite").gameObject
    selectgo:SetActive(selected)
end

function LuaTsjzMainRewardTab:GetLvItems(lv)--ok
    local data = LuaHanJiaMgr.TcjhMainData
    local curcfg = data.Cfg[lv]
    if curcfg == nil then 
        return nil,nil
    else
        return curcfg.NmlItems,curcfg.AdvItems,curcfg.AdvItems2
    end
end

--@region UIEvent

function LuaTsjzMainRewardTab:OnOneKeyBtnClick()
    local data = LuaHanJiaMgr.TcjhMainData
    if data.Lv > data.RewardNmlLv or (data.PassType ~= 0 and data.Lv > data.RewardAdvLv) then
        local lv = data.Lv
        LuaHanJiaMgr.RequireTsjzLevelReward(lv)
    end
end

function LuaTsjzMainRewardTab:OnShopBtnClick()
    local setting = HanJia2022_TianShuoSetting.GetData()
    local shopid = setting.ShopID
    CLuaNPCShopInfoMgr.ShowScoreShopById(shopid)
end

--@endregion
