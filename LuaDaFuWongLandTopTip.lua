local CUITexture = import "L10.UI.CUITexture"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local Animation = import "UnityEngine.Animation"
local CMainCamera = import "L10.Engine.CMainCamera"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CPos = import "L10.Engine.CPos"
local CSetUIScreenPosition = import "L10.UI.CSetUIScreenPosition"
LuaDaFuWongLandTopTip = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaDaFuWongLandTopTip, "sell", "sell", UILabel)
RegistChildComponent(LuaDaFuWongLandTopTip, "Icon", "Icon", CUITexture)
RegistChildComponent(LuaDaFuWongLandTopTip, "MyIcon", "MyIcon", GameObject)
RegistChildComponent(LuaDaFuWongLandTopTip, "bg_wenzi", "bg_wenzi", GameObject)
RegistChildComponent(LuaDaFuWongLandTopTip, "Name", "Name", UILabel)
RegistChildComponent(LuaDaFuWongLandTopTip, "Price", "Price", GameObject)
RegistChildComponent(LuaDaFuWongLandTopTip, "UpOrDown", "UpOrDown", GameObject)
RegistChildComponent(LuaDaFuWongLandTopTip, "Type", "Type", UILabel)
RegistChildComponent(LuaDaFuWongLandTopTip, "Label", "Label", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaDaFuWongLandTopTip, "m_Anim")
RegistClassMember(LuaDaFuWongLandTopTip, "m_Pos")
RegistClassMember(LuaDaFuWongLandTopTip, "m_Height")
RegistClassMember(LuaDaFuWongLandTopTip, "m_LandId")
RegistClassMember(LuaDaFuWongLandTopTip, "m_Owner")
RegistClassMember(LuaDaFuWongLandTopTip, "m_Tick")
RegistClassMember(LuaDaFuWongLandTopTip, "m_bgList")
RegistClassMember(LuaDaFuWongLandTopTip, "m_touxiangBgList")
RegistClassMember(LuaDaFuWongLandTopTip, "m_Data")
RegistClassMember(LuaDaFuWongLandTopTip, "m_BuffView")
RegistClassMember(LuaDaFuWongLandTopTip, "m_Lv")
RegistClassMember(LuaDaFuWongLandTopTip, "m_Desc")
function LuaDaFuWongLandTopTip:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.m_Anim = self.transform:Find("Anchor"):GetComponent(typeof(Animation))
    self.m_Pos = nil
    self.m_Height = DaFuWeng_Setting.GetData().LandUIHeight
    self.m_LandId = 0
    self.m_Owner = 0
    self.m_Tick = nil
    self.m_Data = nil
    self.m_bgList = {"dafuwongplaylandeopwnd_huangd","dafuwongplaylandeopwnd_hongd","dafuwongplaylandeopwnd_zid","dafuwongplaylandeopwnd_lvd"}
	self.m_bgList[0] = "dafuwongplaylandeopwnd_land"
	self.m_touxiangBgList = {"dafuwongplaylandeopwnd_huang","dafuwongplaylandeopwnd_hong","dafuwongplaylandeopwnd_zi","dafuwongplaylandeopwnd_lv"}
	self.m_touxiangBgList[0] = "dafuwongplaylandeopwnd_lan"
    self.m_BuffView = self.transform:Find("Anchor/Content/Left/Buff").gameObject
    self.m_Lv = self.transform:Find("Anchor/Content/Right/lv").gameObject
    self.m_Desc = self.transform:Find("Anchor/Content/Right/Desc"):GetComponent(typeof(UILabel))
end

-- function LuaDaFuWongLandTopTip:Update()
--     if not self.m_Pos or not CMainCamera.Main  then return end
--     local viewPos = CMainCamera.Main:WorldToViewportPoint(self.m_Pos)
-- 	local isInView = viewPos.z > 0 and viewPos.x > 0 and viewPos.x < 1 and viewPos.y > 0 and viewPos.y < 1
-- 	local screenPos = isInView and CMainCamera.Main:WorldToScreenPoint(self.m_Pos) or Vector3(0,3000,0)
-- 	screenPos.z = 0
-- 	local worldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(screenPos)
-- 	--self.transform.position = worldPos
-- end

function LuaDaFuWongLandTopTip:Init(data)
    self.m_BuffView.gameObject:SetActive(false)
    self.sell.gameObject:SetActive(true)
    self.Icon.gameObject:SetActive(false)
    self.MyIcon.gameObject:SetActive(false)
    self.m_Data = data
    self.Name.text = data.Name
    self.UpOrDown.gameObject:SetActive(false)

    self.m_LandId = data.ID
    local pos = Utility.PixelPos2WorldPos(data.Pos[0], data.Pos[1])
    
    if data.Type ~= 2 then
        self.sell.gameObject:GetComponent(typeof(UILabel)).text = LocalString.GetString("公共")
        self.m_Lv.gameObject:SetActive(false)
        self.m_Desc.gameObject:SetActive(true)
        self.Price.gameObject:SetActive(false)
        self.m_Desc.text = data.ShortDesc
        local commonHeight = DaFuWeng_Setting.GetData().CommonLandUIHeight
        self.m_Pos = Vector3(pos.x, pos.y + commonHeight, pos.z)
        self.gameObject:GetComponent(typeof(CSetUIScreenPosition)).Pos = self.m_Pos
    else
        self.m_Pos = Vector3(pos.x, pos.y + self.m_Height, pos.z)
        self.gameObject:GetComponent(typeof(CSetUIScreenPosition)).Pos = self.m_Pos
        local labelColor = "ffffff"
        local cost = data.Price and data.Price[0] or 0
        if CClientMainPlayer.Inst then
            local id = CClientMainPlayer.Inst.Id
            if LuaDaFuWongMgr.PlayerInfo and LuaDaFuWongMgr.PlayerInfo[id].Money < cost then
                labelColor = "FC0A0B"
            end
        end
        self.m_Desc.gameObject:SetActive(false)
        self.Price.gameObject:SetActive(true)
        self.Label.text =  SafeStringFormat3("[%s]%d[-]",labelColor,cost)
        self.Type.text = LocalString.GetString("售价")
        self.bg_wenzi:GetComponent(typeof(CUITexture)):LoadMaterial(self:GetTexturePath(0,false))
        UIEventListener.Get(self.bg_wenzi.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            LuaDaFuWongMgr:OnShowDetailLandWnd(self.m_LandId)
        end)
        local headIcon = self.transform:Find("Anchor/Wnd_Bg/bg_touxiang").gameObject
        headIcon:GetComponent(typeof(CUITexture)):LoadMaterial(self:GetTexturePath(0,true))
        UIEventListener.Get(headIcon.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            LuaDaFuWongMgr:OnShowDetailLandWnd(self.m_LandId)
        end)
    end
end

function LuaDaFuWongLandTopTip:SwitchOwner(data)
    self.m_Anim:Play("dafuwongplaylandtopwnd_switch")
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
    end
    self.m_Tick = RegisterTickOnce(function()
        self:UpdateData(data)
    end, 200)
end
function LuaDaFuWongLandTopTip:GetTexturePath(index,ishead)
    local name = self.m_bgList[index]
    if ishead then
        name = self.m_touxiangBgList[index]
    end
    return SafeStringFormat3("UI/Texture/FestivalActivity/Festival_DaFuWong/Material/%s.mat",name)
end
function LuaDaFuWongLandTopTip:UpdateData(data)
    if not data then return end
    local landData = self.m_Data
    if landData and landData.Type ~= 2 then return end
    self.m_Owner = data.Owner
    local labelColor = "ffffff"
    local cost = 0
    local type = ""
    local OriCost = 0
    local index = 0
    if data.Owner == 0 then
        self.m_BuffView.gameObject:SetActive(false)
        self.sell.gameObject:SetActive(true)
        self.Icon.gameObject:SetActive(false)
        self.MyIcon.gameObject:SetActive(false)
        type = LocalString.GetString("售价")
        cost = data.curPrice
        OriCost = data.price and data.price[data.lv - 1] or 0
        if CClientMainPlayer.Inst then
            local id = CClientMainPlayer.Inst.Id
            if LuaDaFuWongMgr.PlayerInfo and LuaDaFuWongMgr.PlayerInfo[id].Money < cost then
                labelColor = "FC0A0B"
            end
        end
    else
        self.sell.gameObject:SetActive(false)
        self.Icon.gameObject:SetActive(true)
        local isMy = CClientMainPlayer.Inst and data.Owner == CClientMainPlayer.Inst.Id
        type = LocalString.GetString("过路费")
        cost = data.curCost
        OriCost = data.cost and data.cost[data.lv - 1] or 0
        self.MyIcon.gameObject:SetActive(isMy)
        index = 1
        if LuaDaFuWongMgr.PlayerInfo and LuaDaFuWongMgr.PlayerInfo[data.Owner] then
            local playerinfo = LuaDaFuWongMgr.PlayerInfo[data.Owner]
            self.Icon:LoadNPCPortrait(CUICommonDef.GetPortraitName(playerinfo.class, playerinfo.gender, -1), false)
            index = playerinfo.round
        end
        if data.showBuff and data.showBuff.count > 0 then
            self.m_BuffView.gameObject:SetActive(true)
            self.m_BuffView.transform:Find("NumLabel"):GetComponent(typeof(UILabel)).text = data.showBuff.count
            local id = data.showBuff.id
            local buffInfo = DaFuWeng_Buff.GetData(id)
            if buffInfo then 
                self.m_BuffView.transform:Find("Icon"):GetComponent(typeof(UISprite)).spriteName = buffInfo.Icon
            end
        else
            self.m_BuffView.gameObject:SetActive(false)
        end
    end
    self.bg_wenzi:GetComponent(typeof(CUITexture)):LoadMaterial(self:GetTexturePath(index,false))
    local headIcon = self.transform:Find("Anchor/Wnd_Bg/bg_touxiang").gameObject
    headIcon:GetComponent(typeof(CUITexture)):LoadMaterial(self:GetTexturePath(index,true))
    self.UpOrDown.gameObject:SetActive(cost ~= OriCost)
    self.UpOrDown:GetComponent(typeof(UISprite)).spriteName = cost > OriCost and "common_arrow_07_red" or "common_arrow_07_green"
    self.Label.text = SafeStringFormat3("[%s]%d[-]",labelColor,cost)
    self.Type.text = type
    self.UpOrDown:GetComponent(typeof(UIWidget)):ResetAndUpdateAnchors()
    self:InitLv(self.m_Lv,data.lv)
end

function LuaDaFuWongLandTopTip:InitLv(view,lv)
	if not view then return end
	for i = 0,view.transform.childCount - 1 do
		local go = view.transform:GetChild(i)
		local highlight = go.transform:Find("highlight").gameObject
        highlight.gameObject:SetActive(true)
		if i >= lv then
			highlight.gameObject:SetActive(false)
		end
		-- if go then
		-- 	go:GetComponent(typeof(UISprite)).spriteName = name
		-- end
	end
end

function LuaDaFuWongLandTopTip:OnDisable()
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
    end
end
function LuaDaFuWongLandTopTip:GetScreenPos(x,y,z)
	return (x - 8577  +y)* -1,(z  - 9212)  * -1,0
end
--@region UIEvent

--@endregion UIEvent

