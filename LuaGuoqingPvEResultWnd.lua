local GameObject = import "UnityEngine.GameObject"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"

LuaGuoqingPvEResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaGuoqingPvEResultWnd, "Text2", "Text2", UILabel)
RegistChildComponent(LuaGuoqingPvEResultWnd, "Button", "Button", GameObject)
RegistChildComponent(LuaGuoqingPvEResultWnd, "yinzhang_Texture", "yinzhang_Texture", GameObject)

--@endregion RegistChildComponent end

function LuaGuoqingPvEResultWnd:TimeChange(timedata)
	local number = tonumber(timedata)
	local minutes = math.floor(number / 60)
	local seconds = number % 60
	return SafeStringFormat3("%s%s%s%s",tostring(minutes),LocalString.GetString("分"),tostring(seconds),LocalString.GetString("秒"))
	
end

function LuaGuoqingPvEResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    if LuaGuoQingPvEMgr.times == 1 then
        self.yinzhang_Texture:SetActive(true)
    else
        self.yinzhang_Texture:SetActive(false)
    end
    if LuaGuoQingPvEMgr.mode == 2 then
        UIEventListener.Get(self.Button).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnButtonClick()
        end)
        self.Button:SetActive(true)
    else
        self.Button:SetActive(false)
    end
    
end

function LuaGuoqingPvEResultWnd:Init()
    self.Text2.text = self:TimeChange(LuaGuoQingPvEMgr.costTime)
end

--@region UIEvent

function LuaGuoqingPvEResultWnd:OnButtonClick(go)

	Gac2Gas.QueryXiangYaoYuQiuSiRank()
end

--@endregion UIEvent

