local CShiTuMgr = import "L10.Game.CShiTuMgr"
local COpenEntryMgr=import "L10.Game.COpenEntryMgr"
local CustomPropertySerializable = import "L10.Game.Properties.CustomPropertySerializable"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CItemMgr = import "L10.Game.CItemMgr"
local CChatHelper = import "L10.UI.CChatHelper"
local CEffectMgr = import "L10.Game.CEffectMgr"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local Ease = import "DG.Tweening.Ease"
local Animator = import "UnityEngine.Animator"
local EnumClass = import "L10.Game.EnumClass"
local MsgPackImpl = import "MsgPackImpl"
local Object = import "System.Object"
local UInt32 = import "System.UInt32"
local CRenderObject = import "L10.Engine.CRenderObject"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CClientOtherPlayer = import "L10.Game.CClientOtherPlayer"
local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"
local CFX = import "L10.Game.CEffectMgr+CFX"
local CRenderScene = import "L10.Engine.Scene.CRenderScene"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local Utility = import "L10.Engine.Utility"
local CPos = import "L10.Engine.CPos"
local DelegateFactory = import "DelegateFactory"
local Color = import "UnityEngine.Color"
local LayerDefine = import "L10.Engine.LayerDefine"
local MaterialPropertyBlock = import "UnityEngine.MaterialPropertyBlock"
local Renderer = import "UnityEngine.Renderer"
local PlayerPrefs = import "UnityEngine.PlayerPrefs"
local RotateMode = import "DG.Tweening.RotateMode"
local GameObject = import "UnityEngine.GameObject"
local CSoulCoreFxColorChanger = import "L10.Game.CSoulCoreFxColorChanger"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local TextMeshExDataSource = import "L10.UI.TextMeshExDataSource"
local EnumTextMeshExKey = import "L10.UI.EnumTextMeshExKey"
local MessageWndManager = import "L10.UI.MessageWndManager"
local Quaternion = import "UnityEngine.Quaternion"
local CTrackMgr = import "L10.Game.CTrackMgr"
local SectEntranceTrapType = import "L10.Engine.Scene.CRenderScene+SectEntranceTrapType"
local CScene = import "L10.Game.CScene"
local CClientNpc = import "L10.Game.CClientNpc"
local CSharpResourceLoader = import "L10.Game.CSharpResourceLoader"
local RenderSettings = import "UnityEngine.RenderSettings"
local SoundManager = import "SoundManager"
local CLivingSkillMgr = import "L10.Game.CLivingSkillMgr"
local CSceneDynamicStuff = import "L10.Engine.Scene.CSceneDynamicStuff"
local CTaskAndTeamWnd = import "L10.UI.CTaskAndTeamWnd"
local Type = import "UIBasicSprite.Type"
local Effect = import "UILabel.Effect"
local Vector2 = import "UnityEngine.Vector2"
local CTrackMgr = import "L10.Game.CTrackMgr"
local CPos = import "L10.Engine.CPos"
local CTaskMgr = import "L10.Game.CTaskMgr"
local PlayerSettings = import "L10.Game.PlayerSettings"
local CClientMonster = import "L10.Game.CClientMonster"
local Monster_Monster = import "L10.Game.Monster_Monster"

LuaZongMenMgr = {}
LuaZongMenMgr.m_GuiZe = ""
LuaZongMenMgr.m_GuiZeTitle = ""
LuaZongMenMgr.m_GuiZeIsRule = false
LuaZongMenMgr.m_GuiZeRaidMapId = 1
LuaZongMenMgr.m_JoiningZongMenId = 0
LuaZongMenMgr.m_ZongMenProp = nil
LuaZongMenMgr.m_JoinSectInfo = nil
LuaZongMenMgr.m_SectMainPageInfo = nil
LuaZongMenMgr.m_EnableSoulCore = true
LuaZongMenMgr.m_SectHasTitleMemberInfo = {}
LuaZongMenMgr.m_SectOfficeInfo = {}
LuaZongMenMgr.m_SectOfficeCandidataInfo = {}
LuaZongMenMgr.m_SectRecommendInfo = {}
LuaZongMenMgr.m_SectApplyerInfo = {}
LuaZongMenMgr.m_ConfirmRenMianMsg1 = ""
LuaZongMenMgr.m_ConfirmRenMianMsg2 = ""
LuaZongMenMgr.m_ConfirmRenMianRmTbl = nil
LuaZongMenMgr.m_ConfirmRenMianAddTbl = nil
LuaZongMenMgr.m_IsChangedZhangMenWeiJie = false
LuaZongMenMgr.m_SectHistoryInfo = {}
LuaZongMenMgr.m_SectOfficeRightInfo = {}
LuaZongMenMgr.m_SectMemberInfo = {}
LuaZongMenMgr.m_ChangeWeiJiePlayerId = 0
LuaZongMenMgr.m_PlayerId2Forbidden = {}
LuaZongMenMgr.m_SoulCoreCondensePause = false
LuaZongMenMgr.m_SoulCoreCondenseEndTime = nil
LuaZongMenMgr.m_SoulCoreCondenseSpeed = 1
LuaZongMenMgr.m_SoulCoreCondenseLeftTime = nil
LuaZongMenMgr.m_SoulCoreCondensePhase = 0
LuaZongMenMgr.m_SoulCoreCondenseTotoalTime = 1
LuaZongMenMgr.m_SoulCoreCondenseInReset = false
LuaZongMenMgr.m_RuMenGuiZe = nil
LuaZongMenMgr.m_ReplyJoinSectRes = {}
LuaZongMenMgr.m_IsOpen = true
LuaZongMenMgr.m_TempMemberView_PlayerPopupMenu_MemberInfo = nil
LuaZongMenMgr.m_SMSWQueryPlayInfoResultData = {}
LuaZongMenMgr.m_SMSWRankData = {}
LuaZongMenMgr.m_SMSW_DestroySceneId = 0
LuaZongMenMgr.m_SMSW_PlayState = EnumSMSWPlayState.Idle
LuaZongMenMgr.m_ShiMenMiShuSkills = nil
LuaZongMenMgr.m_ExtraTraps = {}
LuaZongMenMgr.m_SelectedAntiProfessionIndex = nil
LuaZongMenMgr.m_SectId2Name = {}
LuaZongMenMgr.m_XingWuItem = nil
LuaZongMenMgr.m_SkyboxId = 0
LuaZongMenMgr.m_SyncCurrentSceneId = 0
LuaZongMenMgr.m_SyncCurrentSectId = 0
LuaZongMenMgr.m_EvilBuffCostExp = 0
LuaZongMenMgr.m_AntiProfessionClickedSlot = {}
LuaZongMenMgr.m_AntiProfessionItemIds = nil
LuaZongMenMgr.m_AntiProfessionConvertResult = nil
LuaZongMenMgr.m_SelfSoulCoreGo = nil
LuaZongMenMgr.m_RepairTongTianTaInfo = nil
LuaZongMenMgr.m_ChangeZongMenNameContextUd = nil
LuaZongMenMgr.m_ZongMenName = nil
LuaZongMenMgr.m_LastOpenSoulCoreWndLv = 0
LuaZongMenMgr.m_MainWndTabIndex = nil
LuaZongMenMgr.m_ZongMenMemberViewTabIndex = 0
LuaZongMenMgr.m_ZongMenSkillViewIndex = nil
LuaZongMenMgr.m_SoulCoreWndTabIndex = nil
-- 当前宗派ID
LuaZongMenMgr.m_CurrentSectId = 0
LuaZongMenMgr.m_CreateSectExpressionId = 0
LuaZongMenMgr.m_CreateSectFuXiDanceMotionId = 0
LuaZongMenMgr.m_SectPrisonListInfo = nil

--宗派福利相关
LuaZongMenMgr.m_HongBaoDynamicOnShelfItemsSet = {}
LuaZongMenMgr.m_HongBaoItemList = {}
LuaZongMenMgr.m_HongBaoItemCount = 0
LuaZongMenMgr.m_CurSelectHongBaoItemIndex = 0
LuaZongMenMgr.m_SectHongBaoItemDetailData = {}
LuaZongMenMgr.m_SectHongBaoItemPlayerData = {}
LuaZongMenMgr.m_SectHongBaoId = 0
LuaZongMenMgr.m_GetSectHongBaoResultData = {}
LuaZongMenMgr.m_HongBaoIsOpen = true
LuaZongMenMgr.m_BgMusicPath = ""
SoundManager.m_hookGetSectBgEventPath = function (this)
    return LuaZongMenMgr.m_BgMusicPath  
end

function LuaZongMenMgr:IsMySectScene()
    if CClientMainPlayer.Inst and  LuaZongMenMgr.m_CurrentSectId ~= 0 and 
        CClientMainPlayer.Inst.BasicProp.SectId == self.m_CurrentSectId then
		return true
    else
        return false
	end
end

function LuaZongMenMgr:TrackToMySect(posX, posY)
    local fazhenPos = CreateFromClass(CPos, posX, posY)
    local sceneTemplateId = 16101788
    local dist = 2

    -- 在宗门直接寻路
    if self:IsMySectScene() then
        CTrackMgr.Inst:Track(nil, sceneTemplateId, fazhenPos, dist, nil, nil, nil, nil, true)
    else
        CTrackMgr.Inst.m_Track:RecordCrossingMap(nil, sceneTemplateId, fazhenPos, dist, nil, nil, nil, nil)
        Gac2Gas.RequestTrackToSectEntrance()
    end
end

function LuaZongMenMgr:CheckFaZhenOperation(op)
    if not self:IsMySectScene() or not CUIManager.IsLoaded(CLuaUIResources.RefineMonsterButtonWnd) then
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("FaZhen_Operation_Back_To_Sect_Confirm", op), function()
            local settingPos = LianHua_Setting.GetData().LianHuaGuaiPosAndArea
            local x = math.floor(settingPos[0])
            local y = math.floor(settingPos[1])
            self:TrackToMySect(x, y)

            CUIManager.CloseUI(CLuaUIResources.RefineMonsterCircleWnd)
            CUIManager.CloseUI(CLuaUIResources.RefineMonsterCircleNewWnd)
            CUIManager.CloseUI(CLuaUIResources.SoulCoreWnd)
            CUIManager.CloseUI(CUIResources.MainPlayerWnd)
        end, nil, LocalString.GetString("确认"), LocalString.GetString("取消"), false)
        return false
    else
        return true
    end
end

function LuaZongMenMgr:IsShiMenShouWeiFaZhenPlay()
    local isShiMenRuQinOpen = LuaZongMenMgr.m_SMSW_PlayState ~= EnumSMSWPlayState.Idle and LuaZongMenMgr.m_SMSW_PlayState ~= EnumSMSWPlayState.Prepare
    if isShiMenRuQinOpen and CScene.MainScene then
        local gamePlayId = CScene.MainScene.GamePlayDesignId
        if gamePlayId == ShiMenShouWei_Settings.GetData().FaZhenGamePlayId then
           return true
        end
    end
    return false
end

function LuaZongMenMgr:GoToGuardZongMen()
    self.m_OpenGuardZongMenWnd = true
    Gac2Gas.SMSW_QueryPlayInfo()
end

function LuaZongMenMgr:ChangeWeiJie(playerId)
    self.m_ChangeWeiJiePlayerId = playerId
    CUIManager.ShowUI(CLuaUIResources.ZongMenChangeWeiJieWnd)
end

function LuaZongMenMgr:GetMingGeMatPath(class)
    if class == 0 then return "" end
    return SafeStringFormat3("UI/Texture/Transparent/Material/%s.mat", SoulCore_WuXing.GetData(class).Icon)
end

function LuaZongMenMgr:IsMainPlayerSoulCoreUpgradeable()
    if CClientMainPlayer.Inst == nil then
        return false
    end
    local mainPlayer = CClientMainPlayer.Inst
    local oriLevel = mainPlayer.SkillProp.SoulCore.Level
    local nextLevel = oriLevel + 1
    local nextData = SoulCore_SoulCore.GetData(nextLevel)
    local hasNextLv = nextData ~= nil
    if hasNextLv then
        local maxLevel = mainPlayer.MaxLevel
        if nextData.PlayerLearnLv > maxLevel then
            return false
        end

        local mana = mainPlayer.SkillProp.SoulCore.Mana
        if nextData.InSoulCoreMana > mana then
            return false
        end
    
        local exp = mainPlayer.PlayProp.Exp
        if nextData.InSoulCoreExp > exp then
            return false
        end

        return true
    end

    return false
end

function LuaZongMenMgr:GetAdjustSoulCoreLevel(fanShenLevel, xianShenLevel, oriLv)
    local formulaId = SoulCore_Settings.GetData().AdjustFeiShengLvFormulaId
    local formula = AllFormulas.Action_Formula[formulaId].Formula

    if formula ~= nil then
        local fixLv = formula(nil, nil, {fanShenLevel, xianShenLevel})

        for lv = oriLv, 1, -1 do
            local data = SoulCore_SoulCore.GetData(lv)
            if data and data.PlayerLearnLv and data.PlayerLearnLv <= fixLv then
                return lv
            end
        end
    end

    return oriLv
end

function LuaZongMenMgr:GetMainPlayerAdjustSoulCoreLevel()
    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer == nil then
        return
    end
    local hasFeiSheng = mainPlayer.HasFeiSheng == true
    local fanShenLevel = mainPlayer.PlayProp.FeiShengData.FeiShengLevel
    local xianShenLevel = mainPlayer.XianShenLevel
    local oriLevel = mainPlayer.SkillProp.SoulCore.Level
    
    return hasFeiSheng and LuaZongMenMgr:GetAdjustSoulCoreLevel(fanShenLevel, xianShenLevel, oriLevel) or oriLevel
end
-- 灵核凝结
function LuaZongMenMgr:SoulCoreUpdateDisplay(reason)
    if reason == "Npc" or reason == "Item" then
        -- 重置灵核外观添加标记
        LuaZongMenMgr.m_SoulCoreCondenseInReset = true
        CUIManager.ShowUI(CLuaUIResources.SoulCoreCondenseWnd)
    end
end

-- matType 1 -> gam_02.mat; matType 2 -> gem_01.mat; matType 3 -> gem_03.mat
function LuaZongMenMgr:SetSoulCoreCoreColor(RO, matType, mingGe)
    local renderers = CommonDefs.GetComponentsInChildren_Component_Type(RO, typeof(Renderer))
    
    local wuXingData = SoulCore_WuXing.GetData(mingGe)
    local coreWuXing = wuXingData.Order[0]
    local colorData = SoulCore_WuXingColor.GetData(coreWuXing)
    if colorData == nil then
        return
    end

    local coreWuXing2, colorData2
    if wuXingData.Order.Length == 2 and coreWuXing ~= wuXingData.Order[1] then
        coreWuXing2 = wuXingData.Order[1]
        colorData2 = SoulCore_WuXingColor.GetData(coreWuXing2)
    end

    local color1, color2, color3, color4, color5, color6, color7
    local color2_1, color2_2, color2_3, color2_4, color2_5, color2_6, color2_7
    for i = 0 ,  renderers.Length - 1 do
        local renderer = renderers[i]
        if string.find(renderer.sharedMaterials[0].shader.name, "VolumetricCrystal") ~= nil then
            local mpb = CreateFromClass(MaterialPropertyBlock)
            renderer:GetPropertyBlock(mpb)

            if matType == 1 then
                if color1 == nil then
                    color1 = NGUIText.ParseColor24(colorData.MainColor1, 0)  -- _Layer0Tint
                    color2 = NGUIText.ParseColor24(colorData.MarbleColor1, 0)  -- _MarbleColor
                    color3 = NGUIText.ParseColor24(colorData.FresnelColorInside1, 0)  -- _FresnelColorInside
                    color4 = NGUIText.ParseColor24(colorData.FresnelColorOutSide1, 0)  -- _FresnelColorOutside
                    color5 = NGUIText.ParseColor24(colorData.SurfaceMaskColor1, 0)  -- _SurfaceAlphaColor
                end
                mpb:SetColor("_Layer0Tint", color1)
                mpb:SetColor("_MarbleColor", color2)
                mpb:SetColor("_FresnelColorInside", color3)
                mpb:SetColor("_FresnelColorOutside", color4)
                mpb:SetColor("_SurfaceAlphaColor", color5)

                if colorData2 ~= nil then
                    if color2_1 == nil then
                        color2_1 = NGUIText.ParseColor24(colorData2.MainColor1, 0)  -- _Layer0Tint
                        color2_2 = NGUIText.ParseColor24(colorData2.MarbleColor1, 0)  -- _MarbleColor
                        color2_3 = NGUIText.ParseColor24(colorData2.FresnelColorInside1, 0)  -- _FresnelColorInside
                        color2_4 = NGUIText.ParseColor24(colorData2.FresnelColorOutSide1, 0)  -- _FresnelColorOutside
                        color2_5 = NGUIText.ParseColor24(colorData2.SurfaceMaskColor1, 0)  -- _SurfaceAlphaColor
                    end
                    mpb:SetFloat("_EnableColor2", 1)
                    --第二颜色
                    mpb:SetColor("_Layer0Tint2", color2_1)
                    mpb:SetColor("_MarbleColor2", color2_2)
                    mpb:SetColor("_FresnelColorInside2", color2_3)
                    mpb:SetColor("_FresnelColorOutside2", color2_4)
                    mpb:SetColor("_SurfaceAlphaColor2", color2_5)
                else 
                    mpb:SetFloat("_EnableColor2", 0)
                end
            elseif matType == 2 then
                if color1 == nil then
                    color1 = NGUIText.ParseColor24(colorData.MainColorA2, 0)  -- _Layer0Tint
                    color2 = NGUIText.ParseColor24(colorData.MainColorB2, 0)  -- _Layer1Tint
                    color3 = NGUIText.ParseColor24(colorData.MainColorC2, 0)  -- _Layer2Tint
                    color4 = NGUIText.ParseColor24(colorData.CausticTint2, 0)  -- _CausticTint
                    color5 = NGUIText.ParseColor24(colorData.FresnelColorInside2, 0)  -- _FresnelColorInside
                    color6 = NGUIText.ParseColor24(colorData.SurfaceMaskColor2, 0)  -- _SurfaceAlphaColor
                    color7 = NGUIText.ParseColor24(colorData.InnerLightColorOutside, 0)  -- _InnerLightColorOutside
                end
                mpb:SetColor("_Layer0Tint", color1)
                mpb:SetColor("_Layer1Tint", color2)
                mpb:SetColor("_Layer2Tint", color3)
                mpb:SetColor("_CausticTint", color4)
                mpb:SetColor("_FresnelColorInside", color5)
                mpb:SetColor("_SurfaceAlphaColor", color6)
                mpb:SetColor("_InnerLightColorOutside", color7)

                if colorData2 ~= nil then
                    if color2_1 == nil then
                        color2_1 = NGUIText.ParseColor24(colorData2.MainColorA2, 0)  -- _Layer0Tint
                        color2_2 = NGUIText.ParseColor24(colorData2.MainColorB2, 0)  -- _Layer1Tint
                        color2_3 = NGUIText.ParseColor24(colorData2.MainColorC2, 0)  -- _Layer2Tint
                        color2_4 = NGUIText.ParseColor24(colorData2.CausticTint2, 0)  -- _CausticTint
                        color2_5 = NGUIText.ParseColor24(colorData2.FresnelColorInside2, 0)  -- _FresnelColorInside
                        color2_6 = NGUIText.ParseColor24(colorData2.SurfaceMaskColor2, 0)  -- _SurfaceAlphaColor
                        color2_7 = NGUIText.ParseColor24(colorData2.InnerLightColorOutside, 0)  -- _InnerLightColorOutside

                    end
                    mpb:SetFloat("_EnableColor2", 1)
                    --第二颜色
                    mpb:SetColor("_Layer0Tint2", color2_1)
                    mpb:SetColor("_Layer1Tint2", color2_2)
                    mpb:SetColor("_Layer2Tint2", color2_3)
                    mpb:SetColor("_CausticTint2", color2_4)
                    mpb:SetColor("_FresnelColorInside2", color2_5)
                    mpb:SetColor("_SurfaceAlphaColor2", color2_6)
                    mpb:SetColor("_InnerLightColorOutside2", color2_7)
                else 
                    mpb:SetFloat("_EnableColor2", 0)
                end
            elseif matType == 3 then
                if color1 == nil then
                    color1 = NGUIText.ParseColor24(colorData.MainColor3, 0)  -- _Layer0Tint
                    color2 = NGUIText.ParseColor24(colorData.SurfaceMaskColor3, 0)  -- _SurfaceAlphaColor
                end
                mpb:SetColor("_Layer0Tint", color1)
                mpb:SetColor("_SurfaceAlphaColor", color2)

                if colorData2 ~= nil then
                    if color2_1 == nil then
                        color2_1 = NGUIText.ParseColor24(colorData2.MainColor3, 0)  -- _Layer0Tint
                        color2_2 = NGUIText.ParseColor24(colorData2.SurfaceMaskColor3, 0)  -- _SurfaceAlphaColor
                    end
                    mpb:SetFloat("_EnableColor2", 1)
                    --第二颜色
                    mpb:SetColor("_Layer0Tint2", color2_1)
                    mpb:SetColor("_SurfaceAlphaColor2", color2_2)

                else 
                    mpb:SetFloat("_EnableColor2", 0)
                end
            end

            -- 变色周期及速度
            mpb:SetFloat("_DissolveThreshold", SoulCore_Settings.GetData().SoulCoreThreshold)
            mpb:SetFloat("_DissolveRange", SoulCore_Settings.GetData().SoulCoreRange)

            renderer:SetPropertyBlock(mpb)
        end
    end
    
end

function LuaZongMenMgr:SetSoulCoreFxColor(fxGo, isDiSe, mingGe)
    local colorChanger = fxGo:GetComponent(typeof(CSoulCoreFxColorChanger))
    if colorChanger == nil then
        colorChanger = CommonDefs.AddComponent_GameObject_Type(fxGo, typeof(CSoulCoreFxColorChanger))
    end

    local wuXingData = SoulCore_WuXing.GetData(mingGe)
    local coreWuXing = wuXingData.Order[0]
    local colorData = SoulCore_WuXingColor.GetData(coreWuXing)
    if colorData == nil then
        return
    end
    if wuXingData.Order.Length == 2 and coreWuXing ~= wuXingData.Order[1] then
        local coreWuXing2, colorData2
        coreWuXing2 = wuXingData.Order[1]
        colorData2 = SoulCore_WuXingColor.GetData(coreWuXing2)

        if isDiSe == true then
            colorChanger:SetColorChanger(NGUIText.ParseColor24(colorData.FxColor1, 0) , NGUIText.ParseColor24(colorData2.FxColor2, 0), true)
        else
            colorChanger:SetColorChanger(NGUIText.ParseColor24(colorData.FxColor2, 0) , NGUIText.ParseColor24(colorData2.FxColor2, 0), true) 
        end
    else 
        if isDiSe == true then
            colorChanger:SetColorChanger(NGUIText.ParseColor24(colorData.FxColor1, 0) , Color.white, false)
        else
            colorChanger:SetColorChanger(NGUIText.ParseColor24(colorData.FxColor2, 0) , Color.white, false) 
        end
    end
end

--soulCoreProp.DisplayCoreId, soulCoreProp.DisplayFxId, soulCoreProp.DisplayPalletId, soulCoreProp.DisplayCoreParticles, soulCoreProp.DisplayCoreEffect}
function LuaZongMenMgr:GetSoulCoreCorePath(DisplayCoreId)
    DisplayCoreId = (DisplayCoreId - 1) % SoulCore_DisplayCoreId.GetDataCount() + 1
    return SoulCore_DisplayCoreId.GetData(DisplayCoreId).Res
end

function LuaZongMenMgr:GetSoulCorePalletIdPath(DisplayPalletId)
    DisplayPalletId = (DisplayPalletId - 1) % SoulCore_DisplayPalletId.GetDataCount() + 1
    return SoulCore_DisplayPalletId.GetData(DisplayPalletId).Res
end

function LuaZongMenMgr:GetSoulCoreFxId(DisplayFxId)
    --DisplayFxId = (DisplayFxId - 1) % SoulCore_DisplayFxId.GetDataCount() + 1
    return SoulCore_DisplayFxId.GetData(1).FxID
end

function LuaZongMenMgr:GetSoulCoreParticlesId(DisplayCoreParticles)
    DisplayCoreParticles = (DisplayCoreParticles - 1) % SoulCore_DisplayCoreParticles.GetDataCount() + 1
    return SoulCore_DisplayCoreParticles.GetData(DisplayCoreParticles).FxID
end

function LuaZongMenMgr:GetSoulCoreCoreEffectId(DisplayCoreEffect, changeType)
    DisplayCoreEffect = (DisplayCoreEffect - 1) % SoulCore_DisplayCoreEffect.GetDataCount() + 1

    if changeType == 0 then
        return SoulCore_DisplayCoreEffect.GetData(DisplayCoreEffect).FxID
    elseif changeType == 1 then
        return SoulCore_DisplayCoreEffect.GetData(DisplayCoreEffect).FxID2
    elseif changeType == 2 then
        return SoulCore_DisplayCoreEffect.GetData(DisplayCoreEffect).FxID3
    end

    return SoulCore_DisplayCoreEffect.GetData(DisplayCoreEffect).FxID
end

function LuaZongMenMgr:CreateSoulCoreGameObject(parentGo, soulCoreProp, mingGe, layer, scale, notAdjustToCenter)
    local DisplayCoreId = soulCoreProp.DisplayCoreId and tonumber(soulCoreProp.DisplayCoreId) or 1
    local DisplayFxId = soulCoreProp.DisplayFxId and tonumber(soulCoreProp.DisplayFxId) or 1
    local DisplayPalletId = soulCoreProp.DisplayPalletId and tonumber(soulCoreProp.DisplayPalletId) or 1
    local DisplayCoreParticles = soulCoreProp.DisplayCoreParticles and tonumber(soulCoreProp.DisplayCoreParticles) or 1
    local DisplayCoreEffect = soulCoreProp.DisplayCoreEffect and tonumber(soulCoreProp.DisplayCoreEffect) or 1
    local level = soulCoreProp.Level and tonumber(soulCoreProp.Level) or 1
    local mingGe = mingGe > 0 and  tonumber(mingGe) or 1
    local scale = scale and tonumber(scale) or 1
    local levelSoulCoreChange = SoulCore_Settings.GetData().SoulCoreChange
    local changeType = 0
    local changeScale = levelSoulCoreChange[0][1] 
    for i = levelSoulCoreChange.Length - 1, 0, -1 do
        if level >= levelSoulCoreChange[i][0] then
            changeType = i
            changeScale = levelSoulCoreChange[i][1] 
            break
        end
    end
    local totalScale = scale * changeScale
    local commonYOffset = 0.27
    local rootRo = CRenderObject.CreateRenderObject(parentGo, "SoulCore")
    local coreRo = CRenderObject.CreateRenderObject(rootRo.gameObject, "Core")
    coreRo:AddLoadAllFinishedListener(DelegateFactory.Action_CRenderObject(function(renderObject)
        local roLayer = layer and layer or LayerDefine.ModelForNGUI_3D
        renderObject.Layer = roLayer
        local DisplayCoreId = (DisplayCoreId - 1) % SoulCore_DisplayCoreId.GetDataCount() + 1
        self:SetSoulCoreCoreColor(renderObject, SoulCore_DisplayCoreId.GetData(DisplayCoreId).Type, mingGe)
        renderObject.Scale = totalScale

        local centerTrans = renderObject.transform:GetChild(0):Find("Center")
        local centerLocalOffset = centerTrans and centerTrans.localPosition.y or 0

        if notAdjustToCenter ~= true then
            commonYOffset = commonYOffset - centerLocalOffset * totalScale
        end
        LuaUtils.SetLocalPositionY(rootRo.transform, commonYOffset)
        -- 底座
        local palletRo = CRenderObject.CreateRenderObject(rootRo.gameObject, "Pallet")
        palletRo:AddLoadAllFinishedListener(DelegateFactory.Action_CRenderObject(function(renderObject)
            local roLayer = layer and layer or LayerDefine.ModelForNGUI_3D
            renderObject.Layer = roLayer
            renderObject.Scale = totalScale
        end))
        palletRo:LoadMain(self:GetSoulCorePalletIdPath(DisplayPalletId), nil, true, false)

        -- 特效
        local colors = nil
        local fx = CEffectMgr.Inst:AddObjectFX(self:GetSoulCoreFxId(DisplayFxId),coreRo,0,totalScale,1,colors,false,EnumWarnFXType.None, Vector3(0, centerLocalOffset, 0), Vector3.zero, DelegateFactory.Action_GameObject(function(go)
            self:SetSoulCoreFxColor(go, true, mingGe)
            LuaUtils.SetLocalScale(go.transform,totalScale,totalScale,totalScale)
        end))
        coreRo:AddFX("DisplayFx", fx)

        local fx = CEffectMgr.Inst:AddObjectFX(self:GetSoulCoreParticlesId(DisplayCoreParticles),coreRo,0,totalScale,1,colors,false,EnumWarnFXType.None, Vector3(0, centerLocalOffset, 0), Vector3.zero, DelegateFactory.Action_GameObject(function(go)
            self:SetSoulCoreFxColor(go, false, mingGe)
            LuaUtils.SetLocalScale(go.transform,totalScale,totalScale,totalScale)
        end))
        coreRo:AddFX("DisplayCoreParticles", fx)

        local fx = CEffectMgr.Inst:AddObjectFX(self:GetSoulCoreCoreEffectId(DisplayCoreEffect, changeType),coreRo,0,scale,1,colors,false,EnumWarnFXType.None, Vector3(0, centerLocalOffset, 0), Vector3.zero, DelegateFactory.Action_GameObject(function(go)
            self:SetSoulCoreFxColor(go, false, mingGe)
            LuaUtils.SetLocalScale(go.transform,scale,scale,scale)
        end))
        coreRo:AddFX("DisplayCoreEffect", fx)
    end))
    coreRo:LoadMain(self:GetSoulCoreCorePath(DisplayCoreId), nil, true, false)

    local roLayer = layer and layer or LayerDefine.ModelForNGUI_3D
    rootRo.Layer = roLayer
    return rootRo
end

function LuaZongMenMgr:GetCondenseShortenTime(quality)
    local shortenTime = 0
    local formulaId = SoulCore_Settings.GetData().CondenseShortenFormulaId
    local formula = AllFormulas.Action_Formula[formulaId].Formula
    if formula ~= nil then
        shortenTime = formula(nil, nil, {quality})
    end
    return shortenTime
end

function LuaZongMenMgr:SyncCondenseLeftTime(bPause, endTimestamp, leftTime, speed, phase, totalTime)
    LuaZongMenMgr.m_SoulCoreCondensePause = bPause
    LuaZongMenMgr.m_SoulCoreCondenseEndTime = endTimestamp
    LuaZongMenMgr.m_SoulCoreCondenseLeftTime = leftTime
    LuaZongMenMgr.m_SoulCoreCondenseSpeed = speed
    LuaZongMenMgr.m_SoulCoreCondensePhase = phase
    LuaZongMenMgr.m_SoulCoreCondenseTotoalTime = totalTime

    g_ScriptEvent:BroadcastInLua("SoulCoreCondenseUpdate")
end

function LuaZongMenMgr:ReplySubmitCondenseItem(bOk, endTimestamp)
    if bOk then
        LuaZongMenMgr.m_SoulCoreCondenseEndTime = endTimestamp
        g_ScriptEvent:BroadcastInLua("SoulCoreCondenseJiasu")
    end
end

function LuaZongMenMgr:SyncCondenseWeatherChange(speed, endTimestamp)
    LuaZongMenMgr.m_SoulCoreCondenseSpeed = speed
    LuaZongMenMgr.m_SoulCoreCondenseEndTime = endTimestamp
    g_ScriptEvent:BroadcastInLua("SoulCoreCondenseUpdate")
end

function LuaZongMenMgr:ReplyPauseCondense(bOk, endTimestamp, leftTime)
    if bOk then
        LuaZongMenMgr.m_SoulCoreCondensePause = true
        g_ScriptEvent:BroadcastInLua("SoulCoreCondenseUpdate")
    end
end

function LuaZongMenMgr:ReplyResumeCondense(bOk, endTimestamp)
    if bOk then
        LuaZongMenMgr.m_SoulCoreCondensePause = false
        g_ScriptEvent:BroadcastInLua("SoulCoreCondenseUpdate")
    end
end

function LuaZongMenMgr:SyncFinishCondense()
    g_ScriptEvent:BroadcastInLua("SoulCoreCondenseFinish")
end

function LuaZongMenMgr:SyncFinishCarving()
    CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.SoulCoreWnd)
end

function LuaZongMenMgr:TryStartCondense()
    local npcId = tonumber(Menpai_Setting.GetData("TongTianTaNpcId").Value)
    local mapId = tonumber(Menpai_Setting.GetData("RaidMapId").Value)
    local msg = g_MessageMgr:FormatMessage("SECT_SOULCORE_NINGJIE_CONFIRM")
    MessageWndManager.ShowConfirmMessage(msg, 10, true, DelegateFactory.Action(function ()
        CTrackMgr.Inst:FindNPC(npcId, mapId, 0, 0, nil, DelegateFactory.Action(function ()
            Gac2Gas.RequestStartCondense()
        end))
    end), nil)
end
--灵核凝结end
--长生诀
function LuaZongMenMgr:GetAdjustHpSkillLevel(oriLevel, playerLevel)
    local skillCls = SoulCore_Settings.GetData().HpSkills

    for lv = oriLevel, 1, -1 do
        local DesignData = SoulCore_HpSkills.GetData(skillCls * 100 + lv)
        if DesignData and DesignData.PlayerLearnLv and DesignData.PlayerLearnLv <= playerLevel then
            return lv
        end
    end

    return 1
end
function LuaZongMenMgr:GetHpSkillExtraValue(hpSkillLv, wqxSkillLv)
    local formulaId = 955
    local formula = AllFormulas.Action_Formula[formulaId].Formula

    local extraValue = 0
    if formula ~= nil then
        extraValue = formula(nil, nil, {wqxSkillLv, hpSkillLv})
    end

    return extraValue
end
--长生诀end
--师门秘术
--根据灵核等级修正师门秘术等级
function LuaZongMenMgr:GetPlayerAdjustSoulCoreLevel(fanShenLevel, xianShenLevel, skillProp)
    local soulCoreProp = skillProp.SoulCore
    local oriLevel = soulCoreProp.Level
    return LuaZongMenMgr:GetAdjustSoulCoreLevel(fanShenLevel, xianShenLevel, oriLevel)
end

function LuaZongMenMgr:GetPlayerAdjustShiMenMiShuSkillLv(realSkillId, playerLv, skillProp)
	local realSkillLv = math.floor(realSkillId % 100)
    if realSkillLv == 0 then
        return realSkillLv
    end

    -- 解耦拿不到cls
    -- local cls = playerCls
	-- if SoulCore_ShiMenMiShu.GetData(cls).SkillCls ~= skillCls then
	-- 	return
	-- end

    local adjustSoulCoreLv = self:GetPlayerAdjustSoulCoreLevel(0, playerLv, skillProp)
    for lv = realSkillLv, 1, -1 do
        local lvData = SoulCore_ShiMenMiShuLevel.GetData(lv)
		if lvData.PlayerLevel <= playerLv and lvData.SoulCoreLevel <= adjustSoulCoreLv then
			return lv
		end
    end
    
    return 1
end

function LuaZongMenMgr:IsShiMenMiShuCls(skillCls)
    if self.m_ShiMenMiShuSkills == nil then
        self.m_ShiMenMiShuSkills = {}

        SoulCore_ShiMenMiShu.Foreach(function (k, v)
            self.m_ShiMenMiShuSkills[v.SkillCls] = k
        end)
    end

    return self.m_ShiMenMiShuSkills[skillCls] and true or false
end
--师门秘术end
--克符
function LuaZongMenMgr:IsAntiprofessionItem(templateId)
    if self.m_AntiProfessionItemIds == nil then
        self.m_AntiProfessionItemIds = {}
        local data = SoulCore_Settings.GetData().AntiProfessionColorId
        for i = 0, data.Length - 1 do 
            self.m_AntiProfessionItemIds[data[i][0]] = 1
        end
    end

    return self.m_AntiProfessionItemIds[templateId] ~= nil
end

function LuaZongMenMgr:GetProfessionScoreIconPath(profession)
    local iconsNames = SoulCore_Settings.GetData().AntiProfession_UpGradeResourceId
    local iconsName = iconsNames[profession - 1]
    return SafeStringFormat3("UI/Texture/Item_Other/Material/%s.mat", iconsName)
end

function LuaZongMenMgr:CalMainPlayerAntiProfessionLevel(needAdjust)
	local calLevelTable = {}
    CommonDefs.DictIterate(CClientMainPlayer.Inst.SkillProp.AntiProfessionSkill, DelegateFactory.Action_object_object(function (idx, AntiProfessionSkillObject)
        table.insert(calLevelTable, {idx, AntiProfessionSkillObject.CurLevel})
    end))

    return self:CalAntiProfessionLevel(calLevelTable, needAdjust)
end

function LuaZongMenMgr:CalMainPlayerAdjustAntiProfessionLevel()
    return self:CalMainPlayerAntiProfessionLevel(true)
end

function LuaZongMenMgr:CalAntiProfessionLevel(calLevelTable, needAdjust)
	local exp = 0
    
    for k, v in ipairs(calLevelTable) do
        local index, level = v[1], v[2]
        local AntiProfessionLv = needAdjust == true and self:GetAdjustAntiProfessionSkillLevel(index, level) or level
        if AntiProfessionLv > 0 then
            exp = exp + SoulCore_AntiProfessionLevel.GetData(AntiProfessionLv).LianFuExp * SoulCore_AntiProfessionSlot.GetData(index).ExpAdditionRate
        end
    end
    
	local targetId = 1
	for id = 2, 1000 do
		local data = SoulCore_AntiProfessionExp.GetData(id)
		if not data then
			break
		end

		if exp < data.LianFuExp then
			break
		end

		targetId = id
	end

	return SoulCore_AntiProfessionExp.GetData(targetId).LianFuLevel, targetId
end

function LuaZongMenMgr:IsAntiProfessionBuff(buffId)
    local baseBuffId = buffId
    return baseBuffId == SoulCore_Settings.GetData().AntiProfession_BuffId
end

function LuaZongMenMgr:GetAdjustAntiProfessionSkillLevel(index, oriLevel)
    if oriLevel == 0 then
        return 0
    end

    local playerLevel = CClientMainPlayer.Inst.Level
    local adjustedSoulCoreLv = self:GetMainPlayerAdjustSoulCoreLevel()
    for lv = oriLevel, 1, -1 do
        local DesignData = SoulCore_AntiProfessionSkills.GetData(index * 100 + lv)
        if DesignData and DesignData.PlayerLearnLv and DesignData.PlayerLearnLv <= playerLevel and DesignData.SoulCoreLevel <= adjustedSoulCoreLv then
            return lv
        end
    end

    return 1
end

function LuaZongMenMgr:AntiProfession_RandomChangeScore(infos)
    self.m_AntiProfessionConvertResult = infos

    CUIManager.ShowUI(CLuaUIResources.AntiProfessionConvertResultWnd)
end

function LuaZongMenMgr:FindLianYaoFaZheng()
    local lianHuaGuaiPosAndArea = LianHua_Setting.GetData().LianHuaGuaiPosAndArea
    self:TrackToMySect(lianHuaGuaiPosAndArea[0], lianHuaGuaiPosAndArea[1])
end

function LuaZongMenMgr:GetXingWuTemplateID()
    if self.m_XingWuTemplateID == nil then
        self.m_XingWuTemplateID = tonumber(Menpai_Setting.GetData("XingWuTemplateID").Value)
    end
    return self.m_XingWuTemplateID
end
--克符end
function LuaZongMenMgr:GetXingWuItemInfo()
    local default, pos, itemId
    if not self.m_XingWuItem then
        default, pos, itemId = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, self:GetXingWuTemplateID())
    else
        itemId = self.m_XingWuItem.Id
        local itemInfo = CItemMgr.Inst:GetItemInfo(itemId)
        if itemInfo then
            pos = itemInfo.pos
        end
    end
    
    local itemPlace = EnumToInt(EnumItemPlace.Bag)
    return default, pos, itemId, itemPlace
end

function LuaZongMenMgr:GetCultShortenValue(grade)
    local ShortenValue = 0
    local formulaId = tonumber(Menpai_Setting.GetData("CultValueShortenFormulaId").Value)
    local formula = AllFormulas.Action_Formula[formulaId].Formula
    if formula ~= nil then
        ShortenValue = formula(nil, nil, {grade})
    end
    return ShortenValue
end

function LuaZongMenMgr:SyncSectEvilValue(bSuccess, sectId, evilValue, bEvil)
    if bSuccess then
        g_ScriptEvent:BroadcastInLua("SyncSectEvilValue", evilValue, bEvil)
    end
end

function LuaZongMenMgr:ReplySubmitSectEvilYaoGuai(bSuccess, sectId, evilValue, bEvil)
    if bSuccess then
        g_ScriptEvent:BroadcastInLua("ReplySubmitSectEvilYaoGuai", evilValue, bEvil)
    end
end

function LuaZongMenMgr:OpenZongMenCreateIntroductionWnd(itemId)
    if not CClientMainPlayer.Inst then return end 
    if CClientMainPlayer.Inst.BasicProp.SectId ~= 0 then
        g_MessageMgr:ShowMessage("SECT_CREATE_IN_SECT")
        return
    end
    if itemId then
        local item = CItemMgr.Inst:GetById(itemId)
        self.m_XingWuItem = item
        if item then
            local extraData = item.Item.ExtraVarData
            if extraData then
                local arr = MsgPackImpl.unpack(extraData.Data) 
                if arr then
                    self.m_RuMenGuiZe = arr
                    CUIManager.ShowUI(CLuaUIResources.ZongMenCreateIntroductionWnd)
                end
            end
        end
    end
end

function LuaZongMenMgr:OpenZongMenCreateWnd()
    CUIManager.CloseUI(CLuaUIResources.ZongMenCreateIntroductionWnd)
    local default, itemPos, itemId,itemPlace = self:GetXingWuItemInfo()
    --Gac2Gas.RequestCreateSectPrecheck(itemPlace,itemPos, itemId)
    CUIManager.ShowUI(CLuaUIResources.ZongMenCreationWnd)
end

LuaZongMenMgr.m_NiuZhuanWuXingResult = 0
LuaZongMenMgr.m_WuXingBeforeNiuZhuan = 0
LuaZongMenMgr.m_NiuZhuanWuXingEndCDTime = 0
LuaZongMenMgr.m_ForceShowNiuZhuanWuXingWnd = false
function LuaZongMenMgr:ChangeNiuZhuanWuXingResult(lastWuXing, wuXing, endCDTime)
    local isNiuZhuan = lastWuXing ~= wuXing
    self.m_WuXingBeforeNiuZhuan = lastWuXing
    self.m_NiuZhuanWuXingResult = isNiuZhuan and wuXing or 0
    self.m_NiuZhuanWuXingEndCDTime = CServerTimeMgr.Inst.timeStamp + endCDTime
    if self.m_NiuZhuanWuXingResult ~= 0 then
        self:SyncLianHuaFaZhen(self.m_NiuZhuanWuXingResult)
    end
    g_ScriptEvent:BroadcastInLua("OnNiuZhuanWuXingResult", lastWuXing, wuXing, CServerTimeMgr.Inst.timeStamp + endCDTime)
end
function LuaZongMenMgr:ShowZongMenWuXingNiuZhuanWnd(lastWuXing, wuXing, endCDTime)
    self:ChangeNiuZhuanWuXingResult(lastWuXing, wuXing, endCDTime)
    if not self.m_ForceShowNiuZhuanWuXingWnd then return end
    self.m_ForceShowNiuZhuanWuXingWnd = false
    if LuaZongMenMgr.m_OpenNewSystem then
		CUIManager.ShowUI(CLuaUIResources.NewZongMenWuXingNiuZhuanWnd)
		return
	end
    CUIManager.ShowUI(CLuaUIResources.ZongMenWuXingNiuZhuanWnd)
end
function LuaZongMenMgr:ClearNiuZhuanWuXingResult()
    self.m_NiuZhuanWuXingResult = 0
    self.m_WuXingBeforeNiuZhuan = 0
    self.m_NiuZhuanWuXingEndCDTime = 0
    self.m_ForceShowNiuZhuanWuXingWnd = false
end

function LuaZongMenMgr:RequestTempChangeSectWuxing(data)
    self.m_ForceShowNiuZhuanWuXingWnd = true
    
    local dict = MsgPackImpl.unpack(data)

    local place = dict.Place
    local pos = dict.Pos
    local itemId = dict.ItemId

    if place and pos and itemId and LuaZongMenMgr.m_SyncCurrentSectId then
        Gac2Gas.RequestTempChangeSectWuxing(LuaZongMenMgr.m_SyncCurrentSectId, place, pos, itemId)
    end
end

function LuaZongMenMgr:OpenZongMenTuDiJoiningWnd()
    local tudiList = CShiTuMgr.Inst:GetAllCurrentTuDi()
    local len = tudiList.Count
    local relationshipProp = CClientMainPlayer.Inst.RelationshipProp
    local infoDic = relationshipProp.TuDi
    local newLen = 0
    for i = 0, len - 1 do
        local info = CommonDefs.DictGetValue(infoDic, typeof(UInt64), tudiList[i])
        local isWaiMen = info and info.Extra:GetBit(2) or false
        if not isWaiMen then
            newLen = newLen + 1
        end
    end
    if newLen > 0 then
        CUIManager.ShowUI(CLuaUIResources.ZongMenTuDiJoiningWnd)
    end
end

function LuaZongMenMgr:OnZongMenMingGeJianDingWnd()
    g_ScriptEvent:BroadcastInLua("OnZongMenMingGeJianDing")
    CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.WatchWuXingMingGe)
end

function LuaZongMenMgr:OpenMingGeJianDingWnd()
    CUIManager.CloseUI(CLuaUIResources.NewMingGeJianDingWnd)
	CUIManager.CloseUI(CLuaUIResources.MingGeJianDingWnd)
	if LuaZongMenMgr.m_OpenNewSystem then
		CUIManager.ShowUI(CLuaUIResources.NewMingGeJianDingWnd)
		return
	end
	CUIManager.ShowUI(CLuaUIResources.MingGeJianDingWnd)
end

function LuaZongMenMgr:OnZongMenGuiZeWnd(title, guize, isRule,raidMapId)
    self.m_GuiZeTitle = title
    self.m_GuiZe = guize
    self.m_GuiZeIsRule = isRule
    self.m_GuiZeRaidMapId = raidMapId
    CUIManager.ShowUI(CLuaUIResources.ZongMenGuiZeWnd)
end

LuaZongMenMgr.m_JoinZongMenParams= {}
-- 第三个参数,是否为IM消息，此外有可能是某个频道中发的
function LuaZongMenMgr:JoinZongMen(id, bByChatLink, inviterId, bIm)
    self.m_JoiningZongMenId = id
    -- 新增参数 bByChatLink ,含义是否为点击邀请链接
    -- 新增参数 inviterId, 含义是邀请者角色id,无人邀请则填0
    inviterId = inviterId and inviterId or 0
    bByChatLink = bByChatLink and true or false
    bIm = bIm and true or false
    LuaZongMenMgr.m_JoinZongMenParams = {self.m_JoiningZongMenId, bByChatLink, inviterId, bIm}
    Gac2Gas.RequestJoinSect(self.m_JoiningZongMenId, bByChatLink, inviterId, bIm)
    --CUIManager.ShowUI(CLuaUIResources.ZongMenJoiningWnd)
end

function LuaZongMenMgr:TopAndRightMenuWndInit(wnd)
    local sceneTemplateId = 0
    if CScene.MainScene then
        sceneTemplateId = CScene.MainScene.SceneTemplateId
    end
    LuaTopAndRightMenuWndMgr:ShowTopRightWnd(wnd, CLuaUIResources.ZongMenTopRightWnd,function ()
        local isShowWnd = false
        if tonumber(Menpai_Setting.GetData("RaidMapId").Value) == sceneTemplateId then
            isShowWnd = CClientMainPlayer.Inst.BasicProp.SectId == self.m_SyncCurrentSectId
        end
        return isShowWnd
    end)
end

function LuaZongMenMgr:ConfirmRenMian(msg1, msg2,rmTbl, addTbl,isZhangMenChanged)
    self.m_ConfirmRenMianMsg1,self.m_ConfirmRenMianMsg2,self.m_ConfirmRenMianRmTbl,self.m_ConfirmRenMianAddTbl,self.m_IsChangedZhangMenWeiJie = msg1,msg2,rmTbl, addTbl,isZhangMenChanged
    CUIManager.ShowUI(CLuaUIResources.ZongMenRenMianMessageBoxWnd)
end

function LuaZongMenMgr:GetCustomPropertyMapData(infoType, ud, checkinfoType, propertyName, saveList)
    if infoType == checkinfoType then
        local info = CustomPropertySerializable(propertyName)
        info:LoadFromString(ud)
        if checkinfoType ~= "history" then
            local map = info.Infos
            CommonDefs.DictIterate(map, DelegateFactory.Action_object_object(function (___key, ___value)
                table.insert(saveList, ___value)
            end)) 
        else
            local map = info.HistoryList
            CommonDefs.DictIterate(map, DelegateFactory.Action_object_object(function (___key, ___value)
                table.insert(saveList, ___value)
            end)) 
        end
        
    end
end

function LuaZongMenMgr:ShowMemberViewPlayerPopupMenu(memberInfo)
    if CClientMainPlayer.Inst and memberInfo then
        self.m_TempMemberView_PlayerPopupMenu_MemberInfo = memberInfo
        Gac2Gas.QueryPlayerSectSpeakStatus(CClientMainPlayer.Inst.BasicProp.SectId, memberInfo.PlayerId)
    end
end

function LuaZongMenMgr:SyncAddSectEntrance(sectId, mapId, x, y, bXiePai)
    if not CRenderScene.Inst then return end
    if CClientMainPlayer.Inst then
        if CClientMainPlayer.Inst.BasicProp.SectId == sectId then
            CRenderScene.Inst:OnUpdateSectEntranceTrapInfo(mapId, x, y,bXiePai,SectEntranceTrapType.Default,- 1)
        end
    end
end

function LuaZongMenMgr:SyncRmSectEntrance(sectId)
    if not CRenderScene.Inst then return end
    CRenderScene.Inst:ClearSectTrap()
    EventManager.Broadcast(EnumEventType.OnSendExtraTrapInfo);
end
------------------------------------------
-- 炼化相关
------------------------------------------
LuaZongMenMgr.m_LingLiParams = {}
LuaZongMenMgr.m_RefineMonsterStatusTable = {}
LuaZongMenMgr.m_RefineMonsterOpenEvil = false
LuaZongMenMgr.m_RefineMonsterItemLabels = {}
LuaZongMenMgr.m_RefineMonsterCommonItem = nil
-- 炼人状态同步的listener是否开启
LuaZongMenMgr.m_IsLianHuaEnable = false
LuaZongMenMgr.m_LianRenTicks = {}
LuaZongMenMgr.m_LianYaoTick = nil
LuaZongMenMgr.m_LianRenInfo = nil
-- 判断当前是哪个炼人法阵需要开启按钮
LuaZongMenMgr.m_LianRenBtnPos = nil
-- 炼怪
LuaZongMenMgr.m_LianHuaGuaiRo = nil
LuaZongMenMgr.m_LianHuaGuaiTempalteId = nil
-- 自己宗派的炼妖法阵是否打开
LuaZongMenMgr.m_IsFaZhenOpen = false
LuaZongMenMgr.m_FaZhenBtnTick = nil
LuaZongMenMgr.m_IsXiepai = 0
LuaZongMenMgr.m_FaZhenBtnAction = nil
-- 五行
LuaZongMenMgr.m_ZongMenWuXing = -1
LuaZongMenMgr.m_ZongMenWuXingFxs = {}
LuaZongMenMgr.m_OnClientCreateAction = nil
LuaZongMenMgr.m_OnMainPlayerDestroyedAction = nil
LuaZongMenMgr.m_OnClientObjVisibleAction = nil
-- 灵核
LuaZongMenMgr.m_LingHeGos = {}
-- 灵材提交界面
LuaZongMenMgr.m_LingCaiCount = nil
LuaZongMenMgr.m_AdvanceLingCaiCount = nil
LuaZongMenMgr.m_LingCaiLowest = nil
LuaZongMenMgr.m_AdvanceLingCaiLowest = nil
LuaZongMenMgr.m_ZongMenLingCaiSum = nil
LuaZongMenMgr.m_PersonalLingCaiSum = nil
LuaZongMenMgr.m_LingCaiTypeList = nil
LuaZongMenMgr.m_IsAdvanceCommit = nil

function LuaZongMenMgr:OpenLingCaiChooseWnd(isAdvance)
    self.m_IsAdvanceCommit = isAdvance
    CUIManager.ShowUI(CLuaUIResources.RefineMonsterItemChooseWnd)
end

function LuaZongMenMgr:GetZongMenLingCaiParam()
    if LuaZongMenMgr.m_ZongMenLingCaiSum then
        return GetFormula(956)(nil, nil, {LuaZongMenMgr.m_ZongMenLingCaiSum})
    end
end

function LuaZongMenMgr:GetPersonalLingCaiParam()
    if self.m_ZongMenLingCaiSum and self.m_PersonalLingCaiSum and self.m_RefineMonsterStatusTable 
        and self.m_RefineMonsterStatusTable["linggen"] and self.m_RefineMonsterStatusTable ["xiepaiAddFactor"] then
        
        local e4 = 0
        if LuaZongMenMgr.m_RefineMonsterOpenEvil then
            e4 = self.m_RefineMonsterStatusTable ["xiepaiAddFactor"]
        end

        return GetFormula(980)(nil, nil, {self.m_PersonalLingCaiSum, self:GetZongMenLingCaiParam() or 0, self.m_RefineMonsterStatusTable["linggen"], e4})
    end
end

function LuaZongMenMgr:GetLingCaiParam(lingli)
    if self.m_ZongMenLingCaiSum and self.m_PersonalLingCaiSum  and self.m_RefineMonsterStatusTable 
        and self.m_RefineMonsterStatusTable["linggen"] and self.m_RefineMonsterStatusTable ["xiepaiAddFactor"] then

        local e4 = 0
        if LuaZongMenMgr.m_RefineMonsterOpenEvil then
            e4 = self.m_RefineMonsterStatusTable ["xiepaiAddFactor"]
        end

        return GetFormula(980)(nil, nil, {lingli, self:GetZongMenLingCaiParam() or 0, self.m_RefineMonsterStatusTable["linggen"], e4})
    end
end


function LuaZongMenMgr:StartLianHuaFaZhenBtnTick(isOpen)
    LuaZongMenMgr.m_IsFaZhenOpen = isOpen
    g_ScriptEvent:Broadcast("UpdateRefineStatus", isOpen)

    if self.m_FaZhenBtnAction == nil then
        self.m_FaZhenBtnAction = DelegateFactory.Action(function()
            self:EndLianHuaFaZhenBtnTick()
        end)
    end

    if self.m_FaZhenBtnTick ~= nil then
        UnRegisterTick(self.m_FaZhenBtnTick)
        self.m_FaZhenBtnTick = nil
    end

    self.m_FaZhenBtnTick = RegisterTick(function()
        if CClientMainPlayer.Inst and CClientMainPlayer.Inst.RO then
            local myPos = CClientMainPlayer.Inst.RO.transform.position
            local settingPos = LianHua_Setting.GetData().LianHuaGuaiPosAndArea
            local x = math.floor(settingPos[0])
            local y = math.floor(settingPos[1])
            local fazhenPos = Utility.PixelPos2WorldPos(CreateFromClass(CPos, x, y))
            local r = settingPos[2]
            -- 不考虑高度
            myPos.y = fazhenPos.y

            -- 处理炼妖法阵
            local dis = Vector3.Distance(myPos, fazhenPos)
            if dis < r then
                if not CUIManager.IsLoaded(CLuaUIResources.RefineMonsterButtonWnd) and not CUIManager.IsLoaded(CUIResources.ShoppingMallWnd) then
                    Gac2Gas.RequestReceiveLianHuaLingLiAward()
                    CUIManager.ShowUI(CLuaUIResources.RefineMonsterButtonWnd)
                elseif CUIManager.IsLoaded(CUIResources.ShoppingMallWnd) then
                    CUIManager.CloseUI(CLuaUIResources.RefineMonsterButtonWnd)
                end
            -- 关界面时加一点距离 防止在一个范围抖动
            elseif dis > r+0.1 or CUIManager.IsLoaded(CUIResources.ShoppingMallWnd) then
                CUIManager.CloseUI(CLuaUIResources.RefineMonsterButtonWnd)
            end

            -- 处理炼人法阵
            local lianrenData = LianHua_Setting.GetData().FaZhenFakeNpcInfo
            for i=1,2 do
                local x = math.floor(lianrenData[i*2 -1])
                local y = math.floor(lianrenData[i*2])
                local lianrenPos = Utility.PixelPos2WorldPos(CreateFromClass(CPos, x, y))
                myPos.y = lianrenPos.y
                local dis = Vector3.Distance(myPos, lianrenPos)
                if dis < r then
                    self.m_LianRenBtnPos = i
                    if not CUIManager.IsLoaded(CLuaUIResources.RefinePeopleButtonWnd) and self.m_LianRenInfo and self.m_LianRenInfo.npc[i]~=0 then
                        CUIManager.ShowUI(CLuaUIResources.RefinePeopleButtonWnd)
                    end
                -- 关界面时加一点距离 防止在一个范围抖动
                elseif self.m_LianRenBtnPos==i and dis > r+0.1 then
                    CUIManager.CloseUI(CLuaUIResources.RefinePeopleButtonWnd)
                end
            end
        end
    end, 100)
    EventManager.AddListenerInternal(EnumEventType.MainPlayerDestroyed, self.m_FaZhenBtnAction)
end

function LuaZongMenMgr:EndLianHuaFaZhenBtnTick()
    if self.m_FaZhenBtnTick~=nil then
        UnRegisterTick(self.m_FaZhenBtnTick)
        self.m_FaZhenBtnTick = nil
    end
    CUIManager.CloseUI(CLuaUIResources.RefineMonsterButtonWnd)
    CUIManager.CloseUI(CLuaUIResources.RefinePeopleButtonWnd)
    EventManager.RemoveListenerInternal(EnumEventType.MainPlayerDestroyed, self.m_FaZhenBtnAction)
end

function LuaZongMenMgr:ShowLingLiDescWnd(statusTable, lingLiParams)
    self.m_RefineMonsterStatusTable = statusTable or {}
    self.m_LingLiParams = lingLiParams or {}
    CUIManager.ShowUI(CLuaUIResources.RefineMonsterLingliDescWnd)
end

function LuaZongMenMgr:ScondToString(sconds)
    local days = math.floor(sconds / 86400)
    local mod = math.fmod(sconds, 86400)
    local hours = math.floor( mod / 3600)
    mod = math.fmod(mod, 3600)
    local mins = math.floor(mod / 60)

    if days == 0 then
        if hours == 0 then
            if mins == 0 then
                mins = 1
            end
            return SafeStringFormat3(LocalString.GetString("%d分"), mins)
        else
            return SafeStringFormat3(LocalString.GetString("%d时%d分"), hours, mins)
        end
    else
        return SafeStringFormat3(LocalString.GetString("%d天%d时%d分"), days, hours, mins)
    end
end

function LuaZongMenMgr:SyncLianYaoStatus(sectId, tuJianId, costTime, beginTime, isXiePai)
    self.m_IsXiepai = isXiePai
    if not CommonDefs.IsUnityObjectNull(self.m_LianHuaGuaiRo) and self.m_LianHuaGuaiTempalteId ~= tuJianId then
        -- 更新怪物信息
        self.m_LianHuaGuaiRo:Destroy()
        self.m_LianHuaGuaiRo = nil
    elseif self.m_LianHuaGuaiRo ~=nil and CommonDefs.IsUnityObjectNull(self.m_LianHuaGuaiRo) then
        -- 释放引用
        self.m_LianHuaGuaiRo = nil
    end

    if self.m_LianYaoTick ~=nil then
        UnRegisterTick(self.m_LianYaoTick)
        self.m_LianYaoTick = nil
    end

    local isSpecial = false
    local isSpecialFx = false
    if tuJianId~= 0 then
        local tujianData = ZhuoYao_TuJian.GetData(tuJianId)
        local scale = 1
        if tujianData.Special == 1 then
            isSpecialFx = true
        end
        if self.m_LianHuaGuaiRo == nil and tujianData then
            -- 创建新的怪物
            scale = tujianData.LianHuaScale
            local height = tujianData.LianHuaHeight
            local monsterId = tujianData.MonsterIds[0]
            local monsterData = Monster_Monster.GetData(monsterId)
            if monsterData~=nil then
                self.m_LianHuaGuaiTempalteId = tuJianId
                self.m_LianHuaGuaiRo = CRenderObject.CreateRenderObject(nil, "LianHuaGuai")
                self.m_LianHuaGuaiRo.Scale = scale

                if tuJianId == 100046 or tuJianId == 100049 then
                    self.m_LianHuaGuaiRo.NewRoleStandAni = "kunbang01"
                    self.m_LianHuaGuaiRo:AddLoadAllFinishedListener(DelegateFactory.Action_CRenderObject(function(ro)
                        local aniFast = ro.m_AnimationFast
                        if aniFast then
                            aniFast:Play("kunbang01", 0)
                        end
                    end))
                end

                local settingPos = LianHua_Setting.GetData().LianHuaGuaiPosAndArea
                local x = math.floor(settingPos[0])
                local y = math.floor(settingPos[1])
                local fazhenPos = Utility.PixelPos2WorldPos(CreateFromClass(CPos, x, y))
                fazhenPos.y = fazhenPos.y + height
                self.m_LianHuaGuaiRo.gameObject.transform.position = fazhenPos
                self.m_LianHuaGuaiRo.gameObject.transform.localRotation = Quaternion.Euler(Vector3(0,180,0))
                local path = CClientMonster.GetMonsterPrefabPath(monsterData)
                if string.find(path, "Special") == nil then
                    isSpecial = false
                    self.m_LianHuaGuaiRo:LoadMain(path, nil, false, false, false)
                else
                    isSpecial = true
                    self.m_LianHuaGuaiRo:LoadMain(path, nil, false, true, false)
                end
            end
        end

        -- 计算状态
        local delayTime = tujianData.Time/3 *2 - (CServerTimeMgr.Inst.timeStamp - beginTime)
        local state = 2
        local isSmallFx = tujianData.LianHuaFxType == 1

        if delayTime > 0 then
            state = 1
            self.m_LianYaoTick = RegisterTickOnce(function()
                if not CommonDefs.IsUnityObjectNull(self.m_LianHuaGuaiRo) then
                    self:AddLianHuaFx(self.m_LianHuaGuaiRo, 2, 1/scale, isSmallFx, isSpecial, isSpecialFx)
                end
            end, delayTime*1000)
        end

        if not CommonDefs.IsUnityObjectNull(self.m_LianHuaGuaiRo) then
            self:AddLianHuaFx(self.m_LianHuaGuaiRo, state, 1/scale, isSmallFx, isSpecial, isSpecialFx)
        end
    end
end

function LuaZongMenMgr:ShuffleTable(tbl)
    -- 打乱数组排序 用来随机灵核位置
    for i=1, #tbl do
        local randomIndex = UnityEngine_Random(i, #tbl + 1)
        local randomValue = tbl[randomIndex]
        tbl[randomIndex] = tbl[i]
        tbl[i] = randomValue
    end
end

function LuaZongMenMgr:SyncLingHeStatus(lingHeList)
    self.m_SelfSoulCoreGo = nil
    if self.m_LingHeGos then
        for i=1, #self.m_LingHeGos do
            if not CommonDefs.IsUnityObjectNull(self.m_LingHeGos[i]) then
                local ro = CommonDefs.GetComponentsInChildren_Component_Type(self.m_LingHeGos[i].transform, typeof(CRenderObject))
                for i=0, ro.Length-1 do
                    ro[i]:Destroy()
                end
                GameObject.Destroy(self.m_LingHeGos[i])
            end
        end
    end
    self.m_LingHeGos = {}

    local data = LianHua_Setting.GetData()
    local height = data.LingHeHight
    local scale = data.LingHeScale
    local moveRange1, moveRange2 = data.LingHeMoveRange[0], data.LingHeMoveRange[1]
    local timeRange1, timeRange2 = data.LingHeMoveTimeRange[0], data.LingHeMoveTimeRange[1]
    local rotateTimeRange1, rotateTimeRange2 = data.LingHeRotateTimeRange[0], data.LingHeRotateTimeRange[1]

    local indexList = {1,2,3,4,5,6,7,8,9,10}
    local lianGuaiPos = LianHua_Setting.GetData().LianHuaGuaiPosAndArea
    local lianRenPos = LianHua_Setting.GetData().FaZhenFakeNpcInfo
    local fazhenPosList = {lianGuaiPos[0], lianGuaiPos[1], lianRenPos[1], lianRenPos[2], lianRenPos[3], lianRenPos[4]}
    local lingHePosOffset = data.LingHePosOffset

    local lingHeTbl = {}
    for i=0, lingHeList.Count -8, 8 do
        local t = {
            Id = lingHeList[i],
            Level = lingHeList[i+1],
            DisplayCoreId = lingHeList[i+2],
            DisplayFxId = lingHeList[i+3],
            DisplayPalletId = lingHeList[i+4],
            DisplayCoreParticles = lingHeList[i+5],
            DisplayCoreEffect = lingHeList[i+6],
            MingGe = lingHeList[i+7],    
        }
        table.insert(lingHeTbl, t)
    end

    table.sort(lingHeTbl, function(n1,n2)
        if n1.Level ~= n2.Level then
            return n1.Level > n2.Level
        else
            return n1.Id > n2.Id
        end
    end)

    for i = 1, #lingHeTbl do
        local data = lingHeTbl[i]
        local fazhenIndex = math.floor((i-1)/10)
        -- 最多处理0 1 2三个法阵的数据
        if fazhenIndex > 2 then
            return
        end

        local x = fazhenPosList[fazhenIndex * 2 +1]
        local y = fazhenPosList[fazhenIndex * 2 + 2]

        local index = i - fazhenIndex*10
        local offsetX = lingHePosOffset[index*2-2]
        local offsetY = lingHePosOffset[index*2-1]
        local lingHePos = Utility.PixelPos2WorldPos(CreateFromClass(CPos, math.floor(x+offsetX), math.floor(y+offsetY)))
        local moveRange = UnityEngine_Random(moveRange1 + 0.01, moveRange2+0.01)
        lingHePos.y = lingHePos.y + height - moveRange/2

        local go = CreateFromClass(GameObject, "LingHe")
        go.transform.position = lingHePos
        table.insert(self.m_LingHeGos, go)

        -- 创建灵核
        LuaZongMenMgr:CreateSoulCoreGameObject(go, data, data.MingGe, LayerDefine.Default, scale, true)
        LuaTweenUtils.SetLoops(LuaTweenUtils.SetEase(LuaTweenUtils.DOMoveY(go.transform, lingHePos.y + moveRange, UnityEngine_Random(timeRange1 + 0.01, timeRange2 +0.01), false), Ease.InOutSine), -1, true)
        CommonDefs.SetLoops_Tweener(LuaTweenUtils.SetEase(LuaTweenUtils.DOLocalRotate(go.transform, Vector3(0,360,0), UnityEngine_Random(rotateTimeRange1+0.01, rotateTimeRange2+0.01), RotateMode.FastBeyond360), Ease.Linear), -1)
        
        if CClientMainPlayer.Inst.Id == data.Id then
            self.m_SelfSoulCoreGo = go
            g_ScriptEvent:BroadcastInLua("MainPlayerSoulCoreCreate")
        end
    end
end

-- 0是开始 1第一阶段 2是第二阶段 3是结束
function LuaZongMenMgr:SyncLianRenStatus(sectId, playerId1, engineId1, time1, playerId2, engineId2, time2, isXiepai, wuxing)
    local players = {playerId1, playerId2}
    local npcs = {engineId1, engineId2}
    local times = {time1, time2}
    self.m_LianRenInfo = {}
    self.m_LianRenInfo.player = players
    self.m_LianRenInfo.npc = npcs
    self.m_LianRenInfo.time = times
    self.m_IsXiepai = isXiepai

    self:SyncLianHuaFaZhen(self.m_NiuZhuanWuXingResult ~= 0 and self.m_NiuZhuanWuXingResult or wuxing)

    -- 用于更新状态的炼人tick
    for i=1,#self.m_LianRenTicks do
        UnRegisterTick(self.m_LianRenTicks[i])
    end
    self.m_LianRenTicks = {}

    -- 刚刚接到rpc时 初始化一下
    for i=1,2 do
        local playerId = self.m_LianRenInfo.player[i]
        local npcId = self.m_LianRenInfo.npc[i]
        local time = self.m_LianRenInfo.time[i]

        -- 隐藏角色
        local player = CClientPlayerMgr.Inst:GetPlayer(playerId)
        if player then
            player:HideObject()
        end

        local clientObj = CClientObjectMgr.Inst:GetObject(npcId)
        local status, delayTime = self:GetLianHuaStatus(time)
        if clientObj and clientObj.RO then
            clientObj.RO:DoAni("lianhua01", true, 0, 1, 0, true, 1)
            self:AddLianRenFx(clientObj.RO, status)
        end

        if status == 0 or status == 1 then
            if delayTime >0 then
                local t = RegisterTickOnce(function()
                    local cbj = CClientObjectMgr.Inst:GetObject(npcId)
                    if cbj and cbj.RO then
                        self:AddLianRenFx(cbj.RO, 2)
                    end
                end, delayTime*1000)
                table.insert(self.m_LianRenTicks, t)
            end
        end
    end

    self:AddLianHuaListener()
end

-- 0是开始 1第一阶段 2是第二阶段 3是结束
function LuaZongMenMgr:ReleaseFaZhenBeiZhuoPlayer(index, playerId)
    if self.m_LianRenInfo then
        --local playerId = self.m_LianRenInfo.player[index]
        local npcId = self.m_LianRenInfo.npc[index]
        local time = self.m_LianRenInfo.time[index]

        -- 炼化信息清空
        self.m_LianRenInfo.player[index] = 0
        self.m_LianRenInfo.npc[index] = 0
        self.m_LianRenInfo.time[index] = 0

        -- 播放炼化特效 放下炼化人物
        local clientObj = CClientObjectMgr.Inst:GetObject(npcId)
        if clientObj and clientObj.RO then
            self:AddLianRenFx(clientObj.RO, 3, playerId, npcId)
        else
            self:AddLianRenFx(nil, 3, playerId, npcId)
        end
    end
end

-- 每次有新的clientObj创建时调用
function LuaZongMenMgr:OnClientCreate(engineId)
    local clientObj = CClientObjectMgr.Inst:GetObject(engineId)
    local otherPlayer = TypeAs(clientObj, typeof(CClientOtherPlayer))
    local mainPlayer = TypeAs(clientObj, typeof(CClientMainPlayer))
    
    if self.m_LianRenInfo then
        if otherPlayer or mainPlayer then
            -- 取ID
            local id = otherPlayer and otherPlayer.PlayerId
            if id == nil then
                id = mainPlayer and mainPlayer.Id
            end
            -- 当前的状态可以信任
            for i=1, #self.m_LianRenInfo.player do
                if id == self.m_LianRenInfo.player[i] then
                    clientObj:HideObject()
                end
            end
        elseif clientObj and clientObj.RO then
            local time = 0
            for i=1, #self.m_LianRenInfo.npc do
                if engineId == self.m_LianRenInfo.npc[i] and clientObj.RO then
                    local time = self.m_LianRenInfo.time[i]
                    local status = self:GetLianHuaStatus(time)
                    RegisterTickOnce(function()
                        clientObj.RO:DoAni("lianhua01", true, 0, 1, 0, true, 1)
                        self:AddLianRenFx(clientObj.RO, status)
                    end, 1)
                end
            end
        end
    end
end

-- 每次有新的clientObj创建时调用
function LuaZongMenMgr:OnClientObjVisible(engineId)
    local clientObj = CClientObjectMgr.Inst:GetObject(engineId)
    local otherPlayer = TypeAs(clientObj, typeof(CClientOtherPlayer))
    local mainPlayer = TypeAs(clientObj, typeof(CClientMainPlayer))
    
    if self.m_LianRenInfo then
        if otherPlayer or mainPlayer then
            -- 取ID
            local id = otherPlayer and otherPlayer.PlayerId
            if id == nil then
                id = mainPlayer and mainPlayer.Id
            end
            -- 当前的状态可以信任
            for i=1, #self.m_LianRenInfo.player do
                if id == self.m_LianRenInfo.player[i] and clientObj.RO then
                    if clientObj.RO.Visible == true then
                        clientObj.RO.Visible = false
                    end
                end
            end
        end
    end
end

-- 0是开始 1第一阶段 2是第二阶段 3是结束
-- 这里不处理结束 结束由RPC来处理
function LuaZongMenMgr:GetLianHuaStatus(catchTime)
    if catchTime == 0 then
        return 1, 0
    end

    local data = LianHua_Setting.GetData().LianRenDuration
    catchTime = CServerTimeMgr.Inst.timeStamp - catchTime
    local t1 = data[0]
    local t2 = data[1]

    -- 被捉两秒内 播放上升动画
    if catchTime < 2 then
        return 0, t2 - catchTime
    elseif catchTime < t2 then
        return 1, t2 - catchTime
    elseif catchTime < t1 then
        return 2, 0
    else
        return 2, 0
    end
end

-- 同步宗派法阵
function LuaZongMenMgr:SyncLianHuaFaZhen(wuxing)
    local data= LianHua_Setting.GetData().LianHuaFaZhenFx
    local fxId = data[wuxing]
    if fxId ~= 0 and (wuxing ~= self.m_ZongMenWuXing or #self.m_ZongMenWuXingFxs == 0) then
        for i=1, #self.m_ZongMenWuXingFxs do
            if self.m_ZongMenWuXingFxs[i] then
                self.m_ZongMenWuXingFxs[i]:Destroy()
            end
        end
        self.m_ZongMenWuXingFxs = {}

        -- 炼怪法阵
        local settingPos = LianHua_Setting.GetData().LianHuaGuaiPosAndArea
        local x = math.floor(settingPos[0])
        local y = math.floor(settingPos[1])
        local fazhenPos = Utility.PixelPos2WorldPos(CreateFromClass(CPos, x, y))
        local fx = CEffectMgr.Inst:AddWorldPositionFX(fxId, fazhenPos, 0, 0, 1, -1, EnumWarnFXType.None, nil, 0, 0, nil)
        table.insert(self.m_ZongMenWuXingFxs, fx)

        -- 炼人法阵
        local lianrenData = LianHua_Setting.GetData().FaZhenFakeNpcInfo
        for i=1,2 do
            local x = math.floor(lianrenData[i*2 -1])
            local y = math.floor(lianrenData[i*2])
            local fazhenPos = Utility.PixelPos2WorldPos(CreateFromClass(CPos, x, y))
            local fx = CEffectMgr.Inst:AddWorldPositionFX(fxId, fazhenPos, 0, 0, 1, -1, EnumWarnFXType.None, nil, 0, 0, nil)
            table.insert(self.m_ZongMenWuXingFxs, fx)
        end
    end
end

-- 添加炼人特效 与炼怪特效有些区别
-- 抬高与放下在这里进行
function LuaZongMenMgr:AddLianRenFx(ro, status, playerId, npcId)
    local height = LianHua_Setting.GetData().LianRenExtraHight
    local timeData = LianHua_Setting.GetData().LianRenUpDownTime
    -- 添加特效
    if not CommonDefs.IsUnityObjectNull(ro) and status == 0 then
        -- 开始阶段 有上升的动画
        -- 这时候可能没有mainObject
        local upTime = timeData[0]
        if ro.m_MainObject then
            local tweener = CommonDefs.SetEase_Tweener(LuaTweenUtils.DOLocalMoveY(ro.m_MainObject.transform, height, upTime, false), Ease.Linear)
            CommonDefs.OnComplete_Tweener(tweener, DelegateFactory.TweenCallback(
                function()
                        self:AddLianHuaFx(ro, status, 1, true)
                end))
        else
            ro:AddLoadAllFinishedListener(DelegateFactory.Action_CRenderObject(function(npcRO)
                local tweener = CommonDefs.SetEase_Tweener(LuaTweenUtils.DOLocalMoveY(npcRO.m_MainObject.transform, height, upTime, false), Ease.Linear)
                CommonDefs.OnComplete_Tweener(tweener, DelegateFactory.TweenCallback(
                    function()
                        self:AddLianHuaFx(npcRO, status, 1, true)
                    end))
            end))
        end
    elseif not CommonDefs.IsUnityObjectNull(ro) and status ~= 3 then
        ro.OffSet = Vector3(0,height,0)
        self:AddLianHuaFx(ro, status, 1, true)
    end

    -- 销毁特效
    -- 动画下降
    if status == 3 then
        local downTime = timeData[1]
        if ro~=nil and not CommonDefs.IsUnityObjectNull(ro)then
            self:RemoveLianHuaFx(ro)
            if ro.m_MainObject then
                CommonDefs.SetEase_Tweener(LuaTweenUtils.DOLocalMoveY(ro.m_MainObject.transform, 0, downTime, false), Ease.Linear)
            else
                ro:AddLoadAllFinishedListener(DelegateFactory.Action_CRenderObject(function(npcRO)
                    CommonDefs.SetEase_Tweener(LuaTweenUtils.DOLocalMoveY(npcRO.m_MainObject.transform, 0, downTime, false), Ease.Linear)
                end))
            end
        end

        RegisterTickOnce(function()
            if playerId then
                local player = CClientPlayerMgr.Inst:GetPlayer(playerId)
                if player then
                    player:ShowObject()
                    EventManager.BroadcastInternalForLua(EnumEventType.ClientObjCreate, {player.EngineId})
                end
            end

            if npcId then
                local clientObj = CClientObjectMgr.Inst:GetObject(npcId)
                if clientObj then
                    clientObj:HideObject()
                end
            end

        end, downTime * 1000)
    end
end

-- 添加炼化特效
function LuaZongMenMgr:AddLianHuaFx(ro, status, scale, isSmallFx, isSpecial, isSpecialFx)
    if CommonDefs.IsUnityObjectNull(ro) then
        return
    end

    if scale == nil then
        scale = 1
    end
    -- 材质特效
    local data = LianHua_Setting.GetData()
    local caiZhiFxId = 0

    local justiceFxId = data.LianHuaSuoLianJusticeFx
    local evilFxId = data.LianHuaSuoLianEvilFx
    local caizhiFxList = {data.LianHuaCaiZhiFx[0], data.LianHuaCaiZhiFx[1], data.LianHuaSpeicalCaiZhiFx[0], data.LianHuaSpeicalCaiZhiFx[1]}

    if isSmallFx then
        justiceFxId = data.LianRenSuoLianJusticeFx
        evilFxId = data.LianRenSuoLianEvilFx
    end

    local suoLianFxId, otherFxId

    if self.m_IsXiepai == 0 then
        suoLianFxId = justiceFxId
        otherFxId = evilFxId
        if isSpecial then
            caiZhiFxId = data.LianHuaSpeicalCaiZhiFx[0]
        else
            caiZhiFxId = data.LianHuaCaiZhiFx[0]
        end
    else
        suoLianFxId = evilFxId
        otherFxId = justiceFxId
        if isSpecial then
            caiZhiFxId = data.LianHuaSpeicalCaiZhiFx[1]
        else
            caiZhiFxId = data.LianHuaCaiZhiFx[1]
        end
    end

    -- 捉妖表对特殊怪物使用特殊材质特效
    if isSpecialFx then
        caiZhiFxId = 88802305
    end

    for i=1, #caizhiFxList do
        if caizhiFxList[i] ~= caiZhiFxId then
            local fx = ro:GetFxById(caizhiFxList[i])
            if fx~=nil then
                fx:Destroy()
            end
        end
    end 

    local mfx = ro:GetFxById(caiZhiFxId)
    if mfx == nil then
        mfx = CEffectMgr.Inst:AddObjectFX(caiZhiFxId, ro, 0, 1, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
    end

    -- 把不要的那个锁链特效删掉
    local ofx = ro:GetFxById(otherFxId)
    if ofx~=nil then
        ofx:Destroy()
    end

    local ifx = ro:GetFxById(suoLianFxId)
    if ifx == nil then
        -- 锁链特效
        local suoLianFx = CEffectMgr.Inst:AddObjectFX(suoLianFxId, ro, 0, scale, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, DelegateFactory.Action_GameObject(function(obj)
            local ani = CommonDefs.GetComponentInChildren_GameObject_Type(obj, typeof(Animator))
            if ani then
                ani:SetInteger("m_State", status)
            end
        end))
    else
        local cfx = TypeAs(ifx, typeof(CFX))
        if cfx ~= nil and cfx.m_FX then
            local ani = CommonDefs.GetComponentInChildren_GameObject_Type(cfx.m_FX.gameObject, typeof(Animator))
            if ani then
                ani:SetInteger("m_State", status)
            end
        else
            -- 这个时候处于加载特效中
            ifx:Destroy()
            -- 锁链特效
            local suoLianFx = CEffectMgr.Inst:AddObjectFX(suoLianFxId, ro, 0, scale, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, DelegateFactory.Action_GameObject(function(obj)
                local ani = CommonDefs.GetComponentInChildren_GameObject_Type(obj, typeof(Animator))
                if ani then
                    ani:SetInteger("m_State", status)
                end
            end))
        end
    end
end

-- 移除炼化特效
function LuaZongMenMgr:RemoveLianHuaFx(ro)
    local data = LianHua_Setting.GetData()
    local fxList = {data.LianHuaCaiZhiFx[0], data.LianHuaCaiZhiFx[1], data.LianHuaSpeicalCaiZhiFx[0], data.LianHuaSpeicalCaiZhiFx[1],
        data.LianHuaSuoLianJusticeFx, data.LianHuaSuoLianEvilFx, data.LianRenSuoLianJusticeFx, data.LianRenSuoLianEvilFx}

    for i=1, #fxList do
        local cfx = ro:GetFxById(fxList[i])
        if cfx ~= nil then
            cfx:Destroy()
        end
    end
end

function LuaZongMenMgr:AddZhuoRenFx(playerEngineId, attackerEngineId, hitDelay, returnDelay)
    local playerObj = CClientObjectMgr.Inst:GetObject(playerEngineId)
    local attackerObj = CClientObjectMgr.Inst:GetObject(attackerEngineId)

    if hitDelay == nil then
        hitDelay = 50
    end

    if returnDelay==nil then
        returnDelay = 1200
    end

    local mainPlayer = TypeAs(attackerObj, typeof(CClientMainPlayer))
    if mainPlayer then
        mainPlayer:StopAutoCastSkill(true)
    end

    if playerObj and attackerObj then
        CEffectMgr.Inst:AddLinkFX(88801986, attackerObj.RO, playerObj.RO, 0, 1, -1)
        CEffectMgr.Inst:AddObjectFX(88801983, playerObj.RO, hitDelay, 1, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
        CEffectMgr.Inst:AddBulletFX(88801984, playerObj.RO, attackerObj.RO, returnDelay, 1, -1, DelegateFactory.Action(function()
            local obj = CClientObjectMgr.Inst:GetObject(attackerEngineId)
            if obj then
                CEffectMgr.Inst:AddObjectFX(88801985, obj.RO, 0, 1, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
            end
        end))
    end
end

function LuaZongMenMgr:AddLianHuaListener()
    if not self.m_IsLianHuaEnable then
        -- 选美的逻辑
        self:StartXuanMeiBtnTick()
        
        self.m_IsLianHuaEnable = true
        if self.m_OnClientCreateAction == nil then
            self.m_OnClientCreateAction = DelegateFactory.Action_uint(function(engineId)
                self:OnClientCreate(engineId)
            end)
        end

        if self.m_OnMainPlayerDestroyedAction == nil then
            self.m_OnMainPlayerDestroyedAction = DelegateFactory.Action(function()
                self.m_CurrentSectId = 0
                self:RemoveLianHuaListener()
            end)
        end

        if self.m_OnClientObjVisibleAction == nil then
            self.m_OnClientObjVisibleAction = DelegateFactory.Action_uint(function(engineId)
                self:OnClientObjVisible(engineId)
            end)
        end

        EventManager.AddListenerInternal(EnumEventType.ClientObjCreate, self.m_OnClientCreateAction)
        EventManager.AddListenerInternal(EnumEventType.MainPlayerDestroyed, self.m_OnMainPlayerDestroyedAction)
        EventManager.AddListenerInternal(EnumEventType.ClientObjVisibleChanged, self.m_OnClientObjVisibleAction)
    end
end

-- 切换场景时
function LuaZongMenMgr:RemoveLianHuaListener()
    if self.m_IsLianHuaEnable then
        self.m_IsLianHuaEnable = false
        self.m_LianRenInfo = nil
        for i=1,#self.m_LianRenTicks do
            UnRegisterTick(self.m_LianRenTicks[i])
        end

        if self.m_LianYaoTick then
            UnRegisterTick(self.m_LianYaoTick)
            self.m_LianYaoTick = nil
        end

        -- 法阵特效销毁
        for i=1, #self.m_ZongMenWuXingFxs do
            if self.m_ZongMenWuXingFxs[i] then
                self.m_ZongMenWuXingFxs[i]:Destroy()
            end
        end
        self.m_ZongMenWuXingFxs = {}

        -- 选美关闭
        self:EndXuanMeiBtnTick()

        self.m_LianRenTicks = {}
        EventManager.RemoveListenerInternal(EnumEventType.ClientObjCreate, self.m_OnClientCreateAction)
        EventManager.RemoveListenerInternal(EnumEventType.MainPlayerDestroyed, self.m_OnMainPlayerDestroyedAction)
        EventManager.RemoveListenerInternal(EnumEventType.ClientObjVisibleChanged, self.m_OnClientObjVisibleAction)
    end
end

------------------------------------------
-- 捉人相关
------------------------------------------
LuaZongMenMgr.m_KunxiansuoItemId = nil
LuaZongMenMgr.m_KunxiansuoPlace = nil
LuaZongMenMgr.m_KunxiansuoPos = nil

function LuaZongMenMgr:OpenKunxiansuoUseWnd(itemId, place, pos)
    self.m_KunxiansuoItemId = itemId
    self.m_KunxiansuoPlace = place
    self.m_KunxiansuoPos = pos
    CUIManager.ShowUI(CLuaUIResources.KunxiansuoUseWnd)
end

----------------------------------------------
-- 宗派选美
-----------------------------------------------
LuaZongMenMgr.m_XuanMeiNpcList = nil
LuaZongMenMgr.m_IsXuanMeiEnable = nil
LuaZongMenMgr.m_OnClientCreateXuanMeiAction = nil
LuaZongMenMgr.m_OnMainPlayerDestroyedXuanMeiAction = nil
LuaZongMenMgr.m_XuanMeiBtnTick = nil
LuaZongMenMgr.m_CurrentXuanMeiNpc = nil
LuaZongMenMgr.m_LingLiYuFx = nil
LuaZongMenMgr.m_ChangeSkyBox = nil

function LuaZongMenMgr:StartXuanMeiBtnTick()
    if self.m_XuanMeiBtnTick == nil then
        self.m_XuanMeiBtnTick = RegisterTick(function()
            if CClientMainPlayer.Inst and CClientMainPlayer.Inst.RO then
                local myPos = CClientMainPlayer.Inst.RO.transform.position
                local dis = -1
                local npcId = -1
                CClientObjectMgr.Inst:ForeachObject(DelegateFactory.Action_CClientObject(function(obj)
                    if TypeIs(obj, typeof(CClientNpc)) then
                        if obj and obj.RO and obj.TemplateId == 20023556 then
                            local npcPos = obj.RO.transform.position
                            local d = Vector3.Distance(myPos, npcPos)
                            if d<dis or dis<0 then
                                dis = d
                                npcId = obj.EngineId
                            end
                        end
                    end
                end))
                if npcId ~= -1 then
                    self.m_CurrentXuanMeiNpc = npcId
                    if not CUIManager.IsLoaded(CLuaUIResources.ZongMenXuanMeiButtonWnd) then
                        CUIManager.ShowUI(CLuaUIResources.ZongMenXuanMeiButtonWnd)
                    end
                else
                    if CUIManager.IsLoaded(CLuaUIResources.ZongMenXuanMeiButtonWnd) then
                        CUIManager.CloseUI(CLuaUIResources.ZongMenXuanMeiButtonWnd)
                    end
                end
            end
        end, 200)
    end
end

function LuaZongMenMgr:EndXuanMeiBtnTick()
    if self.m_XuanMeiBtnTick ~= nil then
        UnRegisterTick(self.m_XuanMeiBtnTick)
        self.m_XuanMeiBtnTick = nil
    end
end

function LuaZongMenMgr:SyncXuanMeiStatus(npcList)
    self.m_XuanMeiNpcList = npcList
    for i=0, npcList.Count-1 do
        local clientObj = CClientObjectMgr.Inst:GetObject(npcList[i])
        if clientObj and clientObj.RO then
            clientObj.RO:DoAni("lianhua01", true, 0, 1, 0, true, 1)
            self:AddLianRenFx(clientObj.RO, 1)
        end
    end
    self:AddXuanMeiListener()
end

function LuaZongMenMgr:AddXuanMeiListener()
    if not self.m_IsXuanMeiEnable then
        self.m_IsXuanMeiEnable = true
        if self.m_OnClientCreateXuanMeiAction == nil then
            self.m_OnClientCreateXuanMeiAction = DelegateFactory.Action_uint(function(engineId)
                self:OnXuanMeilientCreate(engineId)
            end)
        end

        if self.m_OnMainPlayerDestroyedXuanMeiAction == nil then
            self.m_OnMainPlayerDestroyedXuanMeiAction = DelegateFactory.Action(function()
                self:RemoveXuanMeiListener()
            end)
        end
        EventManager.AddListenerInternal(EnumEventType.ClientObjCreate, self.m_OnClientCreateXuanMeiAction)
        EventManager.AddListenerInternal(EnumEventType.MainPlayerDestroyed, self.m_OnMainPlayerDestroyedXuanMeiAction)
    end
end

-- 切换场景时
function LuaZongMenMgr:RemoveXuanMeiListener()
    if self.m_IsXuanMeiEnable then
        self.m_IsXuanMeiEnable = false
        self.m_XuanMeiNpcList = nil

        self:ShutdownLingLiRain()

        EventManager.RemoveListenerInternal(EnumEventType.ClientObjCreate, self.m_OnClientCreateXuanMeiAction)
        EventManager.RemoveListenerInternal(EnumEventType.MainPlayerDestroyed, self.m_OnMainPlayerDestroyedXuanMeiAction)
    end
end

-- 每次有新的clientObj创建时调用
function LuaZongMenMgr:OnXuanMeilientCreate(engineId)
    local clientObj = CClientObjectMgr.Inst:GetObject(engineId)
    if self.m_XuanMeiNpcList then
        if clientObj and clientObj.RO then
            for i=0, self.m_XuanMeiNpcList.Count-1 do
                if engineId == self.m_XuanMeiNpcList[i] and clientObj.RO then
                    clientObj.RO:DoAni("lianhua01", true, 0, 1, 0, true, 1)
                    self:AddLianRenFx(clientObj.RO, 1)
                end
            end
        end
    end
end

function LuaZongMenMgr:ReleaseXuanMeiNpc(engineId)
    if self.m_XuanMeiNpcList then
        -- 播放炼化特效 放下炼化人物
        local clientObj = CClientObjectMgr.Inst:GetObject(engineId)
        if clientObj and clientObj.RO then
            self:AddLianRenFx(clientObj.RO, 3, 0, engineId)
        end

        for i=0, self.m_XuanMeiNpcList.Count-1 do
            if self.m_XuanMeiNpcList[i] == engineId then
                self.m_XuanMeiNpcList[i] = 0
                return
            end
        end
    end
end

function LuaZongMenMgr:ShowLingLiRain()
    self:ShutdownLingLiRain()
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.RO then
		self.m_LingLiYuFx = CEffectMgr.Inst:AddWorldPositionFX(88800590, CClientMainPlayer.Inst.RO.transform.position, 0, 0, 1, -1, EnumWarnFXType.None, nil, 0, 0, nil)
	end

    self.m_ChangeSkyBox = true
    CSharpResourceLoader.Inst:LoadMaterial("Assets/Res/Skybox/zilishimen03_skybox.mat", DelegateFactory.Action_Material(function (mat)
        if mat and CScene.MainScene and CScene.MainScene.SceneTemplateId == 16000241 then
            CRenderScene.Inst:UpdateSkybox(mat)
        end
    end))
end

function LuaZongMenMgr:ShutdownLingLiRain()
    if self.m_LingLiYuFx then
        self.m_LingLiYuFx:Destroy()
        self.m_LingLiYuFx = nil
    end

    if self.m_ChangeSkyBox then
        CSharpResourceLoader.Inst:LoadMaterial("Assets/Res/Skybox/fengxiantai_sky.mat", DelegateFactory.Action_Material(function (mat)
            if mat and CScene.MainScene and CScene.MainScene.SceneTemplateId == 16000241 then
                CRenderScene.Inst:UpdateSkybox(mat)
            end
        end))
        self.m_ChangeSkyBox = false
    end
end

------------------------------------------
-- 敌对门派
------------------------------------------
LuaZongMenMgr.m_ArrestListWndEmenySectId = nil
LuaZongMenMgr.m_ShowMapId = nil
LuaZongMenMgr.m_ShowMapInfo = nil
LuaZongMenMgr.m_SeverMapMapInfo = nil

function LuaZongMenMgr:ShowArrestListWnd(sectId)
    self.m_ArrestListWndEmenySectId = sectId
    CUIManager.ShowUI(CLuaUIResources.EnemyZongPaiArrestListWnd)
end

function LuaZongMenMgr:QuerySectWithMapId(mapId, otherMapId)
    self.m_ShowMapId = mapId
    local data = CreateFromClass(MakeGenericClass(List, Object))
    if mapId then
        CommonDefs.ListAdd(data, typeof(UInt32), mapId)
    end
    if otherMapId then
        CommonDefs.ListAdd(data, typeof(UInt32), otherMapId)
    end

    if data.Count>0 then
        Gac2Gas.QuerySectEntranceByMapId(MsgPackImpl.pack(data))
    end
end

function LuaZongMenMgr:DealSectWithMapData(mapInfo)
    self.m_ShowMapInfo = mapInfo
    if mapInfo.Count/4 >1 then
        CUIManager.ShowUI(CLuaUIResources.ZongMenEnterChooseWnd)
    else
        Gac2Gas.RequestTrackEnemySect(mapInfo[2])
    end
end

function LuaZongMenMgr:SyncXiepaiExtraRewardSwitch(sectId, bOpen, leftTime, expCost)
    self.m_EvilBuffCostExp = expCost
    g_ScriptEvent:BroadcastInLua("SyncXiepaiExtraRewardSwitch", sectId, bOpen, leftTime, expCost)
end

function LuaZongMenMgr:GetHpSkillExtraDisplay(hpSkillData)
    if hpSkillData == nil then
        return ""
    end

    local hpSkillLv = hpSkillData.Level or 0
    local wqxSkillLv = CLivingSkillMgr.Inst:GetSkillLevel(950005)

    local extra = LuaZongMenMgr:GetHpSkillExtraValue(hpSkillLv, wqxSkillLv)

    return g_MessageMgr:FormatMessage("HPSKILL_WQX_EXTRA_BUFFDESC", wqxSkillLv, extra)
end
----------------------------------------------
-- 宗派修炼
-----------------------------------------------
LuaZongMenMgr.m_SectXiuxingPlayStage = nil
LuaZongMenMgr.m_SectXiuxingTotalDoors = 0
LuaZongMenMgr.m_SectXiuxingDestroyedDoors = 0
LuaZongMenMgr.m_SectXiuxingFinishTime = 0
LuaZongMenMgr.m_InZongMenPracticePlay = false
LuaZongMenMgr.m_NextTime = nil
LuaZongMenMgr.m_TeleportId2Hp = {}

function LuaZongMenMgr:IsInZongMenScene()
    local sceneTemplateId = 0
    if CScene.MainScene then
        sceneTemplateId = CScene.MainScene.SceneTemplateId
    end
    return tonumber(Menpai_Setting.GetData("RaidMapId").Value) == sceneTemplateId
end
function LuaZongMenMgr:IsInZongMenPracticePlay()
    if self:IsMySectScene() then
        return self.m_InZongMenPracticePlay
    end
    return false
end
function LuaZongMenMgr:OnMainPlayerCreated()
    LuaZongMenMgr.m_InZongMenPracticePlay = false
end

function LuaZongMenMgr:SyncXiuxingBoomBornPostion(x, y)
    -- 废弃
end

function LuaZongMenMgr:SyncSectXiuxingReliveTime(nextTime)
    LuaZongMenMgr.m_NextTime = nextTime
    CUIManager.ShowUI(CLuaUIResources.ZongMenPracticeReliveWnd)
end

function LuaZongMenMgr:SectXiuxingUpdateChuansongmenHp(engineId, hp)
    self.m_TeleportId2Hp[engineId] = hp
    g_ScriptEvent:BroadcastInLua("SectXiuxingUpdateChuansongmenHp", engineId, hp)
end

function LuaZongMenMgr:UpdateXiuxingDoorAndTime(totalDoors, destroyedDoors, finishTime)
    LuaZongMenMgr.m_SectXiuxingTotalDoors = totalDoors
    LuaZongMenMgr.m_SectXiuxingDestroyedDoors = destroyedDoors
    LuaZongMenMgr.m_SectXiuxingFinishTime = finishTime
    g_ScriptEvent:BroadcastInLua("UpdateXiuxingDoorAndTime", totalDoors, destroyedDoors, finishTime)
end
------------------------------------------rpc 

function Gas2Gac.SMSW_QueryRankResult(rankUD, selfScore)
    LuaZongMenMgr.m_SMSWRankData = {}
    LuaZongMenMgr.m_SMSW_MyRankScore = selfScore
    for k, index in pairs(EnumSMSWPlayerLevelStage) do
        LuaZongMenMgr.m_SMSWRankData[index] = {}
    end
    local rankDictData = MsgPackImpl.unpack(rankUD)
    if rankDictData and (CommonDefs.DictContains(rankDictData, typeof(String), 'IsEmpty') or rankDictData.Count == 0) then
        CUIManager.ShowUI(CLuaUIResources.GuardShiMenRankWnd)
        return
    end
    CommonDefs.DictIterate(rankDictData, DelegateFactory.Action_object_object(function(key,val)
        for i = 0,val.Count - 1 do
            table.insert(LuaZongMenMgr.m_SMSWRankData[tonumber(key) - 1000],val[i])
        end
    end))
    CUIManager.ShowUI(CLuaUIResources.GuardShiMenRankWnd)
end

function Gas2Gac.SMSW_QueryPlayInfoResult(day, leftHp, maxHp)
    LuaZongMenMgr.m_SMSWQueryPlayInfoResultData = {day = day, leftHp = leftHp, maxHp = maxHp}
    g_ScriptEvent:BroadcastInLua("RefreshShiMenRuQinTaskViewVisibleStatus")
    if LuaZongMenMgr.m_OpenGuardZongMenWnd then
        CUIManager.ShowUI(CLuaUIResources.GuardZongMenWnd)
        LuaZongMenMgr.m_OpenGuardZongMenWnd = false
    end
end

function Gas2Gac.SMSW_SyncPlayState(state, day, leftHp, maxHp)
    LuaZongMenMgr.m_SMSW_PlayState = state
    g_ScriptEvent:BroadcastInLua("RefreshShiMenRuQinTaskViewVisibleStatus")
end

function Gas2Gac.SMSW_DestroyScene(sceneTemplateId)
    local array = {}
    array[EnumClass.SheShou] = 16000013
    array[EnumClass.JiaShi] = 16000013
    array[EnumClass.DaoKe] = 16000022
    array[EnumClass.XiaKe] = 16000022
    array[EnumClass.FangShi] = 16000011
    array[EnumClass.YiShi] = 16000011
    array[EnumClass.MeiZhe] = 16000012
    array[EnumClass.YiRen] = 16000012
    array[EnumClass.YanShi] = 16000029
    array[EnumClass.HuaHun] = 16000029
    if CClientMainPlayer.Inst then
        local class = CClientMainPlayer.Inst.Class
        if array[class] and array[class] == sceneTemplateId then
            LuaZongMenMgr.m_SMSW_DestroySceneId = sceneTemplateId
            CUIManager.ShowUI(CLuaUIResources.ShiMenRuinedWnd)
        end
    end
end

function Gas2Gac.SMSW_BroadcastBossInfo(day, leftHp, maxHp)
    LuaZongMenMgr.m_SMSWQueryPlayInfoResultData = {day = day, leftHp = leftHp, maxHp = maxHp}
    g_ScriptEvent:BroadcastInLua("OnSMSWBossInfoUpdate")
end

-- bInSect --> 是否在门派中
-- sectInfoUd --> 信物用到的门派信息 如果 bInSect是true,则返回 CSectXingWuInfoRpc 数据结构，否则是一个nil_userdata
function Gas2Gac.SyncSectXinWuInfo(bInSect, sectInfoUd)
    if bInSect then
        LuaZongMenMgr.m_ZongMenProp = CustomPropertySerializable("CSectXingWuInfoRpc")
        LuaZongMenMgr.m_ZongMenProp:LoadFromString(sectInfoUd)
        if CClientMainPlayer.Inst and LuaZongMenMgr.m_ZongMenProp.LeaderId == CClientMainPlayer.Inst.Id then
            CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.ReCommandZongMen)
        end
    end
end

-- Gac2Gas.QuerySectInWorldMap 对应的返回，返回世界地图上的宗派信息,如果玩家不在任何宗派中,则不会收到这个RPC
function Gas2Gac.SyncSectInWorldMap(mapInfoUd)
    local mapInfo = MsgPackImpl.unpack(mapInfoUd)
    local mapMap = {}
    for i = 0,mapInfo.Count-1,2 do
        local mapId =  mapInfo[i] -- 地图id
        local flag =  mapInfo[i+1] -- 状态 (1只有1个自己宗派,2只有1个敌对宗派，3多个且含自己，4多个不含自己)
        mapMap[mapId] = flag
    end
    LuaZongMenMgr.m_SeverMapMapInfo = mapMap
    g_ScriptEvent:BroadcastInLua("OnSyncSectInWorldMap", mapMap)
end

-- 寻路到宗派入口处 -- Gac2Gas.RequestTrackToSectEntrance
-- 收到这个RPC时，客户端只需要直接寻路,服务端已经做完了必要的状态检测
function Gas2Gac.TrackToSectEntrance(entranceMapId, entrancePosX, entrancePosY)
    if CClientMainPlayer.Inst then
        Gac2Gas.RequestEnterSelfSectScene(CClientMainPlayer.Inst.BasicProp.SectId)
    end
    -- local x = entrancePosX
    -- local y = entrancePosY
    -- CTrackMgr.Inst:Track("", entranceMapId, CreateFromClass(CPos, x * 64,y * 64), 0, nil, nil, nil, nil, true)
end

function Gas2Gac.SyncCreateSectDone()
    if CLuaGuideMgr.EnableGuide() and not COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.GoToZongMen)then
        CLuaMiniMapPopWnd.m_ShowWorldMap = true--直接打开世界地图
    end
    CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.GoToZongMen)
    LuaZongMenMgr:OpenZongMenTuDiJoiningWnd()
    CUIManager.CloseUI(CLuaUIResources.ZongMenCreationWnd)
end

function Gas2Gac.ReplyInviteTudiGoinSect()
    CUIManager.CloseUI(CLuaUIResources.ZongMenTuDiJoiningWnd)
end

function Gas2Gac.SyncTudiSectAndMingGe(infoUd)
    local infoList = MsgPackImpl.unpack(infoUd)
    local step = 3
    local list = {}
    for i = 0,infoList.Count - 1,step do
        local playerId  = infoList[i]
        local sectId    = infoList[i+1]
        local mingGe    = infoList[i+2]
        list[playerId] = {sectId = sectId,mingGe = mingGe }
    end
    g_ScriptEvent:BroadcastInLua("OnSyncTudiSectAndMingGe", list)
end

function Gas2Gac.ReplyJianDingMingGe(mingGe)
    g_ScriptEvent:BroadcastInLua("OnZongMenMingGeJianDing", mingGe)
    CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.WatchWuXingMingGe)
end

function Gas2Gac.SyncPlayerMingGe(mingGe)
    if CClientMainPlayer.Inst then
        CClientMainPlayer.Inst.BasicProp.MingGe = mingGe
    end
    
    if mingGe ~= 0 then
        g_ScriptEvent:BroadcastInLua("OnSyncPlayerMingGe", mingGe)
    end
end

-- newSectId 等于0 表示退出成功
function Gas2Gac.ReplyQuitSect(newSectId)
    CUIManager.CloseUI(CLuaUIResources.ZongMenMainWnd)
end

function Gas2Gac.ReplyJoinSect(newSectId, mingGe, oldSectId, stdListUd, invalidListUd, extraUd)
    
    local list = MsgPackImpl.unpack(extraUd)
    local expressionId = list[0]
    local motionId = list[1]

    LuaZongMenMgr.m_ReplyJoinSectRes = {expressionId = expressionId,motionId = motionId, newSectId = newSectId, mingGe = mingGe, oldSectId = oldSectId, stdListUd = stdListUd, invalidListUd = invalidListUd, isCreateZongMen = false, checkList = true}
    CUIManager.ShowUI(CLuaUIResources.ZongMenJoiningWnd)
end

-- 创建宗派失败,类似于 ReplyJoinSect 需要展示一个界面
-- 如果 bSuccess 为 true，则 mismatchStdListUd 和 allStdListUd 是不准确的,因为界面用不到,为了实现便利,服务端在满足之后没有重复计算
-- place, pos, itemId 是包裹中的信物始终准确, mingGe, oldSectId也准确
-- Gac2Gas.RequestCreateSect 也有可能会返回这个RPC(毕竟中间有个二次确认，时间过了几秒钟，可能之前满足的条件现在不满足了)
function Gas2Gac.SyncCreateSectPrecheckResult(bSuccess, mingGe, oldSectId, stdListUd, invalidListUd, place, pos, itemId)
    LuaZongMenMgr.m_ReplyJoinSectRes = {newSectId = -1, mingGe = mingGe, oldSectId = oldSectId, stdListUd = stdListUd, invalidListUd = invalidListUd, isCreateZongMen = true, checkList = bSuccess == false}
    CUIManager.ShowUI(CLuaUIResources.ZongMenJoiningWnd)
end

-- 如果 bHasSect 是true,则Ud是 CToJoinSectInfoRpc 数据结构,否则是 nil_userdata
function Gas2Gac.SyncToJoinSectInfo(bHasSect, Ud)

    if bHasSect then
        LuaZongMenMgr.m_JoinSectInfo = CustomPropertySerializable("CToJoinSectInfoRpc")
        LuaZongMenMgr.m_JoinSectInfo:LoadFromString(Ud)
        g_ScriptEvent:BroadcastInLua("OnZongMenSearchResult",LuaZongMenMgr.m_JoinSectInfo)
    end
end

-- 设置玩家宗派id,用以更新 propertyBasic中的sectId
function Gas2Gac.SyncSetPlayerSectId(newSectId)
    if CClientMainPlayer.Inst then
        CClientMainPlayer.Inst.BasicProp.SectId = newSectId
    end
    if newSectId == 0 then
        CUIManager.CloseUI(CLuaUIResources.ZongMenMainWnd)
        g_ScriptEvent:BroadcastInLua("OnLeaveZongMen")
    end
end

-- QuerySectInfo 对应的返回下面的RPC的前三个参数就是客户端发上来的三个参数原封不动带下去
function Gas2Gac.SyncSectInfoBegin(sectId, infoType, extra)
    -- if infoType == "title" then
    --     LuaZongMenMgr.m_SectHasTitleMemberInfo = {}
    if infoType == "office" then
        LuaZongMenMgr.m_SectOfficeInfo = {}
    elseif infoType == "candidate" then
        LuaZongMenMgr.m_SectOfficeCandidataInfo = {}
    elseif infoType == "history" then
        LuaZongMenMgr.m_SectHistoryInfo = {}
    elseif infoType == "rights" then
        LuaZongMenMgr.m_SectOfficeRightInfo = {}
    elseif infoType == "member" then
        LuaZongMenMgr.m_SectMemberInfo = {}
    elseif infoType == "recommend" then
        LuaZongMenMgr.m_SectRecommendInfo = {}
    end
end

-- QuerySectInfo,根据不同的infoType，Ud是不同的数据结构
function Gas2Gac.SyncSectInfo(sectId, infoType, extra, Ud)
    if infoType == "overview" then
        LuaZongMenMgr.m_SectMainPageInfo = CustomPropertySerializable("CSectMainPageInfoRpc")
        LuaZongMenMgr.m_SectMainPageInfo:LoadFromString(Ud)
    else
        --LuaZongMenMgr:GetCustomPropertyMapData(infoType, Ud, "title", "CSectHasTitleMemberInfoRpc", LuaZongMenMgr.m_SectHasTitleMemberInfo)
        LuaZongMenMgr:GetCustomPropertyMapData(infoType, Ud, "office", "CSectOfficeInfoRpc", LuaZongMenMgr.m_SectOfficeInfo)
        LuaZongMenMgr:GetCustomPropertyMapData(infoType, Ud, "candidate", "CSectOfficeCandidataInfoRpc", LuaZongMenMgr.m_SectOfficeCandidataInfo)
        -- CSectHistoryInfoRpc 改成了 CSectHistoryInfo,格式也有变动
        LuaZongMenMgr:GetCustomPropertyMapData(infoType, Ud, "history", "CSectHistoryInfo", LuaZongMenMgr.m_SectHistoryInfo)
        LuaZongMenMgr:GetCustomPropertyMapData(infoType, Ud, "rights", "CSectOfficeRightRPC", LuaZongMenMgr.m_SectOfficeRightInfo)
        LuaZongMenMgr:GetCustomPropertyMapData(infoType, Ud, "member", "CSectMemberInfoRpc", LuaZongMenMgr.m_SectMemberInfo)
        LuaZongMenMgr:GetCustomPropertyMapData(infoType, Ud, "recommend", "CSectRecommendInfoRpc", LuaZongMenMgr.m_SectRecommendInfo)
    end
end

-- QuerySectInfo
function Gas2Gac.SyncSectInfoEnd(sectId, infoType, extra, extraUd)
    if infoType == "overview" then
        g_ScriptEvent:BroadcastInLua("OnZongMenInfoResult",LuaZongMenMgr.m_SectMainPageInfo)
    -- elseif infoType == "title" then
    --     g_ScriptEvent:BroadcastInLua("OnZongMenHasTitleMemberInfoResult",LuaZongMenMgr.m_SectHasTitleMemberInfo)
    elseif infoType == "office" then
        local list = MsgPackImpl.unpack(extraUd)
        g_ScriptEvent:BroadcastInLua("OnSectOfficeInfoResult",LuaZongMenMgr.m_SectOfficeInfo, list.Count > 0 and list[0] or 0)
    elseif infoType == "candidate" then
        g_ScriptEvent:BroadcastInLua("OnSectOfficeCandidataInfoResult",LuaZongMenMgr.m_SectOfficeCandidataInfo,extra)
    elseif infoType == "history" then
        g_ScriptEvent:BroadcastInLua("OnSectHistoryInfoResult",LuaZongMenMgr.m_SectHistoryInfo)
    elseif infoType == "rights" then
        g_ScriptEvent:BroadcastInLua("OnSectOfficeRightResult",LuaZongMenMgr.m_SectOfficeRightInfo)
    elseif infoType == "member" then
        local list = MsgPackImpl.unpack(extraUd)
        g_ScriptEvent:BroadcastInLua("OnSectMemberInfoResult",LuaZongMenMgr.m_SectMemberInfo,
        list.Count > 0 and list[0] or 0,list.Count > 1 and list[1] or 0,
        list.Count > 2 and list[2] or 0,list.Count > 3 and list[3] or 0)
    elseif infoType == "recommend" then
        g_ScriptEvent:BroadcastInLua("OnSectRecommendInfoResult",LuaZongMenMgr.m_SectRecommendInfo)
    end
end

-- QueryMySectTitleInfo 解开后是一个list,里面是titleId
function Gas2Gac.SyncMySectTitleInfo(titleListUd,currentTitle)
    local titleIdList = MsgPackImpl.unpack(titleListUd)
    g_ScriptEvent:BroadcastInLua("OnZongMenOwnTitleIdList",titleIdList,currentTitle)
end

-- RequestBatchChangeSectOffice
function Gas2Gac.ReplyBatchChangeSectOffice(sectId, bSuccess)
    if not bSuccess then return end
    g_ScriptEvent:BroadcastInLua("OnReplyBatchChangeSectOffice")
end

function Gas2Gac.ReplySetSectOffice(sectId, targetPlayerId, office, bSuccess)
    if bSuccess then
        g_ScriptEvent:BroadcastInLua("OnReplySetSectOffice", sectId, targetPlayerId,office)
    end
end

function Gas2Gac.ReplyKickSectMember(sectId, targetPlayerId, bSuccess)
    if bSuccess then
        if CClientMainPlayer.Inst then
            Gac2Gas.QuerySectInfo(CClientMainPlayer.Inst.BasicProp.SectId, "member", 0)
        end
    end
end

-- RequestSetSectRights， rightUd是发上来的参数格式原封不动下发
function Gas2Gac.ReplySetSectRights(sectId, bSuccess, rightUd)
    if CClientMainPlayer.Inst then
        Gac2Gas.QuerySectInfo(CClientMainPlayer.Inst.BasicProp.SectId, "rights", 0)
    end
end

-- RequestResignSectOffice
function Gas2Gac.ReplyResignSectOffice(bSuccess)
    if bSuccess then
        g_ScriptEvent:BroadcastInLua("OnReplyResignSectOffice")
    end
end


function Gas2Gac.SyncPlayerSectSpeakStatus(sectId, playerId, bForbidden)
    LuaZongMenMgr.m_PlayerId2Forbidden[playerId] = bForbidden
    g_ScriptEvent:BroadcastInLua("OnPlayerSectSpeakStatusResult", playerId)
end


function Gas2Gac.PlayerContactSectManager(sectId, managerId, managerName)
	CChatHelper.ShowFriendWnd(managerId,  managerName)
end

function Gas2Gac.ReplySectFaZhenAvoidWuXingInfo(sectId, currentWuXing, avoidWuXing, nextCanSetTime, bFree, hasShengWang)
    g_ScriptEvent:BroadcastInLua("OnReplySectFaZhenAvoidWuXingInfo", sectId, currentWuXing, avoidWuXing, nextCanSetTime, bFree, hasShengWang)
end

-- RequestSetSectFaZhenAvoidWuXing
function Gas2Gac.ReplySetSectFaZhenAvoidWuXing(sectId, currentWuXing, avoidWuXing, nextCanSetTime, bFree, hasShengWang)
    g_ScriptEvent:BroadcastInLua("OnReplySetSectFaZhenAvoidWuXing", sectId, currentWuXing, avoidWuXing, nextCanSetTime, bFree, hasShengWang)
end

-- sectHasTitleMemberInfoRpcUd 是 CSectHasTitleMemberInfoRpc 数据结构
function Gas2Gac.SyncSectHasTitleMember(sectId, titleId, sectHasTitleMemberInfoRpcUd)
    local info = CustomPropertySerializable("CSectHasTitleMemberInfoRpc")
    info:LoadFromString(sectHasTitleMemberInfoRpcUd)
    local map = info.Infos
    LuaZongMenMgr.m_SectHasTitleMemberInfo[titleId] = {}
    CommonDefs.DictIterate(map, DelegateFactory.Action_object_object(function (___key, ___value)
        table.insert(LuaZongMenMgr.m_SectHasTitleMemberInfo[titleId] , ___value)
    end)) 
    g_ScriptEvent:BroadcastInLua("OnZongMenHasTitleMemberInfoResult",LuaZongMenMgr.m_SectHasTitleMemberInfo[titleId])
end

--@region 切换宗派天空盒
local Menpai_SkyBoxKind = import "L10.Game.Menpai_SkyBoxKind"
local CRenderScene=import "L10.Engine.Scene.CRenderScene"
local SoundManager = import "SoundManager"

-- 进入宗派副本之后设置天空盒
-- 与家园切换天空盒的方法分开，可能有不同的业务逻辑
function Gas2Gac.SyncSectSkyBox(skyboxId)
    LuaZongMenMgr.m_SkyboxId = skyboxId
    skyboxId = skyboxId - 1
    if skyboxId < 0 then return end
    local data = Menpai_SkyBoxKind.GetData(LuaZongMenMgr.m_SkyboxId)
    if data then
        CRenderScene.Inst:SetAtmosphere(data.AtmosphereId)
        PlayerPrefs.SetInt(CRenderScene.s_CachedZongMenSkyBoxIndexStr, data.AtmosphereId)
        LuaZongMenMgr.m_BgMusicPath = data.MenPaiKind == 2 and (Menpai_Setting.GetData("XiePaiBGM").Value)
            or (SoundManager.Inst:GetBgEventPath(CRenderScene.zilishimenRaidMapId))
        SoundManager.Inst:ForceSetBgMusic(LuaZongMenMgr.m_BgMusicPath)
    end
    if not CRenderScene.Inst then return end
    CRenderScene.Inst.isXilishimenXiePai = data.MenPaiKind == 2
    local curExtraSectTrapInfo = CRenderScene.Inst:GetExtraSectTrapInfo()
    for i= 0, curExtraSectTrapInfo.Count - 1 do
        CRenderScene.Inst:OnUpdateSectEntranceTrapInfo(CScene.MainScene.SceneTemplateId, curExtraSectTrapInfo[i].pos.x, curExtraSectTrapInfo[i].pos.y,CRenderScene.Inst.isXilishimenXiePai,SectEntranceTrapType.Default,- 1)
        break
    end
end

--@endregion

-- 宗派入口传送阵信息新增/变更, 适用于新建或者更改入口位置的情况。注意可能是变更，之前在一个位置,现在处于另外一个位置
-- 切场景、掉线之后应该删除这个传送阵信息，传回来之后，服务端会主动下发当前场景的宗派传送阵
function Gas2Gac.SyncAddSceneSectTrapInfo(sectId, bXiePai, TrapPointUD)
    -- -- 逻辑类似于 SendExtraSceneTrapInfo， 但支持移除和变更
    -- LuaZongMenMgr:SyncAddSceneSectTrapInfo(sectId, bXiePai, TrapPointUD)
end

-- 宗派入口传送阵移除,适用于宗派解散的情况
function Gas2Gac.SyncRmSceneSectTrapInfo(sectId)
    -- print("SyncRmSceneSectTrapInfo", sectId)
    -- LuaZongMenMgr:SyncRmSceneSectTrapInfo(sectId)
end

-- 设置宗派是否可以通过id搜索的结果
function Gas2Gac.SyncSectSetHideFromIdSearch(sectId, bOpen)
    g_ScriptEvent:BroadcastInLua("OnSyncSectSetHideFromIdSearch",bOpen)
end

-- 宗派入口变更(可能是新增，可能是变更，收到之后需要重建入口信息)
function Gas2Gac.SyncAddSectEntrance(sectId, mapId, x, y, bXiePai)
    CUIManager.CloseUI(CUIResources.SectEntranceButtonWnd)
    LuaZongMenMgr:SyncAddSectEntrance(sectId, mapId, x, y, bXiePai)
end

-- 移除宗派入口
function Gas2Gac.SyncRmSectEntrance(sectId)
    CUIManager.CloseUI(CUIResources.SectEntranceButtonWnd)
    LuaZongMenMgr:SyncRmSectEntrance(sectId)
end

-- 如果收到这个RPC，则status 就是 RequestSetSectMemberSpeakStatus发上来的那个值
function Gas2Gac.SyncSetSectMemberSpeakStatusResult(sectId, targetPlayerId, status)
    LuaZongMenMgr.m_PlayerId2Forbidden[targetPlayerId] = status == 1
    g_ScriptEvent:BroadcastInLua("OnSyncSetSectMemberSpeakStatusResult", targetPlayerId, status)
end

LuaZongMenMgr.m_JoinZongMenSendLinkPlayerId = 0
LuaZongMenMgr.m_JoinZongMenBIM = false
function Gas2Gac.SyncSectName(sectId, sectName)
    LuaZongMenMgr.m_SectId2Name[sectId] = sectName
    if CUIManager.IsLoaded(CLuaUIResources.ZongMenJoiningWnd) then
        g_ScriptEvent:BroadcastInLua("OnSyncSectName",  sectId, sectName)
    else
		MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("Invite_to_ZongPai_Confirm",sectName),DelegateFactory.Action(function()
			LuaZongMenMgr:JoinZongMen(sectId, true, LuaZongMenMgr.m_JoinZongMenSendLinkPlayerId, LuaZongMenMgr.m_JoinZongMenBIM)
		end),nil,LocalString.GetString("确定"),LocalString.GetString("取消"),false)
    end
end

-- 当前宗门场景的宗门id，进入宗门场景时下发,可用以区分是否为自己的宗门
function Gas2Gac.SyncCurrentSceneSectId(sceneId, sectId)
    LuaZongMenMgr.m_SyncCurrentSceneId = sceneId
    LuaZongMenMgr.m_SyncCurrentSectId = sectId
end

function Gas2Gac.SyncSectNameAndStd(sectId, name, stdListUd)

    local list = MsgPackImpl.unpack(stdListUd)
    local rule = nil
	for i = 0, list.Count-1 do
		local id = tonumber(list[i])
        local data = Menpai_Rules.GetData(id)
        if data and data.Description~=nil then
            if rule == nil then
                rule = data.Description
            else
                rule = SafeStringFormat3("%s\n%s", rule, data.Description)
            end
        end
    end
    if rule == nil then
        return
    end
    local n = CommonDefs.StringLength(rule)
    local builder = CommonDefs.StringBuilder_int(n+20)
    local index = 0
    for i=0,n-1 do
        local str = CommonDefs.StringSubstring2(rule, i, 1)
        builder:Append(str)
        index = index + 1
        if str == "\n" then
            index = 0
        elseif index == 15 then --自动断行，目前调试下来选择15字符
            if i==n-1 or CommonDefs.StringSubstring2(rule, i+1, 1) ~= "\n" then
                builder:Append("\n") --如果下一个字符不是换行符，就添加一个换行
            end
            index = 0
        end
    end


    TextMeshExDataSource.Inst:SetData(EnumToInt(EnumTextMeshExKey.SectPlaque), name)
    TextMeshExDataSource.Inst:SetData(EnumToInt(EnumTextMeshExKey.SectMonument), ToStringWrap(builder))
end

function Gas2Gac.SyncSetSectShowTitle(sectId, titleId)
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.SectId == sectId then
        g_ScriptEvent:BroadcastInLua("OnSyncSetSectShowTitle",  titleId)
    end
end

function Gas2Gac.SyncSectAlterNameInfo(sectId, lastAlterNameTime, alterNameCost, nameUsedListUd)
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.SectId == sectId then
        g_ScriptEvent:BroadcastInLua("OnSyncSectAlterNameInfo",  lastAlterNameTime, alterNameCost, nameUsedListUd)
    end
end

-- 宗派场景名字变化
function Gas2Gac.SyncSectChangeName(newName)
    TextMeshExDataSource.Inst:SetData(EnumToInt(EnumTextMeshExKey.SectPlaque), newName)
    g_ScriptEvent:BroadcastInLua("OnSyncSectChangeName",  newName)
end

-- #153649 新增参数 lastInviteFriendTime:上次邀请好友的时间 lastInviteGuildTime:上次邀请帮会成员的时间
function Gas2Gac.SyncSectAllMemberId(Ud, lastInviteFriendTime, lastInviteGuildTime)
	local list = MsgPackImpl.unpack(Ud)
	local t = {}
	for i = 0, list.Count - 1 do
		t[list[i]] = true
	end
    local sectInviteCd = tonumber(Menpai_Setting.GetData("SectInviteCd").Value)
	g_ScriptEvent:BroadcastInLua("OnSyncSectAllMemberId",  t,sectInviteCd,lastInviteFriendTime,lastInviteGuildTime)
end

-- 请求邀请进入宗派的结果,只有成功了才会收到返回
function Gas2Gac.ReplySectInviteImMessage(sectId, bOk, inviteType, invitedIdsU)
	local invitedIds = g_MessagePack.unpack(invitedIdsU)
    for _, pIds in ipairs(invitedIds) do
        print(pIds)
    end
    g_ScriptEvent:BroadcastInLua("OnReplySectInviteImMessage",invitedIds)
end

function Gas2Gac.SyncSectInfoForStone(sectId, Ud1, Ud2)
	local list = MsgPackImpl.unpack(Ud1)

    local skyboxId,sectName = tonumber(list[0]) ,list[1]
	-- instruction
	local list2 = MsgPackImpl.unpack(Ud2)
    local t = {}
	for i = 0, list2.Count - 1 do
		table.insert(t,list2[i])
	end
    LuaZongMenMgr:OnZongMenGuiZeWnd(sectName,CommonDefs.ListToArray(list2), true,skyboxId)
end

function Gas2Gac.SyncClientCareExpressionDone(expressionId, motionId)
    -- LuaZongMenMgr.m_JoiningZongMenId = LuaZongMenMgr.m_JoinZongMenParams[1]
    -- local bByChatLink, inviterId, bIm = LuaZongMenMgr.m_JoinZongMenParams[2],LuaZongMenMgr.m_JoinZongMenParams[3],LuaZongMenMgr.m_JoinZongMenParams[4]
    -- Gac2Gas.RequestJoinSect(LuaZongMenMgr.m_JoiningZongMenId, bByChatLink, inviterId, bIm)
end

function Gas2Gac.SyncSectOpenAd(sectId, bOpen)

    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.SectId == sectId then
        g_ScriptEvent:BroadcastInLua("OnSyncSectOpenAd",  bOpen)
    end
end

function Gas2Gac.SyncChangeSectJoinStdPreInfo(sectId, lastChangeTime, bHasRight)
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.SectId == sectId then
        g_ScriptEvent:BroadcastInLua("OnSyncChangeSectJoinStdPreInfo", lastChangeTime, bHasRight)
    end
end

-- newWuxing 是新的五行，不排除等于旧的五行
function Gas2Gac.SyncTempChangeSectWuxingResult(sectId, newWuxing, oldWuXing, expireTime)
    LuaZongMenMgr:ShowZongMenWuXingNiuZhuanWnd(oldWuXing, newWuxing, expireTime)
end

-- 展示炼化法阵特效(在一个不是宗派副本中展示法阵的特效)
function Gas2Gac.SyncShowSectFaZhenEffect(sectId, wuxing)
    LuaZongMenMgr:SyncLianHuaFaZhen(wuxing)
end

-- 玩家在地图(mapId)某处(x,y)发现了一个宗派的入口(出生特效)
function Gas2Gac.SyncDiscoverySectEntrance(sectId, mapId, x, y)
    local pos = CreateFromClass(CPos, x, y)
    local fxId = tonumber(Menpai_Setting.GetData("SectEntranceBornFxId").Value) 
    CEffectMgr.Inst:AddWorldPositionFX(fxId, Utility.GridPos2PixelPos(pos), 0, 0, 1, -1, EnumWarnFXType.None, nil, 0, 0, nil)
end

-- 玩家变身灵核--需求变更，之前只是在副本中，副本中就一个玩家，现在改成了在宗派中，其他人可能也会收到这个RPC,效果等待策划确认
function Gas2Gac.SyncPlayerTurnToSoulCore(playerId, wuxing, bChange, posX, posY)
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == playerId and CClientMainPlayer.Inst.RO then
        local soulCore = CClientMainPlayer.Inst.RO.transform:Find("SoulCore")
        if soulCore then
            GameObject.Destroy(soulCore.gameObject)
        end
        LuaZongMenMgr:CreateSoulCoreGameObject(CClientMainPlayer.Inst.RO.gameObject, CClientMainPlayer.Inst.SkillProp.SoulCore, wuxing,LayerDefine.Default)
        CClientMainPlayer.Inst.RO.Visible = false
    end
end

-- targetPlayerId 有可能是0，完全有可能没选到人
function Gas2Gac.SyncSoulcoreBoomEffect(sectId, targetPlayerId, posX, posY)
    local fxId = tonumber(Menpai_Setting.GetData("SoulcoreBoomFxId").Value) 
    local pos = CPos(posX, posY)
    CEffectMgr.Inst:AddWorldPositionFX(fxId, Utility.GridPos2PixelPos(pos), 0, 0, 1, -1, EnumWarnFXType.None, nil, 0, 0, nil)
end

LuaZongMenMgr.mAddFakeEntranceSectId = 0
LuaZongMenMgr.mAddFakeEntrancePos = CPos(0,0)
function Gas2Gac.SyncAddFakeSectEntrance(sectId, mapId, x, y, bXiePai, leftTime)
    if not CRenderScene.Inst then return end
    if CClientMainPlayer.Inst then
        LuaZongMenMgr.mAddFakeEntranceSectId = sectId
        LuaZongMenMgr.mAddFakeEntrancePos = CPos(x, y)
        CRenderScene.Inst:OnUpdateSectEntranceTrapInfo(mapId, x, y,bXiePai,SectEntranceTrapType.SchoolSectTask,CServerTimeMgr.Inst.timeStamp + leftTime)
    end
end

LuaZongMenMgr.m_OpenNewSystem = true
LuaZongMenMgr.m_EnableNewMingGeJianDing = false

-- 宗派迭代,如果宗派迭代的开关被打开，则会通过SyncSectInfoBegin_Ite/SyncSectInfo_Ite/SyncSectInfoEnd_Ite发送数据，而不再通过旧的三组
function Gas2Gac.SyncSectInfoBegin_Ite(sectId, infoType, extraUd)
    -- if infoType == "title" then
    --     LuaZongMenMgr.m_SectHasTitleMemberInfo = {}
    if infoType == "office" then
        LuaZongMenMgr.m_SectOfficeInfo = {}
    elseif infoType == "candidate" then
        LuaZongMenMgr.m_SectOfficeCandidataInfo = {}
    elseif infoType == "history" then
        LuaZongMenMgr.m_SectHistoryInfo = {}
    elseif infoType == "rights" then
        LuaZongMenMgr.m_SectOfficeRightInfo = {}
    elseif infoType == "member" then
        LuaZongMenMgr.m_SectMemberInfo = {}
    elseif infoType == "recommend" then
        LuaZongMenMgr.m_SectRecommendInfo = {}
    elseif infoType == "applylist" then
        LuaZongMenMgr.m_SectApplyerInfo = {}
    end
end

function Gas2Gac.SyncSectInfo_Ite(sectId, infoType, extra, Ud)
    if infoType == "overview" then
        LuaZongMenMgr.m_SectMainPageInfo = CustomPropertySerializable("CSectMainPageInfoRpc_Ite")
        LuaZongMenMgr.m_SectMainPageInfo:LoadFromString(Ud)
    else
        LuaZongMenMgr:GetCustomPropertyMapData(infoType, Ud, "office", "CSectOfficeInfoRpc", LuaZongMenMgr.m_SectOfficeInfo)
        LuaZongMenMgr:GetCustomPropertyMapData(infoType, Ud, "candidate", "CSectOfficeCandidataInfoRpc", LuaZongMenMgr.m_SectOfficeCandidataInfo)
        LuaZongMenMgr:GetCustomPropertyMapData(infoType, Ud, "history", "CSectHistoryInfo", LuaZongMenMgr.m_SectHistoryInfo)
        LuaZongMenMgr:GetCustomPropertyMapData(infoType, Ud, "rights", "CSectOfficeRightRPC", LuaZongMenMgr.m_SectOfficeRightInfo)
        LuaZongMenMgr:GetCustomPropertyMapData(infoType, Ud, "member", "CSectMemberInfoRpc", LuaZongMenMgr.m_SectMemberInfo)
        LuaZongMenMgr:GetCustomPropertyMapData(infoType, Ud, "recommend", "CSectRecommendInfoRpc_Ite", LuaZongMenMgr.m_SectRecommendInfo)
        LuaZongMenMgr:GetCustomPropertyMapData(infoType, Ud, "applylist", "CSectApplyerInfoRpc_Ite", LuaZongMenMgr.m_SectApplyerInfo)
    end
end

function Gas2Gac.SyncSectInfoEnd_Ite(sectId, infoType, extra, extraUd)
    if infoType == "overview" then
        local list = MsgPackImpl.unpack(extraUd)
        -- 是否可以设置宗旨
        local canSetMission = list.Count > 0 and list[0] or 0
        g_ScriptEvent:BroadcastInLua("OnZongMenInfoResult",LuaZongMenMgr.m_SectMainPageInfo,canSetMission)
    -- elseif infoType == "title" then
    --     g_ScriptEvent:BroadcastInLua("OnZongMenHasTitleMemberInfoResult",LuaZongMenMgr.m_SectHasTitleMemberInfo)
    elseif infoType == "office" then
        local list = MsgPackImpl.unpack(extraUd)
        g_ScriptEvent:BroadcastInLua("OnSectOfficeInfoResult",LuaZongMenMgr.m_SectOfficeInfo, list.Count > 0 and list[0] or 0)
    elseif infoType == "candidate" then
        g_ScriptEvent:BroadcastInLua("OnSectOfficeCandidataInfoResult",LuaZongMenMgr.m_SectOfficeCandidataInfo,extra)
    elseif infoType == "history" then
        g_ScriptEvent:BroadcastInLua("OnSectHistoryInfoResult",LuaZongMenMgr.m_SectHistoryInfo)
    elseif infoType == "rights" then
        g_ScriptEvent:BroadcastInLua("OnSectOfficeRightResult",LuaZongMenMgr.m_SectOfficeRightInfo)
    elseif infoType == "member" then
        local list = MsgPackImpl.unpack(extraUd)
        g_ScriptEvent:BroadcastInLua("OnSectMemberInfoResult",LuaZongMenMgr.m_SectMemberInfo,
        list.Count > 0 and list[0] or 0,list.Count > 1 and list[1] or 0,
        list.Count > 2 and list[2] or 0,list.Count > 3 and list[3] or 0)
    elseif infoType == "recommend" then
        g_ScriptEvent:BroadcastInLua("OnSectRecommendInfoResult",LuaZongMenMgr.m_SectRecommendInfo)
    elseif infoType == "applylist" then    
        g_ScriptEvent:BroadcastInLua("OnSectApplyerInfoResult",LuaZongMenMgr.m_SectApplyerInfo) 
    end
end

-- 宗派宗旨 Gac2Gas.QuerySectMission_Ite(sectId)
-- 对应交互稿中 宗派外-申请加入 当选中某个宗派时展示
function Gas2Gac.SyncSectMission_Ite(sectId, missionStr)
    g_ScriptEvent:BroadcastInLua("OnSyncSectMission_Ite",  sectId, missionStr)
end

-- Gac2Gas.RequestCleanSectApply_Ite 的返回
function Gas2Gac.SyncCleanSectApply_Ite()
    g_ScriptEvent:BroadcastInLua("OnSyncCleanSectApply_Ite")
end


function Gas2Gac.PlayerAcceptSectApplyResult_Ite(bSuccess)
	g_ScriptEvent:BroadcastInLua("OnPlayerAcceptSectApplyResult_Ite",bSuccess)
end


-- 命盘五行修改 Gac2Gas.RequestRandomWuXing_Ite 的返回
function Gas2Gac.SyncRandomWuXing_Ite(newWuXing)
    if CClientMainPlayer.Inst then
        CClientMainPlayer.Inst.BasicProp.MingGe = newWuXing
    end
    
    if newWuXing ~= 0 then
        local module = LuaZongMenMgr.m_OpenNewSystem and CLuaUIResources.NewMingGeJianDingWnd or CLuaUIResources.MingGeJianDingWnd
        if CUIManager.IsLoaded(module) then
            g_ScriptEvent:BroadcastInLua("OnZongMenMingGeJianDing", newWuXing)
        else
            CUIManager.ShowUI(module)
        end
        g_ScriptEvent:BroadcastInLua("OnSyncPlayerMingGe", newWuXing)
    end
end

function Gas2Gac.SyncSectFuWenRank_Ite(sectId, rankUd, effectExpireTime, todayLingfu)
    local rankData = CSectLingFuRankRpc_Ite()
    rankData:LoadFromString(rankUd)
    g_ScriptEvent:BroadcastInLua("SyncSectFuWenRank_Ite", sectId, rankData, effectExpireTime, todayLingfu)
end

function Gas2Gac.NotifySectBoomPlayStart()
    if not LuaZongMenMgr:IsMySectScene() then
        MessageWndManager.ShowOKCancelMessageWithTimelimitAndPriority(
        g_MessageMgr:FormatMessage("SECTPRACTICE_OPEN_INFORM"), 300, 0,
        DelegateFactory.Action(function()
            if CClientMainPlayer.Inst then
                Gac2Gas.RequestEnterSelfSectScene(CClientMainPlayer.Inst.BasicProp.SectId)
            end
        end),nil,nil,nil,false)
    end
    g_ScriptEvent:BroadcastInLua("NotifySectBoomPlayStart")
end

function Gas2Gac.SyncSectBoomInfo_Ite(sectId, towerHp, towerFullHp, endTimestamp)
    -- 废弃
end

function Gas2Gac.SyncSectBoomRank_Ite(sectId, rankUd, sectScore)
    if CClientMainPlayer.Inst == nil or CClientMainPlayer.Inst.BasicProp.SectId ~= sectId then
        return
    end
    local rankData = CSectBoomRankRpc_Ite()
    rankData:LoadFromString(rankUd)
    local rankTab = {}
    CommonDefs.DictIterate(rankData.Infos, DelegateFactory.Action_object_object(function (___key, ___value)
        table.insert(rankTab, ___value)
    end))
    table.sort(rankTab, function(a, b)
        return a.Damage > b.Damage
    end)
    g_ScriptEvent:BroadcastInLua("SyncSectBoomRank_Ite", sectId, rankTab, sectScore)
end

function Gas2Gac.SyncSectXiuxingPlayStatus_Ite(sectId, playStage, bJoined)
    if CClientMainPlayer.Inst == nil or CClientMainPlayer.Inst.BasicProp.SectId ~= sectId then
        return
    end
    LuaZongMenMgr.m_SectXiuxingPlayStage = playStage
    LuaZongMenMgr.m_InZongMenPracticePlay = playStage ~= EnumSectXiuxingPlayStage.eEnd
    if CTaskAndTeamWnd.Instance then
        CTaskAndTeamWnd.Instance.taskListBoard.TaskListVisible = true
    end
    g_ScriptEvent:BroadcastInLua("SyncSectXiuxingPlayStatus_Ite", sectId, playStage, bJoined)
end

function Gas2Gac.SyncSectNameAndStd_Ite(sectId, jiepaiId, name)
    if CommonDefs.IS_VN_CLIENT then
        local obj = CRenderScene.Inst.transform:Find("Models/Others/dj_dishe_062_001")
        if obj then
            obj.gameObject:SetActive(false)
        end
        return
    end
    local hideRoot = CRenderScene.Inst.transform:Find("HideRoot")
    if hideRoot then
        local dynamic = hideRoot:GetComponent(typeof(CSceneDynamicStuff))
        local data = Menpai_ZongpaiSlogan.GetData(jiepaiId)
        if data then
            local staffPath = SafeStringFormat3("Assets/Res/Levels/Dynamic/%s.prefab",data.path)
            if dynamic then
                dynamic:RemoveStuff("SectJieBei")
                dynamic:ShowStuff(true,staffPath,"SectJieBei")
            end
        end
    end
    TextMeshExDataSource.Inst:SetData(EnumToInt(EnumTextMeshExKey.SectPlaque), name)
    TextMeshExDataSource.Inst:SetData(EnumToInt(EnumTextMeshExKey.SectMonument), "")
end

-- 当前场景的修行玩法开启
function Gas2Gac.SceneSectXiuxingPlayStart()
    --print("Gas2Gac.SceneSectXiuxingPlayStart()")
    CUIManager.ShowUI(CLuaUIResources.ZongMenPracticeStateWnd)
    LuaZongMenMgr.m_InZongMenPracticePlay = true
    if CTaskAndTeamWnd.Instance then
        CTaskAndTeamWnd.Instance.taskListBoard.TaskListVisible = true
    end
    PlayerSettings.IsUseTempSetting = true
    PlayerSettings.VisiblePlayerLimit = 35
end

-- 当前场景的修行玩法结束
function Gas2Gac.SceneSectXiuxingPlayEnd()
    --print("Gas2Gac.SceneSectXiuxingPlayEnd()")
    LuaZongMenMgr.m_TeleportId2Hp = {}
    CUIManager.CloseUI(CLuaUIResources.ZongMenPracticeStateWnd)
    LuaZongMenMgr.m_InZongMenPracticePlay = false
    if CTaskAndTeamWnd.Instance then
        CTaskAndTeamWnd.Instance.taskListBoard.TaskListVisible = true
    end
    PlayerSettings.IsUseTempSetting = false
    PlayerSettings.VisiblePlayerLimit = PlayerSettings.VisiblePlayerLimit
    g_ScriptEvent:BroadcastInLua("SceneSectXiuxingPlayEnd")
end

LuaZongMenMgr.m_ShowSectRedAlert = false
-- Gac2Gas.QuerySectRedAlert -> Gas2Gac.SyncSectRedAlert
function Gas2Gac.SyncSectRedAlert(sectId, bHasRedAlert)
    LuaZongMenMgr.m_ShowSectRedAlert = bHasRedAlert
	g_ScriptEvent:BroadcastInLua("OnSyncSectRedAlert",sectId, bHasRedAlert)
end
