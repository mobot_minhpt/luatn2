-- Auto Generated!!
local CLinyuShopPopupMenu = import "L10.UI.CLinyuShopPopupMenu"
local Extensions = import "Extensions"
local Mall_LingYuMallSubCategory = import "L10.Game.Mall_LingYuMallSubCategory"
local NGUITools = import "NGUITools"
local Object = import "System.Object"
local QnButton = import "L10.UI.QnButton"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
CLinyuShopPopupMenu.m_Init_CS2LuaHook = function (this, anchorTrans, subCategorys, initSelect) 
    local count = subCategorys.Count

    this.m_Template.gameObject:SetActive(false)
    Extensions.RemoveAllChildren(this.m_Table.transform)
    this.m_RadioButtons = CreateFromClass(MakeArrayClass(QnSelectableButton), count)

    do
        local i = 0
        while i < count do
            local obj = NGUITools.AddChild(this.m_Table.gameObject, this.m_Template.gameObject)
            obj:SetActive(true)
            local button = CommonDefs.GetComponent_GameObject_Type(obj, typeof(QnSelectableButton))
            if subCategorys[i] == 0 then
                --显示全部图标
                button.m_BackGround.spriteName = this.m_TotalSprite
            else
                local data = Mall_LingYuMallSubCategory.GetData(subCategorys[i])
                if data ~= nil then
                    button.m_BackGround.spriteName = data.SpriteName
                end
            end
            this.m_RadioButtons[i] = button
            this.m_RadioButtons[i]:SetSelected(i == initSelect, false)
            this.m_RadioButtons[i].OnClick = MakeDelegateFromCSFunction(this.On_Click, MakeGenericClass(Action1, QnButton), this)
            i = i + 1
        end
    end
	if initSelect >= 0 and initSelect < count then
		GenericDelegateInvoke(this.OnSelect, Table2ArrayWithCount({this.m_RadioButtons[initSelect], initSelect}, 2, MakeArrayClass(Object)))
	end
    this:ShowSelf(true)
    this.m_Table:Reposition()
end
CLinyuShopPopupMenu.m_On_Click_CS2LuaHook = function (this, go) 
    do
        local i = 0
        while i < this.m_RadioButtons.Length do
            if this.m_RadioButtons[i] == go then
                this.CurrentIndex = i
                this.m_RadioButtons[i]:SetSelected(true, false)
                if this.OnSelect ~= nil then
                    GenericDelegateInvoke(this.OnSelect, Table2ArrayWithCount({this.m_RadioButtons[i], this.CurrentIndex}, 2, MakeArrayClass(Object)))
                end
            else
                this.m_RadioButtons[i]:SetSelected(false, false)
            end
            i = i + 1
        end
    end
end
