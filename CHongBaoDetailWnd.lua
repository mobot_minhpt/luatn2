-- Auto Generated!!
local CHongBaoDetailItem = import "L10.UI.CHongBaoDetailItem"
local CHongBaoDetailWnd = import "L10.UI.CHongBaoDetailWnd"
local CHongBaoMgr = import "L10.UI.CHongBaoMgr"
local DaoJuLiBao_Item = import "L10.Game.DaoJuLiBao_Item"
local DaoJuLiBao_System = import "L10.Game.DaoJuLiBao_System"
local EnumHongbaoType = import "L10.UI.EnumHongbaoType"
local LocalString = import "LocalString"
CHongBaoDetailWnd.m_Init_CS2LuaHook = function (this) 
    this.m_Table.m_DataSource = this

    this.m_MoneyLabel.text = ""
    this.m_DescriptionLabel.text = ""
    this.m_CountLabel.text = ""

    if not System.String.IsNullOrEmpty(CHongBaoMgr.s_HongBaoId) then
        if CHongBaoMgr.m_HongbaoType == EnumHongbaoType.Gift then
            Gac2Gas.RequestDaoJuLiBaoDetails(CHongBaoMgr.s_HongBaoId)
            this.m_GiftRoot:SetActive(true)
            this.m_HongbaoRoot:SetActive(false)
        else
            Gac2Gas.RequestHongBaoDetails(CHongBaoMgr.s_HongBaoId)
            this.m_GiftRoot:SetActive(false)
            this.m_HongbaoRoot:SetActive(true)
        end
    else
        this:Close()
    end
end
CHongBaoDetailWnd.m_OnReplyHongBaoDetails_CS2LuaHook = function (this, data) 
    this.m_DetailData = data
    if this.m_DetailData ~= nil then
        --m_MoneyLabel.text = m_DetailData.TotalSilver.ToString();
        this.m_DescriptionLabel.text = this.m_DetailData.HongBaoDescription
        if CHongBaoMgr.m_HongbaoType == EnumHongbaoType.Gift then
            this.m_TitleLabel.text = System.String.Format(LocalString.GetString("{0}的礼包"), this.m_DetailData.OwnerName)

            if data.ChannelType == EnumLibaoType_lua.WorldLibao then
                this.m_ChannelTypeLabel.text = LocalString.GetString("（世界礼包）")
            elseif data.ChannelType == EnumLibaoType_lua.GuildLibao then
                this.m_ChannelTypeLabel.text = LocalString.GetString("（帮会礼包）")
            elseif data.ChannelType == EnumLibaoType_lua.SystemLibao then
                this.m_ChannelTypeLabel.text = LocalString.GetString("(系统礼包)")
            end
            if data.ChannelType == EnumLibaoType_lua.SystemLibao then
                this.m_GiftTex:LoadMaterial(DaoJuLiBao_System.GetData(math.floor(data.PlayerId)).LiBaoIcon)
                this.m_GiftTypeLabel.text = DaoJuLiBao_System.GetData(math.floor(data.PlayerId)).LiBaoName
            else
                this.m_GiftTex:LoadMaterial(DaoJuLiBao_Item.GetData(data.Price).Icon)
                this.m_GiftTypeLabel.text = (DaoJuLiBao_Item.GetData(data.Price).Name .. "*") .. data.TotalCount
            end
        else
            this.m_TitleLabel.text = System.String.Format(LocalString.GetString("{0}的红包"), this.m_DetailData.OwnerName)
            this.m_MoneyLabel.text = tostring(this.m_DetailData.TotalSilver)
        end
        this.m_CountLabel.text = System.String.Format("{0}/{1}", this.m_DetailData.SnatchNumber, this.m_DetailData.TotalCount)
        this.m_Table:ReloadData(false, false)
    end
end
CHongBaoDetailWnd.m_ItemAt_CS2LuaHook = function (this, view, row) 
    local index = 0
    if CHongBaoMgr.m_HongbaoType == EnumHongbaoType.Gift then
        index = 1
    end
    local item = TypeAs(view:GetFromPool(index), typeof(CHongBaoDetailItem))
    if this.m_DetailData ~= nil and this.m_DetailData.SnatchRecords ~= nil and this.m_DetailData.SnatchRecords.Count > row then
        local record = this.m_DetailData.SnatchRecords[row]
        item:UpdateData(record)
    end
    return item
end
