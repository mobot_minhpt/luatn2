local DelegateFactory = import "DelegateFactory"
local GameObject      = import "UnityEngine.GameObject"
local UIGrid          = import "UIGrid"
local UILabel         = import "UILabel"
local UISprite        = import "UISprite"
local CServerTimeMgr  = import "L10.Game.CServerTimeMgr"
local CUICommonDef    = import "L10.UI.CUICommonDef"
local LuaTweenUtils   = import "LuaTweenUtils"
local Object          = import "UnityEngine.Object"
local UIEventListener = import "UIEventListener"
local UICamera        = import "UICamera"
local CUIManager      = import "L10.UI.CUIManager"
local MessageMgr      = import "L10.Game.MessageMgr"
local Extensions      = import "Extensions"
local CTrackMgr       = import "L10.Game.CTrackMgr"

LuaXinShengHuaJiPaintSetWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaXinShengHuaJiPaintSetWnd, "LeftButton", "LeftButton", GameObject)
RegistChildComponent(LuaXinShengHuaJiPaintSetWnd, "RightButton", "RightButton", GameObject)
RegistChildComponent(LuaXinShengHuaJiPaintSetWnd, "IndicatorTemplate", "IndicatorTemplate", GameObject)
RegistChildComponent(LuaXinShengHuaJiPaintSetWnd, "IndicatorGrid", "IndicatorGrid", UIGrid)
RegistChildComponent(LuaXinShengHuaJiPaintSetWnd, "SlideRegion", "SlideRegion", GameObject)
RegistChildComponent(LuaXinShengHuaJiPaintSetWnd, "FanyeFx", "FanyeFx", GameObject)
RegistChildComponent(LuaXinShengHuaJiPaintSetWnd, "TipButton", "TipButton", GameObject)
RegistChildComponent(LuaXinShengHuaJiPaintSetWnd, "PictureTemplate", "PictureTemplate", GameObject)
RegistChildComponent(LuaXinShengHuaJiPaintSetWnd, "PictureGrid", "PictureGrid", UIGrid)
RegistChildComponent(LuaXinShengHuaJiPaintSetWnd, "EmptyTemplate", "EmptyTemplate", GameObject)
RegistChildComponent(LuaXinShengHuaJiPaintSetWnd, "Part", "Part", GameObject)

--@endregion RegistChildComponent end

RegistClassMember(LuaXinShengHuaJiPaintSetWnd, "availItemNum")
RegistClassMember(LuaXinShengHuaJiPaintSetWnd, "curPage")
RegistClassMember(LuaXinShengHuaJiPaintSetWnd, "fadeTicks")
RegistClassMember(LuaXinShengHuaJiPaintSetWnd, "countDownTick")
RegistClassMember(LuaXinShengHuaJiPaintSetWnd, "dragStartX")
RegistClassMember(LuaXinShengHuaJiPaintSetWnd, "drawingNum")

function LuaXinShengHuaJiPaintSetWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.LeftButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLeftButtonClick()
	end)

	UIEventListener.Get(self.RightButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRightButtonClick()
	end)

	UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)

    --@endregion EventBind end
	self:InitActive()
end

function LuaXinShengHuaJiPaintSetWnd:InitActive()
	self.PictureTemplate:SetActive(false)
	self.IndicatorTemplate:SetActive(false)
	self.EmptyTemplate:SetActive(false)
	self.FanyeFx:SetActive(false)
end

function LuaXinShengHuaJiPaintSetWnd:OnEnable()
    g_ScriptEvent:AddListener("XinShengHuaJiSyncPictures", self, "OnSyncPics")
    g_ScriptEvent:AddListener("XinShengHuaJiAddPictureSuccess", self, "OnAddPicSuccess")
    g_ScriptEvent:AddListener("XinShengHuaJiDelPictureSuccess", self, "OnDelPicSuccess")
    g_ScriptEvent:AddListener("XinShengHuaJiReadPictureSuccess", self, "OnReadPicSuccess")
	g_ScriptEvent:AddListener("XinShengHuaJiSetHuaJiNum",self,"OnSetHuaJiNum")
end

function LuaXinShengHuaJiPaintSetWnd:OnDisable()
    g_ScriptEvent:RemoveListener("XinShengHuaJiSyncPictures", self, "OnSyncPics")
	g_ScriptEvent:RemoveListener("XinShengHuaJiAddPictureSuccess", self, "OnAddPicSuccess")
    g_ScriptEvent:RemoveListener("XinShengHuaJiDelPictureSuccess", self, "OnDelPicSuccess")
	g_ScriptEvent:RemoveListener("XinShengHuaJiReadPictureSuccess", self, "OnReadPicSuccess")
	g_ScriptEvent:RemoveListener("XinShengHuaJiSetHuaJiNum",self,"OnSetHuaJiNum")
end

function LuaXinShengHuaJiPaintSetWnd:OnSyncPics()
	self:InitClassMembers()
	self:InitAllPics()
	self:UpdatePage(1, true)
	if self.drawingNum > 0 then
		self:UpdateTime()
	end
end

-- 增加了一张图像
function LuaXinShengHuaJiPaintSetWnd:OnAddPicSuccess(time, data)
	-- 数据改变
    LuaWuMenHuaShiMgr:InsertTimePic(time, data)
    LuaWuMenHuaShiMgr:UploadPicsSort()
	local id = self:GetId(time)

	-- 显示改变
	local trans = self.PictureGrid.transform
	local picNum = #LuaWuMenHuaShiMgr.uploadPics
	Object.DestroyImmediate(trans:GetChild(picNum - 1).gameObject)

	local beforeDrawingNum = self.drawingNum
	local itemGo = self:AddPicItem(time, id)
	itemGo.transform:SetSiblingIndex(id - 1)
	self:UpdatePage(self.curPage, false)

	-- 新增绘制项需开启计时
	if beforeDrawingNum == 0 and self.drawingNum > 0 then
		self:UpdateTime()
	end
end

-- 删除了一张图像
function LuaXinShengHuaJiPaintSetWnd:OnDelPicSuccess(time)
	-- 数据改变
	local id = self:GetId(time)
    if id == nil then return end
	table.remove(LuaWuMenHuaShiMgr.uploadPics, id)

	-- 显示改变
	local trans = self.PictureGrid.transform
	Object.DestroyImmediate(trans:GetChild(id - 1).gameObject)
	local itemGo = CUICommonDef.AddChild(self.PictureGrid.gameObject, self.EmptyTemplate)
	self:SetAddItem(itemGo)
	itemGo.transform:SetSiblingIndex(#LuaWuMenHuaShiMgr.uploadPics)
	self:UpdatePage(self.curPage, false)
end

-- 改变了新增状态
function LuaXinShengHuaJiPaintSetWnd:OnReadPicSuccess(time)
	-- 数据改变
    local id = self:GetId(time)
	if id == nil then return end
	local picData = LuaWuMenHuaShiMgr.uploadPics[id].pic
	local readType = WuMenHuaShi_XinShengHuaJiSetting.GetData().XinShengHuaJiReadType
	picData[readType] = 1

	-- 显示改变
	local trans = self.PictureGrid.transform
	self:SetNewActive(trans:GetChild(id - 1), id)
end

-- 画集数量改变
function LuaXinShengHuaJiPaintSetWnd:OnSetHuaJiNum(num)
	LuaItemInfoMgr:CloseItemConsumeWnd()

	local defaultNum = WuMenHuaShi_XinShengHuaJiSetting.GetData().XinShengHuaJiDefaultNum
	local availNum = num + defaultNum

	if self.availItemNum < availNum then
		-- 如果num为最大画集数，删除扩容项
		if availNum == WuMenHuaShi_XinShengHuaJiSetting.GetData().XinShengHuaJiMaxNum then
			local trans = self.PictureGrid.transform
			Object.DestroyImmediate(trans:GetChild(trans.childCount - 1).gameObject)
		end

		for i = self.availItemNum + 1, availNum do
			local itemGo = CUICommonDef.AddChild(self.PictureGrid.gameObject, self.EmptyTemplate)
			self:SetAddItem(itemGo)
			itemGo.transform:SetSiblingIndex(i - 1)
		end
		self.availItemNum = availNum
		self:UpdatePage(self.curPage, true)
	end
end

-- 根据time获取id
function LuaXinShengHuaJiPaintSetWnd:GetId(time)
    if LuaWuMenHuaShiMgr.uploadPics == nil then return nil end
    for id = 1, #LuaWuMenHuaShiMgr.uploadPics do
        if LuaWuMenHuaShiMgr.uploadPics[id].time == tonumber(time) then
            return id
        end
    end
    return nil
end

-- 初始化成员变量
function LuaXinShengHuaJiPaintSetWnd:InitClassMembers()
	self:GetItemNum()
	self.fadeTicks = {}
end

-- 添加拖拽响应
function LuaXinShengHuaJiPaintSetWnd:AddDragListener(dragGo)
    UIEventListener.Get(dragGo).onDragStart = DelegateFactory.VoidDelegate(function(go)
		self.dragStartX = UICamera.currentTouch.pos.x
    end)

    UIEventListener.Get(dragGo).onDragEnd = DelegateFactory.VoidDelegate(function(go)
		local diff = UICamera.currentTouch.pos.x - self.dragStartX
        if diff < -50 then
			self:PageRight()
		elseif diff > 50 then
			self:PageLeft()
		end
    end)
end

-- 更新时间
function LuaXinShengHuaJiPaintSetWnd:UpdateTime()
	self:UpdateTimeOnce()
	if self.countDownTick then
		UnRegisterTick(self.countDownTick)
		self.countDownTick = nil
	end
	self.countDownTick = RegisterTick(function()
		self:UpdateTimeOnce()
	end, 1000)
end

-- 更新一次时间
function LuaXinShengHuaJiPaintSetWnd:UpdateTimeOnce()
	local trans = self.PictureGrid.transform
	local pics = LuaWuMenHuaShiMgr.uploadPics
	local curDrawingNum = #pics
	for i = 1, #pics do
		local waitingTime = WuMenHuaShi_XinShengHuaJiSetting.GetData().XinShengHuajiWaitingTime * 60
		local leftTime = waitingTime - math.floor(CServerTimeMgr.Inst.timeStamp - pics[i].time)
		if leftTime <= 0 then
			curDrawingNum = i - 1
			break
		end
		local hour = math.floor(leftTime / 3600)
		local minute = math.floor((leftTime - 3600 * hour) / 60)
		local second = math.floor(leftTime - 3600 * hour - 60 * minute)
		local label = trans:GetChild(i - 1):Find("Drawing")
		label = label:Find("Label"):GetComponent(typeof(UILabel))
		label.text = SafeStringFormat3(LocalString.GetString("尤艾绘制中 %02d:%02d:%02d"), hour, minute, second)
	end

	-- 到时间更新
	if curDrawingNum ~= self.drawingNum then
		for id = curDrawingNum + 1, self.drawingNum do
			Object.DestroyImmediate(trans:GetChild(id - 1).gameObject)
			local itemGo = CUICommonDef.AddChild(self.PictureGrid.gameObject, self.PictureTemplate)
			self:SetPicItem(itemGo, id)
			self:SetPicContent(itemGo, id)
			itemGo.transform:SetSiblingIndex(id - 1)
		end

		self.drawingNum = curDrawingNum
		self:UpdatePage(self.curPage, false)
	end

	-- 没有绘制中项不再更新
	if self.drawingNum == 0 then
		if self.countDownTick then
			UnRegisterTick(self.countDownTick)
			self.countDownTick = nil
		end
	end
end

-- 关闭界面时需关闭计时器
function LuaXinShengHuaJiPaintSetWnd:OnDestroy()
	if self.countDownTick then
		UnRegisterTick(self.countDownTick)
	end
	self:ClearFadeTicks()
end

function LuaXinShengHuaJiPaintSetWnd:ClearFadeTicks()
	if self.fadeTicks == nil then return end
	for id, data in pairs(self.fadeTicks) do
		if data then
			UnRegisterTick(data)
		end
	end
end

function LuaXinShengHuaJiPaintSetWnd:Init()
	self:QueryPics()
end

-- 请求图像
function LuaXinShengHuaJiPaintSetWnd:QueryPics()
	Gac2Gas.XinShengHuaJi_QueryPictures()
end

-- 获取可用的图像数量
function LuaXinShengHuaJiPaintSetWnd:GetItemNum()
	local defaultNum = WuMenHuaShi_XinShengHuaJiSetting.GetData().XinShengHuaJiDefaultNum
	self.availItemNum = CClientMainPlayer.Inst.PlayProp.XinShengHuaJiNum + defaultNum
end

-- 初始化所有图像
function LuaXinShengHuaJiPaintSetWnd:InitAllPics()
	Extensions.RemoveAllChildren(self.PictureGrid.transform)
	self:AddDragListener(self.SlideRegion)

	self.drawingNum = 0
	for i = 1, #LuaWuMenHuaShiMgr.uploadPics do
		local time = LuaWuMenHuaShiMgr.uploadPics[i].time
		self:AddPicItem(time, i)
	end

	-- 添加
	for i = #LuaWuMenHuaShiMgr.uploadPics + 1, self.availItemNum do
		local item = CUICommonDef.AddChild(self.PictureGrid.gameObject, self.EmptyTemplate)
		self:SetAddItem(item)
	end

	-- 扩容
	if self.availItemNum < WuMenHuaShi_XinShengHuaJiSetting.GetData().XinShengHuaJiMaxNum then
		local item = CUICommonDef.AddChild(self.PictureGrid.gameObject, self.EmptyTemplate)
		self:SetExpandItem(item)
	end
end

-- 添加一个图片项
function LuaXinShengHuaJiPaintSetWnd:AddPicItem(time, id)
	local waitingTime = WuMenHuaShi_XinShengHuaJiSetting.GetData().XinShengHuajiWaitingTime * 60
	local leftTime = waitingTime - math.floor(CServerTimeMgr.Inst.timeStamp - time)
	local itemGo = nil
	if leftTime <= 0 then
		-- 图片
		itemGo = CUICommonDef.AddChild(self.PictureGrid.gameObject, self.PictureTemplate)
		self:SetPicContent(itemGo, id)
		self:SetPicItem(itemGo, id)
	else
		-- 绘制中
		self.drawingNum = self.drawingNum + 1
		itemGo = CUICommonDef.AddChild(self.PictureGrid.gameObject, self.EmptyTemplate)
		self:SetEmptyTemplateActive(itemGo, 3)
		self:AddDragListener(itemGo)
	end
	return itemGo
end

-- 设置图片内容
function LuaXinShengHuaJiPaintSetWnd:SetPicContent(itemGo, id)
	LuaWuMenHuaShiMgr:GeneratePic(itemGo.transform:Find("Panel"), self.Part, id)
	self:SetNewActive(itemGo, id)

	self:SetNpcUnlockable(id)
end

-- 设置NPC为可解锁
function LuaXinShengHuaJiPaintSetWnd:SetNpcUnlockable(id)
	local picData = LuaWuMenHuaShiMgr.uploadPics[id].pic
    local npcId = picData[WuMenHuaShi_XinShengHuaJiSetting.GetData().XinShengHuaJiNpcType]
	Gac2Gas.RuMengJi_Unlockable(npcId)
end

-- 是否为新增状态
function LuaXinShengHuaJiPaintSetWnd:SetNewActive(itemGo, id)
	local newGo = itemGo.transform:Find("New").gameObject
	local picData = LuaWuMenHuaShiMgr.uploadPics[id].pic
	local readType = WuMenHuaShi_XinShengHuaJiSetting.GetData().XinShengHuaJiReadType
	local readState = picData[readType]
	if readState == 0 then
		newGo:SetActive(true)
	else
		newGo:SetActive(false)
	end
end

-- 设置图片项属性
function LuaXinShengHuaJiPaintSetWnd:SetPicItem(itemGo, id)
	self:AddDragListener(itemGo)
	local time = LuaWuMenHuaShiMgr.uploadPics[id].time
	UIEventListener.Get(itemGo).onClick = DelegateFactory.VoidDelegate(function(go)
		Gac2Gas.XinShengHuaJi_ReadPicture(time)
		-- 打开放大界面
		LuaWuMenHuaShiMgr.curShowPicId = self:GetId(time)
		CUIManager.ShowUI(CLuaUIResources.XinShengHuaJiBigPicWnd)
	end)
end

-- 设置添加项属性
function LuaXinShengHuaJiPaintSetWnd:SetAddItem(itemGo)
	self:SetEmptyTemplateActive(itemGo, 1)
	self:AddDragListener(itemGo)
	UIEventListener.Get(itemGo).onClick = DelegateFactory.VoidDelegate(function(go)
		CTrackMgr.Inst:FindNPC(20006077, 16000006, 140, 105, nil, nil)
		g_MessageMgr:ShowMessage("WUMENHUASHI_USE_XINSHENGHUAJI_ADDNEWPAINTING")
		LuaWuMenHuaShiMgr:CloseXinShengHuaJiPaintSetWnd()
	end)
end

-- 设置扩容项属性
function LuaXinShengHuaJiPaintSetWnd:SetExpandItem(itemGo)
	self:SetEmptyTemplateActive(itemGo, 2)
	self:AddDragListener(itemGo)
	UIEventListener.Get(itemGo).onClick = DelegateFactory.VoidDelegate(function(go)
        local messageformat = MessageMgr.Inst:GetMessageFormat("NEED_USE_MINGXINJIAN", true)
        LuaWuMenHuaShiMgr:ExpandConfirm(messageformat)
	end)
end

-- 更新页
function LuaXinShengHuaJiPaintSetWnd:UpdatePage(page, doUpdateGrid)
	self.curPage = page
	local trans = self.PictureGrid.transform
	for i = 1, trans.childCount do
		if math.floor((i - 1) / 6) == self.curPage - 1 then
			trans:GetChild(i - 1).gameObject:SetActive(true)
		else
			trans:GetChild(i - 1).gameObject:SetActive(false)
		end
	end
	self.PictureGrid:Reposition()
	if doUpdateGrid then
		self:UpdateIndicatorGrid()
	end
	self:UpdateIndicator()
end

-- 更新指示图标列表
function LuaXinShengHuaJiPaintSetWnd:UpdateIndicatorGrid()
	local trans = self.PictureGrid.transform
	local pageNum = math.ceil(trans.childCount / 6)
	trans = self.IndicatorGrid.transform
	if trans.childCount == pageNum then return end

	for i = 1, trans.childCount do
		trans:GetChild(i - 1).gameObject:SetActive(false)
	end

	if trans.childCount > pageNum then
		for i = 1, pageNum do
			trans:GetChild(i - 1).gameObject:SetActive(true)
		end
	elseif trans.childCount < pageNum then
		for i = 1, trans.childCount do
			trans:GetChild(i - 1).gameObject:SetActive(true)
		end
		for i = trans.childCount + 1, pageNum do
			local itemGo = CUICommonDef.AddChild(self.IndicatorGrid.gameObject, self.IndicatorTemplate)
			itemGo:SetActive(true)
		end
	end
	self.IndicatorGrid:Reposition()
end

-- 更新指示图标
function LuaXinShengHuaJiPaintSetWnd:UpdateIndicator()
	local trans = self.IndicatorGrid.transform
	for i = 1, trans.childCount do
		local sprite = trans:GetChild(i - 1):GetComponent(typeof(UISprite))
		if i == self.curPage then
			sprite.color = NGUIText.ParseColor24("666666", 0)
		else
			sprite.color = NGUIText.ParseColor24("FFFFFF", 0)
		end
	end
end

-- 设置添加、扩容还是绘制中的状态
function LuaXinShengHuaJiPaintSetWnd:SetEmptyTemplateActive(item, id)
	local trans = item.transform
	for i = 1, trans.childCount do
		trans:GetChild(i - 1).gameObject:SetActive(false)
	end
	if id == 1 then
		trans:Find("Add").gameObject:SetActive(true)
	elseif id == 2 then
		trans:Find("Expand").gameObject:SetActive(true)
	elseif id == 3 then
		trans:Find("Drawing").gameObject:SetActive(true)
	end
end

-- 淡入淡出
function LuaXinShengHuaJiPaintSetWnd:FadeInAndOut(nextPage)
	local start = (self.curPage - 1) * 6 + 1
	local trans = self.PictureGrid.transform
	for i = start, start + 5 do
		if i > trans.childCount then break end
		LuaTweenUtils.TweenAlpha(trans:GetChild(i - 1), 1, 0, 0.3)
	end
	self:ClearFadeTicks()
	self.fadeTicks = {}
	start = (nextPage - 1) * 6 + 1
	for i = start, start + 5 do
		if i > trans.childCount then break end
		-- 延时执行
		local tick = RegisterTickOnce(function()
			self:UpdatePage(nextPage, false)
			LuaTweenUtils.TweenAlpha(trans:GetChild(i - 1), 0, 1, 0.3)
		end, 300)
		table.insert(self.fadeTicks, tick)
	end
end

-- 向左翻页
function LuaXinShengHuaJiPaintSetWnd:PageLeft()
	if self.curPage == nil then return end
	if self.curPage <= 1 then return end
	self:FadeInAndOut(self.curPage - 1)
end

-- 向右翻页
function LuaXinShengHuaJiPaintSetWnd:PageRight()
	if self.curPage == nil then return end
	local trans = self.PictureGrid.transform
	if trans.childCount <= self.curPage * 6 then return end
	self.FanyeFx:SetActive(false)
	self.FanyeFx:SetActive(true)
	self:FadeInAndOut(self.curPage + 1)
end

--@region UIEvent

function LuaXinShengHuaJiPaintSetWnd:OnLeftButtonClick()
	self:PageLeft()
end

function LuaXinShengHuaJiPaintSetWnd:OnRightButtonClick()
	self:PageRight()
end

function LuaXinShengHuaJiPaintSetWnd:OnTipButtonClick()
	g_MessageMgr:ShowMessage("XINSHENGHUAJI_PAINTSET_RULE")
end


--@endregion UIEvent
