require("3rdParty/ScriptEvent")

local CCommonLuaWnd = import "L10.UI.CCommonLuaWnd"
local NPC_NPC = import "L10.Game.NPC_NPC"

LuaButterflyCrisisNoticeWnd = class()

RegistChildComponent(LuaButterflyCrisisNoticeWnd, "NoticeLabel", UILabel)
RegistChildComponent(LuaButterflyCrisisNoticeWnd, "CheckBtn", GameObject)
RegistChildComponent(LuaButterflyCrisisNoticeWnd, "IgnoreBtn", GameObject)

RegistClassMember(LuaButterflyCrisisNoticeWnd, "m_Wnd")

function LuaButterflyCrisisNoticeWnd:Awake()
    self.m_Wnd = self.gameObject:GetComponent(typeof(CCommonLuaWnd))
end

function LuaButterflyCrisisNoticeWnd:Init()

    local butterflyPlay = ButterflyCrisis_Play.GetData(LuaButterflyCrisisMgr.m_NoticeNPCIdx)
    if not butterflyPlay then self.m_Wnd:Close() return end

    local npc = NPC_NPC.GetData(butterflyPlay.NpcTemplateId)
    local notice = g_MessageMgr:FormatMessage("Butterfly_Crisis_Notice", npc.Name)
    self.NoticeLabel.text = notice

    CommonDefs.AddOnClickListener(self.CheckBtn.gameObject, DelegateFactory.Action_GameObject(function (go)
        self:OnCheckBtnClicked(go)
    end), false)

    CommonDefs.AddOnClickListener(self.IgnoreBtn.gameObject, DelegateFactory.Action_GameObject(function (go)
        self:OnIngoreBtnClicked(go)
    end), false)
end

function LuaButterflyCrisisNoticeWnd:OnCheckBtnClicked(go)
    Gac2Gas.ButterflyGetUIStatus()
    self.m_Wnd:Close()
end

function LuaButterflyCrisisNoticeWnd:OnIngoreBtnClicked(go)
    self.m_Wnd:Close()
end

function LuaButterflyCrisisNoticeWnd:Update()
    self.m_Wnd:ClickThroughToClose()
end

return LuaButterflyCrisisNoticeWnd