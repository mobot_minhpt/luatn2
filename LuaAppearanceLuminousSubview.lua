local CUITexture = import "L10.UI.CUITexture"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local Extensions = import "Extensions"
local CButton = import "L10.UI.CButton"
local EnumGender = import "L10.Game.EnumGender"
local UISprite = import "UISprite"
local Color = import "UnityEngine.Color"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"

LuaAppearanceLuminousSubview = class()

RegistChildComponent(LuaAppearanceLuminousSubview,"m_LuminousItem", "CommonItemCell", GameObject)
RegistChildComponent(LuaAppearanceLuminousSubview,"m_ItemDisplay", "AppearanceCommonItemDisplayView", CCommonLuaScript)
RegistChildComponent(LuaAppearanceLuminousSubview,"m_SwitchButton", "CommonHeaderSwitch", CButton)
RegistChildComponent(LuaAppearanceLuminousSubview,"m_ContentGrid", "ContentGrid", UIGrid)
RegistChildComponent(LuaAppearanceLuminousSubview,"m_ContentScrollView", "ContentScrollView", UIScrollView)

RegistClassMember(LuaAppearanceLuminousSubview, "m_AllIntensifySuitAppearances") --所有强化套装外观
RegistClassMember(LuaAppearanceLuminousSubview, "m_AllGemGroupAppearances") --所有石之灵外观 同时最多只有一个， 石之灵只有开关，不需要应用和去除
RegistClassMember(LuaAppearanceLuminousSubview, "m_AllSkillAppearances") --所有技能外观，极少获得
RegistClassMember(LuaAppearanceLuminousSubview, "m_SelectedDataId")
RegistClassMember(LuaAppearanceLuminousSubview, "m_PreviewType")

function LuaAppearanceLuminousSubview:Awake()
end

--type取值1 强化套装 2 石之灵  3 技能
function LuaAppearanceLuminousSubview:Init(type)
    self.m_SwitchButton.gameObject:SetActive(true)
    --组件共用，每次需要重新关联响应方法
    UIEventListener.Get(self.m_SwitchButton.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) self:OnSwitchButtonClick() end)
    
    self.m_PreviewType = type
    self:SetDefaultSelection()
    self:LoadData()
end

function LuaAppearanceLuminousSubview:InitItem(itemGo, appearanceData)
    local iconTexture = itemGo.transform:GetComponent(typeof(CUITexture))
    local disabledGo = itemGo.transform:Find("Disabled").gameObject
    local lockedGo = itemGo.transform:Find("Locked").gameObject
    local cornerGo = itemGo.transform:Find("Corner").gameObject
    local selectedGo = itemGo.transform:Find("Selected").gameObject
    iconTexture:LoadMaterial(appearanceData.icon)

    if self.m_PreviewType == EnumAppearancePreviewLuminousType.eIntensifySuit then
        lockedGo:SetActive(not LuaAppearancePreviewMgr:MainPlayerHasIntensitySuit(appearanceData.fxId))
        cornerGo:SetActive(LuaAppearancePreviewMgr:IsCurrentInUseIntensitySuit(appearanceData.fxId))
        selectedGo:SetActive(self.m_SelectedDataId and self.m_SelectedDataId==appearanceData.fxId)

    elseif self.m_PreviewType == EnumAppearancePreviewLuminousType.eGemGroup then
        lockedGo:SetActive(false)
        cornerGo:SetActive(LuaAppearancePreviewMgr:MainPlayerHasGemGroup())
        selectedGo:SetActive(self.m_SelectedDataId and self.m_SelectedDataId == appearanceData.id)
        
    elseif self.m_PreviewType == EnumAppearancePreviewLuminousType.eSkillAppearance then
        lockedGo:SetActive(not LuaAppearancePreviewMgr:MainPlayerHasSkillAppear(appearanceData.id))
        cornerGo:SetActive(LuaAppearancePreviewMgr:IsCurrentInUseSkillAppear(appearanceData.id))
        selectedGo:SetActive(self.m_SelectedDataId and self.m_SelectedDataId == appearanceData.id)
    end
    disabledGo:SetActive(lockedGo.activeSelf)
    
    UIEventListener.Get(itemGo).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnItemClick(go)
    end)
end

function LuaAppearanceLuminousSubview:LoadData()
    if self.m_PreviewType == EnumAppearancePreviewLuminousType.eIntensifySuit then
        self.m_AllIntensifySuitAppearances = LuaAppearancePreviewMgr:GetAllIntensifySuitInfo(false)
    elseif self.m_PreviewType == EnumAppearancePreviewLuminousType.eGemGroup then
        self.m_AllGemGroupAppearances = LuaAppearancePreviewMgr:GetAllGemGroupInfo()
    elseif self.m_PreviewType == EnumAppearancePreviewLuminousType.eSkillAppearance then
        self.m_AllSkillAppearances = LuaAppearancePreviewMgr:GetAllSkillAppearInfo()
    end

    Extensions.RemoveAllChildren(self.m_ContentGrid.transform)
    self.m_ContentGrid.gameObject:SetActive(true)

    if self.m_PreviewType == EnumAppearancePreviewLuminousType.eIntensifySuit then
        for i = 1, #self.m_AllIntensifySuitAppearances do
            local child = CUICommonDef.AddChild(self.m_ContentGrid.gameObject, self.m_LuminousItem)
            child:SetActive(true)
            self:InitItem(child, self.m_AllIntensifySuitAppearances[i])
        end
    elseif self.m_PreviewType == EnumAppearancePreviewLuminousType.eGemGroup then
        for i = 1, #self.m_AllGemGroupAppearances do
            local child = CUICommonDef.AddChild(self.m_ContentGrid.gameObject, self.m_LuminousItem)
            child:SetActive(true)
            self:InitItem(child, self.m_AllGemGroupAppearances[i])
        end
    elseif self.m_PreviewType == EnumAppearancePreviewLuminousType.eSkillAppearance then
        for i = 1, #self.m_AllSkillAppearances do
            local child = CUICommonDef.AddChild(self.m_ContentGrid.gameObject, self.m_LuminousItem)
            child:SetActive(true)
            self:InitItem(child, self.m_AllSkillAppearances[i])
        end
    end
    self.m_ContentGrid:Reposition()
    self.m_ContentScrollView:ResetPosition()
    self:InitSelection()
end

function LuaAppearanceLuminousSubview:SetDefaultSelection()
    if self.m_PreviewType == EnumAppearancePreviewLuminousType.eIntensifySuit then
        self.m_SelectedDataId = LuaAppearancePreviewMgr:IsShowIntensitySuit() and LuaAppearancePreviewMgr:GetCurrentInUseIntensitySuit() or 0
    elseif self.m_PreviewType == EnumAppearancePreviewLuminousType.eGemGroup then
        self.m_SelectedDataId = LuaAppearancePreviewMgr:IsShowGemGroup() and LuaAppearancePreviewMgr:GetCurrentInUseGemGroup() or 0
    elseif self.m_PreviewType == EnumAppearancePreviewLuminousType.eSkillAppearance then
        self.m_SelectedDataId = LuaAppearancePreviewMgr:IsShowSkillAppear() and LuaAppearancePreviewMgr:GetCurrentSkillAppearId() or 0
    end
end

function LuaAppearanceLuminousSubview:InitSelection()
    if self.m_SelectedDataId == 0 then
        self:OnItemClick(nil)
        return
    end

    local childCount = self.m_ContentGrid.transform.childCount
    for i=0, childCount-1 do
        local childGo = self.m_ContentGrid.transform:GetChild(i).gameObject
        if self.m_PreviewType == EnumAppearancePreviewLuminousType.eIntensifySuit then
            local appearanceData = self.m_AllIntensifySuitAppearances[i+1]
            if appearanceData.fxId == self.m_SelectedDataId then
                self:OnItemClick(childGo)
                break
            end
        elseif self.m_PreviewType == EnumAppearancePreviewLuminousType.eGemGroup then
            local appearanceData = self.m_AllGemGroupAppearances[i+1]
            if appearanceData.id == self.m_SelectedDataId then
                self:OnItemClick(childGo)
                break
            end
        elseif self.m_PreviewType == EnumAppearancePreviewLuminousType.eSkillAppearance then
            local appearanceData = self.m_AllSkillAppearances[i+1]
            if appearanceData.id == self.m_SelectedDataId then
                self:OnItemClick(childGo)
                break
            end
        end
    end
end

function LuaAppearanceLuminousSubview:OnItemClick(go)
    local childCount = self.m_ContentGrid.transform.childCount
    for i=0, childCount-1 do
        local childGo = self.m_ContentGrid.transform:GetChild(i).gameObject
        if childGo == go then
            childGo.transform:Find("Selected").gameObject:SetActive(true)
            if self.m_PreviewType == EnumAppearancePreviewLuminousType.eIntensifySuit then
                self.m_SelectedDataId = self.m_AllIntensifySuitAppearances[i+1].fxId
                if not LuaAppearancePreviewMgr:IsShowIntensitySuit() then
                    g_MessageMgr:ShowMessage("Appearance_Preview_Hide_Itensify_Suit_Tip")
                end
            elseif self.m_PreviewType == EnumAppearancePreviewLuminousType.eGemGroup then
                self.m_SelectedDataId = self.m_AllGemGroupAppearances[i+1].id
                if not LuaAppearancePreviewMgr:IsShowGemGroup() then
                    g_MessageMgr:ShowMessage("Appearance_Preview_Hide_Gem_Group_Tip")
                end 
            elseif self.m_PreviewType == EnumAppearancePreviewLuminousType.eSkillAppearance then
                self.m_SelectedDataId = self.m_AllSkillAppearances[i+1].id
                if not LuaAppearancePreviewMgr:IsShowSkillAppear() then
                    g_MessageMgr:ShowMessage("Appearance_Preview_Hide_Skill_Appearance_Tip")
                end
            end
        else
            childGo.transform:Find("Selected").gameObject:SetActive(false)
        end
    end
    
    self:UpdateItemDisplay()
    if self.m_PreviewType == EnumAppearancePreviewLuminousType.eIntensifySuit then
        g_ScriptEvent:BroadcastInLua("RequestPreviewMainPlayerIntensitySuit", self.m_SelectedDataId)
    elseif self.m_PreviewType == EnumAppearancePreviewLuminousType.eGemGroup then
        g_ScriptEvent:BroadcastInLua("RequestPreviewMainPlayerGemGroup", self.m_SelectedDataId)
    elseif self.m_PreviewType == EnumAppearancePreviewLuminousType.eSkillAppearance then
        g_ScriptEvent:BroadcastInLua("RequestPreviewMainPlayerSkillAppearance", self.m_SelectedDataId)
    end
end

function LuaAppearanceLuminousSubview:UpdateItemDisplay()
    self.m_ItemDisplay.gameObject:SetActive(true)
    if self.m_PreviewType == EnumAppearancePreviewLuminousType.eIntensifySuit then
        self.m_SwitchButton.Text = LocalString.GetString("强化套装")
        self.m_SwitchButton.Selected = LuaAppearancePreviewMgr:IsShowIntensitySuit()
    elseif self.m_PreviewType == EnumAppearancePreviewLuminousType.eGemGroup then
        self.m_SwitchButton.Text = LocalString.GetString("石之灵")
        self.m_SwitchButton.Selected = LuaAppearancePreviewMgr:IsShowGemGroup()
    elseif self.m_PreviewType == EnumAppearancePreviewLuminousType.eSkillAppearance then
        self.m_SwitchButton.Text = LocalString.GetString("技能外观")
        self.m_SwitchButton.Selected = LuaAppearancePreviewMgr:IsShowSkillAppear()
    end
    local id = self.m_SelectedDataId and self.m_SelectedDataId or 0
    if id==0 then
        self.m_ItemDisplay:Clear()
        return
    end
    
    local icon = ""
    local name = ""
    local desc = ""
    local disabled = true
    local locked = true
    local buttonTbl = {}
    if self.m_PreviewType == EnumAppearancePreviewLuminousType.eIntensifySuit then
        local appearanceData = self:GetCurrentSelectedSuitData()
        icon = appearanceData.icon
        name = appearanceData.name
        desc = appearanceData.condition
        if CClientMainPlayer.Inst then
            local exist = LuaAppearancePreviewMgr:MainPlayerHasIntensitySuit(appearanceData.fxId)
            local inUse = LuaAppearancePreviewMgr:IsCurrentInUseIntensitySuit(appearanceData.fxId)
            disabled = not exist
            locked = not exist
            if exist and not inUse then
                table.insert(buttonTbl, {text=LocalString.GetString("更换"), isYellow=false, action=function(go) self:OnApplyButtonClick() end})
            elseif exist and inUse then
                table.insert(buttonTbl, {text=LocalString.GetString("脱下"), isYellow=false, action=function(go) self:OnRemoveButtonClick() end})
            end
        end
    elseif self.m_PreviewType == EnumAppearancePreviewLuminousType.eGemGroup then
        local appearanceData = self:GetCurrentSelectedGemGroupData()
        icon = appearanceData.icon
        name = appearanceData.colorName
        desc = appearanceData.condition
        locked = not LuaAppearancePreviewMgr:MainPlayerHasGemGroup()
        disabled = locked
        --石之灵只有开关，无需Apply和Remove
    elseif self.m_PreviewType == EnumAppearancePreviewLuminousType.eSkillAppearance then
        local appearanceData = self:GetCurrentSelectedSkillAppearData()
        icon = appearanceData.icon
        name = appearanceData.name
        desc = appearanceData.condition
        if CClientMainPlayer.Inst then
            local exist = LuaAppearancePreviewMgr:MainPlayerHasSkillAppear(appearanceData.id)
            local inUse = LuaAppearancePreviewMgr:IsCurrentInUseSkillAppear(appearanceData.id)
            disabled = not exist
            locked = not exist
            if exist and not inUse then
                table.insert(buttonTbl, {text=LocalString.GetString("更换"), isYellow=false, action=function(go) self:OnApplyButtonClick() end})
            elseif exist and inUse then
                table.insert(buttonTbl, {text=LocalString.GetString("脱下"), isYellow=false, action=function(go) self:OnRemoveButtonClick() end})
            end
        end
    end
    self.m_ItemDisplay:Init(icon, name, desc, "", false, disabled, locked, buttonTbl)
end

function LuaAppearanceLuminousSubview:GetCurrentSelectedSuitData()
    local id = self.m_SelectedDataId and self.m_SelectedDataId or 0
    if id>0 then
        local appearanceData = nil
        for i=1,#self.m_AllIntensifySuitAppearances do
            if self.m_AllIntensifySuitAppearances[i].fxId == self.m_SelectedDataId then
                return self.m_AllIntensifySuitAppearances[i]
            end
        end
    end
    return nil
end

function LuaAppearanceLuminousSubview:GetCurrentSelectedSkillAppearData()
    local id = self.m_SelectedDataId and self.m_SelectedDataId or 0
    if id>0 then
        local appearanceData = nil
        for i=1,#self.m_AllSkillAppearances do
            if self.m_AllSkillAppearances[i].id == self.m_SelectedDataId then
                return self.m_AllSkillAppearances[i]
            end
        end
    end
    return nil
end

function LuaAppearanceLuminousSubview:GetCurrentSelectedGemGroupData()
    local id = self.m_SelectedDataId and self.m_SelectedDataId or 0
    if id>0 then
        local appearanceData = nil
        for i=1,#self.m_AllGemGroupAppearances do
            if self.m_AllGemGroupAppearances[i].id == self.m_SelectedDataId then
                return self.m_AllGemGroupAppearances[i]
            end
        end
    end
    return nil
end

function LuaAppearanceLuminousSubview:OnApplyButtonClick()
    if self.m_PreviewType == EnumAppearancePreviewLuminousType.eIntensifySuit then
        LuaAppearancePreviewMgr:RequestSetIntensitySuit(self.m_SelectedDataId)
    elseif self.m_PreviewType == EnumAppearancePreviewLuminousType.eGemGroup then
        --不应该进入该分支
    elseif self.m_PreviewType == EnumAppearancePreviewLuminousType.eSkillAppearance then
        LuaAppearancePreviewMgr:RequestSetSkillAppear(self.m_SelectedDataId)
    end
end

function LuaAppearanceLuminousSubview:OnRemoveButtonClick()
    if self.m_PreviewType == EnumAppearancePreviewLuminousType.eIntensifySuit then
        LuaAppearancePreviewMgr:RequestSetIntensitySuit(0)
    elseif self.m_PreviewType == EnumAppearancePreviewLuminousType.eSkillAppearance then
        LuaAppearancePreviewMgr:RequestSetSkillAppear(0)
    end
end

function LuaAppearanceLuminousSubview:OnSwitchButtonClick()
    self.m_SwitchButton.Selected = not self.m_SwitchButton.Selected
    if self.m_PreviewType == EnumAppearancePreviewLuminousType.eIntensifySuit then
        local bShow = LuaAppearancePreviewMgr:IsShowIntensitySuit()
        LuaAppearancePreviewMgr:ChangeIntensitySuitVisibility(not bShow)
    elseif self.m_PreviewType == EnumAppearancePreviewLuminousType.eGemGroup then
        local bShow = LuaAppearancePreviewMgr:IsShowGemGroup()
        LuaAppearancePreviewMgr:ChangeGemGroupVisibility(not bShow)
    elseif self.m_PreviewType == EnumAppearancePreviewLuminousType.eSkillAppearance then
        local bShow = LuaAppearancePreviewMgr:IsShowSkillAppear()
        LuaAppearancePreviewMgr:ChangeSkillAppearVisibility(not bShow)
    end
end

function  LuaAppearanceLuminousSubview:OnEnable()
    g_ScriptEvent:AddListener("EquipmentSuitUpdate",self,"FxUpdate")
    g_ScriptEvent:AddListener("EquipmentGemGroupUpdate",self,"GemgroupFxUpdate")
	g_ScriptEvent:AddListener("OnSyncMainPlayerAppearanceProp",self,"SyncMainPlayerAppearancePropUpdate")
    g_ScriptEvent:AddListener("AppearancePropertySettingInfoReturn",self,"OnAppearancePropertySettingInfoReturn") --开关切换
    g_ScriptEvent:AddListener("SetSkillAppearanceSuccess",self,"OnSetSkillAppearanceSuccess")
end

function  LuaAppearanceLuminousSubview:OnDisable()
    g_ScriptEvent:RemoveListener("EquipmentSuitUpdate",self,"FxUpdate")
    g_ScriptEvent:RemoveListener("EquipmentGemGroupUpdate",self,"GemgroupFxUpdate")
    g_ScriptEvent:RemoveListener("OnSyncMainPlayerAppearanceProp",self,"SyncMainPlayerAppearancePropUpdate")
    g_ScriptEvent:RemoveListener("AppearancePropertySettingInfoReturn",self,"OnAppearancePropertySettingInfoReturn")
    g_ScriptEvent:RemoveListener("SetSkillAppearanceSuccess",self,"OnSetSkillAppearanceSuccess")
end


function LuaAppearanceLuminousSubview:FxUpdate(args)
    local obj = args[0]
    if obj and TypeIs(obj, typeof(CClientMainPlayer)) then
        self:SetDefaultSelection()
		self:LoadData()
	end
end

function LuaAppearanceLuminousSubview:GemgroupFxUpdate(args)
    local obj = args[0]
    if obj and TypeIs(obj, typeof(CClientMainPlayer)) then
        self:SetDefaultSelection()
        self:LoadData()
    end
end

function LuaAppearanceLuminousSubview:SyncMainPlayerAppearancePropUpdate()
    self:InitSelection()
end

function LuaAppearanceLuminousSubview:OnAppearancePropertySettingInfoReturn(args)
    self:SetDefaultSelection()
    self:InitSelection()
end

function LuaAppearanceLuminousSubview:OnSetSkillAppearanceSuccess(appearId, expiredTime)
    self:SetDefaultSelection()
    self:LoadData()
end