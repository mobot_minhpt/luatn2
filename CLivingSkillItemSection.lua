-- Auto Generated!!
local CLivingItem = import "L10.UI.CLivingItem"
local CLivingSkillItemSection = import "L10.UI.CLivingSkillItemSection"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local UIBasicSprite = import "UIBasicSprite"
local UIEventListener = import "UIEventListener"
local UITable = import "UITable"
local Vector3 = import "UnityEngine.Vector3"
CLivingSkillItemSection.m_Awake_CS2LuaHook = function (this) 
    UIEventListener.Get(this.headerBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        if this.mSectionData.expandad then
            this:Shrink()
            this.mSectionData.expandad = false
        else
            this:Expand()
            this.mSectionData.expandad = true
        end
    end)
end
CLivingSkillItemSection.m_Recycle_CS2LuaHook = function (this) 
    for i = this.grid.transform.childCount - 1, 0, - 1 do
        this.pool:Recycle(this.grid.transform:GetChild(i).gameObject)
    end
end
CLivingSkillItemSection.m_Init_CS2LuaHook = function (this, section) 

    this.mSectionData = section
    if not section.expandad then
        this.mSectionData.expandad = false
        this:Shrink()
    else
        this.mSectionData.expandad = true
        this:Expand()
    end
end
CLivingSkillItemSection.m_Shrink_CS2LuaHook = function (this) 
    this.titleLabel.text = this.mSectionData.name
    -- +" +";
    this.expandSprite.flip = UIBasicSprite.Flip.Horizontally
    for i = this.grid.transform.childCount, 1, - 1 do
        this.pool:Recycle(this.grid.transform:GetChild(i - 1).gameObject)
    end
    this:UpdateScrollView()
end
CLivingSkillItemSection.m_UpdateScrollView_CS2LuaHook = function (this) 
    local cmp = CommonDefs.NGUI_FindInParents(this.gameObject, typeof(UITable))
    if cmp ~= nil then
        cmp:Reposition()
    end
end
CLivingSkillItemSection.m_Expand_CS2LuaHook = function (this) 
    this.titleLabel.text = this.mSectionData.name
    -- +" -";
    this.expandSprite.flip = UIBasicSprite.Flip.Vertically
    do
        local i = 0
        while i < this.mSectionData.itemIds.Count do
            --foreach (var item in section.itemIds)
            --{
            local item = this.mSectionData.itemIds[i]
            local go = this.pool:GetFromPool(1)
            --GameObject go = GameObject.Instantiate(itemPrefab) as GameObject;
            --go.name = "Item" + i;
            go.transform.parent = this.grid.transform
            go.transform.localScale = Vector3.one
            go:SetActive(true)

            local cmp = CommonDefs.GetComponentInChildren_GameObject_Type(go, typeof(CLivingItem))
            local data = this.mSectionData:GetItem(item)
            cmp.callback = this.callback
            cmp:Init(data)
            cmp:RefreshMaskState()
            i = i + 1
        end
    end
    this.grid:Reposition()
    this:TrySelectLastItem()
    this:UpdateScrollView()
end
CLivingSkillItemSection.m_TrySelectLastItem_CS2LuaHook = function (this) 
    do
        local i = 0
        while i < this.grid.transform.childCount do
            local cmp = CommonDefs.GetComponent_Component_Type(this.grid.transform:GetChild(i), typeof(CLivingItem))
            if cmp:TrySelect() then
                break
            end
            i = i + 1
        end
    end
end
CLivingSkillItemSection.m_SelectFirstItem_CS2LuaHook = function (this) 
    if this.grid.transform.childCount > 0 then
        local tf = this.grid.transform:GetChild(0)
        local cmp = CommonDefs.GetComponent_Component_Type(tf, typeof(CLivingItem))
        cmp:Select()
    end
end
CLivingSkillItemSection.m_SelectItem_CS2LuaHook = function (this, index) 
    if not this.mSectionData.expandad then
        this:Expand()
        this.mSectionData.expandad = true
    end
    if this.grid.transform.childCount <= index then
        index = 0
    end
    if this.grid.transform.childCount > index then
        local tf = this.grid.transform:GetChild(index)
        if tf ~= nil then
            local cmp = CommonDefs.GetComponent_Component_Type(tf, typeof(CLivingItem))
            cmp:Select()
        end
    end
end
