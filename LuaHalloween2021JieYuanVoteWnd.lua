local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CUIFx = import "L10.UI.CUIFx"
local CUITexture = import "L10.UI.CUITexture"
local UITabBar = import "L10.UI.UITabBar"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local PathMode = import "DG.Tweening.PathMode"
local PathType = import "DG.Tweening.PathType"
local Ease = import "DG.Tweening.Ease"
local NGUIMath = import "NGUIMath"
local Vector4 = import "UnityEngine.Vector4"
local CButton = import "L10.UI.CButton"
local BoxCollider = import "UnityEngine.BoxCollider"

LuaHalloween2021JieYuanVoteWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaHalloween2021JieYuanVoteWnd, "CountDownLabel", "CountDownLabel", UILabel)
RegistChildComponent(LuaHalloween2021JieYuanVoteWnd, "OkButton", "OkButton", CButton)
RegistChildComponent(LuaHalloween2021JieYuanVoteWnd, "VoteReadMeLabel", "VoteReadMeLabel", UILabel)
RegistChildComponent(LuaHalloween2021JieYuanVoteWnd, "RewardButton", "RewardButton", GameObject)
RegistChildComponent(LuaHalloween2021JieYuanVoteWnd, "RewardLabel", "RewardLabel", UILabel)
RegistChildComponent(LuaHalloween2021JieYuanVoteWnd, "RewardFx", "RewardFx", CUIFx)
RegistChildComponent(LuaHalloween2021JieYuanVoteWnd, "SpyResultView", "SpyResultView", GameObject)
RegistChildComponent(LuaHalloween2021JieYuanVoteWnd, "SpyPortrait", "SpyPortrait", CUITexture)
RegistChildComponent(LuaHalloween2021JieYuanVoteWnd, "SpyNameLabel", "SpyNameLabel", UILabel)
RegistChildComponent(LuaHalloween2021JieYuanVoteWnd, "Players", "Players", UITabBar)
RegistChildComponent(LuaHalloween2021JieYuanVoteWnd, "SpyResultFx", "SpyResultFx", CUIFx)

--@endregion RegistChildComponent end
RegistClassMember(LuaHalloween2021JieYuanVoteWnd, "m_CountDownTick")
RegistClassMember(LuaHalloween2021JieYuanVoteWnd, "m_CountDownTime")
RegistClassMember(LuaHalloween2021JieYuanVoteWnd, "m_IsEndCountDown")
RegistClassMember(LuaHalloween2021JieYuanVoteWnd, "m_CanGetReward")
RegistClassMember(LuaHalloween2021JieYuanVoteWnd, "m_TabIndex")

function LuaHalloween2021JieYuanVoteWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.OkButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOkButtonClick()
	end)


	
	UIEventListener.Get(self.RewardButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRewardButtonClick()
	end)


    --@endregion EventBind end
end

function LuaHalloween2021JieYuanVoteWnd:Init()
	self.OkButton.Enabled = false
	self.m_IsEndCountDown = LuaHalloween2021Mgr.jieYuanVoteEnd
	self.m_CanGetReward = LuaHalloween2021Mgr.jieyuanCanGetAward
	self.m_CountDownTime = LuaHalloween2021Mgr.jieYuanVoteExpireTime - CServerTimeMgr.Inst.timeStamp
	self.Players.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
	    self:OnTabsTabChange(index)
	end)
	self:UpdateCountDownLabel()
	self:CancelCountDownTick()
	self.m_CountDownTick = RegisterTick(function ()
		self.m_CountDownTime = LuaHalloween2021Mgr.jieYuanVoteExpireTime - CServerTimeMgr.Inst.timeStamp
		self:UpdateCountDownLabel()
		if self.m_CountDownTime < 0 or self.m_IsEndCountDown then
			self:CancelCountDownTick()
		end
	end,500)
	self.VoteReadMeLabel.text = g_MessageMgr:FormatMessage(LuaHalloween2021Mgr.isSpy and "Halloween2021_JieYuanVoteWnd_Spy_ReadMe" or "Halloween2021_JieYuanVoteWnd_ReadMe")
	self:UpdateState()
	self:OnJieLiangYuanSyncVoteNeiGuiInfo()
end

function LuaHalloween2021JieYuanVoteWnd:CancelCountDownTick()
	if self.m_CountDownTick then
		UnRegisterTick(self.m_CountDownTick)
		self.m_CountDownTick = nil
	end
end

function LuaHalloween2021JieYuanVoteWnd:OnEnable()
	g_ScriptEvent:AddListener("OnJieLiangYuanSyncVoteNeiGuiInfo", self, "OnJieLiangYuanSyncVoteNeiGuiInfo")
end

function LuaHalloween2021JieYuanVoteWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnJieLiangYuanSyncVoteNeiGuiInfo", self, "OnJieLiangYuanSyncVoteNeiGuiInfo")
	self:CancelCountDownTick()
	LuaHalloween2021Mgr.voteResult = 0
	if LuaHalloween2021Mgr.jieyuanCanGetAward and LuaHalloween2021Mgr.jieYuanVoteEnd then
		Gac2Gas.JieLiangYuanRequestGetAward(true)
	end
end

function LuaHalloween2021JieYuanVoteWnd:OnJieLiangYuanSyncVoteNeiGuiInfo()
	self.m_IsEndCountDown = LuaHalloween2021Mgr.jieYuanVoteEnd
	self.m_CanGetReward = LuaHalloween2021Mgr.jieyuanCanGetAward
	for index,data in pairs(LuaHalloween2021Mgr.jieYuanVoteInfo) do
		if LuaHalloween2021Mgr.jieYuanVoteEnd and data.playerId == LuaHalloween2021Mgr.neiGuiPlayerId then
			local name,class,gender = data.playerName,data.playerClass,data.playerGender
			self:OnSpyResult(class, gender, name)
		end
	end
	if self.m_IsEndCountDown then
		self:HideCheckMarks()
		if LuaHalloween2021Mgr:IsSpyRunAway() then
			self.VoteReadMeLabel.text = g_MessageMgr:FormatMessage((LuaHalloween2021Mgr.voteResult == EnumPlayResult.eDraw) 
			and "Halloween2021_JieYuan_DrawResult"
			or "Halloween2021_JieYuan_Spy_RunAway")
			self.SpyResultFx:LoadFx("fx/ui/prefab/UI_dianlianggezi_01.prefab")
		end
	end
	self:InitPlayers(LuaHalloween2021Mgr.jieYuanVoteInfo)
	self:UpdateState()
end

function LuaHalloween2021JieYuanVoteWnd:HideCheckMarks()
	for i = 0,self.Players.transform.childCount - 1 do
		local child = self.Players.transform:GetChild(i)
		child:GetComponent(typeof(BoxCollider)).enabled = false
		child:Find("QnCheckBox/Background").gameObject:SetActive(false)
	end
end

function LuaHalloween2021JieYuanVoteWnd:UpdateState()
	self:UpdateRewardLabel()
	self.m_IsEndCountDown = LuaHalloween2021Mgr.jieYuanVoteEnd
	self.m_CanGetReward = LuaHalloween2021Mgr.jieyuanCanGetAward
	local isSpyRunAway = LuaHalloween2021Mgr:IsSpyRunAway()
	self.VoteReadMeLabel.gameObject:SetActive((not self.m_IsEndCountDown) or isSpyRunAway)
	self.SpyResultView.gameObject:SetActive(self.m_IsEndCountDown and not isSpyRunAway)
	self.RewardFx.gameObject:SetActive(self.m_CanGetReward)
	self.CountDownLabel.gameObject:SetActive(not self.m_IsEndCountDown)
	self.OkButton.gameObject:SetActive(not self.m_IsEndCountDown)
end

function LuaHalloween2021JieYuanVoteWnd:OnSpyResult(class, gender, name)
	self.SpyPortrait:LoadPortrait(CUICommonDef.GetPortraitName(class, gender, -1),true)
	self.m_IsEndCountDown = true
	self.SpyResultFx:LoadFx("fx/ui/prefab/UI_xiaozhu_huodejiangli.prefab")
	self.RewardFx:LoadFx("fx/ui/prefab/UI_kuang_yellow02.prefab")
	self.RewardFx.transform.localPosition=Vector3(0,0,0)
	local b = NGUIMath.CalculateRelativeWidgetBounds(self.RewardFx.transform)
	local waypoints = CUICommonDef.GetWayPoints(Vector4(b.min.x, b.max.y, b.size.x, b.size.y), 1, true)
	self.RewardFx.transform.localPosition = waypoints[0]
	LuaTweenUtils.DOLuaLocalPath(self.RewardFx.transform, waypoints, 2, PathType.Linear, PathMode.Ignore, 2, - 1, Ease.Linear)
	self.SpyNameLabel.text = SafeStringFormat3(LocalString.GetString("[%s]"), name)
	self:UpdateState()
end

function LuaHalloween2021JieYuanVoteWnd:InitPlayers(data)
	for i = 1, 5 do
		local obj = self.Players.transform:GetChild(i - 1).gameObject
		obj.gameObject:SetActive(i <= #data)
		if i <= #data then
			local t = data[i]
			local name,class,gender,voteNum,playerId = t.playerName,t.playerClass,t.playerGender,t.votedNum,t.playerId
			local isSpy = false
			obj.transform:Find("SpySprite").gameObject:SetActive(playerId == LuaHalloween2021Mgr.neiGuiPlayerId)
			obj.transform:Find("Portrait"):GetComponent(typeof(CUITexture)):LoadPortrait(CUICommonDef.GetPortraitName(class, gender, -1),true)
			obj.transform:Find("NameLabel"):GetComponent(typeof(UILabel)).text = name
			obj.transform:Find("QnCheckBox/VoteNumLabel"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("%s票"), voteNum and voteNum or 0)
		end
	end
end

function LuaHalloween2021JieYuanVoteWnd:UpdateCountDownLabel()
	self.CountDownLabel.text = SafeStringFormat3(LocalString.GetString("投票倒计时%d秒"), self.m_CountDownTime < 0 and 0 or self.m_CountDownTime)
end

function LuaHalloween2021JieYuanVoteWnd:UpdateRewardLabel()
	self.RewardLabel.text = SafeStringFormat3("%s%s",self.m_CanGetReward and LocalString.GetString("领取") or "",
		LuaHalloween2021Mgr.isSpy and LocalString.GetString("狐爹爹的封口费") or LocalString.GetString("新郎的谢礼"))
end

function LuaHalloween2021JieYuanVoteWnd:OnTabsTabChange(index)
	self.m_TabIndex = index
	if #LuaHalloween2021Mgr.jieYuanVoteInfo < self.m_TabIndex then return end
	local data = LuaHalloween2021Mgr.jieYuanVoteInfo[self.m_TabIndex + 1]
	if not data then return end
	self.OkButton.Enabled = true
	Gac2Gas.JieLiangYuanRequestVoteNeiGui(data.playerId,false)
end

--@region UIEvent

function LuaHalloween2021JieYuanVoteWnd:OnOkButtonClick()
	if not self.m_TabIndex and #LuaHalloween2021Mgr.jieYuanVoteInfo < self.m_TabIndex then return end
	local data = LuaHalloween2021Mgr.jieYuanVoteInfo[self.m_TabIndex + 1]
	if not data then return end
	Gac2Gas.JieLiangYuanRequestVoteNeiGui(data.playerId,true)
	for i = 0,self.Players.transform.childCount - 1 do
		local child = self.Players.transform:GetChild(i)
		child:GetComponent(typeof(BoxCollider)).enabled = false
		child:Find("QnCheckBox/Background"):GetComponent(typeof(UISprite)).enabled = false
	end
	self.OkButton.Enabled = false
end

function LuaHalloween2021JieYuanVoteWnd:OnRewardButtonClick()
	Gac2Gas.JieLiangYuanRequestGetAward(false)
end

--@endregion UIEvent

