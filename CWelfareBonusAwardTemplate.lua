-- Auto Generated!!
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local CWelfareBonusAwardTemplate = import "L10.UI.CWelfareBonusAwardTemplate"
CWelfareBonusAwardTemplate.m_Init_CS2LuaHook = function (this, item, amount, loadFx, scrollView) 

    this.award = item
    this.amountLabel.text = amount == "1" and "" or amount
    this.iconTexture:LoadMaterial(item.Icon)
    this.qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(item, nil, false)
    if loadFx then
        this:LoadFx()
    end
    this.fx.ScrollView = scrollView
end
CWelfareBonusAwardTemplate.m_LoadFx_CS2LuaHook = function (this) 
    this.fx:LoadFx(CUIFxPaths.ItemRemindYellowFxPath)
end
