
local DefaultTableViewDataSource    = import "L10.UI.DefaultTableViewDataSource"
local QnTableView                   = import "L10.UI.QnTableView"
local Vector3                       = import "UnityEngine.Vector3"

LuaGuildVoteWnd = class()

--------RegistChildComponent-------
RegistChildComponent(LuaGuildVoteWnd,         "AdvView",         QnTableView)
RegistChildComponent(LuaGuildVoteWnd,         "StartBtn",        GameObject)
RegistChildComponent(LuaGuildVoteWnd,         "HintLabel",       GameObject)

function LuaGuildVoteWnd:Init()
    Gac2Gas.RequestYuanDanGuildVoteInfo()
    UIEventListener.Get(self.StartBtn).onClick = DelegateFactory.VoidDelegate(function(p)
        if #luaGuildVoteMgr.VoteData >= 15 then
            g_MessageMgr:ShowMessage("YuanDan2020_GUILD_VOTE_TOO_MANY_QUESTIONS")
            return
        end
        if luaGuildVoteMgr.Limit then
            CUIManager.ShowUI(CLuaUIResources.GuildVoteEditWnd)
        else
            g_MessageMgr:ShowMessage("Guild_Vote_Limited_Authority")
        end
    end)
end

function LuaGuildVoteWnd:OnEnable()
    g_ScriptEvent:AddListener("SendYuanDanGuildVoteInfo", self, "SendYuanDanGuildVoteInfo")
    g_ScriptEvent:AddListener("SendYuanDanGuildVoteCreateResult", self, "SendYuanDanGuildVoteCreateResult")    
end

function LuaGuildVoteWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendYuanDanGuildVoteInfo", self, "SendYuanDanGuildVoteInfo")
    g_ScriptEvent:RemoveListener("SendYuanDanGuildVoteCreateResult", self, "SendYuanDanGuildVoteCreateResult")
    luaGuildVoteMgr:ClearVoteData()
end

function LuaGuildVoteWnd:SendYuanDanGuildVoteInfo()
    self.HintLabel:SetActive(#luaGuildVoteMgr.VoteData == 0)
    self:InitAdvView()
end

function LuaGuildVoteWnd:SendYuanDanGuildVoteCreateResult()
    self.HintLabel:SetActive(#luaGuildVoteMgr.VoteData == 0)
    self.AdvView:Clear()
    self.AdvView:ReloadData(true,false)
end

function LuaGuildVoteWnd:InitAdvView()
    self.AdvView.m_DataSource = DefaultTableViewDataSource.Create(function() 
        return #luaGuildVoteMgr.VoteData
    end, function(item, index)
        self:InitItem(item, index)
    end)

    self.AdvView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
        Gac2Gas.RequestYuanDanGuildVoteQuestion(luaGuildVoteMgr.VoteData[row+1].questionId)
    end)
    self.AdvView:ReloadData(true,false)
end

function LuaGuildVoteWnd:InitItem(item,index)
    item.name = luaGuildVoteMgr.VoteData[index+1].questionId
    local RandomAngle = math.random(0,10) - 5
    local TransRoot = item.transform:Find("TransRoot")
    TransRoot.eulerAngles = Vector3(0,0,RandomAngle)
    local VoteContent = item.transform:Find("TransRoot/VoteContent"):GetComponent(typeof(UILabel))
    local VoteTitle = item.transform:Find("TransRoot/VoteTitle"):GetComponent(typeof(UILabel))
    local IsFinish = item.transform:Find("TransRoot/IsFinish").gameObject
    VoteContent.text = luaGuildVoteMgr.VoteData[index+1].questionContent
    VoteTitle.text = luaGuildVoteMgr.VoteData[index+1].title
    IsFinish:SetActive(luaGuildVoteMgr.VoteData[index+1].isFinish)
end