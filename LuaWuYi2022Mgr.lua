LuaWuYi2022Mgr = {}

LuaWuYi2022Mgr.m_YouSanJieTaskStateList = {}
function LuaWuYi2022Mgr:QueryWuYiManYouSanJieTaskInfoResult(taskInfo_U)
    local list = MsgPackImpl.unpack(taskInfo_U)
    local manYouSanJieTask = WuYi_Setting.GetData().ManYouSanJieTask
    self.m_YouSanJieTaskStateList = {}
    -- for i = 0,manYouSanJieTask.Length - 1 do
    --     local taskId = tonumber(manYouSanJieTask[i][0]) 
    --     self.m_YouSanJieTaskStateList[taskId] = {}
    -- end
    for i = 0,list.Count - 1 do
        for j = 0,list[i].Count - 1,3 do
            local taskId, bHave, bFinished = list[i][j], list[i][j + 1], list[i][j + 2]
            self.m_YouSanJieTaskStateList[taskId] = { bHave = bHave, bFinished = bFinished }
        end
    end
    g_ScriptEvent:BroadcastInLua("QueryWuYiManYouSanJieTaskInfoResult")
end

function LuaWuYi2022Mgr:ShowTaskView()
    local msg = self.m_TaskState == 1 and g_MessageMgr:FormatMessage("WuYi2022_DaGongHunTaskView1",10 - self.m_JiTanNum,"%s") 
        or g_MessageMgr:FormatMessage("WuYi2022_DaGongHunTaskView2",self.m_BossStagePlayResult,"%s") 
    g_ScriptEvent:BroadcastInLua("OnCommonCountDownViewUpdate",msg, true, false, nil, nil, nil, nil)
end

LuaWuYi2022Mgr.m_BossStagePlayResult = 0
function LuaWuYi2022Mgr:EnterDaGongHunBossStage(playResult)
    print(playResult)
    self.m_TaskState = 2
    self.m_BossStagePlayResult = playResult == EnumPlayResult.eLose and 0 or 1
    g_ScriptEvent:BroadcastInLua("OnEnterDaGongHunBossStage")
    self:ShowTaskView()
end

LuaWuYi2022Mgr.m_JiTanNum = 10
LuaWuYi2022Mgr.m_TaskState = 1
function LuaWuYi2022Mgr:SyncDaGongHunJiTanNum(jiTanNum)
    self.m_JiTanNum = jiTanNum
    self.m_TaskState = 1
    print(jiTanNum)
    g_ScriptEvent:BroadcastInLua("OnSyncDaGongHunJiTanNum")
    self:ShowTaskView()
end

LuaWuYi2022Mgr.m_CurButtonEngineId = 0