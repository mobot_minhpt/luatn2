local UIRoot = import "UIRoot"
local Screen = import "UnityEngine.Screen"
local Constants = import "L10.Game.Constants"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaMusicBoxPopControlWnd = class()

RegistChildComponent(LuaMusicBoxPopControlWnd, "CommonView", "CommonView", GameObject)
RegistChildComponent(LuaMusicBoxPopControlWnd, "UnfoldView", "UnfoldView", GameObject)
RegistChildComponent(LuaMusicBoxPopControlWnd, "CommonViewProgressBar", "CommonViewProgressBar", UISlider)
RegistChildComponent(LuaMusicBoxPopControlWnd, "CommonViewFx", "CommonViewFx", CUIFx)
RegistChildComponent(LuaMusicBoxPopControlWnd, "CommonViewFx2", "CommonViewFx2",  CUIFx)
RegistChildComponent(LuaMusicBoxPopControlWnd, "UnfoldViewProgressBar", "UnfoldViewProgressBar",UISlider)
RegistChildComponent(LuaMusicBoxPopControlWnd, "UnfoldViewFx", "UnfoldViewFx",  CUIFx)
RegistChildComponent(LuaMusicBoxPopControlWnd, "MusicStateSprite", "MusicStateSprite", UISprite)
RegistChildComponent(LuaMusicBoxPopControlWnd, "PlayingMusicNameLabel", "PlayingMusicNameLabel", UILabel)
RegistChildComponent(LuaMusicBoxPopControlWnd, "ChangeToLastMusicBtn", "ChangeToLastMusicBtn", GameObject)
RegistChildComponent(LuaMusicBoxPopControlWnd, "ChangeToNextMusicBtn", "ChangeToNextMusicBtn", GameObject)
RegistChildComponent(LuaMusicBoxPopControlWnd, "OpenMusicListBtn", "OpenMusicListBtn", GameObject)
RegistChildComponent(LuaMusicBoxPopControlWnd, "MusicNotes", "MusicNotes", GameObject)
RegistChildComponent(LuaMusicBoxPopControlWnd, "UnfoldViewBox", "UnfoldViewBox", UIWidget)
RegistChildComponent(LuaMusicBoxPopControlWnd, "CloseButton", "CloseButton", GameObject)

RegistClassMember(LuaMusicBoxPopControlWnd, "m_IsUnfold") 
RegistClassMember(LuaMusicBoxPopControlWnd, "m_RebackCommonViewTick") 
RegistClassMember(LuaMusicBoxPopControlWnd, "m_Panel") 
RegistClassMember(LuaMusicBoxPopControlWnd, "m_CommonViewHalfWith")
RegistClassMember(LuaMusicBoxPopControlWnd, "m_CommonViewHalfHeight")
RegistClassMember(LuaMusicBoxPopControlWnd, "m_UnfoldViewHalfWith")
RegistClassMember(LuaMusicBoxPopControlWnd, "m_IsPlaying") 
RegistClassMember(LuaMusicBoxPopControlWnd, "m_UpdateProgressTick") 

function LuaMusicBoxPopControlWnd:Awake()
	UIEventListener.Get(self.ChangeToLastMusicBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnChangeToLastMusicBtnClick()
	end)
	UIEventListener.Get(self.ChangeToNextMusicBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnChangeToNextMusicBtnClick()
	end)
	UIEventListener.Get(self.OpenMusicListBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOpenMusicListBtnClick()
    end)
    UIEventListener.Get(self.CommonView).onClick = DelegateFactory.VoidDelegate(function(p)
        self:OnCommonViewClick()
    end)
    UIEventListener.Get(self.CommonView).onDrag = DelegateFactory.VectorDelegate(function(go,delta)
        self:OnCommonViewDrag(go,delta)
    end)
    UIEventListener.Get(self.CommonView).onDragEnd = DelegateFactory.VoidDelegate(function(go)
        self:OnCommonViewDragEnd()
    end)
    UIEventListener.Get(self.MusicStateSprite.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnMusicStateSpriteClick()
    end)
    UIEventListener.Get(self.CloseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnCloseButtonClick()
    end)
    self.PlayingMusicNameLabel.text = LocalString.GetString("暂无音乐")
    self.m_Panel = self.gameObject:GetComponent(typeof(UIPanel))
    local commonViewWidget = self.CommonView:GetComponent(typeof(UIWidget))
    self.m_CommonViewHalfWith = commonViewWidget.width / 2
    self.m_CommonViewHalfHeight = commonViewWidget.height / 2
    self.m_UnfoldViewHalfWith = self.UnfoldViewBox.width / 2
    self.m_IsPlaying = false
    self.CommonViewProgressBar.value = 0
    self.UnfoldViewProgressBar.value = 0
    local musicData, musicIndex = CLuaMusicBoxMgr:GetCurMusicDataAndIndex()
    local now = CServerTimeMgr.Inst.timeStamp
    if now < musicData.ExpireTime then
        CLuaMusicBoxMgr:PlayMusicBox(musicData, false)
    end
end

function LuaMusicBoxPopControlWnd:Init()
    CLuaMusicBoxMgr:EnableMusicBox(false)
    self.CommonViewFx:LoadFx("fx/ui/prefab/UI_yinyuehe_yinfupiao.prefab")
    self.CommonViewFx2:LoadFx("fx/ui/prefab/UI_yinyuehe_yinfuxuanzhuan.prefab")
    self.UnfoldViewFx:LoadFx("fx/ui/prefab/UI_yinyuehe_yinfupiao.prefab")
    self.CommonViewFx.gameObject:SetActive(false)
    self.CommonViewFx2.gameObject:SetActive(false)
    self.UnfoldViewFx.gameObject:SetActive(false)
    self:ReInitView(false)
end

function LuaMusicBoxPopControlWnd:OnEnable()
    g_ScriptEvent:AddListener("OnSendMusicBoxState", self, "OnSendMusicBoxState")
    CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.MusicBoxPopControlWnd)
end

function LuaMusicBoxPopControlWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnSendMusicBoxState", self, "OnSendMusicBoxState")
    self:CancelRebackCommonViewTick()
    LuaTweenUtils.DOKill(self.UnfoldView.transform, false)
    LuaTweenUtils.DOKill(self.CommonViewProgressBar.transform, false)
    if self.m_UpdateProgressTick then
        UnRegisterTick(self.m_UpdateProgressTick)
        self.m_UpdateProgressTick = nil
    end
    CLuaMusicBoxMgr:StopMusicBox(true)
end

function LuaMusicBoxPopControlWnd:OnSendMusicBoxState(currentSong, currentState, progress)
    local isExpire = false
    local now = CServerTimeMgr.Inst.timeStamp
    for _, t in pairs(CLuaMusicBoxMgr.m_MusicBoxList) do
        if currentSong == t.BuffID then
            if now >= t.ExpireTime then
                isExpire = true
            end
        end
    end
    self.m_IsPlaying = CLuaMusicBoxMgr.m_CurPlayMusicState and CLuaMusicBoxMgr.m_CurPlayMusicState == EnumMusicBoxState.Playing and not isExpire and currentSong ~= 0
    self.MusicStateSprite.spriteName = self.m_IsPlaying and "socialwnd_pause" or "socialwnd_play"
    self.CommonViewFx.gameObject:SetActive(self.m_IsPlaying)
    self.CommonViewFx2.gameObject:SetActive(self.m_IsPlaying)
    self.UnfoldViewFx.gameObject:SetActive(self.m_IsPlaying)
    local data = Buff_MusicBuff.GetData(currentSong)
    if data then
        self.CommonViewProgressBar.value = progress / data.SoundTime
        self.UnfoldViewProgressBar.value = progress / data.SoundTime
        self.PlayingMusicNameLabel.text = data.Name
        LuaTweenUtils.DOKill(self.CommonViewProgressBar.transform, false)
        if self.m_IsPlaying then
            local tween = LuaTweenUtils.TweenFloat(progress,data.SoundTime,(data.SoundTime - progress), function ( val )
                self.CommonViewProgressBar.value = val / data.SoundTime
                self.UnfoldViewProgressBar.value = val / data.SoundTime
                if val >= (data.SoundTime - 2) then
                    Gac2Gas.RequestMusicBoxState()
                end
            end)
            LuaTweenUtils.SetTarget(tween, self.CommonViewProgressBar.transform)
        end
    end
end
--@region UIEvent

function LuaMusicBoxPopControlWnd:OnChangeToLastMusicBtnClick()
    local musicData, musicIndex = CLuaMusicBoxMgr:GetCurMusicDataAndIndex()
    CLuaMusicBoxMgr:SwitchMusic(musicIndex - 1)
    self:ReInitView(false)
end

function LuaMusicBoxPopControlWnd:OnChangeToNextMusicBtnClick()
    local musicData, musicIndex = CLuaMusicBoxMgr:GetCurMusicDataAndIndex()
    CLuaMusicBoxMgr:SwitchMusic(musicIndex + 1)
    self:ReInitView(false)
end

function LuaMusicBoxPopControlWnd:OnOpenMusicListBtnClick()
    self:ReInitView(false)
    CUIManager.ShowUI(CLuaUIResources.MusicListWnd)
end

function LuaMusicBoxPopControlWnd:OnCommonViewClick()
    self:ReInitView(not self.m_IsUnfold)
end

function LuaMusicBoxPopControlWnd:OnCommonViewDrag(go,delta)
    local scale = UIRoot.GetPixelSizeAdjustment(self.transform.parent.gameObject);
    local virtualScreenWidth = Screen.width * scale * CUIManager.UIMainCamera.rect.width
    local virtualScreenHeight = Screen.height * scale * CUIManager.UIMainCamera.rect.height
    local halfVirtualScreenWidth = virtualScreenWidth / 2
    local halfVirtualScreenHeight = virtualScreenHeight / 2
    local trans = self.CommonView.transform
    local pos = trans.localPosition
    pos.x = pos.x + delta.x*scale
    pos.y = pos.y + delta.y*scale
    if ((pos.x + self.m_CommonViewHalfWith) < halfVirtualScreenWidth) and 
       ((pos.x - self.m_CommonViewHalfWith) > - halfVirtualScreenWidth) and 
       ((pos.y + self.m_CommonViewHalfHeight) < halfVirtualScreenHeight) and
       ((pos.y - self.m_CommonViewHalfHeight) > - halfVirtualScreenHeight) then
        trans.localPosition = pos
    end
    if self.m_IsUnfold then
        self:ReInitView(false)
    end
end

function LuaMusicBoxPopControlWnd:OnCommonViewDragEnd()   
end

function LuaMusicBoxPopControlWnd:OnMusicStateSpriteClick()
    if self.m_IsPlaying then
        CLuaMusicBoxMgr:StopMusicBox(true)
    else
        local musicData, musicIndex = CLuaMusicBoxMgr:GetCurMusicDataAndIndex()
        CLuaMusicBoxMgr:PlayMusicBox(musicData, false)
    end
    self:ReInitView(false)
end

function LuaMusicBoxPopControlWnd:OnCloseButtonClick()
    CUIManager.CloseUI(CLuaUIResources.MusicBoxPopControlWnd)
    if not L10.Game.Guide.CGuideMgr.Inst:IsInPhase(EnumGuideKey.FindMusicBox) then
        CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.FindMusicBox)
    end
end
--@endregion

function LuaMusicBoxPopControlWnd:CancelRebackCommonViewTick()
    if self.m_RebackCommonViewTick then
        UnRegisterTick(self.m_RebackCommonViewTick)
        self.m_RebackCommonViewTick = nil
    end
end

function LuaMusicBoxPopControlWnd:ReInitView(isUnfold)
    self.m_IsUnfold = isUnfold
    self:CancelRebackCommonViewTick()
    self.CommonView:SetActive(not self.m_IsUnfold)
    self.UnfoldView.transform.localPosition = self.CommonView.transform.localPosition
    self.UnfoldView:SetActive(self.m_IsUnfold)
    if self.m_IsUnfold then
        self.m_RebackCommonViewTick = RegisterTickOnce(function()
            if self.m_IsUnfold then
                self.m_IsUnfold = false
                self:ReInitView()
            end
        end,10000)
        local x = self.UnfoldView.transform.localPosition.x + self.UnfoldViewBox.transform.localPosition.x
        local scale = UIRoot.GetPixelSizeAdjustment(self.transform.parent.gameObject);
        local virtualScreenWidth = Screen.width * scale * CUIManager.UIMainCamera.rect.width
        if (x + self.m_UnfoldViewHalfWith) > virtualScreenWidth / 2 then
            local unfoldViewMaxX = virtualScreenWidth / 2 - self.UnfoldViewBox.transform.localPosition.x - self.m_UnfoldViewHalfWith
            local tween = LuaTweenUtils.DOLocalMoveX(self.UnfoldView.transform, unfoldViewMaxX, Constants.GeneralAnimationDuration, false)
            LuaTweenUtils.SetTarget(tween, self.UnfoldView.transform)
        end
    end
    local effect = CLuaMusicBoxMgr:GetMusicBoxEffect()
    self.MusicStateSprite.spriteName = effect and "socialwnd_pause" or "socialwnd_play"
end

function LuaMusicBoxPopControlWnd:GetGuideGo(methodName)
    if methodName=="GetCommonViewBtn" then
        return self.CommonView
    end
end

