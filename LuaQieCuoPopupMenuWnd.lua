local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"

LuaQieCuoPopupMenuWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaQieCuoPopupMenuWnd, "QieCuoButton", "QieCuoButton", GameObject)
RegistChildComponent(LuaQieCuoPopupMenuWnd, "JueDouButton", "JueDouButton", GameObject)
RegistChildComponent(LuaQieCuoPopupMenuWnd, "QieCuoDesLabel", "QieCuoDesLabel", UILabel)
RegistChildComponent(LuaQieCuoPopupMenuWnd, "JueDouDesLabel", "JueDouDesLabel", UILabel)

--@endregion RegistChildComponent end

function LuaQieCuoPopupMenuWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	UIEventListener.Get(self.QieCuoButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnQieCuoButtonClick()
	end)

	UIEventListener.Get(self.JueDouButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnJueDouButtonClick()
	end)

    --@endregion EventBind end
end

function LuaQieCuoPopupMenuWnd:Init()
	self.QieCuoDesLabel.text = g_MessageMgr:FormatMessage("QieCuo_ReadMe")
	self.JueDouDesLabel.text = g_MessageMgr:FormatMessage("JueDou_ReadMe")
end

--@region UIEvent

function LuaQieCuoPopupMenuWnd:OnQieCuoButtonClick()
	Gac2Gas.RequestStartQieCuo(LuaPlayerInfoMgr.m_QieCuoPlayerId)
	CUIManager.CloseUI(CLuaUIResources.QieCuoPopupMenuWnd)
end

function LuaQieCuoPopupMenuWnd:OnJueDouButtonClick()
	Gac2Gas.RequestJueDou(LuaPlayerInfoMgr.m_QieCuoPlayerId)
	CUIManager.CloseUI(CLuaUIResources.QieCuoPopupMenuWnd)
end

--@endregion UIEvent

