-- Auto Generated!!
local CButton = import "L10.UI.CButton"
local CChatMgr = import "L10.Game.CChatMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local CPlayerMainStoryMgr = import "L10.Game.CPlayerMainStoryMgr"
local CPlayerMainStoryShareWnd = import "L10.UI.CPlayerMainStoryShareWnd"
local DelegateFactory = import "DelegateFactory"
local UIEventListener = import "UIEventListener"
local ShareMgr = import "ShareMgr"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local CJingLingWebImageMgr = import "L10.Game.CJingLingWebImageMgr"
local Application = import "UnityEngine.Application"
local EShareType = import "L10.UI.EShareType"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
CPlayerMainStoryShareWnd.m_Init_CS2LuaHook = function (this) 
    local picPath = CPlayerMainStoryMgr.Inst:GetNowSharePicPath()
    if System.String.IsNullOrEmpty(picPath) then
        this:Close()
        return
    end

    this.sharePic:LoadMaterial(picPath)

    UIEventListener.Get(this.shareBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        this:ShareBtnClick()
    end)

    UIEventListener.Get(this.backBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        this:Close()
    end)

	if CommonDefs.IS_VN_CLIENT then
		this.shareBtn:SetActive(false)
		this.shareInput.gameObject:SetActive(false)
	end
end
CPlayerMainStoryShareWnd.m_ShareBtnClick_CS2LuaHook = function (this) 
    local shareTexture = this.sharePic.mainTexture
    local shareText = this.shareInput.value
    shareText = CChatMgr.Inst:FilterYangYangEmotion(shareText)

    if shareTexture == nil then
        return
    end

    local textureArray = CreateFromClass(MakeArrayClass(System.String), 1)
    CommonDefs.GetComponent_GameObject_Type(this.shareBtn, typeof(CButton)).Enabled = false

    textureArray[0] = CPlayerMainStoryMgr.Inst:GetNowSharePicURL()
    ShareMgr.DownloadImageWebImage2Share(textureArray[0], DelegateFactory.Action(function ()
        shareText = CWordFilterMgr.Inst:DoFilterOnSendPersonalSpace(shareText, true)
        local fileName = CJingLingWebImageMgr.Inst:GetFileNameByUrl(textureArray[0])
        local filePath = Application.temporaryCachePath .. fileName
        ShareMgr.Share2PersonalSpaceFile = filePath
        ShareMgr.Share2PersonalSpaceUrl = ""
        ShareMgr.CurShare2PersonalSpaceImageType = ShareMgr.EnumShare2PersonalSpaceImageType.Local
        ShareMgr.onShareSuccess = DelegateFactory.Action(function ()
            if CClientMainPlayer.Inst ~= nil then
                CPersonalSpaceMgr.Inst:OpenPersonalSpaceWnd(CClientMainPlayer.Inst.Id, 0)
            end
            this:Close()
        end)
        ShareMgr.Share2PersonalSpace(shareText, true)
    end))
end
