local EventManager=import "EventManager"
local EnumEventType = import "EnumEventType"

CLuaGameVideoPlayWnd = class()
RegistClassMember(CLuaGameVideoPlayWnd,"mask")
RegistClassMember(CLuaGameVideoPlayWnd,"zhankaiGo")
RegistClassMember(CLuaGameVideoPlayWnd,"zhankaiBtn")
RegistClassMember(CLuaGameVideoPlayWnd,"slider")
RegistClassMember(CLuaGameVideoPlayWnd,"timeLabel")
RegistClassMember(CLuaGameVideoPlayWnd,"mEnableUpdateSlider")
RegistClassMember(CLuaGameVideoPlayWnd,"mTime")
RegistClassMember(CLuaGameVideoPlayWnd,"TriggerTime")
RegistClassMember(CLuaGameVideoPlayWnd,"m_Tick")

function CLuaGameVideoPlayWnd:Awake()
    self.mask = self.transform:Find("Anchor/ZhanKai/Mask").gameObject
    self.zhankaiGo = self.transform:Find("Anchor/ZhanKai").gameObject
    self.zhankaiBtn = self.transform:Find("BottomRight/ZhanKaiBtn").gameObject
    self.slider = self.transform:Find("Anchor/ZhanKai/GameObject"):GetComponent(typeof(UISlider))
    self.timeLabel = self.transform:Find("Anchor/ZhanKai/Label"):GetComponent(typeof(UILabel))
    self.mEnableUpdateSlider = true
    self.mTime = 0
    self.TriggerTime = 3

    UIEventListener.Get(self.zhankaiBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnClickZhankaiButton(go)
    end) 
    UIEventListener.Get(self.mask).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnClickMask(go)
    end) 

    self.zhankaiGo:SetActive(false)
    self.zhankaiBtn:SetActive(true)

    self.slider.onDragFinished = LuaUtils.OnDragFinished(function() 
        EventManager.BroadcastInternalForLua(EnumEventType.ChangeGameVideoProgress, {self.slider.value})
        self:ResetTime()
    end)

    UIEventListener.Get(self.slider.gameObject).onPress = DelegateFactory.BoolDelegate(function(go,b)
        self.mEnableUpdateSlider = not b
        self:ResetTime()
    end)

    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
    end
    self.m_Tick = RegisterTick(function()--每秒更新一次
        if not self.mEnableUpdateSlider then
            return
        end
        self.mTime = self.mTime + 0.5
        if self.mTime > self.TriggerTime then
            self.zhankaiBtn:SetActive(true)
            self.zhankaiGo:SetActive(false)
            g_ScriptEvent:BroadcastInLua("ShowVideoProgress",false)
        end
    end,500)
end

function CLuaGameVideoPlayWnd:OnClickZhankaiButton(go)
    self.zhankaiBtn:SetActive(false)
    self.zhankaiGo:SetActive(true)
    self:ResetTime()
	g_ScriptEvent:BroadcastInLua("ShowVideoProgress",true)
end

function CLuaGameVideoPlayWnd:OnClickMask(go)
    self.zhankaiBtn:SetActive(true)
    self.zhankaiGo:SetActive(false)
    self:ResetTime()
	g_ScriptEvent:BroadcastInLua("ShowVideoProgress",false)
end

function CLuaGameVideoPlayWnd:OnEnable()
    g_ScriptEvent:AddListener("MainPlayerDestroyed",self,"OnMainPlayerDestroyed")    
    g_ScriptEvent:AddListener("UpdateGameVideoProgress",self,"OnUpdateGameVideoProgress")    
    
end
function CLuaGameVideoPlayWnd:OnDisable()
    g_ScriptEvent:RemoveListener("MainPlayerDestroyed",self,"OnMainPlayerDestroyed")    
    g_ScriptEvent:RemoveListener("UpdateGameVideoProgress",self,"OnUpdateGameVideoProgress")    

end
function CLuaGameVideoPlayWnd:ResetTime()
    self.mTime = 0
end


function CLuaGameVideoPlayWnd:OnUpdateGameVideoProgress( args) 
    local value1, value2=args[0],args[1]
    --在操作滑竿的时候 不响应进度
    if not self.mEnableUpdateSlider then
        return
    end
    if value2 == 0 then
        return
    end
    --在drag的时候不处理
    self.slider.value = (value1 / value2)
    local value = value1 / 1000
    local h = math.floor((value / 3600))
    local m = math.floor(math.floor(value) / 60) % 60
    local s = math.floor(value) % 60
    self.timeLabel.text = SafeStringFormat3("%02d:%02d:%02d", h, m, s)
end

function CLuaGameVideoPlayWnd:OnMainPlayerDestroyed()
    CUIManager.CloseUI(CUIResources.GameVideoPlayWnd)
end

function CLuaGameVideoPlayWnd:OnDestroy()
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
    end
end
