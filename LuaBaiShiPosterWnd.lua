local GameObject = import "UnityEngine.GameObject"

local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local CShiTuMgr = import "L10.Game.CShiTuMgr"
local CIMMgr = import "L10.Game.CIMMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
local AlignType = import "CPlayerInfoMgr+AlignType"
local CItemMgr = import "L10.Game.CItemMgr"
local CTitleMgr = import "L10.Game.CTitleMgr"

LuaBaiShiPosterWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaBaiShiPosterWnd, "TimeLabel", "TimeLabel", UILabel)
RegistChildComponent(LuaBaiShiPosterWnd, "ShiFuPortrait", "ShiFuPortrait", CUITexture)
RegistChildComponent(LuaBaiShiPosterWnd, "TuDiPortrait", "TuDiPortrait", CUITexture)
RegistChildComponent(LuaBaiShiPosterWnd, "ShiFuNameLabel", "ShiFuNameLabel", UILabel)
RegistChildComponent(LuaBaiShiPosterWnd, "TuDiNameLabel", "TuDiNameLabel", UILabel)
RegistChildComponent(LuaBaiShiPosterWnd, "ShiFuTitleLabel", "ShiFuTitleLabel", UILabel)
RegistChildComponent(LuaBaiShiPosterWnd, "TuDiTitleLabel", "TuDiTitleLabel", UILabel)

--@endregion RegistChildComponent end

function LuaBaiShiPosterWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.ShiFuPortrait.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShiFuPortraitClick()
	end)


    --@endregion EventBind end
end

function LuaBaiShiPosterWnd:Init()
    local shifuPlayerId = CShiTuMgr.Inst:GetCurrentShiFuId()
    local shifu = CIMMgr.Inst:GetBasicInfo(shifuPlayerId)
    if not shifu or not CClientMainPlayer.Inst then
        CUIManager.CloseUI(CLuaUIResources.BaiShiPosterWnd)
        return
    end
    self.ShiFuPortrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(shifu.Class, shifu. Gender, -1), false)
    self.TuDiPortrait:LoadNPCPortrait(CClientMainPlayer.Inst.PortraitName, false)
    self.ShiFuNameLabel.text = shifu.Name
    self.TuDiNameLabel.text = CClientMainPlayer.Inst.Name
    local shituInfo = CommonDefs.DictGetValue(CClientMainPlayer.Inst.RelationshipProp.ShiFu, typeof(UInt64), shifuPlayerId)
    if shituInfo == nil then
        return
    end
    local dateTime = CServerTimeMgr.ConvertTimeStampToZone8Time(shituInfo.Time)
    if CommonDefs.IS_VN_CLIENT then
        self.TimeLabel.text = ToStringWrap(dateTime, LocalString.GetString("yyyy年MM月dd日 ")) .. LocalString.GetString("结为师徒")
    else
        self.TimeLabel.text = ToStringWrap(dateTime, LocalString.GetString("于yyyy年MM月dd日结为师徒"))
    end
    local item = CItemMgr.Inst:GetById(LuaShiTuMgr.m_BaiShiPosterItemid)
    self.ShiFuTitleLabel.text = ""
    self.TuDiTitleLabel.text = ""
    if item then
        if item.Item.ExtraVarData ~= nil and item.Item.ExtraVarData.Data ~= nil then
            local raw = item.Item.ExtraVarData.Data
            local list = g_MessagePack.unpack(raw)
            if #list >= 6 then
                local titleId1, titleId2 = list[5],list[6]
                local titleData1, titleData2 = Title_Title.GetData(titleId1),Title_Title.GetData(titleId2)
                if titleData1 then
                    self.ShiFuTitleLabel.text = titleData1.Name
                    self.ShiFuTitleLabel.color = CTitleMgr.Inst:GetTitleColor(titleId1)
                end
                self.ShiFuTitleLabel.gameObject:SetActive(titleData1 ~= nil)
                if titleData2 then
                    self.TuDiTitleLabel.text = titleData2.Name
                    self.TuDiTitleLabel.color = CTitleMgr.Inst:GetTitleColor(titleId2)
                end
                self.TuDiTitleLabel.gameObject:SetActive(titleData2 ~= nil)
            end
        end
    end
end

--@region UIEvent

function LuaBaiShiPosterWnd:OnShiFuPortraitClick()
    local shifuPlayerId = CShiTuMgr.Inst:GetCurrentShiFuId()
    CPlayerInfoMgr.ShowPlayerPopupMenu(shifuPlayerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, Vector3.zero, AlignType.Default)
end


--@endregion UIEvent

