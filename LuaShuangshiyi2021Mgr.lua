LuaShuangshiyi2021Mgr = {}

-- 砍一刀同步数据
LuaShuangshiyi2021Mgr.KanYiDaoTaskTable = nil

function LuaShuangshiyi2021Mgr:SyncEntrustTaskInfo(taskTbl_U, leftFreeTimes, acceptTimes)
    local dict = MsgPackImpl.unpack(taskTbl_U)
	if not dict then
        return
    end

    -- dict在lua下的遍历
    local list = {}
	CommonDefs.DictIterate(dict, DelegateFactory.Action_object_object(function (key, value)
		local id = tonumber(key)
        local val = {}
        CommonDefs.ListIterate(value, DelegateFactory.Action_object(function (___value)
            table.insert(val, tonumber(___value))
        end))

        table.insert(list, {taskId = id, taskInfo = {npcId = val[1], taskStatus = val[2], pos = val[3]}})
	end))

    -- 按pos排序
    table.sort(list, function(a, b)
        if a.taskInfo.pos ~= b.taskInfo.pos then
            return a.taskInfo.pos < b.taskInfo.pos
        else
            return a.taskId < b.taskId
        end
    end)

    -- dict在lua中取数据
    -- local value=CommonDefs.DictGetValue_LuaCall(dict,"key")
    g_ScriptEvent:BroadcastInLua("SyncEntrustTaskInfo", list, leftFreeTimes, acceptTimes)
end


function LuaShuangshiyi2021Mgr:SyncKanYiDaoTaskInfo(playState, eventTbl_U)
    local dict = MsgPackImpl.unpack(eventTbl_U)
	if not dict then
        return
    end

    local list = {}
    local eventDict = {
        BaoXiangDie = 1,
        LingYuInRing = 2,
        BossDie = 3,
    }
	CommonDefs.DictIterate(dict, DelegateFactory.Action_object_object(function (key, value)
		local id = tostring(key)
		local val = tonumber(value)
        table.insert(list, {eventId = eventDict[id], finishTimes = val})
	end))
    -- 根据eventId排序
    table.sort(list, function(a, b)
        return a.eventId < b.eventId
    end)
    LuaShuangshiyi2021Mgr.KanYiDaoTaskTable = list
    g_ScriptEvent:BroadcastInLua("SyncKanYiDaoTaskInfo")
end