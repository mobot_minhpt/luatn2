local Vector3 = import "UnityEngine.Vector3"
local UILabel = import "UILabel"

CLuaTianMenShanBattleStateWnd = class()
RegistClassMember(CLuaTianMenShanBattleStateWnd,"m_ExpandButton")
RegistClassMember(CLuaTianMenShanBattleStateWnd,"m_Label")

function CLuaTianMenShanBattleStateWnd:Awake()
    self.m_ExpandButton = self.transform:Find("Anchor/Tip/ExpandButton").gameObject
    self.m_Label = self.transform:Find("Anchor/ReliveInfo/Label"):GetComponent(typeof(UILabel))

    UIEventListener.Get(self.m_ExpandButton).onClick = DelegateFactory.VoidDelegate(function (p) 
        self.m_ExpandButton.transform.localEulerAngles = Vector3(0, 0, 0)
        CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
    end)
end

function CLuaTianMenShanBattleStateWnd:Init()
end

function CLuaTianMenShanBattleStateWnd:OnEnable()
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:AddListener("SyncTianMenShanBattleLeftReliveCount", self, "OnSyncTianMenShanBattleLeftReliveCount")

    -- 刷新界面
    self:OnSyncTianMenShanBattleLeftReliveCount(CLuaCityWarMgr.m_TianMenShanLeftReliveCount)
end

function CLuaTianMenShanBattleStateWnd:OnDisable()
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:RemoveListener("SyncTianMenShanBattleLeftReliveCount", self, "OnSyncTianMenShanBattleLeftReliveCount")

    --这个界面不显示的话，那么tip界面肯定也不显示
    if CUIManager.IsLoaded(CUIResources.TopAndRightTipWnd) then
        CUIManager.CloseUI(CUIResources.TopAndRightTipWnd)
    end
end

function CLuaTianMenShanBattleStateWnd:OnHideTopAndRightTipWnd()
    self.m_ExpandButton.transform.localEulerAngles = Vector3(0, 0, 180)
end

function CLuaTianMenShanBattleStateWnd:OnSyncTianMenShanBattleLeftReliveCount(count)
    self.m_Label.text = SafeStringFormat(LocalString.GetString("剩余复活次数：%d"), count)
end

return CLuaTianMenShanBattleStateWnd
