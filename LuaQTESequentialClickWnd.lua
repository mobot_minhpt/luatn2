local DelegateFactory = import "DelegateFactory"
local CommonDefs = import "L10.Game.CommonDefs"
local UICommonDef = import "L10.UI.CUICommonDef"
local Extensions = import "Extensions"
local UILabel = import "UILabel"
local UITexture = import "UITexture"
local UnityEngine = import "UnityEngine"
local CUIFx = import "L10.UI.CUIFx"

LuaQTESequentialClickWnd = class()
RegistClassMember(LuaQTESequentialClickWnd, "ButtonTemplate")
RegistClassMember(LuaQTESequentialClickWnd, "ButtonRoot")
RegistClassMember(LuaQTESequentialClickWnd, "ButtonInfo")
--[[
    ButtonInfo[i] = {x=0, y=0, duration = 3.0, beginTime = 1.0}
--]]
RegistClassMember(LuaQTESequentialClickWnd, "curTime")
RegistClassMember(LuaQTESequentialClickWnd, "nextCorrectBtn")

function LuaQTESequentialClickWnd:Awake()
    self.ButtonTemplate = self.transform:Find("ButtonTemplate").gameObject
    self.ButtonRoot = self.transform:Find("ButtonRoot").gameObject

    self.gameObject:SetActive(false)
end

function LuaQTESequentialClickWnd:Init()
    if LuaQTEMgr.CurrentQTEType ~= EnumQTEType.SequentialClick then
        return
    end

    self.curTime = tonumber(LuaQTEMgr.CurrentQTEInfo.qteTimeOffset)
    self.ButtonTemplate:SetActive(false)
    Extensions.RemoveAllChildren(self.ButtonRoot.transform)

    self.ButtonInfo = {}
    for i, info in ipairs(LuaQTEMgr.CurrentQTEInfo.iconList) do
        self.ButtonInfo[i] = {}
        self.ButtonInfo[i].x = info.xPos
        self.ButtonInfo[i].y = info.yPos
        self.ButtonInfo[i].duration = tonumber(info.duration)
        self.ButtonInfo[i].beginTime = 0
        local curObj = UICommonDef.AddChild(self.ButtonRoot, self.ButtonTemplate)
        self.ButtonInfo[i].go = curObj
        curObj:SetActive(false)
    end
    self.nextCorrectBtn = 1
    self:InitOneButton(self.nextCorrectBtn)
    self.gameObject:SetActive(true)
end

function LuaQTESequentialClickWnd:InitOneButton(number)
    local go = self.ButtonInfo[number].go
    go:SetActive(true)
    local button = go.transform:Find("Button").gameObject
    local label = go.transform:Find("Button/Label"):GetComponent(typeof(UILabel))
    local countDownBar = go.transform:Find("CountDownBar"):GetComponent(typeof(UITexture))
    local Fx = go.transform:Find("Fx"):GetComponent(typeof(CUIFx))

    go.transform.localPosition = LuaQTEMgr.ParsePosition(go, self.ButtonInfo[number].x, self.ButtonInfo[number].y)

    label.text = tostring(number)
    self.ButtonInfo[number].countDownBar = countDownBar

    CommonDefs.AddOnClickListener(
        button,
        DelegateFactory.Action_GameObject(
            function(go)
                self:OnButtonClick(go, number)
            end
        ),
        false
    )

    if LuaQTEMgr.CurrentQTEInfo.HintFx == nil or LuaQTEMgr.CurrentQTEInfo.HintFx == "" then
        label.gameObject:SetActive(true)
        Fx:LoadFx(CLuaUIFxPaths.QTESingleClickFx)
    else
        label.gameObject:SetActive(false)
        Fx:LoadFx(SafeStringFormat3("Fx/UI/Prefab/%s.prefab", LuaQTEMgr.CurrentQTEInfo.HintFx))
    end

    self.ButtonInfo[number].beginTime = self.curTime
end

function LuaQTESequentialClickWnd:Update()
    local deltaTime = UnityEngine.Time.deltaTime
    self.curTime = self.curTime + deltaTime

    if self.nextCorrectBtn and self.nextCorrectBtn>=1 and self.nextCorrectBtn<=#self.ButtonInfo then
        local i = self.nextCorrectBtn
        local CountDownBar = self.ButtonInfo[i].countDownBar
        local beginTime = self.ButtonInfo[i].beginTime

        if beginTime > self.curTime then
            CountDownBar.fillAmount = 1.0
        else
            local passedTime = self.curTime - beginTime
            local duration = self.ButtonInfo[i].duration
            if passedTime >= duration then
                self:QTEFail()
                return
            else
                local ratio = 0.0
                if duration ~= 0 then
                    ratio = 1 - passedTime / duration
                end
                CountDownBar.fillAmount = ratio
            end
        end
    end
end

function LuaQTESequentialClickWnd:OnButtonClick(go, number)
    if number == self.nextCorrectBtn then
        self:OnSingleButtonSuccess(number)
    else
        self:QTEFail()
    end
end

function LuaQTESequentialClickWnd:OnSingleButtonSuccess(number)
    self.ButtonInfo[number].go:SetActive(false)
    LuaQTEMgr.TriggerQTEFinishFx(true, self.ButtonInfo[number].go.transform.localPosition)
    if number == #self.ButtonInfo then
        self:QTESuccess()
    else
        self.nextCorrectBtn = self.nextCorrectBtn + 1
        self:InitOneButton(self.nextCorrectBtn)
    end
end

function LuaQTESequentialClickWnd:QTESuccess()
    self:Reset()
    LuaQTEMgr.FinishCurrentQTE(true)
end

function LuaQTESequentialClickWnd:QTEFail()
    LuaQTEMgr.TriggerQTEFinishFx(false, self.ButtonInfo[self.nextCorrectBtn].go.transform.localPosition)
    self:Reset()
    LuaQTEMgr.FinishCurrentQTE(false)
end

--整体QTE超时
function LuaQTESequentialClickWnd:OnTimeLimitExceeded()
    LuaQTEMgr.TriggerQTEFinishFx(false, self.ButtonInfo[self.nextCorrectBtn].go.transform.localPosition)
    self:Reset()
end

function LuaQTESequentialClickWnd:Reset()
    Extensions.RemoveAllChildren(self.ButtonRoot.transform)
    self.ButtonInfo = {}
    self.nextCorrectBtn = 0
    self.curTime = 0.0
    self.gameObject:SetActive(false)
    g_ScriptEvent:BroadcastInLua("OnQTESubWndClose")
end
