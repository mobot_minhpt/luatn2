local Color = import "UnityEngine.Color"
local CItemMgr = import "L10.Game.CItemMgr"
local CEquipmentProcessMgr = import "L10.Game.CEquipmentProcessMgr"
local CButton = import "L10.UI.CButton"
local CBaseEquipmentItem=import "L10.UI.CBaseEquipmentItem"
local GameSetting_Common_Wapper = import "L10.Game.GameSetting_Common_Wapper"

local EnumItemType = import "L10.Game.EnumItemType"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local UIRotTable=import "UIRotTable"

CLuaEquipInlayStoneDetailView = class()
RegistClassMember(CLuaEquipInlayStoneDetailView,"equipItem")
RegistClassMember(CLuaEquipInlayStoneDetailView,"nameLabel")
RegistClassMember(CLuaEquipInlayStoneDetailView,"holeList")
RegistClassMember(CLuaEquipInlayStoneDetailView,"gemUpgradeBtn")
RegistClassMember(CLuaEquipInlayStoneDetailView,"checkGemgroupBtn")
RegistClassMember(CLuaEquipInlayStoneDetailView,"putoffGemBtn")
RegistClassMember(CLuaEquipInlayStoneDetailView,"m_FaBaoTip")
RegistClassMember(CLuaEquipInlayStoneDetailView,"m_TipLabel2")
RegistClassMember(CLuaEquipInlayStoneDetailView,"item")

RegistClassMember(CLuaEquipInlayStoneDetailView,"m_CommonItem")
RegistClassMember(CLuaEquipInlayStoneDetailView,"m_HoleList")
RegistClassMember(CLuaEquipInlayStoneDetailView,"m_HoleInfoList")
RegistClassMember(CLuaEquipInlayStoneDetailView,"m_TabView")
-- RegistClassMember(CLuaEquipInlayStoneDetailView,"m_HoleSetIndex")
RegistClassMember(CLuaEquipInlayStoneDetailView,"m_SwitchButton")

function CLuaEquipInlayStoneDetailView:Awake()
    self.equipItem = self.transform:Find("EquipItem"):GetComponent(typeof(CBaseEquipmentItem))
    self.nameLabel = self.transform:Find("EquipItem/Name"):GetComponent(typeof(UILabel))
    self.holeList = self.transform:Find("HoleList")--:GetComponent(typeof(CEquipHoleList))
    self.gemUpgradeBtn = self.transform:Find("GemUpgradeBtn").gameObject
    self.checkGemgroupBtn = self.transform:Find("CheckGemGroupBtn").gameObject
    self.putoffGemBtn = self.transform:Find("PutoffGemBtn").gameObject
    
    local tf = self.transform:Find("SwitchButton")
    if tf then
        self.m_SwitchButton = tf.gameObject
        self.m_SwitchButton:SetActive(false)
        UIEventListener.Get(self.m_SwitchButton).onClick=DelegateFactory.VoidDelegate(function(go)
            local itemInfo = CItemMgr.Inst:GetItemInfo(self.item.Id)
            if itemInfo then
                self.m_SwitchButton:SetActive(false)
                Gac2Gas.RequestCanSwitchEquipHoleSet(EnumToInt(itemInfo.place), itemInfo.pos, self.item.Id)
            end
        end)
    end

    self.m_TipLabel2 = FindChildWithType(self.transform,"TipLabel2",typeof(UILabel))

    local tip = self.transform:Find("TipLabel")
    self.m_FaBaoTip = tip and tip.gameObject
    self.item = nil

    UIEventListener.Get(self.gemUpgradeBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnStoneUpdateBtnClicked(go)
    end)
    UIEventListener.Get(self.checkGemgroupBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.ShowUI(CUIResources.CheckGemGroupWnd)
    end)
    UIEventListener.Get(self.putoffGemBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnPutoffGemGroupClick(go)
    end)
    self:Init(nil,false)
end

function CLuaEquipInlayStoneDetailView:Init( itemId, isFaBao) 
    self.nameLabel.text = ""
    self.nameLabel.color = Color.white
    --基本信息
    self.item = CItemMgr.Inst:GetById(itemId)
    self.equipItem:UpdateData(itemId)
    
    if self.m_FaBaoTip ~= nil then
        self.m_FaBaoTip:SetActive(false)
    end
    if isFaBao then
        self.checkGemgroupBtn:SetActive(false)
    end
    if self.item == nil or self.item.Equip.IsExtraEquipment then
        CommonDefs.GetComponent_GameObject_Type(self.gemUpgradeBtn, typeof(CButton)).Enabled = false
        CommonDefs.GetComponent_GameObject_Type(self.checkGemgroupBtn, typeof(CButton)).Enabled = false
        CommonDefs.GetComponent_GameObject_Type(self.putoffGemBtn, typeof(CButton)).Enabled = false
        -- self.holeList:Init(nil)
        self.equipItem:UpdateData("")
        self:InitEquipHoleList(self.holeList,nil)
        return
    end
    CommonDefs.GetComponent_GameObject_Type(self.checkGemgroupBtn, typeof(CButton)).Enabled = true

    local hole2set = self.item.Equip.HoleSetCount==2--是否开启二套宝石

    local gemGroup = GemGroup_GemGroup.GetData(self.item.Equip.GemGroupId)
    if gemGroup ~= nil then
        if hole2set then
            if self.item.Equip.ActiveHoleSetIdx<=1 then
                self.nameLabel.text = SafeStringFormat3(LocalString.GetString("石之灵一·%s(%d级)"), gemGroup.Name, gemGroup.Level)
            else
                self.nameLabel.text = SafeStringFormat3(LocalString.GetString("石之灵二·%s(%d级)"), gemGroup.Name, gemGroup.Level)
            end
        else
            self.nameLabel.text = SafeStringFormat3(LocalString.GetString("石之灵·%s(%d级)"), gemGroup.Name, gemGroup.Level)
        end
        self.nameLabel.color = GameSetting_Common_Wapper.Inst:GetColor(gemGroup.NameColor)
    else
        if hole2set then
            if self.item.Equip.ActiveHoleSetIdx<=1 then
                self.nameLabel.text = LocalString.GetString("石之灵一(无)")
            else
                self.nameLabel.text = LocalString.GetString("石之灵二(无)")
            end
        else
            self.nameLabel.text = LocalString.GetString("石之灵(无)")
        end
        self.nameLabel.color = Color.white
    end

    --是否支持切换
    if self.item.Equip.HasTwoWordSet then
        self.m_SwitchButton:SetActive(true)
        LuaUtils.SetLocalPositionX(self.nameLabel.transform,-20)
    else
        self.m_SwitchButton:SetActive(false)
        LuaUtils.SetLocalPositionX(self.nameLabel.transform,0)
    end

    local hasGem = self:InitEquipHoleList(self.holeList,self.item)--self.holeList:Init(self.item)

    --打孔列表
    CommonDefs.GetComponent_GameObject_Type(self.gemUpgradeBtn, typeof(CButton)).Enabled = hasGem
    CommonDefs.GetComponent_GameObject_Type(self.putoffGemBtn, typeof(CButton)).Enabled = hasGem

    local equip = EquipmentTemplate_Equip.GetData(self.item.TemplateId)
    if self.m_FaBaoTip ~= nil then
        self.m_FaBaoTip:SetActive(equip.Type == EnumBodyPosition_lua.TalismanQian or equip.Type == EnumBodyPosition_lua.TalismanKun)
    end

    EquipmentTemplate_SubType.Foreach(function (key, data) 
        if data.Type == equip.Type and data.SubType == equip.SubType then
            self.checkGemgroupBtn:SetActive(data.GemGroup == 1)
        end
    end)

    local issubhand = self.item.Equip.SubHand > 0
    if self.m_TipLabel2 then
        self.m_TipLabel2.gameObject:SetActive(issubhand)
    end

    self.gemUpgradeBtn:SetActive(not issubhand)
    self.checkGemgroupBtn:SetActive(not issubhand)
    self.putoffGemBtn:SetActive(not issubhand)

end
function CLuaEquipInlayStoneDetailView:OnEnable( )
    g_ScriptEvent:AddListener("SelectEquipment", self, "OnSelected")
    g_ScriptEvent:AddListener("SendItem", self, "OnSendItem")
    g_ScriptEvent:AddListener("RequestCanSwitchEquipHoleSetReturn",self,"OnRequestCanSwitchEquipHoleSetReturn")
end
function CLuaEquipInlayStoneDetailView:OnDisable( )
    g_ScriptEvent:RemoveListener("SelectEquipment", self, "OnSelected")
    g_ScriptEvent:RemoveListener("SendItem", self, "OnSendItem")
    g_ScriptEvent:RemoveListener("RequestCanSwitchEquipHoleSetReturn",self,"OnRequestCanSwitchEquipHoleSetReturn")
end

function CLuaEquipInlayStoneDetailView:OnSelected(args)
    local itemId = args[0]
    CLuaEquipMgr.SelectedEquipmentHoleInfo = nil
    self:Init(itemId,false)
end

function CLuaEquipInlayStoneDetailView:OnSendItem( args) 
    local itemId = args[0]
    if CEquipmentProcessMgr.Inst.SelectEquipment ~= nil then
        if itemId == CEquipmentProcessMgr.Inst.SelectEquipment.itemId then
            self:Init(itemId, false)
        end
    end
end
function CLuaEquipInlayStoneDetailView:OnStoneUpdateBtnClicked( go) 
    --宝石升级
    CLuaEquipMgr.m_GemUpdateSelectedEquipment = self.item
    CLuaGemUpdateScrollView.SelectHoleInfo = CLuaEquipMgr.SelectedEquipmentHoleInfo
    CLuaGemUpgradeWnd.m_HoleSetIndex = self.item.Equip.ActiveHoleSetIdx
    CUIManager.ShowUI(CUIResources.GemUpgradeWnd)
end
function CLuaEquipInlayStoneDetailView:OnPutoffGemGroupClick( go) 
    local holeInfo = CLuaEquipMgr.SelectedEquipmentHoleInfo
    local info = CEquipmentProcessMgr.Inst.SelectEquipment
    if info == nil or holeInfo == nil or holeInfo.stoneItemId == 0 then
        if self.item ~= nil and self.item.IsEquip and self.item.Equip.IsFaBao then
            g_MessageMgr:ShowMessage("FABAO_TAKEOFF_WENSHI_NOTCHOOSE")
        else
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("请选择一个宝石"))
        end
        return
    end
    if not CEquipmentProcessMgr.Inst:CheckOperationConflict(info) then
        return
    end
    Gac2Gas.RequestCanRemoveStoneOnEquipment(EnumToInt(info.place), info.pos, info.itemId, holeInfo.holeIndex, self.item.Equip.ActiveHoleSetIdx)
end

function CLuaEquipInlayStoneDetailView:InitEquipHoleList(transform,item)
    local holeTemplate = transform:Find("HoleTemplate").gameObject
    holeTemplate:SetActive(false)

    local center = transform:Find("Center"):GetComponent(typeof(UIRotTable))
    -- local tableCmp = transform:Find("Table"):GetComponent(typeof(UITable))
    Extensions.RemoveAllChildren(center.transform)

    self.m_CommonItem = item

    local hasStone = false
    self.m_HoleList = {}
    self.m_HoleInfoList = {}

    if item == nil then
        return false
    end

    local gemGroup = GemGroup_GemGroup.GetData(item.Equip.GemGroupId)
    local gemGroupId = {}
    if gemGroup then
        for i=1,gemGroup.ConsistType.Length do
            gemGroupId[gemGroup.ConsistType[i-1]] = true
        end
    end

    local maxHole = CItemMgr.Inst:GetEquipmentMaxHole(item.TemplateId)
    
    local equipment = item.Equip

    local holeItems = {}
    local referenceIndex = {}
    for i=1,equipment.Hole do
        local jewelId = item.Equip.SecondaryHoleItems[i]
        if jewelId>0 and jewelId<100 then
            referenceIndex[item.Equip.HoleItems[jewelId]] = true
            referenceIndex[jewelId] = true
        end
    end
    local items = item.Equip.ActiveHoleSetIdx<=1 and item.Equip.HoleItems or item.Equip.SecondaryHoleItems
    for i=1,equipment.Hole do
        table.insert(holeItems,items[i])
    end

    for i=1,maxHole do
        local stone = nil
        local isReference = false
        if holeItems[i] then
            isReference = referenceIndex[holeItems[i]]
            if holeItems[i]>0 and holeItems[i]<100 then
                stone = CItemMgr.Inst:GetItemTemplate(equipment.HoleItems[holeItems[i]])
            else
                stone = CItemMgr.Inst:GetItemTemplate(holeItems[i])
            end
        end

        local hole = NGUITools.AddChild(center.gameObject, holeTemplate)
        hole:SetActive(true)

        local holeInfo = {
            holeIndex = i,   
            stoneItemId = i <= equipment.Hole and holeItems[i] or 0,
            isReference = isReference,
        }

        table.insert( self.m_HoleInfoList, holeInfo )
        table.insert( self.m_HoleList, hole )
        if equipment.SubHand > 0 then --副手
            self:InitHoleItem(hole.transform,4, holeInfo, nil, false)
            UIEventListener.Get(hole).onClick = DelegateFactory.VoidDelegate(function(go)
                g_MessageMgr:ShowMessage("ZHANKUANG_FUSHOU_FILLIN_FORBID")
            end)
        else
            if i <= equipment.Hole then
                if stone then
                    if gemGroup then
                        local jewel = Jewel_Jewel.GetData(stone.ID)
                        self:InitHoleItem(hole.transform,LuaEnumHoleStatus.availableWithGem, holeInfo, stone, gemGroupId[jewel.JewelKind],isReference)
                    else
                        self:InitHoleItem(hole.transform,LuaEnumHoleStatus.availableWithGem, holeInfo, stone, false,isReference)
                    end
                    UIEventListener.Get(hole).onClick = DelegateFactory.VoidDelegate(function(go)
                        self:OnGemSelect(go)
                    end)
                    hasStone = true
                else--未镶嵌
                    self:InitHoleItem(hole.transform,LuaEnumHoleStatus.availableWithoutGem, holeInfo, nil, false)
                    UIEventListener.Get(hole).onClick = DelegateFactory.VoidDelegate(function(go)
                        self:OnInlayGemClicked(go)
                    end)
                end
            else--未打孔
                self:InitHoleItem(hole.transform,LuaEnumHoleStatus.disable, holeInfo, nil, false)
                UIEventListener.Get(hole).onClick = DelegateFactory.VoidDelegate(function(go)
                    self:OnOpenHoleClicked(go)
                end)
            end
        end
    end
    center:Reposition()
    return hasStone
end

function CLuaEquipInlayStoneDetailView:OnGemSelect( go) 
    for i,v in ipairs(self.m_HoleList) do
        self:SetHoleSelected(v.transform,go==v)

        if go == v then
            CLuaEquipMgr.SelectedEquipmentHoleInfo = self.m_HoleInfoList[i]
            CLuaGemInfoWnd.GemId = self.m_HoleInfoList[i].stoneItemId
            CLuaGemInfoWnd.IsReference = self.m_HoleInfoList[i].isReference
            if CLuaGemInfoWnd.GemId>0 and CLuaGemInfoWnd.GemId<100 then
                local commonItem = CItemMgr.Inst:GetById(CEquipmentProcessMgr.Inst.SelectEquipment.itemId)
                CLuaGemInfoWnd.GemId = commonItem.Equip.HoleItems[CLuaGemInfoWnd.GemId]
            end
            CLuaEquipMgr.ShowGemInfoWnd(false, nil,nil)
        end
    end
end

function CLuaEquipInlayStoneDetailView:OnInlayGemClicked( go) 
    for i,v in ipairs(self.m_HoleList) do
        if go == v then
            self:SetHoleSelected(v.transform,true)

            local count = 0
            local itemList = CItemMgr.Inst:GetPlayerItemAtPlaceByType(EnumItemPlace.Bag, EnumItemType.Gem)
            if itemList ~= nil and itemList.Count > 0 then
                count = itemList.Count
            else
                local equip = self.m_CommonItem.Equip
                if self.item.Equip.ActiveHoleSetIdx==2 then
                    local refLookup = {}
                    for i=1,equip.Hole do
                        local jewelId = equip.SecondaryHoleItems[i]
                        if jewelId>0 and jewelId<100 then
                            refLookup[equip.HoleItems[jewelId]] = i
                        end
                    end
                    for i=1,equip.Hole do
                        local jewelId = equip.HoleItems[i]
                        if jewelId>0 and not refLookup[jewelId] then
                            count=count+1
                        end
                    end
                else
                    for i=1,equip.Hole do
                        local jewelId = equip.SecondaryHoleItems[i]
                        if jewelId>100 then
                            count=count+1
                        end
                    end
                end
            end

            if count>0 then
                CLuaEquipMgr.SelectedEquipmentHoleInfo = self.m_HoleInfoList[i]
                CLuaEquipMgr.m_GemInlaySelectedEquipment = self.m_CommonItem
                CLuaInlayGemListWnd.m_HoleSetIndex = self.item.Equip.ActiveHoleSetIdx
                CUIManager.ShowUI(CUIResources.InlayGemListWnd)
            else
                if self.m_CommonItem and self.m_CommonItem.IsEquip and self.m_CommonItem.Equip.IsFaBao then
                    CItemAccessListMgr.Inst:ShowItemAccessInfo(38, false, go.transform)
                else
                    CItemAccessListMgr.Inst:ShowItemAccessInfo(37, false, go.transform)
                end
            end
        else
            self:SetHoleSelected(v.transform,false)
        end
    end
end

function CLuaEquipInlayStoneDetailView:OnOpenHoleClicked( go) 
    for i,v in ipairs(self.m_HoleList) do
        self:SetHoleSelected(v.transform,go==v)
        if go == v then
            CLuaEquipMgr.SelectedEquipmentHoleInfo = self.m_HoleInfoList[i]
            CUIManager.ShowUI(CUIResources.MakeEquipHoleWnd)
        end
    end
end

function CLuaEquipInlayStoneDetailView:OnRequestCanSwitchEquipHoleSetReturn(place, pos, equipId, equipNewGrade, canSwitch) 

    if not canSwitch then 
        self.m_SwitchButton:SetActive(true)
        return
    end
    if equipId == self.item.Id then
        self.m_SwitchButton:SetActive(true)
        local itemInfo = CItemMgr.Inst:GetItemInfo(equipId)
        if itemInfo == nil then
            return
        end

        local level = CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.FinalMaxLevelForEquip or 0
        if place == EnumItemPlace.Body and equipNewGrade > level then
            --不能替换
        else
            CLuaEquipMgr.SelectedEquipmentHoleInfo = nil--清空选择
            Gac2Gas.RequestSwitchEquipHoleSet(EnumToInt(itemInfo.place), itemInfo.pos, equipId)
        end
    end
end


function CLuaEquipInlayStoneDetailView:InitHoleItem(transform, status, holeInfo, item, isGemGroup,isReference)
    local holeAvailabel = transform:Find("available"):GetComponent(typeof(UISprite))
    local holeDisable = transform:Find("disable").gameObject
    local iconTexture = transform:Find("icon"):GetComponent(typeof(CUITexture))
    local qualitySprite = transform:GetComponent(typeof(UISprite))
    local selectSprite = transform:Find("Select").gameObject
    local levelLabel = transform:Find("Level"):GetComponent(typeof(UILabel))
    local lockSprite = transform:Find("Lock"):GetComponent(typeof(UISprite))
    local shareMark = transform:Find("ShareMarkSprite").gameObject
    -- shareMark:SetActive(false)

    -- if holeInfo.stoneItemId>0 and holeInfo.stoneItemId<100 then--引用类型
    if isReference then
        shareMark:SetActive(true)
    else
        shareMark:SetActive(false)
    end

    levelLabel.text = ""
    holeAvailabel.enabled = status == LuaEnumHoleStatus.availableWithoutGem
    lockSprite.enabled = status == 4 --status为4表示无法启用
    holeDisable:SetActive(status == LuaEnumHoleStatus.disable)
    local jewel = nil
    if item ~= nil then
        iconTexture:LoadMaterial(item.Icon)
        jewel = Jewel_Jewel.GetData(item.ID)
        levelLabel.text = SafeStringFormat3("lv.%d", jewel.JewelLevel)
    end
    qualitySprite.spriteName = isGemGroup and "common_itemcell_border" or "common_itemcell_border"
    selectSprite:SetActive(false)
end

function CLuaEquipInlayStoneDetailView:SetHoleSelected(transform,select)
    local selectSprite = transform:Find("Select").gameObject
    selectSprite:SetActive(select)
end
