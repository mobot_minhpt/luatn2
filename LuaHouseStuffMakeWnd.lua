local CItemBagMgr=import "L10.Game.CItemBagMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CBaseWnd=import "L10.UI.CBaseWnd"
local MessageWndManager = import "L10.UI.MessageWndManager"
local EventDelegate = import "EventDelegate"
local CZhushabiEvaluateMgr = import "L10.UI.CZhushabiEvaluateMgr"
local CLivingSkillQuickMakeMgr = import "L10.UI.CLivingSkillQuickMakeMgr"
local CLivingSkillMgr = import "L10.Game.CLivingSkillMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CHouseStuffMakeWnd = import "L10.UI.CHouseStuffMakeWnd"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CGetMoneyMgr = import "L10.UI.CGetMoneyMgr"
local CItemCountUpdate=import "L10.UI.CItemCountUpdate"

CLuaHouseStuffMakeWnd = class()
RegistClassMember(CLuaHouseStuffMakeWnd,"closeButton")
RegistClassMember(CLuaHouseStuffMakeWnd,"titleLabel")
RegistClassMember(CLuaHouseStuffMakeWnd,"huoliLabel")
RegistClassMember(CLuaHouseStuffMakeWnd,"curHuoliLabel")
RegistClassMember(CLuaHouseStuffMakeWnd,"multiLabel")
RegistClassMember(CLuaHouseStuffMakeWnd,"icon")
RegistClassMember(CLuaHouseStuffMakeWnd,"inputButton")
RegistClassMember(CLuaHouseStuffMakeWnd,"blueprintIcon")
RegistClassMember(CLuaHouseStuffMakeWnd,"blueprintCostLabel")
RegistClassMember(CLuaHouseStuffMakeWnd,"blueprintCountUpdate")
RegistClassMember(CLuaHouseStuffMakeWnd,"blueprintGetNode")
RegistClassMember(CLuaHouseStuffMakeWnd,"itemTemplate")
RegistClassMember(CLuaHouseStuffMakeWnd,"items")
RegistClassMember(CLuaHouseStuffMakeWnd,"startMakeBtn")
RegistClassMember(CLuaHouseStuffMakeWnd,"ZhuShaBiTf")
RegistClassMember(CLuaHouseStuffMakeWnd,"normalTf")
RegistClassMember(CLuaHouseStuffMakeWnd,"blueprintTf")
RegistClassMember(CLuaHouseStuffMakeWnd,"addDaoJuButton")
RegistClassMember(CLuaHouseStuffMakeWnd,"noDaoJuTf")
RegistClassMember(CLuaHouseStuffMakeWnd,"hasDaoJuTf")
RegistClassMember(CLuaHouseStuffMakeWnd,"costLingqiLabel")
RegistClassMember(CLuaHouseStuffMakeWnd,"allLingqiLabel")
RegistClassMember(CLuaHouseStuffMakeWnd,"daojuCountupdate")
RegistClassMember(CLuaHouseStuffMakeWnd,"daojuIcon")
RegistClassMember(CLuaHouseStuffMakeWnd,"m_ZhushabiItemId")
RegistClassMember(CLuaHouseStuffMakeWnd,"yuanbaoLabel")
RegistClassMember(CLuaHouseStuffMakeWnd,"yuanbaoCostLabel")
RegistClassMember(CLuaHouseStuffMakeWnd,"addYuanbaoBtn")
RegistClassMember(CLuaHouseStuffMakeWnd,"autoCostYuanbaoToggle")
RegistClassMember(CLuaHouseStuffMakeWnd,"yuanbaoNode")
RegistClassMember(CLuaHouseStuffMakeWnd,"itemData")
RegistClassMember(CLuaHouseStuffMakeWnd,"mLingQiCost")

RegistClassMember(CLuaHouseStuffMakeWnd,"m_CostYuanBaoLookup")--朱砂笔制作消耗的元宝
RegistClassMember(CLuaHouseStuffMakeWnd,"m_CostItemCountLookup")--朱砂笔制作消耗的数量

CLuaHouseStuffMakeWnd.autoCostYuanbao = false

function CLuaHouseStuffMakeWnd:Awake()
    self.m_CostYuanBaoLookup = {}
    self.m_CostItemCountLookup = {}

    self.closeButton = self.transform:Find("Wnd_Bg_Secondary_1/CloseButton").gameObject
    self.titleLabel = self.transform:Find("Wnd_Bg_Secondary_1/TitleLabel"):GetComponent(typeof(UILabel))
    self.huoliLabel = self.transform:Find("Anchor/Offset/StartMake/Normal/Huoli/HuoliLabel"):GetComponent(typeof(UILabel))
    self.curHuoliLabel = self.transform:Find("Anchor/Offset/StartMake/Normal/Huoli/AllHuoliLabel"):GetComponent(typeof(UILabel))
    self.multiLabel = self.transform:Find("Anchor/Offset/StartMake/Right/MultiLabel"):GetComponent(typeof(UILabel))
    self.icon = self.transform:Find("Anchor/Offset/StartMake/Right/Generated/Icon"):GetComponent(typeof(CUITexture))
    self.inputButton = self.transform:Find("Anchor/Offset/StartMake/Right/InputButton"):GetComponent(typeof(QnAddSubAndInputButton))
    self.blueprintIcon = self.transform:Find("Anchor/Offset/StartMake/Normal/Blueprint/Icon (1)"):GetComponent(typeof(CUITexture))
    self.blueprintCostLabel = self.transform:Find("Anchor/Offset/StartMake/Normal/Blueprint/BlueprintCostLabel"):GetComponent(typeof(UILabel))
    self.blueprintCountUpdate = self.transform:Find("Anchor/Offset/StartMake/Normal/Blueprint"):GetComponent(typeof(CItemCountUpdate))
    self.blueprintGetNode = self.transform:Find("Anchor/Offset/StartMake/Normal/Blueprint/Icon (1)/GetNode").gameObject
    self.itemTemplate = self.transform:Find("Anchor/Offset/StartMake/Left/Items/ItemTemplate").gameObject
    self.items = {}
    self.startMakeBtn = self.transform:Find("Anchor/Offset/StartMake/StartMakeBtn").gameObject
    self.ZhuShaBiTf = self.transform:Find("Anchor/Offset/ZhuShaBi")
    self.normalTf = self.transform:Find("Anchor/Offset/StartMake/Normal")
    self.blueprintTf = self.transform:Find("Anchor/Offset/StartMake/Normal/Blueprint")

    self.addDaoJuButton = self.transform:Find("Anchor/Offset/ZhuShaBi/Border").gameObject
    self.noDaoJuTf = self.transform:Find("Anchor/Offset/ZhuShaBi/NoDaoJu")
    self.hasDaoJuTf = self.transform:Find("Anchor/Offset/ZhuShaBi/HasDaoJu")
    self.costLingqiLabel = self.transform:Find("Anchor/Offset/ZhuShaBi/LingQi/LingqiLabel"):GetComponent(typeof(UILabel))
    self.allLingqiLabel = self.transform:Find("Anchor/Offset/ZhuShaBi/LingQi/AllLingqiLabel"):GetComponent(typeof(UILabel))
    self.daojuCountupdate = self.transform:Find("Anchor/Offset/ZhuShaBi/HasDaoJu"):GetComponent(typeof(CItemCountUpdate))
    self.daojuIcon = self.transform:Find("Anchor/Offset/ZhuShaBi/HasDaoJu"):GetComponent(typeof(CUITexture))

    self.m_ZhushabiItemId = nil
    self.yuanbaoLabel = self.transform:Find("Anchor/Offset/YuanBaoCost/YuanBaoLabel"):GetComponent(typeof(UILabel))
    self.yuanbaoCostLabel = self.transform:Find("Anchor/Offset/YuanBaoCost/YuanBaoCostLabel"):GetComponent(typeof(UILabel))
    self.addYuanbaoBtn = self.transform:Find("Anchor/Offset/YuanBaoCost/GetMoneyBtn").gameObject
    self.autoCostYuanbaoToggle = self.transform:Find("Anchor/Offset/AutoBuyToggle"):GetComponent(typeof(UIToggle))
    self.yuanbaoNode = self.transform:Find("Anchor/Offset/YuanBaoCost").gameObject
    self.itemData = nil
    self.mLingQiCost = 0

end
function CLuaHouseStuffMakeWnd:Init( )
    if CHouseStuffMakeWnd.IsMakeZhuShaBi then
        Gac2Gas.QueryMyHouseLingqi()
        --useDaoJuToggle.value = false;
        self.costLingqiLabel.text = "0"
        self.allLingqiLabel.text = "0"
        self.noDaoJuTf.gameObject:SetActive(true)
        self.hasDaoJuTf.gameObject:SetActive(false)

        self.inputButton.gameObject:SetActive(false)
        self.multiLabel.gameObject:SetActive(false)
    end

    if CHouseStuffMakeWnd.IsMakeZhuShaBi then
        self.titleLabel.text = LocalString.GetString("传家宝制作")
    else
        self.titleLabel.text = LocalString.GetString("家具制作")
    end

    self:UpdateContent()
    if CHouseStuffMakeWnd.IsMakeZhuShaBi then
        self.ZhuShaBiTf.gameObject:SetActive(true)
        self.normalTf.gameObject:SetActive(false)
    else
        self.ZhuShaBiTf.gameObject:SetActive(false)
        self.normalTf.gameObject:SetActive(true)
    end

    UIEventListener.Get(self.addDaoJuButton).onClick = DelegateFactory.VoidDelegate(function (p) 
        CUIManager.ShowUI(CUIResources.HouseStuffMakeUseWnd)
    end)

    UIEventListener.Get(self.addYuanbaoBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        CGetMoneyMgr.Inst:GetYuanBao()
    end)
    -- < MakeDelegateFromCSFunction >(self.OnClickAddYuanbaoBtn, VoidDelegate, self)
    self:InitYuanbaoLogic()

    UIEventListener.Get(self.closeButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnClose(go)
    end)
    -- < MakeDelegateFromCSFunction >(self.OnClose, VoidDelegate, self)
    UIEventListener.Get(self.startMakeBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnClickStartMakeBtn(go)
    end)
end

function CLuaHouseStuffMakeWnd:OnClose( go) 
    if self.m_ZhushabiItemId and self.m_ZhushabiItemId~="" then
        local msg = g_MessageMgr:FormatMessage("MakeZhushabiSuccessTip")
        MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function () 
            CZhushabiEvaluateMgr.ShowEvaluateWnd(self.m_ZhushabiItemId)
        end), nil, LocalString.GetString("前往鉴定"), nil, false)
    end

    self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end
function CLuaHouseStuffMakeWnd:OnEnable( )
    g_ScriptEvent:AddListener("MainPlayerPlayPropUpdate", self, "UpdateHuoli")
    g_ScriptEvent:AddListener("QueryHouseLingqiResult", self, "OnQueryHouseLingqiResult")
    g_ScriptEvent:AddListener("SetZhuShaBiUpgradeMat", self, "OnSetZhuShaBiUpgradeMat")
    g_ScriptEvent:AddListener("MainPlayerUpdateMoney", self, "UpdateMoneyInfo")
    g_ScriptEvent:AddListener("PlayerMakeZhushabiSuccess", self, "OnPlayerMakeZhushabiSuccess")
    g_ScriptEvent:AddListener("PreSellMarketItemDone",self,"OnPreSellDone")
end
function CLuaHouseStuffMakeWnd:OnDisable( )
    g_ScriptEvent:RemoveListener("MainPlayerPlayPropUpdate", self, "UpdateHuoli")
    g_ScriptEvent:RemoveListener("QueryHouseLingqiResult", self, "OnQueryHouseLingqiResult")
    g_ScriptEvent:RemoveListener("SetZhuShaBiUpgradeMat", self, "OnSetZhuShaBiUpgradeMat")
    g_ScriptEvent:RemoveListener("MainPlayerUpdateMoney", self, "UpdateMoneyInfo")
    g_ScriptEvent:RemoveListener("PlayerMakeZhushabiSuccess", self, "OnPlayerMakeZhushabiSuccess")
    g_ScriptEvent:RemoveListener("PreSellMarketItemDone",self,"OnPreSellDone")
end

function CLuaHouseStuffMakeWnd:OnPreSellDone(args)
    local itemId, maxCount, price = args[0],args[1],args[2]
    if self.m_CostYuanBaoLookup[itemId] then
        self.m_CostYuanBaoLookup[itemId] = price
        --刷新
        self:UpdateYuanbaoDisplay()
    end
end

function CLuaHouseStuffMakeWnd:OnPlayerMakeZhushabiSuccess(args)
    local itemId=args[0]
    self.m_ZhushabiItemId = itemId
end

function CLuaHouseStuffMakeWnd:UpdateMoneyInfo()
    if CClientMainPlayer.Inst then
        self.yuanbaoLabel.text = tostring(CClientMainPlayer.Inst.YuanBao)
    end
end

function CLuaHouseStuffMakeWnd:UpdateHuoli()
    if CClientMainPlayer.Inst then
        local curHuoli = CClientMainPlayer.Inst.PlayProp.HuoLi
        local HuoliCost = self.itemData and self.itemData.HuoLi or 0
        self.curHuoliLabel.text = curHuoli > HuoliCost and tostring(curHuoli) or SafeStringFormat3("[c][ff0000]%d[-][/c]", curHuoli)
    end
end

function CLuaHouseStuffMakeWnd:InitYuanbaoLogic( )
    if CHouseStuffMakeWnd.IsMakeZhuShaBi then
        self.autoCostYuanbaoToggle.gameObject:SetActive(true)

        self.autoCostYuanbaoToggle.value = CLuaHouseStuffMakeWnd.autoCostYuanbao
        EventDelegate.Add(self.autoCostYuanbaoToggle.onChange,DelegateFactory.Callback(function () 
            CLuaHouseStuffMakeWnd.autoCostYuanbao = self.autoCostYuanbaoToggle.value
            self:UpdateYuanbaoDisplay()
        end))

        self:UpdateYuanbaoDisplay()
        self:UpdateMoneyInfo()
    else
        --隐藏
        self.yuanbaoNode:SetActive(false)
        self.autoCostYuanbaoToggle.gameObject:SetActive(false)
    end
end
function CLuaHouseStuffMakeWnd:UpdateYuanbaoDisplay( )
    if CLuaHouseStuffMakeWnd.autoCostYuanbao then
        local cost = 0
        for k,v in pairs(self.m_CostYuanBaoLookup) do
            local a,b = CItemMgr.Inst:CalcItemCountInBagByTemplateId(k)
            local c,d = CItemBagMgr.Inst:GetItemCountByTemplateIdInItemBag(k)--考虑itembag中的数量
            local c = a+b+c+d--拥有的数量
            if c>=self.m_CostItemCountLookup[k] then--够了
                c=0
            else
                c=self.m_CostItemCountLookup[k]-c--还需要这么多
            end
            cost = cost + v*c
        end
        if cost > 0 then
            self.yuanbaoCostLabel.text = tostring(cost)
            self.yuanbaoNode.gameObject:SetActive(true)
        else
            --材料够 不需要元宝
            self.yuanbaoNode.gameObject:SetActive(false)
        end
    else
        self.yuanbaoNode.gameObject:SetActive(false)
    end
end
function CLuaHouseStuffMakeWnd:OnSetZhuShaBiUpgradeMat( matId) 
    if CHouseStuffMakeWnd.IsMakeZhuShaBi then
        --addDaoJuButton.SetActive(false);
        self:UpdateZhuShaBiMat()
        --useDaoJuToggle.value = true;
    end
end
function CLuaHouseStuffMakeWnd:OnQueryHouseLingqiResult( args ) 
    local houseId, lingqi=args[0],args[1]
    if CClientMainPlayer.Inst == nil then
        return
    end
    if houseId == CClientMainPlayer.Inst.ItemProp.HouseId then
        local val = math.floor(lingqi)
        if self.mLingQiCost <= lingqi then
            --灵气够了
            self.allLingqiLabel.text = tostring(val)
        else
            self.allLingqiLabel.text = SafeStringFormat3("[c][ff0000]%d[-][/c]", val)
        end
    end
end
function CLuaHouseStuffMakeWnd:UpdateContent( )
    self.itemData = LifeSkill_Item.GetData(CHouseStuffMakeWnd.TemplateId)
    self.multiLabel.text = SafeStringFormat3("X%d", self.itemData.GroupNum)

    local template = CItemMgr.Inst:GetItemTemplate(self.itemData.ID)
    if template ~= nil then
        self.icon:LoadMaterial(template.Icon)
    else
        self.icon.material = nil
    end

    self.huoliLabel.text = tostring(self.itemData.HuoLi)
    self.curHuoliLabel.text = tostring(CClientMainPlayer.Inst.PlayProp.HuoLi)

    self:InitConsumeItem(self.itemData)
    if CHouseStuffMakeWnd.IsMakeZhuShaBi then
        self:InitZhuShaBi(self.itemData)
    else
        self:InitBlueprint(self.itemData)--图纸
    end

    self:UpdateMinMax()
    self.inputButton.onValueChanged = DelegateFactory.Action_uint(function(val)
        self:UpdateCost(val)
    end)
    -- CommonDefs.CombineListner_Action_uint(self.inputButton.onValueChanged, < MakeDelegateFromCSFunction >(self.UpdateCost, MakeGenericClass(Action1, UInt32), self), true)
    self.inputButton:SetValue(1, true)
end
function CLuaHouseStuffMakeWnd:InitBlueprint( itemData) 
    local template = CItemMgr.Inst:GetItemTemplate(itemData.DrawingItem)
    if template == nil or itemData.DrawingItem == 0 then
        self.blueprintTf.gameObject:SetActive(false)
        self.blueprintIcon:Clear()
        return
    end

    self.blueprintTf.gameObject:SetActive(true)

    self.blueprintIcon:LoadMaterial(template.Icon)
    self.blueprintCountUpdate.templateId = itemData.DrawingItem
    self.blueprintCountUpdate.onChange = DelegateFactory.Action_int(function(val)
        -- self:OnBlueprintChange(val)
        self.blueprintCountUpdate.format = val > 0 and "{0}" or "[c][ff0000]{0}[-][/c]";
        self.blueprintGetNode.SetActive(val == 0)
    end)
    
    -- < MakeDelegateFromCSFunction >(self.OnBlueprintChange, MakeGenericClass(Action1, Int32), self)
    UIEventListener.Get(self.blueprintIcon.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        if itemData then
            CItemInfoMgr.ShowLinkItemTemplateInfo(itemData.DrawingItem,false, nil, AlignType.Default, 0, 0, 0, 0)
        end
    end)

    UIEventListener.Get(self.blueprintGetNode).onClick = DelegateFactory.VoidDelegate(function(go)
        -- self:OnClickGetBlueprint(go)
        if self.itemData ~= nil then
            CLivingSkillQuickMakeMgr.Inst:ShowMakeTipWnd(self.itemData.DrawingItem)
        end
    end)
    -- < MakeDelegateFromCSFunction >(self.OnClickGetBlueprint, VoidDelegate, self)

    self.blueprintCountUpdate:UpdateCount()
    self.blueprintCostLabel.text = "1"
end
-- function CLuaHouseStuffMakeWnd:OnClickGetBlueprint( go) 
--     if self.itemData ~= nil then
--         CLivingSkillQuickMakeMgr.Inst:ShowMakeTipWnd(self.itemData.DrawingItem)
--     end
-- end
function CLuaHouseStuffMakeWnd:InitZhuShaBi( itemData) 
    self.mLingQiCost = itemData.LingQi
    self.costLingqiLabel.text = tostring(itemData.LingQi)

    self:UpdateZhuShaBiMat()
end
function CLuaHouseStuffMakeWnd:UpdateZhuShaBiMat( )
    if CHouseStuffMakeWnd.ZhuShaBiUpgradeMat > 0 then
        local item = Item_Item.GetData(CHouseStuffMakeWnd.ZhuShaBiUpgradeMat)
        if item ~= nil then
            self.daojuIcon:LoadMaterial(item.Icon)
        end
        self.hasDaoJuTf.gameObject:SetActive(true)
        self.noDaoJuTf.gameObject:SetActive(false)
        self.daojuCountupdate.templateId = CHouseStuffMakeWnd.ZhuShaBiUpgradeMat
        self.daojuCountupdate.onChange =  DelegateFactory.Action_int(function(val)
            self:OnZhuShaBiMatChange(val)
        end)
        -- < MakeDelegateFromCSFunction >(self.OnZhuShaBiMatChange, MakeGenericClass(Action1, Int32), self)
        self.daojuCountupdate:UpdateCount()
    end
end
function CLuaHouseStuffMakeWnd:OnZhuShaBiMatChange( val) 
    if val == 0 then
        CHouseStuffMakeWnd.ZhuShaBiUpgradeMat = 0
        self.daojuCountupdate.templateId = 0
        --daojuCountupdate.gameObject.SetActive(false);
        self.hasDaoJuTf.gameObject:SetActive(false)
        self.noDaoJuTf.gameObject:SetActive(true)

        self.daojuCountupdate.onChange = nil
        self.daojuIcon:Clear()
    end
end
function CLuaHouseStuffMakeWnd:UpdateMinMax( )
    local max = self:GetMaxCanBuyCount()
    if max == 0 then
        max = 1
    end
    self.inputButton:SetMinMax(1, max, 1)
    if self.inputButton:GetValue() > max then
        self.inputButton:SetValue(max, true)
    end
end
function CLuaHouseStuffMakeWnd:GetMaxCanBuyCount( )--计算最多可以
    if CHouseStuffMakeWnd.IsMakeZhuShaBi then
        return 1
    else
        local rtn = 999

        for i,v in ipairs(self.items) do
            local countUpdate = v:GetComponent(typeof(CItemCountUpdate))
            if self.m_CostItemCountLookup[countUpdate.templateId] then
                local c = math.floor(countUpdate.count/self.m_CostItemCountLookup[countUpdate.templateId])
                if c<rtn then
                    rtn = c
                end
            end
        end

        if self.itemData.DrawingItem > 0 then
            --查看图纸数量是不是够了
            rtn = math.min(self.blueprintCountUpdate.count, rtn)
        end
        return rtn
    end
end
function CLuaHouseStuffMakeWnd:InitConsumeItem( data) 
    if data.ConsumeItems == nil then
        self.itemTemplate:SetActive(false)
        return
    end
    local positions = {}
    local count = data.ConsumeItems.Length
    if count == 1 then
        positions = { Vector3(0, 45, 0) }
        self.items = {self.itemTemplate}
    elseif count == 2 then
        positions = { Vector3(-75, 45, 0), Vector3(75, 45, 0) }
        self.items = {self.itemTemplate}
        table.insert( self.items, NGUITools.AddChild(self.itemTemplate.transform.parent.gameObject, self.itemTemplate))
    elseif count == 3 then
        positions= { Vector3(0, 120, 0), Vector3(-75, -30, 0), Vector3(75, -30, 0) }
        self.items = {self.itemTemplate}
        for i=1,2 do
            table.insert( self.items, NGUITools.AddChild(self.itemTemplate.transform.parent.gameObject, self.itemTemplate))
        end
    elseif count == 4 then
        positions = { Vector3(-75, 120, 0), Vector3(75, 120, 0), Vector3(-75, -30, 0), Vector3(75, -30, 0) }
        self.items = {self.itemTemplate}
        for i=1,3 do
            table.insert( self.items, NGUITools.AddChild(self.itemTemplate.transform.parent.gameObject, self.itemTemplate))
        end
    elseif count == 5 then
        positions= { Vector3(-140, 120, 0), Vector3(0, 120, 0), Vector3(140, 120, 0), Vector3(-75, -30, 0), Vector3(75, -30, 0) }
        self.items = {self.itemTemplate}
        for i=1,4 do
            table.insert( self.items, NGUITools.AddChild(self.itemTemplate.transform.parent.gameObject, self.itemTemplate))
        end
    elseif count == 6 then
        positions= { Vector3(-140, 120, 0), Vector3(0, 120, 0), Vector3(140, 120, 0), Vector3(-140, -30, 0), Vector3(0, -30, 0), Vector3(140, -30, 0) }
        self.items = {self.itemTemplate}
        for i=1,5 do
            table.insert( self.items, NGUITools.AddChild(self.itemTemplate.transform.parent.gameObject, self.itemTemplate))
        end
    end

    for i,v in ipairs(self.items) do
        local designData = data.ConsumeItems[i-1]
        self.m_CostYuanBaoLookup[designData.ID] = 0
        self.m_CostItemCountLookup[designData.ID] = designData.count
    end
    for i,v in ipairs(self.items) do
        local designData = data.ConsumeItems[i-1]
        self:InitLivingConsumeItem(v,designData.ID, designData.count)
        v.transform.localPosition = positions[i]


    end
end
-- function CLuaHouseStuffMakeWnd:OnChangeAction( cmp) 
--     self:UpdateMinMax()
--     local num = self.inputButton:GetValue()
--     if num == 0 then
--         num = 1
--     end
--     cmp:SetMakeCount(num)
-- end
function CLuaHouseStuffMakeWnd:UpdateCost( count) 
    self.huoliLabel.text = tostring((self.itemData.HuoLi * count))
    if count > 0 then
        CUICommonDef.SetActive(self.startMakeBtn, true, true)
    else
        CUICommonDef.SetActive(self.startMakeBtn, false, true)
    end

    --再检查一下等级
    if self.itemData.SkillLev > CLivingSkillMgr.Inst:GetSkillLevel(CLivingSkillMgr.Inst.selectedSkill) then
        CUICommonDef.SetActive(self.startMakeBtn, false, true)
    end

    local makeCount = self.inputButton:GetValue()
    if makeCount == 0 then
        makeCount = 1
    end
    -- CommonDefs.EnumerableIterate(self.items, DelegateFactory.Action_object(function (___value) 
    --     item = ___value
    --     item:SetMakeCount(makeCount)
    -- end))
    for i,v in ipairs(self.items) do
        
    end
end
function CLuaHouseStuffMakeWnd:OnClickStartMakeBtn( go) 
    if CHouseStuffMakeWnd.IsMakeZhuShaBi then
        if CHouseStuffMakeWnd.ZhuShaBiUpgradeMat == 0 then
            g_MessageMgr:ShowMessage("ZHUSHABI_MAKE_NO_MAT")
            return
        end

        local useNormalMat = false
        LifeSkill_ZhuShaBiNormalMat.Foreach(function (key, data) 
            if key == CHouseStuffMakeWnd.TemplateId then
                if CHouseStuffMakeWnd.ZhuShaBiUpgradeMat == data.Item then
                    useNormalMat = true
                end
            end
        end)

        local option = CHouseStuffMakeWnd.IsMakeZhuShaBi and CLuaHouseStuffMakeWnd.autoCostYuanbao or false
        if useNormalMat then
            Gac2Gas.PlayerRequestMakeZhushabi(EnumChuanjiabaoKind_lua.Simple, CHouseStuffMakeWnd.TemplateId, option)
        else
            local grade = 1
            if CHouseStuffMakeWnd.ZhuShaBiUpgradeMat > 0 then
                grade = CLivingSkillMgr.Inst:GetZhuShaBiUpgradeLevel(CHouseStuffMakeWnd.ZhuShaBiUpgradeMat)
            end
            Gac2Gas.PlayerRequestMakeZhushabi(grade, CHouseStuffMakeWnd.TemplateId, option)
        end
    else
        local data = LifeSkill_Item.GetData(CHouseStuffMakeWnd.TemplateId)
        if data ~= nil then
            --有次数
            local count = self.inputButton:GetValue()
            CLivingSkillMgr.Inst:RequestUseLifeSkill(data.ID, count)
        end
    end
end


function CLuaHouseStuffMakeWnd:InitLivingConsumeItem(go,templateId, needCount, cost)
    local transform = go.transform
    local icon = transform:GetComponent(typeof(CUITexture))
    local countUpdate = transform:GetComponent(typeof(CItemCountUpdate))
    local getNode = transform:Find("GetNode").gameObject

    -- local unitCost = needCount
    -- local mPrice = 0

    countUpdate.templateId = templateId

    countUpdate.format = "{0}/" .. tostring(needCount)

    countUpdate.onChange = DelegateFactory.Action_int(function(val)
        self:UpdateMinMax()
        local num = self.inputButton:GetValue()
        if num == 0 then
            num = 1
        end
        local needCount = num*needCount
        if needCount <= val then
            countUpdate.format = "{0}/" .. tostring(needCount)
            getNode:SetActive(false)
        else
            countUpdate.format = "[ff0000]{0}[-]/" .. tostring(needCount)
            getNode:SetActive(true)
        end
    
        if CHouseStuffMakeWnd.IsMakeZhuShaBi then
            self:UpdateYuanbaoDisplay()
        end
    end)
    countUpdate:UpdateCount()

    UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function(g)
        if countUpdate.count >= needCount then
            CItemInfoMgr.ShowLinkItemTemplateInfo(templateId, false, nil, AlignType.Default, 0, 0, 0, 0)
        else
            CLivingSkillQuickMakeMgr.Inst:ShowMakeTipWnd(templateId)
        end
    end)

    local template = CItemMgr.Inst:GetItemTemplate(templateId)
    icon:LoadMaterial(template.Icon)

    --制作朱砂笔需要请求元宝消耗
    if CHouseStuffMakeWnd.IsMakeZhuShaBi then
        Gac2Gas.PreSellMarketItem(templateId, 1)
        -- self:InvokeRepeating("RequestPrice", 15, 15)
    end
end
