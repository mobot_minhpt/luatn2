local CUITexture = import "L10.UI.CUITexture"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local Extensions = import "Extensions"
local CButton = import "L10.UI.CButton"
local EnumGender = import "L10.Game.EnumGender"
local UISprite = import "UISprite"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local CCustomSelector = import "L10.UI.CCustomSelector"
local CPopupMenuInfoMgrAlignType = import "L10.UI.CPopupMenuInfoMgr+AlignType"

LuaAppearanceFashionSubview = class()

RegistChildComponent(LuaAppearanceFashionSubview,"m_FashionBackPendantItem", "CommonItemCell", GameObject)
RegistChildComponent(LuaAppearanceFashionSubview,"m_FashionItem", "FashionItem", GameObject)
RegistChildComponent(LuaAppearanceFashionSubview,"m_ItemDisplay", "AppearanceCommonFashionDisplayView", CCommonLuaScript)
RegistChildComponent(LuaAppearanceFashionSubview,"m_ContentGrid", "ContentGrid", UIGrid)
RegistChildComponent(LuaAppearanceFashionSubview,"m_FashionContentGrid", "FashionContentGrid", UIGrid)
RegistChildComponent(LuaAppearanceFashionSubview,"m_ContentScrollView", "ContentScrollView", UIScrollView)
RegistChildComponent(LuaAppearanceFashionSubview, "m_AppearanceFashionTabBar", "AppearanceFashionTabBar", CCommonLuaScript)
RegistChildComponent(LuaAppearanceFashionSubview, "m_SortSelector", "SortSelector", CCustomSelector)

RegistClassMember(LuaAppearanceFashionSubview, "m_SortNames")
RegistClassMember(LuaAppearanceFashionSubview, "m_SortIndex")
RegistClassMember(LuaAppearanceFashionSubview, "m_AllFashions")
RegistClassMember(LuaAppearanceFashionSubview, "m_SelectedDataId")
RegistClassMember(LuaAppearanceFashionSubview, "m_MultiColorFashionData")
RegistClassMember(LuaAppearanceFashionSubview, "m_PreviewType")

function LuaAppearanceFashionSubview:Awake()
end

function LuaAppearanceFashionSubview:InitSortSelector()
    self.m_SortSelector.gameObject:SetActive(true)
    self.m_SortNames = LuaAppearancePreviewMgr:GetAllFashionInfoSortNames()
    local names = Table2List(self.m_SortNames, MakeGenericClass(List,cs_string))
    self.m_SortSelector:Init(names, nil, DelegateFactory.Action_int(function ( index )
        self.m_SortIndex = index + 1
		self.m_SortSelector:SetName(names[index])
        self:LoadData()
    end))
    self.m_SortIndex = 1
    self.m_SortSelector:SetName(self.m_SortNames[self.m_SortIndex])
end

function LuaAppearanceFashionSubview:Init(type)
    self.m_PreviewType = type
    self:InitSortSelector()
    self:SetDefaultSelection()
    self:LoadData()
end

function LuaAppearanceFashionSubview:GetItemTemplate()
    return self.m_PreviewType == EnumAppearanceFashionPosition.eBeiShi and self.m_FashionBackPendantItem or self.m_FashionItem
end

function LuaAppearanceFashionSubview:GetContentGrid()
    return self.m_PreviewType == EnumAppearanceFashionPosition.eBeiShi and self.m_ContentGrid or self.m_FashionContentGrid
end

function LuaAppearanceFashionSubview:LoadData()
    self.m_AllFashions = LuaAppearancePreviewMgr:GetAllFashionInfo(self.m_PreviewType, self.m_SortIndex)
    local grid = self:GetContentGrid()
    Extensions.RemoveAllChildren(grid.transform)
    grid.gameObject:SetActive(true)

    for i = 1, #self.m_AllFashions do
        local child = CUICommonDef.AddChild(grid.gameObject, self:GetItemTemplate())
        child:SetActive(true)
        self:InitItem(child, self.m_AllFashions[i])
    end
   
    grid:Reposition()
    self.m_ContentScrollView:ResetPosition()
    self:InitSelection()
end

function LuaAppearanceFashionSubview:SetDefaultSelection()
    local previewId = self:GetPreviewFashion()
    local currentInUseId = LuaAppearancePreviewMgr:IsShowFashion(self.m_PreviewType, false) and LuaAppearancePreviewMgr:GetCurrentInUseFashion(self.m_PreviewType) or 0
    self.m_SelectedDataId = previewId>0 and previewId or currentInUseId
end

function LuaAppearanceFashionSubview:InitSelection()
    if self.m_SelectedDataId == 0 then
        self:OnItemClick(nil)
        return
    end
    local grid = self:GetContentGrid()
    local childCount = grid.transform.childCount
    for i=0, childCount-1 do
        local childGo = grid.transform:GetChild(i).gameObject
        local appearanceData = self.m_AllFashions[i+1]
        if appearanceData and self:IsSelectedFashionOrGroup(self.m_SelectedDataId, appearanceData) then
            self:OnItemClick(childGo)
            break
        end
    end
end

function LuaAppearanceFashionSubview:GetGroupIndexById(fashionId, appearanceData)
    if fashionId and appearanceData and #appearanceData.fashionTbl>0 then
        for i=1,#appearanceData.fashionTbl do
            if fashionId==appearanceData.fashionTbl[i].id then
                return i 
            end
        end
    end
    return -1
end

function LuaAppearanceFashionSubview:IsSelectedFashionOrGroup(fashionId, appearanceData)
    return fashionId and appearanceData and ((fashionId==appearanceData.id) or self:GetGroupIndexById(fashionId, appearanceData)>0)
end

--对于多色的时装,依次选择预览款/已装备/第一个已解锁/收集度最大的一个/第一个已获得/第一个
function LuaAppearanceFashionSubview:GetDefaultSelectedFashion(appearanceData)
    if #appearanceData.fashionTbl>0 then
        local firstId = appearanceData.fashionTbl[1].id
        local usingId = 0
        local firstOwnedId = 0
        local firstUnlockedId = 0
        local maxProgressId = 0
        local progress = 0
        for __,data in pairs(appearanceData.fashionTbl) do
            if data.id == self:GetPreviewFashion() then
                return data
            elseif data.isInUse then
                usingId = data.id
            elseif data.isOwned and firstOwnedId==0 then
                firstOwnedId = data.id
            end
            local curProgress, unlocked = LuaAppearancePreviewMgr:GetFashionUnlockProgressInfo(data.id)
            if unlocked and firstUnlockedId==0 then
                firstUnlockedId = data.id
            end
            if data.durability>0 and curProgress>progress and not unlocked then
                 progress = curProgress
                 maxProgressId = data.id
            end
        end
        local targetId = 0
        if usingId>0 then
            targetId = usingId
        elseif firstUnlockedId>0 then
            targetId = firstUnlockedId
        elseif maxProgressId>0 then
            targetId = maxProgressId
        elseif firstOwnedId>0 then
            targetId = firstOwnedId
        else
            targetId = firstId
        end

        local defaultSelectedIdx = self:GetGroupIndexById(targetId,appearanceData)
        if defaultSelectedIdx<1 then defaultSelectedIdx = 1 end
        return appearanceData.fashionTbl[defaultSelectedIdx]
    end
    return appearanceData
end

--判断是否存在已经解锁并可用的时装
function LuaAppearanceFashionSubview:AvaliableFashionExists(appearanceData)
    if #appearanceData.fashionTbl>0 then
        for __,data in pairs(appearanceData.fashionTbl) do
            if LuaAppearancePreviewMgr:IsMainPlayerFashionAvailable(data.id) then
                return true
            end
        end
    else
        return LuaAppearancePreviewMgr:IsMainPlayerFashionAvailable(appearanceData.id)
    end
    return false
end
--对于一组多色时装，获取其中最大的进度信息，如果有解锁的，获取解锁信息
function LuaAppearanceFashionSubview:GetMaxFashionUnlockInfo(appearanceData)
    local progress, unlocked = 0, false
    if appearanceData.fashionTbl==nil or #appearanceData.fashionTbl==0 then
        progress, unlocked = LuaAppearancePreviewMgr:GetFashionUnlockProgressInfo(appearanceData.id) 
    else
        for __,val in pairs(appearanceData.fashionTbl) do
            local curProgress, curUnlocked = LuaAppearancePreviewMgr:GetFashionUnlockProgressInfo(val.id)
            if curUnlocked then
                unlocked = true 
            elseif curProgress>progress then
                progress = curProgress
            end
        end
    end
    return progress, unlocked
end

function LuaAppearanceFashionSubview:InitBackPendantItem(itemGo, appearanceData)
    local iconTexture = itemGo.transform:GetComponent(typeof(CUITexture))
    local borderSprite = itemGo.transform:Find("Border"):GetComponent(typeof(UISprite))
    local disabledGo = itemGo.transform:Find("Disabled").gameObject
    local cornerGo = itemGo.transform:Find("Corner").gameObject
    local selectedGo = itemGo.transform:Find("Selected").gameObject
    local tryOnGo = itemGo.transform:Find("TryOnIcon").gameObject

    appearanceData = self:GetDefaultSelectedFashion(appearanceData)
   
    iconTexture:LoadMaterial(appearanceData.bigIcon)
    borderSprite.spriteName = LuaAppearancePreviewMgr:GetFashionQualityBorder(appearanceData.quality)
    cornerGo:SetActive(LuaAppearancePreviewMgr:IsCurrentInUseFashion(self.m_PreviewType, appearanceData.id))
    disabledGo:SetActive(not self:AvaliableFashionExists(appearanceData))
    selectedGo:SetActive(self.m_SelectedDataId and self.m_SelectedDataId==appearanceData.id or false)
    tryOnGo:SetActive(self:IsPreviewFashion(appearanceData.id))

    UIEventListener.Get(itemGo).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnItemClick(go, true)
    end)
end

function LuaAppearanceFashionSubview:GetPreviewFashion()
    if self.m_PreviewType == EnumAppearanceFashionPosition.eHead then
        return LuaAppearancePreviewMgr.m_PreviewHeadFashionId
    elseif self.m_PreviewType == EnumAppearanceFashionPosition.eBody then
        return LuaAppearancePreviewMgr.m_PreviewBodyFashionId
    elseif self.m_PreviewType == EnumAppearanceFashionPosition.eBeiShi then
        return LuaAppearancePreviewMgr.m_PreviewBackPendantFashionId
    else
        return false
    end
end

function LuaAppearanceFashionSubview:IsPreviewFashion(id)
    if self.m_PreviewType == EnumAppearanceFashionPosition.eHead then
        return LuaAppearancePreviewMgr.m_PreviewHeadFashionId == id
    elseif self.m_PreviewType == EnumAppearanceFashionPosition.eBody then
        return LuaAppearancePreviewMgr.m_PreviewBodyFashionId == id
    elseif self.m_PreviewType == EnumAppearanceFashionPosition.eBeiShi then
        return LuaAppearancePreviewMgr.m_PreviewBackPendantFashionId == id
    else
        return false
    end
end


function LuaAppearanceFashionSubview:InitItem(itemGo, appearanceData)
    if self.m_PreviewType==EnumAppearanceFashionPosition.eBeiShi then
        self:InitBackPendantItem(itemGo, appearanceData)
        return
    end
    local iconTexture = itemGo.transform:GetComponent(typeof(CUITexture))
    local borderSprite = itemGo.transform:Find("Border"):GetComponent(typeof(UISprite))
    local cornerGo = itemGo.transform:Find("Corner").gameObject
    local cornerNewGo = itemGo.transform:Find("CornerNew").gameObject
    local disableGo = itemGo.transform:Find("Disabled").gameObject
    local selectedGo = itemGo.transform:Find("Selected").gameObject
    local grid = itemGo.transform:Find("Grid"):GetComponent(typeof(UIGrid))
    local expressionGo = itemGo.transform:Find("Grid/ExpressionIcon").gameObject
    local multipleStyleGo = itemGo.transform:Find("Grid/MultipleStyleIcon").gameObject
    local transformGo = itemGo.transform:Find("Grid/TransformIcon").gameObject
    local progressSprite = itemGo.transform:Find("Progress"):GetComponent(typeof(UIWidget))
    local tryOnGo = itemGo.transform:Find("TryOnIcon").gameObject

    local icon = appearanceData.bigIcon --使用默认的图标，不需要选择当前选中的那个
    appearanceData = self:GetDefaultSelectedFashion(appearanceData)
   
    iconTexture:LoadMaterial(icon)
    borderSprite.spriteName = LuaAppearancePreviewMgr:GetFashionQualityBorder(appearanceData.quality)
    cornerGo:SetActive(appearanceData.isInUse)
    cornerNewGo:SetActive(appearanceData.bShowNewDisplay and not appearanceData.isInUse)
    disableGo:SetActive(not self:AvaliableFashionExists(appearanceData))
    selectedGo:SetActive(self.m_SelectedDataId and self.m_SelectedDataId==appearanceData.id or false)
    expressionGo:SetActive(appearanceData.specialExpression>0)
    multipleStyleGo:SetActive(appearanceData.multicolor>0)
    transformGo:SetActive(appearanceData.transformFemale>0)
    grid:Reposition()
    local progress, unlocked = self:GetMaxFashionUnlockInfo(appearanceData)
    progressSprite.gameObject:SetActive(appearanceData.durability > 0 and not unlocked)
    if progressSprite.gameObject.activeSelf then
        progressSprite.fillAmount=progress/appearanceData.durability
    end
    tryOnGo:SetActive(self:IsPreviewFashion(appearanceData.id))

    UIEventListener.Get(itemGo).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnItemClick(go, true)
    end)
end

function LuaAppearanceFashionSubview:OnItemClick(go)
    self.m_AppearanceFashionTabBar.gameObject:SetActive(false)
    self.m_MultiColorFashionData = nil
    local grid = self:GetContentGrid()
    local childCount =grid.transform.childCount
    for i=0, childCount-1 do
        local childGo = grid.transform:GetChild(i).gameObject
        local tryOnGo = childGo.transform:Find("TryOnIcon").gameObject
        if childGo == go then
            childGo.transform:Find("Selected").gameObject:SetActive(true)
            tryOnGo:SetActive(true)
            if self.m_AllFashions[i+1].multicolor>0 then
                self.m_MultiColorFashionData = self.m_AllFashions[i+1]
            else
                self.m_SelectedDataId = self.m_AllFashions[i+1].id
            end
        else
            childGo.transform:Find("Selected").gameObject:SetActive(false)
            tryOnGo:SetActive(false)
        end
    end
    if self.m_MultiColorFashionData then
        local defaultSelectData = self:GetDefaultSelectedFashion(self.m_MultiColorFashionData)
        local defaultSelectedIdx = self:GetGroupIndexById(defaultSelectData.id,self.m_MultiColorFashionData)
        if defaultSelectedIdx<1 then defaultSelectedIdx = 1 end
        self.m_SelectedDataId = self.m_MultiColorFashionData.fashionTbl[defaultSelectedIdx].id
        self.m_AppearanceFashionTabBar.gameObject:SetActive(true)
        self.m_AppearanceFashionTabBar:Init(self.m_MultiColorFashionData.fashionTbl, function(go, index) self:OnSpecialAppearTabChange(go, index) end)
        self.m_AppearanceFashionTabBar:ChangeTab(defaultSelectedIdx-1, false)      
    else
        self:UpdateItemDisplay()
    end
end

function LuaAppearanceFashionSubview:OnSpecialAppearTabChange(go, index)
    self.m_SelectedDataId = self.m_MultiColorFashionData.fashionTbl[index+1].id
    -- if not LuaAppearancePreviewMgr:IsShowTalismanAppear() then
    --     g_MessageMgr:ShowMessage("Appearance_Preview_Hide_Talisman_Appearance_Tip")
    -- end
    self:UpdateItemDisplay()
end

function LuaAppearanceFashionSubview:UpdateItemDisplay()
    g_ScriptEvent:BroadcastInLua("RequestPreviewMainPlayerFashion", self.m_PreviewType, self.m_SelectedDataId)
    self.m_ItemDisplay.gameObject:SetActive(true)
    local id = self.m_SelectedDataId and self.m_SelectedDataId or 0
    if id==0 then
        self.m_ItemDisplay:Clear()
        return
    end
    local appearanceData = self:GetSelectedAppearanceData()
    if id>0 then
        LuaAppearancePreviewMgr:CheckAndShowHideFashionTip(self.m_PreviewType)
    end
    local icon = appearanceData.icon
    local disabled = true
    local locked = true
    local buttonTbl = {}
    local qualityBgColor = LuaAppearancePreviewMgr:GetFashionQualityColor(appearanceData.quality)
    local qualityText = SafeStringFormat3(LocalString.GetString("%s 风尚度+%d"), appearanceData.qualityName, appearanceData.fashionability)
    local available = LuaAppearancePreviewMgr:IsMainPlayerFashionAvailable(appearanceData.id)
    local inUse = LuaAppearancePreviewMgr:IsCurrentInUseFashion(self.m_PreviewType, appearanceData.id)
    local progress, unlocked = LuaAppearancePreviewMgr:GetFashionUnlockProgressInfo(appearanceData.id) 
    local bShowProgress = appearanceData.durability > 0 and not unlocked --可解锁永久并且尚未解锁
    local curVal = 0
    local needVal = 1
    if bShowProgress then
        curVal = progress
        needVal = appearanceData.durability
    end
    disabled = not available
    locked = not available
    if available and not inUse then
        table.insert(buttonTbl, {text=LocalString.GetString("更换"), isYellow=false, action=function(go) self:OnApplyButtonClick() end})
    elseif available and inUse then
        table.insert(buttonTbl, {text=LocalString.GetString("脱下"), isYellow=false, action=function(go) self:OnRemoveButtonClick() end})
    elseif bShowProgress and curVal>=needVal then --满足进度需求显示解锁按钮
        table.insert(buttonTbl, {text=LocalString.GetString("解锁永久"), isYellow=true, action=function(go) LuaAppearancePreviewMgr:ShowFashionUnlockWnd(appearanceData.id) end})
    else
        table.insert(buttonTbl, {text=LocalString.GetString("获取"), isYellow=true, action=function(go) self:OnMoreButtonClick(go) end})
    end
    self.m_ItemDisplay:Init(icon, appearanceData.name, qualityBgColor, qualityText, bShowProgress, curVal, needVal, disabled, locked, buttonTbl)
end

function LuaAppearanceFashionSubview:GetSelectedAppearanceData()
    local appearanceData = nil
    for i=1,#self.m_AllFashions do
        if self:IsSelectedFashionOrGroup(self.m_SelectedDataId, self.m_AllFashions[i]) then
            if self.m_AllFashions[i].multicolor>0 then
                local index = self:GetGroupIndexById(self.m_SelectedDataId, self.m_AllFashions[i])
                appearanceData = self.m_AllFashions[i].fashionTbl[index]
            else
                appearanceData = self.m_AllFashions[i]
            end
            break
        end
    end
    return appearanceData
end

function LuaAppearanceFashionSubview:OnApplyButtonClick()
    LuaAppearancePreviewMgr:RequestTakeOnFashion_Permanent(self.m_SelectedDataId)
end

function LuaAppearanceFashionSubview:OnRemoveButtonClick()
    LuaAppearancePreviewMgr:RequestTakeOffFashion_Permanent(self.m_SelectedDataId)
end

function LuaAppearanceFashionSubview:OnMoreButtonClick(go)
    local itemId = self:GetSelectedAppearanceData(self.m_SelectedDataId).itemGetId
    LuaItemAccessListMgr:ShowItemAccessInfoAtLeft(itemId, true, go.transform)
end

function LuaAppearanceFashionSubview:OnEnable()
    g_ScriptEvent:AddListener("OnSyncMainPlayerAppearanceProp",self,"SyncMainPlayerAppearancePropUpdate")
    g_ScriptEvent:AddListener("OnSyncWardrobeProperty",self,"OnSyncWardrobeProperty")
    g_ScriptEvent:AddListener("OnTakeOffFashion_Permanent",self,"OnTakeOffFashion_Permanent")
    g_ScriptEvent:AddListener("OnSetFashionHideSuccess_Permanent",self,"OnSetFashionHideSuccess_Permanent")
end

function LuaAppearanceFashionSubview:OnDisable()
    g_ScriptEvent:RemoveListener("OnSyncMainPlayerAppearanceProp",self,"SyncMainPlayerAppearancePropUpdate")
    g_ScriptEvent:RemoveListener("OnSyncWardrobeProperty",self,"OnSyncWardrobeProperty")
    g_ScriptEvent:RemoveListener("OnTakeOffFashion_Permanent",self,"OnTakeOffFashion_Permanent")
    g_ScriptEvent:RemoveListener("OnSetFashionHideSuccess_Permanent",self,"OnSetFashionHideSuccess_Permanent")
end

--仙凡切换 或其他sync appearance情况
function LuaAppearanceFashionSubview:SyncMainPlayerAppearancePropUpdate()
    self:LoadData()
end

function LuaAppearanceFashionSubview:OnSyncWardrobeProperty(reason)
    self:LoadData()
end

function LuaAppearanceFashionSubview:OnTakeOffFashion_Permanent(fashionId)
    local bFound = false
    if LuaAppearancePreviewMgr.m_PreviewHeadFashionId==fashionId then
        bFound = true
        LuaAppearancePreviewMgr.m_PreviewIsEntiretyHead = false
        LuaAppearancePreviewMgr.m_PreviewHeadFashionId = 0
    elseif LuaAppearancePreviewMgr.m_PreviewBodyFashionId==fashionId then
        bFound = true
        LuaAppearancePreviewMgr.m_PreviewBodyFashionId = 0
        local headId = LuaAppearancePreviewMgr:GetEntiretyHead(fashionId)
        if headId ~= 0 then
            LuaAppearancePreviewMgr.m_PreviewIsEntiretyHead = false
            LuaAppearancePreviewMgr.m_PreviewHeadFashionId = 0
        end
    elseif LuaAppearancePreviewMgr.m_PreviewBackPendantFashionId==fashionId then
        bFound = true
        LuaAppearancePreviewMgr.m_PreviewBackPendantFashionId = 0
    end
    if bFound then
        self:SetDefaultSelection()
        self:LoadData()
    end
end

function LuaAppearanceFashionSubview:OnSetFashionHideSuccess_Permanent(pos,value)
    if pos == self.m_PreviewType then
        local bHide = value>0
        if bHide then
            if pos == EnumAppearanceFashionPosition.eHead then
                LuaAppearancePreviewMgr.m_PreviewHeadFashionId = 0
            elseif pos == EnumAppearanceFashionPosition.eBody then
                LuaAppearancePreviewMgr.m_PreviewBodyFashionId = 0
            elseif pos == EnumAppearanceFashionPosition.eBeiShi then
                LuaAppearancePreviewMgr.m_PreviewBackPendantFashionId = 0
            end
        end
        self:SetDefaultSelection()
        self:LoadData()
    end
end