local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local UICamera = import "UICamera"

LuaCompetitionHonorBookView = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaCompetitionHonorBookView, "CompetitionHonorModelView", CCommonLuaScript)
RegistChildComponent(LuaCompetitionHonorBookView, "CompetitionHonorIntroduceView", CCommonLuaScript)
RegistChildComponent(LuaCompetitionHonorBookView, "PastGameBtn", GameObject)
RegistChildComponent(LuaCompetitionHonorBookView, "IntroduceBtn", GameObject)
RegistChildComponent(LuaCompetitionHonorBookView, "PageTable", UITable)
RegistChildComponent(LuaCompetitionHonorBookView, "NoSelectTemplate", GameObject)
RegistChildComponent(LuaCompetitionHonorBookView, "BeSelectTemplate", GameObject)
RegistChildComponent(LuaCompetitionHonorBookView, "RightArrow", GameObject)
RegistChildComponent(LuaCompetitionHonorBookView, "LeftArrow", GameObject)
RegistChildComponent(LuaCompetitionHonorBookView, "Title", GameObject)

RegistClassMember(LuaCompetitionHonorBookView, "m_NewCompetitionSeason")
RegistClassMember(LuaCompetitionHonorBookView, "m_CompetitionSet")
RegistClassMember(LuaCompetitionHonorBookView, "m_PageTbl")
RegistClassMember(LuaCompetitionHonorBookView, "m_PlayerTbl")
RegistClassMember(LuaCompetitionHonorBookView, "m_Seasons")
RegistClassMember(LuaCompetitionHonorBookView, "m_CompetitionId")
RegistClassMember(LuaCompetitionHonorBookView, "m_SeasonId")
RegistClassMember(LuaCompetitionHonorBookView, "m_GroupId")

--@endregion RegistChildComponent end

function LuaCompetitionHonorBookView:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    UIEventListener.Get(self.PastGameBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnPastGameBtnClick()
    end)
    UIEventListener.Get(self.IntroduceBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnIntroduceBtnClick()
    end)
    UIEventListener.Get(self.LeftArrow).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnPageArrowClick(1)
    end)
    UIEventListener.Get(self.RightArrow).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnPageArrowClick(-1)
    end)
end

function LuaCompetitionHonorBookView:InitBook(index, season)
    self.IntroduceBtn:SetActive(false)
    self.m_CompetitionId = index
    self.m_NewCompetitionSeason = season or 1
    self.m_CompetitionSet = CompetitionHonor_GameShowInfo.GetData(self.m_CompetitionId)
    self.PastGameBtn.gameObject:SetActive(self.m_CompetitionId ~= 2)    -- 全民没有赛事回顾界面，隐藏按钮
    self:InitGameInfo()
    self:InitPageBtn()
end

function LuaCompetitionHonorBookView:InitGameInfo()
    self.m_Seasons = {}
    if self.m_CompetitionId == 1 then
        -- 斗魂坛
        CompetitionHonor_DouHunTanSeason.Foreach(function (key, data)
            if key <= self.m_NewCompetitionSeason then
                self.m_Seasons[key] = data.Season
            end
        end)
        self.m_GroupId = 1
    elseif self.m_CompetitionId == 2 then
        -- 全民争霸
        CompetitionHonor_QMPKSeason.Foreach(function (key, data)
            if key <= self.m_NewCompetitionSeason then
                self.m_Seasons[key] = data.Season
            end
        end)
        self.m_GroupId = 1
    elseif self.m_CompetitionId == 3 then
        -- 明星赛
        CompetitionHonor_StarBiWuSeason.Foreach(function (key, data)
            if key <= self.m_NewCompetitionSeason then
                self.m_Seasons[key] = data.Season
            end
        end)
        self.m_GroupId = 1
    end
end

function LuaCompetitionHonorBookView:InitPageBtn()
    Extensions.RemoveAllChildren(self.PageTable.transform)
    self.BeSelectTemplate.gameObject:SetActive(false)
    self.NoSelectTemplate.gameObject:SetActive(false)
    self.m_PageTbl = {}
    for i = 1, 9 do
        local template = i == 5 and self.BeSelectTemplate or self.NoSelectTemplate
        local go = NGUITools.AddChild(self.PageTable.gameObject, template)
        go:SetActive(true)
        local item = {btn = go, index = 0}
        table.insert(self.m_PageTbl, item)
        UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnPageBtnClick(item)
        end)
    end    
    self.PageTable:Reposition()
    self.m_SeasonId = self.m_NewCompetitionSeason
    self:UpdateCurrentPage()
end

function LuaCompetitionHonorBookView:UpdateCurrentPage()
    -- Title
    local groupName = ""
    if self.m_CompetitionSet.GroupNames then
        groupName = self.m_CompetitionSet.GroupNames[self.m_GroupId - 1]
    end
    local titleStr = SafeStringFormat3(LocalString.GetString("%s届%s冠军"), self.m_Seasons[self.m_SeasonId], groupName)
    self.Title.transform:Find("Label"):GetComponent(typeof(UILabel)).text = titleStr
    -- Page
    for i = 1, #self.m_PageTbl do
        local index = self.m_SeasonId + 5 - i
        local page = self.m_PageTbl[i]
        page.btn.gameObject:SetActive(self.m_Seasons[index])
        if self.m_Seasons[index] then
            page.btn:GetComponent(typeof(UILabel)).text = tostring(index == 0 and 1 or index)
            page.index = index
        end
    end
    self.LeftArrow.gameObject:SetActive(self.m_Seasons[self.m_SeasonId + 1])
    self.RightArrow.gameObject:SetActive(self.m_Seasons[self.m_SeasonId - 1])
    Gac2Gas.QueryCompetitionHonorChapionGroup(self.m_CompetitionId, self.m_SeasonId, self.m_GroupId)
end

--@region UIEvent
function LuaCompetitionHonorBookView:OnPastGameBtnClick()
    if self.m_CompetitionId == 1 then
        Gac2Gas.OpenCrossDouHunMatchRecordWnd()
    elseif self.m_CompetitionId == 3 then
        CUIManager.ShowUI(CLuaUIResources.StarBiwuEventReviewWnd)
    end
end

function LuaCompetitionHonorBookView:OnIntroduceBtnClick()
    self.CompetitionHonorIntroduceView.gameObject:SetActive(true)
    self.CompetitionHonorIntroduceView:InitView(self.m_CompetitionId)
end

function LuaCompetitionHonorBookView:OnPageBtnClick(item)
    self.m_SeasonId = item.index
    self:UpdateCurrentPage()
end

function LuaCompetitionHonorBookView:OnPageArrowClick(delta)
    self.m_SeasonId = self.m_SeasonId + delta
    self:UpdateCurrentPage()
end

--@endregion UIEvent