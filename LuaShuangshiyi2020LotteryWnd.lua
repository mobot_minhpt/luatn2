local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local CUIFx = import "L10.UI.CUIFx"
local CCarnivalLotteryMgr = import "L10.Game.CCarnivalLotteryMgr"
local CChatLinkMgr = import "CChatLinkMgr"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local Task_Task = import "L10.Game.Task_Task"

CLuaShuangshiyi2020LotteryWnd = class()

RegistClassMember(CLuaShuangshiyi2020LotteryWnd,"m_Nodes")
RegistClassMember(CLuaShuangshiyi2020LotteryWnd,"m_CheckBtn")
RegistClassMember(CLuaShuangshiyi2020LotteryWnd,"m_WaitingLogo")
RegistClassMember(CLuaShuangshiyi2020LotteryWnd,"m_GoBuyBtn")
RegistClassMember(CLuaShuangshiyi2020LotteryWnd,"m_LingyuCount")
RegistClassMember(CLuaShuangshiyi2020LotteryWnd,"m_EnoughOrNot")
RegistClassMember(CLuaShuangshiyi2020LotteryWnd,"m_IsEnough")

function CLuaShuangshiyi2020LotteryWnd:Init()
	self:InitComponets()
	self:InitRewardInfo()

	UIEventListener.Get(self.m_CheckBtn).onClick=DelegateFactory.VoidDelegate(function(go)
        CUIManager.ShowUI(CLuaUIResources.Shuangshiyi2020LotteryRankWnd)
	end)

	UIEventListener.Get(self.m_GoBuyBtn).onClick=DelegateFactory.VoidDelegate(function(go)
		CShopMallMgr.ShowLinyuShoppingMall(0)
		CUIManager.CloseUI(CLuaUIResources.Shuangshiyi2020LotteryWnd)
	end)
	
	Gac2Gas.QueryDouble11LotteryResult()
end

function CLuaShuangshiyi2020LotteryWnd:InitComponets()
    self.m_Nodes = {}
    self.m_Nodes[1] = self.transform:Find("Middle/Node1").gameObject
    self.m_Nodes[2] = self.transform:Find("Middle/Node2").gameObject
    self.m_Nodes[3] = self.transform:Find("Middle/Node3").gameObject
    self.m_Nodes[4] = self.transform:Find("Middle/Node4").gameObject
    self.m_Nodes[5] = self.transform:Find("Middle/Node5").gameObject
    
    self.m_CheckBtn = self.transform:Find("Bottom/CheckBtn").gameObject
    self.m_GoBuyBtn = self.transform:Find("Bottom/GoBuyBtn").gameObject
    self.m_LingyuCount = self.transform:Find("Top/Reward/LingyuCount"):GetComponent(typeof(UILabel))
    self.m_EnoughOrNot = self.transform:Find("Top/Reward/EnoughOrNot"):GetComponent(typeof(UILabel))
    self.m_WaitingLogo = self.transform:Find("Bottom/WaitingLogo").gameObject
	
	self.m_TopLabel = self.transform:Find("Top/label"):GetComponent(typeof(UILabel))
end

function CLuaShuangshiyi2020LotteryWnd:OnEnable()
	g_ScriptEvent:AddListener("SendDouble11LotteryResult", self, "OnSendDouble11LotteryResult")
end

function CLuaShuangshiyi2020LotteryWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SendDouble11LotteryResult", self, "OnSendDouble11LotteryResult")
end

function CLuaShuangshiyi2020LotteryWnd:OnSendDouble11LotteryResult(status, lotteryData)
	local lingyuCount = CCarnivalLotteryMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eShuangshiyi2020LingyuConsumption)
	self.m_LingyuCount.text = tostring(lingyuCount)

	self.m_IsEnough = false
	if lingyuCount >= Double11_Setting.GetData().LotteryJoinThreshold then
		self.m_IsEnough = true
	end

	if status == 4 then
		CLuaShuangshiyi2020Mgr.m_LotteryData = lotteryData
		CLuaShuangshiyi2020Mgr:CalculateLotteryData()
		if CLuaShuangshiyi2020Mgr.m_MyLotteryInfo then
			local rewardIndex =  CLuaShuangshiyi2020Mgr.m_MyLotteryInfo["rewardIndex"]
			local itemFx = self.m_Nodes[rewardIndex].transform:Find("fx"):GetComponent(typeof(CUIFx))
			local light = self.m_Nodes[rewardIndex].transform:Find("Light").gameObject
			light:SetActive(true)
            itemFx:DestroyFx()
			itemFx:LoadFx("Fx/UI/Prefab/UI_choujiangmiandan.prefab")
		elseif self.m_IsEnough then
			local itemFx = self.m_Nodes[5].transform:Find("fx"):GetComponent(typeof(CUIFx))
			local light = self.m_Nodes[5].transform:Find("Light").gameObject
			light:SetActive(true)
            itemFx:DestroyFx()
			itemFx:LoadFx("Fx/UI/Prefab/UI_choujiangmiandan.prefab")
		end
	end
	
	if CLuaShuangshiyi2020Mgr.m_RewardFirstShowMark and CLuaShuangshiyi2020Mgr.m_RewardLevel and CLuaShuangshiyi2020Mgr.m_MyLotteryInfo then
		if CLuaShuangshiyi2020Mgr.m_MyLotteryInfo["rewardIndex"] == CLuaShuangshiyi2020Mgr.m_RewardLevel then
			CUIManager.ShowUI(CLuaUIResources.Shuangshiyi2020LotteryRewardWnd)
		end
	end

	self:InitProgressInfo(status)
end

function CLuaShuangshiyi2020LotteryWnd:InitProgressInfo(progress)

	--00FF66 绿色
	--FF0000 红色
	if progress == 0 then
		self.m_CheckBtn.gameObject:SetActive(false)
		self.m_GoBuyBtn.gameObject:SetActive(true)
		self.m_WaitingLogo.gameObject:SetActive(false)
		if not self.m_IsEnough then
			self.m_EnoughOrNot.text = LocalString.GetString("[FF0000]尚未获得抽奖资格，可前往灵玉商城消费")
		else
			self.m_EnoughOrNot.text = LocalString.GetString("[00FF66]已获得抽奖资格，消费越多中奖概率越高")
		end
	elseif  progress == 2 then
		self.m_CheckBtn.gameObject:SetActive(false)
		self.m_GoBuyBtn.gameObject:SetActive(false)
		self.m_WaitingLogo.gameObject:SetActive(true)
		if not self.m_IsEnough then
			self.m_EnoughOrNot.text = LocalString.GetString("[FF0000]尚未获得抽奖资格，今日12点抽奖")
		else
			self.m_EnoughOrNot.text = LocalString.GetString("[00FF66]已获得抽奖资格，今日12点抽奖")
		end
	elseif  progress == 4 then
		self.m_CheckBtn.gameObject:SetActive(true)
		self.m_GoBuyBtn.gameObject:SetActive(false)
		self.m_WaitingLogo.gameObject:SetActive(false)
		if not self.m_IsEnough then
			self.m_EnoughOrNot.text = LocalString.GetString("[FF0000]抽奖结束，未获得抽奖资格")
		else
			local rewardStr = ""
			if CLuaShuangshiyi2020Mgr.m_MyLotteryInfo then
				rewardStr = CLuaShuangshiyi2020Mgr.m_MyLotteryInfo["reward"]
			else
				rewardStr = LocalString.GetString("参与奖")
			end
			self.m_EnoughOrNot.text = SafeStringFormat3(LocalString.GetString("[00FF66]抽奖结束，恭喜获得%s!"), rewardStr)
		end
	end
end

function CLuaShuangshiyi2020LotteryWnd:InitRewardInfo()
	for i=1, 4 do
		local data = Double11_LotterySetting.GetData(i)
		local node = self.m_Nodes[i]
		node.transform:Find("Count"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString('全服抽%s名'), data.Cnt)
		node.transform:Find("Rebate"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString('%s%%'), data.RewardPerc)
	end

	local LotteryRewardItemID = Double11_Setting.GetData().LotteryRewardItemID
	local rewardLink = SafeStringFormat3("<link item=%d>", LotteryRewardItemID)
	rewardLink = CChatLinkMgr.TranslateToNGUIText(rewardLink, true)
	local desc = self.m_Nodes[5].transform:Find("Desc")
	
	desc:GetComponent(typeof(UILabel)).text = rewardLink

	UIEventListener.Get(desc.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
        local url = p:GetComponent(typeof(UILabel)):GetUrlAtPosition(UICamera.lastWorldPosition)
        if url ~= nil then
            CChatLinkMgr.ProcessLinkClick(url, nil)
        end
    end)

	local lotteryJoinThreshold = Double11_Setting.GetData().LotteryJoinThreshold
	self.m_TopLabel.text = SafeStringFormat3(LocalString.GetString("11月1日-11月13日  累计消费%d灵玉及以上的角色可以参与抽奖"), lotteryJoinThreshold)
end
