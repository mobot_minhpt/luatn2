local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local CGuideBubbleDisplay = import "L10.UI.CGuideBubbleDisplay"

LuaSanxingGuideWnd = class()
RegistChildComponent(LuaSanxingGuideWnd,"attackNode1", GameObject)
RegistChildComponent(LuaSanxingGuideWnd,"attackNode2", GameObject)
RegistChildComponent(LuaSanxingGuideWnd,"defendNode1", GameObject)
RegistChildComponent(LuaSanxingGuideWnd,"defendNode2", GameObject)
--RegistChildComponent(LuaSanxingGuideWnd,"attack2_bubble", CGuideBubbleSizeCtl)
--RegistChildComponent(LuaSanxingGuideWnd,"attack2_btn", GameObject)
--RegistChildComponent(LuaSanxingGuideWnd,"defend1_bg", GameObject)
--RegistChildComponent(LuaSanxingGuideWnd,"defend1_bubble", CGuideBubbleSizeCtl)
--RegistChildComponent(LuaSanxingGuideWnd,"defend1_btn", GameObject)
--RegistChildComponent(LuaSanxingGuideWnd,"defend2_bg", GameObject)
--RegistChildComponent(LuaSanxingGuideWnd,"defend2_bubble", CGuideBubbleSizeCtl)
--RegistChildComponent(LuaSanxingGuideWnd,"defend2_btn", GameObject)

--RegistChildComponent(LuaSanxingGuideWnd,"bubble1", CGuideBubbleSizeCtl)

RegistClassMember(LuaSanxingGuideWnd, "maxChooseNum")

function LuaSanxingGuideWnd:Close()
	CUIManager.CloseUI(CLuaUIResources.SanxingGuideWnd)
end

function LuaSanxingGuideWnd:OnEnable()
	--g_ScriptEvent:AddListener("", self, "")
end

function LuaSanxingGuideWnd:OnDisable()
	--g_ScriptEvent:RemoveListener("", self, "")
end

function LuaSanxingGuideWnd:Init()
	self.attackNode1:SetActive(false)
	self.attackNode2:SetActive(false)
	self.defendNode1:SetActive(false)
	self.defendNode2:SetActive(false)

	if LuaSanxingGamePlayMgr.InGuidePha1 then -- defend
		self:InitDefendGuide1()
	elseif LuaSanxingGamePlayMgr.InGuidePha2 then -- attack
		self:InitAttackGuide1()
	end
end

function LuaSanxingGuideWnd:InitDefendGuide()
end

function LuaSanxingGuideWnd:InitDefendGuide1()
	self.defendNode1:SetActive(true)

	local bubble = self.defendNode1.transform:Find('bubble'):GetComponent(typeof(CGuideBubbleDisplay))
  bubble.gameObject:SetActive(true)
  bubble:Init(LocalString.GetString('[c][FFC800]三星成员[-][/c]复活点两侧有[c][FFC800]前往战场[-][/c]的矿车,[c][FFC800]点击矿车[-][/c]即可乘坐'))

	local btn = self.defendNode1.transform:Find('btn').gameObject
	local onClick = function(go)
		self.defendNode1:SetActive(false)
		self:InitDefendGuide2()
	end
	CommonDefs.AddOnClickListener(btn,DelegateFactory.Action_GameObject(onClick),false)
end

function LuaSanxingGuideWnd:InitDefendGuide2()
	self.defendNode2:SetActive(true)

	local bubble = self.defendNode2.transform:Find('bubble'):GetComponent(typeof(CGuideBubbleDisplay))
  bubble.gameObject:SetActive(true)
  bubble:Init(LocalString.GetString('前往战场[c][FFC800]保护旗帜[-][/c]击杀夺旗的敌方成员'))

	local btn = self.defendNode2.transform:Find('btn').gameObject
	local onClick = function(go)
		self:Close()
		CLuaGuideMgr.TryTriggerSanxingGuide2()
	end
	CommonDefs.AddOnClickListener(btn,DelegateFactory.Action_GameObject(onClick),false)
end

function LuaSanxingGuideWnd:InitAttackGuide1()
	self.attackNode1:SetActive(true)

	local bubble = self.attackNode1.transform:Find('bubble'):GetComponent(typeof(CGuideBubbleDisplay))
  bubble.gameObject:SetActive(true)
  bubble:Init(LocalString.GetString('[c][FFC800]金沙成员[-][/c]前往战场[c][FFC800]抢夺旗帜[-][/c],夺旗后返回'))

	local btn = self.attackNode1.transform:Find('btn').gameObject
	local onClick = function(go)
		self.attackNode1:SetActive(false)
		self:InitAttackGuide2()
	end
	CommonDefs.AddOnClickListener(btn,DelegateFactory.Action_GameObject(onClick),false)
end

function LuaSanxingGuideWnd:InitAttackGuide2()
	self.attackNode2:SetActive(true)

	local bubble = self.attackNode2.transform:Find('bubble'):GetComponent(typeof(CGuideBubbleDisplay))
  bubble.gameObject:SetActive(true)
  bubble:Init(LocalString.GetString('回到本方营地范围[c][FFC800]自动提交[-][/c]旗帜'))

	local btn = self.attackNode2.transform:Find('btn').gameObject
	local onClick = function(go)
		self:Close()
		CLuaGuideMgr.TryTriggerSanxingGuide2_1()
	end
	CommonDefs.AddOnClickListener(btn,DelegateFactory.Action_GameObject(onClick),false)
end
return LuaSanxingGuideWnd
