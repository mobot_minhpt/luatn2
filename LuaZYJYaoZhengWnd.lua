local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local SoundManager = import "SoundManager"
local Vector3 = import "UnityEngine.Vector3"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local PlayerSettings = import "L10.Game.PlayerSettings"

LuaZYJYaoZhengWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaZYJYaoZhengWnd, "Btns", "Btns", GameObject)
RegistChildComponent(LuaZYJYaoZhengWnd, "CloseButton", "CloseButton", GameObject)
RegistChildComponent(LuaZYJYaoZhengWnd, "QinPuButton", "QinPuButton", GameObject)

--@endregion RegistChildComponent end

RegistClassMember(LuaZYJYaoZhengWnd, "m_nettickdata")
RegistClassMember(LuaZYJYaoZhengWnd, "m_nettick")
RegistClassMember(LuaZYJYaoZhengWnd, "m_lasttime")

function LuaZYJYaoZhengWnd:Awake()
    self.m_nettickdata = {}
    --@region EventBind: Dont Modify Manually!
    UIEventListener.Get(self.CloseButton.gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:OnCloseButtonClick()
        end
    )

    UIEventListener.Get(self.QinPuButton.gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:QinPuButtonClick()
        end
    )
    --@endregion EventBind end
end

function LuaZYJYaoZhengWnd:SetVisiableUI(tf)
    local hideExcepttb = {"GuideWnd"}
    local hideExcept = Table2List(hideExcepttb, MakeGenericClass(List, cs_string))
    if tf then
        CUIManager.ShowUIGroupByChangeLayer(CUIManager.EUIModuleGroup.EGameUI, hideExcept, false, false)
        CUIManager.ShowUIGroupByChangeLayer(CUIManager.EUIModuleGroup.EBgGameUI, hideExcept, false, false)
    else
        CUIManager.HideUIGroupByChangeLayer(CUIManager.EUIModuleGroup.EGameUI, hideExcept, false, false, false)
        CUIManager.HideUIGroupByChangeLayer(CUIManager.EUIModuleGroup.EBgGameUI, hideExcept, false, false, false)
    end
end

function LuaZYJYaoZhengWnd:OnPlayerDetach(playerId, furnitureId)
    if playerId == CClientMainPlayer.Inst.Id then
        CUIManager.CloseUI(CLuaUIResources.ZYJYaoZhengWnd)
    end
end

function LuaZYJYaoZhengWnd:OnEnable()
    g_ScriptEvent:AddListener("PlayerDetachFurniture", self, "OnPlayerDetach")

    self:SetVisiableUI(false)
    SoundManager.Inst:StopBGMusic()

    if self.m_nettick then
        UnRegisterTick(self.m_nettick)
    end
    local sendfunc = function()
        if #self.m_nettickdata > 0 then
            local datas = g_MessagePack.pack(self.m_nettickdata)
            Gac2Gas.SyncGuZhengKeyStrokes(InstrumentMgr.FurnitureID, datas)
            self.m_nettickdata = {}
        end
    end
    self.m_nettick = RegisterTick(sendfunc, 1000)
end

function LuaZYJYaoZhengWnd:ProcessCamera()
    ZhongYuanJie2022Mgr.AddCameraPoint(1) --增加10s的镜头
end



function LuaZYJYaoZhengWnd:OnDisable()
    g_ScriptEvent:RemoveListener("PlayerDetachFurniture", self, "OnPlayerDetach")
    CameraFollow.Inst:EndToricSpaceCamera()
    if self.m_nettick then
        UnRegisterTick(self.m_nettick)
    end

    self:SetVisiableUI(true)
    SoundManager.Inst:StartBGMusic()

    CameraFollow.Inst:ResetToDefault(true, false)
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.RO then
        CameraFollow.Inst:SetFollowObj(CClientMainPlayer.Inst.RO)
    end
end

function LuaZYJYaoZhengWnd:PlaySound(interval, scale)
    ZhongYuanJie2022Mgr.PlayYaoZhengSound(interval, scale)
end

function LuaZYJYaoZhengWnd:Init()
    if InstrumentMgr.FurnitureID <= 0 then
        CUIManager.CloseUI(CLuaUIResources.ZYJYaoZhengWnd)
        return
    end

    CLuaGuideMgr.TryTriggerGuide(205)

    local soundenable = PlayerSettings.SoundEnabled
    if soundenable == false then
        local msg = g_MessageMgr:FormatMessage("YaoZhengQingYin_OpenSoundEffects")
        local okfunc = function()
            PlayerSettings.SoundEnabled = true
            SoundManager.Inst:StopBGMusic()
        end
        g_MessageMgr:ShowOkCancelMessage(msg, okfunc, nil, nil, nil, true)
    end

    if ZhongYuanJie2022Mgr.MusicDoc then
        self:ProcessCamera()
    end

    self:AddBtnEvents()
end

function LuaZYJYaoZhengWnd:AddBtnEvents()
    for i = 1, 3 do
        local trans = FindChild(self.Btns.transform, "Line" .. i)
        for j = 1, 7 do
            local btn = FindChildWithType(trans, "Button" .. j, typeof(GameObject))
            UIEventListener.Get(btn).onClick =
                DelegateFactory.VoidDelegate(
                function(go)
                    self:PlaySound(3 - i, j)
                    local curtime = CServerTimeMgr.Inst.timeStamp
                    if not self.m_lasttime then
                        self.m_lasttime = curtime
                    end
                    local gap = curtime - self.m_lasttime
                    self.m_lasttime = curtime
                    table.insert(self.m_nettickdata, 3 - i)
                    table.insert(self.m_nettickdata, j)
                    table.insert(self.m_nettickdata, math.floor(gap * 1000))
                    local fx = FindChildWithType(go.transform, "Fx", typeof(CUIFx))
                    fx:DestroyFx()
                    fx:LoadFx("fx/ui/prefab/UI_yaozhengqingyin_dianji.prefab")
                    self:ProcessCamera()
                end
            )
        end
    end
end

--@region UIEvent

function LuaZYJYaoZhengWnd:OnCloseButtonClick()
    Gac2Gas.RequestLeaveGuZheng(InstrumentMgr.FurnitureID)
    CUIManager.CloseUI(CLuaUIResources.ZYJYaoZhengWnd)
end

function LuaZYJYaoZhengWnd:QinPuButtonClick()
    CUIManager.ShowUI(CLuaUIResources.ZYJYaoZhengPuWnd)
end

--@endregion UIEvent

function LuaZYJYaoZhengWnd:GetGuideGo(methodName)
    if methodName == "GetQinPuButton" then
        return self.QinPuButton
    end
end
