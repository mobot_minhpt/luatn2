require("common/common_include")
local LuaGameObject=import "LuaGameObject"
local CConstellationWnd = import "L10.UI.CConstellationWnd"
local NGUITools = import "NGUITools"
local Item_Item = import "L10.Game.Item_Item"
local Extensions = import "Extensions"
local CUICommonDef = import "L10.UI.CUICommonDef"
local UICamera = import "UICamera"

CLuaXingGuanCostItemWnd=class()
RegistClassMember(CLuaXingGuanCostItemWnd, "m_Grid")
RegistClassMember(CLuaXingGuanCostItemWnd, "m_Template")
RegistClassMember(CLuaXingGuanCostItemWnd, "m_XingmangCnt")

function CLuaXingGuanCostItemWnd:Init()
end

function CLuaXingGuanCostItemWnd:OnEnable()
    if self.m_Grid == nil then
        self.m_Grid = LuaGameObject.GetChildNoGC(self.transform, "Grid").grid
        self.m_Template = LuaGameObject.GetChildNoGC(self.transform, "Template").gameObject
        self.m_XingmangCnt = LuaGameObject.GetChildNoGC(LuaGameObject.GetChildNoGC(self.transform, "Xingmang").transform, "Count").label
        self.m_Template:SetActive(false)
    end
    if CConstellationWnd.Inst == nil then
        self.gameObject:SetActive(false)
        return
    end

    Extensions.RemoveAllChildren(self.m_Grid.transform)

    local con = CConstellationWnd.Inst.m_ConstellationList[CConstellationWnd.Inst.m_TabIndex][CConstellationWnd.Inst.m_RowIndex]
    if con == nil then
        return
    end

    local starList = con.m_StarList
    if starList ~= nil then
        local xingmangCnt = 0
        for j = 0, starList.Count - 1 do
            local star = starList[j]
            if star ~= nil and star.m_ItemCost ~= nil and star.m_ItemCost.Length >= 2 then
                local itemId = star.m_ItemCost[0]
                local itemCnt = star.m_ItemCost[1]
                local data = Item_Item.GetData(star.m_ItemCost[0])
                if data ~= nil then
                    local go = NGUITools.AddChild(self.m_Grid.gameObject, self.m_Template)
                    if go ~= nil then
                        go:SetActive(true)
                        LuaGameObject.GetChildNoGC(go.transform, "Texture").cTexture:LoadMaterial(data.Icon)
                        LuaGameObject.GetChildNoGC(go.transform, "Name").label.text = data.Name
                        LuaGameObject.GetChildNoGC(go.transform, "Count").label.text = itemCnt
                    end     
                end
            end
            xingmangCnt = xingmangCnt + star.m_XingmangCost
        end
        self.m_XingmangCnt.text = xingmangCnt
    end
    self.m_Grid:Reposition()
end

function CLuaXingGuanCostItemWnd:Update()
    if Input.GetMouseButtonDown(0) then       
        if (not CUICommonDef.IsOverUI or (UICamera.lastHit.collider and
           not CUICommonDef.IsChild(self.transform, UICamera.lastHit.collider.transform) and
            CUICommonDef.GetNearestPanelDepth(self.transform) > CUICommonDef.GetNearestPanelDepth(UICamera.lastHit.collider.transform))) then
            self.gameObject:SetActive(false)
        end
    end
end

return CLuaXingGuanCostItemWnd