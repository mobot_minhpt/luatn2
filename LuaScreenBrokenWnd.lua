require("3rdParty/ScriptEvent")
require("common/common_include")

local ShatterSpawner = import "L10.Game.ShatterSpawner"
local Vector3 = import "UnityEngine.Vector3"
local Camera = import "UnityEngine.Camera"
local CUICommonDef = import "L10.UI.CUICommonDef"
local Screen = import "UnityEngine.Screen"
local Rect = import "UnityEngine.Rect"
local TextureFormat = import "UnityEngine.TextureFormat"
local Texture2D = import "UnityEngine.Texture2D"
local RenderTexture = import "UnityEngine.RenderTexture"
local Constants = import "L10.Game.Constants"
local CMainCamera = import "L10.Engine.CMainCamera"
local CScene = import "L10.Game.CScene"
local CameraClearFlags = import "UnityEngine.CameraClearFlags"
local GLTextManager = import "GLTextManager"
local CScreenCaptureWnd = import "L10.UI.CScreenCaptureWnd"
local CPostEffect = import "L10.Engine.CPostEffect"
local CUIFx = import "L10.UI.CUIFx"
local EUIModuleGroup = import "L10.UI.CUIManager+EUIModuleGroup"
local UICamera = import "UICamera"
local NGUIText = import "NGUIText"

LuaScreenBrokenWnd=class()

RegistClassMember(LuaScreenBrokenWnd, "m_BG")
RegistClassMember(LuaScreenBrokenWnd, "m_Shatterer")
RegistClassMember(LuaScreenBrokenWnd, "m_ShatterCamera")
RegistClassMember(LuaScreenBrokenWnd, "m_Anchor")
RegistChildComponent(LuaScreenBrokenWnd, "BrokenFx", CUIFx)
RegistChildComponent(LuaScreenBrokenWnd, "ClickFxRoot", GameObject)
RegistChildComponent(LuaScreenBrokenWnd, "ClickFxTemplate", CUIFx)
RegistChildComponent(LuaScreenBrokenWnd, "CloseButton", GameObject)
RegistChildComponent(LuaScreenBrokenWnd, "HintLabel", UILabel)

RegistClassMember(LuaScreenBrokenWnd, "m_ClickCount")
RegistClassMember(LuaScreenBrokenWnd, "m_CachedTime")
RegistClassMember(LuaScreenBrokenWnd, "m_Finished")

RegistClassMember(LuaScreenBrokenWnd, "MAX_CLICK")
RegistClassMember(LuaScreenBrokenWnd, "INCREASE_INTERVAl")

function LuaScreenBrokenWnd:Awake()
	local excepts = {"MiddleNoticeCenter"}
	local List_String = MakeGenericClass(List,cs_string)
	CUIManager.HideUIGroupByChangeLayer(EUIModuleGroup.EGameUI, Table2List(excepts, List_String), false, true)
	CUIManager.HideUIGroupByChangeLayer(EUIModuleGroup.EBgGameUI, Table2List(excepts, List_String), false, true)
end

function LuaScreenBrokenWnd:Init()
	self:InitClassMembers()
	self:InitValues()
	
end

function LuaScreenBrokenWnd:InitClassMembers()
	self.m_BG = self.transform:Find("Anchor/BgTexture").gameObject
	self.m_Shatterer = self.transform:Find("Anchor/Shatterer"):GetComponent(typeof(ShatterSpawner))
	self.m_ShatterCamera = self.transform:Find("Anchor/ShatterCamera"):GetComponent(typeof(Camera))
	self.m_Anchor = self.transform:Find("Anchor").gameObject

	self.BrokenFx:DestroyFx()
	self:ClearAllClickFx()
	self.ClickFxTemplate.gameObject:SetActive(false)
	self.CloseButton:SetActive(not LuaZhuJueJuQingMgr.m_HideExit)
	if LuaZhuJueJuQingMgr.m_HintMsg then
		self.HintLabel.text = LuaZhuJueJuQingMgr.m_HintMsg
	end

	if LuaZhuJueJuQingMgr.m_HintColor then
		self.HintLabel.color = NGUIText.ParseColor24(LuaZhuJueJuQingMgr.m_HintColor, 0)
	end
	

	local onBGClicked = function (go)
		self:OnBGClicked(go)
	end
	CommonDefs.AddOnClickListener(self.m_BG, DelegateFactory.Action_GameObject(onBGClicked), false)
end

function LuaScreenBrokenWnd:ClearAllClickFx()
	CUICommonDef.ClearTransform(self.ClickFxRoot.transform)
end

function LuaScreenBrokenWnd:InitValues()
	self.m_ClickCount = 0
	self.m_CachedTime = 0
	self.m_Finished = false

	self.MAX_CLICK = 15
	self.INCREASE_INTERVAl = 1

	-- 去掉碎屏效果
	self:EnableScreenBroken()
end

function LuaScreenBrokenWnd:EnableScreenBroken()
	CPostEffect.EnableScreenBrokenMat()
	CPostEffect.SetScreenBrokenPercent(0)
end

function LuaScreenBrokenWnd:SetScreenMat(texture)

	--self.m_Texture.material:SetTexture("_MainTex", texture)
end

function LuaScreenBrokenWnd:GetScreenTexture()
	CScreenCaptureWnd.IsCapturing = true
	self.m_Anchor:SetActive(false)
	
	GLTextManager.Visible = false

	local scale = 1920 / Screen.width
	local rect = Rect(0, 0, Screen.width * scale, Screen.height * scale)
	
	local renderTexture = RenderTexture(math.floor(rect.width), math.floor(rect.height), 24)
	if CommonDefs.IsPCGameMode() then
		local CWinSocialWndMgr = import "L10.UI.CWinSocialWndMgr"
		if CWinSocialWndMgr.Inst.m_IsWinSocialWndOpened then
			rect.height = rect.height / (1 - Constants.WinSocialWndRatio)
			renderTexture = RenderTexture(math.floor(rect.width / (1 - Constants.WinSocialWndRatio)), math.floor(rect.height), 24)
		end
	end

	local texture = Texture2D(math.floor(rect.width), math.floor(rect.height), TextureFormat.ARGB32, false)

	local tmpFlags = CMainCamera.CaptureCamera.clearFlags
	if CScene.MainScene and CScene.MainScene.SkyBoxEnabled then
		CMainCamera.CaptureCamera.clearFlags = CameraClearFlags.Skybox
	else
		CMainCamera.CaptureCamera.clearFlags = CameraClearFlags.Color
	end
	CMainCamera.CaptureCamera.targetTexture = renderTexture
	CMainCamera.CaptureCamera:Render()

	CUIManager.UIMainCamera.targetTexture = renderTexture
	CUIManager.UIMainCamera:Render()

	local old = RenderTexture.active
	RenderTexture.active = renderTexture
	texture:ReadPixels(rect, 0, 0)
	texture:Apply()

	CMainCamera.CaptureCamera.clearFlags = tmpFlags
	CMainCamera.CaptureCamera.targetTexture = nil
	CUIManager.UIMainCamera.targetTexture = nil
	RenderTexture.active = old

	GameObject.Destroy(renderTexture)
	self.m_Anchor:SetActive(true)
	GLTextManager.Visible = true
	CScreenCaptureWnd.IsCapturing = false

	return texture
end

function LuaScreenBrokenWnd:OnEnable()
	--g_ScriptEvent:AddListener("CBGSendShelfConditionsInfo", self, "UpdateRequestInfos")
end

function LuaScreenBrokenWnd:OnDisable()
	LuaZhuJueJuQingMgr.m_HideExit = false
	LuaZhuJueJuQingMgr.m_HintMsg = nil
	LuaZhuJueJuQingMgr.m_HintColor = nil

	CPostEffect.DisableScreenBrokenMat()
	local excepts = {"MiddleNoticeCenter"}
	local List_String = MakeGenericClass(List,cs_string)
	CUIManager.ShowUIGroupByChangeLayer(EUIModuleGroup.EGameUI, Table2List(excepts, List_String), false, true)
	CUIManager.ShowUIGroupByChangeLayer(EUIModuleGroup.EBgGameUI, Table2List(excepts, List_String), false, true)
end

--@desc 显示爆炸效果
function LuaScreenBrokenWnd:BlowUp()
	self.BrokenFx:LoadFx("fx/ui/prefab/UI_suiping.prefab")
	CPostEffect.DisableScreenBrokenMat()
	local excepts = {"MiddleNoticeCenter"}
	local List_String = MakeGenericClass(List,cs_string)
	CUIManager.ShowUIGroupByChangeLayer(EUIModuleGroup.EGameUI, Table2List(excepts, List_String), false, true)
	CUIManager.ShowUIGroupByChangeLayer(EUIModuleGroup.EBgGameUI, Table2List(excepts, List_String), false, true)
	RegisterTickOnce(function ()
		Gac2Gas.FinishOpenScreenBroken(LuaZhuJueJuQingMgr.m_ScreenBrokenTaskId)
    	CUIManager.CloseUI(CLuaUIResources.ScreenBrokenWnd)
  	end, 1000)
end

--@desc 点击背景图的效果
function LuaScreenBrokenWnd:OnBGClicked(go)
	if self.m_Finished then
		return
	end

	if self.m_ClickCount < self.MAX_CLICK then
		self.m_ClickCount = self.m_ClickCount + self.INCREASE_INTERVAl
		self:UpdateBrokenScreen()
		self:AddClickFx()
	end
end

function LuaScreenBrokenWnd:AddClickFx()
	if self.m_Finished then return end

	local fx = NGUITools.AddChild(self.ClickFxRoot, self.ClickFxTemplate.gameObject)

	local currentPos = UICamera.currentTouch.pos
	local worldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(Vector3(currentPos.x, currentPos.y, 0))
	worldPos.z = 0
	local localPos = self.ClickFxRoot.transform:InverseTransformPoint(worldPos)

	fx.transform.localPosition = localPos
	fx.gameObject:SetActive(true)
	fx:GetComponent(typeof(CUIFx)):DestroyFx()
	fx:GetComponent(typeof(CUIFx)):LoadFx("fx/ui/prefab/UI_suiping001.prefab")
end

function LuaScreenBrokenWnd:UpdateBrokenScreen()
	if self.m_ClickCount >= self.MAX_CLICK then
		self.m_Finished = true
		self:ClearAllClickFx()
		self:BlowUp()
		CPostEffect.SetScreenBrokenPercent(1)
	else
		local percent = self.m_ClickCount / self.MAX_CLICK
		CPostEffect.SetScreenBrokenPercent(percent)
	end
end

return LuaScreenBrokenWnd
