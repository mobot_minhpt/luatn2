local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CButton = import "L10.UI.CButton"
local CNumberKeyBoardMgr = import "CNumberKeyBoardMgr"
local AlignType = import "L10.UI.CTooltip+AlignType"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
LuaNanDuFanHuaRepaymentWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaNanDuFanHuaRepaymentWnd, "TotallPriceLab", "TotallPriceLab", UILabel)
RegistChildComponent(LuaNanDuFanHuaRepaymentWnd, "QnIncreseAndDecreaseButton", "QnIncreseAndDecreaseButton", GameObject)
RegistChildComponent(LuaNanDuFanHuaRepaymentWnd, "CancelBtn", "CancelBtn", CButton)
RegistChildComponent(LuaNanDuFanHuaRepaymentWnd, "OKBtn", "OKBtn", CButton)

--@endregion RegistChildComponent end
RegistClassMember(LuaNanDuFanHuaRepaymentWnd, "m_DecreaseButton")
RegistClassMember(LuaNanDuFanHuaRepaymentWnd, "m_IncreaseButton")
RegistClassMember(LuaNanDuFanHuaRepaymentWnd, "m_InputLab")
RegistClassMember(LuaNanDuFanHuaRepaymentWnd, "m_InputNum")
RegistClassMember(LuaNanDuFanHuaRepaymentWnd, "m_InputNumMax")
RegistClassMember(LuaNanDuFanHuaRepaymentWnd, "m_TipLab")

function LuaNanDuFanHuaRepaymentWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.m_DecreaseButton   = self.QnIncreseAndDecreaseButton.transform:Find("DecreaseButton"):GetComponent(typeof(QnButton))
    self.m_IncreaseButton   = self.QnIncreseAndDecreaseButton.transform:Find("IncreaseButton"):GetComponent(typeof(QnButton))
    self.m_InputLab         = self.QnIncreseAndDecreaseButton.transform:Find("InputLab"):GetComponent(typeof(UILabel))
    self.m_TipLab           = self.transform:Find("Anchor/Tip"):GetComponent(typeof(UILabel))

    UIEventListener.Get(self.m_DecreaseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnDecreaseButtonClick()
    end)

    UIEventListener.Get(self.m_IncreaseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnIncreaseButtonClick()
    end)

    UIEventListener.Get(self.m_InputLab.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnInputLabClick()
    end)

    UIEventListener.Get(self.CancelBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnCancelBtnClick()
    end)

    UIEventListener.Get(self.OKBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnOKBtnClick()
    end)

    self.m_TipLab.text = g_MessageMgr:FormatMessage("NANDUFANHUA_REPAYMENT_TIP", tonumber(SafeStringFormat3("%.2f", NanDuFanHua_LordSetting.GetData().InterestRate * 100)))
end

function LuaNanDuFanHuaRepaymentWnd:Init()
    self.lordData = {}
    self.scheduleData = {}
    if CClientMainPlayer.Inst and CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, LuaYiRenSiMgr.LordTempPlayDataKey) then
        local playData = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, LuaYiRenSiMgr.LordTempPlayDataKey)
        if playData.ExpiredTime > CServerTimeMgr.Inst.timeStamp and playData.Data and playData.Data.Data then
            local data = g_MessagePack.unpack(playData.Data.Data)
            self.lordData = data[1]
        end
    end
    
    local myDebt = self.lordData[LuaYiRenSiMgr.LordPropertyKey.subMoneyIndex] and self.lordData[LuaYiRenSiMgr.LordPropertyKey.subMoneyIndex] or 0
    local myOwn = self.lordData[LuaYiRenSiMgr.LordPropertyKey.moneyIndex] and self.lordData[LuaYiRenSiMgr.LordPropertyKey.moneyIndex] or 0
    self.TotallPriceLab.text = myDebt
    
    self.m_InputNum = math.min(myOwn, myDebt)
    self.m_InputNum = math.min(self.m_InputNum, System.Int32.MaxValue - 2)
    self.m_InputNumMax = math.min(myOwn, myDebt)
    self.m_InputNumMax = math.max(0, self.m_InputNumMax)
    self.m_InputNumMax = math.min(self.m_InputNumMax, System.Int32.MaxValue - 2)
    self:SetInputNum(self.m_InputNum)
end

--@region UIEvent

--@endregion UIEvent

function LuaNanDuFanHuaRepaymentWnd:OnDecreaseButtonClick()
    self:SetInputNum(self.m_InputNum - 1)
end

function LuaNanDuFanHuaRepaymentWnd:OnIncreaseButtonClick()
    self:SetInputNum(self.m_InputNum + 1)
end

function LuaNanDuFanHuaRepaymentWnd:OnInputLabClick()
    CNumberKeyBoardMgr.ShowNumberKeyBoardWithRange(0, self.m_InputNumMax, self.m_InputNum, 100, DelegateFactory.Action_int(function (val)
        self.m_InputLab.text = val
    end), DelegateFactory.Action_int(function (val)
        self:SetInputNum(val)
    end), self.m_InputLab, AlignType.Right, true)
end

function LuaNanDuFanHuaRepaymentWnd:SetInputNum(num)
    if num < 0 then
        num = 0
    end

    if num > self.m_InputNumMax then
        num = self.m_InputNumMax
    end

    self.m_InputNum = num
    self.m_InputLab.text = num

    self.m_IncreaseButton.Enabled = self.m_InputNumMax - num > 0
    self.m_DecreaseButton.Enabled = num > 0
    if self.m_InputNumMax - num < 0 then
        if self.m_IncreaseTick ~= nil then
            UnRegisterTick(self.m_IncreaseTick)
            self.m_IncreaseTick = nil
        end
    end

    if num < 0 then
        if self.m_DecreaseTick ~= nil then
            UnRegisterTick(self.m_DecreaseTick)
            self.m_DecreaseTick = nil
        end
    end
end

function LuaNanDuFanHuaRepaymentWnd:OnCancelBtnClick()
    CUIManager.CloseUI(CLuaUIResources.NanDuFanHuaRepaymentWnd)
end

function LuaNanDuFanHuaRepaymentWnd:OnOKBtnClick()
    Gac2Gas.NanDuLordRepayDebt(self.m_InputNum)
    CUIManager.CloseUI(CLuaUIResources.NanDuFanHuaRepaymentWnd)
end