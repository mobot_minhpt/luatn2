local GameObject				= import "UnityEngine.GameObject"
local CUITexture				= import "L10.UI.CUITexture"
local CButton					= import "L10.UI.CButton"
local CUIFx						= import "L10.UI.CUIFx"
local QnAddSubAndInputButton	= import "L10.UI.QnAddSubAndInputButton"
local UILabel					= import "UILabel"
local DelegateFactory			= import "DelegateFactory"
local Extensions				= import "Extensions"
local CommonDefs				= import "L10.Game.CommonDefs"
local CItemMgr					= import "L10.Game.CItemMgr"
local EnumItemPlace				= import "L10.Game.EnumItemPlace"
local CItemInfoMgr				= import "L10.UI.CItemInfoMgr"
local AlignType					= import "L10.UI.CItemInfoMgr+AlignType"

--五色丝合成界面
CLuaDwWusesiHeChengWnd = class()

RegistChildComponent(CLuaDwWusesiHeChengWnd, "Item1",		GameObject)
RegistChildComponent(CLuaDwWusesiHeChengWnd, "Item2",		GameObject)
RegistChildComponent(CLuaDwWusesiHeChengWnd, "Item3",		GameObject)
RegistChildComponent(CLuaDwWusesiHeChengWnd, "Item4",		GameObject)
RegistChildComponent(CLuaDwWusesiHeChengWnd, "Item5",		GameObject)
RegistChildComponent(CLuaDwWusesiHeChengWnd, "TargetItem",	CUITexture)
RegistChildComponent(CLuaDwWusesiHeChengWnd, "Button",		CButton)
RegistChildComponent(CLuaDwWusesiHeChengWnd, "Fxnode",		CUIFx)
RegistChildComponent(CLuaDwWusesiHeChengWnd, "Fxnode2",		CUIFx)
RegistChildComponent(CLuaDwWusesiHeChengWnd, "NumInput",	QnAddSubAndInputButton)

RegistClassMember(CLuaDwWusesiHeChengWnd,"m_enable")
RegistClassMember(CLuaDwWusesiHeChengWnd,"m_items")
RegistClassMember(CLuaDwWusesiHeChengWnd, "delayTick")


function CLuaDwWusesiHeChengWnd:Init()
	self.m_items = {}
	self.m_items[21040432] = self.Item1
	self.m_items[21040433] = self.Item2
	self.m_items[21040434] = self.Item3
	self.m_items[21040435] = self.Item4
	self.m_items[21040436] = self.Item5
	
	self:Repaint()
end

function CLuaDwWusesiHeChengWnd:AddItemClickEvent(itemgo,tid)
	local itemclick = function(go)
        CItemInfoMgr.ShowLinkItemTemplateInfo(tid, false, nil, AlignType.Default, 0, 0, 0, 0) 
	end
	UIEventListener.Get(itemgo).onClick = DelegateFactory.VoidDelegate(itemclick)
end

function CLuaDwWusesiHeChengWnd:Repaint()
	local have1 = self:InitItem(self.Item1,21040432) 
	local have2 = self:InitItem(self.Item2,21040433)
	local have3 = self:InitItem(self.Item3,21040434)
	local have4 = self:InitItem(self.Item4,21040435)
	local have5 = self:InitItem(self.Item5,21040436)
	local min = math.min(have1,have2,have3,have4,have5)
	self.m_enable = min > 0

	self.NumInput:SetInputEnabled(self.m_enable)
	self.NumInput:EnableButtons(self.m_enable)

	if self.m_enable then
		self.NumInput:SetMinMax(1,min,1)
		self.NumInput:SetValue(1,true)
	end
	local data = Item_Item.GetData(21040431)
	if data ~= nil then 
		local gray = not self.m_enable
		local trans = self.TargetItem.transform

		Extensions.SetLocalPositionZ(trans,gray and -1 or 0)

	end
end

--初始化一个道具
function CLuaDwWusesiHeChengWnd:InitItem(go,cid)
	local data = Item_Item.GetData(cid) 
	if data == nil then return 0 end
	
	local iconTex = go.transform:Find("Texture"):GetComponent(typeof(CUITexture))
    local ctTxt = go.transform:Find("Name"):GetComponent(typeof(UILabel))

	iconTex:LoadMaterial(data.Icon)

	local count = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag,cid)
	local str = tostring(count)
	if count <= 0 then
		str = "[c][ff0000]0[-]/1"
	else
		str = str.."/1"
	end
	ctTxt.text = str
	return count
end


function CLuaDwWusesiHeChengWnd:OnEnable()
	local submitClick = function(go)
		self:OnSubmitBtnClick()
	end
	CommonDefs.AddOnClickListener(self.Button.gameObject, DelegateFactory.Action_GameObject(submitClick), false)
	g_ScriptEvent:AddListener("SendItem", self, "OnPlayerItemChange")

	self:AddItemClickEvent(self.Item1,21040432)
	self:AddItemClickEvent(self.Item2,21040433)
	self:AddItemClickEvent(self.Item3,21040434)
	self:AddItemClickEvent(self.Item4,21040435)
	self:AddItemClickEvent(self.Item5,21040436)
end

function CLuaDwWusesiHeChengWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SendItem", self, "OnPlayerItemChange")
	if self.delayTick then 
		UnRegisterTick(self.delayTick)
		self.delayTick = nil
	end
end

function CLuaDwWusesiHeChengWnd:OnPlayerItemChange(args)
	self:Repaint()
	--[[
	local item = CItemMgr.Inst:GetById(args[0])
	if not item then return end
	local go = self.m_items[item.TemplateId]
	if go then
		self:InitItem(go,item.TemplateId)
	end
	]]
end


--点击合成按钮
function CLuaDwWusesiHeChengWnd:OnSubmitBtnClick()

	if self.m_enable then
		self.Fxnode.gameObject:SetActive(false)
		self.Fxnode2.gameObject:SetActive(false)
		--todo：特效
		self.Fxnode:LoadFx("Fx/UI/Prefab/UI_wusesi001.prefab")
		self.Fxnode:Reset()

		if self.delayTick then 
			UnRegisterTick(self.delayTick)
			self.delayTick = nil
		end

		local func = function()
			self.Fxnode2:LoadFx("Fx/UI/Prefab/UI_wusesi002.prefab")
			self.Fxnode2:Reset()
		end
		self.delayTick = RegisterTickOnce(func,2 * 1000)

		local groupid = 6
		local count = self.NumInput:GetValue()
		Gac2Gas.RequestExchangeItems(groupid,count)
	else
		g_MessageMgr:ShowMessage("Wusesi_Material_Not_Enough")--提示材料不足
	end
end

