require("common/common_include")

local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local Color = import "UnityEngine.Color"
local BoxCollider = import "UnityEngine.BoxCollider"
local DelegateFactory = import "DelegateFactory"
local UISprite = import "UISprite"
local CPaintTextureDLN = import "L10.UI.CPaintTextureDLN"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CUIFx = import "L10.UI.CUIFx"

LuaShenbingDuliniangWnd = class()
RegistClassMember(LuaShenbingDuliniangWnd,"CloseBtn")

RegistClassMember(LuaShenbingDuliniangWnd,"ShowBg")
RegistClassMember(LuaShenbingDuliniangWnd,"ProgressNode")
RegistClassMember(LuaShenbingDuliniangWnd,"DrawNode")

local PosName = {
	LocalString.GetString("头发"),
	LocalString.GetString("面部"),
	LocalString.GetString("衣裳"),
	LocalString.GetString("饰物"),
	LocalString.GetString("眼眸"),
}

function LuaShenbingDuliniangWnd:Close()
	CUIManager.CloseUI(CLuaUIResources.ShenbingDuliniangWnd)
end

function LuaShenbingDuliniangWnd:Finish()
	Gac2Gas.FinishDuLiNiangPicTask(LuaShenbingMgr.DLNTaskId)
	RegisterTickWithDuration(function ()
		self:Close()
	end,2000,2000)
end

function LuaShenbingDuliniangWnd:SetStateStart(state)
	LuaShenbingMgr.DLNState = state

	if LuaShenbingMgr.DLNState > #self.ProgressNodeTable then
		self:Finish()
		return
	end
	RegisterTickWithDuration(function ()
		g_MessageMgr:ShowMessage("Paint_Area_Next",PosName[LuaShenbingMgr.DLNState])
	end,1000,1000)
	if self.ProgressNodeTable[LuaShenbingMgr.DLNState] then
		self.ProgressNodeTable[LuaShenbingMgr.DLNState].transform:Find("name"):GetComponent(typeof(UILabel)).color = Color.yellow
	end

	if self.DrawNodeTable[LuaShenbingMgr.DLNState] then
		self.DrawNodeTable[LuaShenbingMgr.DLNState]:GetComponent(typeof(CPaintTextureDLN)).FinishSign = false
		self.DrawNodeTable[LuaShenbingMgr.DLNState]:GetComponent(typeof(BoxCollider)).enabled = true
	end


end

function LuaShenbingDuliniangWnd:SetStateEnd(state)
	if self.ProgressNodeTable[LuaShenbingMgr.DLNState] then
		g_MessageMgr:ShowMessage("Paint_Area_Done",PosName[LuaShenbingMgr.DLNState])
		self.ProgressNodeTable[LuaShenbingMgr.DLNState].transform:Find("name"):GetComponent(typeof(UILabel)).color = Color.white
		self.ProgressNodeTable[LuaShenbingMgr.DLNState].transform:Find("bg"):GetComponent(typeof(UISprite)).fillAmount = 1
		self.ProgressNodeTable[LuaShenbingMgr.DLNState].transform:Find("fxNode"):GetComponent(typeof(CUIFx)):LoadFx("Fx/UI/Prefab/UI_duliniang_jindu03.prefab")
		if #self.ProgressNodeTable > LuaShenbingMgr.DLNState then
			self.ProgressNodeTable[LuaShenbingMgr.DLNState].transform:Find("line").gameObject:SetActive(true)
		end

		LuaShenbingMgr.DLNState = LuaShenbingMgr.DLNState + 1
		if self.DrawNodeTable[LuaShenbingMgr.DLNState] then
			self:SetStateStart(LuaShenbingMgr.DLNState)
		else
			self:Finish()
		end
	end
end

function LuaShenbingDuliniangWnd:SetPercent(percent)
	if self.ProgressNodeTable[LuaShenbingMgr.DLNState] and percent[0] then
		self.ProgressNodeTable[LuaShenbingMgr.DLNState].transform:Find("bg"):GetComponent(typeof(UISprite)).fillAmount = percent[0]
	end
end

function LuaShenbingDuliniangWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateDLNDraw", self, "SetStateEnd")
	g_ScriptEvent:AddListener("UpdateDLNState", self, "SetPercent")
end

function LuaShenbingDuliniangWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateDLNDraw", self, "SetStateEnd")
	g_ScriptEvent:RemoveListener("UpdateDLNState", self, "SetPercent")
end

function LuaShenbingDuliniangWnd:Init()
	LuaShenbingMgr.DLNState = 1
	self.ProgressNode:SetActive(false)

	local onCloseClick = function(go)
    MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("Paint_Not_Done"), DelegateFactory.Action(function ()
      self:Close()
    end), nil, nil, nil, false)
	end
	CommonDefs.AddOnClickListener(self.CloseBtn,DelegateFactory.Action_GameObject(onCloseClick),false)

	--local showBgUIWidget = self.ShowBg:GetComponent(typeof(UIWidget))
	--TweenHeight.Begin(showBgUIWidget, 1, 1000)

	self.ProgressNodeTable = {}
	self.DrawNodeTable = {}

	local drawNodeName = {"dln_toufa","dln_lian","dln_yifu","dln_shoushi","dln_yanjing"}
	for i,v in ipairs(drawNodeName) do
		local node = self.DrawNode.transform:Find(v)
		if node then
			node:GetComponent(typeof(CPaintTextureDLN)).FinishSign = true
			node:GetComponent(typeof(BoxCollider)).enabled = false
			table.insert(self.DrawNodeTable, node)
		end
	end

	for i,v in ipairs(drawNodeName) do
		local node = self.ProgressNode.transform:Find(v)
		if node then
			node.transform:Find("name"):GetComponent(typeof(UILabel)).color = Color.grey
			table.insert(self.ProgressNodeTable,node)
		end
	end

	self:SetStateStart(1)

	--RegisterTickWithDuration(function ()
		self.ProgressNode:SetActive(true)
	--end,1000,1000)
end

return LuaShenbingDuliniangWnd
