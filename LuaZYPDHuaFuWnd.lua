local QnButton = import "L10.UI.QnButton"
local GameObject = import "UnityEngine.GameObject"
local Animation = import "UnityEngine.Animation"
local CBrush = import "L10.Game.CBrush"
local MouseEvent = import "L10.Game.CBrush+MouseEvent"
local DelegateFactory  = import "DelegateFactory"
local CUITexture = import "L10.UI.CUITexture"
local Vector2 = import "UnityEngine.Vector2"
local Physics = import "UnityEngine.Physics"
local LayerDefine = import "L10.Engine.LayerDefine"
LuaZYPDHuaFuWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaZYPDHuaFuWnd, "DoneBtn", "DoneBtn", QnButton)
RegistChildComponent(LuaZYPDHuaFuWnd, "FuTexture", "FuTexture", CUITexture)
RegistChildComponent(LuaZYPDHuaFuWnd, "CloseButton", "CloseButton", GameObject)
RegistChildComponent(LuaZYPDHuaFuWnd, "FuTexture_xs", "FuTexture_xs", CUITexture)

--@endregion RegistChildComponent end

RegistClassMember(LuaZYPDHuaFuWnd, "m_Brush")
RegistClassMember(LuaZYPDHuaFuWnd, "m_HasBrush")
RegistClassMember(LuaZYPDHuaFuWnd, "m_Animation")
RegistClassMember(LuaZYPDHuaFuWnd, "m_AnimationTick")
RegistClassMember(LuaZYPDHuaFuWnd, "m_IsClickBtn")

function LuaZYPDHuaFuWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.DoneBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnDoneBtnClick()
	end)


    --@endregion EventBind end
	self.m_Animation = self.transform:GetComponent(typeof(Animation))
	self.m_IsClickBtn = false
end

function LuaZYPDHuaFuWnd:Init()
	self.m_HasBrush = false
	self:InitBrush()

end

function LuaZYPDHuaFuWnd:OnDisable()
	if self.m_AnimationTick then
		UnRegisterTick(self.m_AnimationTick)
		self.m_AnimationTick = nil
	end
end

function LuaZYPDHuaFuWnd:InitBrush()
	if self.m_Brush then
		self.m_Brush:Destory()
	end
	self.m_Brush = CBrush()
	--local texture = CreateFromClass(Texture2D, self.FuTexture.texture.width,self.FuTexture.texture.height, TextureFormat.RGBA32, false)
	self.m_Brush:Init(self.FuTexture.texture.width,self.FuTexture.texture.height, NGUIText.ParseColor24("3a55a5", 0), Color(0,0,0,0), nil, 0.4, 1)
	--NGUITools.SetLayer(self.HuaFuView.transform:Find("Panel").gameObject,LayerDefine.UI)
	--NGUITools.SetLayer(self.HuaFuView.transform:Find("Button").gameObject,LayerDefine.UI)
end

function LuaZYPDHuaFuWnd:Update()
	if not self.m_Brush then return end
	local pos = self:GetMouseCoord()
	if pos == nil then return end
	if Input.GetMouseButtonDown(0) then
		self.m_Brush:MouseDraw(MouseEvent.MouseButtonDown,pos)
		self.m_HasBrush = true
	end
	if Input.GetMouseButton(0) then
		self.m_Brush:MouseDraw(MouseEvent.MouseButton,pos)
		self.m_HasBrush = true
	end
	if Input.GetMouseButtonUp(0) then
		self.m_Brush:MouseDraw(MouseEvent.MouseButtonUp,pos)
		self.m_HasBrush = true
	end
	local rt = self.m_Brush:GetResult()
	self.FuTexture.texture.mainTexture = rt
end

function LuaZYPDHuaFuWnd:GetMouseCoord()
	--local pos = Vector2(Input.mousePosition.x, Input.mousePosition.y)
	if UICamera.currentCamera == nil then
		return nil
	end
	local ray = UICamera.currentCamera:ScreenPointToRay(Input.mousePosition)
	local hits = Physics.RaycastAll(ray, 99999, bit.lshift(1,LayerDefine.UI))
	for i = 0, hits.Length - 1 do
		local collider = hits[i].collider.gameObject
		if collider.gameObject == self.FuTexture.gameObject then
			local worldPoint = hits[i].point
			local localPoint = self.FuTexture.transform:InverseTransformPoint(worldPoint)
			local weightAndHeight = Vector2(self.FuTexture.texture.width,self.FuTexture.texture.height)
			local newpos = Vector2(localPoint.x + weightAndHeight.x / 2, localPoint.y + weightAndHeight.y / 2)
			return newpos
		end
	end
	return nil
end

function LuaZYPDHuaFuWnd:OnDestory()
	if self.m_Brush then
		self.m_Brush:Destory()
	end
end

function LuaZYPDHuaFuWnd:OnDisable()
	if not self.m_IsClickBtn and (LuaZhongYuanJie2023Mgr.stage == 1) then
		CUIManager.ShowUI(CLuaUIResources.ZYPDHuaFuWnd)
	end
end

--@region UIEvent

function LuaZYPDHuaFuWnd:OnDoneBtnClick()
	if not self.m_HasBrush then
		g_MessageMgr:ShowMessage("ZhongYuanPuDu_Not_Draw")
		return
	end

	self.CloseButton:SetActive(false)
	self.FuTexture_xs.texture.mainTexture = self.FuTexture.texture.mainTexture
	self.m_Animation:Play("zypdhuafuwnd_xiaoahi01")
	self.m_AnimationTick = RegisterTickOnce(function ()
		if self.m_AnimationTick then
			UnRegisterTick(self.m_AnimationTick)
			self.m_AnimationTick = nil
		end
		self.m_IsClickBtn = true
		Gac2Gas.ZhongYuanPuDu_NotifyFlowChart("")
		CUIManager.CloseUI(CLuaUIResources.ZYPDHuaFuWnd)
	end, self.m_Animation:get_Item("zypdhuafuwnd_xiaoahi01").length * 1000)
end


--@endregion UIEvent

