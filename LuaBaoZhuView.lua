local CButton = import "L10.UI.CButton"
local CGamePlayMgr=import "L10.Game.CGamePlayMgr"
local CScene = import "L10.Game.CScene"

LuaBaoZhuView = class()

RegistChildComponent(LuaBaoZhuView, "TaskName", UILabel)
RegistChildComponent(LuaBaoZhuView, "TaskInfo", GameObject)
RegistChildComponent(LuaBaoZhuView, "InviteBtn", CButton)
RegistChildComponent(LuaBaoZhuView, "LeaveBtn", CButton)
RegistChildComponent(LuaBaoZhuView, "bg", UISprite)
RegistChildComponent(LuaBaoZhuView, "TaskDes1", UILabel)
RegistChildComponent(LuaBaoZhuView, "TaskDes2", UILabel)
RegistChildComponent(LuaBaoZhuView, "TaskDes3", UILabel)

function LuaBaoZhuView:Awake()
  self.TaskName.text = nil
  --self.TaskInfo.text = nil
end

function LuaBaoZhuView:Init()
  UIEventListener.Get(self.LeaveBtn.gameObject).onClick = LuaUtils.VoidDelegate(function(go)
    CGamePlayMgr.Inst:LeavePlay()
  end)
  UIEventListener.Get(self.InviteBtn.gameObject).onClick = LuaUtils.VoidDelegate(function(go)
    g_MessageMgr:ShowMessage('BaoZhuPlayTip')
  end)

  self:InitPhaText()
end

function LuaBaoZhuView:InitPhaText()
  local sceneName = CScene.MainScene.SceneName
  local pha1Text,pha2Text =
    g_MessageMgr:FormatMessage("BaoZhuPlayPha1"),
    g_MessageMgr:FormatMessage("BaoZhuPlayPha2")

  local showText = '[c][FFC800]' .. sceneName .. '[-][c]'
  self.TaskName.text = showText
  local textTable = {pha1Text,pha2Text}

  if LuaBaoZhuPlayMgr.PhaData then
    for i,v in ipairs(textTable) do
      if LuaBaoZhuPlayMgr.PhaData[3] == i then
        textTable[i] = '[c][FFFE91]' .. v .. '[-][c]'
      else
        textTable[i] = v
      end
    end

    if LuaBaoZhuPlayMgr.PhaData[3] == 1 then
      self.TaskDes1.text = textTable[1]
      self.TaskDes2.text = '[c][AAFFFF]' .. SafeStringFormat3(LocalString.GetString('剩余人数：%s/%s'),LuaBaoZhuPlayMgr.PhaData[1],LuaBaoZhuPlayMgr.PhaData[2]) .. '[-][c]'
      self.TaskDes3.text = textTable[2]
    elseif LuaBaoZhuPlayMgr.PhaData[3] == 2 then
      self.TaskDes1.text = textTable[1]
      self.TaskDes2.text = textTable[2]
      self.TaskDes3.text = '[c][AAFFFF]' .. SafeStringFormat3(LocalString.GetString('剩余人数：%s/%s'),LuaBaoZhuPlayMgr.PhaData[1],LuaBaoZhuPlayMgr.PhaData[2]) .. '[-][c]'
    else
      self.TaskDes1.text = textTable[1]
      self.TaskDes2.text = textTable[2]
      self.TaskDes3.text = ''
    end
  else
    self.TaskDes1.text = textTable[1]
    self.TaskDes2.text = textTable[2]
    self.TaskDes3.text = ''
  end

  --self.TaskDes1:ResetAndUpdateAnchors()
  --self.TaskDes2:ResetAndUpdateAnchors()
  --self.TaskDes3:ResetAndUpdateAnchors()
  --self.TaskInfo:ResetAndUpdateAnchors()
  --self.bg:ResetAndUpdateAnchors()
end

function LuaBaoZhuView:OnEnable()
  g_ScriptEvent:AddListener("UpdateBaoZhuPlayPha", self, "InitPhaText")
end

function LuaBaoZhuView:OnDisable()
  g_ScriptEvent:RemoveListener("UpdateBaoZhuPlayPha", self, "InitPhaText")
end

return LuaBaoZhuView
