local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnTableView = import "L10.UI.QnTableView"

LuaGuildLeagueResultLookUpWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaGuildLeagueResultLookUpWnd, "GroupTable", "GroupTable", QnTableView)
RegistChildComponent(LuaGuildLeagueResultLookUpWnd, "InfoTable", "InfoTable", QnTableView)
RegistChildComponent(LuaGuildLeagueResultLookUpWnd, "Highlight1", "Highlight1", GameObject)
RegistChildComponent(LuaGuildLeagueResultLookUpWnd, "Highlight2", "Highlight2", GameObject)
RegistChildComponent(LuaGuildLeagueResultLookUpWnd, "Highlight3", "Highlight3", GameObject)
RegistChildComponent(LuaGuildLeagueResultLookUpWnd, "HaveTeam", "HaveTeam", GameObject)
RegistChildComponent(LuaGuildLeagueResultLookUpWnd, "NoTeam", "NoTeam", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaGuildLeagueResultLookUpWnd, "m_MatchList")
RegistClassMember(LuaGuildLeagueResultLookUpWnd, "m_Level")
RegistClassMember(LuaGuildLeagueResultLookUpWnd, "m_CurrentRound")

function LuaGuildLeagueResultLookUpWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.Highlight1:SetActive(false)
    self.Highlight2:SetActive(false)
    self.Highlight3:SetActive(false)

    self.NoTeam:SetActive(false)
    self.HaveTeam:SetActive(false)
    self.m_CurrentRound = 0
end

function LuaGuildLeagueResultLookUpWnd:Init()
    self.GroupTable.m_DataSource = DefaultTableViewDataSource.Create(function() 
            return 22
        end, function(item, index)
            local tf = item.transform
            local nameLabel = tf:Find("Label"):GetComponent(typeof(UILabel))
            if LuaGuildLeagueCrossMgr:IsOpenedOnMyServer() then
                nameLabel.text = SafeStringFormat3(LocalString.GetString("%s组"), Extensions.ConvertToChineseString(index+1))
            else
                nameLabel.text = LuaGuildLeagueMgr.GetLevelStr(index+1)
            end

        end)
    self.GroupTable.OnSelectAtRow =DelegateFactory.Action_int(function (row)
            self.m_MatchList = {}
            self.m_Level = row+1
            self.InfoTable:ReloadData(true,false)
            self.NoTeam:SetActive(true)
            self.HaveTeam:SetActive(false)
            Gac2Gas.QueryGLMatchInfo(row+1)
        end)

    self.InfoTable.m_DataSource = DefaultTableViewDataSource.Create(function() 
            return #self.m_MatchList
        end, function(item, index)
            local tf = item.transform
            self:InitMatchItem(tf,index)
        end)
    self.GroupTable:ReloadData(true,false)
    self.GroupTable:SetSelectRow(0,true)
end

--@region UIEvent

--@endregion UIEvent

function LuaGuildLeagueResultLookUpWnd:OnEnable()
    g_ScriptEvent:AddListener("QueryGLMatchInfoResult",self,"OnQueryGLMatchInfoResult")

end
function LuaGuildLeagueResultLookUpWnd:OnDisable()
    g_ScriptEvent:RemoveListener("QueryGLMatchInfoResult",self,"OnQueryGLMatchInfoResult")
end

function LuaGuildLeagueResultLookUpWnd:OnQueryGLMatchInfoResult(level,t)
    if self.m_Level~=level then return end

    if t and #t>0 then
        self.NoTeam:SetActive(false)
        self.HaveTeam:SetActive(true)
    else
        self.NoTeam:SetActive(true)
        self.HaveTeam:SetActive(false)
    end

    local matchInfo = {}
    matchInfo[1] = t[1][1]
    matchInfo[2] = t[1][3]
    matchInfo[3] = t[1][5]
    matchInfo[4] = t[1][7]

    if t[1] and t[1][1] then
        matchInfo[1] = t[1][1]
    else
        matchInfo[1] = {
            guildName = LocalString.GetString("轮空"),
            otherGuildName = LocalString.GetString("轮空"),
        }
    end
    if t[1] and t[1][3] then
        matchInfo[2] = t[1][3]
    else
        matchInfo[2] = {
            guildName = LocalString.GetString("轮空"),
            otherGuildName = LocalString.GetString("轮空"),
        }
    end
    if t[1] and t[1][5] then
        matchInfo[3] = t[1][5]
    else
        matchInfo[3] = {
            guildName = LocalString.GetString("轮空"),
            otherGuildName = LocalString.GetString("轮空"),
        }
    end
    if t[1] and t[1][7] then
        matchInfo[4] = t[1][7]
    else
        matchInfo[4] = {
            guildName = LocalString.GetString("轮空"),
            otherGuildName = LocalString.GetString("轮空"),
        }
    end
    
    if t[2] and t[2][1] then
        matchInfo[5] = t[2][1]
    else
        matchInfo[5] = {
            noinfo = true,
            guildName = SafeStringFormat3(LocalString.GetString("第%d场胜者"),1),
            otherGuildName = SafeStringFormat3(LocalString.GetString("第%d场胜者"),2),
        }
    end
    if t[2] and t[2][5] then
        matchInfo[6] = t[2][5]
    else
        matchInfo[6] = {
            noinfo = true,
            guildName = SafeStringFormat3(LocalString.GetString("第%d场败者"),1),
            otherGuildName = SafeStringFormat3(LocalString.GetString("第%d场败者"),2),
        }
    end
    if t[2] and t[2][3] then
        matchInfo[7] = t[2][3]
    else
        matchInfo[7] = {
            noinfo = true,
            guildName = SafeStringFormat3(LocalString.GetString("第%d场胜者"),3),
            otherGuildName = SafeStringFormat3(LocalString.GetString("第%d场胜者"),4),
        }
    end
    if t[2] and t[2][7] then
        matchInfo[8] = t[2][7]
    else
        matchInfo[8] = {
            noinfo = true,
            guildName = SafeStringFormat3(LocalString.GetString("第%d场败者"),3),
            otherGuildName = SafeStringFormat3(LocalString.GetString("第%d场败者"),4),
        }
    end


    if t[3] and t[3][7] then
        matchInfo[9] = t[3][7]
    else
        matchInfo[9] = {
            noinfo = true,
            desc = LocalString.GetString("七八名之战"),
            guildName = SafeStringFormat3(LocalString.GetString("第%d场败者"),6),
            otherGuildName = SafeStringFormat3(LocalString.GetString("第%d场败者"),8),
        }
    end
    if t[3] and t[3][5] then
        matchInfo[10] = t[3][5]
    else
        matchInfo[10] = {
            noinfo = true,
            desc = LocalString.GetString("五六名之战"),
            guildName = SafeStringFormat3(LocalString.GetString("第%d场胜者"),6),
            otherGuildName = SafeStringFormat3(LocalString.GetString("第%d场胜者"),8),
        }
    end
    if t[3] and t[3][3] then
        matchInfo[11] = t[3][3]
    else
        matchInfo[11] = {
            noinfo = true,
            desc = LocalString.GetString("三四名之战"),
            guildName = SafeStringFormat3(LocalString.GetString("第%d场败者"),5),
            otherGuildName = SafeStringFormat3(LocalString.GetString("第%d场败者"),7),
        }
    end

    if t[3] and t[3][1] then
        matchInfo[12] = t[3][1]
    else
        matchInfo[12] = {
            noinfo = true,
            desc = LocalString.GetString("一二名之战"),
            guildName = SafeStringFormat3(LocalString.GetString("第%d场胜者"),5),
            otherGuildName = SafeStringFormat3(LocalString.GetString("第%d场胜者"),7),
        }
    end
    self.m_MatchList = matchInfo

    self.Highlight1:SetActive(false)
    self.Highlight2:SetActive(false)
    self.Highlight3:SetActive(false)
    self.m_CurrentRound = 1
    if t[3] then
        self.m_CurrentRound = 3
        self.Highlight3:SetActive(true)
    elseif t[2] then
        self.m_CurrentRound = 2
        self.Highlight2:SetActive(true)
    else
        self.Highlight1:SetActive(true)
    end

    self.InfoTable:ReloadData(true,false)

end

function LuaGuildLeagueResultLookUpWnd:InitMatchItem(tf,index)
    local idLabel = tf:Find("IdLabel"):GetComponent(typeof(UILabel))
    local descLabel = tf:Find("DescLabel"):GetComponent(typeof(UILabel))
    idLabel.text = tostring(index+1)
    descLabel.text = nil

    local id = index+1


    local group1Label = tf:Find("Group1Label"):GetComponent(typeof(UILabel))
    local group2Label = tf:Find("Group2Label"):GetComponent(typeof(UILabel))
    local result1Sprite = tf:Find("Result1"):GetComponent(typeof(UISprite))
    local result2Sprite = tf:Find("Result2"):GetComponent(typeof(UISprite))

    local info = self.m_MatchList[id]
    group1Label.text = info.guildName
    group2Label.text = info.otherGuildName

    
    if self.m_CurrentRound==1 and id>4 then
        group1Label.alpha = 0.3
        group2Label.alpha = 0.3
    elseif self.m_CurrentRound==2 and id>8 then
        group1Label.alpha = 0.3
        group2Label.alpha = 0.3
    elseif info.noinfo then
        group1Label.alpha = 0.3
        group2Label.alpha = 0.3
    else
        group1Label.alpha = 1
        group2Label.alpha = 1
    end

    if info.winGuildName==LocalString.GetString("未开始") or info.winGuildName==LocalString.GetString("进行中") then
        result1Sprite.spriteName = nil
        result2Sprite.spriteName = nil
    elseif info.winGuildName and info.winGuildName~="" then
        result1Sprite.spriteName = info.guildName==info.winGuildName and "common_fight_result_win" or "common_fight_result_lose"
        result2Sprite.spriteName = info.otherGuildName==info.winGuildName and "common_fight_result_win" or "common_fight_result_lose"
    else
        result1Sprite.spriteName = nil
        result2Sprite.spriteName = nil
    end
    
    if group1Label.text == LocalString.GetString("轮空") then
        group1Label.alpha = 0.3
        result1Sprite.spriteName = nil
    end
    if group2Label.text == LocalString.GetString("轮空") then
        group2Label.alpha = 0.3
        result2Sprite.spriteName = nil
    end

    if id==9 then
        descLabel.text = LocalString.GetString("七八名之战")
    elseif id==10 then
        descLabel.text = LocalString.GetString("五六名之战")
    elseif id==11 then
        descLabel.text = LocalString.GetString("三四名之战")
    elseif id==12 then
        descLabel.text = LocalString.GetString("一二名之战")
    end
    
end
