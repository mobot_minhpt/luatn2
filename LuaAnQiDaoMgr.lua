local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local MsgPackImpl = import "MsgPackImpl"
local CScene = import "L10.Game.CScene"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local CEffectMgr = import "L10.Game.CEffectMgr"

CLuaAnQiDaoMgr={}

--安期岛之战
CLuaAnQiDaoMgr.m_HaicangHealth=0
CLuaAnQiDaoMgr.m_DafuHealth=0
CLuaAnQiDaoMgr.m_HaicangPlayInfoList={}
CLuaAnQiDaoMgr.m_DafuPlayInfoList={}
CLuaAnQiDaoMgr.m_MainPlayerForce=nil
CLuaAnQiDaoMgr.m_FightTime=nil
CLuaAnQiDaoMgr.haicangInfoList=nil
CLuaAnQiDaoMgr.dafuInfoList=nil
CLuaAnQiDaoMgr.enableShare=true

function CLuaAnQiDaoMgr.OnEnterPlay()
    if CUIManager.IsLoaded(CLuaUIResources.AnQiDaoResultWnd) then
        CUIManager.CloseUI(CLuaUIResources.AnQiDaoResultWnd)
    end

    if CUIManager.IsLoaded(CUIResources.GuanNingResultWnd) then
        CUIManager.CloseUI(CUIResources.GuanNingResultWnd)
    end

    CLuaGuanNingMgr.m_IsGameEnd=false
    CLuaGuanNingMgr.m_ScoreRecords={}
    CLuaGuanNingMgr.m_SelectedPlayerId=0
    CLuaGuanNingMgr.m_Score=0
    CLuaGuanNingMgr.m_LeftDoubleTimes=0 --双倍积分
    CLuaGuanNingMgr.lastBattleData=nil
    CLuaGuanNingMgr.historyMonthData=nil

    CLuaAnQiDaoMgr.m_HaicangHealth=0
    CLuaAnQiDaoMgr.m_DafuHealth=0
    CLuaAnQiDaoMgr.m_HaicangPlayInfoList={}
    CLuaAnQiDaoMgr.m_DafuPlayInfoList={}
end

-- 同步个人分数信息
-- @param sourceInfoUD 分数信息
-- {结果, 势力, 得分, 职业排名, 是否为本场最高分, 是否本势力最高分
-- 击杀数, 死亡数, 助攻数, 复活数,  最大连杀, 采集弹药数, 击杀boss数
-- 本场前最高得分, 本场前最高击杀数, 本场前最高死亡数, 本场前最高助攻数, 本场前最高复活数}
-- @param maxInfoUD 同阵营最大值信息 数组 对应在EnumGuanNingWeekDataKey
function CLuaAnQiDaoMgr.UpdateAnQiIslandPlayerPersonalData(sourceInfoUD, maxInfoUD)

    CLuaGuanNingMgr.lastBattleData = nil

    local scoreInfo = MsgPackImpl.unpack(sourceInfoUD)
    local maxInfo = MsgPackImpl.unpack(maxInfoUD)

    if scoreInfo and scoreInfo.Count>17 then
        local battleData={
            timeStamp = CServerTimeMgr.Inst.timeStamp,
            result = tonumber(scoreInfo[0]),
            force = tonumber(scoreInfo[1]),
            score = tonumber(scoreInfo[2]),
            rank = tonumber(scoreInfo[3]),
            isMaxScore = tonumber(scoreInfo[4]),
            isMaxScoreInForce = tonumber(scoreInfo[5]),

            killNum = tonumber(scoreInfo[6]),
            dieNum = tonumber(scoreInfo[7]),
            helpNum = tonumber(scoreInfo[8]),
            reliveNum = tonumber(scoreInfo[9]),
            maxLianSha = tonumber(scoreInfo[10]),
            getBulletNum = tonumber(scoreInfo[11]),
            killBossNum = tonumber(scoreInfo[12]),

            lastMaxPlayScore = tonumber(scoreInfo[13]),
            lastMaxPlayKillNum = tonumber(scoreInfo[14]),
            lastMaxPlayDieNum = tonumber(scoreInfo[15]),
            lastMaxPlayHelpNum = tonumber(scoreInfo[16]),
            lastMaxPlayReliveNum = tonumber(scoreInfo[17]),
            achieveInfo = {},

            isAnQiDaoData = true,
        }
        if maxInfo then
            for i=1,maxInfo.Count do
                local key = tonumber(maxInfo[i-1])
                --网站暂不支持这两key
                if key ~= EnumGuanNingWeekDataKey.eMaxCannonPickInForce and key ~= EnumGuanNingWeekDataKey.eMaxBosskillInForce then
                    table.insert(battleData.achieveInfo,tonumber(maxInfo[i-1]))
                end
            end
        end
        CLuaGuanNingMgr.lastBattleData = battleData
    end
end

-- 副本信息 防守->海沧 进攻->大福
-- @param cannonInfoUD 火炮信息 {防守方弹药总数, 防守方当前弹药, 防守方血量, 进攻方弹药总数, 进攻方当前弹药, 进攻方血量}
-- @param defendDataUD 防守方玩家信息 {id1, name1, class1, (kill+dead)score1, reloadScore1, ... }
-- @param attackDataUD 进攻方玩家信息 {id1, name1, class1, (kill+dead)score1, reloadScore1, ... }
-- @param isFinish 是否已进入结束状态
-- @param doubleTimes 玩家剩余双倍经验次数
function CLuaAnQiDaoMgr.SendAnQiIslandPlayInfoResult(cannonInfoUD, defendDataUD, attackDataUD, isFinish, doubleTimes)

    CLuaGuanNingMgr.m_IsGameEnd=isFinish
    CLuaAnQiDaoMgr.m_HaicangPlayInfoList={}
    CLuaAnQiDaoMgr.m_DafuPlayInfoList={}
    CLuaGuanNingMgr.m_LeftDoubleTimes=doubleTimes

    local cannonInfo=MsgPackImpl.unpack(cannonInfoUD)
    CLuaAnQiDaoMgr.haicangInfoList = {cannonInfo[2], cannonInfo[0], cannonInfo[1]}
    CLuaAnQiDaoMgr.dafuInfoList = {cannonInfo[5], cannonInfo[3], cannonInfo[4]}
    CLuaAnQiDaoMgr.m_HaicangHealth = cannonInfo[2]
    CLuaAnQiDaoMgr.m_DafuHealth = cannonInfo[5]

    g_ScriptEvent:BroadcastInLua("AnQiDaoSyncState")

    local list1=MsgPackImpl.unpack(defendDataUD)
    local list2=MsgPackImpl.unpack(attackDataUD)

    --fightScore->score11 occupyScore->reloadScore1 不改键名,复用
    for i=1,list1.Count,5 do
        local playInfo={
            playerId = tonumber(list1[i-1]),
            name = tostring(list1[i]),
            cls = tonumber(list1[i+1]),
            fightScore = tonumber(list1[i+2]),
            occupyScore = tonumber(list1[i+3]),
        }
        playInfo.score=playInfo.fightScore+playInfo.occupyScore
        table.insert( CLuaAnQiDaoMgr.m_HaicangPlayInfoList, playInfo)
    end
    for i=1,list2.Count,5 do
        local playInfo={
            playerId = tonumber(list2[i-1]),
            name = tostring(list2[i]),
            cls = tonumber(list2[i+1]),
            fightScore = tonumber(list2[i+2]),
            occupyScore = tonumber(list2[i+3]),
        }
        playInfo.score=playInfo.fightScore+playInfo.occupyScore
        table.insert( CLuaAnQiDaoMgr.m_DafuPlayInfoList, playInfo)
    end

    local sortFunc=function(a,b)
        if a.fightScore + a.occupyScore>b.fightScore+b.occupyScore then
            return true
        elseif a.fightScore + a.occupyScore<b.fightScore+b.occupyScore then
            return false
        elseif a.fightScore>b.fightScore then
            return true
        elseif a.fightScore<b.fightScore then
            return false
        else
            return a.playerId<b.playerId
        end
    end
    table.sort( CLuaAnQiDaoMgr.m_HaicangPlayInfoList,sortFunc)
    table.sort( CLuaAnQiDaoMgr.m_DafuPlayInfoList,sortFunc)

    g_ScriptEvent:BroadcastInLua("QueryAnQiIslandPlayInfo")
end

-- 本场最佳数据
-- @param maxDataInfo
-- {[EnumCommonForce.eDefend] = {[k1] = {playerIdTbl = {id1, ...}, value = 0}, [k2] = {...}},
--  [EnumCommonForce.eAttack] = {...}}
-- k1, k2, ... 参考EnumGuanNingWeekDataKey 27-31 36 37
function CLuaAnQiDaoMgr.SendAnQiIslandMaxDataInfo(maxDataInfo)
    local dic=MsgPackImpl.unpack(maxDataInfo)

    CLuaGuanNingMgr.m_ScoreRecords={}

    local function processOnRecord(d,key)
        local set=CommonDefs.DictGetValue_LuaCall(d,tostring(key))
        if set then
            local value=CommonDefs.DictGetValue_LuaCall(set,"value")
            if value and tonumber(value)>0 then
                local playerIdTbl=CommonDefs.DictGetValue_LuaCall(set,"playerIdTbl")
                if playerIdTbl then
                    CommonDefs.DictIterate(playerIdTbl,DelegateFactory.Action_object_object(function (___key, ___value)
                        local playerId=tonumber(___key)
                        if not CLuaGuanNingMgr.m_ScoreRecords[playerId] then
                            CLuaGuanNingMgr.m_ScoreRecords[playerId]={}
                        end
                        -- print("maxdata",playerId,key)
                        table.insert( CLuaGuanNingMgr.m_ScoreRecords[playerId], key )
                    end))
                end
            end
        end
    end

    local dic1 = CommonDefs.DictGetValue_LuaCall(dic,"0")
    if dic1 then
        processOnRecord(dic1,EnumGuanNingWeekDataKey.eMaxDpsInForce)
        processOnRecord(dic1,EnumGuanNingWeekDataKey.eMaxUnderDamageInForce)
        processOnRecord(dic1,EnumGuanNingWeekDataKey.eMaxHealInForce)
        processOnRecord(dic1,EnumGuanNingWeekDataKey.eMaxCtrlNumInForce)
        processOnRecord(dic1,EnumGuanNingWeekDataKey.eMaxCannonPickInForce)
        processOnRecord(dic1,EnumGuanNingWeekDataKey.eMaxBaoShiNumInForce)
    end
    local dic2 = CommonDefs.DictGetValue_LuaCall(dic,"1")
    if dic2 then
        processOnRecord(dic2,EnumGuanNingWeekDataKey.eMaxDpsInForce)
        processOnRecord(dic2,EnumGuanNingWeekDataKey.eMaxUnderDamageInForce)
        processOnRecord(dic2,EnumGuanNingWeekDataKey.eMaxHealInForce)
        processOnRecord(dic2,EnumGuanNingWeekDataKey.eMaxCtrlNumInForce)
        processOnRecord(dic2,EnumGuanNingWeekDataKey.eMaxCannonPickInForce)
        processOnRecord(dic2,EnumGuanNingWeekDataKey.eMaxBaoShiNumInForce)
    end
end

-- 玩家得分
-- @param score 		得分
-- @param exp 			经验
-- @param freeSilver 	银票
function CLuaAnQiDaoMgr.UpdatePlayerAnQiIslandScore(score, exp, freeSilver)

    CLuaGuanNingMgr.m_Score=score
    g_ScriptEvent:BroadcastInLua("UpdatePlayerAnQiIslandScore",score, exp, freeSilver)
end

-- 火炮发射弹药
-- @param force 势力
-- @param cnt 	发射次数
function CLuaAnQiDaoMgr.AnQiIslandFireCannon(force, cnt)
    g_ScriptEvent:BroadcastInLua("AnQiIslandFireCannon", force, cnt)
end

-- 同步火炮信息
-- @param defInfo {防守方血量, 防守方弹药总数, 防守方当前弹药}
-- @param attInfo {进攻方血量, 进攻方弹药总数, 进攻方当前弹药}
function CLuaAnQiDaoMgr.AnQiIslandSyncCannonInfo(defInfo, attInfo)
	local haicangInfo = MsgPackImpl.unpack(defInfo)
    local dafuInfo = MsgPackImpl.unpack(attInfo)
    if haicangInfo == nil or dafuInfo == nil then
        return 
    end

    CLuaAnQiDaoMgr.haicangInfoList = {haicangInfo[0], haicangInfo[1], haicangInfo[2]}
    CLuaAnQiDaoMgr.dafuInfoList = {dafuInfo[0], dafuInfo[1], dafuInfo[2]}
    
    g_ScriptEvent:BroadcastInLua("AnQiDaoSyncState")
end

function CLuaAnQiDaoMgr.ShowAnQiIslandResultWnd()
    RegisterTickOnce(function ()
        CLuaGuanNingMgr.m_IsGameEnd=true
        CUIManager.ShowUI(CLuaUIResources.AnQiDaoResultWnd)
    end, 2000)
end

-- @param fightTime 战斗开始后的时间
function CLuaAnQiDaoMgr.SendAnQiIslandCurrTime(fightTime)
    CLuaAnQiDaoMgr.m_FightTime = CServerTimeMgr.Inst:GetZone8Time():AddSeconds(-fightTime)
end

function CLuaAnQiDaoMgr.IsInPlay()
    if CScene.MainScene then
        local gamePlayId = CScene.MainScene.GamePlayDesignId
        if gamePlayId == AnQiIsland_Setting.GetData().GamePlayId then
            return true
        end
    end
    return false
end

function CLuaAnQiDaoMgr.AnQiIslandSyncCannonBallAndBossPos(pickInfo, bossInfo)
    local list1 = MsgPackImpl.unpack(pickInfo)
    local list2 = MsgPackImpl.unpack(bossInfo)

    local list = {}
    for i = 0, list1.Count-1, 3 do
        local info = {
            eId = list1[i],
            x = list1[i+1],
            y = list1[i+2],
            isPick = true
        }
        table.insert(list, info)
    end
    for i = 0, list2.Count-1, 3 do
        local info = {
            eId = list2[i],
            x = list2[i+1],
            y = list2[i+2],
            isPick = false
        }
        table.insert(list, info)
    end

    g_ScriptEvent:BroadcastInLua("AnQiIslandSyncCannonBallAndBossPos", list)
end

function CLuaAnQiDaoMgr.AnQiIslandPickCannonBall(pickEngineId, playerId)
    local fxId = AnQiIsland_Setting.GetData().PickUpFxId
    local obj = CClientObjectMgr.Inst:GetObject(pickEngineId)
    if obj ~= nil then
        --附加采集特效
		CEffectMgr.Inst:AddWorldPositionFX(fxId, obj.Pos, 0, 0, 1, -1, EnumWarnFXType.None, nil, 0, 0, nil)
        
        --附加ui特效
        if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.Id == playerId then
            g_ScriptEvent:BroadcastInLua("AnQiIslandPickCannonBall", obj.RO.TopAnchorPos)
        end
    end
end

function CLuaAnQiDaoMgr.AnQiIslandBossKilled(force)
    g_ScriptEvent:BroadcastInLua("AnQiIslandBossKilled", force)
end