require("common/common_include")

local CUITexture = import "L10.UI.CUITexture"

CLuaCommonCharacterDescWnd = class()

function CLuaCommonCharacterDescWnd:Init( ... )
	self.transform:Find("Name"):GetComponent(typeof(UILabel)).text = CLuaCommonCharacterDescMgr.m_Name
	self.transform:Find("Bg/Desc"):GetComponent(typeof(UILabel)).text = CLuaCommonCharacterDescMgr.m_Desc
	self.transform:Find("Portrait"):GetComponent(typeof(CUITexture)):LoadNPCPortrait(CLuaCommonCharacterDescMgr.m_Portrait)
end

return CLuaCommonCharacterDescWnd