local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnTableView = import "L10.UI.QnTableView"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"

LuaPengDaoRewardsListWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaPengDaoRewardsListWnd, "QnTableView", "QnTableView", QnTableView)

--@endregion RegistChildComponent end
RegistClassMember(LuaPengDaoRewardsListWnd, "m_List")

function LuaPengDaoRewardsListWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaPengDaoRewardsListWnd:Init()
    self.QnTableView.m_DataSource = DefaultTableViewDataSource.Create(
        function() 
            return self.m_List and #self.m_List or 0
        end,
        function(item, index)
            self:ItemAt(item,index)
        end)
    self.m_List = LuaPengDaoMgr:GetAwardItemData()
    self.QnTableView:ReloadData(true, false)
end

function LuaPengDaoRewardsListWnd:ItemAt(item, index)
    local icon = item.transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local label = item.transform:Find("Label"):GetComponent(typeof(UILabel))
    local mark = item.transform:Find("Mark")

    local data = self.m_List[index + 1]
    local itemId = data.ItemId
    local itemData = Item_Item.GetData(itemId)
    icon:LoadMaterial(itemData.Icon)
    if data.Difficulty > 3 then
        label.text = SafeStringFormat3(LocalString.GetString("通过第%d层\n且难度之和≥%d"), data.Stage, data.Difficulty)
    else
        label.text = SafeStringFormat3(LocalString.GetString("通过第%d层"), data.Stage)
    end

    local status = LuaPengDaoMgr.m_MainWndData.rewardData[data.Id] --  status -- 奖励状态，0和nil未发奖，1可领奖，2已领奖
    mark.gameObject:SetActive(status == 2 or status == 1)

    UIEventListener.Get(icon.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        CItemInfoMgr.ShowLinkItemTemplateInfo(data.ItemId, false, nil, AlignType.Default, 0, 0, 0, 0)
    end)
end

--@region UIEvent

--@endregion UIEvent

