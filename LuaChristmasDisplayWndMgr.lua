local CPlayerDataMgr = import "L10.Game.CPlayerDataMgr"
CLuaChristmasDisplayWndMgr={}

CLuaChristmasDisplayWndMgr.StickerOnShowData = ""
CLuaChristmasDisplayWndMgr.StickerOnShowList = {}
CLuaChristmasDisplayWndMgr.FileName = "chritmasWindowViewData"

CLuaChristmasDisplayWndMgr.Type_GuaShi = 1
CLuaChristmasDisplayWndMgr.Type_Decoration = 3
CLuaChristmasDisplayWndMgr.Type_Word = 2
CLuaChristmasDisplayWndMgr.Type_BackGround = 4

CLuaChristmasDisplayWndMgr.MaxDiagonalSize = 800

CLuaChristmasDisplayWndMgr.DecorationList = {}
CLuaChristmasDisplayWndMgr.GuashiList = {}
CLuaChristmasDisplayWndMgr.WordStickerList = {}
CLuaChristmasDisplayWndMgr.BackGroundTextureList = {}
CLuaChristmasDisplayWndMgr.isReadDataFromTable = false

CLuaChristmasDisplayWndMgr.delayShowTime = 0.2
CLuaChristmasDisplayWndMgr.savePlayerId = nil

function CLuaChristmasDisplayWndMgr.LoadChristmasWindowViewData()
    local player = CClientMainPlayer.Inst
    if player and player.Id == CLuaChristmasDisplayWndMgr.savePlayerId and next(CLuaChristmasDisplayWndMgr.StickerOnShowList) then
        return
    end

    CLuaChristmasDisplayWndMgr.StickerOnShowList = {}
    CLuaChristmasDisplayWndMgr.m_BackGroundName = nil 
    local dataString = CPlayerDataMgr.Inst:LoadPlayerData(CLuaChristmasDisplayWndMgr.FileName)
    if not dataString or dataString == "" then
        return
    end
    --local data
    for data in string.gmatch(dataString, "(.-;)") do   
        if not string.find(data,",") then
            CLuaChristmasDisplayWndMgr.m_BackGroundName = string.sub(data,1,-2)
        else
            local t = {}
            for info in string.gmatch(data, "(.-,)") do          
                local sub = string.sub(info,1,-2)
                table.insert(t,sub)
            end
            table.insert(CLuaChristmasDisplayWndMgr.StickerOnShowList,t)
        end
        
    end

    if player then
        CLuaChristmasDisplayWndMgr.savePlayerId = player.Id
    end  
end

