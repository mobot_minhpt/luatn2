local CGroupIMContentView = import "L10.UI.CGroupIMContentView"
local EChatPanel = import "L10.Game.EChatPanel"
local CChatListBaseItem = import "L10.UI.CChatListBaseItem"
local CChatHistoryMgr = import "L10.Game.CChatHistoryMgr"
local CFriendView = import "L10.UI.CFriendView"
local CChatListItem = import "L10.UI.CChatListItem"
local CGroupIMMgr = import "L10.Game.CGroupIMMgr"
local CRecordingInfoMgr = import "L10.UI.CRecordingInfoMgr"
local CShiTuMgr = import "L10.Game.CShiTuMgr"

LuaGroupIMContentView = {}
LuaGroupIMContentView.m_Wnd = nil
LuaGroupIMContentView.m_SortedTuDiList = {}

function LuaGroupIMContentView:OnSendShifuAllTudiInfo()
    if self.m_Wnd == nil then
        return
    end
    local info = CGroupIMMgr.Inst:GetGroupIM(self.m_Wnd.mGroupIMID)
    if info and LuaShiTuMgr.m_ShifuAllTudiInfo.shifuId == info.OwnerId then
        self.m_SortedTuDiList = {}
        local allTudiInfo = LuaShiTuMgr.m_ShifuAllTudiInfo.allTudiInfo
        if allTudiInfo then
            for k, v in pairs(allTudiInfo) do
                table.insert(self.m_SortedTuDiList,k)
                table.sort(self.m_SortedTuDiList,function (a,b )
                    local t1 = allTudiInfo[a]
                    local t2 = allTudiInfo[b]
                    if t1 and t2 then
                        return t1.tudiTime < t2.tudiTime
                    end
                    return a < b
                end)
            end
        end
        if self.m_Wnd then
            self.m_Wnd.tableView:Clear()
            self.m_Wnd.allData:Clear()
            self.m_Wnd.updateUUID = 0
            self.m_Wnd:UpdateChatMessages(true)
        end
    end
end

function LuaGroupIMContentView:InitSortedTuDiList(this)
    if this and LuaGroupIMMgr:IsShiTuIMId(CGroupIMContentView.mGroupIMID) then
        self.m_SortedTuDiList = {}
        local info = CGroupIMMgr.Inst:GetGroupIM(CGroupIMContentView.mGroupIMID)
        if info then
            local isShiFu = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == info.OwnerId
            if isShiFu then
                if CClientMainPlayer.Inst and CClientMainPlayer.Inst.RelationshipProp.TuDi then
                    local tudiList = CShiTuMgr.Inst:GetAllCurrentTuDi()
                    for i = 0, tudiList.Count - 1 do
                        table.insert(self.m_SortedTuDiList,tudiList[i])
                    end
                    table.sort(self.m_SortedTuDiList,function (a,b )
                        local t1 = CClientMainPlayer.Inst.RelationshipProp.TuDi[a]
                        local t2 = CClientMainPlayer.Inst.RelationshipProp.TuDi[b]
                        if t1 and t2 then
                            return t1.Time < t2.Time
                        end
                        return a < b
                    end)
                end
            else
                Gac2Gas.RequestShifuAllTudiInfo(1)
            end
        end
    end
end

function LuaGroupIMContentView:ShowShiTuChannelLabel(this,data,chatListItem)
    if data.isOpposite and this then
        if LuaGroupIMMgr:IsShiTuIMId(CGroupIMContentView.mGroupIMID) then
            local item = TypeAs(chatListItem, typeof(CChatListItem))
            if item then
                local info = CGroupIMMgr.Inst:GetGroupIM(CGroupIMContentView.mGroupIMID)
                if info then
                    local myId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
                    local isShiFu = myId == info.OwnerId
                    local title = LocalString.GetString("好友")
                    local j = 0
                    for i = 1,#self.m_SortedTuDiList do
                        local isWaiMen = false
                        if isShiFu then
                            if CClientMainPlayer.Inst then
                                local relationshipProp = CClientMainPlayer.Inst.RelationshipProp
                                local infoDic = relationshipProp.TuDi
                                local t = CommonDefs.DictGetValue(infoDic, typeof(UInt64), self.m_SortedTuDiList[i]) 
                                isWaiMen = t and (t.Extra:GetBit(2)) or false
                            end
                        else
                            local allTudiInfo = LuaShiTuMgr.m_ShifuAllTudiInfo.allTudiInfo
                            if allTudiInfo then
                                local t = allTudiInfo[self.m_SortedTuDiList[i]]
                                if t then
                                    isWaiMen = t.isWaiMen  
                                end
                            end
                        end
                        if not isWaiMen then
                            j = j + 1
                        end
                        if self.m_SortedTuDiList[i] == data.senderId then
                            if isShiFu then
                                if not isWaiMen then
                                    local indexText = ShiTu_Setting.GetData().MemberSerialNumber[j - 1]
                                    title = SafeStringFormat3(LocalString.GetString("%s徒弟"),indexText)
                                else
                                    title = LocalString.GetString("外门")
                                end 
                            elseif not isShiFu then
                                if not isWaiMen then
                                    local indexText = ShiTu_Setting.GetData().MemberSerialNumber[j - 1]
                                    title = SafeStringFormat3(LocalString.GetString("%s师兄"),indexText)
                                else
                                    title = LocalString.GetString("外门")
                                end
                                local allTudiInfo = LuaShiTuMgr.m_ShifuAllTudiInfo.allTudiInfo
                                if allTudiInfo then
                                    local t = allTudiInfo[data.senderId]
                                    if t then
                                        isWaiMen = t.isWaiMen  
                                    end
                                    local myTudiTime = allTudiInfo[myId] and allTudiInfo[myId].tudiTime or 0
                                    if t.tudiTime > myTudiTime then
                                        title = string.gsub(title, LocalString.GetString("师兄"), LocalString.GetString("师弟"))
                                    elseif myId == data.senderId then
                                        title = LocalString.GetString("我")
                                    end
                                    if t.gender == 1 then
                                        title = string.gsub(title, LocalString.GetString("师兄"), LocalString.GetString("师姐"))
                                        title = string.gsub(title, LocalString.GetString("师弟"), LocalString.GetString("师妹"))
                                    end
                                end    
                            end
                            break
                        end
                    end
                    if info.OwnerId == data.senderId then
                        title = LocalString.GetString("师父")
                    end
                    item.channelLabel.text = title
                end
            end
        end
    end
end

CGroupIMContentView.m_CellForRowAtIndex_CS2LuaHook = function (this, index)
    if index < 0 or index >= this.allData.Count then
        return nil
    end
    local cellIdentifier,template = nil, nil
    local data = this.allData[index]
    if data.usedForDisplayTime then
        cellIdentifier = "TimeSplitCell"
        template = this.timesplitTemplate
    elseif data.channel == EChatPanel.System then
        cellIdentifier = "SystemChannelCell"
        template = this.systemChannelTemplate
    elseif data.senderId == 0 then
        cellIdentifier = "SystemChannelCell"
        template = this.systemChannelTemplate
        data.channel = EChatPanel.System
    elseif data.isOpposite then
        if data.isPic then
            cellIdentifier = "OppositePicTemplate"
            template = this.oppositePicTemplate
        else
            cellIdentifier = "OppositeTemplate"
            template = this.oppositeTemplate
        end
    else
        if data.isPic then
            cellIdentifier = "SelfPicTemplate"
            template = this.selfPicTemplate
        else
            cellIdentifier = "SelfTemplate"
            template = this.selfTemplate
        end
    end
    local cell = this.tableView:DequeueReusableCellWithIdentifier(cellIdentifier)
    if cell == nil then
        cell = this.tableView:AllocNewCellWithIdentifier(template, cellIdentifier)
    end
    local chatListItem = cell:GetComponent(typeof(CChatListBaseItem))
    chatListItem:Init(data)
    LuaGroupIMContentView:ShowShiTuChannelLabel(this,data,chatListItem)
    chatListItem.onReLayoutDelegate = MakeDelegateFromCSFunction(this.OnChatListItemReLayout, MakeGenericClass(Action1, CChatListBaseItem), this)
    return cell
end

CGroupIMContentView.m_OnEnable_CS2LuaHook = function (this)
    EventManager.AddListenerInternal(EnumEventType.RecvGroupIMMsg, MakeDelegateFromCSFunction(this.OnRecvGroupIMMsg, MakeGenericClass(Action1, UInt64), this))
    EventManager.AddListenerInternal(EnumEventType.ClearGroupIMMsgs, MakeDelegateFromCSFunction(this.OnClearGroupIMMsgs, MakeGenericClass(Action1, UInt64), this))
    EventManager.AddListenerInternal(EnumEventType.OnWithdrawPlayerGroupIMMsg, MakeDelegateFromCSFunction(this.OnWithdrawPlayerMsg, MakeGenericClass(Action1, UInt64), this))
    EventManager.AddListenerInternal(EnumEventType.MainPlayerLeaveGroupIM, MakeDelegateFromCSFunction(this.OnMainPlayerLeaveGroupIM, MakeGenericClass(Action1, UInt64), this))
    EventManager.AddListenerInternal(EnumEventType.PlayerLeaveGroupIM, MakeDelegateFromCSFunction(this.OnPlayerLeaveGroupIM, MakeGenericClass(Action2, UInt64, UInt64), this))
    EventManager.AddListenerInternal(EnumEventType.PlayerAddToApproveList, MakeDelegateFromCSFunction(this.OnPlayerAddToApproveList, MakeGenericClass(Action1, UInt64), this))
    EventManager.AddListenerInternal(EnumEventType.PlayerRemoveFromApproveList, MakeDelegateFromCSFunction(this.OnPlayerRemoveFromApproveList, MakeGenericClass(Action1, UInt64), this))
    LuaGroupIMContentView.m_Wnd = this
    g_ScriptEvent:AddListener("OnSendShifuAllTudiInfo", LuaGroupIMContentView, "OnSendShifuAllTudiInfo")
    this.chatInput:RestoreTempInput(CChatHistoryMgr.EnumTempInputType.GroupIM)
end

CGroupIMContentView.m_OnDisable_CS2LuaHook = function (this)
    CFriendView.ChatOppositeId = System.UInt64.MaxValue
    EventManager.RemoveListenerInternal(EnumEventType.RecvGroupIMMsg, MakeDelegateFromCSFunction(this.OnRecvGroupIMMsg, MakeGenericClass(Action1, UInt64), this))
    EventManager.RemoveListenerInternal(EnumEventType.ClearGroupIMMsgs, MakeDelegateFromCSFunction(this.OnClearGroupIMMsgs, MakeGenericClass(Action1, UInt64), this))
    EventManager.RemoveListenerInternal(EnumEventType.OnWithdrawPlayerGroupIMMsg, MakeDelegateFromCSFunction(this.OnWithdrawPlayerMsg, MakeGenericClass(Action1, UInt64), this))
    EventManager.RemoveListenerInternal(EnumEventType.MainPlayerLeaveGroupIM, MakeDelegateFromCSFunction(this.OnMainPlayerLeaveGroupIM, MakeGenericClass(Action1, UInt64), this))
    EventManager.RemoveListenerInternal(EnumEventType.PlayerLeaveGroupIM, MakeDelegateFromCSFunction(this.OnPlayerLeaveGroupIM, MakeGenericClass(Action2, UInt64, UInt64), this))
    EventManager.RemoveListenerInternal(EnumEventType.PlayerAddToApproveList, MakeDelegateFromCSFunction(this.OnPlayerAddToApproveList, MakeGenericClass(Action1, UInt64), this))
    EventManager.RemoveListenerInternal(EnumEventType.PlayerRemoveFromApproveList, MakeDelegateFromCSFunction(this.OnPlayerRemoveFromApproveList, MakeGenericClass(Action1, UInt64), this))
    g_ScriptEvent:RemoveListener("OnSendShifuAllTudiInfo", LuaGroupIMContentView, "OnSendShifuAllTudiInfo")
    LuaGroupIMContentView.m_Wnd = nil
    LuaGroupIMContentView.m_SortedTuDiList = {}
    this.chatInput:SaveTempInput(CChatHistoryMgr.EnumTempInputType.GroupIM)
end

CGroupIMContentView.m_Init_CS2LuaHook = function (this,groupIMId,oppositeId,oppositeName,showHintRoot)
    CGroupIMContentView.mGroupIMID = groupIMId
    LuaGroupIMContentView:InitSortedTuDiList(this)
    this.chatRoot:SetActive(not showHintRoot)
    this.chatHintRoot:SetActive(showHintRoot);
    if showHintRoot then
        return
    end
    local info = CGroupIMMgr.Inst:GetGroupIM(CGroupIMContentView.mGroupIMID)
    if info then
        this.groupNameLabel.text = info.GroupName.StringData
    end
    this:UpdateApplyAlertButtonVisible()
    this.friendChatHeader:SetActive(true)
    this.chatInput.gameObject:SetActive(true)
    this.hintLabel.gameObject:SetActive(false)
    this.tableView:Clear()
    this.tableView.dataSource = this
    this.allData:Clear()
    this.hasNewMsg = false
    this:SetNewMsgButtonVisible(false)
    CRecordingInfoMgr.CloseRecordingWnd(true)
    this.updateUUID = 0
    this:UpdateChatMessages(true)
    this:UpdateInputType()
end