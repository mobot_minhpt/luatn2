local CUITexture = import "L10.UI.CUITexture"
local SdkU3d = import "NtUniSdk.Unity3d.SdkU3d"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local Main = import "L10.Engine.Main"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"

LuaHmtBindWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaHmtBindWnd, "RewardItem", "RewardItem", GameObject)
RegistChildComponent(LuaHmtBindWnd, "HasReward", "HasReward", GameObject)
RegistChildComponent(LuaHmtBindWnd, "IconTexture", "IconTexture", CUITexture)
RegistChildComponent(LuaHmtBindWnd, "BindBtn", "BindBtn", GameObject)
RegistChildComponent(LuaHmtBindWnd, "BindTip", "BindTip", GameObject)
RegistChildComponent(LuaHmtBindWnd, "RebindTip", "RebindTip", GameObject)

--@endregion RegistChildComponent end

function LuaHmtBindWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.BindBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBindBtnClick()
	end)


    --@endregion EventBind end
end

function LuaHmtBindWnd:Init()
    local itemId = GameSetting_Common.GetData().HmtBindRewardItem
	local itemData = Item_Item.GetData(itemId)

	if itemData then
		self.IconTexture:LoadMaterial(itemData.Icon)
		UIEventListener.Get(self.IconTexture.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
			CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType.Default, 0, 0, 0, 0)
		end)
	end

	self.m_HasReward = false
	self.BindBtn.gameObject:SetActive(false)
	self.HasReward.gameObject:SetActive(false)

	if CClientMainPlayer.Inst and 
		CommonDefs.DictContains(CClientMainPlayer.Inst.PlayProp.PersistPlayData, typeof(UInt32), EnumPersistPlayDataKey_lua.eHMTBindReward) then
		-- 已领取奖励
		self:SetState(true, true)
	else
		self:SetState(false, true)
		-- 没有领奖 起一个轮询
		self.Tick = RegisterTick(function()
			if self:SDKIsAvailable() and not self.m_HasReward then
				SdkU3d.HMTQueryAccountVerification()
			end
		end, 1000)
	end
end

function LuaHmtBindWnd:SetState(hasReward, isInit)
    self.m_HasReward = hasReward
	self.BindBtn.gameObject:SetActive(not hasReward)
	self.BindTip.gameObject:SetActive(not hasReward)
	self.RebindTip.gameObject:SetActive(hasReward)
	self.HasReward.gameObject:SetActive(hasReward)

	if hasReward and not isInit then
		UnRegisterTick(self.Tick)
		Gac2Gas.RequestHMTBindReward()
	end
end

function LuaHmtBindWnd:OnEnable()
	-- 监听sdk的绑定状态
    self.Action = DelegateFactory.Action_int_Dictionary_string_object(function(code, data)
		if CommonDefs.DictContains_LuaCall(data, "methodId") and data["methodId"] == "accountVerification" then
			if CommonDefs.DictContains_LuaCall(data, "mobileValidation") then
				if data["mobileValidation"] == "1" or data["mobileValidation"] == 1 then
					self:SetState(true, false)
				else
					self:SetState(false, false)
				end
			end
		end
    end)

	UnRegisterTick(self.Tick)
    EventManager.AddListenerInternal(EnumEventType.UnisdkOnExtendFuncCall, self.Action)
end

function LuaHmtBindWnd:OnDisable()
	UnRegisterTick(self.Tick)
    EventManager.RemoveListenerInternal(EnumEventType.UnisdkOnExtendFuncCall, self.Action)
end


--@region UIEvent

function LuaHmtBindWnd:OnBindBtnClick()
	if self:SDKIsAvailable() then
		SdkU3d.ntOpenManager()
	else
		g_MessageMgr:ShowMessage("NEED_UPDATE_CLIENT")
	end
end

function LuaHmtBindWnd:SDKIsAvailable()
    --合并到主干的时候使用主干svn提交版本，下次hmt开分支留意版本号比该版本大还是小，如果小的话，需要从主干拷贝sdk并修改版本号判断以让hmt分支支持新SDK
	return Main.Inst.EngineVersion > 490156
end

--@endregion UIEvent
