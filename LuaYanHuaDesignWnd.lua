local UILabel = import "UILabel"
local UITexture = import "UITexture"
local CommonDefs = import "L10.Game.CommonDefs"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local GameObject = import "UnityEngine.GameObject"
local NGUITools = import "NGUITools"
local TextureFormat = import "UnityEngine.TextureFormat"
local Texture2D = import "UnityEngine.Texture2D"
local UIGrid = import "UIGrid"
local UISprite = import "UISprite"
local Color = import "UnityEngine.Color"
local QnCheckBox = import "L10.UI.QnCheckBox"
local QnTableView = import "L10.UI.QnTableView"
local CPlayerDataMgr = import "L10.Game.CPlayerDataMgr"
local QnNewSlider = import "L10.UI.QnNewSlider"
local UIPanel = import "UIPanel"
local CInputBoxMgr = import "L10.UI.CInputBoxMgr"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local QnTabButton = import "L10.UI.QnTabButton"
local Object = import "UnityEngine.Object"

CLuaYanHuaDesignWnd = class()

RegistChildComponent(CLuaYanHuaDesignWnd, "m_Canvas","Canvas", UISprite)
RegistChildComponent(CLuaYanHuaDesignWnd, "m_Btn","Btn", GameObject)
RegistChildComponent(CLuaYanHuaDesignWnd, "m_Icon","Icon", UITexture)
RegistChildComponent(CLuaYanHuaDesignWnd, "m_Icon2","Icon2", UITexture)

RegistChildComponent(CLuaYanHuaDesignWnd,"m_PixelGrid","PixelGrid", UIGrid)
RegistChildComponent(CLuaYanHuaDesignWnd,"m_PixelItem","PixelItem", GameObject)
RegistChildComponent(CLuaYanHuaDesignWnd,"m_PixelCanvas","PixelCanvas", GameObject)
RegistChildComponent(CLuaYanHuaDesignWnd,"m_FreeCanvas","FreeCanvas", GameObject)
RegistChildComponent(CLuaYanHuaDesignWnd,"m_ColorGrid","ColorGrid", UIGrid)
RegistChildComponent(CLuaYanHuaDesignWnd,"m_PixelEraserBtn","PixelEraserBtn", GameObject)
RegistChildComponent(CLuaYanHuaDesignWnd,"m_SaveBtn","SaveBtn", GameObject)
RegistChildComponent(CLuaYanHuaDesignWnd,"m_FreeCanvasCheckBox","FreeCanvasCheckBox",QnCheckBox)
RegistChildComponent(CLuaYanHuaDesignWnd,"m_PixelCanvasCheckBox","PixelCanvasCheckBox",QnCheckBox)
RegistChildComponent(CLuaYanHuaDesignWnd,"m_FreeTexture","FreeTexture",UITexture)
RegistChildComponent(CLuaYanHuaDesignWnd,"m_FreeEraserBtn","FreeEraserBtn",GameObject)
RegistChildComponent(CLuaYanHuaDesignWnd,"m_TableView","TableView",QnTableView)
RegistChildComponent(CLuaYanHuaDesignWnd,"m_PenSizeSlider","PenSizeSlider",QnNewSlider)
RegistChildComponent(CLuaYanHuaDesignWnd,"m_PenSizeLabel","PenSizeLabel",UILabel)
RegistChildComponent(CLuaYanHuaDesignWnd,"m_CanvasLabel","CanvasLabel",UILabel)
RegistChildComponent(CLuaYanHuaDesignWnd,"m_DeleteBtn","DeleteBtn",GameObject)
RegistChildComponent(CLuaYanHuaDesignWnd,"m_AddItem","AddItem",GameObject)
RegistChildComponent(CLuaYanHuaDesignWnd,"m_Panel1","ScrollView1",UIPanel)
RegistChildComponent(CLuaYanHuaDesignWnd,"m_Panel2","ScrollView2",UIPanel)
RegistChildComponent(CLuaYanHuaDesignWnd,"m_RenameBtn","RenameBtn",GameObject)
RegistChildComponent(CLuaYanHuaDesignWnd,"m_CanvasNameLabel","YanHuaNameLabel",UILabel)
RegistChildComponent(CLuaYanHuaDesignWnd,"m_ApplyBtn","ApplyBtn",GameObject)
RegistChildComponent(CLuaYanHuaDesignWnd,"m_CloseButton","CloseButton",GameObject)

RegistClassMember(CLuaYanHuaDesignWnd, "m_CurCanvasStatus")

RegistClassMember(CLuaYanHuaDesignWnd, "m_PenColorIndex")--0 没有
RegistClassMember(CLuaYanHuaDesignWnd, "m_PenColor")
--自由画布
RegistClassMember(CLuaYanHuaDesignWnd, "m_PenSize")
RegistClassMember(CLuaYanHuaDesignWnd, "m_CanFreeDraw")
RegistClassMember(CLuaYanHuaDesignWnd, "m_LastPos")
--像素画布
RegistClassMember(CLuaYanHuaDesignWnd, "m_PixelCanvasSize")
RegistClassMember(CLuaYanHuaDesignWnd, "m_PixelItemList")
RegistClassMember(CLuaYanHuaDesignWnd, "m_PixelItemColorList")

RegistClassMember(CLuaYanHuaDesignWnd, "m_CanvasList")
RegistClassMember(CLuaYanHuaDesignWnd, "m_SelectCanvasIndex")
RegistClassMember(CLuaYanHuaDesignWnd, "m_CreatedCanvasCount")
RegistClassMember(CLuaYanHuaDesignWnd, "m_OldTablePos")
RegistClassMember(CLuaYanHuaDesignWnd, "m_PanelSize1")
RegistClassMember(CLuaYanHuaDesignWnd, "m_PanelSize2")

RegistClassMember(CLuaYanHuaDesignWnd, "m_IsModify")
RegistClassMember(CLuaYanHuaDesignWnd, "m_CanModify")
RegistClassMember(CLuaYanHuaDesignWnd, "m_IsNameInputing")

RegistClassMember(CLuaYanHuaDesignWnd, "m_LocalPicList")
RegistClassMember(CLuaYanHuaDesignWnd, "m_StatusCanModify")--检查审核状态
RegistClassMember(CLuaYanHuaDesignWnd, "m_InUploadOrDownLoading")
RegistClassMember(CLuaYanHuaDesignWnd, "m_CanvasHasEditList")

function CLuaYanHuaDesignWnd:Awake()
    self.m_InUploadOrDownLoading = false
    UIEventListener.Get(self.m_Btn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnCupsturePic()
    end)
    UIEventListener.Get(self.m_PixelEraserBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self.m_PenColorIndex = 0
        self.m_PenColor = CLuaYanHuaEditorMgr.ClearColor
    end)
    UIEventListener.Get(self.m_FreeEraserBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self.m_PenColorIndex = 0
        self.m_PenColor = CLuaYanHuaEditorMgr.ClearColor
        self.m_FreeEraserBtn.transform:Find("Selected").gameObject:SetActive(true)
        for j = 0,#CLuaYanHuaEditorMgr.Colors-1,1 do
            local colorTab = self.m_ColorGrid.transform:GetChild(j):GetComponent(typeof(QnTabButton))
            colorTab.transform:Find("Selected").gameObject:SetActive(false)
        end
    end)
    UIEventListener.Get(self.m_SaveBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        if self.m_CreatedCanvasCount <=0 then
            g_MessageMgr:ShowMessage("Please_Create_Canvas_First")
            return
        end
        self:OnSaveBtnClick()
        g_MessageMgr:ShowMessage("Save_YanHua_Succeed")
    end)
    UIEventListener.Get(self.m_DeleteBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        if self.m_CreatedCanvasCount <= 0 or self.m_SelectCanvasIndex > self.m_CreatedCanvasCount then 
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", CLuaYanHuaEditorMgr.DeleteNimTip)
            return 
        end
        self:OnDeleteCanvas()
    end)
    UIEventListener.Get(self.m_AddItem).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnAddCanvas()
    end)
    UIEventListener.Get(self.m_RenameBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self.m_IsNameInputing = true
        self:OnRenameCanvas()
    end)
    UIEventListener.Get(self.m_ApplyBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self.m_IsNameInputing = true
        MessageWndManager.ShowOKCancelMessage(CLuaYanHuaEditorMgr.ApplyCannotEditComfire, 
            DelegateFactory.Action(function ()
                self:OnApplyCanvas()
                self.m_IsNameInputing = false
            end),
            DelegateFactory.Action(function ()
                self.m_IsNameInputing = false
            end),
            LocalString.GetString("确定"), LocalString.GetString("取消"), false)
    end)
    UIEventListener.Get(self.m_CloseButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnCloseBtnClick()
    end)
    self.m_OldTablePos = self.m_TableView.transform.localPosition
    self.m_PanelSize1 = self.m_Panel1.height
    self.m_PanelSize2 = self.m_Panel2.height
    self.m_PanelPos1 = self.m_Panel1.transform.localPosition
    self.m_PanelPos2 = self.m_Panel2.transform.localPosition

    self.m_IsInCanvas = false
    self.m_CanFreeDraw = false

    self.m_PixelCanvasSize = 14
    self.m_PixelItemList = {}
    self.m_PixelItemColorList = {}
    self.m_LocalPicList = {}
    self.m_CanvasHasEditList = {}

    self.m_FreeDrawTexture = nil
    self.m_SelectCanvasIndex = CLuaYanHuaEditorMgr.SelectCustomIndex--1--默认选择第一个画布
    self.m_PenSizeSlider.m_UISlider.numberOfSteps = YanHua_Setting.GetData().MaxPenSize+1
    self.m_PenSizeSlider.OnValueChanged = DelegateFactory.Action_float(function(value)
        local minValue = YanHua_Setting.GetData().MinPenSize/YanHua_Setting.GetData().MaxPenSize
        if value < minValue then
            value = minValue
            self.m_PenSizeSlider.m_Value = value
            return
        end        
        value = math.floor(value*YanHua_Setting.GetData().MaxPenSize+0.5)
        self.m_PenSize = value
        self.m_PenSizeLabel.text = value
    end)

    self.m_PenSizeSlider.m_Value = YanHua_Setting.GetData().DefaultPenSize/YanHua_Setting.GetData().MaxPenSize
    
    self.m_CreatedCanvasCount = 0
    self.m_IsModify = false
    self.m_IsNameInputing = false
end

function CLuaYanHuaDesignWnd:LoadLocalData()
    CLuaYanHuaEditorMgr.OnLoadYanHuaDesignData()

    local count = #CLuaYanHuaEditorMgr.CanvasList
    self.m_CreatedCanvasCount = count
    self:RefreshCanvasLabel()

    for i=1,count,1 do
        self.m_CanvasHasEditList[i] = true
    end
end

function CLuaYanHuaDesignWnd:RefreshCanvasLabel()
    if self.m_CreatedCanvasCount == CLuaYanHuaEditorMgr.MaxCanvasCount then
        self.m_AddItem:SetActive(false)
        self.m_TableView.transform.localPosition = self.m_OldTablePos
        local dis = (self.m_PanelSize1-self.m_PanelSize2)
        self.m_Panel1:SetRect(0,dis,self.m_Panel1.width,self.m_PanelSize1)
    else
        self.m_AddItem:SetActive(true)
        local dis = (self.m_PanelSize1-self.m_PanelSize2)/2
        self.m_TableView.transform.localPosition = Vector3(self.m_OldTablePos.x,self.m_OldTablePos.y-dis,self.m_OldTablePos.z)
        self.m_Panel1:SetRect(0,dis*2,self.m_Panel2.width,self.m_PanelSize2)
    end
    self.m_CanvasLabel.text = SafeStringFormat3(LocalString.GetString("自定义%d/%d"),self.m_CreatedCanvasCount,CLuaYanHuaEditorMgr.MaxCanvasCount)

    if self.m_SelectCanvasIndex <1 or self.m_SelectCanvasIndex > self.m_CreatedCanvasCount then
        if self.m_CurCanvasStatus == CLuaYanHuaEditorMgr.EnumCanvasStatus.ePixel then
            self:InitPixelCanvas(nil)
        else
            self:InitFreeCanvas(nil)
        end
    end
end

function CLuaYanHuaDesignWnd:OnAddCanvas()
    if self.m_InUploadOrDownLoading then return end

    if self.m_CreatedCanvasCount == CLuaYanHuaEditorMgr.MaxCanvasCount then
        return        
    end

    local canvas = {}
    canvas.type = self.m_CurCanvasStatus
    table.insert(CLuaYanHuaEditorMgr.CanvasList,canvas)
    table.insert(CLuaYanHuaEditorMgr.OriYanHuaDesignData,canvas)
    CLuaYanHuaEditorMgr.OnSaveYanHuaDesignData()
    
    self.m_CreatedCanvasCount = self.m_CreatedCanvasCount + 1
    self.m_TableView:ReloadData(true,false)
    self:RefreshCanvasLabel()

    self.m_TableView:SetSelectRow(#CLuaYanHuaEditorMgr.CanvasList-1,true)    
    self.m_CanvasHasEditList[self.m_CreatedCanvasCount + 1] = false
end

function CLuaYanHuaDesignWnd:Delete()
    CLuaYanHuaEditorMgr.CanvasList[self.m_SelectCanvasIndex] = {}   
    CLuaYanHuaEditorMgr.OriYanHuaDesignData[self.m_SelectCanvasIndex] = {}         
    table.remove(CLuaYanHuaEditorMgr.CanvasList,self.m_SelectCanvasIndex)
    table.remove(CLuaYanHuaEditorMgr.OriYanHuaDesignData,self.m_SelectCanvasIndex)
    table.remove(CLuaYanHuaEditorMgr.MyYanHuaPicData,self.m_SelectCanvasIndex)---
    for i = self.m_SelectCanvasIndex ,self.m_CreatedCanvasCount - 1,1 do
        self.m_CanvasHasEditList[i] = self.m_CanvasHasEditList[i+1]
    end
    self.m_CanvasHasEditList[self.m_CreatedCanvasCount] = false
    CLuaYanHuaEditorMgr.OnSaveYanHuaDesignData()
    self.m_CreatedCanvasCount = self.m_CreatedCanvasCount - 1
    self.m_IsModify = false
    self.m_TableView:ReloadData(true,false)
    self:RefreshCanvasLabel()
    self.m_TableView:SetSelectRow(#CLuaYanHuaEditorMgr.CanvasList-1,true)
    self:RefreshPicIndexOnWeb(self.m_SelectCanvasIndex)

    if self.m_CreatedCanvasCount == 0 then
        self.m_CanvasNameLabel.text = LocalString.GetString("烟花名称（限5个字）")
    end
end

function CLuaYanHuaDesignWnd:OnDeleteCanvas()
    if self.m_InUploadOrDownLoading then return end
    --审核中的画布不能删除
    local shengheData = CLuaYanHuaEditorMgr.MyYanHuaPicData[self.m_SelectCanvasIndex]
    local canvas = CLuaYanHuaEditorMgr.CanvasList[self.m_SelectCanvasIndex]
    if shengheData and type(shengheData) == "table" and next(shengheData) and shengheData.id == tonumber(canvas.picId) then
        local auditStatus = shengheData.auditStatus
        if auditStatus == CLuaYanHuaEditorMgr.EnumAuditStatus.eInAudi then
            g_MessageMgr:ShowMessage("Cannot_Delete_Canvas_InAudit")
            return
        end
    end
    if not self.m_CanvasHasEditList[self.m_SelectCanvasIndex] then
        self:Delete()
        return
    end
    self.m_IsNameInputing = true
    MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("Delete_Canvas_Comfirm"), 
    DelegateFactory.Action(function ()
        if not self.m_CanModify then
            local canvas = CLuaYanHuaEditorMgr.CanvasList[self.m_SelectCanvasIndex]
            local storeIndex = canvas.storeIndex
            canvas.type = nil
            canvas.url = nil
            canvas.pixelItemColorList = nil
            canvas.name = nil
            canvas.usedCount = nil
            canvas.picId = nil
            local addCount = 0
            if storeIndex and storeIndex~="" then
                for k,v in pairs(CLuaYanHuaEditorMgr.YanHuaLightListTbl) do
                    if v and next(v) and v.type == "custom" then
                        local canvasIndex = CLuaYanHuaEditorMgr.AddCanvasIndexList[tonumber(v.customIndex)]
                        local temp = CLuaYanHuaEditorMgr.CanvasList[canvasIndex]

                        if temp and temp.storeIndex == storeIndex then
                            local libao = {}
                            libao.type = "empty"
                            CLuaYanHuaEditorMgr.YanHuaLightListTbl[tonumber(k)] = libao
                            addCount = addCount + 1                                      
                        end                                     
                    end
                end
            end
            canvas.storeIndex = nil
            CLuaYanHuaEditorMgr.AddedCanvasCount = CLuaYanHuaEditorMgr.AddedCanvasCount - addCount
            self.m_CanModify = true
            local json = luaJson.table2json(CLuaYanHuaEditorMgr.YanHuaLightListTbl)
            CPlayerDataMgr.Inst:SavePlayerData("yanhualibaoeditor",json)--]]
        end
        self:Delete()
        self.m_IsNameInputing = false
    end),
    DelegateFactory.Action(function ()
        self.m_IsNameInputing = false
    end),
    LocalString.GetString("删除"), LocalString.GetString("取消"), false)
end

function CLuaYanHuaDesignWnd:RefreshPicIndexOnWeb(deleteIndex)
    self.m_DeleteIndex = deleteIndex
    for i= 1 ,#CLuaYanHuaEditorMgr.CanvasList,1 do
        local canvas = CLuaYanHuaEditorMgr.CanvasList[i]
        local picId = canvas.picId
        if picId then
            self.m_InUploadOrDownLoading = true
            LuaPersonalSpaceMgrReal.UpdateFireworkPicWithIndex(canvas.url,i-1,function(data)
                self:DeleteFireworkPicBack(data,i)
            end)
        end
    end
    self.m_TableView:ReloadData(false,false)
end

function CLuaYanHuaDesignWnd:OnRenameCanvas()
    if self.m_CreatedCanvasCount <=0 then
        g_MessageMgr:ShowMessage("Please_Create_Canvas_First")
        return
    end
    CInputBoxMgr.ShowInputBox(LocalString.GetString("请输入烟花的新名称"), DelegateFactory.Action_string(function (val)
        if CWordFilterMgr.Inst:CheckName(val) then
            self.m_CanvasNameLabel.text = val
            local canvas = CLuaYanHuaEditorMgr.CanvasList[self.m_SelectCanvasIndex]
            canvas.name = val
            self.m_IsNameInputing = false
        else
            g_MessageMgr:ShowMessage("Name_Violation")
            self.m_IsNameInputing = false
        end
      end), 10, true, nil, LocalString.GetString("最多10个字符(5个汉字)"))
end

function CLuaYanHuaDesignWnd:OnApplyCanvas()
    if self.m_InUploadOrDownLoading then return end

    if self.m_SelectCanvasIndex <1 or self.m_SelectCanvasIndex > self.m_CreatedCanvasCount then
        return
    end

    local canvas = CLuaYanHuaEditorMgr.CanvasList[self.m_SelectCanvasIndex]
    local addedCount = CLuaYanHuaEditorMgr.AddedCanvasCount

    if addedCount < CLuaYanHuaEditorMgr.MaxAddCustomCount then
        if not self.m_AlreadyAddCount then
            CLuaYanHuaEditorMgr.AddedCanvasCount = CLuaYanHuaEditorMgr.AddedCanvasCount + 1
            self.m_AlreadyAddCount = true
        else
            --替换
            for i,data in ipairs(CLuaYanHuaEditorMgr.CanvasList) do
                local storeIndex = data.storeIndex
                local newStr = ""
                if storeIndex then
                    for ii in string.gmatch(storeIndex,"(%d+),?" ) do
                        if ii ~= tostring(CLuaYanHuaEditorMgr.SelectCustomIndex) then
                            newStr = newStr..ii..","
                        end
                    end
                    data.storeIndex = newStr
                end
            end
        end

        if canvas.storeIndex then
            canvas.storeIndex = canvas.storeIndex..CLuaYanHuaEditorMgr.SelectCustomIndex..","
        else
            canvas.storeIndex = CLuaYanHuaEditorMgr.SelectCustomIndex..","
        end
        local shengheData = CLuaYanHuaEditorMgr.MyYanHuaPicData[self.m_SelectCanvasIndex]
        if shengheData and type(shengheData) == "table" and next(shengheData) and shengheData.id == tonumber(canvas.picId) then
            --self:OnSaveBtnClick(false)
            self:SaveCanvasToLocal()
            CLuaYanHuaEditorMgr.OpenYanHuaLiBaoEditor()
            CUIManager.CloseUI(CLuaUIResources.YanHuaDesignWnd)
        else
            self:OnSaveBtnClick(true)
        end
    end
end

function CLuaYanHuaDesignWnd:AddFireworkPicBack(data,canvasIndex,texture)
    self.m_InUploadOrDownLoading = false
    local code = data.code
    local info = data.data
    local id
    if info then
        id = info.id
    end
    self.m_AddFinish = true
    local canvas = CLuaYanHuaEditorMgr.CanvasList[self.m_SelectCanvasIndex]
    canvas.picId = id
    g_MessageMgr:ShowMessage("Add_Canvas_Succeed")

    local t = {}
    t.id = id
    t.index = self.m_SelectCanvasIndex - 1
    t.url = canvas.url
    t.auditStatus = CLuaYanHuaEditorMgr.EnumAuditStatus.eInAudi
    t.texture = texture
    CLuaYanHuaEditorMgr.MyYanHuaPicData[tonumber(self.m_SelectCanvasIndex)] = t

    --self.m_TableView:ReloadData(false,false)
    self:SaveCanvasToLocal()
    CLuaYanHuaEditorMgr.OpenYanHuaLiBaoEditor()
    CUIManager.CloseUI(CLuaUIResources.YanHuaDesignWnd)
end

function CLuaYanHuaDesignWnd:DeleteFireworkPicBack(data,canvasIndex)
    self.m_InUploadOrDownLoading = false
    local code = data.code
    local info = data.data
    local id
    if info then
        id = info.id
    end

    local savecanvas = CLuaYanHuaEditorMgr.OriYanHuaDesignData[canvasIndex]
    savecanvas.picId = id

    local canvas = CLuaYanHuaEditorMgr.CanvasList[canvasIndex]
    canvas.picId = id
    --要把网站上的最后一张图删掉
    local index = #CLuaYanHuaEditorMgr.CanvasList
    CLuaYanHuaEditorMgr.OnSaveYanHuaDesignData()
    LuaPersonalSpaceMgrReal.RemoveFireworkPicWithIndex(index,function(data)
        CLuaYanHuaEditorMgr.OpenYanHuaLiBaoEditor(true)
    end)
end


function CLuaYanHuaDesignWnd:Init()
    self:LoadLocalData()
    self.m_FreeCanvasCheckBox.Selected = false
    self.m_PixelCanvasCheckBox.Selected = true
    self.m_CurCanvasStatus = CLuaYanHuaEditorMgr.EnumCanvasStatus.ePixel
    self:InitColorGrid()

    self.m_FreeCanvasCheckBox.OnClick = DelegateFactory.Action_QnButton(function(btn)
        local old = self.m_PixelCanvasCheckBox.Selected
        self.m_PixelCanvasCheckBox.Selected = not old
    end)
    self.m_PixelCanvasCheckBox.OnClick = DelegateFactory.Action_QnButton(function(btn)
        local old = self.m_FreeCanvasCheckBox.Selected 
        self.m_FreeCanvasCheckBox.Selected = not old
    end)

    self.m_PixelCanvasCheckBox.OnValueChanged = DelegateFactory.Action_bool(function (value) 
        self:OnPixelCanvasCheckBoxChange(value)
    end)
    ---
    self.m_TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #CLuaYanHuaEditorMgr.CanvasList
        end,
        function(item,row)
            self:InitItem(item,row)
        end)

    self.m_TableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
        if self.m_InUploadOrDownLoading then
            return
        end

        if self.m_IsModify then
            self.m_IsNameInputing = true
            MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("Change_Modify_Canvas_Tip"), 
                DelegateFactory.Action(function ()
                    self.m_IsModify = false
                    local func = function() 
                        self:OnSelectTableAtRow(row)
                    end

                    if self.m_CurCanvasStatus == CLuaYanHuaEditorMgr.EnumCanvasStatus.ePixel then
                        self:OnSavePixelPic(false,func)--false
                    elseif self.m_CurCanvasStatus == CLuaYanHuaEditorMgr.EnumCanvasStatus.eFree then
                        self:OnSaveFreePic(false,func)--false
                    end
                    --self.m_IsModify = false 
                    self.m_IsNameInputing = false
                end),
                DelegateFactory.Action(function ()
                    self.m_IsModify = false
                    
                    local refreshFunc = function ()
                        self:OnSelectTableAtRow(row)
                    end
                    if self.m_CurCanvasStatus == CLuaYanHuaEditorMgr.EnumCanvasStatus.ePixel then
                        self:RefreshPixelCanvasData(refreshFunc)
                    elseif self.m_CurCanvasStatus == CLuaYanHuaEditorMgr.EnumCanvasStatus.eFree then
                        self:RefreshFreeCanvasData(refreshFunc)
                    end
                    
                    self.m_IsNameInputing = false
                end),
                LocalString.GetString("保存"), LocalString.GetString("取消"), false)
        else
            self:OnSelectTableAtRow(row)
        end
    end)
    
    self.m_TableView:ReloadData(false, false)
    self.m_TableView:SetSelectRow(0,true)
    
    if self.m_CreatedCanvasCount == 0 then
        self:OnAddCanvas()
    end

    if self.m_SelectCanvasIndex > self.m_CreatedCanvasCount then
        g_MessageMgr:ShowMessage("Please_Create_Canvas_First")
    end
end

function CLuaYanHuaDesignWnd:InitItem(item,row)  
    local addsprite = item.transform:Find("AddSprite").gameObject
    local icon = item.transform:Find("Icon"):GetComponent(typeof(UITexture))
    local tag = item.transform:Find("Tag").gameObject
    local statusLabel = item.transform:Find("StatusLabel"):GetComponent(typeof(UILabel))
    tag:SetActive(false)

    addsprite:SetActive(false)
    local canvas = CLuaYanHuaEditorMgr.CanvasList[row+1]
    if not canvas then return end 
    tag:SetActive(canvas.storeIndex and canvas.storeIndex ~= "")

    local shengheData = CLuaYanHuaEditorMgr.MyYanHuaPicData[row + 1]
    if canvas.type == CLuaYanHuaEditorMgr.EnumCanvasStatus.ePixel then
        if shengheData and next(shengheData) and shengheData.id == tonumber(canvas.picId) then
            icon.mainTexture = shengheData.texture
            statusLabel.text = CLuaYanHuaEditorMgr.AuditStatusText[shengheData.auditStatus+1]
            statusLabel.gameObject:SetActive(true)
        else
            icon.mainTexture = CLuaYanHuaEditorMgr.ColorList2Pic(canvas.pixelItemColorList)
            statusLabel.gameObject:SetActive(false)
        end
    else
        if shengheData and next(shengheData) and shengheData.id == tonumber(canvas.picId) then
            icon.mainTexture = shengheData.texture
            statusLabel.text = CLuaYanHuaEditorMgr.AuditStatusText[shengheData.auditStatus+1]
            statusLabel.gameObject:SetActive(true)
        else
            CLuaYanHuaEditorMgr.Url2PicIcon(canvas.url,icon)
            statusLabel.gameObject:SetActive(false)
        end
    end
end

function CLuaYanHuaDesignWnd:OnSelectTableAtRow(row)
    row = row + 1
    if self.m_NotReload then
        self.m_NotReload = false
        return
    end

    local canvas = CLuaYanHuaEditorMgr.CanvasList[row]
    local shengheData = CLuaYanHuaEditorMgr.MyYanHuaPicData[row]
    if shengheData and type(shengheData) == "table" and next(shengheData) and shengheData.id == tonumber(canvas.picId) then
        self.m_StatusCanModify = shengheData.auditStatus ~= CLuaYanHuaEditorMgr.EnumAuditStatus.eInAudi
    else
        self.m_StatusCanModify = true
    end

    self.m_SelectCanvasIndex = row
    self.m_FreeCanvasCheckBox.Selected = canvas.type ~= CLuaYanHuaEditorMgr.EnumCanvasStatus.ePixel
    self.m_PixelCanvasCheckBox.Selected = canvas.type == CLuaYanHuaEditorMgr.EnumCanvasStatus.ePixel
    
    self:InitCanvasWithIndex(row)    
end

function CLuaYanHuaDesignWnd:InitCanvasWithIndex(index)
    local canvas = CLuaYanHuaEditorMgr.CanvasList[index]
    if canvas and type(canvas) == "table" and next(canvas) then
        local shengheData = CLuaYanHuaEditorMgr.MyYanHuaPicData[index]
        if shengheData and type(shengheData) == "table" and next(shengheData) and shengheData.id == tonumber(canvas.picId) then
            self.m_StatusCanModify = shengheData.auditStatus ~= CLuaYanHuaEditorMgr.EnumAuditStatus.eInAudi
        else
            self.m_StatusCanModify = true
        end
        local storeIndex = canvas.storeIndex
        --已经添加到库中的不能修改
        if storeIndex~= nil and storeIndex~="" then
            self.m_CanModify = false
        else
            self.m_CanModify = true
        end

        if canvas.type == CLuaYanHuaEditorMgr.EnumCanvasStatus.ePixel then
            self:InitPixelCanvas(canvas.pixelItemColorList)
        elseif canvas.type == CLuaYanHuaEditorMgr.EnumCanvasStatus.eFree then
            self:InitFreeCanvas(canvas.url)
        end
        local name = canvas.name
        self.m_CanvasNameLabel.text = name
    end
end
function CLuaYanHuaDesignWnd:ClearAndRemoveAddedCanvas(targetType)
    self.m_IsNameInputing = true
    MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("Edit_Added_Canvas"), 
                    DelegateFactory.Action(function ()   
                        self.m_CanvasHasEditList[self.m_SelectCanvasIndex] = true         
                        local canvas = CLuaYanHuaEditorMgr.CanvasList[self.m_SelectCanvasIndex]
                        local storeIndex = canvas.storeIndex
                        local addCount = 0

                        for k,v in pairs(CLuaYanHuaEditorMgr.YanHuaLightListTbl) do
                            if v and next(v) and v.type == "custom" then
                                local canvasIndex = CLuaYanHuaEditorMgr.AddCanvasIndexList[tonumber(v.customIndex)]
                                if canvasIndex == self.m_SelectCanvasIndex then
                                    local libao = {}
                                    libao.type = "empty"
                                    CLuaYanHuaEditorMgr.YanHuaLightListTbl[tonumber(k)] = libao
                                    addCount = addCount + 1        
                                end                                   
                            end
                        end
                        canvas.storeIndex = nil
                        CLuaYanHuaEditorMgr.AddedCanvasCount = CLuaYanHuaEditorMgr.AddedCanvasCount - addCount
                        if targetType ~= self.m_LastCanvasType then
                            canvas.type = targetType
                            canvas.url = nil
                            canvas.pixelItemColorList = nil
                            canvas.name = nil
                            canvas.usedCount = nil
                        end
                        canvas.picId = nil
                        if targetType == CLuaYanHuaEditorMgr.EnumCanvasStatus.ePixel then
                            self:InitPixelCanvas(canvas.pixelItemColorList)--nil
                        elseif targetType == CLuaYanHuaEditorMgr.EnumCanvasStatus.eFree then
                            self:InitFreeCanvas(canvas.url)
                        end
                        self.m_CanModify = true
                        self:SaveCanvasToLocal()
                        local json = luaJson.table2json(CLuaYanHuaEditorMgr.YanHuaLightListTbl)
                        CPlayerDataMgr.Inst:SavePlayerData("yanhualibaoeditor",json)
                        self.m_IsNameInputing = false
                    end),
                    DelegateFactory.Action(function ()
                        self.m_FreeCanvasCheckBox.Selected = self.m_CurCanvasStatus == CLuaYanHuaEditorMgr.EnumCanvasStatus.eFree
                        self.m_PixelCanvasCheckBox.Selected = self.m_CurCanvasStatus ~= CLuaYanHuaEditorMgr.EnumCanvasStatus.eFree
                        self.m_CanModify = false
                        self.m_IsNameInputing = false
                    end),
                    LocalString.GetString("继续"), LocalString.GetString("取消"), false)
end

function CLuaYanHuaDesignWnd:OnPixelCanvasCheckBoxChange(value)
    self.m_LastCanvasType = self.m_CurCanvasStatus
    local canvas = CLuaYanHuaEditorMgr.CanvasList[self.m_SelectCanvasIndex]
    if not canvas then
        if value then
            self.m_CurCanvasStatus = CLuaYanHuaEditorMgr.EnumCanvasStatus.ePixel
        else
            self.m_CurCanvasStatus = CLuaYanHuaEditorMgr.EnumCanvasStatus.eFree
        end
        g_MessageMgr:ShowMessage("Please_Create_Canvas_First")
        return
    end
    if not self.m_StatusCanModify then
        g_MessageMgr:ShowMessage("Cannot_Edit_Canvas_InAudit")
        return
    end

    local list = canvas.pixelItemColorList
    local type = canvas.type
    local url = canvas.url
    if value then--pixel
        if type ~= CLuaYanHuaEditorMgr.EnumCanvasStatus.ePixel then
            if not self.m_CanModify then
                self:ClearAndRemoveAddedCanvas(CLuaYanHuaEditorMgr.EnumCanvasStatus.ePixel)
            else
                self.m_IsNameInputing = true
                MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("Change_CanvasStatus_ClearComfirm"), 
                DelegateFactory.Action(function () 
                    self.m_IsModify = false           
                    canvas.type = CLuaYanHuaEditorMgr.EnumCanvasStatus.ePixel
                    canvas.url = nil
                    canvas.pixelItemColorList = nil
                    canvas.name = nil
                    canvas.storeIndex = nil
                    canvas.usedCount = nil
                    canvas.picId = nil
                    self:InitPixelCanvas(nil)
                    self.m_IsNameInputing = false
                    self:OnSavePixelPic(false,nil)
                    
                end),
                DelegateFactory.Action(function ()            
                    self.m_FreeCanvasCheckBox.Selected = true
                    self.m_PixelCanvasCheckBox.Selected = false
                    self.m_IsNameInputing = false
                end),
                LocalString.GetString("切换"), LocalString.GetString("取消"), false)
            end
        end
    else--free
        if type ~= CLuaYanHuaEditorMgr.EnumCanvasStatus.eFree then
            if not self.m_CanModify then
                self:ClearAndRemoveAddedCanvas(CLuaYanHuaEditorMgr.EnumCanvasStatus.eFree)
            else
                self.m_IsNameInputing = true
                MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("Change_CanvasStatus_ClearComfirm"), 
                DelegateFactory.Action(function ()
                    self.m_IsModify = false            
                    canvas.type = CLuaYanHuaEditorMgr.EnumCanvasStatus.eFree
                    canvas.pixelItemColorList = nil
                    canvas.url = nil
                    canvas.name = nil
                    canvas.storeIndex = nil
                    canvas.usedCount = nil
                    canvas.picId = nil
                    self:InitFreeCanvas(nil)
                    self.m_IsNameInputing = false
                    self:OnSaveFreePic(false,nil)
                end), 
                DelegateFactory.Action(function ()            
                    self.m_FreeCanvasCheckBox.Selected = false
                    self.m_PixelCanvasCheckBox.Selected = true
                    self.m_IsNameInputing = false
                end), 
                LocalString.GetString("切换"), LocalString.GetString("取消"), false)
            end
        end
    end
end

function CLuaYanHuaDesignWnd:InitColorGrid()
    local count = self.m_ColorGrid.transform.childCount
    for i =1,count,1 do
        local colorItem = self.m_ColorGrid.transform:GetChild(i-1)
        UIEventListener.Get(colorItem.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            self.m_PenColorIndex = i
            self.m_PenColor = CLuaYanHuaEditorMgr.Colors[i]
            for j = 0,count-1,1 do
                local colorTab = self.m_ColorGrid.transform:GetChild(j):GetComponent(typeof(QnTabButton))
                colorTab.transform:Find("Selected").gameObject:SetActive(false)
            end
            colorItem:Find("Selected").gameObject:SetActive(true)
            self.m_FreeEraserBtn.transform:Find("Selected").gameObject:SetActive(false)
        end)
    end

    local penIndex = YanHua_Setting.GetData().DefaultPenColor
    local colorTab = self.m_ColorGrid.transform:GetChild(penIndex-1):GetComponent(typeof(QnTabButton))
    colorTab.Selected = true
    self.m_PenColorIndex = penIndex
    self.m_PenColor = CLuaYanHuaEditorMgr.Colors[penIndex]
    for i = 0,#CLuaYanHuaEditorMgr.Colors-1,1 do
        local colorTab = self.m_ColorGrid.transform:GetChild(i):GetComponent(typeof(QnTabButton))
        colorTab.Selected = penIndex == i+1
        colorTab.transform:Find("Selected").gameObject:SetActive(penIndex == i+1)
    end
    self.m_FreeEraserBtn.transform:Find("Selected").gameObject:SetActive(false)
end

function CLuaYanHuaDesignWnd:InitPixelCanvas(itemColorList)
    self.m_FreeCanvas:SetActive(false)
    self.m_PixelCanvas:SetActive(true)
    self.m_PixelItemList = {}
    self.m_PixelItemColorList = {}
    if itemColorList then
        for i,v in ipairs(itemColorList) do
            self.m_PixelItemColorList[i] = v
        end
    end
    self.m_CurCanvasStatus = CLuaYanHuaEditorMgr.EnumCanvasStatus.ePixel
    Extensions.RemoveAllChildren(self.m_PixelGrid.transform)
    for i=1,self.m_PixelCanvasSize*self.m_PixelCanvasSize,1 do 
        local pixelItem = NGUITools.AddChild(self.m_PixelGrid.gameObject,self.m_PixelItem)
        pixelItem:SetActive(true)
        local sprite = pixelItem.transform:Find("Sprite"):GetComponent(typeof(UISprite))
        if not self.m_PixelItemColorList[i] then
            table.insert(self.m_PixelItemColorList,0)          
        else
            local colorindex = self.m_PixelItemColorList[i]
            sprite.color = CLuaYanHuaEditorMgr.Colors[colorindex]
        end

        UIEventListener.Get(pixelItem).onClick = DelegateFactory.VoidDelegate(function(go)
            if not self.m_StatusCanModify then
                g_MessageMgr:ShowMessage("Cannot_Edit_Canvas_InAudit")
            elseif not self.m_CanModify then
                self.m_IsNameInputing = true
                MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("Edit_Added_Canvas"), 
                    DelegateFactory.Action(function () 
                        self.m_CanvasHasEditList[self.m_SelectCanvasIndex] = true           
                        local canvas = CLuaYanHuaEditorMgr.CanvasList[self.m_SelectCanvasIndex]
                        local storeIndex = canvas.storeIndex
                        canvas.type = CLuaYanHuaEditorMgr.EnumCanvasStatus.ePixel
                        canvas.url = nil
                        canvas.name = nil
                        canvas.usedCount = nil
                        canvas.picId = nil
                        local addCount = 0

                        for k,v in pairs(CLuaYanHuaEditorMgr.YanHuaLightListTbl) do
                            if v and next(v) and v.type == "custom" then
                                local canvasIndex = CLuaYanHuaEditorMgr.AddCanvasIndexList[tonumber(v.customIndex)]
                                if canvasIndex == self.m_SelectCanvasIndex then
                                    local libao = {}
                                    libao.type = "empty"
                                    CLuaYanHuaEditorMgr.YanHuaLightListTbl[tonumber(k)] = libao
                                    addCount = addCount + 1                                      
                                end                                     
                            end
                        end
                        canvas.storeIndex = nil
                        CLuaYanHuaEditorMgr.AddedCanvasCount = CLuaYanHuaEditorMgr.AddedCanvasCount - addCount
                        self:InitPixelCanvas(canvas.pixelItemColorList)
                        self.m_CanModify = true

                        self:SaveCanvasToLocal()
                        local json = luaJson.table2json(CLuaYanHuaEditorMgr.YanHuaLightListTbl)
                        CPlayerDataMgr.Inst:SavePlayerData("yanhualibaoeditor",json)
                        self.m_IsNameInputing = false
                    end),
                    DelegateFactory.Action(function ()
                        self.m_IsNameInputing = false
                    end),
                    LocalString.GetString("继续"), LocalString.GetString("取消"), false)
            else
                if self.m_PenColor and self.m_SelectCanvasIndex <= self.m_CreatedCanvasCount then
                    local sprite = pixelItem.transform:Find("Sprite"):GetComponent(typeof(UISprite))
                    sprite.color = self.m_PenColor
                    self.m_PixelItemColorList[i] = self.m_PenColorIndex
                    self.m_IsModify = true
                    self.m_CanvasHasEditList[self.m_SelectCanvasIndex] = true
                    local canvas = CLuaYanHuaEditorMgr.CanvasList[self.m_SelectCanvasIndex]
                    canvas.picId = nil
                end
            end
            
        end)
        table.insert(self.m_PixelItemList,pixelItem)
    end
    self.m_PixelGrid:Reposition()
end

function CLuaYanHuaDesignWnd:InitFreeCanvas(url)
    self.m_FreeCanvas:SetActive(true)
    self.m_PixelCanvas:SetActive(false)
    self.m_CurCanvasStatus = CLuaYanHuaEditorMgr.EnumCanvasStatus.eFree
    self.m_CanFreeDraw = true
    self.m_FreeDrawTexture = nil

    if self.m_FreeTexture.mainTexture ~= nil then
            Object.Destroy(self.m_FreeTexture.mainTexture)
            self.m_FreeTexture.mainTexture = nil
    end

    local onFinished = DelegateFactory.Action(function ()
        self.m_FreeDrawTexture = self.m_FreeTexture.mainTexture

        if not self.m_FreeDrawTexture then
            self.m_FreeDrawTexture = CreateFromClass(Texture2D, 256, 256, TextureFormat.ARGB32, false)
            for x=0,255,1 do
                for y=0,255,1 do
                    self.m_FreeDrawTexture:SetPixel(x,y,Color.clear)
                end
            end
            self.m_FreeDrawTexture:Apply()
            self.m_FreeTexture.mainTexture = self.m_FreeDrawTexture
        end
    end)

    if url and url ~= "" then
        CPersonalSpaceMgr.DownLoadPic(url, self.m_FreeTexture, self.m_FreeTexture.width, self.m_FreeTexture.height, onFinished, false, false)
    else
        self.m_FreeDrawTexture = CreateFromClass(Texture2D, 256, 256, TextureFormat.ARGB32, false)
        for x=0,255,1 do
            for y=0,255,1 do
                self.m_FreeDrawTexture:SetPixel(x,y,Color.clear)
            end
        end
        self.m_FreeDrawTexture:Apply()
        self.m_FreeTexture.mainTexture = self.m_FreeDrawTexture
    end
end
---save
function CLuaYanHuaDesignWnd:OnSaveBtnClick(isShengHe)
    if self.m_InUploadOrDownLoading then return end

    if self.m_SelectCanvasIndex > #CLuaYanHuaEditorMgr.CanvasList then
        return
    end

    if self.m_CurCanvasStatus == nil then return end
    if self.m_CurCanvasStatus == CLuaYanHuaEditorMgr.EnumCanvasStatus.ePixel then
        self:OnSavePixelPic(isShengHe)
    else
        self:OnSaveFreePic(isShengHe)
    end
    self.m_IsModify = false
end

function CLuaYanHuaDesignWnd:RefreshPixelCanvasData(refreshFunc)
    local canvas = {}
    canvas.type = CLuaYanHuaEditorMgr.EnumCanvasStatus.ePixel
    canvas.pixelItemColorList = {}
    for k,v in ipairs(self.m_PixelItemColorList) do
        canvas.pixelItemColorList[k] = v
    end
    canvas.name = self.m_CanvasNameLabel.text
	local old = CLuaYanHuaEditorMgr.CanvasList[self.m_SelectCanvasIndex]
    if old then
        canvas.storeIndex = old.storeIndex
        canvas.usedCount = old.usedCount
        canvas.picId = old.picId
    end
    if CLuaYanHuaEditorMgr.CanvasList[self.m_SelectCanvasIndex] then
        CLuaYanHuaEditorMgr.CanvasList[self.m_SelectCanvasIndex] = canvas
    end
    self.m_InUploadOrDownLoading = true
    self:UploadPic(nil,refreshFunc,true)
end

function CLuaYanHuaDesignWnd:OnSavePixelPic(isShengHe,func)
    self:RefreshPixelCanvasData()
    self.m_InUploadOrDownLoading = true
    self:UploadPic(isShengHe,func)
end

function CLuaYanHuaDesignWnd:RefreshFreeCanvasData(refreshFunc)
    local canvas = {}
    canvas.type = CLuaYanHuaEditorMgr.EnumCanvasStatus.eFree
    canvas.url = nil
    canvas.name = self.m_CanvasNameLabel.text
    local old = CLuaYanHuaEditorMgr.CanvasList[self.m_SelectCanvasIndex]
    if old then
        canvas.storeIndex = old.storeIndex
        canvas.usedCount = old.usedCount
        canvas.picId = old.picId
    end
    if CLuaYanHuaEditorMgr.CanvasList[self.m_SelectCanvasIndex] then
        CLuaYanHuaEditorMgr.CanvasList[self.m_SelectCanvasIndex] = canvas
    end

    self.m_InUploadOrDownLoading = true
    self:UploadPic(nil,refreshFunc,true)
end

function CLuaYanHuaDesignWnd:OnSaveFreePic(isShengHe,func)
    --self:RefreshFreeCanvasData()
    self.m_InUploadOrDownLoading = true
    self:UploadPic(isShengHe,func)
end

function CLuaYanHuaDesignWnd:SaveCanvasToLocal()
    if self.m_SelectCanvasIndex <1 or self.m_SelectCanvasIndex > self.m_CreatedCanvasCount then
        return
    end
    local index = self.m_SelectCanvasIndex
    local canvas = CLuaYanHuaEditorMgr.CanvasList[index]
    local data = {}
    for k,v in pairs(canvas) do
        data[k] = v
    end
    CLuaYanHuaEditorMgr.OriYanHuaDesignData[index] = data--canvas
    CLuaYanHuaEditorMgr.OnSaveYanHuaDesignData()
end


function CLuaYanHuaDesignWnd:Update()
    if self.m_InUploadOrDownLoading then return end
    
    if not self.m_PenColor then return end
    --像素模式
    if Input.GetMouseButtonDown(0) and self.m_CurCanvasStatus == CLuaYanHuaEditorMgr.EnumCanvasStatus.ePixel and not self.m_IsStartPixeling then
        local hoveredObject = CUICommonDef.SelectedUIWithRacast
        if hoveredObject and string.find(hoveredObject.name,"PixelItem") then
            if not self.m_StatusCanModify then
                g_MessageMgr:ShowMessage("Cannot_Edit_Canvas_InAudit")
                return
            end
            if self.m_CreatedCanvasCount <= 0 then
                g_MessageMgr:ShowMessage("Please_Create_Canvas_First")
                return
            end
        end
        self.m_IsStartPixeling = true
    end
    if Input.GetMouseButton(0) and self.m_CurCanvasStatus == CLuaYanHuaEditorMgr.EnumCanvasStatus.ePixel and self.m_IsStartPixeling then
        local hoveredObject = CUICommonDef.SelectedUIWithRacast
        if hoveredObject and string.find(hoveredObject.name,"PixelItem") then
            if not self.m_StatusCanModify then
                g_MessageMgr:ShowMessage("Cannot_Edit_Canvas_InAudit")
                return
            end
            if self.m_CreatedCanvasCount <= 0 then
                g_MessageMgr:ShowMessage("Please_Create_Canvas_First")
                return
            end
            if not self.m_CanModify then
                self.m_CanModify = true
                self.m_LastCanvasType = self.m_CurCanvasStatus
                self:ClearAndRemoveAddedCanvas(self.m_CurCanvasStatus)
            else
                local itemIndex = hoveredObject.transform:GetSiblingIndex()+1
                self.m_PixelItemColorList[itemIndex] = self.m_PenColorIndex
                hoveredObject.transform:Find("Sprite"):GetComponent(typeof(UISprite)).color = self.m_PenColor  
                self.m_CanvasHasEditList[self.m_SelectCanvasIndex] = true
                self.m_IsModify = true
                local canvas = CLuaYanHuaEditorMgr.CanvasList[self.m_SelectCanvasIndex]
                if canvas then canvas.picId = nil end
            end
        end
    end
    --自由模式
    self.m_IsNameInputing = UICamera.lastHit.collider and not CUICommonDef.IsChild(self.m_FreeCanvas.transform, UICamera.lastHit.collider.transform) and CUICommonDef.GetNearestPanelDepth(self.m_FreeCanvas.transform) < CUICommonDef.GetNearestPanelDepth(UICamera.lastHit.collider.transform)
    if Input.GetMouseButtonDown(0) and self.m_CanFreeDraw and not self.m_IsStartDraw and not self.m_IsNameInputing then
        local mainCamera = CUIManager.UIMainCamera
        if mainCamera then
            local pos = mainCamera:ScreenToWorldPoint(Vector3(Input.mousePosition.x, Input.mousePosition.y, 1))
            local relLocalPos = self.m_FreeCanvas.transform.parent:InverseTransformPoint(pos)
            self.m_IsInCanvas = self:IsPointInFreeCanvas(relLocalPos)
            if not self.m_IsInCanvas then return end
            if self.m_CreatedCanvasCount <= 0 then
                g_MessageMgr:ShowMessage("Please_Create_Canvas_First")
                return
            end
            if not self.m_StatusCanModify then
                g_MessageMgr:ShowMessage("Cannot_Edit_Canvas_InAudit")
                return
            end

            if not self.m_CanModify then
                self.m_CanModify = true
                self.m_LastCanvasType = self.m_CurCanvasStatus
                self:ClearAndRemoveAddedCanvas(self.m_CurCanvasStatus)
            else
                self.m_CanvasHasEditList[self.m_SelectCanvasIndex] = true
                self.m_IsStartDraw = true
                self.m_IsModify = true
                self:DrawTextureLine(nil,relLocalPos)
            end
            local canvas = CLuaYanHuaEditorMgr.CanvasList[self.m_SelectCanvasIndex]
            if canvas then canvas.picId = nil end
        end
    end
    if Input.GetMouseButton(0) and self.m_IsStartDraw and self.m_CanFreeDraw and not self.m_IsNameInputing then
        if not self.m_StatusCanModify then
            g_MessageMgr:ShowMessage("Cannot_Edit_Canvas_InAudit")
            return
        end
        local mainCamera = CUIManager.UIMainCamera
        if mainCamera then
            local pos = mainCamera:ScreenToWorldPoint(Vector3(Input.mousePosition.x, Input.mousePosition.y, 1))
            local relLocalPos = self.m_FreeCanvas.transform.parent:InverseTransformPoint(pos)
            self.m_IsInCanvas = self:IsPointInFreeCanvas(relLocalPos)
            if self.m_IsInCanvas then
                self:DrawTextureLine(self.m_LastPos,relLocalPos)
                self.m_LastPos = relLocalPos
            else
                self.m_IsStartDraw = false
                self.m_LastPos = nil
            end
        end
    end
    if Input.GetMouseButtonUp(0) then
        self.m_IsStartDraw = false
        self.m_LastPos = nil
        self.m_IsStartPixeling = false
    end
end

function CLuaYanHuaDesignWnd:IsPointInFreeCanvas(point)
    local width = self.m_FreeTexture.width
    local height = self.m_FreeTexture.height
    local x = point.x
    local y = point.y
    if x >=0 and x<= width and y >=0 and y <= height then
        return true
    else
        return false
    end
end

function CLuaYanHuaDesignWnd:DrawTextureLine(pos1,pos2)
    local width = self.m_PenSize
    local w1 = math.floor(width/2)
    local w2 = width - w1

    if not pos1 and not pos2 then return end
    if not pos1 and pos2 then
        for wx = 0,width-1,1 do
            for wy = 0,width-1,1 do
                local x =math.floor(pos2.x/820 * 256)+wx
                local y = math.floor(pos2.y/820 * 256)+wy
                x = math.max(math.min(255,x),0)
                y = math.max(math.min(255,y),0)
                self.m_FreeDrawTexture:SetPixel(x,y,self.m_PenColor)
            end
        end

        self.m_FreeDrawTexture:Apply()
        self.m_FreeTexture.mainTexture = self.m_FreeDrawTexture
    elseif pos1 and pos2 then
        local x1 =math.floor(pos1.x/820 * 256)
        local y1 = math.floor(pos1.y/820 * 256)
        local x2 =math.floor(pos2.x/820 * 256)
        local y2 = math.floor(pos2.y/820 * 256)
        
        local gapx = x2-x1>0 and 1 or -1 
        local gapy = y2-y1>0 and 1 or -1

        for wx = 0,width-1,1 do
            for wy = 0,width-1,1 do
                if x2-x1 == 0 then
                    for y = y1,y2,gapy do
                        local x = x1+wx
                        local yy = y+wy
                        x = math.max(math.min(255,x),0)
                        yy = math.max(math.min(255,yy),0)
                        self.m_FreeDrawTexture:SetPixel(x,yy,self.m_PenColor)
                    end
                else
                    local k = (y2-y1)/(x2-x1)
                    local b = y1 - (y2-y1)/(x2-x1)*x1
                    for x = x1,x2,gapx do
                        local y = math.floor(k*x + b)+wy
                        local xx = x+wx
                        xx = math.max(math.min(255,xx),0)
                        y = math.max(math.min(255,y),0)
                        self.m_FreeDrawTexture:SetPixel(xx,y,self.m_PenColor)
                    end
                end               
            end
        end
        
        self.m_FreeDrawTexture:Apply()
        self.m_FreeTexture.mainTexture = self.m_FreeDrawTexture
    end
    
end

function CLuaYanHuaDesignWnd:UploadPic(isShengHe,func,notSave)
    if self.m_CurCanvasStatus == CLuaYanHuaEditorMgr.EnumCanvasStatus.eFree then
        if self.m_FreeDrawTexture ~= nil then
            local data = CommonDefs.EncodeToPNG(self.m_FreeDrawTexture)
            local onFinished = DelegateFactory.Action_string(function (url)
                self.m_InUploadOrDownLoading = false
                if url ==nil then
                    url = ""
                end

                local canvas = CLuaYanHuaEditorMgr.CanvasList[self.m_SelectCanvasIndex]
                canvas.url = url 
                if isShengHe then
                    -----审核
                    local picIndex = self.m_SelectCanvasIndex-1
                    self.m_InUploadOrDownLoading = true
                    LuaPersonalSpaceMgrReal.UpdateFireworkPicWithIndex(url,picIndex,function(data)
                        self:AddFireworkPicBack(data,self.m_SelectCanvasIndex,self.m_FreeDrawTexture)
                    end)
                end
                if not notSave then
                    self:SaveCanvasToLocal()
                end
                self.m_TableView:ReloadData(false,false)
                if func then      
                    func()  
                end
            end)
            CPersonalSpaceMgr.Inst:UploadPic("fireworks", data, onFinished)
        end

    elseif self.m_CurCanvasStatus == CLuaYanHuaEditorMgr.EnumCanvasStatus.ePixel then
        local pixelItemColorList = self.m_PixelItemColorList
        local texture = CLuaYanHuaEditorMgr.ColorList2Pic(pixelItemColorList)
        if texture then
            local data = CommonDefs.EncodeToPNG(texture)
            local onFinished = DelegateFactory.Action_string(function (url)
                if url ==nil then
                    url = ""
                end

                local canvas = CLuaYanHuaEditorMgr.CanvasList[self.m_SelectCanvasIndex]
                canvas.url = url           
                if isShengHe then
                    -----审核
                    local picIndex = self.m_SelectCanvasIndex-1
                    LuaPersonalSpaceMgrReal.UpdateFireworkPicWithIndex(url,picIndex,function(data)
                        self:AddFireworkPicBack(data,self.m_SelectCanvasIndex,texture)
                    end)
                end

                if not notSave then
                    self:SaveCanvasToLocal()
                end
                self.m_TableView:ReloadData(false,false)--]]
                if func then      
                    func()  
                end
                self.m_InUploadOrDownLoading = false
            end)
            CPersonalSpaceMgr.Inst:UploadPic("fireworks", data, onFinished)
        end
    end
end

function CLuaYanHuaDesignWnd:OnCloseBtnClick()
    local function func()
        CLuaYanHuaEditorMgr.OpenYanHuaLiBaoEditor()
        CUIManager.CloseUI(CLuaUIResources.YanHuaDesignWnd)
    end
    if self.m_IsModify then
        self.m_IsNameInputing = true
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("Change_Modify_Canvas_Tip"), 
                DelegateFactory.Action(function ()
                    self.m_IsModify = false
                    if self.m_CurCanvasStatus == CLuaYanHuaEditorMgr.EnumCanvasStatus.ePixel then
                        local canvas = CLuaYanHuaEditorMgr.CanvasList[self.m_SelectCanvasIndex]
                        canvas.pixelItemColorList = self.m_PixelItemColorList
                        canvas.name = self.m_CanvasNameLabel.text
                        self:SaveCanvasToLocal()
                        g_MessageMgr:ShowMessage("Save_YanHua_Succeed")
                        func()
                    elseif self.m_CurCanvasStatus == CLuaYanHuaEditorMgr.EnumCanvasStatus.eFree then
                        self:UploadPic(false,func)
                    end
                end),
                DelegateFactory.Action(function ()
                    func()
                end),
                LocalString.GetString("保存"), LocalString.GetString("取消"), false)
    else
        func()
    end
end
