local UIWidget = import "UIWidget"
local UILabel = import "UILabel"
local CUIFx = import "L10.UI.CUIFx"
local CUITexture = import "L10.UI.CUITexture"
local QnButton = import "L10.UI.QnButton"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CItemMgr = import "L10.Game.CItemMgr"
local Object = import "System.Object"
local MsgPackImpl = import "MsgPackImpl"
local LuaUtils = import "LuaUtils"
local LuaTweenUtils = import "LuaTweenUtils"
local Ease = import "DG.Tweening.Ease"
local CUICommonDef = import "L10.UI.CUICommonDef"

local numString = {LocalString.GetString("一"),LocalString.GetString("二"),LocalString.GetString("三"),LocalString.GetString("四"),LocalString.GetString("五"),
LocalString.GetString("六"),LocalString.GetString("七"),LocalString.GetString("八"),LocalString.GetString("九"),LocalString.GetString("十"),}
local GetNumString = function(num)
	num = tonumber(num)
	if num > 0 and num <= 10 then
		return numString[num]
	elseif num > 10 and num <= 19 then
		return numString[10] .. numString[num%10]
	elseif num >= 20 then
		if num%10 == 0  then
			return numString[math.floor(num/10)] .. numString[10]
		else
			return numString[math.floor(num/10)] .. numString[10] .. numString[num%10]
		end
	end
	return LocalString.GetString("零")
end
local GetYearString = function(year)
	local first = math.floor(year / 1000)
	local second = math.floor((year - first* 1000) / 100)
	local third = math.floor((year - first* 1000 - second * 100) / 10)
	local forth = year%10
	return GetNumString(first) .. GetNumString(second) .. GetNumString(third) .. GetNumString(forth)
end

CLuaSpokesmanPawnCertificateWnd = class()

RegistClassMember(CLuaSpokesmanPawnCertificateWnd, "m_Bg")
RegistClassMember(CLuaSpokesmanPawnCertificateWnd, "m_BgLBorder")
RegistClassMember(CLuaSpokesmanPawnCertificateWnd, "m_BgRBorder")
RegistClassMember(CLuaSpokesmanPawnCertificateWnd, "m_Content")
RegistClassMember(CLuaSpokesmanPawnCertificateWnd, "m_Fx")
RegistClassMember(CLuaSpokesmanPawnCertificateWnd, "m_PlayerIcon")
RegistClassMember(CLuaSpokesmanPawnCertificateWnd, "m_PlayerName")
RegistClassMember(CLuaSpokesmanPawnCertificateWnd, "m_SpokesmanIcon")
RegistClassMember(CLuaSpokesmanPawnCertificateWnd, "m_SpokesmanName")

RegistClassMember(CLuaSpokesmanPawnCertificateWnd, "m_InsPlayerName")
RegistClassMember(CLuaSpokesmanPawnCertificateWnd, "m_MainLab")
RegistClassMember(CLuaSpokesmanPawnCertificateWnd, "m_DateLab")
RegistClassMember(CLuaSpokesmanPawnCertificateWnd, "m_ShareBtn")

RegistClassMember(CLuaSpokesmanPawnCertificateWnd, "m_Tweener1")
RegistClassMember(CLuaSpokesmanPawnCertificateWnd, "m_Tweener2")
RegistClassMember(CLuaSpokesmanPawnCertificateWnd, "m_Tweener3")
RegistClassMember(CLuaSpokesmanPawnCertificateWnd, "m_Tweener4")
RegistClassMember(CLuaSpokesmanPawnCertificateWnd, "m_Tick")

function CLuaSpokesmanPawnCertificateWnd:Awake()
    self:InitComponents()
end

function CLuaSpokesmanPawnCertificateWnd:InitComponents()
    self.m_Bg           = self.transform:Find("Anchor/Bg"):GetComponent(typeof(UIWidget))
    self.m_BgLBorder    = self.transform:Find("Anchor/Bg/LeftBorder").gameObject
    self.m_BgRBorder    = self.transform:Find("Anchor/Bg/RightBorder").gameObject
    self.m_Content      = self.transform:Find("Anchor/Content"):GetComponent(typeof(UIWidget))
    self.m_Fx           = self.transform:Find("Anchor/Fx"):GetComponent(typeof(CUIFx))
    self.m_PlayerIcon   = self.transform:Find("Anchor/Content/Middle/Player/Icon"):GetComponent(typeof(CUITexture))
    self.m_PlayerName   = self.transform:Find("Anchor/Content/Middle/Player/Name"):GetComponent(typeof(UILabel))
    self.m_SpokesmanIcon= self.transform:Find("Anchor/Content/Middle/Spokesman/Icon"):GetComponent(typeof(CUITexture))
    self.m_SpokesmanName= self.transform:Find("Anchor/Content/Middle/Spokesman/Name"):GetComponent(typeof(UILabel))

    self.m_InsPlayerName= self.transform:Find("Anchor/Content/Bottom/Label"):GetComponent(typeof(UILabel))
    self.m_MainLab      = self.transform:Find("Anchor/Content/Bottom/MainLab"):GetComponent(typeof(UILabel))
    self.m_DateLab      = self.transform:Find("Anchor/Content/Bottom/DateLab"):GetComponent(typeof(UILabel))
    self.m_ShareBtn     = self.transform:Find("Anchor/Content/Bottom/ShareBtn"):GetComponent(typeof(QnButton))
end

function CLuaSpokesmanPawnCertificateWnd:Init()
    local playerName = CClientMainPlayer.Inst.Name
    local spokesmanName = Spokesman_Setting.GetData().SpokesmanName
    
    self.m_PlayerName.text = playerName
    self.m_PlayerIcon:LoadNPCPortrait(CClientMainPlayer.Inst.PortraitName, false)
    self.m_SpokesmanIcon:LoadMaterial(Spokesman_Setting.GetData().SpokesmanIcon)
    self.m_SpokesmanName.text = spokesmanName

    local PawnCertificateId = Spokesman_Setting.GetData().PawnCertificateId
    local msg = ""
    if PawnCertificateId[0] == CLuaSpokesmanMgr.currentItemId then
        msg = g_MessageMgr:FormatMessage("PawnPaper_Msg_01", spokesmanName)
    elseif PawnCertificateId[1] == CLuaSpokesmanMgr.currentItemId then
        msg = g_MessageMgr:FormatMessage("PawnPaper_Msg_02", spokesmanName)
    elseif PawnCertificateId[2] == CLuaSpokesmanMgr.currentItemId then
        msg = g_MessageMgr:FormatMessage("PawnPaper_Msg_03", spokesmanName)
        self.m_Fx:LoadFx("Fx/UI/Prefab/UI_ddpzpianghua.prefab")
    end
    self.m_InsPlayerName.text = playerName
    self.m_MainLab.text = msg

    local year, month, day
    local bagPos = 0
    local itemId = nil
    local default
    default, bagPos, itemId = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, CLuaSpokesmanMgr.currentItemId)
    if default == true then
        local item = CItemMgr.Inst:GetById(itemId)
        if item.Item.ExtraVarData ~= nil and item.Item.ExtraVarData.Data ~= nil then
            local raw = item.Item.ExtraVarData.Data
            local list = TypeAs(MsgPackImpl.unpack(raw), typeof(MakeGenericClass(List, Object)))
            if list.Count == 3 then
                year = GetYearString(list[0])
                month = GetNumString(list[1])
                day = GetNumString(list[2])
            end
        end
    end

    self.m_DateLab.text = SafeStringFormat3(LocalString.GetString("%s年%s月%s日"), year, month, day)
    self.m_ShareBtn.OnClick = DelegateFactory.Action_QnButton(function ()
        CUICommonDef.CaptureScreenAndShare()
    end)

    self:anim()
end

function CLuaSpokesmanPawnCertificateWnd:anim()
    local width = self.m_Bg.width
    self.m_Tweener1 = LuaTweenUtils.TweenFloat(0.3, 1, 0.5, function ( val )
        self.m_Bg.width = width * val
        
	end)
    LuaTweenUtils.SetEase(self.m_Tweener1, Ease.OutBack)
    
    self.m_Tweener2 = LuaTweenUtils.TweenFloat(0, 1, 0.5, function ( val )
		self.m_Bg.alpha = val
	end)
    LuaTweenUtils.SetEase(self.m_Tweener2, Ease.Linear)
    
    self.m_Tick = RegisterTickOnce(function ()
        self.m_Tweener3 = LuaTweenUtils.TweenFloat(0, 1, 0.3, function ( val )
            self.m_Content.alpha = val
        end)
        LuaTweenUtils.SetEase(self.m_Tweener3, Ease.Linear)
    end, 200)

    self.m_Tweener4 = LuaTweenUtils.TweenFloat(0, 1, 0.5, function ( val )
        --leftBorder从-280 到 -769
        --rightBorder从280 到 762
        LuaUtils.SetLocalPositionX(self.m_BgLBorder.transform, -280 - 489 * val)
        LuaUtils.SetLocalPositionX(self.m_BgRBorder.transform, 280 + 482 * val)
    end)
    LuaTweenUtils.SetEase(self.m_Tweener4, Ease.OutBack)
end

function CLuaSpokesmanPawnCertificateWnd:OnDestroy()
    if self.m_Tweener1 then
        LuaTweenUtils.Kill(self.m_Tweener1,false)
    end
    if self.m_Tweener2 then
        LuaTweenUtils.Kill(self.m_Tweener2,false)
    end
    if self.m_Tweener3 then
        LuaTweenUtils.Kill(self.m_Tweener3,false)
    end
    if self.m_Tweener4 then
        LuaTweenUtils.Kill(self.m_Tweener4,false)
    end
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
    end
end
