local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local AlignType = import "L10.UI.CTooltip.AlignType"
local WndType = import "L10.UI.WndType"
local StringBuilder = import "System.Text.StringBuilder"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
LuaDaFuWongLandTip = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaDaFuWongLandTip, "LandTexture", "LandTexture", GameObject)
RegistChildComponent(LuaDaFuWongLandTip, "LandName", "LandName", UILabel)
RegistChildComponent(LuaDaFuWongLandTip, "Name", "Name", UILabel)
RegistChildComponent(LuaDaFuWongLandTip, "Cost", "Cost", UILabel)
RegistChildComponent(LuaDaFuWongLandTip, "ExtraCost", "ExtraCost", GameObject)
RegistChildComponent(LuaDaFuWongLandTip, "UpOrDown", "UpOrDown", GameObject)
RegistChildComponent(LuaDaFuWongLandTip, "OriCost", "OriCost", UILabel)
RegistChildComponent(LuaDaFuWongLandTip, "Type", "Type", UILabel)
RegistChildComponent(LuaDaFuWongLandTip, "Des", "Des", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaDaFuWongLandTip, "m_LandID")
function LuaDaFuWongLandTip:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaDaFuWongLandTip:Init()
    if LuaDaFuWongMgr.CurLandId <= 0 then return end
    local landInfo = LuaDaFuWongMgr.LandInfo and LuaDaFuWongMgr.LandInfo[LuaDaFuWongMgr.CurLandId]
    if not landInfo then return end
    self.m_LandID = LuaDaFuWongMgr.CurLandId
    local data = DaFuWeng_Land.GetData(self.m_LandID)
    if not data then return end
    self:InitIcon(data)

    if landInfo.Owner ~= 0 then
        local player = LuaDaFuWongMgr.PlayerInfo and LuaDaFuWongMgr.PlayerInfo[landInfo.Owner]
        if player then
            self.Name.text = player.name
            self.Name.color = NGUIText.ParseColor32("FFFFFFFF", 0)
            local base = landInfo.cost and landInfo.cost[landInfo.lv - 1] or 0
            self:ShowCost(base,landInfo.curCost)
        end
    else
        self.Name.text = LocalString.GetString("在售")
        self.Name.color = NGUIText.ParseColor32("0EC52CFF", 0)
        local base = 0
        for i = 0,landInfo.lv - 1 do
            local lvPrice = landInfo.price and landInfo.price[i] or 0
            base = base + lvPrice
        end
        self:ShowPrice(base,landInfo.curPrice)
    end
    self:InitSeries(landInfo)
    self.Des.text = g_MessageMgr:Format(data.Describe)
    local view = self.transform:Find("Background/Left/lv").gameObject
    self:InitLv(view,landInfo.lv)
end

function LuaDaFuWongLandTip:InitIcon(data)
    self.LandName.text = data.Name
    self.LandTexture:GetComponent(typeof(CUITexture)):LoadMaterial(data.Icon)
end
function LuaDaFuWongLandTip:InitSeries(data)
    self.Type.text = nil
    local seriesBtn = self.transform:Find("Background/Right/TipButton").gameObject
    if data.series then
        local series = DaFuWeng_Series.GetData(data.series)
        if series then 
            self.Type.text = series.Name
            seriesBtn.gameObject:SetActive(true)
            UIEventListener.Get(seriesBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
                self:OnTipButtonClick(go)
            end)
        else
            self.Type.text = LocalString.GetString("无")
            seriesBtn.gameObject:SetActive(false)
        end
        
    else
        seriesBtn.gameObject:SetActive(false)
    end
end
function LuaDaFuWongLandTip:InitLv(view,lv)
	if not view then return end
	for i = 0,view.transform.childCount - 1 do
		local go = view.transform:GetChild(i)
		local highlight = go.transform:Find("highlight").gameObject
        highlight.gameObject:SetActive(true)
		if i >= lv then
			highlight.gameObject:SetActive(false)
		end
		-- if go then
		-- 	go:GetComponent(typeof(UISprite)).spriteName = name
		-- end
	end
end

function LuaDaFuWongLandTip:OnTipButtonClick(go)
	local tip = LuaDaFuWongMgr:GetTipsString(self.m_LandID)
	CMessageTipMgr.Inst:Display(WndType.Tooltip, false, "", tip, 700, go.transform, AlignType.Right, 4294967295)
end
-- 显示售价
function LuaDaFuWongLandTip:ShowPrice(base,cur)
    local namelabel = self.Cost.transform:Find("CostTypeLabel"):GetComponent(typeof(UILabel))
    namelabel.text = LocalString.GetString("售价")
    self.Cost.text = cur
    if base ~= cur then
        self.ExtraCost.gameObject:SetActive(true)
        self.OriCost.text = SafeStringFormat3("(%d)",base)
        self.UpOrDown:GetComponent(typeof(UISprite)).spriteName = base < cur and "common_arrow_07_red" or "common_arrow_07_green"
    else
        self.ExtraCost.gameObject:SetActive(false)
    end
end
-- 显示过路费
function LuaDaFuWongLandTip:ShowCost(base,cur)
    local namelabel = self.Cost.transform:Find("CostTypeLabel"):GetComponent(typeof(UILabel))
    namelabel.text = LocalString.GetString("过路费")
    self.Cost.text = cur
    if base ~= cur then
        self.ExtraCost.gameObject:SetActive(true)
        self.OriCost.text = SafeStringFormat3("(%d)",base)
        self.UpOrDown:GetComponent(typeof(UISprite)).spriteName = base < cur and "common_arrow_07_red" or "common_arrow_07_green"
    else
        self.ExtraCost.gameObject:SetActive(false)
    end
end
--@region UIEvent

--@endregion UIEvent

