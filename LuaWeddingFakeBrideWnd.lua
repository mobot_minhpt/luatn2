local DelegateFactory   = import "DelegateFactory"
local GameObject        = import "UnityEngine.GameObject"
local UILabel           = import "UILabel"
local UIGrid            = import "UIGrid"
local CUICommonDef      = import "L10.UI.CUICommonDef"
local CUIManager        = import "L10.UI.CUIManager"
local UIEventListener   = import "UIEventListener"
local CServerTimeMgr    = import "L10.Game.CServerTimeMgr"
local MessageWndManager = import "L10.UI.MessageWndManager"
local UISprite          = import "UISprite"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CThumbnailChatWnd = import "L10.UI.CThumbnailChatWnd"
local EnumGender        = import "L10.Game.EnumGender"
local UITexture         = import "UITexture"
local CUITexture        = import "L10.UI.CUITexture"

local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local CPropertyAppearance          = import "L10.Game.CPropertyAppearance"
local CPropertySkill               = import "L10.Game.CPropertySkill"

LuaWeddingFakeBrideWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaWeddingFakeBrideWnd, "OptionTemplate", "OptionTemplate", GameObject)
RegistChildComponent(LuaWeddingFakeBrideWnd, "Title", "Title", UILabel)
RegistChildComponent(LuaWeddingFakeBrideWnd, "Grid", "Grid", UIGrid)
RegistChildComponent(LuaWeddingFakeBrideWnd, "LeftButton", "LeftButton", GameObject)
RegistChildComponent(LuaWeddingFakeBrideWnd, "CenterButton", "CenterButton", GameObject)
RegistChildComponent(LuaWeddingFakeBrideWnd, "RightButton", "RightButton", GameObject)
RegistChildComponent(LuaWeddingFakeBrideWnd, "DownLabel", "DownLabel", UILabel)
RegistChildComponent(LuaWeddingFakeBrideWnd, "Up", "Up", GameObject)
RegistChildComponent(LuaWeddingFakeBrideWnd, "CloseButton", "CloseButton", GameObject)
RegistChildComponent(LuaWeddingFakeBrideWnd, "Side", "Side", GameObject)

--@endregion RegistChildComponent end

RegistClassMember(LuaWeddingFakeBrideWnd, "options")
RegistClassMember(LuaWeddingFakeBrideWnd, "curChoice")
RegistClassMember(LuaWeddingFakeBrideWnd, "centerButtonLabel")
RegistClassMember(LuaWeddingFakeBrideWnd, "countDownTick")
RegistClassMember(LuaWeddingFakeBrideWnd, "endTimeStamp")

RegistClassMember(LuaWeddingFakeBrideWnd, "buttonCDTick")
RegistClassMember(LuaWeddingFakeBrideWnd, "buttonCDEndTimeStamp")
RegistClassMember(LuaWeddingFakeBrideWnd, "isButtonInCD")

RegistClassMember(LuaWeddingFakeBrideWnd, "brideWordLabel")
RegistClassMember(LuaWeddingFakeBrideWnd, "guestWordLabel")
RegistClassMember(LuaWeddingFakeBrideWnd, "brideBubbleTick")
RegistClassMember(LuaWeddingFakeBrideWnd, "guestBubbleTick")
RegistClassMember(LuaWeddingFakeBrideWnd, "brideWordId")
RegistClassMember(LuaWeddingFakeBrideWnd, "guestWordId")
RegistClassMember(LuaWeddingFakeBrideWnd, "words")
RegistClassMember(LuaWeddingFakeBrideWnd, "selectedWordIds")

RegistClassMember(LuaWeddingFakeBrideWnd, "guestId")
RegistClassMember(LuaWeddingFakeBrideWnd, "brideId")

RegistClassMember(LuaWeddingFakeBrideWnd, "guestRO")
RegistClassMember(LuaWeddingFakeBrideWnd, "brideRO")
RegistClassMember(LuaWeddingFakeBrideWnd, "guestIdentifier")
RegistClassMember(LuaWeddingFakeBrideWnd, "brideIdentifier")

function LuaWeddingFakeBrideWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.LeftButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLeftButtonClick()
	end)

	UIEventListener.Get(self.CenterButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCenterButtonClick()
	end)

	UIEventListener.Get(self.RightButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRightButtonClick()
	end)

	UIEventListener.Get(self.CloseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCloseButtonClick()
	end)

    --@endregion EventBind end

	self:InitUIComponents()
	self:InitActive()
end

-- 初始化UI组件
function LuaWeddingFakeBrideWnd:InitUIComponents()
	local stage = LuaWeddingIterationMgr.fakeBrideStage
	local trans = stage == "Choose" and self.Side.transform or self.Up.transform
	self.brideWordLabel = trans:Find("Bride/Word"):GetComponent(typeof(UILabel))
	self.guestWordLabel = trans:Find("Guest/Word"):GetComponent(typeof(UILabel))
end

-- 初始化active
function LuaWeddingFakeBrideWnd:InitActive()
	self.OptionTemplate:SetActive(false)
	local optionTrans = self.OptionTemplate.transform
	optionTrans:Find("Male").gameObject:SetActive(false)
	optionTrans:Find("Female").gameObject:SetActive(false)
	optionTrans:Find("Selected").gameObject:SetActive(false)

	self.brideWordLabel.gameObject:SetActive(false)
	self.guestWordLabel.gameObject:SetActive(false)
end


function LuaWeddingFakeBrideWnd:OnEnable()
	g_ScriptEvent:AddListener("SyncNewWeddingCurrentChoice", self, "OnSyncGuestChoiceToBride")
	g_ScriptEvent:AddListener("SyncNewWeddingStageInfo", self, "OnSyncStageInfo")
	g_ScriptEvent:AddListener("SyncConfirmNewWeddingChoice", self, "OnSyncConfirmChoice")
	g_ScriptEvent:AddListener("SyncNewWeddingChoiceConfirm", self, "OnSyncChoiceConfirm")
end

function LuaWeddingFakeBrideWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SyncNewWeddingCurrentChoice", self, "OnSyncGuestChoiceToBride")
	g_ScriptEvent:RemoveListener("SyncNewWeddingStageInfo", self, "OnSyncStageInfo")
	g_ScriptEvent:RemoveListener("SyncConfirmNewWeddingChoice", self, "OnSyncConfirmChoice")
	g_ScriptEvent:RemoveListener("SyncNewWeddingChoiceConfirm", self, "OnSyncChoiceConfirm")
end

-- 同步宾客选择信息
function LuaWeddingFakeBrideWnd:OnSyncGuestChoiceToBride()
	local stage = LuaWeddingIterationMgr.fakeBrideStage

	if stage == "Hint" then
		local guestChoice = LuaWeddingIterationMgr.fakeBrideInfo.guestChoice
		self.guestId = LuaWeddingIterationMgr.fakeBrideInfo.guestId
		if self.guestId and guestChoice then
			self:UpdateOption(guestChoice)
		end
	end
end

-- 同步婚礼进行阶段信息
function LuaWeddingFakeBrideWnd:OnSyncStageInfo()
	local stage = LuaWeddingIterationMgr.stage
	local stageInfo = LuaWeddingIterationMgr.stageInfo

	if LuaWeddingIterationMgr.fakeBrideStage == "SetAnswer" then
		if stage == 1 and stageInfo[1] == "SetAnswer" then
			self:UpdateCenterButtonLabel()
		end
	end
end

-- 同步当前选项是否正确
function LuaWeddingFakeBrideWnd:OnSyncConfirmChoice(senderEngineId, choiceId, isRight)
	local stage = LuaWeddingIterationMgr.fakeBrideStage

	if stage ~= "Choose" or choiceId ~= self.curChoice then return end

	if isRight then
		self:UpdateBubble("FakeBrideAnswerRight")
	else
		self:UpdateBubble("FakeBrideAnswerWrong")
	end
end

-- 假新娘被提问当前选项是否正确
function LuaWeddingFakeBrideWnd:OnSyncChoiceConfirm(senderId, choiceId)
	if self.guestId == nil or self.guestId ~= senderId or self.curChoice ~= choiceId then
		return
	end

	self:UpdateBubble("GroomGuestAsk")
end


function LuaWeddingFakeBrideWnd:Init()
	self:InitClassMembers()
	self:InitModel()
	self:InitQuestion()
	self:InitDownLabel()
	self:InitButton()
end

-- 初始化成员变量
function LuaWeddingFakeBrideWnd:InitClassMembers()
	self.brideWordId = 0
	self.brideWordId = 0
	self.curChoice = 0

	self.isButtonInCD = false

	self.words = {}
	self.selectedWordIds = {}

	-- 待优化
	local wordsArray = WeddingIteration_Setting.GetData().GroomGuestAsk
	self.words["GroomGuestAsk"] = LuaWeddingIterationMgr:Array2Tbl(wordsArray)
	self.selectedWordIds["GroomGuestAsk"] = 0

	wordsArray = WeddingIteration_Setting.GetData().FakeBrideAnswerRight
	self.words["FakeBrideAnswerRight"] = LuaWeddingIterationMgr:Array2Tbl(wordsArray)
	self.selectedWordIds["FakeBrideAnswerRight"] = 0

	wordsArray = WeddingIteration_Setting.GetData().FakeBrideAnswerWrong
	self.words["FakeBrideAnswerWrong"] = LuaWeddingIterationMgr:Array2Tbl(wordsArray)
	self.selectedWordIds["FakeBrideAnswerWrong"] = 0
end

-- 初始化
function LuaWeddingFakeBrideWnd:InitModel()
	local stage = LuaWeddingIterationMgr.fakeBrideStage

	self.Up:SetActive(false)
	self.Side:SetActive(false)
	if stage == "Choose" then
		local brideModel = self.Side.transform:Find("Bride"):Find("Model"):GetComponent(typeof(UITexture))
		local guestModel = self.Side.transform:Find("Guest"):Find("Model"):GetComponent(typeof(UITexture))

		self.brideIdentifier = SafeStringFormat3("__%s__", tostring(brideModel.gameObject:GetInstanceID()))
		self.guestIdentifier = SafeStringFormat3("__%s__", tostring(guestModel.gameObject:GetInstanceID()))

		self:CreateModel(self.brideIdentifier, brideModel, true)
		self:CreateModel(self.guestIdentifier, guestModel, false)
		self.Side:SetActive(true)
	elseif stage == "Hint" then
		local cTex = self.Up.transform:Find("Guest"):GetComponent(typeof(CUITexture))

		local info = LuaWeddingIterationMgr.fakeBrideInfo
		local mat = CUICommonDef.GetPortraitName(info.guestClass, info.guestGender, -1)
		cTex:LoadNPCPortrait(mat)
		self.Up:SetActive(true)
	end
end

-- 创建人物模型
function LuaWeddingFakeBrideWnd:CreateModel(identifier, tex, isBride)
    local loader = self:GetModelLoader(isBride)

    tex.mainTexture = CUIManager.CreateModelTexture(identifier, loader,
        180, 0.05, -1, 4.66, false, true, 1.0, false, false)
end

-- 生成外观
function LuaWeddingFakeBrideWnd:GetModelLoader(isBride)
    return LuaDefaultModelTextureLoader.Create(function (ro)
		local appearanceData = nil
		if isBride then
			local class = LuaWeddingIterationMgr.fakeBrideInfo.brideClass
			appearanceData = CPropertyAppearance.GetDataFromCustom(class, EnumGender.Female,
            CPropertySkill.GetDefaultYingLingStateByGender(EnumGender.Female), nil, nil, 0, 0, 0, 0,
            WeddingIteration_Setting.GetData().WeddingDressHeadId,
            WeddingIteration_Setting.GetData().WeddingDressBodyId,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, nil, nil, nil, nil, 0, 0, 0, 0, 0, 0, nil, 0)
		else
			appearanceData = CClientMainPlayer.Inst.AppearanceProp
		end

        CClientMainPlayer.LoadResource(ro, appearanceData, true, 1.0,
            0, false, 0, false, 0, false, nil, false)

        if isBride then
            self.brideRO = ro
        else
            self.guestRO = ro
        end
    end)
end

-- 初始化题目
function LuaWeddingFakeBrideWnd:InitQuestion()
	local info = LuaWeddingIterationMgr.fakeBrideInfo
	local data = WeddingIteration_Question.GetData(info.questionId)
	self.Title.text = data.Question
	local stage = LuaWeddingIterationMgr.fakeBrideStage

	local answers = {data.Answer1, data.Answer2, data.Answer3, data.Answer4}
	self.options = {}
    Extensions.RemoveAllChildren(self.Grid.transform)
	for i = 1, #answers do
		local option = CUICommonDef.AddChild(self.Grid.gameObject, self.OptionTemplate)
		option:SetActive(true)

		local optionTrans = option.transform
		optionTrans:Find("Label"):GetComponent(typeof(UILabel)).text = answers[i]

		UIEventListener.Get(option).onClick = DelegateFactory.VoidDelegate(function (go)
			if stage == "SetAnswer" or stage == "Choose" then
				self:UpdateOption(i)
			elseif stage == "Hint" then
				g_MessageMgr:ShowMessage("WEDDING_GUEST_IS_CHOOSING")
			end

			if stage == "Choose" then
				Gac2Gas.RequestSendNewWeddingCurrentChoice(i)
			end
		end)

		self.options[i] = optionTrans
	end
	self.Grid:Reposition()

	if stage == "Hint" then
		local rightAnswerId = info.rightAnswerId
		self.options[rightAnswerId]:Find("Female").gameObject:SetActive(true)
	end
	self:OnSyncGuestChoiceToBride()
end

-- 更新选项
function LuaWeddingFakeBrideWnd:UpdateOption(id)
	local stage = LuaWeddingIterationMgr.fakeBrideStage

	local beforeOption = self.options[self.curChoice]
	local option = self.options[id]

	if stage == "SetAnswer" then
		self:SetOptionActive(beforeOption, "Female", false)
		self:SetOptionActive(option, "Female", true)
	elseif stage == "Choose" or stage == "Hint" then
		self:SetOptionActive(beforeOption, "Male", false)
		self:SetOptionActive(option, "Male", true)
	end

	self:SetOptionActive(beforeOption, "Selected", false)
	self:SetOptionActive(option, "Selected", true)

	self.curChoice = id
end

-- 设置选项的状态
function LuaWeddingFakeBrideWnd:SetOptionActive(option, str, active)
	if option == nil then return end
	option:Find(str).gameObject:SetActive(active)
end

-- 初始化底部标签
function LuaWeddingFakeBrideWnd:InitDownLabel()
	local stage = LuaWeddingIterationMgr.fakeBrideStage

	if stage == "SetAnswer" then
		self.DownLabel.text = LocalString.GetString("请选择一个答案让男方宾客来猜")
	elseif stage == "Choose" then
		self.DownLabel.text = LocalString.GetString("请选择你认为她心中的答案")
	elseif stage == "Hint" then
		self.DownLabel.text = LocalString.GetString("男方宾客正在选择，可选择给予提示~")
	end
end

-- 初始化按钮
function LuaWeddingFakeBrideWnd:InitButton()
	local stage = LuaWeddingIterationMgr.fakeBrideStage
	local leftButtonLabel = self.LeftButton.transform:Find("Label"):GetComponent(typeof(UILabel))
	local rightButtonLabel = self.RightButton.transform:Find("Label"):GetComponent(typeof(UILabel))
	self.centerButtonLabel = self.CenterButton.transform:Find("Label"):GetComponent(typeof(UILabel))

	if stage == "SetAnswer" then
		self:SetButtonActive(false, true, false)
		self:UpdateCenterButtonLabel()
	elseif stage == "Choose" then
		self:SetButtonActive(true, false, true)
		leftButtonLabel.text = LocalString.GetString("询问新娘")
		rightButtonLabel.text = LocalString.GetString("确定选择")

		local leftButtonSprite = self.LeftButton.transform:GetComponent(typeof(UISprite))
		leftButtonSprite.spriteName = "common_btn_01_blue"
	elseif stage == "Hint" then
		self:SetButtonActive(true, false, true)
		leftButtonLabel.text = LocalString.GetString("提示正确")
		rightButtonLabel.text = LocalString.GetString("提示错误")
	end
end

-- 设置按钮状态
function LuaWeddingFakeBrideWnd:SetButtonActive(leftActive, centerActive, rightActive)
	self.LeftButton:SetActive(leftActive)
	self.CenterButton:SetActive(centerActive)
	self.RightButton:SetActive(rightActive)
end

-- 更新确认选择倒计时
function LuaWeddingFakeBrideWnd:UpdateCenterButtonLabel()
	self:SetEndTimeStamp()
	if self.endTimeStamp then
		self:StartChooseCountDown()
	else
		self.centerButtonLabel.text = LocalString.GetString("确认选择")
	end
end

-- 设置确认选择的结束时间
function LuaWeddingFakeBrideWnd:SetEndTimeStamp()
	local stage = LuaWeddingIterationMgr.stage
	local stageInfo = LuaWeddingIterationMgr.stageInfo

	self.endTimeStamp = nil
	if #stageInfo == 0 then return end
	if stage == 1 then
		local subStage = stageInfo[1]
		if subStage == "SetAnswer" then
			self.endTimeStamp = stageInfo[2]
		end
	end
end

-- 确认选择倒计时
function LuaWeddingFakeBrideWnd:StartChooseCountDown()
	self:UpdateTimeOnce()
	self:ClearTick(self.countDownTick)
	self.countDownTick = RegisterTick(function()
		self:UpdateTimeOnce()
	end, 1000)
end

-- 更新一次时间
function LuaWeddingFakeBrideWnd:UpdateTimeOnce()
	local count = math.floor(self.endTimeStamp - CServerTimeMgr.Inst.timeStamp)
	if count <= 0 then
		self:RandomChoice()
	end

	self.centerButtonLabel.text = SafeStringFormat3(LocalString.GetString("确认选择(%d)"), count)
end

-- 随机一个选择并关闭界面
function LuaWeddingFakeBrideWnd:RandomChoice()
	CThumbnailChatWnd.Instance:ShowThumbnailExpressions()
	CUIManager.CloseUI(CLuaUIResources.WeddingFakeBrideWnd)
end

-- 更新气泡显示
function LuaWeddingFakeBrideWnd:UpdateBubble(str)
	-- 随机一句话
	local words = self.words[str]
	local curWordId = self.selectedWordIds[str]

	local wordNum = #words
	local newWordId = LuaWeddingIterationMgr:GetDifferentRandomNum(wordNum, curWordId)

	self.selectedWordIds[str] = newWordId
	local word = self.words[str][newWordId]

	if str == "GroomGuestAsk" then
		LuaWeddingIterationMgr:SetBubbleWord(self.guestWordLabel, word)
		self:ClearTick(self.guestBubbleTick)
		self.guestBubbleTick = RegisterTickOnce(function()
			self.guestWordLabel.gameObject:SetActive(false)
		end, 1000 * 2)
	else
		LuaWeddingIterationMgr:SetBubbleWord(self.brideWordLabel, word)
		self:ClearTick(self.brideBubbleTick)
		self.brideBubbleTick = RegisterTickOnce(function()
			self.brideWordLabel.gameObject:SetActive(false)
		end, 1000 * 2)
	end

	self:PlayAction(str)
end

-- 更新动作
function LuaWeddingFakeBrideWnd:PlayAction(str)
	local stage = LuaWeddingIterationMgr.fakeBrideStage

	if stage ~= "Choose" then return end

	if str == "GroomGuestAsk" then
		local expressionId = WeddingIteration_Setting.GetData().GuestAskExpressionId
		local gender = CClientMainPlayer.Inst.Gender
		LuaWeddingIterationMgr:PlayAction(self.guestRO, expressionId, gender, true)
	elseif str == "FakeBrideAnswerRight" then
		local expressionId = WeddingIteration_Setting.GetData().BrideAnswerRightExpressionId
		LuaWeddingIterationMgr:PlayAction(self.brideRO, expressionId, EnumGender.Female, true)
	elseif str == "FakeBrideAnswerWrong" then
		local expressionId = WeddingIteration_Setting.GetData().BrideAnswerWrongExpressionId
		LuaWeddingIterationMgr:PlayAction(self.brideRO, expressionId, EnumGender.Female, true)
	end
end

-- 开始按键cd计时
function LuaWeddingFakeBrideWnd:StartButtonCDTick()
	self.ClearTick(self.buttonCDTick)
	self.isButtonInCD = true
	local cdTime = WeddingIteration_Setting.GetData().FakeBrideCDTime
	self.buttonCDEndTimeStamp = cdTime + CServerTimeMgr.Inst.timeStamp
	self.buttonCDTick = RegisterTickOnce(function()
		self.isButtonInCD = false
		self.buttonCDTick = self.ClearTick(self.buttonCDTick)
	end, 1000 * cdTime)
end

-- 计算剩余时间
function LuaWeddingFakeBrideWnd:GetButtonCdLeftTime()
	if not self.isButtonInCD then return 0 end
	local leftTime = math.ceil(self.buttonCDEndTimeStamp - CServerTimeMgr.Inst.timeStamp)
	if leftTime <= 0 then
		self.isButtonInCD = false
		self.buttonCDTick = self.ClearTick(self.buttonCDTick)
		return 0
	end

	return leftTime
end

-- 清除计时器
function LuaWeddingFakeBrideWnd:ClearTick(tick)
	if tick then
		UnRegisterTick(tick)
	end
	return nil
end

-- 关闭界面时清除计时器
function LuaWeddingFakeBrideWnd:OnDestroy()
	self:ClearTick(self.guestBubbleTick)
	self:ClearTick(self.brideBubbleTick)
	self:ClearTick(self.countDownTick)
	self:ClearTick(self.buttonCDTick)

	CUIManager.DestroyModelTexture(self.guestIdentifier)
	CUIManager.DestroyModelTexture(self.brideIdentifier)
end

--@region UIEvent

function LuaWeddingFakeBrideWnd:OnLeftButtonClick()
	local stage = LuaWeddingIterationMgr.fakeBrideStage

	if stage == "Choose" then
		if self.curChoice <= 0 then
			g_MessageMgr:ShowMessage("WEDDING_NEED_SELECT_AN_ANSWER")
		else
			local leftTime = self:GetButtonCdLeftTime()
			if self.isButtonInCD and leftTime > 0 then
				g_MessageMgr:ShowMessage("WEDDING_ASK_TOO_OFTEN", leftTime)
				return
			end
			Gac2Gas.RequestConfirmNewWeddingChoice(self.curChoice)
			self:UpdateBubble("GroomGuestAsk")
			self:StartButtonCDTick()
		end
	elseif stage == "Hint" then
		if self.curChoice <= 0 or self.guestId == nil then
			g_MessageMgr:ShowMessage("WEDDING_GUEST_NO_CHOICE")
		else
			local leftTime = self:GetButtonCdLeftTime()
			if self.isButtonInCD and leftTime > 0 then
				g_MessageMgr:ShowMessage("WEDDING_HINT_TOO_OFTEN", leftTime)
				return
			end
			Gac2Gas.ReplyConfirmNewWeddingChoice(self.guestId, self.curChoice, true)
			self:UpdateBubble("FakeBrideAnswerRight")
			self:StartButtonCDTick()
		end
	end
end

function LuaWeddingFakeBrideWnd:OnCenterButtonClick()
	local stage = LuaWeddingIterationMgr.fakeBrideStage
	local questionId = LuaWeddingIterationMgr.fakeBrideInfo.questionId

	if stage == "SetAnswer" then
		if self.curChoice <= 0 then
			g_MessageMgr:ShowMessage("WEDDING_NEED_SELECT_AN_ANSWER")
		else
			Gac2Gas.RequestSetNewWeddingAnswer(questionId, self.curChoice)
			CThumbnailChatWnd.Instance:ShowThumbnailExpressions()
			CUIManager.CloseUI(CLuaUIResources.WeddingFakeBrideWnd)
		end
	end
end

function LuaWeddingFakeBrideWnd:OnRightButtonClick()
	local stage = LuaWeddingIterationMgr.fakeBrideStage

	if stage == "Choose" then
		if self.curChoice <= 0 then
			g_MessageMgr:ShowMessage("WEDDING_NEED_SELECT_AN_ANSWER")
		else
			Gac2Gas.RequestSelectNewWeddingChoice(self.curChoice)
			CUIManager.CloseUI(CLuaUIResources.WeddingFakeBrideWnd)
		end
	elseif stage == "Hint" then
		if self.curChoice <= 0 or self.guestId == nil then
			g_MessageMgr:ShowMessage("WEDDING_GUEST_NO_CHOICE")
		else
			local leftTime = self:GetButtonCdLeftTime()
			if self.isButtonInCD and leftTime > 0 then
				g_MessageMgr:ShowMessage("WEDDING_HINT_TOO_OFTEN", leftTime)
				return
			end
			Gac2Gas.ReplyConfirmNewWeddingChoice(self.guestId, self.curChoice, false)
			self:UpdateBubble("FakeBrideAnswerWrong")
			self:StartButtonCDTick()
		end
	end
end

function LuaWeddingFakeBrideWnd:OnCloseButtonClick()
	local stage = LuaWeddingIterationMgr.fakeBrideStage

	if stage == "SetAnswer" then
		local message = g_MessageMgr:FormatMessage("WEDDING_CLOSE_ANSWER_SELECT_CONFIRM")
		MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function ()
			self:RandomChoice()
		end), nil, nil, nil, false)
		return
	elseif stage == "Choose" then
		Gac2Gas.SendNewWeddingCloseConversationWnd()
	elseif stage == "Hint" then
		LuaWeddingIterationMgr:CloseFakeBrideHintWnd()
		return
	end

	CUIManager.CloseUI(CLuaUIResources.WeddingFakeBrideWnd)
end

--@endregion UIEvent

