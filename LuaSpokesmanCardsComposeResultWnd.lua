LuaSpokesmanCardsComposeResultWnd = class()

RegistChildComponent(LuaSpokesmanCardsComposeResultWnd, "m_Template1","Template1",CCommonLuaScript)
RegistChildComponent(LuaSpokesmanCardsComposeResultWnd, "m_Template2","Template2",CCommonLuaScript)
RegistChildComponent(LuaSpokesmanCardsComposeResultWnd, "m_Template3","Template3",CCommonLuaScript)
RegistChildComponent(LuaSpokesmanCardsComposeResultWnd, "m_Fx","Fx",CUIFx)
RegistChildComponent(LuaSpokesmanCardsComposeResultWnd, "m_Button","Button",GameObject)

RegistClassMember(LuaSpokesmanCardsComposeResultWnd, "m_Tick")

function LuaSpokesmanCardsComposeResultWnd:Init()
    self.m_Template1.gameObject:SetActive(false)
    self.m_Template2.gameObject:SetActive(false)
    self.m_Template3.gameObject:SetActive(false)
    if not CLuaSpokesmanMgr.m_SpokesmanCardsComposeResultList then
        CUIManager.CloseUI(CLuaUIResources.SpokesmanCardsComposeResultWnd)
    end
    self.m_Button:SetActive(false)
    self.m_Fx:LoadFx(#CLuaSpokesmanMgr.m_SpokesmanCardsComposeResultList == 1 and
            "Fx/UI/Prefab/UI_CharacterCardHeWnd_HeCheng_01.prefab" or
            "Fx/UI/Prefab/UI_CharacterCardHeWnd_HeCheng_02.prefab")
    self.m_Tick = RegisterTickOnce(function ()
        self:ShowCards()
    end,3000)
    UIEventListener.Get(self.m_Button).onClick = DelegateFactory.VoidDelegate(function (p)
        self:ShowCards()
        if self.m_Tick then
            UnRegisterTick(self.m_Tick)
            self.m_Tick = nil
        end
        self.m_Tick = RegisterTickOnce(function ()
            CUIManager.CloseUI(CLuaUIResources.SpokesmanCardsComposeResultWnd)
        end,100)
    end)
end

function LuaSpokesmanCardsComposeResultWnd:OnDisable()
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
    end
end

function LuaSpokesmanCardsComposeResultWnd:ShowCards()
    local scripts = { self.m_Template1,self.m_Template2, self.m_Template3}
    self.m_Template1.gameObject:SetActive(true)
    self.m_Template2.gameObject:SetActive(#CLuaSpokesmanMgr.m_SpokesmanCardsComposeResultList == 3)
    self.m_Template3.gameObject:SetActive(#CLuaSpokesmanMgr.m_SpokesmanCardsComposeResultList == 3)
    for i, t in pairs(CLuaSpokesmanMgr.m_SpokesmanCardsComposeResultList) do
        local id, owner, isnew = t.id, t.owner, t.isnew
        scripts[i]:InitCard(id, owner, isnew, 0)
    end
    self.m_Button:SetActive(true)
end
