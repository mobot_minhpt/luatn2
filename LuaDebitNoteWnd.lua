local UIVerticalLabel = import "UIVerticalLabel"
local UILongPressButton = import "L10.UI.UILongPressButton"
local UISlider = import "UISlider"
local UILabel = import "UILabel"

LuaDebitNoteWnd = class()

RegistChildComponent(LuaDebitNoteWnd, "m_VerticalLabel","VerticalLabel", UIVerticalLabel)
RegistChildComponent(LuaDebitNoteWnd, "m_Lenders","Lenders", UILongPressButton)
RegistChildComponent(LuaDebitNoteWnd, "m_EyewitnessDefault","EyewitnessDefault", GameObject)
RegistChildComponent(LuaDebitNoteWnd, "m_EyewitnessFingerprint","EyewitnessFingerprint", GameObject)
RegistChildComponent(LuaDebitNoteWnd, "m_LendersDefault","LendersDefault", GameObject)
RegistChildComponent(LuaDebitNoteWnd, "m_LendersFingerprint","LendersFingerprint", GameObject)
RegistChildComponent(LuaDebitNoteWnd, "m_Slider","Slider", UISlider)
RegistChildComponent(LuaDebitNoteWnd, "m_CloseButton","CloseButton", GameObject)

RegistClassMember(LuaDebitNoteWnd,"m_ReviewTime")
RegistClassMember(LuaDebitNoteWnd,"m_Tick")

function LuaDebitNoteWnd:Awake()
    local item = Item_Item.GetData(21031027)
    if item then 
        if CommonDefs.IS_VN_CLIENT then
            local labeltrans = self.transform:FindChild("Anchor/Page/HLabel")
            if labeltrans then
                labeltrans.gameObject:SetActive(true)
                local label = labeltrans.gameObject:GetComponent(typeof(UILabel))
                label.text = string.gsub(item.Content,"#r","\n")
            end
            self.m_VerticalLabel.gameObject:SetActive(false)
        else
            self.m_VerticalLabel.text = item.Content
        end
    end
    self.m_ReviewTime = ZhuJueJuQing_Setting.GetData().CheckDebitNoteTime
    if CLuaTaskMgr.m_DebitNoteState == 1 then
        self.m_Lenders.OnLongPressDelegate = DelegateFactory.Action(function ()
            self:OnFlagonPressStart()
        end)
        self.m_Lenders.OnLongPressEndDelegate = DelegateFactory.Action(function ()
            self:OnLongPressEnd()
        end)
    end

    UIEventListener.Get(self.m_CloseButton).onClick = DelegateFactory.VoidDelegate(function (p)
        if CLuaTaskMgr.m_DebitNoteState == 1 then
            CUIManager.CloseUI(CLuaUIResources.DebitNoteWnd)
            return
        end
        if (CLuaTaskMgr.m_DebitNoteState == 2) then
            if (self.m_Slider.value == 1) then
                CUIManager.CloseUI(CLuaUIResources.DebitNoteWnd)
                return
            else
                g_MessageMgr:ShowMessage("DebitNoteWnd_Reviewing_CanNotCloseWnd")
            end
        end
    end)

    self.m_LendersFingerprint:SetActive(CLuaTaskMgr.m_DebitNoteState == 2)
    self.m_EyewitnessFingerprint:SetActive(CLuaTaskMgr.m_DebitNoteState == 2)
    self.m_LendersDefault:SetActive(CLuaTaskMgr.m_DebitNoteState == 1)
    self.m_EyewitnessDefault:SetActive(false)
    self.m_Slider.gameObject:SetActive(CLuaTaskMgr.m_DebitNoteState == 2)

    if CLuaTaskMgr.m_DebitNoteState == 2 then
        LuaTweenUtils.DOKill(self.transform, false)
        local reviewTween = LuaTweenUtils.TweenFloat(0,1,self.m_ReviewTime, function (val)
            self.m_Slider.value = val
            if (self.m_Slider.value == 1) then
                CUIManager.CloseUI(CLuaUIResources.DebitNoteWnd)
            end
        end)
        LuaTweenUtils.SetTarget(reviewTween, self.transform)
    end
end

function LuaDebitNoteWnd:OnDisable()
    LuaTweenUtils.DOKill(self.transform, false)
    if CLuaTaskMgr.m_DebitNoteState == 2 then
        Gac2Gas.FinishEventTask(CLuaTaskMgr.m_DebitNote_TaskId,"DebitNote2")
    end
    self:CancelCloseWndTick()
end

function LuaDebitNoteWnd:OnFlagonPressStart()
end

function LuaDebitNoteWnd:OnLongPressEnd()
    self.m_LendersFingerprint:SetActive(true)
    self.m_LendersDefault:SetActive(false)
    Gac2Gas.FinishEventTask(CLuaTaskMgr.m_DebitNote_TaskId,"DebitNote1")
    self:CancelCloseWndTick()
    self.m_Tick = RegisterTickOnce(function ()
        CUIManager.CloseUI(CLuaUIResources.DebitNoteWnd)
    end,3000)
end

function LuaDebitNoteWnd:CancelCloseWndTick()
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        m_Tick = nil
    end
end
