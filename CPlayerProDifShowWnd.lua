-- Auto Generated!!
local CPlayerProDifShowItem = import "CPlayerProDifShowItem"
local CPlayerProDifShowWnd = import "L10.UI.CPlayerProDifShowWnd"
local CPlayerProSaveMgr = import "L10.Game.CPlayerProSaveMgr"
local CPropertyDifMgr = import "L10.UI.CPropertyDifMgr"
local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local Extensions = import "Extensions"
local L10 = import "L10"
local NGUITools = import "NGUITools"
local UISprite = import "UISprite"
local Vector3 = import "UnityEngine.Vector3"
CPlayerProDifShowWnd.m_Awake_CS2LuaHook = function (this)
    if CPlayerProDifShowWnd.Instance ~= nil then
        L10.CLogMgr.LogError("more than one instance running!")
    end
    CPlayerProDifShowWnd.Instance = this
end
CPlayerProDifShowWnd.m_Init_CS2LuaHook = function (this)
    this.templateNode:SetActive(false)

    Extensions.RemoveAllChildren(this.fatherNode.transform)

    local showList = CPropertyDifMgr.sourceList
    if showList ~= nil and showList.Count > 0 then
        local totalCount = showList.Count
        local half = totalCount / 2
        if totalCount > 0 then
            do
                local i = 0
                while i < totalCount do
                    local instance = NGUITools.AddChild(this.fatherNode, this.templateNode)
                    instance:SetActive(true)
                    local info = showList[i]
                    CommonDefs.GetComponent_GameObject_Type(instance, typeof(CPlayerProDifShowItem)):init(info, Vector3(0, (half - i - 0.5) * this.dis, 0), i + 1, totalCount)
                    i = i + 1
                end
            end

            CommonDefs.GetComponent_GameObject_Type(this.bgNode, typeof(UISprite)).height = math.floor((this.dis * totalCount)) + 10
        else
            this:close()
        end
    end
end

CPropertyDifMgr.m_ShowPlayerProDif_CS2LuaHook = function ()
    -- 特殊处理一下全民PK，防止刚建号时弹出这个窗口
    if CQuanMinPKMgr.Inst.m_IsQuanMinPKServer and CQuanMinPKMgr.Inst.m_IsOperatingPlayerResource then
        return
    end
    if CPlayerProSaveMgr.Inst.showList ~= nil and CPlayerProSaveMgr.Inst.showList.Count > 0 then
      CPropertyDifMgr.sourceList = CPlayerProSaveMgr.Inst.showList
      if not CUIManager.IsLoaded(CUIResources.PlayerProDifShowWnd) then
        CUIManager.ShowUI(CUIResources.PlayerProDifShowWnd)
      end
    end
end
