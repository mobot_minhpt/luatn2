-- Auto Generated!!
local AlignType = import "CPlayerInfoMgr+AlignType"
local CIMMgr = import "L10.Game.CIMMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local CShiTuChooseTudiWnd = import "L10.UI.CShiTuChooseTudiWnd"
local CShiTuMgr = import "L10.Game.CShiTuMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUITexture = import "L10.UI.CUITexture"
local DelegateFactory = import "DelegateFactory"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumClass = import "L10.Game.EnumClass"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local MessageWndManager = import "L10.UI.MessageWndManager"
local NGUITools = import "NGUITools"
local Profession = import "L10.Game.Profession"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local UISprite = import "UISprite"
local Vector3 = import "UnityEngine.Vector3"
CShiTuChooseTudiWnd.m_GenerateNode_CS2LuaHook = function (this, data) 
    local node = NGUITools.AddChild(this.table.gameObject, this.template)
    node:SetActive(true)

    CommonDefs.GetComponent_Component_Type(node.transform:Find("Name/text"), typeof(UILabel)).text = data.Name
    CommonDefs.GetComponent_Component_Type(node.transform:Find("Name/JobIcon"), typeof(UISprite)).spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), (data.Class)))
    CommonDefs.GetComponent_Component_Type(node.transform:Find("lv"), typeof(UILabel)).text = "lv." .. data.Level
    local iconbutton = node.transform:Find("icon/button").gameObject
    local button = node.transform:Find("button").gameObject

    UIEventListener.Get(iconbutton).onClick = DelegateFactory.VoidDelegate(function (p) 
        CPlayerInfoMgr.ShowPlayerPopupMenu(data.ID, EnumPlayerInfoContext.ChatLink, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
    end)

    UIEventListener.Get(button).onClick = DelegateFactory.VoidDelegate(function (p) 
        local message = g_MessageMgr:FormatMessage("BreakShiTu_Confirm", data.Name)
        MessageWndManager.ShowDelayOKCancelMessage(message, DelegateFactory.Action(function () 
            Gac2Gas.BreakShiTuRelationship(false, data.ID)
        end), nil, 5)
    end)

    CommonDefs.GetComponent_Component_Type(node.transform:Find("icon"), typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(data.Class, data.Gender, data.Expression), false)
end
CShiTuChooseTudiWnd.m_Init_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        this:Close()
    end)

    this.template:SetActive(false)

    local tudiList = CShiTuMgr.Inst:GetAllCurrentTuDi()
    if tudiList.Count > 0 then
        do
            local i = 0
            while i < tudiList.Count do
                local playerId = tudiList[i]
                local info = CIMMgr.Inst:GetBasicInfo(playerId)
                if info ~= nil then
                    this:GenerateNode(info)
                end
                i = i + 1
            end
        end
        this.table:Reposition()
        this.scrollView:ResetPosition()
    else
        this:Close()
    end
end
