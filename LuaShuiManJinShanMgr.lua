local CPostEffectMgr = import "L10.Engine.CPostEffectMgr"
local CRainDropEffect = import "L10.Engine.CRainDropEffect"
local CommonDefs = import "L10.Game.CommonDefs"
local CRenderScene = import "L10.Engine.Scene.CRenderScene"
local CWater = import "L10.Engine.CWater"
local CWaterMeshReArrange = import "L10.Engine.CWaterMeshReArrange"
local CPos = import "L10.Engine.CPos"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local CEffectMgr = import "L10.Game.CEffectMgr"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CPostProcessingMgr = import "L10.Engine.PostProcessing.CPostProcessingMgr"

LuaShuiManJinShanMgr = {}
LuaShuiManJinShanMgr.m_IsConfirmState = false
LuaShuiManJinShanMgr.m_GameplayId = nil
LuaShuiManJinShanMgr.m_SoloInfo = nil
LuaShuiManJinShanMgr.m_GroupInfo = nil
LuaShuiManJinShanMgr.m_CurForce = nil
LuaShuiManJinShanMgr.m_IsSolo = false
LuaShuiManJinShanMgr.m_SignUpDifficulty = 1
LuaShuiManJinShanMgr.m_CurDifficulty = 1
LuaShuiManJinShanMgr.m_IsMatching = false
LuaShuiManJinShanMgr.m_IsConfirmed = false
LuaShuiManJinShanMgr.m_RewardItems = false
LuaShuiManJinShanMgr.m_CurUnlockIdx = false
LuaShuiManJinShanMgr.m_PlayState = 0
LuaShuiManJinShanMgr.m_GameplayId2Force = nil
-- 海浪相关
LuaShuiManJinShanMgr.m_WaterTweener = nil
LuaShuiManJinShanMgr.m_HaiLangFxId = 88803393
LuaShuiManJinShanMgr.m_HalLangDir = 0
LuaShuiManJinShanMgr.m_HaiLangFx = nil
LuaShuiManJinShanMgr.m_HaiLangTweener = nil
LuaShuiManJinShanMgr.m_IsTryOpenSoloWnd = false
LuaShuiManJinShanMgr.m_AllNPC = {}

function LuaShuiManJinShanMgr:OpenSoloWnd()
    self.m_IsSolo = true
    self.m_SignUpDifficulty = 1
    self.m_GameplayId = self.m_GameplayId or XinBaiLianDong_Setting.GetData().ShuiManJinShan_GameplayId
    if self.m_GameplayId and CClientMainPlayer.Inst then
        Gac2Gas.GlobalMatch_RequestCheckSignUp(self.m_GameplayId, CClientMainPlayer.Inst.Id)
        self.m_IsTryOpenSoloWnd = true
    end
end

function LuaShuiManJinShanMgr:OpenGroupWnd(difficulty)
    self.m_IsSolo = false
    self.m_SignUpDifficulty = difficulty or 1
    self.m_GameplayId = self.m_GameplayId or XinBaiLianDong_Setting.GetData().ShuiManJinShan_GameplayId
    if self.m_GameplayId then
        Gac2Gas.ShuiManJinShan_QueryTeamSignUp()
    end
end

function LuaShuiManJinShanMgr:AddListener()
    g_ScriptEvent:RemoveListener("GlobalMatch_CheckInMatchingResultWithInfo", self, "GlobalMatch_CheckInMatchingResultWithInfo")
    g_ScriptEvent:AddListener("GlobalMatch_CheckInMatchingResultWithInfo", self, "GlobalMatch_CheckInMatchingResultWithInfo")
end
LuaShuiManJinShanMgr:AddListener()

function LuaShuiManJinShanMgr:GlobalMatch_CheckInMatchingResultWithInfo(playerId, playId, isInMatching, resultStr, info)
    if CClientMainPlayer.Inst == nil or playerId ~= CClientMainPlayer.Inst.Id or playId ~= self.m_GameplayId then
        return 
    end

    self.m_SoloInfo = info
    self.m_CurForce = info and info.Force
    self.m_IsMatching = isInMatching

    if self.m_IsTryOpenSoloWnd then
        LuaShuiManJinShanMgr.m_IsConfirmState = isInMatching
        CUIManager.ShowUI(CLuaUIResources.ShuiManJinShanConfirmWnd)
        self.m_IsTryOpenSoloWnd = false
    else
        if not isInMatching then
            g_ScriptEvent:BroadcastInLua("GlobalMatch_CancelSignUpResult", playId, true)
        end
    end
end

function LuaShuiManJinShanMgr:ShuiManJinShan_LeaderRequestSignUp(info)
    if CClientMainPlayer.Inst == nil then
        return 
    end

    self.m_IsSolo = false
    self.m_GroupInfo = info
    self.m_CurForce = info and info.Force
    self.m_IsConfirmed = info and info.Confirm[CClientMainPlayer.Inst.Id]
    self.m_CurDifficulty = info and info.Difficulty

    LuaShuiManJinShanMgr.m_IsConfirmState = info ~= nil
    CUIManager.ShowUI(CLuaUIResources.ShuiManJinShanConfirmWnd)
end

function LuaShuiManJinShanMgr:ShuiManJinShan_MemberConfirmSignUp(memberId, buffIdx)
    g_ScriptEvent:BroadcastInLua("ShuiManJinShan_MemberConfirmSignUp", memberId, buffIdx)
end

function LuaShuiManJinShanMgr:ShuiManJinShan_PopUpSuccessWnd(rewardItems, unlockIdx, forceId)
    self.m_RewardItems = rewardItems
    self.m_CurUnlockIdx = unlockIdx
    self.m_CurForce = forceId

    CUIManager.ShowUI(CLuaUIResources.ShuiManJinShanResultWnd)
end

function LuaShuiManJinShanMgr:ShuiManJinShan_SyncPlayState(playState, waterFactor)
    self.m_PlayState = playState
    self:SetWaterHeight(waterFactor)
    g_ScriptEvent:BroadcastInLua("ShuiManJinShan_SyncPlayState", playState)
end

function LuaShuiManJinShanMgr:ShuiManJinShan_CreateWave(x, y, duration, speed, height)
    g_ScriptEvent:BroadcastInLua("ShuiManJinShan_CreateWave", x, y, duration, speed, height)
    if self.m_HaiLangFx ~= nil then
        self.m_HaiLangFx:Destroy()
        self.m_HaiLangFx = nil
    end

    local posX = math.floor(tonumber(x or 0))
    local posY = math.floor(tonumber(y or 0))
    local pos = CreateFromClass(CPos, posX, posY)
    local pixelPos = Utility.GridPos2PixelPos(pos)
    local worldPos = Utility.PixelPos2WorldPos(pixelPos)
    local fxPos = Vector3(worldPos.x, height + 30, worldPos.z)
    self.m_HaiLangFx = CEffectMgr.Inst:AddWorldPositionFX(self.m_HaiLangFxId, fxPos, self.m_HalLangDir, 0, 1, -1, EnumWarnFXType.None, nil, 0, 0, DelegateFactory.Action_GameObject(function(go)
        local moveOffset = CommonDefs.op_Multiply_Vector3_Single(go.transform.forward, speed * duration)
        local targetPos = CommonDefs.op_Addition_Vector3_Vector3(go.transform.position, moveOffset)
        
        if self.m_HaiLangTweener then
            LuaTweenUtils.Kill(self.m_HaiLangTweener, false)
            self.m_HaiLangTweener = nil
        end
        self.m_HaiLangTweener = LuaTweenUtils.TweenPosition(go.transform, targetPos.x, targetPos.y, targetPos.z, duration)
        CommonDefs.OnComplete_Tweener(self.m_HaiLangTweener , DelegateFactory.TweenCallback(function()
            if self.m_HaiLangFx ~= nil then
                self.m_HaiLangFx:Destroy()
                self.m_HaiLangFx = nil
            end
        end))
    end))
end
LuaShuiManJinShanMgr.m_Stage = 0
LuaShuiManJinShanMgr.m_Progress = 0
LuaShuiManJinShanMgr.m_MaxProgress = 0
LuaShuiManJinShanMgr.m_SubProgress = 0
LuaShuiManJinShanMgr.m_MaxSubProgress = 0
LuaShuiManJinShanMgr.m_Pause = false
function LuaShuiManJinShanMgr:ShuiManJinShan_SyncProgress(id, progress, maxProgress, subProgress, maxSubProgress, pause)
    self.m_Stage = id
    self.m_Progress = progress
    self.m_MaxProgress = maxProgress
    self.m_SubProgress = subProgress
    self.m_MaxSubProgress = maxSubProgress
    self.m_Pause = pause
    g_ScriptEvent:BroadcastInLua("ShuiManJinShan_SyncProgress", id, progress, maxProgress, subProgress, maxSubProgress, pause)
end

function LuaShuiManJinShanMgr:ClearProgress()
    LuaShuiManJinShanMgr.m_Stage = 0
    LuaShuiManJinShanMgr.m_Progress = 0
    LuaShuiManJinShanMgr.m_MaxProgress = 0
    LuaShuiManJinShanMgr.m_SubProgress = 0
    LuaShuiManJinShanMgr.m_MaxSubProgress = 0
    LuaShuiManJinShanMgr.m_Pause = false
end

function LuaShuiManJinShanMgr:IsInGameplay()
    if not CClientMainPlayer.Inst then return false end
	return self:GetForceByGameplayId(CClientMainPlayer.Inst.PlayProp.PlayId)
end

function LuaShuiManJinShanMgr:GetForceByGameplayId(gamePlayId)
    if not self.m_GameplayId2Force then
        self.m_GameplayId2Force = {}
        XinBaiLianDong_ShuiManJinShan.ForeachKey(function(key)
            local data = XinBaiLianDong_ShuiManJinShan.GetData(key)
            self.m_GameplayId2Force[data.GameplayId] = key
        end)
    end

    return self.m_GameplayId2Force[gamePlayId]
end

LuaShuiManJinShanMgr.m_RaindropTweener = nil
function LuaShuiManJinShanMgr:PlayRainyEffect(delayDuration, duration, strength)
    if CPostProcessingMgr.s_NewPostProcessingOpen then
        local postEffectCtrl = CPostProcessingMgr.Instance.MainController
        postEffectCtrl:SetRainDropEffect(1, true)

        if self.m_RaindropTweener then
            LuaTweenUtils.Kill(self.m_RaindropTweener, false)
            self.m_RaindropTweener = nil
        end

        self.m_RaindropTweener = LuaTweenUtils.TweenFloat(1, 0, duration, function(val)
            postEffectCtrl:SetRainDropEffect(val * strength, true)
        end)

        LuaTweenUtils.SetDelay(self.m_RaindropTweener, delayDuration)

        CommonDefs.OnComplete_Tweener(self.m_RaindropTweener , DelegateFactory.TweenCallback(function()
            postEffectCtrl:SetRainDropEffect(1, false)
        end))
    else
        if CPostEffectMgr.Instance and CPostEffectMgr.enableRainDropEffect then
            CPostEffectMgr.Instance:SetRainDropEffectExtra(1, true)
            if self.m_RaindropTweener then
                LuaTweenUtils.Kill(self.m_RaindropTweener, false)
                self.m_RaindropTweener = nil
            end
            CRainDropEffect.rate = 1 * strength
            self.m_RaindropTweener = LuaTweenUtils.TweenFloat(1, 0, duration, function(val)
                CRainDropEffect.rate = val * strength
            end)
            LuaTweenUtils.SetDelay(self.m_RaindropTweener, delayDuration)
    
            CommonDefs.OnComplete_Tweener(self.m_RaindropTweener , DelegateFactory.TweenCallback(function()
                CRainDropEffect.rate = 1
                CPostEffectMgr.Instance:SetRainDropEffectExtra(1, false)
            end))
        end
    end
end

function LuaShuiManJinShanMgr:SetWaterHeight(factor)
    if CRenderScene.Inst == nil then
        return 
    end
    local water = CommonDefs.GetComponentInChildren_GameObject_Type(CRenderScene.Inst.gameObject, typeof(CWater))
    if water then
        if self.m_WaterTweener then
            LuaTweenUtils.Kill(self.m_WaterTweener, false)
        end
        local arrange = water:GetComponent(typeof(CWaterMeshReArrange))
        local maxY = 42.71 + XinBaiLianDong_Setting.GetData().ShuiManJinShan_WaterHeight[1]
        local minY = 42.71 + XinBaiLianDong_Setting.GetData().ShuiManJinShan_WaterHeight[0]
        local duration = 1
        local curWaterHeight = water.transform.localPosition.y
        local targetWaterHeight = math.lerp(minY, maxY, factor)
        local waterGo = water.gameObject
        self.m_WaterTweener = LuaTweenUtils.TweenFloat(curWaterHeight, targetWaterHeight, duration, function ( val )
            if not CommonDefs.IsUnityObjectNull(waterGo) then
                LuaUtils.SetLocalPositionY(water.transform, val)
                if arrange then
                    arrange:Init()
                end
            end
        end)
    end
end

function LuaShuiManJinShanMgr:Clear()
    self.m_IsConfirmState = false
    self.m_SoloInfo = nil
    self.m_GroupInfo = nil
    self.m_RewardItems = nil
    self.m_AllNPC = {}
    if self.m_RaindropTweener then
        LuaTweenUtils.Kill(self.m_RaindropTweener, false)
        self.m_RaindropTweener = nil

        if CPostProcessingMgr.s_NewPostProcessingOpen then
            local postEffectCtrl = CPostProcessingMgr.Instance.MainController
            postEffectCtrl:SetRainDropEffect(1, false)
        else
            CRainDropEffect.rate = 1
            CPostEffectMgr.Instance:SetRainDropEffectExtra(1, false)
        end
    end
end

function LuaShuiManJinShanMgr:ShowTopRightWnd(wnd)
    if self:IsInGameplay() then 
        wnd.contentNode:SetActive(false)
        CUIManager.ShowUI(CLuaUIResources.ShuiManJinShanStateWnd)
    else
        CUIManager.CloseUI(CLuaUIResources.ShuiManJinShanStateWnd)
    end
end

function LuaShuiManJinShanMgr:ShuiManJinShan_SyncNpc(npcTbl)
    self.m_AllNPC = {}
    for i = 1, #npcTbl do 
        local engineId = npcTbl[i]
        local npc = CClientObjectMgr.Inst:GetObject(npcTbl[i])
        if npc and npc.RO then 
            local npcTransform = npc.RO:GetSlotTransform("TopAnchor")
            if npcTransform then
                table.insert(self.m_AllNPC, {engineId = engineId, npc = npc, transform = npcTransform, npcId = npc.TemplateId})
            end
        end
    end
end

function LuaShuiManJinShanMgr:ShuiManJinShan_CheckPlayOpenMatchResult(bIsOpen, context)
    if not bIsOpen then
        g_MessageMgr:ShowMessage("GAMEPLAY_NOT_OPEN")
        return
    end

    local taskId = tonumber(context)
    local info = {activityId = 42010125,  taskId = taskId}
    CLuaScheduleMgr.RealTrackSchedule(info)
end