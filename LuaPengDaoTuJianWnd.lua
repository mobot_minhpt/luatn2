local QnIncreaseAndDecreaseButton = import "L10.UI.QnIncreaseAndDecreaseButton"
local UIGrid = import "UIGrid"
local UISlider = import "UISlider"
local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local QnNumberInput = import "L10.UI.QnNumberInput"
local GameObject = import "UnityEngine.GameObject"
local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local QnCheckBox = import "L10.UI.QnCheckBox"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local EnumQualityType = import "L10.Game.EnumQualityType"
local QualityColor = import "L10.Game.QualityColor"
local UIProgressBar = import "UIProgressBar"
local Vector4 = import "UnityEngine.Vector4"
local PathMode = import "DG.Tweening.PathMode"
local PathType = import "DG.Tweening.PathType"
local Ease = import "DG.Tweening.Ease"
local UITableTween = import "L10.UI.UITableTween"

LuaPengDaoTuJianWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaPengDaoTuJianWnd, "AscendingQnCheckBox", "AscendingQnCheckBox", QnCheckBox)
RegistChildComponent(LuaPengDaoTuJianWnd, "DescendingQnCheckBox", "DescendingQnCheckBox", QnCheckBox)
RegistChildComponent(LuaPengDaoTuJianWnd, "RewardSetTopSetting", "RewardSetTopSetting", QnSelectableButton)
RegistChildComponent(LuaPengDaoTuJianWnd, "HideUnLockSetting", "HideUnLockSetting", QnSelectableButton)
RegistChildComponent(LuaPengDaoTuJianWnd, "TableView", "TableView", QnTableView)
RegistChildComponent(LuaPengDaoTuJianWnd, "QnNumberInput", "QnNumberInput", QnNumberInput)
RegistChildComponent(LuaPengDaoTuJianWnd, "MonsterIconTexture", "MonsterIconTexture", CUITexture)
RegistChildComponent(LuaPengDaoTuJianWnd, "MonsterNameLabel", "MonsterNameLabel", UILabel)
RegistChildComponent(LuaPengDaoTuJianWnd, "MonsterStageLabel", "MonsterStageLabel", UILabel)
RegistChildComponent(LuaPengDaoTuJianWnd, "AttSlider", "AttSlider", UISlider)
RegistChildComponent(LuaPengDaoTuJianWnd, "DefSlider", "DefSlider", UISlider)
RegistChildComponent(LuaPengDaoTuJianWnd, "HpSlider", "HpSlider", UISlider)
RegistChildComponent(LuaPengDaoTuJianWnd, "MonsterIntroductionLabel", "MonsterIntroductionLabel", UILabel)
RegistChildComponent(LuaPengDaoTuJianWnd, "LockMonster", "LockMonster", GameObject)
RegistChildComponent(LuaPengDaoTuJianWnd, "UnlockConditionLabel", "UnlockConditionLabel", UILabel)
RegistChildComponent(LuaPengDaoTuJianWnd, "Title1", "Title1", UILabel)
RegistChildComponent(LuaPengDaoTuJianWnd, "Title2", "Title2", UILabel)
RegistChildComponent(LuaPengDaoTuJianWnd, "Title3", "Title3", UILabel)
RegistChildComponent(LuaPengDaoTuJianWnd, "Title4", "Title4", UILabel)
RegistChildComponent(LuaPengDaoTuJianWnd, "RewardButton", "RewardButton", GameObject)
RegistChildComponent(LuaPengDaoTuJianWnd, "KillNumLabel", "KillNumLabel", UILabel)
RegistChildComponent(LuaPengDaoTuJianWnd, "RewardTemplate", "RewardTemplate", GameObject)
RegistChildComponent(LuaPengDaoTuJianWnd, "RewardGrid", "RewardGrid", UIGrid)
RegistChildComponent(LuaPengDaoTuJianWnd, "QnIncreseAndDecreaseButton", "QnIncreseAndDecreaseButton", QnIncreaseAndDecreaseButton)
RegistChildComponent(LuaPengDaoTuJianWnd, "RightView", "RightView", GameObject)
RegistChildComponent(LuaPengDaoTuJianWnd, "TipButton", "TipButton", GameObject)
--@endregion RegistChildComponent end
RegistClassMember(LuaPengDaoTuJianWnd,"m_List")
RegistClassMember(LuaPengDaoTuJianWnd,"m_IsAscending")
RegistClassMember(LuaPengDaoTuJianWnd,"m_IsRewardSetTop")
RegistClassMember(LuaPengDaoTuJianWnd,"m_IsHideLocked")
RegistClassMember(LuaPengDaoTuJianWnd,"m_SinglePageItemCount")
RegistClassMember(LuaPengDaoTuJianWnd,"m_Page")
RegistClassMember(LuaPengDaoTuJianWnd,"m_MaxPage")
RegistClassMember(LuaPengDaoTuJianWnd,"m_SelectIndex")
RegistClassMember(LuaPengDaoTuJianWnd,"m_RewardTemplateList")

function LuaPengDaoTuJianWnd:Awake()
	self.RewardSetTopSetting:SetSelected(true, true)
	self.HideUnLockSetting:SetSelected(false, true)
    --@region EventBind: Dont Modify Manually!

	self.AscendingQnCheckBox.OnValueChanged = DelegateFactory.Action_bool(function (selected)
	    self:OnAscendingQnCheckBoxValueChanged(selected)
	end)

	self.DescendingQnCheckBox.OnValueChanged = DelegateFactory.Action_bool(function (selected)
	    self:OnDescendingQnCheckBoxValueChanged(selected)
	end)

	self.RewardSetTopSetting.OnButtonSelected = DelegateFactory.Action_bool(function (selected)
	    self:OnRewardSetTopSettingSelected(selected)
	end)

	self.HideUnLockSetting.OnButtonSelected = DelegateFactory.Action_bool(function (selected)
	    self:OnHideUnLockSettingSelected(selected)
	end)

	UIEventListener.Get(self.RewardButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRewardButtonClick()
	end)

	UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)
    --@endregion EventBind end
end

function LuaPengDaoTuJianWnd:Init()
	self.m_SinglePageItemCount = 15
    self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
			return self:NumOfRows()
        end,
        function(item, index)
            self:InitItem(item, index)
        end
    )
	self.TableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
		self:OnSelectAtRow(row)
    end)
    self.QnNumberInput.OnValueChanged = DelegateFactory.Action_string(function(v)
        self:OnQnNumberInputValueChanged(v)
    end)
	self.QnNumberInput.OnMaxValueButtonClick = DelegateFactory.Action(function () 
		self:OnQnNumberInputMaxValueButtonClick()
	end)
	self.QnNumberInput.OnKeyboardClosed = DelegateFactory.Action(function () 
		self:OnQnNumberInputKeyboardClosed()
	end)
	self.QnIncreseAndDecreaseButton.onValueChanged = DelegateFactory.Action_int(function(v)
		self:OnPageChanged(v)
	end)
	self.RewardTemplate.gameObject:SetActive(false)
	self.m_IsAscending = false
	self.AscendingQnCheckBox:SetSelected(false, true)
	self.DescendingQnCheckBox:SetSelected(true, true)
	self.m_IsRewardSetTop = true
	self.m_IsHideLocked = false
	self.m_List = {}
	PengDaoFuYao_MonsterList.ForeachKey(function (k)
		table.insert(self.m_List,{designData = PengDaoFuYao_MonsterList.GetData(k)})
	end)
	self.m_Page = 1
	self.m_MaxPage = math.ceil((#self.m_List)/self.m_SinglePageItemCount)
	self.QnIncreseAndDecreaseButton:InitValueList(self.m_Page, self.m_MaxPage,nil)
	self.QnIncreseAndDecreaseButton:SetValue(self.m_Page)
	self:OnPageChanged(self.m_Page)
	self:SortList()
end

function LuaPengDaoTuJianWnd:NumOfRows()
	if self.m_List then
		local leftNum = #self.m_List - (self.m_SinglePageItemCount * (self.m_Page - 1))
		return leftNum < self.m_SinglePageItemCount and leftNum or self.m_SinglePageItemCount
	end
	return 0
end

function LuaPengDaoTuJianWnd:InitItem(item, index)
	item.name = index
    local iconTexture = item.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local stageLabel = item.transform:Find("StageLabel"):GetComponent(typeof(UILabel))
    local redDot = item.transform:Find("RedDot")
    local lockObj = item.transform:Find("Lock")
	
	local trueIndex = self.m_SinglePageItemCount * (self.m_Page - 1) + index + 1
	local data = self.m_List[trueIndex]
	local designData = data.designData
	local monsterId = designData.TempId
	local monsterData = Monster_Monster.GetData(monsterId)
	iconTexture:LoadNPCPortrait(monsterData.HeadIcon, false)
	nameLabel.text = monsterData.Name
	self:InitStageLabel(stageLabel, designData)
	redDot.gameObject:SetActive(LuaPengDaoMgr:GetMonsterRewarded(designData.Id))
	lockObj.gameObject:SetActive(LuaPengDaoMgr:IsMonsterLocked(designData.Id))
end

function LuaPengDaoTuJianWnd:InitStageLabel(label, designData)
	local stage = designData.Class
	local text = stage <= 4 and SafeStringFormat3(LocalString.GetString("%d阶"),stage) or (stage == 5 and LocalString.GetString("精英") or "BOSS")
	local colorArr = {"dbdbdb","dbd5a3","ffa26a","6aa6ff","ff6a6a","da6aff"}
	local color = NGUIText.ParseColor24(colorArr[stage], 0)
	label.text = text
	label.color = color
end

function LuaPengDaoTuJianWnd:OnSelectAtRow(findex)
	local item = self.TableView:GetItemAtRow(findex)
	local index = tonumber(item.name)
	self.RightView.gameObject:SetActive(true)
	local trueIndex = self.m_SinglePageItemCount * (self.m_Page - 1) + index + 1
	self.m_SelectIndex = trueIndex
	local data = self.m_List[trueIndex]
	local designData = data.designData
	local monsterId = designData.TempId
	local monsterData = Monster_Monster.GetData(monsterId)
	self.MonsterIconTexture:LoadNPCPortrait(monsterData.HeadIcon, false)
	self.MonsterNameLabel.text = monsterData.Name
	self:InitStageLabel(self.MonsterStageLabel, designData)
	self.AttSlider.value = designData.AttClass / 5
	self.DefSlider.value = designData.DefClass / 5
	self.HpSlider.value = designData.HpClass / 5
	self.MonsterIntroductionLabel.text = designData.MonsterIntroduction
	self.UnlockConditionLabel.text = designData.UnlockCondition
	local killNum = LuaPengDaoMgr:GetMonsterKillNum(designData.Id)
	self.KillNumLabel.text = killNum > 50 and LocalString.GetString("≥50") or killNum 
	local locked = LuaPengDaoMgr:IsMonsterLocked(designData.Id)
	self.AttSlider.gameObject:SetActive(not locked)
	self.DefSlider.gameObject:SetActive(not locked)
	self.HpSlider.gameObject:SetActive(not locked)
	self.MonsterIntroductionLabel.gameObject:SetActive(not locked)
	self.RewardButton.gameObject:SetActive(not locked and LuaPengDaoMgr:GetMonsterRewarded(designData.Id))
	self.RightView.transform:Find("Kill").gameObject:SetActive(not locked)
	self.LockMonster.gameObject:SetActive(locked)
	self.m_RewardTemplateList = {}
	Extensions.RemoveAllChildren(self.RewardGrid.transform)
	for id = 1, 3 do
		self:InitRewardTemplate(id, designData.Class, designData.Id)
	end
	self.RewardGrid:Reposition()

	local titleMessage = designData.TitleMessage
	local titleArr = {self.Title1,self.Title2,self.Title3,self.Title4}
	for i, title in pairs(titleArr) do
		local showTitle = titleMessage and titleMessage.Length >= i
		title.gameObject:SetActive(showTitle and not locked)
		if showTitle then 
			title.text = titleMessage[i - 1][0]
			UIEventListener.Get(title.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
				g_MessageMgr:ShowMessage(titleMessage[i - 1][1])
			end)
		end
	end
end

function LuaPengDaoTuJianWnd:InitRewardTemplate(index, stage, id)
	local monsterListAwardData = PengDaoFuYao_MonsterListAward.GetData(index)
	local itemIdArr = {
		monsterListAwardData.Class_1,monsterListAwardData.Class_2,monsterListAwardData.Class_3,
		monsterListAwardData.Class_4,monsterListAwardData.Class_5,monsterListAwardData.Class_6
	}
	local itemId = itemIdArr[stage]
	local itemData = Item_Item.GetData(itemId)
	local obj = NGUITools.AddChild(self.RewardGrid.gameObject, self.RewardTemplate.gameObject)
	obj.gameObject:SetActive(true)
	local texture = obj.transform:Find("Texture"):GetComponent(typeof(CUITexture))
	local fx = obj.transform:Find("Fx"):GetComponent(typeof(CUIFx))
	local getObj = obj.transform:Find("Get")
	local lockObj = obj.transform:Find("Lock")
	local numLabel = obj.transform:Find("NumLabel"):GetComponent(typeof(UILabel))
	local lockProgress = obj.transform:Find("LockProgress"):GetComponent(typeof(UIProgressBar))
	texture:LoadMaterial(itemData.Icon)
	fx:LoadFx("fx/ui/prefab/UI_kuang_yellow02.prefab")
	local b = NGUIMath.CalculateRelativeWidgetBounds(texture.transform)
	local waypoints = CUICommonDef.GetWayPoints(Vector4(b.min.x, b.max.y, b.size.x, b.size.y), 1, true)
	fx.transform.localPosition = waypoints[0]
	LuaTweenUtils.DOLuaLocalPath(fx.transform, waypoints, 2, PathType.Linear, PathMode.Ignore, 2, - 1, Ease.Linear)
	numLabel.text = monsterListAwardData.Number
	getObj.gameObject:SetActive(LuaPengDaoMgr:GetMonsterRewarded(id,index))
	local killNum = LuaPengDaoMgr:GetMonsterKillNum(id)
	local canRewarded = (killNum >= monsterListAwardData.Number) and not LuaPengDaoMgr:GetMonsterRewarded(id, index)
	fx.gameObject:SetActive(canRewarded)
	lockObj.gameObject:SetActive((killNum / monsterListAwardData.Number) < 1)
	lockProgress.value = 1 - killNum / monsterListAwardData.Number
	if canRewarded then
		UIEventListener.Get(texture.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
			Gac2Gas.PDFY_RequestMonsterReward(id,index)
		end)
	end
	self.m_RewardTemplateList[index] = obj
end


function LuaPengDaoTuJianWnd:SortList()
    table.sort(self.m_List,function(a,b) 
		if self.m_IsRewardSetTop then
			local locked1 = LuaPengDaoMgr:IsMonsterLocked(a.designData.Id)
			local locked2 = LuaPengDaoMgr:IsMonsterLocked(b.designData.Id)
			if locked1 ~= locked2 then
				return locked2
			end
			local canRewarded1 = LuaPengDaoMgr:GetMonsterRewarded(a.designData.Id)
			local canRewarded2 = LuaPengDaoMgr:GetMonsterRewarded(b.designData.Id)
			if canRewarded1 ~= canRewarded2 then
				return canRewarded1
			end
		end
		if a.designData.Class == b.designData.Class then
			return a.designData.Id < b.designData.Id
		else
			if self.m_IsAscending then
				return a.designData.Class < b.designData.Class
			else
				return a.designData.Class > b.designData.Class
			end
		end
    end)
	self:ReInitList()
end

function LuaPengDaoTuJianWnd:ReInitList()
	self.TableView:ReloadData(true, false)
	self.TableView.m_Grid.transform:GetComponent(typeof(UITableTween)):Reset()
	self.TableView:ScrollToRow(0)
	self.TableView.m_Grid:Reposition()
	-- for i = 0, self.TableView.m_Grid.transform.childCount - 1 do
	-- 	local item = self.TableView.m_Grid.transform:GetChild(i)
	-- 	self:InitItem(item, i)
	-- end
	self.TableView:SetSelectRow(0,true)
end

function LuaPengDaoTuJianWnd:OnEnable()
	g_ScriptEvent:AddListener("PDFY_GetMonsterRewardSuccess", self, "OnGetMonsterRewardSuccess")
	g_ScriptEvent:AddListener("PDFY_RequestMonsterAllRewardResult", self, "OnRequestMonsterAllRewardResult")
end

function LuaPengDaoTuJianWnd:OnDisable()
	g_ScriptEvent:RemoveListener("PDFY_GetMonsterRewardSuccess", self, "OnGetMonsterRewardSuccess")
	g_ScriptEvent:RemoveListener("PDFY_RequestMonsterAllRewardResult", self, "OnRequestMonsterAllRewardResult")
end

function LuaPengDaoTuJianWnd:OnRequestMonsterAllRewardResult()
	self:ReInitList()
end

function LuaPengDaoTuJianWnd:OnGetMonsterRewardSuccess(monsterId, rewardId)
	local data = self.m_List[self.m_SelectIndex]
	local designData = data.designData
	if monsterId == designData.Id then
		if self.m_RewardTemplateList then
			local obj = self.m_RewardTemplateList[rewardId]
			if obj then
				local fx = obj.transform:Find("Fx"):GetComponent(typeof(CUIFx))
				fx.gameObject:SetActive(false)
				local texture = obj.transform:Find("Texture"):GetComponent(typeof(CUITexture))
				UIEventListener.Get(texture.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
				end)
				local getObj = obj.transform:Find("Get")
				getObj.gameObject:SetActive(true)
			end
		end
		self.RewardButton.gameObject:SetActive(LuaPengDaoMgr:GetMonsterRewarded(designData.Id))
	end
	local startIndex = self.m_SinglePageItemCount * (self.m_Page - 1) + 1
	local endIndex = self:NumOfRows()
	for i = 1, endIndex do
		local trueIndex = startIndex + i - 1
		local data = self.m_List[trueIndex]
		local id = data.designData.Id
		if id == monsterId then
			local item = self.TableView:GetItemAtRow(i - 1)
			self:InitItem(item, i - 1)
		end
	end
end
--@region UIEvent

function LuaPengDaoTuJianWnd:OnAscendingQnCheckBoxValueChanged(selected)
    self.m_IsAscending = selected
    self.DescendingQnCheckBox:SetSelected(not self.m_IsAscending, true)
    self:SortList()
end

function LuaPengDaoTuJianWnd:OnDescendingQnCheckBoxValueChanged(selected)
    self.m_IsAscending = not selected
    self.AscendingQnCheckBox:SetSelected(self.m_IsAscending, true)
    self:SortList()
end

function LuaPengDaoTuJianWnd:OnRewardSetTopSettingSelected(selected)
	self.m_IsRewardSetTop = selected
	self:SortList()
end

function LuaPengDaoTuJianWnd:OnHideUnLockSettingSelected(selected)
	self.m_IsHideLocked = selected
	self.m_List = {}
	PengDaoFuYao_MonsterList.ForeachKey(function (k)
		if self.m_IsHideLocked then
			local locked = LuaPengDaoMgr:IsMonsterLocked(k)
			if not locked then
				table.insert(self.m_List,{designData = PengDaoFuYao_MonsterList.GetData(k)})
			end
		else
			table.insert(self.m_List,{designData = PengDaoFuYao_MonsterList.GetData(k)})
		end
	end)
	self.RightView.gameObject:SetActive(false)
	self.m_Page = 1
	self.m_MaxPage = math.ceil((#self.m_List)/self.m_SinglePageItemCount)
	self.QnIncreseAndDecreaseButton:InitValueList(self.m_Page, self.m_MaxPage,nil)
	self.QnIncreseAndDecreaseButton:SetValue(self.m_Page)
	self:OnPageChanged(self.m_Page)
	self:SortList()
end

function LuaPengDaoTuJianWnd:OnRewardButtonClick()
	Gac2Gas.PDFY_RequestMonsterAllReward()
end

function LuaPengDaoTuJianWnd:OnPageChanged(page)
	self.m_Page = math.max(1,math.min(tonumber(page) ,self.m_MaxPage))
	local str = self.m_MaxPage == 0 and 0 or SafeStringFormat3("%d/%d", self.m_Page,self.m_MaxPage)
	self.QnIncreseAndDecreaseButton:SetValue(self.m_Page)
	self.QnNumberInput:ForceSetText(str)
	self:ReInitList()
end

function LuaPengDaoTuJianWnd:OnQnNumberInputValueChanged(v)
end

function LuaPengDaoTuJianWnd:OnQnNumberInputMaxValueButtonClick()
	self:OnPageChanged(self.m_MaxPage)
end

function LuaPengDaoTuJianWnd:OnQnNumberInputKeyboardClosed()
	local default,value = System.Int32.TryParse(self.QnNumberInput.Text)
	if default then
		local newVal = math.min(value, self.m_MaxPage)
		self:OnPageChanged( newVal)
	end
end

function LuaPengDaoTuJianWnd:OnTipButtonClick()
	g_MessageMgr:ShowMessage("PengDaoTuJianWnd_ReadMe")
end
--@endregion UIEvent

