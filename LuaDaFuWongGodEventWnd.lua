local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"

LuaDaFuWongGodEventWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaDaFuWongGodEventWnd, "Title", "Title", UILabel)
RegistChildComponent(LuaDaFuWongGodEventWnd, "Desc", "Desc", UILabel)
RegistChildComponent(LuaDaFuWongGodEventWnd, "CountDownLabel", "CountDownLabel", UILabel)
RegistChildComponent(LuaDaFuWongGodEventWnd, "BuffIcon", "BuffIcon", CUITexture)
RegistChildComponent(LuaDaFuWongGodEventWnd, "Portrait", "Portrait", CUITexture)
RegistChildComponent(LuaDaFuWongGodEventWnd, "BG", "BG", CUITexture)

--@endregion RegistChildComponent end
RegistClassMember(LuaDaFuWongGodEventWnd,"m_HeadIconList")
RegistClassMember(LuaDaFuWongGodEventWnd,"m_data")
function LuaDaFuWongGodEventWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.m_HeadIconList = {"dafuwongplaylandeopwnd_huang","dafuwongplaylandeopwnd_hong","dafuwongplaylandeopwnd_zi","dafuwongplaylandeopwnd_lv"}
    self.m_data = nil
end

function LuaDaFuWongGodEventWnd:Init()
    if not LuaDaFuWongMgr.CurGodId then return end
    local data = DaFuWeng_God.GetData(LuaDaFuWongMgr.CurGodId)
    if not data then return end
    self.m_data = data 
    self:InitRight()
    self:InitLeft()
    local totalTime = DaFuWeng_Countdown.GetData(EnumDaFuWengStage.RoundGod).Time 
    local CountDown = totalTime - LuaDaFuWongMgr.GetCardTime
    local endFunc = function() self:CloseWnd() end
	local durationFunc = function(time) 
        self.CountDownLabel.text = SafeStringFormat3(LocalString.GetString("%d秒后关闭"),time)
    end
	LuaDaFuWongMgr:StartCountDownTick(EnumDaFuWengCountDownStage.RandomEvent,CountDown,durationFunc,endFunc)
end

function LuaDaFuWongGodEventWnd:InitRight()
    for i = 0,self.BuffIcon.transform.childCount - 1 do
        self.BuffIcon.transform:GetChild(i).gameObject:SetActive(i + 1 == LuaDaFuWongMgr.CurGodId)
    end
    --self.BuffIcon:LoadMaterial(self.m_data.IconPath)
    local curPlayer = LuaDaFuWongMgr.CurPlayerRound
    local PlayerData = LuaDaFuWongMgr.PlayerInfo and LuaDaFuWongMgr.PlayerInfo[curPlayer]
    if not PlayerData then 
        self.Portrait.gameObject:SetActive(false)
        self.BG.gameObject:SetActive(false)
    else
        self.Portrait.gameObject:SetActive(true)
        self.BG.gameObject:SetActive(true)
        self.Portrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(PlayerData.class, PlayerData.gender, -1), false)
        self.BG:LoadMaterial(LuaDaFuWongMgr:GetHeadBgPath(PlayerData.round))
    end
end
-- function LuaDaFuWongGodEventWnd:GetTexturePath(index)
--     local name = self.m_HeadIconList[index]
--     return SafeStringFormat3("UI/Texture/FestivalActivity/Festival_DaFuWong/Material/%s.mat",name)
-- end
function LuaDaFuWongGodEventWnd:InitLeft()
    self.Title.text = self.m_data.Name
    self.Title.color = NGUIText.ParseColor(self.m_data.ColorCode, 0)
    self.Desc.text = SafeStringFormat3("[6f452b]%s[-]",g_MessageMgr:Format(self.m_data.Describe)) 
end
function LuaDaFuWongGodEventWnd:OnStageUpdate(CurStage)
    if CurStage ~= EnumDaFuWengStage.RoundGod then
        self:CloseWnd()
    end
end

function LuaDaFuWongGodEventWnd:CloseWnd()
    g_ScriptEvent:BroadcastInLua("OnDaFuWengFinishCurStage",EnumDaFuWengStage.RoundGod)
    CUIManager.CloseUI(CLuaUIResources.DaFuWongGodEventWnd)

end
--@region UIEvent

--@endregion UIEvent
function LuaDaFuWongGodEventWnd:OnEnable()
    g_ScriptEvent:AddListener("OnDaFuWengStageUpdate",self,"OnStageUpdate")
end

function LuaDaFuWongGodEventWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnDaFuWengStageUpdate",self,"OnStageUpdate")
    LuaDaFuWongMgr:EndCountDownTick(EnumDaFuWengCountDownStage.RandomEvent)
end
