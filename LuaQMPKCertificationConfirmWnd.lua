local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local CButton = import "L10.UI.CButton"

LuaQMPKCertificationConfirmWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaQMPKCertificationConfirmWnd, "NameLab", "NameLab", UILabel)
RegistChildComponent(LuaQMPKCertificationConfirmWnd, "IdLab", "IdLab", UILabel)
RegistChildComponent(LuaQMPKCertificationConfirmWnd, "CancelButton", "CancelButton", CButton)
RegistChildComponent(LuaQMPKCertificationConfirmWnd, "ConfirmButton", "ConfirmButton", CButton)

--@endregion RegistChildComponent end
LuaQMPKCertificationConfirmWnd.s_Name = nil
LuaQMPKCertificationConfirmWnd.s_Id = nil

function LuaQMPKCertificationConfirmWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    UIEventListener.Get(self.CancelButton.gameObject).onClick=DelegateFactory.VoidDelegate(function(go)
        CUIManager.CloseUI(CLuaUIResources.QMPKCertificationConfirmWnd)
    end)

    UIEventListener.Get(self.ConfirmButton.gameObject).onClick=DelegateFactory.VoidDelegate(function(go)
        CUIManager.CloseUI(CLuaUIResources.QMPKCertificationConfirmWnd)
        Gac2Gas.SubmitQmpkCertificationInfo(LuaQMPKCertificationConfirmWnd.s_Name, LuaQMPKCertificationConfirmWnd.s_Id)
    end)
end

function LuaQMPKCertificationConfirmWnd:Init()
    self.NameLab.text = LuaQMPKCertificationConfirmWnd.s_Name
    self.IdLab.text = LuaQMPKCertificationConfirmWnd.s_Id
end

--@region UIEvent

--@endregion UIEvent

