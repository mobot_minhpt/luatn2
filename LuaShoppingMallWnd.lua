require("common/common_include")

local CommonDefs = import "L10.Game.CommonDefs"
local GameObject = import "UnityEngine.GameObject"
local QnButton = import "L10.UI.QnButton"
local QnTabView = import "L10.UI.QnTabView"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local CChargeWnd = import "L10.UI.CChargeWnd"
local CLinyuShopWnd = import "L10.UI.CLinyuShopWnd"
local Object = import "System.Object"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

CLuaShoppingMallWnd = class()
RegistChildComponent(CLuaShoppingMallWnd, "VipGiftTip", GameObject)
RegistChildComponent(CLuaShoppingMallWnd, "Tab", QnTabView)
RegistChildComponent(CLuaShoppingMallWnd, "ZhouBianButton", QnButton)
RegistChildComponent(CLuaShoppingMallWnd, "ZhouBianButtonChild", QnButton)
RegistChildComponent(CLuaShoppingMallWnd, "FashionLotteryButton", QnButton)
RegistChildComponent(CLuaShoppingMallWnd, "FashionLotteryButtonChild", QnButton)
RegistChildComponent(CLuaShoppingMallWnd, "Wnd_Bg", UITexture)
RegistChildComponent(CLuaShoppingMallWnd, "ChargeWndBg", GameObject)
RegistChildComponent(CLuaShoppingMallWnd, "LinyuShopWnd", CLinyuShopWnd)
RegistChildComponent(CLuaShoppingMallWnd, "CloseButton", GameObject)

RegistClassMember(CLuaShoppingMallWnd, "NewChargeWndBg")

function CLuaShoppingMallWnd:Awake()
	self.NewChargeWndBg = self.transform:Find("Wnd_Bg/NewChargeWndBg").gameObject
end

function CLuaShoppingMallWnd:Init()
	CShopMallMgr.Init()
	Gac2Gas.RequestAutoShangJiaItem()
    Gac2Gas.RequestPlayerItemLimit(EShopMallRegion_lua.ELingyuMallLimit)
	Gac2Gas.RequestPlayerItemLimit(EShopMallRegion_lua.EYuanBaoMall)
	
	if CUIManager.IsLoaded(CLuaUIResources.Shuangshiyi2020FashionPreviewWnd) then
        CUIManager.CloseUI(CLuaUIResources.Shuangshiyi2020FashionPreviewWnd)
    end

	if CUIManager.IsLoaded(CUIResources.ScreenCaptureWnd) then
        CUIManager.CloseUI(CUIResources.ScreenCaptureWnd)
    end

	if CommonDefs.IS_KR_CLIENT and CommonDefs.DEVICE_PLATFORM == "pc" then
		local chargeGo = self.Tab:GetTabGameObject(2)
		if chargeGo ~= nil then
            chargeGo:SetActive(false)
		end
		local spriteTrans = self.transform:Find("Wnd_Bg/Line")
        if spriteTrans ~= nil and spriteTrans.childCount == 4 then
			spriteTrans:GetChild(2).gameObject:SetActive(false)
			Extensions.SetLocalPositionY(spriteTrans:GetChild(3), -220)
		end
	end

	self.Tab.OnSelect = CommonDefs.CombineListner_Action_QnTabButton_int(self.Tab.OnSelect, (DelegateFactory.Action_QnTabButton_int(function (button, index)
		self.Wnd_Bg.enabled = (index~=2) and true or false
		self.ChargeWndBg:SetActive(index==2 and not LuaWelfareMgr.IsOpenBigMonthCard)
		self.NewChargeWndBg:SetActive(index==2 and LuaWelfareMgr.IsOpenBigMonthCard)
		self:InitZhouBianButton(index)
		self:InitFashionLotteryButton(index)
		self:RecordChargeWndClientLog(index)
	end)), true)

	self.Tab:ChangeTo(CShopMallMgr.SelectMallIndex)
	self:InitZhouBianButton(CShopMallMgr.SelectMallIndex)
	self:InitFashionLotteryButton(CShopMallMgr.SelectMallIndex)

	if LuaWishMgr.WishShop then
		self.transform:Find('Wnd_Bg/Tabs').gameObject:SetActive(false)
		self.VipGiftTip:SetActive(false)
	end

	-- hmt处理一下字体大小
	if CommonDefs.IS_HMT_CLIENT then
		local tabRoot = self.transform:Find("Wnd_Bg/Tabs")
		for i=0,2 do
			local label = tabRoot:GetChild(i):Find("Label"):GetComponent(typeof(UILabel))
			label.fontSize = 40
		end

		local sendLabel = self.transform:Find("Tab/TabWindows/LinyuShopWnd/OnSellRoot/SendArea/SendButton/Text"):GetComponent(typeof(UILabel))
		sendLabel.fontSize = 42
		sendLabel.transform.localPosition = Vector3(0,0,0)
	end
end

function CLuaShoppingMallWnd:RecordChargeWndClientLog(page)
	if page == 2 then
		Gac2Gas.RequestLogMallView("ChargeWnd", LocalString.GetString("充值页"))
	end
end

function CLuaShoppingMallWnd:InitZhouBianButton(page)
	if CLuaShopMallMgr:IsZhouBianMallShopEnabled() and page == 2 then
		self.ZhouBianButton.gameObject:SetActive(true)
		self.ZhouBianButton.OnClick = DelegateFactory.Action_QnButton(function(go)
			self:OnZhouBianMallShopClick(go)
		end)
		self.ZhouBianButtonChild.OnClick = DelegateFactory.Action_QnButton(function(go)
			self:OnZhouBianMallShopClick(go)
		end)
	else
		self.ZhouBianButton.gameObject:SetActive(false)
	end
end

function CLuaShoppingMallWnd:InitFashionLotteryButton(page)
	if LuaFashionLotteryMgr:IsFashionLotteryOpen() and page == 0 then
		self.FashionLotteryButton.gameObject:SetActive(true)
		self.FashionLotteryButton.OnClick = DelegateFactory.Action_QnButton(function(go)
			self:OnFashionLotteryButtonClick(go)
		end)
		self.FashionLotteryButtonChild.OnClick = DelegateFactory.Action_QnButton(function(go)
			self:OnFashionLotteryButtonClick(go)
		end)
	else
		self.FashionLotteryButton.gameObject:SetActive(false)
	end
end

function CLuaShoppingMallWnd:RefreshGiftTip()
	if CommonDefs.IS_KR_CLIENT and CommonDefs.DEVICE_PLATFORM == "pc" then
        self.VipGiftTip:SetActive(false)
	else
		self.VipGiftTip:SetActive(CChargeWnd.HasVipGiftAfterLevel(0))
	end
end

function CLuaShoppingMallWnd:OnEnable()
	self:RefreshGiftTip()
	g_ScriptEvent:AddListener("SendMallMarketItemLimitUpdate", self, "UpdateMarketLimit")
	g_ScriptEvent:AddListener("AutoShangJiaItemUpdate", self, "OnMallAutoShangJiaItemUpdate")
	g_ScriptEvent:AddListener("MarketItemRequestSuccess", self, "BuyRequestSuccess")
	g_ScriptEvent:AddListener("ItemPropUpdated", self, "OnChargePropUpdated")
end

function CLuaShoppingMallWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SendMallMarketItemLimitUpdate", self, "UpdateMarketLimit")
	g_ScriptEvent:RemoveListener("AutoShangJiaItemUpdate", self, "OnMallAutoShangJiaItemUpdate")
	g_ScriptEvent:RemoveListener("MarketItemRequestSuccess", self, "BuyRequestSuccess")
	g_ScriptEvent:RemoveListener("ItemPropUpdated", self, "OnChargePropUpdated")

    CShopMallMgr.ResetIndex()
    LuaWishMgr.WishShop = nil
end

function CLuaShoppingMallWnd:OnDestroy()
    CShopMallMgr.ClearData()
end

function CLuaShoppingMallWnd:UpdateMarketLimit(args)
	local regionId = args[0]
	local limitInfo = args[1]
	if regionId == EShopMallRegion_lua.ELingyuMallLimit then
        CShopMallMgr.UpdateLingYuLimit(limitInfo)
    elseif regionId == EShopMallRegion_lua.EYuanBaoMall then
        CShopMallMgr.UpdateYuanBaoLimit(limitInfo)
    end
end

function CLuaShoppingMallWnd:BuyRequestSuccess(args)
	local param1 = args[0]
	local param2 = args[1]
	local region = math.floor(tonumber(param2 or 0))
    if EShopMallRegion_lua.ELingyuMallLimit == region or EShopMallRegion_lua.EYuanBaoMall == region then
        Gac2Gas.RequestPlayerItemLimit(region)
    end
end

function CLuaShoppingMallWnd:OnChargePropUpdated()
	self:RefreshGiftTip()
	if self.Tab.CurrentSelectTab == 0 then
		Gac2Gas.RequestPlayerItemLimit(EShopMallRegion_lua.ELingyuMallLimit)
	end
end

function CLuaShoppingMallWnd:OnMallAutoShangJiaItemUpdate(args)
	local regionId = args[0]
	local itemInfos = args[1]
	if regionId == EShopMallRegion_lua.ELingyuMallLimit then
        CShopMallMgr.HandleLingyuMallLimitAutoShangJia(itemInfos)
    elseif regionId == EShopMallRegion_lua.EYuanBaoMall then
        CShopMallMgr.HandleYuanBaoMallAutoShangJia(itemInfos)
    elseif regionId == EShopMallRegion_lua.ELingyuMall then
        CShopMallMgr.HandleLingyuMallAutoShangJia(itemInfos)
    end
end

function CLuaShoppingMallWnd:GetGuideGo(methodname)
	local s, e, itemId = string.find(methodname, "LinYu(%d+)")

	if methodname == "GetCloseBtn" then
		return self.CloseButton
	elseif s ~= nil and itemId ~= 0 then
		-- 尝试获取灵玉商城某个物品
		itemId = tonumber(itemId)
		CShopMallMgr.SelectMallIndex = 0
		CShopMallMgr.SelectCategory = CShopMallMgr.GetItemCategoryOfLinyuShop(itemId)
		CShopMallMgr.SearchTemplateId = itemId

		if (CShopMallMgr.SelectCategory == 3) or (CShopMallMgr.SelectCategory == 2) then
			local mall = Mall_LingYuMall.GetData(itemId)
			if mall then
				CShopMallMgr.SelectSubCategory = mall.SubCategory
			else
				return nil
			end
		else
			return nil
		end

		local index = self.LinyuShopWnd.CurrentSelectedItem
		if index>=0 then
			local info = self.LinyuShopWnd.m_CurrentItems[index]
			if info.ItemId == itemId then
				local tableItem = self.LinyuShopWnd.m_Table:GetItemAtRow(index)
				if tableItem ~= nil then

					return tableItem.gameObject
				end
			else
				self.LinyuShopWnd:UpdateView2(false)
				return nil
			end
		end
	end

	return nil
end

function CLuaShoppingMallWnd:OnZhouBianMallShopClick(go)
	CLuaShopMallMgr:OpenZhouBianMallShop()
end

function CLuaShoppingMallWnd:OnFashionLotteryButtonClick(go)
	LuaFashionLotteryMgr:OpenFashionLotteryWnd("LotteryView")
end

return CLuaShoppingMallWnd
