local UISprite = import "UISprite"
local UIGrid = import "UIGrid"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local QnButton = import "L10.UI.QnButton"
local UITable = import "UITable"
local GameObject = import "UnityEngine.GameObject"
local CUITexture = import "L10.UI.CUITexture"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemMgr = import "L10.Game.CItemMgr"
local Animation = import "UnityEngine.Animation"

LuaYaoYeManJuanCompositeWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaYaoYeManJuanCompositeWnd, "TipLab", "TipLab", UILabel)
RegistChildComponent(LuaYaoYeManJuanCompositeWnd, "TipButton", "TipButton", QnButton)
RegistChildComponent(LuaYaoYeManJuanCompositeWnd, "Composite", "Composite", GameObject)
RegistChildComponent(LuaYaoYeManJuanCompositeWnd, "CompositeTipLab", "CompositeTipLab", UILabel)
RegistChildComponent(LuaYaoYeManJuanCompositeWnd, "Icon", "Icon", CUITexture)
RegistChildComponent(LuaYaoYeManJuanCompositeWnd, "DescLab", "DescLab", UILabel)
RegistChildComponent(LuaYaoYeManJuanCompositeWnd, "CompositeBtn", "CompositeBtn", QnButton)
RegistChildComponent(LuaYaoYeManJuanCompositeWnd, "Grid", "Grid", UIGrid)
RegistChildComponent(LuaYaoYeManJuanCompositeWnd, "Template", "Template", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaYaoYeManJuanCompositeWnd, "m_Items")
RegistClassMember(LuaYaoYeManJuanCompositeWnd, "m_Infos")
RegistClassMember(LuaYaoYeManJuanCompositeWnd, "m_InputLabs")
RegistClassMember(LuaYaoYeManJuanCompositeWnd, "m_OwnLabs")
RegistClassMember(LuaYaoYeManJuanCompositeWnd, "m_IncBtns")
RegistClassMember(LuaYaoYeManJuanCompositeWnd, "m_DecBtns")
RegistClassMember(LuaYaoYeManJuanCompositeWnd, "m_CompositeItemId")
RegistClassMember(LuaYaoYeManJuanCompositeWnd, "m_CompositeItemBtn")
RegistClassMember(LuaYaoYeManJuanCompositeWnd, "m_Animation")
RegistClassMember(LuaYaoYeManJuanCompositeWnd, "m_AniNames")
RegistClassMember(LuaYaoYeManJuanCompositeWnd, "m_ItemsIds")
RegistClassMember(LuaYaoYeManJuanCompositeWnd, "m_CompositeItemIds")
RegistClassMember(LuaYaoYeManJuanCompositeWnd, "m_CompositeColorNames")
RegistClassMember(LuaYaoYeManJuanCompositeWnd, "m_CacheType")
RegistClassMember(LuaYaoYeManJuanCompositeWnd, "m_CompositeFormulaId")

function LuaYaoYeManJuanCompositeWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.m_Animation        = self.transform:GetComponent(typeof(Animation))
    self.m_CompositeItemBtn = self.Icon:GetComponent(typeof(QnButton))

    self.TipButton.OnClick = DelegateFactory.Action_QnButton(function (btn)
        g_MessageMgr:ShowMessage("YAOYEMANJUAN_COMPOSITEWND_TIP")
    end)

    self.CompositeBtn.OnClick = DelegateFactory.Action_QnButton(function (btn)
        self:OnCompositeBtnClick(btn)
    end)

    self.m_CompositeItemBtn.OnClick = DelegateFactory.Action_QnButton(function (btn)
        CItemInfoMgr.ShowLinkItemTemplateInfo(self.m_CompositeItemId, false, nil, AlignType.Default, 0, 0, 0, 0)
    end)

    self.TipLab.text = g_MessageMgr:FormatMessage("YAOYEMANJUAN_COMPOSITE_HEAD_TIP")
    self.CompositeTipLab.text = g_MessageMgr:FormatMessage("YAOYEMANJUAN_COMPOSITE_TIP")
    
    self.m_Items = {}
    self.m_InputLabs = {}
    self.m_IncBtns = {}
    self.m_DecBtns = {}
    self.m_OwnLabs = {}
    for i = 1, 3 do
        local item = NGUITools.AddChild(self.Grid.gameObject, self.Template)
        item:SetActive(true)
        local inputLab  = item.transform:Find("IncAndDec/InputLab"):GetComponent(typeof(UILabel))
        local incBtn    = item.transform:Find("IncAndDec/IncreaseButton"):GetComponent(typeof(QnButton))
        local decBtn    = item.transform:Find("IncAndDec/DecreaseButton"):GetComponent(typeof(QnButton))
        local ownLab    = item.transform:Find("OwnLab"):GetComponent(typeof(UILabel))

        table.insert(self.m_Items, item)
        table.insert(self.m_InputLabs, inputLab)
        table.insert(self.m_IncBtns, incBtn)
        table.insert(self.m_DecBtns, decBtn)
        table.insert(self.m_OwnLabs, ownLab)
    end
    self.Template:SetActive(false)
    self.m_CacheType = nil
end

function LuaYaoYeManJuanCompositeWnd:Init()
    self:InitDesginData()
    self.m_Infos = {}
    for i = 1, 3 do
        table.insert(self.m_Infos, {Input = 0, Own = 0, ItemId = self.m_RawMaterialItemIds[i]})
    end
    self:InitItems()
    self:UpdateItemCount()
    self:Refresh()
end

function LuaYaoYeManJuanCompositeWnd:InitDesginData()
    self.m_AniNames = {"yaoyemanjuancompositewnd_lan", "yaoyemanjuancompositewnd_huang", "yaoyemanjuancompositewnd_lv", "yaoyemanjuancompositewnd_zi"}

    local compositeColorNames = g_LuaUtil:StrSplit(YaoYeManJuan_Setting.GetData("CompositeColorNames").Value,",")
    local compositeItemIds = g_LuaUtil:StrSplit(YaoYeManJuan_Setting.GetData("CompositeItemIds").Value,",")
    local rawMaterialItemIds = g_LuaUtil:StrSplit(YaoYeManJuan_Setting.GetData("RawMaterialItemIds").Value,",")

    self.m_CompositeColorNames = {}
    self.m_CompositeItemIds = {}
    self.m_RawMaterialItemIds = {}

    for i = 1, #compositeColorNames do
        table.insert(self.m_CompositeColorNames, compositeColorNames[i])
    end

    for i = 1, #compositeItemIds do
        table.insert(self.m_CompositeItemIds, tonumber(compositeItemIds[i]))
    end

    for i = 1, #rawMaterialItemIds do
        table.insert(self.m_RawMaterialItemIds, tonumber(rawMaterialItemIds[i]))
    end
end
--@region UIEvent
function LuaYaoYeManJuanCompositeWnd:OnCompositeBtnClick(btn)
    if self.m_CompositeFormulaId ~= 0 then
        Gac2Gas.YaoYeManJuan_Synthesize(self.m_CompositeFormulaId)
    end
end
--@endregion UIEvent
function LuaYaoYeManJuanCompositeWnd:OnEnable()
    g_ScriptEvent:AddListener("SendItem", self, "UpdateItemCount")
    g_ScriptEvent:AddListener("SetItemAt", self, "UpdateItemCount")
end

function LuaYaoYeManJuanCompositeWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendItem", self, "UpdateItemCount")
    g_ScriptEvent:RemoveListener("SetItemAt", self, "UpdateItemCount")
end

function LuaYaoYeManJuanCompositeWnd:UpdateItemCount()
    for i = 1, #self.m_Infos do
        self.m_Infos[i].Own = CItemMgr.Inst:GetItemCount(self.m_Infos[i].ItemId)
    end
    self:Refresh()
end

function LuaYaoYeManJuanCompositeWnd:InitItem(item, index, info)
    local btn       = item:GetComponent(typeof(QnButton))
    local icon      = item:GetComponent(typeof(CUITexture))
    local incBtn    = item.transform:Find("IncAndDec/IncreaseButton"):GetComponent(typeof(QnButton))
    local decBtn    = item.transform:Find("IncAndDec/DecreaseButton"):GetComponent(typeof(QnButton))
    local inputLab  = item.transform:Find("IncAndDec/InputLab"):GetComponent(typeof(UILabel))
    local ownLab    = item.transform:Find("OwnLab"):GetComponent(typeof(UILabel))

    inputLab.text = SafeStringFormat3(LocalString.GetString("%d枚"), info.Input)
    ownLab.text = SafeStringFormat3(LocalString.GetString("持有%d枚"), info.Own)

    local data = Item_Item.GetData(info.ItemId)
    icon:LoadMaterial(data.Icon)
    btn.OnClick = DelegateFactory.Action_QnButton(function(btn)
        CItemInfoMgr.ShowLinkItemTemplateInfo(info.ItemId, false, nil, AlignType.Default, 0, 0, 0, 0)
    end)

    incBtn.OnClick = DelegateFactory.Action_QnButton(function(btn)
        self:AddValue(index, 1)
    end)

    decBtn.OnClick = DelegateFactory.Action_QnButton(function(btn)
        self:AddValue(index, -1)
    end)
end

function LuaYaoYeManJuanCompositeWnd:AddValue(index, diffValue)
    self.m_Infos[index].Input = self.m_Infos[index].Input + diffValue
    self:Refresh()
end

function LuaYaoYeManJuanCompositeWnd:Refresh()
    self:RefreshInputAndOwn()
    self:RefreshBtns()
    self:RefreshComposite()
end

function LuaYaoYeManJuanCompositeWnd:RefreshInputAndOwn()
    local over = false
    for i = 1, #self.m_Infos do
        local info = self.m_Infos[i]
        if info.Input > info.Own then
            over = true
            break
        end
    end

    for i = 1, #self.m_Infos do
        local info = self.m_Infos[i]
        info.Input = over and 0 or info.Input
        self.m_InputLabs[i].text = SafeStringFormat3(LocalString.GetString("%d枚"), info.Input)
        self.m_OwnLabs[i].text = SafeStringFormat3(LocalString.GetString("持有%d枚"), info.Own)
    end
end

function LuaYaoYeManJuanCompositeWnd:InitItems()
    for i = 1, #self.m_Infos do
        local item = self.m_Items[i]
        local info = self.m_Infos[i]
        self:InitItem(item, i, info)
    end
end

function LuaYaoYeManJuanCompositeWnd:RefreshBtns()
    local input = 0
    for i = 1, #self.m_Infos do
        local info = self.m_Infos[i]
        local decBtn = self.m_DecBtns[i]
        local incBtn = self.m_IncBtns[i]

        input = input + info.Input
        decBtn.Enabled = info.Input > 0
        incBtn.Enabled = info.Input < info.Own
    end

    if input >= 3 then
        for i = 1, #self.m_IncBtns do
            local incBtn = self.m_IncBtns[i]
            incBtn.Enabled = false
        end
    end
end

function LuaYaoYeManJuanCompositeWnd:RefreshComposite()
    local input = 0
    for i = 1, #self.m_Infos do
        local info = self.m_Infos[i]
        input = input + info.Input
    end

    local compositable = input >= 3
    self.Composite:SetActive(compositable)
    self.CompositeTipLab.gameObject:SetActive(not compositable)

    local type = 0 
    for i = 1, #self.m_Infos do
        local info = self.m_Infos[i]
        if info.Input >= 2 then
            type = i
            break
        end
    end
    local colorNameStr = self.m_CompositeColorNames[type + 1]
    if type == 0 then
        self.DescLab.text = SafeStringFormat3(LocalString.GetString("三种妖叶数量均等将合成%s发饰"), colorNameStr)
    else
        local data = Item_Item.GetData(self.m_Infos[type].ItemId)
        local name = data.Name
        self.DescLab.text = SafeStringFormat3(LocalString.GetString("%s数量≥2将合成%s"), name, colorNameStr)
    end

    self.m_CompositeItemId = self.m_CompositeItemIds[type + 1]
    if self.m_CompositeItemId then
        local data = Item_Item.GetData(self.m_CompositeItemId)
        self.Icon:LoadMaterial(data.Icon)
    end

    if compositable then
        if type ~= self.m_CacheType then 
            self.m_CacheType = type
            self:SwitchBg(type)
        end
    else
        if nil ~= self.m_CacheType then 
            self.m_CacheType = nil
            self.m_Animation:Play("yaoyemanjuancompositewnd_xiaoshi")
        end
    end 

    self:RefreshCompositeFormulaId()
end

function LuaYaoYeManJuanCompositeWnd:RefreshCompositeFormulaId()
    local itemId2Num = {}
    for i = 1, #self.m_Infos do
        local info = self.m_Infos[i]
        itemId2Num[info.ItemId] = info.Input
    end

    self.m_CompositeFormulaId = 0
    local dataCount = YaoYeManJuan_Item.GetDataCount()
    for i = 1, dataCount do 
        local pass = true
        local synthesizeRule = YaoYeManJuan_Item.GetData(i).SynthesizeRule
        for j = 0, synthesizeRule.Length - 1 do 
            local itemId = synthesizeRule[j][0]
            local itemNum = synthesizeRule[j][1]

            if itemId2Num[itemId] ~= itemNum then
                pass = false
                break
            end
        end
        if pass then
            self.m_CompositeFormulaId = i
            return
        end
    end
end

function LuaYaoYeManJuanCompositeWnd:SwitchBg(id)
    local aniName = self.m_AniNames[id + 1]
    self.m_Animation:Play(aniName)
end