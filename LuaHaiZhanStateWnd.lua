local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CoreScene=import "L10.Engine.Scene.CoreScene"
local CClientObjectMgr=import "L10.Game.CClientObjectMgr"
local CClientPick = import "L10.Game.CClientPick"
local CCustomHpAndMpBar=import "L10.UI.CCustomHpAndMpBar"
local GameObject = import "UnityEngine.GameObject"
local CPhysicsTimeMgr=import "L10.Engine.CPhysicsTimeMgr"
local CGuideMgr = import "L10.Game.Guide.CGuideMgr"
local Ease = import "DG.Tweening.Ease"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local MessageMgr = import "L10.Game.MessageMgr"

LuaHaiZhanStateWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaHaiZhanStateWnd, "HpLabel", "HpLabel", UILabel)
RegistChildComponent(LuaHaiZhanStateWnd, "HpBar", "HpBar", CCustomHpAndMpBar)
RegistChildComponent(LuaHaiZhanStateWnd, "ExpandButton", "ExpandButton", GameObject)
RegistChildComponent(LuaHaiZhanStateWnd, "SwitchButton", "SwitchButton", GameObject)
RegistChildComponent(LuaHaiZhanStateWnd, "SwitchButton2", "SwitchButton2", GameObject)
RegistChildComponent(LuaHaiZhanStateWnd, "BulletNumLabel", "BulletNumLabel", UILabel)
RegistChildComponent(LuaHaiZhanStateWnd, "BoardNumLabel", "BoardNumLabel", UILabel)
RegistChildComponent(LuaHaiZhanStateWnd, "Fire", "Fire", Transform)
RegistChildComponent(LuaHaiZhanStateWnd, "LevelLabel", "LevelLabel", UILabel)
RegistChildComponent(LuaHaiZhanStateWnd, "ProgressBar", "ProgressBar", UITexture)
RegistChildComponent(LuaHaiZhanStateWnd, "CUIFx", "CUIFx", Transform)
RegistChildComponent(LuaHaiZhanStateWnd, "attackfx", "attackfx", GameObject)

RegistChildComponent(LuaHaiZhanStateWnd, "PvpInfo", "PvpInfo", GameObject)
RegistChildComponent(LuaHaiZhanStateWnd, "GoldLabel", "GoldLabel", UILabel)
RegistChildComponent(LuaHaiZhanStateWnd, "RankLabel", "RankLabel", UILabel)


RegistClassMember(LuaHaiZhanStateWnd, "m_TotalDeltaHp")
RegistClassMember(LuaHaiZhanStateWnd, "m_EndTime")
RegistClassMember(LuaHaiZhanStateWnd, "m_Duration")
RegistClassMember(LuaHaiZhanStateWnd, "m_IsPvpPlay")

RegistClassMember(LuaHaiZhanStateWnd, "LoudSpeak")
RegistClassMember(LuaHaiZhanStateWnd, "m_LoudSpeakTick")
RegistClassMember(LuaHaiZhanStateWnd, "m_CurDelay")

--@endregion RegistChildComponent end


function LuaHaiZhanStateWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    UIEventListener.Get(self.ExpandButton).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
        self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 0)
    end)

    self.HpLabel.text = "0"

    UIEventListener.Get(self.SwitchButton).onClick = DelegateFactory.VoidDelegate(function(go)
        if not LuaHaiZhanMgr:EnableSwitchCabinAndSea() then 
            g_MessageMgr:ShowMessage(LuaHaiZhanMgr.s_InCabin and "HaiZhanGuide_Cannot_ReturnToSea" or "JiaBan_Cannot_Back")
            return 
        end
        if LuaHaiZhanMgr.s_InCabin then
            Gac2Gas.RequestReturnToNavalSeaPlay()
        else
            Gac2Gas.RequestEnterNavalWarCabinPlay()
        end
    end)
    UIEventListener.Get(self.SwitchButton2).onClick = DelegateFactory.VoidDelegate(function(go)
        if not LuaHaiZhanMgr:EnableChangePos() then 
            g_MessageMgr:ShowMessage("Job_cannot_Choose")
            return 
        end
        CUIManager.ShowUI(CLuaUIResources.HaiZhanChangePosWnd)
    end)
    self.BulletNumLabel.text = "0"
    self.BoardNumLabel.text = "0"

    for i = 1, self.Fire.childCount do
        self.Fire:GetChild(i-1).gameObject:SetActive(false)
    end

    self.m_IsPvpPlay = LuaHaiZhanMgr.IsPvpPlay()
    self.PvpInfo:SetActive(self.m_IsPvpPlay)
    self.GoldLabel.text = "0"
    self.RankLabel.text = "5/5"

    self.LevelLabel.gameObject:SetActive(self.m_IsPvpPlay)
    self.ProgressBar.gameObject:SetActive(self.m_IsPvpPlay)

    self.LoudSpeak = self.transform:Find("TopRight/LoudSpeak")
    self.LoudSpeak.gameObject:SetActive(false)
end

function LuaHaiZhanStateWnd:Init()
    self.HpLabel.text = tostring(LuaHaiZhanMgr.s_Hp)
    if LuaHaiZhanMgr.s_CurrHpFull>0 then
        self.HpBar:UpdateValue(LuaHaiZhanMgr.s_Hp, LuaHaiZhanMgr.s_CurrHpFull, LuaHaiZhanMgr.s_HpFull)
    else
        self.HpBar:UpdateValue(0, 0, 0)
    end

    local icon1 = self.SwitchButton.transform:Find("Icon").gameObject
    local icon2 = self.SwitchButton.transform:Find("Icon2").gameObject
    if LuaHaiZhanMgr.s_InCabin then
        icon1:SetActive(false)
        icon2:SetActive(true)
    else
        icon1:SetActive(true)
        icon2:SetActive(false)
    end

    self:OnSyncNavalWarMaterials(LuaHaiZhanMgr.s_CntInfo,LuaHaiZhanMgr.s_BoardCnt)
    self:OnSyncNavalWarCabinShipHp(LuaHaiZhanMgr.s_Hp,LuaHaiZhanMgr.s_CurrHpFull,LuaHaiZhanMgr.s_HpFull)
    self:OnSyncNavalWarCabinFire(LuaHaiZhanMgr.s_FireNum or 0)

    if LuaHaiZhanMgr:IsInTrainingGameplay() then
        RegisterTickOnce(function()
            if not CUIManager.IsLoaded(CUIResources.JuQingDialogWnd) then
                EventManager.BroadcastInternalForLua(EnumEventType.Guide_ChangeView, {"UIRoot"})
            end
        end, 33)
    end
end

--@region UIEvent

--@endregion UIEvent
function LuaHaiZhanStateWnd:OnEnable()
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:AddListener("SyncNavalWarCabinShipHp",self,"OnSyncNavalWarCabinShipHp")
    g_ScriptEvent:AddListener("SyncNavalWarMaterials",self,"OnSyncNavalWarMaterials")
    g_ScriptEvent:AddListener("SyncNavalWarCabinFire",self,"OnSyncNavalWarCabinFire")
    g_ScriptEvent:AddListener("SyncNavalWarShipVirtualHpChange",self,"OnSyncNavalWarShipVirtualHpChange")
    g_ScriptEvent:AddListener("SyncNavalSpecialMonsterHp", self, "OnSyncNavalSpecialMonsterHp")
    g_ScriptEvent:AddListener("NavalWarShowImpMessage", self, "OnNavalWarShowImpMessage") 
end
function LuaHaiZhanStateWnd:OnDisable()
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:RemoveListener("SyncNavalWarCabinShipHp",self,"OnSyncNavalWarCabinShipHp")
    g_ScriptEvent:RemoveListener("SyncNavalWarMaterials",self,"OnSyncNavalWarMaterials")
    g_ScriptEvent:RemoveListener("SyncNavalWarCabinFire",self,"OnSyncNavalWarCabinFire")
    g_ScriptEvent:RemoveListener("SyncNavalWarShipVirtualHpChange",self,"OnSyncNavalWarShipVirtualHpChange")
    g_ScriptEvent:RemoveListener("SyncNavalSpecialMonsterHp", self, "OnSyncNavalSpecialMonsterHp")
    g_ScriptEvent:RemoveListener("NavalWarShowImpMessage", self, "OnNavalWarShowImpMessage") 
end

function LuaHaiZhanStateWnd:OnDestroy()
    UnRegisterTick(self.m_LoudSpeakTick)
    self.m_LoudSpeakTick = nil
    if self.m_CurDelay then
        UnRegisterTick(self.m_CurDelay.tick)
    end
    self.m_CurDelay = nil
end

local lastHp = nil
function LuaHaiZhanStateWnd:OnSyncNavalSpecialMonsterHp(engineId, hp, currentHpFull, hpFull)
    if not CClientMainPlayer.Inst then return end

    --自己受伤了
    if math.abs(CClientMainPlayer.Inst.AppearanceProp.DriverId)==engineId then
        if lastHp and lastHp>hp then
            self.attackfx:SetActive(false)
            self.attackfx:SetActive(true)
        end
        lastHp = hp
    end
end
--回血的血量
function LuaHaiZhanStateWnd:OnSyncNavalWarShipVirtualHpChange(totalDeltaHp, endTime, duration)
    self.m_TotalDeltaHp = totalDeltaHp
    self.m_EndTime = endTime
    self.m_Duration = duration
    self:UpdateHp()
end
function LuaHaiZhanStateWnd:UpdateHp()
    local current = CServerTimeMgr.Inst.timeStamp
    if self.m_EndTime and self.m_EndTime>=current then
        local hp,currHpFull,hpFull = LuaHaiZhanMgr.s_Hp,LuaHaiZhanMgr.s_CurrHpFull,LuaHaiZhanMgr.s_HpFull
        local left = (self.m_EndTime - current)/self.m_Duration*self.m_TotalDeltaHp
        local v = (hp+left)/hpFull
        self.HpBar.decayValueBar.value = v
    else
        self.HpBar.decayValueBar.value = 0
    end
end
function LuaHaiZhanStateWnd:OnHideTopAndRightTipWnd()
    self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 180);
end

function LuaHaiZhanStateWnd:OnSyncNavalWarCabinFire(fireNum)
    for i = 1, self.Fire.childCount do
        if i<=fireNum then
            self.Fire:GetChild(i-1).gameObject:SetActive(true)
        else
            self.Fire:GetChild(i-1).gameObject:SetActive(false)
        end
    end
    for i = 1,self.CUIFx.childCount do
        if i==fireNum then
            self.CUIFx:GetChild(i-1).gameObject:SetActive(true)
        else
            self.CUIFx:GetChild(i-1).gameObject:SetActive(false)
        end
    end
end

function LuaHaiZhanStateWnd:OnSyncNavalWarCabinShipHp(hp, currHpFull, HpFull)
    self.HpLabel.text = tostring(math.floor(hp))
    self.HpBar:UpdateValue(hp, currHpFull, HpFull)
    self:UpdateHp()

    --刷新战损特效，hpbar处理所有的船
    if CClientMainPlayer.Inst then
        local boatEngineId = math.abs(CClientMainPlayer.Inst.AppearanceProp.DriverId)
        LuaHaiZhanMgr.SyncNavalSpecialMonsterHp(boatEngineId, hp, currHpFull, HpFull)
    end
end

function LuaHaiZhanStateWnd:OnSyncNavalWarMaterials(cntInfo,boardCnt)
    local cnt = 0
    if cntInfo then
        for key, value in pairs(cntInfo) do
            cnt = cnt+value
        end
    end
    self.BulletNumLabel.text = tostring(cnt)
    self.BoardNumLabel.text = tostring(boardCnt)
end

function LuaHaiZhanStateWnd:TrySelectPick()
    if not LuaHaiZhanMgr.s_InCabin then return end

    local player = CClientMainPlayer.Inst
    if not player then return end
    local pos = player.RO.Position

    --应该选一个最近的
    for x=-1,1,1 do
        for y=-1,1,1 do
            local xx = math.floor(pos.x) + x
            local yy = math.floor(pos.z) + y

            local objectid_list = CoreScene.MainCoreScene:GetGridAllObjectIdsWithFloor(xx, yy, 0)
            if objectid_list and objectid_list.Count > 0 then
                for i = 1, objectid_list.Count do
                    local engineId = objectid_list[i-1]
                    local obj = CClientObjectMgr.Inst:GetObject(engineId)
                    if TypeIs(obj,typeof(CClientPick)) then
                        if obj.TemplateId == 37000363 or obj.TemplateId == 37000364 then
                            LuaHaiZhanMgr.s_PickEngineId = engineId
                            if CUIManager.IsLoaded(CLuaUIResources.HaiZhanPickUpWnd) or CUIManager.IsLoading(CLuaUIResources.HaiZhanPickUpWnd) then
                                return
                            else
                                CUIManager.ShowUI(CLuaUIResources.HaiZhanPickUpWnd)
                                return
                            end
                        end
                    end
                end
            end
        end
    end
    
    CUIManager.CloseUI(CLuaUIResources.HaiZhanPickUpWnd)
end
function LuaHaiZhanStateWnd:Update()
    self:TrySelectPick()

    if self.m_IsPvpPlay then
        local myId = math.abs(CClientMainPlayer.Inst and CClientMainPlayer.Inst.AppearanceProp.DriverId or 0)
        local myShipId = LuaHaiZhanMgr.s_EngineId2ShipId[myId] or LuaHaiZhanMgr.s_MyShipId or 0
        local gold = LuaHaiZhanMgr.s_ShipId2Gold[myShipId] or 0
        self.GoldLabel.text =tostring(gold)
        local rank = LuaHaiZhanMgr.s_ShipIdGoldRank[myShipId] or 0
        self.RankLabel.text = string.format("%d/%d",rank,LuaHaiZhanMgr.s_PvpPlayerNum)
        local exp = LuaHaiZhanMgr.s_ShipId2Exp[myShipId]
        self.LevelLabel.text, self.ProgressBar.fillAmount = LuaHaiZhanMgr.GetCurLv(exp)
    end
end

function LuaHaiZhanStateWnd:GetGuideGo(methodName)
    if methodName == "GetExchangeDutyButton" then
        return self.SwitchButton2
    elseif methodName == "GetDeckButton" then
        return self.SwitchButton
    elseif methodName == "GetStatePanel" then
        return LuaHaiZhanMgr.s_InCabin and self.transform:Find("TopRight/Left").gameObject
    end
end

function LuaHaiZhanStateWnd:OnNavalWarShowImpMessage(messageId, params)
    local icon = self.LoudSpeak:Find("LoudSpeakerIcon"):GetComponent(typeof(UITexture))
    local label = self.LoudSpeak:Find("LoudSpeakPanel/LoudSpeakLabel"):GetComponent(typeof(UILabel))
	
    self:Delay(function()
        LuaTweenUtils.DOKill(label.transform, false)
        self.LoudSpeak.gameObject:SetActive(true)
    
        local txt = MessageMgr.Inst:GetMessageFormat(messageId)
        if params then
            txt = g_MessageMgr:Format(txt, unpack(params))
        end
        label.text = txt
        self.LoudSpeak:GetComponent(typeof(UIWidget)):ResetAndUpdateAnchors()
        label:UpdateNGUIText()
        NGUIText.regionWidth = 1000000
        local size = NGUIText.CalculatePrintedSize(label.text)
        label.width = math.ceil(size.x)
    
        local hideMessageAfter = Message_Message.GetData(messageId).HideMessageAfter
    
        if string.len(txt) <= 40 then
            LuaTransformAccess.SetLocalPositionX(label.transform, -label.width / 2 + icon.width / 2 - 25)
            LuaTransformAccess.SetLocalPositionX(icon.transform, -label.width / 2 - icon.width / 2 - 25)
        else
            LuaTransformAccess.SetLocalPositionX(icon.transform, -406)
            local speed = 70
            local clip = self.LoudSpeak.transform:Find("LoudSpeakPanel"):GetComponent(typeof(UIPanel))
        
            --local duration = (label.localSize.x + clip.baseClipRegion.z) / speed
            --hideMessageAfter = math.max(hideMessageAfter, duration)
            local startPos = clip.baseClipRegion.x + clip.baseClipRegion.z * 0.5
            local endPos = clip.baseClipRegion.x - clip.baseClipRegion.z * 0.5 - label.localSize.x
        
            LuaTransformAccess.SetLocalPositionX(label.transform, startPos)
        
            local tween = LuaTweenUtils.DOLocalMoveX(label.transform, endPos, hideMessageAfter, true)
            CommonDefs.SetEase_Tweener(tween, Ease.Linear)
            --local func = DelegateFactory.TweenCallback(function () 
            --    if self and self.LoudSpeak then
            --        self.LoudSpeak.gameObject:SetActive(false)
            --    end
            --end)
            --CommonDefs.OnComplete_Tweener(CommonDefs.SetEase_Tweener(tween, Ease.Linear), func)
        end
    
        UnRegisterTick(self.m_LoudSpeakTick)
        self.m_LoudSpeakTick = RegisterTickOnce(function()
            self.LoudSpeak.gameObject:SetActive(false)
        end, hideMessageAfter * 1000)

        return tostring(messageId), hideMessageAfter
    end)
end

function LuaHaiZhanStateWnd:NxtDelay()
    UnRegisterTick(self.m_CurDelay.tick)
    self.m_CurDelay.tick = nil

    -- deep copy callbacks
    local oldCallbacks = {}
    if self.m_CurDelay.callbacks then
        for _, v in ipairs(self.m_CurDelay.callbacks) do
            table.insert(oldCallbacks, v)
        end
    end
    self.m_CurDelay.isDone = true
    local valid = 1
    for idx, func in ipairs(oldCallbacks) do
        if idx > valid then
            table.insert(self.m_CurDelay.callbacks, func)
        else
            if not self:DoDelay(func) then
                valid = valid + 1
            end
        end
    end
end

function LuaHaiZhanStateWnd:DoDelay(callback)
    if not callback then return end
    local name, duration = callback(self)
    -- print("DoDelay:", name, "duration="..(duration and duration or "nil"))
    if duration and duration > 0 then
        -- renew curDelay
        self.m_CurDelay = {}
        self.m_CurDelay.name = name
        self.m_CurDelay.callbacks = {}
        self.m_CurDelay.isDone = false
        self.m_CurDelay.tick = RegisterTickOnce(function()
            self:NxtDelay()
        end, duration * 1000)

        return true
    end
end

function LuaHaiZhanStateWnd:Delay(func)
    if not self.m_CurDelay or self.m_CurDelay.isDone then
        self:DoDelay(func)
    else
        table.insert(self.m_CurDelay.callbacks, func)
    end
end
