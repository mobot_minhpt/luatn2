local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"

LuaOlympicGamesView = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaOlympicGamesView, "GamePlayView", "GamePlayView", GameObject)
RegistChildComponent(LuaOlympicGamesView, "GamePlayEndView", "GamePlayEndView", GameObject)

--@endregion RegistChildComponent end

function LuaOlympicGamesView:Awake()
    UIEventListener.Get(self.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnClick()
	end)
    self:OnSyncOlympicBossStatus()
end

--@region UIEvent
function LuaOlympicGamesView:OnClick()
    if LuaOlympicGamesMgr.m_IsBossAlive then
        Gac2Gas.RequestOlympicBossInfo()
        --CUIManager.ShowUI(CLuaUIResources.OlympicGamePlayWnd)
    else
        g_MessageMgr:ShowMessage("OlympicGames_End")
    end
end
--@endregion UIEvent
function LuaOlympicGamesView:OnEnable()
    g_ScriptEvent:AddListener("SyncOlympicBossStatus", self, "OnSyncOlympicBossStatus")
    self:OnSyncOlympicBossStatus()
end

function LuaOlympicGamesView:OnDisable()
    g_ScriptEvent:RemoveListener("SyncOlympicBossStatus", self, "OnSyncOlympicBossStatus")
end

function LuaOlympicGamesView:OnSyncOlympicBossStatus()
    self.GamePlayView:SetActive(LuaOlympicGamesMgr.m_IsPlayStart and LuaOlympicGamesMgr.m_IsBossAlive)
    self.GamePlayEndView:SetActive(LuaOlympicGamesMgr.m_IsPlayStart and not LuaOlympicGamesMgr.m_IsBossAlive)
end
