CLuaSanJieFengHuaLuEntryWnd = class()

function CLuaSanJieFengHuaLuEntryWnd:Init()
    local button = self.transform:Find("Button").gameObject
    UIEventListener.Get(button).onClick = DelegateFactory.VoidDelegate(function(go)
        LuaWorldEventMgr2021.ShowFengHuaLu()
        CUIManager.CloseUI(CLuaUIResources.SanJieFengHuaLuEntryWnd)
    end)

    -- local label = self.transform:Find("Bg/Label"):GetComponent(typeof(UILabel))
    -- label.text = g_MessageMgr:FormatMessage("WorldEvent_ShiMen_Message")
    
end