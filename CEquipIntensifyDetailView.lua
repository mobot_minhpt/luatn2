-- Auto Generated!!
local CEquipIntensifyDetailView = import "L10.UI.CEquipIntensifyDetailView"
local CEquipmentIntensifyMgr = import "L10.Game.CEquipmentIntensifyMgr"
local CEquipmentProcessMgr = import "L10.Game.CEquipmentProcessMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CMiddleNoticeMgr = import "L10.UI.CMiddleNoticeMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EquipIntensify_Intensify = import "L10.Game.EquipIntensify_Intensify"
local EventManager = import "EventManager"
local LocalString = import "LocalString"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CEquipIntensifyDetailView.m_Awake_CS2LuaHook = function (this) 
    if CEquipmentProcessMgr.Inst.SelectEquipment == nil then
        this:InitNull()
    else
        local item = CItemMgr.Inst:GetById(CEquipmentProcessMgr.Inst.SelectEquipment.itemId)
        if item == nil or item.Equip.IsExtraEquipment then
            this:InitNull()
        end
    end
    UIEventListener.Get(this.IntensifyButton).onClick = DelegateFactory.VoidDelegate(function (p) 
        if CEquipmentProcessMgr.Inst.SelectEquipment == nil then
            CMiddleNoticeMgr.ShowMiddleNotice(LocalString.GetString("至少选中一件装备"), 1)
            return
        end
        CUIManager.ShowUI(CUIResources.EquipHandIntensifyWnd)
    end)
    UIEventListener.Get(this.AutoIntensifyButton).onClick = MakeDelegateFromCSFunction(this.OnAutoIntensifyButton, VoidDelegate, this)
end
CEquipIntensifyDetailView.m_OnAutoIntensifyButton_CS2LuaHook = function (this, go) 
    if CEquipmentProcessMgr.Inst.SelectEquipment == nil then
        CMiddleNoticeMgr.ShowMiddleNotice(LocalString.GetString("至少选中一件装备"), 1)
        return
    end

    CUIManager.ShowUI(CUIResources.EquipAutoIntensifyWnd)
    return
    --if not CEquipmentProcessMgr.Inst:CheckOperationConflict(CEquipmentProcessMgr.Inst.SelectEquipment) then
    --    return
    --end

    ----if (CEquipmentIntensifyMgr.Inst.CanIntensify(true))
    --do
    --    local info = CEquipmentIntensifyMgr.Inst.SelectedIntensifyDetail
    --    local config = EquipIntensify_Intensify.GetData(info.Level)
    --    if config == nil then
    --        return
    --    end

    --    local equip = CItemMgr.Inst:GetById(CEquipmentProcessMgr.Inst.SelectEquipment.itemId)

    --    if config.EquipGrade > equip.Grade then
    --        if CEquipmentProcessMgr.Inst.SelectEquipment.place == EnumItemPlace.Body then
    --            if config.EquipGrade > CClientMainPlayer.Inst.BasicProp.Level then
    --                local datas = InitializeList(CreateFromClass(MakeGenericClass(List, Object)), Object, config.EquipGrade)
    --                g_MessageMgr:ShowMessage("INTENSIFY_EQUIP_GRADE_TOO_HIGH", List2Params(datas))
    --            else
    --                CUIManager.ShowUI(CUIResources.EquipAutoIntensifyWnd)
    --            end
    --        else
    --            local template = Item_Item.GetData(config.ItemId)
    --            local datas = InitializeList(CreateFromClass(MakeGenericClass(List, Object)), Object, template.Name, config.EquipGrade)
    --            local message = g_MessageMgr:FormatMessage("INTENSIFY_CONFIRM_LEAVE_OVER", CommonDefs.ListToArray(datas))
    --            MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function () 
    --                CUIManager.ShowUI(CUIResources.EquipAutoIntensifyWnd)
    --            end), nil, nil, nil, false)
    --        end
    --    else
    --        CUIManager.ShowUI(CUIResources.EquipAutoIntensifyWnd)
    --    end
    --end
end
CEquipIntensifyDetailView.m_UpdateEquipIntensifyDetail_CS2LuaHook = function (this, equipmentId) 
    local equipment = CItemMgr.Inst:GetById(equipmentId)
    --等级
    local intensifyLevel = equipment.Equip.IntensifyLevel
    if intensifyLevel > 0 then
        this.IntensifyLevelLabel.text = "+" .. tostring(equipment.Equip.IntensifyLevel)
    else
        this.IntensifyLevelLabel.text = "0"
    end

    --加成
    local totalAddValue = equipment.Equip:GetIntensifyTotalValue()
    if totalAddValue > 0 then
        local sumMin, sumMax
        sumMin, sumMax = CEquipmentIntensifyMgr.Inst:GetTotalMinAndMaxValueByLevel(intensifyLevel)
        this.PropAddLabel.text = System.String.Format("{0}% ({1}%-{2}%)", totalAddValue, sumMin, sumMax)
    else
        this.PropAddLabel.text = "0%"
    end
    --完美度
    local perfection = equipment.Equip.Perfection
    CUICommonDef.UpdatePerFectionIcons(this.m_PerfectionIconList, perfection, intensifyLevel)
end
CEquipIntensifyDetailView.m_OnEnable_CS2LuaHook = function (this) 
    --if (CEquipmentProcessMgr.Inst.SelectEquipment !=null)
    --{
    --    OnSelected(CEquipmentProcessMgr.Inst.SelectEquipment.itemId);
    --}
    --else
    --{
    --    OnSelected("");
    --}
    --数据变化
    --if (CEquipmentProcessMgr.Inst.SelectEquipment !=null)
    --{
    --    OnSelected(CEquipmentProcessMgr.Inst.SelectEquipment.itemId);
    --}
    --else
    --{
    --    OnSelected("");
    --}
    EventManager.AddListenerInternal(EnumEventType.SendItem, MakeDelegateFromCSFunction(this.OnSendItem, MakeGenericClass(Action1, String), this))
    --数据变化
    EventManager.AddListenerInternal(EnumEventType.SetItemAt, MakeDelegateFromCSFunction(this.OnSetItemAt, MakeGenericClass(Action4, EnumItemPlace, UInt32, String, String), this))
    --数据有无变化

    EventManager.AddListenerInternal(EnumEventType.SelectEquipment, MakeDelegateFromCSFunction(this.OnSelected, MakeGenericClass(Action1, String), this))
    --数据变化
end
CEquipIntensifyDetailView.m_OnSendItem_CS2LuaHook = function (this, itemId) 
    local info = CEquipmentIntensifyMgr.Inst.SelectedIntensifyDetail
    if info == nil then
        return
    end
    local config = EquipIntensify_Intensify.GetData(info.Level)
    local item = CItemMgr.Inst:GetById(itemId)
    --装备刷新
    if CEquipmentProcessMgr.Inst.SelectEquipment.itemId == itemId then
        this:UpdateEquipIntensifyDetail(itemId)
    end
end
CEquipIntensifyDetailView.m_OnSetItemAt_CS2LuaHook = function (this, place, position, oldItemId, newItemId) 
    local info = CEquipmentIntensifyMgr.Inst.SelectedIntensifyDetail
    if info == nil then
        return
    end
    local config = EquipIntensify_Intensify.GetData(info.Level)
    if oldItemId == nil and newItemId == nil then
        return
    end
    local default
    if oldItemId ~= nil then
        default = oldItemId
    else
        default = newItemId
    end
    local itemId = default
    this:OnSendItem(itemId)
end
