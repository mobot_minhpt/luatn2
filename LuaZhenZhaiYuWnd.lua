local CCommonLuaScript = import "L10.UI.CCommonLuaScript"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnTableView = import "L10.UI.QnTableView"
local CUITexture = import "L10.UI.CUITexture"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local Vector3 = import "UnityEngine.Vector3"
local LayerDefine = import "L10.Engine.LayerDefine"
local Quaternion = import "UnityEngine.Quaternion"
local Camera = import "UnityEngine.Camera"
local CRenderObject = import "L10.Engine.CRenderObject"
local EasyTouch = import "EasyTouch"
local Input = import "UnityEngine.Input"
local Physics = import "UnityEngine.Physics"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local CClientHouseMgr=import "L10.Game.CClientHouseMgr"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local Transform = import "UnityEngine.Transform"
local UICamera = import "UICamera"
local CUICommonDef = import "L10.UI.CUICommonDef"
local EnumItemPlace=import "L10.Game.EnumItemPlace"
local IdPartition=import "L10.Game.IdPartition"
local CItemMgr=import "L10.Game.CItemMgr"
local CTalismanMgr = import "L10.Game.CTalismanMgr"

LuaZhenZhaiYuWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaZhenZhaiYuWnd, "Preview", "Preview", GameObject)
RegistChildComponent(LuaZhenZhaiYuWnd, "ChooseNode", "ChooseNode", GameObject)
RegistChildComponent(LuaZhenZhaiYuWnd, "ChooseBox", "ChooseBox", GameObject)
-- RegistChildComponent(LuaZhenZhaiYuWnd, "FxNode", "FxNode", CUIFx)
RegistChildComponent(LuaZhenZhaiYuWnd, "FishCountLabel", "FishCountLabel", UILabel)
RegistChildComponent(LuaZhenZhaiYuWnd, "FabaoView", "FabaoView", GameObject)
RegistChildComponent(LuaZhenZhaiYuWnd, "EditView", "EditView", GameObject)
RegistChildComponent(LuaZhenZhaiYuWnd, "CaptureBtn", "CaptureBtn", GameObject)
RegistChildComponent(LuaZhenZhaiYuWnd, "EditBtn", "EditBtn", GameObject)
RegistChildComponent(LuaZhenZhaiYuWnd, "FishTableView", "FishTableView", QnTableView)
RegistChildComponent(LuaZhenZhaiYuWnd, "EditFinish", "EditFinish", GameObject)
RegistChildComponent(LuaZhenZhaiYuWnd, "Fabao1", "Fabao1", GameObject)
RegistChildComponent(LuaZhenZhaiYuWnd, "Fabao2", "Fabao2", GameObject)
RegistChildComponent(LuaZhenZhaiYuWnd, "TipBtn", "TipBtn", GameObject)
RegistChildComponent(LuaZhenZhaiYuWnd, "HideFabaoBtn", "HideFabaoBtn", GameObject)
RegistChildComponent(LuaZhenZhaiYuWnd, "ShowFabaoBtn", "ShowFabaoBtn", GameObject)
RegistChildComponent(LuaZhenZhaiYuWnd, "MovePanel", "MovePanel", GameObject)
RegistChildComponent(LuaZhenZhaiYuWnd, "ShowAnchor", "ShowAnchor", GameObject)
RegistChildComponent(LuaZhenZhaiYuWnd, "HideAnchor", "HideAnchor", GameObject)
RegistChildComponent(LuaZhenZhaiYuWnd, "FabaoDetail", "FabaoDetail", GameObject)
RegistChildComponent(LuaZhenZhaiYuWnd, "FxAndModelPreview", "FxAndModelPreview", GameObject)
RegistChildComponent(LuaZhenZhaiYuWnd, "CloseButton", "CloseButton", GameObject)

--@endregion RegistChildComponent end
RegistChildComponent(LuaZhenZhaiYuWnd, "PreviewTexture", "PreviewTexture", UITexture)
RegistClassMember(LuaZhenZhaiYuWnd, "m_FishZswTemplateIdList")
RegistClassMember(LuaZhenZhaiYuWnd, "m_FishList")
RegistClassMember(LuaZhenZhaiYuWnd, "m_FishMinX")
RegistClassMember(LuaZhenZhaiYuWnd, "m_FishMaxX")
RegistClassMember(LuaZhenZhaiYuWnd, "m_FishMinY")
RegistClassMember(LuaZhenZhaiYuWnd, "m_FishMaxY")
RegistClassMember(LuaZhenZhaiYuWnd, "m_SelectedFishIndex")
RegistClassMember(LuaZhenZhaiYuWnd, "m_MaxFishNum")
RegistClassMember(LuaZhenZhaiYuWnd, "m_FabaoAttachedFishs")
RegistClassMember(LuaZhenZhaiYuWnd, "m_Fx")
RegistClassMember(LuaZhenZhaiYuWnd, "m_HideUiTick")

function LuaZhenZhaiYuWnd:Awake()
    self.m_FishList = {}
    self.EditBtn:SetActive(true)
    self.EditFinish:SetActive(false)
    self.CaptureBtn:SetActive(not CommonDefs.IS_VN_CLIENT)
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.CaptureBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCaptureBtnClick()
	end)


	
	UIEventListener.Get(self.EditBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnEditBtnClick()
	end)


	
	UIEventListener.Get(self.EditFinish.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnEditFinishClick()
	end)


	
	UIEventListener.Get(self.HideFabaoBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnHideFabaoBtnClick()
	end)


	
	UIEventListener.Get(self.ShowFabaoBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShowFabaoBtnClick()
	end)

    UIEventListener.Get(self.TipBtn).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipBtnClick()
	end)


    --@endregion EventBind end
    self.m_Camera = UICamera.currentCamera
    CommonDefs.AddOnDragListener(self.PreviewTexture.gameObject, DelegateFactory.Action_GameObject_Vector2(function (go, delta)
		self:OnScreenDrag(go, delta)
    end), false)
    CommonDefs.AddOnClickListener(self.PreviewTexture.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnScreenClick(go) end), false)--]]

    self.m_FishJarMaxX = 504
    self.m_FishJarMinX = -501
    self.m_FishMinY = -258
    self.m_FishMaxY = 258
    self.m_FishMinX = -1523
    self.m_FishMaxX = 1523

    self.m_MaxFishNum = HouseFish_Setting.GetData().ZhenZhaiFishMaxNum

    self.HideFabaoBtn:SetActive(true)
    self.ShowFabaoBtn:SetActive(false)
end

function LuaZhenZhaiYuWnd:Init()
    self.m_SelectedFishIndex = nil
    local loader = function(fx)  
        self.m_Fx = fx
        fx:DestroyFx()
        fx.OnLoadFxFinish = DelegateFactory.Action(function () 
            local tfs = CommonDefs.GetComponentsInChildren_Component_Type(fx.FxGo.transform, typeof(Transform))
            do
                local i = 0
                while i < tfs.Length do
                    tfs[i].gameObject.layer = LayerDefine.ModelForNGUI_3D
                    i = i + 1
                end
            end

            self.m_FishJarRoot = fx.FxRoot
            self.m_FishJarNode = self.m_FishJarRoot:GetChild(0)
            self:OnUpdateHouseFishInfo()
        end)
        fx:LoadFx("Fx/UI/Prefab/UI_zhenzhaiyu.prefab")
    end

    self.m_FishZswTemplateIdList = {}
    self.m_FishGenPointNames = {"yu_zuobiao01","yu_zuobiao02","yu_zuobiao03","yu_zuobiao04","yu_zuobiao05"}
    self.m_LastFishCount = 0
    self.m_FishCount = 0
    self.m_FabaoAttachedFishs = {{0,0},{0,0}}

    for i,fish in ipairs(self.m_FishList) do
        if fish.ro then
            GameObject.DestroyImmediate(fish.ro.transform.gameObject)
        end
        fish = nil
    end
    self.m_FishList = {}

    if not CClientHouseMgr.Inst.mCurFurnitureProp2 then
        g_MessageMgr:ShowMessage("Open_ZhenzhaiyuWnd_Must_AtHome")
        CUIManager.CloseUI(CLuaUIResources.ZhenZhaiYuWnd)
        return
    end
    self.ChooseBox:SetActive(false)
    self.FishTableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return self.m_MaxFishNum
        end,
        function(item,row)
            self:InitItem(item,row)
        end)

    self.FishTableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
        self:OnSelectFishTableView(row + 1)
    end)

    local fishInfoType = CClientHouseMgr.Inst.mCurFurnitureProp2.FishInfo
    local dic = fishInfoType.ZhengzhaiFish

    if not  CClientMainPlayer.Inst then return end
    local playerId = CClientMainPlayer.Inst.Id
    LuaSeaFishingMgr.m_PlacedZhenZhaiYu = {}
    self.m_FabaoAttachedFishs = {{},{}}
    if CommonDefs.DictContains_LuaCall(dic, playerId) then
        local zhengzhaiFishInfoType = CommonDefs.DictGetValue_LuaCall(dic, playerId)
        local zhenzhaiFishMap = zhengzhaiFishInfoType.AllFishes
        CommonDefs.DictIterate(zhenzhaiFishMap, DelegateFactory.Action_object_object(function (___key, ___value) 
            local data = {}
            data.fishmapId = tonumber(___key)
            data.itemId = ___value.TemplateId--tonumber(___key)
            data.templateId = ___value.TemplateId
            data.duration = ___value.Duration
            data.wordId = ___value.WordId
            data.attachedFaBaoId = ___value.AttachedFaBaoId
            data.count = 1

            if data.attachedFaBaoId == 1 then
                table.insert(self.m_FabaoAttachedFishs[1],data)
            elseif data.attachedFaBaoId == 2 then
                table.insert(self.m_FabaoAttachedFishs[2],data)
            end
            
            if Zhuangshiwu_Zhuangshiwu.GetData(data.itemId) then
                table.insert(LuaSeaFishingMgr.m_PlacedZhenZhaiYu,data)
            end
        end))
    end

    local sortfunc = function(a,b)
        return a.itemId < b.itemId
    end
    table.sort(LuaSeaFishingMgr.m_PlacedZhenZhaiYu,sortfunc)

    self.m_FishJarRoot = nil
    self.m_FishJarNode = nil

    self.FxAndModelPreview.transform:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf:Init("YuGang",loader,0,0.5,0, 0, 0,true,self.FxAndModelPreview.transform)
    self.m_Camera = self.FxAndModelPreview.transform:Find("YuGang"):GetComponent(typeof(Camera))
    self:InitFaBaoView()

    self:InitCountLabel()
end

function LuaZhenZhaiYuWnd:InitCountLabel()
    self.FishCountLabel.text = SafeStringFormat3(LocalString.GetString("佑宅福鱼数量%d/%d"),#LuaSeaFishingMgr.m_PlacedZhenZhaiYu,self.m_MaxFishNum)
end

function LuaZhenZhaiYuWnd:InitFaBaoView()
    self:InitFaBao(1,self.Fabao1)
    self:InitFaBao(2,self.Fabao2)
end

function LuaZhenZhaiYuWnd:InitFaBao(suitIndex,item)
    local fabaoLabel = item.transform:Find("FabaoLabel"):GetComponent(typeof(UILabel))
    local suitLabel1 = item.transform:Find("SuitLabel1"):GetComponent(typeof(UILabel))
    local suitLabel2 = item.transform:Find("SuitLabel2"):GetComponent(typeof(UILabel))
    local fish1 = item.transform:Find("Fish1")
    local fish2 = item.transform:Find("Fish2")

    if not CClientMainPlayer.Inst then return end
    local activeIdx = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp.ActiveAttrGroupIdx or 1
    if activeIdx==0 then
        activeIdx=1
    end
    local v = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.AttrGroups,activeIdx)
    local activeTalismanSet = 1
    if v then
        activeTalismanSet = v.TalismanSetIdx
    end

    if suitIndex == activeTalismanSet then
        fabaoLabel.text = SafeStringFormat3(LocalString.GetString("法宝栏%d (生效)"),suitIndex)
    else
        fabaoLabel.text = SafeStringFormat3(LocalString.GetString("法宝栏%d"),suitIndex)
    end

    --套装属性
    suitLabel1.text = ""
    suitLabel2.text = ""
    local suits = CClientMainPlayer.Inst.ItemProp.TalismanSuits
    local suitWords = {}

    local startIdx = LuaEnumBodyPosition.TalismanQian
    local endIdx = LuaEnumBodyPosition.TalismanDui
    startIdx = startIdx + (suitIndex-1)*8
    endIdx = endIdx + (suitIndex-1)*8
    
    local itemProp = CClientMainPlayer.Inst.ItemProp
    local suitTables = {}
    local suitFeiShengAdjusted = {}
    for i=startIdx,endIdx do
        local itemId = itemProp:GetItemAt(EnumItemPlace.Body, i) or ""
        local talisman = CItemMgr.Inst:GetById(itemId)
        if talisman and IdPartition.IdIsTalisman(talisman.TemplateId) then
            local suitInfo = CTalismanMgr.Inst:GetNormalTalismanSuitInfo(talisman)
            if suitInfo then
                for i=0,suitInfo.SuitInfos.Count-1,1 do
                    local suit = suitInfo.SuitInfos[i]
                    local myTbl = {}
                    myTbl.info = suit
                    myTbl.isFeiShengAdjusted = talisman.Equip.IsFeiShengAdjusted
                    if suitTables[suit.SuitId] then
                        table.insert(suitTables[suit.SuitId],myTbl)                       
                    else
                        suitTables[suit.SuitId] = {}
                        table.insert(suitTables[suit.SuitId],myTbl)
                    end
                end
            end
        end
    end

    for suitId,mySuitTbls in pairs(suitTables) do--suitInfos
        local count = #mySuitTbls
        local suitInfos = mySuitTbls[1].info
        local isFeiShengAdjusted = mySuitTbls[1].isFeiShengAdjusted
        local needCount = suitInfos.NeedCount

        if count >= needCount then
            local suitData = Talisman_Suit.GetData(suitId)
            if suitData and suitData.IsXianJia ~= 1 then
                local needAdjusted = isFeiShengAdjusted > 0 and CLuaEquipTip.b_NeedShowFeiShengAdjusted
                if needAdjusted and suitData.FeiShengReplaceWords ~= nil then
                    table.insert(suitWords,suitData.FeiShengReplaceWords[0])
                else
                    table.insert(suitWords,suitData.Words[0])
                end
            end
        end
    end

    local label = suitLabel1
    for i=1,#suitWords,2 do
        if i == 3 then
            label = suitLabel2
        end    
        local word1 = suitWords[i]
        local word2 = suitWords[i+1]
        local str1 = ""
        local str2 =""
        if word1 then
            str1 = Word_Word.GetData(word1).Description
        end
        if word2 then
            str2 = Word_Word.GetData(word2).Description
        end
        if str1=="" and str2=="" then
            label.text = ""
        elseif str2 == "" then
            label.text = str1
        else
            label.text = SafeStringFormat2(LocalString.GetString("%s，%s"),str1, str2)
        end
    end

    --法宝栏attach的鱼
    for i, v in ipairs(self.m_FabaoAttachedFishs[suitIndex]) do
        if i==1 then
            self:InitFaBaoFish(suitIndex,fish1,v,suitWords,i)
        elseif i== 2 then
            self:InitFaBaoFish(suitIndex,fish2,v,suitWords,i)
        end
    end
    if #self.m_FabaoAttachedFishs[suitIndex] < 2 then
        for i= #self.m_FabaoAttachedFishs[suitIndex] + 1 ,2,1 do
            if i==1 then
                self:InitFaBaoFish(suitIndex,fish1,nil,suitWords,i)
            elseif i== 2 then
                self:InitFaBaoFish(suitIndex,fish2,nil,suitWords,i)
            end
        end
    end
    -- self:InitFaBaoFish(fish1,nil)
    -- self:InitFaBaoFish(fish2,nil)

end

function LuaZhenZhaiYuWnd:InitFaBaoFish(suitIndex,fish,zhenzhaiInfo,suitWords,findex)
    local fishIcon = fish:Find("FishIcon"):GetComponent(typeof(CUITexture))
    local emptyNode = fish:Find("EmptyNode")
    local border = fish:Find("FishIcon/Border"):GetComponent(typeof(UISprite))
    local nameLabel = fish:Find("FishIcon/NameLabel"):GetComponent(typeof(UILabel))
    local shuXingLabel = fish:Find("FishIcon/ShuXingLabel"):GetComponent(typeof(UILabel))
    fishIcon.gameObject:SetActive(zhenzhaiInfo ~= nil)
    emptyNode.gameObject:SetActive(zhenzhaiInfo == nil)


    if not zhenzhaiInfo then
        UIEventListener.Get(fish.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            local attachedFishs = self.m_FabaoAttachedFishs[suitIndex]
            LuaZhenZhaiYuMgr.OpenChooseAttachedZhenzhaiyu(suitIndex,suitWords,attachedFishs)
        end)
        return
    else
        UIEventListener.Get(fish.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            LuaSeaFishingMgr.OpenZhenZhaiYuDetailWnd(zhenzhaiInfo.itemId,zhenzhaiInfo)
        end)
    end

    local fishId = zhenzhaiInfo.itemId
    local zswData = Zhuangshiwu_Zhuangshiwu.GetData(fishId)
    local itemData = Item_Item.GetData(zswData.ItemId)
    fishIcon:LoadMaterial(itemData.Icon)
    border.spriteName = CUICommonDef.GetItemCellBorder(itemData.NameColor)
    nameLabel.text = itemData.Name

    local isConflct = false
	local fishWord = zhenzhaiInfo.wordId
	if fishWord and fishWord ~= 0 then
		local fisgCls = math.floor(fishWord/100)
		local data = HouseFish_WordConflict.GetData(fisgCls)
		if data and suitWords then
			local conflicts = data.Conflict
			for i=0,conflicts.Length-1,1 do
				local cls = conflicts[i]
				for j=1,#suitWords,1 do
					if cls == math.floor(suitWords[j]/100) then
						isConflct = true
						break
					end
				end
				if isConflct then
					break
				end
			end
		end
        -- 是否和已关联的镇宅鱼属性相同
		if findex == 2 then
            local attachedFishs = self.m_FabaoAttachedFishs[suitIndex]
            if attachedFishs[1] and attachedFishs[2] then
                local fworld1 = attachedFishs[1].wordId
                local fworld2 = attachedFishs[2].wordId
                if math.floor(fworld1/100) == math.floor(fworld2/100) then
                    isConflct = true
                end
            end
        end

        if isConflct then
            shuXingLabel.text = LocalString.GetString("[c][ff0000]属性互斥[-][/c]")
        else
            shuXingLabel.text = Word_Word.GetData(fishWord).Description
        end
    else
        shuXingLabel.text = LocalString.GetString("暂无")
	end
end

--@region UIEvent

function LuaZhenZhaiYuWnd:OnCaptureBtnClick()
    self:OnHideFabaoBtnClick()
    self:OnEditFinishClick()
    self:RefreshUIForCapture(false)

    if self.m_HideUiTick then
        UnRegisterTick(self.m_HideUiTick)
        self.m_HideUiTick = nil
    end
    self.m_HideUiTick = RegisterTickOnce(function()
        self:RefreshUIForCapture(true)
    end, 500)

    CUICommonDef.CaptureScreenAndShare()
end

function LuaZhenZhaiYuWnd:RefreshUIForCapture(isShow)
    self.CloseButton:SetActive(isShow)
    self.CaptureBtn:SetActive(isShow and not CommonDefs.IS_VN_CLIENT)
    self.FishCountLabel.gameObject:SetActive(isShow)
    self.FabaoView:SetActive(isShow)
    if isShow then
        self.EditBtn:SetActive(isShow)
    else
        self.EditBtn:SetActive(isShow)
        self.EditFinish:SetActive(isShow)
    end  
end

function LuaZhenZhaiYuWnd:OnEditBtnClick()
    self.EditBtn:SetActive(false)
    self.EditFinish:SetActive(true)
    self.EditView:SetActive(true)
    self:OnHideFabaoBtnClick()
end

function LuaZhenZhaiYuWnd:OnTipBtnClick()
    g_MessageMgr:ShowMessage("Zhenzhaiyu_Attach_Talisman_Tip")
end


function LuaZhenZhaiYuWnd:OnEditFinishClick()
    self.EditBtn:SetActive(true)
    self.EditFinish:SetActive(false)
    self.EditView:SetActive(false)
end


function LuaZhenZhaiYuWnd:OnHideFabaoBtnClick()
    self.FabaoDetail:SetActive(false)
    self.HideFabaoBtn:SetActive(false)
    self.ShowFabaoBtn:SetActive(true)
    self.MovePanel.transform.localPosition = self.HideAnchor.transform.localPosition
end

function LuaZhenZhaiYuWnd:OnShowFabaoBtnClick()
    self.FabaoDetail:SetActive(true)
    self.HideFabaoBtn:SetActive(true)
    self.ShowFabaoBtn:SetActive(false)
    self.MovePanel.transform.localPosition = self.ShowAnchor.transform.localPosition
    self:OnEditFinishClick()
end


--@endregion UIEvent
function LuaZhenZhaiYuWnd:OnSelectFishTableView(index)
    --选择
    if index <= self.m_FishCount then
        self.m_SelectedFishIndex = index
        local fish = self.m_FishList[index]
        local boxPos = fish.ro.transform.position
        self:OnSelectFish(boxPos)
    --添加
    else
        LuaSeaFishingMgr.OpenFishCreelWnd()
    end
end

function LuaZhenZhaiYuWnd:InitItem(item,row)
    local fishNode = item.transform:Find("FishIcon")
    local emptyNode = item.transform:Find("EmptyNode")
    --添加
    if row + 1 > self.m_FishCount then
        fishNode.gameObject:SetActive(false)
        emptyNode.gameObject:SetActive(true)
    else
        fishNode.gameObject:SetActive(true)
        emptyNode.gameObject:SetActive(false)

        local fishIcon = fishNode:GetComponent(typeof(CUITexture))
        local border = fishNode:Find("Border"):GetComponent(typeof(UISprite))

        local zswId = self.m_FishZswTemplateIdList[row + 1].zswId
        local itemId = Zhuangshiwu_Zhuangshiwu.GetData(zswId).ItemId
        local itemData = Item_Item.GetData(itemId)
        fishIcon:LoadMaterial(itemData.Icon)
        border.spriteName = CUICommonDef.GetItemCellBorder(itemData.NameColor)
    end
end

function LuaZhenZhaiYuWnd:OnEnable()
    g_ScriptEvent:AddListener("UnSelectZhenZhaiYuInWnd",self,"OnUnSelectZhenZhaiYuInWnd")
    g_ScriptEvent:AddListener("UpdateHouseFishInfo",self,"OnUpdateView")

    g_ScriptEvent:AddListener("UpdateZhengzhaiFishInfo",self,"OnUpdateView")
    g_ScriptEvent:AddListener("ShowZhenZhaiFishEditingView",self,"OnShowZhenZhaiFishEditingView")
end

function LuaZhenZhaiYuWnd:OnDisable()
    g_ScriptEvent:RemoveListener("UnSelectZhenZhaiYuInWnd",self,"OnUnSelectZhenZhaiYuInWnd")
    g_ScriptEvent:RemoveListener("UpdateHouseFishInfo",self,"OnUpdateView")

    g_ScriptEvent:RemoveListener("UpdateZhengzhaiFishInfo",self,"OnUpdateView")
    g_ScriptEvent:RemoveListener("ShowZhenZhaiFishEditingView",self,"OnShowZhenZhaiFishEditingView")
    if self.m_HideUiTick then
        UnRegisterTick(self.m_HideUiTick)
        self.m_HideUiTick = nil
    end
end
--初始化鱼缸
function LuaZhenZhaiYuWnd:InitFishJar(lastCount) 
    if not self.m_FishJarRoot or not self.m_FishJarNode then return end

    for i=lastCount + 1,#self.m_FishZswTemplateIdList,1 do
        local id = self.m_FishZswTemplateIdList[i].zswId
        local data = Zhuangshiwu_Zhuangshiwu.GetData(id)
        local index
        if data then 
            local itemId = data.ItemId
            local fishdata = HouseFish_AllFishes.GetData(itemId)
            if not fishdata then break end
            local prefabname = fishdata.PrefabPath
            local num = #self.m_FishGenPointNames
            if fishdata.IsBigFish == 1 then
                index = LuaZhenZhaiYuMgr.m_BigFishLayerIndex
            else
                index = UnityEngine_Random(LuaZhenZhaiYuMgr.m_SmallFishLayerIndex1, LuaZhenZhaiYuMgr.m_SmallFishLayerIndex2+1)
            end

            local startPointName = self.m_FishGenPointNames[index]
            local parent = self.m_FishJarNode:Find(startPointName)
            if parent then

                local mfish = self.m_FishList[i]
                local newRo
                local needInsert = false
                if mfish and mfish.ro then
                    newRo = mfish.ro
                else
                    if startPointName then   
                        newRo = CRenderObject.CreateRenderObject(self.m_FishJarRoot.gameObject, "fish_"..i)    
                    end
                    newRo.Layer = LayerDefine.ModelForNGUI_3D
                    newRo:LoadMain(prefabname)
                    needInsert = true
                end
                
                local pos = newRo.transform.localPosition
                pos.z = parent.localPosition.z
                local x = UnityEngine_Random(self.m_FishJarMinX, self.m_FishJarMaxX)
                local y = UnityEngine_Random(self.m_FishMinY, self.m_FishMaxY)
                if fishdata.IsBigFish == 1 then
                    y = 0
                end
                pos.x = x
                pos.y = y
                newRo.transform.localPosition = pos
                
                local fish = {}
                fish.ro = newRo
                local velocity = UnityEngine_Random(LuaZhenZhaiYuMgr.m_ZhenZhaiYuMinVelocity, LuaZhenZhaiYuMgr.m_ZhenZhaiYuMaxVelocity+1)
                local tag = UnityEngine_Random(-1, 1)
                tag = tag<0 and -1 or 1
                fish.velocity = velocity * tag
                
                if fish.velocity == 0 then
                    fish.velocity  = 1
                end
                if fishdata.IsBigFish == 1 then
                    fish.velocity = fish.velocity * LuaZhenZhaiYuMgr.m_BigFishVelocityScale
                end
                fish.isBigFish = fishdata.IsBigFish

                if needInsert then
                    table.insert(self.m_FishList,fish)
                end
                newRo.transform.localRotation = Quaternion.Euler(0,-90,0)
                newRo.transform.localScale = Vector3(LuaZhenZhaiYuMgr.m_FishInFishJarScale*data.Scale,LuaZhenZhaiYuMgr.m_FishInFishJarScale*data.Scale,LuaZhenZhaiYuMgr.m_FishInFishJarScale*data.Scale)----
            end
        end
    end
end

function LuaZhenZhaiYuWnd:OnScreenDrag(go, delta)
    if EasyTouch.GetTouchCount() == 1 then
        if not self.m_FishJarRoot then return end
        local pos = self.m_FishJarRoot.localPosition
        pos.x = pos.x + delta.x * 5
        if pos.x > self.m_FishJarMaxX then
            pos.x = self.m_FishJarMaxX
        elseif pos.x < self.m_FishJarMinX then
            pos.x = self.m_FishJarMinX
        end
        self.m_FishJarRoot.localPosition = pos 
    end
end

function LuaZhenZhaiYuWnd:OnScreenClick(go)
    local screenPos = Input.mousePosition
    local worldTexPos = self.PreviewTexture.transform.position
    local width = self.PreviewTexture.width
    local height = self.PreviewTexture.height
    local clickWpos = CUIManager.UIMainCamera:ScreenToWorldPoint(screenPos)

    self.PreviewTexture.transform.position = clickWpos 
    local cliclPos = self.PreviewTexture.transform.localPosition
    self.PreviewTexture.transform.position = worldTexPos

    local textPos = self.PreviewTexture.transform.localPosition    
    local pos = Vector3(cliclPos.x-textPos.x,cliclPos.y-textPos.y,0)
    local viewPos = Vector3(pos.x/width,pos.y/height,0)

   
    local ray = self.m_Camera:ViewportPointToRay(viewPos)
    local hits = Physics.RaycastAll(ray, 20, self.m_Camera.cullingMask)
    if not hits then return end

    for i=0,hits.Length-1 do
        local collider = hits[i].collider.gameObject
        local name = collider.gameObject.name
        local fishname = collider.transform.parent.name
        local fishIndex = string.match(fishname,"(%d+)")
        fishIndex = tonumber(fishIndex)
        self.m_SelectedFishIndex = fishIndex
    end
    if self.m_SelectedFishIndex then
        self:OnSelectFish(clickWpos)
    end
end

function LuaZhenZhaiYuWnd:OnSelectFish(boxPos)
    if boxPos then
        boxPos.z = 0
        self.ChooseBox.transform.position = boxPos
    else
        local worldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(Input.mousePosition)
        worldPos.z = 0
        self.ChooseBox.transform.position = worldPos
    end
    self.ChooseBox:SetActive(true)
    local zhenzhaiInfo = self.m_FishZswTemplateIdList[self.m_SelectedFishIndex]
    local zswId = zhenzhaiInfo.zswId
    local fish = self.m_FishList[self.m_SelectedFishIndex]
    fish.velocity = 0
    LuaSeaFishingMgr.OpenZhenZhaiYuDetailWnd(zswId,zhenzhaiInfo.info)
end

function LuaZhenZhaiYuWnd:OnUnSelectZhenZhaiYuInWnd()
    local fish = self.m_FishList[self.m_SelectedFishIndex]
    if not fish then return end 
    local velocity = UnityEngine_Random(LuaZhenZhaiYuMgr.m_ZhenZhaiYuMinVelocity, LuaZhenZhaiYuMgr.m_ZhenZhaiYuMaxVelocity+1)
    local tag = UnityEngine_Random(-0.5, 0.5)
    tag = tag<=0 and -1 or 1
    fish.velocity = velocity * tag
    if fish.velocity == 0 then
        fish.velocity  = 1
    end

    if fish.isBigFish == 1 then
        fish.velocity = fish.velocity * LuaZhenZhaiYuMgr.m_BigFishVelocityScale
    end
    --转向
    if fish.velocity < 0 and fish.ro.transform.localEulerAngles.y ~= 270 then--向左
        fish.ro.transform.localEulerAngles = Vector3(fish.ro.transform.localEulerAngles.x,270,fish.ro.transform.localEulerAngles.z)
    elseif fish.velocity > 0 and fish.ro.transform.localEulerAngles.y ~= 90 then--向右
        fish.ro.transform.localEulerAngles = Vector3(fish.ro.transform.localEulerAngles.x,90,fish.ro.transform.localEulerAngles.z)
    end

    self.m_SelectedFishIndex = nil
    self.ChooseBox:SetActive(false)
end

function LuaZhenZhaiYuWnd:OnDestroy()
    self.m_FishJarRoot = nil
    self.m_FishJarNode = nil
    if self.m_Fx then
        self.m_Fx:DestroyFx()
    end
    self.PreviewTexture.mainTexture = nil
end

function LuaZhenZhaiYuWnd:Update()
    for i,fish in ipairs(self.m_FishList) do
        if self.m_SelectedFishIndex and i == self.m_SelectedFishIndex then
            fish.velocity = 0
        else
            local pos = fish.ro.transform.localPosition
            pos.x = pos.x  + fish.velocity

            if pos.x > self.m_FishMaxX then
                pos.x = self.m_FishMinX
                fish.velocity = -fish.velocity
            elseif pos.x < self.m_FishMinX then
                pos.x = self.m_FishMaxX 
                fish.velocity = -fish.velocity
            else
                fish.ro.transform.localPosition = pos         
            end
            if fish.velocity < 0 and fish.ro.transform.localEulerAngles.y ~= 270 then--向左
                fish.ro.transform.localEulerAngles = Vector3(fish.ro.transform.localEulerAngles.x,270,fish.ro.transform.localEulerAngles.z)
            elseif fish.velocity > 0 and fish.ro.transform.localEulerAngles.y ~= 90 then--向右
                fish.ro.transform.localEulerAngles = Vector3(fish.ro.transform.localEulerAngles.x,90,fish.ro.transform.localEulerAngles.z)
            end
        end
    end
end

function LuaZhenZhaiYuWnd:OnUpdateHouseFishInfo()
    self.m_FishZswTemplateIdList= {}
    self.m_FishCount = 0
    for i,data in ipairs(LuaSeaFishingMgr.m_PlacedZhenZhaiYu) do
        local zswId = data.itemId
        local count = data.count
        for c=1,count,1 do
            local zhenzhai = {}
            zhenzhai.zswId = zswId
            zhenzhai.info = data
            table.insert(self.m_FishZswTemplateIdList,zhenzhai)
            self.m_FishCount = self.m_FishCount + 1
        end
    end
    self.m_LastFishCount = 0
    self:InitFishJar(self.m_LastFishCount)
    self.m_LastFishCount = self.m_FishCount
    self.FishTableView:ReloadData(true,true)
    self:InitCountLabel()
end

function LuaZhenZhaiYuWnd:OnUpdateView()
    if CUIManager.IsLoaded(CLuaUIResources.ZhenZhaiYuDetailWnd) then
        CUIManager.CloseUI(CLuaUIResources.ZhenZhaiYuDetailWnd)
    end
    if CUIManager.IsLoaded(CLuaUIResources.FishCreelWnd) then
        CUIManager.CloseUI(CLuaUIResources.FishCreelWnd)
    end
    if CUIManager.IsLoaded(CLuaUIResources.BringUpZhenZhaiYuWnd) then
        CUIManager.CloseUI(CLuaUIResources.BringUpZhenZhaiYuWnd)
    end
    if CUIManager.IsLoaded(CLuaUIResources.FeedZhenZhaiYuWnd) then
        CUIManager.CloseUI(CLuaUIResources.FeedZhenZhaiYuWnd)
    end
    self:Init()
end

function LuaZhenZhaiYuWnd:OnShowZhenZhaiFishEditingView()
    if CUIManager.IsLoaded(CLuaUIResources.FishCreelWnd) then
        CUIManager.CloseUI(CLuaUIResources.FishCreelWnd)
    end
    self:OnEditBtnClick()
end
