local COpenEntryMgr=import "L10.Game.COpenEntryMgr"
local UIGrid = import "UIGrid"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local NPC_NPC = import "L10.Game.NPC_NPC"
local CUITexture = import "L10.UI.CUITexture"
local Extensions = import "Extensions"
local ButterflyCrisis_Reward = import "L10.Game.ButterflyCrisis_Reward"
local Item_Item = import "L10.Game.Item_Item"
local CUIFx = import "L10.UI.CUIFx"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local ButterflyCrisis_Setting = import "L10.Game.ButterflyCrisis_Setting"
local CItemMgr = import "L10.Game.CItemMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local ButterflyCrisis_RMB = import "L10.Game.ButterflyCrisis_RMB"
local CGuideMgr = import "L10.Game.Guide.CGuideMgr"
local Time = import "UnityEngine.Time"

LuaButterflyCrisisMainWnd = class()

--@region 参数

-- Header
RegistChildComponent(LuaButterflyCrisisMainWnd, "MoreInfo", GameObject)
RegistChildComponent(LuaButterflyCrisisMainWnd, "ButterflyScalesLabel", UILabel)

-- Story
RegistChildComponent(LuaButterflyCrisisMainWnd, "StoryBoard", CUITexture)
RegistChildComponent(LuaButterflyCrisisMainWnd, "FragRoot", GameObject)
RegistChildComponent(LuaButterflyCrisisMainWnd, "FragItem", GameObject)

RegistChildComponent(LuaButterflyCrisisMainWnd, "LockTexture", CUITexture)
RegistChildComponent(LuaButterflyCrisisMainWnd, "LockMask1", GameObject)
RegistChildComponent(LuaButterflyCrisisMainWnd, "LockMask2", GameObject)
RegistChildComponent(LuaButterflyCrisisMainWnd, "LockMask3", GameObject)
RegistChildComponent(LuaButterflyCrisisMainWnd, "LockMask4", GameObject)

RegistChildComponent(LuaButterflyCrisisMainWnd, "ButterflyFx", CUIFx)
RegistChildComponent(LuaButterflyCrisisMainWnd, "ButterflyTexture", GameObject)
RegistChildComponent(LuaButterflyCrisisMainWnd, "BossNameLabel", UILabel)

-- Tabs
RegistChildComponent(LuaButterflyCrisisMainWnd, "TabsGrid", UIGrid)
RegistChildComponent(LuaButterflyCrisisMainWnd, "TabItem", GameObject)
-- 遮罩
RegistChildComponent(LuaButterflyCrisisMainWnd, "Mask", GameObject)

-- {Play = butterflyPlay, IsNPCOpen = isNPCOpen, IsBossSolved = isBossSolved})
RegistClassMember(LuaButterflyCrisisMainWnd, "ButterflyCrisisPlayTabInfos")
-- 礼包信息
RegistClassMember(LuaButterflyCrisisMainWnd, "GiftLimits")
RegistClassMember(LuaButterflyCrisisMainWnd, "TabItemList")
RegistClassMember(LuaButterflyCrisisMainWnd, "CurrentTabIndex")

-- 奖励相关
RegistChildComponent(LuaButterflyCrisisMainWnd, "RewardGrid", UIGrid)
RegistChildComponent(LuaButterflyCrisisMainWnd, "RewardScrollView", CUIRestrictScrollView)
RegistChildComponent(LuaButterflyCrisisMainWnd, "RewardItem", GameObject)
RegistChildComponent(LuaButterflyCrisisMainWnd, "RMBBtn", GameObject)
RegistChildComponent(LuaButterflyCrisisMainWnd, "RMBFx", CUIFx)

RegistClassMember(LuaButterflyCrisisMainWnd, "RewardItemList")

-- 用于实现动画
RegistClassMember(LuaButterflyCrisisMainWnd, "UpdateLockTextureMode")
RegistClassMember(LuaButterflyCrisisMainWnd, "UpdateAniStart")
RegistClassMember(LuaButterflyCrisisMainWnd, "UpdateAniEnd")
RegistClassMember(LuaButterflyCrisisMainWnd, "UpdateCurentAni")
RegistClassMember(LuaButterflyCrisisMainWnd, "UpdateAniInterval")
RegistClassMember(LuaButterflyCrisisMainWnd, "UpdateAniSpeed")
RegistClassMember(LuaButterflyCrisisMainWnd, "AniStartTime")

RegistClassMember(LuaButterflyCrisisMainWnd, "UpdateAniNPCIdx")
RegistClassMember(LuaButterflyCrisisMainWnd, "UpdateAniMemId")

--@endregion

function LuaButterflyCrisisMainWnd:Awake()
    self.Mask:SetActive(false)
    self.TabItem:SetActive(false)
    self.TabItemList = {}
    self.ButterflyCrisisPlayTabInfos = {}
    self.GiftLimits = {}

    self.CurrentTabIndex = 0

    self.RewardItem:SetActive(false)
    self.RewardItemList = {}

    self.FragItem:SetActive(false)
    self.ButterflyFx:DestroyFx()
    self.RMBFx:DestroyFx()
    self.ButterflyTexture:SetActive(false)

    self.UpdateLockTextureMode = false
    self.UpdateAniInterval = 0.02
    self.UpdateAniSpeed = 0.007
    self.UpdateAniStart = 0
    self.UpdateCurentAni = 0
    self.UpdateAniEnd = 0
    self.AniStartTime = 0
    self.UpdateAniNPCIdx = 0
    self.UpdateAniMemId = 0
    self.UpdateAniNPCIdx = 0

    Gac2Gas.RequestPlayerItemLimit(EShopMallRegion_lua.ELingyuMallLimit)
end

function LuaButterflyCrisisMainWnd:Init()

    self:UpdateHeader()

    CommonDefs.AddOnClickListener(self.MoreInfo, DelegateFactory.Action_GameObject(function (go)
        g_MessageMgr:ShowMessage("Butterfly_Crisis_Tip")
    end), false)

    CommonDefs.AddOnClickListener(self.RMBBtn, DelegateFactory.Action_GameObject(function(go)
        CUIManager.ShowUI(CLuaUIResources.ButterflyCrisisRMBWnd)
    end), false)

    -- 要处理成原来的已经打开的index
    self:UpdateTabs()
    -- 更新奖励信息
    self:UpdateReward()

    local scaleId = ButterflyCrisis_Setting.GetData().ButterflyScalesId
    local num = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, scaleId)
    if num>0 then
        if not COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.ButterflyCrisis) then
            self:OnTabClicked(1)
            CLuaGuideMgr.TryTriggerButterflyCrisis1Guide()
        end
    end
end

function LuaButterflyCrisisMainWnd:UpdateHeader()
    -- ButterflyScalesId
    local scaleId = ButterflyCrisis_Setting.GetData().ButterflyScalesId
    local num = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, scaleId)
    self.ButterflyScalesLabel.text = SafeStringFormat3(LocalString.GetString("[ACF9FF]拥有白蝶鳞片[-] %s"), tostring(num))
end

--@region NPC Tab处理

--@desc 更新NPC Tab信息
function LuaButterflyCrisisMainWnd:UpdateTabs()
    self:ProcessPlayTabInfos()

    CUICommonDef.ClearTransform(self.TabsGrid.transform)
    self.TabItemList = {}

    for i = 1, #self.ButterflyCrisisPlayTabInfos do
        local info = self.ButterflyCrisisPlayTabInfos[i]
        local go = NGUITools.AddChild(self.TabsGrid.gameObject, self.TabItem)
        self:InitTabItem(go, info.Play, info.IsNPCOpen, info.IsBossSolved, i)
        go:SetActive(true)
        table.insert(self.TabItemList, go)
    end

    self.TabsGrid:Reposition()

    -- 选中CurrentTabIndex
    if LuaButterflyCrisisMgr.m_SelectedNPCIdx ~= 0 then
        self:OnTabClicked(LuaButterflyCrisisMgr.m_SelectedNPCIdx)
        LuaButterflyCrisisMgr.m_SelectedNPCIdx = 0
    end

    self:UpdateTabSelections()
end

--@desc 处理白蝶风波Tab相关的信息
function LuaButterflyCrisisMainWnd:ProcessPlayTabInfos()
    self.ButterflyCrisisPlayTabInfos = {}

    ButterflyCrisis_Play.ForeachKey(function (key)
        local butterflyPlay = ButterflyCrisis_Play.GetData(key)
        if butterflyPlay then
            local isNPCOpen = LuaButterflyCrisisMgr.m_NPCOpenInfo[key]
            local isBossSolved = LuaButterflyCrisisMgr.m_BossSolvedInfo[key]
            table.insert(self.ButterflyCrisisPlayTabInfos, {Play = butterflyPlay, IsNPCOpen = isNPCOpen, IsBossSolved = isBossSolved})
        end
    end)
end


--@desc: 初始化Tab
--@item: Item对象
--@butterflyPlay: 玩法信息, ButterFlyCrisis_Play
--@isUnlocked: NPC是否解锁, bool
--@isFinished: Boss是否通关, bool
--@index: Tab的Index
function LuaButterflyCrisisMainWnd:InitTabItem(item, butterflyPlay, isUnlocked, isFinished, index)
    if not item or not butterflyPlay then return end

    local portrait = item.transform:Find("Portrait"):GetComponent(typeof(CUITexture))
    portrait:Clear()
    local lock = item.transform:Find("Lock").gameObject
    lock:SetActive(false)
    local selectedSprite = item.transform:Find("Selected").gameObject
    selectedSprite:SetActive(false)

    local npc = NPC_NPC.GetData(butterflyPlay.NpcTemplateId)
    portrait:LoadNPCPortrait(npc.Portrait)

    if isUnlocked then
        Extensions.SetLocalPositionZ(item.transform, 0)
        portrait.gameObject:SetActive(true)
        lock:SetActive(false)

        if isFinished then
            item:GetComponent(typeof(UISprite)).alpha = 1
        else
            item:GetComponent(typeof(UISprite)).alpha = 0.7
        end
    else
        Extensions.SetLocalPositionZ(item.transform, -1)
        portrait.gameObject:SetActive(false)
        lock:SetActive(true)
    end

    local onTabClicked = function (go)
        if not isUnlocked then
            g_MessageMgr:ShowMessage("Butterfly_Crisis_NPC_Not_Open")
        else
            self:OnTabClicked(index)
        end
    end
    CommonDefs.AddOnClickListener(item, DelegateFactory.Action_GameObject(onTabClicked), false)
end

--@desc: 处理Tab的点击事件
--@index: starts from 1
function LuaButterflyCrisisMainWnd:OnTabClicked(index)

    if self.CurrentTabIndex ~= index then
        Gac2Gas.ButterflyPlayerReadNpc(index)
        self.CurrentTabIndex = index
        self:UpdateTabSelections()
        self:UpdateMainStory(index, true)
    end
end

--@desc 处理Tab的选中显示
function LuaButterflyCrisisMainWnd:UpdateTabSelections()

    if not self.TabItemList then return end
    for i = 1, #self.TabItemList do
        self:SetTabItemSelected(self.TabItemList[i], i == self.CurrentTabIndex)
    end
end

--@desc 显示/隐藏某个Tab的选中效果
--@item: TabItem
--@isSelected: 是否被选中
function LuaButterflyCrisisMainWnd:SetTabItemSelected(item, isSelected)
    if not item then return end
    local selectedSprite = item.transform:Find("Selected").gameObject
    selectedSprite:SetActive(isSelected)
end

--@endregion

--@desc 展示故事界面
--@index: 对应NPCIdx
--@updateLock: 是否在方法里更新遮盖图
function LuaButterflyCrisisMainWnd:UpdateMainStory(index, updateLock)
    local info = self.ButterflyCrisisPlayTabInfos[index]
    if not info then return end

    if info.IsBossSolved then
        self.StoryBoard:LoadMaterial(info.Play.EndFigure)
    else
        self.StoryBoard:LoadMaterial(info.Play.NormalFigure)
    end

    local npc = NPC_NPC.GetData(info.Play.NpcTemplateId)
    if npc then
        self.BossNameLabel.text = npc.Name
    else
        self.BossNameLabel.text = nil
    end
    local isPreBossSolved = true
    if self.ButterflyCrisisPlayTabInfos[index-1] then
        isPreBossSolved = self.ButterflyCrisisPlayTabInfos[index-1].IsBossSolved
    end
    
    self:ShowFrags(info.Play, info.IsBossSolved, isPreBossSolved)
    if updateLock then
        self:ShowLockMasks(info, index)
    end
    self:ShowButterflyFx(info, index)
end

--@desc 展示记忆按钮
function LuaButterflyCrisisMainWnd:ShowFrags(butterflyPlay, isBossSolved, isPreBossSolved)
    if not isBossSolved then
        -- 生成4个frag
        CUICommonDef.ClearTransform(self.FragRoot.transform)

        local memLockInfo = LuaButterflyCrisisMgr.m_MemoryUnlockInfo[self.CurrentTabIndex]
        if not memLockInfo then return end

        local scaleId = ButterflyCrisis_Setting.GetData().ButterflyScalesId
        local num = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, scaleId)
        local needNum = ButterflyCrisis_Setting.GetData().NumOfButterflyScales

        local show = true
        for i = 1, 4 do
            local frag = NGUITools.AddChild(self.FragRoot.gameObject, self.FragItem)
        
            local isUnlock = memLockInfo[i]
            local canBeUnlock = not isUnlock and (num >= needNum) and isPreBossSolved

            self:InitFragItem(frag, show, isUnlock, canBeUnlock, false)
            frag.name = tostring(i)
            local tempShow = show
            CommonDefs.AddOnClickListener(frag, DelegateFactory.Action_GameObject(function (go)
                if isUnlock then
                    local strName = SafeStringFormat3("Story%s", tostring(i))
                    local str = butterflyPlay[strName]
                    LuaButterflyCrisisMgr.ShowStoryWithPos(str, frag.transform.position)
                elseif tempShow then
                    if canBeUnlock then
                        -- 请求解锁
                        Gac2Gas.ButterflyUnlockMemorySegment(self.CurrentTabIndex, i)
                    else
                        if not isPreBossSolved then
                            local prePlay = ButterflyCrisis_Play.GetData(self.CurrentTabIndex-1)
                            local npc = NPC_NPC.GetData(prePlay.NpcTemplateId)
                            g_MessageMgr:ShowMessage("BUTTERFLY_MEMORY_NEED_SAVE_PREV", npc.Name)
                        else 
                            g_MessageMgr:ShowMessage("Butterfly_Crisis_Scales_Not_Enough")
                        end
                        
                    end
                end
            end), false)
            frag.transform.localPosition = Vector3(butterflyPlay.FragPosition[(i-1)*2], butterflyPlay.FragPosition[(i-1)*2+1], 0)
            show = show and isUnlock
        end
    else
        -- 只有一个frag
        CUICommonDef.ClearTransform(self.FragRoot.transform)
        local frag = NGUITools.AddChild(self.FragRoot.gameObject, self.FragItem)

        local canBeUnlock = LuaButterflyCrisisMgr.IsFirstTimeBossSolved(self.CurrentTabIndex)
        self:InitFragItem(frag, true, true, canBeUnlock, true)
        local str = butterflyPlay.EndStory
        CommonDefs.AddOnClickListener(frag, DelegateFactory.Action_GameObject(function (go)
            local alert = go.transform:Find("FragAlert").gameObject
            if alert then
                alert:SetActive(false)
            end

            LuaButterflyCrisisMgr.ShowStoryWithPos(str, frag.transform.position)
            LuaButterflyCrisisMgr.UpdateSolvedBossInfo(self.CurrentTabIndex)
            Gac2Gas.RequestRMBGiftPrivilege(self.CurrentTabIndex)
        end), false)
        -- 读取 butterflyPlay.FragPosition
        if butterflyPlay.FragPosition.Length > 9 then
            frag.transform.localPosition = Vector3(butterflyPlay.FragPosition[8], butterflyPlay.FragPosition[9], 0)
        else
            frag.transform.localPosition = Vector3(960, 540, 0)
        end
    end
    
end

--@desc 创建解锁记忆的按钮
--@frag: 按钮对象
--@show: 是否显示按钮
--@isUnlock: 是否解锁
--@canBeUnlock: 是否可以被解锁
--@isEndStory: 是否是通关后的展示
function LuaButterflyCrisisMainWnd:InitFragItem(frag, show, isUnlock, canBeUnlock, isEndStory)
    if not frag then return end

    frag:SetActive(show)

    local unlock = frag.transform:Find("Unlock").gameObject
    local lock = frag.transform:Find("Lock").gameObject
    local alert = frag.transform:Find("FragAlert").gameObject
    alert:SetActive(false)

    unlock:SetActive(isUnlock)
    lock:SetActive(not isUnlock)
    alert:SetActive(((not isUnlock) or isEndStory) and canBeUnlock)
end

--@desc 显示展示记忆解锁遮罩，包括蝴蝶的显示
function LuaButterflyCrisisMainWnd:ShowLockMasks(info, index)

    local finalUnlock = 0

    if info.IsBossSolved then
        self.LockMask1:SetActive(false)
        self.LockMask2:SetActive(false)
        self.LockMask3:SetActive(false)
        self.LockMask4:SetActive(false)

        finalUnlock = 4
    else
        local memUnlockInfo = LuaButterflyCrisisMgr.m_MemoryUnlockInfo[self.CurrentTabIndex]

        
        for i = 1, 4 do
            local name = SafeStringFormat3("LockMask%s", tostring(i))
            self[name]:SetActive(not memUnlockInfo[i])
            if memUnlockInfo[i] and i > finalUnlock then
                finalUnlock = i
            end
        end
    end

    local lockName = SafeStringFormat3("UI/Texture/Transparent/Material/raw_whitebutterfly_0%s.mat", tostring(index))
    self.LockTexture:LoadMaterial(lockName, false, DelegateFactory.Action_bool(function (completeCb)
        self:UpdateLockTexture(finalUnlock / 4)
    end))

end

function LuaButterflyCrisisMainWnd:ShowButterflyFx(info, index)
    self.ButterflyFx:DestroyFx()
    self.ButterflyFx.gameObject:SetActive(false)

    if info.IsBossSolved then
        self.ButterflyTexture:SetActive(false)

        if LuaButterflyCrisisMgr.IsFirstTimeBossSolved(index) then
            self.ButterflyFx.transform.localPosition = Vector3(info.Play.ButterflyFxPosition[0], info.Play.ButterflyFxPosition[1], 0)
            self.ButterflyFx:DestroyFx()
            self.ButterflyFx.gameObject:SetActive(true)
            self.ButterflyFx:LoadFx("Fx/UI/Prefab/UI_baidie_feixing.prefab")
        end
        UIEventListener.Get(self.ButterflyFx.gameObject).onClick = nil
    else
        local memUnlockInfo = LuaButterflyCrisisMgr.m_MemoryUnlockInfo[self.CurrentTabIndex]
        local isAllUnlocked = true
        
        for i = 1, 4 do
            isAllUnlocked = isAllUnlocked and memUnlockInfo[i]
        end

        
        if isAllUnlocked then
            self.ButterflyFx.transform.localPosition = Vector3(info.Play.ButterflyFxPosition[0], info.Play.ButterflyFxPosition[1], 0)
            self.ButterflyFx:DestroyFx()
            self.ButterflyFx.gameObject:SetActive(true)
            self.ButterflyFx:LoadFx("Fx/UI/Prefab/UI_baidie_tingzhu.prefab")
            self.ButterflyTexture:SetActive(false)

            CommonDefs.AddOnClickListener(self.ButterflyFx.gameObject, DelegateFactory.Action_GameObject(function ()
                Gac2Gas.RequestEnterButterflyCrisisPlay(self.CurrentTabIndex)
            end), false)

            CLuaGuideMgr.TryTriggerButterflyCrisis2Guide()
        else
            self.ButterflyFx:DestroyFx()
            self.ButterflyTexture.transform.localPosition = Vector3(info.Play.ButterflyPosition[0], info.Play.ButterflyPosition[1], 0)
            self.ButterflyTexture:SetActive(true)
            UIEventListener.Get(self.ButterflyFx.gameObject).onClick = nil
        end
    end
end

function LuaButterflyCrisisMainWnd:UpdateLockTexture(delta)
    if self.LockTexture.texture and self.LockTexture.texture.material then
        self.LockTexture.texture.enabled = false
        self.LockTexture.texture.material:SetFloat("_Percent", delta)
        self.LockTexture.texture.enabled = true
    end
end
--@region 奖励处理

--@desc 更新奖励信息
function LuaButterflyCrisisMainWnd:UpdateReward()
    CUICommonDef.ClearTransform(self.RewardGrid.transform)
    self.RewardItemList = {}

    local rewardInfos = {}
    ButterflyCrisis_Reward.ForeachKey(DelegateFactory.Action_object(function (key)
        local butterflyReward = ButterflyCrisis_Reward.GetData(key)
        if butterflyReward then
            -- refs #137576 调整奖励解锁的方式，改为解锁过多少东西就按顺序显示多少
            --[[local npcIdx = math.floor((key - 1) / 5) + 1 -- [1,5]
            local memId = (key - 1) % 5 + 1 -- 1-4代表记忆是否解锁，5代表boss是否被打败--]]
            local isUnlock = self:IsRewardUnlock(key)
            local isTaken = LuaButterflyCrisisMgr.m_RewardTakenInfos[key]
            table.insert(rewardInfos, {key = key, butterflyReward = butterflyReward, isUnlock = isUnlock, isTaken = isTaken})
        end
    end))

    -- 进行排序
    --[[table.sort(rewardInfos, function (a, b)
        -- 解锁的放在没解锁的前面
        -- 可领取的放在最前面
        if a.isUnlock and not b.isUnlock then
            return true
        elseif not a.isUnlock and b.isUnlock then
            return false
        elseif not a.isTaken and b.isTaken then
            return true
        elseif a.isTaken and not b.isTaken then
            return false
        else
            return a.key < b.key
        end
    end)--]]

    local firstUntakenItem = nil
    
    for i = 1, #rewardInfos do
        local info = rewardInfos[i]
        local go = NGUITools.AddChild(self.RewardGrid.gameObject, self.RewardItem)
        local butterflyReward = info.butterflyReward
        local isUnlock = info.isUnlock
        local isTaken = info.isTaken
        self:InitRewardItem(go, butterflyReward, isUnlock, isTaken)
        go:SetActive(true)
        if not firstUntakenItem and isUnlock and not isTaken then
            firstUntakenItem = go
        end
        table.insert(self.RewardItemList, go)
    end

    self.RewardGrid:Reposition()
    if firstUntakenItem then
        CUICommonDef.SetFullyVisible(firstUntakenItem, self.RewardScrollView)
    end
    
end

--@desc 对应奖励是否解锁
--@npcIdx: [1, 5]
--@memId: [1, 5], 1-4代表记忆是否解锁，5代表boss是否被打败
function LuaButterflyCrisisMainWnd:IsRewardUnlock(key)
    if not LuaButterflyCrisisMgr.m_MemoryUnlockInfo or not LuaButterflyCrisisMgr.m_BossSolvedInfo then return false end

    local totalDone = 0

    for i = 1, #LuaButterflyCrisisMgr.m_MemoryUnlockInfo do
        local info = LuaButterflyCrisisMgr.m_MemoryUnlockInfo[i]
        for j = 1, #info do
            if info[j] then
                totalDone = totalDone + 1
            end
        end
    end

    for i = 1, #LuaButterflyCrisisMgr.m_BossSolvedInfo do
        if LuaButterflyCrisisMgr.m_BossSolvedInfo[i] then
            totalDone = totalDone + 1
        end
    end

    return key <= totalDone
end

--@desc 初始化奖励item
--@item: 奖励item
--@reward: ButterflyCrisis_Reward
--@isUnlocked: 奖励是否解锁
--@isTaken: 奖励是否领取
function LuaButterflyCrisisMainWnd:InitRewardItem(item, reward, isUnlocked, isTaken)
    if not item or not reward then return end

    local iconTexture = item.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    iconTexture:Clear()

    local qualitySprite = item.transform:Find("QualitySprite"):GetComponent(typeof(UISprite))
    qualitySprite.spriteName = nil

    local amountLabel = item.transform:Find("AmountLabel"):GetComponent(typeof(UILabel))
    amountLabel.text = nil

    local selectedSprite = item.transform:Find("SelectedSprite").gameObject
    selectedSprite:SetActive(false)

    local fx = item.transform:Find("Fx"):GetComponent(typeof(CUIFx))
    fx.ScrollView = self.RewardScrollView
    fx:DestroyFx()

    local taken = item.transform:Find("Taken").gameObject
    taken:SetActive(false)

    local itemId = reward.RewardShowItemId[0]
    local count = reward.RewardShowItemId[1]

    local itemTemplate = Item_Item.GetData(itemId)
    if not itemTemplate then return end

    iconTexture:LoadMaterial(itemTemplate.Icon)
    amountLabel.text = tostring(count)
    qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(itemTemplate, nil, false)

    if isUnlocked then
        Extensions.SetLocalPositionZ(item.transform, 0)
        taken:SetActive(isTaken)
        if not isTaken then
            -- 展示待领取特效
            fx:LoadFx(CUIFxPaths.CommonYellowLineFxPath)
            LuaTweenUtils.DOKill(fx.FxRoot, false)
            local path={ Vector3(-49,49,0),Vector3(49,49,0),Vector3(49,-49,0),Vector3(-49,-49,0),Vector3(-49,49,0)}
            local array = Table2Array(path, MakeArrayClass(Vector3))
            LuaUtils.SetLocalPosition(fx.FxRoot,-49,49,0)
            LuaTweenUtils.DODefaultLocalPath(fx.FxRoot,array,2)
        end
    else
        Extensions.SetLocalPositionZ(item.transform, -1)
    end

    CommonDefs.AddOnClickListener(item, DelegateFactory.Action_GameObject(function (go)
        if isUnlocked and not isTaken then
            -- 领取
            Gac2Gas.ButterflyPlayerCollectReward(reward.ID)
        else
            CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, CItemInfoMgr.AlignType.Default, 0, 0, 0, 0)
        end
    end), false)
end

--@endregion


--@region 广播更新处理

function LuaButterflyCrisisMainWnd:UpdateButterflyCrisisMainWnd()
    -- 只有切换时才会整体刷新
    if self.CurrentTabIndex ~= LuaButterflyCrisisMgr.m_SelectedNPCIdx then
        self:OnTabClicked(LuaButterflyCrisisMgr.m_SelectedNPCIdx)
    end
end

--@desc 更新记忆解锁的情况
function LuaButterflyCrisisMainWnd:UpdateMemoryUnlockInfo(npcIdx, memId)
    if self.CurrentTabIndex ~= npcIdx then return end

    local play = ButterflyCrisis_Play.GetData(npcIdx)
    if not play then return end

    self:UpdateMainStory(npcIdx, false)
    self:UnlockMemoryAni(npcIdx, memId)
    self:UpdateReward()
end

--@desc 解锁记忆对应的动画效果
function LuaButterflyCrisisMainWnd:UnlockMemoryAni(npcIdx, memId)
    self.UpdateAniStart = (memId - 1 ) / 4
    self.UpdateCurentAni = self.UpdateAniStart
    self.UpdateAniEnd = memId / 4
    self.UpdateLockTextureMode = true
    self.Mask:SetActive(true)

    self.UpdateAniNPCIdx = npcIdx
    self.UpdateAniMemId = memId
end


function LuaButterflyCrisisMainWnd:Update()
    if not self.UpdateLockTextureMode then return end

    local delta = Time.realtimeSinceStartup - self.AniStartTime
    if delta > self.UpdateAniInterval then

        self.UpdateCurentAni = self.UpdateCurentAni + self.UpdateAniSpeed
        self.AniStartTime = Time.realtimeSinceStartup
        self:UpdateLockTexture(self.UpdateCurentAni)
        if self.UpdateCurentAni >= self.UpdateAniEnd then 
            self.UpdateLockTextureMode = false
            self.Mask:SetActive(false)
            self:ShowStoryWithPos()
        end
    end
end


function LuaButterflyCrisisMainWnd:ShowStoryWithPos()
    local childCount = self.FragRoot.transform.childCount
    if self.UpdateAniMemId <= childCount and self.UpdateAniMemId > 0 then
        local frag = self.FragRoot.transform:GetChild(self.UpdateAniMemId-1)

        if frag then
            local butterflyPlay = ButterflyCrisis_Play.GetData(self.UpdateAniNPCIdx)
            if butterflyPlay then
                local strName = SafeStringFormat3("Story%s", tostring(self.UpdateAniMemId))
                local str = butterflyPlay[strName]
                LuaButterflyCrisisMgr.ShowStoryWithPos(str, frag.transform.position)
            end
        end
    end
end

--@desc 更新BOSS打败的情况
function LuaButterflyCrisisMainWnd:UpdateBossSolvedInfo(npcIdx)
    if self.CurrentTabIndex ~= npcIdx then return end

    self.ButterflyFx:DestroyFx()
    self.ButterflyFx:LoadFx("Fx/UI/Prefab/UI_baidie_feixing.prefab")
    self:UpdateTabs()
    self:UpdateMainStory(self.CurrentTabIndex, true)
    self:UpdateReward()

end

--@ 更新购买按钮
function LuaButterflyCrisisMainWnd:UpdateRMBBtnStatus(args)
    local regionId = args[0]
    local limitInfo = args[1]

    self.GiftLimits = {}
    for i = 0 , limitInfo.Count-1, 2 do
        local key = math.floor(tonumber(limitInfo[i] or 0))
        local val = math.floor(tonumber(limitInfo[i + 1] or 0))
        self.GiftLimits[key] = val
    end

    self.RMBPackageInfos = {}

    -- 获得礼包信息
    local needNotify = false

    ButterflyCrisis_RMB.ForeachKey(DelegateFactory.Action_object(function (key)
        local rmbInfo = ButterflyCrisis_RMB.GetData(key)
        if rmbInfo then
            -- isOpen: NPC是否对外开放, isFinished: 是否通关, isBought: 是否购买过
            -- 打开RMB礼包界面的时候，应该白蝶风波主界面是开的，所以有相关Boss信息
            local isOpen = LuaButterflyCrisisMgr.m_NPCOpenInfo[key]
            local isFinished = LuaButterflyCrisisMgr.m_BossSolvedInfo[key]
            local isBought = self:IsRMBPackageBought(rmbInfo.ItemId)
            if isOpen and isFinished and not isBought then
                needNotify = true
            end
        end
    end))

    self.RMBFx:DestroyFx()
    if needNotify then
        self.RMBFx:LoadFx(CUIFxPaths.CommonYellowLineFxPath)
        LuaTweenUtils.DOKill(self.RMBFx.FxRoot, false)
        local path={ Vector3(-100,60,0),Vector3(100,60,0),Vector3(100,-100,0),Vector3(-100,-100,0),Vector3(-100,60,0)}
        local array = Table2Array(path, MakeArrayClass(Vector3))
        LuaUtils.SetLocalPosition(self.RMBFx.FxRoot,-100,60,0)
        LuaTweenUtils.DODefaultLocalPath(self.RMBFx.FxRoot,array,4)
    end
end

function LuaButterflyCrisisMainWnd:IsRMBPackageBought(itemId)
    if not self.GiftLimits or not itemId then return false end
    if not self.GiftLimits[itemId] then return false end
    return self.GiftLimits[itemId] <= 0
end

--@endregion


function LuaButterflyCrisisMainWnd:OnEnable()
    CShopMallMgr.Inst:EnableEvent()
    g_ScriptEvent:AddListener("SendItem", self, "UpdateHeader")
    g_ScriptEvent:AddListener("SetItemAt", self, "UpdateHeader")
    g_ScriptEvent:AddListener("UpdateButterflyCrisisMainWnd", self, "UpdateButterflyCrisisMainWnd")
    g_ScriptEvent:AddListener("UpdateMemoryUnlockInfo", self, "UpdateMemoryUnlockInfo")
    g_ScriptEvent:AddListener("UpdateBossSolvedInfo", self, "UpdateBossSolvedInfo")
    g_ScriptEvent:AddListener("ButterflyCollectRewardSuccess", self, "UpdateReward")
    g_ScriptEvent:AddListener("SendMallMarketItemLimitUpdate", self, "UpdateRMBBtnStatus")
    g_ScriptEvent:AddListener("AutoShangJiaItemUpdate", self, "UpdateRMBBtnStatus")
end

function LuaButterflyCrisisMainWnd:OnDisable()
    CShopMallMgr.Inst:DisableEvent()
    g_ScriptEvent:RemoveListener("SendItem", self, "UpdateHeader")
    g_ScriptEvent:RemoveListener("SetItemAt", self, "UpdateHeader")
    g_ScriptEvent:RemoveListener("UpdateButterflyCrisisMainWnd", self, "UpdateButterflyCrisisMainWnd")
    g_ScriptEvent:RemoveListener("UpdateMemoryUnlockInfo", self, "UpdateMemoryUnlockInfo")
    g_ScriptEvent:RemoveListener("UpdateBossSolvedInfo", self, "UpdateBossSolvedInfo")
    g_ScriptEvent:RemoveListener("ButterflyCollectRewardSuccess", self, "UpdateReward")
    g_ScriptEvent:RemoveListener("SendMallMarketItemLimitUpdate", self, "UpdateRMBBtnStatus")
    g_ScriptEvent:RemoveListener("AutoShangJiaItemUpdate", self, "UpdateRMBBtnStatus")
end

function LuaButterflyCrisisMainWnd:GetGuideGo(methodName)
    if methodName=="GetFragItem" then
        for i=1,self.FragRoot.transform.childCount do
            local tf = self.FragRoot.transform:GetChild(i-1)
            if tf.gameObject.activeSelf then
                return tf.gameObject
            end
        end
        CGuideMgr.Inst:EndCurrentPhase()
    elseif methodName=="GetButterfly" then
        return self.ButterflyFx.gameObject
    end
end
