local QnSelectableButton = import "L10.UI.QnSelectableButton"
local Extensions = import "Extensions"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local CommonDefs = import "L10.Game.CommonDefs"
local CEquipmentProcessMgr = import "L10.Game.CEquipmentProcessMgr"
local UIScrollView=import "UIScrollView"
local UITable=import "UITable"
local CUITexture=import "L10.UI.CUITexture"
local CItemMgr = import "L10.Game.CItemMgr"
local CItem = import "L10.Game.CItem"

CLuaGemUpdateScrollView = class()
RegistClassMember(CLuaGemUpdateScrollView,"scrollview")
RegistClassMember(CLuaGemUpdateScrollView,"table")
RegistClassMember(CLuaGemUpdateScrollView,"stoneTemplate")
RegistClassMember(CLuaGemUpdateScrollView,"stoneList")
RegistClassMember(CLuaGemUpdateScrollView,"stoneButtonList")
RegistClassMember(CLuaGemUpdateScrollView,"StoneSelected")
RegistClassMember(CLuaGemUpdateScrollView,"SelectHoleIndex")

CLuaGemUpdateScrollView.SelectHoleInfo = nil
function CLuaGemUpdateScrollView:Awake()
    self.scrollview = self.transform:Find("scrollview"):GetComponent(typeof(UIScrollView))
    self.table = self.transform:Find("scrollview/table"):GetComponent(typeof(UITable))
    self.stoneTemplate = self.transform:Find("scrollview/GemTemplate").gameObject
    self.stoneList = {}
    self.stoneButtonList = {}
    self.StoneSelected = nil
    self.SelectHoleIndex = 0

end

function CLuaGemUpdateScrollView:Init( )
    Extensions.RemoveAllChildren(self.table.transform)
    self.stoneTemplate:SetActive(false)
    self.stoneButtonList={}



    self.stoneList={}
    local item = CItemMgr.Inst:GetById(CEquipmentProcessMgr.Inst.SelectEquipment.itemId)
    
    local Hole = item.Equip.Hole
    local holeItems = {}
    if CLuaGemUpgradeWnd.m_HoleSetIndex<=1 then
        for i=1,Hole do
            table.insert( holeItems,item.Equip.HoleItems[i] )
        end
    else
        for i=1,Hole do
            table.insert( holeItems,item.Equip.SecondaryHoleItems[i] )
        end
    end

    local referenceIndex = {}
    for i=1,Hole do
        local jewelId = item.Equip.SecondaryHoleItems[i]
        if jewelId>0 and jewelId<100 then
            referenceIndex[item.Equip.HoleItems[jewelId]] = true
            referenceIndex[jewelId] = true
        end
    end

    local hasSelectGem = false
    local selectedHoleIndex = 0
    local selectedHoleId = 0
    if CLuaGemUpdateScrollView.SelectHoleInfo ~= nil then--选中的那个放第一个位置
        selectedHoleIndex = CLuaGemUpdateScrollView.SelectHoleInfo.holeIndex
        selectedHoleId = holeItems[selectedHoleIndex] or 0
        local stone = nil
        local isReference = referenceIndex[selectedHoleId]
        if selectedHoleId>0 and selectedHoleId<100 then
            selectedHoleId = item.Equip.HoleItems[selectedHoleId]
        end
        stone = CItemMgr.Inst:GetItemTemplate(selectedHoleId)

        if stone ~= nil then
            hasSelectGem = true
            local stoneGo = NGUITools.AddChild(self.table.gameObject, self.stoneTemplate)
            stoneGo:SetActive(true)

            local button = CommonDefs.GetComponent_GameObject_Type(stoneGo, typeof(QnSelectableButton))
            button.OnClick = DelegateFactory.Action_QnButton(function(btn)
                self:OnStoneSelected(btn)
            end)
            table.insert( self.stoneButtonList,button )
            button:SetSelected(false, false)

            local holeInfo = {
                holeIndex = selectedHoleIndex,
                stoneItemId = holeItems[selectedHoleIndex],
            }
            table.insert( self.stoneList, holeInfo )
            self:InitGemItem(stoneGo.transform,selectedHoleId,isReference)
        end
    end

    for i=1,Hole do
        if holeItems[i]>0 then
            if hasSelectGem and selectedHoleIndex == i then
            else
                local tempId = holeItems[i]
                local isReference = referenceIndex[tempId]
                if tempId<100 then
                    tempId = item.Equip.HoleItems[tempId]
                end

                local stoneGo = NGUITools.AddChild(self.table.gameObject, self.stoneTemplate)
                stoneGo:SetActive(true)
                local button = stoneGo:GetComponent(typeof(QnSelectableButton))
                button.OnClick = DelegateFactory.Action_QnButton(function(btn)
                    self:OnStoneSelected(btn)
                end)
                button:SetSelected(false, false)

                table.insert( self.stoneButtonList,button )

                local holeInfo = {
                    holeIndex = i,
                    stoneItemId = holeItems[i],
                }

                table.insert( self.stoneList,holeInfo )
                self:InitGemItem(stoneGo.transform,tempId,isReference)
            end
        end
    end

    self.table:Reposition()
    if #self.stoneButtonList > 0 then
        self.stoneButtonList[0+1]:SetSelected(true, false)
    end
    if #self.stoneList > 0 then
        self.StoneSelected(self.stoneList[1])
    end
    CLuaEquipMgr.SelectedEquipmentHoleInfo = self.stoneList[0+1]
end
function CLuaGemUpdateScrollView:OnStoneSelected( button) 
    for i,v in ipairs(self.stoneButtonList) do
        v:SetSelected(button == v, false)
        if button == v then
            if self.StoneSelected then
                self.StoneSelected(self.stoneList[i])
            end

            CLuaEquipMgr.SelectedEquipmentHoleInfo = self.stoneList[i]
            EventManager.Broadcast(EnumEventType.OnSelectGemChangedInGemUpgradeWnd)
            self.SelectHoleIndex = i-1
        end
    end
end

function CLuaGemUpdateScrollView:OnEnable()
    g_ScriptEvent:AddListener("SendItem",self,"OnSendItem")
end
function CLuaGemUpdateScrollView:OnDisable()
    g_ScriptEvent:RemoveListener("SendItem",self,"OnSendItem")

    CLuaGemUpdateScrollView.SelectHoleInfo=nil
end
function CLuaGemUpdateScrollView:OnSendItem(args)
    self:Init()
    if self.SelectHoleIndex>=0 and self.SelectHoleIndex<#self.stoneButtonList then
        self:OnStoneSelected(self.stoneButtonList[self.SelectHoleIndex+1])
    end
end

function CLuaGemUpdateScrollView:InitGemItem(transform,templateId,isReference)
    local iconTexture = transform:Find("stoneTemplate/icon"):GetComponent(typeof(CUITexture))
    local nameLabel = transform:Find("Name"):GetComponent(typeof(UILabel))
    local levelLabel = transform:Find("stoneTemplate/Level"):GetComponent(typeof(UILabel))
    local shareMark = transform:Find("ShareMarkSprite").gameObject
    iconTexture.material = nil
    nameLabel.text = ""
    levelLabel.text = ""

    if isReference then
        shareMark:SetActive(true)
    else
        shareMark:SetActive(false)
    end

    local jewel = Jewel_Jewel.GetData(templateId)
    local item = CItemMgr.Inst:GetItemTemplate(templateId)
    if jewel ~= nil and item ~= nil then
        iconTexture:LoadMaterial(item.Icon)
        nameLabel.text = jewel.JewelName
        nameLabel.color = CItem.GetColor(templateId)
        levelLabel.text = SafeStringFormat3(LocalString.GetString("%d级"), jewel.JewelLevel)
    end
end
