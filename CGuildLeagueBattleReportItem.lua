-- Auto Generated!!
local CGuildLeagueBattleReportItem = import "L10.UI.CGuildLeagueBattleReportItem"
local CLogMgr = import "L10.CLogMgr"
local Constants = import "L10.Game.Constants"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local LocalString = import "LocalString"
CGuildLeagueBattleReportItem.m_Init_CS2LuaHook = function (this, info) 
    local dateTime = CServerTimeMgr.ConvertTimeStampToZone8Time(info.time)
    this.timeLabel.text = System.String.Format("{0}-{1}-{2}", dateTime.Year, dateTime.Month, dateTime.Day)
    if info.result == LocalString.GetString("胜利") then
        this.resultSprite.spriteName = Constants.WinSprite
    elseif info.result == LocalString.GetString("失败") then
        this.resultSprite.spriteName = Constants.LoseSprite
    else
        CLogMgr.LogError(LocalString.GetString("异常数据 ") .. info.result)
    end

    this.opponentLabel.text = info.opponent
    this.numLabel.text = info.playerNum
end
