-- Auto Generated!!
local CTalismanBaptizeWordCell = import "L10.UI.CTalismanBaptizeWordCell"
local Extensions = import "Extensions"
CTalismanBaptizeWordCell.m_Init_CS2LuaHook = function (this, word, color,isMax) 

    this.nameLabel.text = ""
    this.descLabel.text = ""
    if word ~= nil then
        this.nameLabel.text = System.String.Format("[{0}]", word.Name)
        this.descLabel.text = word.Description
        if isMax then
            this.descLabel.text = CLuaEquipMgr.WordMaxMark..this.descLabel.text
        end
        this.nameLabel.color = color
        this.descLabel.color = color
        Extensions.SetLocalPositionY(this.background, this.descLabel.transform.localPosition.y - this.descLabel.height - 10)
    end
end
