local Object               = import "System.Object"
local EWeaponVisibleReason = import "L10.Engine.EWeaponVisibleReason"
local EnumGender           = import "L10.Game.EnumGender"
local EnumClass            = import "L10.Game.EnumClass"
local EnumWarnFXType       = import "L10.Game.EnumWarnFXType"
local CEffectMgr           = import "L10.Game.CEffectMgr"
local CUIManager           = import "L10.UI.CUIManager"
local Vector3              = import "UnityEngine.Vector3"
local UIWidget             = import "UIWidget"
local Color                = import "UnityEngine.Color"
local CClientMainPlayer    = import "L10.Game.CClientMainPlayer"
local SubtitileMgr         = import "L10.UI.SubtitileMgr"
local CClientObjectMgr     = import "L10.Game.CClientObjectMgr"
local CClientNpc           = import "L10.Game.CClientNpc"
local CClientOtherPlayer   = import "L10.Game.CClientOtherPlayer"
local CClientPlayerMgr     = import "L10.Game.CClientPlayerMgr"
local CGamePlayMgr         = import "L10.Game.CGamePlayMgr"
local CCommonUIFxWnd       = import "L10.UI.CCommonUIFxWnd"
local EnumHongBaoRainType  = import "L10.UI.EnumHongBaoRainType"
local CRenderScene         = import "L10.Engine.Scene.CRenderScene"
local CScene               = import "L10.Game.CScene"
local CClientObject        = import "L10.Game.CClientObject"

local CBaziCheckMgr      = import "L10.UI.CBaziCheckMgr"
local CItemMgr           = import "L10.Game.CItemMgr"
local CServerTimeMgr     = import "L10.Game.CServerTimeMgr"
local CItemConsumeWndMgr = import "L10.UI.CItemConsumeWndMgr"
local EHideKuiLeiStatus  = import "L10.Engine.EHideKuiLeiStatus"
local CGuideMgr          = import "L10.Game.Guide.CGuideMgr"

LuaWeddingIterationMgr = {}

LuaWeddingIterationMgr.skyboxId = nil -- 天空盒Id
LuaWeddingIterationMgr.isReview = false -- 是否是重温婚礼
LuaWeddingIterationMgr.sceneSelectInfo = {} -- 场景选择信息
LuaWeddingIterationMgr.partnerInfo = nil -- 伴侣信息
LuaWeddingIterationMgr.cachedInvitationInfo = {} -- 缓存请柬设置数据
LuaWeddingIterationMgr.sceneInfo = {} -- 场景中显示的信息
LuaWeddingIterationMgr.aniFxKeys = {} -- 动作特效
LuaWeddingIterationMgr.cardInfo = {} -- 请柬信息

LuaWeddingIterationMgr.stage = -1 -- 婚礼处于哪一阶段
LuaWeddingIterationMgr.stageInfo = {} -- 婚礼阶段额外信息
LuaWeddingIterationMgr.needInviteGuide = true -- 需要触发邀客引导
LuaWeddingIterationMgr.openedGameplay = "" -- 开启玩法界面
LuaWeddingIterationMgr.fakeBrideStage = "" -- 假新娘玩法处于哪一阶段
LuaWeddingIterationMgr.fakeBrideInfo = {} -- 假新娘玩法额外信息
LuaWeddingIterationMgr.promiseCountDown = -1 -- 宣言界面倒计时
LuaWeddingIterationMgr.promiseInfo = {} -- 宣言信息
LuaWeddingIterationMgr.selectWndItem = "" -- 选择酒席、烟花或是撒糖界面

LuaWeddingIterationMgr.feastId = -1 -- 酒席家具Id
LuaWeddingIterationMgr.feastInfo = nil -- 酒席装填信息
LuaWeddingIterationMgr.feastLeftTimes = 0 -- 享用剩余次数
LuaWeddingIterationMgr.feastIsEnjoyed = false -- 是否被享用过
LuaWeddingIterationMgr.feastBuffSign = -1 -- 酒席buff标记
LuaWeddingIterationMgr.flowerBallInfo = nil -- 花球界面信息
LuaWeddingIterationMgr.pairInfo = nil -- 一线牵配对信息
LuaWeddingIterationMgr.yiXianQianTick = nil -- 一线牵计时
LuaWeddingIterationMgr.yiXianQianMatchTbl = {} -- 一线牵匹配数据
LuaWeddingIterationMgr.candyId = "" -- 红包雨撒糖
LuaWeddingIterationMgr.hongBaoRainTick = nil -- 红包雨tick
LuaWeddingIterationMgr.isHongBaoRaining = false -- 当前正在下红包雨

LuaWeddingIterationMgr.proposalInfo = {} -- 求婚信息
LuaWeddingIterationMgr.baZiItem = nil -- 测八字item
LuaWeddingIterationMgr.baZiStr = "" -- 八字信息
LuaWeddingIterationMgr.certItem = nil -- 结婚证item
LuaWeddingIterationMgr.giftInfo = {} -- 纪念日礼物信息
LuaWeddingIterationMgr.IsOnParade = false -- 正在游行


--callback
-- 请求打开场景选择界面
function LuaWeddingIterationMgr:QueryOpenSceneSelectWnd(data)
    local raw = MsgPackImpl.unpack(data)
    local args = raw["args"]
    local flag = tonumber(args[0])

    if flag == 0 then
        Gac2Gas.QueryChoseNewWeddingScene(false)
    else
        Gac2Gas.QueryChoseNewWeddingScene(true)
    end
end

-- 打开请柬
function LuaWeddingIterationMgr:OpenInvitationCard(args)
    self.cardInfo.groomName = tostring(args[0])
    self.cardInfo.brideName = tostring(args[1])
    self.cardInfo.groomClass = CommonDefs.ConvertIntToEnum(typeof(EnumClass), tonumber(args[2]))
    self.cardInfo.brideClass = CommonDefs.ConvertIntToEnum(typeof(EnumClass), tonumber(args[3]))
    self.cardInfo.sceneId = tostring(args[4])
    self.cardInfo.type = tonumber(args[5])

    if args.Length == 7 then
        self.cardInfo.sign = ""
        self.cardInfo.expressionId = tonumber(args[6])
    else
        self.cardInfo.sign = tostring(args[6])
        self.cardInfo.expressionId = tonumber(args[7])
    end

    CUIManager.ShowUI(CLuaUIResources.WeddingInvitationCardWnd)
end

-- 打开假新娘玩法答题界面
function LuaWeddingIterationMgr:OpenFakeBrideWnd(stage, data)
    local list = MsgPackImpl.unpack(data)
    local info = self.fakeBrideInfo
    info.questionId = list[0]
    if stage == "NewWeddingSetAnswer" then
        self.fakeBrideStage = "SetAnswer"
    elseif stage == "NewWeddingConversation" then
        self.fakeBrideStage = "Choose"
        info.brideClass = CommonDefs.ConvertIntToEnum(typeof(EnumClass), tonumber(list[1]))
    elseif stage == "NewWeddingConversationFemale" then
        self.fakeBrideStage = "Hint"
        info.rightAnswerId = list[1]
        info.guestClass = CommonDefs.ConvertIntToEnum(typeof(EnumClass), tonumber(list[2]))
        info.guestGender = CommonDefs.ConvertIntToEnum(typeof(EnumGender), tonumber(list[3]))
        self:SetHelpAnswerButtonActive(false)
    end

    CUIManager.ShowUI(CLuaUIResources.WeddingFakeBrideWnd)
end


--Gas2Gac
-- 场景是否可以选择(新郎是否有足够的钱) status 为1表示可以选
function LuaWeddingIterationMgr:SyncChooseSceneResult(bReview, teamLeaderId, sceneInfoUd)
    self.isReview = bReview
    local sceneInfo = MsgPackImpl.unpack(sceneInfoUd)

	if sceneInfo.Count < 4 then return end
	local mapId1, status1, mapId2, status2 = sceneInfo[0], sceneInfo[1], sceneInfo[2], sceneInfo[3]
    if status1 ~= 1 and status2 ~= 1 then
        if CClientMainPlayer.Inst.Id == teamLeaderId then
            g_MessageMgr:ShowMessage("WEDDING_GROOM_NO_ENOUGH_MONEY")
        end
        return
    end

    local tbl = {}
    tbl[mapId1] = status1
    tbl[mapId2] = status2
    self.sceneSelectInfo.mapStatus = tbl
    self.sceneSelectInfo.leaderId = teamLeaderId

    CUIManager.ShowUI(CLuaUIResources.WeddingSceneSelectWnd)
end

-- 同步婚礼场景信息，宾客数量，剩余时间，新郎姓名，新娘姓名
function LuaWeddingIterationMgr:SyncSceneInfo(guestCount, leftTime, groomId, groomName, brideId, brideName)
    self.sceneInfo.groomName = groomName
    self.sceneInfo.brideName = brideName
    self.sceneInfo.guestCount = guestCount
    self.sceneInfo.leftTime = leftTime
    self.sceneInfo.groomId = groomId
    self.sceneInfo.brideId = brideId

    g_ScriptEvent:BroadcastInLua("SyncNewWeddingSceneInfo")
end

-- 场景中宾客数目
function LuaWeddingIterationMgr:SyncGuestCount(guestCount)
    self.sceneInfo.guestCount = guestCount
    g_ScriptEvent:BroadcastInLua("NewWeddingSyncGuestCount")
end

-- 同步伴侣信息
function LuaWeddingIterationMgr:SyncPartner(playerId, name, gender, cls)
    local partnerInfo = {}
    partnerInfo.playerId = tonumber(playerId)
    partnerInfo.name = tostring(name)

    partnerInfo.gender = CommonDefs.ConvertIntToEnum(typeof(EnumGender), gender)
    partnerInfo.class = CommonDefs.ConvertIntToEnum(typeof(EnumClass), cls)

    self.partnerInfo = partnerInfo

    g_ScriptEvent:BroadcastInLua("SyncNewWeddingPartner", name)
end

-- 同步选择信息
function LuaWeddingIterationMgr:SyncChooseScene(leaderId, playerId1, choice1, bSameWithLast1, playerId2, choice2, bSameWithLast2)
    local tbl = {}
    table.insert(tbl, {playerId = playerId1, choice = choice1, sameWithLast = bSameWithLast1})
    table.insert(tbl, {playerId = playerId2, choice = choice2, sameWithLast = bSameWithLast2})
    g_ScriptEvent:BroadcastInLua("SyncChooseNewWeddingScene", tbl)
end

-- 同步计时信息
function LuaWeddingIterationMgr:SyncChooseSceneCountDown(endTimestamp)
    g_ScriptEvent:BroadcastInLua("SyncChooseNewWeddingSceneCountDown", endTimestamp)
end

-- 是否可以进入婚礼场景
function LuaWeddingIterationMgr:CheckEnterWeddingByNewInvitationResult(sceneId, invitationType, bCanEnter)
    if tostring(sceneId) ~= self.cardInfo.sceneId then return end
	if tonumber(invitationType) ~= self.cardInfo.type then return end
    CUIManager.CloseUI(CLuaUIResources.WeddingInvitationCardWnd)
	if bCanEnter then
		CUIManager.ShowUI(CLuaUIResources.WeddingEnterSceneWnd)
	end
end

-- 当前玩法处于哪个阶段
function LuaWeddingIterationMgr:SyncStageInfo(stage, extraInfoUd)
    local convertStage = stage
    if stage >= 0 and stage <= 2 then
        convertStage = 1
    elseif stage > 2 then
        convertStage = stage - 1
    end
    self.stage = convertStage

    local extraInfo = MsgPackImpl.unpack(extraInfoUd)
    local convertExtraInfo = {}

    CommonDefs.ListIterate(extraInfo, DelegateFactory.Action_object(function (_value)
        table.insert(convertExtraInfo, _value)
    end))
    if convertExtraInfo[1] then convertExtraInfo[1] = tostring(convertExtraInfo[1]) end
    if convertExtraInfo[2] then convertExtraInfo[2] = tonumber(convertExtraInfo[2]) end
    if convertExtraInfo[3] then convertExtraInfo[3] = tonumber(convertExtraInfo[3]) end

    if stage == 0 then
        convertExtraInfo[1] = "Init"
    elseif stage == 2 then
        convertExtraInfo[1] = "End"
    end
    self.stageInfo = convertExtraInfo
    g_ScriptEvent:BroadcastInLua("SyncNewWeddingStageInfo")

    self:ShowCloseUIWhenStageUpdate()
end

-- 将婚前游戏中答题者当前选项同步给假新娘
function LuaWeddingIterationMgr:SyncCurrentChoice(senderId, choiceIdx)
    self.fakeBrideInfo.guestChoice = choiceIdx
    self.fakeBrideInfo.guestId = senderId

    g_ScriptEvent:BroadcastInLua("SyncNewWeddingCurrentChoice")
end

-- 婚礼副本中有人点了跳过婚前仪式
function LuaWeddingIterationMgr:SyncPartnerSkipPreGame(senderId)
    g_ScriptEvent:BroadcastInLua("SyncPartnerSkipNewWeddingPreGame")
end

-- 有人点了开启婚前仪式
function LuaWeddingIterationMgr:SyncPartnerStartPreGame(senderId)
    g_ScriptEvent:BroadcastInLua("SyncPartnerStartNewWeddingPreGame")
end

-- 同步当前选项是否正确
function LuaWeddingIterationMgr:SyncConfirmChoice(senderEngineId, choiceIdx, boolean_Right)
    g_ScriptEvent:BroadcastInLua("SyncConfirmNewWeddingChoice", senderEngineId, choiceIdx, boolean_Right)
end

-- 假新娘被提问当前选项是否正确
function LuaWeddingIterationMgr:SyncChoiceConfirm(senderId, choiceIdx)
    g_ScriptEvent:BroadcastInLua("SyncNewWeddingChoiceConfirm", senderId, choiceIdx)
end

-- 同步提问者关闭答题界面
function LuaWeddingIterationMgr:SyncCloseConversationWnd(senderId)
    self.fakeBrideStage = ""
    self.fakeBrideInfo = {}

    self:SetHelpAnswerButtonActive(false)
    CUIManager.CloseUI(CLuaUIResources.WeddingFakeBrideWnd)
end

-- 婚前仪式规则界面
function LuaWeddingIterationMgr:ShowPreGameRule(genderSide)
    local gender = CommonDefs.ConvertIntToEnum(typeof(EnumGender), tonumber(genderSide))

    if gender == EnumGender.Male then
        g_MessageMgr:ShowMessage("WEDDING_PREGAME_RULE_GROOM_GUEST")
    else
        g_MessageMgr:ShowMessage("WEDDING_PREGAME_RULE_FAKE_BRIDE")
    end
end

-- 婚前游戏同步正确的数量
function LuaWeddingIterationMgr:SyncPregameRightAnswer(rightCount)
    g_ScriptEvent:BroadcastInLua("NewWeddingSyncPregameRightAnswer", rightCount)
end

-- 显示拜堂按钮
function LuaWeddingIterationMgr:ShowBaiTangButton(progress)
    g_ScriptEvent:BroadcastInLua("ShowNewWeddingBaiTangButton", progress)
end

-- 同步装填进度
function LuaWeddingIterationMgr:SyncFillInDiskProgress(furnitureId, itemId, progress)
    if furnitureId ~= self.feastId then return end

    g_ScriptEvent:BroadcastInLua("NewWeddingSyncFillInDiskProgress", itemId, progress)
end

-- 同步食用结果
function LuaWeddingIterationMgr:SyncEnjoyFoodResult(furnitureId, leftTimes, bEnjoyed)
    if furnitureId ~= self.feastId then return end

    self.feastLeftTimes = leftTimes
    self.feastIsEnjoyed = bEnjoyed
    g_ScriptEvent:BroadcastInLua("NewWeddingSyncEnjoyFoodResult")
end

-- 隐藏npc
function LuaWeddingIterationMgr:HideNpc(engineId)
    local npc = TypeAs(CClientObjectMgr.Inst:GetObject(engineId), typeof(CClientNpc))

    if not npc then return end
    npc.Visible = false
end

-- 同步绣球运动轨迹
function LuaWeddingIterationMgr:SyncXiuQiuMovement(fromEngineId, toEngineId)
    local bride = nil
    if fromEngineId == CClientMainPlayer.Inst.EngineId then
        bride = CClientMainPlayer.Inst
    else
        bride = TypeAs(CClientObjectMgr.Inst:GetObject(fromEngineId), typeof(CClientOtherPlayer))
    end
    local ball = TypeAs(CClientObjectMgr.Inst:GetObject(toEngineId), typeof(CClientNpc))

    if not bride or not ball then return end
    local fxId = WeddingIteration_Setting.GetData().FlowerBallFxId
    local fx = CEffectMgr.Inst:AddBulletFX(fxId, bride.RO, ball.RO, 0, 1, -1, DelegateFactory.Action(function()
        local npc = TypeAs(CClientObjectMgr.Inst:GetObject(toEngineId), typeof(CClientNpc))

        if not npc then return end
        npc.Visible = true
    end))
    bride.RO:UnRefFX(fx)
end

-- 同步花球界面的文字显示
function LuaWeddingIterationMgr:SyncXiuQiuMessage(msgId)
    g_ScriptEvent:BroadcastInLua("NewWeddingSyncXiuQiuMessage", msgId)
end

-- 同步伴侣请求跳过姻缘一线牵
function LuaWeddingIterationMgr:SyncPartnerSkipYiXianQian(partnerId)
    g_ScriptEvent:BroadcastInLua("SyncPartnerSkipNewWeddingYiXianQian")
end

-- 同步伴侣请求跳过闹洞房
function LuaWeddingIterationMgr:SyncPartnerSkipNaoDongFang(partnerId)
    g_ScriptEvent:BroadcastInLua("SyncPartnerSkipNewWeddingNaoDongFang")
end

-- 同步姻缘一线牵配对信息
function LuaWeddingIterationMgr:SyncYiXianQianSelfMatchInfo(targetId, targetName, round, roundLeftTime, bSigned, bCutOff)
    self.pairInfo = {}
    self.pairInfo.targetId = targetId
    self.pairInfo.targetName = targetName
    self.pairInfo.round = round
    self.pairInfo.endTimeStamp = CServerTimeMgr.Inst.timeStamp + roundLeftTime
    self.pairInfo.bSigned = bSigned
    self.pairInfo.bCutOff = bCutOff

    g_ScriptEvent:BroadcastInLua("NewWeddingSyncYiXianQianSelfMatchInfo")
end

-- 游戏开始
function LuaWeddingIterationMgr:ShowPlayStartFx()
    EventManager.BroadcastInternalForLua(EnumEventType.OnLoadCommonUIFx, {g_UIFxPaths.WeddingPlayStartFxPath})
end

-- 游戏结束
function LuaWeddingIterationMgr:ShowPlayEndFx()
    EventManager.BroadcastInternalForLua(EnumEventType.OnLoadCommonUIFx, {g_UIFxPaths.WeddingPlayEndFxPath})
end

-- 同步姻缘一线牵的匹配结果
function LuaWeddingIterationMgr:SyncYiXianQianSceneMatchInfo(matchUd)
    local matchInfo = MsgPackImpl.unpack(matchUd)
    local tbl = {}

    local i = 1
    CommonDefs.ListIterate(matchInfo, DelegateFactory.Action_object(function (_value)
        local id = math.ceil(i / 3)

        if i % 3 == 1 then
            tbl[id] = {playerId1 = tonumber(_value)}
        elseif i % 3 == 2 then
            tbl[id].playerId2 = tonumber(_value)
        else
            tbl[id].color = tonumber(_value)
        end

        i = i + 1
    end))

    self:DelYiXianQianFx()
    self.yiXianQianMatchTbl = tbl
    self:StartYiXianQianTick()
end

-- 同步一线牵特效
function LuaWeddingIterationMgr:SyncYiXianQianSceneCutOffInfo(playerId, matchPlayerId)
    for i = 1, #self.yiXianQianMatchTbl do
        local id1 = self.yiXianQianMatchTbl[i].playerId1
        local id2 = self.yiXianQianMatchTbl[i].playerId2

        if (id1 == playerId and id2 == matchPlayerId) or (id2 == playerId and id1 == matchPlayerId) then
            self:DelOneYiXianQianFx(i)
            table.remove(self.yiXianQianMatchTbl, i)
            break
        end
    end
end

-- 同步伴侣请求结束婚礼
function LuaWeddingIterationMgr:SyncPartnerEndWedding(partnerId)
    g_ScriptEvent:BroadcastInLua("SyncPartnerEndNewWedding")
end

-- 同步发送糖果
function LuaWeddingIterationMgr:SyncSendCandy(candyId)
    if not candyId then return end
    self.candyId = candyId
    CCommonUIFxWnd.Instance:PlayHongBaoFx(EnumHongBaoRainType.WeddingCandy)
    self.isHongBaoRaining = true
    self.hongBaoRainTick = RegisterTickOnce(function()
        self.isHongBaoRaining = false
    end, 10000)
end

-- 同步宾客身份 男方宾客(0)/女方宾客(1)
function LuaWeddingIterationMgr:SyncGuestSide(guestSide)
    g_ScriptEvent:BroadcastInLua("NewWeddingSyncGuestSide", guestSide)
end

-- 拜堂结束特效
function LuaWeddingIterationMgr:SyncBaiTangEndFx(stage)
    g_ScriptEvent:BroadcastInLua("NewWeddingSyncBaiTangEndFx", stage)
end

-- 同步报名是否完成
function LuaWeddingIterationMgr:SyncSignupDone(stage, bOk)
    g_ScriptEvent:BroadcastInLua("NewWeddingSyncSignupDone")
end

-- 闹洞房场景同步新郎新娘Id信息
function LuaWeddingIterationMgr:SyncNDFSceneInfo(groomId, brideId)
    self.sceneInfo.groomId = groomId
    self.sceneInfo.brideId = brideId
    g_ScriptEvent:BroadcastInLua("SyncNewWeddingNDFSceneInfo")
end

-- 开始游行
function LuaWeddingIterationMgr:GroomStartParade()
    self.IsOnParade = true
    EventManager.Broadcast(EnumEventType.SyncDynamicActivitySimpleInfo)
    CUIManager.ShowUI(CLuaUIResources.WeddingSprinkleCandyWnd)
    LuaWeddingIterationMgr:OpenCandySelectWnd()
end

-- 结束游行
function LuaWeddingIterationMgr:GroomStopParade()
    self.IsOnParade = false
    CUIManager.CloseUI(CLuaUIResources.WeddingSprinkleCandyWnd)
    EventManager.Broadcast(EnumEventType.SyncDynamicActivitySimpleInfo)
end

-- 请求求婚
function LuaWeddingIterationMgr:PlayerRequestEngage()
    local itemId = WeddingIteration_Setting.GetData().EngageItemId
    CItemConsumeWndMgr.ShowItemConsumeWnd(itemId, 1,
    g_MessageMgr:FormatMessage("WEDDING_NEED_QINGHAN"),
    LocalString.GetString("使用"),
    DelegateFactory.Action(function () Gac2Gas.PlayerRequestEngage(false) end),
    DelegateFactory.Action(function () CUIManager.ShowUI(CUIResources.LingShouPurchaseWnd) end),
    DelegateFactory.Action(function () Gac2Gas.PlayerRequestEngage(true) end))
end

-- 收到求婚
function LuaWeddingIterationMgr:PlayerRequestEngageMe(playerId, playerName, itemName)
    self.proposalInfo.playerId = playerId
    self.proposalInfo.playerName = playerName
    self.proposalInfo.itemName = itemName
    CUIManager.ShowUI(CLuaUIResources.WeddingProposalWnd)
end

-- 请求开始测八字
function LuaWeddingIterationMgr:PlayerRequestOpenCeBaZiWnd()
    local itemId = WeddingIteration_Setting.GetData().BaZiItemId
    CItemConsumeWndMgr.ShowItemConsumeWnd(itemId, 1,
    g_MessageMgr:FormatMessage("CEZI_NEED_SANJIN"),
    LocalString.GetString("使用"),
    DelegateFactory.Action(function () Gac2Gas.RequestOpenCeBaZiWnd() end),
    DelegateFactory.Action(function () CUIManager.ShowUI(CUIResources.LingShouPurchaseWnd) end),
    DelegateFactory.Action(function () Gac2Gas.BuyMallItem(EShopMallRegion_lua.ELingyuMall, itemId, 1) end))
end

-- 开启测八字窗口
function LuaWeddingIterationMgr:OpenCeBaZiWnd()
    CUIManager.ShowUI(CLuaUIResources.WeddingBirthInfoWnd)
end

-- 八字结果
function LuaWeddingIterationMgr:SendBaZiResult(itemId)
    CUIManager.CloseUI(CLuaUIResources.WeddingBirthInfoWnd)

    CBaziCheckMgr.ShowBaziCheckWnd(itemId, LocalString.GetString("查看"),LocalString.GetString("测字结果"),
    DelegateFactory.Action(function ( ... )
        local item = CItemMgr.Inst:GetById(itemId)
        self:OpenBaZiResultWnd(item)
    end))
end

-- 同步天空盒Id
function LuaWeddingIterationMgr:SyncSkybox(skyboxId)
    self.skyboxId = skyboxId
    self:SetAtmosphere(skyboxId)
end

-- 同步说话内容
function LuaWeddingIterationMgr:SyncSayContent(engineId, sayId)
    local content = WeddingIteration_SayContent.GetData(sayId).Content
    CClientObject.SendSayContent(engineId, content, 0)
end

-- 同步纪念日礼物信息
function LuaWeddingIterationMgr:UpdateWeddingDayGiftInfo(honeyMoonGiftGot, honeyMoonGiftTime, hundredDayGiftGot, hundredDayGiftTime, anniversaryGiftGot, anniversaryGiftTime)
    self.giftInfo.honeyMoonGiftGot = honeyMoonGiftGot
    self.giftInfo.honeyMoonGiftTime = honeyMoonGiftTime
    self.giftInfo.hundredDayGiftGot = hundredDayGiftGot
    self.giftInfo.hundredDayGiftTime = hundredDayGiftTime
    self.giftInfo.anniversaryGiftGot = anniversaryGiftGot
    self.giftInfo.anniversaryGiftTime = anniversaryGiftTime
    EventManager.Broadcast(EnumEventType.OnSyncMarriageAnniversaryInfo)
end

-- 千千结宣言界面
function LuaWeddingIterationMgr:ShowWeddingDayPromiseWnd(groomName, brideName)
    self.promiseInfo.groomName = groomName
    self.promiseInfo.brideName = brideName
    CUIManager.ShowUI(CLuaUIResources.WeddingDayPromiseWnd)
end

-- 关闭测八字
function LuaWeddingIterationMgr:CancelCeBaZi()
    CUIManager.CloseUI(CLuaUIResources.WeddingBirthInfoWnd)
    g_MessageMgr:ShowMessage("BAZI_CANCLE_ALREADY")
end

-- Gac2Gas
-- 发送请柬
function LuaWeddingIterationMgr:SendWeddingInvitation(tbl, sign, expressionId)
    local invitationTypeList = CreateFromClass(MakeGenericClass(List, Object))
	for key, val in pairs(tbl) do
		CommonDefs.ListAdd(invitationTypeList, typeof(UInt32), val)
	end

    Gac2Gas.ConfirmSendWeddingInvitation(MsgPackImpl.pack(invitationTypeList), sign, expressionId)
end

-- 开始会话
function LuaWeddingIterationMgr:StartNewWeddingConversation()
    Gac2Gas.StartNewWeddingConversation()
end

-- Broadcast
-- 设置消息按键的active
function LuaWeddingIterationMgr:SetHelpAnswerButtonActive(active)
    g_ScriptEvent:BroadcastInLua("SetWeddingHelpAnswerButtonActive", active)
end

-- 关闭设置请柬界面
function LuaWeddingIterationMgr:CloseSetInvitationCardWnd()
    CUIManager.CloseUI(CLuaUIResources.WeddingSetInvitationCardWnd)
    CGuideMgr.Inst:StartGuide(EnumGuideKey.WeddingInviteTipGuide, 0)
end

-- 获取请柬中的动作Id
function LuaWeddingIterationMgr:GetActionIds()
    local actionIds = {}
    WeddingIteration_ExpressionAction.Foreach(function(key, date)
        table.insert(actionIds, date.IconExpressionId)
    end)
    return actionIds
end

-- 播放请柬上的动作
function LuaWeddingIterationMgr:PlayActionInInvitationCard(groomRO, brideRO, groomIdentifier, brideIdentifier, id)
    local expressionAction = WeddingIteration_ExpressionAction.GetData(id)
    CUIManager.SetModelRotation(groomIdentifier, 180 + expressionAction.GroomRotation)
    CUIManager.SetModelRotation(brideIdentifier, 180 + expressionAction.BrideRotation)
    self:PlayAction(groomRO, expressionAction.GroomExpressionId, EnumGender.Male, false)
	self:PlayAction(brideRO, expressionAction.BrideExpressionId, EnumGender.Female, false)
end

-- 播放动作
function LuaWeddingIterationMgr:PlayAction(ro, expressionId, gender, playOnce)
    if ro == nil then return end

    self:ClearAni(ro)
    self:PlayExpressionAction(ro, expressionId, gender, playOnce)
end

-- 播放动作（与CClientObject.ShowExpressionAction的区别是:动作结束后删除特效）
function LuaWeddingIterationMgr:PlayExpressionAction(ro, expressionId, gender, playOnce)
	local define = Expression_Define.GetData(expressionId)
	if define == nil then
        ro:DoAni(ro.NewRoleStandAni, true, 0, 1.0, 0.15, true, 1.0)
        return
    end

	local aniName = define.AniName
	ro.NeedUpdateAABB = true

	-- 特效
	local fxids = GetExpressionDefineFx(define,EnumToInt(gender))
    for i = 1, #fxids do
        local fx = CEffectMgr.Inst:AddObjectFX(fxids[i], ro, 0, 1.0, 1.0, nil, false, EnumWarnFXType.None,
            Vector3(0, 0, 0), Vector3(0, 0, 0), nil)
        ro:AddFX(aniName, fx, -1)
    end
    if self.aniFxKeys[aniName] == nil then
        self.aniFxKeys[aniName] = {}
    end

	-- 手持物
	local additionalWeaponId = (gender == EnumGender.Male) and define.MaleAdditionalEquip or define.FemaleAdditionalEquip
	local equip = EquipmentTemplate_Equip.GetData(additionalWeaponId)
	if equip then
		ro:AddChild(equip.Prefab, "weapon03", "weapon01")
	end

	ro:AddLoadAllFinishedListener(DelegateFactory.Action_CRenderObject(function(renderObject)
        local loop = (not playOnce) and (define.Loop > 0)
		local startTime = define.StartTime
		ro:DoAni(aniName, loop, startTime, 1.0, 0.15, false, 1.0)

        if not loop then
            if define.StopAtEnd <= 0 then
                local nextExpressionId = define.NextExpressionID

                if nextExpressionId > 0 then
                    ro.AniEndCallback = DelegateFactory.Action(function ()
                        ro.AniEndCallback = nil
                        self:PlayExpressionAction(ro, nextExpressionId, gender)
                    end)
                else
                    ro.AniEndCallback = DelegateFactory.Action(function ()
                        self:ClearAni(ro)
                        ro:DoAni(ro.NewRoleStandAni, true, 0, 1.0, 0.15, true, 1.0)
                    end)
                end
            end
        end
	end))
end

-- 清除动作，包括手持物、特效等
function LuaWeddingIterationMgr:ClearAni(ro)
    self:SetWeaponVisible(ro)
    ro:RemoveChild("weapon03")
    ro.AniEndCallback = nil

    for key, value in pairs(self.aniFxKeys) do
        ro:RemoveFX(key)
    end
end

-- 隐藏武器
function LuaWeddingIterationMgr:SetWeaponVisible(ro)
    ro:SetWeaponVisible(EWeaponVisibleReason.Expression, false)
    ro:ForceHideKuiLei(EHideKuiLeiStatus.ForceHideKuiLeiAndClose)
end

-- 随机一个不同的数
function LuaWeddingIterationMgr:GetDifferentRandomNum(num, id)
	local newId
	if id <= 0 then
		newId = math.random(1, num)
	else
		newId = math.random(1, num - 1)
		if newId >= id then
			newId = newId + 1
		end
	end
    return newId
end

-- 设置气泡
function LuaWeddingIterationMgr:SetBubbleWord(wordLabel, word)
	wordLabel.gameObject:SetActive(true)
	wordLabel.text = LocalString.GetString(word)
	local bubble = wordLabel.transform:Find("Bubble"):GetComponent(typeof(UIWidget))
	bubble:ResetAndUpdateAnchors()
end

-- 颜色变暗
function LuaWeddingIterationMgr:SetAllWidgetBrightness(root, scale)
    local darkColor = Color(scale, scale, scale)

	local trans = root.transform
    local rootWidget = trans:GetComponent(typeof(UIWidget))
    if rootWidget then
        rootWidget.color = darkColor
    end

	local widgets = CommonDefs.GetComponentsInChildren_Component_Type(trans, typeof(UIWidget))
	for i = 0, widgets.Length - 1 do
		widgets[i].color = darkColor
	end
end

-- array转成table
function LuaWeddingIterationMgr:Array2Tbl(array)
    if array == nil or array.Length == 0 then return nil end
    local tbl = {}
    for i = 0, array.Length - 1 do
        table.insert(tbl, array[i])
    end
    return tbl
end

-- 是否在婚礼玩法中
function LuaWeddingIterationMgr:IsInGamePlay()
    if not CClientMainPlayer.Inst then return false end
	local setting = WeddingIteration_Setting.GetData()
	return CClientMainPlayer.Inst.PlayProp.PlayId == setting.GameplayId
end

-- 是否在婚礼场景中
function LuaWeddingIterationMgr:IsInScene()
    if not CClientMainPlayer.Inst or not CScene.MainScene then return false end
    local setting = WeddingIteration_Setting.GetData()
    return CScene.MainScene.SceneTemplateId == setting.WeddingSceneId
end

-- 修改氛围
function LuaWeddingIterationMgr:SetAtmosphere(skyboxId)
    if self:IsInScene() then
        local atmosphere = WeddingIteration_Skybox.GetData(skyboxId).Atmosphere
        CRenderScene.Inst:SetAtmosphere(atmosphere)
        g_ScriptEvent:BroadcastInLua("SyncWeddingSkyBox", skyboxId)
    end
end

-- 关闭假新娘提示界面
function LuaWeddingIterationMgr:CloseFakeBrideHintWnd()
    CUIManager.CloseUI(CLuaUIResources.WeddingFakeBrideWnd)
    self:SetHelpAnswerButtonActive(true)
    CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.WeddingHelpAnswerGuide)
end

-- 打开宣言界面
function LuaWeddingIterationMgr:OpenPromiseWnd(data)
    local args = MsgPackImpl.unpack(data)
    self.promiseCountDown = args[0]
    CUIManager.ShowUI(CLuaUIResources.WeddingPromiseWnd)
end

-- 打开结婚证界面
function LuaWeddingIterationMgr:NotifyPlayerGetWeddingCert(certItemId)
    CBaziCheckMgr.ShowBaziCheckWnd(certItemId, LocalString.GetString("查看"),LocalString.GetString("结婚证书"),
        DelegateFactory.Action(function ( ... )
            local item = CItemMgr.Inst:GetById(certItemId)
            self:OpenCertificationWnd(item)
    end))
end

-- 打开结婚证界面
function LuaWeddingIterationMgr:OpenCertificationWnd(item)
    if not item then return end
    self.certItem = item
    CUIManager.ShowUI(CLuaUIResources.WeddingCertificationWnd)
end

-- 打开纪念日礼物界面
function LuaWeddingIterationMgr:OpenAnniversaryGiftWnd(marryTime, mateName)
    self.giftInfo.marryTime = marryTime
    self.giftInfo.mateName = mateName
    CUIManager.ShowUI(CLuaUIResources.WeddingAnniversaryGiftWnd)
end

function LuaWeddingIterationMgr:IsGiftAvailable()
    local now = CServerTimeMgr.Inst:GetZone8Time()
    local honey = CServerTimeMgr.ConvertTimeStampToZone8Time(self.giftInfo.honeyMoonGiftTime)
    if CommonDefs.op_GreaterThanOrEqual_DateTime_DateTime(now, honey) and not self.giftInfo.honeyMoonGiftGot then
        return true
    end
    local hundred = CServerTimeMgr.ConvertTimeStampToZone8Time(self.giftInfo.hundredDayGiftTime)
    if CommonDefs.op_GreaterThanOrEqual_DateTime_DateTime(now, hundred) and not self.giftInfo.hundredDayGiftGot then
        return true
    end
    local anni = CServerTimeMgr.ConvertTimeStampToZone8Time(self.giftInfo.anniversaryGiftTime)
    if CommonDefs.op_GreaterThanOrEqual_DateTime_DateTime(now, anni) then
        return true
    end
    return false
end

-- 打开酒席选择界面
function LuaWeddingIterationMgr:OpenFeastSelectWnd()
    self.selectWndItem = "Feast"
    CUIManager.ShowUI(CLuaUIResources.WeddingSelectWnd)
end

-- 打开烟花选择界面
function LuaWeddingIterationMgr:OpenFireworkSelectWnd()
    self.selectWndItem = "Firework"
    CUIManager.ShowUI(CLuaUIResources.WeddingSelectWnd)
end

-- 打开喜糖选择界面
function LuaWeddingIterationMgr:OpenCandySelectWnd()
    self.selectWndItem = "Candy"
    CUIManager.ShowUI(CLuaUIResources.WeddingSelectWnd)
end

-- 打开酒席装填和享用界面
function LuaWeddingIterationMgr:OpenFeastWnd(data)
    local args = MsgPackImpl.unpack(data)
    self.feastId = args[0]
    self.feastLeftTimes = args[2]
    self.feastIsEnjoyed = args[3]
    self.feastBuffSign = args[4]

    self.feastInfo = {}

    local list = {}
    CommonDefs.ListIterate(args[1], DelegateFactory.Action_object(function (value)
        table.insert(list, value)
    end))

    local num = #list / 6
    for i = 0, num - 1 do
        local tbl = {}
        local itemId = tonumber(list[i * 6 + 1])
        tbl.count = tonumber(list[i * 6 + 2])
        tbl.playerId = tonumber(list[i * 6 + 3])
        tbl.playerName = tostring(list[i * 6 + 4])
        tbl.playerClass = CommonDefs.ConvertIntToEnum(typeof(EnumClass), tonumber(list[i * 6 + 5]))
        tbl.playerGender = CommonDefs.ConvertIntToEnum(typeof(EnumGender), tonumber(list[i * 6 + 6]))

        self.feastInfo[itemId] = tbl
    end

    CUIManager.ShowUI(CLuaUIResources.WeddingFeastWnd)
end

-- 打开花球界面
function LuaWeddingIterationMgr:OpenFlowerBallWnd(data)
    local info = {}
    local args = MsgPackImpl.unpack(data)
    if not args then return end

    info.playerName = tostring(args[1])
    info.playerClass = CommonDefs.ConvertIntToEnum(typeof(EnumClass), tonumber(args[2]))
    info.playerGender = CommonDefs.ConvertIntToEnum(typeof(EnumGender), tonumber(args[3]))
    self.flowerBallInfo = info

    CUIManager.ShowUI(CLuaUIResources.WeddingFlowerBallWnd)
end

-- 开启一线牵tick
function LuaWeddingIterationMgr:StartYiXianQianTick()
    self:DestroyYiXianQianTick()
    self:UpdateYiXianQianFx()
    self.yiXianQianTick = RegisterTick(function()
        self:UpdateYiXianQianFx()
    end, 1000)
end

-- 清除一线牵tick
function LuaWeddingIterationMgr:DestroyYiXianQianTick()
    if self.yiXianQianTick then
        UnRegisterTick(self.yiXianQianTick)
        self.yiXianQianTick = nil
    end
end

-- 添加一线牵特效
function LuaWeddingIterationMgr:UpdateYiXianQianFx()
    local matchTbl = self.yiXianQianMatchTbl
    if #matchTbl == 0 then
        self:DestroyYiXianQianTick()
    end

    local linkFxIds = self:Array2Tbl(WeddingIteration_Setting.GetData().YiXianQianLinkFxId)
    local objectFxIds = self:Array2Tbl(WeddingIteration_Setting.GetData().YiXianQianObjectFxId)

    for i = 1, #matchTbl do
        local player1 = CClientPlayerMgr.Inst:GetPlayer(matchTbl[i].playerId1)
        local player2 = CClientPlayerMgr.Inst:GetPlayer(matchTbl[i].playerId2)

        if player1 and player2 then
            if not matchTbl[i].linkFx then
                local linkFxId = linkFxIds[matchTbl[i].color]
                matchTbl[i].linkFx = CEffectMgr.Inst:AddLinkFX(linkFxId, player1.RO, player2.RO, 0, 1, -1)
            end

            local objectFxId = objectFxIds[matchTbl[i].color]
            if not matchTbl[i].objFx1 then
                matchTbl[i].objFx1 = CEffectMgr.Inst:AddObjectFX(objectFxId, player1.RO, 0, 1, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
            end

            if not matchTbl[i].objFx2 then
                matchTbl[i].objFx2 = CEffectMgr.Inst:AddObjectFX(objectFxId, player2.RO, 0, 1, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
            end
        else
            self:DelOneYiXianQianFx(i)
        end
    end
end

-- 删除一线牵特效
function LuaWeddingIterationMgr:DelYiXianQianFx()
    for i = 1, #self.yiXianQianMatchTbl do
        self:DelOneYiXianQianFx(i)
    end
    self.yiXianQianMatchTbl = {}
end

-- 删除一个一线牵特效
function LuaWeddingIterationMgr:DelOneYiXianQianFx(id)
    if self.yiXianQianMatchTbl[id].linkFx then
        self.yiXianQianMatchTbl[id].linkFx:Destroy()
        self.yiXianQianMatchTbl[id].linkFx = nil
    end

    if self.yiXianQianMatchTbl[id].objFx1 then
        self.yiXianQianMatchTbl[id].objFx1:Destroy()
        self.yiXianQianMatchTbl[id].objFx1 = nil
    end

    if self.yiXianQianMatchTbl[id].objFx2 then
        self.yiXianQianMatchTbl[id].objFx2:Destroy()
        self.yiXianQianMatchTbl[id].objFx2 = nil
    end
end

-- 是不是新郎/新娘
function LuaWeddingIterationMgr:IsGroomOrBride()
    local groomId = self.sceneInfo.groomId
	local brideId = self.sceneInfo.brideId
    local playerId = CClientMainPlayer.Inst.Id

	if not groomId or not brideId or not playerId then return nil, nil, nil end

	return playerId == groomId or playerId == brideId, playerId == groomId, playerId == brideId
end

-- 离开闹洞房
function LuaWeddingIterationMgr:LeaveDongFang()
    local isGroomOrBride = self:IsGroomOrBride()
    if isGroomOrBride == nil then return end

	if isGroomOrBride then
		local message = g_MessageMgr:FormatMessage("WEDDING_COUPLE_LEAVE_DONGFANG_CONFIRM")
		MessageWndManager.ShowDelayOKCancelMessage(message, DelegateFactory.Action(function ()
			Gac2Gas.RequestLeavePlay()
		end), nil, 5)
	else
		CGamePlayMgr.Inst:LeavePlay()
	end
end

-- 抢喜糖
function LuaWeddingIterationMgr:SnatchWeddingCandy()
    if self.candyId == "" then return end
    Gac2Gas.NewWeddingRequestSnatchCandy(self.candyId)
end

-- 打开八字结果界面
function LuaWeddingIterationMgr:OpenBaZiResultWnd(item)
    if not item then return end
    self.baZiItem = item
    CUIManager.ShowUI(CLuaUIResources.WeddingBaZiWnd)
end

-- 不同阶段下显示或关闭界面
function LuaWeddingIterationMgr:ShowCloseUIWhenStageUpdate()
    local subStage = self.stageInfo[1]

    -- 婚前仪式结束时播放subtitle
    if self.stage == 1 then
        if subStage == "End" then
            SubtitileMgr.ShowSubtitle(WeddingIteration_Setting.GetData().PregameEndSubtitleId, DelegateFactory.Action(function ()
            end))
        end
    end

    -- 婚前仪式结束或进入其他阶段需关闭假新娘界面
    if self.stage ~= 1 or (subStage and subStage == "End") then
        self:SyncCloseConversationWnd()
    end

    -- 非一线牵阶段需要清除一线牵tick
    if self.stage ~= 5 then
        self:DestroyYiXianQianTick()
        self:DelYiXianQianFx()
    end
end

-- 离开婚礼场景清除数据
function LuaWeddingIterationMgr:ClearData()
    self:SetHelpAnswerButtonActive(false)
    self:DestroyYiXianQianTick()
    self:DelYiXianQianFx()

    self.cachedInvitationInfo = {}
    self.sceneInfo = {}
    self.stage = -1
    self.stageInfo = {}
    self.skyboxId = nil
    self.pairInfo = nil
end

function LuaWeddingIterationMgr:ShowTopRightWnd(wnd)
    if LuaTopAndRightMenuWndMgr.m_GamePlayId == WeddingIteration_Setting.GetData().GameplayId or LuaTopAndRightMenuWndMgr.m_GamePlayId == WeddingIteration_Setting.GetData().NDFGameplayId then
        CUIManager.ShowUI(CLuaUIResources.WeddingBottomWnd)
    else
        CUIManager.CloseUI(CLuaUIResources.WeddingBottomWnd)
    end

    if LuaTopAndRightMenuWndMgr.m_GamePlayId == WeddingIteration_Setting.GetData().GameplayId then
        wnd.contentNode:SetActive(false)
        CUIManager.ShowUI(CLuaUIResources.WeddingTopWnd)
    else
        CUIManager.CloseUI(CLuaUIResources.WeddingTopWnd)
    end
end