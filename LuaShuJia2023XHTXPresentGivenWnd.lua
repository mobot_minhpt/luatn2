local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local UITable = import "UITable"
local GameObject = import "UnityEngine.GameObject"
local CButton = import "L10.UI.CButton"

LuaShuJia2023XHTXPresentGivenWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaShuJia2023XHTXPresentGivenWnd, "TipLab", "TipLab", UILabel)
RegistChildComponent(LuaShuJia2023XHTXPresentGivenWnd, "UITable", "UITable", UITable)
RegistChildComponent(LuaShuJia2023XHTXPresentGivenWnd, "NormalPass", "NormalPass", GameObject)
RegistChildComponent(LuaShuJia2023XHTXPresentGivenWnd, "HightPass", "HightPass", GameObject)
RegistChildComponent(LuaShuJia2023XHTXPresentGivenWnd, "CombinePass", "CombinePass", GameObject)
RegistChildComponent(LuaShuJia2023XHTXPresentGivenWnd, "NormalPassName", "NormalPassName", UILabel)
RegistChildComponent(LuaShuJia2023XHTXPresentGivenWnd, "HightPassName", "HightPassName", UILabel)
RegistChildComponent(LuaShuJia2023XHTXPresentGivenWnd, "CombinePassName", "CombinePassName", UILabel)
RegistChildComponent(LuaShuJia2023XHTXPresentGivenWnd, "NormalPassPriceLab", "NormalPassPriceLab", UILabel)
RegistChildComponent(LuaShuJia2023XHTXPresentGivenWnd, "HighPassPriceLab", "HighPassPriceLab", UILabel)
RegistChildComponent(LuaShuJia2023XHTXPresentGivenWnd, "CombinePassPriceLab", "CombinePassPriceLab", UILabel)
RegistChildComponent(LuaShuJia2023XHTXPresentGivenWnd, "CombinePassOldPriceLab", "CombinePassOldPriceLab", UILabel)
RegistChildComponent(LuaShuJia2023XHTXPresentGivenWnd, "NormalPassBtn", "NormalPassBtn", CButton)
RegistChildComponent(LuaShuJia2023XHTXPresentGivenWnd, "HighPassBtn", "HighPassBtn", CButton)
RegistChildComponent(LuaShuJia2023XHTXPresentGivenWnd, "CombinePassBtn", "CombinePassBtn", CButton)

--@endregion RegistChildComponent end
RegistClassMember(LuaShuJia2023XHTXPresentGivenWnd, "m_PlayerName")
RegistClassMember(LuaShuJia2023XHTXPresentGivenWnd, "m_PlayerId")
RegistClassMember(LuaShuJia2023XHTXPresentGivenWnd, "m_PlayerVipStatus")
RegistClassMember(LuaShuJia2023XHTXPresentGivenWnd, "m_NormalPassJade")
RegistClassMember(LuaShuJia2023XHTXPresentGivenWnd, "m_HighPassJade")
RegistClassMember(LuaShuJia2023XHTXPresentGivenWnd, "m_CombinePassJade")

function LuaShuJia2023XHTXPresentGivenWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

    self.m_PassId = tonumber(ShuJia2023_Setting.GetData("ShuJiaPassId").Value)
    local passData = Pass_Setting.GetData(self.m_PassId)
    local passClientData = Pass_ClientSetting.GetData(self.m_PassId)
    if passData and passClientData then
        self.m_NormalPassJade = passData.VipCost[0]
        self.m_HighPassJade = passData.VipCost[1]
        self.m_CombinePassJade = passData.UnlockType == 2 and tonumber(passData.OnSalesVipCost) or 988
        self.NormalPassPriceLab.text = self.m_NormalPassJade
        self.HighPassPriceLab.text = self.m_HighPassJade
        self.CombinePassPriceLab.text = self.m_CombinePassJade
        self.CombinePassOldPriceLab.text = self.m_NormalPassJade + self.m_HighPassJade
    end

    CommonDefs.AddOnClickListener(self.NormalPassBtn.gameObject, DelegateFactory.Action_GameObject(function(Lab)
        self:SendVIP(self.m_PlayerId, self.m_PlayerName, 1, self.m_NormalPassJade)
    end), false)

    CommonDefs.AddOnClickListener(self.HighPassBtn.gameObject, DelegateFactory.Action_GameObject(function(Lab)
        self:SendVIP(self.m_PlayerId, self.m_PlayerName, 2, self.m_HighPassJade)
    end), false)

    CommonDefs.AddOnClickListener(self.CombinePassBtn.gameObject, DelegateFactory.Action_GameObject(function(Lab)
        self:SendVIP(self.m_PlayerId, self.m_PlayerName, 0, self.m_CombinePassJade)
    end), false)
end

function LuaShuJia2023XHTXPresentGivenWnd:Init()
    self.m_PlayerName = LuaShuJia2023Mgr.m_PresentPlayerName
    self.m_PlayerId = LuaShuJia2023Mgr.m_PresentPlayerId
    self.m_PlayerVipStatus = LuaShuJia2023Mgr.m_PresentPlayerVipStatus

    self.TipLab.text = SafeStringFormat3(LocalString.GetString("请选择赠送给[c][F5EC93]%s[-][/c]的海图"), self.m_PlayerName)
    self.NormalPass:SetActive(not self.m_PlayerVipStatus[1])
    self.HightPass:SetActive(not self.m_PlayerVipStatus[2])
    self.CombinePass:SetActive((not self.m_PlayerVipStatus[1]) and (not self.m_PlayerVipStatus[2]))
    self.UITable:Reposition()
end

--@region UIEvent

--@endregion UIEvent

function LuaShuJia2023XHTXPresentGivenWnd:SendVIP(playerId, playerName, vipType, vipCost)
    local msg = g_MessageMgr:FormatMessage("SHUJIA2023_XHTX_PRESENTPASS_MAKESURE", vipCost, playerName)
    MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function()
        Gac2Gas.RequestSendPassVipByJade(self.m_PassId, vipType, playerId)
        CUIManager.CloseUI(CLuaUIResources.ShuJia2023XHTXPresentGivenWnd)
        RegisterTickOnce(function()
            Gac2Gas.XianHaiTongXingPlayerQueryTeamInfo()
        end, 500)
    end), nil, nil, nil, false)
end