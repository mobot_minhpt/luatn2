local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CCarnivalLotteryMgr = import "L10.Game.CCarnivalLotteryMgr"

LuaLiuYi2021TangGuoEnterWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaLiuYi2021TangGuoEnterWnd, "RuleBtn", "RuleBtn", GameObject)
RegistChildComponent(LuaLiuYi2021TangGuoEnterWnd, "JoinBtn", "JoinBtn", GameObject)
RegistChildComponent(LuaLiuYi2021TangGuoEnterWnd, "CancelBtn", "CancelBtn", GameObject)
RegistChildComponent(LuaLiuYi2021TangGuoEnterWnd, "CountLabel", "CountLabel", UILabel)
RegistChildComponent(LuaLiuYi2021TangGuoEnterWnd, "DescLabel", "DescLabel", UILabel)
RegistChildComponent(LuaLiuYi2021TangGuoEnterWnd, "TimeLabel", "TimeLabel", UILabel)

--@endregion RegistChildComponent end

function LuaLiuYi2021TangGuoEnterWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	UIEventListener.Get(self.RuleBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRuleBtnClick()
	end)

	UIEventListener.Get(self.JoinBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnJoinBtnClick()
	end)

	UIEventListener.Get(self.CancelBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCancelBtnClick()
	end)

    --@endregion EventBind end
end

function LuaLiuYi2021TangGuoEnterWnd:Init()
	Gac2Gas.CandyPlayCheckInMatching()

	self.DescLabel.text = g_MessageMgr:FormatMessage("LiuYi_2021_TangGuo_Enter_Desc")
	self.CountLabel.text = tostring(LiuYi2021_Setting.GetData().MaxDailyAwardTimes - CCarnivalLotteryMgr.Inst:GetGamePlayTempData(84))
end

function LuaLiuYi2021TangGuoEnterWnd:OnEnable( )
    g_ScriptEvent:AddListener("CandyPlayCheckInMatchingResult", self, "UpdateSignStatus")
end

function LuaLiuYi2021TangGuoEnterWnd:OnDisable( )
    g_ScriptEvent:RemoveListener("CandyPlayCheckInMatchingResult", self, "UpdateSignStatus")
end

function LuaLiuYi2021TangGuoEnterWnd:UpdateSignStatus(isMatching)
    self.CancelBtn:SetActive(isMatching)
	self.JoinBtn:SetActive(not isMatching)
end

--@region UIEvent

function LuaLiuYi2021TangGuoEnterWnd:OnRuleBtnClick()
	g_MessageMgr:ShowMessage("LiuYi_2021_TangGuo_Rule_Desc")
end

function LuaLiuYi2021TangGuoEnterWnd:OnJoinBtnClick()
	Gac2Gas.CandyPlaySignUp()
end

function LuaLiuYi2021TangGuoEnterWnd:OnCancelBtnClick()
	Gac2Gas.CandyPlayCancelSignUp()
end

--@endregion UIEvent

