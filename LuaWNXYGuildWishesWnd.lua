local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"

LuaWNXYGuildWishesWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaWNXYGuildWishesWnd, "NextPageBtn", "NextPageBtn", GameObject)
RegistChildComponent(LuaWNXYGuildWishesWnd, "LastPageBtn", "LastPageBtn", GameObject)
RegistChildComponent(LuaWNXYGuildWishesWnd, "CurrentPageView", "CurrentPageView", GameObject)
RegistChildComponent(LuaWNXYGuildWishesWnd, "PageLabel", "PageLabel", UILabel)
RegistChildComponent(LuaWNXYGuildWishesWnd, "MakeWishBtn", "MakeWishBtn", GameObject)

--@endregion RegistChildComponent end                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ")
RegistClassMember(LuaWNXYGuildWishesWnd,"m_CurPageNumber")
RegistClassMember(LuaWNXYGuildWishesWnd,"m_ViewingItems")

function LuaWNXYGuildWishesWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.NextPageBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnNextPageBtnClick()
	end)


	
	UIEventListener.Get(self.LastPageBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLastPageBtnClick()
	end)


	
	UIEventListener.Get(self.MakeWishBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnMakeWishBtnClick()
	end)


    --@endregion EventBind end
	self.m_CountPerPage = 8
	self.m_ViewingItems = {}
	self.m_CurPageNumber = 1
	for i=1,8,1 do
		local name = "Wish"..i
		local item = self.CurrentPageView.transform:Find(name)
		table.insert(self.m_ViewingItems,item)
	end
end

function LuaWNXYGuildWishesWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncGuildChristmasPlayerInfo",self,"OnSyncGuildChristmasPlayerInfo")
end

function LuaWNXYGuildWishesWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncGuildChristmasPlayerInfo",self,"OnSyncGuildChristmasPlayerInfo")
end

function LuaWNXYGuildWishesWnd:OnSyncGuildChristmasPlayerInfo()
	self:Init()
end

function LuaWNXYGuildWishesWnd:Init()
	self:InitPageList(1)
	self:InitPageView()
end

function LuaWNXYGuildWishesWnd:InitPageList(page)
	if not self.m_CurPageList then self.m_CurPageList = {} end  
	if page < 1 or page > math.ceil(#LuaChristmas2021Mgr.GuildWishList/self.m_CountPerPage) then
		return 
	end
	self.m_CurPageNumber = page
	local startIndex = (page-1) * self.m_CountPerPage + 1
	local endIndex = page * self.m_CountPerPage
	for i=startIndex,endIndex,1 do
		table.insert(self.m_CurPageList,LuaChristmas2021Mgr.GuildWishList[i])
	end                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
end

function LuaWNXYGuildWishesWnd:InitPageView()
	for row = 1,self.m_CountPerPage,1 do
		self:InitItem(row)
	end
	local totalPage = math.ceil(#LuaChristmas2021Mgr.GuildWishList/self.m_CountPerPage)
	self.PageLabel.text = SafeStringFormat3(LocalString.GetString("%d / %d页"),totalPage <=0 and 0 or self.m_CurPageNumber,totalPage)
end

function LuaWNXYGuildWishesWnd:InitItem(row)
	local item = self.m_ViewingItems[row]

	local pindex = row
	local index = (self.m_CurPageNumber - 1)*self.m_CountPerPage + pindex

	if pindex > #self.m_CurPageList then
		item.gameObject:SetActive(false)
		return
	end

	local info = LuaChristmas2021Mgr.GuildWishList[index]
	if not info then
		item.gameObject:SetActive(false)
		return
	end
	item.gameObject:SetActive(true)

	local portrait = item:Find("Portrait"):GetComponent(typeof(UITexture))
	local portraitCuitexture = item:Find("Portrait"):GetComponent(typeof(CUITexture))
	local playerName = item:Find("PlayerName"):GetComponent(typeof(UILabel))
	local rewardTag = item:Find("RewardTag").gameObject

	if not LuaChristmas2021Mgr.PlayerId2MengDaoInfo[info.PlayerId] then
		self:QuerryPlayerInfo(item,info,index)
	else
		self:InitItemMengDaoInfo(item,info,index)
	end
end

function LuaWNXYGuildWishesWnd:QuerryPlayerInfo(item,card,index)
	if CommonDefs.IS_CN_CLIENT then
		CPersonalSpaceMgr.Inst:GetUserProfile(card.PlayerId, DelegateFactory.Action_CPersonalSpace_UserProfile_Ret(function (ret)
			if ret.code ~= 0 or CClientMainPlayer.Inst == nil then
				return
			end
			local playerInfo = {}
			playerInfo.PlayerId = card.PlayerId
			playerInfo.RoleName = ret.data.rolename
			playerInfo.Photo = ret.data.photo
			playerInfo.Cls = ret.data.clazz
			playerInfo.Gender = ret.data.gender
			LuaChristmas2021Mgr.PlayerId2MengDaoInfo[card.PlayerId] = playerInfo
			self:InitItemMengDaoInfo(item,card,index)
		end), nil)
	else
		local playerInfo = {}
		playerInfo.PlayerId = card.PlayerId
		playerInfo.RoleName = card.PlayerName
		playerInfo.Photo = nil
		playerInfo.Cls = nil
		playerInfo.Gender = nil
		LuaChristmas2021Mgr.PlayerId2MengDaoInfo[card.PlayerId] = playerInfo
		self:InitItemMengDaoInfo(item,card,index)
	end
end

function LuaWNXYGuildWishesWnd:InitItemMengDaoInfo(item,info,index)
	local portrait = item:Find("Portrait"):GetComponent(typeof(UITexture))
	local portraitCuitexture = item:Find("Portrait"):GetComponent(typeof(CUITexture))
	local playerName = item:Find("PlayerName"):GetComponent(typeof(UILabel))
	local rewardTag = item:Find("RewardTag").gameObject

	local playerInfo = LuaChristmas2021Mgr.PlayerId2MengDaoInfo[info.PlayerId]
	portrait.material = nil
	playerName.text = info.PlayerName
	if playerInfo.Photo then
		CPersonalSpaceMgr.Inst:DownLoadPortraitPic(playerInfo.Photo,portrait, nil)
	elseif playerInfo.Cls and playerInfo.Gender then
		portraitCuitexture:LoadNPCPortrait(CUICommonDef.GetPortraitName(playerInfo.Cls, playerInfo.Gender, -1), false)
	else
		local path = SafeStringFormat3("UI/Texture/Portrait/Material/lnpc606_0%d.mat",(info.PlayerId%3 + 1))
		portraitCuitexture:LoadMaterial(path)
		playerInfo.path = path
	end

	local hasGift = false
	local giftCount = 0
	local giftIndex
	if info.Gifts and info.Gifts.Count > 0 then 
		for i=0,info.Gifts.Count-1,1 do 
			if info.Gifts[i] > 0 then
				hasGift = true
				giftIndex = i
				giftCount = info.Gifts[i]
				break
			end
		end
	end
	rewardTag:SetActive(hasGift)

	local tagPos = rewardTag.transform.localPosition
	tagPos.z = 0
	if hasGift then
		local restCount = giftCount - info.Sended[giftIndex]
		if restCount <= 0 then
			tagPos.z = -1--领取完了的卡片1级界面小礼物置灰
		end
	end
	rewardTag.transform.localPosition = tagPos
	
	UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCardItemClick(index)
	end)
end

function LuaWNXYGuildWishesWnd:OnCardItemClick(index)
	local cardInfo = LuaChristmas2021Mgr.GuildWishList[index]
	Gac2Gas.QueryOtherChristmasCardInGuild(cardInfo.GuildId,cardInfo.Id)
end

--@region UIEvent

function LuaWNXYGuildWishesWnd:OnNextPageBtnClick()
	self:InitPageList(self.m_CurPageNumber+1)
	self:InitPageView()
end

function LuaWNXYGuildWishesWnd:OnLastPageBtnClick()
	self:InitPageList(self.m_CurPageNumber-1)
	self:InitPageView()
end

function LuaWNXYGuildWishesWnd:OnMakeWishBtnClick()
	LuaChristmas2021Mgr:OpenMakeWishWnd()
end

--@endregion UIEvent

