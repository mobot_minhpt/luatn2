-- Auto Generated!!
local AISkillType = import "L10.Game.AISkillType"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CGuaJiMgr = import "L10.Game.CGuaJiMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local Constants = import "L10.Game.Constants"
local CPVPSkillPreferenceBottomView = import "L10.UI.CPVPSkillPreferenceBottomView"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"
local CSkillMgr = import "L10.Game.CSkillMgr"
local EnumClass = import "L10.Game.EnumClass"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local MessageMgr = import "L10.Game.MessageMgr"
local MsgPackImpl = import "MsgPackImpl"
local Object = import "System.Object"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local MessageWndManager = import "L10.UI.MessageWndManager"
local Input = import "UnityEngine.Input"
local CUITexture = import "L10.UI.CUITexture"
local SoundManager = import "SoundManager"
local Vector3 = import "UnityEngine.Vector3"
local CPropertySkill = import "L10.Game.CPropertySkill"

local hideOrShowArrows = function (this, bIsOpen)
    local skillRoot = this.transform:Find("SkillButtonRoot").gameObject
    for i = 1, 4 do
        local name = SafeStringFormat3("arrow%s", tostring(i))
        local arrowObj = skillRoot.transform:Find(name).gameObject
        if arrowObj then
            -- 如果没有打开，需要显示箭头
            arrowObj:SetActive(not bIsOpen)
        end
    end
end

CPVPSkillPreferenceBottomView.m_checkSkillAllowed_CS2LuaHook = function (this, skillId) 
    if CommonDefs.HashSetContains(CGuaJiMgr.Inst.AllowedAISkill, typeof(UInt32), skillId) then
        return true
    end
    return false
end
CPVPSkillPreferenceBottomView.m_OnEnable_CS2LuaHook = function (this) 
    this:InitSkillSet()
    this:UpdateCheckbox()
    this:SubscribeEvents()

    if CClientMainPlayer.Inst == nil then
        return
    end

    LuaPVPSkillPreferenceBottomView:OnEnable(this)
    LuaPVPSkillPreferenceBottomView:InitSkill(this, false, EnumToInt(CClientMainPlayer.Inst.CurYingLingState))
end
CPVPSkillPreferenceBottomView.m_OnDisable_CS2LuaHook = function (this)
    LuaPVPSkillPreferenceBottomView:OnDisable()
    this:UnsubscribeEvents()
end
CPVPSkillPreferenceBottomView.m_SubscribeEvents_CS2LuaHook = function (this) 
    UIEventListener.Get(this.infoBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.infoBtn).onClick, MakeDelegateFromCSFunction(this.OnInfoButtonClick, VoidDelegate, this), true)
    EventManager.AddListener(EnumEventType.MainPlayerPVPSkillPropUpdate, MakeDelegateFromCSFunction(this.InitSkillSet, Action0, this))
    EventManager.AddListener(EnumEventType.MainPlayerMPTZJueJiSkillSettingUpdate, MakeDelegateFromCSFunction(this.UpdateCheckbox, Action0, this))
    EventManager.AddListener(EnumEventType.SwitchYingLingMenPaiRobotState, MakeDelegateFromCSFunction(this.OnChangeYingLingState, Action0, this))

    do
        local i = 0
        while i < this.activeSkillIcons.Length do
            UIEventListener.Get(this.activeSkillIcons[i].gameObject).onDragStart = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.activeSkillIcons[i].gameObject).onDragStart, MakeDelegateFromCSFunction(this.OnDragIconStart, VoidDelegate, this), true)
            UIEventListener.Get(this.activeSkillIcons[i].gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.activeSkillIcons[i].gameObject).onClick, MakeDelegateFromCSFunction(this.OnSkillIconClick, VoidDelegate, this), true)
            i = i + 1
        end
    end
end
CPVPSkillPreferenceBottomView.m_UnsubscribeEvents_CS2LuaHook = function (this) 
    UIEventListener.Get(this.infoBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.infoBtn).onClick, MakeDelegateFromCSFunction(this.OnInfoButtonClick, VoidDelegate, this), false)
    EventManager.RemoveListener(EnumEventType.MainPlayerPVPSkillPropUpdate, MakeDelegateFromCSFunction(this.InitSkillSet, Action0, this))
    EventManager.RemoveListener(EnumEventType.MainPlayerMPTZJueJiSkillSettingUpdate, MakeDelegateFromCSFunction(this.UpdateCheckbox, Action0, this))
    EventManager.RemoveListener(EnumEventType.SwitchYingLingMenPaiRobotState, MakeDelegateFromCSFunction(this.OnChangeYingLingState, Action0, this))

    do
        local i = 0
        while i < this.activeSkillIcons.Length do
            UIEventListener.Get(this.activeSkillIcons[i].gameObject).onDragStart = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.activeSkillIcons[i].gameObject).onDragStart, MakeDelegateFromCSFunction(this.OnDragIconStart, VoidDelegate, this), false)
            UIEventListener.Get(this.activeSkillIcons[i].gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.activeSkillIcons[i].gameObject).onClick, MakeDelegateFromCSFunction(this.OnSkillIconClick, VoidDelegate, this), false)
            i = i + 1
        end
    end
end
CPVPSkillPreferenceBottomView.m_UpdateCheckbox_CS2LuaHook = function (this) 
    local mainplayer = CClientMainPlayer.Inst
    if mainplayer ~= nil then
        this.firstJuejiCheckbox.gameObject:SetActive(mainplayer.SkillProp:IsOriginalSkillClsExist(mainplayer.FirstJuejiSkillCls) and (mainplayer.Class ~= EnumClass.HuaHun or LuaPVPMgr.IsRobotOpen()))
        this.secondJuejiCheckbox.gameObject:SetActive(mainplayer.SkillProp:IsOriginalSkillClsExist(mainplayer.SecondJuejiSkillCls) and mainplayer.Class ~= EnumClass.YanShi)
        --TODO 根据是否投放设置选项
        this.firstJuejiCheckbox:SetSelected(CClientMainPlayer.Inst.SkillProp.IsMPTZCast79Skill > 0, true)
        this.secondJuejiCheckbox:SetSelected(CClientMainPlayer.Inst.SkillProp.IsMPTZCast109Skill > 0, true)

    else
        this.firstJuejiCheckbox.gameObject:SetActive(false)
        this.secondJuejiCheckbox.gameObject:SetActive(false)
    end
end
CPVPSkillPreferenceBottomView.m_GetSkillDetailId_CS2LuaHook = function (this, id) 
    if CClientMainPlayer.Inst ~= nil then
        if CClientMainPlayer.Inst.SkillProp:IsOriginalSkillClsExist(id) then
            local skillId = CClientMainPlayer.Inst.SkillProp:GetSkillIdWithDeltaByCls(id, CClientMainPlayer.Inst.Level)
            return skillId
        else
            local inEffectColorSkill = CClientMainPlayer.Inst.SkillProp:GetInEffectColorSystemSkill(id)
            if inEffectColorSkill > 0 then
                local level = CLuaDesignMgr.Skill_AllSkills_GetLevel(CClientMainPlayer.Inst.SkillProp:GetSkillIdWithDeltaByCls(inEffectColorSkill, CClientMainPlayer.Inst.Level))
                return CLuaDesignMgr.Skill_AllSkills_GetSkillId(id, level)
                --从生效的技能获取等级信息，再拼成这里的技能id，神坑
            end
        end
    end
    return SkillButtonSkillType_lua.NoSkill
end
CPVPSkillPreferenceBottomView.m_hookOnDragIconStart = function (this, go)
    local aiSkillType = LuaPVPMgr.GetAISkillType()
    local startIndex = (EnumToInt(aiSkillType) - 1) * CPropertySkill.ActiveSkillCount
    do
        local i = 0
        while i < this.activeSkillIcons.Length do
            if this.activeSkillIcons[i].gameObject == go then
                if CClientMainPlayer.Inst ~= nil then
                    this.skillId = SkillButtonSkillType_lua.NoSkill
                    if CClientMainPlayer.Inst.SkillProp.NewActiveSkillForAI[i + 1 + startIndex] == SkillButtonSkillType_lua.NoSkill then
                        return
                    end
                    this.skillId = this:GetSkillDetailId(CClientMainPlayer.Inst.SkillProp.NewActiveSkillForAI[i + 1 + startIndex])
                    local data = Skill_AllSkills.GetData(this.skillId)
                    this.srcSkillIndex = i + 1
                    this.clonedIcon.gameObject:SetActive(true)
                    this.clonedIcon:GetComponentInChildren(typeof(CUITexture)):LoadSkillIcon(Skill_AllSkills.GetData(this.skillId).SkillIcon)
                    this.dragging = true
                    this.lastPos = Input.mousePosition
                    if CommonDefs.IsInMobileDevice() then
                        this.fingerIndex = Input.GetTouch(0).fingerId
                        this.lastPos = Input.GetTouch(0).position
                    end
                end
                break
            end
            i = i + 1
        end
    end
    SoundManager.Inst:PlayOneShot(SoundManager.UISoundEvents.HongBaoSnatchSound, Vector3.zero, nil, 0)
end
CPVPSkillPreferenceBottomView.m_OnSkillIconClick_CS2LuaHook = function (this, go) 
    local aiSkillType = LuaPVPMgr.GetAISkillType()
    local startIndex = (EnumToInt(aiSkillType) - 1) * CPropertySkill.ActiveSkillCount
    do
        local i = 0
        while i < this.activeSkillIcons.Length do
            if this.activeSkillIcons[i].gameObject == go then
                if CClientMainPlayer.Inst ~= nil then
                    if CClientMainPlayer.Inst.SkillProp.NewActiveSkillForAI[i + 1 + startIndex] == SkillButtonSkillType_lua.NoSkill then
                        return
                    end
                    local skillId = this:GetSkillDetailId(CClientMainPlayer.Inst.SkillProp.NewActiveSkillForAI[i + 1 + startIndex])
                    CSkillInfoMgr.ShowSkillInfoWnd(skillId, this.tipTopPos.position, CSkillInfoMgr.EnumSkillInfoContext.MainPlayerMPTZ, true, 0, 0, nil)
                end
                break
            end
            i = i + 1
        end
    end
end

local canReplace = function (this, skillId)
    if LuaPVPMgr.IsRobotOpen() then
        local skillCls = CLuaDesignMgr.Skill_AllSkills_GetClass(skillId)
        local propertySkill = CClientMainPlayer.Inst.SkillProp
        local aiSkillType = LuaPVPMgr.GetAISkillType()
        local startIndex = (EnumToInt(aiSkillType) - 1) * CPropertySkill.ActiveSkillCount
        for i = 1, this.activeSkillIcons.Length  do
            if skillCls == propertySkill.NewActiveSkillForAI[i + startIndex] then
                return false
            end
        end
    end
    return true
end


CPVPSkillPreferenceBottomView.m_ReplaceSkill_CS2LuaHook = function (this, srcSkillId, go, srcActiveSkillIndex) 
    if CClientMainPlayer.Inst == nil or not Skill_AllSkills.Exists(srcSkillId) then
        return
    end
    local propertySkill = CClientMainPlayer.Inst.SkillProp
    local aiSkillType = LuaPVPMgr.GetAISkillType()
    local startIndex = (EnumToInt(aiSkillType) - 1) * CPropertySkill.ActiveSkillCount

    local needSwitch = (srcActiveSkillIndex > 0 and srcActiveSkillIndex < propertySkill.NewActiveSkillForAI.Length and propertySkill.NewActiveSkillForAI[srcActiveSkillIndex + startIndex] == math.floor(srcSkillId / 100))
    if not needSwitch and not canReplace(this, srcSkillId) then
        MessageMgr.Inst:ShowMessage("PVP_SKILL_CAN_NOT_SAME", {})
        return
    end

    do
        local i = 0
        while i < this.activeSkillIcons.Length do
            if this.activeSkillIcons[i].gameObject:Equals(go) then
                local destSkillId = SkillButtonSkillType_lua.NoSkill
                if SkillButtonSkillType_lua.NoSkill ~= propertySkill.NewActiveSkillForAI[i + 1 + startIndex] then
                    destSkillId = this:GetSkillDetailId(propertySkill.NewActiveSkillForAI[i + 1 + startIndex])
                end

                --if (destSkillId != (uint)SkillButtonSkillType.NoSkill && destSkillId != srcSkillId)
                if destSkillId ~= srcSkillId then
                    local srcRemain = CClientMainPlayer.Inst.CooldownProp:GetServerRemainTime(srcSkillId)
                    local dstRemain = CClientMainPlayer.Inst.CooldownProp:GetServerRemainTime(destSkillId)
                    if dstRemain <= 0 then
                        local normalSkills = CreateFromClass(MakeGenericClass(List, Object))
                        if not needSwitch then
                            --将skillID对应的技能设置到第(i+1)个位置,色系技能批量替换
                            do
                                local j = 1
                                while j <= this.activeSkillIcons.Length do
                                    if j == i + 1 or this:IsInSameColorSystem(math.floor(srcSkillId / 100), propertySkill.NewActiveSkillForAI[j + startIndex]) then
                                        CommonDefs.ListAdd(normalSkills, typeof(UInt32), math.floor(srcSkillId / 100))
                                    else
                                        CommonDefs.ListAdd(normalSkills, typeof(UInt32), propertySkill.NewActiveSkillForAI[j + startIndex])
                                    end
                                    j = j + 1
                                end
                            end
                        else
                            --交换srcActiveSkillId和i+1位置的技能
                            do
                                local j = 1
                                while j <= this.activeSkillIcons.Length do
                                    if j == i + 1 then
                                        CommonDefs.ListAdd(normalSkills, typeof(UInt32), math.floor(srcSkillId / 100))
                                    elseif j == srcActiveSkillIndex then
                                        CommonDefs.ListAdd(normalSkills, typeof(UInt32), propertySkill.NewActiveSkillForAI[i + 1 + startIndex])
                                    else
                                        CommonDefs.ListAdd(normalSkills, typeof(UInt32), propertySkill.NewActiveSkillForAI[j + startIndex])
                                    end
                                    j = j + 1
                                end
                            end
                        end
                        --							List<object> tianfuSkills = new List<object>();
                        --							propertySkill.ForeachOriginalSkillId((KeyValuePair<uint, uint> kv) =>
                        --							                                     {
                        --								uint skillId = kv.Key;
                        --								Skill_AllSkills skillTemp = Skill_AllSkills.GetData(skillId);
                        --								if (skillTemp.Kind == (int)SkillKind.TianFuSkill)
                        --									tianfuSkills.Add(skillId);
                        --							});
                        --							Gac2Gas.RequestSaveSkill2Template(propertySkill.CurrentTemplateIdx, 
                        --							                                  CClientMainPlayer.Inst.SkillProp.CurrentSkillTemplateName, 
                        --							                                  MsgPackImpl.pack(normalSkills), 
                        --							                                  MsgPackImpl.pack(tianfuSkills)
                        --                                                            MsgPackImpl.pack(new List<uint>()));
                        local tianfuSkills = CreateFromClass(MakeGenericClass(List, Object))
                        do
                            local j = 0
                            while j < Constants.NumOfAvailableMPTZTianFuSkills do
                                CommonDefs.ListAdd(tianfuSkills, typeof(UInt32), CClientMainPlayer.Inst.SkillProp.TianFuSkillForAI[j+LuaPVPMgr.GetTianFuBaseIndex()])
                                j = j + 1
                            end
                        end

                        local srcSkillCls = CLuaDesignMgr.Skill_AllSkills_GetClass(srcSkillId) --浩神新增参数用于影灵技能形态的判断
                        if LuaPVPMgr.IsRobotOpen() then
                            -- CommonDefs.ListAdd(normalSkills, typeof(UInt32), LuaPVPMgr.m_CurrentYingLingState)
                            Gac2Gas.SetActiveSkillForRobot(LuaPVPMgr.GetAISkillType(), MsgPackImpl.pack(normalSkills), MsgPackImpl.pack(tianfuSkills), this.firstJuejiCheckbox.Selected, this.secondJuejiCheckbox.Selected, srcSkillCls, LuaPVPMgr.m_CurrentYingLingState or 0)
                        else
                            Gac2Gas.SetActiveSkillForAI(AISkillType_lua.MPTZ, MsgPackImpl.pack(normalSkills), MsgPackImpl.pack(tianfuSkills), srcSkillCls)
                        end
                    else
                        g_MessageMgr:ShowMessage("SKILL_CHANGE_FAILED_COOLDOWN")
                    end
                end
                break
            end
            i = i + 1
        end
    end
end
CPVPSkillPreferenceBottomView.m_LoadActiveSkills_CS2LuaHook = function (this) 
    if CClientMainPlayer.Inst == nil then
        return
    end

    local isYingLing = CClientMainPlayer.Inst.IsYingLing
    if isYingLing then
        this.yinglingHintLabel.gameObject:SetActive(true)
        local curState = LuaPVPMgr.GetCurrentYingLingState()
        this.yinglingHintLabel.text = g_MessageMgr:FormatMessage("MenPaiTiaoZhan_YingLing_ReadMe", LuaPVPMgr.GetYingLingStateName(curState))
    else
        this.yinglingHintLabel.gameObject:SetActive(false)
    end

    local skillClsIds = CClientMainPlayer.Inst.SkillProp.NewActiveSkillForAI
    --ActiveSkill索引从1开始，第0个位置占位，无用
    local isOpenRobot = LuaPVPMgr.IsRobotOpen()
    local aiSkillType = LuaPVPMgr.GetAISkillType()
    local startIndex = (EnumToInt(aiSkillType) - 1) * CPropertySkill.ActiveSkillCount

    if isOpenRobot and CClientMainPlayer.Inst.SkillProp.IsMPTZRobotSkillInited == 0 then
        -- 还未设置过
        if not isYingLing then
            local autoSkills = LuaPVPMgr.GetAutoAISkills()
            for i = 1, skillClsIds.Length-1 do
                if i <= #autoSkills then
                    skillClsIds[i] = autoSkills[i]
                end
            end
        else
            local autoSkills = LuaPVPMgr.GetAutoAISkills(1)
            for i = 1, skillClsIds.Length-1 do
                if i <= #autoSkills then
                    skillClsIds[i + 5] = autoSkills[i]
                end
            end

            local autoSkills = LuaPVPMgr.GetAutoAISkills(2)
            for i = 1, skillClsIds.Length-1 do
                if i <= #autoSkills then
                    skillClsIds[i + 10] = autoSkills[i]
                end
            end

            local autoSkills = LuaPVPMgr.GetAutoAISkills(3)
            for i = 1, skillClsIds.Length-1 do
                if i <= #autoSkills then
                    skillClsIds[i + 15] = autoSkills[i]
                end
            end
        end      
    end 

    do
        local i = 0
        while i < this.activeSkillIcons.Length do
            UIEventListener.Get(this.activeSkillIcons[i].gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.activeSkillIcons[i].gameObject).onClick, MakeDelegateFromCSFunction(this.OnActiveSkillIconClick, VoidDelegate, this), true)
            if i + 1 + startIndex >= skillClsIds.Length or not Skill_AllSkills.Exists(CLuaDesignMgr.Skill_AllSkills_GetSkillId(skillClsIds[i + 1 + startIndex], 1)) then
                this.activeSkillIcons[i]:LoadSkillIcon(nil)
                this.activeSkillColorIcons[i].gameObject:SetActive(false)
            else
                this.activeSkillIcons[i]:LoadSkillIcon(Skill_AllSkills.GetData(this:GetSkillDetailId(skillClsIds[i + 1 + startIndex])).SkillIcon)
                local classId = skillClsIds[i + 1 + startIndex]
                if CSkillMgr.Inst:IsColorSystemSkill(classId) or CSkillMgr.Inst:IsColorSystemBaseSkill(classId) then
                    this.activeSkillColorIcons[i].gameObject:SetActive(true)
                    this.activeSkillColorIcons[i].spriteName = CSkillMgr.Inst:GetColorSystemSkillIcon(classId)
                else
                    this.activeSkillColorIcons[i].gameObject:SetActive(false)
                end
            end
            i = i + 1
        end
    end
    
    -- 隐藏优先级条 
    hideOrShowArrows(this, isOpenRobot)

    -- 修改文字描述
    local hintLabel = this.transform:Find("SkillButtonRoot/HintLabel"):GetComponent(typeof(UILabel))
    if hintLabel then
        if isOpenRobot then
            if isYingLing then
                hintLabel.text = LocalString.GetString("可分别设置三种形态的五个技能，技能不能重复，系统自动判定释放顺序")
            else
                hintLabel.text = LocalString.GetString("一共可以携带五个普通技能和两个绝技，技能不能重复，系统智能判定释放顺序")
            end
        else
            hintLabel.text = LocalString.GetString("一共可以配置五个技能，系统将从左向右优先释放你配置的技能")
        end
    end

    if isOpenRobot then
        this.firstJuejiCheckbox.Text = LocalString.GetString("携带[F5CB06]70[-]级绝技")
        this.secondJuejiCheckbox.Text = LocalString.GetString("携带[F5CB06]100[-]级绝技")
    else
        this.firstJuejiCheckbox.Text = LocalString.GetString("自动释放[F5CB06]70级[-]绝技")
        this.secondJuejiCheckbox.Text = LocalString.GetString("自动释放[F5CB06]100级[-]绝技")
    end
end

CPVPSkillPreferenceBottomView.m_OnActiveSkillIconClick_CS2LuaHook = function (this, go) 
    if CClientMainPlayer.Inst == nil then
        return
    end
    local aiSkillType = LuaPVPMgr.GetAISkillType()
    local startIndex = (EnumToInt(aiSkillType) - 1) * CPropertySkill.ActiveSkillCount
    do
        local i = 0
        while i < this.activeSkillIcons.Length do
            if this.activeSkillIcons[i].gameObject == go then
                local skillIds = CClientMainPlayer.Inst.SkillProp.NewActiveSkillForAI
                --ActiveSkill索引从1开始，第0个位置占位，无用
                if i + 1 > 0 and i + 1 < skillIds.Length and skillIds[i + 1 + startIndex] ~= SkillButtonSkillType_lua.NoSkill then
                    --CSkillInfoMgr.ShowSkillInfoWnd(skillIds[i + 1]);
                end

                break
            end
            i = i + 1
        end
    end
end


local getNormalSkills = function (this)
    local normalSkills = CreateFromClass(MakeGenericClass(List, Object))
    local aiSkillType = LuaPVPMgr.GetAISkillType()
    local startIndex = (EnumToInt(aiSkillType) - 1) * CPropertySkill.ActiveSkillCount
    for i = 1, this.activeSkillIcons.Length do
        CommonDefs.ListAdd(normalSkills, typeof(UInt32), CClientMainPlayer.Inst.SkillProp.NewActiveSkillForAI[i + startIndex])
    end
    return normalSkills
end

local getTianFuSkills = function (this)
    local tianfuSkills = CreateFromClass(MakeGenericClass(List, Object))
    do
        local j = 0
        while j < Constants.NumOfAvailableMPTZTianFuSkills do
            CommonDefs.ListAdd(tianfuSkills, typeof(UInt32), CClientMainPlayer.Inst.SkillProp.TianFuSkillForAI[j+LuaPVPMgr.GetTianFuBaseIndex()])
            j = j + 1
        end
    end
    return tianfuSkills
end


CPVPSkillPreferenceBottomView.m_hookOnFirstJuejiCheckboxValueChanged = function (this, value)
    if CClientMainPlayer.Inst then
        if LuaPVPMgr.IsRobotOpen() then
            local normalSkills = getNormalSkills(this)
            local tianfuSkills = getTianFuSkills(this)
            -- CommonDefs.ListAdd(normalSkills, typeof(UInt32), LuaPVPMgr.m_CurrentYingLingState)
            Gac2Gas.SetActiveSkillForRobot(LuaPVPMgr.GetAISkillType(), MsgPackImpl.pack(normalSkills), MsgPackImpl.pack(tianfuSkills), value, CClientMainPlayer.Inst.SkillProp.IsMPTZCast109Skill == 1, 0, LuaPVPMgr.m_CurrentYingLingState or 0)
        else
            local setValue = 1
            if not value then
                setValue = 0
            end
            Gac2Gas.SetMPTZAISkill(setValue, CClientMainPlayer.Inst.SkillProp.IsMPTZCast109Skill)
        end
    end
    
end

CPVPSkillPreferenceBottomView.m_hookOnSecondJuejiCheckboxValueChanged = function (this, value)
    if CClientMainPlayer.Inst then
        if LuaPVPMgr.IsRobotOpen() then
            local normalSkills = getNormalSkills(this)
            local tianfuSkills = getTianFuSkills(this)
            -- CommonDefs.ListAdd(normalSkills, typeof(UInt32), LuaPVPMgr.m_CurrentYingLingState)
            Gac2Gas.SetActiveSkillForRobot(LuaPVPMgr.GetAISkillType(), MsgPackImpl.pack(normalSkills), MsgPackImpl.pack(tianfuSkills), CClientMainPlayer.Inst.SkillProp.IsMPTZCast79Skill == 1, value, 0, LuaPVPMgr.m_CurrentYingLingState or 0)
        else
            if CClientMainPlayer.Inst then
                local setValue = 1
                if not value then
                    setValue = 0
                end
                Gac2Gas.SetMPTZAISkill(CClientMainPlayer.Inst.SkillProp.IsMPTZCast79Skill, setValue)
            end
        end
    end
end

CPVPSkillPreferenceBottomView.m_hookOnChangeYingLingState = function (this)
    this:LoadActiveSkills()

    local persistPlayData = CClientMainPlayer.Inst.PlayProp.PersistPlayData
    local data = CommonDefs.DictGetValue(persistPlayData, typeof(UInt32), LuaPVPMgr.eYingLingRobotDefaultSkill)
    if data then
        local datastring = data.StringData
        if datastring ~= nil and datastring ~= "" and datastring ~= "0" then
            local curYingLingState4MPTZ = tonumber(datastring)
            if curYingLingState4MPTZ == LuaPVPMgr.m_CurrentYingLingState then
                return
            end
        end
    end

    local normalSkills = getNormalSkills(this)
    local tianfuSkills = getTianFuSkills(this)
    -- CommonDefs.ListAdd(normalSkills, typeof(UInt32), LuaPVPMgr.m_CurrentYingLingState)
    local takeFirstJueJi = CClientMainPlayer.Inst.SkillProp.IsMPTZCast79Skill > 0 and CClientMainPlayer.Inst.SkillProp:IsOriginalSkillClsExist(CClientMainPlayer.Inst.FirstJuejiSkillCls)
    local takeSecondJueJi = CClientMainPlayer.Inst.SkillProp.IsMPTZCast109Skill > 0 and CClientMainPlayer.Inst.SkillProp:IsOriginalSkillClsExist(CClientMainPlayer.Inst.SecondJuejiSkillCls)
    Gac2Gas.SetActiveSkillForRobot(LuaPVPMgr.GetAISkillType(), MsgPackImpl.pack(normalSkills), MsgPackImpl.pack(tianfuSkills), takeFirstJueJi, takeSecondJueJi, 0, LuaPVPMgr.m_CurrentYingLingState or 0)
end

LuaPVPSkillPreferenceBottomView = {}

LuaPVPSkillPreferenceBottomView.m_Wnd = nil

function LuaPVPSkillPreferenceBottomView:OnEnable(wnd)
    LuaPVPSkillPreferenceBottomView.m_Wnd = wnd
    g_ScriptEvent:AddListener("OnChangeYingLingMenPaiSkillConflict", self, "OnChangeYingLingMenPaiSkillConflict")
end

function LuaPVPSkillPreferenceBottomView:OnDisable()
    LuaPVPSkillPreferenceBottomView.m_Wnd = nil
    g_ScriptEvent:RemoveListener("OnChangeYingLingMenPaiSkillConflict", self, "OnChangeYingLingMenPaiSkillConflict")
end

function LuaPVPSkillPreferenceBottomView:OnChangeYingLingMenPaiSkillConflict(toChangeYingLingState)
    if not CClientMainPlayer.Inst or not CClientMainPlayer.Inst.IsYingLing then
        return
    end
    MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("MenPaiTiaoZhan_YingLing_ChangeSkillStatus_Confrim",LuaPVPMgr.GetYingLingStateName(toChangeYingLingState)),
        DelegateFactory.Action(function() 
            
            --重置技能
            self:InitSkill(self.m_Wnd, true, toChangeYingLingState)

        end),nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
end

function LuaPVPSkillPreferenceBottomView:InitSkill(this, forceInit, yinglingState)
    --从原来的CPVPSkillPreferenceBottomView OnEnable中搬过来，方便复用
    if LuaPVPMgr.IsRobotOpen() then
        if CClientMainPlayer.Inst.SkillProp.IsMPTZRobotSkillInited == 0 or forceInit then
            -- 还未设置过
            local autoSkills = LuaPVPMgr.GetAutoAISkills(yinglingState)
            local normalSkills = CreateFromClass(MakeGenericClass(List, Object))
            for i = 1, this.activeSkillIcons.Length do
                if i <= #autoSkills then
                    CommonDefs.ListAdd(normalSkills, typeof(UInt32), autoSkills[i])
                else
                    CommonDefs.ListAdd(normalSkills, typeof(UInt32), 0)
                end
            end

            local takeFirstJueJi = CClientMainPlayer.Inst.SkillProp.IsMPTZCast79Skill > 0 and CClientMainPlayer.Inst.SkillProp:IsOriginalSkillClsExist(CClientMainPlayer.Inst.FirstJuejiSkillCls)
            local takeSecondJueJi = CClientMainPlayer.Inst.SkillProp.IsMPTZCast109Skill > 0 and CClientMainPlayer.Inst.SkillProp:IsOriginalSkillClsExist(CClientMainPlayer.Inst.SecondJuejiSkillCls)           
            -- CommonDefs.ListAdd(normalSkills, typeof(UInt32), LuaPVPMgr.m_CurrentYingLingState)
            Gac2Gas.SetActiveSkillForRobot(LuaPVPMgr.GetAISkillType(), MsgPackImpl.pack(normalSkills), MsgPackImpl.pack(CreateFromClass(MakeGenericClass(List, Object))), takeFirstJueJi, takeSecondJueJi, 0, LuaPVPMgr.m_CurrentYingLingState or 0)
            CClientMainPlayer.Inst.SkillProp.IsMPTZRobotSkillInited = 1
        end
        
    else
        if math.floor(tonumber(CClientMainPlayer.Inst.SkillProp.ActiveSkillForAIInited[EnumToInt((AISkillType.MPTZ))] or 0)) == 0 or forceInit then
            local normalSkills = CreateFromClass(MakeGenericClass(List, Object))
            if CClientMainPlayer.Inst.IsYingLing then
                --从MenPaiTiaoZhan表的Setting中读取
                local skillClsIds = LuaPVPMgr.GetYingLingDefaultSkills(yinglingState)
                if skillClsIds then
                    for j=0, this.activeSkillIcons.Length - 1 do
                        --这里目前假定策划表中配了5个技能，必须和技能面板上的按钮数目保持一致,注意下标，策划表里面从0开始的
                        local skillId = CClientMainPlayer.Inst.SkillProp:GetSkillIdWithDeltaByCls(skillClsIds[j], CClientMainPlayer.Inst.Level)

                        if this:checkSkillAllowed(skillClsIds[j]) then
                            CommonDefs.ListAdd(normalSkills, typeof(UInt32), skillClsIds[j])
                        else
                            CommonDefs.ListAdd(normalSkills, typeof(UInt32), SkillButtonSkillType_lua.NoSkill)
                        end
                    end
                end
            else
                --从第一套常规技能模板里面尝试取一下
                for j=1, this.activeSkillIcons.Length do
                    local skillId = CClientMainPlayer.Inst.SkillProp.NewActiveSkillTemplate[j]
                    if this:checkSkillAllowed(skillId) then
                        CommonDefs.ListAdd(normalSkills, typeof(UInt32), skillId)
                    else
                        CommonDefs.ListAdd(normalSkills, typeof(UInt32), SkillButtonSkillType_lua.NoSkill)
                    end
                end
            end
            Gac2Gas.SetActiveSkillForAI(AISkillType_lua.MPTZ, MsgPackImpl.pack(normalSkills), MsgPackImpl.pack(CreateFromClass(MakeGenericClass(List, Object))), 0)
        end
    end
end
