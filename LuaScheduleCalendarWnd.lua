local CServerTimeMgr=import "L10.Game.CServerTimeMgr"

CLuaScheduleCalendarWnd=class()
RegistClassMember(CLuaScheduleCalendarWnd,"m_TitleTemplate")
RegistClassMember(CLuaScheduleCalendarWnd,"m_TitleTemplate2")
RegistClassMember(CLuaScheduleCalendarWnd,"m_Item1")
RegistClassMember(CLuaScheduleCalendarWnd,"m_Item2")
RegistClassMember(CLuaScheduleCalendarWnd,"m_Today")
RegistClassMember(CLuaScheduleCalendarWnd,"m_InfoDic")


function CLuaScheduleCalendarWnd:Awake()
    Gac2Gas.QueryWeeklyInfo()

    self.m_InfoDic={}
    self.m_TitleTemplate=FindChild(self.transform,"ItemTemplate").gameObject
    self.m_TitleTemplate:SetActive(false)
    self.m_TitleTemplate2=FindChild(self.transform,"ItemTemplate2").gameObject
    self.m_TitleTemplate2:SetActive(false)
    self.m_Item1=FindChild(self.transform,"Item1").gameObject
    self.m_Item1:SetActive(false)
    self.m_Item2=FindChild(self.transform,"Item2").gameObject
    self.m_Item2:SetActive(false)

    local weekDayNames={
        LocalString.GetString("周一"),
        LocalString.GetString("周二"),
        LocalString.GetString("周三"),
        LocalString.GetString("周四"),
        LocalString.GetString("周五"),
        LocalString.GetString("周六"),
        LocalString.GetString("周日"),
    }
    local days={1,2,3,4,5,6,0}
    local dayOfWeek=EnumToInt(CServerTimeMgr.Inst:GetZone8Time().DayOfWeek)
    local titlesTf=FindChild(self.transform,"Titles")
    local index=0
    for i,v in ipairs(days) do
        if dayOfWeek==v then
            index=i
        end
    end
    self.m_Today=index
    for i=1,7 do
        local go=nil
        if i==index then--变宽
            go=NGUITools.AddChild(titlesTf.gameObject,self.m_TitleTemplate2)
        else
            go=NGUITools.AddChild(titlesTf.gameObject,self.m_TitleTemplate)
        end
        go:SetActive(true)
        go.transform:Find("Label"):GetComponent(typeof(UILabel)).text=weekDayNames[i]
    end
    titlesTf:GetComponent(typeof(UITable)):Reposition()
end


function CLuaScheduleCalendarWnd:OnEnable()
    -- g_ScriptEvent:AddListener("GetWeeklyInfo", self, "OnGetWeeklyInfo")
    g_ScriptEvent:AddListener("SendWeeklyInfo", self, "OnSendWeeklyInfo")
    
end

function CLuaScheduleCalendarWnd:OnDisable()
    -- g_ScriptEvent:RemoveListener("GetWeeklyInfo", self, "OnGetWeeklyInfo")
    g_ScriptEvent:RemoveListener("SendWeeklyInfo", self, "OnSendWeeklyInfo")

end

function CLuaScheduleCalendarWnd:OnSendWeeklyInfo(infos)
    -- local list=CScheduleWeeklyInfoMgr.Inst.infos

    local lookup={}
    -- for
    for i=1,7 do
        lookup[i]={}
    end
    for i=1,#infos do
        local info = infos[i]
        local scheduleData=Task_Schedule.GetData(info.activityId)
        if scheduleData and scheduleData.IsSegmented>0 then
            local pre=lookup[info.day+1][info.activityId]
            if pre then
                if pre.hour*24+pre.minute>info.hour*60+info.minute then
                    lookup[info.day+1][info.activityId]=info
                end
            else
                lookup[info.day+1][info.activityId]=info
            end
        else
            lookup[info.day+1][info.activityId]=info
        end
    end

    local titlesTf=FindChild(self.transform,"Titles")

    for i,v in ipairs(lookup) do
        --排序
        local t={}
        for id,info in pairs(v) do
            table.insert( t,info )
        end
        table.sort( t, function(a,b)
            local time1=a.hour*60+a.minute
            local time2=b.hour*60+b.minute
            if time1>time2 then
                return false
            elseif time1<time2 then
                return true
            else
                return a.activityId>a.activityId
            end
        end )
        lookup[i]=t
    end
    for i,v in ipairs(lookup) do
        -- print(i)
        local grid=FindChild(titlesTf:GetChild(i-1),"Grid")
        CUICommonDef.ClearTransform(grid)
        
        for id,info in pairs(v) do
            local go=nil
            if i==self.m_Today then
                go=NGUITools.AddChild(grid.gameObject,self.m_Item2)
                go:SetActive(true)
                self:InitItem2(go,info)
            else
                go=NGUITools.AddChild(grid.gameObject,self.m_Item1)
                go:SetActive(true)
                self:InitItem(go,info)
            end
            self.m_InfoDic[go]=info
            
            UIEventListener.Get(go).onClick=DelegateFactory.VoidDelegate(function(go)
                self:OnClickItem(go)
            end)
        end
        grid:GetComponent(typeof(UIGrid)):Reposition()
    end

end
function CLuaScheduleCalendarWnd:OnClickItem(go)
    local info=self.m_InfoDic[go]
    if info then
        CLuaScheduleMgr.selectedScheduleInfo=nil
        CLuaScheduleMgr.selectedWeeklyScheduleInfo = info
        CUIManager.ShowUI(CLuaUIResources.ScheduleInfoWnd)
    end
end

function CLuaScheduleCalendarWnd:InitItem(go,info)
    -- go.transform:Find("Sprite").gameObject:SetActive(false)
    go.transform:Find("NameLabel"):GetComponent(typeof(UILabel)).text=info.name
    if CLuaScheduleMgr.IsShowDoubleScore(info.activityId,info.day) then
        go.transform:Find("Double").gameObject:SetActive(true)
    else
        go.transform:Find("Double").gameObject:SetActive(false)
    end
end
function CLuaScheduleCalendarWnd:InitItem2(go,info)
    -- go.transform:Find("Sprite").gameObject:SetActive(false)
    go.transform:Find("NameLabel"):GetComponent(typeof(UILabel)).text=info.name
    go.transform:Find("TimeLabel"):GetComponent(typeof(UILabel)).text=SafeStringFormat3("%02d:%02d",info.hour,info.minute)
    if CLuaScheduleMgr.IsShowDoubleScore(info.activityId,info.day) then
        go.transform:Find("Double").gameObject:SetActive(true)
    else
        go.transform:Find("Double").gameObject:SetActive(false)
    end
end
return CLuaScheduleCalendarWnd
