-- Auto Generated!!
local CChristmasCardItem = import "L10.UI.CChristmasCardItem"
local CChristmasCardPopupMenu = import "L10.UI.CChristmasCardPopupMenu"
local CItemInfo = import "L10.Game.CItemInfo"
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CScene = import "L10.Game.CScene"
local DelegateFactory = import "DelegateFactory"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local MsgPackImpl = import "MsgPackImpl"
local Object = import "System.Object"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CChristmasCardPopupMenu.m_Init_CS2LuaHook = function (this) 
    this.m_TotalEditedItems = this:GetEditedChristmasCardItems()
    this.m_TableView.m_DataSource = this
    this.m_TableView:ReloadData(true, true)
    this.m_TableView.OnSelectAtRow = MakeDelegateFromCSFunction(this.OnSelectAtRow, MakeGenericClass(Action1, Int32), this)
    UIEventListener.Get(this.m_CommitButton.gameObject).onClick = MakeDelegateFromCSFunction(this.OnClickCommitButton, VoidDelegate, this)
    this.m_SelectCountLabel.text = System.String.Format(LocalString.GetString("已选择挂上的贺卡 {0}/{1}"), this.m_SelectItems.Count, this.m_TotalEditedItems.Count)
end
CChristmasCardPopupMenu.m_ItemAt_CS2LuaHook = function (this, view, row) 
    local item = TypeAs(view:GetFromPool(0), typeof(CChristmasCardItem))
    local extraData = this:GetItemExtraData(this.m_TotalEditedItems[row].itemId)
    --{recieverId, recieverName, wishContent, voiceUrl, voiceDuration}
    if extraData ~= nil and extraData.Count >= 5 then
        local recieverName = ToStringWrap(extraData[1])
        local wishContent = ToStringWrap(extraData[2])
        item:UpdateData(recieverName, wishContent)
    end
    return item
end
CChristmasCardPopupMenu.m_OnSelectAtRow_CS2LuaHook = function (this, row) 
    local itemInfo = this.m_TotalEditedItems[row]
    if CommonDefs.HashSetContains(this.m_SelectItems, typeof(CItemInfo), itemInfo) then
        CommonDefs.HashSetRemove(this.m_SelectItems, typeof(CItemInfo), itemInfo)
        local item = TypeAs(this.m_TableView:GetItemAtRow(row), typeof(CChristmasCardItem))
        item:SelectItem(false)
    else
        if this.m_SelectItems.Count <= CChristmasCardPopupMenu.s_MaxSelectCount then
            CommonDefs.HashSetAdd(this.m_SelectItems, typeof(CItemInfo), itemInfo)
            local item = TypeAs(this.m_TableView:GetItemAtRow(row), typeof(CChristmasCardItem))
            item:SelectItem(true)
        else
            g_MessageMgr:ShowMessage("MAX_SELECT_CARD_LIMIT")
        end
    end
    this.m_SelectCountLabel.text = System.String.Format(LocalString.GetString("已选择挂上的贺卡 {0}/{1}"), this.m_SelectItems.Count, this.m_TotalEditedItems.Count)
end
CChristmasCardPopupMenu.m_OnClickCommitButton_CS2LuaHook = function (this, go) 
    if this.m_SelectItems.Count > 0 then
        CommonDefs.EnumerableIterate(this.m_SelectItems, DelegateFactory.Action_object(function (___value) 
            local itemInfo = ___value
            local sceneTemplateId = CScene.MainScene.SceneTemplateId
            Gac2Gas.RequestPutCardIntoTree(sceneTemplateId, itemInfo.itemId, EnumToInt(itemInfo.place), itemInfo.pos)
        end))
        this:Close()
    else
        g_MessageMgr:ShowMessage("NO_CARD_SELECTED")
    end
end
CChristmasCardPopupMenu.m_GetItemExtraData_CS2LuaHook = function (this, itemId) 
    local item = CItemMgr.Inst:GetById(itemId)
    if item.IsItem and item.Item.ExtraVarData ~= nil and item.Item.ExtraVarData.Data ~= nil then
        local data = item.Item.ExtraVarData.Data
        local ret = TypeAs(MsgPackImpl.unpack(data), typeof(MakeGenericClass(List, Object)))
        return ret
    end
    return CreateFromClass(MakeGenericClass(List, Object))
end
