local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
CLuaRanFaMgr = {}
CLuaRanFaMgr.RanFaEnabled = true

EnumSyncRanFaJiType = {
    eDefault = 0, --仅请求数据，不打开界面
    eOpenMainWndRecipeTab = 1, --打开染发主界面-配方
    eOpenMainWndLibraryTab = 2, --打开染发主界面-发色库

}

CLuaRanFaMgr.m_NeedShowRecipeId = 0
function CLuaRanFaMgr:OpenRecipeWnd(recipeId)
	CLuaRanFaMgr.m_OpenWndType = 1
	CLuaRanFaMgr.m_NeedShowRecipeId = recipeId
	CLuaRanFaMgr:RequestSyncRanFaJiPlayData(EnumSyncRanFaJiType.eOpenMainWndRecipeTab)
end


function CLuaRanFaMgr:RequestSyncRanFaJiPlayData(syncType)
    Gac2Gas.RequestSyncRanFaJiPlayData(syncType or 0)
end

function CLuaRanFaMgr:TakeOffAdvancedHairColor(id)
    g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("AdvancedRanfaji_Fade_Attention"), function ()
        Gac2Gas.TakeOffAdvancedHairColor(id)
    end, nil, nil, nil, false)
end

function CLuaRanFaMgr:GetMainPlayerVisibleHeadFashion()
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.AppearanceProp.HideHeadFashionEffect <= 0 and CClientMainPlayer.Inst.AppearanceProp.HideHelmetEffect <= 0 and CClientMainPlayer.Inst.AppearanceProp.HeadFashionId > 0 and not EquipmentTemplate_Equip.Exists(CClientMainPlayer.Inst.AppearanceProp.HeadFashionId) then
        local fashionId = CClientMainPlayer.Inst.AppearanceProp.HeadFashionId
        return fashionId
    else
        return 0
    end
end

function CLuaRanFaMgr:CheckHeadFashion()
    --原来的fashionId>0判断有点迷惑，暂时先保持原来的处理，新外观里面直接使用AppearanceProp.HeadFashionId，忽略else分支中的提示消息
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.AppearanceProp.HideHeadFashionEffect <= 0 and CClientMainPlayer.Inst.AppearanceProp.HideHelmetEffect <= 0 and CClientMainPlayer.Inst.AppearanceProp.HeadFashionId > 0 and not EquipmentTemplate_Equip.Exists(CClientMainPlayer.Inst.AppearanceProp.HeadFashionId) then
        local fashionId = CClientMainPlayer.Inst.AppearanceProp.HeadFashionId
        if fashionId > 0 then
            g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("RanFaJi_Fashion_TakeOff"), function()
                LuaAppearancePreviewMgr:RequestTakeOffFashion_Permanent(fashionId)
            end, nil, nil, nil, false)
        else
            g_MessageMgr:ShowMessage("CUSTOM_STRING1", LocalString.GetString("该套时装无法进行染发，请更换后再试"))
        end
    end
end

function CLuaRanFaMgr:TakeOnAdvancedHairColor(id)
    g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("ChangeOrUseRanFaJi_AdvancedColor"), function()
        if CClientMainPlayer.Inst and CClientMainPlayer.Inst.AppearanceProp.HideHeadFashionEffect <= 0 and CClientMainPlayer.Inst.AppearanceProp.HideHelmetEffect <= 0 
            and CClientMainPlayer.Inst.AppearanceProp.HeadFashionId > 0 and not EquipmentTemplate_Equip.Exists(CClientMainPlayer.Inst.AppearanceProp.HeadFashionId) then
            local fashionId = self:GetMainPlayerVisibleHeadFashion()
            if fashionId > 0 then
                g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("RanFaJi_Fashion_TakeOff"), function()
                    LuaAppearancePreviewMgr:RequestTakeOffFashion_Permanent(fashionId)
                    Gac2Gas.TakeOnAdvancedHairColor(id)
                end, nil, nil, nil, false)
            else
                g_MessageMgr:ShowMessage("CUSTOM_STRING1", LocalString.GetString("该套时装无法进行染发，请更换后再试"))
            end
        else
            Gac2Gas.TakeOnAdvancedHairColor(id)
        end
    end, nil, nil, nil, false)
end


CLuaRanFaMgr.m_ChooseItemInfo = {}

CLuaRanFaMgr.m_NewRecipeId = 0
CLuaRanFaMgr.m_OwnRanFaJiTable = {}
CLuaRanFaMgr.m_RanFaJiTable = {}
CLuaRanFaMgr.m_RecipeTable = {}
CLuaRanFaMgr.m_OpenWndType = 0
CLuaRanFaMgr.m_MyRanFaJiId = 0
