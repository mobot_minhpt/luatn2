local DelegateFactory        = import "DelegateFactory"
local CPlayerInfoMgr         = import "CPlayerInfoMgr"
local EChatPanel             = import "L10.Game.EChatPanel"
local EnumPlayerInfoContext  = import "L10.Game.EnumPlayerInfoContext"
local AlignType              = import "CPlayerInfoMgr+AlignType"
local CQnReturnAwardTemplate = import "L10.UI.CQnReturnAwardTemplate"
local Item_Item              = import "L10.Game.Item_Item"
local CServerTimeMgr         = import "L10.Game.CServerTimeMgr"
local Animation              = import "UnityEngine.Animation"

LuaValentine2023YWQSRankWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaValentine2023YWQSRankWnd, "tab_Rank")
RegistClassMember(LuaValentine2023YWQSRankWnd, "tab_Award")
RegistClassMember(LuaValentine2023YWQSRankWnd, "curTab")

RegistClassMember(LuaValentine2023YWQSRankWnd, "rank")
RegistClassMember(LuaValentine2023YWQSRankWnd, "rank_Info")
RegistClassMember(LuaValentine2023YWQSRankWnd, "noPlayerInRank")
RegistClassMember(LuaValentine2023YWQSRankWnd, "we_Info")
RegistClassMember(LuaValentine2023YWQSRankWnd, "we_NoPartner")
RegistClassMember(LuaValentine2023YWQSRankWnd, "we_RankNum")
RegistClassMember(LuaValentine2023YWQSRankWnd, "we_Player1")
RegistClassMember(LuaValentine2023YWQSRankWnd, "we_Player1Name")
RegistClassMember(LuaValentine2023YWQSRankWnd, "we_Player2")
RegistClassMember(LuaValentine2023YWQSRankWnd, "we_Player2Name")
RegistClassMember(LuaValentine2023YWQSRankWnd, "we_Score")
RegistClassMember(LuaValentine2023YWQSRankWnd, "we_VoteButton")
RegistClassMember(LuaValentine2023YWQSRankWnd, "we_VoteNum")
RegistClassMember(LuaValentine2023YWQSRankWnd, "rank_Template")
RegistClassMember(LuaValentine2023YWQSRankWnd, "rank_Grid")

RegistClassMember(LuaValentine2023YWQSRankWnd, "award")
RegistClassMember(LuaValentine2023YWQSRankWnd, "award_Grid")

function LuaValentine2023YWQSRankWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

    self.rank = self.transform:Find("Anchor/Rank").gameObject
    self.rank_Info = self.rank.transform:Find("Info").gameObject
    self.noPlayerInRank = self.transform:Find("Anchor/Rank/NoPlayerInRank").gameObject

    self.we_Info = self.rank_Info.transform:Find("We/Info").gameObject
    self.we_NoPartner = self.rank_Info.transform:Find("We/NoPartner").gameObject
    self.we_RankNum = self.we_Info.transform:Find("RankNum"):GetComponent(typeof(UILabel))
    self.we_Player1 = self.we_Info.transform:Find("Player1"):GetComponent(typeof(CUITexture))
    self.we_Player1Name = self.we_Info.transform:Find("Player1/Name"):GetComponent(typeof(UILabel))
    self.we_Player2 = self.we_Info.transform:Find("Player2"):GetComponent(typeof(CUITexture))
    self.we_Player2Name = self.we_Info.transform:Find("Player2/Name"):GetComponent(typeof(UILabel))
    self.we_Score = self.we_Info.transform:Find("Score"):GetComponent(typeof(UILabel))
    self.we_VoteButton = self.we_Info.transform:Find("VoteButton").gameObject
    self.we_VoteNum = self.we_Info.transform:Find("VoteNum"):GetComponent(typeof(UILabel))
    self.rank_Template = self.rank_Info.transform:Find("Rank/Template").gameObject
    self.rank_Grid = self.rank_Info.transform:Find("Rank/ScrollView/Grid"):GetComponent(typeof(UIGrid))

    self.award = self.transform:Find("Anchor/Award").gameObject

    UIEventListener.Get(self.transform:Find("Anchor/CloseButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCloseButtonClick()
	end)

    self:InitActive()
    self:InitTabs()
    self:InitAward()
end

function LuaValentine2023YWQSRankWnd:InitTabs()
    self.tab_Rank = self.transform:Find("Anchor/Tabs/Rank")
    UIEventListener.Get(self.tab_Rank.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTabClick(1)
	end)

    self.tab_Award = self.transform:Find("Anchor/Tabs/Award")
    UIEventListener.Get(self.tab_Award.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTabClick(2)
	end)
    self:OnTabClick(1)
end

function LuaValentine2023YWQSRankWnd:InitActive()
    self.rank_Template:SetActive(false)
    self.rank_Info:SetActive(false)
    self.noPlayerInRank:SetActive(false)
end

function LuaValentine2023YWQSRankWnd:InitAward()
    self.award_Grid = self.award.transform:Find("Grid")
    local itemTemplate = self.award.transform:Find("ItemTemplate").gameObject
    itemTemplate:SetActive(false)
    for i = 1, 4 do
        local data = Valentine2023_ListAward.GetData(i)
        local child = self.award_Grid:GetChild(i - 1)

        child:Find("RankNum"):GetComponent(typeof(UILabel)).text = data.Paiming
        child:Find("Name"):GetComponent(typeof(UILabel)).text = data.AwardDescription
        child:Find("We").gameObject:SetActive(false)

        local awardTable = child:Find("Table"):GetComponent(typeof(UITable))
        Extensions.RemoveAllChildren(awardTable.transform)
        for j = 0, data.Award.Length - 1 do
            local awardItem = NGUITools.AddChild(awardTable.gameObject, itemTemplate)
            awardItem:SetActive(true)

            local qnReturnAward = awardItem.transform:GetComponent(typeof(CQnReturnAwardTemplate))
            qnReturnAward:Init(Item_Item.GetData(data.Award[j]), 1)
        end
        awardTable:Reposition()
    end

    local extraAwardItem = self.award.transform:Find("ExtraAward"):GetComponent(typeof(CQnReturnAwardTemplate))
    extraAwardItem:Init(Item_Item.GetData(Valentine2023_YWQS_Setting.GetData().Top1ExtraAwardItemId), 1)

    UIEventListener.Get(self.award.transform:Find("TipButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)

    local diff = CServerTimeMgr.Inst:GetTimeStampByStr(Valentine2023_YWQS_Setting.GetData().RankEndTime) - CServerTimeMgr.Inst.timeStamp
    local tip = self.award.transform:Find("Tip"):GetComponent(typeof(UILabel))
    local day = 0
    if diff > 0 then
        day = math.ceil(diff / 3600 / 24)
    end
    tip.text = SafeStringFormat3(LocalString.GetString("%d天后 活动结束时发放上述奖励"), day)
end

function LuaValentine2023YWQSRankWnd:OnEnable()
    g_ScriptEvent:AddListener("SendValentineYWQSRankInfo", self, "OnSendValentineYWQSRankInfo")
    Gac2Gas.RequestValentineYWQSRankInfo()
end

function LuaValentine2023YWQSRankWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendValentineYWQSRankInfo", self, "OnSendValentineYWQSRankInfo")
end

function LuaValentine2023YWQSRankWnd:OnSendValentineYWQSRankInfo(partnerId, partnerName, partnerClass, partnerGender, loveValue, voteCount, myRank, rankInfo)
    rankInfo = rankInfo and g_MessagePack.unpack(rankInfo)

    if not rankInfo or #rankInfo == 0 then
        self.noPlayerInRank:SetActive(true)
        self.rank_Info:SetActive(false)
        return
    end
    self:UpdateWe(partnerId, partnerName, partnerClass, partnerGender, loveValue, voteCount, myRank)

    self.noPlayerInRank:SetActive(false)
    self.rank_Info:SetActive(true)

    Extensions.RemoveAllChildren(self.rank_Grid.transform)
    local path = "UI/Texture/FestivalActivity/Festival_Valentine/Valentine2023/Material/"
    local rankSpriteTbl = {path .. "common_jiang_1.mat", path .. "common_jiang_2.mat", path .. "common_jiang_3.mat"}
    for i = 1, #rankInfo do
        local child = NGUITools.AddChild(self.rank_Grid.gameObject, self.rank_Template)
        child:SetActive(true)
        self:InitItem(child.transform, rankInfo[i], rankSpriteTbl)
    end
    self.rank_Grid:Reposition()
    self:UpdateAward(myRank)
end

function LuaValentine2023YWQSRankWnd:UpdateWe(partnerId, partnerName, partnerClass, partnerGender, loveValue, voteCount, myRank)
    if partnerId <= 0 then
        self.we_Info:SetActive(false)
        self.we_NoPartner:SetActive(true)
        return
    end
    self.we_Info:SetActive(true)
    self.we_NoPartner:SetActive(false)
    self.we_Player1:LoadPortrait(CUICommonDef.GetPortraitName(CClientMainPlayer.Inst.Class, CClientMainPlayer.Inst.Gender, -1), true)
    self.we_Player1Name.text = CClientMainPlayer.Inst.Name
    self.we_Player2:LoadPortrait(CUICommonDef.GetPortraitName(partnerClass, partnerGender, -1), true)
    self.we_Player2Name.text = partnerName
    self.we_VoteNum.text = voteCount
    self.we_RankNum.text = myRank >= 1 and myRank or LocalString.GetString("未上榜")
    self.we_Score.text = loveValue

    UIEventListener.Get(self.we_Player1.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnPlayerClick(CClientMainPlayer.Inst.Id)
	end)

    UIEventListener.Get(self.we_Player2.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnPlayerClick(partnerId)
	end)

    UIEventListener.Get(self.we_VoteButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnVoteButtonClick(CClientMainPlayer.Inst.Id)
	end)
end

function LuaValentine2023YWQSRankWnd:InitItem(tf, data, rankSpriteTbl)
    local rankNum = tf:Find("RankNum"):GetComponent(typeof(UILabel))
    local rankSprite = tf:Find("RankSprite"):GetComponent(typeof(CUITexture))
    local player1 = tf:Find("Player1"):GetComponent(typeof(CUITexture))
    local player1Name = tf:Find("Player1/Name"):GetComponent(typeof(UILabel))
    local player2 = tf:Find("Player2"):GetComponent(typeof(CUITexture))
    local player2Name = tf:Find("Player2/Name"):GetComponent(typeof(UILabel))
    local score = tf:Find("Score"):GetComponent(typeof(UILabel))
    local voteButton = tf:Find("VoteButton").gameObject
    local voteNum = tf:Find("VoteNum"):GetComponent(typeof(UILabel))
    local bg = tf:Find("Bg"):GetComponent(typeof(UIBasicSprite))

    local rank = data[1]
    if rank >= 1 and rank <= 3 then
        rankSprite:LoadMaterial(rankSpriteTbl[rank])
        rankSprite.gameObject:SetActive(true)
        rankNum.gameObject:SetActive(false)
    else
        rankNum.text = rank
        rankSprite.gameObject:SetActive(false)
        rankNum.gameObject:SetActive(true)
    end
    score.text = data[2]
    voteNum.text = data[3]
    bg.color = NGUIText.ParseColor32(rank % 2 == 1 and "FFD7F088" or "FFFFFF88", 0)

    player1:LoadPortrait(CUICommonDef.GetPortraitName(data[6], data[7], -1), true)
    player1Name.text = data[5]
    player2:LoadPortrait(CUICommonDef.GetPortraitName(data[10], data[11], -1), true)
    player2Name.text = data[9]

    UIEventListener.Get(player1.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnPlayerClick(data[4])
	end)

    UIEventListener.Get(player2.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnPlayerClick(data[8])
	end)

    UIEventListener.Get(voteButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnVoteButtonClick(data[4])
	end)
end

function LuaValentine2023YWQSRankWnd:UpdateAward(myRank)
    for i = 1, 4 do
        local data = Valentine2023_ListAward.GetData(i)
        local child = self.award_Grid:GetChild(i - 1)

        local we = child:Find("We").gameObject
        if myRank >= data.StartNum and (data.EndNum == 0 or myRank <= data.EndNum) then
            we:SetActive(true)
        else
            we:SetActive(false)
        end
    end
end


function LuaValentine2023YWQSRankWnd:Init()
end

function LuaValentine2023YWQSRankWnd:OnDestroy()
    LuaValentine2023Mgr:CloseYWQSRankWnd()
end

--@region UIEvent

function LuaValentine2023YWQSRankWnd:OnCloseButtonClick()
    CUIManager.CloseUI(CLuaUIResources.Valentine2023YWQSRankWnd)
end

function LuaValentine2023YWQSRankWnd:OnPlayerClick(playerId)
    CPlayerInfoMgr.ShowPlayerPopupMenu(playerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
end

function LuaValentine2023YWQSRankWnd:OnVoteButtonClick(votePlayerId)
    LuaValentine2023Mgr:OpenYWQSVoteWnd(votePlayerId)
end

function LuaValentine2023YWQSRankWnd:OnTabClick(i)
    if self.curTab == i then return end

    self.tab_Rank:Find("Normal").gameObject:SetActive(i ~= 1)
    self.tab_Rank:Find("Highlight").gameObject:SetActive(i == 1)
    self.tab_Award:Find("Normal").gameObject:SetActive(i ~= 2)
    self.tab_Award:Find("Highlight").gameObject:SetActive(i == 2)

    self.curTab = i
    self.rank:SetActive(i == 1)
    self.award:SetActive(i == 2)
end

function LuaValentine2023YWQSRankWnd:OnTipButtonClick()
    g_MessageMgr:ShowMessage("VALENTINE2023_YIWANGQINGSHEN_AWARD_TIP")
end

--@endregion UIEvent
