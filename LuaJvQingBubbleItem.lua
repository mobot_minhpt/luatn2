local UITexture = import "UITexture"
local Animation = import "UnityEngine.Animation"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"

LuaJvQingBubbleItem = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaJvQingBubbleItem, "HighLight_di", "HighLight_di", UITexture)
RegistChildComponent(LuaJvQingBubbleItem, "HighLight_glow", "HighLight_glow", UITexture)
RegistChildComponent(LuaJvQingBubbleItem, "CUIFx", "CUIFx", GameObject)
RegistChildComponent(LuaJvQingBubbleItem, "BG", "BG", UITexture)
RegistChildComponent(LuaJvQingBubbleItem, "HighLight", "HighLight", GameObject)
RegistChildComponent(LuaJvQingBubbleItem, "HighLight_xiaoshi", "HighLight_xiaoshi", UITexture)

--@endregion RegistChildComponent end
RegistClassMember(LuaJvQingBubbleItem,"m_BubbleText")
RegistClassMember(LuaJvQingBubbleItem,"m_BubbleHighLightText")
RegistClassMember(LuaJvQingBubbleItem,"m_BubbleHighLightBgTexture")
RegistClassMember(LuaJvQingBubbleItem,"m_BubbleTick")
RegistClassMember(LuaJvQingBubbleItem,"m_BubbleCurStatus")
RegistClassMember(LuaJvQingBubbleItem,"m_OnReachCallBack")
RegistClassMember(LuaJvQingBubbleItem,"m_OnClickCallBack")
RegistClassMember(LuaJvQingBubbleItem,"m_StartScale")
RegistClassMember(LuaJvQingBubbleItem,"m_EndScale")
RegistClassMember(LuaJvQingBubbleItem,"m_OnDisappearCallBack")
RegistClassMember(LuaJvQingBubbleItem,"m_OnDisappearCallBack")
RegistClassMember(LuaJvQingBubbleItem,"m_MoveTime")
RegistClassMember(LuaJvQingBubbleItem,"m_WaitTime")
RegistClassMember(LuaJvQingBubbleItem,"m_TargetPos")
RegistClassMember(LuaJvQingBubbleItem,"m_ItemAnimation")
RegistClassMember(LuaJvQingBubbleItem,"m_NomalAnimation")
RegistClassMember(LuaJvQingBubbleItem,"m_HighLightAnimation")
function LuaJvQingBubbleItem:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
	self.m_BubbleText = self.BG.transform:Find("Bubble"):GetComponent(typeof(UILabel))
	self.m_BubbleHighLightText = self.HighLight.transform:Find("Bubble"):GetComponent(typeof(UILabel))
	self.m_BubbleHighLightBgTexture = self.HighLight.transform:Find("Texture"):GetComponent(typeof(UITexture))
	self.m_ItemAnimation = self.gameObject:GetComponent(typeof(Animation))
	self.m_NomalAnimation = self.BG.gameObject:GetComponent(typeof(Animation))
	self.m_HighLightAnimation = self.HighLight.gameObject:GetComponent(typeof(Animation))
	self.CUIFx.gameObject:SetActive(false)
	self.m_BubbleTick = nil
	self.m_BubbleCurStatus = 0 -- 0 未开始 1 移动中 2 可点击 3消失
	self.m_OnReachCallBack = nil
	self.m_OnClickCallBack = nil
	self.m_OnDisappearCallBack = nil
	self.m_StartScale = 0.5
	self.m_EndScale = 1
	self.m_MoveTime = 0
	self.m_WaitTime = 0
	self.m_TargetPos = nil
	UIEventListener.Get(self.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
		self:OnBGClick()
	end)
end

function LuaJvQingBubbleItem:Init(text,size,targetPos,moveTime,waitTime,OnReachCallBack,OnClickCallBack,OnDisappearCallBack)
	self.m_BubbleText.text = text
	self.m_BubbleHighLightText.text = text
	self.BG.width = size
	self.BG.height = size
	self.HighLight_di.width = size
	self.HighLight_di.height = size
	self.HighLight_glow.width = size
	self.HighLight_glow.height = size
	self.HighLight_xiaoshi.width = size
	self.HighLight_xiaoshi.height = size
	self.HighLight.gameObject:SetActive(false)
	self.BG.gameObject:SetActive(true)
	if self.m_NomalAnimation then
		self.m_NomalAnimation:Stop()
		self.m_NomalAnimation:Play("zhankuangnvbubblewnd_qipao_show")
	end

	self.m_MoveTime = moveTime
	self.m_WaitTime = waitTime
	self.m_TargetPos = targetPos
	LuaUtils.SetLocalPosition(self.transform, 0, 0, 0)

	self.m_OnReachCallBack = OnReachCallBack
	self.m_OnClickCallBack = OnClickCallBack
	self.m_OnDisappearCallBack = OnDisappearCallBack
	if self.m_BubbleTick then 
		UnRegisterTick(self.m_BubbleTick)
		self.m_BubbleTick = nil
	end
	self:ShowBubbleMove()
end

function LuaJvQingBubbleItem:ShowBubbleMove()
	self.m_BubbleCurStatus = 1
	LuaUtils.SetLocalScale(self.transform,self.m_StartScale,self.m_StartScale,self.m_StartScale)
	local MoveAnim = LuaTweenUtils.TweenPosition(self.transform,self.m_TargetPos.x,self.m_TargetPos.y,0,self.m_MoveTime)
	local MoveScale = LuaTweenUtils.TweenScale(self.transform,Vector3(self.m_StartScale,self.m_StartScale,self.m_StartScale),Vector3(self.m_EndScale,self.m_EndScale,self.m_EndScale),self.m_MoveTime)
	LuaTweenUtils.OnComplete(MoveAnim,function()
		self.m_BubbleCurStatus = 2
		self:OnBubbleReach()
		if self.m_BubbleTick then 
			UnRegisterTick(self.m_BubbleTick)
			self.m_BubbleTick = nil
		end
		self.m_BubbleTick = RegisterTickOnce(function()
			self:ShowBubbleDisappear()
		end, self.m_WaitTime * 1000)
	end)
end

--@region UIEvent

function LuaJvQingBubbleItem:OnBGClick()
	if self.m_OnClickCallBack then
		self.m_OnClickCallBack(self.gameObject,self.m_BubbleCurStatus)
	end
end

function LuaJvQingBubbleItem:OnBubbleReach()
	self.HighLight.gameObject:SetActive(true)
	self.m_BubbleHighLightBgTexture.alpha = 1
	self.m_BubbleHighLightText.alpha = 1
	self.HighLight_di.alpha = 1
	self.HighLight_glow.alpha = 1
	if self.m_ItemAnimation then
		self.m_ItemAnimation:Stop()
		self.m_ItemAnimation:Play("zhankuangnvbubblewnd_qipao_qiehuan")
	end
	if self.m_OnReachCallBack then
		self.m_OnReachCallBack(self.gameObject)
	end
end
-- 到时间截止的消失效果
function LuaJvQingBubbleItem:ShowBubbleDisappear()
	self.m_BubbleCurStatus = 3

	if self.m_BubbleTick then 
		UnRegisterTick(self.m_BubbleTick)
		self.m_BubbleTick = nil
	end
	self.BG.gameObject:SetActive(false)
	if self.m_HighLightAnimation then
		self.m_HighLightAnimation:Stop()
		self.m_HighLightAnimation:Play("zhankuangnvbubblewnd_qipao_posui")
	end

	self.m_BubbleTick = RegisterTickOnce(function()
		self.CUIFx.gameObject:SetActive(false)
		if self.m_OnDisappearCallBack then
			self.m_OnDisappearCallBack(self.gameObject)
		end
	end, 1000)
end
-- 点击消失效果
function LuaJvQingBubbleItem:ShowBubbleClickDisappear()
	SoundManager.Inst:PlayOneShot("event:/L10/L10_UI/UI_ZhaiXing_Waterbubble", Vector3(0,0,0),nil,0)
	self:ShowBubbleDisappear()
end

--@endregion UIEvent

function LuaJvQingBubbleItem:OnDisable()
	if self.m_BubbleTick then 
		UnRegisterTick(self.m_BubbleTick)
		self.m_BubbleTick = nil
	end
	self.m_BubbleCurStatus = 0
	self.m_OnReachCallBack = nil
	self.m_OnClickCallBack = nil
	self.m_OnDisappearCallBack = nil
end
