local NGUIMath = import "NGUIMath"
local Extensions = import "Extensions"
local CIndirectUIResources = import "L10.UI.CIndirectUIResources"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CBuffListMgr = import "L10.UI.CBuffListMgr"
local CPropertyBuff = import "L10.Game.CPropertyBuff"
local UITabBar = import "L10.UI.UITabBar"
local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"

CLuaBuffListWnd = class()
RegistClassMember(CLuaBuffListWnd,"m_Wnd")
RegistClassMember(CLuaBuffListWnd,"scrollView")
RegistClassMember(CLuaBuffListWnd,"table")
RegistClassMember(CLuaBuffListWnd,"buffTemplate")
RegistClassMember(CLuaBuffListWnd,"mBackground")
RegistClassMember(CLuaBuffListWnd,"contentBg")
RegistClassMember(CLuaBuffListWnd,"maxHeight")
RegistClassMember(CLuaBuffListWnd,"minHeight")

RegistClassMember(CLuaBuffListWnd,"mListContainer")
RegistClassMember(CLuaBuffListWnd,"mLeftMenu")
RegistClassMember(CLuaBuffListWnd,"mTabBar")

RegistClassMember(CLuaBuffListWnd,"mAllBuffs")       --可显示的buff = buff + debuff
RegistClassMember(CLuaBuffListWnd,"mLongTermBuffs")  --长效buff 对应buff表的IsLongTerm字段
RegistClassMember(CLuaBuffListWnd,"mNeedSplitBuffs") --特殊标记类buff，对应buff表的SprtLongShort字段
RegistClassMember(CLuaBuffListWnd,"m_IsMainPlayerBuff")

function CLuaBuffListWnd:Awake()
    self.m_Wnd=self.gameObject:GetComponent(typeof(CCommonLuaWnd))
    self.scrollView = self.transform:Find("ScrollView"):GetComponent(typeof(UIScrollView))
    self.table = self.transform:Find("ScrollView/Table"):GetComponent(typeof(UITable))
    self.buffTemplate = self.transform:Find("ScrollView/BuffItem").gameObject
    self.buffTemplate:SetActive(false)

    self.mBackground = self.transform:Find("Background"):GetComponent(typeof(UISprite))
    self.contentBg = self.transform:Find("ContentBg"):GetComponent(typeof(UIWidget))

    self.mListContainer = self.transform:Find("Background/ListContainer"):GetComponent(typeof(UIWidget))
    self.mLeftMenu = self.transform:Find("LeftMenu").gameObject
    self.mTabBar = self.transform:Find("LeftMenu"):GetComponent(typeof(UITabBar))

    self.maxHeight = 740
    self.minHeight = 380 --仅限分也时使用，避免按钮显示太小
    self.mAllBuffs = {}
    self.mLongTermBuffs = {}
    self.mNeedSplitBuffs = {}

end

function CLuaBuffListWnd:Init( )
    Extensions.RemoveAllChildren(self.table.transform)
    local obj = CClientObjectMgr.Inst:GetObject(CBuffListMgr.ObjId)
    if obj == nil then
        CUIManager.CloseUI(CIndirectUIResources.BuffListWnd)
        return
    end
    local isMainPlayerBuff = (obj == CClientMainPlayer.Inst)
    self.m_IsMainPlayerBuff = isMainPlayerBuff
    local buffs = {}
    local buffs_permanent = {}
    local debuffs = {}
    local debuffs_permanent = {}
    self.mAllBuffs = {}
    self.mLongTermBuffs = {}
    self.mNeedSplitBuffs = {}

    CommonDefs.DictIterate(obj.BuffProp.Buffs, DelegateFactory.Action_object_object(function (___key, ___value)
        local b = Buff_Buff.GetData(___key)
        if b ~= nil and b.NeedDisplay > 0 and not CPropertyBuff.HideBuffIconInClient(b.ID) and (b.OnlySyncToSelf < 1 or isMainPlayerBuff) then
            if b.Kind >= 0 then
                table.insert(___value.EndTime==-1 and buffs_permanent or buffs, ___value )
            else
                table.insert(___value.EndTime==-1 and debuffs_permanent or debuffs,___value )
            end

            if b.IsLongTerm>0 then
                self.mLongTermBuffs[b.ID] = true
            end
            if b.SprtLongShort>0 then
                self.mNeedSplitBuffs[b.ID] = true
            end
        end
    end))

    
    if not CQuanMinPKMgr.Inst.m_IsQuanMinPKServer then
        table.sort(buffs,function(b1, b2)
            return self:SortBuff(b1,b2)
        end )
    else
        table.sort(buffs,function(b1, b2)
            return self:QMPKSortBuff(b1,b2)
        end)
    end

    table.sort(buffs_permanent,function(b1, b2)
        return self:SortBuff(b1,b2)
    end )

    table.sort(debuffs,function(b1, b2)
        return self:SortBuff(b1,b2)
    end )

    table.sort(debuffs_permanent,function(b1, b2)
        return self:SortBuff(b1,b2)
    end )


    for i,v in ipairs(debuffs) do
        table.insert(buffs,v )
    end

    for i,v in ipairs(buffs_permanent) do
        table.insert(buffs,v )
    end

    for i,v in ipairs(debuffs_permanent) do
        table.insert(buffs,v )
    end

    for i,v in pairs(buffs) do
        table.insert(self.mAllBuffs, v.Id)
    end

    self:Show()
end

function CLuaBuffListWnd:SortBuff(b1, b2) --盛放在svn revision 396024 转lua处理时debuff排序处理的不对，进行了统一
    return self:CommonBuffSort(b1,b2)
end

function CLuaBuffListWnd:QMPKSortBuff(b1, b2)
    if LuaZongMenMgr:IsAntiProfessionBuff(b1.Id) then
        return false
    elseif LuaZongMenMgr:IsAntiProfessionBuff(b2.Id) then
        return true
    end
    return self:CommonBuffSort(b1,b2)
end
--类似PropertyBuff.cs中的CommonBuffSort，由于这里打开频率不高，因此暂不考虑缓存
function CLuaBuffListWnd:CommonBuffSort(b1, b2)
    local b1FitMainPlayerClass = 0
    local b2FitMainPlayerClass = 0
    local b1ClassPriority = 0
    local b2ClassPriority = 0
    local b1Priority = 0
    local b2Priority = 0
    local b1ClsId = math.floor(b1.Id/100)
    local b2ClsId = math.floor(b2.Id/100)
    if Buff_ImportantBuff.Exists(b1ClsId) then
        local data =  Buff_ImportantBuff.GetData(b1ClsId)
        b1FitMainPlayerClass = data.Class and CClientMainPlayer.Inst and CommonDefs.HashSetContains(data.Class, typeof(UInt32), EnumToInt(CClientMainPlayer.Inst.Class)) and 1 or 0
        b1ClassPriority = data.ClassPriorityLevel
        b1Priority = data.PriorityLevel       
    end
    if Buff_ImportantBuff.Exists(b2ClsId) then
        local data =  Buff_ImportantBuff.GetData(b2ClsId)
        b2FitMainPlayerClass = data.Class and CClientMainPlayer.Inst and CommonDefs.HashSetContains(data.Class, typeof(UInt32), EnumToInt(CClientMainPlayer.Inst.Class)) and 1 or 0
        b2ClassPriority = data.ClassPriorityLevel
        b2Priority = data.PriorityLevel
    end
    if not self.m_IsMainPlayerBuff then
        if b1FitMainPlayerClass~=b2FitMainPlayerClass then
            return b1FitMainPlayerClass>b2FitMainPlayerClass
        elseif b1FitMainPlayerClass>0 and b1ClassPriority~=b2ClassPriority then
            return b1ClassPriority>b2ClassPriority
        end
    end
    if b1Priority~=b2Priority then
        return b1Priority>b2Priority
    end
    --old rule
    if b1.AddTime>b2.AddTime then
        return false
    elseif b1.AddTime<b2.AddTime then
        return true
    else
        return b1.Id<b2.Id
    end
end

function CLuaBuffListWnd:Show()
    local longTermBuffCount = self:GetTableChildCount(self.mLongTermBuffs)
    local totalBuffCount = self:GetTableChildCount(self.mAllBuffs)
    if longTermBuffCount>0 and totalBuffCount>longTermBuffCount and (totalBuffCount>5 or self:GetTableChildCount(self.mNeedSplitBuffs)>0) then --同时存在长效和短效buff
        --分开显示
        self.mLeftMenu:SetActive(true)
        self.mBackground.width = 1000
        self.mTabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
            self:OnTabChange(go, index)
        end)
        self.mTabBar:ChangeTab(0, false)
    else
        --合并显示
        self.mLeftMenu:SetActive(false)
        self.mBackground.width = 920
        self:ShowBuffs(self.mAllBuffs)
    end
end

function CLuaBuffListWnd:GetTableChildCount(t)
    if t == nil then return 0 end
    local count = 0
    for k,v in pairs(t) do
        count = count + 1
    end
    return count
end

function CLuaBuffListWnd:ShowBuffs(tbl)
    Extensions.RemoveAllChildren(self.table.transform)
    for i,ID in pairs(tbl) do
        local buffItem = NGUITools.AddChild(self.table.gameObject, self.buffTemplate)
        buffItem:SetActive(true)
        local script = buffItem:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
        script:Init(ID, CBuffListMgr.ObjId)
        script.OnDescLabelChanged = function() self:UpdateBackground() end
    end
    self:UpdateBackground()
end

function CLuaBuffListWnd:OnTabChange(go, index)
    local tbl = {}
    for i,ID in pairs(self.mAllBuffs) do
        if index == 0 then
            if not self.mLongTermBuffs[ID] then
                table.insert(tbl, ID)
            end
        else
            if self.mLongTermBuffs[ID] then
                table.insert(tbl, ID)
            end
        end
    end
    self:ShowBuffs(tbl)
end

function CLuaBuffListWnd:UpdateBackground( )
    self.table:Reposition()
    self.scrollView:ResetPosition()
    --计算背景
    local contentHeight = NGUIMath.CalculateRelativeWidgetBounds(self.table.transform).size.y + math.abs(self.scrollView.panel.topAnchor.absolute) + math.abs(self.scrollView.panel.bottomAnchor.absolute)
    local height = self.mLeftMenu.activeSelf and math.max(contentHeight, self.minHeight) or contentHeight
    self.mBackground.height = math.floor(math.min(height, self.maxHeight))
    self.mListContainer:ResetAndUpdateAnchors()
    self.scrollView.panel:ResetAndUpdateAnchors()
    CommonDefs.GetComponent_Component_Type(self.table, typeof(UIWidget)):ResetAndUpdateAnchors()
    self.scrollView:ResetPosition()
    self.contentBg:ResetAndUpdateAnchors()
end

function CLuaBuffListWnd:Update()
    self.m_Wnd:ClickThroughToClose()
end
