require("common/common_include")


local QnTableView = import "L10.UI.QnTableView"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local CFightingSpiritWatchTeamItem = import "L10.UI.CFightingSpiritWatchTeamItem"
local Vector3 = import "UnityEngine.Vector3"
local CUICommonDef = import "L10.UI.CUICommonDef"
local Transform = import "UnityEngine.Transform"
local CGameVideoMgr = import "L10.Game.CGameVideoMgr"
local UILabel = import "UILabel"
--local Initialization_Init = import "L10.Game.Initialization_Init"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"

CLuaSkillCastHistoryWnd = class()
RegistChildComponent(CLuaSkillCastHistoryWnd, "m_Table", QnTableView)
RegistChildComponent(CLuaSkillCastHistoryWnd, "m_Anchor", Transform)
RegistChildComponent(CLuaSkillCastHistoryWnd, "m_PlayerName", UILabel)
RegistClassMember(CLuaSkillCastHistoryWnd, "m_CastList")
RegistClassMember(CLuaSkillCastHistoryWnd, "m_CastNumber")
RegistClassMember(CLuaSkillCastHistoryWnd, "m_JueJiSkillCls")



function CLuaSkillCastHistoryWnd:Init()
	self.m_JueJiSkillCls = {}
	Initialization_Init.ForeachKey(function (key)
		local info = Initialization_Init.GetData(key)
		local skillCls1 = math.floor(info.JuejiId[0]/100)
		local skillCls2 = math.floor(info.JuejiId[1]/100)
		self.m_JueJiSkillCls[skillCls1] = true
		self.m_JueJiSkillCls[skillCls2] = true
	end)


	local playerId = CFightingSpiritWatchTeamItem.CurrentShowSkillCastPlayerId
	local anchorBounds = CFightingSpiritWatchTeamItem.CurrentBounds
	self.m_PlayerName.text = CFightingSpiritWatchTeamItem.CurrentShowSkillCastPlayerName

	local center = CUICommonDef.WorldSpace2NGUISpace(anchorBounds.center)
	local min = CUICommonDef.WorldSpace2NGUISpace(anchorBounds.min)
	local max = CUICommonDef.WorldSpace2NGUISpace(anchorBounds.max)

	local oldPos = self.transform.localPosition
	local b = NGUIMath.CalculateAbsoluteWidgetBounds(self.m_Anchor)
	local bmax = CUICommonDef.WorldSpace2NGUISpace(b.max)
	local bmin = CUICommonDef.WorldSpace2NGUISpace(b.min)
	local width = bmax.x - bmin.x

	if CFightingSpiritWatchTeamItem.CurrentIsLeft then
		self.m_Anchor.localPosition = Vector3(max.x+math.floor(width/2), oldPos.y, oldPos.z)
	else
		self.m_Anchor.localPosition = Vector3(min.x-math.floor(width/2), oldPos.y, oldPos.z)
	end

	local list = CGameVideoMgr.Inst:GetCastSkillFrameByPlayerId(playerId)
	local lastSceneLeftTime = CGameVideoMgr.Inst:GetLastSetSceneLeftTimeTimestamp()
	self.m_CastList = {Skills={}, Time={}}
	local function RowAt(row, idx)
		if idx>=self.m_CastNumber then return end
		local skillId, time = self.m_CastList.Skills[self.m_CastNumber - idx], self.m_CastList.Time[self.m_CastNumber - idx]
		if not (skillId and time) then return end
		local castInfoItem = row:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
		castInfoItem:SetData(time, skillId, self.m_JueJiSkillCls[math.floor(skillId/100)])
	end

	local startFrameTime = CGameVideoMgr.Inst.LiveStartFrameTime
	if list and list.Count>0 then
		local lastSkilCastTime = {}
		for i = 0, list.Count-1 do
			local info = list[i]
			local skillData = Skill_AllSkills.GetData(info.SkillId)
			local skillCls = math.floor(info.SkillId/100)
			local isHelpSkill = math.floor(skillCls /100)%10 == 8	--辅助技能
			local minCD = skillData.CD/2.5
			if info.Timestamp >= lastSceneLeftTime and not isHelpSkill then
				if skillData.Leading ==1 and self.m_JueJiSkillCls[skillCls] then
					if (not lastSkilCastTime[skillCls]) or  (info.Timestamp/1000 - lastSkilCastTime[skillCls]/1000) > minCD then
						table.insert(self.m_CastList.Skills, info.SkillId)
						table.insert(self.m_CastList.Time, math.floor(info.Timestamp/1000 - math.max(lastSceneLeftTime, startFrameTime)/1000))
						lastSkilCastTime[skillCls] = info.Timestamp
					end
				else
					table.insert(self.m_CastList.Skills, info.SkillId)
					table.insert(self.m_CastList.Time, math.floor(info.Timestamp/1000 - math.max(lastSceneLeftTime, startFrameTime)/1000))
				end
			end
		end
		self.m_CastNumber = #self.m_CastList.Skills
	else
		self.m_CastNumber = 0
	end
	self.m_Table.m_DataSource = DefaultTableViewDataSource.CreateByCount(self.m_CastNumber, RowAt)
	self.m_Table:ReloadData(true, false)
end


