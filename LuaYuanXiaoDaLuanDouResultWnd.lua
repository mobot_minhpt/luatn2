local GameObject = import "UnityEngine.GameObject"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local Animation = import "UnityEngine.Animation"

LuaYuanXiaoDaLuanDouResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaYuanXiaoDaLuanDouResultWnd, "Left", "Left", GameObject)
RegistChildComponent(LuaYuanXiaoDaLuanDouResultWnd, "Right", "Right", GameObject)
RegistChildComponent(LuaYuanXiaoDaLuanDouResultWnd, "ShareBtn", "ShareBtn", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaYuanXiaoDaLuanDouResultWnd,"m_Animation")

function LuaYuanXiaoDaLuanDouResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.ShareBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareBtnClick()
	end)


    --@endregion EventBind end
	self.m_Animation = self.transform:GetComponent(typeof(Animation))
end

function LuaYuanXiaoDaLuanDouResultWnd:Init()
	if LuaYuanXiao2024Mgr.m_IsDaLuanDouWinner then
		self.m_Animation:Play("yuanxiaodaluandouresultwnd_show_win")
	else
		self.m_Animation:Play("yuanxiaodaluandouresultwnd_show_fail")
	end
end

--@region UIEvent

function LuaYuanXiaoDaLuanDouResultWnd:OnShareBtnClick()
end


--@endregion UIEvent

