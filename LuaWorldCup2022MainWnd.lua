local DelegateFactory = import "DelegateFactory"
local CWelfareMgr     = import "L10.UI.CWelfareMgr"

LuaWorldCup2022MainWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaWorldCup2022MainWnd, "JZLYRedDot")
RegistClassMember(LuaWorldCup2022MainWnd, "GJZLRedDot")
RegistClassMember(LuaWorldCup2022MainWnd, "TTJCRedDot")
RegistClassMember(LuaWorldCup2022MainWnd, "QDYLRedDot")

function LuaWorldCup2022MainWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    local anchor = self.transform:Find("Anchor")

    UIEventListener.Get(anchor:Find("CuJuJiaLi").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCuJuJiaLiClick()
	end)

    local setting = WorldCup2022_Setting.GetData()
    local jueZhanLvYin = anchor:Find("JueZhanLvYin")
    self.JZLYRedDot = jueZhanLvYin:Find("RedDot").gameObject
    self.JZLYRedDot:SetActive(false)
    jueZhanLvYin:Find("OpenTime"):GetComponent(typeof(UILabel)).text = setting.JueZhanLvYinActivityTime
    UIEventListener.Get(jueZhanLvYin.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnJueZhanLvYinClick()
	end)

    local guanJunZhiLu = anchor:Find("GuanJunZhiLu")
    self.GJZLRedDot = guanJunZhiLu:Find("RedDot").gameObject
    self.GJZLRedDot:SetActive(false)
    guanJunZhiLu:Find("OpenTime"):GetComponent(typeof(UILabel)).text = setting.GuanJunZhiLuActivityTime
    UIEventListener.Get(guanJunZhiLu.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnGuanJunZhiLuClick()
	end)

    local tianTianJingCai = anchor:Find("TianTianJingCai")
    self.TTJCRedDot = tianTianJingCai:Find("RedDot").gameObject
    self.TTJCRedDot:SetActive(false)
    UIEventListener.Get(tianTianJingCai.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTianTianJingCaiClick()
	end)

    UIEventListener.Get(tianTianJingCai:Find("TipButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTianTianJingCaiTipButtonClick()
	end)

    local qianDaoYouLi = anchor:Find("QianDaoYouLi")
    self.QDYLRedDot = qianDaoYouLi:Find("RedDot").gameObject
    self.QDYLRedDot:SetActive(false)
    UIEventListener.Get(qianDaoYouLi.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnQianDaoYouLiClick()
	end)

    UIEventListener.Get(anchor:Find("JiFenShangDian").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnJiFenShangDianClick()
	end)

    anchor:Find("ActivityTime"):GetComponent(typeof(UILabel)).text = setting.ActivityTime
end

function LuaWorldCup2022MainWnd:OnEnable()
    g_ScriptEvent:AddListener("UpdateActivityRedDot", self, "OnUpdateActivityRedDot")
end

function LuaWorldCup2022MainWnd:OnDisable()
    g_ScriptEvent:RemoveListener("UpdateActivityRedDot", self, "OnUpdateActivityRedDot")
end

function LuaWorldCup2022MainWnd:OnUpdateActivityRedDot(hasChanged)
    if hasChanged then
        self:UpdateRedDot()
    end
end

function LuaWorldCup2022MainWnd:Init()
    self:UpdateRedDot()
end

function LuaWorldCup2022MainWnd:UpdateRedDot()
    local setting = WorldCup2022_Setting.GetData()
    self.JZLYRedDot:SetActive(LuaActivityRedDotMgr:IsRedDot(setting.JueZhanLvYinRedDotId))
    self.GJZLRedDot:SetActive(LuaActivityRedDotMgr:IsRedDot(setting.GuanJunZhiLuRedDotId))
    self.TTJCRedDot:SetActive(LuaActivityRedDotMgr:IsRedDot(setting.TianTianJingCaiRedDotId))
    self.QDYLRedDot:SetActive(LuaActivityRedDotMgr:IsRedDot(setting.QianDaoYouLiRedDotId))
end


--@region UIEvent

function LuaWorldCup2022MainWnd:OnCuJuJiaLiClick()
    local scheduleIds = WorldCup2022_Setting.GetData().CuJuJiaLiScheduleId
    local scheduleId = nil

    for i = 0, 1 do
        if LuaWorldCup2022Mgr:IsTaskOpen(Task_Schedule.GetData(scheduleIds[i]).TaskID[0]) then
            scheduleId = scheduleIds[i]
            break
        end
    end
    if not scheduleId then return end

    local scheduleData = Task_Schedule.GetData(scheduleId)
    local taskId = scheduleData.TaskID[0]

    CLuaScheduleMgr.selectedScheduleInfo = {
        activityId = scheduleId,
        taskId = taskId,
        TotalTimes = 0,
        DailyTimes = 0,
        ActivityTime = scheduleData.ExternTip,
        ShowJoinBtn = false
    }
    CLuaScheduleInfoWnd.s_UseCustomInfo = true
    CUIManager.ShowUI(CLuaUIResources.ScheduleInfoWnd)
end

function LuaWorldCup2022MainWnd:OnJueZhanLvYinClick()
    LuaActivityRedDotMgr:OnRedDotClicked(WorldCup2022_Setting.GetData().JueZhanLvYinRedDotId)
    CUIManager.ShowUI(CLuaUIResources.CuJuEnterWnd)
end

function LuaWorldCup2022MainWnd:OnGuanJunZhiLuClick()
    LuaActivityRedDotMgr:OnRedDotClicked(WorldCup2022_Setting.GetData().GuanJunZhiLuRedDotId)
    CUIManager.ShowUI(CLuaUIResources.WorldCup2022GJZLEnterWnd)
end

function LuaWorldCup2022MainWnd:OnTianTianJingCaiClick()
    if not LuaWorldCup2022Mgr:IsTianTianJingCaiOpen() then
        g_MessageMgr:ShowMessage("WORLDCUP2022_NOT_OPEN_IN_TRIAL_DELIVERY")
        return
    end

    LuaActivityRedDotMgr:OnRedDotClicked(WorldCup2022_Setting.GetData().TianTianJingCaiRedDotId)
    CUIManager.ShowUI(CLuaUIResources.WorldCup2022LotteryWnd)
end

function LuaWorldCup2022MainWnd:OnTianTianJingCaiTipButtonClick()
    if not LuaWorldCup2022Mgr:IsTianTianJingCaiOpen() then
        g_MessageMgr:ShowMessage("WORLDCUP2022_NOT_OPEN_IN_TRIAL_DELIVERY")
        return
    end

    g_MessageMgr:ShowMessage("WORLDCUP2022_LOTTERY_TIP")
end

function LuaWorldCup2022MainWnd:OnQianDaoYouLiClick()
    if not LuaWorldCup2022Mgr:IsQianDaoYouLiOpen() then
        g_MessageMgr:ShowMessage("WORLDCUP2022_NOT_OPEN_IN_TRIAL_DELIVERY")
        return
    end

    LuaActivityRedDotMgr:OnRedDotClicked(WorldCup2022_Setting.GetData().QianDaoYouLiRedDotId)
    CWelfareMgr.OpenWelfareWnd(LocalString.GetString("蹴鞠大赛签到"))
end

function LuaWorldCup2022MainWnd:OnJiFenShangDianClick()
    if not LuaWorldCup2022Mgr:IsTaskOpen(WorldCup2022_Setting.GetData().JiFenShangDianTaskId) then
        g_MessageMgr:ShowMessage("WORLDCUP2022_NOT_OPEN_IN_TRIAL_DELIVERY")
        return
    end

	CLuaNPCShopInfoMgr.ShowScoreShop(WorldCup2022_Setting.GetData().ScoreShopKey)
end

--@endregion UIEvent
