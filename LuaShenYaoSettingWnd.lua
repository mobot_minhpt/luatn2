local QnButton = import "L10.UI.QnButton"
local UIGrid = import "UIGrid"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"

LuaShenYaoSettingWnd = class()
LuaShenYaoSettingWnd.s_CurLv = 0

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaShenYaoSettingWnd, "OkButton", "OkButton", QnButton)
RegistChildComponent(LuaShenYaoSettingWnd, "CancelButton", "CancelButton", QnButton)
RegistChildComponent(LuaShenYaoSettingWnd, "Panel", "Panel", GameObject)
RegistChildComponent(LuaShenYaoSettingWnd, "Grid", "Grid", UIGrid)
RegistChildComponent(LuaShenYaoSettingWnd, "Template", "Template", GameObject)
RegistChildComponent(LuaShenYaoSettingWnd, "SelectedBg", "SelectedBg", GameObject)

--@endregion RegistChildComponent end

RegistClassMember(LuaShenYaoSettingWnd, "m_MaxLevel")
RegistClassMember(LuaShenYaoSettingWnd, "m_ScrollView")
RegistClassMember(LuaShenYaoSettingWnd, "m_CurLevel")
RegistClassMember(LuaShenYaoSettingWnd, "m_HintLabel")


function LuaShenYaoSettingWnd:Awake()
    self.m_MaxLevel = 0
    XinBaiLianDong_ShenYaoNpc.Foreach(function(k, v)
        self.m_MaxLevel = math.max(self.m_MaxLevel, k)
    end)
    self.Template:SetActive(false)

    self.m_HintLabel = self.transform:Find("Anchor/Label2"):GetComponent(typeof(UILabel))

    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.OkButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOkButtonClick()
	end)


	
	UIEventListener.Get(self.CancelButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCancelButtonClick()
	end)


    --@endregion EventBind end

    self.m_ScrollView = self.Panel:GetComponent(typeof(UIScrollView))
    self.m_ScrollView.onDragFinished = DelegateFactory.OnDragNotification(function(go)
        self:Centering()
    end)
    self.m_ScrollView.onMomentumMove = DelegateFactory.OnDragNotification(function(go)
        self:Centering()
    end)
end

function LuaShenYaoSettingWnd:Init()
    Extensions.RemoveAllChildren(self.Grid.transform)

    for i = 0, self.m_MaxLevel do
        local obj = NGUITools.AddChild(self.Grid.gameObject, self.Template)
        obj:SetActive(true)
        if CommonDefs.IS_VN_CLIENT then
            obj:GetComponent(typeof(UILabel)).text = i == 0 and LocalString.GetString("不设置") or LocalString.GetString("阶") .. i
        else
            obj:GetComponent(typeof(UILabel)).text = i == 0 and LocalString.GetString("不设置") or i..LocalString.GetString("阶")
        end
    end
    self.Grid:Reposition()

    local trans = self.Grid.transform:GetChild(LuaShenYaoSettingWnd.s_CurLv) 
    local pos = self.Panel.transform:InverseTransformPoint(trans.position)
    local dis = self.Panel.transform:InverseTransformPoint(self.SelectedBg.transform.position).y - pos.y
    self.m_ScrollView:MoveRelative(Vector3(0, dis, 0))
    self:Centering()
end

--@region UIEvent

function LuaShenYaoSettingWnd:OnOkButtonClick()
    Gac2Gas.ShenYao_SetRejectLevel(self.m_CurLevel)
    CUIManager.CloseUI(CLuaUIResources.ShenYaoSettingWnd)
end

function LuaShenYaoSettingWnd:OnCancelButtonClick()
    CUIManager.CloseUI(CLuaUIResources.ShenYaoSettingWnd)
end


--@endregion UIEvent

function LuaShenYaoSettingWnd:Centering()
    local minDis = 114514
    local signedDis = 0
    local selectedPos = self.Panel.transform:InverseTransformPoint(self.SelectedBg.transform.position) 
    for i = 0, self.Grid.transform.childCount - 1 do
        local trans = self.Grid.transform:GetChild(i)
        local pos = self.Panel.transform:InverseTransformPoint(trans.position)
        local dis = math.abs(selectedPos.y - pos.y)
        if dis < minDis then
            minDis = dis
            signedDis = selectedPos.y - pos.y
            self.m_CurLevel = i
        end
    end
    if minDis < 114514 then
        self.m_ScrollView:MoveRelative(Vector3(0, signedDis, 0))
        if self.m_CurLevel == 0 then
            self.m_HintLabel.text = LocalString.GetString("等阶限制")
        else
            self.m_HintLabel.text = LocalString.GetString("及以下蜃妖")
        end
    end
end
