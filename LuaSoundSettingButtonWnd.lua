local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"

LuaSoundSettingButtonWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaSoundSettingButtonWnd, "Button", "Button", GameObject)

--@endregion RegistChildComponent end

function LuaSoundSettingButtonWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    UIEventListener.Get(self.Button).onClick = DelegateFactory.VoidDelegate(function(go)
        if not CUIManager.IsLoaded(CLuaUIResources.MiniVolumeSettingsWnd) then
            LuaMiniVolumeSettingsMgr:OpenWnd(go, go.transform.position, UIWidget.Pivot.Left)
        else
            CUIManager.CloseUI(CLuaUIResources.MiniVolumeSettingsWnd)
        end
    end)
end

function LuaSoundSettingButtonWnd:OnDestroy()
    CUIManager.CloseUI(CLuaUIResources.MiniVolumeSettingsWnd)
end

--@region UIEvent

--@endregion UIEvent

