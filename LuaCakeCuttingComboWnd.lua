local UILabel = import "UILabel"

LuaCakeCuttingComboWnd = class()
RegistChildComponent(LuaCakeCuttingComboWnd,"Label", UILabel)
RegistClassMember(LuaCakeCuttingComboWnd, "time")

function LuaCakeCuttingComboWnd:Close()
	CUIManager.CloseUI(CLuaUIResources.CakeCuttingComboWnd)
end

function LuaCakeCuttingComboWnd:Update()
  local nTime = os.time()
  if nTime - self.time > 2 then
    self:Close()
  end
end

function LuaCakeCuttingComboWnd:Init()
  self.time = os.time()
  self.Label.text = LuaCakeCuttingMgr.SelfComboNum
end

return LuaCakeCuttingComboWnd
