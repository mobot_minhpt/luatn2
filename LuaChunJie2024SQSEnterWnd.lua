local CMessageTipMgr         = import "L10.UI.CMessageTipMgr"
local CTipParagraphItem      = import "L10.UI.CTipParagraphItem"
local CQnReturnAwardTemplate = import "L10.UI.CQnReturnAwardTemplate"
local Item_Item              = import "L10.Game.Item_Item"

LuaChunJie2024SQSEnterWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaChunJie2024SQSEnterWnd, "ruleTemplate", "RuleTemplate", GameObject)
RegistChildComponent(LuaChunJie2024SQSEnterWnd, "ruleTable", "RuleTable", UITable)
RegistChildComponent(LuaChunJie2024SQSEnterWnd, "rewardTimes", "RewardTimes", UILabel)
RegistChildComponent(LuaChunJie2024SQSEnterWnd, "rewardTemplate", "RewardTemplate", GameObject)
RegistChildComponent(LuaChunJie2024SQSEnterWnd, "rewardGrid", "RewardGrid", UIGrid)
RegistChildComponent(LuaChunJie2024SQSEnterWnd, "startButton", "StartButton", GameObject)
RegistChildComponent(LuaChunJie2024SQSEnterWnd, "tipButton", "TipButton", GameObject)
RegistChildComponent(LuaChunJie2024SQSEnterWnd, "firstPassReward", "FirstPassReward", CQnReturnAwardTemplate)
--@endregion RegistChildComponent end

RegistClassMember(LuaChunJie2024SQSEnterWnd, "tabs")
RegistClassMember(LuaChunJie2024SQSEnterWnd, "levelTemplate")
RegistClassMember(LuaChunJie2024SQSEnterWnd, "teamTemplate")

RegistClassMember(LuaChunJie2024SQSEnterWnd, "curTabId")

function LuaChunJie2024SQSEnterWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	UIEventListener.Get(self.startButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnStartButtonClick()
	end)

	UIEventListener.Get(self.tipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)
    --@endregion EventBind end

    local tabsRoot = self.transform:Find("Anchor/Tabs")
    self.tabs = {tabsRoot:Find("Normal"), tabsRoot:Find("Hero")}
    for i = 1, 2 do
        self.tabs[i]:Find("Texture").gameObject:SetActive(true)
        self.tabs[i]:Find("Highlight").gameObject:SetActive(false)

        UIEventListener.Get(self.tabs[i]:Find("Texture").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnTabClick(i)
        end)
    end

    self.ruleTemplate:SetActive(false)
    self.levelTemplate = self.transform:Find("Anchor/Rule/LevelTemplate").gameObject
	self.levelTemplate:SetActive(false)
    self.teamTemplate = self.transform:Find("Anchor/Rule/TeamTemplate").gameObject
	self.teamTemplate:SetActive(false)

    self.rewardTemplate:SetActive(false)
end

function LuaChunJie2024SQSEnterWnd:OnEnable()
    g_ScriptEvent:AddListener("QuerySongQiongShen2024InfoResult", self, "OnQuerySongQiongShen2024InfoResult")
end

function LuaChunJie2024SQSEnterWnd:OnDisable()
    g_ScriptEvent:RemoveListener("QuerySongQiongShen2024InfoResult", self, "OnQuerySongQiongShen2024InfoResult")
end

function LuaChunJie2024SQSEnterWnd:OnQuerySongQiongShen2024InfoResult(rewardTimes, maxRewardTimes, firstPassTimes)
    self.rewardTimes.gameObject:SetActive(true)
	self.rewardTimes.text = SafeStringFormat3(LocalString.GetString("今日奖励次数 %d/%d"), rewardTimes, maxRewardTimes)
    self.firstPassReward.transform:Find("Mask").gameObject:SetActive(firstPassTimes > 0)
end


function LuaChunJie2024SQSEnterWnd:Init()
    self:InitReward()

    self.curTabId = 0
    self:OnTabClick(1)
    self.rewardTimes.gameObject:SetActive(false)

    Gac2Gas.QuerySongQiongShen2024Info()
end

function LuaChunJie2024SQSEnterWnd:UpdateRuleTable()
	Extensions.RemoveAllChildren(self.ruleTable.transform)

	local levelChild = NGUITools.AddChild(self.ruleTable.gameObject, self.levelTemplate)
	levelChild:SetActive(true)
	local minGrade = ChunJie2024_SongQiongShen.GetData().MinGrade
	levelChild.transform:Find("Label"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("所有成员满%d级"), minGrade)

	local teamChild = NGUITools.AddChild(self.ruleTable.gameObject, self.teamTemplate)
	teamChild:SetActive(true)

	local msg =	g_MessageMgr:FormatMessage("CHUNJIE2024_SQS_RULE")
    local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(msg)
    if info then
		do
			local i = 0
			while i < info.paragraphs.Count do							-- 根据信息创建并显示段落
				local paragraph = CUICommonDef.AddChild(self.ruleTable.gameObject, self.ruleTemplate) -- 添加段落Go
				paragraph:SetActive(true)
				local tip = CommonDefs.GetComponent_GameObject_Type(paragraph, typeof(CTipParagraphItem))
				tip:Init(info.paragraphs[i], 4294967295)				-- 初始化段落，color = 4294967295（0xFFFFFFFF）
				i = i + 1
			end
		end
    end

	self.ruleTable:Reposition()
end

function LuaChunJie2024SQSEnterWnd:InitReward()
	Extensions.RemoveAllChildren(self.rewardGrid.transform)

	local rewardItemIds = ChunJie2024_SongQiongShen.GetData().RewardItemIds
	local count = rewardItemIds.Length
	for i = 1, count do
		local child = NGUITools.AddChild(self.rewardGrid.gameObject, self.rewardTemplate)
		child:SetActive(true)

		local qnReturnAward = child.transform:GetComponent(typeof(CQnReturnAwardTemplate))
        qnReturnAward:Init(Item_Item.GetData(rewardItemIds[i - 1]), 0)
	end
	self.rewardGrid:Reposition()

    local itemId = ChunJie2024_SongQiongShen.GetData().FirstPassRewardItemId
    self.firstPassReward:Init(Item_Item.GetData(itemId), 0)
end

function LuaChunJie2024SQSEnterWnd:UpdateReward()
    local isNormal = self.curTabId == 1
    LuaUtils.SetLocalPositionX(self.rewardGrid.transform, isNormal and 26 or 193)
    self.firstPassReward.gameObject:SetActive(not isNormal)
end

--@region UIEvent

function LuaChunJie2024SQSEnterWnd:OnStartButtonClick()
    Gac2Gas.RequestEnterSongQiongShenPlay(self.curTabId == 2)
end

function LuaChunJie2024SQSEnterWnd:OnTipButtonClick()
    g_MessageMgr:ShowMessage("CHUNJIE2024_SQS_TIP")
end

function LuaChunJie2024SQSEnterWnd:OnTabClick(i)
    if self.curTabId == i then return end

    if self.curTabId > 0 then
        self.tabs[self.curTabId]:Find("Texture").gameObject:SetActive(true)
        self.tabs[self.curTabId]:Find("Highlight").gameObject:SetActive(false)
    end

    self.curTabId = i
    self.tabs[i]:Find("Texture").gameObject:SetActive(false)
    self.tabs[i]:Find("Highlight").gameObject:SetActive(true)

    self:UpdateRuleTable()
    self:UpdateReward()
end

--@endregion UIEvent
