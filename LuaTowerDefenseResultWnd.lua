local CTowerDefenseMgr=import "L10.Game.CTowerDefenseMgr"
local Title_Title=import "L10.Game.Title_Title"
local Item_Item=import "L10.Game.Item_Item"
local CItemInfoMgr=import "L10.UI.CItemInfoMgr"
local AlignType=import "L10.UI.CItemInfoMgr+AlignType"
local LuaGameObject=import "LuaGameObject"

EnumTowerDefenseAwardStatus = {
	eGot = 1,				-- 本次获得
	eNotGot = 2,			-- 未达成
	eAlreadyGot = 3,		-- 已经领取
}
CLuaTowerDefenseResultWnd=class()
RegistClassMember(CLuaTowerDefenseResultWnd,"m_ShowBtn")--炫耀一下
RegistClassMember(CLuaTowerDefenseResultWnd,"m_ClostBtn")--关闭
RegistClassMember(CLuaTowerDefenseResultWnd,"m_Icon1")
RegistClassMember(CLuaTowerDefenseResultWnd,"m_Icon2")
RegistClassMember(CLuaTowerDefenseResultWnd,"m_Icon3")
RegistClassMember(CLuaTowerDefenseResultWnd,"m_StatusLabel1")
RegistClassMember(CLuaTowerDefenseResultWnd,"m_StatusLabel2")
RegistClassMember(CLuaTowerDefenseResultWnd,"m_StatusLabel3")
RegistClassMember(CLuaTowerDefenseResultWnd,"m_NameLabel1")
RegistClassMember(CLuaTowerDefenseResultWnd,"m_NameLabel2")
RegistClassMember(CLuaTowerDefenseResultWnd,"m_NameLabel3")


function CLuaTowerDefenseResultWnd:Init()
    local g = LuaGameObject.GetChildNoGC(self.transform,"CloseButton").gameObject
    UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.CloseUI("TowerDefenseResultWnd") 
    end)
    local roundIndex=CTowerDefenseMgr.Inst.GameResult_RoundIndex
    local maxRoundIndex=CTowerDefenseMgr.Inst.GameResult_MaxRoundIndex;
    local statusParticipation=CTowerDefenseMgr.Inst.GameResult_StatusParticipation;
    local statusRoundIndex=CTowerDefenseMgr.Inst.GameResult_StatusRoundIndex;
    local statusScore=CTowerDefenseMgr.Inst.GameResult_StatusScore;
    local titleId=CTowerDefenseMgr.Inst.GameResult_TitleId;--如果是0表示没有获得成就
    local score=CTowerDefenseMgr.Inst.GameResult_Score;

    LuaGameObject.GetChildNoGC(self.transform,"ProgressLabel").label.text=SafeStringFormat3(LocalString.GetString("%02d/%02d波"),roundIndex,maxRoundIndex) or " "
    LuaGameObject.GetChildNoGC(self.transform,"ProgressBar").slider.value=roundIndex/maxRoundIndex
    
    LuaGameObject.GetChildNoGC(self.transform,"ScoreLabel").label.text=SafeStringFormat3(LocalString.GetString("个人积分 %d"),score) or " "

    local titleLabel=LuaGameObject.GetChildNoGC(self.transform,"GameTitleLabel").label
    titleLabel.text=" "
    if titleId>0 then
        local data=Title_Title.GetData(titleId)
        if data then
            titleLabel.text=SafeStringFormat3(LocalString.GetString("[c][ACF8FF]获得称号：[-][FFED5F]%s[-][/c]"),data.Name) or " "
        end
    end

    self.m_Icon1=LuaGameObject.GetChildNoGC(self.transform,"Icon1").gameObject
    self.m_Icon2=LuaGameObject.GetChildNoGC(self.transform,"Icon2").gameObject
    self.m_Icon3=LuaGameObject.GetChildNoGC(self.transform,"Icon3").gameObject
    self.m_StatusLabel1=LuaGameObject.GetChildNoGC(self.transform,"StatusLabel1").label        
    self.m_StatusLabel2=LuaGameObject.GetChildNoGC(self.transform,"StatusLabel2").label        
    self.m_StatusLabel3=LuaGameObject.GetChildNoGC(self.transform,"StatusLabel3").label        

    local str1=LocalString.GetString("[00ff00]已获得[-]")
    local str2=LocalString.GetString("[ff0000]未达成[-]")
    local str3=LocalString.GetString("[ff0000]已领取[-]")
    if statusParticipation==EnumTowerDefenseAwardStatus.eGot then-- 本次获得
        self.m_StatusLabel1.text=str1
    elseif statusParticipation==EnumTowerDefenseAwardStatus.eNotGot then--未获得
        self.m_StatusLabel1.text=str2
        CUICommonDef.SetActive(self.m_Icon1,false,true)
    elseif statusParticipation==EnumTowerDefenseAwardStatus.eAlreadyGot then
        self.m_StatusLabel1.text=str3
        CUICommonDef.SetActive(self.m_Icon1,false,true)
    end

    if statusRoundIndex==EnumTowerDefenseAwardStatus.eGot then-- 本次获得
        self.m_StatusLabel2.text=str1
    elseif statusRoundIndex==EnumTowerDefenseAwardStatus.eNotGot then--未获得
        self.m_StatusLabel2.text=str2
        CUICommonDef.SetActive(self.m_Icon2,false,true)
    elseif statusRoundIndex==EnumTowerDefenseAwardStatus.eAlreadyGot then
        self.m_StatusLabel2.text=str3
        CUICommonDef.SetActive(self.m_Icon2,false,true)
    end
    if statusScore==EnumTowerDefenseAwardStatus.eGot then-- 本次获得
        self.m_StatusLabel3.text=str1
    elseif statusScore==EnumTowerDefenseAwardStatus.eNotGot then--未获得
        self.m_StatusLabel3.text=str2
        CUICommonDef.SetActive(self.m_Icon3,false,true)
    elseif statusScore==EnumTowerDefenseAwardStatus.eAlreadyGot then
        self.m_StatusLabel3.text=str3
        CUICommonDef.SetActive(self.m_Icon3,false,true)
    end

    self:InitAwardDesc()
end
function CLuaTowerDefenseResultWnd:InitAwardDesc()
    local setting = TowerDefense_Setting.GetData()
    local CanYuLiBao=setting.CanYuLiBao or 21004049
    local TongGuanLiBao=setting.TongGuanLiBao or 21004050
    local JiFenLiBao=setting.JiFenLiBao or 21004051

    self.m_NameLabel1=LuaGameObject.GetChildNoGC(self.transform,"NameLabel1").label
    self.m_NameLabel2=LuaGameObject.GetChildNoGC(self.transform,"NameLabel2").label
    self.m_NameLabel3=LuaGameObject.GetChildNoGC(self.transform,"NameLabel3").label

    local data1=Item_Item.GetData(CanYuLiBao)
    if data1 then
        self.m_NameLabel1.text=data1.Name or " "
    else
        self.m_NameLabel1.text=" "
    end
    local g = LuaGameObject.GetChildNoGC(self.transform,"Award1").gameObject
    UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
        CItemInfoMgr.ShowLinkItemTemplateInfo(CanYuLiBao, false,nil, AlignType.Default,0,0,0,0);
    end)
    local data2=Item_Item.GetData(TongGuanLiBao)
    if data2 then
        self.m_NameLabel2.text=data2.Name or " "
    else
        self.m_NameLabel2.text=" "
    end
    local g = LuaGameObject.GetChildNoGC(self.transform,"Award2").gameObject
    UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
        CItemInfoMgr.ShowLinkItemTemplateInfo(TongGuanLiBao, false,nil, AlignType.Default,0,0,0,0);
    end)
    local data3=Item_Item.GetData(JiFenLiBao)
    if data3 then
        self.m_NameLabel3.text=data3.Name or " "
    else
        self.m_NameLabel3.text=" "
    end
    local g = LuaGameObject.GetChildNoGC(self.transform,"Award3").gameObject
    UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
        CItemInfoMgr.ShowLinkItemTemplateInfo(JiFenLiBao, false,nil, AlignType.Default,0,0,0,0);
    end)
end
function CLuaTowerDefenseResultWnd:OnEnable()
    -- g_ScriptEvent:AddListener("GetLingShouOtherDetails", self, "OnGetLingShouOtherDetails")
end

function CLuaTowerDefenseResultWnd:OnDisable()
    -- g_ScriptEvent:RemoveListener("GetLingShouOtherDetails", self, "OnGetLingShouOtherDetails")

end
