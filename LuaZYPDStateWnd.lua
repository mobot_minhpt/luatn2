local UILabel = import "UILabel"
local CButton = import "L10.UI.CButton"
local GameObject = import "UnityEngine.GameObject"
local DelegateFactory  = import "DelegateFactory"
local Animation = import "UnityEngine.Animation"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local CSkillButtonBoardWnd = import "L10.UI.CSkillButtonBoardWnd"

LuaZYPDStateWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaZYPDStateWnd, "ExpandButton", "ExpandButton", CButton)
RegistChildComponent(LuaZYPDStateWnd, "StageLab", "StageLab", UILabel)

--@endregion RegistChildComponent end

RegistClassMember(LuaZYPDStateWnd, "m_stage")

function LuaZYPDStateWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.ExpandButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnExpandButtonClick()
	end)


    --@endregion EventBind end
    self.m_Animation = self.transform:GetComponent(typeof(Animation))
end

function LuaZYPDStateWnd:Init()
    self:ZhongYuanPuDu_SyncProgress(LuaZhongYuanJie2023Mgr.stage, LuaZhongYuanJie2023Mgr.param1, LuaZhongYuanJie2023Mgr.param2)
    if not CameraFollow.Inst.IsNormalCamera then
        CUIManager.SetUIVisibility(CLuaUIResources.ZYPDStateWnd, false, "")
    end
end

function LuaZYPDStateWnd:OnEnable()
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:AddListener("ZhongYuanPuDu_SyncProgress", self, "ZhongYuanPuDu_SyncProgress")

end

function LuaZYPDStateWnd:OnDisable()
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:RemoveListener("ZhongYuanPuDu_SyncProgress", self, "ZhongYuanPuDu_SyncProgress")
end

function LuaZYPDStateWnd:ZhongYuanPuDu_SyncProgress(stageId, param1, param2)
    if stageId ~= self.m_stage then
        self.m_Animation:Play("zypdstatewnd_shuaxin01")
        self.m_stage = stageId
    end

    local TargetInfo = ZhongYuanJie_ZhongYuanPuDuStageInfo.GetData(stageId).TargetInfo
    if stageId == 1 or stageId == 3 or stageId == 4 or stageId == 5 or stageId == 7 then
        if param1 >= param2 then
            param1 = SafeStringFormat3("[c][00FF00]%s[-][/c]", param1)
        else
            param1 = SafeStringFormat3("[c][FF0000]%s[-][/c]", param1)
        end
        self.StageLab.text = SafeStringFormat3(TargetInfo, param1, param2)
    else
        self.StageLab.text = TargetInfo
    end
    --技能面板
    local skillWnd = CSkillButtonBoardWnd.Instance
    if skillWnd then
        skillWnd.transform:Find("Anchor").gameObject:SetActive(stageId >= 3)
    end
end

function LuaZYPDStateWnd:OnHideTopAndRightTipWnd()
    self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 180)
end

--@region UIEvent

function LuaZYPDStateWnd:OnExpandButtonClick()
    self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 0)
    CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end


--@endregion UIEvent

