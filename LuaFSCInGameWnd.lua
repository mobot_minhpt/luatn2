local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CSocialWndMgr = import "L10.UI.CSocialWndMgr"
local CChatMgr = import "L10.Game.CChatMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local AlignType = import "CPlayerInfoMgr+AlignType"
local CChatHelper = import "L10.UI.CChatHelper"
local Animation = import "UnityEngine.Animation"
local Ease = import "DG.Tweening.Ease"
local ParticleSystem = import "UnityEngine.ParticleSystem"
local MessageMgr = import "L10.Game.MessageMgr"
local Renderer = import "UnityEngine.Renderer"
local QnRichLabel = import "QnRichLabel"
local CGuideMgr = import "L10.Game.Guide.CGuideMgr"


LuaFSCInGameWnd = class()


RegistClassMember(LuaFSCInGameWnd, "m_Panel")
RegistClassMember(LuaFSCInGameWnd, "m_Self")
RegistClassMember(LuaFSCInGameWnd, "m_Oppo")
RegistClassMember(LuaFSCInGameWnd, "m_SelfPanel")
RegistClassMember(LuaFSCInGameWnd, "m_SelfUIPanel")
RegistClassMember(LuaFSCInGameWnd, "m_SelfUIPanelDepth")
RegistClassMember(LuaFSCInGameWnd, "m_OppoPanel")
RegistClassMember(LuaFSCInGameWnd, "m_SpeakingBubble")
RegistClassMember(LuaFSCInGameWnd, "m_ChatBtn")
RegistClassMember(LuaFSCInGameWnd, "m_RuleBtn")
RegistClassMember(LuaFSCInGameWnd, "m_RareBtn")
RegistClassMember(LuaFSCInGameWnd, "m_RoundHint")
RegistClassMember(LuaFSCInGameWnd, "m_DiscardHint")
RegistClassMember(LuaFSCInGameWnd, "m_CardTopDetail")
RegistClassMember(LuaFSCInGameWnd, "m_Combination")
RegistClassMember(LuaFSCInGameWnd, "m_DealPile")
RegistClassMember(LuaFSCInGameWnd, "m_RareInEffect")
RegistClassMember(LuaFSCInGameWnd, "m_SelfHandCards")
RegistClassMember(LuaFSCInGameWnd, "m_OppoHandCards")
RegistClassMember(LuaFSCInGameWnd, "m_PublicCards")
RegistClassMember(LuaFSCInGameWnd, "m_Bg")
RegistClassMember(LuaFSCInGameWnd, "m_SelfCardPile")
RegistClassMember(LuaFSCInGameWnd, "m_OppoCardPile")

RegistClassMember(LuaFSCInGameWnd, "m_RoundTick")
RegistClassMember(LuaFSCInGameWnd, "m_RoundHintTick")
RegistClassMember(LuaFSCInGameWnd, "m_SpCardTick")
RegistClassMember(LuaFSCInGameWnd, "m_BubbleTick")
RegistClassMember(LuaFSCInGameWnd, "m_LvUpTick")

RegistClassMember(LuaFSCInGameWnd, "m_SelectedCard")
RegistClassMember(LuaFSCInGameWnd, "m_HasInited")
RegistClassMember(LuaFSCInGameWnd, "m_IsShowCardDetail") -- 是否正在显示顶部卡牌详情
RegistClassMember(LuaFSCInGameWnd, "m_IsOpeningSocialWnd") -- 是否正在打开聊天界面
RegistClassMember(LuaFSCInGameWnd, "m_ReShuffle") -- 是否需要洗牌

RegistClassMember(LuaFSCInGameWnd, "m_InitialPos") -- 1:Self 2:Oppo 3:Public
RegistClassMember(LuaFSCInGameWnd, "m_InitialRotZ")
RegistClassMember(LuaFSCInGameWnd, "m_DealDuration") -- 发一张牌的时间(s)
RegistClassMember(LuaFSCInGameWnd, "m_ReshuffleInterval") -- 洗牌间隔(s)
RegistClassMember(LuaFSCInGameWnd, "m_CollectDuration") -- 出牌收取公共牌回牌堆的时间(s)
RegistClassMember(LuaFSCInGameWnd, "m_MoveEase") -- 发牌的移动曲线
RegistClassMember(LuaFSCInGameWnd, "m_RotEase") -- 发牌的旋转曲线
RegistClassMember(LuaFSCInGameWnd, "m_DealPilePos") -- 公共牌堆相对于各区域的相对位置
RegistClassMember(LuaFSCInGameWnd, "m_SelfPilePos") -- 己方牌堆相对于各区域的相对位置
RegistClassMember(LuaFSCInGameWnd, "m_OppoPilePos") -- 对方牌堆相对于各区域的相对位置
RegistClassMember(LuaFSCInGameWnd, "m_HandCardOffset") -- 手牌默认间距
RegistClassMember(LuaFSCInGameWnd, "m_HandCardsMaxWidth") -- 手牌最大总宽度 (*总宽度表示第一张牌的中心点到最后一张牌的中心点的距离)
RegistClassMember(LuaFSCInGameWnd, "m_HandCardWidth") -- 一张手牌的宽度
RegistClassMember(LuaFSCInGameWnd, "m_HandCardsCenterX") -- 手牌区域中点的X坐标
RegistClassMember(LuaFSCInGameWnd, "m_CombCardsCenterX") -- 组合牌区域中点的X坐标
RegistClassMember(LuaFSCInGameWnd, "m_CombinationMaxWidth") -- 形成组合的牌最大总宽度
RegistClassMember(LuaFSCInGameWnd, "m_BubbleDuration") -- 聊天气泡显示时间(s)
RegistClassMember(LuaFSCInGameWnd, "m_BubbleMaxWidth") -- 聊天气泡最大宽度
RegistClassMember(LuaFSCInGameWnd, "m_StartDealCards") -- 修正一个手牌位置会重置的问题
--RegistClassMember(LuaFSCInGameWnd, "m_Card2Depth")
RegistClassMember(LuaFSCInGameWnd, "m_MaxPanelDepth")
RegistClassMember(LuaFSCInGameWnd, "m_PanelDepthOffset")

RegistClassMember(LuaFSCInGameWnd, "m_Tweeners")
RegistClassMember(LuaFSCInGameWnd, "m_CurDelay")
RegistClassMember(LuaFSCInGameWnd, "m_OnComplete")


function LuaFSCInGameWnd:SetParams()
    self.m_DealDuration = 0.15
    self.m_ReshuffleInterval = 0.15
    self.m_CollectDuration = ZhouNianQing2023_Setting.GetData().ChuPaiFxDuration
    self.m_MoveEase = Ease.OutCubic
    self.m_RotEase = Ease.InOutQuad
    self.m_CardOffset = 400
    self.m_HandCardsMaxWidth = 2016
    self.m_HandCardWidth = 0.5555556 * 360
    self.m_BubbleDuration = ZhouNianQing2023_Setting.GetData().GossipTime
    self.m_BubbleMaxWidth = 482
    self.m_CombinationMaxWidth = 348.32
    self.m_PanelDepthOffset = 12
end

function LuaFSCInGameWnd:Awake()
    self:SetParams()

    self.m_Bg = self.transform:Find("Bg"):GetComponent(typeof(Animation))
    self.m_Self = self.transform:Find("Anchor/Self")
    self.m_SelfHandCards = self.m_Self:Find("HandCards")
    self.m_Oppo = self.transform:Find("Anchor/Enemy")
    self.m_OppoHandCards = self.m_Oppo:Find("HandCards")
    self.m_PublicCards = self.transform:Find("Anchor/Public/Cards")
    self.m_SelfPanel = self.m_Self:Find("Player")
    self.m_OppoPanel = self.m_Oppo:Find("Player")
    self.m_ChatBtn = self.m_Self:Find("Container/ChatBtn").gameObject
    self.m_RuleBtn = self.m_Self:Find("Container/RuleBtn").gameObject
    self.m_RareBtn = self.m_Self:Find("RareBtn").gameObject
    self.m_RoundHint = self.transform:Find("Anchor/Public/FSCRoundHint"):GetComponent(typeof(Animation))
    self.m_CardTopDetail = self.transform:Find("Anchor/FSCCardTopDetail"):GetComponent(typeof(Animation))
    self.m_DiscardHint = self.transform:Find("Anchor/Public/OtherHint").gameObject
    self.m_DealPile = self.transform:Find("Anchor/Public/DealPile").gameObject
    self.m_RareInEffect = self.transform:Find("Anchor/FSCRareInEffect").gameObject
    self.m_Combination = self.transform:Find("Anchor/FSCCombination").gameObject
    self.m_SelfCardPile = self.m_Self:Find("CardPile"):GetChild(0)
    self.m_OppoCardPile = self.m_Oppo:Find("CardPile"):GetChild(0)
    self.m_SpeakingBubble = {
        self.m_Self:Find("SpeakingBubble/Bubble"):GetComponent(typeof(Animation)),
        self.m_Oppo:Find("SpeakingBubble/Bubble"):GetComponent(typeof(Animation)),
    }

    self.m_Self.gameObject:SetActive(true)
    self.m_Oppo.gameObject:SetActive(true)
    self.m_SelfPanel:Find("ScoreLabel"):GetComponent(typeof(UILabel)).text = LuaFourSeasonCardMgr.SelfInfo.score or 0
    self.m_OppoPanel:Find("ScoreLabel"):GetComponent(typeof(UILabel)).text = LuaFourSeasonCardMgr.OpponentInfo.score or 0
    self.m_SelfPanel:Find("ScoreLabel/Add").gameObject:SetActive(false)
    self.m_OppoPanel:Find("ScoreLabel/Add").gameObject:SetActive(false)
    self.m_DiscardHint:SetActive(false)
    self.m_DealPile.transform:GetChild(0).gameObject:SetActive(false)
    self.m_RoundHint.gameObject:SetActive(false)
    self.m_CardTopDetail.gameObject:SetActive(false)
    self.m_RareInEffect:SetActive(false)
    self.m_Combination:SetActive(false)
    self.m_SpeakingBubble[1].gameObject:SetActive(false)
    self.m_SpeakingBubble[2].gameObject:SetActive(false)
    self.m_DiscardHint.transform:Find("Label"):GetComponent(typeof(UILabel)).text = ZhouNianQing2023_Setting.GetData().FoldTips
    self.m_Self:Find("CardPile").gameObject:SetActive(false)
    self.m_Oppo:Find("CardPile").gameObject:SetActive(false)

    UIEventListener.Get(self.m_ChatBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        if not LuaFourSeasonCardMgr.EnableOperation or LuaFourSeasonCardMgr.GuidingStage then return end
        self.m_IsOpeningSocialWnd = true
        CSocialWndMgr.ShowChatWnd(EChatPanel.Current)
    end)
    UIEventListener.Get(self.m_RuleBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        if not LuaFourSeasonCardMgr.EnableOperation or LuaFourSeasonCardMgr.GuidingStage then return end
        g_MessageMgr:ShowMessage("FSC_IN_GAME_RULE")
    end)
    UIEventListener.Get(self.m_RareBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        if not LuaFourSeasonCardMgr.EnableOperation and not CGuideMgr.Inst:IsInPhase(EnumGuideKey.FSC_Guide2) then return end
        Gac2Gas.Anniv2023FSC_QuerySpecialCards()
    end)
    --UIEventListener.Get(self.m_DealPile).onClick = DelegateFactory.VoidDelegate(function (go)
    --    if self.m_SelectedCard > 0 and self.m_DiscardHint.activeSelf then
    --        Gac2Gas.Anniv2023FSC_RequestReplaceCard(self.m_SelectedCard)
    --    end
    --end)

    self.m_SelectedCard = 0
    self.m_RoundTick = {}
    self.m_BubbleTick = {}

    self.m_Tweeners = {}
    self.m_OnComplete = {}

    self.m_StartDealCards = false
    --self.m_Card2Depth = {}
    self.m_HasInited = false
    self.m_IsShowCardDetail = false
    self.m_IsOpeningSocialWnd = false
    self.m_MaxPanelDepth = 0

    self.m_InitialPos = {}
    self.m_InitialPos[1] = {}
    for i = 0, 9 do
        local card = self.m_SelfHandCards:GetChild(i)
        table.insert(self.m_InitialPos[1], card.localPosition) 
        card.gameObject:SetActive(false)
    end
    self.m_InitialPos[2] = {}
    for i = 0, 9 do
        local card = self.m_OppoHandCards:GetChild(i)
        table.insert(self.m_InitialPos[2], card.localPosition)
        card.gameObject:SetActive(false)
    end
    self.m_InitialPos[3] = {}
    for i = 0, 9 do
        local card = self.m_PublicCards:GetChild(i)
        self.m_InitialRotZ = card.localEulerAngles.z
        table.insert(self.m_InitialPos[3], card.localPosition)
        card.gameObject:SetActive(false)
    end
    self.m_HandCardsCenterX = {
        (self.m_InitialPos[1][5].x + self.m_InitialPos[1][6].x) / 2,
        (self.m_InitialPos[2][5].x + self.m_InitialPos[2][6].x) / 2,
    }
    self.m_CombCardsCenterX = (self.m_Combination.transform:Find("Cards"):GetChild(0).localPosition.x + self.m_Combination.transform:Find("Cards"):GetChild(5).localPosition.x ) / 2

    local pos = self.m_DealPile.transform.position
    pos = Vector3(pos.x - 15, pos.y + 10, pos.z)
    self.m_DealPilePos = {
        self.m_SelfHandCards:InverseTransformPoint(pos),
        self.m_OppoHandCards:InverseTransformPoint(pos),
        self.m_PublicCards:InverseTransformPoint(pos),
    }
    pos = self.m_Self:Find("CardPile").position
    self.m_SelfPilePos = {
        self.m_SelfHandCards:InverseTransformPoint(pos),
        self.m_OppoHandCards:InverseTransformPoint(pos),
        self.m_PublicCards:InverseTransformPoint(pos),
    }
    pos = self.m_Oppo:Find("CardPile").position
    self.m_OppoPilePos = {
        self.m_SelfHandCards:InverseTransformPoint(pos),
        self.m_OppoHandCards:InverseTransformPoint(pos),
        self.m_PublicCards:InverseTransformPoint(pos),
    }

    self:InitPlayerPanel(LuaFourSeasonCardMgr.SelfInfo, true, nil, true)
    self:InitPlayerPanel(LuaFourSeasonCardMgr.OpponentInfo, false, nil, true)
end

-- 处理几个弹窗面板的层级
-- wndDepth = 0
-- cardDepth: wndDepth + 1..10
-- cardSetTopDepth: wndDepth + 11
-- panelDepth: >= wndDepth + 12
function LuaFSCInGameWnd:InitPanelDepth()
    local panel

    panel = self.m_RoundHint:GetComponent(typeof(UIPanel))
    panel.depth = panel.depth + self.m_PanelDepthOffset
    self.m_MaxPanelDepth = math.max(self.m_MaxPanelDepth, panel.depth)

    panel = self.m_RareInEffect:GetComponent(typeof(UIPanel))
    panel.depth = panel.depth + self.m_PanelDepthOffset
    self.m_MaxPanelDepth = math.max(self.m_MaxPanelDepth, panel.depth)

    panel = self.m_CardTopDetail:GetComponent(typeof(UIPanel))
    panel.depth = panel.depth + self.m_PanelDepthOffset
    self.m_MaxPanelDepth = math.max(self.m_MaxPanelDepth, panel.depth)

    panel = self.m_Combination:GetComponent(typeof(UIPanel))
    panel.depth = panel.depth + self.m_PanelDepthOffset
    self.m_MaxPanelDepth = math.max(self.m_MaxPanelDepth, panel.depth)
    local combinationCards = self.m_Combination.transform:Find("Cards")
    local depth = self.m_Combination:GetComponent(typeof(UIPanel)).depth
    for i = 0, 5 do
        local card = combinationCards:GetChild(i)
        local panel = card:GetComponent(typeof(UIPanel))
        if not panel then panel = card.gameObject:AddComponent(typeof(UIPanel)) end
        panel.depth = depth + 6 - i
        self.m_MaxPanelDepth = math.max(self.m_MaxPanelDepth, panel.depth)
    end

    panel = self.m_SpeakingBubble[1].transform.parent:GetComponent(typeof(UIPanel))
    panel.depth = panel.depth + self.m_PanelDepthOffset
    self.m_MaxPanelDepth = math.max(self.m_MaxPanelDepth, panel.depth)

    panel = self.m_SpeakingBubble[2].transform.parent:GetComponent(typeof(UIPanel))
    panel.depth = panel.depth + self.m_PanelDepthOffset
    self.m_MaxPanelDepth = math.max(self.m_MaxPanelDepth, panel.depth)

    panel = self.m_DiscardHint:GetComponent(typeof(UIPanel))
    panel.depth = panel.depth + self.m_PanelDepthOffset
    self.m_MaxPanelDepth = math.max(self.m_MaxPanelDepth, panel.depth)

    panel = self.m_SelfPanel:GetComponent(typeof(UIPanel))
    self.m_SelfUIPanel = panel
    panel.depth = panel.depth + self.m_PanelDepthOffset
    self.m_SelfUIPanelDepth = panel.depth
    self.m_MaxPanelDepth = math.max(self.m_MaxPanelDepth, panel.depth)

    panel = self.m_OppoPanel:GetComponent(typeof(UIPanel))
    panel.depth = panel.depth + self.m_PanelDepthOffset
    self.m_MaxPanelDepth = math.max(self.m_MaxPanelDepth, panel.depth)
end

-- 收集所有牌到公共牌堆为发牌做准备
function LuaFSCInGameWnd:CollectCards()
    for i = 0, 9 do 
        local card = self.m_SelfHandCards:GetChild(i)
        card.gameObject:SetActive(false)
        card.localPosition = self.m_DealPilePos[1]
    end
    for i = 0, 9 do 
        local card = self.m_OppoHandCards:GetChild(i)
        card.gameObject:SetActive(false)
        card.localPosition = self.m_DealPilePos[2]
    end
    for i = 0, 9 do  
        local card = self.m_PublicCards:GetChild(i)
        card.gameObject:SetActive(false)
        if i >= 2 then -- 公共牌只发8张
            card.localEulerAngles = Vector3.zero
            card.localPosition = self.m_DealPilePos[3]
        end
    end
end

function LuaFSCInGameWnd:FindCard(id, side) -- 1: Self, 2: Oppo, 3:Public 
    if side then
        local owner = side == 1 and self.m_SelfHandCards or (side == 2 and self.m_OppoHandCards or self.m_PublicCards)
        for i = 0, 9 do 
            local card = owner:GetChild(i)
            local script = card:GetComponent(typeof(CCommonLuaScript))
            script:Awake()
            script = script.m_LuaSelf
            if script and script.m_Id == id then
                return script, i
            end
        end
    else
        for i = 0, 9 do 
            local card = self.m_SelfHandCards:GetChild(i)
            local script = card:GetComponent(typeof(CCommonLuaScript))
            script:Awake()
            script = script.m_LuaSelf
            if script and script.m_Id == id then
                return script, i
            end
        end
        for i = 0, 9 do 
            local card = self.m_OppoHandCards:GetChild(i)
            local script = card:GetComponent(typeof(CCommonLuaScript))
            script:Awake()
            script = script.m_LuaSelf
            if script and script.m_Id == id then
                return script, i
            end
        end
        for i = 0, 9 do  
            local card = self.m_PublicCards:GetChild(i)
            local script = card:GetComponent(typeof(CCommonLuaScript))
            script:Awake()
            script = script.m_LuaSelf
            if script and script.m_Id == id then
                return script, i
            end
        end
    end
end

function LuaFSCInGameWnd:FindEmpty(side) 
    if side then
        local owner = side == 1 and self.m_SelfHandCards or (side == 2 and self.m_OppoHandCards or self.m_PublicCards)
        for i = 9, 0, -1 do 
            local card = owner:GetChild(i)
            if card.gameObject.activeSelf == false then
                return card
            end
        end
    else
        for i = 9, 0, -1 do 
            local card = self.m_SelfHandCards:GetChild(i)
            if card.gameObject.activeSelf == false then
                return card
            end
        end
        for i = 9, 0, -1 do 
            local card = self.m_OppoHandCards:GetChild(i)
            if card.gameObject.activeSelf == false then
                return card
            end
        end
        for i = 9, 0, -1 do 
            local card = self.m_PublicCards:GetChild(i)
            if card.gameObject.activeSelf == false then
                return card
            end
        end
    end
end

function LuaFSCInGameWnd:FindBack()
    for i = 0, 9 do 
        local card = self.m_OppoHandCards:GetChild(i)
        local script = card:GetComponent(typeof(CCommonLuaScript))
        script:Awake()
        script = script.m_LuaSelf
        if script and script.m_Status == EnumFSC_CardStatus.Back then
            return script, i
        end
    end
end

function LuaFSCInGameWnd:InitPlayerPanel(info, myPanel, isMyRound, init) 
    isMyRound = isMyRound or LuaFourSeasonCardMgr.bIsOnMove
    local trans = myPanel and self.m_SelfPanel or self.m_OppoPanel
    if not trans then 
        return 
    end
    if info.isPlayer then
        trans:Find("Border/Icon"):GetComponent(typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(info.cls, info.sex, -1), false)
        trans:Find("NameLabel"):GetComponent(typeof(UILabel)).text = info.name or ""
    elseif info.npcId then
        local npc = NPC_NPC.GetData(info.npcId)
        if npc then
            trans:Find("Border/Icon"):GetComponent(typeof(CUITexture)):LoadNPCPortrait(npc.Portrait, false)
            trans:Find("NameLabel"):GetComponent(typeof(UILabel)).text = npc.Name or ""
        end
    end
    if init then
        trans:Find("Border/Mask").gameObject:SetActive(false)
    else
        trans:Find("Border/Mask").gameObject:SetActive(info.status ~= EnumFSC_PlayerStatus.eNormal or myPanel == isMyRound)
        trans:Find("Border/Mask/TuoGuan").gameObject:SetActive(info.status ~= EnumFSC_PlayerStatus.eNormal)
    
        local countLabel = trans:Find("Border/Mask/Countdown"):GetComponent(typeof(UILabel))
        if myPanel == isMyRound then
            countLabel.gameObject:SetActive(true)
            local count = math.floor(LuaFourSeasonCardMgr.CountDownEndTime - CServerTimeMgr.Inst.timeStamp)
            if count > 0 then
                countLabel.text = count
                self.m_RoundTick[myPanel and 1 or 2] = RegisterTick(function()
                    count = math.floor(LuaFourSeasonCardMgr.CountDownEndTime - CServerTimeMgr.Inst.timeStamp)
                    if count > 0 then
                        countLabel.text = count
                    else
                        UnRegisterTick(self.m_RoundTick[myPanel and 1 or 2])
                        self.m_RoundTick[myPanel and 1 or 2] = nil
                    end
                end, 500)
            else
                countLabel.text = "0"
            end
        else
            countLabel.gameObject:SetActive(false)
        end
    end
end

function LuaFSCInGameWnd:InitBoardCards(boardCards, leaveSpace)
    if not self.m_PublicCards then return end
    local cards = boardCards or LuaFourSeasonCardMgr.BoardCards
    local st = 10 - #cards + 1
    for i = 1, st - 1 do
        local card = self.m_PublicCards:GetChild(i - 1)
        card.transform.localPosition = self.m_InitialPos[3][i]
        card.transform.localEulerAngles = Vector3(0, 0, self.m_InitialRotZ)
        card.gameObject:SetActive(false)
    end
    for i = st, 10 do
        local card = self.m_PublicCards:GetChild(i - 1)
        card.transform.localPosition = self.m_InitialPos[3][i]
        card.transform.localEulerAngles = Vector3(0, 0, self.m_InitialRotZ)
        if i < 10 or not leaveSpace then
            card.gameObject:SetActive(true)
            local script = card:GetComponent(typeof(CCommonLuaScript))
            script:Awake()
            script.m_LuaSelf:Init(cards[i - st + 1])
        else
            card.gameObject:SetActive(false)
        end
    end
end

function LuaFSCInGameWnd:InitSelfCards(handCards, lastCard, cardPileNum)
    if not self.m_SelfHandCards then return end
    local cards = handCards or LuaFourSeasonCardMgr.HandCards
    cardPileNum = cardPileNum or LuaFourSeasonCardMgr.SelfCardPileNum
    local center = (#cards + 1) / 2
    local offset = self.m_CardOffset
    local needWidth = (#cards - 1) * offset
    if needWidth > self.m_HandCardsMaxWidth then
        offset = self.m_HandCardsMaxWidth  / (#cards - 1)
    end

    for i = 1, #cards do
        local card = self.m_SelfHandCards:GetChild(i - 1)
        LuaUtils.SetLocalPosition(card, self.m_HandCardsCenterX[1] + (i - center) * offset, self.m_InitialPos[1][i].y, 0)
        card.gameObject:SetActive(true)
        local isRare = LuaFourSeasonCardMgr.SelfSpecialCards[cards[i]] and true or false
        local script = card:GetComponent(typeof(CCommonLuaScript))
        if not script then script = script.transform:GetChild(0):GetComponent(typeof(CCommonLuaScript)) end
        if script then
            script:Awake()
            script.m_LuaSelf:Init(cards[i], isRare)
        end
    end
    for i = #cards + 1, 10 do
        local card = self.m_SelfHandCards:GetChild(i - 1)
        card.gameObject:SetActive(false)
    end

    local top = lastCard or LuaFourSeasonCardMgr.SelfLastCard
    local isRare = LuaFourSeasonCardMgr.SelfSpecialCards[top] and true or false
    local pile = self.m_Self:Find("CardPile")
    if not top or top == 0 then
        pile.gameObject:SetActive(false)
    else
        pile.gameObject:SetActive(true)
        local backs = pile:Find("Backs").gameObject
        local back = pile:Find("Back").gameObject
        Extensions.RemoveAllChildren(backs.transform)
        local depth = 4
        cardPileNum = math.min(cardPileNum, 5)
        for i = cardPileNum - 1, 1, -1 do
            local go = NGUITools.AddChild(backs, back)
            go:SetActive(true)
            LuaUtils.SetLocalPosition(go.transform, -i * 8, i * 8, 0)
            go:GetComponent(typeof(UIWidget)).depth = depth
            depth = depth + 1
        end
        local script = self.m_SelfCardPile:GetComponent(typeof(CCommonLuaScript))
        script:Awake()
        script.m_LuaSelf:Init(top, isRare)
    end
end

function LuaFSCInGameWnd:InitOppoCards(cardNum, showedCards, lastCard, cardPileNum)
    if not self.m_OppoHandCards then return end
    local num = cardNum or LuaFourSeasonCardMgr.OpponentCardNum
    local cardDict = showedCards or LuaFourSeasonCardMgr.OpponentShowedCards
    local cards = {}
    for cardId, _ in pairs(cardDict) do 
        table.insert(cards, cardId)
    end
    cardPileNum = cardPileNum or LuaFourSeasonCardMgr.OppoCardPileNum
    local center = (num + 1) / 2
    local offset = self.m_CardOffset
    local needWidth = (num - 1) * offset
    if needWidth > self.m_HandCardsMaxWidth then
        offset = self.m_HandCardsMaxWidth / (num - 1)
    end

    for i = 1, #cards do
        local card = self.m_OppoHandCards:GetChild(i - 1)
        LuaUtils.SetLocalPosition(card, self.m_HandCardsCenterX[2] + (i - center) * offset, self.m_InitialPos[2][i].y, 0)
        card.gameObject:SetActive(true)
        local script = card:GetComponent(typeof(CCommonLuaScript))
        script:Awake()
        script.m_LuaSelf:Init(cards[i])
    end
    for i = #cards + 1, num do
        local card = self.m_OppoHandCards:GetChild(i - 1)
        LuaUtils.SetLocalPosition(card, self.m_HandCardsCenterX[2] + (i - center) * offset, self.m_InitialPos[2][i].y, 0)
        card.gameObject:SetActive(true)
        local script = card:GetComponent(typeof(CCommonLuaScript))
        script:Awake()
        script.m_LuaSelf:Init(0, false, EnumFSC_CardStatus.Back)
    end
    for i = num + 1, 10 do
        local card = self.m_OppoHandCards:GetChild(i - 1)
        card.gameObject:SetActive(false)
    end

    local top = lastCard or LuaFourSeasonCardMgr.OpponentLastCard
    local pile = self.m_Oppo:Find("CardPile")
    if not top or top == 0 then
        pile.gameObject:SetActive(false)
    else
        pile.gameObject:SetActive(true)
        local backs = pile:Find("Backs").gameObject
        local back = pile:Find("Back").gameObject
        Extensions.RemoveAllChildren(backs.transform)
        local depth = 4
        cardPileNum = math.min(cardPileNum, 5)
        for i = cardPileNum - 1, 1, -1 do
            local go = NGUITools.AddChild(backs, back)
            go:SetActive(true)
            LuaUtils.SetLocalPosition(go.transform, -i * 8, i * 8, 0)
            go:GetComponent(typeof(UIWidget)).depth = depth
            depth = depth + 1
        end
        local script = self.m_OppoCardPile:GetComponent(typeof(CCommonLuaScript))
        script:Awake()
        script.m_LuaSelf:Init(top)
    end
end

function LuaFSCInGameWnd:InitRound(animated, isMyRound)
    isMyRound = isMyRound or LuaFourSeasonCardMgr.bIsOnMove
    if animated and not CommonDefs.IsNull(self.m_Bg) then
        self.m_Bg:Play(isMyRound and "fscingamewnd_switch_etom" or "fscingamewnd_switch_mtoe")
    end
  
    if self.m_RoundTick then
        for i, _ in pairs(self.m_RoundTick) do
            UnRegisterTick(self.m_RoundTick[i])
            self.m_RoundTick[i] = nil
        end
    end
    self:InitPlayerPanel(LuaFourSeasonCardMgr.SelfInfo, true, isMyRound)
    self:InitPlayerPanel(LuaFourSeasonCardMgr.OpponentInfo, false, isMyRound)

    if self.m_DiscardHint then
        self.m_DiscardHint:SetActive(false)
    end
    if self.m_CardTopDetail then
        self.m_CardTopDetail.gameObject:SetActive(false)
    end
    if self.m_DealPile then
        self.m_DealPile.transform:GetChild(0).gameObject:SetActive(false)
    end
    self.m_SelectedCard = 0

    if self.m_SelfPanel then
        local scoreLabel1 = self.m_SelfPanel:Find("ScoreLabel"):GetComponent(typeof(UILabel))
        scoreLabel1.text = LuaFourSeasonCardMgr.SelfInfo.score or 0
    end
    if self.m_OppoPanel then
        local scoreLabel2 = self.m_OppoPanel:Find("ScoreLabel"):GetComponent(typeof(UILabel))
        scoreLabel2.text = LuaFourSeasonCardMgr.OpponentInfo.score or 0
    end

    self:InitCardDepth()
end

function LuaFSCInGameWnd:Update()
    if self.m_StartDealCards then
        for i = 0, 9 do 
            local card = self.m_SelfHandCards:GetChild(i)
            card.localPosition = self.m_DealPilePos[1]
        end
        for i = 0, 9 do 
            local card = self.m_OppoHandCards:GetChild(i)
            card.localPosition = self.m_DealPilePos[2]
        end
        self.m_StartDealCards = false
    end

    if self.m_IsOpeningSocialWnd then
        local obj = CUIManager.instance:GetGameObject(CUIResources.SocialWnd)
        if obj then
            local panel = obj:GetComponent(typeof(UIPanel))
            if panel and panel.depth <= self.m_MaxPanelDepth then
                local add = self.m_MaxPanelDepth - panel.depth + 1
                local panels = CommonDefs.GetComponentsInChildren_GameObject_Type_Boolean(obj, typeof(UIPanel), true)
                CommonDefs.EnumerableIterate(panels, DelegateFactory.Action_object(function (p) 
                    p.depth = p.depth + add
                end))
            end
            self.m_IsOpeningSocialWnd = false
        end
    end

    if LuaFSCPrepareRareCardWnd.s_Panel then
        self.m_SelfUIPanel.depth = LuaFSCPrepareRareCardWnd.s_Panel.depth + 1
    elseif LuaFSCCardPileWnd.s_Panel then
        self.m_SelfUIPanel.depth = LuaFSCCardPileWnd.s_Panel.depth + 1
    else
        self.m_SelfUIPanel.depth = self.m_SelfUIPanelDepth
    end
end

function LuaFSCInGameWnd:InitCardDepth() 
    if not self.m_Panel then return end
    local depth = self.m_Panel.depth
    LuaFourSeasonCardMgr.MaxCardDepth = depth + 10
    for i = 0, 9 do
        local card = self.m_SelfHandCards:GetChild(i)
        local panel = card:GetComponent(typeof(UIPanel))
        if not panel then panel = card.gameObject:AddComponent(typeof(UIPanel)) end
        --self.m_Card2Depth[card] = depth + 10 - i
        panel.depth = depth + 10 - i
        panel.sortingOrder = 0
    end
    for i = 0, 9 do
        local card = self.m_OppoHandCards:GetChild(i)
        local panel = card:GetComponent(typeof(UIPanel))
        if not panel then panel = card.gameObject:AddComponent(typeof(UIPanel)) end
        --self.m_Card2Depth[card] = depth + 10 - i
        panel.depth = depth + 10 - i
        panel.sortingOrder = 0
    end
    for i = 0, 9 do
        local card = self.m_PublicCards:GetChild(i)
        local panel = card:GetComponent(typeof(UIPanel))
        if not panel then panel = card.gameObject:AddComponent(typeof(UIPanel)) end
        --self.m_Card2Depth[card] = depth + 10 - i
        panel.depth = depth + 10 - i
        panel.sortingOrder = 0
    end

    --self:InitPanelDepth()
end

function LuaFSCInGameWnd:Init()
    if self.m_HasInited then return end

    self.m_Panel = self.transform:GetComponent(typeof(UIPanel))
    local anim = self.transform:GetComponent(typeof(Animation))
    self:Delay(function(self)
        if not CommonDefs.IsNull(anim) then
            anim["fscingamewnd_show"].speed = 0.7
            anim:Play("fscingamewnd_show")
            return "OPEN", 0.5
        end
    end)

    UIEventListener.Get(self.transform:Find("Bg/CloseButton").gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("CUSTOM_STRING2", LocalString.GetString("确认要关闭并放弃本场四时戏牌局吗？")), function()
            Gac2Gas.Anniv2023FSC_RequestLeaveGame()
        end, nil, nil, nil, false)
    end)

    local bgMask = self.transform:Find("_BgMask_")
    if bgMask then bgMask:GetComponent(typeof(UITexture)).alpha = 1 end

    self:InitCardDepth()
    self:InitPanelDepth()
    
    self:RemoveListener()
    self:AddListener()

    if LuaFourSeasonCardMgr.IsDealCards then -- 处理界面初始化之前收到的OnDealCards
        self:OnDealCards()
    elseif LuaFourSeasonCardMgr.GuidingStage and LuaFourSeasonCardMgr.GuidingStage > 1 then
        --self:InitRound(true, isMyRound)
        self:InitBoardCards()
        self:InitSelfCards()
        self:InitOppoCards()
    end
    if LuaFourSeasonCardMgr.bIsOnMove ~= nil then -- 处理界面初始化之前收到的StartRound
        self:StartRound()
    end
    if LuaFourSeasonCardMgr.CachedAddNewCombo then
        local p = LuaFourSeasonCardMgr.CachedAddNewCombo
        self:StartAddNewCombo(p[1], p[2], p[3], p[4])
        LuaFourSeasonCardMgr.CachedAddNewCombo = nil
    end
    
    self.m_HasInited = true
end

function LuaFSCInGameWnd:AddListener()
    g_ScriptEvent:AddListener("RecvChatMsg", self, "OnRecvChatMsg")
    g_ScriptEvent:AddListener("FSC_StartRound" ,self, "StartRound")
    g_ScriptEvent:AddListener("FSC_UpdateScore" ,self, "UpdateScore")
    g_ScriptEvent:AddListener("FSC_OnDealCards" ,self, "OnDealCards")
    g_ScriptEvent:AddListener("FSC_OnAddBoardCard" ,self, "OnAddBoardCard")
    g_ScriptEvent:AddListener("FSC_OnReShuffleBoardCard" ,self, "OnReShuffleBoardCard")
    g_ScriptEvent:AddListener("FSC_ShowCardTopDetail" ,self, "ShowCardTopDetail")
    g_ScriptEvent:AddListener("FSC_SelectCard" ,self, "SelectCard")
    g_ScriptEvent:AddListener("FSC_ChooseBoardCard" ,self, "ChooseBoardCard")
    g_ScriptEvent:AddListener("FSC_StartSpCardEffect" ,self, "StartSpCardEffect")
    g_ScriptEvent:AddListener("FSC_DisableOperation" ,self, "Reset")
    g_ScriptEvent:AddListener("FSC_TurnOverCards" ,self, "TurnOverCards")
    g_ScriptEvent:AddListener("FSC_OnSelfUseCard" ,self, "OnSelfUseCard")
    g_ScriptEvent:AddListener("FSC_OnOpponentUseCard" ,self, "OnOpponentUseCard")
    g_ScriptEvent:AddListener("FSC_OnSelfReplaceCard" ,self, "OnSelfReplaceCard")
    g_ScriptEvent:AddListener("FSC_OnOpponentReplaceCard" ,self, "OnOpponentReplaceCard")
    g_ScriptEvent:AddListener("FSC_StartAddNewCombo" ,self, "StartAddNewCombo")
    g_ScriptEvent:AddListener("FSC_BanCards" ,self, "BanCards")
    g_ScriptEvent:AddListener("FSC_CopyCards" ,self, "CopyCards")
    g_ScriptEvent:AddListener("FSC_SpCardEffectEnd" ,self, "SpCardEffectEnd")
    g_ScriptEvent:AddListener("FSC_OpponentStartSkill" ,self, "OpponentStartSkill")
    g_ScriptEvent:AddListener("FSC_StartReplicate" ,self, "StartReplicate")
    g_ScriptEvent:AddListener("FSC_StartInterrupt" ,self, "StartInterrupt")
    g_ScriptEvent:AddListener("FSC_SendCollectedCardsData" ,self, "SendCollectedCardsData")

    g_ScriptEvent:AddListener("FSC_LevelUpHandCard" ,self, "LevelUpHandCard")
    g_ScriptEvent:AddListener("FSC_StartRareInEffect" ,self, "StartRareInEffect")
    g_ScriptEvent:AddListener("FSC_CloseRareInEffect" ,self, "CloseRareInEffect")
    g_ScriptEvent:AddListener("FSC_ShowBubble" ,self, "ShowBubble")
    g_ScriptEvent:AddListener("FSC_InitCardDepth" ,self, "InitCardDepth")
end

function LuaFSCInGameWnd:RemoveListener()
    g_ScriptEvent:RemoveListener("RecvChatMsg", self, "OnRecvChatMsg")
    g_ScriptEvent:RemoveListener("FSC_StartRound" ,self, "StartRound")
    g_ScriptEvent:RemoveListener("FSC_UpdateScore" ,self, "UpdateScore")
    g_ScriptEvent:RemoveListener("FSC_OnDealCards" ,self, "OnDealCards")
    g_ScriptEvent:RemoveListener("FSC_OnAddBoardCard" ,self, "OnAddBoardCard")
    g_ScriptEvent:RemoveListener("FSC_OnReShuffleBoardCard" ,self, "OnReShuffleBoardCard")
    g_ScriptEvent:RemoveListener("FSC_ShowCardTopDetail" ,self, "ShowCardTopDetail")
    g_ScriptEvent:RemoveListener("FSC_SelectCard" ,self, "SelectCard")
    g_ScriptEvent:RemoveListener("FSC_ChooseBoardCard" ,self, "ChooseBoardCard")
    g_ScriptEvent:RemoveListener("FSC_DisableOperation" ,self, "Reset")
    g_ScriptEvent:RemoveListener("FSC_TurnOverCards" ,self, "TurnOverCards")
    g_ScriptEvent:RemoveListener("FSC_OnSelfUseCard" ,self, "OnSelfUseCard")
    g_ScriptEvent:RemoveListener("FSC_OnOpponentUseCard" ,self, "OnOpponentUseCard")
    g_ScriptEvent:RemoveListener("FSC_OnSelfReplaceCard" ,self, "OnSelfReplaceCard")
    g_ScriptEvent:RemoveListener("FSC_OnOpponentReplaceCard" ,self, "OnOpponentReplaceCard")
    g_ScriptEvent:RemoveListener("FSC_StartAddNewCombo" ,self, "StartAddNewCombo")
    g_ScriptEvent:RemoveListener("FSC_BanCards" ,self, "BanCards")
    g_ScriptEvent:RemoveListener("FSC_CopyCards" ,self, "CopyCards")
    g_ScriptEvent:RemoveListener("FSC_SpCardEffectEnd" ,self, "SpCardEffectEnd")
    g_ScriptEvent:RemoveListener("FSC_OpponentStartSkill" ,self, "OpponentStartSkill")
    g_ScriptEvent:RemoveListener("FSC_StartReplicate" ,self, "StartReplicate")
    g_ScriptEvent:RemoveListener("FSC_StartInterrupt" ,self, "StartInterrupt")
    g_ScriptEvent:RemoveListener("FSC_SendCollectedCardsData" ,self, "SendCollectedCardsData")

    g_ScriptEvent:RemoveListener("FSC_LevelUpHandCard" ,self, "LevelUpHandCard")
    g_ScriptEvent:RemoveListener("FSC_StartRareInEffect" ,self, "StartRareInEffect")
    g_ScriptEvent:RemoveListener("FSC_CloseRareInEffect" ,self, "CloseRareInEffect")
    g_ScriptEvent:RemoveListener("FSC_ShowBubble" ,self, "ShowBubble")
    g_ScriptEvent:RemoveListener("FSC_InitCardDepth" ,self, "InitCardDepth")
end

function LuaFSCInGameWnd:OnRecvChatMsg()
    if not CClientMainPlayer.Inst then return end
    local msg = CChatMgr.Inst:GetLatestMsg()
    if msg.channelName == CChatMgr.CHANNEL_NAME_CURRENT and msg.fromUserId > 0 and string.sub(msg.message, 1, string.len("voice://")) ~= "voice://" then -- 屏蔽语音消息
        if msg.fromUserId == CClientMainPlayer.Inst.Id then
            self:ShowBubble(1, msg.message)
        elseif msg.fromUserId == LuaFourSeasonCardMgr.OpponentInfo.id then
            self:ShowBubble(2, msg.message)
        end
    end
end

function LuaFSCInGameWnd:ShowBubble(side, msg)
    local bubble = self.m_SpeakingBubble[side]
    if CommonDefs.IsNull(bubble) then return end
    local label = bubble.transform:Find("Label"):GetComponent(typeof(UILabel))
    label.EnableEmotion = true
    label:GetComponent(typeof(QnRichLabel)).enabled = true
    if side == 1 then
        --label.alignment = NGUIText.Alignment.Automatic
        label.pivot = UIWidget.Pivot.BottomLeft
    end
    
    msg = NGUIText.StripSymbols(CUICommonDef.TranslateToNGUIText(msg))
    label.text = msg
    label:UpdateNGUIText()
    NGUIText.regionWidth = 1000000
    local size = NGUIText.CalculatePrintedSize(msg)
    label.width = size.x < self.m_BubbleMaxWidth and math.ceil(size.x) or self.m_BubbleMaxWidth

    --local default, str = label:Wrap(msg) -- UILabel: MaxLines = 0, Overflow = ClampContent
    --if not default then
    --    label.text = CommonDefs.StringSubstring2(str, 0, CommonDefs.StringLength(str) - 1) .. "..."
    --end

    if side == 1 then
        RegisterTickOnce(function()
            if not CommonDefs.IsNull(bubble) and not CommonDefs.IsNull(label) then
                local bg = bubble.transform:Find("Background"):GetComponent(typeof(UITexture))
                if bg then
                    LuaUtils.SetLocalPositionX(label.transform, -468 + 509 - bg.width)
                end
            end
        end, 33)
    end
    
    bubble.gameObject:SetActive(true)
    bubble:Play("fscingamewnd_bubble_in")
    UnRegisterTick(self.m_BubbleTick[side])
    self.m_BubbleTick[side] = RegisterTickOnce(function()
        bubble:Play("fscingamewnd_bubble_out")
    end, self.m_BubbleDuration * 1000)
end

function LuaFSCInGameWnd:SendSkillMsg(isPlayer, skillId, ...) 
    local skillSucceedMsg = ZhouNianQing2023_Setting.GetData().SkillSucceedMsg
    if skillSucceedMsg then
        skillId = skillId % 1000
        self:SendMsg(isPlayer, skillSucceedMsg[skillId - 1], ...)
    end
end

function LuaFSCInGameWnd:SendMsg(isPlayer, msg, ...)
    if type(msg) == "number" then
        msg = g_MessageMgr:Format(Message_Message.GetData(msg).Message, ...)
    else
        msg = CUICommonDef.TranslateToNGUIText(msg)
    end
    if isPlayer then
        --self:ShowBubble(1, msg)
        CChatHelper.SendMsg(EChatPanel.Current, msg, 0)
    else
        self:ShowBubble(2, msg)
    end
end

function LuaFSCInGameWnd:Reset()
    self.m_SelectedCard = 0
    --self:InitRound()
    if self.m_CardTopDetail then
        self.m_CardTopDetail.gameObject:SetActive(false)
    end
end

function LuaFSCInGameWnd:OnDestroy()
    self:RemoveListener()

    self:ClearTweeners()
    for i, _ in pairs(self.m_RoundTick) do
        UnRegisterTick(self.m_RoundTick[i])
        self.m_RoundTick[i] = nil
    end
    for i, _ in ipairs(self.m_BubbleTick) do
        UnRegisterTick(self.m_BubbleTick[i])
        self.m_BubbleTick[i] = nil
    end
    UnRegisterTick(self.m_RoundHintTick)
    self.m_RoundHintTick = nil
    UnRegisterTick(self.m_SpCardTick)
    self.m_SpCardTick = nil
    UnRegisterTick(self.m_LvUpTick)
    self.m_LvUpTick = nil

    LuaFourSeasonCardMgr:OnLeaveGame()
end

function LuaFSCInGameWnd:ClearTweeners()
    if self.m_Tweeners then
        for i = 1, #self.m_Tweeners do
            LuaTweenUtils.Kill(self.m_Tweeners[i], false)
        end
    end
    self.m_Tweeners = {}
    if self.m_CurDelay then
        UnRegisterTick(self.m_CurDelay.tick)
    end
    self.m_CurDelay = nil
end

function LuaFSCInGameWnd:StartRound()
    LuaFourSeasonCardMgr:SetEnableOperation(false)
    if LuaFourSeasonCardMgr.IsDealCards then
        LuaFourSeasonCardMgr.IsDealCards = false
        self:ClearTweeners()
    end
    
    local isMyRound = LuaFourSeasonCardMgr.bIsOnMove
    local boardCards = LuaFourSeasonCardMgr.BoardCards
    local handCards = LuaFourSeasonCardMgr.HandCards
    local selfLastCard = LuaFourSeasonCardMgr.SelfLastCard
    local selfCardPileNum = LuaFourSeasonCardMgr.SelfCardPileNum
    local oppoCardNum = LuaFourSeasonCardMgr.OpponentCardNum
    local oppoShowedCards = LuaFourSeasonCardMgr.OpponentShowedCards
    local oppoLastCard = LuaFourSeasonCardMgr.OpponentLastCard
    local oppoCardPileNum = LuaFourSeasonCardMgr.OppoCardPileNum
        
    self:Delay(function(self)
        Gac2Gas.Anniv2023FSC_QueryCollectedCards(isMyRound and LuaFourSeasonCardMgr.OpponentInfo.gameId or LuaFourSeasonCardMgr.SelfInfo.gameId)
        LuaFourSeasonCardMgr:SetEnableOperation(false)
        
        self:InitRound(true, isMyRound)
        self:InitBoardCards(boardCards)
        self:InitSelfCards(handCards, selfLastCard, selfCardPileNum)
        self:InitOppoCards(oppoCardNum, oppoShowedCards, oppoLastCard, oppoCardPileNum)
    
        if not CommonDefs.IsNull(self.m_RoundHint) then
            self.m_RoundHint.gameObject:SetActive(true)
            local curAnim = isMyRound and "fscroundhint_wofang" or "fscroundhint_duifang"
            self.m_RoundHint:Play(curAnim)
        
            UnRegisterTick(self.m_RoundHintTick)
            self.m_RoundHintTick = RegisterTickOnce(function()
                LuaFourSeasonCardMgr:SetEnableOperation(true)
            end, 1000)--self.m_RoundHint[curAnim].length * 1000)
            
            return "StartRound", self.m_RoundHint[curAnim].length * 2 / 3
        end
    end)
end

function LuaFSCInGameWnd:UpdateScore()
    local scoreLabel1 = self.m_SelfPanel:Find("ScoreLabel"):GetComponent(typeof(UILabel))
    local scoreLabel2 = self.m_OppoPanel:Find("ScoreLabel"):GetComponent(typeof(UILabel))
    local score1 = LuaFourSeasonCardMgr.SelfInfo.score
    local score2 = LuaFourSeasonCardMgr.OpponentInfo.score
    local addVal1 = score1 - tonumber(scoreLabel1.text)
    local addVal2 = score2 - tonumber(scoreLabel2.text)

    --self:OnDelayComplete(LuaFourSeasonCardMgr.bIsOnMove and "OnSelfUseCard" or "OnOpponentUseCard", function()
        scoreLabel1.text = score1
        scoreLabel2.text = score2
        
        if addVal1 > 0 then
            local addLabel1 = scoreLabel1.transform:Find("Add"):GetComponent(typeof(UILabel))
            addLabel1.gameObject:SetActive(true)
            addLabel1.text = "+"..addVal1..LocalString.GetString("分")
            local anim = addLabel1:GetComponent(typeof(Animation))
            if not CommonDefs.IsNull(anim) then
                anim:Play("fscingamewnd_add")
            end
        end
    
        if addVal2 > 0 then
            local addLabel2 = scoreLabel2.transform:Find("Add"):GetComponent(typeof(UILabel))
            addLabel2.gameObject:SetActive(true)
            addLabel2.text = "+"..addVal2..LocalString.GetString("分")
            local anim = addLabel2:GetComponent(typeof(Animation))
            if not CommonDefs.IsNull(anim) then
                anim:Play("fscingamewnd_add_1")
            end
        end
    --end)
end

function LuaFSCInGameWnd:OnDealCards()
    LuaFourSeasonCardMgr:SetEnableOperation(false)
    self.m_Self:Find("CardPile").gameObject:SetActive(false)
    self.m_Oppo:Find("CardPile").gameObject:SetActive(false)
    
    self:Delay(function(self)
        self:CollectCards()
    
        -- 先手A, 后手B, 公共P, 前四张A B A B, 后续A P B A P B...共28张
        local idxA, idxB, idxP = 9, 9, 9
        local gofirst = LuaFourSeasonCardMgr.SelfInfo.gameId == 1
        local A = gofirst and self.m_SelfHandCards or self.m_OppoHandCards
        local B = gofirst and self.m_OppoHandCards or self.m_SelfHandCards
        local P = self.m_PublicCards
        local posA = gofirst and self.m_InitialPos[1] or self.m_InitialPos[2]
        local posB = gofirst and self.m_InitialPos[2] or self.m_InitialPos[1]
        local posP = self.m_InitialPos[3]
        local cardsA = gofirst and LuaFourSeasonCardMgr.HandCards or nil
        local cardsB = not gofirst and LuaFourSeasonCardMgr.HandCards or nil
        local cardsP = LuaFourSeasonCardMgr.BoardCards
        local delay = 0.5
    
        local preTween
        for i = 1, 28 do
            local pos, side, card, cardId
            
            if i <= 4 then
                side = (i % 2 == 1) and 1 or 2
                pos = (side == 1) and posA[idxA + 1] or posB[idxB + 1]
                card = (side == 1) and A:GetChild(idxA) or B:GetChild(idxB)
                cardId = (side == 1) and (cardsA and cardsA[idxA + 1] or 0) or (cardsB and cardsB[idxB + 1] or 0)
            else
                side = (i % 3 == 2) and 1 or ((i % 3 == 1) and 2 or 3)
                pos = (side == 1) and posA[idxA + 1] or ((side == 2) and posB[idxB + 1] or posP[idxP + 1])
                card = (side == 1) and A:GetChild(idxA) or ((side == 2) and B:GetChild(idxB) or P:GetChild(idxP))
                cardId = (side == 1) and (cardsA and cardsA[idxA + 1] or 0) or ((side == 2) and (cardsB and cardsB[idxB + 1] or 0) or cardsP[idxP + 1 - 10 + #cardsP])
            end
    
            card.gameObject:SetActive(true)
            --card:GetComponent(typeof(UIPanel)).depth = self.m_Card2Depth[card]
    
            local script = card:GetComponent(typeof(CCommonLuaScript))
            script:Awake()
            script = script.m_LuaSelf
            --if cardId == 0 or side < 3 then
            script:Init(0, false, EnumFSC_CardStatus.Back)
            --elseif side < 3 then
                --local isRare = LuaFourSeasonCardMgr.SelfSpecialCards[cardId] and true or false
                --script:Init(cardId, isRare)
            --else
            --    script:Init(cardId)
            --end
            --if i == 1 then script:SetTop() end
            
            local tween = 
                LuaTweenUtils.SetDelay(
                    LuaTweenUtils.SetEase(
                        LuaTweenUtils.TweenPosition(card, pos.x, pos.y, pos.z, self.m_DealDuration), 
                    self.m_MoveEase), 
                delay)
            table.insert(self.m_Tweeners, tween)
    
            if side == 3 then
                table.insert(self.m_Tweeners, 
                    LuaTweenUtils.SetDelay(
                        LuaTweenUtils.SetEase(
                            LuaTweenUtils.TweenRotationZ(card, self.m_InitialRotZ, self.m_DealDuration), 
                        self.m_RotEase), 
                    delay)
                )
            end

            LuaTweenUtils.OnStart(tween, function(...)
                SoundManager.Inst:PlayOneShot(LuaFourSeasonCardMgr.Music.deal, Vector3.zero, nil, 0)
            end)
    
            LuaTweenUtils.OnComplete(tween, function(...)
                if script then 
                    --script:SetTop(false)
                    if cardId > 0 then
                        script:Init(cardId)
                    end
                end
                if i == 28 then 
                    LuaFourSeasonCardMgr:TryTriggerGuide()
                end
            end)
            --if preTween then
            --    LuaTweenUtils.OnStart(preTween, function(...)
            --        if script then 
            --            script:SetTop(true)
            --        end
            --    end)
            --end
    
            idxA = (side == 1) and idxA - 1 or idxA
            idxB = (side == 2) and idxB - 1 or idxB
            idxP = (side == 3) and idxP - 1 or idxP
            delay = delay + self.m_DealDuration
    
            preTween = tween
        end
    
        self.m_StartDealCards = true
        LuaFourSeasonCardMgr.IsDealCards = false

        return "DealCards", 28 * self.m_DealDuration + 1
    end)
end

function LuaFSCInGameWnd:OnAddBoardCard(cardId)
    local boardCards = LuaFourSeasonCardMgr.BoardCards
    local handCards = LuaFourSeasonCardMgr.HandCards
    local selfLastCard = LuaFourSeasonCardMgr.SelfLastCard
    local selfCardPileNum = LuaFourSeasonCardMgr.SelfCardPileNum
    local oppoCardNum = LuaFourSeasonCardMgr.OpponentCardNum
    local oppoShowedCards = LuaFourSeasonCardMgr.OpponentShowedCards
    local oppoLastCard = LuaFourSeasonCardMgr.OpponentLastCard
    local oppoCardPileNum = LuaFourSeasonCardMgr.OppoCardPileNum
    self:Delay(function(self)
        LuaFourSeasonCardMgr:SetEnableOperation(false)
        
        self:InitBoardCards(boardCards, true)
        self:InitSelfCards(handCards, selfLastCard, selfCardPileNum)
        self:InitOppoCards(oppoCardNum, oppoShowedCards, oppoLastCard, oppoCardPileNum)

        --local newCard = self:FindEmpty(3)
        local newCard = self.m_PublicCards:GetChild(9)
        if newCard then
            local targetRotZ = newCard.localEulerAngles.z
            local targetPos = newCard.localPosition
            newCard.localEulerAngles = Vector3.zero
            newCard.localPosition = self.m_DealPilePos[3]
            newCard.gameObject:SetActive(true)
            local script = newCard:GetComponent(typeof(CCommonLuaScript))
            script:Awake()
            script = script.m_LuaSelf
            script:Init(cardId)
            script:SetTop(true)
            script.m_Id = 0 -- 临时卡牌的处理，避免FindCard出错
            table.insert(self.m_Tweeners, LuaTweenUtils.TweenPosition(newCard, targetPos.x, targetPos.y, targetPos.z, self.m_DealDuration * 2))
            table.insert(self.m_Tweeners, LuaTweenUtils.TweenRotationZ(newCard, targetRotZ, self.m_DealDuration * 2))
            LuaTweenUtils.OnComplete(self.m_Tweeners[#self.m_Tweeners], function()
                script:SetTop(false)
            end)
            return "OnAddBoardCard", self.m_CollectDuration * 2
        end
    end)
end

function LuaFSCInGameWnd:OnSelfUseCard(usedCardId, boardCardId)
    if LuaFourSeasonCardMgr.GuidingStage then
        self:InitCardDepth() 
    end
    --Gac2Gas.Anniv2023FSC_QueryCollectedCards(LuaFourSeasonCardMgr.SelfInfo.gameId)

    local boardCards = LuaFourSeasonCardMgr.BoardCards
    local handCards = LuaFourSeasonCardMgr.HandCards
    local selfLastCard = LuaFourSeasonCardMgr.SelfLastCard
    local selfCardPileNum = LuaFourSeasonCardMgr.SelfCardPileNum
    local selfSpecialCards = LuaFourSeasonCardMgr.SelfSpecialCards
    self:Delay(function(self)
        LuaFourSeasonCardMgr:SetEnableOperation(false)
        UnRegisterTick(self.m_RoundTick[1])
        self.m_RoundTick[1] = nil

        self:InitBoardCards(boardCards)
        self:InitSelfCards(handCards, selfLastCard, selfCardPileNum)

        local usedCard = self:FindCard(usedCardId, 1)
        local boardCard = self:FindCard(boardCardId, 3)
        if usedCard and boardCard then
            boardCard:SetTop(true)
            table.insert(self.m_Tweeners, LuaTweenUtils.TweenPosition(usedCard.transform, self.m_SelfPilePos[1].x, self.m_SelfPilePos[1].y, self.m_SelfPilePos[1].z, self.m_CollectDuration))
            table.insert(self.m_Tweeners, LuaTweenUtils.TweenPosition(boardCard.transform, self.m_SelfPilePos[3].x, self.m_SelfPilePos[3].y, self.m_SelfPilePos[3].z, self.m_CollectDuration))
            table.insert(self.m_Tweeners, LuaTweenUtils.TweenRotationZ(boardCard.transform, 0, self.m_CollectDuration))
            LuaTweenUtils.OnComplete(self.m_Tweeners[#self.m_Tweeners], function()
                boardCard:SetTop(false)
                local isRare = selfSpecialCards[boardCardId] and true or false
                if isRare then
                    SoundManager.Inst:PlayOneShot(LuaFourSeasonCardMgr.Music.epic, Vector3.zero, nil, 0)
                    local script = boardCard.transform:GetComponent(typeof(CCommonLuaScript))
                    script:Awake()
                    script = script.m_LuaSelf
                    script:LevelUp()
                    script.m_IsRare = true
                    script:SetBg()
                end
                self:InitSelfCards()
            end)

            SoundManager.Inst:PlayOneShot(LuaFourSeasonCardMgr.Music.collect, Vector3.zero, nil, 0)
            return "OnSelfUseCard", self.m_CollectDuration
        end
    end)
end

function LuaFSCInGameWnd:OnOpponentUseCard(usedCardId, boardCardId)
    --Gac2Gas.Anniv2023FSC_QueryCollectedCards(LuaFourSeasonCardMgr.OpponentInfo.gameId)
    
    local boardCards = LuaFourSeasonCardMgr.BoardCards
    local oppoCardNum = LuaFourSeasonCardMgr.OpponentCardNum
    local oppoShowedCards = LuaFourSeasonCardMgr.OpponentShowedCards
    local oppoLastCard = LuaFourSeasonCardMgr.OpponentLastCard
    local oppoCardPileNum = LuaFourSeasonCardMgr.OppoCardPileNum
    self:Delay(function(self)
        LuaFourSeasonCardMgr:SetEnableOperation(false)
        UnRegisterTick(self.m_RoundTick[2])
        self.m_RoundTick[2] = nil

        self:InitBoardCards(boardCards)
        self:InitOppoCards(oppoCardNum, oppoShowedCards, oppoLastCard, oppoCardPileNum)
        
        local usedCard = self:FindCard(usedCardId, 2) or self:FindBack()
        local boardCard = self:FindCard(boardCardId, 3) 
        if usedCard and boardCard then
            usedCard:Init(usedCardId)
            usedCard:SetTop(true) -- 对方牌堆的位置在手牌层级更高的一侧，SetTop避免被途经的手牌压在下面
            LuaFourSeasonCardMgr.MaxCardDepth = LuaFourSeasonCardMgr.MaxCardDepth + 1
            boardCard:SetTop(true) -- InitRound的时候会重置
            LuaFourSeasonCardMgr.MaxCardDepth = LuaFourSeasonCardMgr.MaxCardDepth - 1
            
            table.insert(self.m_Tweeners, LuaTweenUtils.TweenPosition(usedCard.transform, self.m_OppoPilePos[2].x, self.m_OppoPilePos[2].y, self.m_OppoPilePos[2].z, self.m_CollectDuration))
            table.insert(self.m_Tweeners, LuaTweenUtils.TweenPosition(boardCard.transform, self.m_OppoPilePos[3].x, self.m_OppoPilePos[3].y, self.m_OppoPilePos[3].z, self.m_CollectDuration))
            table.insert(self.m_Tweeners, LuaTweenUtils.TweenRotationZ(boardCard.transform, 0, self.m_CollectDuration))
            LuaTweenUtils.OnComplete(self.m_Tweeners[#self.m_Tweeners], function()
                usedCard:SetTop(false)
                boardCard:SetTop(false)
                self:InitOppoCards()
            end)

            SoundManager.Inst:PlayOneShot(LuaFourSeasonCardMgr.Music.collect, Vector3.zero, nil, 0)
            return "OnOpponentUseCard", self.m_CollectDuration
        end
    end)
end

-- 洗牌发牌的客户端表现
function LuaFSCInGameWnd:DoReShuffle(cards, callback)
    self.m_DiscardHint:SetActive(false)

    local delay = self.m_ReshuffleInterval
    local st = 10 - #cards + 1
    for i = 10, st, -1 do
        local card = self.m_PublicCards:GetChild(i - 1)
        local cardId = cards[i - st + 1]
        local pos = self.m_InitialPos[3][i]
        local tween =
            LuaTweenUtils.SetDelay(
                LuaTweenUtils.SetEase(
                    LuaTweenUtils.TweenPosition(card, pos.x, pos.y, pos.z, self.m_ReshuffleInterval), 
                self.m_MoveEase), 
            delay)
        table.insert(self.m_Tweeners, tween)

        table.insert(self.m_Tweeners, 
            LuaTweenUtils.SetDelay(
                LuaTweenUtils.SetEase(
                    LuaTweenUtils.TweenRotationZ(card, self.m_InitialRotZ, self.m_ReshuffleInterval), 
                self.m_RotEase), 
            delay))

        LuaTweenUtils.OnStart(tween, function(...)
            SoundManager.Inst:PlayOneShot(LuaFourSeasonCardMgr.Music.deal, Vector3.zero, nil, 0)
        end)

        LuaTweenUtils.OnComplete(tween, function(...)
            local script = card:GetComponent(typeof(CCommonLuaScript))
            if script then 
                script:Awake()
                script = script.m_LuaSelf
                --script:SetTop(false)
                if cardId > 0 then
                    script:Init(cardId)
                end
            end
            if i == 10 and callback then
                callback()
            end
        end)

        delay = delay + self.m_ReshuffleInterval
    end
end

function LuaFSCInGameWnd:ReplaceCard(side, giveUpCardId, newHandCardId, afterBoardCards)
    local giveUpCard, idx = self:FindCard(giveUpCardId, side) 
    if not giveUpCard and side == 2 then giveUpCard, idx = self:FindBack() end

    if giveUpCard then
        local oriPos = giveUpCard.transform.localPosition
        if self.m_ReShuffle then --换牌时触发洗牌的情况下，换到的牌一定不会出现在洗完的公共牌里
            LuaFourSeasonCardMgr:SetEnableOperation(false)
            local targetPos = self.m_DealPilePos[side]
            table.insert(self.m_Tweeners, LuaTweenUtils.TweenPosition(giveUpCard.transform, targetPos.x, targetPos.y, targetPos.z, self.m_ReshuffleInterval))
            LuaTweenUtils.OnStart(self.m_Tweeners[#self.m_Tweeners], function(...)
                giveUpCard:Init(0, false, EnumFSC_CardStatus.Back)
            end)
            for i = 0, 9 do
                targetPos = self.m_DealPilePos[3]
                local card = self.m_PublicCards:GetChild(i)
                table.insert(self.m_Tweeners, LuaTweenUtils.TweenPosition(card, targetPos.x, targetPos.y, targetPos.z, self.m_ReshuffleInterval))
                table.insert(self.m_Tweeners, LuaTweenUtils.TweenRotationZ(card, 0, self.m_ReshuffleInterval))
                LuaTweenUtils.OnStart(self.m_Tweeners[#self.m_Tweeners], function(...)
                    local script = card:GetComponent(typeof(CCommonLuaScript))
                    if script then 
                        script:Awake()
                        script.m_LuaSelf:Init(0, false, EnumFSC_CardStatus.Back)
                    end
                end)
            end
            SoundManager.Inst:PlayOneShot(LuaFourSeasonCardMgr.Music.collect, Vector3.zero, nil, 0)
            LuaTweenUtils.OnComplete(self.m_Tweeners[#self.m_Tweeners], function(...)
                self:DoReShuffle(afterBoardCards, function()
                    giveUpCard.transform.localEulerAngles = Vector3.zero
                    giveUpCard.transform.localPosition = self.m_DealPilePos[side]
                    if side == 1 then 
                        giveUpCard:Init(newHandCardId)
                    elseif side == 2 then
                        giveUpCard:Init(0, false, EnumFSC_CardStatus.Back)
                    end
                    table.insert(self.m_Tweeners, LuaTweenUtils.TweenPosition(giveUpCard.transform, oriPos.x, oriPos.y, oriPos.z, self.m_CollectDuration))
    
                    g_ScriptEvent:BroadcastInLua("FSC_SelectCard", 0)
                    LuaFourSeasonCardMgr:SetEnableOperation(true)
                end)
            end)
        else
            local publicLoc = self:FindEmpty(3)
            if publicLoc then
                LuaFourSeasonCardMgr:SetEnableOperation(false)
    
                local targetPos = giveUpCard.transform.parent:InverseTransformPoint(publicLoc.position)
                table.insert(self.m_Tweeners, LuaTweenUtils.TweenPosition(giveUpCard.transform, targetPos.x, targetPos.y, targetPos.z, self.m_CollectDuration))
                table.insert(self.m_Tweeners, LuaTweenUtils.TweenRotationZ(giveUpCard.transform, self.m_InitialRotZ, self.m_CollectDuration))
                LuaTweenUtils.OnComplete(self.m_Tweeners[#self.m_Tweeners], function()
                    giveUpCard.transform.localEulerAngles = Vector3.zero
                    giveUpCard.transform.localPosition = self.m_DealPilePos[side]
                    if side == 1 then 
                        giveUpCard:Init(newHandCardId)
                    elseif side == 2 then
                        giveUpCard:Init(0, false, EnumFSC_CardStatus.Back)
                    end
                    table.insert(self.m_Tweeners, LuaTweenUtils.TweenPosition(giveUpCard.transform, oriPos.x, oriPos.y, oriPos.z, self.m_CollectDuration))
    
                    publicLoc.gameObject:SetActive(true)
                    local script = publicLoc:GetComponent(typeof(CCommonLuaScript))
                    script:Awake()
                    script.m_LuaSelf:Init(giveUpCardId)
    
                    g_ScriptEvent:BroadcastInLua("FSC_SelectCard", 0)
                    LuaFourSeasonCardMgr:SetEnableOperation(true)
                end)
            end
        end
    end
end

--[obsolete]
function LuaFSCInGameWnd:OnReShuffleBoardCard()
    --self.m_ReShuffle = true
    --self:InitBoardCards()
end

function LuaFSCInGameWnd:OnSelfReplaceCard(giveUpCardId, newHandCardId, isReShuffle, boardCards1, boardCards2)
    --local boardCards = LuaFourSeasonCardMgr.BoardCards
    local handCards = LuaFourSeasonCardMgr.HandCards
    self:Delay(function(self)
        self.m_ReShuffle = isReShuffle
        self:InitBoardCards(boardCards1)
        self:InitSelfCards(handCards)
        self:ReplaceCard(1, giveUpCardId, newHandCardId, boardCards2)
        local duration = self.m_ReShuffle and self.m_ReshuffleInterval * 11 or self.m_CollectDuration * 2
        self.m_ReShuffle = false
        return "OnSelfReplaceCard", duration
    end) 
end

function LuaFSCInGameWnd:OnOpponentReplaceCard(giveUpCardId, opponentCardNum, isReShuffle, boardCards1, boardCards2)
    --local boardCards = LuaFourSeasonCardMgr.BoardCards
    local showedCards = LuaFourSeasonCardMgr.OpponentShowedCards
    self:Delay(function(self)
        self.m_ReShuffle = isReShuffle
        self:InitBoardCards(boardCards1)
        self:InitOppoCards(opponentCardNum, showedCards)
        self:ReplaceCard(2, giveUpCardId, nil, boardCards2)
        local duration = self.m_ReShuffle and self.m_ReshuffleInterval * 11 or self.m_CollectDuration * 2
        self.m_ReShuffle = false
        return "OnOpponentReplaceCard", duration
    end)
end

function LuaFSCInGameWnd:StartAddNewCombo(newCards, comboId, score, countDownEndTime)
    local selfSpecialCards = LuaFourSeasonCardMgr.SelfSpecialCards
    self:Delay(function(self)
        LuaFourSeasonCardMgr:SetEnableOperation(false)
        
        local data = ZhouNianQing2023_CardCombination.GetData(comboId)
        self.m_Combination:SetActive(true)
        self.m_Combination.transform:Find("Name"):GetComponent(typeof(UILabel)).text = data.Name
        self.m_Combination.transform:Find("Score"):GetComponent(typeof(UILabel)).text = "+"..score

        local cards = data.CardId
        local center = (cards.Length + 1) / 2
        local offset = self.m_CardOffset * 0.5555556
        local needWidth = (cards.Length - 1) * offset
        if needWidth > self.m_CombinationMaxWidth then
            offset = self.m_CombinationMaxWidth  / (cards.Length - 1)
        end

        local combinationCards = self.m_Combination.transform:Find("Cards")
        for i = 1, cards.Length do
            local card = combinationCards:GetChild(i - 1)
            LuaUtils.SetLocalPositionX(card, self.m_CombCardsCenterX + (i - center) * offset)
            card.gameObject:SetActive(true)
            local isRare = selfSpecialCards[cards[i - 1]] and true or false
            local script = card:GetComponent(typeof(CCommonLuaScript))
            script:Awake()
            script.m_LuaSelf:Init(cards[i - 1], isRare)
            --script.m_LuaSelf:Highlight(cards[i - 1] == newCards[1] or cards[i - 1] == newCards[2]) -- 可能一次收回的两张形成新组合
            script.transform:Find("Card/Name"):GetComponent(typeof(UILabel)).color = Color.white
        end
        for i = cards.Length + 1, 6 do
            local card = combinationCards:GetChild(i - 1)
            card.gameObject:SetActive(false)
        end

        local anim = self.m_Combination:GetComponent(typeof(Animation))
        if not CommonDefs.IsNull(anim) then
            anim:Stop()
            anim:Play("fsccombination_in")
            if LuaFourSeasonCardMgr.GuidingStage == 3 then
                LuaFourSeasonCardMgr:TryTriggerGuide({anim})
            end
        end

        if LuaFourSeasonCardMgr.GuidingStage == 5 and LuaFourSeasonCardMgr.WaitForNxtGuide then
            g_ScriptEvent:BroadcastInLua("FSC_InitCardDepth")
            CGuideMgr.Inst:StartGuide(EnumGuideKey.FSC_Guide7, 0)
            LuaFourSeasonCardMgr.WaitForNxtGuide = false
            LuaFourSeasonCardMgr.GuidingStage = nil
        end

        return "StartAddNewCombo", ZhouNianQing2023_Setting.GetData().ShowNewCombinationDuration
    end)
end

function LuaFSCInGameWnd:ShowCardTopDetail(show, cardId, isRare)
    self.m_CardTopDetail.gameObject:SetActive(true)
    if show then
        local script = self.m_CardTopDetail.transform:Find("FSCCard"):GetComponent(typeof(CCommonLuaScript))
        script:Awake()
        script = script.m_LuaSelf
        if script.m_Id == cardId and self.m_IsShowCardDetail then -- 正在显示的相同卡牌详情不用刷新
            return
        end
        script:Init(cardId, isRare, EnumFSC_CardStatus.Detail)

        local template = self.m_CardTopDetail.transform:Find("Compose/Template").gameObject
        template:SetActive(false)
        local grid = self.m_CardTopDetail.transform:Find("Compose/Grid"):GetComponent(typeof(UIGrid))
        Extensions.RemoveAllChildren(grid.transform)
        local cards, combinations = LuaFourSeasonCardMgr:GetCardCombination(cardId)
        if cards then --wyh
            for cardId, _ in pairs(cards) do
                local obj = NGUITools.AddChild(grid.gameObject, template)
                obj:SetActive(true)
                obj:GetComponent(typeof(UILabel)).text = LuaFourSeasonCardMgr:GetCardData(cardId).Title
                if not LuaFourSeasonCardMgr.CollectedCards[LuaFourSeasonCardMgr.SelfInfo.gameId][cardId] then
                    CUICommonDef.SetActive(obj, false)
                end
            end
        end
        grid:Reposition()

        self.m_IsShowCardDetail = true
        if not CommonDefs.IsNull(self.m_CardTopDetail) then
            self.m_CardTopDetail:Play("fsccardtopdetail_in")
        end
    else
        self.m_IsShowCardDetail = false
        if not CommonDefs.IsNull(self.m_CardTopDetail) then
            self.m_CardTopDetail:Play("fsccardtopdetail_out")
        end
    end
end

function LuaFSCInGameWnd:SelectCard(cardId)
    self.m_SelectedCard = cardId

    if cardId > 0 and not LuaFourSeasonCardMgr:CheckPlayCard() then -- 弃牌
        self.m_DiscardHint:SetActive(true)
        --self.m_DealPile.transform:GetChild(0).gameObject:SetActive(true)
    else
        self.m_DiscardHint:SetActive(false)
        --self.m_DealPile.transform:GetChild(0).gameObject:SetActive(false)
    end
end

function LuaFSCInGameWnd:ChooseBoardCard(cardId)
    if self.m_SelectedCard > 0 and not self.m_DiscardHint.activeSelf then
        if LuaFourSeasonCardMgr:GetCardData(cardId).Season == LuaFourSeasonCardMgr:GetCardData(self.m_SelectedCard).Season then
            Gac2Gas.Anniv2023FSC_RequestPlayCard(self.m_SelectedCard, cardId)
        else
            print("Not Match")
        end
    end
end

function LuaFSCInGameWnd:TurnOverCards(index, spCardId, effectId, cards)
    if index == LuaFourSeasonCardMgr.SelfInfo.gameId then
        local showedNum = 0
        for _, __ in pairs(LuaFourSeasonCardMgr.OpponentShowedCards) do
            showedNum = showedNum + 1
        end
        for i = 1, math.min(#cards, 10) do
            local script = self.m_OppoHandCards:GetChild(showedNum + i - 1):GetComponent(typeof(CCommonLuaScript))
            script:Awake()
            script.m_LuaSelf:Init(cards[i])
        end
        for i = 1, #cards do
            LuaFourSeasonCardMgr.OpponentShowedCards[cards[i]] = true
        end
        self:SendSkillMsg(true, effectId, LuaFourSeasonCardMgr:GetEnemyName(), #cards)
    else
        if not LuaFourSeasonCardMgr.OpponentInfo.isPlayer then
            self:SendSkillMsg(false, effectId, LuaFourSeasonCardMgr.SelfInfo.name, #cards)
        end
    end
end

function LuaFSCInGameWnd:BanCards(index, spCardId, effectId, cards)
    if index == LuaFourSeasonCardMgr.SelfInfo.gameId then
        self:SendSkillMsg(true, effectId, LuaFourSeasonCardMgr:GetEnemyName(), #cards > 0 and ZhouNianQing2023_Cards.GetData(cards[1]).SpecialTitle or nil)
    else
        --for i = 1, #cards do
        --    LuaFourSeasonCardMgr.SelfSpecialCards[cards[i]][2] = EnumFSC_SpCardStatus.Interrupted
        --end
        if not LuaFourSeasonCardMgr.OpponentInfo.isPlayer then
            self:SendSkillMsg(false, effectId, LuaFourSeasonCardMgr.SelfInfo.name, #cards > 0 and ZhouNianQing2023_Cards.GetData(cards[1]).SpecialTitle or nil)
        end
    end    
end

function LuaFSCInGameWnd:CopyCards(index, spCardId, effectId, cards)
    if index == LuaFourSeasonCardMgr.SelfInfo.gameId then
        --for i = 1, #cards do
        --    LuaFourSeasonCardMgr.SelfSpecialCards[cards[i]][1] = EnumFSC_SpCardSrcType.Replicate
        --end
        self:SendSkillMsg(true, effectId, LuaFourSeasonCardMgr:GetEnemyName(), #cards > 0 and ZhouNianQing2023_Cards.GetData(cards[1]).SpecialTitle or nil)
    else
        if not LuaFourSeasonCardMgr.OpponentInfo.isPlayer then
            self:SendSkillMsg(false, effectId, LuaFourSeasonCardMgr.SelfInfo.name, #cards > 0 and ZhouNianQing2023_Cards.GetData(cards[1]).SpecialTitle or nil)
        end   
    end
end

function LuaFSCInGameWnd:SpCardEffectEnd(index, spCardId, effectId)
    if index == LuaFourSeasonCardMgr.SelfInfo.gameId then
        self:SendSkillMsg(true, effectId)
    else
        if not LuaFourSeasonCardMgr.OpponentInfo.isPlayer then
            self:SendSkillMsg(false, effectId)
        end
    end
end

function LuaFSCInGameWnd:OpponentStartSkill(spCardId, effectId, countDownEndTime)
    --self:SendMsg(LocalString.GetString("发动珍稀牌技能，选牌中..."))
end

function LuaFSCInGameWnd:StartRareInEffect(spCardId, effectId)
    LuaFourSeasonCardMgr:SetEnableOperation(false)
    self.m_RareInEffect:SetActive(true)
    SoundManager.Inst:PlayOneShot(LuaFourSeasonCardMgr.Music.showup, Vector3.zero, nil, 0)
    local anim = self.m_RareInEffect:GetComponent(typeof(Animation))
    if not CommonDefs.IsNull(anim) then
        anim:Play("fscrareineffect_show")
    end
    local page1 = self.m_RareInEffect.transform:Find("Page1")
    local page2 = self.m_RareInEffect.transform:Find("Page2")
    page1.gameObject:SetActive(true)
    page2.gameObject:SetActive(false)

    local skillData = ZhouNianQing2023_PreciousSkill.GetData(effectId)
    page1:Find("Judge"):GetComponent(typeof(UILabel)).text = "[F6E6FF]"..skillData.SkillDescription
    local script = page1:Find("kapaikgd/FSCCard"):GetComponent(typeof(CCommonLuaScript))
    script:Awake()
    script.m_LuaSelf:Init(spCardId, true)
end

function LuaFSCInGameWnd:CloseRareInEffect(countDownEndTime)
    local duration = math.ceil(countDownEndTime - CServerTimeMgr.Inst.timeStamp)
    UnRegisterTick(self.m_SpCardTick)
    if duration > 0 then
        self.m_SpCardTick = RegisterTickOnce(function()
            local anim = self.m_RareInEffect:GetComponent(typeof(Animation))
            if not CommonDefs.IsNull(anim) then
                anim:Play("fscrareineffect_close")
            end
            LuaFourSeasonCardMgr:SetEnableOperation(true)
            UnRegisterTick(self.m_SpCardTick)
        end, duration * 1000)
    else
        local anim = self.m_RareInEffect:GetComponent(typeof(Animation))
        if not CommonDefs.IsNull(anim) then
            anim:Play("fscrareineffect_close")
        end
        LuaFourSeasonCardMgr:SetEnableOperation(true)
        UnRegisterTick(self.m_SpCardTick)
    end
end

function LuaFSCInGameWnd:StartSpCardEffect(cardId, effectId, countDownEndTime)
    self:ClearDelay()

    self:Delay(function(self)
        g_ScriptEvent:BroadcastInLua("FSC_StartRareInEffect", cardId, effectId)
        g_ScriptEvent:BroadcastInLua("FSC_CloseRareInEffect", countDownEndTime)
        return "StartSpCardEffect", math.ceil(countDownEndTime - CServerTimeMgr.Inst.timeStamp)
    end)
end

function LuaFSCInGameWnd:SwitchRareInEffect(isReplicate, card2Effects)
    local page1 = self.m_RareInEffect.transform:Find("Page1")
    local page2 = self.m_RareInEffect.transform:Find("Page2")
    page1.gameObject:SetActive(false)
    page2.gameObject:SetActive(true)

    -- 4禁用 5复制
    page2:Find("Hint"):GetComponent(typeof(UILabel)).text = isReplicate and LocalString.GetString("选择一张对方已生效的珍稀牌 复制其效果") or LocalString.GetString("选择一张对方已生效的珍稀牌 将其禁用")
    local grid = page2:Find("ScrollView/Grid"):GetComponent(typeof(UIGrid))
    Extensions.RemoveAllChildren(grid.transform)
    local template = page2:Find("ScrollView/Template").gameObject
    template:SetActive(false)

    if not next(card2Effects) then
        local anim = self.m_RareInEffect:GetComponent(typeof(Animation))
        if not CommonDefs.IsNull(anim) then
            anim:Play("fscrareineffect_qiehuanhouxiaoshi")
        end
        return
    end

    for cardId, skills in pairs(card2Effects) do 
        local go = NGUITools.AddChild(grid.gameObject, template)
        go:SetActive(true)
        local script = go.transform:Find("FSCCardHeadshot"):GetComponent(typeof(CCommonLuaScript))
        script:Awake()
        script.m_LuaSelf:Init(cardId, true) --wyh
        go.transform:Find("Banned").gameObject:SetActive(false)
        go.transform:Find("Name"):GetComponent(typeof(UILabel)).text = LuaFourSeasonCardMgr:GetCardData(cardId).SpecialTitle
        local skillGrid = go.transform:Find("Grid"):GetComponent(typeof(UIGrid))
        local skillTemplate = go.transform:Find("Template").gameObject
        skillTemplate:SetActive(false)
        for _, skillId in ipairs(skills) do
            local skill = NGUITools.AddChild(skillGrid.gameObject, skillTemplate)
            skill:SetActive(true)
            skill:GetComponent(typeof(UILabel)).text = ZhouNianQing2023_PreciousSkill.GetData(skillId).SkillDescription
        end
        skillGrid:Reposition()
        local btn = go.transform:Find("Btn")
        btn:Find("Label"):GetComponent(typeof(UILabel)).text = isReplicate and LocalString.GetString("复制") or LocalString.GetString("禁用")
        UIEventListener.Get(btn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            if isReplicate then
                self:PlayCopyCardFx(script.transform)
                Gac2Gas.Anniv2023FSC_RequestReplicate(cardId)
            else
                script.transform:Find("BG/Banned").gameObject:SetActive(true)
                local anim = script:GetComponent(typeof(Animation))
                if not CommonDefs.IsNull(anim) then
                    anim:Play("fsccardheadshot_jinyong")
                end
                Gac2Gas.Anniv2023FSC_RequestInterrupt(cardId)
            end
            UnRegisterTick(self.m_SpCardTick)
            LuaFourSeasonCardMgr:SetEnableOperation(true)
            self.m_SpCardTick = RegisterTickOnce(function()
                local anim = self.m_RareInEffect:GetComponent(typeof(Animation))
                if not CommonDefs.IsNull(anim) then
                    anim:Play("fscrareineffect_close")
                end
                self:NxtDelay()
            end, 666)
        end)
    end
    grid:Reposition()

    local anim = self.m_RareInEffect:GetComponent(typeof(Animation))
    if not CommonDefs.IsNull(anim) then
        anim:Play("fscrareineffect_qiehuan")
    end
end

function LuaFSCInGameWnd:StartReplicate(spCardId, effectId, card2Effects, countDownEndTime)
    self:ClearDelay()

    self:Delay(function(self)
        g_ScriptEvent:BroadcastInLua("FSC_StartRareInEffect", spCardId, effectId)
        UnRegisterTick(self.m_SpCardTick)
        self.m_SpCardTick = RegisterTickOnce(function()
            self:SwitchRareInEffect(true, card2Effects)
           g_ScriptEvent:BroadcastInLua("FSC_CloseRareInEffect", countDownEndTime)
        end, ZhouNianQing2023_Setting.GetData().ShowSkillDuration * 1000)

        return "StartReplicate", math.ceil(countDownEndTime - CServerTimeMgr.Inst.timeStamp) + 1
    end)
end

function LuaFSCInGameWnd:StartInterrupt(spCardId, effectId, card2Effects, countDownEndTime)
    self:ClearDelay()

    self:Delay(function(self)
        g_ScriptEvent:BroadcastInLua("FSC_StartRareInEffect", spCardId, effectId)
        UnRegisterTick(self.m_SpCardTick)
        self.m_SpCardTick = RegisterTickOnce(function()
            self:SwitchRareInEffect(false, card2Effects)
            g_ScriptEvent:BroadcastInLua("FSC_CloseRareInEffect", countDownEndTime)
        end, ZhouNianQing2023_Setting.GetData().ShowSkillDuration * 1000)

        return "StartInterrupt", math.ceil(countDownEndTime - CServerTimeMgr.Inst.timeStamp) + 1
    end)
end

function LuaFSCInGameWnd:SendCollectedCardsData(queryIndex, collectedCards, collectedCombos)

end

function LuaFSCInGameWnd:LevelUpHandCard(idx)
    local card = self.m_SelfHandCards:GetChild(idx)
    local vfx = self.m_SelfHandCards:Find("vfx")
    local vfxfankui = self.m_SelfHandCards:Find("vfxfankui")
    vfx.gameObject:SetActive(false)
    vfxfankui.gameObject:SetActive(false)
    local rs = CommonDefs.GetComponentsInChildren_Component_Type(vfx, typeof(Renderer))
    for i = 0, rs.Length - 1 do
        rs[i].sortingOrder = 1
    end
    rs = CommonDefs.GetComponentsInChildren_Component_Type(vfxfankui, typeof(Renderer))
    for i = 0, rs.Length - 1 do
        rs[i].sortingOrder = 1
    end
    vfx.localPosition = Vector3(1531, 23, 0)
    vfxfankui.localPosition = Vector3(-1408, 0, 0)
    vfx.gameObject:SetActive(true)
    vfxfankui.gameObject:SetActive(true)

    local anim = self.m_SelfHandCards:GetComponent(typeof(Animation))
    if not CommonDefs.IsNull(anim) then
        anim:Play("fscingamewnd_zhenxika")
    end
    table.insert(self.m_Tweeners, LuaTweenUtils.TweenPosition(vfx, card.localPosition.x, card.localPosition.y, card.localPosition.z, 0.5))
    LuaTweenUtils.OnComplete(self.m_Tweeners[#self.m_Tweeners], function()
        local script = card:GetComponent(typeof(CCommonLuaScript))
        script:Awake()
        script = script.m_LuaSelf
        script:LevelUp()
        script.m_IsRare = true
        script:SetBg()
    end)
end

function LuaFSCInGameWnd:PlayCopyCardFx(card)
    local vfx = self.m_SelfHandCards:Find("vfx")
    local vfxfankui = self.m_SelfHandCards:Find("vfxfankui")
    vfx.gameObject:SetActive(false)
    vfxfankui.gameObject:SetActive(false)
    local rs = CommonDefs.GetComponentsInChildren_Component_Type(vfx, typeof(Renderer))
    for i = 0, rs.Length - 1 do
        rs[i].sortingOrder = 1
    end
    rs = CommonDefs.GetComponentsInChildren_Component_Type(vfxfankui, typeof(Renderer))
    for i = 0, rs.Length - 1 do
        rs[i].sortingOrder = 1
    end
    vfx.localPosition = self.m_SelfHandCards:InverseTransformPoint(card.position)
    vfxfankui.localPosition = Vector3(-1408, 0, 0)
    vfx.gameObject:SetActive(true)
    vfxfankui.gameObject:SetActive(true)

    local anim = self.m_SelfHandCards:GetComponent(typeof(Animation))
    if not CommonDefs.IsNull(anim) then
        anim:Play("fscingamewnd_zhenxika_1")
    end
    table.insert(self.m_Tweeners, LuaTweenUtils.TweenPosition(vfx, 1531, 23, 0, 0.33))
end

-- 关于动效表现的处理
-- 发牌是开局时候的固定处理，不做延时，会直接被StartRound打断
-- 回合切换、洗牌、出牌、换牌、得分、添加公共牌、禁牌、翻牌、复制牌、普通珍稀牌生效、对手放技能, 加入延时队列
---- (换牌、洗牌时不暂停回合时间，因为回合没结束；出牌后回合结束，会暂停回合时间)
-- 珍稀牌技能生效并且选择禁用、复制的目标，打断延时队列并立即执行
-- 其它动效立即执行

function LuaFSCInGameWnd:NxtDelay()
    UnRegisterTick(self.m_CurDelay.tick)
    self.m_CurDelay.tick = nil

    -- deep copy callbacks
    local oldCallbacks = {}
    if self.m_CurDelay.callbacks then
        for _, v in ipairs(self.m_CurDelay.callbacks) do
            table.insert(oldCallbacks, v)
        end
    end
    self.m_CurDelay.isDone = true
    --if self.m_CurDelay.name and self.m_OnComplete[self.m_CurDelay.name] then
    --    self.m_OnComplete[self.m_CurDelay.name]()
    --    self.m_OnComplete[self.m_CurDelay.name] = nil
    --end
    local valid = 1
    for idx, func in ipairs(oldCallbacks) do
        if idx > valid then
            table.insert(self.m_CurDelay.callbacks, func)
        else
            if not self:DoDelay(func) then
                valid = valid + 1
            end
        end
    end
end

function LuaFSCInGameWnd:DoDelay(callback)
    if not callback then return end
    local name, duration = callback(self)
    print("DoDelay:", name, "duration="..(duration and duration or "nil"))
    if duration and duration > 0 then
        -- renew curDelay
        self.m_CurDelay = {}
        self.m_CurDelay.name = name
        self.m_CurDelay.callbacks = {}
        self.m_CurDelay.isDone = false
        self.m_CurDelay.tick = RegisterTickOnce(function()
            self:NxtDelay()
        end, duration * 1000)

        return true
    end
end

-- 只能用作纯表现，Delay过程中玩家全程无法操作
-- 珍稀牌选择禁用目标之类的表现不能延时，因为过期服务器就自动选择了
-- 打断延时队列后应该立即调用InitRound
function LuaFSCInGameWnd:Delay(func)
    if not self.m_CurDelay or self.m_CurDelay.isDone then
        self:DoDelay(func)
    else
        table.insert(self.m_CurDelay.callbacks, func)
    end
end

function LuaFSCInGameWnd:ClearDelay()
    self:ClearTweeners()
    self:InitRound()
    self:InitBoardCards()
    self:InitSelfCards()
    self:InitOppoCards()
end

function LuaFSCInGameWnd:OnDelayComplete(name, func)
    self.m_OnComplete[name] = func
end


function LuaFSCInGameWnd:GetGuideGo(methodName)
    if methodName == "GetRareCardBtn" then
        return self.m_RareBtn
    elseif methodName == "GetSelfCard1" then
        local card = self.m_SelfHandCards:GetChild(0)
        if card then 
            local panel = card:GetComponent(typeof(UIPanel))
            if panel then
                panel.sortingOrder = 1
            end
        end
        return card and card.gameObject
    elseif methodName == "GetPublicCard1" then
        local card = LuaFourSeasonCardMgr.GuidingStage == 2 and self:FindCard(33, 3) or self:FindCard(10, 3)
        if card then 
            local panel = card.transform:GetComponent(typeof(UIPanel))
            if panel then
                panel.sortingOrder = 1
            end
        end
        return card and card.gameObject
    elseif methodName == "GetSelfCardPile" then
        return self.m_SelfCardPile.gameObject
    elseif methodName == "GetOppoCardPile" then
        return self.m_OppoCardPile.gameObject
    end
    return nil
end
