local CommonDefs = import "L10.Game.CommonDefs"
local EnumGender = import "L10.Game.EnumGender"
local CFashionMgr = import "L10.Game.CFashionMgr"

CLuaShopMallMgr = {}

CLuaShopMallMgr.m_IsZhouBianMallShopOpen = true

function CLuaShopMallMgr:IsZhouBianMallShopEnabled()
	return CLuaShopMallMgr.m_IsZhouBianMallShopOpen and CommonDefs.IS_CN_CLIENT and CommonDefs.IsPlayEnabled("ZhouBianMallShop")
end

function CLuaShopMallMgr:OpenZhouBianMallShop()
	if not CLuaShopMallMgr:IsZhouBianMallShopEnabled() then
		g_MessageMgr:ShowMessage("FUNCTION_NOT_OPEN")
		return
	end

	if CommonDefs.DEVICE_PLATFORM == "pc" then
		g_MessageMgr:ShowMessage("ZHOUBIAN_MALLSHOP_PC_NOT_SUPPORT")
		return
	end

	Gac2Gas.RequestLoginZhouBianMallShop()
end

CLuaShopMallMgr.m_PlayerShopNumInputBoxOwnedCount = 0
CLuaShopMallMgr.m_ShopMallSelectItem = nil
CLuaShopMallMgr.m_DynamicOnShelfItems = {}

function CLuaShopMallMgr:IsShowTransformableFashionIcon(templateId)
	local transformable = false
	if IsTransformableFashionOpen() then
		local fashionId = CFashionMgr.Inst:GetFashionByItem(templateId)
		local blindBoxData = Item_BlindBox.GetData(templateId)
		if blindBoxData and blindBoxData.List.Length > 0 then
			if blindBoxData.List[0].Length > 0 then
				fashionId = CFashionMgr.Inst:GetFashionByItem(blindBoxData.List[0][0])
			end
		end
		if fashionId ~= 0 then
			local designData = Fashion_Fashion.GetData(fashionId)
			if designData and CClientMainPlayer.Inst then
				if CClientMainPlayer.Inst.Gender == EnumGender.Male then
					transformable = designData.TransformMonsterMale ~= 0
				else
					transformable = designData.TransformMonsterFemale ~= 0
				end
			end
		end
	end
	return transformable
end