require("common/common_include")

local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"

CLuaHuaBiResultWnd=class()
RegistChildComponent(CLuaHuaBiResultWnd, "m_TimeLabel", UILabel)
RegistChildComponent(CLuaHuaBiResultWnd, "m_PercentLabel", UILabel)
RegistChildComponent(CLuaHuaBiResultWnd, "m_ExpLabel", UILabel)
RegistChildComponent(CLuaHuaBiResultWnd, "m_LevelLabel", UILabel)
RegistChildComponent(CLuaHuaBiResultWnd, "m_Result", CUITexture)
RegistChildComponent(CLuaHuaBiResultWnd, "m_SpecialEffect", GameObject)


function CLuaHuaBiResultWnd:Awake()
end

function CLuaHuaBiResultWnd:Init()
	self:RefreshUI()
end

function CLuaHuaBiResultWnd:OnEnable()
end

function CLuaHuaBiResultWnd:OnDisable()
end


function CLuaHuaBiResultWnd:RefreshUI()
	self.m_TimeLabel.text = SafeStringFormat(LocalString.GetString("%d分%d秒"), math.floor(CLuaHuaBiResultWnd.TimeSpend/60), CLuaHuaBiResultWnd.TimeSpend%60)
	self.m_PercentLabel.text = SafeStringFormat(LocalString.GetString("%.1f%%"), CLuaHuaBiResultWnd.KillPercent*100)
	self.m_LevelLabel.text = SafeStringFormat(LocalString.GetString("第%d层"), CLuaHuaBiResultWnd.Level)
	self.m_ExpLabel.text = SafeStringFormat("%d", CLuaHuaBiResultWnd.TotalExp)
	self.m_Result:LoadMaterial(CLuaHuaBiResultWnd.ScoreSpritePath)
	if CLuaHuaBiResultWnd.ShowEffect == 1 then
		self.m_SpecialEffect:SetActive(true)
	else
		self.m_SpecialEffect:SetActive(false)
	end
end


