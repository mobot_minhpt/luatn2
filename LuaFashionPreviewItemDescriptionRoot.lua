local Extensions = import "Extensions"
local CFashionPreviewMgr = import "L10.UI.CFashionPreviewMgr"

LuaFashionPreviewItemDescriptionRoot = class()

RegistChildComponent(LuaFashionPreviewItemDescriptionRoot,"m_NameLabel","NameLabel", UILabel)
RegistChildComponent(LuaFashionPreviewItemDescriptionRoot,"m_LabelTemplate","LabelTemplate", GameObject)
RegistChildComponent(LuaFashionPreviewItemDescriptionRoot,"m_Table","Table", UITable)
RegistChildComponent(LuaFashionPreviewItemDescriptionRoot,"m_Arrow","Arrow", UISprite)
RegistChildComponent(LuaFashionPreviewItemDescriptionRoot,"m_ArrowBg","ArrowBg", GameObject)
RegistChildComponent(LuaFashionPreviewItemDescriptionRoot,"m_ScrollView","ScrollView", UIScrollView)

RegistClassMember(LuaFashionPreviewItemDescriptionRoot,"m_YellowColor")
RegistClassMember(LuaFashionPreviewItemDescriptionRoot,"m_IsShow")
RegistClassMember(LuaFashionPreviewItemDescriptionRoot,"m_InitialPosX")
function LuaFashionPreviewItemDescriptionRoot:Awake()
    self.m_YellowColor = "[FFB74C]"
    self.m_InitialPosX = self.transform.localPosition.x
    self.m_IsShow = true
    self:UpdateState()
    self.m_LabelTemplate:SetActive(false)
    self:Hide()

    UIEventListener.Get(self.m_Arrow.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnArrowClick(go)
    end)
end

function LuaFashionPreviewItemDescriptionRoot:OnEnable()
    g_ScriptEvent:AddListener("ShopMallFashionPreviewWnd_UpdateDescription",self, "UpdateDescription")
end

function LuaFashionPreviewItemDescriptionRoot:OnDisable()
    g_ScriptEvent:RemoveListener("ShopMallFashionPreviewWnd_UpdateDescription",self, "UpdateDescription")
end

function LuaFashionPreviewItemDescriptionRoot:Hide()
    self.m_NameLabel.gameObject:SetActive(false)
    Extensions.RemoveAllChildren(self.m_Table.transform)
    self.m_ArrowBg:SetActive(false)
    self.m_Table:Reposition()
    self.m_ScrollView:ResetPosition()
end

function LuaFashionPreviewItemDescriptionRoot:AddItem(text)
    local go = NGUITools.AddChild(self.m_Table.gameObject, self.m_LabelTemplate)
    go.transform:Find("Label"):GetComponent(typeof(UILabel)).text = CUICommonDef.TranslateToNGUIText(text)
    go:SetActive(true)
end

function LuaFashionPreviewItemDescriptionRoot:AddItemKeyValItem(key, value, isUseDefaultFormat)
    if isUseDefaultFormat then
        self:AddItem(SafeStringFormat3("%s: %s%s",key, self.m_YellowColor, value))
        return
    end
    self:AddItem(SafeStringFormat3("%s: %s",key, value))

end

function LuaFashionPreviewItemDescriptionRoot:UpdateDescription(data)
    local itemID, curCategory = data[1], data[2]
    if itemID == nil then
        self:Hide()
        return
    end
    local item = Item_Item.GetData(itemID)
    if not item then
        self:Hide()
        return
    end
    Extensions.RemoveAllChildren(self.m_Table.transform)
    self.m_NameLabel.gameObject:SetActive(true)

    local Name = item.Name
    self.m_NameLabel.text = Name

    local emSubCategory = LuaShopMallFashionPreviewMgr.EnumLingYuMallSubCategory
    if curCategory == emSubCategory.ZuoQi then
        self:UpdateZuoQiDescription(item)
    elseif curCategory == emSubCategory.Umbrella then
        self:UpdateUmbrellanDescription(item)
    elseif curCategory == emSubCategory.ShiZhuang or curCategory == emSubCategory.BackPendant then
        self:UpdateFashionDescription(item)
    end

    self.m_Table:Reposition()
    self.m_ScrollView:ResetPosition()
    self.m_ArrowBg:SetActive(true)

    if not self.m_IsShow then
        self.m_Table.gameObject:SetActive(false)
        self.m_NameLabel.gameObject:SetActive(false)
    end
end

function LuaFashionPreviewItemDescriptionRoot:AddItemDescription(item)
    local Description = item.Description
    local index = string.find(string.reverse(Description), "r#")
    if not index or index <= 1 then
        self:AddItem(Description)
        return
    end
    Description = string.sub(Description, string.len(Description) - index + 2)
    Description = string.gsub(Description,"endimport","")
    Description = string.gsub(Description,"import","")
    self:AddItem(Description)
end

function LuaFashionPreviewItemDescriptionRoot:AddItemDeleteTime(item)
    local lingYuMall = Mall_LingYuMall.GetData(item.ID)
    if not lingYuMall then
        lingYuMall = Mall_LingYuMallLimit.GetData(item.ID)
    end
    if lingYuMall and not String.IsNullOrEmpty(lingYuMall.DeleteTime) then
        self:AddItemKeyValItem(LocalString.GetString("下架时间"), lingYuMall.DeleteTime, true)
    end
end

function LuaFashionPreviewItemDescriptionRoot:UpdateFashionDescription(item)
    self:AddItemDescription(item)
    self:AddItemDeleteTime(item)
end

function LuaFashionPreviewItemDescriptionRoot:UpdateZuoQiDescription(item)

    local zuoqi = ZuoQi_ZuoQi.GetData(CFashionPreviewMgr.ZuoQiFashionID)
    if not zuoqi then
        return
    end

    local str = LocalString.GetString("单人")
    if zuoqi.CoRide == 1 then
        str = LocalString.GetString("双人")
    elseif zuoqi.CoRide > 1 then
        str = LocalString.GetString("多人")
    end
    local matchStr = SafeStringFormat3("%s%s", self.m_YellowColor, str)

    self:AddItemKeyValItem(LocalString.GetString("坐骑类型"),SafeStringFormat3("%s#W%s",matchStr, LocalString.GetString("坐骑")) , false)

    local barrierTypeMatchStrTable ={ LocalString.GetString("低障"),LocalString.GetString("中障"),LocalString.GetString("高障")}
    self:AddItemKeyValItem(LocalString.GetString("障碍类型"),
            SafeStringFormat3("%s%s%s", LocalString.GetString("可跨越"),self.m_YellowColor,
                    barrierTypeMatchStrTable[zuoqi.BarrierType]), false)

    local speedFactor = zuoqi.SpeedFactor / 0.04
    self:AddItemKeyValItem(LocalString.GetString("移动速度"),SafeStringFormat3("+%d%%",speedFactor), true)

    self:AddItemKeyValItem(LocalString.GetString("坐骑行囊"),SafeStringFormat3("+%d%s",zuoqi.CarryBagSize, LocalString.GetString("格")), true)

    self:AddItemDeleteTime(item)
end

function LuaFashionPreviewItemDescriptionRoot:UpdateUmbrellanDescription(item)
    self:AddItemDescription(item)

    local foundExpressionAppearanceKey = nil
    Expression_Appearance.Foreach(function (key,data)
        if foundExpressionAppearanceKey == nil and data.Group == LuaShopMallFashionPreviewMgr.KaiSanAppearanceGroup and CFashionPreviewMgr.UmbrellaFashionID == data.PreviewDefine then
            foundExpressionAppearanceKey = key
        end
    end)
    if foundExpressionAppearanceKey ~= nil then
        local AvailableTime = Expression_Appearance.GetData(foundExpressionAppearanceKey).AvailableTime
        self:AddItemKeyValItem(LocalString.GetString("有效期"), (AvailableTime == 0) and
                LocalString.GetString("永久") or
                SafeStringFormat3("%d%s", AvailableTime, LocalString.GetString("天")), true)
    end
end

function LuaFashionPreviewItemDescriptionRoot:UpdateState()
    Extensions.SetLocalRotationZ(self.m_Arrow.transform, self.m_IsShow and 90 or -90)
    self.m_Arrow.alpha = self.m_IsShow and 0.3 or 1
    local tween = LuaTweenUtils.TweenPositionX(self.transform, self.m_IsShow and self.m_InitialPosX or (self.m_InitialPosX - 329) ,0.3)
    if not self.m_IsShow then
        LuaTweenUtils.OnComplete(tween,function ()
            self.m_Table.gameObject:SetActive(false)
            self.m_NameLabel.gameObject:SetActive(false)
        end)
    else
        self.m_NameLabel.gameObject:SetActive(true)
        self.m_Table.gameObject:SetActive(true)
        self.m_Table:Reposition()
        self.m_ScrollView:ResetPosition()
    end

end

function LuaFashionPreviewItemDescriptionRoot:OnArrowClick(go)
    self.m_IsShow = not self.m_IsShow
    self:UpdateState()
end
