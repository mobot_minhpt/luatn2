local GameObject = import "UnityEngine.GameObject"
local DelegateFactory  = import "DelegateFactory"
local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local Item_Item = import "L10.Game.Item_Item"
local IdPartition = import "L10.Game.IdPartition"
local EquipmentTemplate_Equip=import "L10.Game.EquipmentTemplate_Equip"
local UIEventListener = import "UIEventListener"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"

LuaQiFuPointItemTemplate = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaQiFuPointItemTemplate, "DisableSprite", "DisableSprite", GameObject)
RegistChildComponent(LuaQiFuPointItemTemplate, "ItemQiFuPointLabel", "ItemQiFuPointLabel", UILabel)
RegistChildComponent(LuaQiFuPointItemTemplate, "IconTexture", "IconTexture", CUITexture)

--@endregion RegistChildComponent end
RegistClassMember(LuaQiFuPointItemTemplate, "m_templateId")


function LuaQiFuPointItemTemplate:Awake()
    --@region EventBind: Dont Modify Manually!
    UIEventListener.Get(self.IconTexture.gameObject).onClick =  DelegateFactory.VoidDelegate(
        function(go)
            CItemInfoMgr.ShowLinkItemTemplateInfo(self.m_templateId)
        end
    )
    --@endregion EventBind end
end

function LuaQiFuPointItemTemplate:Init(templateId, qiFuPoint)
    self.m_templateId = templateId
    self.DisableSprite.gameObject:SetActive(false)
    if IdPartition.IdIsItem(templateId) then
        local item = Item_Item.GetData(templateId)
        self.IconTexture:LoadMaterial(item.Icon)
        self.ItemQiFuPointLabel.text = System.String.Format(LocalString.GetString("{0}万"),qiFuPoint)
    else
        local equip = EquipmentTemplate_Equip.GetData(templateId)
        if equip then
            self.IconTexture:LoadMaterial(equip.Icon)
            self.ItemQiFuPointLabel.text = System.String.Format(LocalString.GetString("{0}万"),qiFuPoint)
        else
            self.IconTexture:Clear()
            self.ItemQiFuPointLabel.text = LocalString.GetString("")
        end
    end
end

function LuaQiFuPointItemTemplate:ReachQiFuPoint()
    self.DisableSprite.gameObject:SetActive(true)
end
--@region UIEvent

--@endregion UIEvent

