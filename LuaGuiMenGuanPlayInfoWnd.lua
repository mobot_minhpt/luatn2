local LuaGameObject=import "LuaGameObject"

local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"

CLuaGuiMenGuanPlayInfoWnd = class()
RegistClassMember(CLuaGuiMenGuanPlayInfoWnd, "m_TableView")
RegistClassMember(CLuaGuiMenGuanPlayInfoWnd, "m_TimeBar")
RegistClassMember(CLuaGuiMenGuanPlayInfoWnd, "m_TimeLabel")

function CLuaGuiMenGuanPlayInfoWnd:Init()
    self.m_TableView = LuaGameObject.GetChildNoGC(self.transform, "TableView").tableView
    self.m_TimeBar = LuaGameObject.GetChildNoGC(self.transform, "TimeBar").progressBar
    self.m_TimeLabel = LuaGameObject.GetChildNoGC(self.transform, "TimeLabel").label

    self.m_TableViewDataSource = DefaultTableViewDataSource.CreateByCount(#CLuaGuiMenGuanMgr.s_SceneBossInfoTbl, function(...) self:InitItem(...) end)
    self.m_TableView.m_DataSource = self.m_TableViewDataSource

    self:Refresh()
end

function CLuaGuiMenGuanPlayInfoWnd:InitItem(item, index)
    if not CLuaGuiMenGuanMgr.s_SceneBossInfoTbl[index + 1] then return end

    local tf = item.transform
    local nameLabel = LuaGameObject.GetChildNoGC(tf, "nameLabel").label
    local countLabel = LuaGameObject.GetChildNoGC(tf, "countLabel").label
    local gotoBtn = LuaGameObject.GetChildNoGC(tf, "gotoBtn").gameObject

    local sceneId, sceneName, count = unpack(CLuaGuiMenGuanMgr.s_SceneBossInfoTbl[index + 1] or {})
    if not (sceneId and sceneName and count) then return end

    nameLabel.text = sceneName
    countLabel.text = tostring(count)
    UIEventListener.Get(gotoBtn).onClick = LuaUtils.VoidDelegate(function(go) self:OnGotoBtnClicked(sceneId) end)
end

function CLuaGuiMenGuanPlayInfoWnd:OnGotoBtnClicked(sceneId)
    Gac2Gas.GuiMenGuanRequestTrackToBoss(sceneId)
end

function CLuaGuiMenGuanPlayInfoWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncGuiMenGuanInfo", self, "Refresh")
end

function CLuaGuiMenGuanPlayInfoWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncGuiMenGuanInfo", self, "Refresh")
end

function CLuaGuiMenGuanPlayInfoWnd:Refresh()
    if not CLuaGuiMenGuanMgr.s_ShowStatusView then
        CUIManager.CloseUI(CUIResources.GuiMenGuanPlayInfoWnd)
        return
    end
    
    self.m_TimeLabel.text = SafeStringFormat(LocalString.GetString("%02d:%02d"), math.floor(CLuaGuiMenGuanMgr.s_RemainTime / 60), CLuaGuiMenGuanMgr.s_RemainTime % 60)
    self.m_TimeBar.value = CLuaGuiMenGuanMgr.s_RemainTime / CLuaGuiMenGuanMgr.s_FightDuration
    self.m_TableView:ReloadData(true, false)
end

return CLuaGuiMenGuanPlayInfoWnd