local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"

CLuaMengGuiJieSkillTipWnd = class()

function CLuaMengGuiJieSkillTipWnd:Init()
    --逃跑方
    local go        = self.transform:Find("Anchor/Runner/Content/Skill").gameObject
    local id        = Halloween2020_MengGuiJie.GetData().DefenderTempSkillIds
    local message   = "HALLOWEEN2020_TONGGUIYUJIN_INTERFACE_TIP"
    self:InitItem(go, id, message)

    --抓捕方
    local skillMessages = {
        "",
        "HALLOWEEN2020_LAREN_INTERFACE_TIP",
        "HALLOWEEN2020_JIASU_INTERFACE_TIP",
        "HALLOWEEN2020_YINSHEN_INTERFACE_TIP",
        "HALLOWEEN2020_SHANXIAN_INTERFACE_TIP",
        "HALLOWEEN2020_SHUYI_INTERFACE_TIP",
        "HALLOWEEN2020_DINGSHEN_INTERFACE_TIP",
    }    

    local skillIds = Halloween2020_MengGuiJie.GetData().TempSkillIds

    for i = 0, skillIds.Length - 1 do 
        local skillId       = skillIds[i]
        local skillGo       = self.transform:Find("Anchor/Catcher/Content/Skill"..(i+1)).gameObject
        local skillMessage  = skillMessages[i+1]

        if skillGo and skillMessage then
            self:InitItem(skillGo, skillId, skillMessage)
        end
    end
end

function CLuaMengGuiJieSkillTipWnd:InitItem(go, skillId, message)
    local icon  = go.transform:Find("SkillIcon/Mask/Icon"):GetComponent(typeof(CUITexture))
    local label = go.transform:Find("Label"):GetComponent(typeof(UILabel))
    local skill = Skill_AllSkills.GetData(skillId)

    icon:LoadMaterial(SafeStringFormat3("UI/Texture/Skill/Material/%s.mat",skill.SkillIcon))
    if message == "" then
        label.text = SafeStringFormat3(LocalString.GetString("[c][FFCD00]%s[-]"), skill.Name)
    else
        label.text = SafeStringFormat3(LocalString.GetString("[c][FFCD00]%s[-]：%s"), skill.Name, g_MessageMgr:FormatMessage(message))
    end
end