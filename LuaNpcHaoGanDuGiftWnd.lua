local AlignType = import "L10.UI.CTooltip+AlignType"
local IdPartition=import "L10.Game.IdPartition"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CItemMgr=import "L10.Game.CItemMgr"
local QnSelectableButton=import "L10.UI.QnSelectableButton"
local CUITexture=import "L10.UI.CUITexture"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CItemCountUpdate = import "L10.UI.CItemCountUpdate"


CLuaNpcHaoGanDuGiftWnd = class()
RegistClassMember(CLuaNpcHaoGanDuGiftWnd,"m_TitleLabel")
RegistClassMember(CLuaNpcHaoGanDuGiftWnd,"m_SelectedItemTemplateId")
RegistClassMember(CLuaNpcHaoGanDuGiftWnd,"m_Items")
RegistClassMember(CLuaNpcHaoGanDuGiftWnd,"m_ItemInfos")

RegistClassMember(CLuaNpcHaoGanDuGiftWnd,"m_CountUpdate")
RegistClassMember(CLuaNpcHaoGanDuGiftWnd,"m_Icon")
RegistClassMember(CLuaNpcHaoGanDuGiftWnd,"m_NameLabel")
RegistClassMember(CLuaNpcHaoGanDuGiftWnd,"m_ButtonLabel")
RegistClassMember(CLuaNpcHaoGanDuGiftWnd,"m_Button")
RegistClassMember(CLuaNpcHaoGanDuGiftWnd,"m_IsPurchase")
RegistClassMember(CLuaNpcHaoGanDuGiftWnd,"m_HaoGanDuLabel")

function CLuaNpcHaoGanDuGiftWnd:Awake()
    self.m_Items={}
    local grid = self.transform:Find("Grid").gameObject
    local item=self.transform:Find("Template").gameObject
    item:SetActive(false)
    -- table.insert( self.m_Items,item )
    Extensions.RemoveAllChildren(grid.transform)

    for i=1,3 do
        local go=NGUITools.AddChild(grid,item)
        go:SetActive(true)
        table.insert( self.m_Items,go )
    end
    grid:GetComponent(typeof(UIGrid)):Reposition()
    for i,v in ipairs(self.m_Items) do
        UIEventListener.Get(v).onClick = DelegateFactory.VoidDelegate(function(g)
            self:OnClickItem(g)
        end)
    end

    self.m_Icon = self.transform:Find("Item/Icon"):GetComponent(typeof(CUITexture))
    self.m_NameLabel = self.transform:Find("Item/NameLabel"):GetComponent(typeof(UILabel))
    self.m_NameLabel.text=nil

    self.m_HaoGanDuLabel=self.transform:Find("Item/HaoGanDuLabel"):GetComponent(typeof(UILabel))
    self.m_HaoGanDuLabel.text=nil

    self.m_ButtonLabel = self.transform:Find("SendButton/Label"):GetComponent(typeof(UILabel))
end
function CLuaNpcHaoGanDuGiftWnd:Init()
    self.m_Button = self.transform:Find("SendButton").gameObject
    UIEventListener.Get(self.m_Button).onClick = DelegateFactory.VoidDelegate(function(go)
        local find,pos, itemId = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, self.m_SelectedItemTemplateId)
        if find then
            Gac2Gas.RequestZengSongSpecialGiftToNpc(CLuaNpcHaoGanDuWnd.m_NpcId,1,pos)
        else
            self:Purchase(1)
        end
    end)

    self.m_TitleLabel = self.transform:Find("Wnd_Bg_Secondary_3/TitleLabel"):GetComponent(typeof(UILabel))


    
    self.m_CountUpdate = self.transform:Find("Item"):GetComponent(typeof(CItemCountUpdate))
    local mask=self.transform:Find("Item/Mask").gameObject
    UIEventListener.Get(mask).onClick = DelegateFactory.VoidDelegate(function(go)
        CItemAccessListMgr.Inst:ShowItemAccessInfo(self.m_SelectedItemTemplateId, true, nil, AlignType.Right)
    end)
    local function OnCountChange(val)
        if val>=1 then
            mask:SetActive(false)
            -- self.m_CountUpdate.format=SafeStringFormat3("{0}/%d",self.m_NeedCount)
        else
            mask:SetActive(true)
            -- self.m_CountUpdate.format=SafeStringFormat3("[ff0000]{0}[-]/%d",self.m_NeedCount)
        end
        self:UpdateItemLabel()
    end
    self.m_CountUpdate.onChange=DelegateFactory.Action_int(OnCountChange)

    self:InitGiftInfo()
    self.m_CountUpdate:UpdateCount()

    self:OnClickItem(self.m_Items[1])
end

function CLuaNpcHaoGanDuGiftWnd:UpdateItemLabel()
    local flag = 0
    for j,m in ipairs(self.m_Items) do
        if self.m_SelectedItemTemplateId == self.m_ItemInfos[j].itemId then
            local statusLabel=m.transform:Find("StatusLabel"):GetComponent(typeof(UILabel))
            flag = self.m_ItemInfos[j].flag
            if flag==0 then
                local find,pos,itemId = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, self.m_SelectedItemTemplateId)
                if find then
                    statusLabel.text = LocalString.GetString("[00ff00]拥有[-]")
                else
                    statusLabel.text = LocalString.GetString("[808080]未拥有[-]")
                end
            else
                statusLabel.text = LocalString.GetString("已赠送")
            end
        end
    end

    local find,pos,itemId = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, self.m_SelectedItemTemplateId)
    if find then
        self.m_ButtonLabel.text = LocalString.GetString("赠送")
    else
        self.m_ButtonLabel.text = LocalString.GetString("购买并赠送")
    end

    CUICommonDef.SetActive(self.m_Button,flag==0,true)
    if flag>0 then
        self.m_ButtonLabel.text = LocalString.GetString("已赠送")
    end
end

function CLuaNpcHaoGanDuGiftWnd:InitTitle()
    local count=0
    for i,v in ipairs(self.m_ItemInfos) do
        if v.flag>0 then
            count=count+1
        end
    end
    self.m_TitleLabel.text = SafeStringFormat3( LocalString.GetString("每日偏爱(%d/%d)"),count,3 )
end

function CLuaNpcHaoGanDuGiftWnd:InitItem(go,itemTemplateId,flag)
    local tf=go.transform
    local icon=tf:Find("Icon"):GetComponent(typeof(CUITexture))

    if IdPartition.IdIsItem(itemTemplateId) then
        icon:LoadMaterial(Item_Item.GetData(itemTemplateId).Icon)
    else
        icon:LoadMaterial(EquipmentTemplate_Equip.GetData(itemTemplateId).Icon)
    end
    local statusLabel=tf:Find("StatusLabel"):GetComponent(typeof(UILabel))
    local tick = tf:Find("TickSprite").gameObject
    if flag==0 then
        tick:SetActive(false)
        local find,pos,itemId = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, itemTemplateId)
        if find then
            statusLabel.text = LocalString.GetString("[00ff00]拥有[-]")
        else
            statusLabel.text = LocalString.GetString("[808080]未拥有[-]")
        end
    elseif flag>0 then
        tick:SetActive(true)
        statusLabel.text = LocalString.GetString("已赠送")
    end
end

function CLuaNpcHaoGanDuGiftWnd:OnClickItem(g)
    local flag = false
    for j,m in ipairs(self.m_Items) do
        if m==g then
            m:GetComponent(typeof(QnSelectableButton)):SetSelected(true,false)
            self.m_SelectedItemTemplateId = self.m_ItemInfos[j].itemId
            flag = self.m_ItemInfos[j].flag
            
        else
            m:GetComponent(typeof(QnSelectableButton)):SetSelected(false,false)
        end
    end

    self.m_CountUpdate.templateId=self.m_SelectedItemTemplateId
    self.m_CountUpdate:UpdateCount()

    if IdPartition.IdIsItem(self.m_SelectedItemTemplateId) then
        local data = Item_Item.GetData(self.m_SelectedItemTemplateId)
        self.m_Icon:LoadMaterial(data.Icon)
        self.m_NameLabel.text = data.Name
    else
        local data = EquipmentTemplate_Equip.GetData(self.m_SelectedItemTemplateId)
        self.m_Icon:LoadMaterial(data.Icon)
        self.m_NameLabel.text = data.Name
    end

    local find,pos,itemId = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, self.m_SelectedItemTemplateId)
    if find then
        self.m_ButtonLabel.text = LocalString.GetString("赠送")
    else
        self.m_ButtonLabel.text = LocalString.GetString("购买并赠送")
    end

    CUICommonDef.SetActive(self.m_Button,flag==0,true)
    if flag>0 then
        self.m_ButtonLabel.text = LocalString.GetString("已赠送")
    end


    local haogandu = 0
    NpcHaoGanDu_SpecialGift.Foreach(function(k,v)
        if v.ItemId ==self.m_SelectedItemTemplateId and v.NpcId==CLuaNpcHaoGanDuWnd.m_NpcId then
            haogandu=v.Basic
        end
    end)
    self.m_HaoGanDuLabel.text = SafeStringFormat3( LocalString.GetString("好感度提升%d"),haogandu )
end

function CLuaNpcHaoGanDuGiftWnd:Purchase(count)
    --判断赠送数量
    local setting = NpcHaoGanDu_Setting.GetData()
    local playProp = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp
    local ret,data = CommonDefs.DictTryGet_LuaCall(playProp.NpcHaoGanDuData,CLuaNpcHaoGanDuWnd.m_NpcId)
    if data then
        local leftTime = setting.FreeZengSongNum+data.BuyNum - data.ZengSongNum
        if leftTime<=0 then
            g_MessageMgr:ShowMessage("NpcHaoGanDu_ZengSongNum_Limit")
            return
        end   
        -- NpcHaoGanDu_ZengSong_Gift_Limit
        local setting = NpcHaoGanDu_Setting.GetData()
        local field = setting.HaoGanDuGrade
        local max = field[field.Length-1][2]
        if data.HaoGanDu>=max then
            g_MessageMgr:ShowMessage("NpcHaoGanDu_ZengSong_Gift_Limit")
            return
        end
    end

    local itemGet = ItemGet_item.GetData(self.m_SelectedItemTemplateId)
    if itemGet then
        if itemGet.GetPath then
            for i=1,itemGet.GetPath.Length do
                if itemGet.GetPath[i-1]==1 then--玩家商店
                    self.m_IsPurchase = true
                    Gac2Gas.RequestQuickBuyItemFromPlayerShop(self.m_SelectedItemTemplateId, count, false)
                    break
                elseif itemGet.GetPath[i-1]==5 then--易市
                    self.m_IsPurchase = true
                    Gac2Gas.BuyMallItemWithReason(4,self.m_SelectedItemTemplateId,count,"NpcHaoGanDu")
                    break
                end
            end
        end
    end
end


function CLuaNpcHaoGanDuGiftWnd:OnEnable()
    g_ScriptEvent:AddListener("PlayerBuyMallItemSuccess",self,"OnPlayerBuyMallItemSuccess")
    g_ScriptEvent:AddListener("OnQuickBuyItemFromPlayerShopSuccess",self,"OnQuickBuyItemFromPlayerShopSuccess")
    g_ScriptEvent:AddListener("SyncNpcHaoGanDuSpecialGiftInfo",self,"OnSyncNpcHaoGanDuSpecialGiftInfo")
    g_ScriptEvent:AddListener("OnQuickBuyItemFromPlayerShopFail",self,"OnQuickBuyItemFromPlayerShopFail")
    
end
function CLuaNpcHaoGanDuGiftWnd:OnDisable()
    g_ScriptEvent:RemoveListener("PlayerBuyMallItemSuccess",self,"OnPlayerBuyMallItemSuccess")
    g_ScriptEvent:RemoveListener("OnQuickBuyItemFromPlayerShopSuccess",self,"OnQuickBuyItemFromPlayerShopSuccess")
    g_ScriptEvent:RemoveListener("SyncNpcHaoGanDuSpecialGiftInfo",self,"OnSyncNpcHaoGanDuSpecialGiftInfo")
    g_ScriptEvent:RemoveListener("OnQuickBuyItemFromPlayerShopFail",self,"OnQuickBuyItemFromPlayerShopFail")
end

function CLuaNpcHaoGanDuGiftWnd:OnSyncNpcHaoGanDuSpecialGiftInfo(npcId)
    if CLuaNpcHaoGanDuWnd.m_NpcId==npcId then
        self:InitGiftInfo()
        
        local find,pos,itemId = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, self.m_SelectedItemTemplateId)
        if find then
            self.m_ButtonLabel.text = LocalString.GetString("赠送")
        else
            self.m_ButtonLabel.text = LocalString.GetString("购买并赠送")
        end
        for j,m in ipairs(self.m_Items) do
            if self.m_SelectedItemTemplateId == self.m_ItemInfos[j].itemId then
                local flag = self.m_ItemInfos[j].flag
                CUICommonDef.SetActive(self.m_Button,flag==0,true)
                if flag>0 then
                    self.m_ButtonLabel.text = LocalString.GetString("已赠送")
                end
                break
            end
        end
    end
end

function CLuaNpcHaoGanDuGiftWnd:InitGiftInfo()
    self.m_ItemInfos={}
    local playProp = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp
    if playProp then
        local ret,data = CommonDefs.DictTryGet_LuaCall(playProp.NpcHaoGanDuData,CLuaNpcHaoGanDuWnd.m_NpcId)
        if ret then--
            CommonDefs.DictIterate(data.SpecialGiftData, DelegateFactory.Action_object_object(function (___key, ___value)
                table.insert( self.m_ItemInfos,{
                    itemId = ___key,
                    flag = ___value
                } )
            end))
            for i,v in ipairs(self.m_Items) do
                self:InitItem(v,self.m_ItemInfos[i].itemId,self.m_ItemInfos[i].flag)
            end
        end
    end

    self:InitTitle()
end

function CLuaNpcHaoGanDuGiftWnd:OnPlayerBuyMallItemSuccess(args)
   local regionId, templateId, itemCount, reason = args[0],args[1],args[2],args[3]
    if templateId==self.m_SelectedItemTemplateId and reason=="NpcHaoGanDu" then
        if self.m_IsPurchase then
            local find,pos, itemId = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, templateId)
            if find then
                Gac2Gas.RequestZengSongSpecialGiftToNpc(CLuaNpcHaoGanDuWnd.m_NpcId,1,pos)
                self.m_IsPurchase = false
            end
        end
   end
end

function CLuaNpcHaoGanDuGiftWnd:OnQuickBuyItemFromPlayerShopSuccess(args)
    local price, itemName, templateId, num = args[0],args[1],args[2],args[3]
    if templateId==self.m_SelectedItemTemplateId then
        if self.m_IsPurchase then
            local find,pos, itemId = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, templateId)
            if find then
                Gac2Gas.RequestZengSongSpecialGiftToNpc(CLuaNpcHaoGanDuWnd.m_NpcId,1,pos)
                self.m_IsPurchase = false
            end
        end

    end
end
function CLuaNpcHaoGanDuGiftWnd:OnQuickBuyItemFromPlayerShopFail(args)
    local templateId, num = args[0],args[1]
    if templateId==self.m_SelectedItemTemplateId then
        g_MessageMgr:ShowMessage("NpcHaoGanDuGift_BuyFail")
    end
end
