local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CJingLingMgr = import "L10.Game.CJingLingMgr"
local Object = import "System.Object"
local CUIManager = import "L10.UI.CUIManager"
local CWebBrowserMgr = import "L10.Game.CWebBrowserMgr"
local CPayMgr = import "L10.Game.CPayMgr"
local EnumJingLingParamReplaceRule = import "L10.Game.EnumJingLingParamReplaceRule"
local HTTPHelper = import "L10.Game.HTTPHelper"
local Main = import "L10.Engine.Main"
local Json = import "L10.Game.Utils.Json"
local String = import "System.String"
local JingLingFromDefine = import "L10.Game.JingLingFromDefine"
local CHTTPForm = import "L10.Game.CHTTPForm"
local ClientHttp = import "L10.Game.ClientHttp"
local JingLingMessageExtraInfoKey = import "L10.Game.JingLingMessageExtraInfoKey"


LuaJingLingMgr = class()

-----------------------------
--BEGIN 精灵问题停留时长LOG--
-----------------------------

LuaJingLingMgr.m_LastQuestion = nil
LuaJingLingMgr.m_LastKeywords = nil
LuaJingLingMgr.m_LastQueryTime = 0

-- token相关
LuaJingLingMgr.m_GameYWToken = nil
LuaJingLingMgr.m_GameYWTokenExpireTime = nil
LuaJingLingMgr.m_GameYWClientUrl = nil

--开始记录精灵问答停留时间
function LuaJingLingMgr.OnRecordQueryDurationBegin(question, answer, keywords)
    LuaJingLingMgr.m_LastQuestion = question or ""
    LuaJingLingMgr.m_LastKeywords = keywords or ""
    LuaJingLingMgr.m_LastQueryTime = CServerTimeMgr.Inst.timeStamp
end
--结束记录精灵问答停留时间，并报告log
function LuaJingLingMgr.OnRecordQueryDurationEnd()
    if LuaJingLingMgr.m_LastQuestion and LuaJingLingMgr.m_LastKeywords and LuaJingLingMgr.m_LastQueryTime then
        local endTime = CServerTimeMgr.Inst.timeStamp
        local openTime = CJingLingMgr.Inst.m_JingLingWndOpenTime
        if openTime>0 and openTime<endTime then
            Gac2Gas.RequestLogJingLingSpendTime(math.max(LuaJingLingMgr.m_LastQueryTime, openTime), endTime, LuaJingLingMgr.m_LastQuestion, LuaJingLingMgr.m_LastKeywords)
        end
    end
    LuaJingLingMgr.m_LastQuestion = nil
    LuaJingLingMgr.m_LastKeywords = nil
    LuaJingLingMgr.m_LastQueryTime = 0
end

function LuaJingLingMgr.PlayRemoteVideo(url)
    LuaPersonalSpaceMgrReal.MoviePlayUrl = url
    CUIManager.ShowUI("DetailMovieShowWnd")
end

function LuaJingLingMgr.OpenSDKJingLing(question)
    if question==nil or question=="" then
        Gac2Gas.QuerySmartGMTokenAndUrl("sprite") --无参数时默认打开主页
        return
    end
    Gac2Gas.QuerySmartGMTokenAndUrlWitchContext("sprite", question, "default")
end
-- 精灵直接跳转内置浏览器客户端日志记录开关
LuaJingLingMgr.EnableAutoClickUrlRecordLog = true 
-- 打开内置浏览器
function LuaJingLingMgr.OpenUrlJingLingInWeb(url, keyWords)
    if System.String.IsNullOrEmpty(url) then return end
    if LuaJingLingMgr.EnableAutoClickUrlRecordLog then LuaJingLingMgr.RecordAutoClickUrlLog(url, keyWords)  end
    CJingLingMgr.Inst:ReplaceParams(url, EnumJingLingParamReplaceRule.ReplaceByValue);
    CWebBrowserMgr.Inst:OpenUrl(url)
end
-- 精灵跳转内置浏览器日志记录
function LuaJingLingMgr.RecordAutoClickUrlLog(url, keyWords) 
    local dict = CreateFromClass(MakeGenericClass(Dictionary, cs_string, Object))
    CommonDefs.DictAdd_LuaCall(dict, "methodName", "AutoClickUrl")
    CommonDefs.DictAdd_LuaCall(dict, "params", LuaChatMgr.TruncateString(tostring(url)))
    CommonDefs.DictAdd_LuaCall(dict, "keyword", tostring(keyWords))
    Gac2Gas.RequestRecordClientLog("JingLingLinkClick", MsgPackImpl.pack(dict))
end
---------------------------
--END 精灵问题停留时长LOG--
---------------------------

-----------------------------
--BEGIN 精灵快捷购买--
-----------------------------
LuaJingLingMgr.m_QuickPurchaseInfo = nil
--CJingLingAnswerItem中也有调用，请留意
function LuaJingLingMgr:CheckQuickPurchaseItem(templateId, purchaseType)
    --[[ purchase method 定义
        1.只有RMB可购买
        2.只有灵玉可购买
        3.只有元宝可购买
        4.灵玉和元宝都可购买

        来自骏骏的整理：
        1.RMB购买，对应Mall_LingYuMallLimit表，且RmbPID不为空
        2.灵玉购买普通灵玉商城道具，对应Mall_LingYuMall表
        3.灵玉购买限购灵玉商城道具，对应Mall_LingYuMallLimit表，且RmbPID为空
        4.元宝购买，对应Mall_YuanBaoMall表
    ]]
    --不检查道具Status 目前发现AutoPatch会修改服务器端Status但是客户端不修改，导致不同步
    if purchaseType == EnumJingLingQuickPurchaseType.eRMB then

        local data = Mall_LingYuMallLimit.GetData(templateId)
        return data and data.RmbPID and data.RmbPID~=""

    elseif purchaseType == EnumJingLingQuickPurchaseType.eLingYu then

        if Mall_LingYuMall.Exists(templateId) then
            local data = Mall_LingYuMall.GetData(templateId)
            return data~=nil
        elseif Mall_LingYuMallLimit.Exists(templateId) then
            local data = Mall_LingYuMallLimit.GetData(templateId)
            return data and (data.RmbPID==nil or data.RmbPID=="")
        else
            return false
        end

    elseif purchaseType == EnumJingLingQuickPurchaseType.eYuanBao then
        
        local data = Mall_YuanBaoMall.GetData(templateId)
        return data~=nil

    elseif purchaseType == EnumJingLingQuickPurchaseType.eLingYuAndYuanBao then

        return self:CheckQuickPurchaseItem(templateId, EnumJingLingQuickPurchaseType.eLingYu) 
                and self:CheckQuickPurchaseItem(templateId,EnumJingLingQuickPurchaseType.eYuanBao)
    
    else
        return false
    end
end

function LuaJingLingMgr:ShowQuickPurchaseWnd(templateId, purchaseType, keywords)
    if self:CheckQuickPurchaseItem(templateId, purchaseType) then
        self.m_QuickPurchaseInfo = {}
        self.m_QuickPurchaseInfo.templateId = templateId
        self.m_QuickPurchaseInfo.purchaseType = purchaseType
        self.m_QuickPurchaseInfo.keywords = keywords or ""
        CUIManager.ShowUI("JingLingQuickPurchaseWnd")
    end
end

function LuaJingLingMgr:GetQuickPurchaseInfo()
    return self.m_QuickPurchaseInfo
end

function LuaJingLingMgr:QuickPurchaseRMBItem(templateId, count)
    local data = Mall_LingYuMallLimit.GetData(templateId)
    if data then
        Gac2Gas.ReuqestLogJingLingQuickView(EShopMallRegion_lua.ELingyuMallLimit, templateId, count, data.Jade*count, self.m_QuickPurchaseInfo.keywords)
        CPayMgr.Inst:BuyProduct(CPayMgr.Inst:PIDConverter(data.RmbPID), 0)
    end
end

function LuaJingLingMgr:QuickPurchaseLingYuMallItem(templateId, count)
    local mallRegion = nil
    if Mall_LingYuMall.Exists(templateId) then
        mallRegion = EShopMallRegion_lua.ELingyuMall
    elseif Mall_LingYuMallLimit.Exists(templateId) then
        mallRegion = EShopMallRegion_lua.ELingyuMallLimit
    end
    if mallRegion then
        Gac2Gas.BuyMallItemWithReason(mallRegion, templateId, count, "jlqb_"..self.m_QuickPurchaseInfo.keywords)
    end
end

function LuaJingLingMgr:QuickPurchaseYuanBaoMallItem(templateId, count)
    local data = Mall_YuanBaoMall.GetData(templateId)
	if data then
        Gac2Gas.BuyMallItemWithReason(EShopMallRegion_lua.EYuanBaoMall, templateId, count, "jlqb_"..self.m_QuickPurchaseInfo.keywords)
	end
end
---------------------------
--END 精灵快捷购买--
---------------------------

-----------------------------
--BEGIN 精灵热词查询--
-----------------------------
--联想热词查询
function LuaJingLingMgr:RequestAssociativeHotWords(question)
    if not CommonDefs.IS_CN_CLIENT then return end      -- 屏蔽海外版本
    if System.String.IsNullOrEmpty(question) then return end
    question = StringTrim(question)
    if CommonDefs.StringLength(question) > 0 then
        Main.Inst:StartCoroutine(HTTPHelper.QueryJingLingTips(question, "seach_recommend", DelegateFactory.Action_bool_string(function (success, ret)
            if success then
                local dict = TypeAs(Json.Deserialize(ret), typeof(MakeGenericClass(Dictionary, String, Object)))
                if CommonDefs.DictContains(dict, typeof(String), "data") then 
                    local list = CommonDefs.DictGetValue(dict, typeof(String), "data")
                    g_ScriptEvent:BroadcastInLua("OnJingLingHotWordsReady", question, list, false)
                end
            end
        end)))
    end
end

--推荐热词查询
LuaJingLingMgr.m_RecommendedHotWordsList = nil
LuaJingLingMgr.m_CanUseJingLingExplore = true
function LuaJingLingMgr:GetJingLingExploreAPI()
    return "/sprite/api/l10/search/explore"
    -- local api = "/sprite/api/%d/search/explore"
    -- return SafeStringFormat3(LocalString.GetString(api), HTTPHelper.JingLingGameId)
end

function LuaJingLingMgr:GameYWTokenExpired()
    return not LuaJingLingMgr.m_GameYWToken or (not LuaJingLingMgr.m_GameYWTokenExpiredTime) or LuaJingLingMgr.m_GameYWTokenExpiredTime < CServerTimeMgr.Inst.timeStamp
end

function LuaJingLingMgr:QuerySpriteGameYWToken()
    if not CommonDefs.IS_CN_CLIENT then return end      -- 屏蔽海外版本
    if LuaJingLingMgr:GameYWTokenExpired() then
        print("GameYWTokenExpired")
        Gac2Gas.QuerySpriteGameYWToken("QueryToken")
    end
end

function LuaJingLingMgr:RequestRecommendedHotWords()
    self.m_RecommendedHotWordsList = nil
    -- 不需要等待
    if not LuaJingLingMgr:GameYWTokenExpired() then
        LuaJingLingMgr:RequestRecommendedHotWordsResult(LuaJingLingMgr.m_GameYWToken, LuaJingLingMgr.m_GameYWTokenExpiredTime, nil, LuaJingLingMgr.m_GameYWClientUrl)
    end
end

function LuaJingLingMgr:RequestRecommendedHotWordsResult(token, expiredTime, context, clientUrl)
    local url = clientUrl .. self:GetJingLingExploreAPI()
    local form = CreateFromClass(CHTTPForm)
    form:AddField("loginFrom", JingLingFromDefine.Sprite)
    form:AddField("method", "explore")
    form:AddField("page", 1)
    form:AddField("pageSize", 5)
    form.m_UseJsonPost = true
    local dic = CreateFromClass(MakeGenericClass(Dictionary, String, String))
    CommonDefs.DictAdd_LuaCall(dic, "token", token)
    CommonDefs.DictAdd_LuaCall(dic, "token-type", "sprite")
    local request = ClientHttp(url, form, dic)
    HTTPHelper.GetResponse(request, DelegateFactory.Action_bool_string(function(ret, text)
        if ret then
            local dict = TypeAs(Json.Deserialize(text), typeof(MakeGenericClass(Dictionary, String, Object)))
            if CommonDefs.DictContains(dict, typeof(String), "data") then
                self.m_RecommendedHotWordsList = CommonDefs.DictGetValue(dict, typeof(String), "data")["keywords"]
            end
        end
    end), false)
end

function LuaJingLingMgr:QuerySpriteGameYWTokenResult(token, expiredTime, context, clientUrl)
    print(token, expiredTime, context, clientUrl)
    if context == "QueryToken" then
        LuaJingLingMgr.m_GameYWToken = token
        LuaJingLingMgr.m_GameYWTokenExpiredTime = expiredTime
        LuaJingLingMgr.m_GameYWClientUrl = clientUrl
    end
    if LuaJingLingMgr.m_CanUseJingLingExplore then      -- 开关
        LuaJingLingMgr:RequestRecommendedHotWordsResult(token, expiredTime, context, clientUrl)
    end
    g_ScriptEvent:BroadcastInLua("QuerySpriteGameYWTokenResult", token, expiredTime, context, clientUrl)
end
---------------------------
--END 精灵热词查询--
---------------------------

-----------------------------
--BEGIN 精灵评价--
-----------------------------
function LuaJingLingMgr:GetJingLingEvaluateTagURL_HTTPS()
    local queryUrl = ""
    if CommonDefs.IS_HMT_CLIENT then
        queryUrl = "https://global-api.chatbot.easebar.com/evaluate/l10hmt.config"
    elseif CommonDefs.IS_CN_CLIENT then
        queryUrl = "https://api.chatbot.nie.163.com/evaluate/x19.config"
    end
    return queryUrl
end

LuaJingLingMgr.m_EvaluateDict = nil
LuaJingLingMgr.m_EvaluateTagMap = nil
function LuaJingLingMgr:QueryJingLingEvaluateTag()
    self.m_EvaluateDict = nil
    self.m_EvaluateTagMap = nil
    local url = self:GetJingLingEvaluateTagURL_HTTPS()
    if System.String.IsNullOrEmpty(url) then 
        CUIManager.CloseUI(CUIResources.JingLingEvaluateWnd)
    end
    Main.Inst:StartCoroutine(HTTPHelper.GetUrl(url, DelegateFactory.Action_bool_string(function (success, text)
        if success then
            local table = luaJson.json2table(text)
            self.m_EvaluateDict = table.evaluate
            self.m_EvaluateTagMap = table.tagmap
            g_ScriptEvent:BroadcastInLua("InitEvaluatePanel")
        else
            CUIManager.CloseUI(CUIResources.JingLingEvaluateWnd)
        end
    end)))
end

function LuaJingLingMgr:SendEvaluateJingLing(otherWord, tagStr, starCount, isHelpful)
    local msg = CJingLingMgr.Inst.saveMsg
    Main.Inst:StartCoroutine(HTTPHelper.EvaluateJingLing(otherWord, tagStr,
        CJingLingMgr.APPChannelHeader .. msg.relatedQuestion, msg.originText, msg.QuestionKey,
        msg.isManual, starCount, CJingLingMgr.GetUserRemarks(msg.isOtherShare),
        CJingLingMgr.Inst:GetQueryJingLingFromParam(), msg.QueryMethod,
        DelegateFactory.Action_bool_string(function (success, result) 

        if not success then
            error("EvaluateJingLing Error! result:" .. result)
            return
        end
        if System.String.IsNullOrEmpty(result) then return end

        if success and luaJson.json2table(result).result == 0 then
            Gac2Gas.EvalJingLingQuestion()
            if not isHelpful and msg.isManual then
                local message = g_MessageMgr:FormatMessage("Ask_Qian_Nv_Zhi_Dao_Confirm")
                MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function () 
                    CJingLingMgr.Inst:AskQianNvZhiDao(msg)
                end), nil, nil, nil, false)
            end
            self:RecordJingLingEvaluate(isHelpful)
        end
    end)))
    CUIManager.CloseUI(CUIResources.JingLingEvaluateWnd)
end

function LuaJingLingMgr:SendEvaluateZhuanQu(isHelpful)
    local msg = CJingLingMgr.Inst.saveMsg
    local issueId = math.floor(tonumber(CommonDefs.DictGetValue(msg.extraInfo, typeof(String), JingLingMessageExtraInfoKey.ZhuanQuIssueID) or 0))
    CJingLingMgr.Inst:OnEvaluateZhuanQuAnswer(issueId, isHelpful, msg.GMSID)
    self:RecordJingLingEvaluate(isHelpful)
    CUIManager.CloseUI(CUIResources.JingLingEvaluateWnd)
end

function LuaJingLingMgr:RecordJingLingEvaluate(isHelpful)
    local msg = CJingLingMgr.Inst.saveMsg
    local evaluate = isHelpful and JingLingEvaluateType_lua.Helpful or JingLingEvaluateType_lua.Helpless
    if not CommonDefs.DictContains(CJingLingMgr.Inst.msgId2EvaluateTypeDict, typeof(UInt32), msg.id) then
        CommonDefs.DictAdd(CJingLingMgr.Inst.msgId2EvaluateTypeDict, typeof(UInt32), msg.id, typeof(Int32), evaluate)
    else
        CommonDefs.DictSet(CJingLingMgr.Inst.msgId2EvaluateTypeDict, typeof(UInt32), msg.id, typeof(Int32), evaluate)
    end
    EventManager.BroadcastInternalForLua(EnumEventType.OnEvaluateJingLingFinished, {msg.id})
end

---------------------------
--END 精灵评价--
---------------------------
local CLoginMgr = import "L10.Game.CLoginMgr"
-- 企微福利官
LuaJingLingMgr.m_WeChatjinglingKey = nil
LuaJingLingMgr.m_OpenWeChatWelfare = true

function LuaJingLingMgr:CanOpenWeChatWelFare()
    return LuaJingLingMgr.m_OpenWeChatWelfare and CLoginMgr.Inst:IsNetEaseOfficialLogin()
end