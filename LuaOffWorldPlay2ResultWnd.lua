local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CUICommonDef = import "L10.UI.CUICommonDef"

LuaOffWorldPlay2ResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaOffWorldPlay2ResultWnd, "CloseButton", "CloseButton", GameObject)
RegistChildComponent(LuaOffWorldPlay2ResultWnd, "score", "score", GameObject)
RegistChildComponent(LuaOffWorldPlay2ResultWnd, "desc1", "desc1", GameObject)
RegistChildComponent(LuaOffWorldPlay2ResultWnd, "desc2", "desc2", GameObject)
RegistChildComponent(LuaOffWorldPlay2ResultWnd, "desc3", "desc3", GameObject)
RegistChildComponent(LuaOffWorldPlay2ResultWnd, "fullTip", "fullTip", GameObject)
RegistChildComponent(LuaOffWorldPlay2ResultWnd, "shareBtn", "shareBtn", GameObject)
RegistChildComponent(LuaOffWorldPlay2ResultWnd, "Victory", "Victory", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaOffWorldPlay2ResultWnd,"m_ShareTick")

function LuaOffWorldPlay2ResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaOffWorldPlay2ResultWnd:InitScoreInfo()
  local realGet = LuaOffWorldPassMgr.Game2Result[3]
  local theoryGet = LuaOffWorldPassMgr.Game2Result[1]
  if realGet < theoryGet then
    self.desc1:SetActive(true)
    self.desc1.transform:Find('Label'):GetComponent(typeof(UILabel)).text = theoryGet
    self.desc2.transform:Find('Bg').gameObject:SetActive(false)
    self.fullTip:SetActive(true)
  else
    self.desc1:SetActive(false)
    self.desc2.transform:Find('Bg').gameObject:SetActive(true)
    self.fullTip:SetActive(false)
  end

  self.score:GetComponent(typeof(UILabel)).text = realGet
  self.desc2.transform:Find('Label'):GetComponent(typeof(UILabel)).text = LuaOffWorldPassMgr.Game2Result[2] + realGet
  self.desc3.transform:Find('Label'):GetComponent(typeof(UILabel)).text = OffWorldPass_Setting.GetData().MaxScorePerWeekCaiDie

  local level = 3
  local totalScoreString = OffWorldPass_Setting.GetData().MingHunFinalScore
  local totalScoreTable = g_LuaUtil:StrSplit(totalScoreString,";")
  for i,v in ipairs(totalScoreTable) do
    local t = g_LuaUtil:StrSplit(v,",")
    local bonusLevel = tonumber(t[1])
    if theoryGet >= tonumber(t[2]) then
      if bonusLevel < level then
        level = bonusLevel
      end
    end
  end

  local materialTable = {"UI/Texture/Transparent/Material/offworldplay1resultwnd_wenzi_01.mat","UI/Texture/Transparent/Material/offworldplay1resultwnd_wenzi_02.mat",
  "UI/Texture/Transparent/Material/offworldplay1resultwnd_wenzi_03.mat"}
  self.Victory:GetComponent(typeof(CUITexture)):LoadMaterial(materialTable[level])
end

function LuaOffWorldPlay2ResultWnd:Init()
  local onCloseClick = function(go)
    CUIManager.CloseUI(CLuaUIResources.OffWorldPlay2ResultWnd)
  end
  CommonDefs.AddOnClickListener(self.CloseButton,DelegateFactory.Action_GameObject(onCloseClick),false)

  local onShareClick = function(go)
    self:SetOpBtnVisible(false)
    if self.m_ShareTick then
      UnRegisterTick(self.m_ShareTick)
      self.m_ShareTick = nil
    end
    self.m_ShareTick = RegisterTickWithDuration(function ()
      self:SetOpBtnVisible(true)
    end, 1000, 1000)
    CUICommonDef.CaptureScreenAndShare()
  end
  CommonDefs.AddOnClickListener(self.shareBtn,DelegateFactory.Action_GameObject(onShareClick),false)

  self:InitScoreInfo()
end

function LuaOffWorldPlay2ResultWnd:SetOpBtnVisible(sign)
  self.shareBtn:SetActive(sign)
  self.CloseButton:SetActive(sign)
end

function LuaOffWorldPlay2ResultWnd:OnDestroy()
  if self.m_ShareTick then
    UnRegisterTick(self.m_ShareTick)
    self.m_ShareTick = nil
  end
end

--@region UIEvent

--@endregion UIEvent
