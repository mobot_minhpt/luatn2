-- Auto Generated!!
local Boolean = import "System.Boolean"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CommonDefs = import "L10.Game.CommonDefs"
local CShopMallGoodsItem = import "L10.UI.CShopMallGoodsItem"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local CYuanbaoShopWnd = import "L10.UI.CYuanbaoShopWnd"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EventManager = import "EventManager"
local Int32 = import "System.Int32"
local Mall_Setting = import "L10.Game.Mall_Setting"
local QnButton = import "L10.UI.QnButton"
local String = import "System.String"
local System = import "System"
local UInt32 = import "System.UInt32"
CYuanbaoShopWnd.m_OnEnable_CS2LuaHook = function (this) 
    CShopMallMgr.OnYuanbaoLimitUpdated = CommonDefs.CombineListner_Action_bool(CShopMallMgr.OnYuanbaoLimitUpdated, MakeDelegateFromCSFunction(this.UpdateView, MakeGenericClass(Action1, Boolean), this), true)
    CShopMallMgr.OnYuanbaoMallAutoShangJiaUpdated = CommonDefs.CombineListner_Action_bool(CShopMallMgr.OnYuanbaoMallAutoShangJiaUpdated, MakeDelegateFromCSFunction(this.UpdateView2, MakeGenericClass(Action1, Boolean), this), true)
    this.m_Table.OnSelectAtRow = CommonDefs.CombineListner_Action_int(this.m_Table.OnSelectAtRow, MakeDelegateFromCSFunction(this.OnItemClick, MakeGenericClass(Action1, Int32), this), true)
    this.m_BuyCountButton.onValueChanged = CommonDefs.CombineListner_Action_uint(this.m_BuyCountButton.onValueChanged, MakeDelegateFromCSFunction(this.OnBuyCountChanged, MakeGenericClass(Action1, UInt32), this), true)
    this.m_RadioBox.OnSelect = CommonDefs.CombineListner_Action_QnButton_int(this.m_RadioBox.OnSelect, MakeDelegateFromCSFunction(this.OnRadioBoxChanged, MakeGenericClass(Action2, QnButton, Int32), this), true)
    this.m_BuyButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_BuyButton.OnClick, MakeDelegateFromCSFunction(this.OnBuyButtonClick, MakeGenericClass(Action1, QnButton), this), true)
    EventManager.AddListenerInternal(EnumEventType.PlayerBuyMallItemSuccess, MakeDelegateFromCSFunction(this.OnPlayerBuyMallItemSuccess, MakeGenericClass(Action4, Int32, UInt32, UInt32, String), this))
end
CYuanbaoShopWnd.m_OnDisable_CS2LuaHook = function (this) 
    CShopMallMgr.OnYuanbaoLimitUpdated = CommonDefs.CombineListner_Action_bool(CShopMallMgr.OnYuanbaoLimitUpdated, MakeDelegateFromCSFunction(this.UpdateView, MakeGenericClass(Action1, Boolean), this), false)
    CShopMallMgr.OnYuanbaoMallAutoShangJiaUpdated = CommonDefs.CombineListner_Action_bool(CShopMallMgr.OnYuanbaoMallAutoShangJiaUpdated, MakeDelegateFromCSFunction(this.UpdateView2, MakeGenericClass(Action1, Boolean), this), false)
    this.m_Table.OnSelectAtRow = CommonDefs.CombineListner_Action_int(this.m_Table.OnSelectAtRow, MakeDelegateFromCSFunction(this.OnItemClick, MakeGenericClass(Action1, Int32), this), false)
    this.m_BuyCountButton.onValueChanged = CommonDefs.CombineListner_Action_uint(this.m_BuyCountButton.onValueChanged, MakeDelegateFromCSFunction(this.OnBuyCountChanged, MakeGenericClass(Action1, UInt32), this), false)
    this.m_RadioBox.OnSelect = CommonDefs.CombineListner_Action_QnButton_int(this.m_RadioBox.OnSelect, MakeDelegateFromCSFunction(this.OnRadioBoxChanged, MakeGenericClass(Action2, QnButton, Int32), this), false)
    this.m_BuyButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_BuyButton.OnClick, MakeDelegateFromCSFunction(this.OnBuyButtonClick, MakeGenericClass(Action1, QnButton), this), false)
    EventManager.RemoveListenerInternal(EnumEventType.PlayerBuyMallItemSuccess, MakeDelegateFromCSFunction(this.OnPlayerBuyMallItemSuccess, MakeGenericClass(Action4, Int32, UInt32, UInt32, String), this))
end
CYuanbaoShopWnd.m_Start_CS2LuaHook = function (this) 

    this.m_Table.m_DataSource = this

    this.m_BuyCountButton:SetMinMax(1, 999, 1)
    this:RefreshVisibleCategory()

    if CShopMallMgr.SelectCategory ~= 0 then
        this.m_RadioBox:ChangeTo(CShopMallMgr.SelectCategory, true)
        local row = this:GetRowByItemId(CShopMallMgr.SearchTemplateId)
        this.CurrentSelectedItem = row
        this.m_Table:SetSelectRow(row, true)
    end
end
CYuanbaoShopWnd.m_ItemAt_CS2LuaHook = function (this, view, row) 
    local item = TypeAs(view:GetFromPool(0), typeof(CShopMallGoodsItem))
    item:UpdateData(this.m_CurrentItems[row], EnumMoneyType.YuanBao, false, false)
    item.OnClickItemTexture = DelegateFactory.Action(function () 
        view:SetSelectRow(row, true)
    end)
    return item
end
CYuanbaoShopWnd.m_OnRadioBoxChanged_CS2LuaHook = function (this, button, index) 
    this.CurrentCategory = index
    this.CurrentSelectedItem = - 1
    this:UpdateView(false)
end
CYuanbaoShopWnd.m_OnBuyButtonClick_CS2LuaHook = function (this, button) 

    if this.m_CurrentItems ~= nil and this.m_CurrentItems.Count > this.CurrentSelectedItem and this.CurrentSelectedItem >= 0 then
        local count = this.m_BuyCountButton:GetValue()
        local template = this.m_CurrentItems[this.CurrentSelectedItem]
        if this.CurrentSelectedItem >= 0 and count > 0 then
            Gac2Gas.BuyMallItem(EShopMallRegion_lua.EYuanBaoMall, template.ItemId, count)
        end
    end
end
CYuanbaoShopWnd.m_RefreshVisibleCategory_CS2LuaHook = function (this) 
    --加入等级需求
    if CClientMainPlayer.Inst ~= nil then
        local setting = Mall_Setting.GetData("XinShouTuiJian_Grade")
        local level = CClientMainPlayer.Inst.MaxLevel
        if level > math.floor(tonumber(setting.Value or 0)) then
            this.m_RadioBox:SetVisibleGroup(Table2ArrayWithCount({1, 2, 3}, 3, MakeArrayClass(System.Int32)), true)
        else
            this.m_RadioBox:SetVisibleGroup(Table2ArrayWithCount({0, 1, 2, 3}, 4, MakeArrayClass(System.Int32)), true)
        end
    end
end
CYuanbaoShopWnd.m_UpdateView2_CS2LuaHook = function (this, updatePosition) 
    this:RefreshVisibleCategory()

    local radioboxindex = CShopMallMgr.GetItemCategoryOfYuanbaoShop(CShopMallMgr.SearchTemplateId)
    if radioboxindex ~= 0 then
        this.m_RadioBox:ChangeTo(radioboxindex, true)
        local row = this:GetRowByItemId(CShopMallMgr.SearchTemplateId)
        this.CurrentSelectedItem = row
    end
    this:UpdateView(updatePosition)
end
CYuanbaoShopWnd.m_UpdateView_CS2LuaHook = function (this, updatePosition) 

    this.m_ModelPreviewer:UpdateData(nil, 0, "")
    this.m_CurrentItems = CShopMallMgr.GetYuanbaoMallInfo(this.CurrentCategory)
    this.m_Table:ReloadData(true, updatePosition)
    if this.m_CurrentItems ~= nil and this.m_CurrentItems.Count > 0 then
        this.m_BuyCountButton:SetMinMax(1, 999, 1)
        if this.m_BuyCountButton:GetValue() > 999 or this.m_BuyCountButton:GetValue() < 1 then
            this.m_BuyCountButton:SetValue(1, true)
        end
        if this.CurrentSelectedItem >= 0 and this.CurrentSelectedItem < this.m_CurrentItems.Count then
            this.m_Table:SetSelectRow(this.CurrentSelectedItem, true)
        else
            this.m_Table:SetSelectRow(0, true)
        end
    else
        this.m_BuyCountButton:SetMinMax(0, 0, 1)
        this.m_BuyCountButton:SetValue(0, true)
        this.CurrentSelectedItem = - 1
    end
    this:UpdateTotalPrice()
end
CYuanbaoShopWnd.m_UpdateTotalPrice_CS2LuaHook = function (this) 

    local totalprice = 0
    if this.m_CurrentItems ~= nil and this.m_CurrentItems.Count > this.CurrentSelectedItem and this.CurrentSelectedItem >= 0 then
        totalprice = this.m_CurrentItems[this.CurrentSelectedItem].Price * this.m_BuyCountButton:GetValue()
    end
    this.m_MoneyCtrl:SetCost(totalprice)
end
CYuanbaoShopWnd.m_UpdatePreview_CS2LuaHook = function (this, index) 
    local template = nil
    if index >= 0 and index <= this.m_CurrentItems.Count then
        template = this.m_CurrentItems[index]
        local maxCount = 999
        if CClientMainPlayer.Inst ~= nil then
            local yuanbao = CClientMainPlayer.Inst.YuanBao
            local singlePrice = math.max(1, template.Price)
            --以防价格为0报错
            maxCount = math.floor(math.floor(yuanbao / singlePrice))
            maxCount = math.min(math.max(maxCount, 1), 999)
        end
        if template.AvalibleCount >= 0 then
            local avaliableCount = math.max(1, template.AvalibleCount)
            maxCount = math.min(maxCount, avaliableCount)
            this.m_BuyCountButton:SetMinMax(1, maxCount, 1)
        else
            this.m_BuyCountButton:SetMinMax(1, maxCount, 1)
        end
    end
    this.m_ModelPreviewer:UpdateData(template, 0, "")
end
CYuanbaoShopWnd.m_GetRowByItemId_CS2LuaHook = function (this, templateId) 
    do
        local i = 0
        while i < this.m_CurrentItems.Count do
            if this.m_CurrentItems[i].ItemId == templateId then
                return i
            end
            i = i + 1
        end
    end
    return 0
end
