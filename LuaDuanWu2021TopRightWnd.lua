local UIGrid = import "UIGrid"


local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CTopAndRightTipWnd=import "L10.UI.CTopAndRightTipWnd"

LuaDuanWu2021TopRightWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaDuanWu2021TopRightWnd, "ExpandButton", "ExpandButton", GameObject)
RegistChildComponent(LuaDuanWu2021TopRightWnd, "Template", "Template", GameObject)
RegistChildComponent(LuaDuanWu2021TopRightWnd, "Grid", "Grid", UIGrid)
RegistChildComponent(LuaDuanWu2021TopRightWnd, "Bg", "Bg", GameObject)
--@endregion RegistChildComponent end
RegistClassMember(LuaDuanWu2021TopRightWnd,"m_TeamInfos")

function LuaDuanWu2021TopRightWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.ExpandButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnExpandButtonClick()
	end)

    --@endregion EventBind end
end

function LuaDuanWu2021TopRightWnd:Init()
	self.Bg:SetActive(false)
	self.Template:SetActive(false)
	self.m_TeamInfos = {}
	Extensions.RemoveAllChildren(self.Grid.transform)
	for i = 1, 5 do
		local obj = NGUITools.AddChild(self.Grid.gameObject, self.Template)
		table.insert(self.m_TeamInfos, obj)
	end
	self.Grid:Reposition()
	LuaDuanWu2021Mgr:ShowOffWorldPlay2TipWnd()
end

--@region UIEvent

function LuaDuanWu2021TopRightWnd:OnExpandButtonClick()
	LuaUtils.SetLocalRotation(self.ExpandButton.transform,0,0,0)
	CTopAndRightTipWnd.showPackage = false
	CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end

--@endregion UIEvent

function LuaDuanWu2021TopRightWnd:OnEnable()
	g_ScriptEvent:AddListener("OnSyncLongZhouPlayPlayInfo", self, "InitTeamsInfo")
end

function LuaDuanWu2021TopRightWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnSyncLongZhouPlayPlayInfo", self, "InitTeamsInfo")
end

function LuaDuanWu2021TopRightWnd:InitTeamsInfo(list,myRank)
	self.Bg:SetActive(true)
	local len = #list
	local num = len < 5 and len or 5
	for i = 1,5 do
		local obj = self.m_TeamInfos[i]
		obj:SetActive(i <= len)
		if i <= len then
			local data = list[i]
			local topThreeSprite = obj.transform:Find("TopThreeSprite"):GetComponent(typeof(CUITexture))
			topThreeSprite.gameObject:SetActive(i <= 3 and data.isArrived)
			if i <= 3 and data.isArrived then
				topThreeSprite:LoadMaterial(SafeStringFormat3("UI/Texture/Transparent/Material/grabchristmasgiftrankwnd_huangguang_0%d.mat",i))
			end
			local rankLabel = obj.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
			local nameLabel = obj.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
			rankLabel.gameObject:SetActive(i ~= 1 or (not data.isArrived))
			rankLabel.text = data.isArrived and data.rank or "-"
			nameLabel.text = data.playerName .. LocalString.GetString("队伍")
			nameLabel.color = myRank == i and Color.green or Color.white
		end
	end
	self.Grid:Reposition()
end

