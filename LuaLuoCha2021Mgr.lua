local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CScene = import "L10.Game.CScene"
local CForcesMgr = import "L10.Game.CForcesMgr"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CClientNpc = import "L10.Game.CClientNpc"
local CEffectMgr = import "L10.Game.CEffectMgr"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local CClientOtherPlayer = import "L10.Game.CClientOtherPlayer"

LuaLuoCha2021Mgr = {}
LuaLuoCha2021Mgr.m_Name = nil
LuaLuoCha2021Mgr.m_Id = nil
LuaLuoCha2021Mgr.m_Class = nil
LuaLuoCha2021Mgr.m_Gender = nil
LuaLuoCha2021Mgr.m_Level = nil
LuaLuoCha2021Mgr.m_ServerName = nil
LuaLuoCha2021Mgr.m_IsLuoCha = nil
LuaLuoCha2021Mgr.m_IsWinner = nil
LuaLuoCha2021Mgr.m_LastTime = nil
LuaLuoCha2021Mgr.m_ResultCount = nil
LuaLuoCha2021Mgr.m_ResultTotalCount = nil
LuaLuoCha2021Mgr.m_ExtraInfo = nil
LuaLuoCha2021Mgr.m_IsSignUp = nil
LuaLuoCha2021Mgr.m_FailReward = nil
LuaLuoCha2021Mgr.m_RenLeiSuccessReward = nil
LuaLuoCha2021Mgr.m_LuoChaSuccessReward = nil
LuaLuoCha2021Mgr.m_SkillInfo = nil
LuaLuoCha2021Mgr.m_LearnSkillCount = 0

-- 界面信息同步
-- 全局
LuaLuoCha2021Mgr.m_SumPickCount = 0
LuaLuoCha2021Mgr.m_Level = 0
-- 人类
LuaLuoCha2021Mgr.m_RenLeiAliveCount = 0
LuaLuoCha2021Mgr.m_RenLeiCurrentPickCount = 0
LuaLuoCha2021Mgr.m_RenLeiMaxPickCount = 2
LuaLuoCha2021Mgr.m_RenLeiUpgradeNeedPickCount = 8
-- 罗刹
LuaLuoCha2021Mgr.m_LuoChaPower = 0
LuaLuoCha2021Mgr.m_LuoChaUpgradeNeedPower = 8

function LuaLuoCha2021Mgr:IsInGamePlay()
    if CScene.MainScene then
        local gamePlayId=CScene.MainScene.GamePlayDesignId
        if gamePlayId == 51102271 then
            return true
        end
    end
    return false
end

function LuaLuoCha2021Mgr:IsInTask()
    if CScene.MainScene then
        local gamePlayId=CScene.MainScene.GamePlayDesignId
        if gamePlayId == 51102268 then
            return true
        end
    end
    return false
end

function LuaLuoCha2021Mgr:SetSceneMode()
	if CScene.MainScene then
        local gamePlayId=CScene.MainScene.GamePlayDesignId
        if gamePlayId == LiuYi2021_Setting.GetData().GameplayId then
            CScene.MainScene.ShowLeavePlayButton = true
            CScene.MainScene.ShowTimeCountDown = true
            CScene.MainScene.ShowMonsterCountDown = false
        end
    end
	if CClientMainPlayer.Inst then
        LuaLuoCha2021Mgr.m_Name = CClientMainPlayer.Inst.Name
        LuaLuoCha2021Mgr.m_Id = CClientMainPlayer.Inst.Id
        LuaLuoCha2021Mgr.m_Class = CClientMainPlayer.Inst.Class
        LuaLuoCha2021Mgr.m_Gender = CClientMainPlayer.Inst.Gender
        LuaLuoCha2021Mgr.m_Level = CClientMainPlayer.Inst.Level
        LuaLuoCha2021Mgr.m_ServerName = CClientMainPlayer.Inst:GetMyServerName()
    end
end

function LuaLuoCha2021Mgr:ShowResultWnd(isLuoCha, isWinner, lastTime, resultCount, extraInfo, resultTotalCount, name)
	self.m_IsLuoCha = isLuoCha
	self.m_IsWinner = isWinner
	self.m_LastTime = lastTime
	self.m_ResultCount = resultCount
	self.m_ExtraInfo = extraInfo
    self.m_ResultTotalCount = resultTotalCount
    self.m_Name = name
    if self:IsInGamePlay() then
        if (isLuoCha and isWinner) or ((not isLuoCha) and (not isWinner)) then
            self:SetLuoChaCarmera()
        else
            self:SetRenLeiCarmera()
        end
    end

    -- 界面延迟打开
    RegisterTickOnce(function()
        CUIManager.ShowUI(CLuaUIResources.LuoCha2021ResultWnd)
    end, 7777)
end

function LuaLuoCha2021Mgr:ShowSignUpWnd(ibSignUp, failedRewardTimes, playerSuccessRewardTimes, luoChaSuccessRewardTimes)
	self.m_IsSignUp = ibSignUp
	self.m_FailReward = failedRewardTimes
	self.m_RenLeiSuccessReward = playerSuccessRewardTimes
	self.m_LuoChaSuccessReward = luoChaSuccessRewardTimes
	CUIManager.ShowUI(CLuaUIResources.LuoCha2021EnterWnd)
end

function LuaLuoCha2021Mgr:IsLuoChaPlayer()
	if CForcesMgr.Inst and CClientMainPlayer.Inst then
        -- 罗刹是1 人类是0
        return CForcesMgr.Inst:GetPlayerForce(CClientMainPlayer.Inst.Id) == 1
    end
    return false
end

function LuaLuoCha2021Mgr:CacheLearnSkillInfo(skillInfo)
	self.m_SkillInfo = skillInfo
end

-- 清数据
function LuaLuoCha2021Mgr:ClearCacheInfo()
	self.m_SumPickCount = 0
    self.m_Level = 0
    -- 人类
    self.m_RenLeiAliveCount = 0
    self.m_RenLeiCurrentPickCount = 0
    self.m_RenLeiMaxPickCount = 2
    self.m_RenLeiUpgradeNeedPickCount = 8
    -- 罗刹
    self.m_LuoChaPower = 0
    self.m_LuoChaUpgradeNeedPower = 8
    self.m_LearnSkillCount = 0
end

-- 尝试改变炮台特效
function LuaLuoCha2021Mgr:CheckFortFx(sumPickInfo)
    if self.m_SumPickCount < 20 and sumPickInfo >= 20 then
        local data = LuoChaHaiShi_Setting.GetData()
        local fortNpcId = data.FortNpc[0]
        local newfxid = data.FortStateFx[1]
        local oldfxid = data.FortStateFx[0]

        CClientObjectMgr.Inst:ForeachObject(DelegateFactory.Action_CClientObject(function(obj)
            if TypeIs(obj, typeof(CClientNpc)) then
                if obj and obj.RO and obj.TemplateId == fortNpcId then
                    -- 删掉旧的
                    local ofx = obj.RO:GetFxById(oldfxid)
                    if ofx~=nil then
                        ofx:Destroy()
                    end
                    -- 添加新的
                    CEffectMgr.Inst:AddObjectFX(newfxid, obj.RO, 0, 1, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
                end
            end
        end))
    end 
end

-- 人类玩家胜利之后的相机设置
function LuaLuoCha2021Mgr:SetRenLeiCarmera()
    local data = LuoChaHaiShi_Setting.GetData()

    local expressionId = data.PlayerEndingExpression
    local fortNpcId = data.FortNpc[0]
    local cameraParam = data.FortCameraParam
    if CameraFollow.Inst then
        CameraFollow.Inst:EndAICamera()
        CameraFollow.Inst:BeginAICamera(Vector3(0, 0, 0), Vector3(0, 0, 0), 3600, false)
        CameraFollow.Inst:AICameraMoveTo(
                Vector3(cameraParam[0], cameraParam[1], cameraParam[2]),
                Vector3(cameraParam[3], cameraParam[4], cameraParam[5]), 1)
    end

    RegisterTickOnce(function()
        if CClientObjectMgr.Inst then
            CClientObjectMgr.Inst:ForeachObject(DelegateFactory.Action_CClientObject(function(obj)
                if TypeIs(obj, typeof(CClientNpc)) then
                    if obj and obj.RO and obj.TemplateId == fortNpcId then
                        obj:ShowExpressionActionState(expressionId)
                    end
                end
            end))
        end
    end, 1000)
end

-- 罗刹玩家胜利之后的相机设置
function LuaLuoCha2021Mgr:SetLuoChaCarmera()
    local expressionId = LuoChaHaiShi_Setting.GetData().LuoChaEndingExpression
    local target = nil
    CClientObjectMgr.Inst:ForeachObject(DelegateFactory.Action_CClientObject(function(obj)
        if CForcesMgr.Inst then
            if TypeIs(obj, typeof(CClientMainPlayer)) and CForcesMgr.Inst:GetPlayerForce(CClientMainPlayer.Inst.Id) == 1 then
                target = obj
            elseif TypeIs(obj, typeof(CClientOtherPlayer)) and CForcesMgr.Inst:GetPlayerForce(obj.PlayerId) == 1 then
                target = obj
            end
        end
    end))

    if target and CameraFollow.Inst then
        CameraFollow.Inst:EndAICamera()
        CameraFollow.Inst:BeginAICamera(Vector3(0, 0, 0), Vector3(0, 0, 0), 3600, false)
        local pos = target.RO.transform.position
		-- 当前的场景相机
		local rotate = CameraFollow.Inst.rotateObj
		CameraFollow.Inst:BeginAICamera(rotate.position, rotate.eulerAngles, 3600, false)
		-- 计算新的相机位置
		local ea = rotate.eulerAngles
		local d = CommonDefs.op_Multiply_Vector3_Single(rotate.forward, -20)
		local pos = CommonDefs.op_Addition_Vector3_Vector3(pos, d)
		CameraFollow.Inst:AICameraMoveTo(pos, ea, 1)
        target:ShowExpressionActionState(expressionId)
    end
end
