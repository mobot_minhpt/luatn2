local CUITexture = import "L10.UI.CUITexture"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local Extensions = import "Extensions"
local CButton = import "L10.UI.CButton"
local EnumGender = import "L10.Game.EnumGender"
local UISprite = import "UISprite"
local LuaTweenUtils = import "LuaTweenUtils"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"

LuaAppearanceRanFaJiSubview = class()

RegistChildComponent(LuaAppearanceRanFaJiSubview,"m_RanFaJiItem", "CommonItemCell", GameObject)
RegistChildComponent(LuaAppearanceRanFaJiSubview,"m_ItemDisplay", "AppearanceCommonItemDisplayView", CCommonLuaScript)
RegistChildComponent(LuaAppearanceRanFaJiSubview,"m_ContentGrid", "ContentGrid", UIGrid)
RegistChildComponent(LuaAppearanceRanFaJiSubview,"m_ContentScrollView", "ContentScrollView", UIScrollView)

function LuaAppearanceRanFaJiSubview:Awake()
    self.m_SelectedDataId = 0
end

function LuaAppearanceRanFaJiSubview:Init()
    Extensions.RemoveAllChildren(self.m_ContentGrid.transform)
    CLuaRanFaMgr:RequestSyncRanFaJiPlayData(EnumSyncRanFaJiType.eDefault)
end

function LuaAppearanceRanFaJiSubview:LoadData()
    self.m_AllRanFaJis = LuaAppearancePreviewMgr:GetAllRanFaJiInfo(false)

    Extensions.RemoveAllChildren(self.m_ContentGrid.transform)
    self.m_ContentGrid.gameObject:SetActive(true)

    for i = 1, # self.m_AllRanFaJis do
        local child = CUICommonDef.AddChild(self.m_ContentGrid.gameObject, self.m_RanFaJiItem)
        child:SetActive(true)
        self:InitItem(child, self.m_AllRanFaJis[i])
    end
    self.m_ContentGrid:Reposition()
    self.m_ContentScrollView:ResetPosition()
    self:InitSelection()
end

function LuaAppearanceRanFaJiSubview:SetDefaultSelection()
    if self.m_SelectedDataId == 0 then
        self.m_SelectedDataId = LuaAppearancePreviewMgr:GetCurrentInUseRanFaJi()
    end
end

function LuaAppearanceRanFaJiSubview:InitSelection()
    if self.m_SelectedDataId == 0 then
        self:OnItemClick(nil)
        return
    end

    local childCount = self.m_ContentGrid.transform.childCount
    for i=0, childCount-1 do
        local childGo = self.m_ContentGrid.transform:GetChild(i).gameObject
        local appearanceData = self.m_AllRanFaJis[i+1]
        if appearanceData.id == self.m_SelectedDataId then
            self:OnItemClick(childGo)
            break
        end
    end
end

function LuaAppearanceRanFaJiSubview:InitItem(itemGo, appearanceData)
    local iconTexture = itemGo.transform:GetComponent(typeof(CUITexture))
    local disabledGo = itemGo.transform:Find("Disabled").gameObject
    local lockedGo = itemGo.transform:Find("Locked").gameObject
    local cornerGo = itemGo.transform:Find("Corner").gameObject
    local selectedGo = itemGo.transform:Find("Selected").gameObject
    iconTexture:LoadMaterial(appearanceData.icon)
    
    local exist = LuaAppearancePreviewMgr:MainPlayerHasRanFaJi(appearanceData.id)
    local existRecipe = LuaAppearancePreviewMgr:MainPlayerHasRanFaJiRecipe(appearanceData.id)
    local inUse = LuaAppearancePreviewMgr:IsCurrentInUseRanFaJi(appearanceData.id)
    disabledGo:SetActive(not exist)
    lockedGo:SetActive(not exist and not existRecipe)
    cornerGo:SetActive(inUse)
    selectedGo:SetActive(self.m_SelectedDataId and self.m_SelectedDataId==appearanceData.id)
    UIEventListener.Get(itemGo).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnItemClick(go)
    end)
end

function LuaAppearanceRanFaJiSubview:OnItemClick(go)
    local childCount = self.m_ContentGrid.transform.childCount
    for i=0, childCount-1 do
        local childGo = self.m_ContentGrid.transform:GetChild(i).gameObject
        if childGo == go then
            childGo.transform:Find("Selected").gameObject:SetActive(true)
            self.m_SelectedDataId = self.m_AllRanFaJis[i+1].id
        else
            childGo.transform:Find("Selected").gameObject:SetActive(false)
        end
    end
    self:UpdateItemDisplay()
    g_ScriptEvent:BroadcastInLua("RequestPreviewMainPlayerRanFaJi", self.m_SelectedDataId)
end

function LuaAppearanceRanFaJiSubview:UpdateItemDisplay()
    self.m_ItemDisplay.gameObject:SetActive(true)
    local id = self.m_SelectedDataId and self.m_SelectedDataId or 0
    if id==0 then
        self.m_ItemDisplay:Clear()
        return
    end
    local appearanceData = nil
    for i=1,#self.m_AllRanFaJis do
        if self.m_AllRanFaJis[i].id == id then
            appearanceData = self.m_AllRanFaJis[i]
            break
        end
    end
    
    local icon = appearanceData.icon
    local disabled = true
    local locked = true
    local desc = ""
    local buttonTbl = {}
    local exist = LuaAppearancePreviewMgr:MainPlayerHasRanFaJi(appearanceData.id)
    local existRecipe = LuaAppearancePreviewMgr:MainPlayerHasRanFaJiRecipe(appearanceData.id)
    local inUse = LuaAppearancePreviewMgr:IsCurrentInUseRanFaJi(appearanceData.id)
    locked = not exist and not existRecipe
    disabled = not exist
    if exist then
        if inUse then
            desc = LocalString.GetString("已染色")
            table.insert(buttonTbl, {text=LocalString.GetString("褪色"), isYellow=false, action=function(go) self:OnRemoveButtonClick() end})
        else
            desc = LocalString.GetString("已获得")
            table.insert(buttonTbl, {text=LocalString.GetString("染色"), isYellow=true, action=function(go) self:OnApplyButtonClick() end})
        end
    else
        if existRecipe then
            desc = LocalString.GetString("已习得、未获得")
        else
            desc = LocalString.GetString("未习得")
        end
        table.insert(buttonTbl, {text=LocalString.GetString("获取"), isYellow=true, action=function(go) self:OnMoreButtonClick(go) end})
    end

    self.m_ItemDisplay:Init(icon, appearanceData.name, desc, SafeStringFormat3(LocalString.GetString("风尚度+%d"), appearanceData.fashionability), true, disabled, locked, buttonTbl)
end

function LuaAppearanceRanFaJiSubview:OnApplyButtonClick()
    local exist = LuaAppearancePreviewMgr:MainPlayerHasRanFaJi(self.m_SelectedDataId)
    if exist then
        LuaAppearancePreviewMgr:RequestTakeOnRanFaJi(self.m_SelectedDataId)
    end
end

function LuaAppearanceRanFaJiSubview:OnRemoveButtonClick()
    LuaAppearancePreviewMgr:RequestTakeOffRanFaJi(self.m_SelectedDataId)
end

function LuaAppearanceRanFaJiSubview:OnMoreButtonClick(go)
    LuaAppearancePreviewMgr:RequestOpenRanFaJiLibrary()
end

function  LuaAppearanceRanFaJiSubview:OnEnable()
    g_ScriptEvent:AddListener("OnSyncRanFaJiPlayData", self, "OnSyncRanFaJiPlayData")
end

function  LuaAppearanceRanFaJiSubview:OnDisable()
    g_ScriptEvent:RemoveListener("OnSyncRanFaJiPlayData", self, "OnSyncRanFaJiPlayData")
end

function LuaAppearanceRanFaJiSubview:OnSyncRanFaJiPlayData(...)
    self:SetDefaultSelection()
    self:LoadData()
end
