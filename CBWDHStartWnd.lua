-- Auto Generated!!
local CBiWuDaHuiMgr = import "L10.Game.CBiWuDaHuiMgr"
local CBWDHStartWnd = import "L10.UI.CBWDHStartWnd"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local DelegateFactory = import "DelegateFactory"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CBWDHStartWnd.m_Init_CS2LuaHook = function (this) 
    this.descLabel.text = g_MessageMgr:FormatMessage("BIWU_MAIN_INTERFACE")
    this.scrollView:ResetPosition()

    Gac2Gas.QueryBiWuSignUpInfo()
    this.applyLabel.text = CBWDHStartWnd.Str_Apply

    if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.IsInGamePlay then
        --副本中不可点击
        CUICommonDef.SetActive(this.watchBtn, false, true)
        CUICommonDef.SetActive(this.prepareBtn, false, true)
    end
    --在准备场景和比武场景 不可点击
    if CBiWuDaHuiMgr.Inst.inBiWu or CBiWuDaHuiMgr.Inst.inBiWuPrepare then
        CUICommonDef.SetActive(this.watchBtn, false, true)
        CUICommonDef.SetActive(this.prepareBtn, false, true)
    end
end
CBWDHStartWnd.m_OnQueryBiWuSignUpInfoResult_CS2LuaHook = function (this, result) 
    if result then
        this.applyLabel.text = CBWDHStartWnd.Str_HaveApply
        CUICommonDef.SetActive(this.applyBtn, false, true)
    else
        this.applyLabel.text = CBWDHStartWnd.Str_Apply
        CUICommonDef.SetActive(this.applyBtn, true, true)
    end
end
CBWDHStartWnd.m_Awake_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeButton).onClick = MakeDelegateFromCSFunction(this.OnClickCloseButton, VoidDelegate, this)
    UIEventListener.Get(this.watchBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        CBiWuDaHuiMgr.Inst.QMJJWatch = false
        CUIManager.ShowUI(CUIResources.BWDHWatchWnd)
    end)
    UIEventListener.Get(this.prepareBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        --CUIManager.ShowUI(CUIResources.);
        --
        --点击进入场地，如果没有报名则提示你没有报名。
        --时间判断 如果时间没到，则提示：还没到报名时间，请在每月第一个星期六12：00至周日13：00间前来报名


        Gac2Gas.RequestEnterBiWuPrepareScene()
    end)
    UIEventListener.Get(this.applyBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        --服务器控制逻辑
        --报名
        Gac2Gas.RequestSignUpForBiWu()
        --//点击报名需要花费银两，银两不足则提示需要xxx银两才能报名。
        --if (CClientMainPlayer.Inst!=null)
        --{
        --    uint level = CClientMainPlayer.Inst.Level;
        --    if (level>=50)
        --    {
        --        ulong silver = CClientMainPlayer.Inst.FreeSilver + CClientMainPlayer.Inst.Silver;
        --        ulong needSilver = level * 100;
        --        if (silver>needSilver)
        --        {
        --            //报名
        --            Gac2Gas.RequestSignUpForBiWu();
        --        }
        --        else
        --        {
        --            //银两不够
        --        }
        --    }
        --    else
        --    {
        --        //等级不够
        --    }
        --}
    end)
end
