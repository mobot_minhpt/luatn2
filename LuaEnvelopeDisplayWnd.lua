require("common/common_include")

local CBaseWnd = import "L10.UI.CBaseWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CUIManager = import "L10.UI.CUIManager"
local UITweener = import "UITweener"
local TweenAlpha = import "TweenAlpha"
local CItemMgr = import "L10.Game.CItemMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"

LuaEnvelopeDisplayWnd = class()

RegistClassMember(LuaEnvelopeDisplayWnd,"m_CloseBtn")
RegistClassMember(LuaEnvelopeDisplayWnd,"m_EnvelopeGo")
RegistClassMember(LuaEnvelopeDisplayWnd,"m_FadeOutTweener")
RegistClassMember(LuaEnvelopeDisplayWnd,"m_FadeOutPaperTweener")

function LuaEnvelopeDisplayWnd:Awake()

end

function LuaEnvelopeDisplayWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end	

function LuaEnvelopeDisplayWnd:Init()
	self.m_CloseBtn = self.transform:Find("Anchor/CloseButton").gameObject
	self.m_EnvelopeGo = self.transform:Find("Anchor/BagTexture").gameObject

	if not self.m_FadeOutTweener then
		self.m_FadeOutTweener = self.transform:Find("Anchor"):GetComponent(typeof(UITweener))
		self.m_FadeOutTweener:ResetToBeginning()
		self.m_FadeOutTweener.enabled = false
		self.m_FadeOutTweener:AddOnFinished(DelegateFactory.Callback(function()
			self:OnFadeOutFinish()
		end))

		self.m_FadeOutPaperTweener = self.transform:Find("Anchor/BagTexture/Rot/PaperTexture"):GetComponent(typeof(TweenAlpha))
		self.m_FadeOutPaperTweener:ResetToBeginning()
		self.m_FadeOutPaperTweener.enabled = false
	end


	
	CommonDefs.AddOnClickListener(self.m_CloseBtn, DelegateFactory.Action_GameObject(function(go) 
		self:OnCloseButtonClick() 
	end), false)

	CommonDefs.AddOnClickListener(self.m_EnvelopeGo, DelegateFactory.Action_GameObject(function(go) 
		self:OnEnvelopeClick() 
	end), false)

	if CLuaLetterDisplayMgr.ItemIdForLetterDisplay then
		local pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Task, CLuaLetterDisplayMgr.ItemIdForLetterDisplay)
		self.m_CloseBtn:SetActive(pos<=0) -- 非任务栏的物品才显示关闭按钮，任务栏的物品隐藏
	end
end

function LuaEnvelopeDisplayWnd:OnFadeOutFinish()
	self:Close()
	CUIManager.ShowUI(CLuaLetterDisplayMgr.LetterWnd)
end

function LuaEnvelopeDisplayWnd:OnCloseButtonClick()
	self:Close()
end

function LuaEnvelopeDisplayWnd:OnEnvelopeClick()
	if self.m_FadeOutTweener then
		self.m_FadeOutTweener:ResetToBeginning()
		self.m_FadeOutTweener.enabled = true
		self.m_FadeOutPaperTweener:ResetToBeginning()
		self.m_FadeOutPaperTweener.enabled = true
	else
		self:OnFadeOutFinish()
	end
end

