local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local EnumClass = import "L10.Game.EnumClass"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local AlignType3 = import "CPlayerInfoMgr+AlignType"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local Profession = import "L10.Game.Profession"
local QnButton = import "L10.UI.QnButton"

LuaGuildLeagueCrossAidEvaluationWnd = class()

RegistChildComponent(LuaGuildLeagueCrossAidEvaluationWnd, "m_ScrollView", "ScrollView", UIScrollView)
RegistChildComponent(LuaGuildLeagueCrossAidEvaluationWnd, "m_Table", "Table", UIGrid)
RegistChildComponent(LuaGuildLeagueCrossAidEvaluationWnd, "m_Item", "Item", GameObject)
RegistChildComponent(LuaGuildLeagueCrossAidEvaluationWnd, "m_NoneLabel", "NoneLabel", GameObject)
RegistChildComponent(LuaGuildLeagueCrossAidEvaluationWnd, "m_Button", "Button", GameObject)


function LuaGuildLeagueCrossAidEvaluationWnd:Awake()
    self.m_Item:SetActive(false)
    UIEventListener.Get(self.m_Button.gameObject).onClick = DelegateFactory.VoidDelegate(function() self:OnExtenalAidButtonClick() end)
end

function LuaGuildLeagueCrossAidEvaluationWnd:Init()
    Extensions.RemoveAllChildren(self.m_Table.transform)
    
    for index, data in pairs(LuaGuildLeagueCrossMgr.m_AidEvaluationInfo) do
        local child = CUICommonDef.AddChild(self.m_Table.gameObject, self.m_Item)
        child:SetActive(true)
        child.transform:Find("Icon"):GetComponent(typeof(UISprite)).spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), data.playerClass))
        child.transform:Find("NameLabel"):GetComponent(typeof(UILabel)).text = data.playerName
        child.transform:Find("TypeLabel"):GetComponent(typeof(UILabel)).text = data.aidType ~= EnumGuildForeignAidApplyInfoType.eElite and LocalString.GetString("普通") or "[fffe91]"..LocalString.GetString("精英")
        local levelRoot = child.transform:Find("Levels")
        local childCnt = levelRoot.childCount
        local levels = data.levels
        for i=0,childCnt-1 do
            local levelChild = levelRoot:GetChild(i)
            if i+1<=#levels then
                levelChild.gameObject:SetActive(true)
                --服务器端复用的帮赛公式，这里客户端也复用一下联赛里面的评价处理
                CLuaGuildMgr.SetLeagueCommentLevel(levelChild:GetComponent(typeof(UILabel)),levels[i+1])
            else
                levelChild.gameObject:SetActive(false)
            end
        end
        child.transform:GetComponent(typeof(QnButton)):SetBackgroundTexture((index-1) % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite)
        local playerId = data.playerId
        UIEventListener.Get(child.gameObject).onClick = DelegateFactory.VoidDelegate(function() self:OnPlayerItemClick(playerId) end)
    end
    self.m_Table:Reposition()
    self.m_ScrollView:ResetPosition()
    self.m_NoneLabel:SetActive(#LuaGuildLeagueCrossMgr.m_AidEvaluationInfo==0)
end

function LuaGuildLeagueCrossAidEvaluationWnd:OnExtenalAidButtonClick()
    LuaGuildExternalAidMgr:ShowGuildExternalAidWnd(EnumExternalAidGamePlay.eGuildLeagueCross, 0)
end

function LuaGuildLeagueCrossAidEvaluationWnd:OnPlayerItemClick(playerId)
		CPlayerInfoMgr.ShowPlayerPopupMenu(playerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType3.Default)
end

