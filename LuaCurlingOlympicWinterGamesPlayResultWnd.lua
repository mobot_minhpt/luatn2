local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"

LuaCurlingOlympicWinterGamesPlayResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaCurlingOlympicWinterGamesPlayResultWnd, "Button", "Button", GameObject)
RegistChildComponent(LuaCurlingOlympicWinterGamesPlayResultWnd, "RankLabel", "RankLabel", UILabel)
RegistChildComponent(LuaCurlingOlympicWinterGamesPlayResultWnd, "DistResultLabel", "DistResultLabel", UILabel)

--@endregion RegistChildComponent end

function LuaCurlingOlympicWinterGamesPlayResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.Button.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnButtonClick()
	end)


    --@endregion EventBind end
end

function LuaCurlingOlympicWinterGamesPlayResultWnd:Init()
	local distArr = {0, 0, 0}
	for j = 1, #LuaOlympicGamesMgr.m_RankResult do 
		local t = LuaOlympicGamesMgr.m_RankResult[j]
		if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == t.Id then
			self.RankLabel.text = j
			for i = 1, 3 do
				if t.Rounds then
					if t.Rounds[i] and t.Rounds[i].Dist then
						distArr[i] = t.Rounds[i].Dist
					end
				end
			end
		end
	end
	self.DistResultLabel.text = LocalString.GetString("无效")
	local v = 999
	for i = 1,3 do
		local value = distArr[i]/64
		if value > 0 then
			v = math.min(v, value)
		end
	end
	if v < 999 then
		self.DistResultLabel.text = SafeStringFormat3(LocalString.GetString("%.1f米"),v) 
	end
end

--@region UIEvent

function LuaCurlingOlympicWinterGamesPlayResultWnd:OnButtonClick()
	CUIManager.ShowUI(CLuaUIResources.CurlingOlympicWinterGamesRankWnd)
end

--@endregion UIEvent

