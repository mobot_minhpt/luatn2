-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CLoginMgr = import "L10.Game.CLoginMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CQingQiuQiYuWnd = import "L10.UI.CQingQiuQiYuWnd"
local CTickMgr = import "L10.Engine.CTickMgr"
local ETickType = import "L10.Engine.ETickType"
local StringBuilder = import "System.Text.StringBuilder"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local WWW = import "UnityEngine.WWW"
CQingQiuQiYuWnd.m_Awake_CS2LuaHook = function (this) 
    this.m_ContentObj:SetActive(false)
    this.m_ShareObj:SetActive(false)
    this.m_TipObj:SetActive(false)
    this.m_YangYangTex.gameObject:SetActive(false)
    UIEventListener.Get(this.m_ShareObj).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_ShareObj).onClick, MakeDelegateFromCSFunction(this.OnShareButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.m_YangYangTex.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_YangYangTex.gameObject).onClick, MakeDelegateFromCSFunction(this.OnLetterButtonClick, VoidDelegate, this), true)
end
CQingQiuQiYuWnd.m_Init_CS2LuaHook = function (this) 
    this.m_CurNum = 0
    this.m_Number = 0
    this.m_StartFx:LoadFx(CQingQiuQiYuWnd.s_StartFxPath)
    CTickMgr.Register(MakeDelegateFromCSFunction(this.StartShowNum, Action0, this), math.floor((CQingQiuQiYuWnd.s_StartAnimDuration * 1000)), ETickType.Once)
end
CQingQiuQiYuWnd.m_GetQingQiuQiYuParams_CS2LuaHook = function (this, rank) 
    local builder = NewStringBuilderWraper(StringBuilder)
    builder:Append("?")
    local gameServer = CLoginMgr.Inst:GetSelectedGameServer()
    if gameServer ~= nil then
        builder:AppendFormat("servername={0}&", WWW.EscapeURL(gameServer.name))
    end

    if CClientMainPlayer.Inst ~= nil then
        builder:AppendFormat("roleid={0}&rolename={1}&", CClientMainPlayer.Inst.Id, WWW.EscapeURL(CClientMainPlayer.Inst.Name))
    end
    builder:AppendFormat("rank={0}", rank)

    return ToStringWrap(builder)
end
