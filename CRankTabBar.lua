-- Auto Generated!!
local CommonDefs = import "L10.Game.CommonDefs"
local CRankTabBar = import "L10.UI.CRankTabBar"
local Extensions = import "Extensions"
local L10 = import "L10"
local NGUITools = import "NGUITools"
local Object = import "System.Object"
local QnButton = import "L10.UI.QnButton"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local UILabel = import "UILabel"
local Vector3 = import "UnityEngine.Vector3"
local Rank_Activity = import "L10.Game.Rank_Activity"
CRankTabBar.m_UpdateTabs_CS2LuaHook = function (this, tabitem) 
    this.tabItem = tabitem
    Extensions.RemoveAllChildren(this.grid.transform)

    local tip = this.transform:Find("Tip"):GetComponent(typeof(UILabel))
    local curRankId = this.tabItem[0].rankId
	if Rank_Activity.Exists(curRankId) then
		tip.text = Rank_Activity.GetData(curRankId).Tip
		tip.gameObject:SetActive(true)
	else
		tip.gameObject:SetActive(false)
	end
	
    this.TabName = CreateFromClass(MakeArrayClass(System.String), tabitem.Count)
    do
        local i = 0
        while i < tabitem.Count do
            this.TabName[i] = tabitem[i].tabName
            i = i + 1
        end
    end

    this.Tabs = CreateFromClass(MakeArrayClass(QnSelectableButton), this.TabName.Length)
    do
        local i = 0
        while i < this.TabName.Length do
            local TabUnit = NGUITools.AddChild(this.grid.gameObject, this.tabTemplate)
            CommonDefs.GetComponent_Component_Type(TabUnit.transform:GetChild(0), typeof(UILabel)).text = this.TabName[i]
            TabUnit.transform.localEulerAngles = Vector3(0, 0, 0)
            TabUnit.transform.localPosition = Vector3(0, 34, 0)
            TabUnit:SetActive(true)
            this.Tabs[i] = CommonDefs.GetComponent_GameObject_Type(TabUnit, typeof(QnSelectableButton))
            this.Tabs[i].OnClick = CommonDefs.CombineListner_Action_QnButton(this.Tabs[i].OnClick, MakeDelegateFromCSFunction(this.onTabSelected, MakeGenericClass(Action1, QnButton), this), true)
            if i == 0 then
                (TypeAs(this.Tabs[i], typeof(QnSelectableButton))):SetSelected(true, false)
                if this.onTabBarClick ~= nil then
                    GenericDelegateInvoke(this.onTabBarClick, Table2ArrayWithCount({tabitem[i]}, 1, MakeArrayClass(Object)))
                end
            end
            i = i + 1
        end
    end
    this.grid:Reposition()
    this.scrollView:ResetPosition()

    L10.Game.Gac2Gas.QueryRank(curRankId)
end
CRankTabBar.m_onTabSelected_CS2LuaHook = function (this, go) 
    do
        local i = 0
        while i < this.Tabs.Length do
            (TypeAs(this.Tabs[i], typeof(QnSelectableButton))):SetSelected(this.Tabs[i] == go, false)
            if this.Tabs[i] == go then
                if this.onTabBarClick ~= nil then
                    GenericDelegateInvoke(this.onTabBarClick, Table2ArrayWithCount({this.tabItem[i]}, 1, MakeArrayClass(Object)))
                end
            end
            i = i + 1
        end
    end
end
