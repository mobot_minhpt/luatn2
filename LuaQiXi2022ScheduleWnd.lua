local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local UIGrid = import "UIGrid"
local CButton = import "L10.UI.CButton"
local CTrackMgr = import "L10.Game.CTrackMgr"
local CShopMallMgr = import "L10.UI.CShopMallMgr"

LuaQiXi2022ScheduleWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaQiXi2022ScheduleWnd, "TimeLabel", "TimeLabel", UILabel)
RegistChildComponent(LuaQiXi2022ScheduleWnd, "ItemTemplate", "ItemTemplate", GameObject)
RegistChildComponent(LuaQiXi2022ScheduleWnd, "ItemsGrid", "ItemsGrid", UIGrid)
RegistChildComponent(LuaQiXi2022ScheduleWnd, "ShangJiaButton", "ShangJiaButton", CButton)
RegistChildComponent(LuaQiXi2022ScheduleWnd, "GiftButton", "GiftButton", CButton)
RegistChildComponent(LuaQiXi2022ScheduleWnd, "PlantTreeButton", "PlantTreeButton", CButton)
RegistChildComponent(LuaQiXi2022ScheduleWnd, "FindNpcButton", "FindNpcButton", CButton)
RegistChildComponent(LuaQiXi2022ScheduleWnd, "PlantTreeLabel", "PlantTreeLabel", UILabel)
RegistChildComponent(LuaQiXi2022ScheduleWnd, "FindNpcLabel", "FindNpcLabel", UILabel)
RegistChildComponent(LuaQiXi2022ScheduleWnd, "FGFx", "FGFx", CUIFx)
RegistChildComponent(LuaQiXi2022ScheduleWnd, "FGFx2", "FGFx2", CUIFx)
RegistChildComponent(LuaQiXi2022ScheduleWnd, "BGFx", "BGFx", CUIFx)
RegistChildComponent(LuaQiXi2022ScheduleWnd, "ShangJiaButtonFx", "ShangJiaButtonFx", CUIFx)
RegistChildComponent(LuaQiXi2022ScheduleWnd, "GiftButtonFx", "GiftButtonFx", CUIFx)
--@endregion RegistChildComponent end
RegistClassMember(LuaQiXi2022ScheduleWnd, "m_IsOpen")
RegistClassMember(LuaQiXi2022ScheduleWnd, "m_IsOpenPlay")

function LuaQiXi2022ScheduleWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.ShangJiaButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShangJiaButtonClick()
	end)


	
	UIEventListener.Get(self.GiftButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnGiftButtonClick()
	end)


	
	UIEventListener.Get(self.PlantTreeButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnPlantTreeButtonClick()
	end)


	
	UIEventListener.Get(self.FindNpcButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnFindNpcButtonClick()
	end)


    --@endregion EventBind end
end

function LuaQiXi2022ScheduleWnd:Init()
	self.FGFx:LoadFx("fx/ui/prefab/UI_QXHD_ZhuJieMian_FG.prefab")
	self.BGFx:LoadFx("fx/ui/prefab/UI_QXHD_ZhuJieMian_BG.prefab")
	self.TimeLabel.text = g_MessageMgr:FormatMessage("QiXi2022ScheduleWnd_Time")
	self.ItemTemplate.gameObject:SetActive(false)

	if CommonDefs.IS_HMT_CLIENT then
		local label = self.ItemTemplate.transform:Find("Panel/Label"):GetComponent(typeof(UILabel))
		label.fontSize = 24

		local titleLabel = self.transform:Find("BgLable/Label"):GetComponent(typeof(UILabel))
		local titleLabel2 = self.transform:Find("BgLable/Label/Label (1)"):GetComponent(typeof(UILabel))
		titleLabel.text = LocalString.GetString("佳期良缘起")
		titleLabel2.text = LocalString.GetString("春星醉风萤")

		titleLabel2.transform.localPosition = Vector3(240,0,0)
    end

	self.PlantTreeLabel.text = LocalString.GetString("双人")
	self.FindNpcLabel.text = QiXi2022_Setting.GetData().SFEQTime
	local showAward = QiXi2022_Setting.GetData().ShowAward
	for i = 0, showAward.Length - 1 do
		local obj = NGUITools.AddChild(self.ItemsGrid.gameObject, self.ItemTemplate.gameObject)
		obj.gameObject:SetActive(true)
		self:InitItem(obj, showAward[i])
	end
	self.PlantTreeButton.transform:Find("Alert").gameObject:SetActive(false)
	self.FindNpcButton.transform:Find("Alert").gameObject:SetActive(false)
	Gac2Gas.QiXi2022_QueryActivityInfo()
	self:OnActivityPannelShowReddot()
end

function LuaQiXi2022ScheduleWnd:InitItem(obj, itemId)
	local itemData = Item_Item.GetData(itemId)
	UIEventListener.Get(obj.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    CItemInfoMgr.ShowLinkItemTemplateInfo(itemId)
	end)
	local icon = obj.transform:Find("Panel/IconPanel/IconTexture"):GetComponent(typeof(CUITexture))
	local label = obj.transform:Find("Panel/Label"):GetComponent(typeof(UILabel))
	icon:LoadMaterial(itemData.Icon)
	local index = string.find(itemData.Name,"[(]")
	label.text = index  and string.sub(itemData.Name,0,index - 1) or itemData.Name
end

function LuaQiXi2022ScheduleWnd:OnEnable()
	g_ScriptEvent:AddListener("QiXi2022_SyncActivityInfo", self, "OnSyncActivityInfo")
	g_ScriptEvent:AddListener("ActivityPannelShowReddot", self, "OnActivityPannelShowReddot")
end

function LuaQiXi2022ScheduleWnd:OnDisable()
	g_ScriptEvent:RemoveListener("QiXi2022_SyncActivityInfo", self, "OnSyncActivityInfo")
	g_ScriptEvent:RemoveListener("ActivityPannelShowReddot", self, "OnActivityPannelShowReddot")
end

function LuaQiXi2022ScheduleWnd:OnSyncActivityInfo(bIsOpen, bIsOpenPlay)
	self.m_IsOpen = bIsOpen
	self.m_IsOpenPlay = bIsOpenPlay
	self.PlantTreeButton.transform:Find("Lock").gameObject:SetActive(not self.m_IsOpen)
	self.PlantTreeButton.transform:Find("Normal").gameObject:SetActive(self.m_IsOpen)
	self.PlantTreeButton.label.color = NGUIText.ParseColor24(self.m_IsOpen and "C3705C" or "ffffda",0)
	self.PlantTreeLabel.color = NGUIText.ParseColor24(self.m_IsOpen and "C3705C" or "ffffda",0)
	self.ShangJiaButton.Enabled = bIsOpen
	self.GiftButton.Enabled = bIsOpen
	if self.m_IsOpen then
		self.ShangJiaButtonFx:LoadFx("fx/ui/prefab/UI_QXHD_ZhuJieMian_LiuGuang_01.prefab")
		self.GiftButtonFx:LoadFx("fx/ui/prefab/UI_QXHD_ZhuJieMian_LiuGuang_02.prefab")
	else
		self.ShangJiaButtonFx:DestroyFx()
		self.GiftButtonFx:DestroyFx()
	end
	if self.m_IsOpen then
		self.FGFx2:LoadFx("fx/ui/prefab/UI_QXHD_ZhuJieMian_FG_Left.prefab")
	else
		self.FGFx2:DestroyFx()
	end
end

function LuaQiXi2022ScheduleWnd:OnActivityPannelShowReddot()
	self.PlantTreeButton.transform:Find("Alert").gameObject:SetActive(CLuaScheduleMgr:IsShowActivityPannelReddot(QiXi2022_Setting.GetData().YYSactivityid))
	self.FindNpcButton.transform:Find("Alert").gameObject:SetActive(CLuaScheduleMgr:IsShowActivityPannelReddot(QiXi2022_Setting.GetData().SFEQactivityid))
end
--@region UIEvent

function LuaQiXi2022ScheduleWnd:OnShangJiaButtonClick()
	if not self.m_IsOpen then
		g_MessageMgr:ShowMessage("QiXi2022ScheduleWnd_Not_Open")
		return
	end
	CShopMallMgr.ShowLinyuShoppingMall(QiXi2022_Setting.GetData().MallID2)
end

function LuaQiXi2022ScheduleWnd:OnGiftButtonClick()
	if not self.m_IsOpen then
		g_MessageMgr:ShowMessage("QiXi2022ScheduleWnd_Not_Open")
		return
	end
	CShopMallMgr.ShowLinyuShoppingMall(QiXi2022_Setting.GetData().MallID1)
end

function LuaQiXi2022ScheduleWnd:OnPlantTreeButtonClick()
	if not self.m_IsOpen then
		g_MessageMgr:ShowMessage("QiXi2022ScheduleWnd_Not_Open")
		return
	end
	Gac2Gas.YYS_RequestOpenTab()
	CUIManager.ShowUI(CLuaUIResources.QiXi2022PlantTreesWnd)
end

function LuaQiXi2022ScheduleWnd:OnFindNpcButtonClick()
	CLuaScheduleMgr.selectedScheduleInfo={
		activityId = 42030096,
		taskId = Task_Schedule.GetData(42030096).TaskID[0],

		TotalTimes = 0,
		DailyTimes = 0,
		FinishedTimes = 0,
		lastingTime = 13 * 60 -1,
		ActivityTime = "",
		minute = 0,
		hour = 11,
	}
	CLuaScheduleInfoWnd.s_UseCustomInfo = false
	CUIManager.ShowUI(CLuaUIResources.ScheduleInfoWnd)	
end

--@endregion UIEvent

