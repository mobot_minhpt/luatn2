
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UIDefaultTableView=import "L10.UI.UIDefaultTableView"
local UIDefaultTableViewCell=import "L10.UI.UIDefaultTableViewCell"
local CellIndexType=import "L10.UI.UITableView+CellIndexType"
local CellIndex=import "L10.UI.UITableView+CellIndex"

local Money = import "L10.Game.Money"
local IdPartition = import "L10.Game.IdPartition"
local EquipmentTemplate_SubType = import "L10.Game.EquipmentTemplate_SubType"
local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local CItemMgr = import "L10.Game.CItemMgr"
local CItem = import "L10.Game.CItem"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CChatLinkMgr = import "CChatLinkMgr"

local CTaskMgr = import "L10.Game.CTaskMgr"
local CEquipment = import "L10.Game.CEquipment"
local QnTableView=import "L10.UI.QnTableView"
local QnAddSubAndInputButton=import "L10.UI.QnAddSubAndInputButton"
local CUITexture=import "L10.UI.CUITexture"
local CButton=import "L10.UI.CButton"
local MessageWndManager = import "L10.UI.MessageWndManager"
local DateTime = import "System.DateTime"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CItemCountUpdate = import "L10.UI.CItemCountUpdate"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CTooltip = import "L10.UI.CTooltip"
local CCommonLuaWnd = import "L10.UI.CCommonLuaWnd"
local BoxCollider = import "UnityEngine.BoxCollider"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local Screen = import "UnityEngine.Screen"
local EWeaponVisibleReason = import "L10.Engine.EWeaponVisibleReason"
local EnumYingLingState = import "L10.Game.EnumYingLingState"

LuaAppearanceShopWnd = class()

RegistChildComponent(LuaAppearanceShopWnd, "m_GroupTableView", "GroupTableView", UIDefaultTableView)
RegistChildComponent(LuaAppearanceShopWnd, "m_ItemScrollView", "ItemScrollView", UIScrollView)
RegistChildComponent(LuaAppearanceShopWnd, "m_ItemTable", "ItemTable", UITable)
RegistChildComponent(LuaAppearanceShopWnd, "m_ItemTemplate", "ItemTemplate", GameObject)

RegistChildComponent(LuaAppearanceShopWnd, "m_NameLabel", "NameLabel", UILabel)
RegistChildComponent(LuaAppearanceShopWnd, "m_LevelLabel", "LevelLabel", UILabel)
RegistChildComponent(LuaAppearanceShopWnd, "m_TypeLabel", "TypeLabel", UILabel)
RegistChildComponent(LuaAppearanceShopWnd, "m_DescLabel", "DescLabel", UILabel)
RegistChildComponent(LuaAppearanceShopWnd, "m_OwnedNumLabel", "OwnedNumLabel", UILabel)
RegistChildComponent(LuaAppearanceShopWnd, "m_DescScrollView", "DescScrollView", UIScrollView)
RegistChildComponent(LuaAppearanceShopWnd, "m_NumberInput", "NumberInput", QnAddSubAndInputButton)
RegistChildComponent(LuaAppearanceShopWnd, "m_Icon", "Icon", CUITexture)
RegistChildComponent(LuaAppearanceShopWnd, "m_BuyBtn", "BuyBtn", CButton)
RegistChildComponent(LuaAppearanceShopWnd, "m_MoneyCtrl", "MoneyCtrl", CCommonLuaScript)
RegistChildComponent(LuaAppearanceShopWnd, "m_CostItemCtrl", "CostItemCtrl", CItemCountUpdate)

RegistChildComponent(LuaAppearanceShopWnd, "m_ModelTexture", "ModelTexture", UITexture)
RegistChildComponent(LuaAppearanceShopWnd, "m_LeaderIcon", "LeaderIcon", UISprite)
RegistChildComponent(LuaAppearanceShopWnd, "m_ProfileFrame", "ProfileFrame", Transform)
RegistChildComponent(LuaAppearanceShopWnd, "m_InfoButton", "InfoButton", GameObject)

RegistClassMember(LuaAppearanceShopWnd,"m_CostItemIcon")
RegistClassMember(LuaAppearanceShopWnd,"m_CostItemMask")

RegistClassMember(LuaAppearanceShopWnd,"m_IsGlobalLimit")
RegistClassMember(LuaAppearanceShopWnd,"m_GlobalLimitInfo")
RegistClassMember(LuaAppearanceShopWnd,"m_IsOpenGlobalLimit")
RegistClassMember(LuaAppearanceShopWnd, "m_SelectedItemId")
RegistClassMember(LuaAppearanceShopWnd, "m_CurMenuInfo")
RegistClassMember(LuaAppearanceShopWnd, "m_ItemInfoTbl")

RegistClassMember(LuaAppearanceShopWnd, "m_Identifier")
RegistClassMember(LuaAppearanceShopWnd, "m_CurRotation")
RegistClassMember(LuaAppearanceShopWnd, "m_HeadId")
RegistClassMember(LuaAppearanceShopWnd, "m_BodyId")
RegistClassMember(LuaAppearanceShopWnd, "m_BackPendantId")
RegistClassMember(LuaAppearanceShopWnd, "m_ExpressionId")
RegistClassMember(LuaAppearanceShopWnd, "m_PreviewRO")

RegistClassMember(LuaAppearanceShopWnd, "m_DefaultSelectItemIndex")

function LuaAppearanceShopWnd:Awake()
    self.m_ItemTemplate:SetActive(false)
    UIEventListener.Get(self.m_BuyBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) self:OnBuyButtonClick() end)
    UIEventListener.Get(self.m_InfoButton).onClick = DelegateFactory.VoidDelegate(function(go) self:OnInfoButtonClick() end)
    UIEventListener.Get(self.m_ModelTexture.gameObject).onDrag = DelegateFactory.VectorDelegate(function(go,delta) self:OnDragModel(go,delta) end)
    self.m_GroupTableView:Init(
        function() return self:NumberOfSectionsInTableView() end,
        function(section) return self:NumberOfRowsInSection(section) end,
        function(cell,index) self:RefreshCellForRowAtIndex(cell, index) end,
        function(index,expanded) self:OnSectionClicked(index, expanded) end,
        function(index) self:OnRowSelected(index) end
    )
    self:GenerateMenuInfo()

    self.m_NumberInput.onValueChanged = DelegateFactory.Action_uint(function(val)
        self.m_MoneyCtrl:SetCost(val * CLuaNPCShopInfoMgr.GetPrice(self.m_SelectedItemId))
    end)
     --数量变化的时候
     self.m_MoneyCtrl.updateAction = function () 
        self:RefreshInputMinMax(self.m_SelectedItemId)--更新最大数量
    end

    self.m_SelectedItemId = 0
    self.m_ItemInfoTbl={}
    self.m_GlobalLimitInfo = {}
    self.m_IsGlobalLimit = false
    self.m_IsOpenGlobalLimit = true
    self.m_Identifier = "__AppearanceShopWnd__"
    self.m_CurRotation = 180
    self.m_HeadId = 0
    self.m_BodyId = 0
    self.m_BackPendantId = 0
    self.m_ExpressionId = 0
end

function LuaAppearanceShopWnd:GetRows(groupId)
    local ret = {}
    Wardrobe_ScoreShop.Foreach(function(key,data)
        if data.Group==groupId and data.Status==0 then
            local tbl = {}
            local itemIds = data.ItemID
            local previewItemIds = data.FashionItemID   
            for i=0,itemIds.Length-1 do
                local itemId = itemIds[i]
                if CLuaNPCShopInfoMgr:ItemExists(itemId) then
                    local item = CItemMgr.Inst:GetItemTemplate(itemId)
                    local previewItemId = previewItemIds and previewItemIds.Length==itemIds.Length and previewItemIds[i] or 0
                    table.insert(tbl, {id=itemId, group=groupId, subGroup=data.SubGroup, icon=item.Icon, previewId=previewItemId})
                end
            end
            if #tbl>0 then
                table.insert(ret, {title=data.Name, subGroup=data.SubGroup, items=tbl, action=function() self:LoadShopItems(tbl) end})
            end
        end
    end)
    table.sort(ret, function(a,b) return a.subGroup < b.subGroup end)
    return ret
end

function LuaAppearanceShopWnd:GenerateMenuInfo()
    self.m_CurMenuInfo = {
        {
            title = LocalString.GetString("时装"),
            rows = self:GetRows(1),
        },
        {
            title = LocalString.GetString("个性化"),
            rows = self:GetRows(2),
        },
    }
end

function LuaAppearanceShopWnd:OnBuyButtonClick()
     Gac2Gas.BuyNpcShopItem(CLuaNPCShopInfoMgr.m_npcShopInfo.NpcEngineId, CLuaNPCShopInfoMgr.m_npcShopInfo.ShopId, self.m_SelectedItemId, self.m_NumberInput:GetValue())
end

function LuaAppearanceShopWnd:OnInfoButtonClick()
    local scoreShopData = Shop_PlayScore.GetData(CLuaNPCShopInfoMgr.m_npcShopInfo.ShopId)
    if scoreShopData and scoreShopData.TipMessage then
        g_MessageMgr:ShowMessage(scoreShopData.TipMessage)
    end
end

-- group table view
function LuaAppearanceShopWnd:NumberOfSectionsInTableView()
    return #self.m_CurMenuInfo
end

function LuaAppearanceShopWnd:NumberOfRowsInSection(section)
    return #self.m_CurMenuInfo[section+1].rows
end

function LuaAppearanceShopWnd:RefreshCellForRowAtIndex(cell, cellIndex)
    local viewCell = cell:GetComponent(typeof(UIDefaultTableViewCell))
    local title = ""
    if cellIndex.type == CellIndexType.Section then
        title = self.m_CurMenuInfo[cellIndex.section+1].title
    else
        title = self.m_CurMenuInfo[cellIndex.section+1].rows[cellIndex.row+1].title
    end
    viewCell.m_Button.Text = title
end

function LuaAppearanceShopWnd:OnSectionClicked(cellIndex, expanded)
    -- section 没有行为
end

function LuaAppearanceShopWnd:OnRowSelected(cellIndex)
    local menuItem = self.m_CurMenuInfo[cellIndex.section+1].rows[cellIndex.row+1]
    menuItem.action()
end

function LuaAppearanceShopWnd:Init()
    local scoreShopData = Shop_PlayScore.GetData(CLuaNPCShopInfoMgr.m_npcShopInfo.ShopId)
    if scoreShopData ==nil or scoreShopData.Key ~= "AppearanceScore" then
        CUIManager.CloseUI("AppearanceShopWnd")
        return
    end
    local globalLimit = scoreShopData.GlobalLimit
        
    if globalLimit and globalLimit ~= "" then
        self.m_IsGlobalLimit = true
        for cronStr in string.gmatch(globalLimit, "([^;]+);?") do
            local t = {}
            local itemId,limitCount = string.match(cronStr, "(%d+),(%d+)")
            t.itemId = tonumber(itemId)
            t.limitCount = tonumber(limitCount)
            self.m_GlobalLimitInfo[t.itemId] = t
        end
    end

    local type = CLuaNPCShopInfoMgr.m_npcShopInfo.MoneyType
    local scoreKey = CLuaNPCShopInfoMgr.m_npcShopInfo.PlayScoreKey
    self:LoadModel()
    self.m_MoneyCtrl:SetType(EnumMoneyType.Score, scoreKey, true)
    --如果指定了默认选中，就定位到默认道具，否则选择第一个
    local defaultSelectSection, defaultSelectRow, defaultSelectIndex = self:GetDefaultItemPos()
    self.m_DefaultSelectItemIndex = defaultSelectIndex
    if defaultSelectSection then
        self.m_GroupTableView:LoadData(CellIndex(defaultSelectSection, defaultSelectRow, CellIndexType.Row), true)
    else
        self.m_GroupTableView:LoadData(CellIndex(0, 0, CellIndexType.Section), true)
    end
end

function LuaAppearanceShopWnd:GetDefaultItemPos()
    local srcItemTemplateId = CLuaNPCShopInfoMgr.m_SrcItemTemplateId
    if srcItemTemplateId and srcItemTemplateId>0 then
        for section, subMenuTbl in pairs(self.m_CurMenuInfo) do
            for row, subMenu in pairs(subMenuTbl.rows) do
                for index, item in pairs(subMenu.items) do
                    if item.id == srcItemTemplateId then
                        return section-1, row-1, index-1 --下标均从0开始
                    end
                end
            end
        end
    end
    return nil
end

function LuaAppearanceShopWnd:RefreshInputMinMax(templateId)
    local limit = CLuaNPCShopInfoMgr.GetRemainCount(templateId)
    local price = CLuaNPCShopInfoMgr.GetPrice(templateId)
    if price == 0 then price = 1 end
    local maxNum = math.min((math.floor(self.m_MoneyCtrl:GetOwn() / price)),999)
    if limit == 0 or maxNum == 0 then
        self.m_NumberInput:SetMinMax(1, 1, 1)
    else
        self.m_NumberInput:SetMinMax(1, maxNum >= limit and limit or maxNum, 1)
    end
    -- --刷新
    --不能用SetCost，会递归调用，引起崩溃
    local cost = self.m_NumberInput:GetValue() * CLuaNPCShopInfoMgr.GetPrice(templateId)
    self.m_MoneyCtrl.m_CostLabel.text = tostring(cost)
    --全服限购的物品每次只允许购买一个
    if self.m_IsGlobalLimit and self.m_GlobalLimitInfo[templateId] then
        self.m_NumberInput:SetMinMax(1, 1, 1)
        self.m_NumberInput:SetMinMax(1, 1, 1)
    end
end

function LuaAppearanceShopWnd:UpdateDescription(templateId)
    local name = ""
    local level = 0
    local desc = ""
    local itemType = ""
    local price = ""
    if IdPartition.IdIsItem(templateId) then
        local item = CItemMgr.Inst:GetItemTemplate(templateId)
        name = item.Name
        level = CLuaDesignMgr.Item_Item_GetAdjustedGradeCheck(item)
        desc = CItem.GetItemDescription(item.ID,true)
        self.m_Icon:LoadMaterial(item.Icon)
        local data = Item_Type.GetData(item.Type)
        if data ~= nil then
            itemType = data.Name
        end
    elseif IdPartition.IdIsEquip(templateId) then
        local equip = EquipmentTemplate_Equip.GetData(templateId)
        local equipment = CLuaNPCShopInfoMgr.GetEquipment(templateId)
        name = equip.Name
        level = equipment.Grade
        desc = equipment.WordDescription
        self.m_Icon:LoadMaterial(equip.Icon)
        local subTypeTemplate = EquipmentTemplate_SubType.GetData(equip.Type * 100 + equip.SubType)
        if subTypeTemplate ~= nil then
            itemType = subTypeTemplate.Name
        end
    end

    self:RefreshCost(templateId)

    self.m_NumberInput:SetValue(1, true)

    self.m_NameLabel.text = name
    local levelStr = level > 0 and "Lv." .. tostring(level) or nil
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Level < level then
        levelStr = SafeStringFormat3("[c][ff0000]%s[-][/c]", levelStr)
    end

    self.m_LevelLabel.text = levelStr
    self.m_TypeLabel.text = itemType
    self.m_DescLabel.text = CChatLinkMgr.TranslateToNGUIText(desc, false)
    self.m_DescScrollView:ResetPosition()
    self.m_MoneyCtrl:SetCost(CLuaNPCShopInfoMgr.GetPrice(templateId))
    self:UpdateOwnedNumLabel()

    --全服限购
    if self.m_IsGlobalLimit and self.m_GlobalLimitInfo[templateId] then
        Gac2Gas.QueryScoreShopGoodsRestStock(CLuaNPCShopInfoMgr.m_npcShopInfo.ShopId,templateId)
    else
        self.m_BuyBtn.Enabled = true
    end
end

function LuaAppearanceShopWnd:RefreshCost(templateId)
    local costItem = CLuaNPCShopInfoMgr.GetCostItem(templateId)
    local costGo = self.m_CostItemCtrl.gameObject
    if costItem then
        costGo:SetActive(true)

        local costItemID = costItem[1]
        local costItemcount = costItem[2]
        local itemcfg = CItemMgr.Inst:GetItemTemplate(costItemID)

        self.m_CostItemCtrl.templateId = costItemID
        self.m_CostItemCtrl.format = "{0}/"..costItem[2]
        self.m_CostItemCtrl:UpdateCount()

        self.m_CostItemIcon:LoadMaterial(itemcfg.Icon)

        local havecount = CItemMgr.Inst:GetItemCount(costItemID)
        local enough = havecount >= costItem[2]

        self.m_CostItemMask.gameObject:SetActive(not enough)

        self.m_CostItemCtrl.onChange = DelegateFactory.Action_int(function(count)
            self.m_CostItemMask.gameObject:SetActive(count<costItemcount)
        end)

        UIEventListener.Get(costGo).onClick=DelegateFactory.VoidDelegate(function(go)
            if enough then
                CItemInfoMgr.ShowLinkItemTemplateInfo(costItemID, false, nil, AlignType.ScreenRight, 0, 0, 0, 0)
            else
                CItemAccessListMgr.Inst:ShowItemAccessInfo(costItemID, false, nil, CTooltip.AlignType.Right)
            end
        end)
    else
        costGo:SetActive(false)
    end
end

function LuaAppearanceShopWnd:UpdateOwnedNumLabel()
    local templateId = CLuaNPCShopInfoMgr.RowAt(self.m_SelectedItemId)
    if IdPartition.IdIsItem(templateId) and not CItemMgr.Inst.IsTaskItem then
        self.m_OwnedNumLabel.text = SafeStringFormat3(LocalString.GetString("已拥有:%d"),CItemMgr.Inst:GetItemCount(templateId))
    else
        self.m_OwnedNumLabel.text = ""
    end
end

function LuaAppearanceShopWnd:LoadShopItems(itemInfoTbl)
    self.m_ItemInfoTbl = itemInfoTbl
    Extensions.RemoveAllChildren(self.m_ItemTable.transform)
    for i = 1, #self.m_ItemInfoTbl do
        local child = CUICommonDef.AddChild(self.m_ItemTable.gameObject, self.m_ItemTemplate)
        child:SetActive(true)
        self:InitItem(child, self.m_ItemInfoTbl[i])
    end
    self.m_ItemTable:Reposition()
    self.m_ItemScrollView:ResetPosition()
    local index = 0
    if self.m_DefaultSelectItemIndex and self.m_DefaultSelectItemIndex< #self.m_ItemInfoTbl then
        index = self.m_DefaultSelectItemIndex
        self.m_DefaultSelectItemIndex = nil
    end
    self:OnItemClick(self.m_ItemTable.transform:GetChild(index).gameObject, self.m_ItemInfoTbl[index+1])
end

function LuaAppearanceShopWnd:InitItem(itemGO,itemInfo)
    local templateId = itemInfo.id
    local icon = itemGO.transform:GetComponent(typeof(CUITexture))
    local disabledGo = itemGO.transform:Find("Disabled").gameObject
    icon:Clear()
    disabledGo:SetActive(false)
    if IdPartition.IdIsItem(templateId) then
        local item = CItemMgr.Inst:GetItemTemplate(templateId)
        if item ~= nil then
            icon:LoadMaterial(item.Icon)
            disabledGo:SetActive(not CItem.GetMainPlayerIsFit(templateId, true))
        end
    elseif IdPartition.IdIsEquip(templateId) then
        --装备
        local equip = EquipmentTemplate_Equip.GetData(templateId)
        icon:LoadMaterial(equip.Icon)
        disabledGo:SetActive(not CEquipment.GetMainPlayerIsFit(templateId, true))
    end

    self:UpdateRemainCountInfo(templateId)

    if self.m_IsGlobalLimit and self.m_GlobalLimitInfo[templateId] then
        Gac2Gas.QueryScoreShopGoodsRestStock(CLuaNPCShopInfoMgr.m_npcShopInfo.ShopId,templateId)
    end

    UIEventListener.Get(itemGO).onClick = DelegateFactory.VoidDelegate(function(go) self:OnItemClick(go, itemInfo) end)
end

function LuaAppearanceShopWnd:OnItemClick(go, itemInfo)
    local childCount = self.m_ItemTable.transform.childCount
    for i=0, childCount-1 do
        local childGo = self.m_ItemTable.transform:GetChild(i).gameObject
        if childGo == go then
            childGo.transform:Find("Selected").gameObject:SetActive(true)
            self.m_SelectedItemId = self.m_ItemInfoTbl[i+1].id
            self:PreviewItem(self.m_ItemInfoTbl[i+1])
        else
            childGo.transform:Find("Selected").gameObject:SetActive(false)
        end
    end
    
    self:UpdateDescription(itemInfo.id)
end

function LuaAppearanceShopWnd:UpdateRemainCountInfo(templateId)
    -- if not self.m_ItemLookup[templateId] then
    --     return 
    -- end
    -- local numLimitLabel = self.m_ItemLookup[templateId]:Find("NumLimitLabel"):GetComponent(typeof(UILabel))

    -- local limit = CLuaNPCShopInfoMgr.GetRemainCount(templateId)
    -- local limitType = CLuaNPCShopInfoMgr.GetLimitType(templateId)

    -- if CLuaNPCShopInfoMgr.m_npcShopInfo.ShopId == 34000105 then
    --     numLimitLabel.text = SafeStringFormat3(LocalString.GetString("剩余%d"), limit)
    --     return
    -- end

    -- if limitType == 1 then
    --     numLimitLabel.text = SafeStringFormat3(LocalString.GetString("剩余%d"), limit)
    -- elseif limitType == 2 then
    --     numLimitLabel.text = SafeStringFormat3(LocalString.GetString("本月剩余%d"), limit)
    -- elseif limitType == 3 then
    --     numLimitLabel.text = SafeStringFormat3(LocalString.GetString("本周剩余%d"), limit)
    -- else
    --     numLimitLabel.text = nil
    --     local globalLimitLabel = self.m_ItemLookup[templateId]:Find("GlobalLimitLabel"):GetComponent(typeof(UILabel))
    --     if self.m_IsGlobalLimit and self.m_GlobalLimitInfo[templateId] then
    --         local limitCount = self.m_GlobalLimitInfo[templateId].limitCount
    --         globalLimitLabel.text = SafeStringFormat3(LocalString.GetString("全服限购%d"),limitCount)
    --     else
    --         globalLimitLabel.text = nil
    --     end
    -- end
end

function LuaAppearanceShopWnd:OnEnable()
    g_ScriptEvent:AddListener("SendItem", self, "SendItem")
	g_ScriptEvent:AddListener("SetItemAt", self, "SetItemAt")
	g_ScriptEvent:AddListener("UpdateNpcShopLimitItemCount", self, "OnNPCShopItemRemainCountChange")
    g_ScriptEvent:AddListener("MainPlayerLevelChange", self, "UpdateDisableStatus")
    g_ScriptEvent:AddListener("OnReplyScoreShopGoodsRestStock", self, "OnReplyScoreShopGoodsRestStock")   
end
function LuaAppearanceShopWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendItem", self, "SendItem")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "SetItemAt")
	g_ScriptEvent:RemoveListener("UpdateNpcShopLimitItemCount", self, "OnNPCShopItemRemainCountChange")
    g_ScriptEvent:RemoveListener("MainPlayerLevelChange", self, "UpdateDisableStatus")
    g_ScriptEvent:RemoveListener("OnReplyScoreShopGoodsRestStock", self, "OnReplyScoreShopGoodsRestStock") 
end

function LuaAppearanceShopWnd:SendItem(args)
    self:UpdateOwnedNumLabel()
end

function LuaAppearanceShopWnd:SetItemAt(args)
    self:UpdateOwnedNumLabel()
end


function LuaAppearanceShopWnd:OnNPCShopItemRemainCountChange( shopId, templateId, remainCount) 
    if CLuaNPCShopInfoMgr.m_npcShopInfo.ShopId == shopId then
        CLuaNPCShopInfoMgr.SetRemainCount(templateId,remainCount)
        self:RefreshInputMinMax(self.m_SelectedItemId)
        self:UpdateRemainCountInfo(templateId)
    end
end


function LuaAppearanceShopWnd:UpdateDisableStatus()
    local childCount = self.m_ItemTable.transform.childCount
    for i = 1, childCount-1 do
        local child = self.m_ItemTable.transform:GetChild(i)
        local info = self.m_ItemInfoTbl[i+1]
        local disabledGo = child:Find("Disabled").gameObject
        disabledGo:SetActive(not info.isFit)
    end
end

function LuaAppearanceShopWnd:OnReplyScoreShopGoodsRestStock(shopId, itemTemplateId, stock)
    if CLuaNPCShopInfoMgr.m_npcShopInfo.ShopId == shopId then
        if itemTemplateId == self.m_SelectedItemId then
            self.m_BuyBtn.Enabeld = (stock > 0)
        end
    end
end

function LuaAppearanceShopWnd:PreviewItem(itemInfo)
    self.m_ModelTexture.gameObject:SetActive(true)
    self.m_LeaderIcon.spriteName = ""
    self.m_ProfileFrame.gameObject:SetActive(false)
    self.m_HeadId = 0
    self.m_BodyId = 0
    self.m_BackPendantId = 0
    self.m_ExpressionId = 0
    local previewId = itemInfo.previewId
    if itemInfo.group == 1 and Fashion_Fashion.Exists(previewId) then
        if itemInfo.subGroup==1 then
            self.m_BodyId = previewId
        elseif itemInfo.subGroup==2 then
            self.m_HeadId = previewId
        elseif itemInfo.subGroup==3 then
            self.m_BackPendantId = previewId 
        end
    elseif itemInfo.group == 2 then
        if itemInfo.subGroup==1 and ExpressionHead_ProfileFrame.Exists(previewId) then
            self.m_ModelTexture.gameObject:SetActive(false)
            self.m_ProfileFrame.gameObject:SetActive(true)
            self.m_ProfileFrame:Find("Icon"):GetComponent(typeof(CUITexture)):LoadNPCPortrait(CClientMainPlayer.Inst and CUICommonDef.GetDieKeSpecialPortraitName(CClientMainPlayer.Inst) or "")
            self.m_ProfileFrame:Find("Icon/Frame"):GetComponent(typeof(CUITexture)):LoadMaterial(ExpressionHead_ProfileFrame.GetData(previewId).ResName)
        elseif itemInfo.subGroup==2 and Expression_Appearance.Exists(previewId) then
            self.m_ExpressionId = Expression_Appearance.GetData(previewId).PreviewDefine
        elseif itemInfo.subGroup==3 and Wardrobe_TeamLeaderIcon.Exists(previewId) then
            self.m_LeaderIcon.spriteName = Wardrobe_TeamLeaderIcon.GetData(previewId).ResName
        end
    end
    if self.m_ModelTexture.gameObject.activeSelf then
        self:LoadResource()
    end
end

function LuaAppearanceShopWnd:LoadModel()
    self.m_ModelTexture.mainTexture = CUIManager.CreateModelTexture(self.m_Identifier, self:GetModelLoader(),
        self.m_CurRotation, 0.05, -1, 4.66, false, true, 1.0, true, false)
end

function LuaAppearanceShopWnd:GetModelLoader()
    return LuaDefaultModelTextureLoader.Create(function (ro)
        self.m_PreviewRO = ro
       self:LoadResource()
    end)
end

function LuaAppearanceShopWnd:LoadResource()
    --清除一些表情动作的状态信息，类似CFullScreenModelTextureLoader中的处理
    --参考LuaAppearanceFashionEvaluationWnd处理
    self.m_PreviewRO:DestroyAllFX()
    self.m_PreviewRO:RemoveChild("weapon03")
    self.m_PreviewRO:SetSepSkeleton(nil)
    self.m_PreviewRO.SepAniEndCallback = nil
    self.m_PreviewRO:SetWeaponVisible(EWeaponVisibleReason.Expression, true)
    local fakeAppear = LuaAppearancePreviewMgr:GetPreviewFashionInfo(self.m_HeadId, self.m_BodyId, 0, self.m_BackPendantId, self.m_IsTransformOn)
    fakeAppear.YingLingState = EnumToInt(EnumYingLingState.eDefault)
    CClientMainPlayer.LoadResource(self.m_PreviewRO, fakeAppear, true, 1.0, 0, false, 0, false, self.m_ExpressionId, false, nil, false, false)
    --reset rotation
    self.m_CurRotation = 180
    CUIManager.SetModelRotation(self.m_Identifier, self.m_CurRotation)
end

function LuaAppearanceShopWnd:OnDragModel(go,delta)
    local deltaVal = -delta.x / Screen.width * 360
    self.m_CurRotation = self.m_CurRotation + deltaVal
    CUIManager.SetModelRotation(self.m_Identifier, self.m_CurRotation)
end

function LuaAppearanceShopWnd:OnDestroy()
    CUIManager.DestroyModelTexture(self.m_Identifier)
end