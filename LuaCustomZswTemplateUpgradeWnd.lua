local GameObject = import "UnityEngine.GameObject"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local CShopMallMgr   = import "L10.UI.CShopMallMgr"

LuaCustomZswTemplateUpgradeWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaCustomZswTemplateUpgradeWnd, "UpgradeBtn", "UpgradeBtn", GameObject)
RegistChildComponent(LuaCustomZswTemplateUpgradeWnd, "MoneyCtrl", "MoneyCtrl", CCurentMoneyCtrl)
RegistChildComponent(LuaCustomZswTemplateUpgradeWnd, "CurrentIcon", "CurrentIcon", CUITexture)
RegistChildComponent(LuaCustomZswTemplateUpgradeWnd, "CurrentLimitLabel", "CurrentLimitLabel", UILabel)
RegistChildComponent(LuaCustomZswTemplateUpgradeWnd, "NextLevelIcon", "NextLevelIcon", CUITexture)
RegistChildComponent(LuaCustomZswTemplateUpgradeWnd, "NextLevelLimitLabel", "NextLevelLimitLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaCustomZswTemplateUpgradeWnd,"m_TemplateIndex")
function LuaCustomZswTemplateUpgradeWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.UpgradeBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnUpgradeBtnClick()
	end)


    --@endregion EventBind end
end

function LuaCustomZswTemplateUpgradeWnd:Init()
	self.m_TemplateIndex = CLuaZswTemplateMgr.CurCustomTemplateIdx
	local curLevel = CLuaZswTemplateMgr.CurCustomTemplateLevel
	local nextLevel = curLevel + 1
	local curData = Zhuangshiwu_CustomTemplate.GetData(curLevel)
	local nextData = Zhuangshiwu_CustomTemplate.GetData(nextLevel)
	if not curData or not nextData then
		CUIManager.CloseUI(CLuaUIResources.CustomZswTemplateUpgradeWnd)
		return
	end
	self.CurrentIcon:LoadMaterial(curData.DefaultIcon)
	self.NextLevelIcon:LoadMaterial(nextData.DefaultIcon)
	self.CurrentLimitLabel.text = SafeStringFormat3(LocalString.GetString("上限%d"),curData.FurNum)
	self.NextLevelLimitLabel.text = SafeStringFormat3(LocalString.GetString("上限%d"),nextData.FurNum)
	self.m_Cost = nextData.JadeCost

    self.MoneyCtrl:SetType(EnumMoneyType.LingYu, EnumPlayScoreKey.NONE, true)
    self.MoneyCtrl:SetCost(self.m_Cost)

end

--@region UIEvent

function LuaCustomZswTemplateUpgradeWnd:OnUpgradeBtnClick()
	local ownMoney = self.MoneyCtrl:GetOwn()
	if ownMoney < self.m_Cost then
		g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("JADE_PAY_NOTICE"), function ()
			CShopMallMgr.ShowChargeWnd()
		end, nil, nil, nil, false)
		return
	end
	MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("CustomZswTemplate_Upgrade_Comfire",self.m_Cost), DelegateFactory.Action(function ()            
		--升级格子，默认给3个，这里的idx从1开始，1-3 是默认给的
		Gac2Gas.RequestUpgradeCustomFurnitureTemplate(self.m_TemplateIndex) 
		CUIManager.CloseUI(CLuaUIResources.CustomZswTemplateUpgradeWnd)
	end), nil, nil, nil, false)
end


--@endregion UIEvent

