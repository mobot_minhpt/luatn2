local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local CTooltip = import "L10.UI.CTooltip"
local MessageMgr = import "L10.Game.MessageMgr"
local Transform_Transform = import "L10.Game.Transform_Transform"

LuaPlayerBasicPropertyView = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaPlayerBasicPropertyView, "Cor", "Cor", GameObject)
RegistChildComponent(LuaPlayerBasicPropertyView, "Sta", "Sta", GameObject)
RegistChildComponent(LuaPlayerBasicPropertyView, "Str", "Str", GameObject)
RegistChildComponent(LuaPlayerBasicPropertyView, "Int", "Int", GameObject)
RegistChildComponent(LuaPlayerBasicPropertyView, "Agi", "Agi", GameObject)
RegistChildComponent(LuaPlayerBasicPropertyView, "Attack", "Attack", GameObject)
RegistChildComponent(LuaPlayerBasicPropertyView, "HpFull", "HpFull", GameObject)
RegistChildComponent(LuaPlayerBasicPropertyView, "MpFull", "MpFull", GameObject)
RegistChildComponent(LuaPlayerBasicPropertyView, "Hit", "Hit", GameObject)
RegistChildComponent(LuaPlayerBasicPropertyView, "Fatal", "Fatal", GameObject)
RegistChildComponent(LuaPlayerBasicPropertyView, "pDef", "pDef", GameObject)
RegistChildComponent(LuaPlayerBasicPropertyView, "mDef", "mDef", GameObject)
RegistChildComponent(LuaPlayerBasicPropertyView, "pMiss", "pMiss", GameObject)
RegistChildComponent(LuaPlayerBasicPropertyView, "mMiss", "mMiss", GameObject)
RegistChildComponent(LuaPlayerBasicPropertyView, "HPBar", "HPBar", GameObject)
RegistChildComponent(LuaPlayerBasicPropertyView, "MPBar", "MPBar", GameObject)
RegistChildComponent(LuaPlayerBasicPropertyView, "ExpBar", "ExpBar", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaPlayerBasicPropertyView, "m_PropertyList")
RegistClassMember(LuaPlayerBasicPropertyView, "m_ClickTick")
RegistClassMember(LuaPlayerBasicPropertyView, "m_ChangeColor")
RegistClassMember(LuaPlayerBasicPropertyView, "m_OriColor")
RegistClassMember(LuaPlayerBasicPropertyView, "m_WordsColor")
function LuaPlayerBasicPropertyView:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.m_PropertyList = {}
    self.m_ChangeColor = "fffe91"
    self.m_OriColor = {}
    self.m_WordsColor = CreateFromClass(MakeGenericClass(List, UInt32))
    self.m_ClickTick = nil
    self:InitProperties()
end

function LuaPlayerBasicPropertyView:InitProperties()
    
    self:InitOneProperty(1,self.HPBar)   -- 当前气血
    self:InitOneProperty(2,self.MPBar)   -- 当前法力   
    self:InitOneProperty(3,self.Cor)     -- 根骨
    self:InitOneProperty(4,self.Sta)     -- 精力
    self:InitOneProperty(5,self.Str)     -- 力量
    self:InitOneProperty(6,self.Int)     -- 智力
    self:InitOneProperty(7,self.Agi)     -- 敏捷
    self:InitOneProperty(14,self.HpFull) -- 最大气血
    self:InitOneProperty(15,self.MpFull) -- 最大法力
    self:InitOneProperty(20,self.pDef)   -- 物理防御
    self:InitOneProperty(21,self.mDef)   -- 法术防御
    self:InitOneProperty(22,self.pMiss)  -- 物理躲避
    self:InitOneProperty(23,self.mMiss)  -- 法术躲避
    self:InitOneProperty(26,self.ExpBar) -- 当前经验
    
    if CClientMainPlayer.Inst == nil then return end
    local physical = CClientMainPlayer.Inst.IsPhysicalAttackType
    if CClientMainPlayer.Inst.AppearanceProp.ResourceId ~= 0 then
        --如果是在变形中?
        local data = Transform_Transform.GetData(CClientMainPlayer.Inst.AppearanceProp.ResourceId)
        if data then
            if data.IsPhysicalAttackType == 1 then
                physical = false
            elseif data.IsPhysicalAttackType == 2 then
                physical = true
            end
        end
    end
    if physical then
        self:InitOneProperty(8,self.Attack)     -- 物理攻击
        self:InitOneProperty(16,self.Hit)       -- 物理命中
        self:InitOneProperty(18,self.Fatal)     -- 物理致命
    else
        self:InitOneProperty(11,self.Attack)    -- 法术攻击
        self:InitOneProperty(17,self.Hit)       -- 法术命中
        self:InitOneProperty(19,self.Fatal)     -- 法术致命
    end
    if CClientMainPlayer.Inst and LuaPlayerPropertyMgr.PropertyGuide then
        if CClientMainPlayer.Inst.Level >= LuaPlayerPropertyMgr:GetPropertyGuideLevel() then
            CLuaGuideMgr.TryTriggerGuide(233)
        end
    end
end

function LuaPlayerBasicPropertyView:InitOneProperty(propertyID,propertyGo)
    if not propertyGo then return end
    local go = propertyGo.transform:Find("NameLabel").gameObject
    local data = PropGuide_Property.GetData(propertyID) 
    if not data then return end

    self.m_PropertyList[propertyID] = propertyGo 
    self.m_OriColor[propertyID] = go:GetComponent(typeof(UILabel)).color
    
    UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function (go)
        -- 属性详析按钮
        if LuaPlayerPropertyMgr.PropertyGuide and data.ShowBtn == 1 then
            self:ShowBtn(go,propertyID)
        else
	        self:ShowMessage(go,data.Desc)
        end
        -- 关联属性变色
        if LuaPlayerPropertyMgr.PropertyGuide and data.RelateProperty then
            
            self:SetWorldColor(propertyID)
            -- 遍历RelateProperty
            for i = 0,data.RelateProperty.Length - 1 do
                self:SetWorldColor(data.RelateProperty[i])
            end
            -- 起Tick
            self:ClearTick()
            self.m_ClickTick = RegisterTickOnce(function()
                self:ClearTick()
                self:RevertColors()
            end,PropGuide_Setting.GetData().RelatePropertyTime * 1000)
        end
	end)
end

function LuaPlayerBasicPropertyView:Update()
    if Input.GetMouseButtonDown(0) then    
        self:ClearTick()
        self:RevertColors()
    end
end
function LuaPlayerBasicPropertyView:RevertColors()
    for i = self.m_WordsColor.Count - 1 , 0, -1 do
        self:RevertWordColor(self.m_WordsColor[i])
    end
end
function LuaPlayerBasicPropertyView:ShowBtn(go,propertyID)
    local showWnd = function()
        LuaPlayerPropertyMgr:ShowPlayerPropertyDetailWnd(propertyID)
    end
    LuaToolBtnMgr:ShowToolBtn(PropGuide_Setting.GetData().PropertyGuideName,showWnd,go.transform)
end
function LuaPlayerBasicPropertyView:ShowMessage(go,MessageName)
    CTooltip.Show(MessageMgr.Inst:FormatMessage(MessageName,{}), go.transform, CTooltip.AlignType.Top);
end
function LuaPlayerBasicPropertyView:SetWorldColor(propID)
    local go = self.m_PropertyList[propID]
    if not go then return end
    if self.m_WordsColor:Contains(propID) then return end
    go.transform:Find("NameLabel"):GetComponent(typeof(UILabel)).color = NGUIText.ParseColor24(self.m_ChangeColor, 0)
    self.m_WordsColor:Add(propID)
end
function LuaPlayerBasicPropertyView:RevertWordColor(propID)
    local go = self.m_PropertyList[propID]
    local oriColor = self.m_OriColor[propID]
    if not go or not oriColor then return end
    go.transform:Find("NameLabel"):GetComponent(typeof(UILabel)).color = oriColor
    if self.m_WordsColor:Contains(propID) then
        self.m_WordsColor:Remove(propID)
    end
end

function LuaPlayerBasicPropertyView:ClearTick()
    if self.m_ClickTick ~= nil then
        UnRegisterTick(self.m_ClickTick)
        self.m_ClickTick = nil
    end
end

function LuaPlayerBasicPropertyView:OnDisable()
    self:ClearTick()
end
--@region UIEvent

--@endregion UIEvent

