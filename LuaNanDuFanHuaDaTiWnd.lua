local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CButton = import "L10.UI.CButton"

LuaNanDuFanHuaDaTiWnd = class()
LuaNanDuFanHuaDaTiWnd.m_QuestionId = nil
LuaNanDuFanHuaDaTiWnd.m_PeriodId = nil
LuaNanDuFanHuaDaTiWnd.m_ScheduleId = nil

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaNanDuFanHuaDaTiWnd, "ChoiceTemplate", "ChoiceTemplate", GameObject)
RegistChildComponent(LuaNanDuFanHuaDaTiWnd, "Table", "Table", GameObject)
RegistChildComponent(LuaNanDuFanHuaDaTiWnd, "Question", "Question", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaNanDuFanHuaDaTiWnd, "m_CloseBtn")
RegistClassMember(LuaNanDuFanHuaDaTiWnd, "m_SelectId")
RegistClassMember(LuaNanDuFanHuaDaTiWnd, "m_ChoiceList")
RegistClassMember(LuaNanDuFanHuaDaTiWnd, "m_totalTime")
RegistClassMember(LuaNanDuFanHuaDaTiWnd, "m_QuestionId")
RegistClassMember(LuaNanDuFanHuaDaTiWnd, "m_AnswerIndex")
RegistClassMember(LuaNanDuFanHuaDaTiWnd, "m_IsClick")
RegistClassMember(LuaNanDuFanHuaDaTiWnd, "m_ShowAnswerTime")
RegistClassMember(LuaNanDuFanHuaDaTiWnd, "m_ShowAlertTime")
RegistClassMember(LuaNanDuFanHuaDaTiWnd, "m_ShowRightTick")
RegistClassMember(LuaNanDuFanHuaDaTiWnd, "m_HasInit")
function LuaNanDuFanHuaDaTiWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.ChoiceTemplate.gameObject:SetActive(false)
    self.Table.gameObject:SetActive(true)
end

function LuaNanDuFanHuaDaTiWnd:Init()
    if not LuaNanDuFanHuaDaTiWnd.m_QuestionId then return end
    self:InitQuestion()
end

function LuaNanDuFanHuaDaTiWnd:InitQuestion()
    local questionData = NanDuFanHua_LordTaskQuestion.GetData(LuaNanDuFanHuaDaTiWnd.m_QuestionId)
    if not questionData then return end
    local lordData = {}
    if CClientMainPlayer.Inst and CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, LuaYiRenSiMgr.LordTempPlayDataKey) then
        local playData = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, LuaYiRenSiMgr.LordTempPlayDataKey)
        if playData.ExpiredTime > CServerTimeMgr.Inst.timeStamp and playData.Data and playData.Data.Data then
            local data = g_MessagePack.unpack(playData.Data.Data)
            lordData = data[1]
        end
    end
    local moneyCnt = lordData[LuaYiRenSiMgr.LordPropertyKey.moneyIndex] and lordData[LuaYiRenSiMgr.LordPropertyKey.moneyIndex] or 0
    
    local answerHead = {"A.","B.","C.","D."}
    self.Question.text = questionData.Title
    self.m_ChoiceList = {}
    self.m_ChoiceList[1] = {answer = questionData.Choice1}
    self.m_ChoiceList[2] = {answer = questionData.Choice2}
    self.m_ChoiceList[3] = {answer = questionData.Choice3}
    self.m_ChoiceList[4] = {answer = questionData.Choice4}
    local moneyRequireList = {questionData.Choice1MoneyRequire, questionData.Choice2MoneyRequire, questionData.Choice3MoneyRequire, questionData.Choice4MoneyRequire}
    Extensions.RemoveAllChildren(self.Table.transform)
    for i = 1, questionData.Num do
        local go = CUICommonDef.AddChild(self.Table.gameObject,self.ChoiceTemplate.gameObject)
        local label = go.transform:Find("Label"):GetComponent(typeof(UILabel))
        local clickbg = go.transform:Find("clickbg").gameObject
        local isRight = go.transform:Find("Sprite").gameObject
        clickbg.gameObject:SetActive(false)
        isRight.gameObject:SetActive(false)
        label.text = answerHead[i]..self.m_ChoiceList[i].answer
        go.gameObject:SetActive(true)
        UIEventListener.Get(go.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            Gac2Gas.NanDuLordSelectScheduleChoice(LuaNanDuFanHuaDaTiWnd.m_PeriodId, LuaNanDuFanHuaDaTiWnd.m_ScheduleId, LuaNanDuFanHuaDaTiWnd.m_QuestionId, i)
            CUIManager.CloseUI(CLuaUIResources.NanDuFanHuaDaTiWnd)
        end)

        if moneyRequireList[i] then
            if moneyCnt >= moneyRequireList[i] then
                --钱够
                go.transform:GetComponent(typeof(CButton)).Enabled = true
            else
                go.transform:GetComponent(typeof(CButton)).Enabled = false
            end
        end
    end
    self.Table:GetComponent(typeof(UITable)):Reposition()
end

--@region UIEvent

--@endregion UIEvent

function LuaNanDuFanHuaDaTiWnd:OnEnable()
end

function LuaNanDuFanHuaDaTiWnd:OnDisable()
end
