local UILabel = import "UILabel"
local UIGrid = import "UIGrid"
local UISprite = import "UISprite"
local GameObject = import "UnityEngine.GameObject"
local QnButton = import "L10.UI.QnButton"
local CommonDefs = import "L10.Game.CommonDefs"
local UICommonDef = import "L10.UI.CUICommonDef"
local Extensions = import "Extensions"
local DelegateFactory  = import "DelegateFactory"
local MessageWndManager = import "L10.UI.MessageWndManager"
local Color = import "UnityEngine.Color"
local UIPanel = import "UIPanel"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Profession = import "L10.Game.Profession"
local EnumClass = import "L10.Game.EnumClass"
local NGUIText = import "NGUIText"
local CGuideMgr = import "L10.Game.Guide.CGuideMgr"

LuaGuanNingCommandWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaGuanNingCommandWnd, "OkButton", "OkButton", QnButton)
RegistChildComponent(LuaGuanNingCommandWnd, "Grid", "Grid", GameObject)
RegistChildComponent(LuaGuanNingCommandWnd, "ItemTemplate", "ItemTemplate", GameObject)
RegistChildComponent(LuaGuanNingCommandWnd, "NoPlayer", "NoPlayer", UILabel)
RegistChildComponent(LuaGuanNingCommandWnd, "TipButton", "TipButton", GameObject)

--@endregion RegistChildComponent end

RegistClassMember(LuaGuanNingCommandWnd, "m_Panel")
RegistClassMember(LuaGuanNingCommandWnd, "m_bIsSignUp")

function LuaGuanNingCommandWnd:Awake()
	self.m_Panel = self.transform:GetComponent(typeof(UIPanel))
    --self.m_Panel.alpha = 0

    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.OkButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOkButtonClick()
	end)


	
	UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)


    --@endregion EventBind end
end

function LuaGuanNingCommandWnd:Init()
	self.OkButton.gameObject:SetActive(false)
    Gac2Gas.RequestGnjcZhiHuiSignUpData()
end

function LuaGuanNingCommandWnd:OnEnable()
	g_ScriptEvent:AddListener("SyncGnjcZhiHuiInfo", self, "OnSyncGnjcZhiHuiInfo")
	g_ScriptEvent:AddListener("SendGnjcZhiHuiSignUpData", self, "RefreshAll")
end

function LuaGuanNingCommandWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SyncGnjcZhiHuiInfo", self, "OnSyncGnjcZhiHuiInfo")
	g_ScriptEvent:RemoveListener("SendGnjcZhiHuiSignUpData", self, "RefreshAll")
end

--@region UIEvent

function LuaGuanNingCommandWnd:OnOkButtonClick()
	if self.m_bIsSignUp then
		Gac2Gas.RequestCancelSignUpGnjcZhiHui()
	else
		g_MessageMgr:ShowOkCancelMessage(LocalString.GetString("确认报名校场指挥？"), function()
			Gac2Gas.RequestSignUpGnjcZhiHui()
		end, nil, nil, nil, false)
	end
end


function LuaGuanNingCommandWnd:OnTipButtonClick()
	g_MessageMgr:ShowMessage("GuanNing_Commander_Tips")
end


--@endregion UIEvent

--报名or取消报名后会刷新
function LuaGuanNingCommandWnd:RefreshAll(list)
	self.NoPlayer.text = #list == 0 and LocalString.GetString("暂无玩家报名") or ""
	Extensions.RemoveAllChildren(self.Grid.transform)
	
	self.m_bIsSignUp = false
	local closed = CLuaGuanNingMgr.m_Status >= EnumGuanNingPlayStatus.eStart

	for i = 1, #list, 8 do
		local item = UICommonDef.AddChild(self.Grid, self.ItemTemplate)
		item:SetActive(true)
		item.transform:Find("ElectedSprite"):GetComponent(typeof(UISprite)).gameObject:SetActive(false)
		item.transform:Find("ElectedLabel"):GetComponent(typeof(UILabel)).gameObject:SetActive(false)
		local playerInfo = {}

		-- playerClass
		playerInfo[1] = item.transform:Find("Sprite"):GetComponent(typeof(UISprite))
		playerInfo[1].spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), list[i + 1]))

		-- playerName
		playerInfo[2] = item.transform:Find("Name"):GetComponent(typeof(UILabel))
		playerInfo[2].text = list[i + 2]

		-- fightScore
		playerInfo[3] = item.transform:Find("FightScore"):GetComponent(typeof(UILabel))
		playerInfo[3].text = list[i + 3]

		-- praiseScore
		playerInfo[4] = item.transform:Find("PraiseScore"):GetComponent(typeof(UILabel))
		playerInfo[4].text = list[i + 4]

		-- thumbUpScore
		playerInfo[5] = item.transform:Find("ThumbUpScore"):GetComponent(typeof(UILabel))
		playerInfo[5].text = list[i + 5]

		-- force
		-- ...

		-- playerId
		if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == list[i] then
			self.m_bIsSignUp = true
			for i = 2, 5 do
				playerInfo[i].color = Color.green
			end
		end

		-- elected
		if closed then
			if list[i + 7] == 1 then
				item.transform:Find("ElectedSprite"):GetComponent(typeof(UISprite)).gameObject:SetActive(true)
				item.transform:Find("ElectedLabel"):GetComponent(typeof(UILabel)).gameObject:SetActive(true)
			else
				for i = 1, 5 do
					playerInfo[i].color = Color(playerInfo[i].color.r, playerInfo[i].color.g, playerInfo[i].color.b, 0.3)
				end
			end
		end
    end

	self.Grid:GetComponent(typeof(UIGrid)):Reposition()
	
	if closed then
		self.OkButton.Enabled = false
		self.OkButton.Text = LocalString.GetString("已结束")
	else
		if self.m_bIsSignUp then
			self.OkButton.Enabled = true
			self.OkButton.m_Label.color = NGUIText.ParseColor("0E3254", 0)
			self.OkButton.Text = LocalString.GetString("取消报名")
			self.OkButton:GetComponent(typeof(UISprite)).spriteName = "common_btn_01_blue"
		else
			self.OkButton.Enabled = true
			self.OkButton.m_Label.color = NGUIText.ParseColor("361F01", 0)
			self.OkButton.Text = LocalString.GetString("报名当指挥")
			self.OkButton:GetComponent(typeof(UISprite)).spriteName = "common_btn_01_yellow"
		end
	end

	--self.m_Panel.alpha = 1
	self.OkButton.gameObject:SetActive(true)
end

function LuaGuanNingCommandWnd:OnSyncGnjcZhiHuiInfo(bOpen, attackZhiHuiPlayerId, defendZhiHuiPlayerId, status)
	if bOpen and CLuaGuanNingMgr.m_Status >= EnumGuanNingPlayStatus.eStart then 
		EventManager.BroadcastInternalForLua(EnumEventType.Guide_ChangeView, {"GuanNingCommandWnd"})
		Gac2Gas.RequestGnjcZhiHuiSignUpData()
	end
end

function LuaGuanNingCommandWnd:GetGuideGo(methodName)
	if methodName == "GetSignUpBtn" then
		if self.m_bIsSignUp or CLuaGuanNingMgr.m_Status >= EnumGuanNingPlayStatus.eStart then
			CGuideMgr.Inst:EndCurrentPhase()
			return nil
		else
			return self.OkButton.gameObject
		end
	elseif methodName == "GetCloseBtn" then
		return self.transform:Find("Wnd_Bg_Secondary_2/CloseButton").gameObject
	end
	return nil
end
