local UITabBar = import "L10.UI.UITabBar"
local QnExpandListBox = import "L10.UI.QnExpandListBox"
local CFightingSpiritMatchRecordWnd = import "L10.UI.CFightingSpiritMatchRecordWnd"
local UIGrid = import "UIGrid"
local Extensions = import "Extensions"
local GameObject = import "UnityEngine.GameObject"
local UITable = import "UITable"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local UILabel = import "UILabel"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local NGUITools = import "NGUITools"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local String = import "System.String"
local Gac2Gas2 = import "L10.Game.Gac2Gas"
local CFightingSpiritMgr = import "L10.Game.CFightingSpiritMgr"

LuaFightingSpiritMatchInfoWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaFightingSpiritMatchInfoWnd, "WildCardScrollview", "WildCardScrollview", CUIRestrictScrollView)
RegistChildComponent(LuaFightingSpiritMatchInfoWnd, "WildCardRaceTeamplate", "WildCardRaceTeamplate", GameObject)
RegistChildComponent(LuaFightingSpiritMatchInfoWnd, "MainRaceTemplate", "MainRaceTemplate", GameObject)
RegistChildComponent(LuaFightingSpiritMatchInfoWnd, "MainRaceTimeLabel", "MainRaceTimeLabel", UILabel)
RegistChildComponent(LuaFightingSpiritMatchInfoWnd, "WildCardTimeLabel", "WildCardTimeLabel", UILabel)
RegistChildComponent(LuaFightingSpiritMatchInfoWnd, "MainRacePositionGrid", "MainRacePositionGrid", GameObject)
RegistChildComponent(LuaFightingSpiritMatchInfoWnd, "WildCardTable", "WildCardTable", UITable)
RegistChildComponent(LuaFightingSpiritMatchInfoWnd, "TeamInfoTemplate", "TeamInfoTemplate", GameObject)
RegistChildComponent(LuaFightingSpiritMatchInfoWnd, "MainRaceTeamSelectBox", "MainRaceTeamSelectBox", QnExpandListBox)
RegistChildComponent(LuaFightingSpiritMatchInfoWnd, "WildCardTeamSelectBox", "WildCardTeamSelectBox", QnExpandListBox)
RegistChildComponent(LuaFightingSpiritMatchInfoWnd, "WildCardRankTeamplate", "WildCardRankTeamplate", GameObject)
RegistChildComponent(LuaFightingSpiritMatchInfoWnd, "RuleDescBtn", "RuleDescBtn", GameObject)
RegistChildComponent(LuaFightingSpiritMatchInfoWnd, "JumpBtn", "JumpBtn", GameObject)
RegistChildComponent(LuaFightingSpiritMatchInfoWnd, "JumpBtnLabel", "JumpBtnLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaFightingSpiritMatchInfoWnd, "m_WildCardRaceData")
RegistClassMember(LuaFightingSpiritMatchInfoWnd, "m_WildCardRankData")
RegistClassMember(LuaFightingSpiritMatchInfoWnd, "m_MainRaceData")
RegistClassMember(LuaFightingSpiritMatchInfoWnd, "m_MainRaceTeamPositionGrid")
RegistClassMember(LuaFightingSpiritMatchInfoWnd, "m_MatchMap")
RegistClassMember(LuaFightingSpiritMatchInfoWnd, "m_PromoteMap")
RegistClassMember(LuaFightingSpiritMatchInfoWnd, "m_MatchRecordReceiveAction")
-- 甲乙丙丁戊
RegistClassMember(LuaFightingSpiritMatchInfoWnd, "m_RankNameList")
-- 一些预期填入数据
RegistClassMember(LuaFightingSpiritMatchInfoWnd, "m_WildCardTimeList")
RegistClassMember(LuaFightingSpiritMatchInfoWnd, "m_MainRaceTimeList")
RegistClassMember(LuaFightingSpiritMatchInfoWnd, "m_MainRacePlayerDesList")
-- 节点
RegistClassMember(LuaFightingSpiritMatchInfoWnd, "TabBar")
RegistClassMember(LuaFightingSpiritMatchInfoWnd, "WildCardRoot")
RegistClassMember(LuaFightingSpiritMatchInfoWnd, "MainRaceRoot")
RegistClassMember(LuaFightingSpiritMatchInfoWnd, "m_TypeMap")
RegistClassMember(LuaFightingSpiritMatchInfoWnd, "m_Level")
RegistClassMember(LuaFightingSpiritMatchInfoWnd, "m_Stage")

function LuaFightingSpiritMatchInfoWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.RuleDescBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRuleDescBtnClick()
	end)


	
	UIEventListener.Get(self.JumpBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnJumpBtnClick()
	end)


    --@endregion EventBind end
end

function LuaFightingSpiritMatchInfoWnd:Init()
    -- 当前阶段和当前level
    self.m_Level = CLuaFightingSpiritMgr:GetMyLevel()
    self.m_Stage = -1

    self.TabBar = self.transform:Find("TitleArea/TabBar"):GetComponent(typeof(UITabBar))
    self.WildCardRoot = self.transform:Find("ShowArea/WildCardRoot").gameObject
    self.MainRaceRoot = self.transform:Find("ShowArea/MainRaceRoot").gameObject

    self.WildCardRaceTeamplate:SetActive(false)
    self.WildCardRankTeamplate:SetActive(false)
    self.MainRaceTemplate:SetActive(false)

    -- 主赛事的位置节点
    self.m_MainRaceTeamPositionGrid = {}
    for i=1, 14 do
        local g = self.MainRacePositionGrid.transform:Find(tostring(i)).gameObject
        table.insert(self.m_MainRaceTeamPositionGrid, g)
    end

    -- 比赛时间
    self.WildCardTimeLabel.text = DouHunTan_Setting.GetData().GroupMatchDate
    self.MainRaceTimeLabel.text = DouHunTan_Setting.GetData().FinalMatchDate

    -- 比赛时间 和 主赛事的预期名单 读表写死
    -- 外卡赛只有四轮
    self.m_WildCardTimeList = {}
    self.m_MainRaceTimeList = {}
    local timeMatchList = {{1,2,3,4}, {5,6,7,8}, {9,10}, {11,12}, {13}, {14}}

    DouHunCross_RoundCron.Foreach(function(key, data)
        if key%2 == 1 and key <= 9 then
            local _, _, mm, hh, MM, yy = string.find(data.CronStr, "(%d+) (%d+) (%d+) (%d+) *")
            if mm == "0" then
                mm = "00"
            end
            self.m_WildCardTimeList[math.floor(key/2)+1] = hh .. ":" .. mm
        elseif key%2 == 1 then
            local _, _, mm, hh, MM, yy = string.find(data.CronStr, "(%d+) (%d+) (%d+) (%d+) *")
            if mm == "0" then
                mm = "00"
            end
            local index = math.floor((key-9)/2)
            local list = timeMatchList[index]

            if list then
                for i=1, #list do
                    self.m_MainRaceTimeList[list[i]] = hh..":"..mm
                end
            end
        end
    end)

    self.m_RankNameList = {}
    local levelName = DouHunCross_Setting.GetData().LevelName
    -- 主赛事
    for i = 0, levelName.Length - 1 do
		table.insert(self.m_RankNameList, levelName[i])
	end
    

    -- 设置主赛事的下拉菜单
    local nameList = CreateFromClass(MakeGenericClass(List, String))
    for i=1, #self.m_RankNameList do
        CommonDefs.ListAdd(nameList, typeof(String), self.m_RankNameList[i])
    end

    self.MainRaceTeamSelectBox:SetItems(nameList)
    self.MainRaceTeamSelectBox:SetNameIndex(0)
    -- level索引从1开始
    self.MainRaceTeamSelectBox.ClickCallback = DelegateFactory.Action_int(
        function(index)
            self:InitMainRaceDefault(index+1)
            Gac2Gas2.QueryCrossDouHunWatchInfo(index+1, EnumDouHunCrossStage.eMain)
        end
    )

    -- 外卡赛下拉菜单
    self.WildCardTeamSelectBox:SetItems(nameList)
    self.WildCardTeamSelectBox:SetNameIndex(0)
    self.WildCardTeamSelectBox.ClickCallback = DelegateFactory.Action_int(
        function(index)
            self:InitWildCardDefault(index+1)
            Gac2Gas2.QueryCrossDouHunWatchInfo(index+1, EnumDouHunCrossStage.eWaiKa)
            Gac2Gas.QueryDouHunCrossWaiKaRank(index+1)
        end
    )

    -- 服务器下发顺序与对应显示场次
    self.m_MatchMap = {
        1, 3, 4, 2, 7, 8, 5, 6, 9, 10, 12, 11, 13, 14 
    }

    -- 类型：0胜者 1败者 2胜败
    self.m_TypeMap = {
        0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 2
    }

    -- [index] = {胜者{{组别，1/2}}，败者}
    self.m_PromoteMap = {
        -- 1,2,3,4
        {{7,1}, {5,1}}, {{7,2},{5,2}}, {{8,1},{6,1}}, {{8,2},{6,2}},
        -- 5,6
        {{9,1}, nil}, {{10,1}, nil},
        -- 7,8
        {{12,1},{10,2}},{{12,2},{9,2}},
        -- 9, 10, 11
        {{11,1}, nil},{{11,2}, nil},{{13,1},nil},
        -- 12
        {{14,1},{13,2}},
        -- 13
        {{14,2}, nil},
        -- 14
        {nil, nil}
    }

    self.m_IsWildCard = true
    
    -- 切换主赛事与外卡赛
    self.TabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function(obj, index)
        self.m_IsWildCard = index == 0
        -- 默认根据level设置
        -- 切换外卡赛
        if self.m_Level < 0 then
            Gac2Gas.QueryDouHunStage()
        else
            self:RequestByLevel(6, self.m_Level)
        end
    end)

    -- 根据OpenCrossDouHunWatchWndResult传入的stage
    -- 判断需要打开哪一个列表
    -- 如果end的话 也不会打开这个界面
    if CFightingSpiritMatchRecordWnd.CurrentStage < EnumDouHunCrossStage.eMainPrepare then
        -- 外卡赛
        self.TabBar:ChangeTab(0, false)
    else
        -- 主赛事
        self.TabBar:ChangeTab(1, false)
    end
end

function LuaFightingSpiritMatchInfoWnd:RequestByLevel(stage, level)
    -- 外卡赛之前默认为1
    self.m_Stage = stage
    if level == 0 then
        level = 1
    end
    self.m_Level = level

    -- 发起请求
    if self.m_IsWildCard then
        self.m_WildCardRankData  = {}
        self.m_WildCardRaceData  = {}
        self.WildCardTeamSelectBox:SetIndex(level-1, true)
        -- 发送排名信息的请求
    else
        self.MainRaceTeamSelectBox:SetIndex(level-1, true)
    end
    
    self.WildCardRoot:SetActive(self.m_IsWildCard)
    self.MainRaceRoot:SetActive(not self.m_IsWildCard)
end

-- 主赛事
function LuaFightingSpiritMatchInfoWnd:InitNameList(level)
    -- 预期名称
    self.m_MainRacePlayerDesList = {}

    local wildList = {1,8,4,5,2,7,3,6}
    for _,index in ipairs(wildList) do
        table.insert(self.m_MainRacePlayerDesList, "[888888]" .. SafeStringFormat3(LocalString.GetString("循环赛第%s名"), index))
    end
    
    local infoList = {}
    for i=1, 4 do
        table.insert(infoList, {i, false})
    end
    for i=1, 4 do
        table.insert(infoList, {i, true})
    end
    table.insert(infoList, {5, true})
    table.insert(infoList, {8, false})
    table.insert(infoList, {6, true})
    table.insert(infoList, {7, false})
    table.insert(infoList, {9, true})
    table.insert(infoList, {10, true})
    table.insert(infoList, {7, true})
    table.insert(infoList, {8, true})
    table.insert(infoList, {11, true})
    table.insert(infoList, {12, false})
    table.insert(infoList, {12, true})
    table.insert(infoList, {13, true})

    for i=1, #infoList do
        local index = infoList[i][1]
        local isSheng = infoList[i][2]
        if isSheng then
            table.insert(self.m_MainRacePlayerDesList, "[888888]" .. SafeStringFormat3(LocalString.GetString("第%s场胜者"), index))
        else
            table.insert(self.m_MainRacePlayerDesList, "[888888]" .. SafeStringFormat3(LocalString.GetString("第%s场败者"), index))
        end
    end
end

-- 设置主赛事显示
-- 数据调用前 应该设置好
function LuaFightingSpiritMatchInfoWnd:InitWildCardShow()
    Extensions.RemoveAllChildren(self.WildCardTable.transform)

    -- 设置对阵
    if self.m_WildCardRaceData then
        self:InitWildCardRace(self.m_WildCardRaceData)
    end
    
    -- 设置排名
    if self.m_WildCardRankList and self.m_WildCardRankDataMap then
        self:InitWildCardRank(self.m_WildCardRankDataMap, self.m_WildCardRankList)
    end
    
    self.WildCardTable:Reposition()
end

-- 刷新跳转按钮设置
function LuaFightingSpiritMatchInfoWnd:Update()
    if self.m_JumpItem then
        local jumpItemY = self.m_JumpItem.transform.localPosition.y
        local scrollY = self.WildCardScrollview.transform.localPosition.y

        if jumpItemY + scrollY > -500 then
            self.m_JumpToTop = true
            self.JumpBtnLabel.text = LocalString.GetString("轮次")
        else
            self.m_JumpToTop = false
            self.JumpBtnLabel.text = LocalString.GetString("排名")
        end
    end
end

function LuaFightingSpiritMatchInfoWnd:InitWildCardDefault(level)
    local fakeData = {}
    fakeData.Count = 0
    self:InitWildCardRaceData(level, fakeData)
end

function LuaFightingSpiritMatchInfoWnd:UpdateRankData(name, round, opponent, hasWin)
    local data = self.m_WildCardRankDataMap[name]

    if data == nil then
        data = {}
        data.name = name
        data.winCount = 0
        data.loseCount = 0
        data.opponentList = {}
        data.winMarkList = {}
        self.m_WildCardRankDataMap[name] = data
    end

    if hasWin then
        data.winCount = data.winCount + 1
    else
        data.loseCount = data.loseCount + 1
    end
    data.opponentList[round] = opponent
    data.winMarkList[round] = hasWin
end

-- 初始化外卡赛数据
function LuaFightingSpiritMatchInfoWnd:InitWildCardRaceData(level, matchList)
    self.m_WildCardRaceData  = {}
    local setting = DouHunCross_Setting.GetData()
    local timeList = setting.FirstDayTime

    --local timeList = {"14:00", "14:30", "15:00", "15:30", "16:00"}
    -- 五轮的信息
    for i=1, 5 do
        local race = {}
        race.title = SafeStringFormat3(LocalString.GetString("第%s轮 %s"), Extensions.ToChinese(i), timeList[i-1])
        race.voidTitle = SafeStringFormat3(LocalString.GetString("第%s轮结束后根据积分对阵"), Extensions.ToChinese(i-1))
        race.teamData = {}
        table.insert(self.m_WildCardRaceData , race)
    end

    local map1 = {1,5,2,6,3,7,4,8}
    local map2 = {1,4,2,5,3,6}
    local map3 = {1,3,2}

    local mapList = {map1, map1, map1, map2, map3}
    local statIndex = {0,8,16,24,30}

    -- 先按名字做map
    -- 名称 总胜负 对阵情况
    -- list用于显示
    self.m_WildCardRankDataList = {}
    -- map用来设置
    self.m_WildCardRankDataMap = {}

    if matchList.Count > 0 then
        -- 数据是按顺序发的
        -- 总共五轮 一轮八组对阵
        for i=1, 5 do
            local race = self.m_WildCardRaceData[i]

            -- 显示数据的时候 需要按 1 5 2 6这样塞进去
            local map = mapList[i]
            for ii, j in ipairs(map) do
                local index = statIndex[i] + j

                if matchList.Count >= index then
                    local matchData = matchList[index-1]

                    local data = {}
                    if matchData.CanWatch then
                        data.state = 1
                    elseif matchData.WinnerServerID >0 then
                        data.state = 2
                    else
                        data.state = 3
                    end

                    data.name1 = matchData.ServerName1
                    data.name2 = matchData.ServerName2
                    data.serverID1 = matchData.ServerID1
                    data.serverID2 = matchData.ServerID2
                    data.num = j
                    if 1 == matchData.WinnerServerID then
                        data.score1 = matchData.Score1
                        data.score2 = matchData.Score2
                    else
                        data.score1 = matchData.Score2
                        data.score2 = matchData.Score1
                    end
                    
                    data.matchIndex = matchData.MatchIndex
                    data.winnerServerID = matchData.WinnerServerID
                    table.insert(race.teamData, data)

                    -- 更新排名数据
                    if matchData.WinnerServerID >0 then
                        self:UpdateRankData(data.name1, i, data.name2, 1 == matchData.WinnerServerID)
                        self:UpdateRankData(data.name2, i, data.name1, 2 == matchData.WinnerServerID)
                    end
                end
            end
        end
    else
        -- 第一组数据可能需要手动创建
        local isGuanWang = true
        if CClientMainPlayer.Inst then
            if DouHunCross_YingHeServer.Exists(CClientMainPlayer.Inst:GetMyServerId()) then
                isGuanWang = false
            end
        end
    
        local serverData = DouHunCross_YingHeServer
        if isGuanWang then
            serverData = DouHunCross_GuanWangServer
        end

        -- 只修改第一轮就可以
        local firstRace = self.m_WildCardRaceData[1]
        local serverName = {}
        local serverId = {}

        local index2ServerId = {}

        serverData.Foreach(function(k, data)
            index2ServerId[data.Index] = data.ServerId
        end)

        for i = 1, 8 do
            local index = i + (level-1) * 8
            local curServerId = index2ServerId[index]
            if curServerId then
                local id = curServerId
                table.insert(serverName, PlayConfig_ServerIdEnabled.GetData(id).ServerName)
                table.insert(serverId, id)
            end
        end

        for i = 1, 8 do
            local j = mapList[1][i]
            local data = {}

            data.state = 4

            data.name1 = SafeStringFormat3(LocalString.GetString("%s·[fb3ffd]乾"), serverName[j])
            data.clientServerId1 = serverId[j]
            data.clientIndex1 = 1
            data.name2 = SafeStringFormat3(LocalString.GetString("%s·[edb743]坤"), serverName[9-j])
            data.clientServerId2 = serverId[9-j]
            data.clientIndex2 = 2
            data.num = j
            table.insert(firstRace.teamData, data)
        end
    end

    for k, v in pairs(self.m_WildCardRankDataMap) do
        table.insert(self.m_WildCardRankDataList, v)
    end

    table.sort(self.m_WildCardRankDataList, function(a, b)
        if a.winCount == b.winCount then
            return CommonDefs.StringCompareTo(a.name, b.name) > 0
        else
            return a.winCount > b.winCount
        end
    end)

    self:InitWildCardShow()
end

-- 初始化外卡赛排名
function LuaFightingSpiritMatchInfoWnd:OnWaiKaRankData(rankList)
    -- {groupId, groupInfo.groupName, groupInfo.waiKaWinCount, groupInfo.waiKaLoseCount, groupInfo.waiKaOppGroupId}
    self.m_WildCardRankList = rankList
    self:InitWildCardShow()
end

-- 根据数据设置外卡赛排名
function LuaFightingSpiritMatchInfoWnd:InitWildCardRank(rankMap, rankList)
    local rankItem = NGUITools.AddChild(self.WildCardTable.gameObject, self.WildCardRankTeamplate)
    rankItem:SetActive(true)

    self.m_JumpItem = rankItem
    self.m_RealJumpItem = rankItem.transform:Find("Title").gameObject
    
    local table = rankItem.transform:Find("TeamInfoTable"):GetComponent(typeof(UITable))
    local template = rankItem.transform:Find("TeamInfoTemplate").gameObject

    template:SetActive(false)
    table.gameObject:SetActive(true)

    Extensions.RemoveAllChildren(table.transform)

    for i=1, #rankList do
        local g = NGUITools.AddChild(table.gameObject, template)
        g.gameObject:SetActive(true)

        if i <= 8 then
            self.m_RealJumpItem = g.gameObject
        end

        local numLabel = g.transform:Find("Num"):GetComponent(typeof(UILabel))
        local teamNameLabel = g.transform:Find("TeamName"):GetComponent(typeof(UILabel))
        local score = g.transform:Find("Score"):GetComponent(typeof(UILabel))
        local encounter = g.transform:Find("Table")

        
        -- {groupId, groupInfo.groupName, groupInfo.waiKaWinCount, groupInfo.waiKaLoseCount, groupInfo.waiKaOppGroupId}
        local rankData = rankList[i]
        local teamName = CFightingSpiritMgr.SetServerNameColor(tostring(rankData[2]))

        local rankMapData = rankMap[tostring(teamName)]

        numLabel.text = tostring(i)
        teamNameLabel.text = LocalString.TranslateAndFormatText(teamName)
        score.text = SafeStringFormat3(LocalString.GetString("[00FF00]%s[-]胜[FF0000]%s[-]败"), tostring(rankData[3]), tostring(rankData[4])) 

        -- 设置自己队伍的颜色
        if self.m_OwnGroupId and self.m_OwnGroupId ~= "" and rankData[1] == self.m_OwnGroupId then
            teamNameLabel.color = Color(0,1,96/255,1)
        else
            teamNameLabel.color = Color(1,1,1,1)
        end
        
        if rankMapData then
            local opList = rankMapData.opponentList
            local winList = rankMapData.winMarkList

            for j=1,5 do
                local item = encounter:Find(tostring(j))

                local nameLabel = item:GetComponent(typeof(UILabel))
                local bg = item:Find("Bg"):GetComponent(typeof(UISprite))

                -- 对阵名称
                if opList[j] then
                    item.gameObject:SetActive(true)
                    nameLabel.text = LocalString.TranslateAndFormatText(opList[j])

                    -- 判断胜负
                    if winList[j] then
                        bg.color = Color(77/255,128/255,214/255,50/255)
                    else
                        bg.color = Color(77/255,128/255,214/255,0/255)
                    end
                else
                    item.gameObject:SetActive(false)
                end
            end
        else
            for j=1,5 do
                local item = encounter:Find(tostring(j))
                item.gameObject:SetActive(false)
            end
        end
    end

    table:Reposition()

    local bg = rankItem.transform:Find("Bg"):GetComponent(typeof(UISprite))
    bg.gameObject:SetActive(true)
    local box = NGUIMath.CalculateRelativeWidgetBounds(rankItem.transform)
    bg.height = box.size.y
end

-- 初始化外卡赛节点
function LuaFightingSpiritMatchInfoWnd:InitWildCardRace(wildCardData)
    for i=1, #wildCardData do
        local g = NGUITools.AddChild(self.WildCardTable.gameObject, self.WildCardRaceTeamplate)
        g:SetActive(true)
        self:InitWildCardRaceItem(g, wildCardData[i],i)
    end
    self.WildCardTable:Reposition()
end

-- 初始化外卡赛比赛信息，有多个队伍的比赛
-- RaceData:
-- title:比赛名称
-- teamData[]:具体对决信息
function LuaFightingSpiritMatchInfoWnd:InitWildCardRaceItem(item, raceData, index)
    local titleLabel = item.transform:Find("TitleBg/TitleLabel"):GetComponent(typeof(UILabel))
    local voidLabel = item.transform:Find("VoidLabel"):GetComponent(typeof(UILabel))
    local teamTable = item.transform:Find("TeamInfoTable"):GetComponent(typeof(UITable))
    local template = item.transform:Find("TeamInfoTemplate").gameObject

    template:SetActive(false)

    titleLabel.text = raceData.title
    voidLabel.text = raceData.voidTitle
    voidLabel.gameObject:SetActive(#raceData.teamData == 0)

    Extensions.RemoveAllChildren(teamTable.transform)
    
    for i=1, #raceData.teamData do
        local g = NGUITools.AddChild(teamTable.gameObject, template)
        g:SetActive(true)
        self:InitWildCardTeamItem(g, raceData.teamData[i],i)
    end

    teamTable:Reposition()
end

-- 初始化外卡赛队伍信息
--  state:三种状态 1可观战2已结束3未开始 
--  name1:队伍名称1 name2:队伍名称2  
--  num:序号 
--  time:时间 
--  score1:分数1 score2:分数2
function LuaFightingSpiritMatchInfoWnd:InitWildCardTeamItem(item, teamData,index)
    local numLabel = item.transform:Find("Num"):GetComponent(typeof(UILabel))
    local teamNameLabel1 = item.transform:Find("TeamName1"):GetComponent(typeof(UILabel))
    local teamNameLabel2 = item.transform:Find("TeamName2"):GetComponent(typeof(UILabel))
    local winMark1 = item.transform:Find("TeamName1/WinMark").gameObject
    local winMark2 = item.transform:Find("TeamName2/WinMark").gameObject
    local scoreLabel = item.transform:Find("ScoreLabel"):GetComponent(typeof(UILabel))
    local viewBtn = item.transform:Find("ViewBtn").gameObject
    local vsMark = item.transform:Find("Vs").gameObject

    local bg = item.transform:Find("Bg"):GetComponent(typeof(UISprite))
    if math.floor(index/2)%2==0 then
        bg.color = Color(1,1,1,35/255)
    else
        bg.color = Color(0,0,0,35/255)
    end

    teamNameLabel1.text = LocalString.TranslateAndFormatText(teamData.name1)
    teamNameLabel2.text = LocalString.TranslateAndFormatText(teamData.name2)
    numLabel.text = tostring(teamData.num)

    -- 设置自己队伍的颜色
    if self.m_OwnGroupId and self.m_OwnGroupId ~= "" and teamData.serverID1 == self.m_OwnGroupId then
        teamNameLabel1.color = Color(0,1,96/255,1)
    else
        teamNameLabel1.color = Color(1,1,1,1)
    end

    if self.m_OwnGroupId and self.m_OwnGroupId ~= "" and teamData.serverID2 == self.m_OwnGroupId then
        teamNameLabel2.color = Color(0,1,96/255,1)
    else
        teamNameLabel2.color = Color(1,1,1,1)
    end

    local state = teamData.state
    viewBtn:SetActive(state == 1)
    scoreLabel.gameObject:SetActive(state == 2)
    vsMark:SetActive(state ~= 2)
    winMark1:SetActive(false)
    winMark2:SetActive(false)

    UIEventListener.Get(teamNameLabel1.gameObject).onClick = DelegateFactory.VoidDelegate(function (g)
        if teamData.serverID1 and teamData.serverID1 ~= "" then
            Gac2Gas.QueryDouHunCrossServerGroupInfo(teamData.serverID1)
        elseif teamData.clientServerId1 and teamData.clientIndex1 then
            Gac2Gas.QueryDouHunJoinCrossGroupInfo(teamData.clientServerId1, teamData.clientIndex1)
        end
    end)
    UIEventListener.Get(teamNameLabel2.gameObject).onClick = DelegateFactory.VoidDelegate(function (g)
        if teamData.serverID2 and teamData.serverID2 ~= "" then
            Gac2Gas.QueryDouHunCrossServerGroupInfo(teamData.serverID2)
        elseif teamData.clientServerId2 and teamData.clientIndex2 then
            Gac2Gas.QueryDouHunJoinCrossGroupInfo(teamData.clientServerId2, teamData.clientIndex2)
        end
    end)

    if state == 1 then
        -- 可观战
        UIEventListener.Get(viewBtn).onClick = DelegateFactory.VoidDelegate(function (g)
            if CLuaFightingSpiritMgr:CanViewMatch() then
                -- 第一个参数用不到了
                Gac2Gas2.RequestWatchCrossDouHun(0, teamData.matchIndex)
            end
        end)
    elseif state == 2 then
        -- 已结束
        if teamData.winnerServerID == 1 then
            winMark1:SetActive(true)
            scoreLabel.text = SafeStringFormat3("[4B8DFD]%s[-] : [9A697E]%s[-]", tostring(teamData.score1), tostring(teamData.score2))
        else
            winMark2:SetActive(true)
            scoreLabel.text = SafeStringFormat3("[9A697E]%s[-] : [4B8DFD]%s[-]", tostring(teamData.score1), tostring(teamData.score2))
        end
    end
end

-- 初始化主赛事数据
function LuaFightingSpiritMatchInfoWnd:InitMainRaceData(matchList)
    self.m_MainRaceData = {}
    for i= 1, 14 do
        local data = {}
        data.name1 =  self.m_MainRacePlayerDesList[i*2 -1]
        data.name2 =  self.m_MainRacePlayerDesList[i*2]
        data.num = i
        data.time = self.m_MainRaceTimeList[i] or "00:00"
        -- 未开始
        data.state = 3
        data.type = self.m_TypeMap[i]
        table.insert(self.m_MainRaceData, data)
    end

    -- 长度为14
    for i= 0, matchList.Count-1 do
        local matchData = matchList[i]
        local realIndex = self.m_MatchMap[i+1]
        local data = self.m_MainRaceData[realIndex]

        -- 本节点信息
        data.name1 = matchData.ServerName1
        data.name2 = matchData.ServerName2
        data.serverID1 = matchData.ServerID1
        data.serverID2 = matchData.ServerID2

        if 1 == matchData.WinnerServerID then
            data.score1 = matchData.Score1
            data.score2 = matchData.Score2
        else
            data.score1 = matchData.Score2
            data.score2 = matchData.Score1
        end
        
        data.matchIndex = matchData.MatchIndex
        data.winnerServerID = matchData.WinnerServerID

        -- 更新状态
        if matchData.CanWatch then
            data.state = 1
        elseif matchData.WinnerServerID >0 then
            data.state = 2
            -- 推送到下一次可能的比赛
            local promoteData = self.m_PromoteMap[realIndex]

            if #promoteData > 0 then
                local pairList = {}
                if matchData.WinnerServerID == 1 then
                    table.insert(pairList, {data.name1, data.serverID1})
                    table.insert(pairList, {data.name2, data.serverID2})
                else
                    table.insert(pairList, {data.name2, data.serverID2})
                    table.insert(pairList, {data.name1, data.serverID1})
                end

                for j=1, #promoteData do
                    local pair = pairList[j]
                    if promoteData[j][2] == 1 then
                        self.m_MainRaceData[promoteData[j][1]].name1 = pair[1]
                        self.m_MainRaceData[promoteData[j][1]].serverID1 = pair[2]
                    else
                        self.m_MainRaceData[promoteData[j][1]].name2 = pair[1]
                        self.m_MainRaceData[promoteData[j][1]].serverID2 = pair[2]
                    end
                end
            end
        else
            data.state = 3
        end
    end

    self:InitMainRaceRoot(self.m_MainRaceData)
end

function LuaFightingSpiritMatchInfoWnd:InitMainRaceRoot(mainRaceData)
    for i=1, #mainRaceData do
        local root = self.m_MainRaceTeamPositionGrid[i]
        Extensions.RemoveAllChildren(root.transform)
        local g = NGUITools.AddChild(root, self.MainRaceTemplate)
        g:SetActive(true)
        self:InitMaineamItem(g, mainRaceData[i])
    end
end

function LuaFightingSpiritMatchInfoWnd:InitMainRaceDefault(level)
    if self.m_Stage > 0 and self.m_Stage <= 6 then
        self:InitNameList(level)
        local fakeData = {}
        fakeData.Count = 0
        self:InitMainRaceData(fakeData)
    end
end

function LuaFightingSpiritMatchInfoWnd:InitMaineamItem(item, teamData)
    local numLabel = item.transform:Find("HeadSprite/Num"):GetComponent(typeof(UILabel))
    local headSprite = item.transform:Find("HeadSprite"):GetComponent(typeof(UISprite))
    local halfSprite = item.transform:Find("HeadSprite/HalfSprite"):GetComponent(typeof(UISprite))
    local teamNameLabel1 = item.transform:Find("TeamName1"):GetComponent(typeof(UILabel))
    local teamNameLabel2 = item.transform:Find("TeamName2"):GetComponent(typeof(UILabel))
    local winMark1 = item.transform:Find("TeamName1/WinMark").gameObject
    local winMark2 = item.transform:Find("TeamName2/WinMark").gameObject
    local finalWinMark1 = item.transform:Find("TeamName1/Final").gameObject
    local finalWinMark2 = item.transform:Find("TeamName2/Final").gameObject
    local timeLabel = item.transform:Find("TimeLabel"):GetComponent(typeof(UILabel))
    local scoreLabel1 = item.transform:Find("TeamName1/Score"):GetComponent(typeof(UILabel))
    local scoreLabel2 = item.transform:Find("TeamName2/Score"):GetComponent(typeof(UILabel))
    local viewBtn = item.transform:Find("ViewBtn").gameObject
    local battleIcon = item.transform:Find("BattleIcon").gameObject

    teamNameLabel1.text = LocalString.TranslateAndFormatText(teamData.name1)
    teamNameLabel2.text = LocalString.TranslateAndFormatText(teamData.name2)
    numLabel.text = tostring(teamData.num)

    -- 设置自己队伍的颜色
    if self.m_OwnGroupId and self.m_OwnGroupId ~= "" and teamData.serverID1 == self.m_OwnGroupId then
        teamNameLabel1.color = Color(0,1,96/255,1)
    else
        teamNameLabel1.color = Color(1,1,1,1)
    end

    if self.m_OwnGroupId and self.m_OwnGroupId ~= "" and teamData.serverID2 == self.m_OwnGroupId then
        teamNameLabel2.color = Color(0,1,96/255,1)
    else
        teamNameLabel2.color = Color(1,1,1,1)
    end

    UIEventListener.Get(teamNameLabel1.gameObject).onClick = DelegateFactory.VoidDelegate(function (g)
        if teamData.serverID1 and teamData.serverID1 ~= "" then
            Gac2Gas.QueryDouHunCrossServerGroupInfo(teamData.serverID1)
        elseif teamData.clientServerId1 and teamData.clientIndex1 then
            Gac2Gas.QueryDouHunJoinCrossGroupInfo(teamData.clientServerId1, teamData.clientIndex1)
        end
    end)
    UIEventListener.Get(teamNameLabel2.gameObject).onClick = DelegateFactory.VoidDelegate(function (g)
        if teamData.serverID2 and teamData.serverID2 ~= "" then
            Gac2Gas.QueryDouHunCrossServerGroupInfo(teamData.serverID2)
        elseif teamData.clientServerId2 and teamData.clientIndex2 then
            Gac2Gas.QueryDouHunJoinCrossGroupInfo(teamData.clientServerId2, teamData.clientIndex2)
        end
    end)

    if teamData.type == 0 then
        --胜者
        headSprite.color = Color(84/255, 119/255, 170/255, 1)
        halfSprite.color = Color(84/255, 119/255, 170/255, 1)
    elseif teamData.type == 1 then
        --败者
        headSprite.color = Color(147/255, 99/255, 113/255, 1)
        halfSprite.color = Color(147/255, 99/255, 113/255, 1)
    else
        --胜败
        headSprite.color = Color(84/255, 119/255, 170/255, 1)
        halfSprite.color = Color(147/255, 99/255, 113/255, 1)
    end

    local state = teamData.state
    viewBtn:SetActive(state == 1)
    scoreLabel1.gameObject:SetActive(state == 2)
    scoreLabel2.gameObject:SetActive(state == 2)
    battleIcon:SetActive(state == 1)
    winMark1:SetActive(false)
    winMark2:SetActive(false)
    finalWinMark1:SetActive(false)
    finalWinMark2:SetActive(false)
    timeLabel.gameObject:SetActive(state == 3)

    if state == 1 then
        -- 可观战
        UIEventListener.Get(viewBtn).onClick = DelegateFactory.VoidDelegate(function ()
            if CLuaFightingSpiritMgr:CanViewMatch() then
                Gac2Gas2.RequestWatchCrossDouHun(0, teamData.matchIndex)
            end
        end)
    elseif state == 2 then
        -- 已结束
        if teamData.winnerServerID == 1 then
            -- 自己战队不处理灰色
            if not(self.m_OwnGroupId and self.m_OwnGroupId ~= "" and teamData.serverID2 == self.m_OwnGroupId) then
                teamNameLabel2.text = "[888888]" .. LocalString.TranslateAndFormatText(teamData.name2)
            end

            if teamData.num == 14 then
                finalWinMark1:SetActive(true)
            else
                winMark1:SetActive(true)
            end
            scoreLabel1.text = SafeStringFormat3("[4B8DFD]%s[-]", tostring(teamData.score1))
            scoreLabel2.text = SafeStringFormat3("[9A697E]%s[-]",  tostring(teamData.score2))
        else

            -- 自己战队不处理灰色
            if not (self.m_OwnGroupId and self.m_OwnGroupId ~= "" and teamData.serverID1 ~= self.m_OwnGroupId) then
                teamNameLabel1.text = "[888888]" .. LocalString.TranslateAndFormatText(teamData.name1)
            end

            if teamData.num == 14 then
                finalWinMark2:SetActive(true)
            else
                winMark2:SetActive(true)
            end
            scoreLabel1.text = SafeStringFormat3("[9A697E]%s[-]", tostring(teamData.score1))
            scoreLabel2.text = SafeStringFormat3("[4B8DFD]%s[-]", tostring(teamData.score2))
        end
    elseif state == 3 then
        -- 未开始
        timeLabel.text = teamData.time
    end
end

function LuaFightingSpiritMatchInfoWnd:OnEnable()
    -- level级别 第二级索引
    if self.m_MatchRecordReceiveAction == nil then
        self.m_MatchRecordReceiveAction = DelegateFactory.Action_uint_uint_List_CFightingSpiritMatch_string(function(level, stage, dataList, ownGroupId)
            self.m_OwnGroupId = ownGroupId
            if stage == EnumDouHunCrossStage.eWaiKa then
                -- 外卡赛
                self:InitWildCardRaceData(level, dataList)
            elseif stage == EnumDouHunCrossStage.eMain then
                -- 主赛事
                self:InitNameList(level)
                self:InitMainRaceData(dataList)
            end
        end)
    end
    EventManager.AddListenerInternal(EnumEventType.FightingSpiritMatchInfoReceive, self.m_MatchRecordReceiveAction)
    g_ScriptEvent:AddListener("QueryDouHunStageResult", self, "RequestByLevel")
    g_ScriptEvent:AddListener("QueryDouHunCrossWaiKaRankResult", self, "OnWaiKaRankData")
end

function LuaFightingSpiritMatchInfoWnd:OnDisable()
    EventManager.RemoveListenerInternal(EnumEventType.FightingSpiritMatchInfoReceive, self.m_MatchRecordReceiveAction)
    g_ScriptEvent:RemoveListener("QueryDouHunStageResult", self, "RequestByLevel")
    g_ScriptEvent:RemoveListener("QueryDouHunCrossWaiKaRankResult", self, "OnWaiKaRankData")
end

--@region UIEvent

function LuaFightingSpiritMatchInfoWnd:OnRuleDescBtnClick()
    g_MessageMgr:ShowMessage("DouHun_MatchInfo_WaiKa_Tip")
end


function LuaFightingSpiritMatchInfoWnd:OnJumpBtnClick()
    if self.m_JumpItem then
        if self.m_JumpToTop then
            self.WildCardScrollview:ResetPosition()
        else
            if self.m_RealJumpItem then
                CUICommonDef.SetFullyVisible(self.m_RealJumpItem.gameObject, self.WildCardScrollview)
            end
        end
    end
end


--@endregion UIEvent

