-- Auto Generated!!
local AssistantMessage = import "L10.Game.AssistantMessage"
local AssistantMessageType = import "L10.Game.AssistantMessageType"
local CContactAssistantAnswerWnd = import "L10.UI.CContactAssistantAnswerWnd"
local CContactAssistantMgr = import "L10.Game.CContactAssistantMgr"
local CContactAssistantWnd = import "L10.UI.CContactAssistantWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local ContactAssistantDataMgr = import "L10.Game.ContactAssistantDataMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local DateTime = import "System.DateTime"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Gac2Gas = import "L10.Game.Gac2Gas"
local GameObject = import "UnityEngine.GameObject"
local Gas2Gac = import "L10.Game.Gas2Gac"
local Int32 = import "System.Int32"
local L10 = import "L10"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local MsgPackImpl = import "MsgPackImpl"
local Object = import "System.Object"
local String = import "System.String"
local TimeSpan = import "System.TimeSpan"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local VoiceManager = import "VoiceManager"
local VoidDelegate = import "UIEventListener+VoidDelegate"



Gas2Gac.m_SyncSubmitSettingQuestionResult_CS2LuaHook = function (this, message) 
    if not StringStartWith(message, "1:") then
        g_MessageMgr:ShowMessage("Illegal_Character")
        return
    end
    if CContactAssistantMgr.s_OpenWndAfterSubmit then
        CContactAssistantMgr.Inst:ShowContactAssistantAnswerWnd()
    else
        Gac2Gas.GetQuestionsByUrs()
    end
end
Gas2Gac.m_SyncSubmitAppealQuestionResult_CS2LuaHook = function (this, message) 
    if not StringStartWith(message, "1:") then
        g_MessageMgr:ShowMessage("Illegal_Character")
        return
    end

    Gac2Gas.GetQuestionsByUrs()
end
Gas2Gac.m_SyncEvaluateAnswerResult_CS2LuaHook = function (this, message) 
    if StringStartWith(message, "200") then
        g_MessageMgr:ShowMessage("ZHUANQU_PINGJIA_SUCCESS")
    end
    Gac2Gas.GetQuestionsByUrs()
end
Gas2Gac.m_SyncGetAnswerResult_CS2LuaHook = function (this, message) 
    EventManager.BroadcastInternalForLua(EnumEventType.CheckContactAssistantState, {message})
end
Gas2Gac.m_SyncGetDetailResult_CS2LuaHook = function (this, message) 
    local dataArray = CommonDefs.StringSplit_ArrayChar(message, "\n")

    if dataArray.Length <= 0 then
        L10.CLogMgr.Log("SyncGetDetailResult Message:" .. message)
        return
    end

    local result = dataArray[0]
    if (result == "200") then
        local resultData = dataArray[2]
        local jsonDict = TypeAs(L10.Game.Utils.Json.Deserialize(resultData), typeof(MakeGenericClass(Dictionary, String, Object)))
        local am = CreateFromClass(AssistantMessage)
        am.msgType = AssistantMessageType.Question
        am.info = jsonDict
        if CContactAssistantWnd.Instance ~= nil then
            CommonDefs.ListClear(CContactAssistantWnd.Instance.assistantMessageList)
            CommonDefs.ListAdd(CContactAssistantWnd.Instance.assistantMessageList, typeof(AssistantMessage), am)
            CContactAssistantWnd.Instance:updateAssistantReplyView()
        end
        if CContactAssistantAnswerWnd.Instance ~= nil then
            CommonDefs.ListClear(CContactAssistantAnswerWnd.Instance.assistantMessageList)
            CommonDefs.ListAdd(CContactAssistantAnswerWnd.Instance.assistantMessageList, typeof(AssistantMessage), am)
            CContactAssistantAnswerWnd.Instance:updateAssistantReplyView()
        end
        local qid = ToStringWrap(CommonDefs.DictGetValue(jsonDict, typeof(String), "qid"))
        --Gac2Gas.GetAnswerByQuestionId(qid);
    else
        --fail
        L10.CLogMgr.Log("SyncGetDetailResult Message:" .. message)
    end
end
Gas2Gac.m_SyncGetQuestionsResult_CS2LuaHook = function (this, message) 
    local dataArray = CommonDefs.StringSplit_ArrayChar(message, "\n")

    if dataArray.Length <= 0 then
        L10.CLogMgr.Log("SyncGetDetailResultFail Message:" .. message)
        return
    end

    local result = dataArray[0]
    if (result == "200") then
        local resultData = dataArray[2]
        local jsonList = TypeAs(L10.Game.Utils.Json.Deserialize(resultData), typeof(MakeGenericClass(List, Object)))

        --L10.CLogMgr.Log("jsonList.Count:" + jsonList.Count);

        if jsonList == nil or jsonList.Count == 0 then
            if CContactAssistantWnd.Instance ~= nil then
                CContactAssistantWnd.Instance:setInitState(false, nil)
            end
            if CContactAssistantAnswerWnd.Instance ~= nil then
                CContactAssistantAnswerWnd.Instance:setInitState(false, nil)
            end
        else
            --					Dictionary<string, object> jsonDict = jsonList[0] as Dictionary<string, object>;
            --					string qid = jsonDict["qid"].ToString();
            if CContactAssistantWnd.Instance ~= nil then
                CContactAssistantWnd.Instance:setInitState(true, jsonList)
            end
            if CContactAssistantAnswerWnd.Instance ~= nil then
                CContactAssistantAnswerWnd.Instance:setInitState(true, jsonList)
            end
        end
    else
        L10.CLogMgr.Log("SyncGetDetailResultFail Message:" .. message)
    end
end
Gas2Gac.m_SyncMaintenanceZoneTags_CS2LuaHook = function (this, star, tagUD) 
    local tagInfo = TypeAs(MsgPackImpl.unpack(tagUD), typeof(MakeGenericClass(List, Object)))
    if tagInfo ~= nil then
        local dataDic = CreateFromClass(MakeGenericClass(Dictionary, Int32, String))
        do
            local i = 0
            while i < tagInfo.Count do
                CommonDefs.DictSet(dataDic, typeof(Int32), i + 1, typeof(String), ToStringWrap(tagInfo[i]))
                i = i + 1
            end
        end
        CommonDefs.DictSet(ContactAssistantDataMgr.Inst.evaluateSignDic, typeof(UInt32), star, typeof(MakeGenericClass(Dictionary, Int32, String)), dataDic)
        EventManager.BroadcastInternalForLua(EnumEventType.EvaluateContactAssistantSign, {star})
    end
end
Gas2Gac.m_SyncPropertyProtect_CS2LuaHook = function (this, isMaintenanceZoneOpen, protectLeftTime) 
    if isMaintenanceZoneOpen then
        MessageWndManager.ShowConfirmMessage(g_MessageMgr:FormatMessage("PROTECT_ALERT_MESS_1", protectLeftTime), 4, false, nil, nil)
    else
        MessageWndManager.ShowConfirmMessage(g_MessageMgr:FormatMessage("PROTECT_ALERT_MESS_2", protectLeftTime), 4, false, nil, nil)
    end
end

CContactAssistantWnd.m_Awake_CS2LuaHook = function (this) 
    if CContactAssistantWnd.Instance ~= nil then
        L10.CLogMgr.LogError("more than one instance running!")
    end
    CContactAssistantWnd.Instance = this
end
CContactAssistantWnd.m_SetPicBack_CS2LuaHook = function (this, strResult) 
    if this.punishExplainView.gameObject.activeSelf then
        this.punishExplainView:OnAvaterCallBack(strResult)
    elseif this.gameSetView.gameObject.activeSelf then
        this.gameSetView:OnAvaterCallBack(strResult)
    end
end
CContactAssistantWnd.m_setInitState_CS2LuaHook = function (this, sign, _list) 
    --needAnswerState = sign;
    this.answerList = _list
    this.needAnswerState = this:InitReplyData()
    if CContactAssistantMgr.Inst.tabIndex == - 1 then
        this.needAnswerState = true
    end

    UIEventListener.Get(this.checkStateBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        this:CheckState(_list)
    end)

    --needAnswerState = false;
    if this.needAnswerState then
        this.tabBar:ChangeTab(1, false)
    else
        this.tabBar:ChangeTab(0, false)
    end
end
CContactAssistantWnd.m_CheckState_CS2LuaHook = function (this, list) 
    if list == nil or list.Count <= 0 then
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", CContactAssistantWnd.NoStateAlertString)
        return
    end

    local jsonDict = TypeAs(list[0], typeof(MakeGenericClass(Dictionary, String, Object)))
    local qid = System.UInt64.Parse(ToStringWrap(CommonDefs.DictGetValue(jsonDict, typeof(String), "qid")))
    CContactAssistantMgr.Inst.checkStateQid = qid
    local content = TypeAs(CommonDefs.DictGetValue(jsonDict, typeof(String), "content"), typeof(MakeGenericClass(Dictionary, String, Object)))
    local qtitle = ToStringWrap((TypeAs(CommonDefs.DictGetValue(content, typeof(String), "qtitle"), typeof(MakeGenericClass(List, Object))))[0])
    local remark = ToStringWrap((TypeAs(CommonDefs.DictGetValue(content, typeof(String), "remark"), typeof(MakeGenericClass(List, Object))))[0])

    qtitle = CContactAssistantMgr.Inst:FilterTitlePrefix(qtitle)
    remark = CContactAssistantMgr.Inst:FilterContentExtraInfo(remark)
    local showText = (("[" .. qtitle) .. "]") .. remark
    CContactAssistantMgr.Inst.checkStateInfo = showText
    CUIManager.ShowUI(CUIResources.ContactAssistantCheckWnd)
end
CContactAssistantWnd.m_MakePhoneCall_CS2LuaHook = function (this, go) 
    MessageWndManager.ShowOKCancelMessage(CContactAssistantWnd.PhoneCallNum, DelegateFactory.Action(function () 
        this.m_Native:MakePhoneCall(CContactAssistantWnd.PhoneCallNum)
    end), nil, LocalString.GetString("拨打"), nil, false)
end
CContactAssistantWnd.m_OnEnable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeBtn).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), true)
    this.tabBar.OnTabChange = CommonDefs.CombineListner_Action_GameObject_int(this.tabBar.OnTabChange, MakeDelegateFromCSFunction(this.OnTabChange, MakeGenericClass(Action2, GameObject, Int32), this), true)
    this.subTabBar.OnTabChange = CommonDefs.CombineListner_Action_GameObject_int(this.subTabBar.OnTabChange, MakeDelegateFromCSFunction(this.OnSubTabChange, MakeGenericClass(Action2, GameObject, Int32), this), true)
    VoiceManager.onPicMsg = MakeDelegateFromCSFunction(this.SetPicBack, MakeGenericClass(Action1, String), this)
end
CContactAssistantWnd.m_OnDisable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeBtn).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), false)
    this.tabBar.OnTabChange = CommonDefs.CombineListner_Action_GameObject_int(this.tabBar.OnTabChange, MakeDelegateFromCSFunction(this.OnTabChange, MakeGenericClass(Action2, GameObject, Int32), this), false)
    this.subTabBar.OnTabChange = CommonDefs.CombineListner_Action_GameObject_int(this.subTabBar.OnTabChange, MakeDelegateFromCSFunction(this.OnSubTabChange, MakeGenericClass(Action2, GameObject, Int32), this), false)
    VoiceManager.onPicMsg = nil
end
CContactAssistantWnd.m_OnCloseButtonClick_CS2LuaHook = function (this, go) 
    CUIManager.ShowUIByChangeLayer(CUIResources.JingLingWnd)
    this:Close()
end
CContactAssistantWnd.m_InitReplyData_CS2LuaHook = function (this) 
    local answerState = false

    CommonDefs.ListClear(this.assistantMessageList)
    local sysTime = CServerTimeMgr.Inst:GetZone8Time()
    if this.answerList == nil then
        return answerState
    end

    do
        local i = 0
        while i < this.answerList.Count do
            local jsonDict = TypeAs(this.answerList[i], typeof(MakeGenericClass(Dictionary, String, Object)))

            local am = CreateFromClass(AssistantMessage)
            am.msgType = AssistantMessageType.Question
            am.info = jsonDict
            CommonDefs.ListAdd(this.assistantMessageList, typeof(AssistantMessage), am)

            local replynum = 0
            if CommonDefs.DictContains(jsonDict, typeof(String), "replynum") then
                replynum = System.Int32.Parse(ToStringWrap(CommonDefs.DictGetValue(jsonDict, typeof(String), "replynum")))
            end
            if replynum > 0 and CommonDefs.DictContains(jsonDict, typeof(String), "last_answer") then
                local bm = CreateFromClass(AssistantMessage)
                bm.msgType = AssistantMessageType.Answer
                bm.info = jsonDict
                CommonDefs.ListAdd(this.assistantMessageList, typeof(AssistantMessage), bm)
            else
            end

            local questionTime = DateTime.Parse(ToStringWrap(CommonDefs.DictGetValue(jsonDict, typeof(String), "qtime")))
            if CommonDefs.DictContains(jsonDict, typeof(String), "eval_aid") then
            else
                local ts = (CreateFromClass(TimeSpan, sysTime.Ticks)):Subtract(CreateFromClass(TimeSpan, questionTime.Ticks)):Duration()

                if ts.Days <= CContactAssistantWnd.WaitDay then
                    answerState = true
                end
            end
            i = i + 1
        end
    end

    return answerState
end
CContactAssistantWnd.m_OnTabChange_CS2LuaHook = function (this, go, index) 
    if index == 0 then
        this.askView.gameObject:SetActive(true)
        this.replyView.gameObject:SetActive(false)
        this.askView:init()
    elseif index == 1 then
        this.askView.gameObject:SetActive(false)
        this.replyView.gameObject:SetActive(true)
        this.assistantReplyView:Init()
        this.replyView:init()
        this:updateAssistantReplyView()
    end
end
CContactAssistantWnd.m_OnSubTabChange_CS2LuaHook = function (this, go, index) 
    if index == 0 then
        this.rechargeProblemView.gameObject:SetActive(true)
        this.gameSetView.gameObject:SetActive(false)
        this.punishExplainView.gameObject:SetActive(false)
        this.waitView:SetActive(false)
    elseif index == 1 then
        if this.needAnswerState then
            this.gameSetView.gameObject:SetActive(false)
            this.waitView:SetActive(true)
        else
            this.gameSetView.gameObject:SetActive(true)
            this.gameSetView:init()
            this.waitView:SetActive(false)
        end
        this.rechargeProblemView.gameObject:SetActive(false)
        this.punishExplainView.gameObject:SetActive(false)
    elseif index == 2 then
        if this.needAnswerState then
            this.punishExplainView.gameObject:SetActive(false)
            this.waitView:SetActive(true)
        else
            this.punishExplainView.gameObject:SetActive(true)
            this.punishExplainView:init()
            this.waitView:SetActive(false)
        end

        this.rechargeProblemView.gameObject:SetActive(false)
        this.gameSetView.gameObject:SetActive(false)
    end
end
