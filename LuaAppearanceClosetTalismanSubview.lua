local CUITexture = import "L10.UI.CUITexture"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local Extensions = import "Extensions"
local CButton = import "L10.UI.CButton"
local EnumGender = import "L10.Game.EnumGender"
local UISprite = import "UISprite"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"

LuaAppearanceClosetTalismanSubview = class()

RegistChildComponent(LuaAppearanceClosetTalismanSubview,"m_ClosetTalismanItem", "CommonClosetItemCell", GameObject)
RegistChildComponent(LuaAppearanceClosetTalismanSubview,"m_SwitchButton", "CommonHeaderSwitch", CButton)
RegistChildComponent(LuaAppearanceClosetTalismanSubview,"m_ItemDisplay", "AppearanceCommonButtonDisplayView", CCommonLuaScript)
RegistChildComponent(LuaAppearanceClosetTalismanSubview,"m_ContentTable", "ContentTable", UITable)
RegistChildComponent(LuaAppearanceClosetTalismanSubview,"m_ContentScrollView", "ContentScrollView", UIScrollView)
RegistChildComponent(LuaAppearanceClosetTalismanSubview, "m_AppearanceTalismanTabBar", "AppearanceTalismanTabBar", CCommonLuaScript)

RegistClassMember(LuaAppearanceClosetTalismanSubview, "m_AllTalismanAppearances")
RegistClassMember(LuaAppearanceClosetTalismanSubview, "m_TalismanSpecialAppearInfo")
RegistClassMember(LuaAppearanceClosetTalismanSubview, "m_SelectedDataId")

function LuaAppearanceClosetTalismanSubview:Awake()
end

function LuaAppearanceClosetTalismanSubview:Init()
    --组件共用，每次需要重新关联响应方法
    self.m_SwitchButton.gameObject:SetActive(true)
    self.m_SwitchButton.Text = LocalString.GetString("法宝开关")
    UIEventListener.Get(self.m_SwitchButton.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) self:OnSwitchButtonClick() end)

    self:SetDefaultSelection()
    self:LoadData()
end

function LuaAppearanceClosetTalismanSubview:LoadData()
    self.m_AllTalismanAppearances = LuaAppearancePreviewMgr:GetAllTalismanAppearInfo(true)
    self.m_TalismanSpecialAppearInfo = {}
    Extensions.RemoveAllChildren(self.m_ContentTable.transform)
    self.m_ContentTable.gameObject:SetActive(true)

    for i = 1, # self.m_AllTalismanAppearances do
        local child = CUICommonDef.AddChild(self.m_ContentTable.gameObject, self.m_ClosetTalismanItem)
        child:SetActive(true)
        self:InitItem(child, self.m_AllTalismanAppearances[i])
    end
    self.m_ContentTable:Reposition()
    self.m_ContentScrollView:ResetPosition()
    self:InitSelection()
end

function LuaAppearanceClosetTalismanSubview:SetDefaultSelection()
    self.m_SelectedDataId = LuaAppearancePreviewMgr:IsShowTalismanAppear() and LuaAppearancePreviewMgr:GetCurrentInUseTalismanAppear() or 0
end

function LuaAppearanceClosetTalismanSubview:InitSelection()
    if self.m_SelectedDataId == 0 then
        self:OnItemClick(nil)
        return
    end

    local childCount = self.m_ContentTable.transform.childCount
    for i=0, childCount-1 do
        local childGo = self.m_ContentTable.transform:GetChild(i).gameObject
        local appearanceData = self.m_AllTalismanAppearances[i+1]
        if appearanceData.id == self.m_SelectedDataId or self:IsSameCategory(appearanceData.id, self.m_SelectedDataId) then
            self:OnItemClick(childGo)
            break
        end
    end
end

function LuaAppearanceClosetTalismanSubview:ExistInUseTalismanAppear(id, fxId)
    if LuaAppearancePreviewMgr:IsCurrentInUseTalismanAppear(id) then
        return true
    end
    local sameCategoryTbl = LuaAppearancePreviewMgr:GetTalismanSpecialAppearInfo(id, fxId)
    for __,data in pairs(sameCategoryTbl) do
        if LuaAppearancePreviewMgr:IsCurrentInUseTalismanAppear(data.id, data.fxId) then
            return true
        end
    end
    return false
end

function LuaAppearanceClosetTalismanSubview:InitItem(itemGo, appearanceData)
    local iconTexture = itemGo.transform:Find("Item"):GetComponent(typeof(CUITexture))
    local cornerGo = itemGo.transform:Find("Item/Corner").gameObject
    local nameLabel = itemGo.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local conditionLabel = itemGo.transform:Find("ConditionLabel"):GetComponent(typeof(UILabel))
    iconTexture:LoadMaterial(appearanceData.icon)
    itemGo:GetComponent(typeof(CButton)).Selected = (self.m_SelectedDataId and self.m_SelectedDataId==appearanceData.id or false)
    cornerGo:SetActive(self:ExistInUseTalismanAppear(appearanceData.id, appearanceData.fxId))
    nameLabel.text = CommonDefs.GenericStringFilter(appearanceData.name,"\\(.+?\\)", 0, "")--剔除名字中的数量信息
    conditionLabel.text = appearanceData.condition

    UIEventListener.Get(itemGo).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnItemClick(go)
    end)
end

function LuaAppearanceClosetTalismanSubview:OnItemClick(go)
    self.m_TalismanSpecialAppearInfo = {}
    local childCount = self.m_ContentTable.transform.childCount
    for i=0, childCount-1 do
        local childGo = self.m_ContentTable.transform:GetChild(i).gameObject
        if childGo == go then
            childGo.transform:GetComponent(typeof(CButton)).Selected = true
            self.m_TalismanSpecialAppearInfo = LuaAppearancePreviewMgr:GetTalismanSpecialAppearInfo(self.m_AllTalismanAppearances[i+1].id, self.m_AllTalismanAppearances[i+1].fxId)
            if #self.m_TalismanSpecialAppearInfo==0 then --不是特殊外观的情况，直接预览
                self.m_SelectedDataId = self.m_AllTalismanAppearances[i+1].id
                
            end
        else
            childGo.transform:GetComponent(typeof(CButton)).Selected = false
        end
    end

    if #self.m_TalismanSpecialAppearInfo<=0 then
        self.m_AppearanceTalismanTabBar.gameObject:SetActive(false)
        self:UpdateButtonsDisplay()
        g_ScriptEvent:BroadcastInLua("RequestPreviewMainPlayerTalismanAppearance", self.m_SelectedDataId)
        if not LuaAppearancePreviewMgr:IsShowTalismanAppear() and self.m_SelectedDataId>0 then
            g_MessageMgr:ShowMessage("Appearance_Preview_Hide_Talisman_Appearance_Tip")
        end
    else
        self.m_AppearanceTalismanTabBar.gameObject:SetActive(true)
        self.m_AppearanceTalismanTabBar:Init(self.m_TalismanSpecialAppearInfo, function(go, index) self:OnSpecialAppearTabChange(go, index) end)        
        --特殊外观里面的第一个或者已装备的那个
        local index = 0
        for i=1,#self.m_TalismanSpecialAppearInfo do
            if LuaAppearancePreviewMgr:IsCurrentInUseTalismanAppear(self.m_TalismanSpecialAppearInfo[i].id) then
                index=i-1
                break
            end
        end
        self.m_AppearanceTalismanTabBar:ChangeTab(index, false)
    end
end

function LuaAppearanceClosetTalismanSubview:OnSpecialAppearTabChange(go, index)
    local appear = self.m_TalismanSpecialAppearInfo[index+1] or nil
    self.m_SelectedDataId = appear.id
    g_ScriptEvent:BroadcastInLua("RequestPreviewMainPlayerTalismanAppearance", appear.id)
    if not LuaAppearancePreviewMgr:IsShowTalismanAppear() and self.m_SelectedDataId>0 then
        g_MessageMgr:ShowMessage("Appearance_Preview_Hide_Talisman_Appearance_Tip")
    end
    self:UpdateButtonsDisplay()
end

function LuaAppearanceClosetTalismanSubview:IsSameCategory(talismanId, selectedId)
    local selectedGrade = LuaAppearancePreviewMgr:GetTalismanGrade(selectedId)
    --特殊法宝外观，利用grade*10+count判断同一类
    if selectedGrade>4 then
        return talismanId%100==selectedId%100
    --1-4阶法宝外观，利用grade判断同一类
    else
        return LuaAppearancePreviewMgr:GetTalismanGrade(talismanId)==selectedGrade
    end
end

function LuaAppearanceClosetTalismanSubview:UpdateButtonsDisplay()
    self.m_SwitchButton.Selected = LuaAppearancePreviewMgr:IsShowTalismanAppear()
    if self.m_SelectedDataId == 0 then
        self.m_ItemDisplay.gameObject:SetActive(false)
        return
    end
    self.m_ItemDisplay.gameObject:SetActive(true)
    local buttonTbl = {}
    local exist = self.m_SelectedDataId and LuaAppearancePreviewMgr:MainPlayerHasTalismanAppear(self.m_SelectedDataId) or false
    local inUse = self.m_SelectedDataId and LuaAppearancePreviewMgr:IsCurrentInUseTalismanAppear(self.m_SelectedDataId) or false
    if not inUse then
        if exist then
            table.insert(buttonTbl, {text=LocalString.GetString("更换"), isYellow=false, action=function(go) self:OnApplyButtonClick(go) end})
        else
            table.insert(buttonTbl, {text=LocalString.GetString("获取"), isYellow=true, action=function(go) self:OnApplyButtonClick(go) end})
        end
    else
        table.insert(buttonTbl, {text=LocalString.GetString("脱下"), isYellow=false, action=function(go) self:OnRemoveButtonClick() end})
    end

    self.m_ItemDisplay:Init(buttonTbl)
end

function LuaAppearanceClosetTalismanSubview:OnApplyButtonClick(go)
    if self.m_SelectedDataId and self.m_SelectedDataId>0 then
        
        if LuaAppearancePreviewMgr:MainPlayerHasTalismanAppear(self.m_SelectedDataId) then
            --设置
            LuaAppearancePreviewMgr:SelectTalismanAppearance(self.m_SelectedDataId)
        else
            --获取
            LuaAppearancePreviewMgr:ShowGetTalismanInfo(go, self.m_SelectedDataId)
        end
    end
end

function LuaAppearanceClosetTalismanSubview:OnRemoveButtonClick()
    LuaAppearancePreviewMgr:SelectTalismanAppearance(0)
end

function LuaAppearanceClosetTalismanSubview:OnSwitchButtonClick()
    self.m_SwitchButton.Selected = not self.m_SwitchButton.Selected
    local bShow = LuaAppearancePreviewMgr:IsShowTalismanAppear()
    LuaAppearancePreviewMgr:ChangeTalismanAppearVisibility(not bShow)
end

function LuaAppearanceClosetTalismanSubview:OnEnable()
    g_ScriptEvent:AddListener("TalismanFxUpdate", self, "OnTalismanFxUpdate")
    g_ScriptEvent:AddListener("TalismanFxListUpdate", self, "OnTalismanFxListUpdate")
    g_ScriptEvent:AddListener("OnSyncMainPlayerAppearanceProp",self,"SyncMainPlayerAppearancePropUpdate")
    g_ScriptEvent:AddListener("AppearancePropertySettingInfoReturn",self,"OnAppearancePropertySettingInfoReturn") --开关切换
end

function LuaAppearanceClosetTalismanSubview:OnDisable()
    g_ScriptEvent:RemoveListener("TalismanFxUpdate", self, "OnTalismanFxUpdate")
    g_ScriptEvent:RemoveListener("TalismanFxListUpdate", self, "OnTalismanFxListUpdate")
    g_ScriptEvent:RemoveListener("OnSyncMainPlayerAppearanceProp",self,"SyncMainPlayerAppearancePropUpdate")
    g_ScriptEvent:RemoveListener("AppearancePropertySettingInfoReturn",self,"OnAppearancePropertySettingInfoReturn") --开关切换
end

function LuaAppearanceClosetTalismanSubview:OnTalismanFxUpdate(args)
    local obj = args[0]
    if obj and TypeIs(obj, typeof(CClientMainPlayer)) then
        self:SetDefaultSelection()
        self:LoadData()
    end
end

function LuaAppearanceClosetTalismanSubview:OnTalismanFxListUpdate()
    self:SetDefaultSelection()
    self:LoadData()
end

function LuaAppearanceClosetTalismanSubview:SyncMainPlayerAppearancePropUpdate()
    self:InitSelection()
end

function LuaAppearanceClosetTalismanSubview:OnAppearancePropertySettingInfoReturn(args)
    
    self:SetDefaultSelection()
    self:InitSelection()
end