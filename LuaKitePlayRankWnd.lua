require("3rdParty/ScriptEvent")
require("common/common_include")

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CUIRes = import "L10.UI.CUIResources"
local UITable = import "UITable"
local UIScrollView = import "UIScrollView"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CUIManager = import "L10.UI.CUIManager"
local NGUITools = import "NGUITools"
local UILabel = import "UILabel"
local UISprite = import "UISprite"
local Profession = import "L10.Game.Profession"

CLuaKiteRankWnd=class()
RegistClassMember(CLuaKiteRankWnd,"CloseBtn")
RegistClassMember(CLuaKiteRankWnd,"SelfNode")
RegistClassMember(CLuaKiteRankWnd,"TemplateNode")
RegistClassMember(CLuaKiteRankWnd,"ScrollViewNode")
RegistClassMember(CLuaKiteRankWnd,"TableNode")

function CLuaKiteRankWnd:Init()
	local onCloseClick = function(go)
		CUIManager.CloseUI(CUIRes.KitePlayRankWnd)
	end
	CommonDefs.AddOnClickListener(self.CloseBtn,DelegateFactory.Action_GameObject(onCloseClick),false)
	self.TemplateNode:SetActive(false)
	for i,v in ipairs(LuaKitePlayMgr.m_RankDataTable) do
		local node = NGUITools.AddChild(self.TableNode,self.TemplateNode)
		node:SetActive(true)
		node.transform:Find("Name/text"):GetComponent(typeof(UILabel)).text = v.playerName
		node.transform:Find("Name/JobIcon"):GetComponent(typeof(UISprite)).spriteName = Profession.GetIconByNumber(v.playerClass)
		node.transform:Find("time"):GetComponent(typeof(UILabel)).text = v.score
		node.transform:Find("rank"):GetComponent(typeof(UILabel)).text = i
		if v.playerId == CClientMainPlayer.Inst.Id then
			self.SelfNode.transform:Find("Name/text"):GetComponent(typeof(UILabel)).text = v.playerName
			self.SelfNode.transform:Find("Name/JobIcon"):GetComponent(typeof(UISprite)).spirteName = Profession.GetIconByNumber(v.playerClass)
			self.SelfNode.transform:Find("time"):GetComponent(typeof(UILabel)).text = v.score
			self.SelfNode.transform:Find("rank"):GetComponent(typeof(UILabel)).text = i
		end
	end

	self.TableNode:GetComponent(typeof(UITable)):Reposition()
	self.ScrollViewNode:GetComponent(typeof(UIScrollView)):ResetPosition()
end

return CLuaKiteRankWnd
