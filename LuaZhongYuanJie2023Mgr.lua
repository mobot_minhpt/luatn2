local CStatusMgr = import "L10.Game.CStatusMgr"
local EPropStatus = import "L10.Game.EPropStatus"
local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"
local EWeaponVisibleReason = import "L10.Engine.EWeaponVisibleReason"
local EHideKuiLeiStatus  = import "L10.Engine.EHideKuiLeiStatus"
local CoreScene=import "L10.Engine.Scene.CoreScene"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local CClientObjectMgr =import "L10.Game.CClientObjectMgr"
local CClientPick = import "L10.Game.CClientPick"
local CScene = import "L10.Game.CScene"
local Message_Message = import "L10.Game.Message_Message"
local CSkillButtonBoardWnd = import "L10.UI.CSkillButtonBoardWnd"

LuaZhongYuanJie2023Mgr = class()

-- 1: normal, 2: hard
LuaZhongYuanJie2023Mgr.ZYPDSelectMode = 1
LuaZhongYuanJie2023Mgr.isSignUp1 = nil
LuaZhongYuanJie2023Mgr.isSignUp2 = nil
LuaZhongYuanJie2023Mgr.s_PickEngineId = 0
LuaZhongYuanJie2023Mgr.isSuccess = true
LuaZhongYuanJie2023Mgr.rewardMode = 1
LuaZhongYuanJie2023Mgr.isWeaponVisible = true

function LuaZhongYuanJie2023Mgr:OnMainPlayerCreated()
    if self.m_TimeTick then
        UnRegisterTick(self.m_TimeTick)
        self.m_TimeTick = nil
    end
    self.m_TimeTick = RegisterTick(function()
        self:CheckWeaponVisibleStatus()
    end, 30)
    self:AppendGameplayInfo()
    g_ScriptEvent:RemoveListener("MainPlayerMoveStepped", self, "OnMoveStepped")
    g_ScriptEvent:AddListener("MainPlayerMoveStepped", self, "OnMoveStepped")
    g_ScriptEvent:RemoveListener("CSkillButtonBoardWnd_Show",self,"OnCSkillButtonBoardWnd_Show")
    g_ScriptEvent:AddListener("CSkillButtonBoardWnd_Show",self,"OnCSkillButtonBoardWnd_Show")
    
end
function LuaZhongYuanJie2023Mgr:OnMainPlayerDestroyed()
    if self.m_TimeTick then
        UnRegisterTick(self.m_TimeTick)
        self.m_TimeTick = nil
    end
    if self.stageCountDown then
        UnRegisterTick(self.stageCountDown)
        self.stageCountDown = nil
    end
    self:SetAllPlayerWeaponVisible(true)
    g_ScriptEvent:RemoveListener("MainPlayerMoveStepped", self, "OnMoveStepped")
    g_ScriptEvent:RemoveListener("CSkillButtonBoardWnd_Show",self,"OnCSkillButtonBoardWnd_Show")
    local skillWnd = CSkillButtonBoardWnd.Instance
    if skillWnd then
        skillWnd.transform:Find("Anchor").gameObject:SetActive(true)
    end
    LuaZhongYuanJie2023Mgr.stage = 1
    LuaZhongYuanJie2023Mgr.param1 = 0
    LuaZhongYuanJie2023Mgr.param2 = 100
end

function LuaZhongYuanJie2023Mgr:AppendGameplayInfo()
    LuaCommonGamePlayTaskViewMgr:AppendGameplayInfo(self:GetGamePlayId(1),
            true, nil,
            false,
            true, function() return LuaZhongYuanJie2023Mgr:GetZYPDTaskTitle() end,
            true, function() return LuaZhongYuanJie2023Mgr:GetZYPDTaskContent() end,
            true, nil, nil,
            true, nil, function() LuaZhongYuanJie2023Mgr:OnZYPDRuleButtonClick() end)
    LuaCommonGamePlayTaskViewMgr:AppendGameplayInfo(self:GetGamePlayId(2),
            true, nil,
            false,
            true, function() return LuaZhongYuanJie2023Mgr:GetZYPDTaskTitle() end,
            true, function() return LuaZhongYuanJie2023Mgr:GetZYPDTaskContent() end,
            true, nil, nil,
            true, nil, function() LuaZhongYuanJie2023Mgr:OnZYPDRuleButtonClick() end)
end

function LuaZhongYuanJie2023Mgr:OnZYPDRuleButtonClick()
    LuaImageRuleMgr.m_ImageRuleInfo.imagePaths = LuaZhongYuanJie2023Mgr:GetRulePic() or {}
    LuaImageRuleMgr.m_ImageRuleInfo.msgs = LuaZhongYuanJie2023Mgr:GetGuideData() or {}
    CUIManager.ShowUI(CLuaUIResources.CommonImageRuleWnd)
end

function LuaZhongYuanJie2023Mgr:GetRulePic()
    local imagePaths = {
        "UI/Texture/FestivalActivity/Festival_ZhongYuanJie/ZhongYuanJie2023/Material/zypd_guide_01.mat",
        "UI/Texture/FestivalActivity/Festival_ZhongYuanJie/ZhongYuanJie2023/Material/zypd_guide_02.mat",
        "UI/Texture/FestivalActivity/Festival_ZhongYuanJie/ZhongYuanJie2023/Material/zypd_guide_03.mat",
    }
    return imagePaths
end

LuaZhongYuanJie2023Mgr.stage = 1
LuaZhongYuanJie2023Mgr.param1 = 0
LuaZhongYuanJie2023Mgr.param2 = 100

function LuaZhongYuanJie2023Mgr:GetZYPDTaskTitle()
    local title = ZhongYuanJie_ZhongYuanPuDuStageInfo.GetData(self.stage).StageName
    return title
end

function LuaZhongYuanJie2023Mgr:GetZYPDTaskContent()
    local StageInfo = ZhongYuanJie_ZhongYuanPuDuStageInfo.GetData(self.stage).StageInfo
    local progress, content
    if self.stage == 1 or self.stage == 3 or self.stage == 4 or self.stage == 5 or self.stage == 7 then
        local param1Color
        if self.param1 >= self.param2 then
            param1Color = SafeStringFormat3("[c][00FF00]%s[-][/c]", self.param1)
        else
            param1Color = SafeStringFormat3("[c][FF0000]%s[-][/c]", self.param1)
        end
        progress = SafeStringFormat3(ZhongYuanJie_ZhongYuanPuDuStageInfo.GetData(self.stage).StageInfoSpecial, param1Color, self.param2)
        content = SafeStringFormat3("%s\n%s", StageInfo, progress)
    elseif self.stage == 2 then
        local param1Color = SafeStringFormat3("[c][FF0000]%s[-][/c]", self.param1)
        progress = SafeStringFormat3(ZhongYuanJie_ZhongYuanPuDuStageInfo.GetData(self.stage).StageInfoSpecial, param1Color)
        content = SafeStringFormat3("%s\n%s", StageInfo, progress)
    else
        content = StageInfo
    end
    return content
end

function LuaZhongYuanJie2023Mgr:ZhongYuanPuDu_SyncProgress(stageId, param1, param2)
    if not self:IsInPlay() then return end
    if self.stageCountDown then
        UnRegisterTick(self.stageCountDown)
        self.stageCountDown = nil
    end
    self.stage = stageId
    self.param1 = param1
    self.param2 = param2
    if stageId == 2 then
        self.stageCountDown = RegisterTick(function()
            self.param1 = self.param1 - 1
            LuaCommonGamePlayTaskViewMgr:OnUpdateTitleAndContent()
        end, 1000)
    end
    LuaCommonGamePlayTaskViewMgr:OnUpdateTitleAndContent()
end

function LuaZhongYuanJie2023Mgr:GetLetterContent()
    local msg = Message_Message.GetData(54021573).Message
    msg = CUICommonDef.TranslateToNGUIText(msg)
    return msg or ""
end


function LuaZhongYuanJie2023Mgr:IsInPlay()
    local gamePlayId = CScene.MainScene and CScene.MainScene.GamePlayDesignId or 0
    local sceneTemplateId = CScene.MainScene and CScene.MainScene.SceneTemplateId or 0
    if (gamePlayId == self:GetGamePlayId(1) or gamePlayId == self:GetGamePlayId(2)) and 
            (sceneTemplateId == 16103106 or sceneTemplateId == 16103108) then 
        return true
    end
    return false
end

function LuaZhongYuanJie2023Mgr:OnMoveStepped()
    if not self:IsInPlay() then return end
    local player = CClientMainPlayer.Inst
    if not player then return end
    local pos = player.RO.Position
    
    for x=-1,1,1 do
        for y=-1,1,1 do
            local xx = math.floor(pos.x) + x
            local yy = math.floor(pos.z) + y

            local objectid_list = CoreScene.MainCoreScene:GetGridAllObjectIdsWithFloor(xx, yy, 0)
            if objectid_list and objectid_list.Count > 0 then
                for i = 1, objectid_list.Count do
                    local engineId = objectid_list[i-1]
                    local obj = CClientObjectMgr.Inst:GetObject(engineId)
                    if TypeIs(obj,typeof(CClientPick)) then
                        if obj.TemplateId == 37000483 or obj.TemplateId == 37000484 or  obj.TemplateId == 37000485 or obj.TemplateId == 37000486 or obj.TemplateId == 37000487 then
                            self.s_PickEngineId = engineId
                            if CUIManager.IsLoaded(CLuaUIResources.ZYPDGetSpiritWnd) or CUIManager.IsLoading(CLuaUIResources.ZYPDGetSpiritWnd) then
                                return
                            else
                                CUIManager.ShowUI(CLuaUIResources.ZYPDGetSpiritWnd)
                                return
                            end
                        end
                    end
                end
            end
        end
    end

    CUIManager.CloseUI(CLuaUIResources.ZYPDGetSpiritWnd)
end

function LuaZhongYuanJie2023Mgr:OnCSkillButtonBoardWnd_Show()
    --print("OnCSkillButtonBoardWnd_Show")
    --技能面板
    local skillWnd = CSkillButtonBoardWnd.Instance
    if skillWnd then
        skillWnd.transform:Find("Anchor").gameObject:SetActive(self.stage and self.stage >= 3)
    end
end

function LuaZhongYuanJie2023Mgr:HideGetSpiritWnd()
    if CUIManager.IsLoaded(CLuaUIResources.ZYPDGetSpiritWnd) then
        CUIManager.CloseUI(CLuaUIResources.ZYPDGetSpiritWnd)
    end
end

function LuaZhongYuanJie2023Mgr:IsSignUp(mode)
    if mode == 1 then
        return self.isSignUp1
    elseif mode == 2 then
        return self.isSignUp2
    end
end

function LuaZhongYuanJie2023Mgr:SetIsSignUp(mode, isSignUp)
    if mode == 1 then
        self.isSignUp1 = isSignUp
    elseif mode == 2 then
        self.isSignUp2 = isSignUp
    end
end

function LuaZhongYuanJie2023Mgr:GetGuideData()
    local GuideStr = ZhongYuanJie_ZhongYuanPuDuSetting.GetData().FlowRuleText
    local GuideData = g_LuaUtil:StrSplit(GuideStr, ";")
    return GuideData
end

function LuaZhongYuanJie2023Mgr:GetGamePlayId(mode)
    if mode == 1 then
        return ZhongYuanJie_ZhongYuanPuDuSetting.GetData().NormalGamePlayId
    else
        return ZhongYuanJie_ZhongYuanPuDuSetting.GetData().HardGamePlayId
    end
end

function LuaZhongYuanJie2023Mgr:GetReceivedRewardNum(mode)
    if mode == 1 then
        return CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eZhongYuanPuDuNormalRewardTimes) or 0
    else
        return CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eZhongYuanPuDuHardRewardTimes) or 0
    end
end

function LuaZhongYuanJie2023Mgr:GetTotalRewardNum(mode)
    if mode == 1 then 
        return ZhongYuanJie_ZhongYuanPuDuSetting.GetData().NormalDailyRewardTimes
    else
        return ZhongYuanJie_ZhongYuanPuDuSetting.GetData().HardDailyRewardTimes
    end
end

function LuaZhongYuanJie2023Mgr:NeedShowRewardNode()
    if not self.extraInfo_U then
        return false
    end
    return self.extraInfo_U.hasReward
end

function LuaZhongYuanJie2023Mgr:GetRewardID(mode)
    if mode == 1 then
        return ZhongYuanJie_ZhongYuanPuDuSetting.GetData().NormalRewardShowIcon
    else
        return ZhongYuanJie_ZhongYuanPuDuSetting.GetData().HardRewardShowIcon
    end
end

function LuaZhongYuanJie2023Mgr:ShowZhongYuanPuDuResult(bResult, mode,extraInfo_U)
    self.isSuccess = bResult
    self.rewardMode = mode
    self.extraInfo_U = g_MessagePack.unpack(extraInfo_U)

    CUIManager.ShowUI(CLuaUIResources.ZYPDResultWnd)
end

-- 检测状态，是否显示自己和队友的武器
function LuaZhongYuanJie2023Mgr:CheckWeaponVisibleStatus()
    
    if not self:IsInPlay() then
        if self.m_TimeTick then
            UnRegisterTick(self.m_TimeTick)
            self.m_TimeTick = nil
        end
        self:SetAllPlayerWeaponVisible(true)
        return
    end
    local player = CClientMainPlayer.Inst
    if not player or not player.RO then return end
    local WeaponVisibleStatus = CStatusMgr.Inst:GetStatus(player, EPropStatus.GetId("ZhongYuanPuDu"))

    -- 当前状态和上一次状态不一致时，设置武器显示状态 与服务器统一 0显示 1隐藏
    if WeaponVisibleStatus == 1 and self.isWeaponVisible or WeaponVisibleStatus == 0 and not self.isWeaponVisible then
        if WeaponVisibleStatus == 1 then
            self.isWeaponVisible = false
            self:SetAllPlayerWeaponVisible(false)
        else
            self.isWeaponVisible = true
            self:SetAllPlayerWeaponVisible(true)
        end
    end
end

function LuaZhongYuanJie2023Mgr:SetAllPlayerWeaponVisible(isShow)
    local player = CClientMainPlayer.Inst
    if not player or not player.RO then return end

    self:SetWeaponVisible(player.RO, isShow)
    
    local objs = CClientPlayerMgr.Inst.m_Id2VisiblePlayer
    CommonDefs.DictIterate(objs, DelegateFactory.Action_object_object(function (___key, obj)
        if not obj or not obj.RO then return end
        self:SetWeaponVisible(obj.RO, isShow)
    end))
end

function LuaZhongYuanJie2023Mgr:SetWeaponVisible(ro, isShow)
    ro:SetWeaponVisible(EWeaponVisibleReason.Expression, isShow)
    ro:ForceHideKuiLei(isShow and EHideKuiLeiStatus.None or EHideKuiLeiStatus.ForceHideKuiLeiAndClose)
end

function LuaZhongYuanJie2023Mgr:ZhongYuanJie2023SignUpCheck()
    local jieRiId = 36
    local taskId = 22029896

    local openTaskIds = CLuaScheduleMgr.m_OpenFestivalTasks[jieRiId]
    if not openTaskIds then return false end

    for _, val in pairs(openTaskIds) do
        if val == taskId then
            local holiday = CSigninMgr.Inst.holidayInfo
            if holiday and holiday.showSignin and not holiday.hasSignGift and holiday.todayInfo and holiday.todayInfo.Open == 1 then
                return true
            end
        end
    end
    return false
end
