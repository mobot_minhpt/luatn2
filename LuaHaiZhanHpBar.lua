local UIGrid=import "UIGrid"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CPhysicsObjectMgr=import "L10.Engine.CPhysicsObjectMgr"
local EPropStatus = import "L10.Game.EPropStatus"
local CMainCamera = import "L10.Engine.CMainCamera"
local CStatusMgr = import "L10.Game.CStatusMgr"
local CClientObjectMgr=import "L10.Game.CClientObjectMgr"
local CCustomHpAndMpBar=import "L10.UI.CCustomHpAndMpBar"
local CNameBoard=import "L10.UI.CNameBoard"

LuaHaiZhanHpBar = class()

RegistChildComponent(LuaHaiZhanHpBar, "HpBar", "HpBar", CCustomHpAndMpBar)
RegistChildComponent(LuaHaiZhanHpBar, "NameBoard", "NameBoard", CNameBoard)
RegistChildComponent(LuaHaiZhanHpBar, "View", "View", GameObject)
RegistChildComponent(LuaHaiZhanHpBar, "Buff", "Buff", Transform)
RegistChildComponent(LuaHaiZhanHpBar, "StatusLabel", "StatusLabel", UILabel)

RegistClassMember(LuaHaiZhanHpBar, "Table")
RegistClassMember(LuaHaiZhanHpBar, "Countdown")
RegistClassMember(LuaHaiZhanHpBar, "CdProgress")
RegistClassMember(LuaHaiZhanHpBar, "CdLabel")

RegistClassMember(LuaHaiZhanHpBar, "m_TemplateId")
RegistClassMember(LuaHaiZhanHpBar, "m_EngineId")
RegistClassMember(LuaHaiZhanHpBar, "m_YOffset")
RegistClassMember(LuaHaiZhanHpBar, "m_DisplayNameBoardAfterInjury")
RegistClassMember(LuaHaiZhanHpBar, "m_DisplayNameBoardAfterInjuryShow")
RegistClassMember(LuaHaiZhanHpBar, "m_DisplayNameBoardAfterInjuryTimeStamp")
RegistClassMember(LuaHaiZhanHpBar, "m_DisplayNameBoardAfterInjuryTime")
RegistClassMember(LuaHaiZhanHpBar, "m_SeaMonsterHidingStatus")
-- RegistClassMember(LuaHaiZhanHpBar, "m_ShowBuffID")

RegistClassMember(LuaHaiZhanHpBar, "m_IsInPvp")
RegistClassMember(LuaHaiZhanHpBar, "m_IsMy")
RegistClassMember(LuaHaiZhanHpBar, "m_EnableHpBar")

function LuaHaiZhanHpBar:Awake()
    self.Table = self.transform:Find("View"):GetComponent(typeof(UITable))
    self.Countdown = self.transform:Find("View/Countdown")
    self.CdProgress = self.Countdown:Find("Progress"):GetComponent(typeof(UITexture))
    self.CdLabel = self.Countdown:Find("Label"):GetComponent(typeof(UILabel))

    self.m_SeaMonsterHidingStatus = EPropStatus.GetId("SeaMonsterHiding")
end

function LuaHaiZhanHpBar:Init(args)
    self.m_IsInPvp = LuaHaiZhanMgr:IsInPvpPlay()

    self.m_DisplayNameBoardAfterInjuryTime = 10--10秒内没发生变化就隐藏
    local engineId,templateId=args[0],args[1]
    self.m_EngineId=engineId
    self.m_TemplateId=templateId

    local design = NavalWar_NameVersion.GetData(templateId)
    self.m_YOffset = design.yOffset
    self.m_DisplayNameBoardAfterInjury = design.DisplayNameBoardAfterInjury>0
    self.m_DisplayNameBoardAfterInjuryShow = false
    self.m_DisplayNameBoardAfterInjuryTimeStamp = 0
    local obj = CClientObjectMgr.Inst:GetObject(engineId)

    if design.ShowName>0 then
        self.NameBoard.gameObject:SetActive(true)
        --pvp并且有title，则显示title
        if LuaHaiZhanMgr.IsPvpPlay() and obj.TitleStr and obj.TitleStr~="" then
            self.NameBoard.displayName = obj.TitleStr
        else
            self.NameBoard.displayName = obj.Name
        end
        self.NameBoard.fontColor = obj.NameDisplayColor
    else
        self.NameBoard.gameObject:SetActive(false)
    end

    if design.EnableHpBar>0 then
        self.m_EnableHpBar = true
        self.HpBar.gameObject:SetActive(true)
        self.HpBar:UpdateValue(obj.Hp,obj.CurrentHpFull,obj.HpFull)
    else
        self.m_EnableHpBar = false
        self.HpBar.gameObject:SetActive(false)
    end

    for i=1,self.Buff.childCount do
        self.Buff:GetChild(i-1).gameObject:SetActive(false)
    end

    self.StatusLabel.gameObject:SetActive(false)
    self.Countdown.gameObject:SetActive(false)
    self.m_IsMy = false
    if CClientMainPlayer.Inst then
        local boatEngineId = math.abs(CClientMainPlayer.Inst.AppearanceProp.DriverId)
        if boatEngineId == self.m_EngineId then
            --自己不显示名字和血条
            self.NameBoard.gameObject:SetActive(false)
            self.HpBar.gameObject:SetActive(false)
            self.m_IsMy = true
        end
    end


    self.View:SetActive(false)

    self.Table:Reposition()
end
function LuaHaiZhanHpBar:OnEnable()
    g_ScriptEvent:AddListener("SyncNavalSpecialMonsterHp", self, "OnSyncNavalSpecialMonsterHp")
    g_ScriptEvent:AddListener("HpUpdate", self, "OnHpUpdate")
    g_ScriptEvent:AddListener("NavalWarStatusChange", self, "OnNavalWarStatusChange")
end
function LuaHaiZhanHpBar:OnDisable()
    g_ScriptEvent:RemoveListener("SyncNavalSpecialMonsterHp", self, "OnSyncNavalSpecialMonsterHp")
    g_ScriptEvent:RemoveListener("HpUpdate", self, "OnHpUpdate")
    g_ScriptEvent:RemoveListener("NavalWarStatusChange", self, "OnNavalWarStatusChange")
end

function LuaHaiZhanHpBar:LayoutBuff()
    local buffNum = 0
    for i = 0, self.Buff.childCount - 1 do
        if self.Buff:GetChild(i).gameObject.activeSelf then
            buffNum = buffNum + 1
        end
    end
    self.Buff.gameObject:SetActive(buffNum > 0)
    self.Table:Reposition()
end

function LuaHaiZhanHpBar:OnNavalWarStatusChange(engineId)
    if self.m_EngineId==engineId then
        local obj = CClientObjectMgr.Inst:GetObject(engineId)
        if obj then
            local v = CStatusMgr.Inst:GetStatus(obj,EPropStatus.GetId("NavalWarNavalBurning"))==1
            self.Buff:GetChild(0).gameObject:SetActive(v)
            local v = CStatusMgr.Inst:GetStatus(obj,EPropStatus.GetId("NavalWarModerate"))==1
            self.Buff:GetChild(1).gameObject:SetActive(v)
            local v = CStatusMgr.Inst:GetStatus(obj,EPropStatus.GetId("NavalWarIncreaseDamage"))==1
            self.Buff:GetChild(2).gameObject:SetActive(v)
            self.Buff:GetComponent(typeof(UIGrid)):Reposition()
            self:LayoutBuff()
        end
    end
end

function LuaHaiZhanHpBar:OnHpUpdate(args)
    local engineId = args[0]
    if self.m_EngineId==engineId then
        local obj = CClientObjectMgr.Inst:GetObject(engineId)
        if obj then
            self.HpBar:UpdateValue(obj.Hp,obj.CurrentHpFull,obj.HpFull)
            if obj.Hp<obj.CurrentHpFull then
                self.m_DisplayNameBoardAfterInjuryShow = true
                self.m_DisplayNameBoardAfterInjuryTimeStamp = CServerTimeMgr.Inst.timeStamp
            end
        end
    end
end

function LuaHaiZhanHpBar:OnSyncNavalSpecialMonsterHp(engineId, hp, currentHpFull, hpFull)
    -- print("OnSyncNavalSpecialMonsterHp",engineId, hp, currentHpFull, hpFull)
    if self.m_EngineId==engineId then
        local obj = CClientObjectMgr.Inst:GetObject(engineId)
        if obj then
            obj.Hp = hp
            obj.CurrentHpFull = currentHpFull
            obj.HpFull = hpFull
            self.HpBar:UpdateValue(hp, currentHpFull, hpFull)
            if hp<currentHpFull then
                self.m_DisplayNameBoardAfterInjuryShow = true
                self.m_DisplayNameBoardAfterInjuryTimeStamp = CServerTimeMgr.Inst.timeStamp
            end
        end
    end
end

function LuaHaiZhanHpBar:Update()
    if not CClientMainPlayer.Inst then return end

    local id = self.m_EngineId
    local obj = CClientObjectMgr.Inst:GetObject(id)
    local po = CPhysicsObjectMgr.Inst:GetObjectByEngineId(id)
    if obj and po then
        if CStatusMgr.Inst:GetStatus(obj, self.m_SeaMonsterHidingStatus) == 1 then
            self.View:SetActive(false)
        else
            local viewActive = self.View.activeSelf
            local timeStamp = CServerTimeMgr.Inst.timeStamp
            if self.m_DisplayNameBoardAfterInjuryShow and timeStamp > self.m_DisplayNameBoardAfterInjuryTimeStamp+self.m_DisplayNameBoardAfterInjuryTime then
                self.m_DisplayNameBoardAfterInjuryShow = false
            end
            local show = true
            --是否受伤后才显示姓名板
            if self.m_DisplayNameBoardAfterInjury and not self.m_DisplayNameBoardAfterInjuryShow then
                show = false
            end
            if show and po.m_LuaSelf and po.m_LuaSelf.m_IsHideHud then
                show = false
            end
            if show then
                local v = po:SetHUDPosition(self.View.transform,self.m_YOffset)
                if v then
                    if not viewActive then self.View:SetActive(true) end
                    self.HpBar:UpdateValue(obj.Hp,obj.CurrentHpFull,obj.HpFull)
                    self:UpdateStatus(id)
                else
                    if viewActive then self.View:SetActive(false) end
                end

            else
                if viewActive then self.View:SetActive(false) end
            end
        end
    else
        self.View:SetActive(false)
    end
end

function LuaHaiZhanHpBar:SetCD(cur, duraion)
    self.Countdown.gameObject:SetActive(true)
    self.CdProgress.fillAmount = cur / duraion
    self.CdLabel.text = cur
end

function LuaHaiZhanHpBar:SetNormalStatus()
    if not self.m_IsMy and self.m_EnableHpBar then
        self.HpBar.gameObject:SetActive(true)
    end
    self.Countdown.gameObject:SetActive(false)
    self.StatusLabel.gameObject:SetActive(false)
end

function LuaHaiZhanHpBar:UpdateStatus(id)
    local activeSL, activeCD, activeHB = self.StatusLabel.gameObject.activeSelf, self.Countdown.gameObject.activeSelf, self.HpBar.gameObject.activeSelf
    local shipId = LuaHaiZhanMgr.s_EngineId2ShipId[id]
    
    if not shipId then 
        self:SetNormalStatus()
    else
        local statusInfo = LuaHaiZhanMgr.s_Status[shipId]
        if statusInfo then
            local timeStamp = CServerTimeMgr.Inst.timeStamp
            local status,start,duration = statusInfo[1],statusInfo[2],statusInfo[3]
            if status==EnumNavalPlayShipStatus.Escape then
                if timeStamp>=start and timeStamp<=duration+start then
                    self.StatusLabel.gameObject:SetActive(true)
                    if self.m_IsInPvp then
                        self.StatusLabel.text = LocalString.GetString("重整旗鼓中...")
                        self:SetCD(math.floor(duration+start-timeStamp), duration)
                        self.HpBar.gameObject:SetActive(false)
                    else
                        self.StatusLabel.text = LocalString.GetString("撤退中"..tostring(math.floor(duration+start-timeStamp)))
                    end
                else
                    self:SetNormalStatus()
                end
            elseif status == EnumNavalPlayShipStatus.Supply then
                if timeStamp>=start and timeStamp<=duration+start then
                    if not self.m_IsMy and self.m_EnableHpBar then
                        self.HpBar.gameObject:SetActive(true)
                    end
                    if self.m_IsInPvp then
                        self.StatusLabel.gameObject:SetActive(true)
                        self.StatusLabel.text = LocalString.GetString("正在补给中...")
                        self:SetCD(math.floor(duration+start-timeStamp), duration)
                    end
                else
                    self:SetNormalStatus()
                end
            else
                self:SetNormalStatus()
            end
        else
            self:SetNormalStatus()
        end
    end
    if activeSL ~= self.StatusLabel.gameObject.activeSelf or activeCD ~= self.Countdown.gameObject.activeSelf or activeHB ~= self.HpBar.gameObject.activeSelf then
        self.Table:Reposition()
    end
end

