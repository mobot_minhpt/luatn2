local CShiTuMgr = import "L10.Game.CShiTuMgr"
local PlayerPrefs = import "UnityEngine.PlayerPrefs"
local Ques_Struct = import "L10.Game.CShiTuMgr+Ques_Struct"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local QnMoneyCostMesssageMgr = import "L10.UI.QnMoneyCostMesssageMgr"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CScene = import "L10.Game.CScene"
local CChatHelper = import "L10.UI.CChatHelper"
local EChatPanel = import "L10.Game.EChatPanel"
local CTeamMgr = import "L10.Game.CTeamMgr"
local CClientObject = import "L10.Game.CClientObject"
local CTaskMgr = import "L10.Game.CTaskMgr"

LuaShiTuMgr = {}

LuaShiTuMgr.InitShiTuLevelInfo = function()
  Gac2Gas.RequestShiFuScoreLevel()
end

function LuaShiTuMgr:OpenShiTuMainWnd()
  local tabIndex = LuaShiTuMgr:GetShiTuMainWndDefaultTabIndex() 
  if CClientMainPlayer.Inst and ShiTu_Setting.GetData().MinBaiShiLevel > CClientMainPlayer.Inst.MaxLevel then
      g_MessageMgr:ShowMessage("RequestShiTuShiMenInfo_Level_Limit")
      return
  end
  Gac2Gas.RequestShiTuShiMenInfo(tabIndex == 0)
end

LuaShiTuMgr.m_ShiTuMainWndTabIndex = 0
LuaShiTuMgr.m_ShiTuChooseWndTagList = {}

function LuaShiTuMgr:GetShiTuMainWndDefaultTabIndex()
  --TODO 关于入门弟子条件
  local hasShifu = CShiTuMgr.Inst:GetCurrentShiFuId() ~= 0
  local hasTudi = CShiTuMgr.Inst:GetAllCurrentTuDi().Count ~= 0
  local tabIndex = 0
  if hasShifu and not hasTudi then
      tabIndex = 0
  elseif not hasShifu and hasTudi then
      tabIndex = 1
  elseif hasShifu and hasTudi then
      tabIndex = 0
  elseif not hasShifu and not hasTudi then
    if CClientMainPlayer.Inst.MaxLevel > ShiTu_Setting.GetData().MaxBaiShiLevel then
      tabIndex = 1
    end
  end
  self.m_ShiTuMainWndTabIndex = tabIndex
  return tabIndex
end

function LuaShiTuMgr:GetShiTuResultWndCacheKey(type)
  local key = type == CShiTuMgr.ShiTuChooseType.SHI and "ShiTuResultWnd_ShouTuData_" or "ShiTuResultWnd_QiuShiData_" 
	return key .. tostring(CClientMainPlayer.Inst.Id)
end

function LuaShiTuMgr:SaveShiTuResultWndCacheData(type, val)
  local key = self:GetShiTuResultWndCacheKey(type)
  PlayerPrefs.SetString(key, val)
end

function LuaShiTuMgr:GetShiTuResultWndCacheData(type)
  local key = self:GetShiTuResultWndCacheKey(type)
  local data = PlayerPrefs.GetString(key)
  if String.IsNullOrEmpty(data) then 
    return false 
  end
  local arr = g_LuaUtil:StrSplit(data,";")
  if #arr < 2 then 
    return false 
  end
  return true
end

function LuaShiTuMgr:LoadShiTuResultWndCacheData(type)
  local key = self:GetShiTuResultWndCacheKey(type)
  local data = PlayerPrefs.GetString(key)
  if String.IsNullOrEmpty(data) then 
    return false 
  end
  local arr = g_LuaUtil:StrSplit(data,";")
  if #arr < 2 then 
    return false 
  end
  local questionCount = self:GetShiTuChooseWndQuestionCount(type)
  CShiTuMgr.Inst.SaveResultValue:Clear()
  CShiTuMgr.Inst.SaveExtraResultValue:Clear()
  for index,s in pairs(arr) do
    local newarr = g_LuaUtil:StrSplit(s,",")
    if #newarr == 2 then
      if index <= questionCount then
        CommonDefs.DictSet(CShiTuMgr.Inst.SaveResultValue, typeof(Int32), newarr[1], typeof(Int32), newarr[2])
      else
        CommonDefs.DictSet(CShiTuMgr.Inst.SaveExtraResultValue, typeof(Int32), newarr[1], typeof(Int32), newarr[2])
      end
    end
  end
  return true
end

function LuaShiTuMgr:GetShiTuChooseWndQuestionCount(type)
  local count = 0
  if type == CShiTuMgr.ShiTuChooseType.SHI then
		if CShiTuMgr.EnableAIRecommend then
			count = ShiTu_SurverySFV2.GetDataCount()
		else
			count = ShiTu_SurverySF.GetDataCount()
		end
	elseif type == CShiTuMgr.ShiTuChooseType.TU then
		if CShiTuMgr.EnableAIRecommend then
      count = 0
			ShiTu_SurveryTDV2.Foreach(function (key, value) 
        if value.IfNeccessary == 1 then
          count = count + 1
        end
      end)
		else
			count = ShiTu_SurveryTD.GetDataCount()
		end
	end
  return count
end

function LuaShiTuMgr:LoadShiTuChooseWndQuestionList()
  CommonDefs.ListClear(CShiTuMgr.Inst.QuestionList)
  CommonDefs.ListClear(CShiTuMgr.Inst.ExtraQuestionList)
  if CShiTuMgr.Inst.CurrentEnterType == CShiTuMgr.ShiTuChooseType.SHI then
		if CShiTuMgr.EnableAIRecommend then
			ShiTu_SurverySFV2.Foreach(function (key, value) 
				local data = CreateFromClass(Ques_Struct)
				data.ID = value.ID
				data.Question = value.Question
				data.Option1 = value.Option1
				data.Option2 = value.Option2
				data.Option3 = value.Option3
				CommonDefs.ListAdd(CShiTuMgr.Inst.QuestionList, typeof(Ques_Struct), data)
			end)
		else
			ShiTu_SurverySF.Foreach(function (key, value) 
                local data = CreateFromClass(Ques_Struct)
                data.ID = value.ID
                data.Question = value.Question
                data.Option1 = value.Option1
                data.Option2 = value.Option2
                data.Option3 = value.Option3
                CommonDefs.ListAdd(CShiTuMgr.Inst.QuestionList, typeof(Ques_Struct), data)
            end)
		end
	elseif CShiTuMgr.Inst.CurrentEnterType == CShiTuMgr.ShiTuChooseType.TU then
		if CShiTuMgr.EnableAIRecommend then
			ShiTu_SurveryTDV2.Foreach(function (key, value) 
                local data = CreateFromClass(Ques_Struct)
                data.ID = value.ID
                data.Question = value.Question
                data.Option1 = value.Option1
                data.Option2 = value.Option2
                data.Option3 = value.Option3
                if value.IfNeccessary == 1 then
                    CommonDefs.ListAdd(CShiTuMgr.Inst.QuestionList, typeof(Ques_Struct), data)
                else
                    CommonDefs.ListAdd(CShiTuMgr.Inst.ExtraQuestionList, typeof(Ques_Struct), data)
                end
            end)
		else
			ShiTu_SurveryTD.Foreach(function (key, value) 
                local data = CreateFromClass(Ques_Struct)
                data.ID = value.ID
                data.Question = value.Question
                data.Option1 = value.Option1
                data.Option2 = value.Option2
                data.Option3 = value.Option3
                CommonDefs.ListAdd(CShiTuMgr.Inst.QuestionList, typeof(Ques_Struct), data)
            end)
		end
	end
	CommonDefs.ListSort1(CShiTuMgr.Inst.QuestionList, typeof(Ques_Struct), DelegateFactory.Comparison_Ques_Struct(function (item1, item2) 
		return NumberCompareTo(item1.ID, item2.ID)
	end))
end

LuaShiTuMgr.m_EnableWaiMen = true
LuaShiTuMgr.m_ShiTuShiMenInfo = {}
LuaShiTuMgr.m_ShiTuShiMenPlayerInfo = {}
LuaShiTuMgr.m_WaiMenDiZiInfo = {}
function LuaShiTuMgr:SendShiTuShiMenInfo(isTuDi, name, level, exp, qingyi, hasWenDaoReward, playerInfo)
  if (isTuDi and LuaShiTuMgr.m_ShiTuMainWndTabIndex == 1) or (not isTuDi and LuaShiTuMgr.m_ShiTuMainWndTabIndex == 0) then  
    return
  end
  self.m_ShiTuShiMenInfo = {
    isTuDi = isTuDi, name = name, level = level, exp = exp, qingyi = qingyi, hasWenDaoReward = hasWenDaoReward,
  }
  self.m_ShiTuShiMenPlayerInfo = {}
  self.m_WaiMenDiZiInfo = {}
  local list = playerInfo and MsgPackImpl.unpack(playerInfo)
  for i = 0, list.Count-1, 10 do
      local playerId = list[i]
      local playerName = list[i+1]
      local shituType = list[i+2] -- 1 师傅 2 徒弟 3 外门弟子
      local playerLevel = list[i+3]
      local class = list[i+4]
      local gender = list[i+5]
      local isFeiSheng = list[i+6]
      local isBest = list[i+7]
      local isOnline = list[i+8]
      local beTuDiTime = list[i+9] -- 成为徒弟的时间
      local t = {
        playerId = playerId, playerName = playerName, shituType = shituType, playerLevel = playerLevel,
        class = class, gender = gender, isFeiSheng = isFeiSheng, isBest = isBest, isOnline = isOnline,
        beTuDiTime = beTuDiTime, 
      }
      if shituType == 3 then
        table.insert(self.m_WaiMenDiZiInfo, t)
      else
        table.insert(self.m_ShiTuShiMenPlayerInfo, t)
      end
  end
  table.sort(self.m_ShiTuShiMenPlayerInfo,function (a,b)
    if a.shituType == b.shituType then
      return a.beTuDiTime < b.beTuDiTime
    end
    return a.shituType < b.shituType
  end)
  table.sort(self.m_WaiMenDiZiInfo,function (a,b)
    return a.beTuDiTime < b.beTuDiTime
  end)
  if not CUIManager.IsLoaded(CLuaUIResources.ShiTuMainWnd) then
    CUIManager.ShowUI(CLuaUIResources.ShiTuMainWnd)
  else
    g_ScriptEvent:BroadcastInLua("OnSendShiTuShiMenInfo")
  end
end

LuaShiTuMgr.m_ShiTuShiMenNews = {}
function LuaShiTuMgr:SendShiTuShiMenNews(isTuDi, news)
  local list = news and g_MessagePack.unpack(news)
  self.m_ShiTuShiMenNews = {}
  for i = 1, #list, 5 do
      local tm = list[i]
      local playerId = list[i+1]
      local playerName = list[i+2]
      local eventType = list[i+3]
      local args = list[i+4]
      table.insert(self.m_ShiTuShiMenNews,{tm = tm,playerId = playerId,playerName = playerName, eventType = eventType, args = args})
  end
  g_ScriptEvent:BroadcastInLua("OnSendShiTuShiMenNews")
end

function LuaShiTuMgr:SendShiTuShiMenHasReward(hasReward1,hasReward2)
  g_ScriptEvent:BroadcastInLua("OnSendShiTuShiMenHasReward",hasReward1,hasReward2 and hasReward2 or false)
end

LuaShiTuMgr.m_RecommendShiFuList = {}
LuaShiTuMgr.m_BestMatchRecommendShiFu = {}
function LuaShiTuMgr:SendRecommendShiFu(succ, shifu)
  if not succ then
    return
  end
  local list = g_MessagePack.unpack(shifu) or {}
  self.m_RecommendShiFuList = {}
  self.m_BestMatchRecommendShiFu = {}
  for i = 1, #list, 10 do
    local playerId = list[i]
    local playerName = list[i+1]
    local class = list[i+2]
    local gender = list[i+3]
    local level = list[i+4]
    local hasFeiSheng = list[i+5]
    local tagList = list[i+6]
    local isBestMatch = list[i+7]
    local waiMen = list[i+8]
    local isApplied = list[i+9]
    local t = {
      playerId = playerId, playerName = playerName, class = class, gender = gender,
      level = level, hasFeiSheng = hasFeiSheng, tagList = tagList,
      isBestMatch = isBestMatch,waiMen = waiMen,isApplied = isApplied
    }
    if isBestMatch then
      self.m_BestMatchRecommendShiFu = t
    else
      table.insert(self.m_RecommendShiFuList, t)
    end
  end
  if not CUIManager.IsLoaded(CLuaUIResources.ShiTuRecommendWnd) then
    if #self.m_RecommendShiFuList > 0 or self.m_BestMatchRecommendShiFu.playerId then
      CUIManager.ShowUI(CLuaUIResources.ShiTuRecommendWnd)
    end
  else
    g_ScriptEvent:BroadcastInLua("OnSendRecommendShiFu")
  end
end

LuaShiTuMgr.m_RecommendTuDiList = {}
LuaShiTuMgr.m_BestMatchRecommendTuDi = {}
function LuaShiTuMgr:SendRecommendTuDi(succ, tudi)
  if not succ then
    LuaShiTuMgr:CanPostShouTuMessageNoneTuDi(false)
    return
  end
  local list = g_MessagePack.unpack(tudi) or {}
  self.m_RecommendTuDiList = {}
  self.m_BestMatchRecommendTuDi = {}
  for i = 1, #list, 10 do
    local playerId = list[i]
    local playerName = list[i+1]
    local class = list[i+2]
    local gender = list[i+3]
    local level = list[i+4]
    local hasFeiSheng = list[i+5]
    local tagList = list[i+6]
    local isBestMatch = list[i+7]
    local waiMen = list[i+8]
    local isApplied = list[i+9]
    local t = {
      playerId = playerId, playerName = playerName, class = class, gender = gender,
      level = level, hasFeiSheng = hasFeiSheng, tagList = tagList,
      isBestMatch = isBestMatch,waiMen = waiMen,isApplied = isApplied
    }
    if isBestMatch then
      self.m_BestMatchRecommendTuDi = t
    else
      table.insert(self.m_RecommendTuDiList, t)
    end
  end
  if not CUIManager.IsLoaded(CLuaUIResources.ShiTuRecommendWnd) then
    if #self.m_RecommendTuDiList > 0 or self.m_BestMatchRecommendTuDi.playerId  then
      CUIManager.ShowUI(CLuaUIResources.ShiTuRecommendWnd)
    end
  else
    g_ScriptEvent:BroadcastInLua("OnSendRecommendTuDi")
  end
end

LuaShiTuMgr.m_IsShouTuWaiMen = false
function LuaShiTuMgr:ShouTuWaiMenDiZi()
  if CClientMainPlayer.Inst and CClientMainPlayer.Inst.MaxLevel < ShiTu_Setting.GetData().CanShouTuLevel then
    g_MessageMgr:ShowMessage("CanShouTu_LevelLimit")
    return
  end
  local issuccess,res = self:ParseShiTuResultWndCacheData(CShiTuMgr.ShiTuChooseType.SHI)
  if not issuccess or String.IsNullOrEmpty(res) then
		local msg = g_MessageMgr:FormatMessage("ShiTuRecommendWnd_DengJi_ShouTu_Confirm") 
		MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
			CShiTuMgr.Inst.SaveResultValue:Clear()
      CShiTuMgr.Inst.CurrentEnterType = CShiTuMgr.ShiTuChooseType.SHI
      self.m_IsShouTuWaiMen = true
      CUIManager.ShowUI(CUIResources.ShiTuChooseWnd)
		end),nil,LocalString.GetString("登记"),LocalString.GetString("取消"),false)
  else
    self.m_IsShouTuWaiMen = true
    CUIManager.ShowUI(CUIResources.ShiTuResultWnd)
	end
end

function LuaShiTuMgr:CanPostShouTuMessage(isWaiMen)
  CShiTuMgr.Inst.CurrentEnterType = CShiTuMgr.ShiTuChooseType.SHI
  CShiTuMgr.Inst.SaveResultValue:Clear()
  self.m_IsShouTuWaiMen = isWaiMen
  CUIManager.ShowUI(CUIResources.ShiTuChooseWnd)
end

function LuaShiTuMgr:CanPostShouTuMessageNoneTuDi(isWaiMen)
  CShiTuMgr.Inst.CurrentEnterType = CShiTuMgr.ShiTuChooseType.SHI
  CShiTuMgr.Inst.SaveResultValue:Clear()
  self.m_IsShouTuWaiMen = isWaiMen
  local issuccess,res = self:ParseShiTuResultWndCacheData(CShiTuMgr.ShiTuChooseType.SHI)
  if not issuccess or String.IsNullOrEmpty(res) then
    CUIManager.ShowUI(CUIResources.ShiTuChooseWnd)
  else
    CUIManager.ShowUI(CUIResources.ShiTuResultWnd)
  end
end

function LuaShiTuMgr:PlayerCanSearchShiFu(isWaiMen)
  CShiTuMgr.Inst.CurrentEnterType = CShiTuMgr.ShiTuChooseType.TU
  CShiTuMgr.Inst.SaveResultValue:Clear()
  self.m_IsShouTuWaiMen = isWaiMen
  if self.m_IsShouTuWaiMen then
    g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("WaiMen_PlayerCanSearchShiFu"), function ()
      local issuccess,res = self:ParseShiTuResultWndCacheData(CShiTuMgr.ShiTuChooseType.TU)
      if not issuccess or String.IsNullOrEmpty(res) then
        CUIManager.ShowUI(CUIResources.ShiTuChooseWnd)
      else
        CUIManager.ShowUI(CUIResources.ShiTuResultWnd)
      end
    end, nil, nil, nil, false)
  else
    CUIManager.ShowUI(CUIResources.ShiTuChooseWnd)
  end
end

function LuaShiTuMgr:ParseShiTuResultWndCacheData(type)
  local issuccess = LuaShiTuMgr:GetShiTuResultWndCacheData(type)
  local key = type == CShiTuMgr.ShiTuChooseType.SHI and "ShiTuChooseWnd_ShouTuData_" or "ShiTuChooseWnd_QiuShiData_" 
  local key = key.. tostring(CClientMainPlayer.Inst.Id)
  local res = PlayerPrefs.GetString(key)
  local arr = g_LuaUtil:StrSplit(res,",")
  LuaShiTuMgr:LoadShiTuChooseWndQuestionList()
  for key, val in pairs(arr) do
    if not String.IsNullOrEmpty(val) then 
      CommonDefs.DictSet(CShiTuMgr.Inst.SaveResultValue, typeof(Int32), key, typeof(Int32),tonumber(val))
    end
  end
  local tagStr = PlayerPrefs.GetString(key .. "Tag")
  local tagArr = g_LuaUtil:StrSplit(tagStr,",")
  LuaShiTuMgr.m_ShiTuChooseWndTagList = {}
  for key, val in pairs(tagArr) do
    table.insert(LuaShiTuMgr.m_ShiTuChooseWndTagList, tonumber(val))
  end
  CShiTuMgr.Inst.ShiTuAskStage = 0
  return issuccess, res
end

function LuaShiTuMgr:SendMyShouTuQuestionInfo(question, tagList)
  if String.IsNullOrEmpty(question) then 
    return
  end
  local arr = g_LuaUtil:StrSplit(question,";")
  if #arr < 2 then 
    return false 
  end
  local questionCount = self:GetShiTuChooseWndQuestionCount(CShiTuMgr.ShiTuChooseType.SHI)
  CShiTuMgr.Inst.SaveResultValue:Clear()
  CShiTuMgr.Inst.SaveExtraResultValue:Clear()
  for index,s in pairs(arr) do
    local newarr = g_LuaUtil:StrSplit(s,",")
    if #newarr == 2 then
      if index <= questionCount then
        CommonDefs.DictSet(CShiTuMgr.Inst.SaveResultValue, typeof(Int32), newarr[1], typeof(Int32), newarr[2])
      else
        CommonDefs.DictSet(CShiTuMgr.Inst.SaveExtraResultValue, typeof(Int32), newarr[1], typeof(Int32), newarr[2])
      end
    end
  end
  LuaShiTuMgr.m_ShiTuChooseWndTagList = g_MessagePack.unpack(tagList)
  LuaShiTuMgr:LoadShiTuChooseWndQuestionList()
	CShiTuMgr.Inst.ShiTuAskStage = CShiTuMgr.Inst.ExtraQuestionList.Count > 0 and 1 or 0
	CUIManager.ShowUI(CUIResources.ShiTuResultWnd)
end

function LuaShiTuMgr:SendMyBaiShiQuestionInfo(question, tagList)
  if String.IsNullOrEmpty(question) then 
    return
  end
  local arr = g_LuaUtil:StrSplit(question,";")
  if #arr < 2 then 
    return false 
  end
  local questionCount = self:GetShiTuChooseWndQuestionCount(CShiTuMgr.ShiTuChooseType.TU)
  CShiTuMgr.Inst.SaveResultValue:Clear()
  CShiTuMgr.Inst.SaveExtraResultValue:Clear()
  for index,s in pairs(arr) do
    local newarr = g_LuaUtil:StrSplit(s,",")
    if #newarr == 2 then
      if index <= questionCount then
        CommonDefs.DictSet(CShiTuMgr.Inst.SaveResultValue, typeof(Int32), newarr[1], typeof(Int32), newarr[2])
      else
        CommonDefs.DictSet(CShiTuMgr.Inst.SaveExtraResultValue, typeof(Int32), newarr[1], typeof(Int32), newarr[2])
      end
    end
  end
  LuaShiTuMgr.m_ShiTuChooseWndTagList = g_MessagePack.unpack(tagList)
  LuaShiTuMgr:LoadShiTuChooseWndQuestionList()
	CShiTuMgr.Inst.ShiTuAskStage = CShiTuMgr.Inst.ExtraQuestionList.Count > 0 and 1 or 0
	CUIManager.ShowUI(CUIResources.ShiTuResultWnd)
end

LuaShiTuMgr.m_QiuXuePuInfo = {}
function LuaShiTuMgr:SendShiMenQiuXuePuInfo(isShifu, infoUd)
  local info =  g_MessagePack.unpack(infoUd) 
  self.m_QiuXuePuInfo = {}
  for shifuId, relationInfo in pairs(info) do
      if relationInfo.tudis then
        for tudiId, tudiInfo in pairs(relationInfo.tudis) do
          if info[tudiId] and not tudiInfo.isWaiMen then
            table.insert(self.m_QiuXuePuInfo, {
              relationInfo = info[shifuId],
              mainId = shifuId,
              othertudiInfo = nil
            })
            table.insert(self.m_QiuXuePuInfo, {
              relationInfo = info[tudiId],
              mainId = tudiId,
              othertudiInfo = self:GetSortedTuDiList(info[shifuId])
            })
            local othertudiInfo = self:GetSortedTuDiList(info[tudiId])
            if #othertudiInfo > 0 then
              table.insert(self.m_QiuXuePuInfo, {
                relationInfo = nil,
                mainId = nil,
                othertudiInfo = othertudiInfo
              })
            end
          elseif info[tudiId] and tudiInfo.isWaiMen then
            table.insert(self.m_QiuXuePuInfo, {
              relationInfo = info[tudiId],
              mainId = tudiId,
              othertudiInfo = nil
            })
            local othertudiInfo = self:GetSortedTuDiList(info[tudiId])
            if #othertudiInfo > 0 then
              table.insert(self.m_QiuXuePuInfo, {
                relationInfo = nil,
                mainId = nil,
                othertudiInfo = othertudiInfo
              })
            end
          end
        end
      end
   end
   if LuaShiTuMgr.m_ShiTuMainWndTabIndex == 1 then
    local myId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
    local relationInfo = info[myId]
    if relationInfo then
      local othertudiInfo = self:GetSortedTuDiList(relationInfo)
      if #othertudiInfo == 0 then
        self.m_QiuXuePuInfo = {}
        table.insert(self.m_QiuXuePuInfo, {
          relationInfo = info[myId],
          mainId = myId,
          othertudiInfo = nil
        })
      end
    end
   end
   if #self.m_QiuXuePuInfo == 0 then
    for shifuId, relationInfo in pairs(info) do
      table.insert(self.m_QiuXuePuInfo, {
        relationInfo = info[shifuId],
        mainId = shifuId,
        othertudiInfo = nil
      })
      local othertudiInfo = self:GetSortedTuDiList(relationInfo)
      if #othertudiInfo > 0 then
        table.insert(self.m_QiuXuePuInfo, {
          relationInfo = nil,
          mainId = nil,
          othertudiInfo = self:GetSortedTuDiList(relationInfo)
        })
      end
      break
   end
  end
  self:SetQiuXuePuInfoTitle()
end

function LuaShiTuMgr:SetQiuXuePuInfoTitle()
  local myIndex1, myIndex2, shifuIndex = self:GetQiuXuePuMyIndexInfo()
  local memberSerialNumber = ShiTu_Setting.GetData().MemberSerialNumber
  for index1 = 1,#self.m_QiuXuePuInfo do
    local t = self.m_QiuXuePuInfo[index1]
    if index1 > myIndex1 and t.othertudiInfo then
      local tudiIndex = 1
      for index2 = 1, #t.othertudiInfo do
        local tudiInfo = self.m_QiuXuePuInfo[index1].othertudiInfo[index2]
        if tudiIndex <= 17 and not tudiInfo.isWaiMen then
          self.m_QiuXuePuInfo[index1].othertudiInfo[index2].title = SafeStringFormat3(LocalString.GetString("%s徒弟"),memberSerialNumber[tudiIndex  - 1])
          tudiIndex = tudiIndex + 1
        elseif tudiInfo.isWaiMen then
          self.m_QiuXuePuInfo[index1].othertudiInfo[index2].title = LocalString.GetString("外门弟子")
        else
          self.m_QiuXuePuInfo[index1].othertudiInfo[index2].title = LocalString.GetString("小徒弟")
        end
      end
    elseif index1 == myIndex1 and t.othertudiInfo then
      local tudiIndex = 1
      for index2 = 1, #t.othertudiInfo do
        local t2 = t.othertudiInfo[index2]
        local titleSuffix = ""
        if index2 > myIndex2 then
          titleSuffix = t2.tudiGender == EnumGender_lua.Male and LocalString.GetString("师弟") or LocalString.GetString("师妹")
        elseif index2 < myIndex2 then
          titleSuffix = t2.tudiGender == EnumGender_lua.Male and LocalString.GetString("师兄") or LocalString.GetString("师姐")
        end
        self.m_QiuXuePuInfo[index1].othertudiInfo[index2].title = SafeStringFormat3(LocalString.GetString("%s%s"), memberSerialNumber[tudiIndex - 1], titleSuffix)
        if not t2.isWaiMen then
          tudiIndex = tudiIndex + 1
        end
      end
    elseif index1 < myIndex1 and t.othertudiInfo then
      local tudiIndex = 1
      for index2 = 1, #t.othertudiInfo do
        local t2 = self.m_QiuXuePuInfo[index1].othertudiInfo[index2]
        local titleSuffix = ""
        if shifuIndex < index2 then
          titleSuffix = SafeStringFormat3(LocalString.GetString("%s师叔"),memberSerialNumber[tudiIndex - 1])
        elseif shifuIndex > index2 then
          titleSuffix = SafeStringFormat3(LocalString.GetString("%s师伯"),memberSerialNumber[tudiIndex - 1])
        end
        self.m_QiuXuePuInfo[index1].othertudiInfo[index2].title = titleSuffix
        if not t2.isWaiMen then
          tudiIndex = tudiIndex + 1
        end
      end
    end
  end
  g_ScriptEvent:BroadcastInLua("SendShiMenQiuXuePuInfo",myIndex1, myIndex2)
end

function LuaShiTuMgr:GetQiuXuePuMyIndexInfo()
  local id = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
  local myIndex1, myIndex2, shifuIndex = 0,0,0
  for index1, t in pairs(self.m_QiuXuePuInfo) do
    if id == t.mainId then
      myIndex1 = index1
    end
    if t.othertudiInfo then
      for index2, t2 in pairs(t.othertudiInfo) do
        if id == t2.tudiId then
          myIndex1 = index1
          myIndex2 = index2
        end
      end
    end
    if myIndex1 > 1 then
      local shiboInfo = self.m_QiuXuePuInfo[myIndex1 - 1]
      if shiboInfo and shiboInfo.othertudiInfo then
        for _index2, t1 in pairs(shiboInfo.othertudiInfo) do
          if shiboInfo.mainId == t1.tudiId then
            shifuIndex = _index2
          end
        end
      end
    end
  end
  return myIndex1, myIndex2, shifuIndex
end

function LuaShiTuMgr:GetSortedTuDiList(relationInfo)
  local tudis = relationInfo.tudis
  local arr = {}
  if tudis then
    for tudiId, tudiInfo in pairs(tudis) do
      table.insert(arr, {tudiId = tudiId, tudiInfo = tudis[tudiId],isWaiMen = tudiInfo.isWaiMen,tudiClass = tudiInfo.tudiClass, tudiGender = tudiInfo.tudiGender})
    end
  end
  table.sort(arr,function (a,b)
    return a.tudiInfo.beTudiTime < b.tudiInfo.beTudiTime
  end)
  return arr
end

LuaShiTuMgr.m_HanZhangJiInfo = {}
function LuaShiTuMgr:SendShiMenHanZhangJiInfo(isShifu, infoUd)
  self.m_HanZhangJiInfo = g_MessagePack.unpack(infoUd) 
  g_ScriptEvent:BroadcastInLua("SendShiMenHanZhangJiInfo")
end

LuaShiTuMgr.m_ChuShiGiftInfo = {}
function LuaShiTuMgr:SendChuShiGiftInfo(tudiId, tudiName, level, class, gender, jiyuId)
  self.m_ChuShiGiftInfo = {tudiId = tudiId, tudiName = tudiName, level = level, class = class, gender = gender, jiyuId = jiyuId}
  CUIManager.ShowUI(CLuaUIResources.ChuShiGiftWnd)
end

function LuaShiTuMgr:ModifyChuShiJiYuSucc(tudiId, jiyuId)
  g_ScriptEvent:BroadcastInLua("ModifyChuShiJiYuSucc",tudiId, jiyuId)
end

LuaShiTuMgr.m_OpenNewSystem = true

LuaShiTuMgr.s_ShowOtherPlayerDirectoryWnd = false

LuaShiTuMgr.m_ShiTuChooseJiYuWnd_TuDiId = 0

------------
----师父邀请徒弟参加情义春秋
------------
function LuaShiTuMgr:OpenShifuInviteTudiJoinShiTuTeamWnd(taskId)
    
    local task = Task_Task.GetData(taskId)
    if not task then return end
    g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("ShiTuQing_ShiFu_Invite_Do_Task_Confirm", task.Display), function ()
        Gac2Gas.RequestInviteTudiJoinShiTuTeam(taskId)
    end, nil, nil, nil, false)
end

function LuaShiTuMgr:HandleInviteToDoShiTuTask(shifuId, taskId)

    if not (shifuId and shifuId>0) then return end
    --没有队伍，申请加入师父队伍
    if not CTeamMgr.Inst:TeamExists() then
        CTeamMgr.Inst:RequestJoinTeam(shifuId)
    else
        local members = CTeamMgr.Inst.Members
        for i=0,members.Count-1 do
            if shifuId == members[i].m_MemberId then
                g_MessageMgr:ShowMessage("ePLAYER_IS_IN_TEAM", "", "", "", "")--复用已有消息
                return
            end
        end
        --在其他队伍
        g_MessageMgr:ShowMessage("ShiTuQing_TuDi_Need_Leave_Then_Join_Team")
        CChatHelper.SendMsg(EChatPanel.Friend, g_MessageMgr:FormatMessage("ShiTuQing_TuDi_Request_To_Wait_A_Moment"), shifuId)
    end
end
------------
----五子玩法
------------
EnumWuZiQiType = { --和服务器定义保持一致
	eShiTuTask = 1,
	eShiTuNormal = 2,
	eXiaoYaoGuanXunYou = 3,
}

EnumWuZiQiInfoKey = { --和服务器定义保持一致
	eOwnerId = 1,
	eOppId = 2,
	eOwnerInfo = 3,
	eOppInfo = 4,
	eType = 5,
	eQiPan = 6,
	eLastStepInfo = 7,
	eStartTime = 8,
	eWatcherInfoList = 9,
	eRoundNum = 10,
	eReadyInfo = 11,
	eWinPlayerId = 12,
	ePlayId = 13,
}

EnumWuZiQiGridColor = { --和服务器定义保持一致
	eNone = 0,
	eBlack = 1,
	eWhite = 2,
}

EnumWuZiQiRpcType = { --和服务器定义保持一致
	eOpenWnd = 1,
	eAddOppInfo = 2,
	eSetPlayerReady = 3,
	eStart = 4,
	eSetGridColor = 5,
	eSetResult = 6,
	eResetGrid = 7,
	eAddWatcher = 8,
	eDelWatcher = 9,
	eDelPlayer = 10,
	eDestroy = 11,
	eMarkGrid = 12,
	eChatMsg = 13,
}

LuaShiTuMgr.m_WuZiQiPlayInfo = {}
LuaShiTuMgr.m_WuZiQiPlayStepAllowResetEndTime = 0
LuaShiTuMgr.m_WuZiQiPlayStepAllowResetMaxTime = 20 --悔棋允许的时间略小于下棋等待时间
LuaShiTuMgr.m_WuZiQiPlayStepMaxDuration = 25 --对应服务器 MasterWuZiQiMgr.m_RoundDuration
LuaShiTuMgr.m_WuZiQiPlayIsInviteFriendForWatcher = false

function LuaShiTuMgr:WZQ_GetPlayId()
    local playId = self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.ePlayId]
    return playId and playId or 0
end

function LuaShiTuMgr:WZQ_GetPlayType()
    local playType = self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eType]
    return playType and playType or 0
end

function LuaShiTuMgr:WZQ_IsShiTuTask()
    return self:WZQ_GetPlayType()==EnumWuZiQiType.eShiTuTask
end

function LuaShiTuMgr:WZQ_GetOwnerId()
  return self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eOwnerId]
end

function LuaShiTuMgr:WZQ_GetOppId()
  return self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eOppId]
end

function LuaShiTuMgr:WZQ_GetOwnerInfo()
    local id, name, class, gender = 0, "", 0, 0
    if self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eOwnerInfo] then
        local info = self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eOwnerInfo]
        id, name, class, gender =  self:WZQ_GetOwnerId(), info[1], info[2], info[3]
    end
    return id, name, class, gender
end

function LuaShiTuMgr:WZQ_GetOppInfo()
    local id, name, class, gender = 0, "", 0, 0
    if self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eOppInfo] then
        local info = self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eOppInfo]
        id, name, class, gender =  self:WZQ_GetOppId(), info[1], info[2], info[3]
    end
    return id, name, class, gender
end

function LuaShiTuMgr:WZQ_IsOwnerReady()
    local readyInfo = self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eReadyInfo]
    return readyInfo and readyInfo[self:WZQ_GetOwnerId()] or false
end

function LuaShiTuMgr:WZQ_IsOppReady()
    local readyInfo = self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eReadyInfo]
    return readyInfo and readyInfo[self:WZQ_GetOppId()] or false
end

function LuaShiTuMgr:WZQ_MainPlayerIsOwner()
    return CClientMainPlayer.Inst and self:WZQ_IsOwner(CClientMainPlayer.Inst.Id)
end

function LuaShiTuMgr:WZQ_MainPlayerIsOpp()
    return CClientMainPlayer.Inst and self:WZQ_IsOpp(CClientMainPlayer.Inst.Id)
end

function LuaShiTuMgr:WZQ_MainPlayerIsWatcher()
    if CClientMainPlayer.Inst == nil then return false end
    return self:WZQ_IsWatcher(CClientMainPlayer.Inst.Id)
end

function LuaShiTuMgr:WZQ_IsOwner(playerId)
    return self:WZQ_GetOwnerId() and playerId  and self:WZQ_GetOwnerId() == playerId
end

function LuaShiTuMgr:WZQ_IsOpp(playerId)
    return self:WZQ_GetOppId() and playerId and self:WZQ_GetOppId() == playerId 
end

function LuaShiTuMgr:WZQ_IsWatcher(playerId)
    local watcherInfoList = self:WZQ_GetWatcherList()
    for __, watcher in pairs(watcherInfoList) do
        if watcher[1] == playerId then
            return true
        end
    end
    return false
end

function LuaShiTuMgr:WZQ_GetWatcherList()
    local watcherInfoList = self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eWatcherInfoList]
    return watcherInfoList and watcherInfoList or {}
end

function LuaShiTuMgr:WZQ_IsPlayStarted()
    local startTime = self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eStartTime]
    return startTime and startTime>0
end

function LuaShiTuMgr:WZQ_IsPlayEnded()
    local winPlayerId = self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eWinPlayerId]
    return winPlayerId>0
end

function LuaShiTuMgr:WZQ_GetWinPlayerId()
    local winPlayerId = self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eWinPlayerId]
    return winPlayerId
end

function LuaShiTuMgr:WZQ_GetPastTimeSeconds()
    local startTime = self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eStartTime]
    local curTime = CServerTimeMgr.Inst.timeStamp
    return math.max(0, curTime - startTime)
end

function LuaShiTuMgr:WZQ_GetPastRoundNum()
    return self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eRoundNum] or 0
end

function LuaShiTuMgr:WZQ_GetLastStepTime()
    local lastStepInfo = self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eLastStepInfo]
    local playerId, now, gridIdx = lastStepInfo[1],lastStepInfo[2],lastStepInfo[3]
    return now
end

function LuaShiTuMgr:WZQ_GetLeftSetGridTime()
    local lastStepInfo = self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eLastStepInfo]
    local playerId, now, gridIdx = lastStepInfo[1],lastStepInfo[2],lastStepInfo[3]
    local curTime = CServerTimeMgr.Inst.timeStamp
    return math.max(0, self.m_WuZiQiPlayStepMaxDuration -(curTime-now))
end

function LuaShiTuMgr:WZQ_GetLastStepPlayerId()
    local lastStepInfo = self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eLastStepInfo]
    local playerId, now, gridIdx = lastStepInfo[1],lastStepInfo[2],lastStepInfo[3]
    return playerId
end

function LuaShiTuMgr:WZQ_GetLastStepGridIndex()
    local lastStepInfo = self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eLastStepInfo]
    local playerId, now, gridIdx = lastStepInfo[1],lastStepInfo[2],lastStepInfo[3]
    return gridIdx
end

function LuaShiTuMgr:WZQ_CanSetGrid()
    if self:WZQ_MainPlayerIsOwner() or self:WZQ_MainPlayerIsOpp() then
        return self:WZQ_IsPlayStarted() and self:WZQ_GetLastStepPlayerId()~=CClientMainPlayer.Inst.Id
    end
    return false
end

function LuaShiTuMgr:WZQ_CanMarkGrid()
    return self:WZQ_IsPlayStarted() and self:WZQ_MainPlayerIsWatcher()
end

function LuaShiTuMgr:WZQ_GetGridIdxInfo()
    --gridIdx to gridColor
    -- lastGridIdx
    return self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eQiPan], self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eLastStepInfo][3]
end

--准备/取消准备
function LuaShiTuMgr:RequestWuZiQiReady(bReady)
    Gac2Gas.RequestWuZiQiReady(self:WZQ_GetPlayId(), bReady)
end
--邀请对手
function LuaShiTuMgr:InvitePlayerJoinWuZiQi(oppId)
    Gac2Gas.InvitePlayerJoinWuZiQi(self:WZQ_GetPlayId(), oppId)
end
--对手接受邀请
function LuaShiTuMgr:AcceptInviteJoinWuZiQi(playId, ownerId)
    Gac2Gas.AcceptInviteJoinWuZiQi(playId, ownerId)
end
--查询师门在线玩家
function LuaShiTuMgr:QueryPlayerForWuZiQi(forWatcher, go, showMenuOnRight)
    local oppId = self:WZQ_GetOppId()
    --师徒五子棋只有一个邀请，其他五子棋有三个选项：附近的人/在线好友/发送至世界频道
    if LuaShiTuMgr:WZQ_IsShiTuTask() then
		local context_U = g_MessagePack.pack({forWatcher and 1 or 0})
		Gac2Gas.QueryPlayerForWuZiQi(self:WZQ_GetPlayId(), oppId and oppId or 0, context_U)
	else
		--附近的人/在线好友/发送至世界频道
		local inviteNearbyMenu = g_PopupMenuMgr:CreateOneDefaultPopupMenuItem(LocalString.GetString("邀请附近的人"), function()
			local context_U = g_MessagePack.pack({forWatcher and 1 or 0})
			Gac2Gas.QueryPlayerForWuZiQi(self:WZQ_GetPlayId(), oppId and oppId or 0, context_U)
		end, false)
		local inviteOnlineFriendMenu = g_PopupMenuMgr:CreateOneDefaultPopupMenuItem(LocalString.GetString("邀请在线好友"), function() 
			self.m_WuZiQiPlayIsInviteFriendForWatcher = forWatcher
			CLuaIMMgr:QueryOnlineReverseFriends(EnumQueryOnlineReverseFriendsContext_lua.ShiTuWuZiQi)
		end, false)
		local sendToWorldChannelMenu = g_PopupMenuMgr:CreateOneDefaultPopupMenuItem(LocalString.GetString("发送至世界频道"), function()
			local msgName = forWatcher and "WU_ZI_QI_INVITE_WATCH_PLAY" or "WU_ZI_QI_INVITE_JOIN"
			local ownerId, ownerName = self:WZQ_GetOwnerInfo()
      if ownerId==nil or ownerId==0 then
          g_MessageMgr:ShowMessage("WU_ZI_QI_INVITATION_NOT_EXIST")
          return
      end
			CChatHelper.SendMsgWithFilterOption(EChatPanel.World, g_MessageMgr:FormatMessage(msgName, ownerId, ownerName, self:WZQ_GetPlayId()), 0, true)
			g_MessageMgr:ShowMessage("WU_ZI_QI_INVITE_WORLD_CHANNEL_FINIESHED")
		end, false)
		
		local tbl = {}
		table.insert(tbl, inviteNearbyMenu)
		table.insert(tbl, inviteOnlineFriendMenu)
		table.insert(tbl, sendToWorldChannelMenu)
		if showMenuOnRight then
			g_PopupMenuMgr:ShowPopupMenuOnRight(tbl, go.transform)
		else
			g_PopupMenuMgr:ShowPopupMenuOnLeft(tbl, go.transform)
		end
	end    
end
--邀请观众
function LuaShiTuMgr:InvitePlayerWatchWuZiQi(watcherId)
    Gac2Gas.InvitePlayerWatchWuZiQi(self:WZQ_GetPlayId(), watcherId)
end
--观众接受邀请
function LuaShiTuMgr:AcceptWatchWuZiQiInvite(playId, ownerId)
    Gac2Gas.AcceptWatchWuZiQiInvite(playId, ownerId)
end
--离开五子棋玩法
function LuaShiTuMgr:LeaveWuZiQi()
    Gac2Gas.LeaveWuZiQi(self:WZQ_GetPlayId())
end
--下棋
function LuaShiTuMgr:RequestSetWuZiQiGrid(gridIndex)
    Gac2Gas.RequestSetWuZiQiGrid(self:WZQ_GetPlayId(), gridIndex)
end
--剩余允许悔棋的时间
function LuaShiTuMgr:WZQ_GetResetGridLeftTime()
	local curTime = CServerTimeMgr.Inst.timeStamp
    local leftTime = math.max(0, self.m_WuZiQiPlayStepAllowResetEndTime - curTime)
	return leftTime
end
--请求悔棋
function LuaShiTuMgr:RequestResetWuZiQiGrid()
    if not self:WZQ_IsPlayStarted() then return end
    local otherPlayerId = self:WZQ_MainPlayerIsOwner() and self:WZQ_GetOppId() or self:WZQ_GetOwnerId()
    local leftTime = self:WZQ_GetResetGridLeftTime()
    -- otherPlayerId, leftTime
	if leftTime<=0 then
		--正常情况下执行不到这里，在调用方法前就拦截并提示了
		g_MessageMgr:ShowMessage("ShiTu_WuZiQi_Reset_Timeout")
	else
    	Gac2Gas.RequestResetWuZiQiGrid(self:WZQ_GetPlayId(), otherPlayerId, leftTime)
	end
end
--观众指棋
function LuaShiTuMgr:RequestMarkWuZiQiGrid(gridIndex)
    Gac2Gas.RequestMarkWuZiQiGrid(self:WZQ_GetPlayId(), gridIndex)
end
--聊天
function LuaShiTuMgr:RequestChatInWuZiQi(chatMsg)
    Gac2Gas.RequestChatInWuZiQi(self:WZQ_GetPlayId(), chatMsg)
end
--认输
function LuaShiTuMgr:RequestGiveUpWuZiQi()
    Gac2Gas.RequestGiveUpWuZiQi(self:WZQ_GetPlayId())
end
--再来一局
function LuaShiTuMgr:RestartWuZiQi()
    local playerIdTbl = {}
    local ownerId =self:WZQ_GetOwnerId()
    local oppId = self:WZQ_GetOppId()
    if not ownerId or not oppId or ownerId==0 or oppId==0 then
        CUIManager.CloseUI("ShiTuWuZiQiWnd")
        g_MessageMgr:ShowMessage("ShiTu_WuZiQi_Opposite_Player_Has_Gone")
        return
    end
    table.insert(playerIdTbl, ownerId and ownerId or 0)
    table.insert(playerIdTbl, oppId and oppId or 0)
    local watcherInfoList = self:WZQ_GetWatcherList()
    for __, watcher in pairs(watcherInfoList) do
        table.insert(playerIdTbl,watcher[1])
    end
    Gac2Gas.RestartWuZiQi(self:WZQ_GetPlayType(), g_MessagePack.pack(playerIdTbl))
end
--服务器返回五子棋玩法信息
function LuaShiTuMgr:SendWuZiQiInfo(rpcType, info_U)
    local infoTbl = g_MessagePack.unpack(info_U)

    if rpcType == EnumWuZiQiRpcType.eOpenWnd then
        if infoTbl then 
            infoTbl = infoTbl[1]--取第一个元素才是真正的服务器数据 
        else
            print("SendWuZiQiInfo Server Data InValid")
            return
        end
        self.m_WuZiQiPlayInfo = infoTbl --服务器定义的结构
        CUIManager.ShowUI("ShiTuWuZiQiWnd")
    elseif rpcType == EnumWuZiQiRpcType.eAddOppInfo then
        local playerId, name, class, gender = infoTbl[1], infoTbl[2], infoTbl[3], infoTbl[4]
        self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eOppId] = playerId
        self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eOppInfo] = {name, class, gender}
        g_ScriptEvent:BroadcastInLua("OnShiTuWuZiQiPlayerInfoUpdate")
    elseif rpcType == EnumWuZiQiRpcType.eSetPlayerReady then
        local playerId, bReady = infoTbl[1], infoTbl[2]
        if playerId ~= self:WZQ_GetOwnerId() and playerId ~= self:WZQ_GetOppId() then
            return
        end
        local readyInfo = self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eReadyInfo]
        readyInfo[playerId] = bReady
        g_ScriptEvent:BroadcastInLua("OnShiTuWuZiQiPlayerInfoUpdate")
    elseif rpcType == EnumWuZiQiRpcType.eStart then
        local now = infoTbl[1]
        self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eStartTime] = now
        self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eLastStepInfo] = { self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eOppId], now, 0}		-- 把对手塞个0步骤，让创建人走第一步
        g_ScriptEvent:BroadcastInLua("OnShiTuWuZiQiPlayStarted")
    elseif rpcType == EnumWuZiQiRpcType.eSetGridColor then
        local gridIndex, gridColor, playerId, now = infoTbl[1], infoTbl[2], infoTbl[3], infoTbl[4]
        self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eQiPan][gridIndex] = gridColor
        self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eLastStepInfo][1] = playerId
        self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eLastStepInfo][2] = now
        self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eLastStepInfo][3] = gridIndex
        self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eRoundNum] = self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eRoundNum] + 1
        if CClientMainPlayer.Inst.Id == playerId then
            self.m_WuZiQiPlayStepAllowResetEndTime = now + self.m_WuZiQiPlayStepAllowResetMaxTime
        end
        g_ScriptEvent:BroadcastInLua("OnShiTuWuZiQiGridInfoUpdate")
    elseif rpcType == EnumWuZiQiRpcType.eSetResult then
        local winPlayerId, losePlayerId, roundNum, resultGridInfo = infoTbl[1], infoTbl[2], infoTbl[3], infoTbl[4]
        self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eWinPlayerId] = winPlayerId
        self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eRoundNum] = roundNum
        if self:WZQ_MainPlayerIsWatcher() then
            g_MessageMgr:ShowMessage("ShiTu_WuZiQi_Over_Msg_For_Watcher")
        end
        g_ScriptEvent:BroadcastInLua("OnShiTuWuZiQiReturnResult", winPlayerId, resultGridInfo)
    elseif rpcType == EnumWuZiQiRpcType.eResetGrid then
        local lastGridIndex, roundNum, playerId, now = infoTbl[1], infoTbl[2], infoTbl[3], infoTbl[4]
        local otherPlayerId = (playerId == self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eOwnerId]) and self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eOppId] or self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eOwnerId]
        local lastStepInfo = self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eLastStepInfo]
        lastStepInfo[1], lastStepInfo[2], lastStepInfo[3] = otherPlayerId, now, EnumWuZiQiGridColor.eNone
        self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eQiPan][lastGridIndex] = EnumWuZiQiGridColor.eNone
        self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eRoundNum] = roundNum
        g_ScriptEvent:BroadcastInLua("OnShiTuWuZiQiGridInfoUpdate")
    elseif rpcType == EnumWuZiQiRpcType.eAddWatcher then
        local watcherId, name, gender, class = infoTbl[1], infoTbl[2], infoTbl[3], infoTbl[4]
        if self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eWatcherInfoList]==nil then self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eWatcherInfoList] = {} end
        for index, info in pairs(self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eWatcherInfoList]) do
            if info[1] == watcherId then
                return
            end
        end
        table.insert(self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eWatcherInfoList], {watcherId, name, gender, class})
        g_ScriptEvent:BroadcastInLua("OnShiTuWuZiQiWatcherInfoUpdate")
    elseif rpcType == EnumWuZiQiRpcType.eDelWatcher then
        local watcherId = infoTbl[1]
        local watcherInfoList = self:WZQ_GetWatcherList()
        for index, info in ipairs(watcherInfoList) do
            if info[1] == watcherId then
                table.remove(watcherInfoList, index)
                break
            end
        end
        g_ScriptEvent:BroadcastInLua("OnShiTuWuZiQiWatcherInfoUpdate")
    elseif rpcType == EnumWuZiQiRpcType.eDelPlayer then
        local playerId = infoTbl[1]
        if playerId == self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eOwnerId] then
            self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eOwnerId] = 0
            self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eOwnerInfo] = {}
        elseif playerId == self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eOppId] then
            self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eOppId] = 0
            self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eOppInfo] = {}
        end
        if self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eReadyInfo] and self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eReadyInfo][playerId] then
          self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eReadyInfo][playerId] = nil
        end
        g_ScriptEvent:BroadcastInLua("OnShiTuWuZiQiPlayerInfoUpdate")
    elseif rpcType == EnumWuZiQiRpcType.eDestroy then
        self.m_WuZiQiPlayInfo = {}
        CUIManager.CloseUI("ShiTuWuZiQiWnd")
    elseif rpcType == EnumWuZiQiRpcType.eMarkGrid then
        local playerId, gridIdx = infoTbl[1], infoTbl[2]
        --显示指棋 gridIdx取值[1,255]
        local watcherInfoList = self:WZQ_GetWatcherList()
        for __, watcher in pairs(watcherInfoList) do
            if watcher[1] == playerId then
                g_ScriptEvent:BroadcastInLua("OnRecvShiTuWuZiQiMarkGridInfo", gridIdx, watcher[2])
                break
            end
        end
    elseif rpcType == EnumWuZiQiRpcType.eChatMsg then
        local playerId, chatMsg = infoTbl[1], infoTbl[2]
        --发送弹幕或者气泡
        g_ScriptEvent:BroadcastInLua("OnRecvShiTuWuZiQiChatMsg", playerId, chatMsg)
    end
end
--服务器返回在线玩家信息
function LuaShiTuMgr:QueryPlayerForWuZiQiResult(playId, result_U, context_U)
    --{Id, masterPlayer.m_Name, masterPlayer.m_Class, masterPlayer.m_Gender, masterPlayer.m_Grade}
	if self:WZQ_GetPlayId()~= playId then return end
    local tbl = g_MessagePack.unpack(result_U)
	local contextTbl = g_MessagePack.unpack(context_U)
	local context = contextTbl and contextTbl[1] or nil
	if context==nil then return end
    if tbl == nil or #tbl==0 then
		if self:WZQ_IsShiTuTask() then
        	g_MessageMgr:ShowMessage("ShiTu_WuZiQi_No_Online_Shi_Men_Players")
		else
			g_MessageMgr:ShowMessage("WU_ZI_QI_NO_AVALIABLE_NEARBY_PLAYERS")
		end
        return
    end
    local onlinePlayerTbl = {}
    for __, data in pairs(tbl) do
        table.insert(onlinePlayerTbl, {
          id = data[1],
          name = data[2],
          class = data[3],
          gender = data[4],
          grade = data[5]
        })
    end
    LuaCommonPlayerListMgr:ShowPlayersForShiTuWuZiQi(onlinePlayerTbl, self:WZQ_IsShiTuTask(), context==1)
end

function LuaShiTuMgr:OnQueryOnlineReverseFriendsFinished(playerIds, context)
    if context == EnumQueryOnlineReverseFriendsContext_lua.ShiTuWuZiQi then
        --基于服务器RPC QueryOnlineReverseFriends 的本服在线双向好友
		local title = self.m_WuZiQiPlayIsInviteFriendForWatcher and LocalString.GetString("邀请观众") or LocalString.GetString("邀请对手")
        LuaCommonPlayerListMgr:ShowOnlineFriendsWithCheck(title, LocalString.GetString("邀请"), function(playerId)
			if self.m_WuZiQiPlayIsInviteFriendForWatcher then
				LuaShiTuMgr:InvitePlayerWatchWuZiQi(playerId)
			else
				LuaShiTuMgr:InvitePlayerJoinWuZiQi(playerId)
			end
		end, "WU_ZI_QI_NO_AVALIABLE_ONLINE_FRIENDS", function(playerId)
			--跳过不在服务器返回结果里面的玩家
            return CommonDefs.ListContains_LuaCall(playerIds, playerId)
		end)
	end
end

function LuaShiTuMgr:CheckRoundEnd()
    local now = CServerTimeMgr.Inst.timeStamp
    if self.m_WuZiQiPlayInfo and self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eStartTime] > 0 
        and self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eWinPlayerId] == 0
        and (self:WZQ_MainPlayerIsOpp() or self:WZQ_MainPlayerIsOwner()) and CClientMainPlayer.Inst and CClientMainPlayer.Inst~=self:WZQ_GetLastStepPlayerId()
        and now > self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eLastStepInfo][2] + self.m_WuZiQiPlayStepMaxDuration then
        return true
    else
        return false
    end
end

--自动选一个位置下棋,copy自服务器端function CMasterWuZiQiMgr:ForceSetGrid(playId)
function LuaShiTuMgr:GetRandomSetGridIndex()
    if not self.m_WuZiQiPlayInfo then return end

    local maxGridIndex = 225 --棋盘总的点位数 15x15
    -- current round playerId
    local lastPlayerId = self:WZQ_GetLastStepPlayerId()
    local curPlayerId = (lastPlayerId == self:WZQ_GetOwnerId()) and self:WZQ_GetOppId() or self:WZQ_GetOwnerId()

    -- random grid
    local gridIndex = nil
    for i = 1, 5 do
        local index = math.random(maxGridIndex)
        if self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eQiPan][index] == EnumWuZiQiGridColor.eNone then
            gridIndex = index
            break
        end
    end
    if not gridIndex then
        for i = 1, maxGridIndex do
            if self.m_WuZiQiPlayInfo[EnumWuZiQiInfoKey.eQiPan][i] == EnumWuZiQiGridColor.eNone then
                gridIndex = i
                break
            end
        end
    end

    return gridIndex
end

------------
----情义春秋-合成大西瓜（复用水果派对）
------------
LuaShiTuMgr.m_XiGuaInfo = {}
LuaShiTuMgr.m_FruitPartyTaskPlayId = nil
LuaShiTuMgr.m_FruitPartyNormalPlayId = nil
function LuaShiTuMgr:GetFruitPartyTaskPlayId()
    if self.m_FruitPartyTaskPlayId == nil then
        self.m_FruitPartyTaskPlayId = ShuiGuoPaiDui_Setting.GetData().ShiTuTaskGamePlayId
    end
    return self.m_FruitPartyTaskPlayId
end

function LuaShiTuMgr:GetFruitPartyNormalPlayId()
    if self.m_FruitPartyNormalPlayId == nil then
        self.m_FruitPartyNormalPlayId = ShuiGuoPaiDui_Setting.GetData().ShiTuNormalGamePlayId
    end
    return self.m_FruitPartyNormalPlayId
end

function LuaShiTuMgr:IsInFruitPartyTaskPlay()
    return  CScene.MainScene and CScene.MainScene.GamePlayDesignId == self:GetShiTuFruitPartyTaskPlayId() or false
end

function LuaShiTuMgr:GetFruitPartyTaskTitle()
    local task = Task_Task.GetData(ShiTu_Setting.GetData().ShuiGuoTaskId)
    return task and task.Display or ""
end

function LuaShiTuMgr:GetFruitPartyTaskContent()
    local task = Task_Task.GetData(ShiTu_Setting.GetData().ShuiGuoTaskId)
    local curCount = self.m_XiGuaInfo.curNum and self.m_XiGuaInfo.curNum or 0
    local needCount =self.m_XiGuaInfo.needNum and self.m_XiGuaInfo.needNum or 0
    return SafeStringFormat3(LocalString.GetString("%s\n合成西瓜（%d/%d）"), g_MessageMgr:FormatMessage("ShiTu_ShuiGuo_Description"), curCount, needCount)
end

function LuaShiTuMgr:OnFruitPartyTaskRuleClick()
    g_MessageMgr:ShowMessage("JIUXIAGUOYIN_SIGNWND_TIP")
end

function LuaShiTuMgr:ShowShiTuFruitPartySignWnd()
    CUIManager.ShowUI("ShiTuFruitPartySignWnd")
end

function LuaShiTuMgr:ShowShiTuFriutPartyRankWnd()
    --rank窗口完全复用FruitPartyRankWnd
    CUIManager.ShowUI("FruitPartyRankWnd")
end

function LuaShiTuMgr:RequestEnterShuiGuoPaiDuiForShiTu()
    Gac2Gas.EnterShuiGuoPaiDuiForShiTuTask()
end

function LuaShiTuMgr:SyncShuiGuoPaiDuiXiGuaNum(xiGuaNum, needXiGuaNum, bAddXiGua)
    self.m_XiGuaInfo.curNum = xiGuaNum
    self.m_XiGuaInfo.needNum = needXiGuaNum
    g_ScriptEvent:BroadcastInLua("SyncShuiGuoPaiDuiXiGuaNum", xiGuaNum, needXiGuaNum, bAddXiGua)
    LuaCommonGamePlayTaskViewMgr:OnUpdateContent()
end

------------
----情义春秋-渡灵
------------
LuaShiTuMgr.m_DuLingTotalScore = 0

function LuaShiTuMgr:RequestStartShiTuDuLingTask()
    Gac2Gas.RequestStartShiTuDuLingTask()
end

function LuaShiTuMgr:ShowShiTuDuLingTaskWnd()
    self.m_DuLingTotalScore = 0
    CUIManager.ShowUI("ShiTuDuLingTaskWnd")
end

function LuaShiTuMgr:CloseShiTuDuLingTaskWnd()
    self.m_DuLingTotalScore = 0
    CUIManager.CloseUI("ShiTuDuLingTaskWnd")
end

function LuaShiTuMgr:RequestSyncShiTuDuLingTaskInfo(otherPlayerId, total, add, index)
    self.m_DuLingTotalScore = math.max(0, total + add)
    Gac2Gas.SyncShiTuDuLingTaskInfo(otherPlayerId, total, add, index)
    g_ScriptEvent:BroadcastInLua("OnSyncShiTuDuLingTaskInfo", true, total, add, index)
    if self.m_DuLingTotalScore >= ShiTu_Setting.GetData().DuLingNeedScore then
        self:RequestFinishShiTuDuLingTask()
    end
end

function LuaShiTuMgr:SyncShiTuDuLingTaskInfo(total, add, index)
    self.m_DuLingTotalScore = math.max(0, total + add)
    g_ScriptEvent:BroadcastInLua("OnSyncShiTuDuLingTaskInfo", false, total, add, index)
end

function LuaShiTuMgr:RequestFinishShiTuDuLingTask()
    Gac2Gas.RequestFinishShiTuDuLingTask()
end

------------
----情义春秋-找秘籍
------------
LuaShiTuMgr.m_ZhaoMiJiMonsterPos = {}
LuaShiTuMgr.m_ZhaoMiJiMonsterInfo = {}

function LuaShiTuMgr:IsInZhaoMiJiPlay()
    return  CScene.MainScene and CScene.MainScene.GamePlayDesignId == ShiTuTask_ZhaoMiJiSetting.GetData().GamePlayId or false
end

function LuaShiTuMgr:ShowShiTuZhaoMiJiStartWnd(posX, posY)
    self.m_ZhaoMiJiMonsterPos.x = posX
    self.m_ZhaoMiJiMonsterPos.y = posY
    --服务器会先发SyncShiTuZhaoMiJiScore后发ShowShiTuZhaoMiJiStartWnd
    --self.m_ZhaoMiJiMonsterInfo.curCount = 0
    --self.m_ZhaoMiJiMonsterInfo.needCount = 0
    CUIManager.ShowUI("ShiTuCompassWnd")
end

function LuaShiTuMgr:SyncShiTuZhaoMiJiScore(curScore, needScore)
    self.m_ZhaoMiJiMonsterInfo.curCount = curScore
    self.m_ZhaoMiJiMonsterInfo.needCount = needScore
    g_ScriptEvent:BroadcastInLua("SyncShiTuZhaoMiJiScore")
end

function LuaShiTuMgr:SyncShiTuZhaoMiJiMonsterPos(posX, posY)
    self.m_ZhaoMiJiMonsterPos.x = posX
    self.m_ZhaoMiJiMonsterPos.y = posY
    g_ScriptEvent:BroadcastInLua("SyncShiTuZhaoMiJiMonsterPos")
end
------------
----情义春秋-捡垃圾
------------
LuaShiTuMgr.m_JianLaJiGossipTbl = nil
LuaShiTuMgr.m_JianLaJiBuffTitleTbl = nil
LuaShiTuMgr.m_JianLaJiScoreInfo = {}
function LuaShiTuMgr:IsInJianLaJiPlay()
  return  CScene.MainScene and CScene.MainScene.GamePlayDesignId == ShiTuTask_JianLaJiSetting.GetData().GamePlayId or false
end

function LuaShiTuMgr:ShowShiTuJianLaJiMsg(engineId, bSuccess)
    if self.m_JianLaJiGossipTbl==nil then
        self.m_JianLaJiGossipTbl = {}
        ShiTuTask_JianLaJiGossip.Foreach(function(k,v)
            if self.m_JianLaJiGossipTbl[v.Type] == nil then
                self.m_JianLaJiGossipTbl[v.Type] = {}
            end
            table.insert(self.m_JianLaJiGossipTbl[v.Type], v.Content)
        end)
    end
    local candidates = self.m_JianLaJiGossipTbl[bSuccess and 0 or 1]
    if candidates then
        CClientObject.SendSayContent(engineId, candidates[math.random(1,#candidates)], 1.0, nil)
    end
end

function LuaShiTuMgr:GetBuffTitleTbl()
    if self.m_JianLaJiBuffTitleTbl==nil then
        self.m_JianLaJiBuffTitleTbl = {}
        ShiTuTask_BuffInfo.Foreach(function(k,v)
            local buff = Buff_Buff.GetData(k)
            self.m_JianLaJiBuffTitleTbl[k] = buff.Name
        end)
    end
    return self.m_JianLaJiBuffTitleTbl
end

function LuaShiTuMgr:SyncShiTuJianLaJiScore(curScore, needScore)
    self.m_JianLaJiScoreInfo = {}
    self.m_JianLaJiScoreInfo.curScore = curScore
    self.m_JianLaJiScoreInfo.needScore = needScore
    g_ScriptEvent:BroadcastInLua("SyncShiTuJianLaJiScore", curScore, needScore)
end
------------
----师徒礼馈
------------
LuaShiTuMgr.s_SendGiftToShiFu = false
LuaShiTuMgr.m_PlayerIdReceivedGift = 0
LuaShiTuMgr.m_WishGifType = 0
LuaShiTuMgr.m_SelectShopItemType = 0
function LuaShiTuMgr:OpenShiTuGiftGivingWnd(otherPlayerId, isShiFu)
  self.s_SendGiftToShiFu = isShiFu
  self.m_PlayerIdReceivedGift = otherPlayerId
  Gac2Gas.RequestPrepareSendShiTuGift(otherPlayerId)
  --CUIManager.ShowUI(CLuaUIResources.ShiTuGiftGivingWnd)
end

function LuaShiTuMgr:SendShiTuGiftSucc()
end

function LuaShiTuMgr:SetShiTuGiftWishSucc(isShifu, otherPlayerId, winshItemId, wishItemCount, wishContent)
    if LuaShiTuMgr.m_WishGifType == 0 then
      LuaShiTuMgr.m_PrepareGiftRet.targetWinshItemId = winshItemId
      LuaShiTuMgr.m_PrepareGiftRet.targetWishItemCount = wishItemCount
      LuaShiTuMgr.m_PrepareGiftRet.targetWishContent = wishContent
    else
      LuaShiTuMgr.m_PrepareGiftRet.myWinshItemId = winshItemId
      LuaShiTuMgr.m_PrepareGiftRet.myWishItemCount = wishItemCount
      LuaShiTuMgr.m_PrepareGiftRet.myWishContent = wishContent
    end
    g_ScriptEvent:BroadcastInLua("OnSetShiTuGiftWishSucc",isShifu, otherPlayerId, winshItemId, wishItemCount, wishContent)
end

LuaShiTuMgr.m_PrepareGiftRet = {}
function LuaShiTuMgr:PrepareSendShiTuGiftRet(isShifu, otherPlayerId, targetWinshItemId, targetWishItemCount, targetWishContent, myWinshItemId, myWishItemCount, myWishContent, remainTimes, remainJadeLimit, dynamicOnShelfItems)
  local list = g_MessagePack.unpack(dynamicOnShelfItems) or {}
  self.m_PrepareGiftRet = {
    isShifu = isShifu, otherPlayerId = otherPlayerId, remainTimes = remainTimes,
    targetWinshItemId = targetWinshItemId, targetWishItemCount = targetWishItemCount, targetWishContent = targetWishContent, 
    myWinshItemId = myWinshItemId, myWishItemCount = myWishItemCount, myWishContent = myWishContent,
    remainJadeLimit = remainJadeLimit,dynamicOnShelfItems = list
  }
  CLuaShopMallMgr.m_DynamicOnShelfItems = list
  CUIManager.ShowUI(CLuaUIResources.ShiTuGiftGivingWnd)
end
------------
----家谱师徒评价
------------
LuaShiTuMgr.m_FamilyTreeShiMenInfo = {}
function LuaShiTuMgr:SendFamilyTreeShiMenInfo(targetPlayerId, isBestTudi, shimenLevel, bestTudiIds)
    local list = g_MessagePack.unpack(bestTudiIds) or {}
    local set = {}
    for _, tudiId in ipairs(list) do
        set[tudiId] = true
    end
    self.m_FamilyTreeShiMenInfo = {targetPlayerId = targetPlayerId, isBestTudi = isBestTudi, shimenLevel = shimenLevel, bestTudiIdSet = set}
    g_ScriptEvent:BroadcastInLua("OnSendFamilyTreeShiMenInfo", targetPlayerId, isBestTudi, shimenLevel, set)
end

function LuaShiTuMgr:ShowShifuPingJiaWnd()
  CUIManager.ShowUI(CLuaUIResources.ShifuPingJiaWnd)
end

------------
----拜师帖、出师帖
------------
LuaShiTuMgr.m_BaiShiPosterItemid = ""
LuaShiTuMgr.m_ChuShiPosterItemid = ""
LuaShiTuMgr.m_ChuShiPosterExtraData = nil
LuaShiTuMgr.m_IsChangingToChuShiPosterWnd = false
function LuaShiTuMgr:ShowBaiShiPosterWnd(itemid)
  self.m_BaiShiPosterItemid = itemid
  local item = CItemMgr.Inst:GetById(itemid)
  if item then
    if item.Item.ExtraVarData ~= nil and item.Item.ExtraVarData.Data ~= nil then
      local raw = item.Item.ExtraVarData.Data
      local list = g_MessagePack.unpack(raw)
      if #list >= 1 then
          local shifuId = list[1]
          local chushiTime = list[8]
          if (shifuId ~= CShiTuMgr.Inst:GetCurrentShiFuId()) or (chushiTime > 0) then
            local cost = ShiTu_Setting.GetData().ChuShiTieNeedYinPiao
            QnMoneyCostMesssageMgr.ShowMoneyCostMessageBox(EnumMoneyType.YinPiao, g_MessageMgr:FormatMessage("ConvertToChuShiPoster_Confirm"), cost, DelegateFactory.Action(function ()
              if CClientMainPlayer.Inst then
                local bagSize = CClientMainPlayer.Inst.ItemProp:GetPlaceSize(EnumItemPlace.Task)
                for i = 1,bagSize do
                  local _itemId = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Task, i)
                  if _itemId == itemid then
                    local pos = i
                    Gac2Gas.RequestExchangeShiTuBaiShiTieToChuShiTie(3, pos, itemid) 
                    break
                  end
                end
              end
            end), nil, false, true, EnumPlayScoreKey.NONE, false)
            return
          end
      end
    end
  end
  CUIManager.ShowUI(CLuaUIResources.BaiShiPosterWnd)
end

function LuaShiTuMgr:ShowChuShiPosterWnd(itemId)
  self.m_IsChangingToChuShiPosterWnd = false
  self.m_ChuShiPosterItemid = itemId
  self.m_ChuShiPosterExtraData = nil
  CUIManager.ShowUI(CLuaUIResources.ChuShiPosterWnd)
end

function LuaShiTuMgr:ShowChuShiPosterWndWithCreateAni(data)
  self.m_IsChangingToChuShiPosterWnd = true
  self.m_ChuShiPosterItemid = ""
  self.m_ChuShiPosterExtraData = data
  CUIManager.ShowUI(CLuaUIResources.ChuShiPosterWnd)
end
------------
----群聊
------------
LuaShiTuMgr.m_ShifuAllTudiInfo = {}
function LuaShiTuMgr:SendShifuAllTudiInfo(shifuId, requestType, allTudiInfo)
  LuaShiTuMgr.m_ShifuAllTudiInfo = {
    shifuId = shifuId,
    allTudiInfo = g_MessagePack.unpack(allTudiInfo)
  }
  g_ScriptEvent:BroadcastInLua("OnSendShifuAllTudiInfo")
end

