local QnTableView = import "L10.UI.QnTableView"
local CItemMgr = import "L10.Game.CItemMgr"
local DelegateFactory  = import "DelegateFactory"
local CZuoQiMgr = import "L10.Game.CZuoQiMgr"
local CButton = import "L10.UI.CButton"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaVehicleBatchRenewalFeesWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaVehicleBatchRenewalFeesWnd, "QnTableView", "QnTableView", QnTableView)

--@endregion RegistChildComponent end
RegistClassMember(LuaVehicleBatchRenewalFeesWnd,"m_List")

function LuaVehicleBatchRenewalFeesWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaVehicleBatchRenewalFeesWnd:Init()
    self.QnTableView.m_DataSource = DefaultTableViewDataSource.Create(
        function() 
            return self.m_List and #self.m_List or 0
        end,
        function(item, index)
            self:ItemAt(item,index)
        end)
    -- self.QnTableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
    --     self:OnSelectAtRow(row)
    -- end)
    self.m_List = LuaVehicleRenewalMgr.m_RenewZuoQiByItemWndData.zuoqiDataList
    -- CommonDefs.ListIterate(CZuoQiMgr.Inst.zuoqis, DelegateFactory.Action_object(function (___value) 
    --     local zuoqi = ___value
    --     local zqdata = ZuoQi_ZuoQi.GetData(zuoqi.templateId)
    --     if zqdata ~= nil then
    --         table.insert(self.m_List, {id = zuoqi.id, templateId = zuoqi.templateId, expTime = zuoqi.expTime})
    --     end
    -- end))
    table.sort(self.m_List, function (a, b)
        local designData1 = ZuoQi_ZuoQi.GetData(a.templateId)
        local designData2 = ZuoQi_ZuoQi.GetData(b.templateId)
        if designData1.AvailableTime == 0 and designData2.AvailableTime ~= 0 then
            return false
        elseif designData2.AvailableTime == 0 and designData1.AvailableTime ~= 0 then
            return true
        else
            return a.expTime < b.expTime
        end
    end)
    self.QnTableView:ReloadData(false, false)
end

function LuaVehicleBatchRenewalFeesWnd:ItemAt(item,index)
    local iconTexture = item.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local timeLabel = item.transform:Find("TimeLabel"):GetComponent(typeof(UILabel))
    local renewalButton = item.transform:Find("RenewalButton"):GetComponent(typeof(CButton))

    local t = self.m_List[index + 1]
    local designData = ZuoQi_ZuoQi.GetData(t.templateId)
    local zqdata = CZuoQiMgr.Inst:GetZuoQiById(t.id)
    local itemdata = Item_Item.GetData(designData.ItemID)

    local icon = ""
    if itemdata ~= nil then
        icon = itemdata.Icon
    end
    iconTexture:LoadMaterial(icon)

    nameLabel.text = designData.Name
    if zqdata ~= nil and zqdata.mapiInfo ~= nil then
        nameLabel.text = zqdata.mapiInfo.Name
    end

    self:InitTimeLabel(timeLabel,t, designData, zqdata)

    renewalButton.Text = SafeStringFormat3(LocalString.GetString("续费%d天"),LuaVehicleRenewalMgr.m_RenewZuoQiByItemWndData.days)

    UIEventListener.Get(renewalButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRenewalButtonClick(nameLabel.text, t.id)
	end)
end

function LuaVehicleBatchRenewalFeesWnd:InitTimeLabel(timeLabel,t, designData, zqdata)
    local curtime = CServerTimeMgr.Inst.timeStamp
    local dt = CServerTimeMgr.ConvertTimeStampToZone8Time(t.expTime)
    if designData.AvailableTime ~= 0 and curtime > t.expTime then
        timeLabel.color = Color(0.9, 0.15, 0.15, 0.8)
        timeLabel.text = SafeStringFormat3(LocalString.GetString("已失效  %d-%d-%d,%02d:%02d"), dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute)
        if designData.RenewalAvailableTime then
            local renewalCutOffTime = CServerTimeMgr.Inst:GetDayBegin(dt)
            renewalCutOffTime = renewalCutOffTime:AddDays(designData.RenewalRange[0] + 1)   
            local outOfData = (renewalCutOffTime:Subtract(CServerTimeMgr.Inst:GetZone8Time())).TotalSeconds <= 0
            local isRenewalCountEnough = zqdata.renewalCount < designData.RenewalAvailableTime[1]
            local canRenewal = isRenewalCountEnough and (not outOfData)
            if canRenewal then
                timeLabel.color = Color(1,1,1,0.8)
                local text = SafeStringFormat3(LocalString.GetString("[E52626]已失效  [999595]%d-%d-%d前可续费"), renewalCutOffTime.Year, renewalCutOffTime.Month, renewalCutOffTime.Day)
                timeLabel.text = CUICommonDef.TranslateToNGUIText(text)  
            end
        end
    else
        timeLabel.color = Color(1, 1, 1, 0.5)
        if designData.AvailableTime == 0 then
            timeLabel.text = LocalString.GetString("永久有效")
        else
            timeLabel.text = SafeStringFormat3(LocalString.GetString("[B6BDC8]有效期至（%d-%d-%d,%02d:%02d）"), dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute)
        end
    end
end
--@region UIEvent
function LuaVehicleBatchRenewalFeesWnd:OnRenewalButtonClick(vehicleName, zuoqiId)
    local item = CItemMgr.Inst:GetById(LuaVehicleRenewalMgr.m_RenewZuoQiByItemWndData.itemId)
    local msg = g_MessageMgr:FormatMessage("VehicleBatchRenewalFeesWnd_Renewal_Confirm",item.Name,vehicleName, LuaVehicleRenewalMgr.m_RenewZuoQiByItemWndData.days)
    MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
        local t = LuaVehicleRenewalMgr.m_RenewZuoQiByItemWndData
        Gac2Gas.RequestRenewZuoQiByItem(t.place, t.pos, t.itemId, zuoqiId)
    end),nil,LocalString.GetString("确定"),LocalString.GetString("取消"),false)
end
--@endregion UIEvent

