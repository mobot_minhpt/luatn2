-- Auto Generated!!
local CChargeMonthCardWnd = import "L10.UI.CChargeMonthCardWnd"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Charge_ChargeSetting = import "L10.Game.Charge_ChargeSetting"
local LocalString = import "LocalString"
local System = import "System"
local CChargeMgr = import "L10.Game.CChargeMgr"
CChargeMonthCardWnd.m_Init_CS2LuaHook = function (this) 
    local chargeSetting = Charge_ChargeSetting.GetData("CardGive_NeedVIPLevel")
    local vipLimit = chargeSetting ~= nil and math.floor(tonumber(chargeSetting.Value or 0)) or 6
    local unlock = (CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.ItemProp.Vip.Level >= vipLimit)
    this.chargeForFriendBtn.Enabled = unlock
    this.chargeForFriendConditionLabel.text = System.String.Format(LocalString.GetString("VIP{0} 解锁"), vipLimit)
    this.chargeForFriendConditionLabel.gameObject:SetActive(not unlock)
end

CChargeMonthCardWnd.m_hookOnChargeForMyselfButtonClick = function (this, go)
    if CChargeMgr.Inst.m_MonthCardID == LuaWelfareMgr.BigMonthCardId then
        LuaWelfareMgr.BuyBigMonthCardForMyself()
    else
        CChargeMgr.Inst:BuyMonthCardForMyself()
    end
    this:Close()
end

CChargeMonthCardWnd.m_hookOnChargeForFriendButtonClick = function (this, go)
    if CChargeMgr.Inst.m_MonthCardID == LuaWelfareMgr.BigMonthCardId then
        LuaWelfareMgr.BuyBigMonthCardForFriend()
    else
        CChargeMgr.Inst:BuyMonthCardForFriend()
    end
    this:Close()
end