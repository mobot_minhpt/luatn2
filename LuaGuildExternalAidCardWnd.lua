local QnTableView = import "L10.UI.QnTableView"
local CChatHelper = import "L10.UI.CChatHelper"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local CInputBoxMgr = import "L10.UI.CInputBoxMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local Constants = import "L10.Game.Constants"
local CButton = import "L10.UI.CButton"

LuaGuildExternalAidCardWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaGuildExternalAidCardWnd, "NoneCardView", "NoneCardView", GameObject)
RegistChildComponent(LuaGuildExternalAidCardWnd, "OwnCardView", "OwnCardView", GameObject)
RegistChildComponent(LuaGuildExternalAidCardWnd, "NoneCardLabel", "NoneCardLabel", UILabel)
RegistChildComponent(LuaGuildExternalAidCardWnd, "NameLabel", "NameLabel", UILabel)
RegistChildComponent(LuaGuildExternalAidCardWnd, "Portrait", "Portrait", CUITexture)
RegistChildComponent(LuaGuildExternalAidCardWnd, "GuildNameLabel", "GuildNameLabel", UILabel)
RegistChildComponent(LuaGuildExternalAidCardWnd, "TypeLabel", "TypeLabel", UILabel)
RegistChildComponent(LuaGuildExternalAidCardWnd, "CoolingTimeLabel", "CoolingTimeLabel", UILabel)
RegistChildComponent(LuaGuildExternalAidCardWnd, "BlueJoinButton", "BlueJoinButton", CButton)
RegistChildComponent(LuaGuildExternalAidCardWnd, "YellowJoinButton", "YellowJoinButton", GameObject)
RegistChildComponent(LuaGuildExternalAidCardWnd, "MyInvitationButton", "MyInvitationButton", GameObject)
RegistChildComponent(LuaGuildExternalAidCardWnd, "TipButton", "TipButton", GameObject)
RegistChildComponent(LuaGuildExternalAidCardWnd, "TableView", "TableView", QnTableView)
RegistChildComponent(LuaGuildExternalAidCardWnd, "QuitButton", "QuitButton", GameObject)
RegistChildComponent(LuaGuildExternalAidCardWnd, "MyInvitationAlertSprite", "MyInvitationAlertSprite", GameObject)
RegistChildComponent(LuaGuildExternalAidCardWnd, "RevocatButton", "RevocatButton", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaGuildExternalAidCardWnd,"m_List")
RegistClassMember(LuaGuildExternalAidCardWnd,"m_SelectRow")
RegistClassMember(LuaGuildExternalAidCardWnd,"m_UpdateTimeTick")

function LuaGuildExternalAidCardWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.BlueJoinButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBlueJoinButtonClick()
	end)


	
	UIEventListener.Get(self.YellowJoinButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnYellowJoinButtonClick()
	end)


	
	UIEventListener.Get(self.MyInvitationButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnMyInvitationButtonClick()
	end)


	
	UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)


	
	UIEventListener.Get(self.QuitButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnQuitButtonClick()
	end)


	
	UIEventListener.Get(self.RevocatButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRevocatButtonClick()
	end)


    --@endregion EventBind end
end

function LuaGuildExternalAidCardWnd:Init()
    if CClientMainPlayer.Inst then
        self.NameLabel.text = CClientMainPlayer.Inst.Name
        self.Portrait:LoadNPCPortrait(CClientMainPlayer.Inst.PortraitName)
    end
    self.TableView.m_DataSource = DefaultTableViewDataSource.Create(function() 
        return self.m_List and #self.m_List or 0
    end, function(item, index)
        self:InitItem(item, index)
    end)

    self.TableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
        self:OnSelectAtRow(row)
    end)

    self.MyInvitationAlertSprite.gameObject:SetActive(false)
    self:InitButtons()
    self:OnSendGuildForeignAidPersonalInfo()
end

function LuaGuildExternalAidCardWnd:InitButtons()
    local isGetCard = LuaGuildExternalAidMgr.m_CardInfo.guildId ~= 0
    self.YellowJoinButton.gameObject:SetActive(not isGetCard)
    self.BlueJoinButton.gameObject:SetActive(isGetCard)
    self.RevocatButton.gameObject:SetActive(false)
end

function LuaGuildExternalAidCardWnd:InitItem(item, index)
    local data = self.m_List[index + 1]

    local guildNameLabel = item.transform:Find("GuildNameLabel"):GetComponent(typeof(UILabel))
    local rankLabel = item.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
    local leaderLabel = item.transform:Find("LeaderLabel"):GetComponent(typeof(UILabel))
    local stateLabel = item.transform:Find("StateLabel"):GetComponent(typeof(UILabel))
    local talkButton = item.transform:Find("TalkButton").gameObject
    local itemSprite = item.transform:GetComponent(typeof(UISprite))

    UIEventListener.Get(talkButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTalkButtonClick(index)
	end)

    itemSprite.spriteName =  index % 2 == 0 and Constants.NewOddBgSprite or Constants.NewEvenBgSprite
    local stateTextArr = {LocalString.GetString("[ffff00]已申请"), LocalString.GetString("[00ff60]已通过"), LocalString.GetString("[FF5050]关闭申请")}
    stateLabel.text =  "[ffffff]"
    if data.applyState > 0 then
        stateLabel.text = stateTextArr[data.applyState] 
    end
    guildNameLabel.text = data.guildName
    leaderLabel.text = data.leaderName
    local groupNameArray = {
        LocalString.GetString("甲级"),
        LocalString.GetString("乙级"),
        LocalString.GetString("丙级"),
        LocalString.GetString("丁级"),
        LocalString.GetString("戊级"),
        LocalString.GetString("己级"),
        LocalString.GetString("庚级"),
        LocalString.GetString("辛级"),
        LocalString.GetString("壬级"),
        LocalString.GetString("癸级"),
        
        LocalString.GetString("子级"),
        LocalString.GetString("丑级"),
        LocalString.GetString("寅级"),
        LocalString.GetString("卯级"),
        LocalString.GetString("辰级"),
        LocalString.GetString("巳级"),
        LocalString.GetString("午级"),
        LocalString.GetString("未级"),
        LocalString.GetString("申级"),
        LocalString.GetString("酉级"),
        LocalString.GetString("戌级"),
        LocalString.GetString("亥级")
    }
    rankLabel.text = (data.leagueRank == 0 or data.leagueLevel == 0) and "" or SafeStringFormat3(LocalString.GetString("%s第%d名"),groupNameArray[data.leagueLevel], data.leagueRank)
end

function LuaGuildExternalAidCardWnd:OnSelectAtRow(row)
    self.m_SelectRow = row
    local data = self.m_List[row + 1]
    local isGetCard = LuaGuildExternalAidMgr.m_CardInfo.guildId ~= 0
    local isJoining = data.applyState == 1
    self.YellowJoinButton.gameObject:SetActive(not isGetCard and not isJoining)
    self.BlueJoinButton.gameObject:SetActive(isGetCard)
    self.RevocatButton.gameObject:SetActive(isJoining)
    self.BlueJoinButton.Enabled = data.applyState == 0
end

function LuaGuildExternalAidCardWnd:OnTalkButtonClick(row)
    local data = self.m_List[row + 1]
	CChatHelper.ShowFriendWnd(data.leaderId, data.leaderName)
end
--@region UIEvent

function LuaGuildExternalAidCardWnd:OnBlueJoinButtonClick()
    if not self.m_SelectRow then
        g_MessageMgr:ShowMessage("GUILD_CHOOSE_ONE")
        return
    end
    local data = self.m_List[self.m_SelectRow + 1]
    local isGetCard = LuaGuildExternalAidMgr.m_CardInfo.guildId ~= 0
    if isGetCard then
        g_MessageMgr:ShowMessage("GuildExternalAidCardWnd_HasCard")
        return
    end
end

function LuaGuildExternalAidCardWnd:OnYellowJoinButtonClick()
    if not self.m_SelectRow then
        g_MessageMgr:ShowMessage("GUILD_CHOOSE_ONE")
        return
    end
    local data = self.m_List[self.m_SelectRow + 1]
    CInputBoxMgr.ShowInputBox(LocalString.GetString("请输入申请宣言"), DelegateFactory.Action_string(function (val)
        if not CWordFilterMgr.Inst:CheckLinkAndMatch(val) then
            g_MessageMgr:ShowMessage("Speech_Violation")		
            return
        end
        LuaGuildExternalAidMgr:GuildForeignAidApply(data.guildId, val)
    end), 24, true, nil, LocalString.GetString("点击此处输入（最多12个汉字）"))
end

function LuaGuildExternalAidCardWnd:OnMyInvitationButtonClick()
    self.MyInvitationAlertSprite.gameObject:SetActive(false)
    LuaGuildExternalAidMgr:RequestGuildForeignAidInviteInfo(LuaGuildExternalAidMgr.m_CardInfo.context)
end

function LuaGuildExternalAidCardWnd:OnTipButtonClick()
    local msgName = LuaGuildExternalAidMgr:GetAidCardTipMessageName(LuaGuildExternalAidMgr.m_CardInfo.context)
    g_MessageMgr:ShowMessage(msgName)
end

function LuaGuildExternalAidCardWnd:OnQuitButtonClick()
    local cardInfo = LuaGuildExternalAidMgr.m_CardInfo
    local msg = g_MessageMgr:FormatMessage("GuildForeignAidCancelApply_Confirm")
    MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
        LuaGuildExternalAidMgr:GuildForeignAidCancelApply(cardInfo.guildId)
    end),nil,nil,nil,false)
end


function LuaGuildExternalAidCardWnd:OnRevocatButtonClick()
    local data = self.m_List[self.m_SelectRow + 1]
    LuaGuildExternalAidMgr:GuildForeignAidCancelApply(data.guildId)
end


--@endregion UIEvent

function LuaGuildExternalAidCardWnd:OnEnable()
    g_ScriptEvent:AddListener("OnSendGuildForeignAidPersonalInfo", self, "OnSendGuildForeignAidPersonalInfo")
end

function LuaGuildExternalAidCardWnd:OnDisable()
    self:CancelUpdateTimeLabelTick()
    g_ScriptEvent:RemoveListener("OnSendGuildForeignAidPersonalInfo", self, "OnSendGuildForeignAidPersonalInfo")
end

function LuaGuildExternalAidCardWnd:OnSendGuildForeignAidPersonalInfo()
    local cardInfo = LuaGuildExternalAidMgr.m_CardInfo
    self.NoneCardLabel.text = LuaGuildExternalAidMgr:GetNoAidCardDesc(cardInfo.context)
    self.NoneCardView.gameObject:SetActive(cardInfo.guildId == 0)
    self.OwnCardView.gameObject:SetActive(cardInfo.guildId ~= 0)
    self.GuildNameLabel.text = cardInfo.guildName
    self.TypeLabel.text = cardInfo.aidType ~= EnumGuildForeignAidApplyInfoType.eElite and LocalString.GetString("普通外援") or LocalString.GetString("精英外援")
    self:UpdateTimeLabel()
    self.MyInvitationAlertSprite.gameObject:SetActive(cardInfo.hasInvite)
    local canFightTime = cardInfo.canFightTime
    local now = CServerTimeMgr.Inst.timeStamp
    local t = canFightTime - now
    if t > 0 then
        self:CancelUpdateTimeLabelTick()
        self.m_UpdateTimeTick = RegisterTick(function()
            self:UpdateTimeLabel()
        end,500)
    end
    self.m_List = LuaGuildExternalAidMgr.m_PersonalInfo
    table.sort(self.m_List,function(a,b) 
        if a.leagueLevel ~= b.leagueLevel then
            if a.leagueLevel == 0 then 
                return false 
            elseif b.leagueLevel == 0 then
                return true
            else
                return a.leagueLevel < b.leagueLevel
            end
        end
        return a.leagueRank < b.leagueRank
    end)
    self.TableView:ReloadData(true,true)
    self:InitButtons()
end

function LuaGuildExternalAidCardWnd:UpdateTimeLabel()
    local cardInfo = LuaGuildExternalAidMgr.m_CardInfo
    local canFightTime = cardInfo.canFightTime
    local now = CServerTimeMgr.Inst.timeStamp
    local t = canFightTime - now
    self.CoolingTimeLabel.text = t > 0 and SafeStringFormat3("%02d:%02d:%02d", math.floor(t / 3600), math.floor((t % 3600) / 60), t % 60) or LocalString.GetString("可参战")
end

function LuaGuildExternalAidCardWnd:CancelUpdateTimeLabelTick()
    if self.m_UpdateTimeTick then
        UnRegisterTick(self.m_UpdateTimeTick)
        self.m_UpdateTimeTick = nil
    end
end