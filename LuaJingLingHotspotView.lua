local CJingLingMgr = import "L10.Game.CJingLingMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CQuickAskImageItem = import "L10.UI.CQuickAskImageItem"
local CQuickAskItem = import "L10.UI.CQuickAskItem"
local CUICommonDef = import "L10.UI.CUICommonDef"
local DelegateFactory = import "DelegateFactory"
local Extensions = import "Extensions"
local Object = import "System.Object"
local System = import "System"
local UIEventListener = import "UIEventListener"
local UIScrollView = import "UIScrollView"
local UIGrid = import "UIGrid"
local UILabel = import "UILabel"
local EnumQueryJingLingContext = import "L10.Game.EnumQueryJingLingContext"
local UICamera = import "UICamera"
local CChatLinkMgr = import "CChatLinkMgr"
local LinkSource = import "CChatLinkMgr.LinkSource"

LuaJingLingHotspotView = class()

RegistClassMember(LuaJingLingHotspotView,"m_ContentRoot")
RegistClassMember(LuaJingLingHotspotView,"m_ScrollView")
RegistClassMember(LuaJingLingHotspotView,"m_Grid")
RegistClassMember(LuaJingLingHotspotView,"m_Banner")
RegistClassMember(LuaJingLingHotspotView,"m_ItemTemplate")
RegistClassMember(LuaJingLingHotspotView,"m_ImageItemTemplate")

RegistClassMember(LuaJingLingHotspotView,"m_ImageContentRoot")
RegistClassMember(LuaJingLingHotspotView,"m_ImageScrollView")
RegistClassMember(LuaJingLingHotspotView,"m_ImageContentLabel")

function LuaJingLingHotspotView:Awake()
    self.m_ContentRoot = self.transform:Find("Offset/Content").gameObject
    self.m_ScrollView = self.transform:Find("Offset/Content/ScrollView"):GetComponent(typeof(UIScrollView))
    self.m_Grid = self.transform:Find("Offset/Content/ScrollView/Grid"):GetComponent(typeof(UIGrid))
    self.m_Banner = self.transform:Find("Offset/Banner").gameObject
    self.m_ItemTemplate = self.transform:Find("Offset/Content/ScrollView/Item").gameObject
    self.m_ImageItemTemplate = self.transform:Find("Offset/Content/ScrollView/ImageItem").gameObject

    self.m_ImageContentRoot = self.transform:Find("Offset/ImageContent").gameObject
    self.m_ImageScrollView = self.transform:Find("Offset/ImageContent/ScrollView"):GetComponent(typeof(UIScrollView))
    self.m_ImageContentLabel = self.transform:Find("Offset/ImageContent/ScrollView/ImageContentLabel"):GetComponent(typeof(UILabel))

    self.m_ItemTemplate:SetActive(false)
    self.m_ImageItemTemplate:SetActive(false)
    self.m_ContentRoot:SetActive(false)
    self.m_ImageContentRoot:SetActive(false)
end

function LuaJingLingHotspotView:Init()
    --do nothing, replaced by execute in OnEnable
end

function LuaJingLingHotspotView:InitContent()
    self:Clear()
    CJingLingMgr.Inst:QueryHotspots()
end

function LuaJingLingHotspotView:Clear()
    self.m_Banner:SetActive(false)
    Extensions.RemoveAllChildren(self.m_Grid.transform)
end

function LuaJingLingHotspotView:LoadData(hotspots) 

    if System.String.IsNullOrEmpty(hotspots) then
        return
    end
    --热点分两种形式，pic|开头表示纯图模式，ban|开头或者无这两种开头则表示原来的热点模式，示例如下
    --[[
         pic|<img url=http://pic.k73.com/up/soft/2019/1206/093827_59096297.jpg w=822 h=548>
            <area shape=rect coords=400,100,64,128  action=showimage(http://pic.k73.com/up/soft/2019/1206/093827_59096297.jpg)>
            <area shape=rect coords=300,200,128,128  action=showimage(http://pic.k73.com/up/soft/2019/1206/093827_59096297.jpg)>
        </img>
        ban|hot这是什么|hot这是什么,img=http://pic16.nipic.com/20110918/455924_101006715159_2.jpg|sdk这是什么,img=http://pic16.nipic.com/20110918/455924_101006715159_2.jpg
    ]]
    if StringStartWith(hotspots, "pic|") then
        self.m_ContentRoot:SetActive(false)
        self.m_ImageContentRoot:SetActive(true)
        self:LoadImageContent(CommonDefs.StringSubstring1(hotspots, 4))
        return
    elseif StringStartWith(hotspots, "ban|") then
        hotspots = CommonDefs.StringSubstring1(hotspots, 4) --直接跳过，按原逻辑处理即可
    end
    self.m_ContentRoot:SetActive(true)
    self.m_ImageContentRoot:SetActive(false)
    local args = CommonDefs.StringSplit_ArrayChar_StringSplitOptions(hotspots, Table2ArrayWithCount({"|"}, 1, MakeArrayClass(String)), System.StringSplitOptions.RemoveEmptyEntries)
    Extensions.RemoveAllChildren(self.m_Grid.transform)
    for i=0,args.Length-1 do
        if StringStartWith(args[i], "sdk") then
            local tmp = "hot" .. CommonDefs.StringSubstring1(args[i], 3)
            local matchsdk = CJingLingMgr.s_HotInfoRegex:Match(tmp)
            if matchsdk.Success then
                self.m_Banner:SetActive(true)
                local text = ToStringWrap(matchsdk.Groups[2])
                local url = ToStringWrap(matchsdk.Groups[5])
                self.m_Banner:GetComponent(typeof(CQuickAskImageItem)):Init(text, false, url)
                UIEventListener.Get(self.m_Banner).onClick = DelegateFactory.VoidDelegate(function (obj) 
                    LuaJingLingMgr.OpenSDKJingLing()
                end)
            end
        else

            local match = CJingLingMgr.s_HotInfoRegex:Match(args[i])
            local isHot = false
            local imageUrl = nil
            local askText = args[i]
            if match.Success then
                isHot = (ToStringWrap(match.Groups[1]) == "hot")
                askText = ToStringWrap(match.Groups[2])
                imageUrl = ToStringWrap(match.Groups[5])
            end

            local sdkText = StringStartWith(askText, "<sdk>") and string.match(askText,"<sdk>(.+)</sdk>") or nil
            if sdkText~=nil then askText = sdkText end

            if not System.String.IsNullOrEmpty(imageUrl) then
                local item = CUICommonDef.AddChild(self.m_Grid.gameObject, self.m_ImageItemTemplate)
                local quickAskItem = CommonDefs.GetComponent_GameObject_Type(item, typeof(CQuickAskImageItem))
                item:SetActive(true)
                local text = askText
                quickAskItem:Init(text, isHot, imageUrl)
                if sdkText~=nil then
                    UIEventListener.Get(item).onClick = DelegateFactory.VoidDelegate(function (obj) 
                        LuaJingLingMgr.OpenSDKJingLing(askText)
                    end)
                else
                    UIEventListener.Get(item).onClick = DelegateFactory.VoidDelegate(function (obj)
                        self:OnImageItemClick(item)
                    end)
                end
            else
                local item = CUICommonDef.AddChild(self.m_Grid.gameObject, self.m_ItemTemplate)
                local quickAskItem = CommonDefs.GetComponent_GameObject_Type(item, typeof(CQuickAskItem))
                item:SetActive(true)
                local text = askText
                quickAskItem:Init(text, isHot, nil)
                if sdkText~=nil then
                    UIEventListener.Get(item).onClick = DelegateFactory.VoidDelegate(function (obj) 
                        LuaJingLingMgr.OpenSDKJingLing(askText)
                    end)
                else
                    UIEventListener.Get(item).onClick = DelegateFactory.VoidDelegate(function (obj)
                        self:OnItemClick(item)
                    end)
                end
            end
        end
    end

    if self.m_Banner.activeSelf then
        Extensions.SetLocalPositionY(self.m_ContentRoot.transform, 0)
    else
        Extensions.SetLocalPositionY(self.m_ContentRoot.transform, 60)
    end

    self.m_Grid:Reposition()
    self.m_ScrollView:ResetPosition()
end

function LuaJingLingHotspotView:LoadImageContent(hotspots)
    --和小精灵约定显示图片内容时不会再有sdk banner信息
    --图像显示宽度不得超过Label的宽度，不然会显示不出来
    self.m_ImageContentLabel.supportTabSymbol = true
    self.m_ImageContentLabel.supportImgSymbol = true
    self.m_ImageContentLabel:SetImgContext("imgsource", "jingling")
    self.m_ImageContentLabel:SetImgContext("subsource", "hotspot")
    self.m_ImageContentLabel:SetImgContext("keyword", "jingling_hotspot")
    self.m_ImageContentLabel.text = hotspots
    UIEventListener.Get(self.m_ImageContentLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function (obj)
        self:OnImageContentLabelClick(self.m_ImageContentLabel.gameObject)
    end)
    self.m_ImageScrollView:ResetPosition()
end

function LuaJingLingHotspotView:OnEnable()
    g_ScriptEvent:AddListener("OnQueryJingLingFinished", self, "OnQueryJingLingFinished")
    self:InitContent()
end

function LuaJingLingHotspotView:OnDisable()
    g_ScriptEvent:RemoveListener("OnQueryJingLingFinished", self, "OnQueryJingLingFinished")
end

function LuaJingLingHotspotView:OnQueryJingLingFinished(args)
    local context = args[0]
    local question = args[1]
    local answer = args[2]
    if context == EnumQueryJingLingContext.Hotspot then
        self:LoadData(answer)
    end
end

function LuaJingLingHotspotView:OnItemClick(go)
    local item = go:GetComponent(typeof(CQuickAskItem))
    if item ~= nil then
        g_ScriptEvent:BroadcastInLua("OnJingLingHotspotItemClick", item.Text)
    end 
end

function LuaJingLingHotspotView:OnImageItemClick(go)
    local item = go:GetComponent(typeof(CQuickAskImageItem))
    if item ~= nil then
        g_ScriptEvent:BroadcastInLua("OnJingLingHotspotItemClick", item.Text)
    end 
end

function LuaJingLingHotspotView:OnImageContentLabelClick(go)
    local url = self.m_ImageContentLabel.GetUrlAtPosition(UICamera.lastWorldPosition)
    local dict = CreateFromClass(MakeGenericClass(Dictionary, cs_string, Object))
    CommonDefs.DictAdd_LuaCall(dict, "keyword", "jingling_hotspot")
    CChatLinkMgr.ProcessLinkClick(url, LinkSource.JingLing, dict)
end
