local UIGrid = import "UIGrid"

local UITable = import "UITable"
local UILabel = import "UILabel"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local UILongPressButton = import "L10.UI.UILongPressButton"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CTooltipAlignType = import "L10.UI.CTooltip+AlignType"
local CItemAccessListMgr=import "L10.UI.CItemAccessListMgr"
LuaShuangshiyi2023LotteryMustGetWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaShuangshiyi2023LotteryMustGetWnd, "MustGetItem1", "MustGetItem1", GameObject)
RegistChildComponent(LuaShuangshiyi2023LotteryMustGetWnd, "ItemObj", "ItemObj", GameObject)
RegistChildComponent(LuaShuangshiyi2023LotteryMustGetWnd, "MustGetText", "MustGetText", UILabel)
RegistChildComponent(LuaShuangshiyi2023LotteryMustGetWnd, "MustGetConsumeText", "MustGetConsumeText", GameObject)
RegistChildComponent(LuaShuangshiyi2023LotteryMustGetWnd, "MustGetOwnText", "MustGetOwnText", GameObject)
RegistChildComponent(LuaShuangshiyi2023LotteryMustGetWnd, "MustGetBottomTips", "MustGetBottomTips", UILabel)
RegistChildComponent(LuaShuangshiyi2023LotteryMustGetWnd, "MustGetButton", "MustGetButton", GameObject)
RegistChildComponent(LuaShuangshiyi2023LotteryMustGetWnd, "RuleButton", "RuleButton", GameObject)
RegistChildComponent(LuaShuangshiyi2023LotteryMustGetWnd, "AddMustGetButton", "AddMustGetButton", GameObject)
RegistChildComponent(LuaShuangshiyi2023LotteryMustGetWnd, "Grid", "Grid", UIGrid)

--@endregion RegistChildComponent end

function LuaShuangshiyi2023LotteryMustGetWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end

    self:InitWndData()
    self:RefreshConstUI()
    self:RefreshVariableUI()
    self:InitUIEvent()
end

function LuaShuangshiyi2023LotteryMustGetWnd:Init()

end

--@region UIEvent

--@endregion UIEvent

function LuaShuangshiyi2023LotteryMustGetWnd:InitWndData()
    self.overviewItemData = {}
    self.maxQuality = 0
    local rawData = Double11_RechargeLottery.GetData().PrizePoolOneHundredItems
    local itemList = g_LuaUtil:StrSplit(rawData, ",")
    for i = 1, #itemList do
        if itemList[i] ~= "" then
            local cfgId = tonumber(itemList[i])
            local cfgData = Double11_RechargeLotteryItems.GetData(cfgId)
            local quality = LuaShuangshiyi2023Mgr:GetItemQuality(cfgData.ItemId)
            if quality > self.maxQuality then
                self.maxQuality = quality
            end
            if self.overviewItemData[quality] == nil then
                self.overviewItemData[quality] = {}
            end
            table.insert(self.overviewItemData[quality], cfgId)
        end
    end

    self.rechargeData = {}
    self.lotteryData = {}
    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer and CommonDefs.DictContains_LuaCall(mainPlayer.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eDouble11RechargeData) then
        local playDataU = CommonDefs.DictGetValue_LuaCall(mainPlayer.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eDouble11RechargeData)
        local list = g_MessagePack.unpack(playDataU.Data.Data)
        self.rechargeData = list
    end
    if mainPlayer and CommonDefs.DictContains_LuaCall(mainPlayer.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eDouble11LotteryData) then
        local playDataU = CommonDefs.DictGetValue_LuaCall(mainPlayer.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eDouble11LotteryData)
        local list = g_MessagePack.unpack(playDataU.Data.Data)
        self.lotteryData = list
    end

    self.curSelectItemList = {}

    self.isButtonOnPress = false
    self.isButtonOnPressing = false
    self.buttonpressStartTime = 0
    self.longPressThreshold = 0.2
    self.longPressItemId = nil
    self.longPressItemObj = nil
end

function LuaShuangshiyi2023LotteryMustGetWnd:OnClickItem(itemObj, itemId)
    local isNewSelect = false
    local clickSelectIndex = nil
    for i = 1, #self.curSelectItemList do
        if self.curSelectItemList[i] == itemId then
            isNewSelect = true
            clickSelectIndex = i
            break
        end
    end
    if isNewSelect then
        --点击取消
        table.remove(self.curSelectItemList, clickSelectIndex)
        itemObj.transform:Find("IsSelected").gameObject:SetActive(false)
        itemObj.transform:Find("CheckIconBg/CheckIcon").gameObject:SetActive(false)
        self:RefreshVariableUI()
    else
        --点击选中
        if #self.curSelectItemList >= 1 then
            g_MessageMgr:ShowMessage("Double11_2023Lottery_MustGet_On_Select_Over_One")
            return
        end

        --选中
        table.insert(self.curSelectItemList, itemId)
        itemObj.transform:Find("IsSelected").gameObject:SetActive(true)
        itemObj.transform:Find("CheckIconBg/CheckIcon").gameObject:SetActive(true)
        self:RefreshVariableUI()
    end
end

function LuaShuangshiyi2023LotteryMustGetWnd:RefreshConstUI()
    self.MustGetText.text = g_MessageMgr:FormatMessage("Double11_2023Lottery_MustGet_Tips")
    self.MustGetBottomTips.text = g_MessageMgr:FormatMessage("Double11_2023Lottery_MustGet_BottomTips")

    self.ItemObj:SetActive(false)
    --refresh itemList
    for quality = self.maxQuality, 1, -1 do
        local itemList = self.overviewItemData[quality] and self.overviewItemData[quality] or {}
        for i = 1, #itemList do
            local cfgId = itemList[i]
            local cfgData = Double11_RechargeLotteryItems.GetData(cfgId)
            local itemId = cfgData.ItemId
            local itemData = Item_Item.GetData(itemId)

            local templateItem = CommonDefs.Object_Instantiate(self.ItemObj)
            templateItem:SetActive(true)
            templateItem.transform.parent = self.Grid.transform

            templateItem.transform:Find("IsSelected").gameObject:SetActive(false)
            templateItem.transform:Find("CheckIconBg/CheckIcon").gameObject:SetActive(false)
            Extensions.SetLocalPositionZ(templateItem.transform:Find("IsSelected"), 0)
            Extensions.SetLocalPositionZ(templateItem.transform:Find("CheckIconBg/CheckIcon"), 0)

            local texture = templateItem.transform:Find("Icon"):GetComponent(typeof(CUITexture))
            if itemData then texture:LoadMaterial(itemData.Icon) end
            templateItem.transform:Find("Quality"):GetComponent(typeof(UISprite)).spriteName = CUICommonDef.GetItemCellBorder(itemData.NameColor)
            local isBind = (cfgData.Bind == 1) or (itemData.Lock == 1)
            templateItem.transform:Find("BindSprite").gameObject:SetActive(false)
            templateItem.transform:Find("AmountLabel"):GetComponent(typeof(UILabel)).text = cfgData.Amount > 1 and cfgData.Amount or ""

            UIEventListener.Get(templateItem).onPress = LuaUtils.BoolDelegate(function (go, isPressed)
                self.isButtonOnPress = isPressed
                if isPressed then
                    self.longPressItemId = cfgId
                    self.longpressItemObj = templateItem
                end
            end)
        end
    end
    self.Grid:Reposition()
end

function LuaShuangshiyi2023LotteryMustGetWnd:Update()
    if self.isButtonOnPressing then
        local timeinterval = CServerTimeMgr.Inst.timeStamp - self.buttonpressStartTime
        if timeinterval > self.longPressThreshold then
            self.isButtonOnPressing = false
            self.isButtonOnPress = false
            local itemId = Double11_RechargeLotteryItems.GetData(self.longPressItemId).ItemId
            CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
        else
            if not self.isButtonOnPress then
                self.isButtonOnPressing = false
                self.isButtonOnPress = false
                self:OnClickItem(self.longpressItemObj, self.longPressItemId)
            end
        end
    else
        if self.isButtonOnPress then
            self.isButtonOnPressing = true
            self.buttonpressStartTime = CServerTimeMgr.Inst.timeStamp
        end
    end
end

function LuaShuangshiyi2023LotteryMustGetWnd:RefreshVariableUI()
    --refresh replacement items
    if self.curSelectItemList[1] then
        self:InitOneItem(self.MustGetItem1, self.curSelectItemList[1])
    else
        local texture = self.MustGetItem1.transform:Find("Item"):GetComponent(typeof(UITexture))
        self.MustGetItem1.transform:Find("Border"):GetComponent(typeof(UISprite)).spriteName = ""
        self.MustGetItem1.transform:Find("BindSprite").gameObject:SetActive(false)
        self.MustGetItem1.transform:Find("AmountLabel"):GetComponent(typeof(UILabel)).text = ""
        texture.material = nil
        UIEventListener.Get(self.MustGetItem1).onClick = DelegateFactory.VoidDelegate(function (_)
            --cancel
        end)
    end

    --refresh replacement item number
    local remainMustGetTime = (self.rechargeData[LuaShuangshiyi2023Mgr.KeyTbl.eCurMustGetTimes] and self.rechargeData[LuaShuangshiyi2023Mgr.KeyTbl.eCurMustGetTimes] or 0)
    self.MustGetOwnText.transform:Find("NumberText"):GetComponent(typeof(UILabel)).text = remainMustGetTime
    self.MustGetConsumeText.transform:Find("NumberText"):GetComponent(typeof(UILabel)).text = #self.curSelectItemList
end

function LuaShuangshiyi2023LotteryMustGetWnd:InitUIEvent()
    UIEventListener.Get(self.AddMustGetButton).onClick = DelegateFactory.VoidDelegate(function (_)
        --没道具, 弹出获得渠道
        local itemId = 21021451
        CItemAccessListMgr.Inst:ShowItemAccessInfo(itemId, false, self.AddMustGetButton.transform, CTooltipAlignType.Right)
    end)
    UIEventListener.Get(self.RuleButton).onClick = DelegateFactory.VoidDelegate(function (_)
        g_MessageMgr:ShowMessage("Double11_2023Lottery_MustGet_Rule")
    end)
    UIEventListener.Get(self.MustGetButton).onClick = DelegateFactory.VoidDelegate(function (_)
        local forbidItemList = self.lotteryData[LuaShuangshiyi2023Mgr.KeyTbl.eReplacementItems] and self.lotteryData[LuaShuangshiyi2023Mgr.KeyTbl.eReplacementItems] or {}
        local forbidItemLength = #forbidItemList
        if forbidItemLength >= 1 then
            g_MessageMgr:ShowMessage("Double11_2023Lottery_Replacement_Select_Before_Tips")
            return
        end

        local mustGetItemId = self.lotteryData[LuaShuangshiyi2023Mgr.KeyTbl.eMustGetItem] and self.lotteryData[LuaShuangshiyi2023Mgr.KeyTbl.eMustGetItem] or 0
        if mustGetItemId ~= 0 then
            g_MessageMgr:ShowMessage("Double11_2023Lottery_MustGet_Full_Tips")
            return
        end
        
        local curSelectLength = #self.curSelectItemList
        if curSelectLength > 0 then
            CUIManager.CloseUI("Shuangshiyi2023LotteryMustGetWnd")
            Gac2Gas.RequestSet2023Double11MustGetItem(self.curSelectItemList[1])
        else
            g_MessageMgr:ShowMessage("Double11_2023Lottery_MustGet_No_Select_Click_Ensure_Button")
        end
    end)
end

function LuaShuangshiyi2023LotteryMustGetWnd:InitOneItem(curItem, cfgId)
    local texture = curItem.transform:Find("Item"):GetComponent(typeof(CUITexture))
    local cfgData = Double11_RechargeLotteryItems.GetData(cfgId)
    local itemID = cfgData.ItemId
    local ItemData = Item_Item.GetData(itemID)
    if ItemData then texture:LoadMaterial(ItemData.Icon) end
    curItem.transform:Find("Border"):GetComponent(typeof(UISprite)).spriteName = CUICommonDef.GetItemCellBorder(ItemData.NameColor)
    local isBind = (cfgData.Bind == 1) or (ItemData.Lock == 1)
    curItem.transform:Find("BindSprite").gameObject:SetActive(false)
    curItem.transform:Find("AmountLabel"):GetComponent(typeof(UILabel)).text = cfgData.Amount > 1 and cfgData.Amount or ""

    UIEventListener.Get(curItem).onClick = DelegateFactory.VoidDelegate(function (_)
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemID, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
    end)
end
