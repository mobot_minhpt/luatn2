local CommonDefs = import "L10.Game.CommonDefs"
local CMainCamera = import "L10.Engine.CMainCamera"
local Vector3 = import "UnityEngine.Vector3"
local UILabel = import "UILabel"

LuaSpecialHaiDiaoPlayPickUpWnd = class()

RegistClassMember(LuaSpecialHaiDiaoPlayPickUpWnd, "m_PickUpBtn")
RegistClassMember(LuaSpecialHaiDiaoPlayPickUpWnd, "m_TopAnchor")
RegistClassMember(LuaSpecialHaiDiaoPlayPickUpWnd, "m_CachedPos")
RegistClassMember(LuaSpecialHaiDiaoPlayPickUpWnd, "m_ViewPos")
RegistClassMember(LuaSpecialHaiDiaoPlayPickUpWnd, "m_ScreenPos")
RegistClassMember(LuaSpecialHaiDiaoPlayPickUpWnd, "m_TopWorldPos")

function LuaSpecialHaiDiaoPlayPickUpWnd:Awake()
    self.m_PickUpBtn = self.transform:Find("Anchor/RefineBtn/Texture/PickUpBtn").gameObject
    self.m_PickUpBtn.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LuaSeaFishingMgr.m_PickBtnText
    UIEventListener.Get(self.m_PickUpBtn).onClick = LuaSeaFishingMgr.m_PickUpWndDelegate
end

function LuaSpecialHaiDiaoPlayPickUpWnd:Init()
    self.m_TopAnchor = nil
    self.m_CachedPos = self.transform.position
    local x = math.floor(LuaSeaFishingMgr.m_CollectionPos.x)
    local z = math.floor(LuaSeaFishingMgr.m_CollectionPos.z)
    self.m_TopAnchor = LuaSeaFishingMgr.m_CollectionPos
    self.m_TopAnchor.y = self.m_TopAnchor.y + LuaSeaFishingMgr.m_TopAnchorY
    self:Update()
end

function LuaSpecialHaiDiaoPlayPickUpWnd:OnDisable()
    LuaSeaFishingMgr.m_CollectTargetEngineId = nil
end

function LuaSpecialHaiDiaoPlayPickUpWnd:Update()
    self.m_ViewPos = CMainCamera.Main:WorldToViewportPoint(self.m_TopAnchor)
    
    if self.m_ViewPos and self.m_ViewPos.z and self.m_ViewPos.z > 0 and 
            self.m_ViewPos.x > 0 and self.m_ViewPos.x < 1 and self.m_ViewPos.y > 0 and self.m_ViewPos.y < 1 then
        self.m_ScreenPos = CMainCamera.Main:WorldToScreenPoint(self.m_TopAnchor)
    else  
        self.m_ScreenPos = CommonDefs.op_Multiply_Vector3_Single(Vector3.up, 3000)
    end
    self.m_ScreenPos.z = 0
    self.m_TopWorldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(self.m_ScreenPos)

    if self.m_CachedPos.x ~= self.m_TopWorldPos.x or self.m_CachedPos.x ~= self.m_TopWorldPos.x then
        self.transform.position = self.m_TopWorldPos
        self.m_CachedPos = self.m_TopWorldPos
    end
end
