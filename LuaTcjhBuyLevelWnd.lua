--@region import

local GameObject                = import "UnityEngine.GameObject"
local CUIManager                = import "L10.UI.CUIManager"
local CCurentMoneyCtrl          = import "L10.UI.CCurentMoneyCtrl"
local QnButton                  = import "L10.UI.QnButton"
local QnAdvanceGridView         = import "L10.UI.QnAdvanceGridView"
local DefaultTableViewDataSource= import "L10.UI.DefaultTableViewDataSource"
local QnNewSlider               = import "L10.UI.QnNewSlider"
local CItemInfoMgr				= import "L10.UI.CItemInfoMgr"
local AlignType					= import "L10.UI.CItemInfoMgr+AlignType"
local DelegateFactory		    = import "DelegateFactory"
local UIEventListener		    = import "UIEventListener"
local UILabel                   = import "UILabel"
local LocalString               = import "LocalString"
local LuaGameObject             = import "LuaGameObject"
--@endregion

--说明
CLuaTcjhBuyLevelWnd = class()

--@region Regist

--RegistChildComponent
RegistChildComponent(CLuaTcjhBuyLevelWnd, "BuyBtn",		GameObject)
RegistChildComponent(CLuaTcjhBuyLevelWnd, "DelBtn",		GameObject)
RegistChildComponent(CLuaTcjhBuyLevelWnd, "AddBtn",		GameObject)
RegistChildComponent(CLuaTcjhBuyLevelWnd, "Slider2",    QnNewSlider)
RegistChildComponent(CLuaTcjhBuyLevelWnd, "TitleLabel",         UILabel)
RegistChildComponent(CLuaTcjhBuyLevelWnd, "Percentage",         UILabel)
RegistChildComponent(CLuaTcjhBuyLevelWnd, "Percentage2",        UILabel)
RegistChildComponent(CLuaTcjhBuyLevelWnd, "TableView",		    QnAdvanceGridView)
RegistChildComponent(CLuaTcjhBuyLevelWnd, "QnCostAndOwnMoney",	CCurentMoneyCtrl)

--RegistClassMember
RegistClassMember(CLuaTcjhBuyLevelWnd, "BuyLevel")
RegistClassMember(CLuaTcjhBuyLevelWnd, "IgnorModify")
RegistClassMember(CLuaTcjhBuyLevelWnd, "SelectedItem")
RegistClassMember(CLuaTcjhBuyLevelWnd, "BuyExp")
RegistClassMember(CLuaTcjhBuyLevelWnd, "BuyCost")

--@endregion

function CLuaTcjhBuyLevelWnd:Awake()
    self.BuyLevel = 1
    self.BuyExp = 0
    self.BuyCost = 0
    self.IgnorModify = false
    self.SelectedItem = nil
    UIEventListener.Get(self.BuyBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnBuyBtnClick()
    end)

    UIEventListener.Get(self.DelBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnModifyLevel(-1)
    end)

    UIEventListener.Get(self.AddBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnModifyLevel(1)
    end)

    self.Slider2.OnValueChanged = DelegateFactory.Action_float(function(value)
        if self.IgnorModify then 
            self.IgnorModify = false
            return 
        end
        local data = LuaHanJia2023Mgr.XKLData
        local buymax = data.CfgMaxLv - data.Lv
        self.BuyLevel = math.min(math.max(math.floor((data.CfgMaxLv-data.Lv)*value),1),buymax)
        self:InitBuyInfo()
    end)
end

function CLuaTcjhBuyLevelWnd:Init()
    local data = LuaHanJia2023Mgr.XKLData
    if data.Lv >= data.CfgMaxLv then
        self.TitleLabel.text = g_MessageMgr:FormatMessage("Tcjh_BuyLevelMax")
    else
        local lv = self.BuyLevel + data.Lv
        if lv <= data.CfgMaxLv then
            self:OnModifyLevel(0)
        end
    end
end

function CLuaTcjhBuyLevelWnd:OnModifyLevel(value)
    self.IgnorModify = true
    local data = LuaHanJia2023Mgr.XKLData
    local buymax = data.CfgMaxLv - data.Lv
    self.BuyLevel = math.min(math.max(self.BuyLevel + value,1),buymax)
    self.Slider2.m_Value = self.BuyLevel / buymax
    self:InitBuyInfo()
end

function CLuaTcjhBuyLevelWnd:AddItem(item,items,itemids)
    if item == nil then return end
    if itemids[item.ItemID] == nil then
        itemids[item.ItemID] = item.Count
        items[#items+1] = item
    else
        itemids[item.ItemID] = itemids[item.ItemID] + item.Count
    end
end

function CLuaTcjhBuyLevelWnd:InitBuyInfo()
    local data = LuaHanJia2023Mgr.XKLData
    local lv = self.BuyLevel + data.Lv
    local exp = 0
    local itemdatas = {}            --itemarray:    index   ->  item={ItemID,Count,IsBind}
    local itemids = {}              --itemdic:      itemid  ->  count
    local curtype = data.PassType

    local lvexp = self:GetLvExp(data.Lv)
    local curexp =  data.Exp - lvexp  --为当前等级下的经验值，不会超过当前等级所需经验的上上限

    for i = data.Lv+1,lv do         --从下一级开始
        local cfg = data.Cfg[i]     
        if cfg ~= nil then 
            exp = exp + cfg.NeedXianLu
            if cfg.NmlItems ~= nil then
                self:AddItem(cfg.NmlItems[1],itemdatas,itemids)
            end
            if curtype == 1 or curtype == 3 then
                if cfg.AdvItems ~= nil then
                    for j=1,#cfg.AdvItems do
                        self:AddItem(cfg.AdvItems[j],itemdatas,itemids)
                    end
                end
            end

            if curtype == 2 or curtype == 3 then
                if cfg.UltraItems ~= nil then
                    for j=1,#cfg.UltraItems do
                        self:AddItem(cfg.UltraItems[j],itemdatas,itemids)
                    end
                end
            end
        end
    end

    if exp > 0 then
        exp = exp - curexp
    end
    exp = math.ceil(exp / 5) * 5

    local setting = HanJia2023_XianKeLaiSetting.GetData()
    local cost = exp / setting.OneJadeAddValue
    
    self.Percentage.text = tostring(exp)
    self.Percentage2.text = SafeStringFormat3(LocalString.GetString("%s级"), tostring(lv))

    self.TitleLabel.text = g_MessageMgr:FormatMessage("Tcjh_BuyLevelTitle",lv)
    
    self.QnCostAndOwnMoney:SetType(4, 0, false)
    self.QnCostAndOwnMoney:SetCost(cost)

    self.BuyExp = exp
    self.BuyCost = cost

    local len = #itemdatas
    local initfunc = function(item,index)
        local data = itemdatas[index+1]
        local count = itemids[data.ItemID]
        self:FillItemCell(item,data,count)
    end
    self.TableView.m_DataSource = DefaultTableViewDataSource.CreateByCount(len,initfunc)
    self.TableView:Clear()
    self.TableView:ReloadData(true, true)

    local dbtn = self.DelBtn:GetComponent(typeof(QnButton))
    local abtn = self.AddBtn:GetComponent(typeof(QnButton))

    local buymax = data.CfgMaxLv - data.Lv
    dbtn.Enabled = self.BuyLevel > 1
    abtn.Enabled = self.BuyLevel < buymax
end

function CLuaTcjhBuyLevelWnd:FillItemCell(item,data,count)

    local cell = item.transform
    local icon = LuaGameObject.GetChildNoGC(cell,"IconTexture").cTexture
    local bindgo = LuaGameObject.GetChildNoGC(cell,"BindSprite").gameObject
    local countlb = LuaGameObject.GetChildNoGC(cell,"AmountLabel").label

    local itemid = data.ItemID
    local isBind = data.IsBind
    
    local itemcfg = Item_Item.GetData(itemid)

    if itemcfg then
        icon:LoadMaterial(itemcfg.Icon)
    end

    bindgo:SetActive(tonumber(isBind) == 1)

    if tonumber(count) <= 1 then
        countlb.text = ""
    else
        countlb.text = count
    end

    UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemid, false, nil, AlignType.Default, 0, 0, 0, 0) 
        if self.SelectedItem ~= go then
            if self.SelectedItem then
                self:SelectItem(self.SelectedItem,false)
            end
            self.SelectedItem = go
            self:SelectItem(self.SelectedItem,true)
        end
    end)

    item.gameObject:SetActive(true)
end

function CLuaTcjhBuyLevelWnd:SelectItem(itemgo,selected)
    local selectgo = LuaGameObject.GetChildNoGC(itemgo.transform,"SelectedSprite").gameObject
    selectgo:SetActive(selected)
end

function CLuaTcjhBuyLevelWnd:OnBuyBtnClick()
    local msg = g_MessageMgr:FormatMessage("BuyJingcui_Reconfirm",self.BuyCost,self.BuyExp)
    g_MessageMgr:ShowOkCancelMessage(msg,function()
        Gac2Gas.RequestBuyXianKeLaiLevel(self.BuyLevel)
        CUIManager.CloseUI(CLuaUIResources.TcjhBuyLevelWnd)
    end,nil,nil,nil,false)
end

function CLuaTcjhBuyLevelWnd:GetGuideGo(methodName)
    if methodName == "GetCloseButton" then
        return self.transform:Find("Wnd_Bg_Secondary_3/CloseButton").gameObject
    end
    return nil
end

function CLuaTcjhBuyLevelWnd:GetLvExp(lv)
    local res = 0
    local cfg = LuaHanJia2023Mgr.XKLData.Cfg
    local min = math.min(#cfg, lv)
    for i = 1, min do
        res  = res + cfg[i].NeedXianLu
    end
    return res
end