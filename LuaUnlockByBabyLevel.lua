require("common/common_include")

local MessageMgr = import "L10.Game.MessageMgr"

LuaUnlockByBabyLevel = class()

RegistClassMember(LuaUnlockByBabyLevel, "LevelRequire")
RegistChildComponent(LuaUnlockByBabyLevel, "Lock", GameObject)
RegistChildComponent(LuaUnlockByBabyLevel, "Unlock", GameObject)
RegistChildComponent(LuaUnlockByBabyLevel, "Mask", GameObject)


function LuaUnlockByBabyLevel:Awake()
	self:Init()
end

function LuaUnlockByBabyLevel:Init()
	self:InitClassMembers()
	self:InitValues()
end

function LuaUnlockByBabyLevel:InitClassMembers()
	self.gameObject:SetActive(true)
end

function LuaUnlockByBabyLevel:InitValues()
	
	self.LevelRequire = 11

	local onLockClicked = function (go)
		self:OnLockClicked(go)
	end
	CommonDefs.AddOnClickListener(self.Mask, DelegateFactory.Action_GameObject(onLockClicked), false)
end

function LuaUnlockByBabyLevel:UpdateLock(grade)
	if not self.Lock then
		self:Init()
	end
	
	self.Lock:SetActive(grade < self.LevelRequire)
	self.Unlock:SetActive(grade >= self.LevelRequire)
	self.Mask:SetActive(grade < self.LevelRequire)
end


function LuaUnlockByBabyLevel:OnLockClicked(go)
	MessageMgr.Inst:ShowMessage("BABY_NOT_OLD_ENOUGH_TO_SCHEDULE", {})
end

function LuaUnlockByBabyLevel:OnEnable()
	g_ScriptEvent:AddListener("BabyWndSelectBaby", self, "UpdateLock")
end

function LuaUnlockByBabyLevel:OnDisable()
	g_ScriptEvent:RemoveListener("BabyWndSelectBaby", self, "UpdateLock")
end

return LuaUnlockByBabyLevel