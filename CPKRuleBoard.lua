-- Auto Generated!!
local CChatLinkMgr = import "CChatLinkMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CPKRuleBoard = import "L10.UI.CPKRuleBoard"
local GameSetting_Client = import "L10.Game.GameSetting_Client"
local NameColor = import "L10.Game.NameColor"
CPKRuleBoard.m_UpdateText_CS2LuaHook = function (this) 
    local color = NameColor.Blue
    if CClientMainPlayer.Inst ~= nil then
        color = CClientMainPlayer.Inst.FightProp:GetPkNameColor()
    end

    local setting = GameSetting_Client.GetData()
    local descript = setting.PK_DESCRIPT

    local header = ""

    repeat
        local default = color
        if default == NameColor.Blue then
            header = setting.Descript_Pkvalue_blue
            break
        elseif default == NameColor.Gray then
            header = setting.Descript_Pkvalue_gray
            break
        elseif default == NameColor.HeavyGreen then
            header = setting.Descript_Pkvalue_darkgreen
            break
        elseif default == NameColor.LightGreen then
            header = setting.Descript_Pkvalue_green
            break
        elseif default == NameColor.Red then
            header = setting.Descript_Pkvalue_red
            break
        elseif default == NameColor.Yellow then
            header = setting.Descript_Pkvalue_yellow
            break
        end
    until 1

    if this.oldHeader ~= nil and (header == this.oldHeader) then
        return
    end
    this.oldHeader = header

    if descript ~= nil and header ~= nil then
        this.m_Board.text = CChatLinkMgr.TranslateToNGUIText(header, false) .. CChatLinkMgr.TranslateToNGUIText(descript, false)
    end
end
