require("common/common_include")

local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local CommonDefs = import "L10.Game.CommonDefs"
local AISkillType = import "L10.Game.AISkillType"
local CPropertySkill = import "L10.Game.CPropertySkill"

LuaPVPMgr = {}

LuaPVPMgr.m_PlayerAIOpen = true
LuaPVPMgr.m_ExclusiveSkillCls2YingLingStateTbl = nil
LuaPVPMgr.eYingLingRobotDefaultSkill = 179

function LuaPVPMgr.IsRobotOpen()
	if not CClientMainPlayer.Inst then return false end
	if not LuaPVPMgr.m_PlayerAIOpen then return false end

	local setting = MenPaiTiaoZhan_Setting.GetData()
	local mainPlayerCls = EnumToInt(CClientMainPlayer.Inst.Class)

	local classUseRobot = setting.ClassUseRobot
	for i = 0, classUseRobot.Length-1 do
		if mainPlayerCls == classUseRobot[i] then
			return true
		end
	end
	return false
end


function LuaPVPMgr.GetAutoAISkills(toChangeYingLingState)
	-- 选五个，从2开始
	local skills = {}
	local mainplayer = CClientMainPlayer.Inst
	if not mainplayer then return skills end

	local mainPlayerCls = EnumToInt(mainplayer.Class)
	MenPaiTiaoZhan_AutoAISkill.Foreach(function (key, value)
		local pro
		if mainplayer.IsYingLing and toChangeYingLingState then
			pro = SafeStringFormat3("Class_%s%s", tostring(mainPlayerCls), EnumToInt(toChangeYingLingState))
		else
			pro = SafeStringFormat3("Class_%s", tostring(mainPlayerCls))
		end
		if key > 1 and value[pro] ~= 0 then
			if mainplayer.SkillProp:IsOriginalSkillClsExist(value[pro]) then

				if not mainplayer.IsYingLing or LuaPVPMgr.IsValidSkillForYingLingState(value[pro], toChangeYingLingState) then
					table.insert(skills, value[pro])
				end
			end
		end
	end)
	return skills
end

function LuaPVPMgr.InitExclusiveTable()
	if LuaPVPMgr.m_ExclusiveSkillCls2YingLingStateTbl == nil then
		LuaPVPMgr.m_ExclusiveSkillCls2YingLingStateTbl = {}
		for i = EnumYingLingState.eMale, EnumYingLingState.eMonster do
			local data = Skill_YingLingSkill.GetData(i)
			for j=0,data.Skills.Length-1 do
				LuaPVPMgr.m_ExclusiveSkillCls2YingLingStateTbl[data.Skills[j]] = i
			end
		end
	end
end

--yinglingState is number type
-- 只检查互斥技能，共享技能暂未考虑
function LuaPVPMgr.IsValidSkillForYingLingState(skillCls, yinglingState)
	--初始化中间数据
	LuaPVPMgr.InitExclusiveTable()
	return not LuaPVPMgr.m_ExclusiveSkillCls2YingLingStateTbl[skillCls] or LuaPVPMgr.m_ExclusiveSkillCls2YingLingStateTbl[skillCls] == yinglingState
end
--yinglingState is number type
--return an C# array
function LuaPVPMgr.GetYingLingDefaultSkills(yinglingState)
	local ids = MenPaiTiaoZhan_Setting.GetData().YingLingMPTZDefaultSkillIds
	if yinglingState == EnumYingLingState.eMale then
		return ids[0]
	elseif yinglingState == EnumYingLingState.eFemale then
		return ids[1]
	elseif yinglingState == EnumYingLingState.eMonster then
		return ids[2]
	else
		return nil
	end
end

--根据SkillAI中定义的技能反推出这套技能对应的影灵形态
function LuaPVPMgr.GetCurrentYingLingStateBySkillAI( )

	LuaPVPMgr.InitExclusiveTable()

	if CClientMainPlayer.Inst == nil then return EnumYingLingState.eDefault end
	local tbl = {}
	local state = EnumYingLingState.eDefault
	local array = CClientMainPlayer.Inst.SkillProp.NewActiveSkillForAI
	for i=1,5 do -- 5个技能暂时写死
		local cls = array[i]

		local tmpState = LuaPVPMgr.m_ExclusiveSkillCls2YingLingStateTbl[cls]
		if tmpState then
			state = tmpState
			table.insert(tbl, tmpState)
		end
	end

	if #tbl>1 then

	end

	return state
end
function LuaPVPMgr.GetYingLingStateName(yinglingState)
	if yinglingState == EnumYingLingState.eMale then
		return LocalString.GetString("月鸣")
	elseif yinglingState == EnumYingLingState.eFemale then
		return LocalString.GetString("瑶羽")
	elseif yinglingState == EnumYingLingState.eMonster then
		return LocalString.GetString("阿乌")
	end
	return ""
end

function Gas2Gac.YingLingAISkillConflict(toChangeYingLingState)
    g_ScriptEvent:BroadcastInLua("OnChangeYingLingMenPaiSkillConflict", toChangeYingLingState)
end


LuaPVPMgr.m_CurrentYingLingState = nil
function LuaPVPMgr.GetCurrentYingLingState()
	if not LuaPVPMgr.IsRobotOpen() then
		return LuaPVPMgr.GetCurrentYingLingStateBySkillAI()
	end
	if not LuaPVPMgr.m_CurrentYingLingState then
		local state = LuaPVPMgr.GetCurrentYingLingStateBySkillAI()
		if state == EnumYingLingState.eDefault then
			local mainplayer = CClientMainPlayer.Inst
			if mainplayer then
				LuaPVPMgr.m_CurrentYingLingState = mainplayer.CurYingLingState
			end
		else
			LuaPVPMgr.m_CurrentYingLingState = state
		end
	end
	return LuaPVPMgr.m_CurrentYingLingState
end

function LuaPVPMgr.GetAISkillType()
	if not LuaPVPMgr.IsRobotOpen() then
		return AISkillType_lua.MPTZ
	end
	local mainplayer = CClientMainPlayer.Inst
	if not mainplayer then
		return  AISkillType_lua.MPTZ
	end
	if not mainplayer.IsYingLing then
		return  AISkillType_lua.MPTZ
	end

	local state = LuaPVPMgr.GetCurrentYingLingState()
	if state == EnumYingLingState.eFemale then
		return AISkillType_lua.MPTZ_YLFemale
	elseif state == EnumYingLingState.eMale then
		return AISkillType_lua.MPTZ_YLMale
	elseif state == EnumYingLingState.eMonster then
		return AISkillType_lua.MPTZ_YLMonster
	else 
		return AISkillType_lua.MPTZ
	end
end

function LuaPVPMgr.GetTianFuBaseIndex()
	if LuaPVPMgr.GetAISkillType() == AISkillType_lua.MPTZ then
		local aiTianFuType = CommonDefs.ConvertIntToEnum(typeof(AISkillType), AISkillType_lua.MPTZ)	
		local baseIdx = CClientMainPlayer.Inst.SkillProp:GetBaseIdxForAISkillType(aiTianFuType)
		return baseIdx
	else
		local aiTianFuType = CommonDefs.ConvertIntToEnum(typeof(AISkillType), AISkillType_lua.MPTZ_YLMale)	
		local baseIdx = CClientMainPlayer.Inst.SkillProp:GetBaseIdxForAISkillType(aiTianFuType)
		return baseIdx
	end
end


function Gas2Gac.UpdateActiveSkillForAI(type, skillUD, tianfuSkillUD)
	if not CClientMainPlayer.Inst then return end

	local aiSkillType = CommonDefs.ConvertIntToEnum(typeof(AISkillType), type)	
	local baseIdx = CClientMainPlayer.Inst.SkillProp:GetBaseIdxForAISkillType(aiSkillType)
	if baseIdx == 0 then return end

	local skillIds = MsgPackImpl.unpack(skillUD)
	if skillIds.Count ~= CPropertySkill.ActiveSkillCount then return end

    local tianfuBaseIdx = baseIdx
	if tianfuBaseIdx == 0 then return end

	local tianfuSkillIds = MsgPackImpl.unpack(tianfuSkillUD)
	if tianfuSkillIds.Count ~= CPropertySkill.TianFuSkillForAIGroupSize then return end

	local skillProp = CClientMainPlayer.Inst.SkillProp
	for i=0,CPropertySkill.ActiveSkillCount-1 do
		skillProp.NewActiveSkillForAI[baseIdx + i] = skillIds[i]
	end

	if LuaPVPMgr.IsRobotOpen() and CClientMainPlayer.Inst.IsYingLing then
		local aiTianFuType = CommonDefs.ConvertIntToEnum(typeof(AISkillType), AISkillType_lua.MPTZ_YLMale)	
		tianfuBaseIdx = CClientMainPlayer.Inst.SkillProp:GetBaseIdxForAISkillType(aiTianFuType)
	end
	for i = 0, CPropertySkill.TianFuSkillForAIGroupSize-1 do
		skillProp.TianFuSkillForAI[i+tianfuBaseIdx] = tianfuSkillIds[i]
	end

	skillProp.ActiveSkillForAIInited[type] = 1
	EventManager.BroadcastInternalForLua(EnumEventType.MainPlayerPVPSkillPropUpdate, {})
end
