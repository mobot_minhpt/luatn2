-- Auto Generated!!
local CPVPMgr = import "L10.Game.CPVPMgr"
local CPVPPromotionWnd = import "L10.UI.CPVPPromotionWnd"
local Extensions = import "Extensions"
CPVPPromotionWnd.m_Init_CS2LuaHook = function (this) 
    this.historyRankLabel.text = tostring(CPVPMgr.SelfPromotionInfo.oldHighestRank)
    this.currentRankLabel.text = tostring(CPVPMgr.SelfPromotionInfo.newHighestRank) .. "("
    this.changedRankLabel.text = tostring(math.abs(CPVPMgr.SelfPromotionInfo.change)) .. ")"
    this.currentRankLayoutTable:Reposition()

    --TODO 奖励
    this.awardLabel.gameObject:SetActive(false)
    Extensions.RemoveAllChildren(this.itemTable.transform)
end
