-- Auto Generated!!
local CButton = import "L10.UI.CButton"
local CJingLingAssistView = import "L10.UI.CJingLingAssistView"
local CJingLingMgr = import "L10.Game.CJingLingMgr"
local CLoginMgr = import "L10.Game.CLoginMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CQuickAskItem = import "L10.UI.CQuickAskItem"
local CUICommonDef = import "L10.UI.CUICommonDef"
local DelegateFactory = import "DelegateFactory"
local EnumQueryJingLingContext = import "L10.Game.EnumQueryJingLingContext"
local Extensions = import "Extensions"
local GameObject = import "UnityEngine.GameObject"
local Object = import "System.Object"
local System = import "System"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local LocalString = import "LocalString"

CJingLingAssistView.m_ShowBtnHighlight_CS2LuaHook = function (this, node) 
    CommonDefs.EnumerableIterate(this.assistBtnArray, DelegateFactory.Action_object(function (___value) 
        local _node = ___value
        if _node:Equals(node) then
            CommonDefs.GetComponent_GameObject_Type(_node, typeof(CButton)).Selected = true
        else
            CommonDefs.GetComponent_GameObject_Type(_node, typeof(CButton)).Selected = false
        end
    end))
end
CJingLingAssistView.m_ClickQuestionQueryBtn_CS2LuaHook = function (this, go)
    local config = PlayConfig_ServerIdEnabled.GetData(CLoginMgr.Inst.ServerGroupId)
    if config == nil then
        return
    end

    if config.Kefu == 1 then
      Gac2Gas.QuerySmartGMToken()
    else
        this:ShowBtnHighlight(go)
        this.QuestionQueryNode:SetActive(true)
        this.HotInfoNode:SetActive(false)
        CJingLingMgr.Inst:QueryServiceQuestion()
    end
end
CJingLingAssistView.m_ClickHotInfoBtn_CS2LuaHook = function (this, go)
    this:ShowBtnHighlight(go)
    this.QuestionQueryNode:SetActive(false)
    this.HotInfoNode:SetActive(true)
    CJingLingMgr.Inst:QueryServiceHotspots()
end
CJingLingAssistView.m_Init_CS2LuaHook = function (this)
    local config = PlayConfig_ServerIdEnabled.GetData(CLoginMgr.Inst.ServerGroupId)
    if config == nil then
        return
    end

    this.QuestionQueryNode:SetActive(false)
    this.HotInfoNode:SetActive(false)

    Gac2Gas.CheckIfMaintenanceZoneAvailable()

    this.assistBtnArray = Table2ArrayWithCount({this.QuestionQueryBtn, this.ContactAssistantBtn, this.HotInfoBtn, this.MaintenanceNoticeBtn}, 4, MakeArrayClass(GameObject))

    UIEventListener.Get(this.QuestionQueryBtn).onClick = MakeDelegateFromCSFunction(this.ClickQuestionQueryBtn, VoidDelegate, this)
    UIEventListener.Get(this.HotInfoBtn).onClick = MakeDelegateFromCSFunction(this.ClickHotInfoBtn, VoidDelegate, this)
    UIEventListener.Get(this.MaintenanceNoticeBtn).onClick = MakeDelegateFromCSFunction(this.ClickMaintenanceNoticeBtn, VoidDelegate, this)

    if config.Kefu == 1 then
        this.ContactAssistantBtn:SetActive(false)
        this.HotInfoBtn.transform.localPosition = Vector3(-609,358,0)
        this.QuestionQueryBtn.transform.localPosition = Vector3(-609,250,0)
        this.QuestionQueryBtn.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("客服中心")
        this.MaintenanceNoticeBtn.transform.localPosition = Vector3(-609,142,0)
        this.HotInfoBtn:SendMessage("OnClick")
    else
        this.ContactAssistantBtn:SetActive(true)
        UIEventListener.Get(this.ContactAssistantBtn).onClick = MakeDelegateFromCSFunction(this.ClickContactAssistantBtn, VoidDelegate, this)
        this.QuestionQueryBtn:SendMessage("OnClick")
    end
    --CJingLingMgr.Inst.QueryHotspots();
end
CJingLingAssistView.m_LoadData_CS2LuaHook = function (this, hotspots, context)
    if System.String.IsNullOrEmpty(hotspots) then
        return
    end

    this.saveContext = context

    local _scrollView = this.scrollView
    local _grid = this.grid
    local _itemTemplate = this.itemTemplate

    if context == EnumQueryJingLingContext.ServiceHotspot then
    elseif context == EnumQueryJingLingContext.ServiceQuestion then
        _scrollView = this.question_scrollView
        _grid = this.question_grid
        _itemTemplate = this.question_itemTemplate
    end

    local args = CommonDefs.StringSplit_ArrayChar_StringSplitOptions(hotspots, Table2ArrayWithCount({"|"}, 1, MakeArrayClass(String)), System.StringSplitOptions.RemoveEmptyEntries)
    Extensions.RemoveAllChildren(_grid.transform)
    do
        local i = 0
        while i < args.Length do
            local item = CUICommonDef.AddChild(_grid.gameObject, _itemTemplate)
            local quickAskItem = CommonDefs.GetComponent_GameObject_Type(item, typeof(CQuickAskItem))
            item:SetActive(true)
            local isHot = StringStartWith(args[i], "hot")
            local default
            if isHot then
                default = CommonDefs.StringSubstring1(args[i], 3)
            else
                default = args[i]
            end
            local text = default
            quickAskItem:Init(text, isHot, nil)
            UIEventListener.Get(item).onClick = MakeDelegateFromCSFunction(this.OnItemClick, VoidDelegate, this)
            i = i + 1
        end
    end
    _grid:Reposition()
    _scrollView:ResetPosition()
end
CJingLingAssistView.m_OnItemClick_CS2LuaHook = function (this, go)

    local item = CommonDefs.GetComponent_GameObject_Type(go, typeof(CQuickAskItem))
    if item ~= nil and this.OnItemClickDelegate ~= nil then
        GenericDelegateInvoke(this.OnItemClickDelegate, Table2ArrayWithCount({item.Text, this.saveContext ~= EnumQueryJingLingContext.ServiceQuestion}, 2, MakeArrayClass(Object)))
    end
end
