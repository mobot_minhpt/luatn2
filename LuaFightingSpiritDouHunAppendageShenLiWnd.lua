local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UIGrid = import "UIGrid"
local UILabel = import "UILabel"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local CFightingSpiritMgr=import "L10.Game.CFightingSpiritMgr"
local CShopMallMgr=import "L10.UI.CShopMallMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local String = import "System.String"
local Gac2Gas2 = import "L10.Game.Gac2Gas"
local Object = import "System.Object"
local MsgPackImpl = import "MsgPackImpl"

LuaFightingSpiritDouHunAppendageShenLiWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaFightingSpiritDouHunAppendageShenLiWnd, "OkBtn", "OkBtn", GameObject)
RegistChildComponent(LuaFightingSpiritDouHunAppendageShenLiWnd, "CancelBtn", "CancelBtn", GameObject)
RegistChildComponent(LuaFightingSpiritDouHunAppendageShenLiWnd, "ItemTemplate", "ItemTemplate", GameObject)
RegistChildComponent(LuaFightingSpiritDouHunAppendageShenLiWnd, "Grid", "Grid", UIGrid)
RegistChildComponent(LuaFightingSpiritDouHunAppendageShenLiWnd, "BottomLabel", "BottomLabel", UILabel)
RegistChildComponent(LuaFightingSpiritDouHunAppendageShenLiWnd, "CountLabel", "CountLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaFightingSpiritDouHunAppendageShenLiWnd, "m_Data")
RegistClassMember(LuaFightingSpiritDouHunAppendageShenLiWnd, "m_CurrentCount")
RegistClassMember(LuaFightingSpiritDouHunAppendageShenLiWnd, "m_MaxCount")
RegistClassMember(LuaFightingSpiritDouHunAppendageShenLiWnd, "m_AddSubBtnList")

function LuaFightingSpiritDouHunAppendageShenLiWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.OkBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOkBtnClick()
	end)


	
	UIEventListener.Get(self.CancelBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCancelBtnClick()
	end)


    --@endregion EventBind end
end

function LuaFightingSpiritDouHunAppendageShenLiWnd:Init()
	if LuaFightingSpiritDouHunAppendageMgr.m_ShenLiData then
		self.ItemTemplate:SetActive(false)
		self.m_Data = LuaFightingSpiritDouHunAppendageMgr.m_ShenLiData

		local level = self.m_Data.level
		local data = DouHunTan_ShenLiFuTi.GetData(level)

		self.BottomLabel.text = SafeStringFormat3(LocalString.GetString("本届神力附体等级为[00ff60]%s级[-]，若不使用，下届可叠加至[00ff60]%s级[-]"), self.m_Data.futiLv, math.min(self.m_Data.futiLv+1, 4))
		-- 最大点数
		self.m_MaxCount = data.TotalPoint * self.m_Data.futiLv

		self.m_CurrentCount = {}
		self.m_AddSubBtnList = {}
		for i=0,self.m_Data.current.Count-1 do
			table.insert(self.m_CurrentCount, self.m_Data.current[i])
		end

		local dataDesc = {LocalString.GetString("角色根骨"), LocalString.GetString("角色力量"), LocalString.GetString("角色智力"), 
			LocalString.GetString("角色敏捷"), LocalString.GetString("角色精力")}
		
		for i =1, 5 do
			local g = NGUITools.AddChild(self.Grid.gameObject, self.ItemTemplate)
			g:SetActive(true)
			self:InitItem(g, dataDesc[i], i)
		end
		self.Grid:Reposition()

		self:RefreshCount()
	else
		CUIManager.CloseUI(CLuaUIResources.FightingSpiritDouHunAppendageQiLiWnd)
	end
end


function LuaFightingSpiritDouHunAppendageShenLiWnd:InitItem(item, text, index)
	local descLabel = item.transform:Find("DesLabel"):GetComponent(typeof(UILabel))
	descLabel.text = text

	local addSubBtn = item.transform:Find("AddSubBtn"):GetComponent(typeof(QnAddSubAndInputButton))
	table.insert(self.m_AddSubBtnList, addSubBtn)

	addSubBtn:SetValue(self.m_CurrentCount[index] or 0)
	addSubBtn.onValueChanged = DelegateFactory.Action_uint(
        function(count)
			print(count)
			self:RefreshCount()
        end
    )
end

function LuaFightingSpiritDouHunAppendageShenLiWnd:RefreshCount()
	self.m_CurrentCount = {}
	-- 更新数据
	local sum = 0
	for i =1, 5 do
		local count = self.m_AddSubBtnList[i]:GetValue()
		sum = sum + count
		table.insert(self.m_CurrentCount, count)
	end

	-- 更新显示
	local left = self.m_MaxCount - sum
	self.CountLabel.text = left .. LocalString.GetString("点")
	for i =1, 5 do
		self.m_AddSubBtnList[i]:SetMinMax(0, math.min(math.min(self.m_CurrentCount[i] + left, 30*self.m_Data.futiLv), 99),1)
	end
end

--@region UIEvent

function LuaFightingSpiritDouHunAppendageShenLiWnd:OnOkBtnClick()
	local sum = 0
	for i =1, 5 do
		sum = sum + self.m_CurrentCount[i]
	end
	local left = self.m_MaxCount - sum

	if left > 0 then
		g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("DouHun_Futi_DianShu_Still_Can_Use"), function()
			local dataList = CreateFromClass(MakeGenericClass(List, Object))
			for i=1, 5 do
				local subList = CreateFromClass(MakeGenericClass(List, Object))
				CommonDefs.ListAdd(subList, typeof(Object), CFightingSpiritMgr.Instance:IndexToEnumDouHunTrainPointType(i-1))
				CommonDefs.ListAdd(subList, typeof(Object), self.m_CurrentCount[i])
				CommonDefs.ListAdd(dataList, typeof(Object), subList)
			end
		Gac2Gas2.SetDouHunTrainShenLiFuTiEffect(MsgPackImpl.pack(dataList))
		end, nil, nil, nil, false)
	else
		local dataList = CreateFromClass(MakeGenericClass(List, Object))
		for i=1, 5 do
			local subList = CreateFromClass(MakeGenericClass(List, Object))
			CommonDefs.ListAdd(subList, typeof(Object), CFightingSpiritMgr.Instance:IndexToEnumDouHunTrainPointType(i-1))
			CommonDefs.ListAdd(subList, typeof(Object), self.m_CurrentCount[i])
			CommonDefs.ListAdd(dataList, typeof(Object), subList)
		end
		Gac2Gas2.SetDouHunTrainShenLiFuTiEffect(MsgPackImpl.pack(dataList))
	end
end

function LuaFightingSpiritDouHunAppendageShenLiWnd:OnCancelBtnClick()
	CUIManager.CloseUI(CLuaUIResources.FightingSpiritDouHunAppendageQiLiWnd)
end

--@endregion UIEvent

