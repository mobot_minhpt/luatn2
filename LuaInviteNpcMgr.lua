local CTrackMgr = import "L10.Game.CTrackMgr"
LuaInviteNpcMgr = class()

LuaInviteNpcMgr.MaxFriendLinessLvel = 4
LuaInviteNpcMgr.MyInviteNpcState = nil
LuaInviteNpcMgr.IsSectLeader = false
LuaInviteNpcMgr.InCompetition = false
LuaInviteNpcMgr.s_InviteNPC_Switch = true
LuaInviteNpcMgr.CurrentQingLaiZhi = 0
LuaInviteNpcMgr.RemainExchangeIncreaseDaoyiValueItemCount = 0
function LuaInviteNpcMgr.ShowInviteNpcWnd()
    if not LuaInviteNpcMgr.s_InviteNPC_Switch then
        return
    end
    Gac2Gas.QueryAllSectNpcData()
end

LuaInviteNpcMgr.InviterList = {}
LuaInviteNpcMgr.MyInviterData = { daoyiValue = 0, daoYiAddonValue = 0}
LuaInviteNpcMgr.TrainWndType = 0
function LuaInviteNpcMgr.OpenNpcLeaveView()
    if not LuaInviteNpcMgr.s_InviteNPC_Switch then
        return
    end
    LuaInviteNpcMgr.TrainWndType = 0
    CUIManager.ShowUI(CLuaUIResources.TrainNpcFavorWnd)
end

function LuaInviteNpcMgr.OpenNpcInvitingView()
    if not LuaInviteNpcMgr.s_InviteNPC_Switch then
        return
    end
    LuaInviteNpcMgr.TrainWndType = 1
    CUIManager.ShowUI(CLuaUIResources.TrainNpcFavorWnd)
end

function LuaInviteNpcMgr.OpenNpcJoinedView()
    if not LuaInviteNpcMgr.s_InviteNPC_Switch then
        return
    end
    LuaInviteNpcMgr.TrainWndType = 2
    CUIManager.ShowUI(CLuaUIResources.TrainNpcFavorWnd)
end

function LuaInviteNpcMgr.ShowFreeNpcItemTip()
    LuaInviteNpcMgr.TipWndType = 0
    CUIManager.ShowUI(CLuaUIResources.NpcInvitedInfoWnd)
end

function LuaInviteNpcMgr.ShowJoinedNpcItemTip()
    LuaInviteNpcMgr.TipWndType = 1
    CUIManager.ShowUI(CLuaUIResources.NpcInvitedInfoWnd)
end

LuaInviteNpcMgr.m_EnchatId = 0
function LuaInviteNpcMgr:ShowNpcInvitedEnchantAbilityInfoWnd(id)
    self.m_EnchatId = id
    local data = SectInviteNpc_EnchantAbility.GetData(self.m_EnchatId)
    if not data then return end
    CUIManager.ShowUI(CLuaUIResources.NpcInvitedEnchantAbilityInfoWnd)
end

function LuaInviteNpcMgr.GetHotSpriteName(inviterCount)
    if inviterCount > 9 then
        inviterCount = 9
    end
    return "playershop_focus_"..inviterCount
end

function LuaInviteNpcMgr.GetLevelByfriendliness(value,grade)
    local formula = AllFormulas.Action_Formula[952].Formula
    local level = 4
    for i=1,4,1 do
        local qinglai = formula(nil, nil, {i,grade})
        if value < qinglai then
            level = i-1      
            break
        end
    end
    if level < 1 then level = 1 end
    return level
end

function LuaInviteNpcMgr.GetFriendLinessByLevel(level,grade)
    local formula = AllFormulas.Action_Formula[952].Formula
    local friendliness = formula(nil, nil, {level,grade})
    return friendliness
end

LuaInviteNpcMgr.m_SectInviteNpcFriendlinessTaskIds = nil
function LuaInviteNpcMgr.IsFriendlinessTask(taskId)
    local friendlinessTaskIds = SectInviteNpc_Setting.GetData().FriendlinessTaskIds
    local res = CommonDefs.HashSetContains(friendlinessTaskIds, typeof(UInt32), taskId)
    if res then return true end
    if not LuaInviteNpcMgr.m_SectInviteNpcFriendlinessTaskIds then
        LuaInviteNpcMgr.m_SectInviteNpcFriendlinessTaskIds = {}
        SectInviteNpc_LingShouRenQing.Foreach(function(k,v) 
            LuaInviteNpcMgr.m_SectInviteNpcFriendlinessTaskIds[v.TaskId] = true
        end)
    end
    return LuaInviteNpcMgr.m_SectInviteNpcFriendlinessTaskIds[taskId]
end

function LuaInviteNpcMgr.TrackToNpc(trackNpcId)
    local sectNpc = SectInviteNpc_NPC.GetData(trackNpcId)
    CTrackMgr.Inst:FindNPC(trackNpcId, sectNpc.PosInScene[0],sectNpc.PosInScene[1],sectNpc.PosInScene[2],nil,nil)
end

--玩皮灵兽
LuaInviteNpcMgr.WanpiLingShouTime = 30

--开宗立派成就
LuaInviteNpcMgr.NeedShowAchievement = true

--灵兽认亲玩法难度（服务器根据入驻npc生成)
LuaInviteNpcMgr.m_LingShouRenQinDifficultyLevel = 1