-- Auto Generated!!
local CDaDiZiMgr = import "L10.Game.CDaDiZiMgr"
local CDaDiZiStageNode = import "L10.UI.CDaDiZiStageNode"
local CommonDefs = import "L10.Game.CommonDefs"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CDaDiZiStageNode.m_Awake_CS2LuaHook = function (this) 
    this.line1.color = CommonDefs.ImplicitConvert_Color_Color32(CDaDiZiStageNode.normalCol)
    if this.line2 ~= nil then
        this.line2.color = CommonDefs.ImplicitConvert_Color_Color32(CDaDiZiStageNode.normalCol)
    end
    this.line3.color = CommonDefs.ImplicitConvert_Color_Color32(CDaDiZiStageNode.normalCol)
    if this.line4 ~= nil then
        this.line4.color = CommonDefs.ImplicitConvert_Color_Color32(CDaDiZiStageNode.normalCol)
    end
    this.battleSprite.enabled = false

    if this.winNameLabel ~= nil then
        this.winNameLabel.gameObject:SetActive(false)
    end
end
CDaDiZiStageNode.m_Init_CS2LuaHook = function (this, winId) 
    if this.winNameLabel ~= nil then
        this.winNameLabel.gameObject:SetActive(false)
        UIEventListener.Get(this.winNameLabel.gameObject).onClick = nil
    end

    if winId == - 1 then
        this.line1.color = CommonDefs.ImplicitConvert_Color_Color32(CDaDiZiStageNode.normalCol)
        if this.line2 ~= nil then
            this.line2.color = CommonDefs.ImplicitConvert_Color_Color32(CDaDiZiStageNode.normalCol)
        end
        this.line3.color = CommonDefs.ImplicitConvert_Color_Color32(CDaDiZiStageNode.normalCol)
        if this.line4 ~= nil then
            this.line4.color = CommonDefs.ImplicitConvert_Color_Color32(CDaDiZiStageNode.normalCol)
        end
        --
        if CDaDiZiMgr.Inst.stage ~= this.stage then
            this.battleSprite.enabled = false
        else
            this.battleSprite.enabled = true
        end

        if this.winLabel ~= nil then
            this.winLabel.text = ""
        end
    elseif winId == 0 then
        this.line1.color = CommonDefs.ImplicitConvert_Color_Color32(CDaDiZiStageNode.normalCol)
        if this.line2 ~= nil then
            this.line2.color = CommonDefs.ImplicitConvert_Color_Color32(CDaDiZiStageNode.normalCol)
        end
        this.line3.color = CommonDefs.ImplicitConvert_Color_Color32(CDaDiZiStageNode.normalCol)
        if this.line4 ~= nil then
            this.line4.color = CommonDefs.ImplicitConvert_Color_Color32(CDaDiZiStageNode.normalCol)
        end

        this.battleSprite.enabled = false

        if this.winLabel ~= nil then
            this.winLabel.text = ""
        end
    elseif winId == this.playerId1 then
        this.battleSprite.enabled = false
        this.line1.color = CommonDefs.ImplicitConvert_Color_Color32(CDaDiZiStageNode.activeCol)
        if this.line2 ~= nil then
            this.line2.color = CommonDefs.ImplicitConvert_Color_Color32(CDaDiZiStageNode.activeCol)
        end
        this.line3.color = CommonDefs.ImplicitConvert_Color_Color32(CDaDiZiStageNode.normalCol)
        if this.line4 ~= nil then
            this.line4.color = CommonDefs.ImplicitConvert_Color_Color32(CDaDiZiStageNode.normalCol)
        end
        if this.winLabel ~= nil then
            this.winLabel.text = tostring((CommonDefs.ListIndexOf(CDaDiZiMgr.Inst.stage1Data, this.playerId1) + 1))
        end

        this:InitWinName(winId)
    elseif winId == this.playerId2 then
        this.battleSprite.enabled = false
        this.line1.color = CommonDefs.ImplicitConvert_Color_Color32(CDaDiZiStageNode.normalCol)
        if this.line2 ~= nil then
            this.line2.color = CommonDefs.ImplicitConvert_Color_Color32(CDaDiZiStageNode.normalCol)
        end
        this.line3.color = CommonDefs.ImplicitConvert_Color_Color32(CDaDiZiStageNode.activeCol)
        if this.line4 ~= nil then
            this.line4.color = CommonDefs.ImplicitConvert_Color_Color32(CDaDiZiStageNode.activeCol)
        end

        if this.winLabel ~= nil then
            this.winLabel.text = tostring((CommonDefs.ListIndexOf(CDaDiZiMgr.Inst.stage1Data, this.playerId2) + 1))
        end
        this:InitWinName(winId)
    end
end
CDaDiZiStageNode.m_InitWinName_CS2LuaHook = function (this, winId) 
    if this.stage == 4 then
        if this.winNameLabel ~= nil then
            this.winNameLabel.gameObject:SetActive(true)
            local index = CommonDefs.ListIndexOf(CDaDiZiMgr.Inst.stage1Data, winId)
            this.winNameLabel.text = CDaDiZiMgr.Inst.nameList[index]
            this.mWinId = winId
            UIEventListener.Get(this.winNameLabel.gameObject).onClick = MakeDelegateFromCSFunction(this.OnClickWinName, VoidDelegate, this)
        end
    end
end
