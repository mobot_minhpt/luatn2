local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local UISprite = import "UISprite"
local CTopAndRightTipWnd=import "L10.UI.CTopAndRightTipWnd"

LuaSanxingTopRightWnd = class()
RegistChildComponent(LuaSanxingTopRightWnd,"rightBtn", GameObject)
RegistChildComponent(LuaSanxingTopRightWnd,"text2", UILabel)
RegistChildComponent(LuaSanxingTopRightWnd,"slider", UISprite)

RegistClassMember(LuaSanxingTopRightWnd, "maxWidth")

function LuaSanxingTopRightWnd:Close()
	--CUIManager.CloseUI(CLuaUIResources.)
end

function LuaSanxingTopRightWnd:OnEnable()
	g_ScriptEvent:AddListener("SanxingScoreUpdate", self, "InitData")
end

function LuaSanxingTopRightWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SanxingScoreUpdate", self, "InitData")
end

function LuaSanxingTopRightWnd:Init()
	UIEventListener.Get(self.rightBtn).onClick=LuaUtils.VoidDelegate(function(go)
		LuaUtils.SetLocalRotation(self.rightBtn.transform,0,0,0)
		CTopAndRightTipWnd.showPackage = false
		CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
	end)

	self.maxWidth = 269

	self.text2.text = ''
	self.slider.width = 0
	self:InitData()
end

function LuaSanxingTopRightWnd:InitData()
	if LuaSanxingGamePlayMgr.currentScore and LuaSanxingGamePlayMgr.totalScore then
		local percent = LuaSanxingGamePlayMgr.currentScore / LuaSanxingGamePlayMgr.totalScore
		if percent > 1 then
			percent = 1
		end
		self.text2.text = LuaSanxingGamePlayMgr.currentScore .. '/' .. LuaSanxingGamePlayMgr.totalScore
		self.slider.width = self.maxWidth * percent
	end
end

return LuaSanxingTopRightWnd
