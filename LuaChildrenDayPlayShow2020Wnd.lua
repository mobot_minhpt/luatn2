local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local CItemMgr = import "L10.Game.CItemMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"

LuaChildrenDayPlayShow2020Wnd = class()
RegistChildComponent(LuaChildrenDayPlayShow2020Wnd,"CloseButton", GameObject)
RegistChildComponent(LuaChildrenDayPlayShow2020Wnd,"node1", GameObject)
RegistChildComponent(LuaChildrenDayPlayShow2020Wnd,"node2", GameObject)
RegistChildComponent(LuaChildrenDayPlayShow2020Wnd,"node3", GameObject)
RegistChildComponent(LuaChildrenDayPlayShow2020Wnd,"restNumLabel", UILabel)
RegistChildComponent(LuaChildrenDayPlayShow2020Wnd,"inPipei", GameObject)
RegistChildComponent(LuaChildrenDayPlayShow2020Wnd,"showTextLabel", UILabel)
RegistChildComponent(LuaChildrenDayPlayShow2020Wnd,"tipBtn", GameObject)
RegistChildComponent(LuaChildrenDayPlayShow2020Wnd,"timeTip", UILabel)
RegistChildComponent(LuaChildrenDayPlayShow2020Wnd,"bonusLabel", UILabel)

--RegistClassMember(LuaChildrenDayPlayShow2020Wnd, "maxChooseNum")

function LuaChildrenDayPlayShow2020Wnd:Close()
	CUIManager.CloseUI(CLuaUIResources.ChildrenDayPlayShow2020Wnd)
end

function LuaChildrenDayPlayShow2020Wnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateChildrenDayPlayShow2020Info", self, "RefData")
end

function LuaChildrenDayPlayShow2020Wnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateChildrenDayPlayShow2020Info", self, "RefData")
end

function LuaChildrenDayPlayShow2020Wnd:RefNode(node,index)
  local attendBtn = node.transform:Find('btn').gameObject
  local cancelBtn = node.transform:Find('attendBtn').gameObject
  local bg1 = node.transform:Find('bg1').gameObject
  local bg2 = node.transform:Find('bg2').gameObject
  if LuaChildrenDay2020Mgr.gameSignUp then
    if LuaChildrenDay2020Mgr.gameSignUpType == index then
      attendBtn:SetActive(false)
			bg1:SetActive(false)
      cancelBtn:SetActive(true)
			bg2:SetActive(true)
    else
      attendBtn:SetActive(true)
			bg1:SetActive(true)
      cancelBtn:SetActive(false)
			bg2:SetActive(false)
    end
  else
		attendBtn:SetActive(true)
		bg1:SetActive(true)
		cancelBtn:SetActive(false)
		bg2:SetActive(false)
  end
end

function LuaChildrenDayPlayShow2020Wnd:RefData()
  self:RefNode(self.node2,2)
  self:RefNode(self.node3,1)

	local gameTypeStringTable = {LocalString.GetString('正在匹配正式比赛...'),LocalString.GetString('正在匹配练习赛...')}

  if LuaChildrenDay2020Mgr.gameSignUp then
    self.inPipei:SetActive(true)
		self.inPipei:GetComponent(typeof(UILabel)).text = gameTypeStringTable[LuaChildrenDay2020Mgr.gameSignUpType]
  else
    self.inPipei:SetActive(false)
  end

  --local curCount = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, LiuYi2020_Setting.GetData().DanZhuItemId)
	local curCount = LuaChildrenDay2020Mgr.gameTodayRestNum
  self.restNumLabel.text = curCount
end

function LuaChildrenDayPlayShow2020Wnd:InitNode(node,index)
  local attendBtn = node.transform:Find('btn').gameObject
  local cancelBtn = node.transform:Find('attendBtn').gameObject

	local onAttendClick = function(go)
		if not LuaChildrenDay2020Mgr.gameSignUp then
			Gac2Gas.DanZhuRequestSignUp(index)
		else
			g_MessageMgr:ShowMessage('CUSTOM_STRING2',LocalString.GetString('已在匹配中'))
		end
	end
	CommonDefs.AddOnClickListener(attendBtn,DelegateFactory.Action_GameObject(onAttendClick),false)
	local onCancelClick = function(go)
		if LuaChildrenDay2020Mgr.gameSignUp then
			Gac2Gas.DanZhuRequestCancelSignUp()
		else
			g_MessageMgr:ShowMessage('CUSTOM_STRING2',LocalString.GetString('未在匹配中'))
		end
	end
	CommonDefs.AddOnClickListener(cancelBtn,DelegateFactory.Action_GameObject(onCancelClick),false)
end

function LuaChildrenDayPlayShow2020Wnd:Init()
	local onCloseClick = function(go)
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.CloseButton,DelegateFactory.Action_GameObject(onCloseClick),false)
	local onTipClick = function(go)
    g_MessageMgr:ShowMessage('ChildrenDayGame2020Tip')
	end
	CommonDefs.AddOnClickListener(self.tipBtn,DelegateFactory.Action_GameObject(onTipClick),false)
	self.timeTip.text = g_MessageMgr:FormatMessage('ChildrenDay2020TimeTip')
	self.showTextLabel.text = g_MessageMgr:FormatMessage('ChildrenDayGameDesc')

	local bonusItemId = 21050843
	local itemData = CItemMgr.Inst:GetItemTemplate(bonusItemId)
	self.bonusLabel.text = '['..itemData.Name..']'

	local onBonusClick = function(go)
		CItemInfoMgr.ShowLinkItemTemplateInfo(bonusItemId, false, nil, AlignType.Default, 0, 0, 0, 0)
	end
	CommonDefs.AddOnClickListener(self.bonusLabel.gameObject,DelegateFactory.Action_GameObject(onBonusClick),false)

--  self:InitNode(self.node1,3)
  self:InitNode(self.node2,2)
  self:InitNode(self.node3,1)
  self:RefData()
end

return LuaChildrenDayPlayShow2020Wnd
