local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UIGrid = import "UIGrid"
local CButton = import "L10.UI.CButton"
local QnCheckBox = import "L10.UI.QnCheckBox"
local EnumClass = import "L10.Game.EnumClass"
local Profession = import "L10.Game.Profession"
local CUITexture = import "L10.UI.CUITexture"
LuaQMPKBattleBeforeSelectPlayerPanel = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaQMPKBattleBeforeSelectPlayerPanel, "bg", "bg", GameObject)
RegistChildComponent(LuaQMPKBattleBeforeSelectPlayerPanel, "SelectPlayerGrid", "SelectPlayerGrid", UIGrid)
RegistChildComponent(LuaQMPKBattleBeforeSelectPlayerPanel, "SelectPlayerItem", "SelectPlayerItem", GameObject)
RegistChildComponent(LuaQMPKBattleBeforeSelectPlayerPanel, "ConfirmButton", "ConfirmButton", CButton)

--@endregion RegistChildComponent end
RegistClassMember(LuaQMPKBattleBeforeSelectPlayerPanel, "m_MemberViewList")
RegistClassMember(LuaQMPKBattleBeforeSelectPlayerPanel, "m_IsBan")
RegistClassMember(LuaQMPKBattleBeforeSelectPlayerPanel, "m_Round")
RegistClassMember(LuaQMPKBattleBeforeSelectPlayerPanel, "m_MemberData")
RegistClassMember(LuaQMPKBattleBeforeSelectPlayerPanel, "m_Force")
RegistClassMember(LuaQMPKBattleBeforeSelectPlayerPanel, "m_SetForce")
RegistClassMember(LuaQMPKBattleBeforeSelectPlayerPanel, "m_CurSelectMemberNum")
RegistClassMember(LuaQMPKBattleBeforeSelectPlayerPanel, "m_round2SelectNum")
RegistClassMember(LuaQMPKBattleBeforeSelectPlayerPanel, "m_colorOrPathList")
function LuaQMPKBattleBeforeSelectPlayerPanel:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.ConfirmButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnConfirmButtonClick()
	end)


    --@endregion EventBind end
	self.m_MemberViewList = nil
	self.m_Round = 0
	self.m_CurSelectMemberNum = 0
	self.m_IsBan = false
	self.m_Force = nil
	self.m_SetForce = nil
	self.m_MemberData = nil
	self.m_colorOrPathList = nil
	self.SelectPlayerItem.gameObject:SetActive(false)
	self.m_round2SelectNum = {5,1,3,2,5}	-- 每局需要选的人数
end

function LuaQMPKBattleBeforeSelectPlayerPanel:ShowWnd(round,isBan,myForce,memberList)
	self.m_Round = round
	self.m_IsBan = isBan
	self.m_MemberData = memberList
	self.m_Force = myForce
	self:InitSelectPanelForceBg()
	self:InitSelectPanel()
end

function LuaQMPKBattleBeforeSelectPlayerPanel:InitSelectPanelForceBg()
	-- 背景和MemberTemplate的颜色
	self.m_SetForce = self.m_Force
	-- ban阶段显示对方的颜色
	if self.m_Round == 4 and self.m_IsBan then
		if self.m_Force == EnumCommonForce_lua.eAttack then self.m_SetForce = EnumCommonForce_lua.eDefend end
		if self.m_Force == EnumCommonForce_lua.eDefend then self.m_SetForce = EnumCommonForce_lua.eAttack end
	end
	local nameLabel = self.SelectPlayerItem.transform:Find("QnCheckBox/Label"):GetComponent(typeof(UILabel))

	self.m_colorOrPathList = {}

	self.m_colorOrPathList[EnumCommonForce_lua.eAttack] = {
		normalbgPath = "qmpkbattlebeforewnd_lan_weixuanzhong",
		highlightbgPath = "qmpkbattlebeforewnd_lan_xuanzhong",
		nameColor = "96a5bb",
	}
	self.m_colorOrPathList[EnumCommonForce_lua.eDefend] = {
		normalbgPath = "qmpkbattlebeforewnd_hong_weixuanzhong",
		highlightbgPath = "qmpkbattlebeforewnd_hong_xuanzhong",
		nameColor = "ffbdb6",
	}
	nameLabel.color = NGUIText.ParseColor24(self.m_colorOrPathList[self.m_SetForce].nameColor, 0)

end

function LuaQMPKBattleBeforeSelectPlayerPanel:InitSelectPanel()
	Extensions.RemoveAllChildren(self.SelectPlayerGrid.transform)
	self.m_MemberViewList = {}
	self.m_CurSelectMemberNum = 0
	for k,v in pairs(self.m_MemberData) do
		local member = CUICommonDef.AddChild(self.SelectPlayerGrid.gameObject,self.SelectPlayerItem.gameObject)
		member.gameObject:SetActive(true)
		local checkBox = member.transform:Find("QnCheckBox").gameObject
		local checkmark = checkBox.transform:Find("Background/Checkmark").gameObject
		local name = checkBox.transform:Find("Label"):GetComponent(typeof(UILabel))
		local classIcon = checkBox.transform:Find("classIcon"):GetComponent(typeof(UISprite))
		local highlight = checkBox.transform:Find("Highlight").gameObject
		local bg = member.transform:Find("QnCheckBox"):GetComponent(typeof(CUITexture))
		checkBox:GetComponent(typeof(CUITexture)):LoadMaterial(CLuaQMPKMgr.GetQmpkTexturePath(self.m_colorOrPathList[self.m_SetForce].normalbgPath))
		highlight:GetComponent(typeof(CUITexture)):LoadMaterial(CLuaQMPKMgr.GetQmpkTexturePath(self.m_colorOrPathList[self.m_SetForce].highlightbgPath))
		highlight.gameObject:SetActive(false)
		local Id = v.Id
		local Name = v.Name
		local Class = v.Class
		local Status = v.Status
		name.text = Name
		classIcon.spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), Class))
		self.m_MemberViewList[k] = {
			item = member,
			checkmark = checkmark,
			highlight = highlight,
			data = v,
			isSelect = false,
		}
		local isLockPick = Status == EnumQmpkSelectBanPickStatus.eRound4Pick or Status == EnumQmpkSelectBanPickStatus.eRound5Pick
		checkmark.gameObject:SetActive(isLockPick)
		highlight.gameObject:SetActive(isLockPick)
		if isLockPick then 
			Extensions.SetLocalPositionZ(checkmark.transform, -1)
			self.m_CurSelectMemberNum = self.m_CurSelectMemberNum  + 1
		else Extensions.SetLocalPositionZ(checkmark.transform, 0) end
		if Status == EnumQmpkSelectBanPickStatus.eCanSelect or isLockPick then
			Extensions.SetLocalPositionZ(member.transform, 0)
		else
			Extensions.SetLocalPositionZ(member.transform, -1)
		end
		UIEventListener.Get(checkBox.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
			self:OnMemberSelect(k)
		end)
	end
	self.SelectPlayerGrid:Reposition()
	self:UpdateOkBtn()
end

function LuaQMPKBattleBeforeSelectPlayerPanel:UpdateMemberSelelct(memberList,ban)
	-- if 1 then return end
	-- if not self.m_MemberViewList then return end
	-- local memberTbl = {}
	-- for k,v in pairs(memberList) do
	-- 	memberTbl[v.Id] = true
	-- end
	-- self.m_CurSelectMemberNum = 0
	-- if self.m_Round == 4 and self.m_IsBan then
	-- 	for k,v in pairs(self.m_MemberViewList) do
	-- 		if ban.Id == k then
	-- 			self.m_CurSelectMemberNum = self.m_CurSelectMemberNum  + 1
	-- 			v.checkmark.gameObject:SetActive(true)
	-- 			v.isSelect = true
	-- 			v.highlight.gameObject:SetActive(true)
	-- 		elseif v.data.Status == EnumQmpkSelectBanPickStatus.eCanSelect then
	-- 			v.checkmark.gameObject:SetActive(false)
	-- 			v.highlight.gameObject:SetActive(false)
	-- 			v.isSelect = false
	-- 		end
	-- 	end
	-- else
	-- 	for k,v in pairs(self.m_MemberViewList) do
	-- 		if v.data.Status == EnumQmpkSelectBanPickStatus.eCanSelect then
	-- 			if memberTbl[v.Id] then
	-- 				self.m_CurSelectMemberNum = self.m_CurSelectMemberNum  + 1
	-- 				v.checkmark.gameObject:SetActive(true)
	-- 				v.highlight.gameObject:SetActive(true)
	-- 				v.isSelect = true
	-- 			else
	-- 				v.checkmark.gameObject:SetActive(false)
	-- 				v.highlight.gameObject:SetActive(false)
	-- 				v.isSelect = false
	-- 			end
	-- 		end
	-- 	end
	-- end
	-- self:UpdateOkBtn()
end


function LuaQMPKBattleBeforeSelectPlayerPanel:OnMemberSelect(id)
	if not self.m_MemberViewList or not self.m_MemberViewList[id] then return end
	local itemdata = self.m_MemberViewList[id]
	if itemdata.data.Status == EnumQmpkSelectBanPickStatus.eCanSelect then
		local setSelect = not itemdata.isSelect
		if setSelect then
			if self.m_round2SelectNum[self.m_Round] == 1 then 	-- 只能选一人的情况
				self.m_CurSelectMemberNum = 0
				self:ClearSelectWithOutId(id)
			elseif self.m_Round == 4 then
				if self.m_IsBan then -- 禁选阶段
					self.m_CurSelectMemberNum = 0
					self:ClearSelectWithOutId(id)
				else	-- 选人阶段
					self.m_CurSelectMemberNum = 1
					self:ClearSelectWithOutId(id)
				end
			elseif self.m_CurSelectMemberNum >= self.m_round2SelectNum[self.m_Round] then
				g_MessageMgr:ShowMessage("QMPK_BanPick_PickMember_OutOfRange")
				return
			end
			self.m_CurSelectMemberNum = self.m_CurSelectMemberNum + 1
		else
			self.m_CurSelectMemberNum = self.m_CurSelectMemberNum - 1
		end
		itemdata.checkmark.gameObject:SetActive(setSelect)
		itemdata.isSelect = setSelect
		itemdata.highlight.gameObject:SetActive(setSelect)
		self:UpdateOkBtn()
		self:SyncSelectMemberInfo()
	else
		g_MessageMgr:ShowMessage("QMPK_BanPick_CannotSelectMember",itemdata.data.Name)
	end

end

function LuaQMPKBattleBeforeSelectPlayerPanel:ClearSelectWithOutId(id)
	if not self.m_MemberViewList then return end
	for k,v in pairs(self.m_MemberViewList) do
		if v.data.Status == EnumQmpkSelectBanPickStatus.eCanSelect and v.isSelect then  -- 其他选中的撤销
			if v.data.Id ~= id then
				v.checkmark.gameObject:SetActive(false)
				v.highlight.gameObject:SetActive(false)
				v.isSelect = false
			end
		end
	end
end

function LuaQMPKBattleBeforeSelectPlayerPanel:UpdateOkBtn()
	local btnText = self.ConfirmButton.transform:Find("Label"):GetComponent(typeof(UILabel))
	local curNum = self.m_CurSelectMemberNum
	if self.m_Round == 4 and self.m_IsBan then
		btnText.text = SafeStringFormat3(LocalString.GetString("禁选敌方角色(%d/1)"),curNum)
		self.ConfirmButton.Enabled = curNum >= 1
	else
		local maxNum = self.m_round2SelectNum[self.m_Round]
		btnText.text = SafeStringFormat3(LocalString.GetString("确认出战角色(%d/%d)"),curNum,maxNum)
		self.ConfirmButton.Enabled = curNum >= maxNum
	end
end

function LuaQMPKBattleBeforeSelectPlayerPanel:SyncSelectMemberInfo()
	if not self.m_MemberViewList then return end
	if self.m_Round == 4 and self.m_IsBan then
		local id = 0
		for k,v in pairs(self.m_MemberViewList) do
			if v.data.Status == EnumQmpkSelectBanPickStatus.eCanSelect and v.isSelect then 
				id = v.data.Id
			end
		end
		--print(self.m_Round,id)
		Gac2Gas.SetQmpkBiWuBanMemberId(self.m_Round,id)
		return
	end
	local membertable = {}
	for k,v in pairs(self.m_MemberViewList) do
		if v.data.Status == EnumQmpkSelectBanPickStatus.eCanSelect and v.isSelect then 
			table.insert(membertable,tostring(v.data.Id))
		end
	end
	local str = table.concat(membertable,",")
	--print(self.m_Round,str)
	Gac2Gas.SetQmpkBiWuPickMemberInfo(self.m_Round,str)
end

function LuaQMPKBattleBeforeSelectPlayerPanel:CloseWnd()

end
--@region UIEvent

function LuaQMPKBattleBeforeSelectPlayerPanel:OnConfirmButtonClick()
	if self.m_Round == 4 and self.m_IsBan then
		Gac2Gas.ConfirmQmpkBiWuBanResult(self.m_Round)
	else
		Gac2Gas.ConfirmQmpkBiWuPickResult(self.m_Round)
	end 
	
end

--@endregion UIEvent

function LuaQMPKBattleBeforeSelectPlayerPanel:OnEnable()
	g_ScriptEvent:AddListener("QMPKUpdateMemberSelelct",self,"UpdateMemberSelelct")
end

function LuaQMPKBattleBeforeSelectPlayerPanel:OnDisable()
	g_ScriptEvent:RemoveListener("QMPKUpdateMemberSelelct",self,"UpdateMemberSelelct")
end