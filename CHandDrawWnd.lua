-- Auto Generated!!
local CDrawLineMgr = import "L10.Game.CDrawLineMgr"
local CFadeEffect = import "L10.UI.CFadeEffect"
local CHandDrawMgr = import "L10.Game.CHandDrawMgr"
local CHandDrawWnd = import "L10.UI.CHandDrawWnd"
local Color = import "UnityEngine.Color"
local CommonDefs = import "L10.Game.CommonDefs"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local Texture2D = import "UnityEngine.Texture2D"
local TextureFormat = import "UnityEngine.TextureFormat"
local ZhuJueJuQing_HandDraw = import "L10.Game.ZhuJueJuQing_HandDraw"
CHandDrawWnd.m_Init_CS2LuaHook = function (this) 

    this.m_FinalPicture.gameObject:SetActive(false)

    this.m_DrawId = CHandDrawMgr.Inst.HandDrawID
    local handDraw = ZhuJueJuQing_HandDraw.GetData(this.m_DrawId)
    if handDraw == nil then
        this:Close()
        return
    end

    this.m_SimplePicture:LoadMaterial(("UI/Texture/Transparent/Material/" .. handDraw.Text) .. ".mat")
    this.m_FinalEffect:LoadMaterial(("UI/Texture/Transparent/Material/" .. handDraw.NextText) .. ".mat")
    CHandDrawWnd.FIT_PASS_PERCENT = handDraw.UntransparentOverlap
    CHandDrawWnd.UNFIT_PASS_PERCENT = handDraw.TransparentOverlap
    
    if System.String.IsNullOrEmpty(handDraw.BGText) then
        this.m_FinalEffectBG:Clear()
    else
        this.m_FinalEffectBG:LoadMaterial(("UI/Texture/Transparent/Material/" .. handDraw.BGText) .. ".mat")
    end
    this.m_FinalEffect.gameObject:SetActive(false)

    local hint1 = this.transform:Find("HuaZhi/HintLabel")
    local hint2 = this.transform:Find("HuaZhi/HintLabel2")

    if CommonDefs.IS_VN_CLIENT then
        hint1.gameObject:SetActive(false)
        hint2.gameObject:SetActive(true)
    end

    if handDraw.IsFlow == 2 then
        local pen1 = this.transform:Find("LineDrawTemplate/Panel/Anchor/PenTexture")
        local pen2 = this.transform:Find("LineDrawTemplate/Panel/Anchor/Pen2")
        pen1.gameObject:SetActive(false)
        pen2.gameObject:SetActive(true)

        local bg1 = this.transform:Find("HuaZhi/HuaZhiTextureBG")
        local bg2 = this.transform:Find("HuaZhi/HuaZhiTextureBG2")
        bg1.gameObject:SetActive(false)
        bg2.gameObject:SetActive(true)

        hint1.gameObject:SetActive(false)
        hint2.gameObject:SetActive(true)

        this.m_SimplePicture.uiTexture.width = 778
        for i=0,3 do
            local pos = this.m_Marks[i].transform.localPosition
            if i==0 or i==2 then
                pos.x = -778/2
            else
                pos.x = 778/2
            end
            this.m_Marks[i].transform.localPosition = pos
        end
    end

    if CHandDrawMgr.Inst.NeedDraw then
        CDrawLineMgr.Inst.LineColor = Color.grey
        CHandDrawWnd.CHECK_ALPHA = 0.5
        if handDraw.IsFlow == 2 then
            CDrawLineMgr.Inst.LineColor = Color(251/255,195/155,0,1)
            CHandDrawWnd.CHECK_ALPHA = 0.95
        end
        CDrawLineMgr.Inst.LineStartWidth = CHandDrawWnd.LINE_WIDTH
        CDrawLineMgr.Inst.LineEndWidth = CHandDrawWnd.LINE_WIDTH
        if handDraw.IsFlow == 0 then
            CDrawLineMgr.Inst.PenMaterialPath = "UI/Texture/Transparent/Material/housewnd_zhushabi_01.mat"
        else
            CDrawLineMgr.Inst.PenMaterialPath = nil
        end
        this.m_LineDrawer:Init()
        CDrawLineMgr.Inst:AllowDraw()
    else
        CDrawLineMgr.Inst:ForbidDraw()
        CDrawLineMgr.Inst.PenMaterialPath = nil
        this.m_LineDrawer:Init()
        this.m_DrawHint.gameObject:SetActive(false)
        this.m_FinishButton:SetActive(false)
    end
end
CHandDrawWnd.m_OnFinishButtonClicked_CS2LuaHook = function (this, go) 
    CDrawLineMgr.Inst:ForbidDraw()

    if this:IsDrawPassed() then
        -- 隐藏按钮
        this.m_SimplePicture.gameObject:SetActive(false)
        this.m_DrawHint.gameObject:SetActive(false)
        this.m_FinishButton:SetActive(false)

        this.m_LineDrawer:ClearAllLines()
        CFadeEffect.AliveDuration = 0.5
        CFadeEffect.IsDisappear = true
        this.m_FinalPicture.gameObject:SetActive(true)
        CommonDefs.GetComponent_Component_Type(this.m_FinalPicture, typeof(CFadeEffect)):Init()

        CFadeEffect.AliveDuration = 1.5
        CFadeEffect.IsDisappear = false
        this.m_FinalEffect.gameObject:SetActive(true)
        CommonDefs.GetComponent_Component_Type(this.m_FinalEffect, typeof(CFadeEffect)):Init()

        -- 显示特效
        this.m_AppearFX:LoadFx("fx/ui/prefab/UI_mapilinfuzhaohuan_baokai_huang.prefab")

        Gac2Gas.FinishHandDraw(CHandDrawMgr.Inst.HandDrawTaskID, CHandDrawMgr.Inst.HandDrawID)
    else
        -- todo 根据是否允许失败来处理
        local handDraw = ZhuJueJuQing_HandDraw.GetData(this.m_DrawId)
        if handDraw then
            if handDraw.EnableFail == 1 then
                -- 给服务器发一个回调
                -- 隐藏按钮
                this.m_FinalEffect:LoadMaterial(("UI/Texture/Transparent/Material/" .. handDraw.NextTextFail) .. ".mat")
                this.m_SimplePicture.gameObject:SetActive(false)
                this.m_DrawHint.gameObject:SetActive(false)
                this.m_FinishButton:SetActive(false)

                this.m_LineDrawer:ClearAllLines()
                CFadeEffect.AliveDuration = 0.5
                CFadeEffect.IsDisappear = true
                this.m_FinalPicture.gameObject:SetActive(true)
                CommonDefs.GetComponent_Component_Type(this.m_FinalPicture, typeof(CFadeEffect)):Init()

                CFadeEffect.AliveDuration = 1.5
                CFadeEffect.IsDisappear = false
                this.m_FinalEffect.gameObject:SetActive(true)
                CommonDefs.GetComponent_Component_Type(this.m_FinalEffect, typeof(CFadeEffect)):Init()

                -- 显示特效
                this.m_AppearFX:LoadFx("fx/ui/prefab/UI_mapilinfuzhaohuan_shibai.prefab")
                -- 一个失败的回调
                Gac2Gas.FailHandDraw(CHandDrawMgr.Inst.HandDrawTaskID, CHandDrawMgr.Inst.HandDrawID)
            else
                g_MessageMgr:ShowMessage("HAND_DRAW_NOT_MATCH")
                this.m_LineDrawer:ClearAllLines()
                CDrawLineMgr.Inst:AllowDraw()
            end
        end
    end
end
--[[CHandDrawWnd.m_IsDrawPassed_CS2LuaHook = function (this) 
    local drawTexture = this.m_LineDrawer:GetLineTexture()
    local cutOutTexture = this:GetCutOut(drawTexture)

    local checkTexture = TypeAs(this.m_SimplePicture.material:GetTexture("_AlphaTex"), typeof(Texture2D))
    local compareTexture = CHandDrawMgr.Inst:ScaleTexture(cutOutTexture, checkTexture.width, checkTexture.height)

    local m_TotalPixelCount = checkTexture.width * checkTexture.height

    local origin = checkTexture:GetPixels()
    local draws = compareTexture:GetPixels()

    -- 计算符合百分比
    local fitCheckTotal = 0
    local fitPassCounter = 0
    -- 计算不符合百分比
    local unfitCheckTotal = 0
    local unfitPassCounter = 0

    if origin.Length == draws.Length then
        do
            local i = 0
            while i < origin.Length do
                if origin[i].a ~= 0 then
                    fitCheckTotal = fitCheckTotal + 1
                    if draws[i].a ~= 0 then
                        fitPassCounter = fitPassCounter + 1
                    end
                else
                    unfitCheckTotal = unfitCheckTotal + 1
                    if draws[i].a == 0 then
                        unfitPassCounter = unfitPassCounter + 1
                    end
                end
                i = i + 1
            end
        end
    end
    this.m_FinalPicture.mainTexture = cutOutTexture
    Debug.Log(CommonDefs.String_Format_String_ArrayObject("{0}/{1}, {2}/{3}", {fitPassCounter, fitCheckTotal, unfitPassCounter, unfitCheckTotal}))
    if fitCheckTotal ~= 0 and (fitPassCounter * 1 / fitCheckTotal) >= CHandDrawWnd.FIT_PASS_PERCENT and unfitCheckTotal ~= 0 and (unfitPassCounter * 1 / unfitCheckTotal) >= CHandDrawWnd.UNFIT_PASS_PERCENT then
        return true
    end
    return false
end--]]
CHandDrawMgr.m_ShowHandDraw_CS2LuaHook = function (this, taskId, drawId) 
    this.HandDrawTaskID = taskId
    this.HandDrawID = drawId
    this.NeedDraw = true
    -- 处理是哪种绘画
    local draw = ZhuJueJuQing_HandDraw.GetData(drawId)
    if draw.IsFlow == 1 then
        CUIManager.ShowUI(CLuaUIResources.InkDrawWnd)
    elseif draw.IsFlow == 3 then
        CUIManager.ShowUI(CLuaUIResources.Halloween2023DiaoKeWnd)
    else
        CUIManager.ShowUI(CUIResources.HandDrawWnd)
    end
    
end
CHandDrawMgr.m_ScaleTexture_CS2LuaHook = function (this, source, targetWidth, targetHeight) 
    this.result = CreateFromClass(Texture2D, targetWidth, targetHeight, TextureFormat.ARGB32, false)

    local incX = (1 / targetWidth)
    local incY = (1 / targetHeight)

    do
        local i = 0
        while i < this.result.height do
            do
                local j = 0
                while j < this.result.width do
                    local newColor = source:GetPixelBilinear(j / this.result.width, i / this.result.height)
                    this.result:SetPixel(j, i, newColor)
                    j = j + 1
                end
            end
            i = i + 1
        end
    end
    this.result:Apply()
    this.result.name = "ScaleTexture"
    return this.result
end

