local CTrackMgr = import "L10.Game.CTrackMgr"
local CExpressionActionMgr = import "L10.Game.CExpressionActionMgr"

LuaZongMenJoiningWnd = class()

RegistChildComponent(LuaZongMenJoiningWnd,"m_MingGeConfirmSign","MingGeConfirmSign", UISprite)
RegistChildComponent(LuaZongMenJoiningWnd,"m_ZongMenConfirmSign","ZongMenConfirmSign", UISprite)
RegistChildComponent(LuaZongMenJoiningWnd,"m_RuMenBiaoZhunConfirmSign","RuMenBiaoZhunConfirmSign", UISprite)
RegistChildComponent(LuaZongMenJoiningWnd,"m_LeaveButton","LeaveButton", GameObject)
RegistChildComponent(LuaZongMenJoiningWnd,"m_JianDingButton","JianDingButton", GameObject)
RegistChildComponent(LuaZongMenJoiningWnd,"m_Grid","Grid", UIGrid)
RegistChildComponent(LuaZongMenJoiningWnd,"m_LabelTemplate","LabelTemplate", GameObject)
RegistChildComponent(LuaZongMenJoiningWnd,"m_SuccesSprite","SuccesSprite", GameObject)
RegistChildComponent(LuaZongMenJoiningWnd,"m_FailedSprite","FailedSprite", GameObject)
RegistChildComponent(LuaZongMenJoiningWnd,"m_RuMenBiaoZhunLabel","RuMenBiaoZhunLabel", UILabel)
RegistChildComponent(LuaZongMenJoiningWnd,"m_SuccesLabel","SuccesLabel", UILabel)
RegistChildComponent(LuaZongMenJoiningWnd,"m_FailedLabel","FailedLabel", UILabel)
RegistChildComponent(LuaZongMenJoiningWnd,"m_TitleLabel","TitleLabel", UILabel)
RegistChildComponent(LuaZongMenJoiningWnd,"m_DoExpressionButton","DoExpressionButton", GameObject)

RegistClassMember(LuaZongMenJoiningWnd, "m_ShowTick")
RegistClassMember(LuaZongMenJoiningWnd, "m_CheckMingGeResult")
RegistClassMember(LuaZongMenJoiningWnd, "m_CheckZongMenResult")
RegistClassMember(LuaZongMenJoiningWnd, "m_CheckRuMenBiaoZhunResult")
RegistClassMember(LuaZongMenJoiningWnd, "m_AllResult")
RegistClassMember(LuaZongMenJoiningWnd, "m_SectName")

function LuaZongMenJoiningWnd:Init()
    self.m_LabelTemplate:SetActive(false)
    self.m_SuccesSprite:SetActive(false)
    self.m_FailedSprite:SetActive(false)
    self.m_MingGeConfirmSign.gameObject:SetActive(false)
    self.m_ZongMenConfirmSign.gameObject:SetActive(false)
    self.m_RuMenBiaoZhunConfirmSign.gameObject:SetActive(false)
    self.m_DoExpressionButton.gameObject:SetActive(false)
    self.m_LeaveButton:SetActive(false)
    if LuaZongMenMgr.m_IsOpen then
        Gac2Gas.GetSectXinWuInfo()
    end
    UIEventListener.Get(self.m_LeaveButton).onClick = DelegateFactory.VoidDelegate(function (p)
        self:OnLeaveBtnClicked()
    end)
    UIEventListener.Get(self.m_JianDingButton).onClick = DelegateFactory.VoidDelegate(function (p)
        self:OnJianDingButtonClicked()
    end)
    UIEventListener.Get(self.m_DoExpressionButton).onClick = DelegateFactory.VoidDelegate(function (p)
        self:OnDoExpressionButtonClicked()
    end)
end

function LuaZongMenJoiningWnd:OnDoExpressionButtonClicked()
    CUIManager.CloseUI(CLuaUIResources.ZongMenJoiningWnd)
    local t = LuaZongMenMgr.m_ReplyJoinSectRes
    local expressionId = t.expressionId
    local fuXiDanceMotionId = t.motionId
    if fuXiDanceMotionId ~= 0 then
        local fuxiDatas = LuaFuxiAniMgr.GetAvaliableFuxiExpressionData()
        if fuxiDatas then
            for __, fuxiData in pairs(fuxiDatas) do
                if fuxiData.motionId == fuXiDanceMotionId then
                    Gac2Gas.MarkClientCareExpression(47000714,fuxiData.motionId)
                    Gac2Gas.RequestPlayFuXiDance(fuxiData.motionId)
                    return
                end
            end
        end
        LuaFuxiAniMgr.OpenFromShare(fuXiDanceMotionId)
    elseif expressionId ~= 0 then
        local regularDatas = CExpressionActionMgr.Inst:GetAvaliableExpressionActionInfos()
        if regularDatas then
            for i=0,regularDatas.Count-1 do
                local info = regularDatas[i]
                if info:GetID() == expressionId then
                    Gac2Gas.MarkClientCareExpression(expressionId,0)
                    Gac2Gas.RequestExpressionAction(expressionId)
                    return
                end
            end
        end
        CExpressionActionMgr.ShowThumbnailExpressionsInNeed(expressionId)
    end
end

function LuaZongMenJoiningWnd:OnJianDingButtonClicked()
    local npcTemplateId = tonumber(Menpai_Setting.GetData("YuanYiXianShengID").Value) 
    local list = g_LuaUtil:StrSplit(Menpai_Setting.GetData("YuanYiXianShengXunLu").Value, ",")
    local sceneTemplateId, posX, posY = tonumber(list[1]),tonumber(list[2]),tonumber(list[3])
    CTrackMgr.Inst:FindNPC(npcTemplateId, sceneTemplateId, posX, posY, nil, nil)
end

function LuaZongMenJoiningWnd:OnLeaveBtnClicked()
    if CClientMainPlayer.Inst then
        local zongMenName = self.m_SectName and self.m_SectName or "XX"
        local msg = g_MessageMgr:FormatMessage("LeaveZongMen_Confirm",zongMenName)
        MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function ()
            Gac2Gas.RequestQuitSect(CClientMainPlayer.Inst.BasicProp.SectId)
        end), nil, nil, nil, false)
        
    end
end

function LuaZongMenJoiningWnd:OnEnable()
    g_ScriptEvent:AddListener("OnSyncSectName", self, "OnSyncSectName")
    local t = LuaZongMenMgr.m_ReplyJoinSectRes
    self.m_SectName = LuaZongMenMgr.m_SectId2Name[t.newSectId]
    if not self.m_SectName then
        Gac2Gas.QuerySectName(t.newSectId)
    end
    self.m_SuccesLabel.text = t.isCreateZongMen and LocalString.GetString("开始创立") or LocalString.GetString("加入成功")
    self.m_FailedLabel.text = t.isCreateZongMen and LocalString.GetString("无法创立") or LocalString.GetString("加入失败")
    self.m_TitleLabel.text = t.isCreateZongMen and LocalString.GetString("创建宗派") or LocalString.GetString("申请加入")
    self:OnZongMenJoiningCheckResult(t.newSectId, t.mingGe, t.oldSectId, t.stdListUd, t.invalidListUd, t.checkList)
end

function LuaZongMenJoiningWnd:OnDisable()
    self:CancelShowTick()
    g_ScriptEvent:RemoveListener("OnSyncSectName", self, "OnSyncSectName")
end

function LuaZongMenJoiningWnd:OnSyncSectName(sectId, sectName)
    local t = LuaZongMenMgr.m_ReplyJoinSectRes
    if t.newSectId == sectId then
        self.m_SectName = sectName
        local zongMenName = self.m_SectName and self.m_SectName or "XX"
        self.m_RuMenBiaoZhunLabel.text = SafeStringFormat3(LocalString.GetString("符合【%s】入派标准"),zongMenName )
    end
end

function LuaZongMenJoiningWnd:CancelShowTick()
    if self.m_ShowTick then
        UnRegisterTick(self.m_ShowTick)
        self.m_ShowTick = nil
    end
end

function LuaZongMenJoiningWnd:OnZongMenJoiningCheckResult(newSectId, mingGe, oldSectId, stdListUd, invalidListUd, checkList)
    self.m_AllResult = true
    self.m_CheckRuMenBiaoZhunResult = {}
    local tempdict = {}
    local list = LuaZongMenMgr.m_RuMenGuiZe
    if checkList then
        list = MsgPackImpl.unpack(stdListUd)
    end
    if list then
        for j = 0, list.Count - 1 do
            local id = list[j]
            tempdict[id] = j + 1
            table.insert(self.m_CheckRuMenBiaoZhunResult, true)
        end
    end
    if checkList then
        local invalidList = MsgPackImpl.unpack(invalidListUd)   
        if invalidList then
            for j = 0, invalidList.Count - 1 do
                local id = invalidList[j]
                local index = tempdict[id]
                self.m_CheckRuMenBiaoZhunResult[index] = false
            end
        end
    end
    self.m_CheckMingGeResult = mingGe ~= 0
    self.m_JianDingButton:SetActive(not self.m_CheckMingGeResult)
    self.m_CheckZongMenResult = oldSectId == 0
    local zongMenName = self.m_SectName and self.m_SectName or LocalString.GetString("入派标准")
    self.m_RuMenBiaoZhunLabel.text =  newSectId == -1 and LocalString.GetString("符合入派标准") or SafeStringFormat3(LocalString.GetString("符合【%s】入派标准"),zongMenName )
    self:CancelShowTick()
    self.m_ShowTick = RegisterTickOnce(function ()
        self.m_AllResult = self.m_AllResult and self.m_CheckMingGeResult
        self.m_MingGeConfirmSign.spriteName = self.m_CheckMingGeResult and "common_log_01" or "common_log_03"
        self.m_MingGeConfirmSign.gameObject:SetActive(true)
        self:CancelShowTick()
        self.m_ShowTick = RegisterTickOnce(function ()
            self.m_AllResult = self.m_AllResult and self.m_CheckZongMenResult
            self.m_ZongMenConfirmSign.spriteName = self.m_CheckZongMenResult and "common_log_01" or "common_log_03"
            self.m_ZongMenConfirmSign.gameObject:SetActive(true)
            self.m_LeaveButton:SetActive(not self.m_CheckZongMenResult)
            self:CancelShowTick()
            self.m_ShowTick = RegisterTickOnce(function ()
                Extensions.RemoveAllChildren(self.m_Grid.transform)
                local res = true
                for i =1, #self.m_CheckRuMenBiaoZhunResult do
                    local go = NGUITools.AddChild(self.m_Grid.gameObject, self.m_LabelTemplate)
                    local label = go:GetComponent(typeof(UILabel))
                    res = res and self.m_CheckRuMenBiaoZhunResult[i]
                    label.color = self.m_CheckRuMenBiaoZhunResult[i] and Color.white or Color.red
                    if list.Count >= i then
                        local id = list[i - 1]
                        if id == tonumber(Menpai_Setting.GetData("CheckExpressionJoinStandard").Value) then
                            self.m_DoExpressionButton.gameObject:SetActive(true)
                        end
                        local data = Menpai_JoinStandard.GetData(id)
                        label.text = data and data.Description or "---"
                    end
                    go:SetActive(true)
                end 
                self.m_Grid:Reposition()
                self.m_RuMenBiaoZhunConfirmSign.spriteName = res and "common_log_01" or "common_log_03"
                self.m_RuMenBiaoZhunConfirmSign.gameObject:SetActive(true)
                self.m_AllResult = self.m_AllResult and res
                self.m_SuccesSprite:SetActive(self.m_AllResult)
                self.m_FailedSprite:SetActive(not self.m_AllResult)
                zongMenName = self.m_SectName and self.m_SectName or ""
                if newSectId ~= -1 then
                    g_MessageMgr:ShowMessage(self.m_AllResult and "Join_ZongMen_Success" or "Join_ZongMen_Failed", zongMenName)
                end
                self:CancelShowTick()
                if self.m_AllResult and newSectId == -1 then
                    local t = LuaZongMenMgr.m_ReplyJoinSectRes
                    if t.isCreateZongMen then
                        self.m_ShowTick = RegisterTickOnce(function ()
                            CUIManager.CloseUI(CLuaUIResources.ZongMenJoiningWnd)
                            CUIManager.ShowUI(CLuaUIResources.ZongMenCreationWnd)
                        end,1000)
                    end
                end
            end,1000)
        end,1000)
    end,1000)
end
