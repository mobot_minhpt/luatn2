-- Auto Generated!!
local CGuildPreLeagueInfoItem = import "L10.UI.CGuildPreLeagueInfoItem"
local CGuildPreLeagueInfoWnd = import "L10.UI.CGuildPreLeagueInfoWnd"
local CGuildPreLeagueTaskPointMgr = import "L10.UI.CGuildPreLeagueTaskPointMgr"
CGuildPreLeagueInfoWnd.m_ItemAt_CS2LuaHook = function (this, view, row) 
    if row < CGuildPreLeagueTaskPointMgr.Inst.infoList.Count then
        local cmp = TypeAs(this.tableView:GetFromPool(0), typeof(CGuildPreLeagueInfoItem))
        cmp:Init(CGuildPreLeagueTaskPointMgr.Inst.infoList[row], row)
        return cmp
    end
    return nil
end
