local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CTaskMgr = import "L10.Game.CTaskMgr"
local CScene = import "L10.Game.CScene"
local CGamePlayMgr = import "L10.Game.CGamePlayMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Quaternion = import "UnityEngine.Quaternion"
local Utility = import "L10.Engine.Utility"
local CMainCamera = import "L10.Engine.CMainCamera"

LuaShiTuCompassTaskView = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaShiTuCompassTaskView, "m_TaskName", "TaskName", UILabel)
RegistChildComponent(LuaShiTuCompassTaskView, "m_CountDownLabel", "CountDownLabel", UILabel)
RegistChildComponent(LuaShiTuCompassTaskView, "m_LeaveBtn", "LeaveBtn", GameObject)
RegistChildComponent(LuaShiTuCompassTaskView, "m_RuleBtn", "RuleBtn", GameObject)
RegistChildComponent(LuaShiTuCompassTaskView, "m_DescLab", "DescLab", UILabel)
RegistChildComponent(LuaShiTuCompassTaskView, "m_Compass", "Compass", GameObject)
RegistChildComponent(LuaShiTuCompassTaskView, "m_Needle", "Needle", GameObject)

RegistClassMember(LuaShiTuCompassTaskView, "m_UpdateNeedleTick")
--@endregion RegistChildComponent end

function LuaShiTuCompassTaskView:Awake()
    UIEventListener.Get(self.m_LeaveBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLeaveBtnClick(go)
	end)

    UIEventListener.Get(self.m_RuleBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRuleBtnClick(go)
	end)
end

function LuaShiTuCompassTaskView:Init()
    self:UpdateTaskInfo()
end

function LuaShiTuCompassTaskView:UpdateTaskInfo()
    local task = Task_Task.GetData(ShiTuTask_ZhaoMiJiSetting.GetData().TaskId)
    local info = LuaShiTuMgr.m_ZhaoMiJiMonsterInfo
    local curCount = info.curCount and info.curCount or 0
    local needCount = info.needCount and info.needCount or 0
    self.m_TaskName.text = task.Display
    self.m_DescLab.text = SafeStringFormat3(LocalString.GetString("%s\n追回（%d/%d）本"), g_MessageMgr:FormatMessage("ShiTu_ZhaoMiJi_Description"), curCount, needCount)
end

function LuaShiTuCompassTaskView:UpdateCompassNeedle()
    local monsterPosX = LuaShiTuMgr.m_ZhaoMiJiMonsterPos.x
    local monsterPosY = LuaShiTuMgr.m_ZhaoMiJiMonsterPos.y

    if monsterPosX and monsterPosY and CClientMainPlayer.Inst then

        local gridPos = Utility.PixelPos2GridPos(CClientMainPlayer.Inst.Pos)
        local playerPosX = gridPos.x
        local playerPosY = gridPos.y
        local ori = Vector3(0,1,0)
        local dir = Vector3(monsterPosX-playerPosX, monsterPosY-playerPosY,0)
        self.m_Needle.transform.localRotation = Quaternion.Euler(0, 0, self:Angle360(ori, dir))
        --罗盘根据摄像机视角调整，北方指向摄像机正前方
        local cameraForward = CMainCamera.Main.transform.forward
        cameraForward.y = 0
        local mapDir = Vector3(0,0,1)
        self.m_Compass.transform.localRotation = Quaternion.Euler(0, 0, -self:Angle360(mapDir, cameraForward))
    end
end

function LuaShiTuCompassTaskView:Angle360(from, to)
    local crossProduct = Vector3.Cross(from, to)
    return (crossProduct.z>0) and Vector3.Angle(from, to) or 360-Vector3.Angle(from, to)
end

function LuaShiTuCompassTaskView:StartUpdateNeedleTick()
    self:StopUpdateNeedleTick()
    self.m_UpdateNeedleTick = RegisterTick(function()
        self:UpdateCompassNeedle()
    end, 1000)
end

function LuaShiTuCompassTaskView:StopUpdateNeedleTick()
    if self.m_UpdateNeedleTick then
        UnRegisterTick(self.m_UpdateNeedleTick)
        self.m_UpdateNeedleTick = nil
    end
end

function LuaShiTuCompassTaskView:OnLeaveBtnClick(go)
    CGamePlayMgr.Inst:LeavePlay()
end

function LuaShiTuCompassTaskView:OnRuleBtnClick(go)
    g_MessageMgr:ShowMessage("ShiTu_Zhao_Mi_Ji_Rule")
end


function LuaShiTuCompassTaskView:OnEnable()
    self:StartUpdateNeedleTick()
    g_ScriptEvent:AddListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
    g_ScriptEvent:AddListener("QuerySmallShiTuCompass", self, "OnQuerySmallShiTuCompass")
    g_ScriptEvent:AddListener("SyncShiTuZhaoMiJiScore", self, "OnSyncShiTuZhaoMiJiScore")
    g_ScriptEvent:AddListener("SyncShiTuZhaoMiJiMonsterPos", self, "OnSyncShiTuZhaoMiJiMonsterPos")
end

function LuaShiTuCompassTaskView:OnDisable()
    self:StopUpdateNeedleTick()
    g_ScriptEvent:RemoveListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
    g_ScriptEvent:RemoveListener("QuerySmallShiTuCompass", self, "OnQuerySmallShiTuCompass")
    g_ScriptEvent:RemoveListener("SyncShiTuZhaoMiJiScore", self, "OnSyncShiTuZhaoMiJiScore")
    g_ScriptEvent:RemoveListener("SyncShiTuZhaoMiJiMonsterPos", self, "OnSyncShiTuZhaoMiJiMonsterPos")
end

function LuaShiTuCompassTaskView:OnSceneRemainTimeUpdate(args)
    if CScene.MainScene then
        if CScene.MainScene.ShowTimeCountDown then
            self.m_CountDownLabel.text = SafeStringFormat3(LocalString.GetString("剩余 [c][00ff00]%s[-][/c]"), LuaGamePlayMgr:GetRemainTimeText(CScene.MainScene.ShowTime))
        else
            self.m_CountDownLabel.text = nil
        end
    else
        self.m_CountDownLabel.text = nil
    end
end

function LuaShiTuCompassTaskView:OnQuerySmallShiTuCompass()
    g_ScriptEvent:BroadcastInLua("OnQuerySmallShiTuCompassResult", self.m_Compass.transform.position)
end

function LuaShiTuCompassTaskView:OnSyncShiTuZhaoMiJiScore()
    self:UpdateTaskInfo()
end

function LuaShiTuCompassTaskView:OnSyncShiTuZhaoMiJiMonsterPos()
    self:UpdateCompassNeedle()
end



