local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local UILabel = import "UILabel"
local CChatLinkMgr = import "CChatLinkMgr"
local Extensions = import "Extensions"
local NGUIText = import "NGUIText"

LuaQTEWnd = class()
RegistClassMember(LuaQTEWnd, "CurEnableQTEWnd")
RegistClassMember(LuaQTEWnd, "QTEWndName")
RegistClassMember(LuaQTEWnd, "MessageLabel")

function LuaQTEWnd:Awake()
    self.QTEWndName = {
        [EnumQTEType.Shake] = "ShakeQTE",
        [EnumQTEType.SingleClick] = "SingleClickQTE",
        [EnumQTEType.ConsecutiveClick] = "ConsecutiveClickQTE",
        [EnumQTEType.SequentialClick] = "SequentialQTE",
        [EnumQTEType.Slide] = "SlideQTE",
    }

    for _,v in pairs(self.QTEWndName) do
        local go = self.transform:Find(v).gameObject
        go:SetActive(true) -- 保证每一种QTE窗口的Awake执行一次
        go:SetActive(false)
    end

    local go = self.transform:Find("QTEFinishFxHelperWnd").gameObject
    go:SetActive(true)
    
    self.MessageLabel = self.transform:Find("Panel/Anchor/MessageLabel"):GetComponent(typeof(UILabel))
    self.MessageLabel.gameObject:SetActive(false)
end

function LuaQTEWnd:GetQTEWndGO(QTEType)
    return self.transform:Find(self.QTEWndName[QTEType]).gameObject
end

function LuaQTEWnd:GetQTEWndLuaScript(QTEType)
    return self.transform:Find(self.QTEWndName[QTEType]):GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
end

function LuaQTEWnd:InitOneWnd(QTEType, message)
    self.CurEnableQTEWnd = QTEType
    self:GetQTEWndGO(QTEType):SetActive(true)
    self:GetQTEWndLuaScript(QTEType):Init()
    self:InitMessage(message)
end

function LuaQTEWnd:OnEnable()
    g_ScriptEvent:AddListener("OnQTESubWndClose", self, "OnQTESubWndClose")
end

function LuaQTEWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnQTESubWndClose", self, "OnQTESubWndClose")
end

function LuaQTEWnd:OnQTESubWndClose()
    self.MessageLabel.gameObject:SetActive(false)
end

function LuaQTEWnd:InitMessage(message)
    if CUIManager.IsLoaded(CLuaUIResources.ScrollTaskWnd) then
        self.MessageLabel.gameObject:SetActive(false)
        g_ScriptEvent:BroadcastInLua("QTEWndInitMessage", message)
        return
    end
    if message==nil or message=="" then
        self.MessageLabel.gameObject:SetActive(false)
        return
    else
        self.MessageLabel.gameObject:SetActive(true)
    end
    self.MessageLabel.text = SafeStringFormat3("[c][%s]%s[-][/c]", 
        NGUIText.EncodeColor24(self.MessageLabel.color), CChatLinkMgr.TranslateToNGUIText(message, false))
    self.MessageLabel:UpdateNGUIText();
    NGUIText.regionWidth = 1000000
    local size = NGUIText.CalculatePrintedSize(self.MessageLabel.text)
    if size.x<1200 then
        self.MessageLabel.width = math.ceil(size.x)
    else
        self.MessageLabel.width = 1200
    end
    self.MessageLabel.transform:Find("Bg"):GetComponent(typeof(UIWidget)):ResetAndUpdateAnchors()
    Extensions.SetLocalPositionX(self.MessageLabel.transform, -self.MessageLabel.localSize.x * 0.5)
end

function LuaQTEWnd:MakeCurQTETimeExceed()
    self:GetQTEWndLuaScript(self.CurEnableQTEWnd):OnTimeLimitExceeded()
end

function LuaQTEWnd:ResetAllQTEWnd()
    for k,_ in pairs(self.QTEWndName) do
        if self:GetQTEWndGO(k).active then
            self:GetQTEWndLuaScript(k):Reset()
        end
    end
end

function LuaQTEWnd:TriggerFinishFx(success, position)
    local go = self.transform:Find("QTEFinishFxHelperWnd").gameObject
    local obj = go:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
    if success then
        obj:TriggerSucFx(position)
    else
        obj:TriggerFailFx(position)
    end
end

function LuaQTEWnd:OnDestroy()
end
