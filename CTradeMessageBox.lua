-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CTradeMessageBox = import "L10.UI.CTradeMessageBox"
local CTradeMgr = import "L10.Game.CTradeMgr"
local Gac2Gas = import "L10.Game.Gac2Gas"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CTradeMessageBox.m_Init_CS2LuaHook = function (this) 
    UIEventListener.Get(this.buyButton).onClick = MakeDelegateFromCSFunction(this.OnBuyButtonClick, VoidDelegate, this)
    UIEventListener.Get(this.sellButton).onClick = MakeDelegateFromCSFunction(this.OnSellButtonClick, VoidDelegate, this)
    UIEventListener.Get(this.cancelButton).onClick = MakeDelegateFromCSFunction(this.OnCancelButtonClick, VoidDelegate, this)

    UIEventListener.Get(this.presentedButton).onClick = MakeDelegateFromCSFunction(this.OnPresentedButtonClick, VoidDelegate, this)
    UIEventListener.Get(this.presentedExplainButton).onClick = MakeDelegateFromCSFunction(this.OnPresentedExplainButtonClick, VoidDelegate, this)

    UIEventListener.Get(this.hongbaoButton).onClick = MakeDelegateFromCSFunction(this.OnHongbaoButtonClick, VoidDelegate, this)
    UIEventListener.Get(this.hongbaoExplainButton).onClick = MakeDelegateFromCSFunction(this.OnHongbaoExplainButtonClick, VoidDelegate, this)
    this.hongbaoText.text = g_MessageMgr:FormatMessage("Personal_HongBao_Description")
end
CTradeMessageBox.m_OnHongbaoButtonClick_CS2LuaHook = function (this, go) 
    if CClientMainPlayer.Inst == nil or CClientMainPlayer.Inst.TargetForLua == nil then
        g_MessageMgr:ShowMessage("TRADE_NOT_NEARBY")
    else
        Gac2Gas.QueryPersonalHongBaoOpenState()
    end
    this:Close()
end
CTradeMessageBox.m_OnPresentedButtonClick_CS2LuaHook = function (this, go) 
    if CClientMainPlayer.Inst == nil or CClientMainPlayer.Inst.TargetForLua == nil then
        g_MessageMgr:ShowMessage("TRADE_NOT_NEARBY")
    else
        Gac2Gas.RequestStartGift(CTradeMessageBox.playerId)
    end
    this:Close()
end
CTradeMessageBox.m_OnBuyButtonClick_CS2LuaHook = function (this, go) 

    if CClientMainPlayer.Inst == nil or CClientMainPlayer.Inst.TargetForLua == nil then
        g_MessageMgr:ShowMessage("TRADE_NOT_NEARBY")
    else
        CTradeMgr.Inst.targetEngineId = CClientMainPlayer.Inst.TargetForLua.EngineId
        Gac2Gas.RequestStartTrade(CTradeMessageBox.playerId, EnumTradeSellType_lua.Silver)
    end
    this:Close()
end
CTradeMessageBox.m_OnSellButtonClick_CS2LuaHook = function (this, go) 

    if CClientMainPlayer.Inst == nil or CClientMainPlayer.Inst.TargetForLua == nil then
        g_MessageMgr:ShowMessage("TRADE_NOT_NEARBY")
    else
        CTradeMgr.Inst.targetEngineId = CClientMainPlayer.Inst.TargetForLua.EngineId
        Gac2Gas.RequestStartTrade(CTradeMessageBox.playerId, EnumTradeSellType_lua.Precious)
    end
    this:Close()
end
