-- Auto Generated!!
local CLingShouSkillSwitchSelectItem = import "L10.UI.CLingShouSkillSwitchSelectItem"
local CLingShouSwitchSkillMgr = import "L10.Game.CLingShouSwitchSkillMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
local Time = import "UnityEngine.Time"
local UInt32 = import "System.UInt32"
CLingShouSkillSwitchSelectItem.m_Init_CS2LuaHook = function (this, skillId) 
    this.mSkillId = skillId
    this.tick:SetActive(false)
    this.lockSprite:SetActive(false)
    local template = Skill_AllSkills.GetData(skillId)
    if template ~= nil then
        this.icon:LoadSkillIcon(template.SkillIcon)
    end
    if CLingShouSwitchSkillMgr.Inst.SwitchAdvanceSkill then
        if this.mSkillId ~= 0 then
            if CLingShouSwitchSkillMgr.Inst.AdvanceSkillToSwitch == this.mSkillId then
                this.tick:SetActive(true)
            end
        end
    else
        if CommonDefs.ListContains(CLingShouSwitchSkillMgr.Inst.lockedSkillIdList, typeof(UInt32), this.mSkillId) then
            this.lockSprite:SetActive(true)
        end
    end
end
CLingShouSkillSwitchSelectItem.m_OnChooseSkill_CS2LuaHook = function (this, id, state) 
    if this.mSkillId == id then
        this.isSelected = state
        if state then
            CLingShouSwitchSkillMgr.Inst.AdvanceSkillToSwitch = this.mSkillId
        end
    end
end
CLingShouSkillSwitchSelectItem.m_OnClick_CS2LuaHook = function (this) 
    if this.mSkillId == 0 then
        return
    end
    if CLingShouSwitchSkillMgr.Inst.SwitchAdvanceSkill then
        if this.isSelected then
            this.isSelected = false
            CLingShouSwitchSkillMgr.Inst.AdvanceSkillToSwitch = 0
            EventManager.BroadcastInternalForLua(EnumEventType.LingShouSwitchSkill_ChooseSkill, {this.mSkillId, false})
        else
            local preId = CLingShouSwitchSkillMgr.Inst.AdvanceSkillToSwitch
            if preId > 0 and preId ~= this.mSkillId then
                EventManager.BroadcastInternalForLua(EnumEventType.LingShouSwitchSkill_ChooseSkill, {preId, false})
            end
            this.isSelected = true
            --isSelected = true;
            --另外那个技能就不能选了
            CLingShouSwitchSkillMgr.Inst.AdvanceSkillToSwitch = this.mSkillId
            EventManager.BroadcastInternalForLua(EnumEventType.LingShouSwitchSkill_ChooseSkill, {this.mSkillId, true})
        end
    else
        --可多选 但是有数量限制
        if this.isSelected then
            this.isSelected = false
            CLingShouSwitchSkillMgr.Inst:RemoveLockedSkill(this.mSkillId)
            EventManager.BroadcastInternalForLua(EnumEventType.LingShouSwitchSkill_LockSkill, {this.mSkillId, false})
            EventManager.Broadcast(EnumEventType.LingShouSwitchSkill_UpdateLockCount)
        else
            if CLingShouSwitchSkillMgr.Inst:CanLock(this.mSkillId) then
                this.isSelected = true
                CLingShouSwitchSkillMgr.Inst:AddLockedSkill(this.mSkillId)
                EventManager.BroadcastInternalForLua(EnumEventType.LingShouSwitchSkill_LockSkill, {this.mSkillId, true})
                EventManager.Broadcast(EnumEventType.LingShouSwitchSkill_UpdateLockCount)
            else
                g_MessageMgr:ShowMessage("LING_SHOU_REPLACE_LOCKSKILL_FULL")
            end
        end
    end

    --弹出提示
    --if (mSkillId > 0)
    --{
    --    CSkillInfoMgr.ShowSkillInfoWnd(mSkillId);
    --}
end
CLingShouSkillSwitchSelectItem.m_OnPress_CS2LuaHook = function (this, pressed) 
    this.mPressed = pressed
    if pressed then
        this.lastPressStartTime = Time.realtimeSinceStartup
    end
end
