local CGamePlayMgr = import "L10.Game.CGamePlayMgr"
local CScene = import "L10.Game.CScene"

LuaDuanWu2020TaskBoardView = class()

RegistChildComponent(LuaDuanWu2020TaskBoardView, "m_TimeLabel","TimeLabel", UILabel)
RegistChildComponent(LuaDuanWu2020TaskBoardView, "m_TaskDesc","TaskDesc", UILabel)
RegistChildComponent(LuaDuanWu2020TaskBoardView, "m_RuleBtn","RuleBtn", GameObject)
RegistChildComponent(LuaDuanWu2020TaskBoardView, "m_LeaveBtn","LeaveBtn", GameObject)

function LuaDuanWu2020TaskBoardView:OnEnable()
    UIEventListener.Get(self.m_LeaveBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        CGamePlayMgr.Inst:LeavePlay()
    end)
    UIEventListener.Get(self.m_RuleBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        g_MessageMgr:ShowMessage("JiuGeShiHun_ReadMe")
    end)
    self:OnMonsterKillCountUpdate()
    g_ScriptEvent:AddListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
    g_ScriptEvent:AddListener("JiuGeShiHunMonsterKillCountUpdate", self, "OnMonsterKillCountUpdate")
end

function LuaDuanWu2020TaskBoardView:OnDisable()
    g_ScriptEvent:RemoveListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
    g_ScriptEvent:RemoveListener("JiuGeShiHunMonsterKillCountUpdate", self, "OnMonsterKillCountUpdate")
end

function LuaDuanWu2020TaskBoardView:OnMonsterKillCountUpdate()
    local monsterKillCount = DuanWu2020Mgr.m_MonsterKillCount
    local msg = g_MessageMgr:FormatMessage("jiuge_counting", math.floor(tonumber(monsterKillCount)))
    self.m_TaskDesc.text = msg
end

function LuaDuanWu2020TaskBoardView:OnSceneRemainTimeUpdate(args)
    if CScene.MainScene then
        if CScene.MainScene.ShowTimeCountDown then
            self.m_TimeLabel.text = self:GetRemainTimeText(CScene.MainScene.ShowTime)
        else
            self.m_TimeLabel.text = nil
        end
    else
        self.m_TimeLabel.text = nil
    end
end

function LuaDuanWu2020TaskBoardView:GetRemainTimeText(totalSeconds)
    if totalSeconds < 0 then
        totalSeconds = 0
    end
    if totalSeconds >= 3600 then
        return SafeStringFormat3("%02d:%02d:%02d", math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
    else
        return SafeStringFormat3("%02d:%02d", math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end