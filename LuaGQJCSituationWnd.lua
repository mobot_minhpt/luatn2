require("common/common_include")

local UIGrid = import "UIGrid"
local Color = import "UnityEngine.Color"
local Profession = import "L10.Game.Profession"
local XianZongShan_Group = import "L10.Game.XianZongShan_Group"

LuaGQJCSituationWnd = class()

RegistClassMember(LuaGQJCSituationWnd, "ItemTemplate")
RegistClassMember(LuaGQJCSituationWnd, "Grid")

RegistClassMember(LuaGQJCSituationWnd, "SituationInfo")

function LuaGQJCSituationWnd:Init()
	self:InitClassMembers()
	self:InitValues()

end

function LuaGQJCSituationWnd:InitClassMembers()
	self.Grid = self.transform:Find("TableView/TableBody/Grid"):GetComponent(typeof(UIGrid))
	self.ItemTemplate = self.transform:Find("TableView/Pool/ItemTemplate").gameObject
	self.ItemTemplate:SetActive(false)

end

function LuaGQJCSituationWnd:InitValues()
	self.SituationInfo = {}
	self:UpdateSituationInfos(CLuaGuoQingJiaoChangMgr.m_SituationInfo)
end

function LuaGQJCSituationWnd:UpdateSituationInfos(infos)
	if not infos then
		CUIManager.CloseUI("GQJCSituationWnd")
		return
	end

	self.SituationInfo = infos
	CUICommonDef.ClearTransform(self.Grid.transform)

	for k, v in ipairs(self.SituationInfo) do
		local go = NGUITools.AddChild(self.Grid.gameObject, self.ItemTemplate)
		go:SetActive(true)
		self:InitItem(go, v)
	end

	self.Grid:Reposition()
end

function LuaGQJCSituationWnd:InitItem(go, info)
	if not info then return end

	local nameLabel = go.transform:Find("Table/NameLabel"):GetComponent(typeof(UILabel))
	local jobImage = go.transform:Find("Table/NameLabel/JobImage"):GetComponent(typeof(UISprite))
	local killNumLabel = go.transform:Find("Table/KillNumLabel"):GetComponent(typeof(UILabel))
	local rebirthNumLabel = go.transform:Find("Table/RebirthNumLabel"):GetComponent(typeof(UILabel))
	local finishLabel = go.transform:Find("Table/FinishLabel"):GetComponent(typeof(UILabel))
	local forceIcon = go.transform:Find("Table/ForceIcon"):GetComponent(typeof(UISprite))

	-- 确定颜色和透明度处理
	local color = Color.white
	if info.playerId == CClientMainPlayer.Inst.Id then
		color = Color.green
	else
		if info.force == CLuaGuoQingJiaoChangMgr.m_MainPlayerForce then
			color = Color.yellow
		end
	end
	local alpha = 1
	if info.status == EnumGQJCPlayerStatus.eEnd then
		alpha = 0.3
	end

	nameLabel.text = info.name
	nameLabel.color = color
	nameLabel.alpha = alpha
	jobImage.spriteName = Profession.GetIconByNumber(info.class)

	-- 添加阵营信息的显示 force
	local groupInfo = XianZongShan_Group.GetData(info.force)
	if not groupInfo then
		forceIcon.spriteName = nil
	else
		forceIcon.spriteName = groupInfo.Icon
	end

	killNumLabel.text = tostring(info.killNum)
	killNumLabel.color = color
	killNumLabel.alpha = alpha

	rebirthNumLabel.text = tostring(info.reliveNum)
	rebirthNumLabel.color = color
	rebirthNumLabel.alpha = alpha

	finishLabel.text = tostring(info.clearNum)
	finishLabel.color = color
	finishLabel.alpha = alpha


end

function LuaGQJCSituationWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateGQJCSituation", self, "UpdateSituationInfos")
end

function LuaGQJCSituationWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateGQJCSituation", self, "UpdateSituationInfos")
end


return LuaGQJCSituationWnd