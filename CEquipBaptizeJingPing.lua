-- Auto Generated!!
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local AlignType1 = import "L10.UI.CTooltip+AlignType"
local CEquipBaptizeJingPing = import "L10.UI.CEquipBaptizeJingPing"
local CEquipmentProcessMgr = import "L10.Game.CEquipmentProcessMgr"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local Constants = import "L10.Game.Constants"
local CSwitchMgr = import "L10.Engine.CSwitchMgr"
local Item_Item = import "L10.Game.Item_Item"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
CEquipBaptizeJingPing.m_InitNull_CS2LuaHook = function (this) 
    this.linkTemplateId = 0
    this.getGo:SetActive(false)
    this.icon:Clear()
    this.nameLabel.text = ""
    this.countLabel.text = ""
end
CEquipBaptizeJingPing.m_OnClick_CS2LuaHook = function (this) 
    if this.matEnough then
        if this.linkTemplateId > 0 then
            CItemInfoMgr.ShowLinkItemTemplateInfo(this.linkTemplateId, false, nil, AlignType.Default, 0, 0, 0, 0)
        end
    else
        --如果没有的时候 点击的时候显示获取途径
        CItemAccessListMgr.Inst:ShowItemAccessInfo(this.linkTemplateId, false, this.transform, AlignType1.Right)
    end
end
CEquipBaptizeJingPing.m_UpdateCount_CS2LuaHook = function (this) 
    local equip = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
    if equip == nil then
        this:InitNull()
        return
    end
    local sum = CEquipmentProcessMgr.Inst:GetWordCostMatNumInBag()

    local cost = CEquipmentProcessMgr.Inst:GetBaptizeCost(equip)
    local costNum = 0
    if cost ~= nil then
        this.wordCostMatId = cost.WordCost[0]
        costNum = cost.WordCost[1]
    else
        this.wordCostMatId = 0
    end

    local bindCount, notBindCount
    bindCount, notBindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(this.wordCostMatId)
    local count = bindCount + notBindCount

	-- 红蓝净瓶积分
	if CSwitchMgr.EnableJingPingScore and CClientMainPlayer.Inst ~= nil then
		if this.wordCostMatId == Constants.JingPing_Red then
			local playScore = CClientMainPlayer.Inst.PlayProp.Scores[LuaEnumPlayScoreKey.HongJingPing] or 0
			count = count + playScore
		elseif this.wordCostMatId == Constants.JingPing_Blue then
			local playScore = CClientMainPlayer.Inst.PlayProp.Scores[LuaEnumPlayScoreKey.LanJingPing] or 0
			count = count + playScore
		end
	end

    local yujingpingCount = 0
    --玉净瓶
    if CSwitchMgr.EnableYuJingPing then
        local isBinded = false
        local equipInfo = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
        if equipInfo ~= nil then
            local equipment = CItemMgr.Inst:GetById(equipInfo.itemId)
            if equipment.IsBinded then
                local bindCount2, notBindCount2
                bindCount2, notBindCount2 = CItemMgr.Inst:CalcItemCountInBagByTemplateIdExclueExpireTime(CEquipmentProcessMgr.YuJingPingItemId)
                yujingpingCount = (bindCount2 + notBindCount2)
            end
        end
    end

    --优先显示红、蓝净瓶
    --然后显示玉净瓶
    --最后显示普通净瓶

    if costNum > count then
        if yujingpingCount < costNum then
            this.linkTemplateId = CEquipmentProcessMgr.Inst.baptizeWordCostBaseItemId
            local template = Item_Item.GetData(CEquipmentProcessMgr.Inst.baptizeWordCostBaseItemId)
            if template ~= nil then
                this.icon:LoadMaterial(template.Icon)
            end
        else
            --玉净瓶
            this.linkTemplateId = CEquipmentProcessMgr.YuJingPingItemId
            local template = Item_Item.GetData(this.linkTemplateId)
            if template ~= nil then
                this.icon:LoadMaterial(template.Icon)
            end
        end
    else
        if this.wordCostMatId == Constants.JingPing_Red or this.wordCostMatId == Constants.JingPing_Blue then
            this.linkTemplateId = this.wordCostMatId
            local template = Item_Item.GetData(this.wordCostMatId)
            if template ~= nil then
                this.icon:LoadMaterial(template.Icon)
            end
        else
            if yujingpingCount < costNum then
                --够了
                this.linkTemplateId = this.wordCostMatId
                local template = Item_Item.GetData(this.wordCostMatId)
                if template ~= nil then
                    this.icon:LoadMaterial(template.Icon)
                end
            else
                --玉净瓶
                this.linkTemplateId = CEquipmentProcessMgr.YuJingPingItemId
                local template = Item_Item.GetData(this.linkTemplateId)
                if template ~= nil then
                    this.icon:LoadMaterial(template.Icon)
                end
            end
        end
    end

    --不够了
    if sum < costNum then
        this.matEnough = false
        this.countLabel.text = System.String.Format("[ff0000]{0}[-]/{1}", sum, costNum)
        if this.getGo ~= nil then
            this.getGo:SetActive(true)
        end
    else
        this.matEnough = true
        this.countLabel.text = System.String.Format("[00ff00]{0}[-]/{1}", sum, costNum)
        if this.getGo ~= nil then
            this.getGo:SetActive(false)
        end
    end
    if this.onCountUpdate ~= nil then
        invoke(this.onCountUpdate)
    end
end
CEquipBaptizeJingPing.m_OnSendItem_CS2LuaHook = function (this, itemId) 

    --数量更新
    local item = CItemMgr.Inst:GetById(itemId)
    if item ~= nil then
        if CEquipmentProcessMgr.Inst.baptizeWordCostBaseItemId == item.TemplateId or this.wordCostMatId == item.TemplateId then
            this:UpdateCount()
        end

        if CSwitchMgr.EnableYuJingPing and CEquipmentProcessMgr.YuJingPingItemId == item.TemplateId then
            this:UpdateCount()
        end
    end

    local equip = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
    if equip ~= nil then
        if item ~= nil and equip.templateId == item.TemplateId then
            this:UpdateCount()
        end
    end
end
CEquipBaptizeJingPing.m_OnSetItemAt_CS2LuaHook = function (this, place, position, oldItemId, newItemId) 

    if oldItemId == nil and newItemId == nil then
        return
    end
    local default
    if oldItemId ~= nil then
        default = oldItemId
    else
        default = newItemId
    end
    local itemId = default

    this:OnSendItem(itemId)
end
