require ("3rdParty/LuaUtil")
require ("common/common_include")
local CLogMgr = import "L10.CLogMgr"

local ThisClassMagic = "Event.94E6493F-245E-41F9-AC39-32F74D06A58E";
LuaSetGlobalIfNil("g_ScriptEvent",
{
	m_strMagic 		= "Event.94E6493F-245E-41F9-AC39-32F74D06A58E";	
	m_iEventId 		= 1, -- event id	
	m_tEventHandler = {}, -- event:subevent handler
	m_tEventList 	= {}, -- all event
});

function g_ScriptEvent:LogErrorValidString(strVal)
	if(type(strVal) ~= "string") then
		--CLogMgr.LogError("g_ScriptEvent: param not a string type: type=> " .. type(strVal));
		return false;
	end
	
	if(strVal == "") then
		--CLogMgr.LogError("g_ScriptEvent: param is empty string");
		return false;
	end
	
	return true;
end

function g_ScriptEvent:LogErrorValidTable(tTable)
	if(type(tTable) ~= "table" or tTable == nil) then
		--CLogMgr("g_ScriptEvent: param not a table type: type=> " .. type(tTable));
		return false;
	end
	return true;
end

function g_ScriptEvent:ValidParam( evtType, tTable, strFuncName )
	-- if(self:LogErrorValidString(strEvtType) == false) then return false; end
	if(self:LogErrorValidTable(tTable) == false) then return false; end
	if(self:LogErrorValidString(strFuncName) == false) then return false; end
	
	return true;
end

function g_ScriptEvent:ClearAll()
	g_ScriptEvent.m_tEventHandler = {};
	g_ScriptEvent.m_tEventList = {};
end

-- add listen for some event
function g_ScriptEvent:AddListener( evtType, tTable, strFuncName )
	if(self:ValidParam(evtType, tTable, strFuncName) == false) then return; end

	local tEventHandlers = self.m_tEventHandler[ evtType ]
	if not tEventHandlers then
		tEventHandlers = { events = {}, delay = {}, isBroadcasting = false }--setmetatable({}, {isBroadcasting = false})
	else
		--广播时再添加，需要加到delay里面
		if tEventHandlers.isBroadcasting then
			if tEventHandlers.delay[tTable] == nil then
				tEventHandlers.delay[tTable] = strFuncName
			end
			return
		end
	end

	
	if tEventHandlers.events[tTable] == nil then
		tEventHandlers.events[tTable] = strFuncName
	else
		--CLogMgr.LogError(string.format("g_ScriptEvent:AddListener: double call to same event on same table, event[%s], table:func[%s:%s]",strEvtType, tostring(tTable), strFuncName) );
		return ;
	end
	self.m_tEventHandler[evtType] = tEventHandlers;
end

-- remove listen for some event
function g_ScriptEvent:RemoveListener(evtType, tTable, strFuncName)
	if(self:ValidParam(evtType, tTable, strFuncName) == false) then return; end

	local tEventHandlers = self.m_tEventHandler[evtType]-- or {} 
	if tEventHandlers then
		if tEventHandlers.events[tTable] ~= nil then
			tEventHandlers.events[tTable] = nil;
			if(g_LuaUtil:GetTableSize(tEventHandlers.events) <= 0.1) then
				self.m_tEventHandler[ evtType ] = nil;
			end
		end
	end
end

function g_ScriptEvent:Broadcast(args)
	local strEvtType = args[0]
	local tEventHandlers = self.m_tEventHandler[strEvtType]
	if(tEventHandlers == nil) then 
		return 
	end
	if tEventHandlers.isBroadcasting then
		CLogMgr.LogError(strEvtType.." recursive broadcast")
		return 
	end

	tEventHandlers.isBroadcasting = true
	for table, func in pairs(tEventHandlers.events) do
		local tListener = table
		local pFunc = func

		local bSucceed, res = pcall(function(arg) tListener[pFunc](tListener, arg) end,args[1])
		if not bSucceed then
			CLogMgr.LogError(res)
		end
	end
	tEventHandlers.isBroadcasting = false
end

--lua中使用的消息方式
function g_ScriptEvent:BroadcastInLua(evtType,...)
	local tEventHandlers = self.m_tEventHandler[evtType]
	if(tEventHandlers == nil) then 

		return 
	end
	if tEventHandlers.isBroadcasting then
		CLogMgr.LogError(evtType.." recursive broadcast")
		return 
	end
	tEventHandlers.isBroadcasting = true
	for table, func in pairs(tEventHandlers.events) do
		local tListener = table
		local pFunc = func

		local bSucceed, res = pcall(function(...) tListener[pFunc](tListener, ...) end,...)
		if not bSucceed then
			CLogMgr.LogError(res)
		end
	end
	tEventHandlers.isBroadcasting = false

	for t,func in pairs(tEventHandlers.delay) do
		if tEventHandlers.events[t] then
			print("override",evtType)
		end
		tEventHandlers.events[t] = func
	end
	--clear delay
	for k in pairs (tEventHandlers.delay) do
		tEventHandlers.delay[k] = nil
	end
end

function g_ScriptEvent:Init()
	
end

function g_ScriptEvent:Dump()
	print("events dump start")
	local function getTableName(tbl)
		for k, v in pairs(_G) do
			  if v == tbl then
				return k
			  end
		end
		return nil
	end
	local function GetName(tbl)
		local name = getTableName(tbl)
		if not name then
			name = getTableName(tbl.__class)
			if not name then
				return "nil"
			end
		end
		return name
	end
	for k,v in pairs(self.m_tEventHandler) do
		local evt = k..":"
		for t,funcname in pairs(v.events) do
			evt = evt .."  "..GetName(t).."."..funcname
		end
		print(evt)
	end
	print("events dump end")
end
return g_ScriptEvent