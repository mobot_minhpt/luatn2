-- Auto Generated!!
local CellIndex = import "L10.UI.UITableView+CellIndex"
local CellIndexType = import "L10.UI.UITableView+CellIndexType"
local CommonDefs = import "L10.Game.CommonDefs"
local CRankData = import "L10.UI.CRankData"
local CRankMasterView = import "L10.UI.CRankMasterView"
local DelegateFactory = import "DelegateFactory"
local Section = import "L10.UI.CRankMasterView+Section"
local String = import "System.String"
CRankMasterView.m_Start_CS2LuaHook = function (this) 
    this.MainClass = CRankData.Inst.AllMainCategories
    local initRankId = CRankData.Inst.InitRankId
    local row, column
    local j = 0
    CommonDefs.ListIterate(this.MainClass, DelegateFactory.Action_object(function (___value) 
        local mainclass = ___value
        local SubClass = mainclass.children
        local SubClassName = CreateFromClass(MakeGenericClass(List, String))
        do
            local i = 0
            while i < SubClass.Count do
                CommonDefs.ListAdd(SubClassName, typeof(String), SubClass[i].name)
                if initRankId ~= 0 and SubClass[i].children[0].rankId == initRankId then
                   row = j
                   column = i 
                end
                i = i + 1
            end
        end
        CommonDefs.ListAdd(this.sections, typeof(Section), CreateFromClass(Section, mainclass.name, SubClassName))
        j = j + 1
    end))
    if row and column then
        this:LoadData(CreateFromClass(CellIndex, row, column, CellIndexType.Row), true)
    else
        this:LoadData(CreateFromClass(CellIndex, 0, 0, CellIndexType.Section), true)
    end
    CRankData.Inst.InitRankId = 0
end
