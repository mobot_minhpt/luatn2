-- Auto Generated!!
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CChargeGitItem = import "L10.UI.CChargeGitItem"
local CChargeWnd = import "L10.UI.CChargeWnd"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CGiftTemplate = import "L10.UI.CGiftTemplate"
local Charge_VipInfo = import "L10.Game.Charge_VipInfo"
local CItem = import "L10.Game.CItem"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local Extensions = import "Extensions"
local GiftRecieveStatus = import "L10.UI.GiftRecieveStatus"
local Item_Item = import "L10.Game.Item_Item"
local LocalString = import "LocalString"
local Quaternion = import "UnityEngine.Quaternion"
local System = import "System"
local UIEventListener = import "UIEventListener"
local UInt16 = import "System.UInt16"
local Vector3 = import "UnityEngine.Vector3"

CChargeGitItem.m_OnRecieveGift_CS2LuaHook = function (this, button)    --先判断是否已经领取了
    if this:GetGiftStatus(this.m_CurrentPageLevel) == GiftRecieveStatus.CanRecieve then
        Gac2Gas.RequestGetVipLevelGift(this.m_CurrentPageLevel)
    end
end
CChargeGitItem.m_UpdateView_CS2LuaHook = function (this, vipId)
    this.m_CurrentPageLevel = vipId
    local status = this:GetGiftStatus(this.m_CurrentPageLevel)
    local info = Charge_VipInfo.GetData(vipId)
    local targetVipCharge = CChargeWnd.GetVipCharge(this.m_CurrentPageLevel)
    local currentTotalCharge = CChargeWnd.VipInfo.TotalCharge
    if CommonDefs.IS_HMT_CLIENT or CommonDefs.IS_VN_CLIENT then
        if info.ShowCharge > 0 or this.m_CurrentPageLevel <= CChargeWnd.VipInfo.Level + 1 then
            this.m_CurrentLevelCostLabel.text = (System.String.Format("{0:N0}", targetVipCharge) .. " ") .. LocalString.GetString("灵玉")
        else
            this.m_CurrentLevelCostLabel.text = "??,??? " --[["??,???" + " "]] .. LocalString.GetString("灵玉")
        end

        --新加逻辑 超过18级 不显示充值金额
        if this.m_CurrentPageLevel > CChargeWnd.MIN_HIDECHARGE_LEVEL then
            this.m_CurrentLevelCostLabel.text = "??,??? " --[["??,???" + " "]] .. LocalString.GetString("灵玉")
        end
    elseif CommonDefs.IS_KR_CLIENT then
        this.m_CurrentLevelCostLabel.text = System.String.Format("{0:N0}", targetVipCharge) .. LocalString.GetString("倩女点")
    else
        if info.ShowCharge > 0 or this.m_CurrentPageLevel <= CChargeWnd.VipInfo.Level + 1 then
            this.m_CurrentLevelCostLabel.text = System.String.Format(LocalString.GetString("{0:N0}元"), targetVipCharge)
        else
            this.m_CurrentLevelCostLabel.text = LocalString.GetString("??,??? 元")
        end

        --新加逻辑 超过18级 不显示充值金额
        if this.m_CurrentPageLevel > CChargeWnd.MIN_HIDECHARGE_LEVEL then
            this.m_CurrentLevelCostLabel.text = LocalString.GetString("??,??? 元")
        end
    end

    Extensions.RemoveAllChildren(this.m_GiftTable.transform)
    do
        local i = 0
        while i < info.Gift.Length do
            local obj = CommonDefs.Object_Instantiate(this.m_GiftTemplate.gameObject)
            obj:SetActive(true)
            obj.transform.parent = this.m_GiftTable.transform
            obj.transform.localScale = Vector3.one
            obj.transform.localRotation = Quaternion.identity
            obj.transform.localPosition = Vector3(0, 0, 0)
            local template = CommonDefs.GetComponent_GameObject_Type(obj, typeof(CGiftTemplate))
            local item = Item_Item.GetData(info.Gift[i][0])
            local count = info.Gift[i][1]
            local type = CItem.GetQualityType(item.ID)
            template:UpdateGiftInfo(item.Icon, type, count, status)
            UIEventListener.Get(template.gameObject).onClick = DelegateFactory.VoidDelegate(function (go) 
                CItemInfoMgr.ShowLinkItemTemplateInfo(item.ID, false, nil, AlignType.Default, 0, 0, 0, 0)
            end)
            i = i + 1
        end
    end
    this.m_GiftTable:Reposition()

    this.m_RecieveGiftButton.Visible = (status == GiftRecieveStatus.CanRecieve)
    this.m_ProgressBar.Visible = (status == GiftRecieveStatus.CannotRecieve and (info.ShowCharge > 0 or this.m_CurrentPageLevel <= CChargeWnd.VipInfo.Level + 1))
    if this.m_ProgressBar.Visible then
        local pecentage = tonumber(currentTotalCharge / targetVipCharge)
        this.m_ProgressBar:SetProgress(pecentage)
        local formatText = System.String.Format("{0:N0}/{1:N0}", currentTotalCharge, targetVipCharge)
        this.m_ProgressBar:SetText(formatText)
    end
end
CChargeGitItem.m_GetGiftStatus_CS2LuaHook = function (this, level) 
    if CClientMainPlayer.Inst ~= nil then
        local vipInfo = CClientMainPlayer.Inst.ItemProp.Vip
        if vipInfo ~= nil then
            if not CommonDefs.DictContains(vipInfo.Level2GiftReceivedTime, typeof(UInt16), level) then
                if level <= vipInfo.Level then
                    return GiftRecieveStatus.CanRecieve
                end
                return GiftRecieveStatus.CannotRecieve
            end
            return GiftRecieveStatus.Recieved
        end
    end
    return GiftRecieveStatus.CannotRecieve
end
