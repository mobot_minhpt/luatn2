local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UIGrid = import "UIGrid"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local Profession=import "L10.Game.Profession"
local EnumClass = import "L10.Game.EnumClass"
local CNumberKeyBoardMgr = import "CNumberKeyBoardMgr"
local CTooltip=import "L10.UI.CTooltip"
local UITabBar = import "L10.UI.UITabBar"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local CPlayerShopPopupMenuInfoMgr = import "L10.UI.CPlayerShopPopupMenuInfoMgr"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local CPopupMenuInfoMgrAlignType = import  "L10.UI.CPopupMenuInfoMgr+AlignType"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local QnTipButton = import "L10.UI.QnTipButton"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local CPlayerInfoMgrAlignType = import "CPlayerInfoMgr+AlignType"
LuaStarBiWuFreePlayerWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaStarBiWuFreePlayerWnd, "Grid", "Grid", UIGrid)
RegistChildComponent(LuaStarBiWuFreePlayerWnd, "Empty", "Empty", UILabel)
RegistChildComponent(LuaStarBiWuFreePlayerWnd, "WordFilterButton", "WordFilterButton", QnTipButton)
RegistChildComponent(LuaStarBiWuFreePlayerWnd, "Sort", "Sort", GameObject)
RegistChildComponent(LuaStarBiWuFreePlayerWnd, "OperateButton", "OperateButton", GameObject)
RegistChildComponent(LuaStarBiWuFreePlayerWnd, "ItemTemplate", "ItemTemplate", GameObject)
RegistChildComponent(LuaStarBiWuFreePlayerWnd, "QnIncreseAndDecreaseButton", "QnIncreseAndDecreaseButton", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaStarBiWuFreePlayerWnd, "m_PageLabel")
RegistClassMember(LuaStarBiWuFreePlayerWnd, "m_DecreaseButton")
RegistClassMember(LuaStarBiWuFreePlayerWnd, "m_IncreaseButton")
RegistClassMember(LuaStarBiWuFreePlayerWnd, "m_SortTab")

RegistClassMember(LuaStarBiWuFreePlayerWnd, "m_CurClass")
RegistClassMember(LuaStarBiWuFreePlayerWnd, "m_CurSortType")
RegistClassMember(LuaStarBiWuFreePlayerWnd, "m_Count")
RegistClassMember(LuaStarBiWuFreePlayerWnd, "m_CurPage")
RegistClassMember(LuaStarBiWuFreePlayerWnd, "m_MaxPage")
RegistClassMember(LuaStarBiWuFreePlayerWnd, "m_CurSelectMember")

RegistClassMember(LuaStarBiWuFreePlayerWnd, "m_ChildTab")
RegistClassMember(LuaStarBiWuFreePlayerWnd, "m_FilterActions")

RegistClassMember(LuaStarBiWuFreePlayerWnd, "m_ModelListView")
RegistClassMember(LuaStarBiWuFreePlayerWnd, "m_CurMemberInfo")
function LuaStarBiWuFreePlayerWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.OperateButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOperateButtonClick()
	end)

    --@endregion EventBind end
	self.m_PageLabel = self.QnIncreseAndDecreaseButton.transform:Find("Label"):GetComponent(typeof(UILabel))
	self.m_DecreaseButton = self.QnIncreseAndDecreaseButton.transform:Find("DecreaseButton"):GetComponent(typeof(QnButton))
	self.m_IncreaseButton = self.QnIncreseAndDecreaseButton.transform:Find("IncreaseButton"):GetComponent(typeof(QnButton))
	self.m_SortTab = self.Sort.transform:Find("TabTable"):GetComponent(typeof(UITabBar))
	
	UIEventListener.Get(self.m_PageLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnPageLabelClick(go)
	end)
	UIEventListener.Get(self.m_IncreaseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSetPage(self.m_CurPage + 1)
	end)
	UIEventListener.Get(self.m_DecreaseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSetPage(self.m_CurPage - 1)
	end)
	self.m_SortTab.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
		self:OnSortTabChange(index)
	end)

	self.m_Count = 5		-- 一页显示的数量
	self.m_CurPage = 1		-- 当前页数
	self.m_CurClass = 0		-- 当前筛选职业
	self.m_CurSortType = 1	-- 默认按战斗力排序 1:按照战力排序 2:按照明星值排序
	self.m_MaxPage = 0		-- 最大页数
	self.m_CurSelectMember = 0	-- 当前选中的成员
	self.m_ModelListView = nil
	self.m_CurMemberInfo = nil

	self.m_ChildTab = {LocalString.GetString("全部职业")}
	self.ItemTemplate.gameObject:SetActive(false)
end

function LuaStarBiWuFreePlayerWnd:Init()
	self.OperateButton.gameObject:SetActive(false)
	Gac2Gas.QueryStarBiwuStarScore()

	self:OnPublishStatusChanged()
	self:InitFilterButton()
	self:InitModelList()
	self:ReplyStarBiwuZhanDuiInfo()
	Gac2Gas.QueryStarBiwuFreememberS13(self.m_CurClass,self.m_CurSortType,self.m_CurPage,self.m_Count)
end

function LuaStarBiWuFreePlayerWnd:InitModelList()
	if not self.m_ModelListView then
		self.m_ModelListView = {}
		Extensions.RemoveAllChildren(self.Grid.transform)
		for i = 1,self.m_Count do
			local viewInfo = {}
			local go = CUICommonDef.AddChild(self.Grid.gameObject,self.ItemTemplate.gameObject)
			go.transform.parent = self.Grid.transform
			go.transform.localPosition = Vector3.zero
			go.transform.localScale = Vector3.one
			local TypeIcon = go.transform:Find("TypeIcon"):GetComponent(typeof(UISprite))
			local SelectedSprite = go.transform:Find("SelectedSprite"):GetComponent(typeof(UISprite))
			SelectedSprite.gameObject:SetActive(false)
			local MemberModel = go.transform:Find("Panel/MemberModel"):GetComponent(typeof(UITexture))
			local NameLabel = go.transform:Find("MemberInfo/nameLabel"):GetComponent(typeof(UILabel))
			local ServerLabel = go.transform:Find("MemberInfo/serverLabel"):GetComponent(typeof(UILabel))
			local CapacityLabel = go.transform:Find("MemberInfo/capacityLabel"):GetComponent(typeof(UILabel))
			local LevelLabel = go.transform:Find("MemberInfo/Level/levelLabel"):GetComponent(typeof(UILabel))
			local MingxingLabel = go.transform:Find("MemberInfo/mingxingLabel"):GetComponent(typeof(UILabel))
			local Ro = nil
			local ModelTextureLoader = LuaDefaultModelTextureLoader.Create(function(ro)
				Ro = ro
			end)
			local itemIdentifity = SafeStringFormat3("__%s__",go.gameObject:GetInstanceID())
			MemberModel.mainTexture = CUIManager.CreateModelTexture(itemIdentifity,ModelTextureLoader, 180, 0.1, -1.1, 4.3, false, true, 1,false,false)
			viewInfo.view = go
			viewInfo.itemIdentifity = itemIdentifity
			viewInfo.TypeIcon = TypeIcon
			viewInfo.SelectedSprite = SelectedSprite
			viewInfo.MemberModel = MemberModel
			viewInfo.NameLabel = NameLabel
			viewInfo.ServerLabel = ServerLabel
			viewInfo.CapacityLabel = CapacityLabel
			viewInfo.LevelLabel = LevelLabel
			viewInfo.MingxingLabel = MingxingLabel
			viewInfo.Ro = Ro
			UIEventListener.Get(go.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
				self:OnMemberClick(i)
			end)
			table.insert(self.m_ModelListView,viewInfo)
		end
	end
end

function LuaStarBiWuFreePlayerWnd:InitFilterButton()
	self.m_FilterActions = {}
	for i = 1,EnumToInt(EnumClass.Undefined)-1 do
		table.insert(self.m_ChildTab,Profession.GetFullName(CommonDefs.ConvertIntToEnum(typeof(EnumClass), i)))
	end
	for i,name in pairs(self.m_ChildTab) do
		table.insert(self.m_FilterActions,PopupMenuItemData(name, DelegateFactory.Action_int(function (index) 
			self:OnSelectIndex(index)
		end), false, nil))
	end
	UIEventListener.Get(self.WordFilterButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnWordFilterButtonClick()
	end)
	self.WordFilterButton:SetTipStatus(true)
end

function LuaStarBiWuFreePlayerWnd:UpdatePage()
	self.m_PageLabel.text = SafeStringFormat3( "%d/%d",self.m_CurPage,self.m_MaxPage )
	if self.m_CurPage <= 1 then
        self.m_DecreaseButton.Enabled = false
    else
        self.m_DecreaseButton.Enabled = true
    end
    if self.m_CurPage >= self.m_MaxPage then
        self.m_IncreaseButton.Enabled = false
    else
        self.m_IncreaseButton.Enabled = true
    end
end

function LuaStarBiWuFreePlayerWnd:UpdateSortTab()
	self.m_SortTab:ChangeTab(self.m_CurSortType - 1,true)
end

function LuaStarBiWuFreePlayerWnd:OnSortTabChange(index)
	self.m_CurPage = math.max(1,math.min(self.m_CurPage,self.m_MaxPage))
	Gac2Gas.QueryStarBiwuFreememberS13(self.m_CurClass,index + 1,self.m_CurPage,self.m_Count)
	self.m_SortTab:ChangeTab(self.m_CurSortType - 1,true)
end

function LuaStarBiWuFreePlayerWnd:OnSelectIndex(index)
	self.m_CurPage = math.max(1,math.min(self.m_CurPage,self.m_MaxPage))
	Gac2Gas.QueryStarBiwuFreememberS13(index,self.m_CurSortType,self.m_CurPage,self.m_Count)
end

--@region UIEvent

function LuaStarBiWuFreePlayerWnd:OnWordFilterButtonClick()
	self.WordFilterButton:SetTipStatus(false)
	CPlayerShopPopupMenuInfoMgr.ShowPlayerShopPopupMenuWithdefaultSelectedIndex(
		Table2Array(self.m_FilterActions, MakeArrayClass(PopupMenuItemData)), self.m_CurClass, 
		self.WordFilterButton.transform, CPopupMenuInfoMgrAlignType.Bottom, 2, nil, nil, DelegateFactory.Action(function ()
        self.WordFilterButton:SetTipStatus(true)
    end), 600,300)
end

function LuaStarBiWuFreePlayerWnd:OnOperateButtonClick()
	if not CLuaStarBiwuMgr.m_PublishInfoToFreePlayer and CClientMainPlayer.Inst then
		g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("StarBiwu_PublishPlayerInfo_Confirm",tostring(CClientMainPlayer.Inst.PlayProp.PlayerCapacity),tostring(CLuaStarBiwuMgr.m_MainPlayerStarScore)), function()
			Gac2Gas.RequestPublishToStarBiwuFreeMember(not CLuaStarBiwuMgr.m_PublishInfoToFreePlayer)
		end, nil, nil, nil, false)
	else
		g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("StarBiwu_CancelPublishPlayerInfo_Confirm"), function()
			Gac2Gas.RequestPublishToStarBiwuFreeMember(not CLuaStarBiwuMgr.m_PublishInfoToFreePlayer)
		end, nil, nil, nil, false)
	end
end

function LuaStarBiWuFreePlayerWnd:OnPageLabelClick(go)
	local min=1
    local max=math.max(1,self.m_MaxPage)

    local val=max
    local num=0
    while val>=1 do
        num = num+1
        val = val/10
    end
	CNumberKeyBoardMgr.ShowNumberKeyBoardWithRange(min, max, self.m_CurPage, num, DelegateFactory.Action_int(function (val)
        self.m_PageLabel.text = tostring(val)
    end), DelegateFactory.Action_int(function (val)
        local page=math.max(1,val)
        page=math.min(self.m_MaxPage,page)
        self.m_PageLabel.text = SafeStringFormat3("%d/%d", page, self.m_MaxPage)
        self:OnSetPage(page)
    end), self.m_PageLabel, CTooltip.AlignType.Top, true)
end

function LuaStarBiWuFreePlayerWnd:OnSetPage(page)
	local curpage = math.max(1,math.min(page,self.m_MaxPage))
	Gac2Gas.QueryStarBiwuFreememberS13(self.m_CurClass,self.m_CurSortType,curpage,self.m_Count)
end

function LuaStarBiWuFreePlayerWnd:OnPublishStatusChanged()
	local isPublish = CLuaStarBiwuMgr.m_PublishInfoToFreePlayer
	local btnLabel = self.OperateButton.transform:Find("Label"):GetComponent(typeof(UILabel))
    if not isPublish then -- 不在公布
        self.OperateButton:GetComponent(typeof(UISprite)).spriteName = "common_btn_01_yellow"
        btnLabel.text = LocalString.GetString("公布我的信息")
    elseif isPublish then -- 公布中
        self.OperateButton:GetComponent(typeof(UISprite)).spriteName = "common_btn_01_blue"
        btnLabel.text = LocalString.GetString("隐藏我的信息")
    end
end

function LuaStarBiWuFreePlayerWnd:OnSyncStarBiwuFreememberS13(profession, sortMethed, memberList, page, count ,bPublishSelfInfo, totalPageCount)
	self.m_CurPage = math.min(page, totalPageCount)
	self.m_CurClass = profession
	self.m_CurSortType = sortMethed
	self.m_MaxPage = totalPageCount
	self:UpdatePage()
	self:UpdateSortTab()
	self:OnPublishStatusChanged()
	self.WordFilterButton.Text = self.m_ChildTab[self.m_CurClass + 1]

	if not memberList or #memberList <= 0 then 
		self.Empty.gameObject:SetActive(true) 
		local label = self.Empty:GetComponent(typeof(UILabel))
		if self.m_CurClass == 0 then
			label.text = g_MessageMgr:FormatMessage("StarBiwu_FreePlayer_Empty_All")
		else
			label.text =  g_MessageMgr:FormatMessage("StarBiwu_FreePlayer_Empty_Class")
		end
		self.Grid.gameObject:SetActive(false) 
		return
	end
	self.Empty.gameObject:SetActive(false)
	self.Grid.gameObject:SetActive(true)
	self.m_CurMemberInfo = memberList
	table.sort(self.m_CurMemberInfo,function(a,b)
		if self.m_CurSortType == 1 then
			return a.Capacity > b.Capacity -- 战力排序
		elseif self.m_CurSortType == 2 then
			return a.StarScore > b.StarScore -- 明星值排序
		else
			return false
		end
	end)
	if not self.m_ModelListView then
		self:InitModelList()
	end
	for i = 1,self.m_Count do
		local viewInfo = self.m_ModelListView[i]
		if self.m_CurMemberInfo[i] then
			viewInfo.view.gameObject:SetActive(true)
			viewInfo.TypeIcon.spriteName = Profession.GetIconByNumber(self.m_CurMemberInfo[i].Class)
			viewInfo.SelectedSprite.gameObject:SetActive(false)
			viewInfo.NameLabel.text = self.m_CurMemberInfo[i].Name
			if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id ==  self.m_CurMemberInfo[i].Id then
				viewInfo.NameLabel.color =  NGUIText.ParseColor24("02F860", 0)
			else
				viewInfo.NameLabel.color =  NGUIText.ParseColor24("ffffff", 0)
			end
			viewInfo.ServerLabel.text = self.m_CurMemberInfo[i].ServerName
			viewInfo.CapacityLabel.text = tostring(self.m_CurMemberInfo[i].Capacity)
			viewInfo.MingxingLabel.text = tostring(self.m_CurMemberInfo[i].StarScore)
			viewInfo.MingxingLabel.gameObject:SetActive(self.m_CurMemberInfo[i].StarScore > 0)
			viewInfo.LevelLabel.text = SafeStringFormat3("lv.%d",self.m_CurMemberInfo[i].Level)
			viewInfo.LevelLabel.color = self.m_CurMemberInfo[i].xianShenStatus == 1 and  NGUIText.ParseColor24("ff7900", 0) or NGUIText.ParseColor24("ffffff", 0)
			if viewInfo.Ro then
				CClientMainPlayer.LoadResource(viewInfo.Ro, self.m_CurMemberInfo[i].Appearance, true, 1, 0, true, 0, false, 0, false, nil, false, false)
			end
		else
			viewInfo.view.gameObject:SetActive(false)
		end
	end
	self.Grid:Reposition()
end

function LuaStarBiWuFreePlayerWnd:OnMemberClick(index)
	if self.m_ModelListView and self.m_ModelListView[self.m_CurSelectMember] then
		self.m_ModelListView[self.m_CurSelectMember].SelectedSprite.gameObject:SetActive(false)
	end
	if self.m_ModelListView and self.m_ModelListView[index] then
		self.m_ModelListView[index].SelectedSprite.gameObject:SetActive(true)
	end
	self.m_CurSelectMember = index
	local id = self.m_CurMemberInfo[index].Id
	CPlayerInfoMgr.ShowPlayerPopupMenu(id, EnumPlayerInfoContext.StarBiWuFreePlayer,EChatPanel.Undefined,"",nil,Vector3.zero,CPlayerInfoMgrAlignType.Default)
end


function LuaStarBiWuFreePlayerWnd:OnGetPlayerStarScore()
	self.OperateButton.gameObject:SetActive(true and CLuaStarBiwuMgr.m_MySelfZhanduiId <= 0)
end

function LuaStarBiWuFreePlayerWnd:ReplyStarBiwuZhanDuiInfo()
	self.OperateButton.gameObject:SetActive(CLuaStarBiwuMgr.m_MySelfZhanduiId <= 0)
end
--@endregion UIEvent

function LuaStarBiWuFreePlayerWnd:OnEnable()
	g_ScriptEvent:AddListener("OnStarBiWuUpdateMainPlayerStarScore",self, "OnGetPlayerStarScore")
	g_ScriptEvent:AddListener("OnPublishStarBiWuInfoToFreePlayerStatusChanged",self, "OnPublishStatusChanged")
	g_ScriptEvent:AddListener("OnSyncStarBiwuFreememberS13",self, "OnSyncStarBiwuFreememberS13")
	g_ScriptEvent:AddListener("ReplyStarBiwuZhanDuiInfo", self, "ReplyStarBiwuZhanDuiInfo")
    g_ScriptEvent:AddListener("ReplyStarBiwuZhanDuiList", self, "ReplyStarBiwuZhanDuiInfo")
end

function LuaStarBiWuFreePlayerWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnStarBiWuUpdateMainPlayerStarScore",self, "OnGetPlayerStarScore")
	g_ScriptEvent:RemoveListener("OnPublishStarBiWuInfoToFreePlayerStatusChanged",self, "OnPublishStatusChanged")
	g_ScriptEvent:RemoveListener("OnSyncStarBiwuFreememberS13",self, "OnSyncStarBiwuFreememberS13")
	g_ScriptEvent:RemoveListener("ReplyStarBiwuZhanDuiInfo", self, "ReplyStarBiwuZhanDuiInfo")
    g_ScriptEvent:RemoveListener("ReplyStarBiwuZhanDuiList", self, "ReplyStarBiwuZhanDuiInfo")
	if self.m_ModelListView then
		for i = 1,#self.m_ModelListView do
			self.m_ModelListView[i].Ro = nil
			self.m_ModelListView[i].MemberModel.mainTexture = nil
			CUIManager.DestroyModelTexture(self.m_ModelListView[i].itemIdentifity)
		end
	end
end