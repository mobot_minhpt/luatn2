local CButton = import "L10.UI.CButton"

CLuaMengGuiJiePiPeiWnd = class()

RegistChildComponent(CLuaMengGuiJiePiPeiWnd, "tipBtn", CButton)
RegistChildComponent(CLuaMengGuiJiePiPeiWnd, "cancelBtn", CButton)

function CLuaMengGuiJiePiPeiWnd:Init()
    CommonDefs.AddOnClickListener(self.tipBtn.gameObject, DelegateFactory.Action_GameObject(function  (go)
        CUIManager.ShowUI(CLuaUIResources.MengGuiJieTipWnd)
    end), false)

    CommonDefs.AddOnClickListener(self.cancelBtn.gameObject, DelegateFactory.Action_GameObject(function (go)
        Gac2Gas.CancelSignUpHalloween2020ArrestPlay()
        CUIManager.CloseUI(CLuaUIResources.MengGuiJiePiPeiWnd)
    end), false)
end