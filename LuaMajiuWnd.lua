local LuaGameObject=import "LuaGameObject"

local CZuoQiMgr = import "L10.Game.CZuoQiMgr"
local ZuoQi_Setting = import "L10.Game.ZuoQi_Setting"
local CSelectMapiWnd = import "L10.UI.CSelectMapiWnd"
local DefaultItemActionDataSource=import "L10.UI.DefaultItemActionDataSource"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local Gac2Gas = import "L10.Game.Gac2Gas"
local MapiSelectWndType = import "L10.UI.MapiSelectWndType"

CLuaMajiuWnd=class()
RegistClassMember(CLuaMajiuWnd, "m_MapiBtnList")
RegistClassMember(CLuaMajiuWnd, "m_MapiList")

function CLuaMajiuWnd:Awake()
	self.m_MapiBtnList = {}
	self.m_MapiList = {}

	for i = 1, 3 do
		local name = SafeStringFormat3("Anchor/Icon%d", i)
		local btn = self.transform:Find(name).gameObject
		self.m_MapiBtnList[i] = btn

		CommonDefs.AddOnClickListener(btn,DelegateFactory.Action_GameObject(function(go)
            self:OnItemCellClick(go)
        end),false)
	end
end

function CLuaMajiuWnd:OnItemCellClick(gameObject)
	for i = 1, 3 do
		if gameObject == self.m_MapiBtnList[i] then
			if self.m_MapiList[i] then
				local t_name= {}
				t_name[1] = LocalString.GetString("取回")
				local t_action={}
                t_action[1]=function()
                    CItemInfoMgr.CloseItemInfoWnd()
                    Gac2Gas.TakeBackMapiFromMajiu(self.m_MapiList[i])
                end
				local actionSource = DefaultItemActionDataSource.Create(1,t_action,t_name)
                CItemInfoMgr.ShowMajiuMapiInfo(self.m_MapiList[i], actionSource)
                LuaGameObject.GetChildNoGC(self.m_MapiBtnList[i].transform,"SelectedSprite").gameObject:SetActive(true)

			else
				CSelectMapiWnd.sType = MapiSelectWndType.eSelectMajiu
				CUIManager.ShowUI(CUIResources.SelectMapiWnd)
			end
		end
	end
end

function CLuaMajiuWnd:Init()
	if CZuoQiMgr.Inst.m_CurMajiuFurnitureId == 0 then
		return
	end

	self.m_MapiList = {}
	local tbl = {}
	CommonDefs.DictIterate(CZuoQiMgr.Inst.m_MajiuMapiInfo, DelegateFactory.Action_object_object(
		function(engineId, majiuMapiInfo)
			if majiuMapiInfo.FurnitureId == CZuoQiMgr.Inst.m_CurMajiuFurnitureId then
				tbl[majiuMapiInfo.Idx] = majiuMapiInfo
			end
		end 
	))

	for i = 1, 3 do
		self:InitSingleBtn(i, self.m_MapiBtnList[i], tbl[i])
	end
end

function CLuaMajiuWnd:InitSingleBtn(i, btn, majiuMapiInfo)
	local texture = LuaGameObject.GetChildNoGC(btn.transform,"Texture").cTexture
	local selectedSprite = LuaGameObject.GetChildNoGC(btn.transform,"SelectedSprite").sprite
	local mask = LuaGameObject.GetChildNoGC(btn.transform,"Mask").sprite
	if majiuMapiInfo then
		self.m_MapiList[i] = majiuMapiInfo.ZuoQiId
		mask.gameObject:SetActive(false)
		selectedSprite.gameObject:SetActive(false)
		texture.gameObject:SetActive(true)

		local setting = ZuoQi_Setting.GetData()
		local fuseId = majiuMapiInfo.mapiDisplayInfo.FuseId
		local icon = setting.MapiIcon[fuseId]
		if majiuMapiInfo.mapiDisplayInfo.Quality == 4 then
			icon = setting.SpecialMapiIcon[0]
		elseif majiuMapiInfo.mapiDisplayInfo.Quality == 5 then
			icon = setting.SpecialMapiIcon[1]
		end

		texture:LoadMaterial(icon)
	else
		mask.gameObject:SetActive(true)
		selectedSprite.gameObject:SetActive(false)
		texture.gameObject:SetActive(false)
	end
end

function CLuaMajiuWnd:OnMajiuMapiStatusChanged(args)
	self:Init()
end

function CLuaMajiuWnd:OnEnable()
    g_ScriptEvent:AddListener("OnMajiuMapiStatusChanged", self, "OnMajiuMapiStatusChanged")
end

function CLuaMajiuWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnMajiuMapiStatusChanged", self, "OnMajiuMapiStatusChanged")
end

return CLuaMajiuWnd 
