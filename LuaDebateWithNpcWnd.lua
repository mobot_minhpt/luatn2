require("3rdParty/ScriptEvent")
require("common/common_include")
local CMainCamera = import "L10.Engine.CMainCamera"
local UISprite = import "UISprite"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CUIManager = import "L10.UI.CUIManager"
local CUIFx = import "L10.UI.CUIFx"
local UITexture = import "UITexture"
local MessageWndManager = import "L10.UI.MessageWndManager"
local RenderTexture = import "UnityEngine.RenderTexture"
local RenderTextureFormat = import "UnityEngine.RenderTextureFormat"
local RenderTextureReadWrite = import "UnityEngine.RenderTextureReadWrite"
local TextureWrapMode = import "UnityEngine.TextureWrapMode"
local FilterMode = import "UnityEngine.FilterMode"
local Camera = import "UnityEngine.Camera"
local GameObject = import "UnityEngine.GameObject"
local CameraClearFlags = import "UnityEngine.CameraClearFlags"
local LayerDefine = import "L10.Engine.LayerDefine"
local Quaternion = import "UnityEngine.Quaternion"
local Vector3 = import "UnityEngine.Vector3"
local CPreDrawMgr = import "L10.Engine.CPreDrawMgr"
local TweenPosition = import "TweenPosition"
local Animation = import "UnityEngine.Animation"
local YangYuShaoNianQi_Setting = import "L10.Game.YangYuShaoNianQi_Setting"
local YangYuShaoNianQi_JiBian = import "L10.Game.YangYuShaoNianQi_JiBian"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Time = import "UnityEngine.Time"
local TweenWidth = import "TweenWidth"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local BoxCollider = import "UnityEngine.BoxCollider"

LuaDebateWithNpcWnd = class()

RegistChildComponent(LuaDebateWithNpcWnd, "closeBtn", GameObject)
RegistChildComponent(LuaDebateWithNpcWnd, "showTexture", UITexture)
RegistChildComponent(LuaDebateWithNpcWnd, "chooseArea", GameObject)
RegistChildComponent(LuaDebateWithNpcWnd, "bianArea", GameObject)
RegistChildComponent(LuaDebateWithNpcWnd, "contentArea", GameObject)
RegistChildComponent(LuaDebateWithNpcWnd, "liTemplate", GameObject)
RegistChildComponent(LuaDebateWithNpcWnd, "faTemplate", GameObject)
RegistChildComponent(LuaDebateWithNpcWnd, "qinTemplate", GameObject)
RegistChildComponent(LuaDebateWithNpcWnd, "beginNode", GameObject)
RegistChildComponent(LuaDebateWithNpcWnd, "endNode", GameObject)
RegistChildComponent(LuaDebateWithNpcWnd, "fxNode", CUIFx)
RegistChildComponent(LuaDebateWithNpcWnd, "fxNode2", CUIFx)

RegistClassMember(LuaDebateWithNpcWnd, "cameraNode")
RegistClassMember(LuaDebateWithNpcWnd, "cameraScript")
RegistClassMember(LuaDebateWithNpcWnd, "showAnimation")
RegistClassMember(LuaDebateWithNpcWnd, "chooseBianIndex")
RegistClassMember(LuaDebateWithNpcWnd, "dataTable")
RegistClassMember(LuaDebateWithNpcWnd, "playerScore")
RegistClassMember(LuaDebateWithNpcWnd, "npcScore")
RegistClassMember(LuaDebateWithNpcWnd, "npcBianText")
RegistClassMember(LuaDebateWithNpcWnd, "playerBianText")
RegistClassMember(LuaDebateWithNpcWnd, "triggerState")
RegistClassMember(LuaDebateWithNpcWnd, "triggerTime")
RegistClassMember(LuaDebateWithNpcWnd, "startTime")
RegistClassMember(LuaDebateWithNpcWnd, "totalTime")
RegistClassMember(LuaDebateWithNpcWnd, "bianAnimation")
RegistClassMember(LuaDebateWithNpcWnd, "m_Tick")
RegistClassMember(LuaDebateWithNpcWnd, "m_MoveTick")
RegistClassMember(LuaDebateWithNpcWnd, "m_EndTick")
RegistClassMember(LuaDebateWithNpcWnd, "m_DelayShowTick")
RegistClassMember(LuaDebateWithNpcWnd, "m_CameraMovePos")

function LuaDebateWithNpcWnd:Split(str, pat)
   local t = {}  -- NOTE: use {n = 0} in Lua-5.0
   local fpat = "(.-)" .. pat
   local last_end = 1
   local s, e, cap = str:find(fpat, 1)
   while s do
      if s ~= 1 or cap ~= "" then
         table.insert(t,cap)
      end
      last_end = e+1
      s, e, cap = str:find(fpat, last_end)
   end
   if last_end <= #str then
      cap = str:sub(last_end)
      table.insert(t, cap)
   end
   return t
end

function LuaDebateWithNpcWnd:LeaveWnd()
	CUIManager.CloseUI(CLuaUIResources.DebateWithNpcWnd)
end

function LuaDebateWithNpcWnd:Init()
	local onCloseClick = function(go)
		MessageWndManager.ShowOKCancelMessage(LocalString.GetString("确定要退出吗？"), DelegateFactory.Action(function ()
				Gac2Gas.ReportBianLunResult(false)
				self:LeaveWnd()
		end), nil, nil, nil, false)
	end
  CommonDefs.AddOnClickListener(self.closeBtn,DelegateFactory.Action_GameObject(onCloseClick),false)

	self.liTemplate:SetActive(false)
	self.faTemplate:SetActive(false)
	self.qinTemplate:SetActive(false)
	--CClientFurnitureMgr.Inst:HideSceneModels(6)

	CPreDrawMgr.m_bEnableRectPreDraw = false

	local screenWidth = CommonDefs.GameScreenWidth
  local screenHeight = CommonDefs.GameScreenHeight
	if 1920 / screenWidth > 1080 / screenHeight then
		self.showTexture.width = 1920--Screen.width
		self.showTexture.height = screenHeight * 1920 / screenWidth--Screen.height
	else
		self.showTexture.width = screenWidth * 1080 / screenHeight--Screen.width
		self.showTexture.height = 1080--Screen.height
	end

	local renderTex = RenderTexture(self.showTexture.width,self.showTexture.height,32,RenderTextureFormat.ARGB32,RenderTextureReadWrite.Default)
	renderTex.wrapMode = TextureWrapMode.Clamp
	renderTex.filterMode = FilterMode.Bilinear
	renderTex.anisoLevel = 2
	self.showTexture.mainTexture = renderTex

	self.cameraNode = GameObject("__DebateWithNpcCamera__")
	local cameraScript = CommonDefs.AddCameraComponent(self.cameraNode)
    CMainCamera.Inst:SetCameraEnableStatus(false,"use_another_camera", false)
	cameraScript.clearFlags = CameraClearFlags.Skybox
  --cameraScript.backgroundColor = Color(49 / 255, 77 / 255, 121 / 255, 5 / 255)
  cameraScript.cullingMask = 2^LayerDefine.ModelForNGUI_3D + 2^LayerDefine.Effect_3D + 2^LayerDefine.Default + 2^LayerDefine.Water + 2^LayerDefine.WaterReflection + 2^LayerDefine.Terrain + 2^LayerDefine.NPC + 2^LayerDefine.MainPlayer
  cameraScript.orthographic = false
  cameraScript.fieldOfView = 60
  cameraScript.farClipPlane = 40
  cameraScript.nearClipPlane = 0.1
  cameraScript.depth = -10
  cameraScript.transform.parent = CUIManager.instance.transform
	--TODO
	local cameraDataTable = self:Split(YangYuShaoNianQi_Setting.GetData().Camera,';')

	if cameraDataTable then
		local positionTable = self:Split(cameraDataTable[1],',')
		local rotateTable = self:Split(cameraDataTable[2],',')
		local endPositionTable = self:Split(cameraDataTable[3],',')
		self.cameraNode.transform.position = Vector3(tonumber(positionTable[1]),tonumber(positionTable[2]),tonumber(positionTable[3]))
		self.cameraNode.transform.localRotation = Quaternion.Euler(tonumber(rotateTable[1]),tonumber(rotateTable[2]),tonumber(rotateTable[3]))
    self.m_CameraMovePos = Vector3(tonumber(endPositionTable[1]),tonumber(endPositionTable[2]),tonumber(endPositionTable[3]))
	else
		self.cameraNode.transform.position = Vector3(28.9,13.1,72.5)
    --TweenPosition.Begin(self.cameraNode, 1, Vector3(29,12,71.5))
		self.cameraNode.transform.localRotation = Quaternion.Euler(16.8,90,0)
    self.m_CameraMovePos = Vector3(29,12,71.5)
	end
  --cameraScript.transform.localScale = Vector3.one
	cameraScript.targetTexture = renderTex
	self.cameraScript = cameraScript

	self.showAnimation = self.chooseArea:GetComponent(typeof(Animation))
	self.bianAnimation = self.bianArea:GetComponent(typeof(Animation))
	self:InitData()
	self:InitAnimation()
	self:ResetPanelInfo()
	self.playerScore = 0
	self.npcScore = 0
	self:SetScore(false)

	if CClientMainPlayer.Inst then
		self.contentArea.transform:Find("playerName"):GetComponent(typeof(UILabel)).text = CClientMainPlayer.Inst.Name
	else
		self.contentArea.transform:Find("playerName"):GetComponent(typeof(UILabel)).text = ""
	end

	self.npcBianText = {}
	self.playerBianText = {}
	self.npcBianText[YangYuShaoNianQi_Setting.GetData().Qing] = self.bianArea.transform:Find("npc/typeNode/qinText").gameObject
	self.npcBianText[YangYuShaoNianQi_Setting.GetData().Li] = self.bianArea.transform:Find("npc/typeNode/liText").gameObject
	self.npcBianText[YangYuShaoNianQi_Setting.GetData().Fa] = self.bianArea.transform:Find("npc/typeNode/faText").gameObject
	self.playerBianText[YangYuShaoNianQi_Setting.GetData().Qing] = self.bianArea.transform:Find("self/typeNode/qinText").gameObject
	self.playerBianText[YangYuShaoNianQi_Setting.GetData().Li] = self.bianArea.transform:Find("self/typeNode/liText").gameObject
	self.playerBianText[YangYuShaoNianQi_Setting.GetData().Fa] = self.bianArea.transform:Find("self/typeNode/faText").gameObject

	self:InitBianBtn()

	self.startTime = Time.time
	self.totalTime = YangYuShaoNianQi_Setting.GetData().TimeLimit
end

function LuaDebateWithNpcWnd:CheckResult()
	local scoreDes = YangYuShaoNianQi_Setting.GetData().WinScore

	if scoreDes <= self.playerScore or scoreDes <= self.npcScore then
		if self.m_Tick then
			UnRegisterTick(self.m_Tick)
		end
		if self.m_EndTick then
			UnRegisterTick(self.m_EndTick)
		end
		if scoreDes <= self.playerScore then
			self.m_EndTick = RegisterTickWithDuration(function()
				EventManager.BroadcastInternalForLua(EnumEventType.OnLoadCommonUIFx, {CUIFxPaths.ShengliFxPath})
        TweenPosition.Begin(self.cameraNode, 1, self.m_CameraMovePos)--Vector3(29,12,71.5))
				Gac2Gas.ReportBianLunResult(true)
			end,2000,2000)
			self.m_Tick = RegisterTickWithDuration(function ()
				self:LeaveWnd()
			end,6000,6000)
			return true
		elseif scoreDes <= self.npcScore then
			self.m_EndTick = RegisterTickWithDuration(function()
				EventManager.BroadcastInternalForLua(EnumEventType.OnLoadCommonUIFx, {CUIFxPaths.ShibaiFxPath})
				TweenPosition.Begin(self.cameraNode, 1, self.m_CameraMovePos)--Vector3(29,12,71.5))
				Gac2Gas.ReportBianLunResult(false)
			end,2000,2000)
			self.m_Tick = RegisterTickWithDuration(function ()
				self:LeaveWnd()
			end,6000,6000)
			return true
		end
	end
	return false
end

function LuaDebateWithNpcWnd:SetScore(playFx)
	local scoreLabel = self.contentArea.transform:Find("score"):GetComponent(typeof(UILabel))
	local oldText = scoreLabel.text
	local newText = self.npcScore .. ":" .. self.playerScore
	if playFx and oldText ~= newText then
		self.fxNode:DestroyFx()
		self.fxNode:LoadFx("Fx/UI/Prefab/UI_bianlunxiaojufenshu.prefab")
		self.fxNode2:DestroyFx()
		self.fxNode2:LoadFx("Fx/UI/Prefab/UI_bianlunxiaojuhuosheng.prefab")
		if self.m_MoveTick then
			UnRegisterTick(self.m_MoveTick)
		end
		self.m_MoveTick = RegisterTickWithDuration(function ()
			scoreLabel.text = newText
		end,1800,1800)
	else
		scoreLabel.text = newText
	end
	return self:CheckResult()
end

function LuaDebateWithNpcWnd:InitData()
	--LuaDebateWithNpcMgr.TaskId = 22202176 --TEST
	local count = YangYuShaoNianQi_JiBian.GetDataCount()
	local dataTable = {}
	for i=1,count do
		local data = YangYuShaoNianQi_JiBian.GetData(i)
		if data then
			if data.TaskID and data.TaskID[0] and data.TaskID[1] then
				if LuaDebateWithNpcMgr.TaskId and LuaDebateWithNpcMgr.TaskId == data.TaskID[0] or LuaDebateWithNpcMgr.TaskId == data.TaskID[1] then
					local npcTable = {}
					local playerTable = {}
					npcTable[YangYuShaoNianQi_Setting.GetData().Qing] = data.NpcQingWords
					npcTable[YangYuShaoNianQi_Setting.GetData().Li] = data.NpcLiWords
					npcTable[YangYuShaoNianQi_Setting.GetData().Fa] = data.NpcFaWords
					playerTable[YangYuShaoNianQi_Setting.GetData().Qing] = data.PlayerQingWords
					playerTable[YangYuShaoNianQi_Setting.GetData().Li] = data.PlayerLiWords
					playerTable[YangYuShaoNianQi_Setting.GetData().Fa] = data.PlayerFaWords
					table.insert(dataTable,{npc = npcTable,player = playerTable})
				end
			end
		end
	end
	self.dataTable = dataTable
end

function LuaDebateWithNpcWnd:PlayChooseAni(sign)
	local chooseAni = self.bianAnimation
	local clipName = ''
	if sign == 1 then
		clipName = "DebateWithNpc_Result_Win"
	elseif sign == -1 then
		clipName = "DebateWithNpc_Result_Lose"
	else
		clipName = "DebateWithNpc_Result_Draw"
	end
	local aniClip = chooseAni:get_Item(clipName)
	if aniClip then
		aniClip.time = 0
		chooseAni:Play(clipName)
	end
end

function LuaDebateWithNpcWnd:ShowChooseInfo()
	local chooseType = self.chooseBianIndex
	if not chooseType or chooseType == 0 then
		return
	end
	--self.bianArea:SetActive(true)
	local selfTranBg = self.bianArea.transform:Find('self/tranBg')
	local npcTranBg = self.bianArea.transform:Find('npc/tranBg')
	selfTranBg:GetComponent(typeof(TweenWidth)).enabled = true
	npcTranBg:GetComponent(typeof(TweenWidth)).enabled = true
	selfTranBg:GetComponent(typeof(TweenWidth)):ResetToBeginning()
	npcTranBg:GetComponent(typeof(TweenWidth)):ResetToBeginning()
	selfTranBg:GetComponent(typeof(UISprite)).width = 0
	npcTranBg:GetComponent(typeof(UISprite)).width = 0
	selfTranBg:GetComponent(typeof(TweenWidth)):PlayForward()
	npcTranBg:GetComponent(typeof(TweenWidth)):PlayForward()
	local quesData = self.dataTable[math.random(#self.dataTable)]
	local npcChoose = math.random(3)
	for i,v in pairs(self.npcBianText) do
		if i == npcChoose then
			v:SetActive(true)
		else
			v:SetActive(false)
		end
	end
	for i,v in pairs(self.playerBianText) do
		if i == chooseType then
			v:SetActive(true)
		else
			v:SetActive(false)
		end
	end
	self.bianArea.transform:Find("self/text"):GetComponent(typeof(UILabel)).text = quesData.player[chooseType]
	self.bianArea.transform:Find("npc/text"):GetComponent(typeof(UILabel)).text = quesData.npc[npcChoose]

	if chooseType == 3 and npcChoose == 1 then
		self.playerScore = self.playerScore + 1
		self:PlayChooseAni(1)
	elseif chooseType == 1 and npcChoose == 3 then
		self.npcScore = self.npcScore + 1
		self:PlayChooseAni(-1)
	elseif chooseType < npcChoose then
		self.playerScore = self.playerScore + 1
		self:PlayChooseAni(1)
	elseif chooseType > npcChoose then
		self.npcScore = self.npcScore + 1
		self:PlayChooseAni(-1)
	else
		--draw
		self:PlayChooseAni(0)
	end

	self.triggerTime = Time.time + 4.5
	self.triggerState = 'showResult'
end
function LuaDebateWithNpcWnd:ChooseOption(chooseType)
	self.chooseBianIndex = chooseType
	self.triggerTime = Time.time + 1
	self.triggerState = 'showContent'
	self:SetChooseAreaClick(false)

	local moveNode = nil
	local faBtn = self.chooseArea.transform:Find("fa/btn").gameObject
	local liBtn = self.chooseArea.transform:Find("li/btn").gameObject
	local qinBtn = self.chooseArea.transform:Find("qin/btn").gameObject

	if chooseType == YangYuShaoNianQi_Setting.GetData().Qing then
		moveNode = self.qinTemplate
		self.beginNode.transform.position = qinBtn.transform.position
	elseif chooseType == YangYuShaoNianQi_Setting.GetData().Li then
		moveNode = self.liTemplate
		self.beginNode.transform.position = liBtn.transform.position
	elseif chooseType == YangYuShaoNianQi_Setting.GetData().Fa then
		moveNode = self.faTemplate
		self.beginNode.transform.position = faBtn.transform.position
	end

	if moveNode then
		moveNode:SetActive(true)
		moveNode.transform.localPosition = self.beginNode.transform.localPosition
		local tweenPos = TweenPosition.Begin(moveNode, 0.7, self.endNode.transform.localPosition)
		local tweenFinish = function()
			moveNode:SetActive(false)
		end
		CommonDefs.AddEventDelegate(tweenPos.onFinished, DelegateFactory.Action(tweenFinish))
	end
end

function LuaDebateWithNpcWnd:Update()
	if self.startTime and self.totalTime then
		local restTime = self.startTime + self.totalTime - Time.time
		if restTime <= 0 then
			Gac2Gas.ReportBianLunResult(false)
			self:LeaveWnd()
			return
		else
			local timeLabel = self.contentArea.transform:Find('timeLabel'):GetComponent(typeof(UILabel))
			local min = restTime/60
			min = math.floor(min)
			local sec = restTime - min * 60
			sec = math.floor(sec)
			if min < 10 then
				min = '0'..min
			end
			if sec < 10 then
				sec = '0'..sec
			end
			timeLabel.text = min..':'..sec
		end
	end

	if not self.triggerState or self.triggerState == 'idle' then
		return
	end
	if Time.time > self.triggerTime then
		if self.triggerState == 'showResult' then
			self.triggerState = 'idle'
			local result = self:SetScore(true)
			if not result then
				if self.m_DelayShowTick then
					UnRegisterTick(self.m_DelayShowTick)
				end
				self.m_DelayShowTick = RegisterTickWithDuration(function ()
					self:ResetPanelInfo()
				end,500,500)
			end
		elseif self.triggerState == 'showContent' then
			self.triggerState = 'idle'
			self:ShowChooseInfo()
		end
	end
end

function LuaDebateWithNpcWnd:InitBianBtn()
	self.triggerState = 'idle'
	local faBtn = self.chooseArea.transform:Find("fa/btn").gameObject
	local liBtn = self.chooseArea.transform:Find("li/btn").gameObject
	local qinBtn = self.chooseArea.transform:Find("qin/btn").gameObject
  CommonDefs.AddOnClickListener(faBtn,DelegateFactory.Action_GameObject(function(go)
		self:ChooseOption(YangYuShaoNianQi_Setting.GetData().Fa)
	end),false)
	CommonDefs.AddOnClickListener(liBtn,DelegateFactory.Action_GameObject(function(go)
		self:ChooseOption(YangYuShaoNianQi_Setting.GetData().Li)
	end),false)
	CommonDefs.AddOnClickListener(qinBtn,DelegateFactory.Action_GameObject(function(go)
		self:ChooseOption(YangYuShaoNianQi_Setting.GetData().Qing)
	end),false)
end

function LuaDebateWithNpcWnd:InitAnimation()
	local chooseAni = self.showAnimation
	local clipName = "DebateWithNpc_Show"
	local aniClip = chooseAni:get_Item(clipName)
	local aniClip2 = chooseAni:get_Item('DebateWithNpc_Close')
	if aniClip then
		aniClip.time = 0
		aniClip.enabled = true
		aniClip.weight = 1
		aniClip2.time = 0
		aniClip2.enabled = true
		aniClip2.weight = 1
		chooseAni:Sample()
		aniClip.enabled = false
		aniClip2.enabled = false
	end

	local bianAni1 = self.bianAnimation['DebateWithNpc_Result_Win']
	local bianAni2 = self.bianAnimation['DebateWithNpc_Result_Lose']
	local bianAni3 = self.bianAnimation['DebateWithNpc_Result_Draw']
	if bianAni1 and bianAni2 then
		bianAni1.time = 0
		bianAni1.enabled = true
		bianAni1.weight = 1
		bianAni2.time = 0
		bianAni2.enabled = true
		bianAni2.weight = 1
		bianAni3.time = 0
		bianAni3.enabled = true
		bianAni3.weight = 1
		self.bianAnimation:Sample()
		bianAni1.enabled = false
		bianAni2.enabled = false
		bianAni3.enabled = false
	end

end

function LuaDebateWithNpcWnd:SetChooseAreaClick(sign)
	self.chooseArea.transform:Find('fa/btn'):GetComponent(typeof(BoxCollider)).enabled = sign
	self.chooseArea.transform:Find('li/btn'):GetComponent(typeof(BoxCollider)).enabled = sign
	self.chooseArea.transform:Find('qin/btn'):GetComponent(typeof(BoxCollider)).enabled = sign
	local chooseAni = self.showAnimation
	local clipName = ""
	if sign then
		clipName = "DebateWithNpc_Show"
	else
		clipName = "DebateWithNpc_Close"
	end

	local aniClip = chooseAni:get_Item(clipName)
	if aniClip then
		aniClip.time = 0
		chooseAni:Play(clipName)
	end
end

function LuaDebateWithNpcWnd:ResetPanelInfo()
	--self.bianArea:SetActive(false)
	local selfTranBg = self.bianArea.transform:Find('self/tranBg')
	local npcTranBg = self.bianArea.transform:Find('npc/tranBg')
	selfTranBg:GetComponent(typeof(UISprite)).width = 0
	npcTranBg:GetComponent(typeof(UISprite)).width = 0
	selfTranBg:GetComponent(typeof(TweenWidth)).enabled = false
	npcTranBg:GetComponent(typeof(TweenWidth)).enabled = false
	self:SetChooseAreaClick(true)
end

function LuaDebateWithNpcWnd:OnDestroy()
    CMainCamera.Inst:SetCameraEnableStatus(true,"use_another_camera", false)
	CPreDrawMgr.m_bEnableRectPreDraw = true
	if self.cameraNode then
		GameObject.Destroy(self.cameraNode)
	end
	if self.m_Tick then
		UnRegisterTick(self.m_Tick)
	end
	if self.m_MoveTick then
		UnRegisterTick(self.m_MoveTick)
	end
	if self.m_EndTick then
		UnRegisterTick(self.m_EndTick)
	end
	if self.m_DelayShowTick then
		UnRegisterTick(self.m_DelayShowTick)
	end
end

return LuaDebateWithNpcWnd
