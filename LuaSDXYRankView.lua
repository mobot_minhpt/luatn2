local QnTableView = import "L10.UI.QnTableView"
local UILabel = import "UILabel"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local GameObject = import "UnityEngine.GameObject"

LuaSDXYRankView = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaSDXYRankView, "TableView", "TableView", QnTableView)
RegistChildComponent(LuaSDXYRankView, "EmptyTipLabel", "EmptyTipLabel", UILabel)
RegistChildComponent(LuaSDXYRankView, "ScrollView", "ScrollView", CUIRestrictScrollView)
RegistChildComponent(LuaSDXYRankView, "MyRankItem", "MyRankItem", GameObject)

--@endregion RegistChildComponent end

function LuaSDXYRankView:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    --LuaChristmas2021Mgr.PlayerRankTable = {}
    self.EmptyTipLabel.gameObject:SetActive(true)
    self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function() 
            return LuaChristmas2021Mgr.PlayerRankTable and #LuaChristmas2021Mgr.PlayerRankTable or 0
        end,
        function(item,row)
            self:InitItem(item,row)
        end)
    self:Refresh()
end

function LuaSDXYRankView:Refresh()
    if not LuaChristmas2021Mgr.PlayerRankTable then
        self.EmptyTipLabel.gameObject:SetActive(true)
    elseif not next(LuaChristmas2021Mgr.PlayerRankTable) then
        self.EmptyTipLabel.gameObject:SetActive(true)
    else
        self.EmptyTipLabel.gameObject:SetActive(false)
    end
    self.TableView:ReloadData(false, false)
    self.ScrollView:ResetPosition()

    --my rank data
    self:InitItem(self.MyRankItem,nil)

    if not LuaChristmas2021Mgr.PlayerRankTable or #LuaChristmas2021Mgr.PlayerRankTable == 0 then
        self.MyRankItem:SetActive(false)
    else
        self.MyRankItem:SetActive(true)
    end
end

function LuaSDXYRankView:InitItem(item,row)
    local rankLabel = item.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
    local rankSprite = item.transform:Find("RankLabel/RankSprite"):GetComponent(typeof(UISprite))
    local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local timeLabel = item.transform:Find("TimeLabel"):GetComponent(typeof(UILabel))
    local bg1 = item.transform:Find("bg1").gameObject
    local bg2 = item.transform:Find("bg2").gameObject
    local isMyItem = not row and true or false
    local info
    if not isMyItem then
        bg1:SetActive(row % 2 == 0)
        bg2:SetActive(row % 2 ~= 0)
        info = LuaChristmas2021Mgr.PlayerRankTable[row+1]
    else
        info = LuaChristmas2021Mgr.MyXingYuRankInfo
    end

    if info then 
        local rank = info.rank
        rankLabel.text = rank
        if rank <= 3 then
            rankSprite.gameObject:SetActive(true)
            rankSprite.spriteName = CLuaQingMing2020Mgr.RankSprites[rank]
        else
            rankSprite.gameObject:SetActive(false)
        end

        nameLabel.text = info.playerName
        timeLabel.text = LuaChristmas2021Mgr.GetRemainTimeText(info.duration)
    elseif isMyItem then
        --未上榜
        rankSprite.gameObject:SetActive(false)
        rankLabel.text = LocalString.GetString("未上榜")
        nameLabel.text = CClientMainPlayer.Inst.RealName
        timeLabel.text = "-"
    end
end

--@region UIEvent

--@endregion UIEvent

