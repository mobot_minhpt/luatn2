local CScene = import "L10.Game.CScene"

LuaValentine2023Mgr = {}

--#region 一往情深

LuaValentine2023Mgr.YWQSInfo = {}

function LuaValentine2023Mgr:OpenYWQSSelectWnd(type, go, awardCount)
    self.YWQSInfo.selectType = type
    self.YWQSInfo.selectGo = go
    self.YWQSInfo.awardCount = awardCount
    CUIManager.ShowUI(CLuaUIResources.Valentine2023YWQSSelectWnd)
end

-- 从npc处打开投票界面
function LuaValentine2023Mgr:ShowValentine2023YWQSVoteWnd(data)
    local votePlayerId = g_MessagePack.unpack(data)[1]
    self:OpenYWQSVoteWnd(votePlayerId)
end

function LuaValentine2023Mgr:OpenYWQSVoteWnd(votePlayerId)
    self.YWQSInfo.votePlayerId = votePlayerId
    CUIManager.ShowUI(CLuaUIResources.Valentine2023YWQSVoteWnd)
end

-- 当前是否在一往情深场景
function LuaValentine2023Mgr:IsInYWQSScene()
    if CScene.MainScene and CScene.MainScene.SceneTemplateId == Valentine2023_YWQS_Setting.GetData().PlayMapId then
        return true
    end
    return false
end

function LuaValentine2023Mgr:SendValentineYWQSInfo(partnerId, partnerIsOnline, loveValue, rank, todayHuoLi, expressionTaskCount, dateTaskCount, shareMengDaoCount, playerAppearanceInfo)
    g_ScriptEvent:BroadcastInLua("SendValentineYWQSInfo", partnerId, partnerIsOnline, loveValue, rank, todayHuoLi, expressionTaskCount, dateTaskCount, shareMengDaoCount, playerAppearanceInfo)
end

function LuaValentine2023Mgr:SendValentineYWQSInvitation(partnerPlayerId, partnerPlayerName)
    MessageWndManager.ShowDelayOKCancelMessage(g_MessageMgr:FormatMessage("VALENTINE2023_YIWANGQINGSHEN_ACCEPT_CONFIRM", partnerPlayerName), DelegateFactory.Action(function ()
        Gac2Gas.ReplyValentineYWQSInvitation(partnerPlayerId, true)
    end), DelegateFactory.Action(function ()
        Gac2Gas.ReplyValentineYWQSInvitation(partnerPlayerId, false)
    end), Valentine2023_YWQS_Setting.GetData().AcceptConfirmDelay, LocalString.GetString("接受"), LocalString.GetString("再想想"))
end

function LuaValentine2023Mgr:SendValentineYWQSRankInfo(partnerId, partnerName, partnerClass, partnerGender, loveValue, voteCount, myRank, rankInfo)
    g_ScriptEvent:BroadcastInLua("SendValentineYWQSRankInfo", partnerId, partnerName, partnerClass, partnerGender, loveValue, voteCount, myRank, rankInfo)
end

function LuaValentine2023Mgr:CloseYWQSRankWnd()
    g_ScriptEvent:BroadcastInLua("CloseYWQSRankWnd")
end

-- 打开一往情深表情界面
function LuaValentine2023Mgr:ShowYWQSExpressionWnd(go)
    self.YWQSInfo.lovingShowButton = go
    CUIManager.ShowUI(CLuaUIResources.Valentine2023YWQSExpressionWnd)
end

function LuaValentine2023Mgr:ShowValentine2023YWQSWnd(isServerCall)
    if LuaValentine2023Mgr.YWQSInfo.isYWQSOpen and isServerCall then
        g_ScriptEvent:BroadcastInLua("ValentineYWQSPlaySwitch")
        return
    end

    self.YWQSInfo.defaultView = "YWQS"
    CUIManager.ShowUI(CLuaUIResources.Valentine2023Wnd)
end

function LuaValentine2023Mgr:OnMainPlayerCreated()
    LuaGamePlayMgr:SetSkillButtonBoardState({
        showGuaJiBtn = false,
        showSwitchTargetBtn = true,
        showHpRecoverBtn = true,
    }, true)
end

function LuaValentine2023Mgr:SyncValentineYWQSTop10(rankInfo)
    rankInfo = rankInfo and g_MessagePack.unpack(rankInfo)
    local dict = {}
    for engineId, rank in pairs(rankInfo) do
        dict[engineId] = {
            atlas = "MainUI",
            spriteName = "personalspacewnd_heart_2",
            label = tostring(rank),
        }
    end
    LuaGamePlayMgr:SetPlayerHeadSpeIconInfos(dict, true, true)
    LuaGamePlayMgr:SetNpcHeadSpeIconInfos(dict, true)
end

--#endregion 一往情深

--#region 灵谷探秘

LuaValentine2023Mgr.LGTMInfo = {}

function LuaValentine2023Mgr:SendValentineLGTMPlayInfo(remainEasyModeRewardTimes, remainHardModeRewardTimes)
    g_ScriptEvent:BroadcastInLua("SendValentineLGTMPlayInfo", remainEasyModeRewardTimes, remainHardModeRewardTimes)
end

function LuaValentine2023Mgr:SendLGTMPlayResult(isWin, isHardMode)
    self.LGTMInfo.isWin = isWin
    self.LGTMInfo.isHardMode = isHardMode
    CUIManager.ShowUI(CLuaUIResources.Valentine2023LGTMResultWnd)
end

function LuaValentine2023Mgr:GetEasyLGTMGameplayId()
    return Valentine2023_LGTM_Setting.GetData().EasyLingGuTanMiGameplayId
end

function LuaValentine2023Mgr:GetHardLGTMGameplayId()
    return Valentine2023_LGTM_Setting.GetData().HardLingGuTanMiGameplayId
end

function LuaValentine2023Mgr:OnRuleButtonClick()
    g_MessageMgr:ShowMessage("VALENTINE2023_LINGGUTANMI_RULE")
end

function LuaValentine2023Mgr:StartLGTMFlyProgress(rangeStart, rangeWidth)
    self.LGTMInfo.rangeStart = rangeStart / 1000
    self.LGTMInfo.rangeWidth = rangeWidth / 1000
    CUIManager.ShowUI(CLuaUIResources.Valentine2023LGTMProgressWnd)
end

function LuaValentine2023Mgr:PlayerDoLGTMFlySkill(doSkillPos, isSucc)
    g_ScriptEvent:BroadcastInLua("PlayerDoLGTMFlySkill", doSkillPos, isSucc)
end

function LuaValentine2023Mgr:LeaveJiaHeFly()
    CUIManager.CloseUI(CLuaUIResources.Valentine2023LGTMProgressWnd)
end

--#endregion 灵谷探秘


-- 打开情人节主界面
function LuaValentine2023Mgr:ShowValentine2023MainWnd()
    self.YWQSInfo.defaultView = "Main"
    CUIManager.ShowUI(CLuaUIResources.Valentine2023Wnd)
end
