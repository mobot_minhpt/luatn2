local Extensions = import "Extensions"
local UISlider=import "UISlider"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CUITexture=import "L10.UI.CUITexture"


CLuaPlayerCapacityView = class()
RegistClassMember(CLuaPlayerCapacityView,"playerCapacityLabel")
RegistClassMember(CLuaPlayerCapacityView,"template")
RegistClassMember(CLuaPlayerCapacityView,"m_Table")
RegistClassMember(CLuaPlayerCapacityView,"items")
RegistClassMember(CLuaPlayerCapacityView,"m_Keys")
RegistClassMember(CLuaPlayerCapacityView,"inited")

CLuaPlayerCapacityView.s_ShowEquipScoreImproveWnd = true

function CLuaPlayerCapacityView:Awake()
    if CommonDefs.IS_VN_CLIENT then
        self.transform:Find("ScrollView/Template/ExpandButton").transform.localPosition = Vector3(-360, -49.4, 0)
    end
    
    self.playerCapacityLabel = self.transform:Find("Header/PlayerCapacityLabel"):GetComponent(typeof(UILabel))
    self.template = self.transform:Find("ScrollView/Template").gameObject
    self.template:SetActive(false)
    self.m_Table = self.transform:Find("ScrollView/Table"):GetComponent(typeof(UITable))
    self.items = {}
    self.inited = false
    self.m_Keys={}

end

function CLuaPlayerCapacityView:OnEnable()
    self:Refresh()
	g_ScriptEvent:AddListener("UpdatePlayerCapacity", self, "UpdatePlayerCapacity")

end
function CLuaPlayerCapacityView:OnDisable()
	g_ScriptEvent:RemoveListener("UpdatePlayerCapacity", self, "UpdatePlayerCapacity")
end
function CLuaPlayerCapacityView:UpdatePlayerCapacity( id, capacity, detailData) 
    -- local id, capacity, detailData = args[0],args[1],args[2]
    self:Refresh()
end

function CLuaPlayerCapacityView:Init( )
    if self.inited then
        return
    end
    self.inited = true
    Extensions.RemoveAllChildren(self.m_Table.transform)
    self.items = {}
    self.m_Keys={}

    for i,v in ipairs(CLuaPlayerCapacityMgr.m_ModuleDef) do
    if not v.ignore then 
        local go = CUICommonDef.AddChild(self.m_Table.gameObject, self.template)
        go:SetActive(true)
        self:InitItem(go.transform,i,v)
        table.insert( self.items,go )
        table.insert( self.m_Keys,i )
    end
    end

    self.m_Table:Reposition()
    self.playerCapacityLabel.text = nil
end
function CLuaPlayerCapacityView:Refresh( )
    self:Init()
    if CClientMainPlayer.Inst ~= nil then
        self.playerCapacityLabel.text = tostring(CClientMainPlayer.Inst.PlayProp.PlayerCapacity)
        for i,v in ipairs(self.items) do
            self:RefreshItem(v.transform,CLuaPlayerCapacityMgr.m_ModuleDef[self.m_Keys[i]])
        end
    end
end

function CLuaPlayerCapacityView:InitItem(transform,idx,data)
    local icon = transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local descLabel = transform:Find("DescLabel"):GetComponent(typeof(UILabel))

    local btn = transform:Find("Btn").gameObject
    
    icon:LoadMaterial(PlayerCapacity_Tooltip.GetData(idx).Icon)
    descLabel.text = data.name
    self:RefreshItem(transform,data)
end

function CLuaPlayerCapacityView:RefreshItem(transform,data)
    local background = transform:GetComponent(typeof(UISprite))
    local icon = transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local descLabel = transform:Find("DescLabel"):GetComponent(typeof(UILabel))
    local curValLabel = transform:Find("CurValLabel"):GetComponent(typeof(UILabel))
    local recommendLabel = transform:Find("Slider2/Thumb/RecommendLabel"):GetComponent(typeof(UILabel))
    local recommendValLabel = transform:Find("Slider2/Thumb/RecommendValLabel"):GetComponent(typeof(UILabel))
    local btn = transform:Find("Btn").gameObject
    local split = transform:Find("Sprite_bg").gameObject
    split:SetActive(false)


    local slider = transform:Find("Slider"):GetComponent(typeof(UISlider))
    local slider2 = transform:Find("Slider2"):GetComponent(typeof(UISlider))
    local slider3 = transform:Find("Slider3"):GetComponent(typeof(UISlider))
    local thumb = transform:Find("Slider2/Thumb").gameObject

    -- local SetRecommendVal = function( isHigh, val) 
    --     if not isHigh then
    --         recommendLabel.text = SafeStringFormat3("[c][ff0000]%s[-][/c]", LocalString.GetString("推荐战力"))
    --         recommendValLabel.text = SafeStringFormat3("[c][ff0000]%s[-][/c]",tostring(val))
    --     else
    --         recommendLabel.text =  SafeStringFormat3("[c][fffa00]%s[-][/c]", LocalString.GetString("更强战力"))
    --         recommendValLabel.text = SafeStringFormat3("[c][fffa00]%s[-][/c]",tostring(val))
    --     end
    -- end

    --取最低的那个
    local haveRecommend, isHigh,recommendVal =false,false,0
    local haveRecommendMin, isHighMin =false,true

    local sumCurVal = 0
    local sum = {0,0,0}
    for i,v in ipairs(data.sub) do
        local pass = true
        if v.condition then pass = v.condition() end--判断一下是否显示
        if pass then
            local subkey = v[1]
            local curVal = CLuaPlayerCapacityMgr.GetValue(subkey)
            sumCurVal = sumCurVal+curVal
            local _haveRecommend, _isHigh, _recommendVal,percent,v1,v2,v3 = CLuaPlayerCapacityMgr.GetRecommendValue(subkey, curVal)
            -- if curVal>v3 then
            --     print(subkey,curVal,v3)
            -- end
            sum[1]=sum[1]+v1
            sum[2]=sum[2]+v2
            sum[3]=sum[3]+v3

            -- recommendVal = _recommendVal+recommendVal

            if _haveRecommend then haveRecommendMin = true end
            if not _isHigh then isHighMin = false end
        end
    end

    local percent2=0
    if sumCurVal<sum[1] then
        haveRecommend=true
        isHigh=false
        percent2 = sum[1]/sum[3]
        recommendVal = sum[1]
    elseif sumCurVal<sum[2] then
        haveRecommend=true
        isHigh=true
        percent2 = sum[2]/sum[3]
        recommendVal = sum[2]
    else
        haveRecommend=false
        percent2=1
    end

    if sum[3]==0 then
        slider.value = 0
    else
        slider.value = sumCurVal/sum[3]
        if CommonDefs.IS_VN_CLIENT then
            slider2.value = math.min(math.max(percent2,0.25),0.84)
        else
            slider2.value = math.min(math.max(percent2,0.16),0.84)
        end
        slider3.value = percent2
    end

    --这里按总的来算
    if haveRecommend then
        slider2.gameObject:SetActive(true)
        slider3.gameObject:SetActive(true)

        if not isHigh then
            recommendLabel.text = SafeStringFormat3("[c][ff0000]%s[-][/c]", LocalString.GetString("推荐战力"))
            recommendValLabel.text = SafeStringFormat3("[c][ff0000]%s[-][/c]",tostring(recommendVal))
        else
            recommendLabel.text =  SafeStringFormat3("[c][fffa00]%s[-][/c]", LocalString.GetString("更强战力"))
            recommendValLabel.text = SafeStringFormat3("[c][fffa00]%s[-][/c]",tostring(recommendVal))
        end
    else
        slider2.gameObject:SetActive(false)
        slider3.gameObject:SetActive(false)
    end

    if haveRecommendMin then
        if not isHighMin then
            curValLabel.text = SafeStringFormat3("[c][ff0000]%s[-][/c]",tostring(sumCurVal))
        else
            curValLabel.text = SafeStringFormat3("[c][fffa00]%s[-][/c]",tostring(sumCurVal))
        end
        btn:SetActive(true)

        UIEventListener.Get(btn).onClick = DelegateFactory.VoidDelegate(function(g)
            CLuaPlayerCapacityRecommendWnd.fromKey = data.key
            CUIManager.ShowUI(CUIResources.PlayerCapacityRecommendWnd)
        end)
    else
        curValLabel.text = SafeStringFormat3("[c][00ff00]%s[-][/c]",tostring(sumCurVal))
        
        btn:SetActive(false)
    end

    local subItems = transform:Find("SubItems").gameObject
    local childItem = transform:Find("ChildItem").gameObject
    childItem:SetActive(false)

    background.height = 170

    local expandButton = transform:Find("ExpandButton").gameObject
    UIEventListener.Get(expandButton).onClick = DelegateFactory.VoidDelegate(function(g)
        if subItems.transform.childCount>0 then
            split:SetActive(false)
            expandButton.transform.localEulerAngles = Vector3(0,0,0)

            Extensions.RemoveAllChildren(subItems.transform)

            background.height = 170
            self.m_Table:Reposition()
        else
            split:SetActive(true)
            expandButton.transform.localEulerAngles = Vector3(0,0,180)

            local subCount = 0
            for i,v in ipairs(data.sub) do
                local pass = true
                if v.condition then pass = v.condition() end--判断一下是否显示
                if pass then
                    local go = NGUITools.AddChild(subItems,childItem)
                    go:SetActive(true)
                    local nameLabel = go.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
                    nameLabel.text = v[2]
                    local valueLabel = go.transform:Find("ValueLabel"):GetComponent(typeof(UILabel))
                    local subKey = v[1]
                    local curVal =  CLuaPlayerCapacityMgr.GetValue(subKey)
                    local alert = go.transform:Find("Label").gameObject
                    alert:SetActive(false)
                    local haveRecommend, isHigh, recommendVal,percent,v1,v2,v3 =CLuaPlayerCapacityMgr.GetRecommendValue(subKey,curVal)

                    if haveRecommend then
                        if isHigh then
                            valueLabel.text = SafeStringFormat3( "[c][fffa00]%d[-][/c]",curVal )
                        else
                            alert:SetActive(true)
                            valueLabel.text = SafeStringFormat3( "[c][ff0000]%d[-][/c]",curVal )
                        end
                    else
                        valueLabel.text = SafeStringFormat3( "[c][00ff00]%d[-][/c]",curVal )
                    end
                    local slider = go.transform:Find("Slider"):GetComponent(typeof(UISlider))
                    --最大值可能是0 特殊处理
                    if v3==0 then
                        slider.value = 0
                    else
                        slider.value = percent
                    end

                    subCount = subCount+1
                end
            end
            subItems:GetComponent(typeof(UIGrid)):Reposition()

            background.height = 170+subCount*70

            self.m_Table:Reposition()
        end
    end)
end
