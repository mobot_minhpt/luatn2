function Gas2Gac.CnvLotteryAddTicketResult(success, lotteryId)

  g_ScriptEvent:BroadcastInLua('CnvLotteryAddSucc')
end

function Gas2Gac.CnvLotterySetLotteryDataResult(success, lotteryId, lotteryData_U)

end

function Gas2Gac.CnvLotteryQueryLotteryResult(success, lotteryInfos_U, lotteryResult_U, sumOfReward, sumOfCanReward)

  g_ScriptEvent:BroadcastInLua('CnvLotteryHisInfoBack',{his = MsgPackImpl.unpack(lotteryInfos_U), result = MsgPackImpl.unpack(lotteryResult_U), reward = sumOfReward, canReward = sumOfCanReward})
end

function Gas2Gac.CnvLotteryQueryPrizePoolResult(prizePool_U)

	g_ScriptEvent:BroadcastInLua("CnvLotteryTotalRewardInfoBack", MsgPackImpl.unpack(prizePool_U))
end
