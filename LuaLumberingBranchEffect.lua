require("common/common_include")

local Random = import "UnityEngine.Random"
local Time = import "UnityEngine.Time"
local Object = import "UnityEngine.Object"
local CUIFx = import "L10.UI.CUIFx"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local Ease = import "DG.Tweening.Ease"

LuaLumberingBranchEffect = class()

RegistClassMember(LuaLumberingBranchEffect, "m_FinalFx")
RegistClassMember(LuaLumberingBranchEffect, "m_ScoreLabel")

RegistClassMember(LuaLumberingBranchEffect, "m_DropSpeed")
RegistClassMember(LuaLumberingBranchEffect, "m_LifeTime")
RegistClassMember(LuaLumberingBranchEffect, "m_Fin")
RegistClassMember(LuaLumberingBranchEffect, "m_BottomLine")
RegistClassMember(LuaLumberingBranchEffect, "m_FxEnd")

function LuaLumberingBranchEffect:OnEnable()
	self.m_DropSpeed = Random.Range(800 - 50, 800 + 50)
	self.m_FinalFx = self.transform:Find("FinalFx"):GetComponent(typeof(CUIFx))
	self.m_FinalFx:DestroyFx()
	self.m_ScoreLabel = self.transform.parent.parent:Find("Hint/ScoreLabel").gameObject
	self.m_LifeTime = 3
	self.m_Fin = false
	self.m_BottomLine = -421
	self.m_FxEnd = false
	self.transform.localEulerAngles = Vector3(0, 0, Random.Range(-15, 15))

	Object.Destroy(self.gameObject, self.m_LifeTime)
end

function LuaLumberingBranchEffect:Update()

	if not self.m_Fin then
		local newPos = Vector3(self.transform.localPosition.x, self.transform.localPosition.y - self.m_DropSpeed * Time.deltaTime,  0)
		self.transform.localPosition = newPos

		if self.transform.localPosition.y < self.m_BottomLine then
			self.m_Fin = true
		end
	else
		if not self.m_FxEnd then
			self.m_FxEnd = true

			LuaTweenUtils.DOKill(self.m_FinalFx.FxRoot, false)
			local targetLocalPos = self.transform:InverseTransformPoint(self.m_ScoreLabel.transform.position)

			local path = {Vector3(0, 0, 0), targetLocalPos}
			local array = Table2Array(path, MakeArrayClass(Vector3))
			LuaUtils.SetLocalPosition(self.m_FinalFx.FxRoot,0, 0, 0)

			LuaTweenUtils.DOLocalPathOnce(self.m_FinalFx.FxRoot, array, 0.5, Ease.Linear)

			self.m_FinalFx:DestroyFx()
			self.m_FinalFx:LoadFx(CUIFxPaths.CommonYellowLineFxPath)
			Object.Destroy(self.gameObject, 3)
			g_ScriptEvent:BroadcastInLua("UpdateLumberingScore")
		end
	end
end

return LuaLumberingBranchEffect
