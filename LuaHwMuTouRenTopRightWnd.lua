local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CButton = import "L10.UI.CButton"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local UIGrid = import "UIGrid"
local CScene = import "L10.Game.CScene"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CUIGameObjectPool = import "L10.UI.CUIGameObjectPool"
local CChatMgr = import "L10.Game.CChatMgr"
local CChatHelper = import "L10.UI.CChatHelper"
local EChatPanel = import "L10.Game.EChatPanel"
local Ease = import "DG.Tweening.Ease"
local PlayerSettings = import "L10.Game.PlayerSettings"
LuaHwMuTouRenTopRightWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaHwMuTouRenTopRightWnd, "LeftTimeLabel", "LeftTimeLabel", UILabel)
RegistChildComponent(LuaHwMuTouRenTopRightWnd, "LeftPlayerLabel", "LeftPlayerLabel", UILabel)
RegistChildComponent(LuaHwMuTouRenTopRightWnd, "TrafficLight", "TrafficLight", GameObject)
RegistChildComponent(LuaHwMuTouRenTopRightWnd, "ChatButton", "ChatButton", CButton)
RegistChildComponent(LuaHwMuTouRenTopRightWnd, "QuickChatPanel", "QuickChatPanel", GameObject)
RegistChildComponent(LuaHwMuTouRenTopRightWnd, "ScreenBound", "ScreenBound", GameObject)
RegistChildComponent(LuaHwMuTouRenTopRightWnd, "BulletTemplate", "BulletTemplate", GameObject)
RegistChildComponent(LuaHwMuTouRenTopRightWnd, "ExpandButton", "ExpandButton", CButton)
RegistChildComponent(LuaHwMuTouRenTopRightWnd, "BulletChatButton", "BulletChatButton", QnSelectableButton)
RegistChildComponent(LuaHwMuTouRenTopRightWnd, "QuickChatGrid", "QuickChatGrid", UIGrid)
RegistChildComponent(LuaHwMuTouRenTopRightWnd, "ChatItemTemplate", "ChatItemTemplate", GameObject)
RegistChildComponent(LuaHwMuTouRenTopRightWnd, "YellowLightEffect", "YellowLightEffect", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaHwMuTouRenTopRightWnd, "m_BulletPool")
RegistClassMember(LuaHwMuTouRenTopRightWnd, "m_BulletScreen")
RegistClassMember(LuaHwMuTouRenTopRightWnd, "m_OnBulletRecv")
RegistClassMember(LuaHwMuTouRenTopRightWnd, "m_TimeAlertTick")      -- 倒计时警告
RegistClassMember(LuaHwMuTouRenTopRightWnd, "m_YellowLightTick")    -- 黄灯时界面闪烁效果
RegistClassMember(LuaHwMuTouRenTopRightWnd, "m_MemberCountTick")    -- 显示人数递减Tick
RegistClassMember(LuaHwMuTouRenTopRightWnd, "m_CurMember")
RegistClassMember(LuaHwMuTouRenTopRightWnd, "m_BuxuDong") -- 红灯提示艺术字
RegistClassMember(LuaHwMuTouRenTopRightWnd, "m_Light")  -- 黄灯警示
RegistClassMember(LuaHwMuTouRenTopRightWnd, "m_ResetCameraBtn")  -- 重置相机视角
RegistClassMember(LuaHwMuTouRenTopRightWnd, "m_JumpBtn")
RegistClassMember(LuaHwMuTouRenTopRightWnd, "m_BulletCnt")
function LuaHwMuTouRenTopRightWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
	UIEventListener.Get(self.ExpandButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnExpandButtonClick()
	end)

    UIEventListener.Get(self.ChatButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnChatButtonClick()
	end)

    self.BulletChatButton.OnClick = DelegateFactory.Action_QnButton(function(btn)
        self:OnBulletChatButtonClick()
    end)

    self.m_OnBulletRecv = DelegateFactory.Action_uint(function(channelDef)
        self:OnBullletRecv()
    end)

    self.m_ResetCameraBtn = self.transform:Find("Anchor/ResetCameraBtn").gameObject
    
    UIEventListener.Get(self.m_ResetCameraBtn).onClick = DelegateFactory.VoidDelegate(function (go)
	    LuaHalloween2022Mgr.ResetCamera(false)
	end)

    self.m_BulletPool = self.transform:Find("Anchor/BulletScreen/Pool"):GetComponent(typeof(CUIGameObjectPool))
    self.m_BulletScreen = self.transform:Find("Anchor/BulletScreen").gameObject
    self.m_BuxuDong = self.transform:Find("buxudongzong").gameObject
    self.m_Light = self.TrafficLight.transform:Find("Light").gameObject
    self.m_BuxuDong:SetActive(false)
    self.m_Light:SetActive(false)
    self.m_JumpBtn = self.transform:Find("JumpButton").gameObject
    self.m_JumpBtn:SetActive(false)

    UIEventListener.Get(self.m_JumpBtn.transform:Find("Tips").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    LuaHalloween2022Mgr.SkipTrafficLightNpcPreform()
	end)
    self.m_BulletCnt = 0
end

function LuaHwMuTouRenTopRightWnd:Init()
    self:CheckHideUI()
    self.BulletTemplate.gameObject:SetActive(false)
    self.QuickChatPanel:SetActive(false)
    self.YellowLightEffect:SetActive(false)
    self:InitTrafficLight()
    self:InitQuickChat()
    self:InitMembersNum()
end
function LuaHwMuTouRenTopRightWnd:CaptureModeOnOff(OnOff)
    self:HideAllUIWnd(not OnOff)
    self.m_JumpBtn.gameObject:SetActive(false)
end
function LuaHwMuTouRenTopRightWnd:HideAllUIWnd(isShow)
    self.transform:Find("Anchor").gameObject:SetActive(isShow)
    self.transform:Find("Bg").gameObject:SetActive(isShow)
    self.transform:Find("ExpandButton").gameObject:SetActive(isShow)
    self.m_JumpBtn.gameObject:SetActive(not isShow)
end

function LuaHwMuTouRenTopRightWnd:CheckHideUI()
    self.TrafficLight.gameObject:SetActive(not LuaHalloween2022Mgr.HideTopRightUI)
    self.transform:Find("Bg/Gua").gameObject:SetActive(not LuaHalloween2022Mgr.HideTopRightUI)
end

function LuaHwMuTouRenTopRightWnd:InitMembersNum()
    self.LeftPlayerLabel.text = "0"
    self.m_CurMember = 0
end
function LuaHwMuTouRenTopRightWnd:OnRightMenuChanged(args)
    if PlayerSettings.RightMenuExpanded then
        --LuaTweenUtils.DOLocalMoveX(self.m_ResetCameraBtn.transform)
        self.m_ResetCameraBtn.gameObject:SetActive(false)
        self.ChatButton.gameObject:SetActive(false)
    else
        self.m_ResetCameraBtn.gameObject:SetActive(true)
        self.ChatButton.gameObject:SetActive(true)
    end
end


-- 初始化快捷聊天
function LuaHwMuTouRenTopRightWnd:InitQuickChat()
    self:SetBulletStatus(LuaHalloween2022Mgr.EnterPlayOpenBullet)
    
    self.ChatItemTemplate:SetActive(false)
    Extensions.RemoveAllChildren(self.QuickChatGrid.transform)
    Halloween2022_LightTalkMsg.Foreach(function(k,v)
        local item = CUICommonDef.AddChild(self.QuickChatGrid.gameObject, self.ChatItemTemplate)
        item:SetActive(true)
        FindChild(item.transform,"Content"):GetComponent(typeof(UILabel)).text=v.Message
        
        UIEventListener.Get(item).onClick=LuaUtils.VoidDelegate(function(go)
            self:OnChatMsgChick(go)
        end)
    end)

    self.QuickChatGrid:GetComponent(typeof(UIGrid)):Reposition()
end
-- 初始化红绿灯
function LuaHwMuTouRenTopRightWnd:InitTrafficLight()
    local red = self.TrafficLight.transform:Find("Red").gameObject
    local Green = self.TrafficLight.transform:Find("Green").gameObject
    local Yellow = self.TrafficLight.transform:Find("Yellow").gameObject
    red:SetActive(false)
    Green:SetActive(false)
    Yellow:SetActive(false)

    -- self:ShowYellowLightEffect()  效果看起来不好，先不用
end
-- 更新剩余人数
function LuaHwMuTouRenTopRightWnd:UpdateLeftPersonNum()
    local newNum = LuaHalloween2022Mgr.TrafficLightPlayLeftMember
    if self.m_CurMember <= newNum then
        self.LeftPlayerLabel.text = newNum
    else
        if self.m_MemberCountTick then
            UnRegisterTick(self.m_MemberCountTick)
            self.m_MemberCountTick = nil
        end
        local curNum = self.m_CurMember
        local result = 1000
        local interval = result / (curNum - newNum + 1)
        local temp = curNum
        self.m_MemberCountTick = RegisterTickWithDuration(function ()
            if temp < newNum then
                UnRegisterTick(self.m_MemberCountTick)
                self.m_MemberCountTick = nil
                return
            end
            self.LeftPlayerLabel.text = temp
            
            temp = temp - 1
        end,interval,result) 
    end
    self.m_CurMember = newNum
    
end
-- 更新剩余时间
function LuaHwMuTouRenTopRightWnd:OnSceneRemainTimeUpdate(args)
    if CScene.MainScene then
        if CScene.MainScene.ShowTimeCountDown then
            self.LeftTimeLabel.text = SafeStringFormat3(LocalString.GetString("剩余 %s"), self:GetRemainTimeText(CScene.MainScene.ShowTime))
            if CScene.MainScene.ShowTime <= Halloween2022_Setting.GetData().TrafficPlayTimeAlert and self.m_TimeAlertTick == nil then
                self:ShowLeftTimeAlert()
            end
        else
            self.LeftTimeLabel.text = nil
        end
    else
        self.LeftTimeLabel.text = nil
    end
end
-- 更新红绿灯
function LuaHwMuTouRenTopRightWnd:UpdateTrafficLightStatus()
    local red = self.TrafficLight.transform:Find("Red").gameObject
    local Green = self.TrafficLight.transform:Find("Green").gameObject
    local Yellow = self.TrafficLight.transform:Find("Yellow").gameObject
    red:SetActive(LuaHalloween2022Mgr.TrafficLightStatus.state == EnumTrafficLightNpcState.Front)
    --Green:SetActive(LuaHalloween2022Mgr.TrafficLightStatus.state == EnumTrafficLightNpcState.Back)
    --Yellow:SetActive(LuaHalloween2022Mgr.TrafficLightStatus.state == EnumTrafficLightNpcState.TurnFront)
    Green:SetActive(false)
    Yellow:SetActive(false)
    if LuaHalloween2022Mgr.TrafficLightStatus.state == EnumTrafficLightNpcState.TurnFront then
        self.YellowLightEffect:SetActive(true)
        self.m_Light:SetActive(true)
        self:DoLightAnim()
    else
        self.YellowLightEffect:SetActive(false)
        self.m_Light:SetActive(false)
    end

    if LuaHalloween2022Mgr.TrafficLightStatus.state == EnumTrafficLightNpcState.Front then
        self.m_BuxuDong:SetActive(true and not LuaHalloween2022Mgr.HideTopRightUI)
    else
        self.m_BuxuDong:SetActive(false)
    end
end
-- 剩余时间小于某值,剩余时间变红并开始闪烁
function LuaHwMuTouRenTopRightWnd:ShowLeftTimeAlert()
    self.LeftTimeLabel.color = Color.red
    self.LeftTimeLabel.gameObject:SetActive(true)
    local hasShow = true
    local countDown = 0
    self.m_TimeAlertTick = RegisterTick(function ()
        countDown = countDown % 10
        if countDown >= 8 then
            hasShow = false
        else
            hasShow = true
        end
        countDown = countDown + 2
        self.LeftTimeLabel.gameObject:SetActive(hasShow)
    end,200)
end

-- 黄灯屏幕闪烁效果
function LuaHwMuTouRenTopRightWnd:ShowYellowLightEffect()
    local ShowEnabel = false
    local EffectTex = self.YellowLightEffect:GetComponent(typeof(UITexture))
    if self.m_YellowLightTick then UnRegisterTick(self.m_YellowLightTick) end
    self.m_YellowLightTick = RegisterTick(function()
        if ShowEnabel then
            EffectTex.alpha = 1
            ShowEnabel = false            
        else
            EffectTex.alpha = 0.5
            ShowEnabel = true
        end
    end,200)
end


function LuaHwMuTouRenTopRightWnd:DoLightAnim()
    if not LuaHalloween2022Mgr.TrafficLightStatus then return end
    local progress = 1
    if LuaHalloween2022Mgr.TrafficLightStatus.totalKeepDuration ~= 0 then
        progress = LuaHalloween2022Mgr.TrafficLightStatus.leftKeepDuration / LuaHalloween2022Mgr.TrafficLightStatus.totalKeepDuration 
    end
    local startScale = progress * 1.5
    local endScale = 0.8
    local duraion = LuaHalloween2022Mgr.TrafficLightStatus.leftKeepDuration / 1000
    LuaTweenUtils.TweenScale(self.m_Light.transform,Vector3(startScale,startScale,1),Vector3(endScale,endScale,1),duraion)
end

function LuaHwMuTouRenTopRightWnd:OnChatButtonClick()
    if self.QuickChatPanel.activeSelf then
        self.QuickChatPanel:SetActive(false)
    else
        self.QuickChatPanel:SetActive(true)
    end
end
-- @region 弹幕
function LuaHwMuTouRenTopRightWnd:OnBulletChatButtonClick()
    if self.BulletChatButton:isSeleted() then
        self:SetBulletStatus(true)
    else
        self:SetBulletStatus(false)
    end
end

function LuaHwMuTouRenTopRightWnd:SetBulletStatus(isOpen)
    self.BulletChatButton:SetSelected(isOpen)
    self.m_BulletScreen:SetActive(isOpen)
end
-- 发送弹幕
function LuaHwMuTouRenTopRightWnd:OnChatMsgChick(go)
    local index = go.transform:GetSiblingIndex()
    self.QuickChatPanel:SetActive(false)
    CChatMgr.Inst:SendChatMsg(CChatMgr.CHANNEL_NAME_PLAY, Halloween2022_LightTalkMsg.GetData(index + 1).Message, false)
end
-- 接受弹幕
function LuaHwMuTouRenTopRightWnd:OnBullletRecv()
    if not self.BulletChatButton:isSeleted() then return end        -- 关弹幕的情况下不接收弹幕 
    local chatMsg = CChatMgr.Inst:GetLatestMsg()
    if chatMsg then
        local channel =  CChatHelper.GetChannelByName(chatMsg.channelName)
        if channel == EChatPanel.Ally  then -- 友方
            if string.sub(chatMsg.message, 1, string.len("voice://")) == "voice://" then -- 屏蔽语音消息
                return
            end
            if string.sub(chatMsg.message, 1, string.len("<link")) == "<link" then -- 屏蔽文本链接
                return
            end
            self:OnShowBullet(chatMsg,chatMsg.message)
        end
    end
end
-- 显示弹幕
function LuaHwMuTouRenTopRightWnd:OnShowBullet(chatMsgInfo,content)
    if self.m_BulletCnt >= self:BulletCount() then return end
    if System.String.IsNullOrEmpty(content) then
        return
    end
    local label = self.m_BulletPool:GetFromPool(0)
    local bulletScreenBound = self.ScreenBound:GetComponent(typeof(UIWidget))
    label.transform.parent = self.m_BulletScreen.transform
    label.transform.localScale = Vector3.one
    label:SetActive(true)
    local bullet = CommonDefs.GetComponent_GameObject_Type(label, typeof(UILabel))
    if bullet == nil then
        return
    end
    self.m_BulletCnt = self.m_BulletCnt + 1
    bullet.text = self:GenerateBulletText(chatMsgInfo, content)
    bullet.fontSize = UnityEngine_Random(38, 52)
    bullet.transform.localPosition = Vector3(math.floor(bulletScreenBound.width / 2), UnityEngine_Random(math.floor(- bulletScreenBound.height / 2), math.floor(bulletScreenBound.height / 2)), 0)
    CommonDefs.OnComplete_Tweener(CommonDefs.SetEase_Tweener(LuaTweenUtils.DOLocalMoveX(bullet.transform, math.floor(- bulletScreenBound.width / 2) - bullet.width, UnityEngine_Random(6, 12), false), Ease.Linear), DelegateFactory.TweenCallback(function () 
        label.transform.parent = nil
        label:SetActive(false)
        self.m_BulletPool:Recycle(label.gameObject)
        self.m_BulletCnt = self.m_BulletCnt - 1
    end))
end

function LuaHwMuTouRenTopRightWnd:GenerateBulletText(chatMsgInfo, content)
    return chatMsgInfo.fromUserName..":"..content
end
-- @endregion 弹幕

--@region UIEvent
function LuaHwMuTouRenTopRightWnd:OnExpandButtonClick()
    self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 0)
	CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end
function LuaHwMuTouRenTopRightWnd:OnHideTopAndRightTipWnd()
	self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 180)
end
--@endregion UIEvent

function LuaHwMuTouRenTopRightWnd:GetRemainTimeText(totalSeconds)
    if totalSeconds <= 1 then
        return ""
    end
    local time
    if totalSeconds >= 3600 then
        time = SafeStringFormat3("%02d:%02d:%02d", math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
    else
        time = SafeStringFormat3("%02d:%02d", math.floor(totalSeconds / 60), totalSeconds % 60)
    end
    return time
end
-- 发弹幕的数量上限
function LuaHwMuTouRenTopRightWnd:BulletCount()
    return 20
end
function LuaHwMuTouRenTopRightWnd:OnEnable()
    g_ScriptEvent:AddListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd",self,"OnHideTopAndRightTipWnd")
    g_ScriptEvent:AddListener("HwTrafficLightChange",self,"UpdateTrafficLightStatus")
    g_ScriptEvent:AddListener("SyncPlayerTrafficMembers",self,"UpdateLeftPersonNum")
    g_ScriptEvent:AddListener("HwMuTouRenStartPerform",self,"HideAllUIWnd")
    g_ScriptEvent:AddListener("CaptureModeOnOff", self, "CaptureModeOnOff")
    g_ScriptEvent:AddListener("Guide_ChangeView", self, "OnRightMenuChanged")
    g_ScriptEvent:AddListener("SyncPlayerComplateTrafficLightGame",self,"SetBulletStatus")
    EventManager.AddListenerInternal(EnumEventType.RecvChatMsg, self.m_OnBulletRecv)
end

function LuaHwMuTouRenTopRightWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd",self,"OnHideTopAndRightTipWnd")
    g_ScriptEvent:RemoveListener("HwTrafficLightChange",self,"UpdateTrafficLightStatus")
    g_ScriptEvent:RemoveListener("SyncPlayerTrafficMembers",self,"UpdateLeftPersonNum")
    g_ScriptEvent:RemoveListener("HwMuTouRenStartPerform",self,"HideAllUIWnd")
    g_ScriptEvent:RemoveListener("CaptureModeOnOff", self, "CaptureModeOnOff")
    g_ScriptEvent:RemoveListener("Guide_ChangeView", self, "OnRightMenuChanged")
    g_ScriptEvent:RemoveListener("SyncPlayerComplateTrafficLightGame",self,"SetBulletStatus")
    EventManager.RemoveListenerInternal(EnumEventType.RecvChatMsg, self.m_OnBulletRecv)
    if CUIManager.IsLoaded(CUIResources.TopAndRightTipWnd) then
		CUIManager.CloseUI(CUIResources.TopAndRightTipWnd)
	end
    LuaHalloween2022Mgr.CloseSoundInPlay()
    if self.m_TimeAlertTick then
        UnRegisterTick(self.m_TimeAlertTick)
        self.m_TimeAlertTick = nil
    end

    if self.m_YellowLightTick then
        UnRegisterTick(self.m_YellowLightTick)
        self.m_YellowLightTick = nil
    end

    if self.m_MemberCountTick then
        UnRegisterTick(self.m_MemberCountTick)
        self.m_MemberCountTick = nil
    end
end
