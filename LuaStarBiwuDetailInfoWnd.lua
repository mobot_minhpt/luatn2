local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local QnTabView = import "L10.UI.QnTabView"

LuaStarBiwuDetailInfoWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaStarBiwuDetailInfoWnd, "TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaStarBiwuDetailInfoWnd, "Tabs", "Tabs", QnTabView)

--@endregion RegistChildComponent end
function LuaStarBiwuDetailInfoWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaStarBiwuDetailInfoWnd:Init()
    if CLuaStarBiwuMgr.m_CurOpenDetailWndIndex > 0 then
        self.Tabs:ChangeTo(CLuaStarBiwuMgr.m_CurOpenDetailWndIndex)
    end
end

function LuaStarBiwuDetailInfoWnd:OnChangeTitleLabel(content)
    self.TitleLabel.text = content
end

function LuaStarBiwuDetailInfoWnd:OnEnable()
    g_ScriptEvent:AddListener("OnStarBiWuDetailInfoWndChange",self, "OnChangeTitleLabel")
end


function LuaStarBiwuDetailInfoWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnStarBiWuDetailInfoWndChange",self, "OnChangeTitleLabel")
    CLuaStarBiwuMgr.m_CurOpenDetailWndIndex = -1
    CLuaStarBiwuMgr.m_InitShowGroup = 0
end
--@region UIEvent

--@endregion UIEvent

