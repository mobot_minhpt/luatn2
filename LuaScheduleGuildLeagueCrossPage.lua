local DelegateFactory  = import "DelegateFactory"
local Extensions = import "Extensions"
local CWebBrowserMgr = import "L10.Game.CWebBrowserMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local ClientAction = import "L10.UI.ClientAction"
local UISlider = import "UISlider"
local CButton = import "L10.UI.CButton"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"

LuaScheduleGuildLeagueCrossPage = class()

RegistClassMember(LuaScheduleGuildLeagueCrossPage, "m_TitleLabel")              --标题
RegistClassMember(LuaScheduleGuildLeagueCrossPage, "m_AgendaButton")            --赛制赛程
RegistClassMember(LuaScheduleGuildLeagueCrossPage, "m_SelfGuildListButton")     --本服代表
RegistClassMember(LuaScheduleGuildLeagueCrossPage, "m_TrainButton")             --赛事培养
RegistClassMember(LuaScheduleGuildLeagueCrossPage, "m_TrainButtonAlert")        --赛事培养红点
RegistClassMember(LuaScheduleGuildLeagueCrossPage, "m_ForeignAidButton")        --外援申请
RegistClassMember(LuaScheduleGuildLeagueCrossPage, "m_ForeignAidButtonAlert")   --外援申请红点
RegistClassMember(LuaScheduleGuildLeagueCrossPage, "m_MatchInfoButton")         --对阵信息
RegistClassMember(LuaScheduleGuildLeagueCrossPage, "m_GambleButton")            --赛事竞猜
RegistClassMember(LuaScheduleGuildLeagueCrossPage, "m_GambleButtonAlert")       --赛事竞猜红点
RegistClassMember(LuaScheduleGuildLeagueCrossPage, "m_AwardOverviewButton")     --奖励一览
RegistClassMember(LuaScheduleGuildLeagueCrossPage, "m_AwardPoolButton")         --奖池
RegistClassMember(LuaScheduleGuildLeagueCrossPage, "m_ReportButton")            --赛事专题
RegistClassMember(LuaScheduleGuildLeagueCrossPage, "m_GoToButton")              --前往
RegistClassMember(LuaScheduleGuildLeagueCrossPage, "m_ChampionRoot")            --冠军
RegistClassMember(LuaScheduleGuildLeagueCrossPage, "m_ChampionServerNameLabel") --冠军服务器名
RegistClassMember(LuaScheduleGuildLeagueCrossPage, "m_ResultButton")            --比赛结果
RegistClassMember(LuaScheduleGuildLeagueCrossPage, "m_ZhanLingButton")          --战令
RegistClassMember(LuaScheduleGuildLeagueCrossPage, "m_ZhanLingButtonAlert")     --战令红点
RegistClassMember(LuaScheduleGuildLeagueCrossPage, "m_ProgressBar")             --进度
RegistClassMember(LuaScheduleGuildLeagueCrossPage, "m_ProgressGrid")            --进度
RegistClassMember(LuaScheduleGuildLeagueCrossPage, "m_ProgressFinalDot")        --进度条末尾紫色点

--TODO progress

function LuaScheduleGuildLeagueCrossPage:Awake()
    self.m_TitleLabel = self.transform:Find("TitleLabel"):GetComponent(typeof(UILabel))
    self.m_AgendaButton = self.transform:Find("AgendaButton").gameObject
    self.m_SelfGuildListButton = self.transform:Find("SelfGuildListButton").gameObject
    self.m_TrainButton = self.transform:Find("TrainButton").gameObject
    self.m_TrainButtonAlert = self.transform:Find("TrainButton/Alert").gameObject
    self.m_ForeignAidButton = self.transform:Find("ForeignAidButton").gameObject
    self.m_ForeignAidButtonAlert = self.transform:Find("ForeignAidButton/Alert").gameObject
    self.m_MatchInfoButton = self.transform:Find("MatchInfoButton").gameObject
    self.m_GambleButton = self.transform:Find("GambleButton").gameObject
    self.m_GambleButtonAlert = self.transform:Find("GambleButton/Alert").gameObject
    self.m_AwardOverviewButton = self.transform:Find("AwardOverviewButton").gameObject
    self.m_AwardPoolButton = self.transform:Find("AwardPoolButton").gameObject
    self.m_ReportButton = self.transform:Find("ReportButton").gameObject
    self.m_GoToButton = self.transform:Find("GoToButton").gameObject
    self.m_ChampionRoot = self.transform:Find("Champion").gameObject
    self.m_ChampionServerNameLabel = self.transform:Find("Champion/ServerNameLabel"):GetComponent(typeof(UILabel))
    self.m_ResultButton = self.transform:Find("Champion/ResultButton").gameObject
    self.m_ZhanLingButton = self.transform:Find("ZhanLingButton").gameObject
    self.m_ZhanLingButtonAlert = self.transform:Find("ZhanLingButton/Alert").gameObject
    self.m_ProgressBar = self.transform:Find("Progress/ProgressBar"):GetComponent(typeof(UISlider))
    self.m_ProgressGrid = self.transform:Find("Progress/Grid")
    self.m_ProgressFinalDot = self.transform:Find("Progress/ProgressBar/FinalDot").gameObject

    UIEventListener.Get(self.m_AgendaButton).onClick = DelegateFactory.VoidDelegate(function() self:OnAgendaButtonClick() end)
    UIEventListener.Get(self.m_SelfGuildListButton).onClick = DelegateFactory.VoidDelegate(function() self:OnSelfGuildListButtonClick() end)
    UIEventListener.Get(self.m_TrainButton).onClick = DelegateFactory.VoidDelegate(function() self:OnTrainButtonClick() end)
    UIEventListener.Get(self.m_ForeignAidButton).onClick = DelegateFactory.VoidDelegate(function() self:OnForeignAidButtonClick() end)
    UIEventListener.Get(self.m_MatchInfoButton).onClick = DelegateFactory.VoidDelegate(function() self:OnMatchInfoButtonClick() end)
    UIEventListener.Get(self.m_GambleButton).onClick = DelegateFactory.VoidDelegate(function() self:OnGambleButtonClick() end)
    UIEventListener.Get(self.m_AwardOverviewButton).onClick = DelegateFactory.VoidDelegate(function() self:OnAwardOverviewButtonClick() end)
    UIEventListener.Get(self.m_AwardPoolButton).onClick = DelegateFactory.VoidDelegate(function() self:OnAwardPoolButtonClick() end)
    UIEventListener.Get(self.m_ReportButton).onClick = DelegateFactory.VoidDelegate(function() self:OnReportButtonClick() end)
    UIEventListener.Get(self.m_GoToButton).onClick = DelegateFactory.VoidDelegate(function() self:OnGoToButtonClick() end)
    UIEventListener.Get(self.m_ResultButton).onClick = DelegateFactory.VoidDelegate(function() self:OnResultButtonClick() end)
    UIEventListener.Get(self.m_ZhanLingButton).onClick = DelegateFactory.VoidDelegate(function() self:OnZhanLingButtonClick() end)

    self.m_ReportButton:SetActive(CommonDefs.IS_CN_CLIENT) --国服版显示赛事专题按钮
end

function LuaScheduleGuildLeagueCrossPage:Init()
    self:InitContent()
end

function LuaScheduleGuildLeagueCrossPage:InitContent()
    local NThMatch = GuildLeagueCross_Setting.GetData().NThMatch
    self.m_TitleLabel.text = SafeStringFormat3(LocalString.GetString("第%s届跨服帮会联赛"), Extensions.ConvertToChineseString(NThMatch))
    self:InitAgenda()
    self:OnQueryForOpenGLCWndResult()
    LuaGuildLeagueCrossMgr:QueryForOpenGLCWnd()
    self.m_ZhanLingButton:GetComponent(typeof(CButton)).Text = SafeStringFormat3(LocalString.GetString("%d级"), LuaGuildLeagueCrossMgr:GetCurZhanLingLevel())
    self.m_ZhanLingButtonAlert:SetActive(LuaGuildLeagueCrossMgr:NeedShowZhanLingAlert())
end

function LuaScheduleGuildLeagueCrossPage:InitAgenda()
    self.m_ProgressBar.value = 0
    local n = self.m_ProgressGrid.transform.childCount
    for i=0,n-1 do
        local child = self.m_ProgressGrid.transform:GetChild(i)
        local data = GuildLeagueCross_Agenda.GetData(i+1)
        if data then
            child.gameObject:SetActive(true)
            local beginTimestamp = CServerTimeMgr.Inst:GetTimeStampByStr(data.BeginTime)
            local beginTime =  CServerTimeMgr.ConvertTimeStampToZone8Time(beginTimestamp)
            child.transform:Find("TimeLabel"):GetComponent(typeof(UILabel)).text = ToStringWrap(beginTime, LocalString.GetString("MM.dd HH:mm"))
            child.transform:Find("BattleIcon").gameObject:SetActive(false)
        else
            child.gameObject:SetActive(false)
        end
    end
end

function LuaScheduleGuildLeagueCrossPage:InitProgress()
    local scheduleInfo = LuaGuildLeagueCrossMgr.m_ScheduleInfo
    local round =  scheduleInfo and scheduleInfo.round or 0
    if round==0 then --round为0可能是服务器尚未同步，或者没有到活动时间，再或者某个轮次结束之后，服务器在CGuildLeagueCrossMgr:OnPlayEndCron()置为了0
        if LuaGuildLeagueCrossMgr:IsDuringOpenTime() then --客户端计算一下当前所在的轮次，没到下一轮之前都算前一轮
            GuildLeagueCross_Agenda.Foreach(function(id, data)
                local beginTimestamp = CServerTimeMgr.Inst:GetTimeStampByStr(data.BeginTime)
                if beginTimestamp<=CServerTimeMgr.Inst.timeStamp then
                    round =  id
                end
            end)
        end
    end
    if round>0 then
        local childCount = self.m_ProgressGrid.transform.childCount
        if round>=childCount then
            self.m_ProgressBar.value = 1
            CUICommonDef.SetGrey(self.m_ProgressFinalDot, false)
        else
            local child = self.m_ProgressGrid.transform:GetChild(round-1)
            local foreground = self.m_ProgressBar.foregroundWidget
            local width = foreground.width
            local offsetPos = foreground.transform:InverseTransformPoint(child.parent:TransformPoint(child.localPosition))
            self.m_ProgressBar.value = offsetPos.x/width
            CUICommonDef.SetGrey(self.m_ProgressFinalDot, true)
        end
    else
        self.m_ProgressBar.value = 0
        CUICommonDef.SetGrey(self.m_ProgressFinalDot, true)
    end

    local roundGoing =  scheduleInfo and scheduleInfo.playStatus 
                    and scheduleInfo.playStatus>EnumGuildLeagueCrossPlayStatus.eBeiZhan 
                    and scheduleInfo.playStatus<EnumGuildLeagueCrossPlayStatus.eFinish or false
    local n = self.m_ProgressGrid.transform.childCount
    for i=0,n-1 do
        local child = self.m_ProgressGrid.transform:GetChild(i)
        child.transform:Find("BattleIcon").gameObject:SetActive(round==i+1 and roundGoing)
        CUICommonDef.SetGrey(child.transform:Find("Icon").gameObject, round<i+1)
    end

end

function LuaScheduleGuildLeagueCrossPage:OnEnable()
    g_ScriptEvent:AddListener("OnQueryForOpenGLCWndResult", self, "OnQueryForOpenGLCWndResult")
    g_ScriptEvent:AddListener("OnGLCScheduleAlertVisibilityChanged", self, "OnGLCScheduleAlertVisibilityChanged")
end

function LuaScheduleGuildLeagueCrossPage:OnDisable()
    g_ScriptEvent:RemoveListener("OnQueryForOpenGLCWndResult", self, "OnQueryForOpenGLCWndResult")
    g_ScriptEvent:RemoveListener("OnGLCScheduleAlertVisibilityChanged", self, "OnGLCScheduleAlertVisibilityChanged")
end

function LuaScheduleGuildLeagueCrossPage:OnQueryForOpenGLCWndResult()
    local scheduleInfo = LuaGuildLeagueCrossMgr.m_ScheduleInfo
    if scheduleInfo and scheduleInfo.champtionServerId and scheduleInfo.champtionServerId>0 then
        self.m_ChampionRoot:SetActive(true)
        self.m_GoToButton.gameObject:SetActive(false)
        self.m_ChampionServerNameLabel.text = scheduleInfo.champtionServerName
        self.m_ForeignAidButton:GetComponent(typeof(CButton)).Text = scheduleInfo.isGuildHasQualification and LocalString.GetString("外援管理") or LocalString.GetString("外援申请")
    else
        self.m_ChampionRoot:SetActive(false)
        self.m_GoToButton.gameObject:SetActive(true)
        self.m_ForeignAidButton:GetComponent(typeof(CButton)).Text = LocalString.GetString("外援申请")
    end

    self:InitProgress()
    self:RefreshAlert()
end

function LuaScheduleGuildLeagueCrossPage:OnGLCScheduleAlertVisibilityChanged()
    self:RefreshAlert()
    EventManager.Broadcast(EnumEventType.UpdateScheduleAlert) -- 刷新一下schedulewnd里面的tab按钮
end

function LuaScheduleGuildLeagueCrossPage:RefreshAlert()
    self.m_TrainButtonAlert:SetActive(LuaGuildLeagueCrossMgr:NeedShowTrainAlert())
    self.m_ForeignAidButtonAlert:SetActive(LuaGuildLeagueCrossMgr:NeedShowForeignAidAlert())
    self.m_GambleButtonAlert:SetActive(LuaGuildLeagueCrossMgr:NeedShowGambleAlert())
end

function LuaScheduleGuildLeagueCrossPage:OnAgendaButtonClick()
    LuaGuildLeagueCrossMgr:ShowAgendaWnd()
end

function LuaScheduleGuildLeagueCrossPage:OnSelfGuildListButtonClick()
    LuaGuildLeagueCrossMgr:ShowSelfGuildListWnd()
end

function LuaScheduleGuildLeagueCrossPage:OnTrainButtonClick()
    --没有帮会时引导去加帮会
    if LuaGuildLeagueCrossMgr:CheckAndOpenJoinGuildWndForTrain() then
        return
    end
    LuaGuildLeagueCrossMgr:QueryGuildLeagueTrainInfo()
    LuaGuildLeagueCrossMgr:MarkScheduleAlertHidden(EnumGuildLeagueCrossScheduleTempAlert.eTrainAlert)
end

function LuaScheduleGuildLeagueCrossPage:OnForeignAidButtonClick()
    local scheduleInfo = LuaGuildLeagueCrossMgr.m_ScheduleInfo
    if scheduleInfo then
        if scheduleInfo.hasAidManagePermission then
            LuaGuildExternalAidMgr:ShowGuildExternalAidWnd(EnumExternalAidGamePlay.eGuildLeagueCross)
        else
            LuaGuildExternalAidMgr:ShowGuildExternalAidCardWnd(EnumExternalAidGamePlay.eGuildLeagueCross)
        end
    end
end

function LuaScheduleGuildLeagueCrossPage:OnMatchInfoButtonClick()
    LuaGuildLeagueCrossMgr:ShowMatchInfoWnd()
end

function LuaScheduleGuildLeagueCrossPage:OnGambleButtonClick()
    LuaGuildLeagueCrossMgr:QueryGuildLeagueCrossGambleInfo()
    LuaGuildLeagueCrossMgr:MarkScheduleAlertHidden(EnumGuildLeagueCrossScheduleTempAlert.eGambleAlert)
end

function LuaScheduleGuildLeagueCrossPage:OnAwardOverviewButtonClick()
    LuaGuildLeagueCrossMgr:ShowAwardOverviewWnd()
end

function LuaScheduleGuildLeagueCrossPage:OnAwardPoolButtonClick()
    LuaGuildLeagueCrossMgr:QueryGuildLeagueCrossRewardPoolInfo(EnumZhanLingAwardPoolWndOpenType.eDefault)
end

function LuaScheduleGuildLeagueCrossPage:OnReportButtonClick()
    CWebBrowserMgr.Inst:OpenUrl(GuildLeagueCross_Setting.GetData().ReportURL)
end

function LuaScheduleGuildLeagueCrossPage:OnGoToButtonClick()
    local scheduleInfo = LuaGuildLeagueCrossMgr.m_ScheduleInfo
    if scheduleInfo and scheduleInfo.isPlayStart then
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("GLC_JOIN_ACTIVITY_CONFIRM"), function ()
            ClientAction.DoAction(GuildLeagueCross_Setting.GetData().GoToAction)
        end, nil, nil, nil, false)
    else
        g_MessageMgr:ShowMessage("GLC_ACTIVITY_NOT_STARTED")
    end
end

function LuaScheduleGuildLeagueCrossPage:OnResultButtonClick()
    LuaGuildLeagueCrossMgr:ShowResultWnd()
end

function LuaScheduleGuildLeagueCrossPage:OnZhanLingButtonClick()
    LuaGuildLeagueCrossMgr:QueryGuildLeagueCrossZhanLingExtInfo()
    LuaGuildLeagueCrossMgr:MarkZhanLingAlertHidden()
    self.m_ZhanLingButtonAlert:SetActive(false)
end

