local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CExpWnd_PopupMenu=import "L10.UI.CExpWnd_PopupMenu"
local Color = import "UnityEngine.Color"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local EnumNetworkType = import "EnumNetworkType"
local NativeUtility = import "NativeUtility"
local NGUIText = import "NGUIText"
local Time = import "UnityEngine.Time"
local UIProgressBar=import "UIProgressBar"
local UIRoot=import "UIRoot"
local VoidDelegate = import "UIEventListener+VoidDelegate"

CLuaExpProgressWnd = class()
RegistClassMember(CLuaExpProgressWnd,"curExp")
RegistClassMember(CLuaExpProgressWnd,"maxExp")
RegistClassMember(CLuaExpProgressWnd,"yingtouExp")
RegistClassMember(CLuaExpProgressWnd,"lastUpdateTime")
RegistClassMember(CLuaExpProgressWnd,"updateInterval")
RegistClassMember(CLuaExpProgressWnd,"progressBar")
RegistClassMember(CLuaExpProgressWnd,"midBackground")
RegistClassMember(CLuaExpProgressWnd,"splitRoot")
RegistClassMember(CLuaExpProgressWnd,"timeLabel")
RegistClassMember(CLuaExpProgressWnd,"batterySprite")
RegistClassMember(CLuaExpProgressWnd,"networkSprite")
RegistClassMember(CLuaExpProgressWnd,"m_PopupMenu")
RegistClassMember(CLuaExpProgressWnd,"m_Tick")
RegistClassMember(CLuaExpProgressWnd,"m_DanmuWnd")
RegistClassMember(CLuaExpProgressWnd,"m_DelayLabel")

function CLuaExpProgressWnd:Awake()
    self.curExp = 0
    self.maxExp = 0
    self.yingtouExp = 0
    self.lastUpdateTime = 0
    self.updateInterval = 10
    self.progressBar = self.transform:Find("Progress Bar"):GetComponent(typeof(UIProgressBar))
    self.midBackground = self.transform:Find("Progress Bar/Midground"):GetComponent(typeof(UISprite))
    self.splitRoot = self.transform:Find("Progress Bar/Splits")
    self.timeLabel = self.transform:Find("PhoneStatus/TimeLabel"):GetComponent(typeof(UILabel))
    self.batterySprite = self.transform:Find("PhoneStatus/Battery/Foreground"):GetComponent(typeof(UISprite))
    self.networkSprite = self.transform:Find("PhoneStatus/Network"):GetComponent(typeof(UISprite))
    self.m_PopupMenu = self.transform:Find("Container"):GetComponent(typeof(CExpWnd_PopupMenu))
    self.m_DelayLabel = self.transform:Find("PhoneStatus/DelayLabel"):GetComponent(typeof(UILabel))
    self.m_DanmuWnd=FindChild(self.transform,"DanmuButton").gameObject
    if UIRoot.EnableIPhoneXAdaptation then
        Extensions.SetLocalPositionY(self.m_DanmuWnd.transform, 122)
    end
    self.m_DanmuWnd:SetActive(false)
    UIEventListener.Get(self.m_DanmuWnd).onClick = DelegateFactory.VoidDelegate(function(go)
        -- CUIManager.ShowUI(CUIResources.DanmuWnd)
        self.m_DanmuWnd:SetActive(false)
	    g_ScriptEvent:BroadcastInLua("ShowDanmuInput",false)
    end)

    self.m_Tick = RegisterTick(function()--每秒更新一次
        if Time.realtimeSinceStartup - self.lastUpdateTime >= self.updateInterval then
            self.lastUpdateTime = Time.realtimeSinceStartup
            self.timeLabel.text = ToStringWrap(CServerTimeMgr.Inst:GetZone8Time(), "HH:mm")
            local batteryLevel = math.min(1,math.max(0,NativeUtility.GetBatteryLevel()))
            self.batterySprite.fillAmount = batteryLevel
            self.batterySprite.color = (batteryLevel <= 0.2) and NGUIText.ParseColor24("fd2202", 0) or Color.green
            local networkStatus = NativeUtility.GetNetworkInfo()
            if networkStatus == EnumNetworkType.Wifi then
                self.networkSprite.spriteName = "device_status_wifi"
                self.networkSprite:MakePixelPerfect()
            elseif networkStatus == EnumNetworkType.Mobile then
                self.networkSprite.spriteName = "device_status_withoutwifi"
                self.networkSprite:MakePixelPerfect()
            else
                self.networkSprite.spriteName = nil
            end
        end
    end,1000)
    --Gac2Gas.RequestMusicBoxPlayList()
end

function CLuaExpProgressWnd:ShowNetworkDelayTimeInfo( )
    if not self.m_DelayLabel then return end
    local val = math.floor(LuaNetworkMgr.m_HeartBeatDelayTime)
    self.m_DelayLabel.text = SafeStringFormat3("%sms", tostring(val))
end

function CLuaExpProgressWnd:OnUpdateExp( )
    if CClientMainPlayer.Inst ~= nil then
        self.curExp = CClientMainPlayer.Inst.PlayProp.Exp
        self.maxExp = CClientMainPlayer.Inst:GetMaxExp()
        self.yingtouExp = CClientMainPlayer.Inst.PlayProp.OfflineTaskExp
        local curPercent = self.curExp * 1 / self.maxExp
        local totalPercent = (self.curExp + self.yingtouExp) * 1 / self.maxExp
        totalPercent = totalPercent < 1 and totalPercent or 1
        self.progressBar.value = curPercent
        self.midBackground.fillAmount = totalPercent
    end
end
function CLuaExpProgressWnd:Init( )
    self.timeLabel.text = nil
    self.m_DelayLabel.text = nil
    self.batterySprite.fillAmount = 0
    self.networkSprite.spriteName = nil
    self.lastUpdateTime = 0
    self:OnUpdateExp()
end


function CLuaExpProgressWnd:OnEnable()
    g_ScriptEvent:AddListener("MainPlayerCreated",self,"OnUpdateExp")
    g_ScriptEvent:AddListener("MainPlayerLevelChange",self,"OnUpdateExp")
    g_ScriptEvent:AddListener("MainPlayerPlayPropUpdate",self,"OnUpdateExp")
    g_ScriptEvent:AddListener("ShowDanmuEntry",self,"OnShowDanmuEntry")
    g_ScriptEvent:AddListener("MainPlayerDestroyed",self,"OnMainPlayerDestroyed")
    g_ScriptEvent:AddListener("OnRTTReturn",self,"OnRTTReturn")
    g_ScriptEvent:AddListener("OnSendMusicBoxState", self, "OnSendMusicBoxState")
    self.m_PopupMenu:Init()
end
function CLuaExpProgressWnd:OnDisable()
    g_ScriptEvent:RemoveListener("MainPlayerCreated",self,"OnUpdateExp")
    g_ScriptEvent:RemoveListener("MainPlayerLevelChange",self,"OnUpdateExp")
    g_ScriptEvent:RemoveListener("MainPlayerPlayPropUpdate",self,"OnUpdateExp")
    g_ScriptEvent:RemoveListener("ShowDanmuEntry",self,"OnShowDanmuEntry")
    g_ScriptEvent:RemoveListener("MainPlayerDestroyed",self,"OnMainPlayerDestroyed")
    g_ScriptEvent:RemoveListener("OnRTTReturn",self,"OnRTTReturn")
    g_ScriptEvent:RemoveListener("OnSendMusicBoxState", self, "OnSendMusicBoxState")
end

function CLuaExpProgressWnd:OnSendMusicBoxState()
    self.m_PopupMenu:Init()
end

function CLuaExpProgressWnd:OnDestroy()
    UnRegisterTick(self.m_Tick)
end

function CLuaExpProgressWnd:OnShowDanmuEntry(show)
    self.m_DanmuWnd:SetActive(show)
end

function CLuaExpProgressWnd:OnMainPlayerDestroyed()
    self.m_DanmuWnd:SetActive(false)
end

function CLuaExpProgressWnd:OnRTTReturn(...)
    self:ShowNetworkDelayTimeInfo()
end

function CLuaExpProgressWnd:GetGuideGo(methodName)
    if methodName=="PopupMenuBtn" then
        return self.m_PopupMenu.m_MainButton
    end

end
CExpWnd_PopupMenu.m_hookInit = function(this)
    this.m_IsFold = true
    this.m_Tweener.transform.localScale = Vector3.zero
    this.m_ReplayKitButton:SetActive(CommonDefs.IS_KR_CLIENT)
    if CommonDefs.IsIOSPlatform() and not CommonDefs.IS_KR_CLIENT then
        local ReplayManager = import "ReplayManager" 
        local Device = import "UnityEngine.iOS.Device"
        this.m_ReplayKitButton:SetActive(EnumToInt(Device.generation) >= 18)
        this.m_IsRecordingSprite:SetActive(ReplayManager.IsRecording())
        this.m_IsRecordingSprite2:SetActive(ReplayManager.IsRecording())
        this.m_AddSprite:SetActive(not ReplayManager.IsRecording())
    end
    if this.m_MusicBoxButton then
        this.m_MusicBoxButton:SetActive(CLuaMusicBoxMgr.m_IsEnable)
    end
    if this.m_ButtonsGrid then
        this.m_ButtonsGrid:Reposition()
    end
    UIEventListener.Get(this.m_MainButton).onClick = MakeDelegateFromCSFunction(this.OnMainButtonClick, VoidDelegate, this)
    UIEventListener.Get(this.m_CaptureButton).onClick = MakeDelegateFromCSFunction(this.OnCaptureButtonClick, VoidDelegate, this)
    UIEventListener.Get(this.m_ReplayKitButton).onClick = MakeDelegateFromCSFunction(this.OnReplayKitButtonClick, VoidDelegate, this)
    UIEventListener.Get(this.m_ForumButton).onClick = MakeDelegateFromCSFunction(this.OnForumButtonClick, VoidDelegate, this)
    UIEventListener.Get(this.m_MusicBoxButton).onClick = DelegateFactory.VoidDelegate(
        function(go)
            CLuaMusicBoxMgr:EnableMusicBox(true)
            this:ShowSelf(false)
        end
    )
end

