require("common/common_include")

local UILabel = import "UILabel"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CChatLinkMgr = import "CChatLinkMgr"
local UIEventListener = import "UIEventListener"
local LuaUtils = import "LuaUtils"

CLuaCityWarHistoryWnd = class()
--CLuaCityWarHistoryWnd.Path = "ui/citywar/LuaCityWarHistoryWnd"

function CLuaCityWarHistoryWnd:Init( ... )
	if not CLuaCityWarHistoryWnd.HistoryDataTable then
		CUIManager.CloseUI(CLuaUIResources.CityWarHistoryWnd)
		return
	end
	local gridView = self.transform:Find("Anchor/ListView"):GetComponent(typeof(QnAdvanceGridView))
    gridView.m_DataSource = DefaultTableViewDataSource.Create(function() 
			return #CLuaCityWarHistoryWnd.HistoryDataTable
		end, function(item, index)
			self:InitItem(item.gameObject, index + 1)
		end)
    gridView:ReloadData(true, false)
end

function CLuaCityWarHistoryWnd:InitItem(obj, index)
	local valueTbl = CLuaCityWarHistoryWnd.HistoryDataTable[index]
	local dataTime = CServerTimeMgr.ConvertTimeStampToZone8Time(valueTbl.Time)
	local timeStr = SafeStringFormat3("%d-%02d-%02d", dataTime.Year, dataTime.Month, dataTime.Day)
	local transTbl = {"Time", "Victory", "Enemy"}
	local valTbl = {timeStr, CChatLinkMgr.TranslateToNGUIText(valueTbl.EventStr), valueTbl.GuildName}
	for i = 1, #transTbl do
		obj.transform:Find(transTbl[i]):GetComponent(typeof(UILabel)).text = valTbl[i]
	end
	local statusObj = obj.transform:Find("StatusBtn").gameObject
	statusObj:SetActive(valueTbl.HasResult)
	UIEventListener.Get(statusObj).onClick = LuaUtils.VoidDelegate(function ( ... )
		Gac2Gas.QueryCityWarHistoryPlaySummary(valueTbl.Time)
	end)
end
