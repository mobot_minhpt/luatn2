local CClothespressEmptyTemplate = import "L10.UI.CClothespressEmptyTemplate"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CItemMgr = import "L10.Game.CItemMgr"
local CZuoQiMgr = import "L10.Game.CZuoQiMgr"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local CFreightEquipSubmitMgr = import "L10.UI.CFreightEquipSubmitMgr"

CClothespressEmptyTemplate.m_OnAddVehicleItem_CS2LuaHook = function()
    local dic = CreateFromClass(MakeGenericClass(Dictionary, Int32, MakeGenericClass(List, cs_string)))
    local equipList = CreateFromClass(MakeGenericClass(List, cs_string))
    local bagSize = CClientMainPlayer.Inst.ItemProp:GetPlaceSize(EnumItemPlace.Bag)
    for i = 1, bagSize do
        local id = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Bag, i)
        if not cs_string.IsNullOrEmpty(id) then
            local equip = CItemMgr.Inst:GetById(id)
            if equip and equip.IsItem and (CZuoQiMgr.Inst:GetZuoqiIDByItemTemplateId(equip.TemplateId) > 0)then
                CommonDefs.ListAdd(equipList, typeof(cs_string), equip.Id)
            end
        end
    end
    CommonDefs.DictAdd(dic, typeof(Int32), 0, typeof(MakeGenericClass(List, cs_string)), equipList)
    CFreightEquipSubmitMgr.OpenEquipmentChooseWnd("", dic, LocalString.GetString("包裹中的坐骑道具"), 0, false, LocalString.GetString("添加"), LocalString.GetString("获取更多坐骑"),
            DelegateFactory.Action(function ()
                CUIManager.CloseUI("FreightEquipSubmitWnd")
                CShopMallMgr.SelectMallIndex = 0
                CShopMallMgr.SelectCategory = 3
                CShopMallMgr.SelectSubCategory = 1
                CUIManager.ShowUI("ShoppingMallWnd")
            end)
    , LocalString.GetString("包裹中暂无坐骑道具"))
end