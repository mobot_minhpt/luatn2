-- Auto Generated!!
local BoolDelegate = import "UIEventListener+BoolDelegate"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CommonDefs = import "L10.Game.CommonDefs"
local Constants = import "L10.Game.Constants"
local CTianFuSkillItemCell = import "L10.UI.CTianFuSkillItemCell"
local LocalString = import "LocalString"
local Object = import "System.Object"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
local SkillCategory = import "L10.Game.SkillCategory"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CTianFuSkillItemCell.m_Init_CS2LuaHook = function (this, classId, originLevel, needXiuWeiLevel, iconName, locked, playerLevel, skillProp) 

    this.classId = classId
    this.originLevel = originLevel
    this.locked = locked

    local data = Skill_AllSkills.GetData(CLuaDesignMgr.Skill_AllSkills_GetSkillId(classId, 1))
    local originWithDeltaLevel = skillProp:GetTianFuSkilleltaLevel(classId, playerLevel)

    local totalLevel = originLevel
    if originLevel > 0 and originWithDeltaLevel > 0 then
        totalLevel = (originLevel + originWithDeltaLevel)
        if totalLevel > data._Before_Extend_Level then
            totalLevel = data._Before_Extend_Level
        end
    end

    this.levelLabel.text = totalLevel > 0 and tostring(totalLevel) or nil
    this.levelLabel.gameObject:SetActive(originLevel > 0)
    if iconName == nil then
        this.skillIcon:Clear()
    else
        this.skillIcon:LoadSkillIcon(iconName)
    end

    this.lockedSprite.enabled = locked
    if this.addSprite ~= nil then
        this.addSprite.enabled = not locked and (originLevel == 0)
    end
    if this.unlockLabel ~= nil then
        this.unlockLabel.gameObject:SetActive(true)
        if locked then
            this.unlockLabel.text = tostring(needXiuWeiLevel) .. LocalString.GetString("修开启")
        elseif originLevel == 0 then
            this.unlockLabel.text = LocalString.GetString("已开启")
        else
            this.unlockLabel.gameObject:SetActive(false)
        end
    end
    this.Selected = false
    --Skill_AllSkills data = Skill_AllSkills.GetData(CLuaDesignMgr.Skill_AllSkills_GetSkillId(classId, 1));
    this.ShowPassiveIcon = (data ~= nil and data.ECategory == SkillCategory.Passive)
    if this.nameLabel ~= nil then
        local default
        if data ~= nil then
            default = data.Name
        else
            default = nil
        end
        this.nameLabel.text = (default)
        this.nameLabel.enabled = (originLevel > 0)
    end
    if this.disableSprite ~= nil then
        this.disableSprite.enabled = false
    end
    --不显示了

    if this.m_FeiSheng and this.m_FeiShengLevel ~= originLevel then
        --飞升调整技能等级后，还需要考虑神兵天赋的技能加成
        local totalLevel = this.m_FeiShengLevel
        if totalLevel > 0 and originWithDeltaLevel > 0 then
            totalLevel = (totalLevel + originWithDeltaLevel)
            if totalLevel > data._Before_Extend_Level then
                totalLevel = data._Before_Extend_Level
            end
        end
        this.levelLabel.text = System.String.Format("[c][{0}]{1}[-][/c]", Constants.ColorOfFeiSheng, totalLevel)
    end
end
CTianFuSkillItemCell.m_InitFeiSheng_CS2LuaHook = function (this, active, level) 
    this.m_FeiSheng = active
    this.m_FeiShengLevel = level
end
CTianFuSkillItemCell.m_Start_CS2LuaHook = function (this) 

    UIEventListener.Get(this.gameObject).onDragStart = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.gameObject).onDragStart, MakeDelegateFromCSFunction(this.OnDragIconStart, VoidDelegate, this), true)
    UIEventListener.Get(this.gameObject).onPress = CommonDefs.CombineListner_BoolDelegate(UIEventListener.Get(this.gameObject).onPress, MakeDelegateFromCSFunction(this.OnPressIcon, BoolDelegate, this), true)
end
CTianFuSkillItemCell.m_OnDragIconStart_CS2LuaHook = function (this, go) 

    if this.OnDragStart ~= nil then
        GenericDelegateInvoke(this.OnDragStart, Table2ArrayWithCount({go, this.classId}, 2, MakeArrayClass(Object)))
    end
end
CTianFuSkillItemCell.m_OnPressIcon_CS2LuaHook = function (this, go, pressed) 

    if this.OnPress ~= nil then
        GenericDelegateInvoke(this.OnPress, Table2ArrayWithCount({go, pressed, this.classId}, 3, MakeArrayClass(Object)))
    end
end
CTianFuSkillItemCell.m_GetDragFromGo_CS2LuaHook = function (this) 

    return this.skillIcon.gameObject
end
