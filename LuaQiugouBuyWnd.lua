local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local CUITexture = import "L10.UI.CUITexture"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CPlayerShopMgr = import "L10.UI.CPlayerShopMgr"
local CClientFurnitureMgr = import "L10.Game.CClientFurnitureMgr"
local CItem=import "L10.Game.CItem"

LuaQiugouBuyWnd = class()
RegistChildComponent(LuaQiugouBuyWnd,"CloseButton", GameObject)

--RegistClassMember(LuaQiugouBuyWnd, "maxChooseNum")

function LuaQiugouBuyWnd:Close()
	CUIManager.CloseUI(CLuaUIResources.QiugouBuyWnd)
end

function LuaQiugouBuyWnd:OnEnable()
	--g_ScriptEvent:AddListener("", self, "")
end

function LuaQiugouBuyWnd:OnDisable()
	--g_ScriptEvent:RemoveListener("", self, "")
end

function LuaQiugouBuyWnd:InitButton()
  local reBuyBtn = self.transform:Find('Content1/QnButton1').gameObject
	local onReBuyClick = function(go)
		local str = LocalString.GetString('是否重新求购该物品？')
		MessageWndManager.ShowOKCancelMessage(str, DelegateFactory.Action(function ()
			Gac2Gas.RefreshRequestBuyOrder(tonumber(LuaQiugouMgr.QiugouBuyItem[1]))
			self:Close()
		end), nil, nil, nil, false)
	end
	CommonDefs.AddOnClickListener(reBuyBtn,DelegateFactory.Action_GameObject(onReBuyClick),false)
  local cancelBuyBtn = self.transform:Find('Content1/QnButton').gameObject
	local onCancelClick = function(go)
    Gac2Gas.CancelRequestBuyOrder(tonumber(LuaQiugouMgr.QiugouBuyItem[1]))
    self:Close()
	end
	CommonDefs.AddOnClickListener(cancelBuyBtn,DelegateFactory.Action_GameObject(onCancelClick),false)
end

function LuaQiugouBuyWnd:InitShowData()
  local itemTemplateId = tonumber(LuaQiugouMgr.QiugouBuyItem[2])
  local item = Item_Item.GetData(itemTemplateId)
  self.transform:Find('Content1/ContentLeft/Texture'):GetComponent(typeof(CUITexture)):LoadMaterial(item.Icon)
  local color = CPlayerShopMgr.Inst:GetColor(itemTemplateId)
  self.transform:Find('Content1/ContentLeft/Name'):GetComponent(typeof(UILabel)).color = color
  self.transform:Find('Content1/ContentLeft/Name'):GetComponent(typeof(UILabel)).text = item.Name

  self.transform:Find('Content1/ContentRight/Grid/Label1/Sprite/Label'):GetComponent(typeof(UILabel)).text = LuaQiugouMgr.QiugouBuyItem[3]
  self.transform:Find('Content1/ContentRight/Grid/Label2/Label'):GetComponent(typeof(UILabel)).text = LuaQiugouMgr.QiugouBuyItem[4]
  self.transform:Find('Content1/ContentRight/Grid/Label3/Sprite/Label'):GetComponent(typeof(UILabel)).text = LuaQiugouMgr.QiugouBuyItem[4] * LuaQiugouMgr.QiugouBuyItem[3]


  local zswdataID = nil
	local zswdata = nil
	local des = CItem.GetItemDescription(itemTemplateId,true)
  Zhuangshiwu_Zhuangshiwu.Foreach(function (key,data)
    if data.ItemId == itemTemplateId then
      zswdataID = data.ID
    end
  end)
	if zswdataID then
		zswdata = Zhuangshiwu_Zhuangshiwu.GetData(zswdataID)
	end

	if zswdata then
		self.transform:Find('Content1/ContentLeft/QnScrollLabel/ScrollView/Label'):GetComponent(typeof(UILabel)).text = g_MessageMgr:Format(System.String.Format(des, zswdata.Repair))
		self.transform:Find('Content1/ContentLeft/Level'):GetComponent(typeof(UILabel)).text = String.Format(LocalString.GetString("{0}级{1}"), zswdata.Grade, CClientFurnitureMgr.GetTypeNameByType(zswdata.Type))
	else
		self.transform:Find('Content1/ContentLeft/QnScrollLabel/ScrollView/Label'):GetComponent(typeof(UILabel)).text = g_MessageMgr:Format(des)
		self.transform:Find('Content1/ContentLeft/Level'):GetComponent(typeof(UILabel)).text = ''
	end
end

function LuaQiugouBuyWnd:Init()
	local onCloseClick = function(go)
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.CloseButton,DelegateFactory.Action_GameObject(onCloseClick),false)

  self:InitButton()
  self:InitShowData()
end

return LuaQiugouBuyWnd
