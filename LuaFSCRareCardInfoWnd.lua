local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local Physics = import "UnityEngine.Physics"

LuaFSCRareCardInfoWnd = class()
LuaFSCRareCardInfoWnd.s_CardId = 0
LuaFSCRareCardInfoWnd.s_CardStatus = nil
LuaFSCRareCardInfoWnd.s_Borrowed = false

RegistClassMember(LuaFSCRareCardInfoWnd, "m_FSCCard")
RegistClassMember(LuaFSCRareCardInfoWnd, "m_Name")
RegistClassMember(LuaFSCRareCardInfoWnd, "m_Dot")
RegistClassMember(LuaFSCRareCardInfoWnd, "m_Subject")
RegistClassMember(LuaFSCRareCardInfoWnd, "m_Judgment")
RegistClassMember(LuaFSCRareCardInfoWnd, "m_SKillInfo1")
RegistClassMember(LuaFSCRareCardInfoWnd, "m_SKillInfo2")

function LuaFSCRareCardInfoWnd:Awake()
    self.m_FSCCard = self.transform:Find("Background/FSCCard"):GetComponent(typeof(CCommonLuaScript))
    self.m_Name = self.transform:Find("Background/Info/Name"):GetComponent(typeof(UILabel))
    self.m_Dot = self.transform:Find("Background/Info/Name/Name"):GetComponent(typeof(UILabel))
    self.m_Subject = self.transform:Find("Background/Info/Name/Name (1)"):GetComponent(typeof(UILabel))
    self.m_Judgment = self.transform:Find("Background/Info/Judgment"):GetComponent(typeof(UILabel))
    self.m_SKillInfo1 = self.transform:Find("Background/Info/SKillInfo1"):GetComponent(typeof(UILabel))
    self.m_SKillInfo2 = self.transform:Find("Background/Info/SKillInfo2"):GetComponent(typeof(UILabel))
end

function LuaFSCRareCardInfoWnd:Init()
    if LuaFSCRareCardInfoWnd.s_CardId > 0 then
        self.m_FSCCard.m_LuaSelf:Init(LuaFSCRareCardInfoWnd.s_CardId, true, LuaFSCRareCardInfoWnd.s_CardStatus, LuaFSCRareCardInfoWnd.s_Borrowed)
        local data = LuaFourSeasonCardMgr:GetCardData(LuaFSCRareCardInfoWnd.s_CardId)
        local color = LuaFourSeasonCardMgr:GetSeasonColor(data.Season)
        self.m_Name.color = color
        self.m_Dot.color = color
        self.m_Subject.color = color
        self.m_Name.text = data.Title
        self.m_Subject.text = LuaFourSeasonCardMgr:GetSubject(data.SpecialTitle)
        self.m_Judgment.text = data.SpecialDescription
        local skill = ZhouNianQing2023_Cards.GetData(LuaFSCRareCardInfoWnd.s_CardId).Skillid
        if skill.Length > 0 then
            self.m_SKillInfo1.gameObject:SetActive(true)
            self.m_SKillInfo1.text = ZhouNianQing2023_PreciousSkill.GetData(skill[0]).SkillDescription
        else
            self.m_SKillInfo1.gameObject:SetActive(false)
        end
        if skill.Length > 1 then
            self.m_SKillInfo2.gameObject:SetActive(true)
            self.m_SKillInfo2.text = ZhouNianQing2023_PreciousSkill.GetData(skill[1]).SkillDescription
        else
            self.m_SKillInfo2.gameObject:SetActive(false)
        end
    end
end

function LuaFSCRareCardInfoWnd:Update()
    if Input.GetMouseButtonDown(0) then
        local cam = UICamera.currentCamera
        local hits = Physics.RaycastAll(cam:ScreenPointToRay(Input.mousePosition), cam.farClipPlane, cam.cullingMask)
        if hits then
            for i = 0, hits.Length - 1 do
                if NGUITools.IsChild(self.transform, hits[i].collider.transform) then
                    return
                end
            end
            CUIManager.CloseUI(CLuaUIResources.FSCRareCardInfoWnd)
        end
    end
end
