local UILabel = import "UILabel"
local CUIFx = import "L10.UI.CUIFx"
local Color = import "UnityEngine.Color"
local NGUIText = import "NGUIText"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

CLuaStarBiwuFinalWatchWnd = class()
CLuaStarBiwuFinalWatchWnd.Path = "ui/starbiwu/LuaStarBiwuFinalWatchWnd"
RegistClassMember(CLuaStarBiwuFinalWatchWnd, "m_CurrentMatchIndex")
RegistClassMember(CLuaStarBiwuFinalWatchWnd, "m_SelfZhanduiId")
RegistClassMember(CLuaStarBiwuFinalWatchWnd, "m_Name1LabelTable")
RegistClassMember(CLuaStarBiwuFinalWatchWnd, "m_Name2LabelTable")
RegistClassMember(CLuaStarBiwuFinalWatchWnd, "m_Score1LabelTable")
RegistClassMember(CLuaStarBiwuFinalWatchWnd, "m_Score2LabelTable")
RegistClassMember(CLuaStarBiwuFinalWatchWnd, "m_MatchingFxTable")

-- For default name
RegistClassMember(CLuaStarBiwuFinalWatchWnd, "m_MatchIndexTable")
RegistClassMember(CLuaStarBiwuFinalWatchWnd, "m_PreMatchIsWinTable")

RegistClassMember(CLuaStarBiwuFinalWatchWnd, "m_RedColor")
RegistClassMember(CLuaStarBiwuFinalWatchWnd, "m_BlueColor")
RegistClassMember(CLuaStarBiwuFinalWatchWnd, "m_CurrentDateColor")

function CLuaStarBiwuFinalWatchWnd:Awake()
    Gac2Gas.QueryStarBiwuZongJueSaiMatchInfo()
    self.m_RedColor = NGUIText.ParseColor24("ec7676", 0)
    self.m_BlueColor = NGUIText.ParseColor24("4a8eff", 0)
    self.m_CurrentDateColor = NGUIText.ParseColor24("fff68f", 0)
    self.m_CurrentMatchIndex = 0
    self.m_SelfZhanduiId = 0
    self.m_Name1LabelTable = {}
    self.m_Name2LabelTable = {}
    self.m_Score2LabelTable =  {}
    self.m_Score1LabelTable = {}
    self.m_MatchingFxTable = {}
    for i = 1, 14 do
        local rootTrans = self.transform:Find("Offset/Status/"..i)
        local name1Label = rootTrans:Find("Name1"):GetComponent(typeof(UILabel))
        name1Label.text = ""
        local name2Label = rootTrans:Find("Name2"):GetComponent(typeof(UILabel))
        name2Label.text = ""
        local score1Label = rootTrans:Find("Score1"):GetComponent(typeof(UILabel))
        score1Label.text = ""
        local score2Label = rootTrans:Find("Score2"):GetComponent(typeof(UILabel))
        score2Label.text = ""
        local fx = rootTrans:Find("Fx"):GetComponent(typeof(CUIFx))
        table.insert(self.m_Name1LabelTable, name1Label)
        table.insert(self.m_Name2LabelTable, name2Label)
        table.insert(self.m_Score1LabelTable, score1Label)
        table.insert(self.m_Score2LabelTable, score2Label)
        table.insert(self.m_MatchingFxTable, fx)
    end
    -- from 5 to 14
    self.m_MatchIndexTable = {1, 2, 3, 4, 1, 2, 3, 4, 5, 8, 6, 7, 7, 8, 9, 10, 11, 12, 11, 13}
    self.m_PreMatchIsWinTable = {0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1}

    UIEventListener.Get(self.transform:Find("Offset/Buttons/WatchBtn").gameObject).onClick = LuaUtils.VoidDelegate(function()
        Gac2Gas.RequestWatchStarBiwuZongJueSai(self.m_CurrentMatchIndex)
    end)

    UIEventListener.Get(self.transform:Find("Offset/Buttons/RefreshBtn").gameObject).onClick = LuaUtils.VoidDelegate(function()
        Gac2Gas.QueryStarBiwuZongJueSaiMatchInfo()
    end)

    -- init date
    local zone8 = CServerTimeMgr.Inst:GetZone8Time()
    local month, day = zone8.Month, zone8.Day
    local finalTime = StarBiWuShow_Setting.GetData().FinalStageTime
    for i = 1, 3 do
        local isCurrentDay = month == finalTime[(i - 1) * 2] and day == finalTime[(i - 1) * 2 + 1]
        local root = self.transform:Find("Offset/Title/"..i)
        root:GetComponent(typeof(UISprite)).spriteName = isCurrentDay and "common_btn_03_highlight" or "common_btn_03_normal"
        local label = root:Find("Label"):GetComponent(typeof(UILabel))
        label.text = SafeStringFormat3(LocalString.GetString("%d月%d日%s"), finalTime[(i - 1) * 2], finalTime[(i - 1) * 2 + 1], isCurrentDay and LocalString.GetString("-进行中") or "")
        label.color = isCurrentDay and self.m_CurrentDateColor or Color.white
    end
end

function CLuaStarBiwuFinalWatchWnd:Init()
end

function CLuaStarBiwuFinalWatchWnd:ReplyStarBiwuZongJueSaiMatch(currentMatchPlayIdx, selfZhanduiId, matchDataUd, syncType, canGetReward)
    -- 当前正在比赛的playIdx,这个值的范围目前对应的是 0-14,也是观战请求 RequestWatchStarBiwuZongJueSai 的参数
    -- 0表示当前没有正在进行的比赛
    self.m_CurrentMatchIndex = currentMatchPlayIdx

    -- 自己的战队id,0表示自己不处于战队中
    self.m_SelfZhanduiId = selfZhanduiId

    -- 对阵信息
    local matchDataList = MsgPackImpl.unpack(matchDataUd)

    local index = 1
    for i = 0, matchDataList.Count - 1, 6 do
      local attackerZId   = matchDataList[i]    -- 战队id
      local attackerZName = matchDataList[i+1]  -- 战队名
      local attackerPoint = matchDataList[i+2]  -- 战队得分,attackerPoint 和 defenderPoint都是0 就不显示

      local defenderZId   = matchDataList[i+3]  -- 战队id
      local defenderZName = matchDataList[i+4]  -- 战队名
      local defenderPoint = matchDataList[i+5]  -- 战队得分,attackerPoint 和 defenderPoint都是0 就不显示

      self:RefreshZhandui(index, attackerZId, attackerZName, attackerPoint, defenderZId, defenderZName, defenderPoint)
      index = index + 1
    end
end

function CLuaStarBiwuFinalWatchWnd:RefreshZhandui(index, id1, name1, score1, id2, name2, score2)
    local hasAlpha1 = false
    local hasAlpha2 = false
    if index >= 5 then
        local iindex = (index - 5) * 2 + 1
        if not name1 or name1 == "" then
            name1 = SafeStringFormat3(LocalString.GetString("第%s场%s者"), self.m_MatchIndexTable[iindex], self.m_PreMatchIsWinTable[iindex] == 1 and LocalString.GetString("胜") or LocalString.GetString("败"))
            hasAlpha1 = true
        end

        if not name2 or name2 == "" then
            name2 = SafeStringFormat3(LocalString.GetString("第%s场%s者"), self.m_MatchIndexTable[iindex + 1], self.m_PreMatchIsWinTable[iindex + 1] == 1 and LocalString.GetString("胜") or LocalString.GetString("败"))
            hasAlpha2 = true
        end
    end

    if index == self.m_CurrentMatchIndex then
        self.m_MatchingFxTable[index]:LoadFx("fx/ui/prefab/UI_duizhanzhuangtai.prefab")
        self.m_Score1LabelTable[index].text = ""
        self.m_Score2LabelTable[index].text = ""
    else
        self.m_MatchingFxTable[index]:DestroyFx()
        if score1 == 0 and score2 == 0 then
            self.m_Score1LabelTable[index].text = ""
            self.m_Score2LabelTable[index].text = ""
        else
            self.m_Score1LabelTable[index].text = score1
            self.m_Score2LabelTable[index].text = score2
            self.m_Score1LabelTable[index].color = score1 > score2 and self.m_BlueColor or self.m_RedColor
            self.m_Score2LabelTable[index].color = score1 <= score2 and self.m_BlueColor or self.m_RedColor
            if score1 > score2 then
                hasAlpha2 = true
            else
                hasAlpha1 = true
            end
        end
    end

    self.m_Name1LabelTable[index].text = name1
    self.m_Name1LabelTable[index].color = id1 == self.m_SelfZhanduiId and Color.green or Color.white
    self.m_Name1LabelTable[index].alpha = hasAlpha1 and 0.3 or 1
    self.m_Name2LabelTable[index].text = name2
    self.m_Name2LabelTable[index].color = id2 == self.m_SelfZhanduiId and Color.green or Color.white
    self.m_Name2LabelTable[index].alpha = hasAlpha2 and 0.3 or 1

    UIEventListener.Get(self.m_Name1LabelTable[index].gameObject).onClick = DelegateFactory.VoidDelegate(function(go) 
        CLuaStarBiwuMgr:ShowZhanDuiMemberWnd(0, id1, 1)
    end)
    UIEventListener.Get(self.m_Name2LabelTable[index].gameObject).onClick = DelegateFactory.VoidDelegate(function(go) 
        CLuaStarBiwuMgr:ShowZhanDuiMemberWnd(0, id2, 1)
    end)
end

function CLuaStarBiwuFinalWatchWnd:OnEnable()
    g_ScriptEvent:AddListener("ReplyStarBiwuZongJueSaiMatch", self, "ReplyStarBiwuZongJueSaiMatch")
end

function CLuaStarBiwuFinalWatchWnd:OnDisable()
    g_ScriptEvent:RemoveListener("ReplyStarBiwuZongJueSaiMatch", self, "ReplyStarBiwuZongJueSaiMatch")
end
