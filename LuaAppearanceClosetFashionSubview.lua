local CUITexture = import "L10.UI.CUITexture"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local Extensions = import "Extensions"
local CButton = import "L10.UI.CButton"
local EnumGender = import "L10.Game.EnumGender"
local UISprite = import "UISprite"
local UISlider = import "UISlider"
local CCustomSelector = import "L10.UI.CCustomSelector"
local CPopupMenuInfoMgrAlignType = import "L10.UI.CPopupMenuInfoMgr+AlignType"
local CUIFx = import "L10.UI.CUIFx"

LuaAppearanceClosetFashionSubview = class()

RegistChildComponent(LuaAppearanceClosetFashionSubview,"m_ClosetFashionItem", "ClosetFashionItem", GameObject)
RegistChildComponent(LuaAppearanceClosetFashionSubview,"m_ContentTable", "ContentTable", UITable)
RegistChildComponent(LuaAppearanceClosetFashionSubview,"m_ContentScrollView", "ContentScrollView", UIScrollView)
RegistChildComponent(LuaAppearanceClosetFashionSubview, "m_SortSelector", "SortSelector", CCustomSelector)

RegistClassMember(LuaAppearanceClosetFashionSubview, "m_SortNames")
RegistClassMember(LuaAppearanceClosetFashionSubview, "m_SortIndex")
RegistClassMember(LuaAppearanceClosetFashionSubview, "m_MyFashions")
RegistClassMember(LuaAppearanceClosetFashionSubview, "m_SelectedDataId")
RegistClassMember(LuaAppearanceClosetFashionSubview, "m_PreviewType")

function LuaAppearanceClosetFashionSubview:Awake()
end

function LuaAppearanceClosetFashionSubview:InitSortSelector()
    self.m_SortSelector.gameObject:SetActive(true)
    self.m_SortNames = LuaAppearancePreviewMgr:GetMyFashionInfoSortNames()
    local names = Table2List(self.m_SortNames, MakeGenericClass(List,cs_string))
    self.m_SortSelector:Init(names, nil, DelegateFactory.Action_int(function ( index )
        self.m_SortIndex = index + 1
		self.m_SortSelector:SetName(names[index])
        self:LoadData()
    end))
    self.m_SortIndex = 1
    self.m_SortSelector:SetName(self.m_SortNames[self.m_SortIndex])
end

function LuaAppearanceClosetFashionSubview:Init(type)
    self.m_PreviewType = type
    self:InitSortSelector()
    self:SetDefaultSelection()
    self:LoadData()
end

function LuaAppearanceClosetFashionSubview:LoadData()
    self.m_MyFashions = LuaAppearancePreviewMgr:GetMyFashionInfo(self.m_PreviewType, self.m_SortIndex)
    Extensions.RemoveAllChildren(self.m_ContentTable.transform)
    self.m_ContentTable.gameObject:SetActive(true)

    for i = 1, #self.m_MyFashions+1 do --末尾增加一个空的item用于打开包裹中的时装道具小窗
        local child = CUICommonDef.AddChild(self.m_ContentTable.gameObject, self.m_ClosetFashionItem)
        child:SetActive(true)
        self:InitItem(child, self.m_MyFashions[i])
        self:InitItemDurabilityFx(child, self.m_MyFashions[i], i<= #self.m_MyFashions and self.m_SelectedDataId==self.m_MyFashions[i].id)
    end
   
    self.m_ContentTable:Reposition()
    self.m_ContentScrollView:ResetPosition()
    self:InitSelection()
end

function LuaAppearanceClosetFashionSubview:SetDefaultSelection()
    self.m_SelectedDataId = LuaAppearancePreviewMgr:IsShowFashion(self.m_PreviewType, false) and LuaAppearancePreviewMgr:GetCurrentInUseFashion(self.m_PreviewType) or 0
end

function LuaAppearanceClosetFashionSubview:InitSelection()
    if self.m_SelectedDataId == 0 then
        self:OnItemClick(nil, false)
        return
    end

    local childCount = self.m_ContentTable.transform.childCount
    for i=0, childCount-1 do
        local childGo = self.m_ContentTable.transform:GetChild(i).gameObject
        local appearanceData = self.m_MyFashions[i+1]
        if appearanceData and appearanceData.id == self.m_SelectedDataId then
            self:OnItemClick(childGo, false)
            break
        end
    end
end

function LuaAppearanceClosetFashionSubview:InitItem(itemGo, appearanceData)
    local iconTexture = itemGo.transform:Find("Item"):GetComponent(typeof(CUITexture))
    local borderSprite = itemGo.transform:Find("Item/Border"):GetComponent(typeof(UISprite))
    local cornerGo = itemGo.transform:Find("Item/Corner").gameObject
    local nameLabel = itemGo.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local timeLabel = itemGo.transform:Find("TimeLabel"):GetComponent(typeof(UILabel))
    local progressLabel = itemGo.transform:Find("ProgressLabel"):GetComponent(typeof(UILabel))
    local progressBar = itemGo.transform:Find("ProgressBar"):GetComponent(typeof(UISlider))
    local addGo = itemGo.transform:Find("Item/Add").gameObject
    local disabledGo = itemGo.transform:Find("Item/Disabled").gameObject
    local lockedGo = itemGo.transform:Find("Item/Locked").gameObject

    if appearanceData == nil then
        addGo:SetActive(true)
        disabledGo:SetActive(false)
        lockedGo:SetActive(false)
        iconTexture:Clear()
        cornerGo:SetActive(false)
        nameLabel.text = ""
        timeLabel.text = ""
        progressLabel.text = ""
        progressBar.gameObject:SetActive(false)
        UIEventListener.Get(itemGo).onClick = DelegateFactory.VoidDelegate(function(go)
            self:OnItemClick(go)
        end)
        return
    end

    iconTexture:LoadMaterial(appearanceData.icon)
    borderSprite.spriteName = LuaAppearancePreviewMgr:GetFashionQualityBorder(appearanceData.quality)
    itemGo:GetComponent(typeof(CButton)).Selected = (self.m_SelectedDataId and self.m_SelectedDataId==appearanceData.id or false)
    cornerGo:SetActive(LuaAppearancePreviewMgr:IsCurrentInUseFashion(self.m_PreviewType, appearanceData.id))
    nameLabel.text = appearanceData.name
    local mainInfo = LuaAppearancePreviewMgr:GetFashionMainInfo(appearanceData.id)
    timeLabel.text = LuaAppearancePreviewMgr:GetFashionExpireText(mainInfo)
    if appearanceData.durability > 0 and mainInfo.IsUnLock==0 then --可解锁永久并且尚未解锁
        progressLabel.text = SafeStringFormat3(LocalString.GetString("解锁进度 %d/%d"), mainInfo.Progress, appearanceData.durability)
        progressBar.gameObject:SetActive(true)
        progressBar.value = mainInfo.Progress/appearanceData.durability
    else --不可解锁永久
        progressLabel.text = ""
        progressBar.gameObject:SetActive(false)
    end  
    addGo:SetActive(false)  
    disabledGo:SetActive(not LuaAppearancePreviewMgr:IsMainPlayerFashionAvailable(appearanceData.id))
    lockedGo:SetActive(false)
    UIEventListener.Get(itemGo).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnItemClick(go, true)
    end)
end

function LuaAppearanceClosetFashionSubview:InitItemDurabilityFx(itemGo, appearanceData, selected)
    local durabilityFx = itemGo.transform:Find("Fx_Full").gameObject
    --local cuifx = itemGo.transform:Find("Fx_Full/CUIFx")
    --if cuifx then
    --    cuifx:GetComponent(typeof(CUIFx)).ScrollView = self.m_ContentScrollView
    --end
    if appearanceData == nil then
        durabilityFx:SetActive(false)
        return
    end
    local mainInfo = LuaAppearancePreviewMgr:GetFashionMainInfo(appearanceData.id)
    durabilityFx:SetActive(not selected and appearanceData.durability > 0 and mainInfo.IsUnLock==0 and mainInfo.Progress>=appearanceData.durability)
end

function LuaAppearanceClosetFashionSubview:OnItemClick(go, isClick)
    local childCount = self.m_ContentTable.transform.childCount
    if go == self.m_ContentTable.transform:GetChild(childCount-1).gameObject then
        LuaAppearancePreviewMgr:ShowPackageFashionItems(self.m_PreviewType)
        return
    end
    
    for i=0, childCount-2 do
        local childGo = self.m_ContentTable.transform:GetChild(i).gameObject
        if childGo == go then
            childGo.transform:GetComponent(typeof(CButton)).Selected = true
            self.m_SelectedDataId = self.m_MyFashions[i+1].id
            if isClick then
                self:ShowPopupMenu(go, self.m_MyFashions[i+1])
            end
            self:InitItemDurabilityFx(childGo, self.m_MyFashions[i+1], true)
        else
            childGo.transform:GetComponent(typeof(CButton)).Selected = false
            self:InitItemDurabilityFx(childGo, self.m_MyFashions[i+1], false)
        end
    end
end

function LuaAppearanceClosetFashionSubview:Delete(appearanceData)
    local context = appearanceData.name
    MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("Fashion_Discard_Notice",context), DelegateFactory.Action(function ()
        LuaAppearancePreviewMgr:RequestDiscardFashion_Permanent(appearanceData.id)
    end), nil, nil, nil, false)
end

function LuaAppearanceClosetFashionSubview:ItemGet(go, itemId)
    LuaItemAccessListMgr:ShowItemAccessInfoAtLeft(itemId, true, go.transform)
end

function LuaAppearanceClosetFashionSubview:ShowPopupMenu(go, appearanceData)
    --更换/脱下/丢弃/续费/获取/解锁永久
    local takeOnMenu = g_PopupMenuMgr:CreateOneDefaultPopupMenuItem(LocalString.GetString("更换"), function()
        LuaAppearancePreviewMgr:RequestTakeOnFashion_Permanent(appearanceData.id) 
    end, false)
    local takeOffMenu = g_PopupMenuMgr:CreateOneDefaultPopupMenuItem(LocalString.GetString("脱下"), function() LuaAppearancePreviewMgr:RequestTakeOffFashion_Permanent(appearanceData.id) end, false)
    local deleteMenu = g_PopupMenuMgr:CreateOneDefaultPopupMenuItem(LocalString.GetString("丢弃"), function() self:Delete(appearanceData) end, false)
    local renewalMenu = g_PopupMenuMgr:CreateOneDefaultPopupMenuItem(LocalString.GetString("续费"), function() LuaAppearancePreviewMgr:RenewalFashion(appearanceData.id) end, false)
    local itemGetMenu = g_PopupMenuMgr:CreateOneDefaultPopupMenuItem(LocalString.GetString("获取"), function() self:ItemGet(go, appearanceData.itemGetId) end, false)
    local unlockMenu = g_PopupMenuMgr:CreateOneOrangePopupMenuItem(LocalString.GetString("解锁永久"), function() LuaAppearancePreviewMgr:ShowFashionUnlockWnd(appearanceData.id) end, false)
    
    local tbl = {}
    local mainInfo = LuaAppearancePreviewMgr:GetFashionMainInfo(appearanceData.id)
    --无论是否过期，只要满足进度需求就显示解锁按钮
    if appearanceData.durability>0 and mainInfo.IsUnLock==0 and mainInfo.Progress>=appearanceData.durability then
        table.insert(tbl, unlockMenu)
    end
    if LuaAppearancePreviewMgr:IsMainPlayerFashionAvailable(appearanceData.id) then
        if LuaAppearancePreviewMgr:IsCurrentInUseFashion(self.m_PreviewType, appearanceData.id) then
            table.insert(tbl, takeOffMenu)
        else
            table.insert(tbl, takeOnMenu)
        end
    elseif appearanceData.canRenewal then
        table.insert(tbl, renewalMenu)
    end
    if appearanceData.itemGetId>0 then
        if mainInfo.IsUnLock~=1 and mainInfo.ExpireTime>0 then --已经解锁的永久时装，或者没有有效期限制的时装，不显示获取按钮
            table.insert(tbl, itemGetMenu)
        end
    end
    if mainInfo.IsUnLock~=1 and mainInfo.ExpireTime>0  then --已解锁永久，或者本身没有过期时间的时装，不显示删除
        table.insert(tbl, deleteMenu)
    end
    g_PopupMenuMgr:ShowPopupMenuOnBottom(tbl, go.transform)
end

function LuaAppearanceClosetFashionSubview:OnEnable()
    g_ScriptEvent:AddListener("OnSyncMainPlayerAppearanceProp",self,"SyncMainPlayerAppearancePropUpdate")
    g_ScriptEvent:AddListener("OnSyncWardrobeProperty",self,"OnSyncWardrobeProperty")
    g_ScriptEvent:AddListener("OnSetFashionHideSuccess_Permanent",self,"OnSetFashionHideSuccess_Permanent")
    g_ScriptEvent:AddListener("OnFreightSubmitEquipSelected", self, "OnAddFashionByItem")
end

function LuaAppearanceClosetFashionSubview:OnDisable()
    g_ScriptEvent:RemoveListener("OnSyncMainPlayerAppearanceProp",self,"SyncMainPlayerAppearancePropUpdate")
    g_ScriptEvent:RemoveListener("OnSyncWardrobeProperty",self,"OnSyncWardrobeProperty")
    g_ScriptEvent:RemoveListener("OnSetFashionHideSuccess_Permanent",self,"OnSetFashionHideSuccess_Permanent")
    g_ScriptEvent:RemoveListener("OnFreightSubmitEquipSelected", self, "OnAddFashionByItem")
end

--仙凡切换 或其他sync appearance情况
function LuaAppearanceClosetFashionSubview:SyncMainPlayerAppearancePropUpdate()
    self:LoadData()
end

function LuaAppearanceClosetFashionSubview:OnSyncWardrobeProperty(reason)
    self:LoadData()
end

function LuaAppearanceClosetFashionSubview:OnSetFashionHideSuccess_Permanent(pos,value)
    if pos == self.m_PreviewType then
        self:SetDefaultSelection()
        self:LoadData()
    end
end

function LuaAppearanceClosetFashionSubview:OnAddFashionByItem(args)
    local itemId = args[0]
    local key = args[1]
    LuaAppearancePreviewMgr:AddFashionByItem(key, itemId)
end

function LuaAppearanceClosetFashionSubview:GetGuideGoByAppearanceWnd()
    for i,appearanceData in pairs(self.m_MyFashions) do
        local mainInfo = LuaAppearancePreviewMgr:GetFashionMainInfo(appearanceData.id)
        --无论是否过期，只要满足进度需求就显示解锁按钮
        if appearanceData.durability>0 and mainInfo.IsUnLock==0 and mainInfo.Progress>=appearanceData.durability then
            return self.m_ContentTable.transform:GetChild(i-1).gameObject
        end
    end
    return nil
end

