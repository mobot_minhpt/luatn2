-- Auto Generated!!
local AlignType = import "L10.UI.CTooltip+AlignType"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CExpressionItem = import "L10.UI.CExpressionItem"
local CExpressionMgr = import "L10.Game.CExpressionMgr"
local CExpressionWnd = import "L10.UI.CExpressionWnd"
local CIMMgr = import "L10.Game.CIMMgr"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CPersonalSpace_BaseRet = import "L10.Game.CPersonalSpace_BaseRet"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local ExpressionHead_Expression = import "L10.Game.ExpressionHead_Expression"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CExpressionWnd.m_Init_CS2LuaHook = function (this) 
    this:Refresh()


    this.tableView.m_DataSource = this
    this.tableView.OnSelectAtRow = MakeDelegateFromCSFunction(this.OnSelectAtRow, MakeGenericClass(Action1, Int32), this)

    this:InitExpressionTxt()
    this:InitSticker()
    Gac2Gas.QueryValidExpression()
end
CExpressionWnd.m_OnSelectAtRow_CS2LuaHook = function (this, row) 
    if CClientMainPlayer.Inst ~= nil then
        local expression = this.dataSource[row]
        local expressionId = CIMMgr.Inst:Expression2ExpressionId(expression, EnumToInt(CClientMainPlayer.Inst.Class), EnumToInt(CClientMainPlayer.Inst.Gender))
        local data = ExpressionHead_Expression.GetData(expressionId)
        if data ~= nil then
            this.descLabel.text = data.Descrition
            this.icon:LoadNPCPortrait(CUICommonDef.GetPortraitName(CClientMainPlayer.Inst.Class, CClientMainPlayer.Inst.Gender, expression), false)
        else
            this.descLabel.text = nil
            this.icon:Clear()
        end
        if CExpressionMgr.Inst:HasGetExpression(expression) then
            this.saveLabel.text = LocalString.GetString("保存")
            UIEventListener.Get(this.saveBtn).onClick = MakeDelegateFromCSFunction(this.OnClickSaveBtn, VoidDelegate, this)
        else
            this.saveLabel.text = LocalString.GetString("获取")
            UIEventListener.Get(this.saveBtn).onClick = MakeDelegateFromCSFunction(this.OnGet, VoidDelegate, this)
        end
    end
end
CExpressionWnd.m_Refresh_CS2LuaHook = function (this) 
    --初始化数据
    --初始化数据
    if CClientMainPlayer.Inst ~= nil then
        this.icon:LoadNPCPortrait(CClientMainPlayer.Inst.PortraitName, false)
        local expression = CClientMainPlayer.Inst.BasicProp.Expression
        local expressionId = CIMMgr.Inst:Expression2ExpressionId(expression, EnumToInt(CClientMainPlayer.Inst.Class), EnumToInt(CClientMainPlayer.Inst.Gender))
        local data = ExpressionHead_Expression.GetData(expressionId)
        if data ~= nil then
            this.descLabel.text = data.Descrition
        else
            this.descLabel.text = nil
        end
        --MessageMgr.Inst.ShowMessage("EXPRESSION_SAVE_SUCCESS");
    end
end
CExpressionWnd.m_SortExpression_CS2LuaHook = function (this, p1, p2) 
    local b1 = CExpressionMgr.Inst:HasGetExpression(p1)
    local b2 = CExpressionMgr.Inst:HasGetExpression(p2)
    if b1 and not b2 then
        return - 1
    elseif not b1 and b2 then
        return 1
    elseif p1 > p2 then
        return 1
    elseif p1 < p2 then
        return - 1
    end
    return 0
end
CExpressionWnd.m_OnClickSaveBtn_CS2LuaHook = function (this, go) 
    local index = this.tableView.currentSelectRow
    local id = this.dataSource[index]
    --gac2gas
    Gac2Gas.SetExpression(CIMMgr.Inst:ExpressionId2Expression(id))
    if CClientMainPlayer.Inst ~= nil then
        local expression = id
        -- CClientMainPlayer.Inst.BasicProp.Expression;
        this.mExpression = id
        local expressionTxtId = CClientMainPlayer.Inst.BasicProp.ExpressionTxt
        local expression_txt_offset_x = CExpressionMgr.Inst.expression_extra.expression_txt_offset_x
        local expression_txt_offset_y = CExpressionMgr.Inst.expression_extra.expression_txt_offset_y
        local expression_txt_scale_x = CExpressionMgr.Inst.expression_extra.expression_txt_scale_x
        local expression_txt_scale_y = CExpressionMgr.Inst.expression_extra.expression_txt_scale_y
        local expression_txt_rotation = CExpressionMgr.Inst.expression_extra.expression_txt_rotation

        local stickerId = CClientMainPlayer.Inst.BasicProp.Sticker
        local profileFrameId = CClientMainPlayer.Inst.BasicProp.ProfileInfo.Frame
        local sticker_offset_x = CExpressionMgr.Inst.expression_extra.sticker_offset_x
        local sticker_offset_y = CExpressionMgr.Inst.expression_extra.sticker_offset_y
        local sticker_scale_x = CExpressionMgr.Inst.expression_extra.sticker_scale_x
        local sticker_scale_y = CExpressionMgr.Inst.expression_extra.sticker_scale_y
        local sticker_rotation = CExpressionMgr.Inst.expression_extra.sticker_rotation

        CPersonalSpaceMgr.Inst:SetExpression(expression, expressionTxtId, stickerId, profileFrameId, expression_txt_offset_x, expression_txt_offset_y, expression_txt_scale_x, expression_txt_scale_y, expression_txt_rotation, sticker_offset_x, sticker_offset_y, sticker_scale_x, sticker_scale_y, sticker_rotation, MakeDelegateFromCSFunction(this.OnSetSuccess, MakeGenericClass(Action1, CPersonalSpace_BaseRet), this), nil)
    end
end
CExpressionWnd.m_OnGet_CS2LuaHook = function (this, go) 
    if this.tableView.currentSelectRow > - 1 and this.tableView.currentSelectRow < this.dataSource.Count then
        local expression = this.dataSource[this.tableView.currentSelectRow]
        local expressionId = CIMMgr.Inst:Expression2ExpressionId(expression, EnumToInt(CClientMainPlayer.Inst.Class), EnumToInt(CClientMainPlayer.Inst.Gender))
        local data = ExpressionHead_Expression.GetData(expressionId)
        if data ~= nil then
            local templateId = data.ItemID
            CItemAccessListMgr.Inst:ShowItemAccessInfo(templateId, false, this.saveBtn.transform, AlignType.Right)
        end
    end
end
CExpressionWnd.m_OnQueryValidExpressionResult_CS2LuaHook = function (this) 
    this.tableView:ReloadData(true, false)
    if CClientMainPlayer.Inst == nil then
        return
    end
    local currentExpression = CClientMainPlayer.Inst.BasicProp.Expression
    local index = CommonDefs.ListIndexOf(this.dataSource, currentExpression)
    this.tableView:SetSelectRow(index, true)

    this:UpdateExpressionCount()
end
CExpressionWnd.m_ItemAt_CS2LuaHook = function (this, view, row) 
    if row < this.dataSource.Count then
        local cmp = TypeAs(view:GetFromPool(0), typeof(CExpressionItem))
        cmp:Init(this.dataSource[row])
        return cmp
    end
    return nil
end
