local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CButton = import "L10.UI.CButton"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local UISlider = import "UISlider"
local PathType = import "DG.Tweening.PathType"
local PathMode = import "DG.Tweening.PathMode"
local Ease = import "DG.Tweening.Ease"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local Utility = import "L10.Engine.Utility"
local CEffectMgr = import "L10.Game.CEffectMgr"
local CameraClearFlags = import "UnityEngine.CameraClearFlags"
local LayerDefine = import "L10.Engine.LayerDefine"
local Quaternion = import "UnityEngine.Quaternion"
local Vector3 = import "UnityEngine.Vector3"
local CPreDrawMgr = import "L10.Engine.CPreDrawMgr"
local RenderTexture = import "UnityEngine.RenderTexture"
local RenderTextureFormat = import "UnityEngine.RenderTextureFormat"
local RenderTextureReadWrite = import "UnityEngine.RenderTextureReadWrite"
local TextureWrapMode = import "UnityEngine.TextureWrapMode"
local FilterMode = import "UnityEngine.FilterMode"
local Camera = import "UnityEngine.Camera"
local CMainCamera = import "L10.Engine.CMainCamera"
local CCenterCountdownWnd = import "L10.UI.CCenterCountdownWnd"

LuaTaoQuan2Wnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaTaoQuan2Wnd, "closeBtn", "closeBtn", CButton)
RegistChildComponent(LuaTaoQuan2Wnd, "showTexture", "showTexture", UITexture)
RegistChildComponent(LuaTaoQuan2Wnd, "quanNode", "quanNode", GameObject)
RegistChildComponent(LuaTaoQuan2Wnd, "leftSide", "leftSide", GameObject)
RegistChildComponent(LuaTaoQuan2Wnd, "rightSide", "rightSide", GameObject)
RegistChildComponent(LuaTaoQuan2Wnd, "throwBtn", "throwBtn", CButton)
RegistChildComponent(LuaTaoQuan2Wnd, "timeLabel", "timeLabel", UILabel)
RegistChildComponent(LuaTaoQuan2Wnd, "liduSlider", "liduSlider", UISlider)
RegistChildComponent(LuaTaoQuan2Wnd, "thumb", "thumb", GameObject)
RegistChildComponent(LuaTaoQuan2Wnd, "mid", "mid", UITexture)

--@endregion RegistChildComponent end
RegistClassMember(LuaTaoQuan2Wnd, "game2CameraPos")
RegistClassMember(LuaTaoQuan2Wnd, "game2CameraRotation")
RegistClassMember(LuaTaoQuan2Wnd, "cameraNode")
RegistClassMember(LuaTaoQuan2Wnd, "cameraScript")
RegistClassMember(LuaTaoQuan2Wnd, "moveDir")
RegistClassMember(LuaTaoQuan2Wnd, "m_LeftTime")
RegistClassMember(LuaTaoQuan2Wnd, "m_CurLiDu")
RegistClassMember(LuaTaoQuan2Wnd, "m_CanThrow")

RegistClassMember(LuaTaoQuan2Wnd, "m_CountDownTick")
RegistClassMember(LuaTaoQuan2Wnd, "m_MoveTick")
RegistClassMember(LuaTaoQuan2Wnd, "m_Tick")
RegistClassMember(LuaTaoQuan2Wnd, "m_fx")

function LuaTaoQuan2Wnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.closeBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OncloseBtnClick()
	end)


	
	UIEventListener.Get(self.throwBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnthrowBtnClick()
	end)

    UIEventListener.Get(self.thumb.gameObject).onDrag = DelegateFactory.VectorDelegate(function(go, delta)
        self:OnDragThumb(delta)
    end)
    --@endregion EventBind end
	self.cameraNode = nil
	self.cameraScript = nil
	self.m_LeftTime = nil
	self.m_CanThrow = false
	self.m_fx = nil
	self.m_Tick = nil
end

function LuaTaoQuan2Wnd:Init()
	self:InitPosData()
	self.quanNode:SetActive(false)

	CPreDrawMgr.m_bEnableRectPreDraw = false
	self:UpdateScreen()
	CMainCamera.Inst:SetCameraEnableStatus(false,"use_another_camera", false)
	self.moveDir = 1
	self.m_CurLiDu = 0
	self:UpdateLidu(0.5)
	self.m_LeftTime = 0
	self.timeLabel.text = nil
	if self.m_MoveTick then UnRegisterTick(self.m_MoveTick) self.m_MoveTick = nil end
	
	self.m_MoveTick = RegisterTickWithDuration(function ()
		self:StartQuanMove()
	end,100,100)
	self:SetHuntRabbitPlayStatus()
end

function LuaTaoQuan2Wnd:DoQuanMove()
	local moveSpeed = 1000
	local pos = 0
	self.quanNode.transform.localPosition = self.rightSide.transform.localPosition
	if self.moveDir == 1 then
		self.moveDir = 0
		self.quanNode.transform.localPosition = self.leftSide.transform.localPosition
		pos = self.rightSide.transform.localPosition
	else
		self.moveDir = 1
		self.quanNode.transform.localPosition = self.rightSide.transform.localPosition
		pos = self.leftSide.transform.localPosition
	end
	local tweenPos = TweenPosition.Begin(self.quanNode,(self.rightSide.transform.localPosition.x - self.leftSide.transform.localPosition.x) / moveSpeed, pos)

	return tweenPos
end

function LuaTaoQuan2Wnd:InitPosData()
	local cameraData = ZhuJueJuQing_Setting.GetData().NvZhanKuangTaoQuanCameraPos
	self.game2CameraPos = Vector3(cameraData[0],cameraData[1],cameraData[2])
	self.game2CameraRotation = Quaternion.Euler(cameraData[3],cameraData[4],cameraData[5])
end

function LuaTaoQuan2Wnd:UpdateScreen()
	if self.cameraNode then
		GameObject.Destroy(self.cameraNode)
	end
	self.cameraNode = nil
	local screenWidth = CommonDefs.GameScreenWidth
  	local screenHeight = CommonDefs.GameScreenHeight
	if 1920 / screenWidth > 1080 / screenHeight then
		self.showTexture.width = 1920--Screen.width
		self.showTexture.height = screenHeight * 1920 / screenWidth--Screen.height
	else
		self.showTexture.width = screenWidth * 1080 / screenHeight--Screen.width
		self.showTexture.height = 1080--Screen.height
	end

	local renderTex = RenderTexture(self.showTexture.width,self.showTexture.height,32,RenderTextureFormat.ARGB32,RenderTextureReadWrite.Default)
	renderTex.wrapMode = TextureWrapMode.Clamp
	renderTex.filterMode = FilterMode.Bilinear
	renderTex.anisoLevel = 2
	self.showTexture.mainTexture = renderTex

	self.cameraNode = GameObject("__TaoQuanCamera__")
	local cameraScript = CommonDefs.AddCameraComponent(self.cameraNode)
	cameraScript.clearFlags = CameraClearFlags.Skybox
 
  	cameraScript.cullingMask = 2^LayerDefine.ModelForNGUI_3D + 2^LayerDefine.Effect_3D + 2^LayerDefine.Default + 2^LayerDefine.Water + 2^LayerDefine.WaterReflection + 2^LayerDefine.Terrain + 2^LayerDefine.NPC
  	cameraScript.orthographic = false
  	cameraScript.fieldOfView = 60
  	cameraScript.farClipPlane = 40
  	cameraScript.nearClipPlane = 0.1
  	cameraScript.depth = -10
  	cameraScript.transform.parent = CUIManager.instance.transform

	self.cameraNode.transform.position = self.game2CameraPos
	self.cameraNode.transform.localRotation = self.game2CameraRotation

	cameraScript.targetTexture = renderTex
	self.cameraScript = cameraScript
end

function LuaTaoQuan2Wnd:StartQuanMove()
	local tweenPos = self:DoQuanMove()

	local tweenFinish = function()
		if self.quanNode and self.quanNode.activeSelf then
			self:DoQuanMove()
		end
	end

	CommonDefs.AddEventDelegate(tweenPos.onFinished, DelegateFactory.Action(tweenFinish))
end

function LuaTaoQuan2Wnd:GetRemainTimeText(totalSeconds)
	if totalSeconds < 0 then
        totalSeconds = 0
    end
    if totalSeconds >= 3600 then
        return SafeStringFormat3("%02d:%02d:%02d", math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
    elseif totalSeconds > 10 then

        return SafeStringFormat3("%02d:%02d", math.floor(totalSeconds / 60), totalSeconds % 60)
	else
		-- if not CUIManager.IsLoaded(CUIResources.CenterCountdownWnd) then
		-- 	CCenterCountdownWnd.count = totalSeconds
        --     CCenterCountdownWnd.InitStartTime()
        --     CUIManager.ShowUI(CUIResources.CenterCountdownWnd)
		-- end
		return SafeStringFormat3("[FF0000]%02d:%02d[-]", math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end

function LuaTaoQuan2Wnd:SetHuntRabbitPlayStatus()
	if not LuaZhuJueJuQingMgr.m_HuntRabbitPlay then return end
	self.m_CanThrow = LuaZhuJueJuQingMgr.m_HuntRabbitPlay.playStatus == EnumHuntRabbitPlayStatus.WaitingForPlayer
	self:UpdateLeftTime(LuaZhuJueJuQingMgr.m_HuntRabbitPlay.leftTime)
	if LuaZhuJueJuQingMgr.m_HuntRabbitPlay.playStatus == EnumHuntRabbitPlayStatus.WaitingForPlayer then
		self.quanNode:SetActive(true)
	else
		self.quanNode:SetActive(false)
		if self.m_MoveTick then UnRegisterTick(self.m_MoveTick) self.m_MoveTick = nil end
		if LuaZhuJueJuQingMgr.m_HuntRabbitPlay.playStatus == EnumHuntRabbitPlayStatus.Finished then
			EventManager.BroadcastInternalForLua(EnumEventType.OnLoadCommonUIFx, {"Fx/Vfx/Prefab/Dynamic/UI_chenggong.prefab"})
			if self.m_CountDownTick then UnRegisterTick(self.m_CountDownTick) self.m_CountDownTick = nil end
		elseif LuaZhuJueJuQingMgr.m_HuntRabbitPlay.playStatus == EnumHuntRabbitPlayStatus.Failed then
			EventManager.BroadcastInternalForLua(EnumEventType.OnLoadCommonUIFx, {"Fx/Vfx/Prefab/Dynamic/UI_shibai01.prefab"})
			if self.m_CountDownTick then UnRegisterTick(self.m_CountDownTick) self.m_CountDownTick = nil end
		end
	end
end

function LuaTaoQuan2Wnd:UpdateLeftTime(leftTime)
	if self.m_CountDownTick then UnRegisterTick(self.m_CountDownTick) self.m_CountDownTick = nil end
	self.m_LeftTime = leftTime
	self.timeLabel.text = self:GetRemainTimeText(self.m_LeftTime)
	self.m_CountDownTick = RegisterTick(function ()
		self.m_LeftTime = math.max(0,self.m_LeftTime - 1)
		self.timeLabel.text = self:GetRemainTimeText(self.m_LeftTime)
		if self.m_LeftTime <= 0 then
			if self.m_CountDownTick then UnRegisterTick(self.m_CountDownTick) self.m_CountDownTick = nil end
		end
	end,1000)
end

function LuaTaoQuan2Wnd:PlayQuanFx(bSuccess, x, y)
	if self.m_fx then self.m_fx:Destroy() end
	local fxId = bSuccess and 88805116 or 88805115
	self.m_fx = CEffectMgr.Inst:AddWorldPositionFX(fxId, Utility.GridPos2WorldPos(x, y), 180, 0, 1, -1, EnumWarnFXType.None, nil, 0, 0, nil)

	local fxTime = 2000
	if self.m_Tick ~= nil then UnRegisterTick(self.m_Tick) self.m_Tick = nil end

	self.m_Tick = RegisterTickOnce(function ()
		if self.m_fx then
			self.m_fx:Destroy()
		end
	end,fxTime)
end

function LuaTaoQuan2Wnd:OnDragThumb(delta)
	local v =  2
	local curx = - math.sin(Deg2Rad * 90 * ( 1 - self.m_CurLiDu)) * 400
	local cury = math.cos(Deg2Rad * 90 * ( 1 - self.m_CurLiDu)) *  400
	local newx = math.max(-400,math.min(curx + delta.x * v,0))
	local newy = math.max(0,math.min(cury + delta.y * v,400))
	local degree = math.atan2(newy,newx)
	local value = math.max(0,math.min((180 - degree * Rad2Deg) / 90,1))
	self:UpdateLidu(value)
end

function LuaTaoQuan2Wnd:UpdateLidu(lidu)
	self.liduSlider.value = lidu
	self.m_CurLiDu = lidu
	self.mid.fillAmount = lidu
	
	local x,y = self:GetLiDuPoint(lidu)
	LuaUtils.SetLocalPosition(self.thumb.transform, x , y , 0)
end

function LuaTaoQuan2Wnd:GetLiDuPoint(lidu)
	local degree = Deg2Rad * 90 * ( 1 - lidu)
	local lengthx = 190
	local lengthy = 180
	local x = - math.sin(degree) * lengthx * 2
	local y = math.cos(degree) *  lengthy * 2 
	return x,y
end

--@region UIEvent

function LuaTaoQuan2Wnd:OncloseBtnClick()
	g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("LEAVE_COPY_CONFIRM"), function ()
		Gac2Gas.RequestLeavePlay()
		CUIManager.CloseUI(CLuaUIResources.TaoQuan2Wnd)
	end,nil, nil, nil, false)
end

function LuaTaoQuan2Wnd:OnthrowBtnClick()
	if not self.m_CanThrow then return end
	self.quanNode:SetActive(false)
	local screenPointX = self.quanNode.transform.localPosition.x + self.showTexture.width / 2
	local screenPointY = (self.m_CurLiDu * 0.5 + 0.25) * self.showTexture.height

	local gridPos = Utility.ScreenPoint2GridPos(Vector3(screenPointX,screenPointY,0),self.cameraScript)

	Gac2Gas.HuntRabbitSetRingPos(gridPos.x,gridPos.y)
end

--@endregion UIEvent

function LuaTaoQuan2Wnd:OnEnable()
	g_ScriptEvent:AddListener("OnScreenChange", self, "UpdateScreen")
	g_ScriptEvent:AddListener("OpenOrCloseWinSocialWnd", self, "UpdateScreen")
	g_ScriptEvent:AddListener("UpdateHuntRabbitPlayStatus", self, "SetHuntRabbitPlayStatus")
	g_ScriptEvent:AddListener("HuntRabbitReplyRingPosResult", self, "PlayQuanFx")
end

function LuaTaoQuan2Wnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnScreenChange", self, "UpdateScreen")
	g_ScriptEvent:RemoveListener("OpenOrCloseWinSocialWnd", self, "UpdateScreen")
	g_ScriptEvent:RemoveListener("UpdateHuntRabbitPlayStatus", self, "SetHuntRabbitPlayStatus")
	g_ScriptEvent:RemoveListener("HuntRabbitReplyRingPosResult", self, "PlayQuanFx")
	CMainCamera.Inst:SetCameraEnableStatus(true,"use_another_camera", false)
	CPreDrawMgr.m_bEnableRectPreDraw = true
	if self.cameraNode then
		GameObject.Destroy(self.cameraNode)
	end
	self.cameraNode = nil
	if self.m_fx then self.m_fx:Destroy() end
	if self.m_MoveTick then UnRegisterTick(self.m_MoveTick) self.m_MoveTick = nil end
	if self.m_CountDownTick then UnRegisterTick(self.m_CountDownTick) self.m_CountDownTick = nil end
	if self.m_Tick then UnRegisterTick(self.m_Tick) self.m_Tick = nil end
end
