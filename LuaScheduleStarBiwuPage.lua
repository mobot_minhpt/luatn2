local CScheduleAlertCtl=import "L10.UI.CScheduleAlertCtl"
local CLoginMgr = import "L10.Game.CLoginMgr"
local ClientAction=import "L10.UI.ClientAction"
local Animation = import "UnityEngine.Animation"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
CLuaScheduleStarBiwuPage = class()

CLuaScheduleStarBiwuPage.s_InJingCai = true

RegistChildComponent(CLuaScheduleStarBiwuPage,"m_ChampionView","ChampionView", GameObject)
RegistChildComponent(CLuaScheduleStarBiwuPage,"m_ChampionNameLabel","ChampionNameLabel", UILabel)
RegistChildComponent(CLuaScheduleStarBiwuPage,"m_SeasonInfoLabel","SeasonInfoLabel", UILabel)
RegistChildComponent(CLuaScheduleStarBiwuPage,"m_GiftRankBtn","GiftRankBtn", GameObject)
RegistChildComponent(CLuaScheduleStarBiwuPage,"m_Button1","Button1", GameObject)
RegistChildComponent(CLuaScheduleStarBiwuPage,"m_Button2","Button2", GameObject)
RegistChildComponent(CLuaScheduleStarBiwuPage,"m_Button3","Button3", GameObject)
RegistChildComponent(CLuaScheduleStarBiwuPage,"m_Button4","Button4", GameObject)
RegistChildComponent(CLuaScheduleStarBiwuPage,"m_Button5","Button5", GameObject)
RegistChildComponent(CLuaScheduleStarBiwuPage,"MainBtn","MainBtn", GameObject)
RegistChildComponent(CLuaScheduleStarBiwuPage,"m_TimeLabel","TimeLabel", UILabel)
RegistChildComponent(CLuaScheduleStarBiwuPage,"m_Bg","Bg", GameObject)
RegistChildComponent(CLuaScheduleStarBiwuPage,"m_BgFx","BgFx", GameObject)
RegistChildComponent(CLuaScheduleStarBiwuPage,"Table","Table", UITable)
RegistChildComponent(CLuaScheduleStarBiwuPage,"RuleBtn","RuleBtn", GameObject)


RegistClassMember(CLuaScheduleStarBiwuPage,"m_ActivityStatus")
RegistClassMember(CLuaScheduleStarBiwuPage,"m_Ani")
RegistClassMember(CLuaScheduleStarBiwuPage,"m_RuleLabel")
RegistClassMember(CLuaScheduleStarBiwuPage,"m_SignBtn")
RegistClassMember(CLuaScheduleStarBiwuPage,"m_saikuangBtn")
RegistClassMember(CLuaScheduleStarBiwuPage,"m_zhanduiBtn")
RegistClassMember(CLuaScheduleStarBiwuPage,"m_ScheduleRuleTipList")
RegistClassMember(CLuaScheduleStarBiwuPage,"m_ChangeRuleTipTick")
RegistClassMember(CLuaScheduleStarBiwuPage,"m_HasShowChampionView")
function CLuaScheduleStarBiwuPage:Awake()
    --报名阶段 非报名阶段
    local setting = StarBiWuShow_Setting.GetData()

    UIEventListener.Get(self.m_Button1.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        -- 赛况
        self:OnSaiKuanhBtnClick()
    end)
    UIEventListener.Get(self.m_Button2.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        -- 奖励
        ClientAction.DoAction(setting.Tab_Button2)
    end)
    UIEventListener.Get(self.m_Button3.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        --礼物榜
        --ClientAction.DoAction(StarBiWuShow_Setting.GetData().Tab_Button3)
        self:OnGiftRankBtnClick()
    end)
    UIEventListener.Get(self.m_Button4.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        --赛事竞猜
        ClientAction.DoAction(setting.Tab_Button4)
        CLuaScheduleMgr.UpdateAlertStatus(setting.MatchServerSchedule,LuaEnumAlertState.Clicked)
    end)
    UIEventListener.Get(self.m_Button5.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        -- 战队
        self:OnZhanDuiBtnClick()
    end)
    UIEventListener.Get(self.RuleBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        -- 规则
        self:OnRuleBtnClick()
    end)
    
    self.m_RuleLabel = self.RuleBtn.transform:Find("RuleBubble"):GetComponent(typeof(UILabel))
    self.m_RuleLabel.gameObject:SetActive(false)
    self.m_SignBtn = self.MainBtn.transform:Find("SignStatus").gameObject
    self.m_saikuangBtn = self.MainBtn.transform:Find("WatchStatus/saikuangBtn").gameObject
    self.m_zhanduiBtn = self.MainBtn.transform:Find("WatchStatus/zhanduiBtn").gameObject
    self.m_zhanduiBtn.gameObject:SetActive(false)

    UIEventListener.Get(self.m_zhanduiBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        -- 战队
        self:OnZhanDuiBtnClick()
    end)

    UIEventListener.Get(self.m_saikuangBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        -- 赛况
        self:OnSaiKuanhBtnClick()
    end)

    UIEventListener.Get(self.m_SignBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        -- 报名参加
        ClientAction.DoAction(StarBiWuShow_Setting.GetData().Tab_Button5)
    end)

    local alertCtrl = self.m_Button4.gameObject:GetComponent(typeof(CScheduleAlertCtl))
    alertCtrl.tabType = 0
    self.m_ScheduleRuleTipList = nil
    self.m_ChangeRuleTipTick = nil
    alertCtrl.scheduleId = StarBiWuShow_Setting.GetData().MatchServerSchedule
    self.m_GiftRankBtn.gameObject:SetActive(false)
    self.m_TimeLabel.text = setting.Tab_Time
    local seasonId = StarBiWuShow_Setting.GetData().Season
    local data = StarBiWuShow_AutoPatch.GetDataBySubKey("Season",seasonId)
    self.m_SeasonInfoLabel.text = SafeStringFormat3(LocalString.GetString("%s总冠军"),data.Desc)
    --self.m_Ani = self.transform:GetComponent(typeof(Animation))
    self.MainBtn.gameObject:SetActive(false)
    self.m_ChampionView.gameObject:SetActive(false)
    self.m_Button2.gameObject:SetActive(false)
    self.m_Button5.gameObject:SetActive(false)
    self.m_HasShowChampionView = false
    self:UpdateView()
end

function CLuaScheduleStarBiwuPage:OnSaiKuanhBtnClick()
    CLuaStarBiwuMgr:OpenStarBiWuDetailWnd(0)
end

function CLuaScheduleStarBiwuPage:OnZhanDuiBtnClick()
    CLuaStarBiwuMgr:OpenStarBiWuZhanDuiWnd(0)
end

function CLuaScheduleStarBiwuPage:OnGiftRankBtnClick()
    CUIManager.ShowUI(CLuaUIResources.StarBiWuGiftRankWnd)
end

function CLuaScheduleStarBiwuPage:OnRuleBtnClick()
    CLuaStarBiwuMgr:OpenStarBiWuDetailWnd(3)
end

function CLuaScheduleStarBiwuPage:UpdateView()
    -- self.m_Ani:Stop()
    -- self.m_Ani:Play("schedulewnd_crossstar_nonequdao")
    local bgfx = self.m_BgFx
    if bgfx then
        bgfx.gameObject:SetActive(true)
    end
end

function CLuaScheduleStarBiwuPage:InitScheduleData()
    local scheduleTbl = self.transform:Find("ScheduleTbl"):GetComponent(typeof(UITable))
    local todaySchedule = scheduleTbl.transform:Find("TodaySchedule").gameObject
    local nextSchedule = scheduleTbl.transform:Find("NextSchedule").gameObject
    local curTime = CServerTimeMgr.Inst:GetZone8Time()
    local todayTimeStamp = CServerTimeMgr.Inst:GetTimeStampByStr(curTime:ToString("yyyy-MM-dd"))
    local todayDate = nil
    local nextDate = nil
    todaySchedule.gameObject:SetActive(false)
    nextSchedule.gameObject:SetActive(false)
    StarBiWuShow_Calendar.Foreach(function(k,v)
        if not nextDate then 
            local timeStamp = CServerTimeMgr.Inst:GetTimeStampByStr(v.Date)
            local y, m, d = string.match(v.Date, "(%d+)-(%d+)-(%d+)")
            if (timeStamp == todayTimeStamp) then
                todayDate = {schedule = v.Schedule, commonGroup = v.CommonGroupSchedule, topGroup = v.TopGroupSchedule, y = y, m = m, d = d}
            elseif (timeStamp > todayTimeStamp) then
                nextDate = {schedule = v.Schedule, commonGroup = v.CommonGroupSchedule, topGroup = v.TopGroupSchedule, y = y, m = m, d = d}
            end
        end
    end)
    if todayDate then
        todaySchedule.gameObject:SetActive(true)
        self:SetScheduleView(todaySchedule, todayDate, true)
    end
    if nextDate then
        nextSchedule.gameObject:SetActive(true)
        self:SetScheduleView(nextSchedule, nextDate, false)
    end
    scheduleTbl:Reposition()
    self:InitScheduleRuleTip()
    self:InitRuleTip()
end

function CLuaScheduleStarBiwuPage:InitScheduleRuleTip()
    self.m_ScheduleRuleTipList = {}
    StarBiWuShow_ScheduleRuleTip.Foreach(function(k,v)
        local startData = v.StartDate
        local endData = v.EndDate
        local startTimeStamp = CServerTimeMgr.Inst:GetTimeStampByStr(startData)
        local endTimeStamp = CServerTimeMgr.Inst:GetTimeStampByStr(endData)
        if CServerTimeMgr.Inst.timeStamp >= startTimeStamp and CServerTimeMgr.Inst.timeStamp < endTimeStamp then
            table.insert(self.m_ScheduleRuleTipList,{tip = v.RuleTip,starttime = startTimeStamp, endtime = endTimeStamp})
        end
    end)
end

function CLuaScheduleStarBiwuPage:InitRuleTip()
    if self.m_ChangeRuleTipTick then
        UnRegisterTick(self.m_ChangeRuleTipTick)
        self.m_ChangeRuleTipTick = nil
    end
    if not self.m_ScheduleRuleTipList or #self.m_ScheduleRuleTipList == 0 then
        self.m_RuleLabel.gameObject:SetActive(false)
        return
    end

    -- if #self.m_ScheduleRuleTipList == 1 then
    --     self.m_RuleLabel.text = self.m_ScheduleRuleTipList[1].tip
    --     self.m_RuleLabel.gameObject:SetActive(true)
    --     return
    -- end
    local index = 1
    local intervel = StarBiWuShow_Setting.GetData().ScheuleRuleTipShowTime
    self.m_RuleLabel.text = self.m_ScheduleRuleTipList[index].tip
    self.m_RuleLabel.gameObject:SetActive(true)
    self.m_ChangeRuleTipTick = RegisterTick(function()
        index = index + 1
        if index > #self.m_ScheduleRuleTipList then
            index = 1
        end
        if #self.m_ScheduleRuleTipList <= 0 then
            self.m_RuleLabel.gameObject:SetActive(false)
            UnRegisterTick(self.m_ChangeRuleTipTick)
            self.m_ChangeRuleTipTick = nil
            return
        end
        local data = self.m_ScheduleRuleTipList[index]
        if CServerTimeMgr.Inst.timeStamp >= data.starttime and CServerTimeMgr.Inst.timeStamp < data.endtime then
            self.m_RuleLabel.text = data.tip
        else
            table.remove(self.m_ScheduleRuleTipList,index)
        end
    end,intervel * 1000)

end

function CLuaScheduleStarBiwuPage:SetScheduleView(Scheduleview,data,isToday)
    local riqi = Scheduleview.transform:Find("Title"):GetComponent(typeof(UILabel))
    riqi.text = isToday and LocalString.GetString("今日") or SafeStringFormat3(LocalString.GetString("%d\n月\n%d\n日"),data.m,data.d)
    local time = Scheduleview.transform:Find("Time"):GetComponent(typeof(UILabel))
    local textTable = Scheduleview.transform:Find("TextTable"):GetComponent(typeof(UITable))
    local commonGroup = textTable.transform:Find("commonGroup"):GetComponent(typeof(UILabel))
    local topGroup = textTable.transform:Find("TopGroup"):GetComponent(typeof(UILabel))
    topGroup.gameObject:SetActive(false)
    commonGroup.gameObject:SetActive(false)
    if (not System.String.IsNullOrEmpty(data.schedule)) then
        commonGroup.gameObject:SetActive(true)
        local info = g_LuaUtil:StrSplit(data.schedule,"\n")
        commonGroup.text = info[1]
        time.text = info[2]
    else
        if (not System.String.IsNullOrEmpty(data.commonGroup)) then
            commonGroup.gameObject:SetActive(true)
            local info = g_LuaUtil:StrSplit(data.commonGroup,"\n")
            commonGroup.text = string.gsub(info[1],LocalString.GetString("普通组"),LocalString.GetString("[acf8ff]普通组[-]"))
            time.text = info[2]
        end  
        if (not System.String.IsNullOrEmpty(data.topGroup)) then
            topGroup.gameObject:SetActive(true)
            local info = g_LuaUtil:StrSplit(data.topGroup,"\n")
            topGroup.text = string.gsub(info[1],LocalString.GetString("巅峰组"),LocalString.GetString("[fbb9ff]巅峰组[-]"))
            time.text = info[2]
        end
        if ((not System.String.IsNullOrEmpty(data.commonGroup)) and (not System.String.IsNullOrEmpty(data.topGroup)))  then
            local info1 = g_LuaUtil:StrSplit(data.commonGroup,"\n")
            local info2 = g_LuaUtil:StrSplit(data.topGroup,"\n")
            local time1startH,time1startM,time1endH,time1endM = string.match(info1[2], "(%d+):(%d+)-(%d+):(%d+)")
            local time2startH,time2startM,time2endH,time2endM = string.match(info2[2], "(%d+):(%d+)-(%d+):(%d+)")
            local timeStamp1 = CServerTimeMgr.Inst:GetTodayTimeStampByTime(time1startH,time1startM,0)
            local timeStamp2 = CServerTimeMgr.Inst:GetTodayTimeStampByTime(time2startH,time2startM,0)
            local timeStamp3 = CServerTimeMgr.Inst:GetTodayTimeStampByTime(time1endH,time1endM,0)
            local timeStamp4 = CServerTimeMgr.Inst:GetTodayTimeStampByTime(time2endH,time2endM,0)
            local startH,startM = 0,0
            local endH,endM = 0,0
            if timeStamp1 < timeStamp2 then
                startH = time1startH
                startM = time1startM
            else
                startH = time2startH
                startM = time2startM
            end
            if timeStamp3 > timeStamp4 then
                endH = time1endH
                endM = time1endM
            else
                endH = time2endH
                endM = time2endM
            end
            time.text = SafeStringFormat3("%02d:%02d-%02d:%02d",startH,startM,endH,endM)
        end      
    end
    textTable:Reposition()
end

function CLuaScheduleStarBiwuPage:OnGetStarBiwuActivityStatusS13(signUpStatus, bSignUp, bHasFight)
    if self.m_HasShowChampionView then return end
    self.MainBtn.gameObject:SetActive(true)
    self.m_SignBtn.gameObject:SetActive(signUpStatus == 3 and not bSignUp)
    self.m_Button1.gameObject:SetActive(signUpStatus == 3 and not bSignUp)
    self.m_Button5.gameObject:SetActive(false)
    self.m_saikuangBtn.gameObject:SetActive(bSignUp and signUpStatus == 3 or signUpStatus == 4)
    self.m_zhanduiBtn.gameObject:SetActive(false)
    local GuanZhanLabel = self.m_saikuangBtn.transform:Find("GuanZhanLabel").gameObject
    GuanZhanLabel.gameObject:SetActive(bHasFight)
    self.Table:Reposition()
    Extensions.SetLocalPositionY(self.Table.transform, 45)
end


function CLuaScheduleStarBiwuPage:OnEnable()
    self:InitScheduleData()
    Gac2Gas.QueryCurrentSeasonStarBiwuChampion()
    Gac2Gas.QueryStarBiwuActivityStatusS13()
    g_ScriptEvent:AddListener("OnSyncStarBiwuActivityStatusS13", self, "OnGetStarBiwuActivityStatusS13")
    g_ScriptEvent:AddListener("OnSyncStarBiwuCurrentSeasonChampionInfo", self, "OnSyncStarBiwuCurrentSeasonChampionInfo")
end

function CLuaScheduleStarBiwuPage:OnDisable()
    g_ScriptEvent:RemoveListener("OnSyncStarBiwuActivityStatusS13", self, "OnGetStarBiwuActivityStatusS13")
    g_ScriptEvent:RemoveListener("OnSyncStarBiwuCurrentSeasonChampionInfo", self, "OnSyncStarBiwuCurrentSeasonChampionInfo")
    if self.m_ChangeRuleTipTick then
        UnRegisterTick(self.m_ChangeRuleTipTick)
        self.m_ChangeRuleTipTick = nil
    end
end

function CLuaScheduleStarBiwuPage:OnSyncStarBiwuCurrentSeasonChampionInfo(currentSeason, leanderId, teamName)
    local memberList = CLuaStarBiwuMgr.m_CurSeasonChampionMemberInfo
    if not memberList then return end
    self.m_HasShowChampionView = true
    self.m_ChampionView.gameObject:SetActive(true)
    self.transform:Find("ScheduleTbl").gameObject:SetActive(false)
    self.MainBtn.gameObject:SetActive(false)
    self.m_Button5.gameObject:SetActive(false)
    self.m_Button1.gameObject:SetActive(true)
    self.Table:Reposition()
    local playerItem = self.m_ChampionView.transform:Find("PlayerItem").gameObject
    local ChampionTable1 = self.m_ChampionView.transform:Find("ChampionTable1"):GetComponent(typeof(UIGrid))
    local ChampionTable2 = self.m_ChampionView.transform:Find("ChampionTable2"):GetComponent(typeof(UIGrid))
    Extensions.RemoveAllChildren(ChampionTable1.transform)
    Extensions.RemoveAllChildren(ChampionTable2.transform)
    for i = 1,math.min(#memberList,4) do
        local item = CUICommonDef.AddChild(ChampionTable1.gameObject,playerItem.gameObject)
        item.gameObject:SetActive(true)
        self:InitChampionItem(item,memberList[i])
    end
    if #memberList > 4 then
        for i = 5,#memberList do
            local item = CUICommonDef.AddChild(ChampionTable2.gameObject,playerItem.gameObject)
            item.gameObject:SetActive(true)
            self:InitChampionItem(item,memberList[i])
        end
    end
    ChampionTable1:Reposition()
    ChampionTable2:Reposition()
    Extensions.SetLocalPositionY(self.Table.transform, -58)
end

function CLuaScheduleStarBiwuPage:InitChampionItem(item,data)
    local portrait = item.transform:Find("Portrait"):GetComponent(typeof(CUITexture))
    local name = item.transform:Find("Name"):GetComponent(typeof(UILabel))
    local lvLabel = item.transform:Find("Level"):GetComponent(typeof(UILabel))
    portrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(data.MemberClass, data.MemberGender, -1), false)
    name.text = tostring(data.MemberName)
    lvLabel.text = SafeStringFormat3("lv.%d",data.MemberLever)
end