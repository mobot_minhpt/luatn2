local UIDefaultTableView = import "L10.UI.UIDefaultTableView"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UITable = import "UITable"
local GameObject = import "UnityEngine.GameObject"
local MessageMgr = import "L10.Game.MessageMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local EPlayerFightProp = import "L10.Game.EPlayerFightProp"
local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"
local StringBuilder = import "System.Text.StringBuilder"
local CCommonItem = import "L10.Game.CCommonItem"
local CItemMgr = import "L10.Game.CItemMgr"
local UITabBar = import "L10.UI.UITabBar"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local CellIndexType=import "L10.UI.UITableView+CellIndexType"
local CellIndex=import "L10.UI.UITableView+CellIndex"
local CButton = import "L10.UI.CButton"
LuaPlayerPropertyCompareWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaPlayerPropertyCompareWnd, "PropertyCompareTip", "PropertyCompareTip", GameObject)
RegistChildComponent(LuaPlayerPropertyCompareWnd, "WndTable", "WndTable", UITable)
RegistChildComponent(LuaPlayerPropertyCompareWnd, "TableList", "TableList", UIDefaultTableView)
RegistChildComponent(LuaPlayerPropertyCompareWnd, "SectionTemplate", "SectionTemplate", GameObject)
RegistChildComponent(LuaPlayerPropertyCompareWnd, "RowCellTemplate", "RowCellTemplate", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaPlayerPropertyCompareWnd, "m_PropertyID")
RegistClassMember(LuaPlayerPropertyCompareWnd, "m_PropertyData")
RegistClassMember(LuaPlayerPropertyCompareWnd, "m_MyPropertyList")
RegistClassMember(LuaPlayerPropertyCompareWnd, "m_OppositePropertyList")
RegistClassMember(LuaPlayerPropertyCompareWnd, "m_View")
RegistClassMember(LuaPlayerPropertyCompareWnd, "m_OppoView")
RegistClassMember(LuaPlayerPropertyCompareWnd, "m_SectionIndex")
RegistClassMember(LuaPlayerPropertyCompareWnd, "m_CurSectionIndex")
RegistClassMember(LuaPlayerPropertyCompareWnd, "m_CurIndex")
RegistClassMember(LuaPlayerPropertyCompareWnd, "m_Index")
RegistClassMember(LuaPlayerPropertyCompareWnd, "m_TabBar")
RegistClassMember(LuaPlayerPropertyCompareWnd, "m_SectionInfo")
RegistClassMember(LuaPlayerPropertyCompareWnd, "m_PropList")
RegistClassMember(LuaPlayerPropertyCompareWnd, "m_HasInit")
function LuaPlayerPropertyCompareWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.m_OppositePropertyList = nil
    self.m_MyPropertyList = nil
    self.m_PropertyID = 0
    self.m_PropertyData = nil
    self.m_View = nil
    self.m_OppoView = nil
    self.m_TabBar = nil
    self.m_SectionInfo = nil
    self.m_PropList = nil
    self.m_SectionIndex = -1
    self.m_CurSectionIndex = -1
    self.m_Index = -1
    self.m_CurIndex = -1
    self.m_HasInit = false
    self.PropertyCompareTip.gameObject:SetActive(false)
    self.TableList.gameObject:SetActive(false)
end

function LuaPlayerPropertyCompareWnd:Init()
    Extensions.RemoveAllChildren(self.WndTable.transform)
    if LuaPlayerPropertyMgr.PropertyTypeList then 
        self:AddTableBar()
    end
    self.m_View = CUICommonDef.AddChild(self.WndTable.gameObject,self.PropertyCompareTip.gameObject)
    self.m_OppoView = CUICommonDef.AddChild(self.WndTable.gameObject,self.PropertyCompareTip.gameObject)
    if LuaPlayerPropertyMgr.PropertyTypeList and self.TableList then
        if not self.m_HasInit then 
            self.TableList.gameObject:SetActive(true)
            self.TableList:LoadData(CellIndex(0, 0, CellIndexType.Section), false)
            self.m_HasInit = true
        end
    end
    self.m_MyPropertyList = {}
    self.m_OppositePropertyList = {}
    self:InitHeader()
    self.m_View.gameObject:SetActive(true)
    self.m_OppoView.gameObject:SetActive(true)
    self.m_View:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf:InitEmpty(self.m_MyPropertyList.Header)
    self.m_OppoView:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf:InitEmpty(self.m_OppositePropertyList.Header)

    self:UpdateValue()
    if CClientMainPlayer.Inst and LuaPlayerPropertyMgr.PropertyGuide then
        if CClientMainPlayer.Inst.Level >= LuaPlayerPropertyMgr:GetPropertyGuideLevel() then
            CLuaGuideMgr.TryTriggerGuide(240)
        end
    end
end

function LuaPlayerPropertyCompareWnd:AddTableBar()
    if #LuaPlayerPropertyMgr.PropertyTypeList < 1 then return end
    self.TableList.transform:SetParent(self.WndTable.transform)
    -- self.m_TabBar = self.TableList.transform:Find("TabScollView/TabTable"):GetComponent(typeof(UITabBar))
    -- Extensions.RemoveAllChildren(self.m_TabBar.transform)
    self.m_PropList = {}
    for i = 1,#LuaPlayerPropertyMgr.PropertyTypeList do
        local eName = LuaPlayerPropertyMgr.PropertyTypeList[i]
        local fightpropID = EnumToInt(EPlayerFightProp[eName])
        local data = PropGuide_Property.GetDataBySubKey("FightPropID",fightpropID)  
        self.m_PropList[data.ID] = {id = data.ID,eName= eName, Name = data.Name}
    end
    
    self.m_SectionInfo = {}
    PropGuide_Rank.Foreach(function(k,v)
		local t = {}
		local rowCount = 0
        local hasSection = false
		for i = 0,v.Content.Length - 1 do
			local id = v.Content[i]
			if self.m_PropList[id] then
                table.insert(t,self.m_PropList[id])
            end
		end
		if t and #t > 0 then
			table.insert(self.m_SectionInfo,{name = v.TitleName, content = t})
		end
	end)

    self.TableList:Init(
		function () return #self.m_SectionInfo end,		-- 一级按钮个数
		function (section) return #self.m_SectionInfo[section + 1].content end,	-- 每个一级按钮下二级按钮的个数
		function (cell,index) self:InitTableViewCell(cell,index) end,			-- 每个按钮的初始化
		function (index,expanded) self:OnSectionClicked(index,expanded) end,-- 一级按钮点击事件
        function (index) self:OnRowBtnClick(index) end	-- 二级按钮点击事件
	)
    -- for i = 1,#LuaPlayerPropertyMgr.PropertyTypeList do
    --     --local table = CUICommonDef.AddChild(self.m_TabBar.gameObject,self.TabTemplate.gameObject)
    --     --local tabLabel = table.transform:Find("Label"):GetComponent(typeof(UILabel))

    --     --if data then tabLabel.text = data.Name end
    --     --table.gameObject:SetActive(true)
    -- end
    -- self.m_TabBar.enabled = true
    -- self.m_TabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
    --     self:OnTabChange(index)
    -- end)
    -- self.m_TabBar.transform:GetComponent(typeof(UITable)):Reposition()
    -- self.TableList.transform:Find("TabScollView"):GetComponent(typeof(CUIRestrictScrollView)):ResetPosition()
    -- 请求第一个属性的对比情况
    self.m_CurIndex = 0
    self.m_CurSectionIndex = 0
    --LuaPlayerPropertyMgr:ShowPlayerRequestCompareOnlineProp(self.m_SectionInfo[1].content[1].eName,LuaPlayerPropertyMgr.PlayerCompareData)
end
function LuaPlayerPropertyCompareWnd:InitTableViewCell(cell,index)
    local nameLabel = cell.transform:Find("Name"):GetComponent(typeof(UILabel))
	if index.type == CellIndexType.Section then
		nameLabel.text = self.m_SectionInfo[index.section + 1].name
	else
		local content = self.m_SectionInfo[index.section + 1].content
		local prop = content[index.row + 1]
		if prop then 
			nameLabel.text = prop.Name
		end
	end
end
function LuaPlayerPropertyCompareWnd:OnSectionClicked(index,expanded)
	local curIndex = CellIndex(self.m_SectionIndex, 0, CellIndexType.Section)
	local curSelectBtn = self.TableList:GetCellByIndex(curIndex)
	if curSelectBtn then curSelectBtn:GetComponent(typeof(CButton)).Selected = true end
	if curSelectBtn and self.m_SectionIndex == index.section and expanded then
		local curRowBtn = self.TableList:GetCellByIndex(CellIndex(self.m_SectionIndex, self.m_Index, CellIndexType.Row))
		if curRowBtn then curRowBtn:GetComponent(typeof(CButton)).Selected = true end
	end
end
function LuaPlayerPropertyCompareWnd:OnRowBtnClick(index)
    if self.m_SectionIndex == index.section and self.m_Index == index.row then return end
    self.m_CurIndex = index.row
    self.m_CurSectionIndex = index.section
    if not self.m_HasInit then return end
    local propEname = self.m_SectionInfo[index.section + 1].content[index.row + 1].eName
    -- 请求服务器数据
    LuaPlayerPropertyMgr:ShowPlayerRequestCompareOnlineProp(propEname,LuaPlayerPropertyMgr.PlayerCompareData)
end
function LuaPlayerPropertyCompareWnd:UpdateValue()
    if not LuaPlayerPropertyMgr.OppositePropData or not LuaPlayerPropertyMgr.PlayerPropData then return end
    if not LuaPlayerPropertyMgr.PropertyGuideID then return end
    self.m_PropertyID = LuaPlayerPropertyMgr.PropertyGuideID
    local data = self:GetUnionPropertyData()
    if not data then return end
    self.m_PropertyData = data
    --self:InitHeader()
    self:InitList()
    self:InitTableView()
    self:UpdateSelectTab()
end

function LuaPlayerPropertyCompareWnd:GetUnionPropertyData()
    local propertyData = LuaPlayerPropertyMgr.PlayerPropData.PropData
    local otherPropertyData = LuaPlayerPropertyMgr.OppositePropData.PropData
    if otherPropertyData.BaseWords then
        for k,v in pairs(otherPropertyData.BaseWords) do
            if v and k > 1000 then
            --if v then  
                if not propertyData.BaseWords then propertyData.BaseWords = {} end
                propertyData.BaseWords[k] = true
                propertyData.BaseWords[k - 1000] = false
            end
        end
    end
    if otherPropertyData.PrecentWords then
        for k,v in pairs(otherPropertyData.PrecentWords) do
            if v and k > 2000 then
            --if v then 
                if not propertyData.PrecentWords then propertyData.PrecentWords = {} end
                propertyData.PrecentWords[k] = true
                propertyData.PrecentWords[k - 2000] = false
            end
        end
    end
    if otherPropertyData.ExtraWords then
        for k,v in pairs(otherPropertyData.ExtraWords) do
            if v and k > 3000 then
            --if v then 
                if not propertyData.ExtraWords then propertyData.ExtraWords = {} end
                propertyData.ExtraWords[k] = true
                propertyData.ExtraWords[k - 3000] = false
            end
        end
    end
    if otherPropertyData.OtherWords then
        for k,v in pairs(otherPropertyData.OtherWords) do
            if v and k > 4000 then
            --if v then 
                if not propertyData.OtherWords then propertyData.OtherWords = {} end
                propertyData.OtherWords[k] = true
                propertyData.OtherWords[k - 4000] = false
            end
        end
    end
    return propertyData
end

function LuaPlayerPropertyCompareWnd:UpdateSelectTab()
	local curSelectBtn = self.TableList:GetCellByIndex(CellIndex(self.m_SectionIndex, 0, CellIndexType.Section))
	local curRowBtn = self.TableList:GetCellByIndex(CellIndex(self.m_SectionIndex, self.m_Index, CellIndexType.Row))
	if curSelectBtn then curSelectBtn:GetComponent(typeof(CButton)).Selected = false end
	if curRowBtn then curRowBtn:GetComponent(typeof(CButton)).Selected = false end

	local selectBtn = self.TableList:GetCellByIndex(CellIndex(self.m_CurSectionIndex, 0, CellIndexType.Section))
	local rowBtn = self.TableList:GetCellByIndex(CellIndex(self.m_CurSectionIndex, self.m_CurIndex, CellIndexType.Row))
	if selectBtn then selectBtn:GetComponent(typeof(CButton)).Selected = true end
	if rowBtn then rowBtn:GetComponent(typeof(CButton)).Selected = true end
	self.m_SectionIndex = self.m_CurSectionIndex
	self.m_Index = self.m_CurIndex
end
function LuaPlayerPropertyCompareWnd:InitHeader()
    if not CClientPlayerMgr.Inst or not CClientMainPlayer.Inst then return end

    local MyHeader = {
        Name = CClientMainPlayer.Inst.RealName,
        IsOpposite = false,
    }
    local OppositeHeader = {
        Name = LuaPlayerPropertyMgr.PlayerCompareData.name,
        IsOpposite = true,
    }
    
    self.m_MyPropertyList.Header = MyHeader
    self.m_OppositePropertyList.Header = OppositeHeader
end

function LuaPlayerPropertyCompareWnd:InitList()
    local MyTable = {}
    local OppositeTable = {}
    local m0st,o0st = self:GetTotalTable(0) -- 一级标题
    table.insert(MyTable,m0st)
    table.insert(OppositeTable,o0st)
    if self.m_PropertyData.BaseWords then   -- 基础加成
        local list = self.m_PropertyData.BaseWords
        local m1st,o1st = self:GetTotalTable(1) 
        table.insert(MyTable,m1st)
        table.insert(OppositeTable,o1st)
        self:GetThirdTitleTable(MyTable,OppositeTable,1,list)
    end
    if self.m_PropertyData.PrecentWords then    -- 百分比加成
        local list = self.m_PropertyData.PrecentWords
        local m1st,o1st = self:GetTotalTable(2) 
        table.insert(MyTable,m1st)
        table.insert(OppositeTable,o1st)
        self:GetThirdTitleTable(MyTable,OppositeTable,2,list)
    end
    if self.m_PropertyData.ExtraWords then  -- 额外加成
        local list = self.m_PropertyData.ExtraWords
        local m1st,o1st = self:GetTotalTable(3) 
        table.insert(MyTable,m1st)
        table.insert(OppositeTable,o1st)
        self:GetThirdTitleTable(MyTable,OppositeTable,3,list)
    end
    if self.m_PropertyData.OtherWords then  -- 其他加成
        local list = self.m_PropertyData.OtherWords
        local m1st,o1st = self:GetTotalTable(4) 
        table.insert(MyTable,m1st)
        table.insert(OppositeTable,o1st)
        self:GetThirdTitleTable(MyTable,OppositeTable,4,list)
    end
    self.m_MyPropertyList.Content = MyTable
    self.m_OppositePropertyList.Content = OppositeTable
end
function LuaPlayerPropertyCompareWnd:InitTableView()
    if not self.m_View or not self.m_OppoView then return end
    -- if LuaPlayerPropertyMgr.PropertyTypeList and self.TableList then
    --     if not self.m_HasInit then 
    --         self.TableList.gameObject:SetActive(true)
    --         self.TableList:LoadData(CellIndex(0, 0, CellIndexType.Row), true)
    --         self.m_HasInit = true
    --     end
    -- end
    -- self.m_View.gameObject:SetActive(true)
    -- self.m_OppoView.gameObject:SetActive(true)
    self.m_View:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf:Init(self.m_MyPropertyList)
    self.m_OppoView:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf:Init(self.m_OppositePropertyList)
    self.WndTable:Reposition()
end
-- index : 0 , 总和 ; 1,基础加成总和 ; 2, 百分比加成总和; 3,额外加成总和; 4, 其他加成总和
function LuaPlayerPropertyCompareWnd:GetTotalTable(index)
    local NameList = {
        [0] = self.m_PropertyData.Name,
        [1] = LocalString.GetString("基础加成"),
        [2] = LocalString.GetString("百分比加成"),
        [3] = LocalString.GetString("额外加成"),
        [4] = LocalString.GetString("其他加成"),
    }
    local myValue = self:GetTotalValue(index,false)
    local OppositeValue = self:GetTotalValue(index,true)
    local Name = NameList[index]
    local myTable = nil
    local OppositeTable = nil
    if index == 0 then
        myTable = {
            type = 1,
            Name = Name,
            Value = myValue,
            Des = MessageMgr.Inst:FormatMessage(self.m_PropertyData.Desc,{}),
            UpOrDown = myValue - OppositeValue,
        }
        OppositeTable = {
            type = 1,
            Name = Name,
            Value = OppositeValue,
            Des = MessageMgr.Inst:FormatMessage(self.m_PropertyData.Desc,{}),
            UpOrDown = OppositeValue - myValue,
        }
    else
        myTable = {
            type = 2,
            Name = Name,
            Value = myValue,
            UpOrDown = myValue - OppositeValue,
            index = index,
        }
        OppositeTable = {
            type = 2,
            Name = Name,
            Value = OppositeValue,
            UpOrDown = OppositeValue - myValue,
            index = index,
        }
    end
    return myTable,OppositeTable
end
function LuaPlayerPropertyCompareWnd:GetThirdTitleTable(mTable,oTable,index,list)
    local worldList = {}
    local count = 1
    for k,v in pairs(list) do
        if v then 
            worldList[count] = {}
            worldList[count].m = {}
            worldList[count].o = {}
            local mval,mequip = self:GetItemValue(index,k,false)
            local oval,oequip = self:GetItemValue(index,k,true)
            local diffVal = mval.showval - oval.showval
            if math.abs(diffVal) <= 1 and diffVal ~= 0 then diffVal = diffVal > 0 and 1 or -1 end 
            worldList[count].m.word = {
                type = 3,
                Value = mval,
                UpOrDown = diffVal,
                index = index,
                wordId = k,
            }
            worldList[count].m.equip = {}
            self:GetEquipList(worldList[count].m.equip,mequip,index)
            worldList[count].o.word = {
                type = 3,
                Value = oval,
                UpOrDown = -diffVal,
                index = index,
                wordId = k,
            }
            worldList[count].o.equip = {}
            self:GetEquipList(worldList[count].o.equip,oequip,index)
            count = count + 1
        end
    end

    table.sort(worldList,function(a,b)
        local aval = math.floor(math.abs(a.o.word.Value.val))
        local bval = math.floor(math.abs(b.o.word.Value.val))
        local cval = math.floor(math.abs(a.m.word.Value.val))
        local dval = math.floor(math.abs(b.m.word.Value.val))
        if aval > 0 and bval <= 0 then return true
        elseif aval == 0 and bval == 0 and cval > 0 and dval <= 0 then return true
        else return false end
    end)
    for k,v in pairs(worldList) do
        table.insert(mTable,v.m.word)
        if #v.m.equip > 0 then
            for m,n in pairs(v.m.equip) do
                table.insert(mTable,n)
            end
        end
        table.insert(oTable,v.o.word)
        if #v.o.equip > 0 then
            for m,n in pairs(v.o.equip) do
                table.insert(oTable,n)
            end
        end
    end
    -- for k,v in pairs(list) do
    --     if v then
            
    --         table.insert(mTable,{
                
    --         })
            
    --         table.insert(oTable,{
    --             type = 3,
    --             Value = oval,
    --             UpOrDown = -diffVal,
    --             index = index,
    --             wordId = k,
    --         })
    --         self:GetEquipList(oTable,oequip,index)
    --     end
    -- end
end
function LuaPlayerPropertyCompareWnd:GetTotalValue(index,isOpposite)
    if not LuaPlayerPropertyMgr.PlayerPropData or not LuaPlayerPropertyMgr.OppositePropData then return 0 end
    local type = CommonDefs.ConvertIntToEnum(typeof(EPlayerFightProp), self.m_PropertyData.FightPropID)
    local val = 0
    if not isOpposite then
        if index == 0 then val = LuaPlayerPropertyMgr.PlayerPropData.total.all.main
        elseif index == 1 then val = LuaPlayerPropertyMgr.PlayerPropData.total.base.main
        elseif index == 2 then val = LuaPlayerPropertyMgr.PlayerPropData.total.precent.main
        elseif index == 3 then val = LuaPlayerPropertyMgr.PlayerPropData.total.extra.main
        elseif index == 4 then val = LuaPlayerPropertyMgr.PlayerPropData.total.other.main
        end
    else
        if index == 0 then val = LuaPlayerPropertyMgr.OppositePropData.total.all.main
        elseif index == 1 then val = LuaPlayerPropertyMgr.OppositePropData.total.base.main
        elseif index == 2 then val = LuaPlayerPropertyMgr.OppositePropData.total.precent.main
        elseif index == 3 then val = LuaPlayerPropertyMgr.OppositePropData.total.extra.main
        elseif index == 4 then val = LuaPlayerPropertyMgr.OppositePropData.total.other.main
        end
    end
    return val
end
function LuaPlayerPropertyCompareWnd:GetItemValue(index,wordID,isOpposite)
    local wordData = nil
    if wordID >= 1000 then
        wordData = PropGuide_PropertyWords.GetData(wordID - index * 1000)
    else
        wordData = PropGuide_PropertyWords.GetData(wordID)
    end
    local t = {val = 0,desc = "",showval = 0}
    if not wordData then return t end
    local wordName = wordData.Desc
    local specialID = PropGuide_Setting.GetData().SpecialPropertyWordID
    if wordID == specialID then
        wordName =  PropGuide_Property.GetData(self.m_PropertyID).SpecialWordDes
    end
    local equipList = {}
    local wndDes = nil
    if not isOpposite then 
        wndDes = wordData.wndName
    end
    if not isOpposite then
        local data =    LuaPlayerPropertyMgr.PlayerPropData.propWords[wordID] or 
                        LuaPlayerPropertyMgr.PlayerPropData.propWords[wordID + index * 1000] or 
                        LuaPlayerPropertyMgr.PlayerPropData.propWords[wordID - index * 1000]
        t.val = data and data.val or 0
        if index == 2 then t.val = t.val * 100  end
        if LuaPlayerPropertyMgr.PlayerPropData.propWords[wordID] then
            equipList = LuaPlayerPropertyMgr.PlayerPropData.propWords[wordID].equip
        end
    elseif isOpposite then
        local data =    LuaPlayerPropertyMgr.OppositePropData.propWords[wordID] or 
                        LuaPlayerPropertyMgr.OppositePropData.propWords[wordID + index * 1000] or 
                        LuaPlayerPropertyMgr.OppositePropData.propWords[wordID - index * 1000]
        t.val = data and data.val or 0
        if index == 2 then t.val = t.val * 100 end
        if LuaPlayerPropertyMgr.OppositePropData.propWords[wordID] then
            equipList = LuaPlayerPropertyMgr.OppositePropData.propWords[wordID].equip
        end
    end
    t.desc,t.showval,t.val = self:BuildString(wordName,t.val,wndDes,index == 2)
    return t,equipList
end
function LuaPlayerPropertyCompareWnd:GetEquipList(tableList,equipList,index)
    if equipList and #equipList > 0 then
        for i = 1,#equipList do
            local item = CItemMgr.Inst:GetById(equipList[i])
            if item then
                local sign = ""
                local existingLinks = CreateFromClass(MakeGenericClass(Dictionary, Int32, CChatInputLink))
                local text = CChatInputLink.AppendItemLink(sign,item,existingLinks)
                local itemLink = CChatInputLink.Encapsulate(text, existingLinks)
                local des = CUICommonDef.TranslateToNGUIText(itemLink)
                table.insert(tableList,{
                    type = 4,
                    Value = des,
                    index = index,
                })
            elseif not System.String.IsNullOrEmpty(equipList[i]) then
                if LuaPlayerPropertyMgr.EquipmentData and LuaPlayerPropertyMgr.EquipmentData[equipList[i]] then
                    item = LuaPlayerPropertyMgr.EquipmentData[equipList[i]]
                    local sign = ""
                    local existingLinks = CreateFromClass(MakeGenericClass(Dictionary, Int32, CChatInputLink))
                    local text = CChatInputLink.AppendItemLink(sign,item,existingLinks)
                    local itemLink = CChatInputLink.Encapsulate(text, existingLinks)
                    local des = CUICommonDef.TranslateToNGUIText(itemLink)
                    table.insert(tableList,{
                        type = 4,
                        Value = des,
                        index = index,
                    })
                else
                    table.insert(tableList,{
                        type = 5,
                        Value = equipList[i],
                        index = index,
                    })
                end
            end
        end
    end
end
function LuaPlayerPropertyCompareWnd:BuildString(worldName,val,wndName,isPrecent)
    local stringbuilder = NewStringBuilderWraper(StringBuilder)
    stringbuilder:Append(worldName)
    local absVal = math.floor(math.abs(val))
    local resVal = absVal
    local showVal = absVal
    if isPrecent then 
        absVal = math.abs(val) - math.abs(val)%0.01
        if math.abs(absVal - 0) < 0.01 then resVal = 0 else resVal = 10 end
        showVal = absVal
    end
    if val < 0 and absVal ~= 0 then 
        stringbuilder:Append(" - ")
        showVal = -1 * showVal
    else
        stringbuilder:Append(" + ")
    end
    
    local value 
    if isPrecent then value = tostring(absVal) .. "%" 
    else value = tostring(absVal) end
    stringbuilder:Append(value)

    if wndName and not System.String.IsNullOrEmpty(wndName) then
        stringbuilder:Append(wndName)
    end
    
    return stringbuilder:ToString(),showVal,resVal
end

function LuaPlayerPropertyCompareWnd:GetGuideGo(methodName)
    if methodName == "GetExpandBtn" then
        if self.m_View and self.m_View:GetComponent(typeof(CCommonLuaScript)) then
            return self.m_View:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf:GetGuideBtn()
        else return nil
        end
    end
end
--@region UIEvent

--@endregion UIEvent
function LuaPlayerPropertyCompareWnd:OnEnable()
    g_ScriptEvent:AddListener("OnCompareOnlinePropResult",self,"UpdateValue")
end
function LuaPlayerPropertyCompareWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnCompareOnlinePropResult",self,"UpdateValue")
    LuaPlayerPropertyMgr.PlayerCompareData = nil
    LuaPlayerPropertyMgr.PropertyTypeList = nil
    LuaPlayerPropertyMgr.OppositePropData = nil
    LuaPlayerPropertyMgr.EquipmentData = nil
end
