local Vector3 = import "UnityEngine.Vector3"
local SoundManager = import "SoundManager"
local CQingQiuZhanBuWnd = import "L10.UI.CQingQiuZhanBuWnd"
local Random = import "UnityEngine.Random"
local Animation=import "UnityEngine.Animation"


CLuaNpcHaoGanDuZhanBuWnd = class()
RegistClassMember(CLuaNpcHaoGanDuZhanBuWnd,"qianName")

RegistClassMember(CLuaNpcHaoGanDuZhanBuWnd,"qiantongAni")
RegistClassMember(CLuaNpcHaoGanDuZhanBuWnd,"qianAni")

RegistClassMember(CLuaNpcHaoGanDuZhanBuWnd,"m_ServerNameLabel")
RegistClassMember(CLuaNpcHaoGanDuZhanBuWnd,"m_CharacterNameLabel")

RegistClassMember(CLuaNpcHaoGanDuZhanBuWnd,"m_Fx")

RegistClassMember(CLuaNpcHaoGanDuZhanBuWnd,"QianClipName")
RegistClassMember(CLuaNpcHaoGanDuZhanBuWnd,"QianTongClipName")
RegistClassMember(CLuaNpcHaoGanDuZhanBuWnd,"isShaked")
RegistClassMember(CLuaNpcHaoGanDuZhanBuWnd,"m_IsShaking")
RegistClassMember(CLuaNpcHaoGanDuZhanBuWnd,"s_FxPath")

RegistClassMember(CLuaNpcHaoGanDuZhanBuWnd,"m_OnPlayerShakeAction")
RegistClassMember(CLuaNpcHaoGanDuZhanBuWnd,"m_StopQianTick")
RegistClassMember(CLuaNpcHaoGanDuZhanBuWnd,"m_Fortunate")
RegistClassMember(CLuaNpcHaoGanDuZhanBuWnd,"m_Unfortunate")
RegistClassMember(CLuaNpcHaoGanDuZhanBuWnd,"m_Result")
RegistClassMember(CLuaNpcHaoGanDuZhanBuWnd,"m_AutoCloseTick")
CLuaNpcHaoGanDuZhanBuWnd.m_TaskID = 0
CLuaNpcHaoGanDuZhanBuWnd.m_EventName = ""
CLuaNpcHaoGanDuZhanBuWnd.m_Probability = 0.5

function CLuaNpcHaoGanDuZhanBuWnd:Awake()
    self.qianName = self.transform:Find("Qian/QianName"):GetComponent(typeof(CUITexture))
    self.shareBtn = self.transform:Find("ShareBtn").gameObject
    self.qiantongAni = self.transform:Find("QianTong"):GetComponent(typeof(Animation))
    self.qianAni = self.transform:Find("Qian"):GetComponent(typeof(Animation))
    --self.m_ServerNameLabel = self.transform:Find("LeftItem/Server"):GetComponent(typeof(UILabel))
   -- self.m_CharacterNameLabel = self.transform:Find("LeftItem/Character"):GetComponent(typeof(UILabel))
   -- self.m_DownloadQRCodeTex = self.transform:Find("LeftItem/Code"):GetComponent(typeof(UITexture))
    self.m_Fx = self.transform:Find("Fx"):GetComponent(typeof(CUIFx))
    self.QianClipName = "YuanDanQiuQian_Qian"
    self.QianTongClipName = "YuanDanQiuQian_QianTong"
    self.m_IsShaking = false
    self.s_FxPath = "fx/ui/prefab/UI_taohuaxia_qiantong.prefab"
    self.m_Fortunate = "UI/Texture/Transparent/Material/yuandan_yaoqian_name_01.mat"
    self.m_Unfortunate = "UI/Texture/Transparent/Material/yuandan_yaoqian_name_05.mat"
    self.m_Result = false
end

-- Auto Generated!!
function CLuaNpcHaoGanDuZhanBuWnd:Init( )
    self.isShaked = false
    self:SamleAni(self.qiantongAni, self.QianTongClipName, 0)
    self:SamleAni(self.qianAni, self.QianClipName, 0)

    local random = Random.value
    self.m_Result = (random < CLuaNpcHaoGanDuZhanBuWnd.m_Probability)
    if (self.m_Result) then
        self.qianName:LoadMaterial(self.m_Fortunate)
    else
        self.qianName:LoadMaterial(self.m_Unfortunate)
    end
end
function CLuaNpcHaoGanDuZhanBuWnd:SamleAni( anim, clipName, time) 

    anim:get_Item(clipName).time = time
    anim:get_Item(clipName).enabled = true
    anim:get_Item(clipName).weight = 1
    anim:Sample()
    anim:get_Item(clipName).enabled = false
end

function CLuaNpcHaoGanDuZhanBuWnd:OnEnable()
    self.m_StopQianTick = nil
    self.m_AutoCloseTick = nil
    self.m_OnPlayerShakeAction = DelegateFactory.Action(function() self:OnPlayerShakeDevice() end)
    EventManager.AddListener(EnumEventType.PlayerShakeDevice, self.m_OnPlayerShakeAction)
    g_MessageMgr:ShowMessage("YuanDan_YaoYiYao")
end

function CLuaNpcHaoGanDuZhanBuWnd:OnDisable()
    EventManager.RemoveListener(EnumEventType.PlayerShakeDevice, self.m_OnPlayerShakeAction)
end

function CLuaNpcHaoGanDuZhanBuWnd:OnDestroy()

    if(self.m_StopQianTick)then
        UnRegisterTick(self.m_StopQianTick)
        self.m_StopQianTick = nil
    end

    if(self.m_AutoCloseTick)then
        UnRegisterTick(self.m_AutoCloseTick)
        self.m_AutoCloseTick = nil
    end
end

function CLuaNpcHaoGanDuZhanBuWnd:OnPlayerShakeDevice( )

    if self.isShaked then
        return
    end
    self.isShaked = true

    self:StartShake()
end
function CLuaNpcHaoGanDuZhanBuWnd:StartShake( )
    self.qiantongAni:get_Item(self.QianTongClipName).time = 0
    self.qianAni:get_Item(self.QianClipName).time = 0
    self.qiantongAni:Play()
    self.qianAni:Play()
    if(self.m_Result)then
        Gac2Gas.HaoGanDuTaskQiuQianComplete(CLuaNpcHaoGanDuZhanBuWnd.m_TaskID, CLuaNpcHaoGanDuZhanBuWnd.m_EventName, 1)
    else
        Gac2Gas.HaoGanDuTaskQiuQianComplete(CLuaNpcHaoGanDuZhanBuWnd.m_TaskID, CLuaNpcHaoGanDuZhanBuWnd.m_EventName, 0)
    end
    if(self.m_StopQianTick)then
        UnRegisterTick(self.m_StopQianTick)
        self.m_StopQianTick = nil
    end
    self.m_StopQianTick = RegisterTickOnce(function()
        self.qianAni.enabled = false
    end, 4250)

    if(self.m_AutoCloseTick)then
        UnRegisterTick(self.m_AutoCloseTick)
        self.m_AutoCloseTick = nil
    end

    self.m_AutoCloseTick = RegisterTickOnce(function()
        CUIManager.CloseUI(CLuaUIResources.NpcHaoGanDuZhanBuWnd)
    end, 6000)

    if not CQingQiuZhanBuWnd.s_EnableAnimCallback then
        SoundManager.Inst:PlayOneShot(SoundManager.UISoundEvents.YuanDanYaoQianound, Vector3.zero, nil, 0, -1)
    end
end

