local QnAddSubAndInputButton=import "L10.UI.QnAddSubAndInputButton"
local CCurentMoneyCtrl=import "L10.UI.CCurentMoneyCtrl"

CLuaNpcHaoGanDuBuyGiftCountWnd = class()
RegistClassMember(CLuaNpcHaoGanDuBuyGiftCountWnd,"m_MaxTime")


function CLuaNpcHaoGanDuBuyGiftCountWnd:Init()
    local setting = NpcHaoGanDu_Setting.GetData()

    local buyNum = 0

    local playProp = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp
    if playProp then
        local ret,data = CommonDefs.DictTryGet_LuaCall(playProp.NpcHaoGanDuData,CLuaNpcHaoGanDuWnd.m_NpcId)
        if not ret then
            --没有，最大赠送数量
            self.m_MaxTime = setting.BuyZengSongNumPrice.Length
            -- data = CNpcHaoGanDuObject()
        else
            buyNum=data.BuyNum
            self.m_MaxTime = setting.BuyZengSongNumPrice.Length-data.BuyNum
        end
    end

    local costCmp = FindChild(self.transform,"QnCostAndOwnMoney"):GetComponent(typeof(CCurentMoneyCtrl))


    local input = FindChild(self.transform,"QnIncreseAndDecreaseButton"):GetComponent(typeof(QnAddSubAndInputButton))
    input.onValueChanged = DelegateFactory.Action_uint(function(v)
        if v>0 then
            local cost = 0
            for i=1,v do
                cost = cost + setting.BuyZengSongNumPrice[buyNum+i-1][1]
            end
            costCmp:SetCost(cost)
        else
            costCmp:SetCost(0)
        end
    end)
    if self.m_MaxTime>0 then
        input:SetMinMax(1,self.m_MaxTime,1)
        input:SetValue(1,true)
    else
        input:SetMinMax(0,0,1)
        input:SetValue(0,true)
    end



    


    local okButton = FindChild(self.transform,"OkButton").gameObject
    UIEventListener.Get(okButton).onClick = DelegateFactory.VoidDelegate(function(go)
        local count= input:GetValue()
        if count>0 then
            Gac2Gas.RequestBuyNpcHaoGanDuZengSongNum(CLuaNpcHaoGanDuWnd.m_NpcId,count)
        else
            --不能再买了
        end
        CUIManager.CloseUI(CLuaUIResources.NpcHaoGanDuBuyGiftCountWnd)
    end)

    local cancelButton = FindChild(self.transform,"CancelButton").gameObject
    UIEventListener.Get(cancelButton).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.CloseUI(CLuaUIResources.NpcHaoGanDuBuyGiftCountWnd)
    end)
end

