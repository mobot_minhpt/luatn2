local UISprite = import "UISprite"
local CSocialWndMgr = import "L10.UI.CSocialWndMgr"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CChatMgr = import "L10.Game.CChatMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local CChatLinkMgr = import "CChatLinkMgr"

LuaQuweiLabaTipWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaQuweiLabaTipWnd, "ConfirmBtn", "ConfirmBtn", GameObject)
RegistChildComponent(LuaQuweiLabaTipWnd, "ContentLabel", "ContentLabel", UILabel)
RegistChildComponent(LuaQuweiLabaTipWnd, "Bubble", "Bubble", UISprite)
RegistChildComponent(LuaQuweiLabaTipWnd, "Icon01", "Icon01", UISprite)
RegistChildComponent(LuaQuweiLabaTipWnd, "Icon02", "Icon02", UISprite)
RegistChildComponent(LuaQuweiLabaTipWnd, "Icon03", "Icon03", UISprite)

--@endregion RegistChildComponent end
RegistClassMember(LuaQuweiLabaTipWnd,"m_Msg")

function LuaQuweiLabaTipWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.ConfirmBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnConfirmBtnClick()
	end)


	
	UIEventListener.Get(self.ContentLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnContentLabelClick()
	end)


    --@endregion EventBind end
end

function LuaQuweiLabaTipWnd:Init()
	self.m_Msg = CChatMgr.Inst and CChatMgr.Inst.m_FuxiLabaMsg
	if self.m_Msg == nil then
		CUIManager.CloseUI(CLuaUIResources.QuweiLabaTipWnd)
		return
	end
	local type = self.m_Msg.subType

	self.Icon02.spriteName = "chat_background_9_2"
	self.Icon03.spriteName = "chat_background_9_3_" .. tostring(type)
	self.Icon01.spriteName = "chat_background_9_1_" .. tostring(type)

	self.ContentLabel.text = g_MessageMgr:FormatMessage("Quwei_Laba_Tip_Wnd_Type_".. tostring(4 - type), self.m_Msg.fromUserId ,self.m_Msg.fromUserName)
end

--@region UIEvent

function LuaQuweiLabaTipWnd:OnConfirmBtnClick()
	CSocialWndMgr.ShowChatWnd(EChatPanel.World)
	CUIManager.CloseUI(CLuaUIResources.QuweiLabaTipWnd)
end

function LuaQuweiLabaTipWnd:OnContentLabelClick()
	print("test")
	local url = self.ContentLabel:GetUrlAtPosition(UICamera.lastWorldPosition)
    if url ~= nil then
        CChatLinkMgr.ProcessLinkClick(url, nil)
    end
end


--@endregion UIEvent

