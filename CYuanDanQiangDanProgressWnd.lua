-- Auto Generated!!
local CScene = import "L10.Game.CScene"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CYuanDanMgr = import "L10.Game.CYuanDanMgr"
local CYuanDanQiangDanProgressWnd = import "L10.UI.CYuanDanQiangDanProgressWnd"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Int32 = import "System.Int32"
local Vector3 = import "UnityEngine.Vector3"
CYuanDanQiangDanProgressWnd.m_Init_CS2LuaHook = function (this) 
    this.expandBtn.transform.localEulerAngles = Vector3(0, 0, 180)
    this:OnRemainTimeUpdate(CScene.MainScene ~= nil and CScene.MainScene.ShowTime or 0)
    this:OnEggInfoUpdate()
    this:OnSoulOutInfoUpdate()
    this:OnMainPlayerCreated()
end
CYuanDanQiangDanProgressWnd.m_OnEnable_CS2LuaHook = function (this) 
    EventManager.AddListener(EnumEventType.HideTopAndRightTipWnd, MakeDelegateFromCSFunction(this.OnHideTopAndRightTipWnd, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.SceneRemainTimeUpdate, MakeDelegateFromCSFunction(this.OnRemainTimeUpdate, MakeGenericClass(Action1, Int32), this))
    EventManager.AddListener(EnumEventType.YuanDanQiangDanEggsInfoUpdate, MakeDelegateFromCSFunction(this.OnEggInfoUpdate, Action0, this))
    EventManager.AddListener(EnumEventType.YuanDanQiangDanSoulOutInfoUpdate, MakeDelegateFromCSFunction(this.OnSoulOutInfoUpdate, Action0, this))
    EventManager.AddListener(EnumEventType.YuanDanQiangDanSceneDurationInfoUpdate, MakeDelegateFromCSFunction(this.OnDurationInfoUpdate, Action0, this))

    EventManager.AddListener(EnumEventType.MainPlayerCreated, MakeDelegateFromCSFunction(this.OnMainPlayerCreated, Action0, this))
end
CYuanDanQiangDanProgressWnd.m_OnDisable_CS2LuaHook = function (this) 
    EventManager.RemoveListener(EnumEventType.HideTopAndRightTipWnd, MakeDelegateFromCSFunction(this.OnHideTopAndRightTipWnd, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.SceneRemainTimeUpdate, MakeDelegateFromCSFunction(this.OnRemainTimeUpdate, MakeGenericClass(Action1, Int32), this))
    EventManager.RemoveListener(EnumEventType.YuanDanQiangDanEggsInfoUpdate, MakeDelegateFromCSFunction(this.OnEggInfoUpdate, Action0, this))
    EventManager.RemoveListener(EnumEventType.YuanDanQiangDanSoulOutInfoUpdate, MakeDelegateFromCSFunction(this.OnSoulOutInfoUpdate, Action0, this))
    EventManager.RemoveListener(EnumEventType.YuanDanQiangDanSceneDurationInfoUpdate, MakeDelegateFromCSFunction(this.OnDurationInfoUpdate, Action0, this))

    EventManager.RemoveListener(EnumEventType.MainPlayerCreated, MakeDelegateFromCSFunction(this.OnMainPlayerCreated, Action0, this))
end
CYuanDanQiangDanProgressWnd.m_OnRemainTimeUpdate_CS2LuaHook = function (this, leftTime) 
    if leftTime < 0 then
        leftTime = 0
    end
    this.leftTimeLabel.text = System.String.Format("{0:00}:{1:00}", math.floor(leftTime / 60), leftTime % 60)
    this:OnDurationInfoUpdate()
end
CYuanDanQiangDanProgressWnd.m_OnDurationInfoUpdate_CS2LuaHook = function (this) 
    local totalSeconds = CYuanDanMgr.Inst.PlayDuration
    local passedSeconds = math.floor((CServerTimeMgr.Inst.timeStamp - CYuanDanMgr.Inst.PlayStartTime))
    if passedSeconds < 0 then
        passedSeconds = 0
    end
    this.progressBar.value = (totalSeconds > 0) and 1 - math.min(math.max(passedSeconds * 1 / totalSeconds, 0), 1) or 0
end
