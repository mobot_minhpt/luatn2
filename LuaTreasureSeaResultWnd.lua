local Animation = import "UnityEngine.Animation"
local CScheduleMgr=import "L10.Game.CScheduleMgr"
LuaTreasureSeaResultWnd = class()

RegistChildComponent(LuaTreasureSeaResultWnd, "RankBtn", "RankBtn", GameObject)
RegistChildComponent(LuaTreasureSeaResultWnd, "ShareBtn", "ShareBtn", GameObject)
RegistChildComponent(LuaTreasureSeaResultWnd, "PlayerInfoRoot", "PlayerInfoRoot", Transform)
RegistChildComponent(LuaTreasureSeaResultWnd, "RankLabel", "RankLabel", UILabel)
RegistChildComponent(LuaTreasureSeaResultWnd, "ScoreLabel", "ScoreLabel", UILabel)
RegistChildComponent(LuaTreasureSeaResultWnd, "KillCountLabel", "KillCountLabel", UILabel)
RegistChildComponent(LuaTreasureSeaResultWnd, "KillCountLabel2", "KillCountLabel2", UILabel)
RegistChildComponent(LuaTreasureSeaResultWnd, "NoReward", "NoReward", GameObject)
RegistChildComponent(LuaTreasureSeaResultWnd, "Reward", "Reward", GameObject)


RegistClassMember(LuaTreasureSeaResultWnd, "m_PlayId")


LuaTreasureSeaResultWnd.s_Rank = 0
LuaTreasureSeaResultWnd.s_KillShip = 0
LuaTreasureSeaResultWnd.s_KillMonster = 0
LuaTreasureSeaResultWnd.s_Info = nil

function LuaTreasureSeaResultWnd:Awake()

    UIEventListener.Get(self.RankBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        LuaTreasureSeaRankRewardDetailWnd.s_Tab = 0
        CUIManager.ShowUI(CLuaUIResources.TreasureSeaRankRewardDetailWnd)
    end)
    UIEventListener.Get(self.ShareBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        CUICommonDef.CaptureScreenAndShare()
    end)

    self.Reward:SetActive(false)
    self.NoReward:SetActive(false)
end

function LuaTreasureSeaResultWnd:Init()
    local rank = LuaTreasureSeaResultWnd.s_Rank
    self.RankLabel.text = tostring(rank)
    local animation = self.transform:GetComponent(typeof(Animation))
    if rank<3 then--1、2
        animation:Play("treasuresearesultwnd_show")
    else
        animation:Play("treasuresearesultwnd_show_1")
    end

    local cnt = CScheduleMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eNavalWarPvpElo)

    self.KillCountLabel.text = tostring(LuaTreasureSeaResultWnd.s_KillShip)
    self.KillCountLabel2.text = tostring(LuaTreasureSeaResultWnd.s_KillMonster)

    local deltaScore = 0
    local myId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0

    for i = 1, 5 do
        local child = self.PlayerInfoRoot:Find(tostring(i))
        if child then
            child:Find("NameLabel"):GetComponent(typeof(UILabel)).text = nil
        end
    end

    local HaiZhanPvPRewardItem = ShuJia2023_Setting.GetData("HaiZhanPvPRewardItem").Value
    local items = {}
    local rank = LuaTreasureSeaResultWnd.s_Rank
    local rewardTypeLabel = self.Reward.transform:Find("Item/RewardTypeLabel"):GetComponent(typeof(UILabel))
    local icon = self.Reward.transform:Find("Item/Icon"):GetComponent(typeof(CUITexture))
    for num,templateId in string.gmatch(HaiZhanPvPRewardItem, "(%d+),(%d+);") do
        if rank==1 and num==2 then
            rewardTypeLabel.text = LocalString.GetString("魁元")--SafeStringFormat3(LocalString.GetString("第%d名"),1)
            icon:LoadMaterial(Item_Item.GetData(templateId).Icon)
        elseif (rank==2 or rank==3) and num==3 then
            rewardTypeLabel.text = LocalString.GetString("优胜")--SafeStringFormat3(LocalString.GetString("%d-%d名"),2,3)
            icon:LoadMaterial(Item_Item.GetData(templateId).Icon)
        elseif (rank==4 or rank==5) and num==4 then
            rewardTypeLabel.text = LocalString.GetString("精英")--SafeStringFormat3(LocalString.GetString("%d-%d名"),4,5)
            icon:LoadMaterial(Item_Item.GetData(templateId).Icon)
        end
    end


    if LuaTreasureSeaResultWnd.s_Info then
        for key, value in pairs(LuaTreasureSeaResultWnd.s_Info) do
            local col = nil
            if myId==key then
                deltaScore = value.deltaElo or 0
                col = NGUIText.ParseColor24("00920B", 0)
            end
            local child = self.PlayerInfoRoot:Find(tostring(value.seatId))
            if child then
                local label = child:Find("NameLabel"):GetComponent(typeof(UILabel))
                label.text = value.name
                if col then label.color = col end
                local script = child:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
                script:Init(key,value.class,value.gender)
            end
        end
    end
    if deltaScore==0 then
        self.ScoreLabel.text = SafeStringFormat3("%d",cnt)
    elseif deltaScore<0 then
        self.ScoreLabel.text = SafeStringFormat3("%d[ff0000](%d)[-]",cnt,deltaScore)
    elseif deltaScore>0 then
        self.ScoreLabel.text = SafeStringFormat3("%d[00ff60](+%d)[-]",cnt,deltaScore)
    end


end

