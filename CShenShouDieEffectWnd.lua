-- Auto Generated!!
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CShenShouDieEffectMgr = import "L10.UI.CShenShouDieEffectMgr"
local CShenShouDieEffectWnd = import "L10.UI.CShenShouDieEffectWnd"
local Monster_Monster = import "L10.Game.Monster_Monster"
CShenShouDieEffectWnd.m_Init_CS2LuaHook = function (this) 
    --显示2秒钟？
    --显示2秒钟？
    this.startTime = CServerTimeMgr.Inst:GetZone8Time()
    this.endTime = this.startTime:AddSeconds(3)

    local templateId = CShenShouDieEffectMgr.teamplateId
    local data = Monster_Monster.GetData(templateId)
    if data ~= nil then
        this.icon:LoadNPCPortrait(data.HeadIcon, false)
    end
    if this.co ~= nil then
        this:StopCoroutine(this.co)
        this.co = nil
    end
    this.co = this:StartCoroutine(this:Show())
end
