local CUITexture = import "L10.UI.CUITexture"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local Extensions = import "Extensions"
local CButton = import "L10.UI.CButton"
local EnumGender = import "L10.Game.EnumGender"
local UISprite = import "UISprite"
local UISlider = import "UISlider"
local CCustomSelector = import "L10.UI.CCustomSelector"
local CPopupMenuInfoMgrAlignType = import "L10.UI.CPopupMenuInfoMgr+AlignType"

LuaAppearanceClosetFashionTabenSubview = class()

RegistChildComponent(LuaAppearanceClosetFashionTabenSubview,"m_ClosetFashionTabenItem", "ClosetFashionItem", GameObject)
RegistChildComponent(LuaAppearanceClosetFashionTabenSubview,"m_ContentTable", "ContentTable", UITable)
RegistChildComponent(LuaAppearanceClosetFashionTabenSubview,"m_ContentScrollView", "ContentScrollView", UIScrollView)
RegistChildComponent(LuaAppearanceClosetFashionTabenSubview, "m_SortSelector", "SortSelector", CCustomSelector)

RegistClassMember(LuaAppearanceClosetFashionTabenSubview, "m_SortNames")
RegistClassMember(LuaAppearanceClosetFashionTabenSubview, "m_SortIndex")
RegistClassMember(LuaAppearanceClosetFashionTabenSubview, "m_MyTabens")
RegistClassMember(LuaAppearanceClosetFashionTabenSubview, "m_SelectedDataId")

function LuaAppearanceClosetFashionTabenSubview:Awake() 
end

function LuaAppearanceClosetFashionTabenSubview:InitSortSelector()
    self.m_SortSelector.gameObject:SetActive(true)
    self.m_SortNames = LuaAppearancePreviewMgr:GetMyTabenInfoSortNames()
    local names = Table2List(self.m_SortNames, MakeGenericClass(List,cs_string))
    self.m_SortSelector:Init(names, nil, DelegateFactory.Action_int(function ( index )
        self.m_SortIndex = index + 1
		self.m_SortSelector:SetName(names[index])
        self:LoadData()
    end))
    self.m_SortIndex = 1
    self.m_SortSelector:SetName(self.m_SortNames[self.m_SortIndex])
end


function LuaAppearanceClosetFashionTabenSubview:Init()
    self:InitSortSelector()
    self:SetDefaultSelection()
    self:LoadData()
end

function LuaAppearanceClosetFashionTabenSubview:GetEmptyItemCount()
    return math.max(0, LuaAppearancePreviewMgr:GetTabenNumLimit() - #self.m_MyTabens)--剩余可用格子数
end

function LuaAppearanceClosetFashionTabenSubview:NeedShowUnlockItem()
    return LuaAppearancePreviewMgr:GetTabenNumLimit()<LuaAppearancePreviewMgr:GetTabenMaxNumLimit()
end

function LuaAppearanceClosetFashionTabenSubview:LoadData()
    self.m_MyTabens = LuaAppearancePreviewMgr:GetMyTabenInfo(self.m_SortIndex)
    Extensions.RemoveAllChildren(self.m_ContentTable.transform)
    self.m_ContentTable.gameObject:SetActive(true)
    local extCount = self:GetEmptyItemCount() + (self:NeedShowUnlockItem() and 1 or 0)
    for i = 1, #self.m_MyTabens+extCount do --末尾增加一个空的item用于制作拓本
        local child = CUICommonDef.AddChild(self.m_ContentTable.gameObject, self.m_ClosetFashionTabenItem)
        child:SetActive(true)
        self:InitItem(child, self.m_MyTabens[i], i==(#self.m_MyTabens+extCount))
    end
   
    self.m_ContentTable:Reposition()
    self.m_ContentScrollView:ResetPosition()
    self:InitSelection()
end

function LuaAppearanceClosetFashionTabenSubview:SetDefaultSelection()
    self.m_SelectedDataId = 0 --拓本的情况有点特殊,所以这里设置为0
end

function LuaAppearanceClosetFashionTabenSubview:InitSelection()
    if self.m_SelectedDataId == 0 then
        self:OnItemClick(nil, false)
        return
    end

    local childCount = self.m_ContentTable.transform.childCount
    for i=0, childCount-1 do
        local childGo = self.m_ContentTable.transform:GetChild(i).gameObject
        local appearanceData = self.m_MyTabens[i+1]
        if appearanceData and appearanceData.id == self.m_SelectedDataId then
            self:OnItemClick(childGo, false)
            break
        end
    end
end

function LuaAppearanceClosetFashionTabenSubview:InitItem(itemGo, appearanceData, isLast)
    local iconTexture = itemGo.transform:Find("Item"):GetComponent(typeof(CUITexture))
    local cornerGo = itemGo.transform:Find("Item/Corner").gameObject
    local nameLabel = itemGo.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    --复用fashion结构，不过label展示内容调整一下
    local positionLabel = itemGo.transform:Find("TimeLabel"):GetComponent(typeof(UILabel))
    local timeLabel = itemGo.transform:Find("ProgressLabel"):GetComponent(typeof(UILabel))

    local progressBar = itemGo.transform:Find("ProgressBar"):GetComponent(typeof(UISlider))
    local addGo = itemGo.transform:Find("Item/Add").gameObject
    local disabledGo = itemGo.transform:Find("Item/Disabled").gameObject
    local lockedGo = itemGo.transform:Find("Item/Locked").gameObject

    progressBar.gameObject:SetActive(false)
    if appearanceData == nil then
        local isUnlockItem = isLast and self:NeedShowUnlockItem()
        addGo:SetActive(not isUnlockItem)
        lockedGo:SetActive(isUnlockItem)
        disabledGo:SetActive(false)
        iconTexture:Clear()
        cornerGo:SetActive(false)
        nameLabel.text = ""
        positionLabel.text = ""
        timeLabel.text = ""
        UIEventListener.Get(itemGo).onClick = DelegateFactory.VoidDelegate(function(go)
            self:OnItemClick(go)
        end)
        return
    end

    iconTexture:LoadMaterial(appearanceData.icon)
    itemGo:GetComponent(typeof(CButton)).Selected = (self.m_SelectedDataId and self.m_SelectedDataId==appearanceData.id or false)
    cornerGo:SetActive(LuaAppearancePreviewMgr:IsCurrentInUseFashion(appearanceData.position, appearanceData.id))
    nameLabel.text = appearanceData.name
    positionLabel.text = LuaAppearancePreviewMgr:GetFashionPositionName(appearanceData.position)
    timeLabel.text = LuaAppearancePreviewMgr:GetTabenExpiredText(appearanceData.expiredTime)
    addGo:SetActive(false)
    disabledGo:SetActive(LuaAppearancePreviewMgr:IsTabenExpired(appearanceData.expiredTime))
    lockedGo:SetActive(false)
    UIEventListener.Get(itemGo).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnItemClick(go, true)
    end)
end

function LuaAppearanceClosetFashionTabenSubview:OnItemClick(go, isClick)
    local childCount = self.m_ContentTable.transform.childCount
    local tabenCount = #self.m_MyTabens
    for i=0, childCount-1 do
        local childGo = self.m_ContentTable.transform:GetChild(i).gameObject
        if childGo == go then
            if i==childCount-1 then
                if self:NeedShowUnlockItem() then
                    LuaAppearancePreviewMgr:ExpandTabenNumLimit()
                else
                    CUIManager.ShowUI("AppearanceFashionTabenWnd")
                end
            elseif i>=tabenCount then
                CUIManager.ShowUI("AppearanceFashionTabenWnd")
            end
            break
        end
    end
    
    for i=0, childCount-self:GetEmptyItemCount()-(self:NeedShowUnlockItem() and 1 or 0) -1 do
        local childGo = self.m_ContentTable.transform:GetChild(i).gameObject
        if childGo == go then
            childGo.transform:GetComponent(typeof(CButton)).Selected = true
            self.m_SelectedDataId = self.m_MyTabens[i+1].id
            if isClick then
                self:ShowPopupMenu(go, self.m_MyTabens[i+1])
            end
        else
            childGo.transform:GetComponent(typeof(CButton)).Selected = false
        end
    end
end

function LuaAppearanceClosetFashionTabenSubview:Delete(appearanceData)
    g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("Fashion_Discard_Notice",appearanceData.name), function ()
        LuaAppearancePreviewMgr:RequestDiscardFashion_Permanent(appearanceData.id)
    end, nil, nil, nil, false)
end


function LuaAppearanceClosetFashionTabenSubview:ShowPopupMenu(go, appearanceData)
    --更换/脱下/丢弃
    local takeOnMenu = g_PopupMenuMgr:CreateOneDefaultPopupMenuItem(LocalString.GetString("更换"), function()
        LuaAppearancePreviewMgr:RequestTakeOnFashion_Permanent(appearanceData.id) 
    end, false)
    local takeOffMenu = g_PopupMenuMgr:CreateOneDefaultPopupMenuItem(LocalString.GetString("脱下"), function() LuaAppearancePreviewMgr:RequestTakeOffFashion_Permanent(appearanceData.id) end, false)
    local deleteMenu = g_PopupMenuMgr:CreateOneDefaultPopupMenuItem(LocalString.GetString("丢弃"), function() self:Delete(appearanceData) end, false)
    
    local tbl = {}
    if LuaAppearancePreviewMgr:IsCurrentInUseFashion(appearanceData.position, appearanceData.id) then
        table.insert(tbl, takeOffMenu)
    elseif not LuaAppearancePreviewMgr:IsTabenExpired(appearanceData.expiredTime) then --非过期的才显示更换
        table.insert(tbl, takeOnMenu)
    end
    table.insert(tbl, deleteMenu)
    g_PopupMenuMgr:ShowPopupMenuOnBottom(tbl, go.transform)
end

function LuaAppearanceClosetFashionTabenSubview:OnEnable()
    g_ScriptEvent:AddListener("OnSyncMainPlayerAppearanceProp",self,"SyncMainPlayerAppearancePropUpdate")
    g_ScriptEvent:AddListener("OnSyncWardrobeProperty",self,"OnSyncWardrobeProperty")
    g_ScriptEvent:AddListener("OnCompositeTabenSuccess_Permanent", self, "OnCompositeTabenSuccess_Permanent")
    g_ScriptEvent:AddListener("OnSetWardRobeTabenNumLimit_Permanent", self, "OnSetWardRobeTabenNumLimit_Permanent")
    g_ScriptEvent:AddListener("OnSetFashionHideSuccess_Permanent",self,"OnSetFashionHideSuccess_Permanent")
end

function LuaAppearanceClosetFashionTabenSubview:OnDisable()
    g_ScriptEvent:RemoveListener("OnSyncMainPlayerAppearanceProp",self,"SyncMainPlayerAppearancePropUpdate")
    g_ScriptEvent:RemoveListener("OnSyncWardrobeProperty",self,"OnSyncWardrobeProperty")
    g_ScriptEvent:RemoveListener("OnCompositeTabenSuccess_Permanent", self, "OnCompositeTabenSuccess_Permanent")
    g_ScriptEvent:RemoveListener("OnSetWardRobeTabenNumLimit_Permanent", self, "OnSetWardRobeTabenNumLimit_Permanent")
    g_ScriptEvent:RemoveListener("OnSetFashionHideSuccess_Permanent",self,"OnSetFashionHideSuccess_Permanent")
end

--仙凡切换 或其他sync appearance情况
function LuaAppearanceClosetFashionTabenSubview:SyncMainPlayerAppearancePropUpdate()
    self:LoadData()
end

function LuaAppearanceClosetFashionTabenSubview:OnSyncWardrobeProperty(reason)
    self:LoadData()
end

function LuaAppearanceClosetFashionTabenSubview:OnCompositeTabenSuccess_Permanent()
    self:LoadData()
end

function LuaAppearanceClosetFashionTabenSubview:OnSetWardRobeTabenNumLimit_Permanent(num)
    self:LoadData()
end

function LuaAppearanceClosetFashionTabenSubview:OnSetFashionHideSuccess_Permanent(pos,value)
    self:SetDefaultSelection()
    self:LoadData()
end
