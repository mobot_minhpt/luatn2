local AlignType = import "L10.UI.CTooltip+AlignType"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"

function Gas2Gac.SyncDiYuShiKongCopyInfo(rongxinHp, rongxinHpFull, faqiHp, faqiHpFull, escapedXiaoGuiCount)
    g_ScriptEvent:BroadcastInLua("SyncDiYuShiKongCopyInfo", rongxinHp, rongxinHpFull, faqiHp, faqiHpFull, escapedXiaoGuiCount)
end

function Gas2Gac.DiYuShiKongNoYiShiAlert()
    
    local msg = g_MessageMgr:FormatMessage("WuYi2020_DYSK_Caiji")
    MessageWndManager.ShowOKCancelMessage(msg,  DelegateFactory.Action(function ()
        CItemAccessListMgr.Inst:ShowItemAccessInfo(21000061, true, nil, AlignType.Right)
    end), nil, LocalString.GetString("获取"), nil, false)
end

function Gas2Gac.AskDiYuShiKongAddBloodToFaQi()
    local msg = g_MessageMgr:FormatMessage("WuYi2020_DYSK_shuxue")
    MessageWndManager.ShowConfirmMessage(msg, 10, false , DelegateFactory.Action(function ()
        Gac2Gas.DiYuShiKongAddBloodToFaQi()
    end), nil, false)
end


