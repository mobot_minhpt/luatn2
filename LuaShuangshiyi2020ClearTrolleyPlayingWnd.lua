local Vector3 = import "UnityEngine.Vector3"
local CUITexture=import "L10.UI.CUITexture"
local Ease = import "DG.Tweening.Ease"
local UILabel = import "UILabel"
local CMainCamera = import "L10.Engine.CMainCamera"
local CUIFx = import "L10.UI.CUIFx"
local CRenderScene = import "L10.Engine.Scene.CRenderScene"
local CUIFxPaths = import "L10.UI.CUIFxPaths"

CLuaShuangshiyi2020ClearTrolleyPlayingWnd = class()
RegistClassMember(CLuaShuangshiyi2020ClearTrolleyPlayingWnd,"m_Trolleys")
RegistClassMember(CLuaShuangshiyi2020ClearTrolleyPlayingWnd,"m_CreditCount")
RegistClassMember(CLuaShuangshiyi2020ClearTrolleyPlayingWnd,"m_ExpandButton")
RegistClassMember(CLuaShuangshiyi2020ClearTrolleyPlayingWnd,"m_MonsterData")
RegistClassMember(CLuaShuangshiyi2020ClearTrolleyPlayingWnd,"m_MonsterRound")
RegistClassMember(CLuaShuangshiyi2020ClearTrolleyPlayingWnd,"m_Fx")
RegistClassMember(CLuaShuangshiyi2020ClearTrolleyPlayingWnd,"m_FxPool")
RegistClassMember(CLuaShuangshiyi2020ClearTrolleyPlayingWnd,"m_BoomFx")
RegistClassMember(CLuaShuangshiyi2020ClearTrolleyPlayingWnd,"m_RewardIcon")
RegistClassMember(CLuaShuangshiyi2020ClearTrolleyPlayingWnd,"m_RewardItem")

function CLuaShuangshiyi2020ClearTrolleyPlayingWnd:Awake()
    self.m_Trolleys = {}
    self.m_Trolleys[1] = self.transform:Find("Anchor/Trolley/Red/CountSprite/Count"):GetComponent(typeof(UILabel))
    self.m_Trolleys[2] = self.transform:Find("Anchor/Trolley/Yellow/CountSprite/Count"):GetComponent(typeof(UILabel))
    self.m_Trolleys[3] = self.transform:Find("Anchor/Trolley/Blue/CountSprite/Count"):GetComponent(typeof(UILabel))
    self.m_CreditCount = self.transform:Find("Anchor/Credit/Count"):GetComponent(typeof(UILabel))
    self.m_MonsterRound = self.transform:Find("Anchor/Credit/MonsterRound"):GetComponent(typeof(UILabel))
    self.m_BoomFx = self.transform:Find("Anchor/Credit/BoomFx"):GetComponent(typeof(CUIFx))
    self.m_RewardIcon = self.transform:Find("Anchor/Credit/Reward"):GetComponent(typeof(CUITexture))
    self.m_Fx = self.transform:Find("Anchor/Fx").gameObject
    self.m_CreditCount.text = 0
    self.m_ExpandButton = self.transform:Find("Anchor/Tip/ExpandButton").gameObject
    self.m_MonsterData = {}
    self.m_FxPool = {}
    self.m_RewardItem = 1

    UIEventListener.Get(self.m_ExpandButton).onClick = DelegateFactory.VoidDelegate(
    function(p)
            self.m_ExpandButton.transform.localEulerAngles = Vector3(0, 0, 0)
            CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
    end)

    if CLuaShuangshiyi2020Mgr.m_Credit and CLuaShuangshiyi2020Mgr.m_MonsterCount then
        self:RefreshState(CLuaShuangshiyi2020Mgr.m_Credit, CLuaShuangshiyi2020Mgr.m_MonsterCount)
    end
end

function CLuaShuangshiyi2020ClearTrolleyPlayingWnd:OnEnable( )
    g_ScriptEvent:AddListener("ClearTrolleyPlayingWndUpdate", self, "RefreshState")
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:AddListener("ClientObjDie", self, "OnMonsterDie")
end

function CLuaShuangshiyi2020ClearTrolleyPlayingWnd:OnDisable( )
    g_ScriptEvent:RemoveListener("ClearTrolleyPlayingWndUpdate", self, "RefreshState")
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:RemoveListener("ClientObjDie", self, "OnMonsterDie")
    if CUIManager.IsLoaded(CUIResources.TopAndRightTipWnd) then
        CUIManager.CloseUI(CUIResources.TopAndRightTipWnd)
    end
end

function CLuaShuangshiyi2020ClearTrolleyPlayingWnd:OnHideTopAndRightTipWnd( )
    self.m_ExpandButton.transform.localEulerAngles = Vector3(0, 0, 180)
end

function CLuaShuangshiyi2020ClearTrolleyPlayingWnd:RefreshState(credit, monsterCount, monsterDeath, round)
    if self.m_CreditCount and self.m_Trolleys then
        for i = 0, 2 do
            self.m_Trolleys[i+1].text = SafeStringFormat3(LocalString.GetString("余%d"), monsterCount[i])
        end
    end

    if monsterDeath~=nil and monsterDeath.Count >=4 then
        local data = {}
        data.credit = credit
        data.x = monsterDeath[0]
        data.z = monsterDeath[1]
        data.id = monsterDeath[3]
        table.insert(self.m_MonsterData, data)
        --self:PlayFx(data)
    else
        self:UpdateRewardIcon(credit)
    end

    if round then
        self.m_MonsterRound.text = SafeStringFormat3(LocalString.GetString("第[FFEA56]%d/6[-]波"), round)
    end
end

function CLuaShuangshiyi2020ClearTrolleyPlayingWnd:UpdateRewardIcon(credit)
    local rewardDic = Double11_Setting.GetData().RewardInfo
    local itemID = 0
    local before = 0
    --local rewardLevel = 1
    -- 比credit小的最大分数
    CommonDefs.DictIterate(rewardDic, DelegateFactory.Action_object_object(function(key,val)
        if credit > key and before < key then
                before = key
        end
    end))
    local current = 999999999
    -- 比最大分数高的最小分数
	CommonDefs.DictIterate(rewardDic, DelegateFactory.Action_object_object(function(key,val)
		if key > before and key < current then
            current = key
            itemID = val
        end
    end))

	-- local colorList = {"[FFFFFF]", "[FA800A]", "[519FFF]", "[FF5050]", "[FF88FF]"}
    
    if self.m_RewardItem ~= itemID and itemID ~= 0 then
        local data = Item_Item.GetData(itemID)
        self.m_RewardIcon:LoadMaterial(data.Icon)
    end

    self.m_CreditCount.text = tostring(credit) -- colorList[rewardLevel].. tostring(credit)
end


function CLuaShuangshiyi2020ClearTrolleyPlayingWnd:OnMonsterDie(avg)
    for i=0,#self.m_MonsterData do
        local data = self.m_MonsterData[i]
        if data~=nil and data.id == avg[0] then
            table.remove(self.m_MonsterData, i)
            self:PlayFx(data)
        end
    end
end

function CLuaShuangshiyi2020ClearTrolleyPlayingWnd:PlayFx(data)
    local y = CRenderScene.Inst:SampleLogicHeight(data.x, data.z)
    local pos = CreateFromClass(Vector3, data.x, y, data.z)
    local screenPos = CMainCamera.Main:WorldToScreenPoint(pos)
    screenPos.z = 0
    local nguiWorldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(screenPos)
    --SoundManager.Inst:PlayOneShot("event:/L10/L10_common/drop01", Vector3.zero, nil, 0)

    -- CCommonUIFxWnd.Instance:PlayLineAni(nguiWorldPos, self.m_CreditCount.transform.position, 1, DelegateFactory.Action(function (p)
    --     self.m_CreditCount.text = data.credit
    -- end))
    self:PlayLineAni(nguiWorldPos, self.m_CreditCount.transform.position, 1, data.credit)
end

function CLuaShuangshiyi2020ClearTrolleyPlayingWnd:PlayLineAni(from, to, duration, credit)
    local lineFx = nil
    if #self.m_FxPool == 0 then
        lineFx = CommonDefs.Object_Instantiate(self.m_Fx):GetComponent(typeof(CUIFx))
        lineFx.gameObject.transform.parent = self.transform
    else
        lineFx = self.m_FxPool[#self.m_FxPool]
        table.remove(self.m_FxPool, #self.m_FxPool)
    end
    lineFx.gameObject:SetActive(true)

    lineFx.OnLoadFxFinish = DelegateFactory.Action(function()
        lineFx.gameObject.transform.position = from
        local tweener = CommonDefs.SetEase_Tweener(LuaTweenUtils.DOMove(lineFx.transform, to, 1, false), Ease.Linear)
        CommonDefs.OnComplete_Tweener(tweener, DelegateFactory.TweenCallback(
            function()
                self.m_BoomFx:DestroyFx()
                self.m_BoomFx:LoadFx(CUIFxPaths.PlayerLevelUpBoomFxPath)
                self:UpdateRewardIcon(credit)

                lineFx:DestroyFx()
                lineFx.gameObject:SetActive(false)
                table.insert(self.m_FxPool, lineFx)
            end))
    end)
    lineFx:LoadFx(CUIFxPaths.CommonYellowLineFxPath)
end
