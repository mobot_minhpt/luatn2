local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UISimpleTableView = import "L10.UI.UISimpleTableView"
local DefaultUISimpleTableViewDataSource = import "L10.UI.DefaultUISimpleTableViewDataSource"
local UITabBar = import "L10.UI.UITabBar"
local QnButton = import "L10.UI.QnButton"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local UILabel = import "UILabel"
local CCommonPlayerListMgr = import "L10.Game.CCommonPlayerListMgr"
local CCommonPlayerListItem = import "L10.UI.CCommonPlayerListItem"
local CIMMgr = import "L10.Game.CIMMgr"
local CCommonPlayerDisplayData = import "L10.Game.CCommonPlayerDisplayData"
local LuaGameObject = import "LuaGameObject"

EnumCommonPlayerListWnd2Tab ={
    Friend = 1,
	ZongPai = 2,
	Guild = 3
}

LuaCommonPlayerListWnd2 = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaCommonPlayerListWnd2, "Header", "Header", UITabBar)
RegistChildComponent(LuaCommonPlayerListWnd2, "BtnOneKey", "BtnOneKey", QnButton)
RegistChildComponent(LuaCommonPlayerListWnd2, "TableView", "TableView", UISimpleTableView)
RegistChildComponent(LuaCommonPlayerListWnd2, "TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaCommonPlayerListWnd2, "Item", "Item", GameObject)
RegistChildComponent(LuaCommonPlayerListWnd2, "DesLabel", "DesLabel", UILabel)

--@endregion RegistChildComponent end

function LuaCommonPlayerListWnd2:Awake()
    --@region EventBind: Dont Modify Manually!

	self.Header.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
	    self:OnHeaderTabChange(index)
	end)

	UIEventListener.Get(self.BtnOneKey.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBtnOneKeyClick(go)
	end)
	
    --@endregion EventBind end

	self.Item:SetActive(false)
end

function LuaCommonPlayerListWnd2:OnEnable()
	g_ScriptEvent:AddListener("OnCommonPlayerListDataUpdate",self,"OnGuildDataUpdate")
	g_ScriptEvent:AddListener("OnSectMemberInfoResult", self, "OnSectMemberInfoResult")  
end

function LuaCommonPlayerListWnd2:OnDisable()
	g_ScriptEvent:RemoveListener("OnCommonPlayerListDataUpdate",self,"OnGuildDataUpdate")
	g_ScriptEvent:RemoveListener("OnSectMemberInfoResult", self, "OnSectMemberInfoResult")  
end



function LuaCommonPlayerListWnd2:Init()
	self.m_index2tab = {}
	self.m_data = {}--储存数据，key为EnumCommonPlayerListWnd2Tab，value为一个数据table
	self.m_datasources = {}--刷新方法，key为EnumCommonPlayerListWnd2Tab，value为DefaultUISimpleTableViewDataSource
	self.m_tab = -1
	
	if LuaCommonPlayerListMgr.BtnData.Fbtn then
		table.insert(self.m_index2tab,EnumCommonPlayerListWnd2Tab.Friend)
	end

	if LuaCommonPlayerListMgr.BtnData.Zbtn then
		table.insert(self.m_index2tab,EnumCommonPlayerListWnd2Tab.ZongPai)
	end

	if LuaCommonPlayerListMgr.BtnData.Bbtn then
		table.insert(self.m_index2tab,EnumCommonPlayerListWnd2Tab.Guild)
	end

	for i=1,#self.m_index2tab do
		local tab = self.m_index2tab[i]
		self.m_datasources[tab] = DefaultUISimpleTableViewDataSource.Create(
			function()
				if self.m_data[tab] == nil then 
					return 0
				end
				return #self.m_data[tab]
			end,
			function (index)
				return self:CellForRowAtIndex(index)
			end)
	end
	if #self.m_index2tab < 3 then
		for i=#self.m_index2tab,3-1 do
			local btn = self.Header:GetTabGoByIndex(i)
			btn:SetActive(false)
		end
	end

	self.Header:ChangeTab(0,false)
end

function LuaCommonPlayerListWnd2:OnSectMemberInfoResult(list,isShowStatusBtn, JoinSettingStatus)
	local tab = EnumCommonPlayerListWnd2Tab.ZongPai
	self.m_data[tab] = {}

	local mems = list
	for _,info in pairs(mems) do
		local pid = info.PlayerId
		if pid ~= CClientMainPlayer.Inst.Id then
			local data = CCommonPlayerDisplayData(info.PlayerId,
				info.Name,
				info.Level,
				info.Class,
				info.Gender,
				0,
				true)
			table.insert(self.m_data[tab],data)
		end
	end

	self:Refresh(tab)
end

function LuaCommonPlayerListWnd2:OnGuildDataUpdate(args)--帮会数据更新
	local type = args[0]
	if type ~= 1 then return end

	local tab = EnumCommonPlayerListWnd2Tab.Guild
	self.m_data[tab] = {}

	local list = CCommonPlayerListMgr.Inst.allData
	CommonDefs.ListIterate(list, DelegateFactory.Action_object(function (value)
		table.insert(self.m_data[tab],value)
	end))

	self:Refresh(tab)
end

function LuaCommonPlayerListWnd2:FillFriendDatas()--好友数据更新
	local tab = EnumCommonPlayerListWnd2Tab.Friend
	self.m_data[tab] = {}
	
	local fds = CIMMgr.Inst.Friends
	CommonDefs.EnumerableIterate(fds, DelegateFactory.Action_object(function (pid)
		local info = CIMMgr.Inst:GetBasicInfo(pid)
		if info then
			if CIMMgr.Inst:IsOnline(info.ID) and CIMMgr.Inst:IsSameServerFriend(info.ID) then
				local data = CCommonPlayerDisplayData(info.ID,
					info.Name,
					info.Level,
					info.Class,
					info.Gender,
					info.Expression,
					true)
				table.insert(self.m_data[tab],data)
			end
		end
	end))

	-- 根据好友度排序
	table.sort(self.m_data[tab], function(a, b)
		local af = CIMMgr.Inst:GetFriendliness(a.playerId)
		local bf = CIMMgr.Inst:GetFriendliness(b.playerId)

		if af == bf then
			return a.playerId > b.playerId
		else
			return af > bf
		end
	end)
	
	self:Refresh(tab)
end

function LuaCommonPlayerListWnd2:Refresh(tab)
	if self.m_tab ~= tab then return end

	self.TableView:Clear()
	self.TableView.dataSource = self.m_datasources[tab]
	self.TableView:LoadDataFromTail()

	self:RefreshOneKeyBtn(tab)
end

function LuaCommonPlayerListWnd2:CellForRowAtIndex(index)--index：行数据索引，0开始
	local rindex = index + 1
	local datas = self.m_data[self.m_tab]
	if rindex <= 0 and rindex > #datas then
        return nil
    end

    local cellIdentifier = "item"
    local template = self.Item
    local data = datas[rindex]

    local cell = self.TableView:DequeueReusableCellWithIdentifier(cellIdentifier)
    if cell == nil then
        cell = self.TableView:AllocNewCellWithIdentifier(template, cellIdentifier)
    end

    local pitem = cell:GetComponent(typeof(CCommonPlayerListItem))
    pitem:Init(data,LuaCommonPlayerListMgr.BtnData.Pbtn.Name,true)
	pitem.OnButtonClickDelegate = DelegateFactory.Action_ulong_CCommonPlayerListItem(function(pid,item)
		LuaCommonPlayerListMgr.BtnData.Pbtn.Func(pid)
	end)
    return cell
end

function LuaCommonPlayerListWnd2:RefreshOneKeyBtn(tab)
	local showbtn = false
	local reason = ""
	local btnname = ""
	local onekeybtnlabel = LuaGameObject.GetChildNoGC(self.BtnOneKey.transform,"Label").label
	if tab == EnumCommonPlayerListWnd2Tab.Friend then
		showbtn = self.m_data[tab] and #self.m_data[tab] > 0
		if not showbtn then 
			reason = LocalString.GetString("暂无在线好友")
		else
			btnname = LuaCommonPlayerListMgr.BtnData.Fbtn.Name
		end
	elseif tab == EnumCommonPlayerListWnd2Tab.ZongPai then
		showbtn = CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.SectId > 0
		if showbtn then
			btnname = LuaCommonPlayerListMgr.BtnData.Zbtn.Name
		else
			reason = LocalString.GetString("暂无宗派")
		end
	elseif tab == EnumCommonPlayerListWnd2Tab.Guild then
		showbtn = CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.GuildId > 0
		if showbtn then
			btnname = LuaCommonPlayerListMgr.BtnData.Bbtn.Name
		else
			reason = LocalString.GetString("暂无帮会")
		end
	end
	onekeybtnlabel.text = btnname
	self.DesLabel.text = reason
	self.BtnOneKey.gameObject:SetActive(showbtn)
end

--@region UIEvent

function LuaCommonPlayerListWnd2:OnHeaderTabChange(index)--index：页签索引，0开始
	local rindex = index + 1
	local tab = self.m_index2tab[rindex]
	if self.m_tab == tab then return end
	self.m_tab = tab
	self.TableView:Clear()
	self:RefreshOneKeyBtn(tab)
	if self.m_data[tab] == nil then
		if tab == EnumCommonPlayerListWnd2Tab.Friend then
			self:FillFriendDatas()
		elseif tab == EnumCommonPlayerListWnd2Tab.ZongPai then --帮会
			if CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.SectId > 0 then
				Gac2Gas.QuerySectInfo(CClientMainPlayer.Inst.BasicProp.SectId, "member", 0)
			else
				self:Refresh(tab)
			end
		elseif tab == EnumCommonPlayerListWnd2Tab.Guild then
			if CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.GuildId > 0 then
				L10.Game.Gac2Gas.QueryOnlineMembersInMyGuild(1)
			else
				self:Refresh(tab)
			end
		end
	else
		self:Refresh(tab)
	end
end

function LuaCommonPlayerListWnd2:OnBtnOneKeyClick(go)
	if self.m_tab == EnumCommonPlayerListWnd2Tab.Friend then
		if LuaCommonPlayerListMgr.BtnData.Fbtn and LuaCommonPlayerListMgr.BtnData.Fbtn.Func then
			LuaCommonPlayerListMgr.BtnData.Fbtn.Func()
		end
	elseif self.m_tab == EnumCommonPlayerListWnd2Tab.ZongPai then
		LuaCommonPlayerListMgr.BtnData.Zbtn.Func()
	elseif self.m_tab == EnumCommonPlayerListWnd2Tab.Guild then
		LuaCommonPlayerListMgr.BtnData.Bbtn.Func()
	end
end

--@endregion UIEvent

