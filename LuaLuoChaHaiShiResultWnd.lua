local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"

LuaLuoChaHaiShiResultWnd = class()

LuaLuoChaHaiShiResultWnd.s_Type = 0 -- 0: success, 1: fail allDie, 2: fail timeOut
LuaLuoChaHaiShiResultWnd.s_Rewards = {}

RegistClassMember(LuaLuoChaHaiShiResultWnd, "Success")
RegistClassMember(LuaLuoChaHaiShiResultWnd, "Fail")
RegistClassMember(LuaLuoChaHaiShiResultWnd, "Reward")
RegistClassMember(LuaLuoChaHaiShiResultWnd, "FailLabel")

function LuaLuoChaHaiShiResultWnd:Awake()
    self.Success = self.transform:Find("Anchor/Success").gameObject
    self.Fail = self.transform:Find("Anchor/Fail").gameObject
    self.Reward = self.transform:Find("Anchor/Info/Reward")
    self.FailLabel = self.transform:Find("Anchor/Info/FailLabel"):GetComponent(typeof(UILabel))
end

function LuaLuoChaHaiShiResultWnd:Init()
    if LuaLuoChaHaiShiResultWnd.s_Type == 0 then
        self.Success:SetActive(true)
        self.Fail:SetActive(false)
        self.Reward.gameObject:SetActive(true)
        self.FailLabel.gameObject:SetActive(false)
        local grid = self.Reward:Find("Grid"):GetComponent(typeof(UIGrid))
        for i = 0, grid.transform.childCount - 1 do
            grid.transform:GetChild(i).gameObject:SetActive(false)
        end
        for i, id in ipairs(LuaLuoChaHaiShiResultWnd.s_Rewards) do
            local go = grid.transform:GetChild(i-1).gameObject
            go:SetActive(true)
            local texture = go.transform:Find("Item"):GetComponent(typeof(CUITexture))
            local ItemData = Item_Item.GetData(id)
            if ItemData then 
                texture:LoadMaterial(ItemData.Icon) 
            end
            UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function (_)
                CItemInfoMgr.ShowLinkItemTemplateInfo(id, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
            end)
        end
        grid:Reposition()
    else
        local failHint = LuoCha2023_Setting.GetData().FailHint
        self.Success:SetActive(false)
        self.Fail:SetActive(true)
        self.Reward.gameObject:SetActive(false)
        self.FailLabel.gameObject:SetActive(true)
        self.FailLabel.text = failHint[LuaLuoChaHaiShiResultWnd.s_Type - 1]
    end
end
