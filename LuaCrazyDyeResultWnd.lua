
local DelegateFactory = import "DelegateFactory"
local UIEventListener = import "UIEventListener"
local UICommonDef = import "L10.UI.CUICommonDef"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local EShareType = import "L10.UI.EShareType"
local UILabel = import "UILabel"
local CButton = import "L10.UI.CButton"
local CUITexture=import "L10.UI.CUITexture"
local QnTableView = import "L10.UI.QnTableView"
local CommonDefs = import "L10.Game.CommonDefs"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local Vector3 = import "UnityEngine.Vector3"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"

LuaCrazyDyeResultWnd = class()

RegistChildComponent(LuaCrazyDyeResultWnd,  "ShareBtn",     GameObject)
RegistChildComponent(LuaCrazyDyeResultWnd,  "RewardBtn",    CButton)
RegistChildComponent(LuaCrazyDyeResultWnd,  "QnTableView",  QnTableView)
RegistChildComponent(LuaCrazyDyeResultWnd,  "LbValue1",     UILabel)
RegistChildComponent(LuaCrazyDyeResultWnd,  "LbValue2",     UILabel)
RegistChildComponent(LuaCrazyDyeResultWnd,  "LbValue3",     UILabel)
RegistChildComponent(LuaCrazyDyeResultWnd,  "LbValue4",     UILabel)
RegistChildComponent(LuaCrazyDyeResultWnd,  "BtnLabel",     UILabel)

RegistClassMember(LuaCrazyDyeResultWnd,"CountDownTick")
RegistClassMember(LuaCrazyDyeResultWnd,"GetRewardTime")

LuaCrazyDyeResultWnd.rankData = {}
LuaCrazyDyeResultWnd.personalData = {}
LuaCrazyDyeResultWnd.rewardData = {}
LuaCrazyDyeResultWnd.bGetReward = false

function LuaCrazyDyeResultWnd:Awake()
    self:InitRankBtn()
    self:InitRewardItems()
    self:InitPersonalInfo()
    UIEventListener.Get(self.RewardBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(p)
        self:GetReward()
        self:UpdateBtn()
    end)
    UIEventListener.Get(self.ShareBtn).onClick = DelegateFactory.VoidDelegate(function(p)
        -- 截图分享
        UICommonDef.CaptureScreen(
            "screenshot",
            true,
            false,
            nil,
            DelegateFactory.Action_string_bytes(
                function(fullPath, jpgBytes)
                    ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, fullPath)
                end
            ),
            false
        )
    end)

    self.GetRewardTime = 10
    self:UpdateBtn()
    if not LuaCrazyDyeResultWnd.bGetReward then
        self.CountDownTick = RegisterTickWithDuration(function ()
            self.GetRewardTime = self.GetRewardTime - 1
            if self.GetRewardTime <= 0 and not LuaCrazyDyeResultWnd.bGetReward then
                self:GetReward()
            end
            self:UpdateBtn()
        end, 1000, self.GetRewardTime * 1000)
    end
end

function LuaCrazyDyeResultWnd:GetReward()
    LuaCrazyDyeResultWnd.bGetReward = true
    self.RewardBtn.Enabled = false
    Gac2Gas.RequestGetDyePlayReward()
end

function LuaCrazyDyeResultWnd:UpdateBtn()
    if LuaCrazyDyeResultWnd.bGetReward then
        self.BtnLabel.text = LocalString.GetString("已领取")
        self.RewardBtn.Enabled = false
    else
        self.BtnLabel.text = SafeStringFormat3(LocalString.GetString("领取奖励(%s)"),self.GetRewardTime)
        self.RewardBtn.Enabled = true
    end
end


function LuaCrazyDyeResultWnd:OnDisable()
    LuaCrazyDyeResultWnd.rankData = nil
    LuaCrazyDyeResultWnd.personalData = nil
    LuaCrazyDyeResultWnd.rewardData = nil
    UnRegisterTick(self.CountDownTick)
    if not LuaCrazyDyeResultWnd.bGetReward then
        self:GetReward()
    end
end

function LuaCrazyDyeResultWnd:InitRankBtn()
    for i = 1 , #LuaCrazyDyeResultWnd.rankData do
        local trans = self.transform:Find("Content/Rank/Rank"..i)
        trans:Find("LbRank/LbScore"):GetComponent(typeof(UILabel)).text = LuaCrazyDyeResultWnd.rankData[i].score
        local levelLabel = trans:Find("LbLv"):GetComponent(typeof(UILabel))
        levelLabel.text = "LV."..CUICommonDef.GetColoredLevelString(LuaCrazyDyeResultWnd.rankData[i].bXianShen, LuaCrazyDyeResultWnd.rankData[i].level, levelLabel.color)
        trans:Find("LbLv/LbName"):GetComponent(typeof(UILabel)).text = LuaCrazyDyeResultWnd.rankData[i].name
        trans:Find("TxIcon"):GetComponent(typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(LuaCrazyDyeResultWnd.rankData[i].cls,LuaCrazyDyeResultWnd.rankData[i].gender,LuaCrazyDyeResultWnd.rankData[i].expression), false)
        UIEventListener.Get(trans:Find("TxIcon").gameObject).onClick = DelegateFactory.VoidDelegate(function(p)
            if LuaCrazyDyeResultWnd.rankData[i].pid > 0 then
                CPlayerInfoMgr.ShowPlayerPopupMenu(LuaCrazyDyeResultWnd.rankData[i].pid, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
            end
        end)
    end
end

function LuaCrazyDyeResultWnd:InitPersonalInfo()
    self.LbValue1.text = LuaCrazyDyeResultWnd.personalData.score
    self.LbValue2.text = LuaCrazyDyeResultWnd.personalData.rankPos.."/"..LuaCrazyDyeResultWnd.personalData.rankLen
    self.LbValue3.text = LuaCrazyDyeResultWnd.personalData.killCount
    self.LbValue4.text = math.floor(LuaCrazyDyeResultWnd.personalData.dps)
end

function LuaCrazyDyeResultWnd:InitRewardItems()
    self.QnTableView.m_DataSource=DefaultTableViewDataSource.Create(function()
        return #LuaCrazyDyeResultWnd.rewardData
    end, function(item, index)
        self:InitRewardItem(item, index)
    end)

    self.QnTableView:ReloadData(true,true)
end

function LuaCrazyDyeResultWnd:InitRewardItem(item,index)
    local ItemData = Item_Item.GetData(LuaCrazyDyeResultWnd.rewardData[index+1].itemId)

    local itemIcon = item.transform:Find("ItemIcon"):GetComponent(typeof(CUITexture))
    itemIcon:LoadMaterial(ItemData.Icon)
    local itemCount = item.transform:Find("ItemIcon/ItemCount"):GetComponent(typeof(UILabel))
    itemCount.text = LuaCrazyDyeResultWnd.rewardData[index+1].count
    --item.transform:Find("ItemIcon/Quality"):GetComponent(typeof(CUITexture))spriteName = CUICommonDef.GetItemCellBorder(1)
    UIEventListener.Get(itemIcon.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
        CItemInfoMgr.ShowLinkItemTemplateInfo(LuaCrazyDyeResultWnd.rewardData[index+1].itemId, false, nil, AlignType.Right, 0, 0, 0, 0)
    end)
end
