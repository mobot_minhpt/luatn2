-- Auto Generated!!
local CQueQiaoEventWnd = import "L10.UI.CQueQiaoEventWnd"
local CQueQiaoMgr = import "L10.Game.CQueQiaoMgr"
CQueQiaoEventWnd.m_Init_CS2LuaHook = function (this) 
    if System.String.IsNullOrEmpty(CQueQiaoMgr.Inst.eventName) or CQueQiaoMgr.Inst.eventDuration < 1 then
        this:Close()
        return
    end
    this.confirmBtn.Text = CQueQiaoMgr.Inst.eventName
    this.aliveDuration = CQueQiaoMgr.Inst.eventDuration
end
