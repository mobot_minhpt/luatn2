local CUIRestrictScrollView  = import "L10.UI.CUIRestrictScrollView"
local CButton                = import "L10.UI.CButton"
local CQnReturnAwardTemplate = import "L10.UI.CQnReturnAwardTemplate"
local Item_Item              = import "L10.Game.Item_Item"

LuaArenaPassportOpenWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaArenaPassportOpenWnd, "typeTable", "Table", Transform)
RegistChildComponent(LuaArenaPassportOpenWnd, "typeName", "Name", UILabel)
RegistChildComponent(LuaArenaPassportOpenWnd, "infoTable", "InfoTable", UITable)
RegistChildComponent(LuaArenaPassportOpenWnd, "descTable", "DescTable", UITable)
RegistChildComponent(LuaArenaPassportOpenWnd, "awardTable", "AwardTable", UITable)
RegistChildComponent(LuaArenaPassportOpenWnd, "descTemplate", "DescTemplate", GameObject)
RegistChildComponent(LuaArenaPassportOpenWnd, "awardTemplate", "AwardTemplate", GameObject)
RegistChildComponent(LuaArenaPassportOpenWnd, "openButton", "OpenButton", GameObject)
RegistChildComponent(LuaArenaPassportOpenWnd, "scrollView", "ScrollView", CUIRestrictScrollView)

--@endregion RegistChildComponent end

RegistClassMember(LuaArenaPassportOpenWnd, "selectId")

function LuaArenaPassportOpenWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.openButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOpenButtonClick()
	end)

    --@endregion EventBind end

	self.descTemplate:SetActive(false)
	self.awardTemplate:SetActive(false)
end

function LuaArenaPassportOpenWnd:Init()
	for i = 1, 3 do
		local child = self.typeTable:GetChild(i - 1)
		local data = Arena_PassportType.GetData(i)
		child:Find("Name"):GetComponent(typeof(UILabel)).text = data.Name
		if not System.String.IsNullOrEmpty(data.Discount) then
			child:Find("Discount/Label"):GetComponent(typeof(UILabel)).text = data.Discount
		end
		if data.NeedJade > 0 then
			child:Find("Jade"):GetComponent(typeof(UILabel)).text = data.NeedJade
		end
		child:Find("Icon"):GetComponent(typeof(CUITexture)):LoadMaterial(data.Icon)

		UIEventListener.Get(child.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
			self:OnTypeClick(i)
		end)
	end
	self.selectId = 0
	self:OnTypeClick(3)
end

function LuaArenaPassportOpenWnd:AddChild(parent, template, num)
    local childCount = parent.transform.childCount
    if childCount < num then
        for i = childCount + 1, num do
            NGUITools.AddChild(parent, template)
        end
    end

    childCount = parent.transform.childCount
    for i = 0, childCount - 1 do
        parent.transform:GetChild(i).gameObject:SetActive(i < num)
    end
end

--@region UIEvent

function LuaArenaPassportOpenWnd:OnOpenButtonClick()
	local message = ""
	if self.selectId == 1 then
		message = g_MessageMgr:FormatMessage("ARENA_OPEN_LOW_LEVEL_PASSPORT_CONFIRM")
	else
		local jade = Arena_PassportType.GetData(self.selectId).NeedJade
		message = g_MessageMgr:FormatMessage("ARENA_OPEN_HIGH_LEVEL_PASSPORT_CONFIRM", jade)
	end
	g_MessageMgr:ShowOkCancelMessage(message, function ()
		Gac2Gas.RequestBuyArenaPassport(self.selectId)
		CUIManager.CloseUI(CLuaUIResources.ArenaPassportOpenWnd)
	end, nil, nil, nil, false)
end

function LuaArenaPassportOpenWnd:OnTypeClick(i)
	if self.selectId == i then return end

	if self.selectId > 0 then
		self.typeTable:GetChild(self.selectId - 1):GetComponent(typeof(CButton)).Selected = false
	end

	self.typeTable:GetChild(i - 1):GetComponent(typeof(CButton)).Selected = true
	self.selectId = i

	local data = Arena_PassportType.GetData(i)
	self.typeName.text = data.Name
	self:AddChild(self.descTable.gameObject, self.descTemplate, 2)
	local label1 = self.descTable.transform:GetChild(0):Find("Label"):GetComponent(typeof(UILabel))
	local label2 = self.descTable.transform:GetChild(1):Find("Label"):GetComponent(typeof(UILabel))
	if i == 1 then
		label1.text = LocalString.GetString("开启战令")
		label2.text = LocalString.GetString("获得1级战令奖励：")
	else
		local level = data.LevelUp
		label1.text = SafeStringFormat3(LocalString.GetString("开启战令并获得[FFFF00]战令点%d[-]，可迅速升级战令等级至[FFFF00]%d级[-]"), self:GetExp(level), level)
		label2.text = LocalString.GetString("直接获得丰厚奖励：")
	end
	self.descTable:Reposition()

	local awards = LuaArenaMgr:GetPassportItemRewardsInLevelRange(1, data.LevelUp)
	self:AddChild(self.awardTable.gameObject, self.awardTemplate, #awards)
	for j = 1, #awards do
		local awardData = awards[j]
		local child = self.awardTable.transform:GetChild(j - 1)
		local returnAward = child:GetComponent(typeof(CQnReturnAwardTemplate))
		local itemData = Item_Item.GetData(awardData.itemId)
		returnAward:Init(itemData, awardData.count)
		child:Find("Bind").gameObject:SetActive(itemData.Lock == 1)
	end
	self.awardTable:Reposition()
	self.infoTable:Reposition()
	self.scrollView:ResetPosition()
end

--@endregion UIEvent

function LuaArenaPassportOpenWnd:GetExp(level)
	local exp = 0
	for i = 1, level do
		exp = exp + Arena_PassportLevelReward.GetData(level).NeedExp
	end
	return exp
end

