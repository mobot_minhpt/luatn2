local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local UISprite = import "UISprite"
local LocalString = import "LocalString"
local CUITexture = import "L10.UI.CUITexture"
local Extensions = import "Extensions"
local Vector3 = import "Vector3"
local MessageWndManager = import "L10.UI.MessageWndManager"
local Constants = import "L10.Game.Constants"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local CChatLinkMgr = import "CChatLinkMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CChatMgr = import "L10.Game.CChatMgr"

LuaDetailWish = class()
RegistChildComponent(LuaDetailWish,"BackButton", GameObject)
RegistChildComponent(LuaDetailWish,"ScrollView", UIScrollView)
RegistChildComponent(LuaDetailWish,"Table", UITable)
RegistChildComponent(LuaDetailWish,"messagebar", GameObject)
RegistChildComponent(LuaDetailWish,"containbg", GameObject)
RegistChildComponent(LuaDetailWish,"SendStatusButton", GameObject)
RegistChildComponent(LuaDetailWish,"OpButton1", GameObject)
RegistChildComponent(LuaDetailWish,"InfoItem", GameObject)

RegistClassMember(LuaDetailWish, "InfoNode")
RegistClassMember(LuaDetailWish, "InfoData")
RegistClassMember(LuaDetailWish, "TimeTick")

function LuaDetailWish:Back()
	--CUIManager.CloseUI(CLuaUIResources.)
  self.gameObject:SetActive(false)

end

function LuaDetailWish:RefreshPanel(wishId)
  LuaPersonalSpaceMgrReal.GetSingleDetailWishInfo(wishId,function(tData)
    local data = {}
    data[0] = tData.data
    self:Init(data)
  end)
end

function LuaDetailWish:OnEnable()
	g_ScriptEvent:AddListener("WishAddHelpUpdate", self, "RefreshPanel")

  if self.TimeTick then
    UnRegisterTick(self.TimeTick)
    self.TimeTick = nil
  end
  self.TimeTick = RegisterTick(function ()
    self:TimeCountDown()
  end, 1000)
end

function LuaDetailWish:OnDisable()
	g_ScriptEvent:RemoveListener("WishAddHelpUpdate", self, "RefreshPanel")

  if self.TimeTick then
    UnRegisterTick(self.TimeTick)
    self.TimeTick = nil
  end
end

function LuaDetailWish:TimeCountDown()
  local node = self.InfoNode
  local data = self.InfoData
  if node then
    if data.currentProgress >= data.totalProgress then
      node.transform:Find('restTime'):GetComponent(typeof(UILabel)).text = LocalString.GetString('已实现')
    else
      local leftTime = data.endTime/1000 - CServerTimeMgr.Inst.timeStamp
      local restDay = math.floor(leftTime/3600/24)

      if leftTime <= 0 then
        node.transform:Find('restTime'):GetComponent(typeof(UILabel)).text = LocalString.GetString("已过期")
      else
        if restDay > 0 then
          --node.transform:Find('restTime'):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString('剩余实现时间 [c][E8E918]%s天[-]'),restDay)
          node.transform:Find('restTime'):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString('剩余实现时间 [c][E8E918]%s[-]'),self:ParseTime(leftTime))
        else
          node.transform:Find('restTime'):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString('剩余实现时间 [c][FF0000]%s[-]'),self:ParseTime(leftTime))
        end
      end
    end
  end
end

--@desc 处理修改时间格式
function LuaDetailWish:ParseTime(leftTime)
    if leftTime <= 0 then
        return LocalString.GetString("已过期")
    end

    if leftTime < 24 * 3600 then
        local hour = math.floor(leftTime / 3600)
        local minute = math.floor(leftTime % 3600 / 60)
        local sec = leftTime % 60
        return SafeStringFormat3("%d:%d:%d", hour, minute, sec)
    else
        --local days = math.floor(leftTime / (24 * 3600))
        --return SafeStringFormat3(LocalString.GetString("%d天"), days)
        local days = math.floor(leftTime / (24 * 3600))
        local hour = math.floor((leftTime % (24 * 3600)) / 3600)
        local minute = math.floor(leftTime % 3600 / 60)
        local sec = leftTime % 60
        return SafeStringFormat3(LocalString.GetString("%d天 %d:%d:%d"), days,hour,minute,sec)
    end
end

function LuaDetailWish:InitWishItemNode(node,data)
  local itemData = CItemMgr.Inst:GetItemTemplate(data.templateId)
  if not itemData then
    node:SetActive(false)
    return
  end
  local itemNum = data.num
  node.transform:Find('num'):GetComponent(typeof(UILabel)).text = itemNum
  node.transform:Find('pic'):GetComponent(typeof(CUITexture)):LoadMaterial(itemData.Icon)
  local picBtn = node.transform:Find('pic').gameObject
	local onPicClick = function(go)
    CItemInfoMgr.ShowLinkItemTemplateInfo(data.templateId, false, nil, AlignType.Default, 0, 0, 0, 0)
	end
	CommonDefs.AddOnClickListener(picBtn,DelegateFactory.Action_GameObject(onPicClick),false)
  node.transform:Find('name'):GetComponent(typeof(UILabel)).text = itemData.Name

  self.InfoNode = node
  self.InfoData = data
  self:TimeCountDown()
--  self.TimeTick = RegisterTick(function ()
--    self:TimeCountDown()
--  end, 1000)

  local percent = math.floor(data.currentProgress * 1000 / data.totalProgress) / 10
  local maxLength = 717
  local nowLength = maxLength * data.currentProgress / data.totalProgress
  if percent == 0 then
    node.transform:Find('sliderBg/slider').gameObject:SetActive(false)
  else
    node.transform:Find('sliderBg/slider').gameObject:SetActive(true)
    node.transform:Find('sliderBg/slider'):GetComponent(typeof(UISprite)).width = nowLength
  end
  node.transform:Find('desc'):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString('进度 [c][E8E918]%s%%[-]'),percent)
end

function LuaDetailWish:InitItemNode(data)
  local node = self.InfoItem
  node.transform:Find('icon'):GetComponent(typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(data.clazz, data.gender, -1),false)
  local iconBtn = node.transform:Find('icon/button').gameObject
	local onIconClick = function(go)
    CPlayerInfoMgr.ShowPlayerPopupMenu(data.roleId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), CPlayerInfoMgr.AlignType.Default)
	end
	CommonDefs.AddOnClickListener(iconBtn,DelegateFactory.Action_GameObject(onIconClick),false)

  node.transform:Find('name'):GetComponent(typeof(UILabel)).text = data.roleName
  local xianFanStatus = data.xianFanStatus or 0
  node.transform:Find('lv'):GetComponent(typeof(UILabel)).text = xianFanStatus > 0 and SafeStringFormat3("[c][%s]lv.%d[-][/c]", Constants.ColorOfFeiSheng, data.grade) or SafeStringFormat3("lv.%d", data.grade)
  local createTime = CServerTimeMgr.ConvertTimeStampToZone8Time(data.startTime/1000)
  node.transform:Find('time'):GetComponent(typeof(UILabel)).text = SafeStringFormat3('%s-%s-%s',createTime.Year,createTime.Month,createTime.Day)
  local showText = CChatMgr.Inst:FilterYangYangEmotion(data.text)
  node.transform:Find('status'):GetComponent(typeof(UILabel)).text = CChatLinkMgr.TranslateToNGUIText(showText, false)

  local textHeight = node.transform:Find('status'):GetComponent(typeof(UILabel)).height
  local timeNode = node.transform:Find('time').gameObject
  local totalHeight = 0
  if data.templateId == 0 then
    node.transform:Find('addNode').gameObject:SetActive(false)
    timeNode.transform.localPosition = Vector3(timeNode.transform.localPosition.x,-31-textHeight,timeNode.transform.localPosition.z)
    totalHeight = 102 + textHeight
  else
    local itemNode = node.transform:Find('addNode').gameObject
    itemNode:SetActive(true)
    self:InitWishItemNode(itemNode,data)
    itemNode.transform.localPosition = Vector3(itemNode.transform.localPosition.x,-48-textHeight,itemNode.transform.localPosition.z)
    timeNode.transform.localPosition = Vector3(timeNode.transform.localPosition.x,-133-textHeight,timeNode.transform.localPosition.z)
    totalHeight = 198 + textHeight
  end

  local containHeight = 666 - totalHeight
  self.containbg:GetComponent(typeof(UISprite)).height = containHeight
  CommonDefs.GetComponent_Component_Type(self.ScrollView.transform, typeof(UIPanel)):ResetAndUpdateAnchors()
end

function LuaDetailWish:DeleteItemComment(wishId,helpId)
  local backFunction = function(data)
    g_ScriptEvent:BroadcastInLua("WishAddHelpUpdate",wishId)
  end
  MessageWndManager.ShowOKCancelMessage(LocalString.GetString("是否删除这条助力的留言内容？"), DelegateFactory.Action(function ()
    LuaPersonalSpaceMgrReal.DeleteDetailWishComment(wishId,helpId,backFunction)
  end), nil, LocalString.GetString("删除"), nil, false)
end

function LuaDetailWish:InitWishComment(data)
  local commentsTable = data.helps
  local templateNode = self.messagebar
  templateNode:SetActive(false)

  Extensions.RemoveAllChildren(self.Table.transform)

  for i,v in ipairs(commentsTable) do
    local node = NGUITools.AddChild(self.Table.gameObject,templateNode)
    node:SetActive(true)
    if not v.roleName or v.roleName == '' then
      v.roleName = v.roleId
    end
    local deleteBtn = node.transform:Find('deletebutton').gameObject
    deleteBtn:SetActive(false)

    local textLabel = node.transform:Find('text'):GetComponent(typeof(UILabel))
    if v.progress and v.progress > 0 then --zhuli wish
      local showText = CChatLinkMgr.TranslateToNGUIText(CPersonalSpaceMgr.GeneratePlayerName(v.roleId, v.roleName))
      if v.text and v.text ~= '' then
        local commentText = CChatMgr.Inst:FilterYangYangEmotion(v.text)
        commentText = CChatLinkMgr.TranslateToNGUIText(commentText, false)
        textLabel.text = showText .. SafeStringFormat3(LocalString.GetString('助力[c][E8E918]%s[-]灵玉，并说：'),v.progress) .. commentText
        if v.roleId == CClientMainPlayer.Inst.Id or data.roleId == CClientMainPlayer.Inst.Id then
          deleteBtn:SetActive(true)
          local helpId = v.id
          local wishId = v.wishId
          local onDeleteClick = function(go)
            self:DeleteItemComment(wishId,helpId)
          end
          CommonDefs.AddOnClickListener(deleteBtn,DelegateFactory.Action_GameObject(onDeleteClick),false)
        end
      else
        textLabel.text = showText .. SafeStringFormat3(LocalString.GetString('助力[c][E8E918]%s[-]灵玉'),v.progress)
      end
    elseif v.text and v.text ~= '' then -- normal wish
      local nameText = CChatLinkMgr.TranslateToNGUIText(CPersonalSpaceMgr.GeneratePlayerName(v.roleId, v.roleName))
      local showText = CChatMgr.Inst:FilterYangYangEmotion(v.text)
      showText = CChatLinkMgr.TranslateToNGUIText(showText, false)
      textLabel.text = nameText .. showText
      if v.roleId == CClientMainPlayer.Inst.Id then
        deleteBtn:SetActive(true)
        local helpId = v.id
        local wishId = v.wishId
        local onDeleteClick = function(go)
          self:DeleteItemComment(wishId,helpId)
        end
        CommonDefs.AddOnClickListener(deleteBtn,DelegateFactory.Action_GameObject(onDeleteClick),false)
      end
    end

    UIEventListener.Get(textLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
      local index = textLabel:GetCharacterIndexAtPosition(UICamera.lastWorldPosition, true)
      if index == 0 then
        index = - 1
      end

      local url = textLabel:GetUrlAtCharacterIndex(index)
      if url ~= nil and CPersonalSpaceMgr.ProcessLinkClick(url) then

        return
      end
    end)

  end

  self.Table:Reposition()
  self.ScrollView:ResetPosition()

--  local commentIsFull = data.helpIsFull
--  if commentIsFull then
--
--  else
--
--  end
end

function LuaDetailWish:CancelWish(data,leftTime)
  if data.templateId > 0 then
    if data.currentProgress >= data.totalProgress or data.currentProgress == 0 or leftTime < 0 then
      MessageWndManager.ShowOKCancelMessage(LocalString.GetString("遗忘心愿后将无法再看到该心愿，确定要这么做吗？"), DelegateFactory.Action(function ()
        Gac2Gas.Wish_DelWish(data.wishId)
        self:Back()
      end), nil, LocalString.GetString("删除"), nil, false)
    else
      g_MessageMgr:ShowMessage('WISH_DELWISH_EXISTS_HELP')
    end
  else
    MessageWndManager.ShowOKCancelMessage(LocalString.GetString("遗忘心愿后将无法再看到该心愿，确定要这么做吗？"), DelegateFactory.Action(function ()
      Gac2Gas.Wish_DelWish(data.wishId)
      self:Back()
    end), nil, LocalString.GetString("删除"), nil, false)
  end
end

function LuaDetailWish:ShowSelfFillWnd(data)
  LuaWishMgr.CurrentWishData = data
  CUIManager.ShowUI(CLuaUIResources.WishSelfFillWnd)
end

function LuaDetailWish:InitSelfOp(data)
  local leftTime = data.endTime/1000 - CServerTimeMgr.Inst.timeStamp
  local restDay = math.floor(leftTime/3600/24)

  if data.templateId > 0 and leftTime > 0 and restDay <= 2 then
    if data.currentProgress >= data.totalProgress then
      self.SendStatusButton:SetActive(false)
    else
      self.SendStatusButton:SetActive(true)
      self.SendStatusButton.transform:Find('Label'):GetComponent(typeof(UILabel)).text = LocalString.GetString('实现心愿')
      local onFillClick = function(go)
        self:ShowSelfFillWnd(data)
      end
      CommonDefs.AddOnClickListener(self.SendStatusButton,DelegateFactory.Action_GameObject(onFillClick),false)
    end
  else
    self.SendStatusButton:SetActive(false)
  end

  self.OpButton1:SetActive(true)
  self.OpButton1.transform:Find('Label'):GetComponent(typeof(UILabel)).text = LocalString.GetString('遗忘心愿')
  local onCancelClick = function(go)
    self:CancelWish(data,leftTime)
  end
  CommonDefs.AddOnClickListener(self.OpButton1,DelegateFactory.Action_GameObject(onCancelClick),false)
end

function LuaDetailWish:OpenZhuliWnd(data)
  --LuaWishMgr.CurrentWishData = data
  --CUIManager.ShowUI(CLuaUIResources.WishItemWnd)
  LuaPersonalSpaceMgrReal.QueryZhuliLimitAndOpenWnd(data)
end

function LuaDetailWish:OpenZhufuWnd(data)
  LuaWishMgr.CurrentWishData = data
  CUIManager.ShowUI(CLuaUIResources.WishWordWnd)
end

function LuaDetailWish:InitFriendOp(data)
  self.OpButton1:SetActive(false)

  local leftTime = data.endTime/1000 - CServerTimeMgr.Inst.timeStamp

  if leftTime <= 0 then
    self.SendStatusButton:SetActive(false)
  else
    local commentIsFull = data.helpIsFull
    if commentIsFull then
      self.SendStatusButton:SetActive(false)
    else
      self.SendStatusButton:SetActive(true)
      if data.templateId > 0 then
        if data.currentProgress >= data.totalProgress then
          self.SendStatusButton:SetActive(false)
        else
          self.SendStatusButton.transform:Find('Label'):GetComponent(typeof(UILabel)).text = LocalString.GetString('助力')
          local onClick = function(go)
            self:OpenZhuliWnd(data)
          end
          CommonDefs.AddOnClickListener(self.SendStatusButton,DelegateFactory.Action_GameObject(onClick),false)
        end
      else
        self.SendStatusButton.transform:Find('Label'):GetComponent(typeof(UILabel)).text = LocalString.GetString('祝福')
        local onClick = function(go)
          self:OpenZhufuWnd(data)
        end
        CommonDefs.AddOnClickListener(self.SendStatusButton,DelegateFactory.Action_GameObject(onClick),false)
      end
    end
  end
end

function LuaDetailWish:Init(data)
	local onBackClick = function(go)
    self:Back()
	end
	CommonDefs.AddOnClickListener(self.BackButton,DelegateFactory.Action_GameObject(onBackClick),false)

  local wishData = data[0]
  self:InitItemNode(wishData)
  self:InitWishComment(wishData)
  if wishData.roleId == CClientMainPlayer.Inst.Id then
    self:InitSelfOp(wishData)
  else
    self:InitFriendOp(wishData)
  end
end

return LuaDetailWish
