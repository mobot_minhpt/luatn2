local CUITexture = import "L10.UI.CUITexture"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local UITable = import "UITable"
local Ease = import "DG.Tweening.Ease"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
LuaDaFuWongPlayerTemplate = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaDaFuWongPlayerTemplate, "bg", "bg", GameObject)
RegistChildComponent(LuaDaFuWongPlayerTemplate, "rank", "rank", UILabel)
RegistChildComponent(LuaDaFuWongPlayerTemplate, "Money", "Money", UILabel)
RegistChildComponent(LuaDaFuWongPlayerTemplate, "BuffTemplate", "BuffTemplate", GameObject)
RegistChildComponent(LuaDaFuWongPlayerTemplate, "Card", "Card", GameObject)
RegistChildComponent(LuaDaFuWongPlayerTemplate, "Task", "Task", GameObject)
RegistChildComponent(LuaDaFuWongPlayerTemplate, "Taotai", "Taotai", GameObject)
RegistChildComponent(LuaDaFuWongPlayerTemplate, "MyIcon", "MyIcon", GameObject)
RegistChildComponent(LuaDaFuWongPlayerTemplate, "Portrait", "Portrait", CUITexture)
RegistChildComponent(LuaDaFuWongPlayerTemplate, "Name", "Name", UILabel)
RegistChildComponent(LuaDaFuWongPlayerTemplate, "Table", "Table", UITable)
RegistChildComponent(LuaDaFuWongPlayerTemplate, "Touzi", "Touzi", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaDaFuWongPlayerTemplate, "m_RoundNum")
RegistClassMember(LuaDaFuWongPlayerTemplate, "m_PlayerId")
RegistClassMember(LuaDaFuWongPlayerTemplate, "m_CurMoney")
RegistClassMember(LuaDaFuWongPlayerTemplate, "m_MoneyTick")
RegistClassMember(LuaDaFuWongPlayerTemplate, "m_bgList")
RegistClassMember(LuaDaFuWongPlayerTemplate, "m_bgHighlightList")
RegistClassMember(LuaDaFuWongPlayerTemplate, "m_rankbgList")
RegistClassMember(LuaDaFuWongPlayerTemplate, "m_rankHighlightList")
RegistClassMember(LuaDaFuWongPlayerTemplate, "m_bgview")
RegistClassMember(LuaDaFuWongPlayerTemplate, "m_rankbgView")
RegistClassMember(LuaDaFuWongPlayerTemplate, "m_bgHighlightView")
RegistClassMember(LuaDaFuWongPlayerTemplate, "m_BuffViewList")
RegistClassMember(LuaDaFuWongPlayerTemplate, "m_PlayerName")
RegistClassMember(LuaDaFuWongPlayerTemplate, "m_hightStatus")
RegistClassMember(LuaDaFuWongPlayerTemplate, "m_CountDown")
function LuaDaFuWongPlayerTemplate:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.m_RoundNum = 0
    self.m_PlayerId = 0
    self.m_bgList = {"dafuwongmainplaywnd_wanjiaxinxi_yellow_n_01","dafuwongmainplaywnd_wanjiaxinxi_red_n_01","dafuwongmainplaywnd_wanjiaxinxi_purple_n_01","dafuwongmainplaywnd_wanjiaxinxi_green_n_01"}
    self.m_bgHighlightList = {"dafuwongmainplaywnd_wanjiaxinxi_yellow_s_01","dafuwongmainplaywnd_wanjiaxinxi_red_s_01","dafuwongmainplaywnd_wanjiaxinxi_purple_s_01","dafuwongmainplaywnd_wanjiaxinxi_green_s_01"}
    self.m_rankbgList = {"dafuwongmainplaywnd_wanjiaxinxi_yellow_n_02","dafuwongmainplaywnd_wanjiaxinxi_red_n_02","dafuwongmainplaywnd_wanjiaxinxi_purple_n_02","dafuwongmainplaywnd_wanjiaxinxi_green_n_02"}
    self.m_rankHighlightList = {"dafuwongmainplaywnd_wanjiaxinxi_yellow_s_02","dafuwongmainplaywnd_wanjiaxinxi_red_s_02","dafuwongmainplaywnd_wanjiaxinxi_purple_s_02","dafuwongmainplaywnd_wanjiaxinxi_green_s_02"}
    self.m_bgview = self.bg.transform:Find("NormalBg"):GetComponent(typeof(CUITexture))
    self.m_rankbgView = self.rank.transform:Find("Texture"):GetComponent(typeof(UISprite))
    self.m_bgHighlightView = self.bg.transform:Find("HighlightBg"):GetComponent(typeof(CUITexture))
    self.m_BuffViewList = nil
    self.BuffTemplate.gameObject:SetActive(false)
    self.m_hightStatus = 0
    self.m_MoneyTick = nil
    self.m_CurMoney = -1
    self.m_CountDown = self.Touzi.transform:Find("CountDown"):GetComponent(typeof(UILabel))
end
function LuaDaFuWongPlayerTemplate:InitData(id,data)
    if not data then return end
    self.m_RoundNum = data.round
    self.m_PlayerName = data.name
    self.m_PlayerId = id
    if data.IsOut then
        self:InitContent(data)
        self:SetFailed()
    else
        self:InitContent(data)
        self:UpdateMoney(data.Money)
        self:UpdateRank(data.Rank)
        self:UpdateTaskInfo(data.taskId)
        self:UpdateBuffInfo(data.BuffInfo)
        self:UpdateItemNum(data.LandsInfo)
        if self.m_PlayerId == LuaDaFuWongMgr.CurRoundPlayer then
            self:SetHightLight()
        end
    end

    
end
function LuaDaFuWongPlayerTemplate:InitContent(data)
    if CClientMainPlayer.Inst and self.m_PlayerId == CClientMainPlayer.Inst.Id then
        self.MyIcon:SetActive(true)
        self.Portrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(CClientMainPlayer.Inst.Class, CClientMainPlayer.Inst.Gender, -1), false)
    else
        self.MyIcon:SetActive(false)
        self.Portrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(data.class, data.gender, -1), false)
    end
    self.Name.text = self.m_PlayerName
    self.transform:Find("anchor/Buff"):GetComponent(typeof(UIWidget)):ResetAndUpdateAnchors()
    self:ResetHightLight()
    UIEventListener.Get(self.Portrait.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    LuaDaFuWongMgr:OnShowPlayerDetailWnd(self.m_PlayerId)
	end)
    local Hpath = SafeStringFormat3("UI/Texture/FestivalActivity/Festival_DaFuWong/Material/%s.mat",self.m_bgHighlightList[self.m_RoundNum])
    self.m_bgHighlightView:LoadMaterial(Hpath)
    local path = SafeStringFormat3("UI/Texture/FestivalActivity/Festival_DaFuWong/Material/%s.mat",self.m_bgList[self.m_RoundNum])
    self.m_bgview:LoadMaterial(path)
end
function LuaDaFuWongPlayerTemplate:SetHightLight()
    if self.m_hightStatus == 2 then return end
    self.m_bgview.gameObject:SetActive(false)
    self.m_bgHighlightView.gameObject:SetActive(true)
    local Portrait = self.Portrait.transform:GetComponent(typeof(UITexture))
    Portrait.width = 110
    Portrait.height = 110
    self.m_rankbgView.spriteName = self.m_rankHighlightList[self.m_RoundNum]
    local anchor = self.transform:Find("anchor").transform
    --LuaUtils.SetLocalPositionX(anchor, 25)
    self.Touzi.gameObject:SetActive(true)
    self.m_hightStatus = 2 
end

function LuaDaFuWongPlayerTemplate:ResetHightLight()
    if self.m_hightStatus == 1 then return end
    self.m_bgview.gameObject:SetActive(true)
    self.m_bgHighlightView.gameObject:SetActive(false)
    local Portrait = self.Portrait.transform:GetComponent(typeof(UITexture))
    Portrait.width = 90
    Portrait.height = 90
    local anchor = self.transform:Find("anchor").transform
    --LuaUtils.SetLocalPositionX(anchor, 0)
    self.m_rankbgView.spriteName = self.m_rankbgList[self.m_RoundNum]
    self.Touzi.gameObject:SetActive(false)
    self.m_hightStatus = 1
end

function LuaDaFuWongPlayerTemplate:UpdateMoney(money)
    

    local time = 1000
    if self.m_CurMoney < 0 then 
        self.Money.text = money or 0

    elseif money > self.m_CurMoney then -- 变多
        local targetMoney = money
        local curNum = self.m_CurMoney
        local interval = time / (targetMoney - curNum + 1)
        interval = math.max(interval,30)
        local delta = math.floor((targetMoney - curNum + 1) / (time / interval))
        local temp = curNum
        if self.m_MoneyTick then UnRegisterTick(self.m_MoneyTick) self.m_MoneyTick = nil end
        self.m_MoneyTick = RegisterTickWithDuration(function ()
            if temp >= targetMoney then
                self.Money.text = targetMoney
                self:EndMoneyScaleTween()

                UnRegisterTick(self.m_MoneyTick)
                self.m_MoneyTick = nil
                return
            end

            self.Money.text = temp
            temp = temp + delta
        end,interval,10000) 
        self:DoMoneyScaleTween()
    elseif money < self.m_CurMoney then -- 变少
        local targetMoney = money
        local curNum = self.m_CurMoney
        local interval = time / (curNum - targetMoney + 1)
        interval = math.max(interval,30)
        local delta = math.floor((curNum - targetMoney + 1) / (time / interval))
        local temp = curNum
        if self.m_MoneyTick then UnRegisterTick(self.m_MoneyTick) self.m_MoneyTick = nil end
        self.m_MoneyTick = RegisterTickWithDuration(function ()
            if temp <= targetMoney then
                self.Money.text = targetMoney

                self:EndMoneyScaleTween()
                UnRegisterTick(self.m_MoneyTick)
                self.m_MoneyTick = nil
                return
            end

            self.Money.text = temp
            
            temp = temp - delta
        end,interval,10000) 
        self:EndMoneyScaleTween()
        self:DoMoneyScaleTween()
    else

    end
    self.m_CurMoney = money
end

function LuaDaFuWongPlayerTemplate:DoMoneyScaleTween()
    local tween = LuaTweenUtils.SetEase(LuaTweenUtils.TweenScale(self.Money.transform, Vector3(1, 1, 1), Vector3(1.2 , 1.2, 1), 0.2),Ease.OutCubic)
    LuaTweenUtils.OnComplete(tween, function()
        --LuaTweenUtils.SetEase(LuaTweenUtils.TweenScale(self.Money.transform, Vector3(1.2, 1.2, 1), Vector3(1 , 1, 1), 0.4),Ease.OutBack)
        LuaTweenUtils.TweenScale(self.Money.transform, Vector3(1.2, 1.2, 1), Vector3(1 , 1, 1), 0.15)
    end)
end

function LuaDaFuWongPlayerTemplate:EndMoneyScaleTween()
    LuaTweenUtils.DOKill(self.Money.transform,false)
    LuaUtils.SetLocalScale(self.Money.transform,1,1,1)
end

function LuaDaFuWongPlayerTemplate:UpdateItemNum(data)
    local num = 0
    for k,v in pairs(data) do
        if v then num = num + 1 end
    end
    self.Card.transform:Find("Label"):GetComponent(typeof(UILabel)).text = num
end                                                                                         

function LuaDaFuWongPlayerTemplate:UpdateBuffInfo(buffInfo)
    if not self.m_BuffViewList then 
        self.m_BuffViewList = {}
        Extensions.RemoveAllChildren(self.Table.transform)
    end
    for k,v in pairs(buffInfo) do
        local buffdata = DaFuWeng_Buff.GetData(k)
        if not self.m_BuffViewList[k] and v > 0 and buffdata then
            local buffview = CUICommonDef.AddChild(self.Table.gameObject,self.BuffTemplate.gameObject)
            local roundLabel = buffview.transform:Find("NumLabel"):GetComponent(typeof(UILabel))
            local Icon = buffview.transform:Find("Icon"):GetComponent(typeof(UISprite))
            Icon.spriteName = buffdata.Icon
            self.m_BuffViewList[k] = {}
            self.m_BuffViewList[k].view = buffview
            self.m_BuffViewList[k].round = v
            self.m_BuffViewList[k].Label = roundLabel
        elseif self.m_BuffViewList[k] then
            self.m_BuffViewList[k].round = v
        end
    end
    for k,v in pairs(self.m_BuffViewList) do
        if v.round > 0 and buffInfo[k]  then
            v.view.gameObject:SetActive(true)
            v.Label.text = v.round
        else
            v.view.gameObject:SetActive(false)
        end
    end
    self.Table:Reposition()
end

function LuaDaFuWongPlayerTemplate:UpdateTaskInfo(taskId)
    if not taskId or taskId == 0 then self.Task.gameObject:SetActive(false) return end
    self.Task.gameObject:SetActive(false)
    if CClientMainPlayer.Inst and self.m_PlayerId == CClientMainPlayer.Inst.Id then
        local taskInfo = DaFuWeng_GongGaoTask.GetData(taskId)
        local taskData = LuaDaFuWongMgr.TaskInfo and LuaDaFuWongMgr.TaskInfo.task and LuaDaFuWongMgr.TaskInfo.task[taskId]
        if taskInfo and taskData then
            local target = taskInfo.Target
            local progress = 0
            if taskInfo.Type == 1 then
                local playerData = LuaDaFuWongMgr.PlayerInfo and LuaDaFuWongMgr.PlayerInfo[self.m_PlayerId].GoodsInfo
                if playerData then
                    for k,v in pairs(playerData) do
                        if v.goodsId == taskInfo.Param1 then progress = progress + 1 end
                    end
                end
            else
                progress = taskData.progress or 0
            end
            local des = SafeStringFormat3(LocalString.GetString("(%d/%d)"),progress,target)
            self.Task:GetComponent(typeof(UILabel)).text = SafeStringFormat3(taskInfo.Describe,des)
            self.Task.gameObject:SetActive(true)
        end
    end
end

function LuaDaFuWongPlayerTemplate:UpdateRank(rank)
    self.rank.text = rank or 1
end

function LuaDaFuWongPlayerTemplate:SetFailed()
    self.rank.gameObject:SetActive(false)
    self.Money.gameObject:SetActive(false)
    self.Card.gameObject:SetActive(false)
    self.Table.gameObject:SetActive(false)
    self.Task.gameObject:SetActive(false)
    self.Taotai.gameObject:SetActive(true)
    self.Name.gameObject:SetActive(false)
    self.MyIcon.gameObject:SetActive(false)
    self.Touzi.gameObject:SetActive(false)
    self.m_bgview.gameObject:SetActive(true)
    self.m_bgHighlightView.gameObject:SetActive(false)
    self.m_bgview:LoadMaterial("UI/Texture/FestivalActivity/Festival_DaFuWong/Material/dafuwongmainplaywnd_wanjiaxinxi_taotai.mat")
    self.Taotai.transform:Find("Sprite/Label"):GetComponent(typeof(UILabel)).text = self.m_PlayerName
end

function LuaDaFuWongPlayerTemplate:OnUpdateStageCoundInfo(totalTime)
    local curStage = LuaDaFuWongMgr.CurStage
    if LuaDaFuWongMgr.NeedShowCountDown and LuaDaFuWongMgr.NeedShowCountDown[curStage] and totalTime > 0 and self.m_PlayerId == LuaDaFuWongMgr.CurPlayerRound then
        self.m_CountDown.gameObject:SetActive(true)
        local CountDown = totalTime
        local endFunc = function() self.m_CountDown.gameObject:SetActive(false) end
        local durationFunc = function(time)
            self.m_CountDown.text = time
            if time < 0 then self.m_CountDown.gameObject:SetActive(false) end
        end
        LuaDaFuWongMgr:StartCountDownTick(EnumDaFuWengCountDownStage.PlayerCountDown,CountDown,durationFunc,endFunc)
    else
        self.m_CountDown.gameObject:SetActive(false)
    end
end
function LuaDaFuWongPlayerTemplate:OnUpdateStage(curStage)
    if not (LuaDaFuWongMgr.NeedShowCountDown and LuaDaFuWongMgr.NeedShowCountDown[curStage]) then 
        LuaDaFuWongMgr:EndCountDownTick(EnumDaFuWengCountDownStage.PlayerCountDown)
        self.m_CountDown.gameObject:SetActive(false)
        return
    end
    if curStage == EnumDaFuWengStage.RoundCard or 
    curStage == EnumDaFuWengStage.RoundTrade or 
    curStage == EnumDaFuWengStage.RoundGridLvUp or
    curStage == EnumDaFuWengStage.RoundGridLvUp or
    curStage == EnumDaFuWengStage.RoundGridBulletinBoard or
    curStage == EnumDaFuWengStage.RoundPawn then
        self:OnUpdateStageCoundInfo(DaFuWeng_Countdown.GetData(curStage).Time)
    end
end
--@region UIEvent

--@endregion UIEvent
function LuaDaFuWongPlayerTemplate:OnEnable()
    g_ScriptEvent:AddListener("OnDaFuWengMoneyUpdate",self,"UpdateMoney")
    g_ScriptEvent:AddListener("OnDaFuWengStartCountDownTick",self,"OnUpdateStageCoundInfo")
    g_ScriptEvent:AddListener("OnDaFuWengStageUpdate",self,"OnUpdateStage")
end

function LuaDaFuWongPlayerTemplate:OnDisable()
    g_ScriptEvent:RemoveListener("OnDaFuWengMoneyUpdate",self,"UpdateMoney")
    g_ScriptEvent:RemoveListener("OnDaFuWengStartCountDownTick",self,"OnUpdateStageCoundInfo")
    g_ScriptEvent:RemoveListener("OnDaFuWengStageUpdate",self,"OnUpdateStage")
    if self.m_MoneyTick then UnRegisterTick(self.m_MoneyTick) self.m_MoneyTick = nil end
    LuaDaFuWongMgr:EndCountDownTick(EnumDaFuWengCountDownStage.PlayerCountDown)
end
