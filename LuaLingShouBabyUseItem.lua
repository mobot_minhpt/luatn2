require("common/common_include")
local LuaGameObject=import "LuaGameObject"
local CItemAccessListMgr=import "L10.UI.CItemAccessListMgr"
local AlignType=import "L10.UI.CTooltip+AlignType"
local CItemMgr=import "L10.Game.CItemMgr"
local EnumItemPlace=import "L10.Game.EnumItemPlace"
local CLingShouMgr=import "L10.Game.CLingShouMgr"
local Gac2Gas=import "L10.Game.Gac2Gas"
local Constants=import "L10.Game.Constants"
--RequestLingShouBabyUseExpItem
CLuaLingShouBabyUseItem=class()
RegistClassMember(CLuaLingShouBabyUseItem,"m_Icon")
RegistClassMember(CLuaLingShouBabyUseItem,"m_NameLabel")
RegistClassMember(CLuaLingShouBabyUseItem,"m_ExpLabel")
RegistClassMember(CLuaLingShouBabyUseItem,"m_LongPressButton")
RegistClassMember(CLuaLingShouBabyUseItem,"m_CountUpdate")
RegistClassMember(CLuaLingShouBabyUseItem,"m_Mask")
RegistClassMember(CLuaLingShouBabyUseItem,"m_TemplateId")
function CLuaLingShouBabyUseItem:ctor()
end

function CLuaLingShouBabyUseItem:Init(tf)
    self.m_Icon=LuaGameObject.GetChildNoGC(tf,"Icon").cTexture
    self.m_CountUpdate=LuaGameObject.GetLuaGameObjectNoGC(tf).itemCountUpdate
    self.m_NameLabel=LuaGameObject.GetChildNoGC(tf,"NameLabel").label
    self.m_ExpLabel=LuaGameObject.GetChildNoGC(tf,"ExpLabel").label
    self.m_Mask=LuaGameObject.GetChildNoGC(tf,"Mask").gameObject
    self.m_LongPressButton=LuaGameObject.GetLuaGameObjectNoGC(tf).longPressButton

    local function LongPressCallback(go)
        if self.m_CountUpdate.count==0 then
            CItemAccessListMgr.Inst.absoluteLocalPosition = Vector3(32, 277)
            CItemAccessListMgr.Inst:ShowItemAccessInfo(self.m_TemplateId, false, nil, AlignType.Absolute)
        elseif self.m_CountUpdate.count>0 then
            if self.m_TemplateId==Constants.BabyFangShengExpItemId then 
                CUIManager.ShowUI(CLuaUIResources.LingShouConsumeBabyExpItemWnd)
            else
                local rst,pos,itemId=CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, self.m_TemplateId)
                local lingShouId=CLingShouMgr.Inst.selectedLingShou
                if rst and itemId and itemId~="" and lingShouId and lingShouId~=nil then
                    --请求
                    Gac2Gas.RequestLingShouBabyUseExpItem(lingShouId,1,pos,itemId)
                end
            end
            
        end
    end
    self.m_LongPressButton.callback=DelegateFactory.Action_GameObject(LongPressCallback)
end

function CLuaLingShouBabyUseItem:InitData(templateId)
    self.m_TemplateId=templateId
    local data=CItemMgr.Inst:GetItemTemplate(templateId)
    if data then
        self.m_Icon:LoadMaterial(data.Icon)
        self.m_NameLabel.text=data.Name
        self.m_ExpLabel.text=CUICommonDef.TranslateToNGUIText(data.Description)
    end

    self.m_CountUpdate.templateId=templateId
    local function OnCountChange(val)
        if val==0 then
            self.m_CountUpdate.format="[ff0000]{0}[-]"
            self.m_LongPressButton.enableLongPressAction = false
            self.m_Mask:SetActive(true)
        else
            self.m_CountUpdate.format="{0}"
            self.m_LongPressButton.enableLongPressAction = true
            self.m_Mask:SetActive(false)
        end
    end
    self.m_CountUpdate.onChange=DelegateFactory.Action_int(OnCountChange)
    self.m_CountUpdate:UpdateCount()
end



return CLuaLingShouBabyUseItem