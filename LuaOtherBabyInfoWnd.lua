local ShareMgr = import "ShareMgr"
local CChatLinkMgr = import "CChatLinkMgr"
local CBabyModelTextureLoader = import "L10.UI.CBabyModelTextureLoader"
local CUIFx = import "L10.UI.CUIFx"
local Baby_QiChang = import "L10.Game.Baby_QiChang"
local CUITexture = import "L10.UI.CUITexture"
local QnTabView=import "L10.UI.QnTabView"
local CButton=import "L10.UI.CButton"
local CServerTimeMgr=import "L10.Game.CServerTimeMgr"
--local Baby_YouEr = import "L10.Game.Baby_YouEr"
local Baby_ShaoNian = import "L10.Game.Baby_ShaoNian"
local Baby_Setting = import "L10.Game.Baby_Setting"
local Baby_Character = import "L10.Game.Baby_Character"
local Xingguan_StarGroup = import "L10.Game.Xingguan_StarGroup"
local CBabyAppearance=import "L10.Game.CBabyAppearance"

CLuaOtherBabyInfoWnd=class()
-- RegistClassMember(CLuaOtherBabyInfoWnd,"m_OkButton")
RegistClassMember(CLuaOtherBabyInfoWnd, "m_BabyTexture")
-- RegistClassMember(CLuaOtherBabyInfoWnd, "m_OwnerLabel")
-- RegistClassMember(CLuaOtherBabyInfoWnd, "m_GuildLabel")
RegistClassMember(CLuaOtherBabyInfoWnd, "m_ScoreLabel")
RegistClassMember(CLuaOtherBabyInfoWnd, "m_TitleLvLabel")
RegistClassMember(CLuaOtherBabyInfoWnd, "QiChangLabel")

RegistClassMember(CLuaOtherBabyInfoWnd, "TabBar")
RegistClassMember(CLuaOtherBabyInfoWnd, "BabyPropertyButton")
RegistClassMember(CLuaOtherBabyInfoWnd, "BabyPropertyView")
RegistClassMember(CLuaOtherBabyInfoWnd, "PropertyGrid")
RegistClassMember(CLuaOtherBabyInfoWnd, "PropertyTemplate")

RegistClassMember(CLuaOtherBabyInfoWnd, "m_PropInfo")
RegistClassMember(CLuaOtherBabyInfoWnd, "m_Grade")

CLuaOtherBabyInfoWnd.m_PlayerId=0
CLuaOtherBabyInfoWnd.m_BabyId=nil
CLuaOtherBabyInfoWnd.m_BabyInfo = nil

function CLuaOtherBabyInfoWnd:Init()
    self.m_BabyTexture = FindChild(self.transform,"BabyTexture"):GetComponent(typeof(CBabyModelTextureLoader))

    self.QiChangNotOpen = self.transform:Find("QiChangRoot/NotOpen").gameObject
    self.QiChangNotOpen:SetActive(false)
    self.QiChangTag = FindChild(self.transform,"QiChangTag").gameObject
    self.QiChangTag:SetActive(false)
    self.QiChangBG=self.QiChangTag.transform:Find("QiChangBG"):GetComponent(typeof(CUITexture))
    self.QiChangFX=self.QiChangTag.transform:Find("QiChangBG/QiChangFX"):GetComponent(typeof(CUIFx))
    self.QiChangLabel = self.QiChangTag.transform:Find("QiChangLabel"):GetComponent(typeof(UILabel))

    self.m_OwnerLabel=FindChild(self.transform,"OwnerLabel"):GetComponent(typeof(UILabel))
    self.m_OwnerLabel.text=nil
    -- self.m_GuildLabel=FindChild(self.transform,"GuildLabel"):GetComponent(typeof(UILabel))
    -- self.m_GuildLabel.text=nil
    self.m_ScoreLabel=FindChild(self.transform,"ScoreLabel"):GetComponent(typeof(UILabel))
    self.m_ScoreLabel.text="0"

    self.m_TitleLvLabel=FindChild(self.transform,"TitleLvLabel"):GetComponent(typeof(UILabel))
    self.m_TitleLvLabel.text=nil

    UIEventListener.Get(FindChild(self.transform,"ShareButton").gameObject).onClick=DelegateFactory.VoidDelegate(function(go)
        CUICommonDef.CaptureScreen("screenshot", true, true, nil, DelegateFactory.Action_string_bytes(function(path,bytes)
            ShareMgr.ShareLocalImage2PersonalSpace(path, nil)
        end))
    end)

    CLuaOtherBabyInfoWnd.m_PlayerId = tonumber(CLuaOtherBabyInfoWnd.m_PlayerId) or 0
    if CLuaOtherBabyInfoWnd.m_PlayerId > 0 and CLuaOtherBabyInfoWnd.m_BabyId then
    	Gac2Gas.QueryBabyInfo(CLuaOtherBabyInfoWnd.m_PlayerId,CLuaOtherBabyInfoWnd.m_BabyId)--playerId, babyId
    end
    
    
    self.TabBar = self.transform:Find("PropertyRoot/TabBar"):GetComponent(typeof(QnTabView))
    self.TabBar.OnSelect = DelegateFactory.Action_QnTabButton_int(function (go, index)
		self:OnTabChange(go, index)
    end)
    

	self.BabyPropertyView = self.transform:Find("PropertyRoot/BabyPropertyView").gameObject
	self.BabyPropertyButton = self.transform:Find("PropertyRoot/TabBar/Property"):GetComponent(typeof(CButton))
	self.PropertyGrid =  self.transform:Find("PropertyRoot/BabyPropertyView/PropertyGrid"):GetComponent(typeof(UIGrid))
	self.PropertyTemplate = self.transform:Find("PropertyRoot/BabyPropertyView/PropertyTemplate").gameObject
    self.PropertyTemplate:SetActive(false)
    self.PropertyGrid_ShaoNian = self.transform:Find("PropertyRoot/BabyPropertyView_ShaoNian/PropertyGrid_ShaoNian"):GetComponent(typeof(UIGrid))
    self.PropertyTemplate_ShaoNian = self.transform:Find("PropertyRoot/BabyPropertyView_ShaoNian/PropertyTemplate_ShaoNian").gameObject
    self.PropertyTemplate_ShaoNian:SetActive(false)

    self.BabyBasicView = self.transform:Find("PropertyRoot/BabyBasicView").gameObject
	self.BasicGrid = self.transform:Find("PropertyRoot/BabyBasicView/BasicGrid"):GetComponent(typeof(UIGrid))
	self.BasicTemplate = self.transform:Find("PropertyRoot/BabyBasicView/BasicTemplate").gameObject
    self.BasicTemplate:SetActive(false)
    
    self.BabyPropertyView_ShaoNian = self.transform:Find("PropertyRoot/BabyPropertyView_ShaoNian").gameObject

    if CLuaOtherBabyInfoWnd.m_BabyInfo then
    	self:OnSendQueryBabyInfoResult(CLuaOtherBabyInfoWnd.m_BabyId, unpack(CLuaOtherBabyInfoWnd.m_BabyInfo))
    	CLuaOtherBabyInfoWnd.m_BabyInfo = nil
    end
end


function CLuaOtherBabyInfoWnd:OnTabChange(go, index)
	if index == 1 then
		self.BabyBasicView:SetActive(false)
		self:OnPropertyViewClicked()
	elseif index == 0 then
		self.BabyPropertyView:SetActive(false)
		self.BabyPropertyView_ShaoNian:SetActive(false)
		self.BabyBasicView:SetActive(true)
	end
end

function CLuaOtherBabyInfoWnd:OnPropertyViewClicked()
	if self.m_Grade >= 11 and self.m_Grade < 31 then
		self.BabyPropertyView:SetActive(true)
	    self.BabyPropertyView_ShaoNian:SetActive(false)
	elseif self.m_Grade >= 31 then
        self.BabyPropertyView:SetActive(false)
        self.BabyPropertyView_ShaoNian:SetActive(true)
    else
        self.BabyPropertyView:SetActive(false)
        self.BabyPropertyView_ShaoNian:SetActive(false)
	end
end


function CLuaOtherBabyInfoWnd:GetPropertyTextColor(key)
	if key == 1 then return "F199FF"
	elseif key == 2 then return "F1EF91"
	elseif key == 3 then return "99DDFF"
	elseif key == 4 then return "A3FFD1"
	elseif key == 5 then return "F2A3FF"
	elseif key == 6 then return "FF8C8C"
	end
	return "FFFFFF"
end

function CLuaOtherBabyInfoWnd:GetBGColor(key)
	if key == 1 then return "A876E6"
	elseif key == 2 then return "FFCE50"
	elseif key == 3 then return "58DCFF"
	elseif key == 4 then return "50FFA1"
	elseif key == 5 then return "F550FF"
	elseif key == 6 then return "FF5050"
	end
	return "FFFFFF"
end

function CLuaOtherBabyInfoWnd:OnEnable()
	g_ScriptEvent:AddListener("SendQueryBabyInfoResult", self, "OnSendQueryBabyInfoResult")
end
function CLuaOtherBabyInfoWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SendQueryBabyInfoResult", self, "OnSendQueryBabyInfoResult")

end
function CLuaOtherBabyInfoWnd:OnSendQueryBabyInfoResult(babyId, ownerId, score,qichangId, ownerName, birthday, xingGuanId, natureId, showInfo,propInfo)
    self.m_PropInfo=propInfo
    self.m_Grade = showInfo.grade
    local babyInfo = CBabyAppearance()
    babyInfo.Status=showInfo.status
    babyInfo.Gender = showInfo.gender
    babyInfo.HairStyle = showInfo.hairstyle
    babyInfo.HairColor = showInfo.hairColor
    babyInfo.SkinColor = showInfo.skinColor
    babyInfo.WingId = showInfo.backId
    babyInfo.FashionHeadId = showInfo.headId
    babyInfo.FashionBodyId = showInfo.bodyId
    babyInfo.BodyWeight = showInfo.bodyWeight
    babyInfo.ColorId = showInfo.colorId
    babyInfo.QiChangId = showInfo.showQiChangId
    self.m_BabyTexture:Init(babyInfo,-180,0,0)

    self.m_OwnerLabel.text= CChatLinkMgr.TranslateToNGUIText(SafeStringFormat3("<link player=%s,%s>",ownerId,ownerName),false)
    UIEventListener.Get(self.m_OwnerLabel.gameObject).onClick=DelegateFactory.VoidDelegate(function(go)
        local url = self.m_OwnerLabel:GetUrlAtPosition(UICamera.lastWorldPosition)
        if url ~= nil then
            CChatLinkMgr.ProcessLinkClick(url, nil)
        end
    end)

    -- birthday, xingGuanId, natureId,
    local setting = Baby_Setting.GetData()
    local dataTime = CServerTimeMgr.ConvertTimeStampToZone8Time(birthday)
    local nature = Baby_Character.GetData(natureId)
    local natureDesc = nature and nature.Character or nil
    local xingguan = Xingguan_StarGroup.GetData(xingGuanId)

    local nameColor = xingguan.NameColor
    if CommonDefs.DictContains_LuaCall(setting.XingGuanChange, xingGuanId) then
        nameColor = setting.XingGuanChange[xingGuanId]
    end
    local xingguanDesc = SafeStringFormat3("%s-%s", xingguan.Xingxiu, nameColor)
    local infos = {
        {LocalString.GetString("生日"),SafeStringFormat3("%d-%d-%d",dataTime.Year,dataTime.Month,dataTime.Day)},
        {LocalString.GetString("星官"),xingguanDesc},
        {LocalString.GetString("性格"),natureDesc,},
        {LocalString.GetString("体型"),setting.BodyWeightName[showInfo.bodyWeight]}
    }

    CUICommonDef.ClearTransform(self.BasicGrid.transform)
	for i = 1, 4 do
		local go = NGUITools.AddChild(self.BasicGrid.gameObject, self.BasicTemplate)
        go:SetActive(true)
        local PropertyLabel = go.transform:Find("PropertyLabel"):GetComponent(typeof(UILabel))
        local ValueLabel = go.transform:Find("ValueLabel"):GetComponent(typeof(UILabel))
        PropertyLabel.text = infos[i][1]
        ValueLabel.text = infos[i][2]
	end
    self.BasicGrid:Reposition()

    --幼儿属性
    if showInfo.grade < 11 then
        --隐藏属性tab
        self.TabBar.m_TabButtons[1].gameObject:SetActive(false)
    elseif showInfo.grade >= 11 and showInfo.grade < 31 then
        CUICommonDef.ClearTransform(self.PropertyGrid.transform)
        Baby_YouEr.ForeachKey(function (key)
            local data = Baby_YouEr.GetData(key)
            local go = NGUITools.AddChild(self.PropertyGrid.gameObject, self.PropertyTemplate)
            go:SetActive(true)
            local PropertyLabel = go.transform:Find("PropertyLabel"):GetComponent(typeof(UILabel))
            local ValueLabel = go.transform:Find("ValueLabel"):GetComponent(typeof(UILabel))
            local BG = go.transform:GetComponent(typeof(CUITexture))

            PropertyLabel.text = data.Name
            BG:LoadMaterial(LuaBabyMgr.GetPropBG(key))
            local value = self.m_PropInfo[key].propNum or 0
            ValueLabel.text = tostring(value)
        end)
        self.PropertyGrid:Reposition()
    else
        CUICommonDef.ClearTransform(self.PropertyGrid_ShaoNian.transform)
        Baby_YouEr.ForeachKey(function (key)
            local data = Baby_YouEr.GetData(key)
            local go = NGUITools.AddChild(self.PropertyGrid_ShaoNian.gameObject, self.PropertyTemplate_ShaoNian)
            go:SetActive(true)

            local MainPropertyLabel = go.transform:Find("MainPropertyLabel"):GetComponent(typeof(UILabel))
            local SubPropertyLabel1 = go.transform:Find("SubPropertyLabel1"):GetComponent(typeof(UILabel))
            local SubPropertyLabel2 = go.transform:Find("SubPropertyLabel2"):GetComponent(typeof(UILabel))
            local BG = go.transform:GetComponent(typeof(CUITexture))
            local RectBG = go.transform:Find("RectBG"):GetComponent(typeof(UISprite))
            local Line = go.transform:Find("Line"):GetComponent(typeof(UISprite))

            MainPropertyLabel.text = data.Name
            BG:LoadMaterial(LuaBabyMgr.GetPropBG(key))

            local subKey1 = key * 2 - 1
            local subKey2 = key * 2
            local subData1 = Baby_ShaoNian.GetData(subKey1)
            SubPropertyLabel1.text = propInfo[subData1.ID] and SafeStringFormat3("[%s]%s %s[-]", self:GetPropertyTextColor(key), subData1.Name, propInfo[subData1.ID].propNum) or nil
            
            local subData2 = Baby_ShaoNian.GetData(subKey2)
            SubPropertyLabel2.text = propInfo[subData2.ID] and SafeStringFormat3("[%s]%s %s[-]", self:GetPropertyTextColor(key), subData2.Name, propInfo[subData2.ID].propNum) or nil

            Line.color = NGUIText.ParseColor24(self:GetPropertyTextColor(key), 0)
            Line.alpha = 0.7
            RectBG.color = NGUIText.ParseColor24(self:GetBGColor(key), 0)
            RectBG.alpha = 0.5
        end)
    
        self.PropertyGrid_ShaoNian:Reposition()
    end
    
    self.TabBar:ChangeTo(0)

    self.m_ScoreLabel.text=score

    self.m_TitleLvLabel.text=SafeStringFormat3(LocalString.GetString("%d级 %s"), showInfo.grade, showInfo.name)

    if qichangId == 0 then
		self.QiChangNotOpen:SetActive(true)
		self.QiChangTag:SetActive(false)
	else
		self.QiChangNotOpen:SetActive(false)
		self.QiChangTag:SetActive(true)
		
		local qichang = Baby_QiChang.GetData(qichangId)
		if qichang then
			local bgPath = LuaBabyMgr.GetQiChangBG(showInfo.gender, qichang.Quality)
			self.QiChangBG:LoadMaterial(bgPath)
			local name = qichang.NameM
			if showInfo.gender == 1 then
				name = qichang.NameF
			end
			self.QiChangLabel.text = SafeStringFormat3("[%s]%s[-]", LuaBabyMgr.GetQiChangNameColor(qichang.Quality-1), name)
            if CommonDefs.IS_VN_CLIENT and (not LocalString.isCN) then
                self.QiChangLabel.width = 200
            end
			self.QiChangFX:DestroyFx()
			local qichangfxPath = LuaBabyMgr.GetQiChangFx(qichang.Quality-1)
			if qichangfxPath then
				self.QiChangFX:LoadFx(qichangfxPath)
			end
		else
			self.QiChangNotOpen:SetActive(true)
			self.QiChangTag:SetActive(false)
		end
    end
end



