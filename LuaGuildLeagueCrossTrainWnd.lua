local DelegateFactory  = import "DelegateFactory"
local UITable = import "UITable"
local Extensions = import "Extensions"
local CButton = import "L10.UI.CButton"
local CItemMgr = import "L10.Game.CItemMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local UITabBar = import "L10.UI.UITabBar"
local CGuideMgr = import "L10.Game.Guide.CGuideMgr"
local COpenEntryMgr=import "L10.Game.COpenEntryMgr"
local Animation = import "UnityEngine.Animation"

LuaGuildLeagueCrossTrainWnd = class()

RegistClassMember(LuaGuildLeagueCrossTrainWnd, "m_TabBar")
RegistClassMember(LuaGuildLeagueCrossTrainWnd, "m_Ani")
RegistClassMember(LuaGuildLeagueCrossTrainWnd, "m_BuffTaskTabAlert")

RegistClassMember(LuaGuildLeagueCrossTrainWnd, "m_Header")
RegistClassMember(LuaGuildLeagueCrossTrainWnd, "m_HeaderTitleLabel")
RegistClassMember(LuaGuildLeagueCrossTrainWnd, "m_HeaderDescLabel")
RegistClassMember(LuaGuildLeagueCrossTrainWnd, "m_HeaderInfoButton")

RegistClassMember(LuaGuildLeagueCrossTrainWnd, "m_TrainSubview")
RegistClassMember(LuaGuildLeagueCrossTrainWnd, "m_BuffTaskSubview")
RegistClassMember(LuaGuildLeagueCrossTrainWnd, "m_ContributionSubview")

RegistClassMember(LuaGuildLeagueCrossTrainWnd, "m_ShopButton")

function LuaGuildLeagueCrossTrainWnd:Awake()
    self.m_TabBar = self.gameObject:GetComponent(typeof(UITabBar))
    self.m_Ani = self.gameObject:GetComponent(typeof(Animation))
    self.m_BuffTaskTabAlert = self.m_TabBar:GetTabGoByIndex(1).transform:Find("Alert").gameObject
    self.m_Header = self.transform:Find("Anchor/Header").gameObject
    self.m_HeaderTitleLabel = self.transform:Find("Anchor/Header/TitleLabel"):GetComponent(typeof(UILabel))
    self.m_HeaderDescLabel = self.transform:Find("Anchor/Header/DescLabel"):GetComponent(typeof(UILabel))
    self.m_HeaderInfoButton = self.transform:Find("Anchor/Header/InfoButton").gameObject

    self.m_TrainSubview = self.transform:Find("Anchor/GuildLeagueCrossTrainSubview").gameObject
    self.m_BuffTaskSubview = self.transform:Find("Anchor/GuildLeagueCrossTrainBuffTaskSubview").gameObject
    self.m_ContributionSubview = self.transform:Find("Anchor/GuildLeagueCrossTrainContributionSubview").gameObject

    self.m_ShopButton = self.transform:Find("Anchor/ShopButton").gameObject
    
    self.m_TabBar.OnTabChange = DelegateFactory.Action_GameObject_int(
        function(go, index)
            self:OnTabChange(go, index)
        end
    )

    UIEventListener.Get(self.m_HeaderInfoButton).onClick = DelegateFactory.VoidDelegate(function() self:OnHeaderInfoButtonClick() end)
    UIEventListener.Get(self.m_ShopButton).onClick = DelegateFactory.VoidDelegate(function() self:OnShopButtonClick() end)
end

function LuaGuildLeagueCrossTrainWnd:Init()
    --需要确保根据rpc返回结果打开页面
    local trainInfo = LuaGuildLeagueCrossMgr.m_TrainInfo
    self.m_TabBar:ChangeTab(trainInfo.isBuffTask and 1 or 0, true)
    self.m_BuffTaskTabAlert:SetActive(self.m_TabBar.SelectedIndex~=1 and LuaGuildLeagueCrossMgr:NeedShowBuffTaskAlert())
    self:ShowView(self.m_TabBar.SelectedIndex)
    self:TryTriggerGuide()
end

function LuaGuildLeagueCrossTrainWnd:OnTabChange(go, index)
    if index == 0 then
        LuaGuildLeagueCrossMgr:QueryGuildLeagueTrainInfo() --等待查询结果返回再切页
    elseif index == 1 then
        LuaGuildLeagueCrossMgr:QueryGuildLeagueBuffTaskInfo() --等待查询结果返回再切页
        LuaGuildLeagueCrossMgr:MarkBuffTaskAlertHidden()
    else -- contribution
        self:ShowView(index)
    end
end

function LuaGuildLeagueCrossTrainWnd:ShowView(index)
    self:InitHeader(index)
    self.m_TrainSubview:SetActive(index==0)
    self.m_BuffTaskSubview:SetActive(index==1)
    self.m_ContributionSubview:SetActive(index==2)
    if index==0 then
        self.m_TrainSubview:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf:Init()
    elseif index==1 then
        self.m_BuffTaskSubview:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf:Init()
    end
    self.m_Ani:Play("guildleaguecrosstrainwnd_qihuan"..tostring(index+1))
end

function LuaGuildLeagueCrossTrainWnd:InitHeader(index)
    if index == 0 then
        self.m_Header:SetActive(true)
        self.m_HeaderTitleLabel.text = LocalString.GetString("提交资源提升培养等级，为出战帮会获得增益")
        self.m_HeaderDescLabel.text = SafeStringFormat3(LocalString.GetString("[00FF00]%s[-] 活动结束，开战时本服将获得增益如下："), LuaGuildLeagueCrossMgr:GetTrainEndTimeStr(LocalString.GetString("yyyy年MM月dd日HH点mm分")))
    elseif index == 1 then
        self.m_Header:SetActive(true)
        self.m_HeaderTitleLabel.text = LocalString.GetString("完成日常任务，为出战帮会增加气血")
        self.m_HeaderDescLabel.text = SafeStringFormat3(LocalString.GetString("[00FF00]%s[-] 活动结束，开战时本服将获得增益如下："), LuaGuildLeagueCrossMgr:GetTrainEndTimeStr(LocalString.GetString("yyyy年MM月dd日HH点mm分")))
    else
        self.m_Header:SetActive(false)
    end    
end


function LuaGuildLeagueCrossTrainWnd:OnHeaderInfoButtonClick()
    --TODO 提醒策划移除旧的GLC_GUILD_LEAGUE_TRAIN_DESC
    LuaGuildLeagueCrossMgr:QueryGuildLeagueCrossMatchBuffInfo()
end


function LuaGuildLeagueCrossTrainWnd:OnShopButtonClick()
    LuaGuildLeagueCrossMgr:ShowTrainShopWnd()
end


--兑换商店引导
function LuaGuildLeagueCrossTrainWnd:GetTabIndex()
    return self.m_TabBar.SelectedIndex
end

function LuaGuildLeagueCrossTrainWnd:GetGuideGo(methodName)
    if methodName == "GetTrainTab" then
        return self.m_TabBar:GetTabGoByIndex(0)
    elseif methodName == "GetBuffTaskTab" then
		return self.m_TabBar:GetTabGoByIndex(1)
	elseif methodName == "GetShopBtn" then
		return self.m_ShopButton
	end
	return nil
end

function LuaGuildLeagueCrossTrainWnd:TryTriggerGuide()
    if not CLuaGuideMgr.EnableGuide() then return end
    if COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.GuildLeagueCrossTrainShop) then
        return
    end
    local triggered = CGuideMgr.Inst:StartGuide(EnumGuideKey.GuildLeagueCrossTrainShop, 0)
    if triggered then
        COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.GuildLeagueCrossTrainShop)
    end
end

