-- Auto Generated!!
local Boolean = import "System.Boolean"
local CGuildMemberDataPickerWnd = import "L10.UI.CGuildMemberDataPickerWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local Extensions = import "Extensions"
local GuildMemberExportMgr = import "L10.Game.GuildMemberExportMgr"
local NGUITools = import "NGUITools"
local QnCheckBox = import "L10.UI.QnCheckBox"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CGuildMemberDataPickerWnd.m_Init_CS2LuaHook = function (this) 
    Extensions.RemoveAllChildren(this.table.transform)
    CommonDefs.ListClear(this.checkboxes)

    do
        local i = 0
        while i < GuildMemberExportMgr.Inst.AllKeys.Length do
            local instance = NGUITools.AddChild(this.table.gameObject, this.checkBoxTpl)
            instance:SetActive(true)
            local uiCheckBox = CommonDefs.GetComponent_GameObject_Type(instance, typeof(QnCheckBox))
            local data = GuildMemberExportMgr.Inst.AllKeys[i]
            uiCheckBox.Text = data.text
            uiCheckBox.Selected = data.defaultSelected
            uiCheckBox.Enabled = data.canChangeSelection
            CommonDefs.ListAdd(this.checkboxes, typeof(QnCheckBox), uiCheckBox)
            i = i + 1
        end
    end
    this.table:Reposition()
end
CGuildMemberDataPickerWnd.m_Start_CS2LuaHook = function (this) 
    this.checkBoxTpl:SetActive(false)
    UIEventListener.Get(this.closeBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeBtn).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.okBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.okBtn).onClick, MakeDelegateFromCSFunction(this.OnOKButtonClick, VoidDelegate, this), true)
end
CGuildMemberDataPickerWnd.m_OnOKButtonClick_CS2LuaHook = function (this, go) 
    local selectedResults = CreateFromClass(MakeGenericClass(List, Boolean))
    do
        local i = 0
        while i < this.checkboxes.Count do
            CommonDefs.ListAdd(selectedResults, typeof(Boolean), this.checkboxes[i].Selected)
            i = i + 1
        end
    end
    GuildMemberExportMgr.Inst:ShowMemberExportWnd(selectedResults)
    this:Close()
end
