-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CommonDefs = import "L10.Game.CommonDefs"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local L10 = import "L10"
local MsgPackImpl = import "MsgPackImpl"
local Object = import "System.Object"
local UInt32 = import "System.UInt32"
--class CSecondPwdMgr
--{
--}

Gas2Gac.ReplySecondaryPasswordProtectContent = function (protectContent_U) 
    L10.CLogMgr.Log("ReplySecondaryPasswordProtectContent")
    local list = TypeAs(MsgPackImpl.unpack(protectContent_U), typeof(MakeGenericClass(List, Object)))
    local ret = CreateFromClass(MakeGenericClass(List, UInt32))
    do
        local i = 0
        while i < list.Count do
            CommonDefs.ListAdd(ret, typeof(UInt32), math.floor(tonumber(list[i] or 0)))
            i = i + 1
        end
    end
    EventManager.BroadcastInternalForLua(EnumEventType.ReplySecondaryPasswordProtectContent, {ret})
end
Gas2Gac.SyncSecondaryPasswordInfo = function (tp, info_U) 
    if CClientMainPlayer.Inst == nil then
        return
    end

    local bufLen = info_U.Length
    local startIndex = 0
    local default
    default, bufLen, startIndex = CClientMainPlayer.Inst.BasicProp.SecondaryPasswordInfo:LoadFromBytes(info_U, bufLen, bufLen, startIndex)
    L10.CLogMgr.Log("SyncSecondaryPasswordInfo  " .. tp)
    if tp == EnumSecondaryPasswordOperationType_lua.eProtectContent then
        --关掉保存内容的窗口
        CUIManager.CloseUI(CUIResources.SecondPwdContentWnd)
    elseif tp == EnumSecondaryPasswordOperationType_lua.eUpdate then
        --修改二级密码
        CUIManager.CloseUI(CUIResources.SecondPwdChangeWnd)
    elseif tp == EnumSecondaryPasswordOperationType_lua.eClose then
        --关闭二级密码
        CUIManager.CloseUI(CUIResources.SecondPwdConfirmWnd)
        CUIManager.CloseUI(CUIResources.SecondPwdWnd)
    elseif tp == EnumSecondaryPasswordOperationType_lua.eSet then
        --设置二级密码
        CUIManager.CloseUI(CUIResources.SecondPwdSetWnd)
        CUIManager.CloseUI(CUIResources.SecondPwdChangeWnd)

        CUIManager.ShowUI(CUIResources.SecondPwdContentWnd)
    elseif tp == EnumSecondaryPasswordOperationType_lua.eClear then
        --忘记密码 关掉所有二级密码相关界面
        CUIManager.CloseUI(CUIResources.SecondPwdChangeWnd)
        CUIManager.CloseUI(CUIResources.SecondPwdSetWnd)
        CUIManager.CloseUI(CUIResources.SecondPwdContentWnd)
        CUIManager.CloseUI(CUIResources.SecondPwdConfirmWnd)
        CUIManager.CloseUI(CUIResources.SecondPwdVerifyWnd)
        CUIManager.CloseUI(CUIResources.SecondPwdWnd)
    elseif tp == EnumSecondaryPasswordOperationType_lua.eVerify then
        CUIManager.CloseUI(CUIResources.SecondPwdVerifyWnd)
        CUIManager.CloseUI(CUIResources.SecondPwdConfirmWnd)
    end
    EventManager.BroadcastInternalForLua(EnumEventType.SyncSecondaryPasswordInfo, {tp})
end
