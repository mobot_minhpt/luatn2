local CCommonUIFxWnd = import "L10.UI.CCommonUIFxWnd"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaCarnival2023Mgr = {}

LuaCarnival2023Mgr.pveResultInfo = {}
LuaCarnival2023Mgr.pveSkillInfo = {}
LuaCarnival2023Mgr.pvePlayerCountInfo = {}

--#region PVE

function LuaCarnival2023Mgr:GetPVENormalGamePlayId()
    return Carnival2023_Normal.GetData().GameplayId
end

function LuaCarnival2023Mgr:GetPVEBossGamePlayId()
    return Carnival2023_Boss.GetData().GameplayId
end

function LuaCarnival2023Mgr:OnMainPlayerCreated(playId)
    if playId == self:GetPVENormalGamePlayId() then
        LuaCommonGamePlayTaskViewMgr:AppendGameplayInfo(self:GetPVENormalGamePlayId(),
            true, nil,
            true,
            true, nil,
            true, nil,
            true, nil, nil,
            true, nil, function() g_MessageMgr:ShowMessage("CARNIVAL_2023_PVE_NORMAL_TIP") end)
    elseif playId == self:GetPVEBossGamePlayId() then
        LuaCommonGamePlayTaskViewMgr:AppendGameplayInfo(self:GetPVEBossGamePlayId(),
            true, nil,
            true,
            true, nil,
            true, nil,
            true, nil, nil,
            true, nil, function() g_MessageMgr:ShowMessage("CARNIVAL_2023_PVE_BOSS_TIP") end)
    end
end

function LuaCarnival2023Mgr:GetPVERankAwardItemId()
	local mailId = Carnival2023_Boss.GetData().RankAwardMailId
    local items = Mail_Mail.GetData(mailId).Items
	local itemId = string.match(items, "(%d+)")
	return tonumber(itemId)
end

function LuaCarnival2023Mgr:GetPVEPassAwardItemId()
    local mailId = Carnival2023_Boss.GetData().JoinAwardMailId
    local items = Mail_Mail.GetData(mailId).Items
	local itemId = string.match(items, "(%d+)")
	return tonumber(itemId)
end

--#endregion PVE

--#region Gas2Gac

function LuaCarnival2023Mgr:Carnival2023MonsterResult(killMonsterNum, getAward, getPoint)
    self.pveResultInfo = {}
    self.pveResultInfo.type = "normal"
    self.pveResultInfo.killMonsterNum = killMonsterNum
    self.pveResultInfo.getAward = getAward
    self.pveResultInfo.getPoint = getPoint
    CUIManager.ShowUI(CLuaUIResources.Carnival2023PVEResultWnd)
end

function LuaCarnival2023Mgr:Carnival2023BossResult(bWin, finishTime, myRank, minTime, award)
    if not bWin then
        CCommonUIFxWnd.Instance:LoadUIFxAssignedPath("Fx/Vfx/Prefab/Dynamic/common_result_fail.prefab", 2)
        return
    end

    self.pveResultInfo = {}
    self.pveResultInfo.type = "boss"
    self.pveResultInfo.finishTime = finishTime
    self.pveResultInfo.myRank = myRank
    self.pveResultInfo.award = award > 0
    CUIManager.ShowUI(CLuaUIResources.Carnival2023PVEResultWnd)
end

function LuaCarnival2023Mgr:QueryCarnival2023RankResult(myRank, finishTime, RankData_U)
    g_ScriptEvent:BroadcastInLua("QueryCarnival2023RankResult", myRank, finishTime, RankData_U)
end

function LuaCarnival2023Mgr:Carnival2023RequestPlayDataResult(attackBuffLevel, defBuffLevel, speedBuffLevel, skillPoint, mReminTime, bReminTime, rReminTime, rank)
    self.pveSkillInfo = {}
    self.pveSkillInfo.attackBuffLevel = attackBuffLevel
    self.pveSkillInfo.defBuffLevel = defBuffLevel
    self.pveSkillInfo.speedBuffLevel = speedBuffLevel
    self.pveSkillInfo.skillPoint = skillPoint
    g_ScriptEvent:BroadcastInLua("Carnival2023RequestPlayDataResult", mReminTime, bReminTime, rReminTime, rank)
end

function LuaCarnival2023Mgr:Carnival2023BossTrapPlayerCount(npcid, currentPlayerNum, needPlayerNum)
    if needPlayerNum == 0 then
        self.pvePlayerCountInfo[npcid] = nil
        return
    end

    self.pvePlayerCountInfo[npcid] = {
        currentPlayerNum = currentPlayerNum,
        needPlayerNum = needPlayerNum
    }
end

--#endregion Gas2Gac

function LuaCarnival2023Mgr:Carnival2023BossOpenCheck()
    local dateTime = CServerTimeMgr.Inst:GetZone8Time()
    local dayOfWeek = EnumToInt(dateTime.DayOfWeek)
    if dayOfWeek == 0 or dayOfWeek == 6 then
        local startTime = 12 * 3600
        local endTime = 23 * 3600 + 59 * 60
        local nowTime = dateTime.Hour * 3600 + dateTime.Minute * 60 + dateTime.Second
        if nowTime > startTime and nowTime < endTime then
            return true
        end
    end
    return false
end
