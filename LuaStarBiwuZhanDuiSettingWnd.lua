local Vector3 = import "UnityEngine.Vector3"
local CUITexture=import "L10.UI.CUITexture"
local MessageWndManager = import "L10.UI.MessageWndManager"
local Extensions = import "Extensions"
local DelegateFactory = import "DelegateFactory"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local BoxCollider = import "UnityEngine.BoxCollider"

CLuaStarBiwuZhanDuiSettingWnd = class()
CLuaStarBiwuZhanDuiSettingWnd.Path = "ui/starbiwu/LuaStarBiwuZhanDuiSettingWnd"
RegistClassMember(CLuaStarBiwuZhanDuiSettingWnd,"Grid")
RegistClassMember(CLuaStarBiwuZhanDuiSettingWnd,"ItemPrefab")
RegistClassMember(CLuaStarBiwuZhanDuiSettingWnd,"CloseButton")
RegistClassMember(CLuaStarBiwuZhanDuiSettingWnd,"HintButton")
RegistClassMember(CLuaStarBiwuZhanDuiSettingWnd,"CountLabels")
RegistClassMember(CLuaStarBiwuZhanDuiSettingWnd,"m_SaveBtn")
RegistClassMember(CLuaStarBiwuZhanDuiSettingWnd,"m_MemberSettingItemList")
RegistClassMember(CLuaStarBiwuZhanDuiSettingWnd,"m_ChuZhanInfo")
RegistClassMember(CLuaStarBiwuZhanDuiSettingWnd,"m_IsDirty")
RegistClassMember(CLuaStarBiwuZhanDuiSettingWnd,"m_MaxPlayerInBattle")

RegistClassMember(CLuaStarBiwuZhanDuiSettingWnd,"m_IsTeamLeader")
RegistClassMember(CLuaStarBiwuZhanDuiSettingWnd,"m_MainPlayerId")
-- Auto Generated!!
function CLuaStarBiwuZhanDuiSettingWnd:Init( )
    if CClientMainPlayer.Inst then
        self.m_MainPlayerId=CClientMainPlayer.Inst.Id
    end
    self.Grid = self.transform:Find("ShowArea/ScrollView/Grid"):GetComponent(typeof(UIGrid))
    self.ScrollView = self.transform:Find("ShowArea/ScrollView"):GetComponent(typeof(UIScrollView))
    self.ItemPrefab = self.transform:Find("ShowArea/Member").gameObject
    self.ItemPrefab:SetActive(false)
    self.CloseButton = self.transform:Find("Wnd_Bg_Primary_Normal/CloseButton").gameObject
    self.HintButton = self.transform:Find("ShowArea/BottomArea/Sprite").gameObject
    self.BottomAreaLabel = self.transform:Find("ShowArea/BottomArea/BottomAreaLabel").gameObject:GetComponent(typeof(UILabel))
    self.BottomAreaLabel.text = g_MessageMgr:FormatMessage("StarBiwuZhanDuiSettingWnd_BottomAreaLabel")
    self.CountLabels = {}
    local scoreArrList = StarBiWuShow_Setting.GetData().Round_Duration_And_Score
    for i = 1, 6 do
        self.CountLabels[i] = self.transform:Find(SafeStringFormat3("ShowArea/TopArea/TitleBlock%d/CountLabel",i)):GetComponent(typeof(UILabel))
        local scoreLabel = self.transform:Find(SafeStringFormat3("ShowArea/TopArea/TitleBlock%d/ScoreLabel",i)):GetComponent(typeof(UILabel))
        scoreLabel.text = SafeStringFormat3(LocalString.GetString("%d分"), scoreArrList[i - 1][1])
    end

    self.m_SaveBtn = self.transform:Find("ShowArea/BottomArea/SaveButton").gameObject
    self.m_MemberSettingItemList = {}
    self.m_ChuZhanInfo = nil
    self.m_IsDirty = false

    self.m_MaxPlayerInBattle={5,1,2,3,5,1}

    self.m_SaveBtn:SetActive(false)
    self.m_IsTeamLeader=false
    if CClientMainPlayer.Inst then
        --是否是队长
        if CLuaStarBiwuMgr.m_MemberInfoTable then
            local leaderInfo=CLuaStarBiwuMgr.m_MemberInfoTable[1]
            if leaderInfo.m_Id==CClientMainPlayer.Inst.Id then
                self.m_SaveBtn:SetActive(true)
                self.m_IsTeamLeader=true
            end
        end
    end

    UIEventListener.Get(self.HintButton).onClick = DelegateFactory.VoidDelegate(function(go) self:onHintClick(go) end)
    UIEventListener.Get(self.m_SaveBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnSaveBtnClicked(go) end)

    self:showMemberList()
end
function CLuaStarBiwuZhanDuiSettingWnd:OnEnable( )
    g_ScriptEvent:AddListener("ReplyStarBiwuZhanDuiInfo", self, "showMemberList")

end
function CLuaStarBiwuZhanDuiSettingWnd:OnDisable( )
    g_ScriptEvent:RemoveListener("ReplyStarBiwuZhanDuiInfo", self, "showMemberList")
    CLuaStarBiwuMgr.m_StarBiwuZhanDuiSettingWndCurRound = 0 
end
function CLuaStarBiwuZhanDuiSettingWnd:showMemberList( )
    self.m_MemberSettingItemList={}

    self.m_ChuZhanInfo = CLuaStarBiwuMgr.m_ChuZhanInfoList
    Extensions.RemoveAllChildren(self.Grid.transform)
    do
        local i = 0 local cnt = #CLuaStarBiwuMgr.m_MemberInfoTable
        while i < cnt do
            local go = CommonDefs.Object_Instantiate(self.ItemPrefab)
            go:SetActive(true)
            local trans = go.transform
            self.Grid:AddChild(trans)
            trans.localScale = Vector3.one
            trans.localPosition = Vector3.zero
            self:InitItem(go.transform,CLuaStarBiwuMgr.m_MemberInfoTable[i + 1], i)
            table.insert( self.m_MemberSettingItemList,go.transform )
            do
                local j = 0 local cnt2 = self.m_ChuZhanInfo.Length
                while j < cnt2 do
                    if (bit.band(self.m_ChuZhanInfo[j], (bit.lshift(1, i)))) ~= 0 then
                        self:SetCheckBox(go.transform,j,true)
                    else
                        self:SetCheckBox(go.transform,j,false)
                    end
                    j = j + 1
                end
            end
            i = i + 1
        end
    end
    self:RefreshLabels()

    self.Grid:Reposition()
    self.ScrollView:ResetPosition()
    self.m_IsDirty = false
    if CLuaStarBiwuMgr.m_StarBiwuZhanDuiSettingWndCurRound > 0 then
        self:SetNonadjustable(CLuaStarBiwuMgr.m_StarBiwuZhanDuiSettingWndCurRound <= 3 and 3 or 6)
    end
end
function CLuaStarBiwuZhanDuiSettingWnd:OnCheckBoxChanged( memberIndex, checkBoxIndex)
    if (bit.band(self.m_ChuZhanInfo[checkBoxIndex], (bit.lshift(1, memberIndex)))) ~= 0 then--取消选择
        self:SetCheckBox(self.m_MemberSettingItemList[memberIndex+1],checkBoxIndex,false)
        self.m_ChuZhanInfo[checkBoxIndex] = bit.band(self.m_ChuZhanInfo[checkBoxIndex], (bit.bnot((bit.lshift(1, memberIndex)))))
        self.m_IsDirty = true
        self:RefreshLabels()
        return
    end
    
    local hasOtherBattle = false
    local battleCnt = 0
    do
        local i = 0 local cnt = #self.m_MemberSettingItemList--.Count
        while i < cnt do
            for j = 0, 5 do
                if (bit.band(self.m_ChuZhanInfo[j], (bit.lshift(1, i)))) ~= 0 then--团队赛
                    if i == memberIndex then
                        hasOtherBattle = true
                    end
                    battleCnt = battleCnt + 1
                    break
                end
            end
            if hasOtherBattle then
                break
            end
            i = i + 1
        end
    end


    if not hasOtherBattle and battleCnt >= 7 then
        g_MessageMgr:ShowMessage("Qmpk_ChuZhan_Set_Special_Rule2")--出战总人数不得超过6人
        return
    end

    --出战人数验证
    if self:GetOneCount(self.m_ChuZhanInfo[checkBoxIndex]) >= self.m_MaxPlayerInBattle[checkBoxIndex+1] then
        g_MessageMgr:ShowMessage("QMPK_Reach_Max_Player_In_Battle")--该场次出战人数已达上限
        return
    end

    local conflictData = StarBiWuShow_Conflict.GetData(checkBoxIndex + 1)
    if conflictData then
        local arr = {conflictData.Round_1,conflictData.Round_2,conflictData.Round_3,conflictData.Round_4,conflictData.Round_5,conflictData.Round_6}
        for i = 1, 6 do
            if ((bit.band(self.m_ChuZhanInfo[i - 1], (bit.lshift(1, memberIndex)))) ~= 0) and (arr[i] > 0) and (i ~= ( checkBoxIndex + 1)) then
                local conflictData2 = StarBiWuShow_Conflict.GetData(i)
                g_MessageMgr:ShowMessage("StarBiwu_Cannot_Adjust_ChuZhan_Round_Conflict",conflictData2.RoundName,conflictData.RoundName)
                return
                --self:SetCheckBox(self.m_MemberSettingItemList[memberIndex + 1],i - 1,false)
                --self.m_ChuZhanInfo[i - 1] = bit.band(self.m_ChuZhanInfo[i - 1], (bit.bnot((bit.lshift(1, memberIndex)))))
            end
        end
        local member1 = CLuaStarBiwuMgr.m_MemberInfoTable[memberIndex + 1]
        if conflictData.SameClass == 0 then
            local find = false
            for i = 1,#CLuaStarBiwuMgr.m_MemberInfoTable do
                if i ~= (memberIndex + 1) and ((bit.band(self.m_ChuZhanInfo[checkBoxIndex], (bit.lshift(1, i - 1)))) ~= 0) then
                    local member2 = CLuaStarBiwuMgr.m_MemberInfoTable[i]
                    if member1.m_Class == member2.m_Class then
                        find =  true
                        if find then
                            g_MessageMgr:ShowMessage("StarBiwu_Cannot_Adjust_ChuZhan_Same_Class",conflictData.RoundName)
                            return
                        end
                        break
                    end
                end
            end
        end
        self.m_ChuZhanInfo[checkBoxIndex] = bit.bor(self.m_ChuZhanInfo[checkBoxIndex], (bit.lshift(1, memberIndex)))
        self:SetCheckBox(self.m_MemberSettingItemList[memberIndex+1],checkBoxIndex,true)
    end

    self.m_IsDirty = true
    self:RefreshLabels()
end
function CLuaStarBiwuZhanDuiSettingWnd:GetOneCount( value)
    local cnt = 0
    while value > 0 do
        cnt = cnt + 1
        value = bit.band(value, (value - 1))
    end
    return cnt
end

function CLuaStarBiwuZhanDuiSettingWnd:OnCloseBtnClicked( go)
    if self.m_IsDirty then
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("QMPK_ZhanDui_Setting_Save_Tip"), DelegateFactory.Action(function ()
            self:OnSaveBtnClicked(nil)
            -- self:Close()
            CUIManager.CloseUI(CLuaUIResources.StarBiwuZhanDuiSettingWnd)
        end), DelegateFactory.Action(function ()
            -- self:Close()
            CUIManager.CloseUI(CLuaUIResources.StarBiwuZhanDuiSettingWnd)
        end), nil, nil, false)
    else
        -- self:Close()
        CUIManager.CloseUI(CLuaUIResources.StarBiwuZhanDuiSettingWnd)
    end
end
function CLuaStarBiwuZhanDuiSettingWnd:OnSaveBtnClicked( go)
    if CLuaStarBiwuMgr.m_IsBiWuChuZhanSetting > 0 then
        Gac2Gas.AdjustStarBiwuBiWuChuZhanInfo(self.m_ChuZhanInfo[0], self.m_ChuZhanInfo[1], self.m_ChuZhanInfo[2], self.m_ChuZhanInfo[3], self.m_ChuZhanInfo[4], self.m_ChuZhanInfo[5])
    else
        Gac2Gas.RequestSetStarBiwuChuZhanInfo(self.m_ChuZhanInfo[0], self.m_ChuZhanInfo[1], self.m_ChuZhanInfo[2], self.m_ChuZhanInfo[3], self.m_ChuZhanInfo[4], self.m_ChuZhanInfo[5])
    end
    self.m_IsDirty = false
end
function CLuaStarBiwuZhanDuiSettingWnd:RefreshLabels( )
    local nameArr = {LocalString.GetString("团队赛"),LocalString.GetString("单人赛"),LocalString.GetString("双人赛"),LocalString.GetString("三人赛"),
    LocalString.GetString("团队赛"),LocalString.GetString("单人赛")}
    for i,v in ipairs(self.CountLabels) do
        local count=self:GetOneCount(self.m_ChuZhanInfo[i-1])
        if count==self.m_MaxPlayerInBattle[i] then
            v.text=SafeStringFormat3("%s(%d/%d)", nameArr[i], count, self.m_MaxPlayerInBattle[i])
        else
            v.text = SafeStringFormat3("%s([ff0000]%d[-]/%d)", nameArr[i], count, self.m_MaxPlayerInBattle[i])
        end
    end

end
function CLuaStarBiwuZhanDuiSettingWnd:onHintClick( go)
    g_MessageMgr:ShowMessage("StarBiwu_CHUZHAN_RULE")
end

function CLuaStarBiwuZhanDuiSettingWnd:InitItem(transform,member, index)
    local IconTexture = transform:Find("PortraitCelll_Sprite"):GetComponent(typeof(CUITexture))
    local LevelLabel = transform:Find("PortraitCelll_Sprite/Level_Label"):GetComponent(typeof(UILabel))
    local NameLabel = transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local m_LeaderFlagObj = transform:Find("Leader").gameObject
    local m_MemberIndex = index
    transform:GetComponent(typeof(UISprite)).spriteName = index % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite


    local parent=transform:Find("CheckBox")
    for i=1,6 do
        local go=parent:GetChild(i-1).gameObject
        UIEventListener.Get(go).onClick=DelegateFactory.VoidDelegate(function(p)
            -- if self.m_IsTeamLeader then
            --     self:OnCheckBoxChanged(m_MemberIndex,i-1)
            -- else
            --     g_MessageMgr:ShowMessage("Qmpk_No_ZhanDui_Operate_Power")
            -- end
            self:OnClickCheckBox(m_MemberIndex,i-1)
        end)
    end

    local pname = CUICommonDef.GetPortraitName(member.m_Class, member.m_Gender, -1)
    IconTexture:LoadNPCPortrait(pname, false)
    LevelLabel.text = tostring(member.m_Grade)
    if self.m_MainPlayerId==member.m_Id then
        NameLabel.text = SafeStringFormat3("[c][00ff00]%s[-][/c]",member.m_Name)
    else
        NameLabel.text = member.m_Name
    end

    m_LeaderFlagObj:SetActive(index == 0)

end
function CLuaStarBiwuZhanDuiSettingWnd:SetCheckBox(transform,index,selected)
    local parent=transform:Find("CheckBox")
    parent:GetChild(index):Find("Checkmark").gameObject:SetActive(selected)
end
function CLuaStarBiwuZhanDuiSettingWnd:OnClickCheckBox(memberIndex,index)
    if self.m_IsTeamLeader then
        self:OnCheckBoxChanged(memberIndex,index)
    else
        -- eEnd        = 0,
        -- eQieCuoSai  = 1,
        -- ePiPeiSai   = 2,
        -- eJiFenSai   = 3,
        -- eTaoTaiSai  = 4,
        -- eZongJueSai = 5,
        -- eReShenSai  = 6,
        -- eMeiRiYiZhanSai = 7,
        local msgKey="Qmpk_No_ZhanDui_Operate_Power"
        if CLuaQMPKMgr.m_PlayStage == EnumQmpkPlayStage.eQieCuoSai
            or CLuaQMPKMgr.m_PlayStage == EnumQmpkPlayStage.ePiPeiSai
            or CLuaQMPKMgr.m_PlayStage == EnumQmpkPlayStage.eMeiRiYiZhanSai then
            msgKey= "Qmpk_No_Team_Operate_Power"
        end

        g_MessageMgr:ShowMessage(msgKey)
    end
end

function CLuaStarBiwuZhanDuiSettingWnd:SetNonadjustable(endIndex)
    for i = 1, 6 do
        local nonadjustable = i <= endIndex
        local titleBlock = self.transform:Find(SafeStringFormat3("ShowArea/TopArea/TitleBlock%d",i))
        local titleBlockLabel = titleBlock.transform:Find("Label"):GetComponent(typeof(UILabel))
        local countLabel = titleBlock:Find("CountLabel"):GetComponent(typeof(UILabel))
        titleBlockLabel.alpha = nonadjustable and 0.3 or 1
        countLabel.alpha = nonadjustable and 0.3 or 1
    end
    for _,go in pairs(self.m_MemberSettingItemList) do
        for i = 1, 6 do
            local nonadjustable = i <= endIndex
            local checkBox = go.transform:Find(SafeStringFormat3("CheckBox/CheckBox%d",i)):GetComponent(typeof(BoxCollider))
            local checkBoxBackground = checkBox.transform:Find("Background"):GetComponent(typeof(UISprite))
            local checkBoxMark = checkBox.transform:Find("Checkmark"):GetComponent(typeof(UISprite))
            checkBoxBackground.alpha = nonadjustable and 0.3 or 1
            checkBoxMark.alpha = nonadjustable and 0.3 or 1
            checkBox.enabled = not nonadjustable
            Extensions.SetLocalPositionZ(checkBox.transform, nonadjustable and -1 or 0)
        end
    end  
end