-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CellIndex = import "L10.UI.UITableView+CellIndex"
local CellIndexType = import "L10.UI.UITableView+CellIndexType"
local CommonDefs = import "L10.Game.CommonDefs"
local CTitleMgr = import "L10.Game.CTitleMgr"
local CTitleRowTemplate = import "L10.UI.CTitleRowTemplate"
local CTitleSectionTemplate = import "L10.UI.CTitleSectionTemplate"
local CTitleTableView = import "L10.UI.CTitleTableView"
local Title_Title = import "L10.Game.Title_Title"
local UInt32 = import "System.UInt32"
CTitleTableView.m_Init_CS2LuaHook = function (this) 

    CTitleMgr.Inst:UpdateList()
    this:LoadData(CreateFromClass(CellIndex, 0, 0, CellIndexType.Row), true)
    local rowIndex = 0 local sectionIndex = 0
    local id = 0
    if CClientMainPlayer.Inst.PlayProp.ShowTitleId ~= 0 then
        id = CClientMainPlayer.Inst.PlayProp.ShowTitleId
    elseif CClientMainPlayer.Inst.PlayProp.AttrTitleId ~= 0 then
        id = CClientMainPlayer.Inst.PlayProp.AttrTitleId
    end
    if CTitleMgr.Inst.SelectTitleId ~= 0 then
        id = CTitleMgr.Inst.SelectTitleId
    end
    if id ~= 0 then
        do
            sectionIndex = 0
            while sectionIndex < CTitleMgr.Inst.SectionList.Count do
                local hasFound = false
                do
                    rowIndex = 0
                    while rowIndex < CTitleMgr.Inst.SectionList[sectionIndex].RowList.Count do
                        if CTitleMgr.Inst.SectionList[sectionIndex].RowList[rowIndex].TitleID == id then
                            hasFound = true
                            break
                        end
                        rowIndex = rowIndex + 1
                    end
                end
                if hasFound then
                    break
                end
                sectionIndex = sectionIndex + 1
            end
        end
    end
    this:SetSectionSelected(CreateFromClass(CellIndex, sectionIndex, rowIndex, CellIndexType.Section))
    this:SetRowSelected(CreateFromClass(CellIndex, sectionIndex, rowIndex, CellIndexType.Row))
end
CTitleTableView.m_RefreshCellForRowAtIndex_CS2LuaHook = function (this, cell, index) 

    if index.type == CellIndexType.Section then
        CommonDefs.GetComponent_GameObject_Type(cell, typeof(CTitleSectionTemplate)):Init(CTitleMgr.Inst.SectionList[index.section].SectionName)
    else
        local baseInfo = CTitleMgr.Inst.SectionList[index.section].RowList[index.row]
        local title = Title_Title.GetData(baseInfo.TitleID)
        if title.IsDynamicName == 1 then
            if CommonDefs.DictContains(CClientMainPlayer.Inst.PlayProp.DynamicTitleNames, typeof(UInt32), title.ID) then
                CommonDefs.GetComponent_GameObject_Type(cell, typeof(CTitleRowTemplate)):Init(baseInfo.TitleID,
                        LocalString.TranslateAndFormatText(CommonDefs.DictGetValue(CClientMainPlayer.Inst.PlayProp.DynamicTitleNames, typeof(UInt32), title.ID)))
            end
        else
            CommonDefs.GetComponent_GameObject_Type(cell, typeof(CTitleRowTemplate)):Init(baseInfo.TitleID, title.Name)
        end
    end
end
