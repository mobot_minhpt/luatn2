require("common/common_include")

local CBaseWnd = import "L10.UI.CBaseWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local UILabel = import "UILabel"
local LocalString = import "LocalString"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local CUITexture = import "L10.UI.CUITexture"
local CButton = import "L10.UI.CButton"
local CItemMgr = import "L10.Game.CItemMgr"
local Item_Item = import "L10.Game.Item_Item"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CUIFx = import "L10.UI.CUIFx"

LuaCityWarBiaoCheResSubmitWnd = class()
RegistClassMember(LuaCityWarBiaoCheResSubmitWnd,"m_CloseBtn")

RegistClassMember(LuaCityWarBiaoCheResSubmitWnd, "m_BiaoCheNameLabel")
RegistClassMember(LuaCityWarBiaoCheResSubmitWnd, "m_BiaoCheInfoItem")
RegistClassMember(LuaCityWarBiaoCheResSubmitWnd, "m_BiaoCheMsgLabel")

RegistClassMember(LuaCityWarBiaoCheResSubmitWnd, "m_ItemIcon")
RegistClassMember(LuaCityWarBiaoCheResSubmitWnd, "m_ItemAmountLabel")
RegistClassMember(LuaCityWarBiaoCheResSubmitWnd, "m_ItemNameLabel")
RegistClassMember(LuaCityWarBiaoCheResSubmitWnd, "m_SubmitBtn")
RegistClassMember(LuaCityWarBiaoCheResSubmitWnd, "m_SubmitFx")

RegistClassMember(LuaCityWarBiaoCheResSubmitWnd, "m_NpcEngineId")
RegistClassMember(LuaCityWarBiaoCheResSubmitWnd, "m_BiaoCheMapId")
RegistClassMember(LuaCityWarBiaoCheResSubmitWnd, "m_BiaoCheProgress")
RegistClassMember(LuaCityWarBiaoCheResSubmitWnd, "m_BiaoCheStatus")
RegistClassMember(LuaCityWarBiaoCheResSubmitWnd, "m_BiaoCheCloseTime")
RegistClassMember(LuaCityWarBiaoCheResSubmitWnd, "m_ItemTemplateId")

function LuaCityWarBiaoCheResSubmitWnd:Awake()
    
end

function LuaCityWarBiaoCheResSubmitWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaCityWarBiaoCheResSubmitWnd:Init()

	self.m_CloseBtn = self.transform:Find("Wnd_Bg_Secondary_2/CloseButton").gameObject
	CommonDefs.AddOnClickListener(self.m_CloseBtn, DelegateFactory.Action_GameObject(function(go) self:Close() end), false)

	self.m_BiaoCheNameLabel = self.transform:Find("Anchor/BiaoChe/NameLabel"):GetComponent(typeof(UILabel))
	self.m_BiaoCheInfoItem = self.transform:Find("Anchor/BiaoChe/Content/Info"):GetComponent(typeof(CCommonLuaScript))
	self.m_BiaoCheMsgLabel = self.transform:Find("Anchor/BiaoChe/Content/MsgLabel"):GetComponent(typeof(UILabel))

	self.m_ItemIcon = self.transform:Find("Anchor/Item/ItemCell/IconTexture"):GetComponent(typeof(CUITexture))
	self.m_ItemAmountLabel = self.transform:Find("Anchor/Item/ItemCell/AmountLabel"):GetComponent(typeof(UILabel))
	self.m_ItemNameLabel = self.transform:Find("Anchor/Item/ItemCell/NameLabel"):GetComponent(typeof(UILabel))
	self.m_SubmitBtn = self.transform:Find("Anchor/SubmitButton"):GetComponent(typeof(CButton))
	self.m_SubmitFx = self.transform:Find("Anchor/Fx"):GetComponent(typeof(CUIFx))

	CommonDefs.AddOnClickListener(self.m_ItemIcon.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnItemIconClick() end), false)
	CommonDefs.AddOnClickListener(self.m_SubmitBtn.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnSubmitButtonClick() end), false)

	if not CLuaCityWarMgr.BiaoCheResSubmitInfo then
		self:Close()
		return
	end

	self.m_NpcEngineId = CLuaCityWarMgr.BiaoCheResSubmitInfo.npcEngineId
	self.m_BiaoCheMapId = CLuaCityWarMgr.BiaoCheResSubmitInfo.mapId
	self.m_BiaoCheProgress = CLuaCityWarMgr.BiaoCheResSubmitInfo.biaoCheProgress
	self.m_BiaoCheStatus = CLuaCityWarMgr.BiaoCheResSubmitInfo.biaoCheStatus
	self.m_BiaoCheCloseTime = CLuaCityWarMgr.BiaoCheResSubmitInfo.biaoCheCloseTime
	self.m_ItemTemplateId = CLuaCityWarMgr.BiaoCheResSubmitInfo.resourceId
	self:ShowInfo()
end

function LuaCityWarBiaoCheResSubmitWnd:ShowInfo()
	local map = self.m_BiaoCheMapId and CityWar_Map.GetData(self.m_BiaoCheMapId)
	if map then
		self.m_BiaoCheNameLabel.text = SafeStringFormat3(LocalString.GetString("镖车·%s"), map.Name)
		self.m_BiaoCheInfoItem.m_LuaSelf:Init(self.m_BiaoCheMapId, self.m_BiaoCheProgress, self.m_BiaoCheStatus, self.m_BiaoCheCloseTime)
		self.m_BiaoCheMsgLabel.text = CLuaCityWarMgr.GetMsgByStatusAndProgress(self.m_BiaoCheStatus, self.m_BiaoCheProgress)
	
	else
		self.m_BiaoCheNameLabel.text = LocalString.GetString("镖车·未设置")
		self.m_BiaoCheInfoItem.m_LuaSelf:Init(self.m_BiaoCheMapId, self.m_BiaoCheProgress, self.m_BiaoCheStatus, self.m_BiaoCheCloseTime)
		self.m_BiaoCheMsgLabel.text = nil
	end

	if self.m_ItemTemplateId then
		local amount = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Task, self.m_ItemTemplateId)
		local data = Item_Item.GetData(self.m_ItemTemplateId)
		local icon = data.Icon
		local name = data.Name
		self.m_ItemIcon:LoadMaterial(icon)
		self.m_ItemAmountLabel.text = tostring(amount)
		self.m_ItemNameLabel.text = name
		self.m_SubmitBtn.Enabled = (amount>0 and self.m_BiaoCheStatus == EnumBiaoCheStatus.eSubmitRes)
	else
		self.m_ItemIcon:Clear()
		self.m_ItemAmountLabel = ""
		self.m_ItemNameLabel.text = ""
		self.m_SubmitBtn.Enabled = false
	end
end


function LuaCityWarBiaoCheResSubmitWnd:OnItemIconClick()
	if self.m_ItemTemplateId then
		CItemInfoMgr.ShowLinkItemTemplateInfo(self.m_ItemTemplateId, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
	end
end

function LuaCityWarBiaoCheResSubmitWnd:OnSubmitButtonClick()
	if not self.m_ItemTemplateId then return end
	if not self.m_NpcEngineId then return end
	Gac2Gas.QuerySubmitResToBiaoChe(self.m_NpcEngineId)
end

function LuaCityWarBiaoCheResSubmitWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateSubmitBiaoCheWnd", self, "UpdateSubmitBiaoCheWnd")
end

function LuaCityWarBiaoCheResSubmitWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateSubmitBiaoCheWnd", self, "UpdateSubmitBiaoCheWnd")
end

function LuaCityWarBiaoCheResSubmitWnd:UpdateSubmitBiaoCheWnd(mapId, biaoCheStatus, biaoCheProgress)
	if self.m_BiaoCheMapId and self.m_BiaoCheMapId == mapId then
		self.m_BiaoCheStatus = biaoCheStatus
		self.m_BiaoCheProgress = biaoCheProgress
		self:ShowInfo()

		self.m_SubmitFx:LoadFx("fx/ui/prefab/UI_zhuangtianjiemian.prefab")
	end
end

function LuaCityWarBiaoCheResSubmitWnd:OnDestroy()
	--TODO
end

return LuaCityWarBiaoCheResSubmitWnd
