local QnNewSlider = import "L10.UI.QnNewSlider"
local CUITexture = import "L10.UI.CUITexture"
local UITexture = import "UITexture"
local TouchPhase = import "UnityEngine.TouchPhase"
local CommonDefs = import "L10.Game.CommonDefs"
local Input = import "UnityEngine.Input"
local Vector3 = import "UnityEngine.Vector3"
local UITabBar = import "L10.UI.UITabBar"
local CButton = import "L10.UI.CButton"
local Color = import "UnityEngine.Color"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CPlayerDataMgr = import "L10.Game.CPlayerDataMgr"
local UILongPressButton = import "L10.UI.UILongPressButton"
local CItemInfoMgr=import "L10.UI.CItemInfoMgr"
local CItem=import "L10.Game.CItem"
local PopupMenuItemData=import "L10.UI.PopupMenuItemData"
local CPopupMenuInfoMgr=import "L10.UI.CPopupMenuInfoMgr"
local AlignType2=import "L10.UI.CPopupMenuInfoMgr+AlignType"

CLuaYanHuaLiBaoEditorWnd = class()

RegistChildComponent(CLuaYanHuaLiBaoEditorWnd, "m_ClearBtn","ClearBtn", GameObject)
RegistChildComponent(CLuaYanHuaLiBaoEditorWnd, "m_LiBaoTable","LiBaoTable", UITabBar)
RegistChildComponent(CLuaYanHuaLiBaoEditorWnd, "m_CloneItem","CloneItem", GameObject)
RegistChildComponent(CLuaYanHuaLiBaoEditorWnd, "m_NormalYanHua","NormalYanHua", UITabBar)
RegistChildComponent(CLuaYanHuaLiBaoEditorWnd, "m_CustomYanHua","CustomYanHua", UITabBar)
RegistChildComponent(CLuaYanHuaLiBaoEditorWnd, "m_RadiusSlider","RadiusSlider", QnNewSlider)
RegistChildComponent(CLuaYanHuaLiBaoEditorWnd, "m_ColorTab","ColorTab", UITabBar)
RegistChildComponent(CLuaYanHuaLiBaoEditorWnd, "m_ColorView","ColorView", GameObject)
RegistChildComponent(CLuaYanHuaLiBaoEditorWnd, "m_LightBtn","LightBtn", GameObject)
RegistChildComponent(CLuaYanHuaLiBaoEditorWnd, "m_TipBtn","TipButton", GameObject)
RegistChildComponent(CLuaYanHuaLiBaoEditorWnd, "m_CloseButton","CloseButton", GameObject)
RegistChildComponent(CLuaYanHuaLiBaoEditorWnd, "m_HintLabel","HintLabel", GameObject)
RegistChildComponent(CLuaYanHuaLiBaoEditorWnd, "m_InfoWnd","InfoWnd", GameObject)
RegistChildComponent(CLuaYanHuaLiBaoEditorWnd, "m_PreviewBtn","PreviewBtn", GameObject)
RegistChildComponent(CLuaYanHuaLiBaoEditorWnd, "m_CustomDes","CustomDes", UILabel)
RegistChildComponent(CLuaYanHuaLiBaoEditorWnd, "m_CustomIcon","CustomIcon", CUITexture)

RegistClassMember(CLuaYanHuaLiBaoEditorWnd,"m_SelectIndexInLiBaoTbl")
RegistClassMember(CLuaYanHuaLiBaoEditorWnd,"m_DragingLiBaoIdx")
RegistClassMember(CLuaYanHuaLiBaoEditorWnd,"m_DragingNormalIdx")
RegistClassMember(CLuaYanHuaLiBaoEditorWnd,"m_DragingCustomIdx")

RegistClassMember(CLuaYanHuaLiBaoEditorWnd,"m_NormalYanHuaStore")

RegistClassMember(CLuaYanHuaLiBaoEditorWnd,"m_NormalIDList")
RegistClassMember(CLuaYanHuaLiBaoEditorWnd,"m_TouFangNormalList")
RegistClassMember(CLuaYanHuaLiBaoEditorWnd,"m_CustomItemList")
RegistClassMember(CLuaYanHuaLiBaoEditorWnd,"m_AddCanvasIndexList")
RegistClassMember(CLuaYanHuaLiBaoEditorWnd,"m_AddCustomUsedList")
RegistClassMember(CLuaYanHuaLiBaoEditorWnd,"m_HasCustomAuditRefrused")--有自定义烟花未通过审核 

function CLuaYanHuaLiBaoEditorWnd:OnEnable()
    g_ScriptEvent:AddListener("ApplyCustomCanvas",self,"onApplyCustomCanvas")
end

function CLuaYanHuaLiBaoEditorWnd:OnDisable()
    g_ScriptEvent:RemoveListener("ApplyCustomCanvas",self,"onApplyCustomCanvas")
end

function CLuaYanHuaLiBaoEditorWnd:onApplyCustomCanvas()
    self:Init()
end

function CLuaYanHuaLiBaoEditorWnd:Awake()
    UIEventListener.Get(self.m_CloseButton).onClick = DelegateFactory.VoidDelegate(function (p)
       self:OnSaveYanHuaLightList()
       CUIManager.CloseUI(CLuaUIResources.YanHuaLiBaoEditorWnd)
    end)

    UIEventListener.Get(self.m_LightBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("YanHuaEditor_Light_Comfirm"),
            DelegateFactory.Action(function ()
                self:OnClickLightBtn()
            end),
            nil,
        LocalString.GetString("确定"), LocalString.GetString("取消"), false)
    end)

    UIEventListener.Get(self.m_TipBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        g_MessageMgr:ShowMessage("YanHua_Editor_Tip")
    end)

    if CommonDefs.IS_VN_CLIENT then
		self.m_CustomYanHua.gameObject:SetActive(false)
        self.transform:Find("Anchor/Right/TitleLabel (1)").gameObject:SetActive(false)
	end
end

function CLuaYanHuaLiBaoEditorWnd:Init()
    self.m_NormalYanHuaStore = {}
    self.m_DragingLiBaoIdx = -1
    self.m_DragingNormalIdx = -1
    self.m_DragingCustomIdx = -1
    self.m_NormalIDList = {}
    self.m_TouFangNormalList = {}
    self.m_CustomItemList = {}
    CLuaYanHuaEditorMgr.AddCanvasIndexList = {}
    self.m_AddCustomUsedList = {}
    self.m_CloneItem:SetActive(false)
    self.m_HasCustomAuditRefrused = false
    UIEventListener.Get(self.m_ClearBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("YanHuaEditor_Clear_ComFirm"), 
            DelegateFactory.Action(function () 
                local t = {}
                t.type = "empty"
                CLuaYanHuaEditorMgr.YanHuaLightListTbl = {}
                for i=1,20,1 do
                    table.insert(CLuaYanHuaEditorMgr.YanHuaLightListTbl,t)
                end
                self:InitYanHuaLiBaoTable()
            end), 
            nil, LocalString.GetString("清空"), LocalString.GetString("取消"), false)
    end)

    self.m_InfoWnd:SetActive(false)
    self:OnLoadLocalYanHuaLightList()
    self.m_LiBaoTable.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
        self:OnLiBaoTabChange(index)
    end)

    self.m_NormalYanHua.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
        self:OnNormalTabChange(index)
    end)

    self.m_CustomYanHua.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
        self:OnCustomTabChange(index)
    end)

    self.m_ColorTab.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
        self:OnColorTabChange(index)
    end)
    self:InitNormalYanHuaList()
    --self:InitYanHuaLiBaoTable()
    self:InitCustomYanHuaList()
    self:InitYanHuaLiBaoTable()

    self.m_ColorView:SetActive(false)
    self.m_RadiusSlider.gameObject:SetActive(false)
    self.m_HintLabel:SetActive(true)

    self.m_RadiusSlider.m_Value = self.m_RadiusSlider.m_Value*(5-0.5)+0.5
    self.m_RadiusSlider.OnValueChanged = DelegateFactory.Action_float(function(value)
        if value == 0 then
            value = 0.1
        end
        local info = CLuaYanHuaEditorMgr.YanHuaLightListTbl[self.m_SelectIndexInLiBaoTbl]
        info.size = value*CLuaYanHuaEditorMgr.YanHuaMaxSize
        self:InitYanHuaLiBaoTable()
	end)
end

function CLuaYanHuaLiBaoEditorWnd:InitYanHuaLiBaoTable()
    for i=1,#CLuaYanHuaEditorMgr.YanHuaLightListTbl,1 do
        local item = self.m_LiBaoTable.transform:GetChild(i-1)
        local icon = item:Find("Icon"):GetComponent(typeof(CUITexture))
        local tex = item:Find("Icon"):GetComponent(typeof(UITexture))
        local info = CLuaYanHuaEditorMgr.YanHuaLightListTbl[i]
        local iconSize = CLuaYanHuaEditorMgr.MaxIconSize
        icon.mainTexture = nil
        icon.material = nil
        if info and next(info) and info.type ~= "empty" then
            local type = info.type 
            if type == "normal" then
                local data = YanHua_Default.GetData(info.normalId)
                local path = data.Prefab
                icon.mainTexture = nil
                icon:LoadMaterial(path)
            elseif type == "custom" then
                local customIndex = tonumber(info.customIndex)
                local customItem = self.m_CustomYanHua.transform:GetChild(customIndex-1)

                local picIndex = CLuaYanHuaEditorMgr.AddCanvasIndexList[customIndex]
                if picIndex ~= -1 then
                    tex.material = nil
                    tex.mainTexture = CLuaYanHuaEditorMgr.MyYanHuaPicData[picIndex].texture
                end
                self.m_AddCustomUsedList[customIndex] = true
                --local yanhua = self.m_CustomYanHua.transform:GetChild(customIndex-1)
                --yanhua:Find("Used").gameObject:SetActive(true)
            end

            if info.size then
                iconSize = CLuaYanHuaEditorMgr.MinIconSize + (iconSize - CLuaYanHuaEditorMgr.MinIconSize)*(info.size/CLuaYanHuaEditorMgr.YanHuaMaxSize)
                tex.width = iconSize
                tex.height = iconSize
            end

            if info.colorIndex then
                tex.color = CLuaYanHuaEditorMgr.Colors[info.colorIndex]
            else
                tex.color = Color.white
            end
        else
            icon:LoadMaterial(nil)
        end

        UIEventListener.Get(item.gameObject).onDragStart = DelegateFactory.VoidDelegate(function(go)
            self:OnDragLiBaoItemStart(i)
        end)

        local lbtn = item:GetComponent(typeof(UILongPressButton))
        lbtn.OnLongPressDelegate = DelegateFactory.Action(function ()
            self:OnItemLongPressed(i)
        end)
    end
end

function CLuaYanHuaLiBaoEditorWnd:OnItemLongPressed(i)
    local info = CLuaYanHuaEditorMgr.YanHuaLightListTbl[i]
    if next (info) then
        if info.type == "normal" then
            local data = YanHua_Default.GetData(info.normalId)
            local itemid = data.ItemID
            CLuaYanHuaEditorMgr.CurPreViewLiBaoIndex = i
            self:InitCustonInfoWnd(itemid)
        elseif info.type == "custom" then
            CLuaYanHuaEditorMgr.CurPreViewLiBaoIndex = i
            local customIndex = tonumber(info.customIndex)
            CLuaYanHuaEditorMgr.CurPreviewCanvasIndex = tonumber(CLuaYanHuaEditorMgr.AddCanvasIndexList[customIndex])
            self:InitCustonInfoWnd()
        end
    end
end

function CLuaYanHuaLiBaoEditorWnd:InitCustonInfoWnd(itemid)
    self.m_InfoWnd:SetActive(true)
    if not itemid then
        self.m_CustomIcon.material = nil
        self.m_CustomIcon.mainTexture = CLuaYanHuaEditorMgr.MyYanHuaPicData[CLuaYanHuaEditorMgr.CurPreviewCanvasIndex].texture
        self.m_CustomDes.text = CUICommonDef.TranslateToNGUIText(YanHua_Setting.GetData().CustomYanHuaItemDes)
        local nameLabel = self.transform:Find("InfoWnd/InfoWnd2/Header/ItemNameLabel"):GetComponent(typeof(UILabel))
        local canvas = CLuaYanHuaEditorMgr.CanvasList[CLuaYanHuaEditorMgr.CurPreviewCanvasIndex]
        local name = canvas.name
        if not name or name == "" then
            name = LocalString.GetString("自定义烟花")
        end
        nameLabel.text = name
        --暂时禁掉自定义烟花的预览功能
        self.m_PreviewBtn:SetActive(false)
    else
        local data = Item_Item.GetData(itemid)
        local iconPath = data.Icon
        self.m_CustomIcon.mainTexture = nil
        self.m_CustomIcon:LoadMaterial(iconPath)
        self.m_CustomDes.text = CUICommonDef.TranslateToNGUIText(CItem.GetItemDescription(itemid,true))
        local nameLabel = self.transform:Find("InfoWnd/InfoWnd2/Header/ItemNameLabel"):GetComponent(typeof(UILabel))
        nameLabel.text = data.Name  
        self.m_PreviewBtn:SetActive(true)      
    end
    
    UIEventListener.Get(self.m_PreviewBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        CLuaYanHuaEditorMgr.PreviewNormalYanhua()
        self.m_InfoWnd:SetActive(false)
    end)
end

function CLuaYanHuaLiBaoEditorWnd:InitInitDetail(index)
    local info = CLuaYanHuaEditorMgr.YanHuaLightListTbl[index]
    if info and next(info) then
        local type = info.type 
        if type == "normal" then
            local data = YanHua_Default.GetData(info.normalId)
            self.m_ColorView:SetActive(data.ChangeColor == 1)
            self.m_RadiusSlider.gameObject:SetActive(data.ChangeSize == 1)
            if data.ChangeSize == 1 then
                local size = info.size
                if not size then size = 1 end
                self.m_RadiusSlider.m_Value = size / CLuaYanHuaEditorMgr.YanHuaMaxSize
            end
            if data.ChangeColor == 1 then
                local colorIndex = info.colorIndex
                if colorIndex then
                    self.m_ColorTab:ChangeTab(colorIndex-1,false)
                else
                    for i=0,self.m_ColorTab.transform.childCount-1,1 do
                        local btn = self.m_ColorTab.transform:GetChild(i):GetComponent(typeof(CButton))
                        btn.Selected = false
                        btn.transform:Find("Selected").gameObject:SetActive(false)
                    end
                end
            end
        elseif type == "custom" then
            self.m_ColorView:SetActive(false)
            self.m_RadiusSlider.gameObject:SetActive(true)
            local size = info.size
            if not size then size = 1 end
            self.m_RadiusSlider.m_Value = size / CLuaYanHuaEditorMgr.YanHuaMaxSize           
        end       
    else
        self.m_ColorView:SetActive(false)
        self.m_RadiusSlider.gameObject:SetActive(false)
    end
end


function CLuaYanHuaLiBaoEditorWnd:InitNormalYanHuaList()
    --Init view 
    local i = -1
    local childCount = self.m_NormalYanHua.transform.childCount

    YanHua_Default.Foreach(function(k, v)
        if v.IsTouFang == 1 then
            table.insert(self.m_TouFangNormalList,k)
        end
    end)

    for i=0,self.m_NormalYanHua.transform.childCount-1,1 do
        local item = self.m_NormalYanHua.transform:GetChild(i)
        local icon = item:Find("Icon"):GetComponent(typeof(CUITexture))
        local id = self.m_TouFangNormalList[i+1]
        local data = YanHua_Default.GetData(id)
        icon:LoadMaterial(data.Prefab)

        UIEventListener.Get(item.gameObject).onDragStart = DelegateFactory.VoidDelegate(function(go)
            self:OnDragNormalYanHuaItemStart(i+1)
        end)
    end
end

function CLuaYanHuaLiBaoEditorWnd:OnClickCustomItem(index)
    local go = self.m_CustomItemList[index]
    local canvasIndex = CLuaYanHuaEditorMgr.AddCanvasIndexList[index]
     if canvasIndex == -1 then
        self:OnSaveYanHuaLightList()
        CLuaYanHuaEditorMgr.SelectCustomIndex = index
        CUIManager.ShowUI(CLuaUIResources.YanHuaDesignWnd)
     else
        local canvas = CLuaYanHuaEditorMgr.CanvasList[canvasIndex]
        local usedCount = canvas.usedCount
        if not usedCount then  usedCount = 0 end
        usedCount = tonumber(usedCount)

        local function SelectAction(idx)                   
            if idx==0 then
                MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("YanHuaEditor_Remove_ClearList_Comfirm"),
                DelegateFactory.Action(function ()
                    CLuaYanHuaEditorMgr.AddCanvasIndexList[index] = -1
                    local storeIndex = canvas.storeIndex
                    local newStr = ""
                    for ii in string.gmatch(storeIndex,"(%d+),?" ) do
                        if ii ~= tostring(index) then
                            newStr = newStr..ii..","
                        end
                    end
                    canvas.storeIndex = newStr
                    CLuaYanHuaEditorMgr.AddedCanvasCount = CLuaYanHuaEditorMgr.AddedCanvasCount - 1
                    self:ReFreshCustomItem(index)
                    CLuaYanHuaEditorMgr.OnSaveYanHuaDesignData()
                    --从燃放列表移除
                    for key,yanhua in ipairs(CLuaYanHuaEditorMgr.YanHuaLightListTbl) do
                        if yanhua.customIndex == index then
                            yanhua.type = "empty"
                        end
                    end
                    self:InitYanHuaLiBaoTable()
                end),
                nil,
                LocalString.GetString("确定"), LocalString.GetString("取消"), false)
            end       
        end
        local selectShareItem=DelegateFactory.Action_int(SelectAction)
        local t={}
        local item=PopupMenuItemData(LocalString.GetString("移除"),selectShareItem,false,nil)
        table.insert(t, item)
        local array=Table2Array(t,MakeArrayClass(PopupMenuItemData))
        CPopupMenuInfoMgr.ShowPopupMenu(array, go, AlignType2.Top,1,nil,600,true,266)
     end
end

function CLuaYanHuaLiBaoEditorWnd:InitCustomYanHuaList()
    CLuaYanHuaEditorMgr.AddedCanvasCount = 0
    local addList = {}
    for j=1,4,1 do
        if not self.m_CustomItemList[j] then
            local item = self.m_CustomYanHua.transform:GetChild(j-1)
            --item:Find("Used").gameObject:SetActive(false)
            item:Find("Tag").gameObject:SetActive(false)
            item:Find("AddSprite").gameObject:SetActive(true)
            item:Find("Icon"):GetComponent(typeof(UITexture)).mainTexture = nil
            table.insert(self.m_CustomItemList,item)
            table.insert(CLuaYanHuaEditorMgr.AddCanvasIndexList,-1)
            table.insert(self.m_AddCustomUsedList,false)
            UIEventListener.Get(item.gameObject).onDragStart = DelegateFactory.VoidDelegate(function(go)
                self:OnDragCustomYanHuaItemStart(j)
            end)
            UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
                self:OnClickCustomItem(j)
            end)
        end
    end
    for i,canvas in ipairs(CLuaYanHuaEditorMgr.CanvasList) do
        if canvas and type(canvas) == "table" and next(canvas) then
            local storeIndex = canvas.storeIndex
            local t = {}
            if storeIndex then
                for index in string.gmatch(storeIndex,"(%d+),?" ) do
                    index = tonumber(index)
                    local item = self.m_CustomItemList[index]
                    CLuaYanHuaEditorMgr.AddCanvasIndexList[index] = i
                    CLuaYanHuaEditorMgr.AddedCanvasCount = CLuaYanHuaEditorMgr.AddedCanvasCount + 1
                    if item then
                        local icon = item:Find("Icon"):GetComponent(typeof(UITexture))
                        icon.mainTexture = nil
                        local addSpriteGo = item:Find("AddSprite").gameObject
                        --local usedGo = item:Find("Used").gameObject
                        local tagLabel = item:Find("Tag"):GetComponent(typeof(UILabel))
                        addSpriteGo:SetActive(false)
                        local shengheData = CLuaYanHuaEditorMgr.MyYanHuaPicData[i]
                        if shengheData and type(shengheData)=="table" and next(shengheData) and shengheData.id == tonumber(canvas.picId) then
                            tagLabel.gameObject:SetActive(true)
                            local audit = tonumber(shengheData.auditStatus)
                            tagLabel.text = CLuaYanHuaEditorMgr.AuditStatusText[audit+1]
                        else
                            tagLabel.gameObject:SetActive(false)
                        end
                        local type = canvas.type
                        if type == CLuaYanHuaEditorMgr.EnumCanvasStatus.ePixel then
                            local pixelItemColorList = canvas.pixelItemColorList
                            icon.mainTexture = CLuaYanHuaEditorMgr.ColorList2Pic(pixelItemColorList)
                        elseif type == CLuaYanHuaEditorMgr.EnumCanvasStatus.eFree then
                            local picIndex = tonumber(i)
                            local tex = CLuaYanHuaEditorMgr.MyYanHuaPicData[picIndex].texture
                            icon.mainTexture = tex
                        end
                        --usedGo:SetActive(self.m_AddCustomUsedList[index]==true)
                    end
                end
            end
        end
    end
end

function CLuaYanHuaLiBaoEditorWnd:ReFreshCustomItem(index)
    local item = self.m_CustomItemList[index]
    if not item then return end

    local icon = item:Find("Icon"):GetComponent(typeof(UITexture))
    local addSpriteGo = item:Find("AddSprite").gameObject
    --local usedGo = item:Find("Used").gameObject
    local tagLabel = item:Find("Tag"):GetComponent(typeof(UILabel))

    local canvasIndex = CLuaYanHuaEditorMgr.AddCanvasIndexList[index]
    if canvasIndex == -1 then
        icon.mainTexture = nil
        addSpriteGo:SetActive(true)
        tagLabel.gameObject:SetActive(false)
    else
        --usedGo:SetActive(self.m_AddCustomUsedList[index]==true)
    end
end

function CLuaYanHuaLiBaoEditorWnd:OnLiBaoTabChange(index)
    if self.m_HintLabel.activeSelf then
        self.m_HintLabel:SetActive(false)
    end
    self.m_SelectIndexInLiBaoTbl = index + 1
    self:InitInitDetail(index+1)
end

function CLuaYanHuaLiBaoEditorWnd:OnNormalTabChange(index)
end

function CLuaYanHuaLiBaoEditorWnd:OnCustomTabChange(index)
end

function CLuaYanHuaLiBaoEditorWnd:OnColorTabChange(index)
    local colorIndex = index + 1
    local info = CLuaYanHuaEditorMgr.YanHuaLightListTbl[self.m_SelectIndexInLiBaoTbl]
    info.colorIndex = colorIndex
    for i=0,#CLuaYanHuaEditorMgr.Colors-1,1 do
        local btn = self.m_ColorTab.transform:GetChild(i)
        btn:Find("Selected").gameObject:SetActive(i==index)
    end

    self:InitYanHuaLiBaoTable()
end

function CLuaYanHuaLiBaoEditorWnd:OnYanHuaRadiusChange()
end


function CLuaYanHuaLiBaoEditorWnd:Update()
    self:ClickThroughToClose()
    if nil == self.m_LastDragPos then
        self.m_LastDragPos = Vector3.zero
    end
    if not self.m_IsDragging then
        return
    end
    if CommonDefs.IsInMobileDevice() then
        for i = 0,Input.touchCount - 1 do
            local touch = Input.GetTouch(i)
            if touch.fingerId == self.m_FingerIndex then
                if touch.phase ~= TouchPhase.Ended and touch.phase ~= TouchPhase.Canceled then
                    if touch.phase ~= TouchPhase.Stationary then
                        local pos = touch.position
                        self.m_LastDragPos = Vector3(pos.x, pos.y, 0)
                        self.m_CloneItem.transform.position = CUIManager.UIMainCamera:ScreenToWorldPoint(self.m_LastDragPos)
                    end
                    return
                else
                    break
                end
            end
        end
    else
        if Input.GetMouseButton(0) then
            self.m_CloneItem.transform.position = CUIManager.UIMainCamera:ScreenToWorldPoint(Input.mousePosition)
            self.m_LastDragPos = Input.mousePosition

            if not Input.GetMouseButtonUp(0) then
                return
            end
        end
    end
    self.m_CloneItem:SetActive(false)

    local hoveredObject = CUICommonDef.SelectedUIWithRacast
    if hoveredObject and self.m_DragingLiBaoIdx ~= -1 then
        if string.find(hoveredObject.name,"YanHuaItemTemplate") then
           -- print("exchange")
            self.m_InfoWnd:SetActive(false)
            self:ExchangeLiBaoItem(self.m_DragingLiBaoIdx,hoveredObject.transform:GetSiblingIndex()+1)
        else

            self.m_InfoWnd:SetActive(false)
            local old = CLuaYanHuaEditorMgr.YanHuaLightListTbl[self.m_DragingLiBaoIdx]
            local oldCustomIndex = nil
            if old.type == "custom" then
                local canvasIndex =  CLuaYanHuaEditorMgr.AddCanvasIndexList[old.customIndex]
                local canvas = CLuaYanHuaEditorMgr.CanvasList[canvasIndex]
                if canvas.usedCount then
                    canvas.usedCount = tonumber(canvas.usedCount) - 1
                else
                    canvas.usedCount = 0
                end
                self.m_AddCustomUsedList[old.customIndex] = false
                self:ReFreshCustomItem(old.customIndex)
            end
            local tt = {}
            tt.type = "empty"
            CLuaYanHuaEditorMgr.YanHuaLightListTbl[self.m_DragingLiBaoIdx] = tt
            self:InitYanHuaLiBaoTable()
            self.m_LiBaoTable:ChangeTab(self.m_DragingLiBaoIdx-1,false)
        end
    elseif hoveredObject and self.m_DragingNormalIdx ~= -1 then
        if string.find(hoveredObject.name,"YanHuaItemTemplate") then
            self:SetNormalAtLiBaoIndex(hoveredObject.transform:GetSiblingIndex()+1,self.m_DragingNormalIdx)
        end
    elseif hoveredObject and self.m_DragingCustomIdx ~= -1 then
        if string.find(hoveredObject.name,"YanHuaItemTemplate") then
            self:SetCustomAtLiBaoIndex(hoveredObject.transform:GetSiblingIndex()+1,self.m_DragingCustomIdx)
        end
    else
        --g_MessageMgr:ShowMessage("CUSTOM_STRING2")
    end

    self.m_IsDragging = false
    self.m_FingerIndex = -1
    self.m_DragingLiBaoIdx = -1
    self.m_DragingNormalIdx = -1
    self.m_DragingCustomIdx = -1
end
function CLuaYanHuaLiBaoEditorWnd:ExchangeLiBaoItem(index1,index2)
    if index1 == index2 then
        return 
    end
    local data1 = CLuaYanHuaEditorMgr.YanHuaLightListTbl[index1]
    local data2 = CLuaYanHuaEditorMgr.YanHuaLightListTbl[index2]

    local new1 = {}
    for k,v in pairs(data2) do
        new1[k] = v
    end
    local new2 = {}
    for k,v in pairs(data1) do
        new2[k] = v
    end
    CLuaYanHuaEditorMgr.YanHuaLightListTbl[index1] = new1
    CLuaYanHuaEditorMgr.YanHuaLightListTbl[index2] = new2

    self:InitYanHuaLiBaoTable()
    self.m_LiBaoTable:ChangeTab(index2-1,false)
end

function CLuaYanHuaLiBaoEditorWnd:SetNormalAtLiBaoIndex(libaoIndex,normalIndex)
    local normalId = self.m_TouFangNormalList[normalIndex] 
    local data = YanHua_Default.GetData(normalId)
    local defaultSize = data.Size
    local defaultColor = data.Color
    local info = {}
    info.type = "normal"
    info.normalId = normalId
    info.size = defaultSize
    if data.ChangeColor==1 then
        info.colorIndex = defaultColor
    end

    local old = CLuaYanHuaEditorMgr.YanHuaLightListTbl[libaoIndex]
    if old and next(old) then
        local oldName = ""
        if old.type == "normal" then
            local data = YanHua_Default.GetData(old.normalId)
            oldName = Item_Item.GetData(data.ItemID).Name
            g_MessageMgr:ShowMessage("YanHuaEditor_Replace",oldName)
        elseif old.type == "custom" then
            local canvasIndex =  CLuaYanHuaEditorMgr.AddCanvasIndexList[old.customIndex]
            local canvas = CLuaYanHuaEditorMgr.CanvasList[canvasIndex]
            if canvas.usedCount then
                canvas.usedCount = tonumber(canvas.usedCount) - 1
            else
                canvas.usedCount = 0
            end
            self.m_AddCustomUsedList[old.customIndex] = false
            self:ReFreshCustomItem(old.customIndex)
            oldName = canvas.name
            if not oldName or oldName == "" then
                oldName = LocalString.GetString("自定义烟花")
            end
            g_MessageMgr:ShowMessage("YanHuaEditor_Replace",oldName)
        end
    end

    CLuaYanHuaEditorMgr.YanHuaLightListTbl[libaoIndex] = info
    self:InitYanHuaLiBaoTable()
    self.m_LiBaoTable:ChangeTab(libaoIndex-1,false)
end

function CLuaYanHuaLiBaoEditorWnd:SetCustomAtLiBaoIndex(libaoIndex,customIndex)
    local old = CLuaYanHuaEditorMgr.YanHuaLightListTbl[libaoIndex]
    if (next(old)) then
        local oldName = ""
        if old.type == "normal" then
            local data = YanHua_Default.GetData(old.normalId)
            oldName = Item_Item.GetData(data.ItemID).Name
            g_MessageMgr:ShowMessage("YanHuaEditor_Replace",oldName)
        elseif old.type == "custom" then
            local canvasIndex =  CLuaYanHuaEditorMgr.AddCanvasIndexList[old.customIndex]
            local canvas = CLuaYanHuaEditorMgr.CanvasList[canvasIndex]
            if canvas.usedCount then
                canvas.usedCount = tonumber(canvas.usedCount) - 1
            else
                canvas.usedCount = 0
            end
            self.m_AddCustomUsedList[old.customIndex] = false
            self:ReFreshCustomItem(old.customIndex)
            oldName = canvas.name
            if not oldName or oldName == "" then
                oldName = LocalString.GetString("自定义烟花")
            end
            g_MessageMgr:ShowMessage("YanHuaEditor_Replace",oldName)
        end
    end

    local canvasIndex =  CLuaYanHuaEditorMgr.AddCanvasIndexList[customIndex]
    local canvas = CLuaYanHuaEditorMgr.CanvasList[canvasIndex]
    local info = {}
    info.type = "custom"
    info.customUrl = canvas.url
    info.size = CLuaYanHuaEditorMgr.CustomYanhuaDefaultSize
    info.customIndex = customIndex
    if canvas.usedCount then
        canvas.usedCount = tonumber(canvas.usedCount) + 1
    else
        canvas.usedCount = 1
    end

    self.m_AddCustomUsedList[customIndex] = true
    CLuaYanHuaEditorMgr.YanHuaLightListTbl[libaoIndex] = info
    self:ReFreshCustomItem(customIndex)
    self:InitYanHuaLiBaoTable()
    self.m_LiBaoTable:ChangeTab(libaoIndex-1,false)
end
----drag
function CLuaYanHuaLiBaoEditorWnd:OnDragLiBaoItemStart(idx)
    local yanhua = CLuaYanHuaEditorMgr.YanHuaLightListTbl[idx]
    if yanhua.type == "empty" then return end
    local libaoItem = self.m_LiBaoTable.transform:GetChild(idx-1)
    local itemIcon = libaoItem:Find("Icon"):GetComponent(typeof(CUITexture))
    local icon = self.m_CloneItem.transform:Find("Icon"):GetComponent(typeof(CUITexture))
    if itemIcon.material then
        icon.mainTexture = nil
        icon.material = itemIcon.material
    else
        icon.material = nil
        icon.mainTexture = itemIcon.mainTexture
    end
    

    self.m_DragingLiBaoIdx = idx
    self.m_CloneItem:SetActive(true)
    self.m_IsDragging = true
    self.m_LastDragPos = Input.mousePosition

    if CommonDefs.IsInMobileDevice() then
        self.m_FingerIndex = Input.GetTouch(0).fingerId
        local pos = Input.GetTouch(0).position
        self.m_LastDragPos = Vector3(pos.x, pos.y, 0)
    end
end

function CLuaYanHuaLiBaoEditorWnd:OnDragNormalYanHuaItemStart(idx)
    local normalItem = self.m_NormalYanHua.transform:GetChild(idx-1)
    local itemIcon = normalItem:Find("Icon"):GetComponent(typeof(CUITexture))
    local icon = self.m_CloneItem.transform:Find("Icon"):GetComponent(typeof(CUITexture))
    icon:LoadMaterial(itemIcon.lastPath)
    self.m_DragingNormalIdx = idx
    self.m_CloneItem:SetActive(true)
    self.m_IsDragging = true
    self.m_LastDragPos = Input.mousePosition

    if CommonDefs.IsInMobileDevice() then
        self.m_FingerIndex = Input.GetTouch(0).fingerId
        local pos = Input.GetTouch(0).position
        self.m_LastDragPos = Vector3(pos.x, pos.y, 0)
    end
end

function CLuaYanHuaLiBaoEditorWnd:OnDragCustomYanHuaItemStart(idx)
    if CLuaYanHuaEditorMgr.AddCanvasIndexList[idx] == -1 then
        return 
    end
    local customItem = self.m_CustomYanHua.transform:GetChild(idx-1)
    local itemIcon = customItem:Find("Icon"):GetComponent(typeof(UITexture))
    local icon = self.m_CloneItem.transform:Find("Icon"):GetComponent(typeof(UITexture))
    icon.material = nil
    icon.mainTexture = itemIcon.mainTexture

    self.m_DragingCustomIdx = idx
    self.m_CloneItem:SetActive(true)
    self.m_IsDragging = true
    self.m_LastDragPos = Input.mousePosition

    if CommonDefs.IsInMobileDevice() then
        self.m_FingerIndex = Input.GetTouch(0).fingerId
        local pos = Input.GetTouch(0).position
        self.m_LastDragPos = Vector3(pos.x, pos.y, 0)
    end
end

function CLuaYanHuaLiBaoEditorWnd:OnSaveYanHuaLightList()
    local json = luaJson.table2json(CLuaYanHuaEditorMgr.YanHuaLightListTbl)
    CPlayerDataMgr.Inst:SavePlayerData("yanhualibaoeditor",json)
    CLuaYanHuaEditorMgr.OnSaveYanHuaDesignData()
end

function CLuaYanHuaLiBaoEditorWnd:OnLoadLocalYanHuaLightList()
    CLuaYanHuaEditorMgr.YanHuaLightListTbl = {}
    local list = {}
    local json = CPlayerDataMgr.Inst:LoadPlayerData("yanhualibaoeditor")
    if json~=nil and json~="" then
        list = luaJson.json2lua(json)
    end

    CLuaYanHuaEditorMgr.YanHuaLightListTbl = {}
    for i=1,20,1 do
        local t = {}
        t.type = "empty"
        table.insert(CLuaYanHuaEditorMgr.YanHuaLightListTbl,t)
    end
    if list then 
        for i,v in pairs(list) do
            local t = {}
            t.type = "empty"
            if v and type(v) == "table" and next(v) then
                t.type = v.type
                t.normalId = v.normalId
                t.customUrl = v.customUrl
                t.size = v.size
                t.colorIndex = v.colorIndex
                t.customIndex = v.customIndex
            end
            if i~=nil and tonumber(i) ~= nil then
                CLuaYanHuaEditorMgr.YanHuaLightListTbl[tonumber(i)] = t
            end
        end
    end

    self:LoadLocalCustomData()
end

function CLuaYanHuaLiBaoEditorWnd:LoadLocalCustomData()
    CLuaYanHuaEditorMgr.OnLoadYanHuaDesignData()
    local count = #CLuaYanHuaEditorMgr.CanvasList
end

function CLuaYanHuaLiBaoEditorWnd:OnClickLightBtn()
    local canLight = true
    local param = ""
    local param2 = ""
    local param3 = ""
    local param4 = ""
    for i,v in ipairs(CLuaYanHuaEditorMgr.YanHuaLightListTbl) do
        if not next(v) then
            canLight = false
        elseif v.type == "empty" then
            canLight = false
        else
            local ty
            if v.type == "normal" then
                ty = CLuaYanHuaEditorMgr.EnumYanHuaType.eNormal
            elseif v.type == "custom" then
                ty = CLuaYanHuaEditorMgr.EnumYanHuaType.eCustom
            end
            local id = v.normalId
            if ty == CLuaYanHuaEditorMgr.EnumYanHuaType.eCustom then
                local customIndex = v.customIndex
                local canvasIndex = CLuaYanHuaEditorMgr.AddCanvasIndexList[tonumber(customIndex)]
                local canvas = CLuaYanHuaEditorMgr.CanvasList[tonumber(canvasIndex)]               
                id = canvas.picId
                local shengheData = CLuaYanHuaEditorMgr.MyYanHuaPicData[tonumber(canvasIndex)]
                if shengheData and type(shengheData)=="table" and next(shengheData) and shengheData.id == tonumber(id) then
                    local audit = shengheData.auditStatus
                    if CLuaYanHuaEditorMgr.AuditSwitchOn  then
                        if not audit or audit ~= CLuaYanHuaEditorMgr.EnumAuditStatus.ePass then
                            self.m_HasCustomAuditRefrused = true
                            canLight = false
                        end
                    end
                else
                    if CLuaYanHuaEditorMgr.AuditSwitchOn  then
                        self.m_HasCustomAuditRefrused = true
                        canLight = false
                    end
                end
            end
            local size = v.size
            local color = v.colorIndex
            if not color then color = 0 end
            if i<= 5 then
                param = param..ty..","..id..","..size..","..color..";"
            elseif i >=6 and i <= 10 then
                param2 = param2..ty..","..id..","..size..","..color..";"
            elseif i >=11 and i <= 15 then
                param3 = param3..ty..","..id..","..size..","..color..";"
            else
                param4 = param4..ty..","..id..","..size..","..color..";"
            end
        end
    end

    if canLight then
        self:OnSaveYanHuaLightList()
        CLuaYanHuaEditorMgr.CanLight = true
        Gac2Gas.RequestPlayYanHuaLiBao(param,param2,param3,param4)
        CItemInfoMgr.CloseItemInfoWnd()
        CUIManager.CloseUI(CUIResources.PackageWnd)
        CUIManager.CloseUI(CLuaUIResources.YanHuaLiBaoEditorWnd)
    else
        if self.m_HasCustomAuditRefrused then
            g_MessageMgr:ShowMessage("YanHuaEditor_CannotLight_Without_Audit")
        else
            g_MessageMgr:ShowMessage("YanHuaEditor_Please_Fill_All")
        end
    end
end

function CLuaYanHuaLiBaoEditorWnd:ClickThroughToClose()
    if self.m_InfoWnd.activeSelf and Input.GetMouseButtonDown(0) then    
        if (not CUICommonDef.IsOverUI or (UICamera.lastHit.collider and
           not CUICommonDef.IsChild(self.m_InfoWnd.transform, UICamera.lastHit.collider.transform) and
            CUICommonDef.GetNearestPanelDepth(self.m_InfoWnd.transform) > CUICommonDef.GetNearestPanelDepth(UICamera.lastHit.collider.transform))) then
            self.m_InfoWnd:SetActive(false)
        end
    end
end
