local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local CUIFxPaths        = import "L10.UI.CUIFxPaths"
local Vector4           = import "UnityEngine.Vector4"
local Item_Item         = import "L10.Game.Item_Item"
local CItemInfoMgr      = import "L10.UI.CItemInfoMgr"
local UIScrollBar       = import "UIScrollBar"
local UIDrawCall        = import "UIDrawCall"
local CServerTimeMgr    = import "L10.Game.CServerTimeMgr"

LuaCommonPassportRewardView = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaCommonPassportRewardView, "vip1_Locked")
RegistClassMember(LuaCommonPassportRewardView, "vip2_Locked")
RegistClassMember(LuaCommonPassportRewardView, "vip1_High_Locked")

RegistClassMember(LuaCommonPassportRewardView, "vip1_Unlock_Vfx")
RegistClassMember(LuaCommonPassportRewardView, "vip2_Unlock_Vfx")
RegistClassMember(LuaCommonPassportRewardView, "vip1_High_Unlock_Vfx")

RegistClassMember(LuaCommonPassportRewardView, "awardTemplate")
RegistClassMember(LuaCommonPassportRewardView, "tableView")
RegistClassMember(LuaCommonPassportRewardView, "scrollBar")
RegistClassMember(LuaCommonPassportRewardView, "milestone_Level")
RegistClassMember(LuaCommonPassportRewardView, "milestone_Base_Award")
RegistClassMember(LuaCommonPassportRewardView, "milestone_VIP1_Award")
RegistClassMember(LuaCommonPassportRewardView, "milestone_VIP2_Award")
RegistClassMember(LuaCommonPassportRewardView, "milestone_VIP1_High_Award")
RegistClassMember(LuaCommonPassportRewardView, "shopButton")
RegistClassMember(LuaCommonPassportRewardView, "shopScoreLabel")
RegistClassMember(LuaCommonPassportRewardView, "buyAdvancedPassButton")
RegistClassMember(LuaCommonPassportRewardView, "oneKeyRedDot")
RegistClassMember(LuaCommonPassportRewardView, "leftBackButton")
RegistClassMember(LuaCommonPassportRewardView, "rightBackButton")

RegistClassMember(LuaCommonPassportRewardView, "passId")
RegistClassMember(LuaCommonPassportRewardView, "hasVIP2")
RegistClassMember(LuaCommonPassportRewardView, "isInited")

RegistClassMember(LuaCommonPassportRewardView, "rewardLevel")
RegistClassMember(LuaCommonPassportRewardView, "selectId") -- 当前选中的childId
RegistClassMember(LuaCommonPassportRewardView, "isVIP1Open")
RegistClassMember(LuaCommonPassportRewardView, "isVIP2Open")
RegistClassMember(LuaCommonPassportRewardView, "passInfo")
RegistClassMember(LuaCommonPassportRewardView, "curMilestoneId") -- 里程碑对应的childId

function LuaCommonPassportRewardView:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

    self.passId = LuaCommonPassportMgr.passId
    self.hasVIP2 = LuaCommonPassportMgr:HasVIP2(self.passId)
    self:InitComponents()
    self:InitEventListener()
    self:InitTableView()
    self:InitShopButtonActive()
end

function LuaCommonPassportRewardView:InitComponents()
    local names = Pass_ClientSetting.GetData(self.passId).BattlePassNames
    local vip1 = self.transform:Find("Type/VIP1")
    local vip2 = self.transform:Find("Type/VIP2")
    local vip1_High = self.transform:Find("Type/VIP1_High")
    vip1.gameObject:SetActive(self.hasVIP2)
    vip2.gameObject:SetActive(self.hasVIP2)
    vip1_High.gameObject:SetActive(not self.hasVIP2)

    self.transform:Find("Type/Base/Label"):GetComponent(typeof(UILabel)).text = names[0]
    if self.hasVIP2 then
        vip1:Find("Label"):GetComponent(typeof(UILabel)).text = names[1]
        vip2:Find("Label"):GetComponent(typeof(UILabel)).text = names[2]
        self.vip1_Locked = vip1:Find("Locked").gameObject
        self.vip2_Locked = vip2:Find("Locked").gameObject

        UIEventListener.Get(vip1.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            self:OnVIP1Click()
        end)
        UIEventListener.Get(vip2.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            self:OnVIP2Click()
        end)
    else
        vip1_High:Find("Label"):GetComponent(typeof(UILabel)).text = names[1]
        self.vip1_High_Locked = vip1_High:Find("Locked").gameObject

        UIEventListener.Get(vip1_High.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            self:OnVIP1Click()
        end)
    end

    self.milestone_Level = self.transform:Find("Milestone/Level"):GetComponent(typeof(UILabel))
    local milestone_VIP1 = self.transform:Find("Milestone/VIP1")
    local milestone_VIP2 = self.transform:Find("Milestone/VIP2")
    local milestone_VIP1_High_Award = self.transform:Find("Milestone/VIP1_High")
    milestone_VIP1.gameObject:SetActive(self.hasVIP2)
    milestone_VIP2.gameObject:SetActive(self.hasVIP2)
    milestone_VIP1_High_Award.gameObject:SetActive(not self.hasVIP2)

    self.milestone_Base_Award = self.transform:Find("Milestone/Base/Award")
    if self.hasVIP2 then
        self.milestone_VIP1_Award = milestone_VIP1:Find("Award")
        self.milestone_VIP2_Award = milestone_VIP2:Find("Award")
    else
        self.milestone_VIP1_High_Award = milestone_VIP1_High_Award:Find("Award")
    end

    self.awardTemplate = self.transform:Find("AwardTemplate").gameObject
    self.awardTemplate:SetActive(false)
    self.tableView = self.transform:Find("TableView"):GetComponent(typeof(QnAdvanceGridView))
    self.scrollBar = self.transform:Find("TableView/ScrollBar"):GetComponent(typeof(UIScrollBar))

    self.shopButton = self.transform:Find("ShopButton").gameObject
    self.shopScoreLabel = self.transform:Find("ShopScore"):GetComponent(typeof(UILabel))
    self.buyAdvancedPassButton = self.transform:Find("BuyAdvancedPassButton").gameObject
    self.oneKeyRedDot = self.transform:Find("OneKeyButton/Alert").gameObject
    self.leftBackButton = self.transform:Find("Back/Left").gameObject
    self.rightBackButton = self.transform:Find("Back/Right").gameObject

    local item = self.tableView.transform:Find("Pool/Item")
    item:Find("Reached/VIP1").gameObject:SetActive(self.hasVIP2)
    item:Find("Reached/VIP2").gameObject:SetActive(self.hasVIP2)
    item:Find("Reached/VIP1_High").gameObject:SetActive(not self.hasVIP2)
    item:Find("NotReached/VIP1").gameObject:SetActive(self.hasVIP2)
    item:Find("NotReached/VIP2").gameObject:SetActive(self.hasVIP2)
    item:Find("NotReached/VIP1_High").gameObject:SetActive(not self.hasVIP2)
    item:Find("VIP1Award").gameObject:SetActive(self.hasVIP2)
    item:Find("VIP2Award").gameObject:SetActive(self.hasVIP2)
    item:Find("VIP1Award_High").gameObject:SetActive(not self.hasVIP2)

    self.vip1_Unlock_Vfx = self.transform:Find("UnlockVfx/VIP1").gameObject
    self.vip2_Unlock_Vfx = self.transform:Find("UnlockVfx/VIP2").gameObject
    self.vip1_High_Unlock_Vfx = self.transform:Find("UnlockVfx/VIP1_High").gameObject
end

function LuaCommonPassportRewardView:InitEventListener()
    UIEventListener.Get(self.shopButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnShopButtonClick()
    end)

    UIEventListener.Get(self.buyAdvancedPassButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnBuyAdvancedPassButtonClick()
    end)

    local oneKeyButton = self.transform:Find("OneKeyButton").gameObject
    UIEventListener.Get(oneKeyButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnOneKeyButtonClick()
    end)

    self.tableView.m_Panel.onClipMove = LuaUtils.OnClippingMoved(function(panel)
        self:OnPanelClipMove()
    end)

    UIEventListener.Get(self.leftBackButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnLeftBackButtonClick()
    end)

    UIEventListener.Get(self.rightBackButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnRightBackButtonClick()
    end)
end

function LuaCommonPassportRewardView:OnEnable()
    if self.hasVIP2 then
        self.vip1_Unlock_Vfx:SetActive(false)
        self.vip2_Unlock_Vfx:SetActive(false)
    else
        self.vip1_High_Unlock_Vfx:SetActive(false)
    end

    g_ScriptEvent:AddListener("UpdatePassDataWithId", self, "OnUpdatePassDataWithId")
    g_ScriptEvent:AddListener("CommonPassportPlayVipUnlockVfx", self, "OnCommonPassportPlayVipUnlockVfx")
    g_ScriptEvent:AddListener("SyncTempPlayScore", self, "OnSyncTempPlayScore")
end

function LuaCommonPassportRewardView:OnDisable()
    g_ScriptEvent:RemoveListener("UpdatePassDataWithId", self, "OnUpdatePassDataWithId")
    g_ScriptEvent:RemoveListener("CommonPassportPlayVipUnlockVfx", self, "OnCommonPassportPlayVipUnlockVfx")
    g_ScriptEvent:RemoveListener("SyncTempPlayScore", self, "OnSyncTempPlayScore")
end

function LuaCommonPassportRewardView:OnUpdatePassDataWithId(id, reason)
    if self.passId == id then
        self:UpdateData()
        self:UpdateLock()
        self.tableView:ReloadData(true, true)
        self:UpdateMilestone(true)

        self:UpdateOneKeyRedDot()
        self:UpdateBuyAdvancedPassButtonActive()
    end
end

function LuaCommonPassportRewardView:OnCommonPassportPlayVipUnlockVfx(id, vip1Unlock, vip2Unlock)
    if self.passId == id then
        if self.hasVIP2 then
            self.vip1_Unlock_Vfx:SetActive(vip1Unlock)
            self.vip2_Unlock_Vfx:SetActive(vip2Unlock)
        else
            self.vip1_High_Unlock_Vfx:SetActive(vip1Unlock)
        end
    end
end

function LuaCommonPassportRewardView:OnSyncTempPlayScore(key, score, expiredTime)
    if key == EnumTempPlayScoreKey_lua[Pass_ClientSetting.GetData(self.passId).ScoreShopKey] then
        self:UpdateShopScore()
    end
end

function LuaCommonPassportRewardView:UpdateShopScore()
    local mainPlayer = CClientMainPlayer.Inst
    if not mainPlayer then return end

    local key = EnumTempPlayScoreKey_lua[Pass_ClientSetting.GetData(self.passId).ScoreShopKey]
    if key then
        local data = CommonDefs.DictGetValue_LuaCall(mainPlayer.PlayProp.TempPlayScore, key)
        if data and data.ExpiredTime and data.ExpiredTime > CServerTimeMgr.Inst.timeStamp then
            self.shopScoreLabel.text = data.Score
        else
            self.shopScoreLabel.text = "0"
        end
    end
end


function LuaCommonPassportRewardView:Init()
    self:UpdateData()
    self:UpdateLock()
    self.tableView:ReloadData(true, self.isInited or false)
    self:AutoScroll()
    self:UpdateMilestone(true)

    self:UpdateOneKeyRedDot()
    self:UpdateBuyAdvancedPassButtonActive()
    self:UpdateShopScore()
    self.isInited = true
end

function LuaCommonPassportRewardView:UpdateData()
    self.rewardLevel = LuaCommonPassportMgr:GetRewardLevel(self.passId)
    self.isVIP1Open = LuaCommonPassportMgr:GetIsVIP1Open(self.passId)
    self.isVIP2Open = LuaCommonPassportMgr:GetIsVIP2Open(self.passId)
    self.passInfo = LuaCommonPassportMgr:GetPassInfo(self.passId)

    local rewardDesignData = LuaCommonPassportMgr:GetRewardDesignData(self.passId)
    self.selectId = 0
    for id, data in ipairs(rewardDesignData) do
        if self.rewardLevel == data.level then
            self.selectId = id
            break
        end
    end
end

-- 更新高级战令解锁图标显示
function LuaCommonPassportRewardView:UpdateLock()
    if self.hasVIP2 then
        self.vip1_Locked:SetActive(not self.isVIP1Open)
        self.vip2_Locked:SetActive(not self.isVIP2Open)
    else
        self.vip1_High_Locked:SetActive(not self.isVIP1Open)
    end
end

function LuaCommonPassportRewardView:InitTableView()
    local count = #LuaCommonPassportMgr:GetRewardDesignData(self.passId)
    self.tableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return count
        end,
        function(item, index)
            self:InitItem(item, index)
        end
    )
end

function LuaCommonPassportRewardView:InitItem(item, index)
    local rewardDesignData = LuaCommonPassportMgr:GetRewardDesignData(self.passId)

    local itemLevel = rewardDesignData[index + 1].level
    local reached = item.transform:Find("Reached")
    local notReached = item.transform:Find("NotReached")
    local isReached = self.rewardLevel >= itemLevel
    reached.gameObject:SetActive(isReached)
    notReached.gameObject:SetActive(not isReached)
    if isReached then
        reached:Find("Level"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("%d级"), itemLevel)
    else
        notReached:Find("Level"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("%d级"), itemLevel)
    end
    item.transform:Find("Selected").gameObject:SetActive(self.selectId == index + 1)

    local itemRewardsTbl = LuaCommonPassportMgr:ParseItemRewards(rewardDesignData[index + 1].id)
    self:InitAwardItem(item.transform:Find("BaseAward"), itemRewardsTbl[1], isReached, true, 1, itemLevel)
    if self.hasVIP2 then
        self:InitAwardItem(item.transform:Find("VIP1Award"), itemRewardsTbl[2], isReached, self.isVIP1Open, 2, itemLevel)
        self:InitAwardItem(item.transform:Find("VIP2Award"), itemRewardsTbl[3], isReached, self.isVIP2Open, 3, itemLevel)
    else
        self:InitAwardItem(item.transform:Find("VIP1Award_High"), itemRewardsTbl[2], isReached, self.isVIP1Open, 2, itemLevel)
    end
end

function LuaCommonPassportRewardView:InitAwardItem(parentTrans, itemTbl, isReached, isOpen, type, itemLevel)
    local isReceived = LuaCommonPassportMgr:IsPassAwardReceived(self.passInfo, itemLevel, type)
    local isLocked = not isReached or not isOpen
    local isCanReceive = not isLocked and not isReceived

    local childCount = parentTrans.childCount
    itemTbl = itemTbl or {}
    if childCount < #itemTbl then
        for i = childCount + 1, #itemTbl do
            NGUITools.AddChild(parentTrans.gameObject, self.awardTemplate)
        end
    elseif childCount > #itemTbl then
        for i = #itemTbl, childCount - 1 do
            parentTrans:GetChild(i).gameObject:SetActive(false)
        end
    end

    for i = 1, #itemTbl do
        local data = itemTbl[i]
        local itemData = Item_Item.GetData(data.itemId)

        local child = parentTrans:GetChild(i - 1)
        child.gameObject:SetActive(true)
        child:Find("Icon"):GetComponent(typeof(CUITexture)):LoadMaterial(itemData.Icon)
        child:Find("Locked").gameObject:SetActive(isLocked)
        child:Find("Quality"):GetComponent(typeof(UISprite)).spriteName = CUICommonDef.GetItemCellBorder(itemData, nil, true)
        child:Find("Bind").gameObject:SetActive(data.isBind)
        child:Find("Received").gameObject:SetActive(isReceived)
        child:Find("Amount"):GetComponent(typeof(UILabel)).text = data.count > 0 and data.count or ""
        child:Find("Fx").gameObject:SetActive(isCanReceive)
        if isCanReceive then
            local cuiFx = child:Find("Fx"):GetComponent(typeof(CUIFx))
            cuiFx:LoadFx(CUIFxPaths.CommonYellowLineFxPath)
            local icon = child:Find("Icon"):GetComponent(typeof(UITexture))
            local gap = 5
            CUIFx.DoAni(Vector4(-icon.width * 0.5 + gap, icon.height * 0.5 - gap,
                icon.width - gap * 2, icon.height - gap * 2), false, cuiFx)
        end

        UIEventListener.Get(child.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            self:OnItemClick(data.itemId, isCanReceive, itemLevel, type)
        end)
    end

    local grid = parentTrans:GetComponent(typeof(UIGrid))
    if grid then grid:Reposition() end
end

-- 列表自动滚动
function LuaCommonPassportRewardView:AutoScroll()
    if self.isInited then return end

    local hasAward = LuaCommonPassportMgr:HasPassAward(self.passId)
    if not hasAward then
        self:BackToCurLevel()
        return
    end

    local rewardDesignData = LuaCommonPassportMgr:GetRewardDesignData(self.passId)
    local targetId = 0
    for id, data in ipairs(rewardDesignData) do
        if self.rewardLevel < data.level then
            break
        end

        if not LuaCommonPassportMgr:IsPassAwardReceived(self.passInfo, data.level, 1) then
            targetId = id
            break
        end
        if self.isVIP1Open and not LuaCommonPassportMgr:IsPassAwardReceived(self.passInfo, data.level, 2) then
            targetId = id
            break
        end
        if self.isVIP2Open and not LuaCommonPassportMgr:IsPassAwardReceived(self.passInfo, data.level, 3) then
            targetId = id
            break
        end
    end
    if targetId > 0 then
        self.tableView:ScrollToRow(targetId - 1)
        self:UpdateScrollBarValue()
        self:UpdateMilestone(false)
    end
end

-- ScrollBar的Start函数执行在tableView的ScrollToRow之后，导致ScrollToRow没有生效
-- 这里先强制设置一下ScrollBar的value
-- 后续再考虑怎么优化
function LuaCommonPassportRewardView:UpdateScrollBarValue()
    local scrollView = self.tableView.m_ScrollView
    local b = scrollView.bounds
    local panel = scrollView.panel
    local clip = panel.finalClipRegion
    local hx = clip.z * 0.5
    local left = b.min.x + hx
    local right = b.max.x - hx

    if panel.clipping == UIDrawCall.Clipping.SoftClip then
        left = left - panel.clipSoftness.x
        right = right + panel.clipSoftness.x
    end
    if left == right then return end

    self.scrollBar.value = (clip.x - left) / (right - left)
end

-- 更新一键领取奖励红点
function LuaCommonPassportRewardView:UpdateOneKeyRedDot()
    self.oneKeyRedDot:SetActive(LuaCommonPassportMgr:HasPassAward(self.passId))
end

-- 更新购买高级战令按钮显示
function LuaCommonPassportRewardView:UpdateBuyAdvancedPassButtonActive()
    local active = not self.isVIP1Open or (self.hasVIP2 and not self.isVIP2Open)
    self.buyAdvancedPassButton:SetActive(active)
end

-- 更新里程碑
function LuaCommonPassportRewardView:UpdateMilestone(forceUpdate)
    local rewardDesignData = LuaCommonPassportMgr:GetRewardDesignData(self.passId)
    local maxCount = #rewardDesignData

    -- 计算除最高和最低级奖励外显示的其他奖励等级
    local itemList = self.tableView:GetVisibleItems()
    local min = 100000
    local max = 0
    local count = itemList.Count
    for i = 0, count - 1 do
        local item = itemList[i]
        if item.Row ~= 0 and item.Row ~= maxCount - 1 then
            min = math.min(min, item.Row)
            max = math.max(max, item.Row)
        end
    end

    -- 计算里程碑等级
    local showAward = Pass_ClientSetting.GetData(self.passId).ShowAward
    local milestoneCount = showAward.Length
    local milestoneCountDict = {}
    for i = 0, milestoneCount - 1 do
        milestoneCountDict[showAward[i]] = true
    end

    local milestoneId = 0
    for i = max + 1, maxCount do
        local level = rewardDesignData[i].level
        if milestoneCountDict[level] then
            milestoneId = i
            break
        end
    end
    if milestoneId == 0 then
        for i = max, 1, -1 do
            local level = rewardDesignData[i].level
            if milestoneCountDict[level] then
                milestoneId = i
                break
            end
        end
    end

    -- 更新显示
    if forceUpdate or not self.curMilestoneId or self.curMilestoneId ~= milestoneId then
        self.curMilestoneId = milestoneId

        local level = rewardDesignData[milestoneId].level
        local isReached = self.rewardLevel >= level
        local itemRewardsTbl = LuaCommonPassportMgr:ParseItemRewards(rewardDesignData[milestoneId].id)
        self:InitAwardItem(self.milestone_Base_Award, itemRewardsTbl[1], isReached, true, 1, level)
        if self.hasVIP2 then
            self:InitAwardItem(self.milestone_VIP1_Award, itemRewardsTbl[2], isReached, self.isVIP1Open, 2, level)
            self:InitAwardItem(self.milestone_VIP2_Award, itemRewardsTbl[3], isReached, self.isVIP2Open, 3, level)
        else
            self:InitAwardItem(self.milestone_VIP1_High_Award, itemRewardsTbl[2], isReached, self.isVIP1Open, 2, level)
        end

        self.milestone_Level.text = SafeStringFormat3(LocalString.GetString("%d级可领"), level)
    end

    self:UpdateBackButtonActive(min + 1, max + 1)
end

-- 更新显示返回按钮
function LuaCommonPassportRewardView:UpdateBackButtonActive(minVisibleId, maxVisibleId)
    self.leftBackButton:SetActive(self.selectId > 0 and self.selectId < minVisibleId - 1)
    self.rightBackButton:SetActive(self.selectId > 0 and self.selectId > maxVisibleId + 1)
end

function LuaCommonPassportRewardView:InitShopButtonActive()
    local scoreShopKey = Pass_ClientSetting.GetData(self.passId).ScoreShopKey
    self.shopButton:SetActive(not System.String.IsNullOrEmpty(scoreShopKey))
end

-- 返回当前等级
function LuaCommonPassportRewardView:BackToCurLevel()
    if self.selectId > 0 then
        self.tableView:ScrollToRow(self.selectId - 1)
        self:UpdateScrollBarValue()
        self:UpdateMilestone(false)
    end
end

--@region UIEvent

function LuaCommonPassportRewardView:OnShopButtonClick()
    CLuaNPCShopInfoMgr.ShowScoreShop(Pass_ClientSetting.GetData(self.passId).ScoreShopKey)
end

function LuaCommonPassportRewardView:OnBuyAdvancedPassButtonClick()
    LuaCommonPassportMgr:OpenVIPUnlockWnd(self.passId)
end

function LuaCommonPassportRewardView:OnOneKeyButtonClick()
    if not LuaCommonPassportMgr:HasPassAward(self.passId) then
        g_MessageMgr:ShowMessage("COMMON_PASSPORT_NO_AWARD_CAN_RECEIVE")
        return
    end

    Gac2Gas.RequestGetPassReward(self.passId, 0, 0)
end

function LuaCommonPassportRewardView:OnItemClick(itemId, isCanReceive, itemLevel, type)
    if isCanReceive then
        Gac2Gas.RequestGetPassReward(self.passId, itemLevel, type)
    else
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemId)
    end
end

function LuaCommonPassportRewardView:OnVIP1Click()
    if not self.isVIP1Open then
        LuaCommonPassportMgr:OpenVIPUnlockWnd(self.passId)
    end
end

function LuaCommonPassportRewardView:OnVIP2Click()
    if not self.isVIP2Open then
        LuaCommonPassportMgr:OpenVIPUnlockWnd(self.passId)
    end
end

function LuaCommonPassportRewardView:OnPanelClipMove()
    self:UpdateMilestone(false)
end

function LuaCommonPassportRewardView:OnLeftBackButtonClick()
    self:BackToCurLevel()
end

function LuaCommonPassportRewardView:OnRightBackButtonClick()
    self:BackToCurLevel()
end

--@endregion UIEvent
