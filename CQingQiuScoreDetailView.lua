-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CQingQiuMgr = import "L10.UI.CQingQiuMgr"
local CQingQiuScoreDetailView = import "L10.UI.CQingQiuScoreDetailView"
local CQingQiuScoreItem = import "L10.UI.CQingQiuScoreItem"
CQingQiuScoreDetailView.m_Awake_CS2LuaHook = function (this) 
    this.m_MyselfSocreObj:SetActive(false)
    this.m_PlayerSocreObj:SetActive(false)
    this.m_TableView.dataSource = this
    this.m_TableView.eventDelegate = this
end
CQingQiuScoreDetailView.m_CellForRowAtIndex_CS2LuaHook = function (this, index) 
    if index < 0 and index >= CQingQiuMgr.Inst.m_ScoreData.Count then
        return nil
    end
    local cell
    if index == 0 and CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.Id == CQingQiuMgr.Inst.m_ScoreData[0].m_Id then
        cell = this.m_TableView:DequeueReusableCellWithIdentifier("MyScoreItem")
        if cell == nil then
            cell = this.m_TableView:AllocNewCellWithIdentifier(this.m_MyselfSocreObj, "MyScoreItem")
        end
    else
        cell = this.m_TableView:DequeueReusableCellWithIdentifier("PlayerScoreItem")
        if cell == nil then
            cell = this.m_TableView:AllocNewCellWithIdentifier(this.m_PlayerSocreObj, "PlayerScoreItem")
        end
    end
    CommonDefs.GetComponent_GameObject_Type(cell, typeof(CQingQiuScoreItem)):Init(CQingQiuMgr.Inst.m_ScoreData[index], index % 2 ~= 0)

    return cell
end
