local DelegateFactory    = import "DelegateFactory"
local CTopAndRightTipWnd = import "L10.UI.CTopAndRightTipWnd"
local CScene             = import "L10.Game.CScene"
local CForcesMgr         = import "L10.Game.CForcesMgr"

LuaCuJuMinimap = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaCuJuMinimap, "expandButton")
RegistClassMember(LuaCuJuMinimap, "leftScore")
RegistClassMember(LuaCuJuMinimap, "rightScore")
RegistClassMember(LuaCuJuMinimap, "time")
RegistClassMember(LuaCuJuMinimap, "widget")
RegistClassMember(LuaCuJuMinimap, "markTemplate")
RegistClassMember(LuaCuJuMinimap, "markTable")
RegistClassMember(LuaCuJuMinimap, "ball")

RegistClassMember(LuaCuJuMinimap, "queryTick")
RegistClassMember(LuaCuJuMinimap, "topLeftPos")
RegistClassMember(LuaCuJuMinimap, "topLeft2RightVector")
RegistClassMember(LuaCuJuMinimap, "topLeft2BottomVector")

function LuaCuJuMinimap:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self:InitUIComponents()
    self.markTemplate:SetActive(false)
    self:InitEventListener()

    local worldPos = CuJu_Setting.GetData().CornerWorldPos
    local tbl = {}
    for x, z in string.gmatch(worldPos, "([%d]+),([%d]+)") do
        table.insert(tbl, {tonumber(x), tonumber(z)})
    end

    self.topLeftPos = Vector3(tbl[1][1], 0, tbl[1][2])
    local topRightPos = Vector3(tbl[2][1], 0, tbl[2][2])
    local bottomLeftPos = Vector3(tbl[3][1], 0, tbl[3][2])
    self.topLeft2RightVector = CommonDefs.op_Subtraction_Vector3_Vector3(topRightPos, self.topLeftPos)
    self.topLeft2BottomVector = CommonDefs.op_Subtraction_Vector3_Vector3(bottomLeftPos, self.topLeftPos)
end

function LuaCuJuMinimap:InitUIComponents()
    local anchor = self.transform:Find("Anchor")
    self.expandButton = self.transform:Find("ExpandButton").gameObject
    self.leftScore = anchor:Find("Center/LeftScore"):GetComponent(typeof(UILabel))
    self.rightScore = anchor:Find("Center/RightScore"):GetComponent(typeof(UILabel))
    self.time = anchor:Find("Center/Time"):GetComponent(typeof(UILabel))
    self.widget = anchor:Find("Center/Widget"):GetComponent(typeof(UIWidget))
    self.markTemplate = anchor:Find("Center/Widget/MarkTemplate").gameObject
    self.ball = anchor:Find("Center/Widget/Ball").gameObject
    self.markTable = anchor:Find("Center/Widget/MarkTable")

    self.ball:SetActive(false)
end

function LuaCuJuMinimap:InitEventListener()
    UIEventListener.Get(self.expandButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnExpandButtonClick()
	end)
end

function LuaCuJuMinimap:OnEnable()
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:AddListener("SceneRemainTimeUpdate", self, "OnRemainTimeUpdate")
    g_ScriptEvent:AddListener("UpdateObjPosInfo", self, "OnUpdateObjPosInfo")
    g_ScriptEvent:AddListener("UpdateCuJuBallPos", self, "OnUpdateBallPos")
    g_ScriptEvent:AddListener("UpdateCuJuTotalScore", self, "OnUpdateTotalScore")
end

function LuaCuJuMinimap:OnDisable()
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:RemoveListener("SceneRemainTimeUpdate", self, "OnRemainTimeUpdate")
    g_ScriptEvent:RemoveListener("UpdateObjPosInfo", self, "OnUpdateObjPosInfo")
    g_ScriptEvent:RemoveListener("UpdateCuJuBallPos", self, "OnUpdateBallPos")
    g_ScriptEvent:RemoveListener("UpdateCuJuTotalScore", self, "OnUpdateTotalScore")

    if CUIManager.IsLoaded(CUIResources.TopAndRightTipWnd) then
        CUIManager.CloseUI(CUIResources.TopAndRightTipWnd)
    end

    LuaCuJuMgr.Info = {}
end

function LuaCuJuMinimap:OnHideTopAndRightTipWnd()
    LuaUtils.SetLocalRotation(self.expandButton.transform, 0, 0, 180)
end

function LuaCuJuMinimap:OnRemainTimeUpdate(args)
    self:SetRemainTime()
end

function LuaCuJuMinimap:OnUpdateObjPosInfo(args)
    local playerData = args[0]

    local totalChildCount = self.markTable.childCount
    for i = 1, totalChildCount do
        self.markTable:GetChild(i - 1).gameObject:SetActive(false)
    end

    local count = playerData.Count
    if not playerData or count == 0 then return end

    for i = 0, count - 1, 6 do
        local playerId = math.floor(tonumber(playerData[i] or 0))

        local x = math.floor(tonumber(playerData[i + 2] or 0))
        local z = math.floor(tonumber(playerData[i + 3] or 0))

        local childId = math.floor(i / 6)
        local childCount = self.markTable.childCount
        local child = nil
        if childId < childCount then
            child = self.markTable:GetChild(childId).gameObject
        else
            child = NGUITools.AddChild(self.markTable.gameObject, self.markTemplate)
        end
        local position = self:CalculateMapPos(Utility.GridPos2WorldPos(x, z))
        if position.x < 0 or position.y > 0 or position.x > self.widget.width or position.y < -self.widget.height then
            child.gameObject:SetActive(false)
        else
            child.gameObject:SetActive(true)
            child.transform.localPosition = position

            if CForcesMgr.Inst.m_PlayForceInfo then
                local default, curPlayerForce = CForcesMgr.Inst.m_PlayForceInfo:TryGetValue(playerId)
                if default then
                    local sprite = child.transform:GetComponent(typeof(UISprite))
                    if (playerId ~= CClientMainPlayer.Inst.Id) then
                        sprite.spriteName = (curPlayerForce == EnumCommonForce_lua.eDefend) and g_sprites.MinimapCujuBlueSprite or g_sprites.MinimapCujuRedSprite
                    else
                        sprite.spriteName = (curPlayerForce == EnumCommonForce_lua.eDefend) and g_sprites.MinimapCujuBlueSelfSprite or g_sprites.MinimapCujuRedSelfSpriteSprite
                    end
                end
            end
        end
    end
end

-- 世界坐标转为UI局部坐标
function LuaCuJuMinimap:CalculateMapPos(worldPos)
    worldPos.y = 0
    local topLeft2DestVector = CommonDefs.op_Subtraction_Vector3_Vector3(worldPos, self.topLeftPos)
    local magtitude = topLeft2DestVector.magnitude

    local angle1 = Vector3.Angle(self.topLeft2RightVector, topLeft2DestVector)
    local angle2 = Vector3.Angle(self.topLeft2BottomVector, topLeft2DestVector)

    local xLength = magtitude * math.cos(angle1 / 180 * math.pi)
    local yLength = magtitude * math.cos(angle2 / 180 * math.pi)
    local xUI = xLength / self.topLeft2RightVector.magnitude * self.widget.width
    local yUI = -yLength / self.topLeft2BottomVector.magnitude * self.widget.height

    return Vector3(xUI, yUI, 0)
end

function LuaCuJuMinimap:OnUpdateBallPos(ballPos)
    local x = ballPos[1]
    local y = ballPos[2]

    if x ~= 0 and y ~= 0 then
        self.ball:SetActive(true)
        self.ball.transform.localPosition = self:CalculateMapPos(Utility.GridPos2WorldPos(x, y))
    else
        self.ball:SetActive(false)
    end
end

function LuaCuJuMinimap:OnUpdateTotalScore(forceScore)
    self.leftScore.text = forceScore[1]
    self.rightScore.text = forceScore[2]
end


function LuaCuJuMinimap:Init()
    self:SetRemainTime()

    self.leftScore.text = 0
    self.rightScore.text = 0

    Gac2Gas.QueryMMapPosInfo()
    self:ClearTick()
    self.queryTick = RegisterTick(function()
        Gac2Gas.QueryMMapPosInfo()
    end, 500)
end

function LuaCuJuMinimap:SetRemainTime()
    local mainScene = CScene.MainScene
    if mainScene then
        if mainScene.ShowTimeCountDown then
            self.time.text = self:GetRemainTimeText(mainScene.ShowTime)
        else
            self.time.text = ""
        end
    else
        self.time.text = ""
    end
end

function LuaCuJuMinimap:GetRemainTimeText(totalSeconds)
    if totalSeconds < 0 then
        totalSeconds = 0
    end
    if totalSeconds >= 3600 then
        return cs_string.Format(LocalString.GetString("{0:00}:{1:00}:{2:00}"),
         math.floor(totalSeconds / 3600),
         math.floor((totalSeconds % 3600) / 60),
         totalSeconds % 60)
    else
        return cs_string.Format(LocalString.GetString("{0:00}:{1:00}"), math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end

function LuaCuJuMinimap:ClearTick()
    if self.queryTick then
        UnRegisterTick(self.queryTick)
    end
end

function LuaCuJuMinimap:OnDestroy()
    self:ClearTick()

    LuaCuJuMgr.Info = {}
end

--@region UIEvent

function LuaCuJuMinimap:OnExpandButtonClick()
    LuaUtils.SetLocalRotation(self.expandButton.transform, 0, 0, 0)
	CTopAndRightTipWnd.showPackage = false
	CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end

--@endregion UIEvent
