--local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local QnTableView=import "L10.UI.QnTableView"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local UISprite = import "UISprite"
local UILabel = import "UILabel"
local Profession = import "L10.Game.Profession"
local EnumClass = import "L10.Game.EnumClass"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local LocalString = import "LocalString"

CLuaQingMingRankWnd = class()

RegistChildComponent(CLuaQingMingRankWnd, "TableView", QnTableView)
RegistChildComponent(CLuaQingMingRankWnd, "EmptyTipLabel", GameObject)
RegistChildComponent(CLuaQingMingRankWnd, "ScrollView", CUIRestrictScrollView)

function CLuaQingMingRankWnd:Awake()
    CLuaQingMing2020Mgr.PlayerRankTable = {}
    self.EmptyTipLabel:SetActive(true)
    self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function() 
            return #CLuaQingMing2020Mgr.PlayerRankTable
        end,
        function(item,row)
            self:InitItem(item,row)
        end)
    self:Refresh()
end

function CLuaQingMingRankWnd:Refresh()

    if not CLuaQingMing2020Mgr.PlayerRankTable then
        self.EmptyTipLabel:SetActive(true)
    elseif not next(CLuaQingMing2020Mgr.PlayerRankTable) then
        self.EmptyTipLabel:SetActive(true)
    else
        self.EmptyTipLabel:SetActive(false)
    end
    self.TableView:ReloadData(false, false)
    self.ScrollView:ResetPosition()
end

function CLuaQingMingRankWnd:InitItem(item, row)
    local info = CLuaQingMing2020Mgr.PlayerRankTable[row+1]
    if info then
        local rankLabel = item.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
        local rankSprite = item.transform:Find("RankLabel/RankSprite"):GetComponent(typeof(UISprite))
        local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
        local classLabel = item.transform:Find("ClassLabel"):GetComponent(typeof(UILabel))
        local timeLabel = item.transform:Find("TimeLabel"):GetComponent(typeof(UILabel))
        local bg1 = item.transform:Find("bg1").gameObject
        local bg2 = item.transform:Find("bg2").gameObject
        bg1:SetActive(row % 2 == 0)
        bg2:SetActive(row % 2 ~= 0)
        
        local rank = info.Rank
        rankLabel.text = rank
        if rank <= 3 then
            rankSprite.gameObject:SetActive(true)
            rankSprite.spriteName = CLuaQingMing2020Mgr.RankSprites[rank]
        else
            rankSprite.gameObject:SetActive(false)
        end

        nameLabel.text = info.Name
        classLabel.text = Profession.GetFullName(CommonDefs.ConvertIntToEnum(typeof(EnumClass), info.Class))
        timeLabel.text = self:GetRemainTimeText(info.UsedTime)
        
    end
end

function CLuaQingMingRankWnd:GetRemainTimeText(totalSeconds)
    if totalSeconds < 0 then
        totalSeconds = 0
    end
    if totalSeconds >= 3600 then
        return SafeStringFormat3(LocalString.GetString("%d小时%d分%d秒"), math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)   
    else
        return SafeStringFormat3(LocalString.GetString("%d分%d秒"), math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end
