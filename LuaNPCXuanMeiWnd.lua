local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CUITexture = import "L10.UI.CUITexture"
local DelegateFactory = import "DelegateFactory"
local CChatLinkMgr = import "CChatLinkMgr"

local QnTableView=import "L10.UI.QnTableView"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local CItemMgr = import "L10.Game.CItemMgr"
local CUIFx = import "L10.UI.CUIFx"
local CButton = import "L10.UI.CButton"

CLuaNPCXuanMeiWnd = class()

RegistChildComponent(CLuaNPCXuanMeiWnd, "DeadLineLabel", UILabel)
RegistChildComponent(CLuaNPCXuanMeiWnd, "MsgLabel", UILabel)
RegistChildComponent(CLuaNPCXuanMeiWnd, "m_TableView","TableView", QnTableView)
RegistChildComponent(CLuaNPCXuanMeiWnd, "CandidateTemplate", GameObject)

RegistClassMember(CLuaNPCXuanMeiWnd, "m_RestVoteNum")
RegistClassMember(CLuaNPCXuanMeiWnd, "m_CandidateIds")
RegistClassMember(CLuaNPCXuanMeiWnd, "m_Npc2Declaration")
RegistClassMember(CLuaNPCXuanMeiWnd, "m_VotesPerFlowerItem")

function CLuaNPCXuanMeiWnd:OnEnable()
    g_ScriptEvent:AddListener("RefreshNPCXuanMeiWnd",self,"OnRefreshXuanMeiWnd")
    g_ScriptEvent:AddListener("SendItem", self, "Init")
    g_ScriptEvent:AddListener("SetItemAt", self, "Init")
end

function CLuaNPCXuanMeiWnd:OnDisable()
    g_ScriptEvent:RemoveListener("RefreshNPCXuanMeiWnd",self,"OnRefreshXuanMeiWnd")
    g_ScriptEvent:RemoveListener("SendItem", self, "Init")
    g_ScriptEvent:RemoveListener("SetItemAt", self, "Init")
end

function CLuaNPCXuanMeiWnd:OnRefreshHuaZhi()
    local itemId = Spokesman_Setting.GetData().SpokesmanHouseItem
    local bindCount, notbindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(itemId)
    local canComiteNum = bindCount + notbindCount--众筹道具的数量
    local maxNum = canComiteNum
    self.m_ItemNumLabel.text = SafeStringFormat3(LocalString.GetString("可提交衔云道具:%d"),canComiteNum)
    self.m_Input:SetValue(1, true)
    self.m_Input:SetMinMax(0, maxNum, 1)

    UIEventListener.Get(self.m_ComiteBtn).onClick = DelegateFactory.VoidDelegate(function (go) 
        local val = self.m_Input:GetValue()
        if val <= 0 then
            local msg = g_MessageMgr:FormatMessage("Sposkesman_Donate_NotEnough")
            MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
                CLuaNPCShopInfoMgr.ShowScoreShop("GlobalSpokesmanScore")
            end),nil,LocalString.GetString("前往兑换"),LocalString.GetString("取消"),false)
        else
            Gac2Gas.DonateToSpokesmanHouse(val)
        end
        
    end)

    UIEventListener.Get(self.m_IncreaseButton).onClick = DelegateFactory.VoidDelegate(function (go) 
        CLuaNPCShopInfoMgr.ShowScoreShop("GlobalSpokesmanScore")
    end)
end

function CLuaNPCXuanMeiWnd:Awake()
    self.CandidateTemplate:SetActive(false)
    self.m_RestVoteNum = 0

    local data = WorldButterfly2020_Setting.GetData()
    self.m_VotesPerFlowerItem = data.VotesPerFlowerItem
    local declarationStr = data.NpcDeclaration
    self.m_Npc2Declaration = {}
    for i=0,data.NpcDeclaration.Length-1,2 do
        local npcId = tonumber(data.NpcDeclaration[i])
        self.m_Npc2Declaration[npcId] = data.NpcDeclaration[i+1]
        
    end

    self.m_TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function() 
            return #CLuaNPCXuanMeiMgr.CandidateNpcIds
        end,
        function(item,row)
            self:InitCandidateItem(item,row)
        end)
end

function CLuaNPCXuanMeiWnd:Init()
    --按票数排序
    local compare = function (a, b)
        return CLuaNPCXuanMeiMgr.CandidateId2Vote[a] > CLuaNPCXuanMeiMgr.CandidateId2Vote[b]
    end
    table.sort(CLuaNPCXuanMeiMgr.CandidateNpcIds,compare)

    local data = WorldButterfly2020_Setting.GetData()
    local flowerItemId = data.FlowerItemId
    self.m_RestVoteNum = CItemMgr.Inst:GetItemCount(flowerItemId)

    local partnerLink = CChatLinkMgr.TranslateToNGUIText(LocalString.GetString(data.GetVotesNpcLink),false)
    local voteStr = ""
    if self.m_RestVoteNum <= 0 then
        voteStr = SafeStringFormat3(LocalString.GetString("[c][ff0000]%d[-][/c]"),self.m_RestVoteNum)
    else
        voteStr = SafeStringFormat3(LocalString.GetString("[c][00ff00]%d[-][/c]"),self.m_RestVoteNum)
    end
    local limitStr = ""
    if CLuaNPCXuanMeiMgr.CurMaxVoteTimes <= 0 then
        limitStr = SafeStringFormat3(LocalString.GetString("[c][ff0000]%d[-][/c]"),CLuaNPCXuanMeiMgr.CurMaxVoteTimes)
    else
        limitStr = SafeStringFormat3(LocalString.GetString("[c][00ff00]%d[-][/c]"),CLuaNPCXuanMeiMgr.CurMaxVoteTimes)
    end
    local msg = SafeStringFormat3(LocalString.GetString(WorldButterfly2020_Setting.GetData().Get_More_Votes),voteStr,limitStr,partnerLink)
    self.MsgLabel.text = msg

    UIEventListener.Get(self.MsgLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        local url = go:GetComponent(typeof(UILabel)):GetUrlAtPosition(UICamera.lastWorldPosition)
        if url ~= nil then
            local res = CChatLinkMgr.ProcessLinkClick(url, nil)
        end
    end)

    self.m_TableView:ReloadData(false, false)
    --self.DeadLineLabel.text = g_MessageMgr:FormatMessage("XuanMei_Finished_Date_Tip")

    --投票结束
    if CLuaNPCXuanMeiMgr.IsPickNpcFinish then
        self.DeadLineLabel.text = LocalString.GetString("活动已结束")
        self.MsgLabel.gameObject:SetActive(false)
    else
        self.DeadLineLabel.text = g_MessageMgr:FormatMessage("XuanMei_Finished_Date_Tip")
    end
end

function CLuaNPCXuanMeiWnd:InitCandidateItem(item,row)
    local npcId = tonumber(CLuaNPCXuanMeiMgr.CandidateNpcIds[row + 1])
    local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local portrait = item.transform:Find("Portrait"):GetComponent(typeof(CUITexture))
    local voteNumLabel = item.transform:Find("Vote/VoteNumLabel"):GetComponent(typeof(UILabel))
    local declarationLabel = item.transform:Find("DeclarationLabel"):GetComponent(typeof(UILabel))
    local voteBtn = item.transform:Find("VoteBtn"):GetComponent(typeof(CButton))--.gameObject

    local fxNode = item.transform:Find("FxNode"):GetComponent(typeof(CUIFx))
    fxNode:DestroyFx()
    if self.m_PickNpcId and self.m_PickNpcId == npcId then
        fxNode:LoadFx("Fx/UI/Prefab/UI_xuanmeitoupiao.prefab")
        self.m_PickNpcId = nil
    end

    local data = NPC_NPC.GetData(npcId)
    if not data then return end
    nameLabel.text = data.Name
    portrait:LoadNPCPortrait(data.Portrait,false)
    declarationLabel.text = WorldButterfly2020_PickNpc.GetData(npcId).Declaration
    voteNumLabel.text = CLuaNPCXuanMeiMgr.CandidateId2Vote[npcId]

    UIEventListener.Get(voteBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        --投票次数超过限制
        if CLuaNPCXuanMeiMgr.CurMaxVoteTimes <= 0 then
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("今日送花次数达到上限，明日再来吧！"))
        --投票道具数量不足
        elseif self.m_RestVoteNum <= 0 then
            g_MessageMgr:ShowMessage("Xuanmei_Get_Vote_Tip")
        else
            local maxNum = math.min(self.m_RestVoteNum,CLuaNPCXuanMeiMgr.CurMaxVoteTimes)
            CLuaNumberInputMgr.ShowNumInputBox(1, maxNum, 1, function (num)
                if num <= 0 then
                    return
                end
                Gac2Gas.ButterflyPickNpc(npcId,num)
            end, LocalString.GetString("请输入送花数量"))
        end
    end)

    voteBtn.Enabled = not CLuaNPCXuanMeiMgr.IsPickNpcFinish
end

function CLuaNPCXuanMeiWnd:OnRefreshXuanMeiWnd(npcId)
    self.m_PickNpcId = npcId
    self:Init()
end


--================
CLuaNPCXuanMeiMgr = class()
CLuaNPCXuanMeiMgr.CandidateNpcIds = {}
CLuaNPCXuanMeiMgr.CandidateId2Vote = {}
CLuaNPCXuanMeiMgr.CurMaxVoteTimes = 0
CLuaNPCXuanMeiMgr.IsPickNpcFinish = false
