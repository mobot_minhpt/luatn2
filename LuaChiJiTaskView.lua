require("common/common_include")

local CButton = import "L10.UI.CButton"
local CGamePlayMgr=import "L10.Game.CGamePlayMgr"
local CPUBGMgr = import "L10.Game.CPUBGMgr"

LuaChiJiTaskView = class()

RegistChildComponent(LuaChiJiTaskView, "TaskName", UILabel)
RegistChildComponent(LuaChiJiTaskView, "TaskDesc", UILabel)
RegistChildComponent(LuaChiJiTaskView, "RuleBtn", CButton)
RegistChildComponent(LuaChiJiTaskView, "LeaveBtn", CButton)
RegistChildComponent(LuaChiJiTaskView, "LeftPlayerLabel", UILabel)
RegistChildComponent(LuaChiJiTaskView, "LeftTeamLabel", UILabel)

function LuaChiJiTaskView:Init()
    -- CHIJI_TASK_TIPS
    self.TaskName.text = LocalString.GetString("逐鹿追沙")
    self.TaskDesc.text = g_MessageMgr:FormatMessage("CHIJI_TASK_RULE")

    CommonDefs.AddOnClickListener(self.LeaveBtn.gameObject, DelegateFactory.Action_GameObject(function (go)
        CGamePlayMgr.Inst:LeavePlay()
    end), false)

    CommonDefs.AddOnClickListener(self.RuleBtn.gameObject, DelegateFactory.Action_GameObject(function (go)
        g_MessageMgr:ShowMessage("CHIJI_TASK_TIPS")
    end), false)

    self:UpdateChiJiLeftTeamInfos()
end

function LuaChiJiTaskView:UpdateChiJiLeftTeamInfos()
    if CPUBGMgr.Inst.ChiJiPlayerNum and CPUBGMgr.Inst.ChiJiPlayerNum ~= -1 then
        self.LeftPlayerLabel.text = SafeStringFormat3(LocalString.GetString("剩余人数：%s人"), tostring(CPUBGMgr.Inst.ChiJiPlayerNum))
    else
        self.LeftPlayerLabel.text = nil
    end
    
    if CPUBGMgr.Inst.ChiJiTeamNum and CPUBGMgr.Inst.ChiJiTeamNum ~= -1 then
        self.LeftTeamLabel.text = SafeStringFormat3(LocalString.GetString("剩余队伍：%s队"), tostring(CPUBGMgr.Inst.ChiJiTeamNum))
    else
        self.LeftTeamLabel.text = nil
    end
end

function LuaChiJiTaskView:OnMainplayerCreated()
    self:UpdateChiJiLeftTeamInfos()
end

function LuaChiJiTaskView:OnEnable()
    g_ScriptEvent:AddListener("SyncChiJiLeftTeamCountInfo", self, "UpdateChiJiLeftTeamInfos")
    g_ScriptEvent:AddListener("MainPlayerCreated", self, "OnMainplayerCreated")
end

function LuaChiJiTaskView:OnDisable()
    g_ScriptEvent:RemoveListener("SyncChiJiLeftTeamCountInfo", self, "UpdateChiJiLeftTeamInfos")
    g_ScriptEvent:RemoveListener("MainPlayerCreated", self, "OnMainplayerCreated")
end

return LuaChiJiTaskView