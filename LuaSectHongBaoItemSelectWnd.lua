local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnRadioBox = import "L10.UI.QnRadioBox"
local QnTableView = import "L10.UI.QnTableView"
local CShopMallItemPreviewer = import "L10.UI.CShopMallItemPreviewer"
local QnButton = import "L10.UI.QnButton"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local GameObject = import "UnityEngine.GameObject"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local UIGrid = import "UIGrid"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local Extensions = import "Extensions"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType2 = import "L10.UI.CItemInfoMgr+AlignType"
local CItem = import "L10.Game.CItem"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local ShopMallTemlate = import "L10.UI.ShopMallTemlate"

LuaSectHongBaoItemSelectWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaSectHongBaoItemSelectWnd, "QnRadioBox", "QnRadioBox", QnRadioBox)
RegistChildComponent(LuaSectHongBaoItemSelectWnd, "QnTableView", "QnTableView", QnTableView)
RegistChildComponent(LuaSectHongBaoItemSelectWnd, "ItemPreviewer", "ItemPreviewer", CShopMallItemPreviewer)
RegistChildComponent(LuaSectHongBaoItemSelectWnd, "QnButton", "QnButton", QnButton)
RegistChildComponent(LuaSectHongBaoItemSelectWnd, "QnIncreseAndDecreaseButton", "QnIncreseAndDecreaseButton", QnAddSubAndInputButton)
RegistChildComponent(LuaSectHongBaoItemSelectWnd, "QnCostAndOwnMoney", "QnCostAndOwnMoney", CCurentMoneyCtrl)
RegistChildComponent(LuaSectHongBaoItemSelectWnd, "ItemTemplate", "ItemTemplate", GameObject)
RegistChildComponent(LuaSectHongBaoItemSelectWnd, "ItemSelectPanelScrollView", "ItemSelectPanelScrollView", CUIRestrictScrollView)
RegistChildComponent(LuaSectHongBaoItemSelectWnd, "ItemSelectPanelTable", "ItemSelectPanelTable", UIGrid)

--@endregion RegistChildComponent end
RegistClassMember(LuaSectHongBaoItemSelectWnd,"m_CurrentItems")
RegistClassMember(LuaSectHongBaoItemSelectWnd,"m_SelectRow")
RegistClassMember(LuaSectHongBaoItemSelectWnd,"m_ItemSelectPanelRadioBox")
RegistClassMember(LuaSectHongBaoItemSelectWnd,"m_ItemSelectPanelSelectRow")
RegistClassMember(LuaSectHongBaoItemSelectWnd,"m_HongBaoDynamicOnShelfItemsSetData")
RegistClassMember(LuaSectHongBaoItemSelectWnd,"m_LimitPurchaseQuantityItemIds")

function LuaSectHongBaoItemSelectWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	self.m_HongBaoDynamicOnShelfItemsSetData = {}
	self.QnRadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn, index)
	    self:OnQnRadioBoxSelected(btn, index)
	end)

	
	UIEventListener.Get(self.QnButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnQnButtonClick()
	end)


    --@endregion EventBind end
end

function LuaSectHongBaoItemSelectWnd:Init()
	self.m_LimitPurchaseQuantityItemIds = g_LuaUtil:StrSplit(Mall_Setting.GetData("LimitPurchaseQuantityItemIds").Value,";")
	for itemId,_ in pairs(LuaZongMenMgr.m_HongBaoDynamicOnShelfItemsSet) do
		local mallData = Mall_LingYuMall.GetData(itemId)
		if mallData then
			if not self.m_HongBaoDynamicOnShelfItemsSetData[mallData.SubCategory] then
				self.m_HongBaoDynamicOnShelfItemsSetData[mallData.SubCategory] = {}
			end
			table.insert(self.m_HongBaoDynamicOnShelfItemsSetData[mallData.SubCategory],mallData)
		end
	end
	self.QnCostAndOwnMoney:SetCost(0)
	self.QnTableView.m_DataSource=DefaultTableViewDataSource.Create(function()
        return self.m_CurrentItems and self.m_CurrentItems.Count or 0
    end, function(item, index)
        self:InitItem(item, index)
    end)
	self.QnTableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        self:OnSelectAtRow(row)
    end)
	self.QnIncreseAndDecreaseButton.onValueChanged = DelegateFactory.Action_uint(function (value) 
		self:OnQnIncreseAndDecreaseButtonValueChanged(value)
	end)
	self.QnRadioBox:ChangeTo(0, true)
	self:InitItemSelectPanel()
end

function LuaSectHongBaoItemSelectWnd:InitItemSelectPanel()
	self.ItemTemplate.gameObject:SetActive(false)
	Extensions.RemoveAllChildren(self.ItemSelectPanelTable.transform)
	self.m_ItemSelectPanelRadioBox = self.ItemSelectPanelTable.gameObject:AddComponent(typeof(QnRadioBox))
	for i = 1,LuaZongMenMgr.m_HongBaoItemCount do
		local obj = NGUITools.AddChild(self.ItemSelectPanelTable.gameObject,self.ItemTemplate.gameObject)
		self:InitItemSelectPanelItem(obj,i)
	end
	self.m_ItemSelectPanelRadioBox.m_RadioButtons = CreateFromClass(MakeArrayClass(QnSelectableButton), self.ItemSelectPanelTable.transform.childCount)
	for i = 0, self.ItemSelectPanelTable.transform.childCount - 1 do
        local go = self.ItemSelectPanelTable.transform:GetChild(i).gameObject
		self.m_ItemSelectPanelRadioBox.m_RadioButtons[i] = go:GetComponent(typeof(QnSelectableButton))
		self.m_ItemSelectPanelRadioBox.m_RadioButtons[i].OnClick = MakeDelegateFromCSFunction(self.m_ItemSelectPanelRadioBox.On_Click, MakeGenericClass(Action1, QnButton), self.m_ItemSelectPanelRadioBox)
	end
	self.m_ItemSelectPanelRadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn,index)
        self.m_ItemSelectPanelSelectRow = index
    end)
    self.m_ItemSelectPanelRadioBox:ChangeTo(LuaZongMenMgr.m_CurSelectHongBaoItemIndex - 1, true)
	self.ItemSelectPanelTable:Reposition()
	self.ItemSelectPanelScrollView:ResetPosition()
	CUICommonDef.SetFullyVisible(self.m_ItemSelectPanelRadioBox.m_RadioButtons[LuaZongMenMgr.m_CurSelectHongBaoItemIndex - 1].gameObject,self.ItemSelectPanelScrollView)
end

function LuaSectHongBaoItemSelectWnd:OnEnable()
	CShopMallMgr.Init()
end

function LuaSectHongBaoItemSelectWnd:OnDisable()
end

--@region UIEvent

function LuaSectHongBaoItemSelectWnd:OnQnButtonClick()
	LuaZongMenMgr.m_HongBaoItemList[self.m_ItemSelectPanelSelectRow + 1] = {
		itemId = self.m_CurrentItems[self.m_SelectRow].ItemId,
		num = self.QnIncreseAndDecreaseButton:GetValue(),
		price = self.QnCostAndOwnMoney:GetCost()
	}
	CUIManager.CloseUI(CLuaUIResources.SectHongBaoItemSelectWnd)
	g_ScriptEvent:BroadcastInLua("SetSectHongBaoItem")
end

function LuaSectHongBaoItemSelectWnd:OnQnRadioBoxSelected(btn, index)
	self.QnIncreseAndDecreaseButton:SetValue(1)
	self.m_CurrentItems = CShopMallMgr.GetLingYuMallInfo(index + 1, 0)
	local list = self.m_HongBaoDynamicOnShelfItemsSetData[index + 1]
	if list then
		for _,v in pairs(list) do
			local template = ShopMallTemlate()
			template.ItemId = v.ID
			template.Price = v.Jade
			template.Category = v.Category
			template.SubCategory = v.SubCategory
			template.PreSell = false
			template.Status = v.Status
			template.Index = v.Index
			template.FeiShengIndex = v.FeiShengIndex
			template.Discount = v.Discount
			template.AvalibleCount = -1
			template.CanZengSong = v.CanZengSong
			template.DeleteTime = v.DeleteTime
			template.CanUseBindJade = v.CanUseBindJade
			CommonDefs.ListAdd(self.m_CurrentItems, typeof(ShopMallTemlate), template)
		end
	end
	for i = self.m_CurrentItems.Count - 1, 0, -1 do
		local template = self.m_CurrentItems[i]
		local mallData = Mall_LingYuMall.GetData(template.ItemId)
		local canZengSongSectItem = mallData and (mallData.CanZengSongSectItem > 0)
		if (template.CanZengSong == 0) and not canZengSongSectItem then
			CommonDefs.ListRemoveAt(self.m_CurrentItems, i)
		end
	end
	self.QnTableView:ReloadData(true,true)
	self.QnTableView:SetSelectRow(0, true)
end

--@endregion UIEvent
function LuaSectHongBaoItemSelectWnd:InitItem(item, index)
	if not self.m_CurrentItems then
		item.gameObject:SetActive(false)
		return
	end
	item:UpdateData(self.m_CurrentItems[index], EnumMoneyType.LingYu, false, false)
	item.OnClickItemTexture = DelegateFactory.Action(function ()
		self.QnTableView:SetSelectRow(index, true)
	end)
end

function LuaSectHongBaoItemSelectWnd:OnSelectAtRow(row)
	self.m_SelectRow = row
	self:UpdateTotalPrice()
	self.ItemPreviewer:UpdateData(nil, 0, "")
	local template = self.m_CurrentItems[row]
	self.QnCostAndOwnMoney:SetType(template.CanUseBindJade > 0 and EnumMoneyType.LingYu_WithBind or EnumMoneyType.LingYu, EnumPlayScoreKey.NONE, true)
	local maxCount = 999
	local itemData = Item_Item.GetData(template.ItemId)
	if itemData then
		maxCount = math.min(maxCount,itemData.Overlap)
	end
	if template.AvalibleCount >= 0 then
		local avaliableCount = math.max(1, template.AvalibleCount)
		maxCount = math.min(maxCount, avaliableCount)
	end
	if template.SubCategory == LuaShopMallFashionPreviewMgr.EnumLingYuMallSubCategory.ShiZhuang or template.SubCategory == LuaShopMallFashionPreviewMgr.EnumLingYuMallSubCategory.ZuoQi then
		maxCount = 1
	end
	for _,itemId in pairs(self.m_LimitPurchaseQuantityItemIds) do
		if itemId == tostring(template.ItemId) then
			maxCount = 1
		end
	end
	self.QnIncreseAndDecreaseButton:SetMinMax(1, maxCount, 1)
	self.ItemPreviewer:UpdateData(template, 0, "")
end

function LuaSectHongBaoItemSelectWnd:OnQnIncreseAndDecreaseButtonValueChanged(value)
	self:UpdateTotalPrice()
end

function LuaSectHongBaoItemSelectWnd:UpdateTotalPrice()
	local totalprice = 0
    if self.m_CurrentItems and self.m_SelectRow and self.m_CurrentItems.Count > self.m_SelectRow and self.m_SelectRow >= 0 then
        totalprice = self.m_CurrentItems[self.m_SelectRow].Price * self.QnIncreseAndDecreaseButton:GetValue()
    end
    self.QnCostAndOwnMoney:SetCost(totalprice)
end

function LuaSectHongBaoItemSelectWnd:InitItemSelectPanelItem(obj, index)
	local data = LuaZongMenMgr.m_HongBaoItemList[index]
	obj.gameObject:SetActive(true)

	local itemId = data and data.itemId or 0
	
	local itemData = Item_Item.GetData(itemId)

	local item = obj.transform:Find("ItemCell")
	local iconTexture = item.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
	local qualitySprite = item.transform:Find("QualitySprite"):GetComponent(typeof(UISprite))
	local addSprite = item.transform:Find("AddSprite")
	local numLabel = item.transform:Find("NumLabel"):GetComponent(typeof(UILabel))

	numLabel.text = data and data.num or ""
	if itemData then
		iconTexture:LoadMaterial(itemData.Icon)
		qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(CItem.GetQualityType(itemId))
	end
	addSprite.gameObject:SetActive(not itemData)
	UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    if itemData then
			CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType2.Default, 0, 0, 0, 0)
		else

		end
		self.m_ItemSelectPanelRadioBox:ChangeTo(index - 1, true)
	end)
end
