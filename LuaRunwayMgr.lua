local MsgPackImpl=import "MsgPackImpl"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local EMoveType = import "L10.Engine.EMoveType"
local EMoveToResult = import "L10.Engine.EMoveToResult"
local Setting = import "L10.Engine.Setting"
local PathFindDefs = import "L10.Engine.PathFinding.PathFindDefs"
local CPos = import "L10.Engine.CPos"
local CHorseRaceMgr = import "L10.Game.CHorseRaceMgr"

CLuaRunwayMgr = class()

CLuaRunwayMgr.m_CurRegionId = 0

CLuaRunwayMgr.m_BaseVerticalSpeed = 5.0
CLuaRunwayMgr.m_BaseHorizontalSpeed = 2.0
CLuaRunwayMgr.m_BaseSpeedCos = 0.928
CLuaRunwayMgr.m_IsRegionCircle = false

-- regionInfo中记录的是最左,最右,中心点,线的方向(取延跑道向左的向量); 象征本区域起始线
-- {
--      left,
--      right,
--      center,
--      line,
-- }
-- 本区域终点则是下一区域的起始点
CLuaRunwayMgr.m_RegionInfo = {}

function CLuaRunwayMgr:AddListener()
    g_ScriptEvent:RemoveListener("MainPlayerMoveStepped", self, "TryNextRegion")
    g_ScriptEvent:RemoveListener("MainPlayerMoveEnded", self, "MainPlayerMoveEnded")
    g_ScriptEvent:RemoveListener("PlayerDragJoyStick", self, "OnChangeRunway")
    g_ScriptEvent:RemoveListener("PlayerDragJoyStickComplete", self, "OnChangeRunwayEnd")
    g_ScriptEvent:AddListener("MainPlayerMoveStepped", self, "TryNextRegion")
    g_ScriptEvent:AddListener("MainPlayerMoveEnded", self, "MainPlayerMoveEnded")
    g_ScriptEvent:AddListener("PlayerDragJoyStick", self, "OnChangeRunway")
    g_ScriptEvent:AddListener("PlayerDragJoyStickComplete", self, "OnChangeRunwayEnd")
end
CLuaRunwayMgr:AddListener()

function Vector3Sub(v1, v2)
    local ret = {}
    ret[1] = v1[1] - v2[1]
    ret[2] = v1[2] - v2[2]
    ret[3] = v1[3] - v2[3]
    return ret
end

function Vector3Add(v1, v2)
    local ret = {}
    ret[1] = v1[1] + v2[1]
    ret[2] = v1[2] + v2[2]
    ret[3] = v1[3] + v2[3]
    return ret
end

function Vector3Cross(v1, v2)
    return {
        v1[2] * v2[3] - v1[3] * v2[2],
        v1[3] * v2[1] - v1[1] * v2[3],
        v1[1] * v2[2] - v1[2] * v2[1],
    }
end

function Vector3Dot(v1, v2)
    return v1[1] * v2[1] + v1[2] * v2[2] + v1[3] * v2[3]
end

function Vector3Magnitude(v)
    return math.sqrt(v[1]*v[1] + v[2]*v[2] + v[3]*v[3])
end

function Vector3Magnitude2(v)
    return v[1]*v[1] + v[2]*v[2] + v[3]*v[3]
end

function Vector3Normalize(v)
    local length = Vector3Magnitude(v)
    if length == 0 then return {0,0,0} end
    return {
        v[1] / length,
        v[2] / length,
        v[3] / length
    }
end

function Vector3Mul(k, v)
    return {
        k * v[1],
        k * v[2],
        k * v[3],
    }
end

function Vector3Dist2(v1, v2)
    local d = Vector3Sub(v1, v2)
    return d[1]*d[1] + d[2]*d[2] + d[3]*d[3]
end
function Vector3Dist(v1, v2)
    local d = Vector3Sub(v1, v2)
    return Vector3Magnitude(d)
end
function IsLineParallel(lineAStart, lineAEnd, lineBStart, lineBEnd)
    local lineA = Vector3Sub(lineAEnd, lineAStart)
    local lineB = Vector3Sub(lineBEnd, lineBStart)
    local normalA = Vector3Cross({0,1,0}, lineA)
    return math.abs(Vector3Dot(normalA, lineB)) < 0.01
end

function GetLineIntersection(lineAStart, lineAEnd, lineBStart, lineBEnd)
    local lineANormal = Vector3Cross({0,1,0}, Vector3Sub(lineAEnd, lineAStart))
    local lineB = Vector3Sub(lineBEnd, lineBStart)

    local a = Vector3Dot(Vector3Sub(lineAStart, lineBStart), lineANormal)
    local b = Vector3Dot(lineB, lineANormal)
    if b == 0 then return end

    return Vector3Add(lineBStart, Vector3Mul(a / b, lineB))
end

function ToCenterOfGrid(v)
    return {
        math.floor(v[1]) + 0.5,
        0,
        math.floor(v[3]) + 0.5
    }
end

function CLuaRunwayMgr:DoMove(target, gridSpeed, moveType, bMoveEnd)
    if CClientMainPlayer.Inst == nil then
        return
    end
    local pixelSpeed = math.min(gridSpeed * Setting.eGridSpan, CClientMainPlayer.Inst.MaxPixelSpeed)

    local targetVec = CreateFromClass(CPos, target[1] * Setting.eGridSpan, target[3] * Setting.eGridSpan)
    local ret = CClientMainPlayer.Inst:MoveTo(targetVec, pixelSpeed, 0,0,0,
        PathFindDefs.EFindPathType.eFPT_HypoLine, CClientMainPlayer.Inst.BarrierType,
        false, moveType, PathFindDefs.MAX_REGION_LIMIT)

    if ret == EMoveToResult.NotAllowed then
        self.m_RetryMoveTick = RegisterTickOnce(function()
            if not (CClientMainPlayer.Inst and CClientMainPlayer.Inst.IsInRunWayAutoMove) then return end
            self:DoMove(target, gridSpeed, moveType, bMoveEnd)
        end, 50)
        return
    end

    if ret ~= EMoveToResult.Success and ret ~= EMoveToResult.SuperPosition then
        ret = CClientMainPlayer.Inst:MoveTo(targetVec, pixelSpeed, 0,0,0,
            PathFindDefs.EFindPathType.eFPT_HypoLineHelper, CClientMainPlayer.Inst.BarrierType,
            false, moveType, PathFindDefs.MAX_REGION_LIMIT)
        if ret ~= EMoveToResult.Success and ret ~= EMoveToResult.SuperPosition and not bMoveEnd then
            CClientMainPlayer.Inst:OnMoveEnded()
        end
    end
end

function CLuaRunwayMgr:IsRegionConvexQuadrangle(startLeft, startRight, endLeft, endRight)
    local startLine = Vector3Sub(startRight, startLeft)
    local startNorm = Vector3Cross({0,1,0}, startLine)
    local left      = Vector3Sub(endLeft, startLeft)
    local right     = Vector3Sub(endRight, startLeft)
    return Vector3Dot(left, startNorm) * Vector3Dot(right, startNorm) > 0
end

function CLuaRunwayMgr:GetRegionTarget(worldPos)
    local curRegionId = self.m_CurRegionId
    if curRegionId > #self.m_RegionInfo - 1 or curRegionId <= 0 then
        -- 这里就不修正了, 计算进下一个区域的地方应该保证这里能过, 所以直接返回
        return
    end
    local regionStartPos = self.m_RegionInfo[curRegionId]
    local regionEndPos = self.m_RegionInfo[curRegionId+1]

    local regionStartLeft      = regionStartPos.left
    local regionStartRight     = regionStartPos.right
    local regionEndLeft        = regionEndPos.left
    local regionEndRight       = regionEndPos.right

    -- 两边平行 / 凹四边形
    local res
    if IsLineParallel(regionStartLeft, regionEndLeft, regionStartRight, regionEndRight) or
        not self:IsRegionConvexQuadrangle(regionStartLeft, regionStartRight, regionEndLeft, regionEndRight) then
        local regionDir = Vector3Sub(regionEndPos.center, regionStartPos.center)
        regionDir = Vector3Add(worldPos, regionDir)
        res = GetLineIntersection(worldPos, regionDir, regionEndLeft, regionEndRight)
    else
        local runwayInterSect = GetLineIntersection(regionStartLeft, regionEndLeft, regionStartRight, regionEndRight)
        res = GetLineIntersection(runwayInterSect, worldPos, regionEndLeft, regionEndRight)
    end

    return res
end

-- 自动沿跑道方向运动
function CLuaRunwayMgr:DoAutoMove(bMoveEnd)
    local speed = CClientMainPlayer.Inst.MaxSpeed

    local worldPosVector = CClientMainPlayer.Inst.WorldPos
    local worldPos = {worldPosVector.x, 0, worldPosVector.z}

    local target = self:GetRegionTarget(worldPos)
    if not target then return end
    self:DoMove(target, speed, EMoveType.Normal, bMoveEnd)
end

function CLuaRunwayMgr:OnChangeRunway(args)
    if not CClientMainPlayer.Inst.IsInRunWayAutoMove then return end

    local Dir = args[0]
    local dir = {Dir.x, Dir.y, Dir.z}

    local curRegionId = self.m_CurRegionId
    if curRegionId > #self.m_RegionInfo - 1 or curRegionId <= 0 then
        return
    end

    local speed = CClientMainPlayer.Inst.MaxSpeed
    local worldPosVector = CClientMainPlayer.Inst.WorldPos
    local worldPos = {worldPosVector.x, 0, worldPosVector.z}

    local target = self:GetRegionTarget(worldPos)
    if not target then return end

    local currentWay = Vector3Sub(target, worldPos)
    local length = Vector3Magnitude(currentWay)
    local vertical
    if length ~= 0 then
        vertical = Vector3Mul(1 / length, currentWay)
    else
        vertical = {0, 0, 0}
    end
    local horizontal = Vector3Cross({0,1,0}, vertical)

    local horizontalDot = Vector3Dot(dir, horizontal)
    local dragdir = 0
    if horizontalDot < -0.3 then
        dragdir = horizontalDot
    elseif horizontalDot > 0.3 then
        dragdir = horizontalDot 
    end

    vertical = Vector3Mul(self.m_BaseVerticalSpeed, vertical)
    horizontal = Vector3Mul(dragdir * self.m_BaseHorizontalSpeed, horizontal)
    local finalDir = Vector3Add(horizontal, vertical)
    local finalTarget = Vector3Add(worldPos, Vector3Mul(2, Vector3Normalize(finalDir)))
    if dragdir == 0 then
        self:DoMove(finalTarget, speed, EMoveType.Normal)
    else
        self:DoMove(finalTarget, speed, EMoveType.Normal)
    end
end

function CLuaRunwayMgr:OnChangeRunwayEnd()
    if not CClientMainPlayer.Inst.IsInRunWayAutoMove then return end
    self:DoAutoMove()
end

function CLuaRunwayMgr:TryNextRegion()
    if not CClientMainPlayer.Inst.IsInRunWayAutoMove then return end

    local worldPosVector = CClientMainPlayer.Inst.WorldPos
    local worldPos = {worldPosVector.x, 0, worldPosVector.z}

    local curRegionId = self.m_CurRegionId
    if curRegionId > #self.m_RegionInfo - 1 or curRegionId <= 0 then
        return
    end

    local lineCenter = self.m_RegionInfo[curRegionId+1].center
    local line = self.m_RegionInfo[curRegionId+1].line

    local cur2center = Vector3Sub(worldPos, lineCenter)
    local dist2 = Vector3Magnitude2(cur2center)
    local dot = Vector3Dot(line, cur2center)
    if dist2 - dot*dot < 0.5 then
        self:EnterNextRegion()
        return true
    end
end

function CLuaRunwayMgr:EnterNextRegion()
    local regionId = self.m_CurRegionId + 1
    if regionId == #self.m_RegionInfo then
        if self.m_IsRegionCircle then
            regionId = 1
        else
            CClientMainPlayer.Inst.IsInRunWayAutoMove = false;
            self.m_CurRegionId = 0
            CClientMainPlayer.Inst:StopMove()
            return
        end
    end

    self.m_CurRegionId = regionId
    g_ScriptEvent:BroadcastInLua("RunwayMgrEnterNextRegion", regionId)
    self:DoAutoMove()
end

function CLuaRunwayMgr:MainPlayerMoveEnded()
    if not CClientMainPlayer.Inst.IsInRunWayAutoMove then return end

    if not self:TryNextRegion() then
        self:DoAutoMove(true)
    end
end

function CLuaRunwayMgr:GetRegionDir(regionId)
    local info = self.m_RegionInfo[regionId]
    local nextInfo = self.m_RegionInfo[regionId + 1]

    if info then
        local dir = Vector3Normalize(Vector3Sub(nextInfo.center ,info.center))
        return math.atan2(dir[3], dir[1]) * 180 / math.pi
    end

    return nil
end

-- 赛马

-- 不假设区域是闭环, 所以 #m_RegionInfo = region_count + 1 
-- 其他玩法可以调整起点线和终点线的设置, 只要保持结构一致就行
-- 传下来的坐标是格子中心坐标
function Gas2Gac.InitSaiMaSequence(regionUD)
    if CClientMainPlayer.Inst == nil then
        return
    end

    local list = MsgPackImpl.unpack(regionUD)
    if list.Count < 8 then return end

    CLuaRunwayMgr.m_RegionInfo = {}
    for i = 0, list.Count - 1, 4 do
        local left = {tonumber(list[i]), 0, tonumber(list[i+1])}
        local right = {tonumber(list[i+2]), 0, tonumber(list[i+3])}
        local line = Vector3Normalize(Vector3Sub(right, left))
        local center = Vector3Mul(0.5, Vector3Add(right, left))
        local data = {
            center = center,
            line = line,
            left = left,
            right = right,
        }
        table.insert(CLuaRunwayMgr.m_RegionInfo, data)
    end
end

function Gas2Gac.StartSaiMa(vVertical, vHorizontal, curRegionId, startPlayTime)
    CHorseRaceMgr.Inst.playStartTime = startPlayTime
    CHorseRaceMgr.Inst.currentRank = 0
    CHorseRaceMgr.Inst.progress = 0
    CUIManager.CloseUI(CUIResources.HorseRaceSelectWnd)
    CUIManager.ShowUI(CLuaUIResources.NewHorseRaceBaseWnd)

    CLuaRunwayMgr.m_BaseVerticalSpeed   = vVertical
    CLuaRunwayMgr.m_BaseHorizontalSpeed = vHorizontal
    CLuaRunwayMgr.m_BaseSpeedCos = vVertical / math.sqrt(vVertical*vVertical + vHorizontal*vHorizontal)
    CLuaRunwayMgr.m_CurRegionId = curRegionId
    CLuaRunwayMgr.m_IsRegionCircle = true
    CClientMainPlayer.Inst.IsInRunWayAutoMove = true

    CLuaRunwayMgr:DoAutoMove()
end

function Gas2Gac.EndSaiMa()
    CLuaRunwayMgr.m_CurRegionId = 1
    CLuaRunwayMgr.m_IsRegionCircle = false
    CClientMainPlayer.Inst.IsInRunWayAutoMove = false
    CClientMainPlayer.Inst:StopMove();
end
