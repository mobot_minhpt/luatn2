local CJoystickWnd = import "L10.UI.CJoystickWnd"
local PlayerSettings = import "L10.Game.PlayerSettings"
local CGuideMgr = import "L10.Game.Guide.CGuideMgr"
local EnumGender = import "L10.Game.EnumGender"
local CZuoQiMgr = import "L10.Game.CZuoQiMgr"
local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"
local COpenEntryMgr = import "L10.Game.COpenEntryMgr"
local EnumMenuItem = import "L10.Game.EnumMenuItem"
local CScene = import "L10.Game.CScene"

CJoystickWnd.m_hookOnEnable = function (this)
    CLuaJoystickWnd:OnEnable(this)
    this:IPhoneXAdaptation()

    this.joystick.OnDragging = CommonDefs.CombineListner_Action_Vector3(this.joystick.OnDragging, MakeDelegateFromCSFunction(this.OnDragging, MakeGenericClass(Action1, Vector3), this), true)
    this.joystick.OnDragFinish = CommonDefs.CombineListner_Action(this.joystick.OnDragFinish, MakeDelegateFromCSFunction(this.OnDragFinish, Action0, this), true)
    this.joystick.OnDragStart = CommonDefs.CombineListner_Action(this.joystick.OnDragStart, MakeDelegateFromCSFunction(this.OnDragStart, Action0, this), true)

    EventManager.AddListener(EnumEventType.JoyStickEnableStatusChanged, MakeDelegateFromCSFunction(this.OnJoyStickEnableStatusChanged, Action0, this))
    EventManager.AddListener(EnumEventType.EnterNewbiePlay, MakeDelegateFromCSFunction(this.OnJoyStickEnableStatusChanged, Action0, this))
    EventManager.AddListener(EnumEventType.LeaveNewbiePlay, MakeDelegateFromCSFunction(this.OnJoyStickEnableStatusChanged, Action0, this))
    EventManager.AddListener(EnumEventType.MainPlayerCreated, MakeDelegateFromCSFunction(this.UpdateVisible, Action0, this))
    EventManager.AddListener(EnumEventType.RenderSceneInit, MakeDelegateFromCSFunction(this.UpdateVisible, Action0, this))
    this:OnJoyStickEnableStatusChanged()
    CLuaJoystickWnd:InitTransformZuoQiBtnsView()
end

CJoystickWnd.m_hookOnDisable = function (this)
    CLuaJoystickWnd:OnDisable(this)
    this.joystick.OnDragging = CommonDefs.CombineListner_Action_Vector3(this.joystick.OnDragging, MakeDelegateFromCSFunction(this.OnDragging, MakeGenericClass(Action1, Vector3), this), false)
    this.joystick.OnDragFinish = CommonDefs.CombineListner_Action(this.joystick.OnDragFinish, MakeDelegateFromCSFunction(this.OnDragFinish, Action0, this), false)
    this.joystick.OnDragStart = CommonDefs.CombineListner_Action(this.joystick.OnDragStart, MakeDelegateFromCSFunction(this.OnDragStart, Action0, this), false)

    EventManager.RemoveListener(EnumEventType.JoyStickEnableStatusChanged, MakeDelegateFromCSFunction(this.OnJoyStickEnableStatusChanged, Action0, this))
    EventManager.RemoveListener(EnumEventType.EnterNewbiePlay, MakeDelegateFromCSFunction(this.OnJoyStickEnableStatusChanged, Action0, this))
    EventManager.RemoveListener(EnumEventType.LeaveNewbiePlay, MakeDelegateFromCSFunction(this.OnJoyStickEnableStatusChanged, Action0, this))
    EventManager.RemoveListener(EnumEventType.MainPlayerCreated, MakeDelegateFromCSFunction(this.UpdateVisible, Action0, this))
    EventManager.RemoveListener(EnumEventType.RenderSceneInit, MakeDelegateFromCSFunction(this.UpdateVisible, Action0, this))
end

CJoystickWnd.m_hookInit = function (this)
    CLuaJoystickWnd:Init()
    this.joystick.gameObject:SetActive(PlayerSettings.JoystickEnabled or CGuideMgr.Inst:IsNewbiePlayId())
end

CJoystickWnd.m_hookSetMountButtonVisible = function(this, visible)
    this.m_AllowShowButton = visible

    this:RefreshMountButtonVisibility()
    CLuaJoystickWnd:UpdateTransformButton()
end

CJoystickWnd.m_hookOnMountButtonClick = function(this, go)
    CLuaJoystickWnd:OnMountButtonClick()
end

CJoystickWnd.m_hookRefreshMountButtonVisibility = function(this)
    if CQuanMinPKMgr.Inst.m_IsQuanMinPKServer then
        this.mountBtn:SetActive(false)
        return
    end
    local mountVisible = this.m_ForceHideButtonSet.Count == 0 and this.m_AllowShowButton and COpenEntryMgr.Inst:IsMenuItemVisible(EnumMenuItem.ZuoQi) and not CGuideMgr.Inst:IsNewbiePlayId()
    if mountVisible and CScene.MainScene then
        mountVisible = mountVisible and CScene.MainScene.AllowZuoQi
    end
    this.mountBtn:SetActive(mountVisible)
    CLuaJoystickWnd:UpdateTransformZuoQiBtnsViewVisibility(mountVisible)
end

--------------------
-- 扩展CJoystickWnd
--------------------

CLuaJoystickWnd = {}
CLuaJoystickWnd.m_Wnd = nil
CLuaJoystickWnd.transformButton = nil
CLuaJoystickWnd.transformZuoQiBtnsView = nil
CLuaJoystickWnd.transformZuoQiBtnsViewRideOffBtn = nil
CLuaJoystickWnd.transformZuoQiBtnsViewZuoQiTransformBtn = nil
CLuaJoystickWnd.showTransformZuoQiBtnsView = false
CLuaJoystickWnd.transformZuoQiBtnsViewInitialPosX = -350
CLuaJoystickWnd.waitMainPlayerCreatedTime = 0

function CLuaJoystickWnd:OnEnable(wnd)
    self.m_Wnd = wnd
    g_ScriptEvent:AddListener("OnSyncMainPlayerAppearanceProp", self, "UpdateTransformButton")
    g_ScriptEvent:AddListener("MainPlayerCreated", self, "UpdateTransformButton")
    g_ScriptEvent:AddListener("MainPlayerGetInVehicleAsDriver", self, "OnMainPlayerGetInVehicleAsDriver")
    g_ScriptEvent:AddListener("OnRideOnOffVehicle", self, "OnRideOnOffVehicle")
end

function CLuaJoystickWnd:OnDisable()
    self.m_Wnd = nil
    self.transformZuoQiBtnsView = nil
    g_ScriptEvent:RemoveListener("OnSyncMainPlayerAppearanceProp", self, "UpdateTransformButton")
    g_ScriptEvent:RemoveListener("MainPlayerCreated", self, "UpdateTransformButton")
    g_ScriptEvent:RemoveListener("MainPlayerGetInVehicleAsDriver", self, "OnMainPlayerGetInVehicleAsDriver")
    g_ScriptEvent:RemoveListener("OnRideOnOffVehicle", self, "OnRideOnOffVehicle")
end

function CLuaJoystickWnd:Init()
    self.transformButton = self.m_Wnd.transform:Find("Anchor/LeftBottomBtnsPanel/TransformBtn"):GetComponent(typeof(QnButton))
    self.transformButton.OnClick = DelegateFactory.Action_QnButton(function(go)
        self:OnTransformButtonClick()
    end)
    self:UpdateTransformButton()
    self.waitMainPlayerCreatedTime = Time.realtimeSinceStartup + 3
end

function CLuaJoystickWnd:InitTransformZuoQiBtnsView()
    if not self.m_Wnd then return end
    self.transformZuoQiBtnsView = self.m_Wnd.transform:Find("Anchor/JoystickZuoQiTransformPanel/TransformZuoQiBtnsView").gameObject
    self.transformZuoQiBtnsViewRideOffBtn = self.transformZuoQiBtnsView.transform:Find("RideOffBtn").gameObject
    self.transformZuoQiBtnsViewZuoQiTransformBtn = self.transformZuoQiBtnsView.transform:Find("ZuoQiTransformBtn").gameObject
    UIEventListener.Get(self.transformZuoQiBtnsViewRideOffBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRideOffBtnClick()
	end)
    UIEventListener.Get(self.transformZuoQiBtnsViewZuoQiTransformBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnZuoQiTransformBtnClick()
	end)
    self.showTransformZuoQiBtnsView = false
    Extensions.SetLocalPositionX(self.transformZuoQiBtnsView.transform, -800)
    self.transformZuoQiBtnsView:SetActive(self.m_Wnd.mountBtn.gameObject.activeSelf)
end

function CLuaJoystickWnd:UpdateTransformZuoQiBtnsViewVisibility(visibility)
    if not self.m_Wnd then return end
    if not self.transformZuoQiBtnsView then return end
    self.transformZuoQiBtnsView:SetActive(visibility)
end

function CLuaJoystickWnd:ShowTransformZuoQiBtnsView()
    self.showTransformZuoQiBtnsView = true
    if not self.m_Wnd then return end
    LuaTweenUtils.DOKill(self.transformZuoQiBtnsView.transform, false)
    local tween = LuaTweenUtils.DOLocalMoveX(self.transformZuoQiBtnsView.transform, self.transformZuoQiBtnsViewInitialPosX, Constants.GeneralAnimationDuration, false)
    LuaTweenUtils.SetTarget(tween, self.transformZuoQiBtnsView.transform)
end

function CLuaJoystickWnd:HideTransformZuoQiBtnsView()
    self.showTransformZuoQiBtnsView = false
    if not self.m_Wnd then return end
    LuaTweenUtils.DOKill(self.transformZuoQiBtnsView.transform, false)
    local tween = LuaTweenUtils.DOLocalMoveX(self.transformZuoQiBtnsView.transform, -800, Constants.GeneralAnimationDuration, false)
    LuaTweenUtils.SetTarget(tween, self.transformZuoQiBtnsView.transform)
end

function CLuaJoystickWnd:UpdateTransformButton()
    if IsTransformableFashionOpen() and CClientMainPlayer.Inst and self.m_Wnd and self.m_Wnd.m_AllowShowButton then
        local headId = CClientMainPlayer.Inst.AppearanceProp.HeadFashionId
        local bodyId = CClientMainPlayer.Inst.AppearanceProp.BodyFashionId
        local head = Fashion_Fashion.GetData(headId)
        local body = Fashion_Fashion.GetData(bodyId)
        local transformable = false
        if body and head and body.Appearance == head.Appearance then
            if CClientMainPlayer.Inst.Gender == EnumGender.Male then
                transformable = body.TransformMonsterMale ~= 0 and head.TransformMonsterMale ~= 0 and body.TransformMonsterMale == head.TransformMonsterMale
            else
                transformable = body.TransformMonsterFemale ~= 0 and head.TransformMonsterFemale ~= 0 and body.TransformMonsterFemale == head.TransformMonsterFemale
            end
        end

        if transformable == true then --主界面按钮显示时触发引导
            CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.TransformFashion)
        end
        self.transformButton.gameObject:SetActive(transformable)
    else
		self.transformButton.gameObject:SetActive(false)
	end
end

function CLuaJoystickWnd:OnTransformButtonClick()
    if not CClientMainPlayer.Inst.IsTransformOn then
        Gac2Gas.DoFashionTransform(true)
    else
        Gac2Gas.DoFashionTransform(false)
    end
end

function CLuaJoystickWnd:OnMountButtonClick()
    if not CGuideMgr.Inst:IsInPhase(EnumGuideKey.ZuoQi) and self.transformZuoQiBtnsView.activeSelf then
        if self.showTransformZuoQiBtnsView and self.m_Wnd then
            self:HideTransformZuoQiBtnsView()
            return
        elseif CClientMainPlayer.Inst and CClientMainPlayer.Inst:IsInVehicle() and CZuoQiMgr.IsTransformZuoQi(CClientMainPlayer.Inst.AppearanceProp.ZuoQiTemplateId) then
            self:ShowTransformZuoQiBtnsView()
            return
        end
    end
    CZuoQiMgr.Inst:RideOnOrOff()
end

function CLuaJoystickWnd:OnRideOffBtnClick()
    CZuoQiMgr.Inst:RideOnOrOff()
end

function CLuaJoystickWnd:OnZuoQiTransformBtnClick()
    if not self.m_Wnd then return end
    if CClientMainPlayer.Inst then
        local data = ZuoQi_Transformers.GetData(CClientMainPlayer.Inst.AppearanceProp.ZuoQiTemplateId)
        Gac2Gas.RequestSwitchZuoQiPassengerMode(CClientMainPlayer.Inst.BasicProp.CurrentZuoQiId, data and 1 or 0)
    end
end

function CLuaJoystickWnd:OnMainPlayerGetInVehicleAsDriver()
    if not self.m_Wnd then return end
    if self.waitMainPlayerCreatedTime and self.waitMainPlayerCreatedTime > Time.realtimeSinceStartup then
        if self.showTransformZuoQiBtnsView then
            self:HideTransformZuoQiBtnsView()
        end 
        return 
    end
    if CClientMainPlayer.Inst and CZuoQiMgr.IsTransformZuoQi(CClientMainPlayer.Inst.AppearanceProp.ZuoQiTemplateId) then
        if self.showTransformZuoQiBtnsView then
            self:HideTransformZuoQiBtnsView()
        else
            self:ShowTransformZuoQiBtnsView()
        end
    end
end

function CLuaJoystickWnd:OnRideOnOffVehicle()
    if not self.m_Wnd then return end
    if self.showTransformZuoQiBtnsView then
        self:HideTransformZuoQiBtnsView()
    end
end