local Extensions = import "Extensions"
local UISlider = import "UISlider"
local RotateMode = import "DG.Tweening.RotateMode"
local UILongPressButton = import "L10.UI.UILongPressButton"
local Object = import "System.Object"
local UITexture = import "UITexture"
local CGuideMgr = import "L10.Game.Guide.CGuideMgr"

LuaPourWineWnd = class()

RegistChildComponent(LuaPourWineWnd,"m_Flagon","Flagon", UILongPressButton)
RegistChildComponent(LuaPourWineWnd,"m_CountDownLabel","CountDownLabel", UILabel)
RegistChildComponent(LuaPourWineWnd,"m_ProgressBar","ProgressBar", UISlider)
RegistChildComponent(LuaPourWineWnd,"m_OverflowEffect","OverflowEffect", CUIFx)
RegistChildComponent(LuaPourWineWnd,"m_AimInterval","AimInterval", UISprite)
RegistChildComponent(LuaPourWineWnd,"m_ProgressLabel","ProgressLabel", UILabel)
RegistChildComponent(LuaPourWineWnd,"m_WineGlass","WineGlass", UITexture)
RegistChildComponent(LuaPourWineWnd,"m_PourWineEffect","PourWineEffect", CUIFx)
RegistChildComponent(LuaPourWineWnd,"m_BlueSliderForeground","BlueSliderForeground", UISprite)
RegistChildComponent(LuaPourWineWnd,"m_RedSliderForeground","RedSliderForeground", UISprite)

RegistClassMember(LuaPourWineWnd, "m_CountDownTick")
RegistClassMember(LuaPourWineWnd, "m_CountDownTime")
RegistClassMember(LuaPourWineWnd, "m_CupNum")
RegistClassMember(LuaPourWineWnd, "m_PourWineTimes")
RegistClassMember(LuaPourWineWnd, "m_GrowthProgressTween")
RegistClassMember(LuaPourWineWnd, "m_IsPouringWine")
RegistClassMember(LuaPourWineWnd, "m_IsSwitchNextCup")
RegistClassMember(LuaPourWineWnd, "m_IsFail")

function LuaPourWineWnd:Init()
    self.m_CupNum = 1
    self.m_ProgressBar.value = 0
    self.m_PourWineTimes = 0
    self.m_BlueSliderForeground.gameObject:SetActive(true)
    self.m_RedSliderForeground.gameObject:SetActive(false)
    self:ShowCupProgressLabel()
    self:InitCountDownLabel()

    self.m_Flagon.OnLongPressDelegate = DelegateFactory.Action(function ()
        self:OnFlagonPressStart()
    end)
    self.m_Flagon.OnLongPressEndDelegate = DelegateFactory.Action(function ()
        self:OnLongPressEnd()
    end)

    CGuideMgr.Inst:StartGuide(EnumGuideKey.PourWineWnd, 0)
end

function LuaPourWineWnd:OnDisable()
    self:CancelCountDownTick()
    LuaTweenUtils.DOKill(self.m_Flagon.transform, false)
    LuaTweenUtils.DOKill(self.transform, false)
end

function LuaPourWineWnd:OnFlagonPressStart()
    self:PourWine()
end

function LuaPourWineWnd:OnLongPressEnd()
    self:EndPourWine()
end

function LuaPourWineWnd:InitCountDownLabel()
    self.m_CountDownTime = 30
    self.m_CountDownLabel.text = tostring(self.m_CountDownTime)
    self:CancelCountDownTick()
    self.m_CountDownTick = RegisterTick(function ()
        self.m_CountDownTime = self.m_CountDownTime - 1
        self.m_CountDownLabel.text = tostring(self.m_CountDownTime)
        if self.m_CountDownTime <= 0 then
            CUIManager.CloseUI(CLuaUIResources.PourWineWnd)
        end
    end, 1000)
end

function LuaPourWineWnd:CancelCountDownTick()
    if self.m_CountDownTick then
        UnRegisterTick(self.m_CountDownTick)
        self.m_CountDownTick = nil
    end
end

function LuaPourWineWnd:PourWine()
    if self.m_IsPouringWine or self.m_IsSwitchNextCup then return end
    self.m_PourWineEffect:DestroyFx()
    self.m_IsPouringWine = true

    local tween = LuaTweenUtils.DORotate(self.m_Flagon.transform, Vector3(0,0,45), 0.5, RotateMode.Fast)
    LuaTweenUtils.SetTarget(tween, self.m_Flagon.transform)
    CommonDefs.OnComplete_Tweener(tween, DelegateFactory.TweenCallback(function ()
        self.m_PourWineEffect:LoadFx("fx/ui/prefab/ui_daojiu05.prefab")
        self:GrowProgress(ZhuJueJuQing_Setting.GetData().PourWineTime[self.m_CupNum - 1])
    end))
end

function LuaPourWineWnd:EndPourWine()
    self.m_PourWineEffect:DestroyFx()
    self:EndGrowProgress()

    if self.m_PourWineTimes == self.m_CupNum then
        if (self.m_ProgressBar.value > 0.8) or (self.m_ProgressBar.value < 0.6) then
            self:FinishTask(0)
        else
            if self.m_CupNum < 3 then
                self.m_CupNum = self.m_CupNum + 1
                self:ShowCupProgressLabel()
                self:SwitchNextCup()
            elseif self.m_CupNum == 3 then
                self:FinishTask(1)
            end
        end
    end

    LuaTweenUtils.DOKill(self.m_Flagon.transform, false)
    local tween = LuaTweenUtils.TweenRotationZ(self.m_Flagon.transform,25, 0.1)
    LuaTweenUtils.SetTarget(tween, self.m_Flagon.transform)
    LuaTweenUtils.OnComplete(tween, function ( ... )
        self.m_IsPouringWine = false
    end)
end

function LuaPourWineWnd:FinishTask(isSuccess)
    g_MessageMgr:ShowMessage((isSuccess == 0) and "PourWine_Task_Failed" or "PourWine_Task_Finsh_Success")
    local list = CreateFromClass(MakeGenericClass(List, Object))
    CommonDefs.ListAdd(list, typeof(Object), isSuccess)
    Gac2Gas.FinishClientTaskEvent(CLuaTaskMgr.m_PourWineWnd_TaskId,"PourWine",MsgPackImpl.pack(list))
    CUIManager.CloseUI(CLuaUIResources.PourWineWnd)
end

function LuaPourWineWnd:GrowProgress(time)
    self.m_PourWineTimes = self.m_PourWineTimes + 1
    self:EndGrowProgress()
    local startValue = self.m_ProgressBar.value
    local len = 1 - startValue
    local delay = 0
    if (len > 0) and (time > 0 ) then
        delay = len * time
    end
    self.m_GrowthProgressTween = LuaTweenUtils.TweenFloat(startValue, 1, delay, function (v)
        self.m_ProgressBar.value = v
        if not self.m_IsFail then
            self.m_IsFail = v > 0.8
            if self.m_IsFail then
                self.m_BlueSliderForeground.gameObject:SetActive(false)
                self.m_RedSliderForeground.gameObject:SetActive(true)
                self.m_ProgressBar.foregroundWidget = self.m_RedSliderForeground
            end
        else
            if self.m_ProgressBar.value == 1 then
                self.m_OverflowEffect:LoadFx("fx/ui/prefab/ui_daojiu06.prefab")
            end
        end
    end)
    LuaTweenUtils.SetTarget(self.m_GrowthProgressTween, self.transform)
end

function LuaPourWineWnd:EndGrowProgress()
    if self.m_GrowthProgressTween then
        LuaTweenUtils.Kill(self.m_GrowthProgressTween, false)
        self.m_GrowthProgressTween = nil
    end
end

function LuaPourWineWnd:SwitchNextCup()
    g_MessageMgr:ShowMessage("PourWine_Switch_NextCup")
    self.m_IsSwitchNextCup = true

    LuaTweenUtils.TweenAlpha(self.m_WineGlass, self.transform, 1, 0, 0.5, function ()
        LuaTweenUtils.TweenAlpha(self.m_WineGlass, self.transform, 0, 1, 0.2, function ()
            local tween = LuaTweenUtils.TweenFloat(200, 0, 0.5, function (v)
                Extensions.SetLocalPositionX(self.m_WineGlass.transform, v)
            end)
            LuaTweenUtils.SetTarget(tween, self.transform)
            LuaTweenUtils.OnComplete(tween, function ( ... )
                self.m_IsSwitchNextCup = false
                self.m_ProgressBar.value = 0
            end)
        end)
    end)
end

function LuaPourWineWnd:ShowCupProgressLabel()
    self.m_ProgressLabel.text = SafeStringFormat3(LocalString.GetString("第%d/3杯"), self.m_CupNum)
end

--------------------------------------------
---引导相关
--------------------------------------------
function LuaPourWineWnd:GetGuideGo(methodName)
    if methodName == "GetFlagon" then
        return self.m_Flagon.gameObject
    elseif methodName == "GetAimInterval" then
        return self.m_AimInterval.gameObject
    end
end
