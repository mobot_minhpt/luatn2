-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"
local Color = import "UnityEngine.Color"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CXialvPKResultPlayerTemplate = import "L10.UI.CXialvPKResultPlayerTemplate"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CXialvPKResultPlayerTemplate.m_Init_CS2LuaHook = function (this, playerId, playerName, cla, gender, level) 
    this.PlayerId = playerId
    local player = CClientPlayerMgr.Inst:GetPlayer(playerId)

    this:InitWithInfo(cla, gender, level, playerName, playerId == CClientMainPlayer.Inst.Id)

    UIEventListener.Get(this.icon.gameObject).onClick = MakeDelegateFromCSFunction(this.OnPlayerClick, VoidDelegate, this)
end
CXialvPKResultPlayerTemplate.m_InitWithInfo_CS2LuaHook = function (this, cla, gender, level, name, isMainPlayer) 

    local ret = CUICommonDef.GetPortraitName(cla, gender, -1)

    if not System.String.IsNullOrEmpty(ret) then
        this.icon:LoadNPCPortrait(ret, false)
    end
    this.grade.text = tostring(level)
    this.playerName.text = name
    if isMainPlayer then
        this.playerName.color = Color.green
    else
        this.playerName.color = Color.white
    end
end
