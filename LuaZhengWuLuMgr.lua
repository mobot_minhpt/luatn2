
LuaZhengWuLuMgr = {}

function LuaZhengWuLuMgr.Init()
    LuaZhengWuLuMgr.Infos = {}
    WorldEvent_zhengwulu.Foreach(function (id, data)
        if LuaZhengWuLuMgr.Infos[data.Type] == nil then LuaZhengWuLuMgr.Infos[data.Type] = {} end
        local readed = CClientMainPlayer.Inst.PlayProp.ZhengWuLuViewInfo:GetBit(id)
        local unlocked = CClientMainPlayer.Inst.PlayProp.ZhengWuLuUnlockInfo:GetBit(id)
        table.insert(LuaZhengWuLuMgr.Infos[data.Type], {ID = id, Type=data.Type, Name=data.Name, 
                                                        Icon=data.Icon, Description=data.Describe, 
                                                        Open=data.Open, Unlocked=unlocked, Readed=readed})
    end)
end

function LuaZhengWuLuMgr.OpenMainWnd(id)
    LuaZhengWuLuMgr.Init()
    LuaZhengWuLuMgr.highlight = id
    CUIManager.ShowUI(CLuaUIResources.ZhengWuLuWnd)
end

function LuaZhengWuLuMgr.OpenTipWnd(id)
    for _, infos in ipairs(LuaZhengWuLuMgr.Infos) do
        for _, info in ipairs(infos) do
            if id == info.ID then
                LuaZhengWuLuMgr.selectedInfo = info
                if info.Readed == false then
                    Gac2Gas.ViewZhengWuLuItem(id)
                end
                CUIManager.ShowUI(CLuaUIResources.ZhengWuLuTipWnd)
                break
            end
        end
    end
end

function LuaZhengWuLuMgr.UpdateState(zhengWuId, unlocked, readed)
    if not CUIManager.IsLoaded(CLuaUIResources.ZhengWuLuWnd) then
        LuaZhengWuLuMgr.OpenMainWnd(zhengWuId)
    end
    for _, infos in ipairs(LuaZhengWuLuMgr.Infos) do
        for _, info in ipairs(infos) do
            if zhengWuId == info.ID then
                info.Readed = readed
                info.Unlocked = unlocked
                g_ScriptEvent:BroadcastInLua("SyncZhengWuLuItemStatus", zhengWuId)
                return
            end
        end
    end
end
