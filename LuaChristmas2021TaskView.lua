local UITable 				= import "UITable"
local DelegateFactory  		= import "DelegateFactory"
local UISprite 				= import "UISprite"
local UILabel 				= import "UILabel"
local GameObject 			= import "UnityEngine.GameObject"
local CScene                = import "L10.Game.CScene"
local CGamePlayMgr          = import "L10.Game.CGamePlayMgr"
local CChatLinkMgr 			= import "CChatLinkMgr"

LuaChristmas2021TaskView = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaChristmas2021TaskView, "BgTexture", "BgTexture", UISprite)
RegistChildComponent(LuaChristmas2021TaskView, "TaskName", "TaskName", UILabel)
RegistChildComponent(LuaChristmas2021TaskView, "RightBtn", "RightBtn", GameObject)
RegistChildComponent(LuaChristmas2021TaskView, "RightBtnLabel", "RightBtnLabel", UILabel)
RegistChildComponent(LuaChristmas2021TaskView, "ContentLabel", "ContentLabel", UILabel)
RegistChildComponent(LuaChristmas2021TaskView, "StageNameLabel", "StageNameLabel", UILabel)
RegistChildComponent(LuaChristmas2021TaskView, "Table", "Table", UITable)
RegistChildComponent(LuaChristmas2021TaskView, "LeftBtn", "LeftBtn", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaChristmas2021TaskView,"m_LastGamePlayId")
function LuaChristmas2021TaskView:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.RightBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRightBtnClick()
	end)


    --@endregion EventBind end
	UIEventListener.Get(self.LeftBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLeftBtnClick()
	end)
end

function LuaChristmas2021TaskView:OnEnable()
	g_ScriptEvent:AddListener("UpdateGamePlayStageInfo",self,"OnUpdateGamePlayStageInfo")
	g_ScriptEvent:AddListener("UpdateTempPlayTimesWithKey", self, "OnUpdateChristmasMJSYTask")
	g_ScriptEvent:AddListener("UpdateRunningTangYuanTask", self, "OnUpdateRunningTangYuanTask")
	g_ScriptEvent:AddListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
end

function LuaChristmas2021TaskView:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateGamePlayStageInfo",self,"OnUpdateGamePlayStageInfo")
	g_ScriptEvent:RemoveListener("UpdateTempPlayTimesWithKey",self,"OnUpdateChristmasMJSYTask")
	g_ScriptEvent:RemoveListener("UpdateRunningTangYuanTask", self, "OnUpdateRunningTangYuanTask")
	g_ScriptEvent:RemoveListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
	self.m_LastTime =  nil
end
function LuaChristmas2021TaskView:Init()
	--梦境识愿望
	if LuaChristmas2021Mgr.IsInMJSYPlay() then
		self:OnUpdateChristmasMJSYTask()
	end
	--圣诞星语
	if LuaChristmas2021Mgr.IsInXingYuPlay() then
		self:OnUpdateChristmasXingYuTask()
	end
	--狂奔的汤圆
	if LuaTangYuan2022Mgr.IsInPlay() then
		self:InitRunningTangYuan()
	end
	--葫芦娃2022
	if LuaHuLuWa2022Mgr.IsInRuYiDongPlay() then
		self:OnUpdateYongChuangRuYiDongTask()
	end
	if LuaHuLuWa2022Mgr.IsInQiaoDuoRuYiPlay() then
		self:OnUpdateQiaoDuoRuYiTask()
	end

end

function LuaChristmas2021TaskView:OnUpdateGamePlayStageInfo(gameplayId,title,content)
	LuaChristmas2021Mgr.m_TaskStageGamePlayId = gameplayId
    LuaChristmas2021Mgr.m_TaskSategTitle = title
    LuaChristmas2021Mgr.m_TaskContent = content

	if gameplayId == Christmas2021_XingYu.GetData().GamePlayId then
		self:OnUpdateChristmasXingYuTask()
        return
    end
    
    if gameplayId == HuluBrothers_Setting.GetData().RuyiPlayId then--巧夺如意副本
		self:OnUpdateQiaoDuoRuYiTask()
        return
    end
    if gameplayId == HuluBrothers_Setting.GetData().NormalPlayId or gameplayId == HuluBrothers_Setting.GetData().HardPlayId or gameplayId == HuluBrothers_Setting.GetData().HellPlayId then
        self:OnUpdateYongChuangRuYiDongTask()
        return
    end
end
--#region 圣诞
function LuaChristmas2021TaskView:OnUpdateChristmasXingYuTask()
	local gameplayId = LuaChristmas2021Mgr.m_TaskStageGamePlayId
    local title = LuaChristmas2021Mgr.m_TaskSategTitle
    local ctext = LuaChristmas2021Mgr.m_TaskContent

	self.TaskName.text = LocalString.GetString("[圣诞星语]")
	self.StageNameLabel.text = title
	self.ContentLabel.gameObject:SetActive(false)

	Extensions.RemoveAllChildren(self.Table.transform)
	--描述
	local content = NGUITools.AddChild(self.Table.gameObject,self.ContentLabel.gameObject)
	content:SetActive(true)
	content.transform:GetComponent(typeof(UILabel)).text = ctext
	self.Table:Reposition()
	local pos = self.Table.transform.localPosition
	local y = self.StageNameLabel.transform.localPosition.y - 30
	local tpos = Vector3(pos.x,y,pos.z)
	self.Table.transform.localPosition = tpos
	--
	local height = NGUIMath.CalculateRelativeWidgetBounds(self.Table.transform).size.y
	height = height + 120
	self.BgTexture.height = height
	local y = self.TaskName.transform.localPosition.y
	local pos = self.RightBtn.transform.localPosition
	self.RightBtn.transform.localPosition = Vector3(pos.x,y - height,pos.z)
	pos = self.LeftBtn.transform.localPosition
	self.LeftBtn.transform.localPosition = Vector3(pos.x,y - height,pos.z)
	self.RightBtn:SetActive(true)
	self.LeftBtn:SetActive(true)
	UIEventListener.Get(self.BgTexture.gameObject).onClick = nil
end

function LuaChristmas2021TaskView:OnUpdateChristmasMJSYTask()
	if not LuaChristmas2021Mgr.IsInMJSYPlay() then
		return
	end
	self.StageNameLabel.text = nil
	local pos = self.Table.transform.localPosition
	local y = self.TaskName.transform.localPosition.y - 30
	local tpos = Vector3(pos.x,y,pos.z)
	self.Table.transform.localPosition = tpos

	local gamePlayId = CScene.MainScene.GamePlayDesignId
	local npcId = LuaChristmas2021Mgr.GetNpcIdByGameMjPlayId(gamePlayId)
	local xiansuoTbl = LuaChristmas2021Mgr.GetXianSuoTblByNpcId(npcId)
	local npcName = NPC_NPC.GetData(npcId).Name
	self.TaskName.text = SafeStringFormat3(LocalString.GetString("[梦境识愿]%s的愿望"),npcName)

	Extensions.RemoveAllChildren(self.Table.transform)
	local isActiveAll = true
	for i,idx in ipairs(xiansuoTbl) do 
		local isActive = LuaChristmas2021Mgr:IsDreamWishActive(idx)
		if not isActive then
			isActiveAll = false
		end
	end
	local height = 0
	if not isActiveAll then
		self.ContentLabel.gameObject:SetActive(false)
		UIEventListener.Get(self.BgTexture.gameObject).onClick = nil
		for i,idx in ipairs(xiansuoTbl) do 
			local item = NGUITools.AddChild(self.Table.gameObject,self.ContentLabel.gameObject)
			item:SetActive(true)
			local label = item.transform:GetComponent(typeof(UILabel))
			local isActive = LuaChristmas2021Mgr:IsDreamWishActive(idx)
			if isActive then
				local xdata = Christmas2021_WishInDream.GetData(idx)
				label.text = SafeStringFormat3("[00ff00]%s(1/1)[-]",xdata.Name)
			else
				label.text = "???(0/1)"
			end
		end
		self.Table:Reposition()
		height = NGUIMath.CalculateRelativeWidgetBounds(self.Table.transform).size.y
		height = height + 80
	else
		self.ContentLabel.gameObject:SetActive(true)
		self.ContentLabel.text = LocalString.GetString("全部线索已找到，快去向神鹿使传递吧！")
		height = self.ContentLabel.height + 80
		UIEventListener.Get(self.BgTexture.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
			if LuaChristmas2021Mgr.IsInMJSYPlay() then
				LuaChristmas2021Mgr.OpenMJSYPlayWnd()
			end
		end)
	end

	self.BgTexture.height = height
	local y = self.TaskName.transform.localPosition.y
	local pos = self.RightBtn.transform.localPosition
	self.RightBtn.transform.localPosition = Vector3(pos.x,y - height - 12,pos.z)
	pos = self.LeftBtn.transform.localPosition
	self.LeftBtn.transform.localPosition = Vector3(pos.x,y - height - 12,pos.z)	

	self.RightBtn:SetActive(false)
	self.LeftBtn:SetActive(true)
end
--#endregion
--#region 
function LuaChristmas2021TaskView:OnUpdateQiaoDuoRuYiTask()
	if LuaChristmas2021Mgr.m_TaskStageGamePlayId ~= 51102436 then
		LuaChristmas2021Mgr.m_TaskSategTitle = nil
		LuaChristmas2021Mgr.m_TaskContent = nil
	end

    local title = LuaChristmas2021Mgr.m_TaskSategTitle
    local content = LuaChristmas2021Mgr.m_TaskContent

	local name = title and title or LocalString.GetString("巧夺如意")
	self.TaskName.text = title and title or SafeStringFormat3("[%s]",name)
	self.StageNameLabel.text = nil
	self.ContentLabel.gameObject:SetActive(true)
	Extensions.RemoveAllChildren(self.Table.transform)
	local pos = self.Table.transform.localPosition
	local y = self.TaskName.transform.localPosition.y - 30
	local tpos = Vector3(pos.x,y,pos.z)
	self.Table.transform.localPosition = tpos

	local contentItem = NGUITools.AddChild(self.Table.gameObject,self.ContentLabel.gameObject)
	contentItem:SetActive(true)
	contentItem.transform:GetComponent(typeof(UILabel)).text = CChatLinkMgr.TranslateToNGUIText(content,false)
	self.Table:Reposition()
	local height = NGUIMath.CalculateRelativeWidgetBounds(self.Table.transform).size.y
	height = height + 120
	self.BgTexture.height = height

	self.LeftBtn:SetActive(true)
	self.RightBtn:SetActive(true)
	self.LeftBtn.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("变身")
	self.RightBtnLabel.text = LocalString.GetString("技能")
	UIEventListener.Get(self.BgTexture.gameObject).onClick = nil

end
function LuaChristmas2021TaskView:OnUpdateYongChuangRuYiDongTask()
	local setting = HuluBrothers_Setting.GetData()
	local gamePlayId = LuaChristmas2021Mgr.m_TaskStageGamePlayId
	if gamePlayId ~= setting.NormalPlayId and gamePlayId ~= setting.HardPlayId and gamePlayId ~= setting.HellPlayId then
		LuaChristmas2021Mgr.m_TaskSategTitle = nil
		LuaChristmas2021Mgr.m_TaskContent = nil
	end
    local title = LuaChristmas2021Mgr.m_TaskSategTitle
    local content = LuaChristmas2021Mgr.m_TaskContent
	local name = title and title or LocalString.GetString("勇闯如意洞")
	self.TaskName.text = title and title or SafeStringFormat3("[%s]",name)
	self.StageNameLabel.text = nil
	self.ContentLabel.gameObject:SetActive(true)
	Extensions.RemoveAllChildren(self.Table.transform)
	local pos = self.Table.transform.localPosition
	local y = self.TaskName.transform.localPosition.y - 30
	local tpos = Vector3(pos.x,y,pos.z)
	self.Table.transform.localPosition = tpos
	local contentItem = NGUITools.AddChild(self.Table.gameObject,self.ContentLabel.gameObject)
	contentItem:SetActive(true)
	contentItem.transform:GetComponent(typeof(UILabel)).text = CChatLinkMgr.TranslateToNGUIText(content,false)
	self.Table:Reposition()
	local height = NGUIMath.CalculateRelativeWidgetBounds(self.Table.transform).size.y
	height = height + 120
	self.BgTexture.height = height

	self.LeftBtn:SetActive(true)
	self.RightBtn:SetActive(true)
	self.LeftBtn.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("变身")
	self.RightBtnLabel.text = LocalString.GetString("技能")
	UIEventListener.Get(self.BgTexture.gameObject).onClick = nil
end
--#endregion
function LuaChristmas2021TaskView:OnSceneRemainTimeUpdate(args)
	if not self.m_LastTime then return end
    if CScene.MainScene then
        self.m_LastTime.text = self:GetRemainTimeText(CScene.MainScene.ShowTime)
    else
        self.m_LastTime.text = nil
    end
end

function LuaChristmas2021TaskView:GetRemainTimeText(totalSeconds)
    if totalSeconds < 0 then
        totalSeconds = 0
    end
    if totalSeconds >= 3600 then
        return SafeStringFormat3("%02d:%02d:%02d", math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
    else
        return SafeStringFormat3("%02d:%02d", math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end
--#region狂奔的汤圆
function LuaChristmas2021TaskView:InitRunningTangYuan()
	local setting = YuanXiao_TangYuan2022Setting.GetData()
	self.TaskName.text = setting.TaskName
	local title = setting.TaskStageName
	local desc = setting.TaskDescription
	self:OnUpdateRunningTangYuanTask(title,desc)
end

function LuaChristmas2021TaskView:OnUpdateRunningTangYuanTask(title,ctext)
	self.StageNameLabel.text = title
	self.ContentLabel.gameObject:SetActive(false)

	Extensions.RemoveAllChildren(self.Table.transform)
	--描述
	local content = NGUITools.AddChild(self.Table.gameObject,self.ContentLabel.gameObject)
	content:SetActive(true)
	content.transform:GetComponent(typeof(UILabel)).text = ctext
	--倒計時
	local restTime = NGUITools.AddChild(self.Table.gameObject,self.ContentLabel.gameObject)
	restTime:SetActive(true)
	self.m_LastTime = restTime.transform:GetComponent(typeof(UILabel))
	self.m_LastTime.color = Color.green
	self.Table:Reposition()

	local pos = self.Table.transform.localPosition
	local y = self.StageNameLabel.transform.localPosition.y - 30
	local tpos = Vector3(pos.x,y,pos.z)
	self.Table.transform.localPosition = tpos
	--
	local height = NGUIMath.CalculateRelativeWidgetBounds(self.Table.transform).size.y
	height = height + 120
	self.BgTexture.height = height
	local y = self.TaskName.transform.localPosition.y
	local pos = self.RightBtn.transform.localPosition
	self.RightBtn.transform.localPosition = Vector3(pos.x,y - height,pos.z)
	pos = self.LeftBtn.transform.localPosition
	self.LeftBtn.transform.localPosition = Vector3(pos.x,y - height,pos.z)

	self.RightBtn:SetActive(true)
	self.LeftBtn:SetActive(true)
	UIEventListener.Get(self.BgTexture.gameObject).onClick = nil
end
--#endregion
--@region UIEvent
--LeftBtn
function LuaChristmas2021TaskView:OnLeftBtnClick()
	if LuaHuLuWa2022Mgr.IsInQiaoDuoRuYiPlay() then
		Gac2Gas.RequestHuluwaResumeTransform()
		return
	end
	if LuaHuLuWa2022Mgr.IsInRuYiDongPlay() then
		Gac2Gas.RequestHuluwaResumeTransform()
		return
	end
	CGamePlayMgr.Inst:LeavePlay()
end

function LuaChristmas2021TaskView:OnRightBtnClick()
	if LuaTangYuan2022Mgr.IsInPlay() then
		LuaTangYuan2022Mgr.ShowImageRuleWnd()
		return
	end
	if LuaHuLuWa2022Mgr.IsInQiaoDuoRuYiPlay() then
		LuaHuLuWa2022Mgr.ShowHuLuWaSkillTip()
		return
	end
	if LuaHuLuWa2022Mgr.IsInRuYiDongPlay() then
		LuaHuLuWa2022Mgr.ShowHuLuWaSkillTip()
		return
	end

	g_MessageMgr:ShowMessage("2021ChristmasSDXY_RULE")
end

--@endregion UIEvent

