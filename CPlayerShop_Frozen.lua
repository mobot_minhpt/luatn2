-- Auto Generated!!
local CFrozenSilverItem = import "CFrozenSilverItem"
local CPlayerShop_Frozen = import "L10.UI.CPlayerShop_Frozen"
CPlayerShop_Frozen.m_CellForRowAtIndex_CS2LuaHook = function (this, index) 
    local cellIdentifier = "RowCell"

    local cell = this.tableView:DequeueReusableCellWithIdentifier(cellIdentifier)
    if cell == nil then
        cell = this.tableView:AllocNewCellWithIdentifier(this.template, cellIdentifier)
    end
    if index >= 0 and index < this.dataList.Count then
        local data = this.dataList[index]
        CommonDefs.GetComponentInChildren_GameObject_Type(cell, typeof(CFrozenSilverItem)):Init(data.silver, data.frozenTime, data.unfrozenTime)
    end

    return cell
end
