local QnModelPreviewer = import "L10.UI.QnModelPreviewer"

LuaArenaPassportPreviewWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaArenaPassportPreviewWnd, "modelPreview", "ModelPreview", QnModelPreviewer)
RegistChildComponent(LuaArenaPassportPreviewWnd, "name", "Label", UILabel)

--@endregion RegistChildComponent end

function LuaArenaPassportPreviewWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaArenaPassportPreviewWnd:Init()
    local appearance = CClientMainPlayer.Inst.AppearanceProp:Clone()
    appearance.YingLingState = EnumYingLingState.eDefault
    local fashionId = LuaArenaMgr.fashionId
    local fashionData = Fashion_Fashion.GetData(fashionId)
    local pos = fashionData.Position
    if pos == 1 then
        appearance.BodyFashionId = fashionId
    elseif pos == 0 then
        appearance.HeadFashionId = fashionId
    end
    self.modelPreview:PreviewFakeAppear(appearance)

    self.name.text = fashionData.Name
end

--@region UIEvent

--@endregion UIEvent
