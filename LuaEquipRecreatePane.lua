local CUITexture = import "L10.UI.CUITexture"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CBaseEquipmentItem = import "L10.UI.CBaseEquipmentItem"
local GameObject = import "UnityEngine.GameObject"
local CItemCountUpdate = import "L10.UI.CItemCountUpdate"
local UILabel = import "UILabel"
local UIToggle = import "UIToggle"
local EventDelegate = import "EventDelegate"
local PlayerPrefs = import "UnityEngine.PlayerPrefs"
local AlignType2 = import "L10.UI.CItemInfoMgr+AlignType"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local AlignType1 = import "L10.UI.CTooltip+AlignType"
local CItemMgr=import "L10.Game.CItemMgr"
local CEquipBaptizeDetailView = import "L10.UI.CEquipBaptizeDetailView"

LuaEquipRecreatePane = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaEquipRecreatePane, "MainEquip", "MainEquip", CBaseEquipmentItem)
RegistChildComponent(LuaEquipRecreatePane, "WuLianToggle", "WuLianToggle", UIToggle)
RegistChildComponent(LuaEquipRecreatePane, "ComfirmBtn", "ComfirmBtn", GameObject)
RegistChildComponent(LuaEquipRecreatePane, "TipButton", "TipButton", GameObject)
RegistChildComponent(LuaEquipRecreatePane, "ItemCostNode", "ItemCostNode", CItemCountUpdate)
RegistChildComponent(LuaEquipRecreatePane, "DescLabel", "DescLabel", UILabel)
RegistChildComponent(LuaEquipRecreatePane, "ComfirmLabel", "ComfirmLabel", UILabel)
RegistChildComponent(LuaEquipRecreatePane, "GetNode", "GetNode", GameObject)
RegistChildComponent(LuaEquipRecreatePane, "ItemIcon", "ItemIcon", CUITexture)
RegistChildComponent(LuaEquipRecreatePane, "ItemNameLabel", "ItemNameLabel", UILabel)

--@endregion RegistChildComponent end

function LuaEquipRecreatePane:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.ComfirmBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnComfirmBtnClick()
	end)

	UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)

    --@endregion EventBind end
end

function LuaEquipRecreatePane:OnSelect(args)
	local itemId = args[0]

	if itemId and itemId ~= "" then
		self.MainEquip:UpdateData(itemId)
	end

	self.m_ItemId = itemId
	self.DescLabel.text = g_MessageMgr:FormatMessage("Equip_Recreate_Pane_Desc")

	if CEquipBaptizeDetailView.s_EnableWuLianRecreate then
		self.WuLianToggle.gameObject:SetActive(true)
		self.WuLianToggle.transform.parent.gameObject:SetActive(true)
		EventDelegate.Add(self.WuLianToggle.onChange, DelegateFactory.Callback(function () 
			if self.WuLianToggle.value then
				PlayerPrefs.SetInt("EquipRecreatePane_WuLianToggle", 1)
				self.ComfirmLabel.text = LocalString.GetString("五连再造")
				self.m_IsWuLian = true
			else
				PlayerPrefs.SetInt("EquipRecreatePane_WuLianToggle", 0)
				self.ComfirmLabel.text = LocalString.GetString("单次再造")
				self.m_IsWuLian = false
			end
			self:UpdateItem()
		end))
	
		if PlayerPrefs.GetInt("EquipRecreatePane_WuLianToggle", 0) == 1 then
			self.WuLianToggle.value = true
		else
			self.WuLianToggle.value = false
		end
	else
		-- 没开启五连
		self.WuLianToggle.gameObject:SetActive(false)
		self.WuLianToggle.transform.parent.gameObject:SetActive(false)

		PlayerPrefs.SetInt("EquipRecreatePane_WuLianToggle", 0)
		self.ComfirmLabel.text = LocalString.GetString("单次再造")
		self.m_IsWuLian = false
	end
	
	self:UpdateItem()
end

function LuaEquipRecreatePane:UpdateItem()
	local templateId, count = CLuaEquipMgr:GetRecreateCost(self.m_ItemId, self.m_IsWuLian)
	local itemData = Item_Item.GetData(templateId)

	local item = CItemMgr.Inst:GetById(self.m_ItemId)
	local isBind = item.IsBinded
	self.ItemCostNode.onlyUnbind = not isBind

	if itemData then
		self.ItemCostNode.templateId = templateId
		self.ItemCostNode.excludeExpireTime = true
		self.ItemCostNode.format = SafeStringFormat3("{0}/%s", tostring(count))
		self.ItemCostNode:UpdateCount()

		self.ItemIcon:LoadMaterial(itemData.Icon)

		if not isBind then
			self.ItemNameLabel.text = SafeStringFormat3(LocalString.GetString("%s（非绑定）"), itemData.Name)
		else
			self.ItemNameLabel.text = itemData.Name
		end
	end

	local curCount = self.ItemCostNode.count

	if curCount >= count then
		self.GetNode:SetActive(false)
		UIEventListener.Get(self.ItemIcon.gameObject).onClick = DelegateFactory.VoidDelegate(function()
			CItemInfoMgr.ShowLinkItemTemplateInfo(templateId, false, nil, AlignType2.Default, 0, 0, 0, 0)
		end)
	else
		self.GetNode:SetActive(true)
		UIEventListener.Get(self.ItemIcon.gameObject).onClick = DelegateFactory.VoidDelegate(function()
			CItemAccessListMgr.Inst:ShowItemAccessInfo(templateId, false, self.transform, AlignType1.Right)
		end)
	end
end

function LuaEquipRecreatePane:OnEnable()
    g_ScriptEvent:AddListener("SendItem", self, "UpdateItem")
	g_ScriptEvent:AddListener("SetItemAt", self, "UpdateItem")
end

function LuaEquipRecreatePane:OnDisable()
    g_ScriptEvent:RemoveListener("SendItem", self, "UpdateItem")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "UpdateItem")
end

--@region UIEvent

function LuaEquipRecreatePane:OnComfirmBtnClick()
	local item = CItemMgr.Inst:GetById(self.m_ItemId)
    local equip = item.Equip
	local forgeLv = equip.ForgeLevel

	if forgeLv > 0 then
		g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("Equip_Forge_Need_Reset_Tip"), 
            function ()
				
			end, 
			
			function()
				-- 请求重置
				local place, pos = CLuaEquipMgr:GetItemPlaceAndPos(self.m_ItemId)
				Gac2Gas.QueryForgeResetExtraEquipCostAndItems(place, pos, self.m_ItemId)
        	end, LocalString.GetString("取消"), LocalString.GetString("前往重置"), false)
	else
		-- 打开再造界面
		CLuaEquipMgr.m_RecreateItemId = self.m_ItemId
		CLuaEquipMgr.m_IsWuLian = self.m_IsWuLian
		CUIManager.ShowUI(CLuaUIResources.RecreateExtraEquipWnd)
	end
end

function LuaEquipRecreatePane:OnTipButtonClick()
	g_MessageMgr:ShowMessage("Equip_Recreate_Pane_Tip")
end

--@endregion UIEvent

