local DelegateFactory  = import "DelegateFactory"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CClientHouseMgr=import "L10.Game.CClientHouseMgr"
local CItemAccessListMgr=import "L10.UI.CItemAccessListMgr"
local AlignType = import "L10.UI.CTooltip+AlignType"

LuaAddPoolMaxCountWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaAddPoolMaxCountWnd, "IncreseAndDecreaseBtn", "IncreseAndDecreaseBtn", QnAddSubAndInputButton)
RegistChildComponent(LuaAddPoolMaxCountWnd, "NeedStoneLabel", "NeedStoneLabel", UILabel)
RegistChildComponent(LuaAddPoolMaxCountWnd, "OwnStoneLabel", "OwnStoneLabel", UILabel)
RegistChildComponent(LuaAddPoolMaxCountWnd, "AddStoneBtn", "AddStoneBtn", GameObject)
RegistChildComponent(LuaAddPoolMaxCountWnd, "ComfirmBtn", "ComfirmBtn", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaAddPoolMaxCountWnd, "m_IsMingYuan")
RegistClassMember(LuaAddPoolMaxCountWnd, "m_BasePoolCount")
RegistClassMember(LuaAddPoolMaxCountWnd, "m_CurMaxPoolCount")
RegistClassMember(LuaAddPoolMaxCountWnd, "m_MaxCount")
RegistClassMember(LuaAddPoolMaxCountWnd, "m_NeedStone")
RegistClassMember(LuaAddPoolMaxCountWnd, "m_OwnStone")
RegistClassMember(LuaAddPoolMaxCountWnd, "m_AddCount")
function LuaAddPoolMaxCountWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	UIEventListener.Get(self.AddStoneBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnAddStoneBtnClick()
	end)

    --@endregion EventBind end
	self.m_AddCount = 0
	self.IncreseAndDecreaseBtn.onValueChanged = DelegateFactory.Action_uint(function (value)
        --costLabel.text = tostring((value * itemValue))
		self:OnAddCountChange(value)
    end)
end

function LuaAddPoolMaxCountWnd:OnAddCountChange(value)
	self.m_NeedStone = (value-self.m_CurMaxPoolCount) * House_Setting.GetData().PoolPricePerGrid
	self.m_AddCount = (value-self.m_CurMaxPoolCount)
	if self.m_NeedStone > self.m_OwnStone then
		self.NeedStoneLabel.text = SafeStringFormat3("[c][ff0000]%d[-][/c]",self.m_NeedStone)
	else
		self.NeedStoneLabel.text = tostring(self.m_NeedStone)
	end
end

function LuaAddPoolMaxCountWnd:Init()
	local setting = House_Setting.GetData()
	self.m_IsMingYuan = CClientHouseMgr.Inst.mCurBasicProp.IsMingyuan == 1
	self.m_BasePoolCount = self.m_IsMingYuan and setting.MingYuanBasePoolGridNum or setting.BasePoolGridNum
	self.m_CurMaxPoolCount = CClientHouseMgr.Inst.mCurFurnitureProp2.PoolGridNum + self.m_BasePoolCount
	if self.m_IsMingYuan then
		self.m_MaxCount = setting.MingYuanPoolMaxSize[0] * setting.MingYuanPoolMaxSize[1]
	else
		self.m_MaxCount = setting.PoolMaxSize[0] * setting.PoolMaxSize[1]
	end

	self.m_OwnStone = CClientHouseMgr.Inst.mConstructProp.Stone
	self.OwnStoneLabel.text = tostring(self.m_OwnStone)

	self.IncreseAndDecreaseBtn:SetMinMax(self.m_CurMaxPoolCount+1,self.m_MaxCount, 1)
	self.IncreseAndDecreaseBtn:SetValue(self.m_CurMaxPoolCount+1)
	self:OnAddCountChange(self.m_CurMaxPoolCount+1)
	UIEventListener.Get(self.ComfirmBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnComfirmBtnClick()
	end)
end

function LuaAddPoolMaxCountWnd:OnEnable()
    g_ScriptEvent:AddListener("OnUpdateHouseResource", self, "Init")
    g_ScriptEvent:AddListener("OnSendHouseData", self, "Init")
	g_ScriptEvent:AddListener("UpdateHousePoolGridNum", self, "Init")
end

function LuaAddPoolMaxCountWnd:OnDisable( )
    g_ScriptEvent:RemoveListener("OnUpdateHouseResource", self, "Init")
    g_ScriptEvent:RemoveListener("OnSendHouseData", self, "Init")
	g_ScriptEvent:RemoveListener("UpdateHousePoolGridNum", self, "Init")
end

--@region UIEvent

function LuaAddPoolMaxCountWnd:OnAddStoneBtnClick()
	 -- 选中五石仓
	 --CShopMallMgr.ShowLinyuShoppingMall(21003558)
	 CItemAccessListMgr.Inst:ShowItemAccessInfo(42, true, nil, AlignType.Right)
	 --print(ItemGet_item.GetData(42))
end

function LuaAddPoolMaxCountWnd:OnComfirmBtnClick()
	if self.m_CurMaxPoolCount >= self.m_MaxCount then
		g_MessageMgr:ShowMessage("PoolNum_Over_Limit")
		return
	end
	LuaPoolMgr.OpenBuyPoolComfirmWnd(self.m_NeedStone, self.m_CurMaxPoolCount + self.m_AddCount,self.m_AddCount)
end

--@endregion UIEvent

