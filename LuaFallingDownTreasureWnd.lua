require("common/common_include")

local Mathf = import "UnityEngine.Mathf"
local Time = import "UnityEngine.Time"
local Random = import "UnityEngine.Random"
local Vector3 = import "UnityEngine.Vector3"
local CUIManager = import "L10.UI.CUIManager"
local Screen = import "UnityEngine.Screen"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local CUIFx = import "L10.UI.CUIFx"
local PathType = import "DG.Tweening.PathType"
local PathMode = import "DG.Tweening.PathMode"
local Ease = import "DG.Tweening.Ease"

LuaFallingDownTreasureWnd = class()

RegistChildComponent(LuaFallingDownTreasureWnd, "Template1", GameObject) --元宝
RegistChildComponent(LuaFallingDownTreasureWnd, "Template2", GameObject) --铜钱	
RegistChildComponent(LuaFallingDownTreasureWnd, "Template3", GameObject) --骨头
RegistChildComponent(LuaFallingDownTreasureWnd, "CountDownLabel", UILabel)
RegistChildComponent(LuaFallingDownTreasureWnd, "ScoreLabel", UILabel)
RegistChildComponent(LuaFallingDownTreasureWnd, "TempTexture", GameObject)
RegistChildComponent(LuaFallingDownTreasureWnd, "PlayerTexture", GameObject)
RegistChildComponent(LuaFallingDownTreasureWnd, "FrozenPlayerTexture", GameObject)
RegistChildComponent(LuaFallingDownTreasureWnd, "FrozenFx", CUIFx)
RegistChildComponent(LuaFallingDownTreasureWnd, "DragBG", UISprite)
RegistChildComponent(LuaFallingDownTreasureWnd, "ToyRoot", GameObject)
RegistChildComponent(LuaFallingDownTreasureWnd, "ResultFx", CUIFx)
RegistChildComponent(LuaFallingDownTreasureWnd, "Success", GameObject)
RegistChildComponent(LuaFallingDownTreasureWnd, "Fail", GameObject)
RegistChildComponent(LuaFallingDownTreasureWnd, "PlayerGuidePos", GameObject)

RegistClassMember(LuaFallingDownTreasureWnd, "m_ToyTemplates")
RegistClassMember(LuaFallingDownTreasureWnd, "m_ToyGeneratePercent")
RegistClassMember(LuaFallingDownTreasureWnd, "m_ToyScores")
RegistClassMember(LuaFallingDownTreasureWnd, "m_TotalTime")
RegistClassMember(LuaFallingDownTreasureWnd, "m_Frozentime")
RegistClassMember(LuaFallingDownTreasureWnd, "m_ToyPassScore")

RegistClassMember(LuaFallingDownTreasureWnd, "m_CountDownTick")
RegistClassMember(LuaFallingDownTreasureWnd, "m_TimeCounter")
RegistClassMember(LuaFallingDownTreasureWnd, "m_EmitIntervalTime")
RegistClassMember(LuaFallingDownTreasureWnd, "m_EmitTimestamp")

RegistClassMember(LuaFallingDownTreasureWnd, "m_FrozeStartTime")
RegistClassMember(LuaFallingDownTreasureWnd, "m_IsFrozen")
RegistClassMember(LuaFallingDownTreasureWnd, "m_FrozeLastTime")

RegistClassMember(LuaFallingDownTreasureWnd, "m_GuideFinger")

function LuaFallingDownTreasureWnd:Init()
	self.m_EmitIntervalTime = 0.5
	self.m_EmitTimestamp = 0
	
	self.m_ToyTemplates = {}
	table.insert(self.m_ToyTemplates, self.Template1)
	table.insert(self.m_ToyTemplates, self.Template2)
	table.insert(self.m_ToyTemplates, self.Template3)
	self.Template1:SetActive(false)
	self.Template2:SetActive(false)
	self.Template3:SetActive(false)

	self.m_GuideFinger = self.transform:Find("FingerRoot/Finger").gameObject

	local setting = ZhuJueJuQing_Setting.GetData()
	self.m_TotalTime = setting.FallingToyTotalTime
	self.m_ToyGeneratePercent = {}

	for i = 0, setting.FallingToyGeneratePercent.Length-1 do
		local percentMin = 0
		if i > 0 then
			percentMin = setting.FallingToyGeneratePercent[i-1]
		end
		table.insert(self.m_ToyGeneratePercent, percentMin + setting.FallingToyGeneratePercent[i])
	end

	self.m_ToyScores = {}
	for i = 0, setting.FallingToyScore.Length-1 do
		table.insert(self.m_ToyScores, setting.FallingToyScore[i])
	end

	self.m_Frozentime = setting.FallingToyFrozentime
	self.m_FrozeStartTime = 0
	self.m_IsFrozen = false
	self.m_FrozeLastTime = 0
	self.m_ToyPassScore = setting.FallingToyPassScore

	self.ResultFx:DestroyFx()

	self:DestroyCountDownTick()
	self:BeginGame()
	self:TryStartGuide()
end

function LuaFallingDownTreasureWnd:BeginGame()
	-- 倒计时
	self:DestroyCountDownTick()
	self.m_CountDownTick = RegisterTick(function ()
		self.m_TimeCounter = self.m_TimeCounter + 1
		local leftTime = self.m_TotalTime - self.m_TimeCounter
		if leftTime < 0 then
			self:EndGame()
			self:DestroyCountDownTick()
		else
			self.CountDownLabel.text = self:ParseTimeText(leftTime)
		end
	end, 1000)

	self.PlayerTexture.gameObject:SetActive(true)
	self.FrozenPlayerTexture.gameObject:SetActive(false)
	CommonDefs.AddOnDragListener(self.PlayerTexture, DelegateFactory.Action_GameObject_Vector2(function (go, delta)
        self:OnPlayerDrag(go, delta)
    end),false)

	LuaZhuJueJuQingMgr.m_FallingDownTreasureEnd = false
	self.Success:SetActive(false)
	self.Fail:SetActive(false)

	self:UpdateScore()
	
end

function LuaFallingDownTreasureWnd:ParseTimeText(totalSeconds)
    if totalSeconds < 0 then
        totalSeconds = 0
    end
    if totalSeconds >= 3600 then
        return SafeStringFormat3("%02d:%02d:%02d", math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
    else
        return SafeStringFormat3("%02d:%02d", math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end

function LuaFallingDownTreasureWnd:EmitToy()
	
	local pos = Vector3.zero
	pos = CUIManager.UIMainCamera:ScreenToWorldPoint(Vector3(Random.Range(0, Screen.width), Screen.height, 0))
	local template, score = self:GetToyTemplate()
	local toy = GameObject.Instantiate(template, pos, self.ToyRoot.transform.rotation)
	if toy then
		if toy.transform:GetComponent(typeof(CCommonLuaScript)) then
			toy.transform:GetComponent(typeof(CCommonLuaScript)):Init({score})
		end
		toy.transform.parent = self.ToyRoot.transform
		toy.transform.localScale = Vector3.one
		toy:SetActive(true)
	end
end

function LuaFallingDownTreasureWnd:GetToyTemplate()
	local random = Random.Range(0, 1)
	if random < self.m_ToyGeneratePercent[1] then
		return self.Template1, self.m_ToyScores[1]
	elseif random >= self.m_ToyGeneratePercent[1] and random < self.m_ToyGeneratePercent[2] then
		return self.Template2, self.m_ToyScores[2]
	else
		return self.Template3, self.m_ToyScores[3]
	end

end

function LuaFallingDownTreasureWnd:OnPlayerDrag(go, delta)
	if LuaZhuJueJuQingMgr.m_FallingDownTreasureEnd then return end

	if not self.m_IsFrozen then
		local currentPos = UICamera.currentTouch.pos
		self.TempTexture.transform.position = UICamera.currentCamera:ScreenToWorldPoint(Vector3(currentPos.x, currentPos.y, 0))
		local x = Mathf.Clamp(self.TempTexture.transform.localPosition.x, -838, 838)
		self.PlayerTexture.transform.localPosition = Vector3(x, 0, 0)
		self:EndGuide()
	else
		-- todo 
	end
	
end

function LuaFallingDownTreasureWnd:EndGame()
	LuaZhuJueJuQingMgr.m_FallingDownTreasureEnd = true
	if LuaZhuJueJuQingMgr.m_FallingDownTreasureScore >= self.m_ToyPassScore then
		self.Success:SetActive(true)
		self.ResultFx:LoadFx("Fx/UI/Prefab/UI_pengpengche_shengli.prefab")
		Gac2Gas.FinishEventTask(LuaZhuJueJuQingMgr.m_FallingDownTreasureTaskId, "FallingDownTreasure")
	else
		self.Fail:SetActive(true)
		self.ResultFx:LoadFx("Fx/UI/Prefab/UI_pengpengche_shibai.prefab")
	end
	RegisterTickOnce(function ()
		CUIManager.CloseUI(CLuaUIResources.FallingDownTreasureWnd)
	end, 5000)
end

function LuaFallingDownTreasureWnd:UpdateScore()
	self.ScoreLabel.text = tostring(LuaZhuJueJuQingMgr.m_FallingDownTreasureScore)
	if LuaZhuJueJuQingMgr.m_FallingDownTreasureScore >= self.m_ToyPassScore then
		self:EndGame()
	end
end

function LuaFallingDownTreasureWnd:UpdateFrozen()
	if self.m_IsFrozen then
		-- 如果已经石化，则再延长石化时间
		self.m_FrozeLastTime = self.m_FrozeLastTime + self.m_Frozentime
	else
		self:BeginFrozen()
	end
end

function LuaFallingDownTreasureWnd:BeginFrozen()
	self.m_IsFrozen = true
	self.m_FrozeStartTime = Time.realtimeSinceStartup
	self.m_FrozeLastTime = self.m_Frozentime

	self.PlayerTexture.gameObject:SetActive(false)
	self.FrozenFx:DestroyFx()
	self.FrozenPlayerTexture.gameObject:SetActive(true)
	self.FrozenPlayerTexture.transform.localPosition = self.PlayerTexture.transform.localPosition
	self.FrozenFx:LoadFx("Fx/UI/Prefab/UI_jiekulou.prefab")
end

function LuaFallingDownTreasureWnd:EndFrozen()
	self.m_IsFrozen = false
	self.PlayerTexture.gameObject:SetActive(true)
	self.FrozenFx:DestroyFx()
	self.FrozenPlayerTexture.gameObject:SetActive(false)
end

function LuaFallingDownTreasureWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateFallingDownTreasureScore", self, "UpdateScore")
	g_ScriptEvent:AddListener("UpdateFallingDownTreasureFrozen", self, "UpdateFrozen")
end

function LuaFallingDownTreasureWnd:OnDisable()
	self:DestroyCountDownTick()
	g_ScriptEvent:RemoveListener("UpdateFallingDownTreasureScore", self, "UpdateScore")
	g_ScriptEvent:RemoveListener("UpdateFallingDownTreasureFrozen", self, "UpdateFrozen")
end

function LuaFallingDownTreasureWnd:Update()
	if LuaZhuJueJuQingMgr.m_FallingDownTreasureEnd then return end

	self.m_EmitTimestamp = self.m_EmitTimestamp + Time.deltaTime
	if self.m_EmitTimestamp > self.m_EmitIntervalTime then
		self.m_EmitTimestamp = 0
		self:EmitToy()
	end
	
	if self.m_IsFrozen then
		if Time.realtimeSinceStartup - self.m_FrozeStartTime > self.m_FrozeLastTime then
			self:EndFrozen()
		end
	end 
end

function LuaFallingDownTreasureWnd:DestroyCountDownTick()
	self.m_TimeCounter = 0
	if self.m_CountDownTick then
		UnRegisterTick(self.m_CountDownTick)
		self.m_CountDownTick = nil
	end
end

function LuaFallingDownTreasureWnd:TryStartGuide()
	local should_guide = CLuaGuideMgr.TryTriggerFallingDownTreasureGuide()
	self.m_GuideFinger:SetActive(should_guide)
	if(should_guide)then
		local waypoints = {Vector3(0,0,0), Vector3(-100,0,0), Vector3(0,0,0), Vector3(100,0,0), Vector3(0,0,0)}
		local wp_array = Table2Array(waypoints, MakeArrayClass(Vector3))
		LuaTweenUtils.DOLuaLocalPath(self.m_GuideFinger.transform, wp_array, 2, PathType.Linear, PathMode.Ignore, 2, - 1, Ease.Linear)
	end
end

function LuaFallingDownTreasureWnd:EndGuide()
	if(self.m_GuideFinger.activeSelf)then
		self.m_GuideFinger:SetActive(false)
		LuaTweenUtils.DOKill(self.m_GuideFinger.transform, false)
	end
	if(L10.Game.Guide.CGuideMgr.Inst:IsInPhase(77))then
        L10.Game.Guide.CGuideMgr.Inst:EndCurrentPhase()
    end
end

function LuaFallingDownTreasureWnd:GetGuideGo(methodName)
	if(methodName == "GetPlayerButton")then
		return self.PlayerGuidePos
	end
	return nil
end

return LuaFallingDownTreasureWnd
