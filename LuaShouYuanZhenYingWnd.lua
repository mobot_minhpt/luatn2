require("3rdParty/ScriptEvent")
require("common/common_include")
local CQianKunDaiMgr=import "L10.Game.CQianKunDaiMgr"
local LuaGameObject=import "LuaGameObject"
local LocalString = import "LocalString"
local MessageMgr = import "L10.Game.MessageMgr"
local Gac2Gas = import "L10.Game.Gac2Gas"
local CItemMgr = import "L10.Game.CItemMgr"
local Item_Item = import "L10.Game.Item_Item"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CItemInfoMgr=import "L10.UI.CItemInfoMgr"
local AlignType=import "L10.UI.CItemInfoMgr+AlignType"
local UILabel = import "UILabel"
local Vector3 = import "UnityEngine.Vector3"
local CUIFx = import "L10.UI.CUIFx"
local TweenPosition = import "TweenPosition"
local Animation = import "UnityEngine.Animation"
local FeiSheng_QianKunDai = import "L10.Game.FeiSheng_QianKunDai"

CLuaShouYuanZhenYingWnd=class()
RegistClassMember(CLuaShouYuanZhenYingWnd,"mItemCell")
RegistClassMember(CLuaShouYuanZhenYingWnd,"mBtnList")
RegistClassMember(CLuaShouYuanZhenYingWnd,"mResLabel")
RegistClassMember(CLuaShouYuanZhenYingWnd,"mBtnQuestion")
RegistClassMember(CLuaShouYuanZhenYingWnd,"mBag")
RegistClassMember(CLuaShouYuanZhenYingWnd,"mFx")
RegistClassMember(CLuaShouYuanZhenYingWnd, "m_Animation")
RegistClassMember(CLuaShouYuanZhenYingWnd, "m_RefineDoneTypeScore")

RegistClassMember(CLuaShouYuanZhenYingWnd, "mTweening")

RegistClassMember(CLuaShouYuanZhenYingWnd, "m_Tick1")
RegistClassMember(CLuaShouYuanZhenYingWnd, "m_BtnTweeners")
RegistClassMember(CLuaShouYuanZhenYingWnd, "m_ItemTweener")


function CLuaShouYuanZhenYingWnd:Awake()
    self.mItemCell=self.transform:Find("Panel/ItemCell").gameObject
    CommonDefs.AddOnClickListener(self.mItemCell,DelegateFactory.Action_GameObject(function(go)
            self:OnItemCellClick(go)
        end),false)
    self.mItemCell:SetActive(false)
    self.m_ItemTweener = self.mItemCell:GetComponent(typeof(TweenPosition))
    self.m_ItemTweener.enabled = false
        
    self.m_BtnTweeners = {}
    self.mBtnList= {}
    for i = 1, 8 do
    	local btn = self.transform:Find("Panel/BtnList/"..i).gameObject
    	local tween = btn:GetComponent(typeof(TweenPosition))
			if tween then
				self.m_BtnTweeners[i] = tween
				tween.enabled = false
			end
    	table.insert(self.mBtnList, btn)
    	CommonDefs.AddOnClickListener(btn,DelegateFactory.Action_GameObject(function(go)
            self:OnAntiScoreBtnClick(go)
        end),false)
    end
    local resLabelTrans = self.transform:Find("Panel/BtnList/Label")
    self.mResLabel = resLabelTrans:GetComponent(typeof(UILabel))
    self.mBtnQuestion = self.transform:Find("Panel/BtnQuestion").gameObject
    CommonDefs.AddOnClickListener(self.mBtnQuestion,DelegateFactory.Action_GameObject(function(go)
            self:OnBtnQuestionClick(go)
        end),false)
        
    self.mBag = self.transform:Find("Panel/Bottom/BagParent/Bag").gameObject
    CommonDefs.AddOnClickListener(self.mBag,DelegateFactory.Action_GameObject(function(go)
            self:OnBagClick(go)
        end),false)
    self.m_Animation = self.mBag:GetComponent(typeof(Animation))
    
    local fxGO = self.transform:Find("Panel/Bottom/BagParent/Bag/Fx")
    self.mFx = fxGO:GetComponent(typeof(CUIFx))
    self.mTweening = false

	local feishengDData = FeiSheng_QianKunDai.GetData()
	self.m_RefineDoneTypeScore = feishengDData.RefineDoneTypeScore
end

function CLuaShouYuanZhenYingWnd:OnItemCellClick(go)
	local zhenQiItem = CQianKunDaiMgr.Lua_ZhenQiItemId and CItemMgr.Inst:GetById(CQianKunDaiMgr.Lua_ZhenQiItemId)
	if zhenQiItem then
		CItemInfoMgr.ShowLinkItemInfo(CQianKunDaiMgr.Lua_ZhenQiItemId, false, nil, AlignType.Default, 0,0,0,0)
	end
end

function CLuaShouYuanZhenYingWnd:OnAntiScoreBtnClick(go)
	if CQianKunDaiMgr.Lua_bRefineDone then
		MessageMgr.Inst:ShowMessage("CUSTOM_STRING2",{LocalString.GetString("已达成合成条件，点击底部合成按钮进行合成!")})
		return
	end
	
	for idx, btn in pairs(self.mBtnList) do
		local selectedSprite = LuaGameObject.GetChildNoGC(btn.transform, "Sprite").gameObject
		selectedSprite:SetActive(go == btn)
		if go == btn then
			local val = CQianKunDaiMgr.Lua_CurAntiScore[idx-1]
			if val >= self.m_RefineDoneTypeScore then
				MessageMgr.Inst:ShowMessage("ShouYuanZhenYing_Already_Done",{})
				return
			else
				CQianKunDaiMgr.Lua_AntiType = idx
				Gac2Gas.QueryEquipForFeiShengQianKunDai(idx)
			end
		end
	end
end

function CLuaShouYuanZhenYingWnd:OnBagClick(go)
	local zhenQiItem = CQianKunDaiMgr.Lua_ZhenQiItemId and CItemMgr.Inst:GetById(CQianKunDaiMgr.Lua_ZhenQiItemId)
	if not zhenQiItem and CQianKunDaiMgr.Lua_bRefineDone then
		Gac2Gas.RequestQianKunDaiRefineDone()
	else
		g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("请先集齐4种元素"))
	end
end

function CLuaShouYuanZhenYingWnd:OnBtnQuestionClick(go)
		MessageMgr.Inst:ShowMessage("ShouYuanZhenYing_Question_Tip",{})
end

function CLuaShouYuanZhenYingWnd:tweenToPos(targetPos, bForward)
	CUICommonDef.KillTween(self.mBag:GetHashCode())	
	self.mTweening = false
	local oriPos = self.mBag.transform.localPosition
	
	local xT = targetPos.x - oriPos.x
	local yT = targetPos.y - oriPos.y
	local onUpdate = function(p)
		local pos = Vector3(0, 0, 0)
		pos.x = xT*p + oriPos.x
		pos.y = yT*p + oriPos.y
		self.mBag.transform.localPosition = pos
	end
	
	local onComplete = function()
		if bForward then
			CommonDefs.GetAnimationState(self.m_Animation, "QianKunDai_Shake").time = 0
			self.m_Animation:Play()
		end
		self.mTweening  = false
	end

	CUICommonDef.Tween(1,DelegateFactory.Action_float(onUpdate),DelegateFactory.Action(onComplete),1,self.mBag:GetHashCode())
	self.mTweening = true
end

function CLuaShouYuanZhenYingWnd:OnEnable()
    g_ScriptEvent:AddListener("OnShouYuanZhenYingUpdate", self, "Init")
    g_ScriptEvent:AddListener("OnQianKunDaiRefineDone", self, "Init")
  	g_ScriptEvent:AddListener("QianKunDaiShakeFinish", self, "QianKunDaiShakeFinish")
end

function CLuaShouYuanZhenYingWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnShouYuanZhenYingUpdate", self, "Init")
    g_ScriptEvent:RemoveListener("OnQianKunDaiRefineDone", self, "Init")
   	g_ScriptEvent:RemoveListener("QianKunDaiShakeFinish", self, "QianKunDaiShakeFinish") 
  
    if self.m_Tick1~=nil then
      UnRegisterTick(self.m_Tick1)
      self.m_Tick1=nil
    end
   	CQianKunDaiMgr.Lua_ZhenQiItemId = nil
   	CQianKunDaiMgr.Lua_bRefineDone = nil
   	CommonDefs.ListClear(CQianKunDaiMgr.Lua_CurAntiScore)
   	CommonDefs.DictClear(CQianKunDaiMgr.Lua_CadidateEquipItem)
   	
		CUICommonDef.KillTween(self.mBag:GetHashCode())	
end


function CLuaShouYuanZhenYingWnd:QianKunDaiShakeFinish()
	self:ShowFx()
	
	local targetPos = Vector3.zero
	targetPos.y = -307
	targetPos.z = 0
	self:tweenToPos(targetPos, false)
	
	self:ShowItemCell()
end


function CLuaShouYuanZhenYingWnd:ShowItemCell()
	local zhenQiItem = CQianKunDaiMgr.Lua_ZhenQiItemId and CItemMgr.Inst:GetById(CQianKunDaiMgr.Lua_ZhenQiItemId)
	if zhenQiItem and CQianKunDaiMgr.Lua_bRefineDone then
		self.mItemCell:SetActive(true)
		local ddata = Item_Item.GetData(zhenQiItem.TemplateId)
		if not ddata then
			return
		end
		
		self.m_ItemTweener.enabled = true
		self.m_ItemTweener:PlayForward()
		
		local icon = LuaGameObject.GetChildNoGC(self.mItemCell.transform, "IconTexture").cTexture
		local nameLabel = LuaGameObject.GetChildNoGC(self.mItemCell.transform, "NameLabel").label
		local qualitySprite = LuaGameObject.GetChildNoGC(self.mItemCell.transform, "QualitySprite").sprite
		
		icon:LoadMaterial(ddata.Icon)
		nameLabel.text = ddata.Name
		qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(zhenQiItem.Item.QualityType)
	else
		self.mItemCell:SetActive(false)
	end
end

function CLuaShouYuanZhenYingWnd:ShowBtnList()
	local zhenQiItem = CQianKunDaiMgr.Lua_ZhenQiItemId and CItemMgr.Inst:GetById(CQianKunDaiMgr.Lua_ZhenQiItemId)
	if zhenQiItem then
		self.mResLabel.gameObject:SetActive(false)
	else
		local count = 0
		for i = 0, 7 do
			local btn = self.mBtnList[i+1]
			btn:SetActive(true)
			local val = CQianKunDaiMgr.Lua_CurAntiScore[i]
			local progress = LuaGameObject.GetChildNoGC(btn.transform, "Progress").slider
			local label = LuaGameObject.GetChildNoGC(btn.transform, "Label").label
			progress.value = val / self.m_RefineDoneTypeScore
			label.text = SafeStringFormat3("%.1f", val * 100/ self.m_RefineDoneTypeScore) .. "%"
			if val >= self.m_RefineDoneTypeScore then
				count = count + 1
			end
			
			local fillSprite = LuaGameObject.GetChildNoGC(btn.transform, "FillSprite").gameObject
			if fillSprite then
				fillSprite:SetActive(val >= self.m_RefineDoneTypeScore)
			end
			
			local selectedSprite = LuaGameObject.GetChildNoGC(btn.transform, "Sprite").gameObject
			if selectedSprite then
				selectedSprite:SetActive(false)
			end
		end
		self.mResLabel.gameObject:SetActive(true)
		self.mResLabel.text = count .. "/4"
	end
end

function CLuaShouYuanZhenYingWnd:ShowBag()
	local zhenQiItem = CQianKunDaiMgr.Lua_ZhenQiItemId and CItemMgr.Inst:GetById(CQianKunDaiMgr.Lua_ZhenQiItemId)
	if zhenQiItem then
		-- 合成成功
		CUICommonDef.SetActive(self.mBag, false, true)
		CUICommonDef.SetActive(self.mBag, true, false)
		
		for i = 0, 7 do
			local tween = self.m_BtnTweeners[i+1]
			if tween then
				tween.enabled = true
				tween:PlayForward()
			end
		end
		
		self.m_Tick1=RegisterTickOnce(function()
				for i = 0, 7 do
					local btn = self.mBtnList[i+1]
					btn:SetActive(false)
					local tween = self.m_BtnTweeners[i+1]
					if tween then
						tween.enabled = false
					end
				end
				if not self.mTweening then
					local targetPos = Vector3.zero
					targetPos.y = 0
					targetPos.z = 0
					self:tweenToPos(targetPos, true)
				end
	    end, 800)
	else
		--CUICommonDef.SetActive(self.mBag, CQianKunDaiMgr.Lua_bRefineDone, true)
		CUICommonDef.SetActive(self.mBag,true, false)
	end
end

function CLuaShouYuanZhenYingWnd:ShowFx()
	self.mFx:DestroyFx()
	self.mFx:LoadFx("fx/ui/prefab/UI_mapilinfuzhaohuan_xingxing_zi.prefab")
end

function CLuaShouYuanZhenYingWnd:Init()
	self:ShowBtnList()
	self:ShowBag()
end

return CLuaShouYuanZhenYingWnd
