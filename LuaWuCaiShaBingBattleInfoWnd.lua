require("common/common_include")

local UISlider = import "UISlider"

CLuaWuCaiShaBingBattleInfoWnd = class()

function CLuaWuCaiShaBingBattleInfoWnd:Init()
	local playerGroupId = LuaWuCaiShaBingMgr.m_PlayerGroupId
	local groupProgressData = LuaWuCaiShaBingMgr.m_GroupProgressData

	local teamNameTbl = {"RedTeam", "YellowTeam", "BlueTeam"}
	for groupId = 1, 3 do
		local teamName = teamNameTbl[groupId]
		local teamSprite = self.transform:Find("Anchor/" .. teamName .. "/Title/Sprite").gameObject
		local nameTransform = self.transform:Find("Anchor/" .. teamName .. "/Title/Label")
		local bg = self.transform:Find("Anchor/" .. teamName .. "/Bg"):GetComponent(typeof(UISprite))
		if groupId ~= playerGroupId then
			teamSprite:SetActive(false)
			nameTransform.localPosition = Vector3(0, 0, 0)
			bg.spriteName = "common_textbg_01_light"
		else
			teamSprite:SetActive(true)
			nameTransform.localPosition = Vector3(23, 0, 0)
			bg.spriteName = "common_select_01"
		end

		local progressData = groupProgressData[groupId]
		for i = 1, #progressData do
			if i % 2 == 0 then
				local idx = math.floor(i / 2)
				local progressGo = self.transform:Find("Anchor/" .. teamName .. "/Progress" .. tostring(idx)).gameObject
				
				local label = progressGo.transform:Find("Label"):GetComponent(typeof(UILabel))
				label.text = SafeStringFormat3("%d/%d", progressData[i - 1], progressData[i])
				
				local progressBar = progressGo.transform:Find("Progress Bar"):GetComponent(typeof(UISlider))
				progressBar.sliderValue = progressData[i] == 0 and 0 or progressData[i - 1] / progressData[i]
			end
		end
	end
end

return CLuaWuCaiShaBingBattleInfoWnd