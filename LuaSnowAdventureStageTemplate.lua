require("common/common_include")
local CServerTimeMgr   = import "L10.Game.CServerTimeMgr"
local CButton = import "L10.UI.CButton"

LuaSnowAdventureStageTemplate = class()

function LuaSnowAdventureStageTemplate:Ctor()
    g_ScriptEvent:AddListener("SelectSnowAdventureStage", self, "OnSelectSnowAdventureStage")
end

function LuaSnowAdventureStageTemplate:Init(obj)
    self.transform = obj.transform
    self.normalBg = obj.transform:Find("circleBg_Normal")
    self.button = self.normalBg:GetComponent(typeof(CButton))
    self.highLightBg = obj.transform:Find("circleBg_Highlight")
    self.highlightButton = self.highLightBg:GetComponent(typeof(CButton))

    self.newest = obj.transform:Find("Newest")
    self.stageLabel = self.normalBg:Find("stageLabel"):GetComponent(typeof(UILabel))
    self.highlightStageLabel = self.highLightBg:Find("stageLabel"):GetComponent(typeof(UILabel))
    self.stageLock = self.normalBg:Find("stageLock")
    self.openLater = obj.transform:Find("openLater")
    self.openLaterLabel = self.openLater:Find("laterLabel"):GetComponent(typeof(UILabel))
    self.footPrints = obj.transform:Find("FootPrints")
    self.isNew = obj.transform:Find("Newest").gameObject
end

function LuaSnowAdventureStageTemplate:RemoveListener()
    --TODO JUNYI 什么时机RemoveListener? 在parentWnd OnDisable的时机移除
    g_ScriptEvent:RemoveListener("SelectSnowAdventureStage", self, "OnSelectSnowAdventureStage")
end


function LuaSnowAdventureStageTemplate:RefreshData(stageId, curRouteMaxUnlockStageId, preStagePass, nextSingleChallengeStageId)
    self.stageId = stageId
    local stageType = math.floor(stageId/100)
    local isSingleMode = not (stageType == 4)
    local stageModId = math.fmod(stageId, 100)

    if isSingleMode then
        --单人模式下需要把第一关的脚印给隐藏了
        if stageModId == 1 then
            local childCount = self.footPrints.transform.childCount
            for i = 1, childCount do
                local subChild = self.footPrints.transform:GetChild(i-1)
                subChild.gameObject:SetActive(false)
            end
        end
    end

    if isSingleMode then
        self.isNew:SetActive(stageId == nextSingleChallengeStageId and curRouteMaxUnlockStageId >= nextSingleChallengeStageId)
    else
        self.isNew:SetActive(stageId == curRouteMaxUnlockStageId)
    end

    if stageId <= curRouteMaxUnlockStageId then
        --这些关卡时间上已解锁
        self.highLightBg.gameObject:SetActive(false)
        self.normalBg.gameObject:SetActive(true)
        self.openLater.gameObject:SetActive(false)
        self.button.Enabled = true
        self.highlightButton.Enabled = true
        self:SetFootprintsPattern(true)
        if preStagePass then
            --前置关卡已挑战, 当前关卡可挑战, 显示数字
            self.stageLabel.gameObject:SetActive(true)
            self.highlightStageLabel.gameObject:SetActive(true)
            self.stageLock.gameObject:SetActive(false)

            self.stageLabel.text = stageModId
            self.highlightStageLabel.text = stageModId

            UIEventListener.Get(self.button.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
                g_ScriptEvent:BroadcastInLua("SelectSnowAdventureStage", stageId)
            end)
            UIEventListener.Get(self.highlightButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
                g_ScriptEvent:BroadcastInLua("SelectSnowAdventureStage", stageId)
            end)

        else
            --前置关卡未挑战, 当前关卡不可挑战, 显示锁
            self.stageLabel.gameObject:SetActive(false)
            self.highlightStageLabel.gameObject:SetActive(false)
            self.stageLock.gameObject:SetActive(true)

            --计算当前卡关的sectionId, stageId
            local inPhaseId = nextSingleChallengeStageId % 100
            local phaseName = ""
            if nextSingleChallengeStageId / 100 > 3 then
                phaseName = LocalString.GetString("困难")
            elseif nextSingleChallengeStageId / 100 > 2 then
                phaseName = LocalString.GetString("中级")
            elseif nextSingleChallengeStageId / 100 > 1 then
                phaseName = LocalString.GetString("初级")
            end

            UIEventListener.Get(self.button.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
                g_MessageMgr:ShowMessage("SnowAdventure_PreStageIsLocked", phaseName, inPhaseId)
            end)
            UIEventListener.Get(self.highlightButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
                g_MessageMgr:ShowMessage("SnowAdventure_PreStageIsLocked", phaseName, inPhaseId)
            end)
        end
    else
        --这些关卡时间上未解锁
        self.stageLabel.gameObject:SetActive(false)
        self.highlightStageLabel.gameObject:SetActive(false)
        self.stageLock.gameObject:SetActive(true)
        self.highLightBg.gameObject:SetActive(false)
        self.normalBg.gameObject:SetActive(true)
        self:SetFootprintsPattern(false)
        self.button.Enabled = false
        self.highlightButton.Enabled = false
        if isSingleMode then
            self.normalBg.gameObject:SetActive(false)
            --单人模式下仅显示接下来的一个的即将开放时间
            local startTime = HanJia2023_XuePoLiXianSetting.GetData().StartTime
            startTime = startTime .. " 00:00:00"
            local startTimeStamp = CServerTimeMgr.Inst:GetTimeStampByStr(startTime)
            local curTimeStamp = CServerTimeMgr.Inst.timeStamp
            local SECONDSPERDAY = 24*60*60
            local dayDiff = math.floor((curTimeStamp-startTimeStamp)/SECONDSPERDAY)
            local unlockDayFromStart = HanJia2023_XuePoLiXianLevel.GetData(stageId).UnlockNeedDay-1
            if stageId == curRouteMaxUnlockStageId+1 then
                --时间上未解锁的第一个关卡, 应显示X后天可达此处
                self.openLater.gameObject:SetActive(true)
                self.openLaterLabel.text = g_MessageMgr:FormatMessage("SnowAdventure_OpenInDays", unlockDayFromStart-dayDiff)
                UIEventListener.Get(self.button.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
                    g_MessageMgr:ShowMessage("SnowAdventure_TimeIsLocked", unlockDayFromStart-unlockDayFromStart)
                end)
            else
                --后续未解锁
                self.openLater.gameObject:SetActive(false)
                UIEventListener.Get(self.button.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
                    g_MessageMgr:ShowMessage("SnowAdventure_TimeIsLocked", unlockDayFromStart-unlockDayFromStart)
                end)
            end
        else
            --多人模式下显示每一个即将开放的时间
            self.openLater.gameObject:SetActive(true)

            local startTime = HanJia2023_XuePoLiXianSetting.GetData().StartTime
            startTime = startTime .. " 00:00:00"
            local startTimeStamp = CServerTimeMgr.Inst:GetTimeStampByStr(startTime)
            local curTimeStamp = CServerTimeMgr.Inst.timeStamp
            local SECONDSPERDAY = 24*60*60
            local dayDiff = math.floor((curTimeStamp-startTimeStamp)/SECONDSPERDAY)
            local unlockDayFromStart = LuaHanJia2023Mgr.snowAdventureData.multipleUnlockDays[stageId]-1
            self.openLaterLabel.text = g_MessageMgr:FormatMessage("SnowAdventure_OpenInDays", unlockDayFromStart-dayDiff)
        end
    end
end

function LuaSnowAdventureStageTemplate:SetFootprintsPattern(isSoild)
    local solidColor = "4d6798"
    local unsolidColor = "9bb4d4"
    local myColor = isSoild and solidColor or unsolidColor

    self.footPrints:GetComponent(typeof(UITexture)).color = NGUIText.ParseColor24(myColor,0)
    local childCount = self.footPrints.transform.childCount
    for i = 1, childCount do
        local subChildTex = self.footPrints.transform:GetChild(i-1):GetComponent(typeof(UISprite))
        subChildTex.color = NGUIText.ParseColor24(myColor,0)
    end
end

function LuaSnowAdventureStageTemplate:OnSelectSnowAdventureStage(selectedStageId)
    if self.highLightBg then
        if self.highLightBg.gameObject.activeInHierarchy or self.normalBg.gameObject.activeInHierarchy then
            self.highLightBg.gameObject:SetActive(self.stageId == selectedStageId)
            self.normalBg.gameObject:SetActive(self.stageId ~= selectedStageId)
        end
    end
end

return LuaSnowAdventureStageTemplate