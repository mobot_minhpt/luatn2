local CTeamMgr = import "L10.Game.CTeamMgr"
local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"

CTeamMgr.m_LookApperanceAction = function (this,memberId)
    local appearance = CClientPlayerMgr.Inst:GetPlayerAppearance(memberId)
    if appearance then
        local memberInfo = CTeamMgr.Inst:GetMemberById(memberId)
        if memberInfo then
            LuaPlayerInfoMgr:ShowPlayerApperanceInfo(memberId,memberInfo.m_MemberName,EnumToInt(memberInfo.m_MemberClass),EnumToInt(memberInfo.m_MemberGender),appearance)
        end 
    end
end