local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CScene = import "L10.Game.CScene"
local NGUIMath = import "NGUIMath"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local WndType = import "L10.UI.WndType"
local CTooltip = import "L10.UI.CTooltip"
LuaMiniMapPopWndInfoButton = class()

--@region RegistChildComponent: Dont Modify Manually!


--@endregion RegistChildComponent end
RegistClassMember(LuaMiniMapPopWndInfoButton,"m_Tips")
RegistClassMember(LuaMiniMapPopWndInfoButton,"m_ContentColor")
function LuaMiniMapPopWndInfoButton:Awake()
    local contentColor = 4294967295
    --白色
    if CScene.MainScene ~= nil then
        local data = PublicMap_PublicMap.GetData(CScene.MainScene.SceneTemplateId)
        self.m_Tips = CreateFromClass(MakeGenericClass(List, String))
        if data ~= nil then
            if data.AllowPK then
                if CScene.MainScene:IsInSafeRegion(CClientMainPlayer.Inst) then
                    contentColor = 4294967295
                    CommonDefs.ListAdd(self.m_Tips, typeof(String), LocalString.GetString("不可进行PK战斗"))
                else
                    CommonDefs.ListAdd(self.m_Tips, typeof(String), LocalString.GetString("可进行PK战斗"))
                    contentColor = 4278190335
                    --红色
                    if data.EnablePK then
                        CommonDefs.ListAdd(self.m_Tips, typeof(String), LocalString.GetString("恶意杀人增加PK值"))
                        local default
                        if data.IsDeathPunish then
                            default = LocalString.GetString("死亡有惩罚")
                        else
                            default = LocalString.GetString("死亡无惩罚")
                        end
                        CommonDefs.ListAdd(self.m_Tips, typeof(String), default)
                    else
                        CommonDefs.ListAdd(self.m_Tips, typeof(String), LocalString.GetString("恶意杀人不增加PK值"))
                        local extern
                        if data.IsDeathPunish then
                            extern = LocalString.GetString("死亡有惩罚")
                        else
                            extern = LocalString.GetString("死亡无惩罚")
                        end
                        CommonDefs.ListAdd(self.m_Tips, typeof(String), extern)
                    end
                    local ref
                    if data.CanDuoHun then
                        ref = LocalString.GetString("可夺魂")
                    else
                        ref = LocalString.GetString("不可夺魂")
                    end
                    CommonDefs.ListAdd(self.m_Tips, typeof(String), ref)
                end
            else
                contentColor = 4294967295
                --白色
                CommonDefs.ListAdd(self.m_Tips, typeof(String), LocalString.GetString("不可进行PK战斗"))
            end
            if (data.DecPK and not CScene.MainScene:IsInSafeRegion(CClientMainPlayer.Inst)) or CScene.MainScene.SceneTemplateId == 16100021 then
                CommonDefs.ListAdd(self.m_Tips, typeof(String), LocalString.GetString("在此区域活动可减少PK值"))
            else
                CommonDefs.ListAdd(self.m_Tips, typeof(String), LocalString.GetString("此区域活动无法减少PK值"))
            end
        end
        
        CommonDefs.GetComponent_Component_Type(self.transform:GetChild(0), typeof(UISprite)).color = NGUIMath.HexToColor(contentColor)
    end
    self.m_ContentColor = contentColor
    UIEventListener.Get(self.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnButtonClick()
	end)
end
--@region UIEvent

--@endregion UIEvent

function LuaMiniMapPopWndInfoButton:OnButtonClick()
    CMessageTipMgr.Inst:Display(WndType.Tooltip, true, LocalString.GetString("当前场景须知"), self.m_Tips, 400, self.transform, CTooltip.AlignType.Top, self.m_ContentColor)
end