-- Auto Generated!!
local CButton = import "L10.UI.CButton"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local ChannelDefine = import "L10.Game.ChannelDefine"
local CItemMgr = import "L10.Game.CItemMgr"
local CLoginMgr = import "L10.Game.CLoginMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CSigninMgr = import "L10.Game.CSigninMgr"
local CWelfareBonusAwardTemplate = import "L10.UI.CWelfareBonusAwardTemplate"
local CWelfareBonusMgr = import "L10.Game.CWelfareBonusMgr"
local CWelfareBonusTemplate = import "L10.UI.CWelfareBonusTemplate"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EnumWelfareBonusCategory = import "L10.Game.EnumWelfareBonusCategory"
local EventManager = import "EventManager"
local Gac2Gas = import "L10.Game.Gac2Gas"
local HolidayOnlineTime_Award = import "L10.Game.HolidayOnlineTime_Award"
local KaiFuActivity_LoginDays = import "L10.Game.KaiFuActivity_LoginDays"
local KaiFuActivity_LvUp = import "L10.Game.KaiFuActivity_LvUp"
local KaiFuActivity_OnlineTime = import "L10.Game.KaiFuActivity_OnlineTime"
local KaiFuActivity_QianDaoDays = import "L10.Game.KaiFuActivity_QianDaoDays"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local NGUITools = import "NGUITools"
local QianDao_Holiday = import "L10.Game.QianDao_Holiday"
local SdkU3d = import "NtUniSdk.Unity3d.SdkU3d"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local UISprite = import "UISprite"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local WeddingDay_Gift = import "L10.Game.WeddingDay_Gift"

CWelfareBonusTemplate.m_InitOnlineTime_CS2LuaHook = function (this, key, scrollview)
    -- 设置活动种类
    this.curCategory = EnumWelfareBonusCategory.OnlineTime
    -- 初始化奖励内容
    local data = KaiFuActivity_OnlineTime.GetData(key)
    this:InitiateAward(data.Award, data.Display, scrollview)
    -- 修改指示文本
    this.indexLabel.text = System.String.Format(LocalString.GetString("第{0}次"), key)

    -- 设置领取按钮
    if key ~= CWelfareBonusMgr.Inst.OnlineAwardId then
        CLuaWelfareBonusTemplate:HideGetBonusBtn(this)
    elseif CWelfareBonusMgr.Inst.OnlineAwardLeftTime <= 0 then
        CLuaWelfareBonusTemplate:InitGetBonusBtn(this, LocalString.GetString("领取"), this.OnGetOnlineTimeAward)
    else
        CLuaWelfareBonusTemplate:ShowGetBonusBtn(this, System.String.Empty)
        this:UpdateLeftTime()
    end
end
CWelfareBonusTemplate.m_InitHolidayOnline_CS2LuaHook = function (this, key, scrollView) 
    this.curCategory = EnumWelfareBonusCategory.HolidayOnline
    local data = HolidayOnlineTime_Award.GetData(key)
    this:InitiateAward(data.Award, data.Display, scrollView)
    this.indexLabel.text = System.String.Format(LocalString.GetString("第{0}次"), key)
   
    if key ~= CWelfareBonusMgr.Inst.holidayOnlineAwardId then
        CLuaWelfareBonusTemplate:HideGetBonusBtn(this)
    elseif CWelfareBonusMgr.Inst.holidayOnlineTime <= 0 then
        CLuaWelfareBonusTemplate:InitGetBonusBtn(this, LocalString.GetString("领取"), this.OnGetHolidayOnlineTimeAward)
    else
        CLuaWelfareBonusTemplate:ShowGetBonusBtn(this, System.String.Empty)
        this:UpdateHolidayOnlineTime()
    end
end
CWelfareBonusTemplate.m_InitLoginDays_CS2LuaHook = function (this, key, scrollview) 
    this.curCategory = EnumWelfareBonusCategory.LoginDays
    local data = KaiFuActivity_LoginDays.GetData(key)
    this:InitiateAward(data.Award, data.Display, scrollview)
    this.indexLabel.text = System.String.Format(LocalString.GetString("第{0}天"), key)
    
    if key == CWelfareBonusMgr.Inst.LoginAwardId and CWelfareBonusMgr.Inst.LoginAwardId <= CWelfareBonusMgr.Inst.LoginDays then
        CLuaWelfareBonusTemplate:InitGetBonusBtn(this, LocalString.GetString("领取"), this.OnGetLoginDaysAward)
    else
        CLuaWelfareBonusTemplate:HideGetBonusBtn(this)
    end
end
CWelfareBonusTemplate.m_InitQianDaoDays_CS2LuaHook = function (this, key, scrollview) 
    this.curCategory = EnumWelfareBonusCategory.QianDaoDays
    local data = KaiFuActivity_QianDaoDays.GetData(key)
    this:InitiateAward(data.Award, data.Display, scrollview)
    this.indexLabel.text = System.String.Format(LocalString.GetString("第{0}天"), key)
   
    if key == CWelfareBonusMgr.Inst.QianDaoAwardId and CWelfareBonusMgr.Inst.QianDaoAwardId <= CWelfareBonusMgr.Inst.QianDaoDays then
        CLuaWelfareBonusTemplate:InitGetBonusBtn(this, LocalString.GetString("签到"), this.OnGetQianDaoDaysAward)
    else
        CLuaWelfareBonusTemplate:HideGetBonusBtn(this)
    end
end
CWelfareBonusTemplate.m_InitLvUp_CS2LuaHook = function (this, key, scrollview) 
    this.curCategory = EnumWelfareBonusCategory.LvUp
    local data = KaiFuActivity_LvUp.GetData(key)
    this:InitiateAward(CWelfareBonusMgr.Inst:InitLvUpAward(key), data.Display, scrollview)
    this.indexLabel.text = System.String.Format(LocalString.GetString("{0}级"), data.Level)

    if key == CWelfareBonusMgr.Inst.LvUpAwardId and KaiFuActivity_LvUp.GetData(CWelfareBonusMgr.Inst.LvUpAwardId).Level <= CClientMainPlayer.Inst.Level then
        CLuaWelfareBonusTemplate:InitGetBonusBtn(this, LocalString.GetString("领取"), this.OnGetLvUpAward)
    else
        CLuaWelfareBonusTemplate:HideGetBonusBtn(this)
    end
end
CWelfareBonusTemplate.m_InitHoliday_CS2LuaHook = function (this, category, key, scrollview)
    this.curCategory = category
    local data = QianDao_Holiday.GetData(key)
    this:InitiateAward(data.Item, data.Display, scrollview)
    this.indexLabel.text = System.String.Format(LocalString.GetString("累计{0}天"), data.NeedTimes)

    -- 分设计大赛和普通节日两类
    local info = nil
    if category == EnumWelfareBonusCategory.DesignCompetiton then
        info = CSigninMgr.Inst.designCompetitionInfo
    else
        info = CSigninMgr.Inst.holidayInfo
    end

    -- 按钮的四种情况：领取、补签、不可领取、已领取
    if data.NeedTimes == info.totalTimes + 1 and not info.hasSignGift and info.todayInfo.Open == 1 then
        CLuaWelfareBonusTemplate:InitGetBonusBtn(this, LocalString.GetString("领取"), this.OnGetHolidayGift)
    elseif data.NeedTimes == info.totalTimes + 1 and info.hasSignGift and info.todayInfo.Open == 1 and info.validTimes >= data.NeedTimes then
        CLuaWelfareBonusTemplate:InitGetBonusBtn(this, LocalString.GetString("补签"), this.OnGetHolidayGift)
    elseif data.NeedTimes > info.totalTimes or info.todayInfo.Open == 0 then
        CLuaWelfareBonusTemplate:HideGetBonusBtn(this)
    else
        CLuaWelfareBonusTemplate:ShowGetBonusBtn(this, LocalString.GetString("已领取"))
    end
end
CWelfareBonusTemplate.m_InitHuiliu_CS2LuaHook = function (this, index, award, scrollview) 
    index = index + 1
    this.curCategory = EnumWelfareBonusCategory.Huiliu
    this:InitiateAward(award, System.String.Empty, scrollview)
    this.indexLabel.text = System.String.Format(LocalString.GetString("累计{0}天"), index)

    if index == CSigninMgr.Inst.huiliuDay and not CSigninMgr.Inst.huiliuAwardGetToday then
        CLuaWelfareBonusTemplate:InitGetBonusBtn(this, LocalString.GetString("领取"), this.OnGetHuiliuAward)
    elseif index < CSigninMgr.Inst.huiliuDay or (index == CSigninMgr.Inst.huiliuDay and CSigninMgr.Inst.huiliuAwardGetToday) then
        CLuaWelfareBonusTemplate:ShowGetBonusBtn(this, LocalString.GetString("已领取"))
    else
        CLuaWelfareBonusTemplate:HideGetBonusBtn(this)
    end
end
CWelfareBonusTemplate.m_InitMarriageAnniversary_CS2LuaHook = function (this, key, scrollview, isGiftGot, nextGiftTime, sprite1) 
    if this.marriageSprite1 ~= nil then
        this.marriageSprite1.spriteName = sprite1
        this.marriageSprite1:ResetAndUpdateAnchors()
    end
    this.marriageAnniKey = key
    local gift = WeddingDay_Gift.GetData(key)
    if gift == nil then return end
    
    this.curCategory = EnumWelfareBonusCategory.Undefined
    this:InitiateAward(gift.NewAward, System.String.Empty, scrollview)
    this.indexLabel.text = gift.Desc

    if isGiftGot then
        CLuaWelfareBonusTemplate:ShowGetBonusBtn(this, LocalString.GetString("已领取"))
    elseif CommonDefs.op_LessThanOrEqual_DateTime_DateTime(CServerTimeMgr.ConvertTimeStampToZone8Time(nextGiftTime), CServerTimeMgr.Inst:GetZone8Time()) then
        CLuaWelfareBonusTemplate:InitGetBonusBtn(this, LocalString.GetString("领    取"), this.OnGetMarriageGift)
    else
        CLuaWelfareBonusTemplate:HideGetBonusBtn(this)
    end
end
CWelfareBonusTemplate.m_InitiateAward_CS2LuaHook = function (this, award, display, scrollView)
    this:Initiate()
    local awardString = CommonDefs.StringSplit_ArrayChar(award, ";")
    local fxAward = InitializeListWithArray(CreateFromClass(MakeGenericClass(List, String)), String, CommonDefs.StringSplit_ArrayChar(display, ";"))

    if awardString ~= nil then
        for i = 0, awardString.Length-1 do
            local info = CommonDefs.StringSplit_ArrayChar(awardString[i], ",")
            if info ~= nil and info.Length >= 2 then
                local award = NGUITools.AddChild(this.table.gameObject, this.itemcell)
                award:SetActive(true)
                local default, templateId = System.UInt32.TryParse(info[0])
                if default then
                    local item = CItemMgr.Inst:GetItemTemplate(templateId)
                    local template = CommonDefs.GetComponent_GameObject_Type(award, typeof(CWelfareBonusAwardTemplate))
                    if item ~= nil and template ~= nil then
                        template:Init(item, info[1], CommonDefs.ListContains(fxAward, typeof(String), tostring(templateId)), scrollView)
                    end
                end
            end
        end
        if this.table.transform.childCount > this.table.maxPerLine then
            this.background.height = this.background.height + (math.floor((this.table.transform.childCount - 1) / this.table.maxPerLine) * (CommonDefs.GetComponent_GameObject_Type(this.itemcell, typeof(UISprite)).height + 15))
        end
        this.table:Reposition()
    end
end
CWelfareBonusTemplate.m_OnGetQianDaoDaysAward_CS2LuaHook = function (this, go) 
    if CLoginMgr.Inst:GetAppChannel() == ChannelDefine.OPPO then
        local isFromOPPOGameCenter = SdkU3d.getPropInt("isFromGameCenter", 0)
        if isFromOPPOGameCenter ~= 1 then
            local msg = g_MessageMgr:FormatMessage("Launch_Oppo_Game_Center")
            MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function () 
                SdkU3d.ntExtendFunc("{\"methodId\":\"launchGameCenter\"}")
            end), nil, nil, nil, false)
            return
        end

        Gac2Gas.RequestQianDaoAward(CWelfareBonusMgr.Inst.QianDaoAwardId)
    end
end
CWelfareBonusTemplate.m_UpdateLeftTime_CS2LuaHook = function (this) 
    if this.curCategory ~= EnumWelfareBonusCategory.OnlineTime then return end
    CLuaWelfareBonusTemplate:UpdateLeftTime(this, CWelfareBonusMgr.Inst.OnlineAwardLeftTime, EnumEventType.OnlineTimeAwardReceived)
end
CWelfareBonusTemplate.m_UpdateHolidayOnlineTime_CS2LuaHook = function (this) 
    if this.curCategory ~= EnumWelfareBonusCategory.HolidayOnline then return end
    CLuaWelfareBonusTemplate:UpdateLeftTime(this, CWelfareBonusMgr.Inst.holidayOnlineTime, EnumEventType.HolidayOnlineTimeAwardReceived)
end

-- 扩展
CLuaWelfareBonusTemplate = {}
function CLuaWelfareBonusTemplate:UpdateLeftTime(this, leftTime, eventType)
    if leftTime <= 0 then
        EventManager.Broadcast(eventType)
        return
    end
    local hour = math.floor(leftTime / 3600)
    local minute = math.floor(leftTime % 3600 / 60)
    local second = leftTime % 60
    if hour > 0 then
        this.getBonusBtnLabel.text = System.String.Format("{0}:{1}:{2}", math.floor(hour / 10) > 0 and tostring(hour) or "0" .. tostring(hour), math.floor(minute / 10) > 0 and tostring(minute) or "0" .. tostring(minute), math.floor(second / 10) > 0 and tostring(second) or "0" .. tostring(second))
    else
        this.getBonusBtnLabel.text = System.String.Format("{0}:{1}", math.floor(minute / 10) > 0 and tostring(minute) or "0" .. tostring(minute), math.floor(second / 10) > 0 and tostring(second) or "0" .. tostring(second))
    end
end
-- 按钮可操作，latexText按钮显示文本，func点击触发
function CLuaWelfareBonusTemplate:InitGetBonusBtn(this, labelText, func)
    this.getBonusBtnLabel.text = labelText
    this.getBonusBtn.gameObject:SetActive(true)
    CommonDefs.GetComponent_GameObject_Type(this.getBonusBtn, typeof(CButton)).Enabled = true
    UIEventListener.Get(this.getBonusBtn).onClick = MakeDelegateFromCSFunction(func, VoidDelegate, this)
end
-- 按钮不可操作但可见
function CLuaWelfareBonusTemplate:ShowGetBonusBtn(this, labelText)
    this.getBonusBtnLabel.text = labelText
    this.getBonusBtn.gameObject:SetActive(true)
    CommonDefs.GetComponent_GameObject_Type(this.getBonusBtn, typeof(CButton)).Enabled = false
end
-- 按钮不可操作不可见
function CLuaWelfareBonusTemplate:HideGetBonusBtn(this)
    this.getBonusBtn.gameObject:SetActive(false)
end