require("common/common_include")

local CUIFx = import "L10.UI.CUIFx"
local Time = import "UnityEngine.Time"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow" 

LuaYanHuaViewWnd = class()

RegistChildComponent(LuaYanHuaViewWnd, "YanHuaViewButton", UISprite)
RegistChildComponent(LuaYanHuaViewWnd, "m_ShowFx","ShowFx", CUIFx)
RegistClassMember(LuaYanHuaViewWnd, "m_LightPos")

function LuaYanHuaViewWnd:Init()
end

function LuaYanHuaViewWnd:OnEnable()
    g_ScriptEvent:AddListener("ShowYanHuaViewBtn",self,"OnShowYanHuaViewBtn")
    g_ScriptEvent:AddListener("StartLoadScene", self, "StartLoadScene")
end

function LuaYanHuaViewWnd:Update()
    if self.YanHuaViewButton.alpha == 0 then
        return
    end

    if CLuaYanHuaEditorMgr.ViewEnterTime <= 0 then
        CLuaYanHuaEditorMgr.ViewEnterTime = 0
        self.m_ShowFx:DestroyFx()
        self.YanHuaViewButton.alpha = 0
        CameraFollow.Inst.MaxSpHeight = 0
        return
    end
    CLuaYanHuaEditorMgr.ViewEnterTime = CLuaYanHuaEditorMgr.ViewEnterTime - Time.deltaTime
end

function LuaYanHuaViewWnd:OnDisable()
    g_ScriptEvent:RemoveListener("ShowYanHuaViewBtn",self,"OnShowYanHuaViewBtn")
    g_ScriptEvent:RemoveListener("StartLoadScene", self, "StartLoadScene")
end

function LuaYanHuaViewWnd:OnShowYanHuaViewBtn()
    self.YanHuaViewButton.alpha = 1
    self.m_ShowFx:DestroyFx()
	self.m_ShowFx:LoadFx("fx/ui/prefab/qinghongyupei_tishi_fx01.prefab")
    CLuaYanHuaEditorMgr.ViewEnterTime = CLuaYanHuaEditorMgr.DefaultViewTime
    CLuaYanHuaEditorMgr.InYanHuaViewStatus = true
end

function LuaYanHuaViewWnd:StartLoadScene(sceneName)
    if sceneName ~= CLuaYanHuaEditorMgr.CurLightYanHuaSceneName then
        self.m_ShowFx:DestroyFx()
        self.YanHuaViewButton.alpha = 0
        CLuaYanHuaEditorMgr.ViewEnterTime = 0
        CLuaYanHuaEditorMgr.InYanHuaViewStatus = false
        CameraFollow.Inst.MaxSpHeight = 0

        for i,lightingTick in ipairs(CLuaYanHuaEditorMgr.LightingTickList) do
            if lightingTick ~= nil then
                UnRegisterTick(lightingTick)
            end 
        end
        CLuaYanHuaEditorMgr.LightingTickList = {}
    end
end

return LuaYanHuaViewWnd
