-- Auto Generated!!
local CPVPMgr = import "L10.Game.CPVPMgr"
local CPVPRecordListItem = import "L10.UI.CPVPRecordListItem"
local CPVPRecordWnd = import "L10.UI.CPVPRecordWnd"
CPVPRecordWnd.m_CellForRowAtIndex_CS2LuaHook = function (this, index) 
    local cellIdentifier = "RowCell"

    local cell = this.tableView:DequeueReusableCellWithIdentifier(cellIdentifier)
    if cell == nil then
        cell = this.tableView:AllocNewCellWithIdentifier(this.recordTemplate, cellIdentifier)
    end
    CommonDefs.GetComponent_GameObject_Type(cell, typeof(CPVPRecordListItem)):Init(CPVPMgr.RecordList[index])

    return cell
end
