require("common/common_include")

local EnumActionState=import "L10.Game.EnumActionState"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"

CLuaChengZhongMgr = {}
CLuaChengZhongMgr.m_NpcEngineId = 0
CLuaChengZhongMgr.m_PlayerEngineId = 0

function CLuaChengZhongMgr.TryBindPlayer(engineId)
	if engineId ~= CLuaChengZhongMgr.m_PlayerEngineId then
		return
	end

	CLuaChengZhongMgr.BindPlayer()
end

function CLuaChengZhongMgr.BindPlayer()
	local npcObj = CClientObjectMgr.Inst:GetObject(CLuaChengZhongMgr.m_NpcEngineId)
	if not npcObj then
		return
	end

	local playerObj = CClientObjectMgr.Inst:GetObject(CLuaChengZhongMgr.m_PlayerEngineId)
	if not playerObj then
		return
	end

	npcObj.RO:AttachRO(playerObj.RO, "ride01", 0, 0.1, 0.2, 0, 0, 0, 1)
	playerObj:ShowExpressionActionState(47000041)
end

function CLuaChengZhongMgr.UnBindPlayer()
	local npcObj = CClientObjectMgr.Inst:GetObject(CLuaChengZhongMgr.m_NpcEngineId)
	if not npcObj then
		return
	end

	local playerObj = CClientObjectMgr.Inst:GetObject(CLuaChengZhongMgr.m_PlayerEngineId)
	if not playerObj then
		return
	end

	playerObj:ShowActionState(EnumActionState.Idle, nil)
	npcObj.RO:DetachRO(playerObj.RO, 0, 0, 0, 0, 0, 0, 1)
end
