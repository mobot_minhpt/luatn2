local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CUITexture      = import "L10.UI.CUITexture"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"

LuaCHRoomTemplate = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaCHRoomTemplate, "NormalBackground", GameObject)
RegistChildComponent(LuaCHRoomTemplate, "SpecialBackground", GameObject)
RegistChildComponent(LuaCHRoomTemplate, "RankTip", GameObject)
RegistChildComponent(LuaCHRoomTemplate, "RoomTag", UISprite)
RegistChildComponent(LuaCHRoomTemplate, "Portrait", CUITexture)
RegistChildComponent(LuaCHRoomTemplate, "OwnerNameLabel", UILabel)
RegistChildComponent(LuaCHRoomTemplate, "PublicRoom", GameObject)
RegistChildComponent(LuaCHRoomTemplate, "PrivateRoom", GameObject)
RegistChildComponent(LuaCHRoomTemplate, "RoomNamePanel", CCommonLuaScript)

RegistClassMember(LuaCHRoomTemplate, "m_RoomId")
RegistClassMember(LuaCHRoomTemplate, "m_RoomOwnerId")
RegistClassMember(LuaCHRoomTemplate, "m_IsSpecialRoom")
--@endregion RegistChildComponent end

function LuaCHRoomTemplate:Awake()
end

function LuaCHRoomTemplate:InitRoomInfo(roomInfo)
    self.m_RoomId = roomInfo.Owner.RoomId
    self:SetRoomTitle(roomInfo.Title)
    self.OwnerNameLabel.color = roomInfo.Owner.IsMe and NGUIText.ParseColor24("00ff60", 0) or NGUIText.ParseColor24("ffffff", 0)
    self.OwnerNameLabel.text = roomInfo.Owner.PlayerName
    local tagData = GameplayItem_ClubHouseTag.GetData(roomInfo.Tag)
    if tagData then
        self.RoomTag.gameObject:SetActive(true)
        self.RoomTag.spriteName = tagData.TagIcon
    else
        self.RoomTag.gameObject:SetActive(false)
    end

    if self.m_RoomOwnerId ~= roomInfo.Owner.PlayerId then
        -- 当且仅当房主发生变化时，才需要重新加载头像
        self.Portrait:LoadNPCPortrait(roomInfo.Owner.Portrait, false)
        self.m_RoomOwnerId = roomInfo.Owner.PlayerId
        LuaClubHouseMgr:SetMengDaoProfile(self.Portrait, roomInfo.Owner.PlayerId, "CHRoomTemplate")
    end

    self:InitPublicRoom(roomInfo)
    
    self.RoomNamePanel:Init(roomInfo.RoomId, roomInfo.RoomName, self.m_IsSpecialRoom)
    
    self.PublicRoom.transform:Find("BroadcasterNumLabel/OpenShow").gameObject:SetActive(roomInfo.HasSpeaking)
end

function LuaCHRoomTemplate:InitPublicRoom(roomInfo)
    self.PublicRoom.gameObject:SetActive(true)
    self.PrivateRoom.gameObject:SetActive(false)
    self.PublicRoom.transform:Find("AudienceNumLabel"):GetComponent(typeof(UILabel)).text = tostring(roomInfo.ListenerCount)
    self.PublicRoom.transform:Find("BroadcasterNumLabel"):GetComponent(typeof(UILabel)).text = tostring(roomInfo.MicCount)
    self.PublicRoom.transform:Find("HeatValueLabel"):GetComponent(typeof(UILabel)).text = tostring(roomInfo.Heat)
end

function LuaCHRoomTemplate:InitPrivateRoom(roomInfo)
    self.PublicRoom.gameObject:SetActive(false)
    self.PrivateRoom.gameObject:SetActive(true)
    local playerNum = roomInfo.MicCount + roomInfo.ListenerCount
    local playerNumLabel = self.PrivateRoom.transform:Find("PlayerNumLabel"):GetComponent(typeof(UILabel))
    playerNumLabel.text = tostring(playerNum)
    self.PrivateRoom.transform:Find("HeatValueLabel"):GetComponent(typeof(UILabel)).text = tostring(roomInfo.Heat)
end

function LuaCHRoomTemplate:SetRoomTitle(title)
    local titleInfo = GameplayItem_ClubHouseTitle.GetData(title)
    self.m_IsSpecialRoom = titleInfo ~= nil
    self.NormalBackground:SetActive(not titleInfo)
    self.SpecialBackground:SetActive(titleInfo)
    self.RankTip:SetActive(titleInfo)
    -- if titleInfo then
    --     local rankImgList = {g_sprites.Common_Huangguan_01, g_sprites.Common_Huangguan_02, g_sprites.Common_Huangguan_03}
    --     self.RankTip.transform:Find("RankImage"):GetComponent(typeof(UISprite)).spriteName = rankImgList[title]
    --     self.RankTip.transform:Find("Label"):GetComponent(typeof(UILabel)).text = titleInfo.TitleName
    -- end
end

function LuaCHRoomTemplate:OnClubHouseNotifyUpdateTitle(RoomId, Title)
    if RoomId == self.m_RoomId then
        self:SetRoomTitle(Title)
    end
end

--@region UIEvent

--@endregion UIEvent
function LuaCHRoomTemplate:OnEnable()
    g_ScriptEvent:AddListener("ClubHouse_Notify_UpdateTitle", self, "OnClubHouseNotifyUpdateTitle") 
end

function LuaCHRoomTemplate:OnDisable()
    g_ScriptEvent:RemoveListener("ClubHouse_Notify_UpdateTitle", self, "OnClubHouseNotifyUpdateTitle") 
end
