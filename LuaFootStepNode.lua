local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local UISlider = import "UISlider"
local BoxCollider = import "UnityEngine.BoxCollider"
local Ease = import "DG.Tweening.Ease"
LuaFootStepNode = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaFootStepNode, "Label", "Label", UILabel)
RegistChildComponent(LuaFootStepNode, "LongNode", "LongNode", GameObject)
RegistChildComponent(LuaFootStepNode, "CommonNode", "CommonNode", GameObject)
RegistChildComponent(LuaFootStepNode, "Thumb", "Thumb", GameObject)
RegistChildComponent(LuaFootStepNode, "LongHighlight", "LongHighlight", GameObject)
RegistChildComponent(LuaFootStepNode, "LongNormal", "LongNormal", UISlider)
RegistChildComponent(LuaFootStepNode, "Normal", "Normal", GameObject)
RegistChildComponent(LuaFootStepNode, "Highlight", "Highlight", GameObject)
RegistChildComponent(LuaFootStepNode, "Miss", "Miss", GameObject)
RegistChildComponent(LuaFootStepNode, "Success", "Success", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaFootStepNode,"m_IsLongPress")
RegistClassMember(LuaFootStepNode,"m_TotalLongPressTime")
RegistClassMember(LuaFootStepNode,"m_IsPress")
RegistClassMember(LuaFootStepNode,"m_CurPressTime")

RegistClassMember(LuaFootStepNode,"m_CurStatus")
RegistClassMember(LuaFootStepNode,"m_LeftOrRight")
RegistClassMember(LuaFootStepNode,"m_MoveSpeed")
RegistClassMember(LuaFootStepNode,"m_StartPos")
RegistClassMember(LuaFootStepNode,"m_EndPos")
RegistClassMember(LuaFootStepNode,"m_selfHeight")

RegistClassMember(LuaFootStepNode,"m_MoveTween")
RegistClassMember(LuaFootStepNode,"m_MoveTick")
RegistClassMember(LuaFootStepNode,"m_LongPressTick")

RegistClassMember(LuaFootStepNode,"m_OnRecycleFunc")
RegistClassMember(LuaFootStepNode,"m_IsSuccess")
RegistClassMember(LuaFootStepNode,"m_OnSuccessFunc")
RegistClassMember(LuaFootStepNode,"m_OnMissFunc")

RegistClassMember(LuaFootStepNode,"m_LongPressCheckPrecent")
RegistClassMember(LuaFootStepNode,"m_Index")

function LuaFootStepNode:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.m_StartPos = nil
    self.m_EndPos = nil
    self.m_OnRecycleFunc = nil
    self.m_OnSuccessFunc = nil
    self.m_OnMissFunc = nil
    self.m_CurStatus = 0    -- 0:未开始 1:移动中 2:可点击 3:点击完成或者Miss
    self.m_IsLongPress = false
    self.m_TotalLongPressTime = 0
    self.m_MoveTick = nil
    self.m_IsSuccess = false

    self.m_LongPressTick = nil
    self.m_MoveTween = nil
    self.m_selfHeight = 0
    self.m_MoveSpeed = 0
    self.m_LeftOrRight = 0
    self.m_LongPressCheckPrecent = 0.8 -- 长按时间高于该比例即算成功
    self.m_Index = 0

    UIEventListener.Get(self.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnClick(go)
    end)

    UIEventListener.Get(self.gameObject).onPress = DelegateFactory.BoolDelegate(function(go, ispressed)
        self:OnPress(go, ispressed)
    end)
end

function LuaFootStepNode:Init(  index,startPos,endPos,moveSpeed,leftOrRight,
                                isLongPress,isFirstLongStep,longStepHeight,totalLongPressTime,
                                onRecycleFunc,onMissFunc,onSuccessFunc,isAutoStartMove)
    self.m_Index = index
    self.m_StartPos = startPos
    self.m_EndPos = endPos
    self.m_IsLongPress = isLongPress
    self.m_TotalLongPressTime = totalLongPressTime
    self.m_OnRecycleFunc = onRecycleFunc
    self.m_OnSuccessFunc = onSuccessFunc
    self.m_OnMissFunc = onMissFunc
    self.m_CurStatus = 0
    self.m_MoveSpeed = moveSpeed
    self.m_IsSuccess = false
    self.m_IsPress = false
    self.m_CurPressTime = 0
    self.m_LeftOrRight = 0
    local width = 0
    LuaUtils.SetLocalPosition(self.gameObject.transform,self.m_StartPos.x,self.m_StartPos.y,0)
    self.LongNode.gameObject:SetActive(isLongPress)
    self.CommonNode.gameObject:SetActive(not isLongPress)
    self.Label.gameObject:SetActive(isLongPress and isFirstLongStep)
    self.Miss.gameObject:SetActive(false)
    self.Success.gameObject:SetActive(false)
    if isLongPress then
        self.LongHighlight.gameObject:SetActive(true)
        self.LongNormal.value = 0
        self.Thumb.gameObject:SetActive(not isLongPress)
        self.m_selfHeight = longStepHeight
        local nomalSprite = self.LongNormal:GetComponent(typeof(UISprite))
        local ProgressSprite = self.LongNormal.transform:Find("Progress"):GetComponent(typeof(UISprite))
        local highlightSprite = self.LongHighlight:GetComponent(typeof(UISprite))
        nomalSprite.height = longStepHeight
        ProgressSprite.height = longStepHeight
        highlightSprite.height = longStepHeight
        width = nomalSprite.width
        nomalSprite.flip = self.m_LeftOrRight == 0 and UIBasicSprite.Flip.Nothing or UIBasicSprite.Flip.Horizontally
        ProgressSprite.flip = self.m_LeftOrRight == 0 and UIBasicSprite.Flip.Nothing or UIBasicSprite.Flip.Horizontally
        highlightSprite.flip = self.m_LeftOrRight == 0 and UIBasicSprite.Flip.Nothing or UIBasicSprite.Flip.Horizontally
        Extensions.SetLocalPositionY(self.Label.transform, longStepHeight / 2)
    else
        self.m_selfHeight = self.Normal:GetComponent(typeof(UISprite)).height
        self.Highlight.gameObject:SetActive(true)
        self.Normal.gameObject:SetActive(true)
        local nomalSprite = self.Normal:GetComponent(typeof(UISprite))
        local highlightSprite = self.Highlight:GetComponent(typeof(UISprite))
        nomalSprite.flip = self.m_LeftOrRight == 0 and UIBasicSprite.Flip.Nothing or UIBasicSprite.Flip.Horizontally
        highlightSprite.flip = self.m_LeftOrRight == 0 and UIBasicSprite.Flip.Nothing or UIBasicSprite.Flip.Horizontally
        width = nomalSprite.width
    end

    local collider = self.gameObject:GetComponent(typeof(BoxCollider))
    collider.size = Vector3(width,self.m_selfHeight,0)
    collider.center = Vector3(0,self.m_selfHeight / 2,0)
    if isAutoStartMove then
        self:StartMove()
    end
end

function LuaFootStepNode:StartMove()
    self:ClearTick()
    local endPos = self.m_EndPos.y - self.m_selfHeight * 2
    local moveDistance = self.m_StartPos.y - endPos
    local speed = self.m_MoveSpeed

    local checkTime = (self.m_StartPos.y - self.m_EndPos.y ) / speed
    self.m_CurStatus = 1
    self.m_MoveTween = LuaTweenUtils.TweenPositionY(self.transform, endPos, moveDistance /speed)
    LuaTweenUtils.SetEase(self.m_MoveTween,Ease.Linear)
    self.m_MoveTick = RegisterTickOnce(function()
        self:OnMoveEnd()
    end, checkTime * 1000)

    LuaTweenUtils.OnComplete(self.m_MoveTween, function()
        if self.m_OnRecycleFunc then
            self.m_OnRecycleFunc(self,self.m_Index)
        end
    end)
end

function LuaFootStepNode:OnClick(go)
    if self.m_IsLongPress or self.m_CurStatus ~= 2 then return end
    self:OnSuccess()
end

function LuaFootStepNode:OnPress(go, ispressed)
    if not self.m_IsLongPress or self.m_CurStatus ~= 2 then return end
    if self.m_LongPressTick then UnRegisterTick(self.m_LongPressTick) self.m_LongPressTick = nil end
    if ispressed then
        self.m_IsPress = true
        self.m_LongPressTick = RegisterTick(function()
            self.m_CurPressTime = self.m_CurPressTime + 0.1
            if self.m_TotalLongPressTime > 0 then
                self.LongNormal.value = self.m_CurPressTime / self.m_TotalLongPressTime
                local precent = self.m_CurPressTime / self.m_TotalLongPressTime
                self.Thumb.gameObject:SetActive(true)
                Extensions.SetLocalPositionY(self.Thumb.transform, self.m_selfHeight * precent )
                if self.m_CurPressTime >= self.m_TotalLongPressTime then
                    UnRegisterTick(self.m_LongPressTick) 
                    self.m_LongPressTick = nil
                    self:OnSuccess()
                end
            end
        end, 100)
    else
        self.m_IsPress = false
        if self.m_CurPressTime >= self.m_TotalLongPressTime * self.m_LongPressCheckPrecent then
            self:OnSuccess()
        else
            self:OnMiss()
        end
    end
end

function LuaFootStepNode:SetCanClickStatus()
    self.m_CurStatus = 2
end

function LuaFootStepNode:OnMoveEnd()
    if self.m_LongPressTick then UnRegisterTick(self.m_LongPressTick) self.m_LongPressTick = nil end
    if self.m_CurStatus ~= 2 then return end
    if not self.m_IsSuccess then
        if self.m_IsLongPress then
            if self.m_IsPress then
                if self.m_CurPressTime >= self.m_TotalLongPressTime * self.m_LongPressCheckPrecent then
                    self:OnSuccess()
                    return
                end
            end
        end
        self:OnMiss()
    end
end

function LuaFootStepNode:OnMiss()
    self.m_IsSuccess = false
    self.m_CurStatus = 3
    self.Miss.gameObject:SetActive(true)
    if self.m_IsLongPress then
        self.LongHighlight.gameObject:SetActive(false)
        self.LongNormal.value = 0
        self.Thumb.gameObject:SetActive(false)
        self.Label.gameObject:SetActive(false)
    else
        self.Highlight.gameObject:SetActive(false)
        self.Normal.gameObject:SetActive(true)
    end
    if self.m_OnMissFunc then
        self.m_OnMissFunc(self,self.m_Index)
    end
end

function LuaFootStepNode:OnSuccess()
    self.m_IsSuccess = true
    self.m_CurStatus = 3
    self.Success.gameObject:SetActive(true)
    if self.m_IsLongPress then
        self.LongHighlight.gameObject:SetActive(false)
        self.LongNormal.value = 0
        self.Thumb.gameObject:SetActive(false)
        self.Label.gameObject:SetActive(false)
    else
        self.Highlight.gameObject:SetActive(false)
        self.Normal.gameObject:SetActive(true)
    end
    if self.m_OnSuccessFunc then
        self.m_OnSuccessFunc(self,self.m_Index)
    end
end

function LuaFootStepNode:Stop()
    self:ClearTick()
    LuaTweenUtils.DOKill(self.transform, false)
end

function LuaFootStepNode:ClearTick()
    if self.m_MoveTick then
        UnRegisterTick(self.m_MoveTick)
        self.m_MoveTick = nil
    end
    if self.m_LongPressTick then
        UnRegisterTick(self.m_LongPressTick)
        self.m_LongPressTick = nil
    end
end

function LuaFootStepNode:OnDisable()
    self:ClearTick()
end


--@region UIEvent

--@endregion UIEvent

