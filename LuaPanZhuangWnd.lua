local LineRenderer = import "UnityEngine.LineRenderer"
local Camera = import "UnityEngine.Camera"
local RenderTexture = import "UnityEngine.RenderTexture"
local RenderTextureFormat = import "UnityEngine.RenderTextureFormat"
local UITexture = import "UITexture"
local GameObject = import "UnityEngine.GameObject"
local SystemObject = import "System.Object"
local RenderTextureReadWrite = import "UnityEngine.RenderTextureReadWrite"
local UIRect = import "UIRect"
local CameraClearFlags = import "UnityEngine.CameraClearFlags"

LuaPanZhuangWnd = class()

RegistChildComponent(LuaPanZhuangWnd,"m_WritingBrush","WritingBrush", CUITexture)
RegistChildComponent(LuaPanZhuangWnd,"m_WritingBrushRoot","WritingBrushRoot", GameObject)
RegistChildComponent(LuaPanZhuangWnd,"m_Label","Label", UILabel)
RegistChildComponent(LuaPanZhuangWnd,"m_LineRender","LineRender", LineRenderer)
RegistChildComponent(LuaPanZhuangWnd,"m_MainTexture","MainTexture", UITexture)
RegistChildComponent(LuaPanZhuangWnd,"m_Camera","Camera", Camera)
RegistChildComponent(LuaPanZhuangWnd,"m_EffectTexture1","EffectTexture1", GameObject)
RegistChildComponent(LuaPanZhuangWnd,"m_EffectTexture2","EffectTexture2", GameObject)
RegistChildComponent(LuaPanZhuangWnd,"m_Effect3","Effect3", CUIFx)
RegistChildComponent(LuaPanZhuangWnd,"m_RedBg","RedBg", GameObject)
RegistChildComponent(LuaPanZhuangWnd,"m_DrawArea","DrawArea", UIRect)

RegistClassMember(LuaPanZhuangWnd, "m_CountDownTick")
RegistClassMember(LuaPanZhuangWnd, "m_DrawLineTick")
RegistClassMember(LuaPanZhuangWnd, "m_CloseWndTick")
RegistClassMember(LuaPanZhuangWnd, "m_PlayEffectTick")
RegistClassMember(LuaPanZhuangWnd, "m_CountDownTime")
RegistClassMember(LuaPanZhuangWnd, "m_IsInit")
RegistClassMember(LuaPanZhuangWnd, "m_LastPos")
RegistClassMember(LuaPanZhuangWnd, "m_VertexCount")
RegistClassMember(LuaPanZhuangWnd, "m_EffetType")
RegistClassMember(LuaPanZhuangWnd, "m_DrawTime")
RegistClassMember(LuaPanZhuangWnd, "m_RenderTexture")
RegistClassMember(LuaPanZhuangWnd, "m_IsDrawEnd")
RegistClassMember(LuaPanZhuangWnd, "m_IsTaskFinished")
RegistClassMember(LuaPanZhuangWnd, "m_IsStartDraw")

function LuaPanZhuangWnd:Init()
    self.m_EffectTexture1:SetActive(false)
    self.m_EffectTexture2:SetActive(false)
    self.m_RedBg:SetActive(false)
    self.m_RenderTexture = RenderTexture.GetTemporary(512, 512, 0, RenderTextureFormat.ARGB32,RenderTextureReadWrite.Default)
    self.m_RenderTexture.name = "PanZhuangWndMainTexture"
    self.m_Camera.targetTexture = self.m_RenderTexture
    self.m_Camera.clearFlags = CameraClearFlags.SolidColor
    self.m_Camera:Render()
    self.m_Camera.clearFlags = CameraClearFlags.Depth
    self.m_MainTexture.mainTexture = self.m_RenderTexture
    local countDownTime = ZhuJueJuQing_Setting.GetData().PanZhuangData[CLuaTaskMgr.m_PanZhuangWnd_EffectType * 2 - 2]
    self.m_VertexCount = 0
    self:PanZhuang(countDownTime, CLuaTaskMgr.m_PanZhuangWnd_EffectType, CLuaTaskMgr.m_PanZhuangWnd_DrawTime)
end

function LuaPanZhuangWnd:Update()
    if (not self.m_IsInit) then return end

    if Input.GetMouseButtonDown(0) then
        self.m_IsStartDraw = true
        if (not self.m_DrawTime) or self.m_DrawTime < 0 then return end
        self:CancelDrawLineTick()
        self.m_DrawLineTick = RegisterTickOnce(function ()
            self.m_IsDrawEnd = true
        end,self.m_DrawTime)
    end
    if Input.GetMouseButton(0) then
        local mainCamera = CUIManager.UIMainCamera
        if mainCamera then
            local pos = mainCamera:ScreenToWorldPoint(Vector3(Input.mousePosition.x, Input.mousePosition.y, 1))
            if self.m_LastPos and (pos.x == self.m_LastPos.x) and (pos.y == self.m_LastPos.y) then return end
            self.m_VertexCount = self.m_VertexCount + 1
            self.m_LineRender:SetVertexCount(self.m_VertexCount)
            self.m_LineRender:SetPosition(self.m_VertexCount - 1, pos)
            self.m_WritingBrushRoot.transform.position = Vector3(pos.x,pos.y,2.2)
            self.m_LastPos = pos
        end
    end
    if Input.GetMouseButtonUp(0) then
        if not self.m_IsDrawEnd then
            CUIManager.CloseUI(CLuaUIResources.PanZhuangWnd)
        else
            self:OnDrawEnd()
            self.m_IsDrawEnd = true
        end

    end
end

function LuaPanZhuangWnd:OnDisable()
    if not self.m_IsStartDraw then
        g_MessageMgr:ShowMessage("PanZhuang_Task_Failed")
    elseif not self.m_IsDrawEnd then
        g_MessageMgr:ShowMessage("PanZhuang_Task_IncompletenessWriting")
    elseif self.m_IsDrawEnd and (not self.m_IsTaskFinished) then
        g_MessageMgr:ShowMessage("PanZhuang_Task_IncompletenessWriting")
    end
    self:CancelCountDownTick()
    self:CancelDrawLineTick()
    self:CancelCloseWndTick()
    self:CancelPlayEffectTick()
    if self.m_IsTaskFinished then
        self:FinishTask()
    end
    self.m_Camera.targetTexture = nil
    self.m_MainTexture.mainTexture = nil
    if self.m_RenderTexture then
        RenderTexture.ReleaseTemporary(self.m_RenderTexture)
        self.m_RenderTexture = nil
    end
end

function LuaPanZhuangWnd:PanZhuang(countDownTime, effectType, drawTime)

    self.m_EffetType = effectType
    self.m_DrawTime = drawTime * 1000
    self.m_CountDownTime = tonumber(countDownTime)
    self:ShowLabel(self.m_CountDownTime)

    self:CancelCountDownTick()
    self.m_CountDownTick = RegisterTick(function ()
        self.m_CountDownTime = self.m_CountDownTime - 1
        if self.m_CountDownTime <= 0 then
            CUIManager.CloseUI(CLuaUIResources.PanZhuangWnd)
        else
            self:ShowLabel(self.m_CountDownTime)
        end
    end, 1000)
    self:InitLineRender()

    self.m_IsInit = true
end

function LuaPanZhuangWnd:CancelCountDownTick()
    if self.m_CountDownTick then
        UnRegisterTick(self.m_CountDownTick)
        self.m_CountDownTick = nil
    end
end

function LuaPanZhuangWnd:CancelDrawLineTick()
    if self.m_DrawLineTick then
        UnRegisterTick(self.m_DrawLineTick)
        self.m_DrawLineTick = nil
    end
end

function LuaPanZhuangWnd:CancelCloseWndTick()
    if self.m_CloseWndTick then
        UnRegisterTick(self.m_CloseWndTick)
        self.m_CloseWndTick = nil
    end
end

function LuaPanZhuangWnd:CancelPlayEffectTick()
    if self.m_PlayEffectTick then
        UnRegisterTick(self.m_PlayEffectTick)
        self.m_PlayEffectTick = nil
    end
end

function LuaPanZhuangWnd:InitLineRender()
    self.m_LineRender.material:SetColor("_Color", Color.red)
    self.m_LineRender:SetColors(Color.red, Color.red);
    self.m_LineRender:SetWidth(0.03, 0.03)
    self.m_LineRender:SetVertexCount(0)
    self.m_LineRender.gameObject:SetActive(true)
end

function LuaPanZhuangWnd:ShowLabel(second)
    local text = g_MessageMgr:FormatMessage("MainPlot_WriteWord_InTimeLimit", tonumber(second))
    self.m_Label.text =  LocalString.StrH2V(text,true)
end

function LuaPanZhuangWnd:OnDrawEnd()
    if self.m_VertexCount > 0 then
        self:CancelDrawLineTick()
        self:CancelCountDownTick()
        self.m_Label.gameObject:SetActive(false)
        self.m_WritingBrushRoot.gameObject:SetActive(false)
        self.m_MainTexture.gameObject:SetActive(false)
        self:ShowEffect()
    end
end

function LuaPanZhuangWnd:Close(closeWndTime)
    self:CancelCloseWndTick()
    self.m_CloseWndTick = RegisterTickOnce(function ()
        CUIManager.CloseUI(CLuaUIResources.PanZhuangWnd)
    end,closeWndTime)
end

function LuaPanZhuangWnd:ShowEffect()
    self.m_IsTaskFinished = true
    local closeWndTime = ZhuJueJuQing_Setting.GetData().PanZhuangData[self.m_EffetType * 2 - 1] * 1000
    if self.m_EffetType == 1 then
        self.m_EffectTexture1:SetActive(true)
        self:Close(closeWndTime)
    elseif self.m_EffetType == 2 then
        self.m_EffectTexture2:SetActive(true)
        self:Close(closeWndTime)
    else
        self:CancelPlayEffectTick()
        self.m_Effect3:LoadFx("fx/ui/prefab/UI_panzi_fx01.prefab")
        self.m_PlayEffectTick = RegisterTickOnce(function ()
            self.m_RedBg:SetActive(true)
            self:Close(closeWndTime)
        end,7100)
    end
end

function LuaPanZhuangWnd:FinishTask()
    local empty = CreateFromClass(MakeGenericClass(List, SystemObject))
    Gac2Gas.FinishClientTaskEvent(CLuaTaskMgr.m_PanZhuangWnd_TaskId,"PanZhuang"..tostring(CLuaTaskMgr.m_PanZhuangWnd_EffectType),MsgPackImpl.pack(empty))
end



