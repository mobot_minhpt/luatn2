local LocalString = import "LocalString"
local Vector3 = import "UnityEngine.Vector3"
local CIMMgr = import "L10.Game.CIMMgr"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local CUIManager = import "L10.UI.CUIManager"
local Constants = import "L10.Game.Constants"
local PlayerInfoMgrAlignType = "CPlayerInfoMgr+AlignType"
local CPopupMenuInfoMgrAlignType = import "L10.UI.CPopupMenuInfoMgr+AlignType"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local NGUIMath = import "NGUIMath"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"

EnumCheckboxPopupMenuAlginType = {
	Default = 0,
	Left = 1,
	Right = 2,
	Top = 3,
	Bottom = 4,
}

g_PopupMenuMgr = class()

g_PopupMenuMgr.m_NeedQueryCrossServerFriendPopupMenu = true
g_PopupMenuMgr.m_CrossServerFriendPopupMenuItems = {}
g_PopupMenuMgr.m_CrossServerFriendInfo = {}
g_PopupMenuMgr.m_CheckboxPopupMenuInfo = nil


function g_PopupMenuMgr:SetCrossServerFriendInfo(playerId, worldPos, alignType, playerName, level, xianfanstatus, cls, gender, serverName, guildName, signature, expression, expressionTxt, frame, lianghaoId, hideLiangHao, lianghaoExpireTime)
	self.m_CrossServerFriendInfo = {}
	self.m_CrossServerFriendInfo.playerId = playerId

	local popupMenuAlignType = CPopupMenuInfoMgrAlignType.Center
	if alignType == PlayerInfoMgrAlignType.Left then popupMenuAlignType = CPopupMenuInfoMgrAlignType.Left
	elseif alignType == PlayerInfoMgrAlignType.Right then popupMenuAlignType = CPopupMenuInfoMgrAlignType.Right
		elseif alignType == PlayerInfoMgrAlignType.Bottom then popupMenuAlignType = CPopupMenuInfoMgrAlignType.Bottom
			elseif alignType == PlayerInfoMgrAlignType.Top then popupMenuAlignType = CPopupMenuInfoMgrAlignType.Top end
	self.m_CrossServerFriendInfo.alignType = popupMenuAlignType

	--这两章清原版是屏幕居中，没有用worldpos，和翻译成lua前的行为保持一致
	self.m_CrossServerFriendInfo.targetWorldPos = Vector3.zero -- worldPos
	self.m_CrossServerFriendInfo.targetWidth = 1
	self.m_CrossServerFriendInfo.targetHeight = 1

	self.m_CrossServerFriendInfo.playerName = playerName
	self.m_CrossServerFriendInfo.level = level
	self.m_CrossServerFriendInfo.xianfanstatus = xianfanstatus
	self.m_CrossServerFriendInfo.cls = cls
	self.m_CrossServerFriendInfo.gender = gender
	self.m_CrossServerFriendInfo.serverName = serverName
	self.m_CrossServerFriendInfo.guildName = guildName
	self.m_CrossServerFriendInfo.signature = signature
	self.m_CrossServerFriendInfo.expression = expression
	self.m_CrossServerFriendInfo.expressionTxt = expressionTxt
	self.m_CrossServerFriendInfo.frame = frame

	self.m_CrossServerFriendInfo.lianghaoId = lianghaoId
	self.m_CrossServerFriendInfo.hideLiangHao = hideLiangHao
	self.m_CrossServerFriendInfo.lianghaoExpireTime = lianghaoExpireTime
	self.m_CrossServerFriendInfo.lianghaoIcon = nil --盘子说这里不显示任何信息

	self.m_CrossServerFriendInfo.portraitName = nil
end

function g_PopupMenuMgr:_ShowCrossServerFriendPopupMenu()
	if not self.m_CrossServerFriendInfo then return end

	local playerId = self.m_CrossServerFriendInfo.playerId
	local lianghaoId = self.m_CrossServerFriendInfo.lianghaoId or 0
	local lianghaoExpireTime = self.m_CrossServerFriendInfo.lianghaoExpireTime

	self.m_CrossServerFriendPopupMenuItems = {}
	local lookatFightItem = {text=LocalString.GetString("查看战斗力"), action = self.CrossServerFriend_LookAtFightAction, imageName=nil}
	table.insert(self.m_CrossServerFriendPopupMenuItems, lookatFightItem)

	if CPersonalSpaceMgr.OpenPersonalSpaceEntrance then
		local openPersonalSpaceItem = {text=LocalString.GetString("梦岛"), action = self.CrossServerFriend_OpenPersonalSpaceAction, imageName=Constants.FriendPopupMenuPersonalSpaceImageName}
		table.insert(self.m_CrossServerFriendPopupMenuItems, openPersonalSpaceItem)
	end
	local lookAtInfoItem = {text=LocalString.GetString("查看信息"), action = self.CrossServerFriend_LookAtInfoAction, imageName=nil}
	table.insert(self.m_CrossServerFriendPopupMenuItems, lookAtInfoItem)

	if (not CIMMgr.Inst:IsMyFriend(playerId)) and lianghaoId>0 and CServerTimeMgr.Inst.timeStamp<lianghaoExpireTime then
		local addFriendItem = {text=LocalString.GetString("添加好友"), action = self.CrossServerFriend_AddFriendAction, imageName=nil}
		table.insert(self.m_CrossServerFriendPopupMenuItems, addFriendItem)
	end

	local reportItem = {text=LocalString.GetString("举报"), action = self.CrossServerFriend_ReportAction, imageName=nil}
	table.insert(self.m_CrossServerFriendPopupMenuItems, reportItem)

	CUIManager.ShowUI("CrossServerFriendPopupMenu")
end

function g_PopupMenuMgr:ShowCrossServerFriendPopupMenu(playerId, worldPos, alignType, playerName, level, xianfanstatus, cls, gender, serverName, guildName, signature, expression, expressionTxt, frame, lianghaoId, hideLiangHao, lianghaoExpireTime)
	self:SetCrossServerFriendInfo(playerId, worldPos, alignType, playerName, level, xianfanstatus, cls, gender, serverName, guildName, signature, expression, expressionTxt, frame, lianghaoId, hideLiangHao, lianghaoExpireTime)
	self:_ShowCrossServerFriendPopupMenu()
end

function g_PopupMenuMgr:ShowCrossServerFriendPopupMenuAfterQuery(playerId, level, portraitName, expressionTxt, profileInfo, displayName, xiuweiGrade, context, online, serverGroupId, isInXianShenStatus, isSuccess)
	if not self.m_CrossServerFriendInfo then return end

	self.m_CrossServerFriendInfo.playerId = playerId
	self.m_CrossServerFriendInfo.level = level
	self.m_CrossServerFriendInfo.expressionTxt = expressionTxt
	self.m_CrossServerFriendInfo.playerName = displayName
	self.m_CrossServerFriendInfo.xianfanstatus = isInXianShenStatus and 1 or 0
	if profileInfo then
		self.m_CrossServerFriendInfo.frame = profileInfo.Frame
		self.m_CrossServerFriendInfo.lianghaoId = profileInfo.LiangHaoId
		self.m_CrossServerFriendInfo.hideLiangHao = profileInfo.HideLiangHaoIcon
		self.m_CrossServerFriendInfo.lianghaoExpireTime = profileInfo.LiangHaoExpiredTime
		self.m_CrossServerFriendInfo.lianghaoIcon = nil --盘子说这里不显示任何信息
	end

	self.m_CrossServerFriendInfo.portraitName = portraitName

	if isSuccess then
		self:_ShowCrossServerFriendPopupMenu()
	else
		self.m_CrossServerFriendPopupMenuItems = {}
		CUIManager.ShowUI("CrossServerFriendPopupMenu")
	end
end

function g_PopupMenuMgr.CrossServerFriend_LookAtInfoAction(playerId, playerName)
	if not CPlayerInfoMgr.CheckCanLookAtInfo(playerId) then
		return
	end
	CPlayerInfoMgr.ShowPlayerInfoWnd(playerId, playerName)
end

function g_PopupMenuMgr.CrossServerFriend_OpenPersonalSpaceAction(playerId, playerName)
	CPersonalSpaceMgr.Inst:OpenPersonalSpaceWnd(playerId, 0)
end

function g_PopupMenuMgr.CrossServerFriend_LookAtFightAction(playerId, playerName)
	CPersonalSpaceMgr.Inst:GetPlayerCapacity(playerId, playerName)
end

function g_PopupMenuMgr.CrossServerFriend_AddFriendAction(playerId, playerName)
	CIMMgr.Inst:RequestAddFriend(playerId, "MengDaoLiangHao") --坤少提供的reason
end

function g_PopupMenuMgr.CrossServerFriend_ReportAction(playerId, playerName)
	LuaPlayerReportMgr:ShowDefaultReportWnd(playerId, playerName)
end


----------
-- 通用Checkbox menu菜单
----------

-- title 可选标题 传入nil或者空字符串时不显示标题部分
-- itemTbl 选项信息，每项包含内容为 text, action, defaultSelected
-- menuWidth可以不填，取默认值
function g_PopupMenuMgr:ShowCheckboxPopupMenuByTransform(title, itemTbl, targetTransform, alignType, menuWidth)
	local pos = Vector3.zero
	if targetTransform and CommonDefs.GetComponentInChildren_Component_Type(targetTransform, typeof(UIWidget)) then
		local  bounds = NGUIMath.CalculateAbsoluteWidgetBounds(targetTransform)
		if alignType == EnumCheckboxPopupMenuAlginType.Left then
			pos.x = bounds.min.x
			pos.y = bounds.center.y
		elseif alignType == EnumCheckboxPopupMenuAlginType.Right then
			pos.x = bounds.max.x
			pos.y = bounds.center.y
		elseif alignType == EnumCheckboxPopupMenuAlginType.Top then
			pos.x = bounds.center.x
			pos.y = bounds.max.y
		elseif alignType == EnumCheckboxPopupMenuAlginType.Bottom then
			pos.x = bounds.center.x
			pos.y = bounds.min.y
		end
	end
	self:ShowCheckboxPopupMenuByPosition(title, itemTbl, pos, alignType, menuWidth)
end
function g_PopupMenuMgr:ShowCheckboxPopupMenuByPosition(title, itemTbl, nguiWorldPos, alignType, menuWidth)
	self.m_CheckboxPopupMenuInfo = {}
	self.m_CheckboxPopupMenuInfo.title = title
	self.m_CheckboxPopupMenuInfo.itemTbl = itemTbl
	self.m_CheckboxPopupMenuInfo.nguiWorldPos = nguiWorldPos
	self.m_CheckboxPopupMenuInfo.alignType = alignType
	local realWidth = menuWidth and menuWidth or 494
	if realWidth<494 then realWidth = 494 end
	self.m_CheckboxPopupMenuInfo.menuWidth = realWidth

	CUIManager.ShowUI("CheckboxPopupMenu")
end


----------
-- 通用menu菜单
----------

function g_PopupMenuMgr:CreateOneDefaultPopupMenuItem(text, action, showAlert)
    return PopupMenuItemData(text, DelegateFactory.Action_int(function(index)
                if action then action() end
            end),false, nil, EnumPopupMenuItemStyle.Default)
end

function g_PopupMenuMgr:CreateOneOrangePopupMenuItem(text, action, showAlert)
    return PopupMenuItemData(text, DelegateFactory.Action_int(function(index)
        if action then action() end
    end),false, nil, EnumPopupMenuItemStyle.Orange)
end

function g_PopupMenuMgr:ShowPopupMenuOnTop(menuItemTbl, targetTransform)
    CPopupMenuInfoMgr.ShowPopupMenu(Table2Array(menuItemTbl,MakeArrayClass(PopupMenuItemData)), targetTransform, CPopupMenuInfoMgrAlignType.Top,1,nil,600,true,266)
end

function g_PopupMenuMgr:ShowPopupMenuOnBottom(menuItemTbl, targetTransform)
    CPopupMenuInfoMgr.ShowPopupMenu(Table2Array(menuItemTbl,MakeArrayClass(PopupMenuItemData)), targetTransform, CPopupMenuInfoMgrAlignType.Bottom,1,nil,600,true,266)
end

function g_PopupMenuMgr:ShowPopupMenuOnLeft(menuItemTbl, targetTransform)
    CPopupMenuInfoMgr.ShowPopupMenu(Table2Array(menuItemTbl,MakeArrayClass(PopupMenuItemData)), targetTransform, CPopupMenuInfoMgrAlignType.Left,1,nil,600,true,266)
end

function g_PopupMenuMgr:ShowPopupMenuOnRight(menuItemTbl, targetTransform)
    CPopupMenuInfoMgr.ShowPopupMenu(Table2Array(menuItemTbl,MakeArrayClass(PopupMenuItemData)), targetTransform, CPopupMenuInfoMgrAlignType.Right,1,nil,600,true,266)
end