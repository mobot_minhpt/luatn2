local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType2 = import "L10.UI.CTooltip+AlignType"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"

LuaWuLiangCompanionAttributeFosterRoot = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaWuLiangCompanionAttributeFosterRoot, "ItemCell", "ItemCell", GameObject)
RegistChildComponent(LuaWuLiangCompanionAttributeFosterRoot, "LevelUpBtn", "LevelUpBtn", GameObject)
RegistChildComponent(LuaWuLiangCompanionAttributeFosterRoot, "ResetBtn", "ResetBtn", GameObject)
RegistChildComponent(LuaWuLiangCompanionAttributeFosterRoot, "ResNumLabel", "ResNumLabel", UILabel)
RegistChildComponent(LuaWuLiangCompanionAttributeFosterRoot, "AttributeAfter", "AttributeAfter", GameObject)
RegistChildComponent(LuaWuLiangCompanionAttributeFosterRoot, "AttributeBefore", "AttributeBefore", GameObject)
RegistChildComponent(LuaWuLiangCompanionAttributeFosterRoot, "AttributeAfterTable", "AttributeAfterTable", GameObject)
RegistChildComponent(LuaWuLiangCompanionAttributeFosterRoot, "AttributeBeforeTable", "AttributeBeforeTable", GameObject)
RegistChildComponent(LuaWuLiangCompanionAttributeFosterRoot, "AlreadyFullLevel", "AlreadyFullLevel", GameObject)
RegistChildComponent(LuaWuLiangCompanionAttributeFosterRoot, "GetLabel", "GetLabel", GameObject)

--@endregion RegistChildComponent end

function LuaWuLiangCompanionAttributeFosterRoot:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaWuLiangCompanionAttributeFosterRoot:InitShow()
	self.huoBanId = LuaWuLiangMgr.huobanSelectId
    self.huoBanInfo = LuaWuLiangMgr.huobanData.HuoBan[LuaWuLiangMgr.huobanSelectId]

	local huoBanDesData = XinBaiLianDong_WuLiangHuoBan.GetData(self.huoBanId)
	local huoBanData = self.huoBanInfo

    local setting = XinBaiLianDong_Setting.GetData()
	-- 显示属性
    local level = self.huoBanInfo.InheritLevel or 0
    --LuaWuLiangMgr:ShowAttr(self.AttributeBeforeTable, level, self.huoBanId)

    local hasFullLevel = level >= setting.WuLiangShenJing_HuoBanInheritMaxLv

	-- 升级显示
	self.AttributeAfter.gameObject:SetActive(not hasFullLevel)
	self.ItemCell.gameObject:SetActive(not hasFullLevel)
	self.LevelUpBtn.gameObject:SetActive(not hasFullLevel)
	self.ResNumLabel.gameObject:SetActive(not hasFullLevel)

	local curData = XinBaiLianDong_WuLiangInherit.GetData(level)
	local nextData = XinBaiLianDong_WuLiangInherit.GetData(level+1)
	-- 当前和之后的
	if curData then
		local curDesLabel = self.AttributeBefore.transform:Find("TitleLabel"):GetComponent(typeof(UILabel))
		curDesLabel.text = SafeStringFormat3(LocalString.GetString("当前属性（继承%s%%）"), 
			tostring(math.floor((curData.PropertyCoefficient + huoBanDesData.PropertyCoefficient + 0.0001)*100)))
	end

	if nextData and not hasFullLevel then
		local nextDesLabel = self.AttributeAfter.transform:Find("TitleLabel"):GetComponent(typeof(UILabel))
		nextDesLabel.text = SafeStringFormat3(LocalString.GetString("升级后属性（继承%s%%）"), 
			tostring(math.floor((nextData.PropertyCoefficient+ huoBanDesData.PropertyCoefficient + 0.0001)*100)))
	end

	-- 满级显示
	self.AlreadyFullLevel.gameObject:SetActive(hasFullLevel)

    local costData = XinBaiLianDong_WuLiangInherit.GetData(level+1)

	if self.queryHuoBanId == nil or self.queryLevel == nil or self.queryHuoBanId ~= self.huoBanId or self.queryLevel ~= level then
		-- 请求
		self.queryHuoBanId = self.huoBanId
		self.queryLevel = level
		Gac2Gas.WuLiangShenJing_QueryHuoBanProp(self.huoBanId, true)
	end
	
	if not hasFullLevel then     
		local itemId = setting.WuLiangShenJing_HuoBanInheritScore

        -- 下一级别信息
        --LuaWuLiangMgr:ShowAttr(self.AttributeAfterTable, level+1, self.huoBanId)

		local needScore = costData.NeedScore
		local currentScore = LuaWuLiangMgr.huobanData.InheritScore

		local notEnough = needScore > currentScore
		if notEnough then
			self.ResNumLabel.text =  SafeStringFormat3(LocalString.GetString("[FFFF00]%s[-]/%s"), tostring(currentScore), tostring(needScore))
		else
			self.ResNumLabel.text =  SafeStringFormat3(LocalString.GetString("%s/%s"), tostring(currentScore), tostring(needScore)) 
		end

		self.GetLabel.gameObject:SetActive(notEnough)

		-- 物品图标
		local itemData = Item_Item.GetData(itemId)
		local icon = self.ItemCell.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
		icon:LoadMaterial(itemData.Icon)

		-- 物品点击显示
		UIEventListener.Get(self.ItemCell.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
			if notEnough then
				-- 获取方式
				CItemAccessListMgr.Inst:ShowItemAccessInfo(itemId, false, nil, AlignType2.Right)
			else
				CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType.Default, 0, 0, 0, 0)
			end
		end)

		-- 升级
		UIEventListener.Get(self.LevelUpBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
			if notEnough then
				g_MessageMgr:ShowMessage("WuLiang_Inherit_Score_Not_Enough")
			else
				-- 请求
				Gac2Gas.WuLiangShenJing_UpgradeInherit(self.huoBanId)
			end
		end)
	end

	-- 重置
	UIEventListener.Get(self.ResetBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
		if level == 0 then
			g_MessageMgr:ShowMessage("WuLiang_Inherit_Not_Upgraded")
		else
			local str = g_MessageMgr:FormatMessage("WuLiang_Inherit_Reset_Confirm", curData.ResetMoney)

			g_MessageMgr:ShowOkCancelMessage(str, function()
				Gac2Gas.WuLiangShenJing_ResetHuoBanInherit(self.huoBanId)
			end, nil, nil, nil, false)
		end
	end)
end

function LuaWuLiangCompanionAttributeFosterRoot:OnShowAttr(huobanId, data, dataNextLevel)
	if huobanId == self.huoBanId then
		LuaWuLiangMgr:ShowAttrNew(self.AttributeBeforeTable, huobanId, data)
	
		if dataNextLevel then
			LuaWuLiangMgr:ShowAttrNew(self.AttributeAfterTable, huobanId, dataNextLevel)
		end
	end
end

function LuaWuLiangCompanionAttributeFosterRoot:OnEnable()
	self.queryHuoBanId = 0
	self.queryLevel = 0
	self:InitShow()
    g_ScriptEvent:AddListener("LuaWuLiangCompanionInfoWnd_SelectIndex", self, "InitShow")
    g_ScriptEvent:AddListener("WuLiangShenJing_QueryHuoBanPropResultWithNextLv", self, "OnShowAttr")
end

function LuaWuLiangCompanionAttributeFosterRoot:OnDisable()
    g_ScriptEvent:RemoveListener("LuaWuLiangCompanionInfoWnd_SelectIndex", self, "InitShow")
    g_ScriptEvent:RemoveListener("WuLiangShenJing_QueryHuoBanPropResultWithNextLv", self, "OnShowAttr")
end


--@region UIEvent

--@endregion UIEvent

