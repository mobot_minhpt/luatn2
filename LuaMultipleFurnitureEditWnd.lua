local UISprite = import "UISprite"

local VirtualJoyStick = import "L10.UI.VirtualJoyStick"

local QnNewSlider = import "L10.UI.QnNewSlider"

local QnTableItem = import "L10.UI.QnTableItem"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local UITexture = import "UITexture"
local EUIModuleGroup = import "L10.UI.CUIManager+EUIModuleGroup"
local Input = import "UnityEngine.Input"
local CUIResources = import "L10.UI.CUIResources"
local CClientFurnitureMgr = import "L10.Game.CClientFurnitureMgr"
local CMainCamera = import "L10.Engine.CMainCamera"
local Physics               = import "UnityEngine.Physics"
local CClientFurniture=import "L10.Game.CClientFurniture"
local LayerDefine = import "L10.Engine.LayerDefine"
local CFurnitureSliderMgr = import "L10.UI.CFurnitureSliderMgr"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local UICamera = import "UICamera"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local CRenderScene = import "L10.Engine.Scene.CRenderScene"
local CJoystickWnd = import "L10.UI.CJoystickWnd"
local GestureRecognizer = import "L10.Engine.GestureRecognizer"
local Vector2 = import "UnityEngine.Vector2"
local GestureType = import "L10.Engine.GestureType"
local NormalCamera=import "L10.Engine.CameraControl.NormalCamera"
local Quaternion = import "UnityEngine.Quaternion"
local Vector3 = import "UnityEngine.Vector3"
local CScene = import "L10.Game.CScene" 
local CWater = import "L10.Engine.CWater"

LuaMultipleFurnitureEditWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaMultipleFurnitureEditWnd, "SelectedCountLable", "SelectedCountLable", UILabel)
RegistChildComponent(LuaMultipleFurnitureEditWnd, "TipLabel", "TipLabel", UILabel)
RegistChildComponent(LuaMultipleFurnitureEditWnd, "ChooseListBtn", "ChooseListBtn", GameObject)
RegistChildComponent(LuaMultipleFurnitureEditWnd, "BoxAddBtn", "BoxAddBtn", QnSelectableButton)
RegistChildComponent(LuaMultipleFurnitureEditWnd, "BoxRemoveBtn", "BoxRemoveBtn", QnSelectableButton)
RegistChildComponent(LuaMultipleFurnitureEditWnd, "CameraBtn", "CameraBtn", QnSelectableButton)
RegistChildComponent(LuaMultipleFurnitureEditWnd, "SelectBox", "SelectBox", UITexture)
RegistChildComponent(LuaMultipleFurnitureEditWnd, "ViewButtons", "ViewButtons", GameObject)
RegistChildComponent(LuaMultipleFurnitureEditWnd, "VerticalSlider", "VerticalSlider", QnNewSlider)
RegistChildComponent(LuaMultipleFurnitureEditWnd, "FloatingLabel", "FloatingLabel", UILabel)
RegistChildComponent(LuaMultipleFurnitureEditWnd, "XuanFuView", "XuanFuView", GameObject)
RegistChildComponent(LuaMultipleFurnitureEditWnd, "VirtualJoyStick", "VirtualJoyStick", VirtualJoyStick)
RegistChildComponent(LuaMultipleFurnitureEditWnd, "SelectBoxBg", "SelectBoxBg", UISprite)

--@endregion RegistChildComponent end
RegistClassMember(LuaMultipleFurnitureEditWnd,"m_LeftTabBtnList")
RegistClassMember(LuaMultipleFurnitureEditWnd,"m_ValideTypeDict")
RegistClassMember(LuaMultipleFurnitureEditWnd,"m_CandidateLimitCount")
RegistClassMember(LuaMultipleFurnitureEditWnd,"m_CameraMinY")
RegistClassMember(LuaMultipleFurnitureEditWnd,"m_CameraMaxY")

function LuaMultipleFurnitureEditWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.ChooseListBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnChooseListBtnClick()
	end)

    --@endregion EventBind end
    CUIManager.CloseUI(CUIResources.HousePopupMenu)
    self.m_LeftTabBtnList = {}
    table.insert(self.m_LeftTabBtnList,self.BoxAddBtn)
    table.insert(self.m_LeftTabBtnList,self.BoxRemoveBtn)
    table.insert(self.m_LeftTabBtnList,self.CameraBtn)
    self.BoxAddBtn:SetSelected(true, false)
    self.BoxRemoveBtn:SetSelected(false, false)
    self.CameraBtn:SetSelected(false, false)
    self.ViewButtons:SetActive(false)
    self.m_SelectedLeftMode = 1
    self.m_ValideTypeDict = {}
    self.m_FocusStatus = true
    local setting = Zhuangshiwu_Setting.GetData()
    for i=0,setting.PiLiangCaoZuoType.Length-1,1 do
        self.m_ValideTypeDict[setting.PiLiangCaoZuoType[i]] = true
    end
    self.m_CandidateLimitCount = setting.PiLiangCaoZuoLimit
    if CLuaZswTemplateMgr.IsInCustomMode then
        self.m_CandidateLimitCount = CLuaZswTemplateMgr.CurCustomComponentLimit
    end

    CLuaHouseMgr.m_SelectedFurnitures = {}
    UIEventListener.Get(self.BoxAddBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnSelectButtonClick(go)
    end)
    UIEventListener.Get(self.BoxRemoveBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnSelectButtonClick(go)
    end)
    UIEventListener.Get(self.CameraBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnSelectButtonClick(go)
    end)
    self.XuanFuView:SetActive(false)

    --Camera
    self.m_CameraMoveSpeed = 1
    self.m_ViewEditButtons = {
        [1] = self.ViewButtons.transform:Find("Left").gameObject,
        [2] = self.ViewButtons.transform:Find("Right").gameObject,
        [3] = self.ViewButtons.transform:Find("Up").gameObject,
        [4] = self.ViewButtons.transform:Find("Down").gameObject,
        [5] = self.ViewButtons.transform:Find("Reset").gameObject,
    }
    self.m_ViewEditButtonsIndex = {}
    for i,v in ipairs(self.m_ViewEditButtons) do
        self.m_ViewEditButtonsIndex[v.name] = i
    end
    for i=1,4 do
        UIEventListener.Get(self.m_ViewEditButtons[i]).onPress = DelegateFactory.BoolDelegate(function(go,state)
            local idx = self.m_ViewEditButtonsIndex[go.name]
            self.m_ViewEditButtonIndex = idx
            self.m_ViewEditIsPressed = state
        end)
    end
    UIEventListener.Get(self.m_ViewEditButtons[5]).onClick = DelegateFactory.VoidDelegate(function(go)
        self:ResetCamera()
    end)
    --VirtualJoyStick
    local OnDragJoyStick = function(dir)
		self:OnDragJoyStick(dir)
	end
	local OnDragFinished = function()
		self:OnDragFinished()
	end
	CommonDefs.AddVSOnDraggingListener(self.VirtualJoyStick.gameObject,DelegateFactory.Action_Vector3(OnDragJoyStick),true)
	CommonDefs.AddVSOnDragFinishListener(self.VirtualJoyStick.gameObject,DelegateFactory.Action(OnDragFinished),true)

    local heightInterval = Zhuangshiwu_Setting.GetData().PlantOffsetInterval
    self.m_MaxHeightValue = math.floor((heightInterval[1] - heightInterval[0]) / (8 - heightInterval[0]) * 100)
    CommonDefs.AddOnClickListener(self.VerticalSlider.transform:Find("plus_icon").gameObject, 
            DelegateFactory.Action_GameObject(function()
                self.VerticalSlider.m_Value = self.VerticalSlider.m_Value + 1/self.m_MaxHeightValue
            end), 
        false)

    CommonDefs.AddOnClickListener(self.VerticalSlider.transform:Find("min_icon").gameObject, 
            DelegateFactory.Action_GameObject(function()
                self.VerticalSlider.m_Value = self.VerticalSlider.m_Value - 1/self.m_MaxHeightValue
            end), 
        false)

    --Clear last select
    if CClientFurnitureMgr.Inst.CurFurniture then
        CLuaHouseMgr.RequestPlaceBackFurniture(CClientFurnitureMgr.Inst.CurFurniture)
    end
    self.lastRightPos = Vector3.zero
    self.m_CameraMinY = Zhuangshiwu_Setting.GetData().HouseCameraMinY
    self.m_CameraMaxY = Zhuangshiwu_Setting.GetData().HouseCameraMaxY
end

function LuaMultipleFurnitureEditWnd:OnEnable()
    local excepts = {"MiddleNoticeCenter"}
	local List_String = MakeGenericClass(List,cs_string)
    --CUIManager.ShowUIByChangeLayer(CUIResources.Joystick,false,false)

	CUIManager.HideUIGroupByChangeLayer(EUIModuleGroup.EGameUI, Table2List(excepts, List_String), false, true)
    CUIManager.HideUIGroupByChangeLayer(EUIModuleGroup.EBgGameUI, Table2List(excepts, List_String), false, true)

    CUIManager.CloseUI(CUIResources.FurnitureRightTopWnd)
    CUIManager.CloseUI(CUIResources.FurnitureSmallTypeWnd)
    CUIManager.CloseUI(CUIResources.FurnitureBigTypeWnd)
    CUIManager.CloseUI(CUIResources.UseZuoanWnd)
    CUIManager.CloseUI(CUIResources.FurnitureSlider)

    g_ScriptEvent:AddListener("UpdateXuanFuSliderActive", self, "OnUpdateXuanFuSliderActive")
    g_ScriptEvent:AddListener("UpdateMultipleFurnitureChoiceFromListWnd", self, "OnUpdateMultipleFurnitureChoiceFromListWnd")
    g_ScriptEvent:AddListener("UpdateMultipleFurnitureChoice", self, "OnUpdateMultipleFurnitureChoiceFromListWnd")
    g_ScriptEvent:AddListener("AppFocusChange", self, "OnApplicationFocus")
    g_ScriptEvent:AddListener("MouseScrollWheel",self,"OnMouseScrollWheel")

    GestureRecognizer.Inst:AddNeedUIPinchWnd(CLuaUIResources.MultipleFurnitureEditWnd)
    self.onPinchInDelegate = DelegateFactory.Action_GenericGesture(function(gesture) self:OnPinchIn(gesture) end)
    self.onPinchOutDelegate = DelegateFactory.Action_GenericGesture(function(gesture) self:OnPinchOut(gesture) end)
    GestureRecognizer.AddGestureListener(GestureType.OnUIPinchIn, self.onPinchInDelegate)
    GestureRecognizer.AddGestureListener(GestureType.OnUIPinchOut, self.onPinchOutDelegate)
end

function LuaMultipleFurnitureEditWnd:OnDisable()
    local excepts = {"MiddleNoticeCenter"}
	local List_String = MakeGenericClass(List,cs_string)
	CUIManager.ShowUIGroupByChangeLayer(EUIModuleGroup.EGameUI, Table2List(excepts, List_String), false, true)
    CUIManager.ShowUIGroupByChangeLayer(EUIModuleGroup.EBgGameUI, Table2List(excepts, List_String), false, true)

    if  CClientMainPlayer.Inst then
        CUIManager.ShowUI(CUIResources.FurnitureRightTopWnd)
        CUIManager.ShowUI(CUIResources.FurnitureSmallTypeWnd)
        CUIManager.ShowUI(CUIResources.FurnitureBigTypeWnd)
    end
    
    CClientFurnitureMgr.Inst.FurnitureEditCanRotate = false
    self:ResetCamera()

    g_ScriptEvent:RemoveListener("UpdateXuanFuSliderActive", self, "OnUpdateXuanFuSliderActive")
    g_ScriptEvent:RemoveListener("UpdateMultipleFurnitureChoiceFromListWnd", self, "OnUpdateMultipleFurnitureChoiceFromListWnd")
    g_ScriptEvent:RemoveListener("UpdateMultipleFurnitureChoice", self, "OnUpdateMultipleFurnitureChoiceFromListWnd")
    g_ScriptEvent:RemoveListener("AppFocusChange", self, "OnApplicationFocus")
    g_ScriptEvent:RemoveListener("MouseScrollWheel",self,"OnMouseScrollWheel")
    GestureRecognizer.RemoveGestureListener(GestureType.OnUIPinchIn, self.onPinchInDelegate)
    GestureRecognizer.RemoveGestureListener(GestureType.OnUIPinchOut, self.onPinchOutDelegate)
    --self.m_MultipleRoot:ClearChildren()
    CLuaHouseMgr.PlaceBack_Multiple()

    if CLuaHouseMgr.MultipleRoot and CLuaHouseMgr.MultipleRoot.transform then
        GameObject.Destroy(CLuaHouseMgr.MultipleRoot.gameObject)
    end
    CLuaHouseMgr.MultipleRoot = nil
    CUIManager.CloseUI(CLuaUIResources.MultipleChooseListWnd)
    CLuaZswTemplateMgr.IsInCustomMode = false
    CLuaZswTemplateMgr.IsCustomPlacing = false
    self.m_CustomInited = false
end

function LuaMultipleFurnitureEditWnd:Init()
    self.SelectedCountLable.text = SafeStringFormat3(LocalString.GetString("已选中%d/%d件装饰物"),0,self.m_CandidateLimitCount)

    self.m_MultipleRoot = CLuaHouseMgr.GetMultipleRoot()
    self.m_MultipleRoot:ClearChildren()

    --self:OnClearAllSelected()

    self.VerticalSlider.OnValueChanged = DelegateFactory.Action_float(function(v)
        self:OnVerticalValueChanged(v)
    end)
    
    if CJoystickWnd.Instance then
        CJoystickWnd.Instance.gameObject:SetActive(true)
        CJoystickWnd.Instance:SetMountAndCaptureButtonVisible(true)
    end
end

function LuaMultipleFurnitureEditWnd:InitForCustomTemplatePlacing()
    self.m_CustomInited = true
    local templateData = CLuaZswTemplateMgr.GetCustomTemplateByIndex(CLuaZswTemplateMgr.CustomFillingData.templateIdx)
    local components = templateData.components
    local rootPos = nil
    local direction = 0
    for i=1,#CLuaZswTemplateMgr.CustomFillingData.slots,1 do
        local zswId = CLuaZswTemplateMgr.CustomFillingData.slots[i]
        local childId = SafeStringFormat3("%d%003d",zswId,i)
        childId = tonumber(childId)

        local componentFur = CClientFurnitureMgr.Inst:CreateFurniture(zswId, childId, "")
        if componentFur then
            componentFur.Selected = true
            if not rootPos then
                rootPos = componentFur.Position
                direction = componentFur.RO.Direction
            end
            local component = components[i]
            local offset = component.offset
            local rotation = component.rotation
            --print(rotation,type(rotation))
            local scale = component.scale
            local vec3 = Vector3(offset.x/100,offset.y/100,offset.z/100)
            
            local quaternion = Quaternion.AngleAxis(direction, Vector3.up)
            vec3 = CommonDefs.op_Multiply_Quaternion_Vector3(quaternion, vec3)
            local pos = CommonDefs.op_Addition_Vector3_Vector3(rootPos, vec3)
            componentFur.Position= pos
            local childDirection = direction + rotation.y
            componentFur.RO.transform.localRotation = Quaternion.Euler(rotation.x, childDirection, rotation.z)
            componentFur.RO.Scale = scale

            CLuaHouseMgr.GetMultipleRoot():AddChild(componentFur)
        end
    end    
    CLuaHouseMgr.ShowMultipleFurnishPopWnd()
    self.BoxAddBtn.gameObject:SetActive(false)
    self.BoxRemoveBtn.gameObject:SetActive(false)
    self.transform:Find("Top").gameObject:SetActive(false)
end

function LuaMultipleFurnitureEditWnd:Update()
    if CLuaZswTemplateMgr.IsCustomPlacing and not self.m_CustomInited then
        self:InitForCustomTemplatePlacing()
    end
    --相机
    if self.m_SelectedLeftMode == 3 then
        if Input.GetMouseButtonDown(0) then
            if not CUICommonDef.IsChild(self.XuanFuView.transform, UICamera.lastHit.collider.transform) then
                self:OnUpdateXuanFuSliderActive(falses)
            end
            CUIManager.CloseUI(CLuaUIResources.MultipleFurnishPopupMenu)
        end
        if self.m_CameraTrans == nil then
            self.m_CameraTrans = CameraFollow.Inst.transform:Find("camRotate/newParent")
        end
        if self.m_ViewEditIsPressed then
            local pos = self.m_CameraTrans.localPosition
            local worldPos = self.m_CameraTrans.position
            local deltaTime = Time.deltaTime
            local val = deltaTime*self.m_CameraMoveSpeed
            if self.m_ViewEditButtonIndex==1 then--right
                pos = Vector3(pos.x-val,pos.y,pos.z)
            elseif self.m_ViewEditButtonIndex==2 then
                pos = Vector3(pos.x+val,pos.y,pos.z)
            elseif self.m_ViewEditButtonIndex==3 then
                if worldPos.y+val <= self.m_CameraMaxY then
                    pos = Vector3(pos.x,pos.y+val,pos.z)
                end              
            elseif self.m_ViewEditButtonIndex==4 then
                local logicHeight = CScene.MainScene:GetLogicHeight(math.floor(self.m_CameraTrans.position.x * 64), math.floor(self.m_CameraTrans.position.z * 64))
                if worldPos.y-val >= math.max(CWater.s_WaterHeight, logicHeight) + self.m_CameraMinY then
                    pos = Vector3(pos.x,pos.y-val,pos.z)
                end
                --pos = Vector3(pos.x,pos.y-val,pos.z)
            end
            self.m_CameraTrans.localPosition = pos
        end
        --相机角度调整
        if (Input.GetMouseButton(1) or Input.touchCount==1) and not self.m_IsDragJoyStick then
                local curpos = Input.mousePosition
                if (self.lastRightPos.x~=0 and self.lastRightPos.y~=0) then
                    local deltaPos = CommonDefs.op_Subtraction_Vector3_Vector3(curpos, self.lastRightPos)                  
                    if (deltaPos.magnitude > 0.1) then
                        CameraFollow.Inst:OnDrag(Vector2(deltaPos.x,deltaPos.y),true)
                    end
                end
                self.lastRightPos = curpos
        else
            self.lastRightPos = Vector3.zero
        end
        local camPos = Vector3(self.m_CameraTrans.position.x, self.m_CameraTrans.position.y, self.m_CameraTrans.position.z)
        if camPos.y > self.m_CameraMaxY then
            camPos.y = self.m_CameraMaxY
            self.m_CameraTrans.position = camPos
            return
        end
        local logicHeight = CScene.MainScene:GetLogicHeight(math.floor(self.m_CameraTrans.position.x * 64), math.floor(self.m_CameraTrans.position.z * 64))
        if camPos.y < math.max(CWater.s_WaterHeight, logicHeight) + self.m_CameraMinY then
            camPos.y = math.max(CWater.s_WaterHeight, logicHeight) + self.m_CameraMinY
            self.m_CameraTrans.position = camPos
            return
        end
        
        return
    end

    --框选 框减
    if (self.m_SelectedLeftMode == 1 or self.m_SelectedLeftMode == 2) and not CLuaZswTemplateMgr.IsCustomPlacing  then
        if not CUICommonDef.IsOverUI or (UICamera.lastHit.collider and
        not CUICommonDef.IsChild(self.transform, UICamera.lastHit.collider.transform)) then
            return
        end

        if Input.GetMouseButtonDown(0) then
            if CUICommonDef.IsChild(self.VirtualJoyStick.transform, UICamera.lastHit.collider.transform) then
                self.m_IsDrawing = false
                return
            end
            if not CUICommonDef.IsChild(self.XuanFuView.transform, UICamera.lastHit.collider.transform) then
                self:OnUpdateXuanFuSliderActive(falses)
            end

            CUIManager.CloseUI(CLuaUIResources.MultipleFurnishPopupMenu)

            if(UICamera.lastHit.collider.transform.name ~="_BgMask_") then
                return
            end

            self.m_StartBoxPos = CUIManager.UIMainCamera:ScreenToWorldPoint(Input.mousePosition)
            self.m_StartBoxPos = self.SelectBox.transform.parent:InverseTransformPoint(self.m_StartBoxPos)
            self.m_StartMousePos = Input.mousePosition
            self.m_IsDrawing = true
            self:StartDrawBox()
            self:OnClickSingleTarget(self.m_StartMousePos)
        end
        if Input.GetMouseButton(0) then
            if CUICommonDef.IsChild(self.VirtualJoyStick.transform, UICamera.lastHit.collider.transform) then
                self.m_IsDrawing = false
                return
            end
            if not self.m_IsDrawing then
                return
            end
            self.m_EndBoxPos = CUIManager.UIMainCamera:ScreenToWorldPoint(Input.mousePosition)
            self.m_EndBoxPos = self.SelectBox.transform.parent:InverseTransformPoint(self.m_EndBoxPos)
            self.m_EndMousePos = Input.mousePosition
            self:DrawBoxArea()
        end
        
        if Input.GetMouseButtonUp(0) then
            if self.m_IsDrawing then
                self.m_IsDrawing = false
                self:EndDrawBox()
                self:OnAddOrRemoveFurniture()
                CLuaHouseMgr.ShowMultipleFurnishPopWnd()
            end          
        end
        return
    end

    --其他可操作状态
    if not CUICommonDef.IsOverUI or (UICamera.lastHit.collider and
        not CUICommonDef.IsChild(self.transform, UICamera.lastHit.collider.transform)) then
            return
    end
    if Input.GetMouseButtonDown(0) then
        if not CUICommonDef.IsChild(self.XuanFuView.transform, UICamera.lastHit.collider.transform) then
            self:OnUpdateXuanFuSliderActive(falses)
        end
        CUIManager.CloseUI(CLuaUIResources.MultipleFurnishPopupMenu)
    end
    if Input.GetMouseButtonUp(0) then
        CLuaHouseMgr.ShowMultipleFurnishPopWnd()      
    end
end

function LuaMultipleFurnitureEditWnd:OnApplicationFocus(args)
    self.m_FocusStatus = args[0]
    if not self.m_FocusStatus then
        self.m_IsDrawing = false
        self:EndDrawBox()
    end
end

function LuaMultipleFurnitureEditWnd:CheckIsMultipleType(fur)
    local templateId = fur.TemplateId
    local data = Zhuangshiwu_Zhuangshiwu.GetData(templateId)
    if data then
        return self.m_ValideTypeDict[data.Type]
    end
    return false
end

function LuaMultipleFurnitureEditWnd:CheckcandidatesCountLimit()
    local res = false
    local count = self.m_MultipleRoot.mChildList.Count
    
    return count < self.m_CandidateLimitCount
end

function LuaMultipleFurnitureEditWnd:StartDrawBox()
    self.SelectBox.gameObject:SetActive(true)
    self.SelectBox.transform.localPosition = self.m_StartBoxPos
    self.SelectBox.width = 1
    self.SelectBox.height = 1
    self.SelectBox.color = self.m_SelectedLeftMode == 1 and NGUIText.ParseColor24("E3FFEC", 0) or NGUIText.ParseColor24("ffeaea", 0)
    self.SelectBox.alpha = 200/255
    self.SelectBoxBg.color = self.m_SelectedLeftMode == 1 and NGUIText.ParseColor24("31D2B3", 0) or NGUIText.ParseColor24("e52c2c", 0)
    self.SelectBoxBg.alpha = 150/255
    --点选
end

function LuaMultipleFurnitureEditWnd:OnClickSingleTarget(mousePos)
    local ray = CMainCamera.Main:ScreenPointToRay(Input.mousePosition)
    local dist = CMainCamera.Main.farClipPlane - CMainCamera.Main.nearClipPlane
    local hits = Physics.RaycastAll(ray, dist, CMainCamera.Main.cullingMask)
    local pos
    local minDis = 0
    local fur = nil
    for i=0,hits.Length-1 do
        local collider = hits[i].collider.gameObject
        if collider.gameObject.layer == LayerDefine.Furniture and collider.transform then
            local trans = collider.transform
            while(not fur)
            do
                fur = trans:GetComponent(typeof(CClientFurniture))
                if not fur then
                    trans = trans.parent
                end
            end
            if fur then
                break
            end
        end
    end

    if fur then
        if not self:CheckIsMultipleType(fur) then
            g_MessageMgr:ShowMessage("Cannot_MultipleEdit_InvalidType")
            return
        end
        if not self:CheckcandidatesCountLimit() then
            if CLuaZswTemplateMgr.IsInCustomMode then
                g_MessageMgr:ShowMessage("AddZhuangshiwu_Limit")
            else
                g_MessageMgr:ShowMessage("Cannot_MultipleEdit_OverLimitCount")
            end
            
            return
        end
        local targetIsSelected = self.m_SelectedLeftMode == 1
        if fur.Selected ~= targetIsSelected then
            if targetIsSelected then
                self.m_MultipleRoot:AddChild(fur)
            else
                self.m_MultipleRoot:RemoveChild(fur.ID)
            end
            
            self:ChangeSelections()
        end
    end
    --CLuaHouseMgr.ShowMultipleFurnishPopWnd()
end

function LuaMultipleFurnitureEditWnd:DrawBoxArea()
    if not self.m_IsDrawing then
        return
    end
    if not self.m_StartBoxPos or not self.m_EndBoxPos then
        return
    end
    local width = math.abs(self.m_EndBoxPos.x - self.m_StartBoxPos.x)
    local height = math.abs(self.m_EndBoxPos.y - self.m_StartBoxPos.y)
    -- s--o
    -- o--e
    if self.m_EndBoxPos.x > self.m_StartBoxPos.x and self.m_EndBoxPos.y < self.m_StartBoxPos.y then
        self.SelectBox.width = width
        self.SelectBox.height = height
        self.SelectBox.transform.localEulerAngles = Vector3(0,0,0)
    -- o--s
    -- e--o
    elseif self.m_EndBoxPos.x < self.m_StartBoxPos.x and self.m_EndBoxPos.y < self.m_StartBoxPos.y then
        self.SelectBox.width = width
        self.SelectBox.height = height
        self.SelectBox.transform.localEulerAngles = Vector3(0,180,0)
    -- o--e
    -- s--o
    elseif self.m_EndBoxPos.x > self.m_StartBoxPos.x and self.m_EndBoxPos.y > self.m_StartBoxPos.y then
        self.SelectBox.width = width
        self.SelectBox.height = height
        self.SelectBox.transform.localEulerAngles = Vector3(180,0,0)
    -- e--o
    -- o--s
    elseif self.m_EndBoxPos.x < self.m_StartBoxPos.x and self.m_EndBoxPos.y > self.m_StartBoxPos.y then
        self.SelectBox.width = width
        self.SelectBox.height = height
        self.SelectBox.transform.localEulerAngles = Vector3(0,0,180)
    end
end

function LuaMultipleFurnitureEditWnd:EndDrawBox()
    self.SelectBox.gameObject:SetActive(false)
end

function LuaMultipleFurnitureEditWnd:OnAddOrRemoveFurniture()
    local dict = CClientFurnitureMgr.Inst.FurnitureList
    local targetIsSelected = self.m_SelectedLeftMode == 1
    CommonDefs.DictIterate(dict,DelegateFactory.Action_object_object(function (___key, ___value) 
        local fid = tonumber(___key)
        local fur = ___value
        local wpos = fur.RO.transform.position
        local screenPos = CMainCamera.Main:WorldToScreenPoint(wpos)
        if self:CheckIsInBox(screenPos) and self:CheckIsMultipleType(fur) and self:CheckcandidatesCountLimit() then
            if fur.Selected ~= targetIsSelected then
                if targetIsSelected then
                    self.m_MultipleRoot:AddChild(fur)
                else
                    self.m_MultipleRoot:RemoveChild(fur.ID)
                end
            end
        else

        end
    end))
    self:ChangeSelections()

    --CLuaHouseMgr.ShowMultipleFurnishPopWnd()
end

function LuaMultipleFurnitureEditWnd:CheckIsInBox(pos)
    local minx = math.min(self.m_StartMousePos.x,self.m_EndMousePos.x)
    local maxx = math.max(self.m_StartMousePos.x,self.m_EndMousePos.x)
    local miny = math.min(self.m_StartMousePos.y,self.m_EndMousePos.y)
    local maxy = math.max(self.m_StartMousePos.y,self.m_EndMousePos.y)
    if pos.x < minx or pos.x > maxx then
        return false
    end
    if pos.y < miny or pos.y > maxy then
        return false
    end
    return true
end

function LuaMultipleFurnitureEditWnd:OnSelectButtonClick(button)
    local selectButton = button:GetComponent(typeof(QnSelectableButton))

    for i = 1, #self.m_LeftTabBtnList, 1 do
        if selectButton == self.m_LeftTabBtnList[i] then
            if selectButton:isSeleted() then
                self.m_LeftTabBtnList[i]:SetSelected(false)
                self.m_SelectedLeftMode = -1
            else
                self.m_LeftTabBtnList[i]:SetSelected(true)
                self.m_SelectedLeftMode = tonumber(i)
            end
        else
            self.m_LeftTabBtnList[i]:SetSelected(false)
        end    
    end

    self.ViewButtons:SetActive(self.m_SelectedLeftMode == 3)
    CClientFurnitureMgr.Inst.FurnitureEditCanRotate = self.m_SelectedLeftMode == 3
    local tipname = self.m_SelectedLeftMode == 1 and LocalString.GetString("增加") or LocalString.GetString("减去") 
    if self.m_SelectedLeftMode == 3 then
        self.TipLabel.gameObject:SetActive(true)
        self.TipLabel.text = LocalString.GetString("滑动屏幕可调整摄像机视角")
    elseif self.m_SelectedLeftMode == -1  then       
        self.TipLabel.gameObject:SetActive(false)
    else
        self.TipLabel.gameObject:SetActive(true)
        self.TipLabel.text = SafeStringFormat3(LocalString.GetString("按住拖拽进行框选，[03B836]%s[-]选中装饰物"),tipname)     
    end
end

function LuaMultipleFurnitureEditWnd:ResetCamera()
    if self.m_CameraTrans == nil then
        self.m_CameraTrans = CameraFollow.Inst.transform:Find("camRotate/newParent")
    end
    self.m_CameraTrans.localPosition = Vector3(0,0,0)
end

function LuaMultipleFurnitureEditWnd:OnSelectCamera(b)
    self.ViewButtons:SetActive(b)
    --CClientFurnitureMgr.Inst.FurnitureEditCanRotate = b
end

function LuaMultipleFurnitureEditWnd:OnClearAllSelected()

	self.m_MultipleRoot:ClearChildren()
    g_ScriptEvent:BroadcastInLua("UpdateMultipleFurnitureChoice")
end

function LuaMultipleFurnitureEditWnd:ChangeSelections()
    g_ScriptEvent:BroadcastInLua("UpdateMultipleFurnitureChoice")

    local count = self.m_MultipleRoot.mChildList.Count   
    self.SelectedCountLable.text = SafeStringFormat3(LocalString.GetString("已选中%d/%d件装饰物"),count,self.m_CandidateLimitCount)
end

function LuaMultipleFurnitureEditWnd:OnUpdateMultipleFurnitureChoiceFromListWnd()
    local count = self.m_MultipleRoot.mChildList.Count  
    self.SelectedCountLable.text = SafeStringFormat3(LocalString.GetString("已选中%d件装饰物"),count)
    CUIManager.CloseUI(CLuaUIResources.MultipleFurnishPopupMenu)
end
--xuanfu
function LuaMultipleFurnitureEditWnd:OnUpdateXuanFuSliderActive(bShow)
    if bShow then
        CUIManager.CloseUI(CLuaUIResources.MultipleChooseListWnd)
        CUIManager.CloseUI(CLuaUIResources.MultipleFurnishPopupMenu)
        CLuaHouseMgr.MultipleRoot:InitTransformPosition()
        local basePos = CLuaHouseMgr.MultipleRoot.mRootPosition
        self.m_BaseRootY = basePos.y
        local terrainHeight = CRenderScene.Inst:SampleLogicHeight(math.floor(basePos.x)+0.5, math.floor(basePos.z)+0.5)
        local yOffset = basePos.y - terrainHeight

        local heightInterval = Zhuangshiwu_Setting.GetData().PlantOffsetInterval
        local percent = (yOffset - heightInterval[0]) / (heightInterval[1] - heightInterval[0])
        percent = math.min(math.max(percent, 0), 1)

        self.VerticalSlider:GetComponent(typeof(UISlider)).value = percent
        local fixvalue = percent * (heightInterval[1] - heightInterval[0])
        fixvalue = fixvalue / (8 - heightInterval[0])
        self.FloatingLabel.text = tostring(math.floor(fixvalue * 100))
    end
    self.XuanFuView:SetActive(bShow)
end
function LuaMultipleFurnitureEditWnd:OnVerticalValueChanged(value)
    local heightInterval = Zhuangshiwu_Setting.GetData().PlantOffsetInterval
    local fixvalue = value * (heightInterval[1] - heightInterval[0])
    fixvalue = fixvalue / (8 - heightInterval[0])

    self.FloatingLabel.text = tostring(math.floor(fixvalue * 100))

    local percent =value
    local yOffset = heightInterval[0]+(heightInterval[1]-heightInterval[0])*percent
    local basePos = CLuaHouseMgr.MultipleRoot.mRootPosition
    local posY = CRenderScene.Inst:SampleLogicHeight(math.floor(basePos.x)+0.5, math.floor(basePos.z)+0.5)

    local shiftPos = CLuaHouseMgr.MultipleRoot.mRootPosition
    local offy = yOffset

    local shifty = posY+offy+CClientFurniture.s_YOffset

    local newOffY = math.floor(shifty - self.m_BaseRootY)
    shiftPos.y = shifty
    --过山车
    for i=0,CLuaHouseMgr.MultipleRoot.mChildList.Count-1,1 do
        local fur = CLuaHouseMgr.MultipleRoot.mChildList[i]
        local zswId = fur.TemplateId
        local zswdata = Zhuangshiwu_Zhuangshiwu.GetData(zswId)
        if CLuaHouseMgr.CheckCanXuanFu(fur, zswdata) then
            if (zswdata.SubType == EnumFurnitureSubType_lua.eRollerCoasterTrack) then
                CLuaHouseMgr.MultipleRoot:RevertChildParentRoot(fur)
                local oldPosy = CLuaHouseMgr.MultipleRoot.mChildrenOffsetCache[i].y
                --local newPosy = CRenderScene.Inst:SampleLogicHeight(math.floor(pos.x)+0.5, math.floor(pos.z)+0.5)
                local newPosy = oldPosy + newOffY      
                local roPos = fur.RO.transform.position     
                fur.RO.transform.position = Vector3(roPos.x,newPosy,roPos.z)
            end
        else
            CLuaHouseMgr.MultipleRoot:RevertChildParentRoot(fur)
        end
    end
    CLuaHouseMgr.MultipleRoot:SetRootPosition(shiftPos,true)
    CLuaClientFurnitureMgr.m_ProcessedFurnitureId = self.FurnitureId
end

--移动
function LuaMultipleFurnitureEditWnd:OnDragJoyStick(screendir)
    --convert to world direction
    local direction = self.VirtualJoyStick:ConvertToWorldDirection(screendir)
    EventManager.BroadcastInternalForLua(EnumEventType.JoyStickDragging, {direction, false})
    self.m_IsDragJoyStick = true
end

function LuaMultipleFurnitureEditWnd:OnDragFinished()
	EventManager.BroadcastInternalForLua(EnumEventType.JoyStickDragging, {Vector3.zero, true})
    self.m_IsDragJoyStick = false
end

function LuaMultipleFurnitureEditWnd:OnPinchIn(gesture)
    local deltaPinch = gesture.deltaPinch
    local pinchScale = math.min(math.max(deltaPinch / 400, 0),1)
    self:OnPinch(pinchScale)
end

function LuaMultipleFurnitureEditWnd:OnPinchOut(gesture)
    local deltaPinch = gesture.deltaPinch
    local pinchScale = math.min(math.max(deltaPinch / 400, 0),1)
    self:OnPinch(-pinchScale)
end

function LuaMultipleFurnitureEditWnd:OnMouseScrollWheel()
    local v = Input.GetAxis("Mouse ScrollWheel")
    self:OnPinch(v)
end

function LuaMultipleFurnitureEditWnd:OnPinch(v)
    if NormalCamera.Inst:IsAllowZoom() then
        local mt = math.min(math.max(NormalCamera.Inst.m_T + v, -1),1)
        NormalCamera.Inst.m_T = mt
        NormalCamera.Inst:SetCurrentRZY()      
    end
end

--@region UIEvent

function LuaMultipleFurnitureEditWnd:OnChooseListBtnClick()
    if CUIManager.IsLoaded(CLuaUIResources.MultipleChooseListWnd) then
		CUIManager.CloseUI(CLuaUIResources.MultipleChooseListWnd)
	else
		CUIManager.ShowUI(CLuaUIResources.MultipleChooseListWnd)
	end
end


--@endregion UIEvent

