local TweenAlpha = import "TweenAlpha"

CLuaErHaOpenJinNangWnd = class()

RegistChildComponent(CLuaErHaOpenJinNangWnd, "m_JinNangTexture","JinNangTexture", TweenAlpha)
RegistChildComponent(CLuaErHaOpenJinNangWnd, "m_JinNangTexture2","JinNangTexture2", TweenAlpha)
RegistChildComponent(CLuaErHaOpenJinNangWnd, "m_GuideFinger","GuideFinger", GameObject)
RegistClassMember(CLuaErHaOpenJinNangWnd,"m_CloseTick")

function CLuaErHaOpenJinNangWnd:Awake()
    self.m_JinNangTexture.enabled = false
    self.m_JinNangTexture2.enabled = false
    self.m_JinNangTexture.gameObject:SetActive(true)
    self.m_JinNangTexture2.gameObject:SetActive(false)
    self.m_GuideFinger:SetActive(true)
end

function CLuaErHaOpenJinNangWnd:Init()
    self.m_JinNangTexture:SetOnFinished(DelegateFactory.Callback(function ()
        self.m_JinNangTexture2.gameObject:SetActive(true)
        self.m_JinNangTexture2.enabled = true
        self.m_JinNangTexture2:PlayForward()
    end))

    self.m_JinNangTexture2:SetOnFinished(DelegateFactory.Callback(function ()
        if self.m_CloseTick ~= nil then
            UnRegisterTick(self.m_CloseTick)
        end
        self.m_CloseTick = RegisterTickWithDuration(function ()
            Gac2Gas.FinishClientTaskEvent(CLuaTaskMgr.m_OpenJinNang_TaskId,"OpenJinNang",MsgPackImpl.pack(empty))
            CUIManager.CloseUI(CLuaUIResources.ErHaOpenJinNangWnd)
        end,2000,2000)
    end))

    UIEventListener.Get(self.m_JinNangTexture.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
        self:OnClickJinNang()
    end)
end

function CLuaErHaOpenJinNangWnd:OnClickJinNang()
    self.m_GuideFinger:SetActive(false)
    self.m_JinNangTexture.enabled = true
    self.m_JinNangTexture:PlayForward()  
end
