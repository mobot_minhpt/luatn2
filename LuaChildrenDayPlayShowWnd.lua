local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local CUITexture = import "L10.UI.CUITexture"
local Extensions = import "Extensions"
local Vector3 = import "UnityEngine.Vector3"
local CHideAndSeekSkillItem = import "L10.UI.CHideAndSeekSkillItem"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"

LuaChildrenDayPlayShowWnd = class()
RegistChildComponent(LuaChildrenDayPlayShowWnd,"closeBtn", GameObject)
RegistChildComponent(LuaChildrenDayPlayShowWnd,"tipBtn", GameObject)
RegistChildComponent(LuaChildrenDayPlayShowWnd,"showTextLabel", UILabel)
RegistChildComponent(LuaChildrenDayPlayShowWnd,"restNumLabel", UILabel)
RegistChildComponent(LuaChildrenDayPlayShowWnd,"skillNode", CHideAndSeekSkillItem)
RegistChildComponent(LuaChildrenDayPlayShowWnd,"skillNode2", CHideAndSeekSkillItem)
RegistChildComponent(LuaChildrenDayPlayShowWnd,"smallNode", GameObject)
RegistChildComponent(LuaChildrenDayPlayShowWnd,"sFNode", GameObject)
RegistChildComponent(LuaChildrenDayPlayShowWnd,"attendBtn", GameObject)
RegistChildComponent(LuaChildrenDayPlayShowWnd,"cancelBtn", GameObject)

--RegistClassMember(LuaChildrenDayPlayShowWnd, "maxChooseNum")
function LuaChildrenDayPlayShowWnd:Split(str, pat)
   local t = {}  -- NOTE: use {n = 0} in Lua-5.0
   local fpat = "(.-)" .. pat
   local last_end = 1
   local s, e, cap = str:find(fpat, 1)
   while s do
      if s ~= 1 or cap ~= "" then
         table.insert(t,cap)
      end
      last_end = e+1
      s, e, cap = str:find(fpat, last_end)
   end
   if last_end <= #str then
      cap = str:sub(last_end)
      table.insert(t, cap)
   end
   return t
end

function LuaChildrenDayPlayShowWnd:Close()
	CUIManager.CloseUI(CLuaUIResources.ChildrenDayPlayShowWnd)
end

function LuaChildrenDayPlayShowWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateChildrenDayPlayShowInfo", self, "Init")
end

function LuaChildrenDayPlayShowWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateChildrenDayPlayShowInfo", self, "Init")
end

function LuaChildrenDayPlayShowWnd:InitSmallSkillNode()
  Extensions.RemoveAllChildren(self.sFNode.transform)

  local skillIdTable = self:Split(LiuYi2019_Setting.GetData().CarExtraSkill,';')
  local width = 105
  local count = 0

  for i,v in ipairs(skillIdTable) do
    local id = tonumber(v)
    if id and id > 0 then
      local node = NGUITools.AddChild(self.sFNode,self.smallNode)
      node:SetActive(true)
      node.transform.localPosition = Vector3(count * width,0,0)
      --local sp = node:GetComponent(typeof(CHideAndSeekSkillItem))
      --sp:Init(id)
      local skillIcon = node.transform:Find('Mask/Icon'):GetComponent(typeof(CUITexture))
      local skillData = Skill_AllSkills.GetData(id)
      skillIcon:LoadSkillIcon(skillData.SkillIcon)
      local onClick = function(go)
        CSkillInfoMgr.ShowSkillInfoWnd(id, Vector3(go.transform.position.x,0,0), CSkillInfoMgr.EnumSkillInfoContext.Default,true,0,0,nil)
      end
      CommonDefs.AddOnClickListener(node,DelegateFactory.Action_GameObject(onClick),false)

      count = count + 1
    end
  end
end

function LuaChildrenDayPlayShowWnd:Init()
	local onCloseClick = function(go)
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.closeBtn,DelegateFactory.Action_GameObject(onCloseClick),false)
  self.smallNode:SetActive(false)
  self.attendBtn:SetActive(false)
  self.cancelBtn:SetActive(false)

  --QueryPengPengCheSignUpInfo
  local onAttendClick = function(go)
    Gac2Gas.RequestSignUpPengPengChe()
    --self.attendBtn:SetActive(false)
    --self.cancelBtn:SetActive(true)
  end
	CommonDefs.AddOnClickListener(self.attendBtn,DelegateFactory.Action_GameObject(onAttendClick),false)
  local onCancelClick = function(go)
    Gac2Gas.RequestCancelSignUpPengPengChe()
    --self.attendBtn:SetActive(true)
    --self.cancelBtn:SetActive(false)
  end
	CommonDefs.AddOnClickListener(self.cancelBtn,DelegateFactory.Action_GameObject(onCancelClick),false)
  local onTipClick = function(go)
    g_MessageMgr:ShowMessage("PengPengChe_Tips")
  end
	CommonDefs.AddOnClickListener(self.tipBtn,DelegateFactory.Action_GameObject(onTipClick),false)

  local skillTable = self:Split(LiuYi2019_Setting.GetData().CarSkill, ';')
	local skillId = tonumber(skillTable[1])
  local skillId2 = tonumber(skillTable[2])
  if skillId and skillId2 then
    self.skillNode:Init(skillId)
    self.skillNode2:Init(skillId2)
    local skillData = Skill_AllSkills.GetData(skillId)
    local skillData2 = Skill_AllSkills.GetData(skillId2)
    self.skillNode.transform:Find('SkillDesc'):GetComponent(typeof(UILabel)).text = skillData.Display
    self.skillNode2.transform:Find('SkillDesc'):GetComponent(typeof(UILabel)).text = skillData2.Display
  end

  self.showTextLabel.text = g_MessageMgr:FormatMessage("PengPengChe_Rules")

  self.restNumLabel.text = '0'
  --LuaChildrenDay2019Mgr.PengPengCheShowInfo = {isSignUp = isSignUp,isClientRequest= isClientRequest,todayRemainRewardTimes=todayRemainRewardTimes}
  if LuaChildrenDay2019Mgr.PengPengCheShowInfo then
    self.restNumLabel.text = LuaChildrenDay2019Mgr.PengPengCheShowInfo.todayRemainRewardTimes
    if LuaChildrenDay2019Mgr.PengPengCheShowInfo.isSignUp then
      self.attendBtn:SetActive(false)
      self.cancelBtn:SetActive(true)
    else
      self.attendBtn:SetActive(true)
      self.cancelBtn:SetActive(false)
    end
  end

  self:InitSmallSkillNode()
end

return LuaChildrenDayPlayShowWnd
