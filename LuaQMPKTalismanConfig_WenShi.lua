--法宝配置 纹饰属性

local Jewel_Jewel=import "L10.Game.Jewel_Jewel"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"
local CQMPKTitleTemplate = import "L10.UI.CQMPKTitleTemplate"
local Word_Word = import "L10.Game.Word_Word"



RegistClassMember(CLuaQMPKTalismanConfig,"m_WenShiIndexTbl")
RegistClassMember(CLuaQMPKTalismanConfig,"m_WenShiTitleTbl")
RegistClassMember(CLuaQMPKTalismanConfig,"m_WenShiDescTbl")
RegistClassMember(CLuaQMPKTalismanConfig,"m_WenShiJewelChoiceTbl")
RegistClassMember(CLuaQMPKTalismanConfig,"m_CurrentWenShiTitleIndex")


function CLuaQMPKTalismanConfig:InitWenShiNil()
    local tip=FindChild(self.m_WenShiRoot.transform,"TipLabel").gameObject
    tip:SetActive(false)
    local parent=FindChild(self.m_WenShiRoot.transform,"Table")
    CUICommonDef.ClearTransform(parent)

end


function CLuaQMPKTalismanConfig:InitWenShi()
    local parent=FindChild(self.m_WenShiRoot.transform,"Table")
    local tip=FindChild(self.m_WenShiRoot.transform,"TipLabel").gameObject
    if #self.m_WenShiIndexTbl>0 then
        tip:SetActive(false)
    else
        tip:SetActive(true)
    end

    CUICommonDef.ClearTransform(parent)
    local titleTemplate=FindChild(self.m_WenShiRoot.transform,"TitleTemplate").gameObject
    local wordTemplate=FindChild(self.m_WenShiRoot.transform,"WordTemplate").gameObject


    self.m_WenShiTitleTbl={}
    self.m_WenShiDescTbl={}

    local talisman = QuanMinPK_Talisman.GetData(self.m_TalismanTemplate2Id[self.m_CurrentTemplateId])
    if talisman then
        local jewelIds= talisman.WenShiStr
        for i,v in ipairs(self.m_WenShiIndexTbl) do
            local jewelData=Jewel_Jewel.GetData(jewelIds[v-1])
            local word = Word_Word.GetData(jewelData.onFaBao)

            local go=NGUITools.AddChild(parent.gameObject,titleTemplate)    
            go:SetActive(true)

            UIEventListener.Get(go).onClick=DelegateFactory.VoidDelegate(function(go) self:OnWenShiTitleClick(go) end)

            go:GetComponent(typeof(CQMPKTitleTemplate)):Init(jewelData.JewelName)
            self.m_WenShiTitleTbl[i]=go

            go=NGUITools.AddChild(parent.gameObject,wordTemplate)
            go:SetActive(true)
            local label=FindChild(go.transform,"Label"):GetComponent(typeof(UILabel))
            label.text=word.Description
            self.m_WenShiDescTbl[i]=go
        end
    end
    parent:GetComponent(typeof(UITable)):Reposition()
end

function CLuaQMPKTalismanConfig:OnWenShiTitleClick( go) 
    if CLuaQMPKMgr.s_IsOtherPlayer then
        return
    end

    self.m_CurrentWenShiTitleIndex=0
    for i,v in ipairs(self.m_WenShiTitleTbl) do
        if go==v then
            self.m_CurrentWenShiTitleIndex=i
            break
        end
    end

    self.m_WenShiJewelChoiceTbl={}
    local talisman = QuanMinPK_Talisman.GetData(self.m_TalismanTemplate2Id[self.m_CurrentTemplateId])
    if talisman then
        local lookup={}
        for i,v in ipairs(self.m_WenShiIndexTbl) do
            lookup[v]=true
        end
        local popupList={}
        local jewelIds= talisman.WenShiStr
        for i=1,jewelIds.Length do
            if not lookup[i] then--选过的不能再选了
                local jewelData=Jewel_Jewel.GetData(jewelIds[i-1])
                table.insert( self.m_WenShiJewelChoiceTbl,jewelIds[i-1] )
                local data = PopupMenuItemData(jewelData.JewelName, DelegateFactory.Action_int(function(index) self:OnWenShiTitleSelected(index) end),false, nil, EnumPopupMenuItemStyle.Light)
                table.insert( popupList,data )
            end
        end

        local array=Table2Array(popupList,MakeArrayClass(PopupMenuItemData))
        CPopupMenuInfoMgr.ShowPopupMenu(array, go.transform, CPopupMenuInfoMgr.AlignType.Bottom, #popupList >= 8 and 2 or 1, nil, 600, true, 296)
    end
end
function CLuaQMPKTalismanConfig:GetJewelIndex(jewelId)
    local talisman = QuanMinPK_Talisman.GetData(self.m_TalismanTemplate2Id[self.m_CurrentTemplateId])
    if talisman then
        local jewelIds= talisman.WenShiStr
        for i=1,jewelIds.Length do
            if jewelId==jewelIds[i-1] then
                return i
            end
        end
    end
    return 0
end

function CLuaQMPKTalismanConfig:OnWenShiTitleSelected(index)
    local jewelId=self.m_WenShiJewelChoiceTbl[index+1]
    if self.m_CurrentWenShiTitleIndex>0 and jewelId then
        if self.m_WenShiIndexTbl[self.m_CurrentWenShiTitleIndex] ~= self:GetJewelIndex(jewelId) then self.m_HasChanged = true end
        --更新索引
        self.m_WenShiIndexTbl[self.m_CurrentWenShiTitleIndex]=self:GetJewelIndex(jewelId)

        --刷新界面
        local jewelData=Jewel_Jewel.GetData(jewelId)
        self.m_WenShiTitleTbl[self.m_CurrentWenShiTitleIndex]:GetComponent(typeof(CQMPKTitleTemplate)):Init(jewelData.JewelName)

        local word = Word_Word.GetData(jewelData.onFaBao)
        FindChild(self.m_WenShiDescTbl[self.m_CurrentWenShiTitleIndex].transform,"Label"):GetComponent(typeof(UILabel)).text=word.Description
    end
end

function CLuaQMPKTalismanConfig:OnWenShiConfirmClicked(pos)
    local raw=""
    for i,v in ipairs(self.m_WenShiIndexTbl) do
        raw=raw..tostring(v)..","
    end
    raw=string.sub(raw,1,string.len( raw )-1)--移除最后一个逗号
    Gac2Gas.RequestSetQmpkTalismanWenShi(EnumItemPlace_lua.Body, pos, self.m_CurrentItemId, raw)
end

