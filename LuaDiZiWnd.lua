local Object = import "System.Object"
local UICamera          = import "UICamera"
local Vector3           = import "UnityEngine.Vector3"
local Physics           = import "UnityEngine.Physics"
local SoundManager      = import "SoundManager"
local COpenEntryMgr     = import "L10.Game.COpenEntryMgr"

LuaDiZiWnd = class()
LuaDiZiWnd.taskId  = 0
LuaDiZiWnd.fluteId  = 1
--------RegistChildComponent-------
RegistChildComponent(LuaDiZiWnd,        "TouchBg",              GameObject)
RegistChildComponent(LuaDiZiWnd,        "SingleEffect",         GameObject)
RegistChildComponent(LuaDiZiWnd,        "HoleCol_1",            GameObject)
RegistChildComponent(LuaDiZiWnd,        "HoleCol_2",            GameObject)
RegistChildComponent(LuaDiZiWnd,        "HoleCol_3",            GameObject)
RegistChildComponent(LuaDiZiWnd,        "HoleCol_4",            GameObject)
RegistChildComponent(LuaDiZiWnd,        "HoleCol_5",            GameObject)
RegistChildComponent(LuaDiZiWnd,        "HoleCol_6",            GameObject)
RegistChildComponent(LuaDiZiWnd,        "CompleteEffect",       GameObject)
RegistChildComponent(LuaDiZiWnd,        "LineHelp",             GameObject)
RegistChildComponent(LuaDiZiWnd,        "ClickHelp",            GameObject)

---------RegistClassMember-------
RegistClassMember(LuaDiZiWnd,           "m_HoleTable")          --  孔洞

RegistClassMember(LuaDiZiWnd,           "m_PlayType")           --  弹奏类型    -- 0 单击 / 1 滑动
RegistClassMember(LuaDiZiWnd,           "m_Audio")              --  当前音频
RegistClassMember(LuaDiZiWnd,           "m_StageTable")         --  所有阶段table
RegistClassMember(LuaDiZiWnd,           "m_Stage")              --  当前阶段
RegistClassMember(LuaDiZiWnd,           "m_PlayHolesTable")     --  当前阶段需要弹奏的孔位（从表里读的数据）
RegistClassMember(LuaDiZiWnd,           "m_PassHolesTable")     --  已弹奏孔位（记录当前已按下的孔位，松手时置空）
RegistClassMember(LuaDiZiWnd,           "m_HolesPos")           --  需要弹奏的孔位位置
RegistClassMember(LuaDiZiWnd,           "m_Sound")              --  音频
RegistClassMember(LuaDiZiWnd,           "m_IntervalTick")       --  间隔倒计时
RegistClassMember(LuaDiZiWnd,           "m_InInterval")         --  是否处在播放间隔期间


function LuaDiZiWnd:Awake()
    self.m_Sound = nil
    self:InitHoleTable()
    self.m_PlayHolesTable = {}
    self.m_PlayType = 0
    self.m_Stage = 1            --默认从第一阶段开始弹奏
    self.m_InInterval = false
    self.LineHelp:SetActive(false)
    self.ClickHelp:SetActive(false)
    self:initStageTable()
    self:RefreshCurentPlayData()        --  刷新当前弹奏状态
    self:ShowCurentPlay()               --  显示当前弹奏状态
end

function LuaDiZiWnd:OnDisable()
    self:StopSound()
    UnRegisterTick(self.m_IntervalTick)
end

function LuaDiZiWnd:StopSound()
	if self.m_Sound~=nil then
        SoundManager.Inst:StopSound(self.m_Sound)
		self.m_Sound = nil
	end
end

function LuaDiZiWnd:initStageTable()
    local Diziqu = ZhuJueJuQing_Diziqu.GetData(LuaDiZiWnd.fluteId)
	self.m_StageTable = {}
	for i = 0, Diziqu.play.Length-1 do
		table.insert(self.m_StageTable, Diziqu.play[i])
	end
end

function LuaDiZiWnd:InitHoleTable()
    self.m_HoleTable = {self.HoleCol_1,self.HoleCol_2,self.HoleCol_3,self.HoleCol_4,self.HoleCol_5,self.HoleCol_6}
    for i=1,#self.m_HoleTable do
        UIEventListener.Get(self.m_HoleTable[i]).onClick = DelegateFactory.VoidDelegate(function(p)
            self:OnClick(i)
        end)
        UIEventListener.Get(self.m_HoleTable[i]).onDrag = DelegateFactory.VectorDelegate(function(go,delta)
            self:OnDrag(go,delta)
        end)
        UIEventListener.Get(self.m_HoleTable[i]).onDragEnd = DelegateFactory.VoidDelegate(function(go)
            self:OnDragEnd()
        end)
    end
end

function LuaDiZiWnd:OnClick(i)
    if self.m_PlayType ~= 0 then return end--  只在单击模式下监听点击

    local HitNum = tonumber(string.sub(self.m_HoleTable[i].name, -1))
    if HitNum == self.m_HolesPos then
        --  单击成功
        self.SingleEffect:SetActive(false);
        self.m_Sound = SoundManager.Inst:PlayOneShot(self.m_Audio, Vector3.zero, nil, 0)
        self:StageComplete()
    end
end

function LuaDiZiWnd:OnDrag(go,delta)
    if self.m_PlayType ~= 1 then return end--  只在滑动模式下监听滑动

    local currentPos = UICamera.currentTouch.pos
    local ray = UICamera.currentCamera:ScreenPointToRay(Vector3(currentPos.x,currentPos.y,0))
    local hits = Physics.RaycastAll(ray, 99999,  UICamera.currentCamera.cullingMask)

    local HitNum = 0
    for i=0,hits.Length-1 do
        local collider = hits[i].collider.gameObject
        if (string.find(collider.name, "HoleCol_", 1, true) ~= nil) then
            HitNum = tonumber(string.sub(collider.name, -1))
            --滑动单个成功并检查是否阶段成功
            if HitNum == self.m_HolesPos then
                if self.m_Sound == nil then
                    self.m_Sound = SoundManager.Inst:PlaySound(self.m_Audio,Vector3.zero, 1, nil, 0)
                end
                collider.transform:Find("LineEffect").gameObject:SetActive(false)
                if self.m_HolesPos >= #self.m_PlayHolesTable then
                    self:StageComplete()
                else
                    self.m_HolesPos = self.m_HolesPos + 1
                end
            end
        end
    end
end

function LuaDiZiWnd:OnDragEnd()
    if self.m_InInterval == false then
        self:ShowCurentPlay()
        self:StopSound()
    end
end

function LuaDiZiWnd:RefreshCurentPlayData() --  刷新当前弹奏空洞状态（操作成功或失败时刷新）
    self:StopSound()
    self.LineHelp:SetActive(false)
    self.ClickHelp:SetActive(false)
    if self.m_Stage > #self.m_StageTable then
        self:ToComplete()
        return
    end
    local Chuidi = ZhuJueJuQing_Chuidi.GetData(self.m_StageTable[self.m_Stage])

    self.m_PlayHolesTable = {}
	for i = 0, Chuidi.action.Length-1 do
		table.insert(self.m_PlayHolesTable, Chuidi.action[i])
    end
    self.m_Audio = Chuidi.audio

    self.m_HolesPos = self.m_PlayHolesTable[1]          -- 不论是点击还是滑动，第一个要经过的孔肯定都是m_PlayHolesTable里的第一个

    if #self.m_PlayHolesTable > 1 then
        self.m_PlayType = 1
    else
        self.m_PlayType = 0
    end
    self:ShowHelper()
end

function LuaDiZiWnd:ShowHelper()    --  检测是否显示小手提示
    if self.m_PlayType == 0 then
        self.ClickHelp:SetActive(not COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.DiZiClickFilter))
        COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.DiZiClickFilter)
        self.ClickHelp.transform:SetParent(self.m_HoleTable[self.m_HolesPos].transform)
        self.ClickHelp.transform.localPosition = Vector3.zero
    elseif self.m_PlayType == 1 then
        self.LineHelp:SetActive(not COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.DiZiLineFilter))
        COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.DiZiLineFilter)
    end
end

function LuaDiZiWnd:ShowCurentPlay()
    self.m_HolesPos = self.m_PlayHolesTable[1]          -- 不论是点击还是滑动，第一个要经过的孔肯定都是m_PlayHolesTable里的第一个

    for i=1,#self.m_PlayHolesTable do
        if self.m_PlayType == 0 then
            self.SingleEffect:SetActive(true);
            self.SingleEffect.transform.localPosition = self.m_HoleTable[self.m_PlayHolesTable[1]].transform.localPosition
        elseif self.m_PlayType == 1 then
            self.m_HoleTable[i].transform:Find("LineEffect").gameObject:SetActive(true)
        end
    end
end

function LuaDiZiWnd:StageComplete() --  阶段完成
    self.CompleteEffect:SetActive(false)
    self.CompleteEffect:SetActive(true)
    self.m_HolesPos = -1
    self.m_Stage = self.m_Stage + 1 --  进入下一阶段
    self.m_PlayHolesTable = {}
    self.m_InInterval = true
    self.m_PlayType = -1

    UnRegisterTick(self.m_IntervalTick)         --  完成后处于1s种播放音频间隔，不可操作
    self.m_IntervalTick = RegisterTickOnce(function ()
        self.m_InInterval = false
        self:RefreshCurentPlayData()
        self:ShowCurentPlay()
    end, 1000)
end

function LuaDiZiWnd:ToComplete()
	local empty = CreateFromClass(MakeGenericClass(List, Object))
	empty = MsgPackImpl.pack(empty)
	Gac2Gas.FinishClientTaskEventWithConditionId(LuaDiZiWnd.taskId, "PlayFlute", empty, LuaDiZiWnd.fluteId)

    UnRegisterTick(self.m_IntervalTick)
    self.m_IntervalTick = RegisterTickOnce(function ()
        CUIManager.CloseUI(CLuaUIResources.DiZiWnd)
    end, 3000)
end
