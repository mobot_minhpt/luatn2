local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CClientObject = import "L10.Game.CClientObject"
local CClientObjectMgr=import "L10.Game.CClientObjectMgr"
local Quaternion = import "UnityEngine.Quaternion"
LuaDaFuWongMiniMap = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaDaFuWongMiniMap, "BG", "BG", GameObject)
RegistChildComponent(LuaDaFuWongMiniMap, "PlayerTemplate", "PlayerTemplate", GameObject)
RegistChildComponent(LuaDaFuWongMiniMap, "PlayerRoot", "PlayerRoot", GameObject)
RegistChildComponent(LuaDaFuWongMiniMap, "LandTemplate", "LandTemplate", GameObject)
RegistChildComponent(LuaDaFuWongMiniMap, "LandNode", "LandNode", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaDaFuWongMiniMap, "m_PlayerView")
RegistClassMember(LuaDaFuWongMiniMap, "m_LandView")
RegistClassMember(LuaDaFuWongMiniMap, "m_MoveTick")
RegistClassMember(LuaDaFuWongMiniMap, "m_MaxMoveTime")
RegistClassMember(LuaDaFuWongMiniMap, "m_ColorList")
RegistClassMember(LuaDaFuWongMiniMap, "m_PlayerPathList")
function LuaDaFuWongMiniMap:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.BG.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBGClick()
	end)

    --@endregion EventBind end
	self.m_PlayerView = nil
	self.m_LandView = nil
	self.m_MoveTick = nil
	self.m_ColorList = {"F6D05E","E75656","A55DC9","3BB86D"}
	self.m_ColorList[0] = "8C9FBA"
	self.m_PlayerPathList = {"dafuwongpopmapwnd_xiaoditu_jiantou_y","dafuwongpopmapwnd_xiaoditu_jiantou_r","dafuwongpopmapwnd_xiaoditu_jiantou_p", "dafuwongpopmapwnd_xiaoditu_jiantou_g"}
	self.m_MaxMoveTime = DaFuWeng_Countdown.GetData(EnumDaFuWengStage.RoundMove).Time + 1
	self.LandTemplate.gameObject:SetActive(false)
	self.PlayerTemplate.gameObject:SetActive(false)
end

function LuaDaFuWongMiniMap:Init()
	self:OnStageUpdate(LuaDaFuWongMgr.CurStage)
end
-- 待优化
-- function LuaDaFuWongMiniMap:Update()
-- 	if not self.m_PlayerView then self:InitPlayerViewList() return end
-- 	for k,v in pairs(self.m_PlayerView) do
-- 		self:SetPlayerViewPos(k,v)
-- 	end
-- end

function LuaDaFuWongMiniMap:InitPlayerViewList()
	if not LuaDaFuWongMgr.PlayerInfo then return end
	self.m_PlayerView = {}
	Extensions.RemoveAllChildren(self.PlayerRoot.transform)
	for k,v in pairs(LuaDaFuWongMgr.PlayerInfo) do
		local go = CUICommonDef.AddChild(self.PlayerRoot.gameObject,self.PlayerTemplate)
		go.gameObject:SetActive(true)
		go:GetComponent(typeof(CUITexture)):LoadMaterial(self:GetHeadBgPath(v.round))
		self.m_PlayerView[k] = go
	end
end

function LuaDaFuWongMiniMap:SetPlayerViewPos(id,go,isMove)
	if not LuaDaFuWongMgr.PlayerInfo then return end
	if not go then return end
	local obj = CClientObjectMgr.Inst and CClientObjectMgr.Inst:GetObject(LuaDaFuWongMgr.PlayerInfo[id].fakePlayerId)
	local landId = LuaDaFuWongMgr.PlayerInfo[id].Pos
	local landPos = LuaDaFuWongMgr.LandInfo[landId] and LuaDaFuWongMgr.LandInfo[landId].movePos
	if obj and isMove then
		local pixelPos =  Utility.WorldPos2PixelPos(obj.RO:GetPosition())
		local x,y = self:GetMapPos(pixelPos.x,pixelPos.y)
		go.gameObject:SetActive(true)
		LuaUtils.SetLocalPosition(go.transform, x, y, 0)
		LuaUtils.SetLocalRotationZ(go.transform,-1.0 * (obj.RO.Direction + 90)- 360)
	elseif LuaDaFuWongMgr.PlayerInfo[id].IsOut then
		go.gameObject:SetActive(false)
	else
		if landPos then
			local x,y = self:GetMapPos(landPos[0],landPos[1])
			go.gameObject:SetActive(true)
			LuaUtils.SetLocalPosition(go.transform, x, y, 0)
			--LuaUtils.SetLocalRotationZ(go.transform, -1.0 * ((landPos[2] + 90) - 360))
			LuaUtils.SetLocalRotationZ(go.transform,   -1.0 *(landPos[2]))
		else
			go.gameObject:SetActive(false)
		end
	end
end
function LuaDaFuWongMiniMap:InitLandDot()
	if not LuaDaFuWongMgr.LandInfo then return end
	self.m_LandView = {}
	Extensions.RemoveAllChildren(self.LandNode.transform)
	for k,v in pairs(LuaDaFuWongMgr.LandInfo) do
		local go = CUICommonDef.AddChild(self.LandNode.gameObject,self.LandTemplate)
		go.gameObject:SetActive(true)
		local x,y = self:GetMapPos(v.movePos[0],v.movePos[1])
		LuaUtils.SetLocalPosition(go.transform, x, y, 0)
		local dot = go.transform:Find("dian"):GetComponent(typeof(UITexture))
		local shadow = go.transform:Find("dian_youying").gameObject
		if v.Owner and v.Owner ~= 0 and LuaDaFuWongMgr.PlayerInfo and LuaDaFuWongMgr.PlayerInfo[v.Owner] then
			shadow.gameObject:SetActive(true)
			dot.color = NGUIText.ParseColor24(self.m_ColorList[LuaDaFuWongMgr.PlayerInfo[v.Owner].round], 0)
		else
			shadow.gameObject:SetActive(false)
			dot.color = NGUIText.ParseColor24(self.m_ColorList[0], 0)
		end
		self.m_LandView[k] = {go = go,dot = dot,shadow = shadow}
	end
end
function LuaDaFuWongMiniMap:GetMapPos(x,y)
	return (x - 7232)* -0.037,(y - 5792)  * -0.038
end
function LuaDaFuWongMiniMap:GetHeadBgPath(index)
    return SafeStringFormat3("UI/Texture/FestivalActivity/Festival_DaFuWong/Material/%s.mat",self.m_PlayerPathList[index])
end
function LuaDaFuWongMiniMap:OnStageUpdate(curStage)
	if not self.m_PlayerView then self:InitPlayerViewList() end
	if not self.m_LandView then self:InitLandDot() end
	if self.m_MoveTick then UnRegisterTick(self.m_MoveTick) self.m_MoveTick = nil end
	if self.m_PlayerView then
		for k,v in pairs(self.m_PlayerView) do
			self:SetPlayerViewPos(k,v,false)
			if curStage == EnumDaFuWengStage.RoundMove and k == LuaDaFuWongMgr.CurPlayerRound then
				self:SetPlayerViewPos(k,v,LuaDaFuWongMgr.CanMoveCameraStage and (not LuaDaFuWongMgr.CanMoveCameraStage[EnumDaFuWengStage.RoundMove]))
				self.m_MoveTick = RegisterTickWithDuration(function()
					self:SetPlayerViewPos(k,v,LuaDaFuWongMgr.CanMoveCameraStage and (not LuaDaFuWongMgr.CanMoveCameraStage[EnumDaFuWengStage.RoundMove]))
				end,500,self.m_MaxMoveTime * 1000)
			end
		end
	end
end
function LuaDaFuWongMiniMap:OnLandInfoUpdate(landId)
	if not self.m_LandView or not self.m_LandView[landId] then return end
	local ownerId = LuaDaFuWongMgr.LandInfo[landId] and LuaDaFuWongMgr.LandInfo[landId].Owner or 0
	local view = self.m_LandView[landId]
	if ownerId ~= 0 and LuaDaFuWongMgr.PlayerInfo and LuaDaFuWongMgr.PlayerInfo[ownerId] then
		view.shadow.gameObject:SetActive(true)
		view.dot.color = NGUIText.ParseColor24(self.m_ColorList[LuaDaFuWongMgr.PlayerInfo[ownerId].round], 0)
	else
		view.shadow.gameObject:SetActive(false)
		view.dot.color = NGUIText.ParseColor24(self.m_ColorList[0], 0)
	end
end
--@region UIEvent

function LuaDaFuWongMiniMap:OnBGClick()
	CUIManager.ShowUI(CLuaUIResources.DaFuWongPopMapWnd)
end

function LuaDaFuWongMiniMap:OnEnable()
	g_ScriptEvent:AddListener("OnDaFuWengStageUpdate",self,"OnStageUpdate")
	g_ScriptEvent:AddListener("OnDaFuWongLandInfoUpdate",self,"OnLandInfoUpdate")
end

function LuaDaFuWongMiniMap:OnDisable()
	g_ScriptEvent:RemoveListener("OnDaFuWengStageUpdate",self,"OnStageUpdate")
	g_ScriptEvent:RemoveListener("OnDaFuWongLandInfoUpdate",self,"OnLandInfoUpdate")
	if self.m_MoveTick then UnRegisterTick(self.m_MoveTick) self.m_MoveTick = nil end
end

--@endregion UIEvent

