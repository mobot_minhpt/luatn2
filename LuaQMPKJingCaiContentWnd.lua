-- local QnSelectableButton=import "L10.UI.QnSelectableButton"
local QnButton=import "L10.UI.QnButton"
CLuaQMPKJingCaiContentWnd = class()
RegistClassMember(CLuaQMPKJingCaiContentWnd,"m_ItemTemplate")
RegistClassMember(CLuaQMPKJingCaiContentWnd,"m_ZhanDuiNameLabel1")
RegistClassMember(CLuaQMPKJingCaiContentWnd,"m_ZhanDuiNameLabel2")
RegistClassMember(CLuaQMPKJingCaiContentWnd,"m_ZhanDuiDescLabel2")
RegistClassMember(CLuaQMPKJingCaiContentWnd,"m_ItemTemplate")
RegistClassMember(CLuaQMPKJingCaiContentWnd,"m_Grid")
RegistClassMember(CLuaQMPKJingCaiContentWnd,"m_Tick")

-- RegistClassMember(CLuaQMPKJingCaiContentWnd,"m_Buttons")


function CLuaQMPKJingCaiContentWnd:Init()
    -- self.m_Buttons={}
    self.m_ZhanDuiNameLabel1=FindChild(self.transform,"ZhanDuiNameLabel1"):GetComponent(typeof(UILabel))
    self.m_ZhanDuiNameLabel2=FindChild(self.transform,"ZhanDuiNameLabel2"):GetComponent(typeof(UILabel))
    self.m_ZhanDuiDescLabel2=FindChild(self.transform,"ZhanDuiDescLabel2"):GetComponent(typeof(UILabel))

    self.m_Grid=FindChild(self.transform,"Grid").gameObject
    self.m_ItemTemplate=FindChild(self.transform,"ItemTemplate").gameObject
    self.m_ItemTemplate:SetActive(false)

    Gac2Gas.RequestQmpkJingCaiData(CLuaQMPKMgr.m_PlayIndex)
    --定时刷新
    self.m_Tick=RegisterTick(function()
        Gac2Gas.RequestQmpkJingCaiData(CLuaQMPKMgr.m_PlayIndex)
    end,CLuaQMPKMgr.m_JingCaiRefreshDelta)

end
function CLuaQMPKJingCaiContentWnd:OnEnable()
    
    g_ScriptEvent:AddListener("ReplyQmpkJingCaiData", self, "OnReplyQmpkJingCaiData")
end
function CLuaQMPKJingCaiContentWnd:OnDisable()
    g_ScriptEvent:RemoveListener("ReplyQmpkJingCaiData", self, "OnReplyQmpkJingCaiData")
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
    end
end

function CLuaQMPKJingCaiContentWnd:InitItem(go,i,v,canClick)
    local designData = QuanMinPK_JingCai.GetData(i)
    local roundLabel=FindChild(go.transform,"RoundLabel"):GetComponent(typeof(UILabel))
    roundLabel.text=designData.Topic
    local descLabel1=FindChild(go.transform,"DescLabel1"):GetComponent(typeof(UILabel))
    descLabel1.text=designData.Choice1
    local descLabel2=FindChild(go.transform,"DescLabel2"):GetComponent(typeof(UILabel))
    descLabel2.text=designData.Choice2
    local peilvLabel1=FindChild(go.transform,"PeiLvLabel1"):GetComponent(typeof(UILabel))
    local peilv1=v[1]
    local peilv2=v[2]
    peilvLabel1.text=SafeStringFormat3(LocalString.GetString("赔率1:%s"),peilv1)
    local peilvLabel2=FindChild(go.transform,"PeiLvLabel2"):GetComponent(typeof(UILabel))
    peilvLabel2.text=SafeStringFormat3(LocalString.GetString("赔率1:%s"),peilv2)

    local shengli1=FindChild(go.transform,"Shengli1").gameObject
    local shengli2=FindChild(go.transform,"Shengli2").gameObject
    shengli1:SetActive(false)
    shengli2:SetActive(false)
    if v[3]==0 then

    elseif v[3]==1 then
        --1胜
        shengli1:SetActive(true)
    elseif v[3]==2 then
        --2胜
        shengli2:SetActive(true)
    end
    local button1=FindChild(go.transform,"Button1"):GetComponent(typeof(QnButton))
    local button2=FindChild(go.transform,"Button2"):GetComponent(typeof(QnButton))
    -- table.insert( self.m_Buttons,button1 )
    -- table.insert( self.m_Buttons,button2 )
    if canClick then
        button1.OnClick=DelegateFactory.Action_QnButton(function(b)
            CLuaQMPKMgr.m_JingCaiIndex=i
            CLuaQMPKMgr.m_SelectIndex=1
            CLuaQMPKMgr.m_Peilv=peilv1
            -- CLuaQMPKMgr.m_PlayRound=i
            CUIManager.ShowUI(CLuaUIResources.QMPKJingCaiConfirmWnd)
        end)
        button2.OnClick=DelegateFactory.Action_QnButton(function(b)
            CLuaQMPKMgr.m_JingCaiIndex=i
            CLuaQMPKMgr.m_SelectIndex=2
            CLuaQMPKMgr.m_Peilv=peilv2
            -- CLuaQMPKMgr.m_PlayRound=i
            CUIManager.ShowUI(CLuaUIResources.QMPKJingCaiConfirmWnd)
        end)
    end

end


function CLuaQMPKJingCaiContentWnd:OnReplyQmpkJingCaiData(playIndex, playRound, peilvData,zhanduiName1, zhanduiName2, daibi)
    self.m_ZhanDuiNameLabel1.text=zhanduiName1
    self.m_ZhanDuiNameLabel2.text=zhanduiName2
    local isHide = zhanduiName2 == ""
    self.m_ZhanDuiDescLabel2.gameObject:SetActive(isHide)
    self.m_ZhanDuiNameLabel2.gameObject:SetActive(not isHide)

    local lookup =  {1, 1, 1, 2, 3, 4, 4, 5}
    CUICommonDef.ClearTransform(self.m_Grid.transform)
    -- playRound=2

    for i,v in ipairs(peilvData) do
        if lookup[i] and lookup[i]>playRound then
            local go=NGUITools.AddChild(self.m_Grid,self.m_ItemTemplate)
            go:SetActive(true)
            self:InitItem(go,i,v,true)
        end
    end
    for i,v in ipairs(peilvData) do
        if lookup[i] and lookup[i]<=playRound then
            local go=NGUITools.AddChild(self.m_Grid,self.m_ItemTemplate)
            go:SetActive(true)
            self:InitItem(go,i,v,false)
            CUICommonDef.SetActive(go,false,true)
        end
    end
    self.m_Grid:GetComponent(typeof(UIGrid)):Reposition()
end

function CLuaQMPKJingCaiContentWnd:OnDestroy()
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
    end
end
return CLuaQMPKJingCaiContentWnd