local Profession  = import "L10.Game.Profession"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local EShareType  = import "L10.UI.EShareType"
local Animation   = import "UnityEngine.Animation"

LuaDuanWu2023PVPVEResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaDuanWu2023PVPVEResultWnd, "getScore", "GetScore", UILabel)
RegistChildComponent(LuaDuanWu2023PVPVEResultWnd, "scoreLimit", "ScoreLimit", UILabel)
RegistChildComponent(LuaDuanWu2023PVPVEResultWnd, "againButton", "AgainButton", GameObject)
RegistChildComponent(LuaDuanWu2023PVPVEResultWnd, "shareButton", "ShareButton", GameObject)
RegistChildComponent(LuaDuanWu2023PVPVEResultWnd, "playerInfo", "PlayerInfo", GameObject)
RegistChildComponent(LuaDuanWu2023PVPVEResultWnd, "logo", "Logo", GameObject)
RegistChildComponent(LuaDuanWu2023PVPVEResultWnd, "closeButton", "CloseButton", GameObject)
--@endregion RegistChildComponent end

RegistClassMember(LuaDuanWu2023PVPVEResultWnd, "myComponents")
RegistClassMember(LuaDuanWu2023PVPVEResultWnd, "otherComponents")

function LuaDuanWu2023PVPVEResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	UIEventListener.Get(self.againButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnAgainButtonClick()
	end)

	UIEventListener.Get(self.shareButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareButtonClick()
	end)
    --@endregion EventBind end
	self.myComponents = self:GetComponents(self.transform:Find("Info/Me"))
	self.otherComponents = self:GetComponents(self.transform:Find("Info/Other"))
	self.playerInfo:SetActive(false)
	self.logo:SetActive(false)
end

function LuaDuanWu2023PVPVEResultWnd:GetComponents(root)
	local template = root:Find("Template").gameObject
	template:SetActive(false)
	return {
		score = root:Find("Score"):GetComponent(typeof(UILabel)),
		template = template,
		table = root:Find("Grid"):GetComponent(typeof(UIGrid)),
	}
end

function LuaDuanWu2023PVPVEResultWnd:Init()
	local info = LuaDuanWu2023Mgr.pvpveResultInfo
	self.myComponents.score.text = info.myTeamScore
	self.otherComponents.score.text = info.otherTeamScore

	self:InitTeam(self.myComponents.template, self.myComponents.table, info.myTeamInfo)
	self:InitTeam(self.otherComponents.template, self.otherComponents.table, info.otherTeamInfo)

	self.getScore.text = info.getScore
	self.scoreLimit.text = SafeStringFormat3("%d/%d", info.dailyScore, Duanwu2023_PVPVESetting.GetData().ScoreLimit)

	local animation = self.transform:GetComponent(typeof(Animation))
	if info.myTeamScore == info.otherTeamScore then
		animation:Play("duanwu2023pvpveresultwnd_ping")
	elseif info.meIsWinner then
		animation:Play("duanwu2023pvpveresultwnd_win")
	else
		animation:Play("duanwu2023pvpveresultwnd_fail")
	end
end

function LuaDuanWu2023PVPVEResultWnd:InitTeam(template, table, list)
	Extensions.RemoveAllChildren(table.transform)
	for i, data in ipairs(list) do
		local child = NGUITools.AddChild(table.gameObject, template)
		child:SetActive(true)

		local tf = child.transform
		local name = tf:Find("Name"):GetComponent(typeof(UILabel))
		name.text = data.name
		local contribution = tf:Find("Contribution"):GetComponent(typeof(UILabel))
		contribution.text = data.contribution
		local score = tf:Find("Score"):GetComponent(typeof(UILabel))
		score.text = data.score
		local job = tf:Find("Job"):GetComponent(typeof(UISprite))
		job.spriteName = Profession.GetIconByNumber(data.class)
		local server = tf:Find("Server"):GetComponent(typeof(UILabel))
		server.text = data.serverName
		local myBg = tf:Find("MyBg").gameObject
		myBg:SetActive(false)

		if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == data.playerId then
			local color = NGUIText.ParseColor24("00FF60", 0)
			name.color = color
			score.color = color
			server.color = color
			contribution.color = color
			myBg:SetActive(true)
		end
	end
	table:Reposition()
end

function LuaDuanWu2023PVPVEResultWnd:OnDestroy()
	LuaDuanWu2023Mgr.pvpveResultInfo = {}
end

--@region UIEvent

function LuaDuanWu2023PVPVEResultWnd:OnAgainButtonClick()
	CUIManager.ShowUI(CLuaUIResources.DuanWu2023PVPVEEnterWnd)
	CUIManager.CloseUI(CLuaUIResources.DuanWu2023PVPVEResultWnd)
end

function LuaDuanWu2023PVPVEResultWnd:OnShareButtonClick()
	self.playerInfo:SetActive(true)
	self.logo:SetActive(true)
	self.closeButton:SetActive(false)
	self.shareButton:SetActive(false)
	self.againButton:SetActive(false)

    CUIManager.SetUITop(CLuaUIResources.DuanWu2023PVPVEEnterWnd)
    CUICommonDef.CaptureScreen("screenshot", false, false, nil,
        DelegateFactory.Action_string_bytes(function(fullPath, jpgBytes)
			self.playerInfo:SetActive(false)
			self.logo:SetActive(false)
			self.closeButton:SetActive(true)
			self.shareButton:SetActive(true)
			self.againButton:SetActive(true)

            CUIManager.ResetUITop(CLuaUIResources.DuanWu2023PVPVEEnterWnd)
            ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, fullPath)
        end), false)
end

--@endregion UIEvent
