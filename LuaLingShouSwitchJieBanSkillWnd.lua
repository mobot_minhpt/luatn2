local MessageWndManager = import "L10.UI.MessageWndManager"
local CLingShouMgr = import "L10.Game.CLingShouMgr"
local CSkillInfoMgr=import "L10.UI.CSkillInfoMgr"
local Collider=import "UnityEngine.Collider"
local CScene=import "L10.Game.CScene"
local CUITexture=import "L10.UI.CUITexture"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local QnTableView=import "L10.UI.QnTableView"
local UIToggle=import "UIToggle"
local CCurentMoneyCtrl=import "L10.UI.CCurentMoneyCtrl"
local CShopMallMgr=import "L10.UI.CShopMallMgr"

CLuaLingShouSwitchJieBanSkillWnd = class()
RegistClassMember(CLuaLingShouSwitchJieBanSkillWnd,"m_TableView")
RegistClassMember(CLuaLingShouSwitchJieBanSkillWnd,"m_AssistLingShouId")
RegistClassMember(CLuaLingShouSwitchJieBanSkillWnd,"m_FuTiLingShouId")
RegistClassMember(CLuaLingShouSwitchJieBanSkillWnd,"m_BattleLingShouId")
RegistClassMember(CLuaLingShouSwitchJieBanSkillWnd,"m_JieBanLingShouId")
RegistClassMember(CLuaLingShouSwitchJieBanSkillWnd,"m_SelectedLingShouInfos")
RegistClassMember(CLuaLingShouSwitchJieBanSkillWnd,"m_LingShouOverviewInfos")
RegistClassMember(CLuaLingShouSwitchJieBanSkillWnd,"m_SkillItem")
RegistClassMember(CLuaLingShouSwitchJieBanSkillWnd,"moneyCtrl")

function CLuaLingShouSwitchJieBanSkillWnd:Awake()
    self.m_SelectedLingShouInfos = {}
    self.m_LingShouOverviewInfos = {}

    self.moneyCtrl = FindChild(self.transform,"QnCostAndOwnMoney"):GetComponent(typeof(CCurentMoneyCtrl))
    self.moneyCtrl:SetCost(0)

    self.m_SkillItem = FindChild(self.transform,"SkillItem").gameObject
    self.m_SkillItem:SetActive(false)
    self.m_TableView = self.transform:Find("SortTable"):GetComponent(typeof(QnTableView))

    local tipButton = FindChild(self.transform,"TipBtn").gameObject
    UIEventListener.Get(tipButton).onClick = DelegateFactory.VoidDelegate(function(go)
        g_MessageMgr:ShowMessage("JieBanSkill_ZhuanYi_ReadMe")
    end)

    local switchButton = FindChild(self.transform,"SwitchBtn").gameObject
    UIEventListener.Get(switchButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:SwitchSkill()
    end)

    local lingshou1 = FindChild(self.transform,"LingShou1").gameObject
    local lingshou2 = FindChild(self.transform,"LingShou2").gameObject

    local function initLingShou(transform)
        local slots = transform:Find("Slots").gameObject
        for i=1,8 do
            local go = NGUITools.AddChild(slots,self.m_SkillItem)
            go:SetActive(true)
            self:InitSkillSlot(go.transform,0)
        end
    end
    initLingShou(lingshou1.transform)
    initLingShou(lingshou2.transform)

    -- UIEventListener.Get(lingshou1).onClick = DelegateFactory.VoidDelegate(function(go)
        
    -- end)
    -- UIEventListener.Get(lingshou2).onClick = DelegateFactory.VoidDelegate(function(go)

    -- end)

    self.m_SelectedLingShouInfos[1] = {
        go = lingshou1,
        toggle = lingshou1:GetComponent(typeof(UIToggle)),
        id = nil,
    }
    self.m_SelectedLingShouInfos[2] = {
        go = lingshou2,
        toggle = lingshou2:GetComponent(typeof(UIToggle)),
        id = nil,
    }
end

function CLuaLingShouSwitchJieBanSkillWnd:InitLingShouIds()
    if CClientMainPlayer.Inst then
        self.m_AssistLingShouId = CClientMainPlayer.Inst.BasicProp.AssistLingShouId
        self.m_FuTiLingShouId = CClientMainPlayer.Inst.ItemProp.FuTiLingShouId
        self.m_BattleLingShouId = CClientMainPlayer.Inst.BasicProp.LastLingShouId
        self.m_JieBanLingShouId = CClientMainPlayer.Inst.ItemProp.BindedPartenerLingShouId
    end
end

function CLuaLingShouSwitchJieBanSkillWnd:Init()

    self.m_TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function() 
            return #self.m_LingShouOverviewInfos
        end,
        function(item,row) 
            self:InitItem(item.transform,self.m_LingShouOverviewInfos[row+1])
        end
    )
    self.m_TableView.OnSelectAtRow = DelegateFactory.Action_int(function (row) 
        self:OnSelectedAtRow(row)
    end)
    CLingShouMgr.Inst:RequestLingShouList()
end

function CLuaLingShouSwitchJieBanSkillWnd:OnEnable()
    g_ScriptEvent:AddListener("GetAllLingShouOverview", self, "OnGetAllLingShouOverview")
    g_ScriptEvent:AddListener("GetLingShouDetails", self, "GetLingShouDetails")
    g_ScriptEvent:AddListener("SyncLingShouPartnerInfo", self, "OnSyncLingShouPartnerInfo")
    g_ScriptEvent:AddListener("ExchangeLingShouPartnerSkillSuccess", self, "OnExchangeLingShouPartnerSkillSuccess")
end



function CLuaLingShouSwitchJieBanSkillWnd:OnDisable()
    g_ScriptEvent:RemoveListener("GetAllLingShouOverview", self, "OnGetAllLingShouOverview")
    g_ScriptEvent:RemoveListener("GetLingShouDetails", self, "GetLingShouDetails")
    g_ScriptEvent:RemoveListener("SyncLingShouPartnerInfo", self, "OnSyncLingShouPartnerInfo")
    g_ScriptEvent:RemoveListener("ExchangeLingShouPartnerSkillSuccess", self, "OnExchangeLingShouPartnerSkillSuccess")
end
function CLuaLingShouSwitchJieBanSkillWnd:OnGetAllLingShouOverview()
    self:InitLingShouIds()
    self.m_LingShouOverviewInfos = {}
    for i=1,CLingShouMgr.Inst:GetLingShouOverviewList().Count do
        local overview = CLingShouMgr.Inst:GetLingShouOverviewList()[i-1]
        table.insert( self.m_LingShouOverviewInfos, {
            id = overview.id,
            name = overview.name,
            level = overview.level,
            evolveGrade = overview.evolveGrade,
            templateId = overview.templateId,
        } )
    end
    self.m_TableView:ReloadData(true, false)
    self.m_TableView:SetSelectRow(-1, true)--不选择
end

function CLuaLingShouSwitchJieBanSkillWnd:InitItem(transform,data)
    local nameLabel = transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local descLabel = transform:Find("DescLabel"):GetComponent(typeof(UILabel))
    local selectedSprite = transform:Find("SelectedSprite").gameObject
    selectedSprite.gameObject:SetActive(false)
    local markSprite = transform:Find("MarkSprite"):GetComponent(typeof(UISprite))
    local icon = transform:Find("Icon"):GetComponent(typeof(CUITexture))

    local id = data.id
    if self.m_AssistLingShouId == id then
        markSprite.enabled=true
        markSprite.spriteName = "common_support"
    elseif self.m_FuTiLingShouId == id then
        markSprite.enabled=true
        markSprite.spriteName = "common_hua"
    elseif self.m_BattleLingShouId == id then
        markSprite.enabled=true
        markSprite.spriteName = "common_fight"
    elseif self.m_JieBanLingShouId == id then
        markSprite.enabled=true
        markSprite.spriteName = "common_ban"
    else
        markSprite.enabled=false
    end

    if CScene.MainScene and CScene.MainScene.LingShouMode==1 then
        if self.m_AssistLingShouId == id or self.m_BattleLingShouId == id then
            CUICommonDef.SetActive(markSprite.gameObject, false)
        end
    end
    local templateData = LingShou_LingShou.GetData(data.templateId)
    icon:LoadNPCPortrait(templateData.Portrait)

    descLabel.text = CLuaLingShouMgr.GetEvolveGradeDesc(data.evolveGrade)..SafeStringFormat3(LocalString.GetString("%d级"),data.level)
    nameLabel.text = data.name

end
function CLuaLingShouSwitchJieBanSkillWnd:OnSelectedAtRow(row)
    local lingshou1 = self.m_SelectedLingShouInfos[1]
    local lingshou2 = self.m_SelectedLingShouInfos[2]

    local id = self.m_LingShouOverviewInfos[row+1].id
    if id == lingshou1.id or id == lingshou2.id then
        g_MessageMgr:ShowMessage("LING_SHOU_REPLACE_LINGSHOU_EXISTS")
        return
    end

    if lingshou1.toggle.value then
        if lingshou1.id~=id then
            lingshou1.id = id
            self:InitLingShouInfo(lingshou1.go.transform,self.m_LingShouOverviewInfos[row+1])
        end

        if lingshou2.id==nil then
            lingshou2.toggle.value = true
        end
    elseif lingshou2.toggle.value then
        if lingshou2.id~=id then
            lingshou2.id = id
            self:InitLingShouInfo(lingshou2.go.transform,self.m_LingShouOverviewInfos[row+1])
        end
        if lingshou1.id==nil then
            lingshou1.toggle.value = true
        end
    end

    CLingShouMgr.Inst:RequestLingShouDetails(id)

    --刷新列表的选择状态
    for i=1,self.m_TableView.m_ItemList.Count do
        local item = self.m_TableView.m_ItemList[i-1]
        local selectedSprite = item.transform:Find("SelectedSprite").gameObject
        local id = self.m_LingShouOverviewInfos[i].id
        if id == self.m_SelectedLingShouInfos[1].id or id == self.m_SelectedLingShouInfos[2].id then
            selectedSprite:SetActive(true)
        else
            selectedSprite:SetActive(false)
        end
    end

end

function CLuaLingShouSwitchJieBanSkillWnd:GetLingShouSkillCount(lingshou)
    local count = 0
    local partnerInfo = lingshou.data.Props.PartnerInfo
    local skillList=partnerInfo.PartnerNormalSkills
    for i=1,5 do 
        if skillList[i]>0 then
            count = count+1
        end
    end
    local skillList=partnerInfo.PartnerProfessionalSkills
    for i=1,3 do 
        if skillList[i]>0 then
            count = count+1
        end
    end
    return count
end
function CLuaLingShouSwitchJieBanSkillWnd:SwitchSkill()
    local id1,id2 = self.m_SelectedLingShouInfos[1].id,self.m_SelectedLingShouInfos[2].id
    local lookup = {
        [self.m_AssistLingShouId]=true,
        [self.m_FuTiLingShouId]=true,
        [self.m_BattleLingShouId]=true,
        [self.m_JieBanLingShouId]=true,
    }
    if lookup[id1] or lookup[id2] then
        g_MessageMgr:ShowMessage("JieBanSkill_ZhuanYi_Fail_NotInIdle")
        return 
    end

    local lingshou1 = CLingShouMgr.Inst:GetLingShouDetails(id1)
    local lingshou2 = CLingShouMgr.Inst:GetLingShouDetails(id2)
    if lingshou1 and lingshou2 then
        local count1 = self:GetLingShouSkillCount(lingshou1)
        local count2 = self:GetLingShouSkillCount(lingshou2)
        if count1==0 and count2==0 then
            g_MessageMgr:ShowMessage("JieBanSkill_ZhuanYi_Fail_NoSkill")
            return 
        end
    end
    if not self.moneyCtrl.moneyEnough then
        CShopMallMgr.ShowChargeWnd()
        return
    end
    if id1 and id2 then
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("JieBanSkill_ZhuanYi_Confirm",self.moneyCtrl:GetCost()), DelegateFactory.Action(function () 
            --播放特效
            -- local fx1 = FindChild(self.transform,"Fx1"):GetComponent(typeof(CUIFx))
            -- local fx2 = FindChild(self.transform,"Fx2"):GetComponent(typeof(CUIFx))
            -- fx1:DestroyFx()
            -- fx2:DestroyFx()
            -- fx1:LoadFx("Fx/UI/Prefab/UI_huanranyixin_cizhui.prefab")
            -- fx2:LoadFx("Fx/UI/Prefab/UI_huanranyixin_cizhui.prefab")
            Gac2Gas.RequestExchangeLingShouPartnerSkill(id1,id2)
        end), nil, nil, nil, false)
        
    else
        g_MessageMgr:ShowMessage("JieBanSkill_ZhuanYi_Fail_NotEnoughLS")
    end
end

function CLuaLingShouSwitchJieBanSkillWnd:InitLingShouInfo(transform,data)
    local nameLabel = transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local icon = transform:Find("Icon"):GetComponent(typeof(CUITexture))
    if data then
        nameLabel.text= data.name
        local templateData = LingShou_LingShou.GetData(data.templateId)
        icon:LoadNPCPortrait(templateData.Portrait)
    else
        nameLabel.text=nil
        icon:Clear()
    end
    local slots = transform:Find("Slots")
    for i=1,8 do
        self:InitSkillSlot(slots:GetChild(i-1),0)
    end
end

function CLuaLingShouSwitchJieBanSkillWnd:GetLingShouDetails(args)
    local lingshouId=args[0]
    local details=args[1]

    local target= nil
    for i,v in ipairs(self.m_SelectedLingShouInfos) do
        if v.id == lingshouId then
            target = v.go
        end
    end
    if target then
        --初始化技能
        local slots = target.transform:Find("Slots")

        local partnerInfo = details.data.Props.PartnerInfo
        local normalSkills = partnerInfo.PartnerNormalSkills
        local professionalSkills = partnerInfo.PartnerProfessionalSkills
        for i=1,5 do
            self:InitSkillSlot(slots:GetChild(i-1),normalSkills[i])
        end
        for i=1,3 do
            self:InitSkillSlot(slots:GetChild(i-1+5),professionalSkills[i])
        end
    end
    local id1,id2 = self.m_SelectedLingShouInfos[1].id,self.m_SelectedLingShouInfos[2].id
    if id1 and id2 then
        --刷新价格
        -- local  function formula(e) return e*15+600 end
        local formula = AllFormulas.Action_Formula[740].Formula
        local function count_all_partner_skilllv(lingshou)
            local count = 0
            local partnerInfo = lingshou.data.Props.PartnerInfo
            local skillList=partnerInfo.PartnerNormalSkills
            for i=1,5 do 
                count = count+skillList[i]%100
            end
            local skillList=partnerInfo.PartnerProfessionalSkills
            for i=1,3 do 
                count = count+skillList[i]%100
            end
            return count
        end
        local lingshou1 = CLingShouMgr.Inst:GetLingShouDetails(id1)
        local lingshou2 = CLingShouMgr.Inst:GetLingShouDetails(id2)
        if lingshou1 and lingshou2 then
            local price1 = formula(nil,nil,{count_all_partner_skilllv(lingshou1)})
            local price2 = formula(nil,nil,{count_all_partner_skilllv(lingshou2)})
        
            local price = math.max(price1, price2)
            self.moneyCtrl:SetCost(price)
        else
            self.moneyCtrl:SetCost(0)
        end
    end
end


function CLuaLingShouSwitchJieBanSkillWnd:InitSkillSlot(transform, skillId)
    local icon = transform:Find("HasSkill/Texture"):GetComponent(typeof(CUITexture))
    local levelLabel = transform:Find("HasSkill/LevelLabel"):GetComponent(typeof(UILabel))
    local toggle = transform:GetComponent(typeof(UIToggle))
    local hasSkillTf = transform:Find("HasSkill")

    local template = Skill_AllSkills.GetData(skillId)
    if template ~= nil then
        hasSkillTf.gameObject:SetActive(true)
        icon:LoadSkillIcon(template.SkillIcon)
        levelLabel.text = "Lv." .. template.Level

        local col = transform:GetComponent(typeof(Collider))
        if col ~= nil then
            col.enabled = true
        end
    else
        local col = transform:GetComponent(typeof(Collider))
        if col ~= nil then
            col.enabled = false
        end
        hasSkillTf.gameObject:SetActive(false)
        icon.material = nil
        levelLabel.text = ""
    end

    if skillId>0 then
        UIEventListener.Get(transform.gameObject).onClick=DelegateFactory.VoidDelegate(function(go)
            if skillId>0 then
                CSkillInfoMgr.ShowSkillInfoWnd(skillId,true,0,0,nil)
            end
        end)
    else
        UIEventListener.Get(transform.gameObject).onClick=nil
    end
end

-- UI_huanranyixin_cizhui

function CLuaLingShouSwitchJieBanSkillWnd:OnSyncLingShouPartnerInfo(lingshouId,partnerInfo)
    local target= nil
    for i,v in ipairs(self.m_SelectedLingShouInfos) do
        if v.id == lingshouId then
            target = v.go
        end
    end
    if target then
        --初始化技能
        local slots = target.transform:Find("Slots")

        -- local partnerInfo = details.data.Props.PartnerInfo
        local normalSkills = partnerInfo.PartnerNormalSkills
        local professionalSkills = partnerInfo.PartnerProfessionalSkills
        for i=1,5 do
            self:InitSkillSlot(slots:GetChild(i-1),normalSkills[i])
        end
        for i=1,3 do
            self:InitSkillSlot(slots:GetChild(i-1+5),professionalSkills[i])
        end
    end
end

function CLuaLingShouSwitchJieBanSkillWnd:OnExchangeLingShouPartnerSkillSuccess(lingshouId1,lingshouId2)
    self.moneyCtrl:SetCost(0)
    local lingshou1 = self.m_SelectedLingShouInfos[1]
    local lingshou2 = self.m_SelectedLingShouInfos[2]
    lingshou1.id=nil
    lingshou2.id=nil

    self:InitLingShouInfo(lingshou1.go.transform,nil)
    self:InitLingShouInfo(lingshou2.go.transform,nil)

    --已添加的标记去掉
    --刷新列表的选择状态
    for i=1,self.m_TableView.m_ItemList.Count do
        local item = self.m_TableView.m_ItemList[i-1]
        local selectedSprite = item.transform:Find("SelectedSprite").gameObject
        selectedSprite:SetActive(false)
    end
end
