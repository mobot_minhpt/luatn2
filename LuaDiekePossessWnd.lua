local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CUITexture = import "L10.UI.CUITexture"
local Profession = import "L10.Game.Profession"
local UITable = import "UITable"

CLuaDiekePossessWnd = class()

function CLuaDiekePossessWnd:Awake()
    UIEventListener.Get(self.transform:Find("Anchor/ClearAllButton").gameObject).onClick = LuaUtils.VoidDelegate(function()
        Gac2Gas.DieKeForceUnPossessAll()
        CUIManager.CloseUI(CIndirectUIResources.DiekePossessWnd)
        return
    end)
end

function CLuaDiekePossessWnd:Init()
    if not CClientMainPlayer.Inst or CClientMainPlayer.Inst.AppearanceProp.PositivePossessNum <= 0 then
        CUIManager.CloseUI(CIndirectUIResources.DiekePossessWnd)
        return
    end
    local tableRootObj = self.transform:Find("Anchor/MemberList/Scrollview/Table").gameObject
    Extensions.RemoveAllChildren(tableRootObj.transform)
    local templateObj = self.transform:Find("Anchor/MemberList/Template").gameObject
    templateObj:SetActive(false)
    for i = 0, CClientMainPlayer.Inst.AppearanceProp.PositivePossessByOther.Length - 1 do
        local id = CClientMainPlayer.Inst.AppearanceProp.PositivePossessByOther[i]
        if id > 0 then
            local clientObj = CClientObjectMgr.Inst:GetObject(id)
            if clientObj then
                local obj = NGUITools.AddChild(tableRootObj, templateObj)
                self:InitItem(obj, clientObj)
            end
        end
    end
    tableRootObj:GetComponent(typeof(UITable)):Reposition()
end

function CLuaDiekePossessWnd:InitItem(obj, clientObj)
    obj:SetActive(true)
    local appearProp = clientObj.AppearanceProp
    obj.transform:Find("Class"):GetComponent(typeof(UISprite)).spriteName = Profession.GetIcon(appearProp.Class)
    obj.transform:Find("Portrait"):GetComponent(typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(appearProp.Class, appearProp.Gender, - 1), false)
    obj.transform:Find("Name"):GetComponent(typeof(UILabel)).text = clientObj.Name
    obj.transform:Find("Level"):GetComponent(typeof(UILabel)).text = "lv."..CUICommonDef.GetColoredLevelString(appearProp.FeiShengPlayXianFanStatus > 0, clientObj.Level, Color.white)
    UIEventListener.Get(obj.transform:Find("ClearAllButton").gameObject).onClick = LuaUtils.VoidDelegate(function()
        Gac2Gas.DieKeForceUnPossess(clientObj.EngineId)
    end)
end

function CLuaDiekePossessWnd:DieKeUnPossessByOther(args)
    if args[4] == CClientMainPlayer.Inst then
        self:Init()
    end
end

function CLuaDiekePossessWnd:OnEnable()
    g_ScriptEvent:AddListener("DieKeUnPossessByOther", self, "DieKeUnPossessByOther")
end

function CLuaDiekePossessWnd:OnDisable()
    g_ScriptEvent:RemoveListener("DieKeUnPossessByOther", self, "DieKeUnPossessByOther")
end
