local CShopMallMgr = import "L10.UI.CShopMallMgr"

LuaShuJia2023PassportBuyAll = class()

RegistClassMember(LuaShuJia2023PassportBuyAll, "passId")

function LuaShuJia2023PassportBuyAll:Awake()
    self.passId = LuaCommonPassportMgr.passId
    local settingData = Pass_Setting.GetData(self.passId)
    local clientSettingData = Pass_ClientSetting.GetData(self.passId)
    local names = clientSettingData.BattlePassNames
    local originVipCost = clientSettingData.OriginVipCost
    self.transform:Find("Hint"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("%s和%s一起购买更优惠哦"), names[1], names[2])
    local price = tonumber(settingData.OnSalesVipCost)
    self.transform:Find("Price"):GetComponent(typeof(UILabel)).text = LocalString.GetString("原价 ")..(originVipCost[0] + originVipCost[1])
    self.transform:Find("Btn/Label"):GetComponent(typeof(UILabel)).text = price
    UIEventListener.Get(self.transform:Find("Btn").gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:BuyAll()
    end)
end

function LuaShuJia2023PassportBuyAll:OnEnable()
    self:OnCommonPassortUnlockVipSuccess(self.passId, LuaCommonPassportMgr:GetIsVIP1Open(self.passId), LuaCommonPassportMgr:GetIsVIP2Open(self.passId))
    g_ScriptEvent:AddListener("CommonPassortUnlockVipSuccess", self, "OnCommonPassortUnlockVipSuccess")
end

function LuaShuJia2023PassportBuyAll:OnDisable()
    g_ScriptEvent:RemoveListener("CommonPassortUnlockVipSuccess", self, "OnCommonPassortUnlockVipSuccess")
end

function LuaShuJia2023PassportBuyAll:OnCommonPassortUnlockVipSuccess(passId, vip1Unlock, vip2Unlock)
    if passId == self.passId and (vip1Unlock or vip2Unlock) then
        self.gameObject:SetActive(false)
    end
end

function LuaShuJia2023PassportBuyAll:BuyAll()
    local mainPlayer = CClientMainPlayer.Inst
    if not mainPlayer then return end
    local needJade = tonumber(Pass_Setting.GetData(LuaCommonPassportMgr.passId).OnSalesVipCost)
    if mainPlayer.Jade < needJade then
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("JADE_PAY_NOTICE"), function()
            CShopMallMgr.ShowChargeWnd()
        end, nil, LocalString.GetString("前往"), nil, false)
    else
        local names = Pass_ClientSetting.GetData(LuaCommonPassportMgr.passId).BattlePassNames
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("COMMON_PASSPORT_VIP_UNLOCK_CONFIRM", needJade, names[1]..LocalString.GetString("和")..names[2]), function ()
            Gac2Gas.RequestBuyPassVipByJade(LuaCommonPassportMgr.passId, 0)
        end, nil, LocalString.GetString("购买"), nil, false)
    end
end
