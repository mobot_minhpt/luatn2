local GameObject = import "UnityEngine.GameObject"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local UISprite = import "UISprite"
local LocalString = import "LocalString"
local Profession = import "L10.Game.Profession"
local EnumClass = import "L10.Game.EnumClass"
local UIScrollView = import "UIScrollView"
local UITable = import "UITable"
local UIGrid = import "UIGrid"
local CItemMgr = import "L10.Game.CItemMgr"

LuaDoubleOneBonusLotteryRankWnd = class()
RegistChildComponent(LuaDoubleOneBonusLotteryRankWnd,"closeBtn", GameObject)
RegistChildComponent(LuaDoubleOneBonusLotteryRankWnd,"timeTemplate", GameObject)
RegistChildComponent(LuaDoubleOneBonusLotteryRankWnd,"timeScrollView", UIScrollView)
RegistChildComponent(LuaDoubleOneBonusLotteryRankWnd,"timeTable", UIGrid)
RegistChildComponent(LuaDoubleOneBonusLotteryRankWnd,"rankTemplate", GameObject)
RegistChildComponent(LuaDoubleOneBonusLotteryRankWnd,"rankScrollView", UIScrollView)
RegistChildComponent(LuaDoubleOneBonusLotteryRankWnd,"rankTable", UITable)
RegistChildComponent(LuaDoubleOneBonusLotteryRankWnd,"selfInfoNode", GameObject)

RegistClassMember(LuaDoubleOneBonusLotteryRankWnd, "highLightNode")
RegistClassMember(LuaDoubleOneBonusLotteryRankWnd, "bonusName")
RegistClassMember(LuaDoubleOneBonusLotteryRankWnd, "bonusRankName")
RegistClassMember(LuaDoubleOneBonusLotteryRankWnd, "bonusRankTable")

function LuaDoubleOneBonusLotteryRankWnd:Close()
	CUIManager.CloseUI("DoubleOneBonusLotteryRankWnd")
end

function LuaDoubleOneBonusLotteryRankWnd:OnEnable()
	g_ScriptEvent:AddListener("DoubleOneBonusRankInfoUpdate", self, "InitInfo")
end

function LuaDoubleOneBonusLotteryRankWnd:OnDisable()
	g_ScriptEvent:RemoveListener("DoubleOneBonusRankInfoUpdate", self, "InitInfo")
end

function LuaDoubleOneBonusLotteryRankWnd:Init()
	self.bonusRankTable = {}
	self.timeTemplate:SetActive(false)
	self.rankTemplate:SetActive(false)
	local onCloseClick = function(go)
		self:Close()
	end
	CommonDefs.AddOnClickListener(self.closeBtn,DelegateFactory.Action_GameObject(onCloseClick),false)

	local nodeRankPrize = {
		g_LuaUtil:StrSplit(SinglesDay2019_LotterSetting.GetData().Rank1Prize,","),
		g_LuaUtil:StrSplit(SinglesDay2019_LotterSetting.GetData().Rank2Prize,","),
		g_LuaUtil:StrSplit(SinglesDay2019_LotterSetting.GetData().Rank3Prize,","),
		g_LuaUtil:StrSplit(SinglesDay2019_LotterSetting.GetData().Rank4Prize,","),
	}

	local item = CItemMgr.Inst:GetItemTemplate(nodeRankPrize[4][1])

	self.bonusName = {
		nodeRankPrize[1][2]..LocalString.GetString('灵玉'),
		nodeRankPrize[2][2]..LocalString.GetString('灵玉'),
		nodeRankPrize[3][2]..LocalString.GetString('灵玉'),
		item.Name,
	}

	self.bonusRankName = {
		LocalString.GetString('特等奖'),
		LocalString.GetString('一等奖'),
		LocalString.GetString('二等奖'),
		LocalString.GetString('三等奖')
	}
	self:InitTab()
end

function LuaDoubleOneBonusLotteryRankWnd:InitTab()
	if not LuaDoubleOne2019Mgr.LotteryRankInfo then
		self:Close()
		return
	end

	CUICommonDef.ClearTransform(self.timeTable.transform)
	for i,v in ipairs(LuaDoubleOne2019Mgr.LotteryTotalRankTime) do
		local node = NGUITools.AddChild(self.timeTable.gameObject,self.timeTemplate)
		node:SetActive(true)
		local hightNode = node.transform:Find('highlight').gameObject
		hightNode:SetActive(false)
		local timeString = os.date(LocalString.GetString('%m月%d日'),v)
		node.transform:Find('Label'):GetComponent(typeof(UILabel)).text = timeString
		local onTabClick = function(go)
			if self.highLightNode then
				self.highLightNode:SetActive(false)
			end
			hightNode:SetActive(true)
			self.highLightNode = hightNode
			LuaDoubleOne2019Mgr.LotteryRankTime = v
			if self.bonusRankTable and self.bonusRankTable[LuaDoubleOne2019Mgr.LotteryRankTime] then
				self:InitInfoByData(self.bonusRankTable[LuaDoubleOne2019Mgr.LotteryRankTime])
			else
				Gac2Gas.QuerySinglesDayLotteryResult(LuaDoubleOne2019Mgr.LotteryRankTime)
			end
		end
		CommonDefs.AddOnClickListener(node,DelegateFactory.Action_GameObject(onTabClick),false)

		if LuaDoubleOne2019Mgr.LotteryRankTime and LuaDoubleOne2019Mgr.LotteryRankTime == v then
			hightNode:SetActive(true)
			self.highLightNode = hightNode
			self:InitInfo()
		end
	end
	self.timeTable:Reposition()
	self.timeScrollView:ResetPosition()
end

function LuaDoubleOneBonusLotteryRankWnd:InitInfoByData(data)
	if self.bonusRankTable and not self.bonusRankTable[LuaDoubleOne2019Mgr.LotteryRankTime] then
		self.bonusRankTable[LuaDoubleOne2019Mgr.LotteryRankTime] = data
	end
	CUICommonDef.ClearTransform(self.rankTable.transform)
	if not CClientMainPlayer.Inst then
		self:Close()
		return
	end

	local selfId = CClientMainPlayer.Inst.Id
	local selfInfo = {0,CClientMainPlayer.Inst:GetMyServerName(),selfId,CClientMainPlayer.Inst.Name,CClientMainPlayer.Inst.Class}
	for i,v in ipairs(data) do
		local node = NGUITools.AddChild(self.rankTable.gameObject,self.rankTemplate)
		node:SetActive(true)
		self:InitNodeInfo(node,v)
		if v[3] == selfId	then
			selfInfo[1] = v[1]
		end
	end
	self:InitNodeInfo(self.selfInfoNode,selfInfo,true)

	self.rankTable:Reposition()
	self.rankScrollView:ResetPosition()
end

function LuaDoubleOneBonusLotteryRankWnd:InitInfo()
	self:InitInfoByData(LuaDoubleOne2019Mgr.LotteryRankInfo)
end

function LuaDoubleOneBonusLotteryRankWnd:InitNodeInfo(node,data,selfSign)
	node.transform:Find('name'):GetComponent(typeof(UILabel)).text = data[4]
	node.transform:Find('server'):GetComponent(typeof(UILabel)).text = data[2]
	if data[1] > 0 then
		node.transform:Find('reward'):GetComponent(typeof(UILabel)).text = self.bonusRankName[data[1]]
		node.transform:Find('bonus'):GetComponent(typeof(UILabel)).text = self.bonusName[data[1]]
	else
		node.transform:Find('reward'):GetComponent(typeof(UILabel)).text = LocalString.GetString('未中奖')
		node.transform:Find('bonus'):GetComponent(typeof(UILabel)).text = LocalString.GetString('未中奖')
	end
	if selfSign then
		node.transform:Find('clsMark'):GetComponent(typeof(UISprite)).spriteName = Profession.GetIcon(data[5])
	else
		node.transform:Find('clsMark'):GetComponent(typeof(UISprite)).spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), data[5]))
	end
end

function LuaDoubleOneBonusLotteryRankWnd:OnDestroy()

end

return LuaDoubleOneBonusLotteryRankWnd
