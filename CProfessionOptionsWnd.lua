-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CommonDefs = import "L10.Game.CommonDefs"
local CProfessionOptionsItem = import "L10.UI.CProfessionOptionsItem"
local CProfessionOptionsWnd = import "L10.UI.CProfessionOptionsWnd"
local CUICommonDef = import "L10.UI.CUICommonDef"
local DelegateFactory = import "DelegateFactory"
local EnumClass = import "L10.Game.EnumClass"
local EnumGender = import "L10.Game.EnumGender"
local Extensions = import "Extensions"
--local Initialization_Init = import "L10.Game.Initialization_Init"
local NGUITools = import "NGUITools"
local Profession = import "L10.Game.Profession"
local ProfessionTransfer_Setting = import "L10.Game.ProfessionTransfer_Setting"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local LuaTweenUtils = import "LuaTweenUtils"
CProfessionOptionsWnd.m_Init_CS2LuaHook = function (this) 
    this.portrait:Clear()
    this.radar:Clear()
    this.featureLabel.text = nil
    this.introductionLabel.text = nil

    Extensions.RemoveAllChildren(this.table.transform)
    CommonDefs.ListClear(this.professionList)
    CommonDefs.ListClear(this.items)

    this.selectedCls = EnumClass.Undefined

    if CClientMainPlayer.Inst == nil then
        return
    end

    local forbiddenClasses = CreateFromClass(MakeGenericClass(HashSet, UInt32))
    local arr = ProfessionTransfer_Setting.GetData().Nonopened
    if arr ~= nil then
        do
            local i = 0
            while i < arr.Length do
                CommonDefs.HashSetAdd(forbiddenClasses, typeof(UInt32), arr[i])
                i = i + 1
            end
        end
    end

    local mainplayerCls = EnumToInt(CClientMainPlayer.Inst.Class)
    local mainplayerGender = EnumToInt(CClientMainPlayer.Inst.Gender)
    --Initialization_Init.Foreach(DelegateFactory.Action_uint_Initialization_Init(function (key, data) 
    Initialization_Init.Foreach(function (key, data) 
        if data.Status == 0 and data.Gender == mainplayerGender and data.Class ~= mainplayerCls and not CommonDefs.HashSetContains(forbiddenClasses, typeof(UInt32), data.Class) then
            if not CommonDefs.ListContains(this.professionList, typeof(UInt32), data.Class) then
                CommonDefs.ListAdd(this.professionList, typeof(UInt32), data.Class)
            end
        end
    end)

    CommonDefs.ListSort1(this.professionList, typeof(UInt32), DelegateFactory.Comparison_uint(function (val1, val2) 
        return NumberCompareTo(val1, val2)
    end))
    --从小到大排列

    do
        local i = 0
        while i < this.professionList.Count do
            local itemcell = NGUITools.AddChild(this.table.gameObject, this.itemTpl)
            itemcell:SetActive(true)
            local item = CommonDefs.GetComponent_GameObject_Type(itemcell, typeof(CProfessionOptionsItem))
            item:Init(Profession.GetLargeIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), this.professionList[i])), Profession.GetFullName(CommonDefs.ConvertIntToEnum(typeof(EnumClass), this.professionList[i])))
            UIEventListener.Get(itemcell).onClick = MakeDelegateFromCSFunction(this.OnItemClick, VoidDelegate, this)
            CommonDefs.ListAdd(this.items, typeof(CProfessionOptionsItem), item)
            i = i + 1
        end
    end
    this.table:Reposition()
    this.scrollView:ResetPosition()
    if this.items.Count > 0 then
        this:OnItemClick(this.items[0].gameObject)
    end
end
CProfessionOptionsWnd.m_OnItemClick_CS2LuaHook = function (this, go) 
    do
        local i = 0
        while i < this.items.Count do
            local selected = this.items[i].gameObject == go
            this.items[i].Selected = selected
            if selected then
                this:ShowProfessionInfo(this.professionList[i])
            end
            i = i + 1
        end
    end
end
CProfessionOptionsWnd.m_ShowProfessionInfo_CS2LuaHook = function (this, cls) 
    local eClass = CommonDefs.ConvertIntToEnum(typeof(EnumClass), cls)
    local eGender = CClientMainPlayer.Inst == nil and EnumGender.Male or CClientMainPlayer.Inst.Gender
    this.portrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(eClass, eGender, -1), false)
    this.selectedCls = eClass
    if eClass == EnumClass.YingLing then
        this.radar:Clear()
        this:RegisterYingLingRadarMapTick()
    else
        this:CancelYingLingRadarMapTick()
        this.radar.texture.alpha = 1
        this.radar:LoadMaterial(Profession.GetRadarMap(eClass))
    end
    local data = Initialization_Init.GetData(cls * 100 + EnumToInt(eGender))
    if data ~= nil then
        this.featureLabel.text = data.Feature
        this.introductionLabel.text = data.Introduction
    end
end

CProfessionOptionsWnd.m_RegisterYingLingRadarMapTick_CS2LuaHook = function (this) 
    this:CancelYingLingRadarMapTick()
    local i = 0
    local updateFunc = function ()
        if this.radar ~= nil then
            this.radar:LoadMaterial(Profession.GetRadarMap(this.selectedCls, i%3 + 1))
            i = i + 1
            LuaTweenUtils.DOKill(this.radar.transform, false)
            local fadeInTweener = LuaTweenUtils.TweenAlpha(this.radar.texture, this.radar.transform, 0,1,0.5)
            local fadeOutTweener = LuaTweenUtils.TweenAlpha(this.radar.texture, this.radar.transform,1,0,0.5)
            LuaTweenUtils.SetDelay(fadeOutTweener ,2)
        end
    end

    this.m_YingLingRadarMapTick = RegisterTick(function ( ... )
            updateFunc()
        end, 3000)
    updateFunc()--先执行一下
end

CProfessionOptionsWnd.m_CancelYingLingRadarMapTick_CS2LuaHook = function (this)
    if this.m_YingLingRadarMapTick then
        UnRegisterTick(this.m_YingLingRadarMapTick)
        this.m_YingLingRadarMapTick = nil
        LuaTweenUtils.DOKill(this.radar.transform, false)
    end
end
