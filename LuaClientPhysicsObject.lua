-- 静态的物理对象

LuaClientPhysicsObject = class()

RegistClassMember(LuaClientPhysicsObject, "m_PhysicsObject")
RegistClassMember(LuaClientPhysicsObject, "m_PhysicsObjectId")


function LuaClientPhysicsObject:Ctor(physicsObjId,physicsObject)
    self.m_PhysicsObjectId = physicsObjId
    self.m_PhysicsObject = physicsObject
    g_PhysicsObjectHandlers[self.m_PhysicsObjectId] = self
end

function LuaClientPhysicsObject:Init(table)

end

function LuaClientPhysicsObject:OnDestroy()
    g_PhysicsObjectHandlers[self.m_PhysicsObjectId] = nil
end