-- Auto Generated!!
local CCommonItemSelectCell = import "L10.UI.CCommonItemSelectCell"
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CSwitchMgr = import "L10.Engine.CSwitchMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local EnumQualityType = import "L10.Game.EnumQualityType"
local IdPartition = import "L10.Game.IdPartition"
local Item_Item = import "L10.Game.Item_Item"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
CCommonItemSelectCell.m_Init_CS2LuaHook = function (this, index, data) 

    local item = CItemMgr.Inst:GetById(data.itemId)
    if item ~= nil then
        this.itemIcon:LoadMaterial(item.Icon)
        this.bindSprite.enabled = (item.IsBinded and data.showBindIcon)
        local default
        if item.IsEquip then
            default = CUICommonDef.GetItemCellBorder(item.Equip.QualityType)
        else
            default = CUICommonDef.GetItemCellBorder(EnumQualityType.White)
        end
        this.cellBorder.spriteName = default
        if this.skillIcon ~= nil then
            this.skillIcon:Clear()
            this.skillIcon.gameObject:SetActive(false)
            if CSwitchMgr.EnableIdentifySkill and item.IsEquip and item.Equip.IsIdentifiable and Skill_AllSkills.Exists(item.Equip.FixedIdentifySkillId) then
                this.skillIcon.gameObject:SetActive(true)
                this.skillIcon:LoadSkillIcon(Skill_AllSkills.GetData(item.Equip.FixedIdentifySkillId).SkillIcon)
            end
        end
    else
        local template = Item_Item.GetData(data.templateId)
        if template ~= nil then
            this.itemIcon:LoadMaterial(template.Icon)
            this.bindSprite.enabled = false
            local extern
            if IdPartition.IdIsEquip(data.templateId) then
                extern = CUICommonDef.GetItemCellBorder(CommonDefs.ConvertIntToEnum(typeof(EnumQualityType), template.Quality))
            else
                extern = CUICommonDef.GetItemCellBorder(EnumQualityType.White)
            end
            this.cellBorder.spriteName = extern
        else
            this.itemIcon:Clear()
            this.bindSprite.enabled = false
            this.cellBorder.spriteName = CUICommonDef.GetItemCellBorder(EnumQualityType.White)
        end
        if this.skillIcon ~= nil then
            this.skillIcon:Clear()
            this.skillIcon.gameObject:SetActive(false)
        end
    end

    this.numLabel.text = tostring(data.count)
    this.numLabel.gameObject:SetActive(data.count > 1)

    this.Selected = false
    this.Index = index
    this.ItemId = data.itemId
    this.TemplateId = data.templateId
end
