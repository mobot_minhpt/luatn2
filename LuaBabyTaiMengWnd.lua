local ShareMgr = import "ShareMgr"
local CUITexture = import "L10.UI.CUITexture"
local Baby_TaiMeng = import "L10.Game.Baby_TaiMeng"
local Time = import "UnityEngine.Time"
local UITexture = import "UITexture"

LuaBabyTaiMengWnd = class()

RegistClassMember(LuaBabyTaiMengWnd, "Texture")
RegistClassMember(LuaBabyTaiMengWnd, "CTexture")

RegistClassMember(LuaBabyTaiMengWnd, "AliveDuration")
RegistClassMember(LuaBabyTaiMengWnd, "FadeInTime")
RegistClassMember(LuaBabyTaiMengWnd, "FadeOutTime")
RegistClassMember(LuaBabyTaiMengWnd, "StartTime")
RegistClassMember(LuaBabyTaiMengWnd, "TaiMengId")

function LuaBabyTaiMengWnd:Init()
	self:InitClassMembers()
	self:InitValues()
end

function LuaBabyTaiMengWnd:InitClassMembers()
	self.CTexture = self.transform:Find("Anchor/Texture"):GetComponent(typeof(CUITexture))
	self.Texture = self.transform:Find("Anchor/Texture"):GetComponent(typeof(UITexture))
	self.CTexture:Clear()

end

function LuaBabyTaiMengWnd:InitValues()

	self.AliveDuration = 8
	self.FadeInTime = 1
	self.FadeOutTime = 1
	self.StartTime = Time.realtimeSinceStartup

	self.TaiMengId = LuaBabyMgr.m_TaiMengId
	local taimeng = Baby_TaiMeng.GetData(self.TaiMengId)
	if not taimeng then
		CUIManager.CloseUI(CLuaUIResources.BabyTaiMengWnd)
		return
	end

	self.CTexture:LoadMaterial(taimeng.PicPath)
	self.Texture.alpha = 0
end

function LuaBabyTaiMengWnd:Update()
	local time = Time.realtimeSinceStartup - self.StartTime
	if time < self.FadeInTime then
		self.Texture.alpha = time / self.FadeInTime
		self.Texture:SetDirty()
	elseif self.AliveDuration - time <= self.FadeOutTime and time <= self.AliveDuration then
		self.Texture.alpha = (self.AliveDuration - time) / self.FadeOutTime
		self.Texture:SetDirty()
	elseif time > self.AliveDuration then
		if not CommonDefs.IS_VN_CLIENT then
			-- 结束，进入分享
			local taimeng = Baby_TaiMeng.GetData(self.TaiMengId)
			ShareMgr.ShareWebImage2PersonalSpace(taimeng.PicLink, nil)
		end
		CUIManager.CloseUI(CLuaUIResources.BabyTaiMengWnd)
	end
end


return LuaBabyTaiMengWnd