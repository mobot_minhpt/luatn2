-- Auto Generated!!
local ActionPair = import "L10.UI.CChatInputWnd+ActionPair"
local Byte = import "System.Byte"
local CAchievementMgr = import "L10.Game.CAchievementMgr"
local CButton = import "L10.UI.CButton"
local CChatInputAchievementCell = import "L10.UI.CChatInputAchievementCell"
local CChatInputEmoticonCell = import "L10.UI.CChatInputEmoticonCell"
local CChatInputItemCell = import "L10.UI.CChatInputItemCell"
local CChatInputLingShouCell = import "L10.UI.CChatInputLingShouCell"
local CChatInputMgr = import "L10.UI.CChatInputMgr"
local CChatInputWnd = import "L10.UI.CChatInputWnd"
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CCommonItem = import "L10.Game.CCommonItem"
local CEmotionMgr = import "L10.UI.CEmotionMgr"
local CHongBaoMgr = import "L10.UI.CHongBaoMgr"
local CIndirectUIResources = import "L10.UI.CIndirectUIResources"
local CItemMgr = import "L10.Game.CItemMgr"
local CLingShouMgr = import "L10.Game.CLingShouMgr"
local CMiniMap = import "L10.UI.CMiniMap"
local CommonDefs = import "L10.Game.CommonDefs"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local CPortraitSocialWndMgr = import "L10.UI.CPortraitSocialWndMgr"
local CScene = import "L10.Game.CScene"
local CUIManager = import "L10.UI.CUIManager"
local DelegateFactory = import "DelegateFactory"
local EChatPanel = import "L10.Game.EChatPanel"
local ENUM_AVATAR_RESULT = import "ENUM_AVATAR_RESULT"
local EnumEventType = import "EnumEventType"
local EnumHongbaoType = import "L10.UI.EnumHongbaoType"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EnumItemPlaceSize = import "L10.Game.EnumItemPlaceSize"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local GameObject = import "UnityEngine.GameObject"
local GameSetting_Common = import "L10.Game.GameSetting_Common"
local HTTPHelper = import "L10.Game.HTTPHelper"
local Input = import "UnityEngine.Input"
local InputType = import "L10.UI.CChatInputWnd+InputType"
local Int32 = import "System.Int32"
local L10 = import "L10"
local LoadPicMgr = import "L10.Game.LoadPicMgr"
local LocalString = import "LocalString"
local Main = import "L10.Engine.Main"
local NGUITools = import "NGUITools"
local Object = import "System.Object"
local SavePicData = import "L10.Game.LoadPicDef+SavePicData"
local SavePicRet = import "L10.Game.LoadPicDef+SavePicRet"
local String = import "System.String"
local UICamera = import "UICamera"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local UIRoot = import "UIRoot"
local UISprite = import "UISprite"
local Utility = import "L10.Engine.Utility"
local Vector3 = import "UnityEngine.Vector3"
local Wedding_Setting = import "L10.Game.Wedding_Setting"

local BabyMgr=import "L10.Game.BabyMgr"
local CUITexture=import "L10.UI.CUITexture"
local Component=import "UnityEngine.Component"
local CChatLinkMgr = import "CChatLinkMgr"
local ChatLocationLink = import "CChatLinkMgr+ChatLocationLink"

CChatInputWnd.m_Awake_CS2LuaHook = function (this) 
    if CChatInputWnd.Instance ~= nil then
        L10.CLogMgr.LogError("more than one instance running!" .. this:GetType():ToString())
    end
    CChatInputWnd.Instance = this
end
CChatInputWnd.m_Update_CS2LuaHook = function (this) 
    if CUIManager.IsLoaded(CIndirectUIResources.ChatPicAddWnd) then
        return
    end

    if Input.GetMouseButtonUp(0) then
        if CUICommonDef.IsOverUI and UICamera.lastHit.collider and CChatInputMgr.TriggierGo == UICamera.lastHit.collider.gameObject then
            return
        end
        local corners = this.background.worldCorners
        local lastPos = CUIManager.UIMainCamera:ScreenToWorldPoint(Input.mousePosition)

        if lastPos.x < corners[0].x or lastPos.x > corners[2].x or lastPos.y < corners[0].y or lastPos.y > corners[2].y then
            this:Close()
        end
    end
end
CChatInputWnd.m_Init_CS2LuaHook = function (this) 
    this:IPhoneXAdaptation()

    local totalActions = CreateFromClass(MakeGenericClass(List, ActionPair))
    LuaChatInputWndEmotionBtnMgr:GetAllTotalActions(this, totalActions)
    local bgMask = this.transform:Find("_BgMask_")
    local extern = CChatInputMgr.ParentType
    if bgMask ~= nil then
        bgMask.gameObject:SetActive(extern == CChatInputMgr.EParentType.PersonalSpace and extern == CChatInputMgr.EParentType.JingLingShareToPersonalSpace and 
        extern == CChatInputMgr.EParentType.BabyChat)
    end

    Extensions.RemoveAllChildren(this.btnTable.transform)

    local firstBtn = nil
    do
        local i = 0
        while i < totalActions.Count do
            local btn = NGUITools.AddChild(this.btnTable.gameObject, this.btnTemplate)
            btn:SetActive(true)
            CommonDefs.GetComponentInChildren_GameObject_Type(btn, typeof(UILabel)).text = totalActions[i].Key
            local index = i
            if i == 0 then
                firstBtn = btn
            end
            UIEventListener.Get(btn).onClick = DelegateFactory.VoidDelegate(function (go) 
                this:SetSelectedState(go)
                invoke(totalActions[index].Value)
            end)
            i = i + 1
        end
    end
    this.btnTable:Reposition()
    if firstBtn ~= nil then
        this:SetSelectedState(firstBtn)
        invoke(totalActions[0].Value)
    end
end
CChatInputWnd.m_SetSelectedState_CS2LuaHook = function (this, go) 
    do
        local i = 0
        while i < this.btnTable.transform.childCount do
            local btn = this.btnTable.transform:GetChild(i).gameObject
            CommonDefs.GetComponent_GameObject_Type(btn, typeof(CButton)).Selected = btn:Equals(go)
            i = i + 1
        end
    end
end
CChatInputWnd.m_OnEnable_CS2LuaHook = function (this) 
    EventManager.AddListener(EnumEventType.GetAllLingShouOverview, MakeDelegateFromCSFunction(this.LoadLingShousAsync, Action0, this))
end
CChatInputWnd.m_IPhoneXAdaptation_CS2LuaHook = function (this) 

    if not CPortraitSocialWndMgr.Inst:IsPortraitSocialWndOpened() then
        return
    end

    if UIRoot.EnableIPhoneXAdaptation then
        local t = this.transform:GetChild(0)
        Extensions.SetLocalPositionX(t, t.localPosition.x - UIRoot.VirtualIPhoneXWidthMargin)
    end
end
CChatInputWnd.m_OnDisable_CS2LuaHook = function (this) 
    EventManager.RemoveListener(EnumEventType.GetAllLingShouOverview, MakeDelegateFromCSFunction(this.LoadLingShousAsync, Action0, this))
end
CChatInputWnd.m_LoadItems_CS2LuaHook = function (this) 
    this:OpenShowMode(false)

    Extensions.RemoveAllChildren(this.table.transform)
    --TODO 身上的和包裹的物品显示优先级是怎样的
    if CClientMainPlayer.Inst ~= nil then
        local itemProp = CClientMainPlayer.Inst.ItemProp
        local items = CreateFromClass(MakeGenericClass(List, CCommonItem))
        --先身上
        local num = 0
        local count = EnumItemPlaceSize.GetPlaceSize(EnumItemPlace.Body)



        do
            local i = 1
            while i <= count do
                local continue
                repeat
                    local itemId = itemProp:GetItemAt(EnumItemPlace.Body, i)
                    if System.String.IsNullOrEmpty(itemId) then
                        continue = true
                        break
                    end
                    local item = CItemMgr.Inst:GetById(itemId)
                    if item ~= nil then
                        CommonDefs.ListAdd(items, typeof(CCommonItem), item)
                        num = num + 1
                    end
                    continue = true
                until 1
                if not continue then
                    break
                end
                i = i + 1
            end
        end

        count = itemProp:GetPlaceSize(EnumItemPlace.Bag)

        --TODO: 服务器端的包裹位置信息是否需要调整为多个分页？
        do
            local i = 1
            while i <= count do
                local continue
                repeat
                    local itemId = itemProp:GetItemAt(EnumItemPlace.Bag, i)
                    if System.String.IsNullOrEmpty(itemId) then
                        continue = true
                        break
                    end
                    local item = CItemMgr.Inst:GetById(itemId)
                    if item ~= nil and item.TemplateId ~= Wedding_Setting.GetData().BaZiCardItemId and item.TemplateId ~= Wedding_Setting.GetData().CertificationItemId then
                        CommonDefs.ListAdd(items, typeof(CCommonItem), item)
                    end
                    continue = true
                until 1
                if not continue then
                    break
                end
                i = i + 1
            end
        end

        --包裹中的任务道具也放进来
        count = itemProp:GetPlaceSize(EnumItemPlace.Task)
        do
            local i = 1
            while i <= count do
                local continue
                repeat
                    local itemId = itemProp:GetItemAt(EnumItemPlace.Task, i)
                    if System.String.IsNullOrEmpty(itemId) then
                        continue = true
                        break
                    end
                    local item = CItemMgr.Inst:GetById(itemId)
                    if item ~= nil and item.TemplateId ~= Wedding_Setting.GetData().BaZiCardItemId and item.TemplateId ~= Wedding_Setting.GetData().CertificationItemId then
                        CommonDefs.ListAdd(items, typeof(CCommonItem), item)
                    end
                    continue = true
                until 1
                if not continue then
                    break
                end
                i = i + 1
            end
        end

        do
            local i = 0
            while i < items.Count do
                local instance = NGUITools.AddChild(this.table.gameObject, this.itemCellTemplate)
                instance:SetActive(true)
                instance.transform.localEulerAngles = Vector3(0, 0, 90)
                local cell = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CChatInputItemCell))
                cell:Init(items[i].Id, i < num)
                local index = i
                cell.OnItemClickDelegate = DelegateFactory.Action_string(function (itemId) 
                    if CChatInputMgr.Listener ~= nil then
                        CChatInputMgr.Listener:OnInputItemLink(items[index])
                    end
                end)
                i = i + 1
            end
        end
        this.table.columns = 2
        this.table:Reposition()
        this.scrollView:ResetPosition()
    end
end
CChatInputWnd.m_LoadWeddingItem_CS2LuaHook = function (this) 
    this:OpenShowMode(false)
    Extensions.RemoveAllChildren(this.table.transform)
    --TODO 身上的和包裹的物品显示优先级是怎样的
    if CClientMainPlayer.Inst ~= nil then
        local itemProp = CClientMainPlayer.Inst.ItemProp
        local items = CreateFromClass(MakeGenericClass(List, CCommonItem))
        --先身上
        local num = 0
        local count = itemProp:GetPlaceSize(EnumItemPlace.Bag)

        --TODO: 服务器端的包裹位置信息是否需要调整为多个分页？
        do
            local i = 1
            while i <= count do
                local continue
                repeat
                    local itemId = itemProp:GetItemAt(EnumItemPlace.Bag, i)
                    if System.String.IsNullOrEmpty(itemId) then
                        continue = true
                        break
                    end
                    local item = CItemMgr.Inst:GetById(itemId)
                    if item ~= nil and (item.TemplateId == Wedding_Setting.GetData().BaZiCardItemId or item.TemplateId == Wedding_Setting.GetData().CertificationItemId) then
                        CommonDefs.ListAdd(items, typeof(CCommonItem), item)
                    end
                    continue = true
                until 1
                if not continue then
                    break
                end
                i = i + 1
            end
        end

        --包裹中的任务道具也放进来
        count = itemProp:GetPlaceSize(EnumItemPlace.Task)
        do
            local i = 1
            while i <= count do
                local continue
                repeat
                    local itemId = itemProp:GetItemAt(EnumItemPlace.Task, i)
                    if System.String.IsNullOrEmpty(itemId) then
                        continue = true
                        break
                    end
                    local item = CItemMgr.Inst:GetById(itemId)
                    if item ~= nil and (item.TemplateId == Wedding_Setting.GetData().BaZiCardItemId or item.TemplateId == Wedding_Setting.GetData().CertificationItemId) then
                        CommonDefs.ListAdd(items, typeof(CCommonItem), item)
                    end
                    continue = true
                until 1
                if not continue then
                    break
                end
                i = i + 1
            end
        end

        do
            local i = 0
            while i < items.Count do
                local instance = NGUITools.AddChild(this.table.gameObject, this.itemCellTemplate)
                instance:SetActive(true)
                instance.transform.localEulerAngles = Vector3(0, 0, 90)
                local cell = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CChatInputItemCell))
                cell:Init(items[i].Id, i < num)
                local index = i
                cell.OnItemClickDelegate = DelegateFactory.Action_string(function (itemId) 
                    if CChatInputMgr.Listener ~= nil then
                        CChatInputMgr.Listener:OnInputWeddingItemLink(items[index])
                    end
                end)
                i = i + 1
            end
        end
        this.table.columns = 2
        this.table:Reposition()
        this.scrollView:ResetPosition()
    end
end
CChatInputWnd.m_OpenShowMode_CS2LuaHook = function (this, picshow) 
    this.scrollView.gameObject:SetActive(not picshow)
    if this.addpicNode ~= nil then
        this.addpicNode:SetActive(picshow)
    end
end
CChatInputWnd.m_LoadEmoticonsByData_CS2LuaHook = function (this, _scrollView, _table) 
    if _scrollView == nil or _table == nil then
        return
    end
    local mEmotionInfos = CEmotionMgr.Inst.EmotionInfos
    Extensions.RemoveAllChildren(_table.transform)
    _table.columns = 3

    do
        local i = 0
        while i < mEmotionInfos.Count do
            local continue
            repeat
                local emotionIndex = mEmotionInfos[i].ImgIndex
                if not CChatInputWnd.s_EnableShowExtraEmotions and emotionIndex >= CChatInputWnd.exclusiveNumber then
                    break
                end

                if CEmotionMgr.Inst:IsExclusive(emotionIndex) then
                    continue = true
                    break
                end

                if CChatInputMgr.ParentType == CChatInputMgr.EParentType.PersonalSpace and emotionIndex >= CChatInputWnd.exclusiveNumber then
                    continue = true
                    break
                end

                -- 茶室屏蔽部分表情
                if CChatInputMgr.ParentType == CChatInputMgr.EParentType.ClubHouse then
                    local setData = GameplayItem_ClubHouseEmoticon.GetData(emotionIndex)
                    if setData == nil or setData.InputWndShow == 0 then
                        continue = true
                        break
                    end
                end

                local instance = NGUITools.AddChild(_table.gameObject, this.emoticonCellTemplate)
                instance:SetActive(true)
                instance.transform.localEulerAngles = Vector3(0, 0, 90)
                CommonDefs.GetComponent_GameObject_Type(instance, typeof(UISprite)).spriteName = nil
                local cell = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CChatInputEmoticonCell))
                cell:Init(emotionIndex, MakeDelegateFromCSFunction(this.OnEmotionClicked, MakeGenericClass(Action2, UISprite, String), this), MakeDelegateFromCSFunction(this.OnEmotionLongPress, MakeGenericClass(Action2, UISprite, String), this), MakeDelegateFromCSFunction(this.OnEmotionLongPressEnd, MakeGenericClass(Action2, UISprite, String), this))
                continue = true
            until 1
            if not continue then
                break
            end
            i = i + 1
        end
    end
    _table.columns = 3
    _table:Reposition()
    _scrollView:ResetPosition()
end
CChatInputWnd.m_LoadEmoticons_CS2LuaHook = function (this) 
    this:OpenShowMode(false)
    this:LoadEmoticonsByData(this.scrollView, this.table)
end
CChatInputWnd.m_LoadSavePic_CS2LuaHook = function (this) 
    if LoadPicMgr.EnableChatSaveSmallPic then
        if GameSetting_Common.GetData().PersonalSpaceLevel > CClientMainPlayer.Inst.MaxLevel then
            g_MessageMgr:ShowMessage("UPLOAD_PIC_LEVEL", GameSetting_Common.GetData().PersonalSpaceLevel)
            return
        else
            if LoadPicMgr.Inst.SavePicList ~= nil then
                CommonDefs.ListClear(LoadPicMgr.Inst.SavePicList)
            end
            LoadPicMgr.GetSavePicList(MakeDelegateFromCSFunction(this.LoadSavePicListBack, MakeGenericClass(Action1, SavePicRet), this), nil)
        end
    else
        g_MessageMgr:ShowMessage("FUNCTION_NOT_OPEN")
    end
end
CChatInputWnd.m_LoadSavePicListBack_CS2LuaHook = function (this, ret) 
    if not this then
        return
    end
    if ret.code == 0 then
        if ret.data == nil or ret.data.Length == 0 then
            if LoadPicMgr.Inst.SavePicList ~= nil then
                CommonDefs.ListClear(LoadPicMgr.Inst.SavePicList)
            end
        else
            LoadPicMgr.Inst.SavePicList = InitializeListWithArray(CreateFromClass(MakeGenericClass(List, SavePicData)), SavePicData, ret.data)
        end
        this:InitSavePicBar()
    end
end
CChatInputWnd.m_OnTabChange_CS2LuaHook = function (this, go, index) 
    if index == 0 then
        this:OpenShowMode(true)
        this:LoadEmoticonsByData(this.addpicScrollView, this.addpicTable)
    elseif index == 1 then
        if CClientMainPlayer.Inst ~= nil then
            if CClientMainPlayer.Inst.MaxLevel >= LoadPicMgr.EnableChatSaveSmallPicLevel then
                this:OpenShowMode(true)
                Extensions.RemoveAllChildren(this.addpicTable.transform)
                this:LoadSavePic()
            else
                g_MessageMgr:ShowMessage("Get_Ultskill_Condition_Level", LoadPicMgr.EnableChatSaveSmallPicLevel)
            end
        end
    end
end
CChatInputWnd.m_LoadEmoticonsAndPic_CS2LuaHook = function (this) 
    if this.addpicTab ~= nil then
        this.addpicTab.OnTabChange = MakeDelegateFromCSFunction(this.OnTabChange, MakeGenericClass(Action2, GameObject, Int32), this)
        this.addpicTab:ChangeTab(0, false)
    end
end
CChatInputWnd.m_OnEmotionClicked_CS2LuaHook = function (this, sprite, prefix)
    if CChatInputMgr.Listener==nil then return end
    CChatInputMgr.Listener:OnInputEmoticon(prefix)
end
CChatInputWnd.m_LoadTasks_CS2LuaHook = function (this) 
    Extensions.RemoveAllChildren(this.table.transform)
end
CChatInputWnd.m_LoadLingShous_CS2LuaHook = function (this) 
    --Gac2Gas.RequestAllLingShouOverview();
    --Gac2Gas.RequestAllLingShouOverview();
    CLingShouMgr.Inst:RequestLingShouList()
end
CChatInputWnd.m_LoadLingShousAsync_CS2LuaHook = function (this) 
    this:OpenShowMode(false)
    if this.inputType ~= InputType.LingShou then
        return
    end
    Extensions.RemoveAllChildren(this.table.transform)
    local list = CLingShouMgr.Inst:GetLingShouOverviewList()
    do
        local i = 0
        while i < list.Count do
            local instance = NGUITools.AddChild(this.table.gameObject, this.lingshouTemplate)
            instance:SetActive(true)
            local cell = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CChatInputLingShouCell))
            cell:Init(list[i].id, list[i].templateId)
            cell.OnLingShouItemClickDelegate = DelegateFactory.Action_string_string(function (id, name) 
                if CChatInputMgr.Listener ~= nil then
                    CChatInputMgr.Listener:OnInputLingShouLink(id, name)
                end
            end)
            i = i + 1
        end
    end
    --baby
    local ids = BabyMgr.Inst:GetBabyIdList()
    for i=1,ids.Count do
        local baby = BabyMgr.Inst:GetBabyById(ids[i-1])
        local instance = NGUITools.AddChild(this.table.gameObject, this.lingshouTemplate)
        instance:SetActive(true)
        Component.Destroy(instance:GetComponent(typeof(CChatInputLingShouCell)))

        local nameLabel = instance.transform:Find("ItemBg/NameLabel"):GetComponent(typeof(UILabel))
        local name = baby.Name
        local id= baby.Id
        nameLabel.text = baby.Name
        local gradeLaebl = instance.transform:Find("ItemBg/EvolveGradeLabel"):GetComponent(typeof(UILabel))
        gradeLaebl.text = SafeStringFormat3(LocalString.GetString("%d级"),baby.Grade)
        local levelLabel = instance.transform:Find("ItemBg/LevelLabel"):GetComponent(typeof(UILabel))
        levelLabel.text=nil
        local icon = instance.transform:Find("ItemBg/Border/Icon"):GetComponent(typeof(CUITexture))
        icon:LoadNPCPortrait(BabyMgr.Inst:GetBabyPortrait(baby), false)
        UIEventListener.Get(instance).onClick=DelegateFactory.VoidDelegate(function(g)
            -- CLuaOtherBabyInfoWnd.m_PlayerId = CClientMainPlayer.Inst.Id
            -- CLuaOtherBabyInfoWnd.m_BabyId = baby.Id
            -- CUIManager.ShowUI(CLuaUIResources.OtherBabyInfoWnd)
            if CChatInputMgr.Listener ~= nil then
                CChatInputMgr.Listener:OnInputBabyLink(id, name)
            end
        end)
    end



    this.table.columns = 2
    this.table:Reposition()
    this.scrollView:ResetPosition()
end
CChatInputWnd.m_LoadStalls_CS2LuaHook = function (this) 
    Extensions.RemoveAllChildren(this.table.transform)
end
CChatInputWnd.m_LoadAchievements_CS2LuaHook = function (this) 
    this:OpenShowMode(false)
    Extensions.RemoveAllChildren(this.table.transform)

    local list = CAchievementMgr.Inst:GetMyAchievements()
    do
        local i = 0
        while i < list.Count do
            local instance = NGUITools.AddChild(this.table.gameObject, this.achievementCellTemplate)
            instance:SetActive(true)
            local cell = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CChatInputAchievementCell))
            cell:Init(list[i])
            cell.OnAchievementCellClickDelegate = DelegateFactory.Action_uint(function (id) 
                if CChatInputMgr.Listener ~= nil then
                    CChatInputMgr.Listener:OnInputAchievement(id)
                end
            end)
            i = i + 1
        end
    end
    this.table.columns = 3
    this.table:Reposition()
    this.scrollView:ResetPosition()
end
CChatInputWnd.m_LoadSkills_CS2LuaHook = function (this) 
    Extensions.RemoveAllChildren(this.table.transform)
end
CChatInputWnd.m_LoadRedPackets_CS2LuaHook = function (this) 

    Extensions.RemoveAllChildren(this.table.transform)

    if not CHongBaoMgr.s_EnableHongBao then
        return
    end
    --打开红包界面
    if CChatInputMgr.ChatChannel == EChatPanel.World then
        CHongBaoMgr.ShowHongBaoWnd(EnumHongbaoType.WorldHongbao)
    elseif CChatInputMgr.ChatChannel == EChatPanel.Guild then
        CHongBaoMgr.ShowHongBaoWnd(EnumHongbaoType.GuildHongbao)
    end
end
CChatInputWnd.m_LoadLiBao_CS2LuaHook = function (this) 

    Extensions.RemoveAllChildren(this.table.transform)
    --打开礼包界面
    if CChatInputMgr.ChatChannel == EChatPanel.World or CChatInputMgr.ChatChannel == EChatPanel.Guild then
        CHongBaoMgr.ShowGiftWnd()
    end
end
CChatInputWnd.m_LoadPicAlert_CS2LuaHook = function (this) 
    g_MessageMgr:ShowMessage("FRIENDLINESS_NOT_ENOUGH_SENDPIC", LoadPicMgr.NormalFriendlinessNum)
end
CChatInputWnd.m_OnMomentAvaterCallBack_CS2LuaHook = function (this, strResult) 
    if (strResult == ToStringWrap(ENUM_AVATAR_RESULT.eResult_Success)) then
        -- 成功
        this:StartCoroutine(LoadPicMgr.LoadMomentTexture(LoadPicMgr.PicSaveName, MakeDelegateFromCSFunction(this.AddPicBack, MakeGenericClass(Action1, MakeArrayClass(Byte)), this)))
    elseif (strResult == ToStringWrap(ENUM_AVATAR_RESULT.eResult_Cancel)) then
        -- 取消
    elseif (strResult == ToStringWrap(ENUM_AVATAR_RESULT.eResult_Failed)) then
        -- 失败
    end
end
CChatInputWnd.m_UploadChatPic_CS2LuaHook = function (this, textureByte, pdata, cSave, backFunc) 
    LoadPicMgr.UploadPic(CPersonalSpaceMgr.Upload_PicType, textureByte, DelegateFactory.Action_string(function (url) 
        if LoadPicMgr.EnableNosPicInfoGet then
            Main.Inst:StartCoroutine(HTTPHelper.NOSGetImageInfo(url, DelegateFactory.Action_string(function (ret) 
                this:GetNosImageInfoBack(ret, url, pdata, cSave, backFunc)
            end)))
        else
            LoadPicMgr.SaveUploadPic(CPersonalSpaceMgr.Upload_PicType, url, pdata.type, DelegateFactory.Action_CPersonalSpace_BaseRet(function (ret) 
                if ret.code == 0 then
                    GenericDelegateInvoke(backFunc, Table2Array({ret.msg, cSave, pdata}, MakeArrayClass(Object)))
                end
            end), nil)
        end
    end))
end
CChatInputWnd.m_LoadLocation_CS2LuaHook = function (this) 
    if CClientMainPlayer.Inst ~= nil and CScene.MainScene ~= nil then
        if CChatInputMgr.Listener ~= nil then
            local curScene = CScene.MainScene
            local pos = Utility.PixelPos2GridPos(CClientMainPlayer.Inst.Pos)
            local mapIdx = CMiniMap.Instance ~= nil and CMiniMap.Instance.mapIdx or 0

            local link = CChatLinkMgr.TryConvertToTrackToSectChatButtonLink(curScene.SceneTemplateId, curScene.SceneName, pos.x, pos.y, curScene.SceneId, mapIdx)
            if link then
                CChatInputMgr.Listener:OnSendLink(link.logicTag, true)
            else
                CChatInputMgr.Listener:OnSendLink(ChatLocationLink.GenerateLink(curScene.SceneTemplateId, curScene.SceneName, pos.x, pos.y, curScene.SceneId, mapIdx).logicTag, true)
            end
        end
    end
end
CChatInputWnd.m_LoadHouse_CS2LuaHook = function (this) 
    if CClientMainPlayer.Inst ~= nil and not System.String.IsNullOrEmpty(CClientMainPlayer.Inst.ItemProp.HouseId) and not System.String.IsNullOrEmpty(CClientHouseMgr.Inst.mCommunityName) then
        local houseName = CClientHouseMgr.Inst:GetHouseName()

        if not CClientHouseMgr.Inst:IsHouseOwner() then
            houseName = houseName .. CClientHouseMgr.Inst:GetXiangfangName()
        end
        local link = SafeStringFormat3("<link button=%s,FindHouse,%s>", houseName, CClientMainPlayer.Inst.ItemProp.HouseId)
        CChatInputMgr.Listener:OnSendLink(link, true)
    end
end

CChatInputMgr.m_ShowChatInputWnd_CS2LuaHook = function (type, listener, triggerGo, chatChannel, targetid, chatgroupid) 
    CChatInputMgr.ParentType = type
    CChatInputMgr.Listener = listener
    CChatInputMgr.TriggierGo = triggerGo
    CChatInputMgr.ChatChannel = chatChannel
    CChatInputMgr.TargetId = targetid
    CChatInputMgr.CharGroupId = chatgroupid
    --CUIManager.ShowUI(CIndirectUIResources.ChatInputWnd);
    if CPortraitSocialWndMgr.Inst:IsPortraitSocialWndOpened() then
        CUIManager.ShowUI(CIndirectUIResources.PortraitChatInputWnd)
    else
        CUIManager.ShowUI(CIndirectUIResources.ChatInputWnd)
    end
end

LuaChatInputWndEmotionBtnMgr = {}
function LuaChatInputWndEmotionBtnMgr:GetAllTotalActions(this, totalActions)
    self:AddBtnNamedEmoticon(this, totalActions)
    self:AddBtnNamedItem(this, totalActions)
    self:AddBtnNamedLingShouChildren(this, totalActions)
    self:AddBtnNamedRedPacket(this, totalActions)
    self:AddBtnNamedLiBao(this, totalActions)
    self:AddBtnNamedWeddingItem(this, totalActions)
    self:AddBtnNamedLocation(this, totalActions)
    self:AddBtnNamedPic(this, totalActions)
    self:AddBtnNamedAchievement(this, totalActions)
end

function LuaChatInputWndEmotionBtnMgr:AddEmoticonBtnActiobnPair(this, actionKey, actionValue, checkVisableFuncTable, totalActions)
    local actionPair = CreateFromClass(ActionPair, actionKey, actionValue)
    local extern = CChatInputMgr.ParentType
    if checkVisableFuncTable[extern] then
        CommonDefs.ListAdd(totalActions, typeof(ActionPair), actionPair)
    end
end

function LuaChatInputWndEmotionBtnMgr:AddCommonBtn(this, totalActions, actionKey, actionValue)
    local checkVisableFuncTable = {}
    checkVisableFuncTable[CChatInputMgr.EParentType.ChatWnd] = true
    checkVisableFuncTable[CChatInputMgr.EParentType.Friend] = true
    checkVisableFuncTable[CChatInputMgr.EParentType.FriendIMGroup] = true
    checkVisableFuncTable[CChatInputMgr.EParentType.FriendNormal] = true
    checkVisableFuncTable[CChatInputMgr.EParentType.FriendGroup] = true
    checkVisableFuncTable[CChatInputMgr.EParentType.LoudSpeaker] = true
    checkVisableFuncTable[CChatInputMgr.EParentType.PersonalSpace] = true
    checkVisableFuncTable[CChatInputMgr.EParentType.ClubHouse] = true
    self:AddEmoticonBtnActiobnPair(this, actionKey, actionValue, checkVisableFuncTable,totalActions)
end

function LuaChatInputWndEmotionBtnMgr:AddBtnNamedEmoticon(this, totalActions)
    local checkVisableFuncTable = {}
    checkVisableFuncTable[CChatInputMgr.EParentType.ChatWnd] = true
    checkVisableFuncTable[CChatInputMgr.EParentType.Friend] = true
    checkVisableFuncTable[CChatInputMgr.EParentType.FriendIMGroup] = true
    checkVisableFuncTable[CChatInputMgr.EParentType.FriendNormal] = true
    checkVisableFuncTable[CChatInputMgr.EParentType.FriendGroup] = true
    checkVisableFuncTable[CChatInputMgr.EParentType.LoudSpeaker] = true
    checkVisableFuncTable[CChatInputMgr.EParentType.PersonalSpace] = true
    checkVisableFuncTable[CChatInputMgr.EParentType.JingLingShareToPersonalSpace] = true
    checkVisableFuncTable[CChatInputMgr.EParentType.BabyChat] = true
    checkVisableFuncTable[CChatInputMgr.EParentType.ClubHouse] = true
    local extern = CChatInputMgr.ParentType
    local isAddEmoticonsAndPicBtn = extern == CChatInputMgr.EParentType.ChatWnd and this.addpicNode ~= nil and LoadPicMgr.EnableChatSaveSmallPic and (CChatInputMgr.ChatChannel == EChatPanel.Guild or CChatInputMgr.ChatChannel == EChatPanel.Team)
    isAddEmoticonsAndPicBtn = isAddEmoticonsAndPicBtn or ((extern == CChatInputMgr.EParentType.Friend or extern == CChatInputMgr.EParentType.FriendIMGroup) and this.addpicNode ~= nil and LoadPicMgr.EnableChatSaveSmallPic)
    self:AddEmoticonBtnActiobnPair(this, LocalString.GetString("表情"), DelegateFactory.Action(function ()
        this.inputType = InputType.Emoticon
        if isAddEmoticonsAndPicBtn then
            this:LoadEmoticonsAndPic()
        else
            this:LoadEmoticons()
        end
    end),checkVisableFuncTable,totalActions)
end

function LuaChatInputWndEmotionBtnMgr:AddBtnNamedItem(this, totalActions)
    self:AddCommonBtn(this, totalActions, LocalString.GetString("道具"), DelegateFactory.Action(function ()
        this.inputType = InputType.Item
        this:LoadItems()
    end))
end

function LuaChatInputWndEmotionBtnMgr:AddBtnNamedLingShouChildren(this, totalActions)
    self:AddCommonBtn(this, totalActions, LocalString.GetString("灵兽孩子"), DelegateFactory.Action(function ()
        this.inputType = InputType.LingShou
        this:LoadLingShous()
    end))
end 

function LuaChatInputWndEmotionBtnMgr:AddBtnNamedAchievement(this, totalActions)
    self:AddCommonBtn(this, totalActions, LocalString.GetString("成就"), DelegateFactory.Action(function ()
        this.inputType = InputType.Achievement
        this:LoadAchievements()
    end))
end

function LuaChatInputWndEmotionBtnMgr:AddBtnNamedWeddingItem(this, totalActions)
    self:AddCommonBtn(this, totalActions, LocalString.GetString("结婚道具"), DelegateFactory.Action(function ()
        this.inputType = InputType.Wedding
        this:LoadWeddingItem()
    end))
end

function LuaChatInputWndEmotionBtnMgr:AddBtnNamedLocation(this, totalActions)
    local checkVisableFuncTable = {}
    checkVisableFuncTable[CChatInputMgr.EParentType.ChatWnd] = true
    checkVisableFuncTable[CChatInputMgr.EParentType.Friend] = true
    checkVisableFuncTable[CChatInputMgr.EParentType.FriendIMGroup] = true
    checkVisableFuncTable[CChatInputMgr.EParentType.FriendNormal] = true
    checkVisableFuncTable[CChatInputMgr.EParentType.FriendGroup] = true
    if LuaZongMenMgr.m_IsOpen then
        Gac2Gas.GetSectXinWuInfo()
    end
    Gac2Gas.QueryMyTeamRecruit()
    self:AddEmoticonBtnActiobnPair(this, LocalString.GetString("位置邀请"), DelegateFactory.Action(function () 
        this.inputType = InputType.Location
        self:LoadAllLocation(this)
    end),checkVisableFuncTable,totalActions)
end

function LuaChatInputWndEmotionBtnMgr.LoadZongMenIvitationLetter(this)
    local zongMenName = LuaZongMenMgr.m_ZongMenProp and LuaZongMenMgr.m_ZongMenProp.Name.StringData or nil 
    if zongMenName and CClientMainPlayer.Inst then
        local link = g_MessageMgr:FormatMessage("ZongMen_IvitationLetter_Channel",zongMenName, CClientMainPlayer.Inst.BasicProp.SectId,CClientMainPlayer.Inst.Id)
        if CChatInputMgr.Listener then
            CChatInputMgr.Listener:OnSendLink(link, true)
        end
    end
end

function LuaChatInputWndEmotionBtnMgr.LoadMyTeamRecruit(this)
    if LuaTeamRecruitMgr:HasMyRecruit() then
        if CChatInputMgr.Listener then
            CChatInputMgr.Listener:OnSendLink(LuaTeamRecruitMgr:GetMyRecruitLinkMsg(), true)
        end
    else
        g_MessageMgr:ShowMessage("TEAM_RECURIT_MINE_IS_EXPIRED")
    end
end

function LuaChatInputWndEmotionBtnMgr:LoadAllLocation(this)
    this:OpenShowMode(false)
    Extensions.RemoveAllChildren(this.table.transform)
    local curScene = CScene.MainScene
    local mapIdx = CMiniMap.Instance ~= nil and CMiniMap.Instance.mapIdx or 0
    local houseName = CClientHouseMgr.Inst:GetHouseName()
    local zongMenName = LuaZongMenMgr.m_ZongMenProp and LuaZongMenMgr.m_ZongMenProp.Name.StringData or nil 
    local list = {
        {text1 = LocalString.GetString("我的位置"),text2 = SafeStringFormat3(LocalString.GetString("(%s,分线%s)"),curScene.SceneName, mapIdx),action = this.LoadLocation}
    }
    if LuaGuildTerritorialWarsMgr:IsInPlay() or LuaGuildTerritorialWarsMgr:IsPeripheryPlay() then
        list = self:GetGuildTerritorialWarsLocation(list)
    end
    if CClientHouseMgr.Inst:IsHouseOwner() then
        table.insert(list, {text1 = LocalString.GetString("我的家园"),text2 = SafeStringFormat3("[%s]",houseName),action = this.LoadHouse})
    end
    if LuaZongMenMgr.m_ZongMenProp and LuaZongMenMgr.m_ZongMenProp.Id ~= 0 and zongMenName then
        table.insert(list, {text1 = LocalString.GetString("宗派邀请"),text2 = SafeStringFormat3("[%s]",zongMenName), action = self.LoadZongMenIvitationLetter})
    end
    if LuaTeamRecruitMgr:HasMyRecruit() then
        table.insert(list, {text1 = LocalString.GetString("我的招募"),text2 = SafeStringFormat3("[%s]",LuaTeamRecruitMgr:GetMyRecruitTargetName()), action = self.LoadMyTeamRecruit})
    end
    for i = 1, #list do
        local instance = NGUITools.AddChild(this.table.gameObject, this.transform:Find("Anchor/Background/LocationBtnTemplate").gameObject)
        instance:SetActive(true)
        local data = list[i]
        local text1 = data.text1
        local text2 = data.text2
        local action = data.action
        instance.transform:Find("Label1"):GetComponent(typeof(UILabel)).text = text1
        instance.transform:Find("Label2"):GetComponent(typeof(UILabel)).text = text2
        UIEventListener.Get(instance).onClick = DelegateFactory.VoidDelegate(function (p)
            action(this)
        end)
    end
    this.table.columns = 1
    this.table:Reposition()
    this.scrollView:ResetPosition()
end

function LuaChatInputWndEmotionBtnMgr:GetGuildTerritorialWarsLocation(list)
    local gridId = LuaGuildTerritorialWarsMgr.m_CurGridId
        local mapData = GuildTerritoryWar_Map.GetData(gridId)
        if mapData then
            local regionData = GuildTerritoryWar_Region.GetData(mapData.Region)
            if regionData then
                local text2 = SafeStringFormat3(LocalString.GetString("(%s,%s,%s)"), regionData.Name, mapData.PosX, mapData.PosY)
                list = {
                    {text1 = LocalString.GetString("我的位置"),text2 = text2,action = function (this)
                        if CChatInputMgr.Listener ~= nil then
                            local link = g_MessageMgr:FormatMessage("GuildTerritorialWar_PosShare_Link",regionData.Name, mapData.PosX, mapData.PosY, gridId) 
                            CChatInputMgr.Listener:OnSendLink(link, true)
                        end
                    end}
                }
            end
        end
    return list
end

-- function LuaChatInputWndEmotionBtnMgr:AddBtnNamedHouse(this, totalActions)
--     if CClientMainPlayer.Inst ~= nil and not System.String.IsNullOrEmpty(CClientMainPlayer.Inst.ItemProp.HouseId) and not System.String.IsNullOrEmpty(CClientHouseMgr.Inst.mCommunityName) then
--         local checkVisableFuncTable = {}
--         checkVisableFuncTable[CChatInputMgr.EParentType.ChatWnd] = true
--         checkVisableFuncTable[CChatInputMgr.EParentType.Friend] = true
--         checkVisableFuncTable[CChatInputMgr.EParentType.FriendIMGroup] = true
--         checkVisableFuncTable[CChatInputMgr.EParentType.FriendNormal] = true
--         checkVisableFuncTable[CChatInputMgr.EParentType.FriendGroup] = true
--         self:AddEmoticonBtnActiobnPair(this, LocalString.GetString("我的家园"), DelegateFactory.Action(function () 
--             this.inputType = InputType.House
--             this:LoadHouse()
--         end),checkVisableFuncTable,totalActions)
--     end
-- end

function LuaChatInputWndEmotionBtnMgr:AddBtnNamedRedPacket(this, totalActions)
    local checkVisableFuncTable = {}
    checkVisableFuncTable[CChatInputMgr.EParentType.ChatWnd] = CHongBaoMgr.s_EnableHongBao and (CChatInputMgr.ChatChannel == EChatPanel.World or CChatInputMgr.ChatChannel == EChatPanel.Guild or (CChatInputMgr.ChatChannel == EChatPanel.Sect and LuaZongMenMgr.m_HongBaoIsOpen))
    local redPacketNameDict = {}
    redPacketNameDict[EChatPanel.World] = LocalString.GetString("世界红包")
    redPacketNameDict[EChatPanel.Guild] = LocalString.GetString("帮会红包")
    redPacketNameDict[EChatPanel.Sect] = LocalString.GetString("宗派福利")
    local redPacketName = redPacketNameDict[CChatInputMgr.ChatChannel]
    self:AddEmoticonBtnActiobnPair(this, redPacketName, DelegateFactory.Action(function ()
        if CChatInputMgr.ChatChannel == EChatPanel.Sect then
            CHongBaoMgr.ShowHongBaoWnd(EnumHongbaoType.SectHongbao)
            return 
        end
        this.inputType = InputType.RedPacket
        this:LoadRedPackets()
    end),checkVisableFuncTable,totalActions)
end

function LuaChatInputWndEmotionBtnMgr:AddBtnNamedLiBao(this, totalActions)
    local checkVisableFuncTable = {}
    checkVisableFuncTable[CChatInputMgr.EParentType.ChatWnd] = CHongBaoMgr.Inst:IsLiBaoEnable() and (CChatInputMgr.ChatChannel == EChatPanel.World or CChatInputMgr.ChatChannel == EChatPanel.Guild)
    self:AddEmoticonBtnActiobnPair(this, LocalString.GetString("节日礼包"), DelegateFactory.Action(function () 
        this.inputType = InputType.LiBao
        this:LoadLiBao()
    end),checkVisableFuncTable,totalActions)
end

function LuaChatInputWndEmotionBtnMgr:AddBtnNamedPic(this, totalActions)
    local checkVisableFuncTable = {}
    checkVisableFuncTable[CChatInputMgr.EParentType.ChatWnd] = LoadPicMgr.Inst:GetSendPicCheckInfo() and (CChatInputMgr.ChatChannel == EChatPanel.Guild or CChatInputMgr.ChatChannel == EChatPanel.Team or CChatInputMgr.ChatChannel == EChatPanel.Sect)
    checkVisableFuncTable[CChatInputMgr.EParentType.Friend] = LoadPicMgr.EnableChatSendPic
    checkVisableFuncTable[CChatInputMgr.EParentType.FriendIMGroup] = LoadPicMgr.Inst:GetSendPicCheckInfo()
    checkVisableFuncTable[CChatInputMgr.EParentType.FriendNormal] = LoadPicMgr.EnableChatSendPic

    local extern = CChatInputMgr.ParentType
    local isAddPicAlertBtn = extern == CChatInputMgr.EParentType.FriendNormal
    self:AddEmoticonBtnActiobnPair(this, LocalString.GetString("图片"), DelegateFactory.Action(function () 
        this.inputType = InputType.Pic
        if isAddPicAlertBtn then
            this:LoadPicAlert()
        else
            this:LoadPic()
        end
    end),checkVisableFuncTable,totalActions)
end
