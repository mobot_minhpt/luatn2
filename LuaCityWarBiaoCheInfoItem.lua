require("common/common_include")

local UILabel = import "UILabel"
local UISprite = import "UISprite"
local CUIFx = import "L10.UI.CUIFx"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaCityWarBiaoCheInfoItem = class()
RegistClassMember(LuaCityWarBiaoCheInfoItem,"m_ResIcon")
RegistClassMember(LuaCityWarBiaoCheInfoItem,"m_Fx")
RegistClassMember(LuaCityWarBiaoCheInfoItem,"m_InfoLabel")
RegistClassMember(LuaCityWarBiaoCheInfoItem,"m_StatusLabel")

RegistClassMember(LuaCityWarBiaoCheInfoItem,"m_CloseTime")
RegistClassMember(LuaCityWarBiaoCheInfoItem,"m_UpdateTick")

function LuaCityWarBiaoCheInfoItem:Awake()
	
end

function LuaCityWarBiaoCheInfoItem:Init(mapId, progress, status, closeTime)
	self.m_ResIcon = self.transform:Find("Sprite"):GetComponent(typeof(UISprite))
	self.m_Fx = self.transform:Find("Fx"):GetComponent(typeof(CUIFx))
	self.m_InfoLabel = self.transform:Find("InfoLabel"):GetComponent(typeof(UILabel))
	self.m_StatusLabel = self.transform:Find("StatusLabel"):GetComponent(typeof(UILabel))

	self:CancelTick()
	self.m_CloseTime = nil

	local pos = self.transform.localPosition
	pos.z = 0

	local map = CityWar_Map.GetData(mapId)
	self.m_ResIcon.spriteName = (map and CityWarMapZone2ResIcon[map.Zone]) or nil
	self.m_Fx:DestroyFx()
	if status == EnumBiaoCheStatus.eSubmitRes then
		self.m_InfoLabel.text = SafeStringFormat3("%d", progress)
		self.m_StatusLabel.text = LocalString.GetString("镖车装填中")
		self.m_InfoLabel.color = EnumBiaoCheStatusTextColor.eSubmitting
		self.m_StatusLabel.color = EnumBiaoCheStatusTextColor.eSubmitting
	elseif status == EnumBiaoCheStatus.eYaYun then
		self.m_CloseTime = closeTime
		self.m_InfoLabel.text = nil
		self:ShowTimeInfo()
		self:StartTick()
		self.m_StatusLabel.text = LocalString.GetString("镖车押运中")
		self.m_InfoLabel.color = EnumBiaoCheStatusTextColor.eYaYun
		self.m_StatusLabel.color = EnumBiaoCheStatusTextColor.eYaYun
		self.m_Fx:LoadFx("fx/ui/prefab/UI_yabiaozhong.prefab")
	elseif status == EnumBiaoCheStatus.eYaYunEnd then
		self.m_InfoLabel.text = nil
		self.m_StatusLabel.text = LocalString.GetString("镖车准备中")
		self.m_StatusLabel.color = EnumBiaoCheStatusTextColor.eYaYunEnd
		pos.z = -1--置为灰色
	end
	
	self.transform.localPosition = pos
end

function LuaCityWarBiaoCheInfoItem:ShowTimeInfo()
	if not self.m_InfoLabel then return end
	if not self.m_CloseTime then return end

	local totalSeconds = self.m_CloseTime - CServerTimeMgr.Inst.timeStamp
	if totalSeconds<0 then
		totalSeconds=0
	end

	local hours = math.floor(totalSeconds / 3600)
	local minutes = math.floor((totalSeconds - hours * 3600) / 60)
	local seconds = totalSeconds-hours*3600-minutes*60
	self.m_InfoLabel.text = SafeStringFormat3("%02d:%02d:%02d", hours, minutes, seconds)
end

function LuaCityWarBiaoCheInfoItem:StartTick()
	self:CancelTick()
	self.m_UpdateTick = CTickMgr.Register(DelegateFactory.Action(function ()
		self:ShowTimeInfo()
	end), 1 * 1000, ETickType.Loop)
end

function LuaCityWarBiaoCheInfoItem:CancelTick()
	if self.m_UpdateTick then
		invoke(self.m_UpdateTick)
		self.m_UpdateTick = nil
	end
end

function LuaCityWarBiaoCheInfoItem:OnDisable()
	self:CancelTick()
end


return LuaCityWarBiaoCheInfoItem
