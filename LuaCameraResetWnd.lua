local CameraFollow = import "L10.Engine.CameraControl.CameraFollow" 
local UICommonDef = import "L10.UI.CUICommonDef"
local CCinemachineMgr=import "L10.Game.CCinemachineMgr"

CLuaCameraResetWnd = class()

RegistChildComponent(CLuaCameraResetWnd, "m_ReportBtn","ReportBtn", GameObject)
RegistChildComponent(CLuaCameraResetWnd, "m_Button","Button", GameObject)
RegistChildComponent(CLuaCameraResetWnd, "m_YanHuaView","YanHuaView", GameObject)

function CLuaCameraResetWnd:Awake()
    UIEventListener.Get(self.m_ReportBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        self:ReportFireworks()
    end)

    UIEventListener.Get(self.m_Button).onClick = DelegateFactory.VoidDelegate(function (p)
        if CCinemachineMgr.IsActive then
            CCinemachineMgr.Inst:ResetToDefault()
        else
            CameraFollow.Inst:ResetToDefault()
        end
    end)

    self.m_YanHuaView:SetActive(CLuaYanHuaEditorMgr.OpenWndByYanHuaView or CLuaYanHuaEditorMgr.InYanHuaViewStatus == true)
    if CLuaYanHuaEditorMgr.OpenWndByYanHuaView or CLuaYanHuaEditorMgr.InYanHuaViewStatus == true then
        CameraFollow.Inst.MaxSpHeight = YanHua_Setting.GetData().MaxSpHeight
    end
end

function CLuaCameraResetWnd:OnDestroy()
    if CCinemachineMgr.IsActive then
        CCinemachineMgr.Inst:ResetToDefault()
    elseif CLuaYanHuaEditorMgr.OpenWndByYanHuaView or CLuaYanHuaEditorMgr.InYanHuaViewStatus == true then
        CameraFollow.Inst:ResetToDefault()
    end

    CLuaYanHuaEditorMgr.OpenWndByYanHuaView = false
end

function CLuaCameraResetWnd:ReportFireworks()
    UICommonDef.CaptureScreen(
        "screenshot",
        true,
        false,
        nil,
        DelegateFactory.Action_string_bytes(
            function(fullPath, jpgBytes)
                CLuaYanHuaEditorMgr.ImagePath = fullPath
                CUIManager.ShowUI(CLuaUIResources.YanHuaReportWnd)
            end
        ),
        true
        )
end