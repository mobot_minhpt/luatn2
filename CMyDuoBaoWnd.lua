-- Auto Generated!!
local CDuoBaoMgr = import "L10.UI.CDuoBaoMgr"
local CMyDuobaoItem = import "L10.UI.CMyDuobaoItem"
local CMyDuoBaoWnd = import "L10.UI.CMyDuoBaoWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local EGlobalDuoBaoStatus = import "L10.UI.EGlobalDuoBaoStatus"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local QnButton = import "L10.UI.QnButton"
local UILabel = import "UILabel"
local UInt32 = import "System.UInt32"
CMyDuoBaoWnd.m_Init_CS2LuaHook = function (this) 
    this.m_MyDuoBaoTable.m_DataSource = this
    CDuoBaoMgr.Inst:QueryMyDuoBaoInfos()
    this.m_Previewer.m_LuaSelf:UpdateData({[0] = CDuoBaoMgr.Inst.GlobalDuoBaoStatus,[1] = nil})
    this.m_DuoBaoSlotLabel.text = System.String.Format(LocalString.GetString("[acf8ff]夺宝空间[-] {0}/{1}"), this.m_MyDuoBaoInfos.Count, CDuoBaoMgr.Inst.InvestLimit)
end
CMyDuoBaoWnd.m_OnEnable_CS2LuaHook = function (this) 
    this.m_MyDuoBaoTable.OnSelectAtRow = CommonDefs.CombineListner_Action_int(this.m_MyDuoBaoTable.OnSelectAtRow, MakeDelegateFromCSFunction(this.OnSelectItem, MakeGenericClass(Action1, Int32), this), true)
    this.m_HistoryGetButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_HistoryGetButton.OnClick, MakeDelegateFromCSFunction(this.OnClickHistoryGetButtonClick, MakeGenericClass(Action1, QnButton), this), true)
    EventManager.AddListener(EnumEventType.ReplyMyDuoBaoInfoEnd, MakeDelegateFromCSFunction(this.OnReplyMyDuoBaoInfoEnd, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.ReplyDuoBaoPlayStatus, MakeDelegateFromCSFunction(this.OnReplyDuoBaoPlayStatus, MakeGenericClass(Action1, UInt32), this))
end
CMyDuoBaoWnd.m_OnDisable_CS2LuaHook = function (this) 
    this.m_MyDuoBaoTable.OnSelectAtRow = CommonDefs.CombineListner_Action_int(this.m_MyDuoBaoTable.OnSelectAtRow, MakeDelegateFromCSFunction(this.OnSelectItem, MakeGenericClass(Action1, Int32), this), false)
    this.m_HistoryGetButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_HistoryGetButton.OnClick, MakeDelegateFromCSFunction(this.OnClickHistoryGetButtonClick, MakeGenericClass(Action1, QnButton), this), false)
    EventManager.RemoveListener(EnumEventType.ReplyMyDuoBaoInfoEnd, MakeDelegateFromCSFunction(this.OnReplyMyDuoBaoInfoEnd, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.ReplyDuoBaoPlayStatus, MakeDelegateFromCSFunction(this.OnReplyDuoBaoPlayStatus, MakeGenericClass(Action1, UInt32), this))
end
CMyDuoBaoWnd.m_ItemAt_CS2LuaHook = function (this, view, row) 
    local item = TypeAs(view:GetFromPool(0), typeof(CMyDuobaoItem))
    item:UpdateData(CDuoBaoMgr.Inst.GlobalDuoBaoStatus, this.m_MyDuoBaoInfos[row])
    item.OnClickItemTexture = (DelegateFactory.Action(function () 
        view:SetSelectRow(row, true)
    end))
    return item
end
CMyDuoBaoWnd.m_OnReplyMyDuoBaoInfoEnd_CS2LuaHook = function (this) 
    this.m_MyDuoBaoInfos = CDuoBaoMgr.Inst.MyDuoBaoInfos
    this.m_HasInvestRoot:SetActive(this.m_MyDuoBaoInfos.Count > 0)
    this.m_HasNoInvestRoot:SetActive(this.m_MyDuoBaoInfos.Count <= 0)

    local m_label = CommonDefs.GetComponent_GameObject_Type(this.m_HasNoInvestRoot, typeof(UILabel))
    if m_label ~= nil then
        if CDuoBaoMgr.Inst.GlobalDuoBaoStatus == EGlobalDuoBaoStatus.NotStarted or CDuoBaoMgr.Inst.GlobalDuoBaoStatus == EGlobalDuoBaoStatus.DuoBaoEnd then
            m_label.text = LocalString.GetString("全民夺宝活动已结束")
        else
            m_label.text = LocalString.GetString("您当日没有参加全民夺宝哦")
        end
    end
    this.m_MyDuoBaoTable:ReloadData(false, true)
    this.m_DuoBaoSlotLabel.text = System.String.Format(LocalString.GetString("[acf8ff]夺宝空间[-]：{0}/{1}"), this.m_MyDuoBaoInfos.Count, CDuoBaoMgr.Inst.InvestLimit)
    if this.m_MyDuoBaoInfos.Count > 0 then
        this.m_MyDuoBaoTable:SetSelectRow(0, true)
    end
end
