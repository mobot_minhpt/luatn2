require("common/common_include")
local LuaGameObject=import "LuaGameObject"
-- local EnumGuanNingWeekDataKey=import "L10.Game.EnumGuanNingWeekDataKey"
local NGUIMath=import "NGUIMath"


CLuaGuanNingPlayerAchievementWnd=class()
-- RegistClassMember(CLuaGuanNingPlayerAchievementWnd,"m_Records")
RegistClassMember(CLuaGuanNingPlayerAchievementWnd,"m_Template")
RegistClassMember(CLuaGuanNingPlayerAchievementWnd,"m_Background")
RegistClassMember(CLuaGuanNingPlayerAchievementWnd,"m_Node")

function CLuaGuanNingPlayerAchievementWnd:Init()
    local playerId=CLuaGuanNingMgr.m_SelectedPlayerId
    local records=CLuaGuanNingMgr.GetPlayerScoreRecord(playerId)

    self.m_Template=LuaGameObject.GetChildNoGC(self.transform,"Template").gameObject
    self.m_Template:SetActive(false)

    self.m_Background=LuaGameObject.GetChildNoGC(self.transform,"Background").sprite
    self.m_Node=LuaGameObject.GetChildNoGC(self.transform,"Node").gameObject

    local gridTf=LuaGameObject.GetChildNoGC(self.transform,"Grid").transform
    local grid=LuaGameObject.GetChildNoGC(self.transform,"Grid").grid
    -- self.m_Records={}
    for i=1,#records do
        -- table.insert( self.m_Records,records[i-1] )
        local go=NGUITools.AddChild(gridTf.gameObject,self.m_Template)
        go:SetActive(true)
        local key=records[i]
        LuaGameObject.GetChildNoGC(go.transform,"MarkSprite").sprite.spriteName=CLuaGuanNingMgr.GetScoreRecordSpriteName(key)
        LuaGameObject.GetChildNoGC(go.transform,"DescLabel").label.text=self:GetFullDesc(key)
    end
    grid:Reposition()
    local b=NGUIMath.CalculateRelativeWidgetBounds(self.m_Node.transform)
    self.m_Background.height=b.size.y+45
    self.m_Node:SetActive(false)
    self.m_Node:SetActive(true)
end

function CLuaGuanNingPlayerAchievementWnd:GetFullDesc(score)
    if score==EnumGuanNingWeekDataKey.eMaxSubmitFlagNumInForce then
        return LocalString.GetString("全队提交武魂旗次数最高")
    elseif score==EnumGuanNingWeekDataKey.eMaxOccupyScoreInForce then
        return LocalString.GetString("全队占领分数最高")
    elseif score==EnumGuanNingWeekDataKey.eMaxCtrlNumInForce then
        return LocalString.GetString("全队控制数量最高")
    elseif score==EnumGuanNingWeekDataKey.eMaxBaoShiNumInForce then
        return LocalString.GetString("全队暴尸数量最高")
    elseif score==EnumGuanNingWeekDataKey.eMaxDpsInForce then
        return LocalString.GetString("全队伤害最高")
    elseif score==EnumGuanNingWeekDataKey.eMaxHealInForce then
        return LocalString.GetString("全队治疗量最高")
    elseif score==EnumGuanNingWeekDataKey.eMaxUnderDamageInForce then
        return LocalString.GetString("全队承受伤害最高")
    elseif score == EnumGuanNingWeekDataKey.eMaxCannonPickInForce then
        return LocalString.GetString("全队装填分数最高")
    end
    return nil
end

return CLuaGuanNingPlayerAchievementWnd
