local CButton = import "L10.UI.CButton"
local CUIFx = import "L10.UI.CUIFx"
local CUIFxPaths = import "L10.UI.CUIFxPaths"

LuaDuiDuiLeSettlementWnd = class()

RegistChildComponent(LuaDuiDuiLeSettlementWnd,"m_OpenRankingListBtn","OpenRankingListBtn", CButton)
RegistChildComponent(LuaDuiDuiLeSettlementWnd,"m_GetRewardBtn","GetRewardBtn", CButton)
RegistChildComponent(LuaDuiDuiLeSettlementWnd,"m_Fx","Fx", CUIFx)

function LuaDuiDuiLeSettlementWnd:Init()

    local receiveRewardTimes, rewardTotalNum = LuaYuanXiao2020Mgr:GetRewardsProgress()
    self.m_GetRewardBtn.label.text = String.Format(LocalString.GetString("领取奖励({0}/{1})"), receiveRewardTimes, rewardTotalNum)
    self.m_GetRewardBtn.Enabled = receiveRewardTimes < rewardTotalNum
    self.m_Fx:LoadFx(CUIFxPaths.GetRewardFx)
    UIEventListener.Get(self.m_OpenRankingListBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function()
        LuaYuanXiao2020Mgr:OpenRankingList()
    end)
    UIEventListener.Get(self.m_GetRewardBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function()
        LuaYuanXiao2020Mgr:GetReward()
    end)
end

function LuaDuiDuiLeSettlementWnd:OnEnable()
    g_ScriptEvent:AddListener("YuanXiao2020_DuiDuiLeOpenSignUpWnd", self, "LoadRewardData")
end

function LuaDuiDuiLeSettlementWnd:OnDisable()
    g_ScriptEvent:RemoveListener("YuanXiao2020_DuiDuiLeOpenSignUpWnd", self, "LoadRewardData")
end

function LuaDuiDuiLeSettlementWnd:LoadRewardData()
    local receiveRewardTimes, rewardTotalNum = LuaYuanXiao2020Mgr:GetRewardsProgress()
    self.m_GetRewardBtn.label.text = String.Format(LocalString.GetString("领取奖励({0}/{1})"), receiveRewardTimes, rewardTotalNum)
    self.m_GetRewardBtn.Enabled = receiveRewardTimes < rewardTotalNum
end