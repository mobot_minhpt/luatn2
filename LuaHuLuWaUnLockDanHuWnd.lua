local UILabel = import "UILabel"
local QnTableView = import "L10.UI.QnTableView"
local GameObject = import "UnityEngine.GameObject"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local CUITexture = import "L10.UI.CUITexture"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr=import "L10.UI.CItemInfoMgr"
local CPayMgr = import "L10.Game.CPayMgr"

LuaHuLuWaUnLockDanHuWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaHuLuWaUnLockDanHuWnd, "LeftDescLabel", "LeftDescLabel", UILabel)
RegistChildComponent(LuaHuLuWaUnLockDanHuWnd, "LeftRewardLabel", "LeftRewardLabel", UILabel)
RegistChildComponent(LuaHuLuWaUnLockDanHuWnd, "LeftTableView", "LeftTableView", QnTableView)
RegistChildComponent(LuaHuLuWaUnLockDanHuWnd, "LeftUnLockBtn", "LeftUnLockBtn", GameObject)
RegistChildComponent(LuaHuLuWaUnLockDanHuWnd, "LeftOldLuCount", "LeftOldLuCount", UILabel)
RegistChildComponent(LuaHuLuWaUnLockDanHuWnd, "LeftNewLuCount", "LeftNewLuCount", UILabel)
RegistChildComponent(LuaHuLuWaUnLockDanHuWnd, "RightDescLabel", "RightDescLabel", UILabel)
RegistChildComponent(LuaHuLuWaUnLockDanHuWnd, "RightFireLabel", "RightFireLabel", UILabel)
RegistChildComponent(LuaHuLuWaUnLockDanHuWnd, "RightTableView", "RightTableView", QnTableView)
RegistChildComponent(LuaHuLuWaUnLockDanHuWnd, "RightUnLockBtn", "RightUnLockBtn", GameObject)
RegistChildComponent(LuaHuLuWaUnLockDanHuWnd, "LeftUnLockLabel", "LeftUnLockLabel", UILabel)
RegistChildComponent(LuaHuLuWaUnLockDanHuWnd, "RightUnLockLabel", "RightUnLockLabel", UILabel)
RegistChildComponent(LuaHuLuWaUnLockDanHuWnd, "RightOldLuCount", "RightOldLuCount", UILabel)
RegistChildComponent(LuaHuLuWaUnLockDanHuWnd, "RightNewLuCount", "RightNewLuCount", UILabel)
RegistChildComponent(LuaHuLuWaUnLockDanHuWnd, "RightFireSpeedLabel", "RightFireSpeedLabel", UILabel)
RegistChildComponent(LuaHuLuWaUnLockDanHuWnd, "DanLuLevel", "DanLuLevel", UILabel)
RegistChildComponent(LuaHuLuWaUnLockDanHuWnd, "LeftProgressLoadedLabel", "LeftProgressLoadedLabel", GameObject)
RegistChildComponent(LuaHuLuWaUnLockDanHuWnd, "RightProgressLoadedLabel", "RightProgressLoadedLabel", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaHuLuWaUnLockDanHuWnd,"m_CurLevel")
RegistClassMember(LuaHuLuWaUnLockDanHuWnd,"m_TotalProgress")
RegistClassMember(LuaHuLuWaUnLockDanHuWnd,"m_Setting")
function LuaHuLuWaUnLockDanHuWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.LeftUnLockBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLeftUnLockBtnClick()
	end)

	UIEventListener.Get(self.RightUnLockBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRightUnLockBtnClick()
	end)

    --@endregion EventBind end
	self.m_Setting = ZhanLing_Setting.GetData()
	self.LeftTableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return self.m_Setting.Vip1Prize.Length / 2
        end,
        function(item,row)
            self:InitItem(item,row,1)
        end)
	
	self.RightTableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return self.m_Setting.Vip2Prize.Length / 2
        end,
        function(item,row)
            self:InitItem(item,row,2)
        end)

	self.LeftTableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
		self:OnSelectItemAt(row,1)
	end)
	self.RightTableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
		self:OnSelectItemAt(row,2)
	end)
	
end

function LuaHuLuWaUnLockDanHuWnd:OnEnable()
	g_ScriptEvent:AddListener("UnLockLianDanLuVipSuccess", self, "OnUnLockLianDanLuVipSuccess")
end
function LuaHuLuWaUnLockDanHuWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UnLockLianDanLuVipSuccess", self, "OnUnLockLianDanLuVipSuccess")
end

function LuaHuLuWaUnLockDanHuWnd:Init()
	self.m_CurLevel,self.m_TotalProgress = LuaHuLuWa2022Mgr.GetCurZhanLingLevel()

	local vip2AddRate = self.m_Setting.Vip2AddRate
    local vip2AddValue = self.m_Setting.Vip2AddValue
    local vip1AddValue = self.m_Setting.Vip1AddValue

	self.DanLuLevel.text = self.m_CurLevel
	self.LeftUnLockBtn:SetActive(not LuaHuLuWa2022Mgr.m_IsVip1)
	self.RightUnLockBtn:SetActive(not LuaHuLuWa2022Mgr.m_IsVip2)
	--Left vip1
	self.LeftDescLabel.text = SafeStringFormat3(LocalString.GetString("炼丹进度立即增加%d炉"),vip1AddValue/100)
	self.LeftOldLuCount.text = self.m_CurLevel
	local nextLeftLevel = math.min(49,self.m_CurLevel + vip1AddValue/100)
	self.LeftNewLuCount.text = nextLeftLevel
	self.LeftTableView:ReloadData(false,false)
	self.LeftOldLuCount.gameObject:SetActive(not LuaHuLuWa2022Mgr.m_IsVip1)
	self.LeftNewLuCount.gameObject:SetActive(not LuaHuLuWa2022Mgr.m_IsVip1)
	self.LeftProgressLoadedLabel.gameObject:SetActive(LuaHuLuWa2022Mgr.m_IsVip1)
	--Right vip2
	self.RightDescLabel.text = SafeStringFormat3(LocalString.GetString("炼丹进度立即增加%d炉"),vip2AddValue/100)
	self.RightOldLuCount.text = self.m_CurLevel
	local nextRightLevel = math.min(49,self.m_CurLevel + vip2AddValue/100)
	self.RightNewLuCount.text = nextRightLevel
	self.RightFireLabel.text = SafeStringFormat3(LocalString.GetString("火候增加速度提升至%d%%"),vip2AddRate*100)
	self.RightFireSpeedLabel.text = SafeStringFormat3("x%.1f",vip2AddRate)
	self.RightTableView:ReloadData(false,false)
	self.RightOldLuCount.gameObject:SetActive(not LuaHuLuWa2022Mgr.m_IsVip2)
	self.RightNewLuCount.gameObject:SetActive(not LuaHuLuWa2022Mgr.m_IsVip2)
	self.RightProgressLoadedLabel.gameObject:SetActive(LuaHuLuWa2022Mgr.m_IsVip2)

	self.transform:Find("Center/DanLu/Texture/Texture").gameObject:SetActive(not LuaHuLuWa2022Mgr.m_IsVip2)
	self.transform:Find("Center/DanLu/Texture/Texture (2)").gameObject:SetActive(not LuaHuLuWa2022Mgr.m_IsVip2)
	self.transform:Find("Center/DanLu/Texture/Texture (1)").gameObject:SetActive(not LuaHuLuWa2022Mgr.m_IsVip1)
	self.transform:Find("Center/DanLu/Texture/Texture (3)").gameObject:SetActive(not LuaHuLuWa2022Mgr.m_IsVip1)
end

function LuaHuLuWaUnLockDanHuWnd:InitItem(item,row,vip)

	local data
	if vip == 1 then
		data = self.m_Setting.Vip1Prize
	else
		data = self.m_Setting.Vip2Prize
	end
	local itemId = data[row*2]
	local count = data[row*2 + 1]

	local icon = item.transform:GetComponent(typeof(CUITexture))
	local countLabel = item.transform:Find("Label"):GetComponent(typeof(UILabel))
	local itemdata = Item_Item.GetData(itemId)
	if itemdata then
		icon:LoadMaterial(itemdata.Icon)
		countLabel.text = count > 1 and count or nil
	end
end

function LuaHuLuWaUnLockDanHuWnd:OnSelectItemAt(row,vip)
	local data
	if vip == 1 then
		data = self.m_Setting.Vip1Prize
	else
		data = self.m_Setting.Vip2Prize
	end
	local itemId = data[row*2]
	CItemInfoMgr.ShowLinkItemTemplateInfo(itemId,false,nil,AlignType.Default, 0, 0, 0, 0)

end

function LuaHuLuWaUnLockDanHuWnd:OnUnLockLianDanLuVipSuccess(vip)
end
--@region UIEvent

function LuaHuLuWaUnLockDanHuWnd:OnLeftUnLockBtnClick()
	local pid = self.m_Setting.Vip1PID
    if not pid then return end
    CPayMgr.Inst:BuyProduct(CPayMgr.Inst:PIDConverter(pid), 0)
end

function LuaHuLuWaUnLockDanHuWnd:OnRightUnLockBtnClick()
	local pid = self.m_Setting.Vip2PID
    if not pid then return end
    CPayMgr.Inst:BuyProduct(CPayMgr.Inst:PIDConverter(pid), 0)
end


--@endregion UIEvent

