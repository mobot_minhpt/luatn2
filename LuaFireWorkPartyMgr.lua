local Color = import "UnityEngine.Color"
local CCustomParticleEffect = import "L10.Game.CCustomParticleEffect"
local CEffectMgr = import "L10.Game.CEffectMgr"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local Vector3 = import "UnityEngine.Vector3"
local CScene = import "L10.Game.CScene"
local Quaternion = import "UnityEngine.Quaternion"

local CCCChatMgr = import "L10.Game.CCCChatMgr"
local CCPlayerMgr = import "L10.Game.CCPlayerMgr"
local CCPlayer = import "CCPlayer"
local CClientCCVideoObj = import "L10.Game.CClientCCVideoObj"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local PlayerSettings = import "L10.Game.PlayerSettings"
local CRenderScene = import "L10.Engine.Scene.CRenderScene"
local NGUIText = import "NGUIText"
local CommonDefs = import "L10.Game.CommonDefs"
local Texture2D = import "UnityEngine.Texture2D"
local CWorldPositionFX = import "L10.Game.CWorldPositionFX"
local CCustomParticleMultyEffect = import "L10.Game.CCustomParticleMultyEffect"
local Rect = import "UnityEngine.Rect"
local TextureFormat = import "UnityEngine.TextureFormat"

LuaFireWorkPartyMgr = {}

LuaFireWorkPartyMgr.PartyStatus = {}
LuaFireWorkPartyMgr.NoticeList = {}
LuaFireWorkPartyMgr.SelectFriendId = nil
LuaFireWorkPartyMgr.SelectFriendName = nil
LuaFireWorkPartyMgr.TextureGap = 1

function LuaFireWorkPartyMgr.OpenZhongChouWnd()
    Gac2Gas.RequestOpenFireworkPartyWnd()
end

function LuaFireWorkPartyMgr.OpenSendFireworkToFriendWnd()
    CUIManager.ShowUI(CLuaUIResources.SendShowLoveFireWorkWnd)
end

function LuaFireWorkPartyMgr.LightConfessFireworkInScene(partyId, playerId, playerName, targetId, targetName, x, y, dir,bloomHeight,scale)
    local party = YuanXiao_FireworkPartySchedule.GetData(partyId)

    if not party then return end 

    local mapId = party.MapId
    --当前所在场景为party场景才放烟花
    if CScene.MainScene and CScene.MainScene.SceneTemplateId == mapId then
        local terrianHeight = CScene.MainScene:GetLogicHeight(math.floor(x * 64), math.floor(y * 64))
        local pos = Vector3(x,terrianHeight,y)
        local textColor = Color.red
        local target = CUIManager.s_GlobalFont
        local drawOffsetX,drawOffsetY,textGap,spaceGap,rowHeight=0,0,0,10,0
        local textureArray = {} 
        local rot = dir
        local textureColorTypes = {}
        local textIsCnTbl = {}

        local name1 = targetName
        local name2 = playerName
        local heart = CCustomParticleEffect.TextToTexture(target,LocalString.GetString(" ♥      "),textColor,0,0,drawOffsetX,drawOffsetY,textGap,spaceGap,rowHeight)
        local pos2 = Vector3(pos.x,pos.y+bloomHeight,pos.z)

        local str1 = LocalString.StrH2V(name1,true)
        local tbl1 =  g_LuaUtil:StrSplit(str1,"\n")
        for idx=#tbl1,1,-1 do 
            local v = tbl1[idx]
            local isCn = false
            for c in string.gmatch(v,".") do
                if System.String.IsNullOrEmpty(c) then
                    isCn = true
                end
            end 
            local texture = CCustomParticleEffect.TextToTexture(target,v,textColor,0,0,drawOffsetX,drawOffsetY,textGap,spaceGap,rowHeight)
            if isCn then               
                table.insert(textureArray,texture)
                table.insert(textureColorTypes,0)
                table.insert(textIsCnTbl,isCn)
            else
                local len = #v
                if len > 2 then len = 2 end
                for t=len,1,-1 do
                    local perWidth = texture.width / len
                    local perTexture = CreateFromClass(Texture2D, math.floor(perWidth), math.floor(texture.height), TextureFormat.RGBA32, false)
                    local rect = CreateFromClass(Rect, 0, 0, perTexture, texture.height)
                    for x=0,perWidth-1,1 do
                        for y=0,texture.height-1,1 do 
                            local xx = (t-1)*perWidth + x
                            local pcolor = texture:GetPixel(xx,y)
                            perTexture:SetPixel(x,y,pcolor)
                        end
                    end
                    perTexture:Apply()
                    table.insert(textureArray,perTexture)
                    table.insert(textureColorTypes,0)
                    table.insert(textIsCnTbl,isCn)
                end
            end
        end
       
        table.insert(textureArray,heart)
        table.insert(textureColorTypes,1)
        table.insert(textIsCnTbl,true)
        local str2 = LocalString.StrH2V(name2,true)
        local tbl2 =  g_LuaUtil:StrSplit(str2,"\n")
        for idx=#tbl2,1,-1 do 
            local v = tbl2[idx]
            local isCn = false
            for c in string.gmatch(v,".") do
                if System.String.IsNullOrEmpty(c) then
                    isCn = true
                end
            end

            local texture = CCustomParticleEffect.TextToTexture(target,v,textColor,0,0,drawOffsetX,drawOffsetY,textGap,spaceGap,rowHeight)
            if isCn then
                table.insert(textureArray,texture)
                table.insert(textureColorTypes,0)
                table.insert(textIsCnTbl,isCn)
            else
                local len = #v
                for t=len,1,-1 do
                    local perWidth = texture.width / len
                    local perTexture = CreateFromClass(Texture2D, math.floor(perWidth), math.floor(texture.height), TextureFormat.RGBA32, false)
                    local rect = CreateFromClass(Rect, 0, 0, perTexture, texture.height)
                    for x=0,perWidth-1,1 do
                        for y=0,texture.height-1,1 do 
                            local xx = (t-1)*perWidth + x
                            local pcolor = texture:GetPixel(xx,y)
                            perTexture:SetPixel(x,y,pcolor)
                        end
                    end
                    perTexture:Apply()
                    table.insert(textureArray,perTexture)
                    table.insert(textureColorTypes,0)
                    table.insert(textIsCnTbl,isCn)
                end
            end      
        end
        
        local array = Table2Array(textureArray, MakeArrayClass(Texture2D))
        local colorTypeArray = Table2Array(textureColorTypes, MakeArrayClass(int))

        local fx = CEffectMgr.Inst:AddCustomYanHuaWpFX(88801851, pos, rot,1,nil,nil,0, 1, -1, EnumWarnFXType.None, nil, 0, 0)
        local biaobaiFxId = YuanXiao_FireworkPartySetting.GetData().LoveFireWorkFxId

        local onLoadFinish = DelegateFactory.Action_GameObject(function(go)
            if not go then return end
            local wf = go.transform:GetComponent(typeof(CWorldPositionFX))
            wf.m_Scale = scale
            local multyEffects = CommonDefs.GetComponentsInChildren_Component_Type(go.transform, typeof(CCustomParticleMultyEffect))
            if multyEffects then
                local multyEffect = multyEffects[0]
                if multyEffect then
                    multyEffect.transform.localRotation =Quaternion.Euler(Vector3(0,0,0))
                    multyEffect.customEffectList:Clear()
                    multyEffect.customEffectList:Add(multyEffect.customEffect)
                    local centerPos = multyEffect.centerPos
                    local tempPos = Vector3(0,0,0)
                    if array then
                        if array then
                            local centerIndex = math.floor(array.Length/2)
                            local widths = {}
                            local posx = {}
                            for i=0,array.Length-1,1 do 
                                local effect
                                if i ~= 0 then
                                    effect = CommonDefs.Object_Instantiate(multyEffect.customEffect.gameObject)
                                    effect = effect.transform:GetComponent(typeof(CCustomParticleEffect))
                                    multyEffect.customEffectList:Add(effect)
                                else
                                    effect = multyEffect.customEffectList[0]
                                end
                                effect.useTextureColor = true
                                effect.s_EmitCount = 200
                                if CommonDefs.IsIOSPlatform() then
                                    effect.size = 0.5
                                else
                                    effect.size = 0.3
                                end
                                
                                local texwitdh = array[i].width + LuaFireWorkPartyMgr.TextureGap
                                table.insert(widths,texwitdh)
                                
                            end

                            local lposx = 0
                            for k=centerIndex,0,-1 do
                                tempPos.x = lposx
                                local effect = multyEffect.customEffectList[k]
                                effect.transform.parent = multyEffect.transform
                                effect.sampleTexture = array[k]
                                effect.transform.localPosition = tempPos
                                effect.transform.localRotation = Quaternion.Euler(0,0,0)
                                if k >=1 then
                                    lposx = lposx - widths[k+1]*0.01/2 - widths[k]*0.01/2
                                end
                                local isCn = textIsCnTbl[k+1]                                                              
                            end
                            local rposx = 0 + widths[centerIndex+1]*0.01/2
                            for k2=centerIndex+1,array.Length-1,1 do
                                rposx = rposx + widths[k2+1]*0.01/2
                                tempPos.x = rposx
                                local effect = multyEffect.customEffectList[k2]
                                effect.transform.parent = multyEffect.transform
                                effect.sampleTexture = array[k2]
                                effect.transform.localPosition = tempPos
                                effect.transform.localRotation = Quaternion.Euler(0,0,0)
                                rposx = rposx + widths[k2+1]*0.01/2
                                
                                local isCn = textIsCnTbl[k2+1]                       
                            end                           
                        end
                    end

                    for i=0,multyEffect.customEffectList.Count-1,1 do 
                        multyEffect.customEffectList[i].m_Scale = scale
                    end
                end
            end
            wf.Inited = false
            wf:Init(wf.m_FX, pos2, rot, 0, 0, scale, wf.ParticleColors, nil,wf.ParticleHSVColors)
        end)
        local yuanhua = CEffectMgr.Inst:AddWorldPositionFX(biaobaiFxId, pos2, rot, 0, 1, -1, EnumWarnFXType.None, nil, 0, 0, onLoadFinish)
    end
end

--零星烟花
LuaFireWorkPartyMgr.CurLightingFireworkPartyId = nil
LuaFireWorkPartyMgr.SingleFireworkList = {}
function LuaFireWorkPartyMgr.OnFireworkPartyStart(id,endTime)
    CameraFollow.Inst.MaxSpHeight = YuanXiao_FireworkPartySetting.GetData().MaxSpHeight
    --初始化烟花燃放列表 按燃放顺序排序
    if not LuaFireWorkPartyMgr.SingleFireworkList[id] then
        LuaFireWorkPartyMgr.SingleFireworkList[id] = {}
        YuanXiao_FireWorkParty.Foreach(function(key,data)
            local t = {}
            t.id = data.ID 
            t.startDelay = data.StartDelay
            t.endDelay = data.StartDelay + data.LastTime
            if data.Session == id then
                table.insert(LuaFireWorkPartyMgr.SingleFireworkList[id],t)
            end
        end)
        local sort_func = function(a,b)
            return a.startDelay < b.startDelay
        end
        table.sort(LuaFireWorkPartyMgr.SingleFireworkList[id],sort_func)
    end
    local count = #LuaFireWorkPartyMgr.SingleFireworkList[id]
    if count == 0 then
        print("count == 0")
        return
    end
    local last = LuaFireWorkPartyMgr.SingleFireworkList[id][count]
    local lastStratTime = 20

    if  LuaFireWorkPartyMgr.SessionFireworkTick then
        UnRegisterTick(LuaFireWorkPartyMgr.SessionFireworkTick)
        LuaFireWorkPartyMgr.SessionFireworkTick = nil
    end
    local seesionFunc = function()
        if LuaFireWorkPartyMgr.SingleFireworkTisk then
            UnRegisterTick(LuaFireWorkPartyMgr.SingleFireworkTisk)
            LuaFireWorkPartyMgr.SingleFireworkTisk = nil
        end
        local idx = 0
        LuaFireWorkPartyMgr.SingleFireworkTisk = RegisterTick(function()
            idx = idx + 1
            local fireData = LuaFireWorkPartyMgr.SingleFireworkList[id][idx]
            if fireData then
                local yanhuaId = fireData.id
                local ydata = YuanXiao_FireWorkParty.GetData(yanhuaId)
                local fxId = ydata.FxId 
                local x,z = ydata.Position[0],ydata.Position[1]
                local fxScale = ydata.FxScale >0 and ydata.FxScale or 1

                local colors = nil
                if ydata.FxColor and ydata.FxColor ~= "" then
                    local color = NGUIText.ParseColor24(ydata.FxColor, 0)
                    if color then
                        colors = CreateFromClass(MakeArrayClass(Color), 1)
                        colors[0] = color
                    end
                end

                if  CRenderScene.Inst then
                    local y = CRenderScene.Inst:SampleLogicHeight(math.floor(x) + 0.5, math.floor(z) + 0.5)
                    local pos = Vector3(x,y,z)
                    local pos2 = Vector3(x,y+ydata.BloomHeight,z)
                    CEffectMgr.Inst:AddCustomYanHuaWpFX(88801851, pos, 0,1,colors,nil,0, 1, -1, EnumWarnFXType.None, nil, 0, 0)
                    CEffectMgr.Inst:AddCustomYanHuaWpFX(fxId, pos2, 0, fxScale,colors,nil,1000, 1, -1, EnumWarnFXType.None, nil, 0, 0)
                end
            end
        end,1000)
    end
    LuaFireWorkPartyMgr.SessionFireworkTick = RegisterTick(function()
        --每次走一遍表里的烟花 循环播放
        seesionFunc()    
    end,lastStratTime*1000)
end

function LuaFireWorkPartyMgr.OnFireworkPartyEnd(id)
    CameraFollow.Inst.MaxSpHeight = 0
    if  LuaFireWorkPartyMgr.SessionFireworkTick then
        UnRegisterTick(LuaFireWorkPartyMgr.SessionFireworkTick)
        LuaFireWorkPartyMgr.SessionFireworkTick = nil
    end
    if LuaFireWorkPartyMgr.SingleFireworkTisk then
        UnRegisterTick(LuaFireWorkPartyMgr.SingleFireworkTisk)
        LuaFireWorkPartyMgr.SingleFireworkTisk = nil
    end
end

--直播大屏
LuaFireWorkPartyMgr.ZhiBoObj = nil
if LuaFireWorkPartyMgr.SingleFireworkTisk then
    UnRegisterTick(LuaFireWorkPartyMgr.SingleFireworkTisk)
    LuaFireWorkPartyMgr.SingleFireworkTisk = nil
end

function LuaFireWorkPartyMgr.AddListener()
    g_ScriptEvent:AddListener("OnCCPlayerBeginPlay",LuaFireWorkPartyMgr,"OnCCPlayerBeginPlay")
    g_ScriptEvent:AddListener("GasDisconnect", LuaFireWorkPartyMgr, "OnGasDisconnect")
    g_ScriptEvent:AddListener("RenderSceneInit", LuaFireWorkPartyMgr, "OnRenderSceneInit")
    g_ScriptEvent:AddListener("MainPlayerMoveStepped", LuaFireWorkPartyMgr, "OnPlayerMove")
    g_ScriptEvent:AddListener("OnSoundEnableChanged",LuaFireWorkPartyMgr,"OnSoundEnableChanged")
end

function LuaFireWorkPartyMgr.RemoveListener()
    g_ScriptEvent:RemoveListener("OnCCPlayerBeginPlay",LuaFireWorkPartyMgr,"OnCCPlayerBeginPlay")
    g_ScriptEvent:RemoveListener("GasDisconnect", LuaFireWorkPartyMgr, "OnGasDisconnect")
    g_ScriptEvent:RemoveListener("RenderSceneInit", LuaFireWorkPartyMgr, "OnRenderSceneInit")
    g_ScriptEvent:RemoveListener("MainPlayerMoveStepped", LuaFireWorkPartyMgr, "OnPlayerMove")
    g_ScriptEvent:RemoveListener("OnSoundEnableChanged",LuaFireWorkPartyMgr,"OnSoundEnableChanged")
end

function LuaFireWorkPartyMgr.SyncCCInfo(id, RemoteChannelInfo)
    -- print("LuaFireWorkPartyMgr.SyncCCInfo")
    LuaFireWorkPartyMgr.CurPartyId = id
    LuaFireWorkPartyMgr.LeavePartyScene()
    LuaFireWorkPartyMgr.IsPlaying = true
    CCCChatMgr.Inst:InitCurrentStation(RemoteChannelInfo)
    local url = CCCChatMgr.Inst.CCLiveURL

    SoundManager.Inst:StopBGMusic()
    LuaFireWorkPartyMgr.AddListener()

    CCPlayerMgr.Inst:Play(url)
    CCPlayerMgr.Inst:Lock("FIREWORKPARTY_ZHIBO_CONFLICT")
end

function LuaFireWorkPartyMgr.OnCCPlayerBeginPlay( )
    -- print("OnCCPlayerBeginPlay")
    LuaFireWorkPartyMgr.OnBeginPlay()
end

function LuaFireWorkPartyMgr.OnGasDisconnect()
    -- print("OnGasDisconnect")
    LuaFireWorkPartyMgr.StopZhiBo()
end

function LuaFireWorkPartyMgr.OnPlayerMove()  
    local t = CameraFollow.Inst.TValue
    if  t <= CameraFollow.s_HideUINearBar or t > CameraFollow.m_HideUIFarBar then 
        return 
    end
    LuaFireWorkPartyMgr.ExitCameraLock()
end

function LuaFireWorkPartyMgr.ExitCameraLock()
    if LuaFireWorkPartyMgr.CameraLock then
        CameraFollow.Inst:SetFollowObj(CClientMainPlayer.Inst.RO, nil)
        CameraFollow.Inst:EnableZoom(true)
        --LuaFireWorkPartyMgr.ShowHideUI(true)
        LuaFireWorkPartyMgr.CameraLock = false
        LuaFireWorkPartyMgr.OnSoundEnableChanged()
    end
end

function LuaFireWorkPartyMgr.OnRenderSceneInit()
    if LuaFireWorkPartyMgr.CurPartyId then
        local party = YuanXiao_FireworkPartySchedule.GetData(LuaFireWorkPartyMgr.CurPartyId)
        if party then
            local partySceneId = party.MapId
            if CScene.MainScene and CScene.MainScene.SceneTemplateId == partySceneId then
                return
            end
        end
    end

    LuaFireWorkPartyMgr.LeavePartyScene()
end

function LuaFireWorkPartyMgr.LeavePartyScene()
    LuaFireWorkPartyMgr.RemoveListener()
    LuaFireWorkPartyMgr.OnSoundEnableChanged()--重新设置CC直播是否开启语音

    if LuaFireWorkPartyMgr.ZhiBoObj then
        LuaFireWorkPartyMgr.ZhiBoObj.RO:Destroy()
    end 
    LuaFireWorkPartyMgr.ZhiBoObj = nil

    CCPlayerMgr.Inst:Unlock()
    if CCPlayer.Inst:IsPlaying() then
        CCPlayerMgr.Inst:Stop()
    end

    CClientCCVideoObj.Inited = false
    LuaFireWorkPartyMgr.IsPlaying = false
    SoundManager.Inst:StartBGMusic()
end

function LuaFireWorkPartyMgr.OnSoundEnableChanged()
    local setting = PlayerSettings.SoundEnabled
    if not CCPlayer.Inst:IsPlaying() then return end
    
    if LuaFireWorkPartyMgr.CameraLock then
        CCPlayer.Inst:MuteAudio(false)
    else
        CCPlayer.Inst:MuteAudio(not setting)
    end

end

function LuaFireWorkPartyMgr.SetUpCamera()
    CameraFollow.Inst:ResetToDefault(true,false)
    LuaFireWorkPartyMgr.CameraLock = true
    LuaFireWorkPartyMgr.OnSoundEnableChanged()
    --停止寻路
    Gas2Gac.TryStopTrack()
end

LuaFireWorkPartyMgr.CameraLock = false
function LuaFireWorkPartyMgr.OnBeginPlay()
    if LuaFireWorkPartyMgr.ZhiBoObj then 
        LuaFireWorkPartyMgr.ZhiBoObj:Init()
        return
    end

    --@策划 todo
    local res = YuanXiao_FireworkPartySetting.GetData().ScreenObjRes
    if not LuaFireWorkPartyMgr.CurPartyId then return end 

    local party = YuanXiao_FireworkPartySchedule.GetData(LuaFireWorkPartyMgr.CurPartyId)
    if not party then  return end 

    local transforms = party.ScreenTransform

    local valuetable = g_LuaUtil:StrSplitAdv(transforms, ";")
    local vtable = g_LuaUtil:StrSplitAdv(valuetable[1], ",")

    local obj = CClientCCVideoObj.Create(res,vtable[1],vtable[2],vtable[3],vtable[4],vtable[5])
    LuaFireWorkPartyMgr.ZhiBoObj = obj
    LuaFireWorkPartyMgr.OnSoundEnableChanged()
end

function LuaFireWorkPartyMgr.StopZhiBo()
    LuaFireWorkPartyMgr.LeavePartyScene()
    LuaFireWorkPartyMgr.OnPlayerMove()
    CScene.MainScene.AllowZuoQi = true
end
