local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local Money = import "L10.Game.Money"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CServerTimeMgr=import "L10.Game.CServerTimeMgr"
local LingShou_LingShou=import "L10.Game.LingShou_LingShou"
local CGetMoneyMgr=import "L10.UI.CGetMoneyMgr"
local AlignType = import "L10.UI.CTooltip+AlignType"
local CItemMgr=import "L10.Game.CItemMgr"
local CItemAccessListMgr=import "L10.UI.CItemAccessListMgr"
local Item_Item=import "L10.Game.Item_Item"
local Collider=import "UnityEngine.Collider"
local CSkillInfoMgr=import "L10.UI.CSkillInfoMgr"
local Skill_AllSkills=import "L10.Game.Skill_AllSkills"
local CUITexture=import "L10.UI.CUITexture"
local CCurentMoneyCtrl=import "L10.UI.CCurentMoneyCtrl"
local CItemCountUpdate=import "L10.UI.CItemCountUpdate"


local CLingShouMgr = import "L10.Game.CLingShouMgr"
local QnTabView = import "L10.UI.QnTabView"
local CLingShouModelTextureLoader=import "L10.UI.CLingShouModelTextureLoader"

CLuaLingShouSkillDetailView = class()
RegistClassMember(CLuaLingShouSkillDetailView,"textureLoader")
RegistClassMember(CLuaLingShouSkillDetailView,"tipBtn")
RegistClassMember(CLuaLingShouSkillDetailView,"learnBtn")

RegistClassMember(CLuaLingShouSkillDetailView,"m_SkillSlots")
RegistClassMember(CLuaLingShouSkillDetailView,"m_UpgradeButton")
RegistClassMember(CLuaLingShouSkillDetailView,"m_ItemCountUpdate")
RegistClassMember(CLuaLingShouSkillDetailView,"m_MatEnough")
RegistClassMember(CLuaLingShouSkillDetailView,"m_MoneyCtrl")
RegistClassMember(CLuaLingShouSkillDetailView,"m_ExpCtrl")
RegistClassMember(CLuaLingShouSkillDetailView,"m_ExpCost")
RegistClassMember(CLuaLingShouSkillDetailView,"m_HintLabel")
-- RegistClassMember(CLuaLingShouSkillDetailView,"ownExpIcon")
-- RegistClassMember(CLuaLingShouSkillDetailView,"needExpIcon")

RegistClassMember(CLuaLingShouSkillDetailView,"m_NeedCount")
RegistClassMember(CLuaLingShouSkillDetailView,"m_MatTemplateId")
RegistClassMember(CLuaLingShouSkillDetailView,"m_SelectedSkillPos")
RegistClassMember(CLuaLingShouSkillDetailView,"m_TabView")
RegistClassMember(CLuaLingShouSkillDetailView,"m_WndTabView")
--jieban
RegistClassMember(CLuaLingShouSkillDetailView,"m_UpgrateNormalItemId")
RegistClassMember(CLuaLingShouSkillDetailView,"m_UpgradeProfessionalItemId")
RegistClassMember(CLuaLingShouSkillDetailView,"m_UpgrateNormalItemConsume")
RegistClassMember(CLuaLingShouSkillDetailView,"m_UpgradeProfessionalItemConsume")

RegistClassMember(CLuaLingShouSkillDetailView,"m_IsLingShouJieBan")
RegistClassMember(CLuaLingShouSkillDetailView,"m_LastZhandouli")
RegistClassMember(CLuaLingShouSkillDetailView,"m_LastZhandouli2Id")

RegistClassMember(CLuaLingShouSkillDetailView,"m_FxNode")

RegistClassMember(CLuaLingShouSkillDetailView,"m_UpgradeNotice")

function CLuaLingShouSkillDetailView:Awake()
    self.m_ExpCost=0

    self.m_WndTabView=self.transform.parent:GetComponent(typeof(QnTabView))

    self.m_SkillSlots={}
    self.m_SelectedSkillPos=0
    self.m_MatEnough=false
    self.m_NeedCount=0
    self.m_MatTemplateId=0

    self.m_TabView=FindChild(self.transform,"TabBar"):GetComponent(typeof(QnTabView))
    
    self.m_UpgradeButton = FindChild(self.transform,"UpgradeButton").gameObject
    self.m_ItemCountUpdate=FindChild(self.transform,"Upgrade"):GetComponent(typeof(CItemCountUpdate))
    self.m_MoneyCtrl = FindChild(self.transform,"QnCostAndOwnMoney"):GetComponent(typeof(CCurentMoneyCtrl))
    self.m_ExpCtrl = FindChild(self.transform,"QnCostAndOwnMoney2")--:GetComponent(typeof(CCurentMoneyCtrl))
    self.m_ExpCtrl.gameObject:SetActive(false)
    self.m_HintLabel = FindChild(self.transform,"HintLabel"):GetComponent(typeof(UILabel))

    self.textureLoader = self.transform:Find("Normal/1/TextureLoader"):GetComponent(typeof(CLingShouModelTextureLoader))
    self.tipBtn = self.transform:Find("Normal/1/TipBtn").gameObject
    self.learnBtn = self.transform:Find("Normal/1/LearnBtn").gameObject
    self.m_FxNode = self.transform:Find("Normal/1/ZhandouliLabel/FxNode"):GetComponent(typeof(CUIFx))

    local setting = LingShouPartner_Setting.GetData()
    self.m_UpgrateNormalItemId=setting.UpgrateNormalItemId
    self.m_UpgradeProfessionalItemId=setting.UpgradeProfessionalItemId
    self.m_UpgrateNormalItemConsume = {}
    for i=1,setting.UpgradeNormalItemConsume.Length do
        table.insert( self.m_UpgrateNormalItemConsume ,setting.UpgradeNormalItemConsume[i-1] )
    end
    self.m_UpgradeProfessionalItemConsume = {}
    for i=1,setting.UpgradeProfessionalItemConsume.Length do
        table.insert( self.m_UpgradeProfessionalItemConsume ,setting.UpgradeProfessionalItemConsume[i-1] )
    end

    UIEventListener.Get(self.tipBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        if self:IsJieBan() then
            g_MessageMgr:ShowMessage("JieBan_SkillTips")
        else
            g_MessageMgr:ShowMessage("LINGSHOU_JINENG_TIP")
        end
    end)

    UIEventListener.Get(self.learnBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        CUIManager.ShowUI(CUIResources.LingShouLearnSkillWnd)
    end)

    self:InitSkillSlots()
    self:InitUpgradePane()

    UIEventListener.Get(self.m_UpgradeButton).onClick = DelegateFactory.VoidDelegate(function(go)
        if self.m_TabView.CurrentIndex==0 then
            self:OnClickUpgradeButton(go)
        else
            self:OnClickUpgradeJieBanSkillButton(go)
        end
    end)

    self.m_TabView.OnSelect=DelegateFactory.Action_QnTabButton_int(function(btn,index)
        self:OnSelectTabView(index)
    end)
    -- self:OnSelectTabView(0)
end

function CLuaLingShouSkillDetailView:IsJieBan()
    return self.m_TabView.CurrentIndex==1
end
function CLuaLingShouSkillDetailView:IsProfessionalSkill()
    return self.m_SelectedSkillPos>5
end
--结伴技能升级
function CLuaLingShouSkillDetailView:OnClickUpgradeJieBanSkillButton(go)
    if self.m_IsLingShouJieBan and not self:IsJieBan() then
        g_MessageMgr:ShowMessage("LingShou_NoJieBan_Cannot_Upgrade_Skill")
        return
    end
    if not self.m_IsLingShouJieBan and self:IsJieBan() then
        g_MessageMgr:ShowMessage("LingShou_JieBan_Cannot_Upgrade_Skill")
        return
    end

    local skillId = 0
    local details = CLingShouMgr.Inst:GetLingShouDetails(CLingShouMgr.Inst.selectedLingShou)
    if details then
        local partnerInfo = details.data.Props.PartnerInfo
        local normalSkills = partnerInfo.PartnerNormalSkills
        local professionalSkills = partnerInfo.PartnerProfessionalSkills
        if self:IsProfessionalSkill() then
            skillId = professionalSkills[self.m_SelectedSkillPos-5]
        else
            skillId = self.m_SelectedSkillPos>0 and normalSkills[self.m_SelectedSkillPos] or 0
        end
    end

    if skillId==0 then
        g_MessageMgr:ShowMessage("LINGSHOU_SKILL_UPGRADE_TIP")
        return
    end


    --数量不足的时候，打开快速通道
    if not self.m_MatEnough then
        CItemAccessListMgr.Inst:ShowItemAccessInfo(self.m_MatTemplateId, false, go.transform, AlignType.Right)
        return
    elseif not self.m_MoneyCtrl.moneyEnough then
        CGetMoneyMgr.Inst:GetYinLiang()
        return
    else
        local skillPrivateExp = CClientMainPlayer.Inst.SkillProp.SkillPrivateExp
        local exp = CClientMainPlayer.Inst.PlayProp.Exp
        local expEnough = skillPrivateExp+exp>=self.m_ExpCost --角色经验+专有经验
        if not expEnough then
            g_MessageMgr:ShowMessage("SKILL_UPDATE_FAIL_NOT_ENOUGH_EXP")
            return
        end
    end

    local skillType=1
    local skillPos=self.m_SelectedSkillPos
    if self:IsProfessionalSkill() then
        skillType=2
        skillPos = self.m_SelectedSkillPos-5
    end
    Gac2Gas.RequestUpgradeLingShouPartnerSkill(CLingShouMgr.Inst.selectedLingShou,skillType,skillPos)
end

function CLuaLingShouSkillDetailView:OnSelectTabView(index)
    
    if index==0 then
        self.m_HintLabel.gameObject:SetActive(false)
        self.m_ExpCtrl.gameObject:SetActive(false)
    else
        self.m_HintLabel.gameObject:SetActive(true)
        self.m_ExpCtrl.gameObject:SetActive(true)
    end

    local selectedLingShou = CLingShouMgr.Inst.selectedLingShou
    if selectedLingShou and selectedLingShou~="" then
        local d = CLingShouMgr.Inst:GetLingShouDetails(selectedLingShou)
        if d then
            if index==0 then
                local list = d.data.Props.SkillListForSave
                -- self:InitSkillData(list)
                --显示8个技能
                -- self.m_SkillSlots[8].gameObject:SetActive(true)

                for i=1,8 do
                    self:InitSkillSlot(self.m_SkillSlots[i],list and list[i] or nil,i)
                end
            elseif index==1 then--结伴技能
                local partnerInfo = d.data.Props.PartnerInfo
                local normalSkills = partnerInfo.PartnerNormalSkills
                local professionalSkills = partnerInfo.PartnerProfessionalSkills
                for i=1,5 do
                    self:InitSkillSlot(self.m_SkillSlots[i],normalSkills[i],i)
                end
                for i=1,3 do
                    self:InitSkillSlot(self.m_SkillSlots[i+5],professionalSkills[i],i+5)
                end
                --显示7个技能
                -- self.m_SkillSlots[8].gameObject:SetActive(false)
            end
        end
		self.m_UpgradeNotice = nil
    end
end

function CLuaLingShouSkillDetailView:OnEnable( )
    g_ScriptEvent:AddListener("SelectLingShouAtRow", self, "SelectLingShouAtRow")
    g_ScriptEvent:AddListener("GetLingShouDetails", self, "GetLingShouDetails")
    g_ScriptEvent:AddListener("UpdateLingShouSkillProp", self, "UpdateLingShouSkillProp")
    g_ScriptEvent:AddListener("SyncLingShouPartnerInfo", self, "OnSyncLingShouPartnerInfo")

    --更新经验显示、提示文字
    g_ScriptEvent:AddListener("MainPlayerPlayPropUpdate", self, "UpdateExpDisplay")
    g_ScriptEvent:AddListener("MainPlayerUpdateMoney", self, "UpdateExpDisplay")
    g_ScriptEvent:AddListener("MainPlayerSkillPropUpdate", self, "UpdateExpDisplay")
    
    
    self.transform:Find("Normal").gameObject:SetActive(false)
    local selectedLingShou = CLingShouMgr.Inst.selectedLingShou
    if selectedLingShou and selectedLingShou~="" then
        CLingShouMgr.Inst:RequestLingShouDetails(selectedLingShou)
    end

    self.m_FxNode:DestroyFx()
    self.m_LastZhandouli = nil

end

function CLuaLingShouSkillDetailView:OnDisable()
    g_ScriptEvent:RemoveListener("SelectLingShouAtRow", self, "SelectLingShouAtRow")
    g_ScriptEvent:RemoveListener("GetLingShouDetails", self, "GetLingShouDetails")
    g_ScriptEvent:RemoveListener("UpdateLingShouSkillProp", self, "UpdateLingShouSkillProp")
    g_ScriptEvent:RemoveListener("SyncLingShouPartnerInfo", self, "OnSyncLingShouPartnerInfo")

    --更新经验显示、提示文字
    g_ScriptEvent:RemoveListener("MainPlayerPlayPropUpdate", self, "UpdateExpDisplay")
    g_ScriptEvent:RemoveListener("MainPlayerUpdateMoney", self, "UpdateExpDisplay")
    g_ScriptEvent:RemoveListener("MainPlayerSkillPropUpdate", self, "UpdateExpDisplay")
end

-- function CLuaLingShouSkillDetailView:RefreshSkillUpgradeView(...)

-- end


function CLuaLingShouSkillDetailView:OnSyncLingShouPartnerInfo(lingShouId,partnerInfo)
    if lingShouId == CLingShouMgr.Inst.selectedLingShou then
        if self.m_TabView.CurrentIndex == 1 then
            self:OnSelectTabView(1)
        end

        local details = CLingShouMgr.Inst:GetLingShouDetails(CLingShouMgr.Inst.selectedLingShou)
        if details then
            self:InitView(details)
        end
    end
end

function CLuaLingShouSkillDetailView:SelectLingShouAtRow( row) 
    --请求详细信息
    --如果管理器有他的详情的话，就不用再请求了
    local selectedLingShou = CLingShouMgr.Inst.selectedLingShou
    if selectedLingShou and selectedLingShou~="" then
        CLingShouMgr.Inst:RequestLingShouDetails(selectedLingShou)
    else
        self.textureLoader:InitNull()
        self:InitSkillData(nil)
        self:SetDongFangOrLeaveState(false,false,false)

        self:InitView(nil)

    end
end
function CLuaLingShouSkillDetailView:GetLingShouDetails( args ) 
    if self.m_WndTabView.CurrentSelectTab ~= 2 then return end 
    local lingShouId, details=args[0],args[1]
    if lingShouId == CLingShouMgr.Inst.selectedLingShou then
        self.transform:Find("Normal").gameObject:SetActive(true)

        --看看是否结伴
        local bindPartenerLingShouId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.BindedPartenerLingShouId or nil
        self.m_IsLingShouJieBan = lingShouId==bindPartenerLingShouId
        local jiebanButton = self.m_TabView.transform:Find("Tab2").gameObject
        local jiebanLabel = jiebanButton.transform:Find("JieBanLabel"):GetComponent(typeof(UILabel))
        CUICommonDef.SetActive(jiebanButton,true,true)

        --更新界面
        if details ~= nil then
            self.textureLoader:Init(details)

            self.m_TabView:ChangeTo(bindPartenerLingShouId==lingShouId and 1 or 0)

            local  marryInfo = details.data.Props.MarryInfo
            local isDongFnag = marryInfo.IsInDongfang>0
            local isLeave = (marryInfo.LeaveStartTime + marryInfo.LeaveDuration) > CServerTimeMgr.Inst.timeStamp

            if isDongFnag or isLeave then
                self:SetDongFangOrLeaveState(true, isDongFnag, isLeave)
            else
                self:SetDongFangOrLeaveState(false, false, false)
            end

            self:InitView(details)
        else
            self.textureLoader:InitNull()
            self:InitSkillData(nil)
            self:SetDongFangOrLeaveState(false,false,false)

            self:InitView(nil)

        end
    end
end

function CLuaLingShouSkillDetailView:UpdateLingShouSkillProp(args)
    self:InitSkillData(args[1])

    local details = CLingShouMgr.Inst:GetLingShouDetails(CLingShouMgr.Inst.selectedLingShou)
    if details then
        self:InitView(details)
    end
end
function CLuaLingShouSkillDetailView:SetDongFangOrLeaveState( active, isDongFang, isLeave) 
    self.transform:Find("Normal").gameObject:SetActive(not active)
    self.transform.parent:Find("DongFangOrLeave").gameObject:SetActive(active)


    if active then
        local tf2 = self.transform.parent:Find("DongFangOrLeave")
        if isDongFang then
            tf2:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("灵兽生宝宝中，无法进行技能相关的操作")
        elseif isLeave then
            tf2:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("灵兽离家出走中，无法进行技能相关的操作")
        end
    end
end

function CLuaLingShouSkillDetailView:InitSkillSlots()
    local transform = self.transform:Find("Normal/Common/Skills")
    local first = transform:Find("1").gameObject
    local grid = transform:GetComponent(typeof(UIGrid))

    self.m_SkillSlots = {}
    table.insert( self.m_SkillSlots,first.transform )
    for i = 0, 6 do
        local go = CommonDefs.Object_Instantiate(first)
        go.transform.parent = grid.transform
        go.transform.localScale = Vector3.one
        table.insert( self.m_SkillSlots,go.transform )
        --初始化
        self:InitSkillSlot(go.transform,0,i+1)
    end
    grid:Reposition()
    -- self:InitNull()
end

function CLuaLingShouSkillDetailView:InitSkillData(skillList)
    for i=1,8 do
        self:InitSkillSlot(self.m_SkillSlots[i],skillList and skillList[i] or nil,i)
    end
end

function CLuaLingShouSkillDetailView:InitSkillSlot(transform,  skillId, pos)
    local icon = transform:Find("HasSkill/Texture"):GetComponent(typeof(CUITexture))
    local levelLabel = transform:Find("HasSkill/LevelLabel"):GetComponent(typeof(UILabel))
    local toggle = transform:GetComponent(typeof(UIToggle))
    local hasSkillTf = transform:Find("HasSkill")

    local template = Skill_AllSkills.GetData(skillId)
    if template ~= nil then
        hasSkillTf.gameObject:SetActive(true)
        icon:LoadSkillIcon(template.SkillIcon)
        levelLabel.text = "Lv." .. template.Level

        local col = transform:GetComponent(typeof(Collider))
        if col ~= nil then
            col.enabled = true
        end

        local noskill = self.transform:Find("NoSkill")
        if noskill ~= nil then
            noskill.gameObject:SetActive(false)
        end
    else
        local col = transform:GetComponent(typeof(Collider))
        if col ~= nil then
            col.enabled = false
        end
        hasSkillTf.gameObject:SetActive(false)
        icon.material = nil
        levelLabel.text = ""
    
        local noskill = self.transform:Find("NoSkill")
        if noskill ~= nil then
            noskill.gameObject:SetActive(true)
        end
    end

    if toggle.value then
        --更新选中的条目
        self:RefreshSelectSkill(skillId)
    end
    if skillId>0 then
        UIEventListener.Get(transform.gameObject).onClick=DelegateFactory.VoidDelegate(function(go)
            if skillId>0 then
                CSkillInfoMgr.ShowSkillInfoWnd(skillId,true,0,0,nil)
                self.m_SelectedSkillPos = pos
                self:RefreshSelectSkill(skillId)
            end
        end)
    else
        UIEventListener.Get(transform.gameObject).onClick=nil
    end
end

function CLuaLingShouSkillDetailView:InitUpgradePane()
    local transform = FindChild(self.transform,"Upgrade")
    local maxLevelNode = transform:Find("MaxLevelNode").gameObject
    local baseNode = transform:Find("BaseNode").gameObject
    local icon = transform:Find("BaseNode/Icon/Texture"):GetComponent(typeof(CUITexture))
    local nameLabel = FindChild(baseNode.transform,"NameLabel"):GetComponent(typeof(UILabel))

    local getNode = transform:Find("BaseNode/Icon/GetNode").gameObject

    getNode:SetActive(false)
    maxLevelNode:SetActive(false)
    baseNode:SetActive(false)
end

function CLuaLingShouSkillDetailView:OnClickUpgradeButton(go)
    if self.m_IsLingShouJieBan and not self:IsJieBan() then
        g_MessageMgr:ShowMessage("LingShou_NoJieBan_Cannot_Upgrade_Skill")
        return
    end
    if not self.m_IsLingShouJieBan and self:IsJieBan() then
        g_MessageMgr:ShowMessage("LingShou_JieBan_Cannot_Upgrade_Skill")
        return
    end

    local selectedSkillId = 0
    local details = CLingShouMgr.Inst:GetLingShouDetails(CLingShouMgr.Inst.selectedLingShou)
    if details then
        selectedSkillId = details.data.Props.SkillListForSave[self.m_SelectedSkillPos]
    end


    if selectedSkillId==0 then
        g_MessageMgr:ShowMessage("LINGSHOU_SKILL_UPGRADE_TIP")
        return
    end

	local skillListForSave = details.data.Props.SkillListForSave
	local isHaveEmpty = false
	for i = 3, 8 do
		if skillListForSave[i] == 0 then
			isHaveEmpty = true
			break
		end
	end
	if self.m_SelectedSkillPos>=1 and self.m_SelectedSkillPos<=2 then
		isHaveEmpty = false
	end

    --数量不足的时候，打开快速通道
    if not self.m_MatEnough then
        CItemAccessListMgr.Inst:ShowItemAccessInfo(self.m_MatTemplateId, false, go.transform, AlignType.Right)
    elseif not self.m_MoneyCtrl.moneyEnough then
        CGetMoneyMgr.Inst:GetYinLiang()
    else
        if self.m_NeedCount > self.m_ItemCountUpdate.count then
            g_MessageMgr:ShowMessage("LINGSHOU_HAVE_NO_UPGRADE_SKILL_ITEM")
            return
        end
		if not self.m_UpgradeNotice and isHaveEmpty then
			g_MessageMgr:ShowMessage("LINGSHOU_SKILL_NOT_FULL")
			self.m_UpgradeNotice = true
			return
		end

        --需要选择背包里的一个物品
        if selectedSkillId > 0 then
            local default, pos, itemId = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, self.m_MatTemplateId)
            if default then
                Gac2Gas.RequestUpgradeLingShouSkill(CLingShouMgr.Inst.selectedLingShou, selectedSkillId, EnumItemPlace_lua.Bag, pos, itemId, false)
            end
        end
    end
end

function CLuaLingShouSkillDetailView:RefreshSelectSkill(lingshouSkillId)
    local transform = FindChild(self.transform,"Upgrade")

    local maxLevelNode = transform:Find("MaxLevelNode").gameObject
    local baseNode = transform:Find("BaseNode").gameObject
    local icon = transform:Find("BaseNode/Icon/Texture"):GetComponent(typeof(CUITexture))
    -- icon.gameObject:SetActive(true)
    baseNode.transform:Find("Icon").gameObject:SetActive(true)
    -- local nameLabel = transform:Find("BaseNode/NameLabel"):GetComponent(typeof(UILabel))
    local nameLabel = FindChild(baseNode.transform,"NameLabel"):GetComponent(typeof(UILabel))
    local getNode = transform:Find("BaseNode/Icon/GetNode").gameObject

    local skillTemplate = Skill_AllSkills.GetData(lingshouSkillId)
    if skillTemplate ~= nil then
        local curLevel = lingshouSkillId%100
        local nextLevel = curLevel +1 
        self.m_MatTemplateId = 0
        if self:IsJieBan() then
            if self:IsProfessionalSkill() then
                self.m_MatTemplateId = self.m_UpgradeProfessionalItemId
            else
                -- self.m_MatTemplateId = self.m_UpgrateNormalItemId
                --普通结伴技能升级支持消耗不一样的道具。比如1~2级消耗道具1,2~30级消耗道具2。消耗的道具数量用现有配表
                local data= LingShouPartner_UpgradeLevelRequire.GetData(curLevel)
                self.m_MatTemplateId = data.UpgrateNormalItemId
            end
        else
            if skillTemplate.Item ~= nil then
                self.m_MatTemplateId = skillTemplate.Item[0]
            end
        end
        if self.m_MatTemplateId>0 then
            --更新耗材的信息
            local template = Item_Item.GetData(self.m_MatTemplateId)
            if template ~= nil then
                icon:LoadMaterial(template.Icon)
                nameLabel.text = template.Name
            else
                nameLabel.text = ""
                icon.material = nil
            end

            UIEventListener.Get(getNode).onClick = DelegateFactory.VoidDelegate(function (p) 
                CItemAccessListMgr.Inst:ShowItemAccessInfo(self.m_MatTemplateId, false, p.transform, AlignType.Right)
            end)
        end

        --获取下一级技能的消耗
        -- local cost = 0
        self.m_NeedCount=0
        local nextSkillTemplate = Skill_AllSkills.GetData(lingshouSkillId + 1)


        if nextSkillTemplate ~= nil then
            baseNode:SetActive(true)
            maxLevelNode:SetActive(false)
            
            self.m_NeedCount = 1
            if self:IsJieBan() then
                local data= LingShouPartner_UpgradeLevelRequire.GetData(curLevel)
                if self:IsProfessionalSkill() then
                    self.m_NeedCount = self.m_UpgradeProfessionalItemConsume[curLevel]

                    self.m_MoneyCtrl:SetCost(data.ProfessionalSilverConsume)
                    -- self.m_ExpCtrl:SetCost(data.ProfessionalExpConsume)
                    self.m_ExpCost = data.ProfessionalExpConsume
                else
                    self.m_NeedCount = self.m_UpgrateNormalItemConsume[curLevel]

                    self.m_MoneyCtrl:SetCost(data.SilverConsume)
                    -- self.m_ExpCtrl:SetCost(data.ExpConsume)
                    self.m_ExpCost = data.ExpConsume
                end
                self:UpdateExpDisplay()

            else
                --下一级需要的数量
                self.m_NeedCount = nextSkillTemplate.Item and nextSkillTemplate.Item[1] or 0
                self.m_MoneyCtrl:SetCost(nextSkillTemplate.AdjustedMoney)
            end

            if self.m_NeedCount==0 then
                baseNode.transform:Find("Icon").gameObject:SetActive(false)
            end

            self.m_ItemCountUpdate.templateId = self.m_MatTemplateId
            self.m_ItemCountUpdate.format = SafeStringFormat3("{0}/%d",self.m_NeedCount)

            self.m_ItemCountUpdate.onChange = DelegateFactory.Action_int(function (val) 
                if val >= self.m_NeedCount then
                    self.m_MatEnough=true
                    CommonDefs.GetComponent_GameObject_Type(getNode, typeof(Collider)).enabled = false
                    getNode:SetActive(false)
                else
                    self.m_MatEnough=false
                    CommonDefs.GetComponent_GameObject_Type(getNode, typeof(Collider)).enabled = true
                    getNode:SetActive(true)
                end
            end)
            self.m_ItemCountUpdate:UpdateCount()
        else
            baseNode:SetActive(false)
            maxLevelNode:SetActive(true)
        end
    else
        baseNode:SetActive(false)
        maxLevelNode:SetActive(false)
    end

    --刷新按钮状态
    if skillTemplate ~= nil then
        local nextSkillTemplate = Skill_AllSkills.GetData(lingshouSkillId + 1)
        if nextSkillTemplate ~= nil then
            CUICommonDef.SetActive(self.m_UpgradeButton, true, true)
        else
            CUICommonDef.SetActive(self.m_UpgradeButton, false, true)
        end
    else
        CUICommonDef.SetActive(self.m_UpgradeButton, true, true)
    end
end

function CLuaLingShouSkillDetailView:UpdateExpDisplay()
    local skillPrivateExp = CClientMainPlayer.Inst.SkillProp.SkillPrivateExp
    local exp = CClientMainPlayer.Inst.PlayProp.Exp

    local needExpIcon = self.m_ExpCtrl:Find("Cost/Sprite"):GetComponent(typeof(UISprite))
    local ownExpIcon = self.m_ExpCtrl:Find("Own/Sprite"):GetComponent(typeof(UISprite))
    local needExpLabel = self.m_ExpCtrl:Find("Cost"):GetComponent(typeof(UILabel))
    local ownExpLabel = self.m_ExpCtrl:Find("Own"):GetComponent(typeof(UILabel))

    if skillPrivateExp>0 then
        needExpIcon.spriteName = Money.GetIconName(EnumMoneyType.PrivateExp, EnumPlayScoreKey.NONE)
        ownExpIcon.spriteName = needExpIcon.spriteName
        ownExpLabel.text = tostring(skillPrivateExp)
    else
        needExpIcon.spriteName = Money.GetIconName(EnumMoneyType.Exp, EnumPlayScoreKey.NONE)
        ownExpIcon.spriteName = needExpIcon.spriteName
        ownExpLabel.text = tostring(exp)
    end

    needExpLabel.text = tostring(self.m_ExpCost)

    local expEnough = skillPrivateExp+exp>=self.m_ExpCost --角色经验+专有经验
    local icons = ""
    if expEnough then
        -- if skillPrivateExp==0 then
        --     --显示角色经验图标
        --     self.m_HintLabel.text = SafeStringFormat3(LocalString.GetString("[c][ff0000]不足的部分将从%s中补足[-][/c]"),"#290")
        -- else
            --显示专有经验图标
            if skillPrivateExp>=self.m_ExpCost then
                self.m_HintLabel.text = nil
            else
                self.m_HintLabel.text = SafeStringFormat3(LocalString.GetString("[c][ff0000]不足的部分将从%s中补足[-][/c]"),"#290")
            end
        -- end
    else
                    --290 个人经验图标
        self.m_HintLabel.text = LocalString.GetString("[c][ff0000]不足的部分无法从#290中补足[-][/c]")
    end
end

function CLuaLingShouSkillDetailView:InitView(details)
    local notNull = details and true or false
    local zhandouliLabel = self.transform:Find("Normal/1/ZhandouliLabel"):GetComponent(typeof(UILabel))
    local nameLabel = self.transform:Find("Normal/1/NameLabel"):GetComponent(typeof(UILabel))
    local titleLabel = self.transform:Find("Normal/1/TitleLabel"):GetComponent(typeof(UILabel))
    local levelLabel = self.transform:Find("Normal/1/LevelLabel"):GetComponent(typeof(UILabel))
    local pinzhi = self.transform:Find("Normal/1/Pinzhi"):GetComponent(typeof(UISprite))
    local shenShouSprite = self.transform:Find("Normal/1/ShenShou"):GetComponent(typeof(UISprite))

    pinzhi.spriteName = notNull and CLuaLingShouMgr.GetLingShouQualitySprite(CLuaLingShouMgr.GetLingShouQuality(details.data.Props.Quality)) or nil

    --"1:灵兽等级2:成长3:资质系数4:悟性5:修为6:技能数量7:技能满级比之和（各个技能/满级等级之和）"
    -- zhandouliLabel.text = notNull and tostring(CLuaLingShouMgr.GetLingShouZhandouli(details.data)) or ""
    local bindPartenerLingShouId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.BindedPartenerLingShouId or nil
    local qichangId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp.JieBanQiChangId or 0
    local isJieBan = bindPartenerLingShouId == details.id
    
    zhandouliLabel.text = notNull and tostring(isJieBan and CLuaLingShouMgr.GetLingShouPartnerZhandouli(details.data,details.fightProperty,qichangId) or CLuaLingShouMgr.GetLingShouZhandouli(details.data)) or ""
    zhandouliLabel.transform:Find("Label"):GetComponent(typeof(UILabel)).text =isJieBan and LocalString.GetString("结伴战斗力") or LocalString.GetString("出战战斗力")
    local curZhandouli = zhandouliLabel.text
    if curZhandouli == "" then
        curZhandouli = 0
    end

    self.m_FxNode:DestroyFx()
    if self.m_LastZhandouli then
        if tonumber(curZhandouli) > tonumber(self.m_LastZhandouli) and details.id == self.m_LastZhandouli2Id then
            self.m_FxNode:LoadFx("Fx/UI/Prefab/UI_dagongshouce_kekaiqicishu.prefab")
        end
    end
    self.m_LastZhandouli = curZhandouli
    self.m_LastZhandouli2Id = details.id

    nameLabel.text = notNull and details.data.Name or nil

    titleLabel.text = notNull and CLuaLingShouMgr.GetLingShouFullDesc(details.data) or nil

    levelLabel.text = notNull and "Lv." .. tostring(details.data.Level) or nil

    shenShouSprite.enabled = false
    local data = LingShou_LingShou.GetData(details.data.TemplateId)
    if data ~= nil then
        if data.ShenShou > 0 then
            shenShouSprite.enabled = true
        end
    end
end
