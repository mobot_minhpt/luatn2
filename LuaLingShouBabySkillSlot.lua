require("3rdParty/ScriptEvent")
require("common/common_include")
local LuaGameObject=import "LuaGameObject"
local Skill_AllSkills=import "L10.Game.Skill_AllSkills"
local CSkillInfoMgr=import "L10.UI.CSkillInfoMgr"
--灵兽技能
CLuaLingShouBabySkillSlot=class()
RegistClassMember(CLuaLingShouBabySkillSlot,"m_TemplateId")
RegistClassMember(CLuaLingShouBabySkillSlot,"m_LevelLabel")
RegistClassMember(CLuaLingShouBabySkillSlot,"m_Icon")
RegistClassMember(CLuaLingShouBabySkillSlot,"m_LockSprite")
RegistClassMember(CLuaLingShouBabySkillSlot,"m_SkillId")
RegistClassMember(CLuaLingShouBabySkillSlot,"m_GameObject")

function CLuaLingShouBabySkillSlot:ctor()
end
function CLuaLingShouBabySkillSlot:SetSelected( selected )
    local get=LuaGameObject.GetChildNoGC(self.m_GameObject.transform,"Outline")
    if get then
        get.gameObject:SetActive(selected)
    end
end
function CLuaLingShouBabySkillSlot:Init( tf )
    self.m_GameObject=tf.gameObject
    self.m_LockSprite=LuaGameObject.GetChildNoGC(tf,"LockedSprite").sprite
    self.m_LevelLabel=LuaGameObject.GetChildNoGC(tf,"LevelLabel").label
    self.m_Icon=LuaGameObject.GetChildNoGC(tf,"Texture").cTexture
    self.m_SkillId=0
    local function OnClick(go)
        if self.m_SkillId>0 then
            CSkillInfoMgr.ShowSkillInfoWnd(self.m_SkillId,true,0,0,nil)
        end
    end
    CommonDefs.AddOnClickListener(tf.gameObject,DelegateFactory.Action_GameObject(OnClick),true)
end
function CLuaLingShouBabySkillSlot:InitData(skillId)
    -- print(skillId)
    self.m_SkillId=skillId
    if skillId==0 then
        self.m_LevelLabel.gameObject:SetActive(false)
        self.m_Icon:Clear()
        self.m_LockSprite.gameObject:SetActive(true)
    else
        local data = Skill_AllSkills.GetData(skillId)
        if data then
            self.m_LevelLabel.gameObject:SetActive(true)
            -- print(data.SkillIcon)
            self.m_Icon:LoadSkillIcon(data.SkillIcon)
            self.m_LevelLabel.text = "Lv." .. data.Level
            self.m_LockSprite.gameObject:SetActive(false)
        else
            self.m_LevelLabel.gameObject:SetActive(false)
            self.m_Icon:Clear()
            self.m_LockSprite.gameObject:SetActive(true)
        end

    end
end
return CLuaLingShouBabySkillSlot
