local CUITexture = import "L10.UI.CUITexture"
local CButton = import "L10.UI.CButton"
local CTooltip = import "L10.UI.CTooltip"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local CUIFx = import "L10.UI.CUIFx"
local SoundManager = import "SoundManager"
local CModelAutoRotate = import "L10.UI.CModelAutoRotate"
local Screen = import "UnityEngine.Screen"
local QualitySettings = import "UnityEngine.QualitySettings"
local CRenderObject  = import "L10.Engine.CRenderObject"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local CEffectMgr = import "L10.Game.CEffectMgr"
local NGUITools = import "NGUITools"
local LayerDefine = import "L10.Engine.LayerDefine"
local ParticleSystem = import "UnityEngine.ParticleSystem"
local Renderer = import "UnityEngine.Renderer"
local MaterialPropertyBlock = import "UnityEngine.MaterialPropertyBlock"
local GameObject = import "UnityEngine.GameObject"

CLuaSoulCoreInfoView = class()

RegistClassMember(CLuaSoulCoreInfoView, "m_ModelTex")
RegistClassMember(CLuaSoulCoreInfoView, "m_SoulCoreName")
RegistClassMember(CLuaSoulCoreInfoView, "m_CurManaLab")
RegistClassMember(CLuaSoulCoreInfoView, "m_ClassTex")
RegistClassMember(CLuaSoulCoreInfoView, "m_LevelLab")
RegistClassMember(CLuaSoulCoreInfoView, "m_LevelUpFx")
RegistClassMember(CLuaSoulCoreInfoView, "m_LevelSufLab")
RegistClassMember(CLuaSoulCoreInfoView, "m_UpLab")
RegistClassMember(CLuaSoulCoreInfoView, "m_DownLab")
RegistClassMember(CLuaSoulCoreInfoView, "m_EfficientLab")
RegistClassMember(CLuaSoulCoreInfoView, "m_NeedLab")
RegistClassMember(CLuaSoulCoreInfoView, "m_AlertLab1")
RegistClassMember(CLuaSoulCoreInfoView, "m_AlertLab2")
RegistClassMember(CLuaSoulCoreInfoView, "m_NeedsWidget")
RegistClassMember(CLuaSoulCoreInfoView, "m_NextCostWid")
RegistClassMember(CLuaSoulCoreInfoView, "m_LevelFullTip")
RegistClassMember(CLuaSoulCoreInfoView, "m_LevelUpBtn")
RegistClassMember(CLuaSoulCoreInfoView, "m_TipBtn")
RegistClassMember(CLuaSoulCoreInfoView, "m_ClassToName")
RegistClassMember(CLuaSoulCoreInfoView, "m_AlertColor")
RegistClassMember(CLuaSoulCoreInfoView, "m_NorColor")
RegistClassMember(CLuaSoulCoreInfoView, "m_FixColor")
RegistClassMember(CLuaSoulCoreInfoView, "m_ModelTextureName")
RegistClassMember(CLuaSoulCoreInfoView, "m_AutoRotSpeed")
RegistClassMember(CLuaSoulCoreInfoView, "m_Rotation")
RegistClassMember(CLuaSoulCoreInfoView, "m_BlueLabCol")
RegistClassMember(CLuaSoulCoreInfoView, "m_YellowLabCol")
RegistClassMember(CLuaSoulCoreInfoView, "m_Need1Lab")
--RegistClassMember(CLuaSoulCoreInfoView, "m_Need3Lab")
RegistClassMember(CLuaSoulCoreInfoView, "m_AutoRotate")
RegistClassMember(CLuaSoulCoreInfoView, "m_IsDraging")

RegistClassMember(CLuaSoulCoreInfoView, "m_Inited")

RegistClassMember(CLuaSoulCoreInfoView, "m_CircleSlider")
RegistClassMember(CLuaSoulCoreInfoView, "m_ManaCostGo")
RegistClassMember(CLuaSoulCoreInfoView, "m_ExpCostGo")
RegistClassMember(CLuaSoulCoreInfoView, "m_ManaCost")
RegistClassMember(CLuaSoulCoreInfoView, "m_ManaOwn")
RegistClassMember(CLuaSoulCoreInfoView, "m_ExpCost")
RegistClassMember(CLuaSoulCoreInfoView, "m_ExpOwn")
RegistClassMember(CLuaSoulCoreInfoView, "m_Errors")
RegistClassMember(CLuaSoulCoreInfoView, "m_MingGeCache")

RegistClassMember(CLuaSoulCoreInfoView, "m_NeedExp")
RegistClassMember(CLuaSoulCoreInfoView, "m_NeedMana")

RegistClassMember(CLuaSoulCoreInfoView, "m_RO")
RegistClassMember(CLuaSoulCoreInfoView, "m_LastChangeType")
RegistClassMember(CLuaSoulCoreInfoView,"m_ZongPaiBtn")
RegistClassMember(CLuaSoulCoreInfoView,"m_LianHuaBtn")
function CLuaSoulCoreInfoView:Awake()
    self:InitComponents()

    self.m_AlertColor   = NGUIText.ParseColor24("FF4A40", 0)
    self.m_NorColor     = NGUIText.ParseColor24("FFFFFF", 0)
    self.m_FixColor     = NGUIText.ParseColor24("FF790B", 0)

    self.m_BlueLabCol   = NGUIText.ParseColor24("0E3254", 0)
    self.m_YellowLabCol = NGUIText.ParseColor24("351B01", 0)

    self.m_ModelTextureName = "__SoulCoreInfoWndModelCamera__"
    self.m_AutoRotSpeed = SoulCore_Settings.GetData().SoulCoreCarveRotSpeed or 12
    self.m_Rotation = 180
    self.m_NeedExp = 0
    self.m_NeedMana = 0
    self.m_Errors = {}

    if CClientMainPlayer.Inst ~= nil then
        LuaZongMenMgr.m_LastOpenSoulCoreWndLv = CClientMainPlayer.Inst.SkillProp.SoulCore.Level
        g_ScriptEvent:BroadcastInLua("OnOpenSoulCoreWnd")
    end
end

function CLuaSoulCoreInfoView:InitComponents()
    self.m_ModelTex     = self.transform:Find("Anchor/Left/LeftTop/SoulCore/ModelTexture"):GetComponent(typeof(UITexture))
    self.m_SoulCoreName = self.transform:Find("Anchor/Left/LeftTop/SoulCoreName/Label"):GetComponent(typeof(UILabel))
    self.m_CurManaLab   = self.transform:Find("Anchor/Left/LeftTop/SoulCore/CurrentSpiritual/Label"):GetComponent(typeof(UILabel))

    self.m_ClassTex     = self.transform:Find("Anchor/Right/RightTop/WuXing"):GetComponent(typeof(CUITexture))
    self.m_LevelLab     = self.transform:Find("Anchor/Right/RightTop/Level/Label"):GetComponent(typeof(UILabel))
    self.m_LevelUpFx    = self.transform:Find("Anchor/Right/RightTop/Level/Label/Fx"):GetComponent(typeof(CUIFx))
    self.m_LevelSufLab  = self.transform:Find("Anchor/Right/RightTop/Level/Suffix"):GetComponent(typeof(UILabel))

    self.m_UpLab        = self.transform:Find("Anchor/Right/RightTop/Grid/Up/Label"):GetComponent(typeof(UILabel))
    self.m_DownLab      = self.transform:Find("Anchor/Right/RightTop/Grid/Down/Label"):GetComponent(typeof(UILabel))
    self.m_EfficientLab = self.transform:Find("Anchor/Right/RightTop/Grid/Efficient/Label"):GetComponent(typeof(UILabel))

    self.m_Need1Lab     = self.transform:Find("Anchor/Right/RightMiddle/Needs/Alert1"):GetComponent(typeof(UILabel))

    self.m_NeedsWidget  = self.transform:Find("Anchor/Right/RightMiddle/Needs"):GetComponent(typeof(UIWidget))
    self.m_NextCostWid  = self.transform:Find("Anchor/Right/RightBottom/Cost"):GetComponent(typeof(UIWidget))
    self.m_LevelFullTip = self.transform:Find("Anchor/Right/FullLevelTip"):GetComponent(typeof(UILabel))
    self.m_LevelUpBtn   = self.transform:Find("Anchor/Right/BlueButton"):GetComponent(typeof(CButton))
    self.m_TipBtn       = self.transform:Find("Anchor/Right/RuleDescBtn"):GetComponent(typeof(QnButton))

    self.m_AutoRotate   = self.transform:Find("Anchor/Left/LeftTop/SoulCore/ModelTexture"):GetComponent(typeof(CModelAutoRotate))

    self.m_CircleSlider = self.transform:Find("Anchor/Left/LeftTop/SoulCore/CircleSlider"):GetComponent(typeof(UISlider))
    self.m_ManaCostGo   = self.transform:Find("Anchor/Right/RightBottom/Cost/ManaCost").gameObject
    self.m_ExpCostGo    = self.transform:Find("Anchor/Right/RightBottom/Cost/ExpCost").gameObject
    self.m_ManaCost     = self.transform:Find("Anchor/Right/RightBottom/Cost/ManaCost/Cost/Cost"):GetComponent(typeof(UILabel))
    self.m_ManaOwn      = self.transform:Find("Anchor/Right/RightBottom/Cost/ManaCost/Own/Own"):GetComponent(typeof(UILabel))
    self.m_ExpCost      = self.transform:Find("Anchor/Right/RightBottom/Cost/ExpCost/QnCostAndOwnMoney1/Cost/Cost"):GetComponent(typeof(UILabel))
    self.m_ExpOwn       = self.transform:Find("Anchor/Right/RightBottom/Cost/ExpCost/QnCostAndOwnMoney1/Own/Own"):GetComponent(typeof(UILabel))
    self.m_ZongPaiBtn   = self.transform:Find("Anchor/Left/LeftTop/SoulCore/Buttons/ZongMenButton"):GetComponent(typeof(CButton))
    self.m_LianHuaBtn   = self.transform:Find("Anchor/Left/LeftTop/SoulCore/Buttons/LianHuaButton"):GetComponent(typeof(CButton))
end

function CLuaSoulCoreInfoView:Start()
    self:InitModelTexCallback()
    self.m_ClassToName = {}

    for i=1, SoulCore_WuXing.GetDataCount() do
        self.m_ClassToName[i] = SoulCore_WuXing.GetData(i).Name
    end
    
    self:InitTip()

    CommonDefs.AddOnClickListener(self.m_LevelUpBtn.gameObject, DelegateFactory.Action_GameObject(function(Lab)
        LuaSkillMgr:SoulCoreUpgrade()
    end), false)

    CommonDefs.AddOnClickListener(self.m_ZongPaiBtn.gameObject, DelegateFactory.Action_GameObject(function(Lab)
        CUIManager.ShowUI(CLuaUIResources.ZongMenMainWnd)
    end), false)

    CommonDefs.AddOnClickListener(self.m_LianHuaBtn.gameObject, DelegateFactory.Action_GameObject(function(Lab)
        if LuaZongMenMgr.m_OpenNewSystem then
            CUIManager.ShowUI(CLuaUIResources.RefineMonsterCircleNewWnd)
        else
            CUIManager.ShowUI(CLuaUIResources.RefineMonsterCircleWnd)
        end
    end), false)

    local hasSect = CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.SectId ~= 0 
    self.m_ZongPaiBtn.gameObject:SetActive(hasSect)
    self.m_LianHuaBtn.gameObject:SetActive(hasSect)

    self.m_TipBtn.OnClick = DelegateFactory.Action_QnButton(function(Lab)
        g_MessageMgr:ShowMessage("SoulCore_LevelUp_Tip")
    end)

    local mainPlayer = (CClientMainPlayer.Inst or {})
    self.m_SoulCoreName.text = mainPlayer.Name .. LocalString.GetString("灵核")

    local soulCoreProp = (mainPlayer.SkillProp or {}).SoulCore
    local wuXing = (mainPlayer.BasicProp or {}).MingGe
    local appearData = {DisplayCoreId = soulCoreProp.DisplayCoreId,
        DisplayFxId = soulCoreProp.DisplayFxId,
        DisplayPalletId = soulCoreProp.DisplayPalletId,
        DisplayCoreParticles = soulCoreProp.DisplayCoreParticles,
        DisplayCoreEffect = soulCoreProp.DisplayCoreEffect,
        Level = soulCoreProp.Level}

    self:LoadModel(appearData, wuXing)
    self:LoadCurrentProperty(wuXing, soulCoreProp.Mana, soulCoreProp.Level, LuaZongMenMgr:CalMainPlayerAntiProfessionLevel())
    -- 记录一下等级变化阶段
    self.m_LastChangeType = self:GetLevelChangeType()
    self.m_Inited = true
end

function CLuaSoulCoreInfoView:OnEnable()
    g_ScriptEvent:AddListener("UpdateMainPlayerSoulCoreLevel", self, "OnUpdateMainPlayerSoulCoreLevel")
    g_ScriptEvent:AddListener("UpdateMainPlayerSoulCoreMana", self, "RefreshCurrentProperty")
    g_ScriptEvent:AddListener("MainPlayerPlayPropUpdate", self, "RefreshOwnAndBtnState")
    g_ScriptEvent:AddListener("OnSyncPlayerMingGe", self, "OnSyncPlayerMingGe")

    if LuaShopMallFashionPreviewMgr.enableMultiLight then
        QualitySettings.pixelLightCount = 3
    end

    -- 切换view停止了监听需要主动刷新一下一些数据
    if self.m_Inited == true then
        self:RefreshCurrentProperty()

        -- 命格发生改变也重新load下模型
        local mainPlayer = (CClientMainPlayer.Inst or {})
        local wuXing = (mainPlayer.BasicProp or {}).MingGe
        if wuXing ~= self.m_MingGeCache then
            self:RefreshModel()
        end
    end
end

function CLuaSoulCoreInfoView:OnDisable()
    g_ScriptEvent:RemoveListener("UpdateMainPlayerSoulCoreLevel", self, "OnUpdateMainPlayerSoulCoreLevel")
    g_ScriptEvent:RemoveListener("UpdateMainPlayerSoulCoreMana", self, "RefreshCurrentProperty")
    g_ScriptEvent:RemoveListener("MainPlayerPlayPropUpdate", self, "RefreshOwnAndBtnState")
    g_ScriptEvent:RemoveListener("OnSyncPlayerMingGe", self, "OnSyncPlayerMingGe")

    if LuaShopMallFashionPreviewMgr.enableMultiLight then
        QualitySettings.pixelLightCount = 0
    end
    self.m_LevelUpFx:DestroyFx()
end

function CLuaSoulCoreInfoView:InitModelTexCallback()
    CommonDefs.AddOnDragListener(self.m_ModelTex.gameObject, DelegateFactory.Action_GameObject_Vector2(function (go, delta)
		self:OnScreenDrag(go, delta)
    end), false)
    
    UIEventListener.Get(self.m_ModelTex.gameObject).onDragStart = DelegateFactory.VoidDelegate(function(g)
        self.m_IsDraging = true
        self:CheckIfNeedRotate()
    end)

    UIEventListener.Get(self.m_ModelTex.gameObject).onDragEnd = DelegateFactory.VoidDelegate(function(g)
        self.m_IsDraging = false
        self:CheckIfNeedRotate()
    end)
end

function CLuaSoulCoreInfoView:OnScreenDrag(go, delta)
    local rot = -delta.x / Screen.width * 360
	self.m_AutoRotate:Rotate(rot)
end

function CLuaSoulCoreInfoView:CheckIfNeedRotate()
    local IsNeedRotate = not self.m_IsDraging
    self.m_AutoRotate.IsRotating = IsNeedRotate
end

function CLuaSoulCoreInfoView:InitTip()
    local titles = {}
    local m_UpTitleLab        = self.transform:Find("Anchor/Right/RightTop/Grid/Up/Title"):GetComponent(typeof(UILabel))
    local m_DownTitleLab      = self.transform:Find("Anchor/Right/RightTop/Grid/Down/Title"):GetComponent(typeof(UILabel))
    local m_EfficientTitleLab = self.transform:Find("Anchor/Right/RightTop/Grid/Efficient/Title"):GetComponent(typeof(UILabel))

    table.insert(titles, {label = m_UpTitleLab, tip = "SoulCore_Tips_Up_Message"})
    table.insert(titles, {label = m_DownTitleLab, tip = "SoulCore_Tips_Down_Message"})
    table.insert(titles, {label = m_EfficientTitleLab, tip = "SoulCore_Tips_BaseRate_Message"})

    for i = 1, #titles do
        local title = titles[i]
        UIEventListener.Get(title.label.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
            CTooltip.Show(g_MessageMgr:FormatMessage(title.tip), title.label.transform, CTooltip.AlignType.Top)
        end)
    end
end

function CLuaSoulCoreInfoView:RefreshCurrentProperty()
    local mainPlayer = (CClientMainPlayer.Inst or {})
    local soulCoreProp = (mainPlayer.SkillProp or {}).SoulCore
    local wuXing = (mainPlayer.BasicProp or {}).MingGe
    self:LoadCurrentProperty(wuXing, soulCoreProp.Mana, soulCoreProp.Level, LuaZongMenMgr:CalMainPlayerAntiProfessionLevel())
end

function CLuaSoulCoreInfoView:OnUpdateMainPlayerSoulCoreLevel()
    self.m_LevelUpFx:DestroyFx()
    self.m_LevelUpFx:LoadFx("Fx/UI/Prefab/UI_lingheshengji02.prefab")
    SoundManager.Inst:PlayOneShot(SoundManager.UISoundEvents.Skill_Update, Vector3.zero, nil, 0)
    self:RefreshCurrentProperty()

    local changeType = self:GetLevelChangeType()
    -- 阶段不一样则重新加载一下
    if self.m_LastChangeType ~= changeType then
        self:RefreshModel()
    end
    self.m_LastChangeType = changeType
end

function CLuaSoulCoreInfoView:GetLevelChangeType()
    local mainPlayer = (CClientMainPlayer.Inst or {})
    local soulCoreProp = (mainPlayer.SkillProp or {}).SoulCore
    local level = soulCoreProp.Level
    local levelSoulCoreChange = SoulCore_Settings.GetData().SoulCoreChange
    local changeType = 0
    for i = levelSoulCoreChange.Length - 1, 0, -1 do
        if level >= levelSoulCoreChange[i][0] then
            changeType = i
            break
        end
    end

    return changeType
end

function CLuaSoulCoreInfoView:LoadModel(appearData, wuXing)
    self.m_MingGeCache = wuXing
    self.m_ModelTex.mainTexture = CUIManager.CreateModelTexture(self.m_ModelTextureName, LuaDefaultModelTextureLoader.Create(function (ro)
        self.m_RO = ro
        LuaZongMenMgr:CreateSoulCoreGameObject(ro.gameObject, appearData, wuXing)
        self.m_AutoRotate:Init(self.m_ModelTextureName, 180, self.m_AutoRotSpeed)
        self:InitShadowCameraAndLight()
    end) , 180, 0, -0.33, 4.66 * 0.85, false, true, 1)
end

function CLuaSoulCoreInfoView:ReLoadSoulCoreModel(appearData, wuXing)
    GameObject.Destroy(self.m_RO.transform:Find("SoulCore").gameObject)
    self.m_MingGeCache = wuXing
    LuaZongMenMgr:CreateSoulCoreGameObject(self.m_RO.gameObject, appearData, wuXing)
end

function CLuaSoulCoreInfoView:LoadCurrentProperty(class, curMana, oriLevel, curAntiProfessionLv)
    local mainPlayer = (CClientMainPlayer.Inst or {})
    local hasFeiSheng = mainPlayer.HasFeiSheng == true
    local fanShenLevel = ((mainPlayer.PlayProp or {}).FeiShengData or {}).FeiShengLevel
    local xianShenLevel = mainPlayer.XianShenLevel
    local curLevel = hasFeiSheng and LuaZongMenMgr:GetAdjustSoulCoreLevel(fanShenLevel, xianShenLevel, oriLevel) or oriLevel 

    --五行属性
    self:LoadWuXingProp(class)
    
    --当前属性
    self:LoadCurLvProp(curLevel, curMana, hasFeiSheng, oriLevel)
    
    --下一级属性和升级条件(只根据原始等级)
    self:LoadNextLvPropAndNeeds(oriLevel, curMana, curAntiProfessionLv)
end

function CLuaSoulCoreInfoView:LoadWuXingProp(class)
    local classData = SoulCore_WuXing.GetData(class)

    if classData ~= nil then
        for i = 0, classData.Up.Length - 1 do
            if i == 0 then
                self.m_UpLab.text = SafeStringFormat3("%s%s", self.m_ClassToName[classData.Up[0]], self:FormatNumber(classData.UpValue[0]))
            else
                self.m_UpLab.text = self.m_UpLab.text .. LocalString.GetString("、") .. SafeStringFormat3("%s%s", self.m_ClassToName[classData.Up[i]], self:FormatNumber(classData.UpValue[i]))
            end
        end
        
        for i = 0, classData.Down.Length - 1 do
            if i == 0 then
                self.m_DownLab.text = SafeStringFormat3("%s%s", self.m_ClassToName[classData.Down[0]], self:FormatNumber(classData.DownValue[0]))
            else
                self.m_DownLab.text = self.m_DownLab.text .. LocalString.GetString("、") .. SafeStringFormat3("%s%s", self.m_ClassToName[classData.Down[i]], self:FormatNumber(classData.DownValue[i]))
            end
        end

        self.m_ClassTex:LoadMaterial(LuaZongMenMgr:GetMingGeMatPath(class))
    else
        self.m_UpLab.text = "-"
        self.m_DownLab.text = "-"
    end
end

function CLuaSoulCoreInfoView:LoadCurLvProp(curLevel, curMana, hasFeiSheng, oriLevel)
    local curData = SoulCore_SoulCore.GetData(curLevel)
    if curData ~= nil then
        self.m_LevelLab.text = curLevel
        self.m_CircleSlider.value = curMana / curData.ManaLimit
        self.m_EfficientLab.text = SafeStringFormat3(LocalString.GetString("%s灵力/分钟"), self:FormatNumber(curData.BaseRate))
        --凡身仙身样式
        local showFeiSheng = hasFeiSheng and curLevel ~= oriLevel
        if showFeiSheng then
            self.m_CurManaLab.text = SafeStringFormat3("%s/[c][FF790B]%s[-]", curMana, curData.ManaLimit)
            self.m_LevelLab.color = self.m_FixColor
            self.m_LevelSufLab.text = SafeStringFormat3("/%d%s", oriLevel, LocalString.GetString("级"))
            self.m_EfficientLab.color = self.m_FixColor
        else
            self.m_CurManaLab.text = SafeStringFormat3("%s/%s", curMana, curData.ManaLimit)
            self.m_LevelLab.color = self.m_NorColor
            self.m_LevelSufLab.text = LocalString.GetString("级")
            self.m_EfficientLab.color = self.m_NorColor
        end
    else
        self.m_CurManaLab.text = "-"
        self.m_CircleSlider.value = 0
        self.m_LevelLab.text = curLevel
        self.m_EfficientLab.text = "-"
    end
end

function CLuaSoulCoreInfoView:LoadNextLvPropAndNeeds(oriLevel, curMana, curAntiProfessionLv)
    local nextLevel = oriLevel + 1
    local nextData = SoulCore_SoulCore.GetData(nextLevel)
    local hasNextLv = nextData ~= nil
    if hasNextLv then
        self.m_Need1Lab.text = SafeStringFormat3(LocalString.GetString("角色等级达到%s级"), nextData.PlayerLearnLv)

        local lvCondition = (CClientMainPlayer.Inst or {}).MaxLevel < nextData.PlayerLearnLv
        self.m_Errors[1] = lvCondition
        if lvCondition then
            self.m_Need1Lab.color = self.m_AlertColor
        else
            self.m_Need1Lab.color = self.m_NorColor
        end

        self.m_NeedMana = nextData.InSoulCoreMana
        self.m_NeedExp = nextData.InSoulCoreExp
        self.m_ManaCost.text = self.m_NeedMana
        self.m_ExpCost.text = self.m_NeedExp
        self.m_ExpCostGo:SetActive(self.m_NeedExp > 0)
        self:RefreshOwnManaAndExp()
        self:RefreshLevelUpBtnState()
    end

    self.m_NeedsWidget.alpha = hasNextLv and 1 or 0
    self.m_NextCostWid.alpha = hasNextLv and 1 or 0
    self.m_LevelFullTip.gameObject:SetActive(not hasNextLv)
    CUICommonDef.SetActive(self.m_LevelUpBtn.gameObject, hasNextLv, true)
end

function CLuaSoulCoreInfoView:RefreshOwnAndBtnState()
    self:RefreshOwnManaAndExp()
    self:RefreshLevelUpBtnState()
end

function CLuaSoulCoreInfoView:RefreshOwnManaAndExp()
    if CClientMainPlayer.Inst == nil then
        return
    end

    local mainPlayer = CClientMainPlayer.Inst
    local mana = mainPlayer.SkillProp.SoulCore.Mana
    local exp = mainPlayer.PlayProp.Exp
    
    self.m_ManaOwn.text = mana
    local manaCondition = self.m_NeedMana > mana
    self.m_Errors[2] = manaCondition
    if manaCondition then
        self.m_ManaOwn.color = self.m_AlertColor
    else
        self.m_ManaOwn.color = self.m_NorColor
    end

    self.m_ExpOwn.text = exp
    local expCondition = self.m_NeedExp > exp
    self.m_Errors[3] = expCondition
    if expCondition then
        self.m_ExpOwn.color = self.m_AlertColor
    else
        self.m_ExpOwn.color = self.m_NorColor
    end
end

function CLuaSoulCoreInfoView:RefreshModel()
    local mainPlayer = (CClientMainPlayer.Inst or {})
    local soulCoreProp = (mainPlayer.SkillProp or {}).SoulCore
    local wuXing = (mainPlayer.BasicProp or {}).MingGe
    local appearData = {DisplayCoreId = soulCoreProp.DisplayCoreId,
        DisplayFxId = soulCoreProp.DisplayFxId,
        DisplayPalletId = soulCoreProp.DisplayPalletId,
        DisplayCoreParticles = soulCoreProp.DisplayCoreParticles,
        DisplayCoreEffect = soulCoreProp.DisplayCoreEffect, 
        Level = soulCoreProp.Level
    }

    self:ReLoadSoulCoreModel(appearData, wuXing)
end

function CLuaSoulCoreInfoView:OnSyncPlayerMingGe()
    self:RefreshModel()
    self:RefreshCurrentProperty()
end

function CLuaSoulCoreInfoView:RefreshLevelUpBtnState()
    local errorCount = 0
    for i = 1, #self.m_Errors do
        errorCount = errorCount + (self.m_Errors[i] == true and 1 or 0)
    end

    if errorCount > 0 then
        self.m_LevelUpBtn.background.spriteName = "common_btn_01_blue"
        self.m_LevelUpBtn.label.color = self.m_BlueLabCol
    else
        self.m_LevelUpBtn.background.spriteName = "common_btn_01_yellow"
        self.m_LevelUpBtn.label.color = self.m_YellowLabCol
    end
end

function CLuaSoulCoreInfoView:FormatNumber(num)
    return tonumber(SafeStringFormat3("%.2f", num))
end

function CLuaSoulCoreInfoView:OnDestroy()
    self.m_ModelTex.mainTexture = nil
    CUIManager.DestroyModelTexture(self.m_ModelTextureName)
end

function CLuaSoulCoreInfoView:InitShadowCameraAndLight()
    local t = CUIManager.instance.transform:Find(self.m_ModelTextureName)
    if not t then return end
    if t:Find("LightAndFloorAndWall") ~= nil then
        return
    end
    local newRO = CRenderObject.CreateRenderObject(t.gameObject, "LightAndFloorAndWall")
    local fx = CEffectMgr.Inst:AddObjectFX(88801543, newRO ,0,1,1,nil,false,EnumWarnFXType.None, Vector3.zero, Vector3.zero, DelegateFactory.Action_GameObject(function(go)
        NGUITools.SetLayer(newRO.gameObject,LayerDefine.Effect_3D)
        local beijingRenderer = go.transform:Find("Anchor/beijing"):GetComponent(typeof(Renderer))
        local spotLight = go.transform:Find("Anchor/Spotlight")
        local particle = go.transform:Find("Anchor/PretendParticleEffect"):GetComponent(typeof(ParticleSystem))

        LuaUtils.SetLocalPositionY(beijingRenderer.transform, -15)
        local mpb = CreateFromClass(MaterialPropertyBlock)
        beijingRenderer:GetPropertyBlock(mpb)
        mpb:SetColor("_Color", NGUIText.ParseColor24("00030EFF", 0))
        beijingRenderer:SetPropertyBlock(mpb)
        spotLight.gameObject:SetActive(false)--00030EFF
        LuaUtils.SetLocalPosition(particle.transform, 4.04, -7.29, -3.66)
        particle.maxParticles = 15
    end))
    newRO:AddFX("LightAndFloorAndWall", fx, -1)
end
