require("common/common_include")

local UIPanel = import "UIPanel"
local CBaseWnd = import "L10.UI.CBaseWnd"
local CPreciousItemShareMgr = import "L10.Game.CPreciousItemShareMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local UISprite = import "UISprite"
local Item_Item = import "L10.Game.Item_Item"
local DelegateFactory = import "DelegateFactory"
local CLoginMgr = import "L10.Game.CLoginMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"

LuaPreciousItemShareWnd = class()
RegistClassMember(LuaPreciousItemShareWnd,"closeBtn")

RegistClassMember(LuaPreciousItemShareWnd,"itemTitleLabel")
RegistClassMember(LuaPreciousItemShareWnd,"itemIcon")
RegistClassMember(LuaPreciousItemShareWnd,"itemBorder")
RegistClassMember(LuaPreciousItemShareWnd,"itemNameLabel")

RegistClassMember(LuaPreciousItemShareWnd,"playerIcon")
RegistClassMember(LuaPreciousItemShareWnd,"playerLevelLabel")
RegistClassMember(LuaPreciousItemShareWnd,"playerNameLabel")
RegistClassMember(LuaPreciousItemShareWnd,"serverNameLabel")
RegistClassMember(LuaPreciousItemShareWnd,"playerIDLabel")

RegistClassMember(LuaPreciousItemShareWnd,"excludedRoot")
RegistClassMember(LuaPreciousItemShareWnd,"shareBtn")
RegistClassMember(LuaPreciousItemShareWnd,"shareMenu")
RegistClassMember(LuaPreciousItemShareWnd,"shareToMengDaoBtn")
RegistClassMember(LuaPreciousItemShareWnd,"shareToOtherBtn")

function LuaPreciousItemShareWnd:Awake()
    self.gameObject:GetComponent(typeof(UIPanel)).IgnoreIphoneXMargin = true
end

function LuaPreciousItemShareWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaPreciousItemShareWnd:Init()
	self.closeBtn = self.transform:Find("Anchor/BottomRight/Panel/CloseButton").gameObject
	
	self.itemTitleLabel = self.transform:Find("Anchor/Content/Label"):GetComponent(typeof(UILabel))
	self.itemIcon = self.transform:Find("Anchor/Content/ItemIcon"):GetComponent(typeof(CUITexture))
	self.itemBorder = self.transform:Find("Anchor/Content/ItemIcon/Border"):GetComponent(typeof(UISprite))
	self.itemNameLabel = self.transform:Find("Anchor/Content/ItemNameLabel"):GetComponent(typeof(UILabel))
	
	self.playerIcon = self.transform:Find("Anchor/PlayerInfo/Icon"):GetComponent(typeof(CUITexture))
	self.playerLevelLabel = self.transform:Find("Anchor/PlayerInfo/Icon/LevelLabel"):GetComponent(typeof(UILabel))
	self.playerNameLabel = self.transform:Find("Anchor/PlayerInfo/Name/Label"):GetComponent(typeof(UILabel))
	self.serverNameLabel = self.transform:Find("Anchor/PlayerInfo/Server/Label"):GetComponent(typeof(UILabel))
	self.playerIDLabel = self.transform:Find("Anchor/PlayerInfo/ID/Label"):GetComponent(typeof(UILabel))
	
	self.excludedRoot = self.transform:Find("Anchor/BottomRight/Panel").gameObject
	self.shareBtn = self.transform:Find("Anchor/BottomRight/Panel/ShareButton").gameObject
	self.shareMenu = self.transform:Find("Anchor/BottomRight/Panel/Menu").gameObject
	
	self.shareMenu:SetActive(false) -- 先隐藏分享菜单
	
	CommonDefs.AddOnClickListener(self.closeBtn, DelegateFactory.Action_GameObject(function(go) self:Close() end), false)
	CommonDefs.AddOnClickListener(self.shareBtn, DelegateFactory.Action_GameObject(function(go) self:OnShareButtonClick(go) end), false)
	if CommonDefs.IS_VN_CLIENT then
		self.shareBtn:SetActive(false)
	end

	local citem = CItemMgr.Inst:GetById(CPreciousItemShareMgr.Inst.ShareItemId)
	if not citem then
		self:Close()
	  	return 
	end

	self.itemTitleLabel.text = CPreciousItemShareMgr.Inst.ShareItemTitle
	self.itemIcon:LoadMaterial(citem.Icon)

	if citem.IsItem then
		local template = Item_Item.GetData(citem.TemplateId)
		self.itemBorder.spriteName = CUICommonDef.GetItemCellBorder(template, citem, false)
	else
		self.itemBorder.spriteName = CUICommonDef.GetItemCellBorder(citem.Equip.QualityType)
	end
	self.itemNameLabel.text = citem.ColoredName

	local mainplayer = CClientMainPlayer.Inst
	if mainplayer then
		self.playerIcon:LoadNPCPortrait(mainplayer.PortraitName, false)
		self.playerLevelLabel.text = CUICommonDef.GetMainPlayerColoredLevelString(self.playerLevelLabel.color)
		self.playerNameLabel.text = mainplayer.Name
		self.serverNameLabel.text = CLoginMgr.Inst:GetSelectedGameServer().name
		self.playerIDLabel.text = tostring(mainplayer.Id)
	else
		self.playerIcon:Clear()
		self.playerLevelLabel.text = nil
		self.playerNameLabel.text = nil
		self.serverNameLabel.text = CLoginMgr.Inst:GetSelectedGameServer().name
		self.playerIDLabel.text = nil
	end

	CPreciousItemShareMgr.Inst:HideUIBeforeCapture()
end

function LuaPreciousItemShareWnd:OnShareButtonClick( go )
	if not self.shareMenu then return end
	CPreciousItemShareMgr.Inst:SharePreciousItem2Other(self.excludedRoot)
end

function LuaPreciousItemShareWnd:OnEnable()
	g_ScriptEvent:AddListener("ViewLoadingQueueFinish", self, "ViewLoadingQueueFinish")
end

function LuaPreciousItemShareWnd:OnDisable()
	g_ScriptEvent:RemoveListener("ViewLoadingQueueFinish", self, "ViewLoadingQueueFinish")
end

function LuaPreciousItemShareWnd:ViewLoadingQueueFinish()
	CPreciousItemShareMgr.Inst:HideUIBeforeCapture()
end

function LuaPreciousItemShareWnd:OnDestroy( go )
	CPreciousItemShareMgr.Inst:ShowUIAfterCapture()
	self:Close()
end

return LuaPreciousItemShareWnd
