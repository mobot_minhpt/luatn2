local UITabBar = import "L10.UI.UITabBar"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UITable = import "UITable"
local CButton = import "L10.UI.CButton"
local GameObject = import "UnityEngine.GameObject"
local QnButton = import "L10.UI.QnButton"
local UILabel = import "UILabel"
local SoundManager = import "SoundManager"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CNumberKeyBoardMgr = import "CNumberKeyBoardMgr"
local CTooltipAlignType= import "L10.UI.CTooltip+AlignType"
local PlayerSettings = import "L10.Game.PlayerSettings"
LuaDaFuWongVoicePackageWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaDaFuWongVoicePackageWnd, "TabTable", "TabTable", UITable)
RegistChildComponent(LuaDaFuWongVoicePackageWnd, "VoiceTable", "VoiceTable", UITable)
RegistChildComponent(LuaDaFuWongVoicePackageWnd, "OKBtn", "OKBtn", CButton)
RegistChildComponent(LuaDaFuWongVoicePackageWnd, "GetBtn", "GetBtn", CButton)
RegistChildComponent(LuaDaFuWongVoicePackageWnd, "Sections", "Sections", GameObject)
RegistChildComponent(LuaDaFuWongVoicePackageWnd, "VoiceTemplate", "VoiceTemplate", GameObject)
RegistChildComponent(LuaDaFuWongVoicePackageWnd, "LeftBtn", "LeftBtn", QnButton)
RegistChildComponent(LuaDaFuWongVoicePackageWnd, "RightBtn", "RightBtn", QnButton)
RegistChildComponent(LuaDaFuWongVoicePackageWnd, "PageNum", "PageNum", UILabel)
RegistChildComponent(LuaDaFuWongVoicePackageWnd, "Left", "Left", UITabBar)

--@endregion RegistChildComponent end
RegistClassMember(LuaDaFuWongVoicePackageWnd, "m_TabList")
RegistClassMember(LuaDaFuWongVoicePackageWnd, "m_VoiceList")
RegistClassMember(LuaDaFuWongVoicePackageWnd, "m_CurSelectTab")
RegistClassMember(LuaDaFuWongVoicePackageWnd, "m_CurSelectVoice")
RegistClassMember(LuaDaFuWongVoicePackageWnd, "m_CurPage")
RegistClassMember(LuaDaFuWongVoicePackageWnd, "m_MaxVoiceNumOnePage")
RegistClassMember(LuaDaFuWongVoicePackageWnd, "m_MaxVoiceNum")
RegistClassMember(LuaDaFuWongVoicePackageWnd, "m_VoiceTick")
RegistClassMember(LuaDaFuWongVoicePackageWnd, "m_SoundPlayingEvent")
RegistClassMember(LuaDaFuWongVoicePackageWnd, "m_VoiceDataList")
function LuaDaFuWongVoicePackageWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.OKBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOKBtnClick()
	end)


	
	UIEventListener.Get(self.GetBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnGetBtnClick()
	end)


	
	UIEventListener.Get(self.LeftBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLeftBtnClick()
	end)


	
	UIEventListener.Get(self.RightBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRightBtnClick()
	end)

	UIEventListener.Get(self.PageNum.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnPageNameClick()
	end)

    --@endregion EventBind end
	self.VoiceTemplate.gameObject:SetActive(false)
	self.Sections.gameObject:SetActive(false)
	self.Left.enabled = false
	self.m_CurSelectTab = -1
	self.m_CurSelectVoice = 0
	self.m_MaxVoiceNumOnePage = 7
	self.m_VoiceList = {}
	self.m_VoiceDataList = nil
	self.m_CurPage  = 0
	self.m_MaxVoiceNum = nil
	self.m_TabList = nil
	self.m_VoiceTick = nil
	self.m_SoundPlayingEvent = nil
end

function LuaDaFuWongVoicePackageWnd:Init()
	self.LeftBtn.gameObject:SetActive(false)
	self.RightBtn.gameObject:SetActive(false)
	self.PageNum.gameObject:SetActive(false)
	self.OKBtn.gameObject:SetActive(false)
	self.GetBtn.gameObject:SetActive(false)	
	self:InitData()
	
	if not LuaDaFuWongMgr.VoicePackage then
        Gac2Gas.RequestDaFuWengPlayData()
    else
		self:InitRightTable()
		self:InitLeftTable()
	end
end

function LuaDaFuWongVoicePackageWnd:InitData()
	self.m_VoiceDataList = {}
	self.m_MaxVoiceNum = {}
	DaFuWeng_VoiceList.Foreach(function(k,v)
		if not self.m_VoiceDataList[v.PackageID] then self.m_VoiceDataList[v.PackageID] = {} end
		if v.NotShowOnPreview ~= 1 then table.insert(self.m_VoiceDataList[v.PackageID],{desc = v.Describe, path = v.VoicePath, time = v.Duration}) end
	end)
	for k,v in pairs(self.m_VoiceDataList) do
		self.m_MaxVoiceNum[k] = #v
	end
end

function LuaDaFuWongVoicePackageWnd:InitLeftTable()
	self.m_TabList = {}
	Extensions.RemoveAllChildren(self.TabTable.transform)
	local voiceData = LuaDaFuWongMgr.VoicePackage
	local count = 0
	local index = 0
	DaFuWeng_VoicePackage.Foreach(function(k,v)
		local go = CUICommonDef.AddChild(self.TabTable.gameObject,self.Sections)
		go.gameObject:SetActive(true)
		go.transform:Find("Label"):GetComponent(typeof(UILabel)).text = v.Title
		local lock = go.transform:Find("lock").gameObject
		local leftTime = go.transform:Find("LeftTime"):GetComponent(typeof(UILabel))
		local durationTime = 0--v.Duration * 3600 * 24
		local isLock = true
		local isOutData = false
		local isUse = voiceData.use == k
		if voiceData.bag[k] then
			isLock = false
			lock.gameObject:SetActive(false)
			leftTime.gameObject:SetActive(true)
			if CServerTimeMgr.Inst.timeStamp >= voiceData.bag[k] + durationTime then
				isOutData = true
				leftTime.text = LocalString.GetString("已过期")
			else
				isOutData = false
				leftTime.text = self:GetLeftTime(voiceData.bag[k] + durationTime)
			end
		else
			isLock = true
			lock.gameObject:SetActive(true)
			leftTime.gameObject:SetActive(false)
		end
		local Use = go.transform:Find("Use").gameObject
		Use:SetActive(isUse)
		go.transform:Find("bg").gameObject:SetActive(true)
		go.transform:Find("Highlight").gameObject:SetActive(false)
		self.m_TabList[index] = {id = k,go = go, lock = lock,leftTime = leftTime,Use = Use,
		isLock = isLock,isOutData = isOutData, isUse = isUse,
		type = v.Type,btnDesc = v.BtnDesc,Message = v.Message,duration = v.Duration}
		index = index + 1
	end)
	self.TabTable:Reposition()
	self.Left.enabled = true
    self.Left:ReloadTabButtons()
	if not self.Left.OnTabChange then
		self.Left.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
			self:OnTabChange(index)
		end)
	end
	self.Left:ChangeTab(0,false)
end

function LuaDaFuWongVoicePackageWnd:GetLeftTime(endTime)
	local leftTime = endTime - CServerTimeMgr.Inst.timeStamp
	if leftTime <= 3600 then return LocalString.GetString("剩余1小时")
	elseif leftTime <= (3600 * 24) then	-- 不足一天
		local leftHour = math.floor(leftTime / 3600)
		return SafeStringFormat3(LocalString.GetString("剩余%d小时"),leftHour)
	else								-- 大于一天
		local leftDay = math.floor(leftTime / (3600 * 24))
		return SafeStringFormat3(LocalString.GetString("剩余%d天"),leftDay)
	end 
end

function LuaDaFuWongVoicePackageWnd:OnPageNameClick()
	-- local maxPage = math.ceil(self.m_MaxVoiceNum / self.m_MaxVoiceNumOnePage)
	-- CNumberKeyBoardMgr.ShowNumberKeyBoardWithRange(1, maxPage, self.m_CurPage, 1, DelegateFactory.Action_int(function (val) 
	-- 		local page = math.min(math.max(val,1),maxPage)
	-- 		self.PageNum.text = SafeStringFormat3("%d/%d",page,maxPage)
	-- 		self:UpdateRightTable(page)
	-- 		CUIManager.CloseUI(CUIResources.NumKeyBoardWnd)
	-- 	end), DelegateFactory.Action_int(function (val) 
	-- 	end), self.PageNum, CTooltipAlignType.Right, true)
end

function LuaDaFuWongVoicePackageWnd:OnTabChange(index)
	self:SetSectionGoClick(self.m_CurSelectTab,false)
	self:SetSectionGoClick(index,true)
	self.m_CurSelectTab = index
	self:UpdateRightTable(self.m_CurPage <= 0 and 1 or self.m_CurPage)
	self:UpdateBtn()
end

function LuaDaFuWongVoicePackageWnd:InitRightTable()
	Extensions.RemoveAllChildren(self.VoiceTable.transform)
	for i = 1,self.m_MaxVoiceNumOnePage do
		local go = CUICommonDef.AddChild(self.VoiceTable.gameObject,self.VoiceTemplate)
		go.gameObject:SetActive(true)
		local label = go.transform:Find("Label"):GetComponent(typeof(UILabel))
		local SelectLabel = go.transform:Find("clickbg/Label"):GetComponent(typeof(UILabel))
		local clickBg =  go.transform:Find("clickbg").gameObject
		local ListenBtn = go.transform:Find("clickbg/ListenBtn").gameObject
		self.m_VoiceList[i] = {go = go, label = label,selectLabel = SelectLabel ,clickBg = clickBg,listenBtn = listenBtn}
		UIEventListener.Get(go.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
			self:OnVoiceBtnClick(i)
		end)
	end
	self.VoiceTable:Reposition()
end

function LuaDaFuWongVoicePackageWnd:UpdateRightTable(page)
	self.m_CurPage = page
	if self.m_CurSelectVoice and self.m_VoiceList[self.m_CurSelectVoice] then
		self:SetVoiceGoClick(self.m_CurSelectVoice,false)
		self.m_CurSelectVoice = 0
		if self.m_SoundPlayingEvent then
			SoundManager.Inst:StopSound(self.m_SoundPlayingEvent)
			self.m_SoundPlayingEvent=nil
		end
		if self.m_VoiceTick then UnRegisterTick(self.m_VoiceTick) self.m_VoiceTick = nil end
	end
	local startIndex = (page - 1) * self.m_MaxVoiceNumOnePage
	for i = 1,self.m_MaxVoiceNumOnePage do
		local viewData = self.m_VoiceList[i]
		local data = self.m_VoiceDataList[self.m_CurSelectTab + 1] and self.m_VoiceDataList[self.m_CurSelectTab + 1][startIndex + i]
		if data then
			viewData.go.gameObject:SetActive(true)
			viewData.clickBg.gameObject:SetActive(false)
			viewData.label.text = data.desc
			viewData.selectLabel.text = data.desc
		else
			viewData.go.gameObject:SetActive(false)
		end
	end

	self:UpdatePageInfo()
end

function LuaDaFuWongVoicePackageWnd:UpdateBtn()
	if self.m_TabList and self.m_TabList[self.m_CurSelectTab] then
		local viewData = self.m_TabList[self.m_CurSelectTab]
		--local id = viewData.id
		--print(id,data.BtnDesc,DaFuWeng_VoicePackage.GetData(id).BtnDesc)
		if viewData.isLock or viewData.isOutData then
			self.OKBtn.gameObject:SetActive(false)
			self.GetBtn.gameObject:SetActive(true)
			self.GetBtn.transform:Find("Label"):GetComponent(typeof(UILabel)).text = viewData.btnDesc
		else
			self.OKBtn.gameObject:SetActive(true)
			self.GetBtn.gameObject:SetActive(false)
			if viewData.isUse then
				self.OKBtn.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("使用中")
				self.OKBtn.Enabled = false
			else
				self.OKBtn.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("使用")
				self.OKBtn.Enabled = true
			end
		end
	else
		self.OKBtn.gameObject:SetActive(false)
		self.GetBtn.gameObject:SetActive(false)	
	end
end

function LuaDaFuWongVoicePackageWnd:OnVoiceBtnClick(index)
	self:SetVoiceGoClick(self.m_CurSelectVoice,false)
	self:SetVoiceGoClick(index,true)
	self.m_CurSelectVoice = index
	-- if self.m_SoundPlayingEvent then
	-- 	SoundManager.Inst:StopSound(self.m_SoundPlayingEvent)
    --     self.m_SoundPlayingEvent=nil
	-- end
	if self.m_VoiceTick then UnRegisterTick(self.m_VoiceTick) self.m_VoiceTick = nil end
	local pageIndex = self.m_CurSelectTab
	local voiceId = (self.m_CurPage - 1) * self.m_MaxVoiceNumOnePage + index
	local voice = self.m_VoiceDataList[pageIndex + 1]
	if voice then 
		local path = voice[voiceId].path
		if path then
			local length = voice[voiceId].time
			if not PlayerSettings.SoundEnabled then
				local message = g_MessageMgr:FormatMessage("DaFuWeng_VoicePackage_NotOpenSound")
				g_MessageMgr:ShowOkCancelMessage(message, function()
					PlayerSettings.SoundEnabled = true
					self:PlayVoice(path,length)
				end, function() self:PlayVoice(path,length) end, nil, nil, true)
			else
				self:PlayVoice(path,length)
			end

		end
	end
end

function LuaDaFuWongVoicePackageWnd:PlayVoice(path,length)
	--self.m_SoundPlayingEvent = SoundManager.Inst:PlayOneShot(path,Vector3.zero,nil,0)
	SoundManager.Inst:StartDialogSound(path)
	self.m_VoiceTick = RegisterTickOnce(function()
		if self.m_CurSelectVoice and self.m_VoiceList[self.m_CurSelectVoice] then
			self:SetVoiceGoClick(self.m_CurSelectVoice,false)
			self.m_CurSelectVoice = 0
		end
	end,length * 1000)
end

function LuaDaFuWongVoicePackageWnd:SetSectionGoClick(index,isClick)
	if self.m_TabList and self.m_TabList[index] then
		self.m_TabList[index].go.transform:Find("bg").gameObject:SetActive(not isClick)
		self.m_TabList[index].go.transform:Find("Highlight").gameObject:SetActive(isClick)
	end
end

function LuaDaFuWongVoicePackageWnd:SetVoiceGoClick(index,isClick)
	if self.m_VoiceList and self.m_VoiceList[index] then
		self.m_VoiceList[index].clickBg.gameObject:SetActive(isClick)
	end
end

function LuaDaFuWongVoicePackageWnd:OnDataUpdate()
	if #self.m_VoiceList < 1 then self:InitRightTable() end 
	if not self.m_TabList then
		self:InitLeftTable()
	else
		local voiceData = LuaDaFuWongMgr.VoicePackage
		for k,v in pairs(self.m_TabList) do
			local id = v.id
			local durationTime = 0--v.duration * 3600 * 24
			local isLock = true
			local isOutData = false
			local isUse = voiceData.use == id

			if voiceData.bag[id] then
				isLock = false
				v.lock.gameObject:SetActive(false)
				v.leftTime.gameObject:SetActive(true)
				if CServerTimeMgr.Inst.timeStamp >= voiceData.bag[id] + durationTime then
					isOutData = true
					v.leftTime.text = LocalString.GetString("已过期")
				else
					v.leftTime.text = self:GetLeftTime(voiceData.bag[id] + durationTime)
				end
			else
				isLock = true
				v.lock.gameObject:SetActive(true)
				v.leftTime.gameObject:SetActive(false)
			end
			v.Use:SetActive(voiceData.use == id)
			self.m_TabList[k].isLock = isLock
			self.m_TabList[k].isOutData = isOutData
			self.m_TabList[k].isUse = isUse
		end
		self:UpdateBtn()
	end
	
end

function LuaDaFuWongVoicePackageWnd:UpdatePageInfo()
	self.LeftBtn.gameObject:SetActive(true)
	self.RightBtn.gameObject:SetActive(true)
	self.PageNum.gameObject:SetActive(true)
	local maxPage = math.ceil(self.m_MaxVoiceNum[self.m_CurSelectTab + 1] / self.m_MaxVoiceNumOnePage)
	local curPage = self.m_CurPage
	self.PageNum.text = SafeStringFormat3("%d/%d",curPage,maxPage)
	self.LeftBtn.Enabled = curPage > 1
	self.RightBtn.Enabled = curPage < maxPage
end
--@region UIEvent

function LuaDaFuWongVoicePackageWnd:OnOKBtnClick()
	if self.m_TabList and self.m_TabList[self.m_CurSelectTab] then
		--print(self.m_TabList[self.m_CurSelectTab].id)
		Gac2Gas.RequestUseDaFuWengVoicePackage(self.m_TabList[self.m_CurSelectTab].id)
	end
end

function LuaDaFuWongVoicePackageWnd:OnGetBtnClick()
	if self.m_TabList and self.m_TabList[self.m_CurSelectTab] then
		local viewData = self.m_TabList[self.m_CurSelectTab]
		local id = viewData.id
		if viewData.type == 1 then
			LuaDaFuWongMgr:ShowBuyVoicePackageWnd(id)
		else
			g_MessageMgr:ShowMessage("CUSTOM_STRING2",viewData.Message)
		end
	end
end

function LuaDaFuWongVoicePackageWnd:OnLeftBtnClick()
	local maxPage = math.ceil(self.m_MaxVoiceNum[self.m_CurSelectTab + 1] / self.m_MaxVoiceNumOnePage)
	self:UpdateRightTable(math.max(1,self.m_CurPage - 1))
end

function LuaDaFuWongVoicePackageWnd:OnRightBtnClick()
	local maxPage = math.ceil(self.m_MaxVoiceNum[self.m_CurSelectTab + 1] / self.m_MaxVoiceNumOnePage)
	self:UpdateRightTable(math.min(maxPage,self.m_CurPage + 1))
end

--@endregion UIEvent

function LuaDaFuWongVoicePackageWnd:OnEnable()
	g_ScriptEvent:AddListener("OnTongXingZhengDataUpdate", self, "OnDataUpdate")
end

function LuaDaFuWongVoicePackageWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnTongXingZhengDataUpdate", self, "OnDataUpdate")
	g_ScriptEvent:BroadcastInLua("OnSoundStatusUpdate")
	if self.m_SoundPlayingEvent then
		SoundManager.Inst:StopSound(self.m_SoundPlayingEvent)
        self.m_SoundPlayingEvent=nil
	end
	if self.m_VoiceTick then UnRegisterTick(self.m_VoiceTick) self.m_VoiceTick = nil end
end
