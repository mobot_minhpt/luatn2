-- Auto Generated!!
local CommonDefs = import "L10.Game.CommonDefs"
local CPVPPromotionAwardItem = import "L10.UI.CPVPPromotionAwardItem"
local CUICommonDef = import "L10.UI.CUICommonDef"
local EnumQualityType = import "L10.Game.EnumQualityType"
local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"
local IdPartition = import "L10.Game.IdPartition"
local Item_Item = import "L10.Game.Item_Item"
CPVPPromotionAwardItem.m_Init_CS2LuaHook = function (this, itemId, itemTemplateId, count) 
    this.ItemId = (itemId == nil and "" or itemId)
    this.ItemTemplateId = itemTemplateId
    this.iconTexture.mainTexture = nil
    this.iconTexture.material = nil
    this.amountLabel.text = nil
    this.qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(EnumQualityType.White)

    if IdPartition.IdIsItem(itemTemplateId) then
        local item = Item_Item.GetData(itemTemplateId)
        if item ~= nil then
            this.iconTexture:LoadMaterial(item.Icon)
            if count > 1 then
                this.amountLabel.text = tostring(count)
            else
                this.amountLabel.text = nil
            end
            this.qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(EnumQualityType.White)
        end
    elseif IdPartition.IdIsEquip(itemTemplateId) then
        local equip = EquipmentTemplate_Equip.GetData(itemTemplateId)
        if equip ~= nil then
            this.iconTexture:LoadMaterial(equip.Icon)
            if count > 1 then
                this.amountLabel.text = tostring(count)
            else
                this.amountLabel.text = nil
            end
            this.qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(CommonDefs.ConvertIntToEnum(typeof(EnumQualityType), equip.Color))
        end
    end
end
