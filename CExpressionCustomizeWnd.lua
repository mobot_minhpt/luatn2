-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CExpressionCustomizeItem = import "L10.UI.CExpressionCustomizeItem"
local CExpressionCustomizeWnd = import "L10.UI.CExpressionCustomizeWnd"
local CExpressionMgr = import "L10.Game.CExpressionMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CUITexture = import "L10.UI.CUITexture"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local ExpressionHead_ProfileFrame = import "L10.Game.ExpressionHead_ProfileFrame"
local ExpressionHead_Sticker = import "L10.Game.ExpressionHead_Sticker"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local QnButton = import "L10.UI.QnButton"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local UITexture = import "UITexture"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CExpressionCustomizeWnd.m_SortProfileFrame_CS2LuaHook = function (this, p1, p2) 
    local a = ExpressionHead_ProfileFrame.GetData(p1)
    local b = ExpressionHead_ProfileFrame.GetData(p2)
    local aHas = CExpressionMgr.Inst:HasGetProfileFrame(p1) ~= 0
    local bHas = CExpressionMgr.Inst:HasGetProfileFrame(p2) ~= 0
    if aHas ~= bHas then
        if aHas then
            return -1
        else 
            return 1
        end
    end

    if a.StickerOrder > b.StickerOrder then
        return 1
    elseif a.StickerOrder < b.StickerOrder then
        return - 1
    end
    return 0
end
CExpressionCustomizeWnd.m_SortSticker_CS2LuaHook = function (this, p1, p2) 
    local a = ExpressionHead_Sticker.GetData(p1)
    local b = ExpressionHead_Sticker.GetData(p2)
    if a.StickerOrder > b.StickerOrder then
        return 1
    elseif a.StickerOrder < b.StickerOrder then
        return - 1
    end
    return 0
end
CExpressionCustomizeWnd.m_SortTxt_CS2LuaHook = function (this, p1, p2) 
    local a = ExpressionHead_ExpressionTxt.GetData(p1)
    local b = ExpressionHead_ExpressionTxt.GetData(p2)
    if a.TxtOrder > b.TxtOrder then
        return 1
    elseif a.TxtOrder < b.TxtOrder then
        return - 1
    end
    return 0
end
CExpressionCustomizeWnd.m_Init_CS2LuaHook = function (this) 
    --初始化数据
    --初始化数据
    if CClientMainPlayer.Inst ~= nil then
        this.icon:LoadNPCPortrait(CClientMainPlayer.Inst.PortraitName, false)
    end

    this.imageHandler.gameObject:SetActive(false)

    this.imageHandler.onCancle = MakeDelegateFromCSFunction(this.OnImageHandlerCancle, Action0, this)

    Gac2Gas.QueryValidExpressionTxt()
    Gac2Gas.QueryValidSticker()

    this.mExpressionTxtId = CClientMainPlayer.Inst.BasicProp.ExpressionTxt
    this.mStickerId = CClientMainPlayer.Inst.BasicProp.Sticker
    this.mProfileFrameId = CClientMainPlayer.Inst.BasicProp.ProfileInfo.Frame
    this:InitImage()

    this.tableView.m_DataSource = this
    this.tableView.OnSelectAtRow = MakeDelegateFromCSFunction(this.OnSelectAtRow, MakeGenericClass(Action1, Int32), this)
    if CExpressionMgr.EnableProfileFrame then
        this.radioBox:SetVisibleGroup(Table2ArrayWithCount({0, 1, 2}, 3, MakeArrayClass(System.Int32)), false)
    else
        this.radioBox:SetVisibleGroup(Table2ArrayWithCount({0, 1}, 2, MakeArrayClass(System.Int32)), false)
    end
end
CExpressionCustomizeWnd.m_OnImageHandlerCancle_CS2LuaHook = function (this) 
    this.tableView:SetSelectRow(- 1, true)
    --清空列表选择
    this.imageHandler.gameObject:SetActive(false)

    local widget = this.imageHandler:GetRelatedWidget()
    if widget ~= nil then
        if widget == this.expressionTxtTexture.texture then
            this.mExpressionTxtId = 0
            -- CClientMainPlayer.Inst.BasicProp.ExpressionTxt;
            this.expressionTxtTexture:Clear()
        else
            this.mStickerId = 0
            this.stickerTexture:Clear()
        end
    end
    this.imageHandler:SetRelatedWidget(nil)
end
CExpressionCustomizeWnd.m_InitImage_CS2LuaHook = function (this) 

    --if (isTxt)
    do
        local data = ExpressionHead_ExpressionTxt.GetData(this.mExpressionTxtId)
        if data ~= nil then
            if CExpressionMgr.Inst:HasGetExpressionTxt(data.ID) then
                this.expressionTxtTexture:LoadMaterial(data.ResName)
                local extra = CExpressionMgr.Inst.expression_extra
                this.imageHandler:InitTxtParams(this.expressionTxtTexture.texture, extra.expression_txt_offset_x, extra.expression_txt_offset_y, extra.expression_txt_scale_x, extra.expression_txt_scale_y, extra.expression_txt_rotation)
            else
                this.expressionTxtTexture:Clear()
            end
        end
    end
    --else
    do
        local data = ExpressionHead_Sticker.GetData(this.mStickerId)
        if data ~= nil then
            if CExpressionMgr.Inst:HasGetSticker(data.ID) then
                this.stickerTexture:LoadMaterial(data.ResName)
                local extra = CExpressionMgr.Inst.expression_extra
                this.imageHandler:InitStickerParams(this.stickerTexture.texture, extra.sticker_offset_x, extra.sticker_offset_y, extra.sticker_scale_x, extra.sticker_scale_y, extra.sticker_rotation)
            else
                this.stickerTexture:Clear()
            end
        end
    end
    if CExpressionMgr.EnableProfileFrame then
        local data = ExpressionHead_ProfileFrame.GetData(this.mProfileFrameId)
        if data ~= nil then
            local frameTime = CExpressionMgr.Inst:HasGetProfileFrame(data.ID)
            if frameTime ~= 0 then
                --profileFrameTexture.LoadMaterial(data.ResName);
                CommonDefs.GetComponent_Component_Type(this.profileFrameNode1.transform:Find("texture"), typeof(CUITexture)):LoadMaterial(data.ResName)
                CommonDefs.GetComponent_Component_Type(this.profileFrameNode2.transform:Find("texture"), typeof(CUITexture)):LoadMaterial(data.ResName)
                CommonDefs.GetComponent_Component_Type(this.singleProfileFrameNode.transform:Find("texture"), typeof(CUITexture)):LoadMaterial(data.ResName)
                local timeText = ""
                if frameTime > 0 then
                    local endTime = CServerTimeMgr.ConvertTimeStampToZone8Time(frameTime)
                    local now = CServerTimeMgr.Inst:GetZone8Time()
                    local span = endTime:Subtract(now)
                    if span.TotalSeconds > 0 then
                        --未过期
                        if span.TotalDays >= 1 or endTime.Day ~= now.Day then
                            timeText = System.String.Format(LocalString.GetString("[00ff00]有效期至 {0}[-]"), ToStringWrap(endTime, "yyyy-MM-dd"))
                        else
                            timeText = System.String.Format(LocalString.GetString("[00ff00]有效期至 今日{0}[-]"), ToStringWrap(endTime, "HH:mm"))
                        end
                    end
                else
                    timeText = LocalString.GetString("[00ff00]有效期至 永久[-]")
                end
                CommonDefs.GetComponent_Component_Type(this.showNode2.transform:Find("timeLabel"), typeof(UILabel)).text = timeText
            else
                --profileFrameTexture.Clear();
                CommonDefs.GetComponent_Component_Type(this.profileFrameNode1.transform:Find("texture"), typeof(CUITexture)):Clear()
                CommonDefs.GetComponent_Component_Type(this.profileFrameNode2.transform:Find("texture"), typeof(CUITexture)):Clear()
                CommonDefs.GetComponent_Component_Type(this.singleProfileFrameNode.transform:Find("texture"), typeof(CUITexture)):Clear()
            end
        end
    end
    this.imageHandler.gameObject:SetActive(false)
end
CExpressionCustomizeWnd.m_Awake_CS2LuaHook = function (this) 
    UIEventListener.Get(this.saveBtn).onClick = MakeDelegateFromCSFunction(this.OnClickSaveBtn, VoidDelegate, this)
    UIEventListener.Get(this.closeBtn).onClick = MakeDelegateFromCSFunction(this.OnClose, VoidDelegate, this)

    UIEventListener.Get(this.clearBtn).onClick = MakeDelegateFromCSFunction(this.OnClickClearBtn, VoidDelegate, this)

    UIEventListener.Get(this.expressionTxtTexture.gameObject).onClick = MakeDelegateFromCSFunction(this.OnClickExpressionTxtTexture, VoidDelegate, this)
    UIEventListener.Get(this.stickerTexture.gameObject).onClick = MakeDelegateFromCSFunction(this.OnClickStickerTexture, VoidDelegate, this)

    this.radioBox.OnSelect = MakeDelegateFromCSFunction(this.OnSelectRadioBox, MakeGenericClass(Action2, QnButton, Int32), this)
    UIEventListener.Get(this.tipBtn).onClick = MakeDelegateFromCSFunction(this.OnClickTipButton, VoidDelegate, this)
end
CExpressionCustomizeWnd.m_OnClose_CS2LuaHook = function (this, go) 
    if CClientMainPlayer.Inst ~= nil then
        if CClientMainPlayer.Inst.BasicProp.ExpressionTxt ~= this.mExpressionTxtId then
            local msg = g_MessageMgr:FormatMessage("EXPRESSION_NOT_YET_SAVED")
            MessageWndManager.ShowOKCancelMessage(msg, MakeDelegateFromCSFunction(this.Close, Action0, this), nil, nil, nil, false)
            return
        elseif CClientMainPlayer.Inst.BasicProp.Sticker ~= this.mStickerId then
            local msg = g_MessageMgr:FormatMessage("EXPRESSION_NOT_YET_SAVED")
            MessageWndManager.ShowOKCancelMessage(msg, MakeDelegateFromCSFunction(this.Close, Action0, this), nil, nil, nil, false)
            return
        elseif CClientMainPlayer.Inst.BasicProp.ProfileInfo.Frame ~= this.mProfileFrameId then
            local msg = g_MessageMgr:FormatMessage("PROFILEFRAME_NOT_YET_SAVED")
            MessageWndManager.ShowOKCancelMessage(msg, MakeDelegateFromCSFunction(this.Close, Action0, this), nil, nil, nil, false)
            return
        end
    end

    this:Close()
end
CExpressionCustomizeWnd.m_OnSelectRadioBox_CS2LuaHook = function (this, btn, index) 
    if index < 2 then
        this.showNode1:SetActive(true)
        this.showNode2:SetActive(false)
        local label = this.showNode1.transform:Find("Label"):GetComponent(typeof(UILabel))
        if index == 0 then
            this.isTxt = 1
            label.text = LocalString.GetString("文字最多添加一个")
        else
            this.isTxt = 0
            label.text = LocalString.GetString("贴图最多添加一个")
        end
        this.tableView:ReloadData(true, false)
        this.tableView:SetSelectRow(- 1, true)
    elseif index == 2 then
        this.showNode1:SetActive(false)
        this.showNode2:SetActive(true)
        CommonDefs.GetComponent_Component_Type(this.showNode2.transform:Find("timeLabel"), typeof(UILabel)).text = ""
        local id = this.mProfileFrameId
        local data = ExpressionHead_ProfileFrame.GetData(id)
        local frameTime = CExpressionMgr.Inst:HasGetProfileFrame(id)
        if frameTime ~= 0 then
            CommonDefs.GetComponent_Component_Type(this.profileFrameNode1.transform:Find("texture"), typeof(CUITexture)):LoadMaterial(data.ResName)
            CommonDefs.GetComponent_Component_Type(this.profileFrameNode2.transform:Find("texture"), typeof(CUITexture)):LoadMaterial(data.ResName)
            CommonDefs.GetComponent_Component_Type(this.singleProfileFrameNode.transform:Find("texture"), typeof(CUITexture)):LoadMaterial(data.ResName)
            local timeText = ""
            if frameTime > 0 then
                local endTime = CServerTimeMgr.ConvertTimeStampToZone8Time(frameTime)
                local now = CServerTimeMgr.Inst:GetZone8Time()
                local span = endTime:Subtract(now)
                if span.TotalSeconds > 0 then
                    --未过期
                    if span.TotalDays >= 1 or endTime.Day ~= now.Day then
                        timeText = System.String.Format(LocalString.GetString("[00ff00]有效期至 {0}[-]"), ToStringWrap(endTime, "yyyy-MM-dd"))
                    else
                        timeText = System.String.Format(LocalString.GetString("[00ff00]有效期至 今日{0}[-]"), ToStringWrap(endTime, "HH:mm"))
                    end
                end
            else
                timeText = LocalString.GetString("[00ff00]有效期至 永久[-]")
            end
            CommonDefs.GetComponent_Component_Type(this.showNode2.transform:Find("timeLabel"), typeof(UILabel)).text = timeText
        else
            CommonDefs.GetComponent_Component_Type(this.profileFrameNode1.transform:Find("texture"), typeof(CUITexture)):Clear()
            CommonDefs.GetComponent_Component_Type(this.profileFrameNode2.transform:Find("texture"), typeof(CUITexture)):Clear()
            CommonDefs.GetComponent_Component_Type(this.singleProfileFrameNode.transform:Find("texture"), typeof(CUITexture)):Clear()
        end

        if CClientMainPlayer.Inst ~= nil then
            CommonDefs.GetComponent_Component_Type(this.profileFrameNode1.transform:Find("icon"), typeof(CUITexture)):LoadNPCPortrait(CClientMainPlayer.Inst.PortraitName, false)
            CommonDefs.GetComponent_Component_Type(this.singleProfileFrameNode.transform:Find("icon"), typeof(CUITexture)):LoadNPCPortrait(CClientMainPlayer.Inst.PortraitName, false)
        end
        if not System.String.IsNullOrEmpty(CExpressionMgr.Inst.playerIconUrl) then
            this.profileFrameNode1:SetActive(true)
            this.profileFrameNode2:SetActive(true)
            this.singleProfileFrameNode:SetActive(false)

            local utex = CommonDefs.GetComponent_Component_Type(this.profileFrameNode2.transform:Find("icon"), typeof(UITexture))
            if utex == nil then
                return
            end

            local photoUrl = System.String.Format("{0}?imageView&thumbnail={1}x{2}", CExpressionMgr.Inst.playerIconUrl, utex.width, utex.height)
            CPersonalSpaceMgr.DownLoadPic(photoUrl, utex, utex.height, utex.width, nil, true, false)
        else
            this.profileFrameNode1:SetActive(false)
            this.profileFrameNode2:SetActive(false)
            this.singleProfileFrameNode:SetActive(true)
        end

        this.isTxt = 2
        this.tableView:ReloadData(true, false)
        this.tableView:SetSelectRow(- 1, true)
    end

    return
    --选中指定的行
    --if (isTxt == 1)
    --{
    --	if (mExpressionTxtId > 0)
    --	{
    --		int id = txtList.IndexOf((uint)mExpressionTxtId);
    --		tableView.SetSelectRow(id, true);
    --	}
    --	else
    --	{
    --		tableView.SetSelectRow(0, true);
    --	}
    --}
    --else if(isTxt == 0)
    --{
    --	if (mStickerId>0)
    --	{
    --		int id = stickerList.IndexOf((uint)mStickerId);
    --		tableView.SetSelectRow(id, true);
    --	}
    --	else
    --	{
    --		tableView.SetSelectRow(0, true);
    --	}
    --}
end
CExpressionCustomizeWnd.m_OnSelectAtRow_CS2LuaHook = function (this, row) 

    if this.isTxt == 1 then
        if this.txtList.Count <= row then
            return
        end

        local id = this.txtList[row]

        local data = ExpressionHead_ExpressionTxt.GetData(id)
        local label = this.showNode1.transform:Find("Label"):GetComponent(typeof(UILabel))
        label.text = LocalString.GetString("文字最多添加一个")
        if CExpressionMgr.Inst:HasGetExpressionTxt(id) then
            this.mExpressionTxtId = this.txtList[row]
            this.expressionTxtTexture:LoadMaterial(data.ResName)
            this.imageHandler.gameObject:SetActive(true)
            this.imageHandler:SetEditTxt()
            this.imageHandler:SetRelatedWidget(this.expressionTxtTexture.texture)
            
            if data.UnlockRenQi == -1 then -- 限时贴纸要显示有效期
                local expiredTime = LuaExpressionMgr.m_ValidLimitExpressionTxtList[id] or 0
                if expiredTime >= 0 then
                    expiredTime = CServerTimeMgr.ConvertTimeStampToZone8Time(expiredTime)
                    label.text = System.String.Format(LocalString.GetString("[00ff00]有效期至 {0}[-]"), ToStringWrap(expiredTime, "yyyy-MM-dd"))
                end
            end
        else
            LuaExpressionCustomizeWndMgr:ShowUnlockMsg(data)
        end
    elseif this.isTxt == 0 then
        if this.stickerList.Count <= row then
            return
        end

        local id = this.stickerList[row]

        local data = ExpressionHead_Sticker.GetData(id)
        if CExpressionMgr.Inst:HasGetSticker(id) then
            this.mStickerId = this.stickerList[row]
            this.stickerTexture:LoadMaterial(data.ResName)
            this.imageHandler.gameObject:SetActive(true)
            this.imageHandler:SetEditSticker()
            this.imageHandler:SetRelatedWidget(this.stickerTexture.texture)
        else
            LuaExpressionCustomizeWndMgr:ShowUnlockMsg(data)
        end
    elseif this.isTxt == 2 then
        if this.profileFrameList.Count <= row then
            return
        end

        local id = this.profileFrameList[row]
        local data = ExpressionHead_ProfileFrame.GetData(id)
        local frameTime = CExpressionMgr.Inst:HasGetProfileFrame(id)
        if frameTime ~= 0 then
            this.mProfileFrameId = this.profileFrameList[row]
            --profileFrameTexture.LoadMaterial(data.ResName);
            CommonDefs.GetComponent_Component_Type(this.profileFrameNode1.transform:Find("texture"), typeof(CUITexture)):LoadMaterial(data.ResName)
            CommonDefs.GetComponent_Component_Type(this.profileFrameNode2.transform:Find("texture"), typeof(CUITexture)):LoadMaterial(data.ResName)
            CommonDefs.GetComponent_Component_Type(this.singleProfileFrameNode.transform:Find("texture"), typeof(CUITexture)):LoadMaterial(data.ResName)

            local timeText = ""
            if frameTime > 0 then
                local endTime = CServerTimeMgr.ConvertTimeStampToZone8Time(frameTime)
                local now = CServerTimeMgr.Inst:GetZone8Time()
                local span = endTime:Subtract(now)
                if span.TotalSeconds > 0 then
                    --未过期
                    if span.TotalDays >= 1 or endTime.Day ~= now.Day then
                        timeText = System.String.Format(LocalString.GetString("[00ff00]有效期至 {0}[-]"), ToStringWrap(endTime, "yyyy-MM-dd"))
                    else
                        timeText = System.String.Format(LocalString.GetString("[00ff00]有效期至 今日{0}[-]"), ToStringWrap(endTime, "HH:mm"))
                    end
                end
            else
                timeText = LocalString.GetString("[00ff00]有效期至 永久[-]")
            end
            CommonDefs.GetComponent_Component_Type(this.showNode2.transform:Find("timeLabel"), typeof(UILabel)).text = timeText
        else
            g_MessageMgr:ShowMessage("UNLOCK_PROFILEFRAME")
        end
    end
end
CExpressionCustomizeWnd.m_OnClickExpressionTxtTexture_CS2LuaHook = function (this, go) 
    if CClientMainPlayer.Inst == nil then
        return
    end
    if this.mExpressionTxtId > 0 then
        this.imageHandler.gameObject:SetActive(true)
        this.imageHandler:SetEditTxt()
        this.imageHandler:SetRelatedWidget(this.expressionTxtTexture.texture)
    end
end
CExpressionCustomizeWnd.m_OnClickStickerTexture_CS2LuaHook = function (this, go) 
    if CClientMainPlayer.Inst == nil then
        return
    end
    if this.mStickerId > 0 then
        this.imageHandler.gameObject:SetActive(true)
        this.imageHandler:SetEditSticker()
        this.imageHandler:SetRelatedWidget(this.stickerTexture.texture)
    end
end
CExpressionCustomizeWnd.m_OnClickClearBtn_CS2LuaHook = function (this, go) 
    this.mExpressionTxtId = 0
    this.tableView:SetSelectRow(- 1, true)
    --清空列表选择
    this.expressionTxtTexture:Clear()
    this.imageHandler.gameObject:SetActive(false)
    this.imageHandler:SetRelatedWidget(nil)

    this.mStickerId = 0
    this.stickerTexture:Clear()

    this.mProfileFrameId = 0
    CommonDefs.GetComponent_Component_Type(this.profileFrameNode1.transform:Find("texture"), typeof(CUITexture)):Clear()
    CommonDefs.GetComponent_Component_Type(this.profileFrameNode2.transform:Find("texture"), typeof(CUITexture)):Clear()
    CommonDefs.GetComponent_Component_Type(this.singleProfileFrameNode.transform:Find("texture"), typeof(CUITexture)):Clear()
end
CExpressionCustomizeWnd.m_OnEnable_CS2LuaHook = function (this) 
    EventManager.AddListenerInternal(EnumEventType.QueryValidExpressionTxtResult, MakeDelegateFromCSFunction(this.OnQueryValidExpressionTxtResult, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.QueryValidStickerResult, MakeDelegateFromCSFunction(this.OnQueryValidStickerResult, Action0, this))

    EventManager.AddListenerInternal(EnumEventType.UpdateMainPlayerExpressionTxt, MakeDelegateFromCSFunction(this.OnUpdatePlayerExpressionTxt, MakeGenericClass(Action1, Int32), this))
    EventManager.AddListenerInternal(EnumEventType.UpdateMainPlayerSticker, MakeDelegateFromCSFunction(this.OnUpdateMainPlayerSticker, Action0, this))
end
CExpressionCustomizeWnd.m_OnDisable_CS2LuaHook = function (this) 
    EventManager.RemoveListenerInternal(EnumEventType.QueryValidExpressionTxtResult, MakeDelegateFromCSFunction(this.OnQueryValidExpressionTxtResult, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.QueryValidStickerResult, MakeDelegateFromCSFunction(this.OnQueryValidStickerResult, Action0, this))

    EventManager.RemoveListenerInternal(EnumEventType.UpdateMainPlayerExpressionTxt, MakeDelegateFromCSFunction(this.OnUpdatePlayerExpressionTxt, MakeGenericClass(Action1, Int32), this))
    EventManager.RemoveListenerInternal(EnumEventType.UpdateMainPlayerSticker, MakeDelegateFromCSFunction(this.OnUpdateMainPlayerSticker, Action0, this))
end
CExpressionCustomizeWnd.m_NumberOfRows_CS2LuaHook = function (this, view) 
    if this.isTxt == 1 then
        return this.txtList.Count
    elseif this.isTxt == 0 then
        return this.stickerList.Count
    elseif this.isTxt == 2 then
        return this.profileFrameList.Count
    end

    return 0
end
CExpressionCustomizeWnd.m_ItemAt_CS2LuaHook = function (this, view, row) 
    local cmp = TypeAs(view:GetFromPool(0), typeof(CExpressionCustomizeItem))
    if cmp ~= nil then
        if this.isTxt == 1 then
            cmp:Init(this.txtList[row], this.isTxt)
        elseif this.isTxt == 0 then
            cmp:Init(this.stickerList[row], this.isTxt)
        elseif this.isTxt == 2 then
            cmp:Init(this.profileFrameList[row], this.isTxt)
        end
    end
    return cmp
end

CExpressionCustomizeWnd.m_hookGetTxtList = function(this)
    if this.mTxtList.Count == 0 then
        ExpressionHead_ExpressionTxt.Foreach(function (k, v)
            if v.Status == 0 and v.UnlockRenQi ~= -1 or CExpressionMgr.Inst:HasGetExpressionTxt(k) then -- 未解锁的限时贴纸不显示
                this.mTxtList:Add(k)
            end
        end)
        this.mTxtList:Sort(DelegateFactory.Comparison_uint(function(p1, p2)
            return this:SortTxt(p1, p2)
        end))
    end
    return this.mTxtList
end

LuaExpressionCustomizeWndMgr = {}
function LuaExpressionCustomizeWndMgr:ShowUnlockMsg(data)
    if data.UnlockRenQi == -1 then -- 限时贴纸
        g_MessageMgr:ShowCustomMsg(LocalString.GetString("已过期"))
    elseif data.UnlockRenQi > 0 then
        g_MessageMgr:ShowMessage("UNLOCK_RENQI", data.UnlockRenQi)
    elseif data.UnlockTaskId and data.UnlockTaskId > 0 then
        local taskData = Task_Task.GetData(data.UnlockTaskId)
        g_MessageMgr:ShowMessage("UNLOCK_NOT_FINISH_TASK", taskData.Display)
    end
end

