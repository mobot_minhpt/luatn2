-- Auto Generated!!
local CIndirectUIResources = import "L10.UI.CIndirectUIResources"
local CommonDefs = import "L10.Game.CommonDefs"
local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"
local CSkillAcquisitionWnd = import "L10.UI.CSkillAcquisitionWnd"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local CUIManager = import "L10.UI.CUIManager"
local CXialvPKMgr = import "L10.Game.CXialvPKMgr"
local DelegateFactory = import "DelegateFactory"
local Ease = import "DG.Tweening.Ease"
local LuaTweenUtils = import "LuaTweenUtils"
local PathMode = import "DG.Tweening.PathMode"
local PathType = import "DG.Tweening.PathType"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
local Skill_Setting = import "L10.Game.Skill_Setting"
local Skill_TempSkill = import "L10.Game.Skill_TempSkill"
local SkillCategory = import "L10.Game.SkillCategory"
local SkillKind = import "L10.Game.SkillKind"
local SoundManager = import "SoundManager"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local Vector3 = import "UnityEngine.Vector3"
local Vector4 = import "UnityEngine.Vector4"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local EnumClass = import "L10.Game.EnumClass"
local CSkillAcquisitionMgr = import "L10.UI.CSkillAcquisitionMgr"

CSkillAcquisitionWnd.m_Init_CS2LuaHook = function (this) 

    this:ShowNewSkillInfo(false)
    this.fxRootBomb:LoadFx(CUIFxPaths.SkillAquisitionFxPath)
    this.fxRootLine:LoadFx(CUIFxPaths.CommonYellowLineFxPath)
    this:DoAni(Vector4(this.skillIconTexture.transform.localPosition.x - this.skillIconTexture.texture.width * 0.5, this.skillIconTexture.transform.localPosition.y + this.skillIconTexture.texture.height * 0.5, this.skillIconTexture.texture.width, this.skillIconTexture.texture.height), false)
    this:PlaySound()
end
CSkillAcquisitionWnd.m_DoAni_CS2LuaHook = function (this, bounds, isEllipsePath) 

    LuaTweenUtils.DOKill(this.fxRootLine.transform, false)
    local waypoints = CUICommonDef.GetWayPoints(bounds, 1, isEllipsePath)
    this.fxRootLine.transform.localPosition = waypoints[0]
    LuaTweenUtils.DOLuaLocalPath(this.fxRootLine.transform, waypoints, 2, PathType.Linear, PathMode.Ignore, 2, - 1, Ease.Linear)
end
CSkillAcquisitionWnd.m_Start_CS2LuaHook = function (this) 

    UIEventListener.Get(this.closeButton).onClick = MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this)
    UIEventListener.Get(this.skillIconTexture.gameObject).onClick = MakeDelegateFromCSFunction(this.OnSkillIconClick, VoidDelegate, this)
end
CSkillAcquisitionWnd.m_ShowNewSkillInfo_CS2LuaHook = function (this, moveNext) 

    this.skillNameLabel.text = nil
    this.skillIconTexture.material = nil
    if CSkillAcquisitionMgr.SkillIds.Count > 0 then
        if moveNext then
            CommonDefs.ListRemoveAt(CSkillAcquisitionMgr.SkillIds, CSkillAcquisitionMgr.SkillIds.Count - 1)
        end
        if CSkillAcquisitionMgr.SkillIds.Count > 0 then
            local skillId = CSkillAcquisitionMgr.SkillIds[CSkillAcquisitionMgr.SkillIds.Count - 1]
            local template = Skill_AllSkills.GetData(skillId)
            if template ~= nil then
                this.skillNameLabel.text = template.Name
                this.skillIconTexture:LoadSkillIcon(template.SkillIcon)
            end
        end
    end
end
CSkillAcquisitionWnd.m_OnCloseButtonClick_CS2LuaHook = function (this, go) 
    --根据需求#27605 同时学会多个技能后的提示信息关闭一次即可
    --点击关闭时立即关闭
    if CSkillAcquisitionMgr.s_EnableYingLingShowMultiSkill and CClientMainPlayer.Inst and CClientMainPlayer.Inst.Class == EnumClass.YingLing then
        if CSkillAcquisitionMgr.SkillIds.Count > 1 then
            this:ShowNewSkillInfo(true)
        else
            CommonDefs.ListClear(CSkillAcquisitionMgr.SkillIds)
            CUIManager.CloseUI(CIndirectUIResources.SkillAcquisitionWnd)
        end
    else
        CommonDefs.ListClear(CSkillAcquisitionMgr.SkillIds)
        CUIManager.CloseUI(CIndirectUIResources.SkillAcquisitionWnd)
    end
end
CSkillAcquisitionWnd.m_OnSkillIconClick_CS2LuaHook = function (this, go) 

    if CSkillAcquisitionMgr.SkillIds.Count > 0 then
        local skillId = CSkillAcquisitionMgr.SkillIds[CSkillAcquisitionMgr.SkillIds.Count - 1]
        CSkillInfoMgr.ShowSkillInfoWnd(skillId, true, 0, 0, nil)
    end
end
CSkillAcquisitionWnd.m_PlaySound_CS2LuaHook = function (this) 

    this:DestroySoundEvent()
    this.m_SoundEvent = SoundManager.Inst:PlayOneShot(SoundManager.UISoundEvents.NewItem, Vector3.zero, nil, 0)
end
CSkillAcquisitionWnd.m_DestroySoundEvent_CS2LuaHook = function (this) 

    if this.m_SoundEvent~=nil then
        SoundManager.Inst:StopSound(this.m_SoundEvent)
        this.m_SoundEvent = nil
    end
end

CSkillAcquisitionMgr.m_ShowSkillAquisitionWnd_CS2LuaHook = function (ids) 

    if CXialvPKMgr.Inst.InXiaLvPK or CXialvPKMgr.Inst.InXialvPKPrepare then
        return
    end

    if CQuanMinPKMgr.Inst.m_IsQuanMinPKServer then
        return
    end

    if ids == nil or ids.Count == 0 then
        return
    end
    if CSkillAcquisitionMgr.inst.noAcquisitionPromptSkills == nil then
        CSkillAcquisitionMgr.inst.noAcquisitionPromptSkills = CreateFromClass(MakeGenericClass(HashSet, UInt32))
        local data = Skill_Setting.GetData().NoAcquisitionPromptSkills
        if data ~= nil then
            CommonDefs.EnumerableIterate(data, DelegateFactory.Action_object(function (___value) 
                local skill = ___value
                CommonDefs.HashSetAdd(CSkillAcquisitionMgr.inst.noAcquisitionPromptSkills, typeof(UInt32), skill)
            end))
        end
    end

    CommonDefs.ListClear(CSkillAcquisitionMgr.inst.tmpList)

    do
        local i = 0
        while i < ids.Count do
            local skillCls = CLuaDesignMgr.Skill_AllSkills_GetClass(ids[i])
            -- 临时技能不显示获得提醒
            if not CommonDefs.HashSetContains(CSkillAcquisitionMgr.inst.noAcquisitionPromptSkills, typeof(UInt32), skillCls) and nil == Skill_TempSkill.GetData(skillCls) then
                CommonDefs.ListAdd(CSkillAcquisitionMgr.inst.tmpList, typeof(UInt32), ids[i])
            end
            i = i + 1
        end
    end

    if CSkillAcquisitionMgr.inst.tmpList.Count == 0 then
        return
    end
    CommonDefs.ListAddRange(CSkillAcquisitionMgr.inst.skillIds, CSkillAcquisitionMgr.inst.tmpList)

    if CSkillAcquisitionMgr.s_EnableYingLingShowMultiSkill and CClientMainPlayer.Inst and CClientMainPlayer.Inst.Class == EnumClass.YingLing then
        while CSkillAcquisitionMgr.inst.skillIds.Count > 2 do
            CommonDefs.ListRemoveAt(CSkillAcquisitionMgr.inst.skillIds, CSkillAcquisitionMgr.inst.skillIds.Count - 1)
        end
    end

    for i = CSkillAcquisitionMgr.inst.tmpList.Count - 1, 0, - 1 do
        local skill = Skill_AllSkills.GetData(CSkillAcquisitionMgr.inst.tmpList[i])
        if skill.ECategory == SkillCategory.Active and skill.EKind == SkillKind.CommonSkill then
            CSkillAcquisitionMgr.inst.latestAcquiredProfessionSkillId = skill.ID
            break
        end
    end

    CUIManager.ShowUI(CIndirectUIResources.SkillAcquisitionWnd)
end
CSkillAcquisitionMgr.m_ShowTempSkillsTip_CS2LuaHook = function (ids) 
    if LuaPengDaoMgr:IsInPlay() then
        return
    end
    CSkillAcquisitionMgr.TempSkillIds = ids
    CUIManager.ShowUI(CIndirectUIResources.CommonTempSkillsTip)
end
