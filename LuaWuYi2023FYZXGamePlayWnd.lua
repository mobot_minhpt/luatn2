local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local UIGrid = import "UIGrid"
local DelegateFactory  = import "DelegateFactory"
local CUICenterOnChild = import "L10.UI.CUICenterOnChild"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"

LuaWuYi2023FYZXGamePlayWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaWuYi2023FYZXGamePlayWnd, "ImageTemplate", "ImageTemplate", GameObject)
RegistChildComponent(LuaWuYi2023FYZXGamePlayWnd, "IndicatorTemplate", "IndicatorTemplate", GameObject)
RegistChildComponent(LuaWuYi2023FYZXGamePlayWnd, "IndicatorGrid", "IndicatorGrid", UIGrid)
RegistChildComponent(LuaWuYi2023FYZXGamePlayWnd, "NextArrow", "NextArrow", GameObject)
RegistChildComponent(LuaWuYi2023FYZXGamePlayWnd, "LastArrow", "LastArrow", GameObject)
RegistChildComponent(LuaWuYi2023FYZXGamePlayWnd, "TimeLabel", "TimeLabel", UILabel)
RegistChildComponent(LuaWuYi2023FYZXGamePlayWnd, "FormLabel", "FormLabel", UILabel)
RegistChildComponent(LuaWuYi2023FYZXGamePlayWnd, "LevelLabel", "LevelLabel", UILabel)
RegistChildComponent(LuaWuYi2023FYZXGamePlayWnd, "DescLabel", "DescLabel", UILabel)
RegistChildComponent(LuaWuYi2023FYZXGamePlayWnd, "RewardItem1", "RewardItem1", GameObject)
RegistChildComponent(LuaWuYi2023FYZXGamePlayWnd, "RewardItem2", "RewardItem2", GameObject)
RegistChildComponent(LuaWuYi2023FYZXGamePlayWnd, "RewardItem3", "RewardItem3", GameObject)
RegistChildComponent(LuaWuYi2023FYZXGamePlayWnd, "GetRewardInfoLabel1", "GetRewardInfoLabel1", UILabel)
RegistChildComponent(LuaWuYi2023FYZXGamePlayWnd, "GetRewardInfoLabel2", "GetRewardInfoLabel2", UILabel)
RegistChildComponent(LuaWuYi2023FYZXGamePlayWnd, "JoinButton", "JoinButton", GameObject)
RegistChildComponent(LuaWuYi2023FYZXGamePlayWnd, "RuleButton", "RuleButton", GameObject)
RegistChildComponent(LuaWuYi2023FYZXGamePlayWnd, "CancelButton", "CancelButton", GameObject)
RegistChildComponent(LuaWuYi2023FYZXGamePlayWnd, "CancelMatchLabel", "CancelMatchLabel", UILabel)
RegistChildComponent(LuaWuYi2023FYZXGamePlayWnd, "ImageReadMeLabel", "ImageReadMeLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaWuYi2023FYZXGamePlayWnd,"m_CurPage")
RegistClassMember(LuaWuYi2023FYZXGamePlayWnd,"m_ImagePaths")
RegistClassMember(LuaWuYi2023FYZXGamePlayWnd,"m_Msgs")
RegistClassMember(LuaWuYi2023FYZXGamePlayWnd,"m_CenterOnChild")
RegistClassMember(LuaWuYi2023FYZXGamePlayWnd,"m_GameplayId")
RegistClassMember(LuaWuYi2023FYZXGamePlayWnd,"m_DragStartX")

function LuaWuYi2023FYZXGamePlayWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.NextArrow.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnNextArrowClick()
	end)


	
	UIEventListener.Get(self.LastArrow.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLastArrowClick()
	end)


	
	UIEventListener.Get(self.JoinButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnJoinButtonClick()
	end)


	
	UIEventListener.Get(self.RuleButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRuleButtonClick()
	end)


	
	UIEventListener.Get(self.CancelButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCancelButtonClick()
	end)


    --@endregion EventBind end

    UIEventListener.Get(self.ImageTemplate.gameObject).onDragStart = DelegateFactory.VoidDelegate(function(g)
        self:OnDragStart()
    end)

    UIEventListener.Get(self.ImageTemplate.gameObject).onDragEnd = DelegateFactory.VoidDelegate(function(g)
        self:OnDragEnd()
    end)
end

function LuaWuYi2023FYZXGamePlayWnd:Init()
    self.ImageTemplate.gameObject:SetActive(true)
    self.IndicatorTemplate.gameObject:SetActive(false)
    self:InitLeftView()
    self:InitRightView()
    self:RefreshnMatchingState(false)
    self.m_GameplayId = WuYi2023_FenYongZhenXian.GetData().GamePlayId
    if CClientMainPlayer.Inst then
        Gac2Gas.GlobalMatch_RequestCheckSignUp(self.m_GameplayId, CClientMainPlayer.Inst.Id)
    end
end

function LuaWuYi2023FYZXGamePlayWnd:InitRightView()
    local baseInfo = WuYi2023_FenYongZhenXian.GetData().GamePlayBaseInfo
    local rewardItemIds = WuYi2023_FenYongZhenXian.GetData().RewardItemIds 
    if baseInfo.Length < 4 or rewardItemIds.Length < 3 then return end
    self.TimeLabel.text = g_MessageMgr:FormatMessage(baseInfo[0])
    self.FormLabel.text = g_MessageMgr:FormatMessage(baseInfo[1])
    self.LevelLabel.text = g_MessageMgr:FormatMessage(baseInfo[2])
    self.DescLabel.text = g_MessageMgr:FormatMessage(baseInfo[3])
    self:InitRewardItem(self.RewardItem1, rewardItemIds[0])
    self:InitRewardItem(self.RewardItem2, rewardItemIds[1])
    self:InitRewardItem(self.RewardItem3, rewardItemIds[2])
    local winTimes = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eFenYongZhenXianWinTimes)
    self.GetRewardInfoLabel1.text = SafeStringFormat3(LocalString.GetString("胜败奖 %d/1"), (winTimes and winTimes > 0) and 1 or 0)
    local extraTimes = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eFenYongZhenXianExtraTimes)
    self.GetRewardInfoLabel2.text = SafeStringFormat3(LocalString.GetString("卓越奖 %d/1"), (extraTimes and extraTimes > 0) and 1 or 0)
end

function LuaWuYi2023FYZXGamePlayWnd:InitRewardItem(item, itemId)
    local icon = item.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    local qualitySprite = item.transform:Find("QualitySprite"):GetComponent(typeof(UISprite))
    
    local itemData = Item_Item.GetData(itemId)
    icon:LoadMaterial(itemData.Icon)
    qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(itemData.NameColor)
    UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType.Default, 0, 0, 0, 0)
	end)
end

function LuaWuYi2023FYZXGamePlayWnd:InitLeftView()
    self.m_ImagePaths, self.m_Msgs = LuaWuYi2023Mgr:GetCommonImageRuleData()

    Extensions.RemoveAllChildren(self.IndicatorGrid.transform)

    for i = 1, #self.m_ImagePaths do
        local indicator = CUICommonDef.AddChild(self.IndicatorGrid.gameObject, self.IndicatorTemplate)
        indicator:SetActive(true)
    end
    self.IndicatorGrid:Reposition()

    self:UpdateImagePage(1)
end

function LuaWuYi2023FYZXGamePlayWnd:UpdateIndicator(index)
    for i = 1, self.IndicatorGrid.transform.childCount do
        local sprite = self.IndicatorGrid.transform:GetChild(i - 1):GetComponent(typeof(UITexture))
        sprite.color = NGUIText.ParseColor24(i == index and "FBE023" or "C6C6B8", 0)
    end
end

function LuaWuYi2023FYZXGamePlayWnd:UpdateImagePage(page)
    self.m_CurPage = page
    if self.m_CurPage < 1 then
        self.m_CurPage = self.m_CurPage + #self.m_ImagePaths
    end
    if self.m_CurPage > #self.m_ImagePaths then
        self.m_CurPage = self.m_CurPage - #self.m_ImagePaths
    end
    self:UpdateIndicator(self.m_CurPage)
    self.ImageReadMeLabel.text = self.m_Msgs[self.m_CurPage]

    local texture = self.ImageTemplate:GetComponent(typeof(CUITexture))
    local imagePath = self.m_ImagePaths[self.m_CurPage]
    texture:LoadMaterial(imagePath)
end

function LuaWuYi2023FYZXGamePlayWnd:OnEnable()
	g_ScriptEvent:AddListener("GlobalMatch_CheckInMatchingResult", self, "OnCheckInMatchingResult")
    g_ScriptEvent:AddListener("GlobalMatch_SignUpPlayResult", self, "OnSignUpPlayResult")
    g_ScriptEvent:AddListener("GlobalMatch_CancelSignUpResult", self, "OnCancelSignUpResult")
    g_ScriptEvent:AddListener("UpdateTempPlayTimesWithKey", self, "OnUpdateTempPlayTimesWithKey")
end

function LuaWuYi2023FYZXGamePlayWnd:OnDisable()
	g_ScriptEvent:RemoveListener("GlobalMatch_CheckInMatchingResult", self, "OnCheckInMatchingResult")
    g_ScriptEvent:RemoveListener("GlobalMatch_SignUpPlayResult", self, "OnSignUpPlayResult")
    g_ScriptEvent:RemoveListener("GlobalMatch_CancelSignUpResult", self, "OnCancelSignUpResult")
    g_ScriptEvent:RemoveListener("UpdateTempPlayTimesWithKey", self, "OnUpdateTempPlayTimesWithKey")
end

function LuaWuYi2023FYZXGamePlayWnd:OnCheckInMatchingResult(playerId, playId, isInMatching, resultStr)
    print("OnCheckInMatchingResult", playerId, playId, isInMatching, resultStr)
    if (CClientMainPlayer.Inst or {}).Id == playerId and playId == self.m_GameplayId then
        self:RefreshnMatchingState(isInMatching)
    end
end

function LuaWuYi2023FYZXGamePlayWnd:OnSignUpPlayResult(playId, success)
    if playId == self.m_GameplayId then
        self:RefreshnMatchingState(success)
    end
end

function LuaWuYi2023FYZXGamePlayWnd:OnCancelSignUpResult(playId, success)
    if playId == self.m_GameplayId then
       self:RefreshnMatchingState(not success)
    end
end

function LuaWuYi2023FYZXGamePlayWnd:OnUpdateTempPlayTimesWithKey(args)
    local key = args[0]
    if key == EnumPlayTimesKey_lua.eFenYongZhenXianWinTime then
        local winTimes = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eFenYongZhenXianWinTimes)
        self.GetRewardInfoLabel1.text = SafeStringFormat3(LocalString.GetString("胜败奖 %d/1"), (winTimes and winTimes > 0) and 1 or 0)
    elseif key == EnumPlayTimesKey_lua.eFenYongZhenXianExtraTimes then
        local extraTimes = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eFenYongZhenXianExtraTimes)
        self.GetRewardInfoLabel2.text = SafeStringFormat3(LocalString.GetString("卓越奖 %d/1"), (extraTimes and extraTimes > 0) and 1 or 0)
    end
end

function LuaWuYi2023FYZXGamePlayWnd:RefreshnMatchingState(isInMatching)
    self.JoinButton.gameObject:SetActive(not isInMatching)
    self.CancelButton.gameObject:SetActive(isInMatching)
    self.CancelMatchLabel.gameObject:SetActive(isInMatching)
end
--@region UIEvent

function LuaWuYi2023FYZXGamePlayWnd:OnNextArrowClick()
    self:UpdateImagePage(self.m_CurPage + 1)
end

function LuaWuYi2023FYZXGamePlayWnd:OnLastArrowClick()
    self:UpdateImagePage(self.m_CurPage - 1)
end

function LuaWuYi2023FYZXGamePlayWnd:OnJoinButtonClick()
    Gac2Gas.GlobalMatch_RequestSignUp(self.m_GameplayId)
end

function LuaWuYi2023FYZXGamePlayWnd:OnRuleButtonClick()
    g_MessageMgr:ShowMessage("WuYi2023FYZXGamePlayWnd_ReadMe")
end

function LuaWuYi2023FYZXGamePlayWnd:OnCancelButtonClick()
    Gac2Gas.GlobalMatch_RequestCancelSignUp(self.m_GameplayId)
end

function LuaWuYi2023FYZXGamePlayWnd:OnDragStart()
    self.m_DragStartX = UICamera.currentTouch.pos.x
end

function LuaWuYi2023FYZXGamePlayWnd:OnDragEnd()
    local diff = UICamera.currentTouch.pos.x - self.m_DragStartX
    if diff > 20 then
        self:UpdateImagePage(self.m_CurPage - 1)
    elseif diff < -20 then
        self:UpdateImagePage(self.m_CurPage + 1)
    end
end
--@endregion UIEvent

