local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local CPlayerInfoMgrAlignType = import "CPlayerInfoMgr+AlignType"
local Object = import "System.Object"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"

LuaCHAudienceListWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistClassMember(LuaCHAudienceListWnd, "m_AudienceGrid")
RegistClassMember(LuaCHAudienceListWnd, "m_AudienceTemplate")
RegistClassMember(LuaCHAudienceListWnd, "m_NextPageButton")
RegistClassMember(LuaCHAudienceListWnd, "m_PrevPageButton")
RegistClassMember(LuaCHAudienceListWnd, "m_CurPageLabel")

RegistClassMember(LuaCHAudienceListWnd, "m_AudienceTbl")
RegistClassMember(LuaCHAudienceListWnd, "m_AudiencePerPage")
RegistClassMember(LuaCHAudienceListWnd, "m_MaxPage")
RegistClassMember(LuaCHAudienceListWnd, "m_Page")
--@endregion RegistChildComponent end

function LuaCHAudienceListWnd:Awake()
    self.m_AudienceGrid = self.transform:Find("Anchor/AudienceGrid"):GetComponent(typeof(UIGrid))
    self.m_AudienceTemplate = self.transform:Find("Anchor/AudienceTemplate").gameObject
    self.m_NextPageButton = self.transform:Find("Anchor/Page/NextPageButton").gameObject
    self.m_PrevPageButton = self.transform:Find("Anchor/Page/PrevPageButton").gameObject
    self.m_CurPageLabel = self.transform:Find("Anchor/Page/Label"):GetComponent(typeof(UILabel))
    UIEventListener.Get(self.m_NextPageButton).onClick = DelegateFactory.VoidDelegate(function (go)	self:OnPageButtonClick(1) end)
    UIEventListener.Get(self.m_PrevPageButton).onClick = DelegateFactory.VoidDelegate(function (go)	self:OnPageButtonClick(-1) end)
end

function LuaCHAudienceListWnd:Init()
    self.m_Page = 1
    self.m_AudiencePerPage = 18
    self.m_AudienceTbl = LuaClubHouseMgr:GetAudiences()
    self.m_MaxPage = math.ceil(#self.m_AudienceTbl / self.m_AudiencePerPage)
    self.m_AudienceTemplate:SetActive(false)
    self:OnPageButtonClick(0)
end

function LuaCHAudienceListWnd:InitAudienceGrid()
    for i = 1, self.m_AudiencePerPage do
        local index = (self.m_Page - 1) * self.m_AudiencePerPage + i
        local go
        if i <= self.m_AudienceGrid.transform.childCount then
            go = self.m_AudienceGrid.transform:GetChild(i - 1).gameObject
        elseif index <= #self.m_AudienceTbl then
            go = NGUITools.AddChild(self.m_AudienceGrid.gameObject, self.m_AudienceTemplate)
        end
        if go then self:InitOneAudience(go, self.m_AudienceTbl[index]) end
    end
    self.m_AudienceGrid:Reposition()
end

function LuaCHAudienceListWnd:InitOneAudience(item, member)
    if member then
        item:SetActive(true)
        local portrait = item.transform:GetComponent(typeof(CUITexture))
        local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
        local rankSprite = item.transform:Find("Rank"):GetComponent(typeof(UISprite))
        portrait:LoadNPCPortrait(member.Portrait, false)
        LuaClubHouseMgr:SetMengDaoProfile(portrait, member.PlayerId, "CHAudienceListWnd")
        self:WrapText(nameLabel, member.PlayerName, member.IsMe)
        LuaClubHouseMgr:SetGivePresentTopThreeSprite(rankSprite, member.AccId)
        
        CommonDefs.AddOnClickListener(item, DelegateFactory.Action_GameObject(function(go) self:OnAudienceClick(go, member.AccId) end), false)
    else
        item:SetActive(false)
    end
end

function LuaCHAudienceListWnd:WrapText(label, originText, isMe)
    --赋值对label各项参数进行初始化，否则下面的计算会出问题，label类型需要clamp content， maxlines = 1
    label.text = originText
    local fit = true
    local outerText = ""
    fit, outerText = label:Wrap(originText)
    if not fit then
        outerText = CommonDefs.StringSubstring2(outerText, 0, CommonDefs.StringLength(outerText) - 1) .. "..."
        label.text = isMe and "[c][00ff60]"..outerText.."[-][/c]" or outerText
    elseif isMe then
        label.text = "[c][00ff60]"..originText.."[-][/c]"
    end
end

function LuaCHAudienceListWnd:OnAudienceListUpdate()
    self:Init()
end

--@region UIEvent
function LuaCHAudienceListWnd:OnPageButtonClick(delta)
    self.m_Page = self.m_Page + delta
    CUICommonDef.SetActive(self.m_NextPageButton, self.m_Page < self.m_MaxPage, true)
    CUICommonDef.SetActive(self.m_PrevPageButton, self.m_Page > 1, true)
    self.m_CurPageLabel.text = tostring(self.m_Page)
    self:InitAudienceGrid()
end

function LuaCHAudienceListWnd:OnAudienceClick(go, accid)
    local info = LuaClubHouseMgr:GetMember(accid)
    if info then
        local extraInfo = CreateFromClass(MakeGenericClass(Dictionary, String, Object))
        CommonDefs.DictAdd_LuaCall(extraInfo, "AccId", info.AccId)
        CommonDefs.DictAdd_LuaCall(extraInfo, "AppName", info.AppName)
        CPlayerInfoMgr.ShowPlayerPopupMenu(info.PlayerId, EnumPlayerInfoContext.ChatRoom, EChatPanel.Undefined, nil, nil, extraInfo, go.transform.position, CPlayerInfoMgrAlignType.Right)
    else
        g_MessageMgr:ShowMessage("CLUBHOUSE_AUDIENCE_NOTFOUND")
    end
end
--@endregion UIEvent

function LuaCHAudienceListWnd:OnEnable()
    g_ScriptEvent:AddListener("ClubHouse_RoomContentView_Broadcaster_Update", self, "OnAudienceListUpdate")
end

function LuaCHAudienceListWnd:OnDisable()
    g_ScriptEvent:RemoveListener("ClubHouse_RoomContentView_Broadcaster_Update", self, "OnAudienceListUpdate")
end