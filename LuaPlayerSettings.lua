local PlayerPrefs = import "UnityEngine.PlayerPrefs"
local CPlayerDataMgr=import "L10.Game.CPlayerDataMgr"
local CServerTimeMgr=import "L10.Game.CServerTimeMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local PlayerSettings = import "L10.Game.PlayerSettings"
local Shader=import "UnityEngine.Shader"
local ShaderLodParams = import "L10.Game.ShaderLodParams"

CLuaPlayerSettings = {}
CLuaPlayerSettings.m_PlayerId = 0
CLuaPlayerSettings.m_Settings = nil

local Key_FurnitureColors = "FurnitureColors"
local Key_HouseZhuangXiuAlert = "HouseZhuangXiuAlert"
local Key_HiddenInTaskTrackPanelIds = "HiddenInTaskTrackPanelIds" -- 在任务追踪栏不显示的任务id列表，以逗号隔开
local Key_WaterReflectionEnabledCAM = "WaterReflectionEnabledCAM"
local Key_HidePetCAM = "HidePetCAM"
local Key_HideNpcCAM = "HideNpcCAM"
local Key_HideSkillEffectCAM = "HideSkillEffectCAM"
local Key_HideShenbingEffectCAM = "HideShenbingEffectCAM"
local Key_HideHeadInfoCAM = "HideHeadInfoCAM"
local Key_PlayerLimitCAM = "PlayerLimitCAM"
local Key_CaptureHideNoticeWndCAM = "CaptureHideNoticeWndCAM"
local Key_HideMonsterInScreenCaptureWndCAM = "HideMonsterInScreenCaptureWndCAM"
local Key_BlockSelfPetCAM = "BlockSelfPetCAM"
local Key_BlockSelfBabyCAM = "BlockSelfBabyCAM"
local Key_TalismanAppearanceCAM = "TalismanAppearanceCAM"
local Key_HideGemFxToOtherPlayerCAM = "HideGemFxToOtherPlayerCAM"
local Key_HideSuitIdToOtherPlayerCAM = "HideSuitIdToOtherPlayerCAM"
local Key_HideWingCAM = "HideWingCAM"
local Key_ShowLineCheckCAM = "ShowLineCheckCAM"
local Key_DepthOfFieldEnableCAM = "DepthOfFieldEnableCAM"
local Key_DofRangeCAM = "DofRangeCAM"
local Key_BlinRangeCAM = "BlinRangeCAM"
local Key_BlinIntensityCAM = "BlinIntensityCAM"
local Key_ThresholdCAM = "ThresholdCAM"
local Key_ShowBrotherCAM = "ShowBrotherCAM"
local Key_ShowFriendCAM = "ShowFriendCAM"
local Key_ShowGuildMemberCAM = "ShowGuildMemberCAM"
local Key_ShowPartnerCAM = "ShowPartnerCAM"
local Key_ShowOtherCAM = "ShowOtherCAM"
local Key_ShowLogoCAM = "ShowLogoCAM"
local Key_ShowCodeCAM = "ShowCodeCAM"
local Key_LogeSettingCAM = "LogeSettingCAM"

local Key_GuanNingSignalState = "GuanNingSignalState"
local Key_GuanNingBulletChatState = "GuanNingBulletChatState"

local Key_HanJia2023Vip1Animation = "HanJia2023Vip1Animation"
local Key_HanJia2023Vip2Animation = "KHanJia2023Vip2Animation"
local Key_GLC_ZhanLing_Alert = "GLC_ZhanLing_Alert"

local SettingNameToKey = 
{
    WaterReflectionEnabled = Key_WaterReflectionEnabledCAM,
    HidePetEnabled = Key_HidePetCAM,
    HideHpcEnabled = Key_HideNpcCAM,
    HideSkillEffect = Key_HideSkillEffectCAM,
    HideShenbingEffect = Key_HideShenbingEffectCAM,
    HideHeadInfoEnabled = Key_HideHeadInfoCAM,
    VisiblePlayerLimit = Key_PlayerLimitCAM,
    CaptureHideNoticeWnd = Key_CaptureHideNoticeWndCAM,
    HideMonsterInScreenCaptureWndEnabled = Key_HideMonsterInScreenCaptureWndCAM,
    BlockSelfPet = Key_BlockSelfPetCAM,
    BlockSelfBaby = Key_BlockSelfBabyCAM,
    TalismanAppearance = Key_TalismanAppearanceCAM,
    HideGemFxToOtherPlayer = Key_HideGemFxToOtherPlayerCAM,
    HideSuitIdToOtherPlayer = Key_HideSuitIdToOtherPlayerCAM,
    HideWing = Key_HideWingCAM,
    ShowLineCheck = Key_ShowLineCheckCAM,
    DepthOfFieldEnable = Key_DepthOfFieldEnableCAM,
    DofRange = Key_DofRangeCAM,
    BlinRange = Key_BlinRangeCAM,
    BlinIntensity = Key_BlinIntensityCAM,
    Threshold = Key_ThresholdCAM,
    ShowBrother = Key_ShowBrotherCAM,
    ShowFriend = Key_ShowFriendCAM,
    ShowGuildMember = Key_ShowGuildMemberCAM,
    ShowPartner = Key_ShowPartnerCAM,
    ShowOther = Key_ShowOtherCAM,
    ShowLogo = Key_ShowLogoCAM,
    ShowCode = Key_ShowCodeCAM,
    LogeSetting = Key_LogeSettingCAM,
    GuanNingSignalState = Key_GuanNingSignalState,
    GuanNingBulletChatState = Key_GuanNingBulletChatState,
    HanJia2023Vip1Animation = Key_HanJia2023Vip1Animation,
    HanJia2023Vip2Animation = Key_HanJia2023Vip2Animation,
    GLC_ZhanLing_Alert = Key_GLC_ZhanLing_Alert,
}

function CLuaPlayerSettings.SetShaderLod()
    --移动平台不开3s效果，
    if not CommonDefs.IsInMobileDevice() then
        if Shader.globalMaximumLOD == ShaderLodParams.SHADER_LOD_HIGH_LEVEL then
            Shader.globalMaximumLOD = 800
        end
    end
end
--一些效果只有pc上支持
function CLuaPlayerSettings.SupportHighQuality()
    if not CommonDefs.IsInMobileDevice() then return true end
    return false
end

function CLuaPlayerSettings.Get(settingName, defaultValue)
    local key = SettingNameToKey[settingName]
    local valueType = type(defaultValue)
    if valueType == "number" then
        return PlayerPrefs.GetInt(key, defaultValue)
    elseif valueType == "boolean" then
        return PlayerPrefs.GetInt(key, defaultValue and 1 or 0) > 0
    elseif valueType == "string" then
        return PlayerPrefs.GetString(key, defaultValue)
    end
end

function CLuaPlayerSettings.Set(settingName, value)
    local key = SettingNameToKey[settingName]
    local valueType = type(value)
    if valueType == "number" then
        PlayerPrefs.SetInt(key, value)
    elseif valueType == "boolean" then
        PlayerPrefs.SetInt(key, value and 1 or 0)
    elseif valueType == "string" then
        PlayerPrefs.SetString(key, value)
    end
end

function CLuaPlayerSettings.GetFurnitureColors()
    local colors = {}
    local raw = PlayerPrefs.GetString(Key_FurnitureColors,"")
    for r,g,b in string.gmatch(raw, "(%d+),(%d+),(%d+);?") do 
        table.insert( colors,{r,g,b} )
    end
    return colors
end

function CLuaPlayerSettings.SetFurnitureColors(colors)
    local t = {}
    local max = math.min(#colors,8)
    for i=1,max do
        table.insert( t,table.concat( colors[i], "," ) )
    end
    local raw = table.concat( t, ";" )
    PlayerPrefs.SetString(Key_FurnitureColors,raw)
end

function CLuaPlayerSettings.InitPlayerSettings()
    local id = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
    if id~=CLuaPlayerSettings.m_PlayerId then
        CLuaPlayerSettings.m_Settings = nil
    end
    CLuaPlayerSettings.m_PlayerId = id

    if not CLuaPlayerSettings.m_Settings then
        local json = CPlayerDataMgr.Inst:LoadPlayerData("playersetting")
        if json~=nil and json~="" then
            CLuaPlayerSettings.m_Settings = luaJson.json2lua(json)
        end
    end
    if not CLuaPlayerSettings.m_Settings then
        CLuaPlayerSettings.m_Settings = {}
    end
end
function CLuaPlayerSettings.NeedHouseZhuangXiuAlert()
    CLuaPlayerSettings.InitPlayerSettings()

    local now=CServerTimeMgr.Inst:GetZone8Time()
    local val = now:ToString("yyyy-MM-dd")
    if CLuaPlayerSettings.m_Settings[Key_HouseZhuangXiuAlert]==val then
        return false
    end
    return true
end

function CLuaPlayerSettings.SetHouseZhuangXiuAlert()
    local now=CServerTimeMgr.Inst:GetZone8Time()
    local raw=now:ToString("yyyy-MM-dd")

    if not CLuaPlayerSettings.m_Settings then
        CLuaPlayerSettings.m_Settings = {}
    end
    CLuaPlayerSettings.m_Settings[Key_HouseZhuangXiuAlert] = raw

    local json = luaJson.table2json(CLuaPlayerSettings.m_Settings)
    CPlayerDataMgr.Inst:SavePlayerData("playersetting",json)
end

------------------
-- HiddenInTaskTrackPanelIds BEGIN
------------------
function CLuaPlayerSettings.GetHiddenInTaskTrackPanelIds()
    local ret = {}
    local raw = PlayerPrefs.GetString(Key_HiddenInTaskTrackPanelIds,"")
    for taskId in string.gmatch(raw, "(%d+)") do
        ret[tonumber(taskId)] = 1 --tonumber is very important!!
    end
    return ret
end

function CLuaPlayerSettings.SetHiddenInTaskTrackPanelIds(taskIds)
    local tmp1 = {}
    local tmp2 = {}
    if taskIds then
        for taskId,__ in pairs(taskIds) do
            local id = tonumber(taskId)
            if tmp1[id] == nil then
                tmp1[id] = 1
                table.insert(tmp2, id)
            end
        end
    end
    if #tmp2 > 0 then
        PlayerPrefs.SetString(Key_HiddenInTaskTrackPanelIds,table.concat(tmp2, ","))
    else
        PlayerPrefs.SetString(Key_HiddenInTaskTrackPanelIds,"")
    end
end
------------------
-- HiddenInTaskTrackPanelIds END
------------------

------------------
-- ProductRecommendation BEGIN
------------------

function CLuaPlayerSettings.GetProductRecommendationEnabled()
    local mainplayer = CClientMainPlayer.Inst
    if mainplayer and mainplayer.PlayProp then
        return mainplayer.PlayProp.RecommendIgnoreBigData == 0
    else
        return false
    end
end

function CLuaPlayerSettings.SetProductRecommendationEnabled(value)
    local mainplayer = CClientMainPlayer.Inst
    if mainplayer and mainplayer.PlayProp then
        Gac2Gas.SetRecommendIgnoreBigData(not value) -- 这里要取反，服务器端0表示开启
    end
end

------------------
-- ProductRecommendation END
------------------

function CLuaPlayerSettings.GetValueForGLCWatchSettingWnd(key)
    return  CLuaPlayerSettings.GetValueForSettingWnd(key)
end

function CLuaPlayerSettings.SetValueForGLCWatchSettingWnd(key, value)
    CLuaPlayerSettings.SetValueForSettingWnd(key, value)
end

function CLuaPlayerSettings.GetValueForSettingWnd(key)
    PlayerSettings.s_IgnoreRuntimeSetting = true
    local ret =  PlayerSettings[key]
    PlayerSettings.s_IgnoreRuntimeSetting = false
    return ret
end

function CLuaPlayerSettings.SetValueForSettingWnd(key, value)
    PlayerSettings[key] = value
end