local CMailMgr = import "L10.Game.CMailMgr"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local PlayerSettings = import "L10.Game.PlayerSettings"

LuaMailMgr = class()
LuaMailMgr.m_IsInBatchDeleteMode = false
function LuaMailMgr:SetBatchDeleteMode(mode)
	self.m_IsInBatchDeleteMode = mode or false
	g_ScriptEvent:BroadcastInLua("MailBatchDeleteModeChanged")
end

function LuaMailMgr:GetBatchDeleteMode()
	return self.m_IsInBatchDeleteMode
end
--两个reason  mainplayercreated 和 canceldirectshowjueji
function LuaMailMgr:SendMailWhenCancelDirectShowJueJi(reason)
    if PlayerSettings.DirectShowJueJiEnabled then return end
	Gac2Gas.RequestSendJueJiDisplayMail(reason)
end

function Gas2Gac.HandleMailNotDelSuccess(mailId)
	local mail = CMailMgr.Inst:GetMail(mailId)
	if mail then
		mail.IsHandled = 1
		g_ScriptEvent:BroadcastInLua("HandleMailNotDelSuccess", mailId)
	end
end


function Gas2Gac.DelMailSuccess(mailId, delReason)
	CMailMgr.Inst:DelMail(mailId)
    CMailMgr.Inst.MailUpdated = true
    EventManager.BroadcastInternalForLua(EnumEventType.MailDeleteDone, {true, mailId})
end

function Gas2Gac.DelMailFailed(mailId)
end

function Gas2Gac.BatchDelMailBegin()
end

function Gas2Gac.BatchDelMailEnd()
end
