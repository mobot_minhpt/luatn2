local DelegateFactory  = import "DelegateFactory"
local UITable = import "UITable"
local Extensions = import "Extensions"
local CButton = import "L10.UI.CButton"
local UITabBar = import "L10.UI.UITabBar"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local CCurentMoneyCtrl=import "L10.UI.CCurentMoneyCtrl"
local NGUIMath = import "NGUIMath"
local ClientAction = import "L10.UI.ClientAction"
local CBaseWnd = import "L10.UI.CBaseWnd"
local Vector3 = import "UnityEngine.Vector3"
local AlignType2 = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local Animation = import "UnityEngine.Animation"
local SpringPanel = import "SpringPanel"

LuaGuildLeagueCrossZhanLingWnd = class()
RegistChildComponent(LuaGuildLeagueCrossZhanLingWnd, "m_InfoButton", "InfoButton", GameObject)
RegistChildComponent(LuaGuildLeagueCrossZhanLingWnd, "m_CurrentLevelLabel", "CurrentLevelLabel", UILabel)
RegistChildComponent(LuaGuildLeagueCrossZhanLingWnd, "m_UpgradeExpLabel", "UpgradeExpLabel", UILabel)
RegistChildComponent(LuaGuildLeagueCrossZhanLingWnd, "m_TabBar", "TabBar", UITabBar)

RegistChildComponent(LuaGuildLeagueCrossZhanLingWnd, "m_AwardView", "AwardView", GameObject)
RegistChildComponent(LuaGuildLeagueCrossZhanLingWnd, "m_TaskView", "TaskView", GameObject)
RegistChildComponent(LuaGuildLeagueCrossZhanLingWnd, "m_BottomView", "BottomView", GameObject)
RegistChildComponent(LuaGuildLeagueCrossZhanLingWnd, "m_PurchaseView", "PurchaseView", GameObject)

--awardview
RegistChildComponent(LuaGuildLeagueCrossZhanLingWnd, "m_AwardScrollView", "AwardScrollView", UIScrollView)
RegistChildComponent(LuaGuildLeagueCrossZhanLingWnd, "m_AwardTable", "AwardTable", UITable)
RegistChildComponent(LuaGuildLeagueCrossZhanLingWnd, "m_AwardItemTemplate", "AwardItemTemplate", GameObject)
RegistChildComponent(LuaGuildLeagueCrossZhanLingWnd, "m_GetAllAwardButton", "GetAllAwardButton", GameObject)

--taskview
RegistChildComponent(LuaGuildLeagueCrossZhanLingWnd, "m_TaskScrollView", "TaskScrollView", UIScrollView)
RegistChildComponent(LuaGuildLeagueCrossZhanLingWnd, "m_TaskTable", "TaskTable", UITable)
RegistChildComponent(LuaGuildLeagueCrossZhanLingWnd, "m_TaskItemTemplate", "TaskItemTemplate", GameObject)

--bottomview
RegistChildComponent(LuaGuildLeagueCrossZhanLingWnd, "m_PurchaseButton", "PurchaseButton", GameObject)
RegistChildComponent(LuaGuildLeagueCrossZhanLingWnd, "m_RankButton", "RankButton", GameObject)
RegistChildComponent(LuaGuildLeagueCrossZhanLingWnd, "m_RankLabel", "RankLabel", UILabel)
RegistChildComponent(LuaGuildLeagueCrossZhanLingWnd, "m_AwardPoolLabel", "AwardPoolLabel", UILabel)
RegistChildComponent(LuaGuildLeagueCrossZhanLingWnd, "m_AwardPoolButton", "AwardPoolButton", GameObject)

--purchaseview
RegistChildComponent(LuaGuildLeagueCrossZhanLingWnd, "m_PurchaseNumButton", "PurchaseNumButton", QnAddSubAndInputButton)
RegistChildComponent(LuaGuildLeagueCrossZhanLingWnd, "m_FutureLevelLabel", "FutureLevelLabel", UILabel)
RegistChildComponent(LuaGuildLeagueCrossZhanLingWnd, "m_MoneyCtrl", "QnCostAndOwnMoney", CCurentMoneyCtrl)
RegistChildComponent(LuaGuildLeagueCrossZhanLingWnd, "m_CancelPurchaseButton", "CancelPurchaseButton", GameObject)
RegistChildComponent(LuaGuildLeagueCrossZhanLingWnd, "m_DoPurchaseButton", "DoPurchaseButton", CButton)

RegistClassMember(LuaGuildLeagueCrossZhanLingWnd, "m_Ani") --动画组件

RegistClassMember(LuaGuildLeagueCrossZhanLingWnd, "m_FirstInited")
RegistClassMember(LuaGuildLeagueCrossZhanLingWnd, "m_ExpAddValuePerJade")
RegistClassMember(LuaGuildLeagueCrossZhanLingWnd, "m_ExpAddProgressTbl")
RegistClassMember(LuaGuildLeagueCrossZhanLingWnd, "m_AwardItemChildWidth")


function LuaGuildLeagueCrossZhanLingWnd:Awake()
    self.m_Ani = self.gameObject:GetComponent(typeof(Animation))

    self.m_AwardItemTemplate:SetActive(false)
    self.m_TaskItemTemplate:SetActive(false)

    self.m_TabBar.OnTabChange = CommonDefs.CombineListner_Action_GameObject_int(self.m_TabBar.OnTabChange, DelegateFactory.Action_GameObject_int(function(go, index)
        self:OnTabChange(go, index)
    end), true)

    self.m_PurchaseNumButton:SetMinMax(0,0,1)
    self.m_PurchaseNumButton.onValueChanged = DelegateFactory.Action_uint(function (value) 
        self:OnPurchaseNumChanged(value)
    end)

    UIEventListener.Get(self.m_InfoButton).onClick = DelegateFactory.VoidDelegate(function() self:OnInfoButtonClick() end)
    UIEventListener.Get(self.m_GetAllAwardButton).onClick = DelegateFactory.VoidDelegate(function() self:OnGetAllAwardButtonClick() end)
    UIEventListener.Get(self.m_PurchaseButton).onClick = DelegateFactory.VoidDelegate(function() self:OnPurchaseButtonClick() end)
    UIEventListener.Get(self.m_RankButton).onClick = DelegateFactory.VoidDelegate(function() self:OnRankButtonClick() end)
    UIEventListener.Get(self.m_AwardPoolButton).onClick = DelegateFactory.VoidDelegate(function() self:OnAwardPoolButtonClick() end)
    UIEventListener.Get(self.m_CancelPurchaseButton).onClick = DelegateFactory.VoidDelegate(function() self:OnCancelPurchaseButtonClick() end)
    UIEventListener.Get(self.m_DoPurchaseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function() self:OnDoPurchaseButtonClick() end)

end

function LuaGuildLeagueCrossZhanLingWnd:Init()
    if not self.m_FirstInited then
        self.m_FirstInited = true
        self.m_TabBar:ChangeTab(self.m_TabBar.SelectedIndex>=0 and self.m_TabBar.SelectedIndex or 0, false)
        self.m_BottomView:SetActive(true)
        self.m_PurchaseView:SetActive(false)
    end
    self:InitCurrentLevelInfo()
    self:InitBottomView()
end

function LuaGuildLeagueCrossZhanLingWnd:InitCurrentLevelInfo()
    self.m_CurrentLevelLabel.text = tostring(LuaGuildLeagueCrossMgr:GetCurZhanLingLevel())
    self.m_UpgradeExpLabel.text = tostring(LuaGuildLeagueCrossMgr:GetZhanLingUpgradeNeedExp())
end

function LuaGuildLeagueCrossZhanLingWnd:OnTabChange(go, index)
    if index == 0 then
        self.m_AwardView:SetActive(true)
        self.m_TaskView:SetActive(false)
        self.m_Ani:Play("guildleaguecrosszhanlingwnd_awardview")
        self:InitAwardView()
    else
        self.m_AwardView:SetActive(false)
        self:StopPlayBuyLevelSuccess()
        self.m_TaskView:SetActive(true)
        self.m_Ani:Play("guildleaguecrosszhanlingwnd_taskview")
        self:InitTaskView()
    end
end

function LuaGuildLeagueCrossZhanLingWnd:OnEnable()
    g_ScriptEvent:AddListener("UpdatePassDataWithId", self, "OnUpdatePassDataWithId")
    g_ScriptEvent:AddListener("CommonPassortBuyLevelSuccess", self, "OnCommonPassortBuyLevelSuccess")
end

function LuaGuildLeagueCrossZhanLingWnd:OnDisable()
    g_ScriptEvent:RemoveListener("UpdatePassDataWithId", self, "OnUpdatePassDataWithId")
    g_ScriptEvent:RemoveListener("CommonPassortBuyLevelSuccess", self, "OnCommonPassortBuyLevelSuccess")
    self:StopPlayBuyLevelSuccess()
end
--战令信息更新
function LuaGuildLeagueCrossZhanLingWnd:OnUpdatePassDataWithId(zhanlingId)
    self:InitCurrentLevelInfo()
    if self.m_AwardView.activeSelf then
        self:InitAwardView()
    elseif self.m_TaskView.activeSelf then
        self:InitTaskView()
    end
end
--购买等级成功
function LuaGuildLeagueCrossZhanLingWnd:OnCommonPassortBuyLevelSuccess(zhanlingId, oldRewardLevel, newRewardLevel)
    self:InitCurrentLevelInfo()
    self.m_BottomView:SetActive(true)
    self.m_PurchaseView:SetActive(false)
    self.m_Ani:Play("guildleaguecrosszhanlingwnd_bottomview")
    self:StopBreathingBlinkingAnimation()
    self:InitBottomView()
    if self.m_AwardView.activeSelf then
        self:InitAwardView()
        --播放从oldrewardelevel 到 newrewardlevel之间依次划过的特效
        self:PlayBuyLevelSuccess(oldRewardLevel, newRewardLevel)

    end
    --主动请求刷新奖池信息
    LuaGuildLeagueCrossMgr:QueryGuildLeagueCrossZhanLingExtInfo()
end

function LuaGuildLeagueCrossZhanLingWnd:PlayBuyLevelSuccess(prevLevel, curLevel)
    local awardInfo = LuaGuildLeagueCrossMgr:GetZhanLingAwardInfo()
    for i=1,#awardInfo-1 do
        local go = self.m_AwardTable.transform:GetChild(i-1).gameObject
        if awardInfo[i].level>prevLevel and awardInfo[i].level<=curLevel then
            go.transform:Find("Rotate/BoomFx").gameObject:SetActive(true)
        end
    end
end

function LuaGuildLeagueCrossZhanLingWnd:StopPlayBuyLevelSuccess()
    local n = self.m_AwardTable.transform.childCount
    for i=0,n-1 do
        local go = self.m_AwardTable.transform:GetChild(i).gameObject
        local boomGo = go.transform:Find("Rotate/BoomFx").gameObject
        if boomGo.activeInHierarchy then
            boomGo:SetActive(false)
        end
    end
end

function LuaGuildLeagueCrossZhanLingWnd:InitAwardView()
    --存在奖励时显示一键领取
    self.m_GetAllAwardButton:SetActive(LuaGuildLeagueCrossMgr:HasZhanLingReward())

    local curLevel = LuaGuildLeagueCrossMgr:GetCurZhanLingLevel()
    local curRewardedLevel = LuaGuildLeagueCrossMgr:GetCurZhanLingRewardLevel()
    local n = self.m_AwardTable.transform.childCount
    for i=0,n-1 do
        self.m_AwardTable.transform:GetChild(i).gameObject:SetActive(false)
    end

    local awardInfo = LuaGuildLeagueCrossMgr:GetZhanLingAwardInfo()
    local curLevelIndex = 0
    for i=1,#awardInfo do
        local go = nil
        if i<n then
			go = self.m_AwardTable.transform:GetChild(i-1).gameObject
		else
			go = CUICommonDef.AddChild(self.m_AwardTable.gameObject, self.m_AwardItemTemplate)
		end
        go:SetActive(true)
        local info = awardInfo[i]
        local levelLabel = go.transform:Find("LevelLabel"):GetComponent(typeof(UILabel))
        local rotateRoot = go.transform:Find("Rotate")
        local currentGo = go.transform:Find("Rotate/Current").gameObject
        local futureGo = go.transform:Find("Rotate/Future").gameObject
        local itemRoot = go.transform:Find("Rotate/ItemCell")
        local iconTexture = go.transform:Find("Rotate/ItemCell/IconTexture"):GetComponent(typeof(CUITexture))
        local disableSprite =go.transform:Find("Rotate/ItemCell/DisableSprite").gameObject
        local lockSprite = go.transform:Find("Rotate/ItemCell/LockSprite").gameObject
        local checkmark =go.transform:Find("Rotate/ItemCell/Checkmark").gameObject
        local rewardGo = go.transform:Find("Rotate/ItemCell/Reward").gameObject
        local lineShiXian =go.transform:Find("Line/ShiXian").gameObject
        local lineShiXianHightlight =go.transform:Find("Line/ShiXian_Highlight").gameObject
        local boomGo = go.transform:Find("Rotate/BoomFx").gameObject

        boomGo:SetActive(false)
        lineShiXian:SetActive(false)
        lineShiXianHightlight:SetActive(false)

        levelLabel.text = tostring(info.level)
        local itemDesignData = Item_Item.GetData(info.itemId)
        iconTexture:LoadMaterial(itemDesignData.Icon)
        rotateRoot.gameObject:SetActive(true)
        currentGo:SetActive(info.level<=curLevel and (awardInfo[i+1]==nil or awardInfo[i+1].level>curLevel))
        futureGo:SetActive(info.level>curLevel)
        Extensions.SetLocalRotationZ(rotateRoot, i%2==0 and 180 or 0)
        Extensions.SetLocalRotationZ(itemRoot, i%2==0 and 180 or 0)
        local rewardReceived = LuaGuildLeagueCrossMgr:IsZhanLingRewardReceived(info.level) 
        disableSprite:SetActive(rewardReceived or info.level>curLevel)
        lockSprite:SetActive(info.level>curLevel)
        checkmark:SetActive(rewardReceived)
        rewardGo:SetActive(info.level<=curLevel and not rewardReceived)
    
        if curLevel>=info.level then
            curLevelIndex = i
        end

        UIEventListener.Get(itemRoot.gameObject).onClick = DelegateFactory.VoidDelegate(function() self:OnAwardItemClick(info.level,info.itemId) end)
    end
    self.m_AwardTable:Reposition()
    self.m_AwardScrollView:ResetPosition()
    self.m_AwardItemChildWidth = NGUIMath.CalculateRelativeWidgetBounds(self.m_AwardTable.transform:GetChild(0)).size.x
    self:MoveAwardScrollViewByLevel(curLevel, false)
    --table布局完成后显示连接线
    for i=2,#awardInfo do
        local go = self.m_AwardTable.transform:GetChild(i-1).gameObject
        local lineShiXian =go.transform:Find("Line/ShiXian").gameObject
        local lineShiXianHightlight =go.transform:Find("Line/ShiXian_Highlight").gameObject
        local levelReached = awardInfo[i] and awardInfo[i].level<=curLevel
        if levelReached then
            lineShiXianHightlight:SetActive(true)
        else
            lineShiXian:SetActive(true)
        end
    end
end

function LuaGuildLeagueCrossZhanLingWnd:MoveAwardScrollViewByLevel(level, animated)
    local awardInfo = LuaGuildLeagueCrossMgr:GetZhanLingAwardInfo()
    local childIndex = 0
    for i=2,#awardInfo do
        if awardInfo[i].level<=level then
            childIndex = i-1
        else
            break
        end
    end

     --调整显示位置,超过4个的情况下，让当前等级的节点位于左侧开始第3.5个item的位置
     local leftmostChild = self.m_AwardTable.transform:GetChild(childIndex>3 and childIndex-3 or 0)  
    --仿照UICenterOnChild
    -- Figure out the difference between the chosen child and the panel's center in local coordinates
    local cp = self.m_AwardScrollView.transform:InverseTransformPoint(leftmostChild.position)
    local corners = self.m_AwardScrollView.panel.worldCorners
    local panelCenter = CommonDefs.op_Multiply_Vector3_Single(CommonDefs.op_Addition_Vector3_Vector3(corners[2], corners[0]),0.5)
    local cc =  self.m_AwardScrollView.transform:InverseTransformPoint(panelCenter)
    local offsetX = self.m_AwardScrollView.transform.localPosition.x - (cp.x-cc.x) - self.m_AwardScrollView.panel.baseClipRegion.z * 0.5
    if childIndex<=3 then
        offsetX = offsetX + self.m_AwardItemChildWidth * 0.5 + self.m_AwardScrollView.panel.clipSoftness.x
    end
    self:MoveAwardScrollView(offsetX, animated)
end

function LuaGuildLeagueCrossZhanLingWnd:MoveAwardScrollView(absolutePosX, animated)
    if animated then --以动画形式滚动ScrollView到目标位置
        local pos = self.m_AwardScrollView.transform.localPosition
        pos.x = math.floor(absolutePosX)
        SpringPanel.Begin(self.m_AwardScrollView.gameObject, pos, 13)
    else --立即滚动ScrollView到目标位置
        local pos = self.m_AwardScrollView.transform.localPosition
        self.m_AwardScrollView:MoveRelative(Vector3(absolutePosX - pos.x,0,0))
        self.m_AwardScrollView:RestrictWithinBounds(true)
    end
end


function LuaGuildLeagueCrossZhanLingWnd:OnAwardItemClick(level, itemId)
    local curLevel = LuaGuildLeagueCrossMgr:GetCurZhanLingLevel()
    local rewardReceived = LuaGuildLeagueCrossMgr:IsZhanLingRewardReceived(level) 
    if curLevel>=level and not rewardReceived then --领奖
        LuaGuildLeagueCrossMgr:GetOneZhanLingReward(level)
    else --显示tip
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType2.Default, 0, 0, 0, 0)
    end
end

function LuaGuildLeagueCrossZhanLingWnd:InitTaskView()

    local n = self.m_TaskTable.transform.childCount
    for i=0,n-1 do
        self.m_TaskTable.transform:GetChild(i).gameObject:SetActive(false)
    end

    local taskInfo = LuaGuildLeagueCrossMgr:GetZhanLingTaskInfo()
    for i=1,#taskInfo do
        local go = nil
        if i<n then
			go = self.m_TaskTable.transform:GetChild(i-1).gameObject
		else
			go = CUICommonDef.AddChild(self.m_TaskTable.gameObject, self.m_TaskItemTemplate)
		end
        go:SetActive(true)
        local info = taskInfo[i]
        local taskLabel = go.transform:Find("TaskLabel"):GetComponent(typeof(UILabel))
        local processLabel = go.transform:Find("ProcessLabel"):GetComponent(typeof(UILabel))
        local expLabel = go.transform:Find("ExpLabel"):GetComponent(typeof(UILabel))
        local finishTimesLabel = go.transform:Find("FinishTimesLabel"):GetComponent(typeof(UILabel))
        local refreshLabel = go.transform:Find("RefreshLabel"):GetComponent(typeof(UILabel))
        local goToButton = go.transform:Find("GoToButton"):GetComponent(typeof(CButton))
        local finishedGo = go.transform:Find("Finished").gameObject
       
        local finished = info.finishTimes>=info.totalTimes
        taskLabel.text = info.desc
        processLabel.text = SafeStringFormat3("%d/%d", finished and info.needProgress or info.progress,info.needProgress)
        expLabel.text = tostring(info.reward)
        finishTimesLabel.text = SafeStringFormat3("%d/%d", info.finishTimes,info.totalTimes)
        refreshLabel.text = info.refeshText
        goToButton.Enabled = info.finishTimes<info.totalTimes
        goToButton.Text = finished and LocalString.GetString("已完成") or LocalString.GetString("去完成")
        finishedGo:SetActive(finished)
       
        UIEventListener.Get(goToButton.gameObject).onClick = DelegateFactory.VoidDelegate(function() self:OnGoToTaskButtonClick(info.action) end)
    end
    self.m_TaskTable:Reposition()
    self.m_TaskScrollView:ResetPosition()
end

function LuaGuildLeagueCrossZhanLingWnd:InitBottomView()
    local extInfo = LuaGuildLeagueCrossMgr.m_ZhanLingExtInfo
    self.m_RankLabel.text = extInfo.myRank>0 and SafeStringFormat3(LocalString.GetString("当前 第%d名"), extInfo.myRank) or LocalString.GetString("当前 未上榜")
    self.m_AwardPoolLabel.text = tostring(extInfo.lingyuPoolAmount)
end

function LuaGuildLeagueCrossZhanLingWnd:InitPurchaseView()
    self.m_ExpAddValuePerJade, self.m_ExpAddProgressTbl = LuaGuildLeagueCrossMgr:GetJadeCostAndExpProgressInfoForBuyZhanLingLevel()
    
    local curLevel = LuaGuildLeagueCrossMgr:GetCurZhanLingLevel()
    local purchaseMaxLevel = math.max(0, LuaGuildLeagueCrossMgr:GetZhanLingMaxLevel() - curLevel)
    self.m_PurchaseNumButton:SetMinMax(1, purchaseMaxLevel , 1)
    self.m_PurchaseNumButton:SetValue(1, true)
end

function LuaGuildLeagueCrossZhanLingWnd:OnPurchaseNumChanged(value)
    local curLevel = LuaGuildLeagueCrossMgr:GetCurZhanLingLevel()
    self.m_FutureLevelLabel.text = tostring(curLevel + value)
    local cost = LuaGuildLeagueCrossMgr:GetZhanLingBuyLevelJadeCost(curLevel+value, curLevel, self.m_ExpAddValuePerJade, self.m_ExpAddProgressTbl)
    self.m_MoneyCtrl:SetCost(cost)
    self:StartBreathingBlinkingAnimation(curLevel + value)
    self:MoveAwardScrollViewByLevel(curLevel + value, true)
    self.m_DoPurchaseButton.Enabled = cost>0
end

function LuaGuildLeagueCrossZhanLingWnd:StartBreathingBlinkingAnimation(futureLevel)
    local awardInfo = LuaGuildLeagueCrossMgr:GetZhanLingAwardInfo()
    local curLevel = LuaGuildLeagueCrossMgr:GetCurZhanLingLevel()
    for i=1,#awardInfo do
        local go = self.m_AwardTable.transform:GetChild(i-1).gameObject
        local futureW = go.transform:Find("Rotate/Future"):GetComponent(typeof(UIWidget))
        if awardInfo[i].level>curLevel and awardInfo[i].level<=futureLevel then
            LuaTweenUtils.SetLoops(LuaTweenUtils.TweenAlpha(futureW, 1, 0, 0.6), -1, true)
        else
            LuaTweenUtils.DOKill(futureW.transform, false)
            futureW.alpha = 1
        end
    end
end

function LuaGuildLeagueCrossZhanLingWnd:StopBreathingBlinkingAnimation()
    for i=1,self.m_AwardTable.transform.childCount-1 do
        local go = self.m_AwardTable.transform:GetChild(i-1).gameObject
        local futureTrans = go.transform:Find("Rotate/Future")
        LuaTweenUtils.DOKill(futureTrans, false)
        futureTrans:GetComponent(typeof(UIWidget)).alpha = 1
    end
end

function LuaGuildLeagueCrossZhanLingWnd:OnGoToTaskButtonClick(actionStr)
    ClientAction.DoAction(actionStr)
    self:Close()
end

function LuaGuildLeagueCrossZhanLingWnd:OnInfoButtonClick()
    g_MessageMgr:ShowMessage("GLC_ZHANLING_MAIN_DESC")
end

function LuaGuildLeagueCrossZhanLingWnd:OnGetAllAwardButtonClick()
    LuaGuildLeagueCrossMgr:GetAllZhanLingRewards()
end

function LuaGuildLeagueCrossZhanLingWnd:OnPurchaseButtonClick()
    self.m_BottomView:SetActive(false)
    self.m_PurchaseView:SetActive(true)
    self.m_Ani:Play("guildleaguecrosszhanlingwnd_purchaseview")
    self:InitPurchaseView()

end

function LuaGuildLeagueCrossZhanLingWnd:OnRankButtonClick()
    LuaGuildLeagueCrossMgr:ShowZhanLingRankWnd()
end

function LuaGuildLeagueCrossZhanLingWnd:OnAwardPoolButtonClick()
    LuaGuildLeagueCrossMgr:QueryGuildLeagueCrossRewardPoolInfo(EnumZhanLingAwardPoolWndOpenType.eZhanLing)
end

function LuaGuildLeagueCrossZhanLingWnd:OnCancelPurchaseButtonClick()
    self.m_BottomView:SetActive(true)
    self.m_PurchaseView:SetActive(false)
    self.m_Ani:Play("guildleaguecrosszhanlingwnd_bottomview")
    self:StopBreathingBlinkingAnimation()
    self:InitBottomView()
end

function LuaGuildLeagueCrossZhanLingWnd:OnDoPurchaseButtonClick()
    local curLevel = LuaGuildLeagueCrossMgr:GetCurZhanLingLevel()
    local buyLevel = self.m_PurchaseNumButton:GetValue()
    local cost = LuaGuildLeagueCrossMgr:GetZhanLingBuyLevelJadeCost(curLevel+buyLevel, curLevel, self.m_ExpAddValuePerJade, self.m_ExpAddProgressTbl)
    LuaGuildLeagueCrossMgr:BuyZhanLingLevel(curLevel + buyLevel, cost)
    --测试代码，注销
    --self:OnCommonPassortBuyLevelSuccess(1, curLevel, curLevel + buyLevel)
end


function LuaGuildLeagueCrossZhanLingWnd:Close()
    self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end
