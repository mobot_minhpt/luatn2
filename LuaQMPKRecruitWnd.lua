local Profession = import "L10.Game.Profession"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"
local EnumClass = import "L10.Game.EnumClass"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CChatHelper = import "L10.UI.CChatHelper"
local CQMPKTitleTemplate=import "L10.UI.CQMPKTitleTemplate"
local QnInput=import "L10.UI.QnInput"


CLuaQMPKRecruitWnd = class()
RegistClassMember(CLuaQMPKRecruitWnd,"m_CloseBtn")
RegistClassMember(CLuaQMPKRecruitWnd,"m_SendBtn")
RegistClassMember(CLuaQMPKRecruitWnd,"m_SaveBtn")
RegistClassMember(CLuaQMPKRecruitWnd,"m_ClassTitle")
RegistClassMember(CLuaQMPKRecruitWnd,"m_CanCommandTitle")
RegistClassMember(CLuaQMPKRecruitWnd,"m_SloganInput")
RegistClassMember(CLuaQMPKRecruitWnd,"m_MyRecruitRoot")
RegistClassMember(CLuaQMPKRecruitWnd,"m_OtherRecruitRoot")
RegistClassMember(CLuaQMPKRecruitWnd,"m_ChatBtn")
RegistClassMember(CLuaQMPKRecruitWnd,"m_TitleLabel")
RegistClassMember(CLuaQMPKRecruitWnd,"m_CanCommandStrList")
RegistClassMember(CLuaQMPKRecruitWnd,"m_CanCommandPopupList")
RegistClassMember(CLuaQMPKRecruitWnd,"m_ClassPopupList")
RegistClassMember(CLuaQMPKRecruitWnd,"m_ClassStrList")
RegistClassMember(CLuaQMPKRecruitWnd,"m_MaxClass")
RegistClassMember(CLuaQMPKRecruitWnd,"m_ClassIndex")
RegistClassMember(CLuaQMPKRecruitWnd,"m_SelectedClassIndex")
RegistClassMember(CLuaQMPKRecruitWnd,"m_CommandIndex")

function CLuaQMPKRecruitWnd:Awake()
    self.m_CloseBtn = self.transform:Find("Wnd_Bg_Secondary_1/CloseButton").gameObject
    self.m_SendBtn = self.transform:Find("Anchor/MyRecruitBottom/SendBtn").gameObject
    self.m_SaveBtn = self.transform:Find("Anchor/MyRecruitBottom/SaveBtn").gameObject
self.m_ClassTitle = {}
local parent=FindChild(self.transform,"Class")
for i=1,4 do
    table.insert(self.m_ClassTitle,parent:GetChild(i-1):GetComponent(typeof(CQMPKTitleTemplate)))
end

    self.m_CanCommandTitle = self.transform:Find("Anchor/Director/TitleTemplate"):GetComponent(typeof(CQMPKTitleTemplate))
    self.m_SloganInput = self.transform:Find("Anchor/Etra/QnInputField"):GetComponent(typeof(QnInput))
    self.m_MyRecruitRoot = self.transform:Find("Anchor/MyRecruitBottom").gameObject
    self.m_OtherRecruitRoot = self.transform:Find("Anchor/OtherRecruitBottom").gameObject
    self.m_ChatBtn = self.transform:Find("Anchor/OtherRecruitBottom/SendBtn").gameObject
    self.m_TitleLabel = self.transform:Find("Wnd_Bg_Secondary_1/TitleLabel"):GetComponent(typeof(UILabel))

self.m_CanCommandStrList = {LocalString.GetString("不限"), LocalString.GetString("是")}
self.m_CanCommandPopupList = {}
self.m_ClassPopupList = {}
self.m_ClassStrList = {}
--self.m_MaxClass = ?
self.m_ClassIndex = {}
self.m_SelectedClassIndex = 0
self.m_CommandIndex = 0

end

-- Auto Generated!!
function CLuaQMPKRecruitWnd:OnEnable( )
    -- UIEventListener.Get(self.m_CloseBtn).onClick = MakeDelegateFromCSFunction(self.OnClickCloseButton, VoidDelegate, self)
    UIEventListener.Get(self.m_SendBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnSendBtnClicked(go) end)
    UIEventListener.Get(self.m_SaveBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnSaveBtnClicked(go) end)

    for i,v in ipairs(self.m_ClassTitle) do
        UIEventListener.Get(v.gameObject).onClick =DelegateFactory.VoidDelegate(function(go) self:OnClassClicked(go) end)
    end


    UIEventListener.Get(self.m_CanCommandTitle.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) self:OnCommandClicked(go) end)
    UIEventListener.Get(self.m_ChatBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnChatBtnClicked(go) end)
end
function CLuaQMPKRecruitWnd:Init( )
    -- CommonDefs.ListClear(self.m_ClassPopupList)
    -- CommonDefs.ListClear(self.m_ClassStrList)

    self.m_ClassPopupList={}
    self.m_ClassStrList={LocalString.GetString("无")}
    local choice1 = PopupMenuItemData(
        self.m_ClassStrList[1], 
        DelegateFactory.Action_int(function(index) self:OnClassSelected(index) end), 
        false, 
        nil, 
        EnumPopupMenuItemStyle.Light)
    table.insert( self.m_ClassPopupList,choice1 )


    local classes={
        EnumClass.SheShou,
        EnumClass.JiaShi,
        EnumClass.DaoKe,
        EnumClass.XiaKe,
        EnumClass.FangShi,
        EnumClass.YiShi,
        EnumClass.MeiZhe,
        EnumClass.YiRen,
        EnumClass.YanShi,
        EnumClass.HuaHun,
        EnumClass.YingLing,
        EnumClass.DieKe,
    }
    for i,v in ipairs(classes) do
        local name = Profession.GetFullName(v)
        local data = PopupMenuItemData(name, DelegateFactory.Action_int(function(index) self:OnClassSelected(index) end), false, nil, EnumPopupMenuItemStyle.Light)
        table.insert( self.m_ClassStrList,name )
        table.insert( self.m_ClassPopupList,data )
    end

    self.m_ClassIndex={}
    local index = 0
    for i = 0, 14 do
        if (bit.band(CQuanMinPKMgr.Inst.m_RecruitClass, (bit.lshift(1, i)))) > 0 then
            table.insert( self.m_ClassIndex,i+1 )
            self.m_ClassTitle[index+1]:Init(self.m_ClassStrList[i + 1+1])
            index = index + 1
        end
    end

    do
        local cnt = #self.m_ClassTitle
        while index < cnt do
            table.insert( self.m_ClassIndex,0 )
            self.m_ClassTitle[index+1]:Init(self.m_ClassStrList[0+1])
            index = index + 1
        end
    end

    self.m_CanCommandPopupList={}
    do
        local i = 0 local cnt = #self.m_CanCommandStrList
        while i < cnt do
            local data = PopupMenuItemData(self.m_CanCommandStrList[i+1], DelegateFactory.Action_int(function(index) self:OnCommandSelected(index) end), false, nil, EnumPopupMenuItemStyle.Light)
            table.insert( self.m_CanCommandPopupList,data )
            i = i + 1
        end
    end
    self.m_CommandIndex = CQuanMinPKMgr.Inst.m_RecruitCommandIndex > 0 and 1 or 0
    self.m_CanCommandTitle:Init(self.m_CanCommandStrList[self.m_CommandIndex+1])

    self.m_SloganInput.Text = CQuanMinPKMgr.Inst.m_CurrentZhanduiSlogan
    self.m_TitleLabel.text = CQuanMinPKMgr.Inst.m_CurrentZhanduiName .. LocalString.GetString("的招募信息")

    if CQuanMinPKMgr.Inst.m_CurrentZhanduiId == CQuanMinPKMgr.Inst.m_MySelfZhanduiId then
        self.m_MyRecruitRoot:SetActive(true)
        self.m_OtherRecruitRoot:SetActive(false)
    else
        self.m_MyRecruitRoot:SetActive(false)
        self.m_OtherRecruitRoot:SetActive(true)
        self.m_CanCommandTitle:SetArrowVisible(false)
        do
            local i = 0 local len = #self.m_ClassTitle
            while i < len do
                self.m_ClassTitle[i+1]:SetArrowVisible(false)
                i = i + 1
            end
        end
        if CommonDefs.StringLength(CQuanMinPKMgr.Inst.m_CurrentZhanduiSlogan) == 0 then
            self.m_SloganInput.Text = " "
        end
        self.m_SloganInput.gameObject:SetActive(false)
    end
end
function CLuaQMPKRecruitWnd:OnClassSelected( index) 
    self.m_ClassTitle[self.m_SelectedClassIndex+1]:Init(self.m_ClassStrList[index+1])
    self.m_ClassIndex[self.m_SelectedClassIndex+1] = index
end
function CLuaQMPKRecruitWnd:OnCommandSelected( index) 
    self.m_CanCommandTitle:Init(self.m_CanCommandStrList[index+1])
    self.m_CommandIndex = index
end
function CLuaQMPKRecruitWnd:GetGoodAtClass( )
    local result = 0
    do
        local i = 0 local len = #self.m_ClassIndex
        while i < len do
            local continue
            repeat
                if self.m_ClassIndex[i+1] <= 0 then
                    continue = true
                    break
                end
                result = bit.bor(result, (bit.lshift(1, self.m_ClassIndex[i+1] - 1)))
                continue = true
            until 1
            if not continue then
                break
            end
            i = i + 1
        end
    end
    return result
end

function CLuaQMPKRecruitWnd:OnSendBtnClicked( go) 
    if CUICommonDef.GetStrByteLength(self.m_SloganInput.Text) > 40 then
        g_MessageMgr:ShowMessage("QMPK_Slogan_Too_Long")
        return
    end
    if not CWordFilterMgr.Inst:CheckName(self.m_SloganInput.Text) then
        g_MessageMgr:ShowMessage("QMPK_Name_Slogan_Violation")
        return
    end
    Gac2Gas.BroadcastQmpkZhanDuiRecruitInfo(self:GetGoodAtClass(), self.m_CommandIndex, self.m_SloganInput.Text)
end
function CLuaQMPKRecruitWnd:OnSaveBtnClicked( go) 
    if CUICommonDef.GetStrByteLength(self.m_SloganInput.Text) > 40 then
        g_MessageMgr:ShowMessage("QMPK_Slogan_Too_Long")
        return
    end
    if not CWordFilterMgr.Inst:CheckName(self.m_SloganInput.Text) then
        g_MessageMgr:ShowMessage("QMPK_Name_Slogan_Violation")
        return
    end
    Gac2Gas.SetQmpkZhanDuiRecruitInfo(self:GetGoodAtClass(), self.m_CommandIndex, self.m_SloganInput.Text)
end

function CLuaQMPKRecruitWnd:OnChatBtnClicked( go) 
    if CQuanMinPKMgr.Inst.m_MemberInfoList.Count <= 0 then
        return
    end
    CChatHelper.ShowFriendWnd(math.floor(CQuanMinPKMgr.Inst.m_MemberInfoList[0].m_Id), CQuanMinPKMgr.Inst.m_MemberInfoList[0].m_Name)
end
function CLuaQMPKRecruitWnd:OnClassClicked( go) 
    if CQuanMinPKMgr.Inst.m_MySelfZhanduiId ~= CQuanMinPKMgr.Inst.m_CurrentZhanduiId then
        return
    end

    do
        local i = 0 local len = #self.m_ClassTitle
        while i < len do
            if go == self.m_ClassTitle[i+1].gameObject then
                self.m_SelectedClassIndex = i
                self.m_ClassTitle[i+1]:Expand(true)
                local array=Table2Array(self.m_ClassPopupList,MakeArrayClass(PopupMenuItemData))
                CPopupMenuInfoMgr.ShowPopupMenu(array, 
                    go.transform, 
                    CPopupMenuInfoMgr.AlignType.Bottom, 
                    #self.m_ClassPopupList >= 8 and 2 or 1, 
                    DelegateFactory.Action(function () self.m_ClassTitle[i+1]:Expand(false) end), 
                    600, 
                    true, 
                    296)
                break
            end
            i = i + 1
        end
    end
end
function CLuaQMPKRecruitWnd:OnCommandClicked( go) 
    if CQuanMinPKMgr.Inst.m_MySelfZhanduiId ~= CQuanMinPKMgr.Inst.m_CurrentZhanduiId then
        return
    end

    self.m_CanCommandTitle:Expand(true)

    local array=Table2Array(self.m_CanCommandPopupList,MakeArrayClass(PopupMenuItemData))
    CPopupMenuInfoMgr.ShowPopupMenu(array, 
        go.transform, 
        CPopupMenuInfoMgr.AlignType.Bottom, 
        1, 
        DelegateFactory.Action(function () self.m_CanCommandTitle:Expand(false) end), 
        600, 
        true, 
        296)
end

return CLuaQMPKRecruitWnd
