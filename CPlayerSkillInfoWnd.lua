-- Auto Generated!!
local CommonDefs = import "L10.Game.CommonDefs"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local CPlayerSkillInfoWnd = import "L10.UI.CPlayerSkillInfoWnd"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"
local CSkillItemCell = import "L10.UI.CSkillItemCell"
local CSkillMgr = import "L10.Game.CSkillMgr"
local CTianFuSkillItemCell = import "L10.UI.CTianFuSkillItemCell"
local DelegateFactory = import "DelegateFactory"
local EnumClass = import "L10.Game.EnumClass"
local Extensions = import "Extensions"
local GameObject = import "UnityEngine.GameObject"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local NGUITools = import "NGUITools"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
local SkillKind = import "L10.Game.SkillKind"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local UInt32 = import "System.UInt32"
local UISprite = import "UISprite"
local Vector3 = import "UnityEngine.Vector3"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local SkillCategory = import "L10.Game.SkillCategory"
local EnumGender = import "L10.Game.EnumGender"
local __Skill_AllSkills_Template = import "L10.Game.__Skill_AllSkills_Template"

CPlayerSkillInfoWnd.m_Start_CS2LuaHook = function (this) 

    this.skillTemplate:SetActive(false)
    this.scaleTemplate:SetActive(false)
    this.arrowTemplate:SetActive(false)
    this.tianfuSkillTemplate:SetActive(false)
    UIEventListener.Get(this.closeBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeBtn).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), true)
    do
        local i = 0
        while i < this.activeSkillIcons.Length do
            UIEventListener.Get(this.activeSkillIcons[i].gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.activeSkillIcons[i].gameObject).onClick, MakeDelegateFromCSFunction(this.OnSkillItemClick, VoidDelegate, this), true)
            i = i + 1
        end
    end
    UIEventListener.Get(this.switchSuiteBtn.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.switchSuiteBtn.gameObject).onClick, MakeDelegateFromCSFunction(this.OnSwitchSuiteButtonClick, VoidDelegate, this), true)
end
CPlayerSkillInfoWnd.m_Init_CS2LuaHook = function (this) 

    this.titleLabel.text = LocalString.GetString("查看玩家技能")
    this:LoadProfessionSkills(EnumClass.Undefined, nil)
    this:LoadTianFuSkills(EnumClass.Undefined, 0, nil)
    this:LoadActiveSkills(nil)
    this.showFirstActiveSkillSuite = true
    this.switchSuiteBtn.Text = "1"
    this.switchSuiteBtn.gameObject:SetActive(false)
    if CPlayerInfoMgr.PlayerInfo == nil then
        return
    end
    this.titleLabel.text = CPlayerInfoMgr.PlayerInfo.name .. LocalString.GetString("的技能")
    this.switchSuiteBtn.gameObject:SetActive(CPlayerInfoMgr.PlayerInfo.skillProp.IsSkillSuite2Opened == 1)
    this.tabBar.OnTabChange = MakeDelegateFromCSFunction(this.OnTabChange, MakeGenericClass(Action2, GameObject, Int32), this)

    this.skillSeries = CSkillMgr.Inst:GetSkillSeries(CPlayerInfoMgr.PlayerInfo.cls)

    if this.skillSeries ~= nil then
        local labels = CreateFromClass(MakeArrayClass(System.String), this.skillSeries.Length)

        do
            local i = 0
            while i < labels.Length do
                labels[i] = this.skillSeries[i].Value
                i = i + 1
            end
        end
        this.tabBar:InitWithLabels(labels)
    end

    --如果开启了第二套技能并且第二套技能当前生效，初次打开时默认显示第二套技能
    local skillProp = CPlayerInfoMgr.PlayerInfo.skillProp
    if skillProp and skillProp.IsSkillSuite2Opened == 1 and  skillProp.CurrentActiveSkillSuite == 2 then
         this.showFirstActiveSkillSuite = false
    else
        this.showFirstActiveSkillSuite = true
    end

    this.tabBar:ChangeTab(0, false)
    this:LoadTianFuSkills(CommonDefs.ConvertIntToEnum(typeof(EnumClass), CPlayerInfoMgr.PlayerInfo.cls), CPlayerInfoMgr.PlayerInfo.xiuwei, CPlayerInfoMgr.PlayerInfo.skillProp)
    this:LoadActiveSkills(CPlayerInfoMgr.PlayerInfo.skillProp)
end
CPlayerSkillInfoWnd.m_OnCloseButtonClick_CS2LuaHook = function (this, go) 

    this:Close()
end
CPlayerSkillInfoWnd.m_OnTabChange_CS2LuaHook = function (this, go, index) 

    if CPlayerInfoMgr.PlayerInfo ~= nil then
        this:LoadProfessionSkills(CommonDefs.ConvertIntToEnum(typeof(EnumClass), CPlayerInfoMgr.PlayerInfo.cls), CPlayerInfoMgr.PlayerInfo.skillProp)
    end
end
CPlayerSkillInfoWnd.m_LoadProfessionSkills_CS2LuaHook = function (this, playerClass, skillProperty) 

    Extensions.RemoveAllChildren(this.scaleLine.transform)
    CommonDefs.DictClear(this.itemCellDict)
    CommonDefs.ListClear(this.itemCells)

    if this.skillSeries == nil or this.tabBar.SelectedIndex < 0 or this.tabBar.SelectedIndex >= this.skillSeries.Length then
        return
    end

    local info = CSkillMgr.Inst:GetPlayerProfessionSkillInfo(playerClass, skillProperty, this.skillSeries[this.tabBar.SelectedIndex].Key, CPlayerInfoMgr.PlayerInfo.level)
    local professionSkillIds = info.professionSkillIds
    local unlockLevels = info.unlockLevels
    local layoutDict = info.layoutDict

    local prefix = 900 + EnumToInt(playerClass)
    local unlockLevels_Special ={}
    local isYingLingJueJi =((playerClass == EnumClass.YingLing) and (this.skillSeries[this.tabBar.SelectedIndex].Key == "4"))

    if isYingLingJueJi then
        __Skill_AllSkills_Template.ForeachKey(DelegateFactory.Action_object(function (cls)
            if math.floor(cls / 1000) == prefix then
                local skillId = CLuaDesignMgr.Skill_AllSkills_GetSkillId(cls, 1)
                if not CLuaDesignMgr.Skill_AllSkills_IsOneLevelSkill(CLuaDesignMgr.Skill_AllSkills_GetClass(skillId)) then
                    local data = Skill_AllSkills.GetData(skillId)
                    if (data.ECategory == SkillCategory.Active or data.ECategory == SkillCategory.Passive) and data.EKind == SkillKind.CommonSkill then
                        if data.SeriesName ~= this.skillSeries[this.tabBar.SelectedIndex].Key then
                            return
                        end
                        table.insert(unlockLevels_Special, {["layout"] = data.Layout[1] / 1000, ["level"] = data.PlayerLevel})
                    end
                end
            end
        end))
    end

    if unlockLevels.Count == 0 and not isYingLingJueJi then
        return
    end
    CommonDefs.ListSort1(unlockLevels, typeof(UInt32), DelegateFactory.Comparison_uint(function (level1, level2) 
        return NumberCompareTo(level1, level2)
    end))

    local horizontalGap = math.floor(this.scaleLine.width / math.max(2, unlockLevels.Count - 1))
    local TotalLength = this.scaleLine.width - 50 --[[LeftOffset]]

    if isYingLingJueJi then
        for i = 1,#unlockLevels_Special do
            local item = NGUITools.AddChild(this.scaleLine.gameObject, this.scaleTemplate)
            item:SetActive(true)
            Extensions.SetLocalPositionX(item.transform, unlockLevels_Special[i]["layout"] * 900 --[[TotalLength]] + 50 --[[LeftOffset]])
            CommonDefs.GetComponent_GameObject_Type(item, typeof(UILabel)).text = tostring(unlockLevels_Special[i]["level"])
            CommonDefs.GetComponentInChildren_GameObject_Type(item, typeof(UISprite)):ResetAndUpdateAnchors()
        end
    else
        do
            local i = 0
            while i < unlockLevels.Count do
                local item = NGUITools.AddChild(this.scaleLine.gameObject, this.scaleTemplate)
                item:SetActive(true)
                Extensions.SetLocalPositionX(item.transform, CommonDefs.DictGetValue(layoutDict, typeof(UInt32), unlockLevels[i]) * TotalLength + 50 --[[LeftOffset]])
                CommonDefs.GetComponent_GameObject_Type(item, typeof(UILabel)).text = tostring(unlockLevels[i])
                CommonDefs.GetComponentInChildren_GameObject_Type(item, typeof(UISprite)):ResetAndUpdateAnchors()
                i = i + 1
            end
        end
    end

    local isYingLing = playerClass == EnumClass.YingLing
    if isYingLing then
        professionSkillIds = LuaSkillMgr.GetYingLingNewProfessionSkillIds(professionSkillIds,
                EnumToInt(skillProperty:GetCurYingLingState(CommonDefs.ConvertIntToEnum(typeof(EnumGender), CPlayerInfoMgr.PlayerInfo.gender))))
    end

    do
        local i = 0
        while i < professionSkillIds.Count do
            local item = NGUITools.AddChild(this.scaleLine.gameObject, this.skillTemplate)
            item:SetActive(true)
            local data = Skill_AllSkills.GetData(professionSkillIds[i].Key)
            local cell = CommonDefs.GetComponent_GameObject_Type(item, typeof(CSkillItemCell))
            local clsExists = skillProperty:IsOriginalSkillClsExist(data.SkillClass)

            local originSkillId = clsExists and skillProperty:GetOriginalSkillIdByCls(data.SkillClass) or 0
            local originSkillIdWithDeltaLevel = clsExists and skillProperty:GetSkillIdWithDeltaByCls(data.SkillClass, CPlayerInfoMgr.PlayerInfo.level) or 0
            local originLevel = clsExists and CLuaDesignMgr.Skill_AllSkills_GetLevel(originSkillId) or 0
            local originWithDeltaLevel = clsExists and CLuaDesignMgr.Skill_AllSkills_GetLevel(originSkillIdWithDeltaLevel) or 0

            local playerLevel = CPlayerInfoMgr.PlayerInfo.level
            local feishengLevel = clsExists and skillProperty:GetFeiShengModifyLevel(originSkillId, data.Kind, playerLevel) or 0
            local delta = originWithDeltaLevel - feishengLevel

            if CPlayerInfoMgr.PlayerInfo.xianfanStatus > 0 then
                cell:InitFeiSheng(true, feishengLevel)
            else
                cell:InitFeiSheng(false, 0)
            end

            cell:Init(data.SkillClass, originLevel, delta, true, data.SkillIcon, originLevel == 0, nil)

            UIEventListener.Get(item).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(item).onClick, MakeDelegateFromCSFunction(this.OnSkillItemClick, VoidDelegate, this), true)
            item.transform.localPosition = Vector3((data.Layout[1] / 1000) * TotalLength + 50 --[[LeftOffset]], data.Layout[0] > 0 and 100 or - 100, 0)
            CommonDefs.DictAdd(this.itemCellDict, typeof(UInt32), cell.ClassId, typeof(CSkillItemCell), cell)
            CommonDefs.ListAdd(this.itemCells, typeof(CSkillItemCell), cell)
            i = i + 1
        end
    end

    do
        local i = 0
        while i < professionSkillIds.Count do
            local data = Skill_AllSkills.GetData(professionSkillIds[i].Key)
            local _pairs = data.AdjustedPreSkills
            if _pairs ~= nil and _pairs.Length > 0 then
                do
                    local j = 0
                    while j < _pairs.Length do
                        local realFromSkillCls = _pairs[j][0]
                        if CSkillMgr.Inst:IsColorSystemBaseSkill(realFromSkillCls) then
                            realFromSkillCls = skillProperty:GetInEffectColorSystemSkill(realFromSkillCls)
                        end

                        -- 影灵破云击特殊处理
                        local cls = LuaSkillMgr.GetYingLingRealSkillCls(realFromSkillCls,CommonDefs.ConvertIntToEnum(typeof(EnumGender), CPlayerInfoMgr.PlayerInfo.gender))
                        if cls then
                            realFromSkillCls = cls
                        end

                        if CommonDefs.DictContains(this.itemCellDict, typeof(UInt32), realFromSkillCls) then
                            --生成一条箭头
                            local item = NGUITools.AddChild(this.scaleLine.gameObject, this.arrowTemplate)
                            item:SetActive(true)
                            local sprite = CommonDefs.GetComponent_GameObject_Type(item, typeof(UISprite))
                            local from = CommonDefs.DictGetValue(this.itemCellDict, typeof(UInt32), realFromSkillCls)
                            local to = CommonDefs.DictGetValue(this.itemCellDict, typeof(UInt32), CLuaDesignMgr.Skill_AllSkills_GetClass(professionSkillIds[i].Key))
                            sprite.width = math.floor((to.transform.localPosition.x - from.transform.localPosition.x - to.Width * 0.5 - from.Width * 0.5))
                            sprite.transform.localPosition = CommonDefs.op_Addition_Vector3_Vector3(from.transform.localPosition, CommonDefs.op_Multiply_Vector3_Single(CommonDefs.op_Multiply_Vector3_Single(Vector3.right, from.Width), 0.5))
                        end
                        j = j + 1
                    end
                end
            end
            i = i + 1
        end
    end
end
CPlayerSkillInfoWnd.m_LoadTianFuSkills_CS2LuaHook = function (this, playerClass, xiuweiGrade, skillProp) 

    Extensions.RemoveAllChildren(this.tianfuGrid.transform)
    CommonDefs.ListClear(this.tianfuItemCells)
    if skillProp == nil then
        return
    end
    --获取player的所有职业技能
    local prefix = 900 + EnumToInt(playerClass)
    local needXiuWeiLevels = CreateFromClass(MakeGenericClass(List, Int32))
    local tianfuSkills = CreateFromClass(MakeGenericClass(Dictionary, Int32, UInt32))
    __Skill_AllSkills_Template.ForeachKey(DelegateFactory.Action_object(function (cls)
        if math.floor(cls / 1000) == prefix then
            local skillId = CLuaDesignMgr.Skill_AllSkills_GetSkillId(cls, 1)
            --以(900+ClassId)开头，以01结尾

            local data = Skill_AllSkills.GetData(skillId)
            if data.EKind ~= SkillKind.TianFuSkill then
                return
            end
            local cls = CLuaDesignMgr.Skill_AllSkills_GetClass(skillId)
            if not CommonDefs.ListContains(needXiuWeiLevels, typeof(Int32), data.NeedXiuweiLevel) then
                CommonDefs.ListAdd(needXiuWeiLevels, typeof(Int32), data.NeedXiuweiLevel)
            end
            if skillProp:IsOriginalSkillClsExist(cls) then
                CommonDefs.DictAdd(tianfuSkills, typeof(Int32), data.NeedXiuweiLevel, typeof(UInt32), skillProp:GetSkillIdWithDeltaByCls(cls, CPlayerInfoMgr.PlayerInfo.level))
            end
        end
    end))
    CommonDefs.ListSort1(needXiuWeiLevels, typeof(Int32), DelegateFactory.Comparison_int(function (level1, level2) 
        return NumberCompareTo(level1, level2)
    end))

    do
        local i = 0
        while i < needXiuWeiLevels.Count do
            local item = NGUITools.AddChild(this.tianfuGrid.gameObject, this.tianfuSkillTemplate)
            item:SetActive(true)
            local cell = CommonDefs.GetComponent_GameObject_Type(item, typeof(CTianFuSkillItemCell))
            if CommonDefs.DictContains(tianfuSkills, typeof(Int32), needXiuWeiLevels[i]) then
                local skillId = CommonDefs.DictGetValue(tianfuSkills, typeof(Int32), needXiuWeiLevels[i])
                local data = Skill_AllSkills.GetData(skillId)
                local level = data.Level
                if CPlayerInfoMgr.PlayerInfo.xianfanStatus > 0 then
                    local playerLevel = CPlayerInfoMgr.PlayerInfo.level
                    local originSkillId = skillProp:GetOriginalSkillIdByCls(data.SkillClass)
                    local feishengLevel = level > 0 and CPlayerInfoMgr.PlayerInfo.skillProp:GetFeiShengModifyLevel(originSkillId, data.Kind, playerLevel) or 0
                    cell:InitFeiSheng(true, feishengLevel)
                else
                    cell:InitFeiSheng(false, 0)
                end
                cell:Init(data.SkillClass, level, needXiuWeiLevels[i], data.SkillIcon, xiuweiGrade < needXiuWeiLevels[i], CPlayerInfoMgr.PlayerInfo.level, skillProp)
                UIEventListener.Get(item).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(item).onClick, MakeDelegateFromCSFunction(this.OnSkillItemClick, VoidDelegate, this), true)
            else
                cell:Init(0, 0, needXiuWeiLevels[i], nil, xiuweiGrade < needXiuWeiLevels[i], CPlayerInfoMgr.PlayerInfo.level, skillProp)
            end
            CommonDefs.ListAdd(this.tianfuItemCells, typeof(CTianFuSkillItemCell), cell)
            i = i + 1
        end
    end
    this.tianfuGrid:Reposition()
end
CPlayerSkillInfoWnd.m_LoadActiveSkills_CS2LuaHook = function (this, skillProp) 
    
    local nameLabel = this.transform:Find("Anchor/ActiveSkills/name"):GetComponent(typeof(UILabel))

    if skillProp == nil then
        if nameLabel then nameLabel.text = LocalString.GetString("当前技能") end
        do
            local i = 0
            while i < this.activeSkillIcons.Length do
                this.activeSkillIcons[i]:Clear()
                this.activeSkillColorIcons[i].gameObject:SetActive(false)
                i = i + 1
            end
        end
    else
        if skillProp.IsSkillSuite2Opened == 1 then
            this.switchSuiteBtn.gameObject:SetActive(true)
            this.switchSuiteBtn.Text = this.showFirstActiveSkillSuite and "1" or "2"
        else
            this.switchSuiteBtn.gameObject:SetActive(false)
            this.showFirstActiveSkillSuite = true
        end

        local default
        if this.showFirstActiveSkillSuite then
            default = skillProp.NewActiveSkill
        else
            default = skillProp.NewActiveSkill2
        end

        if skillProp.CurrentActiveSkillSuite == 1 and this.showFirstActiveSkillSuite then
            if nameLabel then nameLabel.text = LocalString.GetString("使用中") end
        elseif skillProp.CurrentActiveSkillSuite == 2 and not this.showFirstActiveSkillSuite then
            if nameLabel then nameLabel.text = LocalString.GetString("使用中") end
        else
            if nameLabel then nameLabel.text = LocalString.GetString("备用中") end
        end

        local skillIds = default
        --ActiveSkill索引从1开始，第0个位置占位，无用
        do
            local i = 0
            while i < this.activeSkillIcons.Length do
                if i + 1 >= skillIds.Length or skillIds[i + 1] == SkillButtonSkillType_lua.NoSkill or not Skill_AllSkills.Exists(skillIds[i + 1]) then
                    this.activeSkillIcons[i]:LoadSkillIcon(nil)
                    this.activeSkillColorIcons[i].gameObject:SetActive(false)
                else
                    this.activeSkillIcons[i]:LoadSkillIcon(Skill_AllSkills.GetData(skillIds[i + 1]).SkillIcon)
                    local classId = CLuaDesignMgr.Skill_AllSkills_GetClass(skillIds[i + 1])
                    if CSkillMgr.Inst:IsColorSystemSkill(classId) or CSkillMgr.Inst:IsColorSystemBaseSkill(classId) then
                        this.activeSkillColorIcons[i].gameObject:SetActive(true)
                        this.activeSkillColorIcons[i].spriteName = CSkillMgr.Inst:GetColorSystemSkillIcon(classId)
                    else
                        this.activeSkillColorIcons[i].gameObject:SetActive(false)
                    end
                end
                i = i + 1
            end
        end
    end
end
CPlayerSkillInfoWnd.m_OnSkillItemClick_CS2LuaHook = function (this, go) 

    do
        local i = 0
        while i < this.itemCells.Count do
            if go:Equals(this.itemCells[i].gameObject) then
                this.itemCells[i].Selected = true
                CSkillInfoMgr.ShowSkillInfoWnd(CLuaDesignMgr.Skill_AllSkills_GetSkillId(this.itemCells[i].ClassId, this.itemCells[i].OriginWithDeltaLevel == 0 and 1 or this.itemCells[i].OriginWithDeltaLevel), this.tipTopPos.position, CSkillInfoMgr.EnumSkillInfoContext.ShowPlayerSkillInfo, false, CPlayerInfoMgr.PlayerInfo.level, CPlayerInfoMgr.PlayerInfo.xiuwei, CPlayerInfoMgr.PlayerInfo.skillProp)
            else
                this.itemCells[i].Selected = false
            end
            i = i + 1
        end
    end
    do
        local i = 0
        while i < this.tianfuItemCells.Count do
            if go:Equals(this.tianfuItemCells[i].gameObject) then
                this.tianfuItemCells[i].Selected = true
                CSkillInfoMgr.ShowSkillInfoWnd(CLuaDesignMgr.Skill_AllSkills_GetSkillId(this.tianfuItemCells[i].ClassId, this.tianfuItemCells[i].OriginLevel), this.tipTopPos.position, CSkillInfoMgr.EnumSkillInfoContext.ShowPlayerSkillInfo, false, CPlayerInfoMgr.PlayerInfo.level, CPlayerInfoMgr.PlayerInfo.xiuwei, CPlayerInfoMgr.PlayerInfo.skillProp)
            else
                this.tianfuItemCells[i].Selected = false
            end
            i = i + 1
        end
    end
    do
        local i = 0
        while i < this.activeSkillIcons.Length do
            if go:Equals(this.activeSkillIcons[i].gameObject) then
                local info = CPlayerInfoMgr.PlayerInfo
                if info ~= nil then
                    local id = this.showFirstActiveSkillSuite and info.skillProp.NewActiveSkill[i + 1] or info.skillProp.NewActiveSkill2[i + 1]
                    if id == SkillButtonSkillType_lua.NoSkill then
                        return
                    end

                    local equipId = nil
                    local skillCls = CLuaDesignMgr.Skill_AllSkills_GetClass(id)
                    if CSkillMgr.Inst:IsIdentifySkill(skillCls) then
                        (function () 
                            local __try_get_result
                            __try_get_result, equipId = CommonDefs.DictTryGet(info.skillProp.IdentifySkills, typeof(UInt32), skillCls, typeof(String))
                            return __try_get_result
                        end)()
                        --查找生效技能对应的装备ID
                    end
                    CSkillInfoMgr.ShowSkillInfoWnd(id, equipId, true, this.tipTopPos.position, CSkillInfoMgr.EnumSkillInfoContext.ShowPlayerSkillInfo, false, info.level, info.xiuwei, info.skillProp)
                end
            end
            i = i + 1
        end
    end
end
CPlayerSkillInfoWnd.m_OnSwitchSuiteButtonClick_CS2LuaHook = function (this, go) 

    local skillProp = CPlayerInfoMgr.PlayerInfo.skillProp
    if skillProp ~= nil and skillProp.IsSkillSuite2Opened == 1 then
        this.showFirstActiveSkillSuite = not this.showFirstActiveSkillSuite
        this:LoadActiveSkills(skillProp)
    end
end
