local GameObject = import "UnityEngine.GameObject"
local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local CLoginMgr = import "L10.Game.CLoginMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local EShareType = import "L10.UI.EShareType"
local Animation = import "UnityEngine.Animation"

LuaSnowManPVEResultWnd = class()
LuaSnowManPVEResultWnd.bResult = nil
LuaSnowManPVEResultWnd.curSelect = nil
LuaSnowManPVEResultWnd.curDifficulty = nil

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaSnowManPVEResultWnd, "winButtonList", "winButtonList", GameObject)
RegistChildComponent(LuaSnowManPVEResultWnd, "shareBtn", "shareBtn", GameObject)
RegistChildComponent(LuaSnowManPVEResultWnd, "ChallengeNextBtn", "ChallengeNextBtn", GameObject)
RegistChildComponent(LuaSnowManPVEResultWnd, "loseButtonList", "loseButtonList", GameObject)
RegistChildComponent(LuaSnowManPVEResultWnd, "ChallengeOtherBtn", "ChallengeOtherBtn", GameObject)
RegistChildComponent(LuaSnowManPVEResultWnd, "playerInfoNode", "playerInfoNode", GameObject)
RegistChildComponent(LuaSnowManPVEResultWnd, "winPattern", "winPattern", GameObject)
RegistChildComponent(LuaSnowManPVEResultWnd, "losePattern", "losePattern", GameObject)
RegistChildComponent(LuaSnowManPVEResultWnd, "SnowManIconTexture", "SnowManIconTexture", CUITexture)
RegistChildComponent(LuaSnowManPVEResultWnd, "SnowManName", "SnowManName", UILabel)
RegistChildComponent(LuaSnowManPVEResultWnd, "SnowManLevel", "SnowManLevel", UILabel)
RegistChildComponent(LuaSnowManPVEResultWnd, "ChallengeAgainBtn", "ChallengeAgainBtn", GameObject)
RegistChildComponent(LuaSnowManPVEResultWnd, "closeBtn", "closeBtn", GameObject)

--@endregion RegistChildComponent end

function LuaSnowManPVEResultWnd:Awake()
    if CommonDefs.IS_VN_CLIENT then
        self.shareBtn:SetActive(false)
    end
    
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end

    self:InitUIEvent()
    self:InitUIData()
end

function LuaSnowManPVEResultWnd:Init()

end

function LuaSnowManPVEResultWnd:InitUIEvent()
    UIEventListener.Get(self.shareBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        self.closeBtn.gameObject:SetActive(false)
        self.shareBtn.gameObject:SetActive(false)
        self.ChallengeNextBtn.gameObject:SetActive(false)
        CUICommonDef.CaptureScreen("screenshot", true, false, nil, DelegateFactory.Action_string_bytes(function(fullPath, jpgBytes)
            ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, fullPath)
            self.closeBtn.gameObject:SetActive(true)
            self.shareBtn.gameObject:SetActive(true)
            self.ChallengeNextBtn.gameObject:SetActive(true)
        end))
    end)

    UIEventListener.Get(self.ChallengeNextBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        CUIManager.CloseUI(CLuaUIResources.SnowManPVEResultWnd)
        Gac2Gas.BingTianShiLianEnterPlay(LuaSnowManPVEResultWnd.curSelect)
    end)

    UIEventListener.Get(self.ChallengeOtherBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        CUIManager.CloseUI(CLuaUIResources.SnowManPVEResultWnd)
        CUIManager.ShowUI(CLuaUIResources.SnowManPVEPlayWnd)
    end)

    UIEventListener.Get(self.ChallengeAgainBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        CUIManager.CloseUI(CLuaUIResources.SnowManPVEResultWnd)
        Gac2Gas.BingTianShiLianEnterPlay(LuaSnowManPVEResultWnd.curSelect)
    end)

    UIEventListener.Get(self.closeBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        CUIManager.CloseUI(CLuaUIResources.SnowManPVEResultWnd)
    end)
end

function LuaSnowManPVEResultWnd:InitUIData()
    self.winButtonList:SetActive(LuaSnowManPVEResultWnd.bResult)
    self.loseButtonList:SetActive(not LuaSnowManPVEResultWnd.bResult)

    if CClientMainPlayer.Inst then
        self.playerInfoNode:SetActive(true)
        self.playerInfoNode.transform:Find("name"):GetComponent(typeof(UILabel)).text = CClientMainPlayer.Inst.Name
        local myGameServer = CLoginMgr.Inst:GetSelectedGameServer()
        self.playerInfoNode.transform:Find("server"):GetComponent(typeof(UILabel)).text = myGameServer.name
        self.playerInfoNode.transform:Find("ID"):GetComponent(typeof(UILabel)).text = CClientMainPlayer.Inst.Id
        self.playerInfoNode.transform:Find("icon"):GetComponent(typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(CClientMainPlayer.Inst.Class, CClientMainPlayer.Inst.Gender))
        self.playerInfoNode.transform:Find("icon/lv"):GetComponent(typeof(UILabel)).text = CClientMainPlayer.Inst.Level
    else
        self.playerInfoNode:SetActive(false)
    end
    
    self.winPattern:SetActive(LuaSnowManPVEResultWnd.bResult)
    self.losePattern:SetActive(not LuaSnowManPVEResultWnd.bResult)

    local snowmanConfigData = HanJia2023_BingTianShiLian.GetData(LuaSnowManPVEResultWnd.curSelect)
    self.SnowManIconTexture:LoadMaterial(snowmanConfigData.SnowManMaterial)
    self.SnowManName.text = snowmanConfigData.Name
    self.SnowManName.color = NGUIText.ParseColor24(snowmanConfigData.SnowManColor, 0)
    
    self.SnowManLevel.text = SafeStringFormat3(LocalString.GetString("试炼等级 %d"), LuaSnowManPVEResultWnd.curDifficulty)

    if LuaSnowManPVEResultWnd.bResult then
        self.transform:GetComponent(typeof(Animation)):Play("snowmanpveresultwnd_sl")
    else
        self.transform:GetComponent(typeof(Animation)):Play("snowmanpveresultwnd_sb")
    end
end

--@region UIEvent

--@endregion UIEvent

