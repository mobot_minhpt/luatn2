local UILabel = import "UILabel"
local UITable = import "UITable"
local NGUITools = import "NGUITools"

CLuaRanFaJiBookWnd = class()
CLuaRanFaJiBookWnd.Path = "ui/ranfa/LuaRanFaJiBookWnd"

function CLuaRanFaJiBookWnd:Init()
    local templateObj = self.transform:Find("Offset/Item").gameObject
    templateObj:SetActive(false)
    local itemTable = self.transform:Find("Offset/ItemRoot"):GetComponent(typeof(UITable))
    local tipLabel = self.transform:Find("Offset/Panel/Label"):GetComponent(typeof(UILabel))
    tipLabel.gameObject:SetActive(false)

    for i = 1, 4 do
        for j = 0, 5 do
            local id = i + j * 4
            local designData = RanFaJi_RanFaJi.GetData(id)
            if j == 0 then
                self.transform:Find("Offset/TitleRoot/"..(j + 1)):GetComponent(typeof(UILabel)).text = designData.ColorSystemName
            end
            local obj = NGUITools.AddChild(itemTable.gameObject, templateObj)
            obj:SetActive(true)
            obj.transform:Find("Mask").gameObject:SetActive(CLuaRanFaMgr.m_OwnRanFaJiTable[id] == nil)
            local colorTexObj = obj.transform:Find("ColorTex").gameObject
            colorTexObj:GetComponent(typeof(UITexture)).color = NGUIText.ParseColor24(designData.ColorRGB, 0)
            UIEventListener.Get(colorTexObj).onClick = LuaUtils.VoidDelegate(function()
                local data = RanFaJi_RanFaJi.GetData(id)
                tipLabel.text = data.ColorName..LocalString.GetString("染发剂")
                tipLabel.gameObject:SetActive(true)
                tipLabel.transform.position = obj.transform.position
            end)
        end
    end
    itemTable:Reposition()
end
