local QnTabButton = import "L10.UI.QnTabButton"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local UIGrid = import "UIGrid"
local ClientAction = import "L10.UI.ClientAction"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local Extensions = import "Extensions"

LuaGuildWarsEnterWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaGuildWarsEnterWnd, "TianDiTab", "TianDiTab", QnTabButton)
RegistChildComponent(LuaGuildWarsEnterWnd, "JuDianTab", "JuDianTab", QnTabButton)
RegistChildComponent(LuaGuildWarsEnterWnd, "RankBtn", "RankBtn", GameObject)
RegistChildComponent(LuaGuildWarsEnterWnd, "RuleBtn", "RuleBtn", GameObject)
RegistChildComponent(LuaGuildWarsEnterWnd, "DuationLabel", "DuationLabel", UILabel)
RegistChildComponent(LuaGuildWarsEnterWnd, "TimeLabel", "TimeLabel", UILabel)
RegistChildComponent(LuaGuildWarsEnterWnd, "ConditionLabel", "ConditionLabel", UILabel)
RegistChildComponent(LuaGuildWarsEnterWnd, "TaskLabel", "TaskLabel", UILabel)
RegistChildComponent(LuaGuildWarsEnterWnd, "RewardGrid", "RewardGrid", UIGrid)
RegistChildComponent(LuaGuildWarsEnterWnd, "ItemTemplate", "ItemTemplate", GameObject)
RegistChildComponent(LuaGuildWarsEnterWnd, "TianDiRoot", "TianDiRoot", GameObject)
RegistChildComponent(LuaGuildWarsEnterWnd, "JuDianRoot", "JuDianRoot", GameObject)
RegistChildComponent(LuaGuildWarsEnterWnd, "WaiYuanBtn", "WaiYuanBtn", GameObject)
RegistChildComponent(LuaGuildWarsEnterWnd, "JieMengBtn", "JieMengBtn", GameObject)
RegistChildComponent(LuaGuildWarsEnterWnd, "CanJiaBtn", "CanJiaBtn", GameObject)
RegistChildComponent(LuaGuildWarsEnterWnd, "TianDiHighlight", "TianDiHighlight", GameObject)
RegistChildComponent(LuaGuildWarsEnterWnd, "JuDianTabHighlight", "JuDianTabHighlight", GameObject)
RegistChildComponent(LuaGuildWarsEnterWnd, "JieMengBtnRedDot", "JieMengBtnRedDot", GameObject)
--@endregion RegistChildComponent end
RegistClassMember(LuaGuildWarsEnterWnd, "ExtraLabel")
RegistClassMember(LuaGuildWarsEnterWnd, "m_IsTianDi")
RegistClassMember(LuaGuildWarsEnterWnd, "m_TianDiEnable")
RegistClassMember(LuaGuildWarsEnterWnd, "m_JuDianEnable")

function LuaGuildWarsEnterWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.RankBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRankBtnClick()
	end)


	
	UIEventListener.Get(self.RuleBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRuleBtnClick()
	end)


	
	UIEventListener.Get(self.WaiYuanBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnWaiYuanBtnClick()
	end)


	
	UIEventListener.Get(self.JieMengBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnJieMengBtnClick()
	end)


	
	UIEventListener.Get(self.CanJiaBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCanJiaBtnClick()
	end)


    --@endregion EventBind end
end

function LuaGuildWarsEnterWnd:Init()
	self.ExtraLabel = self.transform:Find("Anchor/Desc/Reward/ExtraLabel")
	self.JieMengBtnRedDot.gameObject:SetActive(false)
	self.m_Panel = self.gameObject:GetComponent(typeof(UIPanel))
    self.m_Panel.IgnoreIphoneXMargin = true
	
	self.m_TianDiEnable = false
	self.m_JuDianEnable = false

	self.TianDiTab.onClick = DelegateFactory.Action_QnTabButton(function(btn)
        self:SetMode(true)
    end)

    self.JuDianTab.onClick = DelegateFactory.Action_QnTabButton(function(btn)
        self:SetMode(false)
    end)

    self:SetMode(LuaJuDianBattleMgr.m_ShowTianDiEnterWnd)

	local itemId = {
		-- 帮贡
		21000620,
		-- 银票
		21000056,
		-- 领土
		21030423,
		-- 经验
		21000618,
		-- 帮会资金
		21051588}

	for i=1, 5 do
		local item = self.RewardGrid.transform:Find(tostring(i)).gameObject
		UIEventListener.Get(item).onClick = DelegateFactory.VoidDelegate(function()
			CItemInfoMgr.ShowLinkItemTemplateInfo(itemId[i], false, nil, AlignType.Default, 0, 0, 0, 0)
		end)
	end

	local seasonLabel = self.transform:Find("Anchor/Desc/0"):GetComponent(typeof(UILabel))
	local seasonIndex = GuildTerritoryWar_Setting.GetData().CurrentSeasonId

	seasonLabel.text = SafeStringFormat3(LocalString.GetString("第%s赛季"), Extensions.ToChinese(seasonIndex))

	-- 据点战
	Gac2Gas.GuildJuDianQueryJoinQualification()
	-- 领土战
	Gac2Gas.QueryPlayerCanJoinGuildTerritoryWar()
	--领土战结盟申请红点
	Gac2Gas.RequestTerritoryWarAllianceApplyInfo()
end

function LuaGuildWarsEnterWnd:SetMode(isTianDi)

	self.TianDiTab.gameObject:SetActive(not isTianDi)
	self.TianDiHighlight:SetActive(isTianDi)

	self.JuDianTab.gameObject:SetActive(isTianDi)
	self.JuDianTabHighlight:SetActive(not isTianDi)

	self.m_IsTianDi = isTianDi

    self.JuDianRoot:SetActive(not isTianDi)
    self.TianDiRoot:SetActive(isTianDi)
	self.ExtraLabel.gameObject:SetActive(isTianDi)

	self.RankBtn:SetActive(true)

	if isTianDi then
		self.DuationLabel.text = g_MessageMgr:FormatMessage("TDQJ_SaiJi_Time")
		self.TimeLabel.text = g_MessageMgr:FormatMessage("TDQJ_Activity_Time")
		self.ConditionLabel.text = g_MessageMgr:FormatMessage("TDQJ_Condition_Desc")

		if self.m_TianDiEnable then
			self.ConditionLabel.color = Color(0,1,96/255)
		else
			self.ConditionLabel.color = Color(1,80/255,80/255)
		end
		self.TaskLabel.text = g_MessageMgr:FormatMessage("TDQJ_Task_Desc")
	else
		self.DuationLabel.text = g_MessageMgr:FormatMessage("RJQJ_SaiJi_Time")
		self.TimeLabel.text = g_MessageMgr:FormatMessage("RJQJ_Activity_Time")
		self.ConditionLabel.text = g_MessageMgr:FormatMessage("RJQJ_Condition_Desc")

		if self.m_JuDianEnable then
			self.ConditionLabel.color = Color(0,1,96/255)
		else
			self.ConditionLabel.color = Color(1,80/255,80/255)
		end

		self.TaskLabel.text = g_MessageMgr:FormatMessage("RJQJ_Task_Desc")
	end
end

function LuaGuildWarsEnterWnd:OnJuDianInfo(canJoin)
	self.m_JuDianEnable = canJoin
	if self.m_JuDianEnable then
		self.ConditionLabel.color = Color(0,1,96/255)
	else
		self.ConditionLabel.color = Color(1,80/255,80/255)
	end
end

function LuaGuildWarsEnterWnd:OnTianDiInfo(canJoin)
	self.m_TianDiEnable = canJoin
	if self.m_TianDiEnable then
		self.ConditionLabel.color = Color(0,1,96/255)
	else
		self.ConditionLabel.color = Color(1,80/255,80/255)
	end
end

function LuaGuildWarsEnterWnd:OnAllianceApplyInfo()
	self.JieMengBtnRedDot.gameObject:SetActive(#LuaGuildTerritorialWarsMgr.m_AllianceApplyInfo > 0)
end

function LuaGuildWarsEnterWnd:OnEnable()
    g_ScriptEvent:AddListener("GuildJuDianSyncJoinQualification", self, "OnJuDianInfo")
    g_ScriptEvent:AddListener("SendPlayerCanJoinGuildTerritoryWar", self, "OnTianDiInfo")
	g_ScriptEvent:AddListener("SendTerritoryWarAllianceApplyInfo", self, "OnAllianceApplyInfo")
end

function LuaGuildWarsEnterWnd:OnDisable()
    g_ScriptEvent:RemoveListener("GuildJuDianSyncJoinQualification", self, "OnJuDianInfo")
    g_ScriptEvent:RemoveListener("SendPlayerCanJoinGuildTerritoryWar", self, "OnTianDiInfo")
	g_ScriptEvent:RemoveListener("SendTerritoryWarAllianceApplyInfo", self, "OnAllianceApplyInfo")
end

--@region UIEvent

function LuaGuildWarsEnterWnd:OnRankBtnClick()
	if self.m_IsTianDi then
		Gac2Gas.RequestTerritoryWarScoreData()
	else
		Gac2Gas.GuildJuDianQueryRankInfo()
	end
end

function LuaGuildWarsEnterWnd:OnRuleBtnClick()
	if self.m_IsTianDi then
		LuaCommonTextImageRuleMgr:ShowWnd(1)
	else
		LuaCommonTextImageRuleMgr:ShowWnd(2)
	end
end

function LuaGuildWarsEnterWnd:OnWaiYuanBtnClick()
	LuaGuildExternalAidMgr:ShowGuildExternalAidWnd()
end

function LuaGuildWarsEnterWnd:OnJieMengBtnClick()
	CUIManager.ShowUI("GuildTerritorialWarsJieMengWnd")
end

function LuaGuildWarsEnterWnd:OnCanJiaBtnClick()
	print(GuildTerritoryWar_Setting.GetData().GoToAction)
	ClientAction.DoAction(GuildTerritoryWar_Setting.GetData().GoToAction)
end


--@endregion UIEvent

