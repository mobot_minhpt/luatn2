local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CQnReturnAwardTemplate = import "L10.UI.CQnReturnAwardTemplate"
local CItemMgr=import "L10.Game.CItemMgr"
local UIDragScrollView = import "UIDragScrollView"

LuaFashionLotteryPrizePoolWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaFashionLotteryPrizePoolWnd, "ZhizunGrid", UIGrid)
RegistChildComponent(LuaFashionLotteryPrizePoolWnd, "HaohuaGrid", UIGrid)
RegistChildComponent(LuaFashionLotteryPrizePoolWnd, "JingzhiGrid", UIGrid)
RegistChildComponent(LuaFashionLotteryPrizePoolWnd, "ItemCell", GameObject)
RegistChildComponent(LuaFashionLotteryPrizePoolWnd, "RuleBtn", GameObject)
RegistChildComponent(LuaFashionLotteryPrizePoolWnd, "DescLabel", UILabel)

--@endregion RegistChildComponent end

function LuaFashionLotteryPrizePoolWnd:Awake()
    UIEventListener.Get(self.RuleBtn).onClick =  DelegateFactory.VoidDelegate(function (go)
        self:OnRuleBtnClick()
    end)
end

function LuaFashionLotteryPrizePoolWnd:Init()
    self.DescLabel.text = g_MessageMgr:FormatMessage("FashionLottery_PrizePool_Desc")
    self.ItemCell:SetActive(false)
    self:InitItemList(self.ZhizunGrid, 1)
    self:InitItemList(self.HaohuaGrid, 2)
    self:InitItemList(self.JingzhiGrid, 3)
end

function LuaFashionLotteryPrizePoolWnd:InitItemList(grid, id)
	Extensions.RemoveAllChildren(grid.transform)
    local data = FashionLottery_ItemPreview.GetData(id)
    if data then
        local itemList = data.Items
        for i = 0, itemList.Length - 1 do
            local itemId = itemList[i][0]
            local count = itemList[i][1]
            local cell = NGUITools.AddChild(grid.gameObject, self.ItemCell)
            cell:SetActive(true)
            cell:GetComponent(typeof(CQnReturnAwardTemplate)):Init(CItemMgr.Inst:GetItemTemplate(itemId), count)
            cell:GetComponent(typeof(UIDragScrollView)).enabled = id == 3
            if id == 3 then
                cell.transform:Find("QualitySprite"):GetComponent(typeof(UISprite)).spriteName = "common_itemcell_border"
            end
            cell.transform:Find("NumLabel").gameObject:SetActive(count > 1)
        end
    end
    grid:Reposition()
end

--@region UIEvent
function LuaFashionLotteryPrizePoolWnd:OnRuleBtnClick()
    g_MessageMgr:ShowMessage("FashionLottery_PrizePool_Rule_Tip")
end
--@endregion UIEvent

