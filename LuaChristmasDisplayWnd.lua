local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local Vector3 = import "UnityEngine.Vector3"
local Mathf = import "UnityEngine.Mathf"
local QnTableView=import "L10.UI.QnTableView"
local QnTabView=import "L10.UI.QnTabView"
local UICommonDef = import "L10.UI.CUICommonDef"
local UIWidget = import "UIWidget"
local Vector2 = import "UnityEngine.Vector2"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"

local ShengDan_ChristmasWindow = import "L10.Game.ShengDan_ChristmasWindow"
local CPlayerDataMgr = import "L10.Game.CPlayerDataMgr"
local EShareType = import "L10.UI.EShareType"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local DateTime = import "System.DateTime"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local UITexture = import "UITexture"
local Screen = import "UnityEngine.Screen"
local UIRoot = import "UIRoot"
local UICamera = import "UICamera"
local CommonDefs = import "L10.Game.CommonDefs"
local Constants = import "L10.Game.Constants"

CLuaChristmasDisplayWnd = class()

RegistClassMember(CLuaChristmasDisplayWnd,"m_DecorationView")
RegistClassMember(CLuaChristmasDisplayWnd,"m_CloseButton")
RegistClassMember(CLuaChristmasDisplayWnd,"m_BottomLeftBtns")
RegistClassMember(CLuaChristmasDisplayWnd,"m_SaveBtn")
RegistClassMember(CLuaChristmasDisplayWnd,"m_ShareBtn")
RegistClassMember(CLuaChristmasDisplayWnd,"m_DisplayBox")
RegistClassMember(CLuaChristmasDisplayWnd,"m_ShowNode")
RegistClassMember(CLuaChristmasDisplayWnd,"m_EditImageHander")
RegistClassMember(CLuaChristmasDisplayWnd,"m_StickerTexture")
RegistClassMember(CLuaChristmasDisplayWnd,"m_CustomizeAnchor")
RegistClassMember(CLuaChristmasDisplayWnd,"m_StickerTextureAnchor")
RegistClassMember(CLuaChristmasDisplayWnd,"m_SaveBox")
RegistClassMember(CLuaChristmasDisplayWnd,"closeTick")
RegistClassMember(CLuaChristmasDisplayWnd,"m_EditImageHandlerList") 
RegistClassMember(CLuaChristmasDisplayWnd,"m_StickerList")
RegistClassMember(CLuaChristmasDisplayWnd,"m_StickerTypeList")
RegistClassMember(CLuaChristmasDisplayWnd,"m_BackGroudName")
RegistClassMember(CLuaChristmasDisplayWnd,"m_Customize")
RegistClassMember(CLuaChristmasDisplayWnd,"m_BoxCancleBtn")
RegistClassMember(CLuaChristmasDisplayWnd,"m_BoxSaveBtn")
RegistClassMember(CLuaChristmasDisplayWnd,"m_GuashiView")
RegistClassMember(CLuaChristmasDisplayWnd,"m_WordView")
RegistClassMember(CLuaChristmasDisplayWnd,"m_BackGroundView")
RegistClassMember(CLuaChristmasDisplayWnd,"m_DecorationTable")
RegistClassMember(CLuaChristmasDisplayWnd,"m_TabView")
RegistClassMember(CLuaChristmasDisplayWnd,"m_Id2ReuseTimes")
RegistClassMember(CLuaChristmasDisplayWnd,"m_HandlerIndex")
RegistClassMember(CLuaChristmasDisplayWnd,"m_CurHandlerIndex")
RegistClassMember(CLuaChristmasDisplayWnd,"m_CurWorldIndex")
RegistClassMember(CLuaChristmasDisplayWnd,"m_StartDepth")

function CLuaChristmasDisplayWnd:Awake()
    self.closeTick = nil
    self.m_HandlerIndex = 0
    self.m_CurHandlerIndex = 0
    self.m_CurWorldIndex = nil
    self.m_StartDepth = 0

    self.m_EditImageHandlerList = {}
    self.m_StickerList = {}
    self.m_StickerTypeList = {}
    self.m_BackGroudName = nil
    self.m_Id2ReuseTimes = {}

    self.m_StickerTexture = self.transform:Find("Anchor/showNode/portrait/SystemPortrait/StickerTexture").gameObject
    self.m_StickerTextureAnchor = self.transform:Find("Anchor/showNode/portrait/SystemPortrait/StickerTextureAnchor").gameObject
    self.m_CustomizeAnchor = self.transform:Find("Anchor/showNode/CustomizeAnchor").gameObject
    self.m_Customize = self.transform:Find("Anchor/showNode/customize").gameObject

    self.m_SaveBtn = self.transform:Find("Anchor/BottomBtns/SaveButton").gameObject
    self.m_ShareBtn = self.transform:Find("Anchor/BottomBtns/ShareButton").gameObject
    self.m_CloseButton = self.transform:Find("CloseBtn/CloseButton").gameObject

    self.m_SaveBox = self.transform:Find("SaveBox")
    self.m_BoxCancleBtn =  self.transform:Find("SaveBox/Page/CancelButton").gameObject
    self.m_BoxSaveBtn =  self.transform:Find("SaveBox/Page/OKButton").gameObject
    local decorationBtn = self.transform:Find("Anchor/BottomBtns/BottonLeftTabView/DecorationBtn")
    local guashiBtn = self.transform:Find("Anchor/BottomBtns/BottonLeftTabView/GuashiBtn")
    local backGroundBtn = self.transform:Find("Anchor/BottomBtns/BottonLeftTabView/BackGroundBtn")
    local wordBtn = self.transform:Find("Anchor/BottomBtns/BottonLeftTabView/WordBtn")
    self.m_BottomLeftBtns = {backGroundBtn,guashiBtn,decorationBtn,wordBtn}

    self.m_DecorationView = self.transform:Find("Anchor/DecorationView"):GetComponent(typeof(QnTableView))
    self.m_GuashiView = self.transform:Find("Anchor/GuashiView"):GetComponent(typeof(QnTableView))
    self.m_WordView = self.transform:Find("Anchor/WordView"):GetComponent(typeof(QnTableView))
    self.m_BackGroundView = self.transform:Find("Anchor/BackGroundView"):GetComponent(typeof(QnTableView))
    self.m_TabView = self.transform:Find("Anchor/BottomBtns/BottonLeftTabView"):GetComponent(typeof(QnTabView))

    UIEventListener.Get(self.m_SaveBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        self:OnSaveCurrentWindow()
    end)

    UIEventListener.Get(self.m_CloseButton).onClick = DelegateFactory.VoidDelegate(function (p)
        self.m_SaveBox.gameObject:SetActive(true)      
    end)

    UIEventListener.Get(self.m_ShareBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        self:OnShareBtnClick()
    end)

    UIEventListener.Get(self.m_BoxCancleBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        CUIManager.CloseUI("ChristmasDisplayWnd")
    end)

    UIEventListener.Get(self.m_BoxSaveBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        self:OnSaveCurrentWindow()
        CUIManager.CloseUI("ChristmasDisplayWnd")
    end)
end

function CLuaChristmasDisplayWnd:Init()
    CLuaChristmasDisplayWndMgr.LoadChristmasWindowViewData()
    self.m_BackGroudName = CLuaChristmasDisplayWndMgr.m_BackGroundName
    if self.m_BackGroudName then
        local bgTexture = self.transform:Find("Anchor/DisplayBox/BgTexture"):GetComponent(typeof(CUITexture))
        bgTexture:LoadMaterial(self.m_BackGroudName)
        bgTexture.gameObject:SetActive(true)
    end
    
    self.m_SaveBox.gameObject:SetActive(false)
    self:InitStickerData()
    for index,data in ipairs(CLuaChristmasDisplayWndMgr.StickerOnShowList) do
        local stickerId = tonumber(data[5])
        local stickerType = tonumber(data[6])
        local flipScaleX = tonumber(data[7])
        local depth = tonumber(data[8])
        local x = tonumber(data[1])
        local y = tonumber(data[2])
        local x2 = tonumber(data[3])
        local y2 = tonumber(data[4])
        local pivotPos = Vector3(x,y)
        local bottomRightPos = Vector3(x2,y2)
        local info = ShengDan_ChristmasWindow.GetData(stickerId)
        local width = info.Width
        local height = info.Height
        self:AddSticker(stickerId,stickerType,nil,pivotPos,bottomRightPos,flipScaleX,depth)
    end
    self.m_HandlerIndex = self.m_StartDepth
 
    self.m_DecorationView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #CLuaChristmasDisplayWndMgr.DecorationList
        end,
        function(item,row)
            self:InitItem(item,row)
        end)

    self.m_GuashiView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #CLuaChristmasDisplayWndMgr.GuashiList
        end,
        function(item,row)
            self:InitItem(item,row)
        end)
    ----------
    self.m_WordView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #CLuaChristmasDisplayWndMgr.WordStickerList
        end,
        function(item,row)
            self:InitItem(item,row)
        end)
    --------
    self.m_BackGroundView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #CLuaChristmasDisplayWndMgr.BackGroundTextureList
        end,
        function(item,row)
            self:InitItem(item,row)
        end)

    self.m_TabView.OnSelect = DelegateFactory.Action_QnTabButton_int(function (go, index)
        if self.m_TabView.CurrentSelectTab==0 then--背景
            self.m_BackGroundView:ReloadData(false, false)
            self.m_DecorationView.gameObject:SetActive(false)
            self.m_GuashiView.gameObject:SetActive(false)
            self.m_WordView.gameObject:SetActive(false)
            self.m_BackGroundView.gameObject:SetActive(true)
            self.m_BackGroundView:ReloadData(false, false)
        elseif self.m_TabView.CurrentSelectTab==1 then--挂饰
            self.m_GuashiView:ReloadData(false, false)
            self.m_DecorationView.gameObject:SetActive(false)
            self.m_GuashiView.gameObject:SetActive(true)
            self.m_WordView.gameObject:SetActive(false)
            self.m_BackGroundView.gameObject:SetActive(false)
            self.m_GuashiView:ReloadData(false, false)
        elseif self.m_TabView.CurrentSelectTab==2 then--摆件
            self.m_DecorationView:ReloadData(false, false)
            self.m_DecorationView.gameObject:SetActive(true)
            self.m_GuashiView.gameObject:SetActive(false)
            self.m_WordView.gameObject:SetActive(false)
            self.m_BackGroundView.gameObject:SetActive(false)
            self.m_DecorationView:ReloadData(false, false)           
        elseif self.m_TabView.CurrentSelectTab==3 then--文字
            self.m_WordView:ReloadData(false, false)
            self.m_DecorationView.gameObject:SetActive(false)
            self.m_GuashiView.gameObject:SetActive(false)
            self.m_WordView.gameObject:SetActive(true)
            self.m_BackGroundView.gameObject:SetActive(false)
            self.m_WordView:ReloadData(false, false)
        end
        self:SetLeftButnStatus()    
    end)
end

function CLuaChristmasDisplayWnd:SetLeftButnStatus()
    for i=1,4,1 do
        if i == self.m_TabView.CurrentSelectTab+1 then
            self.m_BottomLeftBtns[i]:Find("highlight").gameObject:SetActive(true)
        else
            self.m_BottomLeftBtns[i]:Find("highlight").gameObject:SetActive(false)
        end
    end
end

function CLuaChristmasDisplayWnd:OnSelectAtNormalItem(data)
    if data then
        local stickerName = data.ResName
        local type = data.Type
        local scale = data.Scale
        if(self.m_Id2ReuseTimes[data.ID]) <= 0 then
            g_MessageMgr:ShowMessage("Christmas_Sticker_Use_Out")
        else
            self:AddSticker(data.ID,type,scale)
        end   
    end
end
function CLuaChristmasDisplayWnd:OnSelectAtBackGroundItem(data)
    if data then
        local textureName = data.ResName
        local bgTexture = self.transform:Find("Anchor/DisplayBox/BgTexture"):GetComponent(typeof(CUITexture))
        bgTexture.gameObject:SetActive(true)
        bgTexture:LoadMaterial(textureName)
        self.m_BackGroudName = textureName
        self.m_BackGroundView.gameObject:SetActive(false)
        self.m_BackGroundView:Clear()
    end
end
function CLuaChristmasDisplayWnd:InitItem(item,row)
    local data
    if self.m_TabView.CurrentSelectTab==0 then
        data = CLuaChristmasDisplayWndMgr.BackGroundTextureList[row+1]
    elseif self.m_TabView.CurrentSelectTab==1 then
        data = CLuaChristmasDisplayWndMgr.GuashiList[row+1]
    elseif self.m_TabView.CurrentSelectTab==2 then
        data = CLuaChristmasDisplayWndMgr.DecorationList[row+1]
    elseif self.m_TabView.CurrentSelectTab==3 then
        data = CLuaChristmasDisplayWndMgr.WordStickerList[row+1]
    end
    if data then
        local texture = item.transform:Find("Texture"):GetComponent(typeof(CUITexture))
        local stickerName = data.ResName
        texture:LoadMaterial(stickerName)
        local countLabel = item.transform:Find("CountLabel"):GetComponent(typeof(UILabel))
        countLabel.text = self.m_Id2ReuseTimes[data.ID]
        --if begin
        local year, month, day, hour, min = string.match(data.BeginTime, "(%d+)-(%d+)-(%d+) (%d+):(%d+)") 
        local now = CServerTimeMgr.Inst:GetZone8Time()
        local beginOfTheUseDay = CreateFromClass(DateTime, year, month, day, hour, min, 0)
        local isBegin = false
        if DateTime.Compare(now, beginOfTheUseDay) >= 0 then
            Extensions.SetLocalPositionZ(texture.transform, 0)
            isBegin = true
        else
            Extensions.SetLocalPositionZ(texture.transform, -1)
            isBegin = false
        end

        UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
            if isBegin then
                if data.Type == CLuaChristmasDisplayWndMgr.Type_BackGround then
                    self:OnSelectAtBackGroundItem(data)
                else
                    self:OnSelectAtNormalItem(data)
                end
            else
                g_MessageMgr:ShowMessage("CUSTOM_STRING2",SafeStringFormat3(LocalString.GetString("%d年%d月%d日开放，敬请期待。"),year,month,day))
            end
        end)
    end
end

function CLuaChristmasDisplayWnd:InitStickerData()
    CLuaChristmasDisplayWndMgr.DecorationList = {}
    CLuaChristmasDisplayWndMgr.GuashiList = {}
    CLuaChristmasDisplayWndMgr.WordStickerList = {}
    CLuaChristmasDisplayWndMgr.BackGroundTextureList = {}

    ShengDan_ChristmasWindow.ForeachKey(DelegateFactory.Action_object(function (key)       
        local data = ShengDan_ChristmasWindow.GetData(key)
        if data.Type == CLuaChristmasDisplayWndMgr.Type_GuaShi then
            table.insert(CLuaChristmasDisplayWndMgr.GuashiList,data)
        elseif data.Type == CLuaChristmasDisplayWndMgr.Type_Decoration then
            table.insert(CLuaChristmasDisplayWndMgr.DecorationList,data)
        elseif data.Type == CLuaChristmasDisplayWndMgr.Type_Word then
            table.insert(CLuaChristmasDisplayWndMgr.WordStickerList,data)
        elseif data.Type == CLuaChristmasDisplayWndMgr.Type_BackGround then
            table.insert(CLuaChristmasDisplayWndMgr.BackGroundTextureList,data)
        end
        self.m_Id2ReuseTimes[key] = data.ReuseTimes
    end))
    CLuaChristmasDisplayWndMgr.isReadDataFromTable = true
end

function CLuaChristmasDisplayWnd:OnEnable()
    g_ScriptEvent:AddListener("OnCapsturePartOfWinSucceed", self, "OnCaptureSucceed")
end

function CLuaChristmasDisplayWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnCapsturePartOfWinSucceed", self, "OnCaptureSucceed")
end

function CLuaChristmasDisplayWnd:OnCaptureSucceed()
    self.m_CloseButton:SetActive(true)
end

function CLuaChristmasDisplayWnd:CloseAllTableView()
    self.m_BottomLeftBtns[self.m_TabView.CurrentSelectTab + 1]:Find("highlight").gameObject:SetActive(false)
    self.m_GuashiView.gameObject:SetActive(false)
    self.m_GuashiView:Clear()
    self.m_DecorationView.gameObject:SetActive(false)
    self.m_DecorationView:Clear()
    self.m_WordView.gameObject:SetActive(false)
    self.m_WordView:Clear()
    self.m_BackGroundView.gameObject:SetActive(false)
    self.m_BackGroundView:Clear()
end

function CLuaChristmasDisplayWnd:AddSticker(stickerId,type,scale,defaultPivotPos,defaultRightBottomPos,flipScaleX,depth)
    self.m_Id2ReuseTimes[stickerId] = self.m_Id2ReuseTimes[stickerId] - 1
    self:CloseAllTableView()
    self.m_HandlerIndex = self.m_HandlerIndex + 1

    if self.m_EditImageHandlerList[self.m_CurHandlerIndex] then
        self.m_EditImageHandlerList[self.m_CurHandlerIndex]:OnClickConfirmBtn() 
    end
    self.m_CurHandlerIndex = self.m_HandlerIndex--

    local customizeGo = NGUITools.AddChild(self.m_CustomizeAnchor,self.m_Customize)
    customizeGo:SetActive(true)
    local handler = customizeGo.transform:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
    handler:SetHandlerIndex(self.m_HandlerIndex)
    self.m_EditImageHandlerList[self.m_HandlerIndex] = handler
    local stickerGo = NGUITools.AddChild(self.m_StickerTextureAnchor,self.m_StickerTexture)
    local data = ShengDan_ChristmasWindow.GetData(stickerId)
    local widget = stickerGo.transform:GetComponent(typeof(UIWidget))
    local width = data.Width
    local height = data.Height
    widget.width = width
    widget.height = height

    if scale then
        local widget = stickerGo.transform:GetComponent(typeof(UIWidget))
        widget.width = widget.width * scale
        widget.height = widget.height * scale
    end
    stickerGo:SetActive(true)
    local stickerName = data.ResName

    stickerGo.transform:GetComponent(typeof(CUITexture)):LoadMaterial(stickerName)
    self.m_StickerList[self.m_HandlerIndex] = stickerId
    self.m_StickerTypeList[self.m_HandlerIndex] = type
    stickerGo:GetComponent(typeof(UITexture)).depth = self.m_HandlerIndex
    handler.Depth = self.m_HandlerIndex

    if type == CLuaChristmasDisplayWndMgr.Type_GuaShi then
        handler:SetEditSticker()
    elseif type ==CLuaChristmasDisplayWndMgr.Type_Word then
        handler:SetEditTxt()
        if self.m_CurWorldIndex then
            self.m_EditImageHandlerList[self.m_CurWorldIndex]:OnClickCancleBtn()
        end
        self.m_CurWorldIndex = self.m_HandlerIndex
    elseif type == CLuaChristmasDisplayWndMgr.Type_Decoration then
        handler:SetEditSticker()
    end
    handler:SetRelatedWidget(stickerGo.transform:GetComponent(typeof(CUITexture)).texture)

    UIEventListener.Get(stickerGo).onClick = DelegateFactory.VoidDelegate(function (p)
        if self.m_EditImageHandlerList[self.m_CurHandlerIndex] then
            self.m_EditImageHandlerList[self.m_CurHandlerIndex]:OnClickConfirmBtn() 
        end
        self.m_CurHandlerIndex = handler:GetHandlerIndex()        
        customizeGo:SetActive(true)
        self.m_HandlerIndex = self.m_HandlerIndex + 1
        stickerGo:GetComponent(typeof(UITexture)).depth = self.m_HandlerIndex
        handler.Depth = self.m_HandlerIndex
        self:CloseAllTableView()
    end)

    if defaultPivotPos and defaultRightBottomPos then
        handler.pivot.transform.localPosition = defaultPivotPos
        handler.bottomRightPos = defaultRightBottomPos
        handler:UpdatePosition()
        handler.prePivotPos = defaultPivotPos
        handler.preBottomRightPos = defaultRightBottomPos
        --flipScaleX
        handler.relatedWidget.transform.localScale = Vector3(flipScaleX,1,1)
        handler.gameObject:SetActive(false)
        if depth then
            stickerGo:GetComponent(typeof(UITexture)).depth = depth
            if depth >= self.m_StartDepth then
                self.m_StartDepth = depth
            end
        end
    end

    handler.prePivotPos = handler.pivot.transform.localPosition
    handler.preBottomRightPos = handler.rotateBtn.transform.localPosition
    handler.onCancle = DelegateFactory.Action(function ()
        local index = handler:GetHandlerIndex()
        local id = self.m_StickerList[index]
        self.m_Id2ReuseTimes[id] = self.m_Id2ReuseTimes[id] + 1
        self.m_EditImageHandlerList[index] = nil
        self.m_StickerTypeList[index] = nil
        self.m_StickerList[index] = nil
        GameObject.Destroy(customizeGo)
        GameObject.Destroy(stickerGo)
        if type == CLuaChristmasDisplayWndMgr.Type_GuaShi then
            self.m_GuashiView:ReloadData(false, false)
        elseif type == CLuaChristmasDisplayWndMgr.Type_Decoration then
            self.m_DecorationView:ReloadData(false, false)
        elseif type == CLuaChristmasDisplayWndMgr.Type_Word then
            self.m_WordView:ReloadData(false, false)
            self.m_CurWorldIndex = nil
        elseif type == CLuaChristmasDisplayWndMgr.Type_BackGround then
            self.m_BackGroundView:ReloadData(false, false)
        end 
    end)

    handler.onConfirm = DelegateFactory.Action(function ()
        if type == CLuaChristmasDisplayWndMgr.Type_GuaShi then
            local topPosY = Mathf.Max(handler.confirmBtn.transform.localPosition.y,handler.cancleBtn.transform.localPosition.y)
            topPosY = Mathf.Max(topPosY,handler.rotateBtn.transform.localPosition.y)
            topPosY = Mathf.Max(topPosY,handler.bottomRightPos.y)
            local corners = handler.portrait.worldCorners
            local topY = handler.pivot.transform.parent:InverseTransformPoint(corners[2]).y
            if topPosY < topY then
                local offset = topY - topPosY
                local povotPos = handler.pivot.transform.localPosition
                handler.pivot.transform.localPosition = Vector3(povotPos.x,povotPos.y+offset,povotPos.z)
                local bottomRightPos = handler.bottomRightPos
                bottomRightPos = Vector3(bottomRightPos.x,bottomRightPos.y+offset,bottomRightPos.z)
                handler.bottomRightPos = bottomRightPos
                handler:UpdatePosition()
            end
        end

        handler.prePivotPos = handler.pivot.transform.localPosition
        handler.preBottomRightPos = handler.rotateBtn.transform.localPosition
    end)
end

function CLuaChristmasDisplayWnd:OnShareBtnClick()
    self.m_CloseButton:SetActive(false)
    self.transform:Find("Anchor/BottomBtns").gameObject:SetActive(false)
    self:WaitShowCloseBtn()
    local displayBox = self.transform:Find("Anchor/DisplayBox/DisPlayBoxBg"):GetComponent(typeof(UIWidget))
    local capstureAnchor = self.transform:Find("Anchor/CapstureAnchor")
    local startPos = displayBox.transform.localPosition

    local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject)
    local virtualScreenHeight = Screen.height * scale
    local virtualScreenWidth = Screen.width * scale
    local width = displayBox.width
    local height = displayBox.height
    local pos = UICamera.currentCamera:WorldToScreenPoint(displayBox.transform.position)
    local x = pos.x * scale
    local y = virtualScreenHeight - pos.y*scale

    if virtualScreenWidth > 1920 and virtualScreenHeight < 1090 then
        -- 是否打开外置聊天
        local percent = 1
        if CommonDefs.IsPCGameMode() then
            local CWinSocialWndMgr = import "L10.UI.CWinSocialWndMgr"
            if CWinSocialWndMgr.Inst.m_IsWinSocialWndOpened then
                percent = 1 - Constants.WinSocialWndRatio
            end
        end
        
        height = (height/scale)*(1920/Screen.width)
        height = height / percent
        width = (width/scale)*(1920/Screen.width)
        x = x/scale*(1920/Screen.width)
        width = width/percent
        x = x/percent
    end
    UICommonDef.CapturePartOfScreenAndShare("ChristmasWindowShoot",EShareType.ShareImage,Vector2(x,y), width, height)
end

function CLuaChristmasDisplayWnd:OnSaveCurrentWindow()
    CLuaChristmasDisplayWndMgr.StickerOnShowData = ""
    CLuaChristmasDisplayWndMgr.StickerOnShowList = {}
    if self.m_BackGroudName then
        CLuaChristmasDisplayWndMgr.StickerOnShowData = CLuaChristmasDisplayWndMgr.StickerOnShowData..self.m_BackGroudName..";"
    end
    for i,handler in pairs(self.m_EditImageHandlerList) do
        local data = ""
        data = data..handler.pivot.transform.localPosition.x..","
        data = data..handler.pivot.transform.localPosition.y..","
        data = data..handler.bottomRightPos.x..","
        data = data..handler.bottomRightPos.y..","
        data = data..self.m_StickerList[i]..","
        data = data..self.m_StickerTypeList[i]..","
        --flip
        data = data..handler.relatedWidget.transform.localScale.x..","
        data = data..handler.Depth..","
        data = data..";"
        CLuaChristmasDisplayWndMgr.StickerOnShowData = CLuaChristmasDisplayWndMgr.StickerOnShowData..data
    end
    CPlayerDataMgr.Inst:SavePlayerData(CLuaChristmasDisplayWndMgr.FileName,CLuaChristmasDisplayWndMgr.StickerOnShowData)
    g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("当前装扮已保存，下次打开界面自动显示。"))
end

function CLuaChristmasDisplayWnd:WaitShowCloseBtn()
    if self.closeTick then
        UnRegisterTick(self.closeTick)
        self.closeTick = nil
    end
    self.closeTick = RegisterTickOnce(function ( ... )
        self.closeTick = nil
        self.m_CloseButton:SetActive(true)
        self.transform:Find("Anchor/BottomBtns").gameObject:SetActive(true)     
		end, 1000 * CLuaChristmasDisplayWndMgr.delayShowTime)
end
