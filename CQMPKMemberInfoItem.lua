-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Color = import "UnityEngine.Color"
local Constants = import "L10.Game.Constants"
local CQMPKMemberInfoItem = import "L10.UI.CQMPKMemberInfoItem"
local CUICommonDef = import "L10.UI.CUICommonDef"
local LocalString = import "LocalString"
local Object = import "System.Object"
local Profession = import "L10.Game.Profession"
CQMPKMemberInfoItem.m_OnClick_CS2LuaHook = function (this) 
    if this.ClickCallback ~= nil then
        GenericDelegateInvoke(this.ClickCallback, Table2ArrayWithCount({this.m_PlayerId, this.gameObject}, 2, MakeArrayClass(Object)))
    end
end
CQMPKMemberInfoItem.m_SetMemberInfo_CS2LuaHook = function (this, member, index, chuZhanInfo) 
    if nil == member then
        this.m_NoMemberObj:SetActive(true)
        this.m_InfoRoot:SetActive(false)
        this.m_PlayerId = 0
        return
    end
    this.m_NoMemberObj:SetActive(false)
    this.m_InfoRoot:SetActive(true)
    this.m_SelectedObj:SetActive(false)
    this.m_PlayerId = member.m_Id
    this.IconTexture:LoadNPCPortrait(CUICommonDef.GetPortraitName(member.m_Class, member.m_Gender, -1), false)
    this.LevelLabel.text = System.String.Format("lv.{0}", member.m_Grade)
    this.NameLabel.text = member.m_Name
    this.LeaderMark:SetActive(index == 0)
    this.ClazzMarkSprite.spriteName = Profession.GetIcon(member.m_Class)
    if CClientMainPlayer.Inst ~= nil and this.m_PlayerId == CClientMainPlayer.Inst.Id then
        this.NameLabel.color = Constants.SelfLabelColor
    else
        this.NameLabel.color = Color.white
    end
    if chuZhanInfo == nil or chuZhanInfo.Count < 5 then
        return
    end
    if (bit.band(chuZhanInfo[0], (bit.lshift(1, index)))) ~= 0 or (bit.band(chuZhanInfo[3], (bit.lshift(1, index)))) ~= 0 then
        this.m_GroupObj:SetActive(true)
    else
        this.m_GroupObj:SetActive(false)
    end
    if (bit.band(chuZhanInfo[1], (bit.lshift(1, index)))) ~= 0 or (bit.band(chuZhanInfo[4], (bit.lshift(1, index)))) ~= 0 then
        this.m_SigleObj:SetActive(true)
        if (bit.band(chuZhanInfo[1], (bit.lshift(1, index)))) ~= 0 then
            this.m_SigleIndexLabel.text = LocalString.GetString("一")
        else
            this.m_SigleIndexLabel.text = LocalString.GetString("二")
        end
    else
        this.m_SigleObj:SetActive(false)
    end
    if (bit.band(chuZhanInfo[2], (bit.lshift(1, index)))) ~= 0 then
        this.m_ThreeVThreeObj:SetActive(true)
    else
        this.m_ThreeVThreeObj:SetActive(false)
    end
end
