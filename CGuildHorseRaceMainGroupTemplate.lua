-- Auto Generated!!
local AlignType = import "CPlayerInfoMgr+AlignType"
local CGuildHorseRaceMainGroupTemplate = import "L10.UI.CGuildHorseRaceMainGroupTemplate"
local CGuildHorseRaceMainPlayerTemplate = import "L10.UI.CGuildHorseRaceMainPlayerTemplate"
local CGuildHorseRaceMgr = import "L10.Game.CGuildHorseRaceMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local Extensions = import "Extensions"
local LocalString = import "LocalString"
local NGUITools = import "NGUITools"
local Object = import "System.Object"
local UIEventListener = import "UIEventListener"
local Vector3 = import "UnityEngine.Vector3"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CGuildHorseRaceMainGroupTemplate.m_Init_CS2LuaHook = function (this, info, selectPlayerId) 
    this.info = info
    this.orderTag:SetActive(info.order)
    if CGuildHorseRaceMgr.Inst.mainStatus <= 3 then
        this.groupLabel.text = System.String.Format(LocalString.GetString("{0}组"), info.groupIndex)
    else
        this.groupLabel.text = System.String.Format(LocalString.GetString("{0}-{1}名"), (info.groupIndex - 1) * 5 + 1, math.min(info.groupIndex * 5, CGuildHorseRaceMgr.Inst.mainTotalPlayer))
    end
    Extensions.RemoveAllChildren(this.grid.transform)
    this.playerTemplate:SetActive(false)
    CommonDefs.ListClear(this.playerList)
    this.button:SetSelected(false, false)
    do
        local i = 0
        while i < info.playerList.Count do
            local instance = NGUITools.AddChild(this.grid.gameObject, this.playerTemplate)
            instance:SetActive(true)
            local template = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CGuildHorseRaceMainPlayerTemplate))
            if template ~= nil then
                template:Init(info.playerList[i])
                CommonDefs.ListAdd(this.playerList, typeof(CGuildHorseRaceMainPlayerTemplate), template)
                UIEventListener.Get(instance).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(instance).onClick, MakeDelegateFromCSFunction(this.OnPlayerSelect, VoidDelegate, this), true)
                template:SetSelect(selectPlayerId == info.playerList[i].playerId)
            end
            i = i + 1
        end
    end
    this.grid:Reposition()
end
CGuildHorseRaceMainGroupTemplate.m_OnPlayerSelect_CS2LuaHook = function (this, go) 
    do
        local i = 0
        while i < this.playerList.Count do
            this.playerList[i]:SetSelect(go == this.playerList[i].gameObject)
            if go == this.playerList[i].gameObject then
                this.selectPlayerIndex = i
                this.selectPlayerId = this.playerList[i].info.playerId
                if this.OnPlayerInThisGroupSelect ~= nil then
                    GenericDelegateInvoke(this.OnPlayerInThisGroupSelect, Table2ArrayWithCount({this.gameObject}, 1, MakeArrayClass(Object)))
                end
                CPlayerInfoMgr.ShowPlayerPopupMenu(this.selectPlayerId, EnumPlayerInfoContext.RankList, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
            end
            i = i + 1
        end
    end
end
CGuildHorseRaceMainGroupTemplate.m_UnSelectAll_CS2LuaHook = function (this) 
    do
        local i = 0
        while i < this.playerList.Count do
            this.playerList[i]:SetSelect(false)
            i = i + 1
        end
    end
    this.selectPlayerIndex = - 1
    this.selectPlayerId = 0
end
