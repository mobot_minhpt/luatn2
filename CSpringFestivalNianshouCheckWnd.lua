-- Auto Generated!!
local Color = import "UnityEngine.Color"
local Constants = import "L10.Game.Constants"
local CSpringFestivalMgr = import "L10.Game.CSpringFestivalMgr"
local CSpringFestivalNianshouCheckWnd = import "L10.UI.CSpringFestivalNianshouCheckWnd"
local Extensions = import "Extensions"
local NGUITools = import "NGUITools"
local UILabel = import "UILabel"
local UISprite = import "UISprite"
CSpringFestivalNianshouCheckWnd.m_Init_CS2LuaHook = function (this) 
    this.yongshiTemplate:SetActive(false)
    this.nianshouTemplate:SetActive(false)

    if CSpringFestivalMgr.Instance.nianshouGameplayResult == 2 then
        this.yongshiResult.gameObject:SetActive(true)
        this.yongshiResult.spriteName = Constants.WinSprite
        this.yongshiResultBg.gameObject:SetActive(true)
        Extensions.SetLocalPositionZ(this.yongshiResultBg.transform, 0)

        this.nianshouResult.gameObject:SetActive(true)
        this.nianshouResult.spriteName = Constants.LoseSprite
        this.nianshouResultBg.gameObject:SetActive(true)
        Extensions.SetLocalPositionZ(this.nianshouResultBg.transform, - 1)
    elseif CSpringFestivalMgr.Instance.nianshouGameplayResult == 3 then
        this.nianshouResult.gameObject:SetActive(true)
        this.nianshouResult.spriteName = Constants.WinSprite
        this.nianshouResultBg.gameObject:SetActive(true)
        Extensions.SetLocalPositionZ(this.nianshouResultBg.transform, 0)

        this.yongshiResult.gameObject:SetActive(true)
        this.yongshiResult.spriteName = Constants.LoseSprite
        this.yongshiResultBg.gameObject:SetActive(true)
        Extensions.SetLocalPositionZ(this.yongshiResultBg.transform, - 1)
    else
        this.yongshiResult.gameObject:SetActive(false)
        this.yongshiResultBg.gameObject:SetActive(false)
        this.nianshouResult.gameObject:SetActive(false)
        this.nianshouResultBg.gameObject:SetActive(false)
    end

    do
        local i = 0
        while i < CSpringFestivalMgr.Instance.yongShiDataList.Count do
            local data = CSpringFestivalMgr.Instance.yongShiDataList[i]
            local node = NGUITools.AddChild(this.yongshiTable.gameObject, this.yongshiTemplate)
            node:SetActive(true)
            CommonDefs.GetComponent_Component_Type(node.transform:Find("NameLabel"), typeof(UILabel)).text = data.playerName
            CommonDefs.GetComponent_Component_Type(node.transform:Find("ScoreLabel"), typeof(UILabel)).text = tostring(data.score)
            CommonDefs.GetComponent_Component_Type(node.transform:Find("KillLabel"), typeof(UILabel)).text = tostring(data.killNum)
            CommonDefs.GetComponent_Component_Type(node.transform:Find("NumLabel"), typeof(UILabel)).text = tostring(data.getNum)
            if not data.isAlive then
                CommonDefs.GetComponent_Component_Type(node.transform:Find("NameLabel"), typeof(UILabel)).color = Color.grey
                CommonDefs.GetComponent_Component_Type(node.transform:Find("ScoreLabel"), typeof(UILabel)).color = Color.grey
                CommonDefs.GetComponent_Component_Type(node.transform:Find("KillLabel"), typeof(UILabel)).color = Color.grey
                CommonDefs.GetComponent_Component_Type(node.transform:Find("NumLabel"), typeof(UILabel)).color = Color.grey
            end
            i = i + 1
        end
    end

    do
        local i = 0
        while i < CSpringFestivalMgr.Instance.nianShouDataList.Count do
            local data = CSpringFestivalMgr.Instance.nianShouDataList[i]
            local node = NGUITools.AddChild(this.nianshouTable.gameObject, this.nianshouTemplate)
            node:SetActive(true)
            CommonDefs.GetComponent_Component_Type(node.transform:Find("NameLabel"), typeof(UILabel)).text = data.playerName
            CommonDefs.GetComponent_Component_Type(node.transform:Find("ScoreLabel"), typeof(UILabel)).text = tostring(data.score)
            CommonDefs.GetComponent_Component_Type(node.transform:Find("KillLabel"), typeof(UILabel)).text = tostring(data.killNum)
            if not data.isAlive then
                CommonDefs.GetComponent_Component_Type(node.transform:Find("NameLabel"), typeof(UILabel)).color = Color.grey
                CommonDefs.GetComponent_Component_Type(node.transform:Find("ScoreLabel"), typeof(UILabel)).color = Color.grey
                CommonDefs.GetComponent_Component_Type(node.transform:Find("KillLabel"), typeof(UILabel)).color = Color.grey
            end
            if i % 2 == 0 then
                CommonDefs.GetComponent_Component_Type(node.transform:Find("Bg"), typeof(UISprite)).alpha = 0
            else
                CommonDefs.GetComponent_Component_Type(node.transform:Find("Bg"), typeof(UISprite)).alpha = 1
            end
            i = i + 1
        end
    end

    this.yongshiTable:Reposition()
    this.yongshiScrollView:ResetPosition()
    this.nianshouTable:Reposition()
    this.nianshouScrollView:ResetPosition()

    this.yongshiResultLabel.text = (tostring(CSpringFestivalMgr.Instance.yongShiCount) .. "/") .. tostring(CSpringFestivalMgr.Instance.yongShiMaxCount)
    this.nianshouResultLabel.text = (tostring(CSpringFestivalMgr.Instance.nianShouCount) .. "/") .. tostring(CSpringFestivalMgr.Instance.nianShouMaxCount)
end
