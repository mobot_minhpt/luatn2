-- Auto Generated!!
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CEquipment = import "L10.Game.CEquipment"
local CItem = import "L10.Game.CItem"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local Color = import "UnityEngine.Color"
local CommonDefs = import "L10.Game.CommonDefs"
local CShopMallGoodsItem = import "L10.UI.CShopMallGoodsItem"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
--local Fashion_Fashion = import "L10.Game.Fashion_Fashion"
local IdPartition = import "L10.Game.IdPartition"
local LocalString = import "LocalString"
local UIEventListener = import "UIEventListener"
--local ZuoQi_ZuoQi = import "L10.Game.ZuoQi_ZuoQi"
local Baby_Fashion = import "L10.Game.Baby_Fashion"
local EnumGender = import "L10.Game.EnumGender"
local CFashionMgr = import "L10.Game.CFashionMgr"
CShopMallGoodsItem.m_UpdateData_UInt32_UInt32_String_Int32_Boolean_Boolean_CS2LuaHook = function (this, templateId, price, discountInfo, avalibleCount, showItemInfo, need)

    this.templateId = templateId
    this.showItemInfo = showItemInfo
    local item = CItemMgr.Inst:GetItemTemplate(templateId)
    
    this.m_Texture:LoadMaterial(item.Icon)
    this.m_NameLabel.Text = item.Name
    if this.m_PriceLabel ~= nil then
        this.m_PriceLabel.Text = tostring(price)
    end
    if this.m_DiscountLabel ~= nil then
        if not System.String.IsNullOrEmpty(discountInfo) then
            this.m_DiscountLabel.Text = discountInfo
            this.m_DiscountLabel.gameObject:SetActive(true)
        else
            this.m_DiscountLabel.gameObject:SetActive(false)
        end
    end
    if this.m_SoldoutSprite ~= nil then
        this.m_SoldoutSprite.gameObject:SetActive(false)
    end

    if avalibleCount >= 0 then
        this.m_AvalibleCountLabel.Text = System.String.Format(LocalString.GetString("限购{0}"), avalibleCount)
        this.m_AvalibleCountLabel.gameObject:SetActive(true)
        if this.OOSSprite ~= nil then
            this.OOSSprite.enabled = true
        end
        if avalibleCount == 0 then
            this:MarkedAsSoldout()
        end
    else
        this.m_AvalibleCountLabel.gameObject:SetActive(false)
        if this.OOSSprite ~= nil then
            this.OOSSprite.enabled = false
        end
    end
    if this.disableSprite ~= nil then
        this.disableSprite.enabled = false
        this:UpdateDisableStatus()
    end
    if this.needTag ~= nil then
        this.needTag:SetActive(need)
    end
    local ownRoot = this.transform:Find("Owner")
    if ownRoot then
        local data = Expression_Appearance.GetDataBySubKey("ItemId",item.ID)
        local isSan = false
        if data then
            local kaiSanAppearanceGroup =  tonumber(Fashion_Setting.GetData("FashionPreview_UmbrellaGroup").Value)
            isSan = kaiSanAppearanceGroup == data.Group
        end
        local lvJingData = PaiZhao_LvJing.GetDataBySubKey("Locked",item.ID)
        local isLvJing = false
        if lvJingData then
            isLvJing = true
        end
        ownRoot.gameObject:SetActive(LuaShopMallFashionPreviewMgr.m_OwnedShopItems[templateId] and (isLvJing or isSan or LuaShopMallFashionPreviewMgr.m_OwnedLockGroupItemIds[templateId]))
    end

    local transformableIcon = this.transform:Find("Transformable")
    if transformableIcon ~= nil then
	    transformableIcon.gameObject:SetActive(CLuaShopMallMgr:IsShowTransformableFashionIcon(templateId))
    end
end
CShopMallGoodsItem.m_UpdateDiscountColor_CS2LuaHook = function (this, color)
    if this.m_DiscountLabel ~= nil then
        this.m_DiscountLabel.Color = color
    end
    if this.upSprite ~= nil then
        this.upSprite.enabled = CommonDefs.op_Equality_Color_Color(color, Color.red)
    end
    if this.downSprite ~= nil then
        this.downSprite.enabled = CommonDefs.op_Equality_Color_Color(color, Color.green)
    end
end
CShopMallGoodsItem.m_OnEnable_CS2LuaHook = function (this)
    EventManager.AddListener(EnumEventType.MainPlayerLevelChange, MakeDelegateFromCSFunction(this.UpdateDisableStatus, Action0, this))
    UIEventListener.Get(this.m_Texture.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        if LuaWishMgr.WishShop then
          if this.OnClickItemTexture ~= nil then
            invoke(this.OnClickItemTexture)
          end
          return
        end
        local item = CItemMgr.Inst:GetItemTemplate(this.templateId)
        if item.Type == EnumItemType_lua.Fashion then
            local data = Fashion_Fashion.GetDataBySubKey("ItemID",item.ID)
            if data then
                local fashionId = data.ID
                if fashionId ~= 0 then
                    LuaShopMallFashionPreviewMgr:ShowFashionPreview({[data.Position] = fashionId})
                end
            end
        elseif item.Type == EnumItemType_lua.Mount then
            local data = ZuoQi_ZuoQi.GetDataBySubKey("ItemID",item.ID)
            if data then
                local vehicleId = data.ID
                if vehicleId ~= 0 then
                    LuaShopMallFashionPreviewMgr:ShowZuiQiPreview(vehicleId)
                end
            end
        elseif item.Type == EnumItemType_lua.Other then
            local data = Baby_Fashion.GetDataBySubKey("ItemID",item.ID)
            if data then
                local babyFashionId = data.ID
                if babyFashionId ~= 0 then
                    LuaBabyMgr.ShowBabyFashionPreview(babyFashionId)
                end
            end
            data = Expression_Appearance.GetDataBySubKey("ItemId",item.ID)
            local kaiSanAppearanceGroup =  tonumber(Fashion_Setting.GetData("FashionPreview_UmbrellaGroup").Value)
            if data and data.Group == kaiSanAppearanceGroup then
                local expression_Appearance_PreviewDefine = data.PreviewDefine
                LuaShopMallFashionPreviewMgr:ShowUmbrellaPreview(expression_Appearance_PreviewDefine)
            end
        elseif item.Type == EnumItemType_lua.GiftBag then
            local template = HouseCompetition_HouseTemplate.GetData(item.ID)
            if template then
                local zswTemplateId = template.ZswID
                if zswTemplateId ~= 0 then
                    CLuaZswTemplateMgr.ShowZswTemplatePreview(zswTemplateId)
                end
            end
        elseif item.Type == EnumItemType_lua.Chest then
            local blindBoxData = Item_BlindBox.GetData(item.ID)
            if blindBoxData then
                LuaShopMallFashionPreviewMgr:ShowBlindBox(item.ID,blindBoxData)
            end
            local previewChestData = Item_PreviewChest.GetData(item.ID)
            if previewChestData then
                LuaShopMallFashionPreviewMgr:ShowPreviewChest(previewChestData)
            end
        end
        if this.showItemInfo then
            CItemInfoMgr.ShowLinkItemTemplateInfo(this.templateId, false, nil, AlignType.Default, 0, 0, 0, 0)
        end
        if this.OnClickItemTexture ~= nil then
            invoke(this.OnClickItemTexture)
        end
    end)
end
CShopMallGoodsItem.m_UpdateDisableStatus_CS2LuaHook = function (this)
    if this.disableSprite ~= nil then
        if IdPartition.IdIsItem(this.templateId) then
            this.disableSprite.enabled = not CItem.GetMainPlayerIsFit(this.templateId, true)
        elseif IdPartition.IdIsEquip(this.templateId) then
            this.disableSprite.enabled = not CEquipment.GetMainPlayerIsFit(this.templateId, true)
        end
    end
end
