-- Auto Generated!!
local CGaoChangMgr = import "L10.Game.CGaoChangMgr"
local CGaoChangResultWnd = import "L10.UI.CGaoChangResultWnd"
local Extensions = import "Extensions"
local L10 = import "L10"
local NGUITools = import "NGUITools"
local UILabel = import "UILabel"
local UISprite = import "UISprite"
CGaoChangResultWnd.m_Awake_CS2LuaHook = function (this) 
    if CGaoChangResultWnd.Instance ~= nil then
        L10.CLogMgr.LogError("more than one instance running!")
    end
    CGaoChangResultWnd.Instance = this
end
CGaoChangResultWnd.m_Init_CS2LuaHook = function (this) 
    this.templateNode:SetActive(false)
    Extensions.RemoveAllChildren(this.table.transform)

    local totalInfo = CGaoChangMgr.Inst:getTotalInfo()
    --table.initTable(table.gameObject, templateNode, uiScrollView, totalInfo);
    do
        local i = 0
        while i < totalInfo.Count do
            local info = totalInfo[i]
            local instance = NGUITools.AddChild(this.table.gameObject, this.templateNode)
            instance:SetActive(true)
            CommonDefs.GetComponent_Component_Type(instance.transform:Find("NameLabel"), typeof(UILabel)).text = info.name
            CommonDefs.GetComponent_Component_Type(instance.transform:Find("KillLabel"), typeof(UILabel)).text = tostring(info.kill_num)
            CommonDefs.GetComponent_Component_Type(instance.transform:Find("BeKilledLabel"), typeof(UILabel)).text = tostring(info.bekilled_num)
            if i % 2 == 1 then
                CommonDefs.GetComponent_GameObject_Type(instance.transform:Find("Bg").gameObject, typeof(UISprite)).alpha = 0
            end
            i = i + 1
        end
    end
    this.table:Reposition()
    this.uiScrollView:ResetPosition()

    local selfInfo = CGaoChangMgr.Inst:getSelfInfo()
    if selfInfo ~= nil then
        Extensions.RemoveAllChildren(this.selfInfoNode.transform)
        local instance = NGUITools.AddChild(this.selfInfoNode, this.templateNode)
        instance:SetActive(true)
        CommonDefs.GetComponent_Component_Type(instance.transform:Find("NameLabel"), typeof(UILabel)).text = selfInfo.name
        CommonDefs.GetComponent_Component_Type(instance.transform:Find("KillLabel"), typeof(UILabel)).text = tostring(selfInfo.kill_num)
        CommonDefs.GetComponent_Component_Type(instance.transform:Find("BeKilledLabel"), typeof(UILabel)).text = tostring(selfInfo.bekilled_num)
        instance.transform:Find("Bg").gameObject:SetActive(false)
    end
end
