local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local IdPartition = import "L10.Game.IdPartition"
local Extensions = import "Extensions"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CQMPKWordsListTemplate = import "L10.UI.CQMPKWordsListTemplate"
local CQMPKTitleTemplate = import "L10.UI.CQMPKTitleTemplate"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
local CBodyEquipSlot = import "L10.UI.CBodyEquipSlot"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CCommonItem = import "L10.Game.CCommonItem"

CLuaQMPKEquipmentConfig = class()
RegistClassMember(CLuaQMPKEquipmentConfig,"m_EquipmentFrame")
RegistClassMember(CLuaQMPKEquipmentConfig,"m_CurrentEquipment")
RegistClassMember(CLuaQMPKEquipmentConfig,"m_CurrentEquipmentName")
RegistClassMember(CLuaQMPKEquipmentConfig,"m_ConfirmBtn")
RegistClassMember(CLuaQMPKEquipmentConfig,"m_WordsTable")
RegistClassMember(CLuaQMPKEquipmentConfig,"m_TitleTemplate")
RegistClassMember(CLuaQMPKEquipmentConfig,"m_WordsTemplate")
RegistClassMember(CLuaQMPKEquipmentConfig,"m_Equipment2TemplateDict")
RegistClassMember(CLuaQMPKEquipmentConfig,"m_Equipment2GemDict")
RegistClassMember(CLuaQMPKEquipmentConfig,"m_IsInited")
RegistClassMember(CLuaQMPKEquipmentConfig,"m_Titles")
RegistClassMember(CLuaQMPKEquipmentConfig,"m_Words")
RegistClassMember(CLuaQMPKEquipmentConfig,"m_CurrentEquipmentTemplateId")
RegistClassMember(CLuaQMPKEquipmentConfig,"m_CurrentEquipmentGemGroupId")
RegistClassMember(CLuaQMPKEquipmentConfig,"m_CurrentEquipmentIdentifySkillGroupId")
RegistClassMember(CLuaQMPKEquipmentConfig,"m_AttributeType")
RegistClassMember(CLuaQMPKEquipmentConfig,"m_TemplateIndex")
RegistClassMember(CLuaQMPKEquipmentConfig,"m_CurrentItemTemplateId")
RegistClassMember(CLuaQMPKEquipmentConfig,"m_CurrentItemId")

--新添加鉴定技能相关
RegistClassMember(CLuaQMPKEquipmentConfig,"m_Equipment2IdentifySkillDict")
RegistClassMember(CLuaQMPKEquipmentConfig,"m_CurrentEquipmentAuthenticatingId")
RegistClassMember(CLuaQMPKEquipmentConfig,"m_ScrollViews")

--他人配置相关
RegistClassMember(CLuaQMPKEquipmentConfig,"m_ConfirmBtnLab")
RegistClassMember(CLuaQMPKEquipmentConfig,"m_TipLab")
RegistClassMember(CLuaQMPKEquipmentConfig,"m_OtherTipLab")
RegistClassMember(CLuaQMPKEquipmentConfig,"m_OtherAlertTipLab")
RegistClassMember(CLuaQMPKEquipmentConfig,"m_IsOtherPlayer")
RegistClassMember(CLuaQMPKEquipmentConfig,"m_OtherPlayerInfo")
RegistClassMember(CLuaQMPKEquipmentConfig,"m_Class")

RegistClassMember(CLuaQMPKEquipmentConfig,"m_WordsGo")
RegistClassMember(CLuaQMPKEquipmentConfig,"m_NoneLabelGo")
RegistClassMember(CLuaQMPKEquipmentConfig,"m_HasChanged")
function CLuaQMPKEquipmentConfig:Awake()
    self.m_IsOtherPlayer = CLuaQMPKMgr.s_IsOtherPlayer
    self.m_OtherPlayerInfo = CLuaQMPKMgr.s_PlayerInfo
    if self.m_IsOtherPlayer then
        self.m_Class = CLuaQMPKMgr.s_PlayerCls
    else
        self.m_Class = EnumToInt(CClientMainPlayer.Inst.Class)
    end

    local script=self.transform:Find("PlayerEquipmentView/EquipmentFrame"):GetComponent(typeof(CCommonLuaScript))
    script:Awake()
    self.m_EquipmentFrame = script.m_LuaSelf

    self.m_CurrentEquipment = self.transform:Find("CurrentEquipment"):GetComponent(typeof(CBodyEquipSlot))
    self.m_CurrentEquipmentName = self.transform:Find("CurrentEquipmentName"):GetComponent(typeof(UILabel))
    self.m_ConfirmBtn = self.transform:Find("ConfirmBtn").gameObject
    self.m_WordsTable = self.transform:Find("Words/WordsTable"):GetComponent(typeof(UITable))
    self.m_TitleTemplate = self.transform:Find("Words/TitleTemplate").gameObject
    self.m_WordsTemplate = self.transform:Find("Words/WordTemplate").gameObject
    self.m_WordsTemplate.transform:Find("Scrollview/PureTextLabel").gameObject:SetActive(false)
    self.m_WordsTemplate.transform:Find("Scrollview/Word").gameObject:SetActive(false)
    self.m_TipLab = self.transform:Find("Label"):GetComponent(typeof(UILabel))
    self.m_OtherTipLab = self.transform:Find("CompareTipLab"):GetComponent(typeof(UILabel))
    self.m_OtherAlertTipLab = self.transform:Find("AlertLab"):GetComponent(typeof(UILabel))
    self.m_ConfirmBtnLab = self.transform:Find("ConfirmBtn/Label"):GetComponent(typeof(UILabel))
    self.m_WordsGo = self.transform:Find("Words/WordsTable").gameObject
    self.m_NoneLabelGo = self.transform:Find("NoneLabel").gameObject

    self.m_CurrentEquipmentGemGroupId = 0
    self.m_AttributeType = 0
    self.m_HasChanged = false
end

function CLuaQMPKEquipmentConfig:OnEnable( )
    g_ScriptEvent:AddListener("QMPKEquipmentClick", self, "OnQMPKEquipmentClick")

    if self.m_IsInited then
        return
    end
    self.m_IsInited = true
    self:Init()

    UIEventListener.Get(self.m_CurrentEquipment.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnIconClick(go)
    end)
end
function CLuaQMPKEquipmentConfig:OnDisable()
    g_ScriptEvent:RemoveListener("QMPKEquipmentClick", self, "OnQMPKEquipmentClick")
end

function CLuaQMPKEquipmentConfig:OnPageCloseOrChange(finalFunc)
    if self.m_HasChanged then
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("QMPK_ConfigSave_Confirm"), function()
            self:OnConfirmBtnClick(self.m_ConfirmBtn)
            if finalFunc then finalFunc() end
        end, function()
            if finalFunc then finalFunc() end
        end, nil, nil, false)
        self.m_HasChanged = false
        return true
    else
        if finalFunc then finalFunc() end
    end
    return false
end

function CLuaQMPKEquipmentConfig:FilterSkillGroupIDs(_skill_group_str)
    local return_val = {}

    local player_class = self.m_Class
    for i=1,_skill_group_str.Length do
        local skill_id = _skill_group_str[i - 1]
        local skill_class = math.floor(skill_id / 100)
        if(QuanMinPK_IdentifySkillClass.Exists(skill_class))then
            local need_class = QuanMinPK_IdentifySkillClass.GetData(skill_class).ClassInfo

            if(player_class == need_class)then

                table.insert(return_val, skill_id)
            end
        else
            table.insert(return_val, skill_id)
        end
    end
    return return_val
end

function CLuaQMPKEquipmentConfig:Init( )
    self.m_Titles = CreateFromClass(MakeGenericClass(List, CQMPKTitleTemplate))
    self.m_Words = CreateFromClass(MakeGenericClass(List, CQMPKWordsListTemplate))
    self.m_ScrollViews = {}

    self.m_Equipment2TemplateDict ={}-- CreateFromClass(MakeGenericClass(Dictionary, UInt32, MakeGenericClass(List, CQMPKEquipmentTemplate)))
    do
        local keys ={}
        QuanMinPK_Equipment.ForeachKey(function (key)
            table.insert( keys,key)
        end)
        for i,v in ipairs(keys) do
            local template = QuanMinPK_Equipment.GetData(v)
            if EnumToInt(self.m_Class) == template.Class then

                if not self.m_Equipment2TemplateDict[template.TemplateId] then
                    self.m_Equipment2TemplateDict[template.TemplateId]={}
                end
                table.insert(self.m_Equipment2TemplateDict[template.TemplateId],{m_Id = v,m_Words = template.Words,m_Description = template.Description})
            end
        end
    end

    self.m_Equipment2GemDict ={}

    do
        local keys ={}
        QuanMinPK_Gem.ForeachKey(function (key)
            table.insert( keys,key )
        end)

        for i,v in ipairs(keys) do
            local template = QuanMinPK_Gem.GetData(v)
            self.m_Equipment2GemDict[v]={m_GemGroupId=template.GemGroupIdStr,m_Description=template.Description}
        end
    end

    --鉴定技能
    self.m_Equipment2IdentifySkillDict = {}
    do
        local keys ={}
        QuanMinPK_IdentifySkill.ForeachKey(function (key)
            table.insert( keys,key )
        end)

        for i,v in ipairs(keys) do
            local template = QuanMinPK_IdentifySkill.GetData(v)
            local filtered_skill_group = self:FilterSkillGroupIDs(template.IdentifySkillGroupIdStr)

            self.m_Equipment2IdentifySkillDict[v]={m_IdentifySkillGroupId = filtered_skill_group, m_Description = template.Description}
        end
    end

    UIEventListener.Get(self.m_ConfirmBtn).onClick =  DelegateFactory.VoidDelegate(function(go)
        self:OnConfirmBtnClick(go)
    end)
    --增加鉴定词条 i递增至2
    CommonDefs.ListClear(self.m_Titles)
    Extensions.RemoveAllChildren(self.m_WordsTable.transform)
    self.m_TitleTemplate:SetActive(false)
    self.m_WordsTemplate:SetActive(false)
    for i = 0, 2 do
        local go = NGUITools.AddChild(self.m_WordsTable.gameObject, self.m_TitleTemplate)
        UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function(go)
            self:OnTitleClick(go)
        end)

        go:SetActive(true)
        local title = CommonDefs.GetComponent_GameObject_Type(go, typeof(CQMPKTitleTemplate))
        if nil ~= title then
            CommonDefs.ListAdd(self.m_Titles, typeof(CQMPKTitleTemplate), title)
            title:Expand(i == 0)
        end
        go = NGUITools.AddChild(self.m_WordsTable.gameObject, self.m_WordsTemplate)
        go:SetActive(i == 0)
        local word = CommonDefs.GetComponent_GameObject_Type(go, typeof(CQMPKWordsListTemplate))
        if nil ~= word then
            CommonDefs.ListAdd(self.m_Words, typeof(CQMPKWordsListTemplate), word)
        end
        local _scroll_view = go.transform:Find("Scrollview").gameObject
        table.insert(self.m_ScrollViews, _scroll_view)
    end
    local defaultItemId
    if self.m_IsOtherPlayer then
        local list = {}
        for k, v in pairs(self.m_OtherPlayerInfo.Pos2Equip) do
            table.insert(list, {Pos = k, ItemInfo = v})
        end
        table.sort(list, function(a, b)
			return a.Pos<b.Pos
		end)
        if #list <= 0 or not IdPartition.IdIsEquip(list[1].ItemInfo.TemplateId) or IdPartition.IdIsTalisman(list[1].ItemInfo.TemplateId) then
            self.m_CurrentEquipmentName.text = ""
            do
                local i = 0 local cnt = self.m_Words.Count
                while i < cnt do
                    self.m_Words[i]:Init(nil)
                    i = i + 1
                end
            end
            self:InitNone(true)
            return
        end
        defaultItemId = list[1].ItemInfo.Id
    else
        local list = CItemMgr.Inst:GetPlayerEquipmentListAtPlace(EnumItemPlace.Body)
        if list.Count <= 0 or not IdPartition.IdIsEquip(list[0].templateId) or IdPartition.IdIsTalisman(list[0].templateId) then
            self.m_CurrentEquipmentName.text = ""
            do
                local i = 0 local cnt = self.m_Words.Count
                while i < cnt do
                    self.m_Words[i]:Init(nil)
                    i = i + 1
                end
            end
            self:InitNone(true)
            return
        end
        defaultItemId = list[0].itemId
    end
    self:OnEquipmentClick(defaultItemId)

    self.m_NoneLabelGo:SetActive(false)
    if self.m_IsOtherPlayer then
        self:InitOtherPlayerConfig()
    else
        self.m_OtherTipLab.gameObject:SetActive(false)
    end
end
function CLuaQMPKEquipmentConfig:OnTitleClick( go)
    local oldTitle = self.m_AttributeType
    do
        local i = 0 local cnt = self.m_Titles.Count
        while i < cnt do
            local isEqual = go == self.m_Titles[i].gameObject
            if isEqual then
                self.m_AttributeType = i
            end
            self.m_Words[i].gameObject:SetActive(isEqual)
            -- if(self.m_AttributeType == 2)then
            --     self.m_ScrollViews[3].gameObject:SetActive(isEqual)
            -- end
            self.m_Titles[i]:Expand(isEqual)
            i = i + 1
        end
    end
    if oldTitle == self.m_AttributeType then
        self:ShowPopupMenu()
    end
    self.m_WordsTable:Reposition()
end
function CLuaQMPKEquipmentConfig:ShowPopupMenu( )
    if self.m_IsOtherPlayer then
        return
    end

    local popupList ={}
    local max_column_num = 2
    local button_width = 296
    if self.m_AttributeType == 0 then
        if self.m_Equipment2TemplateDict[self.m_CurrentItemTemplateId] then
            local list=self.m_Equipment2TemplateDict[self.m_CurrentItemTemplateId]
            for i,v in ipairs(list) do
                local data = PopupMenuItemData(v.m_Description, DelegateFactory.Action_int(function(index) self:OnWordClick(index) end),false, nil, EnumPopupMenuItemStyle.Light)
                table.insert( popupList,data)
            end
        end
    elseif self.m_AttributeType == 1 then
        if self.m_Equipment2GemDict[self.m_CurrentItemTemplateId] then
            local equip2gem=self.m_Equipment2GemDict[self.m_CurrentItemTemplateId]
            local groupIds =equip2gem.m_GemGroupId
            for i=1,groupIds.Length do
                local data = PopupMenuItemData(equip2gem.m_Description[i-1], DelegateFactory.Action_int(function(index) self:OnWordClick(index) end), false, nil, EnumPopupMenuItemStyle.Light)
                table.insert( popupList,data)
            end
        end
    else
        if self.m_Equipment2IdentifySkillDict[self.m_CurrentItemTemplateId]then
            --max_column_num = 1
            button_width = 310
            local equip2identifyskill = self.m_Equipment2IdentifySkillDict[self.m_CurrentItemTemplateId]
            local groupIds = equip2identifyskill.m_IdentifySkillGroupId
            for k, v in ipairs(groupIds) do

                local identify_skill = Skill_AllSkills.GetData(v)
                local _name = Extensions.ConvertToChineseString(identify_skill.Level) .. LocalString.GetString("级") .. identify_skill.Name
                local data = PopupMenuItemData(_name, DelegateFactory.Action_int(function(index) self:OnWordClick(index) end), false, nil, EnumPopupMenuItemStyle.Light)
                table.insert( popupList,data)
            end
        end
    end
    CPopupMenuInfoMgr.ShowPopupMenu(Table2Array(popupList,MakeArrayClass(PopupMenuItemData)), self.m_Titles[self.m_AttributeType].transform, CPopupMenuInfoMgr.AlignType.Bottom, #popupList >= 8 and max_column_num or 1, nil, 430, true, button_width)
end
function CLuaQMPKEquipmentConfig:OnWordClick( index)
    if self.m_AttributeType == 0 then
        local equipment2template=self.m_Equipment2TemplateDict[self.m_CurrentItemTemplateId]
        if equipment2template and #equipment2template > index then
            if self.m_CurrentEquipmentTemplateId ~= equipment2template[index+1].m_Id then self.m_HasChanged = true end
            self.m_Titles[0]:Init(LocalString.GetString("[AAFFFF]词条[-]"))
            self.m_Titles[0].transform:Find("ExtraName"):GetComponent(typeof(UILabel)).text = equipment2template[index+1].m_Description
            self.m_Words[0]:Init(equipment2template[index+1].m_Words)
            self.m_CurrentEquipmentTemplateId = equipment2template[index+1].m_Id
        end
    elseif self.m_AttributeType == 1 then
        local equip2gem=self.m_Equipment2GemDict[self.m_CurrentItemTemplateId]
        if equip2gem and equip2gem.m_GemGroupId.Length > index then
            if self.m_CurrentEquipmentGemGroupId ~= equip2gem.m_GemGroupId[index] then self.m_HasChanged = true end
            local gem = GemGroup_GemGroup.GetData(equip2gem.m_GemGroupId[index])
            self.m_Titles[1]:Init(LocalString.GetString("[AAFFFF]石之灵[-]"))
            self.m_Titles[1].transform:Find("ExtraName"):GetComponent(typeof(UILabel)).text = Extensions.ConvertToChineseString(gem.Level) .. LocalString.GetString("级") .. gem.Name
            self.m_Words[1]:Init(gem.Effect)
            self.m_CurrentEquipmentGemGroupId =equip2gem.m_GemGroupId[index]
        end
    else
        local equip2identifyskill=self.m_Equipment2IdentifySkillDict[self.m_CurrentItemTemplateId]
        if equip2identifyskill and (#equip2identifyskill.m_IdentifySkillGroupId) > index then
            if self.m_CurrentEquipmentIdentifySkillGroupId ~= equip2identifyskill.m_IdentifySkillGroupId[index + 1] then self.m_HasChanged = true end
            local identify_skill = Skill_AllSkills.GetData(equip2identifyskill.m_IdentifySkillGroupId[index + 1])
            --鉴定技能的细节描述与其他类型不同，使用纯文本
            local _table = self.m_ScrollViews[3].transform:Find("Table").gameObject
            local _text = self.m_ScrollViews[3].transform:Find("PureTextLabel").gameObject
            Extensions.RemoveAllChildren(_table.transform)
            local _new_text = NGUITools.AddChild(_table, _text):GetComponent(typeof(UILabel))
            _new_text.gameObject:SetActive(true)

            self.m_Titles[2]:Init(LocalString.GetString("[AAFFFF]鉴定技能[-]"))
            self.m_Titles[2].transform:Find("ExtraName"):GetComponent(typeof(UILabel)).text = Extensions.ConvertToChineseString(identify_skill.Level) .. LocalString.GetString("级") .. LocalString.GetString(identify_skill.Name)
            --_new_text.text = identify_skill.TranslatedDisplay
            _new_text.text = SafeStringFormat3(LocalString.GetString("[c][ffff00]【%s·%d级】[-]%s[/c]"),
            identify_skill.Name,
            identify_skill.Level,
            identify_skill.TranslatedDisplay
            )
            _table:GetComponent(typeof(UITable)):Reposition()
            self.m_CurrentEquipmentIdentifySkillGroupId = equip2identifyskill.m_IdentifySkillGroupId[index + 1]
        end
    end
end
function CLuaQMPKEquipmentConfig:OnEquipmentClick( itemId)
    self.m_CurrentItemId = itemId
    local item 
    if self.m_IsOtherPlayer then
        local equip = self.m_OtherPlayerInfo.ItemId2Equip[itemId]
        item = CreateFromClass(CCommonItem, equip)
    else
        item = CItemMgr.Inst:GetById(itemId)
    end
    self.m_CurrentItemTemplateId = item.TemplateId
    self.m_CurrentEquipmentName.text = item.ColoredName
    self.m_CurrentEquipment:Init(item.Icon, item.Equip.Color, false, item.Equip, 0)
    if self.m_Equipment2TemplateDict[item.TemplateId] then
        if item.Equip.ExtraData.Data.varbinary ~= nil then
            local templateId = math.floor(tonumber(item.Equip.ExtraData.Data.varbinary.StringData or 0))
            local equip = QuanMinPK_Equipment.GetData(templateId)
            if equip ~= nil then
                self.m_Titles[0]:Init(LocalString.GetString("[AAFFFF]词条[-]"))
                self.m_Titles[0].transform:Find("ExtraName"):GetComponent(typeof(UILabel)).text = equip.Description
                self.m_Words[0]:Init(equip.Words)
                self.m_CurrentEquipmentTemplateId = templateId
            end
        else
            self.m_Titles[0]:Init(LocalString.GetString("[AAFFFF]词条[-]"))
            self.m_Titles[0].transform:Find("ExtraName"):GetComponent(typeof(UILabel)).text = self.m_Equipment2TemplateDict[item.TemplateId][0+1].m_Description
            self.m_Words[0]:Init(self.m_Equipment2TemplateDict[item.TemplateId][0+1].m_Words)
            self.m_CurrentEquipmentTemplateId =self.m_Equipment2TemplateDict[self.m_CurrentItemTemplateId][1].m_Id
        end
    end
    if self.m_Equipment2GemDict[item.TemplateId] then
        local gem = GemGroup_GemGroup.GetData(item.Equip.GemGroupId)
        self.m_Titles[1].gameObject:SetActive(true)
        self.m_Words[1].gameObject:SetActive(self.m_AttributeType == 1)
        self.m_Titles[1]:Init(LocalString.GetString("[AAFFFF]石之灵[-]"))
        self.m_Titles[1].transform:Find("ExtraName"):GetComponent(typeof(UILabel)).text = Extensions.ConvertToChineseString(gem.Level) .. LocalString.GetString("级") .. gem.Name
        self.m_Words[1]:Init(gem.Effect)
        self.m_CurrentEquipmentGemGroupId = item.Equip.GemGroupId
    else
        local new_active_id = (self.m_AttributeType == 1) and 0 or self.m_AttributeType
        self.m_Titles[1].gameObject:SetActive(false)
        self.m_Words[1].gameObject:SetActive(false)
        self.m_Words[new_active_id].gameObject:SetActive(true)
        self.m_AttributeType = new_active_id
        self.m_Titles[new_active_id]:Expand(true)
    end
    --鉴定技能
    do
        local identify_skill = Skill_AllSkills.GetData(item.Equip.IdentifySkillId)
        if self.m_Equipment2IdentifySkillDict[item.TemplateId] and identify_skill ~= nil then


            self.m_Titles[2].gameObject:SetActive(true)
            --鉴定技能的细节描述与其他类型不同，使用纯文本
            self.m_Words[2].gameObject:SetActive(true)
            self.m_ScrollViews[3].gameObject:SetActive(true)
            local _table = self.m_ScrollViews[3].transform:Find("Table").gameObject
            local _text = self.m_ScrollViews[3].transform:Find("PureTextLabel").gameObject
            Extensions.RemoveAllChildren(_table.transform)
            local _new_text = NGUITools.AddChild(_table, _text):GetComponent(typeof(UILabel))
            _new_text.gameObject:SetActive(true)

            self.m_Titles[2]:Init(LocalString.GetString("[AAFFFF]鉴定技能[-]"))
            self.m_Titles[2].transform:Find("ExtraName"):GetComponent(typeof(UILabel)).text = Extensions.ConvertToChineseString(identify_skill.Level) .. LocalString.GetString("级") .. identify_skill.Name
            --_new_text.text = identify_skill.TranslatedDisplay
            _new_text.text = SafeStringFormat3(LocalString.GetString("[c][ffff00]【%s·%d级】[-]%s[/c]"),
            identify_skill.Name,
            identify_skill.Level,
            identify_skill.TranslatedDisplay
            )
            _table:GetComponent(typeof(UITable)):Reposition()
            self.m_Words[2].gameObject:SetActive(self.m_AttributeType == 2)
            self.m_CurrentEquipmentIdentifySkillGroupId = item.Equip.IdentifySkillId
        else
            local new_active_id = (self.m_AttributeType == 2) and 0 or self.m_AttributeType
            self.m_Titles[2].gameObject:SetActive(false)
            self.m_Words[2].gameObject:SetActive(false)
            --self.m_ScrollViews[3].gameObject:SetActive(false)
            self.m_Words[new_active_id].gameObject:SetActive(true)
            self.m_AttributeType = new_active_id
            self.m_Titles[new_active_id]:Expand(true)
        end
    end
    self.m_WordsTable:Reposition()
end
function CLuaQMPKEquipmentConfig:OnConfirmBtnClick( go)
    if System.String.IsNullOrEmpty(self.m_CurrentItemId) then
        return
    end
    local pos, itemId, ownItem
    if self.m_IsOtherPlayer then
        pos = self.m_OtherPlayerInfo.ItemId2Pos[self.m_CurrentItemId]
        local itemProp = CClientMainPlayer.Inst.ItemProp
        itemId = itemProp:GetItemAt(EnumItemPlace.Body, pos)
        if itemId == nil then
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("当前并未穿戴对应装备"))
            return
        end
        ownItem = CItemMgr.Inst:GetById(itemId)
        local otherItem = self.m_OtherPlayerInfo.ItemId2Equip[self.m_CurrentItemId]
        if ownItem and otherItem and ownItem.TemplateId ~= otherItem.TemplateId then
            g_MessageMgr:ShowMessage("QMPK_EquipmentConfig_LearnNeedSameEquip")
            return
        end
        if ownItem and math.floor(tonumber(ownItem.Equip.ExtraData.Data.varbinary.StringData or 0)) == self.m_CurrentEquipmentTemplateId and (ownItem.Equip.GemGroupId == 0 or ownItem.Equip.GemGroupId == self.m_CurrentEquipmentGemGroupId) and (ownItem.Equip.IdentifySkillId == 0 or ownItem.Equip.IdentifySkillId == self.m_CurrentEquipmentIdentifySkillGroupId) then
            g_MessageMgr:ShowMessage("QMPK_EquipmentConfig_AlreadySet")
            return
        end
    else
        pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Body, self.m_CurrentItemId)
        itemId = self.m_CurrentItemId
        ownItem = CItemMgr.Inst:GetById(itemId)
    end
    Gac2Gas.RequestSetQmpkEquipPropertyAndWord(EnumItemPlace_lua.Body, pos, itemId, self.m_CurrentEquipmentTemplateId)
    Gac2Gas.RequestSetQmpkEquipGem(EnumItemPlace_lua.Body, pos, itemId, self.m_CurrentEquipmentGemGroupId)
    Gac2Gas.RequestSetQmpkEquipIdentifySkill(EnumItemPlace_lua.Body, pos, itemId, self.m_CurrentEquipmentIdentifySkillGroupId)
    self.m_HasChanged = false
end
function CLuaQMPKEquipmentConfig:OnIconClick( go)
    if System.String.IsNullOrEmpty(self.m_CurrentItemId) then
        return
    end

    if self.m_IsOtherPlayer then
        local item = CreateFromClass(CCommonItem, self.m_OtherPlayerInfo.ItemId2Equip[self.m_CurrentItemId])
        CItemInfoMgr.ShowLinkItemInfo(item, true, nil, AlignType.Default, 0, 0, 0, 0, 0)
    else 
        CItemInfoMgr.ShowBodyItemInfo(self.m_CurrentItemId, CItemMgr.Inst:FindItemPosition(EnumItemPlace.Body, self.m_CurrentItemId), nil)
    end
end

function CLuaQMPKEquipmentConfig:OnQMPKEquipmentClick(itemId)
    local finalFunc = function() self:OnEquipmentClick( itemId) end
    if self.m_CurrentItemId ~= itemId then
        self:OnPageCloseOrChange(finalFunc)
    else
        finalFunc()
    end
end

function CLuaQMPKEquipmentConfig:InitNone(isNone)
    self.m_WordsGo:SetActive(not isNone)
    self.m_NoneLabelGo:SetActive(isNone)
    self.m_CurrentEquipment.gameObject:SetActive(not isNone)
    self.m_CurrentEquipmentName.gameObject:SetActive(not isNone)
    self.m_TipLab.gameObject:SetActive(not isNone)
    self.m_OtherTipLab.gameObject:SetActive(not isNone)
    self.m_ConfirmBtn:SetActive(not isNone)
end

-- 他人配置
function CLuaQMPKEquipmentConfig:InitOtherPlayerConfig()
    self.m_ConfirmBtnLab.text = LocalString.GetString("学习此装备")
    self.m_TipLab.gameObject:SetActive(false)
    self.m_OtherTipLab.gameObject:SetActive(true)
    local isSameCls = EnumToInt(CClientMainPlayer.Inst.Class) == CLuaQMPKMgr.s_PlayerCls
    self.m_OtherAlertTipLab.gameObject:SetActive(not isSameCls)
    self.m_ConfirmBtn:SetActive(isSameCls)
end

return CLuaQMPKEquipmentConfig
