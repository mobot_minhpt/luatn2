require("common/common_include")

local UIProgressBar = import "UIProgressBar"
local UITabBar = import "L10.UI.UITabBar"
local UIGrid = import "UIGrid"
local BabyMgr = import "L10.Game.BabyMgr"
local CUITexture = import "L10.UI.CUITexture"
local Baby_BabyExp = import "L10.Game.Baby_BabyExp"
local Baby_YouEr = import "L10.Game.Baby_YouEr"
--local Baby_ShaoNian = import "L10.Game.Baby_ShaoNian"
local Baby_Setting = import "L10.Game.Baby_Setting"
local Xingguan_StarGroup = import "L10.Game.Xingguan_StarGroup"
local CBabyModelTextureLoader = import "L10.UI.CBabyModelTextureLoader"
local CLingShouModelTextureLoader = import "L10.UI.CLingShouModelTextureLoader"
local CLingShouMgr = import "L10.Game.CLingShouMgr"
local Ease = import "DG.Tweening.Ease"
local Vector3 = import "UnityEngine.Vector3"
local CUIRes = import "L10.UI.CUIResources"
local CBabyMgr = import "L10.Game.BabyMgr"
local Baby_Character = import "L10.Game.Baby_Character"
local CButton = import "L10.UI.CButton"
local CSwitchMgr = import "L10.Engine.CSwitchMgr"
local MessageWndManager = import "L10.UI.MessageWndManager"
--local Baby_QiYu = import "L10.Game.Baby_QiYu"
local CUIFx = import "L10.UI.CUIFx"
--local Baby_QiChang = import "L10.Game.Baby_QiChang"
local NGUIText = import "NGUIText"
local UITable = import "UITable"
local Baby_Skill = import "L10.Game.Baby_Skill"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local UIPanel = import "UIPanel"
local RotateMode = import "DG.Tweening.RotateMode"
local UIWidget = import "UIWidget"
local UILabelOverflow = import "UILabel+Overflow"

LuaBabyInfoView = class()

-- baby root --
RegistClassMember(LuaBabyInfoView, "Portrait1")
RegistClassMember(LuaBabyInfoView, "Portrait2")
RegistClassMember(LuaBabyInfoView, "Portrait1Container")
RegistClassMember(LuaBabyInfoView, "Portrait2Container")
RegistClassMember(LuaBabyInfoView, "BabyInfo")
RegistClassMember(LuaBabyInfoView, "BabyNameLabel")
RegistClassMember(LuaBabyInfoView, "BabyLevelLabel")
RegistClassMember(LuaBabyInfoView, "ExpProgress")
RegistClassMember(LuaBabyInfoView, "ExpPercentLabel")
RegistClassMember(LuaBabyInfoView, "ReNameBtn")

-- qichang root --
RegistClassMember(LuaBabyInfoView, "QiChangNotOpen")


-- model root --
RegistClassMember(LuaBabyInfoView, "BabyTexture")
RegistClassMember(LuaBabyInfoView, "Babybg1")
RegistClassMember(LuaBabyInfoView, "Babybg2")
RegistClassMember(LuaBabyInfoView, "Babybg3")
RegistClassMember(LuaBabyInfoView, "LingshouTexture")

-- property root --
RegistClassMember(LuaBabyInfoView, "TabBar")
RegistClassMember(LuaBabyInfoView, "BabyPropertyButton")
RegistClassMember(LuaBabyInfoView, "BabyPropertyView")
RegistClassMember(LuaBabyInfoView, "PropertyGrid")
RegistClassMember(LuaBabyInfoView, "PropertyTemplate")

RegistClassMember(LuaBabyInfoView, "BabyBasicView")
RegistClassMember(LuaBabyInfoView, "BasicGrid")
RegistClassMember(LuaBabyInfoView, "BasicTemplate")

-- operation root --
RegistClassMember(LuaBabyInfoView, "OperationGrid")
RegistClassMember(LuaBabyInfoView, "HomeBtn")
RegistClassMember(LuaBabyInfoView, "ChatBtn")
RegistClassMember(LuaBabyInfoView, "ChatBtnAlert")
RegistClassMember(LuaBabyInfoView, "AppearBtn")
RegistClassMember(LuaBabyInfoView, "SummonBtn")
RegistClassMember(LuaBabyInfoView, "DiaryBtn")
RegistClassMember(LuaBabyInfoView, "DiaryBtnAlert")

-- data --
RegistClassMember(LuaBabyInfoView, "SelectedBaby")
RegistClassMember(LuaBabyInfoView, "OtherBaby")
RegistClassMember(LuaBabyInfoView, "LeftBabyId")
RegistClassMember(LuaBabyInfoView, "RightBabyId")
RegistClassMember(LuaBabyInfoView, "FatherName")
RegistClassMember(LuaBabyInfoView, "MotherName")
RegistClassMember(LuaBabyInfoView, "BabyIsSummon")
RegistClassMember(LuaBabyInfoView, "m_JieBanLabel")
RegistClassMember(LuaBabyInfoView, "IsSkillExpaned")


RegistChildComponent(LuaBabyInfoView, "QiChangTag", GameObject)
RegistChildComponent(LuaBabyInfoView, "QiChangLabel", UILabel)
RegistChildComponent(LuaBabyInfoView, "QiChangBG", CUITexture)
RegistChildComponent(LuaBabyInfoView, "QiChangFX", CUIFx)
RegistChildComponent(LuaBabyInfoView, "QiChangAlert_NotOpen", GameObject)
RegistChildComponent(LuaBabyInfoView, "QiChangAlert_Open", GameObject)
RegistChildComponent(LuaBabyInfoView, "XiLianBtn", GameObject)

RegistChildComponent(LuaBabyInfoView, "BabyPropertyView_ShaoNian", GameObject)
RegistChildComponent(LuaBabyInfoView, "PropertyGrid_ShaoNian", UIGrid)
RegistChildComponent(LuaBabyInfoView, "PropertyTemplate_ShaoNian", GameObject)
RegistChildComponent(LuaBabyInfoView, "SkillTemplate", GameObject)

--Baby2023
RegistClassMember(LuaBabyInfoView, "BubbleRoot")
RegistClassMember(LuaBabyInfoView, "BubbleLabel")
RegistClassMember(LuaBabyInfoView, "NotAtHomeRoot")
RegistClassMember(LuaBabyInfoView, "HightestPropertyScore")
RegistClassMember(LuaBabyInfoView, "TopPropertyGo")
RegistClassMember(LuaBabyInfoView, "mTargetQiChang")
RegistClassMember(LuaBabyInfoView, "mTargetQiChangData")
RegistClassMember(LuaBabyInfoView, "BubbleList")
RegistClassMember(LuaBabyInfoView, "BubbleTick")

function LuaBabyInfoView:Init()
	self:InitTargetQichang()
	self:InitClassMembers()
	self:InitValues()
end

function LuaBabyInfoView:InitClassMembers()
	self.Portrait1 = self.transform:Find("BabyRoot/Portrait1").gameObject
	self.Portrait1Container = self.transform:Find("BabyRoot/Portrait1/Portrait1Container").gameObject
	self.Portrait2 = self.transform:Find("BabyRoot/Portrait2").gameObject
	self.Portrait2Container = self.transform:Find("BabyRoot/Portrait2/Portrait2Container").gameObject

	self.BabyInfo = self.transform:Find("BabyRoot/BabyInfo").gameObject
	self.BabyNameLabel = self.transform:Find("BabyRoot/BabyInfo/BabyNameLabel"):GetComponent(typeof(UILabel))
	self.BabyLevelLabel = self.transform:Find("BabyRoot/BabyInfo/BabyLevelLabel"):GetComponent(typeof(UILabel))
	self.ExpProgress = self.transform:Find("BabyRoot/BabyInfo/ExpProgress"):GetComponent(typeof(UIProgressBar))
	self.ExpPercentLabel = self.transform:Find("BabyRoot/BabyInfo/ExpProgress/ExpPercentLabel"):GetComponent(typeof(UILabel))
	self.ReNameBtn = self.transform:Find("BabyRoot/BabyInfo/ReNameBtn").gameObject

	self.QiChangNotOpen = self.transform:Find("QiChangRoot/NotOpen").gameObject

	self.BabyTexture = self.transform:Find("ModelRoot/BabyTexture"):GetComponent(typeof(CBabyModelTextureLoader))
	self.Babybg1 = self.transform:Find("ModelRoot/Babybg1").gameObject
	self.Babybg2 = self.transform:Find("ModelRoot/Babybg2").gameObject
	self.Babybg3 = self.transform:Find("ModelRoot/Babybg3").gameObject

	self.LingshouTexture = self.transform:Find("ModelRoot/lingshouTexture"):GetComponent(typeof(CLingShouModelTextureLoader))

	self.TabBar = self.transform:Find("PropertyRoot/TabBar"):GetComponent(typeof(UITabBar))

	self.BabyPropertyView = self.transform:Find("PropertyRoot/BabyPropertyView").gameObject
	self.BabyPropertyButton = self.transform:Find("PropertyRoot/TabBar/Property"):GetComponent(typeof(CButton))
	self.PropertyGrid =  self.transform:Find("PropertyRoot/BabyPropertyView/PropertyGrid"):GetComponent(typeof(UIGrid))
	self.PropertyTemplate = self.transform:Find("PropertyRoot/BabyPropertyView/PropertyTemplate").gameObject
	self.PropertyTemplate:SetActive(false)
	self.PropertyTemplate_ShaoNian:SetActive(false)

	self.BabyBasicView = self.transform:Find("PropertyRoot/BabyBasicView").gameObject
	self.BasicGrid = self.transform:Find("PropertyRoot/BabyBasicView/BasicGrid"):GetComponent(typeof(UIGrid))
	self.BasicTemplate = self.transform:Find("PropertyRoot/BabyBasicView/BasicTemplate").gameObject
	self.BasicTemplate:SetActive(false)
	self.SkillTemplate:SetActive(false)

	self.OperationGrid = self.transform:Find("OperationRoot/Grid"):GetComponent(typeof(UIGrid))
	self.HomeBtn = self.transform:Find("OperationRoot/Grid/HomeBtn").gameObject
	self.ChatBtn = self.transform:Find("OperationRoot/Grid/ChatBtn").gameObject
	self.ChatBtnAlert = self.transform:Find("OperationRoot/Grid/ChatBtn/Alert").gameObject
	self.AppearBtn = self.transform:Find("OperationRoot/Grid/AppearBtn").gameObject
	self.SummonBtn = self.transform:Find("OperationRoot/Grid/SummonBtn").gameObject

	self.DiaryBtn = self.transform:Find("OperationRoot/DiaryBtn").gameObject
	self.DiaryBtnAlert = self.transform:Find("OperationRoot/DiaryBtn/Alert").gameObject

	self.ChatBtn:SetActive(CSwitchMgr.EnableBabyChat)
	self.OperationGrid:Reposition()
	--Baby2023
	self.BubbleRoot = self.transform:Find("BubbleRoot")
	if self.BubbleRoot then
		self.BubbleLabel = self.BubbleRoot:Find("BubbleLabel"):GetComponent(typeof(UILabel))
		self.BubbleBg = self.BubbleRoot:Find("Texture"):GetComponent(typeof(UIWidget))
	end
	self.NotAtHomeRoot = self.transform:Find("NotAtHomeRoot")
end

function LuaBabyInfoView:InitValues()

	self.BabyIsSummon = false
	self.IsSkillExpaned = false

	self.TabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
		self:OnTabChange(go, index)
	end)

	local onQiChangNotOpenClicked = function (go)
		self:OnQiChangNotOpenClicked(go)
	end
	CommonDefs.AddOnClickListener(self.QiChangNotOpen, DelegateFactory.Action_GameObject(onQiChangNotOpenClicked), false)
	CommonDefs.AddOnClickListener(self.QiChangBG.gameObject, DelegateFactory.Action_GameObject(onQiChangNotOpenClicked), false)

	local onHomeBtnClicked = function (go)
		self:OnHomeBtnClicked(go)
	end
	CommonDefs.AddOnClickListener(self.HomeBtn, DelegateFactory.Action_GameObject(onHomeBtnClicked), false)

	local onChatBtnClicked = function (go)
		self:OnChatBtnClicked(go)
	end
	CommonDefs.AddOnClickListener(self.ChatBtn, DelegateFactory.Action_GameObject(onChatBtnClicked), false)

	local onAppearBtnClicked = function (go)
		self:OnAppearBtnClicked(go)
	end
	CommonDefs.AddOnClickListener(self.AppearBtn, DelegateFactory.Action_GameObject(onAppearBtnClicked), false)

	local onSummonBtnClicked = function (go)
		self:OnSummonBtnClicked(go)
	end
	CommonDefs.AddOnClickListener(self.SummonBtn, DelegateFactory.Action_GameObject(onSummonBtnClicked), false)

	local onDiaryBtnClicked = function (go)
		self:OnDiaryBtnClicked(go)
	end
	CommonDefs.AddOnClickListener(self.DiaryBtn, DelegateFactory.Action_GameObject(onDiaryBtnClicked), false)

	local onReNameBtnClicked = function (go)
		self:OnReNameBtnClicked(go)
	end
	CommonDefs.AddOnClickListener(self.ReNameBtn, DelegateFactory.Action_GameObject(onReNameBtnClicked), false)

	local onXiLianBtnClicked = function (go)
		self:OnXiLianBtnClicked(go)
	end
	CommonDefs.AddOnClickListener(self.XiLianBtn, DelegateFactory.Action_GameObject(onXiLianBtnClicked), false)
	self:InitPortrait()
end

function LuaBabyInfoView:UpdateChatButtonAlert()
	if not self.ChatBtnAlert then return end
	if not self.SelectedBaby then
		self.ChatBtnAlert:SetActive(false)
	else
		self.ChatBtnAlert:SetActive(LuaBabyMgr.HasUnreadMsg(self.SelectedBaby.Id))
	end
end

function LuaBabyInfoView:BabyUnreadMsgChanged(babyId)
	if self.SelectedBaby and self.SelectedBaby.Id == babyId then
		self:UpdateChatButtonAlert()
	end
end

function LuaBabyInfoView:UpdateDiaryBtnAlert()
	if not self.SelectedBaby then
		self.DiaryBtnAlert:SetActive(false)
		return
	end

	local result = false
	local diaryDict = self.SelectedBaby.Props.ScheduleData.DiaryData
	CommonDefs.DictIterate(diaryDict, DelegateFactory.Action_object_object(function (___key, ___value)
		if ___value.Accepted == 0 then -- 成长手记有任务没接
			result = true
		end
	end))

	-- 奇遇有奖品时显示红点
	local qiyuDict = self.SelectedBaby.Props.ScheduleData.QiYuData
	CommonDefs.DictIterate(qiyuDict, DelegateFactory.Action_object_object(function (___key, ___value)
		if ___value.ReceivedAward == 0 then -- 有奖品没接
			result = true
		end
	end))

	local taskData = self.SelectedBaby.Props.TaskData

	local finishedTasks = taskData.FinishedTasks
	local acceptedTasks = taskData.AcceptedTasks
	-- 奇遇有任务可接时显示红点
	Baby_QiYu.ForeachKey(function (key)
		local data = Baby_QiYu.GetData(key)
    	if data.TaskId ~= 0 and self.SelectedBaby.Grade >= data.GetLevel then
    		if not CommonDefs.DictContains_LuaCall(finishedTasks, data.TaskId) and not CommonDefs.DictContains_LuaCall(acceptedTasks, data.TaskId) then
    			result = true
    		end
    	end
	end)

	self.DiaryBtnAlert:SetActive(result)
end

function LuaBabyInfoView:InitPortrait()
	local selectedBabyId = BabyMgr.Inst:TrySelectBaby()
	if not selectedBabyId then return end

	local selectedBaby = BabyMgr.Inst:GetSelectedBaby()
	self.SelectedBaby = selectedBaby

	if BabyMgr.Inst.BabyDictionary.Count < 1 then
		-- 玩家身上没有宝宝，关闭界面
		CUIManager.CloseUI(CLuaUIResources.BabyWnd)
		return

	elseif BabyMgr.Inst.BabyDictionary.Count == 1 then
		-- 玩家身上只有一个宝宝，选中该宝宝，初始化一个头像框
		self.Portrait1:SetActive(true)
		self.Portrait2:SetActive(false)
		LuaUtils.SetLocalPositionX(self.Portrait1.transform, -660)

		local portraitTexture = self.Portrait1Container.transform:Find("PortraitTexture"):GetComponent(typeof(CUITexture))
		local portraint = BabyMgr.Inst:GetBabyPortrait(self.SelectedBaby)
		portraitTexture:LoadNPCPortrait(portraint, false)

		self:ClearPortraitClick(self.Portrait1.gameObject)

		if LuaBabyMgr.m_BabyOnPlayerAndPregnant then
			self.Portrait2:SetActive(true)
			LuaUtils.SetLocalPositionX(self.Portrait1.transform, -703)
			LuaUtils.SetLocalPositionX(self.Portrait2.transform, -608)
			LuaTweenUtils.TweenAlpha(self.Portrait2Container.transform, 1, 0.5, 0)
			LuaTweenUtils.TweenScale(self.Portrait2Container.transform, Vector3(1, 1, 1), Vector3(0.7 , 0.7, 1), 0)

			local portraitTexture2 = self.Portrait2Container.transform:Find("PortraitTexture"):GetComponent(typeof(CUITexture))
			local portraint2 = "ynpc001b"
			portraitTexture2:LoadNPCPortrait(portraint2, false)

			CommonDefs.AddOnClickListener(self.Portrait2.gameObject, DelegateFactory.Action_GameObject(function (go)
				Gac2Gas.RequestInspectInfo()
			end), false)
		else
			self:ClearPortraitClick(self.Portrait2.gameObject)
		end

	elseif BabyMgr.Inst.BabyDictionary.Count == 2 then
		-- 玩家身上有两个宝宝，选中出生早的宝宝，初始化两个头像框
		self.Portrait1:SetActive(true)
		self.Portrait2:SetActive(true)
		LuaUtils.SetLocalPositionX(self.Portrait1.transform, -703)
		LuaUtils.SetLocalPositionX(self.Portrait2.transform, -608)

		local babyIdList = BabyMgr.Inst:GetBabyIdList()

		self.LeftBabyId = babyIdList[0]
		self.RightBabyId = babyIdList[1]

		local portraitTexture1 = self.Portrait1Container.transform:Find("PortraitTexture"):GetComponent(typeof(CUITexture))
		local portraint1 = BabyMgr.Inst:GetBabyPortraitById(self.LeftBabyId)
		portraitTexture1:LoadNPCPortrait(portraint1, false)

		local portraitTexture2 = self.Portrait2Container.transform:Find("PortraitTexture"):GetComponent(typeof(CUITexture))
		local portraint2 = BabyMgr.Inst:GetBabyPortraitById(self.RightBabyId)
		portraitTexture2:LoadNPCPortrait(portraint2, false)

		self:ProcessTwoBabyPortrait()

	end

	self:InitBabyInfo()
end

function LuaBabyInfoView:InitTargetQichang()
	self.mTargetQiChang = 0
	local selectedBabyId = BabyMgr.Inst:TrySelectBaby()

	if not selectedBabyId then return end
	Gac2Gas.QueryTargetQiChang(selectedBabyId)
end

function LuaBabyInfoView:ProcessTwoBabyPortrait()
	if self.LeftBabyId == self.SelectedBaby.Id then

		LuaTweenUtils.TweenAlpha(self.Portrait2Container.transform, 1, 0.5, 0)
		LuaTweenUtils.TweenScale(self.Portrait2Container.transform, Vector3(1, 1, 1), Vector3(0.7 , 0.7, 1), 0)

		LuaTweenUtils.TweenAlpha(self.Portrait1Container.transform, 0.5, 1, 0)
		LuaTweenUtils.TweenScale(self.Portrait1Container.transform, Vector3(0.7, 0.7, 1), Vector3(1, 1, 1), 0)

		self:ClearPortraitClick(self.Portrait1.gameObject)

		CommonDefs.AddOnClickListener(self.Portrait2.gameObject, DelegateFactory.Action_GameObject(function (go)
			self:SwitchBaby(true)
		end), false)
	else

		LuaTweenUtils.TweenAlpha(self.Portrait1Container.transform, 1, 0.5, 0)
		LuaTweenUtils.TweenScale(self.Portrait1Container.transform, Vector3(1, 1, 1), Vector3(0.7 , 0.7, 1), 0)

		LuaTweenUtils.TweenAlpha(self.Portrait2Container.transform, 0.5, 1, 0)
		LuaTweenUtils.TweenScale(self.Portrait2Container.transform, Vector3(0.7, 0.7, 1), Vector3(1, 1, 1), 0)

		self:ClearPortraitClick(self.Portrait2.gameObject)

		CommonDefs.AddOnClickListener(self.Portrait1.gameObject, DelegateFactory.Action_GameObject(function (go)
			self:SwitchBaby(false)
		end), false)
	end
end


function LuaBabyInfoView:SwitchBaby(left2right)
	-- Step 1 头像大小修改
	self:SwitchPortrait(left2right)

	-- Step 2 BabyMgr处理
	local switchedBaby = BabyMgr.Inst:TrySwitchBaby()
	if switchedBaby then
		-- Step 3 更新界面信息
		self.SelectedBaby = BabyMgr.Inst:GetSelectedBaby()
		self.OtherBaby = BabyMgr.Inst:GetTheOtherBaby()
		self:InitBabyInfo()
		self:ProcessTwoBabyPortrait()
		self:InitTargetQichang()
	end

end

function LuaBabyInfoView:SwitchPortrait(left2right)
	if left2right then
		LuaTweenUtils.TweenAlpha(self.Portrait1Container.transform, 1, 0.5, 0.8)
		LuaTweenUtils.SetEase(LuaTweenUtils.TweenScale(self.Portrait1Container.transform, Vector3(1, 1, 1), Vector3(0.7 , 0.7, 1),0.8), Ease.OutBack)

		LuaTweenUtils.TweenAlpha(self.Portrait2Container.transform, 0.5, 1, 0.8)
		LuaTweenUtils.SetEase(LuaTweenUtils.TweenScale(self.Portrait2Container.transform, Vector3(0.7, 0.7, 1), Vector3(1, 1, 1), 0.8), Ease.OutBack)
	else
		LuaTweenUtils.TweenAlpha(self.Portrait2Container.transform, 1, 0.5, 0.8)
		LuaTweenUtils.SetEase(LuaTweenUtils.TweenScale(self.Portrait2Container.transform, Vector3(1, 1, 1), Vector3(0.7 , 0.7, 1),0.8), Ease.OutBack)

		LuaTweenUtils.TweenAlpha(self.Portrait1Container.transform, 0.5, 1, 0.8)
		LuaTweenUtils.SetEase(LuaTweenUtils.TweenScale(self.Portrait1Container.transform, Vector3(0.7, 0.7, 1), Vector3(1, 1, 1), 0.8), Ease.OutBack)
	end
end

function LuaBabyInfoView:ClearPortraitClick(go)
	CommonDefs.AddOnClickListener(go, DelegateFactory.Action_GameObject(function (go) end), false)
end

function LuaBabyInfoView:InitLingshouData(args)
	if args and args[0] and args[1] then
		local lingShouId = args[0]
		local lingshouDetails = args[1]
		if lingshouDetails then
			self.LingshouTexture.gameObject:SetActive(true)
			self.LingshouTexture:Init(lingshouDetails)
		end
	end
end

function LuaBabyInfoView:InitLingshou()
  self.LingshouTexture.gameObject:SetActive(false)
	if CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.BindedPartenerLingShouId ~= "" and CClientMainPlayer.Inst.ItemProp.OwnBabyId ~= "" then
		if CClientMainPlayer.Inst.ItemProp.OwnBabyId == self.SelectedBaby.Id then
			--local lingshouDetails = CLingShouOtherMgr.Inst:GetLingShouDetails(CClientMainPlayer.Inst.ItemProp.BindedPartenerLingShouId)
			CLingShouMgr.Inst:RequestLingShouDetails(CClientMainPlayer.Inst.ItemProp.BindedPartenerLingShouId)
		end
	end
end

function LuaBabyInfoView:InitBabyInfo()
	if not self.SelectedBaby then return end

	if self.BubbleRoot then --养育迭代后界面，增加引导
		CLuaGuideMgr.TriggerBabySetTargetQiChangGuide()
	end

	self.BabyNameLabel.text = self.SelectedBaby.Name
	self.BabyLevelLabel.text = SafeStringFormat3(LocalString.GetString("%d级"), self.SelectedBaby.Grade)

	g_ScriptEvent:BroadcastInLua("BabyWndSelectBaby", self.SelectedBaby.Grade)

	local grade = self.SelectedBaby.Grade
	local babyExp = Baby_BabyExp.GetData(grade)
	if babyExp then
		self.ExpProgress.value = (self.SelectedBaby.Exp / babyExp.Value)
		self.ExpPercentLabel.text = SafeStringFormat3("%s/%s", tostring(self.SelectedBaby.Exp), tostring(babyExp.Value))
	else
		self.ExpProgress.value = 1
		self.ExpPercentLabel.text = ""
	end
	
	self.BabyTexture:Init(self.SelectedBaby, -180, false, 0)
	self.BabyTexture.gameObject:SetActive(self.SelectedBaby.Props.ExtraData.IsBabyHide == 0)

	if self.NotAtHomeRoot then
		self.NotAtHomeRoot.gameObject:SetActive(self.SelectedBaby.Props.ExtraData.IsBabyHide ~= 0)
	end
	self:ShowBabyBG(self.SelectedBaby.Props.ExtraData.ShowStatus)
	--self:InitLingshou()
	self.FatherName = nil
	self.MotherName = nil
	-- 请求父母名称
	Gac2Gas.QueryBabyParentsName(self.SelectedBaby.Id)
	-- 请求宝宝是否跟随
	Gac2Gas.QueryBabyFollowStatus(self.SelectedBaby.Id)

	-- 根据baby的阶段来确定tab
	if self.SelectedBaby.Status == EnumBabyStatus.eBorn then
		self.BabyPropertyButton.gameObject:SetActive(false)
	else
		self.BabyPropertyButton.gameObject:SetActive(true)
	end
	-- 少年期默认选中属性界面
	if self.SelectedBaby.Status == EnumBabyStatus.eYoung then
		self.TabBar:ChangeTab(1)
	else
		self.TabBar:ChangeTab(0)
	end
	
	self.XiLianBtn:SetActive(self.SelectedBaby.Status == EnumBabyStatus.eYoung)
	CLuaGuideMgr.TryTriggerBabyUpgradeGuide(self.SelectedBaby.Id)

	self:UpdateChatButtonAlert()
	self:UpdateDiaryBtnAlert()
	self:UpdateQiChang()
end

function LuaBabyInfoView:UpdateQiChang()
	if not self.SelectedBaby then return end
	if self.SelectedBaby.Status < EnumBabyStatus.eYoung then
		self.QiChangNotOpen:SetActive(true)
		self.QiChangTag:SetActive(false)
		self.QiChangAlert_Open:SetActive(false)
		self.QiChangAlert_NotOpen:SetActive(false)
	else
		local hasNewQiChang = self:CheckBabyHasNewQiChang()

		self.QiChangNotOpen:SetActive(false)
		self.QiChangTag:SetActive(true)
		self.QiChangAlert_Open:SetActive(hasNewQiChang)
		self.QiChangAlert_NotOpen:SetActive(false)
		
		local currentQiChangId = self.SelectedBaby.Props.QiChangData.CurrentQiChangId
		local qichang = Baby_QiChang.GetData(currentQiChangId)
		if qichang then
			local bgPath = LuaBabyMgr.GetQiChangBG(self.SelectedBaby.Gender, qichang.Quality)
			self.QiChangBG:LoadMaterial(bgPath)
			local name = qichang.NameM
			if self.SelectedBaby.Gender == 1 then
				name = qichang.NameF
			end
			local qctext = SafeStringFormat3("[%s]%s[-]", LuaBabyMgr.GetQiChangNameColor(qichang.Quality-1), name)
			self.QiChangLabel.text =LocalString.StrH2V(qctext,false)
			self.QiChangFX:DestroyFx()
			local qichangfxPath = LuaBabyMgr.GetQiChangFx(qichang.Quality-1)
			if qichangfxPath then
				self.QiChangFX:LoadFx(qichangfxPath)
			end
		else
			self.QiChangNotOpen:SetActive(true)
			self.QiChangTag:SetActive(false)
			self.QiChangAlert_Open:SetActive(false)
			self.QiChangAlert_NotOpen:SetActive(hasNewQiChang)
		end
	end
end

function LuaBabyInfoView:CheckBabyHasNewQiChang()
	local activedQiChang = self.SelectedBaby.Props.QiChangData.ActivedQiChang
	local newQiChangs = {}

	Baby_QiChang.ForeachKey(function (key)
		if not CommonDefs.DictContains_LuaCall(activedQiChang, key) and #newQiChangs <= 0 then
			local canBeCheck = self:CheckQiChangCanBeJianDing(key)
			if canBeCheck then
				table.insert(newQiChangs, key)
			end
		end
	end)

	return #newQiChangs > 0
end

function LuaBabyInfoView:CheckBabyHasNewQiChangExceptTarget()
	local activedQiChang = self.SelectedBaby.Props.QiChangData.ActivedQiChang
	local newQiChangs = {}

	Baby_QiChang.ForeachKey(function (key)
		if not CommonDefs.DictContains_LuaCall(activedQiChang, key) and key ~= self.mTargetQiChang and key ~= self.mTargetQiChangData.PreQichang and #newQiChangs <= 0 then
			local canBeCheck = self:CheckQiChangCanBeJianDing(key)
			if canBeCheck then
				table.insert(newQiChangs, key)
			end
		end
	end)

	return #newQiChangs > 0
end

function LuaBabyInfoView:CheckQiChangCanBeJianDing(qichangId)
	local selectedQiChang = Baby_QiChang.GetData(qichangId)
	if not selectedQiChang then return end

	local activedQiChang = self.SelectedBaby.Props.QiChangData.ActivedQiChang
	-- step 1 前置气场
    if selectedQiChang.PreQichang ~= 0 then
    	local preQiChang = Baby_QiChang.GetData(selectedQiChang.PreQichang)
    	if not CommonDefs.DictContains_LuaCall(activedQiChang, preQiChang.ID) then
    		return false
    	end
    end

    -- Step 2 获得奇遇
    local qiyuDict = self.SelectedBaby.Props.ScheduleData.QiYuData
    if selectedQiChang.QiyuGet ~= 0 then
    	if not CommonDefs.DictContains_LuaCall(qiyuDict, selectedQiChang.QiyuGet) then
    		return false
    	end
    end

    -- Step 3 属性要求, 有12个
    for i = 1, 12 do
    	if self:GetQiChangRequestPropByIndex(i, selectedQiChang) > 0 then
    		local request = self:GetQiChangRequestPropByIndex(i, selectedQiChang)
            local own = 0
            if self.SelectedBaby.Props.YoungProp and CommonDefs.DictContains_LuaCall(self.SelectedBaby.Props.YoungProp, i) then
                own = self.SelectedBaby.Props.YoungProp[i]
            end
            if own < request then
            	return false
            end
    	end
    end

    -- Step 4 属性最高
    if selectedQiChang.TopAttribute ~= 0 and selectedQiChang.Quality >= 5 then
    	local youer = Baby_YouEr.GetData(selectedQiChang.TopAttribute)
    	if youer and self.SelectedBaby.Props.YoungProp then
    		local highest = 0
    		if CommonDefs.DictContains_LuaCall(self.SelectedBaby.Props.YoungProp, selectedQiChang.TopAttribute) then
                highest = LuaBabyMgr.GetYoundPropByGroup(self.SelectedBaby, selectedQiChang.TopAttribute)
            end
            local isHighest = true

            if self.SelectedBaby.Props.YoungProp and self.SelectedBaby.Props.YoungProp.Count >= 12 then
                for i = 1, 6, 1 do
                    if selectedQiChang.TopAttribute ~=i and LuaBabyMgr.GetYoundPropByGroup(self.SelectedBaby, i) > highest then
                        isHighest = false
                    end
                end
            end

            if not isHighest then
            	return false
            end
    	end
    end

    return true
end

function LuaBabyInfoView:GetQiChangRequestPropByIndex(index, qichang)
    if index == 1 then
        return qichang.Strategy
    elseif index == 2 then
        return qichang.Taoism
    elseif index == 3 then
        return qichang.Martial
    elseif index == 4 then
        return qichang.Military
    elseif index == 5 then
        return qichang.Poetry
    elseif index == 6 then
        return qichang.Yayi
    elseif index == 7 then
        return qichang.Heaven
    elseif index == 8 then
        return qichang.Process
    elseif index == 9 then
        return qichang.Virtue
    elseif index == 10 then
        return qichang.Ambiguity
    elseif index == 11 then
        return qichang.Tyrannical
    elseif index == 12 then
        return qichang.Evil
    end
    return ""
end

function LuaBabyInfoView:ShowBabyBG(status)
	self.Babybg1:SetActive(status == 1)
	self.Babybg2:SetActive(status == 2)
	self.Babybg3:SetActive(status == 3)
end

function LuaBabyInfoView:OnTabChange(go, index)
	if index == 1 then
		self.BabyBasicView:SetActive(false)
		self:OnPropertyViewClicked()
	elseif index == 0 then
		self.BabyPropertyView:SetActive(false)
		self.BabyPropertyView_ShaoNian:SetActive(false)
		self.BabyBasicView:SetActive(true)
		self:UpdateBabyBasic()
	end
end

function LuaBabyInfoView:OnPropertyViewClicked()
	self.BabyPropertyView:SetActive(false)
	self.BabyPropertyView_ShaoNian:SetActive(false)

	if self.SelectedBaby.Status == EnumBabyStatus.eChild then
		self.BabyPropertyView:SetActive(true)
		self:UpdateBabyProperty()
	elseif self.SelectedBaby.Status == EnumBabyStatus.eYoung then
		self.BabyPropertyView_ShaoNian:SetActive(true)
		self:UpdateBabyProperty_ShaoNian()
	end
end

function LuaBabyInfoView:UpdateBabyProperty()
	CUICommonDef.ClearTransform(self.PropertyGrid.transform)

	if self.SelectedBaby.Status == EnumBabyStatus.eBorn then
		return
	end

	Baby_YouEr.ForeachKey(DelegateFactory.Action_object(function (key)
        local data = Baby_YouEr.GetData(key)
        local go = NGUITools.AddChild(self.PropertyGrid.gameObject, self.PropertyTemplate)
        self:InitBabyPropertyItem(go, key, data)
        go:SetActive(true)
    end))

    self.PropertyGrid:Reposition()
end

function LuaBabyInfoView:UpdateBabyProperty_ShaoNian()
	CUICommonDef.ClearTransform(self.PropertyGrid_ShaoNian.transform)

	if self.SelectedBaby.Status == EnumBabyStatus.eBorn then
		return
	end

	self.HightestPropertyScore = 0
	self.TopPropertyGo = nil
	Baby_YouEr.ForeachKey(DelegateFactory.Action_object(function (key)
        local data = Baby_YouEr.GetData(key)
        local go = NGUITools.AddChild(self.PropertyGrid_ShaoNian.gameObject, self.PropertyTemplate_ShaoNian)
        self:InitBabyPropertyItem_ShaoNian(go, key, data)
        go:SetActive(true)
    end))
	if self.TopPropertyGo then
		local topTag = self.TopPropertyGo.transform:Find("TopTag")
		if topTag then
			topTag.gameObject:SetActive(true)
		end
	end

	self.PropertyGrid_ShaoNian:Reposition()
end

function LuaBabyInfoView:InitBabyPropertyItem(go, key, data)
	local PropertyLabel = go.transform:Find("PropertyLabel"):GetComponent(typeof(UILabel))
	local ValueLabel = go.transform:Find("ValueLabel"):GetComponent(typeof(UILabel))
	local BG = go.transform:GetComponent(typeof(CUITexture))

	PropertyLabel.text = data.Name
	BG:LoadMaterial(LuaBabyMgr.GetPropBG(key))
	if self.SelectedBaby.Status == EnumBabyStatus.eChild then
		local value = 0
		if CommonDefs.DictContains_LuaCall(self.SelectedBaby.Props.ChildProp, key) then
			value = self.SelectedBaby.Props.ChildProp[key]
		end
		ValueLabel.text = tostring(value)
	elseif self.SelectedBaby.Status == EnumBabyStatus.eYoung then
		local needValue = {}
		Baby_ShaoNian.ForeachKey(function (id)
			local shaoNian = Baby_ShaoNian.GetData(id)
			if shaoNian.YouErName == key then
				table.insert(needValue, id)
			end
		end)

		local totalValue = 0
		for i = 1, #needValue do
			if CommonDefs.DictContains_LuaCall(self.SelectedBaby.Props.YoungProp, needValue[i]) then
				totalValue = totalValue + self.SelectedBaby.Props.YoungProp[needValue[i]]
			end
		end
		ValueLabel.text = tostring(totalValue)
	end
end

function LuaBabyInfoView:InitBabyPropertyItem_ShaoNian(go, key, data)
	local MainPropertyLabel = go.transform:Find("MainPropertyLabel"):GetComponent(typeof(UILabel))
	local SubPropertyLabel1 = go.transform:Find("SubPropertyLabel1"):GetComponent(typeof(UILabel))
	local SubPropertyLabel2 = go.transform:Find("SubPropertyLabel2"):GetComponent(typeof(UILabel))
	local BG = go.transform:GetComponent(typeof(CUITexture))
	local RectBG = go.transform:Find("RectBG"):GetComponent(typeof(UISprite))
	local Line = go.transform:Find("Line"):GetComponent(typeof(UISprite))
	local NodeTexture = go.transform:Find("NodeTexture"):GetComponent(typeof(UITexture))
	--baby2023
	local TopTag = go.transform:Find("TopTag")
	local BelowStandardTag1 = go.transform:Find("BelowStandardTag1")
	local BelowStandardTag2 = go.transform:Find("BelowStandardTag2")

	MainPropertyLabel.text = data.Name
	BG:LoadMaterial(LuaBabyMgr.GetPropBG(key))

	local subKey1 = key * 2 - 1
	local subKey2 = key * 2
	local score = 0
	

	local subData1 = Baby_ShaoNian.GetData(subKey1)
	if CommonDefs.DictContains_LuaCall(self.SelectedBaby.Props.YoungProp, subData1.ID) then

		local color = self:GetPropertyTextColor(key)

		if BelowStandardTag1 and BelowStandardTag2 then--是否是迭代后界面
			if self.mTargetQiChang == 0 or self.mTargetQiChang == nil then
				BelowStandardTag1.gameObject:SetActive(false)
			else
				local request = self:GetQiChangRequestPropByIndex(subData1.ID, self.mTargetQiChangData)
				local own = self.SelectedBaby.Props.YoungProp[subData1.ID]
				if own < request then
					BelowStandardTag1.gameObject:SetActive(true)
				else
					if request == 0 then
						color = self:GetPropertyTextColorNotTarget(key)
					end
					BelowStandardTag1.gameObject:SetActive(false)
				end
			end
		end
		
		SubPropertyLabel1.text = SafeStringFormat3("[%s]%s %s[-]", color, subData1.Name, tostring(self.SelectedBaby.Props.YoungProp[subData1.ID]))
		score = score + self.SelectedBaby.Props.YoungProp[subData1.ID]
	else
		SubPropertyLabel1.text = ""
	end
	
	local subData2 = Baby_ShaoNian.GetData(subKey2)
	if CommonDefs.DictContains_LuaCall(self.SelectedBaby.Props.YoungProp, subData2.ID) then

		local color = self:GetPropertyTextColor(key)

		if BelowStandardTag1 and BelowStandardTag2 then--是否是迭代后界面
			if self.mTargetQiChang == 0 or self.mTargetQiChang == nil then
				BelowStandardTag2.gameObject:SetActive(false)
			else
				local request = self:GetQiChangRequestPropByIndex(subData2.ID, self.mTargetQiChangData)
				local own = self.SelectedBaby.Props.YoungProp[subData2.ID]
				if own < request then
					BelowStandardTag2.gameObject:SetActive(true)
				else
					if request == 0 then
						color = self:GetPropertyTextColorNotTarget(key)
					end
					BelowStandardTag2.gameObject:SetActive(false)
				end
			end
		end
		
		SubPropertyLabel2.text = SafeStringFormat3("[%s]%s %s[-]", color, subData2.Name, tostring(self.SelectedBaby.Props.YoungProp[subData2.ID]))
		score = score + self.SelectedBaby.Props.YoungProp[subData2.ID]
	else
		SubPropertyLabel2.text = ""
	end

	Line.color = NGUIText.ParseColor24(self:GetPropertyTextColor(key), 0)
	Line.alpha = 0.7
	RectBG.color = NGUIText.ParseColor24(self:GetBGColor(key), 0)
	RectBG.alpha = 0.5
	NodeTexture.color = NGUIText.ParseColor24(self:GetPropertyTextColor(key), 0)

	--养育迭代2023
	if score >= self.HightestPropertyScore then
		self.TopPropertyGo = go
		self.HightestPropertyScore = score
	end

	if TopTag then
		TopTag.gameObject:SetActive(false)
	end
	----目标气场属性达标检查
	--if BelowStandardTag1 and BelowStandardTag2 then--是否是迭代后界面
	--	if self.mTargetQiChang == 0 then
	--		BelowStandardTag1.gameObject:SetActive(false)
	--		BelowStandardTag2.gameObject:SetActive(false)
	--	else
	--		--根据目标气场刷新tag显示
	--	end
	--end
	
end

function LuaBabyInfoView:UpdateBabyBasic()

	CUICommonDef.ClearTransform(self.BasicGrid.transform)
	for i = 1, 6 do
		local go = NGUITools.AddChild(self.BasicGrid.gameObject, self.BasicTemplate)
		self:InitBabyBasicTemplate(go, i)
		go:SetActive(true)
	end
	local skill = NGUITools.AddChild(self.BasicGrid.gameObject, self.SkillTemplate)
	self:InitBabySkillITemplate(skill)
	skill:SetActive(true)

	self.BasicGrid:Reposition()

end

function LuaBabyInfoView:InitBabyBasicTemplate(go, index)
	local PropertyLabel = go.transform:Find("PropertyLabel"):GetComponent(typeof(UILabel))
	local ValueLabel = go.transform:Find("ValueLabel"):GetComponent(typeof(UILabel))
	if index == 1 then
		PropertyLabel.text = LocalString.GetString("父亲")
		ValueLabel.text = self.FatherName
		go.name = "father"
	elseif index == 2 then
		PropertyLabel.text = LocalString.GetString("母亲")
		ValueLabel.text = self.MotherName
		go.name = "mother"
	elseif index == 3 then
		PropertyLabel.text = LocalString.GetString("星官")
		local xingguan = self.SelectedBaby.Props.BasicProp.XingGuan
		local starGroup =  Xingguan_StarGroup.GetData(xingguan)

		if not starGroup then
			ValueLabel.text = "-"
		else
			local setting = Baby_Setting.GetData()
			local nameColor = starGroup.NameColor
			if CommonDefs.DictContains_LuaCall(setting.XingGuanChange, xingguan) then
				nameColor = setting.XingGuanChange[xingguan]
			end
			ValueLabel.text = SafeStringFormat3("%s-%s", starGroup.Xingxiu, nameColor)
		end

	elseif index == 4 then
		PropertyLabel.text = LocalString.GetString("阶段")
		local setting = Baby_Setting.GetData()
		local stage = self.SelectedBaby.Status
		if stage <= setting.StageName.Length then
			ValueLabel.text = setting.StageName[stage-1]
		else
			ValueLabel.text = ""
		end
	elseif index == 5 then
		PropertyLabel.text = LocalString.GetString("性格")
		local nature = self.SelectedBaby.Props.BasicProp.Nature
		local charater = Baby_Character.GetData(nature)
		if charater then
			ValueLabel.text = charater.Character
		else
			ValueLabel.text = ""
		end
	elseif index == 6 then
		PropertyLabel.text = LocalString.GetString("体型")
		local bodyWeight = self.SelectedBaby.Props.BasicProp.BodyWeight
		local setting = Baby_Setting.GetData()
		local bodyWeightName = setting.BodyWeightName
		if CommonDefs.DictContains_LuaCall(bodyWeightName, bodyWeight) then
			ValueLabel.text = bodyWeightName[bodyWeight]
		else
			ValueLabel.text = tostring(bodyWeight)
		end
	end

end

function LuaBabyInfoView:InitBabySkillITemplate(go)
	local Panel = go.transform:Find("Panel"):GetComponent(typeof(UIPanel))
	local SkillGrid = go.transform:Find("Panel/SkillGrid"):GetComponent(typeof(UITable))
	local Skill = go.transform:Find("Skill").gameObject
	local ExpandBtn = go.transform:Find("ExpandBtn").gameObject
	if CommonDefs.IS_VN_CLIENT then
		ExpandBtn:SetActive(false)
	end
	local BackGround = go.transform:Find("BackGround"):GetComponent(typeof(UIWidget))

	Skill:SetActive(false)

	CUICommonDef.ClearTransform(SkillGrid.transform)

	local babySkills = {}
	local activeSkillIds = {}
	Baby_Skill.ForeachKey(DelegateFactory.Action_object(function (key)
		local data = Baby_Skill.GetData(key)
		-- todo 之后版本去掉占卜技能的开关
		if self:IsSkillOpen(data) then
			if key == 4 then
				if LuaBabyMgr.m_ZhanBuOpen then
					table.insert(babySkills, data)
					if self:IsSkillUnlocked(data) then
    					table.insert(activeSkillIds, key)
    				end
				end
			else
				table.insert(babySkills, data)
				if self:IsSkillUnlocked(data) then
    				table.insert(activeSkillIds, key)
    			end
			end
		end
	end))

	local savedSkills = LuaBabyMgr.GetActivedBabySkill()

	for i = 1, #babySkills do
		local skillItem = NGUITools.AddChild(SkillGrid.gameObject, Skill)
		local isActive = self:IsSkillUnlocked(babySkills[i])
		local isNotSaved = false 
		if isActive then
			isNotSaved = self:IsActivedNotSavedSkill(babySkills[i].ID, savedSkills)
		end 
		self:InitBabySkillItem(skillItem, babySkills[i], isActive, isNotSaved)
		skillItem:SetActive(true)
	end
	LuaBabyMgr.SaveActivedBabySkill(activeSkillIds)
	SkillGrid:Reposition()

	CommonDefs.AddOnClickListener(ExpandBtn.gameObject, DelegateFactory.Action_GameObject(function (go)
		self:OnExpandSkillPanel(go, Panel, SkillGrid, BackGround)
	end), false)
end

function LuaBabyInfoView:OnExpandSkillPanel(go, panel, skillGrid, backGround)
	-- 暂存数据
	self.IsSkillExpaned = not self.IsSkillExpaned
	-- Step 1 按钮旋转
	local rotation = self.IsSkillExpaned and 0 or 180
	LuaTweenUtils.DOLocalRotate(go.transform, Vector3(0, 0, rotation), 0.2, RotateMode.Fast)
	-- Step 2 panel处理

	local width = self.IsSkillExpaned and 560 or 407
	local posX = self.IsSkillExpaned and -360 and -180
	backGround.width = width
	panel:ResetAndUpdateAnchors()
	skillGrid.transform:GetComponent(typeof(UIWidget)):ResetAndUpdateAnchors()
	skillGrid:Reposition()
end

function LuaBabyInfoView:IsActivedNotSavedSkill(skillId, savedSkills)
	for i = 1, #savedSkills do
		if savedSkills[i] == skillId then
			return false
		end
	end
	return true
end

function LuaBabyInfoView:InitBabySkillItem(go, skill, isActive, isNotSaved)
	local IconTexture = go.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
	local Disable = go.transform:Find("Disable").gameObject
	local Lock = go.transform:Find("Lock").gameObject
	IconTexture:Clear()
	IconTexture:LoadMaterial(skill.Icon)

	if isActive then
		Disable:SetActive(false)
		Lock:SetActive(false)
		if isNotSaved then
			g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("你有新技能解锁"))
		end
	else
		Disable:SetActive(true)
		Lock:SetActive(true)
	end

	local onSkillClicked = function (gameObject)
		self:OnSkillClicked(gameObject, skill)
	end
	CommonDefs.AddOnClickListener(go, DelegateFactory.Action_GameObject(onSkillClicked), false)
end

function LuaBabyInfoView:IsSkillUnlocked(skill)
	if skill.ExQiChang ~= 0 then
		-- 是否激活这个气场
		local activedQiChang = self.SelectedBaby.Props.QiChangData.ActivedQiChang
		return CommonDefs.DictContains_LuaCall(activedQiChang, skill.ExQiChang)
	elseif skill.ID == 2 then
		return self.SelectedBaby.Props.ExtraData.CanWritePoem == 1
	end
	return true
end

function LuaBabyInfoView:OnSkillClicked(go, skill)
	if self:IsSkillUnlocked(skill) then
		if self[skill.Action] then
			self[skill.Action](self, go)
		end
	else
		LuaBabyMgr.ShowBabySkillTip(skill.ID)
	end
end

function LuaBabyInfoView:toucai(go)
	local msg = g_MessageMgr:FormatMessage("BABY_SHOUCAI_CONFIRM")
	LuaBabyMgr.OpenCommonRuleTip(msg, function ()
		Gac2Gas.RequestPlantAndHarvestInHouse(self.SelectedBaby.Id)
		CUIManager.CloseUI(CLuaUIResources.CommonRuleTipWnd)
	end, nil, LocalString.GetString("宝宝收菜种菜"))
	
end

function LuaBabyInfoView:xieshi(go)
	-- 写诗技能只展示，具体操作在聊天界面中
	LuaBabyMgr.ShowBabySkillTip(2)
end

function LuaBabyInfoView:bianshen(go)
	local menuList = CreateFromClass(MakeGenericClass(List, PopupMenuItemData))
	CommonDefs.ListAdd(menuList, typeof(PopupMenuItemData), PopupMenuItemData(LocalString.GetString("变身少年期"), DelegateFactory.Action_int(function (idx) 
     	Gac2Gas.RequestChangeBabyStatus(self.SelectedBaby.Id, EnumBabyStatus.eYoung)
    end), false, nil, EnumPopupMenuItemStyle.Light))
    CommonDefs.ListAdd(menuList, typeof(PopupMenuItemData), PopupMenuItemData(LocalString.GetString("变身幼儿期"), DelegateFactory.Action_int(function (idx) 
     	Gac2Gas.RequestChangeBabyStatus(self.SelectedBaby.Id, EnumBabyStatus.eChild)
    end), false, nil, EnumPopupMenuItemStyle.Light))
    CommonDefs.ListAdd(menuList, typeof(PopupMenuItemData), PopupMenuItemData(LocalString.GetString("变身婴儿期"), DelegateFactory.Action_int(function (idx) 
     	Gac2Gas.RequestChangeBabyStatus(self.SelectedBaby.Id, EnumBabyStatus.eBorn)
    end), false, nil, EnumPopupMenuItemStyle.Light))
    CPopupMenuInfoMgr.ShowPopupMenu(CommonDefs.ListToArray(menuList), go.transform, CPopupMenuInfoMgr.AlignType.Right, 1, nil, 350, true, 280)
end

function LuaBabyInfoView:zhanbu(go)
	-- 占卜技能只展示，具体操作在聊天界面中
	LuaBabyMgr.ShowBabySkillTip(4)
end

function LuaBabyInfoView:chengyujielong(go)
	LuaBabyMgr.ShowBabySkillTip(5)
end

function LuaBabyInfoView:GetBGMat(key)
	if key == 1 then
		return "UI/Texture/Transparent/Material/BabyWnd_zhimou.mat"
	elseif key == 2 then
		return "UI/Texture/Transparent/Material/BabyWnd_wuli.mat"
	elseif key == 3 then
		return "UI/Texture/Transparent/Material/BabyWnd_fengya.mat"
	elseif key == 4 then
		return "UI/Texture/Transparent/Material/BabyWnd_jiqiao.mat"
	elseif key == 5 then
		return "UI/Texture/Transparent/Material/BabyWnd_lifa.mat"
	elseif key == 6 then
		return "UI/Texture/Transparent/Material/BabyWnd_zuie.mat"
	end
	return ""
end

function LuaBabyInfoView:GetBGColor(key)
	if key == 1 then
		return "665496"
	elseif key == 2 then
		return "99864d"
	elseif key == 3 then
		return "4990a6"
	elseif key == 4 then
		return "50a778"
	elseif key == 5 then
		return "9948a7"
	elseif key == 6 then
		return "9d494d"
	end
	return "FFFFFF"
end

function LuaBabyInfoView:GetPropertyTextColor(key)
	if key == 1 then
		return "ffa2ff"
	elseif key == 2 then
		return "f2f292"
	elseif key == 3 then
		return "99dcfe"
	elseif key == 4 then
		return "B6FFDB"
	elseif key == 5 then
		return "ffb2ff"
	elseif key == 6 then
		return "ff9393"
	end
	return "FFFFFF"
end

-- 不为目标属性时的颜色
function LuaBabyInfoView:GetPropertyTextColorNotTarget(key)
	if key == 1 then
		return "B06BB3"
	elseif key == 2 then
		return "B3B363"
	elseif key == 3 then
		return "5BABD4"
	elseif key == 4 then
		return "5DCB94"
	elseif key == 5 then
		return "B27AB2"
	elseif key == 6 then
		return "B76F6F"
	end
	return "FFFFFF"
end

function LuaBabyInfoView:ShowBubble()
	if not self.BubbleRoot then
		return
	end
	if self.SelectedBaby.Status < EnumBabyStatus.eYoung then
		self.BubbleRoot.gameObject:SetActive(false)
		return
	end
	if #self.BubbleList > 0 then
		self.BubbleRoot.gameObject:SetActive(true)
	else
		self.BubbleRoot.gameObject:SetActive(false)
		return
	end
	
	if #self.BubbleList == 1 then
		self:SetBubbleContent(self.BubbleList[1])
	else
		local i = 1
		self:SetBubbleContent(self.BubbleList[i])
		i = i + 1 
		self.BubbleTick = RegisterTick(function()
			if i > #self.BubbleList then
				i = 1
			end
			self:SetBubbleContent(self.BubbleList[i])
			i = i + 1
		end, Baby_Setting.GetData().BABY_QICHANG_TIME * 1000)
	end
end

function LuaBabyInfoView:SetBubbleContent(msgId)
	local content = g_MessageMgr:FormatMessage(Message_Message.GetData(msgId).name)
	if string.len(content) < 12 then
		self.BubbleLabel.overflowMethod = UILabelOverflow.ResizeFreely
	else
		self.BubbleLabel.overflowMethod = UILabelOverflow.ResizeHeight
	end
	self.BubbleLabel.text = content
	self.BubbleBg:ResetAnchors()
end

function LuaBabyInfoView:SetBubbleList()
	if not self.BubbleRoot then
		return
	end
	if self.SelectedBaby.Status < EnumBabyStatus.eYoung then
		return
	end
	self.BubbleList = {}
	if self.BubbleTick ~= nil then
		UnRegisterTick(self.BubbleTick)
		self.BubbleTick = nil
	end

	local activedQiChang = self.SelectedBaby.Props.QiChangData.ActivedQiChang
	
	if self.mTargetQiChang == 0 or self.mTargetQiChang == nil then
		if self.QiChangAlert_NotOpen.activeSelf or self.QiChangAlert_Open.activeSelf then
			table.insert(self.BubbleList, Baby_Setting.GetData().BABY_QICHANG_TIP2)
		else
			table.insert(self.BubbleList, Baby_Setting.GetData().BABY_QICHANG_TIP1)
		end
	else
		if self.QiChangAlert_NotOpen.activeSelf or self.QiChangAlert_Open.activeSelf then
			if not CommonDefs.DictContains_LuaCall(activedQiChang, self.mTargetQiChang) and self:CheckQiChangCanBeJianDing(self.mTargetQiChang) then
				table.insert(self.BubbleList, Baby_Setting.GetData().BABY_QICHANG_TIP3)
			end
			if self.mTargetQiChangData.PreQichang ~= nil and not CommonDefs.DictContains_LuaCall(activedQiChang, self.mTargetQiChangData.PreQichang) and self:CheckQiChangCanBeJianDing(self.mTargetQiChangData.PreQichang) then
				table.insert(self.BubbleList, Baby_Setting.GetData().BABY_QICHANG_TIP4)
			end
			if #self.BubbleList == 0 or self:CheckBabyHasNewQiChangExceptTarget() then
				table.insert(self.BubbleList, Baby_Setting.GetData().BABY_QICHANG_TIP2)
			end
		end
	end
end

function LuaBabyInfoView:OnReNameBtnClicked(go)
	LuaBabyMgr.m_NamingBabyId = self.SelectedBaby.Id
	LuaBabyMgr.m_IsRename = true

	CUIManager.ShowUI(CLuaUIResources.BabyNamingWnd)
end

function LuaBabyInfoView:OnXiLianBtnClicked(go)
	LuaBabyMgr.m_BabyXiLianType = 0
	LuaBabyMgr.m_ChosenBaby = self.SelectedBaby
	CUIManager.ShowUI(CLuaUIResources.BabyPropXiLianWnd)
end

function LuaBabyInfoView:OnQiChangNotOpenClicked(go)
	if self.SelectedBaby.Status < EnumBabyStatus.eYoung then
		g_MessageMgr:ShowMessage("BABY_QICHANG_NOT_OPEN")
	else
		-- 直接改成打开气场鉴定界面
		local msg = g_MessageMgr:FormatMessage("OPEN_BABY_QICHANG_JIANDING_CONFIRM")
		if self.QiChangAlert_NotOpen.activeSelf or self.QiChangAlert_Open.activeSelf then
			msg = g_MessageMgr:FormatMessage("BABY_HAS_NEW_QICHANG")
		end
		if not self.BubbleRoot then --如果是迭代前的界面
			self.QiChangAlert_NotOpen:SetActive(false)
			self.QiChangAlert_Open:SetActive(false)
			CUIManager.CloseUI(CLuaUIResources.BabyWnd)
		end
		--MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function ()
			LuaBabyMgr.m_ChosenBaby = self.SelectedBaby
			CUIManager.ShowUI(CLuaUIResources.BabyQiChangJianDingWnd)
			
		--end), nil, nil, nil, false)
		
	end
end

-- 将宝宝放回家
function LuaBabyInfoView:OnHomeBtnClicked(go)
	local msg = g_MessageMgr:FormatMessage("BABY_PUT_HOME_COMFIRM")
	MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function()
		if self.SelectedBaby then
			Gac2Gas.RequestReturnBaby(self.SelectedBaby.Id)
		end
	end),nil,LocalString.GetString("确认"),LocalString.GetString("取消"),false)
end

-- 聊天
function LuaBabyInfoView:OnChatBtnClicked(go)
	if self.SelectedBaby and CSwitchMgr.EnableBabyChat then
		LuaBabyMgr.OpenBabyChatWnd(self.SelectedBaby.Id, 0, true, EnumShowBabyChatContext.eFromBabyWnd)
		CUIManager.CloseUI(CLuaUIResources.BabyWnd)
	end
end

-- 召唤/召回
function LuaBabyInfoView:OnSummonBtnClicked()
	if self.BabyIsSummon then
		Gac2Gas.RequestPackBaby(self.SelectedBaby.Id)
	else
		Gac2Gas.RequestSummonBaby(self.SelectedBaby.Id)
	end
	-- 请求宝宝是否跟随
	Gac2Gas.QueryBabyFollowStatus(self.SelectedBaby.Id)
end

function LuaBabyInfoView:OnAppearBtnClicked(go)
	if self.SelectedBaby and self.SelectedBaby.Status >= EnumBabyStatus.eChild then
		CBabyMgr.Inst.ShowBabyInfo = self.SelectedBaby
		CUIManager.ShowUI(CUIRes.BabyClothWnd)
	else
		g_MessageMgr:ShowMessage("BABY_NOT_OLD_ENOUGH_TO_OPEN_FASHION")
	end
end

function LuaBabyInfoView:OnDiaryBtnClicked(go)
	CUIManager.ShowUI(CLuaUIResources.BabyGrowDiaryWnd)
end

function LuaBabyInfoView:PlayerRenameBabySuccess(babyId)
	if self.SelectedBaby and self.SelectedBaby.Id == babyId then
		self.BabyNameLabel.text = self.SelectedBaby.Name
	end
end

function LuaBabyInfoView:QueryBabyParentsNameDone(babyId, fatherName, motherName)
	if self.SelectedBaby and self.SelectedBaby.Id == babyId then

		self.FatherName = fatherName
		self.MotherName = motherName

		if self.BasicGrid.transform:Find("father") then
			local fatherNode = self.BasicGrid.transform:Find("father").gameObject
			local fatherLabel = fatherNode.transform:Find("ValueLabel"):GetComponent(typeof(UILabel))
			fatherLabel.text = fatherName
		end

		if self.BasicGrid.transform:Find("mother") then
			local motherNode = self.BasicGrid.transform:Find("mother").gameObject
			local motherLabel = motherNode.transform:Find("ValueLabel"):GetComponent(typeof(UILabel))
			motherLabel.text = motherName
		end
	end
end

function LuaBabyInfoView:UpdateBabyAppearance()
	self:InitBabyInfo()
end

function LuaBabyInfoView:UpdateFollowBtn(bFollow)
	self.BabyIsSummon = bFollow
	local Logo_ZhaoHuan = self.SummonBtn.transform:Find("Logo_ZhaoHuan").gameObject
	local Logo_ZhaoHui = self.SummonBtn.transform:Find("Logo_ZhaoHui").gameObject

	if self.BabyIsSummon then
		Logo_ZhaoHuan:SetActive(false)
		Logo_ZhaoHui:SetActive(true)
	else
		Logo_ZhaoHuan:SetActive(true)
		Logo_ZhaoHui:SetActive(false)
	end
end

function LuaBabyInfoView:UpdateDiartBtn(babyId)
	if self.SelectedBaby and self.SelectedBaby.Id == babyId then
		self:UpdateDiaryBtnAlert()
	end
end

function LuaBabyInfoView:SyncBabyXiLianResult()
	if self.SelectedBaby.Status == EnumBabyStatus.eChild then
		self:UpdateBabyProperty()
	elseif self.SelectedBaby.Status == EnumBabyStatus.eYoung then
		self:UpdateBabyProperty_ShaoNian()
	end

	self:UpdateQiChang()

	if self.SelectedBaby.Props.ExtraData.IsBabyHide == 0 then
		self:SetBubbleList()
		self:ShowBubble()
	elseif self.BubbleRoot then
		self.BubbleRoot.gameObject:SetActive(false)
	end
end

function LuaBabyInfoView:PlantAndHarvestInHouseSuccess(babyId)
	if self.SelectedBaby.Id == babyId then
		self.BabyTexture.gameObject:SetActive(false)

		if self.NotAtHomeRoot then
			self.NotAtHomeRoot.gameObject:SetActive(true)
		end
		if self.BubbleRoot then
			self.BubbleRoot.gameObject:SetActive(false)
		end
	end
end

function LuaBabyInfoView:BabyPlantAndHarvestComeBack(babyId)
	if self.SelectedBaby.Id == babyId then
		self.BabyTexture.gameObject:SetActive(true)
		self.BabyTexture:Init(self.SelectedBaby, -180, false, 0)
		if self.NotAtHomeRoot then
			self.NotAtHomeRoot.gameObject:SetActive(false)
		end
		self:SetBubbleList()
		self:ShowBubble()
	end
end

function LuaBabyInfoView:OnChangeBabyStatusSuccess(babyId)
	if self.SelectedBaby.Id == babyId then
		if BabyMgr.Inst.BabyDictionary.Count == 2 then
			local portraitTexture1 = self.Portrait1Container.transform:Find("PortraitTexture"):GetComponent(typeof(CUITexture))
			local portraint1 = BabyMgr.Inst:GetBabyPortraitById(self.LeftBabyId)
			portraitTexture1:LoadNPCPortrait(portraint1, false)

			local portraitTexture2 = self.Portrait2Container.transform:Find("PortraitTexture"):GetComponent(typeof(CUITexture))
			local portraint2 = BabyMgr.Inst:GetBabyPortraitById(self.RightBabyId)
			portraitTexture2:LoadNPCPortrait(portraint2, false)
		else
			local portraitTexture = self.Portrait1Container.transform:Find("PortraitTexture"):GetComponent(typeof(CUITexture))
			local portraint = BabyMgr.Inst:GetBabyPortrait(self.SelectedBaby)
			portraitTexture:LoadNPCPortrait(portraint, false)
		end
		self.BabyTexture:Init(self.SelectedBaby, -180, false, 0)
	end
end

function LuaBabyInfoView:OnSyncTargetQiChangInfo(babyId, targetQiChangId, recommendQiChangId)
	--print("OnSyncTargetQiChangInfo",babyId, targetQiChangId, recommendQiChangId)
	self.mTargetQiChang = targetQiChangId
	self.mTargetQiChangData = Baby_QiChang.GetData(targetQiChangId)
	self:UpdateBabyProperty_ShaoNian()

	self:UpdateQiChang()
	if self.SelectedBaby.Props.ExtraData.IsBabyHide == 0 then
		self:SetBubbleList()
		self:ShowBubble()
	elseif self.BubbleRoot then
		self.BubbleRoot.gameObject:SetActive(false)
	end

end

function LuaBabyInfoView:OnActiveBabyQiChangSuccess()
	self:UpdateQiChang()
	
	if self.SelectedBaby.Props.ExtraData.IsBabyHide == 0 then
		self:SetBubbleList()
		self:ShowBubble()
	elseif self.BubbleRoot then
		self.BubbleRoot.gameObject:SetActive(false)
	end
end

function LuaBabyInfoView:OnEnable()
	g_ScriptEvent:AddListener("PlayerRenameBabySuccess", self, "PlayerRenameBabySuccess")
	g_ScriptEvent:AddListener("QueryBabyParentsNameDone", self, "QueryBabyParentsNameDone")
	g_ScriptEvent:AddListener("BabyInfoUpdate", self, "UpdateBabyAppearance")
	g_ScriptEvent:AddListener("QueryBabyFollowStatusDone", self, "UpdateFollowBtn")
	g_ScriptEvent:AddListener("BabyUnreadMsgChanged", self, "BabyUnreadMsgChanged")
	g_ScriptEvent:AddListener("AcceptTaskFromBabySuccess", self, "UpdateDiartBtn")
	g_ScriptEvent:AddListener("ReceiveQiYuAwardSuccess", self, "UpdateDiartBtn")
	g_ScriptEvent:AddListener("AcceptQiYuTaskFromBabySuccess", self, "UpdateDiartBtn")
	g_ScriptEvent:AddListener("SyncAdmonishRes", self, "SyncBabyXiLianResult")
	g_ScriptEvent:AddListener("PlantAndHarvestInHouseSuccess", self, "PlantAndHarvestInHouseSuccess")
	g_ScriptEvent:AddListener("BabyPlantAndHarvestComeBack", self, "BabyPlantAndHarvestComeBack")
	g_ScriptEvent:AddListener("ChangeBabyStatusSuccess", self, "OnChangeBabyStatusSuccess")
	--2023
	g_ScriptEvent:AddListener("SyncTargetQiChangInfo", self, "OnSyncTargetQiChangInfo")
	g_ScriptEvent:AddListener("ActiveBabyQiChangSuccess", self, "OnActiveBabyQiChangSuccess")
	g_ScriptEvent:AddListener("SyncSetTargetQiChangRes", self, "OnSyncTargetQiChangInfo")
end

function LuaBabyInfoView:OnDisable()
	g_ScriptEvent:RemoveListener("PlayerRenameBabySuccess", self, "PlayerRenameBabySuccess")
	g_ScriptEvent:RemoveListener("QueryBabyParentsNameDone", self, "QueryBabyParentsNameDone")
	g_ScriptEvent:RemoveListener("BabyInfoUpdate", self, "UpdateBabyAppearance")
	g_ScriptEvent:RemoveListener("QueryBabyFollowStatusDone", self, "UpdateFollowBtn")
	g_ScriptEvent:RemoveListener("BabyUnreadMsgChanged", self, "BabyUnreadMsgChanged")
	g_ScriptEvent:RemoveListener("AcceptTaskFromBabySuccess", self, "UpdateDiartBtn")
	g_ScriptEvent:RemoveListener("ReceiveQiYuAwardSuccess", self, "UpdateDiartBtn")
	g_ScriptEvent:RemoveListener("AcceptQiYuTaskFromBabySuccess", self, "UpdateDiartBtn")
	g_ScriptEvent:RemoveListener("SyncAdmonishRes", self, "SyncBabyXiLianResult")
	g_ScriptEvent:RemoveListener("PlantAndHarvestInHouseSuccess", self, "PlantAndHarvestInHouseSuccess")
	g_ScriptEvent:RemoveListener("BabyPlantAndHarvestComeBack", self, "BabyPlantAndHarvestComeBack")
	g_ScriptEvent:RemoveListener("ChangeBabyStatusSuccess", self, "OnChangeBabyStatusSuccess")
	--2023
	g_ScriptEvent:RemoveListener("SyncTargetQiChangInfo", self, "OnSyncTargetQiChangInfo")
	g_ScriptEvent:RemoveListener("ActiveBabyQiChangSuccess", self, "OnActiveBabyQiChangSuccess")
	g_ScriptEvent:RemoveListener("SyncSetTargetQiChangRes", self, "OnSyncTargetQiChangInfo")
end

function LuaBabyInfoView:OnDestroy()
	if self.BubbleTick ~= nil then
		UnRegisterTick(self.BubbleTick)
		self.BubbleTick = nil
	end
end

function LuaBabyInfoView:IsSkillOpen(skill)
	if CommonDefs.IS_HMT_CLIENT or CommonDefs.IS_VN_CLIENT then
		-- hmt和vn版本屏蔽宝宝说话相关技能和气场相关奖励
		if not skill then return false end
		if skill.ID == 2 or skill.ID == 4 or skill.ID == 5 then
			return false
		end
		return true
	else 
		return true
	end
end

return LuaBabyInfoView
