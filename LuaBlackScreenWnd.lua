local UIPanel = import "UIPanel"
local UITexture = import "UITexture"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local Camera = import "UnityEngine.Camera"
local Constants = import "L10.Game.Constants"
local Rect = import "UnityEngine.Rect"

LuaBlackScreenWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaBlackScreenWnd, "BGPanel", "BGPanel", UIPanel)
RegistChildComponent(LuaBlackScreenWnd, "BgTexture", "BgTexture", UITexture)

--@endregion RegistChildComponent end
RegistClassMember(LuaBlackScreenWnd, "m_FadeInTweener")
RegistClassMember(LuaBlackScreenWnd, "m_FadeOutTweener")

function LuaBlackScreenWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.BgTexture.alpha = 0
    --NGUITools.SetLayer(self.gameObject, LayerDefine.CutSceneUI)
end

function LuaBlackScreenWnd:Init()
    self:AdjustScreen()
    if LuaBlackScreenMgr.m_FadeInTime then
        self:FadeIn(LuaBlackScreenMgr.m_FadeInTime)
    end
end

function LuaBlackScreenWnd:AdjustScreen()
	if CommonDefs.IsPCGameMode() then
		local CWinSocialWndMgr = import "L10.UI.CWinSocialWndMgr"
		local camera = CUICommonDef.TraverseFindChild("Camera", self.transform):GetComponent(typeof(Camera))
		if camera then
			if CWinSocialWndMgr.Inst.m_IsWinSocialWndOpened then
                local rect = Rect(0, 0, 1 - Constants.WinSocialWndRatio, 1)
                camera.rect = rect
            else
                local rect = Rect(0, 0, 1, 1)
                camera.rect = rect
            end
		end
	end
end
--@region UIEvent

--@endregion UIEvent
function LuaBlackScreenWnd:OnEnable()
    g_ScriptEvent:AddListener("BlackSceenStartFadeIn", self, "OnBlackSceenStartFadeIn")
    g_ScriptEvent:AddListener("BlackSceenStartFadeOut", self, "OnBlackSceenStartFadeOut")
end

function LuaBlackScreenWnd:OnDisable()
    g_ScriptEvent:RemoveListener("BlackSceenStartFadeIn", self, "OnBlackSceenStartFadeIn")
    g_ScriptEvent:RemoveListener("BlackSceenStartFadeOut", self, "OnBlackSceenStartFadeOut")
end

function LuaBlackScreenWnd:OnBlackSceenStartFadeIn(time)
    self:FadeIn(time)
end

function LuaBlackScreenWnd:OnBlackSceenStartFadeOut(time)
    self:FadeOut(time)
end

function LuaBlackScreenWnd:FadeIn(fadeInTime)
    self:ClearTweener()
    self.m_FadeInTweener = LuaTweenUtils.TweenAlpha(self.BgTexture, 0, 1, fadeInTime, function() 
        self.m_FadeInTweener = nil
    end)
end

function LuaBlackScreenWnd:FadeOut(fadeOutTime)
    self:ClearTweener()
    self.m_FadeOutTweener = LuaTweenUtils.TweenAlpha(self.BgTexture, 1, 0, fadeOutTime, function() 
        self.m_FadeOutTweener = nil
        CUIManager.CloseUI(CLuaUIResources.BlackScreenWnd)
    end)
end

function LuaBlackScreenWnd:ClearTweener()
    if self.m_FadeInTweener then
        LuaTweenUtils.Kill(self.m_FadeInTweener, false)
    end
    if self.m_FadeOutTweener then
        LuaTweenUtils.Kill(self.m_FadeOutTweener, false)
    end
end

function LuaBlackScreenWnd:OnDestroy()
    self:ClearTweener()
    LuaBlackScreenMgr:Reset()
end