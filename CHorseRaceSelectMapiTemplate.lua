-- Auto Generated!!
local CHorseRaceSelectMapiTemplate = import "L10.UI.CHorseRaceSelectMapiTemplate"
local LocalString = import "LocalString"
local ZuoQi_MapiType = import "L10.Game.ZuoQi_MapiType"
local ZuoQi_Setting = import "L10.Game.ZuoQi_Setting"
CHorseRaceSelectMapiTemplate.m_Init_CS2LuaHook = function (this, data) 
    this.data = data
    --this.name.text = data.mapiName
    local nameLabel = this.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    nameLabel.text = data.mapiName
    
    this.level.text = System.String.Format(LocalString.GetString("{0}级"), data.mapiLevel)
    this:OnRequestSelectHorseResult()
    this.select.enabled = false
    local zqType = ZuoQi_MapiType.GetData(data.quality)
    if zqType ~= nil then
        if data.mapiInfo ~= nil then
            if data.quality == 4 then
                this.icon:LoadMaterial(ZuoQi_Setting.GetData().SpecialMapiIcon[0])
            elseif data.quality == 5 then
                this.icon:LoadMaterial(ZuoQi_Setting.GetData().SpecialMapiIcon[1])
            else
                this.icon:LoadMaterial(ZuoQi_Setting.GetData().MapiIcon[data.mapiInfo.FuseId])
            end
        end
        this.type.text = zqType.Name
    else
        this.type.text = ""
        this.icon.material = nil
    end
end
