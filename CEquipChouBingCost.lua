-- Auto Generated!!
local AlignType = import "L10.UI.CTooltip+AlignType"
local AlignType1 = import "L10.UI.CItemInfoMgr+AlignType"
local CEquipChouBingCost = import "L10.UI.CEquipChouBingCost"
local CEquipmentProcessMgr = import "L10.Game.CEquipmentProcessMgr"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local EventDelegate = import "EventDelegate"
local Item_Item = import "L10.Game.Item_Item"
local LocalString = import "LocalString"
local Object = import "System.Object"
local UIEventListener = import "UIEventListener"
CEquipChouBingCost.m_Awake_CS2LuaHook = function (this) 
    if this.toggle ~= nil then
        CommonDefs.ListAdd(this.toggle.onChange, typeof(EventDelegate), CreateFromClass(EventDelegate, DelegateFactory.Callback(function () 
            this:UpdateCount()
            if this.toggle.value then
                this.nameLabel.text = this.templateName .. LocalString.GetString("（绑定）")
            else
                this.nameLabel.text = this.templateName
            end
        end)))
    end
    if this.btn ~= nil then
        UIEventListener.Get(this.btn).onClick = DelegateFactory.VoidDelegate(function (p) 
            if this.getGo.activeInHierarchy then
                CItemAccessListMgr.Inst:ShowItemAccessInfo(this.templateId, false, this.transform, AlignType.Right)
            else
                if this.templateId > 0 then
                    CItemInfoMgr.ShowLinkItemTemplateInfo(this.templateId, false, nil, AlignType1.Default, 0, 0, 0, 0)
                end
            end
        end)
    end
end
CEquipChouBingCost.m_Init_CS2LuaHook = function (this) 
    this.costNum = 0
    local equip = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
    local cost = CEquipmentProcessMgr.Inst:GetBaptizeCost(equip)
    if cost ~= nil then
        if cost.ChouBingCost ~= nil then
            this.templateId = cost.ChouBingCost[0]
            this.costNum = cost.ChouBingCost[1]
            local template = Item_Item.GetData(this.templateId)
            this.icon:LoadMaterial(template.Icon)
            this:UpdateCount()
            this.templateName = template.Name
            this.nameLabel.text = template.Name
            if not this.isBinded then
                this.nameLabel.text = System.String.Format(LocalString.GetString("{0}(非绑定)"), template.Name)
            end

            if this.toggleLabel ~= nil then
                this.toggleLabel.text = LocalString.GetString("只使用绑定的") .. this.templateName
            end
        else
            this.templateId = 0
            this.costNum = 0
            this.icon:Clear()
            this.countLabel.text = ""
            this.nameLabel.text = ""
            if this.toggleLabel ~= nil then
                this.toggleLabel.text = ""
            end
            this.getGo:SetActive(false)
        end
    else
        this.countLabel.text = ""
        this.nameLabel.text = ""
        this.getGo:SetActive(false)
    end
    this:UpdateCount()
end
CEquipChouBingCost.m_UpdateCost_CS2LuaHook = function (this) 
    local equip = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
    local cost = CEquipmentProcessMgr.Inst:GetBaptizeCost(equip)
    if cost ~= nil then
        if cost.ChouBingCost ~= nil then
            this.costNum = cost.ChouBingCost[1]
        else
            this.costNum = 0
        end
    end
end
CEquipChouBingCost.m_OnSendItem_CS2LuaHook = function (this, itemId) 
    local item = CItemMgr.Inst:GetById(itemId)

    if item ~= nil and this.templateId == item.TemplateId then
        this:UpdateCount()
    end
end
CEquipChouBingCost.m_OnSetItemAt_CS2LuaHook = function (this, place, position, oldItemId, newItemId) 
    if oldItemId == nil and newItemId == nil then
        return
    end
    local default
    if oldItemId ~= nil then
        default = oldItemId
    else
        default = newItemId
    end
    local itemId = default

    this:OnSendItem(itemId)
end
CEquipChouBingCost.m_UpdateCount_CS2LuaHook = function (this) 
    this:UpdateCost()
    local bindItemCountInBag, notBindItemCountInBag
    bindItemCountInBag, notBindItemCountInBag = CItemMgr.Inst:CalcItemCountInBagByTemplateId(this.templateId)
    local count = bindItemCountInBag + notBindItemCountInBag

    if this.onlyCostBindItem then
        count = bindItemCountInBag
    end
    if not this.isBinded then
        count = notBindItemCountInBag
    end
    if count < this.costNum then
        this.countLabel.text = System.String.Format("[ff0000]{0}[-]/{1}", count, this.costNum)
        if this.getGo ~= nil then
            this.getGo:SetActive(true)
        end
    else
        this.countLabel.text = System.String.Format("{0}/{1}", count, this.costNum)

        if this.getGo ~= nil then
            this.getGo:SetActive(false)
        end
    end

    if this.onChange ~= nil then
        GenericDelegateInvoke(this.onChange, Table2ArrayWithCount({count >= this.costNum}, 1, MakeArrayClass(Object)))
    end
end
