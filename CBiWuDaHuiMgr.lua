-- Auto Generated!!
local BiWu_Setting = import "L10.Game.BiWu_Setting"
local BiWuChampionInfo = import "L10.Game.BiWuChampionInfo"
local BiWuChampionMemberInfo = import "L10.Game.BiWuChampionMemberInfo"
local BiWuSelectPlayerResult = import "L10.Game.BiWuSelectPlayerResult"
local CBiWuDaHuiMgr = import "L10.Game.CBiWuDaHuiMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CommonDefs = import "L10.Game.CommonDefs"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local LocalString = import "LocalString"
local MsgPackImpl = import "MsgPackImpl"
local Object = import "System.Object"
local UIntUIntKeyValuePair = import "L10.Game.UIntUIntKeyValuePair"
local CTeamMgr = import "L10.Game.CTeamMgr"
local CScene=import "L10.Game.CScene"

LuaBiWuDaHuiMgr = {}
LuaBiWuDaHuiMgr.myTeamLeaderId = 0
LuaBiWuDaHuiMgr.myTeamId = 0

function LuaBiWuDaHuiMgr.IsInBiWuCross()
    if not CScene.MainScene then 
        return false 
    end

    local sid = CScene.MainScene.SceneTemplateId
    return sid == 16102826 or sid == 16000492
end

function LuaBiWuDaHuiMgr.IsInBiWuPrepare()
    if not CScene.MainScene then 
        return false 
    end

    local sid = CScene.MainScene.SceneTemplateId
    return sid == 16102827
end


function LuaBiWuDaHuiMgr.IsLeader()
    local iscross = LuaBiWuDaHuiMgr.IsInBiWuCross() or LuaBiWuDaHuiMgr.IsInBiWuPrepare()
    if iscross then
        if not CClientMainPlayer.Inst then
            return false
        else
            return CClientMainPlayer.Inst.Id == LuaBiWuDaHuiMgr.myTeamLeaderId
        end
    else
        --队长可能更换，所以要用teammgr中的信息来判断
        return CTeamMgr.Inst:MainPlayerIsTeamLeader()
    end
end

CBiWuDaHuiMgr.m_OnMainPlayerCreated_CS2LuaHook = function (this) 
    Gac2Gas.QueryBiWuSignUpInfo()
end
CBiWuDaHuiMgr.m_Reset_CS2LuaHook = function (this) 
    this.ApplyState = false
    LuaBiWuDaHuiMgr.myTeamId = 0
    LuaBiWuDaHuiMgr.myTeamLeaderId = 0
end
CBiWuDaHuiMgr.m_IsTeamRound_CS2LuaHook = function (round) 
    local data = BiWu_Setting.GetData()
    local index = round - 1
    if index >= 0 and index < data.RoundToPlayerNum.Length then
        return data.RoundToPlayerNum[index] > 1
    end
    return true
end
CBiWuDaHuiMgr.m_GetMyStage_CS2LuaHook = function (this) 
    if CClientMainPlayer.Inst ~= nil then
        local level = CClientMainPlayer.Inst.Level
        if level >= 50 and level < 70 then
            return 1
        elseif level >= 70 and level < 90 then
            return 2
        elseif level >= 90 and level < 110 then
            return 3
        elseif level >= 110 and level < 130 then
            return 4
        elseif level >= 130 then
            return 5
        end
    end

    return 0
end
CBiWuDaHuiMgr.m_GetStageDesc_CS2LuaHook = function (this, stage) 
    repeat
        local default = stage
        if default == 1 then
            return LocalString.GetString("新锐积分")
        elseif default == 2 then
            return LocalString.GetString("英武积分")
        elseif default == 3 then
            return LocalString.GetString("神勇积分")
        elseif default == 4 then
            return LocalString.GetString("天罡积分")
        elseif default == 5 then
            return LocalString.GetString("天元积分")
        end
    until 1
    return LocalString.GetString("积分")
end
CBiWuDaHuiMgr.m_GetStageFullDesc_CS2LuaHook = function (this, stage) 
    repeat
        local default = stage
        if default == 1 then
            return LocalString.GetString("新锐积分(50级~69级)")
        elseif default == 2 then
            return LocalString.GetString("英武积分(70级~89级)")
        elseif default == 3 then
            return LocalString.GetString("神勇积分(90级~109级)")
        elseif default == 4 then
            return LocalString.GetString("天罡积分(110级~129级)")
        elseif default == 5 then
            return LocalString.GetString("天元积分(130级以上)")
        end
    until 1
    return LocalString.GetString("积分")
end
Gas2Gac.QueryBiWuScoreResult = function (scoreInfo_U) 
    local rtn = CreateFromClass(MakeGenericClass(List, UIntUIntKeyValuePair))

    local curScore = 0
    local curStage = CBiWuDaHuiMgr.Inst:GetMyStage()
    local scoreInfo = TypeAs(MsgPackImpl.unpack(scoreInfo_U), typeof(MakeGenericClass(List, Object)))
    do
        local i = 0
        while i < scoreInfo.Count do
            local stage = math.floor(tonumber(scoreInfo[i] or 0))
            local score = math.floor(tonumber(scoreInfo[i + 1] or 0))
            CommonDefs.ListAdd(rtn, typeof(UIntUIntKeyValuePair), CreateFromClass(UIntUIntKeyValuePair, stage, score))
            if stage == curStage then
                curScore = score
            end
            i = i + 2
        end
    end
    CBiWuDaHuiMgr.Inst.historyScore = rtn
    EventManager.BroadcastInternalForLua(EnumEventType.QueryBiWuScoreResult, {rtn})
    EventManager.BroadcastInternalForLua(EnumEventType.QueryPlayerCurrentBiWuScore, {curScore})
end
Gas2Gac.UpdateBiWuTeamSelectResult = function (teamId, teamSelect_U) 
    local rtn = CreateFromClass(MakeGenericClass(List, BiWuSelectPlayerResult))
    local selectResult = TypeAs(MsgPackImpl.unpack(teamSelect_U), typeof(MakeGenericClass(List, Object)))
    do
        local i = 0
        while i < selectResult.Count do
            local item = CreateFromClass(BiWuSelectPlayerResult)
            item.round = math.floor(tonumber(selectResult[i] or 0))
            item.playerId = math.floor(tonumber(selectResult[i + 1] or 0))
            item.teamId = teamId
            CommonDefs.ListAdd(rtn, typeof(BiWuSelectPlayerResult), item)
            i = i + 2
        end
    end
    CBiWuDaHuiMgr.Inst.updateSelectionList = rtn
    EventManager.BroadcastInternalForLua(EnumEventType.UpdateBiWuTeamSelectResult, {rtn})
end
Gas2Gac.QueryBiWuChampionInfoResult = function (stage, teamInfo_U) 
    local teamInfo = TypeAs(MsgPackImpl.unpack(teamInfo_U), typeof(MakeGenericClass(List, Object)))
    local list = CreateFromClass(MakeGenericClass(List, BiWuChampionInfo))
    do
        local i = 0
        while i < teamInfo.Count do
            local teamMemberInfo = TypeAs(teamInfo[i], typeof(MakeGenericClass(List, Object)))
            if teamMemberInfo.Count > 1 then
                local info = CreateFromClass(BiWuChampionInfo)
                info.leaderName = ToStringWrap(teamMemberInfo[0])
                local memberInfo = TypeAs(teamMemberInfo[1], typeof(MakeGenericClass(List, Object)))
                do
                    local j = 0
                    while j < memberInfo.Count do
                        local memeber = CreateFromClass(BiWuChampionMemberInfo)
                        memeber.playerdId = math.floor(tonumber(memberInfo[j] or 0))
                        memeber.name = ToStringWrap(memberInfo[j + 1])
                        memeber.grade = math.floor(tonumber(memberInfo[j + 2] or 0))
                        memeber.role = math.floor(tonumber(memberInfo[j + 3] or 0))
                        CommonDefs.ListAdd(info.members, typeof(BiWuChampionMemberInfo), memeber)
                        j = j + 4
                    end
                end
                CommonDefs.ListAdd(list, typeof(BiWuChampionInfo), info)
            end
            i = i + 1
        end
    end
    EventManager.BroadcastInternalForLua(EnumEventType.QueryBiWuChampionInfoResult, {list})
end
