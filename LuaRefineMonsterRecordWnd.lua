local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local CChatLinkMgr = import "CChatLinkMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaRefineMonsterRecordWnd = class()
RegistClassMember(LuaRefineMonsterRecordWnd,"m_TableViewDataSource")
RegistClassMember(LuaRefineMonsterRecordWnd,"m_TableView")
RegistClassMember(LuaRefineMonsterRecordWnd,"m_RecordInfo")
RegistClassMember(LuaRefineMonsterRecordWnd,"m_VoidLabel")
RegistClassMember(LuaRefineMonsterRecordWnd,"m_RecordData")

function LuaRefineMonsterRecordWnd:Init()
    Gac2Gas.QueryLianHuaItemSubmitHistroy()
    self.m_TableView = self.transform:Find("Anchor/TableView"):GetComponent(typeof(QnTableView))
    self.m_VoidLabel = self.transform:Find("Anchor/VoidLabel").gameObject

    local function initItem(item,index)
        self:InitItem(item,index)
    end

    self.m_TableViewDataSource = DefaultTableViewDataSource.CreateByCount(0,initItem)
    self.m_TableView.m_DataSource = self.m_TableViewDataSource
    self.m_TableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        self:OnSelectAtRow(row)
    end)

    self.m_RecordInfo = nil
end

function LuaRefineMonsterRecordWnd:OnSelectAtRow(row)
end

function LuaRefineMonsterRecordWnd:InitItem(item,index)
    -- 节点
    local tf = item.transform
    local timeLabel = FindChild(tf,"TimeLabel"):GetComponent(typeof(UILabel))
    local content = FindChild(tf,"Content"):GetComponent(typeof(UILabel))
    
    -- 数据
    local recordData = self.m_RecordData[index+1]
    local ztime = CServerTimeMgr.ConvertTimeStampToZone8Time(recordData.Time)
    local itemData = LianHua_Item.GetData(recordData.TemplateId)
    
    -- 信息初始化
    timeLabel.text = ToStringWrap(ztime, "yyyy-MM-dd")

    local contentLink = g_MessageMgr:FormatMessage("LianHua_Item_Record_Desc", recordData.OwnerId, recordData.OwnerName, tostring(recordData.TemplateId), itemData.Quality) 
    --SafeStringFormat3(LocalString.GetString("<link player=%d,%s>提交了<link item=%s>，评分%d"), recordData.OwnerId, recordData.OwnerName, tostring(recordData.TemplateId), itemData.Quality)
    content.text = CChatLinkMgr.TranslateToNGUIText(contentLink, false)

    UIEventListener.Get(content.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
        local url = p:GetComponent(typeof(UILabel)):GetUrlAtPosition(UICamera.lastWorldPosition)
        if url ~= nil then
            CChatLinkMgr.ProcessLinkClick(url, nil)
        end
    end)
end

function LuaRefineMonsterRecordWnd:InitInfo(info)
    self.m_RecordInfo = info
    self.m_RecordData = {}

    for i=0, info.Count-6, 6 do
        local m = {}
        m.Time = info[i]
        m.LocalId = info[i+1]
        m.OwnerId = info[i+2]
        if info[i+3] and  #info[i+3]>0 then
            m.OwnerName = info[i+3]
        else
            m.OwnerName = " "
        end
        m.TemplateId = info[i+4]
        m.Rank = info[i+5]
        m.Index = i
        table.insert(self.m_RecordData, m)
    end

    table.sort(self.m_RecordData, function(a, b)
        local at = a.Time + a.LocalId
        local bt = b.Time + b.LocalId
        if at ~= bt then
            return at > bt
        else
            return a.Index < b.Index
        end
    end)

    if info.Count >= 6 then
        self.m_VoidLabel:SetActive(false)
        self.m_TableViewDataSource.count= info.Count/6
        self.m_TableView:ReloadData(true,false)
    else
        self.m_VoidLabel:SetActive(true)
        self.m_TableViewDataSource.count= 0
        self.m_TableView:ReloadData(true,false)
    end
end

function LuaRefineMonsterRecordWnd:OnEnable()
	g_ScriptEvent:AddListener("QueryLianHuaItemSubmitHistroyResult", self, "InitInfo")
end

function LuaRefineMonsterRecordWnd:OnDisable()
	g_ScriptEvent:RemoveListener("QueryLianHuaItemSubmitHistroyResult", self, "InitInfo")
end
