-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CGuildDevelopDetailView_Main = import "L10.UI.CGuildDevelopDetailView_Main"
local CGuildMgr = import "L10.Game.CGuildMgr"
local Color = import "UnityEngine.Color"
local CommonDefs = import "L10.Game.CommonDefs"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local LocalString = import "LocalString"
local String = import "System.String"
CGuildDevelopDetailView_Main.m_OnClickUpgradeButton_CS2LuaHook = function (this, go) 
    -- TODO zzm 升级
    -- TODO zzm 升级
    if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.BasicProp.GuildId ~= 0 then
        Gac2Gas.RequestOperationInGuild(CClientMainPlayer.Inst.BasicProp.GuildId, "SizeLevel", "", "")
    end
end
CGuildDevelopDetailView_Main.m_OnRequestOperationInGuildSucceed_CS2LuaHook = function (this, requestType, paramStr) 
    -- 房屋 建设成功 
    -- 房屋 建设成功 
    if (CGuildMgr.BUILD_TYPE_SizeLevel == requestType) then
        CGuildMgr.Inst.canUpdateSizeLevel = false

        --更新剩下的时间
        local updateTime = CGuildMgr.Inst:GetUpdateTime(CGuildDevelopDetailView_Main.UPDATE_TYPE)

        local nowTime = CServerTimeMgr.Inst:GetZone8Time()
        local d = nowTime:AddSeconds(updateTime - 1)
        local duration = CommonDefs.op_Subtraction_DateTime_DateTime(d, nowTime)
        this.labelBuildingTime.text = System.String.Format(CGuildDevelopDetailView_Main.TimeFormat, duration.Days, duration.Hours, duration.Minutes)
        this.upgradeBtn.Enabled = false
        this.upgradeBtn.label.text = LocalString.GetString("正在升级")

        if CGuildMgr.Inst.m_GuildInfo ~= nil then
            CGuildMgr.Inst.m_GuildInfo.CurBuild = requestType
            CGuildMgr.Inst.m_GuildInfo.CurBuildStartTime = math.floor(CServerTimeMgr.Inst.timeStamp)
            CGuildMgr.Inst:GetMyGuildInfo()
        end
    end
end
CGuildDevelopDetailView_Main.m_Init_CS2LuaHook = function (this) 

    -- 等级变量
    this.levelLabel.text = System.String.Format(LocalString.GetString("{0}级帮会"), CGuildMgr.Inst.m_GuildInfo.Scale)
    this.limitLabel.text = System.String.Format(LocalString.GetString("房屋、金库、书院个数上限为{0}个"), CGuildMgr.Inst:GetMaxBuilding())
    this.upgradeBtn.Enabled = true

    -- 升级资金
    CGuildMgr.Inst.canUpdateSizeLevel = true
    local satify = true
    local costSilver = CGuildMgr.Inst:GetUpdateCost(CGuildDevelopDetailView_Main.UPDATE_TYPE)
    -- 升级条件1, 帮会资金
    local totalSilver = costSilver + 2 * CGuildMgr.Inst:GetMaintainCost()
    if CGuildMgr.Inst:GetSilver() < totalSilver then
        satify = false
    end

    if CGuildMgr.Inst:GetSilver() < costSilver then
        this.upgradeCostLabel.text = System.String.Format(LocalString.GetString("帮会资金[ff0000]{0}[-]"), costSilver)
    else
        this.upgradeCostLabel.text = System.String.Format(LocalString.GetString("帮会资金[00ff00]{0}[-]"), costSilver)
        --橙色？？
    end

    this.needLabel.text = System.String.Format("{0}/{1}", CGuildMgr.Inst:GetSilver(), totalSilver)
    this.needSlider.value = (CGuildMgr.Inst:GetSilver() / totalSilver)

    -- 升级条件2, 建筑数量
    local buildingReachLimit = 0
    if CGuildMgr.Inst.m_GuildInfo.WingRoomNum >= CGuildMgr.Inst:GetMaxBuilding() then
        buildingReachLimit = buildingReachLimit + 1
    end
    if CGuildMgr.Inst.m_GuildInfo.SilverStoreNum >= CGuildMgr.Inst:GetMaxBuilding() then
        buildingReachLimit = buildingReachLimit + 1
    end
    if CGuildMgr.Inst.m_GuildInfo.SanctumNum >= CGuildMgr.Inst:GetMaxBuilding() then
        buildingReachLimit = buildingReachLimit + 1
    end

    if buildingReachLimit >= 2 then
        this.requireLabel.color = Color.green
    else
        this.requireLabel.color = Color.red
        satify = false
    end
    this.requireLabel.text = System.String.Format(LocalString.GetString("2类建筑达到{0}个"), CGuildMgr.Inst:GetMaxBuilding())

    if not satify then
        this.upgradeBtn.Enabled = false
        if CGuildMgr.Inst ~= nil then
            CGuildMgr.Inst.canUpdateSizeLevel = false
        end
    end
    -- 升级时间

    local updateTime = CGuildMgr.Inst:GetUpdateTime(CGuildDevelopDetailView_Main.UPDATE_TYPE)

    if (CGuildMgr.Inst.m_GuildInfo.CurBuild == CGuildMgr.BUILD_TYPE_SizeLevel) then
        local startTime = CServerTimeMgr.ConvertTimeStampToZone8Time(CGuildMgr.Inst.m_GuildInfo.CurBuildStartTime)
        local nowTime = CServerTimeMgr.Inst:GetZone8Time()
        local duration = CommonDefs.op_Subtraction_DateTime_DateTime(startTime:AddSeconds(updateTime), nowTime)
        local totalSeconds = duration.TotalSeconds

        this.upgradeBtn.label.text = LocalString.GetString("正在升级")
        if totalSeconds > 0 then
            CGuildMgr.Inst.canUpdateSizeLevel = false
            this.labelBuildingTime.text = System.String.Format(CGuildDevelopDetailView_Main.TimeFormat, duration.Days, duration.Hours, duration.Minutes)
        end
    else
        local timeStr = LocalString.GetString("需要")
        if math.floor(updateTime / 3600) > 0 then
            if math.floor((updateTime % 3600) / 60) > 0 then
                timeStr = SafeStringFormat3(LocalString.GetString("需要%s小时%s分钟"), tostring(math.floor(updateTime / 3600)), tostring((math.floor((updateTime % 3600) / 60))))
            else
                timeStr = SafeStringFormat3(LocalString.GetString("需要%s小时"), tostring(math.floor(updateTime / 3600)))
            end
        end
        
        this.labelBuildingTime.text = timeStr
        this.upgradeBtn.label.text = LocalString.GetString("开始升级")
    end

    -- 升级按钮是否可按
    local curBuild = CGuildMgr.Inst.m_GuildInfo.CurBuild
    if not CGuildMgr.Inst.canUpdateSizeLevel or not ((curBuild=="" or curBuild==nil) or (CGuildMgr.BUILD_TYPE_SizeLevel == curBuild)) then
        this.upgradeBtn.Enabled = false
    end

    -- 判断满级的情况
    local stringLimitMax = LocalString.GetString("已满级")
    if CGuildMgr.Inst.m_GuildInfo.Scale >= CGuildMgr.Inst:GetMaxScale_All() then
        this.requireLabel.text = stringLimitMax
        this.requireLabel.color = Color.green
        this.labelBuildingTime.text = stringLimitMax
        this.upgradeBtn.Enabled = false
    end
end
CGuildDevelopDetailView_Main.m_OnEnable_CS2LuaHook = function (this) 
    EventManager.AddListenerInternal(EnumEventType.RequestOperationInGuildSucceed, MakeDelegateFromCSFunction(this.OnRequestOperationInGuildSucceed, MakeGenericClass(Action2, String, String), this))

    if CGuildMgr.Inst.m_GuildInfo == nil then
        return
    end

    this:Init()
end
