local UISlider = import "UISlider"

LuaKeJuOptionTemplate = class()

RegistChildComponent(LuaKeJuOptionTemplate,"m_Label","Label", UILabel)
RegistChildComponent(LuaKeJuOptionTemplate,"m_RightTip","RightTip", GameObject)
RegistChildComponent(LuaKeJuOptionTemplate,"m_WrongTip","WrongTip", GameObject)
RegistChildComponent(LuaKeJuOptionTemplate,"m_Slider","Slider", UISlider)
RegistChildComponent(LuaKeJuOptionTemplate,"m_BrightProgressBar","BrightProgressBar", UIWidget)
RegistChildComponent(LuaKeJuOptionTemplate,"m_LacklusterProgressBar","LacklusterProgressBar", UIWidget)

RegistClassMember(LuaKeJuOptionTemplate, "m_Idx")
RegistClassMember(LuaKeJuOptionTemplate, "m_IsSelected")
RegistClassMember(LuaKeJuOptionTemplate, "m_IsRight")

function LuaKeJuOptionTemplate:Awake()
    self.m_Slider.value = 0
    self.m_BrightProgressBar.gameObject:SetActive(false)
    self.m_LacklusterProgressBar.gameObject:SetActive(true)
    self.m_Slider.foregroundWidget = self.m_LacklusterProgressBar
end

function LuaKeJuOptionTemplate:OnEnable()
    g_ScriptEvent:AddListener("KeJuQuestionAnswerUpdatePercentage", self, "UpdatePercentage")
end

function LuaKeJuOptionTemplate:OnDisable()
    g_ScriptEvent:RemoveListener("KeJuQuestionAnswerUpdatePercentage", self, "UpdatePercentage")
end

function LuaKeJuOptionTemplate:UpdatePercentage(data, questionId)
    if self.m_Idx < data.Count then
        self.m_Slider.value = data[self.m_Idx]
        if self.m_IsSelected then
            g_MessageMgr:ShowMessage(self.m_IsRight and "KeJu_PercentOfPlayers_CorrectAnswer" or "KeJu_PercentOfPlayers_WrongAnswer",
                    math.floor(self.m_Slider.value * 100))
        end
    end
end

function LuaKeJuOptionTemplate:Init(data)
    local title,idx = data[0], data[1]
    self.m_Idx = idx
    self.m_Label.text = title
    self.m_RightTip:SetActive(false)
    self.m_WrongTip:SetActive(false)
    self.m_Slider.value = 0
    self.m_BrightProgressBar.gameObject:SetActive(false)
    self.m_LacklusterProgressBar.gameObject:SetActive(true)
    self.m_Slider.foregroundWidget = self.m_LacklusterProgressBar
end

function LuaKeJuOptionTemplate:SetResult(right)
    self.m_IsRight = right
    self.m_RightTip:SetActive(right)
    self.m_WrongTip:SetActive(right == false)
end

--玩家选中的
function LuaKeJuOptionTemplate:SetSelected()
    self.m_BrightProgressBar.gameObject:SetActive(true)
    self.m_LacklusterProgressBar.gameObject:SetActive(false)
    self.m_Slider.foregroundWidget = self.m_BrightProgressBar
    self.m_IsSelected = true
end
