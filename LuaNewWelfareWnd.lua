require("3rdParty/ScriptEvent")
require("common/common_include")

local UIGrid = import "UIGrid"
local UIScrollView = import "UIScrollView"
local CWelfareBonusMgr = import "L10.Game.CWelfareBonusMgr"
local CWelfareAlertCtl = import "L10.UI.CWelfareAlertCtl"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local Extensions = import "Extensions"
local EnumWelfareBonusCategory = import "L10.Game.EnumWelfareBonusCategory"
local CWelfareMgr = import "L10.UI.CWelfareMgr"
local MsgPackImpl = import "MsgPackImpl"
local Object = import "System.Object"
local CChargeActivityWindow = import "L10.UI.CChargeActivityWindow"
local CSigninMgr = import "L10.Game.CSigninMgr"
local CJiFenWebMgr = import "L10.Game.CJiFenWebMgr"
local CSwitchMgr = import "L10.Engine.CSwitchMgr"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local CUIFx = import "L10.UI.CUIFx"
local CUITexture = import "L10.UI.CUITexture"
local CLoginMgr = import "L10.Game.CLoginMgr"
local CChargeWnd = import "L10.UI.CChargeWnd"
local CPropertyItem = import "L10.Game.CPropertyItem"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local DateTime = import "System.DateTime"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"

LuaNewWelfareWnd = class()

--子界面
RegistChildComponent(LuaNewWelfareWnd, "MainWelafareWnd", GameObject)
RegistChildComponent(LuaNewWelfareWnd, "FirstChargGiftWindow", GameObject)
RegistChildComponent(LuaNewWelfareWnd, "BonusWindow", GameObject)
RegistChildComponent(LuaNewWelfareWnd, "QnReturnWindow", GameObject)
RegistChildComponent(LuaNewWelfareWnd, "OtherWelfareWindow", GameObject)
RegistChildComponent(LuaNewWelfareWnd, "ChargeAwardWindow", GameObject)
RegistChildComponent(LuaNewWelfareWnd, "FashionGiftWindow", GameObject)
RegistChildComponent(LuaNewWelfareWnd, "BabyFundWindow", GameObject)
RegistChildComponent(LuaNewWelfareWnd, "NeteaseMemberWindow", GameObject)
RegistChildComponent(LuaNewWelfareWnd, "YunyingfuliWindow", GameObject)

-- Tab按钮
RegistChildComponent(LuaNewWelfareWnd, "TabTable", UIGrid)
RegistChildComponent(LuaNewWelfareWnd, "TabScrollView", UIScrollView)
RegistChildComponent(LuaNewWelfareWnd, "ButtonTemplate", GameObject)
RegistChildComponent(LuaNewWelfareWnd, "ButtonTemplate_Yellow", GameObject)
RegistChildComponent(LuaNewWelfareWnd, "ButtonTemplate3", GameObject)
RegistChildComponent(LuaNewWelfareWnd, "ButtonTemplate4", GameObject)
RegistChildComponent(LuaNewWelfareWnd, "ButtonTemplate_SpringFestival", GameObject)

--新加子界面
RegistChildComponent(LuaNewWelfareWnd, "ZhuanzhiMonthCardReturnWindow", GameObject)
RegistChildComponent(LuaNewWelfareWnd, "Carnival2020Window", GameObject)
RegistChildComponent(LuaNewWelfareWnd, "HuiLiuFundWindow", GameObject)
RegistChildComponent(LuaNewWelfareWnd, "SpokesmanZhouBianWindow", GameObject)
RegistChildComponent(LuaNewWelfareWnd, "ZhuanZhiFundWindow", GameObject)
RegistChildComponent(LuaNewWelfareWnd, "HuiLiuFuliWindow", GameObject)
RegistChildComponent(LuaNewWelfareWnd, "Carnival2021Window", GameObject)
RegistChildComponent(LuaNewWelfareWnd, "AppRateWindow", GameObject)
RegistChildComponent(LuaNewWelfareWnd, "SocialFollowWindow", GameObject)
RegistChildComponent(LuaNewWelfareWnd, "BuyBookWnd", GameObject)

-- 几个按钮
RegistChildComponent(LuaNewWelfareWnd, "SignInButton", GameObject)
RegistChildComponent(LuaNewWelfareWnd, "SignInFx", CUIFx)

RegistClassMember(LuaNewWelfareWnd, "WelfareList")	-- Welfare_Welfare数据
RegistClassMember(LuaNewWelfareWnd, "ButtonList") -- tab按钮的list<qnButton>
RegistClassMember(LuaNewWelfareWnd, "OnClickList") -- <quButton, onClick>

RegistClassMember(LuaNewWelfareWnd, "SelectIndex") -- 选中的tab index，签到为-1
RegistClassMember(LuaNewWelfareWnd, "WindowIdxList")
RegistClassMember(LuaNewWelfareWnd, "ChargeActivityTabIdx") --充值活动的tab idx
RegistClassMember(LuaNewWelfareWnd, "PlatformValid") -- 用于是否关闭历史返利
RegistClassMember(LuaNewWelfareWnd, "FashionGiftInited") -- 折扣礼包界面是否初始化过
RegistClassMember(LuaNewWelfareWnd, "BabyInitButton")
RegistClassMember(LuaNewWelfareWnd, "BabyFundButtonIndex")
RegistClassMember(LuaNewWelfareWnd, "BabyFundBtn")
RegistClassMember(LuaNewWelfareWnd, "HomePageIdx")

-- 不同子界面的Idx
RegistClassMember(LuaNewWelfareWnd, "OnlineTimeBtnIndex") -- 在线时间
RegistClassMember(LuaNewWelfareWnd, "HolidayOnlineTimeBtnIndex") --节日在线时间
RegistClassMember(LuaNewWelfareWnd, "HolidaySignInBtnIndex") --节日签到
RegistClassMember(LuaNewWelfareWnd, "DesignCompetitionBtnIndex") --节日签到
RegistClassMember(LuaNewWelfareWnd, "LvUpBtnIndex")	-- 升级
RegistClassMember(LuaNewWelfareWnd, "OtherWelfareIndex") -- 其他奖励
RegistClassMember(LuaNewWelfareWnd, "QnReturnIndex") -- 历史返利
RegistClassMember(LuaNewWelfareWnd, "HuiliuIndex") -- 回流奖励
RegistClassMember(LuaNewWelfareWnd, "YunYingFuLiBtn") -- 运营福利按钮
RegistClassMember(LuaNewWelfareWnd, "m_NeteaseMemberButton") --网易会员按钮
RegistClassMember(LuaNewWelfareWnd, "Carnival2020Inited") -- 折扣礼包界面是否初始化过
RegistClassMember(LuaNewWelfareWnd, "SpokesmanZhouBianInited") -- 代言人周边是否初始化过
RegistClassMember(LuaNewWelfareWnd, "HuiLiuFundIndex") --回流基金
RegistClassMember(LuaNewWelfareWnd, "ZhuanZhiFundIndex") -- 转职基金
RegistClassMember(LuaNewWelfareWnd, "Carnival2021Inited") -- 2021嘉年华售票界面是否初始化过
RegistClassMember(LuaNewWelfareWnd, "RateAPPIndex") -- app评分
RegistClassMember(LuaNewWelfareWnd, "SocialFollowIndex") -- 渠道关注

RegistClassMember(LuaNewWelfareWnd, "NewMainWelfareWnd") -- 新首页
RegistClassMember(LuaNewWelfareWnd, "MultipleExpWnd") -- 多倍经验


-- 福利相关数据
LuaNewWelfareWnd.ChargeActivityID = {}

function LuaNewWelfareWnd:Awake()
	self.NewMainWelfareWnd = self.transform:Find("NewMainWelfareWnd").gameObject
	self.MultipleExpWnd = self.transform:Find("MultipleExpWnd").gameObject
	if LuaWelfareMgr.IsOpenBigMonthCard then
		self.SignInButton = self.transform:Find("NewMainWelfareWnd/SignInButton").gameObject
	end
	LuaWelfareMgr.CheckMainWelfare = true
end

function LuaNewWelfareWnd:Init()
	self:HideAllSubWindows()
	self:HideAllTabs()

	--self:InitBtnIndex()

	UIEventListener.Get(self.SignInButton).onClick = DelegateFactory.VoidDelegate(function (go)
		self:OnSignInButtonClicked(go)
	end)
end

function LuaNewWelfareWnd:OnSignInButtonClicked(go)
    CUIManager.ShowUI(CLuaUIResources.SignInWnd)
end

function LuaNewWelfareWnd:InitWelfares()
	-- Step 1 隐藏所有界面
	self:HideAllSubWindows()
	self:HideAllTabs()

	-- Step 2 初始化tab
	self:InitTabs()
	-- Step 3 显示主福利界面(判断是否有选中index)
	--[[if self.SelectIndex == -1 then
		self.MainWelafareWnd:SetActive(true)
	end--]]
end

function LuaNewWelfareWnd:InitBtnIndex()
	self.SelectIndex = -1
	self.HomePageIdx = 0
	self.OnlineTimeBtnIndex = 0
	self.HolidayOnlineTimeBtnIndex = 0
	self.HolidaySignInBtnIndex = 0
	self.DesignCompetitionBtnIndex = 0
	self.LvUpBtnIndex = 0
	self.OtherWelfareIndex = 0
	self.QnReturnIndex = 0
	self.HuiliuIndex = -1
	self.PlatformValid = true
	self.FashionGiftInited = false
	self.HuiLiuFundIndex = -1
	self.ZhuanZhiFundIndex = -1
end

function LuaNewWelfareWnd:HideAllSubWindows()
	self.MainWelafareWnd:SetActive(false)
	self.FirstChargGiftWindow:SetActive(false)
	self.BonusWindow:SetActive(false)
	self.QnReturnWindow:SetActive(false)
	self.OtherWelfareWindow:SetActive(false)
	self.ChargeAwardWindow:SetActive(false)
	self.FashionGiftWindow:SetActive(false)
	self.BabyFundWindow:SetActive(false)
	self.NeteaseMemberWindow:SetActive(false)
	self.YunyingfuliWindow:SetActive(false)
	self.ZhuanzhiMonthCardReturnWindow:SetActive(false)
	self.HuiLiuFundWindow:SetActive(false)
	self.Carnival2020Window:SetActive(false)
	self.SpokesmanZhouBianWindow:SetActive(false)
	self.ZhuanZhiFundWindow:SetActive(false)
	self.HuiLiuFuliWindow:SetActive(false)
	self.Carnival2021Window:SetActive(false)
	self.NewMainWelfareWnd:SetActive(false)
	self.MultipleExpWnd:SetActive(false)
	self.AppRateWindow:SetActive(false)
	self.SocialFollowWindow:SetActive(false)
	self.BuyBookWnd:SetActive(false)
end

function LuaNewWelfareWnd:HideAllTabs()
	self.ButtonTemplate:SetActive(false)
	self.ButtonTemplate_Yellow:SetActive(false)
	self.ButtonTemplate3:SetActive(false)
	self.ButtonTemplate4:SetActive(false)
	self.ButtonTemplate_SpringFestival:SetActive(false)
end


function LuaNewWelfareWnd:InitTabs()

	self.WelfareList = {}
	self.ButtonList = {}
	self.WindowIdxList = {}
	self.OnClickList = {}
	self.ChargeActivityTabIdx = {}

	if not CClientMainPlayer.Inst then return end

	Extensions.RemoveAllChildren(self.TabTable.transform)

	self:AddHomePage()
	-- todo 此处先代码写，之后代码控制
	Welfare_Welfare.ForeachKey(function(key)
		local value = Welfare_Welfare.GetData(key)
		table.insert(self.WelfareList, value)
	end)

	table.sort(self.WelfareList, function (a, b)
		return a.Order < b.Order
	end)


	for i = 1, #self.WelfareList do
		local welfare = self.WelfareList[i]

		if welfare.WelfareStatus == 0 then
			if self[welfare.AddWelfare] then
				self[welfare.AddWelfare](self)
			end
		end
	end

	self.TabTable:Reposition()
	self.TabScrollView:ResetPosition()
	if not LuaWelfareMgr.IsOpenNewQianDao then
		self:ShowSignInAlert()
	end

	self:SelectButton()


	if LuaHuiLiuMgr.CheckNewHuiLiuOpen() then
		Gac2Gas.QueryHuiGuiInviterInfo()
	end
end

-- 刷新福利界面时，应该显示那个子界面的处理
function LuaNewWelfareWnd:SelectButton()

	if not CWelfareMgr.OpenLvUpAward then
		if self.SelectIndex == -1 then
			self:OnTabClicked(self.ButtonList[1])
		else
			if self.SelectIndex <= #self.ButtonList and self.SelectIndex > 0 then
				self:OnTabClicked(self.ButtonList[self.SelectIndex])
			else
				self:OnTabClicked(self.ButtonList[1])
			end
		end
	else
		self.SelectIndex = self.LvUpBtnIndex
		local lvUpBtn = self.ButtonList[self.LvUpBtnIndex+1]
		self:OnTabClicked(lvUpBtn)
		CWelfareMgr.OpenLvUpAward = false
	end
end

-- OnEnable时调用，确保界面初始化时数据正确
function LuaNewWelfareWnd:RequestWelfareInfos()
    Gac2Gas.QueryHuiLiuFund()
	Gac2Gas.CheckQnRechargeReturn()
	Gac2Gas.QueryHuiLiuReward()
	if LuaWelfareMgr.IsOpenNewQianDao then
		Gac2Gas.QueryQianDaoInfo_New()
	else
		Gac2Gas.QueryQianDaoInfo()
	end
end

-- 根据返回的数据进行处理
function LuaNewWelfareWnd:ResposeToWelfareInfos()
	-- 查询是否有有充值活动
	if not CWelfareMgr.OpenChargeActivity then
		self:InitWelfares()
	else
		LuaNewWelfareWnd.ChargeActivityID = {}
		Gac2Gas.QueryAvailableShopActivityIds()
	end
end

function LuaNewWelfareWnd:CheckMainWelfareAllReceived()
	if not LuaWelfareMgr.IsOpenBigMonthCard or not CClientMainPlayer.Inst then return end

	local info = CClientMainPlayer.Inst.ItemProp.MonthCard
	local info2 = CClientMainPlayer.Inst.ItemProp.BigMonthCardInfo 
	local leftDay = info and CChargeWnd.MonthCardLeftDay(info)
	local leftDay2 = info2 and LuaWelfareMgr:MonthCardLeftDay(info2)
	-- 当天领不了月卡资源的玩家得留在首页
	local canGetMonthCardReturn = leftDay and leftDay > 0 or leftDay2 and leftDay2 > 0

	local showAlert = (not CSigninMgr.Inst.hasSignedToday 
		or (leftDay and leftDay > 0 and not CPropertyItem.IsSamePeriod(info.LastReturnTime, "d"))
		or (leftDay2 and leftDay2 > 0 and not CPropertyItem.IsSamePeriod(info2.LastReturnTime, "d")))

	if not CommonDefs.IsNull(LuaWelfareMgr.MainWelfareTabBtn) then
		local alertCtrl = LuaWelfareMgr.MainWelfareTabBtn:GetComponent(typeof(CWelfareAlertCtl))
		if alertCtrl then
			alertCtrl:ShowAlert(showAlert)
		end
	end

	if not showAlert and canGetMonthCardReturn and self.SelectIndex == 1 then
		if CClientMainPlayer.Inst.MaxLevel > 69 then
			if  CClientMainPlayer.Inst.MaxLevel >= MultipleExp_GameSetting.GetData().Receive_Grade
			and not CommonDefs.IsNull(LuaWelfareMgr.MultipleExpTabBtn) then 
				self:OnTabClicked(LuaWelfareMgr.MultipleExpTabBtn)
			end
		else
			if not CommonDefs.IsNull(LuaWelfareMgr.OnlineTimeTabBtn) and KaiFuActivity_OnlineTime.Exists(CWelfareBonusMgr.Inst.OnlineAwardId) 
			and CWelfareBonusMgr.Inst.OnlineAwardLeftTime <= 0 then
				self:OnTabClicked(LuaWelfareMgr.OnlineTimeTabBtn)
			elseif not CommonDefs.IsNull(LuaWelfareMgr.LvUpAwardTabBtn) and KaiFuActivity_LvUp.Exists(CWelfareBonusMgr.Inst.LvUpAwardId) 
			and KaiFuActivity_LvUp.GetData(CWelfareBonusMgr.Inst.LvUpAwardId).Level <= CClientMainPlayer.Inst.Level then
				self:OnTabClicked(LuaWelfareMgr.LvUpAwardTabBtn)
			elseif CClientMainPlayer.Inst.MaxLevel >= MultipleExp_GameSetting.GetData().Receive_Grade
			and not CommonDefs.IsNull(LuaWelfareMgr.MultipleExpTabBtn) then 
				self:OnTabClicked(LuaWelfareMgr.MultipleExpTabBtn)
			end
		end
	end
end

function LuaNewWelfareWnd:OnSigninInfoUpdate()
	self:ShowSignInAlert()
	if LuaWelfareMgr.CheckMainWelfare then
		self:CheckMainWelfareAllReceived() -- 只让打开界面和签到成功的时候进来
		LuaWelfareMgr.CheckMainWelfare = false
	end
end

function LuaNewWelfareWnd:ShowSignInAlert()
	if LuaWelfareMgr.IsOpenBigMonthCard then
		self.SignInButton = self.transform:Find("NewMainWelfareWnd/SignInButton").gameObject
		local signed = self.SignInButton.transform:Find("Signed").gameObject
		local notSigned = self.SignInButton.transform:Find("NotSigned").gameObject
		if CSigninMgr.Inst.hasSignedToday then
			signed:SetActive(true)
			notSigned:SetActive(false)
		else
			signed:SetActive(false)
			notSigned:SetActive(true)
			if not LuaWelfareMgr.HasSignedTodayShow then
				LuaWelfareMgr.HasSignedTodayShow = true
				CUIManager.ShowUI(CLuaUIResources.SignInWnd)
			end
		end
	else
		local bg = self.SignInButton.transform:Find("BG"):GetComponent(typeof(CUITexture))
		if CSigninMgr.Inst.hasSignedToday then
			self.SignInFx:DestroyFx()
			bg:LoadMaterial("UI/Texture/Transparent/Material/raw_welfarewnd_btn_qiandao_yiqian.mat")
		else
			self.SignInFx:LoadFx("Fx/UI/Prefab/UI_fulijiemian_qiandao.prefab")
			bg:LoadMaterial("UI/Texture/Transparent/Material/raw_welfarewnd_btn_qiandao.mat")
			if not LuaWelfareMgr.HasSignedTodayShow then
				LuaWelfareMgr.HasSignedTodayShow = true
				CUIManager.ShowUI(CLuaUIResources.SignInWnd)
			end
		end
	end
end

function LuaNewWelfareWnd:OnMonthCardInfoChange()
	self:InitWelfares()
	self:CheckMainWelfareAllReceived()
end

function LuaNewWelfareWnd:OnLvUpAwardReceived()
	if not KaiFuActivity_LvUp.Exists(CWelfareBonusMgr.Inst.LvUpAwardId) then
		self.SelectIndex = 1
	end
	self:InitWelfares()
end

function LuaNewWelfareWnd:OnLoginDaysAwardReceived()
	if not KaiFuActivity_LoginDays.Exists(CWelfareBonusMgr.Inst.LoginAwardId) then 
		self.SelectIndex = 1
	end
	self:InitWelfares()
end

function LuaNewWelfareWnd:OnQianDaoDaysAwardReceived()
	if not KaiFuActivity_QianDaoDays.Exists(CWelfareBonusMgr.Inst.QianDaoAwardId) then
		self.SelectIndex = 1
	end
	self:InitWelfares()
end

function LuaNewWelfareWnd:OnEnable()
	self:InitBtnIndex()
	self:RequestWelfareInfos()

    --[[if CLuaWelfareWnd.s_forceGcOnInit then
        CResourceMgr.Inst:UnloadUnusedResource(false)
    end--]]

    g_ScriptEvent:AddListener("OnQnReturnInfoUpdate", self, "ResposeToWelfareInfos")
    g_ScriptEvent:AddListener("ItemPropUpdated", self, "InitWelfares")
    g_ScriptEvent:AddListener("OnlineTimeAwardReceived", self, "OnlineTimeAwardUpdate")
    g_ScriptEvent:AddListener("HolidayOnlineTimeAwardReceived", self, "HolidayOnlineTimeAwardUpdate")
    g_ScriptEvent:AddListener("HolidayOnlineTimeEnd", self, "HolidayOnlineTimeEnd")
    g_ScriptEvent:AddListener("LoginDaysAwardReceived", self, "OnLoginDaysAwardReceived")
    g_ScriptEvent:AddListener("QianDaoDaysAwardReceived", self, "OnQianDaoDaysAwardReceived")
    g_ScriptEvent:AddListener("LvUpAwardReceived", self, "OnLvUpAwardReceived")
    g_ScriptEvent:AddListener("OnHolidaySigninInfoUpdate", self, "HolidayInfoUpdate")
    g_ScriptEvent:AddListener("OnCheckInviteFriend", self, "OnOtherWelfareUpdate")
    g_ScriptEvent:AddListener("OnCheckBindPhone", self, "OnOtherWelfareUpdate")
    g_ScriptEvent:AddListener("OnCheckQnReturn", self, "OnCheckQnReturn")
    g_ScriptEvent:AddListener("OnHuiliuAwardGet", self, "OnHuiliuAwardGet")
    g_ScriptEvent:AddListener("OnHuiliuInfoUpdate", self, "OnHuiliuInfoUpdate")
    g_ScriptEvent:AddListener("OnQueryChargeActivityIdReturn", self, "InitWelfares")
	g_ScriptEvent:AddListener("OnSyncVNClientAwardResult", self, "InitWelfares")
	g_ScriptEvent:AddListener("OnNewChargeActivity", self, "OnChargeActivityUpdate")
    g_ScriptEvent:AddListener("JiFenWebInfoResult", self, "OnOtherWelfareUpdate")
    g_ScriptEvent:AddListener("ReceiveBabyFundInfo", self, "CheckBabyFundTabBtn")
	g_ScriptEvent:AddListener("OnWelfareBonusEmpty", self, "SelectMainWelfareWnd")
	g_ScriptEvent:AddListener("ReceiveYunYingFuLiInfo", self, "CheckYunYingFuliBtn")
	g_ScriptEvent:AddListener("OnQueryNeteaseMemberInfoResult", self, "OnQueryNeteaseMemberInfoResult")
	g_ScriptEvent:AddListener("OnSigninInfoUpdate", self, "OnSigninInfoUpdate")
	g_ScriptEvent:AddListener("GetZhuanZhiFundAwardSuccess", self, "InitWelfares")
	g_ScriptEvent:AddListener("GetHuiLiuFundTaskAwardSuccess", self, "InitWelfares")
	g_ScriptEvent:AddListener("UpdateHuiLiuFundTaskProgress", self, "OnHuiLiuFundUpdate")
	g_ScriptEvent:AddListener("UpdateZhuanZhiFundProgressInfo", self, "OnZhuanZhiFundUpdate")
	g_ScriptEvent:AddListener("UpdateWelfareNewHuiLiu", self, "OnNewHuiLiuUpdate")
	g_ScriptEvent:AddListener("OnRefresbUnity2018UpgradeInfo", self, "OnRefresbUnity2018UpgradeInfo")
	g_ScriptEvent:AddListener("MonthCardInfoChange", self, LuaWelfareMgr.IsOpenNewQianDao and "RequestWelfareInfos" or "OnMonthCardInfoChange") -- 月卡会影响签到的补签次数,所以要重新请求数据
	--g_ScriptEvent:AddListener("OpenNewQianDao", self, "RequestWelfareInfos")
    --g_ScriptEvent:AddListener("SelectMainWelfareWnd", self, "SelectMainWelfareWnd")
end

function LuaNewWelfareWnd:OnDisable()

	g_ScriptEvent:RemoveListener("OnQnReturnInfoUpdate", self, "ResposeToWelfareInfos")
	g_ScriptEvent:RemoveListener("ItemPropUpdated", self, "InitWelfares")
	g_ScriptEvent:RemoveListener("OnlineTimeAwardReceived", self, "OnlineTimeAwardUpdate")
	g_ScriptEvent:RemoveListener("HolidayOnlineTimeAwardReceived", self, "HolidayOnlineTimeAwardUpdate")
	g_ScriptEvent:RemoveListener("HolidayOnlineTimeEnd", self, "HolidayOnlineTimeEnd")
	g_ScriptEvent:RemoveListener("LoginDaysAwardReceived", self, "OnLoginDaysAwardReceived")
	g_ScriptEvent:RemoveListener("QianDaoDaysAwardReceived", self, "OnQianDaoDaysAwardReceived")
	g_ScriptEvent:RemoveListener("LvUpAwardReceived", self, "OnLvUpAwardReceived")
	g_ScriptEvent:RemoveListener("OnHolidaySigninInfoUpdate", self, "HolidayInfoUpdate")
	g_ScriptEvent:RemoveListener("OnCheckInviteFriend", self, "OnOtherWelfareUpdate")
	g_ScriptEvent:RemoveListener("OnCheckBindPhone", self, "OnOtherWelfareUpdate")
	g_ScriptEvent:RemoveListener("OnCheckQnReturn", self, "OnCheckQnReturn")
	g_ScriptEvent:RemoveListener("OnHuiliuAwardGet", self, "OnHuiliuAwardGet")
	g_ScriptEvent:RemoveListener("OnHuiliuInfoUpdate", self, "OnHuiliuInfoUpdate")
	g_ScriptEvent:RemoveListener("OnQueryChargeActivityIdReturn", self, "InitWelfares")
	g_ScriptEvent:RemoveListener("OnNewChargeActivity", self, "OnChargeActivityUpdate")
	g_ScriptEvent:RemoveListener("JiFenWebInfoResult", self, "OnOtherWelfareUpdate")
	g_ScriptEvent:RemoveListener("ReceiveBabyFundInfo", self, "CheckBabyFundTabBtn")
	g_ScriptEvent:RemoveListener("OnWelfareBonusEmpty", self, "SelectMainWelfareWnd")
	g_ScriptEvent:RemoveListener("ReceiveYunYingFuLiInfo", self, "CheckYunYingFuliBtn")
	g_ScriptEvent:RemoveListener("OnQueryNeteaseMemberInfoResult", self, "OnQueryNeteaseMemberInfoResult")
	g_ScriptEvent:RemoveListener("OnSigninInfoUpdate", self, "OnSigninInfoUpdate")
	g_ScriptEvent:RemoveListener("GetZhuanZhiFundAwardSuccess", self, "InitWelfares")
	g_ScriptEvent:RemoveListener("GetHuiLiuFundTaskAwardSuccess", self, "InitWelfares")
	g_ScriptEvent:RemoveListener("OnSyncVNClientAwardResult", self, "InitWelfares")
	g_ScriptEvent:RemoveListener("UpdateHuiLiuFundTaskProgress", self, "OnHuiLiuFundUpdate")
	g_ScriptEvent:RemoveListener("UpdateZhuanZhiFundProgressInfo", self, "OnZhuanZhiFundUpdate")
	g_ScriptEvent:RemoveListener("UpdateWelfareNewHuiLiu", self, "OnNewHuiLiuUpdate")
	g_ScriptEvent:RemoveListener("OnRefresbUnity2018UpgradeInfo", self, "OnRefresbUnity2018UpgradeInfo")
	g_ScriptEvent:RemoveListener("MonthCardInfoChange", self, LuaWelfareMgr.IsOpenNewQianDao and "RequestWelfareInfos" or "OnMonthCardInfoChange") -- 月卡会影响签到的补签次数,所以要重新请求数据
	--g_ScriptEvent:RemoveListener("OpenNewQianDao", self, "RequestWelfareInfos")
    --g_ScriptEvent:RemoveListener("SelectMainWelfareWnd", self, "SelectMainWelfareWnd")
    self.SelectIndex = - 1
    CWelfareBonusMgr.Inst.isUpdateOnlineTimeTroughOpenWelfareWnd = false
end

-------  首页  -------

function LuaNewWelfareWnd:AddHomePage()
	if  CClientMainPlayer.Inst==nil or CClientMainPlayer.Inst.IsCrossServerPlayer then
        return
    end

    LuaWelfareMgr.MainWelfareTabBtn = self:AddButton(LocalString.GetString("首页"), LuaWelfareMgr.IsOpenBigMonthCard and LuaWelfareMgr:CheckNewMainWelfareAlert(), 0, self.OnHomePageClicked, false)
    self.HomePageIdx = #self.ButtonList - 1
    self:UpdateCurSelectIndex(LocalString.GetString("升级礼包"), self.HomePageIdx)

	if LuaWelfareMgr.IsOpenBigMonthCard then
		LuaWelfareMgr.MultipleExpTabBtn = self:AddButton(LocalString.GetString("多倍经验"), LuaWelfareMgr:CheckMultipleExpAlert(), 0, function(self, go) 
			self.MultipleExpWnd:SetActive(true)
		end, false)
		self:UpdateCurSelectIndex(LocalString.GetString("多倍经验"), #self.ButtonList - 1)
	end
end

function LuaNewWelfareWnd:OnHomePageClicked(go)
	--self:SelectMainWelfareWnd()
	self:OpenMainWelfareWnd()
end

-------  LvUp  -------
function LuaNewWelfareWnd:AddLvUp()
    if  CClientMainPlayer.Inst==nil or CClientMainPlayer.Inst.IsCrossServerPlayer then
        return
    end

    if KaiFuActivity_LvUp.Exists(CWelfareBonusMgr.Inst.LvUpAwardId) then
        LuaWelfareMgr.LvUpAwardTabBtn = self:AddButton(LocalString.GetString("升级礼包"), KaiFuActivity_LvUp.GetData(CWelfareBonusMgr.Inst.LvUpAwardId).Level <= CClientMainPlayer.Inst.Level, 3, self.OnLvUpClicked, false)
        -- 如果LvUp相关信息，则增加相关的tab
        self.LvUpBtnIndex = #self.ButtonList - 1
        self:UpdateCurSelectIndex(LocalString.GetString("升级礼包"), self.LvUpBtnIndex)
    end
end

function LuaNewWelfareWnd:OnLvUpClicked()
	self.BonusWindow:SetActive(true)
	local luaScript = CommonDefs.GetComponent_GameObject_Type(self.BonusWindow, typeof(CCommonLuaScript))
    luaScript:Init({EnumWelfareBonusCategory.LvUp})

	--CommonDefs.GetComponent_GameObject_Type(self.BonusWindow, typeof(CWelfareBonusWindow)):Init(EnumWelfareBonusCategory.LvUp)
end


-------  在线时间  -------
function LuaNewWelfareWnd:AddOnlineTime()
	if  CClientMainPlayer.Inst==nil or CClientMainPlayer.Inst.IsCrossServerPlayer then
        return
    end

    if KaiFuActivity_OnlineTime.Exists(CWelfareBonusMgr.Inst.OnlineAwardId) then
        LuaWelfareMgr.OnlineTimeTabBtn = self:AddButton(LocalString.GetString("新手在线奖励"), CWelfareBonusMgr.Inst.OnlineAwardLeftTime <= 0, 3, self.OnOnlineTimeClicked, false)
        self.OnlineTimeBtnIndex = #self.ButtonList - 1
        self:UpdateCurSelectIndex(LocalString.GetString("新手在线奖励"), #self.ButtonList - 1)
    end
end

function LuaNewWelfareWnd:OnOnlineTimeClicked(go)
	self.BonusWindow:SetActive(true)
	local luaScript = CommonDefs.GetComponent_GameObject_Type(self.BonusWindow, typeof(CCommonLuaScript))
    luaScript:Init({EnumWelfareBonusCategory.OnlineTime})
end


function LuaNewWelfareWnd:OnlineTimeAwardUpdate()
	if not self.ButtonList then return end
	if self.OnlineTimeBtnIndex < #self.ButtonList and self.OnlineTimeBtnIndex > 0 then
        if KaiFuActivity_OnlineTime.Exists(CWelfareBonusMgr.Inst.OnlineAwardId) then
            CommonDefs.GetComponent_Component_Type(self.ButtonList[self.OnlineTimeBtnIndex+1], typeof(CWelfareAlertCtl)):ShowAlert(CWelfareBonusMgr.Inst.OnlineAwardLeftTime <= 0)
        else
            local onlineBtn = self.ButtonList[self.OnlineTimeBtnIndex+1]
            onlineBtn.gameObject:SetActive(false)
            self.TabTable:Reposition()
            self.TabScrollView:ResetPosition()
			self:SelectMainWelfareWnd()
        end
    end
end

-------  oppo登录好礼  -------
function LuaNewWelfareWnd:AddQianDaoDays()
	if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.IsCrossServerPlayer then
        return
    end
    if KaiFuActivity_QianDaoDays.Exists(CWelfareBonusMgr.Inst.QianDaoAwardId) then
        self:AddButton(LocalString.GetString("oppo登录好礼"), CWelfareBonusMgr.Inst.QianDaoAwardId <= CWelfareBonusMgr.Inst.QianDaoDays, 3, self.OnQianDaoDaysClicked, false)
        self:UpdateCurSelectIndex(LocalString.GetString("oppo登录好礼"), #self.ButtonList - 1)
    end

end

function LuaNewWelfareWnd:OnQianDaoDaysClicked(go)
	self.BonusWindow:SetActive(true)
	local luaScript = CommonDefs.GetComponent_GameObject_Type(self.BonusWindow, typeof(CCommonLuaScript))
    luaScript:Init({EnumWelfareBonusCategory.QianDaoDays})
end


-------  养育基金  -------

function LuaNewWelfareWnd:AddBabyFund()
	self.BabyInitButton = false

	Gac2Gas.BabyFundRequestShowEntrance()

	if not CClientMainPlayer.Inst then return end

	local saveBabyData = CommonDefs.DictGetValue(CClientMainPlayer.Inst.PlayProp.PersistPlayData, typeof(UInt32), 146)
	if saveBabyData and saveBabyData.Data then
		local saveBabyDic = TypeAs(MsgPackImpl.unpack(saveBabyData.Data), typeof(MakeGenericClass(Dictionary, String, Object)))
		local buttonShow = CommonDefs.DictGetValue(saveBabyDic, typeof(String), "Show")
		if buttonShow and buttonShow > 0 then

			self:AddButtonByTemplate(self.ButtonTemplate4, LocalString.GetString("养育基金"), false, 14, self.OnBabyFundClicked)
			self.BabyFundButtonIndex = #self.ButtonList -1
			self.BabyFundBtn = self.ButtonList[self.BabyFundButtonIndex+1].gameObject
		end
	end
end


function LuaNewWelfareWnd:OnBabyFundClicked(go)
	-- 不显示红点
	local buttonScript = go:GetComponent(typeof(CWelfareAlertCtl))
	if buttonScript then
     	buttonScript:ShowAlert(false)
     end

    self.BabyFundWindow:SetActive(true)
    local luaScript = CommonDefs.GetComponent_GameObject_Type(self.BabyFundWindow, typeof(CCommonLuaScript))
    luaScript:Init({})
end


function LuaNewWelfareWnd:CheckBabyFundTabBtn()
	if self.BabyInitButton then
    	return
	end

	if LuaBabyMgr.m_BabyFundShow and LuaBabyMgr.m_BabyFundShow > 0 then
    	local needShowNotify = false
    	if LuaBabyMgr.m_BabyFundInfo then
      		for i,v in pairs(LuaBabyMgr.m_BabyFundInfo) do
        		if v == 1 then
          			needShowNotify = true
        		end
      		end
    	end

    	if self.BabyFundBtn and needShowNotify then
        	if CommonDefs.GetComponent_GameObject_Type(self.BabyFundBtn, typeof(CWelfareAlertCtl)) then
            	CommonDefs.GetComponent_GameObject_Type(self.BabyFundBtn, typeof(CWelfareAlertCtl)):ShowAlert(true)
      		end
    	end
    	self.BabyInitButton = true
  	end
end

-------  充值活动  -------

function LuaNewWelfareWnd:AddChargeActivity()
	for i,v in ipairs(LuaNewWelfareWnd.ChargeActivityID) do
        self:AddChargeActivityById(v)
    end
end

function LuaNewWelfareWnd:AddChargeActivityById(ID)
	local sa = Charge_ShopActivity.GetData(ID)
	if sa then
		local showAlert = CommonDefs.ListContains(CWelfareBonusMgr.Inst.giftAvailableID, typeof(UInt32), ID)
                or CommonDefs.ListContains(CWelfareBonusMgr.Inst.newChargeActivityID, typeof(UInt32), ID)
		self:AddButton(sa.Tab, showAlert, 7, function ()
                self:OnChargeActivityClicked(ID)
            end, true)
		self:UpdateCurSelectIndex(sa.Tab, #self.ButtonList - 1)
		self.ChargeActivityTabIdx[ID] = #self.ButtonList - 1
	end
end

function LuaNewWelfareWnd:OnChargeActivityClicked(ID)
	self.ChargeAwardWindow:SetActive(true)
	CommonDefs.GetComponent_GameObject_Type(self.ChargeAwardWindow, typeof(CChargeActivityWindow)):Init(ID)
	if CommonDefs.ListContains(CWelfareBonusMgr.Inst.newChargeActivityID, typeof(UInt32), ID) then
        CommonDefs.ListRemove(CWelfareBonusMgr.Inst.newChargeActivityID, typeof(UInt32), ID)
        Gac2Gas.RequestSetShopActivityViewed(ID)
        self:OnChargeActivityUpdate()
        EventManager.Broadcast(EnumEventType.OnNewChargeActivity)
    end
end

-- 更新充值活动tab的红点提示
function LuaNewWelfareWnd:OnChargeActivityUpdate()
	for k, v in pairs(self.ChargeActivityTabIdx) do
		local showAlert = CommonDefs.ListContains(CWelfareBonusMgr.Inst.giftAvailableID, typeof(UInt32), k)
				or CommonDefs.ListContains(CWelfareBonusMgr.Inst.newChargeActivityID, typeof(UInt32), k)
        self.ButtonList[v+1]:GetComponent(typeof(CWelfareAlertCtl)):ShowAlert(showAlert)
    end
end


-------  登录7天有礼  -------
function LuaNewWelfareWnd:AddLoginDays()
	if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.IsCrossServerPlayer then
        return
    end
    if KaiFuActivity_LoginDays.Exists(CWelfareBonusMgr.Inst.LoginAwardId) then
    	local showAlert = CWelfareBonusMgr.Inst.LoginAwardId <= CWelfareBonusMgr.Inst.LoginDays
        self:AddButton(LocalString.GetString("登录7天有礼"), showAlert, 3, self.OnLoginDaysClicked, false)
        self:UpdateCurSelectIndex(LocalString.GetString("登录7天有礼"), #self.ButtonList - 1)
    end
end

function LuaNewWelfareWnd:OnLoginDaysClicked(go)
	self.BonusWindow:SetActive(true)
	local luaScript = CommonDefs.GetComponent_GameObject_Type(self.BonusWindow, typeof(CCommonLuaScript))
    luaScript:Init({EnumWelfareBonusCategory.LoginDays})
	--CommonDefs.GetComponent_GameObject_Type(self.BonusWindow, typeof(CWelfareBonusWindow)):Init(EnumWelfareBonusCategory.LoginDays)
end


-------  节日签到  -------

function LuaNewWelfareWnd:AddHolidaySignin()
	if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.IsCrossServerPlayer then
        return
    end

    -- 登录或顶号时会同步
    if CSigninMgr.Inst.holidayInfo == nil then
        return
    end

    -- 相关数据在QianDao_Holiday
    if CSigninMgr.Inst.holidayInfo.showSignin then
        local holiday = CSigninMgr.Inst.holidayInfo.todayInfo
        if holiday ~= nil then
        	local showAlert = holiday.Open == 1 and not CSigninMgr.Inst.holidayInfo.hasSignGift
			if CSigninMgr.Inst.holidayInfo.batchId == 75 then -- 2023年春节特殊处理
				self:AddButtonByTemplate(self.ButtonTemplate_SpringFestival, holiday.ListTitle, showAlert, 3, self.OnHolidaySigninClicked)
			else
				self:AddButton(holiday.ListTitle, showAlert, 3, self.OnHolidaySigninClicked, false)
			end
            self.HolidaySignInBtnIndex = #self.ButtonList - 1
            self:UpdateCurSelectIndex(holiday.ListTitle, #self.ButtonList - 1)
        end
    end
end


function LuaNewWelfareWnd:OnHolidaySigninClicked(go)
	self.BonusWindow:SetActive(true)
	local luaScript = CommonDefs.GetComponent_GameObject_Type(self.BonusWindow, typeof(CCommonLuaScript))
    luaScript:Init({EnumWelfareBonusCategory.Holiday})
end

function LuaNewWelfareWnd:HolidayInfoUpdate()
	if self.HolidaySignInBtnIndex < #self.ButtonList and self.HolidaySignInBtnIndex > 0 then
		local holiday = CSigninMgr.Inst.holidayInfo.todayInfo
        if holiday ~= nil then
            CommonDefs.GetComponent_Component_Type(self.ButtonList[self.HolidaySignInBtnIndex+1], typeof(CWelfareAlertCtl)):ShowAlert(not CSigninMgr.Inst.holidayInfo.hasSignGift and holiday.Open == 1)
        else
            local onlineBtn = self.ButtonList[self.HolidaySignInBtnIndex+1]
            onlineBtn.gameObject:SetActive(false)
            self.TabTable:Reposition()
            self.TabScrollView:ResetPosition()
        end
	end

	self:DesignCompetitionUpdate()
end

-- 设计大赛处理
function LuaNewWelfareWnd:AddDesignCompetitionSignin()
	if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.IsCrossServerPlayer then
        return
    end

    -- 登录或顶号时会同步
    if CSigninMgr.Inst.designCompetitionInfo == nil then
        return
    end

    -- 相关数据在QianDao_Holiday
    if CSigninMgr.Inst.designCompetitionInfo.showSignin then
        local holiday = CSigninMgr.Inst.designCompetitionInfo.todayInfo
        if holiday ~= nil then
        	local showAlert = holiday.Open == 1 and not CSigninMgr.Inst.designCompetitionInfo.hasSignGift

			self:AddButton(holiday.ListTitle, showAlert, self.DesignCompetitionBtnIndex, self.OnDesignCompetitionClick, false)
			self.DesignCompetitionBtnIndex = #self.ButtonList - 1
			self:UpdateCurSelectIndex(holiday.ListTitle, #self.ButtonList - 1)
        end
    end
end

function LuaNewWelfareWnd:OnDesignCompetitionClick(go)
	self.BonusWindow:SetActive(true)
	local luaScript = CommonDefs.GetComponent_GameObject_Type(self.BonusWindow, typeof(CCommonLuaScript))
    luaScript:Init({EnumWelfareBonusCategory.DesignCompetiton})
end

function LuaNewWelfareWnd:DesignCompetitionUpdate()
	if self.DesignCompetitionBtnIndex < #self.ButtonList and self.DesignCompetitionBtnIndex > 0 then
        local holiday = CSigninMgr.Inst.designCompetitionInfo.todayInfo
		if holiday ~= nil then
            CommonDefs.GetComponent_Component_Type(self.ButtonList[self.DesignCompetitionBtnIndex+1], typeof(CWelfareAlertCtl)):ShowAlert(not CSigninMgr.Inst.designCompetitionInfo.hasSignGift and holiday.Open == 1)
        else
            local onlineBtn = self.ButtonList[self.DesignCompetitionBtnIndex+1]
            onlineBtn.gameObject:SetActive(false)
            self.TabTable:Reposition()
            self.TabScrollView:ResetPosition()
        end
    end
end

-------  节日在线  -------

function LuaNewWelfareWnd:AddHolidayOnlineTime()
	if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.IsCrossServerPlayer then
        return
    end

    if HolidayOnlineTime_Award.Exists(CWelfareBonusMgr.Inst.holidayOnlineAwardId) then
        local a1 = HolidayOnlineTime_Award.GetData(CWelfareBonusMgr.Inst.holidayOnlineAwardId)
        local a2 = HolidayOnlineTime_Setting.GetData(a1.BatchId)

        local showAlert = CWelfareBonusMgr.Inst.holidayOnlineTime <= 0
        self:AddButton(a2.Title, showAlert, 3, self.OnHolidayOnlineTimeClicked, false)
        self.HolidayOnlineTimeBtnIndex = #self.ButtonList - 1
        self:UpdateCurSelectIndex(a2.Title, self.ButtonList - 1)
    end
end

function LuaNewWelfareWnd:OnHolidayOnlineTimeClicked(go)
	self.BonusWindow:SetActive(true)
	local luaScript = CommonDefs.GetComponent_GameObject_Type(self.BonusWindow, typeof(CCommonLuaScript))
    luaScript:Init({EnumWelfareBonusCategory.HolidayOnline})
end

function LuaNewWelfareWnd:HolidayOnlineTimeAwardUpdate()
	if self.HolidayOnlineTimeBtnIndex < #self.ButtonList and self.HolidayOnlineTimeBtnIndex > 0 then
        if HolidayOnlineTime_Award.Exists(CWelfareBonusMgr.Inst.holidayOnlineAwardId) then
            CommonDefs.GetComponent_Component_Type(self.ButtonList[self.HolidayOnlineTimeBtnIndex+1], typeof(CWelfareAlertCtl)):ShowAlert(CWelfareBonusMgr.Inst.holidayOnlineTime <= 0)
        else
            local onlineBtn = self.ButtonList[self.HolidayOnlineTimeBtnIndex+1]
            onlineBtn.gameObject:SetActive(false)
            self.TabTable:Reposition()
            self.TabScrollView:ResetPosition()
        end
    end
end

function LuaNewWelfareWnd:HolidayOnlineTimeEnd()
	if self.HolidayOnlineTimeBtnIndex < #self.ButtonList and self.HolidayOnlineTimeBtnIndex > 0 then
        CommonDefs.GetComponent_Component_Type(self.ButtonList[self.HolidayOnlineTimeBtnIndex+1], typeof(CWelfareAlertCtl)):ShowAlert(false)
    end
end


-------  历史返利奖励  -------

function LuaNewWelfareWnd:AddQnReturn()
	if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.IsCrossServerPlayer then
        return
    end

    if not self.PlatformValid then
    	return
    end

    if (CSigninMgr.Inst.qnReturnInfo.hasConfirmBind and CSigninMgr.Inst.qnReturnInfo.isReturnHere) or not CSigninMgr.Inst.qnReturnInfo.hasConfirmBind then
        self:AddButton(LocalString.GetString("历史返利"), CSigninMgr.Inst:CheckQnReturn(), 4, self.OnQnReturnClicked, false)
        self.QnReturnIndex = #self.ButtonList - 1
        self:UpdateCurSelectIndex(LocalString.GetString("历史返利"), #self.ButtonList - 1)
    end

end

function LuaNewWelfareWnd:OnQnReturnClicked(go)
	self.QnReturnWindow:SetActive(true)
end


function LuaNewWelfareWnd:OnCheckQnReturn()
	if self.QnReturnIndex < #self.ButtonList and self.QnReturnIndex > 0 then
        CommonDefs.GetComponent_Component_Type(self.ButtonList[self.QnReturnIndex+1], typeof(CWelfareAlertCtl)):ShowAlert(CSigninMgr.Inst:CheckQnReturn())
    end
end


-------  回流奖励  -------


function LuaNewWelfareWnd:AddHuiliu()
	if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.IsCrossServerPlayer then
        return
    end

    if CSigninMgr.Inst.huiliuLevel > 0 and Huiliu_Item.Exists(CSigninMgr.Inst.huiliuLevel) then
    	local showAlert = not CSigninMgr.Inst.huiliuAwardGetToday
        self:AddButton(LocalString.GetString("回流礼包"), showAlert, 3, self.OnHuiliuClicked, false)
        self.HuiliuIndex = #self.ButtonList - 1
        self:UpdateCurSelectIndex(LocalString.GetString("回流礼包"), #self.ButtonList - 1)
    end
end


function LuaNewWelfareWnd:OnHuiliuClicked(go)
	self.BonusWindow:SetActive(true)
	local luaScript = CommonDefs.GetComponent_GameObject_Type(self.BonusWindow, typeof(CCommonLuaScript))
    luaScript:Init({EnumWelfareBonusCategory.Huiliu})
	--CommonDefs.GetComponent_GameObject_Type(self.BonusWindow, typeof(CWelfareBonusWindow)):Init(EnumWelfareBonusCategory.Huiliu)
end

function LuaNewWelfareWnd:OnHuiliuAwardGet()
	if self.HuiliuIndex < #self.ButtonList and self.HuiliuIndex > 0 then
        CommonDefs.GetComponent_Component_Type(self.ButtonList[self.HuiliuIndex+1], typeof(CWelfareAlertCtl)):ShowAlert(not CSigninMgr.Inst.huiliuAwardGetToday)
    end
end

function LuaNewWelfareWnd:OnHuiliuInfoUpdate()
	--if not self.ButtonList then
	--	self:InitWelfares()
	--end

	if not self.ButtonList or not self.HuiliuIndex then return end

	if not Huiliu_Item.Exists(CSigninMgr.Inst.huiliuLevel) and self.HuiliuIndex < #self.ButtonList and self.HuiliuIndex > 0 then
        local huiliuBtn = self.ButtonList[self.HuiliuIndex+1]
        huiliuBtn.gameObject:SetActive(false)
        self.TabTable:Reposition()
        self.TabScrollView:ResetPosition()
        self:SelectButton()
    elseif self.HuiliuIndex < #self.ButtonList and self.HuiliuIndex > 0 then
        CommonDefs.GetComponent_Component_Type(self.ButtonList[self.HuiliuIndex+1], typeof(CWelfareAlertCtl)):ShowAlert(not CSigninMgr.Inst.huiliuAwardGetToday)
    end
end

-------  其他奖励  -------

function LuaNewWelfareWnd:AddOtherWelfare()
	if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.IsCrossServerPlayer then
        return
    end

    local showAlert = self:NeedShowOtherWelfareAlert()
    self:AddButton(LocalString.GetString("其他奖励"), showAlert, 5, self.OnOtherWelfareClicked, false)
    self.OtherWelfareIndex = #self.ButtonList - 1
    self:UpdateCurSelectIndex(LocalString.GetString("其他奖励"), #self.ButtonList - 1)
    -- 奇遇
    CJiFenWebMgr.Inst:IsOpen()

end


function LuaNewWelfareWnd:OnOtherWelfareClicked(go)
	self.OtherWelfareWindow:SetActive(true)
end

function LuaNewWelfareWnd:NeedShowOtherWelfareAlert()
	return not CSigninMgr.Inst.hasCheckInviteFriend
			or (not CSigninMgr.Inst.hasCheckBindPhone and CSigninMgr.Inst:playerFitBindPhone()) 
			or (CJiFenWebMgr.Inst.m_CanAccess and CJiFenWebMgr.Inst.m_HasAlert)
			or LuaWelfareMgr:CanShowUnity2018UpgradeAlert()
end

function LuaNewWelfareWnd:OnOtherWelfareUpdate()
	if self.OtherWelfareIndex <= #self.ButtonList and self.OtherWelfareIndex > 0 then
		local showAlert = self:NeedShowOtherWelfareAlert()
		CommonDefs.GetComponent_Component_Type(self.ButtonList[self.OtherWelfareIndex+1], typeof(CWelfareAlertCtl)):ShowAlert(showAlert)
	end
end

-------  折扣奖励  -------

function LuaNewWelfareWnd:AddFashionGiftButton()
	if CSwitchMgr.EnableFashionGift then
        self:AddButtonByTemplate(self.ButtonTemplate3, LocalString.GetString("折扣礼包"), false, 9, self.OnFashionGiftButtonClicked)
    end
end


function LuaNewWelfareWnd:OnFashionGiftButtonClicked(go)
	self.FashionGiftWindow:SetActive(true)
	if not self.FashionGiftInited then
		local luaScript = self.FashionGiftWindow:GetComponent(typeof(CCommonLuaScript))
        luaScript:Init({})
		self.FashionGiftInited = true
	end
end



-------  网易会员  -------


function LuaNewWelfareWnd:AddNeteaseMember()
	--PC版本session信息没有，无法领奖，屏蔽掉入口
    if not CommonDefs.IsInMobileDevice() then
        return
    end

    local openLv = NeteaseMember_Setting.GetData().OpenLevel
    local mainplayer = CClientMainPlayer.Inst
    local neteaseOffical = (CommonDefs.IS_CN_CLIENT and  CLoginMgr.Inst:IsNetEaseOfficialLogin())
    if neteaseOffical and mainplayer and mainplayer.Level >= openLv then
        self:AddButton(LocalString.GetString("网易会员"), false, 15, self.OnNeteaseMemberClicked, false)
        self.m_NeteaseMemberButton = self.ButtonList[#self.ButtonList].gameObject --最后一个添加的按钮
        LuaWelfareMgr.QueryNeteaseMemberWelfareInfo()
    end

end

function LuaNewWelfareWnd:OnNeteaseMemberClicked(go)
	self.NeteaseMemberWindow:SetActive(true)
end

function LuaNewWelfareWnd:OnQueryNeteaseMemberInfoResult()
    if self.m_NeteaseMemberButton then
        local ctrl = self.m_NeteaseMemberButton.transform:GetComponent(typeof(CWelfareAlertCtl))
        if ctrl then
            ctrl:ShowAlert(LuaWelfareMgr.HasNeedAwardedGift())
        end
    end
end


-------  运营福利  -------

function LuaNewWelfareWnd:AddYunyingfuli()
	 Gac2Gas.QueryFuliWndStatus()
    if CClientMainPlayer.Inst then
      self:AddButton(LocalString.GetString("运营福利"), false, 16, nil, false)
      self.YunYingFuLiBtn = self.ButtonList[#self.ButtonList].gameObject
      self.YunYingFuLiBtn:SetActive(false)
    end
end


function LuaNewWelfareWnd:OnYunyingfuliClicked(go)
	self.YunyingfuliWindow:SetActive(true)
end

function LuaNewWelfareWnd:CheckYunYingFuliBtn(open)
	if open and self.YunYingFuLiBtn then
	  self.YunYingFuLiBtn:SetActive(true)
	  self.TabTable:Reposition()
	  self.TabScrollView:ResetPosition()
	end
end

-- 增加新的tab
function LuaNewWelfareWnd:AddButton(title, showAlert, windowIndex, onClick, yellow)

	local template = self.ButtonTemplate
	if yellow then
		template = self.ButtonTemplate_Yellow
	end

	return self:AddButtonByTemplate(template, title, showAlert, windowIndex, onClick)
end

-- 根据buttonTemplate增加tab
function LuaNewWelfareWnd:AddButtonByTemplate(template, title, showAlert, windowIndex, onClick)
	local button = NGUITools.AddChild(self.TabTable.gameObject, template)
	button:SetActive(true)

	if CommonDefs.GetComponentInChildren_GameObject_Type(button, typeof(UILabel)) then
		CommonDefs.GetComponentInChildren_GameObject_Type(button, typeof(UILabel)).text = title
	end

	if CommonDefs.GetComponent_GameObject_Type(button, typeof(CWelfareAlertCtl)) then
		 CommonDefs.GetComponent_GameObject_Type(button, typeof(CWelfareAlertCtl)):ShowAlert(showAlert)
	end

	local qnButton = CommonDefs.GetComponent_GameObject_Type(button, typeof(QnSelectableButton))

	table.insert(self.ButtonList, qnButton)

    --table.insert( self.windowIdxList, windowIndex)
    self.OnClickList[qnButton] = onClick

	qnButton.OnClick = DelegateFactory.Action_QnButton(function(btn) self:OnTabClicked(btn) end)

	return qnButton
end


function LuaNewWelfareWnd:OnTabClicked(go)

	self:HideAllSubWindows()

	for i = 1, #self.ButtonList do
		if self.ButtonList[i] == go then
			self.SelectIndex = i
			self.ButtonList[i]:SetSelected(true, false)
		else
			self.ButtonList[i]:SetSelected(false, false)
		end
		-- 春节2023特殊高亮处理
		local highlight = self.ButtonList[i].transform:Find("HighLight")
		if highlight then highlight.gameObject:SetActive(self.ButtonList[i] == go) end
	end


    --self.tabWindows[self.windowIdxList[self.curSelectIndex+1]]:SetActive(true)
    --self.buttonList[self.curSelectIndex+1]:SetSelected(true, false)

    -- if CommonDefs.DictGetValue(self.onClickList, typeof(QnButton), go) ~= nil then
    --     CommonDefs.DictGetValue(self.onClickList, typeof(QnButton), go):Invoke()
    -- end

    if self.OnClickList[go] then
        self.OnClickList[go](self, go)
    end

end

-- 更新选中tab（支持外部跳转）
function LuaNewWelfareWnd:UpdateCurSelectIndex(title, index)
    if CWelfareMgr.OpenTitle == title then
        self.SelectIndex = index+1
        CWelfareMgr.OpenTitle = ""
    end
end

-- 正确的关闭子界面后切换成主福利界面
function LuaNewWelfareWnd:SelectMainWelfareWnd()
	self.SelectIndex = 1
	local mainBtn = self.ButtonList[self.SelectIndex]
	self:OnTabClicked(mainBtn)
end

-- 打开福利界面
function LuaNewWelfareWnd:OpenMainWelfareWnd()
	if LuaWelfareMgr.IsOpenBigMonthCard then
		self.SignInButton = self.transform:Find("NewMainWelfareWnd/SignInButton").gameObject
		UIEventListener.Get(self.SignInButton).onClick = DelegateFactory.VoidDelegate(function (go)
			self:OnSignInButtonClicked(go)
		end)
		self:ShowSignInAlert()
	end
	self.MainWelafareWnd:SetActive(not LuaWelfareMgr.IsOpenBigMonthCard)
	self.NewMainWelfareWnd:SetActive(LuaWelfareMgr.IsOpenBigMonthCard)
end



function LuaNewWelfareWnd:ClickZhuanzhiFuli()
	self.ZhuanzhiMonthCardReturnWindow:SetActive(true)

end

function LuaNewWelfareWnd:AddZhuanzhiFuli()
	if LuaProfessionTransferMgr.ShowZhuanzhiReturnActivity() then
		local cardInfo = CClientMainPlayer.Inst.ItemProp.ZhuanZhiReturn
		--1.活动期间有转职 2.距转职时间不超过30天 3.当天没有领取过
		local showAlert = LuaProfessionTransferMgr.TransferInDuration4Return()
		local cardInfo = CClientMainPlayer.Inst.ItemProp.ZhuanZhiReturn
		if cardInfo ~= nil and cardInfo.ZhuanZhiTime ~= 0 then
			local leftDay = CChargeWnd.HuahunMonthCardLeftDay(cardInfo)
			showAlert = showAlert and leftDay > 0
			showAlert = showAlert and (not CPropertyItem.IsSamePeriod(cardInfo.LastReturnTime, "d"))
		end
		self:AddButton(LocalString.GetString("转职返还"), showAlert, 10, self.ClickZhuanzhiFuli, false)
		self:UpdateCurSelectIndex(LocalString.GetString("转职返还"), #self.ButtonList - 1)
	end
end

function LuaNewWelfareWnd:ClickCarnival2020()
	self.Carnival2020Window:SetActive(true)
	if not self.Carnival2020Inited then
		local luaScript = self.Carnival2020Window:GetComponent(typeof(CCommonLuaScript))
        luaScript:Init({})
		self.Carnival2020Inited = true
	end
end

function LuaNewWelfareWnd:AddCarnival2020()
	local now = CServerTimeMgr.Inst:GetZone8Time()
	local beginOfTheBuyDay = CreateFromClass(DateTime, 2020, 8, 7, 0, 0, 0)
	local endOfTheBuyDay = CreateFromClass(DateTime, 2020, 8, 31, 0, 0, 0)
	local seconds = beginOfTheBuyDay:Subtract(now).TotalSeconds
	local endSeconds = endOfTheBuyDay:Subtract(now).TotalSeconds

	local nowTimeStamp = CServerTimeMgr.Inst.timeStamp
	local beginOfTheShowDay2021 = CServerTimeMgr.Inst:GetTimeStampByStr(JiaNianHua_Setting.GetData().EntranceOpenTime2021)
	local endOfTheShowDay2021 = CServerTimeMgr.Inst:GetTimeStampByStr(JiaNianHua_Setting.GetData().EntranceCloseTime2021)
	local beginOfTheBuyDay2021 = CServerTimeMgr.Inst:GetTimeStampByStr(JiaNianHua_Setting.GetData().TicketBeginTime2021)
	local seconds2021 = beginOfTheShowDay2021 - nowTimeStamp
	local endSeconds2021 = endOfTheShowDay2021 - nowTimeStamp
	local beginSecond2021 = beginOfTheBuyDay2021 - nowTimeStamp	

	if seconds < 0 and endSeconds > 0 then	-- 2020嘉年华
		self:AddButton(LocalString.GetString("嘉年华门票"), false, 11, self.ClickCarnival2020, false)
		self:UpdateCurSelectIndex(LocalString.GetString("嘉年华门票"), #self.ButtonList - 1)
	elseif seconds2021 < 0 and endSeconds2021 > 0 and (CClientMainPlayer.Inst and not CClientMainPlayer.Inst.IsCrossServerPlayer) then	-- 2021嘉年华
		local ShowAlert = false
		if CClientMainPlayer.Inst ~= nil and CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eOpenJiaNianHuaTicketSellWndTime) then
			local playerDate = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eOpenJiaNianHuaTicketSellWndTime).Data.StringData
			local lastopenTime = tonumber(playerDate)
			if lastopenTime < beginOfTheBuyDay2021 and beginSecond2021 < 0 then	-- 上次打开在开始售票前,并且目前已开始售票 显示红点
				ShowAlert = true
			end
		else	-- 首次打开之前显示红点
			ShowAlert = true
		end
		self:AddButton(LocalString.GetString("嘉年华门票"), ShowAlert , 11, self.ClickCarnival2021, false)
		self:UpdateCurSelectIndex(LocalString.GetString("嘉年华门票"), #self.ButtonList - 1)
	end
end

function LuaNewWelfareWnd:ClickCarnival2021(go)
	self.Carnival2021Window:SetActive(true)
	CommonDefs.GetComponent_Component_Type(go, typeof(CWelfareAlertCtl)):ShowAlert(false)
	Gac2Gas.OpenJiaNianHuaTicketSellWnd()
	if not self.Carnival2021Inited then
		local luaScript = self.Carnival2021Window:GetComponent(typeof(CCommonLuaScript))
        luaScript:Init({})
		self.Carnival2021Inited = true
	end
end

--@region 回流基金

function LuaNewWelfareWnd:AddHuiLiuFund()
	-- 以下情况需要显示回流基金Tab
	-- 玩家购买了基金且奖励没有领取完
	-- 玩家没有购买基金且还在基金购买时间段内

	if LuaHuiLiuMgr.FundExpiredTime ~= 0 then -- FundExpiredTime为0表明没有触发回流

		local purchasedNeed = LuaHuiLiuMgr.FundPurchased and not LuaHuiLiuMgr.FundAllAwarded
		local notPurchasedNeed = not LuaHuiLiuMgr.FundPurchased and LuaHuiLiuMgr.FundExpiredTime > CServerTimeMgr.Inst.timeStamp
		local showAlert = LuaHuiLiuMgr.FundShowAlert or LuaHuiLiuMgr.HasFundAwardToTake()

		if purchasedNeed or notPurchasedNeed then
			self:AddButton(LocalString.GetString("回流基金"), showAlert, 11, self.OnHuiLiuFundClicked, false)
			self.HuiLiuFundIndex = #self.ButtonList - 1
			self:UpdateCurSelectIndex(LocalString.GetString("回流基金"), #self.ButtonList - 1)
		end
	end
end

function LuaNewWelfareWnd:OnHuiLiuFundClicked(go)
	self.HuiLiuFundWindow:SetActive(true)
	local luaScript = CommonDefs.GetComponent_GameObject_Type(self.HuiLiuFundWindow, typeof(CCommonLuaScript))
	luaScript:Init({})
	local showAlert = LuaHuiLiuMgr.HasFundAwardToTake()
	CommonDefs.GetComponent_Component_Type(go, typeof(CWelfareAlertCtl)):ShowAlert(showAlert)
end

function LuaNewWelfareWnd:OnNewHuiLiuUpdate(showAlert)
	if not self.HuiLiuFuliWindow.activeSelf then
		self:AddButton(LocalString.GetString("回流福利"), showAlert, 11, self.OnNewHuiLiuFundClicked, false)
		self.HuiLiuFundIndex = #self.ButtonList - 1
		self:UpdateCurSelectIndex(LocalString.GetString("回流福利"), #self.ButtonList - 1)
		self.TabTable:Reposition()
		self.TabScrollView:ResetPosition()
	end
end

function LuaNewWelfareWnd:OnRefresbUnity2018UpgradeInfo()
	self:OnOtherWelfareUpdate()
end

function LuaNewWelfareWnd:OnNewHuiLiuFundClicked(go)
	self.HuiLiuFuliWindow:SetActive(true)
	local luaScript = CommonDefs.GetComponent_GameObject_Type(self.HuiLiuFuliWindow, typeof(CCommonLuaScript))
	luaScript:Init({})
	local showAlert = false
	CommonDefs.GetComponent_Component_Type(go, typeof(CWelfareAlertCtl)):ShowAlert(showAlert)
end

--@desc 刷新回流基金的小红点
function LuaNewWelfareWnd:OnHuiLiuFundUpdate()
	if self.HuiLiuFundIndex < #self.ButtonList and self.OtherWelfareIndex >= 0 then
		local go = self.ButtonList[self.HuiLiuFundIndex+1]
		local showAlert = LuaHuiLiuMgr.HasFundAwardToTake()
		CommonDefs.GetComponent_Component_Type(go, typeof(CWelfareAlertCtl)):ShowAlert(showAlert)
	end
end

--@endregion


--代言人周边-萌物杂货铺
function LuaNewWelfareWnd:AddSpokesmanZhouBian()
	self:AddButton(LocalString.GetString("萌物杂货铺"), false, 12, self.ClickSpokesmanZhouBian, false)
	self:UpdateCurSelectIndex(LocalString.GetString("萌物杂货铺"), #self.ButtonList - 1)
end
function LuaNewWelfareWnd:ClickSpokesmanZhouBian()
	self.SpokesmanZhouBianWindow:SetActive(true)
	if not self.SpokesmanZhouBianInited then
		local luaScript = self.SpokesmanZhouBianWindow:GetComponent(typeof(CCommonLuaScript))
        luaScript:Init({})
		self.SpokesmanZhouBianInited = true
	end
end

--@SEA渠道关注
function LuaNewWelfareWnd:AddSocialFollow()
	if not CClientMainPlayer.Inst then
		return
	end

	if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.IsCrossServerPlayer then
		return
	end

	if CClientMainPlayer.Inst.MaxLevel >= 10 then
		local showAlert = LuaSEASdkMgr:IsSocialFollowShowAlert()
		self:AddButton(LocalString.GetString("关注有礼"), showAlert, 22, self.OnSocialFollowClicked, false)
		self.SocialFollowIndex = #self.ButtonList -1
		self:UpdateCurSelectIndex(LocalString.GetString("关注有礼"), #self.ButtonList - 1)
	end
end

function LuaNewWelfareWnd:OnSocialFollowClicked()
	self.SocialFollowWindow:SetActive(true)
	local luaScript = CommonDefs.GetComponent_GameObject_Type(self.SocialFollowWindow, typeof(CCommonLuaScript))
	luaScript:Init({})
end

--@app评分
function LuaNewWelfareWnd:AddRateApp()
	if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.IsCrossServerPlayer then
		return
	end
	
	if LuaSEASdkMgr:IsVoteAppOpen() then
		local showAlert = not CommonDefs.Is_PC_PLATFORM()
		self:AddButton(LocalString.GetString("VoteApp"), showAlert, 22, self.OnRateAPPClicked, false)
		self.RateAPPIndex = #self.ButtonList -1
		self:UpdateCurSelectIndex(LocalString.GetString("VoteApp"), #self.ButtonList - 1)
	end
end

function LuaNewWelfareWnd:OnRateAPPClicked()
	self.AppRateWindow:SetActive(true)
	local luaScript = CommonDefs.GetComponent_GameObject_Type(self.AppRateWindow, typeof(CCommonLuaScript))
	luaScript:Init({})
end

--@region 转职基金
function LuaNewWelfareWnd:AddZhuanZhiFund()
	-- 显示tab的条件
	-- 未购买，且在购买时间内
	-- 已购买，在可领取时间且没有全部领取
	if LuaProfessionTransferMgr.ShowZhuanzhiFundActivity() then
		local notPurchasedNeed = not LuaProfessionTransferMgr.FundPurchased and LuaProfessionTransferMgr.FundNowTime < LuaProfessionTransferMgr.FundEndTime
		local purchasedNeed = LuaProfessionTransferMgr.FundPurchased and not LuaProfessionTransferMgr.FundAllAwarded and LuaProfessionTransferMgr.FundNowTime < LuaProfessionTransferMgr.FundTaskExpireTime
		local showAlert = (LuaProfessionTransferMgr.FundShowAlert or LuaProfessionTransferMgr.HasFundAwardToTake()) and (notPurchasedNeed or purchasedNeed)
		self:AddButton(LocalString.GetString("转职基金"), showAlert, 11, self.OnZhuanZhiFundClicked, false)
		self.ZhuanZhiFundIndex = #self.ButtonList -1
		self:UpdateCurSelectIndex(LocalString.GetString("转职基金"), #self.ButtonList - 1)
	end
end

function LuaNewWelfareWnd:OnZhuanZhiFundClicked(go)
	self.ZhuanZhiFundWindow:SetActive(true)
	local luaScript = CommonDefs.GetComponent_GameObject_Type(self.ZhuanZhiFundWindow, typeof(CCommonLuaScript))
	luaScript:Init({})
	local showAlert = LuaProfessionTransferMgr.HasFundAwardToTake()
	CommonDefs.GetComponent_Component_Type(go, typeof(CWelfareAlertCtl)):ShowAlert(showAlert)
end

function LuaNewWelfareWnd:OnZhuanZhiFundUpdate()
	if self.ZhuanZhiFundIndex < #self.ButtonList and self.ZhuanZhiFundIndex >= 0 then
		local go = self.ButtonList[self.ZhuanZhiFundIndex+1]
		local showAlert = LuaProfessionTransferMgr.HasFundAwardToTake()
		CommonDefs.GetComponent_Component_Type(go, typeof(CWelfareAlertCtl)):ShowAlert(showAlert)
	end
end
--@endregion

--@region 全民开书
function LuaNewWelfareWnd:AddBuyBook()
	if LuaProfessionTransferMgr.ShowBuyBookActivity() then
		if CClientMainPlayer.Inst then
			self:AddButton(LocalString.GetString("开书比拼"), false, 11, function()
				self.BuyBookWnd:SetActive(true)
				local luaScript = CommonDefs.GetComponent_GameObject_Type(self.BuyBookWnd, typeof(CCommonLuaScript))
				luaScript:Init({})
			end , false)
			self.AddBookIndex = #self.ButtonList -1
			self:UpdateCurSelectIndex(LocalString.GetString("开书比拼"), #self.ButtonList - 1)
		end
	end
end

--@endregion