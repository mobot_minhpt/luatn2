-- Auto Generated!!
local CChargeWnd = import "L10.UI.CChargeWnd"
local CChargeWnd_Privilege = import "L10.UI.CChargeWnd_Privilege"
local CommonDefs = import "L10.Game.CommonDefs"
local OnCenterCallback = import "UICenterOnChild+OnCenterCallback"
local QnButton = import "L10.UI.QnButton"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local UICenterOnChild = import "UICenterOnChild"
local CUICenterOnChild = import "L10.UI.CUICenterOnChild"
CChargeWnd_Privilege.m_OnEnable_CS2LuaHook = function (this) 
    this.m_PrevButton.gameObject:SetActive(this.m_CurrentRow >= 1)
    this.m_NextButton.gameObject:SetActive(this.m_CurrentRow < CChargeWnd.MAX_VIP_LEVEL - 1)

    this.m_PrevButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_PrevButton.OnClick, MakeDelegateFromCSFunction(this.OnPrevPage, MakeGenericClass(Action1, QnButton), this), true)
    this.m_NextButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_NextButton.OnClick, MakeDelegateFromCSFunction(this.OnNextPage, MakeGenericClass(Action1, QnButton), this), true)
    this.m_CenterOnChild.onCenter = CommonDefs.CombineListner_OnCenterCallback(this.m_CenterOnChild.onCenter, MakeDelegateFromCSFunction(this.OnCenterPage, OnCenterCallback, this), true)
    UIEventListener.Get(this.m_CloseButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_CloseButton).onClick, MakeDelegateFromCSFunction(this.OnClose, VoidDelegate, this), true)
end
CChargeWnd_Privilege.m_OnNextPage_CS2LuaHook = function (this, button) 
    if this.m_CurrentRow < CChargeWnd.MAX_VIP_LEVEL - 1 then
        this.m_CurrentRow = this.m_CurrentRow + 1
        if ENABLE_XLUA then
            cast(this.m_CenterOnChild, typeof(UICenterOnChild))
			this.m_CenterOnChild:CenterOn(this.m_TableView:GetItemAtRow(this.m_CurrentRow).transform)
			cast(this.m_CenterOnChild, typeof(CUICenterOnChild))
        else
            this.m_CenterOnChild:CenterOn(this.m_TableView:GetItemAtRow(this.m_CurrentRow).transform)
        end
    end
end
CChargeWnd_Privilege.m_OnPrevPage_CS2LuaHook = function (this, button) 
    if this.m_CurrentRow >= 1 then
        this.m_CurrentRow = this.m_CurrentRow - 1
        if ENABLE_XLUA then
            cast(this.m_CenterOnChild, typeof(UICenterOnChild))
			this.m_CenterOnChild:CenterOn(this.m_TableView:GetItemAtRow(this.m_CurrentRow).transform)
			cast(this.m_CenterOnChild, typeof(CUICenterOnChild))
        else
            this.m_CenterOnChild:CenterOn(this.m_TableView:GetItemAtRow(this.m_CurrentRow).transform)
        end
    end
end
CChargeWnd_Privilege.m_OnCenterPage_CS2LuaHook = function (this, go) 
    do
        local i = 0
        while i < CChargeWnd.MAX_VIP_LEVEL do
            local item = this.m_TableView:GetItemAtRow(i)
            if item.gameObject == go then
                this.m_CurrentRow = i
                break
            end
            i = i + 1
        end
    end
    this.m_PrevButton.gameObject:SetActive(this.m_CurrentRow >= 1)
    this.m_NextButton.gameObject:SetActive(this.m_CurrentRow < CChargeWnd.MAX_VIP_LEVEL - 1)
end
