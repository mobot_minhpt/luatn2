-- Auto Generated!!
local BiWuRoundInfo = import "L10.UI.CBWDHResultWnd+BiWuRoundInfo"
local BiWuTeamRoundData = import "L10.Game.BiWuTeamRoundData"
local CBiWuDaHuiMgr = import "L10.Game.CBiWuDaHuiMgr"
local CBWDHResultItem = import "L10.UI.CBWDHResultItem"
local CBWDHResultWnd = import "L10.UI.CBWDHResultWnd"
local CGameVideoMgr = import "L10.Game.CGameVideoMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CQMJJMgr = import "L10.Game.CQMJJMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local Debug = import "UnityEngine.Debug"
local DelegateFactory = import "DelegateFactory"
local Gac2Gas = import "L10.Game.Gac2Gas"
local LocalString = import "LocalString"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CBWDHResultWnd.m_Clean_CS2LuaHook = function () 
    CBWDHResultWnd.winTeamList = nil
    CBWDHResultWnd.roundDataList = nil
    CBWDHResultWnd.teamDataList = nil
end
CBWDHResultWnd.m_Init_CS2LuaHook = function (this) 
    this.team1NameLabel.text = ""
    this.team2NameLabel.text = ""

    if CBWDHResultWnd.winTeamList == nil or CBWDHResultWnd.roundDataList == nil or CBWDHResultWnd.teamDataList == nil then
        if CBiWuDaHuiMgr.Inst.inBiWu or CBiWuDaHuiMgr.Inst.inBiWuPrepare then
            if CGameVideoMgr.Inst:IsInGameVideoScene() then
                CGameVideoMgr.Inst:QueryGameVideoBiWuTeamRoundData()
            else
                Gac2Gas.QueryBiWuTeamRoundData()
            end
        elseif CQMJJMgr.Inst.inQMJJ or CQMJJMgr.Inst.inQMJJPrepare then
            Gac2Gas.QueryQMJJTeamRoundData()
        end
    else
        this:UpdateBiWuTeamRoundData(CBWDHResultWnd.winTeamList, CBWDHResultWnd.roundDataList, CBWDHResultWnd.teamDataList)
    end

    --观战场景中置灰
    if CGameVideoMgr.Inst:IsInGameVideoScene() then
        CUICommonDef.SetActive(this.openSelectPlayerWndBtn, false, true)
    end
end
CBWDHResultWnd.m_Awake_CS2LuaHook = function (this) 
    UIEventListener.Get(this.openSelectPlayerWndBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        if CBiWuDaHuiMgr.Inst.inBiWu or CBiWuDaHuiMgr.Inst.inBiWuPrepare then
            Gac2Gas.RequestOpenBiWuSelectWnd()
        elseif CQMJJMgr.Inst.inQMJJ or CQMJJMgr.Inst.inQMJJPrepare then
            Gac2Gas.RequestOpenQMJJSelectWnd()
        end
    end)
    UIEventListener.Get(this.closeButton).onClick = MakeDelegateFromCSFunction(this.OnClickCloseButton, VoidDelegate, this)
end
CBWDHResultWnd.m_UpdateBiWuTeamRoundData_CS2LuaHook = function (this, list1, list2, list3) 

    local teamLeftId = 0
    local teamRightId = 0
    if list3.Count == 2 then
        this.team1NameLabel.text = list3[0].teamName
        this.team2NameLabel.text = list3[1].teamName

        teamLeftId = list3[0].teamId
        teamRightId = list3[1].teamId
    else
        Debug.LogError("error")
    end

    local team1Score = 0
    local team2Score = 0
    CommonDefs.ListIterate(list1, DelegateFactory.Action_object(function (___value) 
        local item = ___value
        if item == teamLeftId then
            team1Score = team1Score + 1
        elseif item == teamRightId then
            team2Score = team2Score + 1
        else
            Debug.LogError("error")
        end
    end))
    this.team1ScoreLabel.text = tostring(team1Score)
    this.team2ScoreLabel.text = tostring(team2Score)

    CommonDefs.ListClear(this.infoList)
    local runingIndex = list1.Count
    for i = 0, 4 do
        local info = CreateFromClass(BiWuRoundInfo)
        --赢的队伍
        info.round = i + 1
        info.leftData = CommonDefs.ListFind(list2, typeof(BiWuTeamRoundData), DelegateFactory.Predicate_BiWuTeamRoundData(function (p) 
            return p.round == (i + 1) and p.teamId == teamLeftId
        end))
        info.rightData = CommonDefs.ListFind(list2, typeof(BiWuTeamRoundData), DelegateFactory.Predicate_BiWuTeamRoundData(function (p) 
            return p.round == (i + 1) and p.teamId == teamRightId
        end))
        info.winTeamId = i < list1.Count and list1[i] or 0
        info.leftTeamId = teamLeftId
        info.rightTeamId = teamRightId
        CommonDefs.ListAdd(this.infoList, typeof(BiWuRoundInfo), info)
    end

    this.tableView.m_DataSource = this
    this.tableView:ReloadData(true, true)
    --看下那条数据为0的 就是选中的那条
    this.tableView:SetSelectRow(runingIndex, true)
end
CBWDHResultWnd.m_ItemAt_CS2LuaHook = function (this, view, row) 
    if row < this.infoList.Count then
        local cmp = TypeAs(this.tableView:GetFromPool(0), typeof(CBWDHResultItem))
        local info = this.infoList[row]
        cmp:Init(info.round, info.leftData, info.rightData, info.winTeamId, info.leftTeamId, info.rightTeamId)
        return cmp
    end
    return nil
end
