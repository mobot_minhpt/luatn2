-- Auto Generated!!
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CLingfuView = import "L10.UI.CLingfuView"
local CMapiLingfuItemCell = import "L10.UI.CMapiLingfuItemCell"
local CMapiViewSimpleItem = import "L10.UI.CMapiViewSimpleItem"
local Color = import "UnityEngine.Color"
local CommonDefs = import "L10.Game.CommonDefs"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local CZuoQiEquipProp = import "L10.Game.CZuoQiEquipProp"
local CZuoQiMgr = import "L10.Game.CZuoQiMgr"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EnumMapiLingfuPos = import "L10.Game.EnumMapiLingfuPos"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local Gac2Gas = import "L10.Game.Gac2Gas"
local Item_Item = import "L10.Game.Item_Item"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local MsgPackImpl = import "MsgPackImpl"
local Object = import "System.Object"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local UInt32 = import "System.UInt32"
local Vector3 = import "UnityEngine.Vector3"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local ZuoQi_MapiUpgrade = import "L10.Game.ZuoQi_MapiUpgrade"
local ZuoQi_Setting = import "L10.Game.ZuoQi_Setting"
local ZuoQiData = import "L10.Game.ZuoQiData"
CLingfuView.m_Init_CS2LuaHook = function (this) 
    -- Mapi List
    local list = CreateFromClass(MakeGenericClass(List, ZuoQiData))
    do
        local i = 0
        while i < CZuoQiMgr.Inst.zuoqis.Count do
            local templateId = CZuoQiMgr.Inst.zuoqis[i].templateId
            if CZuoQiMgr.IsMapiTemplateId(templateId) then
                CommonDefs.ListAdd(list, typeof(ZuoQiData), CZuoQiMgr.Inst.zuoqis[i])
            end
            i = i + 1
        end
    end

    local setting = ZuoQi_Setting.GetData()

    CommonDefs.ListClear(this.oneKeyReclaimLingfuList)
    CommonDefs.DictClear(this.ItemDic)
    this.mapiViewTemplate:SetActive(false)
    Extensions.RemoveAllChildren(this.zuoqiGrid.transform)
    local maxMapiCount = setting.MapiMaxCount
    local majiuMapiCount = CZuoQiMgr.Inst:GetMajiuMapiCount()
    do
        local i = 0
        while i < maxMapiCount + majiuMapiCount do
            local icon = nil
            local showAdd = true
            local name = ""
            local level = 0
            local textureAction = nil
            local addAction = nil
            local id = tostring(i)
            if i < list.Count then
                local zqdata = list[i]
                if zqdata.mapiInfo ~= nil then
                    icon = setting.MapiIcon[zqdata.mapiInfo.FuseId]
                    if zqdata.quality == 4 then
                        icon = setting.SpecialMapiIcon[0]
                    elseif zqdata.quality == 5 then
						icon = setting.SpecialMapiIcon[1]
					end
                end
                name = zqdata.mapiName
                level = zqdata.mapiLevel
                showAdd = false
                id = zqdata.id
                local closureId = zqdata.id
                textureAction = DelegateFactory.Action(function () 
                    this:ShowMapi(closureId)
                end)
            else
                addAction = DelegateFactory.Action(function () 
                    CUIManager.ShowUI(CUIResources.SelectAddMapiWnd)
                end)
            end


            local item = CommonDefs.Object_Instantiate(this.mapiViewTemplate)
            item.transform.parent = this.zuoqiGrid.transform
            item.transform.localScale = Vector3.one
            item.gameObject:SetActive(true)

            local simpleItem = CommonDefs.GetComponent_GameObject_Type(item, typeof(CMapiViewSimpleItem))
            CommonDefs.DictAdd(this.ItemDic, typeof(String), id, typeof(CMapiViewSimpleItem), simpleItem)
            simpleItem:Init(icon, name, level, showAdd, nil, addAction, textureAction, nil)
            i = i + 1
        end
    end
    this.zuoqiGrid:Reposition()
    this.zuoqiScrollView:ResetPosition()

    if list.Count ~= 0 then
        local zuoqiId = list[0].id
        if CZuoQiMgr.Inst.curSelectedZuoqiId ~= nil and CZuoQiMgr.Inst.curSelectedZuoqiTid == setting.MapiTemplateId then
            zuoqiId = CZuoQiMgr.Inst.curSelectedZuoqiId
        end
        this:ShowMapi(zuoqiId)

        this.detailGO.gameObject:SetActive(true)
    else
        g_MessageMgr:ShowMessage("Lingfu_View_No_Mapi_Found")
        this.detailGO.gameObject:SetActive(false)
    end
    this:ShowBtns()
end
CLingfuView.m_ShowBtns_CS2LuaHook = function (this) 
    this.selectBtn:SetActive(not this.mbReclaimMode)
    this.selectAllBtn:SetActive(this.mbReclaimMode)
    this.returnBtn:SetActive(this.mbReclaimMode)
    this.reclaimAllBtn:SetActive(this.mbReclaimMode)
    this.onekeyReclaimBtn:SetActive(not this.mbReclaimMode)
    if this.selectBtn.activeInHierarchy then
        CommonDefs.GetComponentInChildren_GameObject_Type(this.selectBtn, typeof(UILabel)).text = CLingfuView.names[EnumToInt(this.mCurLingfuPosFilter)]
    end
end
CLingfuView.m_OnEnable_CS2LuaHook = function (this) 
    EventManager.AddListenerInternal(EnumEventType.OnSyncMapiInfo, MakeDelegateFromCSFunction(this.OnSyncMapiInfo, MakeGenericClass(Action1, String), this))
    EventManager.AddListenerInternal(EnumEventType.OnSyncMapiEquip, MakeDelegateFromCSFunction(this.OnSyncMapiInfo, MakeGenericClass(Action1, String), this))
    EventManager.AddListenerInternal(EnumEventType.SetItemAt, MakeDelegateFromCSFunction(this.OnSetItemAt, MakeGenericClass(Action4, EnumItemPlace, UInt32, String, String), this))
    EventManager.AddListener(EnumEventType.OnAddDeleteVehicle, MakeDelegateFromCSFunction(this.OnAddDeleteVehicle, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.OnSendMajiuMapiEquipList, MakeDelegateFromCSFunction(this.OnSendMajiuMapiEquipList, MakeGenericClass(Action1, String), this))

    UIEventListener.Get(this.huanlingBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.huanlingBtn).onClick, MakeDelegateFromCSFunction(this.OnHuanlingBtnClick, VoidDelegate, this), true)
    UIEventListener.Get(this.selectBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.selectBtn).onClick, MakeDelegateFromCSFunction(this.OnSelectBtnClick, VoidDelegate, this), true)
    UIEventListener.Get(this.selectAllBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.selectAllBtn).onClick, MakeDelegateFromCSFunction(this.OnSelectAllBtnClick, VoidDelegate, this), true)
    UIEventListener.Get(this.returnBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.returnBtn).onClick, MakeDelegateFromCSFunction(this.OnReturnBtnClick, VoidDelegate, this), true)
    UIEventListener.Get(this.reclaimAllBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.reclaimAllBtn).onClick, MakeDelegateFromCSFunction(this.OnReclaimAllBtnClick, VoidDelegate, this), true)
    UIEventListener.Get(this.onekeyReclaimBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.onekeyReclaimBtn).onClick, MakeDelegateFromCSFunction(this.OnOnekeyReclaimBtnClick, VoidDelegate, this), true)
    UIEventListener.Get(this.questionBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.questionBtn).onClick, MakeDelegateFromCSFunction(this.OnQuestionBtnClick, VoidDelegate, this), true)
end
CLingfuView.m_OnDisable_CS2LuaHook = function (this) 
    EventManager.RemoveListenerInternal(EnumEventType.OnSyncMapiInfo, MakeDelegateFromCSFunction(this.OnSyncMapiInfo, MakeGenericClass(Action1, String), this))
    EventManager.RemoveListenerInternal(EnumEventType.OnSyncMapiEquip, MakeDelegateFromCSFunction(this.OnSyncMapiInfo, MakeGenericClass(Action1, String), this))
    EventManager.RemoveListenerInternal(EnumEventType.SetItemAt, MakeDelegateFromCSFunction(this.OnSetItemAt, MakeGenericClass(Action4, EnumItemPlace, UInt32, String, String), this))
    EventManager.RemoveListener(EnumEventType.OnAddDeleteVehicle, MakeDelegateFromCSFunction(this.OnAddDeleteVehicle, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.OnSendMajiuMapiEquipList, MakeDelegateFromCSFunction(this.OnSendMajiuMapiEquipList, MakeGenericClass(Action1, String), this))

    UIEventListener.Get(this.huanlingBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.huanlingBtn).onClick, MakeDelegateFromCSFunction(this.OnHuanlingBtnClick, VoidDelegate, this), false)
    UIEventListener.Get(this.selectBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.selectBtn).onClick, MakeDelegateFromCSFunction(this.OnSelectBtnClick, VoidDelegate, this), false)
    UIEventListener.Get(this.selectAllBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.selectAllBtn).onClick, MakeDelegateFromCSFunction(this.OnSelectAllBtnClick, VoidDelegate, this), false)
    UIEventListener.Get(this.returnBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.returnBtn).onClick, MakeDelegateFromCSFunction(this.OnReturnBtnClick, VoidDelegate, this), false)
    UIEventListener.Get(this.reclaimAllBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.reclaimAllBtn).onClick, MakeDelegateFromCSFunction(this.OnReclaimAllBtnClick, VoidDelegate, this), false)
    UIEventListener.Get(this.onekeyReclaimBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.onekeyReclaimBtn).onClick, MakeDelegateFromCSFunction(this.OnOnekeyReclaimBtnClick, VoidDelegate, this), false)
    UIEventListener.Get(this.questionBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.questionBtn).onClick, MakeDelegateFromCSFunction(this.OnQuestionBtnClick, VoidDelegate, this), false)

    this.mCurLingfuPosFilter = EnumMapiLingfuPos.eAll
    CLingfuView.sCurZuoQiId = nil
end
CLingfuView.m_OnSelectAllBtnClick_CS2LuaHook = function (this, go) 
    CommonDefs.DictIterate(this.EquipDic, DelegateFactory.Action_object_object(function (___key, ___value) 
        local p = {}
        p.Key = ___key
        p.Value = ___value
        p.Value:CheckLingfu(true)
    end))
end
CLingfuView.m_OnReturnBtnClick_CS2LuaHook = function (this, go) 
    CommonDefs.ListClear(this.oneKeyReclaimLingfuList)
    this.mbReclaimMode = false
    this:ShowBtns()
    CommonDefs.DictIterate(this.EquipDic, DelegateFactory.Action_object_object(function (___key, ___value) 
        local p = {}
        p.Key = ___key
        p.Value = ___value
        p.Value:ShowCheckBox(false)
        p.Value:CheckLingfu(false)
    end))
end
CLingfuView.m_OnReclaimAllBtnClick_CS2LuaHook = function (this, go) 
    local names = ""
    do
        local i = 0
        while i < this.oneKeyReclaimLingfuList.Count do
            local lfid = this.oneKeyReclaimLingfuList[i]
            local item = CItemMgr.Inst:GetById(lfid)
            if item ~= nil and item.Item.LingfuItemInfo ~= nil and item.Item.LingfuItemInfo.Quality >= 3 then
                if not System.String.IsNullOrEmpty(names) then
                    names = names .. LocalString.GetString("、")
                end
                local data = Item_Item.GetData(item.TemplateId)
                names = names .. data.Name
            end
            i = i + 1
        end
    end

    if System.String.IsNullOrEmpty(names) then
        this:DoOneKeyReclaim()
    else
        local msg = g_MessageMgr:FormatMessage("Mapi_Reclaim_Lingfu_Confirm", names)
        MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function () 
            this:DoOneKeyReclaim()
        end), nil, nil, nil, false)
    end
end
CLingfuView.m_DoOneKeyReclaim_CS2LuaHook = function (this) 
    local posList = CreateFromClass(MakeGenericClass(List, Object))
    do
        local i = 0
        while i < this.oneKeyReclaimLingfuList.Count do
            local lfid = this.oneKeyReclaimLingfuList[i]
            local item = CItemMgr.Inst:GetById(lfid)
            if item.Item.LingfuItemInfo ~= nil then
                local pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, lfid)
                if pos > 0 then
                    CommonDefs.ListAdd(posList, typeof(UInt32), pos)
                end
            end
            if posList.Count >= CLingfuView.s_OnTimeReclaimCount then
                Gac2Gas.RequestOneKeyReclaimLingfuItems(MsgPackImpl.pack(posList))
                CommonDefs.ListClear(posList)
            end
            i = i + 1
        end
    end
    if posList.Count > 0 then
        Gac2Gas.RequestOneKeyReclaimLingfuItems(MsgPackImpl.pack(posList))
    end
    CommonDefs.ListClear(this.oneKeyReclaimLingfuList)
end
CLingfuView.m_OnOnekeyReclaimBtnClick_CS2LuaHook = function (this, go) 
    this.mbReclaimMode = true
    this:ShowBtns()
    CommonDefs.DictIterate(this.EquipDic, DelegateFactory.Action_object_object(function (___key, ___value) 
        local p = {}
        p.Key = ___key
        p.Value = ___value
        p.Value:ShowCheckBox(true)
    end))
end
CLingfuView.m_OnSetItemAt_CS2LuaHook = function (this, place, pos, oldItemId, newItemId) 
    if place == EnumItemPlace.Bag and CLingfuView.sCurZuoQiId ~= nil then
        local zqdata = CZuoQiMgr.Inst:GetZuoQiById(CLingfuView.sCurZuoQiId)
        if zqdata ~= nil and zqdata.mapiInfo ~= nil then
            this:ShowLingfuList(zqdata.mapiInfo)
        end
    end
end
CLingfuView.m_ShowLingfuList_CS2LuaHook = function (this, mapiInfo) 
    if CClientMainPlayer.Inst == nil then
        return
    end

    local openHoleNum = 0
    local upgradeData = ZuoQi_MapiUpgrade.GetData(mapiInfo.Level)
    if upgradeData ~= nil then
        openHoleNum = upgradeData.Hole
    end

    this.lingfuTemplate:SetActive(false)
    Extensions.RemoveAllChildren(this.lingfuTbl.transform)

    local itemProp = CClientMainPlayer.Inst.ItemProp
    local bagSize = itemProp:GetPlaceSize(EnumItemPlace.Bag)

    local lingfus = CreateFromClass(MakeGenericClass(List, String))
    do
        local i = 1
        while i <= bagSize do
            local id = itemProp:GetItemAt(EnumItemPlace.Bag, i)
            if id ~= nil then
                local item = CItemMgr.Inst:GetById(id)
                if item ~= nil then
                    local lingfuPos = CZuoQiMgr.GetLingfuPosByTemplateId(item.TemplateId)
                    if lingfuPos > 0 and (this.mCurLingfuPosFilter == EnumMapiLingfuPos.eAll or lingfuPos == EnumToInt(this.mCurLingfuPosFilter)) then
                        CommonDefs.ListAdd(lingfus, typeof(String), id)
                    end
                end
            end
            i = i + 1
        end
    end
    CommonDefs.ListSort1(lingfus, typeof(String), DelegateFactory.Comparison_string(function (s1, s2) 
        local res = 0
        local item1 = CItemMgr.Inst:GetById(s1)
        local item2 = CItemMgr.Inst:GetById(s2)
        if item1 ~= nil and item2 ~= nil and item1.Item.LingfuItemInfo ~= nil and item2.Item.LingfuItemInfo ~= nil then
            if item1.Item.LingfuItemInfo.Quality == item2.Item.LingfuItemInfo.Quality then
                return NumberCompareTo(item2.Item.LingfuItemInfo.TaozhuangId, item1.Item.LingfuItemInfo.TaozhuangId)
            else
                res = NumberCompareTo(item2.Item.LingfuItemInfo.Quality, item1.Item.LingfuItemInfo.Quality)
            end
        end
        return res
    end))


    CommonDefs.DictClear(this.EquipDic)
    do
        local i = 0
        while i < lingfus.Count do
            local id = lingfus[i]
            if id ~= nil then
                local item = CItemMgr.Inst:GetById(id)
                if item ~= nil then
                    local itemcell = CommonDefs.Object_Instantiate(this.lingfuTemplate)
                    itemcell.transform.parent = this.lingfuTbl.transform
                    itemcell.transform.localScale = Vector3.one
                    itemcell.gameObject:SetActive(true)

                    local simpleItem = CommonDefs.GetComponent_GameObject_Type(itemcell, typeof(CMapiLingfuItemCell))
                    CommonDefs.DictAdd(this.EquipDic, typeof(String), id, typeof(CMapiLingfuItemCell), simpleItem)
                    simpleItem:Init(CLingfuView.sCurZuoQiId, item, openHoleNum, DelegateFactory.Action(function () 
                        this:SelectLingfu(id)
                    end), DelegateFactory.Action_string_bool(function (lfid, bVal) 
                        this:CheckLingfuBox(lfid, bVal)
                    end), this.mbReclaimMode)
                end
            end
            i = i + 1
        end
    end

    this.lingfuTbl:Reposition()
end
CLingfuView.m_CheckLingfuBox_CS2LuaHook = function (this, lingfuId, bVal) 
    if bVal then
        if not CommonDefs.ListContains(this.oneKeyReclaimLingfuList, typeof(String), lingfuId) then
            CommonDefs.ListAdd(this.oneKeyReclaimLingfuList, typeof(String), lingfuId)
        end
    else
        if CommonDefs.ListContains(this.oneKeyReclaimLingfuList, typeof(String), lingfuId) then
            CommonDefs.ListRemove(this.oneKeyReclaimLingfuList, typeof(String), lingfuId)
        end
    end
end
CLingfuView.m_SelectLingfu_CS2LuaHook = function (this, lingfuId) 
    CommonDefs.DictIterate(this.EquipDic, DelegateFactory.Action_object_object(function (___key, ___value) 
        local p = {}
        p.Key = ___key
        p.Value = ___value
        p.Value:SetSelected(lingfuId == p.Key)
    end))

    if not this.mbReclaimMode then
        local item = CItemMgr.Inst:GetById(lingfuId)
        if item ~= nil then
            local cell = nil
            (function () 
                local __try_get_result
                __try_get_result, cell = CommonDefs.DictTryGet(this.EquipDic, typeof(String), lingfuId, typeof(CMapiLingfuItemCell))
                return __try_get_result
            end)()
            CItemInfoMgr.ShowLinkItemInfo(item, false, cell, AlignType.Default, 0, 0, 0, 0, 0)
        end
    end
end
CLingfuView.m_ShowMapi_CS2LuaHook = function (this, zuoqiId) 
    CLingfuView.sCurZuoQiId = zuoqiId
    CZuoQiMgr.Inst.curSelectedZuoqiId = zuoqiId

    CommonDefs.DictIterate(this.ItemDic, DelegateFactory.Action_object_object(function (___key, ___value) 
        local p = {}
        p.Key = ___key
        p.Value = ___value
        p.Value:SetSelected(p.Key == zuoqiId)
    end))

    Gac2Gas.QueryMapiDetail(zuoqiId)
end
CLingfuView.m_OnSelectBtnClick_CS2LuaHook = function (this, go) 
    local menuList = CreateFromClass(MakeGenericClass(List, PopupMenuItemData))


    for i = 0, EnumMapiLingfuPos_lua.ePosCount do
        local pos = i
        CommonDefs.ListAdd(menuList, typeof(PopupMenuItemData), PopupMenuItemData(CLingfuView.names[i], DelegateFactory.Action_int(function (idx) 
            this:FilterMapiLingfu(idx)
        end), false, nil))
    end

    CPopupMenuInfoMgr.ShowPopupMenu(CommonDefs.ListToArray(menuList), this.selectBtn.transform, CPopupMenuInfoMgr.AlignType.Center)
end
CLingfuView.m_FilterMapiLingfu_CS2LuaHook = function (this, filterType) 
    this.mCurLingfuPosFilter = CommonDefs.ConvertIntToEnum(typeof(EnumMapiLingfuPos), filterType)
    if CLingfuView.sCurZuoQiId == nil then
        return
    end

    local zqdata = CZuoQiMgr.Inst:GetZuoQiById(CLingfuView.sCurZuoQiId)
    if zqdata ~= nil and zqdata.mapiInfo ~= nil then
        this:ShowLingfuList(zqdata.mapiInfo)
    end

    CommonDefs.GetComponentInChildren_GameObject_Type(this.selectBtn, typeof(UILabel)).text = CLingfuView.names[filterType]
end
CLingfuView.m_SelectMapiEquip_CS2LuaHook = function (this, equipId, pos) 
    if CLingfuView.sCurZuoQiId == nil then
        return
    end
    if System.String.IsNullOrEmpty(equipId) then
        return
    end

    local cell = this.equipList[pos]
    local item = CItemMgr.Inst:GetById(equipId)
    if item ~= nil then
        CItemInfoMgr.ShowLinkItemInfo(item, true, cell, AlignType.Default, 0, 0, 0, 0, 0)
    end
end
CLingfuView.m_ShowEquipList_CS2LuaHook = function (this, mapiInfo) 
    if CClientMainPlayer.Inst == nil then
        return
    end

    local bIsMajiuMapi = false
    local majiuEquipList = nil
    do
        local i = 0
        while i < CZuoQiMgr.Inst.zuoqis.Count do
            if CLingfuView.sCurZuoQiId == CZuoQiMgr.Inst.zuoqis[i].id then
                bIsMajiuMapi = CZuoQiMgr.Inst.zuoqis[i].isMajiuMapi
                majiuEquipList = CZuoQiMgr.Inst.zuoqis[i].majiuMapiEquipList
            end
            i = i + 1
        end
    end

    local equip = nil
    if bIsMajiuMapi then
        if majiuEquipList ~= nil and majiuEquipList.Count == 6 then
            equip = CreateFromClass(CZuoQiEquipProp)
            for i = 0, 5 do
                equip.Equip[i + 1] = majiuEquipList[i]
            end
        end
    else
        (function () 
            local __try_get_result
            __try_get_result, equip = CommonDefs.DictTryGet(CClientMainPlayer.Inst.ItemProp.ZuoQiEquip, typeof(String), CLingfuView.sCurZuoQiId, typeof(CZuoQiEquipProp))
            return __try_get_result
        end)()
    end

    local openHoleNum = 0
    local upgradeData = ZuoQi_MapiUpgrade.GetData(mapiInfo.Level)
    if upgradeData ~= nil then
        openHoleNum = upgradeData.Hole
    end

    local bNoticed = false
    do
        local i = 0
        while i < this.equipList.Length do
            local cell = this.equipList[i]
            local equipId = nil
            if equip ~= nil and equip.Equip.Length > i + 1 then
                equipId = equip.Equip[i + 1]
            end
            local bLocked = (openHoleNum <= i)
            local itemIcon = nil
            local c = Color.white
            if equipId ~= nil then
                local item = CItemMgr.Inst:GetById(equipId)
                if item ~= nil then
                    local ittid = item.TemplateId
                    local itemdata = Item_Item.GetData(ittid)
                    if itemdata ~= nil then
                        itemIcon = itemdata.Icon
                        local data = item.Item.LingfuItemInfo
                        if data ~= nil then
                            if not bNoticed then
                                if data.Duration <= 0 then
                                    g_MessageMgr:ShowMessage("Mapi_Lingfu_Duration_0_Notice", itemdata.Name)
                                    bNoticed = true
                                end
                            end

                            c = CZuoQiMgr.GetLingFuColor(data.Quality)
                        end
                    end
                end
            end
            local pos = i
            cell:Init(CLingfuView.sCurZuoQiId, CommonDefs.ConvertIntToEnum(typeof(EnumMapiLingfuPos), (i + 1)), equipId, bLocked, itemIcon, DelegateFactory.Action(function () 
                this:SelectMapiEquip(equipId, pos)
            end), c)

            this.textureLoader:Init(mapiInfo, nil, nil, 90, CLingfuView.sCurZuoQiId)
            i = i + 1
        end
    end
end
CLingfuView.m_OnSyncMapiInfo_CS2LuaHook = function (this, zuoqiId) 
    if CLingfuView.sCurZuoQiId == zuoqiId then
        local mapiInfo = nil
        do
            local i = 0
            while i < CZuoQiMgr.Inst.zuoqis.Count do
                if zuoqiId == CZuoQiMgr.Inst.zuoqis[i].id then
                    mapiInfo = CZuoQiMgr.Inst.zuoqis[i].mapiInfo
                    break
                end
                i = i + 1
            end
        end

        if mapiInfo ~= nil then
            this:ShowEquipList(mapiInfo)
            this:ShowLingfuList(mapiInfo)
        end


        CZuoQiMgr.Inst.curSelectedZuoqiId = zuoqiId
        CZuoQiMgr.Inst.curSelectedZuoqiTid = ZuoQi_Setting.GetData().MapiTemplateId
    end
end
CLingfuView.m_OnSendMajiuMapiEquipList_CS2LuaHook = function (this, zuoqiId) 
    if CLingfuView.sCurZuoQiId == zuoqiId then
        local mapiInfo = nil
        do
            local i = 0
            while i < CZuoQiMgr.Inst.zuoqis.Count do
                if zuoqiId == CZuoQiMgr.Inst.zuoqis[i].id then
                    mapiInfo = CZuoQiMgr.Inst.zuoqis[i].mapiInfo
                    break
                end
                i = i + 1
            end
        end

        if mapiInfo ~= nil then
            this:ShowEquipList(mapiInfo)
        end
    end
end
