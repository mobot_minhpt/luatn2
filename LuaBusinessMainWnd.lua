local TweenWidth = import "TweenWidth"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local LuaTweenUtils = import "LuaTweenUtils"
local CButton = import "L10.UI.CButton"

LuaBusinessMainWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaBusinessMainWnd, "BusinessView", "BusinessView", GameObject)
RegistChildComponent(LuaBusinessMainWnd, "BusinessDailyEndView", "BusinessDailyEndView", GameObject)
RegistChildComponent(LuaBusinessMainWnd, "BusinessModeSelectView", "BusinessModeSelectView", GameObject)
RegistChildComponent(LuaBusinessMainWnd, "BusinessCitySelectView", "BusinessCitySelectView", GameObject)
RegistChildComponent(LuaBusinessMainWnd, "BusinessScrollView", "BusinessScrollView", GameObject)
RegistChildComponent(LuaBusinessMainWnd, "WndBg", "WndBg", TweenWidth)

--@endregion RegistChildComponent end
RegistClassMember(LuaBusinessMainWnd, "m_BusinessViewWidget")
RegistClassMember(LuaBusinessMainWnd, "m_BusinessModeSelectViewWidget")
RegistClassMember(LuaBusinessMainWnd, "m_BusinessCitySelectViewWidget")
RegistClassMember(LuaBusinessMainWnd, "m_BusinessScrollViewWidget")
RegistClassMember(LuaBusinessMainWnd, "m_Widgets")
RegistClassMember(LuaBusinessMainWnd, "m_AnimTweeners")
RegistClassMember(LuaBusinessMainWnd, "m_CloseBtn")
RegistClassMember(LuaBusinessMainWnd, "m_IsPlaying")
RegistClassMember(LuaBusinessMainWnd, "m_LastView")

function LuaBusinessMainWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    LuaBusinessMgr:RefreshAll()

    self.m_BusinessViewWidget           = self.BusinessView:GetComponent(typeof(UIWidget))
    self.m_BusinessModeSelectViewWidget = self.BusinessModeSelectView:GetComponent(typeof(UIWidget))
    self.m_BusinessCitySelectViewWidget = self.BusinessCitySelectView:GetComponent(typeof(UIWidget))
    self.m_BusinessScrollViewWidget     = self.BusinessScrollView:GetComponent(typeof(UIWidget))
    self.m_CloseBtn                     = self.transform:Find("BusinessScrollView/Wnd_Bg_Secondary_2/CloseButton"):GetComponent(typeof(CButton))

    self.m_Widgets = {self.m_BusinessViewWidget, self.m_BusinessModeSelectViewWidget, self.m_BusinessCitySelectViewWidget}
    self.m_AnimTweeners = {}

    UIEventListener.Get(self.m_CloseBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.CloseUI(CLuaUIResources.BusinessMainWnd)
    end)
    self.WndBg:SetCurrentValueToStart()
    self.m_IsPlaying = false
end

function LuaBusinessMainWnd:Init()
    self:RefreshState(true)
end

--@region UIEvent

--@endregion UIEvent

function LuaBusinessMainWnd:OnEnable()
    g_ScriptEvent:AddListener("OnBusinessDataUpdate", self, "OnBusinessDataUpdate")
end

function LuaBusinessMainWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnBusinessDataUpdate", self, "OnBusinessDataUpdate")
end

function LuaBusinessMainWnd:ShowView(view, noAnim)
    self:KillAll()
    if view == self.m_BusinessCitySelectViewWidget and self.m_LastView == self.m_BusinessViewWidget then
        self:OnBusinessDayEnd()
    end
    self.m_LastView = view
    for i = 1, #self.m_Widgets do
        local widget = self.m_Widgets[i]
        
        if view == widget then
            widget.gameObject:SetActive(true)
        end

        if not noAnim then
            local tweener = LuaTweenUtils.TweenAlpha(widget, widget.alpha, view == widget and 1 or 0, 1)
            CommonDefs.OnComplete_Tweener(tweener, DelegateFactory.TweenCallback(function()
                if view ~= widget then
                    widget.gameObject:SetActive(false)
                end
            end))

            table.insert(self.m_AnimTweeners, tweener)
        else
            if view ~= widget then
                widget.gameObject:SetActive(false)
            end
        end

        -- 模式选择界面和城市选择界面共用一个背景:BusinessScrollView
        if view ~= self.m_BusinessViewWidget then
            self.BusinessScrollView:SetActive(true)
        end
        if not noAnim then
            local tweener = LuaTweenUtils.TweenAlpha(self.m_BusinessScrollViewWidget, self.m_BusinessScrollViewWidget.alpha, view ~= self.m_BusinessViewWidget and 1 or 0, 1)
            CommonDefs.OnComplete_Tweener(tweener, DelegateFactory.TweenCallback(function()
                if view == self.m_BusinessViewWidget then
                    self.BusinessScrollView:SetActive(false)
                    self.WndBg.enabled = false
                    self.WndBg:SetCurrentValueToEnd()
                end
            end))
            table.insert(self.m_AnimTweeners, tweener)
        else
            if view == self.m_BusinessViewWidget then
                self.BusinessScrollView:SetActive(false)
                self.WndBg.enabled = false
                self.WndBg:SetCurrentValueToEnd()
            end
        end
    end
end

function LuaBusinessMainWnd:OnBusinessDayEnd()
    self.BusinessDailyEndView:SetActive(true)
end

function LuaBusinessMainWnd:OnBusinessDataUpdate()
    self:RefreshState()
end

function LuaBusinessMainWnd:RefreshState(noAnim)
    if CClientMainPlayer.Inst then
        local tradeInfo = CClientMainPlayer.Inst.PlayProp.TradeSimulateInfo
        if tradeInfo.PlayType ~= 0 then
            -- 正在玩法
            self.m_IsPlaying = true
            Gac2Gas.StartTradeSimulation(tradeInfo.PlayType)
            if tradeInfo.Repository.IsSleep == 1 then
                self:ShowView(self.m_BusinessCitySelectViewWidget, noAnim)
            else
                self:ShowView(self.m_BusinessViewWidget, noAnim)
            end
        else
            -- 未开启玩法
            if not self.m_IsPlaying then
                self:ShowView(self.m_BusinessModeSelectViewWidget, noAnim)
            else
                CUIManager.CloseUI(CLuaUIResources.BusinessMainWnd) 
            end
        end
    end
end

function LuaBusinessMainWnd:OnDestroy()
    self:KillAll()
    LuaBusinessMgr:ClearAll()
end

function LuaBusinessMainWnd:KillAll()
    if self.m_AnimTweeners then
        for i = 1, #self.m_AnimTweeners do
            LuaTweenUtils.Kill(self.m_AnimTweeners[i], false)
        end
    end
    self.m_AnimTweeners = {}
end

function LuaBusinessMainWnd:GetGuideGo(methodName)
    local go = nil
    if methodName == "GetHangZhouBtn" or methodName == "GetStartBtn" then
        go = self.BusinessCitySelectView.gameObject
    elseif methodName == "GetGoodToBuy" or methodName == "GetBuyBtn" or methodName == "GetEndDayBtn" then
        go = self.BusinessView.gameObject
    end

    if go then
        local scirpt = go:GetComponent(typeof(CCommonLuaScript))
        if scirpt and scirpt.m_LuaSelf then
            return scirpt.m_LuaSelf:GetGuideGo(methodName)
        end
    end
	return nil
end
