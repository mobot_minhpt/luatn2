LuaSpokesmanCardTemplate = class()

RegistChildComponent(LuaSpokesmanCardTemplate, "m_NumLabel","NumLabel",UILabel)
RegistChildComponent(LuaSpokesmanCardTemplate, "m_NameLabel","NameLabel",UILabel)
RegistChildComponent(LuaSpokesmanCardTemplate, "m_BlueTexture","BlueTexture",GameObject)
RegistChildComponent(LuaSpokesmanCardTemplate, "m_RedTexture","RedTexture",GameObject)
RegistChildComponent(LuaSpokesmanCardTemplate, "m_PurpleTexture","PurpleTexture",GameObject)
RegistChildComponent(LuaSpokesmanCardTemplate, "m_OwnerTexture","OwnerTexture",CUITexture)
RegistChildComponent(LuaSpokesmanCardTemplate, "m_SketchTexture","SketchTexture",CUITexture)
RegistChildComponent(LuaSpokesmanCardTemplate, "m_NewTipTexture","NewTipTexture",GameObject)
RegistChildComponent(LuaSpokesmanCardTemplate, "m_Border","Border",GameObject)
RegistChildComponent(LuaSpokesmanCardTemplate, "m_QiDaiLabel","QiDaiLabel",GameObject)
RegistChildComponent(LuaSpokesmanCardTemplate, "m_Fx","Fx",CUIFx)

RegistClassMember(LuaSpokesmanCardTemplate, "m_IsOwner")
RegistClassMember(LuaSpokesmanCardTemplate, "m_IsSelected")

function LuaSpokesmanCardTemplate:Awake()
    self.gameObject:SetActive(false)
    self.m_NewTipTexture:SetActive(false)
    self.m_BlueTexture:SetActive(false)
    self.m_RedTexture:SetActive(false)
    self.m_PurpleTexture:SetActive(false)
    self.m_OwnerTexture.gameObject:SetActive(false)
    self.m_SketchTexture.gameObject:SetActive(false)
    self.m_IsSelected = false
    self.m_Border:SetActive(self.m_IsSelected)
    if self.m_QiDaiLabel then
        self.m_QiDaiLabel:SetActive(false)
    end
end

function LuaSpokesmanCardTemplate:InitCard(tcgID,owner,isNew,num)
    self.m_IsOwner = owner
    local data = SpokesmanTCG_TCG.GetData(tcgID)
    self.m_OwnerTexture.gameObject:SetActive(owner and data.Status ~= 3)
    self.m_SketchTexture.gameObject:SetActive(not owner or data.Status == 3)
    if data.Status ~= 3 then
        self.m_OwnerTexture:LoadMaterial(data.Res)
        if data.Type == 3 then
            self.m_Fx:LoadFx("fx/ui/prefab/UI_kapaikuang_zise.prefab")
        elseif data.Type == 2 then
            self.m_Fx:LoadFx("fx/ui/prefab/UI_kapaikuang_chengse.prefab")
        end
    end
    self.m_SketchTexture:LoadMaterial(data.Silhouette)
    self.m_BlueTexture:SetActive(data.Type == 1)
    self.m_RedTexture:SetActive(data.Type == 2)
    self.m_PurpleTexture:SetActive(data.Type == 3)
    self.gameObject:SetActive(true)
    self.m_NewTipTexture:SetActive(isNew)
    self.m_NumLabel.text = num
    self.m_NameLabel.text = data.Name
    if num == 0 or data.Status == 3 then
        self.m_NumLabel.gameObject:SetActive(false)
    end
    if self.m_QiDaiLabel and data.Status == 3 then
        self.m_QiDaiLabel:SetActive(true)
    end
end

function LuaSpokesmanCardTemplate:OnCardClick()
    self.m_NewTipTexture:SetActive(false)
    self:SetSelect(true)

    if not self.m_IsOwner then
        g_MessageMgr:ShowMessage("SpokesmanCard_NotHave")
    end
end

function LuaSpokesmanCardTemplate:SetSelect(isSelected)
    self.m_IsSelected = isSelected
    self.m_Border:SetActive(self.m_IsSelected)
end
