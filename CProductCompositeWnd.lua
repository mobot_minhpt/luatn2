-- Auto Generated!!
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CLivingSkillQuickMakeMgr = import "L10.UI.CLivingSkillQuickMakeMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CProductCompositeWnd = import "L10.UI.CProductCompositeWnd"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local Item_Item = import "L10.Game.Item_Item"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local Zhuangshiwu_ProducePlant = import "L10.Game.Zhuangshiwu_ProducePlant"
local CItemExchangeMaterial = import "L10.UI.CItemExchangeMaterial"

CProductCompositeWnd.m_Init_CS2LuaHook = function (this)
    if CProductCompositeWnd.TargetItemTempId ~= 0 then
        this:InitByTargetItemTempId()
    end

    if not CommonDefs.DictContains(Zhuangshiwu_ProducePlant.GetData().ProduceMap, typeof(UInt32), CProductCompositeWnd.ResouceItemTempId) then
        return
    end
    this.currentProduceInfo = CommonDefs.DictGetValue(Zhuangshiwu_ProducePlant.GetData().ProduceMap, typeof(UInt32), CProductCompositeWnd.ResouceItemTempId)
    local resourceItem = Item_Item.GetData(CProductCompositeWnd.ResouceItemTempId)
    this.resourceIcon:LoadMaterial(resourceItem.Icon)
    this.qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(resourceItem, nil, false)

    local productItem = Item_Item.GetData(this.currentProduceInfo.IconDisplayItemId)
    this.productIcon:LoadMaterial(productItem.Icon)
    local addonItem = Item_Item.GetData(this.currentProduceInfo.PotId)
    this.addonIcon:LoadMaterial(addonItem.Icon)
    local addonUpdate = CommonDefs.GetComponent_Component_Type(this.addonIcon, typeof(CItemExchangeMaterial))
    addonUpdate:Init(this.currentProduceInfo.PotId,1,1, true)
    --addonUpdate.format = "{0}"
    --addonUpdate.templateId = this.currentProduceInfo.PotId
    --addonUpdate:UpdateCount()

    local resourceUpdate = CommonDefs.GetComponent_Component_Type(this.resourceIcon, typeof(CItemExchangeMaterial))
    resourceUpdate:Init(CProductCompositeWnd.ResouceItemTempId,this.currentProduceInfo.ResourceNum,1, true)
    --resourceUpdate.format = "{0}/" .. tostring(this.currentProduceInfo.ResourceNum)
    --resourceUpdate.templateId = CProductCompositeWnd.ResouceItemTempId
    --resourceUpdate:UpdateCount()
    this.countBtn:SetMinMax(1, 999, 1)
    this.countBtn:SetValue(1, true)
    this:UpdateGenerateBtnStatus()
end
CProductCompositeWnd.m_InitByTargetItemTempId_CS2LuaHook = function (this) 
    if not CommonDefs.DictContains(Zhuangshiwu_ProducePlant.GetData().AllProduct2Resource, typeof(UInt32), CProductCompositeWnd.TargetItemTempId) then
        CUIManager.CloseUI(CUIResources.ProductCompositeWnd)
        return
    end

    local resourceId = 0
    local resourceList = CommonDefs.DictGetValue(Zhuangshiwu_ProducePlant.GetData().AllProduct2Resource, typeof(UInt32), CProductCompositeWnd.TargetItemTempId)
    do
        local i = 0
        while i < resourceList.ResourceId.Length do
            if i == 0 then
                resourceId = resourceList.ResourceId[i]
            end
            local count = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, resourceList.ResourceId[i])
            if count > 0 then
                resourceId = resourceList.ResourceId[i]
                break
            end
            i = i + 1
        end
    end

    if resourceId ~= 0 then
        CProductCompositeWnd.ResouceItemTempId = resourceId
    end
    CProductCompositeWnd.TargetItemTempId = 0
end
CProductCompositeWnd.m_UpdateGenerateBtnStatus_CS2LuaHook = function (this) 
    --local bindItemCountInBag, notBindItemCountInBag
    --bindItemCountInBag, notBindItemCountInBag = CItemMgr.Inst:CalcItemCountInBagByTemplateId(CProductCompositeWnd.ResouceItemTempId)
    --local count = bindItemCountInBag + notBindItemCountInBag
    --this.generateBtn:GetComponent(typeof(CButton)).Enabled = count >= this.currentProduceInfo.ResourceNum
end
CProductCompositeWnd.m_Awake_CS2LuaHook = function (this) 
    UIEventListener.Get(this.generateBtn).onClick = MakeDelegateFromCSFunction(this.OnGenerateBtnClick, VoidDelegate, this)
    UIEventListener.Get(this.closeButton).onClick = MakeDelegateFromCSFunction(this.OnClickCloseButton, VoidDelegate, this)
    UIEventListener.Get(this.ProductGO).onClick = MakeDelegateFromCSFunction(this.OnProductGOClick, VoidDelegate, this)
    UIEventListener.Get(this.ResourceGO).onClick = MakeDelegateFromCSFunction(this.OnResourceGOClick, VoidDelegate, this)
    UIEventListener.Get(this.HuaxiangGO).onClick = MakeDelegateFromCSFunction(this.OnHuaxiangGOClick, VoidDelegate, this)
end
CProductCompositeWnd.m_OnProductGOClick_CS2LuaHook = function (this, go) 
    if this.currentProduceInfo ~= nil then
        CItemInfoMgr.ShowLinkItemTemplateInfo(this.currentProduceInfo.IconDisplayItemId)
    end
end
CProductCompositeWnd.m_OnResourceGOClick_CS2LuaHook = function (this, go) 
    if this.currentProduceInfo ~= nil then
        CItemInfoMgr.ShowLinkItemTemplateInfo(CProductCompositeWnd.ResouceItemTempId)
    end
end
CProductCompositeWnd.m_OnHuaxiangGOClick_CS2LuaHook = function (this, go) 
    if this.currentProduceInfo ~= nil then
        local count = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, this.currentProduceInfo.PotId)
        if count > 0 then
            CItemInfoMgr.ShowLinkItemTemplateInfo(this.currentProduceInfo.PotId)
        else
            --CItemAccessListMgr.Inst.ShowItemAccessInfo(currentProduceInfo.PotId, false, go.transform);
            CLivingSkillQuickMakeMgr.Inst:ShowMakeTipWnd(this.currentProduceInfo.PotId)
        end
    end
end
CProductCompositeWnd.m_OnGenerateBtnClick_CS2LuaHook = function (this, go) 
    local count = this.countBtn:GetValue()
    local totalResourceCost = this.currentProduceInfo.ResourceNum * count
    local totalPotCount = count

    local resNumBind, resNumNotBind
    resNumBind, resNumNotBind = CItemMgr.Inst:CalcItemCountInBagByTemplateId(CProductCompositeWnd.ResouceItemTempId)
    local resNumInBag = resNumBind + resNumNotBind

    if resNumInBag < totalResourceCost then
        g_MessageMgr:ShowMessage("RESOURCE_NUM_NOT_ENOUGH")
        return
    end

    local potNumBind, potNumNotBind
    potNumBind, potNumNotBind = CItemMgr.Inst:CalcItemCountInBagByTemplateId(this.currentProduceInfo.PotId)
    local potNumInBag = potNumBind + potNumNotBind
    if potNumInBag < totalPotCount then
        g_MessageMgr:ShowMessage("POT_NOT_ENOUGH")
        return
    end

    Gac2Gas.PlayerRequestCompositeHuamu(CProductCompositeWnd.ResouceItemTempId, this.currentProduceInfo.IconDisplayItemId, count)
end
CProductCompositeWnd.m_OnSendItem_CS2LuaHook = function (this, itemId) 
    local item = CItemMgr.Inst:GetById(itemId)
    if item then
        if item.TemplateId == this.currentProduceInfo.PotId then
            local addonUpdate = CommonDefs.GetComponent_Component_Type(this.addonIcon, typeof(CItemExchangeMaterial))
            addonUpdate:Init(this.currentProduceInfo.PotId,1,1, true)
        end
        if item.TemplateId == CProductCompositeWnd.ResouceItemTempId then
            local resourceUpdate = CommonDefs.GetComponent_Component_Type(this.resourceIcon, typeof(CItemExchangeMaterial))
            resourceUpdate:Init(CProductCompositeWnd.ResouceItemTempId,this.currentProduceInfo.ResourceNum,1, true)
        end
    end
    this:UpdateGenerateBtnStatus()
end
CProductCompositeWnd.m_OnSetItemAt_CS2LuaHook = function (this, place, position, oldItemId, newItemId) 
    if oldItemId == nil and newItemId == nil then
        return
    end
    local default
    if oldItemId ~= nil then
        default = oldItemId
    else
        default = newItemId
    end
    local itemId = default

    this:OnSendItem(itemId)
end
