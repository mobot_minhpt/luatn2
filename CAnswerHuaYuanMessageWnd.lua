-- Auto Generated!!
local CAnswerHuaYuanMessageWnd = import "L10.UI.CAnswerHuaYuanMessageWnd"
local ChujiaMgr = import "L10.Game.ChujiaMgr"
local Constants = import "L10.Game.Constants"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local DelegateFactory = import "DelegateFactory"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
CAnswerHuaYuanMessageWnd.m_Init_CS2LuaHook = function (this) 
    this.nameLabel.text = System.String.Format(LocalString.GetString("[c][ffed5f]{0}[-][/c]对你说:"), ChujiaMgr.HuaYuanAskerName)
    this.messageLabel.text = ChujiaMgr.AskHuaYuanMessage
    this.moneyCtrl:SetType(EnumMoneyType.YinLiang, EnumPlayScoreKey.NONE, true)
    this.moneyCtrl:SetCost(ChujiaMgr.HuaYuanSilver)
end
CAnswerHuaYuanMessageWnd.m_SilverNotEnough_CS2LuaHook = function (this) 
    local txt = g_MessageMgr:FormatMessage("NotEnough_Silver_QuickBuy")
    MessageWndManager.ShowOKCancelMessage(txt, DelegateFactory.Action(function () 
        CShopMallMgr.ShowLinyuShoppingMall(Constants.SilverItemId)
    end), nil, nil, nil, false)
end
CAnswerHuaYuanMessageWnd.m_OnOkButtonClick_CS2LuaHook = function (this, go) 
    if not this.moneyCtrl.moneyEnough then
        this:SilverNotEnough()
    else
        Gac2Gas.ChuJiaHuaYuanAgree(ChujiaMgr.HuaYuanAskerId)
        this:Close()
    end
end
