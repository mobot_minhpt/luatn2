require("common/common_include")

local UIGrid = import "UIGrid"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local CUITexture = import "L10.UI.CUITexture"
local QianKunDai_Divine = import "L10.Game.QianKunDai_Divine"
local QianKunDai_Setting = import "L10.Game.QianKunDai_Setting"
local MessageMgr = import "L10.Game.MessageMgr"
local Color = import "UnityEngine.Color"
local CChatLinkMgr = import "CChatLinkMgr"
local UICamera = import "UICamera"
local MessageWndManager = import "L10.UI.MessageWndManager"

LuaBaGuaLuLingQiZhuRuWnd = class()

RegistClassMember(LuaBaGuaLuLingQiZhuRuWnd, "ZhuRuGrid")
RegistClassMember(LuaBaGuaLuLingQiZhuRuWnd, "ZhuRuPrefab")

RegistClassMember(LuaBaGuaLuLingQiZhuRuWnd, "OwnLingQi")
RegistClassMember(LuaBaGuaLuLingQiZhuRuWnd, "AddLingQiBtn")

RegistClassMember(LuaBaGuaLuLingQiZhuRuWnd, "YiZhuRuPercentLabel")
RegistClassMember(LuaBaGuaLuLingQiZhuRuWnd, "QnIncreseAndDecreaseButton")
RegistClassMember(LuaBaGuaLuLingQiZhuRuWnd, "OkButton")
RegistClassMember(LuaBaGuaLuLingQiZhuRuWnd, "CancelButton")
RegistClassMember(LuaBaGuaLuLingQiZhuRuWnd, "SelectedDivineItem")
RegistClassMember(LuaBaGuaLuLingQiZhuRuWnd, "Upper")
RegistClassMember(LuaBaGuaLuLingQiZhuRuWnd, "Lower")

-- data
RegistClassMember(LuaBaGuaLuLingQiZhuRuWnd, "LING_QI_STEP") -- 以10开始注入
RegistClassMember(LuaBaGuaLuLingQiZhuRuWnd, "DivineIndex")
RegistClassMember(LuaBaGuaLuLingQiZhuRuWnd, "CurrentPosZhuRuInfo")
RegistClassMember(LuaBaGuaLuLingQiZhuRuWnd, "CurrentPosTotalZhuRu")
RegistClassMember(LuaBaGuaLuLingQiZhuRuWnd, "SelectedZhuRuType") -- 0为没选，1为上卦，2为下卦
RegistClassMember(LuaBaGuaLuLingQiZhuRuWnd, "UpdateTick")

RegistClassMember(LuaBaGuaLuLingQiZhuRuWnd, "PlayerZhuRuZhuGua") -- 玩家主卦注入的灵力
RegistClassMember(LuaBaGuaLuLingQiZhuRuWnd, "PlayerZhuRuKeGua") -- 玩家注入客卦

function LuaBaGuaLuLingQiZhuRuWnd:Init()
	self:InitClassMembers()
	self:InitValues()
end

function LuaBaGuaLuLingQiZhuRuWnd:InitClassMembers()
	self.ZhuRuGrid = self.transform:Find("Anchor/LeftArea/ScrollView/ZhuRuGrid"):GetComponent(typeof(UIGrid))
	self.ZhuRuPrefab = self.transform:Find("Anchor/LeftArea/ZhuRuPrefab").gameObject
	self.ZhuRuPrefab:SetActive(false)
	
	self.OwnLingQi = self.transform:Find("Anchor/MyLingQi/OwnLingQi"):GetComponent(typeof(UILabel))
	self.AddLingQiBtn = self.transform:Find("Anchor/MyLingQi/OwnLingQi/AddLingQiBtn").gameObject

	self.YiZhuRuPercentLabel = self.transform:Find("Anchor/RightArea/YiZhuRu/YiZhuRuPercentLabel"):GetComponent(typeof(UILabel))
	self.QnIncreseAndDecreaseButton = self.transform:Find("Anchor/RightArea/QnIncreseAndDecreaseButton"):GetComponent(typeof(QnAddSubAndInputButton))
	self.OkButton = self.transform:Find("Anchor/RightArea/OkButton").gameObject
	self.CancelButton = self.transform:Find("Anchor/RightArea/CancelButton").gameObject
	self.SelectedDivineItem = self.transform:Find("Anchor/RightArea/SelectedDivineItem").gameObject
	self.Upper = self.transform:Find("Anchor/RightArea/Upper").gameObject
	self.Lower = self.transform:Find("Anchor/RightArea/Lower").gameObject

end

function LuaBaGuaLuLingQiZhuRuWnd:InitValues()

	self.DivineIndex = LuaBaGuaLuMgr.m_SelectedDivineIndex
	self.CurrentPosZhuRuInfo = nil
	self.CurrentPosTotalZhuRu = 0
	self.LING_QI_STEP = 10
	self.SelectedZhuRuType = 0
	self.PlayerZhuRuZhuGua = 0
	self.PlayerZhuRuKeGua = 0


	self.QnIncreseAndDecreaseButton:SetValue(0, false)

	if self.UpdateTick ~= nil then
      	UnRegisterTick(self.UpdateTick)
      	self.UpdateTick = nil
    end

    self.UpdateTick = RegisterTick(function ()
		Gac2Gas.RequestBaGuaLuZhuRuInfo(self.DivineIndex)
	end, 3000)

	local onAddLingQiBtnClicked = function (go)
		self:OnAddLingQiBtnClicked(go)
	end
	CommonDefs.AddOnClickListener(self.AddLingQiBtn, DelegateFactory.Action_GameObject(onAddLingQiBtnClicked), false)

	local onOkButtonClicked = function (go)
		self:OnOkButtonClicked(go)
	end
	CommonDefs.AddOnClickListener(self.OkButton, DelegateFactory.Action_GameObject(onOkButtonClicked), false)

	local onCancelButtonClicked = function (go)
		self:OnCancelButtonClicked(go)
	end
	CommonDefs.AddOnClickListener(self.CancelButton, DelegateFactory.Action_GameObject(onCancelButtonClicked), false)

	local onUpperClicked = function (go)
		self:OnUpperClicked(go)
	end
	CommonDefs.AddOnClickListener(self.Upper, DelegateFactory.Action_GameObject(onUpperClicked), false)


	local onLowerClicked = function (go)
		self:OnLowerClicked()
	end
	CommonDefs.AddOnClickListener(self.Lower, DelegateFactory.Action_GameObject(onLowerClicked), false)

	self:InitDivineTexture()
	self:UpdateDivineTexture()
	self:UpdateSelfZhuRuInfo()
	self:UpdateLingQi()
end


function LuaBaGuaLuLingQiZhuRuWnd:UpdateLingQi()
	local myLingQi = 0

	if CClientMainPlayer.Inst then
		myLingQi = CClientMainPlayer.Inst.PlayProp:GetScoreByKey(EnumPlayScoreKey.QianKunDai)
	end
	self.OwnLingQi.text = tostring(myLingQi)

	local setting = QianKunDai_Setting.GetData()
	self.YiZhuRuPercentLabel.text = SafeStringFormat3("%s/%s", tostring(self.CurrentPosTotalZhuRu), tostring(setting.LingqiUpperLimit))

	local maxValue = math.min(myLingQi, setting.PersonalZhuRuUpperLimit)
	if maxValue == 0 then
		maxValue = 1
	end
	self.QnIncreseAndDecreaseButton:SetMinMax(0, maxValue, self.LING_QI_STEP)
	self.QnIncreseAndDecreaseButton.onKeyBoardClosed = DelegateFactory.Action_uint(function (value)
		self:OnNumberKeyBoardClosed(value)
	end)
	self.QnIncreseAndDecreaseButton.onIncAndDecButtonClicked = DelegateFactory.Action_uint(function (value)
		local currentLingQi = 0
		if CClientMainPlayer.Inst then
			currentLingQi = CClientMainPlayer.Inst.PlayProp:GetScoreByKey(EnumPlayScoreKey.QianKunDai)
		end
		if currentLingQi < 10 then
			local msg = MessageMgr.Inst:FormatMessage("BAGUALU_LINGQI_NOT_ENOUGH", {})
			MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function()
				CUIManager.ShowUI(CLuaUIResources.BaGuaLuLingQiGetWnd)
			end),nil,LocalString.GetString("确认"),LocalString.GetString("取消"),false)
			self.QnIncreseAndDecreaseButton:SetValue(0, false)
		end
	end)
end

function LuaBaGuaLuLingQiZhuRuWnd:OnOkButtonClicked(go)
	
	if self.QnIncreseAndDecreaseButton:GetValue() <= 0 then
		MessageMgr.Inst:ShowMessage("BAGUALU_LINGQIZHURU_ZERO", {})
		return
	end

	if self.SelectedZhuRuType == 0 then
		MessageMgr.Inst:ShowMessage("BAGUALU_NO_SELECT_ZHURU_TYPE", {})
		return
	end

	-- 注入的灵力必须是10的倍数
	if self.QnIncreseAndDecreaseButton:GetValue() % 10 ~= 0 then
		MessageMgr.Inst:ShowMessage("BAGUALU_LINGLI_ZHURU_AT_LEAST", {})
		return
	end

	Gac2Gas.RequestBaGuaLuZhuRuLingqi(self.DivineIndex, self.QnIncreseAndDecreaseButton:GetValue(), self.SelectedZhuRuType == 1)
end

function LuaBaGuaLuLingQiZhuRuWnd:OnCancelButtonClicked(go)
	CUIManager.CloseUI(CLuaUIResources.BaGuaLuLingQiZhuRuWnd)
end

function LuaBaGuaLuLingQiZhuRuWnd:OnUpperClicked(go)
	self.SelectedZhuRuType = 1
	self:SelectZhuRuType(self.Upper, true)
	self:SelectZhuRuType(self.Lower, false)
end


function LuaBaGuaLuLingQiZhuRuWnd:OnLowerClicked(go)
	self.SelectedZhuRuType = 2
	self:SelectZhuRuType(self.Upper, false)
	self:SelectZhuRuType(self.Lower, true)
end

function LuaBaGuaLuLingQiZhuRuWnd:SelectZhuRuType(go, isSelected)
	local Selected = go.transform:Find("Texture/Selected").gameObject
	Selected:SetActive(isSelected)
end

-- 初始化卦象的图片
function LuaBaGuaLuLingQiZhuRuWnd:InitDivineTexture()

	local left = self.SelectedDivineItem.transform:Find("BG/Left"):GetComponent(typeof(CUITexture))
	local right = self.SelectedDivineItem.transform:Find("BG/Right"):GetComponent(typeof(CUITexture))

	local divine = QianKunDai_Divine.GetData(self.DivineIndex)
	if not divine then return end
	left:LoadMaterial(divine.DivinePic)
	right:LoadMaterial(divine.DivinePic)

end

function LuaBaGuaLuLingQiZhuRuWnd:UpdateZhuRuInfos()
	self.CurrentPosZhuRuInfo = LuaBaGuaLuMgr.m_CurrentPosZhuRuInfos
	if LuaBaGuaLuMgr.m_CurrentPosTotalZhuRu > self.CurrentPosTotalZhuRu then
		self.CurrentPosTotalZhuRu = LuaBaGuaLuMgr.m_CurrentPosTotalZhuRu
	end

	self:UpdateLingQi()
 
	LuaBaGuaLuMgr.m_CurrentPosZhuRuInfo = nil

	CUICommonDef.ClearTransform(self.ZhuRuGrid.transform)

	-- 玩家的卦
	if self.PlayerZhuRuZhuGua > 0 then
		local zhugua = NGUITools.AddChild(self.ZhuRuGrid.gameObject, self.ZhuRuPrefab)
		self:InitZhuRuItem(zhugua, {
			PlayerId = CClientMainPlayer.Inst.Id,
			PlayerName = CClientMainPlayer.Inst.Name ,
			LingQi = self.PlayerZhuRuZhuGua,
			IsZhuGua = true,
		})
		zhugua:SetActive(true)
	end
	
	if self.PlayerZhuRuKeGua > 0 then
		local kegua = NGUITools.AddChild(self.ZhuRuGrid.gameObject, self.ZhuRuPrefab)
		self:InitZhuRuItem(kegua, {
			PlayerId = CClientMainPlayer.Inst.Id,
			PlayerName = CClientMainPlayer.Inst.Name,
			LingQi = self.PlayerZhuRuKeGua,
			IsZhuGua = false,
		})
		kegua:SetActive(true)
	end
	

	if self.CurrentPosZhuRuInfo then
		for i = 1, #self.CurrentPosZhuRuInfo do
			local go = NGUITools.AddChild(self.ZhuRuGrid.gameObject, self.ZhuRuPrefab)
			self:InitZhuRuItem(go, self.CurrentPosZhuRuInfo[i])
			go:SetActive(true)
		end
	end
	
	self.ZhuRuGrid:Reposition()
	
end

function LuaBaGuaLuLingQiZhuRuWnd:OnNumberKeyBoardClosed(value)
	if value % 10  == 0 then
		return
	end
	local left = value % 10
	value = value - left
	self.QnIncreseAndDecreaseButton:SetValue(value, false)
end

function LuaBaGuaLuLingQiZhuRuWnd:InitZhuRuItem(go, info)
	local NameLabel = go.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
	local LingQiLabel = go.transform:Find("LingQiLabel"):GetComponent(typeof(UILabel))
	NameLabel.text = CChatLinkMgr.ChatPlayerLink.GenerateLink(info.PlayerId, info.PlayerName).displayTag
	local onNameClicked = function (go)
		self:OnNameClicked(go)
	end
	CommonDefs.AddOnClickListener(NameLabel.gameObject, DelegateFactory.Action_GameObject(onNameClicked), false)

	if info.IsZhuGua then
		LingQiLabel.text = SafeStringFormat3(LocalString.GetString("注入主卦[8CC7FF]%s[-]灵力"), tostring(info.LingQi))
	else
		LingQiLabel.text = SafeStringFormat3(LocalString.GetString("注入客卦[8CC7FF]%s[-]灵力"), tostring(info.LingQi))
	end
end

function LuaBaGuaLuLingQiZhuRuWnd:OnNameClicked(go)
	local NameLabel = go:GetComponent(typeof(UILabel))
	local url = NameLabel:GetUrlAtPosition(UICamera.lastWorldPosition)
    if url then
        CChatLinkMgr.ProcessLinkClick(url, nil)
    end
end

-- 根据注入的主客卦信息来显示卦象图片
function LuaBaGuaLuLingQiZhuRuWnd:UpdateDivineTexture()
	local redColor = NGUIText.ParseColor24("FF1212", 0)
	local blueColor = NGUIText.ParseColor24("00B5FF", 0)
	
	local left = self.SelectedDivineItem.transform:Find("BG/Left"):GetComponent(typeof(CUITexture))
	local right = self.SelectedDivineItem.transform:Find("BG/Right"):GetComponent(typeof(CUITexture))

	local isZhuGuaZhuRu = false
	if LuaBaGuaLuMgr.m_ZhuGuaPosList then
		for i = 1, #LuaBaGuaLuMgr.m_ZhuGuaPosList do
			if LuaBaGuaLuMgr.m_ZhuGuaPosList[i] == self.DivineIndex then
				isZhuGuaZhuRu = true
			end
		end
	end
	
	local isKeGuaZhuRu = false
	if LuaBaGuaLuMgr.m_KeGuaPosList then
		for i = 1, #LuaBaGuaLuMgr.m_KeGuaPosList do
			if LuaBaGuaLuMgr.m_KeGuaPosList[i] == self.DivineIndex then
				isKeGuaZhuRu = true
			end
		end
	end

	if isZhuGuaZhuRu and not isKeGuaZhuRu then
		left.color = redColor
		right.color = redColor
	elseif isZhuGuaZhuRu and isKeGuaZhuRu then
		left.color = redColor
		right.color = blueColor
	elseif not isZhuGuaZhuRu and isKeGuaZhuRu then
		left.color = blueColor
		right.color = blueColor
	elseif not isZhuGuaZhuRu and not isKeGuaZhuRu then
		left.color = Color.white
		right.color = Color.white
	end

end

function LuaBaGuaLuLingQiZhuRuWnd:OnAddLingQiBtnClicked(go)
	CUIManager.ShowUI(CLuaUIResources.BaGuaLuLingQiGetWnd)
end

function LuaBaGuaLuLingQiZhuRuWnd:UpdateSelfLingQi(itemId, score)
	self:UpdateLingQi()
end

function LuaBaGuaLuLingQiZhuRuWnd:UpdateSelfZhuRuInfo()
	self.CurrentPosTotalZhuRu = LuaBaGuaLuMgr.m_TotalZhuRu
	self.PlayerZhuRuZhuGua = LuaBaGuaLuMgr.m_PlayerZhuGua
	self.PlayerZhuRuKeGua = LuaBaGuaLuMgr.m_PlayerKeGua
	self:UpdateZhuRuInfos()
end

function LuaBaGuaLuLingQiZhuRuWnd:UpdateBaGuaLuPlayStatus()
	-- 进入新的一局，重新请求
	if LuaBaGuaLuMgr.m_BaGuaLuStatus == EnumBaGuaLuStatus.eZhuRu then
		Gac2Gas.RequestOpenBaGuaLuPos(self.DivineIndex)
	end
end

function LuaBaGuaLuLingQiZhuRuWnd:OnEnable()
	g_ScriptEvent:AddListener("RequestOpenBaGuaLuPosDone", self, "UpdateSelfZhuRuInfo")
	g_ScriptEvent:AddListener("SyncBaGuaLuZhuRuInfo", self, "UpdateZhuRuInfos")
	g_ScriptEvent:AddListener("SyncPlayerTotalZhuRuInfo", self, "UpdateDivineTexture")
	g_ScriptEvent:AddListener("QianKunDaiHeChengSuccess", self, "UpdateSelfLingQi")
	g_ScriptEvent:AddListener("UpdateBaGuaLuPlayStatus", self, "UpdateBaGuaLuPlayStatus")
end

function LuaBaGuaLuLingQiZhuRuWnd:OnDisable()
	g_ScriptEvent:RemoveListener("RequestOpenBaGuaLuPosDone", self, "UpdateSelfZhuRuInfo")
	g_ScriptEvent:RemoveListener("SyncBaGuaLuZhuRuInfo", self, "UpdateZhuRuInfos")
	g_ScriptEvent:RemoveListener("SyncPlayerTotalZhuRuInfo", self, "UpdateDivineTexture")
	g_ScriptEvent:RemoveListener("QianKunDaiHeChengSuccess", self, "UpdateSelfLingQi")
	g_ScriptEvent:RemoveListener("UpdateBaGuaLuPlayStatus", self, "UpdateBaGuaLuPlayStatus")

	if self.UpdateTick ~= nil then
      	UnRegisterTick(self.UpdateTick)
      	self.UpdateTick = nil
    end

    LuaBaGuaLuMgr.m_CurrentPosTotalZhuRu = 0
    LuaBaGuaLuMgr.m_CurrentPosZhuRuInfos = nil
    CUIManager.CloseUI(CUIResources.NumKeyBoardWnd)
    
end


return LuaBaGuaLuLingQiZhuRuWnd