local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local CChatHelper = import "L10.UI.CChatHelper"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"

LuaOppsiteVideoItem = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaOppsiteVideoItem, "pic", "pic", GameObject)
RegistChildComponent(LuaOppsiteVideoItem, "ChannelNameLabel", "ChannelNameLabel", UILabel)
RegistChildComponent(LuaOppsiteVideoItem, "SenderNameLabel", "SenderNameLabel", UILabel)
RegistChildComponent(LuaOppsiteVideoItem, "Portrait", "Portrait", CUITexture)

--@endregion RegistChildComponent end
RegistClassMember(LuaOppsiteVideoItem, "m_Data")
function LuaOppsiteVideoItem:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaOppsiteVideoItem:Init(data)
    self.m_Data = data
    self:InitSenderInfo()
    UIEventListener.Get(self.pic).onClick = DelegateFactory.VoidDelegate(function()
        LuaPersonalSpaceMgrReal.MoviePlayUrl = self.m_Data.text
        CUIManager.ShowUI("DetailMovieShowWnd")
    end)
end
function LuaOppsiteVideoItem:InitSenderInfo()
    self.ChannelNameLabel.text = SafeStringFormat3("[%s]%s[-]",CChatHelper.GetChannelColor24(self.m_Data.channel), self.m_Data.channelName)
    self.SenderNameLabel.text = self.m_Data.senderName;
    self.Portrait:LoadNPCPortrait(self.m_Data.senderPortrait)
end
--@region UIEvent

--@endregion UIEvent

