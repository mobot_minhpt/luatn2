local CUITexture = import "L10.UI.CUITexture"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local CTickMgr = import "L10.Engine.CTickMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local ETickType = import "L10.Engine.ETickType"

LuaShenYaoChallengerSelectionWnd = class()
LuaShenYaoChallengerSelectionWnd.s_ExpiredTime = nil
LuaShenYaoChallengerSelectionWnd.s_NpcId = nil
LuaShenYaoChallengerSelectionWnd.s_Level = 1

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaShenYaoChallengerSelectionWnd, "CountdownLabel", "CountdownLabel", UILabel)
RegistChildComponent(LuaShenYaoChallengerSelectionWnd, "Portrait", "Portrait", CUITexture)
RegistChildComponent(LuaShenYaoChallengerSelectionWnd, "NameLabel", "NameLabel", UILabel)
RegistChildComponent(LuaShenYaoChallengerSelectionWnd, "LevelLabel", "LevelLabel", UILabel)

--@endregion RegistChildComponent end

RegistClassMember(LuaShenYaoChallengerSelectionWnd, "m_countDownTick")

function LuaShenYaoChallengerSelectionWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    if LuaShenYaoChallengerSelectionWnd.s_NpcId then
        local npcData = NPC_NPC.GetData(LuaShenYaoChallengerSelectionWnd.s_NpcId)
        self.Portrait:LoadNPCPortrait(npcData.Portrait)
        self.NameLabel.text = npcData.Name
        self.LevelLabel.text = LuaShenYaoChallengerSelectionWnd.ToHanZi( SafeStringFormat3(LocalString.GetString("%s阶蜃妖"), tostring(LuaShenYaoChallengerSelectionWnd.s_Level)))
    end
end

function LuaShenYaoChallengerSelectionWnd:Init()
    if LuaShenYaoChallengerSelectionWnd.s_ExpiredTime and LuaShenYaoChallengerSelectionWnd.s_NpcId then
        if CServerTimeMgr.Inst.timeStamp < LuaShenYaoChallengerSelectionWnd.s_ExpiredTime then
            local countdown = math.floor(LuaShenYaoChallengerSelectionWnd.s_ExpiredTime - CServerTimeMgr.Inst.timeStamp)
            local maxInterval = XinBaiLianDong_Setting.GetData().ShenYao_MatchInterval
            -- 倒计时限定在[1,5]之间
            countdown = math.min(maxInterval, math.max(1, countdown))
            self.CountdownLabel.text = SafeStringFormat3(LocalString.GetString("(%s秒)"), tostring(countdown))
            self:CancelTick()
            self.m_countDownTick = CTickMgr.Register(DelegateFactory.Action(function()
                if CServerTimeMgr.Inst.timeStamp >= LuaShenYaoChallengerSelectionWnd.s_ExpiredTime then
                    CUIManager.CloseUI(CLuaUIResources.ShenYaoChallengerSelectionWnd)
                else
                    countdown = math.min(maxInterval, math.max(1, countdown - 1))
                    self.CountdownLabel.text = SafeStringFormat3(LocalString.GetString("(%s秒)"), tostring(countdown))
                end
            end), 1000, ETickType.Loop)
        end
    end
end

function LuaShenYaoChallengerSelectionWnd:OnDestroy()
    self:CancelTick()
end

--@region UIEvent

--@endregion UIEvent

function LuaShenYaoChallengerSelectionWnd:CancelTick()
    if self.m_countDownTick ~= nil then
        invoke(self.m_countDownTick)
        self.m_countDownTick = nil
    end
end

function LuaShenYaoChallengerSelectionWnd.ToHanZi(szNum)
    local iLen = 0
    local str = ''
    local hzUnit = {LocalString.GetString("十")}
    local hzNum = {LocalString.GetString("一"), LocalString.GetString("二"), LocalString.GetString("三"), LocalString.GetString("四"), LocalString.GetString("五"), LocalString.GetString("六"), LocalString.GetString("七"), LocalString.GetString("八"), LocalString.GetString("九")}
    local hzZero = {LocalString.GetString("零")}
    local iNum, iNum2
    if nil == tonumber(szNum) then
        return tostring(szNum)
    end
    iLen = string.len(szNum)
    
    --0~9
    if iLen == 1 then
        if szNum == 0 then
            str = hzZero[1]
        elseif szNum > 0 then
            str = hzNum[szNum]
        end
    end
    
    --10~99
    if iLen == 2 then
        iNum = tonumber(string.sub(szNum,1,1))
        iNum2 = tonumber(string.sub(szNum,2,2))
        if iNum == 1 then
            if iNum2 == 0 then
                str = hzUnit[1]
            elseif iNum2 > 0 then
                str = hzUnit[1]..hzNum[iNum2]
            end
        elseif iNum >= 2 and  iNum <= 9 then
            if iNum2 == 0 then
                str = hzNum[iNum]..hzUnit[1]
            elseif iNum2 > 0 then
                str = hzNum[iNum]..hzUnit[1]..hzNum[iNum2]
            end
        end
    end

    return str
end