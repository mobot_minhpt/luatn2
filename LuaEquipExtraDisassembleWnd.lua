local CBaseEquipmentItem = import "L10.UI.CBaseEquipmentItem"
local Extensions = import "Extensions"
local CPackageItemCell = import "L10.UI.CPackageItemCell"
local CItemMgr=import "L10.Game.CItemMgr"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local UITable = import "UITable"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType2 = import "L10.UI.CItemInfoMgr+AlignType"
local CItemMgr = import "L10.Game.CItemMgr"

LuaEquipExtraDisassembleWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaEquipExtraDisassembleWnd, "NameLabel", "NameLabel", UILabel)
RegistChildComponent(LuaEquipExtraDisassembleWnd, "XiLianLabel", "XiLianLabel", UILabel)
RegistChildComponent(LuaEquipExtraDisassembleWnd, "CommitBtn", "CommitBtn", GameObject)
RegistChildComponent(LuaEquipExtraDisassembleWnd, "Cost", "Cost", CCurentMoneyCtrl)
RegistChildComponent(LuaEquipExtraDisassembleWnd, "ItemTemplate", "ItemTemplate", GameObject)
RegistChildComponent(LuaEquipExtraDisassembleWnd, "Table", "Table", UITable)
RegistChildComponent(LuaEquipExtraDisassembleWnd, "MainEquip", "MainEquip", CBaseEquipmentItem)
RegistChildComponent(LuaEquipExtraDisassembleWnd, "TipBtn", "TipBtn", GameObject)

--@endregion RegistChildComponent end

function LuaEquipExtraDisassembleWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.CommitBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCommitBtnClick()
	end)


	
	UIEventListener.Get(self.TipBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipBtnClick()
	end)


    --@endregion EventBind end
end

function LuaEquipExtraDisassembleWnd:Init()
    local data = CLuaEquipMgr.m_DisassembleData

    -- 设置得到物品
    local itemList = data.itemList
    Extensions.RemoveAllChildren(self.Table.transform)
    
    self.ItemTemplate:SetActive(false)

    -- 分解的装备
    local equipId = data.equipId
    self.MainEquip:UpdateData(equipId)
    local item = CItemMgr.Inst:GetById(equipId)

    -- 银票是2 银两是1
    if item.IsBinded then
        -- 绑定是银票
        self.Cost:SetType(2,0,true)
    else
        -- 非绑定是银两
        self.Cost:SetType(1,0,true)
    end

    for i=1, #itemList, 3 do
        local itemId = itemList[i]
        local count = itemList[i+1]
        local isbind = itemList[i+2]

        local addItem = true
        -- 处理一大袋净瓶
        if itemId == 21001245 then
            -- 如果没有净瓶
            local hasJingPing = false

            for j = 1, #itemList, 3 do
                if itemList[j] == 21000041 then
                    hasJingPing = true
                end
            end

            if not hasJingPing then
                itemId = 21000041
            else
                addItem = false
            end
        end

        if addItem then
            local g = NGUITools.AddChild(self.Table.gameObject, self.ItemTemplate)
            g:SetActive(true)
            
            local icon = g.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
            local numLabel = g.transform:Find("NumLabel"):GetComponent(typeof(UILabel))
            local bindSprite = g.transform:Find("BindSprite").gameObject

            local itemData = Item_Item.GetData(itemId)
            icon:LoadMaterial(itemData.Icon)

            UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function (go)
                CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType2.Default, 0, 0, 0, 0)
            end)
            
            -- 处理净瓶
            if itemId == 21000041 then
                numLabel.text = ""
                bindSprite:SetActive(true)
            else
                numLabel.text = tostring(count)
                bindSprite:SetActive(isbind == 1)
            end
        end
    end
    self.Table:Reposition()

    -- 设置消耗
    local moneyCost = data.cost
    self.Cost:SetCost(moneyCost)

    if item.IsEquip then
        self.NameLabel.text = item.ColoredName
        self.XiLianLabel.text = tostring(item.Equip.BaptizeScore)
    end
end

--@region UIEvent

function LuaEquipExtraDisassembleWnd:OnCommitBtnClick()
    local itemId = CLuaEquipMgr.m_DisassembleData.equipId
    local equipItem = CItemMgr.Inst:GetById(itemId)

    local msg = g_MessageMgr:FormatMessage("Extra_Equip_Disassemble_Confirm", equipItem.Equip.BaptizeScore, equipItem.ColoredName)

    g_MessageMgr:ShowOkCancelMessage(msg, function()
        local itemId = CLuaEquipMgr.m_DisassembleData.equipId
        local place, pos = CLuaEquipMgr:GetItemPlaceAndPos(itemId)
        Gac2Gas.RequestDisassembleExtraEquip(place, pos, itemId)
        CUIManager.CloseUI(CLuaUIResources.EquipExtraDisassembleWnd)
    end, nil, nil, nil, false)
end


function LuaEquipExtraDisassembleWnd:OnTipBtnClick()
    g_MessageMgr:ShowMessage("Equip_Disassemble_Desc")
end


--@endregion UIEvent

