require("3rdParty/ScriptEvent")
require("common/common_include")

local UIScrollView = import "UIScrollView"
local UITable = import "UITable"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CItemMgr = import "L10.Game.CItemMgr"
local NGUITools = import "NGUITools"
local UIGrid = import "UIGrid"
local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"
local QualityColor = import "L10.Game.QualityColor"
local NGUIText = import "NGUIText"
local CButton = import "L10.UI.CButton"
local CPlayerShopMgr = import "L10.UI.CPlayerShopMgr"
local EnumQualityType = import "L10.Game.EnumQualityType"
local CYuanbaoMarketMgr = import "L10.UI.CYuanbaoMarketMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local DefaultItemActionDataSource=import "L10.UI.DefaultItemActionDataSource"
local CUITexture = import "L10.UI.CUITexture"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"

LuaShenBingExchangeQyBuyWnd=class()

RegistClassMember(LuaShenBingExchangeQyBuyWnd,"ContentScrollView")
RegistClassMember(LuaShenBingExchangeQyBuyWnd,"ContentTable")

RegistClassMember(LuaShenBingExchangeQyBuyWnd,"AvailableTemplate")
RegistClassMember(LuaShenBingExchangeQyBuyWnd,"UnavailableTemplate")

RegistClassMember(LuaShenBingExchangeQyBuyWnd,"EquipItemsInBag") -- 所有包裹内已有的equipItem, 用于处理选中效果

function LuaShenBingExchangeQyBuyWnd:Init()
	if not LuaShenbingMgr.QianYuanBuyInfo or not LuaShenbingMgr.QianYuanBuyInfo.Consumes then
		CUIManager.CloseUI(CLuaUIResources.ShenBingExchangeQyBuyWnd)
		return
	end
	
	self:InitClassMembers()
	self:InitInfos()
	
end

-- 初始化所有成员变量
function LuaShenBingExchangeQyBuyWnd:InitClassMembers()
	self.ContentScrollView = self.transform:Find("Anchor/bg/ContentScrollView"):GetComponent(typeof(UIScrollView))
	self.ContentTable = self.transform:Find("Anchor/bg/ContentScrollView/ContentTable"):GetComponent(typeof(UITable))

	self.AvailableTemplate = self.transform:Find("Anchor/bg/Templates/AvailableTemplate").gameObject
	self.UnavailableTemplate = self.transform:Find("Anchor/bg/Templates/UnavailableTemplate").gameObject

	self.AvailableTemplate:SetActive(false)
	self.UnavailableTemplate:SetActive(false)
end

-- 数据初始化，并根据数据创建界面对象
function LuaShenBingExchangeQyBuyWnd:InitInfos()

	self.EquipItemsInBag = {}
	-- 删除所有子节点
	CUICommonDef.ClearTransform(self.ContentTable.transform)

	-- 根据传入的公式情况，创建子节点
	local consumes = LuaShenbingMgr.QianYuanBuyInfo.Consumes
	for i = 1, #consumes do
		local consume = consumes[i]
		local equips = self:GetEquipsInBag(consume.templateId, consume.isFake)
		if #equips < 1 then
			local unavailable = NGUITools.AddChild(self.ContentTable.gameObject, self.UnavailableTemplate)
			unavailable:SetActive(true)
			self:InitUnavailableTemplate(unavailable, consume.templateId, consume.isFake)
		else
			local available = NGUITools.AddChild(self.ContentTable.gameObject, self.AvailableTemplate)
			available:SetActive(true)
			local selecedItemId = consume.itemToExchange
			self:InitAvailableTemplate(available, equips, consume.templateId, consume.isFake, selecedItemId)
		end
	end
	self.ContentTable:Reposition()
	self.ContentScrollView:UpdatePosition()
	self.ContentScrollView:Scroll(0.01)
end

-- 获得包裹内符合条件的装备table of itemId
function LuaShenBingExchangeQyBuyWnd:GetEquipsInBag(templatedId, isFake)
	local equipsInBag = {}

	local itemProp = CClientMainPlayer.Inst.ItemProp
    local bagSize = itemProp:GetPlaceSize(EnumItemPlace.Bag)
    for i = 1, bagSize, 1 do
		local id = itemProp:GetItemAt(EnumItemPlace.Bag, i)
		if id and id ~= "" then
			local item = CItemMgr.Inst:GetById(id)
			-- 是装备
			if item and item.IsEquip then
				if item.TemplateId == templatedId and item.Equip.IsFake == isFake then
					-- 有强化或升了宝石的装备不允许兑换前缘材料
					if item.Equip.IntensifyLevel <= 0 and not item.Equip.HasGems then
						table.insert(equipsInBag, id)
					end
				end
			end
		end
	end
	return equipsInBag
end

-- 初始化Available item
function LuaShenBingExchangeQyBuyWnd:InitAvailableTemplate(gameObject, itemIdList, templateId, isFake, selecedItemId)
	
	local tf = gameObject.transform
	local equipNameLabel = tf:Find("EquipNameLabel"):GetComponent(typeof(UILabel))

	local template = EquipmentTemplate_Equip.GetData(templateId)
	if not template then return end

	local color = QualityColor.GetRGBValue(CommonDefs.ConvertIntToEnum(typeof(EnumQualityType), template.Color))
	local colorText = NGUIText.EncodeColor24(color)

	local name = template.Name
	if isFake then 
		name = template.AliasName
	end
	local title = SafeStringFormat3("[%s]%s[-]", colorText, name)
	equipNameLabel.text = title

	local equipTable = tf:Find("EquipInBags/Table"):GetComponent(typeof(UIGrid))
	CUICommonDef.ClearTransform(equipTable.transform)

	local equipTemplate = tf:Find("EquipInBags/EquipTemplate").gameObject
	equipTemplate:SetActive(false)

	for i = 1, #itemIdList do
		local equipItem = NGUITools.AddChild(equipTable.gameObject, equipTemplate)
		equipItem:SetActive(true)
		table.insert(self.EquipItemsInBag, equipItem)
		self:InitEquipInBag(equipItem, templateId, isFake, itemIdList[i], selecedItemId)
	end

	local onBuyEquip = function ()
		local toBuyTId = templateId
		if isFake then
			toBuyTId = CPlayerShopMgr.Inst:ToAliasTemplateId(templateId)
		end
		CYuanbaoMarketMgr.ShowPlayerShop(toBuyTId)
	end

	local buyBtn = tf:Find("BuyBtn"):GetComponent(typeof(CButton))
	CommonDefs.AddOnClickListener(buyBtn.gameObject,DelegateFactory.Action_GameObject(onBuyEquip),false)

end

-- 初始化包裹内已有的装备
function LuaShenBingExchangeQyBuyWnd:InitEquipInBag(go, templateId, isFake, itemId, selecedItemId)
	local tf = go.transform
	local iconTexture = tf:Find("EquipTexture"):GetComponent(typeof(CUITexture))
	local qualitySprite = tf:Find("QualitySprite"):GetComponent(typeof(UISprite))
	local bindSprite = tf:Find("BindSprite"):GetComponent(typeof(UISprite))

	iconTexture:Clear()
	qualitySprite.spriteName = CUICommonDef.GetItemCellBorder()

	local selectedToExchange = tf:Find("SelectedToExchangeSprite").gameObject
	local selectedSprite = tf:Find("SelectedSprite").gameObject

	selectedToExchange:SetActive(itemId == selecedItemId)
	selectedSprite:SetActive(false)

	local template = EquipmentTemplate_Equip.GetData(templateId)
	if not template then return end
	local item = CItemMgr.Inst:GetById(itemId)
	if not item then return end

	iconTexture:LoadMaterial(template.Icon)
	qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(item.Equip.QualityType)
	bindSprite.spriteName = item.BindOrEquipCornerMark

	-- 选择替换装备
	local onSelectEquip = function (go)
		self:OnSelectEquip(go, templateId, isFake, itemId)
	end

	CommonDefs.AddOnClickListener(go,DelegateFactory.Action_GameObject(onSelectEquip),false)
end

-- 选中装备, 可进行装备的替换
function LuaShenBingExchangeQyBuyWnd:OnSelectEquip(go, templateId, isFake, itemId)
	for i = 1, #self.EquipItemsInBag do
		local tf = self.EquipItemsInBag[i].transform
		local selectedSprite = tf:Find("SelectedSprite").gameObject
		selectedSprite:SetActive(go == self.EquipItemsInBag[i])
	end

	local t_name={}
	t_name[1]=LocalString.GetString("选择") 
	local t_action={}
	t_action[1] = function ()
		for i = 1, #LuaShenbingMgr.QianYuanBuyInfo.Consumes do
			local consume = LuaShenbingMgr.QianYuanBuyInfo.Consumes[i]
			if consume.templateId == templateId then
				LuaShenbingMgr.QianYuanBuyInfo.Consumes[i].itemToExchange = itemId
			end
		end
		self:InitInfos()
		g_ScriptEvent:BroadcastInLua("ShenBingExchangeQySelect", itemId, templateId, LuaShenbingMgr.QianYuanFormulaIndex)
		CItemInfoMgr.CloseItemInfoWnd()
	end
	local actionSource=DefaultItemActionDataSource.Create(1,t_action,t_name)
    CItemInfoMgr.ShowLinkItemInfo(itemId, false, actionSource, AlignType.ScreenRight,0, 0, 0, 0)
end


-- 初始化Unavailable item
function LuaShenBingExchangeQyBuyWnd:InitUnavailableTemplate(gameObject, templateId, isFake)
	local tf = gameObject.transform
	local equipNameLabel = tf:Find("EquipNameLabel"):GetComponent(typeof(UILabel))

	local template = EquipmentTemplate_Equip.GetData(templateId)
	if not template then return end

	local color = QualityColor.GetRGBValue(CommonDefs.ConvertIntToEnum(typeof(EnumQualityType), template.Color))
	local colorText = NGUIText.EncodeColor24(color)

	local name = template.Name
	if isFake then 
		name = template.AliasName
	end
	local title = SafeStringFormat3("[%s]%s[-]", colorText, name)
	equipNameLabel.text = title

	local onBuyEquip = function ()
		local toBuyTId = templateId
		if isFake then
			toBuyTId = CPlayerShopMgr.Inst:ToAliasTemplateId(templateId)
		end
		CYuanbaoMarketMgr.ShowPlayerShop(toBuyTId)
	end

	local buyBtn = tf:Find("BuyBtn"):GetComponent(typeof(CButton))
	CommonDefs.AddOnClickListener(buyBtn.gameObject,DelegateFactory.Action_GameObject(onBuyEquip),false)
end

function LuaShenBingExchangeQyBuyWnd:OnEnable()
	g_ScriptEvent:AddListener("SendItem", self, "SendItem")
	g_ScriptEvent:AddListener("SetItemAt", self, "SetItemAt")
end

function LuaShenBingExchangeQyBuyWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SendItem", self, "SendItem")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "SetItemAt")
end

function LuaShenBingExchangeQyBuyWnd:SendItem(args)
	self:InitInfos()
end

function LuaShenBingExchangeQyBuyWnd:SetItemAt(place, position, oldItemId, newItemId)
	self:InitInfos()
end

return LuaShenBingExchangeQyBuyWnd
