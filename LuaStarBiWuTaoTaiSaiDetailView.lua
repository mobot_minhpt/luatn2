local GameObject = import "UnityEngine.GameObject"
local CButton = import "L10.UI.CButton"
local QnButton = import "L10.UI.QnButton"
local UILabel = import "UILabel"
local QnTipButton = import "L10.UI.QnTipButton"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CPlayerShopPopupMenuInfoMgr = import "L10.UI.CPlayerShopPopupMenuInfoMgr"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local CPopupMenuInfoMgrAlignType = import  "L10.UI.CPopupMenuInfoMgr+AlignType"
local CScheduleAlertCtl=import "L10.UI.CScheduleAlertCtl"

LuaStarBiWuTaoTaiSaiDetailView = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaStarBiWuTaoTaiSaiDetailView, "Title", "Title", GameObject)
RegistChildComponent(LuaStarBiWuTaoTaiSaiDetailView, "WatchBtn", "WatchBtn", GameObject)
RegistChildComponent(LuaStarBiWuTaoTaiSaiDetailView, "RefreshBtn", "RefreshBtn", GameObject)
RegistChildComponent(LuaStarBiWuTaoTaiSaiDetailView, "TipButton", "TipButton", GameObject)
RegistChildComponent(LuaStarBiWuTaoTaiSaiDetailView, "Label", "Label", UILabel)
RegistChildComponent(LuaStarBiWuTaoTaiSaiDetailView, "JingCaiButton", "JingCaiButton", GameObject)
RegistChildComponent(LuaStarBiWuTaoTaiSaiDetailView, "Status", "Status", GameObject)
RegistChildComponent(LuaStarBiWuTaoTaiSaiDetailView, "WordFilterButton", "WordFilterButton", QnTipButton)
RegistChildComponent(LuaStarBiWuTaoTaiSaiDetailView, "Empty", "Empty", UILabel)

--@endregion RegistChildComponent end

RegistClassMember(LuaStarBiWuTaoTaiSaiDetailView, "m_Buttons")

RegistClassMember(LuaStarBiWuTaoTaiSaiDetailView, "m_CurrentMatchIndex")
RegistClassMember(LuaStarBiWuTaoTaiSaiDetailView, "m_SelfZhanduiId")
RegistClassMember(LuaStarBiWuTaoTaiSaiDetailView, "m_Name1LabelTable")
RegistClassMember(LuaStarBiWuTaoTaiSaiDetailView, "m_Name2LabelTable")
RegistClassMember(LuaStarBiWuTaoTaiSaiDetailView, "m_Score1LabelTable")
RegistClassMember(LuaStarBiWuTaoTaiSaiDetailView, "m_Score2LabelTable")
RegistClassMember(LuaStarBiWuTaoTaiSaiDetailView, "m_MatchingFxTable")
RegistClassMember(LuaStarBiWuTaoTaiSaiDetailView, "m_InitMatchIndex")

RegistClassMember(LuaStarBiWuTaoTaiSaiDetailView, "m_MatchIndexTable")
RegistClassMember(LuaStarBiWuTaoTaiSaiDetailView, "m_PreMatchIsWinTable")

RegistClassMember(LuaStarBiWuTaoTaiSaiDetailView, "m_RedColor")
RegistClassMember(LuaStarBiWuTaoTaiSaiDetailView, "m_BlueColor")
RegistClassMember(LuaStarBiWuTaoTaiSaiDetailView, "m_CurrentDateColor")
RegistClassMember(LuaStarBiWuTaoTaiSaiDetailView, "m_CurSelectGroup")
RegistClassMember(LuaStarBiWuTaoTaiSaiDetailView, "m_MainPlayerCurGroup")
RegistClassMember(LuaStarBiWuTaoTaiSaiDetailView, "m_GroupIndex2Name")
RegistClassMember(LuaStarBiWuTaoTaiSaiDetailView, "m_FilterActions")
RegistClassMember(LuaStarBiWuTaoTaiSaiDetailView, "m_IsInit")
RegistClassMember(LuaStarBiWuTaoTaiSaiDetailView, "m_TimeTable")
function LuaStarBiWuTaoTaiSaiDetailView:Awake()
    --@region EventBind: Dont Modify Manually!
	UIEventListener.Get(self.WatchBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnWatchBtnClick()
	end)

    UIEventListener.Get(self.RefreshBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRefreshBtnClick()
	end)

    UIEventListener.Get(self.JingCaiButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnJingCaiButtonClick()
	end)

    UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)

    --@endregion EventBind end
	self.Label.text = g_MessageMgr:FormatMessage("StarBiWu_TaoTaiSai_Rule_Title")

    local alertCtrl = self.JingCaiButton.gameObject:GetComponent(typeof(CScheduleAlertCtl))
    alertCtrl.tabType = 0
	alertCtrl.scheduleId = StarBiWuShow_Setting.GetData().MatchServerSchedule

	self.m_CurSelectGroup = -1	-- 当前所选组别
	self.m_GroupIndex2Name = nil
	self.m_FilterActions = nil
	self.m_MainPlayerCurGroup = 0
    self.m_IsInit = false
	self.m_Buttons = self.transform:Find("Buttons")
	self.WordFilterButton.gameObject:SetActive(false)
	self.JingCaiButton.gameObject:SetActive(false)
    self.m_Buttons.gameObject:SetActive(false)
    self.Empty.gameObject:SetActive(false)
    self.transform:Find("Title").gameObject:SetActive(false)
    self.Status.gameObject:SetActive(false)
	self:InitView()
end

function LuaStarBiWuTaoTaiSaiDetailView:OnGetGroupInfo()
	-- 初始化组别信息
	self.m_GroupIndex2Name = {}
	local totalNum = CLuaStarBiwuMgr.m_GroupNum
    local groupList = StarBiWuShow_Setting.GetData().NewGroupName
    self.m_GroupIndex2Name[1] = SafeStringFormat3(LocalString.GetString("[d293e9]%s[-]"),groupList[0])

	self.m_CurSelectGroup = 1
	self.m_FilterActions = {}
	for i = 1, totalNum do
		table.insert(self.m_FilterActions,PopupMenuItemData(self.m_GroupIndex2Name[i], DelegateFactory.Action_int(function (index) 
			-- 请求指定组积分赛信息
			Gac2Gas.QueryStarBiwuTaotaisaiMatchS13(i)
		end),false, self.m_MainPlayerCurGroup == i and "common_battletype_personal" or nil))
	end
	-- UIEventListener.Get(self.WordFilterButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	--     self:OnWordFilterButtonClick()
	-- end)
	-- 请求当前所在组的信息
	Gac2Gas.QueryStarBiwuTaotaisaiMatchS13(self.m_CurSelectGroup)
end

function LuaStarBiWuTaoTaiSaiDetailView:InitView()
	self.m_RedColor = NGUIText.ParseColor24("ec7676", 0)
    self.m_BlueColor = NGUIText.ParseColor24("4a8eff", 0)
    self.m_CurrentDateColor = NGUIText.ParseColor24("fff68f", 0)
    local timeList = StarBiWuShow_Setting.GetData().TaoTaiSaiStartTimeShow
	self.m_CurrentMatchIndex = 0
    self.m_SelfZhanduiId = 0
    self.m_Name1LabelTable = {}
    self.m_Name2LabelTable = {}
    self.m_Score2LabelTable =  {}
    self.m_Score1LabelTable = {}
    self.m_MatchingFxTable = {}
    self.m_InitMatchIndex = {}
    self.m_TimeTable = {}
	for i = 1, 14 do
        local rootTrans = self.transform:Find("Status/"..i)
        local name1Label = rootTrans:Find("Name1"):GetComponent(typeof(UILabel))
        name1Label.text = ""
        local name2Label = rootTrans:Find("Name2"):GetComponent(typeof(UILabel))
        name2Label.text = ""
        local score1Label = rootTrans:Find("Score1"):GetComponent(typeof(UILabel))
        score1Label.text = ""
        local score2Label = rootTrans:Find("Score2"):GetComponent(typeof(UILabel))
        score2Label.text = ""
        local fx = rootTrans:Find("Fx"):GetComponent(typeof(CUIFx))
        local time = rootTrans:Find("Time").gameObject
        time:SetActive(true)
        local timeLabel = time.transform:Find("TimeLable"):GetComponent(typeof(UILabel))
        timeLabel.text = tostring(timeList[i - 1])
        table.insert(self.m_Name1LabelTable, name1Label)
        table.insert(self.m_Name2LabelTable, name2Label)
        table.insert(self.m_Score1LabelTable, score1Label)
        table.insert(self.m_Score2LabelTable, score2Label)
        table.insert(self.m_MatchingFxTable, fx)
        table.insert(self.m_TimeTable, time)
    end
    -- from 5 to 14
    self.m_MatchIndexTable =    {1, 2, 3, 4, 1, 2, 3, 4, 6, 7, 5, 8, 7, 8, 9, 10, 11, 12, 11, 13}
    self.m_PreMatchIsWinTable = {0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1}
    self.m_InitMatchIndex = {1,8,4,5,2,7,3,6}
end

function LuaStarBiWuTaoTaiSaiDetailView:UpdateTime()
    self.transform:Find("Title").gameObject:SetActive(true)
	local zone8 = CServerTimeMgr.Inst:GetZone8Time()
    local month, day = zone8.Month, zone8.Day
    local finalTime = StarBiWuShow_Setting.GetData().FinalStageTime
	if self.m_CurSelectGroup > 1 then
		finalTime = StarBiWuShow_Setting.GetData().FinalStageTimeCommonGroup
	end
    for i = 1, 3 do
        local isCurrentDay = month == finalTime[(i - 1) * 2] and day == finalTime[(i - 1) * 2 + 1]
        local root = self.transform:Find("Title/"..i)
        root:GetComponent(typeof(UISprite)).spriteName = isCurrentDay and "common_btn_03_highlight" or "common_btn_03_normal"
        local label = root:Find("Label"):GetComponent(typeof(UILabel))
        label.text = SafeStringFormat3(LocalString.GetString("%d月%d日%s"), finalTime[(i - 1) * 2], finalTime[(i - 1) * 2 + 1], isCurrentDay and LocalString.GetString("-进行中") or "")
        label.color = isCurrentDay and self.m_CurrentDateColor or Color.white
    end
    if self.m_GroupIndex2Name and self.m_GroupIndex2Name[self.m_CurSelectGroup] then
		g_ScriptEvent:BroadcastInLua("OnStarBiWuDetailInfoWndChange",SafeStringFormat3(LocalString.GetString("%s淘汰赛"),self.m_GroupIndex2Name[1]))
	end
end

function LuaStarBiWuTaoTaiSaiDetailView:ReplyStarBiwuZongJueSaiMatch(currentMatchPlayIdx, selfZhanduiId, matchData, groupId)
    -- 当前正在比赛的playIdx,这个值的范围目前对应的是 0-14,也是观战请求 RequestWatchStarBiwuZongJueSai 的参数
    -- 0表示当前没有正在进行的比赛
	-- groupId 当前查看的组别信息
    self.m_CurrentMatchIndex = currentMatchPlayIdx

    -- 自己的战队id,0表示自己不处于战队中
    self.m_SelfZhanduiId = selfZhanduiId
    self.m_CurSelectGroup = groupId
	self:UpdateWordFilterButton()
	self:UpdateTime()
    local matchDataList = matchData
	if #matchDataList <= 0 then
		--if self.m_CurSelectGroup == 0 then
			--self.Empty.text = g_MessageMgr:FormatMessage("Star_BiWu_TaoTaiSai_Group_Empty_Top")	-- 巅峰组
		-- else
		-- 	self.Empty.text = g_MessageMgr:FormatMessage("Star_BiWu_TaiTaiSai_Group_Empty_Common") -- 普通组
		--end
		self.Empty.gameObject:SetActive(false)
		self.JingCaiButton.gameObject:SetActive(false)
		self.Status.gameObject:SetActive(true)
		self.m_Buttons.gameObject:SetActive(false)
        for i = 1,14 do
            self:RefreshZhandui(i, 0, "", 0, 0, "", 0,0,0)
        end
		return
	else
		self.Empty.gameObject:SetActive(false)
		self.Status.gameObject:SetActive(true)
		self.m_Buttons.gameObject:SetActive(true)
		self.JingCaiButton.gameObject:SetActive(true)
	end
	-- 对阵信息
    local index = 1
    for i = 1, #matchDataList do
      local attackerZId   = matchDataList[i].attackId    -- 战队id
      local attackerZName = matchDataList[i].attackName  -- 战队名
      local attackerPoint = matchDataList[i].attackScore  -- 战队得分,attackerPoint 和 defenderPoint都是0 就不显示

      local defenderZId   = matchDataList[i].defendId  -- 战队id
      local defenderZName = matchDataList[i].defendName  -- 战队名
      local defenderPoint = matchDataList[i].defendScore  -- 战队得分,attackerPoint 和 defenderPoint都是0 就不显示
      local winnerId = matchDataList[i].winnerId
      local status = matchDataList[i].status

      self:RefreshZhandui(index, attackerZId, attackerZName, attackerPoint, defenderZId, defenderZName, defenderPoint,winnerId,status)
      index = index + 1
    end
end

function LuaStarBiWuTaoTaiSaiDetailView:UpdateWordFilterButton()
	-- if not self.m_GroupIndex2Name or not self.m_GroupIndex2Name[self.m_CurSelectGroup] then return end
	-- self.WordFilterButton.gameObject:SetActive(true)
	-- local btnlabel = self.WordFilterButton.transform:Find("Label"):GetComponent(typeof(UILabel))
	-- btnlabel.text = self.m_GroupIndex2Name and self.m_GroupIndex2Name[self.m_CurSelectGroup] or ""
end

function LuaStarBiWuTaoTaiSaiDetailView:RefreshZhandui(index, id1, name1, score1, id2, name2, score2,winner,status)
    local hasAlpha1 = false
    local hasAlpha2 = false
    if index >= 5 then
        local iindex = (index - 5) * 2 + 1
        if not name1 or name1 == "" then
            name1 = SafeStringFormat3(LocalString.GetString("第%s场%s者"), self.m_MatchIndexTable[iindex], self.m_PreMatchIsWinTable[iindex] == 1 and LocalString.GetString("胜") or LocalString.GetString("败"))
            hasAlpha1 = true
        end

        if not name2 or name2 == "" then
            name2 = SafeStringFormat3(LocalString.GetString("第%s场%s者"), self.m_MatchIndexTable[iindex + 1], self.m_PreMatchIsWinTable[iindex + 1] == 1 and LocalString.GetString("胜") or LocalString.GetString("败"))
            hasAlpha2 = true
        end
    else
        if not name1 or name1 == "" then
            name1 = SafeStringFormat3(LocalString.GetString("积分赛第%s名"), self.m_InitMatchIndex[index * 2 - 1])
            hasAlpha1 = true
        end
        if not name2 or name2 == "" then
            name2 = SafeStringFormat3(LocalString.GetString("积分赛第%s名"), self.m_InitMatchIndex[index * 2])
            hasAlpha2 = true
        end
    end

    if index == self.m_CurrentMatchIndex then
        self.m_TimeTable[index].gameObject:SetActive(false)
        self.m_MatchingFxTable[index]:LoadFx("fx/ui/prefab/UI_duizhanzhuangtai.prefab")
        self.m_Score1LabelTable[index].text = ""
        self.m_Score2LabelTable[index].text = ""
    else
        self.m_MatchingFxTable[index]:DestroyFx()
        if status ~= 2 then
            self.m_TimeTable[index].gameObject:SetActive(true)
            self.m_Score1LabelTable[index].text = ""
            self.m_Score2LabelTable[index].text = ""
        else
            self.m_TimeTable[index].gameObject:SetActive(false)
            self.m_Score1LabelTable[index].text = score1
            self.m_Score2LabelTable[index].text = score2
            self.m_Score1LabelTable[index].color = winner == id1 and self.m_BlueColor or self.m_RedColor
            self.m_Score2LabelTable[index].color = winner == id2 and self.m_BlueColor or self.m_RedColor
            if winner == id1 then
                hasAlpha2 = true
            else
                hasAlpha1 = true
            end
        end
    end

    self.m_Name1LabelTable[index].text = name1
    self.m_Name1LabelTable[index].color = (id1 == self.m_SelfZhanduiId and self.m_SelfZhanduiId ~= 0) and Color.green or Color.white
    self.m_Name1LabelTable[index].alpha = hasAlpha1 and 0.3 or 1
    self.m_Name2LabelTable[index].text = name2
    self.m_Name2LabelTable[index].color = (id2 == self.m_SelfZhanduiId and self.m_SelfZhanduiId ~= 0) and Color.green or Color.white
    self.m_Name2LabelTable[index].alpha = hasAlpha2 and 0.3 or 1

    UIEventListener.Get(self.m_Name1LabelTable[index].gameObject).onClick = DelegateFactory.VoidDelegate(function(go) 
        CLuaStarBiwuMgr:ShowZhanDuiMemberWnd(0, id1, 1)
    end)
    UIEventListener.Get(self.m_Name2LabelTable[index].gameObject).onClick = DelegateFactory.VoidDelegate(function(go) 
        CLuaStarBiwuMgr:ShowZhanDuiMemberWnd(0, id2, 1)
    end)
end
--@region UIEvent

function LuaStarBiWuTaoTaiSaiDetailView:OnWatchBtnClick()
	Gac2Gas.RequestWatchStarBiwuZongJueSai(self.m_CurrentMatchIndex)
end

function LuaStarBiWuTaoTaiSaiDetailView:OnRefreshBtnClick()
	Gac2Gas.QueryStarBiwuTaotaisaiMatchS13(self.m_CurSelectGroup)
end

function LuaStarBiWuTaoTaiSaiDetailView:OnTipButtonClick()
	g_MessageMgr:ShowMessage("StarBiWu_TaoTaiSai_Rule_Tip")
end

function LuaStarBiWuTaoTaiSaiDetailView:OnJingCaiButtonClick()
    CLuaScheduleMgr.UpdateAlertStatus(StarBiWuShow_Setting.GetData().MatchServerSchedule,LuaEnumAlertState.Clicked)
	CUIManager.ShowUI(CLuaUIResources.StarBiwuJingCaiWnd)
end

-- function LuaStarBiWuTaoTaiSaiDetailView:OnWordFilterButtonClick()
-- 	self.WordFilterButton:SetTipStatus(false)
-- 	CPlayerShopPopupMenuInfoMgr.ShowPlayerShopPopupMenuWithdefaultSelectedIndex(Table2Array(self.m_FilterActions, MakeArrayClass(PopupMenuItemData)), self.m_CurSelectGroup, self.WordFilterButton.transform, CPopupMenuInfoMgrAlignType.Bottom, 1, nil, nil, DelegateFactory.Action(function ()
--         self.WordFilterButton:SetTipStatus(true)
--     end), 600,300)
-- end


function LuaStarBiWuTaoTaiSaiDetailView:OnEnable()
	-- 服务器返回数据监听
    g_ScriptEvent:AddListener("OnSyncStarBiwuTaotaiSaiGroupS13", self, "OnGetGroupInfo")
	g_ScriptEvent:AddListener("OnStarBiWuTaoTaiSaiResultData", self, "ReplyStarBiwuZongJueSaiMatch")

    if not self.m_GroupIndex2Name then
		Gac2Gas.QueryStarBiwuTaotaisaiGroupS13()
		-- 请求分组情况
	end
    -- if self.m_GroupIndex2Name and self.m_GroupIndex2Name[self.m_CurSelectGroup] then
	-- 	g_ScriptEvent:BroadcastInLua("OnStarBiWuDetailInfoWndChange",SafeStringFormat3(LocalString.GetString("%s淘汰赛"),self.m_GroupIndex2Name[self.m_CurSelectGroup]))
	-- end
    if self.m_GroupIndex2Name then
        g_ScriptEvent:BroadcastInLua("OnStarBiWuDetailInfoWndChange",SafeStringFormat3(LocalString.GetString("%s淘汰赛"),self.m_GroupIndex2Name[1]))
    end
end

function LuaStarBiWuTaoTaiSaiDetailView:OnDisable()
    g_ScriptEvent:RemoveListener("OnSyncStarBiwuTaotaiSaiGroupS13", self, "OnGetGroupInfo")
	g_ScriptEvent:RemoveListener("OnStarBiWuTaoTaiSaiResultData", self, "ReplyStarBiwuZongJueSaiMatch")
end

--@endregion UIEvent