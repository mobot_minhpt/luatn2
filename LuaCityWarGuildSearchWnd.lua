require("common/common_include")

local UILabel = import "UILabel"
local UIEventListener = import "UIEventListener"
local LuaUtils = import "LuaUtils"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local AlignType = import "L10.UI.CPopupMenuInfoMgr+AlignType"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"
local UISearchView = import "L10.UI.UISearchView"


CLuaCityWarGuildSearchWnd = class()
RegistClassMember(CLuaCityWarGuildSearchWnd, "m_ServerTable")
RegistClassMember(CLuaCityWarGuildSearchWnd, "m_MyServerId")
RegistClassMember(CLuaCityWarGuildSearchWnd, "m_ServerId2GuildInfoTable")
RegistClassMember(CLuaCityWarGuildSearchWnd, "m_CurrentServerId")
RegistClassMember(CLuaCityWarGuildSearchWnd, "m_GridView")
RegistClassMember(CLuaCityWarGuildSearchWnd, "m_CurrentDataTable")
RegistClassMember(CLuaCityWarGuildSearchWnd, "m_ServerSelectorExpandTrans")
RegistClassMember(CLuaCityWarGuildSearchWnd, "m_ServerLabel")

CLuaCityWarGuildSearchWnd.Path = "ui/citywar/LuaCityWarGuildSearchWnd"

function CLuaCityWarGuildSearchWnd:Init( ... )
	Gac2Gas.QueryCityWarServerList()
	-- 0:my server
	Gac2Gas.QueryCityWarServerCityList(0)

	self.m_ServerTable = {}
	self.m_ServerId2GuildInfoTable = {}

	self.m_GridView = self.transform:Find("Anchor/ListView"):GetComponent(typeof(QnAdvanceGridView))
    self.m_GridView.m_DataSource = DefaultTableViewDataSource.Create(function()
			return #self.m_CurrentDataTable
		end, function(item, index)
			self:InitItem(item.gameObject, index + 1)
		end)
    self.m_ServerSelectorExpandTrans = self.transform:Find("Anchor/Top/ServerSelector/ExpandFlag")
    self.m_ServerLabel = self.transform:Find("Anchor/Top/ServerSelector/Server"):GetComponent(typeof(UILabel))
    self.transform:Find("Anchor/Top/SearchView"):GetComponent(typeof(UISearchView)).OnSearch = DelegateFactory.Action_string(function (text)
		self:Search(text)
	end)
end

function CLuaCityWarGuildSearchWnd:Search(text)
	if not text then
		self.m_CurrentDataTable = self.m_ServerId2GuildInfoTable[self.m_CurrentServerId]
	else
		self.m_CurrentDataTable = {}
		if not self.m_ServerId2GuildInfoTable[self.m_CurrentServerId] then return end
		for k, v in ipairs(self.m_ServerId2GuildInfoTable[self.m_CurrentServerId]) do
			if string.find(tostring(v.GuildId), text) or string.find(v.GuildName, text) then
				table.insert(self.m_CurrentDataTable, v)
			end
		end
	end
	self.m_GridView:ReloadData(true, false)
end

function CLuaCityWarGuildSearchWnd:InitItem(obj, index)
	local transNameTbl = {"GuildId", "GuildName", "MapName", "CityName", "MapLevel"}
	local guildInfo = self.m_CurrentDataTable[index]
	local valTbl = {guildInfo.GuildId, guildInfo.GuildName, CityWar_Map.GetData(guildInfo.MapId).Name, CityWar_MapCity.GetData(guildInfo.TemplateId).Name, CityWar_Map.GetData(guildInfo.MapId).Level}
	for i = 1, #transNameTbl do
		obj.transform:Find(transNameTbl[i]):GetComponent(typeof(UILabel)).text = valTbl[i]
	end
end

function CLuaCityWarGuildSearchWnd:QueryCityWarServerListResult(serverListUD, myServerGroupId)
	self.m_MyServerId = myServerGroupId
	self.m_CurrentServerId = self.m_MyServerId

	local list = MsgPackImpl.unpack(serverListUD)
	if not list then return end
	self.m_ServerTable= {}
	table.insert(self.m_ServerTable, {ServerId = myServerGroupId, ServerName = LocalString.GetString("自己服务器")})
	for i = 0, list.Count - 1, 2 do
		local serverId = list[i]
		local serverName = list[i + 1]
		if serverId ~= myServerGroupId then
			table.insert(self.m_ServerTable, {ServerId = serverId, ServerName = serverName})
		end
	end

	local popupMenuItemTable = {}
	for i = 1, #self.m_ServerTable do
		table.insert(popupMenuItemTable, PopupMenuItemData(self.m_ServerTable[i].ServerName, DelegateFactory.Action_int(function ( index )
			self.m_CurrentServerId = self.m_ServerTable[index + 1].ServerId
			self:ServerSelectorOnSelected(index + 1)
		end), false, nil, EnumPopupMenuItemStyle.Light))
	end
	local popupMenuItemArray = Table2Array(popupMenuItemTable, MakeArrayClass(PopupMenuItemData))
	UIEventListener.Get(self.transform:Find("Anchor/Top/ServerSelector").gameObject).onClick = LuaUtils.VoidDelegate(function (go)
		Extensions.SetLocalRotationZ(self.m_ServerSelectorExpandTrans, 180)
		CPopupMenuInfoMgr.ShowPopupMenu(popupMenuItemArray, go.transform, AlignType.Bottom, 1, DelegateFactory.Action(function ( ... )
			Extensions.SetLocalRotationZ(self.m_ServerSelectorExpandTrans, 0)
		end), 600, true, 296)
	end)
end

function CLuaCityWarGuildSearchWnd:ServerSelectorOnSelected(index)
	Gac2Gas.QueryCityWarServerCityList(self.m_CurrentServerId)
	self.m_ServerLabel.text = self.m_ServerTable[index].ServerName
end

function CLuaCityWarGuildSearchWnd:QueryCityWarServerCityListResult(cityListUD, serverId)
	local list = MsgPackImpl.unpack(cityListUD)
	if not list then return end

	if serverId == 0 then serverId = self.m_MyServerId end
	self.m_ServerId2GuildInfoTable[serverId] = {}
	for i = 0, list.Count - 1, 5 do
		local guildId = list[i]
		local guildName = list[i + 1]
		local mapId = list[i + 2]
		local templateId = list[i + 3]
		local cityId = list[i + 4]
		table.insert(self.m_ServerId2GuildInfoTable[serverId], {GuildId = guildId, GuildName = guildName, MapId = mapId, TemplateId = templateId, CityId = cityId})
	end
	local sortFunc = function (a, b)
		if CityWar_MapCity.GetData(a.TemplateId).Name ~= LocalString.GetString("主城") then
			return false
		elseif CityWar_MapCity.GetData(b.TemplateId).Name ~= LocalString.GetString("主城") then
			if CityWar_MapCity.GetData(a.TemplateId).Name == LocalString.GetString("主城") then
				return true
			end
			return false
		else
			return CityWar_Map.GetData(a.MapId).Level > CityWar_Map.GetData(b.MapId).Level
		end
	end
	table.sort(self.m_ServerId2GuildInfoTable[serverId], sortFunc)
	self.m_CurrentDataTable = self.m_ServerId2GuildInfoTable[serverId]
	self.m_GridView:ReloadData(true, false)
end

function CLuaCityWarGuildSearchWnd:OnEnable( ... )
	g_ScriptEvent:AddListener("QueryCityWarServerListResult", self, "QueryCityWarServerListResult")
	g_ScriptEvent:AddListener("QueryCityWarServerCityListResult", self, "QueryCityWarServerCityListResult")
end

function CLuaCityWarGuildSearchWnd:OnDisable( ... )
	g_ScriptEvent:RemoveListener("QueryCityWarServerListResult", self, "QueryCityWarServerListResult")
	g_ScriptEvent:RemoveListener("QueryCityWarServerCityListResult", self, "QueryCityWarServerCityListResult")
end
