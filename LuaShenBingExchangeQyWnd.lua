require("3rdParty/ScriptEvent")
require("common/common_include")

local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local UIGrid = import "UIGrid"
local CUITexture = import "L10.UI.CUITexture"
local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"
local Item_Item = import "L10.Game.Item_Item"
local EnumQualityType = import "L10.Game.EnumQualityType"
local CItemMgr = import "L10.Game.CItemMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CButton = import "L10.UI.CButton"
local CCommonItemSelectInfoMgr = import "L10.UI.CCommonItemSelectInfoMgr"
local CCommonItemSelectCellData = import "L10.UI.CCommonItemSelectCellData"
local NGUIText = import "NGUIText"
local QualityColor = import "L10.Game.QualityColor"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CFreightEquipSubmitMgr = import "L10.UI.CFreightEquipSubmitMgr"
local CYuanbaoMarketMgr = import "L10.UI.CYuanbaoMarketMgr"
local MsgPackImpl = import "MsgPackImpl"
local Object = import "System.Object"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local MessageWndManager = import "L10.UI.MessageWndManager"
local MessageMgr = import "L10.Game.MessageMgr"
local CPlayerShopMgr = import "L10.UI.CPlayerShopMgr"
local UIWidget = import "UIWidget"


LuaShenBingExchangeQyWnd=class()

RegistClassMember(LuaShenBingExchangeQyWnd,"TableViewDataSource")
RegistClassMember(LuaShenBingExchangeQyWnd,"TableView")

RegistClassMember(LuaShenBingExchangeQyWnd, "m_QianYuanFormulas")
RegistClassMember(LuaShenBingExchangeQyWnd, "m_EquipsInBag")

function LuaShenBingExchangeQyWnd:Init()
	-- 初始化数据
	self.m_QianYuanFormulas = {}

	self:InitEquipsInBag()

	ShenBing_Qianyuan.Foreach(function(k, v)
		local consumes = {}
		local consumeInfos = CommonDefs.StringSplit_ArrayChar(v.Combination, ";")
		local fitConsumeCounter = 0

		for i = 0, consumeInfos.Length - 1, 1 do
			local details = CommonDefs.StringSplit_ArrayChar(consumeInfos[i], ",")
			if details.Length >= 2 then
				local templateId = tonumber(details[0])
				local isFake = tonumber(details[1]) == 0

				-- 检查该玩家是否拥有这件装备
				local itemId = nil
				if self.m_EquipsInBag[templateId] then
					for k, v in ipairs(self.m_EquipsInBag[templateId]) do
						if v.IsEquip and v.Equip.IsFake == isFake and not itemId then
							-- 有强化或升了宝石的装备不允许兑换前缘材料
							if v.Equip.IntensifyLevel <= 0 and not v.Equip.HasGems then
								fitConsumeCounter = fitConsumeCounter + 1
								itemId = v.Id
							end
						end
					end
				end

				table.insert(consumes, {
					templateId = templateId,
					isFake = isFake,
					itemToExchange = itemId,
					})
			end
		end

		local fitPercent = 0
		if fitConsumeCounter ~=0 then
			fitPercent = fitConsumeCounter / #consumes
		end
		table.insert(self.m_QianYuanFormulas, {
			ID = v.ID,
			Name = v.Name,
			Consumes = consumes,
			ExchangeNum1 = v.ExchangeNum1,
			ExchangeNum2 = v.ExchangeNum2,
			ExchangeNum3 = v.ExchangeNum3,
			FitPercent = fitPercent,
			})
	end)

	self:SortFormulas()

	local function initItem(item,index)
    	self:InitItem(item, index)
    end
    self.TableViewDataSource=DefaultTableViewDataSource.CreateByCount(#self.m_QianYuanFormulas,initItem)
    self.TableView.m_DataSource=self.TableViewDataSource
    self.TableView.OnSelectAtRow=DelegateFactory.Action_int(function (row)
        self:OnSelectAtRow(row)
    end)
    self.TableView:ReloadData(true,false)

end

-- 初始化包裹内的装备数据
-- <templateId, table of item with same templateId>
function LuaShenBingExchangeQyWnd:InitEquipsInBag()
	self.m_EquipsInBag = {}

	local itemProp = CClientMainPlayer.Inst.ItemProp
    local bagSize = itemProp:GetPlaceSize(EnumItemPlace.Bag)
    for i = 1, bagSize, 1 do
		local id = itemProp:GetItemAt(EnumItemPlace.Bag, i)
		if id and id ~= "" then
			local item = CItemMgr.Inst:GetById(id)
			if item and item.IsEquip then
				if not self.m_EquipsInBag[item.TemplateId] then
					local equipTable = { }
					self.m_EquipsInBag[item.TemplateId] = equipTable
				end
				table.insert(self.m_EquipsInBag[item.TemplateId], item)
			end
		end
	end
end

-- 给兑换公式排序
function LuaShenBingExchangeQyWnd:SortFormulas()
	local function compareFunc(a, b)
		if a.FitPercent == b.FitPercent then
			if #a.Consumes == #b.Consumes then
				return a.ID < b.ID
			else
				return #a.Consumes < #b.Consumes
			end
		else
			return a.FitPercent > b.FitPercent
		end
	end
	table.sort(self.m_QianYuanFormulas, compareFunc)
end

-- 初始化兑换公式
function LuaShenBingExchangeQyWnd:InitItem(item, index)
	local info = self.m_QianYuanFormulas[index+1]
	if not info then return end

	local tf = item.transform
	local nameLabel = tf:Find("NameLabel"):GetComponent(typeof(UILabel))
    nameLabel.text=info.Name
    local exchangeBtn = tf:Find("ExchangeBtn"):GetComponent(typeof(CButton))

    local consumeGrid = tf:Find("Consumes/Table"):GetComponent(typeof(UIGrid))
    CUICommonDef.ClearTransform(consumeGrid.transform)
    local consumeTemplate = tf:Find("Consumes/EquipTemplate").gameObject
    consumeTemplate:SetActive(false)

    local consumes = info.Consumes
    local enough = true
    for k, v in ipairs(consumes) do
    	local consume = NGUITools.AddChild(consumeGrid.gameObject, consumeTemplate)
    	consume:SetActive(true)
    	local result = self:InitConsume(consume, v.templateId, v.isFake, index, v.itemToExchange)
    	enough = enough and result
    end
    consumeGrid:Reposition()
    exchangeBtn.Enabled = enough

    local onExchange = function (go)
    	self:OnExchangeQianYuan(go, index)
    end
    CommonDefs.AddOnClickListener(exchangeBtn.gameObject,DelegateFactory.Action_GameObject(onExchange),false)


    local productGrid = tf:Find("Products/Table"):GetComponent(typeof(UIGrid))
    CUICommonDef.ClearTransform(productGrid.transform)
    local productTemplate = tf:Find("Products/ProductTemplate").gameObject
    productTemplate:SetActive(false)

    for i = 1, 3, 1 do
    	local product = NGUITools.AddChild(productGrid.gameObject, productTemplate)
			local templateId = 0
			if i == 1 then templateId = ShenBing_Setting.GetData().Qy1ItemId
			elseif i == 2 then templateId = ShenBing_Setting.GetData().Qy2ItemId
			else templateId = ShenBing_Setting.GetData().Qy3ItemId end
    	local count = info[SafeStringFormat3("ExchangeNum%d", i)]
    	if templateId then
    		product:SetActive(true)
    		self:InitProduct(product, templateId, count)
    	end
    end
    productGrid:Reposition()
end

-- 请求兑换前缘
function LuaShenBingExchangeQyWnd:OnExchangeQianYuan(go, index)
	local info = self.m_QianYuanFormulas[index+1]
	if not info then return end

	local list = CreateFromClass(MakeGenericClass(List, Object))

	local consumes = info.Consumes
	local enough = true
	for k, v in ipairs(consumes) do
		if v.itemToExchange then
			local item = CItemMgr.Inst:GetById(v.itemToExchange)
			local pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, v.itemToExchange)
			if item and pos ~= 0 then
				-- EnumItemPlace
				 CommonDefs.ListAdd(list, typeof(Int32), EnumItemPlace_lua.Bag)
				 CommonDefs.ListAdd(list, typeof(UInt32), pos)
				 CommonDefs.ListAdd(list, typeof(String), v.itemToExchange)
			else
				enough = false
			end
		else
			enough = false
		end
	end
	if enough then

		local msg = MessageMgr.Inst:FormatMessage("ShenBing_Exchange_QY_Confirm", {})
		MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(
        	function()
        		Gac2Gas.RequestExchangeQianYuan(info.ID, MsgPackImpl.pack(list))
        	end
        	), nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
	end

end

-- 初始化兑换需要的紫装
function LuaShenBingExchangeQyWnd:InitConsume(go, templateId, isFake, index, itemId)
	local tf = go.transform
	local iconTexture = tf:Find("EquipTexture"):GetComponent(typeof(CUITexture))
	local qualitySprite = tf:Find("QualitySprite"):GetComponent(typeof(UISprite))
	local disableSprite = tf:Find("DisableSprite").gameObject
	local addSprite = tf:Find("AddSprite").gameObject
	local bindSprite = tf:Find("BindSprite"):GetComponent(typeof(UISprite))

	iconTexture:Clear()
	qualitySprite.spriteName = CUICommonDef.GetItemCellBorder()
	disableSprite:SetActive(false)
	addSprite:SetActive(false)
	bindSprite.spriteName = nil

	local template = EquipmentTemplate_Equip.GetData(templateId)
	if not template then return end

	iconTexture:LoadMaterial(template.Icon)
	qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(CommonDefs.ConvertIntToEnum(typeof(EnumQualityType), template.Color))

	local enough = false
	if itemId then
		enough = true
	end

	disableSprite:SetActive(not enough)
	addSprite:SetActive(not enough)

	local item = CItemMgr.Inst:GetById(itemId)
	if item then
		qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(item.Equip.QualityType)
		bindSprite.spriteName = item.BindOrEquipCornerMark
	end

	local onBuyEquip = function (go)
		self:OnBuyEquip(go, templateId, isFake)
	end

	local onChangeEquip = function (go)
		self:OnChangeEquip(go, templateId, isFake, index)
	end

	if enough then
		--CommonDefs.AddOnClickListener(go,DelegateFactory.Action_GameObject(onChangeEquip),false)
	else
		--CommonDefs.AddOnClickListener(go,DelegateFactory.Action_GameObject(onBuyEquip),false)
	end
	return enough
end

-- 请求打开购买界面
function LuaShenBingExchangeQyWnd:OnBuyEquip(go, templateId, isFake)
	local template = EquipmentTemplate_Equip.GetData(templateId)
	if not template then return end

	local color = QualityColor.GetRGBValue(CommonDefs.ConvertIntToEnum(typeof(EnumQualityType), template.Color))
	local colorText = NGUIText.EncodeColor24(color)
	local name = template.Name
	if isFake then -- 显示盗版装备的名字
		name = template.AliasName
	end
	local title = SafeStringFormat3("[%s]%s[-]",colorText, name)

	local dict = CreateFromClass(MakeGenericClass(Dictionary, Int32, MakeGenericClass(List, cs_string)))
	local list = CreateFromClass(MakeGenericClass(List, cs_string))
	CommonDefs.DictAdd(dict, TypeOfInt32, 0, typeof(MakeGenericClass(List, cs_string)), list)

	local callback = function ()
		local toBuyTId = templateId
		if isFake then
			toBuyTId = CPlayerShopMgr.Inst:ToAliasTemplateId(templateId)
		end
		CYuanbaoMarketMgr.ShowPlayerShop(toBuyTId)
	end
	CFreightEquipSubmitMgr.OpenEquipmentChooseWnd("", dict, name, 0, false, LocalString.GetString("购买"), LocalString.GetString("前往购买"), DelegateFactory.Action(callback), LocalString.GetString("包裹中没有该物品"))
end

-- 请求换提交装备
function LuaShenBingExchangeQyWnd:OnChangeEquip(go, templateId, isFake, index)
	local template = EquipmentTemplate_Equip.GetData(templateId)
	if not template then return end

	local color = QualityColor.GetRGBValue(CommonDefs.ConvertIntToEnum(typeof(EnumQualityType), template.Color))
	local colorText = NGUIText.EncodeColor24(color)
	local name = template.Name
	if isFake then -- 显示盗版装备的名字
		name = template.AliasName
	end
	CCommonItemSelectInfoMgr.Inst.Title = SafeStringFormat3("[%s]%s[-]",colorText, name)
	CommonDefs.ListClear(CCommonItemSelectInfoMgr.Inst.AllData)

	-- Find selected item id
	local info = self.m_QianYuanFormulas[index+1]
	if not info then return end
	local selectedItemId = ""
    for k, v in ipairs(info.Consumes) do
    	if v.templateId == templateId and v.isFake == isFake then
    		selectedItemId = v.itemToExchange
    		break
    	end
    end

	local itemProp = CClientMainPlayer.Inst.ItemProp
    local bagSize = itemProp:GetPlaceSize(EnumItemPlace.Bag)
	for i = 1, bagSize, 1 do
		local id = itemProp:GetItemAt(EnumItemPlace.Bag, i)
		if id and id ~= "" then
			local item = CItemMgr.Inst:GetById(id)
			if item and item.TemplateId == templateId and item.IsEquip and item.Equip.IsFake == isFake then
				if item.Equip.IntensifyLevel <= 0 and not item.Equip.HasGems then
					local data = CreateFromClass(CCommonItemSelectCellData, item.Id, item.TemplateId, item.IsBinded, item.Amount)
					if item.Id == selectedItemId then
						CommonDefs.ListInsert(CCommonItemSelectInfoMgr.Inst.AllData, 0, typeof(CCommonItemSelectCellData), data)
					else
						CommonDefs.ListAdd(CCommonItemSelectInfoMgr.Inst.AllData, typeof(CCommonItemSelectCellData), data)
					end
				end
			end
		end
	end

	local callback = function (itemId, templateId)
		self:OnSelectEquipChange(itemId, templateId, index)
	end
	CCommonItemSelectInfoMgr.Inst.m_OnSelectCallback = DelegateFactory.Action_string_uint(callback)
	CUIManager.ShowUI(CUIResources.CommonItemSelectWnd)
end

-- 选择替换用来兑换的装备
function LuaShenBingExchangeQyWnd:OnSelectEquipChange(itemId, templateId, index)
	local info = self.m_QianYuanFormulas[index+1]
	if not info then return end

	local item = CItemMgr.Inst:GetById(itemId)
	if not item or not item.IsEquip then return end
	local consumes = info.Consumes
    for k, v in ipairs(consumes) do
    	if v.templateId == item.TemplateId and v.isFake == item.Equip.IsFake and v.itemToExchange ~= itemId then
    		v.itemToExchange = itemId
    	end
    end
    -- 刷新
    local tableItem = self.TableView:GetItemAtRow(index)
    if tableItem then
    	self:InitItem(tableItem, index)
    end
end

-- 初始化兑换的前缘物品
function LuaShenBingExchangeQyWnd:InitProduct(go, templateId, count)
	local tf = go.transform
	local iconTexture = tf:Find("IconTexture"):GetComponent(typeof(CUITexture))
	local qualitySprite = tf:Find("QualitySprite"):GetComponent(typeof(UISprite))
	local amountLabel = tf:Find("AmountLabel"):GetComponent(typeof(UILabel))

	iconTexture:Clear()
	qualitySprite.spriteName = CUICommonDef.GetItemCellBorder()
	amountLabel.text = ""

	local template = Item_Item.GetData(templateId)
	if not template then return end

	iconTexture:LoadMaterial(template.Icon)
	qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(template, nil, false)
	amountLabel.text = tostring(count)

	local onClickProduct = function (product)
		CItemInfoMgr.ShowLinkItemTemplateInfo(templateId, false, nil, AlignType.Default, 0, 0, 0, 0)
	end
	CommonDefs.AddOnClickListener(go,DelegateFactory.Action_GameObject(onClickProduct),false)

	local widget = go:GetComponent(typeof(UIWidget))
	if count == 0 then
		widget.alpha = 0.3
	else
		widget.alpha = 1.0
	end
end

-- 显示该公式所需物品的购买页
function LuaShenBingExchangeQyWnd:OnSelectAtRow(row)
	local info = self.m_QianYuanFormulas[row+1]
	if not info then return end
	LuaShenbingMgr:OpenBuyQianYuanEquips(info, row)
end

function LuaShenBingExchangeQyWnd:OnEnable()
	g_ScriptEvent:AddListener("RequestExchangeQianYuanSuccess", self, "Init")
	g_ScriptEvent:AddListener("SendItem", self, "SendItem")
	g_ScriptEvent:AddListener("SetItemAt", self, "SetItemAt")
	g_ScriptEvent:AddListener("ShenBingExchangeQySelect", self, "OnSelectEquipChange")
end

function LuaShenBingExchangeQyWnd:OnDisable()
	g_ScriptEvent:RemoveListener("RequestExchangeQianYuanSuccess", self, "Init")
	g_ScriptEvent:RemoveListener("SendItem", self, "SendItem")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "SetItemAt")
	g_ScriptEvent:RemoveListener("ShenBingExchangeQySelect", self, "OnSelectEquipChange")
end

function LuaShenBingExchangeQyWnd:SendItem(args)
	local item = CItemMgr.Inst:GetById(args[0])
	if not item or not item.IsEquip then return end

	if item.Equip.IntensifyLevel > 0 or item.Equip.HasGems then return end

	self:InitEquipsInBag()
	for _, v in ipairs(self.m_QianYuanFormulas) do
		local consumes = v.Consumes
		local fitConsumeCounter = 0
		for _, info in ipairs(consumes) do
			if info.templateId == item.TemplateId and info.isFake == item.Equip.IsFake then
				if not info.itemToExchange then
					info.itemToExchange = args[0]
				end
			end
			if info.itemToExchange then
				fitConsumeCounter = fitConsumeCounter + 1
			end
		end
		local fitPercent = 0
		if fitConsumeCounter ~=0 then
			fitPercent = fitConsumeCounter / #consumes
		end
		v.FitPercent = fitPercent
	end
	self:SortFormulas()
	self.TableView:ReloadData(true,false)

end

function LuaShenBingExchangeQyWnd:SetItemAt(place, position, oldItemId, newItemId)
	if EnumItemPlace.Bag ~= place or CClientMainPlayer.Inst == nil or position == 0 then
        return
    end

    self:InitEquipsInBag()

	local itemProp = CClientMainPlayer.Inst.ItemProp
    local itemId = itemProp:GetItemAt(place, position)

	-- 添加一个是否包裹的检查
	local item = CItemMgr.Inst:GetById(itemId)
	if not item or not item.IsEquip then return end

	if item.Equip.IntensifyLevel > 0 or item.Equip.HasGems then return end

	for _, v in ipairs(self.m_QianYuanFormulas) do
		local consumes = v.Consumes
		local fitConsumeCounter = 0
		for _, info in ipairs(consumes) do
			if info.templateId == item.TemplateId and info.isFake == item.Equip.IsFake then
				if not info.itemToExchange then
					info.itemToExchange = itemId
				end
			end
			if info.itemToExchange then
				fitConsumeCounter = fitConsumeCounter + 1
			end
		end
		local fitPercent = 0
		if fitConsumeCounter ~=0 then
			fitPercent = fitConsumeCounter / #consumes
		end
		v.FitPercent = fitPercent
	end

	self:SortFormulas()
	self.TableView:ReloadData(true,false)

end

return LuaShenBingExchangeQyWnd
