local UILabel = import "UILabel"
local CCommonUITween = import "L10.UI.CCommonUITween"
local GameObject = import "UnityEngine.GameObject"
local QnCheckBox = import "L10.UI.QnCheckBox"
local CItemCountUpdate = import "L10.UI.CItemCountUpdate"
local UITable = import "UITable"
local UITexture = import "UITexture"
local CRenderObject  = import "L10.Engine.CRenderObject"
local CFashionPreviewMgr = import "L10.UI.CFashionPreviewMgr"
local Quaternion = import "UnityEngine.Quaternion"
local CEffectMgr = import "L10.Game.CEffectMgr"
local LayerDefine = import "L10.Engine.LayerDefine"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local Quaternion = import "UnityEngine.Quaternion"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CEquipmentProcessMgr = import "L10.Game.CEquipmentProcessMgr"
local CItem = import "L10.Game.CItem"
local CItemMgr = import "L10.Game.CItemMgr"
local EnumQualityType = import "L10.Game.EnumQualityType"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CPreciousMgr = import "L10.Game.CPreciousMgr"
local CRecreateData = import "L10.Game.CRecreateData"
local UILabelOverflow = import "UILabel+Overflow"
local AlignType2 = import "L10.UI.CItemInfoMgr+AlignType"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local AlignType1 = import "L10.UI.CTooltip+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local Animation = import "UnityEngine.Animation"
local PlayerShowDifProType = import "L10.Game.PlayerShowDifProType"
local CPropertyDifMgr = import "L10.UI.CPropertyDifMgr"
local LayerDefine = import "L10.Engine.LayerDefine"
local ParticleSystem = import "UnityEngine.ParticleSystem"
local CEquipBaptizeDetailView = import "L10.UI.CEquipBaptizeDetailView"
local QualityColor = import "L10.Game.QualityColor"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local BoxCollider = import "UnityEngine.BoxCollider"
local CMainCamera = import "L10.Engine.CMainCamera"
local CameraClearFlags = import "UnityEngine.CameraClearFlags"

LuaRecreateExtraEquipWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaRecreateExtraEquipWnd, "Bottom", "Bottom", GameObject)
RegistChildComponent(LuaRecreateExtraEquipWnd, "Right", "Right", GameObject)
RegistChildComponent(LuaRecreateExtraEquipWnd, "AniCheckBox", "AniCheckBox", QnCheckBox)
RegistChildComponent(LuaRecreateExtraEquipWnd, "WuLianCheckBox", "WuLianCheckBox", QnCheckBox)
RegistChildComponent(LuaRecreateExtraEquipWnd, "ReplaceBtn", "ReplaceBtn", GameObject)
RegistChildComponent(LuaRecreateExtraEquipWnd, "TipBtn", "TipBtn", GameObject)
RegistChildComponent(LuaRecreateExtraEquipWnd, "ShareBtn", "ShareBtn", GameObject)
RegistChildComponent(LuaRecreateExtraEquipWnd, "EquipWordState", "EquipWordState", GameObject)
RegistChildComponent(LuaRecreateExtraEquipWnd, "ItemCostNode", "ItemCostNode", CItemCountUpdate)
RegistChildComponent(LuaRecreateExtraEquipWnd, "CommitBtn", "CommitBtn", GameObject)
RegistChildComponent(LuaRecreateExtraEquipWnd, "EquipIcon", "EquipIcon", UITexture)
RegistChildComponent(LuaRecreateExtraEquipWnd, "WordItem", "WordItem", GameObject)
RegistChildComponent(LuaRecreateExtraEquipWnd, "Panel3D", "Panel3D", GameObject)
RegistChildComponent(LuaRecreateExtraEquipWnd, "Multi", "Multi", CCommonUITween)
RegistChildComponent(LuaRecreateExtraEquipWnd, "Single", "Single", CCommonUITween)
RegistChildComponent(LuaRecreateExtraEquipWnd, "Anchor", "Anchor", GameObject)
RegistChildComponent(LuaRecreateExtraEquipWnd, "OnlyBindCheckBox", "OnlyBindCheckBox", QnCheckBox)
RegistChildComponent(LuaRecreateExtraEquipWnd, "ItemIcon", "ItemIcon", UITexture)
RegistChildComponent(LuaRecreateExtraEquipWnd, "EquipInfo_Style01", "EquipInfo_Style01", GameObject)
RegistChildComponent(LuaRecreateExtraEquipWnd, "EquipInfo_Style02", "EquipInfo_Style02", Animation)
RegistChildComponent(LuaRecreateExtraEquipWnd, "ItemNameLabel", "ItemNameLabel", UILabel)
RegistChildComponent(LuaRecreateExtraEquipWnd, "GetNode", "GetNode", GameObject)
RegistChildComponent(LuaRecreateExtraEquipWnd, "MainWordTable", "MainWordTable", UITable)
RegistChildComponent(LuaRecreateExtraEquipWnd, "MainScoreLabel", "MainScoreLabel", UILabel)
RegistChildComponent(LuaRecreateExtraEquipWnd, "Bg", "Bg", GameObject)
RegistChildComponent(LuaRecreateExtraEquipWnd, "CloseButton", "CloseButton", GameObject)
RegistChildComponent(LuaRecreateExtraEquipWnd, "CUIFxtuowei", "CUIFxtuowei", Animation)

--@endregion RegistChildComponent end

function LuaRecreateExtraEquipWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.ReplaceBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnReplaceBtnClick()
	end)

	UIEventListener.Get(self.TipBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipBtnClick()
	end)

	UIEventListener.Get(self.ShareBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareBtnClick()
	end)

	UIEventListener.Get(self.CommitBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCommitBtnClick()
	end)

	UIEventListener.Get(self.CloseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCloseButtonClick()
	end)

    --@endregion EventBind end
end

function LuaRecreateExtraEquipWnd:Init()
    self.m_ItemId = CLuaEquipMgr.m_RecreateItemId
	self.m_IsWuLian	= CLuaEquipMgr.m_IsWuLian

    local item = CItemMgr.Inst:GetById(self.m_ItemId)
    self.m_EquipData = item.Equip
    self.m_TemplateId = self.m_EquipData.TemplateId
    self.m_TemplateData = EquipmentTemplate_Equip.GetData(self.m_TemplateId)
    self.m_IsBind = self.m_EquipData.IsBinded

    self.m_SkipAni = false
    self.m_OnlyBind = false

    -- 当然有没有选中的金色卡牌
    self.m_HasUnsetPrecious = false
    self.m_HasUnsetResult = false

    -- 当前是否是3D卡牌界面
    self.m_Full3DView = false
    self.CUIFxtuowei.gameObject:SetActive(false)
    
    self.m_MainAni = self.transform:GetComponent(typeof(Animation))

    self.WordItem:SetActive(false)

    self:InitBottomUI()
    self:InitMainWords()
    self:InitEquipInfo()

    -- 控制节点的x偏移
    self.m_WuLianOriginPos = {-700, -350, 0, 350, 700}
    self.m_WuLianPointList = {}

    for i=1,5 do
        table.insert(self.m_WuLianPointList, self.Multi.transform:Find(tostring(i)))
    end

    self.m_Scale = 0.72
    self.m_CardSize = 372
    self.m_Padding = 12
    self.m_StartPos = -630

    self.Bg.gameObject:GetComponent(typeof(UIWidget)).depth = -1
    
    -- 判断是否有为保持的结果
    local hasUnset = self.m_EquipData.RecreateData.Data and self.m_EquipData.RecreateData.Data.Length > 0
    if hasUnset then
        self.m_ItemData = CItemMgr.Inst:GetById(self.m_ItemId)
        self.m_EquipData = self.m_ItemData.Equip

        self.m_RecreateData = CreateFromClass(CRecreateData)
		self.m_RecreateData:LoadFromString(self.m_EquipData.RecreateData.Data)

        -- 设置一下五连状态再刷新就好了
        self.WuLianCheckBox:SetSelected(self.m_RecreateData.WordSets.Count == 5, false)
        -- 特殊设置一下
        self.m_IsWuLian = self.m_RecreateData.WordSets.Count == 5

        self:OnRecreateExtraEquipResult(self.m_ItemId , true, nil, true)
    else
        self.m_MainAni:Play("recreateextraequipwnd_show01")
    end
end

-- 重置位置
function LuaRecreateExtraEquipWnd:ResetMutilPos()
    for i=1, 5 do
        self.m_WuLianPointList[i].localPosition = Vector3(self.m_WuLianOriginPos[i],0,0)
    end
end

-- 设置界面是否3D阶段 为了防止界面与其他界面的交叠关系出错
-- 手动调整一个缩放
function LuaRecreateExtraEquipWnd:Set3DLayer(is3D)
    if is3D then
        self.Bg.gameObject:SetActive(true)
        self.Panel3D.transform.localScale = Vector3(1,1,1)
        NGUITools.SetLayer(self.Panel3D.gameObject, LayerDefine.ModelForNGUI_3D)
    else
        -- 非3D场景关闭点击事件
        self.Bg.gameObject:SetActive(false)

        for i =1, #self.m_RecreateDataList do
            local r = self.m_RecreateDataList[i]
            local boxCollider = r.item.transform.parent:GetComponent(typeof(BoxCollider))
            boxCollider.size = Vector3(370, 800, 0)
        end

        local s = 1.095
        self.Panel3D.transform.localScale = Vector3(s,s,1)
        NGUITools.SetLayer(self.Panel3D.gameObject, LayerDefine.UI)
    end
end

-- 每次变动选中的时候 调用一下
function LuaRecreateExtraEquipWnd:CalMutilPos()
    self.m_MutilPos = {}

    local p = self.m_StartPos
    if self.m_CurrentSelect == 1 then
        p = p + self.m_CardSize/2
    else
        p = p + self.m_CardSize*self.m_Scale/2
    end
    -- 第一个点位置
    table.insert(self.m_MutilPos, p)

    for i= 2, 5 do
        if self.m_CurrentSelect == i or self.m_CurrentSelect == i -1 then
            -- 如果这个是选中的 或者上一个是选中的
            p = p + self.m_CardSize*self.m_Scale/2 + self.m_CardSize/2 + self.m_Padding
        else
            -- 都不是选中的
            p = p + self.m_CardSize*self.m_Scale + self.m_Padding
        end
        table.insert(self.m_MutilPos, p)
    end

    self.m_TweenList = {}
    for i=1, 5 do
        self.m_TweenList[i] = LuaTweenUtils.DOLocalMoveX(self.m_WuLianPointList[i], self.m_MutilPos[i], 0.5, false)
    end
    
end

-- 初始化时候调用一次
function LuaRecreateExtraEquipWnd:InitBottomUI()
    self.AniCheckBox:SetSelected(self.m_SkipAni, true)

    self.AniCheckBox.OnValueChanged = DelegateFactory.Action_bool(function(val)
        self.m_SkipAni = val
    end)

    if CEquipBaptizeDetailView.s_EnableWuLianRecreate then
        self.WuLianCheckBox.gameObject:SetActive(true)
        self.WuLianCheckBox:SetSelected(self.m_IsWuLian, true)
        self.WuLianCheckBox.OnValueChanged = DelegateFactory.Action_bool(function(val)
            self.m_IsWuLian = val
            self:InitWuLianState()
        end)
    else
        self.WuLianCheckBox.gameObject:SetActive(false)
        self.m_IsWuLian = false
    end

    -- 初始时候置灰替换按钮
    CUICommonDef.SetActive(self.ReplaceBtn, false, true)

    -- 刷一下消耗和词条显示
    self:InitWuLianState()
end

-- 初始化主词条显示
function LuaRecreateExtraEquipWnd:InitMainWords()
    local fixed = self.m_EquipData:GetFixedWordList(true)
    local ext = self.m_EquipData:GetExtWordList(true)

    local list = {}

    for i=0, fixed.Count -1 do
        table.insert(list, fixed[i])
    end
    for i=0, ext.Count -1 do
        table.insert(list, ext[i])
    end

    Extensions.RemoveAllChildren(self.MainWordTable.transform)
    self:InitWord(self.m_ItemId, self.MainWordTable, self.WordItem, list)

    -- 刷一下位置
    local scrollView = self.MainWordTable.transform.parent:GetComponent(typeof(CUIRestrictScrollView))
    scrollView:ResetPosition()

    self.MainScoreLabel.text = SafeStringFormat3(LocalString.GetString("评分 %s"), self.m_EquipData.Score)
end

-- 初始化装备信息显示
function LuaRecreateExtraEquipWnd:InitEquipInfo()
    -- 创建贴图
    self.m_EquipTextureLoader = LuaDefaultModelTextureLoader.Create(function (ro)
        self:LoadEquip(ro)
    end)
    local texture = CUIManager.CreateModelTexture("__recreateExtraEquip__", self.m_EquipTextureLoader, 180, 0, 0, 0, false,true,1)

    local icon1 = self.EquipInfo_Style01.transform:Find("EquipIcon"):GetComponent(typeof(UITexture))
    local icon2 = self.EquipInfo_Style02.transform:Find("EquipIcon"):GetComponent(typeof(UITexture))
    icon1.mainTexture = texture
    icon2.mainTexture = texture

    local name1 = self.EquipInfo_Style01.transform:Find("EquipName"):GetComponent(typeof(UILabel))
    local name2 = self.EquipInfo_Style02.transform:Find("EquipName"):GetComponent(typeof(UILabel))
    name1.text = self.m_EquipData.ColoredDisplayName
    name2.text = self.m_EquipData.ColoredDisplayName

    local category1 = self.EquipInfo_Style01.transform:Find("EquipCategory"):GetComponent(typeof(UILabel))
    local category2 = self.EquipInfo_Style02.transform:Find("EquipName/EquipCategory"):GetComponent(typeof(UILabel))

    local typeData = EquipmentTemplate_Type.GetData(self.m_TemplateData.Type)
    category1.text = typeData.Name
    category2.text = typeData.Name

    local level1 = self.EquipInfo_Style01.transform:Find("EquipLevel"):GetComponent(typeof(UILabel))
    local level2 = self.EquipInfo_Style02.transform:Find("EquipName/EquipLevel"):GetComponent(typeof(UILabel))
    level1.text = SafeStringFormat3(LocalString.GetString("%s级"), self.m_EquipData.Grade)
    level2.text = SafeStringFormat3(LocalString.GetString("%s级"), self.m_EquipData.Grade)

    -- self.EquipInfo_Style01:SetActive(true)
    -- self.EquipInfo_Style02:SetActive(true)
end

-- 修改五连状态与消耗
function LuaRecreateExtraEquipWnd:InitWuLianState()
    local btnLabel = self.CommitBtn.transform:Find("Label"):GetComponent(typeof(UILabel))
    if self.m_IsWuLian then
        btnLabel.text = LocalString.GetString("五连再造")
    else
        btnLabel.text = LocalString.GetString("单次再造")
    end

    local templateId, count = CLuaEquipMgr:GetRecreateCost(self.m_ItemId, self.m_IsWuLian)
	local itemData = Item_Item.GetData(templateId)

    -- 非绑定只能消耗非绑
	self.ItemCostNode.onlyUnbind = not self.m_IsBind

    -- 只消耗绑定的checkbox
    if not self.m_IsBind then
        self.m_OnlyBind = false
        self.OnlyBindCheckBox.gameObject:SetActive(false)
    else
        self.OnlyBindCheckBox.gameObject:SetActive(true)

        self.OnlyBindCheckBox:SetSelected(self.m_OnlyBind, true)
        self.OnlyBindCheckBox.OnValueChanged = DelegateFactory.Action_bool(function(val)
            self.m_OnlyBind = val
            self:InitWuLianState()
        end)
    end

    -- 消耗显示节点
    self.ItemCostNode.templateId = templateId
    self.ItemCostNode.excludeExpireTime = true
    self.ItemCostNode.onlyBind = self.m_OnlyBind
    self.ItemCostNode.format = SafeStringFormat3("{0}/%s", tostring(count))
    self.ItemCostNode:UpdateCount()

    self.ItemIcon.transform:GetComponent(typeof(CUITexture)):LoadMaterial(itemData.Icon)

    if not self.m_IsBind then
        self.ItemNameLabel.text = SafeStringFormat3(LocalString.GetString("%s(非绑定)"), itemData.Name)
    elseif self.m_OnlyBind then
        self.ItemNameLabel.text = SafeStringFormat3(LocalString.GetString("%s(绑定)"), itemData.Name)
    else
        self.ItemNameLabel.text = itemData.Name
    end
	
    local curCount = self.ItemCostNode.count
	if curCount >= count then
		self.GetNode:SetActive(false)
		UIEventListener.Get(self.ItemIcon.gameObject).onClick = DelegateFactory.VoidDelegate(function()
			CItemInfoMgr.ShowLinkItemTemplateInfo(templateId, false, nil, AlignType2.ScreenRight, 0, 0, 0, 0)
		end)
	else
		self.GetNode:SetActive(true)
		UIEventListener.Get(self.ItemIcon.gameObject).onClick = DelegateFactory.VoidDelegate(function()
			CItemAccessListMgr.Inst:ShowItemAccessInfo(templateId, false, self.GetNode.transform, AlignType1.Right)
		end)
	end
end

-- 加载模型贴图
function LuaRecreateExtraEquipWnd:LoadEquip(ro)
    local _, _, data = CLuaEquipMgr:GetRecreateCost(self.m_ItemId, false)

    local pos = data.EquipPosition
    local rot = data.EquipRotation

    ro.gameObject.transform.localPosition = Vector3(pos[0], pos[1], pos[2])
    ro.gameObject.transform.localRotation = Quaternion.Euler(rot[0], rot[1], rot[2])

    local perfabPath = self.m_TemplateData.Prefab
    local fxList = g_LuaUtil:StrSplit(perfabPath, ";")
    local fxId = tonumber(fxList[1])

    local scale = 2
    CEffectMgr.Inst:AddObjectFX(fxId, ro ,0, scale,1,nil,false,EnumWarnFXType.None, Vector3.zero, Vector3.zero, DelegateFactory.Action_GameObject(function(go)
        local particleList = CommonDefs.GetComponentsInChildren_Component_Type(go.transform, typeof(ParticleSystem))
        for i=0, particleList.Length-1 do
			particleList[i].gameObject:SetActive(false)
		end

        -- 乌金扇
        if self.m_TemplateId == 23200035 then
            go.transform:Find("bs_004_shanzi_Lv109/Bone01/plane01 (2)").gameObject:SetActive(false)
            go.transform:Find("bs_004_shanzi_Lv109/Bone01/plane01 (3)").gameObject:SetActive(false)
        elseif self.m_TemplateId == 23200045 then
            -- 张继之月落乌啼
            go.transform:Find("bs_005_chuanghua_skin/Bone01/plane_glow_001").gameObject:SetActive(false)
            go.transform:Find("bs_005_chuanghua_skin/Bone01/plane_glow_002").gameObject:SetActive(false)
            go.transform:Find("bs_005_chuanghua_skin/Bone01/plane_glow_003").gameObject:SetActive(false)
            go.transform:Find("bs_005_chuanghua_skin/Bone01/plane002").gameObject:SetActive(false)
            go.transform:Find("bs_005_chuanghua_skin/Bone01/plane003").gameObject:SetActive(false)
        end
    end))
end

function LuaRecreateExtraEquipWnd:InitWord(itemId, tbl, wordItem, wordList, heightList, insertTbl)
    -- 处理颜色
    local item = CItemMgr.Inst:GetById(itemId)
    local color = QualityColor.GetRGBValue(EnumQualityType.Purple, #wordList)

    local wordMaxList = CLuaEquipMgr:EquipWordIdIsMaxWord(itemId,wordList)
    for i, id in ipairs(wordList) do
        local g = NGUITools.AddChild(tbl.gameObject, wordItem)
        g:SetActive(true)
        
        local nameLabel = g.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
        local descLabel = g.transform:Find("DescLabel"):GetComponent(typeof(UILabel))
        local bgSprite = g.transform:Find("Bg"):GetComponent(typeof(UISprite))
        
        local data = Word_Word.GetData(id)

        -- descLabel.overflowMethod = UILabelOverflow.ShrinkContent
        if data then
            nameLabel.text = SafeStringFormat3(LocalString.GetString("[%s]"), data.Name)
            descLabel.text = data.Description
            descLabel.text = data.Description
            if wordMaxList[i] then
                descLabel.text = CLuaEquipMgr.WordMaxMark..data.Description
            end
        else
            nameLabel.text = ""
            descLabel.text = ""
        end

        nameLabel.color = color
        descLabel.color = color

        if heightList then
            descLabel.height = heightList[i]
        end
    end

    if insertTbl then
        table.insert(self.m_WordTableList, tbl)
    end
end

-- 检查材料是否足够
function LuaRecreateExtraEquipWnd:CheckItem()
    local templateId, count = CLuaEquipMgr:GetRecreateCost(self.m_ItemId, self.m_IsWuLian)
    local curCount = self.ItemCostNode.count
	if curCount >= count then
		return true
	else
		-- 材料不足
        return false
	end
end

-- 再造成功的回调
function LuaRecreateExtraEquipWnd:OnRecreateExtraEquipResult(equipId, isSuccess, tbl, jumpToResult)
    if equipId == self.m_ItemId and isSuccess then
        self:StopFloping()

        -- 清掉当前有金色卡牌未选择的状态
        self.m_HasUnsetPrecious = false
        self.m_HasUnsetResult = true

        -- 刷新一下
        self.m_ItemData = CItemMgr.Inst:GetById(self.m_ItemId)
        self.m_EquipData = self.m_ItemData.Equip

        self.m_RecreateData = CreateFromClass(CRecreateData)
		self.m_RecreateData:LoadFromString(self.m_EquipData.RecreateData.Data)

        UnRegisterTick(self.m_ConfirmTick)
        self.Single:PlayAppearAnimation()
        self.Multi:PlayAppearAnimation()

        UnRegisterTick(self.m_Tick)
        self:Set3DLayer(true)

        self.m_FlopList = {}

        -- 属性变更的记录
        self.m_ChangeTbl = tbl

        -- 清楚一下显示selected状态的tick
        UnRegisterTick(self.m_ShowSelectTick)
        if self.m_IsWuLian then
            self:InitWuLianRecreate(jumpToResult)
        else
            self:InitSingleRecreate(jumpToResult)
        end

        -- 刷一下底部ui
        -- 主要是为了把特殊的初始五连状态刷掉
        if jumpToResult then
            self.m_IsWuLian = CLuaEquipMgr.m_IsWuLian
            self:InitBottomUI()
        end
        
        -- 但是上面会把替换按钮刷掉 这里再处理一下
        CUICommonDef.SetActive(self.ReplaceBtn, true, true)
    elseif not isSuccess then
        self.m_CommitMark = false
    end
end

-- 进入到五连界面
function LuaRecreateExtraEquipWnd:InitWuLianRecreate(jumpToResult)
    -- 重置位置
    self:ResetMutilPos()

    local dataList = {}
    CommonDefs.DictIterate(self.m_RecreateData.WordSets, DelegateFactory.Action_object_object(function (k,v) 
        local data = {}
        data.id = tonumber(k)
        data.data = v

        table.insert(dataList, data)
    end))

    for i=1, #dataList do
        dataList[i].originNum = i
    end

    table.sort(dataList, function(a, b)
        local s1 = a.data.TempScore
        local s2 = b.data.TempScore

        if s1 ~= s2 then
            return s1 > s2
        else
            return a.id > b.id
        end
    end)

    self.m_RecreateDataList = dataList
    self.m_WordTableList = {}
    for i= 1, 5 do
        local data = dataList[i].data
        local id = dataList[i].id
        local originNum = dataList[i].originNum
        local item = self.Multi.transform:Find(tostring(i).."/" ..tostring(i)).gameObject
        self:InitCardInfo(item, id, data, i, originNum)
    end

    self.Bg.gameObject:SetActive(true)
    UIEventListener.Get(self.Bg).onClick = DelegateFactory.VoidDelegate(function()
        local r = self.m_RecreateDataList[1]
        self:OnCardClick(r.item, 1, r.isPrecious)
    end)

    --播放音效
    if self.m_Voice then
        SoundManager.Inst:StopSound(self.m_Voice)
    end

    if not jumpToResult then
        if self.m_SkipAni then
            self.m_Voice = SoundManager.Inst:PlayOneShot("event:/L10/L10_UI/ZaiZaoGongNeng/UI_Rebuid_Five_Skip", Vector3.zero, nil, 0, -1)
        else
            self.m_Voice = SoundManager.Inst:PlayOneShot("event:/L10/L10_UI/ZaiZaoGongNeng/UI_Rebuid_Five", Vector3.zero, nil, 0, -1)
        end
    end
    
    UnRegisterTick(self.m_SoundTick)
    self.m_SoundTick = RegisterTickOnce(function()
        SoundManager.Inst:StopSound(self.m_Voice)
    end, 2000)

    if jumpToResult then
        self:FlopAllCard()
        self.EquipInfo_Style01:SetActive(true)
        self.m_MainAni:Play("recreateextraequipwnd_multi_shuxing")
        return
    end

    if not self.m_SkipAni then
        self.m_MainAni:Play("recreateextraequipwnd_multi")
    else
        self.m_MainAni:Play("recreateextraequipwnd_multi_skip")
    end
    self.m_Full3DView = true
end

-- 进入到单抽界面
function LuaRecreateExtraEquipWnd:InitSingleRecreate(jumpToResult)
    local dataList = {}
    CommonDefs.DictIterate(self.m_RecreateData.WordSets, DelegateFactory.Action_object_object(function (k,v) 
        local data = {}
        data.id = tonumber(k)
        data.data = v
        table.insert(dataList, data)
    end))
    self.m_RecreateDataList = dataList
    dataList[1].originNum = 1
    
    local data = dataList[1].data
    local id = dataList[1].id
    local item = self.Single.transform:Find("1/1").gameObject

    self.m_WordTableList = {}
    self:InitCardInfo(item, id, data, 1, 1)

    self.Bg.gameObject:SetActive(true)
    UIEventListener.Get(self.Bg).onClick = DelegateFactory.VoidDelegate(function()
        local r = self.m_RecreateDataList[1]
        self:OnCardClick(r.item, 1, r.isPrecious)
    end)
    
    --播放音效
    if self.m_Voice then
        SoundManager.Inst:StopSound(self.m_Voice)
    end

    if not jumpToResult then
        if not self.m_SkipAni then
            self.m_Voice = SoundManager.Inst:PlayOneShot("event:/L10/L10_UI/ZaiZaoGongNeng/UI_Rebuid_Single", Vector3.zero, nil, 0, -1)
        else
            self.m_Voice = SoundManager.Inst:PlayOneShot("event:/L10/L10_UI/ZaiZaoGongNeng/UI_Rebuid_Single_Skip", Vector3.zero, nil, 0, -1)
        end
    end
    
    UnRegisterTick(self.m_SoundTick)
    self.m_SoundTick = RegisterTickOnce(function()
        SoundManager.Inst:StopSound(self.m_Voice)
    end, 2000)

    if jumpToResult then
        self:FlopAllCard()
        self.EquipInfo_Style01:SetActive(true)
        self.m_MainAni:Play("recreateextraequipwnd_single_shuxing")
        return
    end
    
    if not self.m_SkipAni then
        self.m_MainAni:Play("recreateextraequipwnd_single")
    else
        self.m_MainAni:Play("recreateextraequipwnd_single_skip")
    end
    self.m_Full3DView = true
end

-- 初始化一张卡牌
-- 每次只在抽牌时候 初始化一次
function LuaRecreateExtraEquipWnd:InitCardInfo(item, id, data, index, originNum)
    local scoreLabel = item.transform:Find("Content/ScoreItem/ScoreLabel"):GetComponent(typeof(UILabel))
    local upMark = item.transform:Find("Content/ScoreItem/Up").gameObject
    local compareBtn = item.transform:Find("Content/ScoreItem/CompareBtn").gameObject
    local compareSp = item.transform:Find("Content/ScoreItem/Sprite")
    local attrLabel = item.transform:Find("Content/AttrItem/ScoreLabel"):GetComponent(typeof(UILabel))
    local attrBtn = item.transform:Find("Content/AttrItem/Btn").gameObject
    local wordTable = item.transform:Find("Content/WordItem/Panel/WordTable"):GetComponent(typeof(UITable))
    local scrollView = item.transform:Find("Content/WordItem/Panel"):GetComponent(typeof(CUIRestrictScrollView))

    local tuowei = item.transform:Find("CUIFxtuowei_kapai"):GetComponent(typeof(Animation))
    tuowei.gameObject:SetActive(false)

    -- 这里把卡牌合上
    item.transform:Find("Content").gameObject:SetActive(false)

    -- 恢复一下卡牌的大小
    item.transform.parent.localScale = Vector3(0.72, 0.72, 1)

    scoreLabel.text = SafeStringFormat3(LocalString.GetString("评分 %s"), tostring(data.TempScore))
    upMark:SetActive(data.TempScore > self.m_EquipData.Score)

    local wordList = {}

    local list = CreateFromClass(MakeGenericClass(List, UInt32))
    
    for i=0, data.TempFixedWordContents.Length-1 do
        local w = data.TempFixedWordContents[i]
        if w >0 then
            table.insert(wordList, w)
            CommonDefs.ListAdd(list, typeof(UInt32), w)
        end
    end

    for i=0, data.TempWordContents.Length-1 do
        local w = data.TempWordContents[i]
        if w >0 then
            table.insert(wordList, w)
            CommonDefs.ListAdd(list, typeof(UInt32), w)
        end
    end

    local isPrecious = CPreciousMgr.Inst:IsPreciousWord(list, self.m_TemplateId)

    if isPrecious then
        self.m_HasUnsetPrecious = true
    end

    local b = item.transform:Find("BlueCardBg").gameObject
    local y = item.transform:Find("YellowCardBg").gameObject

    y.transform:Find("YellowCardBackBg").gameObject:SetActive(true)
    y.transform:Find("YellowCardBackBg/yellow_bg").gameObject:SetActive(true)
    y.transform:Find("YellowCardBackBg/BackBg _vfx").gameObject:SetActive(true)

    b.transform:Find("BlueCardBackBg").gameObject:SetActive(true)
    b.transform:Find("BlueCardBackBg/blue_bg (1)").gameObject:SetActive(true)

    -- 反转状态复原
    b.transform.localRotation = Quaternion.Euler(0,0,0)
    b.transform:Find("BlueCardBackBg").localRotation = Quaternion.Euler(0,0,0)
    y.transform.localRotation = Quaternion.Euler(0,0,0)
    y.transform:Find("YellowCardBackBg").localRotation = Quaternion.Euler(0,0,0)

    b.transform:Find("FrontBg").gameObject:SetActive(false)
    b.transform:Find("Selected").gameObject:SetActive(false)
    y.transform:Find("FrontBg").gameObject:SetActive(false)
    y.transform:Find("Selected").gameObject:SetActive(false)

    -- 存起来
    self.m_RecreateDataList[index].isPrecious = isPrecious
    self.m_RecreateDataList[index].item = item

    b:SetActive(not isPrecious)
    y:SetActive(isPrecious)

    item.transform:Find("Content/AttrItem").gameObject:SetActive(false)
    Extensions.RemoveAllChildren(wordTable.transform)

    local changeList =  self.m_ChangeTbl and self.m_ChangeTbl[originNum]
    if changeList then
        local pList =  CreateFromClass(MakeGenericClass(List, PlayerShowDifProType))

        -- 基础属性判断一个就知道是上升还是下降了
        local isup = true
        for k,v in pairs(changeList) do
            local add = self.m_EquipData:GetPropertyDif(k,v)

            if v <0 then
                isup = false
            end
            if add then
                CommonDefs.ListAdd(pList, typeof(PlayerShowDifProType), add)
            end
        end

        if pList.Count > 0 and isup then
            local attrItem = item.transform:Find("Content/AttrItem").gameObject
            local g = NGUITools.AddChild(wordTable.gameObject, attrItem)
            g:SetActive(true)

            local attrBtn = g.transform:Find("Btn").gameObject
            -- 属性对比
            UIEventListener.Get(attrBtn).onClick = DelegateFactory.VoidDelegate(function()
                CPropertyDifMgr.ShowEquipDifWnd(pList)
            end)
        end
    end

    self:InitWord(self.m_ItemId, wordTable, self.WordItem, wordList, nil, true)
    scrollView:ResetPosition()

    local boxCollider = item.transform.parent:GetComponent(typeof(BoxCollider))

    if self.m_SkipAni then
        boxCollider.size = Vector3(370, 750, 0)
    else
        boxCollider.size = Vector3(370, 1300, 0)
    end

    -- 卡牌点击事件
    UIEventListener.Get(item.transform.parent.gameObject).onClick = DelegateFactory.VoidDelegate(function()
        self:OnCardClick(item, index, isPrecious)
    end)

    compareBtn:SetActive(self.m_ItemData.OnBody)
    if compareSp then
        compareSp.gameObject:SetActive(self.m_ItemData.OnBody)
    end

    local tempScore = data.TempScore
    -- 评分对比
    UIEventListener.Get(compareBtn).onClick = DelegateFactory.VoidDelegate(function()
        local mainPlace, mainPos = CLuaEquipMgr:GetItemPlaceAndPos(self.m_ItemId)
        CLuaEquipMgr.m_RecreateTempScore = tempScore
        Gac2Gas.TryConfirmRecreateExtraEquipWord(mainPlace, mainPos, self.m_ItemId, id)
    end)
end

function LuaRecreateExtraEquipWnd:SetWordState(item, selected)
    local wordTable = item.transform:Find("Content/WordItem/Panel/WordTable"):GetComponent(typeof(UITable))
    
    for i = 0, wordTable.transform.childCount -1 do
        local g = wordTable.transform:GetChild(i)

        if g.transform:Find("NameLabel") ~= nil then
            local nameLabel = g.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
            local descLabel = g.transform:Find("DescLabel"):GetComponent(typeof(UILabel))
            local bg = g.transform:Find("Bg"):GetComponent(typeof(UISprite))

            if selected then
                nameLabel.gameObject:SetActive(true)
                descLabel.fontSize = 28
                descLabel.mWidth = 240
                
                bg.leftAnchor.absolute = -109
                bg.rightAnchor.absolute = 11
            else
                nameLabel.gameObject:SetActive(false)
                descLabel.fontSize = 34
                descLabel.mWidth = 343

                bg.leftAnchor.absolute = -10
                bg.rightAnchor.absolute = -107
            end
        end
    end
end

-- 卡牌点击事件 可能情况有两种
function LuaRecreateExtraEquipWnd:OnCardClick(item, index, isPrecious)
    SoundManager.Inst:PlayOneShot("event:/L10/L10_UI/ZaiZaoGongNeng/UI_Rebuid_Five_Flip", Vector3.zero, nil, 0, -1)
    
    if self.m_Full3DView then
        -- 点击之后跳入属性选择界面
        self.m_Full3DView = false
        -- 初始时候置灰替换按钮
        CUICommonDef.SetActive(self.ReplaceBtn, true, true)
        self.EquipInfo_Style01:SetActive(true)

        if self.m_IsWuLian then
            self.m_MainAni:Play("recreateextraequipwnd_multi_shuxing")
        else
            self.m_MainAni:Play("recreateextraequipwnd_single_shuxing")
        end

        -- 延迟变成2D界面
        -- 但是点击事件不能延迟
        self.Bg.gameObject:SetActive(false)

        UnRegisterTick(self.m_Tick)
        self.m_Tick = RegisterTickOnce(function()
            self:Set3DLayer(false)
        end, 1000)

        -- 可能需要延迟打开所有的卡牌 并且调整位置
        self:SetSelectCard(item, index, isPrecious)
        self:FlopCard()
    else
        -- 普通的选择
        self:SetSelectCard(item, index, isPrecious)
    end
end

-- 翻开所有卡 为了快速显示
function LuaRecreateExtraEquipWnd:FlopAllCard()
    self.m_Full3DView = false
    -- 初始时候置灰替换按钮
    CUICommonDef.SetActive(self.ReplaceBtn, true, true)

    UnRegisterTick(self.m_Tick)
    self:Set3DLayer(false)

    for i =1, #self.m_RecreateDataList do
        local r = self.m_RecreateDataList[i]
        local ani = r.item.transform:GetComponent(typeof(Animation))

        if i == 1 then
            self:SetSelectCard(r.item, 1, r.isPrecious)
        end

        if not r.isPrecious then
            ani:Play("recreateextraequipwnd_kapai01")
            self.m_FlopList[i] = true
        else
            ani:Play("recreateextraequipwnd_kapai02")
            self.m_FlopList[i] = true
        end
    end
end

function LuaRecreateExtraEquipWnd:StopFloping()
    local list = {}
    table.insert(list, self.Single.transform:Find("1/1"))
    for i =1, 5 do
        table.insert(list, self.Multi.transform:Find(i.."/" ..i))
    end

    for _, item in ipairs(list) do
        item.transform:GetComponent(typeof(Animation)):Stop()
    end

    if self.m_TweenList then
        for _, t in ipairs(self.m_TweenList) do
            LuaTweenUtils.Kill(t, false)
        end
    end
end

function LuaRecreateExtraEquipWnd:FlopCard()
    for i =1, #self.m_RecreateDataList do
        local r = self.m_RecreateDataList[i]
        local ani = r.item.transform:GetComponent(typeof(Animation))

        if not r.isPrecious then
            ani:Play("recreateextraequipwnd_kapai01")
            self.m_FlopList[i] = true
        elseif i == self.m_CurrentSelect then
            ani:Play("recreateextraequipwnd_kapai02")
            self.m_FlopList[i] = true
        end
    end
end

function LuaRecreateExtraEquipWnd:Update()
    if self.m_WordTableList then
        for i = 1, #self.m_WordTableList do
            if self.m_WordTableList[i] and self.m_WordTableList[i].gameObject.activeSelf then
                self.m_WordTableList[i]:Reposition()
            end
        end
    end

    if self.MainWordTable and self.MainWordTable.gameObject.activeSelf then
        self.MainWordTable:Reposition()
    end
end

-- 设置当前被选中的卡牌
function LuaRecreateExtraEquipWnd:SetSelectCard(item, index, isPrecious)
    self.m_CurrentSelect = index
    -- 选中卡牌之后 才允许再次再造
    self.m_CommitMark = false

    UnRegisterTick(self.m_ShowSelectTick)

    for i =1, #self.m_RecreateDataList do
        local r = self.m_RecreateDataList[i]
        local bSelect = r.item.transform:Find("BlueCardBg/Selected").gameObject
        local ySelect = r.item.transform:Find("YellowCardBg/Selected").gameObject
        local ani = r.item.transform:GetComponent(typeof(Animation))
            
        bSelect:SetActive(false)
        ySelect:SetActive(false)
        
        self:SetWordState(r.item, i == self.m_CurrentSelect)

        local widgetList = CommonDefs.GetComponentsInChildren_Component_Type_Boolean(r.item.transform, typeof(UIWidget), true)
        
        for j =0, widgetList.Length-1 do
            local w = widgetList[j]
            if i == self.m_CurrentSelect and w.depth < 100 then
                w.depth = w.depth + 100
            elseif i ~= self.m_CurrentSelect and w.depth >= 100 then
                w.depth = w.depth - 100
            end
        end

        if i == self.m_CurrentSelect then
            r.item.transform.parent.localScale = Vector3(1, 1, 1)
            -- 如果没翻开 进行翻开操作
            if not self.m_FlopList[i] then

                UnRegisterTick(self.m_ShowSelectTick)
                self.m_ShowSelectTick = RegisterTickOnce(function()
                    bSelect:SetActive(true)
                    ySelect:SetActive(true)
                end, 500)

                if not r.isPrecious then
                    ani:Play("recreateextraequipwnd_kapai01")
                else
                    ani:Play("recreateextraequipwnd_kapai02")
                end

                self.m_FlopList[i] = true
            else
                bSelect:SetActive(true)
                ySelect:SetActive(true)
            end
        else
            r.item.transform.parent.localScale = Vector3(0.72, 0.72, 1)
        end
    end

    self:CalMutilPos()
end

function LuaRecreateExtraEquipWnd:OnConfirm()
    -- 变灰替换按钮
    CUICommonDef.SetActive(self.ReplaceBtn, false, true)

    -- 播放替换特效
    self.CUIFxtuowei.gameObject:SetActive(true)
    self.CUIFxtuowei:Play("recreateextraequipwnd_guiji")
    
    local r = self.m_RecreateDataList[self.m_CurrentSelect]
    if r then
        local ani = r.item.transform:Find("CUIFxtuowei_kapai"):GetComponent(typeof(Animation))
        ani.gameObject:SetActive(true)
        ani:Play("recreateextraequipwnd_guiji_1")
    end

    self.EquipInfo_Style01:SetActive(false)
    self.m_HasUnsetPrecious = false
    self.m_HasUnsetResult = false

    UnRegisterTick(self.m_RefreshItemTick)
    self.m_ConfirmTick = RegisterTickOnce(function()
        self:RefreshItem()
    end, 300)

    UnRegisterTick(self.m_ConfirmTick)
    self.m_ConfirmTick = RegisterTickOnce(function()
        self:RefreshItem()
        self.Single:PlayDisapperAnimation(nil)
        self.Multi:PlayDisapperAnimation(nil)
        self.EquipInfo_Style02:Play("recreateextraequipwnd_info_show")
    end, 500)

    UnRegisterTick(self.m_ConfirmShowTick)
    self.m_ConfirmShowTick = RegisterTickOnce(function()
        self.CUIFxtuowei.gameObject:SetActive(false)
    end, 2500)
end

function LuaRecreateExtraEquipWnd:RefreshItem()
    local item = CItemMgr.Inst:GetById(self.m_ItemId)
    self.m_EquipData = item.Equip
    self.m_TemplateId = self.m_EquipData.TemplateId
    self.m_TemplateData = EquipmentTemplate_Equip.GetData(self.m_TemplateId)
    self.m_IsBind = self.m_EquipData.IsBinded

    self:InitEquipInfo()
    self:InitWuLianState()
    self:InitMainWords()
end

function LuaRecreateExtraEquipWnd:OnShowDif(args)
    local obj = args[0]
    local list = TypeAs(obj, typeof(MakeGenericClass(List, PlayerShowDifProType)))
    if list == nil then
        return
    end
    if list.Length == 0 then
        g_MessageMgr:ShowMessage("BaptizeWordResultNoPropDif")
        return
    end

    CPropertyDifMgr.ShowPlayerProDifWithEquip(list)
end

function LuaRecreateExtraEquipWnd:UpdateItem(args)
    self:InitWuLianState()
end

function LuaRecreateExtraEquipWnd:OnEnable()
    CMainCamera.Inst:SetCameraEnableStatus(false, "RecreateExtraEquipWnd", false)

    g_ScriptEvent:AddListener("SendItem", self, "UpdateItem")
	g_ScriptEvent:AddListener("SetItemAt", self, "UpdateItem")
    g_ScriptEvent:AddListener("RecreateExtraEquipResultConfirmResult", self, "OnConfirm")
    g_ScriptEvent:AddListener("RecreateExtraEquipResult", self, "OnRecreateExtraEquipResult")
    g_ScriptEvent:AddListener("TryConfirmBaptizeWordResult", self, "OnShowDif")
end

function LuaRecreateExtraEquipWnd:OnDisable()
    CMainCamera.Inst:SetCameraEnableStatus(true, "RecreateExtraEquipWnd", false)

    UnRegisterTick(self.m_Tick)
    UnRegisterTick(self.m_ConfirmTick)
    UnRegisterTick(self.m_ConfirmTick)
    UnRegisterTick(self.m_ConfirmShowTick)
    UnRegisterTick(self.m_RefreshItemTick)
    UnRegisterTick(self.m_SoundTick)

    --播放音效
    if self.m_Voice then
        SoundManager.Inst:StopSound(self.m_Voice)
    end

    CLuaEquipMgr.m_OpenUnsetRecreate = false
    g_ScriptEvent:RemoveListener("SendItem", self, "UpdateItem")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "UpdateItem")
    g_ScriptEvent:RemoveListener("RecreateExtraEquipResultConfirmResult", self, "OnConfirm")
    g_ScriptEvent:RemoveListener("RecreateExtraEquipResult", self, "OnRecreateExtraEquipResult")
    CUIManager.DestroyModelTexture("__recreateExtraEquip__")
    g_ScriptEvent:AddListener("TryConfirmBaptizeWordResult", self, "OnShowDif")
end

--@region UIEvent

function LuaRecreateExtraEquipWnd:OnReplaceBtnClick()
    if not self.m_CommitMark and self.m_CurrentSelect > 0 then
        local mainPlace, mainPos = CLuaEquipMgr:GetItemPlaceAndPos(self.m_ItemId)
        local r = self.m_RecreateDataList[self.m_CurrentSelect]
        if r then
            Gac2Gas.RecreateExtraEquipResultConfirm(mainPlace, mainPos, self.m_ItemId, true, r.id)
        end
    end
end

function LuaRecreateExtraEquipWnd:OnTipBtnClick()
    g_MessageMgr:ShowMessage("Equip_Recreate_Wnd_Tip")
end

function LuaRecreateExtraEquipWnd:OnShareBtnClick()
    CUICommonDef.CaptureScreenAndShare()
end

function LuaRecreateExtraEquipWnd:OnCommitBtnClick()
    -- 材料不足
    if self:CheckItem() == false then
        g_MessageMgr:ShowMessage("Equip_Recreate_Material_Not_Enough")
        return
    end

    if not self.m_CommitMark and self.m_HasUnsetPrecious then
        -- 有金色卡牌尚未选择
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("Equip_Recreate_Precious_Unchoose_Tip"), function ()
            -- 检查
            CLuaEquipMgr:CheckUnsetComposite(self.m_ItemId, function()
                CLuaEquipMgr:CheckUnsetBaptize(self.m_ItemId, function()
                    self.m_CommitMark = true
                    local mainPlace, mainPos = CLuaEquipMgr:GetItemPlaceAndPos(self.m_ItemId)
                    Gac2Gas.RequestRecreateExtraEquip(mainPlace, mainPos, self.m_ItemId, self.m_IsWuLian, self.m_OnlyBind)
                end)
            end)  
        end, nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
    elseif not self.m_CommitMark then
        -- 检查
		CLuaEquipMgr:CheckUnsetComposite(self.m_ItemId, function()
			CLuaEquipMgr:CheckUnsetBaptize(self.m_ItemId, function()
                self.m_CommitMark = true
                local mainPlace, mainPos = CLuaEquipMgr:GetItemPlaceAndPos(self.m_ItemId)
                Gac2Gas.RequestRecreateExtraEquip(mainPlace, mainPos, self.m_ItemId, self.m_IsWuLian, self.m_OnlyBind)
			end)
		end)
    end
end


function LuaRecreateExtraEquipWnd:OnCloseButtonClick()
    if self.m_HasUnsetResult then
        -- 有再造结果未选择
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("Equip_Recreate_Result_Unchoose_Exit_Tip"), function ()
            CUIManager.CloseUI(CLuaUIResources.RecreateExtraEquipWnd)
        end, nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
    else
        CUIManager.CloseUI(CLuaUIResources.RecreateExtraEquipWnd)
    end
end


--@endregion UIEvent

