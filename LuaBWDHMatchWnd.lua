

local CXialvPKMgr = import "L10.Game.CXialvPKMgr"
local CQMJJMgr = import "L10.Game.CQMJJMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CBiWuDaHuiMgr = import "L10.Game.CBiWuDaHuiMgr"
local Constants = import "L10.Game.Constants"
local QnRadioBox=import "L10.UI.QnRadioBox"
local QnTableView=import "L10.UI.QnTableView"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"

CLuaBWDHMatchWnd = class()
RegistClassMember(CLuaBWDHMatchWnd,"group")
RegistClassMember(CLuaBWDHMatchWnd,"infoLabel")
RegistClassMember(CLuaBWDHMatchWnd,"tableView")
RegistClassMember(CLuaBWDHMatchWnd,"winList")
RegistClassMember(CLuaBWDHMatchWnd,"loseList")

function CLuaBWDHMatchWnd:Awake()
    self.group = self.transform:Find("Anchor/GroupTable/Table"):GetComponent(typeof(QnRadioBox))
    self.infoLabel = self.transform:Find("Anchor/InfoLabel"):GetComponent(typeof(UILabel))
    self.tableView = self.transform:Find("Anchor/InfoTable"):GetComponent(typeof(QnTableView))
    self.winList = {}
    self.loseList = {}

    self.group:ChangeTo(0, true)
    self.group.OnSelect = DelegateFactory.Action_QnButton_int(function()
        self.tableView:ReloadData(true,false)
    end)

    self.tableView.m_DataSource = DefaultTableViewDataSource.Create2(function()
        if self.group.CurrentSelectIndex == 0 then
            return self.winList and #self.winList or 0
        elseif self.group.CurrentSelectIndex == 1 then
            return self.loseList and #self.loseList or 0
        end
        return 0
    end, function(item, row)
        if self.group.CurrentSelectIndex == 0 then
            if self.winList then
                if row < #self.winList then
                    local item = self.tableView:GetFromPool(0)
                    if item ~= nil then
                        self:InitItem(item,self.winList[row+1], row)
                        return item
                    end
                end
            end
        elseif self.group.CurrentSelectIndex == 1 then
            if self.loseList then
                if row < #self.loseList then
                    local item =self.tableView:GetFromPool(0)
                    if item ~= nil then
                        self:InitItem(item,self.loseList[row+1], row)
                        return item
                    end
                end
            end
        end
        return nil
    end)
end

function CLuaBWDHMatchWnd:Init( )
    if CXialvPKMgr.Inst.InXialvPKPrepare then
        self.infoLabel.text = SafeStringFormat3(LocalString.GetString("现在侠侣PK擂台赛上的队伍数量还剩%d支"), 0)
    else
        self.infoLabel.text = SafeStringFormat3(LocalString.GetString("现在比武大会上的队伍数量还剩%d支"), 0)
    end
    if CBiWuDaHuiMgr.Inst.inBiWu or CBiWuDaHuiMgr.Inst.inBiWuPrepare then
        Gac2Gas.QueryBiWuStageMatchInfo()
    elseif CQMJJMgr.Inst.inQMJJ or CQMJJMgr.Inst.inQMJJPrepare then
        Gac2Gas.QueryQMJJStageMatchInfo()
    elseif CXialvPKMgr.Inst.InXialvPKPrepare then
        if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.IsCrossServerPlayer then
            Gac2Gas.CrossXiaLvPkQueryStageMatchInfo()
        else
            Gac2Gas.XiaLvPkQueryStageMatchInfo()
        end
    end
end

function CLuaBWDHMatchWnd:OnQueryBiWuStageMatchInfoResult( list) 
    local count = 0
    self.winList = {}
    self.loseList = {}

    for i,item in ipairs(list) do
        count = count + item.teamCount
        if item.group == 1 then
            table.insert( self.winList,item )
        elseif item.group == 2 then
            table.insert( self.loseList,item )
        end
    end

    if CXialvPKMgr.Inst.InXialvPKPrepare then
        self.infoLabel.text = SafeStringFormat3(LocalString.GetString("现在侠侣PK擂台赛上的队伍数量还剩%d支"), count)
    else
        self.infoLabel.text = SafeStringFormat3(LocalString.GetString("现在比武大会上的队伍数量还剩%d支"), count)
    end


    local function sortFunc(p1,p2)
        local hasResult1 = not (p1.winTeamName==nil or p1.winTeamName=="")
        local hasResult2 = not (p2.winTeamName==nil or p2.winTeamName=="")

        if p1.hasMyTeam and not p2.hasMyTeam then
            return true
        elseif not p1.hasMyTeam and p2.hasMyTeam then
            return false
        else
            if hasResult1 and not hasResult2 then
                return true
            elseif not hasResult1 and hasResult2 then
                return false
            else
                return p1.index<p2.index
            end
        end
    end
    table.sort( self.winList,sortFunc)

    self.tableView:ReloadData(true, false)
end

function CLuaBWDHMatchWnd:OnEnable()
    g_ScriptEvent:AddListener("QueryBiWuStageMatchInfoResult", self, "OnQueryBiWuStageMatchInfoResult")
end
function CLuaBWDHMatchWnd:OnDisable()
    g_ScriptEvent:RemoveListener("QueryBiWuStageMatchInfoResult", self, "OnQueryBiWuStageMatchInfoResult")
end

function CLuaBWDHMatchWnd:InitItem(cmp,info,index)
    local transform = cmp.transform
    local team1Label = transform:Find("Team1Label"):GetComponent(typeof(UILabel))
    local team2Label = transform:Find("Team2Label"):GetComponent(typeof(UILabel))
    local resultTf = transform:Find("Result")
    local noResultTf = transform:Find("NoResult")

    if info.team1Name==nil or info.team1Name=="" then
        team1Label.text = LocalString.GetString("轮空")
    else
        team1Label.text = info.team1Name
    end

    if info.team2Name==nil or info.team2Name=="" then
        team2Label.text = LocalString.GetString("轮空")
    else
        team2Label.text = info.team2Name
    end

    if index % 2 == 0 then
        cmp:SetBackgroundTexture(Constants.NewEvenBgSprite)
    else
        cmp:SetBackgroundTexture(Constants.NewOddBgSprite)
    end

    if info.hasMyTeam then
        cmp:SetBackgroundTexture("mission_background_s")
    end

    local resultSprite1 = resultTf:GetChild(0):GetComponent(typeof(UISprite))
    local resultSprite2 = resultTf:GetChild(1):GetComponent(typeof(UISprite))
    if not (info.winTeamName==nil or info.winTeamName=="") then
        resultTf.gameObject:SetActive(true)
        noResultTf.gameObject:SetActive(false)
        if info.winTeamName == info.team1Name then
            resultSprite1.spriteName = Constants.WinSprite
            resultSprite2.spriteName = Constants.LoseSprite
        else
            resultSprite1.spriteName = Constants.LoseSprite
            resultSprite2.spriteName = Constants.WinSprite
        end
    else
        resultTf.gameObject:SetActive(false)
        noResultTf.gameObject:SetActive(true)
    end
    if info.invalid then--都失败
        resultSprite1.spriteName = Constants.LoseSprite
        resultSprite2.spriteName = Constants.LoseSprite
    end
end

