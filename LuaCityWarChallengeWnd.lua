require("common/common_include")

local UILabel = import "UILabel"
local UIEventListener = import "UIEventListener"
local LuaUtils = import "LuaUtils"
local QnButton = import "L10.UI.QnButton"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local AlignType3 = import "L10.UI.CPopupMenuInfoMgr+AlignType"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"

CLuaCityWarChallengeWnd = class()
CLuaCityWarChallengeWnd.Path = "ui/citywar/LuaCityWarChallengeWnd"

RegistClassMember(CLuaCityWarChallengeWnd, "m_CityInfoRootObj")
RegistClassMember(CLuaCityWarChallengeWnd, "m_SelectedGuildId")
RegistClassMember(CLuaCityWarChallengeWnd, "m_SelectedGuildName")
RegistClassMember(CLuaCityWarChallengeWnd, "m_MapObj2SelectedBgObj")
RegistClassMember(CLuaCityWarChallengeWnd, "m_LastSelectedObj")
RegistClassMember(CLuaCityWarChallengeWnd, "m_OwnerId2InfoTable")
RegistClassMember(CLuaCityWarChallengeWnd, "m_TickTable")
RegistClassMember(CLuaCityWarChallengeWnd, "m_StatusLabelTable")
RegistClassMember(CLuaCityWarChallengeWnd, "m_TimeTable")
RegistClassMember(CLuaCityWarChallengeWnd, "m_StatusTable")
RegistClassMember(CLuaCityWarChallengeWnd, "m_ChallengeQnBtn")
RegistClassMember(CLuaCityWarChallengeWnd, "m_MyGuildId")
RegistClassMember(CLuaCityWarChallengeWnd, "m_EnterBtnObj")

RegistClassMember(CLuaCityWarChallengeWnd, "m_RankRootObj")
RegistClassMember(CLuaCityWarChallengeWnd, "m_RankTick")

function CLuaCityWarChallengeWnd:Init( ... )
	local mapId2Trans = {}
	local myMapId = CLuaCityWarMgr.WarMapInfoMyMapId
	local tranNameTable = {"Top", "Bottom", "Left", "Right", "Center"}
	local map = CityWar_Map.GetData(myMapId)
	local mapIdTable = {map.NorthMap, map.SouthMap, map.WestMap, map.EastMap, myMapId}
	for i = 1, #tranNameTable do
		local trans = self.transform:Find("Anchor/"..tranNameTable[i])
		trans.gameObject:SetActive(mapIdTable[i] > 0)
		if mapIdTable[i] > 0 then
			mapId2Trans[mapIdTable[i]] = trans
			trans:Find("Label"):GetComponent(typeof(UILabel)).text = CityWar_Map.GetData(mapIdTable[i]).Level
			trans:Find("GuildInfo").gameObject:SetActive(false)
		end
	end

	self.m_MapObj2SelectedBgObj = {}
	self.m_OwnerId2InfoTable = {}
	self.m_TickTable = {}
	self.m_StatusLabelTable = {}
	self.m_TimeTable = {}
	self.m_StatusTable = {}

	local myGuildState = 1

	local dict = MsgPackImpl.unpack(CLuaCityWarMgr.WarMapInfoMapInfoUD)
	CommonDefs.DictIterate(dict, DelegateFactory.Action_object_object(function(key,val)
		local mapId = tonumber(key)
		local templateId = val[0]
		local ownerId = val[1]
		local ownerName = val[2]
		local grade = val[3]
		local unitNum = val[4]
		local material = val[5]
		local fightState = val[6]
		local duration = val[7]

		if mapId == myMapId then
			myGuildState = fightState
			self.m_MyGuildId = ownerId
		end

		self.m_OwnerId2InfoTable[ownerId] = {mapId, templateId, ownerName, grade, unitNum, material}
		local trans = mapId2Trans[mapId]
		if trans then
			local guildInfoTrans = trans:Find("GuildInfo")
			guildInfoTrans.gameObject:SetActive(true)
			guildInfoTrans:Find("Name"):GetComponent(typeof(UILabel)).text = mapId == myMapId and LocalString.GetString("自己的帮会") or ownerName

			local statusLabel = guildInfoTrans:Find("Status"):GetComponent(typeof(UILabel))
			local map = CityWar_Map.GetData(mapId)
			if map.Level > 1 or mapId == myMapId then
				local status = CLuaCityWarMgr.StatusNameTable[fightState]
				if fightState == 1 then
					if map.Level < 2 then
						status = status[1]
					else
						status = status[2]
					end
				end
				statusLabel.text = status
				if fightState == 9 or fightState == 6 or fightState == 4 then
					table.insert(self.m_StatusLabelTable, statusLabel)
					table.insert(self.m_TimeTable, duration)
					table.insert(self.m_StatusTable, fightState)
					self:InitTick(#self.m_StatusLabelTable)
				end
			else
				statusLabel.text = ""
			end

			local bgObj = trans:Find("Bg").gameObject
			local selectedBgObj = trans:Find("SelectedBg").gameObject
			selectedBgObj:SetActive(false)
			self.m_MapObj2SelectedBgObj[bgObj] = selectedBgObj
			UIEventListener.Get(bgObj).onClick = LuaUtils.VoidDelegate(function (go)
				self:OnMapClick(go, ownerId, ownerName)
			end)
		end
	end))

	self.m_CityInfoRootObj = self.transform:Find("CityInfoRoot").gameObject
	self.m_CityInfoRootObj:SetActive(false)
	local challengeObj = self.transform:Find("Anchor/ChallengeBtn").gameObject
	self.m_ChallengeQnBtn = challengeObj:GetComponent(typeof(QnButton))
	self.m_ChallengeQnBtn.Enabled = false
	UIEventListener.Get(challengeObj).onClick = LuaUtils.VoidDelegate(function ( ... )
		if self.m_SelectedGuildId and self.m_OwnerId2InfoTable[self.m_SelectedGuildId] then
			local info = self.m_OwnerId2InfoTable[self.m_SelectedGuildId]
			CLuaCityWarMgr.RequestDeclareMirrorWar(self.m_SelectedGuildId, self.m_SelectedGuildName, info[1], info[2])
		end
	end)

	self:InitButton(myGuildState)
end

function CLuaCityWarChallengeWnd:InitButton(state)
	self.m_ChallengeQnBtn.gameObject:SetActive(state ~= 5 and not CLuaCityWarMgr.WarMapInfoIsMatchPeriod)
	self.m_EnterBtnObj = self.transform:Find("Anchor/EnterCopyScene").gameObject
	self.m_EnterBtnObj:SetActive(state == 5 and not CLuaCityWarMgr.WarMapInfoIsMatchPeriod)
	if state == 5 then
		UIEventListener.Get(self.m_EnterBtnObj).onClick = LuaUtils.VoidDelegate(function (go)
			Gac2Gas.QueryMirrorWarPlayNum()
		end)
	end

	UIEventListener.Get(self.transform:Find("Anchor/TipBtn").gameObject).onClick = LuaUtils.VoidDelegate(function (go)
		g_MessageMgr:ShowMessage("CITYWAR_XUANZHAN_QUEUE_INTERFACE_HELP_TIP")
	end)

	self.m_RankRootObj = self.transform:Find("Anchor/RankRoot").gameObject
	self.m_RankRootObj:SetActive(CLuaCityWarMgr.WarMapInfoIsMatchPeriod)
	if CLuaCityWarMgr.WarMapInfoIsMatchPeriod then
		UIEventListener.Get(self.m_RankRootObj.transform:Find("RankBtn").gameObject).onClick = LuaUtils.VoidDelegate(function (go)
			if not self.m_SelectedGuildId then
				g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("请先选择一个帮会领土"))
				return
			end
			Gac2Gas.QueryMirrorWarPriorityMatchGroup(self.m_SelectedGuildId)
		end)
		local label = self.m_RankRootObj.transform:Find("TipLabel"):GetComponent(typeof(UILabel))
		local time = CLuaCityWarMgr.WarMapInfoMatchLeftTime
		label.text = LocalString.GetString("宣战排队期 ")..SafeStringFormat3(LocalString.GetString("%02d:%02d"), math.floor(time / 60), math.floor(time % 60))
		if self.m_RankTick then
			UnRegisterTick(self.m_RankTick)
			self.m_RankTick = nil
		end
		self.m_RankTick = RegisterTickWithDuration(function()
			time = time - 1
			label.text = LocalString.GetString("宣战排队期 ")..SafeStringFormat3(LocalString.GetString("%02d:%02d"), math.floor(time / 60), math.floor(time % 60))
		end, 1000, time * 1000)
	end
end

function CLuaCityWarChallengeWnd:EnterCopyScene(index)
	Gac2Gas.RequestEnterMirrorWarPlay(index)
end

function CLuaCityWarChallengeWnd:OnMapClick(go, ownerId, ownerName)
	if self.m_LastSelectedObj then self.m_MapObj2SelectedBgObj[self.m_LastSelectedObj]:SetActive(false) end
	self.m_LastSelectedObj = go
	self.m_MapObj2SelectedBgObj[go]:SetActive(true)
	self.m_SelectedGuildId = ownerId
	self.m_SelectedGuildName = ownerName
	self:InitInfo(ownerId)
	self.m_ChallengeQnBtn.Enabled = ownerId ~= self.m_MyGuildId
end

function CLuaCityWarChallengeWnd:InitInfo(ownerId)
	local info = self.m_OwnerId2InfoTable[ownerId]
	if not info then return end
	self.m_CityInfoRootObj:SetActive(true)
	local transNameTable = {"Region", "Guild", "Grade", "Material", "BuildingCount"}
	local valueTable = {CityWar_Map.GetData(info[1]).Name, info[3], info[4], info[6], info[5]}
	for i = 1, #transNameTable do
		self.m_CityInfoRootObj.transform:Find(transNameTable[i].."/"..transNameTable[i]):GetComponent(typeof(UILabel)).text = valueTable[i]
	end
	self.m_CityInfoRootObj.transform:Find("Name"):GetComponent(typeof(UILabel)).text = CityWar_MapCity.GetData(info[2]).Name
	UIEventListener.Get(self.m_CityInfoRootObj.transform:Find("MapBtn").gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
		CLuaCityWarMgr:OpenSecondaryMap(info[1])
	end)
end

function CLuaCityWarChallengeWnd:UpdateTime(time, index)
	local h, m, s = math.floor(time / 3600), math.floor((time % 3600) / 60), math.floor(time % 60)
	self.m_StatusLabelTable[index].text = CLuaCityWarMgr.StatusNameTable[self.m_StatusTable[index]]..SafeStringFormat3(LocalString.GetString("(%02d:%02d:%02d)"), h, m, s)
end

function CLuaCityWarChallengeWnd:InitTick(index)
	self:UpdateTime(self.m_TimeTable[index], index)
	local tick = RegisterTickWithDuration(function ()
		self.m_TimeTable[index] = self.m_TimeTable[index] - 1
		self:UpdateTime(self.m_TimeTable[index], index)
	end, 1000, self.m_TimeTable[index] * 1000)
	table.insert(self.m_TickTable, tick)
end

function CLuaCityWarChallengeWnd:OnDestroy( ... )
	for i = 1, #self.m_TickTable do
		UnRegisterTick(self.m_TickTable[i])
		self.m_TimeTable[i] = 1
	end
	if self.m_RankTick then
		UnRegisterTick(self.m_RankTick)
		self.m_RankTick = nil
	end
end

function CLuaCityWarChallengeWnd:QueryMirrorWarPlayNumResult( playNum )
	local popupMenuItemTable = {}
	local indexNameTbl = {LocalString.GetString("一"), LocalString.GetString("二"), LocalString.GetString("三")}
	for i = 1, playNum do
		table.insert(popupMenuItemTable, PopupMenuItemData(LocalString.GetString("战场")..indexNameTbl[i], DelegateFactory.Action_int(function (index)
			self:EnterCopyScene(i)
			end), false, nil, EnumPopupMenuItemStyle.Default))
	end
	local popupMenuItemArray = Table2Array(popupMenuItemTable, MakeArrayClass(PopupMenuItemData))
	CPopupMenuInfoMgr.ShowPopupMenu(popupMenuItemArray, self.m_EnterBtnObj.transform, AlignType3.Top, 1, nil, 600, true, 227)
end

function CLuaCityWarChallengeWnd:OnEnable( ... )
	g_ScriptEvent:AddListener("QueryMirrorWarPlayNumResult", self, "QueryMirrorWarPlayNumResult")
end

function CLuaCityWarChallengeWnd:OnDisable( ... )
	g_ScriptEvent:RemoveListener("QueryMirrorWarPlayNumResult", self, "QueryMirrorWarPlayNumResult")
end
