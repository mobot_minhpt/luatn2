require("3rdParty/ScriptEvent")
require("common/common_include")
local CMainCamera = import "L10.Engine.CMainCamera"
--local Gac2Gas = import "L10.Game.Gac2Gas"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CUIManager = import "L10.UI.CUIManager"
local CUIRes = import "L10.UI.CUIResources"
local RenderTexture = import "UnityEngine.RenderTexture"
local Screen = import "UnityEngine.Screen"
local RenderTextureFormat = import "UnityEngine.RenderTextureFormat"
local RenderTextureReadWrite = import "UnityEngine.RenderTextureReadWrite"
local TextureWrapMode = import "UnityEngine.TextureWrapMode"
local FilterMode = import "UnityEngine.FilterMode"
local Camera = import "UnityEngine.Camera"
local GameObject = import "UnityEngine.GameObject"
local CameraClearFlags = import "UnityEngine.CameraClearFlags"
local LayerDefine = import "L10.Engine.LayerDefine"
local Quaternion = import "UnityEngine.Quaternion"
local Vector3 = import "UnityEngine.Vector3"
local CPreDrawMgr = import "L10.Engine.CPreDrawMgr"
local KitePlayMgr = import "L10.Game.KitePlayMgr"
local TweenPosition = import "TweenPosition"
local TweenRotation = import "TweenRotation"
local CRenderScene = import "L10.Engine.Scene.CRenderScene"

local function split(str, pat)
   local t = {}  -- NOTE: use {n = 0} in Lua-5.0
   local fpat = "(.-)" .. pat
   local last_end = 1
   local s, e, cap = str:find(fpat, 1)
   while s do
      if s ~= 1 or cap ~= "" then
         table.insert(t,cap)
      end
      last_end = e+1
      s, e, cap = str:find(fpat, last_end)
   end
   if last_end <= #str then
      cap = str:sub(last_end)
      table.insert(t, cap)
   end
   return t
end

LuaHandkerchiefFlyWnd=class()
RegistClassMember(LuaHandkerchiefFlyWnd,"CloseBtn")
RegistClassMember(LuaHandkerchiefFlyWnd,"ShowTexture")
RegistClassMember(LuaHandkerchiefFlyWnd,"LeftVSNode")

local cameraNode = nil
local handkerchiefNode = nil
local selfPlayerId = 0
local flyPosTable = {}

function LuaHandkerchiefFlyWnd:OnEnable()
	--g_ScriptEvent:AddListener("UpdateKitePos", self, "UpdatePos")
	--g_ScriptEvent:AddListener("AddNewKite", self, "InitPos")
	--g_ScriptEvent:AddListener("DeleteKite", self, "DeletePlayerKite")
  if CRenderScene.Inst then
    CRenderScene.Inst.ShowAllObject = true
  end
end

function LuaHandkerchiefFlyWnd:OnDisable()
	--g_ScriptEvent:RemoveListener("UpdateKitePos", self, "UpdatePos")
	--g_ScriptEvent:RemoveListener("AddNewKite", self, "InitPos")
	--g_ScriptEvent:RemoveListener("DeleteKite", self, "DeletePlayerKite")
  if CRenderScene.Inst then
    CRenderScene.Inst.ShowAllObject = false
  end
end

function LuaHandkerchiefFlyWnd:Update()

end

function LuaHandkerchiefFlyWnd:InitCamera(fatherNode)
	if not fatherNode then
		return
	end

	CPreDrawMgr.m_bEnableRectPreDraw = false

	if 1920 / Screen.width > 1080 / Screen.height then
		self.ShowTexture.width = 1920--Screen.width
		self.ShowTexture.height = Screen.height * 1920 / Screen.width--Screen.height
	else
		self.ShowTexture.width = Screen.width * 1080 / Screen.height--Screen.width
		self.ShowTexture.height = 1080--Screen.height
	end

	local renderTex = RenderTexture(self.ShowTexture.width,self.ShowTexture.height,32,RenderTextureFormat.ARGB32,RenderTextureReadWrite.Default)
	renderTex.wrapMode = TextureWrapMode.Clamp
	renderTex.filterMode = FilterMode.Bilinear
	renderTex.anisoLevel = 2
	--local TargetCamera = self.TargetCameraNode:GetComponent(typeof(Camera))
	self.ShowTexture.mainTexture = renderTex

	cameraNode = GameObject("__HandkerchiefFlyCamera__")
	local cameraScript = CommonDefs.AddCameraComponent(cameraNode)
    CMainCamera.Inst:SetCameraEnableStatus(false,"use_another_camera", false)
	cameraScript.clearFlags = CameraClearFlags.Skybox
  --cameraScript.backgroundColor = Color(49 / 255, 77 / 255, 121 / 255, 5 / 255)
  cameraScript.cullingMask = 2^LayerDefine.ModelForNGUI_3D + 2^LayerDefine.Effect_3D + 2^LayerDefine.Default + 2^LayerDefine.Water + 2^LayerDefine.WaterReflection + 2^LayerDefine.Terrain + 2^LayerDefine.NPC
  cameraScript.orthographic = false
  cameraScript.fieldOfView = 60
  cameraScript.farClipPlane = 140
  cameraScript.nearClipPlane = 0.1
  cameraScript.depth = -10
  cameraScript.transform.parent = fatherNode.transform
	local cameraPosString = ShiJieShiJian_FlyShoupa.GetData().CameraPos
	local cameraPosTable = split(cameraPosString,',')

	cameraNode.transform.localPosition = Vector3(0,cameraPosTable[2],-cameraPosTable[1])

  cameraScript.transform.rotation = Quaternion.LookRotation(Vector3(0,-cameraPosTable[2],cameraPosTable[1]))
  --cameraScript.transform.localScale = Vector3.one
	cameraScript.targetTexture = renderTex
end

function LuaHandkerchiefFlyWnd:InitPos(posData)
	if not posData then
		return
	end
	local onLoadFinished = function(generateNode)
		if not generateNode then
			return
		end
		handkerchiefNode = generateNode
		handkerchiefNode.transform.position = Vector3(posData[1],posData[2],posData[3])
		self:InitCamera(generateNode)
		self:UpdatePos()
	end
	KitePlayMgr.Inst:LoadPrefabByPath("UI/Prefab/HandkerchiefFly/HandkerchiefNode.prefab",DelegateFactory.Action_GameObject(onLoadFinished))
end

local movePosCount = 1
local moveSpeed = 7

function LuaHandkerchiefFlyWnd:UpdatePos()
	if not handkerchiefNode then
		return
	end

  movePosCount = movePosCount + 1
  if movePosCount > #flyPosTable then
    RegisterTickWithDuration(function ()
      Gac2Gas.HandkerchiefFlyFinished()
      CUIManager.CloseUI(CUIRes.HandkerchiefFlyWnd)
  	end,2000,2000)
    return
  end
  local dis_x = handkerchiefNode.transform.position.x - flyPosTable[movePosCount][1]
  local dis_y = handkerchiefNode.transform.position.y - flyPosTable[movePosCount][2]
  local dis_z = handkerchiefNode.transform.position.z - flyPosTable[movePosCount][3]
  local dis = math.sqrt(dis_x*dis_x + dis_y*dis_y + dis_z*dis_z)

  local tweenPos = TweenPosition.Begin(handkerchiefNode,dis/moveSpeed,Vector3(flyPosTable[movePosCount][1],flyPosTable[movePosCount][2],flyPosTable[movePosCount][3]))

  if movePosCount == 2 then
    local tweenFinish = function()
      self:UpdatePos()
    end

    CommonDefs.AddEventDelegate(tweenPos.onFinished, DelegateFactory.Action(tweenFinish))
  end
	---[[


	--TweenRotation.Begin(handkerchiefNode,0.105,KitePlayMgr.QuaDot(
	--Quaternion.LookRotation(Vector3(posData.kitePos[1] - posData.playerPos[1],posData.kitePos[2] - posData.playerPos[2],posData.kitePos[3] - posData.playerPos[3])),
	--Quaternion.LookRotation(Vector3(posData.kitePos[1] - handkerchiefNode.transform.position.x,posData.kitePos[2] - handkerchiefNode.transform.position.y,posData.kitePos[3] - handkerchiefNode.transform.position.z))
	--))

	--handkerchiefNode.transform.rotation =
  local rotateTime = 1
  if movePosCount == 2 then
    rotateTime = 0.01
  else
    rotateTime = 1
  end
	local ratio = 15
	local extraRation = 1.4
	TweenRotation.Begin(handkerchiefNode,rotateTime,
		Quaternion.LookRotation(Vector3(
		flyPosTable[movePosCount][1] - handkerchiefNode.transform.position.x,
		flyPosTable[movePosCount][2] - handkerchiefNode.transform.position.y,
		flyPosTable[movePosCount][3] - handkerchiefNode.transform.position.z))
  )
	--]]
end

function LuaHandkerchiefFlyWnd:Init()
	local onCloseClick = function(go)
		CUIManager.CloseUI(CUIRes.HandkerchiefFlyWnd)
	end
	CommonDefs.AddOnClickListener(self.CloseBtn,DelegateFactory.Action_GameObject(onCloseClick),false)

  if cameraNode then
		GameObject.Destroy(cameraNode)
	end
  if handkerchiefNode then
    GameObject.Destroy(handkerchiefNode)
  end
  movePosCount = 1
	--
	local onLeftVSNodeMove = function(dir)

	end
	local onLeftVSNodeEnd = function()

	end
	CommonDefs.AddVSOnDraggingListener(self.LeftVSNode,DelegateFactory.Action_Vector3(onLeftVSNodeMove),false)
	CommonDefs.AddVSOnDragFinishListener(self.LeftVSNode,DelegateFactory.Action(onLeftVSNodeEnd),false)

	--set fly pos data
	flyPosTable = {}
	local setPosString = ShiJieShiJian_FlyShoupa.GetData().ViaPos
	local setPosTable = split(setPosString,';')
	for i,v in ipairs(setPosTable) do
		table.insert(flyPosTable,split(v,','))
	end

	self:InitPos(flyPosTable[1])
end

function LuaHandkerchiefFlyWnd:OnDestroy()
    CMainCamera.Inst:SetCameraEnableStatus(true,"use_another_camera", false)
	CPreDrawMgr.m_bEnableRectPreDraw = true
	if cameraNode then
		GameObject.Destroy(cameraNode)
	end
  if handkerchiefNode then
    GameObject.Destroy(handkerchiefNode)
  end
end

return LuaHandkerchiefFlyWnd
