-- local Mathf = import "UnityEngine.Mathf"
-- local LocalString = import "LocalString"
local Color = import "UnityEngine.Color"
-- local CFreightWnd = import "L10.UI.CFreightWnd"
local CFreightMgr = import "L10.Game.CFreightMgr"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local CFreightEquipSubmitMgr=import "L10.UI.CFreightEquipSubmitMgr"


CLuaFreightWnd = class()
-- RegistClassMember(CLuaFreightWnd,"closeBtn")
RegistClassMember(CLuaFreightWnd,"cargoList")
RegistClassMember(CLuaFreightWnd,"cargoInfo")
RegistClassMember(CLuaFreightWnd,"leftTime")
RegistClassMember(CLuaFreightWnd,"transport")
RegistClassMember(CLuaFreightWnd,"lastTime")
RegistClassMember(CLuaFreightWnd,"cancel")

function CLuaFreightWnd:Awake()
    -- self.closeBtn = self.transform:Find("Wnd_Bg_Secondary_1/CloseButton").gameObject
    
    self.leftTime = self.transform:Find("Anchor/LeftTime"):GetComponent(typeof(UILabel))
    self.lastTime = 0
    self.cancel = nil

end

-- function CLuaFreightWnd:OnEnable()
    -- local script = self.transform:Find("Anchor/CargoList"):GetComponent(typeof(CCommonLuaScript))
    -- self.cargoList =script.m_LuaSelf
    -- self.cargoList.OnCargoClicked = function()
    --     cargoInfo:Init()
    -- end
-- end

function CLuaFreightWnd:OnDisable()
    CFreightEquipSubmitMgr.ResetData()
    CFreightMgr.Inst.playerId = 0
    self:DoCancel()
end

function CLuaFreightWnd:DoCancel()
    if self.cancel then
        UnRegisterTick(self.cancel)
        self.cancel=nil
    end
end
-- Auto Generated!!
function CLuaFreightWnd:Init( )
    self.lastTime = CFreightMgr.Inst.endTime - CFreightMgr.Inst.currentTime

    self.cargoInfo = self.transform:Find("Anchor/CargoInfo"):GetComponent(typeof(CCommonLuaScript)).m_LuaSelf

    self.cargoList =self.transform:Find("Anchor/CargoList"):GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
    self.cargoList.OnCargoClicked = function(idx)
        self.cargoInfo:Init(idx)
    end
    self.cargoList:Init()


    self:UpdateTime()
    self:DoCancel()
    self.cancel = RegisterTick(function()
        self:UpdateTime()
    end,1000*60)

    self.transport = self.transform:Find("Anchor/Transport"):GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
    self.transport:Init()
end
function CLuaFreightWnd:UpdateTime( )
    if self.lastTime < 0 then
        self.leftTime.text = LocalString.GetString("任务已失效，请放弃任务")
        self.leftTime.color = Color.red
    end
    local hour = math.floor(self.lastTime / 3600)
    local minute = math.floor(self.lastTime % 3600 / 60)
    self.leftTime.text = System.String.Format(LocalString.GetString("还有{0}小时{1}分到期"), hour, minute)
    if hour == 0 and minute <= 10 then
        self.leftTime.color = Color.red
    else
        self.leftTime.color = Color.yellow
    end
    self.lastTime = self.lastTime - 60
end

