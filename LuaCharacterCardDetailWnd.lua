local GameObject = import "UnityEngine.GameObject"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CommonDefs = import "L10.Game.CommonDefs"
local UITexture = import "UITexture"
local DelegateFactory = import "DelegateFactory"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local ShareMgr = import "ShareMgr"

LuaCharacterCardDetailWnd = class()
RegistChildComponent(LuaCharacterCardDetailWnd,"closeBtn", GameObject)
RegistChildComponent(LuaCharacterCardDetailWnd,"shareBtn", GameObject)
RegistChildComponent(LuaCharacterCardDetailWnd,"pic", UITexture)

RegistClassMember(LuaCharacterCardDetailWnd, "maxChooseNum")

function LuaCharacterCardDetailWnd:Close()
	CUIManager.CloseUI(CLuaUIResources.CharacterCardDetailWnd)
  g_ScriptEvent:BroadcastInLua('UpdateCharacterCardInfo')
end

function LuaCharacterCardDetailWnd:OnEnable()
	--g_ScriptEvent:AddListener("", self, "")
end

function LuaCharacterCardDetailWnd:OnDisable()
	--g_ScriptEvent:RemoveListener("", self, "")
end

function LuaCharacterCardDetailWnd:Init()
	local onCloseClick = function(go)
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.closeBtn,DelegateFactory.Action_GameObject(onCloseClick),false)

	local onShareClick = function(go)
		Gac2Gas.RequestGetRdcShareReward()
		CUICommonDef.CaptureScreenAndShare()
	end
	CommonDefs.AddOnClickListener(self.shareBtn,DelegateFactory.Action_GameObject(onShareClick),false)
	if LuaCharacterCardMgr.ShareBigPicUrl then
		local url = ShareMgr.GetWebImagePath(LuaCharacterCardMgr.ShareBigPicUrl)
		CPersonalSpaceMgr.DownLoadPic(url, self.pic, 1080, 1920, DelegateFactory.Action(function ()
		end), true, true)
		LuaCharacterCardMgr.ShareBigPicUrl = nil
	elseif LuaCharacterCardMgr.ShareBigPicId then
		local data = RoleDrawingCard_CardItem2Idx.GetData(LuaCharacterCardMgr.ShareBigPicId)
		if data then
			local url = ShareMgr.GetWebImagePath(data.BigPic)
			CPersonalSpaceMgr.DownLoadPic(url, self.pic, 1080, 1920, DelegateFactory.Action(function ()
			end), true, true)
		end

		Gac2Gas.RequestCheckRdcCard(LuaCharacterCardMgr.ShareBigPicId)
	end

end

return LuaCharacterCardDetailWnd
