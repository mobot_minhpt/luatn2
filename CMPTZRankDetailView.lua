-- Auto Generated!!
local AlignType = import "CPlayerInfoMgr+AlignType"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CMPTZRankDetailView = import "L10.UI.CMPTZRankDetailView"
local CMPTZRankListItem = import "L10.UI.CMPTZRankListItem"
local CommonDefs = import "L10.Game.CommonDefs"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local CPVPMgr = import "L10.Game.CPVPMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumClass = import "L10.Game.EnumClass"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local LocalString = import "LocalString"
local PlayerGradeGroup = import "L10.Game.PlayerGradeGroup"
local UInt32 = import "System.UInt32"
local Vector3 = import "UnityEngine.Vector3"
CMPTZRankDetailView.m_Init_CS2LuaHook = function (this, cls) 

    if EnumToInt(this.curCls) == cls then
        return
    end
    this.curCls = CommonDefs.ConvertIntToEnum(typeof(EnumClass), cls)
    this.tableView.dataSource = this
    this.tableView.eventDelegate = this
    this.rankGroupView.OnGroupSelected = MakeDelegateFromCSFunction(this.OnGroupSelected, MakeGenericClass(Action1, UInt32), this)
    this.rankGroupView:Init(this.curCls)
end
CMPTZRankDetailView.m_OnMPTZQueryRankInfoFinish_CS2LuaHook = function (this) 

    local showMainPlayerInfo = false
    if CClientMainPlayer.Inst ~= nil and this.curCls == CClientMainPlayer.Inst.Class then
        local level = CClientMainPlayer.Inst.HasFeiSheng and CClientMainPlayer.Inst.XianShenLevel or CClientMainPlayer.Inst.Level

        if this.rankGroupView.CurSelectedIndex == 0 or this.rankGroupView.CurSelectedIndex - 1 == PlayerGradeGroup.GetGroupIndexByGrade(level) then
            --需要显示自己的信息
            showMainPlayerInfo = true
        end
    end
    if showMainPlayerInfo then
        this.mainplayerInfoRoot:SetActive(true)
        local default
        if CPVPMgr.MainPlayerRankInfo == nil then
            default = LocalString.GetString("未上榜")
        else
            default = tostring(CPVPMgr.MainPlayerRankInfo.rankIdx)
        end
        this.mainplayerRankLabel.text = default
        local extern
        if CPVPMgr.MainPlayerRankInfo == nil then
            extern = CClientMainPlayer.Inst.Name
        else
            extern = CPVPMgr.MainPlayerRankInfo.name
        end
        this.mainplayerNameLabel.text = extern
        this.mainplayerTotalRankLabel.text = CPVPMgr.MainPlayerRankInfo == nil and "" or tostring(CPVPMgr.MainPlayerRankInfo.totalRankIdx)
        this.mainplayerLevelLabel.text = CPVPMgr.MainPlayerRankInfo == nil and tostring(CClientMainPlayer.Inst.Level) or tostring(CPVPMgr.MainPlayerRankInfo.grade)
        if this.tableBg.height ~= this.normalTableBgHeight then
            this.tableBg.height = this.normalTableBgHeight
            this.tableHeaderBg:ResetAndUpdateAnchors()
            this.tableView.scrollView.panel:ResetAndUpdateAnchors()
        end
    else
        this.mainplayerInfoRoot:SetActive(false)
        if this.tableBg.height ~= this.higherTableBgHeight then
            this.tableBg.height = this.higherTableBgHeight
            this.tableHeaderBg:ResetAndUpdateAnchors()
            this.tableView.scrollView.panel:ResetAndUpdateAnchors()
        end
    end
    this.tableView:LoadData(0, false)
end
CMPTZRankDetailView.m_CellForRowAtIndex_CS2LuaHook = function (this, index) 

    local cellIdentifier = "RowCell"

    local cell = this.tableView:DequeueReusableCellWithIdentifier(cellIdentifier)
    if cell == nil then
        cell = this.tableView:AllocNewCellWithIdentifier(this.rankTemplate, cellIdentifier)
    end
    local info = CPVPMgr.RankList[index]
    CommonDefs.GetComponent_GameObject_Type(cell, typeof(CMPTZRankListItem)):Init(info, EnumToInt(this.curCls), info.rankIdx % 2 ~= 0, true)

    return cell
end
CMPTZRankDetailView.m_OnRowSelected_CS2LuaHook = function (this, index) 

    if index >= 0 and index < CPVPMgr.RankList.Count then
        CPlayerInfoMgr.ShowPlayerPopupMenu(math.floor(CPVPMgr.RankList[index].targetId), EnumPlayerInfoContext.RankList, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
    end
end
CMPTZRankDetailView.m_OnGroupSelected_CS2LuaHook = function (this, index) 

    this.tableView:Clear()
    this.mainplayerInfoRoot:SetActive(false)
    if this.tableBg.height ~= this.higherTableBgHeight then
        this.tableBg.height = this.higherTableBgHeight
        this.tableHeaderBg:ResetAndUpdateAnchors()
        this.tableView.scrollView.panel:ResetAndUpdateAnchors()
    end
    if index == 0 then
        CPVPMgr.QueryMPTZRankInfo(this.curCls, 0)
        this.tableLevelHeaderLabel.text = LocalString.GetString("等级")
    else
        if index - 1 >= 0 and index - 1 < PlayerGradeGroup.Groups.Length then
            CPVPMgr.QueryMPTZRankInfo(this.curCls, PlayerGradeGroup.Groups[index - 1])
            this.tableLevelHeaderLabel.text = System.String.Format(LocalString.GetString("等级({0}-{1}级)"), index - 1 > 0 and PlayerGradeGroup.Groups[index - 2] + 1 or 0, PlayerGradeGroup.Groups[index - 1])
        else
            this.tableLevelHeaderLabel.text = LocalString.GetString("等级")
        end
    end
end
