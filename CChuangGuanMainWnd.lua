-- Auto Generated!!
local Boolean = import "System.Boolean"
local CChuangGuanItem = import "L10.UI.CChuangGuanItem"
local CChuangGuanMainWnd = import "L10.UI.CChuangGuanMainWnd"
local ChuangGuan_Dispatch = import "L10.Game.ChuangGuan_Dispatch"
local ChuangGuan_Setting = import "L10.Game.ChuangGuan_Setting"
local CMenPaiChuangGuanMgr = import "L10.Game.CMenPaiChuangGuanMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local Time = import "UnityEngine.Time"
local UIEventListener = import "UIEventListener"
local UISprite = import "UISprite"
local Vector3 = import "UnityEngine.Vector3"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CChuangGuanMainWnd.m_Init_CS2LuaHook = function (this) 
    this:initGridItems()
    this:addListeners()
    this.TimeLabel.text = LocalString.GetString("活动时间：") .. ChuangGuan_Setting.GetData().PlayTime
    if CMenPaiChuangGuanMgr.Inst.isHero then
        this:showHeroMode()
    else
        this:showNormalMode()
    end
    this.lastClickTime = 0
end
CChuangGuanMainWnd.m_addListeners_CS2LuaHook = function (this) 
    UIEventListener.Get(this.NormalButton).onClick = MakeDelegateFromCSFunction(this.onNormalButtonClick, VoidDelegate, this)
    UIEventListener.Get(this.HeroButton).onClick = MakeDelegateFromCSFunction(this.onHeroButtonClick, VoidDelegate, this)
    UIEventListener.Get(this.HintButton).onClick = MakeDelegateFromCSFunction(this.onHintButtonClick, VoidDelegate, this)
    UIEventListener.Get(this.CloseButton).onClick = MakeDelegateFromCSFunction(this.onCloseButtonClick, VoidDelegate, this)
    UIEventListener.Get(this.RankButton).onClick = MakeDelegateFromCSFunction(this.onRankButtonClick, VoidDelegate, this)
end
CChuangGuanMainWnd.m_initGridItems_CS2LuaHook = function (this) 
    local item = nil
    --     int count = ChuangGuan_Dispatch.GetDataCount();
    for i = 0, 1 do
        local go = CommonDefs.Object_Instantiate(this.itemPrefab)
        this.ContentGrid:AddChild(go.transform)
        go.transform.localScale = Vector3.one
        item = CommonDefs.GetComponent_GameObject_Type(go, typeof(CChuangGuanItem))
        item.ClickCallback = MakeDelegateFromCSFunction(this.onItemClick, MakeGenericClass(Action2, Int32, Boolean), this)
        CommonDefs.ListAdd(this.itemList, typeof(CChuangGuanItem), item)
    end
    this.ContentGrid:AddChild(this.itemPrefab.transform)
    this.itemPrefab.transform.localScale = Vector3.one
    item = CommonDefs.GetComponent_GameObject_Type(this.itemPrefab, typeof(CChuangGuanItem))
    item.ClickCallback = MakeDelegateFromCSFunction(this.onItemClick, MakeGenericClass(Action2, Int32, Boolean), this)
    CommonDefs.ListAdd(this.itemList, typeof(CChuangGuanItem), item)
    this.ContentGrid.repositionNow = true
end
CChuangGuanMainWnd.m_showNormalMode_CS2LuaHook = function (this) 
    local t = Time.realtimeSinceStartup
    if (t - this.lastClickTime) < CChuangGuanMainWnd.clickInterval then
        g_MessageMgr:ShowMessage("NOT_SWITCH_TOOFAST")
        return
    end
    this.lastClickTime = t
    CMenPaiChuangGuanMgr.Inst.isHero = false
    this.RankButton:SetActive(false)
    CommonDefs.GetComponent_GameObject_Type(this.NormalButton, typeof(UISprite)).spriteName = CChuangGuanMainWnd.highlightButtonSpriteString
    CommonDefs.GetComponent_GameObject_Type(this.HeroButton, typeof(UISprite)).spriteName = CChuangGuanMainWnd.normalButtonSpriteString
    this.InfoArea.transform.localPosition = Vector3.zero
    CMenPaiChuangGuanMgr.Inst:RequestChuangGuanOverview(false)
end
CChuangGuanMainWnd.m_showHeroMode_CS2LuaHook = function (this) 
    local t = Time.realtimeSinceStartup
    if (t - this.lastClickTime) < CChuangGuanMainWnd.clickInterval then
        g_MessageMgr:ShowMessage("NOT_SWITCH_TOOFAST")
        return
    end
    this.lastClickTime = t
    CMenPaiChuangGuanMgr.Inst.isHero = true
    this.RankButton:SetActive(true)
    CommonDefs.GetComponent_GameObject_Type(this.NormalButton, typeof(UISprite)).spriteName = CChuangGuanMainWnd.normalButtonSpriteString
    CommonDefs.GetComponent_GameObject_Type(this.HeroButton, typeof(UISprite)).spriteName = CChuangGuanMainWnd.highlightButtonSpriteString
    this.InfoArea.transform.localPosition = CChuangGuanMainWnd.heroInfoPosition
    CMenPaiChuangGuanMgr.Inst:RequestChuangGuanOverview(true)
end
CChuangGuanMainWnd.m_refreshOverviewItems_CS2LuaHook = function (this) 
    local data = nil
    if CMenPaiChuangGuanMgr.Inst.isHero then
        data = CMenPaiChuangGuanMgr.Inst.HeroList
    else
        data = CMenPaiChuangGuanMgr.Inst.NormalList
    end
    if data == nil or data.Count == 0 then
        this.ContentGrid.gameObject:SetActive(false)
        return
    end
    this.ContentGrid.gameObject:SetActive(true)
    do
        local i = 0
        while i < data.Count do
            local v = data[i]
            this.itemList[i]:SetIndex(math.floor(v.x), math.floor(v.y), math.floor(v.z), CMenPaiChuangGuanMgr.Inst.isHero)
            i = i + 1
        end
    end
end
CChuangGuanMainWnd.m_onItemClick_CS2LuaHook = function (this, id, isHero) 
    local data = ChuangGuan_Dispatch.GetData(id)
    if isHero then
        CMenPaiChuangGuanMgr.Inst:RequestEnterChuangGuan(data.HardId)
    else
        CMenPaiChuangGuanMgr.Inst:RequestEnterChuangGuan(data.NormalId)
    end
end
