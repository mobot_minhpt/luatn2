local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local MessageWndManager = import "L10.UI.MessageWndManager"
local GuildDefine = import "L10.Game.GuildDefine"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local CGuildMgr = import "L10.Game.CGuildMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local QnSelectableButton=import "L10.UI.QnSelectableButton"


CLuaGuildJobChangeWnd = class()
RegistClassMember(CLuaGuildJobChangeWnd,"m_LabelName")
RegistClassMember(CLuaGuildJobChangeWnd,"m_LabelOffice")
RegistClassMember(CLuaGuildJobChangeWnd,"m_ApplyJobBtn")
RegistClassMember(CLuaGuildJobChangeWnd,"m_UnApplyJobBtn")
RegistClassMember(CLuaGuildJobChangeWnd,"m_CloseBtn")
RegistClassMember(CLuaGuildJobChangeWnd,"questionBtn")
RegistClassMember(CLuaGuildJobChangeWnd,"buttons")
RegistClassMember(CLuaGuildJobChangeWnd,"m_Idx")

CLuaGuildJobChangeWnd.idx2Office = {
    1,      --// 帮主
    2,      --// 副帮主
    3,      --// 长老
    11,     --// 战龙堂主
    12,     --// 巧燕堂主

    13,     --// 金凤堂主
    14,     --// 卧虎堂主
    21,     --// 战龙香主
    22,     --// 巧燕香主
    23,     --// 金凤香主

    24,     --// 卧虎香主
    31,     --// 精英
    100,    --// 帮众
    4,--//左护法
    5,--//右护法
    -1, --// 内务府
}
CLuaGuildJobChangeWnd.officeKey = {
    "AppointBangZhu",
    "AppointFuBangZhu",
    "AppointZhangLao",

    "AppointTangZhu",
    "AppointTangZhu",
    "AppointTangZhu",
    "AppointTangZhu",

    "AppointXiangZhu",
    "AppointXiangZhu",
    "AppointXiangZhu",
    "AppointXiangZhu",

    "AppointJingYing",
    "AppointBangZhong",
    "AppointHuFa",
    "AppointHuFa",
    "AppointNeiWuFu",
}

CLuaGuildJobChangeWnd.m_Name=nil
CLuaGuildJobChangeWnd.m_Office=nil
CLuaGuildJobChangeWnd.m_PlayerId=nil
function CLuaGuildJobChangeWnd.ShowWnd(playerId,name,office)
    CLuaGuildJobChangeWnd.m_Name=name
    CLuaGuildJobChangeWnd.m_Office=office
    CLuaGuildJobChangeWnd.m_PlayerId=playerId
    local isNeiWuFuOfficer = function(id)
        if id==0 then id = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id end
        if CGuildMgr.Inst.m_GuildMemberInfo then
            local memberInfo = CommonDefs.DictGetValue_LuaCall(CGuildMgr.Inst.m_GuildMemberInfo.Infos,id)
            if memberInfo then
                return memberInfo.MiscData.IsNeiWuFuOfficer == 1
            end
        end
        return false
    end
    if isNeiWuFuOfficer(playerId) then
        CLuaGuildJobChangeWnd.m_Office = (CLuaGuildJobChangeWnd.m_Office or "") ..","..Guild_Setting.GetData().NeiWuFuOfficerName
    end
    CUIManager.ShowUI(CUIResources.GuildChangeJobWnd)
end

function CLuaGuildJobChangeWnd:Awake()
    self.m_LabelName = self.transform:Find("LabelName"):GetComponent(typeof(UILabel))
    self.m_LabelOffice = self.transform:Find("LabelOffice"):GetComponent(typeof(UILabel))
    self.m_ApplyJobBtn = self.transform:Find("JobBtn").gameObject
    self.m_UnApplyJobBtn = self.transform:Find("CancleBtn").gameObject
    self.questionBtn = self.transform:Find("QuestionBtn").gameObject
    self.buttons = {}
    local parent = self.transform:Find("Contents/QnRadioBox")
    self.buttons[1] = parent:Find("bzhu"):GetComponent(typeof(QnSelectableButton))
    self.buttons[2] = parent:Find("fbz"):GetComponent(typeof(QnSelectableButton))
    self.buttons[3] = parent:Find("zl"):GetComponent(typeof(QnSelectableButton))
    self.buttons[4] = parent:Find("zlt"):GetComponent(typeof(QnSelectableButton))
    self.buttons[5] = parent:Find("qyt"):GetComponent(typeof(QnSelectableButton))
    self.buttons[6] = parent:Find("jft"):GetComponent(typeof(QnSelectableButton))
    self.buttons[7] = parent:Find("wht"):GetComponent(typeof(QnSelectableButton))
    self.buttons[8] = parent:Find("zlx"):GetComponent(typeof(QnSelectableButton))
    self.buttons[9] = parent:Find("qyx"):GetComponent(typeof(QnSelectableButton))
    self.buttons[10] = parent:Find("jfx"):GetComponent(typeof(QnSelectableButton))
    self.buttons[11] = parent:Find("whx"):GetComponent(typeof(QnSelectableButton))
    self.buttons[12] = parent:Find("jy"):GetComponent(typeof(QnSelectableButton))
    self.buttons[13] = parent:Find("bzong"):GetComponent(typeof(QnSelectableButton))
    self.buttons[14] = parent:Find("zhf"):GetComponent(typeof(QnSelectableButton))--左护法
    self.buttons[15] = parent:Find("yhf"):GetComponent(typeof(QnSelectableButton))--右护法
    self.buttons[16] = parent:Find("nwf"):GetComponent(typeof(QnSelectableButton))--内务府

    for i,v in ipairs(self.buttons) do
        v.OnClick = DelegateFactory.Action_QnButton(function(btn)
            self:OnClickButton(btn)
        end)
    end

    self.m_Idx = -1

    UIEventListener.Get(self.m_ApplyJobBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnClickApplyJobButton(go)
    end)
    UIEventListener.Get(self.m_UnApplyJobBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnClickUnApplyJobButton(go)
    end)
    UIEventListener.Get(self.questionBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        g_MessageMgr:ShowMessage("Guild_ChangeJob_Question_Msg")
    end)


end

function CLuaGuildJobChangeWnd:Init( )
    self:UpdateButtonState()

    --Gac2Gas.QueryPlayerGuildInfo(1);
    --GetGuildMemberInfo主要是要拿到自己的职位信息，如果拿过了就不要去拿了，如果没拿过先取memberinfo，再请求rightinfo
    if CGuildMgr.Inst.m_GuildMemberInfo == nil then
        CGuildMgr.Inst:GetGuildMemberInfo()
    else
        -- CGuildMgr.Inst:GetGuildRightsInfo()
        CLuaGuildMgr.GetGuildRightsInfo()
    end
end
function CLuaGuildJobChangeWnd:UpdateButtonState( )
    if CGuildMgr.Inst.m_GuildMemberInfo then
        for i,v in ipairs(CLuaGuildJobChangeWnd.idx2Office) do
            local key = CLuaGuildJobChangeWnd.officeKey[i]
            if CLuaGuildMgr.GetMyRight(key) then
                CUICommonDef.SetActive(self.buttons[i].gameObject, true, true)
            else
                CUICommonDef.SetActive(self.buttons[i].gameObject, false, true)
            end
        end
    end
end
function CLuaGuildJobChangeWnd:OnEnable( )
    g_ScriptEvent:AddListener("RequestOperationInGuildSucceed", self, "OnRequestOperationInGuildSucceed")
    g_ScriptEvent:AddListener("GuildInfoReceived", self, "OnGuildInfoReceived")
    g_ScriptEvent:AddListener("SendGuildInfo_GuildRightsInfo", self, "OnGetGuildRightsInfo")

    self.m_LabelName.text = CLuaGuildJobChangeWnd.m_Name
    self.m_LabelOffice.text = CLuaGuildJobChangeWnd.m_Office
    self.m_Idx = 0
end

function CLuaGuildJobChangeWnd:OnDisable()
    g_ScriptEvent:RemoveListener("RequestOperationInGuildSucceed", self, "OnRequestOperationInGuildSucceed")
    g_ScriptEvent:RemoveListener("GuildInfoReceived", self, "OnGuildInfoReceived")
    g_ScriptEvent:RemoveListener("SendGuildInfo_GuildRightsInfo", self, "OnGetGuildRightsInfo")

end

function CLuaGuildJobChangeWnd:OnGuildInfoReceived( args) 
    local rpcType = tostring(args[0])

    if (("GuildMemberInfo") == rpcType) then
        CLuaGuildMgr.GetGuildRightsInfo()
    end
end
function CLuaGuildJobChangeWnd:OnRequestOperationInGuildSucceed(args) 
    local operationType, paramStr=args[0],args[1]
    if (("AppointMember") == operationType) or (("DismissMember") == operationType) or (("NeiWuFuOperation") == operationType) then
        CUIManager.CloseUI(CUIResources.GuildChangeJobWnd)
    end
end
function CLuaGuildJobChangeWnd:OnGetGuildRightsInfo()
    self:UpdateButtonState()
end

function CLuaGuildJobChangeWnd:OnClickButton( btn) 
    self.m_Idx=-1
    for i,v in ipairs(self.buttons) do
        if btn==v then
            v:SetSelected(true, false)
            self.m_Idx=i
        else
            v:SetSelected(false, false)
        end
    end

    self:UpdateButtonState()
end

function CLuaGuildJobChangeWnd:OnClickApplyJobButton( go) 
    if self.m_Idx < 1 or self.m_Idx > 16 then
        return
    end
    local guildId = CClientMainPlayer.Inst.BasicProp.GuildId
    local office = CLuaGuildJobChangeWnd.idx2Office[self.m_Idx]

    if office == - 1 then
        Gac2Gas.RequestOperationInGuild(
            guildId, 
            "NeiWuFuOperation", 
            "Appoint", 
            tostring(CLuaGuildJobChangeWnd.m_PlayerId))
        return
    end

    -- 判断玩家自己是不是帮主
    local isLeader = false
    if CGuildMgr.Inst.m_GuildMemberInfo ~= nil then
        local selfInfo = CommonDefs.DictGetValue(CGuildMgr.Inst.m_GuildMemberInfo.Infos, typeof(UInt64), CClientMainPlayer.Inst.BasicProp.Id)
        if selfInfo ~= nil and selfInfo.Title == 1 then
            isLeader = true
        end
    end
    -- 自己是帮主，并且任命帮主
    if isLeader and office == 1 then
        local message = g_MessageMgr:FormatMessage("Guild_Appointment_Leader_Confirm", CLuaGuildJobChangeWnd.m_Name)
        MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function () 
            Gac2Gas.RequestOperationInGuild(
                guildId, 
                "AppointMember", 
                (tostring(CLuaGuildJobChangeWnd.m_PlayerId) .. ",") .. office, 
                "")
        end), nil, nil, nil, false)
        return
    end
    Gac2Gas.RequestOperationInGuild(
        guildId, 
        "AppointMember", 
        (tostring(CLuaGuildJobChangeWnd.m_PlayerId) .. ",") .. office, 
        "")
end

function CLuaGuildJobChangeWnd:OnClickUnApplyJobButton( go) 
    local info = CommonDefs.DictGetValue(CGuildMgr.Inst.m_GuildMemberInfo.Infos, typeof(UInt64), CLuaGuildJobChangeWnd.m_PlayerId)
    local title = GuildDefine.GetOfficeName(info.Title)

    local neiwufuTItle = Guild_Setting.GetData().NeiWuFuOfficerName

    if CGuildMgr.Inst:IsNeiWuFuOfficer(CLuaGuildJobChangeWnd.m_PlayerId) then

        local t={}
        local item=PopupMenuItemData(title,DelegateFactory.Action_int(function (index) 
                local message = g_MessageMgr:FormatMessage("Guild_Dismiss_Confirm", CLuaGuildJobChangeWnd.m_Name, title)
                MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function() self:RequestDismissMember() end), nil, nil, nil, false)
            end),false,nil)

        table.insert(t, item)
        local item=PopupMenuItemData(neiwufuTItle,DelegateFactory.Action_int(function (index) 
                local message = g_MessageMgr:FormatMessage("Guild_Dismiss_Confirm", CLuaGuildJobChangeWnd.m_Name, neiwufuTItle)
                MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function() self:RequestDismissNeiWuFuMember() end), nil, nil, nil, false)
            end),false,nil)
        table.insert(t, item)

        CPopupMenuInfoMgr.ShowPopupMenu(Table2Array(t,MakeArrayClass(PopupMenuItemData)), go.transform, CPopupMenuInfoMgr.AlignType.Top,1,nil,600,true,266)
    else
        local message = g_MessageMgr:FormatMessage("Guild_Dismiss_Confirm", CLuaGuildJobChangeWnd.m_Name, title)
        MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function() self:RequestDismissMember() end), nil, nil, nil, false)
    end
end

function CLuaGuildJobChangeWnd:RequestDismissMember()
    Gac2Gas.RequestOperationInGuild(
        CClientMainPlayer.Inst.BasicProp.GuildId, 
        "DismissMember", 
        tostring(CLuaGuildJobChangeWnd.m_PlayerId),
        "")
end
function CLuaGuildJobChangeWnd:RequestDismissNeiWuFuMember()
    Gac2Gas.RequestOperationInGuild(
        CClientMainPlayer.Inst.BasicProp.GuildId, 
        "NeiWuFuOperation", 
        "Dismiss", 
        tostring(CLuaGuildJobChangeWnd.m_PlayerId))
end
