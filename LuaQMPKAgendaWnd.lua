local UILabel = import "UILabel"
local NGUITools = import "NGUITools"
local QnTabView = import "L10.UI.QnTabView"
-- local CQMPKAgendaWnd = import "L10.UI.CQMPKAgendaWnd"
local CChatLinkMgr = import "CChatLinkMgr"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"


CLuaQMPKAgendaWnd = class()
RegistClassMember(CLuaQMPKAgendaWnd,"m_RulesTable")
RegistClassMember(CLuaQMPKAgendaWnd,"m_RuleItem")
RegistClassMember(CLuaQMPKAgendaWnd,"m_TimeLabel")
RegistClassMember(CLuaQMPKAgendaWnd,"m_TabView")
RegistClassMember(CLuaQMPKAgendaWnd,"m_RulesScrollView")
CLuaQMPKAgendaWnd.StartWnd = 0

function CLuaQMPKAgendaWnd:Awake()
    self.m_RulesScrollView = self.transform:Find("ShowArea/RuleRoot/Scrollview"):GetComponent(typeof(CUIRestrictScrollView))
    self.m_RulesTable = self.transform:Find("ShowArea/RuleRoot/Scrollview/Table"):GetComponent(typeof(UITable))
    self.m_RuleItem = self.transform:Find("ShowArea/RuleRoot/Item").gameObject
    self.m_TabView = self.transform:Find("ShowArea"):GetComponent(typeof(QnTabView))
self.m_TimeLabel = {}
self.m_TimeLabel[1]=FindChild(self.transform,"Time1"):GetComponent(typeof(UILabel))
self.m_TimeLabel[2]=FindChild(self.transform,"Time2"):GetComponent(typeof(UILabel))
self.m_TimeLabel[3]=FindChild(self.transform,"Time3"):GetComponent(typeof(UILabel))
self.m_TimeLabel[4]=FindChild(self.transform,"Time4"):GetComponent(typeof(UILabel))
self.m_TimeLabel[5]=FindChild(self.transform,"Time5"):GetComponent(typeof(UILabel))
self.m_TimeLabel[6]=FindChild(self.transform,"Time6"):GetComponent(typeof(UILabel))
self.m_TimeLabel[7]=FindChild(self.transform,"Time7"):GetComponent(typeof(UILabel))
self.m_TimeLabel[8]=FindChild(self.transform,"Time8"):GetComponent(typeof(UILabel))
self.m_TimeLabel[9]=FindChild(self.transform,"Time9"):GetComponent(typeof(UILabel))
-- self.m_TimeLabel[10]=FindChild(self.transform,"Time10"):GetComponent(typeof(UILabel))

end

function CLuaQMPKAgendaWnd:OnEnable()
    self.m_RuleItem:SetActive(false)
    local setting = QuanMinPK_Setting.GetData()
    if nil == setting then
        -- self:Close()
        CUIManager.CloseUI(CLuaUIResources.QMPKAgendaWnd)
        return
    end

    for i,v in ipairs(self.m_TimeLabel) do
        v.text=CChatLinkMgr.TranslateToNGUIText(setting.Saicheng_Rule[i-1], false)
    end
    QuanMinPK_SaiZhiRule.Foreach(function (key,data)
        local go = NGUITools.AddChild(self.m_RulesTable.gameObject, self.m_RuleItem)
        if go ~= nil then
            if go ~= nil then
                go:SetActive(true)
                go.transform:Find("Sprite/Title"):GetComponent(typeof(UILabel)).text= CChatLinkMgr.TranslateToNGUIText(data.Title, false)
                go.transform:Find("Content"):GetComponent(typeof(UILabel)).text= CChatLinkMgr.TranslateToNGUIText(data.Description, false)
            end 
        end
    end)
    -- do
    --     local i = 0 local cnt = QuanMinPK_SaiZhiRule.GetDataCount()
    --     while i < cnt do
    --         local go = NGUITools.AddChild(self.m_RulesTable.gameObject, self.m_RuleItem)
    --         local data = QuanMinPK_SaiZhiRule.GetData(i + 1)
    --         if go ~= nil then
    --             go:SetActive(true)
    --             go.transform:Find("Sprite/Title"):GetComponent(typeof(UILabel)).text= CChatLinkMgr.TranslateToNGUIText(setting.Saizhi_Rule[i][0], false)
    --             -- CommonDefs.GetComponent_Component_Type(go.transform:Find("Sprite/Title"), typeof(UILabel)).text = CChatLinkMgr.TranslateToNGUIText(setting.Saizhi_Rule[i][0], false)
    --             -- CommonDefs.GetComponent_Component_Type(go.transform:Find("Content"), typeof(UILabel)).text = CChatLinkMgr.TranslateToNGUIText(setting.Saizhi_Rule[i][1], false)
    --             go.transform:Find("Content"):GetComponent(typeof(UILabel)).text= CChatLinkMgr.TranslateToNGUIText(setting.Saizhi_Rule[i][1], false)
    --         end
    --         i = i + 1
    --     end
    -- end
    self.m_RulesTable:Reposition()
end

-- Auto Generated!!
function CLuaQMPKAgendaWnd:Init( )
    self.m_TabView:ChangeTo(0)
    self.m_RulesScrollView:ResetPosition()
    self.m_TabView:ChangeTo(CLuaQMPKAgendaWnd.StartWnd)
    CLuaQMPKAgendaWnd.StartWnd = 0
    self.m_RulesTable:Reposition()
end

return CLuaQMPKAgendaWnd
