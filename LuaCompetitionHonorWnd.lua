local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local EnumClass = import "L10.Game.EnumClass"
local EnumGender = import "L10.Game.EnumGender"
local CPropertyAppearance = import "L10.Game.CPropertyAppearance"
local Quaternion = import "UnityEngine.Quaternion"
local EnumYingLingState = import "L10.Game.EnumYingLingState"
local Animation = import "UnityEngine.Animation"
local CCommonUITween = import "L10.UI.CCommonUITween"
local BoxCollider = import "UnityEngine.BoxCollider"


LuaCompetitionHonorWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaCompetitionHonorWnd, "CloseButton", GameObject)
RegistChildComponent(LuaCompetitionHonorWnd, "BookChooseView", GameObject)
RegistChildComponent(LuaCompetitionHonorWnd, "DouHunTanEnter", GameObject)
RegistChildComponent(LuaCompetitionHonorWnd, "DouHunTanView", CCommonLuaScript)
RegistChildComponent(LuaCompetitionHonorWnd, "QMPKEnter", GameObject)
RegistChildComponent(LuaCompetitionHonorWnd, "QMPKView", CCommonLuaScript)
RegistChildComponent(LuaCompetitionHonorWnd, "StarBiWuEnter", GameObject)
RegistChildComponent(LuaCompetitionHonorWnd, "StarBiWuView", CCommonLuaScript)
RegistChildComponent(LuaCompetitionHonorWnd, "PlayerList", GameObject)
RegistChildComponent(LuaCompetitionHonorWnd, "PlayerTemplate", GameObject)
RegistChildComponent(LuaCompetitionHonorWnd, "PlayerTable", UITable)
RegistChildComponent(LuaCompetitionHonorWnd, "CompetitionHonorModelView", CCommonLuaScript)
RegistChildComponent(LuaCompetitionHonorWnd, "CompetitionHonorIntroduceView", GameObject)
RegistChildComponent(LuaCompetitionHonorWnd, "PopViewCloseBg", GameObject)

RegistClassMember(LuaCompetitionHonorWnd, "m_Animation")
RegistClassMember(LuaCompetitionHonorWnd, "m_CompetitionHonorModelViewOpen")
RegistClassMember(LuaCompetitionHonorWnd, "m_ViewId")
RegistClassMember(LuaCompetitionHonorWnd, "m_ViewTbl")
RegistClassMember(LuaCompetitionHonorWnd, "m_ViewPageTbl")
RegistClassMember(LuaCompetitionHonorWnd, "m_PlayerTbl")
RegistClassMember(LuaCompetitionHonorWnd, "m_ModelViewUITween")
RegistClassMember(LuaCompetitionHonorWnd, "m_CompetitionHonorModelViewCloseBg")
--@endregion RegistChildComponent end

function LuaCompetitionHonorWnd:Awake()
    self.m_Animation = self.transform:GetComponent(typeof(Animation))
    self.m_ModelViewUITween = self.CompetitionHonorModelView.transform:GetComponent(typeof(CCommonUITween))
    UIEventListener.Get(self.CloseButton).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnCloseButtonClick()
    end)
    UIEventListener.Get(self.PopViewCloseBg).onClick = DelegateFactory.VoidDelegate(function (go)
        self:ClosePopView()
    end)
end

function LuaCompetitionHonorWnd:Init()
    self.m_ViewId = 0
    self.PlayerList:SetActive(false)
    self.PlayerTemplate:SetActive(false)
    self.PopViewCloseBg:SetActive(false)
    self.CompetitionHonorIntroduceView:SetActive(false)
    self.CompetitionHonorModelView.gameObject:SetActive(false)
    self.m_ViewTbl = {}
    self.m_ViewPageTbl = {}
    self.m_PlayerTbl = {}
    self:AddToViewTbl(1, self.DouHunTanEnter, self.DouHunTanView)
    self:AddToViewTbl(2, self.QMPKEnter, self.QMPKView)
    self:AddToViewTbl(3, self.StarBiWuEnter, self.StarBiWuView)
    self.m_Animation:Play("competitionhonorwnd_show")
end

function LuaCompetitionHonorWnd:AddToViewTbl(index, enterBtn, view)
    self.m_ViewTbl[index] = {}
    self.m_ViewTbl[index].enterBtn = enterBtn
    self.m_ViewTbl[index].view = view
    view.gameObject:SetActive(false)
    UIEventListener.Get(enterBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self:SetView(index)
    end)
end

function LuaCompetitionHonorWnd:OnQueryCompetitionHonorChapionGroupResult(chapionInfosU)
    self.PlayerList:SetActive(true)
    for i = 1, #self.m_PlayerTbl do
        if self.m_PlayerTbl[i] then
            self.m_PlayerTbl[i].itemGo:SetActive(false)
        end
    end
    local chapionInfos = MsgPackImpl.unpack(chapionInfosU)
    for i = 0, chapionInfos.Count - 1, 7 do
        local playerId = chapionInfos[i]
        local name = chapionInfos[i + 1]
        local server = chapionInfos[i + 2]
        local class = chapionInfos[i + 3]
        local gender = chapionInfos[i + 4]
        local appearanceProp = chapionInfos[i + 5]
        local rank = chapionInfos[i + 6]

        if class ~= "" then
            local index = i / 7 + 1
            local go = nil
            if self.m_PlayerTbl[index] then
                go = self.m_PlayerTbl[index].itemGo
            else
                go = CUICommonDef.AddChild(self.PlayerTable.gameObject, self.PlayerTemplate)
                self.m_PlayerTbl[index] = {itemGo = go, identifierStr = go:GetInstanceID()}
                UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function (go)
                    self:OnPlayerClick(index)
                end)
            end
            go:SetActive(true)

            self.m_PlayerTbl[index].playerId = playerId
            self.m_PlayerTbl[index].name = name
            self.m_PlayerTbl[index].server = server
            self.m_PlayerTbl[index].class = class
            self.m_PlayerTbl[index].gender = gender
            self.m_PlayerTbl[index].appearanceProp = appearanceProp
            self.m_PlayerTbl[index].rank = rank
        
            go.gameObject:SetActive(true)
            go.transform:Find("NameLabel"):GetComponent(typeof(UILabel)).text = name
            go.transform:Find("ServerLabel"):GetComponent(typeof(UILabel)).text = server
            self:SetRoModel(index, class, gender, appearanceProp)
        end

    end
    self.PlayerTable:Reposition()
end

function LuaCompetitionHonorWnd:SetRoModel(index, class, gender, appearanceProp)
    if self.m_PlayerTbl[index].ro == nil then
        local modelTextureLoader = LuaDefaultModelTextureLoader.Create(function(ro)
            self.m_PlayerTbl[index].ro = ro
            self:ResetModel(ro, class, gender, appearanceProp)
        end)
        local modelTexture = self.m_PlayerTbl[index].itemGo.transform:Find("FakeModel/ModelTexture"):GetComponent(typeof(CUITexture))
        modelTexture.mainTexture = CUIManager.CreateModelTexture(self.m_PlayerTbl[index].identifierStr, modelTextureLoader, 180, 0.05, -1, 4.66, false, true, 1, true, false)
    else
        self:ResetModel(self.m_PlayerTbl[index].ro, class, gender, appearanceProp)
    end
end

function LuaCompetitionHonorWnd:ResetModel(ro, class, gender, appearanceProp)
    local fakeAppearance = CreateFromClass(CPropertyAppearance)
    fakeAppearance:LoadFromString(appearanceProp, CommonDefs.ConvertIntToEnum(typeof(EnumClass), class), CommonDefs.ConvertIntToEnum(typeof(EnumGender), gender))
    if CommonDefs.ConvertIntToEnum(typeof(EnumClass), class) == EnumClass.YingLing then
        fakeAppearance.YingLingState = gender == 0 and EnumYingLingState.eMale or EnumYingLingState.eFemale
    end
    fakeAppearance.IntesifySuitId = 0       -- 关特效
    fakeAppearance.Wing = 0                 -- 关翅膀
    fakeAppearance.TalismanAppearance = 0   -- 关法宝
    fakeAppearance.HideBackPendant = 1      -- 关背饰
    fakeAppearance.HideGemFxToOtherPlayer = 1   -- 关武器石之灵
    fakeAppearance.Equipment[1] = 0         -- 卸武器
    CClientMainPlayer.LoadResource(ro, fakeAppearance, false, 1.0, 0, false, 0, false, 0, false, nil, false)
    -- 调整模型位置
    local data = CompetitionHonor_HeadPos.GetData(class * 100 + gender)
    local pos = data and data.Position or CompetitionHonor_HeadPos.GetData(200).Position
    ro.Position = Vector3(pos[0], pos[1], pos[2])
    ro.transform.localRotation = Quaternion.Euler(0, pos[3] or 0, 0)
    ro:Preview("stand01", 0)
end

function LuaCompetitionHonorWnd:OnQueryCompetitionSeasonDataResult(GameSeasonData_U)
    local data = g_MessagePack.unpack(GameSeasonData_U)
    for i = 1, #data, 2 do
        self.m_ViewPageTbl[data[i]] = data[i+1]
    end
end

function LuaCompetitionHonorWnd:SetEnterBtnEnabled(isEnabled)
    for _, view in pairs(self.m_ViewTbl) do
        view.enterBtn:GetComponent(typeof(BoxCollider)).enabled = isEnabled
    end
end
--@region UIEvent
function LuaCompetitionHonorWnd:SetView(index)
    local path = "UI/Texture/Transparent/Material/zhongqiu2022mainwnd_btn_close_04.mat"
    self.CloseButton.transform:Find("Close"):GetComponent(typeof(CUITexture)):LoadMaterial(path)
    self.m_ViewId = index
    local view = self.m_ViewTbl[index].view
    view:InitBook(index, self.m_ViewPageTbl[index])
    self.PlayerList:SetActive(true)

    if index == 1 then
        self.m_Animation:Play("competitionhonorwnd_book_douhun")
    elseif index == 2 then
        self.m_Animation:Play("competitionhonorwnd_book_qmpk")
    elseif index == 3 then
        self.m_Animation:Play("competitionhonorwnd_book_kuafu")
    end
    view.gameObject:SetActive(true)
    self.m_ViewTbl[index].enterBtn.gameObject:SetActive(false)
    self:SetEnterBtnEnabled(false)
end

function LuaCompetitionHonorWnd:OnCloseButtonClick()
    if self.m_ViewId == 0 then
        CUIManager.CloseUI(CLuaUIResources.CompetitionHonorWnd)
    else
        self:ClosePopView()
        if self.CompetitionHonorIntroduceView.activeSelf then
            self.CompetitionHonorIntroduceView:SetActive(false)
        else
            self.BookChooseView:SetActive(true)
            self.m_ViewTbl[self.m_ViewId].view.gameObject:SetActive(false)
            self.m_ViewTbl[self.m_ViewId].enterBtn.gameObject:SetActive(true)
            self.m_ViewId = 0
            local path = "UI/Texture/Transparent/Material/zhongqiu2022mainwnd_btn_close_03.mat"
            self.CloseButton.transform:Find("Close"):GetComponent(typeof(CUITexture)):LoadMaterial(path)
            self.PlayerList:SetActive(false)
            self:SetEnterBtnEnabled(true)
        end
    end
end

function LuaCompetitionHonorWnd:OnPlayerClick(index)
    local info = self.m_PlayerTbl[index]
    if info and info.playerId then
        self.m_CompetitionHonorModelViewOpen = true
        self.PopViewCloseBg:SetActive(true)
        self.CompetitionHonorModelView.gameObject:SetActive(true)
        self.m_ModelViewUITween:PlayAppearAnimation()
        self.CompetitionHonorModelView:UpdateModel(info.playerId, info.class, info.gender, info.name, info.server, info.rank, info.appearanceProp, Vector3(0, -1.12, 4.66))
        local itemPos = self.m_PlayerTbl[index].itemGo.transform.position
        local modelViewPos = self.CompetitionHonorModelView.transform.position
        self.CompetitionHonorModelView.transform.position = Vector3(itemPos.x, modelViewPos.y, modelViewPos.z)
    end
end

function LuaCompetitionHonorWnd:ClosePopView()
    -- 关闭模型弹窗
    if self.m_CompetitionHonorModelViewOpen then
        self.m_CompetitionHonorModelViewOpen = false
        self.PopViewCloseBg:SetActive(false)
        self.m_ModelViewUITween:PlayDisapperAnimation()
    end
end
--@endregion UIEvent

function LuaCompetitionHonorWnd:OnEnable()
    Gac2Gas.QueryCompetitionSeasonData()
    g_ScriptEvent:AddListener("QueryCompetitionHonorChapionGroupResult", self, "OnQueryCompetitionHonorChapionGroupResult")
    g_ScriptEvent:AddListener("QueryCompetitionSeasonDataResult", self, "OnQueryCompetitionSeasonDataResult")
end

function LuaCompetitionHonorWnd:OnDisable()
    g_ScriptEvent:RemoveListener("QueryCompetitionHonorChapionGroupResult", self, "OnQueryCompetitionHonorChapionGroupResult")
    g_ScriptEvent:RemoveListener("QueryCompetitionSeasonDataResult", self, "OnQueryCompetitionSeasonDataResult")
    if self.m_PlayerTbl then
        for i = 1, #self.m_PlayerTbl do
            if self.m_PlayerTbl[i] then
                CUIManager.DestroyModelTexture(self.m_PlayerTbl[i].identifierStr)
            end
        end
    end
end