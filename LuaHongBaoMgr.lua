local CHongBaoMgr=import "L10.UI.CHongBaoMgr"
local EnumHongbaoType = import "L10.UI.EnumHongbaoType"
local EnumHongBaoEvent=import "L10.UI.EnumHongBaoEvent"
local CServerTimeMgr=import "L10.Game.CServerTimeMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CClientMainPlayer= import "L10.Game.CClientMainPlayer"
local PlayerPrefs = import "UnityEngine.PlayerPrefs"
CLuaHongBaoMgr = {}
CLuaHongBaoMgr.m_HongbaoInfo = nil
CLuaHongBaoMgr.m_HongbaoDataTable = {}
EnumHongBaoDisplayType = {
	eWorld = 1,
	eGuild = 2,
    eGift = 3,
    eSect = 4,
}

CLuaHongBaoMgr.m_HongbaoCoverInfo = nil     -- 红包封面获取情况信息
CLuaHongBaoMgr.m_CurShowHongBaoCoverData = nil

CLuaHongBaoMgr.m_DetailHongBaoCoverType = nil

CLuaHongBaoMgr.m_PopupMenuData = nil

CLuaHongBaoMgr.m_HongBaoColorList = nil

function CLuaHongBaoMgr.ShowHongBaoDetailWnd(hongbaoId,eventType,coverType)
    if CHongBaoMgr.s_EnableHongBao then
        CHongBaoMgr.s_HongBaoId = hongbaoId
        CLuaHongBaoMgr.m_DetailHongBaoCoverType = coverType
        local coverData = HongBao_Cover.GetData(coverType)
        CLuaHongBaoMgr.m_HongBaoColorList = coverData and coverData.ColorList or nil
        CHongBaoMgr.s_HongBaoEvent = CommonDefs.ConvertIntToEnum(typeof(EnumHongBaoEvent), eventType)
        if CHongBaoMgr.m_HongbaoType == EnumHongbaoType.GuildHongbao
        or CHongBaoMgr.m_HongbaoType == EnumHongbaoType.WorldHongbao
        or CHongBaoMgr.m_HongbaoType == EnumHongbaoType.SystemHongbao then
            if eventType == 3 or eventType == 2 then--EnumHongBaoEvent.HuaKui or eventType == EnumHongBaoEvent.BangHua then
                CUIManager.ShowUI(CIndirectUIResources.HongbaoHuakuiDetailWnd)
            else
                CUIManager.ShowUI(CIndirectUIResources.HongbaoDetailNewWnd)
            end
        else
            CUIManager.ShowUI(CIndirectUIResources.HongbaoDetailWnd)
        end
    end
end
function CLuaHongBaoMgr.SnatchVoiceHongbao(hongbaoInfo)
    CLuaHongBaoMgr.m_HongbaoInfo = hongbaoInfo
    CUIManager.ShowUI(CUIResources.SnatchVoiceHongBaoWnd)
end

--圣诞期间
function CLuaHongBaoMgr.IsShengDan()
    local month1, day1, month2,day2 = string.match(HongBao_Setting.GetData().ChristmasUIChangeTime, "(%d+).(%d+);(%d+).(%d+)")
    local month1, day1, month2,day2 = tonumber(month1),tonumber(day1),tonumber(month2),tonumber(day2)

    local time=CServerTimeMgr.Inst:GetZone8Time()
    local month,day=time.Month,time.Day

    local open = false
    if month1>month2 then--跨年
        if (month>month1 or (month==month1 and day>=day1)) or (month<month2 or (month==month2 and day<day2)) then
            open=true
        end
    else
        if (month>month1 or (month==month1 and day>=day1)) and (month<month2 or (month==month2 and day<day2)) then
            open=true
        end
    end
    return open
end
-- 设置默认红包封面
function CLuaHongBaoMgr.SetCurHongBaoCover(CoverId)
    Gac2Gas.SetCrtSelectedHongBaoType(CoverId)
end

function CLuaHongBaoMgr.ShowHongBaoCoverInfoWnd(hongbaoData)
    CLuaHongBaoMgr.m_CurShowHongBaoCoverData = hongbaoData
    CUIManager.ShowUI(CLuaUIResources.HongBaoCoverInfoWnd)
end

function CLuaHongBaoMgr.GetCurHongBaoCover()
    local MainPlayer = CClientMainPlayer.Inst
    if MainPlayer then
        if not MainPlayer.PlayProp.CrtSelectedHongBaoIdx or MainPlayer.PlayProp.CrtSelectedHongBaoIdx < 0 then
            return 0
        end
        if CLuaHongBaoMgr.HasGetHongBaoCover(MainPlayer.PlayProp.CrtSelectedHongBaoIdx) then
            return MainPlayer.PlayProp.CrtSelectedHongBaoIdx
        end
    end
    return 0
end

function CLuaHongBaoMgr.ShowPopupMenu(worldPos,type,selectIndex,ButtonList) 
    -- ButtonList.texturePath
    -- ButtonList.name
    -- ButtonList.describe
    CLuaHongBaoMgr.m_PopupMenuData = {}
    CLuaHongBaoMgr.m_PopupMenuData.type = type
    CLuaHongBaoMgr.m_PopupMenuData.selectIndex = selectIndex    -- 默认选中高亮
    CLuaHongBaoMgr.m_PopupMenuData.worldPos = worldPos
    CLuaHongBaoMgr.m_PopupMenuData.ButtonList = ButtonList
    CUIManager.ShowUI(CLuaUIResources.HongBaoCoverPopupMenu)
end

function CLuaHongBaoMgr.OnClickPopupMenuBtn(index)
    if not CLuaHongBaoMgr.m_PopupMenuData then return end
    g_ScriptEvent:BroadcastInLua("ClickPopupMenuWithPic",index)
end

function CLuaHongBaoMgr.HasGetHongBaoCover(id)
    local data = HongBao_Cover.GetData(id)
    if not data then return false end

    local hasGet = CClientMainPlayer.Inst.PlayProp.HongBaoCover:GetBit(id) -- 从服务器拿数据
    local isInTime = false
    if id == 0 then return true -- 默认封面
    elseif id == 1 then hasGet = LuaHalloween2022Mgr.HasGetHongBaoCover() end    -- 万圣节红包封面
    local endStamp = CServerTimeMgr.Inst:GetTimeStampByStr(data.EndTime)
    local StartStamp = CServerTimeMgr.Inst:GetTimeStampByStr(data.StartTime)
    local nowStamp = CServerTimeMgr.Inst.timeStamp
    if nowStamp >= StartStamp and (nowStamp < endStamp or endStamp == 0) then -- 时间判断
        isInTime = true
    end
    return hasGet and isInTime
end

-- 红包封面解锁
function CLuaHongBaoMgr.UnlockHongBaoCover(coverType)
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp then
        CClientMainPlayer.Inst.PlayProp.HongBaoCover:SetBit(coverType, true)
        g_ScriptEvent:BroadcastInLua("UpdateHongBaoCoverInfo")
    end
end

-- 是否有新的封面
function CLuaHongBaoMgr.HasNewCover()
    local hasNew = false
    HongBao_Cover.Foreach(function(k,v)
        if k ~= 0 and CLuaHongBaoMgr.HasGetHongBaoCover(k) then
            if CLuaHongBaoMgr.HongBaoCoverIsNew(k) then 
                hasNew = true  
                return
            end
        end
    end)
    return hasNew
end

function CLuaHongBaoMgr.HongBaoCoverIsNew(id)
    if id == 0 then return false end
    if not CClientMainPlayer.Inst then return false end
    return PlayerPrefs.GetInt(CClientMainPlayer.Inst.Id.."HasClickHongBaoCover"..tostring(id)) ~= 1
end

function CLuaHongBaoMgr.GetHongBaoRetByAmount(amount)
    local lingyu = HongBao_Setting.GetData().Show_Different_LingYu
    local ret = -1
    for i = 0,lingyu.Length do
        if amount < lingyu[i] then
            ret = i
            break
        end
    end
    if ret < 0 then
        ret = lingyu.Length - 1
    end
    return ret
end

function CLuaHongBaoMgr.GetOpenHongBaoIconByAmount(amount,coverType)
    local texPath = ""
    local ret = CLuaHongBaoMgr.GetHongBaoRetByAmount(amount)
    local CoverData = HongBao_Cover.GetData(coverType)
    if CoverData then 
        texPath = "UI/Texture/Transparent/Material/"..string.gsub(CoverData.OpenResource,"{TypeName}",tostring(ret + 1))..".mat"
    end
    return texPath
end


CHongBaoMgr.m_GetHongBaoIconByAmount_CS2LuaHook = function (this,amount,isBigIcon,coverType)
    local texPath = ""
    local ret = CLuaHongBaoMgr.GetHongBaoRetByAmount(amount)
    local CoverData = HongBao_Cover.GetData(coverType)
    if CoverData then 
        if isBigIcon then 
            texPath = "UI/Texture/Transparent/Material/"..string.gsub(CoverData.DifferentResource,"{TypeNum}",tostring(ret + 1))..".mat"
        else
            texPath = "UI/Texture/Transparent/Material/"..string.gsub(CoverData.IconResource,"{TypeNum}",tostring(ret + 1))..".mat"
        end
    else
        if isBigIcon then
            texPath = HongBao_Setting.GetData().Show_Different_Big_Icons[ret]
        else
            texPath = HongBao_Setting.GetData().Show_Different_Small_Icons[ret]
        end
    end

    return texPath
end