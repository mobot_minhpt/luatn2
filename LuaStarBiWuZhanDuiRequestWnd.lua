local CButton = import "L10.UI.CButton"
local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local EChatPanel = import "L10.Game.EChatPanel"
local Profession = import "L10.Game.Profession"
local AlignType = import "CPlayerInfoMgr+AlignType"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"

LuaStarBiWuZhanDuiRequestWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaStarBiWuZhanDuiRequestWnd, "RejectButton", "RejectButton", CButton)
RegistChildComponent(LuaStarBiWuZhanDuiRequestWnd, "AcceptButton", "AcceptButton", CButton)
RegistChildComponent(LuaStarBiWuZhanDuiRequestWnd, "ClearButton", "ClearButton", CButton)
RegistChildComponent(LuaStarBiWuZhanDuiRequestWnd, "ApplicationAdvScrollView", "ApplicationAdvScrollView", QnAdvanceGridView)
RegistChildComponent(LuaStarBiWuZhanDuiRequestWnd, "Empty", "Empty", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaStarBiWuZhanDuiRequestWnd,"m_CurrentRequestPlayerIndex")
RegistClassMember(LuaStarBiWuZhanDuiRequestWnd,"m_RequestPlayers")
function LuaStarBiWuZhanDuiRequestWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.RejectButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRejectButtonClick()
	end)


	
	UIEventListener.Get(self.AcceptButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnAcceptButtonClick()
	end)


	
	UIEventListener.Get(self.ClearButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnClearButtonClick()
	end)


    --@endregion EventBind end

	self.m_CurrentRequestPlayerIndex = 0
	self.m_RequestPlayers = nil
end

function LuaStarBiWuZhanDuiRequestWnd:Init()
	self.ApplicationAdvScrollView.m_DataSource=DefaultTableViewDataSource.CreateByCount(0,function(item,row)
        if row % 2 == 1 then
            item:SetBackgroundTexture(g_sprites.OddBgSpirite)
        else
            item:SetBackgroundTexture(g_sprites.EvenBgSprite)
        end

        self:InitRequestPlayerItem(item,row,self.m_RequestPlayers[row+1])
    end)
	self.ApplicationAdvScrollView.OnSelectAtRow =DelegateFactory.Action_int(function (row)
        self:OnRequestPlayerItemClicked(row)
    end)
	if not CLuaStarBiwuMgr.m_RequestZhanDuiMemberList then
		Gac2Gas.RequestStarBiwuZhanDuiApplyList(1)
	else
		self:OnReplyRequestPlayers(CLuaStarBiwuMgr.m_RequestZhanDuiMemberList)
	end
end

function LuaStarBiWuZhanDuiRequestWnd:InitRequestPlayerItem(item,row,player)
	local transform=item.transform
    local m_BgSprite = transform:GetComponent(typeof(UISprite))
    local m_PlayerProfessionSprite = transform:Find("class"):GetComponent(typeof(UISprite))
    m_PlayerProfessionSprite.spriteName = Profession.GetIconByNumber(player.m_Clazz)

    local transTbl = {"Name", "Sever", "Capacity", "EquipScore", "Score"}
    local valueTbl = {player.m_Name, player.m_Server, player.m_Capacity, player.m_Score, player.m_StarScore}
    for i = 1, #transTbl do
      transform:Find(transTbl[i]):GetComponent(typeof(UILabel)).text = valueTbl[i]
    end
end

function LuaStarBiWuZhanDuiRequestWnd:OnRequestPlayerItemClicked(index)
    self.m_CurrentRequestPlayerIndex = index + 1
    if index < #self.m_RequestPlayers then
        CPlayerInfoMgr.ShowPlayerPopupMenu(
            self.m_RequestPlayers[index+1].m_Id,
            EnumPlayerInfoContext.QMPK,
            EChatPanel.Undefined,
            nil,
            nil,
            Vector3.zero,
            AlignType.Default)
    end
end
function LuaStarBiWuZhanDuiRequestWnd:OnReplyRequestPlayers( players )
    self.m_CurrentRequestPlayerIndex = 0
    self.m_RequestPlayers = players
	if #self.m_RequestPlayers <= 0 then
		self.Empty.gameObject:SetActive(true)
		self.ApplicationAdvScrollView.gameObject:SetActive(false)
		return
	else
		self.Empty.gameObject:SetActive(false)
		self.ApplicationAdvScrollView.gameObject:SetActive(true)
	end
    if not self.m_ApplicationFirstFlag then
        self.ApplicationAdvScrollView.m_DataSource.count=#self.m_RequestPlayers
        self.ApplicationAdvScrollView:ReloadData(true, false)
    end
end
--@region UIEvent

function LuaStarBiWuZhanDuiRequestWnd:OnRejectButtonClick()
	if self.m_CurrentRequestPlayerIndex > 0 and self.m_CurrentRequestPlayerIndex <= #self.m_RequestPlayers then
        Gac2Gas.RefuseApplyPlayerJoinStarBiwuZhanDui(self.m_RequestPlayers[self.m_CurrentRequestPlayerIndex].m_Id)
    else
        g_MessageMgr:ShowMessage("DOUHUN_NEED_ONE_PLAYER")
    end
end

function LuaStarBiWuZhanDuiRequestWnd:OnAcceptButtonClick()
	if self.m_CurrentRequestPlayerIndex > 0 and self.m_CurrentRequestPlayerIndex <= #self.m_RequestPlayers then
        Gac2Gas.AcceptApplyPlayerJoinStarBiwuZhanDui(self.m_RequestPlayers[self.m_CurrentRequestPlayerIndex].m_Id)
    else
        g_MessageMgr:ShowMessage("DOUHUN_NEED_ONE_PLAYER")
    end
end

function LuaStarBiWuZhanDuiRequestWnd:OnClearButtonClick()
	Gac2Gas.RequestClearStarBiwuZhanDuiApplyList()
end


--@endregion UIEvent

function LuaStarBiWuZhanDuiRequestWnd:OnEnable()
	g_ScriptEvent:AddListener("ReplyStarBiwuZhanDuiApplyList", self, "OnReplyRequestPlayers")
end

function LuaStarBiWuZhanDuiRequestWnd:OnDisable()
	g_ScriptEvent:RemoveListener("ReplyStarBiwuZhanDuiApplyList", self, "OnReplyRequestPlayers")
end