local CButton      = import "L10.UI.CButton"
local CChatLinkMgr = import "CChatLinkMgr"

LuaCarnivalCollectWnd = class()

RegistClassMember(LuaCarnivalCollectWnd, "location2Trans")
RegistClassMember(LuaCarnivalCollectWnd, "awardButton")
RegistClassMember(LuaCarnivalCollectWnd, "awardButtonRedDot")
RegistClassMember(LuaCarnivalCollectWnd, "location2StampIds")
RegistClassMember(LuaCarnivalCollectWnd, "taskStatusTemplate")

RegistClassMember(LuaCarnivalCollectWnd, "beforeTaskStatus")
RegistClassMember(LuaCarnivalCollectWnd, "tweeners")

function LuaCarnivalCollectWnd:Awake()
	self:InitTable()

	self.taskStatusTemplate = self.transform:Find("Anchor/TaskStatusTemplate").gameObject
	self.taskStatusTemplate:SetActive(false)
	self.awardButton = self.transform:Find("Anchor/Bottom/AwardButton"):GetComponent(typeof(CButton))
	self.awardButtonRedDot = self.awardButton.transform:Find("RedDot").gameObject
	UIEventListener.Get(self.awardButton.gameObject).onClick = LuaUtils.VoidDelegate(function (go)
		self:OnAwardButtonClick()
	end)

	UIEventListener.Get(self.transform:Find("Anchor/Bottom/TipButton").gameObject).onClick = LuaUtils.VoidDelegate(function (go)
		self:OnTipButtonClick()
	end)
end

function LuaCarnivalCollectWnd:OnEnable()
	g_ScriptEvent:AddListener("SyncStampCollectProgress", self, "OnSyncStampCollectProgress")
	g_ScriptEvent:AddListener("SyncCarnivalPrizedLocationId", self, "OnSyncCarnivalPrizedLocationId")
	g_ScriptEvent:AddListener("SyncCarnivalBigTicketCount", self, "OnSyncTicketCount")
	g_ScriptEvent:AddListener("SyncCarnivalSmallTicketCount", self, "OnSyncTicketCount")
end

function LuaCarnivalCollectWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SyncStampCollectProgress", self, "OnSyncStampCollectProgress")
	g_ScriptEvent:RemoveListener("SyncCarnivalPrizedLocationId", self, "OnSyncCarnivalPrizedLocationId")
	g_ScriptEvent:RemoveListener("SyncCarnivalBigTicketCount", self, "OnSyncTicketCount")
	g_ScriptEvent:RemoveListener("SyncCarnivalSmallTicketCount", self, "OnSyncTicketCount")
end

function LuaCarnivalCollectWnd:OnSyncStampCollectProgress(stampId, newCount)
	self:UpdateBeforeTaskStatus()
	self:ClearTweeners()
	self:UpdateLocationStatus(tonumber(JiaNianHua_Stamp.GetData(stampId).Location))
end

function LuaCarnivalCollectWnd:OnSyncCarnivalPrizedLocationId(locationId)
	self:UpdateBeforeTaskStatus()
	self:ClearTweeners()
	self:UpdateLocationStatus(locationId)
end

function LuaCarnivalCollectWnd:OnSyncTicketCount()
	self:UpdateAwardButton()
end


function LuaCarnivalCollectWnd:Init()
	self.location2StampIds = LuaCarnivalCollectMgr:ParseLocation2StampIds()

	self:UpdateBeforeTaskStatus()
	self:ClearTweeners()
	for id = 1, 5 do
		self:UpdateLocationStatus(id)
	end
	self:UpdateAwardButton()
	Gac2Gas.UpdataCarnivalCollectTaskInfo()
end

function LuaCarnivalCollectWnd:InitTable()
	local tableRoot = self.transform:Find("Anchor/Table")
	self.location2Trans = {}
	for id = 1, 5 do
		self.location2Trans[id] = tableRoot:Find(JiaNianHua_Location.GetData(id).PublicMapName)
	end
end

function LuaCarnivalCollectWnd:UpdateBeforeTaskStatus()
	self.beforeTaskStatus = {}
	local mainPlayer = CClientMainPlayer.Inst
	if mainPlayer and CommonDefs.DictContains_LuaCall(mainPlayer.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eCarnival2023CollectData) then
		local playDataU = CommonDefs.DictGetValue_LuaCall(mainPlayer.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eCarnival2023CollectData)
		local list = g_MessagePack.unpack(playDataU.Data.Data)
		for i = 1, #list, 2 do
			self.beforeTaskStatus[tonumber(list[i])] = tonumber(list[i + 1])
		end
	end
end

-- 更新状态
function LuaCarnivalCollectWnd:UpdateLocationStatus(id)
	local transRoot = self.location2Trans[id]

	local beforeFinishCount = self:GetBeforeTaskFinishCount(id)
	local beforeIsFinished = beforeFinishCount >= 6
	local finishCount = self:GetTaskFinishCount(id)
	local isFinished = finishCount >= 6
	local isReceived = LuaCarnivalCollectMgr:GetLocationPrized(id)
	local canReceived = isFinished and not isReceived

	local going = transRoot:Find("Status/Going")
	going.gameObject:SetActive(not isFinished)
	if not isFinished then
		local grid = going:Find("Grid")
		if grid.childCount ~= 6 then
			Extensions.RemoveAllChildren(grid.transform)
			for i = 1, 6 do
				NGUITools.AddChild(grid.gameObject, self.taskStatusTemplate):SetActive(true)
			end
			grid:GetComponent(typeof(UIGrid)):Reposition()
		end

		for i = 1, 6 do
			local child = grid:GetChild(i - 1)
			child:Find("Normal").gameObject:SetActive(beforeFinishCount < i)
			child:Find("Highlight").gameObject:SetActive(beforeFinishCount >= i)
		end

		if beforeFinishCount < finishCount then
			local duration = (finishCount - beforeFinishCount) * 0.5
			local tweener = LuaTweenUtils.TweenInt(beforeFinishCount, finishCount, duration, function(val)
				self:OnTweenUpdate(id, val, beforeFinishCount)
			end)
			table.insert(self.tweeners, tweener)
		end
	end

	if (not isFinished and beforeFinishCount < finishCount) or (isFinished and not beforeIsFinished) then
		local tweener = LuaTweenUtils.TweenFloat(0, 1, 0.4, function()
		end, function()
			self.location2Trans[id]:Find("Status/Bg/Bg_glow").gameObject:SetActive(true)
		end)
		table.insert(self.tweeners, tweener)
	end

	transRoot:Find("Status/CanReceive").gameObject:SetActive(canReceived)
	transRoot:Find("Status/Finished").gameObject:SetActive(isFinished)
	UIEventListener.Get(transRoot.gameObject).onClick = LuaUtils.VoidDelegate(function (go)
		self:OnLocationClick(id)
	end)
end

function LuaCarnivalCollectWnd:OnTweenUpdate(id, val, beforeFinishCount)
	if val > beforeFinishCount then
		local transRoot = self.location2Trans[id]
		local grid = transRoot:Find("Status/Going/Grid")
		local child = grid:GetChild(val - 1)
		child:Find("Normal").gameObject:SetActive(false)
		local highlight = child:Find("Highlight")
		highlight.gameObject:SetActive(true)
		highlight:Find("CUIFx").gameObject:SetActive(true)
	end
end

-- 获取上次打开界面时任务完成数量
function LuaCarnivalCollectWnd:GetBeforeTaskFinishCount(locationId)
	local stampIds = self.location2StampIds[locationId]

	local count = 0
	for _, id in ipairs(stampIds) do
		local progress = self.beforeTaskStatus[id] or 0
		if progress >= LuaCarnivalCollectMgr:GetStampTotalProgress(id) then
			count = count + 1
		end
	end
	return count
end

function LuaCarnivalCollectWnd:GetTaskFinishCount(locationId)
	local stampIds = self.location2StampIds[locationId]

	local count = 0
	for _, id in ipairs(stampIds) do
		if LuaCarnivalCollectMgr:GetStampProgress(id) >= LuaCarnivalCollectMgr:GetStampTotalProgress(id) then
			count = count + 1
		end
	end
	return count
end

function LuaCarnivalCollectWnd:UpdateAwardButton()
	local hasTicket = LuaCarnivalCollectMgr:CarnivalCollectTicketCheck()
	self.awardButton.Text = hasTicket and LocalString.GetString("抽  奖") or LocalString.GetString("预览奖池")
	self.awardButtonRedDot:SetActive(hasTicket)
end

function LuaCarnivalCollectWnd:ClearTweeners()
	if self.tweeners then
		for _, tweener in pairs(self.tweeners) do
			LuaTweenUtils.Kill(tweener, true)
		end
	end
	self.tweeners = {}
end

function LuaCarnivalCollectWnd:OnDestroy()
	self:ClearTweeners()
end


function LuaCarnivalCollectWnd:OnAwardButtonClick()
	CUIManager.ShowUI(CLuaUIResources.CarnivalDrawWnd)
end

function LuaCarnivalCollectWnd:OnTipButtonClick()
	g_MessageMgr:ShowMessage("CARNIVAL_COLLECT_TIP")
end

function LuaCarnivalCollectWnd:OnLocationClick(id)
	local finishCount = self:GetTaskFinishCount(id)
	local isFinished = finishCount >= 6
	local isReceived = LuaCarnivalCollectMgr:GetLocationPrized(id)
	local canReceived = isFinished and not isReceived

	if canReceived then
		local data = JiaNianHua_Location.GetData(id)
		local sceneName = PublicMap_PublicMap.GetData(data.SceneId).Name
		local locationStr = CChatLinkMgr.TranslateToNGUIText(data.NPCLocation, false)
		g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("CARNIVAL_COLLECT_GET_TICKET_CONFIRM", sceneName, sceneName), function()
			CUIManager.CloseUI(CLuaUIResources.CarnivalCollectWnd)
			CUIManager.CloseUI(CLuaUIResources.Carnival2023MainWnd)
			local linkEnd = CommonDefs.StringIndexOf_String(locationStr, "]")
			local url = CommonDefs.StringSubstring2(locationStr, 5, linkEnd - 5)
			CChatLinkMgr.ProcessLinkClick(url, nil)
		end, nil, LocalString.GetString("去领奖"), nil, false)
	elseif not isFinished then
		LuaCarnivalCollectMgr.locationId = id
		CUIManager.ShowUI(CLuaUIResources.CarnivalCollectSubWnd)
	else
		g_MessageMgr:ShowMessage("CARNIVAL_COLLECT_LOCATION_IS_RECEIVED")
	end
end
