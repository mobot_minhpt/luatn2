local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local GameObject = import "UnityEngine.GameObject"
local CUIFx = import "L10.UI.CUIFx"

LuaSDXYSucceedWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaSDXYSucceedWnd, "TimeLabel", "TimeLabel", UILabel)
RegistChildComponent(LuaSDXYSucceedWnd, "RankLabel", "RankLabel", UILabel)
RegistChildComponent(LuaSDXYSucceedWnd, "GiftIcon", "GiftIcon", CUITexture)
RegistChildComponent(LuaSDXYSucceedWnd, "GetTag", "GetTag", GameObject)
RegistChildComponent(LuaSDXYSucceedWnd, "FxNode", "FxNode", CUIFx)
RegistChildComponent(LuaSDXYSucceedWnd, "RankBtn", "RankBtn", GameObject)

--@endregion RegistChildComponent end

function LuaSDXYSucceedWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.RankBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRankBtnClick()
	end)


    --@endregion EventBind end
end

function LuaSDXYSucceedWnd:Init()
	self.TimeLabel.text = LuaChristmas2021Mgr.GetRemainTimeText(LuaChristmas2021Mgr.MyPassDuration)
	self.RankLabel.text = SafeStringFormat3(LocalString.GetString("全服第%d名"),LuaChristmas2021Mgr.MySDXYRank)

	local mailId = Christmas2021_Setting.GetData().ShengDanXingYuMailId
	local mail = Mail_Mail.GetData(mailId)
	if mail then
		local items = mail.Items
		if items and items ~= "" then
			local itemId,_,_ = string.match(items,"(%d+),(%d+),(%d+);")
			itemId = tonumber(itemId)
			self.GiftIcon:LoadMaterial(Item_Item.GetData(itemId).Icon)
		end
	end
end

--@region UIEvent

function LuaSDXYSucceedWnd:OnRankBtnClick()
	LuaChristmas2021Mgr.OpenXingYuRankWnd()
end

--@endregion UIEvent

