local UILabel = import "UILabel"

LuaHalloween2021SpyTipWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaHalloween2021SpyTipWnd, "ReadMeLabel", "ReadMeLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaHalloween2021SpyTipWnd,"m_Tick")
function LuaHalloween2021SpyTipWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaHalloween2021SpyTipWnd:Init()
    self.ReadMeLabel.text = g_MessageMgr:FormatMessage("Halloween2021Spy_Tip")
    self:CancelTick()
    self.m_Tick = RegisterTickOnce(function()
        CUIManager.CloseUI(CLuaUIResources.Halloween2021SpyTipWnd)
    end, 10000)
end

function LuaHalloween2021SpyTipWnd:OnDisable()
    self:CancelTick()
end

--@region UIEvent

--@endregion UIEvent
function LuaHalloween2021SpyTipWnd:CancelTick()
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
    end
end
