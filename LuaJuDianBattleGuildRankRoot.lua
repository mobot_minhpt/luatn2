local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"

LuaJuDianBattleGuildRankRoot = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaJuDianBattleGuildRankRoot, "Self", "Self", GameObject)
RegistChildComponent(LuaJuDianBattleGuildRankRoot, "Rank", "Rank", GameObject)

--@endregion RegistChildComponent end

function LuaJuDianBattleGuildRankRoot:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaJuDianBattleGuildRankRoot:InitWnd()

end

function LuaJuDianBattleGuildRankRoot:OnRankData(rankDataList)
    local selfGuildId = 0
    if CClientMainPlayer.Inst then
        selfGuildId = CClientMainPlayer.Inst.BasicProp.GuildId
    end

    for i=1, #rankDataList do
        local data = rankDataList[i]
        if selfGuildId == data.guildId then
            self:InitSelf(self.Self, data)
        end

        local rank = data.rankPos
        if rank <= 3 then
            local item = self.Rank.transform:Find(tostring(rank)).gameObject
            self:InitRank(item, data)
        end
    end
end

function LuaJuDianBattleGuildRankRoot:InitRank(item, rankData)
    local name = item.transform:Find("Name"):GetComponent(typeof(UILabel))
    local count = item.transform:Find("Count"):GetComponent(typeof(UILabel))

    name.text = rankData.guildName
    count.text = rankData.score
end

function LuaJuDianBattleGuildRankRoot:InitSelf(item, rankData)
    self:InitRank(item, rankData)

    local texture = item.transform:Find("RankTexture").gameObject
    local label = item.transform:Find("RankLabel"):GetComponent(typeof(UILabel))

    local rank = rankData.rankPos
    if rank <= 3 then
        texture:SetActive(true)
        label.gameObject:SetActive(false)
        for i=1, 3 do
            local icon = texture.transform:Find(tostring(i)).gameObject
            icon:SetActive(i == rank)
        end
    else
        texture:SetActive(false)
        label.gameObject:SetActive(true)
        label.text = tostring(rank)
    end
end

function LuaJuDianBattleGuildRankRoot:OnEnable()
    self:InitWnd()
    -- 请求前四名帮会
    -- Gac2Gas.GuildJuDianQueryTopFourGuildRank()
    g_ScriptEvent:AddListener("GuildJuDianSyncTopFourGuildRank", self, "OnRankData")
end

function LuaJuDianBattleGuildRankRoot:OnDisable()
    g_ScriptEvent:RemoveListener("GuildJuDianSyncTopFourGuildRank", self, "OnRankData")
end

--@region UIEvent

--@endregion UIEvent

