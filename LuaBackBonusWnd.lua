local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local CUIFx = import "L10.UI.CUIFx"
local CUITexture = import "L10.UI.CUITexture"
local Vector3 = import "UnityEngine.Vector3"
local NGUITools = import "NGUITools"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CItemMgr=import "L10.Game.CItemMgr"
local MessageWndManager = import "L10.UI.MessageWndManager"

LuaBackBonusMgr = {}

LuaBackBonusWnd = class()
RegistChildComponent(LuaBackBonusWnd,"closeBtn", GameObject)
RegistChildComponent(LuaBackBonusWnd,"fNode", GameObject)
RegistChildComponent(LuaBackBonusWnd,"templateNode", GameObject)
RegistChildComponent(LuaBackBonusWnd,"titleLabel", UILabel)
RegistChildComponent(LuaBackBonusWnd,"fxNode", CUIFx)


--RegistClassMember(LuaBackBonusWnd, "maxChooseNum")

function LuaBackBonusWnd:Split(str, pat)
   local t = {}  -- NOTE: use {n = 0} in Lua-5.0
   local fpat = "(.-)" .. pat
   local last_end = 1
   local s, e, cap = str:find(fpat, 1)
   while s do
      if s ~= 1 or cap ~= "" then
         table.insert(t,cap)
      end
      last_end = e+1
      s, e, cap = str:find(fpat, last_end)
   end
   if last_end <= #str then
      cap = str:sub(last_end)
      table.insert(t, cap)
   end
   return t
end


function LuaBackBonusWnd:Close()
	CUIManager.CloseUI(CLuaUIResources.BackBonusWnd)
end

function LuaBackBonusWnd:OnEnable()
	--g_ScriptEvent:AddListener("", self, "")
end

function LuaBackBonusWnd:OnDisable()
	--g_ScriptEvent:RemoveListener("", self, "")
end

function LuaBackBonusWnd:InitNode(node,data)
	local rewardItemTable = self:Split(data.Reward,';')
	local rewardItemNode = {node.transform:Find('bonusNode/node1'),node.transform:Find('bonusNode/node2'),node.transform:Find('bonusNode/node3')}

	if #rewardItemTable == 1 then
		rewardItemNode[1].localPosition = Vector3(0,-13,0)
		rewardItemNode[1].gameObject:SetActive(true)
		rewardItemNode[2].gameObject:SetActive(false)
		rewardItemNode[3].gameObject:SetActive(false)
	elseif #rewardItemTable == 2 then
		rewardItemNode[1].localPosition = Vector3(-94,-13,0)
		rewardItemNode[2].localPosition = Vector3(94,-13,0)
		rewardItemNode[1].gameObject:SetActive(true)
		rewardItemNode[2].gameObject:SetActive(true)
		rewardItemNode[3].gameObject:SetActive(false)
	elseif #rewardItemTable == 3 then
		rewardItemNode[1].localPosition = Vector3(0,55,0)
		rewardItemNode[2].localPosition = Vector3(-94,-95,0)
		rewardItemNode[3].localPosition = Vector3(94,-95,0)
		rewardItemNode[1].gameObject:SetActive(true)
		rewardItemNode[2].gameObject:SetActive(true)
		rewardItemNode[3].gameObject:SetActive(true)
	end

	for i,v in ipairs(rewardItemTable) do
		local itemInfoTable = self:Split(v,',')
		local itemId = tonumber(itemInfoTable[1])
		local itemNum = tonumber(itemInfoTable[2])

		local item = Item_Item.GetData(itemId)
		local IconTexture = rewardItemNode[i]:Find('icon'):GetComponent(typeof(CUITexture))
		IconTexture:LoadMaterial(item.Icon)
		if itemNum > 1 then
			rewardItemNode[i]:Find('num'):GetComponent(typeof(UILabel)).text = itemNum
		else
			rewardItemNode[i]:Find('num'):GetComponent(typeof(UILabel)).text = ''
		end
		CommonDefs.AddOnClickListener(rewardItemNode[i]:Find('icon').gameObject, DelegateFactory.Action_GameObject(function(go)
			CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, CItemInfoMgr.AlignType.Default, 0,0,0,0)
		end), false)

	end

	node.transform:Find('info/NameLabel'):GetComponent(typeof(UILabel)).text = data.Name
	self.titleLabel.text = data.Title

	local submitBtn = node.transform:Find('info/getBtn').gameObject
	CommonDefs.AddOnClickListener(submitBtn, DelegateFactory.Action_GameObject(function(go)
		MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("HUILIU_CONFIRM_TEXT",data.Name),
		DelegateFactory.Action(function ()
			local default, pos, backItemId = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, data.Item)
			Gac2Gas.RequestUseHuiLiuItemWithChoice(backItemId,EnumItemPlace.Bag,pos,data.ChoiceNum)
			self:Close()
		end),nil, nil, nil, false)
	end), false)
end

function LuaBackBonusWnd:Init()
	local onCloseClick = function(go)
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.closeBtn,DelegateFactory.Action_GameObject(onCloseClick),false)

	self.templateNode:SetActive(false)
	local itemInfoTable = {}
  if LuaBackBonusMgr.ShowItemId then
		local count = Huiliu_MultiChoice.GetDataCount()
		for i=1,count do
			local data = Huiliu_MultiChoice.GetData(i)
			if data.Item == LuaBackBonusMgr.ShowItemId then
				table.insert(itemInfoTable,data)
			end
		end

		table.sort(itemInfoTable,function(a,b) return a.ChoiceNum < b.ChoiceNum end)
  end

	local pos2T = {Vector3(-297,0,0),Vector3(297,0,0)}
	local pos3T = {Vector3(-438,0,0),Vector3.zero,Vector3(438,0,0)}

	for i,v in ipairs(itemInfoTable) do
		local node = NGUITools.AddChild(self.fNode,self.templateNode)
		node:SetActive(true)
		if #itemInfoTable == 2 then
			node.transform.localPosition = pos2T[i]
		elseif #itemInfoTable == 3 then
			node.transform.localPosition = pos3T[i]
		end
		self:InitNode(node,v)
	end

  self.fxNode:LoadFx("Fx/UI/Prefab/UI_zhaohuilibao.prefab")
end

return LuaBackBonusWnd
