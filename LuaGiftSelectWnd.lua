local Object                    = import "System.Object"
local CItemMgr                  = import "L10.Game.CItemMgr"
local CommonDefs                = import "L10.Game.CommonDefs"
local GameObject                = import "UnityEngine.GameObject"
local QnTableView               = import "L10.UI.QnTableView"
local DefaultTableViewDataSource= import "L10.UI.DefaultTableViewDataSource"
local CItemInfoMgr				= import "L10.UI.CItemInfoMgr"
local AlignType					= import "L10.UI.CItemInfoMgr+AlignType"
local CUIManager                = import "L10.UI.CUIManager"
local UILabel                   = import "UILabel"
local MsgPackImpl               = import "MsgPackImpl"
local DelegateFactory           = import "DelegateFactory"
local LuaGameObject             = import "LuaGameObject"
local UIEventListener		    = import "UIEventListener"

LuaGiftSelectWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaGiftSelectWnd, "OKBtn", "OKBtn", GameObject)
RegistChildComponent(LuaGiftSelectWnd, "QnTableView1", "QnTableView1", QnTableView)
RegistChildComponent(LuaGiftSelectWnd, "NoBtn", "NoBtn", GameObject)
RegistChildComponent(LuaGiftSelectWnd, "ItemLabel", "ItemLabel", UILabel)
RegistChildComponent(LuaGiftSelectWnd, "TitleLabel", "TitleLabel", UILabel)

--@endregion RegistChildComponent end

--RegistClassMember
RegistClassMember(LuaGiftSelectWnd,"SelectItemCfg")
RegistClassMember(LuaGiftSelectWnd, "SelectedItem")
--@endregion

function LuaGiftSelectWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.OKBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOKBtnClick()
	end)

	UIEventListener.Get(self.NoBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnNoBtnClick()
	end)

    --@endregion EventBind end

    self.TitleLabel.text = g_MessageMgr:FormatMessage("ZIXUAN_SELECT_TITLE")
end

function LuaGiftSelectWnd:Init()
    self.SelectItemCfg = nil
    self.SelectedItem = nil

    local cfgs = self:GetGiftItems()
    if cfgs == nil then return end

    local len = #cfgs
    local initfunc = function (item,index)
        local giftcfg = cfgs[index+1]
        self:FillItemCell(item,giftcfg)
    end

    self.QnTableView1.m_DataSource = DefaultTableViewDataSource.CreateByCount(len,initfunc)
    self.QnTableView1:ReloadData(true, true)

    self.ItemLabel.transform.parent.gameObject:SetActive(false)
end

function LuaGiftSelectWnd:FillItemCell(item,cfgdata)
    if item == nil then return end

    local cfg = cfgdata.cfg
    if cfg == nil then return end

    local cell = item.transform
    local icon = LuaGameObject.GetChildNoGC(cell,"IconTexture").cTexture
    icon:LoadMaterial(cfg.Icon)

    if cfgdata.count>1 then
        local amountLabel = LuaGameObject.GetChildNoGC(cell,"AmountLabel").label
        amountLabel.text = tostring(cfgdata.count)
    end

    self:SelectItem(item.gameObject,false)

    UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        CItemInfoMgr.ShowLinkItemTemplateInfo(cfg.ID, false, nil, AlignType.Default, 0, 0, 0, 0)
        self.ItemLabel.transform.parent.gameObject:SetActive(true)
        self.ItemLabel.text = cfg.Name
        self.SelectItemCfg = cfg
        if self.SelectedItem ~= go then
            if self.SelectedItem then
                self:SelectItem(self.SelectedItem,false)
            end
            self.SelectedItem = go
            self:SelectItem(self.SelectedItem,true)
        end 
    end)

    item.gameObject:SetActive(true)
end

function LuaGiftSelectWnd:SelectItem(itemgo,selected)
    local selectgo = LuaGameObject.GetChildNoGC(itemgo.transform,"SelectedSprite").gameObject
    selectgo:SetActive(selected)
end

function LuaGiftSelectWnd:GetGiftItems()
    if LuaGiftSelectWndMgr.ItemID == nil then return nil end
    local item = CItemMgr.Inst:GetById(LuaGiftSelectWndMgr.ItemID)
    if item == nil then return nil end
    local cfgid = item.TemplateId
    local cfg = Item_Item.GetData(cfgid);
    if cfg == nil then return nil end
    local extradata = cfg.ExtraAttribute
    if System.String.IsNullOrEmpty(extradata) then return nil end
    local splits = Table2ArrayWithCount({";",",",":","|"}, 4, MakeArrayClass(System.String))
    local op = System.StringSplitOptions.RemoveEmptyEntries
    local strs = CommonDefs.StringSplit_ArrayChar_StringSplitOptions(extradata, splits, op)
    local count = tonumber(strs[1])--可选的数量
    if count ~= 1 then return nil end --目前仅支持选择1个的情况
    local res = {}

    for i=2,strs.Length-1,2 do
        local itemid = tonumber(strs[i])
        local itemcount = tonumber(strs[i+1])
        local giftcfg = Item_Item.GetData(itemid)
        if giftcfg then 
            res[#res+1]={cfg = giftcfg,count = itemcount}
        end
    end
    
    return res
end

--@region UIEvent

function LuaGiftSelectWnd:OnOKBtnClick()
    if self.SelectItemCfg == nil then
        g_MessageMgr:DisplayMessage("ZIXUAN_SELECT_EMPTY")
        return --玩家没有选择
    end
    local msg = g_MessageMgr:FormatMessage("ZIXUAN_SELECT_COMFIRE",self.SelectItemCfg.Name)
    local selectid = self.SelectItemCfg.ID
    local okfunc = function()
        local itemid = LuaGiftSelectWndMgr.ItemID
        local iteminfo = CItemMgr.Inst:GetItemInfo(itemid)

        local infos = CreateFromClass(MakeGenericClass(List, Object))
        CommonDefs.ListAdd(infos, typeof(Int32), self.SelectItemCfg.ID)

        Gac2Gas.SelectRewardsByUseItem(MsgPackImpl.pack(infos),iteminfo.place,iteminfo.pos,itemid)
        CUIManager.CloseUI(CLuaUIResources.GiftSelectWnd)
    end
    g_MessageMgr:ShowOkCancelMessage(msg,okfunc,nil,nil,nil,false)
end

function LuaGiftSelectWnd:OnNoBtnClick()
    CUIManager.CloseUI(CLuaUIResources.GiftSelectWnd)
end


--@endregion UIEvent

