-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CItemMgr = import "L10.Game.CItemMgr"
local CLivingItem = import "L10.UI.CLivingItem"
local CLivingSkillMgr = import "L10.Game.CLivingSkillMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local LifeSkill_Item = import "L10.Game.LifeSkill_Item"
local UInt32 = import "System.UInt32"
CLivingItem.m_Init_CS2LuaHook = function (this, itemData) 

    this.itemId = itemData.ID
    this.requireLevel = itemData.SkillLev
    --requireLevelLabel.text = itemData.SkillLev.ToString();

    local template = CItemMgr.Inst:GetItemTemplate(itemData.ID)
    if template ~= nil then
        this.icon:LoadMaterial(template.Icon)
    else
        this.icon.material = nil
    end

    this.toggle.activeSprite.alpha = 0
end
CLivingItem.m_OnClick_CS2LuaHook = function (this) 
    local data = LifeSkill_Item.GetData(this.itemId)
    if data ~= nil then
        CLivingSkillMgr.Inst.selectedItemId = this.itemId
        EventManager.Broadcast(EnumEventType.SelectLivingItem)
    end
end
CLivingItem.m_RefreshMaskState_CS2LuaHook = function (this) 
    if CommonDefs.DictContains(CClientMainPlayer.Inst.SkillProp.LifeSkillGradeMap, typeof(UInt32), CLivingSkillMgr.Inst.selectedSkill) then
        local skillLevel = CommonDefs.DictGetValue(CClientMainPlayer.Inst.SkillProp.LifeSkillGradeMap, typeof(UInt32), CLivingSkillMgr.Inst.selectedSkill)
        if this.requireLevel > skillLevel then
            this.disableMask.gameObject:SetActive(true)
        else
            this.disableMask.gameObject:SetActive(false)
        end
    else
        if this.requireLevel == 0 then
            this.disableMask.gameObject:SetActive(false)
        else
            this.disableMask.gameObject:SetActive(true)
        end
    end
end
CLivingItem.m_TrySelect_CS2LuaHook = function (this) 
    if CLivingSkillMgr.Inst.selectedItemId == this.itemId then
        this.toggle.activeSprite.alpha = 1
        this.toggle.value = true
        return true
    end
    return false
end
