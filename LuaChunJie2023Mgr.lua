local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CScene = import "L10.Game.CScene"
local EnumEventType = import "EnumEventType"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local CTaskMgr = import "L10.Game.CTaskMgr"
local CClientObjectRoot = import "L10.Game.CClientObjectRoot"
local CPos = import "L10.Engine.CPos"
local EnumWarnFXType	= import "L10.Game.EnumWarnFXType"
local CEffectMgr = import "L10.Game.CEffectMgr"
local ParticleSystem = import "UnityEngine.ParticleSystem"
local CWorldPositionFX = import "L10.Game.CWorldPositionFX"
local Vector3 = import "UnityEngine.Vector3"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local DelegateFactory = import "DelegateFactory"
local CScheduleMgr = import "L10.Game.CScheduleMgr"

LuaChunJie2023Mgr = {}

function LuaChunJie2023Mgr:OnMainPlayerCreated(playId)
    if playId == self:GetTTSYPlayId() then
        CUIManager.ShowUI(CLuaUIResources.ChunJie2023ShengYanTopRightWnd)
    end
end
function LuaChunJie2023Mgr:OnMainPlayerDestroyed()
    self:RemoveAllTraps()
    if self.TTSY_SitDownCheckTick then
        UnRegisterTick(self.TTSY_SitDownCheckTick)
        self.TTSY_SitDownCheckTick = nil
    end
end
-------------------
-- 主界面
-------------------
LuaChunJie2023Mgr.openTaskIds = nil
LuaChunJie2023Mgr.isTrialDelivery = false
function LuaChunJie2023Mgr:OpenChunJie2023MainWnd()
    CScheduleMgr.Inst:RequestActivity()
    self.isTrialDelivery = self:IsTrialDelivery()
    self:OnUpdateZJNHTaskStatus()
    CUIManager.ShowUI(CLuaUIResources.ChunJie2023MainWnd)
end

function LuaChunJie2023Mgr:IsTaskOpen(taskId)
    if self.openTaskIds == nil then return 1
    elseif self.openTaskIds[taskId] then return 3
    elseif self.isTrialDelivery then return 1   -- 试投放
    else return 2 end       -- 正式未开启
end

function LuaChunJie2023Mgr:IsTrialDelivery()
    local jieRiId = ChunJie_Setting2023.GetData().JieRiID
    self.openTaskIds = CLuaScheduleMgr.m_OpenFestivalTasks[jieRiId]
    if not self.openTaskIds then return true end
    for _, val in pairs(self.openTaskIds) do
        self.openTaskIds[val] = true
    end
    local setData = ChunJie_Setting2023.GetData()
    return self.openTaskIds[setData.TaoTieShengYanTaskID] and self.openTaskIds[setData.ShouLieTaoTieTaskID] and not self.openTaskIds[setData.ZhengJiuNianHuoTaskID]
end

-------------------
-- 拯救年货
-------------------
LuaChunJie2023Mgr.ZZNH_TaskLst = nil
LuaChunJie2023Mgr.ZZNH_FinishTaskCount = 0
LuaChunJie2023Mgr.ZZNH_hasNewTask = false

function LuaChunJie2023Mgr:InitZJNHTaskList()
    self.ZZNH_TaskLst = {}
    local taskLink = ChunJie_ZJNHSetting.GetData().TaskChain
    for i = 0, taskLink.Length - 1, 2 do
        local task = {}
        task.firstTaskId = taskLink[i]
        task.lastTaskId = taskLink[i + 1]
        task.isOpen = false
        task.isFinish = false
        table.insert(self.ZZNH_TaskLst, task)
    end
end

function LuaChunJie2023Mgr:IsZZNHTaskId(taskId)
    if self.ZZNH_TaskLst == nil then self:InitZJNHTaskList() end
    for i = 1, #self.ZZNH_TaskLst do
        if taskId == self.ZZNH_TaskLst[i].firstTaskId then return true end
        if taskId == self.ZZNH_TaskLst[i].lastTaskId then return true end
    end
    return false
end

function LuaChunJie2023Mgr:OnUpdateZJNHTaskStatus()
    if self.ZZNH_TaskLst == nil then self:InitZJNHTaskList() end
    self.ZZNH_FinishTaskCount = 0
    local taskProp = CClientMainPlayer.Inst.TaskProp
    for i = 1, #self.ZZNH_TaskLst do
        if CTaskMgr.Inst:CheckTaskTime(self.ZZNH_TaskLst[i].firstTaskId) then -- 首任务决定开启
            self.ZZNH_TaskLst[i].isOpen = true
        else
            self.ZZNH_TaskLst[i].isOpen = false
        end
        if taskProp:IsMainPlayerTaskFinished(self.ZZNH_TaskLst[i].lastTaskId) then -- 尾任务决定完成
            self.ZZNH_TaskLst[i].isFinish = true
            self.ZZNH_FinishTaskCount = i
        else
            self.ZZNH_TaskLst[i].isFinish = false
        end
    end
end

LuaChunJie2023Mgr.m_LastCheckNewTaskTime = 0
function LuaChunJie2023Mgr:ChunJie2023ZJNHCheck()
    local now = CServerTimeMgr.Inst.timeStamp
    if now - self.m_LastCheckNewTaskTime < 5 then return self.ZZNH_hasNewTask end
    self.m_LastCheckNewTaskTime = now

    self:OnUpdateZJNHTaskStatus()     
    local taskInfo = self.ZZNH_TaskLst[self.ZZNH_FinishTaskCount + 1]
    if taskInfo and taskInfo.isOpen then -- 新任务链存在且可接
        local hasReceived = CommonDefs.DictContains(CClientMainPlayer.Inst.TaskProp.CurrentTasks, typeof(UInt32), taskInfo.firstTaskId)
        local hasFinished = CClientMainPlayer.Inst.TaskProp:IsMainPlayerTaskFinished(taskInfo.firstTaskId)
        if hasReceived or hasFinished then self.ZZNH_hasNewTask = false -- 首任务已接或已完成，即进行中，红点不出现
        else self.ZZNH_hasNewTask = true end    -- 红点出现
    else
        self.ZZNH_hasNewTask = false
    end

    return self.ZZNH_hasNewTask
end

-------------------
-- 狩猎饕餮
-------------------
LuaChunJie2023Mgr.SLTT_playDesignId = nil
function LuaChunJie2023Mgr:GetSLTTPlayId()  -- 玩法ID
    if self.SLTT_playDesignId == nil then
        self.SLTT_playDesignId = ChunJie_SLTTSetting.GetData().PlayDesignId
    end
    return self.SLTT_playDesignId
end
function LuaChunJie2023Mgr:GetSLTTTaskTitle()
    return Gameplay_Gameplay.GetData(self:GetSLTTPlayId()).Name
end

LuaChunJie2023Mgr.SLTT_isWin = false
LuaChunJie2023Mgr.SLTT_Time = nil
LuaChunJie2023Mgr.SLTT_Rewards = {}
function LuaChunJie2023Mgr:Spring2023SLTT_ShowRewardWnd(bSuccess, duration, rewards_U)
    self.SLTT_isWin = bSuccess
    self.SLTT_Time = duration
    self.SLTT_Rewards = g_MessagePack.unpack(rewards_U)
    CUIManager.ShowUI(CLuaUIResources.ChunJie2023ShouLieResultWnd)
end

LuaChunJie2023Mgr.SLTT_TrapInfo = {}
LuaChunJie2023Mgr.SLTT_TrapRatio = nil
function LuaChunJie2023Mgr:Spring2023SLTT_AddOrUpdateTrap(trapId, x, y, beginRadius, endRadius, beginTime, endTime, playerBuff, monsterBuff, delta)
    if self.SLTT_TrapFxId == nil then self.SLTT_TrapFxId = ChunJie_SLTTSetting.GetData().OuTuFlagEffectId end
    local trapInfo = {}
    trapInfo.beginRadius = beginRadius
    trapInfo.endRadius = endRadius
    trapInfo.beginTime = beginTime
    trapInfo.endTime = endTime
    trapInfo.delta = delta
    if self.SLTT_TrapInfo[trapId] then
        trapInfo.fx = self.SLTT_TrapInfo[trapId].fx
        UnRegisterTick(self.SLTT_TrapInfo[trapId].tick)
        self:UpdateTrapSize(trapInfo.fx.m_FX.gameObject, trapInfo)
    else
        trapInfo.fx = CEffectMgr.Inst:AddWorldPositionFX(self.SLTT_TrapFxId, Utility.GridPos2PixelPos(CPos(x,y)), 0, 0, 1, -1, EnumWarnFXType.None, nil, 0, 0, 
        DelegateFactory.Action_GameObject(function(go)
            -- 首次创建需要获得特效默认比例
            if self.SLTT_TrapRatio == nil then self:GetTrapRatio(go) end
            self:UpdateTrapSize(go, trapInfo)
        end))
    end
    self.SLTT_TrapInfo[trapId] = trapInfo
    self.SLTT_TrapInfo[trapId].tick = RegisterTick(function()
        self:UpdateTrapTick(trapId)
    end, 1000)
end

function LuaChunJie2023Mgr:GetTrapRatio(go)
    self.SLTT_TrapRatio = {}
    local initialSize = 2
    local particleSystems = CommonDefs.GetComponentsInChildren_GameObject_Type_Boolean(go, typeof(ParticleSystem), true)
    for i = 0, particleSystems.Length - 1 do
        -- qipao、nianye同时等比缩放startsize和radius，其他只改变startsize
        local delta = {}
        delta.startSize = CommonDefs.GetParticleSystemScale(particleSystems[i]) / initialSize
        if particleSystems[i].name == "qipao" or particleSystems[i].name == "nianye" then
            delta.radius = CommonDefs.GetParticleSystemShapeRadius(particleSystems[i]) / initialSize
        end
        self.SLTT_TrapRatio[particleSystems[i].name] = delta
    end
end

function LuaChunJie2023Mgr:RemoveTrapByID(trapId)
    if self.SLTT_TrapInfo[trapId] then
        self.SLTT_TrapInfo[trapId].fx:Destroy() 
        UnRegisterTick(self.SLTT_TrapInfo[trapId].tick)
    end
    self.SLTT_TrapInfo[trapId] = nil
end

function LuaChunJie2023Mgr:RemoveAllTraps()
    for trapId, _ in pairs(self.SLTT_TrapInfo) do
        self:RemoveTrapByID(trapId)
    end
end

function LuaChunJie2023Mgr:UpdateTrapTick(trapId)
    local trapInfo = self.SLTT_TrapInfo[trapId]
    self:UpdateTrapSize(trapInfo.fx.m_FX.gameObject, trapInfo)
end

function LuaChunJie2023Mgr:UpdateTrapSize(go, trapInfo)
    local curRadius = self:CalCurRadius(trapInfo.beginRadius, trapInfo.endRadius, trapInfo.beginTime, trapInfo.endTime, trapInfo.delta)
    local particleSystems = CommonDefs.GetComponentsInChildren_GameObject_Type_Boolean(go, typeof(ParticleSystem), true)
    for i = 0, particleSystems.Length - 1 do
        local delta = LuaChunJie2023Mgr.SLTT_TrapRatio[particleSystems[i].name]
        CommonDefs.SetParticleSystemScale(particleSystems[i], delta.startSize * curRadius)
        if particleSystems[i].name == "qipao" or particleSystems[i].name == "nianye" then
            CommonDefs.SetParticleSystemShapeRadius(particleSystems[i], delta.radius * curRadius)
        end
    end
end

function LuaChunJie2023Mgr:CalCurRadius(beginRadius, endRadius, beginTime, endTime, delta)
    local curRadius = beginRadius + (endRadius - beginRadius) * (CServerTimeMgr.Inst.timeStamp - beginTime) / (endTime - beginTime) + delta
    curRadius = math.min(endRadius, curRadius)
    curRadius = math.max(beginRadius, curRadius)
    return curRadius
end

function LuaChunJie2023Mgr:OnClickSLTTTask()    -- 规则描述
    return g_MessageMgr:ShowMessage("ChunJie2023_ShouLieTaoTie_TaskDes")
end

-------------------
-- 饕餮盛宴
-------------------
EnumTTSYPlayState = {
    Idle = 1001,
	Stage_1 = 1002,
    Stage_2 = 1003,
    Stage_3 = 1004,
	Reward = 1005,
	End = 1006,
}

LuaChunJie2023Mgr.TTSY_playDesignId = nil
function LuaChunJie2023Mgr:GetTTSYPlayId()  -- 玩法ID
    if self.TTSY_playDesignId == nil then
        self.TTSY_playDesignId = ChunJie_TTSYSetting.GetData().PlayDesignId
    end
    return self.TTSY_playDesignId
end

LuaChunJie2023Mgr.TTSY_startTimeStamp = nil
LuaChunJie2023Mgr.TTSY_endTimeStamp = nil
function LuaChunJie2023Mgr:ChunJie2023TTSYCheck()
    if self.TTSY_startTimeStamp == nil then 
        local timeStr = ChunJie_TTSYSetting.GetData().PlayTimeString
        local startHour,startMin,endHour,endMin = string.match(timeStr,"(%d+):(%d+)-(%d+):(%d+)")
        self.TTSY_startTimeStamp = CServerTimeMgr.Inst:GetTodayTimeStampByTime(startHour,startMin,0)
        self.TTSY_endTimeStamp = CServerTimeMgr.Inst:GetTodayTimeStampByTime(endHour,endMin,0)
    end
    local nowTime = CServerTimeMgr.Inst.timeStamp
    return nowTime >= self.TTSY_startTimeStamp and nowTime <= self.TTSY_endTimeStamp
end

-- 玩法左侧任务栏显示
LuaChunJie2023Mgr.TTSY_playState = EnumTTSYPlayState.Idle
LuaChunJie2023Mgr.TTSY_endTime = 0
function LuaChunJie2023Mgr:GetTTSYTaskTitle()   -- 玩法名
    return Gameplay_Gameplay.GetData(self:GetTTSYPlayId()).Name
end
function LuaChunJie2023Mgr:GetTTSYTaskContent()     -- 玩法信息
    local playState = self.TTSY_playState % 1000 - 1
    if playState > 2 then playState = 2 end
    return SafeStringFormat3(LocalString.GetString("第%d/%d轮"), playState, 2)
end
function LuaChunJie2023Mgr:GetTTSYStateRemainTime()     -- 剩余时间
    if self.TTSY_playState == EnumTTSYPlayState.End then
        return CScene.MainScene.ShowTime
    else
        return math.floor(self.TTSY_endTime - CServerTimeMgr.Inst.timeStamp)
    end
end
function LuaChunJie2023Mgr:OnClickTTSYTask()    -- 规则描述
    return g_MessageMgr:ShowMessage("ChunJie2023_TaoTieShengYan_TaskDes")
end

-- 排行榜及阶段同步
LuaChunJie2023Mgr.TTSY_RankTbl = {}
LuaChunJie2023Mgr.TTSY_MainPlayerRankInfo = nil
LuaChunJie2023Mgr.TTSY_HeadInfoTbl = {}
LuaChunJie2023Mgr.TTSY_SitDownCheckTick = nil   -- 抢凳子判断
function LuaChunJie2023Mgr:Spring2023TTSY_SyncPlayState(playState, startTime, endTime, RankTbl_U)
    self.TTSY_endTime = endTime
    self.TTSY_RankTbl = g_MessagePack.unpack(RankTbl_U)
    self:GetTTSYStageRank("Score_1", "Rank_1") -- 第一轮排名
    self:GetTTSYStageRank("Score_2", "Rank_2") -- 第二轮排名
    
    
    local rankImgList = {g_sprites.Common_Huangguan_01, g_sprites.Common_Huangguan_02, g_sprites.Common_Huangguan_03}
    self.TTSY_HeadInfoTbl = {}
    -- 主控情况及前三名头顶信息
    for i, info in pairs(self.TTSY_RankTbl) do
        if CClientMainPlayer.IsPlayerId(info.PlayerId) then
            self.TTSY_MainPlayerRankInfo = info
        end
        if info.Rank >=1 and info.Rank <= 3 then
            self.TTSY_HeadInfoTbl[info.PlayerId] = rankImgList[info.Rank]
        end
    end

    -- 状态改变，通知任务栏更新
    if self.TTSY_playState ~= playState then 
        self.TTSY_playState = playState
        LuaCommonGamePlayTaskViewMgr:OnUpdateContent() 
    end
    
    g_ScriptEvent:BroadcastInLua("SyncTTSYPlayState")
    EventManager.BroadcastInternalForLua(EnumEventType.UpdatePlayerHeadInfoSepIcon, {true})
end

function LuaChunJie2023Mgr:GetTTSYStageRank(scoreType, stageType)
    -- 根据self.TTSY_RankTbl中的[scoreType]单独排名，并存储在[stageType]中
    local sortTbl = {}
    for i, info in pairs(self.TTSY_RankTbl) do
        table.insert(sortTbl, {idx = i, score = info[scoreType]})
    end
    table.sort(sortTbl, function(a, b)
        return a.score > b.score
    end)
    self.TTSY_RankTbl[sortTbl[1].idx][stageType] = 1
    for i = 2, #sortTbl do
        if sortTbl[i].score == sortTbl[i-1].score then
            self.TTSY_RankTbl[sortTbl[i].idx][stageType] = self.TTSY_RankTbl[sortTbl[i-1].idx][stageType]
        else
            self.TTSY_RankTbl[sortTbl[i].idx][stageType] = i
        end
    end
end

-- 结算
LuaChunJie2023Mgr.TTSY_FinalRank = nil
LuaChunJie2023Mgr.TTSY_FinalScore = nil
LuaChunJie2023Mgr.TTSY_Rewards = {}
function LuaChunJie2023Mgr:Spring2023TTSY_ShowRewardWnd(rank, score, rewards_U)
    self.TTSY_FinalRank = rank
    self.TTSY_FinalScore = score
    self.TTSY_Rewards = g_MessagePack.unpack(rewards_U)
    CUIManager.ShowUI(CLuaUIResources.ChunJie2023ShengYanResultWnd)
end

-- 抢凳子按钮
LuaChunJie2023Mgr.TTSY_CurrentStoolMonster = nil
function LuaChunJie2023Mgr:Spring2023TTSY_ShowSitDownBtn(engineId)
    self.TTSY_CurrentStoolMonster = engineId
    if not CUIManager.IsLoaded(CLuaUIResources.ChunJie2023ShengYanSitDownButtonWnd) then
        CUIManager.ShowUI(CLuaUIResources.ChunJie2023ShengYanSitDownButtonWnd)
    end
end

function LuaChunJie2023Mgr:Spring2023TTSY_HideSitDownBtn(engineId)
    if self.TTSY_CurrentStoolMonster == engineId and CUIManager.IsLoaded(CLuaUIResources.ChunJie2023ShengYanSitDownButtonWnd) then
        self.TTSY_CurrentStoolMonster = nil
        CUIManager.CloseUI(CLuaUIResources.ChunJie2023ShengYanSitDownButtonWnd)
    end
end
