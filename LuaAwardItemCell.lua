local GameObject = import "UnityEngine.GameObject"
local DelegateFactory  = import "DelegateFactory"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local UIEventListener = import "UIEventListener"
local CUITexture = import "L10.UI.CUITexture"
local Item_Item = import "L10.Game.Item_Item"

LuaAwardItemCell = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaAwardItemCell, "node", "node", GameObject)
RegistChildComponent(LuaAwardItemCell, "IconTexture", "IconTexture", GameObject)
RegistChildComponent(LuaAwardItemCell, "DisableTexture", "DisableTexture", GameObject)
RegistChildComponent(LuaAwardItemCell, "MaskSprite", "MaskSprite", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaAwardItemCell, "m_templateId")
RegistClassMember(LuaAwardItemCell, "m_isComplate")

function LuaAwardItemCell:Awake()
    --@region EventBind: Dont Modify Manually!
    UIEventListener.Get(self.node.gameObject).onClick =  DelegateFactory.VoidDelegate(
        function(go)
            CItemInfoMgr.ShowLinkItemTemplateInfo(self.m_templateId)
        end
    )
    
    --@endregion EventBind end
end

function LuaAwardItemCell:Init(templateId)
    self.m_templateId = templateId
    self:SetAwardComplete(false)
    local item = Item_Item.GetData(templateId)
    if item then 
        self.IconTexture:GetComponent(typeof(CUITexture)):LoadMaterial(item.Icon)
       self.DisableTexture:GetComponent(typeof(CUITexture)):LoadMaterial(item.Icon)
    end
end

function LuaAwardItemCell:SetAwardComplete(Complete)
    local maskSprite = self.transform:Find("MaskSprite").gameObject
	local disableSprite = self.transform:Find("DisableTexture").gameObject
    self.m_isComplate = Complete
    maskSprite:SetActive(Complete)
    disableSprite:SetActive(not Complete)
end

--@region UIEvent

--@endregion UIEvent

