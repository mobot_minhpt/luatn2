local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

if rawget(_G, "LuaLingyuGiftRecommendMgr") then
    g_ScriptEvent:RemoveListener("GasDisconnect", LuaLingyuGiftRecommendMgr, "OnDisconnect")
end

LuaLingyuGiftRecommendMgr = {}

LuaLingyuGiftRecommendMgr.info = {}
LuaLingyuGiftRecommendMgr.needShowButtonFx = true

g_ScriptEvent:AddListener("GasDisconnect", LuaLingyuGiftRecommendMgr, "OnDisconnect")

-- 切换角色清除数据
function LuaLingyuGiftRecommendMgr:OnDisconnect()
    self.info = {}
    self.needShowButtonFx = true
end

function LuaLingyuGiftRecommendMgr:SyncUXDynamicShopShangJiaGiftItems(event, traceId, scm, itemInfos_U, oriTotalJade, totalJade, discount, expireTime)
    self.info.event = event
    self.info.traceId = traceId
    self.info.scm = scm
    self.info.oriTotalJade = oriTotalJade
    self.info.totalJade = totalJade
	self.info.discount = discount
    self.info.expireTime = expireTime
    self.info.itemInfo = g_MessagePack.unpack(itemInfos_U)

    self.needShowButtonFx = true
    EventManager.Broadcast(EnumEventType.SyncDynamicActivitySimpleInfo)

    if not self:HasTriggeredGift() and CUIManager.IsLoaded(CLuaUIResources.LingyuGiftRecommendWnd) then
        CUIManager.CloseUI(CLuaUIResources.LingyuGiftRecommendWnd)
    end
    g_ScriptEvent:BroadcastInLua("SyncUXDynamicShopShangJiaGiftItems")
end

function LuaLingyuGiftRecommendMgr:BuyUXDynamicGiftItemsSuccess(event, traceId)
    g_MessageMgr:ShowMessage("UXDYNAMICGIFT_BUY_SUCCESS")
    if self.info and self.info.event == event and self.info.traceId == traceId then
        if CUIManager.IsLoaded(CLuaUIResources.LingyuGiftRecommendWnd) then
            CUIManager.CloseUI(CLuaUIResources.LingyuGiftRecommendWnd)
        end
    end
end

-- 有礼包可以购买
function LuaLingyuGiftRecommendMgr:HasTriggeredGift()
    if self.info.expireTime then
        return CServerTimeMgr.Inst.timeStamp < self.info.expireTime
    end
    return false
end
