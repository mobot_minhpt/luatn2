local TweenAlpha         = import "TweenAlpha"
local TweenPosition      = import "TweenPosition"
local TweenScale         = import "TweenScale"
local CTopAndRightTipWnd = import "L10.UI.CTopAndRightTipWnd"

LuaBaiSheBianRenTopRightWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaBaiSheBianRenTopRightWnd, "slider")
RegistClassMember(LuaBaiSheBianRenTopRightWnd, "fx")
RegistClassMember(LuaBaiSheBianRenTopRightWnd, "template")
RegistClassMember(LuaBaiSheBianRenTopRightWnd, "table")
RegistClassMember(LuaBaiSheBianRenTopRightWnd, "name")
RegistClassMember(LuaBaiSheBianRenTopRightWnd, "addNum")
RegistClassMember(LuaBaiSheBianRenTopRightWnd, "expandButton")

RegistClassMember(LuaBaiSheBianRenTopRightWnd, "curScore")
RegistClassMember(LuaBaiSheBianRenTopRightWnd, "tweenFloat")

function LuaBaiSheBianRenTopRightWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self:InitUIComponents()
    self:InitActive()
    self:InitEventListener()
end

-- 初始化UI组件
function LuaBaiSheBianRenTopRightWnd:InitUIComponents()
    self.slider = self.transform:Find("Anchor/Offset/Slider"):GetComponent(typeof(UISlider))
    self.fx = self.slider.transform:Find("Fx"):GetComponent(typeof(CUIFx))
    self.template = self.transform:Find("Anchor/Offset/Template").gameObject
    self.table = self.transform:Find("Anchor/Offset/Table")
    self.name = self.transform:Find("Anchor/Offset/Name"):GetComponent(typeof(UILabel))
    self.addNum = self.transform:Find("Anchor/Offset/AddNum"):GetComponent(typeof(UILabel))
    self.expandButton = self.transform:Find("Anchor/Offset/ExpandButton").gameObject
end

function LuaBaiSheBianRenTopRightWnd:InitActive()
    self.template:SetActive(false)
    self.addNum.alpha = 0
end

function LuaBaiSheBianRenTopRightWnd:InitEventListener()
    UIEventListener.Get(self.expandButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnExpandButtonClick()
	end)
end

function LuaBaiSheBianRenTopRightWnd:OnEnable()
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:AddListener("BaiSheBianRenSyncPlayState", self, "OnBaiSheBianRenSyncPlayState")
end

function LuaBaiSheBianRenTopRightWnd:OnDisable()
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:RemoveListener("BaiSheBianRenSyncPlayState", self, "OnBaiSheBianRenSyncPlayState")
end

function LuaBaiSheBianRenTopRightWnd:OnBaiSheBianRenSyncPlayState()
    self:UpdateProgress()
end

function LuaBaiSheBianRenTopRightWnd:OnHideTopAndRightTipWnd()
    LuaUtils.SetLocalRotation(self.expandButton.transform, 0, 0, 180)
end


function LuaBaiSheBianRenTopRightWnd:Init()
    local data = ZhuJueJuQing_BaiSheBianRenGameplay.GetData(LuaXinBaiLianDongMgr.BSBRGameplayId)

    self.name.text = data.ProgressName

    local width = self.slider.foregroundWidget.width

    for i = 0, data.ProgressNode.Length - 1 do
        local child = CUICommonDef.AddChild(self.table.gameObject, self.template)
        child:SetActive(true)

        local score = data.ProgressNode[i]
        child.transform:Find("Score"):GetComponent(typeof(UILabel)).text = SafeStringFormat3("%d%s", score, data.ProgressUnit)

        LuaUtils.SetLocalPositionX(child.transform, score / data.MaxScore * width)
    end

    self:UpdateProgress()
end

-- 更新进度条
function LuaBaiSheBianRenTopRightWnd:UpdateProgress()
    local score = LuaXinBaiLianDongMgr.BSBRInfo.score
    if not score then
        self.slider.value = 0
        return
    end

    local maxScore = LuaXinBaiLianDongMgr.BSBRInfo.maxScore
    local newValue = score / maxScore

    self.slider.foregroundWidget.spriteName = g_sprites.BlueProgressSprite
    self.slider.foregroundWidget.alpha = 1

    if not self.curScore then
        self.slider.value = newValue
        self.curScore = score
        return
    end

    if self.tweenFloat then
        LuaTweenUtils.Kill(self.tweenFloat, true)
    end

    -- 进度条逐渐变化
    self.tweenFloat = LuaTweenUtils.TweenFloat(self.slider.value, newValue, 0.5, function (val)
        self.slider.value = val
	end)

    -- 分数增加显示增加数字动效 分数减少红色闪烁
    self.fx:DestroyFx()
    if self.curScore < score then
        self.addNum.text = SafeStringFormat3("+%d", score - self.curScore)
        local tweenAlpha = self.addNum.transform:GetComponent(typeof(TweenAlpha))
        local tweenScale = self.addNum.transform:GetComponent(typeof(TweenScale))
        local tweenPosition = self.addNum.transform:GetComponent(typeof(TweenPosition))

        tweenAlpha:ResetToBeginning()
        tweenScale:ResetToBeginning()
        tweenPosition:ResetToBeginning()

        tweenAlpha:PlayForward()
        tweenScale:PlayForward()
        tweenPosition:PlayForward()

        self.fx:LoadFx(g_UIFxPaths.JinDuTiaoFxPath)

        CommonDefs.OnComplete_Tweener(self.tweenFloat, DelegateFactory.TweenCallback(function()
            self.tweenFloat = nil
            self.fx:DestroyFx()
        end))
    else
        self.slider.foregroundWidget.spriteName = g_sprites.RedProgressSprite

        local tweenAlpha = self.slider.foregroundWidget.transform:GetComponent(typeof(TweenAlpha))
        tweenAlpha.enabled = true

        CommonDefs.OnComplete_Tweener(self.tweenFloat, DelegateFactory.TweenCallback(function()
            self.tweenFloat = nil
            tweenAlpha.enabled = false
            self.slider.foregroundWidget.spriteName = g_sprites.BlueProgressSprite
            self.slider.foregroundWidget.alpha = 1
        end))
    end

    self.curScore = score
end

-- 离开场景清除数据
function LuaBaiSheBianRenTopRightWnd:OnDestroy()
    if self.tweenFloat then
        LuaTweenUtils.Kill(self.tweenFloat, false)
    end
    LuaXinBaiLianDongMgr.BSBRInfo = {}
end

--@region UIEvent

function LuaBaiSheBianRenTopRightWnd:OnExpandButtonClick()
    LuaUtils.SetLocalRotation(self.expandButton.transform, 0, 0, 0)
	CTopAndRightTipWnd.showPackage = false
	CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end

--@endregion UIEvent
