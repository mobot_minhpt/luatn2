local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local QnNewSlider = import "L10.UI.QnNewSlider"
local UISprite = import "UISprite"
local PlayerSettings = import "L10.Game.PlayerSettings"
local LayerDefine = import "L10.Engine.LayerDefine"
local AMPlayer = import "L10.Game.CutScene.AMPlayer"

LuaMiniVolumeSettingsWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaMiniVolumeSettingsWnd, "MusicSetting", "MusicSetting", QnSelectableButton)
RegistChildComponent(LuaMiniVolumeSettingsWnd, "SoundSetting", "SoundSetting", QnSelectableButton)
RegistChildComponent(LuaMiniVolumeSettingsWnd, "VolumeSlider", "VolumeSlider", QnNewSlider)
RegistChildComponent(LuaMiniVolumeSettingsWnd, "Background", "Background", Transform)

--@endregion RegistChildComponent end
RegistClassMember(LuaMiniVolumeSettingsWnd, "m_VolumeSlider_UISlider")
RegistClassMember(LuaMiniVolumeSettingsWnd, "m_TargetPosition")
RegistClassMember(LuaMiniVolumeSettingsWnd, "m_Pivot")
RegistClassMember(LuaMiniVolumeSettingsWnd, "m_Panel")
RegistClassMember(LuaMiniVolumeSettingsWnd, "m_CloseBtn")
RegistClassMember(LuaMiniVolumeSettingsWnd, "m_OpenButtonBg")
RegistClassMember(LuaMiniVolumeSettingsWnd, "m_OpenButtonGo")
RegistClassMember(LuaMiniVolumeSettingsWnd, "m_Bg")



LuaMiniVolumeSettingsMgr = {}
LuaMiniVolumeSettingsMgr.s_CutSceneUIInstance = nil
LuaMiniVolumeSettingsMgr.s_OpenButton = nil
LuaMiniVolumeSettingsMgr.s_TargetPosition = nil
LuaMiniVolumeSettingsMgr.s_Pivot =  nil
LuaMiniVolumeSettingsMgr.s_OnDestoryAction = nil
LuaMiniVolumeSettingsMgr.s_OnCloseAction = nil

function LuaMiniVolumeSettingsMgr:OpenWnd(btn, pos, pivot, OnDestroyAction)
    self.s_OpenButton = btn
    self.s_TargetPosition = pos
    self.s_Pivot = pivot
    self.s_OnDestoryAction = OnDestroyAction
    CUIManager.ShowUI(CLuaUIResources.MiniVolumeSettingsWnd)
end

function LuaMiniVolumeSettingsWnd:Awake()
    self.m_Panel = self.transform:GetComponent(typeof(UIPanel))
    self.m_VolumeSlider_UISlider = self.VolumeSlider:GetComponent(typeof(UISlider))
    self.m_CloseBtn = self.transform:Find("Background/CloseButton").gameObject
    self.m_Bg = self.transform:Find("Bg").gameObject

    self.VolumeSlider.OnValueChanged = DelegateFactory.Action_float(function(volume)
        self:OnSetVolume(volume)
    end)

    self.MusicSetting.OnClick = DelegateFactory.Action_QnButton(function(btn)
        self:OnSetMusicEnabled()
    end)

    self.SoundSetting.OnClick = DelegateFactory.Action_QnButton(function(btn)
        self:OnSetSoundEnabled()
    end)

    UIEventListener.Get(self.m_CloseBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnCloseBtnClicked()
    end)

    UIEventListener.Get(self.m_Bg).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnCloseBtnClicked()
    end)
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaMiniVolumeSettingsWnd:Init()
    if self.gameObject.layer == LayerDefine.CutSceneUI and LuaMiniVolumeSettingsMgr.s_CutSceneUIInstance ~= self then
        LuaMiniVolumeSettingsMgr.s_CutSceneUIInstance = self
        self.m_Panel.depth = 10
        self.gameObject:SetActive(false)
    end
    if LuaMiniVolumeSettingsMgr.s_OpenButton and not CommonDefs.IsUnityObjectNull(LuaMiniVolumeSettingsMgr.s_OpenButton) and self.m_OpenButtonBg == nil then
        local bgTransform = LuaMiniVolumeSettingsMgr.s_OpenButton.transform:Find("Bg")
        if bgTransform then
            self.m_OpenButtonGo = LuaMiniVolumeSettingsMgr.s_OpenButton
            self.m_OpenButtonBg = bgTransform:GetComponent(typeof(UISprite))
        end
    end
    self.m_TargetPosition = LuaMiniVolumeSettingsMgr.s_TargetPosition
    self.m_Pivot = LuaMiniVolumeSettingsMgr.s_Pivot

    self:LoadData()
    self:Reposition()
end

function LuaMiniVolumeSettingsWnd:OnEnable()
    self:Init()
    if self.m_OpenButtonBg and not CommonDefs.IsUnityObjectNull(self.m_OpenButtonGo) then
        self.m_OpenButtonBg.spriteName = "common_btn_09"
        self.m_OpenButtonBg.width = 94
        self.m_OpenButtonBg.height = 94
    end
    g_ScriptEvent:AddListener("ReloadSettings", self, "LoadData")
end

function LuaMiniVolumeSettingsWnd:OnDisable()
    if self.gameObject.layer == LayerDefine.CutSceneUI and LuaMiniVolumeSettingsMgr.s_CutSceneUIInstance == self and self.gameObject.activeSelf then
        self.gameObject:SetActive(false)
    end
    if self.m_OpenButtonBg and not CommonDefs.IsUnityObjectNull(self.m_OpenButtonGo) then
        self.m_OpenButtonBg.spriteName = "common_btn_05"
        self.m_OpenButtonBg.width = 72
        self.m_OpenButtonBg.height = 72
    end
    if LuaMiniVolumeSettingsMgr.s_OpenButton then
        local selected = LuaMiniVolumeSettingsMgr.s_OpenButton.transform:Find("Selected")
        if selected then
            selected.gameObject:SetActive(false)
        end
    end
    g_ScriptEvent:RemoveListener("ReloadSettings", self, "LoadData")
end

--@region UIEvent

--@endregion UIEvent

function LuaMiniVolumeSettingsWnd:OnSetVolume(volume)
    PlayerSettings.VolumeSetting = volume
    g_ScriptEvent:BroadcastInLua("OnMiniVolumeSettingsChanged")
end

function LuaMiniVolumeSettingsWnd:OnSetMusicEnabled()
    PlayerSettings.MusicEnabled = self.MusicSetting:isSeleted()
    g_ScriptEvent:BroadcastInLua("OnMiniVolumeSettingsChanged")
end

function LuaMiniVolumeSettingsWnd:OnSetSoundEnabled()
    PlayerSettings.SoundEnabled = self.SoundSetting:isSeleted()
    if AMPlayer.Inst and AMPlayer.Inst.IsPlaying then
        AMPlayer.Inst:SetMSoundVolume(self.SoundSetting:isSeleted() and 1 or 0)
    end
    g_ScriptEvent:BroadcastInLua("OnMiniVolumeSettingsChanged")
end

function LuaMiniVolumeSettingsWnd:OnCloseBtnClicked()
    if self.gameObject.layer == LayerDefine.CutSceneUI then
        self.gameObject:SetActive(false)
    else
        CUIManager.CloseUI(CLuaUIResources.MiniVolumeSettingsWnd)
    end
end

function LuaMiniVolumeSettingsWnd:LoadData()
    self.MusicSetting:SetSelected(PlayerSettings.MusicEnabled)
    self.SoundSetting:SetSelected(PlayerSettings.SoundEnabled)

    self.m_VolumeSlider_UISlider.value = PlayerSettings.VolumeSetting
end

function LuaMiniVolumeSettingsWnd:Reposition()
    local b = NGUIMath.CalculateRelativeWidgetBounds(self.Background)
    local localPos = self.m_TargetPosition and self.Background.parent:InverseTransformPoint(self.m_TargetPosition) or Vector3(0, 0, 0)
    if self.m_Pivot == UIWidget.Pivot.Left then
        self.Background.localPosition = Vector3(localPos.x - b.size.x * 0.5 + 50, localPos.y + b.size.y * 0.5 + 20, 0)
    elseif self.m_Pivot == UIWidget.Pivot.Right then
        self.Background.localPosition = Vector3(localPos.x + b.size.x * 0.5 - 50, localPos.y + b.size.y * 0.5 + 20, 0)
    else
        self.Background.localPosition = Vector3(localPos.x, localPos.y + b.size.y * 0.5 + 20, 0)
    end
end

function LuaMiniVolumeSettingsWnd:OnDestroy()
    LuaMiniVolumeSettingsMgr.s_TargetPosition =  nil
    LuaMiniVolumeSettingsMgr.s_Pivot =  nil
    LuaMiniVolumeSettingsMgr.s_OpenButton = nil
    if LuaMiniVolumeSettingsMgr.s_CutSceneUIInstance == self then
        LuaMiniVolumeSettingsMgr.s_CutSceneUIInstance = nil
    end
    if LuaMiniVolumeSettingsMgr.s_OnDestoryAction then
        LuaMiniVolumeSettingsMgr.s_OnDestoryAction()
        LuaMiniVolumeSettingsMgr.s_OnDestoryAction = nil
    end
end