-- Auto Generated!!
local CChatLinkMgr = import "CChatLinkMgr"
local CCommonListButtonItem = import "CCommonListButtonItem"
local CommonDefs = import "L10.Game.CommonDefs"
local CQianKunDaiMgr = import "L10.Game.CQianKunDaiMgr"
local CQianKunDaiRecipeWnd = import "L10.UI.CQianKunDaiRecipeWnd"
local CUICommonDef = import "L10.UI.CUICommonDef"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local PlayerSettings = import "L10.Game.PlayerSettings"
local QianKunDai_Formula = import "L10.Game.QianKunDai_Formula"
local String = import "System.String"
local UILabel = import "UILabel"
local UInt32 = import "System.UInt32"
CQianKunDaiRecipeWnd.m_Init_CS2LuaHook = function (this) 
    CommonDefs.ListClear(this.ButtonList)
    CommonDefs.ListClear(this.FormulaList)
    this.m_RecipeSelectPrefab:SetActive(false)
    this.m_RecipeBriefPrefab:SetActive(false)
    this:InitRecipes()

    -- 根据DetailRecipeId来判断初始化显示的配方界面
    if this.ButtonList.Count > 0 and this.ButtonList[0] ~= nil then
        do
            local i = 0
            while i < this.FormulaList.Count do
                if this.FormulaList[i] == CQianKunDaiMgr.Inst.DetailRecipeId then
                    this:OnRecipeSelected(i + 1)
                    return
                end
                i = i + 1
            end
        end
        this:OnRecipeSelected(0)
    end
end
CQianKunDaiRecipeWnd.m_InitRecipes_CS2LuaHook = function (this) 
    CUICommonDef.ClearTransform(this.m_SelectGrid.transform)

    local recipeNames = CreateFromClass(MakeGenericClass(List, String))

    -- 全部配方选项
    CommonDefs.ListAdd(recipeNames, typeof(String), LocalString.GetString("全部配方"))

    local tempFormulaList = CreateFromClass(MakeGenericClass(List, QianKunDai_Formula))
    QianKunDai_Formula.ForeachKey(DelegateFactory.Action_object(function (key) 
        local formula = QianKunDai_Formula.GetData(key)
        if formula.Status ~= 3 then
            CommonDefs.ListAdd(tempFormulaList, typeof(QianKunDai_Formula), formula)
        end
    end))

    -- 进行排序
    CommonDefs.ListSort1(tempFormulaList, typeof(QianKunDai_Formula), DelegateFactory.Comparison_QianKunDai_Formula(function (a, b) 
        return NumberCompareTo(a.Order, b.Order)
    end))

    do
        local i = 0
        while i < tempFormulaList.Count do
            CommonDefs.ListAdd(recipeNames, typeof(String), tempFormulaList[i].Type)
            CommonDefs.ListAdd(this.FormulaList, typeof(UInt32), tempFormulaList[i].ID)
            i = i + 1
        end
    end

    do
        local i = 0
        while i < recipeNames.Count do
            local go = CUICommonDef.InstantiateUniformItem(this.m_RecipeSelectPrefab, this.m_SelectGrid.transform)
            go:SetActive(true)
            local item = CommonDefs.GetComponent_GameObject_Type(go, typeof(CCommonListButtonItem))
            local alert = item.transform:Find("Alert").gameObject

            item:SetNameLabel(recipeNames[i])
            item.clickAction = MakeDelegateFromCSFunction(this.OnRecipeSelected, MakeGenericClass(Action1, Int32), this)
            item.Index = i
            if i > 1 then
                alert:SetActive(this.FormulaList[i - 1] > PlayerSettings.QianKunDaiRecipeMaxId)
            end

            CommonDefs.ListAdd(this.ButtonList, typeof(CCommonListButtonItem), item)
            i = i + 1
        end
    end
    this.m_SelectGrid:Reposition()
    this.m_SelectScrollView:InvalidateBounds()
    this.m_SelectScrollView:SetDragAmount(0, 0, false)
end
CQianKunDaiRecipeWnd.m_OnRecipeSelected_CS2LuaHook = function (this, index) 

    if this.m_ShowIndex == index then
        return
    end

    this.m_ShowIndex = index
    do
        local i = 0
        while i < this.ButtonList.Count do
            this.ButtonList[i]:SetHighLight(i == index)
            i = i + 1
        end
    end

    if index == 0 then
        this:InitRecipeTotalView()
    end
    if index > 0 and (index - 1) < this.FormulaList.Count then
        this:InitDetailRecipeView(index)
        this.ButtonList[index].transform:Find("Alert").gameObject:SetActive(false)
    end
end
CQianKunDaiRecipeWnd.m_InitRecipeTotalView_CS2LuaHook = function (this) 
    this.m_TotalRoot:SetActive(true)
    this.m_DetailRoot:SetActive(false)

    CUICommonDef.ClearTransform(this.m_RecipeBriefGrid.transform)

    do
        local i = 0
        while i < this.FormulaList.Count do
            local go = CUICommonDef.InstantiateUniformItem(this.m_RecipeBriefPrefab, this.m_RecipeBriefGrid.transform)
            go:SetActive(true)
            local label = CommonDefs.GetComponent_Component_Type(CUICommonDef.TraverseFindChild("RecipeLabel", go.transform), typeof(UILabel))
            if label ~= nil then
                local formula = QianKunDai_Formula.GetData(this.FormulaList[i])
                label.text = System.String.Format(CQianKunDaiRecipeWnd.RecipeItemFormat, formula.MeterialDesc, formula.ProductDesc)
            end
            i = i + 1
        end
    end
    this.m_RecipeBriefGrid:Reposition()
    this.m_RecipeBriefScrollView:InvalidateBounds()
    this.m_RecipeBriefScrollView:SetDragAmount(0, 0, false)
end
CQianKunDaiRecipeWnd.m_InitDetailRecipeView_CS2LuaHook = function (this, selectedIdx) 
    this.m_TotalRoot:SetActive(false)
    this.m_DetailRoot:SetActive(true)

    local formula = QianKunDai_Formula.GetData(this.FormulaList[selectedIdx - 1])
    if formula ~= nil then
        this.m_FormulaInput.text = formula.MeterialDesc
        this.m_FormulaOutput.text = formula.ProductDesc
        this.m_FormulaScrollView:ResetPosition()
        this.m_FormulaDesc.text = CChatLinkMgr.TranslateToNGUIText(CQianKunDaiMgr.Inst:GetDescForFormula(formula), false)
        if formula.ID > PlayerSettings.QianKunDaiRecipeMaxId then
            PlayerSettings.QianKunDaiRecipeMaxId = formula.ID
            EventManager.Broadcast(EnumEventType.QianKunDaiUpdateMaxId)
        end
    end
end
CQianKunDaiRecipeWnd.m_OnCompoundBtnClicked_CS2LuaHook = function (this, go) 
    if this.m_ShowIndex == 0 then
        return
    end
    if this.m_ShowIndex > 0 and (this.m_ShowIndex - 1) < this.FormulaList.Count then
        EventManager.BroadcastInternalForLua(EnumEventType.QianKunDaiUpdateFormula, {this.FormulaList[this.m_ShowIndex - 1]})
        this:Close()
    end
end
