local Input = import "UnityEngine.Input"
local CUITexture = import "L10.UI.CUITexture"
local Time = import "UnityEngine.Time"
local CBaseWnd = import "L10.UI.CBaseWnd"
local CTeamMgr = import "L10.Game.CTeamMgr"
local CShiTuMgr = import "L10.Game.CShiTuMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CUIResources = import "L10.UI.CUIResources"
local UITweener = import "UITweener"
local NGUIText = import "NGUIText"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"
local EGetPlayerAppearanceContext = import "L10.Game.EGetPlayerAppearanceContext"
local Animator = import "UnityEngine.Animator"
local LuaTweenUtils = import "LuaTweenUtils"
local UIPanel = import "UIPanel"

LuaShiTuDuLingTaskWnd=class()
RegistChildComponent(LuaShiTuDuLingTaskWnd, "m_BallRoot", "BallRoot", Transform)--当前活动根节点
RegistChildComponent(LuaShiTuDuLingTaskWnd, "m_SpiritBallPool", "SpiritBallPool", Transform)--灵球缓存池
RegistChildComponent(LuaShiTuDuLingTaskWnd, "m_EvilBallPool", "EvilBallPool", Transform)--魔球缓存池
RegistChildComponent(LuaShiTuDuLingTaskWnd, "m_SpiritBall", "SpiritBall", GameObject)--当前活动根节点
RegistChildComponent(LuaShiTuDuLingTaskWnd, "m_EvilBall", "EvilBall", GameObject)--当前活动根节点
RegistChildComponent(LuaShiTuDuLingTaskWnd, "m_ShiFuPartMask", "ShiFuPartMask", GameObject)
RegistChildComponent(LuaShiTuDuLingTaskWnd, "m_TuDiPartMask", "TuDiPartMask", GameObject)

RegistChildComponent(LuaShiTuDuLingTaskWnd, "m_SpiritBallClickFx", "fx_baodianlan", GameObject)
RegistChildComponent(LuaShiTuDuLingTaskWnd, "m_EvilBallClickFx", "fx_baodianzi", GameObject)
RegistChildComponent(LuaShiTuDuLingTaskWnd, "m_TailClickFx", "tuoweikgd2", GameObject)
RegistChildComponent(LuaShiTuDuLingTaskWnd, "m_ShiFuBlueLineFx", "xianlan_left", GameObject)
RegistChildComponent(LuaShiTuDuLingTaskWnd, "m_TuDiBlueLineFx", "xianlan_right", GameObject)
RegistChildComponent(LuaShiTuDuLingTaskWnd, "m_ShiFuPurpleLineFx", "xianzi_left", GameObject)
RegistChildComponent(LuaShiTuDuLingTaskWnd, "m_TuDiPurpleLineFx", "xianzi_right", GameObject)

RegistChildComponent(LuaShiTuDuLingTaskWnd, "m_ShiFuScoreRoot", "ShiFuScoreRoot", GameObject)
RegistChildComponent(LuaShiTuDuLingTaskWnd, "m_TuDiScoreRoot", "TuDiScoreRoot", GameObject)
RegistChildComponent(LuaShiTuDuLingTaskWnd, "m_ShiFuScoreTarget", "ShiFuScoreTarget", GameObject)
RegistChildComponent(LuaShiTuDuLingTaskWnd, "m_TuDiScoreTarget", "TuDiScoreTarget", GameObject)
RegistChildComponent(LuaShiTuDuLingTaskWnd, "m_ShiFuScoreLabel", "ShiFuScoreLabel", GameObject)
RegistChildComponent(LuaShiTuDuLingTaskWnd, "m_TuDiScoreLabel", "TuDiScoreLabel", GameObject)
RegistChildComponent(LuaShiTuDuLingTaskWnd, "m_RatioAnimator", "RatioSprite", Animator)
RegistChildComponent(LuaShiTuDuLingTaskWnd, "m_RatioLabel", "RatioLabel", UILabel)


RegistChildComponent(LuaShiTuDuLingTaskWnd, "m_ShiFuModelTexture", "ShiFuModelTexture", UITexture)
RegistChildComponent(LuaShiTuDuLingTaskWnd, "m_TuDiModelTexture", "TuDiModelTexture", UITexture)

RegistClassMember(LuaShiTuDuLingTaskWnd, "m_NextUUID")--球的序号
RegistClassMember(LuaShiTuDuLingTaskWnd, "m_Index2GameObjTbl") -- 序号和球的对应关系
RegistClassMember(LuaShiTuDuLingTaskWnd, "m_BallAniTick")--魔球灵球掉落动画Tick
RegistClassMember(LuaShiTuDuLingTaskWnd, "m_BallDropSpeed")--魔球灵球掉落速度
RegistClassMember(LuaShiTuDuLingTaskWnd, "m_ClickTrailDuration")--点击球后的拖尾特效播放时间
RegistClassMember(LuaShiTuDuLingTaskWnd, "m_SpiritColor")
RegistClassMember(LuaShiTuDuLingTaskWnd, "m_EvilColor")
RegistClassMember(LuaShiTuDuLingTaskWnd, "m_IamShiFu")
RegistClassMember(LuaShiTuDuLingTaskWnd, "m_ShiFuID")
RegistClassMember(LuaShiTuDuLingTaskWnd, "m_TuDiID")
RegistClassMember(LuaShiTuDuLingTaskWnd, "m_LastIncreaseTime")--上次自动累加积分时间
RegistClassMember(LuaShiTuDuLingTaskWnd, "m_ShiFuModelTextureIdentifier") --模型贴图唯一标识
RegistClassMember(LuaShiTuDuLingTaskWnd, "m_TuDiModelTextureIdentifier") --模型贴图唯一标识
RegistClassMember(LuaShiTuDuLingTaskWnd, "m_ModelROTbl") -- RenderObject

RegistClassMember(LuaShiTuDuLingTaskWnd, "m_ShiFuLineDelayTick")
RegistClassMember(LuaShiTuDuLingTaskWnd, "m_DuTiLineDelayTick")

function LuaShiTuDuLingTaskWnd:Awake()
    self.m_SpiritBall:SetActive(false)
    self.m_EvilBall:SetActive(false)
    self.m_ShiFuScoreLabel.gameObject:SetActive(false)
    self.m_TuDiScoreLabel.gameObject:SetActive(false)
    self.m_SpiritBallClickFx:SetActive(false)
    self.m_EvilBallClickFx:SetActive(false)
    self.m_TailClickFx:SetActive(false)
    self.m_ShiFuBlueLineFx:SetActive(false)
    self.m_TuDiBlueLineFx:SetActive(false)
    self.m_ShiFuPurpleLineFx:SetActive(false)
    self.m_TuDiPurpleLineFx:SetActive(false)
    
    self.m_NextUUID = 0
    self.m_Index2GameObjTbl = {}
    self.m_BallDropSpeed = 400
    self.m_ClickTrailDuration = 0.5
    self.m_EvilColor = NGUIText.ParseColor24("A94CFF", 0)
    self.m_SpiritColor = NGUIText.ParseColor24("4CB1FF", 0)
    self.m_IamShiFu = self:MainPlayerIsShiFu()
    self.m_LastIncreaseTime = 0
    self.m_ShiFuPartMask:SetActive(not self.m_IamShiFu)
    self.m_TuDiPartMask:SetActive(self.m_IamShiFu)
    local now = CServerTimeMgr.Inst:GetZone8Time()
    math.randomseed(CTeamMgr.Inst.LeaderId) -- 保证师傅和徒弟生成的序列是一样的，不过考虑到打开界面时间不一样，两边的动画可能会有差异
    self.m_ShiFuModelTextureIdentifier = SafeStringFormat3("__%s__", tostring(self.m_ShiFuModelTexture.gameObject:GetInstanceID()))
	self.m_TuDiModelTextureIdentifier = SafeStringFormat3("__%s__", tostring(self.m_TuDiModelTexture.gameObject:GetInstanceID()))
    self.m_ModelROTbl = {}

    self.gameObject:GetComponent(typeof(UIPanel)).IgnoreIphoneXMargin = true
end

function LuaShiTuDuLingTaskWnd:Init()
    local members = CTeamMgr.Inst.Members
    if members.Count~=2 then
        CUIManager.CloseUI("ShiTuDuLingTaskWnd")
        return
    end
    self.m_ShiFuID = 0
    self.m_TuDiID = 0
    for i=0,members.Count-1 do
        if CShiTuMgr.Inst:IsCurrentShiFu(members[i].m_MemberId) then
            self.m_ShiFuID = members[i].m_MemberId
            self.m_TuDiID = CClientMainPlayer.Inst.Id
            break
        elseif CShiTuMgr.Inst:IsCurrentTuDi(members[i].m_MemberId) then
            self.m_ShiFuID = CClientMainPlayer.Inst.Id
            self.m_TuDiID = members[i].m_MemberId
            break
        end
    end

    if not (self.m_ShiFuID>0 and self.m_TuDiID>0) then
        CUIManager.CloseUI("ShiTuDuLingTaskWnd")
        return
    end
   -- self:UpdateScore()
    self:StartBallAnimation()
    CClientPlayerMgr.Inst:RequestGetPlayerAppearance(self.m_ShiFuID, EGetPlayerAppearanceContext.UpdateTeamMember)
    CClientPlayerMgr.Inst:RequestGetPlayerAppearance(self.m_TuDiID, EGetPlayerAppearanceContext.UpdateTeamMember)
end

function LuaShiTuDuLingTaskWnd:MainPlayerIsShiFu()
    local iamShiFu = false
    local members = CTeamMgr.Inst.Members
    for i=0,members.Count-1 do
        if CShiTuMgr.Inst:IsCurrentTuDi(members[i].m_MemberId) then
            iamShiFu = true
            break
        end
    end
    return iamShiFu
end

function LuaShiTuDuLingTaskWnd:OnEnable()
    g_ScriptEvent:AddListener("OnSyncShiTuDuLingTaskInfo", self, "OnSyncShiTuDuLingTaskInfo")
    g_ScriptEvent:AddListener("PlayerAppearanceUpdate", self, "OnPlayerAppearanceUpdate")
end

function LuaShiTuDuLingTaskWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnSyncShiTuDuLingTaskInfo", self, "OnSyncShiTuDuLingTaskInfo")
    g_ScriptEvent:RemoveListener("PlayerAppearanceUpdate", self, "OnPlayerAppearanceUpdate")
end

function LuaShiTuDuLingTaskWnd:OnDestroy()
    self:StopBallAnimation()
    CUIManager.DestroyModelTexture(self.m_ShiFuModelTextureIdentifier)
	CUIManager.DestroyModelTexture(self.m_TuDiModelTextureIdentifier)
    LuaTweenUtils.DOKill(self.m_TailClickFx.transform, false)
    self:StopLineFxTick(true)
    self:StopLineFxTick(false)
end

function LuaShiTuDuLingTaskWnd:OnSyncShiTuDuLingTaskInfo(bUpdateByMyself, total, add, index)
    if add==0 then
        self:UpdateScore()
        return
    end
    if bUpdateByMyself then
        if self.m_IamShiFu then
            self:PlayScore(bUpdateByMyself, true, add, self.m_ShiFuScoreRoot, self.m_ShiFuScoreLabel)
        else
            self:PlayScore(bUpdateByMyself, false, add, self.m_TuDiScoreRoot, self.m_TuDiScoreLabel)
        end
    else
        if self.m_IamShiFu then
            self:PlayScore(bUpdateByMyself, false, add, self.m_TuDiScoreRoot, self.m_TuDiScoreLabel)
        else
            self:PlayScore(bUpdateByMyself, true, add, self.m_ShiFuScoreRoot, self.m_ShiFuScoreLabel)
        end
    end
    self:UpdateScore()
    -- if index>0 then
    --     for idx, obj in pairs(self.m_Index2GameObjTbl) do
    --         if idx == index then
    --             self.m_Index2GameObjTbl[idx] = nil
    --             self:RecycleBall(obj, true)
    --             break
    --         end
    --     end
    -- end
end

function LuaShiTuDuLingTaskWnd:UpdateScore()
    local clip = self.m_RatioAnimator.runtimeAnimatorController.animationClips[0]
    local progress = math.min(1, LuaShiTuMgr.m_DuLingTotalScore / ShiTu_Setting.GetData().DuLingNeedScore)
    clip:SampleAnimation(self.m_RatioAnimator.gameObject, progress * clip.length)
    self.m_RatioLabel.text = tostring(math.min(LuaShiTuMgr.m_DuLingTotalScore,ShiTu_Setting.GetData().DuLingNeedScore))
end

function LuaShiTuDuLingTaskWnd:StartBallAnimation()
    self:StopBallAnimation()
    self.m_BallAniTick = RegisterTick(function()
        self:GenBallForShiTu()
    end, 800)
end

function LuaShiTuDuLingTaskWnd:StopBallAnimation()
    if self.m_BallAniTick then
        UnRegisterTick(self.m_BallAniTick)
        self.m_BallAniTick = nil
    end
end

function LuaShiTuDuLingTaskWnd:GenBallForShiTu()
    local virtualScreenSize = CUICommonDef.GetVirtualScreenSize()
    local halfWidth = virtualScreenSize.x * 0.5
    local halfHeight = virtualScreenSize.y * 0.5
    --左侧师傅，右侧徒弟
    local shifuStartPos = Vector3(math.random(-halfWidth + 100, -100), halfHeight + math.random(self.m_BallDropSpeed,self.m_BallDropSpeed*1.5), 0)
    local tudiStartPos = Vector3(math.random(100,halfWidth-100), halfHeight + math.random(self.m_BallDropSpeed,self.m_BallDropSpeed*1.5), 0)
    local shifuGenEvilBall = math.random(0,9)>=5
    local tudiGenEvilBall = math.random(0,9)>=5
    local shifuGo = self:GenBall(shifuStartPos, shifuGenEvilBall, shifuGenEvilBall and self.m_EvilBall or self.m_SpiritBall, shifuGenEvilBall and self.m_EvilBallPool or self.m_SpiritBallPool)
    local tudiGo = self:GenBall(tudiStartPos, tudiGenEvilBall, tudiGenEvilBall and self.m_EvilBall or self.m_SpiritBall, tudiGenEvilBall and self.m_EvilBallPool or self.m_SpiritBallPool)
    UIEventListener.Get(shifuGo).onClick = DelegateFactory.VoidDelegate(function() self:OnClickShiFuBall(shifuGo, shifuGenEvilBall) end)
    UIEventListener.Get(tudiGo).onClick = DelegateFactory.VoidDelegate(function() self:OnClickTuDiBall(tudiGo, tudiGenEvilBall) end)
end

function LuaShiTuDuLingTaskWnd:GetUUID()
    self.m_NextUUID = self.m_NextUUID + 1
    return self.m_NextUUID
end

function LuaShiTuDuLingTaskWnd:GenBall(startPos, isEvil, ballTemplate, ballPool)
    local go = nil
    if ballPool.childCount > 0 then
        go = ballPool:GetChild(0).gameObject
        go.transform.parent = self.m_BallRoot
    else
        go = CUICommonDef.AddChild(self.m_BallRoot.gameObject, ballTemplate)
        go.name = isEvil and "Evil" or "Spirit"
    end
    go:SetActive(true)
    go.transform.localPosition = startPos
    self.m_Index2GameObjTbl[self:GetUUID()] = go
    return go
end

function LuaShiTuDuLingTaskWnd:RecycleBall(go, playClickFx)
    local isEvil = (go.name=="Evil")
    if playClickFx then
        local fx = isEvil and self.m_EvilBallClickFx or self.m_SpiritBallClickFx
        fx:SetActive(false)
        fx.transform.position = go.transform.position
        fx:SetActive(true)
    end
    go.transform.parent = isEvil and self.m_EvilBallPool or self.m_SpiritBallPool
    go.gameObject:SetActive(false)
end

function LuaShiTuDuLingTaskWnd:Update()
    local virtualScreenSize = CUICommonDef.GetVirtualScreenSize()
    local removeTbl = {}
    local childCount = self.m_BallRoot.transform.childCount
    for i=0,childCount-1 do
        local child = self.m_BallRoot.transform:GetChild(i)
        if child.transform.localPosition.y < - virtualScreenSize.y * 0.5 - 200 then
            table.insert(removeTbl, child)
        else
            Extensions.SetLocalPositionY(child, child.localPosition.y - Time.deltaTime * self.m_BallDropSpeed)
        end
    end
    for __, child in pairs(removeTbl) do
        self:RecycleBall(child.gameObject, false)
        for index, go in pairs(self.m_Index2GameObjTbl) do
            if go == child then
                self.m_Index2GameObjTbl[index] = nil
                break
            end
        end
    end
    --师父一方负责同步自动增加分数的信息
    if self.m_IamShiFu and CServerTimeMgr.Inst.timeStamp>=self.m_LastIncreaseTime+1 then
        self.m_LastIncreaseTime = CServerTimeMgr.Inst.timeStamp
        local members = CTeamMgr.Inst.Members
        for i=0,members.Count-1 do
            if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id ~= members[i].m_MemberId then
                LuaShiTuMgr:RequestSyncShiTuDuLingTaskInfo(members[i].m_MemberId, LuaShiTuMgr.m_DuLingTotalScore + ShiTu_Setting.GetData().DuLingAddScoreSpeed, 0, 0)
            end
        end
    end
end

function LuaShiTuDuLingTaskWnd:OnClickShiFuBall(go, isEvil)
    if self.m_IamShiFu then
        self:ClearBallAndSyncScoreInfo(go, isEvil)
        self:PlayClickLineFx(go.transform.position, self.m_ShiFuScoreTarget.transform.position, true, isEvil)
    end
end

function LuaShiTuDuLingTaskWnd:OnClickTuDiBall(go, isEvil)
    if not self.m_IamShiFu then
        self:ClearBallAndSyncScoreInfo(go, isEvil)
        self:PlayClickLineFx(go.transform.position, self.m_TuDiScoreTarget.transform.position, false, isEvil)
    end
end

function LuaShiTuDuLingTaskWnd:PlayClickLineFx(from, to, isShifu, isEvil)
    self.m_TailClickFx:SetActive(false)
    self.m_TailClickFx.transform.position = from
    self.m_TailClickFx:SetActive(true)
    LuaTweenUtils.OnComplete(LuaTweenUtils.DOMove(self.m_TailClickFx.transform, to, self.m_ClickTrailDuration, false), function()
        --local animator = self.m_TailClickFx.transform:GetComponent(typeof(Animator))
        --animator:Play("shitudulingtaskwnd_tuowein", -1, 0)
        --self.m_TailClickFx:SetActive(false)
        --self:StartLineFxTick(isShifu, isEvil)
    end)
end

function LuaShiTuDuLingTaskWnd:StartLineFxTick(isShifu, isEvil)
    self:StopLineFxTick(isShifu)
    if isShifu then
        if isEvil then
            self.m_ShiFuPurpleLineFx:SetActive(true)
        else
            self.m_ShiFuBlueLineFx:SetActive(true)
        end
        self.m_ShiFuLineDelayTick = RegisterTick(function()
            if isEvil then
                self.m_ShiFuPurpleLineFx:SetActive(false)
            else
                self.m_ShiFuBlueLineFx:SetActive(false)
            end
        end, 800)
    else
        if isEvil then
            self.m_TuDiPurpleLineFx:SetActive(true)
        else
            self.m_TuDiBlueLineFx:SetActive(true)
        end
        self.m_TuDiLineDelayTick = RegisterTick(function()
            if isEvil then
                self.m_TuDiPurpleLineFx:SetActive(false)
            else
                self.m_TuDiBlueLineFx:SetActive(false)
            end
        end, 800)
    end
end

function LuaShiTuDuLingTaskWnd:StopLineFxTick(isShifu)
    if isShifu then
        if self.m_ShiFuLineDelayTick then
            UnRegisterTick(self.m_ShiFuLineDelayTick)
            self.m_ShiFuLineDelayTick = nil
        end
        self.m_ShiFuPurpleLineFx:SetActive(false)
        self.m_ShiFuBlueLineFx:SetActive(false)
    else
        if self.m_TuDiLineDelayTick then
            UnRegisterTick(self.m_TuDiLineDelayTick)
            self.m_TuDiLineDelayTick = nil
        end
        self.m_TuDiPurpleLineFx:SetActive(false)
        self.m_TuDiBlueLineFx:SetActive(false)
    end
end

function LuaShiTuDuLingTaskWnd:ClearBallAndSyncScoreInfo(go, isEvil)
    
    self:RecycleBall(go, true)
    local index = 0
    for idx, obj in pairs(self.m_Index2GameObjTbl) do
        if go == obj then
            self.m_Index2GameObjTbl[idx] = nil
            index = idx
            break
        end
    end
    local oppId = self.m_IamShiFu and self.m_TuDiID or self.m_ShiFuID
    LuaShiTuMgr:RequestSyncShiTuDuLingTaskInfo(oppId, LuaShiTuMgr.m_DuLingTotalScore, isEvil and 
            ShiTu_Setting.GetData().DuLingEvilScore or  ShiTu_Setting.GetData().DuLingSpiritScore, index)
end


function LuaShiTuDuLingTaskWnd:PlayScore(bUpdateByMyself, isShifu, value, scoreRoot, scoreLabel)
    -- 两个效果，其一是连接线，其二是分数显示
    RegisterTickOnce(function()
        if not (self.gameObject and scoreRoot and scoreLabel) then return end
        --第一个效果
        self:StartLineFxTick(isShifu, value<0)
        --第二个效果
        local childGo = CUICommonDef.AddChild(scoreRoot.gameObject, scoreLabel)
        childGo:SetActive(true)
        local label = childGo.transform:GetComponent(typeof(UILabel))
        label.text = (value > 0 and "+" or "")..tostring(value)
        label.color = value>0 and self.m_SpiritColor or self.m_EvilColor
        local tweener = childGo.transform:GetComponent(typeof(UITweener))
        tweener:AddOnFinished(DelegateFactory.Callback(function()
            GameObject.Destroy(childGo)
        end))
    end, bUpdateByMyself and self.m_ClickTrailDuration * 1000 or 0.1 * 1000) --己方延迟播放，等待trial拖尾结束，他方默认延迟0.1s，不播放拖尾了
end

function LuaShiTuDuLingTaskWnd:LoadModel(identifier, playerId, modelTexture, faceRight)
    modelTexture.mainTexture = CUIManager.CreateModelTexture(identifier, self:GetModelLoader(playerId, modelTexture),
        faceRight and 90 or 270, 0.05, -0.8, 4.66, false, true, 1.0, false, false)
end

function LuaShiTuDuLingTaskWnd:GetModelLoader(playerId, modelTexture)
    return LuaDefaultModelTextureLoader.Create(function (ro)
        local appearanceData = CClientPlayerMgr.Inst:GetPlayerAppearance(playerId)
        CClientMainPlayer.LoadResource(ro, appearanceData, true, 1.0, 0, false, 0, false, 47000041, false, nil, false, false)
        self.m_ModelROTbl[playerId] = ro
    end)
end


function LuaShiTuDuLingTaskWnd:OnPlayerAppearanceUpdate(args)
    local playerId = tonumber(args[0])
    if self.m_ShiFuID == playerId then
        self:LoadModel(self.m_ShiFuModelTextureIdentifier, playerId, self.m_ShiFuModelTexture, true)
    elseif self.m_TuDiID == playerId then
        self:LoadModel(self.m_TuDiModelTextureIdentifier, playerId, self.m_TuDiModelTexture, false)
    end
end