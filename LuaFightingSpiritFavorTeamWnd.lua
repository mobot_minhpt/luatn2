local LuaGameObject=import "LuaGameObject"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Color = import "UnityEngine.Color"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CFightingSpiritMgr = import "L10.Game.CFightingSpiritMgr"

CLuaFightingSpiritFavorTeamWnd = class()

RegistClassMember(CLuaFightingSpiritFavorTeamWnd, "m_TeamNameLabel")
RegistClassMember(CLuaFightingSpiritFavorTeamWnd, "m_FavorCountLabel")
RegistClassMember(CLuaFightingSpiritFavorTeamWnd, "m_FavorSprite")
RegistClassMember(CLuaFightingSpiritFavorTeamWnd, "m_HeadList")

RegistClassMember(CLuaFightingSpiritFavorTeamWnd, "m_CurrServerId")

function CLuaFightingSpiritFavorTeamWnd:Init()
	self.m_CurrServerId = CLuaFightingSpiritMgr.m_FavorServerId
	self.m_CurrGroupId = CLuaFightingSpiritMgr.m_FavorGroupId
	local centerRoot = self.transform:Find("Panel/Center").gameObject
	self.m_TeamNameLabel = LuaGameObject.GetChildNoGC(centerRoot.transform, "TeamLabel").label
	self.m_FavorCountLabel = LuaGameObject.GetChildNoGC(centerRoot.transform, "CountLabel").label
	self.m_FavorSprite = LuaGameObject.GetChildNoGC(centerRoot.transform, "FavorSprite").sprite
	local headListRoot = self.transform:Find("Panel/HeadList").gameObject

	self.m_TeamNameLabel.text = ""
	self.m_FavorCountLabel.text = "0"
	self.m_FavorSprite.spriteName = "common_dianzan_02"

	self:OnFreshCenterInfo()

	self.m_HeadList = {}
	for i = 1, 10 do
		local headGO = headListRoot.transform:Find(tostring(i)).gameObject
		table.insert(self.m_HeadList, headGO)
		headGO:SetActive(false)
	end


	Gac2Gas.RequestDouHunPraiseServerInfo(self.m_CurrGroupId)
end

function CLuaFightingSpiritFavorTeamWnd:OnFreshCenterInfo()
	for i = 1, #CLuaFightingSpiritMgr.m_FavorTeamList do
		local teamInfo = CLuaFightingSpiritMgr.m_FavorTeamList[i]
		if teamInfo.serverId == self.m_CurrServerId and teamInfo.groupId == self.m_CurrGroupId then

			local serverInfo = CFightingSpiritMgr.Instance:GetServerInfoFromID(teamInfo.serverId)
			local serverName = serverInfo and serverInfo.ServerName or "" 

			if teamInfo.groupName == 1 then
				self.m_TeamNameLabel.text = SafeStringFormat3(LocalString.GetString("%s·[c][fb3ffd]乾[/c]"), serverName)
			elseif teamInfo.groupName == 2 then
				self.m_TeamNameLabel.text =  SafeStringFormat3(LocalString.GetString("%s·[c][edb743]坤[/c]"), serverName)
			end

			self.m_FavorCountLabel.text = teamInfo.favorCount
			if teamInfo.bFavor then
				self.m_FavorSprite.spriteName = "personalspacewnd_heart_2"
			else
				self.m_FavorSprite.spriteName = "personalspacewnd_heart_1"
			end
			break
		end
	end
end

function CLuaFightingSpiritFavorTeamWnd:OnReceiveServerInfo(groupId)
	if self.m_CurrGroupId ~= groupId then return end
	self:OnFreshInfo()
end

function CLuaFightingSpiritFavorTeamWnd:OnFreshInfo()
	local infoList = CLuaFightingSpiritMgr.m_FavorInfoList
	for i = 1, #infoList do
		local headGO = self.m_HeadList[i]
		local info = infoList[i]

		local favorButton = LuaGameObject.GetLuaGameObjectNoGC(headGO.transform).qnButton
		local favorSprite = LuaGameObject.GetLuaGameObjectNoGC(headGO.transform).sprite
		local portraitTexture = LuaGameObject.GetChildNoGC(headGO.transform, "Portrait").cTexture
		local nameLabel = LuaGameObject.GetChildNoGC(headGO.transform, "NameLabel").label
		local fxGO = LuaGameObject.GetChildNoGC(headGO.transform, "UI_aixin").gameObject

		favorButton.OnClick = DelegateFactory.Action_QnButton(function(button)

			Gac2Gas.RequestDouHunPraisePlayer(info.playerId)
		end)
		if info.bFavor then
			favorSprite.spriteName = "common_dianzan_01"
		else
			favorSprite.spriteName = "common_dianzan_02"
		end
		portraitTexture:LoadNPCPortrait(CUICommonDef.GetPortraitName(info.clazz, info.gender, -1), false)
		if CClientMainPlayer.Inst and info.playerId == CClientMainPlayer.Inst.Id then
			nameLabel.color = Color.green
		else
			nameLabel.color = Color.white
		end
		CUICommonDef.SetClampLabelText(nameLabel, info.name)

		fxGO:SetActive(false)
		headGO:SetActive(true)
	end

	for i = #infoList + 1, #self.m_HeadList do
		self.m_HeadList[i]:SetActive(false)
	end

	self:OnFreshCenterInfo()
end

function CLuaFightingSpiritFavorTeamWnd:OnFavorPlayerSuccess(targetPlayerId, targetServerId)
	for i = 1, #CLuaFightingSpiritMgr.m_FavorInfoList do
		local info = CLuaFightingSpiritMgr.m_FavorInfoList[i]
		if info.playerId == targetPlayerId then
			local headGO = self.m_HeadList[i]
			local favorSprite = LuaGameObject.GetLuaGameObjectNoGC(headGO.transform).sprite
			favorSprite.spriteName = "common_dianzan_01"
			local fxGO = LuaGameObject.GetChildNoGC(headGO.transform, "UI_aixin").gameObject
			fxGO:SetActive(true)
			break
		end
	end

	self:OnFreshCenterInfo()
end

function CLuaFightingSpiritFavorTeamWnd:OnEnable()
	g_ScriptEvent:AddListener("RequestDouHunPraiseServerInfoResult", self, "OnReceiveServerInfo")
	g_ScriptEvent:AddListener("RequestDouHunPraisePlayerSuccess", self, "OnFavorPlayerSuccess")
end

function CLuaFightingSpiritFavorTeamWnd:OnDisable()
	g_ScriptEvent:RemoveListener("RequestDouHunPraiseServerInfoResult", self, "OnReceiveServerInfo")
	g_ScriptEvent:RemoveListener("RequestDouHunPraisePlayerSuccess", self, "OnFavorPlayerSuccess")
end
return CLuaFightingSpiritFavorTeamWnd
