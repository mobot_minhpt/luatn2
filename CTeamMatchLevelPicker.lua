-- Auto Generated!!
local CommonDefs = import "L10.Game.CommonDefs"
local CResourceMgr = import "L10.Engine.CResourceMgr"
local CTeamMatchLevelPicker = import "L10.UI.CTeamMatchLevelPicker"
local CTeamMatchLevelPickerItemResource = import "L10.UI.CTeamMatchLevelPickerItemResource"
local Extensions = import "Extensions"
local GameObject = import "UnityEngine.GameObject"
local Object = import "System.Object"
local OnCenterCallback = import "UICenterOnChild+OnCenterCallback"
local Quaternion = import "UnityEngine.Quaternion"
local UILabel = import "UILabel"
local Vector3 = import "UnityEngine.Vector3"
CTeamMatchLevelPicker.m_Init_CS2LuaHook = function (this, minVal, maxVal, curVal) 
    this.centerOnChild.onCenter = MakeDelegateFromCSFunction(this.OnCenter, OnCenterCallback, this)
    if minVal > maxVal then
        local tmp = minVal
        minVal = maxVal
        maxVal = tmp
    end
    curVal = math.min(math.max(curVal, minVal), maxVal)
    this.minVal = minVal
    this.maxVal = maxVal
    this.curVal = curVal

    --table.transform.RemoveAllChildren();
    local children
    children = Extensions.GetChildren(this.table, false)
    do
        local i = 0
        while i < children.Count do
            children[i].parent = nil
            CResourceMgr.Inst:Destroy(children[i].gameObject)
            i = i + 1
        end
    end
    CommonDefs.ListClear(this.items)

    if this.Temp then 
        CTeamMatchLevelPickerItemResource.Inst.Resource = this.Temp
    end

    local n = (minVal == maxVal) and 1 or (maxVal - minVal + 1)
    do
        local i = 0    
        while i < n do
            local go = CResourceMgr.Inst:Instantiate(CTeamMatchLevelPickerItemResource.Inst, false, 1)
            go.transform.parent = this.table.transform
            go.transform.localPosition = Vector3.zero
            go.transform.localRotation = Quaternion.identity
            go.transform.localScale = Vector3.one
            CommonDefs.GetComponent_GameObject_Type(go, typeof(UILabel)).text = tostring((minVal + i))
            go:SetActive(true)
            CommonDefs.ListAdd(this.items, typeof(GameObject), go)
            i = i + 1
        end
    end
    this.table:Reposition()
    this.scrollView:ResetPosition()
    this.centerOnChild:CenterOnInstant(this.items[curVal - minVal].transform)
end
CTeamMatchLevelPicker.m_Clear_CS2LuaHook = function (this) 
    this.minVal = 0
    this.maxVal = 0
    this.curVal = 0
    --items.Clear();
    --table.transform.RemoveAllChildren();
    local children
    children = Extensions.GetChildren(this.table, false)
    do
        local i = 0
        while i < children.Count do
            children[i].parent = nil
            CResourceMgr.Inst:Destroy(children[i].gameObject)
            i = i + 1
        end
    end
    CommonDefs.ListClear(this.items)
end
CTeamMatchLevelPicker.m_OnCenter_CS2LuaHook = function (this, go) 
    if this.OnValueChanged ~= nil then
        do
            local i = 0
            while i < this.items.Count do
                if this.items[i] == this.centerOnChild.centeredObject then
                    GenericDelegateInvoke(this.OnValueChanged, Table2ArrayWithCount({this.minVal + i}, 1, MakeArrayClass(Object)))
                    break
                end
                i = i + 1
            end
        end
    end
end

CTeamMatchLevelPickerHandler = {}
function CTeamMatchLevelPickerHandler.SetValue(picker,value)
    local curVal = math.min(math.max(value, picker.minVal), picker.maxVal)
    local cb = picker.OnValueChanged
    picker.OnValueChanged = nil
    picker.centerOnChild:CenterOnInstant(picker.items[curVal - picker.minVal].transform)
    picker.OnValueChanged = cb
end
