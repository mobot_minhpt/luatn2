local CScene = import "L10.Game.CScene"

LuaBaoZhuPlayMgr = {}

LuaBaoZhuPlayMgr.OpenPlayWnd = function()
  Gac2Gas.CareFireworkCheckMatch()
end

LuaBaoZhuPlayMgr.OpenPlayRankWnd = function()
  LuaBaoZhuPlayMgr.InitRank = true
  Gac2Gas.CareFireworkCheckMatch()
end

LuaBaoZhuPlayMgr.IsInPlay = function()
	if CScene.MainScene then
		local gamePlayId = CScene.MainScene.GamePlayDesignId
		if gamePlayId == Chunjie2021_CareFirework.GetData().PlayDesignId then
			return true
		end
	end
	return false
end
----

-- 小心爆竹玩法报名结果
function Gas2Gac.CareFireworkSignUpPlayResult(bSuccess)
  if bSuccess then
    LuaBaoZhuPlayMgr.InMatch = true
    g_ScriptEvent:BroadcastInLua("UpdateBaoZhuPlayShowInfo")
  end
end

-- 小心爆竹玩法取消报名结果
function Gas2Gac.CareFireworkCancelSignUpResult(bSuccess)
  if bSuccess then
    LuaBaoZhuPlayMgr.InMatch = false
    g_ScriptEvent:BroadcastInLua("UpdateBaoZhuPlayShowInfo")
  end
end

-- 小心爆竹玩法检查是否在匹配中的结果, bSuccess 和 bInMatching 同时为true,则在匹配中
function Gas2Gac.CareFireworkCheckInMatchingResult(bSuccess, bInMatching, resultStr, useNum)
  if bSuccess and bInMatching then
    LuaBaoZhuPlayMgr.InMatch = true
  else
    LuaBaoZhuPlayMgr.InMatch = false
  end
  LuaBaoZhuPlayMgr.PlayUseNum = useNum or 0

  if CUIManager.IsLoaded("BaoZhuPlayShowWnd") then
    g_ScriptEvent:BroadcastInLua("UpdateBaoZhuPlayShowInfo")
  else
    CUIManager.ShowUI("BaoZhuPlayShowWnd")
  end
end

-- 副本中返回某个玩家的简略信息(名字、几段血、躲避分、传递分) 玩家分数发生变化且在前三的时候副本中每个人都会收到
function Gas2Gac.SyncCarefireWorkPlayerInfo(playerId, playerUd)
    if not playerUd then return end
    local playerInfo = MsgPackImpl.unpack(playerUd)
    if not playerInfo then return end

    local playerId = playerInfo[0]
    local playerName = playerInfo[1]
    local hideScore = playerInfo[2]
    local deliverScore = playerInfo[3]
    local rankPos = playerInfo[4]
    local showRank = playerInfo[5]
    local totalScore = hideScore + deliverScore
    g_ScriptEvent:BroadcastInLua("UpdateBaoZhuPlayTopRankInfo",{playerId = playerId, name = playerName, score = hideScore + deliverScore, rank = rankPos, showRank = showRank})
end

-- 玩家自己的信息 自己任意信息发生改变的时候发给自己
function Gas2Gac.SyncCarefireWorkPlayerSelfInfo(playerId, playerUd)
    if not playerUd then return end
    local playerInfo = MsgPackImpl.unpack(playerUd)
    if not playerInfo then return end

    local playerId = playerInfo[0]
    local playerName = playerInfo[1]
    local hideScore = playerInfo[2]
    local deliverScore = playerInfo[3]
    local rankPos = playerInfo[4]
    local health = playerInfo[5]
    local maxHp = playerInfo[6]
    local totalScore = hideScore + deliverScore
    g_ScriptEvent:BroadcastInLua("UpdateBaoZhuPlaySelfRankInfo",{playerId = playerId, name = playerName, score = hideScore + deliverScore, rank = rankPos})
    
    LuaGamePlayHpMgr:AddHpInfo(playerId, health, maxHp, false, 0, true)
end

-- 副本中返回副本的信息(总共几个人,排第几,当前哪个阶段)
-- phase -> 1 第一阶段，躲避  2 第二阶段，传递
function Gas2Gac.SyncCarefireWorkSceneInfo(leftCount, totalCount, phase)
  LuaBaoZhuPlayMgr.PhaData = {leftCount,totalCount,phase}
  g_ScriptEvent:BroadcastInLua("UpdateBaoZhuPlayPha")
end

-- 副本结算界面 副本结束的时候发
function Gas2Gac.SyncCarefireWorkSummaryInfo(summaryUd)
  if not summaryUd then return end
  local summaryRank = MsgPackImpl.unpack(summaryUd)
  if not summaryRank then return end

  LuaBaoZhuPlayMgr.ResultData = {}
  for i = 0, summaryRank.Count - 1, 1 do
    local playerInfo = summaryRank[i]
    local rankPos = tonumber(playerInfo[0])
    local playerId = tonumber(playerInfo[1])
    local playerName = playerInfo[2]
    local hideScore = tonumber(playerInfo[3])
    local deliverScore = tonumber(playerInfo[4])
    local totalScore = hideScore + deliverScore

    table.insert(LuaBaoZhuPlayMgr.ResultData,{rank = rankPos, playerId = playerId, name = playerName, score1 = hideScore, score2 = deliverScore, score = totalScore})
  end

  CUIManager.ShowUI("BaoZhuPlayEndWnd")
end
