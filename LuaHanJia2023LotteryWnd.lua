local UILabel = import "UILabel"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"
LuaHanJia2023LotteryWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaHanJia2023LotteryWnd, "Box", "Box", GameObject)
RegistChildComponent(LuaHanJia2023LotteryWnd, "CloseButton", "CloseButton", GameObject)
RegistChildComponent(LuaHanJia2023LotteryWnd, "DrawRemainLabel", "DrawRemainLabel", UILabel)
RegistChildComponent(LuaHanJia2023LotteryWnd, "DrawTips", "DrawTips", UILabel)
RegistChildComponent(LuaHanJia2023LotteryWnd, "PokeButton1", "PokeButton1", GameObject)
RegistChildComponent(LuaHanJia2023LotteryWnd, "PokeButton2", "PokeButton2", GameObject)
RegistChildComponent(LuaHanJia2023LotteryWnd, "PokeButton3", "PokeButton3", GameObject)
RegistChildComponent(LuaHanJia2023LotteryWnd, "PokeButton4", "PokeButton4", GameObject)
RegistChildComponent(LuaHanJia2023LotteryWnd, "PokeButton5", "PokeButton5", GameObject)
RegistChildComponent(LuaHanJia2023LotteryWnd, "PokeButton6", "PokeButton6", GameObject)
RegistChildComponent(LuaHanJia2023LotteryWnd, "PokeButton7", "PokeButton7", GameObject)
RegistChildComponent(LuaHanJia2023LotteryWnd, "PokeButton8", "PokeButton8", GameObject)
RegistChildComponent(LuaHanJia2023LotteryWnd, "PokeButton9", "PokeButton9", GameObject)
RegistChildComponent(LuaHanJia2023LotteryWnd, "RewardItem1", "RewardItem1", GameObject)
RegistChildComponent(LuaHanJia2023LotteryWnd, "RewardItem2", "RewardItem2", GameObject)
RegistChildComponent(LuaHanJia2023LotteryWnd, "RewardItem3", "RewardItem3", GameObject)
RegistChildComponent(LuaHanJia2023LotteryWnd, "RewardItem4", "RewardItem4", GameObject)
RegistChildComponent(LuaHanJia2023LotteryWnd, "RewardItem5", "RewardItem5", GameObject)
RegistChildComponent(LuaHanJia2023LotteryWnd, "RuleButton", "RuleButton", GameObject)
RegistChildComponent(LuaHanJia2023LotteryWnd, "redVfx", "redVfx", GameObject)
RegistChildComponent(LuaHanJia2023LotteryWnd, "emptyVfx", "emptyVfx", GameObject)
RegistChildComponent(LuaHanJia2023LotteryWnd, "blueVfx", "blueVfx", GameObject)
RegistChildComponent(LuaHanJia2023LotteryWnd, "yellowVfx", "yellowVfx", GameObject)

--@endregion RegistChildComponent end

function LuaHanJia2023LotteryWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    LuaHanJia2023Mgr.lotteryData = nil
    self:InitWndData()
    self:RefreshConstUI()
    self:OnSyncLotteryData()
    self:InitUIEvent()
    if self.ShowGridTick then
        invoke(self.ShowGridTick)
    end
    self.ShowGridTick = RegisterTickOnce(function()
        self.Box.transform:Find("Grid").gameObject:SetActive(true)
    end, 300)
end

function LuaHanJia2023LotteryWnd:Init()
    LuaHanJia2023Mgr.lotteryData = nil
    Gac2Gas.RequestHanHuaMiBaoData()
end

function LuaHanJia2023LotteryWnd:OnEnable()
    g_ScriptEvent:AddListener("HanJia2023_SyncLotteryData", self, "OnSyncLotteryData")
end

function LuaHanJia2023LotteryWnd:OnDisable()
    g_ScriptEvent:RemoveListener("HanJia2023_SyncLotteryData", self, "OnSyncLotteryData")
end

function LuaHanJia2023LotteryWnd:OnSyncLotteryData()
    if LuaHanJia2023Mgr.lotteryData then
        self.curHuoLi = LuaHanJia2023Mgr.lotteryData.curHuoLi
        local drawAvailableTime = math.floor(self.curHuoLi/self.huoLiPerDraw)
        local drawColorHyperText = ""
        if drawAvailableTime > 0 then
            drawColorHyperText = "[c][00ff60]" .. drawAvailableTime .. "[-][/c]"
        else
            drawColorHyperText = "[c][ff5050]" .. drawAvailableTime .. "[-][/c]"
        end
        self.DrawRemainLabel.text = g_MessageMgr:FormatMessage("HanJia2023_Lottery_RemainDraw", drawColorHyperText )
        if LuaHanJia2023Mgr.lotteryData.lastSelect == 0 then
            -- 是因为RequestHanHuaMiBaoData而同步的.. 应该是发起同步请求而造成的同步
            for i = 1, #self.pokeButtonList do
                self.pokeButtonList[i].transform:Find("BrokenState").gameObject:SetActive(LuaHanJia2023Mgr.lotteryData.lotteryResult[i] ~= nil)
            end
        else    
            -- 是因为抽奖request而再次同步, 需要表现一下抽到的效果
            local drawIdx = LuaHanJia2023Mgr.lotteryData.lotteryResult[LuaHanJia2023Mgr.lotteryData.lastSelect]
            local vfxTable = {[0] = self.emptyVfx, [1] = self.yellowVfx, [2] = self.blueVfx, [3] = self.redVfx}
            if drawIdx then
                --没有全清
                local quality = HanJia2023_HanHuaMiBaoReward.GetData(drawIdx).Quality
                local itemId = HanJia2023_HanHuaMiBaoReward.GetData(drawIdx).RewardItemId
                local vfx = self.pokeButtonList[LuaHanJia2023Mgr.lotteryData.lastSelect].transform:Find("vfx")
                vfxTable[quality].transform:SetParent(vfx)
                vfxTable[quality].transform.localScale = Vector3.one
                vfxTable[quality].transform.localPosition = Vector3.zero
                vfxTable[quality].transform.localEulerAngles = Vector3.zero
                vfxTable[quality].gameObject:SetActive(false)
                vfxTable[quality].gameObject:SetActive(true)
                --应该过个一会,再弹获得
                if self.ShowRewardTick then        self.DrawTips.text = g_MessageMgr:FormatMessage("HanJia2023_Lottery_JifenTips", self.huoLiPerDraw, self.huoLiPerDraw-(self.curHuoLi%self.huoLiPerDraw))

                    invoke(self.ShowRewardTick)
                end
                for i = 1, #self.pokeButtonList do
                    self.pokeButtonList[i].transform:Find("BrokenState").gameObject:SetActive(LuaHanJia2023Mgr.lotteryData.lotteryResult[i] ~= nil)
                end
                self.ShowRewardTick = RegisterTickOnce(function()
                    if itemId ~= 0 then
                        LuaCommonGetRewardWnd.m_materialName = "gongxihuode"
                        LuaCommonGetRewardWnd.m_Reward1List = { {ItemID=itemId, Count=1} }
                        LuaCommonGetRewardWnd.m_Reward2Label = ""
                        LuaCommonGetRewardWnd.m_Reward2List = {  }
                        LuaCommonGetRewardWnd.m_hint = ""
                        LuaCommonGetRewardWnd.m_button = {
                            {spriteName="blue", buttonLabel = LocalString.GetString("确定"), clickCB = function() CUIManager.CloseUI(CLuaUIResources.CommonGetRewardWnd) end},
                        }
                        CUIManager.ShowUI(CLuaUIResources.CommonGetRewardWnd)
                    end
                    self.ShowRewardTick = nil
                end, self.showRewardDelay * 1000)
            else
                --每一盒的最后一抽
                local quality = HanJia2023_HanHuaMiBaoSetting.GetData().BaoDiRewardQuality
                local itemId = HanJia2023_HanHuaMiBaoSetting.GetData().BaoDiRewardId
                local vfx = self.pokeButtonList[LuaHanJia2023Mgr.lotteryData.lastSelect].transform:Find("vfx")
                vfxTable[quality].transform:SetParent(vfx)
                vfxTable[quality].transform.localScale = Vector3.one
                vfxTable[quality].transform.localPosition = Vector3.zero
                vfxTable[quality].transform.localEulerAngles = Vector3.zero
                vfxTable[quality].gameObject:SetActive(false)
                vfxTable[quality].gameObject:SetActive(true)
                --应该过个一会,再弹获得, 然后recover
                if self.ShowRewardTick then
                    invoke(self.ShowRewardTick)
                end
                self.ShowRewardTick = RegisterTickOnce(function()
                    LuaCommonGetRewardWnd.m_materialName = "gongxihuode"
                    LuaCommonGetRewardWnd.m_Reward1List = { {ItemID=itemId, Count=1} }
                    LuaCommonGetRewardWnd.m_Reward2Label = ""
                    LuaCommonGetRewardWnd.m_Reward2List = {  }
                    LuaCommonGetRewardWnd.m_hint = ""
                    LuaCommonGetRewardWnd.m_button = {
                        {spriteName="blue", buttonLabel = LocalString.GetString("确定"), clickCB = function() CUIManager.CloseUI(CLuaUIResources.CommonGetRewardWnd) end},
                    }
                    CUIManager.ShowUI(CLuaUIResources.CommonGetRewardWnd)
                    for i = 1, #self.pokeButtonList do
                        self.pokeButtonList[i].transform:Find("BrokenState").gameObject:SetActive(false)
                    end
                    self.ShowRewardTick = nil
                end, self.showRewardDelay * 1000)
            end
        end
    end
end

function LuaHanJia2023LotteryWnd:InitWndData()
    self.lastPokeButtonIdx = -1
    self.displayRewardItemIds = HanJia2023_HanHuaMiBaoSetting.GetData().DisplayRewards
    self.huoLiPerDraw = HanJia2023_HanHuaMiBaoSetting.GetData().LotteryNeedHuoLi
    
    self.curHuoLi = 0

    self.pokeButtonList = {self.PokeButton1, self.PokeButton2, self.PokeButton3, self.PokeButton4, self.PokeButton5,
                            self.PokeButton6, self.PokeButton7, self.PokeButton8, self.PokeButton9}
    
    self.showRewardDelay = 0.3
end

function LuaHanJia2023LotteryWnd:RefreshConstUI()
    local rewardItemList = {self.RewardItem1, self.RewardItem2, self.RewardItem3, self.RewardItem4, self.RewardItem5}
    for i = 1, #rewardItemList do
        local itemId = self.displayRewardItemIds[i-1]
        if itemId then
            self:InitOneItem(rewardItemList[i], itemId)
            rewardItemList[i]:SetActive(true)
        else
            rewardItemList[i]:SetActive(false)
        end
    end
end

function LuaHanJia2023LotteryWnd:InitOneItem(curItem, itemID)
    local texture = curItem.transform:Find("Item"):GetComponent(typeof(CUITexture))
    local ItemData = Item_Item.GetData(itemID)
    if ItemData then texture:LoadMaterial(ItemData.Icon) end
    UIEventListener.Get(curItem).onClick = DelegateFactory.VoidDelegate(function (_)
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemID, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
    end)
end

function LuaHanJia2023LotteryWnd:InitUIEvent()
    UIEventListener.Get(self.RuleButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        g_MessageMgr:ShowMessage("HanJia2023_Lottery_Tips")
    end)
    
    for i = 1, #self.pokeButtonList do
        UIEventListener.Get(self.pokeButtonList[i]).onClick = DelegateFactory.VoidDelegate(function (_)
            if LuaHanJia2023Mgr.lotteryData then
                --有数据了再相应点击吧
                if LuaHanJia2023Mgr.lotteryData.lotteryResult[i] then
                    --这个东西已经被戳过了
                    g_MessageMgr:ShowMessage("HanJia2023_Lottery_PockBefore")
                else
                    --没被戳过, 判断积分够不够戳
                    if self.curHuoLi >= self.huoLiPerDraw then
                        Gac2Gas.LotteryHanHuaMiBao(i)
                    else
                        g_MessageMgr:ShowMessage("HanJia2023_Lottery_JiFenNotSatisfy")
                    end
                end
            end
        end)
    end
end

--@region UIEvent

--@endregion UIEvent

