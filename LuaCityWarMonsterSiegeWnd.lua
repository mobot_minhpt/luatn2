local UIProgressBar=import "UIProgressBar"

local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local CommonDefs = import "L10.Game.CommonDefs"
local Vector3 = import "UnityEngine.Vector3"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local UICommonDef = import "L10.UI.CUICommonDef"
local Extensions = import "Extensions"
local UISprite = import "UISprite"

LuaCityWarMonsterSiegeWnd = class()

RegistClassMember(LuaCityWarMonsterSiegeWnd,"m_ExpandButton")
RegistClassMember(LuaCityWarMonsterSiegeWnd,"m_DamageProgressBar")
RegistClassMember(LuaCityWarMonsterSiegeWnd,"m_PercentageLabel")
RegistClassMember(LuaCityWarMonsterSiegeWnd,"m_MonsterProgressBar")

RegistClassMember(LuaCityWarMonsterSiegeWnd,"m_BossIconParent")
RegistClassMember(LuaCityWarMonsterSiegeWnd,"m_BossIconTemplate")
RegistClassMember(LuaCityWarMonsterSiegeWnd,"m_BossIconTable")
RegistClassMember(LuaCityWarMonsterSiegeWnd,"m_bossNumber")

function LuaCityWarMonsterSiegeWnd:Init()
	self.m_ExpandButton = self.transform:Find("Anchor/Tip/ExpandButton").gameObject
	self.m_DamageProgressBar = self.transform:Find("Anchor/Content/DamageProgressBar"):GetComponent(typeof(UIProgressBar))
	self.m_PercentageLabel = self.transform:Find("Anchor/Content/Bg/PercentageLabel"):GetComponent(typeof(UILabel))
	self.m_MonsterProgressBar = self.transform:Find("Anchor/Content/MonsterProgressBar"):GetComponent(typeof(UIProgressBar))
	self.m_BossIconParent = self.transform:Find("Anchor/Content/MonsterProgressBar/Table").gameObject
	self.m_BossIconTemplate = self.transform:Find("Anchor/Content/MonsterProgressBar/BossIconTemplate").gameObject
	self.m_BossIconTable={}

	--获取Boss数量
	self.m_bossNumber=MonsterSiege_Reward.GetDataCount()		

	self:ClearBossAvatar()
	self:InitBossAvatar()	--生成Boss图标

	CommonDefs.AddOnClickListener(self.m_ExpandButton, DelegateFactory.Action_GameObject(function(go) self:OnExpandButtonClick() end), false)

	--Value initialization
	self:UpdateDamageValue(1,1)
	self:UpdateProgressValue(0,1)
	self:UpdateBossDisplay(0)
end

function LuaCityWarMonsterSiegeWnd:InitBossAvatar()
	local length=self.m_BossIconParent.transform.parent.gameObject:GetComponent(typeof(UISprite)).width --进度条总长度
	local interval=length/self.m_bossNumber
	local org_position=self.m_BossIconParent.transform.localPosition
	self.m_BossIconParent.transform.localPosition=Vector3(-length/2,org_position.y,org_position.z) --将Table节点固定在进度条最左侧

	local maxSize=self.m_BossIconTemplate:GetComponent(typeof(UISprite)).width --最后一个boss图标的大小（模板图标的边长）
	local minSize=maxSize*0.75

	self.m_BossIconTable={}
	for i = 1, self.m_bossNumber do
		self.m_BossIconTable[i]=UICommonDef.AddChild(self.m_BossIconParent,self.m_BossIconTemplate)
		self.m_BossIconTable[i]:SetActive(true)
		self.m_BossIconTable[i].transform.localPosition=Vector3(interval*i,0,0)

		--调整boss头像大小
		local sprite=self.m_BossIconTable[i]:GetComponent(typeof(UISprite))
		sprite.width=minSize+(maxSize-minSize)*(i-1)/(self.m_bossNumber-1)
		sprite.height=sprite.width
	end

	self.m_BossIconTemplate:SetActive(false)
end

function LuaCityWarMonsterSiegeWnd:ClearBossAvatar()
	Extensions.RemoveAllChildren(self.m_BossIconParent.transform)
	self.m_BossIconTable={}
end

function LuaCityWarMonsterSiegeWnd:UpdateDamageValue(currentHp,fullHp)
	local ratio=0
	if currentHp and fullHp and fullHp~=0 then
		ratio=currentHp/fullHp
	end

	self.m_DamageProgressBar.value=ratio
	self.m_PercentageLabel.text=tostring(math.ceil(ratio*100)).."%"
end

function LuaCityWarMonsterSiegeWnd:UpdateProgressValue(killMonsterNum,totalMonsterNum)
	local ratio=0
	if killMonsterNum and totalMonsterNum and totalMonsterNum ~=0 then
		ratio=killMonsterNum/totalMonsterNum
	end
	self.m_MonsterProgressBar.value=ratio
end

function LuaCityWarMonsterSiegeWnd:UpdateBossDisplay(killBossNum)
	for i = 1, self.m_bossNumber do
		local sprite=self.m_BossIconTable[i]:GetComponent(typeof(UISprite))
		local org_position=self.m_BossIconTable[i].transform.position
		if(i<=killBossNum) then
			self.m_BossIconTable[i].transform.position=Vector3(org_position.x,org_position.y,-1)
			sprite.alpha=0.8
		else
			self.m_BossIconTable[i].transform.position=Vector3(org_position.x,org_position.y,0)
			sprite.alpha=1
		end
	end
end

function LuaCityWarMonsterSiegeWnd:OnEnable()
	g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")

	g_ScriptEvent:AddListener("OnUpdateMonsterSiegeInfo", self, "OnUpdateMonsterSiegeInfo")

end

function LuaCityWarMonsterSiegeWnd:OnDisable()
	g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")

	g_ScriptEvent:RemoveListener("OnUpdateMonsterSiegeInfo", self, "OnUpdateMonsterSiegeInfo")
end

function LuaCityWarMonsterSiegeWnd:OnExpandButtonClick()
    self.m_ExpandButton.transform.localEulerAngles = Vector3(0, 0, 0)
    CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end

function LuaCityWarMonsterSiegeWnd:OnHideTopAndRightTipWnd()
    self.m_ExpandButton.transform.localEulerAngles = Vector3(0, 0, 180)
end

function LuaCityWarMonsterSiegeWnd:OnUpdateMonsterSiegeInfo(fullHp,currentHp,killMonsterNum,totalMonsterNum,killBossNum)
	self:UpdateDamageValue(currentHp,fullHp)
	self:UpdateProgressValue(killMonsterNum,totalMonsterNum)
	self:UpdateBossDisplay(killBossNum)
end
