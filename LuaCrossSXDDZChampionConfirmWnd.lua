require("3rdParty/ScriptEvent")
require("common/common_include")

local UILabel = import "UILabel"
local CButton = import "L10.UI.CButton"

CLuaCrossSXDDZChampionConfirmWnd=class()
RegistChildComponent(CLuaCrossSXDDZChampionConfirmWnd, "m_Text", UILabel)
RegistChildComponent(CLuaCrossSXDDZChampionConfirmWnd, "m_CountLbl", UILabel)
RegistChildComponent(CLuaCrossSXDDZChampionConfirmWnd, "m_OkBtn", CButton)
RegistChildComponent(CLuaCrossSXDDZChampionConfirmWnd, "m_CancelBtn", CButton)
RegistClassMember(CLuaCrossSXDDZChampionConfirmWnd, "m_Count")
RegistClassMember(CLuaCrossSXDDZChampionConfirmWnd, "m_Tick")


function CLuaCrossSXDDZChampionConfirmWnd:Awake()
end

function CLuaCrossSXDDZChampionConfirmWnd:Init()
	local msg = CLuaCrossSXDDZChampionConfirmWnd.Message
	self.m_Text.text = msg
	self.m_Tick = nil

	CommonDefs.AddOnClickListener(self.m_OkBtn.gameObject, DelegateFactory.Action_GameObject(function (go)
		if CLuaCrossSXDDZChampionConfirmWnd.ActionType == 1 then
			Gac2Gas.CrossSXDDZChallenged_Accept()
		elseif CLuaCrossSXDDZChampionConfirmWnd.ActionType == 2 then
			Gac2Gas.QueryBattleStartEnterConfirm_Accept()
		end
		CUIManager.CloseUI(CLuaUIResources.CrossSXDDZChampionConfirmWnd)
		if self.m_Tick then
			UnRegisterTick(self.m_Tick)
			self.m_Tick = nil
		end
	end), false)
	CommonDefs.AddOnClickListener(self.m_CancelBtn.gameObject, DelegateFactory.Action_GameObject(function (go)
		if CLuaCrossSXDDZChampionConfirmWnd.ActionType == 1 then
			Gac2Gas.CrossSXDDZChallenged_Escape()
		elseif CLuaCrossSXDDZChampionConfirmWnd.ActionType == 2 then
			Gac2Gas.QueryBattleStartEnterConfirm_Escape()
		end		
		CUIManager.CloseUI(CLuaUIResources.CrossSXDDZChampionConfirmWnd)
		if self.m_Tick then
			UnRegisterTick(self.m_Tick)
			self.m_Tick = nil
		end
	end), false)

	self:InitCountDown()
end

function CLuaCrossSXDDZChampionConfirmWnd:InitCountDown()
	self.m_Count = CLuaCrossSXDDZChampionConfirmWnd.WaitDuration
	self.m_CountLbl.text = tonumber(self.m_Count)

	self.m_Tick = RegisterTickWithDuration(function ()
		self.m_Count = self.m_Count - 1
		if self.m_Count < 0 then
			CUIManager.CloseUI(CLuaUIResources.CrossSXDDZChampionConfirmWnd)
		end
		self.m_CountLbl.text = LocalString.GetString("认输(")..tonumber(self.m_Count)..")"
	end, 1000, (self.m_Count+1)*1000)
end

