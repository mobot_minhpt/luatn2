local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local MessageWndManager = import "L10.UI.MessageWndManager"

LuaDoubleOneBaotuanBeginWnd = class()
RegistChildComponent(LuaDoubleOneBaotuanBeginWnd,"closeBtn", GameObject)
RegistChildComponent(LuaDoubleOneBaotuanBeginWnd,"attendBtn", GameObject)
RegistChildComponent(LuaDoubleOneBaotuanBeginWnd,"cancelBtn", GameObject)
RegistChildComponent(LuaDoubleOneBaotuanBeginWnd,"text", UILabel)

RegistClassMember(LuaDoubleOneBaotuanBeginWnd, "maxChooseNum")

function LuaDoubleOneBaotuanBeginWnd:Close()
  CUIManager.CloseUI("DoubleOneBaotuanBeginWnd")
end

function LuaDoubleOneBaotuanBeginWnd:OnEnable()
	g_ScriptEvent:AddListener("DoubleOneBaotuanAttendSuccess", self, "AttendSucc")
	g_ScriptEvent:AddListener("DoubleOneBaotuanCancelAttendSuccess", self, "CancelSucc")
end

function LuaDoubleOneBaotuanBeginWnd:OnDisable()
	g_ScriptEvent:RemoveListener("DoubleOneBaotuanAttendSuccess", self, "AttendSucc")
	g_ScriptEvent:RemoveListener("DoubleOneBaotuanCancelAttendSuccess", self, "CancelSucc")
end

function LuaDoubleOneBaotuanBeginWnd:AttendSucc()
  self.attendBtn:SetActive(false)
  self.cancelBtn:SetActive(true)
end

function LuaDoubleOneBaotuanBeginWnd:CancelSucc()
  self.attendBtn:SetActive(true)
  self.cancelBtn:SetActive(false)
end

function LuaDoubleOneBaotuanBeginWnd:Init()
	local onCloseClick = function(go)
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.closeBtn,DelegateFactory.Action_GameObject(onCloseClick),false)
	local onAttendClick = function(go)
		if LuaDoubleOne2019Mgr.BaotuanLeftTime and LuaDoubleOne2019Mgr.BaotuanLeftTime <= 0 then
			MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage('BAO_TUAN_ZUO_ZHAN_AWARD_TIMES_LIMIT_CLIENT'), DelegateFactory.Action(function ()
				Gac2Gas.RequestApplyBaoTuanZuoZhan()
			end), nil, nil, nil, false)
		else
			Gac2Gas.RequestApplyBaoTuanZuoZhan()
		end
	end
	CommonDefs.AddOnClickListener(self.attendBtn,DelegateFactory.Action_GameObject(onAttendClick),false)
	local onCancelClick = function(go)
    Gac2Gas.CancelApplyBaoTuanZuoZhan()
	end
	CommonDefs.AddOnClickListener(self.cancelBtn,DelegateFactory.Action_GameObject(onCancelClick),false)

  if LuaDoubleOne2019Mgr.BaotuanApply then
    self.attendBtn:SetActive(false)
    self.cancelBtn:SetActive(true)
  else
    self.attendBtn:SetActive(true)
    self.cancelBtn:SetActive(false)
  end

	self.text.text = g_MessageMgr:FormatMessage("BaoTuanZuoZhanTips")
end

return LuaDoubleOneBaotuanBeginWnd
