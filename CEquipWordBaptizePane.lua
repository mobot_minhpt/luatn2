-- Auto Generated!!
local CEquipmentProcessMgr = import "L10.Game.CEquipmentProcessMgr"
local CEquipWordBaptizePane = import "L10.UI.CEquipWordBaptizePane"
local CItemMgr = import "L10.Game.CItemMgr"
local CMiddleNoticeMgr = import "L10.UI.CMiddleNoticeMgr"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local DelegateFactory = import "DelegateFactory"
local Item_Item = import "L10.Game.Item_Item"
local LocalString = import "LocalString"
local Object = import "System.Object"
local UIEventListener = import "UIEventListener"

CEquipWordBaptizePane.m_OnSelected_CS2LuaHook = function (this, itemId) 
    this:SetBaseInfo()
    local item = CItemMgr.Inst:GetById(itemId)
    --UpdateEquipBaptizeDetail(item);
    this.equipItem:UpdateData(itemId)
    this.tipLabel.text = g_MessageMgr:FormatMessage("EquipBaptize_CiTiao_ReadMe_Index")
end
CEquipWordBaptizePane.m_SetBaseInfo_CS2LuaHook = function (this) 
    local equip = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
    local equipItem = CEquipmentProcessMgr.Inst.BaptizingEquipment
    local cost = CEquipmentProcessMgr.Inst:GetBaptizeCost(equip)
    if cost ~= nil then
        --初始化icon和名字
        local template = Item_Item.GetData(cost.WordCost[0])
        if template ~= nil then
            this.wordCostMatNameLabel.text = template.Name
            this.wordCostMatIcon:LoadMaterial(template.Icon)
        end
        this.jingping:Refresh()
    else
        this.jingping:InitNull()
    end
end
CEquipWordBaptizePane.m_Awake_CS2LuaHook = function (this) 
    UIEventListener.Get(this.tipButton).onClick = DelegateFactory.VoidDelegate(function (p) 
        g_MessageMgr:ShowMessage("EquipBaptize_CiTiao_ReadMe")
    end)
    UIEventListener.Get(this.autoBaptizeWordBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        if CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize == nil then
            --
            CMiddleNoticeMgr.ShowMiddleNotice(LocalString.GetString("至少选中一件装备"), 1)
        else
            if not CEquipmentProcessMgr.Inst:CheckOperationConflict(CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize) then
                return
            end
            --一键洗词条
            CUIManager.ShowUI(CUIResources.EquipWordAutoBaptizeWnd)
        end
    end)
    UIEventListener.Get(this.baptizeWordBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        if CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize == nil then
            --
            CMiddleNoticeMgr.ShowMiddleNotice(LocalString.GetString("至少选中一件装备"), 1)
        else
            if not CEquipmentProcessMgr.Inst:CheckOperationConflict(CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize) then
                return
            end
            if this.wordCostMatNum >= this.wordCostNum then
                CUIManager.ShowUI(CUIResources.EquipWordBaptizeWnd)
            else
                --CMiddleNoticeMgr.ShowMiddleNotice("净瓶数量太少，无法洗练");
                g_MessageMgr:ShowMessage("BAPTIZE_EQUIP_WORD_ITEM_NOT_ENOUGH", List2Params(InitializeList(CreateFromClass(MakeGenericClass(List, Object)), Object, LocalString.GetString("净瓶"))))
            end
        end
    end)
end
