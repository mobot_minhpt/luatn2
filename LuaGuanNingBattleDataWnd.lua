
local LuaGameObject=import "LuaGameObject"
local DefaultModelTextureLoader=import "L10.UI.DefaultModelTextureLoader"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local Profession=import "L10.Game.Profession"
-- local Gac2Gas=import "L10.Game.Gac2Gas"

CLuaGuanNingBattleDataWnd=class()
-- RegistClassMember(CLuaGuanNingBattleDataWnd,"m_ShareButton")
RegistClassMember(CLuaGuanNingBattleDataWnd,"m_LastBattleDataView")
RegistClassMember(CLuaGuanNingBattleDataWnd,"m_HistoryBattleDataView")
RegistClassMember(CLuaGuanNingBattleDataWnd,"m_FriendRankView")

RegistClassMember(CLuaGuanNingBattleDataWnd,"m_ModelTexture")
RegistClassMember(CLuaGuanNingBattleDataWnd,"m_IDLabel")
RegistClassMember(CLuaGuanNingBattleDataWnd,"m_ClsLabel")
RegistClassMember(CLuaGuanNingBattleDataWnd,"m_NameLabel")
RegistClassMember(CLuaGuanNingBattleDataWnd,"m_LevelLabel")
-- RegistClassMember(CLuaGuanNingBattleDataWnd,"m_ClsSprite")
RegistClassMember(CLuaGuanNingBattleDataWnd,"m_ModelLoader")

RegistClassMember(CLuaGuanNingBattleDataWnd,"m_LeftView")
RegistClassMember(CLuaGuanNingBattleDataWnd,"m_TitleLabel")

RegistChildComponent(CLuaGuanNingBattleDataWnd, "m_Alert", "Alert", GameObject)
RegistChildComponent(CLuaGuanNingBattleDataWnd, "m_Alert2", "Alert2", GameObject)

function CLuaGuanNingBattleDataWnd:Awake()
    self.m_Alert:SetActive(CLuaGuanNingMgr.m_BreakRecords.isLastBattleDataBreakRecord)
    self.m_Alert2:SetActive(CLuaGuanNingMgr.m_BreakRecords.weekData and not CLuaGuanNingMgr.m_BreakRecords.isLastBattleDataBreakRecord)
end

function CLuaGuanNingBattleDataWnd:Init()
    local g = LuaGameObject.GetChildNoGC(self.transform,"CloseButton").gameObject
    UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.CloseUI(CUIResources.GuanNingBattleDataWnd)
    end)

    self.m_LastBattleDataView=CLuaGuanNingLastBattleDataView:new()
    self.m_LastBattleDataView:Init(LuaGameObject.GetChildNoGC(self.transform,"LastData").transform)
    self.m_HistoryBattleDataView=CLuaGuanNingHistoryBattleDataView:new()
    self.m_HistoryBattleDataView:Init(LuaGameObject.GetChildNoGC(self.transform,"HistoryData").transform)

    self.m_FriendRankView=CLuaGuanNingFriendRankView:new()
    self.m_FriendRankView:Init(LuaGameObject.GetChildNoGC(self.transform,"FriendRank").transform)

    self.m_TitleLabel=LuaGameObject.GetChildNoGC(self.transform,"TitleLabel").label

    local tabView=LuaGameObject.GetLuaGameObjectNoGC(self.transform).qnTabView
    self.m_LeftView=LuaGameObject.GetChildNoGC(self.transform,"Main").gameObject
    tabView.OnSelect=DelegateFactory.Action_QnTabButton_int(function (p1,p2)
        if p2==0 then
            self.m_LeftView:SetActive(true)
            self.m_TitleLabel.text=LocalString.GetString("本场表现")
        elseif p2==1 then
            self.m_LeftView:SetActive(true)
            self.m_TitleLabel.text=LocalString.GetString("本周数据")
        elseif p2==2 then
            self.m_LeftView:SetActive(false)
            self.m_TitleLabel.text=LocalString.GetString("好友排行")
            self.m_FriendRankView:OnEnable()
        end
        --EventManager.BroadcastInternalForLua(EnumEventType.Guide_ChangeView, {"GuanNingBattleDataWnd"})
    end)

    if CLuaGuanNingMgr.m_BreakRecords.weekData and not CLuaGuanNingMgr.m_BreakRecords.isLastBattleDataBreakRecord then
        tabView:GetTabGameObject(0):SetActive(true)
        tabView:ChangeTo(1)
        LuaUtils.SetLocalPositionY(tabView:GetTabGameObject(1).transform,72)
        LuaUtils.SetLocalPositionY(tabView:GetTabGameObject(2).transform,-144)
    elseif CLuaGuanNingMgr.ShowLastBattleData then
        tabView:ChangeTo(0)
        tabView:GetTabGameObject(0):SetActive(true)
        LuaUtils.SetLocalPositionY(tabView:GetTabGameObject(1).transform,72)
        LuaUtils.SetLocalPositionY(tabView:GetTabGameObject(2).transform,-144)
    else
        tabView:ChangeTo(1)
        tabView:GetTabGameObject(0):SetActive(false)
        LuaUtils.SetLocalPositionY(tabView:GetTabGameObject(1).transform,288)
        LuaUtils.SetLocalPositionY(tabView:GetTabGameObject(2).transform,72)
    end





    self.m_ModelTexture=LuaGameObject.GetChildNoGC(self.transform,"ModelTexture").cTexture
    self.m_ModelLoader = DefaultModelTextureLoader.Create()
    self.m_ModelTexture.mainTexture=CUIManager.CreateModelTexture("__GuanNingMainPlayerModelCamera__", self.m_ModelLoader,180,0.05,-1,4.66,false,true,1)
    
    self.m_LevelLabel=LuaGameObject.GetChildNoGC(self.transform,"PlayerLevelLabel").label
    self.m_NameLabel=LuaGameObject.GetChildNoGC(self.transform,"PlayerNameLabel").label
    self.m_ClsLabel=LuaGameObject.GetChildNoGC(self.transform,"PlayerClassLabel").label
    self.m_IDLabel=LuaGameObject.GetChildNoGC(self.transform,"PlayerIDLabel").label


    self:InitPlayerInfo()

    if not CLuaGuanNingMgr.ShowLastBattleData then -- 从结算界面打开时不需要再次请求，防止突破记录被清掉
        Gac2Gas.QueryGuanNingHistoryWeekData()
    end
    Gac2Gas.QueryFriendGuanNingRankData()


end

function CLuaGuanNingBattleDataWnd:InitPlayerInfo()
    if CClientMainPlayer.Inst then
        local mainPlayer=CClientMainPlayer.Inst
        self.m_LevelLabel.text=tostring(mainPlayer.Level)
        self.m_NameLabel.text=mainPlayer.Name
        self.m_ClsLabel.text=Profession.GetFullName(mainPlayer.Class)
        self.m_IDLabel.text=tostring(mainPlayer.Id)
        -- self.m_ClsSprite.spriteName=Profession.GetIcon(mainPlayer.Class)
    end
end

function CLuaGuanNingBattleDataWnd:OnEnable()
    g_ScriptEvent:AddListener("QueryGuanNingHistoryWeekDataResult", self, "OnQueryGuanNingHistoryWeekDataResult")
    -- g_ScriptEvent:AddListener("QueryGuanNingShareImageUrl", self, "OnQueryGuanNingShareImageUrl")
    g_ScriptEvent:AddListener("QueryGuanNingRankDataResult", self, "OnQueryGuanNingRankDataResult")
end

function CLuaGuanNingBattleDataWnd:OnDisable()
    g_ScriptEvent:RemoveListener("QueryGuanNingHistoryWeekDataResult", self, "OnQueryGuanNingHistoryWeekDataResult")
    -- g_ScriptEvent:RemoveListener("QueryGuanNingShareImageUrl", self, "OnQueryGuanNingShareImageUrl")
    g_ScriptEvent:RemoveListener("QueryGuanNingRankDataResult", self, "OnQueryGuanNingRankDataResult")
    CLuaGuanNingMgr.m_BreakRecords = {}
end
function CLuaGuanNingBattleDataWnd:OnDestroy()
    if self.m_ModelTexture then
        self.m_ModelTexture.mainTexture=nil
    end
    CUIManager.DestroyModelTexture("__GuanNingMainPlayerModelCamera__")
end

function CLuaGuanNingBattleDataWnd:OnQueryGuanNingHistoryWeekDataResult()
    self.m_HistoryBattleDataView:InitData()
end
-- function CLuaGuanNingBattleDataWnd:OnQueryGuanNingShareImageUrl( )
--     self.m_LastBattleDataView:OnQueryGuanNingShareImageUrl()
-- end
function  CLuaGuanNingBattleDataWnd:OnQueryGuanNingRankDataResult(rank)
    self.m_FriendRankView:OnQueryGuanNingRankDataResult(rank)
end

function CLuaGuanNingBattleDataWnd:GetGuideGo(methodName)
    if methodName == "GetWeekOverviewBtn" then
        return self.transform:Find("Anchor/HistoryData/TabView"):GetChild(1).gameObject
    elseif methodName == "GetThumbsUpCnt" then
        return self.transform:Find("Anchor/HistoryData/Common/ScoreLabels/5/Bg").gameObject
    elseif methodName == "GetWeekDataBtn" then
        return self.transform:Find("Wnd_Bg_Primary_Tab/Tabs"):GetChild(1).gameObject
    end
end

function CLuaGuanNingBattleDataWnd:GetTabIndex()
    if self.transform:Find("Anchor/LastData").gameObject.activeSelf then
        return 0
    elseif self.transform:Find("Anchor/HistoryData").gameObject.activeSelf then
        return 1
    else
        return 2
    end
end

return CLuaGuanNingBattleDataWnd
