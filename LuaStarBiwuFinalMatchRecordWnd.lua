local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local QnTableView=import "L10.UI.QnTableView"
local QnButton=import "L10.UI.QnButton"
local CCommonSelector=import "L10.UI.CCommonSelector"

CLuaStarBiwuFinalMatchRecordWnd = class()
CLuaStarBiwuFinalMatchRecordWnd.Path = "ui/starbiwu/LuaStarBiwuFinalMatchRecordWnd"
RegistClassMember(CLuaStarBiwuFinalMatchRecordWnd,"m_WatchBtn")
RegistClassMember(CLuaStarBiwuFinalMatchRecordWnd,"m_AdvGridView")
RegistClassMember(CLuaStarBiwuFinalMatchRecordWnd,"m_TitleLabel")
RegistClassMember(CLuaStarBiwuFinalMatchRecordWnd,"m_TimeLabel")
RegistClassMember(CLuaStarBiwuFinalMatchRecordWnd,"m_JueSaiSelector")
RegistClassMember(CLuaStarBiwuFinalMatchRecordWnd,"m_JueSaiIndex")
RegistClassMember(CLuaStarBiwuFinalMatchRecordWnd,"m_CurrentRow")

RegistClassMember(CLuaStarBiwuFinalMatchRecordWnd,"m_DataSource")
RegistClassMember(CLuaStarBiwuFinalMatchRecordWnd,"m_ZongJueSaiTimeStr")
RegistClassMember(CLuaStarBiwuFinalMatchRecordWnd,"m_JueSaiNameList")

CLuaStarBiwuFinalMatchRecordWnd.QuarterFinals = 4

function CLuaStarBiwuFinalMatchRecordWnd:Awake()
    self.m_WatchBtn = self.transform:Find("ShowArea/ViewButton"):GetComponent(typeof(QnButton))
    self.m_AdvGridView = self.transform:Find("ShowArea/QnAdvView"):GetComponent(typeof(QnTableView))
    self.m_TitleLabel = self.transform:Find("Wnd_Bg_Primary_Normal/TitleLabel"):GetComponent(typeof(UILabel))
    self.m_TimeLabel = self.transform:Find("ShowArea/TopArea/TimeLabel"):GetComponent(typeof(UILabel))
    self.m_JueSaiSelector = self.transform:Find("ShowArea/TopArea/JueSaiSelector"):GetComponent(typeof(CCommonSelector))
    self.m_JueSaiIndex = 0
    self.m_CurrentRow = -1
    self.m_JueSaiNameList={
        LocalString.GetString("八强赛"),
        LocalString.GetString("胜者组第一场"),
        LocalString.GetString("胜者组第二场"),
        LocalString.GetString("胜者组决赛"),
        LocalString.GetString("败者组第一场"),
        LocalString.GetString("败者组决赛"),
        LocalString.GetString("总决赛")
    }
    self.m_ZongJueSaiTimeStr = StarBiWuShow_Setting.GetData().ZongJueSaiTimeDes

    local getNumFunc=function() return #CLuaStarBiwuMgr.m_FinalMatchRecordList end
    local initItemFunc=function(item,index)
        if index % 2 == 1 then
            item:SetBackgroundTexture(g_sprites.OddBgSpirite)
        else
            item:SetBackgroundTexture(g_sprites.EvenBgSprite)
        end

        self:InitItem(item,index,CLuaStarBiwuMgr.m_FinalMatchRecordList[index+1])
    end

    self.m_DataSource=DefaultTableViewDataSource.Create(getNumFunc,initItemFunc)
    self.m_AdvGridView.m_DataSource=self.m_DataSource
    self.m_AdvGridView.OnSelectAtRow=DelegateFactory.Action_int(function (row)
        self.m_CurrentRow = row
        if CLuaStarBiwuMgr.m_FinalMatchRecordList[self.m_CurrentRow+1].m_Win < 0
            and CLuaStarBiwuMgr.m_FinalMatchRecordList[self.m_CurrentRow+1].m_IsMatching then
            self.m_WatchBtn.Enabled = true
        else
            self.m_WatchBtn.Enabled = false
        end
    end)

    local refreshButton=FindChild(self.transform,"RefreshButton").gameObject
    UIEventListener.Get(refreshButton).onClick=DelegateFactory.VoidDelegate(function(go)
        Gac2Gas.RequestStarBiwuZongJueSaiWatchList(self.m_JueSaiIndex)
    end)
end


function CLuaStarBiwuFinalMatchRecordWnd:OnEnable( )
    UIEventListener.Get(self.m_WatchBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) self:OnWatchBtnClicked(go) end)
    g_ScriptEvent:AddListener("ReplyStarBiwuZongJueSaiWatchList", self, "ReplyStarBiwuZongJueSaiWatchList")

end

function CLuaStarBiwuFinalMatchRecordWnd:OnDisable( )
    g_ScriptEvent:RemoveListener("ReplyStarBiwuZongJueSaiWatchList", self, "ReplyStarBiwuZongJueSaiWatchList")
end

function CLuaStarBiwuFinalMatchRecordWnd:ReplyStarBiwuZongJueSaiWatchList(playIndex)
  self.m_JueSaiIndex = playIndex
  self.m_JueSaiSelector:SetName(self.m_JueSaiNameList[playIndex > CLuaStarBiwuFinalMatchRecordWnd.QuarterFinals and playIndex - CLuaStarBiwuFinalMatchRecordWnd.QuarterFinals + 1 or 1])
  self.m_WatchBtn.Enabled = false
  self.m_AdvGridView:ReloadData(false, false)
end
function CLuaStarBiwuFinalMatchRecordWnd:Init( )
    self.m_JueSaiIndex = 1
    self.m_WatchBtn.Enabled = false
    self.m_TitleLabel.text = LocalString.GetString("总决赛观战")
    self.m_TimeLabel.text = LocalString.GetString("比赛时间：") .. self.m_ZongJueSaiTimeStr
    self.m_JueSaiSelector.gameObject:SetActive(true)
    local List_String = MakeGenericClass(List,cs_string)
    self.m_JueSaiSelector:Init(Table2List(self.m_JueSaiNameList,List_String),
        DelegateFactory.Action_int(function(index) self:OnJueSaiIndexClick(index) end))
    self.m_JueSaiSelector:SetName("")
    Gac2Gas.RequestStarBiwuZongJueSaiWatchList(self.m_JueSaiIndex)
end

function CLuaStarBiwuFinalMatchRecordWnd:OnJueSaiIndexClick( index)
    if index > 0 then index = CLuaStarBiwuFinalMatchRecordWnd.QuarterFinals + index end
    Gac2Gas.RequestStarBiwuZongJueSaiWatchList(index + 1)
end

function CLuaStarBiwuFinalMatchRecordWnd:OnWatchBtnClicked( go)
  Gac2Gas.RequestWatchStarBiwuZongJueSai(self.m_JueSaiIndex)
end

function CLuaStarBiwuFinalMatchRecordWnd:InitItem(item,index,record)
    local transform=item.transform
    local m_NameLabel={}
    m_NameLabel[0]=FindChild(transform,"NameLabel1"):GetComponent(typeof(UILabel))
    m_NameLabel[1]=FindChild(transform,"NameLabel2"):GetComponent(typeof(UILabel))
    local m_ResultSprite={}
    m_ResultSprite[0]=FindChild(transform,"ResultSprite1"):GetComponent(typeof(UISprite))
    m_ResultSprite[1]=FindChild(transform,"ResultSprite2"):GetComponent(typeof(UISprite))
    UIEventListener.Get(m_NameLabel[0].gameObject).onClick = DelegateFactory.VoidDelegate(function(go) 
        CLuaStarBiwuMgr:ShowZhanDuiMemberWnd(0, record.m_Zhandui1, 1)
    end)
    UIEventListener.Get(m_NameLabel[1].gameObject).onClick = DelegateFactory.VoidDelegate(function(go) 
        CLuaStarBiwuMgr:ShowZhanDuiMemberWnd(0, record.m_Zhandui2, 1)
    end)

    local m_CannotWatchObj = transform:Find("CannotWatchMark").gameObject
    local m_MatchingLabel = transform:Find("Label"):GetComponent(typeof(UILabel))
    m_NameLabel[0].text = record.m_TeamName1
    m_NameLabel[1].text = record.m_TeamName2

    if record.m_Win >= 0 then
        m_CannotWatchObj:SetActive(true)
        m_MatchingLabel.text = LocalString.GetString("VS")
        m_ResultSprite[0].gameObject:SetActive(true)
        m_ResultSprite[1].gameObject:SetActive(true)

        local default
        if record.m_Win == record.m_Zhandui1 then
            default = "common_fight_result_win"
        else
            default = "common_fight_result_lose"
        end
        m_ResultSprite[0].spriteName = default

        local extern
        if record.m_Win == record.m_Zhandui2 then
            extern = "common_fight_result_win"
        else
            extern = "common_fight_result_lose"
        end
        m_ResultSprite[1].spriteName = extern
    else
        if record.m_IsMatching then
            m_MatchingLabel.text = LocalString.GetString("进行中")
            m_ResultSprite[0].gameObject:SetActive(false)
            m_ResultSprite[1].gameObject:SetActive(false)
            m_CannotWatchObj:SetActive(false)
        else
            m_MatchingLabel.text = LocalString.GetString("准备中")
            m_ResultSprite[0].gameObject:SetActive(false)
            m_ResultSprite[1].gameObject:SetActive(false)
            m_CannotWatchObj:SetActive(true)
        end
    end
end
