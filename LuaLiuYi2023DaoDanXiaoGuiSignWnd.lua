local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnButton = import "L10.UI.QnButton"
local UILabel = import "UILabel"
local UIGrid = import "UIGrid"
local CButton = import "L10.UI.CButton"
local CUITexture = import "L10.UI.CUITexture"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemMgr = import "L10.Game.CItemMgr"

LuaLiuYi2023DaoDanXiaoGuiSignWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaLiuYi2023DaoDanXiaoGuiSignWnd, "RankBtn", "RankBtn", QnButton)
RegistChildComponent(LuaLiuYi2023DaoDanXiaoGuiSignWnd, "RuleBtn", "RuleBtn", QnButton)
RegistChildComponent(LuaLiuYi2023DaoDanXiaoGuiSignWnd, "TypeLab", "TypeLab", UILabel)
RegistChildComponent(LuaLiuYi2023DaoDanXiaoGuiSignWnd, "TimeLab", "TimeLab", UILabel)
RegistChildComponent(LuaLiuYi2023DaoDanXiaoGuiSignWnd, "LimitLab", "LimitLab", UILabel)
RegistChildComponent(LuaLiuYi2023DaoDanXiaoGuiSignWnd, "DescLab", "DescLab", UILabel)
RegistChildComponent(LuaLiuYi2023DaoDanXiaoGuiSignWnd, "RewardTimesLab", "RewardTimesLab", UILabel)
RegistChildComponent(LuaLiuYi2023DaoDanXiaoGuiSignWnd, "Rewards", "Rewards", UIGrid)
RegistChildComponent(LuaLiuYi2023DaoDanXiaoGuiSignWnd, "Reward1", "Reward1", QnButton)
RegistChildComponent(LuaLiuYi2023DaoDanXiaoGuiSignWnd, "Reward2", "Reward2", QnButton)
RegistChildComponent(LuaLiuYi2023DaoDanXiaoGuiSignWnd, "SignBtn", "SignBtn", CButton)
RegistChildComponent(LuaLiuYi2023DaoDanXiaoGuiSignWnd, "SignTimesLab", "SignTimesLab", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaLiuYi2023DaoDanXiaoGuiSignWnd, "m_MaxJoinNum")
RegistClassMember(LuaLiuYi2023DaoDanXiaoGuiSignWnd, "m_MinGrade")
RegistClassMember(LuaLiuYi2023DaoDanXiaoGuiSignWnd, "m_JoinRewardMaxNum")
RegistClassMember(LuaLiuYi2023DaoDanXiaoGuiSignWnd, "m_JoinRewardItemId")

function LuaLiuYi2023DaoDanXiaoGuiSignWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.m_MaxJoinNum       = tonumber(LiuYi2023_DaoDanXiaoGui.GetData("MaxJoinNum").Value)
    self.m_MinGrade         = tonumber(LiuYi2023_DaoDanXiaoGui.GetData("MinGrade").Value)
    self.m_JoinRewardMaxNum = tonumber(LiuYi2023_DaoDanXiaoGui.GetData("JoinRewardMaxNum").Value)
    self.m_WinRewardItemId  = tonumber((LiuYi2023_DaoDanXiaoGui.GetData("WinRewardItemId") or {}).Value)
    self.m_JoinRewardItemId = tonumber(LiuYi2023_DaoDanXiaoGui.GetData("JoinRewardItemId").Value)

    self.RankBtn.OnClick = DelegateFactory.Action_QnButton(function(btn)
        CUIManager.ShowUI(CLuaUIResources.LiuYi2023DaoDanXiaoGuiRankWnd)
    end)

    self.RuleBtn.OnClick = DelegateFactory.Action_QnButton(function(btn)
        g_MessageMgr:ShowMessage("LIUYI2023_DAODANXIAOGUI_RULE")
    end)

    UIEventListener.Get(self.SignBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go) 
        Gac2Gas.DaoDanXiaoGuiEnterGame()
    end)

    if CommonDefs.IS_VN_CLIENT then
        self.RewardTimesLab.transform.parent:Find("Title"):GetComponent(typeof(UILabel)).width = 240
    end
end

function LuaLiuYi2023DaoDanXiaoGuiSignWnd:Init()
    self.TimeLab.text = (LiuYi2023_DaoDanXiaoGui.GetData("TimeDesc") or {}).Value
    self.TypeLab.text = LocalString.GetString("三人及以上组队")
    self.LimitLab.text = SafeStringFormat3(LocalString.GetString("%d级"), self.m_MinGrade)
    self.DescLab.text = g_MessageMgr:FormatMessage("LIUYI2023_DAODANXIAOGUI_SIGNDESC")

    self.RewardTimesLab.text = SafeStringFormat3("%d/%d", 0, self.m_JoinRewardMaxNum)
    self.SignTimesLab.text = SafeStringFormat3("%d/%d", 0, self.m_MaxJoinNum)

    self.Rewards:Reposition()
    self:InitReward(self.Reward1, self.m_WinRewardItemId) 
    self:InitReward(self.Reward2, self.m_JoinRewardItemId)

    Gac2Gas.PlayerQueryDaoDanXiaoGuiData()
end

--@region UIEvent

--@endregion UIEvent


function LuaLiuYi2023DaoDanXiaoGuiSignWnd:OnEnable()
    g_ScriptEvent:AddListener("PlayerQueryDaoDanXiaoGuiDataResult", self, "OnPlayerQueryDaoDanXiaoGuiDataResult")
end

function LuaLiuYi2023DaoDanXiaoGuiSignWnd:OnDisable()
    g_ScriptEvent:RemoveListener("PlayerQueryDaoDanXiaoGuiDataResult", self, "OnPlayerQueryDaoDanXiaoGuiDataResult")
end

function LuaLiuYi2023DaoDanXiaoGuiSignWnd:InitReward(rewardBtn, itemTemplateId)
    local itemTex = rewardBtn.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))

    local item = CItemMgr.Inst:GetItemTemplate(itemTemplateId)
    if item then
        itemTex:LoadMaterial(item.Icon)
        rewardBtn.OnClick = DelegateFactory.Action_QnButton(function(btn)
            CItemInfoMgr.ShowLinkItemTemplateInfo(itemTemplateId, false, nil, AlignType.Default, 0, 0, 0, 0)
        end)
    end
end

function LuaLiuYi2023DaoDanXiaoGuiSignWnd:OnPlayerQueryDaoDanXiaoGuiDataResult(todayJoinTime, todayRewardTime, passTime)
    self.RewardTimesLab.text = SafeStringFormat3("%d/%d", todayRewardTime, self.m_JoinRewardMaxNum)
    self.SignTimesLab.text = SafeStringFormat3("%d/%d", todayJoinTime, self.m_MaxJoinNum)
end