
EnumCookingPlayType = {
	eNone = 0,
	eCooking = 1,
	eYinMengXiang = 2,
	eRandomCooking = 3,
}

LuaCookingPlayMgr = {}
LuaCookingPlayMgr.m_TaskId = 0
LuaCookingPlayMgr.m_CurrentCookId = 0
LuaCookingPlayMgr.m_CookIdList = {}
LuaCookingPlayMgr.m_PlayFinish = false
LuaCookingPlayMgr.m_CurrentProgress = 0
LuaCookingPlayMgr.m_TotalProgress = 1
LuaCookingPlayMgr.m_AllowShake = false
LuaCookingPlayMgr.m_Type = EnumCookingPlayType.eNone
LuaCookingPlayMgr.m_UIPath = ""
LuaCookingPlayMgr.m_ShakeAniName = "CookingPlay_Shake"
LuaCookingPlayMgr.m_JiuTanPath = "UI/Texture/Transparent/Material/fenjiu_jiugang.mat"


function LuaCookingPlayMgr:InitCookList(cookIdListUD)
	self.m_CookIdList = {}
	local cookIdList = MsgPackImpl.unpack(cookIdListUD)
	for i = 0, cookIdList.Count - 1 do
		table.insert(self.m_CookIdList, cookIdList[i])
	end
end
