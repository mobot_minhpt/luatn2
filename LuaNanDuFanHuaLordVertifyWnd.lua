require("common/common_include")

local UIGrid = import "UIGrid"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaNanDuFanHuaLordVertifyWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaNanDuFanHuaLordVertifyWnd,"ConditionGrid","ConditionGrid", UIGrid)
RegistChildComponent(LuaNanDuFanHuaLordVertifyWnd,"LordConditionTemplate","LordConditionTemplate", GameObject)
RegistChildComponent(LuaNanDuFanHuaLordVertifyWnd,"SuccesSprite","SuccesSprite", GameObject)
RegistChildComponent(LuaNanDuFanHuaLordVertifyWnd,"FailedSprite","FailedSprite", GameObject)

--@endregion RegistChildComponent end

function LuaNanDuFanHuaLordVertifyWnd:Init()
    self:OnCheckUpgradeLord()
end

function LuaNanDuFanHuaLordVertifyWnd:OnCheckUpgradeLord()
    self.lordData = {}
    if CClientMainPlayer.Inst and CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, LuaYiRenSiMgr.LordTempPlayDataKey) then
        local playData = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, LuaYiRenSiMgr.LordTempPlayDataKey)
        if playData.ExpiredTime > CServerTimeMgr.Inst.timeStamp and playData.Data and playData.Data.Data then
            local data = g_MessagePack.unpack(playData.Data.Data)
            self.lordData = data[1]
        end
    end

    local isSatisfyAll = true
    NanDuFanHua_LordTest.Foreach(function (k,v)
        local go = NGUITools.AddChild(self.ConditionGrid.gameObject, self.LordConditionTemplate.gameObject)
        go:SetActive(true)
        local isSatisfy = self:InitRow(go.transform, v)
        isSatisfyAll = isSatisfyAll and isSatisfy
    end)

    if isSatisfyAll and (self.lordData[LuaYiRenSiMgr.LordPropertyKey.isLordIndex] and self.lordData[LuaYiRenSiMgr.LordPropertyKey.isLordIndex] or true) then
        Gac2Gas.NanDuLordRequestBecomeLord()
        self.SuccesSprite:SetActive(true)
    else
        self.FailedSprite:SetActive(true)
    end
end

function LuaNanDuFanHuaLordVertifyWnd:InitRow(objTrans, cfg)
    local valueId = cfg.ID
    local value = self.lordData[valueId] and self.lordData[valueId] or 0
    local extraCondition = true
    if valueId == 1 then
        --第一项的时需要额外算一个负债
        extraCondition = (self.lordData[LuaYiRenSiMgr.LordPropertyKey.subMoneyIndex] and self.lordData[LuaYiRenSiMgr.LordPropertyKey.subMoneyIndex] or 0) <= 0
    elseif valueId == LuaYiRenSiMgr.LordPropertyKey.YiZhangIndex then
        --计算仪仗数据时
        self.fashionData = {}
        self.fashionAddValue = {}
        if CClientMainPlayer.Inst and CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, LuaYiRenSiMgr.LordTempPlayDataKey) then
            local playData = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, LuaYiRenSiMgr.LordTempPlayDataKey)
            if playData.ExpiredTime > CServerTimeMgr.Inst.timeStamp and playData.Data and playData.Data.Data then
                local data = g_MessagePack.unpack(playData.Data.Data)
                self.fashionData = data[5]
            end
        end
        local fashion1Id = self.fashionData[1] and self.fashionData[1] or 0
        self.fashionAddValue = {}
        if fashion1Id ~= 0 then
            self:ParseFashionAttrChange(self.fashionAddValue, NanDuFanHua_LordFashion.GetData(fashion1Id).AttrChange)
        end

        local fashion2Id = self.fashionData[2] and self.fashionData[2] or 0
        if fashion2Id ~= 0 then
            self:ParseFashionAttrChange(self.fashionAddValue, NanDuFanHua_LordFashion.GetData(fashion2Id).AttrChange)
        end

        local zuoqiId = self.fashionData[3] and self.fashionData[3] or 0
        if zuoqiId ~= 0 then
            self:ParseFashionAttrChange(self.fashionAddValue, NanDuFanHua_LordZuoQi.GetData(zuoqiId).AttrChange)
        end
        
        value = value + (self.fashionAddValue[6] and self.fashionAddValue[6] or 0)
    end
    local isSatisfy = value >= cfg.RequireNum and extraCondition
    
    objTrans:Find("Label"):GetComponent(typeof(UILabel)).text = CUICommonDef.TranslateToNGUIText(cfg.Text)
    objTrans:Find("Label/PassCheckSign").gameObject:SetActive(isSatisfy)
    objTrans:Find("Label/NoPassCheckSign").gameObject:SetActive(not isSatisfy)
    objTrans:Find("Label/LabelBg").gameObject:SetActive(valueId % 4 == 0 or valueId % 4 == 1)
    return isSatisfy
end

function LuaNanDuFanHuaLordVertifyWnd:ParseFashionAttrChange(tbl, parseText)
    local attrList = g_LuaUtil:StrSplit(parseText, ";")
    for i = 1, #attrList-1 do
        local subKeyValuePair = g_LuaUtil:StrSplit(attrList[i], ",")
        tbl[tonumber(subKeyValuePair[1])] = (tbl[tonumber(subKeyValuePair[1])] and tbl[tonumber(subKeyValuePair[1])] or 0) + tonumber(subKeyValuePair[2])
    end
end

