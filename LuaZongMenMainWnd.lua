local UITabBar = import "L10.UI.UITabBar"
local CGuideMgr=import "L10.Game.Guide.CGuideMgr"

LuaZongMenMainWnd = class()

RegistChildComponent(LuaZongMenMainWnd,"m_Tabs","Tabs", UITabBar)
RegistChildComponent(LuaZongMenMainWnd,"m_InfoView","InfoView", GameObject)
RegistChildComponent(LuaZongMenMainWnd,"m_SkillView","SkillView", GameObject)
RegistChildComponent(LuaZongMenMainWnd,"m_MemberView","MemberView", GameObject)
RegistChildComponent(LuaZongMenMainWnd,"m_NewMemberView","NewMemberView", GameObject)

function LuaZongMenMainWnd:Init()
    self.m_Tabs.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
        self:OnTabChange(index)
    end)
    self.m_Tabs:ChangeTab(LuaZongMenMgr.m_MainWndTabIndex or 0, false)
    LuaZongMenMgr.m_MainWndTabIndex = nil
    if CClientMainPlayer.Inst then
        --Gac2Gas.QuerySectRedAlert(CClientMainPlayer.Inst.BasicProp.SectId)
        self:OnSyncSectRedAlert(CClientMainPlayer.Inst.BasicProp.SectId, LuaZongMenMgr.m_ShowSectRedAlert)
    end
end

function LuaZongMenMainWnd:OnEnable()
    g_ScriptEvent:AddListener("OnSyncSectRedAlert", self, "OnSyncSectRedAlert")
end

function LuaZongMenMainWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnSyncSectRedAlert", self, "OnSyncSectRedAlert")
end

function LuaZongMenMainWnd:OnSyncSectRedAlert(sectId, bHasRedAlert)
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.SectId == sectId and self.m_Tabs and self.m_Tabs.SelectedIndex ~= 1 then
        local btn = self.m_Tabs:GetTabGoByIndex(1)
        btn.transform:Find("Alert").gameObject:SetActive(bHasRedAlert)
    end
end

function LuaZongMenMainWnd:OnTabChange(index)
    self.m_InfoView:SetActive(index == 0)
    self.m_SkillView:SetActive(index == 2)
    self.m_MemberView:SetActive(index == 1 and not LuaZongMenMgr.m_OpenNewSystem)
    self.m_NewMemberView:SetActive(index == 1 and LuaZongMenMgr.m_OpenNewSystem)
    if index == 1 and L10.Game.Guide.CGuideMgr.Inst:IsInPhase(EnumGuideKey.ReCommandZongMen) then
        L10.Game.Guide.CGuideMgr.Inst:TriggerGuide(3)
    end
    if index == 1 then
        local btn = self.m_Tabs:GetTabGoByIndex(1)
        btn.transform:Find("Alert").gameObject:SetActive(false)
    end
end

function LuaZongMenMainWnd:GetGuideGo(methodName)
    if methodName == "GetChangeMemberTab" then
        if self:GetTabIndex() == 1 and L10.Game.Guide.CGuideMgr.Inst:IsInPhase(EnumGuideKey.ReCommandZongMen) then
            L10.Game.Guide.CGuideMgr.Inst:TriggerGuide(3)
            return nil
        end
        return self.m_Tabs.transform:GetChild(1).gameObject
    elseif methodName == "GetPromoteSetting" then
        if LuaZongMenMgr.m_OpenNewSystem then
            CGuideMgr.Inst:EndCurrentPhase()
        end
        return self.m_MemberView.transform:Find("LeftTopRow/PromoteSetting").gameObject
    end
end

function LuaZongMenMainWnd:GetTabIndex()
    return self.m_Tabs.SelectedIndex
end