-- Auto Generated!!
local CChargeGuideCtrl = import "L10.UI.CChargeGuideCtrl"
local CommonDefs = import "L10.Game.CommonDefs"
local OnCenterCallback = import "UICenterOnChild+OnCenterCallback"
local QnButton = import "L10.UI.QnButton"
local UICenterOnChild = import "UICenterOnChild"
local CUICenterOnChild = import "L10.UI.CUICenterOnChild"
CChargeGuideCtrl.m_OnEnable_CS2LuaHook = function (this) 
    this.m_PrevButton.gameObject:SetActive(this.m_CurrentRow >= 1)
    this.m_NextButton.gameObject:SetActive(this.m_CurrentRow < this.m_Contents.Length - 1)
    this.m_PrevButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_PrevButton.OnClick, MakeDelegateFromCSFunction(this.OnPrevPage, MakeGenericClass(Action1, QnButton), this), true)
    this.m_NextButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_NextButton.OnClick, MakeDelegateFromCSFunction(this.OnNextPage, MakeGenericClass(Action1, QnButton), this), true)
    this.m_CenterOnChild.onCenter = CommonDefs.CombineListner_OnCenterCallback(this.m_CenterOnChild.onCenter, MakeDelegateFromCSFunction(this.OnCenterPage, OnCenterCallback, this), true)
end
CChargeGuideCtrl.m_OnNextPage_CS2LuaHook = function (this, button) 
    if this.m_CurrentRow < this.m_Contents.Length - 1 then
        this.m_CurrentRow = this.m_CurrentRow + 1
        if ENABLE_XLUA then
            cast(this.m_CenterOnChild, typeof(UICenterOnChild))
			this.m_CenterOnChild:CenterOn(this.m_Contents[this.m_CurrentRow])
			cast(this.m_CenterOnChild, typeof(CUICenterOnChild))
        else
            this.m_CenterOnChild:CenterOn(this.m_Contents[this.m_CurrentRow])
        end
    end
end
CChargeGuideCtrl.m_OnPrevPage_CS2LuaHook = function (this, button) 
    if this.m_CurrentRow >= 1 then
        this.m_CurrentRow = this.m_CurrentRow - 1
        if this.m_Contents.Length > this.m_CurrentRow - 1 then
            if ENABLE_XLUA then
                cast(this.m_CenterOnChild, typeof(UICenterOnChild))
                this.m_CenterOnChild:CenterOn(this.m_Contents[this.m_CurrentRow])
                cast(this.m_CenterOnChild, typeof(CUICenterOnChild))
            else
                this.m_CenterOnChild:CenterOn(this.m_Contents[this.m_CurrentRow])
            end
        end
    end
    this.m_PrevButton.gameObject:SetActive(this.m_CurrentRow >= 1)
    this.m_NextButton.gameObject:SetActive(this.m_CurrentRow < this.m_Contents.Length - 1)
end
CChargeGuideCtrl.m_OnCenterPage_CS2LuaHook = function (this, go) 
    do
        local i = 0
        while i < this.m_Contents.Length do
            if this.m_Contents[i] ~= nil and this.m_Contents[i].gameObject == go then
                this.m_CurrentRow = i
                break
            end
            i = i + 1
        end
    end
    this.m_PrevButton.gameObject:SetActive(this.m_CurrentRow >= 1)
    this.m_NextButton.gameObject:SetActive(this.m_CurrentRow < this.m_Contents.Length - 1)
end
