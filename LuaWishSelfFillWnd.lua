local CCurentMoneyCtrl=import "L10.UI.CCurentMoneyCtrl"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local MessageWndManager = import "L10.UI.MessageWndManager"

LuaWishSelfFillWnd = class()
--RegistClassMember(LuaWishSelfFillWnd,"m_MaxTime")

function LuaWishSelfFillWnd:Init()
  local data = LuaWishMgr.CurrentWishData
--  local itemData = CItemMgr.Inst:GetItemTemplate(data.templateId)
--  if not itemData then
--    print('Wrong ItemId:',data.templateId)
--    CUIManager.CloseUI(CLuaUIResources.WishSelfFillWnd)
--    return
--  end
  local cost = data.totalProgress - data.currentProgress
  if cost <= 0 then
    CUIManager.CloseUI(CLuaUIResources.WishSelfFillWnd)
    return
  end
  local textLabel = FindChild(self.transform,"TextLabel"):GetComponent(typeof(UILabel))
  textLabel.text = SafeStringFormat3(LocalString.GetString('你确定消耗%s灵玉来实现该心愿吗？'),cost)

  local costCmp = FindChild(self.transform,"QnCostAndOwnMoney"):GetComponent(typeof(CCurentMoneyCtrl))
  costCmp:SetCost(cost)

  local okButton = FindChild(self.transform,"OkButton").gameObject
  UIEventListener.Get(okButton).onClick = DelegateFactory.VoidDelegate(function(go)
    if costCmp.moneyEnough then
      --function CServerPlayer:Gac2Gas_Wish_AddHelp(TargetPlayerId, TargetPlayerName, wishId, RegionId, TemplateId, LingYu, Visibility, ...)
      local regionId = EShopMallRegion_lua.ELingyuMall
      Gac2Gas.Wish_AddHelp(data.roleId,data.roleName or data.roleId,data.wishId,regionId,data.templateId,cost,0,'','')
      CUIManager.CloseUI(CLuaUIResources.WishSelfFillWnd)
    else
      MessageWndManager.ShowOKCancelMessage(LocalString.GetString("灵玉不足，是否前往充值？"), DelegateFactory.Action(function ()
        CShopMallMgr.ShowChargeWnd()
      end), nil, nil, nil, false)
    end
  end)

  local cancelButton = FindChild(self.transform,"CancelButton").gameObject
  UIEventListener.Get(cancelButton).onClick = DelegateFactory.VoidDelegate(function(go)
    CUIManager.CloseUI(CLuaUIResources.WishSelfFillWnd)
  end)
end
