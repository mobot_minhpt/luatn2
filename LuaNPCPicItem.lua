local DelegateFactory  = import "DelegateFactory"
local UITexture = import "UITexture"
local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local CChatHelper = import "L10.UI.CChatHelper"
local CPersonalSpace_Image = import "L10.Game.CPersonalSpace_Image"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
LuaNPCPicItem = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaNPCPicItem, "pic", "pic", UITexture)
RegistChildComponent(LuaNPCPicItem, "ChannelNameLabel", "ChannelNameLabel", UILabel)
RegistChildComponent(LuaNPCPicItem, "SenderNameLabel", "SenderNameLabel", UILabel)
RegistChildComponent(LuaNPCPicItem, "Portrait", "Portrait", CUITexture)

--@endregion RegistChildComponent end
RegistClassMember(LuaNPCPicItem, "m_Data")
function LuaNPCPicItem:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaNPCPicItem:Init(data)
    self.m_Data = data
    self:InitSenderInfo()
    self:LoadSmallPic()
end
function LuaNPCPicItem:InitSenderInfo()
    self.ChannelNameLabel.text = SafeStringFormat3("[%s]%s[-]",CChatHelper.GetChannelColor24(self.m_Data.channel), self.m_Data.channelName)
    self.SenderNameLabel.text = self.m_Data.senderName;
    self.Portrait:LoadNPCPortrait(self.m_Data.senderPortrait)
end
function LuaNPCPicItem:LoadSmallPic()
    local PicInfo = CreateFromClass(CPersonalSpace_Image)
    PicInfo.thumb = ""
    PicInfo.pic = self.m_Data.text
    local PicTable = {PicInfo}
    CPersonalSpaceMgr.DownLoadPic(self.m_Data.text,self.pic,250,250,DelegateFactory.Action(function ()
        UIEventListener.Get(self.pic.gameObject).onClick = DelegateFactory.VoidDelegate(function()
            LuaPersonalSpaceMgrReal.InitDetailPicPanel(PicTable,1)
        end)
        self.pic:ResizeCollider()
        self.gameObject:GetComponent(typeof(UIWidget)).height = math.max(135 , 75 + self.pic.height)
        g_ScriptEvent:BroadcastInLua("UpdateFriendChatListItemReLayout",self.transform)

    end),false,true)
end
--@region UIEvent

--@endregion UIEvent

