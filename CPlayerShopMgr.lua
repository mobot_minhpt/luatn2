-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CommonDefs = import "L10.Game.CommonDefs"
local CPlayerShopMgr = import "L10.UI.CPlayerShopMgr"
local DelegateFactory = import "DelegateFactory"
local CPlayerShopData=import "L10.UI.CPlayerShopData"
local CPlayerShopNumberInputMgr = import "L10.UI.CPlayerShopNumberInputMgr"
local CItemMgr = import "L10.Game.CItemMgr"

CPlayerShopMgr.m_GetAllFocusTemplate_CS2LuaHook = function (this) 
    local ret = CreateFromClass(MakeGenericClass(HashSet, UInt32))
    if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.PlayProp ~= nil then
        local temp = CClientMainPlayer.Inst.PlayProp:GetPlayerShopTemplateFocus()
        CommonDefs.EnumerableIterate(temp, DelegateFactory.Action_object(function (___value) 
            if CommonDefs.DictContains_LuaCall(this.m_ItemTemplateIdDict, ___value) or 
                CommonDefs.DictContains_LuaCall(this.m_EquipTemplateIdDict, ___value) or
                CommonDefs.DictContains_LuaCall(this.m_FurnitureTemplateIdDict, ___value) then
                CommonDefs.HashSetAdd(ret, typeof(UInt32), ___value)
            end
        end))
    end
    return ret
end

CPlayerShopMgr.m_TakeOutMoney_CS2LuaHook = function (this) 
    local currentMoneyInShop = CPlayerShopData.Main.TotalFund - CPlayerShopData.Main.MinimalFund
    if currentMoneyInShop <= 0 then
        g_MessageMgr:ShowMessage("PLAYERSHOP_NOT_DRAW_MONEY", CPlayerShopData.Main.MinimalFund)
    else
        local shopId = CPlayerShopData.Main.PlayerShopId
        CLuaNumberInputMgr.ShowNumInputBox(0, currentMoneyInShop, 0, function (num) 
            Gac2Gas.GetPlayerShopFund(shopId, num)
        end,LocalString.GetString("请输入需要取出的资金"), -1) 
    end
end

CPlayerShopMgr.m_AddInMoney_CS2LuaHook = function (this) 
    local canAdd = CClientMainPlayer.Inst.Silver
    if canAdd > CPlayerShopMgr.Shop_Money_Max then
        canAdd = CPlayerShopMgr.Shop_Money_Max
    end
    if CPlayerShopData.Main.TotalFund >= CPlayerShopMgr.Shop_Money_Max then
        canAdd = 0
        g_MessageMgr:ShowMessage("PLAYER_SHOP_CANNOT_ADD_FUND_OVER_LIMIT")
        return
    elseif CPlayerShopData.Main.TotalFund > 0 and canAdd + CPlayerShopData.Main.TotalFund > CPlayerShopMgr.Shop_Money_Max then
        canAdd = CPlayerShopMgr.Shop_Money_Max - CPlayerShopData.Main.TotalFund
    end
    local shopId = CPlayerShopData.Main.PlayerShopId
    CLuaNumberInputMgr.ShowNumInputBox(0, canAdd, 0, function (num) 
        Gac2Gas.AddPlayerShopFund(shopId, num)
    end, LocalString.GetString("请输入需要投入的资金"), -1)
end

CPlayerShopMgr.m_BuyItemFromShop_CPlayerShopItemData_CS2LuaHook = function(this, data)
    if data.Item then
        if data.Count >= CPlayerShopMgr.s_MinBuyCount then
            CLuaShopMallMgr.m_PlayerShopNumInputBoxOwnedCount = CItemMgr.Inst:GetItemCount(data.Item.TemplateId)
            CPlayerShopNumberInputMgr.ShowNumInputBox(data.Item.Icon, data.Price, 1, data.Count, 1,DelegateFactory.Action_uint(function (count) 
               this:BuyItemFromShop(data, count);
            end))
        else
            this:BuyItemFromShop(data, 1)
        end
    end
end