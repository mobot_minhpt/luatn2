local DelegateFactory = import "DelegateFactory"
local CButton         = import "L10.UI.CButton"
local CSkillInfoMgr   = import "L10.UI.CSkillInfoMgr"

LuaCuJuEnterWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaCuJuEnterWnd, "score")
RegistClassMember(LuaCuJuEnterWnd, "time")
RegistClassMember(LuaCuJuEnterWnd, "taskForm")
RegistClassMember(LuaCuJuEnterWnd, "level")
RegistClassMember(LuaCuJuEnterWnd, "desc")
RegistClassMember(LuaCuJuEnterWnd, "skillTemplate")
RegistClassMember(LuaCuJuEnterWnd, "skillTable")
RegistClassMember(LuaCuJuEnterWnd, "signButton")
RegistClassMember(LuaCuJuEnterWnd, "matching")

RegistClassMember(LuaCuJuEnterWnd, "bAttend")

function LuaCuJuEnterWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

    self:InitUIComponents()
    self.skillTemplate:SetActive(false)
    self.signButton.gameObject:SetActive(false)
    self.matching:SetActive(false)
    self:InitEventListener()
end

function LuaCuJuEnterWnd:InitUIComponents()
    local anchor = self.transform:Find("Anchor")
    self.score = anchor:Find("Center/xinxi/Score"):GetComponent(typeof(UILabel))
    self.time = anchor:Find("Center/xinxi/Time"):GetComponent(typeof(UILabel))
    self.taskForm = anchor:Find("Center/xinxi/TaskForm"):GetComponent(typeof(UILabel))
    self.level = anchor:Find("Center/xinxi/Level"):GetComponent(typeof(UILabel))
    self.desc = anchor:Find("Center/xinxi/Desc"):GetComponent(typeof(UILabel))
    self.skillTemplate = anchor:Find("Center/xinxi/SkillTemplate").gameObject
    self.skillTable = anchor:Find("Center/xinxi/SkillTable"):GetComponent(typeof(UITable))
    self.signButton = anchor:Find("Center/xinxi/SignButton"):GetComponent(typeof(CButton))
    self.matching = anchor:Find("Center/xinxi/Matching").gameObject
end

function LuaCuJuEnterWnd:InitEventListener()
    local rankButton = self.transform:Find("Anchor/RankButton").gameObject
    local ruleButton = self.transform:Find("Anchor/RuleButton").gameObject

    UIEventListener.Get(rankButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRankButtonClick()
	end)

    UIEventListener.Get(ruleButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRuleButtonClick()
	end)

    UIEventListener.Get(self.signButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSignButtonClick()
	end)
end

function LuaCuJuEnterWnd:OnEnable()
    g_ScriptEvent:AddListener("QueryCuJuCrossSignUpResult", self, "OnQueryCuJuCrossSignUpResult")
end

function LuaCuJuEnterWnd:OnDisable()
    g_ScriptEvent:RemoveListener("QueryCuJuCrossSignUpResult", self, "OnQueryCuJuCrossSignUpResult")
end

function LuaCuJuEnterWnd:OnQueryCuJuCrossSignUpResult(bAttend, attendTimes, rewardTimes)
    self.bAttend = bAttend

    self.signButton.gameObject:SetActive(true)
    local sprite = self.signButton.transform:GetComponent(typeof(UISprite))
    self.matching:SetActive(bAttend)
    if bAttend then
        self.signButton.Text = LocalString.GetString("取消匹配")
        sprite.spriteName = g_sprites.GreenHandGiftWnd_BlueButtonName
        self.signButton.label.color = NGUIText.ParseColor24("0E3254", 0)
    else
        self.signButton.Text = LocalString.GetString("报名参加")
        sprite.spriteName = g_sprites.GreenHandGiftWnd_OrangeButtonName
        self.signButton.label.color = NGUIText.ParseColor24("351B01", 0)
    end
end


function LuaCuJuEnterWnd:Init()
    local data = CuJu_Setting.GetData()
    local count = data.ClientSkill.Length
    Extensions.RemoveAllChildren(self.skillTable.transform)
    for i = 0, count - 1 do
        local child = NGUITools.AddChild(self.skillTable.gameObject, self.skillTemplate)
        child:SetActive(true)

        local skillId = data.ClientSkill[i]
        local skillData = Skill_AllSkills.GetData(data.ClientSkill[i])
        local icon = child.transform:Find("Mask/Icon"):GetComponent(typeof(CUITexture))
        icon:LoadSkillIcon(skillData.SkillIcon)
        child.transform:Find("Name"):GetComponent(typeof(UILabel)).text = skillData.Name

        UIEventListener.Get(child).onClick = DelegateFactory.VoidDelegate(function (go)
            CSkillInfoMgr.ShowSkillInfoWnd(skillId, true, 0, 0, nil)
        end)
    end
    self.skillTable:Reposition()

    self.time.text = data.ActivityTime
    self.taskForm.text = data.TaskForm
    self.level.text = SafeStringFormat3(LocalString.GetString("%d级"), data.SignUpGradeLimit)
    self.desc.text = data.TaskDesc

    Gac2Gas.QueryCuJuCrossSignUpInfo()

    local todayScore = CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eWorldCup2022JZLY_DailyScore)
    local maxTodayScore = data.MaxTodayScore
    self.score.text = SafeStringFormat3("%d/%d", todayScore, maxTodayScore)
    self.score.color = NGUIText.ParseColor24(todayScore >= maxTodayScore and "FF5050" or "FFFFFF", 0)
end

--@region UIEvent

function LuaCuJuEnterWnd:OnRankButtonClick()
    CUIManager.ShowUI(CLuaUIResources.CuJuRankWnd)
end

function LuaCuJuEnterWnd:OnRuleButtonClick()
    g_MessageMgr:ShowMessage("CUJU_RULE")
end

function LuaCuJuEnterWnd:OnSignButtonClick()
    if self.bAttend then
        Gac2Gas.RequestCancelSignUpForCrossCuJu()
    else
        Gac2Gas.RequestSignUpForCrossCuJu()
    end
end

--@endregion UIEvent
