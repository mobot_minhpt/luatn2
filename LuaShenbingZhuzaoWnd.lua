local LuaGameObject=import "LuaGameObject"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CItemMgr = import "L10.Game.CItemMgr"
local CFreightEquipSubmitMgr = import "L10.UI.CFreightEquipSubmitMgr"
local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"
local Item_Item = import "L10.Game.Item_Item"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local AlignType = import "L10.UI.CTooltip+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType2 = import "L10.UI.CItemInfoMgr+AlignType"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local UIEventListener = import "UIEventListener"
local DelegateFactory = import "DelegateFactory"
local Screen = import "UnityEngine.Screen"
local Extensions = import "Extensions"
local CYuanbaoMarketMgr = import "L10.UI.CYuanbaoMarketMgr"
local MessageWndManager = import "L10.UI.MessageWndManager"
local Collider = import "UnityEngine.Collider"
local CUICommonDef = import "L10.UI.CUICommonDef"
local TweenAlpha = import "TweenAlpha"
local TweenPosition = import "TweenPosition"
local TweenScale = import "TweenScale"
local CRenderObject = import "L10.Engine.CRenderObject"
local LayerDefine = import "L10.Engine.LayerDefine"
local DefaultItemActionDataSource = import "L10.UI.DefaultItemActionDataSource"
local CIndirectUIResources = import "L10.UI.CIndirectUIResources"

CLuaShenbingZhuzaoWnd = class()
RegistClassMember(CLuaShenbingZhuzaoWnd, "m_ShowRootObj")
RegistClassMember(CLuaShenbingZhuzaoWnd, "m_PrepareRootObj")
RegistClassMember(CLuaShenbingZhuzaoWnd, "m_EquipTexture")
RegistClassMember(CLuaShenbingZhuzaoWnd, "m_EquipQualitySprite")
RegistClassMember(CLuaShenbingZhuzaoWnd, "m_EquipAddObj")
RegistClassMember(CLuaShenbingZhuzaoWnd, "m_ZhuzaoTrans")
RegistClassMember(CLuaShenbingZhuzaoWnd, "m_Item2CntLabel")
RegistClassMember(CLuaShenbingZhuzaoWnd, "m_Item2GotObj")
RegistClassMember(CLuaShenbingZhuzaoWnd, "m_Item1GotObj")
RegistClassMember(CLuaShenbingZhuzaoWnd, "m_Item1CntLabel")
RegistClassMember(CLuaShenbingZhuzaoWnd, "m_Itme1Texture")
RegistClassMember(CLuaShenbingZhuzaoWnd, "m_Item2TemplateId")
RegistClassMember(CLuaShenbingZhuzaoWnd, "m_Item1TemplateId")
RegistClassMember(CLuaShenbingZhuzaoWnd, "m_Item2NeedCount")

RegistClassMember(CLuaShenbingZhuzaoWnd, "m_EquipPlace")
RegistClassMember(CLuaShenbingZhuzaoWnd, "m_EquipPos")
RegistClassMember(CLuaShenbingZhuzaoWnd, "m_EquipId")
RegistClassMember(CLuaShenbingZhuzaoWnd, "m_EquipName")
RegistClassMember(CLuaShenbingZhuzaoWnd, "m_IsAttackEquip")
RegistClassMember(CLuaShenbingZhuzaoWnd, "m_Item1Place")
RegistClassMember(CLuaShenbingZhuzaoWnd, "m_Item1Pos")
RegistClassMember(CLuaShenbingZhuzaoWnd, "m_Item1Id")

RegistClassMember(CLuaShenbingZhuzaoWnd, "m_NameLabel")
RegistClassMember(CLuaShenbingZhuzaoWnd, "m_ModelTexture")
RegistClassMember(CLuaShenbingZhuzaoWnd, "m_CurrentRotation")
RegistClassMember(CLuaShenbingZhuzaoWnd, "m_ModelCameraName")
RegistClassMember(CLuaShenbingZhuzaoWnd, "m_ImageTexture")
RegistClassMember(CLuaShenbingZhuzaoWnd, "m_Fx")

RegistClassMember(CLuaShenbingZhuzaoWnd, "m_TweenTable")
RegistClassMember(CLuaShenbingZhuzaoWnd, "m_CanFloat")
RegistClassMember(CLuaShenbingZhuzaoWnd, "m_ModelMoveTime")
RegistClassMember(CLuaShenbingZhuzaoWnd, "m_ModelInitPosition")

function CLuaShenbingZhuzaoWnd:DragModel(delta)
	local deltaVal = -delta.x / Screen.width * 360
	self.m_CurrentRotation = self.m_CurrentRotation + deltaVal
	CUIManager.SetModelRotation(self.m_ModelCameraName, self.m_CurrentRotation)
end

function CLuaShenbingZhuzaoWnd:Init()
	self.m_TweenTable = {}
	self.m_CanFloat = false
	self.m_ModelMoveTime = 0

	local prepareRootTrans = LuaGameObject.GetChildNoGC(self.transform, "PrepareRoot").transform
	prepareRootTrans.gameObject:SetActive(true)
	self.m_PrepareRootObj = prepareRootTrans.gameObject
	self.m_ShowRootObj = LuaGameObject.GetChildNoGC(self.transform, "ShowRoot").gameObject
	self.m_NameLabel = LuaGameObject.GetChildNoGC(self.m_ShowRootObj.transform, "Name").label
	self.m_ModelTexture = LuaGameObject.GetChildNoGC(self.m_ShowRootObj.transform, "ModelTexture").texture
	self.m_ImageTexture = LuaGameObject.GetChildNoGC(self.m_ShowRootObj.transform, "ImageTexture").cTexture
	self.m_CurrentRotation = 0
	self.m_ModelCameraName = "__ShenbingShowEquip__"
	UIEventListener.Get(self.m_ModelTexture.gameObject).onDrag = LuaUtils.VectorDelegate(function(go, delta)
		self:DragModel(delta)
	end)
	self.m_ShowRootObj:SetActive(false)
	local addEquipAction = function ( ... )
		if nil == CClientMainPlayer.Inst then
			return
		end
		local dict = CreateFromClass(MakeGenericClass(Dictionary, Int32, MakeGenericClass(List, cs_string)))
		local list = CreateFromClass(MakeGenericClass(List, cs_string))
		-- find
		local placeTable = {EnumItemPlace.Body, EnumItemPlace.Bag}
		for _, v in pairs(placeTable) do
			local size = CClientMainPlayer.Inst.ItemProp:GetPlaceSize(v)
			if size > 0 then
				for i = 1, size do
					local id = CClientMainPlayer.Inst.ItemProp:GetItemAt(v, i)
					local equip = CItemMgr.Inst:GetById(id)
					if equip ~= nil and equip.IsEquip and not equip.Equip.IsShenBing then
						local data = EquipmentTemplate_Equip.GetData(equip.TemplateId)
						-- 项链只要比蓝好就可以铸造
						if data.ShenBingType > 0 and data.Grade >= 109 and ShenBing_OpenEquip.GetData(data.Type) and ShenBing_OpenEquip.GetData(data.Type).Status == 0 and (equip.IsPrecious or (data.Type == 10 and equip.Equip.IsBlueOrBetterEquipExceptTalisman)) then
							if id == self.m_EquipId then
								CommonDefs.ListInsert(list, 0, typeof(cs_string), id)
							else
								CommonDefs.ListAdd(list, typeof(cs_string), id)
							end
						end
					end
				end
			end
		end
		CommonDefs.DictAdd(dict, TypeOfInt32, 0, typeof(MakeGenericClass(List, cs_string)), list)

		if list.Count <= 0 then
			CFreightEquipSubmitMgr.OpenEquipmentChooseWnd("ShenbingZhuzao", dict, LocalString.GetString("请选择底板装备"), 0, false, LocalString.GetString("选择"), LocalString.GetString("前往购买"), DelegateFactory.Action(function ( ... )
					CYuanbaoMarketMgr.ShowPlayerShopBuySection(1, 1)
				end), LocalString.GetString("没有可用于铸造神兵的珍品装备"))
		else
			CFreightEquipSubmitMgr.OpenEquipmentChooseWnd("ShenbingZhuzao", dict, LocalString.GetString("请选择底板装备"), 0, false, LocalString.GetString("选择"), LocalString.GetString("前往购买"), nil, LocalString.GetString("没有可用于铸造神兵的珍品装备"))
		end
	end
	local equipTrans = LuaGameObject.GetChildNoGC(prepareRootTrans, "Equip").transform
	self.m_EquipQualitySprite = LuaGameObject.GetChildNoGC(equipTrans, "QualitySprite").sprite
	self.m_EquipQualitySprite.spriteName = ""
	local equipTex = LuaGameObject.GetChildNoGC(equipTrans, "Texture")
	UIEventListener.Get(equipTex.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
		addEquipAction(go)
	end)
	self.m_EquipTexture = equipTex.cTexture
	local addObj = LuaGameObject.GetChildNoGC(equipTrans, "Add")
	UIEventListener.Get(addObj.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
		addEquipAction(go)
	end)

	self.m_EquipAddObj = addObj.gameObject
	local zhuzaoBtn = LuaGameObject.GetChildNoGC(prepareRootTrans, "ZhuZaoBtn")
	UIEventListener.Get(zhuzaoBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
		MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("ShenBing_ZhuZao_Confirm_Msg", self.m_EquipName), DelegateFactory.Action(function ( ... )
			Gac2Gas.RequestMakeShenBing(self.m_Item1Place, self.m_Item1Pos, self.m_Item1Id, self.m_EquipPlace, self.m_EquipPos, self.m_EquipId)
        end), nil, nil, nil, false)
	end)
	self.m_ZhuzaoTrans = zhuzaoBtn.transform
	self:SetZhuZaoBtnEnable(false)
	local g = LuaGameObject.GetChildNoGC(self.transform, "TipBtn").gameObject
	UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
		g_MessageMgr:ShowMessage("ShenBing_ZhuZao_Tip")
	end)

	--Item
	local gotItemAction = function ( go )
		if go == self.m_Item1GotObj then
			--CItemAccessListMgr.Inst:ShowItemAccessInfo(self.m_Item1TemplateId, false, go.transform, AlignType.Right)
			local default = DefaultItemActionDataSource.Create(1, {function ( ... )
				CUIManager.CloseUI(CIndirectUIResources.ItemInfoWnd)
				CItemAccessListMgr.Inst:ShowItemAccessInfo(self.m_Item1TemplateId, false, go.transform, AlignType.Right)
			end}, {LocalString.GetString("获取")})
			CItemInfoMgr.ShowLinkItemTemplateInfo(self.m_Item1TemplateId, false, default, AlignType2.Right, 0, 0, 0, 0)
		elseif go == self.m_Item2GotObj then
			CItemAccessListMgr.Inst:ShowItemAccessInfo(self.m_Item2TemplateId, false, go.transform, AlignType.Right)
		end
	end

	local showItemInfoAction = function ( go )
		if go == self.m_Itme1Texture.gameObject then
			if self.m_Item1TemplateId then
				CItemInfoMgr.ShowLinkItemTemplateInfo(self.m_Item1TemplateId, false, nil, AlignType2.ScreenLeft, 0, 0, 0, 0)
			end
		else
			CItemInfoMgr.ShowLinkItemTemplateInfo(self.m_Item2TemplateId, false, nil, AlignType2.ScreenRight, 0, 0, 0, 0)
		end
	end

	local item2RootTrans = LuaGameObject.GetChildNoGC(prepareRootTrans, "Item1").transform
	self.m_Item2GotObj = LuaGameObject.GetChildNoGC(item2RootTrans, "Got").gameObject
	UIEventListener.Get(self.m_Item2GotObj).onClick = DelegateFactory.VoidDelegate(function(go)
		gotItemAction(go)
	end)
	self.m_Item2CntLabel = LuaGameObject.GetChildNoGC(item2RootTrans, "Label").label
	self.m_Item2TemplateId = ShenBing_Setting.GetData().JqzItemId
	local item2Tex = LuaGameObject.GetChildNoGC(item2RootTrans, "Texture")
	UIEventListener.Get(item2Tex.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
		showItemInfoAction(go)
	end)

	local item2Data = Item_Item.GetData(self.m_Item2TemplateId)
	if item2Data ~= nil then
		item2Tex.cTexture:LoadMaterial(item2Data.Icon)
	end
	self.m_Item2GotObj:SetActive(false)
	self.m_Item2CntLabel.text = ""

	local item1RootTrans = LuaGameObject.GetChildNoGC(prepareRootTrans, "Item").transform
	self.m_Item1GotObj = LuaGameObject.GetChildNoGC(item1RootTrans, "Got").gameObject
	UIEventListener.Get(self.m_Item1GotObj).onClick = DelegateFactory.VoidDelegate(function(go)
		gotItemAction(go)
	end)

	self.m_Item1CntLabel = LuaGameObject.GetChildNoGC(item1RootTrans, "Label").label
	local item1Tex = LuaGameObject.GetChildNoGC(item1RootTrans, "Texture")
	UIEventListener.Get(item1Tex.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
		showItemInfoAction(go)
	end)

	self.m_Itme1Texture = item1Tex.cTexture
	self.m_Item1GotObj:SetActive(false)
	self.m_Item1CntLabel.text = ""

	-- init tween
	local tbl = {item1RootTrans.gameObject, item2RootTrans.gameObject, equipTrans.gameObject}
	for k, v in pairs(tbl) do
		local tbl2 = {typeof(TweenPosition), typeof(TweenScale), typeof(TweenAlpha)}
		for k1, v1 in pairs(tbl2) do
			local com = CommonDefs.GetComponent_GameObject_Type(v, v1)
			if com then
				table.insert(self.m_TweenTable, com)
			end
		end
	end

	self.m_Fx = LuaGameObject.GetChildNoGC(self.transform, "Fx").uiFx

end

function CLuaShenbingZhuzaoWnd:ShowFx()
	CommonDefs.AddEventDelegate(self.m_TweenTable[1].onFinished, DelegateFactory.Action(function ( ... )
		self.m_Fx:LoadFx("Fx/UI/Prefab/UI_zhuzaohecheng.prefab")
		self.m_PrepareRootObj:SetActive(false)
		self.m_ShowRootObj:SetActive(true)
	end))
	for k, v in pairs(self.m_TweenTable) do
		v.enabled = true
	end
end

function CLuaShenbingZhuzaoWnd:SetZhuZaoBtnEnable(enabled)
	 Extensions.SetLocalPositionZ(self.m_ZhuzaoTrans, enabled and 0 or -1)
	 local col = self.m_ZhuzaoTrans.gameObject:GetComponent(typeof(Collider))
	 if col then
	 	col.enabled = enabled
	 end
end

function CLuaShenbingZhuzaoWnd:Update()
	if not self.m_CanFloat then
		return
	end
	--self:DragModel(Vector2(3, 0))
	self.m_ModelMoveTime  = self.m_ModelMoveTime + Time.deltaTime
	self.m_ModelInitPosition.y = 0.05 * math.sin(self.m_ModelMoveTime * 1.7)
	CUIManager.SetModelPosition(self.m_ModelCameraName, self.m_ModelInitPosition)
end

function CLuaShenbingZhuzaoWnd:InitAppearance( ... )
	local equip = CItemMgr.Inst:GetById(self.m_EquipId)
	if not equip or not equip.Equip.IsShenBing then
		return
	end

	local data = EquipmentTemplate_Equip.GetData(equip.TemplateId)
	if not data then
		return
	end

	self.m_NameLabel.text = equip.Equip.DisplayName
	self.m_NameLabel.color = equip.Equip.DisplayColor
	self.m_ModelTexture.gameObject:SetActive(self.m_IsAttackEquip)
	self.m_ImageTexture.gameObject:SetActive(not self.m_IsAttackEquip)
	if self.m_IsAttackEquip then
		local interface = LuaDefaultModelTextureLoader.Create(function (ro)
			self:LoadModel(ro)
		end)

		local tx = CUIManager.CreateModelTexture(self.m_ModelCameraName, interface, self.m_CurrentRotation, 0.05, 0, ShenBing_Rotation.GetData(data.ShenBingType).Scale, false, true, 1.0)
		self.m_ModelTexture.mainTexture = tx
		self.m_ModelInitPosition = Vector3(0.05, 0, ShenBing_Rotation.GetData(data.ShenBingType).Scale)
		CUIManager.SetModelPosition(self.m_ModelCameraName, self.m_ModelInitPosition)
	else
		self.m_ImageTexture:LoadMaterial(equip.Equip.BigIcon)
		local height = equip.Equip.UseSquareBigIcon and 256 or 384
		local width = equip.Equip.UseSquareBigIcon and 256 or 194
		self.m_ImageTexture.uiTexture.height = height
		self.m_ImageTexture.uiTexture.width = width
	end
end

function CLuaShenbingZhuzaoWnd:InitRO(ro, angle, pos, shenbingType)
	--if shenbingType == 17 or shenbingType == 20 or shenbingType == 21 then
	--	for i = 0, ro.m_Renderers.Count - 1 do
	--		local mats = ro.m_Renderers[i].materials
	--		for j = 0, mats.Length - 1 do
	--			mats[j]:DisableKeyword("SKIN_ON")
	--		end
	--	end
	--end

	ro.transform.localPosition = pos
	ro.transform.localEulerAngles = angle
end

function CLuaShenbingZhuzaoWnd:LoadSkinModel(ro, shenbingType, firstId, secondId, oldPrefab)
	local resName = shenbingType == 17 and "gnpc_nys" or (shenbingType == 22 and "bnpc_yingling" or "gnpc_huashi")
	local skeletonResPath = SafeStringFormat3("Character/Player_New/%s/Prefab/%s.prefab", resName, resName)
	ro:LoadMain(skeletonResPath, nil, false, false)
	if firstId > 0 then
		local pattern = "Item/Weapon_New/Prefab/%s_new_%s.prefab"
		if shenbingType == 22 then
			-- 笛子未拆分成两部分
			pattern = "Item/Weapon_New/Prefab/%s_nan.prefab"
			if ShenBing_WeaponModel.GetData(firstId) then
				ro:AddChild(SafeStringFormat3(pattern, ShenBing_WeaponModel.GetData(firstId).Prefab), "weapon01", "weapon01")
			end
		else
			if ShenBing_WeaponModel.GetData(firstId) then
				ro:AddChild(SafeStringFormat3(pattern, ShenBing_WeaponModel.GetData(firstId).Prefab, "wp1_02"), "weapon01", "weapon01")
			end
			if ShenBing_WeaponModel.GetData(secondId) then
				ro:AddChild(SafeStringFormat3(pattern, ShenBing_WeaponModel.GetData(secondId).Prefab, "wp2_02"), "weapon01_01", "weapon01")
			end
		end
	else
		-- 笛子
		if shenbingType == 22 then
			ro:AddChild(oldPrefab, "weapon01", "weapon01")
		else
			ro:AddChild(string.gsub(oldPrefab, "%.prefab", "_new.prefab"), "weapon01", "weapon01")
		end
	end
	if shenbingType == 17 then
		ro:DoAni("stand04", true, 0, 1.0, 0, true, 1.0)
	end
end

function CLuaShenbingZhuzaoWnd:LoadModel(ro)
	local equip = CItemMgr.Inst:GetById(self.m_EquipId)
	local data = EquipmentTemplate_Equip.GetData(equip.TemplateId)
	if not data then
		return
	end

	local a, b, c = string.match(ShenBing_Rotation.GetData(data.ShenBingType).OriAngle, "([^;]+);([^;]+);([^;]+)")
	local angle = Vector3(tonumber(a), tonumber(b), tonumber(c))
	local e, f, g = string.match(ShenBing_Rotation.GetData(data.ShenBingType).OriPos, "([^;]+);([^;]+);([^;]+)")
	local pos = Vector3(tonumber(e), tonumber(f), tonumber(g))

	local newRO = CRenderObject.CreateRenderObject(ro.gameObject, "newRenderObject")
	newRO.NeedUpdateAABB = true
	newRO.Layer = LayerDefine.ModelForNGUI_3D
	newRO.EnableDelayLoad = false

	-- Skin weapon
	if data.ShenBingType == 17 or data.ShenBingType == 20 or data.ShenBingType == 21 or data.ShenBingType == 22 then
		self:LoadSkinModel(newRO, data.ShenBingType, equip.Equip.HolyFirstModelId, equip.Equip.HolySecondModelId, data.Prefab)
	else
		local firstId = equip.Equip.HolyFirstModelId
		if firstId > 0 then
			local cfg1 = ShenBing_WeaponModel.GetData(firstId)
			-- 双手偃甲
			if data.ShenBingType == 19 then
				newRO:LoadMain(SafeStringFormat3("Item/AniWeapon/%s/Prefab/%s_02.prefab", cfg1.Prefab, cfg1.Prefab))
			else
				local pattern = "Item/Weapon_New/Prefab/%s_%s.prefab"
				if cfg1 then
					newRO:LoadMain(SafeStringFormat3(pattern, cfg1.Prefab, "wp1_02"), nil, false, false)
				end
				local secondId = equip.Equip.HolySecondModelId
				local cfg2 = ShenBing_WeaponModel.GetData(secondId)
				if cfg2 then
					newRO:AddChild(SafeStringFormat3(pattern, cfg2.Prefab, "wp2_02"), "wp2", "wp2")
				end
			end
		else
			newRO:LoadMain(data.Prefab, nil, false, false)
		end
	end

	newRO:AddLoadAllFinishedListener(DelegateFactory.Action_CRenderObject(function (ro)
		self:InitRO(ro, angle, pos, data.ShenBingType)
		self.m_CanFloat = true
	end))
end

function CLuaShenbingZhuzaoWnd:AddEquip(args)
	if args[1] ~= "ShenbingZhuzao" then
		return
	end
	local equip = CItemMgr.Inst:GetById(args[0])
	if equip ~= nil and equip.IsEquip then
		self.m_EquipId = args[0]
		self.m_EquipQualitySprite.spriteName = CUICommonDef.GetItemCellBorder(equip.Equip.QualityType)
		self.m_EquipTexture:LoadMaterial(equip.Icon)
		self.m_EquipAddObj:SetActive(false)
		local itemData = EquipmentTemplate_Equip.GetData(equip.Equip.TemplateId)
		if itemData ~= nil then
			self:InitItem(itemData.Type, itemData.Grade)
		end
		local pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Body, args[0])
		if pos > 0 then
			self.m_EquipPlace = EnumItemPlace.Body
			self.m_EquipPos = pos
		else
			self.m_EquipPlace = EnumItemPlace.Bag
			self.m_EquipPos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, args[0])
		end
		self.m_IsAttackEquip = itemData.Type == 1
		self.m_EquipName = equip.ColoredName
	end
end

function CLuaShenbingZhuzaoWnd:InitItem(typee, grade)
	local id = 0
	ShenBing_Make.Foreach(function(k, v)
		if v.EquipType == typee and grade >= v.MinOriEquipLevel and grade <= v.MaxOriEquipLevel then
			id = v.ID
		end
	end)
	if id <= 0 then
		return
	end

	self.m_Item1TemplateId = ShenBing_Make.GetData(id).BlueprintID
	self.m_Item2NeedCount = ShenBing_Make.GetData(id).JqzCount
	local item1Data = Item_Item.GetData(self.m_Item1TemplateId)
	if item1Data ~= nil then
		self.m_Itme1Texture:LoadMaterial(item1Data.Icon)
	end
	self:ResetItem()
end

function CLuaShenbingZhuzaoWnd:ResetItem()
	if not self.m_EquipId then
		return
	end
	local cnt = CItemMgr.Inst:GetItemCount(self.m_Item1TemplateId)
	self.m_Item1CntLabel.text = cnt.."/1"
	self.m_Item1GotObj:SetActive(cnt < 1)

	local cnt2 = CItemMgr.Inst:GetItemCount(self.m_Item2TemplateId)
	self.m_Item2CntLabel.text = cnt2.."/"..self.m_Item2NeedCount
	self.m_Item2GotObj:SetActive(cnt2 < self.m_Item2NeedCount)
	self:SetZhuZaoBtnEnable(cnt >= 1 and cnt2 >= self.m_Item2NeedCount)
	if cnt >= 1 then
		local found, pos, id = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, self.m_Item1TemplateId)
		self.m_Item1Place = EnumItemPlace.Bag
		self.m_Item1Pos = pos
		self.m_Item1Id = id
	end
end

function CLuaShenbingZhuzaoWnd:SendItem(args)
	self:ResetItem()
end

function CLuaShenbingZhuzaoWnd:RequestMakeShenBingSuccess( ... )
	--self.m_ShowRootObj:SetActive(true)
	--self.m_PrepareRootObj:SetActive(false)
	self:InitAppearance()
	self:ShowFx()
	self.m_ZhuzaoTrans.gameObject:SetActive(false)
end

function CLuaShenbingZhuzaoWnd:OnEnable()
	g_ScriptEvent:AddListener("OnFreightSubmitEquipSelected", self, "AddEquip")
	g_ScriptEvent:AddListener("SendItem", self, "SendItem")
	g_ScriptEvent:AddListener("SetItemAt", self, "SendItem")
	g_ScriptEvent:AddListener("RequestMakeShenBingSuccess", self, "RequestMakeShenBingSuccess")
end

function CLuaShenbingZhuzaoWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnFreightSubmitEquipSelected", self, "AddEquip")
	g_ScriptEvent:RemoveListener("SendItem", self, "SendItem")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "SendItem")
	g_ScriptEvent:RemoveListener("RequestMakeShenBingSuccess", self, "RequestMakeShenBingSuccess")
end

function CLuaShenbingZhuzaoWnd:OnDestroy()
	CUIManager.DestroyModelTexture(self.m_ModelCameraName)
end

return CLuaShenbingZhuzaoWnd
