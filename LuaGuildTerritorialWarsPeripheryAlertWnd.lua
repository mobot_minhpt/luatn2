local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CGuideMgr = import "L10.Game.Guide.CGuideMgr"
local COpenEntryMgr = import "L10.Game.COpenEntryMgr"

LuaGuildTerritorialWarsPeripheryAlertWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaGuildTerritorialWarsPeripheryAlertWnd, "Button", "Button", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsPeripheryAlertWnd, "Label", "Label", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsPeripheryAlertWnd, "MapBtn", "MapBtn", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsPeripheryAlertWnd, "Grid", "Grid", UIGrid)

--@endregion RegistChildComponent end

function LuaGuildTerritorialWarsPeripheryAlertWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.Button.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnButtonClick()
	end)

	UIEventListener.Get(self.MapBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnMapBtnClick()
	end)
    --@endregion EventBind end
end

function LuaGuildTerritorialWarsPeripheryAlertWnd:Init()
	self:Show()
end

function LuaGuildTerritorialWarsPeripheryAlertWnd:Show()
	self.Label.text = SafeStringFormat3(LocalString.GetString("余%d"),LuaGuildTerritorialWarsMgr.m_MonsterCount)
	self.Button.gameObject:SetActive(LuaGuildTerritorialWarsMgr.m_MonsterCount > 0)
	self.Grid:Reposition()
	CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.GuildTerritorialWarPeripheryPlay)
end

function LuaGuildTerritorialWarsPeripheryAlertWnd:OnEnable()
	g_ScriptEvent:AddListener("SendGTWRelatedPlayMonsterCount", self, "Show")
	g_ScriptEvent:AddListener("MainPlayerCreated",self,"OnMainPlayerCreated")
end

function LuaGuildTerritorialWarsPeripheryAlertWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SendGTWRelatedPlayMonsterCount", self, "Show")
	g_ScriptEvent:RemoveListener("MainPlayerCreated",self,"OnMainPlayerCreated")
end

function LuaGuildTerritorialWarsPeripheryAlertWnd:OnMainPlayerCreated()
	if LuaGuildTerritorialWarsMgr.EnableGuide_GuildTerritorialWarPeripheryPlay2 and COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.GuildTerritorialWarPeripheryPlay) then
		CGuideMgr.Inst:StartGuide(EnumGuideKey.GuildTerritorialWarPeripheryPlay2,0)
		LuaGuildTerritorialWarsMgr.EnableGuide_GuildTerritorialWarPeripheryPlay2 = false
	end
end
--@region UIEvent

function LuaGuildTerritorialWarsPeripheryAlertWnd:OnButtonClick()
	Gac2Gas.RequestGTWRelatedPlayMonsterSceneInfo()
end

function LuaGuildTerritorialWarsPeripheryAlertWnd:OnMapBtnClick()
	Gac2Gas.RequestTerritoryWarMapOverview()
end
--@endregion UIEvent


function LuaGuildTerritorialWarsPeripheryAlertWnd:GetGuideGo(methodName) 

    if methodName == "GetGuildTerritorialWarsPlayViewMapBtn" then
        return self.MapBtn
    end
    return nil
end