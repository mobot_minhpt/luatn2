-- Auto Generated!!
local CExpertTeamMgr = import "L10.Game.CExpertTeamMgr"
local CExpertTeamPersonalInfoWnd = import "L10.UI.CExpertTeamPersonalInfoWnd"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local CUITexture = import "L10.UI.CUITexture"
local DelegateFactory = import "DelegateFactory"
local Extensions = import "Extensions"
local GameSetting_Common_Wapper = import "L10.Game.GameSetting_Common_Wapper"
local LocalString = import "LocalString"
local NGUITools = import "NGUITools"
local Title_Title = import "L10.Game.Title_Title"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
CExpertTeamPersonalInfoWnd.m_Init_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        this:Close()
    end)

    this.templateNode:SetActive(false)
    local title = Title_Title.GetData(CExpertTeamMgr.Inst.SavePersonalInfo.titleId)
    if title ~= nil then
        CommonDefs.GetComponent_Component_Type(this.infoNode.transform:Find("text1/num"), typeof(UILabel)).text = title.Name
    else
        CommonDefs.GetComponent_Component_Type(this.infoNode.transform:Find("text1/num"), typeof(UILabel)).text = LocalString.GetString("无法查看")
    end
    if CExpertTeamMgr.Inst.SavePersonalInfo.score < 0 then
        CommonDefs.GetComponent_Component_Type(this.infoNode.transform:Find("text2/num"), typeof(UILabel)).text = LocalString.GetString("无法查看")
    else
        CommonDefs.GetComponent_Component_Type(this.infoNode.transform:Find("text2/num"), typeof(UILabel)).text = tostring(CExpertTeamMgr.Inst.SavePersonalInfo.score)
    end

    CommonDefs.GetComponent_Component_Type(this.infoNode.transform:Find("text3/num"), typeof(UILabel)).text = tostring(CExpertTeamMgr.Inst.SavePersonalInfo.adoptNum)
    CommonDefs.GetComponent_Component_Type(this.infoNode.transform:Find("text4/num"), typeof(UILabel)).text = tostring(CExpertTeamMgr.Inst.SavePersonalInfo.quesNum)
    CommonDefs.GetComponent_Component_Type(this.infoNode.transform:Find("text5/num"), typeof(UILabel)).text = tostring(CExpertTeamMgr.Inst.SavePersonalInfo.answerNum)
    CommonDefs.GetComponent_Component_Type(this.infoNode.transform:Find("text6/num"), typeof(UILabel)).text = tostring(CExpertTeamMgr.Inst.SavePersonalInfo.prosNum)
    CommonDefs.GetComponent_Component_Type(this.infoNode.transform:Find("text7/num"), typeof(UILabel)).text = CExpertTeamMgr.Inst.SavePersonalInfo.roleName
    --if (CExpertTeamMgr.Inst.SavePersonalInfo.roleId == 101)
    --{
    --    //iconNode.GetComponent<CUITexture>().LoadNPCPortrait(CUICommonDef.GetPortraitName(cls, gender));
    --    infoNode.transform.FindChild("icon").GetComponent<CUITexture>().LoadNPCPortrait(GameSetting_Common_Wapper.Inst.JingLingPortrait);
    --}
    --else
    --{
    --    infoNode.transform.FindChild("icon").GetComponent<CUITexture>().LoadNPCPortrait(CUICommonDef.GetPortraitName(CExpertTeamMgr.Inst.SavePersonalInfo.clazz, CExpertTeamMgr.Inst.SavePersonalInfo.gender));
    --}
    Extensions.RemoveAllChildren(this.table.transform)
    CExpertTeamMgr.Inst:SetNormalPlayerIcon(CExpertTeamMgr.Inst.SavePersonalInfo.roleId, (CExpertTeamMgr.Inst.SavePersonalInfo.clazz .. ";") .. CExpertTeamMgr.Inst.SavePersonalInfo.gender, this.infoNode.transform:Find("icon").gameObject, this.infoNode.transform:Find("icon/button").gameObject, "", 0, 0)

    if CExpertTeamMgr.Inst.SavePersonalInfo.answers ~= nil and CExpertTeamMgr.Inst.SavePersonalInfo.answers.Count > 0 then
        do
            local i = 0
            while i < CExpertTeamMgr.Inst.SavePersonalInfo.answers.Count do
                local ainfo = CExpertTeamMgr.Inst.SavePersonalInfo.answers[i]
                local node = NGUITools.AddChild(this.table.gameObject, this.templateNode)
                node:SetActive(true)
                local iconNode = node.transform:Find("icon").gameObject
                if ainfo.qRoleId == 101 then
                    CommonDefs.GetComponent_GameObject_Type(iconNode, typeof(CUITexture)):LoadNPCPortrait(GameSetting_Common_Wapper.Inst.JingLingPortrait, false)
                else
                    --string[] roleinfo = ainfo.qAvatar.Split(';');
                    --if (roleinfo.Length == 2)
                    --{
                    --    uint cls = uint.Parse(roleinfo[0]);
                    --    uint gender = uint.Parse(roleinfo[1]);
                    --    iconNode.GetComponent<CUITexture>().LoadNPCPortrait(CUICommonDef.GetPortraitName(cls, gender));
                    --}
                    CExpertTeamMgr.Inst:SetNormalPlayerIcon(math.floor(ainfo.qRoleId), ainfo.qAvatar, iconNode, iconNode.transform:Find("button").gameObject, "", 0, 0)
                end

                CommonDefs.GetComponent_Component_Type(node.transform:Find("detail"), typeof(UILabel)).text = ainfo.content
                CommonDefs.GetComponent_Component_Type(node.transform:Find("answernum/num"), typeof(UILabel)).text = tostring(ainfo.answerNumber)
                CExpertTeamMgr.SetUILabelText(CommonDefs.GetComponent_Component_Type(node.transform:Find("answer"), typeof(UILabel)), ((("[c][8AC2D8][" .. CExpertTeamMgr.Inst.SavePersonalInfo.roleName) .. "][-][/c][c][888B90]") .. ainfo.answer) .. "[-][/c]")

                local bgBtn = node.transform:Find("bgbutton").gameObject
                UIEventListener.Get(bgBtn).onClick = DelegateFactory.VoidDelegate(function (go) 
                    CExpertTeamMgr.Inst.SaveSingleQuestionID = ainfo.qId
                    CUIManager.ShowUI(CUIResources.ExpertTeamAskDetailWnd)
                end)
                i = i + 1
            end
        end

        this.table:Reposition()
        this.scrollview:ResetPosition()
    end
end
