require("common/common_include")

local UIGrid = import "UIGrid"
local Baby_Setting = import "L10.Game.Baby_Setting"
--local Baby_QiChang = import "L10.Game.Baby_QiChang"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local QnTableView = import "L10.UI.QnTableView"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local CUITexture = import "L10.UI.CUITexture"
local Item_Item = import "L10.Game.Item_Item"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CUIFx = import "L10.UI.CUIFx"
local CChatLinkMgr = import "CChatLinkMgr"
local CButton = import "L10.UI.CButton"

LuaBabyQiChangShuWnd = class()

RegistChildComponent(LuaBabyQiChangShuWnd, "TypeScrollView", CUIRestrictScrollView)
RegistChildComponent(LuaBabyQiChangShuWnd, "TypeGrid", UIGrid)
RegistChildComponent(LuaBabyQiChangShuWnd, "QiChangTypePrefab", GameObject)

RegistChildComponent(LuaBabyQiChangShuWnd, "QiChangTypeLabel", UILabel)
RegistChildComponent(LuaBabyQiChangShuWnd, "ActiveAllTypeQiChang", UILabel)
RegistChildComponent(LuaBabyQiChangShuWnd, "GainButton", CButton)
RegistChildComponent(LuaBabyQiChangShuWnd, "GainFx", CUIFx)

RegistChildComponent(LuaBabyQiChangShuWnd, "YiJianDingLabel", UILabel)
RegistChildComponent(LuaBabyQiChangShuWnd, "TableView", QnTableView)
RegistChildComponent(LuaBabyQiChangShuWnd, "QiChangScrollView", CUIRestrictScrollView)
RegistChildComponent(LuaBabyQiChangShuWnd, "QiChangGrid", UIGrid)
RegistChildComponent(LuaBabyQiChangShuWnd, "QiChangItem", GameObject)
RegistChildComponent(LuaBabyQiChangShuWnd, "SetButton", GameObject)

RegistClassMember(LuaBabyQiChangShuWnd, "SelectedBaby")
RegistClassMember(LuaBabyQiChangShuWnd, "TypeItems")
RegistClassMember(LuaBabyQiChangShuWnd, "SelectedQiChangTypeIndex")
RegistClassMember(LuaBabyQiChangShuWnd, "SelectedQiChangId")
RegistClassMember(LuaBabyQiChangShuWnd, "TableViewDataSource")
RegistClassMember(LuaBabyQiChangShuWnd, "SelectedTypeQiChangs")

function LuaBabyQiChangShuWnd:Init()
	self:InitClassMembers()
	self:InitValues()

end

function LuaBabyQiChangShuWnd:InitClassMembers()
	if not LuaBabyMgr.m_ChosenBaby then
		CUIManager.CloseUI(CLuaUIResources.BabyQiChangShuWnd)
		return
	end

	self.SelectedBaby = LuaBabyMgr.m_ChosenBaby
	self.SelectedQiChangTypeIndex = 0
	self.SelectedQiChangId = 0
	self.SelectedTypeQiChangs = {}

	local initItem = function (item, index)
		self:InitItem(item, index)
	end
	self.TableViewDataSource = DefaultTableViewDataSource.CreateByCount(#self.SelectedTypeQiChangs, initItem)
    self.TableView.m_DataSource = self.TableViewDataSource
    self.TableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        self:OnSelectAtRow(row)
    end)

end

function LuaBabyQiChangShuWnd:InitValues()
	self.QiChangTypePrefab:SetActive(false)
	self.QiChangItem:SetActive(false)

	self:InitQiChangeTypes()
	-- 选中第一个

	if #self.TypeItems > 0 and self.TypeItems[1]:GetComponent(typeof(QnSelectableButton)) then
		local selectableBtn = self.TypeItems[1]:GetComponent(typeof(QnSelectableButton))
		selectableBtn:On_Click(self.TypeItems[1])
		self:OnQiChangTypeClicked(self.TypeItems[1], 0)
	end
	
	local onSetButtonClicked = function (go)
		self:OnSetButtonClicked(go)
	end
	CommonDefs.AddOnClickListener(self.SetButton, DelegateFactory.Action_GameObject(onSetButtonClicked), false)
end

function LuaBabyQiChangShuWnd:InitQiChangeTypes()
	CUICommonDef.ClearTransform(self.TypeGrid.transform)
	self.TypeItems = {}

	local setting = Baby_Setting.GetData()
	local qiChangTypes = setting.QiChangTypeName
	for i = 0, qiChangTypes.Length-1 do
		local go = NGUITools.AddChild(self.TypeGrid.gameObject, self.QiChangTypePrefab)
		self:InitQiChangType(go, qiChangTypes[i], i)
		go:SetActive(true)
		table.insert(self.TypeItems, go)
	end

	self.TypeGrid:Reposition()
end

function LuaBabyQiChangShuWnd:InitQiChangType(go, name, index)
	local Label = go.transform:Find("Label"):GetComponent(typeof(UILabel))
	local Alert = go.transform:Find("Alert").gameObject
	Label.text = SafeStringFormat3("[%s]%s[-]", LuaBabyMgr.GetQiChangShuColor(index), name)

	-- 显示红点
	Alert:SetActive(LuaBabyMgr.HasNewQiChangRewardForType(index+1, self.SelectedBaby))
	local onTypeClicked = function (go)
		self:OnQiChangTypeClicked(go, index)
	end
	CommonDefs.AddOnClickListener(go, DelegateFactory.Action_GameObject(onTypeClicked), false)
end

-- index 从0开始
function LuaBabyQiChangShuWnd:OnQiChangTypeClicked(go, index)
	self.SelectedQiChangTypeIndex = index

	for i = 1, #self.TypeItems do
		local selectableBtn = self.TypeItems[i]:GetComponent(typeof(QnSelectableButton))
		if self.TypeItems[i] ~= go then
			selectableBtn:SetSelected(false, false)
		end
	end

	local setting = Baby_Setting.GetData()
	local qiChangTypes = setting.QiChangTypeName
	self.QiChangTypeLabel.text = SafeStringFormat3(LocalString.GetString("[%s]%s气场[-]"), LuaBabyMgr.GetQiChangShuColor(index), qiChangTypes[index])
	

	local qichangReward = Baby_QiChangReward.GetData(index+1)
	if not qichangReward then
		self.ActiveAllTypeQiChang.gameObject:SetActive(false)
	else
		self.ActiveAllTypeQiChang.gameObject:SetActive(true)
		local itemTId = qichangReward.Reward[0]
		local itemLink =  CChatLinkMgr.ChatItemLink.GenerateLink(itemTId, nil)
		self.ActiveAllTypeQiChang.text = SafeStringFormat3(LocalString.GetString("[ACF8FF]鉴定出全部%s气场奖励[-]%s"), qiChangTypes[index], itemLink.displayTag)

		CommonDefs.AddOnClickListener(self.ActiveAllTypeQiChang.gameObject, DelegateFactory.Action_GameObject(function (go)
			local url = self.ActiveAllTypeQiChang:GetUrlAtPosition(UICamera.lastWorldPosition)
    		if url then
        		CChatLinkMgr.ProcessLinkClick(url, nil)
    		end
		end), false)
	end


	self.SelectedQiChangId = 0

	local totalQiChangCounter = 0
	local activeQiChangCounter = 0
	local activedQiChang = self.SelectedBaby.Props.QiChangData.ActivedQiChang
	local currentQiChangId = self.SelectedBaby.Props.QiChangData.CurrentQiChangId

	local jiebanQiChangId = CClientMainPlayer.Inst.PlayProp.JieBanQiChangId
	local isOwnBaby = self.SelectedBaby.OwnerId == CClientMainPlayer.Inst.Id -- 只有专属宝宝才能结伴气场

	self.SelectedTypeQiChangs = {}
	local isAllActived = true

	Baby_QiChang.ForeachKey(function (key)
        local qichang = Baby_QiChang.GetData(key)
        if qichang.Quality-1 == index then

        	local isActive = CommonDefs.DictContains_LuaCall(activedQiChang, key)
        	local isCurrent = key == currentQiChangId
        	local quality = index
        	local isSetJieBan = (jiebanQiChangId == key) and isOwnBaby
        	table.insert(self.SelectedTypeQiChangs, {key = key, 
        		isActive = isActive,
        		isCurrent = isCurrent,
        		quality = quality,
        		isSetJieBan = isSetJieBan,})

        	totalQiChangCounter = totalQiChangCounter + 1
        	if isActive then
        		activeQiChangCounter = activeQiChangCounter + 1
        	end
        	isAllActived = isAllActived and isActive
        end
    end)

	-- todo 领取显示
	if qichangReward then
		local activedReward = self.SelectedBaby.Props.QiChangData.ActivedRewardStatus
		if not activedReward or not CommonDefs.DictContains_LuaCall(activedReward, index+1) then
			-- 没有领取过
			if isAllActived then
				self.GainFx:LoadFx("Fx/UI/Prefab/UI_huangdonglibao.prefab")
				self.GainButton.gameObject:SetActive(true)
				self.GainButton.Enabled = true
				CommonDefs.AddOnClickListener(self.GainButton.gameObject, DelegateFactory.Action_GameObject(function (go)
					Gac2Gas.RequestReceiveQiChangActiveReward(self.SelectedBaby.Id, index+1)
				end), false)
			else
				self.GainFx:DestroyFx()
				self.GainButton.gameObject:SetActive(false)
			end
		else
			-- 领取过了
			self.GainFx:DestroyFx()
			self.GainButton.gameObject:SetActive(true)
			self.GainButton.Enabled = false
		end
	end

	-- 加一个排序
	local function compare(a, b)
		if a.isCurrent and not b.isCurrent then
			return true
		elseif not a.isCurrent and b.isCurrent then
			return false
		else
			if a.isSetJieBan and not b.isSetJieBan then
				return true
			elseif not a.isSetJieBan and b.isSetJieBan then
				return false
			else
				if a.isActive and not b.isActive then
					return true
				elseif not a.isActive and b.isActive then
					return false
				else
					return a.key < b.key
				end
			end
			
		end
	end
	table.sort(self.SelectedTypeQiChangs, compare)

	self.TableViewDataSource.count = #self.SelectedTypeQiChangs
    self.TableView:ReloadData(true, true)

	self.YiJianDingLabel.text = SafeStringFormat3(LocalString.GetString("[ACF8FF]已鉴定[-]  %d/%d"), activeQiChangCounter, totalQiChangCounter)
end

function LuaBabyQiChangShuWnd:InitItem(item, index)

	local info = self.SelectedTypeQiChangs[index+1]
	if not info then return nil end

	local key = info.key
	local isCurrent = info.isCurrent
	local isActive = info.isActive
	local quality = info.quality
	local isSetJieBan = info.isSetJieBan

	local BG = item.transform:GetComponent(typeof(UITexture))
	local QiChangNameLabel = item.transform:Find("QiChangNameLabel"):GetComponent(typeof(UILabel))
	local Lock = item.transform:Find("Lock").gameObject
	local TagGrid = item.transform:Find("TagGrid"):GetComponent(typeof(UIGrid))
	CUICommonDef.ClearTransform(TagGrid.transform)
	local Current = item.transform:Find("Current").gameObject
	Current:SetActive(false)
	local JieBan = item.transform:Find("JieBan").gameObject
	JieBan:SetActive(false)
	local Reward = item.transform:Find("Reward").gameObject
	Reward:SetActive(false)
	local IconTexture = item.transform:Find("Reward/IconTexture"):GetComponent(typeof(CUITexture))
	IconTexture:Clear()
	local DisableSprite = item.transform:Find("Reward/DisableSprite").gameObject

	local qichang = Baby_QiChang.GetData(key)
	local name = qichang.NameM
	local qiChangReward = qichang.RewardM
	if self.SelectedBaby.Gender == 1 then
		name = qichang.NameF
		qiChangReward = qichang.RewardF
	end
	local cname = SafeStringFormat3("[%s]%s[-]", LuaBabyMgr.GetQiChangShuColor(quality), name)
	QiChangNameLabel.text = LocalString.StrH2V(cname,false)
	if not isActive then
		BG.alpha = 0.5
		QiChangNameLabel.alpha = 0.5
	else
		BG.alpha = 1
		QiChangNameLabel.alpha = 1
	end
	Lock:SetActive(not isActive)
	Reward:SetActive(false)
	if qiChangReward and qiChangReward.Length > 0 then
		Reward:SetActive(true)

		local itemTId = qiChangReward[0]
		local item = Item_Item.GetData(itemTId)
    	if item then
        	IconTexture:LoadMaterial(item.Icon)
        	CommonDefs.AddOnClickListener(Reward, DelegateFactory.Action_GameObject(function (go)
            	CItemInfoMgr.ShowLinkItemTemplateInfo(itemTId, false, nil, CItemInfoMgr.AlignType.Default, 0, 0, 0, 0)
        	end), false)
		end
		DisableSprite:SetActive(not isActive)
	else
		local qiChangSkill = nil
		Baby_Skill.ForeachKey((function (skillId)
			local data  = Baby_Skill.GetData(skillId)
			if data.ExQiChang == key and self:IsSkillOpen(skillId) then
				qiChangSkill = data
			end
		end))
		if qiChangSkill then
			Reward:SetActive(true)
			IconTexture:LoadMaterial(qiChangSkill.Icon)
			CommonDefs.AddOnClickListener(Reward, DelegateFactory.Action_GameObject(function (go)
            	LuaBabyMgr.ShowBabySkillTip(qiChangSkill.ID)
        	end), false)
		end
		DisableSprite:SetActive(not isActive)
	end
	

	if isCurrent then
		local go = NGUITools.AddChild(TagGrid.gameObject, Current)
		go:SetActive(true)
	end

	if isSetJieBan then
		local go = NGUITools.AddChild(TagGrid.gameObject, JieBan)
		go:SetActive(true)
	end
	TagGrid:Reposition()

end

function LuaBabyQiChangShuWnd:OnSelectAtRow(row)
	local info = self.SelectedTypeQiChangs[row+1]
	if not info then return end
	self.SelectedQiChangId = info.key
	LuaBabyMgr.OpenQiChangTip(info.key)
end

function LuaBabyQiChangShuWnd:OnQiChangItemClicked(go, key)
	self.SelectedQiChangId = key
	LuaBabyMgr.OpenQiChangTip(key)
end

function LuaBabyQiChangShuWnd:OnSetButtonClicked(go)
	if self.SelectedQiChangId == 0 then
		g_MessageMgr:ShowMessage("BABY_NO_SELECT_QICHANG")
		return
	else
		if self.SelectedBaby.OwnerId == CClientMainPlayer.Inst.Id then
			Gac2Gas.SetJieBanQiChang(self.SelectedBaby.Id , self.SelectedQiChangId)
		else
			g_MessageMgr:ShowMessage("CANNOT_SET_JIEBAN")
		end
		
	end
end

function LuaBabyQiChangShuWnd:SetJieBanQiChangSuccess(bSuccess, babyId, qiChangId)
	if CClientMainPlayer.Inst then
		if #self.TypeItems >= self.SelectedQiChangTypeIndex+1 and self.TypeItems[self.SelectedQiChangTypeIndex+1]:GetComponent(typeof(QnSelectableButton)) then
			local selectableBtn = self.TypeItems[self.SelectedQiChangTypeIndex+1]:GetComponent(typeof(QnSelectableButton))
			selectableBtn:On_Click(self.TypeItems[self.SelectedQiChangTypeIndex+1])
			self:OnQiChangTypeClicked(self.TypeItems[self.SelectedQiChangTypeIndex+1], self.SelectedQiChangTypeIndex)
		end
	end
end

function LuaBabyQiChangShuWnd:ReceiveQiChangActiveRewardSuccess(babyId)
	if babyId == self.SelectedBaby.Id then
		self.GainFx:DestroyFx()
		self.GainButton.Enabled = false
	end

	-- 更新左边一列的红点
	for i = 1, #self.TypeItems do
		local Alert = self.TypeItems[i].transform:Find("Alert").gameObject
		Alert:SetActive(LuaBabyMgr.HasNewQiChangRewardForType(i, self.SelectedBaby))
	end
end

function LuaBabyQiChangShuWnd:OnEnable()
	g_ScriptEvent:AddListener("SetJieBanBabyQiChangRes", self, "SetJieBanQiChangSuccess")
	g_ScriptEvent:AddListener("ReceiveQiChangActiveRewardSuccess", self, "ReceiveQiChangActiveRewardSuccess")
end

function LuaBabyQiChangShuWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SetJieBanBabyQiChangRes", self, "SetJieBanQiChangSuccess")
	g_ScriptEvent:RemoveListener("ReceiveQiChangActiveRewardSuccess", self, "ReceiveQiChangActiveRewardSuccess")
end

function LuaBabyQiChangShuWnd:IsSkillOpen(skillId)
	if CommonDefs.IS_HMT_CLIENT or CommonDefs.IS_VN_CLIENT then
		-- hmt和vn版本屏蔽宝宝说话相关技能和气场相关奖励
		if skillId == 2 or skillId == 4 or skillId == 5 then
			return false
		end
			return true
	else 
		return true
	end
end

return LuaBabyQiChangShuWnd
