-- Auto Generated!!
local Boolean = import "System.Boolean"
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CTalismanTransferItemCell = import "L10.UI.CTalismanTransferItemCell"
local CTalismanTransferWnd = import "L10.UI.CTalismanTransferWnd"
local CUICommonDef = import "L10.UI.CUICommonDef"
local DelegateFactory = import "DelegateFactory"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local Extensions = import "Extensions"
local Gac2Gas = import "L10.Game.Gac2Gas"
local GameObject = import "UnityEngine.GameObject"
local MessageWndManager = import "L10.UI.MessageWndManager"
local MsgPackImpl = import "MsgPackImpl"
local Object = import "System.Object"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CTalismanTransferWnd.m_Init_CS2LuaHook = function (this) 

    CommonDefs.ListClear(this.candidateIds)
    this.agreeCheckbox:SetSelected(false, true)
    this.transferBtn.Enabled = false
    this:LoadItems()
end
CTalismanTransferWnd.m_LoadItems_CS2LuaHook = function (this) 
    CommonDefs.ListClear(this.itemCells)
    Extensions.RemoveAllChildren(this.grid.transform)
    local n = math.max(this.candidateIds.Count, this.grid.maxPerLine * 3)
    do
        local i = 0
        while i < n do
            local go = CUICommonDef.AddChild(this.grid.gameObject, this.itemTpl)
            go:SetActive(true)
            local cell = CommonDefs.GetComponent_GameObject_Type(go, typeof(CTalismanTransferItemCell))
            if i < this.candidateIds.Count then
                cell:Init(this.candidateIds[i])
            else
                cell:Init(nil)
            end
            cell.OnSelectedDelegate = MakeDelegateFromCSFunction(this.OnSelectedDelegate, MakeGenericClass(Action1, GameObject), this)
            CommonDefs.ListAdd(this.itemCells, typeof(CTalismanTransferItemCell), cell)
            i = i + 1
        end
    end
    this.grid:Reposition()
    this.scrollView:ResetPosition()
    this.mask:SetActive(this.candidateIds.Count == 0)
    this.transferBtn.Enabled = (this.candidateIds.Count > 0 and this.agreeCheckbox.Selected)
end
CTalismanTransferWnd.m_Start_CS2LuaHook = function (this) 

    UIEventListener.Get(this.hintBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.hintBtn).onClick, MakeDelegateFromCSFunction(this.OnHintButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.transferBtn.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.transferBtn.gameObject).onClick, MakeDelegateFromCSFunction(this.OnTransferButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.infoBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.infoBtn).onClick, MakeDelegateFromCSFunction(this.OnInfoButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.closeBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeBtn).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), true)
    this.agreeCheckbox.OnValueChanged = CommonDefs.CombineListner_Action_bool(this.agreeCheckbox.OnValueChanged, MakeDelegateFromCSFunction(this.OnAgreeCheckboxValueChanged, MakeGenericClass(Action1, Boolean), this), true)
end
CTalismanTransferWnd.m_OnSelectedDelegate_CS2LuaHook = function (this, go) 
    local itemCell = CommonDefs.GetComponent_GameObject_Type(go, typeof(CTalismanTransferItemCell))
    do
        local i = 0
        while i < this.itemCells.Count do
            this.itemCells[i].Selected = (this.itemCells[i] == itemCell)
            i = i + 1
        end
    end
end
CTalismanTransferWnd.m_OnTransferButtonClick_CS2LuaHook = function (this, go) 
    local message = g_MessageMgr:FormatMessage("TransferFaBao_Confirm")
    MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function () 
        local candidates = CreateFromClass(MakeGenericClass(List, Object))
        do
            local i = 0
            while i < this.candidateIds.Count do
                local pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, this.candidateIds[i])
                if pos > 0 then
                    CommonDefs.ListAdd(candidates, typeof(UInt32), pos)
                end
                i = i + 1
            end
        end

        Gac2Gas.ConfirmTransferXianJiaFaBao(MsgPackImpl.pack(candidates))
    end), nil, nil, nil, false)
end
