local UILabel = import "UILabel"
local Profession = import "L10.Game.Profession"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CUICommonDef = import "L10.UI.CUICommonDef"

LuaLiuYi2022PopoResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaLiuYi2022PopoResultWnd, "ShareBtn", "ShareBtn", GameObject)
RegistChildComponent(LuaLiuYi2022PopoResultWnd, "RestartBtn", "RestartBtn", GameObject)
RegistChildComponent(LuaLiuYi2022PopoResultWnd, "Team", "Team", GameObject)
RegistChildComponent(LuaLiuYi2022PopoResultWnd, "ScoreLabel", "ScoreLabel", UILabel)
RegistChildComponent(LuaLiuYi2022PopoResultWnd, "LimitMark", "LimitMark", UILabel)

--@endregion RegistChildComponent end

function LuaLiuYi2022PopoResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.ShareBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareBtnClick()
	end)


	
	UIEventListener.Get(self.RestartBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRestartBtnClick()
	end)


    --@endregion EventBind end
end

-- win, addPoint, dailyPoint
-- list{ teamId, score,  members dict{playid, {info.name, info.class, info.serverId, info.contribution}} }
function LuaLiuYi2022PopoResultWnd:Init()
    self.ShareBtn.gameObject:SetActive(not CommonDefs.IS_VN_CLIENT)
    local info = LuaLiuYi2022Mgr.resultInfo
    local setting = DuanWu_Setting.GetData()
    local maxCount = setting.BubbleMaxDailyScore

    self.ScoreLabel.text = SafeStringFormat3(LocalString.GetString("本局获得%s积分"), tostring(info.addPoint))

    if info.dailyPoint >= maxCount then
        self.LimitMark.text = LocalString.GetString("[FF5050]今日积分已达上限")
    else
        self.LimitMark.text = SafeStringFormat3(LocalString.GetString("今日积分上限%s/%s"), info.dailyPoint, maxCount)
    end

    local selfId = 0
	if CClientMainPlayer.Inst then
		selfId = CClientMainPlayer.Inst.Id
	end

    local isTeamAWin = not info.win

	local teamA = info.teamInfo[1]
	local teamB = info.teamInfo[2]

    for id, mem in pairs(teamA.members) do
        if id == selfId then
            isTeamAWin = info.win
        end
    end

    local teamList = {}
    if isTeamAWin then
        teamList = {teamA, teamB}
    else
        teamList = {teamB, teamA}
    end
    
    for i=1, 2 do
		local team = teamList[i]
        local teamItem = self.Team.transform:Find(tostring(i))
		local count = teamItem:Find("Count"):GetComponent(typeof(UILabel))
		local name = teamItem:Find("Name"):GetComponent(typeof(UILabel))
		count.text = team.score

        -- 阵营名
		if i == 1 then
			name.text = LocalString.GetString("柠檬糖")
		else
			name.text = LocalString.GetString("草莓卷")
		end

        for k=1,3 do
            teamItem:Find("Info/"..tostring(k)).gameObject:SetActive(false)
        end

        -- 初始化玩家信息
		local j = 0
		for id, info in pairs(team.members) do
			j = j + 1

            local playerItem = teamItem:Find("Info/"..tostring(j))
            playerItem.gameObject:SetActive(true)
			local clsSp = playerItem:Find("ClsSprite"):GetComponent(typeof(UISprite))
			local name = playerItem:Find("Name"):GetComponent(typeof(UILabel))
			local playerCount = playerItem:Find("Count"):GetComponent(typeof(UILabel))
			local severName = playerItem:Find("SeverName"):GetComponent(typeof(UILabel))

			if id == selfId then
                -- 设置玩家label颜色
                name.color = Color(0,1,96/255)
                playerCount.color = Color(0,1,96/255)
                severName.color = Color(0,1,96/255)
			end

			name.text = info.name
			playerCount.text = info.contribution
			clsSp.spriteName = Profession.GetIconByNumber(info.class)
            severName.text = PlayConfig_ServerIdEnabled.GetData(info.serverId).ServerName
		end
	end
end

function LuaLiuYi2022PopoResultWnd:OnDisable()
    LuaDuanWu2022Mgr.resultInfo = nil
    UnRegisterTick(self.m_ShareTick)
end

function LuaLiuYi2022PopoResultWnd:SetOpBtnVisible(sign)
    self.ShareBtn:SetActive(sign)
    self.RestartBtn:SetActive(sign)
  end

--@region UIEvent

function LuaLiuYi2022PopoResultWnd:OnShareBtnClick()
    self:SetOpBtnVisible(false)
    UnRegisterTick(self.m_ShareTick)

    self.m_ShareTick = RegisterTickWithDuration(function ()
      self:SetOpBtnVisible(true)
    end, 1000, 1000)

    CUICommonDef.CaptureScreenAndShare()
end

function LuaLiuYi2022PopoResultWnd:OnRestartBtnClick()
    CUIManager.ShowUI(CLuaUIResources.LiuYi2022PopoEnterWnd)
    CUIManager.CloseUI(CLuaUIResources.LiuYi2022PopoResultWnd)
end


--@endregion UIEvent

