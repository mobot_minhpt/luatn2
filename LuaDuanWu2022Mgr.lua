local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CScene = import "L10.Game.CScene"
local PlayerPrefs = import "UnityEngine.PlayerPrefs"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CCommonUIFxWnd = import "L10.UI.CCommonUIFxWnd"

LuaDuanWu2022Mgr = class()
LuaDuanWu2022Mgr.selfPlayerRankData = nil
LuaDuanWu2022Mgr.resultData = nil
LuaDuanWu2022Mgr.buffData = nil

LuaDuanWu2022Mgr.EnumDuanWuPlayTime ={
    eDuanWu2022PVE = 231,
    eDuanWu2022PV_PassTime = 232,
    eDuanWu2022FKPP_Daily = 236,
}

LuaDuanWu2022Mgr.EnumSeasonRankType = {
	eTypeStart = 2000,
	-- 端午2022驱五毒各职业排行榜
	eSheShouDuanWu2022QuWuDu = 2001,
	eJiaShiDuanWu2022QuWuDu = 2002,
	eDaoKeDuanWu2022QuWuDu = 2003,
	eXiaKeDuanWu2022QuWuDu = 2004,
	eFangShiDuanWu2022QuWuDu = 2005,
	eYiShiDuanWu2022QuWuDu = 2006,
	eMeiZheDuanWu2022QuWuDu = 2007,
	eYiRenDuanWu2022QuWuDu = 2008,
	eYanShiDuanWu2022QuWuDu = 2009,
	eHuaHunDuanWu2022QuWuDu = 2010,
	eYingLingDuanWu2022QuWuDu = 2011,
	eDieKeDuanWu2022QuWuDu = 2012,
}

function LuaDuanWu2022Mgr:IsInWuduPlay()
    if CScene.MainScene then
        local gamePlayId = CScene.MainScene.GamePlayDesignId
        local gameplayList = DuanWu_Setting.GetData().QuWuDuGameplayId
        for i=0, gameplayList.Length-1 do
            if gamePlayId == gameplayList[i] then
                return true
            end
        end
    end
    return false
end

function LuaDuanWu2022Mgr:GetPlayerRankType()
    if CClientMainPlayer.Inst then
        local clz = EnumToInt(CClientMainPlayer.Inst.Class)
        return clz + self.EnumSeasonRankType.eTypeStart
    end
end

function LuaDuanWu2022Mgr:OpenChooseWnd(selfPlayerData)
    self.selfPlayerRankData = selfPlayerData
    CUIManager.ShowUI(CLuaUIResources.DuanWu2022WuduChooseWnd)
end

function LuaDuanWu2022Mgr:OpenResultWnd(difficulty1, difficulty2, difficulty3, floor, usedTime, score, bBreakRecord)
    self.resultData = {}
    self.resultData.difficulty1 = difficulty1
    self.resultData.difficulty2 = difficulty2
    self.resultData.difficulty3 = difficulty3
    self.resultData.floor = floor
    self.resultData.usedTime = usedTime
    self.resultData.score = score
    self.resultData.bBreakRecord = bBreakRecord

    if floor == 5 then
        -- 胜利
        CCommonUIFxWnd.AddUIFx(89000135, 1, true, false)
    else
        -- 死亡 战斗结束
        CCommonUIFxWnd.AddUIFx(89000137, 1, true, false)
    end
    
    RegisterTickOnce(function()
        CUIManager.ShowUI(CLuaUIResources.DuanWu2022WuduResultWnd)
    end, 1200)
end

function LuaDuanWu2022Mgr:OpenBuffWnd(candidateBuffData, selectedBuffsData)
    self.buffData = {}
    self.buffData.candidateBuffList = g_MessagePack.unpack(candidateBuffData)
    self.buffData.selectedBuffList = g_MessagePack.unpack(selectedBuffsData)
    CUIManager.ShowUI(CLuaUIResources.DuanWu2022WuduBuffWnd)
end

-- Key:Score Difficulty1 Difficulty2 Difficulty3 playerId name updateTime overdueTime
function LuaDuanWu2022Mgr:TransformRankData(selfDataU, rankDataU)
    local selfRankData = {}
    local rankDataList = {}

    local selfData = MsgPackImpl.unpack(selfDataU)
    if CommonDefs.IsDic(selfData) then
        CommonDefs.DictIterate(selfData, DelegateFactory.Action_object_object(function (___key, ___value)
            selfRankData[tostring(___key)]=___value
        end))
    end

    local selfId = selfRankData.playerId
    local selfRank = 0

    local rankData = MsgPackImpl.unpack(rankDataU)
    if rankData.Count>0 then
        for i=1,rankData.Count do
            local data = rankData[i-1]
            local t = {}
            if CommonDefs.IsDic(data) then
                CommonDefs.DictIterate(data, DelegateFactory.Action_object_object(function (___key, ___value) 
                    t[tostring(___key)] = ___value
                end))
            end

            if t.playerId and t.playerId==selfId then
                selfRank = i
            end

            table.insert(rankDataList, t)
        end
    end

    selfRankData.rank = selfRank
    return selfRankData, rankDataList
end