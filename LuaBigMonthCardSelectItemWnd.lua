local Item_Item = import "L10.Game.Item_Item"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"

LuaBigMonthCardSelectItemWnd = class()

RegistClassMember(LuaBigMonthCardSelectItemWnd, "m_SelectedItemId")
RegistClassMember(LuaBigMonthCardSelectItemWnd, "m_SelectedSp")

function LuaBigMonthCardSelectItemWnd:Awake()
    UIEventListener.Get(self.transform:Find("Anchor/SettingButton").gameObject).onClick = DelegateFactory.VoidDelegate(function()
        CUIManager.ShowUI(CLuaUIResources.ReceiveGiftPackSettingWnd)
    end)
    UIEventListener.Get(self.transform:Find("Anchor/OkButton").gameObject).onClick = DelegateFactory.VoidDelegate(function()
        if self.m_SelectedItemId then
            Gac2Gas.RequestGetBigMonthCardDailyGift(self.m_SelectedItemId)
            CUIManager.CloseUI(CLuaUIResources.BigMonthCardSelectItemWnd)
        end
    end)
end

function LuaBigMonthCardSelectItemWnd:Init()
    if not CClientMainPlayer.Inst then 
        CUIManager.CloseUI(CLuaUIResources.BigMonthCardSelectItemWnd)
        return 
    end
    self.m_SelectedSp = {}
    local i = 1
    Charge_BigMonthCardItem.Foreach(function(k, v)
        local trans = self.transform:Find("Anchor/Item"..i)
        self:InitRewardItem(trans, k, v.ItemCount, v.NeedBind == 1, v.NeedFeiSheng == 1)
        self.m_SelectedSp[i] = trans:Find("Selected").gameObject
        i = i + 1
    end)
end

function LuaBigMonthCardSelectItemWnd:InitRewardItem(trans, itemId, count, needBind, needFeiSheng)
    local iconTexture = trans:Find("IconTexture"):GetComponent(typeof(CUITexture))
    iconTexture:Clear()
    local qualitySprite = trans:Find("QualitySprite"):GetComponent(typeof(UISprite))
    qualitySprite.spriteName = nil
    local amountLabel = trans:Find("AmountLabel"):GetComponent(typeof(UILabel))
    amountLabel.text = nil
    trans:Find("BindSprite").gameObject:SetActive(needBind)
    local selected = trans:Find("Selected").gameObject
    local nameLb = trans:Find("NameLabel"):GetComponent(typeof(UILabel))

    local isFeiSheng = CClientMainPlayer.Inst.HasFeiSheng
    if isFeiSheng then
        trans:Find("FeiSheng").gameObject:SetActive(false)
    else
        trans:Find("FeiSheng").gameObject:SetActive(needFeiSheng)
        trans:GetComponent(typeof(UISprite)).alpha = needFeiSheng and 0.5 or 1
    end

    local itemTemplate = Item_Item.GetData(itemId)
    if not itemTemplate then return end

    nameLb.text = itemTemplate.Name
    iconTexture:LoadMaterial(itemTemplate.Icon)
    if count > 1 then
        amountLabel.text = tostring(count)
    end
    qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(itemTemplate, nil, false)
    local selectFunc = function()
        if not isFeiSheng and needFeiSheng then
            g_MessageMgr:ShowMessage("BMC_Cannot_Get_ZiBei")
        else
            self.m_SelectedItemId = itemId
            for i = 1, 3 do
                self.m_SelectedSp[i]:SetActive(false)
            end
            selected:SetActive(true)
        end
    end
    CommonDefs.AddOnClickListener(iconTexture.gameObject, DelegateFactory.Action_GameObject(function (go)
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, CItemInfoMgr.AlignType.Default, 0, 0, 0, 0)
        selectFunc()
    end), false)
    CommonDefs.AddOnClickListener(trans.gameObject, DelegateFactory.Action_GameObject(function (go)
        selectFunc()
    end), false)
end
