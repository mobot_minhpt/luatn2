-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CNationalDayMgr = import "L10.UI.CNationalDayMgr"
local CNationalDayWnd = import "L10.UI.CNationalDayWnd"
local CTickMgr = import "L10.Engine.CTickMgr"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local ETickType = import "L10.Engine.ETickType"
local EventManager = import "EventManager"
local LocalString = import "LocalString"
local NGUIText = import "NGUIText"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
CNationalDayWnd.m_Init_CS2LuaHook = function (this) 
    this:InitTime()
    this.m_PlayerNameLabel.text = CNationalDayMgr.Inst.m_PlayerName
    if CNationalDayMgr.Inst.m_PlayerId ~= CClientMainPlayer.Inst.Id then
        this.m_IsAudience = true
        this.m_PlayerNameLabel.color = NGUIText.ParseColor("ACF8FF", 0)
        this.m_TopTipLabel.text = LocalString.GetString("当前为观众，请等待队友答题")
    else
        this.m_IsAudience = false
        this.m_PlayerNameLabel.color = NGUIText.ParseColor("03DB0B", 0)
        this.m_TopTipLabel.text = (LocalString.GetString("请在") .. CNationalDayMgr.Inst.m_AnswerDuration) .. LocalString.GetString("秒内作答")
    end
    this.m_QuestionLabel.text = CNationalDayMgr.Inst.m_Question
    this.m_RuleLabel.text = g_MessageMgr:FormatMessage("GuoQing_DaTi_Rule")
    this:InitOptions()
end
CNationalDayWnd.m_InitTime_CS2LuaHook = function (this) 
    this.m_LeftTime = CNationalDayMgr.Inst.m_AnswerDuration
    this.m_CountDownLabel.text = (LocalString.GetString("还剩") .. tostring(this.m_LeftTime)) .. LocalString.GetString("秒")
    this.m_CountDownSlider.value = 1
    this.m_CountDownTick = CTickMgr.Register(DelegateFactory.Action(function () 
        this.m_LeftTime = this.m_LeftTime - 1
        this.m_CountDownLabel.text = (LocalString.GetString("还剩") .. tostring(this.m_LeftTime)) .. LocalString.GetString("秒")
        this.m_CountDownSlider.value = this.m_LeftTime / CNationalDayMgr.Inst.m_AnswerDuration
        if this.m_LeftTime <= 0 then
            Gac2Gas.SubmitGuoQingXianLiDaTiAnswer(0)
            if this.m_CountDownTick ~= nil then
                invoke(this.m_CountDownTick)
                this.m_CountDownTick = nil
            end
        end
    end), 1000, ETickType.Loop)
end
CNationalDayWnd.m_OnOptionClick_CS2LuaHook = function (this, go) 
    do
        local j = 0
        while j < this.m_OptionObjs.Count do
            if this.m_OptionObjs[j] == go then
                Gac2Gas.SubmitGuoQingXianLiDaTiAnswer(j + 1)
                break
            end
            j = j + 1
        end
    end
end
CNationalDayWnd.m_OnDestroy_CS2LuaHook = function (this) 
    EventManager.RemoveListenerInternal(EnumEventType.NationalDayUpdateAnswer, MakeDelegateFromCSFunction(this.ShowAnswer, MakeGenericClass(Action3, UInt32, UInt32, UInt32), this))
    if this.m_CountDownTick ~= nil then
        invoke(this.m_CountDownTick)
        this.m_CountDownTick = nil
    end
    if this.m_CloseWindowTick ~= nil then
        invoke(this.m_CloseWindowTick)
        this.m_CloseWindowTick = nil
    end
end
CNationalDayWnd.m_ShowAnswer_CS2LuaHook = function (this, rightAnswer, playerAnswer, showDuration) 
    this.m_CloseWindowTick = CTickMgr.Register(DelegateFactory.Action(function () 
        this:Close()
    end), showDuration * 1000, ETickType.Once)
    if this.m_CountDownTick ~= nil then
        invoke(this.m_CountDownTick)
        this.m_CountDownTick = nil
    end
    if not this.m_IsAudience then
        do
            local i = 0
            while i < this.m_OptionObjs.Count do
                UIEventListener.Get(this.m_OptionObjs[i]).onClick = nil
                i = i + 1
            end
        end
    end
    if playerAnswer ~= 0 then
        if rightAnswer > 0 and rightAnswer <= this.m_OptionObjs.Count then
            local trans = this.m_OptionObjs[rightAnswer - 1].transform:Find("RightTip")
            if trans ~= nil then
                trans.gameObject:SetActive(true)
            end
        end
        if playerAnswer ~= rightAnswer then
            local trans = this.m_OptionObjs[playerAnswer - 1].transform:Find("WrongTip")
            if trans ~= nil then
                trans.gameObject:SetActive(true)
            end
        end
    else
        this.m_CountDownLabel.text = LocalString.GetString("还剩0秒")
        this.m_CountDownSlider.value = 0
    end
end
