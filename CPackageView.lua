-- Auto Generated!!
local CCJBMgr = import "L10.Game.CCJBMgr"
local CClientFurnitureMgr = import "L10.Game.CClientFurnitureMgr"
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local EnumClass = import "L10.Game.EnumClass"
local CCommonItem = import "L10.Game.CCommonItem"
local CCommonItemExchangeMgr = import "L10.UI.CCommonItemExchangeMgr"
local CEquipment = import "L10.Game.CEquipment"
local CEquipmentProcessMgr = import "L10.Game.CEquipmentProcessMgr"
local CEquipWuZuanReplaceMgr = import "L10.Game.CEquipWuZuanReplaceMgr"
local CExpandPackageMgr = import "L10.UI.CExpandPackageMgr"
local CFoeListMgr = import "L10.UI.CFoeListMgr"
local CHouseUIMgr = import "L10.UI.CHouseUIMgr"
local CHunPoMgr = import "L10.UI.CHunPoMgr"
local CIndirectUIResources = import "L10.UI.CIndirectUIResources"
local CItemBagMgr = import "L10.Game.CItemBagMgr"
local CItemComposeMgr = import "L10.UI.CItemComposeMgr"
local CItemInfo = import "L10.Game.CItemInfo"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CKaiguangMgr = import "L10.UI.CKaiguangMgr"
local CLingfuBaptizeWnd = import "L10.UI.CLingfuBaptizeWnd"
-- local CLingShouItemUseMgr = import "L10.UI.CLingShouItemUseMgr"
local CLingShouMgr = import "L10.Game.CLingShouMgr"
local CLingShouMorphWnd = import "L10.UI.CLingShouMorphWnd"
local CLingShouSwitchSkillMgr = import "L10.Game.CLingShouSwitchSkillMgr"
local CLivingSkillMgr = import "L10.Game.CLivingSkillMgr"
local CMapiProp = import "L10.Game.CMapiProp"
local CommonDefs = import "L10.Game.CommonDefs"
local Constants = import "L10.Game.Constants"
local CPackageView = import "L10.UI.CPackageView"
local CPlayerMainStoryMgr = import "L10.Game.CPlayerMainStoryMgr"
local CPlayerShopData = import "L10.UI.CPlayerShopData"
local CPlayerShopItemData = import "L10.UI.CPlayerShopItemData"
local CPlayerShopMgr = import "L10.UI.CPlayerShopMgr"
local CPos = import "L10.Engine.CPos"
local CPresentMgr = import "L10.Game.CPresentMgr"
local CProductCompositeWnd = import "L10.UI.CProductCompositeWnd"
local CQianliyanMgr = import "L10.UI.CQianliyanMgr"
local CQingQiuMgr = import "L10.UI.CQingQiuMgr"
local CResourceMgr = import "L10.Engine.CResourceMgr"
local CScene = import "L10.Game.CScene"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CShanHeTuMgr = import "L10.UI.CShanHeTuMgr"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"
local CSpringFestivalMgr = import "L10.Game.CSpringFestivalMgr"
local CStatusMgr = import "L10.Game.CStatusMgr"
local CSwitchMgr = import "L10.Engine.CSwitchMgr"
local CTalismanWndMgr = import "L10.UI.CTalismanWndMgr"
local CTaskMgr = import "L10.Game.CTaskMgr"
local CTrackMgr = import "L10.Game.CTrackMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local CValentineMgr = import "L10.UI.CValentineMgr"
local CWelfareMgr = import "L10.UI.CWelfareMgr"
local CYuanbaoMarketMgr = import "L10.UI.CYuanbaoMarketMgr"
local CZhushabiEvaluateMgr = import "L10.UI.CZhushabiEvaluateMgr"
local CZuoQiMgr = import "L10.Game.CZuoQiMgr"
local DelegateFactory = import "DelegateFactory"
local EHouseMainWndTab = import "L10.UI.EHouseMainWndTab"
local EnumComposeItemType = import "L10.UI.EnumComposeItemType"
local EnumEvent = import "L10.Game.EnumEvent"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EnumLingfuBaptizeType = import "L10.UI.EnumLingfuBaptizeType"
local EnumQualityType = import "L10.Game.EnumQualityType"
local EquipBaptize_EquipTransform = import "L10.Game.EquipBaptize_EquipTransform"
local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"
local Extensions = import "Extensions"
local GameplayItem_OfflineItem = import "L10.Game.GameplayItem_OfflineItem"
local GameSetting_Common = import "L10.Game.GameSetting_Common"
local GameSetting_Common_Wapper = import "L10.Game.GameSetting_Common_Wapper"
local House_RedeemResource = import "L10.Game.House_RedeemResource"
local House_Setting = import "L10.Game.House_Setting"
local House_ZhuLongShiCompose = import "L10.Game.House_ZhuLongShiCompose"
local IdPartition = import "L10.Game.IdPartition"
local Item_Item = import "L10.Game.Item_Item"
local Jewel_HeCheng = import "L10.Game.Jewel_HeCheng"
local Jewel_JinGangZuan = import "L10.Game.Jewel_JinGangZuan"
local L10 = import "L10"
local LingShou_Setting = import "L10.Game.LingShou_Setting"
local LiuYi_Setting = import "L10.Game.LiuYi_Setting"
local LocalString = import "LocalString"
local Main = import "L10.Engine.Main"
local Mall_YuanBaoMarket = import "L10.Game.Mall_YuanBaoMarket"
local MessageWndManager = import "L10.UI.MessageWndManager"
local MsgPackImpl = import "MsgPackImpl"
local NGUIText = import "NGUIText"
local Object = import "System.Object"
local PreSellContextType = import "L10.UI.PreSellContextType"
local QualityColor = import "L10.Game.QualityColor"
local RequestReason = import "L10.UI.CPlayerShopData+RequestReason"
local ShiTu_PlaySetting = import "L10.Game.ShiTu_PlaySetting"
local SoundManager = import "SoundManager"
local String = import "System.String"
local StringActionKeyValuePair = import "L10.Game.StringActionKeyValuePair"
local StringBuilder = import "System.Text.StringBuilder"
local System = import "System"
local Talisman_MakeXianJia = import "L10.Game.Talisman_MakeXianJia"
local Item_Preview = import "L10.Game.Item_Preview"
local Talisman_ResetWord = import "L10.Game.Talisman_ResetWord"
local Talisman_Setting = import "L10.Game.Talisman_Setting"
local Talisman_XianJia = import "L10.Game.Talisman_XianJia"
local UInt32 = import "System.UInt32"
local Utility = import "L10.Engine.Utility"
local Vector3 = import "UnityEngine.Vector3"
local WaBao_Setting = import "L10.Game.WaBao_Setting"
local Wedding_Setting = import "L10.Game.Wedding_Setting"
local ZhaoQin_Setting = import "L10.Game.ZhaoQin_Setting"
local ZhouMoActivity_ClientSetting = import "L10.Game.ZhouMoActivity_ClientSetting"
local Zhuangshiwu_ProducePlant = import "L10.Game.Zhuangshiwu_ProducePlant"
local Zhuangshiwu_Zhuangshiwu = import "L10.Game.Zhuangshiwu_Zhuangshiwu"
local ZuoQi_Setting = import "L10.Game.ZuoQi_Setting"
local CFashionPreviewMgr = import "L10.UI.CFashionPreviewMgr"
local CExpressionActionMgr = import "L10.Game.CExpressionActionMgr"

local Zhuangshiwu_Setting = import "L10.Game.Zhuangshiwu_Setting"
local CNearbyPlayerWndMgr = import "L10.UI.CNearbyPlayerWndMgr"
local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"
local ShareMgr = import "ShareMgr"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
local IdentifySkill_Settings = import "L10.Game.IdentifySkill_Settings"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local CChuanjiabaoWordType = import "L10.Game.CChuanjiabaoWordType"
local CPackageViewItemResource = import "L10.UI.CPackageViewItemResource"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EnumItemFlag = import "L10.Game.EnumItemFlag"
local EnumPlayerInfoContext=import "L10.Game.EnumPlayerInfoContext"
local EChatPanel=import "L10.Game.EChatPanel"
local AlignType2=import "CPlayerInfoMgr+AlignType"
local EnumPreviewType = import "L10.UI.EnumPreviewType"
local CWebBrowserMgr = import "L10.Game.CWebBrowserMgr"
local CLoginMgr = import "L10.Game.CLoginMgr"
local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"
local CItemAccessListMgr=import "L10.UI.CItemAccessListMgr"
local AlignType1 = import "L10.UI.CTooltip+AlignType"
--local IdentifySkill_JianXiaLevel = import "L10.Game.IdentifySkill_JianXiaLevel"
CPackageView.m_PreviewFashionItemFunction_CS2LuaHook = function (this)
    local item = CItemMgr.Inst:GetById(this.SelectedItemId)
    if item then
        local data = Item_Preview.GetData(item.TemplateId)
        if data then
            if data.Type == 1 then
                CZuoQiMgr.Inst:ShowVehiclePreview(data.PreviewID)
            elseif data.Type == 2 then
                CFashionPreviewMgr.ShowFashionItemPreview(data.PreviewID, data.IsSuit)
            elseif data.Type == 3 then
                CFashionPreviewMgr.curPreviewType = EnumPreviewType.PreviewUmbrella
                CFashionPreviewMgr.UmbrellaFashionID = data.PreviewID
                CUIManager.ShowUI(CUIResources.FashionPreviewWnd)
            end
        end
    end
end
CPackageView.m_ShowPackage_CS2LuaHook = function (this, place)
    this:SetPackageStatus(false)
    this.moneyInfoView.gameObject:SetActive(true)
    this.playerInfoView.gameObject:SetActive(true)
    this.recycleView.gameObject:SetActive(false)

    this.settingBtn:SetActive(place == EnumItemPlace.Bag)
    this.clearupBtn:SetActive(place == EnumItemPlace.Bag)
    this.TaskTopOffset = this.PackageTopOffset
    this:InitPackage(place)
end
CPackageView.m_StopInitPackageCoroutine_CS2LuaHook = function (this)
    if this.initPackageCoroutine ~= nil then
        Main.Inst:StopCoroutine(this.initPackageCoroutine)
        this.initPackageCoroutine = nil
    end
end
CPackageView.m_RecycleItems_CS2LuaHook = function (this)
    if this==nil or this.grid==nil then return end -- app dump报错屏蔽
    local children
    children = Extensions.GetChildren(this.grid, false)
    do
        local i = 0
        while i < children.Count do
            children[i].parent = nil
            CResourceMgr.Inst:Destroy(children[i].gameObject)
            i = i + 1
        end
    end
    CommonDefs.ListClear(this.itemCells)
end
CPackageView.m_LoadItems_CS2LuaHook = function (this)
    if CClientMainPlayer.Inst == nil then
        return
    end
    local itemProp = CClientMainPlayer.Inst.ItemProp

    do
        local i = 0
        while i < this.itemCells.Count do
            local itemId = itemProp:GetItemAt(this.curPlace, i + 1)
            this.itemCells[i]:Init(itemId, this.curPlace == EnumItemPlace.Bag and i + 1 > CClientMainPlayer.Inst.ItemProp.BagPlaceSize and i + 1 <= CClientMainPlayer.Inst.ItemProp:GetPlaceMaxSize(EnumItemPlace.Bag), this.IsInClearUpStatus, CommonDefs.ListContains(this.waitingForClearUpList, typeof(String), itemId), this.scrollView)
            this.itemCells[i].Selected = false
            i = i + 1
        end
    end
end
CPackageView.m_OnItemClick_CS2LuaHook = function (this, itemCell)
    if CClientMainPlayer.Inst == nil or itemCell == nil then
        return
    end

    if not this.IsInClearUpStatus then
        do
            local i = 0
            while i < this.itemCells.Count do
                this.itemCells[i].Selected = (this.itemCells[i] == itemCell)
                i = i + 1
            end
        end
    end

    --未解锁包裹格子，打开包裹开格子窗口
    if this.curPlace == EnumItemPlace.Bag and itemCell.IsLocked then
        CExpandPackageMgr.Inst:ShowExpandPackageWnd()
        return
    end
    local itemId = itemCell.ItemId
    if System.String.IsNullOrEmpty(itemId) then
        return
    end
    local pos = CClientMainPlayer.Inst.ItemProp:GetItemPos(this.curPlace, itemId)
    if pos == 0 then
        return
    end
    local item = CItemMgr.Inst:GetById(itemId)
    if item == nil then
        return
    end

    if this.IsInClearUpStatus then
        if (item.IsEquip or item.Item.CanBeDiscarded) then
            itemCell.isChecked = not itemCell.isChecked
            if itemCell.isChecked and not CommonDefs.ListContains(this.waitingForClearUpList, typeof(String), itemId) then
                CommonDefs.ListAdd(this.waitingForClearUpList, typeof(String), itemId)
            elseif not itemCell.isChecked and CommonDefs.ListContains(this.waitingForClearUpList, typeof(String), itemId) then
                CommonDefs.ListRemove(this.waitingForClearUpList, typeof(String), itemId)
                if (item.IsEquip and not item.Equip.IsRedOrPurpleEquipment) or (item.IsItem and item:IsItemExpire()) then
                    this.selectBlueCheckbox.Selected = false
                end
            end
        else
            g_MessageMgr:ShowMessage("CANNOT_THROW", item.Name)
        end
        return
    end
    this.SelectedItemId = itemId
    CItemInfoMgr.ShowPackageItemInfo(itemId, pos, this, this)
end
CPackageView.m_GetActionPairs_CS2LuaHook = function (this, itemId, templateId)
    if CClientMainPlayer.Inst == nil or System.String.IsNullOrEmpty(itemId) then
        return nil
    end

    local ret,actions=GetActionPairs(this,itemId,templateId)
    if ret then return actions end

    --#region 任务栏操作
    local taskItemActions = CPackageViewMethodHolder.GetTaskActionPairs(this, itemId, templateId)
    if taskItemActions ~= nil and taskItemActions.Count > 0  then return taskItemActions end
    --#endregion

    local pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, itemId)
    local item = CItemMgr.Inst:GetById(itemId)
    if pos == 0 or item == nil then
        return nil
    end
    if item.IsEquip then
        local applyAction = StringActionKeyValuePair(LocalString.GetString("装备"), MakeDelegateFromCSFunction(this.OnApply, System.Action, this))
        local processAction = StringActionKeyValuePair(LocalString.GetString("打造"), MakeDelegateFromCSFunction(this.OnEquipmentClick, System.Action, this))
        local auctionAction = StringActionKeyValuePair(LocalString.GetString("出售"), MakeDelegateFromCSFunction(this.OnSell, System.Action, this))
        local fixAction = StringActionKeyValuePair(LocalString.GetString("修理"), MakeDelegateFromCSFunction(this.OnRepair, System.Action, this))
        local discardAction = StringActionKeyValuePair(LocalString.GetString("回收"), MakeDelegateFromCSFunction(this.OnDiscard, System.Action, this))
        local disassembleAction = StringActionKeyValuePair(LocalString.GetString("拆解"), MakeDelegateFromCSFunction(this.OnDisassemble, System.Action, this))
        local bindAction = StringActionKeyValuePair(LocalString.GetString("绑定"), MakeDelegateFromCSFunction(this.OnBindItem, System.Action, this))
        local fixLostSoulAction = StringActionKeyValuePair(LocalString.GetString("失魂修复"), MakeDelegateFromCSFunction(this.OnFixLostSoul, System.Action, this))
        local equipTalismanAction = StringActionKeyValuePair(LocalString.GetString("佩戴"), MakeDelegateFromCSFunction(this.OnEquipTalisman, System.Action, this))
        local upgradeXianjiaAction = StringActionKeyValuePair(LocalString.GetString("升阶"), MakeDelegateFromCSFunction(this.OnUpgradeXianjia, System.Action, this))
        local qianShanSwitchAction = StringActionKeyValuePair(LocalString.GetString("琴扇转换"), MakeDelegateFromCSFunction(this.OnQinShanSwitch, System.Action, this))
        local talismanChaijieAction = StringActionKeyValuePair(LocalString.GetString("拆解"), MakeDelegateFromCSFunction(this.OnTalismanChaiJie, System.Action, this))
        
        --战狂武器主副手转化
        local handchange = function()
            if CClientMainPlayer.Inst ~= nil then
                local pos = CClientMainPlayer.Inst.ItemProp:GetItemPos(this.curPlace, this.SelectedItemId)
                local item = CItemMgr.Inst:GetById(this.SelectedItemId)
                if pos > 0 and item ~= nil and item.IsEquip then
                    Gac2Gas.CanTransformZKWeapon(EnumToInt(this.curPlace), pos, this.SelectedItemId)
                end
            end
        end
        local tosubhandAction = StringActionKeyValuePair(LocalString.GetString("转为副手"), DelegateFactory.Action(handchange))
        local tomainhandAction = StringActionKeyValuePair(LocalString.GetString("转为主手"), DelegateFactory.Action(handchange))

        --海钓
        local fishBasketAction = StringActionKeyValuePair(LocalString.GetString("打开鱼篓"), DelegateFactory.Action(function ()
            CUIManager.ShowUI(CLuaUIResources.SeaFishingPackageWnd)
        end))
        local fishPoleTakeApartAction = StringActionKeyValuePair(LocalString.GetString("拆解"), DelegateFactory.Action(function ()
            MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("FishPole_TakeApart_Confirm"),
                DelegateFactory.Action(function ()
                    Gac2Gas.ChaijieFishingPole(EnumItemPlace.Bag, pos, itemId)
                end),
                nil,
                LocalString.GetString("确定"), LocalString.GetString("取消"), false)
        end))

        local actionPairs = CreateFromClass(MakeGenericClass(List, StringActionKeyValuePair))
        if IdPartition.IdIsTalisman(item.TemplateId) then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), equipTalismanAction)
            if item.Equip.Color ~= EnumQualityType.Orange then
                CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), processAction)
            end
            if item.Equip.IsFairyTalisman and this.curPlace == EnumItemPlace.Bag then
                local xianjia = Talisman_XianJia.GetData(item.TemplateId)
                if xianjia.Grade ~= 4 then
                    CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), upgradeXianjiaAction)
                end
            end
            local chaijieColor = Talisman_Setting.GetData().ChaiJieColor
            do
                local i = 0
                while i < chaijieColor.Length do
                    if chaijieColor[i] == EnumToInt(item.Equip.Color) then
                        CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), talismanChaijieAction)
                        break
                    end
                    i = i + 1
                end
            end
        else
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), applyAction)
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), processAction)
            if CLuaWeddingMgr.IsWeddingRing(item.TemplateId) then
                CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), g_WeddingRingTabenAction)
            end
            if item.Equip.Duration < item.Equip.MaxDuration then
                CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), fixAction)
            end
            --本次测试不做拆解功能，暂时全部替换成丢弃
            if item.Equip.IsDisassemble and not item.Equip.IsShenBing then
                CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), disassembleAction)
            end
            if item.Equip.IsLostSoul == 1 then
                CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), fixLostSoulAction)
            end

            --法宝不考虑
            --是否是鬼装 绑定
            if CSwitchMgr.EnableGhostWordCompose then
                if item.Equip.IsGhost and item.IsBinded and not item.Equip.IsExtraEquipment then
                    local composeGhost = StringActionKeyValuePair(LocalString.GetString("二套词条"), MakeDelegateFromCSFunction(this.ComposeGhostEquipWord, System.Action, this))
                    CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), composeGhost)
                end
            end

            --神兵打造
            --if item.Equip.IsShenBing then
            --    CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), StringActionKeyValuePair(LocalString.GetString("神兵打造"), DelegateFactory.Action(function ( ... )
            --        CUIManager.ShowUI(CLuaUIResources.ShenBingPeiYangWnd)
            --    end)))
            --end
        end

        local zfdata = EquipBaptize_ZKWeaponTransform.GetData(templateId)
        if zfdata and zfdata.Status == 0 then
            if item.Equip.SubHand > 0 then
                CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), tomainhandAction)
            else
                if CommonDefs.IsZhanKuang(CClientMainPlayer.Inst.Class) then
                    CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), tosubhandAction)
                end
            end
        end

        local canQinShanSwitch = false
        EquipBaptize_EquipTransform.ForeachKey(DelegateFactory.Action_object(function (key)
            if item.TemplateId == key then
                canQinShanSwitch = true
            end
        end))
        if canQinShanSwitch then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), qianShanSwitchAction)
        end
        if CPlayerShopMgr.Inst:isAllowToTrade(item) then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), auctionAction)
        end
        --装备鉴定技能
        if item.Equip.Identifiable > 0 then
            local identifyAction = StringActionKeyValuePair(LocalString.GetString("鉴定"), DelegateFactory.Action(function ()
                LuaEquipIdentifySkillMgr.ShowIdentifySkillWnd(item.Id)
            end))

            -- local enableSkillAction = StringActionKeyValuePair(LocalString.GetString("开启技能"), DelegateFactory.Action(function ()
            --     Gac2Gas.IdentifySkillSetDisable(item.Id, false)
            -- end))
            -- local disableSkillAction = StringActionKeyValuePair(LocalString.GetString("禁用技能"), DelegateFactory.Action(function ()
            --      Gac2Gas.IdentifySkillSetDisable(item.Id, true)
            -- end))
            local extendSkillAction = StringActionKeyValuePair(LocalString.GetString("补益技能"), DelegateFactory.Action(function ()
                LuaEquipIdentifySkillMgr.ShowExtendSkillWnd(item.Id)
            end))

            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), identifyAction)

            local skill = Skill_AllSkills.GetData(item.Equip.FixedIdentifySkillId)
            if skill then
                -- if skill.ECategory == SkillCategory.Passive then
                --     if item.Equip.IdentifySkillDisable > 0 then
                --         CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), enableSkillAction)
                --     else
                --         CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), disableSkillAction)
                --     end
                -- end
                if item.Equip.OriginalIdentifyEndTime <= CServerTimeMgr.Inst.timeStamp and item.Equip.SubHand <= 0 then -- 副手武器不显示补益技能
                    CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), extendSkillAction)
                end
            end
        end
        --如果是鱼竿 增加鱼篓按钮
        if HouseFish_PoleType.GetDataBySubKey("TemplateID",item.TemplateId) then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), fishBasketAction)
            --2_5级鱼竿增加拆解
            local pole = HouseFish_PoleType.GetDataBySubKey("TemplateID",item.TemplateId)
            if pole.PoleLevel >=2 then
                CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), fishPoleTakeApartAction)
            end
        end
        
        if not item.IsBinded then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), bindAction)
        end
         CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), discardAction)

        local shareString = CItemMgr.Inst:CheckEquipShare(itemId)
        if shareString and shareString ~= "" and CPersonalSpaceMgr.OpenPersonalSpace then
          local shareAction = StringActionKeyValuePair(LocalString.GetString("分享"), DelegateFactory.Action(function()
              --统一为一个按钮 2018.12.03 cgz
              ShareMgr.ShareWebImage2Other(shareString)
              end))
          CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), shareAction)
        end

        return actionPairs
    else
        local applyAction = StringActionKeyValuePair(LocalString.GetString("使用"), MakeDelegateFromCSFunction(this.OnApply, System.Action, this))
        --吴山石合成，20级顶级
        local wushanshiHeChengAction = StringActionKeyValuePair(LocalString.GetString("合成"), MakeDelegateFromCSFunction(this.ComposeWushanshi, System.Action, this))
        local diamondComposeAction = StringActionKeyValuePair(LocalString.GetString("合成"), MakeDelegateFromCSFunction(this.ComposeDiamond, System.Action, this))
        --宝石合成
        local StoneHeChengAction = StringActionKeyValuePair(LocalString.GetString("合成"), MakeDelegateFromCSFunction(this.ComposeJewel, System.Action, this))
        --法宝碎片合成
        local talismanComposeAction = StringActionKeyValuePair(LocalString.GetString("合成"), MakeDelegateFromCSFunction(this.ComposeTalisman, System.Action, this))
        --铭牌合成
        local dogTagComposeAction = StringActionKeyValuePair(LocalString.GetString("合成"), MakeDelegateFromCSFunction(this.ComposeDogTag, System.Action, this))
        --待鉴定的朱砂笔鉴定
        local evaluateZhushabiAction = StringActionKeyValuePair(LocalString.GetString("鉴定"), MakeDelegateFromCSFunction(this.EvaluateZhushabi, System.Action, this))
        --传家宝开光
        local kaiguangAction = StringActionKeyValuePair(LocalString.GetString("开光"), MakeDelegateFromCSFunction(this.KaiguangChuanjiabao, System.Action, this))
        --传家宝预览
        local previewChuanjiaboModel = StringActionKeyValuePair(LocalString.GetString("预览"), MakeDelegateFromCSFunction(this.PreviewCJBModel, System.Action, this))
		--活动物品预览
		local previewFashionItem = StringActionKeyValuePair(LocalString.GetString("预览"), MakeDelegateFromCSFunction(this.PreviewFashionItemFunction, System.Action, this))
        --烛龙石合成
        local zhulongshiComposeAction = StringActionKeyValuePair(LocalString.GetString("合成"), MakeDelegateFromCSFunction(this.ComposeZhulongshi, System.Action, this))
        ----鲁班经合成
        --local lubanjingComposeAction = StringActionKeyValuePair(LocalString.GetString("合成"), MakeDelegateFromCSFunction(this.ComposeLubanjing, System.Action, this))
		----彩虹碎片合成
		--local rainbowPieceComposeAction = StringActionKeyValuePair(LocalString.GetString("合成"), MakeDelegateFromCSFunction(this.ComposeRainbowPiece, System.Action, this))
		--染色合成
		--local ranseComposeAction = StringActionKeyValuePair(LocalString.GetString("合成"), MakeDelegateFromCSFunction(this.ComposeRanse, System.Action, this))
        local auctionAction = StringActionKeyValuePair(LocalString.GetString("出售"), MakeDelegateFromCSFunction(this.OnSell, System.Action, this))
        local medicineAction = StringActionKeyValuePair(LocalString.GetString("装备"), MakeDelegateFromCSFunction(this.OnMedicineEquip, System.Action, this))
        local summonLingShouAction = StringActionKeyValuePair(LocalString.GetString("召唤灵兽"), MakeDelegateFromCSFunction(this.OnApply, System.Action, this))
        local lianhuaLingShouAction = StringActionKeyValuePair(LocalString.GetString("炼化"), MakeDelegateFromCSFunction(this.OnLianHua, System.Action, this))

        local callAutoDrugWnd = StringActionKeyValuePair(LocalString.GetString("自动"), MakeDelegateFromCSFunction(this.CallAutoDrug, System.Action, this))

        local bindAction = StringActionKeyValuePair(LocalString.GetString("绑定"), MakeDelegateFromCSFunction(this.OnBindItem, System.Action, this))
        local splitAction = StringActionKeyValuePair(LocalString.GetString("拆分"), MakeDelegateFromCSFunction(this.OnSplitItem, System.Action, this))
        local actionPairs = CreateFromClass(MakeGenericClass(List, StringActionKeyValuePair))

        local furnishAction = StringActionKeyValuePair(LocalString.GetString("装修"), MakeDelegateFromCSFunction(this.OnFurnish, System.Action, this))
        local decomposeFurnitureAction = StringActionKeyValuePair(LocalString.GetString("拆解"), MakeDelegateFromCSFunction(this.OnDecomposeFurniture, System.Action, this))

        local openItemBagAction = StringActionKeyValuePair(LocalString.GetString("打开"), MakeDelegateFromCSFunction(this.OnOpenItemBagAction, System.Action, this))
        local quickStoreAction = StringActionKeyValuePair(LocalString.GetString("一键收纳"), MakeDelegateFromCSFunction(this.OnQuickStoreAction, System.Action, this))

        local enterContractAction = StringActionKeyValuePair(LocalString.GetString("进入观看"), MakeDelegateFromCSFunction(this.OnEnterContractAction, System.Action, this))

        local jiandingAction = StringActionKeyValuePair(LocalString.GetString("鉴定"), MakeDelegateFromCSFunction(this.OnApply, System.Action, this))

        local compositeHuamuAction = StringActionKeyValuePair(LocalString.GetString("合成"), MakeDelegateFromCSFunction(this.OnCompositeHuamuAction, System.Action, this))
        local exchangeCropAction = StringActionKeyValuePair(LocalString.GetString("兑换"), MakeDelegateFromCSFunction(this.OnExchangeCrop, System.Action, this))

        local fixCJBAction = StringActionKeyValuePair(LocalString.GetString("修理"), MakeDelegateFromCSFunction(this.OnFixChuanjiabao, System.Action, this))

        local viewZhitiaoAction = StringActionKeyValuePair(LocalString.GetString("查看"), MakeDelegateFromCSFunction(this.OnViewZhitiao, System.Action, this))
        local viewLiquanAction = StringActionKeyValuePair(LocalString.GetString("查看"), MakeDelegateFromCSFunction(this.OnViewLiquan, System.Action, this))
        local useJingnangAction = StringActionKeyValuePair(LocalString.GetString("使用"), MakeDelegateFromCSFunction(this.OnJingNangUse, System.Action, this))

        local zhushabiChaijieAction = StringActionKeyValuePair(LocalString.GetString("拆解"), MakeDelegateFromCSFunction(this.OnZhushabiChaijie, System.Action, this))
        local chuanjiabaoChaijieAction = StringActionKeyValuePair(LocalString.GetString("拆解"), MakeDelegateFromCSFunction(this.OnChuanjiabaoChaijie, System.Action, this))
        local chuanjiabaoWashAction = StringActionKeyValuePair(LocalString.GetString("打造"), MakeDelegateFromCSFunction(this.OnChuanjiabaoChaijie, System.Action, this))

        -- 马匹灵符相关
        local lingfuZhuangbeiAction = StringActionKeyValuePair(LocalString.GetString("装备"), MakeDelegateFromCSFunction(this.OnLingfuZhuangbei, System.Action, this))
        local lingfuXilianCiTiaoAction = StringActionKeyValuePair(LocalString.GetString("洗炼词条"), MakeDelegateFromCSFunction(this.OnLingfuXilianCiTiao, System.Action, this))
        local lingfuXilianTaozhuangAction = StringActionKeyValuePair(LocalString.GetString("洗炼套装"), MakeDelegateFromCSFunction(this.OnLingfuXilianTaozhuang, System.Action, this))

        local discardAction = StringActionKeyValuePair(LocalString.GetString("回收"), MakeDelegateFromCSFunction(this.OnDiscard, System.Action, this))

        --放生镇宅鱼
        local fangShengZhenZhaiYuAction = StringActionKeyValuePair(LocalString.GetString("放生"), DelegateFactory.Action(function ()
                MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("FISH_FREE_CONFIRM"),
                DelegateFactory.Action(function ()
                    Gac2Gas.RequestFangShengZhengzhaiFish(EnumItemPlace.Bag, pos, itemId)
                end),
                nil,
                LocalString.GetString("确定"), LocalString.GetString("取消"), false)
        end))

        --烟花大会拓本合成
        local fireworkHeCheng = StringActionKeyValuePair(LocalString.GetString("合成"), DelegateFactory.Action(function ()
            local msgName = YuanXiao_FireworkPartyHeCheng.GetData(templateId).ConfirmMsg
            MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage(msgName),
            DelegateFactory.Action(function ()
               Gac2Gas.RequestHeChengMeiGuiHuaTaBen(itemId,EnumItemPlace.Bag,pos)
            end),
            nil,
            LocalString.GetString("确定"), LocalString.GetString("取消"), false)
        end))
        --道具获取途径
        local itemGetAccessAction = StringActionKeyValuePair(LocalString.GetString("获取"), DelegateFactory.Action(function ()
            CItemAccessListMgr.Inst:ShowItemAccessInfo(templateId, true, nil, AlignType1.Right)
        end))

        local itemdata = Item_Item.GetData(item.TemplateId)
        if itemdata == nil then
            return nil
        end

		if item:IsItemExpire() then
			 --如果物品不可销毁或者是可以出售给系统的，没有丢弃按钮
    	    if item.Item.CanBeDiscarded then
    	        CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), discardAction)
    	    end
			return actionPairs
		end


        if House_RedeemResource.Exists(item.TemplateId) and CClientHouseMgr.Inst:IsInOwnHouse() then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), exchangeCropAction)
        end

        if CommonDefs.DictContains(Zhuangshiwu_ProducePlant.GetData().ProduceMap, typeof(UInt32), item.Item.TemplateId) then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), compositeHuamuAction)
        elseif item.Item.TemplateId == House_Setting.GetData().ContractItemId or item.Item.TemplateId == House_Setting.GetData().PreciousContractItemId or item.Item.TemplateId == House_Setting.GetData().FamousHouseContract then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), enterContractAction)
        elseif item.Item.Type == EnumItemType_lua.Pet then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), summonLingShouAction)
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), lianhuaLingShouAction)
        elseif item.TemplateId == ShiTu_Setting.GetData().ChuShiTieId then
            local showChuShiPosterWndAction = StringActionKeyValuePair(LocalString.GetString("查看"), DelegateFactory.Action(function (...)
                LuaShiTuMgr:ShowChuShiPosterWnd(itemId)
            end))
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), showChuShiPosterWndAction)
        elseif item.Item.Type == EnumItemType_lua.WushanStone and not (string.find(item.Name, "20", 1, true) ~= nil) then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), applyAction)
            --TODO 鉴定还没做
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), wushanshiHeChengAction)
        elseif item.Item.Type == EnumItemType_lua.Diamond and Jewel_JinGangZuan.GetData(item.TemplateId).Level < 5 then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), applyAction)
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), diamondComposeAction)
        elseif item.Item.Type == EnumItemType_lua.EvaluatedZhushabi then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), applyAction)

            if IsZhuShaBiXiLianOpen() then
                CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), g_ChuanJiaBaoDaZaoAction)
            end

            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), zhushabiChaijieAction)
        elseif item.Item.Type == EnumItemType_lua.UnEvaluateZhushabi then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), evaluateZhushabiAction)
        elseif item.Item.Type == EnumItemType_lua.UnEvaluateChuanjiabao then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), kaiguangAction)
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), previewChuanjiaboModel)
        elseif item.Item.Type == EnumItemType_lua.Chuanjiabao then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), applyAction)
            --打造 6星以上
            if IsZhuShaBiXiLianOpen() then
                CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), g_ChuanJiaBaoDaZaoAction)
            end

            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), previewChuanjiaboModel)
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), fixCJBAction)
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), chuanjiabaoChaijieAction)
        elseif CZuoQiMgr.GetLingfuPosByTemplateId(item.TemplateId) > 0 then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), lingfuZhuangbeiAction)
            local lingfuPos = CZuoQiMgr.GetLingfuPosByTemplateId(item.TemplateId)
            if lingfuPos == 1 or lingfuPos == 3 or lingfuPos == 5 then
                CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), lingfuXilianCiTiaoAction)
            end
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), lingfuXilianTaozhuangAction)
        elseif CZuoQiMgr.IsMaPi(item.TemplateId) and item.TemplateId ~= ZuoQi_Setting.GetData().MapiXiaomaItemId then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), applyAction)
        elseif House_ZhuLongShiCompose.Exists(item.TemplateId) then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), applyAction)
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), zhulongshiComposeAction)
        elseif CCommonItemExchangeMgr.IsComposableLubanjing(item.TemplateId, "RuBanJing") then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), applyAction)
            --CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), lubanjingComposeAction)
            --elseif CCommonItemExchangeMgr.IsComposableLubanjing(item.TemplateId, "ranse") then
            --CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), ranseComposeAction)
        elseif CCommonItemExchangeMgr.IsRainbowPiece(item.TemplateId) then
            --CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), applyAction)
            --CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), rainbowPieceComposeAction)
        elseif Talisman_MakeXianJia.GetData(item.TemplateId) ~= nil then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), talismanComposeAction)
        elseif item.TemplateId == GameSetting_Common.GetData().DogTagItemTempId then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), applyAction)
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), dogTagComposeAction)
        elseif item.Item.Type == EnumItemType_lua.Gem then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), applyAction)
            --TODO 鉴定还没做
            if Jewel_HeCheng.Exists(item.TemplateId) and CPackageView.OpenGemCompose then
                CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), StoneHeChengAction)
            end
        elseif item.Item:IsItemBag() then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), openItemBagAction)
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), quickStoreAction)
        elseif item.Item.Type == EnumItemType_lua.DaijianZhuangshiwu then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), jiandingAction)
        elseif item.Item.Type == EnumItemType_lua.Zhuangshiwu then
            local zswtid = CClientFurnitureMgr.GetZhuangshiwuIdFromItemData(itemdata)
            local zswdata = zswtid and Zhuangshiwu_Zhuangshiwu.GetData(zswtid)

                -- 9064星月之辉不显示使用按钮
                if itemdata.NameColor ~= "COLOR_ORANGE" and CLuaHouseMgr.CanShowUseButtonInPackage(zswdata) then
                    CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), applyAction)
                end

                local bCanFurnish = CClientFurnitureMgr.Inst:CheckIsFurnitureItem(item)
                if bCanFurnish then
                    CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), furnishAction)
                end

			    if zswdata and zswdata.Type ~= EnumFurnitureType_lua.eLingshou and
				    zswdata.Type ~= EnumFurnitureType_lua.ePuppet and
				    zswdata.Type ~= EnumFurnitureType_lua.eMapi and
				    zswdata.Type ~= EnumFurnitureType_lua.eNpc  and
                    zswdata.NoDecompose ~= 1 then
				    CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), decomposeFurnitureAction)
			    end
        elseif CSpringFestivalMgr.Instance:IsXiaozhitiao(item.TemplateId) then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), viewZhitiaoAction)
        elseif CSpringFestivalMgr.Instance:IsChunjieLiquan(item.TemplateId) or CQingQiuMgr.Inst:IsGiftItem(item.TemplateId) then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), viewLiquanAction)
        elseif CSpringFestivalMgr.Instance:IsJingnang(item.TemplateId) then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), useJingnangAction)
        elseif GameplayItem_OfflineItem.Exists(item.TemplateId) then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), g_UseOfflineItemAction)
        elseif not CommonDefs.HashSetContains(GameSetting_Common_Wapper.Inst.HuoYunNotDisplayUsingSet, typeof(UInt32), item.TemplateId) and (not item.Item.IsDuoHunFan or item.Item:GetDuoHunPlayerId() <= 0) and (not CClientHouseMgr.Inst:IsHuLuProduct(item.TemplateId))
			and (not CPackageViewMethodHolder.Is2019Firework(item.TemplateId)) and item.Item.TemplateId ~= LianHua_Setting.GetData().InitedKunXianSuoItemId and item.TemplateId ~= 21006331 and item.TemplateId ~= 21006332 then
                CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), applyAction)
        end

        if Valentine_DriftBottle.GetData().EditedJuanBoItemID == item.TemplateId then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), StringActionKeyValuePair(LocalString.GetString("查看"), DelegateFactory.Action(function ()
                Gac2Gas.RequestViewDriftBottleData(itemId, EnumToInt(this.curPlace), pos)
            end)))
        elseif Valentine_DriftBottle.GetData().ReceivedDriftBottleItemID == item.TemplateId then
            actionPairs = CreateFromClass(MakeGenericClass(List, StringActionKeyValuePair))
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), StringActionKeyValuePair(LocalString.GetString("查看"), DelegateFactory.Action(function ()
                Gac2Gas.RequestViewDriftBottleData(itemId, EnumToInt(this.curPlace), pos)
            end)))
        end

        -- 增加一个查询玩家的接口
        if item:FlagIsSet(EnumItemFlag.NameInExtraVarData) then
            local queryAction = StringActionKeyValuePair(LocalString.GetString("查询"), DelegateFactory.Action(function ()
                local commonItem = CItemMgr.Inst:GetById(g_PackageViewItemId)
                local queryPlayerId = commonItem.Item:GetPlayerIdInExtraVarDataWithName()
                if queryPlayerId ~= 0 then
                    CPlayerInfoMgr.ShowPlayerPopupMenu(queryPlayerId, EnumPlayerInfoContext.MSKK, EChatPanel.Undefined, nil, nil, Vector3.zero, AlignType2.Right)
                end
            end))
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), queryAction)
        end


        CPackageViewMethodHolder.CheckZhuangShiWuExchange(this, item, actionPairs)

        if LuaZhenZhaiYuMgr.CheckCanFangSheng(item.TemplateId) then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), fangShengZhenZhaiYuAction)
        end
        if YuanXiao_FireworkPartyHeCheng.GetData(item.TemplateId) then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), fireworkHeCheng)
        end
        if g_ComposeMgr.CheckCanCompose(item.TemplateId) and not LuaZhenZhaiYuMgr.CheckCanFangSheng(item.TemplateId) then
            CPackageViewMethodHolder.AddComposeActionPairs(actionPairs, this)
        end

		if Item_Preview.GetData(item.TemplateId) ~= nil then
			CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), previewFashionItem)
		end

        --染发剂
        if item.TemplateId >= 21050769 and item.TemplateId <= 21050792 then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), g_RanFaJiPreviewAction)
        end

        if Item_BlindBox.GetData(item.TemplateId) then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), g_BlindBoxPreviewAction)
        end

        if LuaProfessionTransferMgr.IsJueJiSkillBook(item.TemplateId) then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), g_JueJiSkillBookExchangeAction)
        end

        -- 永久时装抽奖
        if LuaFashionLotteryMgr:IsFashionLotteryDisassemble(item.TemplateId, item.IsBinded) then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), g_FashionLotteryDisassembleAction)
        end

        --时装套装预览
        if LuaAppearancePreviewMgr:GetPreviewFashonSuitByItem(item.TemplateId)>0 then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), g_PreviewFashionSuitAction)
        end
        --永久时装交易
        if CommonDefs.IS_CN_CLIENT and CLoginMgr.Inst:IsNetEaseOfficialLogin() then --仅官方渠道支持cbg及转换
            if LuaAppearancePreviewMgr:IsCBGTradeItem(item.TemplateId) then
                CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), g_OpenCBGAction)
                CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), g_ChangeToNoneCbgTradeItemAction)
            elseif LuaAppearancePreviewMgr:IsCBGNoneTradeItem(item.TemplateId) then
                if not item.IsBinded then
                    CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), g_ChangeToCbgTradeItemAction)
                end
            end
        end

        --如果物品已经绑定，没有出售按钮
        --藏宝图(待鉴定)暂时没有出售按钮
        --必须能市场交易

        if CPlayerShopMgr.Inst:isAllowToTrade(item) or (not item.IsBinded and Mall_YuanBaoMarket.Exists(item.TemplateId)) then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), auctionAction)
        end
        if item.Item.Type == EnumItemType_lua.Drug then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), medicineAction)
        end
        --else if (item.Item.Type==(int)EnumItemType.Pet)
        --{
        --    actionPairs.Add(summonLingShouAction);
        --}
        if item.TemplateId == LuaLiangHaoMgr.GetLiangHaoItemTemplateId() then
            --靓号增加兑换按钮
             local exchangeAction = StringActionKeyValuePair(LocalString.GetString("兑换"), DelegateFactory.Action(function ()
                LuaLiangHaoMgr.RequestExchangeLiangHaoItem(item.Id, EnumToInt(this.curPlace), pos)
            end))

            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), exchangeAction)
        end

        if item.Item.Type == EnumItemType_lua.Food then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), callAutoDrugWnd)
        end
        local itemGetData = ItemGet_item.GetData(item.TemplateId)
        if itemGetData and itemGetData.ShowGetButton then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), itemGetAccessAction)
        end
        if not item.IsBinded then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), bindAction)
        end
        if not item.IsBinded and item.Item.IsOverlap and item.Amount > 1 then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), splitAction)
        end

        --如果物品不可销毁或者是可以出售给系统的，没有丢弃按钮
        if item.Item.CanBeDiscarded then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), discardAction)
        end

        return actionPairs
    end
end
CPackageView.m_OnItemLongPress_CS2LuaHook = function (this, itemCell)
    if this.IsInClearUpStatus then
        local item = CItemMgr.Inst:GetById(itemCell.ItemId)
        if item ~= nil then
            CItemInfoMgr.ShowLinkItemInfo(itemCell.ItemId, false, nil, CItemInfoMgr.AlignType.ScreenLeft, 0, 0, 0, 0)
        end
    end
end
CPackageView.m_OnEnable_CS2LuaHook = function (this)
    --TODO 需要考虑一下异步加载过程中可能被引导将窗口隐藏的情形，引导隐藏窗口的方式是否改成窗口透明化
    this:RegisterEvent()
end
CPackageView.m_OnDisable_CS2LuaHook = function (this)
    this:StopInitPackageCoroutine()
    this:UnRegisterEvent()
end
CPackageView.m_OnDestroy_CS2LuaHook = function (this)
    this:RecycleItems()
    CPackageViewItemResource.Inst.Resource = nil
end
CPackageView.m_OnSelectBlueValueChanged_CS2LuaHook = function (this, button)
    if CClientMainPlayer.Inst == nil then
        return
    end
    local itemProp = CClientMainPlayer.Inst.ItemProp
    do
        local i = 0
        while i < this.itemCells.Count do
            local continue
            repeat
                local itemId = itemProp:GetItemAt(this.curPlace, i + 1)
                if not System.String.IsNullOrEmpty(itemId) then
                    local item = CItemMgr.Inst:GetById(itemId)
                    if item == nil then
                        continue = true
                        break
                    end
                    if this.IsInClearUpStatus then
                        --勾选过期、蓝装及以下不包括法宝
                        if (not item.IsBinded and item.IsEquip and not IdPartition.IdIsTalisman(item.TemplateId) and not item.Equip.IsRedOrPurpleEquipment) or (item.IsItem and item:IsItemExpire() and item.Item.CanBeDiscarded) then
                            this.itemCells[i].isChecked = this.selectBlueCheckbox.Selected
                            if this.selectBlueCheckbox.Selected and not CommonDefs.ListContains(this.waitingForClearUpList, typeof(String), itemId) then
                                CommonDefs.ListAdd(this.waitingForClearUpList, typeof(String), itemId)
                            elseif not this.selectBlueCheckbox.Selected and CommonDefs.ListContains(this.waitingForClearUpList, typeof(String), itemId) then
                                CommonDefs.ListRemove(this.waitingForClearUpList, typeof(String), itemId)
                            end
                        end
                    end
                end
                continue = true
            until 1
            if not continue then
                break
            end
            i = i + 1
        end
    end
end
CPackageView.m_OnClearUpButtonClick_CS2LuaHook = function (this, go)
    this:SetPackageStatus(true)
    if this.curPlace ~= EnumItemPlace.Bag then
        this:InitPackage(EnumItemPlace.Bag)
    else
        this:LoadItems()
    end
    this.moneyInfoView.gameObject:SetActive(false)
    this.playerInfoView.gameObject:SetActive(false)
    this.recycleView.gameObject:SetActive(true)
end
CPackageView.m_OnSettingButtonClick_CS2LuaHook = function (this, go)
    CUIManager.ShowUI(CUIResources.AutoPickupWnd)
end
CPackageView.m_OnArrangeButtonClick_CS2LuaHook = function (this, go)
    Gac2Gas.PackageOrder(EnumToInt(this.curPlace))
end
CPackageView.m_OnDiscardButtonClick_CS2LuaHook = function (this, go)

    local importantItems = CreateFromClass(MakeGenericClass(List, CCommonItem))
    local existingPreciousItem = nil
    do
        local i = 0
        while i < this.waitingForClearUpList.Count do
            local continue
            repeat
                local item = CItemMgr.Inst:GetById(this.waitingForClearUpList[i])
                if item == nil then
                    continue = true
                    break
                end
                if item.IsEquip and (item.Equip.IsPurpleEquipment or item.Equip.IsGhostEquipment or item.Equip:IsPrecious()) then
                    existingPreciousItem = item
                    break
                elseif item.IsItem and item.IsPrecious and not item:IsItemExpire() then
                    existingPreciousItem = item
                    break
                end
                if item.IsEquip and item.Equip.NeedConfirmationWhenDiscard then
                    CommonDefs.ListAdd(importantItems, typeof(CCommonItem), item)
                elseif item.IsItem and item.Item.NeedConfirmationWhenDiscard and not item:IsItemExpire() then
                    CommonDefs.ListAdd(importantItems, typeof(CCommonItem), item)
                end
                continue = true
            until 1
            if not continue then
                break
            end
            i = i + 1
        end
    end
    if existingPreciousItem ~= nil then
        MessageWndManager.ShowOKMessage(g_MessageMgr:FormatMessage("Message_Can_not_BatchRecycle_Expensive", existingPreciousItem.ColoredName), nil)
        return
    end
    if importantItems.Count > 0 then
        local builder = StringBuilder()
        if importantItems.Count <= 4 then
            do
                local i = 0
                while i < importantItems.Count do
                    local item = importantItems[i]
                    builder:Append(item.ColoredName)
                    if i < importantItems.Count - 1 then
                        builder:Append(LocalString.GetString("、"))
                    end
                    i = i + 1
                end
            end
        else
            for i = 0, 3 do
                local item = importantItems[i]
                builder:Append(item.ColoredName)
                if i < 3 then
                    builder:Append(LocalString.GetString("、"))
                else
                    builder:Append(LocalString.GetString("等"))
                end
            end
        end
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("Recycle_Confirm", builder:ToString(), importantItems.Count), DelegateFactory.Action(function ()
            this:BatchDiscard()
        end), nil, nil, nil, false)
    else
        this:BatchDiscard()
    end
end
CPackageView.m_OnCancelButtonClick_CS2LuaHook = function (this, go)
    this:SetPackageStatus(false)
    this:LoadItems()
    this.moneyInfoView.gameObject:SetActive(true)
    this.playerInfoView.gameObject:SetActive(true)
    this.recycleView.gameObject:SetActive(false)
end
CPackageView.m_OnSetItemAt_CS2LuaHook = function (this, place, position, oldItemId, newItemId)

    if this.curPlace ~= place or CClientMainPlayer.Inst == nil or position > this.itemCells.Count or position == 0 then
        return
    end
    local itemProp = CClientMainPlayer.Inst.ItemProp
    local itemId = itemProp:GetItemAt(place, position)
    local itemCell = this.itemCells[position - 1]
    if place == EnumItemPlace.Bag then
        --更新包裹页信息
        itemCell:Init(itemId, position > CClientMainPlayer.Inst.ItemProp.BagPlaceSize and position <= CClientMainPlayer.Inst.ItemProp:GetPlaceMaxSize(EnumItemPlace.Bag), this.IsInClearUpStatus, CommonDefs.ListContains(this.waitingForClearUpList, typeof(String), itemId), this.scrollView)
    elseif place == EnumItemPlace.Task then
        --更新任务页信息
        itemCell:Init(itemId, false, false, false, this.scrollView)
    end
    --如果该格子没有物品，取消可能的选中状态
    if System.String.IsNullOrEmpty(itemId) or CItemMgr.Inst:GetById(itemId) == nil then
        itemCell.Selected = false
    end
end
CPackageView.m_OnUpdateItemInfo_CS2LuaHook = function (this, itemId)
    if System.String.IsNullOrEmpty(itemId) then
        return
    end
    local n = this.itemCells.Count
    do
        local i = 0
        while i < n do
            if this.itemCells[i].ItemId == itemId then
                this.itemCells[i]:UpdateItemInfo()
                break
            end
            i = i + 1
        end
    end
end
CPackageView.m_OnEquipDurationUpdate_CS2LuaHook = function (this, place, position)
    if CClientMainPlayer.Inst == nil then
        return
    end
    local itemId = CClientMainPlayer.Inst.ItemProp:GetItemAt(place, position)
    if System.String.IsNullOrEmpty(itemId) then
        return
    end
    local n = this.itemCells.Count
    do
        local i = 0
        while i < n do
            if this.itemCells[i].ItemId == itemId then
                this.itemCells[i]:UpdateItemInfo()
                break
            end
            i = i + 1
        end
    end
end
CPackageView.m_OnItemBagSizeUpdate_CS2LuaHook = function (this)
    this:LoadItems()
end
CPackageView.m_BatchDiscard_CS2LuaHook = function (this)
    if this.curPlace == EnumItemPlace.Bag then
        local positions = CreateFromClass(MakeGenericClass(List, UInt32))
        do
            local i = 0
            while i < this.waitingForClearUpList.Count do
                local itemId = this.waitingForClearUpList[i]
                local item = CItemMgr.Inst:GetById(itemId)
                if item ~= nil and (item.IsEquip or item.Item.CanBeDiscarded) then
                    CommonDefs.ListAdd(positions, typeof(UInt32), CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, itemId))
                end
                --Gac2Gas.RequestRecycleItem((uint)curPlace, CItemMgr.Inst.FindItemPosition(EnumItemPlace.Bag, itemId), itemId);
                --Gac2Gas.RequestDiscardItem((uint)EnumItemPlace.Bag, CItemMgr.Inst.FindItemPosition(EnumItemPlace.Bag, itemId), itemId);
                i = i + 1
            end
        end
        if positions.Count > 0 then
            Gac2Gas.RequestBatchRecycleItemBegin()
            do
                local i = 0
                while i < positions.Count do
                    Gac2Gas.RequestBatchRecycleItem(positions[i])
                    i = i + 1
                end
            end
            Gac2Gas.RequestBatchRecycleItemEnd()
        end
    end
end
CPackageView.m_SetPackageStatus_CS2LuaHook = function (this, clearUp)
    CommonDefs.ListClear(this.waitingForClearUpList)
    this.normalBtnRoot:SetActive(not clearUp)
    this.clearupBtnRoot:SetActive(clearUp)
    this.selectBlueCheckbox.Selected = false
end
CPackageView.m_OnRequestBatchRecycleItemReturn_CS2LuaHook = function (this)
    CommonDefs.ListClear(this.waitingForClearUpList)
end
CPackageView.m_OnDiscard_CS2LuaHook = function (this)

    if this.SelectedItemId == nil then
        return
    end

    local item = CItemMgr.Inst:GetById(this.SelectedItemId)
    if item == nil then
        return
    end

    if not IdPartition.IdIsEquip(item.TemplateId) and not IdPartition.IdIsItem(item.TemplateId) then
        return
    end
    if item.IsEquip then
        if item.Equip.NeedConfirmationWhenDiscard then
            local content = g_MessageMgr:FormatMessage("THROW_EQUIP_CONFIRM", item.ColoredName)
            MessageWndManager.ShowOKCancelMessage(content, DelegateFactory.Action(function ()
                if not item.IsBinded and item.IsPrecious then
                    local text = g_MessageMgr:FormatMessage("Message_Recycle_Expensive_Confirm", item.ColoredName)
                    MessageWndManager.ShowOKCancelMessage(text, DelegateFactory.Action(function ()
                        this:DoDiscard()
                    end), nil, nil, nil, false)
                else
                    this:DoDiscard()
                end
            end), nil, nil, nil, false)
        else
            this:DoDiscard()
        end
    elseif item.IsItem then
        if item.Item.NeedConfirmationWhenDiscard and not item:IsItemExpire() then
            local content = g_MessageMgr:FormatMessage("THROW_ITEM_CONFIRM", item.ColoredName)
            MessageWndManager.ShowOKCancelMessage(content, DelegateFactory.Action(function ()
                if not item.IsBinded and item.IsPrecious then
                    local text = g_MessageMgr:FormatMessage("Message_Recycle_Expensive_Confirm", item.ColoredName)
                    MessageWndManager.ShowOKCancelMessage(text, DelegateFactory.Action(function ()
                        this:DoDiscard()
                    end), nil, nil, nil, false)
                else
                    this:DoDiscard()
                end
            end), nil, nil, nil, false)
        else
            this:DoDiscard()
        end
    end
end
CPackageView.m_DoDiscard_CS2LuaHook = function (this)
    local pos = CClientMainPlayer.Inst.ItemProp:GetItemPos(this.curPlace, this.SelectedItemId)
    if pos > 0 then
        Gac2Gas.RequestRecycleItem(EnumToInt(this.curPlace), pos, this.SelectedItemId)
    end
end
CPackageView.m_OnEquipmentClick_CS2LuaHook = function (this)
    if CClientMainPlayer.Inst == nil or System.String.IsNullOrEmpty(this.SelectedItemId) then
        return
    end
    local pos = CClientMainPlayer.Inst.ItemProp:GetItemPos(this.curPlace, this.SelectedItemId)
    local item = CItemMgr.Inst:GetById(this.SelectedItemId)
    if pos == 0 or item == nil or item.IsItem then
        return
    end
    if not IdPartition.IdIsTalisman(item.TemplateId) then
        local equipInfo = CItemInfo()
        equipInfo.templateId = item.TemplateId
        equipInfo.itemId = this.SelectedItemId
        equipInfo.place = EnumItemPlace.Bag
        equipInfo.pos = pos
        this:OnEquipmentProcess(0, 0, equipInfo)
    else
        if item.Equip.IsNormalTalisman then
            CTalismanWndMgr.BaptizeTalismanExtraWord(this.SelectedItemId)
        else
            CTalismanWndMgr.BaptizeTalismanBasicWord(this.SelectedItemId)
        end
    end
end
CPackageView.m_OnLianHua_CS2LuaHook = function (this)
    local msg = g_MessageMgr:FormatMessage("LSLH_ASK", LingShou_Setting.GetData().LianHuaCost)
    MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function ()
        local item = CItemMgr.Inst:GetById(this.SelectedItemId)
        if item == nil then
            return
        end
        local pos = CClientMainPlayer.Inst.ItemProp:GetItemPos(this.curPlace, this.SelectedItemId)
        if pos == 0 then
            return
        end
        Gac2Gas.LingShouYuanShengLianHua(EnumToInt(this.curPlace), pos, item.Item.Id)
    end), nil, nil, nil, false)
end
CPackageView.m_OnApply_CS2LuaHook = function (this)
    if this.SelectedItemId == nil or CClientMainPlayer.Inst == nil then
        return
    end
    local item = CItemMgr.Inst:GetById(this.SelectedItemId)
    if item == nil then
        return
    end
    local pos = CClientMainPlayer.Inst.ItemProp:GetItemPos(this.curPlace, this.SelectedItemId)
    if pos == 0 then
        return
    end

    SoundManager.Inst:PlayOneShot(SoundManager.UISoundEvents.Equip, Vector3.zero, nil, 0)
    if item.IsItem then
        if item.Item:IsExpire() then
            g_MessageMgr:ShowMessage("ITEM_EXPIRED")
            return
        end
        local template = Item_Item.GetData(item.TemplateId)
        if template == nil then
            --TODO ERROR
            return
        end
        local itemInfo = CItemInfo()
        itemInfo.itemId = item.Id
        itemInfo.place = this.curPlace
        itemInfo.pos = pos
        itemInfo.templateId = item.TemplateId
        if item.TemplateId == Wedding_Setting.GetData().BaZiCardItemId then
            LuaWeddingIterationMgr:OpenBaZiResultWnd(item)
            return
        end
        if item.TemplateId == Wedding_Setting.GetData().CertificationItemId then
            LuaWeddingIterationMgr:OpenCertificationWnd(item)
            return
        end
        --引导特殊处理：那个蛋的ID，就是打开界面的物品
        if item.TemplateId == 21000770 then
            --请求打开选择界面
            Gac2Gas.RequestSelectLingShou(EnumToInt(this.curPlace), pos, this.SelectedItemId)
        elseif item.TemplateId == Constants.BuJingMuItemId or item.TemplateId == Constants.JiuQuZhuItemId then
            Gac2Gas.RequestUseItem(EnumToInt(this.curPlace), pos, this.SelectedItemId, "")
        elseif item.TemplateId == Constants.JiuZhuanQianKunItemId or item.TemplateId == Constants.YiXingHuanDouItemId then
            CLingShouSwitchSkillMgr.Inst.FromTempalteId = item.TemplateId
            CLingShouSwitchSkillMgr.Inst.place = this.curPlace
            CLingShouSwitchSkillMgr.Inst.pos = pos
            CLingShouSwitchSkillMgr.Inst.itemId = this.SelectedItemId
            CLingShouSwitchSkillMgr.Inst:OpenWnd()
        elseif item.TemplateId == GameSetting_Common.GetData().SoulItemId then
            Gac2Gas.RequestFixLostSoulEquip(EnumToInt(this.curPlace), pos, this.SelectedItemId)
            return
        elseif item.TemplateId == System.UInt32.Parse(ZhouMoActivity_ClientSetting.GetData("Need_Item_TemplateId").Value) then
            --前世秘闻录，寻路到白云大师
            CItemInfoMgr.CloseItemInfoWnd()
            CUIManager.CloseUI(CUIResources.PackageWnd)
            CTrackMgr.Inst:FindNPC(System.UInt32.Parse(ZhouMoActivity_ClientSetting.GetData("Npc_Id").Value), System.UInt32.Parse(ZhouMoActivity_ClientSetting.GetData("Map_Id").Value), 0, 0, nil, nil)
        elseif item.TemplateId == CEquipWuZuanReplaceMgr.Inst.WuYaoShenZhuId then
            CEquipWuZuanReplaceMgr.Inst.IsWuZuanReplace = true
            this:OnEquipmentProcess(0, 2, nil)
            return
        elseif item.Item.Type == EnumItemType_lua.WushanStone then
            --this.OnEquipmentProcess();
            this:OnEquipmentProcess(1, 0, nil)
            return
        elseif CPlayerMainStoryMgr.Inst:CheckPlayerMainStoryShareItem(item.TemplateId) then
            return
        elseif Talisman_ResetWord.GetData(1).ResetExtraCost[0] == item.TemplateId then
            CTalismanWndMgr.BaptizeTalismanExtraWord("")
        elseif Talisman_ResetWord.GetData(1).ResetBasicCost[0] == item.TemplateId then
            CTalismanWndMgr.BaptizeTalismanBasicWord("")
        elseif Talisman_Setting.GetData().LianJingItemId == item.TemplateId then
            CTalismanWndMgr.FuzhuXianjia()
        elseif item.Item.Type == EnumItemType_lua.Forge then
			CPackageViewMethodHolder.HandleForgeTypeItem(this, item, itemInfo.place, itemInfo.pos)
            return
        elseif item.Item.Type == EnumItemType_lua.Gem or item.Item.Type == EnumItemType_lua.Diamond then
            this:OnEquipmentProcess(2, 0, nil)
            return
         elseif item.Item.Type == EnumItemType_lua.WenShi then
            CTalismanWndMgr.BaptizeTalismanInlay()
            return
        elseif item.Item.Type == EnumItemType_lua.Map then
            Gac2Gas.RequestCheckUseCangBaoTu(EnumToInt(this.curPlace), pos, this.SelectedItemId)
            return
        elseif item.Item.Type == EnumItemType_lua.PuzzleMap then
            Gac2Gas.RequestCheckUseCangBaoTu(EnumToInt(this.curPlace), pos, this.SelectedItemId)
            --CTreasureMapMgr.Inst.curItemId = this.SelectedItemId;
            --CTreasureMapMgr.RequestUsePuzzleMap();
            return
        elseif CommonDefs.HashSetContains(CShanHeTuMgr.Inst.ShanHeTuTemplateId_V2, typeof(UInt32), item.TemplateId) then
            Gac2Gas.RequestPreCheckYiZhangV2(EnumToInt(this.curPlace), pos, item.Id)
            return
        elseif CommonDefs.HashSetContains(CPresentMgr.Inst.PresentSet, typeof(UInt32), item.TemplateId) then
            CPresentMgr.Inst:CheckPlayerCanUsePresentItem(item, this.curPlace, pos)
            return
        elseif item.Item.Type == EnumItemType_lua.GreenHandGift then
            CItemInfoMgr.CloseItemInfoWnd()
            CWelfareMgr.OpenLvUpAward = true
            CUIManager.ShowUI(CUIResources.WelfareWnd)
            return
        elseif item.Item.IsQianLiYan then
            CQianliyanMgr.OpenQianliyanWnd(itemInfo)
            return
        elseif item.Item.IsDuoHunFan and System.String.IsNullOrEmpty(item.Item.DuoHunTargetName) then
            CFoeListMgr.OpenFoeListWnd(itemInfo)
            return
        elseif item.Item.IsZhaoHunFan then
            Gac2Gas.CanZhaoHun()
            return
        elseif item.Item.IsHunPo then
            CHunPoMgr.OpenHunPoTips(item)
        elseif item.TemplateId == ShuJia_Setting.GetData().FoodDetectItemId then
            CUIManager.ShowUI("SummerLBSItemWnd")
            return
        elseif item.Item.Type == EnumItemType_lua.Chuanjiabao then
            --CHouseMainWnd.CurTabIdx = EHouseMainWndTab.EChuanjiabaoTab;
            --CUIManager.ShowUI(CUIResources.HouseMainWnd);
            CHouseUIMgr.OpenHouseMainWnd(EHouseMainWndTab.EChuanjiabaoTab)
            return
        elseif item.Item.Type == EnumItemType_lua.EvaluatedZhushabi then
            CKaiguangMgr.ShowKaiguangWnd(item.Id)
            return
        elseif CommonDefs.DictContains(Zhuangshiwu_ProducePlant.GetData().AllPotId2Resource, typeof(UInt32), item.TemplateId) then
            -- 如果是花箱尝试找找包裹有没有合成的资源
            this:TryCompositeHuamuByPotId(item)
            return
        elseif item.Item.IsDianYinDaoYangSan then
            local message = g_MessageMgr:FormatMessage("CHANGE_SEX_TIP")
            MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function ()
                Gac2Gas.RequestUseItem(EnumToInt(this.curPlace), pos, this.SelectedItemId, "")
            end), nil, nil, nil, false)
            return
        elseif item.Item.Type == EnumItemType_lua.Gather or item.Item.Type == EnumItemType_lua.Fishing or item.Item.Type == EnumItemType_lua.Pharmacy or item.Item.Type == EnumItemType_lua.Cooking or item.Item.Type == EnumItemType_lua.Craft then
            --打开生活技能界面
            CLivingSkillMgr.Inst.useItemTemplateId = item.Item.TemplateId
            CLivingSkillMgr.Inst:InitExpandState()
            CSkillInfoMgr.ShowLivingSkillWnd()
            return
        elseif Constants.BabyFangShengExpItemId == item.TemplateId then
            CLingShouMgr.Inst.selectedFeedTemplateItem = template.ID
            CItemInfoMgr.CloseItemInfoWnd()
            CLuaLingShouMainWnd.UseRenShenGuo()
            return
        elseif template.Type == EnumItemType_lua.PetItem then
            --当没有灵兽，但是有相关灵兽道具的时候，不能进入lingshou界面
            if CClientMainPlayer.Inst.hasLingShou then
                if CommonDefs.ListContains(CLingShouMgr.Inst.feedTemplateIdList, typeof(UInt32), template.ID) then
                    CLingShouMgr.Inst.selectedFeedTemplateItem = template.ID
                    CItemInfoMgr.CloseItemInfoWnd()
                    CLuaLingShouMainWnd.UseRenShenGuo()
                    return
                elseif CommonDefs.ArrayFindIndex(CLingShouMgr.Inst.ShouMingItemIds, typeof(UInt32), DelegateFactory.Predicate_uint(function (p)
                    return p == template.ID
                end)) >= 0 then
                    CItemInfoMgr.CloseItemInfoWnd()
                    CLuaLingShouMainWnd.UseShouYuan()
                    return
                elseif CLingShouMgr.Inst.washTemplateId == template.ID then
                    CItemInfoMgr.CloseItemInfoWnd()
                    CLuaLingShouMainWnd.UseBiLiuLu()
                    return
                elseif CLingShouMgr.Inst.tiwuTempalteId == template.ID then
                    CItemInfoMgr.CloseItemInfoWnd()
                    CLuaLingShouMainWnd.UseCangHaiShiXin()
                    return
                elseif template.ID == Constants.SiLingWanID then
                    CItemInfoMgr.CloseItemInfoWnd()
                    CLuaLingShouMainWnd.UseSiLingWan()
                    return
                elseif CommonDefs.ListContains(CLingShouMgr.Inst.skillTemplateIdList, typeof(UInt32), template.ID) then
                    CItemInfoMgr.CloseItemInfoWnd()
                    CLuaLingShouMainWnd.UseSkillBook()
                    return
                elseif template.ID == CLingShouMgr.Inst.huarongdanId then
                    --化容丹
                    CItemInfoMgr.CloseItemInfoWnd()

                    CLuaLingShouMgr.m_GetSkin_SelectedPos = pos
                    CUIManager.ShowUI(CLuaUIResources.LingShouGetSkinWnd)
                    return
                elseif CLingShouMgr.IsYirongdan(template.ID) then
                    --易容丹
                    CItemInfoMgr.CloseItemInfoWnd()
                    CLingShouMorphWnd.selectedItem = item
                    CUIManager.ShowUI(CUIResources.LingShouMorphWnd)
                    return
                elseif CLuaLingShouMgr.IsJieBanSkillBook(template.ID) then
                    CItemInfoMgr.CloseItemInfoWnd()
                    CLuaLingShouMainWnd.UseSkillBook()
                    return
                end
            else
                --如果没有灵兽，但是选中的都是相关的道具
                if CommonDefs.ListContains(CLingShouMgr.Inst.feedTemplateIdList, typeof(UInt32), template.ID) or CommonDefs.ArrayFindIndex(CLingShouMgr.Inst.ShouMingItemIds, typeof(UInt32), DelegateFactory.Predicate_uint(function (p)
                    return p == template.ID
                end)) >= 0 or CLingShouMgr.Inst.washTemplateId == template.ID or CLingShouMgr.Inst.tiwuTempalteId == template.ID or template.ID == Constants.SiLingWanID or CommonDefs.ListContains(CLingShouMgr.Inst.skillTemplateIdList, typeof(UInt32), template.ID) or CLingShouMgr.Inst.huarongdanId == template.ID or CLingShouMgr.IsYirongdan(template.ID) then
                    g_MessageMgr:ShowMessage("LINGSHOU_DONT_HAVE")
                    return
                end
            end
        elseif item.TemplateId == ShiTu_PlaySetting.GetData().CaiChengYuItemId then
            CTaskMgr.Inst:TrackToDoTask(ShiTu_PlaySetting.GetData().CaiChengYuTaskId)
            return
        elseif item.TemplateId == ZhaoQin_Setting.GetData().m_WeddingBallItem then
            Gac2Gas.GetZhaoQinItemTargetName(item.Id, EnumItemPlace_lua.Bag, pos)
            return
        elseif item.TemplateId == GameSetting_Common.GetData().ShanHaiJingItemId then
            local taskId = ShiTu_PlaySetting.GetData().ShanHaiJingTaskId
            if CommonDefs.DictContains(CClientMainPlayer.Inst.TaskProp.CurrentTasks, typeof(UInt32), taskId) then
                local currentTask = CommonDefs.DictGetValue(CClientMainPlayer.Inst.TaskProp.CurrentTasks, typeof(UInt32), taskId)
                local location = CTaskMgr.Inst:GetTaskTrackPos(currentTask)
                if CScene.MainScene.SceneTemplateId == location.sceneTemplateId then
                    local trackpos = L10.Engine.Utility.GridPos2PixelPos(CPos(location.targetX, location.targetY))
                    local dist = L10.Engine.Utility.Grid2Pixel(2.0)
                    CTrackMgr.Inst:TaskTrack(location.taskId, location.sceneId, location.sceneTemplateId, 0, trackpos, dist, DelegateFactory.Action(function ()
                        Gac2Gas.RequestUseItem(EnumItemPlace_lua.Bag, pos, item.Id, "")
                        return
                    end), nil)
                    return
                end
            end
        elseif item.TemplateId == CValentineMgr.Inst.m_CollectorItemId then
            CValentineMgr.Inst:ShowExchangeWnd()
            return
        elseif item.TemplateId == CQingQiuMgr.Inst.m_TaoHuaBaoXiaId then
            CQingQiuMgr.Inst:OpenTaoHuaBaoXiaWnd()
            return
        elseif CommonDefs.HashSetContains(GameSetting_Common_Wapper.Inst.MarriageJadeItemSet, typeof(UInt32), item.TemplateId) then
            local message = g_MessageMgr:FormatMessage("JADE_ITEM_CONFIRM", template.ExtraAttribute)

            MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function ()
                Gac2Gas.RequestUseItem(EnumToInt(this.curPlace), pos, this.SelectedItemId, "")
            end), nil, nil, nil, false)
            return
        elseif item.TemplateId == LiuYi_Setting.GetData().PhotoTemplateID then
            local message = g_MessageMgr:FormatMessage("PHOTO_USE_CONFIRM")
            MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function ()
                Gac2Gas.RequestUseItem(EnumToInt(this.curPlace), pos, this.SelectedItemId, "")
            end), nil, nil, nil, false)
            return
		elseif item.TemplateId == Zhuangshiwu_Setting.GetData().RepaireMaterialItemTempId then
			CHouseUIMgr.OpenHouseMainWnd(EHouseMainWndTab.EQishuTab)
            return
        elseif CLuaHouseMgr.IsSkyBoxItem(template) then
            if CClientHouseMgr.Inst:IsInOwnHouse() then
                Gac2Gas.RequestUseItem(EnumToInt(this.curPlace), pos, this.SelectedItemId, "")
            else
                local msg = g_MessageMgr:FormatMessage("SKYBOX_CAN_UNLOCK_AT_HOME")
		        MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function()
			        CClientHouseMgr.Inst:GoBackHome()
		        end), nil, LocalString.GetString("回家"), nil, false)
            end
            return
        elseif item.TemplateId == 21031471 then--直线剧情 道具：彼岸映雪
            LuaZhuJueJuQingMgr.OpenChooseBiAnTailWnd(EnumToInt(this.curPlace), pos, this.SelectedItemId)
            return
        elseif item.TemplateId == 21041451 or item.TemplateId == 21041452 then
            LuaMeiXiangLouMgr.m_LaBaId = item.TemplateId
    	    LuaMeiXiangLouMgr.m_LaBaItemId = this.SelectedItemId
		    LuaMeiXiangLouMgr.m_LaBaPlace = EnumItemPlace_lua.Bag
		    LuaMeiXiangLouMgr.m_LaBaPos = pos
            LuaMeiXiangLouMgr.OpenInviteLaBaWnd(pos,this.SelectedItemId)
            return
    elseif CPackageViewMethodHolder.HandleLiuYiItem(this, item) then
        return
    elseif CPackageViewMethodHolder.HandleCarnivalItem(this, item) then
        return
    elseif CPackageViewMethodHolder.HandleMusicBoxItem(this, itemInfo.itemId, itemInfo.place, itemInfo.pos, itemInfo.templateId) then
        return
	elseif CPackageViewMethodHolder.HandleTokenMoney(this, item, itemInfo.place, itemInfo.pos) then
		return
	elseif CPackageViewMethodHolder.HandleNeedConfirmItem(this, item, itemInfo.place, itemInfo.pos) then
		return
    elseif CPackageViewMethodHolder.HandleKeyItem(this,item) then
        return 
    elseif CPackageViewMethodHolder.HandleChunJie2022HeKaItem(this,item) then
        return
        -- 下面加判断时很容易超过60 upvalues，请谨慎
    else
      local minGrade = nil
      Item_ExpItemUseTip.Foreach(function(key,data)
        if key == item.TemplateId then
          minGrade = data.MinGrade
        end
      end)

      if minGrade and minGrade > CClientMainPlayer.Inst.Level and not CClientMainPlayer.Inst.IsInXianShenStatus then
        if item.TimesGroup > 0 and item.TimesPerDay > 0 then
          local useTimes = CClientMainPlayer.Inst.ItemProp:GetItemUseTimes(item.TimesGroup, item.ItemPeriod)
          local remainTimes = item.TimesPerDay - useTimes
          if remainTimes > 0 then
            local message = g_MessageMgr:FormatMessage("ExpItemUseTip",minGrade)
            MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function ()
              Gac2Gas.RequestUseItem(EnumToInt(this.curPlace), pos, this.SelectedItemId, "")
            end), nil, nil, nil, false)
            return
          end
        end
      end
    end

        local groupId = template.CDGroup
        if CClientMainPlayer.Inst.CooldownProp:IsInCooldown(groupId) then
            local remain = CClientMainPlayer.Inst.CooldownProp:GetServerRemainTime(groupId)
            g_MessageMgr:ShowMessage("ITEM_IN_COOLDOWN_TIME_REMAINING", Utility.CalcCDTimeLeft(remain))
            return
        else
            if not System.String.IsNullOrEmpty(template.HowToUse) then
                MessageWndManager.ShowOKMessage(template.HowToUse, nil)
            else
                local availableAmount = item.Amount
                --判断是否为限制使用物品
                if item.TimesGroup > 0 and item.TimesPerDay > 0 then
                    local useTimes = CClientMainPlayer.Inst.ItemProp:GetItemUseTimes(item.TimesGroup, item.ItemPeriod)
                    local remainTimes = item.TimesPerDay - useTimes

                    if template.TimesPerDayParam > 0 then
                        local timesLimitformulaId = template.TimesPerDayParam
                        local timesLimitFormula = AllFormulas.Action_Formula[timesLimitformulaId] and AllFormulas.Action_Formula[timesLimitformulaId].Formula
                        local timesLimit = timesLimitFormula and timesLimitFormula(nil, nil, {CClientMainPlayer.Inst.Level,CClientMainPlayer.Inst.Class,CClientMainPlayer.Inst.Gender,CClientMainPlayer.Inst.BasicProp.CreateTime,CClientMainPlayer.Inst.ItemProp:GetHolidayWithoutFishingPermanent()})
                        if timesLimit then
                            remainTimes = timesLimit - useTimes
                        end
                    end

                    if remainTimes < 0 then
                        remainTimes = 0
                    end
                    if availableAmount > remainTimes then
                        availableAmount = remainTimes
                    end
                    --TODO 如果服务器端真的可能需要long类型才放得下，这里有转换风险
                end
                if template.BatchUse > 0 and availableAmount > 2 then
                    this:BatchUseItem(availableAmount, EnumToInt(this.curPlace), pos, template)
                else
                    if CommonDefs.DictContains(GameSetting_Common_Wapper.ExpBooks, typeof(UInt32), template.ID) then
                        local msgName = CClientMainPlayer.Inst.HasFeiSheng and "FS_INJECT_EXP_CONFIRM" or "INJECT_EXP_CONFIRM"
                        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage(msgName, CommonDefs.DictGetValue(GameSetting_Common_Wapper.ExpBooks, typeof(UInt32), template.ID)), DelegateFactory.Action(function ()
                            Gac2Gas.RequestUseItem(EnumToInt(this.curPlace), pos, this.SelectedItemId, "")
                        end), nil, nil, nil, false)
                    elseif CClientMainPlayer.Inst.HasFeiSheng and item.Item.IsJingYanDanShu then
                        local msg = g_MessageMgr:FormatMessage("OLD_BOOK_EXP_TIP", nil)
                        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("OLD_BOOK_EXP_TIP", nil), DelegateFactory.Action(function ()
                            Gac2Gas.RequestUseItem(EnumToInt(this.curPlace), pos, this.SelectedItemId, "")
                        end), nil, nil, nil, false)
                    elseif CommonDefs.IS_CN_CLIENT and template.Readable == 1 then
                        CItemInfoMgr.ShowLetterDisplayWnd(EnumToInt(this.curPlace),this.SelectedItemId, pos)
                    elseif CommonDefs.IS_CN_CLIENT and template.Readable == 2 then
                        ShareMgr.ShareWebImage2Other(template.Content)
                    elseif template.ID~=0 and HouseFish_HeMaAward.GetDataBySubKey("CodeItem",template.ID) then--盒马鲜生兑换后的物品
                        local code
                        if item.Item.ExtraVarData then
                            code = item.Item.ExtraVarData.Data
                        end
                        if not code then code = " " end
                        local msg = g_MessageMgr:FormatMessage("HeMaXianSheng_UseTo_CopyCode", code)
                        MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
                            CUICommonDef.clipboardText = code
                        end),nil,LocalString.GetString("复制"),LocalString.GetString("取消"),false)
                    else
                        --如果是海钓物品 加二次确认
                        local hemaData = HouseFish_HeMaAward.GetData(template.ID)
                        if HouseFish_AllFishes.GetData(template.ID) or HouseFish_AllOther.GetData(template.ID) then
                            local otherData = HouseFish_AllOther.GetData(template.ID)
                            if otherData and otherData.Level == 0 then
                                return
                            end
                            MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("FISH_USE_CONFIRM"),
                            DelegateFactory.Action(function ()
                                Gac2Gas.RequestUseItem(EnumToInt(this.curPlace), pos, this.SelectedItemId, "")
                            end),
                            nil,
                            LocalString.GetString("确定"), LocalString.GetString("取消"), false)
                        --盒馬鮮生
                        elseif hemaData and hemaData.CodeItem ~= 0 then
                            MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("HMXS_USE_CONFIRM",item.Name),
                            DelegateFactory.Action(function ()
                                Gac2Gas.RequestUseItem(EnumToInt(this.curPlace), pos, this.SelectedItemId, "")
                            end),
                            nil,
                            LocalString.GetString("确定"), LocalString.GetString("取消"), false)
                        else
                            Gac2Gas.RequestUseItem(EnumToInt(this.curPlace), pos, this.SelectedItemId, "")
                        end
                    end
                end
            end
        end
    else
        --装备
        if item.IsEquip and not item.IsBinded and item.Equip:IsPrecious() and CEquipment.GetMainPlayerIsFit(item.TemplateId, false) then
            local message = g_MessageMgr:FormatMessage("PRECIOUS_EQUIP_CONFIRM", item.ColoredName)

            MessageWndManager.ShowConfirmMessage(message, 5, false, DelegateFactory.Action(function ()
                Gac2Gas.RequestEquipItem(EnumToInt(this.curPlace), pos, this.SelectedItemId)
            end), nil)
        else
            Gac2Gas.RequestEquipItem(EnumToInt(this.curPlace), pos, this.SelectedItemId)
        end
    end
end
CPackageView.m_BatchUseItem_CS2LuaHook = function (this, availableAmount, curPlace, pos, template)
    local availableItemAmount = availableAmount
    local isMap = false
    do
        local i = 0
        while i < WaBao_Setting.GetData().BatchUse_Item.Length do
            if WaBao_Setting.GetData().BatchUse_Item[i] == template.ID then
                isMap = true
                break
            end
            i = i + 1
        end
    end
    if isMap then
        if CClientMainPlayer.Inst.ItemProp:GetEmptyPosCount(EnumItemPlace.Bag) <= 1 then
            g_MessageMgr:ShowMessage("BAG_SPACE_NOT_ENOUGH")
            return
        else
            availableItemAmount = math.floor(math.min(CClientMainPlayer.Inst.ItemProp:GetEmptyPosCount(EnumItemPlace.Bag) - 1, 100))
            availableItemAmount = math.floor(math.min(availableItemAmount, availableAmount))
        end
    end
    CLuaNumberInputMgr.ShowNumInputBox(1, availableItemAmount, availableItemAmount, function (val)
        Gac2Gas.RequestBatchUseItem(curPlace, pos, this.SelectedItemId, val, "")
    end, LocalString.GetString("请输入使用的数量"), -1)
end
CPackageView.m_OnEquipmentProcess_CS2LuaHook = function (this, selectIndex, subIndex, equipInfo)

    local result = CStatusMgr.Inst:CheckConflict(CClientMainPlayer.Inst, EnumEvent.GetId("BagEquipmentOperation"))
    if result ~= nil then
        if result.msgs ~= nil then
            local datas = CreateFromClass(MakeGenericClass(List, Object))
            do
                local i = 1
                while i < result.msgs.Length do
                    CommonDefs.ListAdd(datas, typeof(String), result.msgs[i])
                    i = i + 1
                end
            end
            g_MessageMgr:ShowMessage(result.msgs[0], List2Params(datas))
        end
        return
    end

    CEquipmentProcessMgr.Inst.selectedEquip = equipInfo
    CItemInfoMgr.CloseItemInfoWnd()
    CEquipmentProcessMgr.Show(selectIndex, subIndex)
end
CPackageView.m_OnSell_CS2LuaHook = function (this)
    --TODO 出售
    --TODO 出售
    if this.SelectedItemId == nil then
        return
    end
    local item = CItemMgr.Inst:GetById(this.SelectedItemId)
    if item == nil then
        return
    end
    if CPlayerShopMgr.Inst:isAllowToTrade(item) then
        local protectTime = CPlayerShopMgr.Inst:CheckProtectTime(item)
        if protectTime > 0 then
            g_MessageMgr:ShowMessage("PLAYER_SHOP_ON_PROTECT_TIME", math.ceil(1.0 * protectTime / 60))
            return
        end
        if item.IsItem and item.Item:IsExpire() then
            g_MessageMgr:ShowMessage("ITEM_NOT_ALLOW_TO_TRADE")
            return
        end
        CPlayerShopMgr.Inst:QueryPlayerShopStatus(DelegateFactory.Action(function ()
            if CPlayerShopData.Main.HasPlayerShop and CPlayerShopData.Main.BankruptTime == 0 then
                CPlayerShopData.Main:RequestPlayerShopInfo(DelegateFactory.Action_CPlayerShopData_Dictionary(function (shopData, itemdata)
                    if CPlayerShopData.Main.HasEmptyPlace then
                        local FirstEmptyPlace = 1
                        while CommonDefs.DictContains(itemdata, typeof(UInt32), FirstEmptyPlace) do
                            FirstEmptyPlace = FirstEmptyPlace + 1
                        end
                        local data = CPlayerShopItemData()
                        data.ShopId = CPlayerShopData.Main.PlayerShopId
                        data.Item = item
                        data.OwnerId = CPlayerShopData.Main.OwnerId
                        data.OwnerName = CPlayerShopData.Main.OwnerName
                        data.Count = item.IsItem and item.Item.Count or 1
                        data.ShelfPlace = FirstEmptyPlace
                        data.PackagePlace = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, item.Id)
                        data.Price = 100
                        data.FocusCount = 0
                        data.IsFocus = false
                        CPlayerShopMgr.ShowShelfWnd(data)
                    else
                        g_MessageMgr:ShowMessage("BAGGAGE_IS_FULL")
                    end
                end), RequestReason.GetData)
            else
                g_MessageMgr:ShowMessage("NO_PLAYER_SHOP")
            end
        end))
    elseif item.IsItem and item.Item.isSell2Market then
        CYuanbaoMarketMgr.SellItemInYuanBaoMarket(item.Id)
    end
end

CPackageView.m_OnDisassemble_CS2LuaHook = function(this)
    if not CLuaEquipMgr.CanDisassembleEquip(this.curPlace, this.SelectedItemId) then
        return
    end
    local item = CItemMgr.Inst:GetById(this.SelectedItemId)

    -- 额外装备处理
    if item and item.IsEquip and item.Equip.IsExtraEquipment then
        if item.Equip.ForgeTotalValue == 0 then
            -- 请求消耗
            local place, pos = CLuaEquipMgr:GetItemPlaceAndPos(this.SelectedItemId)
            Gac2Gas.QueryDisassembleExtraEquipCostAndItems(place, pos, this.SelectedItemId)
        else
            if not CQuanMinPKMgr.Inst.m_IsQuanMinPKServer then
                local msg = g_MessageMgr:FormatMessage("Cant_Disassemble_Forged_Equipment")

                g_MessageMgr:ShowOkCancelMessage(msg, function()
                    CEquipmentProcessMgr.Inst.selectedEquip = CItemMgr.Inst:GetItemInfo(this.SelectedItemId)
                    CItemInfoMgr.CloseItemInfoWnd()
                    CEquipmentProcessMgr.Show(0, 1)

                    -- 弹重置界面
                    -- 请求重置
                    local place, pos = CLuaEquipMgr:GetItemPlaceAndPos(this.SelectedItemId)
                    Gac2Gas.QueryForgeResetExtraEquipCostAndItems(place, pos, this.SelectedItemId)
                end, nil, nil, nil, false)
            else
                g_MessageMgr:ShowMessage("NOT_ALLOW_IN_QUANMINPK")
            end
        end
        return
    end

    if CSwitchMgr.EnableGhostDisassemble then
        if item ~= nil and item.Equip.IsGhost then
            CLuaEquipMgr.GhostDisassembleEquip = this.SelectedItemId
            if CLuaEquipMgr.EnableVersion2 then
                CUIManager.ShowUI(CLuaUIResources.GhostEquipDisassembleWnd)
            else
                CUIManager.ShowUI(CUIResources.GhostEquipWordDisassembleWnd)
            end
            return
        end
    end

    if item ~= nil and item.IsEquip then
        if CLuaEquipMgr.EnableVersion2 then --神兵三阶新材料开启
            if item.Equip.WordsCount >= 6 and item.Equip.Color == EnumQualityType.Purple then --6个词条以上的紫色装备要打开界面
                CLuaEquipMgr.DisassembleItemId = this.SelectedItemId
                CUIManager.ShowUI(CLuaUIResources.EquipAdvDisassembleWnd)
                return
            end
        end
    end

    if item ~= nil and item.IsEquip and not item.IsBinded and item.IsPrecious then
        local text = g_MessageMgr:FormatMessage("Message_Recycle_Expensive_Confirm", item.ColoredName)
        MessageWndManager.ShowOKCancelMessage(
            text,
            DelegateFactory.Action(
                function()
                    CLuaEquipMgr.RequestDisassembleEquip(this.curPlace, this.SelectedItemId, false,false)
                end
            ),
            nil,
            nil,
            nil,
            false
        )
    else
        CLuaEquipMgr.RequestDisassembleEquip(this.curPlace, this.SelectedItemId, false,false)
    end
end

CPackageView.m_OnChuanjiabaoChaijie_CS2LuaHook = function (this)
    if CClientMainPlayer.Inst ~= nil then
        local pos = CClientMainPlayer.Inst.ItemProp:GetItemPos(this.curPlace, this.SelectedItemId)
        local item = CItemMgr.Inst:GetById(this.SelectedItemId)

        if pos > 0 and item ~= nil then
            local info = item.Item.ChuanjiabaoItemInfo
            local confirmStarNum = House_Setting.GetData().ChaijieConfirmStar
            local confirmWordNum = House_Setting.GetData().ChaijieConfirmWordNum
            local cost = House_Setting.GetData().ChaijieCost
            if info.Star >= confirmStarNum or info.WordInfo.WordNum >= confirmWordNum then
                local msg = g_MessageMgr:FormatMessage("Chuanjiabao_Fenjie_ZhenPin_Confirm")
                MessageWndManager.ShowDelayOKCancelMessage(msg, DelegateFactory.Action(function ()
                    local msg1 = g_MessageMgr:FormatMessage("Chuanjiabao_Fenjie_Cost_Confirm")
                    MessageWndManager.ShowOKCancelMessage(msg1, DelegateFactory.Action(function ()
                        Gac2Gas.RequestChaijieChuanjiabao(EnumToInt(this.curPlace), pos, this.SelectedItemId)
                    end), nil, nil, nil, false)
                end), nil, 3)
            else
                local msg1 = g_MessageMgr:FormatMessage("Chuanjiabao_Fenjie_Cost_Confirm")
                MessageWndManager.ShowOKCancelMessage(msg1, DelegateFactory.Action(function ()
                    Gac2Gas.RequestChaijieChuanjiabao(EnumToInt(this.curPlace), pos, this.SelectedItemId)
                end), nil, nil, nil, false)
            end
        end
    end
end
CPackageView.m_OnLingfuZhuangbei_CS2LuaHook = function (this)
    CZuoQiMgr.TabIndex = 2
    CUIManager.ShowUI(CUIResources.VehicleMainWnd)
end
CPackageView.m_OnLingfuXilianCiTiao_CS2LuaHook = function (this)

    CLingfuBaptizeWnd.sCurBaptizeLingfuId = this.SelectedItemId
    CLingfuBaptizeWnd.sType = EnumLingfuBaptizeType.eWord
    CUIManager.CloseUI(CIndirectUIResources.ItemInfoWnd)
    CUIManager.ShowUI(CUIResources.LingfuBaptizeWnd)
    local item = CItemMgr.Inst:GetById(this.SelectedItemId)
end
CPackageView.m_OnLingfuXilianTaozhuang_CS2LuaHook = function (this)
    CLingfuBaptizeWnd.sCurBaptizeLingfuId = this.SelectedItemId
    CLingfuBaptizeWnd.sType = EnumLingfuBaptizeType.eTaozhuang
    CUIManager.CloseUI(CIndirectUIResources.ItemInfoWnd)
    CUIManager.ShowUI(CUIResources.LingfuBaptizeWnd)
end
CPackageView.m_OnMapiPreview_CS2LuaHook = function (this)
    local item = CItemMgr.Inst:GetById(this.SelectedItemId)
    if item ~= nil then
        local prop = CMapiProp()
        if prop:LoadFromString(item.Item.ExtraVarData.Data) then
            CZuoQiMgr.Inst:ShowMapiPreview(prop)
        end
    end
end
CPackageView.m_OnZhushabiChaijie_CS2LuaHook = function (this)
    if CClientMainPlayer.Inst ~= nil then
        local pos = CClientMainPlayer.Inst.ItemProp:GetItemPos(this.curPlace, this.SelectedItemId)
        local item = CItemMgr.Inst:GetById(this.SelectedItemId)

        if pos > 0 and item ~= nil then
            if item.IsPrecious then
                local msg = g_MessageMgr:FormatMessage("ZhuShaBi_Fenjie_ZhenPin_Confirm")
                MessageWndManager.ShowDelayOKCancelMessage(msg, DelegateFactory.Action(function ()
                    Gac2Gas.RequestChaijieZhushabiDetails(EnumToInt(this.curPlace), pos, this.SelectedItemId)
                end), nil, 3)
            else
                Gac2Gas.RequestChaijieZhushabiDetails(EnumToInt(this.curPlace), pos, this.SelectedItemId)
            end
        end
    end
end
CPackageView.m_OnTalismanChaiJie_CS2LuaHook = function (this)
    if CClientMainPlayer.Inst ~= nil then
        local pos = CClientMainPlayer.Inst.ItemProp:GetItemPos(this.curPlace, this.SelectedItemId)
        if pos > 0 then
            Gac2Gas.RequestChaiJieFaBao(EnumToInt(this.curPlace), pos)
        end
    end
end

CPackageView.m_OnRepair_CS2LuaHook = function (this)
    if CClientMainPlayer.Inst ~= nil then
        local pos = CClientMainPlayer.Inst.ItemProp:GetItemPos(this.curPlace, this.SelectedItemId)
        if pos > 0 then
            Gac2Gas.RequestRepairEquip(EnumToInt(this.curPlace), pos)
        end
    end
end
CPackageView.m_OnBindItem_CS2LuaHook = function (this)
    if CClientMainPlayer.Inst ~= nil then
        local pos = CClientMainPlayer.Inst.ItemProp:GetItemPos(this.curPlace, this.SelectedItemId)
        if pos > 0 then
            MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("Binding_Or_Not"), DelegateFactory.Action(function ()
                Gac2Gas.RequestBindItem(EnumToInt(this.curPlace), pos, this.SelectedItemId)
            end), nil, nil, nil, false)
        end
    end
end
CPackageView.m_OnEquipTalisman_CS2LuaHook = function (this)
    local item = CItemMgr.Inst:GetById(this.SelectedItemId)
--    if CClientMainPlayer.Inst.MaxLevel >= item.Equip.NeedLevel then
        CItemInfoMgr.CloseItemInfoWnd()
        CTalismanWndMgr.EquipTalisman(this.SelectedItemId, 0)
--    else
--        g_MessageMgr:ShowMessage("EQUIPMENT_GRADE_NOT_MATCH")
--    end
end
CPackageView.m_OnUpgradeXianjia_CS2LuaHook = function (this)
    CItemInfoMgr.CloseItemInfoWnd()
    CTalismanWndMgr.UpgradeXianjia(this.SelectedItemId)
end
CPackageView.m_OnQinShanSwitch_CS2LuaHook = function (this)
    if CClientMainPlayer.Inst ~= nil then
        local pos = CClientMainPlayer.Inst.ItemProp:GetItemPos(this.curPlace, this.SelectedItemId)
        local item = CItemMgr.Inst:GetById(this.SelectedItemId)
        if pos > 0 and item ~= nil and item.IsEquip then
            Gac2Gas.RequesetCanQianZhuanShan(EnumToInt(this.curPlace), pos, this.SelectedItemId)
        end
    end
end
CPackageView.m_OnFixLostSoul_CS2LuaHook = function (this)
    if CClientMainPlayer.Inst ~= nil then
        local pos = CClientMainPlayer.Inst.ItemProp:GetItemPos(this.curPlace, this.SelectedItemId)
        local item = CItemMgr.Inst:GetById(this.SelectedItemId)
        if pos > 0 and item ~= nil and item.IsEquip and item.Equip.IsLostSoul == 1 then
            local serverNow = CServerTimeMgr.Inst:GetZone8Time()
            local endTime = CServerTimeMgr.ConvertTimeStampToZone8Time(item.Equip.LostSoulTime):AddDays(item.Equip.LostSoulDuration)
            if CommonDefs.op_GreaterThan_DateTime_DateTime(endTime, serverNow) then
                local hasRelatedSoulItem = this:CheckHasRelatedSoulItem(item)
                if hasRelatedSoulItem then
                    Gac2Gas.RequestFixLostSoulEquip(EnumToInt(this.curPlace), pos, this.SelectedItemId)
                else
                    local ts = endTime:Subtract(serverNow)
                    local remainDays = math.max(ts.Days, math.ceil(ts.TotalDays))
                    MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("FIX_LOST_SOUL_CONFIRM", item.Equip.LostSoulPricePerDay * remainDays), DelegateFactory.Action(function ()
                        Gac2Gas.RequestFixLostSoulEquip(EnumToInt(this.curPlace), pos, this.SelectedItemId)
                    end), nil, nil, nil, false)
                end
            end
        end
    end
end
CPackageView.m_CheckHasRelatedSoulItem_CS2LuaHook = function (this, item)
    local itemProp = CClientMainPlayer.Inst.ItemProp
    local bagSize = itemProp:GetPlaceSize(EnumItemPlace.Bag)

    do
        local i = 1
        while i <= bagSize do
            local id = itemProp:GetItemAt(EnumItemPlace.Bag, i)
            if not System.String.IsNullOrEmpty(id) then
                local commonItem = CItemMgr.Inst:GetById(id)
                if commonItem ~= nil and commonItem.TemplateId == GameSetting_Common.GetData().SoulItemId then
                    local data = CommonDefs.TypeAs(MsgPackImpl.unpack(commonItem.Item.ExtraVarData.Data), typeof(MakeGenericClass(List, Object)))
                    if data ~= nil and data.Count >= 6 then
                        local lostSoulTime = math.floor(tonumber(data[2]))
                        local equipId = tostring(data[5])

                        if (equipId == this.SelectedItemId) and lostSoulTime == item.Equip.LostSoulTime then
                            return true
                        end
                    end
                end
            end
            i = i + 1
        end
    end
    return false
end
CPackageView.m_OnSplitItem_CS2LuaHook = function (this)
    if CClientMainPlayer.Inst ~= nil then
        local pos = CClientMainPlayer.Inst.ItemProp:GetItemPos(this.curPlace, this.SelectedItemId)
        local item = CItemMgr.Inst:GetById(this.SelectedItemId)
        if pos > 0 and item ~= nil and (not item.IsBinded) and item.IsItem and item.Item.IsOverlap and item.Amount > 1 then
            CLuaNumberInputMgr.ShowNumInputBox(1, item.Amount - 1, 1,function (val)
                if val > 0 and val < item.Amount then
                    Gac2Gas.RequestSplitItem(EnumToInt(this.curPlace), pos, this.SelectedItemId, val)
                end
            end, LocalString.GetString("请输入需要拆分的数量"), -1)
        end
    end
end
CPackageView.m_OnDecomposeFurniture_CS2LuaHook = function (this)
    CItemInfoMgr.CloseItemInfoWnd()
	CLuaFurnitureExchangeWnd.m_SelectedItemId = this.SelectedItemId
    CLuaClientFurnitureMgr.ShowTableType = 2 --包裹
	CUIManager.ShowUI(CUIResources.FurnitureExchangeWnd)

end
CPackageView.m_OnFurnish_CS2LuaHook = function (this)
    CItemInfoMgr.CloseItemInfoWnd()

    if not CClientHouseMgr.Inst:IsInOwnHouse() then
        g_MessageMgr:ShowMessage("OPERATION_NEED_IN_OWN_HOUSE")
        return
    end

    if CClientMainPlayer.Inst ~= nil then
        local pos = CClientMainPlayer.Inst.ItemProp:GetItemPos(this.curPlace, this.SelectedItemId)
        local item = CItemMgr.Inst:GetById(this.SelectedItemId)

        if pos > 0 and item ~= nil then
            local data = Item_Item.GetData(item.TemplateId)
            if data ~= nil and data.Type == EnumItemType_lua.Zhuangshiwu then
                local extraString = data.ExtraAttribute
                if (string.find(extraString, "zhuangshiwu", 1, true) ~= nil) then
                    local number = CommonDefs.StringSubstring1(extraString, 12)
                    local zswTid
                    local default
                    default, zswTid = System.UInt32.TryParse(number)
                    if default then
                        local zswData = Zhuangshiwu_Zhuangshiwu.GetData(zswTid)
                        if zswData ~= nil then
                            if data.NameColor == "COLOR_ORANGE" or data.NameColor == "COLOR_BLUE" then
                                Gac2Gas.ConvertItem2RepositoryFurniture(EnumItemPlace_lua.Bag, pos, item.Id)
                            else
                                local color = "FFFFFF"
                                if data.NameColor == "COLOR_RED" then
                                    color = NGUIText.EncodeColor24(QualityColor.GetRGBValue(EnumQualityType.Red))
                                elseif data.NameColor == "COLOR_PURPLE" then
                                    color = NGUIText.EncodeColor24(QualityColor.GetRGBValue(EnumQualityType.Purple))
                                end

                                MessageWndManager.ShowOKCancelMessage(System.String.Format(LocalString.GetString("确认将 [c][{0}]{1} [-][/c]放入装修仓库吗？该物品将消失，并获得装修仓库内的一个装饰物。"), color, data.Name), DelegateFactory.Action(function ()
                                    Gac2Gas.ConvertItem2RepositoryFurniture(EnumItemPlace_lua.Bag, pos, item.Id)
                                end), nil, nil, nil, false)
                            end
                        end
                    end
                end
            end
        end
    end
end
CPackageView.m_OnOpenItemBagAction_CS2LuaHook = function (this)
    local item = CItemMgr.Inst:GetById(this.SelectedItemId)
    local pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, this.SelectedItemId)
    if item == nil or pos == 0 then
        return
    end
    CItemBagMgr.Inst:ShowItemBag(this.SelectedItemId)
end
CPackageView.m_OnCompositeHuamuAction_CS2LuaHook = function (this)
    local item = CItemMgr.Inst:GetById(this.SelectedItemId)
    local pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, this.SelectedItemId)
    if item == nil or pos == 0 then
        return
    end
    CItemInfoMgr.CloseItemInfoWnd()
    CProductCompositeWnd.ResouceItemTempId = item.TemplateId
    CUIManager.ShowUI(CUIResources.ProductCompositeWnd)
end
CPackageView.m_OnExchangeCrop_CS2LuaHook = function (this)
    CUIManager.ShowUI(CUIResources.PlantExchangeWnd)
end
CPackageView.m_TryCompositeHuamuByPotId_CS2LuaHook = function (this, item)
    local potId2Res = Zhuangshiwu_ProducePlant.GetData().AllPotId2Resource
    if not CommonDefs.DictContains(potId2Res, typeof(UInt32), item.TemplateId) then
        return
    end
    local resList = CommonDefs.DictGetValue(potId2Res, typeof(UInt32), item.TemplateId).ResourceId
    do
        local i = 0
        while i < resList.Length do
            local itemTempId = resList[i]
            local pos = 0
            local itemId = nil
            local default
            default, pos, itemId = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, itemTempId)
            if default then
                CProductCompositeWnd.ResouceItemTempId = itemTempId
                CUIManager.ShowUI(CUIResources.ProductCompositeWnd)
                return
            end
            i = i + 1
        end
    end

    g_MessageMgr:ShowMessage("RESOURCE_NUM_NOT_ENOUGH")
end
CPackageView.m_OnEnterContractAction_CS2LuaHook = function (this)
    local item = CItemMgr.Inst:GetById(this.SelectedItemId)
    local pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, this.SelectedItemId)
    if item == nil or pos == 0 then
        return
    end
    CItemInfoMgr.CloseItemInfoWnd()
    Gac2Gas.RequestEnterContract(EnumItemPlace_lua.Bag, pos, this.SelectedItemId)
end
CPackageView.m_OnFixChuanjiabao_CS2LuaHook = function (this)
    local item = CItemMgr.Inst:GetById(this.SelectedItemId)
    local pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, this.SelectedItemId)
    if item == nil or pos == 0 then
        return
    end
    CItemInfoMgr.CloseItemInfoWnd()
    Gac2Gas.RequestFixChuanjiabaoItem(EnumItemPlace_lua.Bag, pos, this.SelectedItemId)
end
CPackageView.m_OnViewZhitiao_CS2LuaHook = function (this)
    local item = CItemMgr.Inst:GetById(this.SelectedItemId)
    CSpringFestivalMgr.Instance.CurrentZhitiaoID = item.TemplateId
    CUIManager.ShowUI("SpringFestivalZhitiaoWnd")
end
CPackageView.m_OnJingNangUse_CS2LuaHook = function (this)
    CSpringFestivalMgr.Instance:tryUseJingnang(this.SelectedItemId)
end

CPackageView.m_OnViewLiquan_CS2LuaHook = function (this)
    local item = CItemMgr.Inst:GetById(this.SelectedItemId)
    if not item.Item:IsExpire() then
        if CQingQiuMgr.Inst:IsGiftItem(item.TemplateId) then
            CQingQiuMgr.Inst.m_TicketItemId = item.Id
            Gac2Gas.VerifyGetCinemaTicketMobile(item.Id)
        elseif CSpringFestivalMgr.Instance:IsChunjieLiquan(item.TemplateId) then
            CSpringFestivalMgr.Instance.CurrentLiquanID = item.Id
            CUIManager.ShowUI("SpringFestivalGiftWnd")
        end
    else
        g_MessageMgr:ShowMessage("ITEM_EXPIRED")
    end
end
CPackageView.m_OnQuickStoreAction_CS2LuaHook = function (this)
    local item = CItemMgr.Inst:GetById(this.SelectedItemId)
    local pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, this.SelectedItemId)
    if item == nil or pos == 0 then
        return
    end
    CItemInfoMgr.CloseItemInfoWnd()
    Gac2Gas.MoveAllItemsToItem(pos)
end
CPackageView.m_CallAutoDrug_CS2LuaHook = function (this)
    CItemInfoMgr.CloseItemInfoWnd()
    CUIManager.ShowUI(CUIResources.AutoDrugWnd)
end
CPackageView.m_OnMedicineEquip_CS2LuaHook = function (this)
    if this.SelectedItemId == nil then
        return
    end

    local item = CItemMgr.Inst:GetById(this.SelectedItemId)
    if item == nil then
        return
    end
    CItemInfoMgr.CloseItemInfoWnd()
    Gac2Gas.SetHpDrugShortcut(item.Item.TemplateId)
end
CPackageView.m_OnPreSellDone_CS2LuaHook = function (this, itemId, maxCount, price)
    if CUICommonDef.PreSellContext ~= PreSellContextType.PackageWnd then
        return
    end
    local message = g_MessageMgr:FormatMessage("MARKET_PRESELL_DONE", price)
    CLuaNumberInputMgr.ShowNumInputBox(1, maxCount, maxCount, function (count)
        Gac2Gas.SellMarketItem(itemId, count)
    end, message, 15)
end
CPackageView.m_ComposeTalisman_CS2LuaHook = function (this)
    CItemInfoMgr.CloseItemInfoWnd()
    CUIManager.ShowUI(CUIResources.XianjiaComposeWnd)
end
CPackageView.m_ComposeDogTag_CS2LuaHook = function (this)
    Gac2Gas.RequestCompositeDaMingPai(this.SelectedItemId)
end
CPackageView.m_EvaluateZhushabi_CS2LuaHook = function (this)
    CZhushabiEvaluateMgr.ShowEvaluateWnd(this.SelectedItemId)
end
CPackageView.m_KaiguangChuanjiabao_CS2LuaHook = function (this)
    CKaiguangMgr.ShowKaiguangWnd(this.SelectedItemId)
end
CPackageView.m_PreviewCJBModel_CS2LuaHook = function (this)
    CCJBMgr.Inst:OpenTipShow(this.SelectedItemId, true)
end
CPackageView.m_ComposeWushanshi_CS2LuaHook = function (this)
    local item = CItemMgr.Inst:GetById(this.SelectedItemId)
    CItemComposeMgr.ComposeItem = CItemMgr.Inst:GetItemTemplate(item.TemplateId)
    CItemComposeMgr.ComposeItemType = EnumComposeItemType.wushan
    CItemInfoMgr.CloseItemInfoWnd()
    CEquipmentProcessMgr.Show(3, 0)
end
CPackageView.m_ComposeDiamond_CS2LuaHook = function (this)
    local item = CItemMgr.Inst:GetById(this.SelectedItemId)
    CItemComposeMgr.ComposeItem = CItemMgr.Inst:GetItemTemplate(item.TemplateId)
    CItemComposeMgr.ComposeItemType = EnumComposeItemType.diamond
    CItemInfoMgr.CloseItemInfoWnd()
    CEquipmentProcessMgr.Show(3, 0)
end
CPackageView.m_ComposeJewel_CS2LuaHook = function (this)
    local item = CItemMgr.Inst:GetById(this.SelectedItemId)
    CItemComposeMgr.ComposeItem = CItemMgr.Inst:GetItemTemplate(item.TemplateId)
    CItemComposeMgr.ComposeItemType = EnumComposeItemType.jewery
    CItemInfoMgr.CloseItemInfoWnd()
    CEquipmentProcessMgr.Show(3, 0)
end
CPackageView.m_ComposeZhulongshi_CS2LuaHook = function (this)
    local item = CItemMgr.Inst:GetById(this.SelectedItemId)
    if item ~= nil then
        CCommonItemExchangeMgr.ShowZhulongshiComposeWnd(item.TemplateId)
    end
end
CPackageView.m_ComposeLubanjing_CS2LuaHook = function (this)
    local item = CItemMgr.Inst:GetById(this.SelectedItemId)
    if item ~= nil then
        CCommonItemExchangeMgr.ShowItemExchangeWnd(item.TemplateId, "RuBanJing", LocalString.GetString("鲁班经合成"))
    end
end
CPackageView.m_ComposeRainbowPiece_CS2LuaHook = function (this)
    local item = CItemMgr.Inst:GetById(this.SelectedItemId)
    if item ~= nil then
        CCommonItemExchangeMgr.ShowItemExchangeWnd(item.TemplateId, "jiaoren", LocalString.GetString("鲛人合成"))
    end
end
CPackageView.m_ComposeRanse_CS2LuaHook = function (this)
	local item = CItemMgr.Inst:GetById(this.SelectedItemId)
	if item ~= nil then
		CCommonItemExchangeMgr.ShowItemExchangeWnd(item.TemplateId, "ranse", LocalString.GetString("染色合成"))
	end
end
CPackageView.m_ComposeGhostEquipWord_CS2LuaHook = function (this)

    local pos = CClientMainPlayer.Inst.ItemProp:GetItemPos(this.curPlace, this.SelectedItemId)
    local item = CItemMgr.Inst:GetById(this.SelectedItemId)
    if pos == 0 or item == nil or item.IsItem then
        return
    end

    if item.Equip.IsLostSoul > 0 then
        g_MessageMgr:ShowMessage("Double_EquipWords_Fail_IsShiHun")
        return
    end

    local equipInfo = CItemInfo()
    equipInfo.templateId = item.TemplateId
    equipInfo.itemId = this.SelectedItemId
    equipInfo.place = EnumItemPlace.Bag
    equipInfo.pos = pos


    CEquipmentProcessMgr.Inst.selectedEquipForGhostCompose = equipInfo

    CUIManager.ShowUI(CUIResources.GhostEquipWordComposeWnd)
end

CPackageView.m_HasNextEquip_CS2LuaHook = function (this, itemId, templateId)
    if CClientMainPlayer.Inst == nil or System.String.IsNullOrEmpty(itemId) or this.curPlace ~= EnumItemPlace.Bag then
        return false
    end
    local pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, itemId)
    local item = CItemMgr.Inst:GetById(itemId)
    if pos == 0 or item == nil or not item.IsEquip then
        return false
    end
    local n = CClientMainPlayer.Inst.ItemProp:GetPlaceSize(EnumItemPlace.Bag)
    local template = EquipmentTemplate_Equip.GetData(item.TemplateId)
    do
        local i = pos + 1
        while i < n do
            local id = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Bag, i)
            local equip = CItemMgr.Inst:GetById(id)
            if equip ~= nil and equip.IsEquip then
                if EquipmentTemplate_Equip.GetData(equip.TemplateId).Type == template.Type then
                    return true
                end
            end
            i = i + 1
        end
    end
    return false
end
CPackageView.m_HasPreviousEquip_CS2LuaHook = function (this, itemId, templateId)
    if CClientMainPlayer.Inst == nil or System.String.IsNullOrEmpty(itemId) or this.curPlace ~= EnumItemPlace.Bag then
        return false
    end
    local pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, itemId)
    local item = CItemMgr.Inst:GetById(itemId)
    if pos == 0 or item == nil or not item.IsEquip then
        return false
    end
    local template = EquipmentTemplate_Equip.GetData(item.TemplateId)
    for i = pos - 1, 1, - 1 do
        local id = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Bag, i)
        local equip = CItemMgr.Inst:GetById(id)
        if equip ~= nil and equip.IsEquip then
            if EquipmentTemplate_Equip.GetData(equip.TemplateId).Type == template.Type then
                return true
            end
        end
    end
    return false
end
CPackageView.m_OnMoveToNextEquip_CS2LuaHook = function (this, itemId, templateId)
    if CClientMainPlayer.Inst == nil or System.String.IsNullOrEmpty(itemId) or this.curPlace ~= EnumItemPlace.Bag then
        return false
    end
    local pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, itemId)
    local item = CItemMgr.Inst:GetById(itemId)
    if pos == 0 or item == nil or not item.IsEquip then
        return false
    end
    local n = CClientMainPlayer.Inst.ItemProp:GetPlaceSize(EnumItemPlace.Bag)
    local template = EquipmentTemplate_Equip.GetData(item.TemplateId)
    do
        local i = pos + 1
        while i < n do
            local id = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Bag, i)
            local equip = CItemMgr.Inst:GetById(id)
            if equip ~= nil and equip.IsEquip then
                if EquipmentTemplate_Equip.GetData(equip.TemplateId).Type == template.Type and i <= this.itemCells.Count then
                    this:OnItemClick(this.itemCells[i - 1])
                    return true
                end
            end
            i = i + 1
        end
    end
    return false
end
CPackageView.m_OnMoveToPreviousEquip_CS2LuaHook = function (this, itemId, templateId)
    if CClientMainPlayer.Inst == nil or System.String.IsNullOrEmpty(itemId) or this.curPlace ~= EnumItemPlace.Bag then
        return false
    end
    local pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, itemId)
    local item = CItemMgr.Inst:GetById(itemId)
    if pos == 0 or item == nil or not item.IsEquip then
        return false
    end
    local template = EquipmentTemplate_Equip.GetData(item.TemplateId)
    for i = pos - 1, 1, - 1 do
        local id = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Bag, i)
        local equip = CItemMgr.Inst:GetById(id)
        if equip ~= nil and equip.IsEquip then
            if EquipmentTemplate_Equip.GetData(equip.TemplateId).Type == template.Type and i <= this.itemCells.Count then
                this:OnItemClick(this.itemCells[i - 1])
                return true
            end
        end
    end
    return false
end


CPackageViewMethodHolder = class()
function CPackageViewMethodHolder.HandleLiuYiItem(this, item)
    if item.TemplateId == 21030255 then
        -- 超大泡泡水
        if CClientMainPlayer.Inst.Target == nil then
            CNearbyPlayerWndMgr.ShowWnd(LocalString.GetString("使用神奇泡泡棒，需要先选中使用对象哦！"), LocalString.GetString("选择"), DelegateFactory.Action_ulong(function (playerId)
                local target = CClientPlayerMgr.Inst:GetPlayer(playerId)
                if target ~= nil then
                    CClientMainPlayer.Inst.Target = target
                    this:OnApply()
                    if CUIManager.IsLoaded(CIndirectUIResources.ItemInfoWnd) then
                        CUIManager.CloseUI(CIndirectUIResources.ItemInfoWnd)
                    end
                    if CUIManager.IsLoaded(CUIResources.PackageWnd) then
                        CUIManager.CloseUI(CUIResources.PackageWnd)
                    end
                end
            end))
            return true
        else
            return false
        end
    end
    return false
end

function CPackageViewMethodHolder.HandleCarnivalItem(this, item)
    if item.TemplateId == 21030306 or item.TemplateId == 21030307 then
        CUIManager.ShowUI(CLuaUIResources.CarnivalCollectWnd)
        return true
    end
    -- 2019复用
    if item.TemplateId == 21030813 or item.TemplateId == 21030814 then
      CUIManager.ShowUI(CLuaUIResources.CarnivalCollectWnd)
      return true
    end
    -- 2020复用
    if item.TemplateId == 21031206 or item.TemplateId == 21031207 then
      CUIManager.ShowUI(CLuaUIResources.CarnivalCollectWnd)
      return true
    end
    -- 2021复用
    if item.TemplateId == JiaNianHua_Setting.GetData().FreeStampId or item.TemplateId == JiaNianHua_Setting.GetData().StampId then
        CUIManager.ShowUI(CLuaUIResources.CarnivalCollectWnd)
        return true
      end
    return false
end

function CPackageViewMethodHolder.HandleMusicBoxItem(this, itemId, place, pos, templateId)
  if templateId == StarBiWuShow_MusicBoxSetting.GetData().DiskItemTemplateIdBeforeRecord then
    CLuaMusicBoxMgr:OpenRecordWnd(itemId, place, pos)
    return true
  end
  return false
end

function CPackageViewMethodHolder.HandleTokenMoney(this, item, place, pos)
	if item.Item.Type == EnumItemType_lua.TokenMoney then
		local template = Item_Item.GetData(item.TemplateId)
		local message = g_MessageMgr:FormatMessage("TOKENMONEY_USE_CONFIRM", template and template.ExtraAttribute or "")
		MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function ()
			Gac2Gas.RequestUseItem(EnumToInt(place), pos, item.Id, "")
		end), nil, nil, nil, false)
		return true
	end
	return false
end

function CPackageViewMethodHolder.HandleNeedConfirmItem(this, item, place, pos)
	if item.TemplateId == 21031234 then
		local message = g_MessageMgr:FormatMessage("YUEGUANGBAOXIA_USE_CONFIRM")
		MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function ()
			Gac2Gas.RequestUseItem(EnumToInt(place), pos, item.Id, "")
		end), nil, nil, nil, false)
		return true
	elseif LuaZongMenMgr:IsAntiprofessionItem(item.TemplateId) then
        local buffId = SoulCore_Settings.GetData().AntiProfession_BuffId
        if CClientMainPlayer.Inst ~= nil and CommonDefs.DictContains(CClientMainPlayer.Inst.BuffProp.Buffs, typeof(UInt32), buffId) then
            local buffObj = CommonDefs.DictGetValue(CClientMainPlayer.Inst.BuffProp.Buffs, typeof(UInt32), buffId)
            local timeStr = ""
            if buffObj.EndTime < 0 then
                timeStr = SafeStringFormat3(LocalString.GetString("永久"))
            else
                local ts = CommonDefs.op_Subtraction_DateTime_DateTime(CServerTimeMgr.Inst:GetZone8Time(), buffObj.receivedTime)
                if ts.TotalSeconds >= buffObj.EndTime or ts.TotalSeconds < 0 then
                    timeStr = SafeStringFormat3(LocalString.GetString("0分0秒") )
                else
                    local totalLeftSeconds = math.floor((buffObj.EndTime - ts.TotalSeconds))
                    if totalLeftSeconds >= 86400 then
                        timeStr = SafeStringFormat3(LocalString.GetString("%d天%d小时"), math.floor(totalLeftSeconds / 86400), math.floor((totalLeftSeconds % 86400) / 3600))
                    elseif totalLeftSeconds >= 3600 then
                        timeStr = SafeStringFormat3(LocalString.GetString("%d小时%d分"), math.floor(totalLeftSeconds / 3600), math.floor((totalLeftSeconds % 3600) / 60))
                    else
                        timeStr = SafeStringFormat3(LocalString.GetString("%d分%d秒"), math.floor(totalLeftSeconds / 60), totalLeftSeconds % 60)
                    end
                end
            end
            local message = g_MessageMgr:FormatMessage("KeFu_ItemUseConfirm", timeStr)
            MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function ()
                Gac2Gas.RequestUseItem(EnumToInt(place), pos, item.Id, "")
            end), nil, nil, nil, false)
            return true
        end
	elseif Expression_LockGroup.GetDataBySubKey("ItemID", item.TemplateId) and Mall_LingYuMall.Exists(item.TemplateId) then
		-- 已解锁表情动作重复使用返还灵玉二次确认
		local designData = Expression_LockGroup.GetDataBySubKey("ItemID", item.TemplateId)
		if CExpressionActionMgr.Inst.UnLockGroupInfo[designData.LockGroupID] and CExpressionActionMgr.Inst.UnLockGroupInfo[designData.LockGroupID] == 1 then
			local lingyuMall = Mall_LingYuMall.GetData(item.TemplateId)
			local message = g_MessageMgr:FormatMessage("Expression_ReturnJade_Confirm", item.Name, lingyuMall.Jade)
			MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function()
				Gac2Gas.RequestUseItem(EnumToInt(place), pos, item.Id, "")
			end), nil, nil, nil, false)
			return true
		end
	elseif Expression_Appearance.GetDataBySubKey("ItemId", item.TemplateId) and Mall_LingYuMall.Exists(item.TemplateId) then
		-- 已解锁永久表情外观重复使用返还灵玉二次确认
		local designData = Expression_Appearance.GetDataBySubKey("ItemId", item.TemplateId)
		if designData and designData.AvailableTime <= 0 and CClientMainPlayer.Inst then
			local appearanceData = CClientMainPlayer.Inst.PlayProp.ExpressionAppearanceData
			if CommonDefs.DictContains(appearanceData, typeof(Byte), designData.Group) then
				local appearances = CommonDefs.DictGetValue(appearanceData, typeof(Byte), designData.Group).Appearances
				if CommonDefs.DictContains(appearances, typeof(UInt32), designData.ID) then
					local info = CommonDefs.DictGetValue(appearances, typeof(UInt32), designData.ID)
					if info.IsExpired == 0 then
						local lingyuMall = Mall_LingYuMall.GetData(item.TemplateId)
						local message = g_MessageMgr:FormatMessage("ExpressionAppearance_ReturnJade_Confirm", item.Name, lingyuMall.Jade)
						MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function()
							Gac2Gas.RequestUseItem(EnumToInt(place), pos, item.Id, "")
						end), nil, nil, nil, false)
						return true
					end
				end
			end
		end
    end
	return false
end

function CPackageViewMethodHolder.HandleStarBiwuItem(this, item)
  if item.TemplateId == StarBiWuShow_Setting.GetData().QinYouItemID then
    local CInputBoxMgr = import "L10.UI.CInputBoxMgr"
    CInputBoxMgr.ShowInputBox(LocalString.GetString("请输入亲友团成员的角色ID"), DelegateFactory.Action_string(function (val)
      local playerId = tonumber(val)
      if playerId then
        local found, pos, id = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, item.TemplateId)
        Gac2Gas.StarBiwuInviteAudience(playerId, item.TemplateId, item.Id, EnumItemPlace.Bag, pos)
      else
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("请输入正确的角色ID"))
      end
    end), 20, true, nil, LocalString.GetString("请输入角色ID"))
    return true
  end
  return false
end

function CPackageViewMethodHolder.HandleForgeTypeItem(this, item, place, pos)
	if item.Item.Type == EnumItemType_lua.Forge then
		local template = Item_Item.GetData(item.TemplateId)
		local templateId = item.TemplateId
		--锤炼、支机石
		if EquipBaptize_ChuiLian.Exists(templateId) then
			CLuaEquipZhouNianQingMgr.m_ChuiLianItemId=templateId
			CUIManager.ShowUI(CUIResources.EquipChuiLianWnd)
		elseif templateId==CLuaEquipZhouNianQingMgr.m_ZhiJiShiItemId then
			CUIManager.ShowUI(CUIResources.EquipZhiJiShiUseWnd)
		elseif CommonDefs.HashSetContains(IdentifySkill_Settings.GetData().IdentifyItemTemplateIds, typeof(uint), templateId) then
			MessageWndManager.ShowOKMessage(template.HowToUse, nil)
		elseif  LuaEquipIdentifySkillMgr.IsEquipIdentifyCostItem(templateId) then
			MessageWndManager.ShowOKMessage(template.HowToUse, nil)
		elseif IdentifySkill_Settings.GetData().JiuQuZhuItemTemplateId == templateId then
			MessageWndManager.ShowOKMessage(template.HowToUse, nil)
		elseif templateId == IdentifySkill_Settings.GetData().QiGongItemTemplateId then
			MessageWndManager.ShowOKMessage(template.HowToUse, nil)
		elseif templateId == 21000891 or templateId == 21000892 then
			this:OnEquipmentProcess(0, 2, nil)
		elseif CSwitchMgr.EnableJingPingScore and (templateId == Constants.JingPing_Red or templateId == Constants.JingPing_Blue) then
			if not System.String.IsNullOrEmpty(template.HowToUse) then
				MessageWndManager.ShowOKMessage(template.HowToUse, nil)
			else
				local availableAmount = item.Amount
				if template.BatchUse > 0 and availableAmount > 2 then
					this:BatchUseItem(availableAmount, EnumToInt(place), pos, template)
				else
					Gac2Gas.RequestUseItem(EnumToInt(place), pos, item.Id, "")
				end
			end
        elseif CLuaEquipMgr:IsForgeItem(templateId) then
            -- 装备锻造
            local itemInfo = CLuaEquipMgr:TryGetExtraEquip()
            this:OnEquipmentProcess(0, 1, itemInfo)
        elseif CLuaEquipMgr:IsCompositeItem(templateId) then
            -- 装备熔炼
            local itemInfo = CLuaEquipMgr:TryGetExtraEquip()
            this:OnEquipmentProcess(0, 2, itemInfo)
		else
			this:OnEquipmentProcess(0, 0, nil)
		end
		return true
	end
	return false
end

function CPackageViewMethodHolder.Is2019Firework(itemTempId)
	local NewYear2019 = import "L10.Game.NewYear2019_Setting"
	return NewYear2019.GetData().NormalFirework == itemTempId or NewYear2019.GetData().AdvancedFirework == itemTempId
end

function CPackageViewMethodHolder.GetTaskActionPairs(this, itemId, templateId)
    if CClientMainPlayer.Inst == nil or System.String.IsNullOrEmpty(itemId) then
        return nil
    end

    --注意！！！任务道具只能查看，不要做实质性的使用操作，道具的使用交给任务功能来触发和消耗

    local pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Task, itemId)
    local mItem = CItemMgr.Inst:GetById(itemId)
    local taskItemActions = CreateFromClass(MakeGenericClass(List, StringActionKeyValuePair))
    if mItem ~= nil and pos ~= 0 and mItem.IsItem then
        local itemdata = Item_Item.GetData(mItem.TemplateId)

        local cityWarZhanLongItemTable = {
        [21030384] = DelegateFactory.Action(function ( ... ) Gac2Gas.RequestUseBombForCityWarZhanLong(itemId, pos) end),
        [21030385] = DelegateFactory.Action(function ( ... ) Gac2Gas.RequestDamageQiXieForCityWarZhanLong(itemId, pos) end),
        [21030386] = DelegateFactory.Action(function ( ... ) Gac2Gas.RequestOpenGrabWndForCityWarZhanLong(itemId, pos) end),
        [21030393] = DelegateFactory.Action(function ( ... ) Gac2Gas.RequestRepairWallForCityWarZhanLong(itemId, pos) end),
        [21030394] = DelegateFactory.Action(function ( ... ) Gac2Gas.RequestRepairQiXieForCityWarZhanLong(itemId, pos) end),
        [21030468] = DelegateFactory.Action(function ( ... ) Gac2Gas.RequestSummonMonsterForCityWarZhanLong(itemId, pos) end),
        [21030469] = DelegateFactory.Action(function ( ... ) Gac2Gas.RequestSummonSpyNpcForCityWarZhanLong(itemId, pos) end),}

        if cityWarZhanLongItemTable[mItem.TemplateId] then
            local applyAction = StringActionKeyValuePair(LocalString.GetString("使用"), cityWarZhanLongItemTable[mItem.TemplateId])
            CommonDefs.ListAdd(taskItemActions, typeof(StringActionKeyValuePair), applyAction)
        elseif mItem.TemplateId == CLuaCityWarMgr.ZhanLongFlagTemplateId then
            --战旗
            local setContentAction = StringActionKeyValuePair(LocalString.GetString("设置"), DelegateFactory.Action(function ( ... )
                local CInputBoxMgr = import "L10.UI.CInputBoxMgr"
                CInputBoxMgr.ShowInputBox(LocalString.GetString("请输入战旗标语"), DelegateFactory.Action_string(function (content)
                    local ret = CWordFilterMgr.Inst:DoFilterOnSendWithoutVoiceCheck(content, false)
                    if ret.msg == nil or ret.shouldBeIgnore then
                        g_MessageMgr:ShowMessage("QMPK_Name_Slogan_Violation")
                        return
                    end
                    Gac2Gas.RequestSetFlagContentForCityWarZhanLong(itemId, pos, content)
                end), 30, true, nil, nil)
            end))
            CommonDefs.ListAdd(taskItemActions, typeof(StringActionKeyValuePair), setContentAction)
            local applyAction = StringActionKeyValuePair(LocalString.GetString("使用"), DelegateFactory.Action(function ( ... )
                Gac2Gas.RequestPlaceFlagForCityWarZhanLong(itemId, pos)
            end))
            CommonDefs.ListAdd(taskItemActions, typeof(StringActionKeyValuePair), applyAction)
        --任务包裹里查看结婚证书
        elseif mItem.TemplateId == Wedding_Setting.GetData().CertificationItemId then
            local checkCertificationAction = StringActionKeyValuePair(LocalString.GetString("查看"), MakeDelegateFromCSFunction(this.OnApply, System.Action, this))
            CommonDefs.ListAdd(taskItemActions, typeof(StringActionKeyValuePair), checkCertificationAction)
        elseif mItem.TemplateId == ShiTu_Setting.GetData().BaiShiTieId then
            local showBaiShiPosterWndAction = StringActionKeyValuePair(LocalString.GetString("查看"), DelegateFactory.Action(function (...)
                LuaShiTuMgr:ShowBaiShiPosterWnd(itemId)
            end))
            CommonDefs.ListAdd(taskItemActions, typeof(StringActionKeyValuePair), showBaiShiPosterWndAction)
        elseif mItem.TemplateId == ShiTuCultivate_Setting.GetData().CultivateManualItemId then
            local openShiTuTrainingHandbookAction = StringActionKeyValuePair(LocalString.GetString("查看"), DelegateFactory.Action(function (...)
                local mainPlayer = CClientMainPlayer.Inst
                if mainPlayer then
                    if mainPlayer.IsCrossServerPlayer then
                        g_MessageMgr:ShowMessage("CROSS_SERVER_NOT_USE_THE_FEATURE")
                        return
                    end
                    local t = {}
                    CommonDefs.DictIterate(mainPlayer.RelationshipProp.TuDi, DelegateFactory.Action_object_object(function(key,val)
                        if val.Finished == 0 then
                            table.insert(t,val)
                        end
                    end))
                    if #t > 1 then
                        if LuaShiTuMgr.m_OpenNewSystem then
                            LuaShiTuTrainingHandbookMgr:ShowWndForTeacher()
                        else
                            LuaShiTuTrainingHandbookMgr:ShowTuDiPlayerListWnd(LocalString.GetString("培养"), function(playerId)
                                LuaShiTuTrainingHandbookMgr:ShowWndForTeacher(playerId)
                            end)
                        end
                    elseif #t == 1 then
                        local playerId = t[1].PlayerId
                        LuaShiTuTrainingHandbookMgr:ShowWndForTeacher(playerId)
                    end
                end

            end))
            CommonDefs.ListAdd(taskItemActions, typeof(StringActionKeyValuePair), openShiTuTrainingHandbookAction)
            if LuaShiTuMgr.m_OpenNewSystem then
                local openShiTuMainWndAction = StringActionKeyValuePair(LocalString.GetString("师徒事宜"), DelegateFactory.Action(function (...)
                    LuaShiTuMgr:OpenShiTuMainWnd()
                end))
                CommonDefs.ListAdd(taskItemActions, typeof(StringActionKeyValuePair), openShiTuMainWndAction)
            end

        elseif mItem.TemplateId == ShiTuCultivate_Setting.GetData().ApprenticeCultivateManualItemId then
            local openShiTuTrainingHandbookAction = StringActionKeyValuePair(LocalString.GetString("查看"), DelegateFactory.Action(function (...)
                LuaShiTuTrainingHandbookMgr:ShowWndForStudent()
            end))
            CommonDefs.ListAdd(taskItemActions, typeof(StringActionKeyValuePair), openShiTuTrainingHandbookAction)
            if LuaShiTuMgr.m_OpenNewSystem then
                local openShiTuMainWndAction = StringActionKeyValuePair(LocalString.GetString("师徒事宜"), DelegateFactory.Action(function (...)
                    LuaShiTuMgr:OpenShiTuMainWnd()
                end))
                CommonDefs.ListAdd(taskItemActions, typeof(StringActionKeyValuePair), openShiTuMainWndAction)
            end
        elseif itemdata.Readable == 2 and itemdata.Content ~= nil and itemdata.Content ~= "" and CPersonalSpaceMgr.OpenPersonalSpace then
            --查看显示分享图片
            --至少要开启梦岛分享才有效
            local shareAction = StringActionKeyValuePair(LocalString.GetString("分享"), DelegateFactory.Action(function()
                --统一分享 2018.12.03 cgz
                ShareMgr.ShareWebImage2Other(itemdata.Content)
                end))
            CommonDefs.ListAdd(taskItemActions, typeof(StringActionKeyValuePair), shareAction)
        elseif itemdata.Readable == 1 and itemdata.Content ~= nil and itemdata.Content ~= "" and (CommonDefs.IS_CN_CLIENT or CommonDefs.IS_HMT_CLIENT) then
            --国服查看显示书信效果
            --这里和任务机制使用道具会冲突，和策划讨论后先屏蔽任务栏里的这种处理
            -- local showLetterAction = StringActionKeyValuePair(LocalString.GetString("查看"), DelegateFactory.Action(function()
            --         CItemInfoMgr.ShowLetterDisplayWnd(EnumToInt(this.curPlace),this.SelectedItemId, pos)
            --     end))
            -- CommonDefs.ListAdd(taskItemActions, typeof(StringActionKeyValuePair), showLetterAction)
        end

        --可丢弃的任务物品，和服务器怕判断逻辑保持一致，不考虑CanBeDiscarded
        if CommonDefs.HashSetContains(GameSetting_Common_Wapper.Inst.RecycleTaskItem, typeof(UInt32), templateId) then
            local discardAction = StringActionKeyValuePair(LocalString.GetString("丢弃"), DelegateFactory.Action(function()

                local content = g_MessageMgr:FormatMessage("RecycleTaskItem_Corfirm")
                MessageWndManager.ShowOKCancelMessage(content, DelegateFactory.Action(function ()
                    Gac2Gas.RequestRecycleTaskItem(pos, itemId)
                end), nil, nil, nil, false)
            end))
            CommonDefs.ListAdd(taskItemActions, typeof(StringActionKeyValuePair), discardAction)
        end
    end
    return taskItemActions
end

function CPackageViewMethodHolder.AddComposeActionPairs(actionPairs, this)
    CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair),
            StringActionKeyValuePair(LocalString.GetString("合成"),
                    DelegateFactory.Action(function()
                        local item = CItemMgr.Inst:GetById(this.SelectedItemId)
                        if item ~= nil then
                            local can, key, wndTitle = g_ComposeMgr.CheckCanCompose(item.TemplateId)
                            CCommonItemExchangeMgr.ShowItemExchangeWnd(item.TemplateId, key, wndTitle)
                        end
                    end)
            )
    )
end

function CPackageViewMethodHolder.HandleKeyItem(this,item)
    if item.TemplateId == GameplayItem_Setting.GetData().TianJiangBaoXiang_ChiTong_ItemId then
        Gac2Gas.TianJiangBaoXiang_OpenWnd(tostring(EnumTianJiangBaoXiangDoor.ChiTong))
        return true
    elseif item.TemplateId == GameplayItem_Setting.GetData().TianJiangBaoXiang_FeiCui_ItemId then
        Gac2Gas.TianJiangBaoXiang_OpenWnd(tostring(EnumTianJiangBaoXiangDoor.FeiCui))
        return true
    end
    return false
end
--春节2022贺卡
function CPackageViewMethodHolder.HandleChunJie2022HeKaItem(this,item)
    if LuaChunJie2022Mgr.IsHeKa(item.TemplateId) and item.Item.ExtraVarData and item.Item.ExtraVarData.Data then
        local data = MsgPackImpl.unpack(item.Item.ExtraVarData.Data)
        if data and data.Count >= 6 then
            --content targetId name class gender
            local t = {}
            if data.Count>5 then
                t.content = tostring(data[0])
                t.id = tonumber(data[1])
                t.name = tostring(data[2])
                t.class = tostring(data[3])
                t.gender = tostring(data[4])
                t.time = tonumber(data[5])
            end

            LuaChunJie2022HeKaWnd.s_Content = t
            CUIManager.ShowUI(CLuaUIResources.ChunJie2022HeKaWnd)
        end
        return true
    end
    return false
end

-- 废旧家具材料合成
function CPackageViewMethodHolder.CheckZhuangShiWuExchange(this, item, actionPairs)
    if item.TemplateId ~= Zhuangshiwu_Setting.GetData().ZhuangShiWuExchangeItemId then  return   end

    local nowTimeStamp = CServerTimeMgr.Inst.timeStamp
    local EndTime = Zhuangshiwu_Setting.GetData().ZhuangShiWuExchangeEndTime
    local EndTimeInfo = g_LuaUtil:StrSplit(EndTime,"-")
    local EndTimeString = EndTimeInfo[1].."-"..EndTimeInfo[2].."-"..EndTimeInfo[3].." "..EndTimeInfo[4]..":"..EndTimeInfo[5]
    local EndTimeStamp = CServerTimeMgr.Inst:GetTimeStampByStr(EndTimeString)
    if nowTimeStamp <= EndTimeStamp then  
        CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair),
            StringActionKeyValuePair(LocalString.GetString("合成"),
                DelegateFactory.Action(function()
                    local item = CItemMgr.Inst:GetById(this.SelectedItemId)
                    if item ~= nil then
                        CUIManager.ShowUI(CLuaUIResources.ItemExchange1To1Wnd)
                    end
                end)
            )
        )
    end
end

--------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------
g_PeiyuAction = StringActionKeyValuePair(LocalString.GetString("培育"), DelegateFactory.Action(function()
    --打开培育对话框
    -- CUIManager.ShowUI(CLuaUIResources.QMPKJiXiangWuWnd)
    if g_PackageViewItemId == nil then
        return
    end
    local itemInfo=CItemMgr.Inst:GetItemInfo(g_PackageViewItemId)
    if itemInfo == nil then
        return
    end

    Gac2Gas.RequestUseItem(EnumToInt(itemInfo.place), itemInfo.pos, g_PackageViewItemId, "")
end))
g_DiscardAction = StringActionKeyValuePair(LocalString.GetString("回收"), DelegateFactory.Action(function()
    if g_PackageViewItemId == nil then
        return
    end

    local item = CItemMgr.Inst:GetById(g_PackageViewItemId)
    if item == nil then
        return
    end

    if not IdPartition.IdIsEquip(item.TemplateId) and not IdPartition.IdIsItem(item.TemplateId) then
        return
    end
    local discard=function()
        local itemInfo=CItemMgr.Inst:GetItemInfo(g_PackageViewItemId)
        if itemInfo then
            Gac2Gas.RequestRecycleItem(EnumToInt(itemInfo.place), itemInfo.pos, g_PackageViewItemId)
        end
    end
    if item.IsEquip then
        if item.Equip.NeedConfirmationWhenDiscard then
            local content = g_MessageMgr:FormatMessage("THROW_EQUIP_CONFIRM", item.ColoredName)
            MessageWndManager.ShowOKCancelMessage(content, DelegateFactory.Action(function ()
                if not item.IsBinded and item.IsPrecious then
                    local text = g_MessageMgr:FormatMessage("Message_Recycle_Expensive_Confirm", item.ColoredName)
                    MessageWndManager.ShowOKCancelMessage(text, DelegateFactory.Action(function ()
                        discard()
                    end), nil, nil, nil, false)
                else
                    discard()
                end
            end), nil, nil, nil, false)
        else
            discard()
        end
    elseif item.IsItem then
        if item.Item.NeedConfirmationWhenDiscard and not item:IsItemExpire()  then
            local content = g_MessageMgr:FormatMessage("THROW_ITEM_CONFIRM", item.ColoredName)
            MessageWndManager.ShowOKCancelMessage(content, DelegateFactory.Action(function ()
                if not item.IsBinded and item.IsPrecious then
                    local text = g_MessageMgr:FormatMessage("Message_Recycle_Expensive_Confirm", item.ColoredName)
                    MessageWndManager.ShowOKCancelMessage(text, DelegateFactory.Action(function ()
                        discard()
                    end), nil, nil, nil, false)
                else
                    discard()
                end
            end), nil, nil, nil, false)
        else
            discard()
        end
    end
end))

g_UseOfflineItemAction = StringActionKeyValuePair(LocalString.GetString("使用"), DelegateFactory.Action(function()
    CLuaOfflineItemWnd.m_ItemId = g_PackageViewItemId
    CUIManager.ShowUI(CLuaUIResources.OfflineItemWnd)
    CItemInfoMgr.CloseItemInfoWnd()
end))

g_WeddingRingTabenAction = StringActionKeyValuePair(LocalString.GetString("拓本"), DelegateFactory.Action(function()
    CItemInfoMgr.CloseItemInfoWnd()
    CLuaWeddingMgr.ShowWeddingRingTabenWnd(g_PackageViewItemId)
end))

g_OpenYaoXiaAction =  StringActionKeyValuePair(LocalString.GetString("打开"), DelegateFactory.Action(function()
    CUIManager.ShowUI(CLuaUIResources.BuffItemWnd)
end))

g_OneKeyStoreYaoXia = StringActionKeyValuePair(LocalString.GetString("一键收纳"), DelegateFactory.Action(function()
    local str = g_MessageMgr:FormatMessage("YAOXIA_COLLECT_CONFIRM")
    local reqfunc = function()
        Gac2Gas.OneKeyCollectItemToMedicineBox()--请求一键收纳
        CUIManager.ShowUI(CLuaUIResources.BuffItemWnd)
    end
    MessageWndManager.ShowOKCancelMessage(str, DelegateFactory.Action(reqfunc),nil, nil, nil, false)
end))

g_ChuanJiaBaoDaZaoAction = StringActionKeyValuePair(LocalString.GetString("打造"), DelegateFactory.Action(function()
    if g_PackageViewItemId == nil then
        return
    end
    local itemInfo=CItemMgr.Inst:GetItemInfo(g_PackageViewItemId)
    if itemInfo == nil then
        return
    end
    local commonItem = CItemMgr.Inst:GetById(g_PackageViewItemId)
    local data = nil
    if commonItem.Item.Type==EnumItemType_lua.EvaluatedZhushabi then
        if commonItem.Item.ExtraVarData and commonItem.Item.ExtraVarData.Data then
            data = CreateFromClass(CChuanjiabaoWordType)
            data:LoadFromString(commonItem.Item.ExtraVarData.Data)
        end
    else--传家宝
        data = commonItem.Item.ChuanjiabaoItemInfo and commonItem.Item.ChuanjiabaoItemInfo.WordInfo
    end
    if CLuaZhuShaBiXiLianMgr.CanXiLian(data) then
        CLuaZhuShaBiProcessWnd.m_SelectedItemId = g_PackageViewItemId
        CUIManager.CloseUI(CIndirectUIResources.ItemInfoWnd)
        CUIManager.ShowUI(CLuaUIResources.ZhuShaBiProcessWnd)
    else
        g_MessageMgr:ShowMessage("WASH_CHUANJIABAO_LIMIT")
    end
end))

g_JueJiSkillBookExchangeAction = StringActionKeyValuePair(LocalString.GetString("兑换"), DelegateFactory.Action(function()
    if g_PackageViewItemId == nil then
        return
    end
    local itemInfo=CItemMgr.Inst:GetItemInfo(g_PackageViewItemId)
    if itemInfo == nil then
        return
    end
    local commonItem = CItemMgr.Inst:GetById(g_PackageViewItemId)
    if not commonItem then return end
    LuaProfessionTransferMgr.ShowJueJiExchangeWndByJueJiSkillBook(g_PackageViewItemId, commonItem.TemplateId)

end))

g_RanFaJiPreviewAction = StringActionKeyValuePair(LocalString.GetString("预览"), DelegateFactory.Action(function()
    if g_PackageViewItemId == nil then
        return
    end
    local commonItem = CItemMgr.Inst:GetById(g_PackageViewItemId)
    if not commonItem then return end
    local recipeId = 0
    RanFaJi_RanFaJi.Foreach(function(k, v)
        if v.RanFaJiItemID == commonItem.TemplateId then
            recipeId = k
        end
    end)
    if recipeId > 0 then
        CLuaRanFaMgr:OpenRecipeWnd(recipeId)
    end
end))

g_BlindBoxPreviewAction = StringActionKeyValuePair(LocalString.GetString("预览"), DelegateFactory.Action(function()
    if g_PackageViewItemId == nil then
        return
    end
    local commonItem = CItemMgr.Inst:GetById(g_PackageViewItemId)
    if not commonItem then return end
    LuaShopMallFashionPreviewMgr:ShowBlindBoxPreviewWnd(commonItem.TemplateId)
end))

g_FashionLotteryDisassembleAction = StringActionKeyValuePair(LocalString.GetString("分解"), DelegateFactory.Action(function()
    if g_PackageViewItemId == nil then
        return
    end
    local commonItem = CItemMgr.Inst:GetById(g_PackageViewItemId)
    if not commonItem then return end
    LuaFashionLotteryMgr:CheckFashionDissamble(g_PackageViewItemId, commonItem.TemplateId)
end))

g_PreviewFashionSuitAction = StringActionKeyValuePair(LocalString.GetString("预览"), DelegateFactory.Action(function()
    if g_PackageViewItemId == nil then
        return
    end
    local commonItem = CItemMgr.Inst:GetById(g_PackageViewItemId)
    if not commonItem then return end
    
    local itemTemplateId = commonItem.TemplateId
    local suitId = LuaAppearancePreviewMgr:GetPreviewFashonSuitByItem(itemTemplateId)
    LuaAppearancePreviewMgr:ShowFashionSuitSubview(suitId)
end))

g_OpenCBGAction = StringActionKeyValuePair(LocalString.GetString("上架藏宝阁"), DelegateFactory.Action(function()
    if g_PackageViewItemId == nil then
        return
    end
    local commonItem = CItemMgr.Inst:GetById(g_PackageViewItemId)
    if not commonItem then return end
    LuaAppearancePreviewMgr:OpenCBG()
end))

g_ChangeToCbgTradeItemAction = StringActionKeyValuePair(LocalString.GetString("转藏宝锦盒"), DelegateFactory.Action(function()
    if g_PackageViewItemId == nil then
        return
    end
    local commonItem = CItemMgr.Inst:GetById(g_PackageViewItemId)
    if not commonItem then return end
    LuaAppearancePreviewMgr:ShowCBGItemTransformWnd(g_PackageViewItemId)
end))

g_ChangeToNoneCbgTradeItemAction = StringActionKeyValuePair(LocalString.GetString("转天衣锦盒"), DelegateFactory.Action(function()
    if g_PackageViewItemId == nil then
        return
    end
    local commonItem = CItemMgr.Inst:GetById(g_PackageViewItemId)
    if not commonItem then return end
    LuaAppearancePreviewMgr:ShowCBGItemTransformWnd(g_PackageViewItemId)
end))

--register--
g_PackageViewActionPairs={}
g_PackageViewActionPairs[21050292]={--全民pk吉祥物
    g_PeiyuAction,
    g_DiscardAction,
}

g_PackageViewActionPairs[21030713]={g_OpenYaoXiaAction,g_OneKeyStoreYaoXia,g_DiscardAction}
--

--cache--
g_PackageViewItemId=nil

function GetActionPairs(this, itemId, templateId)--
    g_PackageViewItemId = itemId

    local expireFunc = function()
        local pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, itemId)
        local item = CItemMgr.Inst:GetById(itemId)
        if pos == 0 or item == nil then
            return true, nil
        end

        if item.IsEquip then
        else
            if item:IsItemExpire() then
                --如果物品不可销毁或者是可以出售给系统的，没有丢弃按钮
                local taskItemActions = CreateFromClass(MakeGenericClass(List, StringActionKeyValuePair))
                if item.Item.CanBeDiscarded then
                    CommonDefs.ListAdd(taskItemActions, typeof(StringActionKeyValuePair), g_DiscardAction)
                end
                return true,taskItemActions
            end
        end
        return false
    end

    if g_PackageViewActionPairs[templateId] then
        local ret,actions = expireFunc()
        if ret then return true,actions end

        local taskItemActions = CreateFromClass(MakeGenericClass(List, StringActionKeyValuePair))
        for i,v in ipairs(g_PackageViewActionPairs[templateId]) do
            CommonDefs.ListAdd(taskItemActions,typeof(StringActionKeyValuePair),v)
        end
        return true,taskItemActions
    end

    --if templateId ==  then
        --local ret,actions = expireFunc()
        --if ret then return true,actions end
        --local acts = CreateFromClass(MakeGenericClass(List, StringActionKeyValuePair))
       -- CommonDefs.ListAdd(acts, typeof(StringActionKeyValuePair),g_OpenYaoXiaAction)
        --CommonDefs.ListAdd(acts, typeof(StringActionKeyValuePair),g_OneKeyStoreYaoXia)

        --return true,acts
    --end

    return false
end
