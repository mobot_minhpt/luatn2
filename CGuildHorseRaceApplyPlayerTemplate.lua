-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CGuildHorseRaceApplyPlayerTemplate = import "L10.UI.CGuildHorseRaceApplyPlayerTemplate"
local CGuildHorseRaceMgr = import "L10.Game.CGuildHorseRaceMgr"
local LocalString = import "LocalString"
CGuildHorseRaceApplyPlayerTemplate.m_Init_CS2LuaHook = function (this, info, bg) 
    this.playerInfo = info
    this.background.spriteName = bg
    local color
    if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.Name == this.playerInfo.playerName then
        color = "00FF00"
        this.background.spriteName = this.myselfSprite
    else
        color = "112C4C"
    end
    this.rank.text = System.String.Format("[c][{0}]{1}", color, info.rank)
    this.playerName.text = System.String.Format("[c][{0}]{1}", color, info.playerName)
    this.time.text = System.String.Format(LocalString.GetString("[c][{0}]{1}分{2}秒"), color, math.floor(info.time / 60), info.time % 60)
    this:InitStatus(color)
end
CGuildHorseRaceApplyPlayerTemplate.m_InitStatus_CS2LuaHook = function (this, color) 
    local result = ""
    repeat
        local default = this.playerInfo.status
        if default == (CGuildHorseRaceMgr.EnumGuildHorseRaceApplyStatus.applied) then
            result = LocalString.GetString("已申请")
            break
        elseif default == (CGuildHorseRaceMgr.EnumGuildHorseRaceApplyStatus.fight) then
            result = LocalString.GetString("出战")
            break
        elseif default == (CGuildHorseRaceMgr.EnumGuildHorseRaceApplyStatus.alternate) then
            result = LocalString.GetString("替补")
            break
        else
            break
        end
    until 1
    this.status.text = System.String.Format("[c][{0}]{1}", color, result)
end
