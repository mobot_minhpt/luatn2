-- Auto Generated!!
local AlignType = import "L10.UI.CTooltip+AlignType"
local CExpressionActionItem = import "L10.UI.CExpressionActionItem"
local CExpressionActionMgr = import "L10.Game.CExpressionActionMgr"
local CExpressionActionView = import "L10.UI.CExpressionActionView"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local DelegateFactory = import "DelegateFactory"
local Expression_Show = import "L10.Game.Expression_Show"
local LocalString = import "LocalString"
local UInt32 = import "System.UInt32"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
CExpressionActionView.m_getActionInfo_CS2LuaHook = function (this, expressionId) 
    local result = nil
    local show = Expression_Show.GetData(expressionId)
    if show ~= nil then
        local default
        default, result = CExpressionActionMgr.Inst.OtherSkillInfos:TryGetValue(show.DisplayOrder)
    end

    return result
end
CExpressionActionView.m_Init_CS2LuaHook = function (this) 
    this.table.m_DataSource = this
    this.table:ReloadData(false, false)
    this.table:SetSelectRow(0, true)
    this.settingButton.Visible = CExpressionActionView.openSettingButton
end
CExpressionActionView.m_OnItemClick_CS2LuaHook = function (this, row) 
    if row >= 0 and row < this.mDataSource.Count then
        CExpressionActionMgr.ExpressionActionID = this.dataSource[row]:GetID()
        local actionInfo = this.dataSource[row]
        local show = Expression_Show.GetData(actionInfo:GetID())
        if actionInfo ~= nil and show ~= nil then
            if not actionInfo:GetIsLock() then
                this.applyButton.Text = LocalString.GetString("进行动作")
            else
                this.applyButton.Text = LocalString.GetString("获取")
            end

            if show.Message ~= nil then
                this.hintLabel.gameObject:SetActive(true)
                this.hintLabel.text = show.Message
            else
                this.hintLabel.gameObject:SetActive(false)
            end
            local text, isShowLabel = LuaExpressionMgr:GetValidityPeriodStr(show)
            if isShowLabel then
                this.hintLabel.gameObject:SetActive(true)
                this.hintLabel.text = SafeStringFormat3(LocalString.GetString("有效期剩%s"),text) 
            end
        end
    end
end
CExpressionActionView.m_ItemAt_CS2LuaHook = function (this, view, row) 
    if row < this.dataSource.Count then
        local item = TypeAs(this.dataSource[row]:IsFashionSpecialExpression() and view:GetFromPool(1) or view:GetFromPool(0), typeof(CExpressionActionItem))
        local expressionId = this.dataSource[row]:GetID()
        local isInNeed = CommonDefs.ListContains(CExpressionActionMgr.ExpressionActionInNeed, typeof(UInt32), expressionId)
        item:Init(this.dataSource[row], isInNeed)
        item.OnItemLongPressed = DelegateFactory.Action(function () 
            view:SetSelectRow(row, view)
        end)
        return item
    end
    return nil
end
CExpressionActionView.m_OnApplyButtonClicked_CS2LuaHook = function (this, button) 
    local expressionId = CExpressionActionMgr.ExpressionActionID
    local show = Expression_Show.GetData(expressionId)
    if this:CheckIsLocked(show) then
        CItemAccessListMgr.Inst:ShowItemAccessInfo(show.ID, true, button.transform, AlignType.Right)
    else
        --PlayerSettings.SetExpressionPlayed(expressionId);
        CExpressionActionMgr.Inst:UpdateExpressionAlert(expressionId)
        CExpressionActionMgr.Inst:DoExpressionAction(false, false, 0)
    end
end
CExpressionActionView.m_OnSettingButtonClicked_CS2LuaHook = function (this, button) 
    if CUIManager.IsLoaded(CUIResources.ExpressionActionSettingWnd) then
        CUIManager.CloseUI(CUIResources.ExpressionActionSettingWnd)
    else
        CExpressionActionMgr.ShowExpressionActionSetting(button.transform)
    end
end
CExpressionActionView.m_CheckIsLocked_CS2LuaHook = function (this, show) 
    if show == nil then
        return false
    end
    if show.LockGroup ~= 0 and CExpressionActionMgr.Inst.UnLockGroupInfo[show.LockGroup] == 0 then
        return true
    end
    if show.LockGroup ~= 0 and show.AvailableTime > 0 then
        local mainplayer = CClientMainPlayer.Inst
        if mainplayer then
            local dict = mainplayer.PlayProp.ExpressionExpireInfo
            if dict then
                local __try_get_result, time = CommonDefs.DictTryGet(dict, typeof(Byte), show.LockGroup, typeof(UInt32))
                if __try_get_result then
                    local now = CServerTimeMgr.Inst.timeStamp
                    time = time - now
                    return time <= 0
                end
            end
        end
    end
    return false
end
