-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CTalismanMenuTemplate = import "L10.UI.CTalismanMenuTemplate"
local CTalismanMgr = import "L10.Game.CTalismanMgr"
local Gac2Gas = import "L10.Game.Gac2Gas"
local Talisman_Setting = import "L10.Game.Talisman_Setting"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CTalismanMenuTemplate.m_Init_CS2LuaHook = function (this, open, name, index, select) 
    this.isMenuOpen = open
    this.index = index
    this.lockSprite:SetActive(not open)
    this.nameLabel.text = name
    this.grid:Reposition()

    UIEventListener.Get(this.gameObject).onClick = MakeDelegateFromCSFunction(this.OnButtonClick, VoidDelegate, this)
end
CTalismanMenuTemplate.m_OnButtonClick_CS2LuaHook = function (this, go) 
    if CLuaQMPKMgr.s_IsOtherPlayer then
        CLuaQMPKMgr.s_PlayerInfo.TalismanIndex = this.index + 1
        g_ScriptEvent:BroadcastInLua("OnOtherTalismanMenuSelect", this.index)
        return
    end
    if not this.isMenuOpen and CTalismanMgr.OpenTalismanSecondSheet then
        g_MessageMgr:ShowMessage("Open_FaBao_ZuHe2_Fail_Tips", Talisman_Setting.GetData().SecondaryTalismanXiuWei, NumberTruncate(CClientMainPlayer.Inst.BasicProp.XiuWeiGrade, 1))
    elseif not CTalismanMgr.OpenTalismanSecondSheet then
        g_MessageMgr:ShowMessage("FUNCTION_NOT_OPEN")
    else
        Gac2Gas.RequestActiveTalismanOnBody(this.index + 1)
    end
end
