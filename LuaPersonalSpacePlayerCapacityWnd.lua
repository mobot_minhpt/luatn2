

CLuaPersonalSpacePlayerCapacityWnd = class()
RegistClassMember(CLuaPersonalSpacePlayerCapacityWnd,"template")
RegistClassMember(CLuaPersonalSpacePlayerCapacityWnd,"playerCapacityLabel")
RegistClassMember(CLuaPersonalSpacePlayerCapacityWnd,"table")
RegistClassMember(CLuaPersonalSpacePlayerCapacityWnd,"titleLabel")
RegistClassMember(CLuaPersonalSpacePlayerCapacityWnd,"childTemplate")

CLuaPersonalSpacePlayerCapacityWnd.playerCapacity_playerid = 0
CLuaPersonalSpacePlayerCapacityWnd.playerCapacity_playername = nil
CLuaPersonalSpacePlayerCapacityWnd.playerCapacity_totalZhanli = 0
CLuaPersonalSpacePlayerCapacityWnd.playerCapacity_detailData={}

function CLuaPersonalSpacePlayerCapacityWnd:Awake()
    if CommonDefs.IS_VN_CLIENT then
        local myLabel = self.transform:Find("Anchor/ScrollView/Template/Label")
        myLabel.transform.localPosition = Vector3(131, -41.6, 0)
    end
    
    self.template = self.transform:Find("Anchor/ScrollView/Template").gameObject
    self.template:SetActive(false)
    self.childTemplate = self.transform:Find("Anchor/ScrollView/ChildTemplate").gameObject
    self.childTemplate:SetActive(false)
    self.playerCapacityLabel = self.transform:Find("Anchor/PlayerCapacityLabel"):GetComponent(typeof(UILabel))
    self.table = self.transform:Find("Anchor/ScrollView/Grid"):GetComponent(typeof(UITable))
    self.titleLabel = self.transform:Find("Wnd_Bg_Secondary_3/TitleLabel"):GetComponent(typeof(UILabel))

end

function CLuaPersonalSpacePlayerCapacityWnd:Init( )
    self.template:SetActive(false)
    self.titleLabel.text = SafeStringFormat3("%s(ID:%s)", CLuaPersonalSpacePlayerCapacityWnd.playerCapacity_playername, CLuaPersonalSpacePlayerCapacityWnd.playerCapacity_playerid)
    self:UpdatePlayerCapacity(CLuaPersonalSpacePlayerCapacityWnd.playerCapacity_playerid, CLuaPersonalSpacePlayerCapacityWnd.playerCapacity_totalZhanli, CLuaPersonalSpacePlayerCapacityWnd.playerCapacity_detailData)
end
function CLuaPersonalSpacePlayerCapacityWnd:UpdatePlayerCapacity( id, capacity, detailData) 
    CLuaPlayerCapacityMgr.InitModuleDef()
    for i,v in ipairs(CLuaPlayerCapacityMgr.m_ModuleDef) do
    if not v.ignore then
        local go = NGUITools.AddChild(self.table.gameObject,self.template)
        go:SetActive(true)
        local tf = go.transform
        local nameLabel = tf:Find("NameLabel"):GetComponent(typeof(UILabel))
        nameLabel.text = v.name

        local grid = tf:Find("Grid"):GetComponent(typeof(UIGrid))
        for j,subinfo in ipairs(v.sub) do
            local child = NGUITools.AddChild(grid.gameObject,self.childTemplate)
            child:SetActive(true)
            local descLabel = child.transform:Find("DescLabel"):GetComponent(typeof(UILabel))
            descLabel.text = subinfo[2]
            local label1 = child.transform:Find("Label1"):GetComponent(typeof(UILabel))

            local subkey = subinfo[1]
            local otherValue =CLuaPersonalSpacePlayerCapacityWnd.playerCapacity_detailData[subinfo[1]] or 0
            label1.text = tostring(otherValue)
            local label2 = child.transform:Find("Label2"):GetComponent(typeof(UILabel))
            local visible = true
            if subinfo.condition then
                visible = subinfo.condition()
            end
            label2.gameObject:SetActive(visible)
            local myValue = math.floor(CLuaPlayerCapacityMgr.GetValue(subinfo[1]))
            label2.text = tostring(myValue)
            if myValue>=otherValue then
                label2.color =Color.green--NGUIText.ParseColor("ff0000", 0)
            else
                label2.color = Color.red--NGUIText.ParseColor("ff0000", 0)
            end
        end
        grid:Reposition()
        go:GetComponent(typeof(UISprite)).height = #v.sub*64+25
    end
    end

    self.table:Reposition()

    self.playerCapacityLabel.text = tostring((math.floor(capacity)))
end
