local UIGrid = import "UIGrid"

local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"

LuaNanDuFanHuaLordChangePropertyWnd = class()
LuaNanDuFanHuaLordChangePropertyWnd.m_showLive2D = nil
LuaNanDuFanHuaLordChangePropertyWnd.m_attributeList = nil
--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaNanDuFanHuaLordChangePropertyWnd, "Root1", "Root1", GameObject)
RegistChildComponent(LuaNanDuFanHuaLordChangePropertyWnd, "Root2", "Root2", GameObject)
RegistChildComponent(LuaNanDuFanHuaLordChangePropertyWnd, "Root3", "Root3", GameObject)
RegistChildComponent(LuaNanDuFanHuaLordChangePropertyWnd, "PropertyGrid", "PropertyGrid", UIGrid)
RegistChildComponent(LuaNanDuFanHuaLordChangePropertyWnd, "PropertyTemplate", "PropertyTemplate", UILabel)
RegistChildComponent(LuaNanDuFanHuaLordChangePropertyWnd, "Live2dRoot", "Live2dRoot", GameObject)

--@endregion RegistChildComponent end

function LuaNanDuFanHuaLordChangePropertyWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self:RefreshUI()
end

function LuaNanDuFanHuaLordChangePropertyWnd:Init()

end

function LuaNanDuFanHuaLordChangePropertyWnd:OnDisable()
    if self.autoHideTick then
        UnRegisterTick(self.autoHideTick)
    end
end

--@region UIEvent

--@endregion UIEvent

function LuaNanDuFanHuaLordChangePropertyWnd:RefreshUI()
    local showLive2d = LuaNanDuFanHuaLordChangePropertyWnd.m_showLive2D
    self.Root1:SetActive(showLive2d and showLive2d == 1 or false)
    self.Root2:SetActive(showLive2d and showLive2d == 2 or false)
    self.Root3:SetActive(showLive2d and showLive2d == 3 or false)
    self.Live2dRoot:SetActive(showLive2d and showLive2d ~= -1 or false)
    self.transform:Find("PropertyRoot").gameObject:SetActive(#LuaNanDuFanHuaLordChangePropertyWnd.m_attributeList > 0)
    local grid = self.PropertyGrid
    local template = self.PropertyTemplate.gameObject
    for i = 1, #LuaNanDuFanHuaLordChangePropertyWnd.m_attributeList do
        local attributeInfo = LuaNanDuFanHuaLordChangePropertyWnd.m_attributeList[i]
        local propertyObj = NGUITools.AddChild(grid.gameObject, template)
        propertyObj:SetActive(true)
        propertyObj.transform:GetComponent(typeof(UILabel)).text = CUICommonDef.TranslateToNGUIText(attributeInfo)
    end
    grid:Reposition()

    if self.autoHideTick then
        UnRegisterTick(self.autoHideTick)
    end
    if showLive2d and showLive2d == -1 or false then
        self.autoHideTick = RegisterTickOnce(function()
            CUIManager.CloseUI(CLuaUIResources.NanDuFanHuaLordChangePropertyWnd)
        end, 5000)
    end
end

