-- Auto Generated!!
local CEquipIntensifyWnd = import "L10.UI.CEquipIntensifyWnd"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
CEquipIntensifyWnd.m_Init_CS2LuaHook = function (this) 
    this.intensifySwitchMasterView:Init()

    this.tabView.OnSelect = DelegateFactory.Action_QnTabButton_int(function (btn, index) 
        if index == 0 then
            --装备强化
            this.intensifySwitchMasterView.gameObject:SetActive(false)
            this.intensifySwitchDetailView.gameObject:SetActive(false)
            --equipmentTable.gameObject.SetActive(true);
            this.intensifyDetailView.gameObject:SetActive(true)
            EventManager.BroadcastInternalForLua(EnumEventType.SwitchEquipmentTabIndex, {1, 0})
        elseif index == 1 then
            --强化转移
            this.intensifySwitchMasterView.gameObject:SetActive(true)
            this.intensifySwitchDetailView.gameObject:SetActive(true)
            --equipmentTable.gameObject.SetActive(false);
            this.intensifyDetailView.gameObject:SetActive(false)
            EventManager.BroadcastInternalForLua(EnumEventType.SwitchEquipmentTabIndex, {1, 1})
        end
    end)
    this.tabView:ChangeTo(this.tabView.CurrentSelectTab)
end
