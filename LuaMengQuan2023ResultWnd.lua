local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local EShareType = import "L10.UI.EShareType"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Profession = import "L10.Game.Profession"
local CForcesMgr = import "L10.Game.CForcesMgr"
local CRankData = import "L10.UI.CRankData"

LuaMengQuan2023ResultWnd = class()
LuaMengQuan2023ResultWnd.m_isGetDailyAward = nil
LuaMengQuan2023ResultWnd.m_isGetEliteAward = nil
LuaMengQuan2023ResultWnd.m_rankCache = nil
LuaMengQuan2023ResultWnd.m_fightData = nil
LuaMengQuan2023ResultWnd.m_newRecord = false

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaMengQuan2023ResultWnd, "RankLabel", "RankLabel", GameObject)
RegistChildComponent(LuaMengQuan2023ResultWnd, "CloseButton", "CloseButton", GameObject)
RegistChildComponent(LuaMengQuan2023ResultWnd, "PodiumWidget", "PodiumWidget", GameObject)
RegistChildComponent(LuaMengQuan2023ResultWnd, "Rank1", "Rank1", GameObject)
RegistChildComponent(LuaMengQuan2023ResultWnd, "Rank2", "Rank2", GameObject)
RegistChildComponent(LuaMengQuan2023ResultWnd, "Rank3", "Rank3", GameObject)
RegistChildComponent(LuaMengQuan2023ResultWnd, "TopNode1", "TopNode1", GameObject)
RegistChildComponent(LuaMengQuan2023ResultWnd, "RewardNode", "RewardNode", GameObject)
RegistChildComponent(LuaMengQuan2023ResultWnd, "NoRewardReminder", "NoRewardReminder", UILabel)
RegistChildComponent(LuaMengQuan2023ResultWnd, "Item1", "Item1", GameObject)
RegistChildComponent(LuaMengQuan2023ResultWnd, "Item2", "Item2", GameObject)
RegistChildComponent(LuaMengQuan2023ResultWnd, "ShareButton", "ShareButton", GameObject)
RegistChildComponent(LuaMengQuan2023ResultWnd, "FightAgainButton", "FightAgainButton", GameObject)
RegistChildComponent(LuaMengQuan2023ResultWnd, "RankButton", "RankButton", GameObject)
RegistChildComponent(LuaMengQuan2023ResultWnd, "DetailButton", "DetailButton", GameObject)
RegistChildComponent(LuaMengQuan2023ResultWnd, "TopNode2", "TopNode2", GameObject)
RegistChildComponent(LuaMengQuan2023ResultWnd, "TopNode3", "TopNode3", GameObject)

--@endregion RegistChildComponent end

function LuaMengQuan2023ResultWnd:Awake()
    if CommonDefs.IS_VN_CLIENT then
        self.ShareButton:SetActive(false)
        
        if LocalString.language == "cn" then
            self.RankLabel.transform:Find("Yi"):GetComponent(typeof(UITexture)).width = 82
            self.RankLabel.transform:Find("Yi"):GetComponent(typeof(UITexture)).height = 19
            self.RankLabel.transform:Find("Er"):GetComponent(typeof(UITexture)).width = 82
            self.RankLabel.transform:Find("Er"):GetComponent(typeof(UITexture)).height = 51
            self.RankLabel.transform:Find("San"):GetComponent(typeof(UITexture)).width = 82
            self.RankLabel.transform:Find("San"):GetComponent(typeof(UITexture)).height = 60
        elseif LocalString.language == "vn" then
            self.RankLabel.transform:Find("Yi"):GetComponent(typeof(UITexture)).width = 259
            self.RankLabel.transform:Find("Yi"):GetComponent(typeof(UITexture)).height = 60
            self.RankLabel.transform:Find("Er"):GetComponent(typeof(UITexture)).width = 82
            self.RankLabel.transform:Find("Er"):GetComponent(typeof(UITexture)).height = 82
            self.RankLabel.transform:Find("San"):GetComponent(typeof(UITexture)).width = 82
            self.RankLabel.transform:Find("San"):GetComponent(typeof(UITexture)).height = 82
        else
            self.RankLabel.transform:Find("Yi"):GetComponent(typeof(UITexture)).width = 82
            self.RankLabel.transform:Find("Yi"):GetComponent(typeof(UITexture)).height = 82
            self.RankLabel.transform:Find("Er"):GetComponent(typeof(UITexture)).width = 82
            self.RankLabel.transform:Find("Er"):GetComponent(typeof(UITexture)).height = 82
            self.RankLabel.transform:Find("San"):GetComponent(typeof(UITexture)).width = 82
            self.RankLabel.transform:Find("San"):GetComponent(typeof(UITexture)).height = 82
        end
    end
    
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self:InitWndData()
    self:InitUIEvent()
    self:RefreshVariableUI()

end

function LuaMengQuan2023ResultWnd:Init()

end

--@region UIEvent

--@endregion UIEvent

function LuaMengQuan2023ResultWnd:InitWndData()
    self.teamNameList = {LocalString.GetString("红队"),
                          LocalString.GetString("黄队"),
                          LocalString.GetString("蓝队")}
    self.teamNameColorList = {NGUIText.ParseColor("7E1B00", 0),
                               NGUIText.ParseColor("603F00", 0),
                               NGUIText.ParseColor("003B79", 0)}
    self.teamScoreColorList = {NGUIText.ParseColor("ffffff", 0),
                                NGUIText.ParseColor("ffffff", 0),
                                NGUIText.ParseColor("ffffff", 0)}
    self.teamBgColorList = {NGUIText.ParseColor("C2503E", 0),
                             NGUIText.ParseColor("BD9048", 0),
                             NGUIText.ParseColor("38588A", 0)}

    self.MQHXConfigData = Double11_MQLD.GetData()
end

function LuaMengQuan2023ResultWnd:RefreshVariableUI()
    if CClientMainPlayer.Inst == nil then
        self.RewardNode:SetActive(false)
        self.transform:Find("Anchor/InfoView/mengquandiban/RankNode").gameObject:SetActive(false)
        return
    end
    
    self.myPlayerId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
    self.teamId = 0
    self.myTeamRank = 1
    if self.myPlayerId ~= 0 then
        local bFind, force = CommonDefs.DictTryGet_LuaCall(CForcesMgr.Inst.m_PlayForceInfo, self.myPlayerId)
        if bFind == true then
            self.teamId = force
        else
            --这个地方不应该跑到, 做个保底
            self.teamId = 0
        end
    end

    for i = 1, #self.m_rankCache do
        self:RefreshTeamRankNode(i, self.m_rankCache[i])
    end
    for i = #self.m_rankCache+1, 3 do
        self:RefreshTeamRankNode(i, nil)
    end

    local myTeamFightData = {}
    local myFightData = {}
    for i = 1, #self.m_fightData do
        if self.m_fightData[i][3] == self.teamId then
            if (self.m_fightData[i][1] == self.myPlayerId) then
                myFightData = self.m_fightData[i]
            else
                table.insert(myTeamFightData, self.m_fightData[i])
            end
        end
    end
    self:RefreshTopNode(1, myFightData)
    for i = 1, #myTeamFightData do
        self:RefreshTopNode(i+1, myTeamFightData[i])
    end
    for i = #myTeamFightData+2, 3 do
        self:RefreshTopNode(i, nil)
    end
    
    self:InitRewardType(self.m_isGetDailyAward, self.m_isGetEliteAward)
    
    self.RankLabel.transform:Find("Yi").gameObject:SetActive(self.myTeamRank == 1)
    self.RankLabel.transform:Find("Er").gameObject:SetActive(self.myTeamRank == 2)
    self.RankLabel.transform:Find("San").gameObject:SetActive(self.myTeamRank == 3)
end

function LuaMengQuan2023ResultWnd:InitUIEvent()
    UIEventListener.Get(self.FightAgainButton).onClick = DelegateFactory.VoidDelegate(function (_)
        CUIManager.CloseUI(CLuaUIResources.MengQuan2023ResultWnd)
        CUIManager.ShowUI(CLuaUIResources.MengQuan2023SignUpWnd)
    end)

    UIEventListener.Get(self.ShareButton).onClick = DelegateFactory.VoidDelegate(function (_)
        self.FightAgainButton:SetActive(false)
        self.ShareButton:SetActive(false)
        self.CloseButton:SetActive(false)
        self.RankButton:SetActive(false)
        CUICommonDef.CaptureScreen("screenshot", true, false, nil, DelegateFactory.Action_string_bytes(function(fullPath, jpgBytes)
            ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, fullPath)
            self.FightAgainButton.gameObject:SetActive(true)
            self.ShareButton.gameObject:SetActive(true)
            self.CloseButton:SetActive(true)
            self.RankButton:SetActive(true)
        end))
    end)

    UIEventListener.Get(self.RankButton).onClick = DelegateFactory.VoidDelegate(function (_)
        Gac2Gas.QueryRank(self.MQHXConfigData.RankId)
    end)

    UIEventListener.Get(self.DetailButton).onClick = DelegateFactory.VoidDelegate(function (_)
        LuaMengQuan2023StatWnd.battleRank = self.m_rankCache
        LuaMengQuan2023StatWnd.battleStat = self.m_fightData
        CUIManager.ShowUI(CLuaUIResources.MengQuan2023StatWnd)
    end)
end

function LuaMengQuan2023ResultWnd:RefreshTeamRankNode(rank, info)
    local node = self["Rank" .. rank]
    if info then
        local teamId = info[1]
        local score = info[2]
        local nameLabel = node.transform:Find("teamName"):GetComponent(typeof(UILabel))
        local scoreLabel = node.transform:Find("score"):GetComponent(typeof(UILabel))
        local isMeBg = node.transform:Find("isMeBg")
        local teamBg = node.transform:Find("teamBg"):GetComponent(typeof(UITexture))
        local isMe = teamId == self.teamId
        if isMe then
            self.myTeamRank = rank
        end
        
        nameLabel.text = self.teamNameList[teamId] and self.teamNameList[teamId] or ""
        nameLabel.color = self.teamNameColorList[teamId] and self.teamNameColorList[teamId] or NGUIText.ParseColor("ffffff", 0)
        scoreLabel.text = score
        scoreLabel.color = self.teamScoreColorList[teamId] and self.teamScoreColorList[teamId] or NGUIText.ParseColor("ffffff", 0)
        isMeBg.gameObject:SetActive(isMe)
        teamBg.color = self.teamBgColorList[teamId] and self.teamBgColorList[teamId] or NGUIText.ParseColor("ffffff", 0)
        node:SetActive(true)
    else
        node:SetActive(false)
    end
end

function LuaMengQuan2023ResultWnd:RefreshTopNode(posIndex, info)
    local node = self["TopNode" .. posIndex]
    if info then
        --playerId, name, teamId, killNum, ctrlNum, heal, reliveNum, career
        local careerSprite = node.transform:Find("careerSprite"):GetComponent(typeof(UISprite))
        local nameLabel = node.transform:Find("PlayerName"):GetComponent(typeof(UILabel))
        local killScoreLabel = node.transform:Find("KillScore"):GetComponent(typeof(UILabel))
        local ctrlScoreLabel = node.transform:Find("CtrlScore"):GetComponent(typeof(UILabel))
        local healScoreLabel = node.transform:Find("HealScore"):GetComponent(typeof(UILabel))
        local reviveScoreLabel = node.transform:Find("RevivieScore"):GetComponent(typeof(UILabel))
        
        nameLabel.text = info[2]
        killScoreLabel.text = info[4]
        ctrlScoreLabel.text = info[5]

        local healScore = info[6]
        if healScore >= 10000 then
            healScoreLabel.text = math.floor(healScore / 10000) .. LocalString.GetString("万")
        else
            healScoreLabel.text = math.floor(healScore)
        end
        reviveScoreLabel.text = info[7]
        careerSprite.spriteName = Profession.GetIconByNumber(info[8])
        node:SetActive(true)

        if posIndex == 1 then
            node.transform:Find("NewRecord").gameObject:SetActive(LuaMengQuan2023ResultWnd.m_newRecord)
        end
    else
        node:SetActive(false)
    end
end

function LuaMengQuan2023ResultWnd:InitRewardType(isGetDailyAward, isGetEliteAward)
    local haveReward = isGetDailyAward or isGetEliteAward
    
    self.NoRewardReminder.gameObject:SetActive(not haveReward)
    if not haveReward then
        --self.NoRewardReminder
        local isZeroScore = false
        for i = 1, #self.m_rankCache do
            --self:RefreshTeamRankNode(i, self.m_rankCache[i])
            local info = self.m_rankCache[i]
            local teamId = info[1]
            local score = info[2]
            if teamId == self.teamId then
                isZeroScore = (score == 0)
            end
        end

        if isZeroScore then
            self.NoRewardReminder.text = LocalString.GetString("无奖励")
        else
            self.NoRewardReminder.text = LocalString.GetString("今日奖励次数已达上限")
        end
        
    end
    self.Item1:SetActive(false)
    self.Item2:SetActive(false)
    local itemIdx = 1
    local itemObjList = {self.Item1, self.Item2}
    if isGetDailyAward then
        --这次有获得日常奖励
        local itemObj = itemObjList[itemIdx]
        itemObj:SetActive(true)
        itemIdx = itemIdx + 1

        self:InitOneItem(itemObj, self.myTeamRank == 1 and self.MQHXConfigData.JoinRewardItemId[1] or self.MQHXConfigData.JoinRewardItemId[0])
    end
    
    if isGetEliteAward then
        --这次有获得精英奖励
        local itemObj = itemObjList[itemIdx]
        itemObj:SetActive(true)
        self:InitOneItem(itemObj, self.MQHXConfigData.ScoreRewardItemId)
        itemIdx = itemIdx + 1
    end
end

function LuaMengQuan2023ResultWnd:InitOneItem(curItem, itemID)
    local texture = curItem.transform:Find("Item"):GetComponent(typeof(CUITexture))
    local nameLabel = curItem.transform:Find("RewardName"):GetComponent(typeof(UILabel))
    local ItemData = Item_Item.GetData(itemID)
    if ItemData then 
        texture:LoadMaterial(ItemData.Icon)
        nameLabel.text = ItemData.Name
    end
    UIEventListener.Get(curItem).onClick = DelegateFactory.VoidDelegate(function (_)
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemID, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
    end)
end

function LuaMengQuan2023ResultWnd:OnEnable()
    g_ScriptEvent:AddListener("OnRankDataReady", self, "OnRankDataReady")
end

function LuaMengQuan2023ResultWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnRankDataReady", self, "OnRankDataReady")
end

function LuaMengQuan2023ResultWnd:OnRankDataReady()
    if CLuaRankData.m_CurRankId ~= self.MQHXConfigData.RankId then return end
    CUIManager.ShowUI(CLuaUIResources.MengQuan2023RankWnd)
end 
