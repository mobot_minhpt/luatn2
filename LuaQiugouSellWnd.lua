local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local CUITexture = import "L10.UI.CUITexture"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CPlayerShopMgr = import "L10.UI.CPlayerShopMgr"
local CClientFurnitureMgr = import "L10.Game.CClientFurnitureMgr"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local CItemMgr = import "L10.Game.CItemMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CItem=import "L10.Game.CItem"

LuaQiugouSellWnd = class()
RegistChildComponent(LuaQiugouSellWnd,"CloseButton", GameObject)
RegistChildComponent(LuaQiugouSellWnd,"SellButton", GameObject)
RegistChildComponent(LuaQiugouSellWnd,"SingleLabel", UILabel)
RegistChildComponent(LuaQiugouSellWnd,"NumLabel", UILabel)
RegistChildComponent(LuaQiugouSellWnd,"TotalLabel", UILabel)
RegistChildComponent(LuaQiugouSellWnd,"FeeLabel", UILabel)

--RegistClassMember(LuaQiugouSellWnd, "maxChooseNum")

function LuaQiugouSellWnd:Close()
	CUIManager.CloseUI(CLuaUIResources.QiugouSellWnd)
end

function LuaQiugouSellWnd:OnEnable()
	--g_ScriptEvent:AddListener("", self, "")
end

function LuaQiugouSellWnd:OnDisable()
	--g_ScriptEvent:RemoveListener("", self, "")
end

function LuaQiugouSellWnd:InitItemData()
  local itemTemplateId = LuaQiugouMgr.BuyOrderListItemId
  local item = Item_Item.GetData(itemTemplateId)
  self.transform:Find('Content1/ContentLeft/Texture'):GetComponent(typeof(CUITexture)):LoadMaterial(item.Icon)
  local color = CPlayerShopMgr.Inst:GetColor(itemTemplateId)
  self.transform:Find('Content1/ContentLeft/Name'):GetComponent(typeof(UILabel)).color = color
  self.transform:Find('Content1/ContentLeft/Name'):GetComponent(typeof(UILabel)).text = item.Name

	if item.Overlap <= 1 then
		self.overLap = false
	else
		self.overLap = true
	end

  self.NumLabel.text = LuaQiugouMgr.BuyOrderListSingleItemData[3]
  self.SingleLabel.text = LuaQiugouMgr.BuyOrderListSingleItemData[4]

  local zswdataID = nil
  local zswdata = nil
  local des = CItem.GetItemDescription(itemTemplateId,true)
  Zhuangshiwu_Zhuangshiwu.Foreach(function (key,data)
    if data.ItemId == itemTemplateId then
      zswdataID = data.ID
    end
  end)
	if zswdataID then
		zswdata = Zhuangshiwu_Zhuangshiwu.GetData(zswdataID)
	end

	if zswdata then
		self.transform:Find('Content1/ContentLeft/QnScrollLabel/ScrollView/Label'):GetComponent(typeof(UILabel)).text = g_MessageMgr:Format(System.String.Format(des, zswdata.Repair))
		self.transform:Find('Content1/ContentLeft/Level'):GetComponent(typeof(UILabel)).text = String.Format(LocalString.GetString("{0}级{1}"), zswdata.Grade, CClientFurnitureMgr.GetTypeNameByType(zswdata.Type))
	else
		self.transform:Find('Content1/ContentLeft/QnScrollLabel/ScrollView/Label'):GetComponent(typeof(UILabel)).text = des
		self.transform:Find('Content1/ContentLeft/Level'):GetComponent(typeof(UILabel)).text = ''
	end
end

function LuaQiugouSellWnd:RgbToHex(color)
	local rgb = {color.r*255,color.g*255,color.b*255}
	local hexadecimal = ''

	for key, value in pairs(rgb) do
		local hex = ''

		while(value > 0)do
			local index = math.fmod(value, 16) + 1
			value = math.floor(value / 16)
			hex = string.sub('0123456789ABCDEF', index, index) .. hex
		end

		if(string.len(hex) == 0)then
			hex = '00'

		elseif(string.len(hex) == 1)then
			hex = '0' .. hex
		end

		hexadecimal = hexadecimal .. hex
	end

	return hexadecimal
end


function LuaQiugouSellWnd:InitOp()

  self.m_SellListNumButton = self.transform:Find('Content1/ContentRight/Num/QnIncreseAndDecreaseButton'):GetComponent(typeof(QnAddSubAndInputButton))
	if self.overLap then

		self.m_SellListNumButton:SetMinMax(1, LuaQiugouMgr.BuyOrderListSingleItemData[3], 1)
		self.m_SellListNumButton.onValueChanged = DelegateFactory.Action_uint(function(value)
			self:NumValueChange(value)
		end)
		self.m_SellListNumButton:SetValue(1, true)

		self.m_SellListNumButton:SetInputEnabled(true)
		self.m_SellListNumButton:EnableButtons(true)
	else
		self.m_SellListNumButton:SetValue(1, true)
		self.m_SellListNumButton:SetInputEnabled(false)
		self.m_SellListNumButton:EnableButtons(false)
		self:NumValueChange(1)
	end

	local onSubmitClick = function(go)
    local count = self.m_SellListNumButton:GetValue()

    local pos, itemId, ret = 0, "", false
    ret, pos, itemId = CItemMgr.Inst:FindItemByTemplateIdPriorToNotBinded(EnumItemPlace.Bag, LuaQiugouMgr.BuyOrderListItemId)
    local bindCount, notbindCount
    bindCount, notbindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(LuaQiugouMgr.BuyOrderListItemId)

    if itemId and notbindCount >= count then
			local item = Item_Item.GetData(LuaQiugouMgr.BuyOrderListItemId)
			local color = CPlayerShopMgr.Inst:GetColor(LuaQiugouMgr.BuyOrderListItemId)
      local str = LocalString.GetString('您以#287[c][%s]%s[-]的单价出售[c][FFD400]%s[-]个[c][%s]%s[-]，扣除交易税后收入为#287[c][%s]%s[-]。是否确认？')
      local price = LuaQiugouMgr.BuyOrderListSingleItemData[4]
      local totalPrice = price * count
      local tax = math.ceil(CPlayerShopMgr.Transaction_Tax_Client * totalPrice)--CPlayerShopMgr.Inst:GetTax(price, count)
      local getPrice = totalPrice - tax

			local moneyColor1 = self:RgbToHex(UILabel.GetMoneyColor(price))
			local moneyColor2 = self:RgbToHex(UILabel.GetMoneyColor(totalPrice))
      str = SafeStringFormat3(str,moneyColor1,price,count,self:RgbToHex(color),item.Name,moneyColor2,getPrice)
      MessageWndManager.ShowOKCancelMessage(str, DelegateFactory.Action(function ()
        Gac2Gas.SellItemToRequestBuyOrder(LuaQiugouMgr.BuyOrderListSingleItemData[1],pos,itemId,count) -- orderId, bagPos, itemId, sellCount
      end), nil, nil, nil, false)
    else
      g_MessageMgr:ShowMessage('CUSTOM_STRING2',LocalString.GetString('物品数量不足！'))
    end
	end
	CommonDefs.AddOnClickListener(self.SellButton,DelegateFactory.Action_GameObject(onSubmitClick),false)
end

function LuaQiugouSellWnd:NumValueChange(value)
	local totalPrice = LuaQiugouMgr.BuyOrderListSingleItemData[4] * value
	local tax = math.ceil(CPlayerShopMgr.Transaction_Tax_Client * totalPrice)--CPlayerShopMgr.Inst:GetTax(price, count)
	self.TotalLabel.text = totalPrice
	self.FeeLabel.text = tax
end

function LuaQiugouSellWnd:Init()
	local onCloseClick = function(go)
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.CloseButton,DelegateFactory.Action_GameObject(onCloseClick),false)
  --LuaQiugouMgr.BuyOrderListSingleItemData
  --table.insert(LuaQiugouMgr.BuyOrderListItemTable,{orderId,playerId,count,price,onShelfTime,expireTime})

  self:InitItemData()
  self:InitOp()
end

return LuaQiugouSellWnd
