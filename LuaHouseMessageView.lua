local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UISimpleTableView = import "L10.UI.UISimpleTableView"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CClientHouseMgr=import "L10.Game.CClientHouseMgr"
local DefaultUISimpleTableViewDataSource = import "L10.UI.DefaultUISimpleTableViewDataSource"
local CHouseMsgItemWnd = import "L10.UI.CHouseMsgItemWnd"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local UISprite = import "UISprite"
local AnchorUpdate = import "UIRect.AnchorUpdate"
local UIWidget = import "UIWidget"

LuaHouseMessageView = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaHouseMessageView, "TableBody", "TableBody", UISimpleTableView)
RegistChildComponent(LuaHouseMessageView, "MsgTemplate", "MsgTemplate", GameObject)
RegistChildComponent(LuaHouseMessageView, "Bianbian", "Bianbian", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaHouseMessageView, "m_DefaultSimpleTableViewDataSource")
RegistClassMember(LuaHouseMessageView, "m_SingleRowHeight")

function LuaHouseMessageView:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end  
    self.m_TextRowPady = self.MsgTemplate:GetComponent(typeof(CHouseMsgItemWnd)).msgLbl.spacingY
    self.m_SingleRowHeight = self.MsgTemplate:GetComponent(typeof(CHouseMsgItemWnd)).msgLbl.fontSize+self.m_TextRowPady
    
    self.MsgTemplate:SetActive(false)
end

function LuaHouseMessageView:Init0()
    self.TableBody:Clear()
    if not self.m_DefaultSimpleTableViewDataSource then

		self.m_DefaultSimpleTableViewDataSource = DefaultUISimpleTableViewDataSource.Create(function ()
			return self:NumberOfRows()
		end,
		function ( index )
			return self:CellForRowAtIndex(index)
		end)
	end

    self.TableBody.dataSource = self.m_DefaultSimpleTableViewDataSource
    self.TableBody:LoadDataFromTail()
end

function LuaHouseMessageView:NumberOfRows()
	return CClientHouseMgr.Inst.NewsNum
end

function LuaHouseMessageView:CellForRowAtIndex(index)
    local cell = self.TableBody:DequeueReusableCellWithIdentifier("")
    if cell == nil then
        cell = self.TableBody:AllocNewCellWithIdentifier(self.MsgTemplate, "")
    end
    local itemWnd = cell:GetComponent(typeof(CHouseMsgItemWnd))
    local sprite = cell:GetComponent(typeof(UISprite))
    sprite.height = self.MsgTemplate:GetComponent(typeof(UISprite)).height

    local news = CClientHouseMgr.Inst:GetNewsAt(index)
    if not news then
        return nil
    end

    local msgName = news.Message
    if msgName == "Furniture_Duration_Zero_New_2" then--气数折叠
        local msgParam = MsgPackImpl.unpack(news.MessageParam.Data)
        local newMsgContent = ""
        for i=0,msgParam.Count-1,1 do
            local itemId = msgParam[i]
            local data = Item_Item.GetData(itemId)
            if data then
                if i==msgParam.Count-1 then
                    newMsgContent = SafeStringFormat3("%s%s",newMsgContent,g_MessageMgr:FormatMessage("Furniture_Duration_Zero",data.Name))
                else
                    newMsgContent = SafeStringFormat3("%s%s",newMsgContent,g_MessageMgr:FormatMessage("Furniture_Duration_Zero_New_2",data.Name))
                end
            end
        end
        itemWnd.msgLbl.text = newMsgContent
        local time = CServerTimeMgr.ConvertTimeStampToZone8Time(news.Time)
        itemWnd.timeLbl.text = SafeStringFormat3("%s-%s-%s",time.Year,time.Month,time.Day)
        itemWnd.rewardGO:SetActive(false)
        if sprite then
            sprite.height = self.MsgTemplate:GetComponent(typeof(UISprite)).height+ self.m_SingleRowHeight*(msgParam.Count-1)
        end
    else   
        itemWnd:SetMsg(news)
        sprite.height = self.MsgTemplate:GetComponent(typeof(UISprite)).height + (itemWnd.msgLbl.height - self.m_SingleRowHeight)
    end 
    return cell
end

function LuaHouseMessageView:ShowMsgWnd()
    self.TableBody:LoadData(0)
    self.Bianbian.gameObject:SetActive(CClientHouseMgr.sbEnableJiayuanJiyu)
    local count = 0
    if CClientHouseMgr.Inst.mFurnitureProp then 
        count = CClientHouseMgr.Inst.mFurnitureProp.JiaYuanJiYuNpcs.Count
    end             
    self.Bianbian.text = count
end

function LuaHouseMessageView:UpdateMsgTable()
    self:ShowMsgWnd()
end

function LuaHouseMessageView:OnGetHouseNewsRewardSuccess(newsId)
    self:UpdateMsgTable()
end

function LuaHouseMessageView:OnSendHouseData(args)
    self:UpdateMsgTable()
end

function LuaHouseMessageView:OnEnable()
    self:Init0()
    self:ShowMsgWnd()
    g_ScriptEvent:AddListener("OnSendHouseData", self, "OnSendHouseData")
    g_ScriptEvent:AddListener("OnGetHouseNewsRewardSuccess", self, "OnGetHouseNewsRewardSuccess")
    --OnGetHouseNewsRewardSuccess
end

function LuaHouseMessageView:OnDisable()
    g_ScriptEvent:RemoveListener("OnSendHouseData", self, "OnSendHouseData")
    g_ScriptEvent:RemoveListener("OnGetHouseNewsRewardSuccess", self, "OnGetHouseNewsRewardSuccess")
end

--@region UIEvent

--@endregion UIEvent

