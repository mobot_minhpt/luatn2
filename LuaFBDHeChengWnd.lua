require("3rdParty/ScriptEvent")
require("common/common_include")
local CQianKunDaiMgr=import "L10.Game.CQianKunDaiMgr"
local LuaGameObject=import "LuaGameObject"
local LocalString = import "LocalString"
local MessageMgr = import "L10.Game.MessageMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local Item_Item = import "L10.Game.Item_Item"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CItemInfoMgr=import "L10.UI.CItemInfoMgr"
local AlignType=import "L10.UI.CItemInfoMgr+AlignType"
local UILabel = import "UILabel"
local Vector3 = import "UnityEngine.Vector3"
local CUIFx = import "L10.UI.CUIFx"
local DefaultItemActionDataSource=import "L10.UI.DefaultItemActionDataSource"
local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"
local FeiSheng_QianKunDai = import "L10.Game.FeiSheng_QianKunDai"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local TweenPosition = import "TweenPosition"
local Animation = import "UnityEngine.Animation"
local MessageWndManager = import "L10.UI.MessageWndManager"

CLuaFBDHeChengWnd=class()
RegistClassMember(CLuaFBDHeChengWnd,"mItemCell")
RegistClassMember(CLuaFBDHeChengWnd,"mBtnList")
RegistClassMember(CLuaFBDHeChengWnd,"mResLabel")
RegistClassMember(CLuaFBDHeChengWnd,"mBtnQuestion")
RegistClassMember(CLuaFBDHeChengWnd,"mBag")
RegistClassMember(CLuaFBDHeChengWnd,"mFx")
RegistClassMember(CLuaFBDHeChengWnd, "mStatusLabel")
RegistClassMember(CLuaFBDHeChengWnd, "m_Animation")

RegistClassMember(CLuaFBDHeChengWnd, "mTweening")
RegistClassMember(CLuaFBDHeChengWnd, "m_Tick1")
RegistClassMember(CLuaFBDHeChengWnd, "m_BtnTweeners")
RegistClassMember(CLuaFBDHeChengWnd, "m_ItemTweener")


function CLuaFBDHeChengWnd:Awake()
		CommonDefs.ListClear(CQianKunDaiMgr.Lua_SelectedFabaoItems)
		for i = 1, 6 do
			CommonDefs.ListAdd(CQianKunDaiMgr.Lua_SelectedFabaoItems, typeof(cs_string), "")
		end
		
		local statusLabelGO = self.transform:Find("Panel/Label").gameObject
		self.mStatusLabel = statusLabelGO:GetComponent(typeof(UILabel))
		 
    self.mItemCell=self.transform:Find("Panel/ItemCell").gameObject
    self.mItemCell:SetActive(false)
    CommonDefs.AddOnClickListener(self.mItemCell,DelegateFactory.Action_GameObject(function(go)
            self:OnItemCellClick(go)
        end),false)
    self.m_ItemTweener = self.mItemCell:GetComponent(typeof(TweenPosition))
    self.m_ItemTweener.enabled = false
        
    self.mBtnList= {}
    self.m_BtnTweeners = {}
    for i = 1, 6 do
    	local btn = self.transform:Find("Panel/BtnList/"..i).gameObject
    	local tween = btn:GetComponent(typeof(TweenPosition))
			if tween then
				self.m_BtnTweeners[i] = tween
				tween.enabled = false
			end
    	table.insert(self.mBtnList, btn)
    	CommonDefs.AddOnClickListener(btn,DelegateFactory.Action_GameObject(function(go)
            self:OnFabaoBtnClick(go)
        end),false)
    end
    local resLabelTrans = self.transform:Find("Panel/BtnList/Label")
    self.mResLabel = resLabelTrans:GetComponent(typeof(UILabel))
    self.mBtnQuestion = self.transform:Find("Panel/BtnQuestion").gameObject
    CommonDefs.AddOnClickListener(self.mBtnQuestion,DelegateFactory.Action_GameObject(function(go)
            self:OnBtnQuestionClick(go)
        end),false)
        
    self.mBag = self.transform:Find("Panel/Bottom/BagParent/Bag").gameObject
    CommonDefs.AddOnClickListener(self.mBag,DelegateFactory.Action_GameObject(function(go)
            self:OnBagClick(go)
        end),false)
        
    self.m_Animation = self.mBag:GetComponent(typeof(Animation))
    
    local fxGO = self.transform:Find("Panel/Bottom/BagParent/Bag/Fx")
    self.mFx = fxGO:GetComponent(typeof(CUIFx))
    self.mTweening = false
end

function CLuaFBDHeChengWnd:OnItemCellClick(go)
	local fabaodingItem = CQianKunDaiMgr.Lua_FaBaoDingItemId and CItemMgr.Inst:GetById(CQianKunDaiMgr.Lua_FaBaoDingItemId)
	if fabaodingItem then
		CItemInfoMgr.ShowLinkItemInfo(CQianKunDaiMgr.Lua_FaBaoDingItemId, false, nil, AlignType.Default, 0,0,0,0)
	end
end

function CLuaFBDHeChengWnd:OnFabaoBtnClick(go)
	for idx, btn in pairs(self.mBtnList) do
		if go == btn then
			if CQianKunDaiMgr.Lua_SelectedFabaoItems.Count >= idx and CQianKunDaiMgr.Lua_SelectedFabaoItems[idx-1] ~= "" then
				local t_name={}
			  local t_action={}
		  	t_name[1]=LocalString.GetString("取回")
			  t_action[1]=function()
		      CItemInfoMgr.CloseItemInfoWnd()
					CQianKunDaiMgr.Lua_SelectedFabaoItems[idx-1] = ""
					self:Init()
			  end
			  
			  local actionSource=DefaultItemActionDataSource.Create(1,t_action,t_name)
			  
				CItemInfoMgr.ShowLinkItemInfo(CQianKunDaiMgr.Lua_SelectedFabaoItems[idx-1], false, actionSource, AlignType.Default, 0,0,0,0)
			else
				CUIManager.ShowUI(CUIResources.SelectFBDHeChengEquip)
			end
		end
	end
end

function CLuaFBDHeChengWnd:OnBagClick(go)
	local count = 0
	local bContainPrecious = false
	for i = 1, CQianKunDaiMgr.Lua_SelectedFabaoItems.Count do
		if CQianKunDaiMgr.Lua_SelectedFabaoItems[i-1] ~= "" then
			local item = CItemMgr.Inst:GetById(CQianKunDaiMgr.Lua_SelectedFabaoItems[i-1])
			if item then
				count = count + 1
				if item.IsPrecious then
					bContainPrecious = true
				end
			end
		end
	end
	if count >= 6 then
		local onOk = function()
				CQianKunDaiMgr.RequestRefineTalismanTripod()
			end
		if bContainPrecious then
			local str=MessageMgr.Inst:FormatMessage("FaBaoDing_HeCheng_Confirm",{})
			MessageWndManager.ShowOKCancelMessage(str,DelegateFactory.Action(onOk),nil,LocalString.GetString("炼化"),nil,false)
		else
			onOk()
		end
	else
		g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("请先添加6个蓝法宝"))
	end
end

function CLuaFBDHeChengWnd:OnBtnQuestionClick(go)
		MessageMgr.Inst:ShowMessage("FaBaoDing_HeCheng_Question_Tip",{})
end

function CLuaFBDHeChengWnd:tweenToPos(targetPos, bForward)
	CUICommonDef.KillTween(self.mBag:GetHashCode())	
	self.mTweening = false
	local oriPos = self.mBag.transform.localPosition
	
	local xT = targetPos.x - oriPos.x
	local yT = targetPos.y - oriPos.y
	local onUpdate = function(p)
		local pos = Vector3(0, 0, 0)
		pos.x = xT*p + oriPos.x
		pos.y = yT*p + oriPos.y
		self.mBag.transform.localPosition = pos
	end
	
	local onComplete = function()
		self.mTweening  = false
		if bForward then
			CommonDefs.GetAnimationState(self.m_Animation, "QianKunDai_Shake").time = 0
			self.m_Animation:Play()
		end
	end

	CUICommonDef.Tween(1,DelegateFactory.Action_float(onUpdate),DelegateFactory.Action(onComplete),1,self.mBag:GetHashCode())
	self.mTweening = true
end

function CLuaFBDHeChengWnd:OnEnable()
  g_ScriptEvent:AddListener("OnFaBaoDingRefineDone", self, "Init")
  g_ScriptEvent:AddListener("OnFaBaoDingSelectChange", self, "Init")
  g_ScriptEvent:AddListener("QianKunDaiShakeFinish", self, "QianKunDaiShakeFinish") 
end

function CLuaFBDHeChengWnd:OnDisable()
	CUICommonDef.KillTween(self.mBag:GetHashCode())	
	CQianKunDaiMgr.Lua_FaBaoDingItemId = nil
	CommonDefs.ListClear(CQianKunDaiMgr.Lua_SelectedFabaoItems)
		
  g_ScriptEvent:RemoveListener("OnFaBaoDingRefineDone", self, "Init")
  g_ScriptEvent:RemoveListener("OnFaBaoDingSelectChange", self, "Init")
  g_ScriptEvent:RemoveListener("QianKunDaiShakeFinish", self, "QianKunDaiShakeFinish") 
  
  if self.m_Tick1~=nil then
    UnRegisterTick(self.m_Tick1)
    self.m_Tick1=nil
 end
end

function CLuaFBDHeChengWnd:QianKunDaiShakeFinish()
	self:ShowFx()
	
	local targetPos = Vector3.zero
	targetPos.y = -341
	targetPos.z = 1
	self:tweenToPos(targetPos, false)
	
	self:ShowItemCell()
end

function CLuaFBDHeChengWnd:ShowItemCell()
	local fabaodingItem = CQianKunDaiMgr.Lua_FaBaoDingItemId and CItemMgr.Inst:GetById(CQianKunDaiMgr.Lua_FaBaoDingItemId)
	if fabaodingItem then
		self.mItemCell:SetActive(true)
		
		self.m_ItemTweener.enabled = true
		self.m_ItemTweener:PlayForward()
		
		local ddata = Item_Item.GetData(fabaodingItem.TemplateId)
		if not ddata then
			return
		end
		
		local icon = LuaGameObject.GetChildNoGC(self.mItemCell.transform, "IconTexture").cTexture
		local nameLabel = LuaGameObject.GetChildNoGC(self.mItemCell.transform, "NameLabel").label
		local qualitySprite = LuaGameObject.GetChildNoGC(self.mItemCell.transform, "QualitySprite").sprite
		
		icon:LoadMaterial(ddata.Icon)
		nameLabel.text = ddata.Name
		qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(fabaodingItem.Item.QualityType)
	else
		self.mItemCell:SetActive(false)
	end
end

function CLuaFBDHeChengWnd:ShowBtnList()
	local fabaodingItem = CQianKunDaiMgr.Lua_FaBaoDingItemId and CItemMgr.Inst:GetById(CQianKunDaiMgr.Lua_FaBaoDingItemId)
	if fabaodingItem then
		self.mResLabel.gameObject:SetActive(false)
	else
		local count = 0
		for i = 0, 5 do
			local btn = self.mBtnList[i+1]
			btn:SetActive(true)
			
			local addSprite = LuaGameObject.GetChildNoGC(btn.transform, "AddSprite").gameObject
			local qualitySprite = LuaGameObject.GetChildNoGC(btn.transform, "QualitySprite").sprite
			local icon = LuaGameObject.GetChildNoGC(btn.transform, "IconTexture").cTexture
			
			local itemid = nil
			if CQianKunDaiMgr.Lua_SelectedFabaoItems.Count > i then
				itemid = CQianKunDaiMgr.Lua_SelectedFabaoItems[i]
			end
			local fabaodingItem = itemid and CItemMgr.Inst:GetById(itemid)
			if fabaodingItem then
				addSprite:SetActive(false)
				qualitySprite.gameObject:SetActive(true)
				icon.gameObject:SetActive(true)
				
				local ddata = EquipmentTemplate_Equip.GetData(fabaodingItem.TemplateId)
				if ddata then
					icon:LoadMaterial(ddata.Icon)
				end
				qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(fabaodingItem.Equip.QualityType)
				
				count = count + 1
			else
				addSprite:SetActive(true)
				qualitySprite.spriteName = "common_itemcell_border"
				icon.gameObject:SetActive(false)
			end
		end
		self.mResLabel.gameObject:SetActive(true)
		self.mResLabel.text = count .. "/6"
	end
end

function CLuaFBDHeChengWnd:ShowBag()
	local fabaodingItem = CQianKunDaiMgr.Lua_FaBaoDingItemId and CItemMgr.Inst:GetById(CQianKunDaiMgr.Lua_FaBaoDingItemId)
	if fabaodingItem then
		CUICommonDef.SetActive(self.mBag, false, true)
		CUICommonDef.SetActive(self.mBag, true, false)
		for i = 0, 5 do
			local tween = self.m_BtnTweeners[i+1]
			if tween then
				tween.enabled = true
				tween:PlayForward()
			end
		end
		
		self.m_Tick1=RegisterTickOnce(function()
				for i = 0, 5 do
					local btn = self.mBtnList[i+1]
					btn:SetActive(false)
					local tween = self.m_BtnTweeners[i+1]
					if tween then
						tween.enabled = false
					end
				end
				if not self.mTweening then
					local targetPos = Vector3.zero
					targetPos.y = 0
					targetPos.z = 0
					self:tweenToPos(targetPos, true)
				end
	    end, 800)
	else
		local count = 0
		for i = 0, 5 do
			local itemId = CQianKunDaiMgr.Lua_SelectedFabaoItems[i]
			local item = itemId and CItemMgr.Inst:GetById(itemId)
			if item then
				count = count + 1
			end
		end
		--CUICommonDef.SetActive(self.mBag, count >= 6, true)
		CUICommonDef.SetActive(self.mBag, true, false)
	end
end

function CLuaFBDHeChengWnd:ShowFx()
	self.mFx:DestroyFx()
	self.mFx:LoadFx("fx/ui/prefab/UI_mapilinfuzhaohuan_xingxing_zi.prefab")
end

function CLuaFBDHeChengWnd:Init()
	self:ShowBtnList()
	self:ShowBag()
	self:ShowStatusLabel()
end

function CLuaFBDHeChengWnd:ShowStatusLabel()
	local feishengDData = FeiSheng_QianKunDai.GetData()
	local total = feishengDData.NeedTalismanTripodNum
	local count = 0
	local itemProp = CClientMainPlayer.Inst.ItemProp
  local bagSize = itemProp:GetPlaceSize(EnumItemPlace.Task)
  for i = 1, bagSize do
  	local itemId = itemProp:GetItemAt(EnumItemPlace.Task, i)
  	local item = itemId and CItemMgr.Inst:GetById(itemId)
  	if item and item.TemplateId == feishengDData.TalismanTripodId then
  		count = count + item.Item.Count
  	end
  end
	
	self.mStatusLabel.text = SafeStringFormat3(LocalString.GetString("已合成%d个，还能合成%d个法宝鼎"), count, total-count)
end

return CLuaFBDHeChengWnd
