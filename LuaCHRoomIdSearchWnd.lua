local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"

LuaCHRoomIdSearchWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaCHRoomIdSearchWnd, "ClearSearchButton", GameObject)
RegistChildComponent(LuaCHRoomIdSearchWnd, "SearchBtn", GameObject)
RegistChildComponent(LuaCHRoomIdSearchWnd, "RoomIdInput", UIInput)
--@endregion RegistChildComponent end

function LuaCHRoomIdSearchWnd:Awake()
    UIEventListener.Get(self.ClearSearchButton).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnClearSearchButtonClick()
    end)
    UIEventListener.Get(self.SearchBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnSearchBtnClick()
    end)
    CommonDefs.AddEventDelegate(self.RoomIdInput.onChange, DelegateFactory.Action(function ()
        self:OnInputValueChanged()
    end))
end

function LuaCHRoomIdSearchWnd:Init()
    self.RoomIdInput.value = ""
    self.ClearSearchButton.gameObject:SetActive(false)
end

function LuaCHRoomIdSearchWnd:OnInputValueChanged()
    self.ClearSearchButton.gameObject:SetActive(not System.String.IsNullOrEmpty(self.RoomIdInput.value))
end

--@region UIEvent
function LuaCHRoomIdSearchWnd:OnClearSearchButtonClick()
    self.RoomIdInput.value = ""
end

function LuaCHRoomIdSearchWnd:OnSearchBtnClick()
    local roomId = tonumber(self.RoomIdInput.value) or 0
    LuaClubHouseMgr:QueryRoomList(roomId, LuaClubHouseMgr.m_RoomQueryType, LuaClubHouseMgr.m_RoomSortType, 1, "roomsearch")
    CUIManager.CloseUI(CLuaUIResources.CHRoomIdSearchWnd)
end
--@endregion UIEvent

