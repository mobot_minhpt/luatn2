local GameObject = import "UnityEngine.GameObject"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"

LuaYuanXiaoDaLuanDouSkillWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaYuanXiaoDaLuanDouSkillWnd, "SkillBtn", "SkillBtn", GameObject)

--@endregion RegistChildComponent end

function LuaYuanXiaoDaLuanDouSkillWnd:Awake()
    --@region EventBind: Dont Modify Manually!

		UIEventListener.Get(self.SkillBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)	    self:OnSkillBtnClick()	end)

    --@endregion EventBind end
end

function LuaYuanXiaoDaLuanDouSkillWnd:Init()

end

--@region UIEvent

function LuaYuanXiaoDaLuanDouSkillWnd:OnSkillBtnClick()
end


--@endregion UIEvent

