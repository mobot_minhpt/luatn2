EnumCityReplaceHelperReplaceType = {
    eSpecialReplace = "FIELD_NAME", -- 特殊替换模式，使用专用的替换函数
    eCommonReplace = "Common", -- 普通替换模式， 使用通用的替换函数
    eClientTaskNPCLocation = "TaskNPCLocation",
    eCrossMapPathHelper = "CrossMapPathHelper",
    eStringToUintArray = "StringToUintArray",
    eStringToUintUintArray = "StringToUintUintArray",
    eGuideKeyValuePair = "GuideKeyValuePair",
}

local eCommonReplace = EnumCityReplaceHelperReplaceType.eCommonReplace
local eSpecialReplace = EnumCityReplaceHelperReplaceType.eSpecialReplace
local eClientTaskNPCLocation = EnumCityReplaceHelperReplaceType.eClientTaskNPCLocation
local eCrossMapPathHelper = EnumCityReplaceHelperReplaceType.eCrossMapPathHelper
local eStringToUintArray = EnumCityReplaceHelperReplaceType.eStringToUintArray
local eStringToUintUintArray = EnumCityReplaceHelperReplaceType.eStringToUintUintArray
local eGuideKeyValuePair = EnumCityReplaceHelperReplaceType.eGuideKeyValuePair

CityReplaceHelperFieldDefine = {
    Message_Message_Message = {
        ReplaceFunctions = {gac = eCommonReplace, gas = eCommonReplace, master = eCommonReplace, im = eCommonReplace, playershop = eCommonReplace, gateway = eCommonReplace, paimai = eCommonReplace},
        PostActions = {gas = {"ReloadMessage"}, master = {"ReloadMessage"}, gateway = {"ReloadMessage"}, playershop = {"ReloadMessage"}},
    },

    -- PublicMap 相关
    PublicMap_PublicMap_Name = {
        ReplaceFunctions = {gac = eCommonReplace, gas = eCommonReplace, master = eCommonReplace},
    },

    PublicMap_PublicMap_Res = {
        ReplaceFunctions = {gac = eCommonReplace},
        PostActions = {master = {"DestoryScene"}},
    },

    PublicMap_PublicMap_Info = {
        ReplaceFunctions = {gas = eCommonReplace, master = eCommonReplace},
        PostActions = {master = {"DestoryScene"}},
    },

    PublicMap_PublicMap_NPC = {
        ReplaceFunctions = {gac = eCrossMapPathHelper, gas = eCommonReplace, master = eCommonReplace},
        PostActions = {gac = {"ReloadCrossMapPath", "ReloadLocation"}, gas = {"ReloadCrossMapPath", "ReloadLocation", "ReloadMapEntrance"}, master = {"DestoryScene"}},
    },

    PublicMap_PublicMap_Monster = {
        ReplaceFunctions = {gac = eCrossMapPathHelper, gas = eCommonReplace, master = eCommonReplace},
        PostActions = {gac = {"ReloadLocation"}, gas = {"ReloadLocation"}, master = {"DestoryScene"}},
    },

    PublicMap_PublicMap_Pick = {
        ReplaceFunctions = {gac = eCrossMapPathHelper, gas = eCommonReplace, master = eCommonReplace},
        PostActions = {gac = {"ReloadLocation"}, gas = {"ReloadLocation"}, master = {"DestoryScene"}},
    },

    PublicMap_PublicMap_Temple = {
        ReplaceFunctions = {gac = eCrossMapPathHelper, gas = eCommonReplace, master = eCommonReplace},
        PostActions = {gac = {"ReloadLocation"}, gas = {"ReloadLocation"}, master = {"DestoryScene"}},
    },

    PublicMap_PublicMap_Teleport = {
        ReplaceFunctions = {gac = eSpecialReplace, gas = eCommonReplace, master = eCommonReplace},
        PostActions = {gac = {"ReloadCrossMapPath"}, gas = {"ReloadCrossMapPath", "ReloadMapEntrance", "ResetMapTeleportCache"}, master = {"DestoryScene"}},
    },

    PublicMap_PublicMap_AssistantTeleport = {
        ReplaceFunctions = {gac = eCrossMapPathHelper, gas = eCommonReplace, master = eCommonReplace},
        PostActions = {gac = {"ReloadCrossMapPath"}, gas = {"ReloadCrossMapPath"}},
    },

    PublicMap_PublicMap_TeleportFxType = {
        ReplaceFunctions = {gac = eSpecialReplace},
    },

    PublicMap_PublicMap_RelivePos = {
        ReplaceFunctions = {gas = eCommonReplace, master = eCommonReplace},
        PostActions = {gas = {"ReloadSceneRelivePos"}}
    },

    PublicMap_PublicMap_LevelTeleportDestPos = {
        ReplaceFunctions = {gas = eCommonReplace, master = eCommonReplace},
    },

    PublicMap_PublicMap_JiaJuInfo = {
        ReplaceFunctions = {gas = eCommonReplace, master = eCommonReplace},
        PostActions = {master = {"DestoryScene"}}
    },

    PublicMap_PublicMap_MapParameter = {
        ReplaceFunctions = {gas = eCommonReplace, master = eCommonReplace},
        PostActions = {master = {"DestoryScene"}}
    },

    PublicMap_PublicMap_Weather = {
        ReplaceFunctions = {gas = eCommonReplace, master = eCommonReplace},
        PostActions = {gas = {"ReloadSceneWeather"}}
    },

    PublicMap_PublicMap_View3D = {
        ReplaceFunctions = {gac = eCommonReplace},
    },
    

    -- !!  只支持修改里面的传送坐标！
    Dialog_Dialog_Choice = {
        ReplaceFunctions = {gac = eSpecialReplace, gas = eCommonReplace},
        PostActions = {gac = {"ReloadCrossMapPath"}, gas = {"ReloadDialog", "ReloadMapEntrance", "ReloadCrossMapPath"}},
    },

    -- Task相关
    Task_Task_Display = {
        ReplaceFunctions = {gac = eCommonReplace, gas = eCommonReplace, master = eCommonReplace},
    },

    Task_Task_BeginNPC = {
        ReplaceFunctions = {gac = eCommonReplace, gas = eCommonReplace, master = eCommonReplace},
        PostActions = {gas = {"ReloadTask"}},
    },

    Task_Task_BeginNPCPos = {
        ReplaceFunctions = {gac = eClientTaskNPCLocation, gas = eCommonReplace},
        PostActions = {gas = {"ReloadTask"}},
        ClientAliasName = "BeginNPCLocation",
    },

    Task_Task_SubmitNPC = {
        ReplaceFunctions = {gac = eCommonReplace, gas = eCommonReplace, master = eCommonReplace},
        PostActions = {gas = {"ReloadTask"}},
    },

    Task_Task_SubmitNPCPos = {
        ReplaceFunctions = {gac = eClientTaskNPCLocation, gas = eCommonReplace, master = eCommonReplace},
        PostActions = {gas = {"ReloadTask"}},
        ClientAliasName = "SubmitNPCLocation",
    },

    Task_Task_EntranceNPCPos = {
        ReplaceFunctions = {gac = eClientTaskNPCLocation, gas = eCommonReplace, master = eCommonReplace},
        PostActions = {gas = {"ReloadTask"}},
        ClientAliasName = "EntranceNPCLocation",
    },

    Task_Task_TaskDescription = {
        ReplaceFunctions = {gac = eCommonReplace},
    },

    Task_Task_UseItem = {
        ReplaceFunctions = {gac = eSpecialReplace, gas = eCommonReplace, master = eCommonReplace},
        PostActions = {gac = {"FixUseItemTaskConditionLocation"}, gas = {"ReloadTask"}},
    },

    -- 客户端解析的内容只有事件名和数量，可能带有坐标的剩余参数并未解析，因此客户端上不作处理。
    Task_Task_NeedEvents = {
        ReplaceFunctions = {gas = eCommonReplace, master = eCommonReplace},
        PostActions = {gas = {"ReloadTask"}},
    },

    -- !!!注意这个替换目前只支持修改坐标!!!!!!
    Task_Task_Locations = {
        ReplaceFunctions = {gac = eSpecialReplace, gas = eCommonReplace, master = eCommonReplace},
        PostActions = {gas = {"ReloadTask"}},
    },

    Task_Task_Status = {
        ReplaceFunctions = {gac = eCommonReplace, gas = eCommonReplace, master = eCommonReplace},
        PostActions = {gas = {"ReloadTask"}, master = {"ReloadTaskActivity"}}
    },

    Task_Task_ShowTimeoutTips = {
        ReplaceFunctions = {gac = eCommonReplace, gas = eCommonReplace, master = eCommonReplace},
    },

    Task_Task_TaskCheck = {
        ReplaceFunctions = {gas = eCommonReplace, master = eCommonReplace},
        PostActions = {gas = {"ReloadTask"}},
    },

    Task_Schedule_Action = {
        ReplaceFunctions = {gac = eCommonReplace},
    },

    -- ShiTu_CaiChengYuPlace_Point、ShiTu_ShanHaiJingItemPlace_Point这两个的修改
    -- 还涉及到玩家身上道具和任务寻路数据的修改。

    ShiTu_CaiChengYuPlace_Point = {
        ReplaceFunctions = {gas = eCommonReplace, master = eCommonReplace},
        PostActions = {gas = {"ReloadShiTuPlayInfo"}},
    },

    ShiTu_ShanHaiJingItemPlace_Point = {
        ReplaceFunctions = {gas = eCommonReplace, master = eCommonReplace},
        PostActions = {gas = {"ReloadShiTuPlayInfo"}},
    },

    ShiTu_Setting_BaiShiVideoName = {
        ReplaceFunctions = {gas = eCommonReplace, master = eCommonReplace},
        PostActions = {gas = {"ReloadBaishiVideoName"}},
    },

    GameplayItem_YiZhangLocations_Locations = {
        ReplaceFunctions = {gac = eSpecialReplace, gas = eCommonReplace, master = eCommonReplace},
        PostActions = {},
    },

    Item_Item_HowToUse = {
        ReplaceFunctions = {gac = eCommonReplace},
    },

    Item_Item_Description = {
        ReplaceFunctions = {gac = eCommonReplace},
    },

    Initialization_Init_Location = {
        ReplaceFunctions = {gas = eCommonReplace, master = eCommonReplace, database = eCommonReplace},
        PostActions = {gas = {"ReloadNewRoleLocationInfo"}, master = {"ReloadNewRoleLocationInfo"}},
    },

    ChuJia_Setting_ChuJia_Leave_Teleport_DestPos = {
        ReplaceFunctions = {gas = eCommonReplace},
    },

    ChunJie_Setting_XUYUANSHU_ZUOBIAO = {
        ReplaceFunctions = {gas = eCommonReplace, master = eCommonReplace},
        PostActions = {gas = {"ReloadChunJieXuYuanShuInfo"}}
    },

    BiWu_Setting_BiWuFlagInfo = {
        ReplaceFunctions = {gas = eCommonReplace, master = eCommonReplace},
        PostActions = {gas = {"ReloadBiWuFlagInfo"}}
    },

    BiWu_Setting_AddFlagMapId = {
        ReplaceFunctions = {master = eCommonReplace},
        PostActions = {master = {"ReloadBiWuAddFlagMapId"}}
    },

    Concert_Setting_ConcertNpc = {
        ReplaceFunctions = {gas = eCommonReplace},
        PostActions = {gas = {"ReloadConcertConfig"}}
    },

    DuoHun_Setting_ZhaoHunTeleportPos = {
        ReplaceFunctions = {gas = eCommonReplace},
        PostActions = {gas = {"ReloadDuoHunonfig"}}
    },

    DuoHun_UseSkillPos_Pos = {
        ReplaceFunctions = {gas = eCommonReplace},
        PostActions = {gas = {"ReloadDuoHunonfig"}}
    },

    FestivalGift_Festival_ShiTuMailContent = {
        ReplaceFunctions = {gas = eCommonReplace},
    },

    DouHunTan_Setting_StatuePosInfo = {
        ReplaceFunctions = {gac = eStringToUintUintArray, gas = eCommonReplace},
        PostActions = {gas = {"ReloadDouHunStatuePosInfo"}}
    },

    Gameplay_PlayUpdateAlert_Action = {
        ReplaceFunctions = {gac = eCommonReplace, gas = eCommonReplace, master = eCommonReplace},
    },

    HuaKui_Setting_HuaKuiStatueConfig = {
        ReplaceFunctions = {gas = eCommonReplace, master = eCommonReplace},
        PostActions = {gas = {"ReloadHuaKuiStatueConfig"}, master = {"ReloadHuaKuiStatueConfig"}}
    },

    FeiSheng_Setting_FeiShengFinishTransport = {
        ReplaceFunctions = {gas = eCommonReplace},
        PostActions = {gas = {"ReloadFeiShengFinishTransport"}}
    },

    JiangXueYuanShuang_Setting_ResurrectionPoints = {
        ReplaceFunctions = {gas = eCommonReplace},
        PostActions = {gas = {"ReloadJXYSResurrectionPoints"}}
    },

    MenPaiTiaoZhan_Setting_SafeRegionPoint = {
        ReplaceFunctions = {gas = eCommonReplace},
        PostActions = {gas = {"ReloadMPTZSafeRegionPoint"}}
    },

    MenPaiTiaoZhan_Setting_AllowSceneIds = {
        ReplaceFunctions = {gas = eCommonReplace},
        PostActions = {gas = {"ReloadMPTZAllowSceneIds"}}
    },

    Mskk_NpcPlace_Location = {
        ReplaceFunctions = {gas = eCommonReplace, master = eCommonReplace},
        PostActions = {master = {"ReloadMskkNpcLocation"}}
    },

    Bail_Setting_NpcScenePosX = {
        ReplaceFunctions = {gas = eCommonReplace},
        PostActions = {gas = {"ReloadBaoShiNpcPos"}}
    },

    Bail_Setting_NpcScenePosY = {
        ReplaceFunctions = {gas = eCommonReplace},
        PostActions = {gas = {"ReloadBaoShiNpcPos"}}
    },

    Bail_Trade_NpcScenePosX = {
        ReplaceFunctions = {gas = eCommonReplace},
        PostActions = {gas = {"ReloadBaoShiNpcPos"}}
    },

    Bail_Trade_NpcScenePosY = {
        ReplaceFunctions = {gas = eCommonReplace},
        PostActions = {gas = {"ReloadBaoShiNpcPos"}}
    },

    GameSetting_Common_DaShenNpcInfo = {
        ReplaceFunctions = {gas = eCommonReplace},
        PostActions = {gas = {"ReloadDaShenNpcConfig"}}
    },

    GameSetting_Common_ReplacedDaShenNpcId = {
        ReplaceFunctions = {gas = eCommonReplace},
        PostActions = {gas = {"ReloadDaShenNpcConfig"}}
    },

    GameSetting_Server_PKLeavePrisonLoc = {
        ReplaceFunctions = {gas = eCommonReplace},
        PostActions = {gas = {"ReloadPkLeavePrisonLoc"}}
    },

    GameSetting_Server_EnemyGuildCityRelivePos = {
        ReplaceFunctions = {gas = eCommonReplace},
    },

    GameSetting_Server_NewRoleTeleportLimitScene = {
        ReplaceFunctions = {gas = eCommonReplace},
        PostActions = {gas = {"ReloadNewRoleLimitSceneConfig"}}
    },

    GameSetting_Server_NEW_PLAYER_LOGIN_COPY = {
        ReplaceFunctions = {gas = eCommonReplace},
    },

    GameSetting_Server_DefaultFallbackTeleportPos = {
        ReplaceFunctions = {gas = eCommonReplace, database = eCommonReplace, master = eCommonReplace},
        PostActions = {gas = {"ReloadDefaultFallbackTeleportPos"}, master = {"ReloadDefaultFallbackTeleportPos"}, database = {"ReloadDefaultFallbackTeleportPos"}}
    },

    GameSetting_Server_NewRolePlayBossId = {
        ReplaceFunctions = {gas = eCommonReplace},
        PostActions = {gas = {"ReloadNewRolePlayBossId"}}
    },

    GameSetting_Common_NEW_PLAYER_LOGIN_COPY = {
        ReplaceFunctions = {gas = eCommonReplace},
    },

    GameSetting_Client_NEW_PLAYER_LOGIN_COPY = {
        ReplaceFunctions = {gac = eCommonReplace},
    },

    GameSetting_Client_NewPlayerFirstTaskId = {
        ReplaceFunctions = {gac = eCommonReplace},
        PostActions = {gac = {"ReloadNewPlayerFirstTaskId"}}
    },

    House_Setting_XinYiClubExitPos = {
        ReplaceFunctions = {gas = eCommonReplace},
        PostActions = {gas = {"ReloadXinYiClubExitPos"}}
    },

    MergeVote_Setting_NPC = {
        ReplaceFunctions = {gas = eCommonReplace, master = eCommonReplace},
        PostActions = {gas = {"ReloadMergeVoteNpc"}}
    },

    HouseCompetition_Setting_TargetPos = {
        ReplaceFunctions = {gas = eCommonReplace, master = eCommonReplace},
    },

    XiaLvPK_Setting_TeleportPos = {
        ReplaceFunctions = {gas = eCommonReplace, master = eCommonReplace},
        PostActions = {gas = {"ReloadXiaLvPkTeleportPos"}}
    },

    XiaLvPK_Setting_CrossPlayExitPos = {
        ReplaceFunctions = {gas = eCommonReplace, master = eCommonReplace},
        PostActions = {gas = {"ReloadXiaLvPkCrossPlayExitPos"}}
    },

    XiaLvPK_Setting_StatuePosInfo = {
        ReplaceFunctions = {gas = eCommonReplace, master = eCommonReplace},
        PostActions = {gas = {"ReloadXiaLvPkStatuePosInfo"}}
    },

    WaBao_Place_Point = {
        ReplaceFunctions = {gac = eStringToUintArray, gas = eCommonReplace, master = eCommonReplace},
        PostActions = {gas = {"ReloadWaBaoPlace"}}

    },

    WaBao_PuzzlePlace_Point = {
        ReplaceFunctions = {gac = eStringToUintArray, gas = eCommonReplace, master = eCommonReplace},
        PostActions = {gas = {"ReloadWaBaoPlace"}}
    },

    WaBao_PuzzlePlace_Puzzle = {
        ReplaceFunctions = {gac = eCommonReplace, gas = eCommonReplace, master = eCommonReplace},
    },

    Gameplay_Gameplay_Entrance = {
        ReplaceFunctions = {gas = eCommonReplace, master = eCommonReplace},
    },

    Gameplay_Gameplay_Scenario = {
        ReplaceFunctions = {gas = eCommonReplace, master = eCommonReplace},
    },

    QingMingJiJiu_Setting_EliminateSafeAreaMapId = {
        ReplaceFunctions = {gas = eCommonReplace, master = eCommonReplace},
        PostActions = {gas = {"ReloadYuanHunJiEliminateSafeAreaMapId"}, master = {"ReloadYuanHunJiEliminateSafeAreaMapId"}}
    },

    QingMingJiJiu_Setting_YuanHunJiResurrectionPos = {
        ReplaceFunctions = {gas = eCommonReplace, master = eCommonReplace},
        PostActions = {gas = {"ReloadYuanHunJiYuanHunJiResurrectionPos"}}
    },

    Monster_Monster_PrefabPath = {
        ReplaceFunctions = {gac = eCommonReplace},
    },
    
    NPC_NPC_PrefabPath = {
        ReplaceFunctions = {gac = eCommonReplace},
    },

    NPC_NPC_Portrait = {
        ReplaceFunctions = {gac = eCommonReplace},
    },

    Wedding_Parade_Hangzhou_Parade_Route = {
        ReplaceFunctions = {gas = eCommonReplace, master = eCommonReplace},
        PostActions = {gas = {"ReloadHangZhouParadeRoute"}}
    },

    Wedding_Parade_Jinling_Parade_Route = {
        ReplaceFunctions = {gas = eCommonReplace, master = eCommonReplace},
        PostActions = {gas = {"ReloadJinLingParadeRoute"}}
    },

    Wedding_Parade_Hangzhou_QiangQin_Npc = {
        ReplaceFunctions = {gas = eCommonReplace, master = eCommonReplace},
        PostActions = {gas = {"ReloadQiangQinInfo"}}
    },

    Wedding_Parade_Hangzhou_QiangQin_CheckPos = {
        ReplaceFunctions = {gas = eCommonReplace, master = eCommonReplace},
        PostActions = {gas = {"ReloadQiangQinInfo"}}
    },

    Wedding_Parade_Jinling_QiangQin_Npc = {
        ReplaceFunctions = {gas = eCommonReplace, master = eCommonReplace},
        PostActions = {gas = {"ReloadQiangQinInfo"}}
    },

    Wedding_Parade_Jinling_QiangQin_CheckPos = {
        ReplaceFunctions = {gas = eCommonReplace, master = eCommonReplace},
        PostActions = {gas = {"ReloadQiangQinInfo"}}
    },

    Baby_Setting_YangYuNpcId = {
        ReplaceFunctions = {gas = eCommonReplace, master = eCommonReplace},
        PostActions = {gas = {"ReloadYangYuNpc"}},
    },

    BodyWeight_TimePoint_FatMoreContent = {
        ReplaceFunctions = {gas = eCommonReplace},
    },

    BodyWeight_TimePoint_FatLessContent = {
        ReplaceFunctions = {gas = eCommonReplace},
    },

    BodyWeight_TimePoint_NormalMoreContent = {
        ReplaceFunctions = {gas = eCommonReplace},
    },

    BodyWeight_TimePoint_NormalLessContent = {
        ReplaceFunctions = {gas = eCommonReplace},
    },

    BodyWeight_TimePoint_ThinMoreContent = {
        ReplaceFunctions = {gas = eCommonReplace},
    },

    BodyWeight_TimePoint_ThinLessContent = {
        ReplaceFunctions = {gas = eCommonReplace},
    },

    BodyWeight_TimePoint_NoChangeContent = {
        ReplaceFunctions = {gas = eCommonReplace},
    },

    YangYuShaoNianQi_Setting_Direction = {
        ReplaceFunctions = {gas = eCommonReplace},
        PostActions = {gas = {"ReloadYangYuShaoNianQiSettingDirection"}},
    },

    Relive_Setting_RELIVE_OUTPLAY_LOC = {
        ReplaceFunctions = {gas = eCommonReplace},
        PostActions = {gas = {"ReloadReliveOutplayLoc"}},
    },

    ZhaoQin_Setting_NpcLocation = {
        ReplaceFunctions = {gas = eCommonReplace, master = eCommonReplace},
        PostActions = {master = {"ReloadZhaoQinNpcLocation"}},
    },

    GuildChallenge_Occupation_Maps_PixelPosition = {
        ReplaceFunctions = {gas = eCommonReplace, master = eCommonReplace},
    },

    YangYuShaoNianQi_Setting_Camera = {
        ReplaceFunctions = {gac = eCommonReplace},
    },

    Task_Task_EndEvent = {
        ReplaceFunctions = {gas = eCommonReplace, master = eCommonReplace},
        PostActions = {gas = {"ReloadTaskEndEvent"}},
    },

    Mail_Mail_Content = {
        ReplaceFunctions = {gac = eCommonReplace, gas = eCommonReplace, master = eCommonReplace},
    },

    ShengDan_Setting_NpcPos = {
        ReplaceFunctions = {gas = eCommonReplace, master = eCommonReplace},
        PostActions = {gas = {"ReloadShengDanNpcPos"}},
    },

    JieBai_Setting_JieBaiPos = {
        ReplaceFunctions = {gas = eCommonReplace},
        PostActions = {gas = {"ReloadJieBaiPos"}},
    },

    JieBai_FirePos_Pos = {
        ReplaceFunctions = {gas = eCommonReplace},
        PostActions = {gas = {"ReloadJieBaiFirePos"}},
    },

    JieBai_Setting_JieBaiVideoName = {
        ReplaceFunctions = {gac = eCommonReplace, gas = eCommonReplace},
    },

    JieBai_Setting_DongHuaInterval = {
        ReplaceFunctions = {gas = eCommonReplace},
        PostActions = {gas = {"ReloadJieBaiDongHuaInterval"}}
    },

    Story_Settings_RelivePos = {
        ReplaceFunctions = {gas = eCommonReplace},
        PostActions = {gas = {"ReloadStoryRelivePos"}}
    },

    QingQiuPlay_GameSetting_RelivePos = {
        ReplaceFunctions = {gas = eCommonReplace, master = eCommonReplace},
        PostActions = {gas = {"ReloadQingQiuPlayRelivePos"}}
    },

    Guide_Guide_TriggerConditionParams = {
        ReplaceFunctions = {gac = eGuideKeyValuePair},
        ClientAliasName = "TriggerConditionParamSet"
    },

    Guide_Guide_TriggeredActionParams = {
        ReplaceFunctions = {gac = eGuideKeyValuePair},
        ClientAliasName = "TriggeredActionParamSet"
    },

    Guide_Guide_Force = {
        ReplaceFunctions = {gac = eCommonReplace},
    },

}

-- 默认序order是0，order大的后执行。
CityReplaceHelperPostActionOrder = {
    ReloadTask = 0,
    ReloadLocation = 1, -- ReloadLocation要放在ReloadTask后面。
    FixUseItemTaskConditionLocation = 2,
    DestoryScene = 101,
}

