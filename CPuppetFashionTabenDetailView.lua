-- Auto Generated!!
local Boolean = import "System.Boolean"
local Byte = import "System.Byte"
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CFashionPreviewMgr = import "L10.UI.CFashionPreviewMgr"
local CFreightEquipSubmitMgr = import "L10.UI.CFreightEquipSubmitMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CPuppetClothesMgr = import "L10.UI.CPuppetClothesMgr"
local CPuppetFashionTabenDetailView = import "L10.UI.CPuppetFashionTabenDetailView"
local CUICommonDef = import "L10.UI.CUICommonDef"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local EnumPreviewType = import "L10.UI.EnumPreviewType"
local EnumQualityType = import "L10.Game.EnumQualityType"
local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"
local EventManager = import "EventManager"
local Fashion_Setting = import "L10.Game.Fashion_Setting"
local HousePuppet_CPPuppet = import "L10.Game.HousePuppet_CPPuppet"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local Mall_LingYuMall = import "L10.Game.Mall_LingYuMall"
local MessageWndManager = import "L10.UI.MessageWndManager"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local CFashionMgr = import "L10.Game.CFashionMgr"

CPuppetFashionTabenDetailView.m_Init_CS2LuaHook = function (this) 
    this.itemTemplateId = System.UInt32.Parse(Fashion_Setting.GetData("Taben_Translate_ItemId").Value)

    local item = CItemMgr.Inst:GetItemTemplate(this.itemTemplateId)
    local data = Mall_LingYuMall.GetData(this.itemTemplateId)
    this.moneyCtrl:SetType(data.CanUseBindJade > 0 and EnumMoneyType.LingYu_WithBind or EnumMoneyType.LingYu, EnumPlayScoreKey.NONE, true)
    this.moneyCtrl:SetCost(data.Jade)
    this.itemGo:SetActive(true)
    this.moneyCtrl.gameObject:SetActive(false)
    this.itemIconTexture:LoadMaterial(item.Icon)
    this:InitItemAmount()
    this.useLingyuCheckBox.Selected = false
    this.useLingyuCheckBox.OnValueChanged = MakeDelegateFromCSFunction(this.OnCheckBoxValueChanged, MakeGenericClass(Action1, Boolean), this)

    this.rawMatQualitySprite.spriteName = CUICommonDef.GetItemCellBorder(EnumQualityType.White)
    this.rawMatIconTexture.material = nil
    this.previewIconTexture.material = nil
    this.curRawMaterial = nil
    this.tabenNameLabel.text = ""
end
CPuppetFashionTabenDetailView.m_InitItemAmount_CS2LuaHook = function (this) 
    local bindCount, notBindCount
    bindCount, notBindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(this.itemTemplateId)
    if bindCount + notBindCount > 0 then
        this.itemCountLabel.text = System.String.Format("{0}/1", bindCount + notBindCount)
        this.acquireGo:SetActive(false)
    else
        this.itemCountLabel.text = "[c][FF0000]0[-]/1"
        this.acquireGo:SetActive(true)
    end
end
CPuppetFashionTabenDetailView.m_InitLingyuCtrl_CS2LuaHook = function (this) 
    this.itemTemplateId = System.UInt32.Parse(Fashion_Setting.GetData("Taben_Translate_ItemId").Value)
    local bindCount, notBindCount
    bindCount, notBindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(this.itemTemplateId)
    local item = CItemMgr.Inst:GetItemTemplate(this.itemTemplateId)
    if bindCount + notBindCount == 0 and this.useLingyuCheckBox.Selected then
        this.itemGo:SetActive(false)
        this.moneyCtrl.gameObject:SetActive(true)
    else
        this.itemGo:SetActive(true)
        this.moneyCtrl.gameObject:SetActive(false)
    end
end
CPuppetFashionTabenDetailView.m_OnEnable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.changeRawMatButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.changeRawMatButton).onClick, MakeDelegateFromCSFunction(this.OnChangeRawMatButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.previewButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.previewButton).onClick, MakeDelegateFromCSFunction(this.OnPreviewButtonClick, VoidDelegate, this), true)
    --UIEventListener.Get(rawMatGo).onClick += this.OnRawMatGoClick;
    UIEventListener.Get(this.makeTabenButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.makeTabenButton).onClick, MakeDelegateFromCSFunction(this.OnMakeTabenButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.itemGo).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.itemGo).onClick, MakeDelegateFromCSFunction(this.OnItemClick, VoidDelegate, this), true)
    UIEventListener.Get(this.tips).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.tips).onClick, MakeDelegateFromCSFunction(this.OnTipsClick, VoidDelegate, this), true)
    UIEventListener.Get(this.acquireGo).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.acquireGo).onClick, MakeDelegateFromCSFunction(this.OnAcquireGoClick, VoidDelegate, this), true)

    EventManager.AddListenerInternal(EnumEventType.OnFreightSubmitEquipSelected, MakeDelegateFromCSFunction(this.OnEquipmentSelect, MakeGenericClass(Action2, String, String), this))
    EventManager.AddListenerInternal(EnumEventType.SendItem, MakeDelegateFromCSFunction(this.OnSendItem, MakeGenericClass(Action1, String), this))
    EventManager.AddListenerInternal(EnumEventType.SetItemAt, MakeDelegateFromCSFunction(this.OnSetItemAt, MakeGenericClass(Action4, EnumItemPlace, UInt32, String, String), this))
end
CPuppetFashionTabenDetailView.m_OnDisable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.changeRawMatButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.changeRawMatButton).onClick, MakeDelegateFromCSFunction(this.OnChangeRawMatButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.previewButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.previewButton).onClick, MakeDelegateFromCSFunction(this.OnPreviewButtonClick, VoidDelegate, this), false)
    --UIEventListener.Get(rawMatGo).onClick -= this.OnRawMatGoClick;
    UIEventListener.Get(this.makeTabenButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.makeTabenButton).onClick, MakeDelegateFromCSFunction(this.OnMakeTabenButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.itemGo).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.itemGo).onClick, MakeDelegateFromCSFunction(this.OnItemClick, VoidDelegate, this), false)
    UIEventListener.Get(this.tips).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.tips).onClick, MakeDelegateFromCSFunction(this.OnTipsClick, VoidDelegate, this), false)
    UIEventListener.Get(this.acquireGo).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.acquireGo).onClick, MakeDelegateFromCSFunction(this.OnAcquireGoClick, VoidDelegate, this), false)

    EventManager.RemoveListenerInternal(EnumEventType.OnFreightSubmitEquipSelected, MakeDelegateFromCSFunction(this.OnEquipmentSelect, MakeGenericClass(Action2, String, String), this))
    EventManager.RemoveListenerInternal(EnumEventType.SendItem, MakeDelegateFromCSFunction(this.OnSendItem, MakeGenericClass(Action1, String), this))
    EventManager.RemoveListenerInternal(EnumEventType.SetItemAt, MakeDelegateFromCSFunction(this.OnSetItemAt, MakeGenericClass(Action4, EnumItemPlace, UInt32, String, String), this))
end
CPuppetFashionTabenDetailView.m_OnEquipmentSelect_CS2LuaHook = function (this, id, key) 
    local equip = CItemMgr.Inst:GetById(id)
    if equip ~= nil and equip.IsEquip then
        this.rawMatIconTexture:LoadMaterial(equip.Icon)
        this.rawMatQualitySprite.spriteName = CUICommonDef.GetItemCellBorder(equip.Equip.QualityType)
        this.curRawMaterial = equip
        this.previewIconTexture:LoadMaterial(equip.Icon)
        this.tabenNameLabel.text = System.String.Format(LocalString.GetString("{0}·拓本"), equip.Name)
    elseif this.curRawMaterial == nil then
        this.rawMatIconTexture.material = nil
        this.previewIconTexture.mainTexture = nil
        this.rawMatQualitySprite.spriteName = CUICommonDef.GetItemCellBorder(EnumQualityType.White)
    end
end
CPuppetFashionTabenDetailView.m_OnChangeRawMatButtonClick_CS2LuaHook = function (this, go) 
    --更换原材料
    if CommonDefs.DictContains(CClientHouseMgr.Inst.mCurFurnitureProp.PuppetInfo, typeof(Byte), CPuppetClothesMgr.selectIdx) then
        local puppetInfo = CommonDefs.DictGetValue(CClientHouseMgr.Inst.mCurFurnitureProp.PuppetInfo, typeof(Byte), CPuppetClothesMgr.selectIdx)
        if puppetInfo == nil then
            return
        end
        local cppuppet = HousePuppet_CPPuppet.GetData(puppetInfo.Id)
        if cppuppet == nil then
            return
        end
        local dic = CreateFromClass(MakeGenericClass(Dictionary, Int32, MakeGenericClass(List, String)))
        local equipList = CreateFromClass(MakeGenericClass(List, String))
        local bagSize = CClientMainPlayer.Inst.ItemProp:GetPlaceSize(EnumItemPlace.Bag)
        do
            local i = 1
            while i <= bagSize do
                local id = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Bag, i)
                if not System.String.IsNullOrEmpty(id) then
                    local equip = CItemMgr.Inst:GetById(id)
                    if equip ~= nil and equip.IsEquip then
                        local template = EquipmentTemplate_Equip.GetData(equip.TemplateId)
                        if template ~= nil and equip.Equip:GetEquipIsFit(cppuppet.Class, cppuppet.Gender) and (CFashionMgr.IsFashionTypePos(template.Type)) then
                            CommonDefs.ListAdd(equipList, typeof(String), equip.Id)
                        end
                    end
                end
                i = i + 1
            end
        end
        if equipList.Count > 0 then
            CommonDefs.DictAdd(dic, typeof(Int32), 0, typeof(MakeGenericClass(List, String)), equipList)
            CFreightEquipSubmitMgr.OpenEquipmentChooseWnd("", dic, LocalString.GetString("选择拓本原材料"), 0, false, LocalString.GetString("选择"), "", nil, "")
        else
            g_MessageMgr:ShowMessage("NO_EQUIP_FOR_TABEN")
        end
    end
end
CPuppetFashionTabenDetailView.m_OnPreviewButtonClick_CS2LuaHook = function (this, go) 
    --预览
    if this.curRawMaterial ~= nil then
        CFashionPreviewMgr.FashionLevel = this.curRawMaterial.Equip.ForgeLevel
        CFashionPreviewMgr.curPreviewType = EnumPreviewType.PreviewPuppetWeaponTaben
        CFashionPreviewMgr.ShowFashionPreview(this.curRawMaterial.TemplateId)
    else
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("还没有选择拓本原料呢！"))
    end
end
CPuppetFashionTabenDetailView.m_OnMakeTabenButtonClick_CS2LuaHook = function (this, go) 
    --制作拓本
    if this.curRawMaterial ~= nil then
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("TABEN_CONFIRM_MESSAGE", this.curRawMaterial.Name), DelegateFactory.Action(function () 
            local pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, this.curRawMaterial.Id)
            --Gac2Gas.RequestCompositeTaben((uint)EnumItemPlace.Bag, pos, useLingyuCheckBox.Selected);
            Gac2Gas.PlayerRequestAddTabenForPuppet(CPuppetClothesMgr.selectIdx, EnumItemPlace_lua.Bag, pos, this.curRawMaterial.Equip.Id, this.useLingyuCheckBox.Selected)
            CUIManager.CloseUI(CUIResources.PuppetFashionAcquireWnd)
        end), nil, nil, nil, false)
    else
        g_MessageMgr:ShowMessage("Taben_Equip_Material_Not_Exist")
    end
end
