local CChatLinkMgr = import "CChatLinkMgr"
local QnSelectableButton = import "L10.UI.QnSelectableButton"

LuaGuildTerritorialWarsZhanShenTaskWnd = class()

RegistClassMember(LuaGuildTerritorialWarsZhanShenTaskWnd, "m_ItemTemplate")
RegistClassMember(LuaGuildTerritorialWarsZhanShenTaskWnd, "m_Grid")
RegistClassMember(LuaGuildTerritorialWarsZhanShenTaskWnd, "m_AcceptButton")
RegistClassMember(LuaGuildTerritorialWarsZhanShenTaskWnd, "m_TaskId")
RegistClassMember(LuaGuildTerritorialWarsZhanShenTaskWnd, "m_Items")

function LuaGuildTerritorialWarsZhanShenTaskWnd:Awake()
    self.m_ItemTemplate = self.transform:Find("Bg/ItemTemplate").gameObject
    self.m_ItemTemplate:SetActive(false)
    self.m_Grid = self.transform:Find("Bg/Grid"):GetComponent(typeof(UIGrid))
    self.m_AcceptButton = self.transform:Find("AcceptButton").gameObject

    self.m_TaskId = 0
    self.m_Items = {}

    CommonDefs.AddOnClickListener(self.m_AcceptButton, DelegateFactory.Action_GameObject(function(go)
        if self.m_TaskId == 0 then
            g_MessageMgr:ShowCustomMsg(LocalString.GetString("请先选择一个棋局战神挑战任务"))
        else
            Gac2Gas.RequestAcceptGTWZhanShenTask(self.m_TaskId)
            CUIManager.CloseUI(CLuaUIResources.GuildTerritorialWarsZhanShenTaskWnd)
        end
    end), false)
end

function LuaGuildTerritorialWarsZhanShenTaskWnd:Init()
    
end

function LuaGuildTerritorialWarsZhanShenTaskWnd:OnEnable()
    self:OnSendGTWZhanShenTaskChooseData(nil, LuaGuildTerritorialWarsMgr.m_ZhanShenTaskIds)
    g_ScriptEvent:AddListener("SendGTWZhanShenTaskChooseData", self, "OnSendGTWZhanShenTaskChooseData")
    g_ScriptEvent:AddListener("SyncGTWZhanShenTaskInfo", self, "OnSyncGTWZhanShenTaskInfo")

end
function LuaGuildTerritorialWarsZhanShenTaskWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendGTWZhanShenTaskChooseData", self, "OnSendGTWZhanShenTaskChooseData")
    g_ScriptEvent:RemoveListener("SyncGTWZhanShenTaskInfo", self, "OnSyncGTWZhanShenTaskInfo")
end

function LuaGuildTerritorialWarsZhanShenTaskWnd:OnSyncGTWZhanShenTaskInfo(acceptedTaskId, progress, isFinish)
    if acceptedTaskId > 0 then
        g_MessageMgr:ShowCustomMsg(LocalString.GetString("棋局战神挑战任务领取成功，可在任务栏进行查看"))
        CUIManager.CloseUI(CLuaUIResources.GuildTerritorialWarsZhanShenTaskWnd)
    end
end

function LuaGuildTerritorialWarsZhanShenTaskWnd:OnSendGTWZhanShenTaskChooseData(acceptedTaskId, taskIds)
    Extensions.RemoveAllChildren(self.m_Grid.transform)
    self.m_TaskId = 0
    self.m_Items = {}
    for i = 1, #taskIds do
        local go = NGUITools.AddChild(self.m_Grid.gameObject, self.m_ItemTemplate)
        go:SetActive(true)
        self:InitItem(go.transform, taskIds[i])
        local cmp = go:GetComponent(typeof(QnSelectableButton))
        table.insert(self.m_Items, cmp)

        cmp.OnClick = DelegateFactory.Action_QnButton(function(qnButton)
            self.m_TaskId = self.m_TaskId == taskIds[i] and 0 or taskIds[i]
            for i, v in ipairs(self.m_Items) do
                if v ~= qnButton then
                    v:SetSelected(false, false)
                end
            end
        end)
    end
    self.m_Grid:Reposition()
end

function LuaGuildTerritorialWarsZhanShenTaskWnd:InitItem(tf, taskId)
    local data = GuildTerritoryWar_ZhanShenTask.GetData(taskId)
    local taskLv = data.TaskLevel
    local LabelType = typeof(UILabel)
    local nameLabel = FindChild(tf, "NameLabel"):GetComponent(LabelType)
    local descLabel = FindChild(tf, "DescLabel"):GetComponent(LabelType)
    local typeLabel = FindChild(tf, "TypeLabel"):GetComponent(LabelType)
    local scoreLabel = FindChild(tf, "ScoreLabel"):GetComponent(LabelType)

    nameLabel.text = data.TaskTitle
    descLabel.text = CChatLinkMgr.TranslateToNGUIText(SafeStringFormat3(data.Desc, "#G"..CLuaGuanNingWuShenMgr.FormatNum(data.Target)), false)
    scoreLabel.text = data.ScoreReward

    local c = Color(0.275,0.44,0.86)
    if taskLv == 1 then
        typeLabel.text = LocalString.GetString("简单")
    elseif taskLv == 2 then
        typeLabel.text = LocalString.GetString("普通")
        c = Color(0.94,0.3,0.32)
    elseif taskLv == 3 then
        typeLabel.text = LocalString.GetString("困难")
        c = Color(0.95,0.35,0.95)
    end
    typeLabel.color = c
    nameLabel.color = c
end
