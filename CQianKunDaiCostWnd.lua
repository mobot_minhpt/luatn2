-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CItemMgr = import "L10.Game.CItemMgr"
local Constants = import "L10.Game.Constants"
local CQianKunDaiCostWnd = import "L10.UI.CQianKunDaiCostWnd"
local CQianKunDaiMgr = import "L10.Game.CQianKunDaiMgr"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local DelegateFactory = import "DelegateFactory"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local LuaUtils = import "LuaUtils"
local MessageWndManager = import "L10.UI.MessageWndManager"
local Object = import "System.Object"
local QianKunDai_Formula = import "L10.Game.QianKunDai_Formula"
local QianKunDai_Item = import "L10.Game.QianKunDai_Item"
CQianKunDaiCostWnd.m_Init_CS2LuaHook = function (this) 

    -- 根据formulaID获取所需活力和银两
    -- 积分合成的花费/描述需要额外获取，活力依然从formula中获取
    local formula = QianKunDai_Formula.GetData(CQianKunDaiMgr.Inst.SelectedFormulaID)
    if formula ~= nil then
        local message = this:GetDesc(formula)
        this.m_Message.text = message

        local huoli = formula.HuoLi
        this.m_HuoliCtrl:SetCost(huoli)

        local cost = this:GetCost(formula)
        this.m_Cost = cost
        this.m_MoneyCtrl:SetCost(cost)
        if not CClientMainPlayer.Inst then return end
        local freeSilver = CClientMainPlayer.Inst.FreeSilver
        local silver = CClientMainPlayer.Inst.Silver
        local moneyEnough = (freeSilver + silver >= 0) and ((freeSilver + silver) >= cost)

        if moneyEnough then
            local freeSilverNotEnough = (freeSilver > 0 and freeSilver < cost)
            this.m_YinPiaoNotEnough:SetActive(freeSilverNotEnough)

            -- 优先消耗银票
            if CClientMainPlayer.Inst.FreeSilver > 0 then
                this.m_MoneyCtrl:SetType(EnumMoneyType.YinPiao, EnumPlayScoreKey.NONE, true)
            else
                this.m_MoneyCtrl:SetType(EnumMoneyType.YinLiang, EnumPlayScoreKey.NONE, true)
            end
        else
            this.m_MoneyCtrl:SetType(EnumMoneyType.YinLiang, EnumPlayScoreKey.NONE, true)
            this.m_YinPiaoNotEnough:SetActive(false)
        end
    else
        this:Close()
    end
end
CQianKunDaiCostWnd.m_GetDesc_CS2LuaHook = function (this, formula) 
    local message = ""
    if CQianKunDaiMgr.Inst.SelectedFormulaID == CQianKunDaiMgr.Inst.JiFenFormulaId then
        message = g_MessageMgr:FormatMessage("QIANKUNDAI_HECHENG_LL_CONFIRM", CQianKunDaiMgr.Inst:GetQianKunDaiJiFenCanCompound())
    else
        message = g_MessageMgr:FormatMessage("QIANKUNDAI_HECHENG_CONFIRM", formula.ProductDesc)
    end
    return message
end
CQianKunDaiCostWnd.m_GetCost_CS2LuaHook = function (this, formula) 
    if formula.ID == CQianKunDaiMgr.Inst.JiFenFormulaId then
        local cost = 0
        do
            local i = 0
            while i < CQianKunDaiMgr.Inst.SelectedItems.Count do
                local item = CItemMgr.Inst:GetById(CQianKunDaiMgr.Inst.SelectedItems[i])
                if item ~= nil and QianKunDai_Item.Exists(item.TemplateId) then
                    local jifenItem = QianKunDai_Item.GetData(item.TemplateId)
                    local count = math.min(item.Amount, jifenItem.UpperLimit)
                    cost = cost + count * jifenItem.Silver
                end
                i = i + 1
            end
        end
        return cost
    else
        local silverFormulaId = math.floor(tonumber(formula.SilverFormulaId or 0))
        local para = CQianKunDaiMgr.Inst:GetSilverPara(CQianKunDaiMgr.Inst.SelectedFormulaID)
        local formulaFuc = LuaUtils.GetLuaFormula(silverFormulaId)

        if formulaFuc ~= nil then
            local table = {}
            table[1] = para
            local ret = Table2Array({formulaFuc(nil, nil, table)}, MakeArrayClass(Object))
            local result = ret[0]
            local cost = math.floor(result)
            return cost
        end
    end
    return CQianKunDaiCostWnd.DEFAULT_COST
end
CQianKunDaiCostWnd.m_OnOKButtonClick_CS2LuaHook = function (this, go) 

    if not this.m_HuoliCtrl.moneyEnough then
        g_MessageMgr:ShowMessage("USE_LIFE_SKILL_NEED_HUOLI")
        return
    end
    if not CClientMainPlayer.Inst then return end
    local freeSilver = CClientMainPlayer.Inst.FreeSilver
    local silver = CClientMainPlayer.Inst.Silver
    local moneyEnough = (freeSilver + silver >= 0) and ((freeSilver + silver) >= this.m_Cost)

    if not moneyEnough then
        local txt = g_MessageMgr:FormatMessage("NotEnough_Silver_QuickBuy")
        MessageWndManager.ShowOKCancelMessage(txt, DelegateFactory.Action(function () 
            CShopMallMgr.ShowLinyuShoppingMall(Constants.SilverItemId)
        end), nil, nil, nil, false)
        return
    end

    if this.m_MoneyCtrl.moneyEnough and this.m_HuoliCtrl.moneyEnough then
        if CQianKunDaiMgr.Inst.OnOKButtonClick ~= nil then
            invoke(CQianKunDaiMgr.Inst.OnOKButtonClick)
        end
        this:Close()
    end
end
