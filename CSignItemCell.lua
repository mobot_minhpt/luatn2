-- Auto Generated!!
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CSigninMgr = import "L10.Game.CSigninMgr"
local CSignItemCell = import "L10.UI.CSignItemCell"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local Gac2Gas = import "L10.Game.Gac2Gas"
local QianDao_Item = import "L10.Game.QianDao_Item"
local Vector4 = import "UnityEngine.Vector4"
local Extensions = import "Extensions"
CSignItemCell.m_Init_CS2LuaHook = function (this, d, scrollView) 

    this.date = d
    this.fx.ScrollView = scrollView
    local ID = CSigninMgr.Inst.month * 100 + this.date
    this.item = QianDao_Item.GetData(ID)
    this.row25Label.gameObject:SetActive(true)
    this.row25Label.text = Extensions.ToChineseWithinFifty(this.date)
	if CommonDefs.IS_VN_CLIENT then
		this.row25Label.text = tostring(this.date)
	end
    if (d - 2) % 5 == 0 or d % 5 == 0 or d == 31 then
        if this.date > CSigninMgr.Inst.signedDays then
            this.preciousFx:LoadFx(CUIFxPaths.ItemRemindYellowFxPath)
        end
    end
    this.signedFlag:SetActive(this.date <= CSigninMgr.Inst.signedDays)
    this.amount.text = ""
    local template
    if not CClientMainPlayer.Inst.HasFeiSheng then
        template = CItemMgr.Inst:GetItemTemplate(this.item.ItemID)
        this.amount.text = this.item.Count > 1 and tostring(this.item.Count) or ""
    else
        template = CItemMgr.Inst:GetItemTemplate(this.item.FeiShengItemID)
        this.amount.text = this.item.FeiShengItemCount > 1 and tostring(this.item.FeiShengItemCount) or ""
    end
    if template == nil then
        return
    end
    this.tex:LoadMaterial(template.Icon)
end
CSignItemCell.m_OnItemClicked_CS2LuaHook = function (this, go) 

    if not CSigninMgr.Inst.hasSignedToday and this.date - CSigninMgr.Inst.signedDays == 1 then
        LuaWelfareMgr.CheckMainWelfare = true
        Gac2Gas.SignUpForQianDao()
    else
        if not CClientMainPlayer.Inst.HasFeiSheng then
            CSigninMgr.Inst.itemID = this.item.ItemID
            CSigninMgr.Inst.itemDate = this.date
            CItemInfoMgr.ShowSigninLinkItemTemplateInfo(this.item.ItemID, AlignType.Default, 0, 0, 0, 0)
        else
            CSigninMgr.Inst.itemID = this.item.FeiShengItemID
            CSigninMgr.Inst.itemDate = this.date
            CItemInfoMgr.ShowSigninLinkItemTemplateInfo(this.item.FeiShengItemID, AlignType.Default, 0, 0, 0, 0)
        end
    end
end
CSignItemCell.m_LoadFx_CS2LuaHook = function (this) 
    this.fx:LoadFx(CUIFxPaths.CommonBlueLineFxPath)
    local gap = 10
    this:DoAni(Vector4(- this.background.width * 0.5, this.background.height * 0.5, this.background.width, this.background.height), false)
    --this:DoAni(Vector4(- this.background.width * 0.5 + gap, this.background.height * 0.5 - gap, this.background.width - gap * 2, this.background.height - gap * 2), false)
end
