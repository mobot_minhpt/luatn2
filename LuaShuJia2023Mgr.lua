local CTeamMgr = import "L10.Game.CTeamMgr"
local CScene = import "L10.Game.CScene"

LuaShuJia2023Mgr = {}

LuaShuJia2023Mgr.m_PresentPlayerName = ""
LuaShuJia2023Mgr.m_PresentPlayerId = 0
LuaShuJia2023Mgr.m_PresentPlayerVipStatus = {}
LuaShuJia2023Mgr.m_XHTXProgress = 0
LuaShuJia2023Mgr.m_XHTXLastWorldShareTime = 0

function LuaShuJia2023Mgr:XianHaiTongXingPlayerQueryTeamInfo_Result(member1Id, member2Id, member3Id, member4Id, member5Id, vipInfo, progress, extraInfo)
    self.m_XHTXProgress = progress
    g_ScriptEvent:BroadcastInLua("XianHaiTongXingPlayerQueryTeamInfo_Result", member1Id, member2Id, member3Id, member4Id, member5Id, vipInfo, progress, extraInfo)
end

function LuaShuJia2023Mgr:XianHaiTongXingPlayerQueryTeamProgress_Result(progress)
    self.m_XHTXProgress = progress
    g_ScriptEvent:BroadcastInLua("XianHaiTongXingPlayerQueryTeamProgress_Result", progress)
end

LuaShuJia2023Mgr.m_LastPreviewType = nil

LuaShuJia2023Mgr.m_StageTwoStatus = 0
LuaShuJia2023Mgr.m_StageTwoStartTime = 0

function LuaShuJia2023Mgr:ShuJia2023WorldEventsSyncStageTwoStatus(stageTwoStatus, stageTwoStartTime)
    LuaShuJia2023Mgr.m_StageTwoStatus = stageTwoStatus
    LuaShuJia2023Mgr.m_StageTwoStartTime = stageTwoStartTime
    g_ScriptEvent:BroadcastInLua("ShuJia2023WorldEventsSyncStageTwoStatus", stageTwoStatus, stageTwoStartTime)
end

function LuaShuJia2023Mgr:IsNeedShowWorldEvent()
    return self.m_StageTwoStatus == 1 or self.m_StageTwoStatus == 2
end

function LuaShuJia2023Mgr:OpenWorldEventWnd()
    LuaCopySceneChosenMgr.OpenWnd = "ShuJia2023WorldEventWnd"
    Gac2Gas.RequestGetTeleportPublicMapInfo(tonumber(ShuJia2023_Setting.GetData("BossSceneTemplateId").Value))
end

function LuaShuJia2023Mgr:IsInYXW()
    return CScene.MainScene and CScene.MainScene.SceneTemplateId == tonumber(ShuJia2023_Setting.GetData("BossSceneTemplateId").Value)
end
