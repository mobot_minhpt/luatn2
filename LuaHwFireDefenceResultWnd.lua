local QnButton = import "L10.UI.QnButton"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"
local Profession = import "L10.Game.Profession"
local EnumClass = import "L10.Game.EnumClass"
local CTeamMgr = import "L10.Game.CTeamMgr"
local Animator = import "UnityEngine.Animator"
LuaHwFireDefenceResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaHwFireDefenceResultWnd, "ModeLb", "ModeLb", UILabel)
RegistChildComponent(LuaHwFireDefenceResultWnd, "RoleItem1", "RoleItem1", GameObject)
RegistChildComponent(LuaHwFireDefenceResultWnd, "RoleItem2", "RoleItem2", GameObject)
RegistChildComponent(LuaHwFireDefenceResultWnd, "RoleItem3", "RoleItem3", GameObject)
RegistChildComponent(LuaHwFireDefenceResultWnd, "RoleItem4", "RoleItem4", GameObject)
RegistChildComponent(LuaHwFireDefenceResultWnd, "RoleItem5", "RoleItem5", GameObject)
RegistChildComponent(LuaHwFireDefenceResultWnd, "RwTipLb2", "RwTipLb2", UILabel)
RegistChildComponent(LuaHwFireDefenceResultWnd, "ItemCell1", "ItemCell1", GameObject)
RegistChildComponent(LuaHwFireDefenceResultWnd, "ItemCell2", "ItemCell2", GameObject)
--RegistChildComponent(LuaHwFireDefenceResultWnd, "WinTex", "WinTex", GameObject)
--RegistChildComponent(LuaHwFireDefenceResultWnd, "LossTex", "LossTex", GameObject)
RegistChildComponent(LuaHwFireDefenceResultWnd, "FightBtn", "FightBtn", QnButton)
RegistChildComponent(LuaHwFireDefenceResultWnd, "ShareButton", "ShareButton", QnButton)
RegistChildComponent(LuaHwFireDefenceResultWnd, "RwTipLb1", "RwTipLb1", UILabel)
RegistChildComponent(LuaHwFireDefenceResultWnd, "CloseButton", "CloseButton", GameObject)
RegistChildComponent(LuaHwFireDefenceResultWnd, "NormalTex", "NormalTex", GameObject)
RegistChildComponent(LuaHwFireDefenceResultWnd, "HeroTex", "HeroTex", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaHwFireDefenceResultWnd, "m_ShareTick")
function LuaHwFireDefenceResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!


	UIEventListener.Get(self.FightBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnFightBtnClick()
	end)

    --@endregion EventBind end

	local onShareClick = function(go)
		self:SetOpBtnVisible(false)
		if self.m_ShareTick then
		  UnRegisterTick(self.m_ShareTick)
		  self.m_ShareTick = nil
		end
		self.m_ShareTick = RegisterTickWithDuration(function ()
		  self:SetOpBtnVisible(true)
		end, 1000, 1000)
		CUICommonDef.CaptureScreenAndShare()
	  end
	CommonDefs.AddOnClickListener(self.ShareButton.gameObject,DelegateFactory.Action_GameObject(onShareClick),false)
	if CommonDefs.IS_VN_CLIENT then
		self.ShareButton:SetActive(false)
	end
	self.RwTipLb1.gameObject:SetActive(false)
end

function LuaHwFireDefenceResultWnd:Init()
	if LuaHalloween2022Mgr.FireDefencePlayResult == nil then return end
	local anim = self.gameObject:GetComponent(typeof(Animator))
	if LuaHalloween2022Mgr.FireDefencePlayResult.isWin then
		anim:Play("hwfiredefenceresultwnd_win")
	else 
		anim:Play("hwfiredefenceresultwnd_loss")
	end
	--self.WinTex:SetActive(false)
	--self.LossTex:SetActive(false)
	local mode = LuaHalloween2022Mgr.FireDefencePlayResult.mode
	self.NormalTex:SetActive(not mode)
	self.HeroTex:SetActive(mode)
	self:InitMembers()
	self:InitReward()
end

function LuaHwFireDefenceResultWnd:SetOpBtnVisible(sign)
	self.CloseButton.gameObject:SetActive(sign)
	self.ShareButton.gameObject:SetActive(sign)
end

function LuaHwFireDefenceResultWnd:InitMembers()
	local memberList = CTeamMgr.Inst.Members
	local RoleItemList = {self.RoleItem1,self.RoleItem2,self.RoleItem3,self.RoleItem4,self.RoleItem5}
	--self.transform:Find("Info/HeroTex").gameObject:SetActive(LuaHalloween2022Mgr.FireDefencePlayResult.mode)
	--self.transform:Find("Info/NormalTex").gameObject:SetActive(not LuaHalloween2022Mgr.FireDefencePlayResult.mode )
	-- if LuaHalloween2022Mgr.FireDefencePlayResult.mode == 0 then
	-- 	self.transform:Find("ModeLb"):GetComponent(typeof(UILabel)).text = LocalString.GetString("普通模式")
	-- elseif LuaHalloween2022Mgr.FireDefencePlayResult.mode == 1 then
	-- 	self.transform:Find("ModeLb"):GetComponent(typeof(UILabel)).text = LocalString.GetString("英雄模式")
	-- end
	for i=1,#RoleItemList do
		RoleItemList[i]:SetActive(false)
	end
	for i=0,memberList.Count-1 do
		self:InitOneMember(RoleItemList[i+1],memberList[i])
	end
end

function LuaHwFireDefenceResultWnd:InitReward()
	local awardList = LuaHalloween2022Mgr.FireDefencePlayResult.rewards
	self.ItemCell1:SetActive(false)
	self.ItemCell2:SetActive(false)
	if #awardList == 0 then
		self.RwTipLb1.gameObject:SetActive(false)
		self.RwTipLb2.gameObject:SetActive(true)
		if LuaHalloween2022Mgr.FireDefencePlayResult.isWin then
			if not LuaHalloween2022Mgr.FireDefencePlayResult.mode then
				self.RwTipLb2.text = g_MessageMgr:FormatMessage("ZNQ_CAKECUTTING_REWARD_FULL")
			else
				self.RwTipLb2.text = g_MessageMgr:FormatMessage("Campfire_HARD_WIN_TEXT")
			end
		else
			self.RwTipLb2.text = LocalString.GetString("篝火熄灭，奖励被怪物夺走了")
		end
	else 
		--self.RwTipLb1.gameObject:SetActive(true)
		self.RwTipLb2.gameObject:SetActive(false)
		if awardList[1] then 
			self.ItemCell1:SetActive(true)
			self:InitOneItem(self.ItemCell1,awardList[1])
		end
		if awardList[2] then
			self.ItemCell2:SetActive(true)
			self:InitOneItem(self.ItemCell2,awardList[2])
		end
	end
end


function LuaHwFireDefenceResultWnd:InitOneItem(curItem, itemID)
    local texture = curItem.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    local ItemData = Item_Item.GetData(itemID)
	curItem:SetActive(true)
    if ItemData then texture:LoadMaterial(ItemData.Icon) end

    CommonDefs.AddOnClickListener(
        curItem,
        DelegateFactory.Action_GameObject(
            function(go)
                CItemInfoMgr.ShowLinkItemTemplateInfo(itemID, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
            end
        ),
        false
    )
end

function LuaHwFireDefenceResultWnd:InitOneMember(curItem,member)
	if curItem == nil then return end
	curItem:SetActive(true)
	local jobtex = curItem.transform:Find("JobSp"):GetComponent(typeof(UISprite))
	local nameLb = curItem.transform:Find("NameLb"):GetComponent(typeof(UILabel))
	local LvLb = curItem.transform:Find("LvLb"):GetComponent(typeof(UILabel))

	jobtex.spriteName = Profession.GetIcon(member.m_MemberClass)
	nameLb.text = member.m_MemberName
	LvLb.text = "Lv."..member.m_MemberLevel
end
--@region UIEvent

function LuaHwFireDefenceResultWnd:OnFightBtnClick()
	LuaHalloween2022Mgr.m_OpenFireDeferenceWndFromMain = false
	CUIManager.CloseUI(CLuaUIResources.HwFireDefenceResultWnd)
	CUIManager.ShowUI(CLuaUIResources.HwFireDefenceEnterWnd)
end

--@endregion UIEvent

function LuaHwFireDefenceResultWnd:OnDisable()
	if self.m_ShareTick~=nil then UnRegisterTick(self.m_ShareTick) end
end

