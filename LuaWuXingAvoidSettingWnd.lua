local CButton = import "L10.UI.CButton"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaWuXingAvoidSettingWnd = class()

RegistChildComponent(LuaWuXingAvoidSettingWnd, "CurFaZhengWuXingTex", "CurFaZhengWuXingTex", CUITexture)
RegistChildComponent(LuaWuXingAvoidSettingWnd, "AvoidFaZhengWuXingTex", "AvoidFaZhengWuXingTex", CUITexture)
RegistChildComponent(LuaWuXingAvoidSettingWnd, "QnRadioBox", "QnRadioBox", QnRadioBox)
RegistChildComponent(LuaWuXingAvoidSettingWnd, "SettingButton", "SettingButton", CButton)
RegistChildComponent(LuaWuXingAvoidSettingWnd, "CostShengWangLabel", "CostShengWangLabel", UILabel)
RegistChildComponent(LuaWuXingAvoidSettingWnd, "OwnShengWangLabel", "OwnShengWangLabel", UILabel)
RegistChildComponent(LuaWuXingAvoidSettingWnd, "CoolingTimeLabel", "CoolingTimeLabel", UILabel)
RegistChildComponent(LuaWuXingAvoidSettingWnd, "BottomView", "BottomView", GameObject)
RegistChildComponent(LuaWuXingAvoidSettingWnd, "MingGe5", "MingGe5", GameObject)
RegistChildComponent(LuaWuXingAvoidSettingWnd, "TipButton", "TipButton", GameObject)
RegistChildComponent(LuaWuXingAvoidSettingWnd, "NiuZhuanWuXingTag", "NiuZhuanWuXingTag", GameObject)
RegistChildComponent(LuaWuXingAvoidSettingWnd, "NiuZhuanWuXingDesLabel", "NiuZhuanWuXingDesLabel", UILabel)
RegistClassMember(LuaWuXingAvoidSettingWnd,"m_SelectIndex")

function LuaWuXingAvoidSettingWnd:Awake()
    self.CoolingTimeLabel.gameObject:SetActive(false)
    self.QnRadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn, index)
        self:OnQnRadioBoxClick(btn, index)
    end)
    self.QnRadioBox:ChangeTo(0, true)
    UIEventListener.Get(self.SettingButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnSettingButtonClick()
    end)
    UIEventListener.Get(self.TipButton).onClick = DelegateFactory.VoidDelegate(function (p)
        g_MessageMgr:ShowMessage("AvoidWuXingExplanation")
    end)
    CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.WuXingAvoidSetting)
    if CClientMainPlayer.Inst then
        Gac2Gas.QuerySectFaZhenAvoidWuXingInfo(CClientMainPlayer.Inst.BasicProp.SectId)
    end
end

function LuaWuXingAvoidSettingWnd:OnEnable()
    g_ScriptEvent:AddListener("OnReplySectFaZhenAvoidWuXingInfo", self, "OnReplySectFaZhenAvoidWuXingInfo")
    g_ScriptEvent:AddListener("OnReplySetSectFaZhenAvoidWuXing", self, "OnReplySetSectFaZhenAvoidWuXing")
    g_ScriptEvent:AddListener("OnNiuZhuanWuXingResult", self, "OnNiuZhuanWuXingResult")
end

function LuaWuXingAvoidSettingWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnReplySectFaZhenAvoidWuXingInfo", self, "OnReplySectFaZhenAvoidWuXingInfo")
    g_ScriptEvent:RemoveListener("OnReplySetSectFaZhenAvoidWuXing", self, "OnReplySetSectFaZhenAvoidWuXing")
    g_ScriptEvent:RemoveListener("OnNiuZhuanWuXingResult", self, "OnNiuZhuanWuXingResult")
end

function LuaWuXingAvoidSettingWnd:OnNiuZhuanWuXingResult(lastWuXing, wuXing, endCDTime)
    self.NiuZhuanWuXingTag:SetActive(LuaZongMenMgr.m_NiuZhuanWuXingResult ~= 0 and endCDTime > CServerTimeMgr.Inst.timeStamp)
    local lastWuXing = SoulCore_WuXing.GetData(lastWuXing)
    local wuXing = SoulCore_WuXing.GetData(wuXing)
    local lastWuXingName = lastWuXing and lastWuXing.Name or ""
    local wuXingName = wuXing and wuXing.Name or ""
    local t = CServerTimeMgr.ConvertTimeStampToZone8Time(endCDTime)
    local now = CServerTimeMgr.Inst:GetZone8Time()
    local delta = t:Subtract(now)
    self.NiuZhuanWuXingDesLabel.text = g_MessageMgr:FormatMessage("WuXingNiuZhuan_CD_Text", lastWuXingName,  wuXingName) ..
        SafeStringFormat3(" %02d:%02d:%02d",delta.Hours, delta.Minutes, delta.Seconds)
    if LuaZongMenMgr.m_NiuZhuanWuXingResult ~= 0 then
        self.CurFaZhengWuXingTex:LoadMaterial(LuaZongMenMgr:GetMingGeMatPath(LuaZongMenMgr.m_NiuZhuanWuXingResult))
    end
end

function LuaWuXingAvoidSettingWnd:OnReplySectFaZhenAvoidWuXingInfo(sectId, currentWuXing, avoidWuXing, nextCanSetTime, bFree, hasShengWang)
    self:Reload(sectId, currentWuXing, avoidWuXing, nextCanSetTime, bFree, hasShengWang)
end

function LuaWuXingAvoidSettingWnd:OnReplySetSectFaZhenAvoidWuXing(sectId, currentWuXing, avoidWuXing, nextCanSetTime, bFree, hasShengWang)
    self:Reload(sectId, currentWuXing, avoidWuXing, nextCanSetTime, bFree, hasShengWang)
end

function LuaWuXingAvoidSettingWnd:Reload(sectId, currentWuXing, avoidWuXing, nextCanSetTime, bFree, hasShengWang)
    self.CurFaZhengWuXingTex:LoadMaterial(LuaZongMenMgr:GetMingGeMatPath(LuaZongMenMgr.m_NiuZhuanWuXingResult ~= 0 and LuaZongMenMgr.m_NiuZhuanWuXingResult or currentWuXing))
    self.AvoidFaZhengWuXingTex:LoadMaterial(LuaZongMenMgr:GetMingGeMatPath(avoidWuXing))
    self.OwnShengWangLabel.text = hasShengWang
    self.CostShengWangLabel.text = bFree and LocalString.GetString("免费") or  Menpai_Setting.GetData("WuXingAvoidConsume").Value
end


function LuaWuXingAvoidSettingWnd:Init()
    self:OnNiuZhuanWuXingResult(LuaZongMenMgr.m_WuXingBeforeNiuZhuan, LuaZongMenMgr.m_NiuZhuanWuXingResult, LuaZongMenMgr.m_NiuZhuanWuXingEndCDTime)
end

--@region UIEvent

function LuaWuXingAvoidSettingWnd:OnQnRadioBoxClick(btn, index)
    self.m_SelectIndex = index + 1
end

function LuaWuXingAvoidSettingWnd:OnSettingButtonClick()
    if CClientMainPlayer.Inst then
        Gac2Gas.RequestSetSectFaZhenAvoidWuXing(CClientMainPlayer.Inst.BasicProp.SectId,self.m_SelectIndex)
    end
end

--@endregion

function LuaWuXingAvoidSettingWnd:GetGuideGo(methodName)
    if methodName=="GetFirstGuideGo" then
        return self.CurFaZhengWuXingTex.gameObject
    elseif methodName=="GetSecondGuideGo" then
        return self.transform:Find("Anchor/Label2/AvoidFaZhengWuXingTex").gameObject
    elseif methodName=="GetThirdGuideGo" then
        return self.MingGe5
    elseif methodName=="GetLastGuideGo" then
        return self.BottomView
    end
end
