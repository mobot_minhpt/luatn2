local CSwitchMgr=import "L10.Engine.CSwitchMgr"
local CScene=import "L10.Game.CScene"
local CUITexture=import "L10.UI.CUITexture"
local CLingShouMgr=import "L10.Game.CLingShouMgr"
local CLingShouBaseMgr=import "L10.Game.CLingShouBaseMgr"
local QnButton = import "L10.UI.QnButton"
CLuaLingShouBattleConfigWnd=class()

RegistClassMember(CLuaLingShouBattleConfigWnd,"marryStateLabel")

function CLuaLingShouBattleConfigWnd:Init()
    self.prefab = self.transform:Find("Content/Grid/Item").gameObject
    --self.items = ?
    self.grid = self.transform:Find("Content/Grid"):GetComponent(typeof(UIGrid))

    NGUITools.AddChild(self.grid.gameObject, self.prefab)
    NGUITools.AddChild(self.grid.gameObject, self.prefab)
    self.grid:Reposition()

    self:InitItem(self.grid.transform:GetChild(0),1)
    self:InitItem(self.grid.transform:GetChild(1),2)
    self:InitItem(self.grid.transform:GetChild(2),3)
end

function CLuaLingShouBattleConfigWnd:InitItem(transform,index)
    local contentGo = transform:Find("Content").gameObject
    local descLabel = transform:Find("DescLabel"):GetComponent(typeof(UILabel))
    descLabel.text=nil

    local icon = transform:Find("Content/Icon"):GetComponent(typeof(CUITexture))
    local stateLabel = transform:Find("Content/StateLabel"):GetComponent(typeof(UILabel))
    local nameLabel = transform:Find("Content/NameLabel"):GetComponent(typeof(UILabel))
    local levelLabel = transform:Find("Content/Icon/LevelLabel"):GetComponent(typeof(UILabel))
    local btnLabel = transform:Find("Content/Btn/BtnLabel"):GetComponent(typeof(UILabel))
    local btn = transform:Find("Content/Btn").gameObject
    local btnTip = transform:Find("Content/BtnTipLabel").gameObject
    btnTip:SetActive(false)
    local NotOpenMark = transform:Find("Content/Icon/NotOpenMark").gameObject
    local tipGo = transform:Find("Content/TipLabel").gameObject
    tipGo:SetActive(false)
    local tipBtn = transform:Find("Content/TipBtn").gameObject

    local isLingShouFuTi =CSwitchMgr.EnableLingShouHuaLing and CScene.MainScene and CScene.MainScene.LingShouMode==1 or false


    local list=CLingShouMgr.Inst.lingShouOverviewList
    local lastLingShouId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.LastLingShouId or nil
    local assistLingShouId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.AssistLingShouId or nil
    local futiId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.FuTiLingShouId or nil
    local bindPartenerLingShouId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.BindedPartenerLingShouId or nil
    local battleData = nil
    local assistData = nil
    local futiData = nil
    local bindPartenerData = nil
    for i=1,list.Count do
        if lastLingShouId == list[i-1].id then
            battleData = list[i-1]
        elseif assistLingShouId == list[i-1].id then
            assistData = list[i-1]
        elseif futiId == list[i-1].id then
            futiData = list[i-1]
        elseif bindPartenerLingShouId == list[i-1].id then
            bindPartenerData = list[i-1]
        end
    end

    if index==1 then--出战或附体
        if isLingShouFuTi then
            --附体
            if futiData==nil then
                btnLabel.text = LocalString.GetString("化灵")
                NotOpenMark:SetActive(true)
                levelLabel.gameObject:SetActive(false)
                nameLabel.text = ""
                icon:Clear()
                stateLabel.text = LocalString.GetString("已化灵")
                -- UIEventListener.Get(self.btn).onClick = MakeDelegateFromCSFunction(self.SetBattle, VoidDelegate, self)

                -- self.mLingShouId = ""
                UIEventListener.Get(btn).onClick = DelegateFactory.VoidDelegate(function(go)
                    self:SetFuTi()
                end)
            else
                NotOpenMark:SetActive(false)

                nameLabel.text = futiData.name
                icon:LoadNPCPortrait(CLingShouBaseMgr.GetLingShouIcon(futiData.templateId), false)
                levelLabel.gameObject:SetActive(true)
                levelLabel.text = SafeStringFormat3(LocalString.GetString("%s级"), tostring(futiData.level))

                stateLabel.text = LocalString.GetString("已化灵")

                if futiData.id == CLingShouMgr.Inst.selectedLingShou then
                    btnLabel.text = LocalString.GetString("休息")
                    UIEventListener.Get(btn).onClick = DelegateFactory.VoidDelegate(function(go)
                        self:SetRest()
                    end)
                else
                    btnLabel.text = LocalString.GetString("替换当前")
                    UIEventListener.Get(btn).onClick = DelegateFactory.VoidDelegate(function(go)
                        self:SetFuTi()
                    end)
                    -- UIEventListener.Get(self.btn).onClick = MakeDelegateFromCSFunction(self.SetBattle, VoidDelegate, self)
                end
            end
            UIEventListener.Get(tipBtn).onClick = DelegateFactory.VoidDelegate(function(go)
              g_MessageMgr:ShowMessage("LINGSHOU_HUALING_TIP")
            end)
        else
            if battleData == nil then
                btnLabel.text = LocalString.GetString("出战")
                NotOpenMark:SetActive(true)
                levelLabel.gameObject:SetActive(false)
                nameLabel.text = ""
                icon:Clear()
                stateLabel.text = LocalString.GetString("出战中")
                -- UIEventListener.Get(self.btn).onClick = MakeDelegateFromCSFunction(self.SetBattle, VoidDelegate, self)

                -- self.mLingShouId = ""
                UIEventListener.Get(btn).onClick = DelegateFactory.VoidDelegate(function(go)
                    self:SetBattle()
                end)
            else
                -- self.mLingShouId = data.id

                NotOpenMark:SetActive(false)

                nameLabel.text = battleData.name
                icon:LoadNPCPortrait(CLingShouBaseMgr.GetLingShouIcon(battleData.templateId), false)
                levelLabel.gameObject:SetActive(true)
                levelLabel.text = SafeStringFormat3(LocalString.GetString("%s级"), tostring(battleData.level))
                stateLabel.text = LocalString.GetString("出战中")

                if battleData.id == CLingShouMgr.Inst.selectedLingShou then
                    btnLabel.text = LocalString.GetString("休息")
                    -- UIEventListener.Get(self.btn).onClick = MakeDelegateFromCSFunction(self.SetRest, VoidDelegate, self)
                    UIEventListener.Get(btn).onClick = DelegateFactory.VoidDelegate(function(go)
                        self:SetRest()
                    end)
                else
                    btnLabel.text = LocalString.GetString("替换当前")
                    UIEventListener.Get(btn).onClick = DelegateFactory.VoidDelegate(function(go)
                        self:SetBattle()
                    end)
                    -- UIEventListener.Get(self.btn).onClick = MakeDelegateFromCSFunction(self.SetBattle, VoidDelegate, self)
                end
            end
            tipBtn:SetActive(false)
        end

    elseif index==2 then--助战
        if isLingShouFuTi then
            tipGo:SetActive(true)
        else
            tipGo:SetActive(false)
        end
        if assistData == nil then
            btnLabel.text = LocalString.GetString("助战")
            NotOpenMark:SetActive(true)
            levelLabel.gameObject:SetActive(false)
            levelLabel.text = ""
            nameLabel.text = ""
            stateLabel.text = LocalString.GetString("助战中")
            icon:Clear()

            local level = CClientMainPlayer.Inst and CClientMainPlayer.Inst.MaxLevel or 0
            if level>=70  then
                btn:GetComponent(typeof(QnButton)).Enabled = true
                UIEventListener.Get(btn).onClick = DelegateFactory.VoidDelegate(function(go)
                    self:SetAssist()
                end)
            else
                btn:GetComponent(typeof(QnButton)).Enabled = false
                descLabel.text=LocalString.GetString("需角色达到70级")
            end


        else
            NotOpenMark:SetActive(false)

            nameLabel.text = assistData.name
            icon:LoadNPCPortrait(CLingShouBaseMgr.GetLingShouIcon(assistData.templateId), false)
            levelLabel.gameObject:SetActive(true)
            levelLabel.text = SafeStringFormat3(LocalString.GetString("%s级"), tostring(assistData.level))
            stateLabel.text = LocalString.GetString("助战中")

            if assistData.id == CLingShouMgr.Inst.selectedLingShou then
                btnLabel.text = LocalString.GetString("休息")
                UIEventListener.Get(btn).onClick = DelegateFactory.VoidDelegate(function(go)
                    self:UnsetAssist()
                end)
            else
                btnLabel.text = LocalString.GetString("替换当前")
                UIEventListener.Get(btn).onClick = DelegateFactory.VoidDelegate(function(go)
                    self:SetAssist()
                end)
                -- UIEventListener.Get(self.btn).onClick = MakeDelegateFromCSFunction(self.SetAssist, VoidDelegate, self)
            end
        end
        UIEventListener.Get(tipBtn).onClick = DelegateFactory.VoidDelegate(function(go)
          g_MessageMgr:ShowMessage("LINGSHOU_ZHUZHAN_TIP")
        end)
    elseif index==3 then
        tipGo:SetActive(false)
        if CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp.SealId < 3 then
            btnLabel.text = LocalString.GetString("结伴")
            NotOpenMark:SetActive(true)
            levelLabel.gameObject:SetActive(false)
            levelLabel.text = ""
            nameLabel.text = ""
            stateLabel.text = LocalString.GetString("结伴中")
            icon:Clear()

            btnTip:SetActive(true)
            btn:SetActive(false)
        else
            if bindPartenerData == nil then
                btnLabel.text = LocalString.GetString("结伴")
                NotOpenMark:SetActive(true)
                levelLabel.gameObject:SetActive(false)
                levelLabel.text = ""
                nameLabel.text = ""
                stateLabel.text = LocalString.GetString("结伴中")
                icon:Clear()
                -- UIEventLstener.Get(self.btn).onClick = MakeDelegateFromCSFunction(self.SetAssist, VoidDelegate, self)
                if CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.OwnBabyId ~= "" then
                    btn:GetComponent(typeof(QnButton)).Enabled = true
                    UIEventListener.Get(btn).onClick = DelegateFactory.VoidDelegate(function(go)
                        self:SetBind()
                    end)
                else
                    descLabel.text=LocalString.GetString("需拥有专属孩子")
                    btn:GetComponent(typeof(QnButton)).Enabled = false
                end
            else
                NotOpenMark:SetActive(false)

                nameLabel.text = bindPartenerData.name
                icon:LoadNPCPortrait(CLingShouBaseMgr.GetLingShouIcon(bindPartenerData.templateId), false)
                levelLabel.gameObject:SetActive(true)
                levelLabel.text = SafeStringFormat3(LocalString.GetString("%s级"), tostring(bindPartenerData.level))
                stateLabel.text = LocalString.GetString("结伴中")

                if bindPartenerData.id == CLingShouMgr.Inst.selectedLingShou then
                    btnLabel.text = LocalString.GetString("休息")
                    UIEventListener.Get(btn).onClick = DelegateFactory.VoidDelegate(function(go)
                        self:UnsetBind()
                    end)
                else
                    btnLabel.text = LocalString.GetString("替换当前")
                    UIEventListener.Get(btn).onClick = DelegateFactory.VoidDelegate(function(go)
                        self:SetBind()
                    end)
                end
            end
        end
        UIEventListener.Get(tipBtn).onClick = DelegateFactory.VoidDelegate(function(go)
            g_MessageMgr:ShowMessage("LINGSHOU_JIEBAN_TIP")
        end)
    end
end

function CLuaLingShouBattleConfigWnd:SetRest()
    Gac2Gas.RequestPackLingShou()
    --之前那只出战的灵兽需要重新请求数据了
    if CClientMainPlayer.Inst.CurrentLingShou ~= nil then
        local detail = CLingShouMgr.Inst:GetLingShouDetails(CClientMainPlayer.Inst.CurrentLingShou.ID)
        detail.dirty = true
    end
    Gac2Gas.RequestLingShouDetails(CLingShouMgr.Inst.selectedLingShou, 0)
    CUIManager.CloseUI(CUIResources.LingShouBattleConfigWnd)
end
function CLuaLingShouBattleConfigWnd:SetAssist()
    Gac2Gas.RequestSetAssistLingShou(CLingShouMgr.Inst.selectedLingShou)
    CUIManager.CloseUI(CUIResources.LingShouBattleConfigWnd)
end
function CLuaLingShouBattleConfigWnd:UnsetAssist()
    CUIManager.CloseUI(CUIResources.LingShouBattleConfigWnd)
end
function CLuaLingShouBattleConfigWnd:SetBind()
  Gac2Gas.RequestBindPartner_LingShou(CLingShouMgr.Inst.selectedLingShou)
  CUIManager.CloseUI(CUIResources.LingShouBattleConfigWnd)
end
function CLuaLingShouBattleConfigWnd:UnsetBind()
  Gac2Gas.RequestCancelBindPartner_LingShou(CLingShouMgr.Inst.selectedLingShou)
  CUIManager.CloseUI(CUIResources.LingShouBattleConfigWnd)
end
function CLuaLingShouBattleConfigWnd:SetBattle()
    --之前那只出战的灵兽需要重新请求数据了
    if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.CurrentLingShou ~= nil then
        local detail = CLingShouMgr.Inst:GetLingShouDetails(CClientMainPlayer.Inst.CurrentLingShou.ID)
        if detail ~= nil then
            detail.dirty = true
        end
    end
    Gac2Gas.RequestSummonLingShou(CLingShouMgr.Inst.selectedLingShou)
    CUIManager.CloseUI(CUIResources.LingShouBattleConfigWnd)
end

function CLuaLingShouBattleConfigWnd:SetFuTi()
    --之前那只出战的灵兽需要重新请求数据了
    if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.CurrentLingShou ~= nil then
        local detail = CLingShouMgr.Inst:GetLingShouDetails(CClientMainPlayer.Inst.CurrentLingShou.ID)
        if detail ~= nil then
            detail.dirty = true
        end
    end

    Gac2Gas.RequestSwitchFuTiLingShou(CLingShouMgr.Inst.selectedLingShou)
    CUIManager.CloseUI(CUIResources.LingShouBattleConfigWnd)
end

function CLuaLingShouBattleConfigWnd:OnEnable()
    g_ScriptEvent:AddListener("UpdateAssistLingShouId", self, "OnUpdateAssistLingShouId")
    g_ScriptEvent:AddListener("SyncLastLingShouId", self, "OnSyncLastLingShouId")
    g_ScriptEvent:AddListener("UpdateFuTiLingShouId", self, "OnUpdateFuTiLingShouId")
end
function CLuaLingShouBattleConfigWnd:OnDisable()
    g_ScriptEvent:RemoveListener("UpdateAssistLingShouId", self, "OnUpdateAssistLingShouId")
    g_ScriptEvent:RemoveListener("SyncLastLingShouId", self, "OnSyncLastLingShouId")
    g_ScriptEvent:RemoveListener("UpdateFuTiLingShouId", self, "OnUpdateFuTiLingShouId")
end

function CLuaLingShouBattleConfigWnd:OnUpdateAssistLingShouId(args)
    -- local lingShouId = tostring(args[0])
    self:InitItem(self.grid.transform:GetChild(1),2)
end
function CLuaLingShouBattleConfigWnd:OnSyncLastLingShouId(args)
    -- local lingShouId = tostring(args[0])
    self:InitItem(self.grid.transform:GetChild(0),1)
end
function CLuaLingShouBattleConfigWnd:OnUpdateFuTiLingShouId(args)
    self:InitItem(self.grid.transform:GetChild(0),1)
end
