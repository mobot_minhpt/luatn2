local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UITexture = import "UITexture"
local UILabel = import "UILabel"
local CButton = import "L10.UI.CButton"
local GestureRecognizer = import "L10.Engine.GestureRecognizer"
local GestureType = import "L10.Engine.GestureType"
local NativeHandle = import "NativeHandle"
local LoadPicMgr = import "L10.Game.LoadPicMgr"
local Texture2D = import "UnityEngine.Texture2D"
local TextureFormat = import "UnityEngine.TextureFormat"
local Screen = import "UnityEngine.Screen"
local EasyTouch = import "EasyTouch"
local Vector2 = import "UnityEngine.Vector2"
local Rect = import "UnityEngine.Rect"
local Camera = import "UnityEngine.Camera"
local LayerDefine = import "L10.Engine.LayerDefine"
local CFuxiFaceDNAMgr = import "L10.Game.CFuxiFaceDNAMgr"
local MessageMgr = import "L10.Game.MessageMgr"
local Animation = import "UnityEngine.Animation"
local VoiceManager = import "VoiceManager"
local Main = import "L10.Engine.Main"
local CFacialMgr = import "L10.Game.CFacialMgr"
local CPinchFaceCinemachineCtrlMgr = import "L10.Game.CPinchFaceCinemachineCtrlMgr"
local NormalCamera = import "L10.Engine.CameraControl.NormalCamera"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local ENUM_AVATAR_RESULT = import "ENUM_AVATAR_RESULT"
local Overflow = import "UILabel+Overflow"

EnumFuxiPinchFaceState = {
    AddPic = 1,
    Cliping = 2,
    UploadAndReviewProcessing = 3,
    UploadAndReviewResult = 4,
    FuxiProcessing = 5,
    FuxiResult = 6,
}

LuaPinchFaceFuxiWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaPinchFaceFuxiWnd, "MainView", "MainView", GameObject)
RegistChildComponent(LuaPinchFaceFuxiWnd, "ClipView", "ClipView", GameObject)
RegistChildComponent(LuaPinchFaceFuxiWnd, "MainPreviewTexture", "MainPreviewTexture", UITexture)
RegistChildComponent(LuaPinchFaceFuxiWnd, "UploadedPic", "UploadedPic", UITexture)
RegistChildComponent(LuaPinchFaceFuxiWnd, "UploadTip", "UploadTip", GameObject)
RegistChildComponent(LuaPinchFaceFuxiWnd, "UploadFailTipLab", "UploadFailTipLab", UILabel)
RegistChildComponent(LuaPinchFaceFuxiWnd, "FuxiFailTipLab", "FuxiFailTipLab", UILabel)
RegistChildComponent(LuaPinchFaceFuxiWnd, "UploadFx", "UploadFx", GameObject)
RegistChildComponent(LuaPinchFaceFuxiWnd, "AddPicBtns", "AddPicBtns", GameObject)
RegistChildComponent(LuaPinchFaceFuxiWnd, "ProcessingBtns", "ProcessingBtns", GameObject)
RegistChildComponent(LuaPinchFaceFuxiWnd, "ResultBtns", "ResultBtns", GameObject)
RegistChildComponent(LuaPinchFaceFuxiWnd, "AddPicBtn", "AddPicBtn", CButton)
RegistChildComponent(LuaPinchFaceFuxiWnd, "CaptureBtn", "CaptureBtn", CButton)
RegistChildComponent(LuaPinchFaceFuxiWnd, "CancelBtn", "CancelBtn", CButton)
RegistChildComponent(LuaPinchFaceFuxiWnd, "StateLab", "StateLab", UILabel)
RegistChildComponent(LuaPinchFaceFuxiWnd, "ResetBtn", "ResetBtn", CButton)
RegistChildComponent(LuaPinchFaceFuxiWnd, "ApplyBtn", "ApplyBtn", CButton)
RegistChildComponent(LuaPinchFaceFuxiWnd, "FailStateLab", "FailStateLab", UILabel)
RegistChildComponent(LuaPinchFaceFuxiWnd, "SelectedPic", "SelectedPic", UITexture)
RegistChildComponent(LuaPinchFaceFuxiWnd, "RotBtn", "RotBtn", CButton)
RegistChildComponent(LuaPinchFaceFuxiWnd, "ClipResetBtn", "ClipResetBtn", CButton)
RegistChildComponent(LuaPinchFaceFuxiWnd, "ClipApplyBtn", "ClipApplyBtn", CButton)
RegistChildComponent(LuaPinchFaceFuxiWnd, "ShuFaBtn", "ShuFaBtn", CButton)
RegistChildComponent(LuaPinchFaceFuxiWnd, "ZhuShiBtn", "ZhuShiBtn", CButton)

--@endregion RegistChildComponent end
RegistClassMember(LuaPinchFaceFuxiWnd, "m_Native")
RegistClassMember(LuaPinchFaceFuxiWnd, "m_PicSaveName")
RegistClassMember(LuaPinchFaceFuxiWnd, "m_PicResolution")
RegistClassMember(LuaPinchFaceFuxiWnd, "m_State")
RegistClassMember(LuaPinchFaceFuxiWnd, "m_Picture")
RegistClassMember(LuaPinchFaceFuxiWnd, "m_ClipedPicture")
RegistClassMember(LuaPinchFaceFuxiWnd, "m_PanelResolution")
RegistClassMember(LuaPinchFaceFuxiWnd, "m_OnPinchInDelegate")
RegistClassMember(LuaPinchFaceFuxiWnd, "m_OnPinchOutDelegate")
RegistClassMember(LuaPinchFaceFuxiWnd, "m_UploadSuccessed")
RegistClassMember(LuaPinchFaceFuxiWnd, "m_FuxiSuccessed")
RegistClassMember(LuaPinchFaceFuxiWnd, "m_CloseBtn")
RegistClassMember(LuaPinchFaceFuxiWnd, "m_CachedFacialPartData")
RegistClassMember(LuaPinchFaceFuxiWnd, "m_CachedFaceDnaData")
RegistClassMember(LuaPinchFaceFuxiWnd, "m_Url")
RegistClassMember(LuaPinchFaceFuxiWnd, "m_Target")
RegistClassMember(LuaPinchFaceFuxiWnd, "m_OutTimeTick")
RegistClassMember(LuaPinchFaceFuxiWnd, "m_Animation")
RegistClassMember(LuaPinchFaceFuxiWnd, "m_LastFxState")
RegistClassMember(LuaPinchFaceFuxiWnd, "m_FuxiDataExist")
RegistClassMember(LuaPinchFaceFuxiWnd, "m_Camera")
RegistClassMember(LuaPinchFaceFuxiWnd, "m_IsFromCamera")
RegistClassMember(LuaPinchFaceFuxiWnd, "m_LastView")

function LuaPinchFaceFuxiWnd:Awake()
    self.m_CloseBtn = self.MainView.transform:Find("CloseBtn"):GetComponent(typeof(CButton))
    self.m_Animation = self.UploadFx:GetComponent(typeof(Animation))

    UIEventListener.Get(self.m_CloseBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnCloseBtnClick()
    end)

    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.AddPicBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnAddPicBtnClick()
	end)


	
	UIEventListener.Get(self.CaptureBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCaptureBtnClick()
	end)


	
	UIEventListener.Get(self.CancelBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCancelBtnClick()
	end)


	
	UIEventListener.Get(self.ResetBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnResetBtnClick()
	end)


	
	UIEventListener.Get(self.ApplyBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnApplyBtnClick()
	end)


	
	UIEventListener.Get(self.RotBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRotBtnClick()
	end)


	
	UIEventListener.Get(self.ClipResetBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnClipResetBtnClick()
	end)


	
	UIEventListener.Get(self.ClipApplyBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnClipApplyBtnClick()
	end)
    --@endregion EventBind end

    CommonDefs.AddOnDragListener(self.SelectedPic.gameObject, DelegateFactory.Action_GameObject_Vector2(function (go, delta)
		self:OnScreenDrag(go, delta)
    end), false)

    UIEventListener.Get(self.MainPreviewTexture.gameObject).onDrag = LuaUtils.VectorDelegate(function(go, delta)
		self:OnSwipe(delta)
	end)

    UIEventListener.Get(self.ZhuShiBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnZhuShiBtnClick()
	end)

	UIEventListener.Get(self.ShuFaBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShuFaBtnClick()
	end)


    self.m_Native = CreateFromClass(NativeHandle)
    self.m_PicSaveName = "fuxi_face_image.png"
    self.m_PicResolution = 512
    self.m_PanelResolution = Vector2(680, 680)
    self.CaptureBtn.gameObject:SetActive(LuaCameraMgr:IsCameraValided())
    self.m_FuxiDataExist = false
    self.m_LastFxState = false
end

function LuaPinchFaceFuxiWnd:Init()
    self:StoreFacialData()
    self:InitCamera()
    self:SwitchState(EnumFuxiPinchFaceState.AddPic)
end

function LuaPinchFaceFuxiWnd:StoreFacialData()
    self.m_CachedFacialPartData = LuaPinchFaceMgr.m_RO.FacialPartData
    self.m_CachedFaceDnaData = LuaPinchFaceMgr.m_RO.FaceDnaData
end

function LuaPinchFaceFuxiWnd:RestoreFacialData()
    local customFacialData = CFacialMgr.GetCustomFacialData(self.m_CachedFaceDnaData, self.m_CachedFacialPartData)
    LuaPinchFaceMgr.m_IsFaceChanged = false
    LuaPinchFaceMgr:LoadPlayerRes(false, true, 0, 0, false, false, true, customFacialData)
end

function LuaPinchFaceFuxiWnd:InitCamera()
	self.m_Camera = GameObject.Find("__PinchFaceModel__/PinchFaceWndCCinemachineCtrl"):GetComponent(typeof(Camera))
    self.m_InitialCullingMask = self.m_Camera.cullingMask
end

function LuaPinchFaceFuxiWnd:OnSwipe(delta)
    if self.m_State == EnumFuxiPinchFaceState.Cliping then
        return
    end

	local deltaVal= - delta.x / Screen.width * 360
	self:RotateRoleModel(deltaVal)
end

-- 旋转模型
function LuaPinchFaceFuxiWnd:RotateRoleModel(degree)
	if not LuaPinchFaceMgr.m_RO then return end
    local t = LuaPinchFaceMgr.m_RO.transform
    if t and t.transform.gameObject.activeSelf then
		local newEulerAngleY = (t.transform.localEulerAngles.y + degree + 180) % 360 - 180
		if newEulerAngleY ~= t.transform.localEulerAngles.y then
			t.transform.localEulerAngles = Vector3(0, newEulerAngleY, 0)
			self:OnSetPinchFaceIKLookAtTo()
		end
    end
end

-- 调用IK
function LuaPinchFaceFuxiWnd:OnSetPinchFaceIKLookAtTo()
	if not LuaPinchFaceMgr.m_RO then return end
	local cameraObj = self.m_Camera
	local t = LuaPinchFaceMgr.m_IsUseIKLookAt and self.m_IKTarget or nil
	if not self:CheckCameraPosForIK() then
		t = nil
	end
	if LuaPinchFaceMgr.m_IsUseIKLookAt and self.m_Target == t then
		return
	end
	LuaPinchFaceMgr.m_RO:SetLookAtIKTarget(t, true, false)
	self.m_Target = t
end

function LuaPinchFaceFuxiWnd:CheckCameraPosForIK()
	if not LuaPinchFaceMgr.m_IsUseIKLookAt or nil == LuaPinchFaceMgr.m_RO then
		return false
	end
	local t = LuaPinchFaceMgr.m_RO.gameObject
    if t ~= nil and t.gameObject.activeSelf then
		local yAngle = t.transform.localEulerAngles.y
		local cameraObj = self.m_Camera
		local targetDir = CommonDefs.op_Subtraction_Vector3_Vector3(cameraObj.transform.position, LuaPinchFaceMgr.m_RO.transform.position)
		local conbined = (Vector3.Lerp(LuaPinchFaceMgr.m_RO:GetSlotTransform("Bip001 Pelvis").up, LuaPinchFaceMgr.m_RO.transform.forward, NormalCamera.m_lerpFactor)).normalized
		local pTocAngle = Vector3.Angle(targetDir, conbined)
		return pTocAngle <= 120 and (yAngle >= 90 and yAngle <= 270)
    end
	return false
end

function LuaPinchFaceFuxiWnd:OnEnable()
    local pinchWnd = CUIManager.instance.loadedUIs[CLuaUIResources.PinchFaceWnd]
    self.m_LastView = pinchWnd.transform:Find("PinchFaceMainView"):GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
	self.ZhuShiBtn:GetComponent(typeof(CButton)).Selected = self.m_LastView.ZhuShiButton.Selected
	self.ShuFaBtn:GetComponent(typeof(CButton)).Selected = self.m_LastView.ShuFaButton.Selected
    if self.m_LastView.ZhuShiButton.Selected then
        self.m_LastView:OnZhuShiButtonClick()
    end

    if CPinchFaceCinemachineCtrlMgr.Inst then
		CPinchFaceCinemachineCtrlMgr.Inst:SetScreenOffsetXOffset(-0.15)
	end
    self.m_OnPinchInDelegate = DelegateFactory.Action_GenericGesture(function(gesture) self:OnPinchIn(gesture) end)
    self.m_OnPinchOutDelegate = DelegateFactory.Action_GenericGesture(function(gesture) self:OnPinchOut(gesture) end)
    GestureRecognizer.AddGestureListener(GestureType.OnUIPinchIn, self.m_OnPinchInDelegate)
    GestureRecognizer.AddGestureListener(GestureType.OnUIPinchOut, self.m_OnPinchOutDelegate)
    GestureRecognizer.Inst:AddNeedUIPinchWnd(CLuaUIResources.PinchFaceFuxiWnd)
    g_ScriptEvent:AddListener("MouseScrollWheel",self,"OnMouseScrollWheel")
    g_ScriptEvent:AddListener("OnReviewFacialPicBegin", self, "OnReviewFacialPicBegin")
    g_ScriptEvent:AddListener("OnReviewFacialPicEnd", self, "OnReviewFacialPicEnd")
    g_ScriptEvent:AddListener("OnReviewFacialPicBeginInLua", self, "OnReviewFacialPicBeginInLua")
    g_ScriptEvent:AddListener("OnReviewFacialPicEndInLua", self, "OnReviewFacialPicEndInLua")

    CUIManager.SetUIVisibility(CLuaUIResources.PinchFaceWnd, false, "pinchface_fuxi")

end

function LuaPinchFaceFuxiWnd:OnDisable()
    if CPinchFaceCinemachineCtrlMgr.Inst then
		CPinchFaceCinemachineCtrlMgr.Inst:SetScreenOffsetXOffset(0.15)
	end
    GestureRecognizer.RemoveGestureListener(GestureType.OnUIPinchIn, self.m_OnPinchInDelegate)
    GestureRecognizer.RemoveGestureListener(GestureType.OnUIPinchOut, self.m_OnPinchOutDelegate)
    g_ScriptEvent:RemoveListener("MouseScrollWheel",self,"OnMouseScrollWheel")
    g_ScriptEvent:RemoveListener("OnReviewFacialPicBegin", self, "OnReviewFacialPicBegin")
    g_ScriptEvent:RemoveListener("OnReviewFacialPicEnd", self, "OnReviewFacialPicEnd")
    g_ScriptEvent:RemoveListener("OnReviewFacialPicBeginInLua", self, "OnReviewFacialPicBeginInLua")
    g_ScriptEvent:RemoveListener("OnReviewFacialPicEndInLua", self, "OnReviewFacialPicEndInLua")
    LuaPinchFaceMgr:SetPinchEnabled(true)
    CUIManager.SetUIVisibility(CLuaUIResources.PinchFaceWnd, true, "pinchface_fuxi")
end
--@region UIEvent

function LuaPinchFaceFuxiWnd:OnZhuShiBtnClick()
	self.m_LastView:OnZhuShiButtonClick()
	self.ZhuShiBtn:GetComponent(typeof(CButton)).Selected = self.m_LastView.ZhuShiButton.Selected
	self.ShuFaBtn:GetComponent(typeof(CButton)).Selected = self.m_LastView.ShuFaButton.Selected
end

function LuaPinchFaceFuxiWnd:OnShuFaBtnClick()
	self.m_LastView:OnShuFaButtonClick()
	self.ZhuShiBtn:GetComponent(typeof(CButton)).Selected = self.m_LastView.ZhuShiButton.Selected
	self.ShuFaBtn:GetComponent(typeof(CButton)).Selected = self.m_LastView.ShuFaButton.Selected
end


function LuaPinchFaceFuxiWnd:OnReviewFacialPicBegin(args)
    self:OnReviewFacialPicBeginInLua(args[0] or "", args[1] or "", args[2] or "", args[3] or 0)
end

function LuaPinchFaceFuxiWnd:OnReviewFacialPicEnd(args)
    self:OnReviewFacialPicEndInLua(args[0] or "", args[1] or "", args[2] or "", args[3] or 0)
end


function LuaPinchFaceFuxiWnd:OnReviewFacialPicBeginInLua(url, pic_id, context, status)
    print("ReviewFacialPicBegin",url, pic_id, context, status)
    if self.m_State ~= EnumFuxiPinchFaceState.UploadAndReviewProcessing then
        return
    end

    if status == -1 then
        print("ReviewFacialPicBegin Fail",url, pic_id, context, status)
        self:OnUploadAndReviewFail()
        return 
    end
end

function LuaPinchFaceFuxiWnd:OnReviewFacialPicEndInLua(url, pic_id, context, status)
    print("ReviewFacialPicEnd",url, pic_id, context, status)
    if status == -1 then
        print("ReviewFacialPicEnd Fail",url, pic_id, context, status)
        self:OnUploadAndReviewFail()
        return 
    end
    if self.m_State ~= EnumFuxiPinchFaceState.UploadAndReviewProcessing then
        return
    end

    self.m_Url = url
    self.UploadedPic.mainTexture = self.m_ClipedPicture
    self:OnUploadAndReviewSuccess()
end

function LuaPinchFaceFuxiWnd:OnFuxiFinished(code)
    if self.m_State ~= EnumFuxiPinchFaceState.FuxiProcessing then
        return
    end
    if code == 200 then
        self:OnFuxiSuccess()
    else
        self:OnFuxiFail(code)
    end
end

function LuaPinchFaceFuxiWnd:OnAddPicBtnClick()
    self.m_IsFromCamera = false
    self:SettingPicAndGifFromMobile()
end

function LuaPinchFaceFuxiWnd:OnCaptureBtnClick()
    self.m_IsFromCamera = true
    self:OpenCameraWnd()
end

function LuaPinchFaceFuxiWnd:OnCancelBtnClick()
    if self.m_State == EnumFuxiPinchFaceState.UploadAndReviewProcessing then
        self:SwitchState(EnumFuxiPinchFaceState.AddPic)
    else
        self:SwitchState(EnumFuxiPinchFaceState.FuxiResult)
    end
end

function LuaPinchFaceFuxiWnd:OnResetBtnClick()
    self:SwitchState(EnumFuxiPinchFaceState.AddPic)
end

function LuaPinchFaceFuxiWnd:OnApplyBtnClick()
    if self.m_State == EnumFuxiPinchFaceState.UploadAndReviewResult then
        self:StartOutTimeTick(EnumFuxiPinchFaceState.UploadAndReviewResult, 10 * 1000)
        CFuxiFaceDNAMgr.m_RO = LuaPinchFaceMgr.m_RO
        CFuxiFaceDNAMgr.m_Class = LuaPinchFaceMgr.m_CurEnumClass
        CFuxiFaceDNAMgr.m_Gender = LuaPinchFaceMgr.m_CurEnumGender
        CFuxiFaceDNAMgr.Inst:PostRequestFuxiDNAData(self.m_Url, DelegateFactory.Action_int(function (code)
            if code == 200 then
                local data = CFacialMgr.CurFacialSaveData
                if data then
                    LuaPinchFaceMgr:ApplyData(data)
                end
            end
            self:OnFuxiFinished(code)
        end))
        self:SwitchState(EnumFuxiPinchFaceState.FuxiProcessing)
    else
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("PinchFace_Fuxi_ApplyMakeSure"), DelegateFactory.Action(function()
            CFuxiFaceDNAMgr.Inst:TryReportApplyLog("PhotoApply")
            CUIManager.CloseUI(CLuaUIResources.PinchFaceFuxiWnd)
        end),nil,LocalString.GetString("确定"),LocalString.GetString("取消"),false)
    end
end

function LuaPinchFaceFuxiWnd:OnRotBtnClick()
    if self.m_Picture == nil then
        return
    end
    self:InitClipPicture(LoadPicMgr.RotatePic90(self.m_Picture))
end

function LuaPinchFaceFuxiWnd:OnClipResetBtnClick()
    if self.m_IsFromCamera then
        self:OpenCameraWnd()
    else
        self:SettingPicAndGifFromMobile()
    end
end

function LuaPinchFaceFuxiWnd:OnClipApplyBtnClick()
    self.m_ClipedPicture = self:ClipPic()
    self:UploadPicture(self.m_ClipedPicture)
end

function LuaPinchFaceFuxiWnd:OnCloseBtnClick()
    if self.m_FuxiDataExist then
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("PinchFace_Fuxi_ExitMakeSure"), function()
			self:RestoreFacialData()
            CUIManager.CloseUI(CLuaUIResources.PinchFaceFuxiWnd)
		end, nil, nil, nil, false)
    else
        CUIManager.CloseUI(CLuaUIResources.PinchFaceFuxiWnd)
    end
end

function LuaPinchFaceFuxiWnd:OnPinchIn(gesture)
    local deltaPinch = gesture.deltaPinch
    local pinchScale = math.min(math.max(deltaPinch / 400, 0),1)
    self:OnPinch(-pinchScale)
end

function LuaPinchFaceFuxiWnd:OnPinchOut(gesture)
    local deltaPinch = gesture.deltaPinch
    local pinchScale = math.min(math.max(deltaPinch / 400, 0),1)
    self:OnPinch(pinchScale)
end

function LuaPinchFaceFuxiWnd:OnMouseScrollWheel()
    local v = Input.GetAxis("Mouse ScrollWheel")
    self:OnPinch(v)
end

function LuaPinchFaceFuxiWnd:OnPinch(delta)
    if self.m_State ~= EnumFuxiPinchFaceState.Cliping then
        return
    end
    local localScale = self.SelectedPic.transform.localScale
    localScale.x = localScale.x + delta
    localScale.y = localScale.y + delta
    localScale.z = localScale.z + delta
    self.SelectedPic.transform.localScale = localScale
    self:LimitPicScale()
    self:LimitPicPosition()

    self.SelectedPic:ResizeCollider()
end

function LuaPinchFaceFuxiWnd:OnScreenDrag(go, delta)
    if EasyTouch.GetTouchCount() == 1 then
        local localPosition = self.SelectedPic.transform.localPosition
        localPosition.x = localPosition.x + delta.x
        localPosition.y = localPosition.y + delta.y
        self.SelectedPic.transform.localPosition = localPosition
        self:LimitPicPosition()
    end
end
--@endregion UIEvent

function LuaPinchFaceFuxiWnd:SwitchState(state)
    self.m_State = state

    local isHidePlayer = state == EnumFuxiPinchFaceState.Cliping
    self.m_Camera.cullingMask = (isHidePlayer and 0 or 2^LayerDefine.ModelForNGUI_3D ) + 2^LayerDefine.WaterReflection
    self.UploadedPic.gameObject:SetActive(state ~= EnumFuxiPinchFaceState.AddPic)
    self.MainView:SetActive(state ~= EnumFuxiPinchFaceState.Cliping)
    self.ClipView:SetActive(state == EnumFuxiPinchFaceState.Cliping)
    self.UploadTip:SetActive(state == EnumFuxiPinchFaceState.AddPic)
    self.AddPicBtns:SetActive(state == EnumFuxiPinchFaceState.AddPic)
    self.ProcessingBtns:SetActive(state == EnumFuxiPinchFaceState.UploadAndReviewProcessing or state == EnumFuxiPinchFaceState.FuxiProcessing)

    local fxState = state == EnumFuxiPinchFaceState.UploadAndReviewProcessing or state == EnumFuxiPinchFaceState.FuxiProcessing
    if self.m_LastFxState ~= fxState then
        self.m_Animation:Play(fxState and "pinchfacefuxiwnd_uploading_show" or "pinchfacefuxiwnd_uploading_close")
    end
    if not fxState then
        self:EndOutTimeTick()
    end
    self.m_LastFxState = fxState

    self.ResultBtns:SetActive(state == EnumFuxiPinchFaceState.UploadAndReviewResult or state == EnumFuxiPinchFaceState.FuxiResult)
    self.StateLab.text = state == EnumFuxiPinchFaceState.UploadAndReviewProcessing and LocalString.GetString("上传中...") or LocalString.GetString("生成中...")
    self.ApplyBtn.Text = state == EnumFuxiPinchFaceState.UploadAndReviewResult and LocalString.GetString("生成捏脸") or LocalString.GetString("应用")
    self.FailStateLab.text = state == EnumFuxiPinchFaceState.UploadAndReviewResult and LocalString.GetString("上传失败") or LocalString.GetString("生成失败")
    self.UploadedPic.alpha = state == EnumFuxiPinchFaceState.FuxiProcessing and 0.5 or 1
    self.ClipResetBtn.Text = self.m_IsFromCamera and LocalString.GetString("重拍") or LocalString.GetString("重选")
    if state == EnumFuxiPinchFaceState.UploadAndReviewResult then
        self.ApplyBtn.gameObject:SetActive(self.m_UploadSuccessed)
        self.FailStateLab.gameObject:SetActive(not self.m_UploadSuccessed)
        self.UploadFailTipLab.gameObject:SetActive(not self.m_UploadSuccessed)
    elseif state == EnumFuxiPinchFaceState.FuxiResult then
        self.ApplyBtn.gameObject:SetActive(self.m_FuxiSuccessed)
        self.FailStateLab.gameObject:SetActive(not self.m_FuxiSuccessed)
        self.FuxiFailTipLab.gameObject:SetActive(not self.m_FuxiSuccessed)
    else
        self.UploadFailTipLab.gameObject:SetActive(false)
        self.FuxiFailTipLab.gameObject:SetActive(false)
    end

    LuaPinchFaceMgr:SetPinchEnabled(state ~= EnumFuxiPinchFaceState.Cliping)
end

function LuaPinchFaceFuxiWnd:SettingPicAndGifFromMobile()
    VoiceManager.onPicMsg = DelegateFactory.Action_string(function (strResult)
        if (strResult ~= ToStringWrap(ENUM_AVATAR_RESULT.eResult_Success)) then
        end
		Main.Inst:StartCoroutine(LoadPicMgr.LoadMomentTexture(self.m_PicSaveName, DelegateFactory.Action_bytes(function (bytes)
			self:AddPicBack(bytes)
		end)))
	end)

    self.m_Native:SettingPicAndGifFromMobile("Main/VoiceListener", "OnAvaterCallBack", self.m_PicSaveName, DelegateFactory.Action_bytes(function (bytes)
        self:AddPicBack(bytes)
    end), 0, self.m_PicResolution, self.m_PicResolution)
end

function LuaPinchFaceFuxiWnd:OpenCameraWnd()
    LuaCameraMgr:OpenCameraWnd(1, 60, 1, "Camera_Fuxi_TopTip", "FaceTip", false, function(bytes)
        self:AddPicBack(bytes)
    end)
end

function LuaPinchFaceFuxiWnd:AddPicBack(bytes)
    if not LoadPicMgr.CheckGifPic(bytes) then
        local tex = CreateFromClass(Texture2D, 4, 4, TextureFormat.RGB24, false)
        CommonDefs.LoadImage(tex, bytes)
        if tex.width == 8 and tex.height == 8 then
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("该照片格式不支持！"))
            return
        end
        self:InitClipPicture(tex)
        self:SwitchState(EnumFuxiPinchFaceState.Cliping)
    else
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("该照片格式不支持！"))
    end
end

function LuaPinchFaceFuxiWnd:InitClipPicture(tex)
    self.m_Picture = tex
    self.SelectedPic.mainTexture = tex
    self.SelectedPic.height = tex.height
    self.SelectedPic.width = tex.width
    self.SelectedPic.transform.localScale = Vector3.one
    self.SelectedPic.transform.localPosition = Vector3.zero

    local width = self.SelectedPic.width
    local height = self.SelectedPic.height
    local clipWidth = self.m_PanelResolution.x
    local clipHeight = self.m_PanelResolution.y

    self.m_MinScale = math.max(math.max(clipWidth / width, clipHeight / height), 0.25)
    self.m_MaxScale = math.max(math.min(width / clipWidth, height / clipHeight), 4)
    self:LimitPicScale()
end

function LuaPinchFaceFuxiWnd:LimitPicPosition()
    local localPosition = self.SelectedPic.transform.localPosition
    local localScale = self.SelectedPic.transform.localScale
    local limitX = math.abs(self.SelectedPic.width * localScale.x - self.m_PanelResolution.x) / 2
    local limitY = math.abs(self.SelectedPic.height * localScale.y - self.m_PanelResolution.y) / 2

    localPosition.x = Mathf.Clamp(localPosition.x, -limitX, limitX)
    localPosition.y = Mathf.Clamp(localPosition.y, -limitY, limitY)

    self.SelectedPic.transform.localPosition = localPosition
end

function LuaPinchFaceFuxiWnd:LimitPicScale()
    local localScale = self.SelectedPic.transform.localScale
    localScale.x = Mathf.Clamp(localScale.x, self.m_MinScale, self.m_MaxScale)
    localScale.y = Mathf.Clamp(localScale.y, self.m_MinScale, self.m_MaxScale)
    localScale.z = Mathf.Clamp(localScale.z, self.m_MinScale, self.m_MaxScale)
    self.SelectedPic.transform.localScale = localScale
end

function LuaPinchFaceFuxiWnd:ClipPic()
    local localPosition = self.SelectedPic.transform.localPosition
    local localScale = self.SelectedPic.transform.localScale

    local originalWidth = self.SelectedPic.mainTexture.width
    local originalHeight = self.SelectedPic.mainTexture.height

    local x = ((originalWidth * localScale.x - self.m_PanelResolution.x) / 2 - localPosition.x ) / localScale.x
    local y = ((originalHeight * localScale.y - self.m_PanelResolution.y) / 2 - localPosition.y) / localScale.y
    local width = self.m_PanelResolution.x / localScale.x
    local height = self.m_PanelResolution.y / localScale.y

    local rect = Rect(x, y, width, height)
    return LoadPicMgr.ClipPic(self.m_Picture, rect)
end

function LuaPinchFaceFuxiWnd:UploadPicture(clipedTex)
    self:StartOutTimeTick(EnumFuxiPinchFaceState.AddPic, 10 * 1000)
    CFuxiFaceDNAMgr.Inst:UploadPic2FpAndReview(clipedTex)
    self:SwitchState(EnumFuxiPinchFaceState.UploadAndReviewProcessing)
end

function LuaPinchFaceFuxiWnd:OnUploadAndReviewSuccess()
    self.m_UploadSuccessed = true
    self:SwitchState(EnumFuxiPinchFaceState.UploadAndReviewResult)
end

function LuaPinchFaceFuxiWnd:OnUploadAndReviewFail()
    self.m_UploadSuccessed = false
    self:SwitchState(EnumFuxiPinchFaceState.UploadAndReviewResult)
end

function LuaPinchFaceFuxiWnd:OnFuxiSuccess()
    self.m_FuxiSuccessed = true
    self.m_FuxiDataExist = true
    self:SwitchState(EnumFuxiPinchFaceState.FuxiResult)
end

function LuaPinchFaceFuxiWnd:OnFuxiFail(code)
    local msgName = "PinchFace_Fuxi_ErrorRet_" .. code
    local messageId = MessageMgr.Inst:GetMessageIdByName(msgName)
     
    self.FuxiFailTipLab.text = messageId ~= 0 and g_MessageMgr:FormatMessage(msgName) or g_MessageMgr:FormatMessage("PinchFace_Fuxi_Error") 
    self.m_FuxiSuccessed = false
    self:SwitchState(EnumFuxiPinchFaceState.FuxiResult)
end

function LuaPinchFaceFuxiWnd:OnDestroy()
    self.m_Camera.cullingMask = self.m_InitialCullingMask
    self:EndOutTimeTick()
end

function LuaPinchFaceFuxiWnd:StartOutTimeTick(backState, time)
    UnRegisterTick(self.m_OutTimeTick)
    self.m_OutTimeTick = RegisterTickOnce(function()
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("与服务器连接超时，请稍后重试！"))
        self:SwitchState(backState)
    end, time)
end

function LuaPinchFaceFuxiWnd:EndOutTimeTick()
    UnRegisterTick(self.m_OutTimeTick)
    self.m_OutTimeTick = nil
end

