require("3rdParty/ScriptEvent")
require("common/common_include")

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CUITexture = import "L10.UI.CUITexture"
local Extensions = import "Extensions"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local GameObject = import "UnityEngine.GameObject"
local NGUITools = import "NGUITools"
local LocalString = import "LocalString"
local YuanDan2019_TaskBook = import "L10.Game.YuanDan2019_TaskBook"
local YuanDan2019_TaskBookReward = import "L10.Game.YuanDan2019_TaskBookReward"
local Vector3 = import "UnityEngine.Vector3"
local UILabel = import "UILabel"
local CButton = import "L10.UI.CButton"
local YuanDan2019_TaskBookSetting = import "L10.Game.YuanDan2019_TaskBookSetting"
local NGUIText = import "NGUIText"
local CItemMgr = import "L10.Game.CItemMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local NGUIMath = import "NGUIMath"

LuaNewYear2019TaskBookWnd=class()
RegistChildComponent(LuaNewYear2019TaskBookWnd,"closeBtn", GameObject)
RegistChildComponent(LuaNewYear2019TaskBookWnd,"addFNode", GameObject)
RegistChildComponent(LuaNewYear2019TaskBookWnd,"templateNode", GameObject)
RegistChildComponent(LuaNewYear2019TaskBookWnd,"slider", UISlider)
RegistChildComponent(LuaNewYear2019TaskBookWnd,"bonusNode", GameObject)
RegistChildComponent(LuaNewYear2019TaskBookWnd,"scoreText", UILabel)
RegistChildComponent(LuaNewYear2019TaskBookWnd,"tipText", UILabel)
RegistChildComponent(LuaNewYear2019TaskBookWnd,"bonusFNode", GameObject)


--RegistClassMember(LuaNewYear2019TaskBookWnd, "m_")

function LuaNewYear2019TaskBookWnd:RefreshTask(taskId,taskLevel)
  local itemTemplateId = YuanDan2019_TaskBookSetting.GetData().RefreshConsumeItemId
  local itemCNum = YuanDan2019_TaskBookSetting.GetData().RefreshConsumeCount

  local count = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, itemTemplateId)

  if count >= itemCNum then
      if taskLevel >= 4 then
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("NEWYEAR_TASKREF_ITEM"),
        DelegateFactory.Action(function ()
          Gac2Gas.NewYearTaskBookRefreshTask(LuaNewYear2019Mgr.TaskItemId,taskId)
        end),nil, LocalString.GetString("刷新"), nil, false)
      else
        Gac2Gas.NewYearTaskBookRefreshTask(LuaNewYear2019Mgr.TaskItemId,taskId)
      end
  else
    MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("NEWYEAR_TASKREF_YINLIANG"),
    DelegateFactory.Action(function ()
      Gac2Gas.NewYearTaskBookRefreshTask(LuaNewYear2019Mgr.TaskItemId,taskId)
    end),nil, LocalString.GetString("刷新"), nil, false)
  end


end

function LuaNewYear2019TaskBookWnd:InitShowNode()
  Extensions.RemoveAllChildren(self.addFNode.transform)

  if not LuaNewYear2019Mgr.TaskInfoTable then
    return
  end

  if not CClientMainPlayer.Inst then
    return
  end

  local nodeWidth = 250
  local totalCount = #LuaNewYear2019Mgr.TaskInfoTable
  for i,v in ipairs(LuaNewYear2019Mgr.TaskInfoTable) do
    local node = NGUITools.AddChild(self.addFNode,self.templateNode)
    node:SetActive(true)
    node.transform.localPosition = Vector3(nodeWidth * (i - totalCount / 2 - 0.5),0,0)
    local taskId = v[1]
    local taskLevel = v[2]
    local taskInfo = YuanDan2019_TaskBook.GetData(taskId)
    if taskInfo then
      local npcIcon = taskInfo.Icon
      local npcName = taskInfo.Name

      local totalNum = taskInfo.LevelRequire[v[2]-1]
      local nowNum = v[3]
      if tonumber(nowNum) >= tonumber(totalNum) then
        nowNum = totalNum
      end

      if tonumber(totalNum) > 10000 then
        totalNum = totalNum / 10000 .. LocalString.GetString('万')
      end

      if tonumber(nowNum) > 10000 then
        nowNum = nowNum / 10000 .. LocalString.GetString('万')
      end

      local desc = ''

      if taskId == 8 then
        local charClass = EnumToInt(CClientMainPlayer.Inst.Class)
        local iconTable = g_LuaUtil:StrSplit(taskInfo.Icon,";")
        local nameTable = g_LuaUtil:StrSplit(taskInfo.Name,";")
        for i,v in pairs(iconTable) do
          local vTable = g_LuaUtil:StrSplit(v,',')
          if vTable and vTable[1] and vTable[2] and tonumber(vTable[1]) == charClass then
            npcIcon = vTable[2]
          end
        end

        for i,v in pairs(nameTable) do
          local vTable = g_LuaUtil:StrSplit(v,',')
          if vTable and vTable[1] and vTable[2] and tonumber(vTable[1]) == charClass then
            npcName = vTable[2]
            npcName = string.gsub(npcName,'#r','\n')
          end
        end
        desc = SafeStringFormat3(taskInfo.Description,totalNum,taskInfo.LevelExtraRequire[v[2]-1],nowNum,totalNum)
      else
        desc = SafeStringFormat3(taskInfo.Description,totalNum,nowNum,totalNum)
      end

      local score = taskInfo.LevelScore[v[2]-1]

      node.transform:Find("name"):GetComponent(typeof(UILabel)).text = npcName
      local descLabel = node.transform:Find("tipText"):GetComponent(typeof(UILabel))
      desc = string.gsub(desc,'#r','\n')
      descLabel.text = desc
      local colorTable = {'c7701c','00a40f','519fff','ff5050','c000c0'}
      if colorTable[v[2]] then
        local descColor = NGUIText.ParseColor24(colorTable[v[2]], 0)
        descLabel.color = descColor
      end
      node.transform:Find("bonusText"):GetComponent(typeof(UILabel)).text = score .. LocalString.GetString("积分")
      local btn = node.transform:Find("btn").gameObject
      local btnLabel = btn.transform:Find("text"):GetComponent(typeof(UILabel))
      local btnCBtn = btn:GetComponent(typeof(CButton))
      local npcIconNode = node.transform:Find("icon"):GetComponent(typeof(CUITexture))
      npcIconNode:LoadNPCPortrait(npcIcon, false)
      if v[4] == 1 then
        btnLabel.text = LocalString.GetString("已完成")
        btnCBtn.Enabled = false
      elseif v[5] == 1 then
        btnLabel.text = LocalString.GetString("提交")
        btnCBtn.Enabled = true
        CommonDefs.AddOnClickListener(btn, DelegateFactory.Action_GameObject(function(go)

          MessageWndManager.ShowOKCancelMessage(LocalString.GetString("#R每人只能完成5个任务，提交后将无法刷新，请谨慎选择，#Y是否继续提交？#n"),
          DelegateFactory.Action(function ()
            Gac2Gas.NewYearTaskBookFinishTask(LuaNewYear2019Mgr.TaskItemId,taskId)
          end), DelegateFactory.Action(function ()
            self:RefreshTask(taskId,taskLevel)
          end), LocalString.GetString("提交"), LocalString.GetString("刷新"), true)
        end), false)
      else
        btnLabel.text = LocalString.GetString("刷新")
        btnCBtn.Enabled = true
        CommonDefs.AddOnClickListener(btn, DelegateFactory.Action_GameObject(function(go)
          self:RefreshTask(taskId,taskLevel)
        end), false)
      end

    end
  end
end

function LuaNewYear2019TaskBookWnd:InitBonusNode()
  Extensions.RemoveAllChildren(self.bonusFNode.transform)
  if LuaNewYear2019Mgr.TaskRewardTable then
    local maxData = YuanDan2019_TaskBookReward.GetData(YuanDan2019_TaskBookReward.GetDataCount())
    local maxValue = maxData.Score
    self.slider.value = LuaNewYear2019Mgr.TaskScore / maxValue
    local maxLength = 950
    for i,v in pairs(LuaNewYear2019Mgr.TaskRewardTable) do
      local data = YuanDan2019_TaskBookReward.GetData(v[1])
      if data then
        local node = NGUITools.AddChild(self.bonusFNode,self.bonusNode)
        node:SetActive(true)
        if v[2] == 1 then
          node.transform:Find("close").gameObject:SetActive(false)
          node.transform:Find("open").gameObject:SetActive(true)
        else
          node.transform:Find("close").gameObject:SetActive(true)
          node.transform:Find("open").gameObject:SetActive(false)
          local itemId = data.ItemID
          local btn = node.transform:Find("btn").gameObject
          local b1 = NGUIMath.CalculateRelativeWidgetBounds(btn.transform)
          local height = b1.size.y
          local width = b1.size.x

          CommonDefs.AddOnClickListener(btn, DelegateFactory.Action_GameObject(function(go)
            --CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, CItemInfoMgr.AlignType.Top, btn.transform.position.x, btn.transform.position.y, width, height)
            CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, CItemInfoMgr.AlignType.Top, 0,0,0,0)
          end), false)
        end
        node.transform.localPosition = Vector3(maxLength * data.Score / maxValue, 0, 0)
        node.transform:Find("Label"):GetComponent(typeof(UILabel)).text = data.Score

      end
    end
  end
end

function LuaNewYear2019TaskBookWnd:Init()
  self.templateNode:SetActive(false)
  self.bonusNode:SetActive(false)
  self.scoreText.text = LuaNewYear2019Mgr.TaskScore
  self:InitShowNode()
  self:InitBonusNode()
end

function LuaNewYear2019TaskBookWnd:OnDestroy()

end

return LuaNewYear2019TaskBookWnd
