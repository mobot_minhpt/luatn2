local LuaUtils = import "LuaUtils"
local CCityWarMgr = import "L10.Game.CCityWarMgr"
local EUIModuleGroup = import "L10.UI.CUIManager+EUIModuleGroup"
local LocalString = import "LocalString"
local CGuideMgr=import "L10.Game.Guide.CGuideMgr"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local Extensions = import "Extensions"
local UIGrid = import "UIGrid"

CLuaCityWarMiniMap = class()
RegistClassMember(CLuaCityWarMiniMap, "m_ConstructionButton")
RegistClassMember(CLuaCityWarMiniMap, "m_ExpandButtonTrans")
RegistClassMember(CLuaCityWarMiniMap, "m_CityBtnObj")
RegistClassMember(CLuaCityWarMiniMap, "m_UIGrid")
RegistClassMember(CLuaCityWarMiniMap, "m_UpgradeBtnObj")
RegistClassMember(CLuaCityWarMiniMap, "m_ChallengeButton")
RegistClassMember(CLuaCityWarMiniMap, "m_RepairBtnObj")
CLuaCityWarMiniMap.Path = "ui/citywar/LuaCityWarMiniMap"

function CLuaCityWarMiniMap:Awake( ... )
	self.m_UIGrid = self.transform:Find("Anchor/Tip"):GetComponent(typeof(UIGrid))
	self.m_ConstructionButton = self.transform:Find("Anchor/Tip/ConstructionBtn").gameObject
	self.m_ExpandButtonTrans = self.transform:Find("Anchor/ExpandButton")
	self.m_CityBtnObj = self.transform:Find("Anchor/Tip/CityBtn").gameObject
	self.m_UpgradeBtnObj = self.transform:Find("Anchor/Tip/UpgradeBtn").gameObject
	self.m_ChallengeButton = self.transform:Find("Anchor/Tip/ChallengeBtn").gameObject
	self.m_RepairBtnObj = self.transform:Find("Anchor/Tip/RepairBtn").gameObject
	self.m_ConstructionButton:SetActive(false)
	self.m_UpgradeBtnObj:SetActive(false)
	self.m_RepairBtnObj:SetActive(false)
	self.m_UIGrid:Reposition()
	self:ShowCity()
end

function CLuaCityWarMiniMap:Init( ... )
	UIEventListener.Get(self.m_ConstructionButton).onClick = LuaUtils.VoidDelegate(function ( ... )
		self:OnConstructionBtnClick()
	end)

	UIEventListener.Get(self.m_CityBtnObj).onClick = LuaUtils.VoidDelegate(function ( ... )
		self:OnCityBtnClick()
	end)

	UIEventListener.Get(self.m_UpgradeBtnObj).onClick = LuaUtils.VoidDelegate(function ( ... )
		self:OnUpgradeBtnClick()
	end)

	UIEventListener.Get(self.m_RepairBtnObj).onClick = LuaUtils.VoidDelegate(function()
		Gac2Gas.RequestRepairAllCityUnitInfo(CLuaCityWarMgr.CurrentCityId)
	end)

	UIEventListener.Get(self.m_ExpandButtonTrans.gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
		Extensions.SetLocalRotationZ(self.m_ExpandButtonTrans, 0)
		CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
	end)
	UIEventListener.Get(self.m_ChallengeButton).onClick = LuaUtils.VoidDelegate(function ( ... )
		Gac2Gas.QueryMirrorWarMapInfo()
	end)
end

function CLuaCityWarMiniMap:OnConstructionBtnClick( ... )
	CCityWarMgr.Inst.PlaceMode = true

	CameraFollow.Inst:SetMaximumPinchIn(false,false)
	local excepts = {"MiddleNoticeCenter","GuideWnd", "PlaceCityUnitWnd"}
	local List_String = MakeGenericClass(List,cs_string)
	CUIManager.HideUIGroupByChangeLayer(EUIModuleGroup.EGameUI, Table2List(excepts, List_String), false, true)
	CUIManager.HideUIGroupByChangeLayer(EUIModuleGroup.EBgGameUI, Table2List(excepts, List_String), false, true)
	CUIManager.ShowUI(CLuaUIResources.PlaceCityUnitWnd)

	local bIsInCopyScene = CCityWarMgr.Inst:IsInCityWarCopyScene()
    if bIsInCopyScene then
        Gac2Gas.CheckMyRights("RebornPoint", 0)
    else
        Gac2Gas.CheckMyRights("CityBuild", 0)
    end
end


function CLuaCityWarMiniMap:GetGuideGo( methodName )
	if methodName=="GetConstructionButton" then
		if self.m_ConstructionButton==nil or not self.m_ConstructionButton.activeSelf then
			CGuideMgr.Inst:EndCurrentPhase()
			return nil
		end
		return self.m_ConstructionButton
	end
	return nil
end

function CLuaCityWarMiniMap:OnCityBtnClick( ... )
	Gac2Gas.QueryMyCityData(EnumQueryCityDataAction.eCityData)
end

function CLuaCityWarMiniMap:OnUpgradeBtnClick()
	local level = 0
	if CLuaCityWarMgr.CurrentBasicInfo then
		level = CLuaCityWarMgr.CurrentBasicInfo.Grade
	end
	local ddata = CityWar_CityGrade.GetData(level)
	if not ddata then return end


	local msg = LocalString.GetString([=[<tip>
	<title>建筑可以升级了！</title>
	]=])
	local msgTemplate = [=[<p>%s</p>
	]=]
	CityWar_UnitTypeName.Foreach(function(typeId, typeData)
		if typeId ~= 25 then
			local str = typeData.DisplayName .. "<tab w=270></tab>" .. CLuaCityWarMgr:GetPlacedCountForType(typeId) .. "/" .. CLuaCityWarMgr:GetTypeLimit(ddata, typeId)
			local concatStr = SafeStringFormat3(msgTemplate, str)
			msg = msg .. concatStr
		end
	end)

	msg = msg .. LocalString.GetString([=[<suffix>在非城建状态下，长按建筑可直接进行升级</suffix>
	</tip>]=])
	g_MessageMgr:ShowMessage("CUSTOM_STRING13", msg)
end

function CLuaCityWarMiniMap:HideTopAndRightTipWnd( ... )
	Extensions.SetLocalRotationZ(self.m_ExpandButtonTrans, 180)
end

function CLuaCityWarMiniMap:ShowCity( ... )
	local guildId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.GuildId or 0
	if CLuaCityWarMgr.CurrentGuildId == guildId then
		self.m_ConstructionButton:SetActive(true)
		self.m_UpgradeBtnObj:SetActive(true)
		self.m_RepairBtnObj:SetActive(true)
		self.m_UIGrid:Reposition()
	end
end

function CLuaCityWarMiniMap:HideCity( ... )
	self.m_ConstructionButton:SetActive(false)
	self.m_UpgradeBtnObj:SetActive(false)
	self.m_RepairBtnObj:SetActive(false)
	self.m_UIGrid:Reposition()
end

function CLuaCityWarMiniMap:OnEnable( ... )
	g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "HideTopAndRightTipWnd")
	g_ScriptEvent:AddListener("UpdateCityWarGrade", self, "ShowCity")
	g_ScriptEvent:AddListener("HideCity", self, "HideCity")
end

function CLuaCityWarMiniMap:OnDisable( ... )
	g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "HideTopAndRightTipWnd")
	g_ScriptEvent:RemoveListener("HideCity", self, "HideCity")
	g_ScriptEvent:RemoveListener("UpdateCityWarGrade", self, "ShowCity")
end

function CLuaCityWarMiniMap:GetGuideGo( methodName)
    if methodName == "GetChallengeButton" then
		return self.m_ChallengeButton
	elseif methodName == "GetCityButton" then
		return self.m_CityBtnObj
	elseif methodName == "GetConstructionButton" then
		return self.m_ConstructionButton
    else
        return nil
    end
end
