local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CUITexture = import "L10.UI.CUITexture"
local Animation = import "UnityEngine.Animation"
LuaDaFuWongItemChangeWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaDaFuWongItemChangeWnd, "MyPortrait", "MyPortrait", CUITexture)
RegistChildComponent(LuaDaFuWongItemChangeWnd, "Portrait", "Portrait", CUITexture)

--@endregion RegistChildComponent end
RegistClassMember(LuaDaFuWongItemChangeWnd, "m_Tick")
RegistClassMember(LuaDaFuWongItemChangeWnd, "m_Anim")
function LuaDaFuWongItemChangeWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.m_Anim = self.gameObject:GetComponent(typeof(Animation))
    self.m_Tick = nil
end

function LuaDaFuWongItemChangeWnd:Init()
    if LuaDaFuWongMgr.CurAniInfo then
        local player1 = LuaDaFuWongMgr.PlayerInfo[LuaDaFuWongMgr.CurAniInfo.playerId]
        local player2 = LuaDaFuWongMgr.PlayerInfo[LuaDaFuWongMgr.CurAniInfo.selectPlayerId]
        self.MyPortrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(player1.class, player1.gender, -1), false)
        self.Portrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(player2.class, player2.gender, -1), false)
    end
    if self.m_Anim then
        if self.m_Tick then
            UnRegisterTick(self.m_Tick)
            self.m_Tick = nil
        end
        self.m_Anim:Play("dafuwongitemchangewnd_rol")
        self.m_Tick = RegisterTickOnce(function()
            self.gameObject:SetActive(false)
        end, self.m_Anim.clip.length * 1000)
    end
    -- SoundManager.Inst:PlayOneShot("event:/L10/L10_UI/DaFuWeng/UI_DaFuWeng_Exchange", Vector3.zero, nil, 0)
end
function LuaDaFuWongItemChangeWnd:OnDisable()
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
    end
end
--@region UIEvent

--@endregion UIEvent

