
LuaCHChatRoomHandsUpAlert = class()

RegistClassMember(LuaCHChatRoomHandsUpAlert, "m_Sprite")

function LuaCHChatRoomHandsUpAlert:Awake()
    self.m_Sprite = self.transform:GetComponent(typeof(UISprite))
end

function LuaCHChatRoomHandsUpAlert:OnEnable()
    --状态发生改变，包括举手
    g_ScriptEvent:AddListener("ClubHouse_Member_Update", self, "OnClubHouseMemberUpdate")
    --主播或者观众离开房间
    g_ScriptEvent:AddListener("ClubHouse_Broadcaster_Leave_Room", self, "OnClubHouseBroadcasterLeaveRoom")
    g_ScriptEvent:AddListener("ClubHouse_Audience_Leave_Room", self, "OnClubHouseAudienceLeaveRoom")
    -- 上下麦
    g_ScriptEvent:AddListener("ClubHouse_Member_Character_Changed", self, "OnClubHouseMemberCharacterChanged")
    -- 房主清空状态广播
    g_ScriptEvent:AddListener("ClubHouse_Notify_ClearStatusAll", self, "OnClubHouseNotifyClearStatusAll")
    self:UpdateDisplay()
end

function LuaCHChatRoomHandsUpAlert:OnDisable()
    g_ScriptEvent:RemoveListener("ClubHouse_Member_Update", self, "OnClubHouseMemberUpdate")
    g_ScriptEvent:RemoveListener("ClubHouse_Broadcaster_Leave_Room", self, "OnClubHouseBroadcasterLeaveRoom")
    g_ScriptEvent:RemoveListener("ClubHouse_Audience_Leave_Room", self, "OnClubHouseAudienceLeaveRoom")
    g_ScriptEvent:RemoveListener("ClubHouse_Member_Character_Changed", self, "OnClubHouseMemberCharacterChanged")
    g_ScriptEvent:RemoveListener("ClubHouse_Notify_ClearStatusAll", self, "OnClubHouseNotifyClearStatusAll")
end

function LuaCHChatRoomHandsUpAlert:OnClubHouseMemberUpdate(member)
    self:UpdateDisplay()
end

function LuaCHChatRoomHandsUpAlert:OnClubHouseBroadcasterLeaveRoom()
    self:UpdateDisplay()
end

function LuaCHChatRoomHandsUpAlert:OnClubHouseAudienceLeaveRoom()
    self:UpdateDisplay()
end

function LuaCHChatRoomHandsUpAlert:OnClubHouseMemberCharacterChanged(member)
    self:UpdateDisplay()
end

function LuaCHChatRoomHandsUpAlert:OnClubHouseNotifyClearStatusAll(status)
    if status == EnumCHStatusType.HandsUp then
        self:UpdateDisplay()
    end
end

function LuaCHChatRoomHandsUpAlert:UpdateDisplay()
    self.m_Sprite.enabled = LuaClubHouseMgr:GetHandsUpCount()>0
end



