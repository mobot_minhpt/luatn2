local UILabel = import "UILabel"
local CUITexture=import "L10.UI.CUITexture"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local CRenderObject = import "L10.Engine.CRenderObject"
local LayerDefine = import "L10.Engine.LayerDefine"
local Quaternion = import "UnityEngine.Quaternion"
local Screen = import "UnityEngine.Screen"
local QnTableView=import "L10.UI.QnTableView"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local CEffectMgr = import "L10.Game.CEffectMgr"
local UITabBar = import "L10.UI.UITabBar"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local Vector3 = import "UnityEngine.Vector3"
local Color = import "UnityEngine.Color"
local CButton = import "L10.UI.CButton"
local CClientFurnitureMgr = import "L10.Game.CClientFurnitureMgr"
local CClientHouseMgr=import "L10.Game.CClientHouseMgr"
local CInputBoxMgr = import "L10.UI.CInputBoxMgr"
local Time = import "UnityEngine.Time"
local Camera = import "UnityEngine.Camera"
local GestureRecognizer = import "L10.Engine.GestureRecognizer"
local GestureType = import "L10.Engine.GestureType"
local DelegateFactory = import "DelegateFactory"
local MessageWndManager = import "L10.UI.MessageWndManager"
local EasyTouch = import "EasyTouch"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local CoreScene = import "L10.Engine.Scene.CoreScene"

CLuaZswTemplateEditWnd = class()

RegistClassMember(CLuaZswTemplateEditWnd, "m_ModelTextureLoader")
RegistClassMember(CLuaZswTemplateEditWnd, "m_ModelTexture")
RegistChildComponent(CLuaZswTemplateEditWnd, "m_TableView","TableView", QnTableView)
RegistChildComponent(CLuaZswTemplateEditWnd,"m_TabBar","TabBar",UITabBar)
RegistChildComponent(CLuaZswTemplateEditWnd,"m_FastFillBtn","FastFillBtn",GameObject)
RegistChildComponent(CLuaZswTemplateEditWnd,"m_SaveBtn","SaveBtn",GameObject)
RegistChildComponent(CLuaZswTemplateEditWnd,"m_PlaceBtn","PlaceBtn",CButton)
RegistChildComponent(CLuaZswTemplateEditWnd, "m_Designer","Designer", UILabel)
RegistChildComponent(CLuaZswTemplateEditWnd, "m_HouseGrade","HouseGrade", UILabel)
RegistChildComponent(CLuaZswTemplateEditWnd, "m_ZswCount","ZswCount", UILabel)
RegistChildComponent(CLuaZswTemplateEditWnd, "m_TemplateNameLabel","TemplateNameLabel", UILabel)
RegistChildComponent(CLuaZswTemplateEditWnd,"m_RenameBtn","RenameBtn",GameObject)
RegistChildComponent(CLuaZswTemplateEditWnd,"m_OverLook","OverLook",GameObject)
RegistChildComponent(CLuaZswTemplateEditWnd,"m_LeftBtn","LeftBtn",GameObject)
RegistChildComponent(CLuaZswTemplateEditWnd,"m_RightBtn","RightBtn",GameObject)
RegistChildComponent(CLuaZswTemplateEditWnd,"m_TipBtn","TipBtn",GameObject)
RegistChildComponent(CLuaZswTemplateEditWnd,"m_BestView","BestView",GameObject)
RegistChildComponent(CLuaZswTemplateEditWnd,"m_BestPreview","BestPreview",CUITexture)
RegistChildComponent(CLuaZswTemplateEditWnd,"m_FastClearBtn","FastClearBtn",GameObject)
RegistChildComponent(CLuaZswTemplateEditWnd,"m_ResetBtn","ResetBtn",GameObject)
RegistChildComponent(CLuaZswTemplateEditWnd,"m_HintLabel","HintLabel",GameObject)
RegistChildComponent(CLuaZswTemplateEditWnd,"m_CloseBtn","CloseButton",GameObject)
RegistChildComponent(CLuaZswTemplateEditWnd, "m_ListBtn","ListBtn", GameObject)

RegistClassMember(CLuaZswTemplateEditWnd, "m_Offset")
RegistClassMember(CLuaZswTemplateEditWnd, "m_TemplateIds")
RegistClassMember(CLuaZswTemplateEditWnd, "m_TemplateIds4Huamu")
RegistClassMember(CLuaZswTemplateEditWnd, "m_TemplateIds4Other")
RegistClassMember(CLuaZswTemplateEditWnd, "m_Scales")
RegistClassMember(CLuaZswTemplateEditWnd, "m_Rotations")
RegistClassMember(CLuaZswTemplateEditWnd, "m_SlotNum")
RegistClassMember(CLuaZswTemplateEditWnd, "m_Id2Consumed")
RegistClassMember(CLuaZswTemplateEditWnd, "m_RoIndex2ItemIndex")
RegistClassMember(CLuaZswTemplateEditWnd, "m_RoIndex2TabIndex")
RegistClassMember(CLuaZswTemplateEditWnd, "m_ModelRoot")
RegistClassMember(CLuaZswTemplateEditWnd, "m_IsOverLook")
RegistClassMember(CLuaZswTemplateEditWnd, "m_BestPreviewRotation")
RegistClassMember(CLuaZswTemplateEditWnd, "m_LeftPressed")
RegistClassMember(CLuaZswTemplateEditWnd, "m_RightPressed")
RegistClassMember(CLuaZswTemplateEditWnd, "m_StartPressTime")
RegistClassMember(CLuaZswTemplateEditWnd, "m_DeltaTime")
RegistClassMember(CLuaZswTemplateEditWnd, "m_DeltaRot")
RegistClassMember(CLuaZswTemplateEditWnd, "m_FilledCount")
RegistClassMember(CLuaZswTemplateEditWnd, "m_TotalCount")

function CLuaZswTemplateEditWnd:OnEnable()
    GestureRecognizer.Inst:AddNeedUIPinchWnd(CLuaUIResources.ZswTemplateEditWnd)
    g_ScriptEvent:AddListener("OnFillPartOfZswTemplate",self,"OnFillPartOfZswTemplate")

    self.onPinchInDelegate = DelegateFactory.Action_GenericGesture(function(gesture) self:OnPinchIn(gesture) end)
    self.onPinchOutDelegate = DelegateFactory.Action_GenericGesture(function(gesture) self:OnPinchOut(gesture) end)
    GestureRecognizer.AddGestureListener(GestureType.OnUIPinchIn, self.onPinchInDelegate)
    GestureRecognizer.AddGestureListener(GestureType.OnUIPinchOut, self.onPinchOutDelegate)
    g_ScriptEvent:AddListener("MouseScrollWheel",self,"OnMouseScrollWheel")
    g_ScriptEvent:AddListener("SetRepositoryFurnitureCount",self,"OnSetRepositoryFurnitureCount")
end

function CLuaZswTemplateEditWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnFillPartOfZswTemplate",self,"OnFillPartOfZswTemplate")

    GestureRecognizer.RemoveGestureListener(GestureType.OnUIPinchIn, self.onPinchInDelegate)
    GestureRecognizer.RemoveGestureListener(GestureType.OnUIPinchOut, self.onPinchOutDelegate)
    g_ScriptEvent:RemoveListener("MouseScrollWheel",self,"OnMouseScrollWheel")
    g_ScriptEvent:RemoveListener("SetRepositoryFurnitureCount",self,"OnSetRepositoryFurnitureCount")
end

function CLuaZswTemplateEditWnd:Awake()
    CLuaZswTemplateMgr.TemplateIds4Huamu = {}
    CLuaZswTemplateMgr.TemplateIds4Other = {}
    CLuaZswTemplateMgr.Id2Consumed = {}
    self.m_RoIndex2ItemIndex = {}
    self.m_RoIndex2TabIndex = {}

    self.Rotation = 0
    self.RotationY = 0
    self.m_IsOverLook = false
    self.m_StartPressTime = 0
    self.m_DeltaTime = 0.1
    self.m_DeltaRot = -10
    self.m_FilledCount = 0
    
    self.m_ModelTexture = self.transform:Find("Anchor/Preview"):GetComponent(typeof(CUITexture))

    self.m_ModelTextureLoader = LuaDefaultModelTextureLoader.Create(function (ro)
        self:LoadModel(ro)
    end)
    CommonDefs.AddOnDragListener(self.m_ModelTexture.gameObject, DelegateFactory.Action_GameObject_Vector2(function (go, delta)
		self:OnScreenDrag(go, delta)
    end), false)
    
    --用于预览的模板ro table
    CLuaZswTemplateMgr.CurTemplateRoTbl = {}
    self.m_SelectRoIndex = 0
    UIEventListener.Get(self.m_CloseBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("ZswTemplate_Save_Tip"), 
            DelegateFactory.Action(function ()            
                self:SaveData()
                CLuaZswTemplateMgr.OnSaveTemplateFillData()
                g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("模板填充的数据已成功保存到本地"))
                CUIManager.CloseUI(CLuaUIResources.ZswTemplateEditWnd)
            end), 
            DelegateFactory.Action(function ()            
                CUIManager.CloseUI(CLuaUIResources.ZswTemplateEditWnd)
            end), LocalString.GetString("保存"), LocalString.GetString("不保存"), false)
    end)
    UIEventListener.Get(self.m_SaveBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:SaveData()
        CLuaZswTemplateMgr.OnSaveTemplateFillData()
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("模板填充的数据已成功保存到本地"))
    end)

    UIEventListener.Get(self.m_PlaceBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnPlaceTemplateFillData()
    end)

    UIEventListener.Get(self.m_FastFillBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("ZswTemplate_FastFill_Comfirm"), DelegateFactory.Action(function ()            
            self:OnFastFillBtnClick()
        end), nil, nil, nil, false)
    end)

    UIEventListener.Get(self.m_FastClearBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("ZswTemplate_FastClear_Comfirm"), DelegateFactory.Action(function ()            
            self:OnFastClearBtnClick(true)
        end), nil, nil, nil, false)
    end)

    UIEventListener.Get(self.m_RenameBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        CInputBoxMgr.ShowInputBox(LocalString.GetString("请输入装饰物模板的新名称"), DelegateFactory.Action_string(function (val)
            if CWordFilterMgr.Inst:CheckName(val) then
                self:OnRenameTemplate(val)
                self.m_TemplateNameLabel.text = val
            else
                g_MessageMgr:ShowMessage("Name_Violation")
            end
          end), 14, true, nil, LocalString.GetString("最多14个字符(7个汉字)"))
    end)
    UIEventListener.Get(self.m_LeftBtn).onPress = DelegateFactory.BoolDelegate(function (go, isPressed)
        self.m_LeftPressed = isPressed
    end)
    UIEventListener.Get(self.m_RightBtn).onPress = DelegateFactory.BoolDelegate(function (go, isPressed)
        self.m_RightPressed = isPressed
    end)
    UIEventListener.Get(self.m_OverLook).onClick = DelegateFactory.VoidDelegate(function(go)
        self:ChangeToBestView()
    end)
    UIEventListener.Get(self.m_ResetBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnResetBtnClick()
    end)
    UIEventListener.Get(self.m_TipBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        g_MessageMgr:ShowMessage("ZswTemplate_Edit_Rule")
    end)

    self.m_PlaceBtn.Enabled = false

    local data = Zhuangshiwu_ZswTemplate.GetData(CLuaZswTemplateMgr.SelectEditTemplateId)
    if CLuaZswTemplateMgr.SelectEditTemplateId and data then
        self:InitModelData(data)
    end

    UIEventListener.Get(self.m_ListBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        CLuaZswTemplateMgr.CurPreviewTemplateId = CLuaZswTemplateMgr.SelectEditTemplateId
        CLuaZswTemplateComponentListWnd.s_IsDefault = true
        CUIManager.ShowUI(CLuaUIResources.ZswTemplateComponentListWnd)
    end)
end

function CLuaZswTemplateEditWnd:InitModelData(data)
    --加载本地保存数据
    CLuaZswTemplateMgr.LoadTemplateFillData()
    CLuaZswTemplateMgr.LoadTemplateRenameData()
    self.m_CurFillData = CLuaZswTemplateMgr.HouseZswTemplateFillData[tostring(CLuaZswTemplateMgr.SelectEditTemplateId)] 
    self.m_TemplateIds = data.DefaultComponents
    self.m_Offsets = data.Offset
    self.m_Scales = data.Scale
    self.m_Rotations = data.Rotation

    for i=0,self.m_TemplateIds.Length-1,1 do
        local tid = self.m_TemplateIds[i]
        local zswdata = Zhuangshiwu_Zhuangshiwu.GetData(tid)
        local t = {}
        local localData = nil
        local fillId
        local isEnough
        
        if self.m_CurFillData and next(self.m_CurFillData) then
            localData = self.m_CurFillData[tostring(i+1)]
            if localData and localData.isFilled then
                fillId = localData.id
                isEnough = CLuaZswTemplateMgr.CheckCanConsume(fillId)
                if isEnough then
                    CLuaZswTemplateMgr.AddConsumeById(fillId)
                    self.m_FilledCount = self.m_FilledCount + 1
                else
                    localData.isFilled=false
                end
            end
        end

        if zswdata.Type == EnumFurnitureType_lua.eHuamu then        
            t.index = i+1
            t.id = tid
            t.fillId = fillId
            t.isEnough = isEnough
            table.insert(CLuaZswTemplateMgr.TemplateIds4Huamu,t)
            local typeIndex = #CLuaZswTemplateMgr.TemplateIds4Huamu
            self.m_RoIndex2ItemIndex[i+1] = typeIndex
            self.m_RoIndex2TabIndex[i+1] = 0
        else
            t.index = i+1
            t.id = tid
            t.fillId = fillId
            t.isEnough = isEnough
            table.insert(CLuaZswTemplateMgr.TemplateIds4Other,t)
            local typeIndex = #CLuaZswTemplateMgr.TemplateIds4Other
            self.m_RoIndex2ItemIndex[i+1] = typeIndex
            self.m_RoIndex2TabIndex[i+1] = 1
        end
    end

    local previewPos = data.PreviewPos
    local previewScale = data.PreviewScale
    self.Rotation = data.PreviewDefaultRot
    if previewScale == 0 then previewScale = 1 end
    if previewPos then
        self.m_ModelTexture.mainTexture=CUIManager.CreateModelTexture("__zsw_template__", self.m_ModelTextureLoader,self.Rotation,previewPos[0],previewPos[1],previewPos[2],false,true,previewScale)
        self.m_PreviewPos = Vector3(previewPos[0],previewPos[1],previewPos[2])
    else
        self.m_ModelTexture.mainTexture=CUIManager.CreateModelTexture("__zsw_template__", self.m_ModelTextureLoader,self.Rotation,-1.65,-3.05,14.98,false,true,previewScale)
        self.m_PreviewPos = Vector3(-1.65,-3.05,14.98)
    end
    --打开俯视视角
    local overLook = data.OverLook
    self.m_IsOverLook = overLook == 1
    self.m_OverLook:SetActive(self.m_IsOverLook)

    self.localModelScale = previewScale
    self.m_MinPreviewZ = data.MinPosZ
    self.m_MaxPreviewZ = data.MaxPosZ
end

function CLuaZswTemplateEditWnd:Init()
    self.m_HintLabel:SetActive(false)
    if not CLuaZswTemplateMgr.SelectEditTemplateId then
        return 
    end

    local data = Zhuangshiwu_ZswTemplate.GetData(CLuaZswTemplateMgr.SelectEditTemplateId)
    if not data then
        return
    end

    self.m_TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            if CLuaZswTemplateMgr.CurSelectBarIndex == 0 then--self.m_TabBar.SelectedIndex
                return #CLuaZswTemplateMgr.TemplateIds4Huamu
            elseif CLuaZswTemplateMgr.CurSelectBarIndex == 1 then
                return #CLuaZswTemplateMgr.TemplateIds4Other
            end
        end,
        function(item,row)
            self:InitItem(item,row)
        end)

    self.m_TableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
        self.m_SelectRoIndex = row
        CLuaZswTemplateMgr.SelectRoIndex = row

        if CLuaZswTemplateMgr.CurTemplateRoTbl and CLuaZswTemplateMgr.CurTemplateRoTbl[row+1] then
            local roIndex
            if self.m_TabBar.SelectedIndex == 0 then
                CLuaZswTemplateMgr.CurSelectPartTid = CLuaZswTemplateMgr.TemplateIds4Huamu[row+1].id
                roIndex = CLuaZswTemplateMgr.TemplateIds4Huamu[row+1].index
            elseif self.m_TabBar.SelectedIndex == 1 then
                CLuaZswTemplateMgr.CurSelectPartTid = CLuaZswTemplateMgr.TemplateIds4Other[row+1].id
                roIndex = CLuaZswTemplateMgr.TemplateIds4Other[row+1].index
            end

            self.m_SelectRo = CLuaZswTemplateMgr.CurTemplateRoTbl[roIndex].ro
            local isFilled = CLuaZswTemplateMgr.CurTemplateRoTbl[roIndex].isFilled
            CLuaZswTemplateMgr.IsSelectItemFilled = CLuaZswTemplateMgr.CurTemplateRoTbl[roIndex].isFilled
            CLuaZswTemplateMgr.SelectItem = self.m_TableView:GetItemAtRow(row)
            CUIManager.ShowUI(CLuaUIResources.ZswTemplateFillingComfirmWnd)
            self.m_SelectRo:SetOutLinedEffect(true, Color.white)
        else

        end        
    end)

    self.m_TabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
        CLuaZswTemplateMgr.CurSelectBarIndex = index
        self.m_TableView:ReloadData(true, false)
        local itemCount = self.m_TableView.m_Grid.transform.childCount
        if itemCount == 0 then
            self.m_HintLabel:SetActive(true)
        else
            self.m_HintLabel:SetActive(false)
        end
    end)
    self.m_TabBar:ChangeTab(1, false)
    self.m_TabBar:ChangeTab(0, false)

    self:InitInfoView(data)
    self.m_TableView:ReloadData(false, false)
end

function CLuaZswTemplateEditWnd:InitInfoView(data)
    self.m_TotalCount = data.SlotNum
    self.m_Designer.text = data.Designer
    self.m_ZswCount.text = SafeStringFormat3(LocalString.GetString("%d(已装填%d)"),tonumber(self.m_TotalCount),self.m_FilledCount)
    self.m_HouseGrade.text = data.HouseGrade
    --默认名字
    local rename = CLuaZswTemplateMgr.HouseZswTemplateRenameData[tostring(data.FurnitureId)]
    if rename then
        self.m_TemplateNameLabel.text = rename
    else
        self.m_TemplateNameLabel.text = data.Name
    end
end

function CLuaZswTemplateEditWnd:OnRenameTemplate(name)
    local id = CLuaZswTemplateMgr.SelectEditTemplateId
    CLuaZswTemplateMgr.HouseZswTemplateRenameData[tostring(id)] = name
    CLuaZswTemplateMgr.OnSaveTemplateRenameData()
end

function CLuaZswTemplateEditWnd:InitItem(item, row)
    local icon = item.transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local mask = item.transform:Find("Mask").gameObject

    local zswId
    local isEnough
    if CLuaZswTemplateMgr.CurSelectBarIndex == 0 then--self.m_TabBar.SelectedIndex
        local huamu = CLuaZswTemplateMgr.TemplateIds4Huamu[row+1]
        zswId = huamu.id
        if huamu.fillId then
            zswId = huamu.fillId
        end
        isEnough = huamu.isEnough
    elseif CLuaZswTemplateMgr.CurSelectBarIndex == 1 then
        local other = CLuaZswTemplateMgr.TemplateIds4Other[row+1]
        zswId = other.id
        if other.fillId then
            zswId = other.fillId
        end
        isEnough = other.isEnough
    end

    local isMask = true
    local roIndex = CLuaZswTemplateMgr.GetRoIndexByItemIndex(row)
    if CLuaZswTemplateMgr.CurTemplateRoTbl[roIndex] and CLuaZswTemplateMgr.CurTemplateRoTbl[roIndex].isFilled then
        isMask = false
        local fillId = CLuaZswTemplateMgr.GetFillIdByRoIndex(roIndex)
        if fillId then
            zswId = fillId
        end
        local ro = CLuaZswTemplateMgr.CurTemplateRoTbl[roIndex].ro
        if not isEnough then
            isMask = true
            
            if not ro:ContainFx(CLuaZswTemplateMgr.MaterialFxId) then
                local fx = CEffectMgr.Inst:AddObjectFX(CLuaZswTemplateMgr.MaterialFxId, ro, 0, 1, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
                ro:AddFX(CLuaZswTemplateMgr.MaterialFxId, fx, -1)
            end
            CLuaZswTemplateMgr.CurTemplateRoTbl[roIndex].isEnough = false
        else
            ro:RemoveFX(CLuaZswTemplateMgr.MaterialFxId)
            CLuaZswTemplateMgr.CurTemplateRoTbl[roIndex].isEnough = true
        end

    end

    mask:SetActive(isMask)

    local data = Zhuangshiwu_Zhuangshiwu.GetData(zswId)
    if data then
        local itemId = data.ItemId
        local itemData = Item_Item.GetData(itemId)
        if itemData then
            local defaultIconName = itemData.Icon
            icon:LoadMaterial(defaultIconName)
        end
    end

end

function CLuaZswTemplateEditWnd:LoadModel(ro) 
    self.m_ModelRoot = ro.transform.parent
    for i=0,self.m_TemplateIds.Length-1,1 do
        -----
        local fillId
        if self.m_CurFillData and next(self.m_CurFillData) then
            local localData = self.m_CurFillData[tostring(i+1)]
            if localData and localData.isFilled then 
                fillId = localData.id
            end
        end
        -----
        local id = self.m_TemplateIds[i]
        if fillId then
            id = fillId
        end
        local data = Zhuangshiwu_Zhuangshiwu.GetData(id)
        local resName = data.ResName
        local resid = SafeStringFormat3("_%02d",data.ResId)
        local prefabname = "Assets/Res/Character/Jiayuan/" .. resName .. "/Prefab/" .. resName .. resid .. ".prefab"

        local ident = "templatepart_"..i
        local newRo = CRenderObject.CreateRenderObject(ro.gameObject, ident)
        newRo.Layer = LayerDefine.ModelForNGUI_3D
        newRo:LoadMain(prefabname)
        newRo.transform.localPosition = Vector3(self.m_Offsets[i*3]/100,self.m_Offsets[i*3+1]/100,self.m_Offsets[i*3+2]/100)
        newRo.Scale = self.m_Scales[i]
        newRo.transform.localRotation =Quaternion.Euler(Vector3(self.m_Rotations[i*3],self.m_Rotations[i*3+1],self.m_Rotations[i*3+2]))

        local info = {}
        info.ro = newRo
        info.isFilled = false
        info.id = id

        if fillId then
            info.isFilled = true
            --只要装填了一个装饰物 就可以开放摆放按钮
            if not self.m_PlaceBtn.Enabled then
                self.m_PlaceBtn.Enabled = true
            end
        end
        table.insert(CLuaZswTemplateMgr.CurTemplateRoTbl, info)--newRo

        local fx = CEffectMgr.Inst:AddObjectFX(CLuaZswTemplateMgr.MaterialFxId, newRo, 0, 1, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
        newRo:AddFX(CLuaZswTemplateMgr.MaterialFxId, fx, -1)
        --furniture.RO.AddFX("SelfFx", fx);
        if fillId then
            id = fillId
            local fdata = Zhuangshiwu_Zhuangshiwu.GetData(id)
            if fdata.FX > 0 and id ~= 9336 then---
                local selffx = CEffectMgr.Inst:AddObjectFX(fdata.FX, newRo, 0, 1, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
                newRo:AddFX("selfFx", selffx, -1)
            end
        end
        
    end
end

function CLuaZswTemplateEditWnd:OnScreenDrag(go, delta)    
    if EasyTouch.GetTouchCount() == 1 then
        local rotx = -delta.x / Screen.width * 360
        self.Rotation = self.Rotation + rotx
        CUIManager.SetModelRotation("__zsw_template__", self.Rotation)
    end
end

function CLuaZswTemplateEditWnd:SaveData()
    local data = {}
    data.templateId = CLuaZswTemplateMgr.SelectEditTemplateId
    for i,roInfo in pairs(CLuaZswTemplateMgr.CurTemplateRoTbl) do
        local t = {}
        t.id = roInfo.id
        t.isFilled = roInfo.isFilled
        t.index = i
        data[t.index] = t
    end
    if not CLuaZswTemplateMgr.HouseZswTemplateFillData then
        CLuaZswTemplateMgr.HouseZswTemplateFillData = {}
    end
    CLuaZswTemplateMgr.HouseZswTemplateFillData[tostring(CLuaZswTemplateMgr.SelectEditTemplateId)] = data
end

function CLuaZswTemplateEditWnd:OnCloseBtnClick()
    self:SaveData()
end

function CLuaZswTemplateEditWnd:OnPlaceTemplateFillData()
    MessageWndManager.ShowOKCancelMessage(LocalString.GetString("摆放后会清空当前模板装填数据，是否继续摆放？"), DelegateFactory.Action(function ()
        CLuaHouseMgr.ClickItemInfo = nil
        --检查有没有正在摆放的装饰物
        if CClientFurnitureMgr.Inst.CurFurniture and not CClientFurnitureMgr.Inst.FurnitureList:ContainsKey(CClientFurnitureMgr.Inst.CurFurniture.ID) then
            CClientFurnitureMgr.Inst.CurFurniture:Destroy()
            CClientFurnitureMgr.Inst.CurFurniture = nil
    
            CUIManager.CloseUI(CUIResources.HousePopupMenu)
        end
    
        local mubanTid = CLuaZswTemplateMgr.SelectEditTemplateId
        local data = Zhuangshiwu_Zhuangshiwu.GetData(mubanTid)
        if not data then return end

        if not CLuaClientFurnitureMgr.CheckLocation(data) then
            return 
        end
        local isInYard = CClientHouseMgr.Inst:GetCurSceneType() == EnumHouseSceneType_lua.eYard
        if not  isInYard then
            g_MessageMgr:ShowMessage("FURNITURE_CANNOT_PUT_IN_PLACE")
            return
        end       
        --检查数量限制        
        local canPut = true
        local belowTypeLimit = true
        local belowHouseLimit = true
        local fillConflic = false
        for id,consumed in pairs(CLuaZswTemplateMgr.Id2Consumed) do
            local numlimit = 0
            local curNum = CLuaClientFurnitureMgr.GetPlacedFurnitureCountByTemplateId(id)
            local zdata = Zhuangshiwu_Zhuangshiwu.GetData(id)
            if CLuaHouseWoodPileMgr.IsWoodPile(id) then
                numlimit = CLuaHouseWoodPileMgr.m_NumLimit
            else
                numlimit = CLuaClientFurnitureMgr.GetRepositoryNumLimit(CClientHouseMgr.Inst:GetCurHouseGrade(), zdata.Type, zdata.SubType)
            end
            if numlimit < curNum + consumed then
                belowTypeLimit = false
                canPut = false
            end
            local repositoryCount = CClientFurnitureMgr.Inst:GetRepositoryFurnitureCount(id)
            if consumed > repositoryCount then
                fillConflic = true
                canPut = false
            end
        end

        local maxNum = CLuaClientFurnitureMgr.GetMaxNum()

        local bSetLimitStatus = CClientHouseMgr.Inst and CClientHouseMgr.Inst.mConstructProp.FurnitureLimitStatus > 0 or false
        local curNum = CLuaClientFurnitureMgr.GetTotalPlacedFurnitureCount()
        if bSetLimitStatus then
            curNum = CLuaClientFurnitureMgr.GetPlacedFurnitureCount(2)
        end
        
        if curNum + self.m_FilledCount > maxNum then
            canPut = false
            belowHouseLimit = false
        end
        if not canPut then
            if fillConflic then
                self:OnFastClearBtnClick(false)
                g_MessageMgr:ShowMessage("ZswTemplate_Fill_Conflict")
                return
            end
            if not belowTypeLimit then
                g_MessageMgr:ShowMessage("Cant_Add_Furniture_Exceed_Limit")
            end
            if not belowHouseLimit then
                g_MessageMgr:ShowMessage("Cant_Add_Furniture_Exceed_Total")
            end
            return
        end

        CLuaHouseMgr.ClickItemInfo = {
            ChuanjiabaoItemId = nil,
            TemplateId = mubanTid,
        }
        g_ScriptEvent:BroadcastInLua("OnFurnitureMoveChoose_Lua", data.Name)
        CClientFurnitureMgr.Inst:SetContinuation(false)
        ----摆放模板
        local fid = mubanTid..mubanTid
        fid = tonumber(fid)
        CLuaClientFurnitureMgr.m_LatColorGridOverlap = false
        local templateFur = CLuaClientFurnitureMgr.CreateFurniture(mubanTid, fid, "")
        templateFur.ID = fid
        templateFur.Selected = true
        templateFur.Moving = true
        CClientFurnitureMgr.Inst.CurFurniture = templateFur
        self:CheckOverLap(templateFur)
        -------
        CUIManager.CloseUI(CLuaUIResources.ZswTemplateEditWnd)
    end), nil, nil, nil, false)
end

function CLuaZswTemplateEditWnd:CheckOverLap(fur)
    local mapWidth = CoreScene.MainCoreScene.WidthInGrid
    local mapHeight = CoreScene.MainCoreScene.HeightInGrid

    if fur:GetFurnitureType(fur.TemplateId) == EnumFurnitureType_lua.eZswTemplate then
        fur:UpdateChildrenPosition()
        local barrierComponentCountDic = {}

        for i=0,fur.Children.Length-1,1 do
            if fur.Children[i] > 0 then
                local fid = fur.Children[i]
                local childFur = CClientFurnitureMgr.Inst:GetFurniture(fid)
                if childFur then
                    local barrierCache = childFur.BarrierCache
                    local m_tdata = Zhuangshiwu_Zhuangshiwu.GetData(childFur.m_tid)
                    if m_tdata then
                        barrierCache = m_tdata.Barrier
                    end
                    if not barrierCache then 
                        return 
                    end

                    local parent = childFur.transform.parent
                    local pos = childFur.RO.Position
                    local gridWidth = childFur:GetGridWidth()
                    local gridHeight = childFur:GetGridHeight()
                    for index = 0,barrierCache.Length-1,1 do
                        local barrier = tonumber(barrierCache[index])
                        
                        if barrier ~= 0 then
                            local x = index % gridWidth - gridWidth / 2
                            local z = math.floor(index / gridWidth) - gridHeight / 2
                            x = math.floor(pos.x) + math.floor(x)
                            z = math.floor(pos.z) + math.floor(z)

                            local worldPos = parent:TransformPoint(Vector3(x, 0, z))
                            local tIndex = mapWidth * math.floor(worldPos.z) + math.floor(worldPos.x)

                            if  barrierComponentCountDic[tIndex] and barrierComponentCountDic[tIndex] > 0 then
                                fur.IsZswTemplateBarrierConclict = true
                                g_MessageMgr:ShowMessage("ZswTemplate_Overlap_CannotPut")
                                return
                            else
                                barrierComponentCountDic[tIndex] = 1
                            end
                        end
                        
                    end
                end
            end
        end
       
    end
    fur.IsZswTemplateBarrierConclict = false
end

function CLuaZswTemplateEditWnd:OnFastFillBtnClick()   
    local selectIndex
    local hasAnyCanFill = false
    for index,roInfo in ipairs(CLuaZswTemplateMgr.CurTemplateRoTbl) do
        local ro = roInfo.ro
        --没有被装填时 id就是默认id ro就是默认ro
        local fastFillId = roInfo.id
        local prefabname = ro.MainPath       
        local isFilled = roInfo.isFilled
        if not isFilled then
            local count = CClientFurnitureMgr.Inst:GetRepositoryFurnitureCount(fastFillId)
            local consumed = CLuaZswTemplateMgr.GetConsumedById(fastFillId)
            count = count - consumed
            if count > 0 then
                CLuaZswTemplateMgr.Id2Consumed[tonumber(fastFillId)] = consumed + 1
                roInfo.isFilled = true
                ro:RemoveFX(CLuaZswTemplateMgr.MaterialFxId)
                ro:LoadMain(prefabname,nil,true,false,false)
                local data = Zhuangshiwu_Zhuangshiwu.GetData(fastFillId)
                if data and data.FX > 0 and fastFillId ~= 9336 then
                    local selffx = CEffectMgr.Inst:AddObjectFX(data.FX, ro, 0, 1, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
                    ro:AddFX("selfFx", selffx, -1)
                end
                --只要装填了一个装饰物 就可以开放摆放按钮
                if not self.m_PlaceBtn.Enabled then
                    self.m_PlaceBtn.Enabled = true
                end
                hasAnyCanFill = true

                local itemIndex = self.m_RoIndex2ItemIndex[index]
                local tabIndex = self.m_RoIndex2TabIndex[index]
                if tabIndex == 0 then
                    CLuaZswTemplateMgr.TemplateIds4Huamu[itemIndex].fillId = fastFillId
                    CLuaZswTemplateMgr.TemplateIds4Huamu[itemIndex].isEnough = true
                elseif tabIndex == 1 then
                    CLuaZswTemplateMgr.TemplateIds4Other[itemIndex].fillId = fastFillId
                    CLuaZswTemplateMgr.TemplateIds4Other[itemIndex].isEnough = true
                end  
                self.m_FilledCount = self.m_FilledCount + 1             
            end
        end 
    end
    local tabIndex = self.m_TabBar.SelectedIndex
    if tabIndex == 0 then
        for i=1,0,-1 do
            CLuaZswTemplateMgr.CurSelectBarIndex = i
            self.m_TableView:ReloadData(true,false)
        end
    else
        for i=0,1,1 do
            CLuaZswTemplateMgr.CurSelectBarIndex = i
            self.m_TableView:ReloadData(true,false)
        end
    end  
    CLuaZswTemplateMgr.CurSelectBarIndex = self.m_TabBar.SelectedIndex
    self.m_ZswCount.text = SafeStringFormat3(LocalString.GetString("%d(已装填%d)"),tonumber(self.m_TotalCount),self.m_FilledCount)
    if hasAnyCanFill then
        g_MessageMgr:ShowMessage("ZswTemplate_Fill_Success")
    else
        g_MessageMgr:ShowMessage("ZswTemplate_Fill_Fail")
    end
end

--一键清空当前模板
function CLuaZswTemplateEditWnd:OnFastClearBtnClick(showMessage)
    --模板没有装填时不进行处理
    if not self.m_PlaceBtn.Enabled then
        return
    end
    for index,roInfo in ipairs(CLuaZswTemplateMgr.CurTemplateRoTbl) do
        local isFilled = roInfo.isFilled
        local ro = roInfo.ro
        local fillId = roInfo.id
        local defaultId = self.m_TemplateIds[index-1]
        if isFilled then
            roInfo.isFilled = false
            roInfo.id = defaultId
            local data = Zhuangshiwu_Zhuangshiwu.GetData(defaultId)
            local resName = data.ResName
            local resid = SafeStringFormat3("_%02d",data.ResId)
            local prefabname = "Assets/Res/Character/Jiayuan/" .. resName .. "/Prefab/" .. resName .. resid .. ".prefab"
            ro:LoadMain(prefabname,nil,true,false,false)
            local fx = CEffectMgr.Inst:AddObjectFX(CLuaZswTemplateMgr.MaterialFxId, ro, 0, 1, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
            ro:AddFX(CLuaZswTemplateMgr.MaterialFxId, fx, -1)
            ro:RemoveFX("selfFx")
            if self.m_PlaceBtn.Enabled then
                self.m_PlaceBtn.Enabled = false
            end
            local itemIndex = self.m_RoIndex2ItemIndex[index]
            local tabIndex = self.m_RoIndex2TabIndex[index]
            CLuaZswTemplateMgr.RevertConsumeById(fillId)

            if tabIndex == 0 then
                CLuaZswTemplateMgr.TemplateIds4Huamu[itemIndex].fillId = defaultId
            elseif tabIndex == 1 then
                CLuaZswTemplateMgr.TemplateIds4Other[itemIndex].fillId = defaultId
            end  
              
        end
    end
    local tabIndex = self.m_TabBar.SelectedIndex
    if tabIndex == 0 then
        for i=1,0,-1 do
            CLuaZswTemplateMgr.CurSelectBarIndex = i
            self.m_TableView:ReloadData(true,false)
        end
    else
        for i=0,1,1 do
            CLuaZswTemplateMgr.CurSelectBarIndex = i
            self.m_TableView:ReloadData(true,false)
        end
    end  
    CLuaZswTemplateMgr.CurSelectBarIndex = self.m_TabBar.SelectedIndex
    self.m_FilledCount = 0
    self.m_ZswCount.text = SafeStringFormat3(LocalString.GetString("%d(已装填%d)"),tonumber(self.m_TotalCount),self.m_FilledCount)

    if self.m_FilledCount==0 and showMessage then
        g_MessageMgr:ShowMessage("ZswTemplate_Xiexia_Success")
    end
end

function CLuaZswTemplateEditWnd:OnFillPartOfZswTemplate(fillId,isFill)
    local index =  CLuaZswTemplateMgr.SelectRoIndex + 1
    if isFill then
        local isEnough = true
        if self.m_TabBar.SelectedIndex == 0 then
            CLuaZswTemplateMgr.TemplateIds4Huamu[index].fillId = fillId
            CLuaZswTemplateMgr.TemplateIds4Huamu[index].isEnough = isEnough
        elseif self.m_TabBar.SelectedIndex == 1 then
            CLuaZswTemplateMgr.TemplateIds4Other[index].fillId = fillId
            CLuaZswTemplateMgr.TemplateIds4Other[index].isEnough = isEnough
        end

        --只要装填了一个装饰物 就可以开放摆放按钮
        if not self.m_PlaceBtn.Enabled then
            self.m_PlaceBtn.Enabled = true
        end
        self.m_FilledCount = self.m_FilledCount  + 1
    else
        --如果都没有装填 置灰摆放按钮
        local canPlace = false
        for i=1,#CLuaZswTemplateMgr.CurTemplateRoTbl do
            local roinfo = CLuaZswTemplateMgr.CurTemplateRoTbl[i]
            if roinfo.isFilled then
                canPlace = true
            end
        end
        if self.m_PlaceBtn.Enabled and not canPlace then
            self.m_PlaceBtn.Enabled = false
        end
        self.m_FilledCount  = self.m_FilledCount  - 1
        if self.m_TabBar.SelectedIndex == 0 then
            CLuaZswTemplateMgr.TemplateIds4Huamu[index].fillId = fillId
        elseif self.m_TabBar.SelectedIndex == 1 then
            CLuaZswTemplateMgr.TemplateIds4Other[index].fillId = fillId
        end
    end

    self.m_TableView:ReloadData(true, false)
    self.m_ZswCount.text = SafeStringFormat3(LocalString.GetString("%d(已装填%d)"),tonumber(self.m_TotalCount),self.m_FilledCount)
end

function CLuaZswTemplateEditWnd:Update()
    if self.m_LeftPressed and Time.realtimeSinceStartup - self.m_StartPressTime > self.m_DeltaTime then
        self.Rotation = self.Rotation + self.m_DeltaRot
        self.m_StartPressTime = Time.realtimeSinceStartup
        CUIManager.SetModelRotation("__zsw_template__", self.Rotation)
    elseif self.m_RightPressed and Time.realtimeSinceStartup - self.m_StartPressTime > self.m_DeltaTime then
        self.Rotation = self.Rotation - self.m_DeltaRot
        self.m_StartPressTime = Time.realtimeSinceStartup
        CUIManager.SetModelRotation("__zsw_template__", self.Rotation)
    end

    self:ClickThroughToClose()
end
--------
function CLuaZswTemplateEditWnd:OnPinchIn(gesture)
    local deltaPinch = gesture.deltaPinch
    local pinchScale = math.min(math.max(deltaPinch / 400, 0),1)
    self:OnPinch(-pinchScale)
end

function CLuaZswTemplateEditWnd:OnPinchOut(gesture)
    local deltaPinch = gesture.deltaPinch
    local pinchScale = math.min(math.max(deltaPinch / 400, 0),1)
    self:OnPinch(pinchScale)
end

function CLuaZswTemplateEditWnd:OnMouseScrollWheel()
    local v = Input.GetAxis("Mouse ScrollWheel")
    self:OnPinch(v)
end

function CLuaZswTemplateEditWnd:OnPinch(delta)
    local posZ = self.m_PreviewPos.z
    posZ = posZ - delta
    if (self.m_MaxPreviewZ and self.m_MinPreviewZ) and (posZ > self.m_MaxPreviewZ or posZ < self.m_MinPreviewZ) then
        return 
    end
    self.m_PreviewPos.z = posZ
    local pos = self.m_PreviewPos
    CUIManager.SetModelPosition("__zsw_template__", pos)
end
function CLuaZswTemplateEditWnd:SetModelPosAndScale(pos, scale)
    self.localModelScale = scale
    pos = Vector3(pos.x / scale, pos.y / scale, pos.z /scale)
    CUIManager.SetModelPosition("__zsw_template__", pos)
end

function CLuaZswTemplateEditWnd:ClickThroughToClose()
    if self.m_BestView.activeSelf and Input.GetMouseButtonDown(0) then    
        if (not CUICommonDef.IsOverUI or (UICamera.lastHit.collider and
           not CUICommonDef.IsChild(self.m_BestView.transform, UICamera.lastHit.collider.transform) and
            CUICommonDef.GetNearestPanelDepth(self.m_BestView.transform) > CUICommonDef.GetNearestPanelDepth(UICamera.lastHit.collider.transform))) then
            self.m_BestView:SetActive(false)
            self.m_BestPreview.mainTexture=nil
            CUIManager.DestroyModelTexture("__zsw_templatebest__")
            self.m_ModelRoot.localRotation = self.m_LastModleRot
            self.m_ModelRoot.localPosition = self.m_LastModlePosition
            self.m_Camera.enabled = true
        end
    end
end

function CLuaZswTemplateEditWnd:ChangeToBestView()
    local identiTf = CUIManager.instance.transform:Find("__zsw_template__")
    self.m_Camera = identiTf:GetComponent(typeof(Camera))
    self.m_Camera.enabled = false
    self.m_BestView:SetActive(true)

    local data = Zhuangshiwu_ZswTemplate.GetData(CLuaZswTemplateMgr.SelectEditTemplateId)
    local previewPos = data.PreviewPos
    local previewScale = data.PreviewScale
    local rotation = data.PreviewDefaultRot
    if previewScale == 0 then previewScale = 1 end

    local loader = LuaDefaultModelTextureLoader.Create(function (ro)
    end)

    if previewPos then
        self.m_BestPreview.mainTexture=CUIManager.CreateModelTexture("__zsw_templatebest__", loader,0,previewPos[0],previewPos[1],previewPos[2],false,true,previewScale)
    else
        self.m_BestPreview.mainTexture=CUIManager.CreateModelTexture("__zsw_templatebest__", loader,0,-1.65,-3.05,14.98,false,true,previewScale)
    end
    CUIManager.instance.transform:Find("__zsw_templatebest__").localPosition = identiTf.localPosition
    --打开俯视视角
    self.m_BestPreviewRotation = data.BestPreviewRotation
    self.m_LastModleRot = self.m_ModelRoot.localRotation
    self.m_LastModlePosition = self.m_ModelRoot.localPosition
    self.m_ModelRoot.localRotation = Quaternion.Euler(self.m_BestPreviewRotation[0], self.m_BestPreviewRotation[1], self.m_BestPreviewRotation[2])
    if data.BestPreviewPos then
        self.m_ModelRoot.localPosition = Vector3(data.BestPreviewPos[0],data.BestPreviewPos[1],data.BestPreviewPos[2])
    end
end

--还原到默认位置
function CLuaZswTemplateEditWnd:OnResetBtnClick()
    local data = Zhuangshiwu_ZswTemplate.GetData(CLuaZswTemplateMgr.SelectEditTemplateId)
    self.Rotation = data.PreviewDefaultRot
    CUIManager.SetModelRotation("__zsw_template__", self.Rotation)
end

function CLuaZswTemplateEditWnd:OnDestroy()
    self:SaveData()

    self.m_ModelTexture.mainTexture=nil
    CUIManager.DestroyModelTexture("__zsw_template__")
    CLuaZswTemplateMgr.SelectRoIndex = nil

    for i,roInfo in ipairs(CLuaZswTemplateMgr.CurTemplateRoTbl) do
        roInfo.ro:Destroy()
    end

    self.m_BestPreview.mainTexture=nil
    CUIManager.DestroyModelTexture("__zsw_templatebest__")
end

function CLuaZswTemplateEditWnd:OnSetRepositoryFurnitureCount(templateId,count)
    self:Init()
end
