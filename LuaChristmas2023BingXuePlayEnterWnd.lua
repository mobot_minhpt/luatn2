local Transform = import "UnityEngine.Transform"

local GameObject = import "UnityEngine.GameObject"

local UIScrollView = import "UIScrollView"

local UITable = import "UITable"

local UILabel = import "UILabel"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"

local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CCarnivalLotteryMgr = import "L10.Game.CCarnivalLotteryMgr"

LuaChristmas2023BingXuePlayEnterWnd = class()
RegistClassMember(LuaChristmas2023BingXuePlayEnterWnd, "m_BingxueItemId")  -- 礼盒ID
RegistClassMember(LuaChristmas2023BingXuePlayEnterWnd, "m_BingdiaoItemId") -- 冰雪家具兑换ID

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaChristmas2023BingXuePlayEnterWnd, "LevelLabel", "LevelLabel", UILabel)
RegistChildComponent(LuaChristmas2023BingXuePlayEnterWnd, "TextTable", "TextTable", UITable)
RegistChildComponent(LuaChristmas2023BingXuePlayEnterWnd, "ScrollView", "ScrollView", UIScrollView)
RegistChildComponent(LuaChristmas2023BingXuePlayEnterWnd, "EnterBtn", "EnterBtn", GameObject)
RegistChildComponent(LuaChristmas2023BingXuePlayEnterWnd, "Loading", "Loading", GameObject)
RegistChildComponent(LuaChristmas2023BingXuePlayEnterWnd, "ExitBtn", "ExitBtn", GameObject)
RegistChildComponent(LuaChristmas2023BingXuePlayEnterWnd, "TextTemplate", "TextTemplate", GameObject)
RegistChildComponent(LuaChristmas2023BingXuePlayEnterWnd, "RewardIcon", "RewardIcon", Transform)
RegistChildComponent(LuaChristmas2023BingXuePlayEnterWnd, "CountLabel", "CountLabel", UILabel)

--@endregion RegistChildComponent end

function LuaChristmas2023BingXuePlayEnterWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end
-- 初始化奖励信息
function LuaChristmas2023BingXuePlayEnterWnd:InitReward(hasreward)
    if hasreward > 0 then
        self.CountLabel.text = SafeStringFormat3(LocalString.GetString("今日奖励次数 [ff0000]%d[-]/1"), hasreward)
    else
        self.CountLabel.text = SafeStringFormat3(LocalString.GetString("今日奖励次数 %d/1"), hasreward)
    end

    self.m_BingxueItemId = Christmas2023_BingXuePlay.GetData().BingxueItemId
    local item = Item_Item.GetData(self.m_BingxueItemId)
    local icon = self.RewardIcon:GetChild(0):Find("IconTexture"):GetComponent(typeof(CUITexture))
    icon:LoadMaterial(item.Icon)
    UIEventListener.Get(icon.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        CItemInfoMgr.ShowLinkItemTemplateInfo(self.m_BingxueItemId, false, nil, AlignType.Default, 0, 0, 0, 0)
    end)
    self.RewardIcon:GetChild(0):Find("HasReward").gameObject:SetActive(hasreward > 0)

    self.m_BingdiaoItemId = Christmas2023_BingXuePlay.GetData().BingdiaoItemId
    item = Item_Item.GetData(self.m_BingdiaoItemId)
    icon = self.RewardIcon:GetChild(1):Find("IconTexture"):GetComponent(typeof(CUITexture))
    icon:LoadMaterial(item.Icon)
    UIEventListener.Get(icon.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        CItemInfoMgr.ShowLinkItemTemplateInfo(self.m_BingdiaoItemId, false, nil, AlignType.Default, 0, 0, 0, 0)
    end)
    self.RewardIcon:GetChild(1):Find("HasReward").gameObject:SetActive(hasreward > 0)
end
-- 初始化文本表
function LuaChristmas2023BingXuePlayEnterWnd:InitTextTable()
    self.TextTemplate:SetActive(false)
    Extensions.RemoveAllChildren(self.TextTable.transform)
    local textcontent = g_MessageMgr:FormatMessage("Christmas2023_BingXue_TIPS")
    local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(textcontent)
    if info then
        for i = 0, info.paragraphs.Count - 1 do
            local paragraphGo = NGUITools.AddChild(self.TextTable.gameObject, self.TextTemplate)
            paragraphGo:SetActive(true)
            paragraphGo.transform:Find("Label"):GetComponent(typeof(UILabel)).text = info.paragraphs[i]
        end
    end
    self.TextTable:Reposition()
    self.ScrollView:ResetPosition()
end
function LuaChristmas2023BingXuePlayEnterWnd:Init()
    self.LevelLabel.text = SafeStringFormat3(LocalString.GetString("≥%d级"), Christmas2023_BingXuePlay.GetData().LowestLevel)

    local hasreward = tonumber(CCarnivalLotteryMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eBingXueBingWorldPlayDailyAwardTimes))
    self:InitReward(hasreward)
    self:InitTextTable()

    self.EnterBtn:SetActive(true)
    self.Loading:SetActive(false) -- 匹配中按钮目前弃用
    UIEventListener.Get(self.EnterBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        Gac2Gas.BingXuePlay2023EnterGame() -- 申请进入副本
    end)
end

--@region UIEvent

--@endregion UIEvent

