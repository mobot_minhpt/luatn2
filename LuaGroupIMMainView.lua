local CGroupIMMainView = import "L10.UI.CGroupIMMainView"
local CGroupIMMgr = import "L10.Game.CGroupIMMgr"
local CChatHelper = import "L10.UI.CChatHelper"
CGroupIMMainView.m_Init_CS2LuaHook = function (this)
    this.contentView:Init(0, 18446744073709551615, nil, true)
    this.GroupIms = CGroupIMMgr.Inst:GetGroupIMs()
    this.tableView.m_DataSource = this
    this.tableView.OnSelectAtRow = MakeDelegateFromCSFunction(this.OnSelectAtRow, MakeGenericClass(Action1, Int32), this)
    this.tableView:ReloadData(true, false)
    if CChatHelper.s_ShowGroupChat then
        for i = 0, this.GroupIms.Count - 1 do
            if this.GroupIms[i].GroupIMID == CChatHelper.ShowGroupIMID then
                this.tableView:SetSelectRow(i, true)
                break
            end
        end
        CChatHelper.s_ShowGroupChat = false
    end
end