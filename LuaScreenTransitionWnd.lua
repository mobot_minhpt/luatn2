local CommonDefs = import "L10.Game.CommonDefs"
local UISprite = import "UISprite"
local LuaTweenUtils = import "LuaTweenUtils"
local NGUIText = import "NGUIText"
local CUIManager = import "L10.UI.CUIManager"

local Camera=import "UnityEngine.Camera"
local Rect = import "UnityEngine.Rect"
local Constants = import "L10.Game.Constants"
LuaScreenTransitionWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistClassMember(LuaScreenTransitionWnd, "m_Bg")

--@endregion RegistChildComponent end
RegistClassMember(LuaScreenTransitionWnd, "m_Camera")
function LuaScreenTransitionWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    self.gameObject:GetComponent(typeof(UIPanel)).IgnoreIphoneXMargin = true
    self.m_Camera = self.transform:Find("Camera"):GetComponent(typeof(Camera))
    --@endregion EventBind end
end

function LuaScreenTransitionWnd:Init()
    self.m_Bg = self.transform:Find("Camera/Panel/BlackBg").gameObject
    -- pc版本
	local bIsWinSocialWndOpened = false
	if CommonDefs.IsPCGameMode() then
        local CWinSocialWndMgr = import "L10.UI.CWinSocialWndMgr"
		bIsWinSocialWndOpened = CWinSocialWndMgr.Inst.m_IsWinSocialWndOpened
	end
    if not bIsWinSocialWndOpened then
		self:ShowWnd()
	else
		-- pc版本打开了外置聊天
	    self.m_Camera.rect = Rect(0, 0, 1 - Constants.WinSocialWndRatio, 1)
        self:ShowWnd()
	end
end

function LuaScreenTransitionWnd:ShowWnd()
    -- 颜色
    local sprite = self.m_Bg.gameObject:GetComponent(typeof(UISprite))
    sprite.color = NGUIText.ParseColor24(CLuaTaskMgr.ScreenColor,0)
    
    LuaTweenUtils.DOKill(self.m_Bg.transform, false)
    -- 渐入
    local fadeInTween = LuaTweenUtils.TweenAlpha(sprite,self.m_Bg.transform,0,1,CLuaTaskMgr.ScreenFadeIn)
    -- 渐出
    local fadeOutTween = LuaTweenUtils.TweenAlpha(sprite,self.m_Bg.transform,1,0,CLuaTaskMgr.ScreenFadeOut)
    LuaTweenUtils.SetDelay(fadeOutTween,CLuaTaskMgr.ScreenDuration + CLuaTaskMgr.ScreenFadeIn)

    -- 动画结束后关闭wnd
    LuaTweenUtils.OnComplete(fadeOutTween, function() self:CloseWnd() end)
end

function LuaScreenTransitionWnd:CloseWnd()
    CUIManager.CloseUI(CLuaUIResources.ScreenTransitionWnd)
end
--@region UIEvent

--@endregion UIEvent

