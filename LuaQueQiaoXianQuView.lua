local CButton = import "L10.UI.CButton"
local UILabel = import "UILabel"
local DelegateFactory  = import "DelegateFactory"

LuaQueQiaoXianQuView = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaQueQiaoXianQuView, "LeaveBtn", "LeaveBtn", CButton)
RegistChildComponent(LuaQueQiaoXianQuView, "RuleBtn", "RuleBtn", CButton)
RegistChildComponent(LuaQueQiaoXianQuView, "TaskName", "TaskName", UILabel)
RegistChildComponent(LuaQueQiaoXianQuView, "TaskStage", "TaskStage", UILabel)
RegistChildComponent(LuaQueQiaoXianQuView, "TaskDes", "TaskDes", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaQueQiaoXianQuView, "m_stage")
RegistClassMember(LuaQueQiaoXianQuView, "m_TaskDesTable")

function LuaQueQiaoXianQuView:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.LeaveBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLeaveBtnClick()
	end)


	
	UIEventListener.Get(self.RuleBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRuleBtnClick()
	end)


    --@endregion EventBind end
end

function LuaQueQiaoXianQuView:Init()
    LuaQiXi2021Mgr.InitData()
    local taskName = LocalString.GetString("鹊桥仙曲")
    self.m_TaskDesTable = {
        g_MessageMgr:FormatMessage("2021QiXi_QueQiaoXianQu_FirstMsg"),
        g_MessageMgr:FormatMessage("2021QiXi_QueQiaoXianQu_SecondMsg"),
        g_MessageMgr:FormatMessage("2021QiXi_QueQiaoXianQu_ThirdMsg")
    }
    self.TaskName.text = '[c][FFC800]' .. taskName .. '[-][c]'
    self.TaskStage.text = ""
    self.TaskDes.text = ""
    self:UpdateQueQiaoXianQuTaskViewInfo()
end
-- 同步服务器信息
function LuaQueQiaoXianQuView:UpdateQueQiaoXianQuTaskViewInfo()

    if not LuaQiXi2021Mgr.init then LuaQiXi2021Mgr.InitData() end

    if LuaQiXi2021Mgr.stage == 0 then
        self:ShowFirstStageView()
    elseif LuaQiXi2021Mgr.stage == 1 then
        self:ShowSecondStageView()
    elseif LuaQiXi2021Mgr.stage == 2 then
        self:ShowThirdStageView()
    end
end

function LuaQueQiaoXianQuView:ShowFirstStageView()
    self.TaskStage.text = LocalString.GetString("[一]")..LuaQiXi2021Mgr.QueQiaoXianQuStageTable[1]
    local QuestionString = SafeStringFormat3(LocalString.GetString("默契答题 (%d/%d)"),LuaQiXi2021Mgr.questionFinishCount,LuaQiXi2021Mgr.questionCount)
    if LuaQiXi2021Mgr.questionFinishCount == LuaQiXi2021Mgr.questionCount then
        QuestionString = SafeStringFormat3( "[c][00ff60]%s[-][c]\n",QuestionString)
    else
        QuestionString = SafeStringFormat3( "[c][ffffff]%s[-][c]\n",QuestionString)
    end
    local monsterString = ""
    for i=1,#LuaQiXi2021Mgr.monsterInfo do
        local tempString = ""
        local monsterId = LuaQiXi2021Mgr.monsterInfo[i].monsterId
        local totalCount = LuaQiXi2021Mgr.monsterInfo[i].totalCount
        local killCount = LuaQiXi2021Mgr.monsterInfo[i].killCount
        if totalCount ~= 1 then
            tempString = SafeStringFormat3( LocalString.GetString("击败%s (%d/%d)"),Monster_Monster.GetData(monsterId).Name,killCount,totalCount)
            if totalCount == killCount then
                tempString = SafeStringFormat3( "[c][00ff60]%s[-][c]\n",tempString )
            else
                tempString = SafeStringFormat3( "[c][ffffff]%s[-][c]\n",tempString )
            end
            monsterString = monsterString .. tempString
        end
    end
    self.TaskDes.text = self.m_TaskDesTable[1].."\n"..QuestionString..monsterString
end

function LuaQueQiaoXianQuView:ShowSecondStageView()
    self.TaskStage.text = LocalString.GetString("[二]")..LuaQiXi2021Mgr.QueQiaoXianQuStageTable[2]
    local result = 0 
    if LuaQiXi2021Mgr.queQiaoFished then result = 1 end
    local QueQiaoString = SafeStringFormat3( LocalString.GetString("同时搭建鹊桥 (%d/1)"),result)
    if result == 1 then
        QueQiaoString = SafeStringFormat3( "[c][00ff60]%s[-][c]\n",QueQiaoString )
    else
        QueQiaoString = SafeStringFormat3( "[c][ffffff]%s[-][c]\n",QueQiaoString )
    end
    self.TaskDes.text = self.m_TaskDesTable[2].."\n"..QueQiaoString
end

function LuaQueQiaoXianQuView:ShowThirdStageView()
    self.TaskStage.text = LocalString.GetString("[三]")..LuaQiXi2021Mgr.QueQiaoXianQuStageTable[3]
    local monsterString = ""
    for i=1,#LuaQiXi2021Mgr.monsterInfo do
        local tempString = ""
        local monsterId = LuaQiXi2021Mgr.monsterInfo[i].monsterId
        local totalCount = LuaQiXi2021Mgr.monsterInfo[i].totalCount
        local killCount = LuaQiXi2021Mgr.monsterInfo[i].killCount
        if totalCount == 1 then
            tempString = SafeStringFormat3( LocalString.GetString("击败%s (%d/%d)"),Monster_Monster.GetData(monsterId).Name,killCount,totalCount)
            if totalCount == killCount then
                tempString = SafeStringFormat3( "[c][00ff60]%s[-][c]\n",tempString )
            else
                tempString = SafeStringFormat3( "[c][ffffff]%s[-][c]\n",tempString )
            end
            monsterString = monsterString .. tempString
        end
    end
    self.TaskDes.text = self.m_TaskDesTable[3].."\n"..monsterString
end
--@region UIEvent

function LuaQueQiaoXianQuView:OnLeaveBtnClick()
    local OnOkBtn = function()
		Gac2Gas.RequestLeavePlay()
	end
	g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("2021QiXi_QueQiaoXianQu_Leave"), OnOkBtn, nil, nil, nil, false)
end

function LuaQueQiaoXianQuView:OnRuleBtnClick()
    g_MessageMgr:ShowMessage("2021QiXi_QUEQIAOXIANQU_RULE")
end

function LuaQueQiaoXianQuView:OnEnable()
    g_ScriptEvent:AddListener("SyncQueQiaoXianQuPlayInfo",self, "UpdateQueQiaoXianQuTaskViewInfo")
end

function LuaQueQiaoXianQuView:OnDisable()
    g_ScriptEvent:RemoveListener("SyncQueQiaoXianQuPlayInfo",self, "UpdateQueQiaoXianQuTaskViewInfo")
end


--@endregion UIEvent

