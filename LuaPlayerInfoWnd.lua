local Initialization_Init=import "L10.Game.Initialization_Init"
local Constants=import "L10.Game.Constants"
local Vector2 = import "UnityEngine.Vector2"
local EquipIntensify_Suit = import "L10.Game.EquipIntensify_Suit"
local CZuoQiMgr = import "L10.Game.CZuoQiMgr"
local CUIResources = import "L10.UI.CUIResources"
local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local Color = import "UnityEngine.Color"
local CMiddleNoticeMgr = import "L10.UI.CMiddleNoticeMgr"
local CHouseBuffShowMgr = import "L10.UI.CHouseBuffShowMgr"
local CExpressionMgr = import "L10.Game.CExpressionMgr"
local CEquipment = import "L10.Game.CEquipment"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CButton=import "L10.UI.CButton"
local CUITexture=import "L10.UI.CUITexture"
local CButtonClippable=import "L10.UI.CButtonClippable"
local UITable = import "UITable"
local UIGrid = import "UIGrid"
local CPopupMenuInfoMgrAlignType = import "L10.UI.CPopupMenuInfoMgr+AlignType"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"
local Profession=import "L10.Game.Profession"
local Screen = import "UnityEngine.Screen"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local EnumClass = import "L10.Game.EnumClass"
local EnumGender = import "L10.Game.EnumGender"

CLuaPlayerInfoWnd = class()
RegistClassMember(CLuaPlayerInfoWnd,"portrait")
RegistClassMember(CLuaPlayerInfoWnd,"profileFrame")
RegistClassMember(CLuaPlayerInfoWnd,"expressionTxt")
RegistClassMember(CLuaPlayerInfoWnd,"playerNameLabel")
RegistClassMember(CLuaPlayerInfoWnd,"playerIDLabel")
RegistClassMember(CLuaPlayerInfoWnd,"playerLiangHaoIDLabel")
RegistClassMember(CLuaPlayerInfoWnd,"levelLabel")
RegistClassMember(CLuaPlayerInfoWnd,"titleLabel")
RegistClassMember(CLuaPlayerInfoWnd,"xiuweiLabel")
RegistClassMember(CLuaPlayerInfoWnd,"xiulianLabel")
RegistClassMember(CLuaPlayerInfoWnd,"guildLabel")
RegistClassMember(CLuaPlayerInfoWnd,"zongmenLabel")
-- RegistClassMember(CLuaPlayerInfoWnd,"teamLabel")
RegistClassMember(CLuaPlayerInfoWnd,"scoreLabel")
RegistClassMember(CLuaPlayerInfoWnd,"houseLabel")
RegistClassMember(CLuaPlayerInfoWnd,"visitBtn")
RegistClassMember(CLuaPlayerInfoWnd,"appearanceBtn")
RegistClassMember(CLuaPlayerInfoWnd,"additionBtn")
RegistClassMember(CLuaPlayerInfoWnd,"talismanBtn")
RegistClassMember(CLuaPlayerInfoWnd,"houseBuffBtn")
RegistClassMember(CLuaPlayerInfoWnd,"addtionValueLabel")
RegistClassMember(CLuaPlayerInfoWnd,"equipInfoView")
RegistClassMember(CLuaPlayerInfoWnd,"mask")
RegistClassMember(CLuaPlayerInfoWnd,"bottomBtns")
RegistClassMember(CLuaPlayerInfoWnd,"lookAtSkillBtn")
RegistClassMember(CLuaPlayerInfoWnd,"lookAtFightingCapacityBtn")
RegistClassMember(CLuaPlayerInfoWnd,"lookAtLingshouBtn")
RegistClassMember(CLuaPlayerInfoWnd,"personalspaceBtn")
RegistClassMember(CLuaPlayerInfoWnd,"bottomBtns2")
RegistClassMember(CLuaPlayerInfoWnd,"lookAtSkillBtn2")
RegistClassMember(CLuaPlayerInfoWnd,"lookAtFightingCapacityBtn2")
RegistClassMember(CLuaPlayerInfoWnd,"lookAtLingshouBtn2")
RegistClassMember(CLuaPlayerInfoWnd,"intensifySuitId")
RegistClassMember(CLuaPlayerInfoWnd,"intensifySuitFxIndex")
RegistClassMember(CLuaPlayerInfoWnd,"m_LookAtMoreMenuList")
RegistClassMember(CLuaPlayerInfoWnd,"playerId")
RegistClassMember(CLuaPlayerInfoWnd,"playerName")
RegistClassMember(CLuaPlayerInfoWnd,"m_HouseId")
RegistClassMember(CLuaPlayerInfoWnd,"m_BabyButton1")
RegistClassMember(CLuaPlayerInfoWnd,"m_BabyButton2")
RegistClassMember(CLuaPlayerInfoWnd,"m_CityButton")
RegistClassMember(CLuaPlayerInfoWnd,"m_BtnSortGrid")
RegistClassMember(CLuaPlayerInfoWnd,"m_ZongMenLabelRoot")
RegistClassMember(CLuaPlayerInfoWnd,"m_BasicInfoItemsTable")
RegistClassMember(CLuaPlayerInfoWnd,"m_ModelTexture")
RegistClassMember(CLuaPlayerInfoWnd,"m_Ro")
RegistClassMember(CLuaPlayerInfoWnd,"m_CurRotation")
RegistClassMember(CLuaPlayerInfoWnd,"m_DefaultPos")
RegistClassMember(CLuaPlayerInfoWnd,"m_CurScale")
RegistClassMember(CLuaPlayerInfoWnd,"maxPreviewPos")
RegistClassMember(CLuaPlayerInfoWnd,"maxPreviewScale")

function CLuaPlayerInfoWnd:Awake()
    self.portrait = self.transform:Find("Anchor/Offset/BasicInfo/Portrait"):GetComponent(typeof(CUITexture))
    self.profileFrame = self.transform:Find("Anchor/Offset/BasicInfo/Portrait/ProfileFrame"):GetComponent(typeof(CUITexture))
    self.expressionTxt = self.transform:Find("Anchor/Offset/BasicInfo/Portrait/ExpressionTxt"):GetComponent(typeof(CUITexture))
    self.playerNameLabel = self.transform:Find("Anchor/Offset/BasicInfo/NameLabel"):GetComponent(typeof(UILabel))
    self.playerFlagSprite = self.transform:Find("Anchor/Offset/BasicInfo/CountryFlag"):GetComponent(typeof(UISprite))
    self.playerIDLabel = self.transform:Find("Anchor/Offset/BasicInfo/ID/ValueLabel"):GetComponent(typeof(CButtonClippable))
    self.playerLiangHaoIDLabel = self.transform:Find("Anchor/Offset/BasicInfo/ID/LiangHaoLabel"):GetComponent(typeof(CButtonClippable))
    self.levelLabel = self.transform:Find("Anchor/Offset/BasicInfo/Items/Level/ValueLabel"):GetComponent(typeof(UILabel))
    self.titleLabel = self.transform:Find("Anchor/Offset/BasicInfo/TitleLabel"):GetComponent(typeof(UILabel))
    self.xiuweiLabel = self.transform:Find("Anchor/Offset/BasicInfo/Items/Xiuwei/ValueLabel"):GetComponent(typeof(UILabel))
    self.xiulianLabel = self.transform:Find("Anchor/Offset/BasicInfo/Items/Xiulian/ValueLabel"):GetComponent(typeof(UILabel))
    self.guildLabel = self.transform:Find("Anchor/Offset/BasicInfo/Items/Guild/ValueLabel"):GetComponent(typeof(UILabel))
    self.m_ZongMenLabelRoot = self.transform:Find("Anchor/Offset/BasicInfo/Items/Sect").gameObject
    self.m_ZongMenLabelRoot:SetActive(false)
    self.zongmenLabel = self.transform:Find("Anchor/Offset/BasicInfo/Items/Sect/ValueLabel"):GetComponent(typeof(UILabel))
    self.m_BasicInfoItemsTable = self.transform:Find("Anchor/Offset/BasicInfo/Items"):GetComponent(typeof(UITable))
    -- self.teamLabel = self.transform:Find("Anchor/Offset/BasicInfo/Items/Team/ValueLabel"):GetComponent(typeof(UILabel))
    self.scoreLabel = self.transform:Find("Anchor/Offset/RightPart/EquipmentInfo/EquipmentScore/Label"):GetComponent(typeof(UILabel))
    self.houseLabel = self.transform:Find("Anchor/Offset/BasicInfo/Items/House/HouseName"):GetComponent(typeof(UILabel))
    self.visitBtn = self.transform:Find("Anchor/Offset/BasicInfo/Table/VisitBtn"):GetComponent(typeof(CButton))
    self.appearanceBtn = self.transform:Find("Anchor/Offset/RightPart/AppearanceBtn").gameObject
    self.additionBtn = self.transform:Find("Anchor/Offset/RightPart/AdditionBtn").gameObject
    self.talismanBtn = self.transform:Find("Anchor/Offset/RightPart/TalismanBtn").gameObject
    self.houseBuffBtn = self.transform:Find("Anchor/Offset/RightPart/HouseBuffBtn").gameObject
    self.addtionValueLabel = self.transform:Find("Anchor/Offset/RightPart/AdditionBtn/ValueLabel"):GetComponent(typeof(UILabel))

    self.equipInfoView = CLuaPlayerEquipInfoView:new()
    self.equipInfoView:Init(self.transform:Find("Anchor/Offset/RightPart/EquipmentInfo"))

    self.mask = self.transform:Find("Anchor/Offset/RightPart/Mask").gameObject
    self.bottomBtns = self.transform:Find("Anchor/Offset/RightPart/BottomBtns").gameObject
    self.lookAtSkillBtn = self.transform:Find("Anchor/Offset/RightPart/BottomBtns/LookAtSkillBtn"):GetComponent(typeof(CButton))
    self.lookAtFightingCapacityBtn = self.transform:Find("Anchor/Offset/RightPart/BottomBtns/LookAtFightingCapacityBtn"):GetComponent(typeof(CButton))
    self.lookAtLingshouBtn = self.transform:Find("Anchor/Offset/RightPart/BottomBtns/LookAtLingshouBtn"):GetComponent(typeof(CButton))
    self.personalspaceBtn = self.transform:Find("Anchor/Offset/RightPart/BottomBtns/PersonalSpaceBtn"):GetComponent(typeof(CButton))
    self.bottomBtns2 = self.transform:Find("Anchor/Offset/RightPart/BottomBtns2").gameObject
    self.lookAtSkillBtn2 = self.transform:Find("Anchor/Offset/RightPart/BottomBtns2/LookAtSkillBtn"):GetComponent(typeof(CButton))
    self.lookAtFightingCapacityBtn2 = self.transform:Find("Anchor/Offset/RightPart/BottomBtns2/LookAtFightingCapacityBtn"):GetComponent(typeof(CButton))
    self.lookAtLingshouBtn2 = self.transform:Find("Anchor/Offset/RightPart/BottomBtns2/LookAtLingshouBtn"):GetComponent(typeof(CButton))
    self.intensifySuitId = 0
	self.intensifySuitFxIndex = 0
    self.m_LookAtMoreMenuList = {}
    self.playerId = 0
    self.playerName = nil
    self.m_HouseId = nil
    self.maxPreviewPos = nil
    self.maxPreviewScale = nil

    self.m_SoulCoreButton = self.transform:Find("Anchor/Offset/BasicInfo/Table/SoulCoreButton").gameObject
    self.m_BabyButton = self.transform:Find("Anchor/Offset/BasicInfo/Table/BabyButton").gameObject
    self.m_BabyPanel = self.transform:Find("Anchor/Offset/BasicInfo/Table/BabyButton/BabysPanel").gameObject
    self.m_BabyButton1 = self.transform:Find("Anchor/Offset/BasicInfo/Table/BabyButton/BabysPanel/BabyButton1").gameObject
    self.m_BabyButton2 = self.transform:Find("Anchor/Offset/BasicInfo/Table/BabyButton/BabysPanel/BabyButton2").gameObject
    self.m_SoulCoreButton:SetActive(LuaZongMenMgr.m_EnableSoulCore)
    self.m_BabyButton:SetActive(false)
    self.m_BabyPanel:SetActive(false)

    self.m_CityButton = self.transform:Find("Anchor/Offset/BasicInfo/Table/CityButton").gameObject
    self.m_BtnSortGrid = self.transform:Find("Anchor/Offset/BasicInfo/Table"):GetComponent(typeof(UIGrid))
    self.m_ModelTexture = self.transform:Find("Anchor/Offset/Panel/ModelTexture"):GetComponent(typeof(UITexture))
    self.m_Ro = nil

    UIEventListener.Get(self.m_SoulCoreButton).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.ShowUI(CLuaUIResources.OtherSoulCoreWnd)
    end)
    self.playerLiangHaoIDLabel.OnLongPressed = DelegateFactory.Action(function ()
        self:OnLiangHaoLabelLongPress()
    end)
    self:InitModelTransformData()
    self:SetMaskLabel()
end

function CLuaPlayerInfoWnd:SetMaskLabel()
    local maskLabel = self.mask.transform:Find("Label"):GetComponent(typeof(UILabel))
    maskLabel.text = LocalString.GetString("该玩家不在线，无法查看其装备和详细外观信息")
end

function CLuaPlayerInfoWnd:OnLiangHaoLabelLongPress()
    local info = CPlayerInfoMgr.PlayerInfo
    local playerId = info.id
    local lianghaoId = info.profileInfo.LiangHaoId

    local selectAction = DelegateFactory.Action_int(function (index)
        if index == 0 then
            CUICommonDef.clipboardText = tostring(playerId)
            g_MessageMgr:ShowMessage("ShowPlayer_YuanShiId", playerId)
        else
            CUICommonDef.clipboardText = tostring(lianghaoId)
            g_MessageMgr:ShowMessage("Copy_Success_Tips")
        end
    end)

    local item1 = PopupMenuItemData(LocalString.GetString("查原始ID"), selectAction, false, nil, EnumPopupMenuItemStyle.Default)
    local item2 = PopupMenuItemData(LocalString.GetString("复制ID"), selectAction, false, nil, EnumPopupMenuItemStyle.Default)
    local tbl = {item1, item2}
    local array = Table2Array(tbl, MakeArrayClass(PopupMenuItemData))
    CPopupMenuInfoMgr.ShowPopupMenu(array, self.playerLiangHaoIDLabel.transform, CPopupMenuInfoMgrAlignType.Top)
end

-- Auto Generated!!
function CLuaPlayerInfoWnd:Start( )
    UIEventListener.Get(self.appearanceBtn).onClick = DelegateFactory.VoidDelegate(function(go) 
        if IsOpenNewPlayerAppearance() then
            CUIManager.ShowUI(CLuaUIResources.NewPlayerAppearanceInfoWnd)
        else
            CUIManager.ShowUI(CUIResources.PlayerAppearanceInfoWnd)
        end
    end)
    UIEventListener.Get(self.additionBtn).onClick = DelegateFactory.VoidDelegate(function(go) CLuaEquipmentSuitInfoMgr:ShowOtherPlayerSuit(self.intensifySuitId, self.intensifySuitFxIndex) end)
    UIEventListener.Get(self.talismanBtn).onClick = DelegateFactory.VoidDelegate(function(go) CUIManager.ShowUI(CUIResources.PlayerTalismanInfoWnd) end)

    if CPersonalSpaceMgr.OpenPersonalSpaceEntrance then
        self.bottomBtns:SetActive(true)
        self.bottomBtns2:SetActive(false)
        UIEventListener.Get(self.lookAtSkillBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) self:OnLookAtSkillBtnClick(go) end)
        UIEventListener.Get(self.lookAtLingshouBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) self:OnLookAtMoreBtnClick(go) end)
        UIEventListener.Get(self.lookAtFightingCapacityBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) self:OnLookAtFightingCapacityBtnClick(go) end)
        UIEventListener.Get(self.personalspaceBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) self:OnPersonalSpaceButtonClick(go) end)
    else
        self.bottomBtns:SetActive(false)
        self.bottomBtns2:SetActive(true)
        UIEventListener.Get(self.lookAtSkillBtn2.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) self:OnLookAtSkillBtnClick(go) end)
        UIEventListener.Get(self.lookAtLingshouBtn2.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) self:OnLookAtMoreBtnClick(go) end)
        UIEventListener.Get(self.lookAtFightingCapacityBtn2.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) self:OnLookAtFightingCapacityBtnClick(go) end)
    end
    UIEventListener.Get(self.visitBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) Gac2Gas.QueryHouseIntroByHouseId(self.m_HouseId) end)

    UIEventListener.Get(self.m_CityButton).onClick = DelegateFactory.VoidDelegate(function(go)  self:OnCityButtonClick() end)
end

function CLuaPlayerInfoWnd:OnCityButtonClick()
    local info = CPlayerInfoMgr.PlayerInfo
    if info and info.guildId > 0 then
        CLuaCityWarMgr.ShowCityInfoWnd(info.id, info.guildId)
    end
end

function CLuaPlayerInfoWnd:Init( )
    CUIManager.ShowUIByChangeLayer(CUIResources.PlayerInfoWnd, false, true) --可能会被子窗口隐藏，这里主动调整一下层次
    self:InitInfo()
    Gac2Gas.QueryPlayerInfo(CPlayerInfoMgr.PlayerId)
    self.m_LookAtMoreMenuList={}
    local menu = PopupMenuItemData(LocalString.GetString("查看灵兽"),DelegateFactory.Action_int(function(index) self:OnMoreMenuSelected(index) end),false,nil)
    table.insert( self.m_LookAtMoreMenuList, menu )
    if CZuoQiMgr.IsMapiEnable() then
        local menu2 = PopupMenuItemData(LocalString.GetString("查看马匹"),DelegateFactory.Action_int(function(index) self:OnMoreMenuSelected(index) end),false,nil)
        table.insert( self.m_LookAtMoreMenuList, menu2 )
    end
end

function CLuaPlayerInfoWnd:OnMoreMenuSelected(index)
    if index==0 then
        self:OnLookAtLingshouBtnClick(nil)
    elseif index==1 then
        CZuoQiMgr.Inst.m_OtherPlayerId = self.playerId
        CZuoQiMgr.Inst.m_OtherPlayerName = self.playerName
        CZuoQiMgr.Inst.m_OtherMapiOverviewList:Clear()
        Gac2Gas.QueryAllPlayerMapi(self.playerId)
    end
end

function CLuaPlayerInfoWnd:InitInfo( )
    self.playerNameLabel.text = nil
    self.playerIDLabel.content = nil
    self.playerLiangHaoIDLabel.content = nil
    self.playerIDLabel.gameObject:SetActive(true)
    self.playerLiangHaoIDLabel.gameObject:SetActive(false)
    self.levelLabel.text = nil
    self.titleLabel.text = nil
    self.xiuweiLabel.text = nil
    self.xiulianLabel.text = nil
    self.guildLabel.text = ""
    self.zongmenLabel.text = ""
    -- self.teamLabel.text = ""
    self.scoreLabel.text = ""
    self.houseLabel.text = ""
    self.playerFlagSprite.spriteName = ""
    self.visitBtn.Enabled = false
    self.portrait:Clear()
    self.profileFrame:Clear()
    self.expressionTxt:Clear()
    self.equipInfoView:InitEquipSlots()
    self.mask:SetActive(true)

    self.m_CityButton:SetActive(false)
    self.m_BtnSortGrid:Reposition()

    self.lookAtSkillBtn.Enabled = false
    self.lookAtFightingCapacityBtn.Enabled = false
    self.lookAtLingshouBtn.Enabled = false

    self:UpdateIntensifySuitLevel(0, 0)
end
function CLuaPlayerInfoWnd:UpdateIntensifySuitLevel(intensifySuitId, intensifySuitFxIndex)
    self.intensifySuitId = intensifySuitId
	self.intensifySuitFxIndex = intensifySuitFxIndex
    local suit = EquipIntensify_Suit.GetData(intensifySuitId)
    if suit == nil then
        self.addtionValueLabel.text = ""
    else
        self.addtionValueLabel.text = tostring(suit.IntensifyLevel)
        if suit.SuitTextColor and suit.SuitTextColor~="" then
            self.addtionValueLabel.color = NGUIText.ParseColor24(suit.SuitTextColor, 0)
        else
            self.addtionValueLabel.color = Color.white
        end
    end


    self:SetIntensifySuitButtonSprite(intensifySuitId, intensifySuitFxIndex)
end
function CLuaPlayerInfoWnd:SetIntensifySuitButtonSprite(suitId, fxIndex)
    local tex = self.additionBtn:GetComponent(typeof(CUITexture))
    if tex == nil then
        return
    end
    if suitId == 9 then
        tex:LoadMaterial("UI/Texture/Transparent/Material/packagewnd_icon_equipmentaddition_01.mat")
    elseif suitId == 10 then
        tex:LoadMaterial("UI/Texture/Transparent/Material/packagewnd_icon_equipmentaddition_02.mat")
    elseif suitId == 11 then
        if fxIndex == 0 then
            tex:LoadMaterial("UI/Texture/Transparent/Material/packagewnd_icon_equipmentaddition_03.mat")
        else
            tex:LoadMaterial("UI/Texture/Transparent/Material/packagewnd_icon_equipmentaddition_04.mat")
        end
    else
        tex:LoadMaterial("UI/Texture/Transparent/Material/packagewnd_icon_equipmentaddition.mat")
    end
end
function CLuaPlayerInfoWnd:OnEnable( )
    g_ScriptEvent:AddListener("OnQueryPlayerBasicInfoDone", self, "OnQueryPlayerBasicInfoDone")
    g_ScriptEvent:AddListener("OnQueryPlayerEquipmentInfoDone", self, "OnQueryPlayerEquipmentInfoDone")
    g_ScriptEvent:AddListener("OnQueryPlayerIntensifySuitInfoDoneLua", self, "OnQueryPlayerIntensifySuitInfoDoneLua")
    g_ScriptEvent:AddListener("OnQueryPlayerHousePrayWordsDone", self, "OnQueryPlayerHousePrayWordsDone")
    g_ScriptEvent:AddListener("OnSendOffLinePlayerHouse", self, "OnSendOffLinePlayerHouse")
    g_ScriptEvent:AddListener("QueryHouseBabyListDone", self, "OnQueryHouseBabyListDone")
    g_ScriptEvent:AddListener("SendQueryCityFlagResult", self, "SendQueryCityFlagResult")
    CLuaPlayerInfoWnd:RefreshUIModelPreviewListener()
end
function CLuaPlayerInfoWnd:OnDisable( )
    g_ScriptEvent:RemoveListener("OnQueryPlayerBasicInfoDone", self, "OnQueryPlayerBasicInfoDone")
    g_ScriptEvent:RemoveListener("OnQueryPlayerEquipmentInfoDone", self, "OnQueryPlayerEquipmentInfoDone")
    g_ScriptEvent:RemoveListener("OnQueryPlayerIntensifySuitInfoDoneLua", self, "OnQueryPlayerIntensifySuitInfoDoneLua")
    g_ScriptEvent:RemoveListener("OnQueryPlayerHousePrayWordsDone", self, "OnQueryPlayerHousePrayWordsDone")
    g_ScriptEvent:RemoveListener("OnSendOffLinePlayerHouse", self, "OnSendOffLinePlayerHouse")
    g_ScriptEvent:RemoveListener("QueryHouseBabyListDone", self, "OnQueryHouseBabyListDone")
    g_ScriptEvent:RemoveListener("SendQueryCityFlagResult", self, "SendQueryCityFlagResult")
    LuaUIModelPreviewMgr:RemoveModelPreviewScaleTransformListener("__PlayerInfoModel__")
end

function CLuaPlayerInfoWnd:OnDestroy()
    self.m_ModelTexture.mainTexture = nil
    CUIManager.DestroyModelTexture("__PlayerInfoModel__")
end

function CLuaPlayerInfoWnd:OnQueryHouseBabyListDone(playerId,t)
    local girlIcon = "UI/Texture/Transparent/Material/otherbaby_girl.mat"
    local boyIcon = "UI/Texture/Transparent/Material/otherbaby_boy.mat"
    if #t==1 then
        self.m_BabyButton:SetActive(true)

        local icon = FindChild(self.m_BabyButton.transform,"Texture"):GetComponent(typeof(CUITexture))
        icon.gameObject:SetActive(true)
        local gender = t[1].gender
        icon:LoadMaterial(gender==0 and boyIcon or girlIcon)

        local iconLeft = self.m_BabyButton.transform:Find("TextureLeft"):GetComponent(typeof(CUITexture))
        local iconRight = self.m_BabyButton.transform:Find("TextureRight"):GetComponent(typeof(CUITexture))
        iconLeft.gameObject:SetActive(false)
        iconRight.gameObject:SetActive(false)

        UIEventListener.Get(self.m_BabyButton).onClick=DelegateFactory.VoidDelegate(function(go)
            CLuaOtherBabyInfoWnd.m_PlayerId=playerId
            CLuaOtherBabyInfoWnd.m_BabyId=t[1].babyId
            CUIManager.ShowUI(CLuaUIResources.OtherBabyInfoWnd)
        end)
    elseif #t==2 then
        self.m_BabyButton:SetActive(true)

        local icon = self.m_BabyButton.transform:Find("Texture"):GetComponent(typeof(CUITexture))
        icon.gameObject:SetActive(false)

        local iconLeft = self.m_BabyButton.transform:Find("TextureLeft"):GetComponent(typeof(CUITexture))
        local iconRight = self.m_BabyButton.transform:Find("TextureRight"):GetComponent(typeof(CUITexture))
        iconLeft.gameObject:SetActive(true)
        iconRight.gameObject:SetActive(true)

        local icon = FindChild(self.m_BabyButton1.transform,"Texture"):GetComponent(typeof(CUITexture))
        local gender = t[1].gender
        icon:LoadMaterial(gender==0 and boyIcon or girlIcon)
        iconLeft:LoadMaterial(gender==0 and boyIcon or girlIcon)

        local icon = FindChild(self.m_BabyButton2.transform,"Texture"):GetComponent(typeof(CUITexture))
        local gender = t[2].gender
        icon:LoadMaterial(gender==0 and boyIcon or girlIcon)
        iconRight:LoadMaterial(gender==0 and boyIcon or girlIcon)

        UIEventListener.Get(self.m_BabyButton).onClick=DelegateFactory.VoidDelegate(function(go)
            self.m_BabyPanel:SetActive(true)
        end)
        UIEventListener.Get(self.m_BabyButton1).onClick=DelegateFactory.VoidDelegate(function(go)
            CLuaOtherBabyInfoWnd.m_PlayerId=playerId
            CLuaOtherBabyInfoWnd.m_BabyId=t[1].babyId
            CUIManager.ShowUI(CLuaUIResources.OtherBabyInfoWnd)
        end)
        UIEventListener.Get(self.m_BabyButton2).onClick=DelegateFactory.VoidDelegate(function(go)
            CLuaOtherBabyInfoWnd.m_PlayerId=playerId
            CLuaOtherBabyInfoWnd.m_BabyId=t[2].babyId
            CUIManager.ShowUI(CLuaUIResources.OtherBabyInfoWnd)
        end)
    else
        self.m_BabyButton:SetActive(false)
    end

    self.m_BtnSortGrid:Reposition()
end

function CLuaPlayerInfoWnd:OnSendOffLinePlayerHouse( args )
    local targetPlayerId, houseId, houseName = args[0],args[1],args[2]
    if self.playerId == targetPlayerId and houseId and houseId~="" then
        self.m_HouseId = houseId
        self.houseLabel.text = houseName
        self.visitBtn.Enabled = true
    end
end
function CLuaPlayerInfoWnd:OnQueryPlayerBasicInfoDone( args)
    local playerId, online = args[0],args[1]
    if CPlayerInfoMgr.PlayerId == playerId then
        local info = CPlayerInfoMgr.PlayerInfo
        
        self.playerId = playerId
        self.playerName = info.name
        local default
        if info.serverName and info.serverName~="" then
            default = System.String.Format("{0}[ACF9FF]({1})[-]", info.name, info.serverName)
        else
            default = info.name
        end

        local name = default
        self.playerNameLabel.text = name
        self.playerNameLabel.transform:Find('clazz'):GetComponent(typeof(UISprite)).spriteName = Profession.GetIconByNumber(info.cls)

        local lianghaoIcon = CUICommonDef.GetLiangHaoIcon(info.profileInfo, false, false)

        if LuaLiangHaoMgr.LiangHaoEnabled() and lianghaoIcon~=nil then
            self.playerIDLabel.gameObject:SetActive(false)
            self.playerLiangHaoIDLabel.gameObject:SetActive(true)
            self.playerLiangHaoIDLabel.content = tostring(info.profileInfo.LiangHaoId)
        else
            self.playerIDLabel.gameObject:SetActive(true)
            self.playerLiangHaoIDLabel.gameObject:SetActive(false)
            self.playerIDLabel.content = tostring(info.id)
        end
        self.titleLabel.text = info.title
        local flagSpriteName = LuaSEASdkMgr:GetFlagSpriteName(info.profileInfo.CountryFlag)
        self.playerFlagSprite.spriteName = flagSpriteName


        local extern
        if info.isInXianShenStatus then
            extern = Constants.ColorOfFeiSheng
        else
            extern = NGUIText.EncodeColor24(self.levelLabel.color)
        end
        self.levelLabel.text = System.String.Format("[c][{0}]{1}[-][/c]", extern, info.level)
        self.xiuweiLabel.text = NumberComplexToString(NumberTruncate(info.xiuwei, 1), typeof(Double), "F1")
        self.xiulianLabel.text = NumberComplexToString(NumberTruncate(info.xiulian, 1), typeof(Double), "F1")
        self.guildLabel.text = info.guildName
        self.m_ZongMenLabelRoot:SetActive(info.zongmenId ~= 0)
        --self.m_BasicInfoItemsTable.padding = Vector2(0,info.zongmenId ~= 0 and 1.12 or 5)
        self.m_BasicInfoItemsTable:Reposition()
        self.zongmenLabel.text = info.zongmenName
        -- local ref
        -- if info.teamName and info.teamName~="" then
        --     ref = info.teamName
        -- else
        --     ref = LocalString.GetString("未组队")
        -- end

        -- self.teamLabel.text = ref
        if not info.online then
            self.houseLabel.text = LocalString.GetString("未知")
            self.visitBtn.Enabled = false

            Gac2Gas.QueryOffLinePlayerHouse(playerId)
        elseif not info.houseId or info.houseId=="" or not info.houseName or info.houseName=="" then
            self.houseLabel.text = LocalString.GetString("无家园")
            self.visitBtn.Enabled = false
        else
            self.m_HouseId = info.houseId
            self.houseLabel.text = info.houseName
            self.visitBtn.Enabled = true

            Gac2Gas.QueryHouseBabyList(self.m_HouseId, playerId)
        end

        --查询是否存在城池
        if info and info.guildId > 0 then
            Gac2Gas.QueryCityFlag(info.id, info.guildId)
        end

        self.scoreLabel.text = tostring(info.equipscore)

        local resName =nil
        local initData=Initialization_Init.GetData(info.cls*100+info.gender)
        if initData then
            if info.expression<=0 then
                resName=initData.ResName
            else
                resName=SafeStringFormat3("%s_%02d",initData.ResName ,info.expression)
            end
        end
        self.portrait:LoadNPCPortrait(resName, false)

        if self.profileFrame ~= nil and CExpressionMgr.EnableProfileFrame then
            self.profileFrame:LoadMaterial(CUICommonDef.GetProfileFramePath(info.profileFrame))
        end
        self.expressionTxt:LoadMaterial(CUICommonDef.GetExpressionTxtPath(info.expressionTxt))
        self.mask:SetActive(not online)

        self.lookAtSkillBtn.Enabled = online
        self.lookAtFightingCapacityBtn.Enabled = online
        self.lookAtLingshouBtn.Enabled = online

        --灵核入口
        local soulCoreLv = (((CPlayerInfoMgr.PlayerInfo or {}).skillProp or {}).SoulCore or {}).Level or 0
        self.m_SoulCoreButton:SetActive(LuaZongMenMgr.m_EnableSoulCore and soulCoreLv > 0)
    end

    self:ShowPlayerModelView()
end

function CLuaPlayerInfoWnd:InitModelTransformData()
    self.m_DefaultPos = Vector3.zero
    self.m_DefaultPos.x = 0
    self.m_DefaultPos.y = -0.91
    self.m_DefaultPos.z = 5.61
    self.m_CurScale = 1 
    self.m_CurRotation = 180
end

function CLuaPlayerInfoWnd:ShowPlayerModelView()

    if not self.m_ModelTexture then return end
    self.m_ModelTextureLoader = LuaDefaultModelTextureLoader.Create(function (ro)
        self:LoadModel(ro)
    end)

    self.m_ModelTexture.mainTexture = CUIManager.CreateModelTexture("__PlayerInfoModel__", self.m_ModelTextureLoader,self.m_CurRotation,
    self.m_DefaultPos.x,self.m_DefaultPos.y,self.m_DefaultPos.z,false,true,self.m_CurScale,true,false)
    UIEventListener.Get(self.m_ModelTexture.gameObject).onDrag = DelegateFactory.VectorDelegate(function(go,delta) 
        local deltaVal = -delta.x / Screen.width * 360;
        self.m_CurRotation = self.m_CurRotation + deltaVal;
        CUIManager.SetModelRotation("__PlayerInfoModel__", self.m_CurRotation);
    end)
end

function CLuaPlayerInfoWnd:LoadModel(ro)
    local info = CPlayerInfoMgr.PlayerInfo
    self.m_Ro = ro
    if info then 
        CClientMainPlayer.LoadResource(ro, info.appearanceProp,true,1,0,false, 0,true,0,false,nil,false,false);

        self:SetModelTransformData(info.appearanceProp.Class,info.appearanceProp.Gender,ro)
    end
end

function CLuaPlayerInfoWnd:SetModelTransformData(class,gender,ro)
    if class == EnumClass.YingLing and gender == EnumGender.Monster then 
        self.maxPreviewScale = 2
    else
        self.maxPreviewScale = 4
    end
    if ro:IsAllFinished() then
        local Headtransform = ro:GetSlotTransform("Head",false)
        self.maxPreviewPos = Vector3.zero
        self.maxPreviewPos.y = -1 * Headtransform.localPosition.y
        self.maxPreviewPos.x = self.m_DefaultPos.x / self.maxPreviewScale
        self.maxPreviewPos.z = self.m_DefaultPos.z / self.maxPreviewScale
        self:RefreshUIModelPreviewListener()
    else
        ro:AddLoadAllFinishedListener(DelegateFactory.Action_CRenderObject(function(renderObject)
            local Headtransform = renderObject:GetSlotTransform("Head",false)
            self.maxPreviewPos = Vector3.zero
            self.maxPreviewPos.y = -1 * Headtransform.localPosition.y
            self.maxPreviewPos.x = self.m_DefaultPos.x / self.maxPreviewScale
            self.maxPreviewPos.z = self.m_DefaultPos.z / self.maxPreviewScale
            self:RefreshUIModelPreviewListener()
        end))
    end
end

function CLuaPlayerInfoWnd:RefreshUIModelPreviewListener()
    if self.maxPreviewPos then
        LuaUIModelPreviewMgr:AddModelPreviewScaleTransformListener("__PlayerInfoModel__",
        self.m_DefaultPos,self.m_CurScale,
        self.maxPreviewScale,self.maxPreviewPos,
        CUIResources.PlayerInfoWnd,true)
    end
end
function CLuaPlayerInfoWnd:OnQueryPlayerEquipmentInfoDone(args)
    local playerId, pos, equip = args[0],args[1],args[2]

    if CPlayerInfoMgr.PlayerInfo ~= nil and CPlayerInfoMgr.PlayerId == playerId then
        if CommonDefs.DictContains(CPlayerInfoMgr.PlayerInfo.pos2equipDict, typeof(UInt32), pos) then
            CommonDefs.DictSet(CPlayerInfoMgr.PlayerInfo.pos2equipDict, typeof(UInt32), pos, typeof(CEquipment), equip)
        else
            CommonDefs.DictAdd(CPlayerInfoMgr.PlayerInfo.pos2equipDict, typeof(UInt32), pos, typeof(CEquipment), equip)
        end
        self.equipInfoView:UpdateSlot(pos, equip, CPlayerInfoMgr.PlayerInfo.cls)
    end
end
function CLuaPlayerInfoWnd:OnQueryPlayerIntensifySuitInfoDoneLua(playerId, intensifySuitId, intensifySuitFxIndex, talismanIndex, talismanUserWordCls, isActiveXianJiaSuitUseWord)
    if CPlayerInfoMgr.PlayerId == playerId then
        CPlayerInfoMgr.PlayerInfo.talismanIndex = talismanIndex
        CPlayerInfoMgr.PlayerInfo.talismanUserWordCls = talismanUserWordCls
        CPlayerInfoMgr.PlayerInfo.isActiveXianJiaSuitUseWord = isActiveXianJiaSuitUseWord
        self:UpdateIntensifySuitLevel(intensifySuitId, intensifySuitFxIndex)
    end
end
function CLuaPlayerInfoWnd:OnQueryPlayerHousePrayWordsDone( playerId, prayWords)
    -- local playerId, prayWords=args[0],args[1]
    if CPlayerInfoMgr.PlayerId == playerId then
        if CPlayerInfoMgr.PlayerInfo == nil then
            return
        end
        CPlayerInfoMgr.PlayerInfo.prayWords = prayWords
        UIEventListener.Get(self.houseBuffBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnHouseBuffButtonClick(go) end)
    end
end
function CLuaPlayerInfoWnd:OnHouseBuffButtonClick( go)
    if CQuanMinPKMgr.Inst.m_IsQuanMinPKServer then
        g_MessageMgr:ShowMessage("QMPK_OTHER_PRAY")
        return
    end
    if CClientMainPlayer.Inst ~= nil then
        if CPlayerInfoMgr.PlayerInfo.prayWords.StartTime <= 0 then
            if CClientMainPlayer.Inst.Id == self.playerId then
                g_MessageMgr:ShowMessage("HAVE_NO_QIFU_SHOW_SELF")
            else
                g_MessageMgr:ShowMessage("HAVE_NO_QIFU_SHOW")
            end
        else
            CHouseBuffShowMgr.ShowBuffList(self.playerId, self.playerName, go.transform, false)
        end
    end
end
function CLuaPlayerInfoWnd:OnLookAtSkillBtnClick( go)
    if CPlayerInfoMgr.PlayerInfo == nil then
        CMiddleNoticeMgr.ShowMiddleNotice(LocalString.GetString("查询玩家技能信息失败"), 1)
        --TODO 加message
        return
    end
    CUIManager.ShowUI(CUIResources.PlayerSkillInfoWnd)
end

function CLuaPlayerInfoWnd:OnLookAtMoreBtnClick(go)
    local array=Table2Array(self.m_LookAtMoreMenuList,MakeArrayClass(PopupMenuItemData))
    CPopupMenuInfoMgr.ShowPopupMenu(array, go.transform, CPopupMenuInfoMgr.AlignType.Top)
end

function CLuaPlayerInfoWnd:OnLookAtFightingCapacityBtnClick(go)
    CLuaPlayerCapacityOtherWnd.playerId = self.playerId
    CLuaPlayerCapacityOtherWnd.name = self.playerName
    CUIManager.ShowUI(CUIResources.PlayerCapacityOtherWnd)
end

function CLuaPlayerInfoWnd:OnPersonalSpaceButtonClick(go)
    CPersonalSpaceMgr.Inst:OpenPersonalSpaceWnd(self.playerId)
end

function CLuaPlayerInfoWnd:OnLookAtLingshouBtnClick( go)
    CLuaLingShouOtherMgr.lingShouId = ""
    CLuaLingShouOtherMgr.playerId = self.playerId
    CLuaLingShouOtherMgr.playerName = self.playerName
    CLuaLingShouOtherMgr.requestType = LuaEnumOtherLingShouRequestType.PlayerLingShou
    CLuaLingShouOtherMgr.ClearLingShouData()
    Gac2Gas.QueryAllPlayerLingShou(CLuaLingShouOtherMgr.playerId, 0)
end

function CLuaPlayerInfoWnd:SendQueryCityFlagResult(guildId, hasCity)
    local info = CPlayerInfoMgr.PlayerInfo
    if hasCity and info and info.guildId > 0 and info.guildId == guildId then
        self.m_CityButton:SetActive(true)
        self.m_BtnSortGrid:Reposition()
    end
end
