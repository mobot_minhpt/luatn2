-- Auto Generated!!
local CJXYSComicWnd = import "L10.UI.CJXYSComicWnd"
local CJXYSMgr = import "L10.Game.CJXYSMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local ETickType = import "L10.Engine.ETickType"
local Extensions = import "Extensions"
local JiangXueYuanShuang_Comic = import "L10.Game.JiangXueYuanShuang_Comic"
local L10 = import "L10"
local Single = import "System.Single"
local SoundManager = import "SoundManager"
local Time = import "UnityEngine.Time"
local Vector3 = import "UnityEngine.Vector3"
CJXYSComicWnd.m_Init_CS2LuaHook = function (this) 

    this.wordsShown = Table2ArrayWithCount({
        false, 
        false, 
        false, 
        false, 
        false, 
        false
    }, 6, MakeArrayClass(System.Boolean))
    this.fxShown = Table2ArrayWithCount({
        false, 
        false, 
        false, 
        false, 
        false, 
        false
    }, 6, MakeArrayClass(System.Boolean))

    this.m_Period = CreateFromClass(MakeGenericClass(List, Single))
    this.m_ComicInfos = CreateFromClass(MakeGenericClass(List, JiangXueYuanShuang_Comic))
    this:HideAllWords()

    if this.cancel ~= nil then
        invoke(this.cancel)
    end

    if this.hideBG ~= nil then
        invoke(this.hideBG)
    end

    CommonDefs.ListAdd(this.m_Period, typeof(Single), 0)
    local comicId = CJXYSMgr.ComicID


    if JiangXueYuanShuang_Comic.Exists(comicId) then
        -- 初始化漫画展示的信息
        local comic = nil
        repeat
            comic = JiangXueYuanShuang_Comic.GetData(comicId)
            CommonDefs.ListAdd(this.m_ComicInfos, typeof(JiangXueYuanShuang_Comic), comic)
            this.m_TotalTime = this.m_TotalTime + comic.ShowTime
            CommonDefs.ListAdd(this.m_Period, typeof(Single), this.m_TotalTime)
            comicId = comic.Next
        until not (comic.Next ~= 0)

        -- 对漫画/文字的材质设置参数
        do
            local i = 0
            while i < CJXYSComicWnd.MAX_COMIC do
                if CJXYSComicWnd.JXYS_OPEN_COMIC then
                    local cTexture = this.m_Comics[i]
                    if cTexture ~= nil then
                        cTexture.mainTexture = nil
                        local screenPercent = (CJXYSComicWnd.DEFAULT_WIDTH / this.m_ComicInfos[i].ComicWidth * 1)
                        cTexture.material:SetFloat("_ScreenPercent", screenPercent)

                        if screenPercent < 1 then
                            cTexture.material:SetFloat("_ShowTime", 0)
                        else
                            cTexture.material:SetFloat("_ShowTime", this.m_Period[i + 1])
                        end

                        if this.m_ComicInfos[i].DisappearType <= this.m_DissolveTextures.Length then
                            cTexture.material:SetTexture("_DissolveTex", this.m_DissolveTextures[this.m_ComicInfos[i].DisappearType].mainTexture)
                            cTexture.material:SetFloat("_BurnSize", this.m_DissolveBurnSize[this.m_ComicInfos[i].DisappearType])
                            cTexture.material:SetFloat("_DisappearSpeed", this.m_DissolveDisappearSpeed[this.m_ComicInfos[i].DisappearType])
                            cTexture.material:SetFloat("_DelayTime", this.m_ComicInfos[i].DelayTime)
                        end

                        cTexture.material:SetFloat("_StartTime", Time.timeSinceLevelLoad)
                    end
                else
                    if this.m_Comics[i] ~= nil then
                        this.m_Comics[i].gameObject:SetActive(false)
                    end
                end

                if CJXYSComicWnd.JXYS_OPEN_TEXT then
                    local wTexture = this.m_WordTextures[i]
                    if wTexture ~= nil then
                        wTexture.mainTexture = nil
                        Extensions.SetLocalPositionX(wTexture.transform, this.m_ComicInfos[i].TextPosX)
                        Extensions.SetLocalPositionY(wTexture.transform, this.m_ComicInfos[i].TextPosY)
                        wTexture.width = this.m_ComicInfos[i].TextWidth
                        wTexture.height = this.m_ComicInfos[i].TextHeigt
                        wTexture.material:SetFloat("_SpeedX", this.m_ComicInfos[i].TextSpeed)
                    end
                else
                    if this.m_WordTextures[i] ~= nil then
                        this.m_WordTextures[i].gameObject:SetActive(false)
                    end
                end


                if CJXYSComicWnd.JXYS_OPEN_FX then
                    this.m_Fxs[i]:LoadFx(this.m_ComicInfos[i].Fx)
                    this.m_FxPanels[i].gameObject:SetActive(false)
                end
                i = i + 1
            end
        end
    end

    this.m_StartTime = Time.realtimeSinceStartup

    -- 关闭背景音乐
    SoundManager.Inst:StopBGMusic()
    this.m_SoundInst = SoundManager.Inst:PlayOneShot(this.JXYS_BG, Vector3.zero, nil, 0)

    this.hideBG = L10.Engine.CTickMgr.Register(DelegateFactory.Action(function () 
        this.blackBG.gameObject:SetActive(false)
    end), math.floor(this.m_TotalTime) * 1000 - 500, ETickType.Once)
    this.cancel = L10.Engine.CTickMgr.Register(DelegateFactory.Action(function () 
        this:Close()
        if this.m_SoundInst~=nil then
            SoundManager.Inst:StopSound(this.m_SoundInst)
            this.m_SoundInst = nil
        end
        SoundManager.Inst:StartBGMusic()
    end), math.floor(this.m_TotalTime) * 1000 + 300, ETickType.Once)
end
CJXYSComicWnd.m_ShowWord_CS2LuaHook = function (this, wordId) 
    if wordId < this.m_WordTextures.Length and this.m_WordTextures[wordId] ~= nil then
        if wordId > 0 then
            this.m_WordTextures[wordId - 1].gameObject:SetActive(false)
        end
        this.m_WordTextures[wordId].gameObject:SetActive(true)
        this.m_WordTextures[wordId].alpha = 1
        this.m_WordTextures[wordId].material:SetFloat("_StartTime", Time.timeSinceLevelLoad + CJXYSComicWnd.DEFAULT_DELAY_TEXT_DISAPPEAR)

        this.wordsShown[wordId] = true
    end
end
CJXYSComicWnd.m_OnDestroy_CS2LuaHook = function (this) 
    if this.hideBG ~= nil then
        invoke(this.hideBG)
        this.hideBG = nil
    end
    if this.cancel ~= nil then
        invoke(this.cancel)
        this.cancel = nil
    end
end
