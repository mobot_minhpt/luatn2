local CUITexture = import "L10.UI.CUITexture"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UITexture = import "UITexture"
local UILabel = import "UILabel"
local UIWidget = import "UIWidget"
local QnButton = import "L10.UI.QnButton"
local GameObject = import "UnityEngine.GameObject"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local BoxCollider = import "UnityEngine.BoxCollider"
local Physics = import "UnityEngine.Physics"
local CEffectMgr = import "L10.Game.CEffectMgr"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local CGamePlayMgr=import "L10.Game.CGamePlayMgr"

LuaZhanKuangTaskChangeFaceWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaZhanKuangTaskChangeFaceWnd, "CloseButton", "CloseButton", GameObject)
RegistChildComponent(LuaZhanKuangTaskChangeFaceWnd, "modelTexture", "modelTexture", UITexture)
RegistChildComponent(LuaZhanKuangTaskChangeFaceWnd, "PortraitOut", "PortraitOut", CUITexture)
RegistChildComponent(LuaZhanKuangTaskChangeFaceWnd, "PortraitIn", "PortraitIn", CUITexture)
RegistChildComponent(LuaZhanKuangTaskChangeFaceWnd, "EquipOpTipLab", "EquipOpTipLab", UILabel)
RegistChildComponent(LuaZhanKuangTaskChangeFaceWnd, "UnEquipTipLab", "UnEquipTipLab", UILabel)
RegistChildComponent(LuaZhanKuangTaskChangeFaceWnd, "UnEquipBtn", "UnEquipBtn", QnButton)
RegistChildComponent(LuaZhanKuangTaskChangeFaceWnd, "Portrait", "Portrait", BoxCollider)
RegistChildComponent(LuaZhanKuangTaskChangeFaceWnd, "Equip", "Equip", UIWidget)
RegistChildComponent(LuaZhanKuangTaskChangeFaceWnd, "UnEquip", "UnEquip", UIWidget)
RegistChildComponent(LuaZhanKuangTaskChangeFaceWnd, "EquipTipLab", "EquipTipLab", UILabel)
RegistChildComponent(LuaZhanKuangTaskChangeFaceWnd, "Mask", "Mask", CUITexture)
RegistChildComponent(LuaZhanKuangTaskChangeFaceWnd, "MaskFx", "MaskFx", UITexture)

--@endregion RegistChildComponent end
RegistClassMember(LuaZhanKuangTaskChangeFaceWnd, "m_Identifier")
RegistClassMember(LuaZhanKuangTaskChangeFaceWnd, "m_RO")
RegistClassMember(LuaZhanKuangTaskChangeFaceWnd, "m_DragOffset")
RegistClassMember(LuaZhanKuangTaskChangeFaceWnd, "m_Finished")
RegistClassMember(LuaZhanKuangTaskChangeFaceWnd, "m_Type")
RegistClassMember(LuaZhanKuangTaskChangeFaceWnd, "m_OutPrefabPath")
RegistClassMember(LuaZhanKuangTaskChangeFaceWnd, "m_InPrefabPath")
RegistClassMember(LuaZhanKuangTaskChangeFaceWnd, "m_Ticks")
RegistClassMember(LuaZhanKuangTaskChangeFaceWnd, "m_CurTipLab")
RegistClassMember(LuaZhanKuangTaskChangeFaceWnd, "m_CurWidget")
RegistClassMember(LuaZhanKuangTaskChangeFaceWnd, "m_FxId")
RegistClassMember(LuaZhanKuangTaskChangeFaceWnd, "m_StartAniName")
RegistClassMember(LuaZhanKuangTaskChangeFaceWnd, "m_EndAniName")
RegistClassMember(LuaZhanKuangTaskChangeFaceWnd, "m_ShowFxTime")
RegistClassMember(LuaZhanKuangTaskChangeFaceWnd, "m_ChangeResTime")
RegistClassMember(LuaZhanKuangTaskChangeFaceWnd, "m_FinishTime")
RegistClassMember(LuaZhanKuangTaskChangeFaceWnd, "m_Key")
RegistClassMember(LuaZhanKuangTaskChangeFaceWnd, "m_MaskBg")

function LuaZhanKuangTaskChangeFaceWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.m_MaskBg = self.Mask.transform:Find("MaskFx"):GetComponent(typeof(CUITexture))
    UIEventListener.Get(self.Mask.gameObject).onDragEnd = DelegateFactory.VoidDelegate(function(g)
        self:OnDragEnd(g)
    end)

    UIEventListener.Get(self.Mask.gameObject).onDragStart = DelegateFactory.VoidDelegate(function(g)
        self:OnDragStart(g)
    end)

    CommonDefs.AddOnDragListener(self.Mask.gameObject, DelegateFactory.Action_GameObject_Vector2(function (go, delta)
		self:OnScreenDrag(go, delta)
    end), false)

	UIEventListener.Get(self.CloseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCloseButtonClick()
	end)

    self.UnEquipBtn.OnClick = DelegateFactory.Action_QnButton(function (go)
        self:OnUnEquipBtnClick()
    end)

    self.m_Identifier = SafeStringFormat3("__%s__", tostring(self.modelTexture.gameObject:GetInstanceID()))
    self.m_Finished = false

    local settings          = ZhuJueJuQing_Setting.GetData()
    self.m_FxId             = settings.XuerenMaskExchangeFX
    local startExpressionId = settings.XuerenMaskExchangeOriginResExpression
    local endExpressionId   = settings.XuerenMaskExchangeFinalResExpression
    self.m_StartAniName     = Expression_Define.GetData(startExpressionId).AniName
    self.m_EndAniName       = Expression_Define.GetData(endExpressionId).AniName
    self.m_ShowFxTime       = settings.XuerenMaskExchangeFXStartTime
    self.m_ChangeResTime    = settings.XuerenMaskExchangeFinalResShowTime
    self.m_FinishTime       = settings.XuerenMaskExchangeSuccessTime
end

function LuaZhanKuangTaskChangeFaceWnd:Init()
    self.m_Key = LuaZhuJueJuQingMgr.ChangeFaceKey
    local data = ZhuJueJuQing_Mask.GetData(self.m_Key)
    self.m_Type = data.ExchangeType == 0 and 1 or 2

    self.Equip.alpha = 0
    self.UnEquip.alpha = 0
    self.PortraitOut.alpha = 1
    self.PortraitIn.alpha = 0
    self.m_CurTipLab = self.m_Type == 1 and self.EquipTipLab or self.UnEquipTipLab
    self.m_CurWidget = self.m_Type == 1 and self.Equip or self.UnEquip
    self.m_CurWidget.alpha = 1
    
    self.m_CurTipLab.text = data.Message
    self.m_OutPrefabPath = data.OriginRes
    self.m_InPrefabPath = data.FinalRes
    self.PortraitOut:LoadMaterial(data.OriginProtrait)
    self.PortraitIn:LoadMaterial(data.FinalProtrait)
    self.Mask:LoadMaterial(data.ExchangeIcon)
    self.m_MaskBg:LoadMaterial(data.LightRes)

    self:InitModel()
end


--@region UIEvent
function LuaZhanKuangTaskChangeFaceWnd:OnScreenDrag(go, delta)
    if self.m_Finished then
        return
    end

    local currentPos = UICamera.currentTouch.pos
    go.transform.position = CommonDefs.op_Addition_Vector3_Vector3(UICamera.currentCamera:ScreenToWorldPoint(Vector3(currentPos.x,currentPos.y,0)), self.m_DragOffset)
end

function LuaZhanKuangTaskChangeFaceWnd:OnDragStart(go)
    if self.m_Finished then
        return
    end

    local currentPos = UICamera.currentTouch.pos
    self.m_DragOffset = CommonDefs.op_Subtraction_Vector3_Vector3(go.transform.position, UICamera.currentCamera:ScreenToWorldPoint(Vector3(currentPos.x,currentPos.y,0)))
end

function LuaZhanKuangTaskChangeFaceWnd:OnDragEnd(go, word)
    if self.m_Finished then
        return
    end

    local currentPos = UICamera.currentTouch.pos
    local ray = UICamera.currentCamera:ScreenPointToRay(Vector3(currentPos.x,currentPos.y,0))
    local hits = Physics.RaycastAll(ray, 99999,  UICamera.currentCamera.cullingMask)

    for i=0,hits.Length-1 do
        local collider = hits[i].collider
        if collider == self.Portrait then
            self.m_Finished = true
            self:StartChangeFace()
            return 
        end
    end
    LuaUtils.SetLocalPosition(go.transform, 0, 0, 0)
end

function LuaZhanKuangTaskChangeFaceWnd:OnCloseButtonClick()
    CGamePlayMgr.Inst:LeavePlay()
end

function LuaZhanKuangTaskChangeFaceWnd:OnUnEquipBtnClick()
    if self.m_Finished then
        return
    end

    self.m_Finished = true
    self:StartChangeFace()
end
--@endregion UIEvent
function LuaZhanKuangTaskChangeFaceWnd:UIDoAni()
    LuaTweenUtils.TweenAlpha(self.m_CurWidget, 1, 0, 0.5)
    LuaTweenUtils.TweenAlpha(self.PortraitOut.texture, 1, 0, 0.5)
    LuaTweenUtils.TweenAlpha(self.PortraitIn.texture, 0, 1, 0.5)
end

function LuaZhanKuangTaskChangeFaceWnd:StartChangeFace()
    self:UIDoAni()
    self.m_RO:DoAni(self.m_StartAniName,false,0,1,0,true,1)

    self.m_Ticks = {}
    local tick = RegisterTickOnce(function()
        self:PlayFx()
    end, self.m_ShowFxTime * 1000)
    table.insert(self.m_Ticks, tick)

    local tick = RegisterTickOnce(function()
        self:ChangeModel()
    end, self.m_ChangeResTime * 1000)
    table.insert(self.m_Ticks, tick)


    local tick = RegisterTickOnce(function()
        self:Finished()
    end, self.m_FinishTime * 1000)
    table.insert(self.m_Ticks, tick)
end

function LuaZhanKuangTaskChangeFaceWnd:PlayFx()
    local fx = CEffectMgr.Inst:AddObjectFX(self.m_FxId, self.m_RO, 0,1,1,nil,false,EnumWarnFXType.None, Vector3.zero, Vector3.zero)
    self.m_RO:AddFX("SelfFx", fx)
end

function LuaZhanKuangTaskChangeFaceWnd:ChangeModel()
    self.m_RO:LoadMain(self.m_InPrefabPath)
    self.m_RO:AddLoadAllFinishedListener(DelegateFactory.Action_CRenderObject(function(ro)
		self.m_RO:DoAni(self.m_EndAniName,false,0,1,0,true,1)
	end))
end

function LuaZhanKuangTaskChangeFaceWnd:Finished()
    Gac2Gas.OnFinishJuQingMaskPlay(self.m_Key)
    CUIManager.CloseUI(CLuaUIResources.ZhanKuangTaskChangeFaceWnd)
end

function LuaZhanKuangTaskChangeFaceWnd:InitModel()
    self.modelTexture.mainTexture = CUIManager.CreateModelTexture(self.m_Identifier, LuaDefaultModelTextureLoader.Create(function (ro)
        self:CreateModel(ro)
    end), 180, -0.04, -1.13, 5, false, true, 1.0, true, false)
end

function LuaZhanKuangTaskChangeFaceWnd:CreateModel(ro)
    self.m_RO = ro
    self.m_RO:LoadMain(self.m_OutPrefabPath)
end

function LuaZhanKuangTaskChangeFaceWnd:OnDestroy()
    self.modelTexture.mainTexture = nil
    CUIManager.DestroyModelTexture(self.m_Identifier)
    if self.m_Ticks then
        for i = 1, #self.m_Ticks do
            UnRegisterTick(self.m_Ticks[i])
        end
    end
end

