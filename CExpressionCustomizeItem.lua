-- Auto Generated!!
local Boolean = import "System.Boolean"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CExpressionCustomizeItem = import "L10.UI.CExpressionCustomizeItem"
local CExpressionMgr = import "L10.Game.CExpressionMgr"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local ExpressionHead_ExpressionTxt = import "L10.Game.ExpressionHead_ExpressionTxt"
local ExpressionHead_ProfileFrame = import "L10.Game.ExpressionHead_ProfileFrame"
local ExpressionHead_Sticker = import "L10.Game.ExpressionHead_Sticker"
local Int32 = import "System.Int32"
CExpressionCustomizeItem.m_Init_CS2LuaHook = function (this, expressionId, isTxt)
    this.mExpressionId = expressionId
    this.mIsTxt = isTxt

    local resName = ""
    if isTxt == 1 then
        local data = ExpressionHead_ExpressionTxt.GetData(expressionId)
        resName = data.ResName
        this.needRenqi = data.UnlockRenQi
        this.icon.texture.width = CExpressionCustomizeItem.Width
        this.icon.texture.height = CExpressionCustomizeItem.Height
    elseif isTxt == 0 then
        local data = ExpressionHead_Sticker.GetData(expressionId)
        resName = data.ResName
        this.needRenqi = data.UnlockRenQi
        this.icon.texture.width = CExpressionCustomizeItem.TextureSize
        this.icon.texture.height = CExpressionCustomizeItem.TextureSize
    elseif isTxt == 2 then
        local data = ExpressionHead_ProfileFrame.GetData(expressionId)
        resName = data.ResName
        this.needRenqi = - 100
        this.icon.texture.width = CExpressionCustomizeItem.TextureSize
        this.icon.texture.height = CExpressionCustomizeItem.TextureSize
    end

    this.frameLock:SetActive(false)
    if isTxt == 2 then
        this.lockGo:SetActive(false)
        if CExpressionMgr.Inst:HasGetProfileFrame(expressionId) ~= 0 then
            this.icon:LoadMaterial(resName,false)
            this.icon.texture.alpha = 1
            this.frameLock:SetActive(false)
        else
            this.icon:LoadMaterial(resName, true)
            this.icon.texture.alpha = 0.5
            this.frameLock:SetActive(true)
            --renqiLabel.text = needRenqi.ToString();
        end
        this.renqiLabel.gameObject:SetActive(true)
        this.renqiLabel.text = ""
    else
        if CExpressionMgr.Inst.Renqi >= this.needRenqi and this.needRenqi ~= -1 then
            this.icon:LoadMaterial(resName,false)
            if isTxt == 1 then
                if not CExpressionMgr.Inst:HasGetExpressionTxt(expressionId) then
                    this.icon.texture.alpha = 0.5
                    this.lockGo:SetActive(true)
                    Gac2Gas.UnlockExpressionTxt(expressionId)
                else
                    this.icon.texture.alpha = 1
                    this.lockGo:SetActive(false)
                end
            elseif isTxt == 0 then
                if not CExpressionMgr.Inst:HasGetSticker(expressionId) then
                    this.icon.texture.alpha = 0.5
                    local data = ExpressionHead_Sticker.GetData(expressionId)
                    this.lockGo:SetActive(true)
                    if data.UnlockTaskId > 0 then
                        if CClientMainPlayer.Inst and CClientMainPlayer.Inst.TaskProp:IsMainPlayerTaskFinished(data.UnlockTaskId) then
                            Gac2Gas.UnlockSticker(expressionId) 
                        else
                            this.renqiLabel.gameObject:SetActive(false)
                        end
                    else
                        Gac2Gas.UnlockSticker(expressionId)
                    end
                else
                    this.icon.texture.alpha = 1
                    this.lockGo:SetActive(false)
                end
            end
        else
            if this.mIsTxt == 1 then
                --已经获得了 以服务器结果为准
                if CExpressionMgr.Inst:HasGetExpressionTxt(expressionId) then
                    this.icon:LoadMaterial(resName,false)
                    this.icon.texture.alpha = 1
                    this.lockGo:SetActive(false)
                else
                    this.icon:LoadMaterial(resName, true)
                    this.icon.texture.alpha = 0.5
                    this.lockGo:SetActive(true)
                    this.renqiLabel.gameObject:SetActive(true)
                    this.renqiLabel.text = tostring(this.needRenqi)
                end
            elseif this.mIsTxt == 0 then
                if CExpressionMgr.Inst:HasGetSticker(expressionId) then
                    this.icon:LoadMaterial(resName,false)
                    this.icon.texture.alpha = 1
                    this.lockGo:SetActive(false)
                else
                    this.icon:LoadMaterial(resName, true)
                    this.icon.texture.alpha = 0.5
                    this.lockGo:SetActive(true)
                    this.renqiLabel.gameObject:SetActive(true)
                    this.renqiLabel.text = tostring(this.needRenqi)
                end
            end
        end
    end

    this:OnUpdatePlayerExpressionTxt(CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.ExpressionTxt or 0)
end
CExpressionCustomizeItem.m_OnEnable_CS2LuaHook = function (this)
    EventManager.AddListenerInternal(EnumEventType.UpdateMainPlayerExpressionTxt, MakeDelegateFromCSFunction(this.OnUpdatePlayerExpressionTxt, MakeGenericClass(Action1, Int32), this))
    EventManager.AddListenerInternal(EnumEventType.UnlockMainPlayerExpressionTxt, MakeDelegateFromCSFunction(this.OnUnlockPlayerExpressionTxt, MakeGenericClass(Action2, Int32, Boolean), this))
    EventManager.AddListenerInternal(EnumEventType.UnlockMainPlayerSticker, MakeDelegateFromCSFunction(this.OnUnlockPlayerSticker, MakeGenericClass(Action2, Int32, Boolean), this))
    EventManager.AddListenerInternal(EnumEventType.UpdateMainPlayerRenqi, MakeDelegateFromCSFunction(this.OnUpdateRenqi, Action0, this))
end
CExpressionCustomizeItem.m_OnDisable_CS2LuaHook = function (this)
    EventManager.RemoveListenerInternal(EnumEventType.UpdateMainPlayerExpressionTxt, MakeDelegateFromCSFunction(this.OnUpdatePlayerExpressionTxt, MakeGenericClass(Action1, Int32), this))
    EventManager.RemoveListenerInternal(EnumEventType.UnlockMainPlayerExpressionTxt, MakeDelegateFromCSFunction(this.OnUnlockPlayerExpressionTxt, MakeGenericClass(Action2, Int32, Boolean), this))
    EventManager.RemoveListenerInternal(EnumEventType.UnlockMainPlayerSticker, MakeDelegateFromCSFunction(this.OnUnlockPlayerSticker, MakeGenericClass(Action2, Int32, Boolean), this))
    EventManager.RemoveListenerInternal(EnumEventType.UpdateMainPlayerRenqi, MakeDelegateFromCSFunction(this.OnUpdateRenqi, Action0, this))
end
CExpressionCustomizeItem.m_OnUpdatePlayerExpressionTxt_CS2LuaHook = function (this, expressionTxt)
    if CClientMainPlayer.Inst == nil then
        return
    end
    if this.mIsTxt == 1 then
        if CClientMainPlayer.Inst.BasicProp.ExpressionTxt == this.mExpressionId then
            this.currentMark:SetActive(true)
        else
            this.currentMark:SetActive(false)
        end
    elseif this.mIsTxt == 0 then
        if CClientMainPlayer.Inst.BasicProp.Sticker == this.mExpressionId then
            this.currentMark:SetActive(true)
        else
            this.currentMark:SetActive(false)
        end
    elseif this.mIsTxt == 2 then
        if CClientMainPlayer.Inst.BasicProp.ProfileInfo.Frame == this.mExpressionId then
            this.currentMark:SetActive(true)
        else
            this.currentMark:SetActive(false)
        end
    end
end
CExpressionCustomizeItem.m_OnUnlockPlayerExpressionTxt_CS2LuaHook = function (this, expressionTxtId, unlock)
    if expressionTxtId == this.mExpressionId then
        if this.mIsTxt == 1 then
            this:Init(this.mExpressionId, 1)
        end
    end
end
CExpressionCustomizeItem.m_OnUnlockPlayerSticker_CS2LuaHook = function (this, stickerId, unlock)
    if stickerId == this.mExpressionId then
        if this.mIsTxt == 0 then
            this:Init(this.mExpressionId, 0)
        end
    end
end
CExpressionCustomizeItem.m_OnUnlockPlayerProfileFrame_CS2LuaHook = function (this, profileFrameId, unlock)
    if profileFrameId == this.mExpressionId then
        if this.mIsTxt == 2 then
            this:Init(this.mExpressionId, 2)
        end
    end
end
CExpressionCustomizeItem.m_OnUpdateRenqi_CS2LuaHook = function (this)
    if CExpressionMgr.Inst.Renqi >= this.needRenqi then
        if this.mIsTxt == 1 then
            if not CExpressionMgr.Inst:HasGetExpressionTxt(this.mExpressionId) then
                Gac2Gas.UnlockExpressionTxt(this.mExpressionId)
            end
        elseif this.mIsTxt == 0 then
            if not CExpressionMgr.Inst:HasGetSticker(this.mExpressionId) then
                Gac2Gas.UnlockSticker(this.mExpressionId)
            end
        end
    end
end
