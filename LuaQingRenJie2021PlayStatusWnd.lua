local CServerTimeMgr=import "L10.Game.CServerTimeMgr"

CLuaQingRenJie2021PlayStatusWnd  = class()
RegistClassMember(CLuaQingRenJie2021PlayStatusWnd, "m_StatusLabel")
RegistClassMember(CLuaQingRenJie2021PlayStatusWnd, "m_Slider")
RegistClassMember(CLuaQingRenJie2021PlayStatusWnd, "m_Button")
RegistClassMember(CLuaQingRenJie2021PlayStatusWnd, "m_Tick")


CLuaQingRenJie2021PlayStatusWnd.npcEngineId = 0
CLuaQingRenJie2021PlayStatusWnd.stage = 0
CLuaQingRenJie2021PlayStatusWnd.time1 = 0--到达这个阶段的时间
CLuaQingRenJie2021PlayStatusWnd.time2 = 0--到达下个阶段的时间
CLuaQingRenJie2021PlayStatusWnd.bOperate = false

function CLuaQingRenJie2021PlayStatusWnd:Awake()
    self.m_StatusLabel = self.transform:Find("Bottom/StatusLabel"):GetComponent(typeof(UILabel))
    self.m_Slider = self.transform:Find("Bottom/Slider"):GetComponent(typeof(UISlider))
    self.m_Button = self.transform:Find("Bottom/Button").gameObject
    UIEventListener.Get(self.m_Button).onClick = DelegateFactory.VoidDelegate(function(go)
        Gac2Gas.RequestJianJiaCangCangFlowerOperate(CLuaQingRenJie2021PlayStatusWnd.npcEngineId,"")
    end)
end

function CLuaQingRenJie2021PlayStatusWnd:Init()
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick=nil
    end

    self.m_StatusLabel.gameObject:SetActive(false)
    self.m_Button:SetActive(false)

    local now = CServerTimeMgr.Inst.timeStamp
    local time1 = CLuaQingRenJie2021PlayStatusWnd.time1
    local time2 = CLuaQingRenJie2021PlayStatusWnd.time2
    local stage = CLuaQingRenJie2021PlayStatusWnd.stage
    local bOperate = CLuaQingRenJie2021PlayStatusWnd.bOperate

    if time2>time1 then
        if stage == 1 then
            if now>time2 then
                self:SetSliderValue(0.5)
            else
                self:SetSliderValue((now-time1)/(time2-time1)*0.5)
                self.m_Tick = RegisterTick(function()
                    local now1 = CServerTimeMgr.Inst.timeStamp
                    if now1>=time2 then
                        self:SetSliderValue(0.5)
                        UnRegisterTick(self.m_Tick)
                        self.m_Tick=nil
                    else
                        self:SetSliderValue((now1-time1)/(time2-time1)*0.5)
                    end
                end,33)
            end
        elseif stage == 2 then
            if now>time2 then
                self:SetSliderValue(1)
            else
                self:SetSliderValue((now-time1)/(time2-time1)*0.5+0.5)
                self.m_Tick = RegisterTick(function()
                    local now1 = CServerTimeMgr.Inst.timeStamp
                    if now1>=time2 then
                        self:SetSliderValue(1)
                        UnRegisterTick(self.m_Tick)
                        self.m_Tick=nil
                    else
                        self:SetSliderValue((now1-time1)/(time2-time1)*0.5+0.5)
                    end
                end,33)
            end
        elseif stage == 3 then
            self.m_Slider.value = 1
        end
    end

end
function CLuaQingRenJie2021PlayStatusWnd:SetSliderValue(val)
    self.m_Slider.value = val
    local stage = CLuaQingRenJie2021PlayStatusWnd.stage
    local bOperate = CLuaQingRenJie2021PlayStatusWnd.bOperate
    if stage == 1 then
        if bOperate then
            self.m_StatusLabel.gameObject:SetActive(true)
            self.m_StatusLabel.text = LocalString.GetString("等待对方")
            self.m_Button:SetActive(false)
        else
            if val<0.5 then
                self.m_StatusLabel.gameObject:SetActive(false)
                self.m_Button:SetActive(false)
            else
                self.m_StatusLabel.gameObject:SetActive(true)
                self.m_StatusLabel.text = LocalString.GetString("点击松土按钮")
                self.m_Button:SetActive(true)
                self.m_Button.transform:GetChild(0):GetComponent(typeof(CUITexture)):LoadMaterial("UI/Texture/Transparent/Material/halloween_songtu.mat")
            end
        end
    elseif stage == 2 then
        if bOperate then
            self.m_StatusLabel.gameObject:SetActive(true)
            self.m_StatusLabel.text = LocalString.GetString("等待对方")
            self.m_Button:SetActive(false)
        else
            if val<1 then
                self.m_StatusLabel.gameObject:SetActive(false)
                self.m_Button:SetActive(false)
            else
                self.m_StatusLabel.gameObject:SetActive(true)
                self.m_StatusLabel.text = LocalString.GetString("点击浇水按钮")
                self.m_Button:SetActive(true)
                self.m_Button.transform:GetChild(0):GetComponent(typeof(CUITexture)):LoadMaterial("UI/Texture/Transparent/Material/halloween_jiaoshui.mat")
            end
        end
    end

end

function CLuaQingRenJie2021PlayStatusWnd:OnDestroy()
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick=nil
    end
end