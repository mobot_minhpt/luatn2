-- Auto Generated!!
local CButton = import "L10.UI.CButton"
local CChatHelper = import "L10.UI.CChatHelper"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CCommonPlayerListMgr = import "L10.Game.CCommonPlayerListMgr"
local CExpertTeamAskDetailWnd = import "L10.UI.CExpertTeamAskDetailWnd"
local CExpertTeamMgr = import "L10.Game.CExpertTeamMgr"
local CIMMgr = import "L10.Game.CIMMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local DelegateFactory = import "DelegateFactory"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local Int32 = import "System.Int32"
local L10 = import "L10"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local NGUITools = import "NGUITools"
local Object = import "System.Object"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local String = import "System.String"
local System = import "System"
local Time = import "UnityEngine.Time"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local Vector3 = import "UnityEngine.Vector3"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CExpertTeamAskDetailWnd.m_Init_CS2LuaHook = function (this) 
    this.templateNode:SetActive(false)
    this.answerBtn:SetActive(false)
    UIEventListener.Get(this.backBtn).onClick = DelegateFactory.VoidDelegate(function (go) 
        this:Close()
    end)

    if CExpertTeamMgr.Inst.SaveSingleQuestionID > 0 then
        CExpertTeamMgr.Inst:GetSingleQuestionInfo(CExpertTeamMgr.Inst.SaveSingleQuestionID, MakeDelegateFromCSFunction(this.OnDataBack, MakeGenericClass(Action1, String), this), nil)
    end

    UIEventListener.Get(this.answerBtn).onClick = MakeDelegateFromCSFunction(this.AnswerBtnClick, VoidDelegate, this)
    UIEventListener.Get(this.shareBtn).onClick = MakeDelegateFromCSFunction(this.ShareBtnClick, VoidDelegate, this)
end
CExpertTeamAskDetailWnd.m_CanShare_CS2LuaHook = function (this) 
    --限制分享的调用频率
    if this.m_EnableRestrictShareFrequency then
        if Time.realtimeSinceStartup - this.m_LastShareTime < this.m_ShareInterval then
            g_MessageMgr:ShowMessage("INGAME_SHARE_TOO_FREQUENT")
            return false
        end
        this.m_LastShareTime = Time.realtimeSinceStartup
    end
    return true
end
CExpertTeamAskDetailWnd.m_ShareBtnClick_CS2LuaHook = function (this, go) 
    local rows = CreateFromClass(MakeGenericClass(List, PopupMenuItemData))
    CommonDefs.ListAdd(rows, typeof(PopupMenuItemData), PopupMenuItemData(this.SpaceShareText[0], MakeDelegateFromCSFunction(this.ShareToChatChannel, MakeGenericClass(Action1, Int32), this), false, nil))
    CommonDefs.ListAdd(rows, typeof(PopupMenuItemData), PopupMenuItemData(this.SpaceShareText[1], MakeDelegateFromCSFunction(this.ShareToChatChannel, MakeGenericClass(Action1, Int32), this), false, nil))
    CommonDefs.ListAdd(rows, typeof(PopupMenuItemData), PopupMenuItemData(this.SpaceShareText[2], MakeDelegateFromCSFunction(this.ShareToChatChannel, MakeGenericClass(Action1, Int32), this), false, nil))

    local shareToFriend = PopupMenuItemData(LocalString.GetString("我的好友"), DelegateFactory.Action_int(function (index) 
        local shareText = this:GetShareText()
        if System.String.IsNullOrEmpty(shareText) then
            return
        end
        CCommonPlayerListMgr.Inst:ShowFriendsToShareJingLingAnswer(DelegateFactory.Action_ulong(function (playerId) 
            if not this:CanShare() then
                return
            end
            local mainPlayer = CClientMainPlayer.Inst
            if mainPlayer == nil then
                return
            end
            CIMMgr.Inst:SendMsg(playerId, mainPlayer.Id, mainPlayer.EngineId, mainPlayer.Name, EnumToInt(mainPlayer.Class), EnumToInt(mainPlayer.Gender), shareText, true)
            --if (onShareComplete != null)
            --    onShareComplete(shareText, EnumInGameShareChannel.eIM);
            g_MessageMgr:ShowMessage("INGAME_SHARE_SUCCESS")
        end))
    end), false, nil)

    local shareToPersonalSpace = PopupMenuItemData(LocalString.GetString("梦岛心情"), DelegateFactory.Action_int(function (index) 
        if not this:CanShare() then
            return
        end
        local shareText = this:GetShareText()
        if System.String.IsNullOrEmpty(shareText) then
            return
        end


        CPersonalSpaceMgr.Inst:ShareTextToPersonalSpace(shareText, DelegateFactory.Action_string(function (text) 
            g_MessageMgr:ShowMessage("INGAME_SHARE_SUCCESS")
            -- if (text == shareText && onShareComplete != null)
            --    onShareComplete(shareText, EnumInGameShareChannel.ePersonalSpace);
        end))
    end), false, nil)

    CommonDefs.ListAdd(rows, typeof(PopupMenuItemData), shareToFriend)
    CommonDefs.ListAdd(rows, typeof(PopupMenuItemData), shareToPersonalSpace)

    CPopupMenuInfoMgr.ShowPopupMenu(CommonDefs.ListToArray(rows), go.transform, CPopupMenuInfoMgr.AlignType.Top)
end
CExpertTeamAskDetailWnd.m_ShareToChatChannel_CS2LuaHook = function (this, row) 
    if this.returnData ~= nil then
        --string message = string.Format(LocalString.GetString("分享了<link button={0}的梦岛,SharePersonalSpace,{1}>"), sharePlayerName, sharePlayerId);
        local message = this:GetShareText()
        local chatType = EChatPanel.Team
        if row == 0 then
            chatType = EChatPanel.World
        elseif row == 1 then
            chatType = EChatPanel.Guild
        elseif row == 2 then
            chatType = EChatPanel.Team
        end
        --MessageMgr.Inst.ShowMessage("SHARE_MENGDAO_SUCCESS");
        g_MessageMgr:ShowMessage("SHARE_EXPERTTEAM_SUCCESS")
        CChatHelper.SendMsgWithFilterOption(chatType, message, 0, true)
    end
end
CExpertTeamAskDetailWnd.m_AnswerBtnClick_CS2LuaHook = function (this, go) 
    if this.returnData ~= nil then
        CExpertTeamMgr.Inst.SaveQuestionData = this.returnData
        CUIManager.ShowUI(CUIResources.ExpertTeamAnswerWnd)
    end
end
CExpertTeamAskDetailWnd.m_OnDataBack_CS2LuaHook = function (this, ret) 
    if not this then
        return
    end
    CExpertTeamMgr.Inst.SaveSingleQuestionInfo = ret
    this:InitData()
end
CExpertTeamAskDetailWnd.m_AcceptQuestion_CS2LuaHook = function (this, qid) 
    if this.accBtn ~= nil then
        this.accBtn:SetActive(false)
    end

    if this.accSign ~= nil then
        this.accSign:SetActive(true)
    end
end
CExpertTeamAskDetailWnd.m_SetAccept_CS2LuaHook = function (this, go, acceptSign, answer_id) 
    MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("JINGLINGEXPERT_ANSWERACC_CONFIRM"), DelegateFactory.Action(function () 
        this.accBtn = go
        this.accSign = acceptSign
        Gac2Gas.AdoptExpertAnswer(this.qid, answer_id, this.reward_score)
    end), nil, nil, nil, false)
end
CExpertTeamAskDetailWnd.m_SetFavorIcon_CS2LuaHook = function (this, node, sign) 
    local signNode = node.transform:Find("favoricon").gameObject
    local num = System.Int32.Parse(CommonDefs.GetComponent_Component_Type(node.transform:Find("favornum"), typeof(UILabel)).text)
    if sign == 1 then
        signNode:SetActive(true)
        CommonDefs.GetComponent_Component_Type(node.transform:Find("favornum"), typeof(UILabel)).text = tostring((num + 1))
    else
        signNode:SetActive(false)
        CommonDefs.GetComponent_Component_Type(node.transform:Find("favornum"), typeof(UILabel)).text = tostring((num - 1))
    end
end
CExpertTeamAskDetailWnd.m_SetFavor_CS2LuaHook = function (this, go, answer_id) 
    local signNode = go.transform:Find("favoricon").gameObject
    local sign = 1
    if signNode.activeSelf then
        sign = - 1
    else
        sign = 1
    end
    CommonDefs.GetComponent_GameObject_Type(go, typeof(CButton)).Enabled = false

    this.saveSignNode = go
    this.saveSign = sign

    CExpertTeamMgr.Inst:SetFavor(answer_id, this.qid, sign, nil, nil)
end
CExpertTeamAskDetailWnd.m_SetFavorBack_CS2LuaHook = function (this, ret) 
    if not this then
        return
    end

    if this.saveSignNode == nil then
        return
    end
    local retDic = TypeAs(L10.Game.Utils.Json.Deserialize(ret), typeof(MakeGenericClass(Dictionary, String, Object)))
    local result = CommonDefs.DictGetValue(retDic, typeof(String), "result")
    local data = TypeAs(CommonDefs.DictGetValue(retDic, typeof(String), "data"), typeof(MakeGenericClass(List, Object)))
    if result then
        this:SetFavorIcon(this.saveSignNode, this.saveSign)
    end
    CommonDefs.GetComponent_GameObject_Type(this.saveSignNode, typeof(CButton)).Enabled = true

    this.saveSignNode = nil
    this.saveSign = 0
end
CExpertTeamAskDetailWnd.m_InitAnswerList_CS2LuaHook = function (this, answerList, qid) 
    Extensions.RemoveAllChildren(this.table.transform)
    local quesAccept = false
    if answerList ~= nil and answerList.Count > 0 then
        do
            local i = 0
            while i < answerList.Count do
                local info = TypeAs(answerList[i], typeof(MakeGenericClass(Dictionary, String, Object)))
                local expertInfo = TypeAs(CommonDefs.DictGetValue(info, typeof(String), "expert"), typeof(MakeGenericClass(Dictionary, String, Object)))
                local node = NGUITools.AddChild(this.table.gameObject, this.templateNode)
                node:SetActive(true)

                local iconNode = node.transform:Find("icon").gameObject
                CommonDefs.GetComponent_Component_Type(node.transform:Find("name"), typeof(UILabel)).text = ToStringWrap(CommonDefs.DictGetValue(expertInfo, typeof(String), "role_name"))
                local statusLabel = CommonDefs.GetComponent_Component_Type(node.transform:Find("status"), typeof(UILabel))
                statusLabel.text = ToStringWrap(CommonDefs.DictGetValue(info, typeof(String), "answer"))

                local favorBtn = node.transform:Find("subInfo/favorbutton").gameObject
                local favorSign = node.transform:Find("subInfo/favorbutton/favoricon").gameObject
                local acceptBtn = node.transform:Find("subInfo/acceptbutton").gameObject
                local acceptSign = node.transform:Find("subInfo/acceptsign").gameObject

                local is_praised = System.Int32.Parse(ToStringWrap(CommonDefs.DictGetValue(info, typeof(String), "is_praised")))
                if is_praised == 1 then
                    favorSign:SetActive(true)
                else
                    favorSign:SetActive(false)
                end

                local subInfo = node.transform:Find("subInfo").gameObject
                subInfo.transform.localPosition = Vector3(subInfo.transform.localPosition.x, - statusLabel.height, subInfo.transform.localPosition.z)

                CExpertTeamMgr.Inst:SetPlayerIcon(System.UInt64.Parse(ToStringWrap(CommonDefs.DictGetValue(expertInfo, typeof(String), "role_id"))), ToStringWrap(CommonDefs.DictGetValue(expertInfo, typeof(String), "avatar")), iconNode, nil, statusLabel.text, qid, System.UInt64.Parse(ToStringWrap(CommonDefs.DictGetValue(info, typeof(String), "answer_id"))))

                CommonDefs.GetComponent_Component_Type(favorBtn.transform:Find("favornum"), typeof(UILabel)).text = ToStringWrap(CommonDefs.DictGetValue(info, typeof(String), "praised"))

                UIEventListener.Get(favorBtn).onClick = DelegateFactory.VoidDelegate(function (go) 
                    this:SetFavor(go, System.UInt64.Parse(ToStringWrap(CommonDefs.DictGetValue(info, typeof(String), "answer_id"))))
                end)

                local acceptsign = System.Int32.Parse(ToStringWrap(CommonDefs.DictGetValue(info, typeof(String), "adopt")))
                if acceptsign == 1 then
                    acceptSign:SetActive(true)
                    acceptBtn:SetActive(false)
                    quesAccept = true
                else
                    acceptSign:SetActive(false)
                    if this.role_id == CClientMainPlayer.Inst.Id then
                        acceptBtn:SetActive(true)
                        UIEventListener.Get(acceptBtn).onClick = DelegateFactory.VoidDelegate(function (go) 
                            this:SetAccept(go, acceptSign, System.UInt64.Parse(ToStringWrap(CommonDefs.DictGetValue(info, typeof(String), "answer_id"))))
                        end)
                    else
                        acceptBtn:SetActive(false)
                    end
                end
                i = i + 1
            end
        end
        this.table:Reposition()
        this.scrollView:ResetPosition()
    end
    if quesAccept then
        this.answerBtn:SetActive(false)
    else
        this.answerBtn:SetActive(true)
    end
end
CExpertTeamAskDetailWnd.m_InitData_CS2LuaHook = function (this) 
    if not System.String.IsNullOrEmpty(CExpertTeamMgr.Inst.SaveSingleQuestionInfo) then
        local retDic = TypeAs(L10.Game.Utils.Json.Deserialize(CExpertTeamMgr.Inst.SaveSingleQuestionInfo), typeof(MakeGenericClass(Dictionary, String, Object)))
        local result = CommonDefs.DictGetValue(retDic, typeof(String), "result")
        local data = TypeAs(CommonDefs.DictGetValue(retDic, typeof(String), "data"), typeof(MakeGenericClass(Dictionary, String, Object)))
        if result then
            if data ~= nil then
                this.returnData = data

                this.playerNameLable.text = ToStringWrap(CommonDefs.DictGetValue(data, typeof(String), "role_name"))
                this.questionText.text = ToStringWrap(CommonDefs.DictGetValue(data, typeof(String), "content"))
                this.questionScoreLabel.text = ToStringWrap(CommonDefs.DictGetValue(data, typeof(String), "reward_score"))

                this.qid = System.UInt64.Parse(ToStringWrap(CommonDefs.DictGetValue(data, typeof(String), "qid")))
                this.role_id = System.UInt64.Parse(ToStringWrap(CommonDefs.DictGetValue(data, typeof(String), "role_id")))
                this.reward_score = System.UInt32.Parse(ToStringWrap(CommonDefs.DictGetValue(data, typeof(String), "reward_score")))

                if this.role_id == CClientMainPlayer.Inst.Id then
                    this.deleteBtn:SetActive(true)
                    UIEventListener.Get(this.deleteBtn).onClick = DelegateFactory.VoidDelegate(function (go) 
                        this:DeleteQuestion(System.UInt64.Parse(ToStringWrap(CommonDefs.DictGetValue(data, typeof(String), "qid"))))
                    end)
                else
                    this.deleteBtn:SetActive(false)
                end
                CExpertTeamMgr.Inst:SetPlayerIcon(System.UInt64.Parse(ToStringWrap(CommonDefs.DictGetValue(data, typeof(String), "role_id"))), ToStringWrap(CommonDefs.DictGetValue(data, typeof(String), "avatar")), this.playerIconNode, nil, this.questionText.text, this.qid, 0)

                local answerList = TypeAs(CommonDefs.DictGetValue(data, typeof(String), "answers"), typeof(MakeGenericClass(List, Object)))
                this:InitAnswerList(answerList, this.qid)
            end
        else
            --"{\"data\": {\"msg\": \"问题已删除\"}, \"result\": false}"
            local msg = ToStringWrap(CommonDefs.DictGetValue(data, typeof(String), "msg"))
            if not System.String.IsNullOrEmpty(msg) then
                g_MessageMgr:ShowMessage("CUSTOM_STRING2", msg)
            end
            this:Close()
        end
    else
        this:Close()
    end
end
CExpertTeamAskDetailWnd.m_DeleteQuestion_CS2LuaHook = function (this, qid) 
    MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("JINGLINGEXPERT_DEL_CONFIRM"), DelegateFactory.Action(function () 
        --CExpertTeamMgr.Inst.DeleteQuestion(qid, DeleteQuestionBack);
        Gac2Gas.DeleteExpertQuestion(qid)
    end), nil, nil, nil, false)
end
CExpertTeamAskDetailWnd.m_DeleteQuestionBack_CS2LuaHook = function (this, ret) 
    local retDic = TypeAs(L10.Game.Utils.Json.Deserialize(ret), typeof(MakeGenericClass(Dictionary, String, Object)))
    local result = CommonDefs.DictGetValue(retDic, typeof(String), "result")
    if result then
        g_MessageMgr:ShowMessage("QuestionDeleted")
        this:Close()
        EventManager.BroadcastInternalForLua(EnumEventType.OnExpertTeamDeleteQuestion, {this.qid})
    end
end
