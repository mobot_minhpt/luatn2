local DelegateFactory              = import "DelegateFactory"
local ShareBoxMgr                  = import "L10.UI.ShareBoxMgr"
local EShareType                   = import "L10.UI.EShareType"
local CCommonSendFlowerMgr         = import "L10.UI.CCommonSendFlowerMgr"
local EnumSendFlowerType           = import "L10.UI.CCommonSendFlowerMgr+EnumSendFlowerType"
local CPropertyAppearance          = import "L10.Game.CPropertyAppearance"
local EnumClass                    = import "L10.Game.EnumClass"
local EnumGender                   = import "L10.Game.EnumGender"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local CTeamMgr                     = import "L10.Game.CTeamMgr"
local CItemInfoMgr                 = import "L10.UI.CItemInfoMgr"
local CEffectMgr                   = import "L10.Game.CEffectMgr"
local EnumWarnFXType               = import "L10.Game.EnumWarnFXType"
local EWeaponVisibleReason         = import "L10.Engine.EWeaponVisibleReason"
local EHideKuiLeiStatus            = import "L10.Engine.EHideKuiLeiStatus"
local Animation                    = import "UnityEngine.Animation"
local CTrackMgr                    = import "L10.Game.CTrackMgr"

LuaValentine2023YWQSView = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaValentine2023YWQSView, "top1")
RegistClassMember(LuaValentine2023YWQSView, "top1_LeaderModel")
RegistClassMember(LuaValentine2023YWQSView, "top1_PartnerModel")
RegistClassMember(LuaValentine2023YWQSView, "top1_LeaderName")
RegistClassMember(LuaValentine2023YWQSView, "top1_PartnerName")
RegistClassMember(LuaValentine2023YWQSView, "we")
RegistClassMember(LuaValentine2023YWQSView, "we_MeModel")
RegistClassMember(LuaValentine2023YWQSView, "we_PartnerModel")
RegistClassMember(LuaValentine2023YWQSView, "we_MeName")
RegistClassMember(LuaValentine2023YWQSView, "we_PartnerName")
RegistClassMember(LuaValentine2023YWQSView, "register")

RegistClassMember(LuaValentine2023YWQSView, "huoli")
RegistClassMember(LuaValentine2023YWQSView, "huoli_List")
RegistClassMember(LuaValentine2023YWQSView, "huoli_Num")
RegistClassMember(LuaValentine2023YWQSView, "huoli_Template")
RegistClassMember(LuaValentine2023YWQSView, "huoli_Table")
RegistClassMember(LuaValentine2023YWQSView, "huoli_Slider")
RegistClassMember(LuaValentine2023YWQSView, "rightBottom")
RegistClassMember(LuaValentine2023YWQSView, "expressionTimes")
RegistClassMember(LuaValentine2023YWQSView, "sceneTimes")
RegistClassMember(LuaValentine2023YWQSView, "shareTimes")
RegistClassMember(LuaValentine2023YWQSView, "aWu")
RegistClassMember(LuaValentine2023YWQSView, "aWuModel")
RegistClassMember(LuaValentine2023YWQSView, "aWuWord")
RegistClassMember(LuaValentine2023YWQSView, "rank")
RegistClassMember(LuaValentine2023YWQSView, "rankInfo")
RegistClassMember(LuaValentine2023YWQSView, "rankInfo_Rank")
RegistClassMember(LuaValentine2023YWQSView, "rankInfo_Score")
RegistClassMember(LuaValentine2023YWQSView, "tipButton")
RegistClassMember(LuaValentine2023YWQSView, "closeButton")
RegistClassMember(LuaValentine2023YWQSView, "goToButton")
RegistClassMember(LuaValentine2023YWQSView, "title")

RegistClassMember(LuaValentine2023YWQSView, "partnerId")
RegistClassMember(LuaValentine2023YWQSView, "partnerName")
RegistClassMember(LuaValentine2023YWQSView, "partnerGender")
RegistClassMember(LuaValentine2023YWQSView, "partnerIsOnline")
RegistClassMember(LuaValentine2023YWQSView, "expressionTaskCount")
RegistClassMember(LuaValentine2023YWQSView, "dateTaskCount")
RegistClassMember(LuaValentine2023YWQSView, "hasTop1")
RegistClassMember(LuaValentine2023YWQSView, "top1_LeaderIdentifier")
RegistClassMember(LuaValentine2023YWQSView, "top1_PartnerIdentifier")
RegistClassMember(LuaValentine2023YWQSView, "we_MeIdentifier")
RegistClassMember(LuaValentine2023YWQSView, "we_PartnerIdentifier")
RegistClassMember(LuaValentine2023YWQSView, "we_MeRO")
RegistClassMember(LuaValentine2023YWQSView, "we_PartnerRO")
RegistClassMember(LuaValentine2023YWQSView, "aWuIdentifier")

RegistClassMember(LuaValentine2023YWQSView, "singleExpressionTick")
RegistClassMember(LuaValentine2023YWQSView, "curExpressionId")
RegistClassMember(LuaValentine2023YWQSView, "singleExpressionTbl")
RegistClassMember(LuaValentine2023YWQSView, "doubleExpressionTbl")
RegistClassMember(LuaValentine2023YWQSView, "fxKeys")
RegistClassMember(LuaValentine2023YWQSView, "maleExpressionTick")
RegistClassMember(LuaValentine2023YWQSView, "femaleExpressionTick")
RegistClassMember(LuaValentine2023YWQSView, "nextMaleExpressionId")
RegistClassMember(LuaValentine2023YWQSView, "nextFemaleExpressionId")

RegistClassMember(LuaValentine2023YWQSView, "animation")
RegistClassMember(LuaValentine2023YWQSView, "needPlaySwitch")

function LuaValentine2023YWQSView:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

    self:InitUIComponents()
    self:InitEventListener()
    self:InitActive()
    self:InitHuoLi()
    self:InitExpressionTbl()
end

function LuaValentine2023YWQSView:InitUIComponents()
    local info = self.transform:Find("Info")

    self.animation = self.transform:GetComponent(typeof(Animation))

    self.top1 = info:Find("Top1").gameObject
    self.top1_LeaderModel = info:Find("Top1/Model/Leader"):GetComponent(typeof(UITexture))
    self.top1_PartnerModel = info:Find("Top1/Model/Partner"):GetComponent(typeof(UITexture))
    self.top1_LeaderName = info:Find("Top1/Name/Leader"):GetComponent(typeof(UILabel))
    self.top1_PartnerName = info:Find("Top1/Name/Partner"):GetComponent(typeof(UILabel))
    self.we = info:Find("We").gameObject
    self.we_MeModel = info:Find("We/Model/Me"):GetComponent(typeof(UITexture))
    self.we_PartnerModel = info:Find("We/Model/Partner"):GetComponent(typeof(UITexture))
    self.we_MeName = info:Find("We/Name/Me"):GetComponent(typeof(UILabel))
    self.we_PartnerName = info:Find("We/Name/Partner"):GetComponent(typeof(UILabel))
    self.register = info:Find("Register").gameObject

    self.huoli = info:Find("HuoLi").gameObject
    self.huoli_Slider = info:Find("HuoLi/Slider"):GetComponent(typeof(UISlider))
    self.huoli_Template = info:Find("HuoLi/Template").gameObject
    self.huoli_Table = info:Find("HuoLi/Table"):GetComponent(typeof(UITable))
    self.huoli_Num = info:Find("HuoLi/Num"):GetComponent(typeof(UILabel))
    self.huoli_Num.text = 0
    self.rightBottom = info:Find("RightBottom").gameObject
    self.expressionTimes = info:Find("RightBottom/ExpressionButton/Num"):GetComponent(typeof(UILabel))
    self.sceneTimes = info:Find("RightBottom/SceneButton/Num"):GetComponent(typeof(UILabel))
    self.shareTimes = info:Find("RightBottom/ShareButton/Num"):GetComponent(typeof(UILabel))
    self.aWu = info:Find("AWu").gameObject
    self.aWuModel = info:Find("AWu/Model"):GetComponent(typeof(UITexture))
    self.aWuWord = info:Find("AWu/Word"):GetComponent(typeof(UILabel))
    self.rank = info:Find("Rank").gameObject
    self.rankInfo = info:Find("Rank/Info").gameObject
    self.rankInfo_Rank = info:Find("Rank/Info/Rank"):GetComponent(typeof(UILabel))
    self.rankInfo_Score = info:Find("Rank/Info/Score"):GetComponent(typeof(UILabel))

    self.tipButton = info:Find("Title/TipButton").gameObject
    self.goToButton = info:Find("GoToButton").gameObject
    self.closeButton = self.transform.parent:Find("CloseButton").gameObject
    self.title = info:Find("Title").gameObject
end

function LuaValentine2023YWQSView:InitEventListener()
    local info = self.transform:Find("Info")

    UIEventListener.Get(info:Find("Register/Button").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRegisterButtonClick()
	end)

    UIEventListener.Get(self.tipButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)

    UIEventListener.Get(self.goToButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnGoToButtonClick()
	end)

    UIEventListener.Get(info:Find("HuoLi/Num/AddButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnAddButtonClick()
	end)

    UIEventListener.Get(info:Find("RightBottom/SendFlowerButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSendFlowerButtonClick()
	end)

    UIEventListener.Get(info:Find("RightBottom/ExpressionButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnExpressionButtonClick(go)
	end)

    UIEventListener.Get(info:Find("RightBottom/SceneButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSceneButtonClick(go)
	end)

    UIEventListener.Get(info:Find("RightBottom/ShareButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareButtonClick()
	end)

    UIEventListener.Get(info:Find("Rank/Button").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRankButtonClick()
	end)
end

function LuaValentine2023YWQSView:InitActive()
    self.top1:SetActive(false)
    self.aWu:SetActive(false)
    self.rightBottom:SetActive(false)
    self.rankInfo:SetActive(false)
    self.we:SetActive(false)
    self.register:SetActive(false)
end

function LuaValentine2023YWQSView:InitExpressionTbl()
    self.singleExpressionTbl = {}
    self.doubleExpressionTbl = {}

    Valentine2023_UIExpression.Foreach(function (key, value)
        if value.IsSingle > 0 then
            table.insert(self.singleExpressionTbl, key)
        else
            table.insert(self.doubleExpressionTbl, key)
        end
    end)

    self.fxKeys = {}
end

function LuaValentine2023YWQSView:OnEnable()
    g_ScriptEvent:AddListener("SendValentineYWQSInfo", self, "OnSendValentineYWQSInfo")
    g_ScriptEvent:AddListener("CloseYWQSRankWnd", self, "OnCloseYWQSRankWnd")
    g_ScriptEvent:AddListener("ValentineYWQSPlaySwitch", self, "OnValentineYWQSPlaySwitch")
    g_ScriptEvent:AddListener("PersonalSpaceShareFinished", self, "OnPersonalSpaceShareFinished")
    Gac2Gas.RequestValentineYWQSInfo()
    LuaValentine2023Mgr.YWQSInfo.isYWQSOpen = true
end

function LuaValentine2023YWQSView:OnDisable()
    g_ScriptEvent:RemoveListener("SendValentineYWQSInfo", self, "OnSendValentineYWQSInfo")
    g_ScriptEvent:RemoveListener("CloseYWQSRankWnd", self, "OnCloseYWQSRankWnd")
    g_ScriptEvent:RemoveListener("ValentineYWQSPlaySwitch", self, "OnValentineYWQSPlaySwitch")
    g_ScriptEvent:RemoveListener("PersonalSpaceShareFinished", self, "OnPersonalSpaceShareFinished")
    LuaValentine2023Mgr.YWQSInfo.isYWQSOpen = false
end

function LuaValentine2023YWQSView:OnCloseYWQSRankWnd()
    self.animation:Play("valentine2023wnd_townd")
    self.rightBottom:SetActive(self.partnerId and self.partnerId > 0 or false)
    self:SetActiveForRank(true)
    self.top1:SetActive(self.hasTop1)
    self.register:SetActive(self.partnerId and self.partnerId == 0 or false)
end

function LuaValentine2023YWQSView:OnValentineYWQSPlaySwitch()
    self.needPlaySwitch = true
    Gac2Gas.RequestValentineYWQSInfo()
end

function LuaValentine2023YWQSView:OnPersonalSpaceShareFinished()
    Gac2Gas.ShareValentineYWQSToMengDao()
end

function LuaValentine2023YWQSView:SetActiveForRank(active)
    self.huoli:SetActive(active)
    self.title:SetActive(active)
    self.goToButton:SetActive(active)
    self.closeButton:SetActive(active)
    self.rank:SetActive(active)
    self.aWuWord.gameObject:SetActive(active)
end

function LuaValentine2023YWQSView:OnSendValentineYWQSInfo(partnerId, partnerIsOnline, loveValue, rank, todayHuoLi, expressionTaskCount, dateTaskCount, shareMengDaoCount, playerAppearanceInfo)
    self:InitActive()
    self:UpdateHuoLi(todayHuoLi)
    playerAppearanceInfo = playerAppearanceInfo and MsgPackImpl.unpack(playerAppearanceInfo)
    self:UpdateTop1(playerAppearanceInfo)

    self.partnerId = partnerId
    if partnerId == 0 then
        self.register:SetActive(true)
        self.aWu:SetActive(true)
        self:InitAWuModel()
        self.aWuWord.text = LocalString.GetString("参加活动需要单独和一位异性好友组队才行哦")
        return
    end

    self.partnerIsOnline = partnerIsOnline
    self.rankInfo:SetActive(true)
    self.rankInfo_Score.text = loveValue
    self.rankInfo_Rank.text = rank > 0 and rank or LocalString.GetString("未上榜")

    self.rightBottom:SetActive(true)
    self.expressionTimes.gameObject:SetActive(expressionTaskCount > 0)
    self.expressionTimes.text = expressionTaskCount
    self.shareTimes.gameObject:SetActive(shareMengDaoCount > 0)
    self.shareTimes.text = shareMengDaoCount
    self.sceneTimes.gameObject:SetActive(dateTaskCount > 0)
    self.sceneTimes.text = dateTaskCount
    self.expressionTaskCount = expressionTaskCount
    self.dateTaskCount = dateTaskCount
    self:UpdateWe(playerAppearanceInfo)
    self:PlayExpression()

    if self.needPlaySwitch then
        self.animation:Play("valentine2023wnd_switch")
        self.needPlaySwitch = false
    end
end

-- 初始化啊呜模型
function LuaValentine2023YWQSView:InitAWuModel()
    if self.aWuIdentifier then return end

    self.aWuIdentifier = SafeStringFormat3("__%s__", tostring(self.aWuModel.gameObject:GetInstanceID()))
    local prefabname = Valentine2023_YWQS_Setting.GetData().AWuModelPath
    local loader = LuaDefaultModelTextureLoader.Create(function (ro)
        ro:LoadMain(prefabname, nil, false, false)
    end)
    self.aWuModel.mainTexture = CUIManager.CreateModelTexture(self.aWuIdentifier, loader, 180, 0, -0.71, 4.66, false, true, 1, true)
end

-- 榜一显示
function LuaValentine2023YWQSView:UpdateTop1(playerAppearanceInfo)
    if not playerAppearanceInfo or playerAppearanceInfo.Count < 2 then return end

    self.hasTop1 = true
    self.top1:SetActive(true)
    local leaderInfo = playerAppearanceInfo.Count == 2 and playerAppearanceInfo[0] or playerAppearanceInfo[1]
    local partnerInfo = playerAppearanceInfo.Count == 2 and playerAppearanceInfo[1] or playerAppearanceInfo[2]
    self.top1_LeaderName.text = leaderInfo[1]
    self.top1_PartnerName.text = partnerInfo[1]

    local leaderAppearance = CPropertyAppearance()
    leaderAppearance:LoadFromString(leaderInfo[4], CommonDefs.ConvertIntToEnum(typeof(EnumClass), leaderInfo[2]), CommonDefs.ConvertIntToEnum(typeof(EnumGender), leaderInfo[3]))
    self.top1_LeaderIdentifier = SafeStringFormat3("__%s__", tostring(self.top1_LeaderModel.gameObject:GetInstanceID()))
    local leaderRO
    local leaderLoader = LuaDefaultModelTextureLoader.Create(function (ro)
        CClientMainPlayer.LoadResource(ro, self:HandleAppearance(leaderAppearance), true, 1.0,
            0, false, 0, false, 0, false, nil, false, false)
        leaderRO = ro
    end)
    self.top1_LeaderModel.mainTexture = CUIManager.CreateModelTexture(self.top1_LeaderIdentifier, leaderLoader,
        180, 0.05, -1, 4.66, false, true, 1.0, false, false)
    self:SetWeaponVisible(leaderRO)
    self.top1_LeaderModel.depth = leaderInfo[3] == 0 and 14 or 15

    local partnerAppearance = CPropertyAppearance()
    partnerAppearance:LoadFromString(partnerInfo[4], CommonDefs.ConvertIntToEnum(typeof(EnumClass), partnerInfo[2]), CommonDefs.ConvertIntToEnum(typeof(EnumGender), partnerInfo[3]))
    self.top1_PartnerIdentifier = SafeStringFormat3("__%s__", tostring(self.top1_PartnerModel.gameObject:GetInstanceID()))
    local partnerRO
    local partnerLoader = LuaDefaultModelTextureLoader.Create(function (ro)
        CClientMainPlayer.LoadResource(ro, self:HandleAppearance(partnerAppearance), true, 1.0,
            0, false, 0, false, 0, false, nil, false, false)
        partnerRO = ro
    end)
    self.top1_PartnerModel.mainTexture = CUIManager.CreateModelTexture(self.top1_PartnerIdentifier, partnerLoader,
        180, 0.05, -1, 4.66, false, true, 1.0, false, false)
    self:SetWeaponVisible(partnerRO)
    self.top1_PartnerModel.depth = partnerInfo[3] == 0 and 14 or 15
end

-- 主角模型显示
function LuaValentine2023YWQSView:UpdateWe(playerAppearanceInfo)
    if not CClientMainPlayer.Inst then return end

    self.we:SetActive(true)
    self.we_MeName.text = CClientMainPlayer.Inst.Name
    local count = playerAppearanceInfo.Count
    self.partnerName = (count == 1 or count == 3) and playerAppearanceInfo[0][1]
    self.we_PartnerName.text = self.partnerName or ""

    self.we_MeIdentifier = SafeStringFormat3("__%s__", tostring(self.we_MeModel.gameObject:GetInstanceID()))
    local meLoader = LuaDefaultModelTextureLoader.Create(function (ro)
        CClientMainPlayer.LoadResource(ro, self:HandleAppearance(CClientMainPlayer.Inst.AppearanceProp:Clone()), true, 1.0,
            0, false, 0, false, 0, false, nil, false, false)
        self.we_MeRO = ro
    end)
    self.we_MeModel.mainTexture = CUIManager.CreateModelTexture(self.we_MeIdentifier, meLoader,
        180, 0.05, -1, 4.66, false, true, 1.0, true, false)
    CUIManager.SetModelRotation(self.we_MeIdentifier, (not self.partnerIsOnline or count == 0 or count == 2) and 180 or 210)
    self.we_MeModel.depth = CClientMainPlayer.Inst.Gender == EnumGender.Male and 14 or 15

    if not self.partnerIsOnline then
        self.aWu:SetActive(true)
        self:InitAWuModel()
        self.aWuWord.text = LocalString.GetString("眷侣不在线，快呼唤眷侣上来陪伴你吧")
        CUIManager.DestroyModelTexture(self.we_PartnerIdentifier)
    elseif count == 1 or count == 3 then
        self.we_PartnerIdentifier = SafeStringFormat3("__%s__", tostring(self.we_PartnerModel.gameObject:GetInstanceID()))
        local info = playerAppearanceInfo[0]
        local appearance = CPropertyAppearance()
        appearance:LoadFromString(info[4], CommonDefs.ConvertIntToEnum(typeof(EnumClass), info[2]), CommonDefs.ConvertIntToEnum(typeof(EnumGender), info[3]))
        local partnerLoader = LuaDefaultModelTextureLoader.Create(function (ro)
            CClientMainPlayer.LoadResource(ro, self:HandleAppearance(appearance), true, 1.0,
                0, false, 0, false, 0, false, nil, false, false)
            self.we_PartnerRO = ro
        end)
        self.we_PartnerModel.mainTexture = CUIManager.CreateModelTexture(self.we_PartnerIdentifier, partnerLoader,
            150, 0.05, -1, 4.66, false, true, 1.0, true, false)
        self.we_PartnerModel.depth = info[3] == 0 and 14 or 15
        self.partnerGender = info[3] == 0 and EnumGender.Male or EnumGender.Female
    else
        CUIManager.DestroyModelTexture(self.we_PartnerIdentifier)
    end
end

-- 处理外观 不显示特效
function LuaValentine2023YWQSView:HandleAppearance(appearance)
    appearance.HideSuitIdToOtherPlayer = 1
    appearance.HideGemFxToOtherPlayer = 1
    appearance.HideWing = 1
    appearance.TalismanAppearance = 0
    appearance.YingLingState = EnumYingLingState.eDefault
    return appearance
end

function LuaValentine2023YWQSView:InitHuoLi()
    self.huoli_List = {}
    for huoli, itemId, _ in string.gmatch(Valentine2023_YWQS_Setting.GetData().DailyHuoLiRewardMails, "(%d+),(%d+),(%d+)") do
        table.insert(self.huoli_List, {tonumber(huoli), tonumber(itemId)})
    end

    self.huoli_Template:SetActive(false)
    Extensions.RemoveAllChildren(self.huoli_Table.transform)
    local maxHuoLi = self.huoli_List[#self.huoli_List][1]
    local maxX = self.huoli_Slider.transform:Find("Background"):GetComponent(typeof(UIWidget)).width
    for _, data in pairs(self.huoli_List) do
        local child = NGUITools.AddChild(self.huoli_Table.gameObject, self.huoli_Template)
        child:SetActive(true)

        child.transform:Find("Highlight").gameObject:SetActive(false)
        child.transform:Find("Panel/MaskSprite").gameObject:SetActive(false)
        child.transform:Find("Num"):GetComponent(typeof(UILabel)).text = data[1]
        LuaUtils.SetLocalPositionX(child.transform, data[1] / maxHuoLi * maxX)
        local icon = child.transform:Find("Panel/Icon"):GetComponent(typeof(CUITexture))
        icon:LoadMaterial(Item_Item.GetData(data[2]).Icon)
        UIEventListener.Get(icon.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            CItemInfoMgr.ShowLinkItemTemplateInfo(data[2])
        end)
    end
    self.huoli_Slider.sliderValue = 0
end

function LuaValentine2023YWQSView:UpdateHuoLi(todayHuoLi)
    self.huoli_Num.text = todayHuoLi

    local maxHuoLi = self.huoli_List[#self.huoli_List][1]
    self.huoli_Slider.sliderValue = math.min(todayHuoLi / maxHuoLi, 1)
    for id, data in pairs(self.huoli_List) do
        local child = self.huoli_Table.transform:GetChild(id - 1)
        local highlight = child:Find("Highlight").gameObject
        local maskSprite = child:Find("Panel/MaskSprite").gameObject
        if todayHuoLi >= data[1] then
            highlight:SetActive(true)
            maskSprite:SetActive(true)
        else
            highlight:SetActive(false)
            maskSprite:SetActive(false)
        end
    end
end

-- 播放表情
function LuaValentine2023YWQSView:PlayExpression()
    self:ClearTick()
    self:ClearExpression(self.we_MeRO)
    self:ClearExpression(self.we_PartnerRO)
    -- 眷侣没上线，播放单人动作
    if self.partnerId > 0 and not self.partnerIsOnline and self.we_MeRO and CClientMainPlayer.Inst then
        self:RandomExpressionId(true)
        self:DelayPlayExpression(self.we_MeRO, CClientMainPlayer.Inst.Gender, true)

    -- 眷侣上线，播放双人动作
    elseif self.partnerIsOnline and self.we_MeRO and self.we_PartnerRO and CClientMainPlayer.Inst then
        self:RandomExpressionId(false)
        self:DelayPlayExpression(self.we_MeRO, CClientMainPlayer.Inst.Gender, false)
        self:DelayPlayExpression(self.we_PartnerRO, self.partnerGender, false)
    end
end

-- 随机选择一个表情
function LuaValentine2023YWQSView:RandomExpressionId(isSingle)
    local tbl = {}
    local expressionTbl = isSingle and self.singleExpressionTbl or self.doubleExpressionTbl
    for _, id in pairs(expressionTbl) do
        if not self.curExpressionId or self.curExpressionId ~= id then
            table.insert(tbl, id)
        end
    end
    self.curExpressionId = tbl[math.random(#tbl)]
end

-- 延时播放表情动作
function LuaValentine2023YWQSView:DelayPlayExpression(ro, gender, isSingle)
    local isMale = gender == EnumGender.Male
    local data = Valentine2023_UIExpression.GetData(self.curExpressionId)
    local expressionId = isMale and data.MaleExpressionId or data.FemaleExpressionId
    local playTime = isMale and data.MalePlayTime or data.FemalePlayTime
    local isMaleExpressionLonger = data.MalePlayTime[1] > data.FemalePlayTime[0]
    local startTime = playTime[0] == 0 and 0.01 or playTime[0]
    local endTime = playTime[1]

    if isMale then
        self.maleExpressionTick = RegisterTickOnce(function ()
            self:ShowExpressionAction(ro, expressionId, gender)

            self.maleExpressionTick = RegisterTickOnce(function ()
                if self.nextMaleExpressionId then
                    self:ShowExpressionAction(ro, self.nextMaleExpressionId, gender)
                    self.nextMaleExpressionId = nil
                else
                    self:ClearExpression(ro)
                end
                if isMaleExpressionLonger or isSingle then
                    self.maleExpressionTick = RegisterTickOnce(function()
                        self:PlayExpression()
                    end, Valentine2023_YWQS_Setting.GetData().UIExpressionPlayInterval * 1000)
                end
            end, (endTime - startTime) * 1000)
        end, startTime * 1000)
    else
        self.femaleExpressionTick = RegisterTickOnce(function ()
            self:ShowExpressionAction(ro, expressionId, gender)

            self.femaleExpressionTick = RegisterTickOnce(function ()
                if self.nextFemaleExpressionId then
                    self:ShowExpressionAction(ro, self.nextFemaleExpressionId, gender)
                    self.nextFemaleExpressionId = nil
                else
                    self:ClearExpression(ro)
                end
                if not isMaleExpressionLonger or isSingle then
                    self.femaleExpressionTick = RegisterTickOnce(function()
                        self:PlayExpression()
                    end, Valentine2023_YWQS_Setting.GetData().UIExpressionPlayInterval * 1000)
                end
            end, (endTime - startTime) * 1000)
        end, startTime * 1000)
    end
end

-- 令一个ro做一个表情动作
function LuaValentine2023YWQSView:ShowExpressionAction(ro, expressionId, gender)
    local define = Expression_Define.GetData(expressionId)
    if not define then
        ro:DoAni(ro.NewRoleStandAni, true, 0, 1.0, 0.15, true, 1.0)
        return
    end

    local aniName = define.AniName
    ro.NeedUpdateAABB = true

    -- 特效
    --local gender = self.modelPreview.m_FakeAppear.Gender
    local fxids = GetExpressionDefineFx(define,EnumToInt(gender))
    for i = 1, #fxids do
        local fx = CEffectMgr.Inst:AddObjectFX(fxids[i], ro, 0, 1.0, 1.0, nil, false, EnumWarnFXType.None,
        Vector3(0, 0, 0), Vector3(0, 0, 0), nil)
        ro:AddFX(aniName, fx, -1)
    end
    if self.fxKeys[aniName] == nil then
        self.fxKeys[aniName] = true
    end


    -- 手持物
    local additionalWeaponId = gender == EnumGender.Male and define.MaleAdditionalEquip or define.FemaleAdditionalEquip
    local equip = EquipmentTemplate_Equip.GetData(additionalWeaponId)
    if equip then
        ro:AddChild(equip.Prefab, "weapon03", "weapon01")
    end

    ro:AddLoadAllFinishedListener(DelegateFactory.Action_CRenderObject(function(renderObj)
        local loop = define.Loop > 0
        local startTime = define.StartTime
        ro:DoAni(aniName, loop, startTime, 1.0, 0.15, false, 1.0)

        if not loop then
            if define.StopAtEnd <= 0 then
                local nextExpressionId = define.NextExpressionID

                if nextExpressionId > 0 then
                    ro.AniEndCallback = DelegateFactory.Action(function ()
                        ro.AniEndCallback = nil
                        self:ShowExpressionAction(ro, nextExpressionId, gender)
                    end)
                else
                    ro.AniEndCallback = DelegateFactory.Action(function ()
                        self:ClearExpression(ro)
                    end)
                end
            end
        else
            local nextExpressionId = define.NextExpressionID
            if nextExpressionId > 0 then
                if gender == EnumGender.Male then
                    self.nextMaleExpressionId = nextExpressionId
                else
                    self.nextFemaleExpressionId = nextExpressionId
                end
            end
        end
    end))
end

-- 隐藏武器
function LuaValentine2023YWQSView:SetWeaponVisible(ro)
    ro:SetWeaponVisible(EWeaponVisibleReason.Expression, false)
    ro:ForceHideKuiLei(EHideKuiLeiStatus.ForceHideKuiLeiAndClose)
end

-- 清除表情，包括手持物、特效等
function LuaValentine2023YWQSView:ClearExpression(ro)
    if not ro then return end

    self:SetWeaponVisible(ro)
    ro:RemoveChild("weapon03")
    ro.AniEndCallback = nil
    ro:DoAni(ro.NewRoleStandAni, true, 0, 1.0, 0.15, true, 1.0)

    for key, _ in pairs(self.fxKeys) do
        ro:RemoveFX(key)
    end
end

function LuaValentine2023YWQSView:ClearTick()
    if self.maleExpressionTick then UnRegisterTick(self.maleExpressionTick) end
    if self.femaleExpressionTick then UnRegisterTick(self.femaleExpressionTick) end
end

function LuaValentine2023YWQSView:OnDestroy()
    CUIManager.DestroyModelTexture(self.top1_LeaderIdentifier)
	CUIManager.DestroyModelTexture(self.top1_PartnerIdentifier)
	CUIManager.DestroyModelTexture(self.we_MeIdentifier)
	CUIManager.DestroyModelTexture(self.we_PartnerIdentifier)
	CUIManager.DestroyModelTexture(self.aWuIdentifier)
    self:ClearTick()
end

--@region UIEvent

function LuaValentine2023YWQSView:OnTipButtonClick()
    g_MessageMgr:ShowMessage("VALENTINE2023_YIWANGQINGSHEN_TIP")
end

function LuaValentine2023YWQSView:OnGoToButtonClick()
    local msg = g_MessageMgr:FormatMessage("VALENTINE2023_YIWANGQINGSHEN_TELEPORT_CONFIRM")
    g_MessageMgr:ShowOkCancelMessage(msg, function()
        CUIManager.CloseUI(CLuaUIResources.Valentine2023Wnd)
        Gac2Gas.LevelTeleport(Valentine2023_YWQS_Setting.GetData().PlayMapId)
        end, nil, nil, nil, false)
end

function LuaValentine2023YWQSView:OnRegisterButtonClick()
    if CTeamMgr.Inst.TotalMemebersCount ~= 2 then
        g_MessageMgr:ShowMessage("YWQS_NEED_TWO_PLAYER_TEAM")
        return
    end

    local menbers = CTeamMgr.Inst.Members
    if menbers[0].m_MemberGender == menbers[1].m_MemberGender then
        g_MessageMgr:ShowMessage("YWQS_NEED_DIFFERENT_GENDER")
        return
    end

    if not CTeamMgr.Inst:MainPlayerIsTeamLeader() then
        g_MessageMgr:ShowMessage("YWQS_NEED_TEAM_LEADER")
        return
    end

    local members = CTeamMgr.Inst:GetTeamMembersExceptMe()
    MessageWndManager.ShowDelayOKCancelMessage(g_MessageMgr:FormatMessage("VALENTINE2023_YIWANGQINGSHEN_INVITE_CONFIRM", members[0].m_MemberName), DelegateFactory.Action(function ()
        Gac2Gas.RequestSignUpValentineYWQS()
    end), nil, Valentine2023_YWQS_Setting.GetData().InviteConfirmDelay, LocalString.GetString("邀请"), LocalString.GetString("取消"))
end

function LuaValentine2023YWQSView:OnAddButtonClick()
    CUIManager.ShowUI(CLuaUIResources.ScheduleWnd)
end

function LuaValentine2023YWQSView:OnSendFlowerButtonClick()
    if self.partnerName then
        CCommonSendFlowerMgr.Inst:ShowSendFlowerWnd(self.partnerId, self.partnerName, EnumSendFlowerType.PlayerInfoMenu, 0)
    end
end

function LuaValentine2023YWQSView:OnExpressionButtonClick(go)
    LuaValentine2023Mgr:OpenYWQSSelectWnd("expression", go, self.expressionTaskCount)
end

function LuaValentine2023YWQSView:OnSceneButtonClick(go)
    LuaValentine2023Mgr:OpenYWQSSelectWnd("scene", go, self.dateTaskCount)
end

function LuaValentine2023YWQSView:SetShareActive(active)
    self.rank:SetActive(active)
    self.huoli:SetActive(active)
    self.rightBottom:SetActive(active)
    self.tipButton:SetActive(active)
    self.goToButton:SetActive(active)
    self.closeButton:SetActive(active)
end

function LuaValentine2023YWQSView:OnShareButtonClick()
    if self.partnerId <= 0 or not self.partnerIsOnline then
        g_MessageMgr:ShowMessage("VALENTINE2023_YIWANGQINGSHEN_CANNOT_SHARE")
        return
    end

    self:SetShareActive(false)
    self.top1:SetActive(false)

    CUIManager.SetUITop(CLuaUIResources.Valentine2023Wnd)
    CUICommonDef.CaptureScreen("screenshot", false, false, nil,
        DelegateFactory.Action_string_bytes(function(fullPath, jpgBytes)
            self:SetShareActive(true)
            self.top1:SetActive(self.hasTop1)
            CUIManager.ResetUITop(CLuaUIResources.Valentine2023Wnd)
            CLuaShareMgr.SimpleShareLocalImage2PersonalSpace(fullPath)
        end), false)
end

function LuaValentine2023YWQSView:OnRankButtonClick()
    self.animation:Play("valentine2023wnd_torank")
    self.rightBottom:SetActive(false)
    self:SetActiveForRank(false)
    self.top1:SetActive(false)
    self.register:SetActive(false)
    CUIManager.ShowUI(CLuaUIResources.Valentine2023YWQSRankWnd)
end

--@endregion UIEvent
