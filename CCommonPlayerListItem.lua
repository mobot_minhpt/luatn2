-- Auto Generated!!
local CCommonPlayerListItem = import "L10.UI.CCommonPlayerListItem"
local CommonDefs = import "L10.Game.CommonDefs"
local CUICommonDef = import "L10.UI.CUICommonDef"
local EnumClass = import "L10.Game.EnumClass"
local Extensions = import "Extensions"
local Profession = import "L10.Game.Profession"
CCommonPlayerListItem.m_Init_CS2LuaHook = function (this, data, defaultBtnText, buttonEnabled) 
    this.playerId = data.playerId
    this.portrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(data.cls, data.gender, data.expressionIndex), false)
    this.professionIcon.spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), data.cls))
    this.nameLabel.text = data.playerName
    this.levelLabel.text = System.String.Format("lv.{0}", data.level)
    this.btn.Text = defaultBtnText
    this.btn.Enabled = buttonEnabled

    Extensions.SetLocalPositionZ(this.headGo.transform, data.isOnline and 0 or - 1)
    --不在线的头像置灰
end
