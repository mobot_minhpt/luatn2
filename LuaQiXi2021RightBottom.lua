local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local Vector3 = import "UnityEngine.Vector3"
local CScene = import "L10.Game.CScene"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CRenderScene = import "L10.Engine.Scene.CRenderScene"

LuaQiXi2021RightBottom = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaQiXi2021RightBottom, "Button1", "Button1", GameObject)
RegistChildComponent(LuaQiXi2021RightBottom, "Button2", "Button2", GameObject)
RegistChildComponent(LuaQiXi2021RightBottom, "Button3", "Button3", GameObject)
RegistChildComponent(LuaQiXi2021RightBottom, "Label", "Label", UILabel)

--@endregion RegistChildComponent end

function LuaQiXi2021RightBottom:Awake()
    --@region EventBind: Dont Modify Manually!
	UIEventListener.Get(self.Button1.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnButton1Click(go)
	end)

	UIEventListener.Get(self.Button2.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnButton2Click(go)
	end)

	UIEventListener.Get(self.Button3.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnButton3Click(go)
	end)

    --@endregion EventBind end
end

function LuaQiXi2021RightBottom:OnEnable()
	g_ScriptEvent:AddListener("SyncYanHuaPlayInfo",self,"SyncYanHuaPlayInfo")
end

function LuaQiXi2021RightBottom:OnDisable()
	g_ScriptEvent:RemoveListener("SyncYanHuaPlayInfo",self,"SyncYanHuaPlayInfo")
end

function LuaQiXi2021RightBottom:SyncYanHuaPlayInfo()
	local yanhuadata = CLuaQiXiMgr.YanHuaData
	local isowner = yanhuadata.isOwner
	local visitornum = yanhuadata.visitorNum
	local effecttype = yanhuadata.effectType
	local max = yanhuadata.maxVisitorNum
 	self.Label.text = SafeStringFormat3(LocalString.GetString("观众(%d/%d)"),visitornum,max)
end

function LuaQiXi2021RightBottom:Init()

	self.m_camIndex = 0
	self.m_camDatas = {}
	
	local data = QiXi_FireWork2021.GetData()

	local camdatas = nil
	local gameplayid = CScene.MainScene.GamePlayDesignId
	if gameplayid == data.HZGameplayId then
		camdatas = data.HZCamera
	elseif gameplayid == data.JSJGameplayId then
		camdatas = data.JSJCamera
	end
	
	for i=0,camdatas.Length-1,6 do
		local data = {
			x = camdatas[i+0],
			y = camdatas[i+1],
			z = camdatas[i+2],
			rx = camdatas[i+3],
			ry = camdatas[i+4],
			rz = camdatas[i+5]
		}
		table.insert(self.m_camDatas,data)
	end

	local yanhuadata = CLuaQiXiMgr.YanHuaData 
	self.Button2:SetActive(yanhuadata.isOwner)
	self.Button3:SetActive(yanhuadata.isOwner)

	local visitornum = yanhuadata.visitorNum
	local max = yanhuadata.maxVisitorNum
 	self.Label.text = SafeStringFormat3(LocalString.GetString("观众(%d/%d)"),visitornum,max)
end

function LuaQiXi2021RightBottom:ShowHideUI(tf)
    local hideExcepttb = {"QiXi2021RightBottom"}
    local hideExcept = Table2List(hideExcepttb, MakeGenericClass(List, cs_string))
    if tf then
        CUIManager.ShowUIGroupByChangeLayer(CUIManager.EUIModuleGroup.EGameUI, hideExcept, false, false)
        CUIManager.ShowUIGroupByChangeLayer(CUIManager.EUIModuleGroup.EBgGameUI, hideExcept, false, false)
    else
        CUIManager.HideUIGroupByChangeLayer(CUIManager.EUIModuleGroup.EGameUI, hideExcept, false, false, false)
        CUIManager.HideUIGroupByChangeLayer(CUIManager.EUIModuleGroup.EBgGameUI, hideExcept, false, false, false)
    end
end

--@region UIEvent

function LuaQiXi2021RightBottom:OnButton1Click(go)
	local label = LuaGameObject.GetChildNoGC(go.transform,"Label").label
	local yanhuadata = CLuaQiXiMgr.YanHuaData 
	local isowner = yanhuadata.isOwner

	local camCount = #self.m_camDatas --cam数据，从1开始
	local count = 1 + camCount 
	self.m_camIndex = (self.m_camIndex + 1) % count --index：[0,count-1]，0表示正常视角,大于0表示特别视角
	
	if self.m_camIndex == camCount then --最后一个特别视角
		label.text = LocalString.GetString("显示界面")
	else
		label.text = LocalString.GetString("观看视角")
	end

	if self.m_camIndex > 0 then
		local data = self.m_camDatas[self.m_camIndex]
		CameraFollow.Inst:SetCameraStill(Vector3(data.x,data.y,data.z),Vector3(data.rx,data.ry,data.rz))
		CameraFollow.Inst:EnableZoom(false)
		if isowner then 
			self.Button2:SetActive(false)
			self.Button3:SetActive(false)
		end
		self:ShowHideUI(false)
	else
		CRenderScene.Inst.UserDefineLocation = false
		CameraFollow.Inst:SetFollowObj(CClientMainPlayer.Inst.RO, nil)
        CameraFollow.Inst:EnableZoom(true)
		if isowner then 
			self.Button2:SetActive(true)
			self.Button3:SetActive(true)
		end
		self:ShowHideUI(true)
	end

end

function LuaQiXi2021RightBottom:OnButton2Click(go)
	local yanhuadata = CLuaQiXiMgr.YanHuaData
	if yanhuadata.canManualStart then
		CUIManager.ShowUI(CLuaUIResources.QiXi2021ConfirmWnd)
	else
		g_MessageMgr:ShowMessage("2021QiXi_Firework_FirstOver")
	end
end

function LuaQiXi2021RightBottom:OnButton3Click(go)
	CLuaQiXiMgr.ShowInvitWnd()
end

--@endregion UIEvent

