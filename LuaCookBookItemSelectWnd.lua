local QnTableView = import "L10.UI.QnTableView"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"

LuaCookBookItemSelectWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaCookBookItemSelectWnd, "TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaCookBookItemSelectWnd, "QnTableView", "QnTableView", QnTableView)
RegistChildComponent(LuaCookBookItemSelectWnd, "ItemsScrollView", "ItemsScrollView", CUIRestrictScrollView)

--@endregion RegistChildComponent end
RegistClassMember(LuaCookBookItemSelectWnd,"m_List")

function LuaCookBookItemSelectWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaCookBookItemSelectWnd:Init()
    self.TitleLabel.text = LuaCookBookMgr.m_ItemSelectWndType == 1 and LocalString.GetString("选择食材") or LocalString.GetString("选择烹饪技法")
    Gac2Gas.ShangShiMyFoodIngredient()
end

function LuaCookBookItemSelectWnd:OnEnable()
    g_ScriptEvent:AddListener("OnShangShiSendMyFoodIngredient", self, "OnShangShiSendMyFoodIngredient")
end

function LuaCookBookItemSelectWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnShangShiSendMyFoodIngredient", self, "OnShangShiSendMyFoodIngredient")
end

function LuaCookBookItemSelectWnd:OnShangShiSendMyFoodIngredient()
    local list = {}
    for k , v in pairs(LuaCookBookMgr.m_MyFoodIngredientData) do
        if (v.data.IsMethod == 1 and LuaCookBookMgr.m_ItemSelectWndType == 2) or (v.data.IsMethod == 0 and LuaCookBookMgr.m_ItemSelectWndType == 1) then
            table.insert(list,LuaCookBookMgr.m_MyFoodIngredientData[k])
        end
    end
    table.sort(list,function(a,b) 
        if a.count > 0 and b.count == 0 then 
            return true
        elseif a.count == 0 and b.count > 0 then 
            return false
        end
        return a.data.ID < b.data.ID
    end)
    self.m_List = list
    self.QnTableView.m_DataSource = DefaultTableViewDataSource.Create(
        function() 
            return self.m_List and #self.m_List or 0
        end,
        function(item, index)
            self:ItemAt(item,index)
        end)
    self.QnTableView:ReloadData(true, true)
    self.ItemsScrollView:ResetPosition()
    if LuaCookBookMgr.m_ItemSelectWndCurSelectItem then
        for i = 1,#self.m_List do
            local data = self.m_List[i]
            if data.data.ID == LuaCookBookMgr.m_ItemSelectWndCurSelectItem.data.ID then
                local row = i - 1
                self.QnTableView:SetSelectRow(row, true)
                self.QnTableView:ScrollToRow(row)
            end
        end
    end
    self.QnTableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        self:OnSelectAtRow(row)
    end)
end
--@region UIEvent

--@endregion UIEvent
function LuaCookBookItemSelectWnd:ItemAt(item,index)
    local iconTexture = item.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    local numLabel = item.transform:Find("NumLabel"):GetComponent(typeof(UILabel))
    local markSprite = item.transform:Find("MarkSprite").gameObject
    local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))

    local data = self.m_List[index + 1]

    iconTexture:LoadMaterial(SafeStringFormat3("UI/Texture/Item_Other/Material/%s.mat",data.data.Icon))
    numLabel.text = data.count
    numLabel.color = data.count == 0 and Color.red or Color.white
    nameLabel.text = data.data.Name
    markSprite.gameObject:SetActive(data.count == 0)
end

function LuaCookBookItemSelectWnd:OnSelectAtRow(row)
    local data = self.m_List[row + 1]
    if data.count == 0 then
        g_MessageMgr:ShowMessage("CookBookWnd_Select_NoneFood")
        return 
    end
    LuaCookBookMgr.m_SelectIngredientId = data.data.ID
    g_ScriptEvent:BroadcastInLua("OnCookBookSelectIngredientId",data.data.ID,data.count)
    CUIManager.CloseUI(CLuaUIResources.CookBookItemSelectWnd)
end
