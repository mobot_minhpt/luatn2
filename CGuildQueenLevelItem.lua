-- Auto Generated!!
local BangHua_Setting = import "L10.Game.BangHua_Setting"
local CGuildQueenLevelItem = import "L10.UI.CGuildQueenLevelItem"
local CommonDefs = import "L10.Game.CommonDefs"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local DelegateFactory = import "DelegateFactory"
local EGuildQueenStatus = import "L10.UI.EGuildQueenStatus"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
CGuildQueenLevelItem.m_UpdateData_CS2LuaHook = function (this, level, status) 
    this.m_Level = level
    this.m_Status = status

    if this.m_Level == 2 then
        this.m_SecondStageActions = CreateFromClass(MakeGenericClass(List, PopupMenuItemData))
        CommonDefs.ListAdd(this.m_SecondStageActions, typeof(PopupMenuItemData), PopupMenuItemData(LocalString.GetString("进入羊肠大道-1"), MakeDelegateFromCSFunction(this.EnterSecondStage, MakeGenericClass(Action1, Int32), this), false, nil))
        CommonDefs.ListAdd(this.m_SecondStageActions, typeof(PopupMenuItemData), PopupMenuItemData(LocalString.GetString("进入羊肠大道-2"), MakeDelegateFromCSFunction(this.EnterSecondStage, MakeGenericClass(Action1, Int32), this), false, nil))
        CommonDefs.ListAdd(this.m_SecondStageActions, typeof(PopupMenuItemData), PopupMenuItemData(LocalString.GetString("进入羊肠大道-3"), MakeDelegateFromCSFunction(this.EnterSecondStage, MakeGenericClass(Action1, Int32), this), false, nil))
        CommonDefs.ListAdd(this.m_SecondStageActions, typeof(PopupMenuItemData), PopupMenuItemData(LocalString.GetString("进入英雄救美!"), MakeDelegateFromCSFunction(this.EnterSecondStage, MakeGenericClass(Action1, Int32), this), false, nil))
    end

    this.m_StatusSprite.spriteName = CGuildQueenLevelItem.GetStatusSpriteName(status)
    local data = BangHua_Setting.GetData()
    if this.m_Level == 1 then
        this.m_EnterButton.Enabled = status == EGuildQueenStatus.NotStarted
        if status == EGuildQueenStatus.NotStarted then
            this.m_EnterButton.Text = LocalString.GetString("开启第一关")
        else
            this.m_EnterButton.Text = LocalString.GetString("已开启")
        end
        this.m_DescriptionLabel.text = data.FirstLevelDesp
    else
        if this.m_Level == 2 then
            this.m_DescriptionLabel.text = data.SecondLevelDesp
        else
            this.m_DescriptionLabel.text = data.ThirdLevelDesp
        end
        this.m_HighlightSprite.gameObject:SetActive(status == EGuildQueenStatus.Processing)
        if this.m_EnterButton ~= nil then
            this.m_EnterButton.Enabled = (this.m_Status == EGuildQueenStatus.Processing)
        end
    end
end
CGuildQueenLevelItem.m_OnEnterButtonClick_CS2LuaHook = function (this, button) 
    if this.m_Level == 1 then
        local format = g_MessageMgr:FormatMessage("BANG_HUA_STAGE1_START_CONFIRM")
        MessageWndManager.ShowOKCancelMessage(format, DelegateFactory.Action(function () 
            Gac2Gas.StartBangHuaFirstStage()
            CUIManager.CloseUI(CUIResources.GuildQueenWnd)
        end), nil, nil, nil, false)
    elseif this.m_Level == 2 then
        CPopupMenuInfoMgr.ShowPopupMenu(CommonDefs.ListToArray(this.m_SecondStageActions), this.m_EnterButton.transform, CPopupMenuInfoMgr.AlignType.Top, 1, nil, 600, true, 350)
    elseif this.m_Level == 3 then
        Gac2Gas.EnterBangHuaThirdStagePlay()
        CUIManager.CloseUI(CUIResources.GuildQueenWnd)
    end
end
CGuildQueenLevelItem.m_GetStatusSpriteName_CS2LuaHook = function (status) 
    if status == EGuildQueenStatus.NotStarted then
        return CGuildQueenLevelItem.s_NotBeginSpriteName
    elseif status == EGuildQueenStatus.Processing then
        return CGuildQueenLevelItem.s_ProcessingSpriteName
    end
    return CGuildQueenLevelItem.s_FinishSpriteName
end
CGuildQueenLevelItem.m_EnterSecondStage_CS2LuaHook = function (this, param) 
    if param < 3 then
        Gac2Gas.EnterBangHuaSecondStageMultiPlay(param + 1)
    else
        Gac2Gas.EnterBangHuaSecondStageSinglePlay()
    end
    CUIManager.CloseUI(CUIResources.GuildQueenWnd)
end
