local MessageWndManager = import "L10.UI.MessageWndManager"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local Gac2Login = import "L10.Game.Gac2Login"
local CLoginMgr = import "L10.Game.CLoginMgr"
local MessageBoxType = import "L10.UI.MessageWndManager+MessageBoxType"
local CLogMgr = import "L10.CLogMgr"

if rawget(_G, "LuaPinchFaceTimeCtrlMgr") then
    g_ScriptEvent:RemoveListener("MainPlayerCreated", LuaPinchFaceTimeCtrlMgr, "OnMainPlayerCreated")
    g_ScriptEvent:RemoveListener("BackToLoginScene", LuaPinchFaceTimeCtrlMgr, "OnBackToLoginScene")
end

EnumState = {
    None = 0,
    Normal = 1,
    PinchFaceTick = 2,
    WaitSetChange = 3,
    MsgCountDown = 4,
    UICountDown = 5,
}
EnumStateIdx2Name = {}
for k, v in pairs(EnumState) do
    EnumStateIdx2Name[v] = k
end

EnumEvent = {
    OpenWnd = 1,
    OpenWndTimesUp = 2,
    CloseWnd = 3,
    SetUnchanged = 4,
    TimesUp = 5,
    ContinuePinchFace = 6,
    ConfirmMsg = 7,
    ContinueTimesUp = 8,
}

StateTransDef = {
    [EnumState.Normal] = {
        [EnumEvent.OpenWnd] = EnumState.PinchFaceTick,
        [EnumEvent.OpenWndTimesUp] = EnumState.MsgCountDown,
    },
    [EnumState.PinchFaceTick] = {
        [EnumEvent.CloseWnd] = EnumState.Normal,
        [EnumEvent.TimesUp] = EnumState.MsgCountDown,
        [EnumEvent.SetUnchanged] = EnumState.WaitSetChange,
    },
    [EnumState.WaitSetChange] = {
        [EnumEvent.ContinuePinchFace] = EnumState.PinchFaceTick,
        [EnumEvent.ContinueTimesUp] = EnumState.MsgCountDown,
    },
    [EnumState.MsgCountDown] = {
        [EnumEvent.ConfirmMsg] = EnumState.UICountDown,
    },
    [EnumState.UICountDown] = {
        [EnumEvent.CloseWnd] = EnumState.Normal,
    },
}

LuaPinchFaceTimeCtrlMgr = {}

LuaPinchFaceTimeCtrlMgr.m_Status = EnumState.None
LuaPinchFaceTimeCtrlMgr.m_PinchFaceDuration = 0
LuaPinchFaceTimeCtrlMgr.m_PinchFaceEndTime = 0

-- 服务器登录超时时间
LuaPinchFaceTimeCtrlMgr.m_TimeoutDuration = 5*60
LuaPinchFaceTimeCtrlMgr.m_CheckIsOperatingInterval = 4 * 60
LuaPinchFaceTimeCtrlMgr.m_TickInterval = 4*60
LuaPinchFaceTimeCtrlMgr.m_Tick = nil

LuaPinchFaceTimeCtrlMgr.m_LastResetTime = 0

LuaPinchFaceTimeCtrlMgr.m_StartPinchFace = false
LuaPinchFaceTimeCtrlMgr.m_CountDownMsgId = nil

LuaPinchFaceTimeCtrlMgr.m_Debug = true

g_ScriptEvent:AddListener("MainPlayerCreated", LuaPinchFaceTimeCtrlMgr, "OnMainPlayerCreated")
g_ScriptEvent:AddListener("BackToLoginScene", LuaPinchFaceTimeCtrlMgr, "OnBackToLoginScene")

function LuaPinchFaceTimeCtrlMgr:Init()
    print("-------------- Init")
    self.m_Status = EnumState.None

    self:UnRegisterTick()
    self:CloseMsg()

    self.m_PinchFaceDuration = 0
    self.m_PinchFaceEndTime = 0
    self.m_LastResetTime = 0

    self.m_StartPinchFace = false
end

function LuaPinchFaceTimeCtrlMgr:OnSendPinchFaceDuration(duration)
    self:Init()

    self.m_Status = EnumState.Normal
    self.m_PinchFaceDuration = duration

    print("-------------- OnSendPinchFaceDuration", duration)
end

function LuaPinchFaceTimeCtrlMgr:OnLoginTimeout()
    if self.m_Debug then
        local text = SafeStringFormat3("OnLoginTimeout,,%d,,%s,,%s,,%s", self.m_Status, self:GetNowTime(), self.m_LastResetTime, self.m_PinchFaceEndTime)
        CLogMgr.Log(text)
    end
    print(self.m_Status, self.m_StartPinchFace, self.m_PinchFaceEndTime, self:GetNowTime())

    if self.m_Status == EnumState.None then
        return
    end

    self:UnRegisterTick()

    local message

    if CUIManager.IsLoaded(CLuaUIResources.PinchFaceWnd) then
        if self:GetNowTime() > self.m_PinchFaceEndTime then
            message = g_MessageMgr:FormatMessage("PINCH_FACE_TOO_LONG")
        elseif self.m_Status == EnumState.WaitSetChange then
            message = g_MessageMgr:FormatMessage("PINCH_FACE_WITHOUT_OPERTATION")
        end
        LuaPinchFaceMgr.m_CanCacheOfflineData = true
        LuaPinchFaceMgr:CacheOfflineData()
        CUIManager.CloseUI(CLuaUIResources.PinchFaceWnd)
    end

    self:Init()
    if message then
        MessageWndManager.ShowOKMessage(message, DelegateFactory.Action(function ()
            CLoginMgr.Inst:DisconnectToStartWnd(true);
        end))
    else
        CLoginMgr.Inst:_TryBackToLoginScene()
    end
end

-- 进入游戏
function LuaPinchFaceTimeCtrlMgr:OnMainPlayerCreated()
    self:Init()
end

-- 返回登录界面
function LuaPinchFaceTimeCtrlMgr:OnBackToLoginScene()
    self:Init()
end

-- 创角阶段需要延长登录超时时间才调这个接口
function LuaPinchFaceTimeCtrlMgr:OnOpenWnd()
    local now = self:GetNowTime()
    print("OnOpenWnd", now)

    if not self.m_StartPinchFace then
        self.m_PinchFaceEndTime = now + self.m_PinchFaceDuration
        self.m_StartPinchFace = true 
    end

    if now < self.m_PinchFaceEndTime then
        self:TriggerEvent(EnumEvent.OpenWnd)
    else
        self:TriggerEvent(EnumEvent.OpenWndTimesUp)
    end
end

function LuaPinchFaceTimeCtrlMgr:OnCloseWnd()
    print("OnCloseWnd")
    self:TriggerEvent(EnumEvent.CloseWnd)
end

function LuaPinchFaceTimeCtrlMgr:ContinuePinchFace()
    print("ContinuePinchFace")
    if self:GetNowTime() > self.m_PinchFaceEndTime then
        self:TriggerEvent(EnumEvent.ContinueTimesUp)
    else
        self:TriggerEvent(EnumEvent.ContinuePinchFace)
    end
end

function LuaPinchFaceTimeCtrlMgr:ConfirmAboutToTimeout()
    print("ConfirmAboutToTimeout")
    self:TriggerEvent(EnumEvent.ConfirmMsg)
end

function LuaPinchFaceTimeCtrlMgr:TriggerEvent(event, ...)
    local def = StateTransDef[self.m_Status]
    local nextStatus = def and def[event]
    if not nextStatus then
        return
    end

    local onLeave = "OnLeave" .. EnumStateIdx2Name[self.m_Status]
    if self[onLeave] then
        self[onLeave](self, ...)
    end

    if self.m_Debug then
        CLogMgr.Log(SafeStringFormat3("TriggerEvent,,%d,,%s,,%s", event, EnumStateIdx2Name[self.m_Status], EnumStateIdx2Name[nextStatus]))
    end
    print("TriggerEvent", event, EnumStateIdx2Name[self.m_Status], EnumStateIdx2Name[nextStatus])
    self.m_Status = nextStatus

    local onEnter = "OnEnter" .. EnumStateIdx2Name[nextStatus]
    if self[onEnter] then
        self[onEnter](self, ...)
    end
end

function LuaPinchFaceTimeCtrlMgr:ResetTimeOut(bCheckTime)
    local now = self:GetNowTime()
    
    if self.m_Debug then
        CLogMgr.Log(SafeStringFormat3("ResetTimeOut check,,%d,,%d,,%d", bCheckTime and 1 or 0, now, self.m_PinchFaceEndTime))
    end

    print("ResetTimeOut check", bCheckTime, now, self.m_PinchFaceEndTime)

    if bCheckTime and now > self.m_PinchFaceEndTime then
        return
    end

    Gac2Login.RequestResetTimeout("PinchFace")

    self.m_LastResetTime = now

    if self.m_Debug then
        CLogMgr.Log(SafeStringFormat3("ResetTimeOut,,%d,,%d", now, self.m_PinchFaceEndTime))
    end

    print("ResetTimeOut", bCheckTime, now, self.m_PinchFaceEndTime)
end

function LuaPinchFaceTimeCtrlMgr:GetLastResetTime()
    if self.m_LastResetTime == 0 then
        return self:GetNowTime()
    end
    return self.m_LastResetTime
end

function LuaPinchFaceTimeCtrlMgr:GetRemindText()
    local lastResetTime = self:GetLastResetTime()
    local leftTime = lastResetTime + self.m_TimeoutDuration - self:GetNowTime()

    if leftTime <= 0 then
        return ""
    end

    local minSecFmt = LocalString.GetString("%s分%s秒")
    local secFmt = LocalString.GetString("%s秒")

    local text
    if leftTime >= 60 then
        local min = math.floor(leftTime / 60)
        local sec = math.floor(leftTime % 60)
        text = LocalString.GetString(SafeStringFormat3(minSecFmt, min, sec))
    else
        local sec = leftTime
        text = LocalString.GetString(SafeStringFormat3(secFmt, sec))
    end

    local message = g_MessageMgr:FormatMessage("PINCH_FACE_ABOUT_TO_TIMEOUT_TOP_NOTICE", text)

    return message
end

function LuaPinchFaceTimeCtrlMgr:CloseMsg()
    if self.m_CountDownMsgId then
        MessageWndManager.CloseMessageBox(self.m_CountDownMsgId)
        self.m_CountDownMsgId = nil
    end
end

function LuaPinchFaceTimeCtrlMgr:ShowNoOperationMsg()
    local lastResetTime = self:GetLastResetTime()
    local leftTime = lastResetTime + self.m_TimeoutDuration - self:GetNowTime()

    if leftTime <= 0 then
        self:CloseMsg()
        self:UnRegisterTick()
        return
    end

    local fmt = LocalString.GetString("%s秒")
    local text = LocalString.GetString(SafeStringFormat3(fmt, leftTime))

    local message = g_MessageMgr:FormatMessage("PINCH_FACE_WITHOUT_OPERATION_REMIND", text)

    if self.m_CountDownMsgId then
        local messageBoxInfo = MessageWndManager.MessageBoxInfo
        if messageBoxInfo.id == self.m_CountDownMsgId then
            messageBoxInfo.text = message
            MessageWndManager.MessageBoxInfo = messageBoxInfo
            return
        end
    end

    self.m_CountDownMsgId = self:ShowConfirmMessageWithUpdateText(message, DelegateFactory.Action(function()
        self:ContinuePinchFace()
    end))
end

function LuaPinchFaceTimeCtrlMgr:ShowCountDownMsg()
    local lastResetTime = self:GetLastResetTime()
    local leftTime = lastResetTime + self.m_TimeoutDuration - self:GetNowTime()

    if leftTime <= 0 then
        self:CloseMsg()
        self:UnRegisterTick()
        return
    end

    local minSecFmt = LocalString.GetString("%s分%s秒")
    local secFmt = LocalString.GetString("%s秒")

    local text
    if leftTime >= 60 then
        local min = math.floor(leftTime / 60)
        local sec = math.floor(leftTime % 60)
        text = LocalString.GetString(SafeStringFormat3(minSecFmt, min, sec))
    else
        local sec = leftTime
        text = LocalString.GetString(SafeStringFormat3(secFmt, sec))
    end

    local message = g_MessageMgr:FormatMessage("PINCH_FACE_ABOUT_TO_TIMEOUT_REMIND", text)

    if self.m_CountDownMsgId then
        local messageBoxInfo = MessageWndManager.MessageBoxInfo
        if messageBoxInfo.id == self.m_CountDownMsgId then
            messageBoxInfo.text = message
            MessageWndManager.MessageBoxInfo = messageBoxInfo
            return
        end
    end

    self.m_CountDownMsgId = self:ShowConfirmMessageWithUpdateText(message, DelegateFactory.Action(function()
        self:ConfirmAboutToTimeout()
    end))
end

function LuaPinchFaceTimeCtrlMgr:CheckIsChanged()
    if CUIManager.IsLoaded(CLuaUIResources.PinchFaceWnd) then
        return LuaPinchFaceMgr:IsPlayerOperating()
    end
    return LuaCharacterCreation2023Mgr:IsPlayerOperating()
end

function LuaPinchFaceTimeCtrlMgr:OnTick()
    if self.m_Status ~= EnumState.PinchFaceTick then
        return
    end

    -- 没有操作
    if not self:CheckIsChanged() then
        self:TriggerEvent(EnumEvent.SetUnchanged)
        return
    end

    self:ResetTimeOut()

    -- 达到总创建时长
    if self:GetNowTime() > self.m_PinchFaceEndTime then
        self:TriggerEvent(EnumEvent.TimesUp)
        return
    end
end

function LuaPinchFaceTimeCtrlMgr:UnRegisterTick()
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
    end
end

function LuaPinchFaceTimeCtrlMgr:OnLeaveNormal()
    self:ResetTimeOut(true)
end

function LuaPinchFaceTimeCtrlMgr:OnEnterPinchFaceTick()
    self:UnRegisterTick()

    self.m_Tick = RegisterTick(function()
        self:OnTick()
    end, self.m_TickInterval * 1000)
end

function LuaPinchFaceTimeCtrlMgr:OnLeavePinchFaceTick()
    self:UnRegisterTick()
end

function LuaPinchFaceTimeCtrlMgr:OnEnterWaitSetChange()
    self:UnRegisterTick()

    self:ShowNoOperationMsg()

    self.m_Tick = RegisterTick(function()
        self:ShowNoOperationMsg()
    end, 400)
end

function LuaPinchFaceTimeCtrlMgr:OnLeaveWaitSetChange()
    self:UnRegisterTick()
    self:CloseMsg()
    self:ResetTimeOut(true)
end

function LuaPinchFaceTimeCtrlMgr:OnEnterMsgCountDown()
    self:UnRegisterTick()
    
    self:ShowCountDownMsg()

    self.m_Tick = RegisterTick(function()
        self:ShowCountDownMsg()
    end, 400)
end

function LuaPinchFaceTimeCtrlMgr:OnLeaveMsgCountDown()
    self:UnRegisterTick()
    self:CloseMsg()
end

function LuaPinchFaceTimeCtrlMgr:OnEnterUICountDown()
    local lastResetTime = self:GetLastResetTime()
    local leftTime = lastResetTime + self.m_TimeoutDuration - self:GetNowTime()

    g_MessageMgr:ShowTopNotice(function() 
        return self:GetRemindText() 
    end, leftTime)
end

function LuaPinchFaceTimeCtrlMgr:OnLeaveUICountDown()
    g_MessageMgr:HideTopNotice()
end

function LuaPinchFaceTimeCtrlMgr:GetNowTime()
    return math.floor(UnityEngine.Time.realtimeSinceStartup)
end

function LuaPinchFaceTimeCtrlMgr:ShowConfirmMessageWithUpdateText(msg, okBtnAction)
    local messageBoxInfo = MessageWndManager.MessageBoxInfo
    messageBoxInfo.id = MessageWndManager.GetNextID()
	MessageWndManager.s_CurrentMessageBoxUUID = messageBoxInfo.id
    messageBoxInfo.text = msg
	messageBoxInfo.aliveDuration = -1
    messageBoxInfo.okLabel = LocalString.GetString("确定")
    messageBoxInfo.onOKButtonClick = okBtnAction
    messageBoxInfo.cancelLabel = ""
	messageBoxInfo.okBtnDefaultSelected = false
    messageBoxInfo.type = MessageBoxType.OK
    messageBoxInfo.updateText = true
    MessageWndManager.MessageBoxInfo = messageBoxInfo
    CUIManager.ShowUI("MessageBox")
	return messageBoxInfo.id
end