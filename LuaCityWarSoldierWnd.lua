require("common/common_include")

local LuaUtils = import "LuaUtils"
local UIEventListener = import "UIEventListener"
local Extensions = import "Extensions"
local UILabel = import "UILabel"
local Constants = import "L10.Game.Constants"
local UISprite = import "UISprite"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local AlignType3 = import "L10.UI.CPopupMenuInfoMgr+AlignType"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"
local Profession = import "L10.Game.Profession"
local NGUIText = import "NGUIText"
local PublicMap_PublicMap = import "L10.Game.PublicMap_PublicMap"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
local CPlayerInfoMgrAlignType = import "CPlayerInfoMgr+AlignType"
local Guild_Setting = import "L10.Game.Guild_Setting"
local QnCheckBox = import "L10.UI.QnCheckBox"
local PlayerSettings = import "L10.Game.PlayerSettings"

CLuaCityWarSoldierWnd = class()
CLuaCityWarSoldierWnd.Path = "ui/citywar/LuaCityWarSoldierWnd"
CLuaCityWarSoldierWnd.FistPlayerId = nil

RegistClassMember(CLuaCityWarSoldierWnd, "m_GridView")
RegistClassMember(CLuaCityWarSoldierWnd, "m_DataTable")
RegistClassMember(CLuaCityWarSoldierWnd, "m_DataObjTable")
RegistClassMember(CLuaCityWarSoldierWnd, "m_FlagNameTable")
RegistClassMember(CLuaCityWarSoldierWnd, "m_TypeNameTable")
RegistClassMember(CLuaCityWarSoldierWnd, "m_PlayerId2ObjTable")
RegistClassMember(CLuaCityWarSoldierWnd, "m_SortBtnObjTable")
RegistClassMember(CLuaCityWarSoldierWnd, "m_SortBtnStatusTable")
RegistClassMember(CLuaCityWarSoldierWnd, "m_LastSortBtnObj")
RegistClassMember(CLuaCityWarSoldierWnd, "m_SortFuncTable")

RegistClassMember(CLuaCityWarSoldierWnd, "m_HideNoneEnemyConfigObj")

function CLuaCityWarSoldierWnd:Awake( ... )
	Gac2Gas.RequestGetGuildInfo("GuildCityWarAppointInfo")

	UIEventListener.Get(self.transform:Find("Offset/Buttons/TipBtn").gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
		g_MessageMgr:ShowMessage("KFCZ_MERCENARY_INTERFACE_TIP")
	end)
	UIEventListener.Get(self.transform:Find("Offset/Buttons/RefreshBtn").gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
		Gac2Gas.RequestGetGuildInfo("GuildCityWarAppointInfo")
	end)
	UIEventListener.Get(self.transform:Find("Offset/Buttons/CancelBtn").gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
		if CClientMainPlayer.Inst ~= nil then
        	Gac2Gas.RequestOperationInGuild(CClientMainPlayer.Inst.BasicProp.GuildId, "ClearCityWarAppointInfo", "", "")
    	end
	end)
	UIEventListener.Get(self.transform:Find("Offset/Buttons/SummonBtn").gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
		Gac2Gas.RequestSummonGuildMemberToCity()
	end)

	self.m_GridView = self.transform:Find("Offset/AdvView"):GetComponent(typeof(QnAdvanceGridView))
    self.m_GridView.m_DataSource = DefaultTableViewDataSource.Create(function()
			return #self.m_DataTable
		end, function(item, index)
			self:InitItem(item.gameObject, index + 1)
		end)
    self.m_GridView.OnSelectAtRow=DelegateFactory.Action_int(function (row)
    	CPlayerInfoMgr.ShowPlayerPopupMenu(self.m_DataTable[row + 1].PlayerId, EnumPlayerInfoContext.GuildMember, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), CPlayerInfoMgrAlignType.Default)
    end)

	self.m_DataObjTable = {}
	self.m_PlayerId2ObjTable = {}
	self.m_FlagNameTable = {LocalString.GetString("无"), LocalString.GetString("战场一"), LocalString.GetString("战场二"), LocalString.GetString("战场三")}
	self.m_TypeNameTable = {LocalString.GetString("无"), LocalString.GetString("医疗佣兵"), LocalString.GetString("士气佣兵"), LocalString.GetString("舍身佣兵")}

	self:InitSortButtons()

	self.m_HideNoneEnemyConfigObj = self.transform:Find("Offset/ConfigRoot").gameObject
	self.m_HideNoneEnemyConfigObj:SetActive(false)
	UIEventListener.Get(self.transform:Find("Offset/Buttons/ConfigBtn").gameObject).onClick = LuaUtils.VoidDelegate(function ()
		self.m_HideNoneEnemyConfigObj:SetActive(true)
	end)
	local checkbox = self.m_HideNoneEnemyConfigObj.transform:Find("HideCheckBox"):GetComponent(typeof(QnCheckBox))
	checkbox.OnValueChanged = DelegateFactory.Action_bool(function (checked)
		PlayerSettings.HideNoneEnemyInCityWar = checked
	end)
	checkbox:SetSelected(PlayerSettings.HideNoneEnemyInCityWar, true)
end

function CLuaCityWarSoldierWnd:InitSortButtons( ... )
	self.m_SortBtnStatusTable = {}
	self.m_SortBtnObjTable = {}
	self.m_SortFuncTable = {}
	local transNameTbl = {"RankBtnRole", "RankBtnLevel", "RankBtnXiuWei", "RankBtnXiuLian", "RankBtnEquipScore", "RankBtnMessage"}
	for i = 1, #transNameTbl do
		local obj = self.transform:Find("Offset/Header/Table/"..transNameTbl[i]).gameObject
		table.insert(self.m_SortBtnObjTable, obj)
		table.insert(self.m_SortBtnStatusTable, -1)
		UIEventListener.Get(obj).onClick = LuaUtils.VoidDelegate(function (obj)
			self:ChooseSortBtn(obj)
		end)
	end

	self.m_SortFuncTable = {function (a, b)
			return a.IsOnline > b.IsOnline
		end,function (a, b)
			return a.IsOnline < b.IsOnline
		end, function (a, b)
			if a.XianFanStatus ~= b.XianFanStatus then
				return a.XianFanStatus > b.XianFanStatus
			else
				return a.Level > b.Level
			end
		end, function (a, b)
			if a.XianFanStatus ~= b.XianFanStatus then
				return a.XianFanStatus < b.XianFanStatus
			else
				return a.Level < b.Level
			end
		end, function (a, b)
			return a.Capacity > b.Capacity
		end, function (a, b)
			return a.Capacity < b.Capacity
		end, function (a, b)
			return a.MapIndex > b.MapIndex
		end, function (a, b)
			return a.MapIndex < b.MapIndex
		end, function (a, b)
			return a.BattleGroundFlag < b.BattleGroundFlag
		end, function (a, b)
			return a.BattleGroundFlag > b.BattleGroundFlag
		end, function (a, b)
			return a.MercenaryType < b.MercenaryType
		end, function (a, b)
			return a.MercenaryType > b.MercenaryType
		end}
end

function CLuaCityWarSoldierWnd:ChangeSortBtnLabel(obj, bClear, bInc)
	local label = obj.transform:Find("Label"):GetComponent(typeof(UILabel))
	local str = string.gsub(label.text, LocalString.GetString("▲"), "")
	str = string.gsub(str, LocalString.GetString("▼"), "")
	if bClear then label.text = str return end
	str = str..(bInc and LocalString.GetString("▲") or LocalString.GetString("▼"))
	label.text = str
end

function CLuaCityWarSoldierWnd:ChooseSortBtn(obj)
	if self.m_LastSortBtnObj then
		 self:ChangeSortBtnLabel(self.m_LastSortBtnObj, true, true)
	end
	for i = 1, #self.m_SortBtnObjTable do
		if obj == self.m_SortBtnObjTable[i] then
			if self.m_SortBtnStatusTable[i] == 1 then
				self.m_SortBtnStatusTable[i] = 0
			else
				self.m_SortBtnStatusTable[i] = self.m_SortBtnStatusTable[i] + 1
			end
			self:ChangeSortBtnLabel(obj, false, self.m_SortBtnStatusTable[i] == 1)
			self:RefreshData((i - 1) * 2 + 1 + self.m_SortBtnStatusTable[i])
		else
			self.m_SortBtnStatusTable[i] = -1
		end
	end

	self.m_LastSortBtnObj = obj
end

function CLuaCityWarSoldierWnd:RefreshData(sortType)
	-- sort the data firstly by index
	table.sort(self.m_DataTable, self.m_SortFuncTable[sortType])
	self.m_GridView:ReloadData(true, false)
end

function CLuaCityWarSoldierWnd:InitItem(obj, index)
	local v = self.m_DataTable[index]
	obj.transform:Find("LabelName"):GetComponent(typeof(UILabel)).text = v.Name
	local GradeLabel = obj.transform:Find("LabelGrade"):GetComponent(typeof(UILabel))
	GradeLabel.text = "Lv."..v.Level
	GradeLabel.color = v.XianFanStatus > 0 and NGUIText.ParseColor24(Constants.ColorOfFeiSheng, 0) or NGUIText.ParseColor24("112C4C", 0)
	obj.transform:Find("LabelPower"):GetComponent(typeof(UILabel)).text = v.Capacity
	local sceneLabel = obj.transform:Find("LabelScene"):GetComponent(typeof(UILabel))
	if v.MapIndex > 0 then
		sceneLabel.text = self.m_FlagNameTable[v.MapIndex + 1]
	else
		sceneLabel.text = v.MapTemplateId == 0 and "" or PublicMap_PublicMap.GetData(v.MapTemplateId).Name
	end
	obj.transform:Find("LabelFlag"):GetComponent(typeof(UILabel)).text = self.m_FlagNameTable[v.BattleGroundFlag + 1]
	obj.transform:Find("LabelType"):GetComponent(typeof(UILabel)).text = self.m_TypeNameTable[v.MercenaryType + 1]
	local classSprite = obj.transform:Find("SpriteClass"):GetComponent(typeof(UISprite))
	classSprite.spriteName = Profession.GetIconByNumber(v.Class)
	Extensions.SetLocalPositionZ(classSprite.transform, v.IsOnline > 0 and 0 or -1)
	obj:GetComponent(typeof(UISprite)).spriteName = index % 2 == 0 and Constants.GreyItemBgSpriteName or Constants.WhiteItemBgSpriteName
	UIEventListener.Get(obj.transform:Find("LabelFlag/Bg").gameObject).onClick = LuaUtils.VoidDelegate(function (go)
		self:OnFlagClick(go, v.PlayerId)
	end)
	UIEventListener.Get(obj.transform:Find("LabelType/Bg").gameObject).onClick = LuaUtils.VoidDelegate(function (go)
		self:OnTypeClick(go, v.PlayerId)
	end)
	self.m_PlayerId2ObjTable[v.PlayerId] = obj
end

function CLuaCityWarSoldierWnd:OnFlagClick(go, playerId)
	local cnt = {"", 0, 0, 0}
	for k, v in pairs(self.m_DataTable) do
		if v.BattleGroundFlag ~= 0 then
			cnt[v.BattleGroundFlag + 1] = cnt[v.BattleGroundFlag + 1] + 1
		end
	end
	local popupMenuItemTable = {}
	-- 最多只显示两个战场的选择，上个版本设置成战场三的依然显示"战场三"
	for i = 1, 3 do
		local title = self.m_FlagNameTable[i]
		if i ~= 1 then title = title.."("..cnt[i].."/50)" end
		table.insert(popupMenuItemTable, PopupMenuItemData(title, DelegateFactory.Action_int(function (index)
			self:OnFlagSelect(index, playerId)
		end), false, nil, EnumPopupMenuItemStyle.Light))
	end
	local popupMenuItemArray = Table2Array(popupMenuItemTable, MakeArrayClass(PopupMenuItemData))
	CPopupMenuInfoMgr.ShowPopupMenu(popupMenuItemArray, go.transform, AlignType3.Bottom, 1, DelegateFactory.Action(function ()
			self:SetExpandedFlag(go, false)
		end), 600, true, 296)
	self:SetExpandedFlag(go, true)
end

function CLuaCityWarSoldierWnd:SetExpandedFlag(go, bExpanded)
	Extensions.SetLocalRotationZ(go.transform.parent:Find("Flag"), bExpanded and 270 or 90)
end

function CLuaCityWarSoldierWnd:OnFlagSelect(index, playerId)
	if CClientMainPlayer.Inst ~= nil then
        Gac2Gas.RequestOperationInGuild(CClientMainPlayer.Inst.BasicProp.GuildId, "SetCityWarBattleGroundFlag", tostring(playerId), tostring(index))
    end
end

function CLuaCityWarSoldierWnd:OnTypeClick(go, playerId)
	local a, b, c = string.match(Guild_Setting.GetData().CityWarMercenaryTypeLimit, "1,(%d+);2,(%d+);3,(%d+);")
	local total = {"", a, b, c}
	local cnt = {"", 0, 0, 0}
	for k, v in pairs(self.m_DataTable) do
		if v.MercenaryType ~= 0 then
			cnt[v.MercenaryType + 1] = cnt[v.MercenaryType + 1] + 1
		end
	end
	local popupMenuItemTable = {}
	for i = 1, #self.m_TypeNameTable do
		local title = self.m_TypeNameTable[i]
		if i ~= 1 then title = title.."("..cnt[i].."/"..total[i]..")" end
		table.insert(popupMenuItemTable, PopupMenuItemData(title, DelegateFactory.Action_int(function (index)
			self:OnTypeSelect(index, playerId)
		end), false, nil, EnumPopupMenuItemStyle.Light))
	end
	local popupMenuItemArray = Table2Array(popupMenuItemTable, MakeArrayClass(PopupMenuItemData))
	CPopupMenuInfoMgr.ShowPopupMenu(popupMenuItemArray, go.transform, AlignType3.Bottom, 1, DelegateFactory.Action(function ()
			self:SetExpandedFlag(go, false)
		end), 600, true, 296)
	self:SetExpandedFlag(go, true)
end

function CLuaCityWarSoldierWnd:OnTypeSelect(index, playerId)
	if CClientMainPlayer.Inst ~= nil then
        Gac2Gas.RequestOperationInGuild(CClientMainPlayer.Inst.BasicProp.GuildId, "SetCityWarMercenaryType", tostring(playerId), tostring(index))
    end
end

function CLuaCityWarSoldierWnd:Init( ... )

end

function CLuaCityWarSoldierWnd:SendGuildInfo_GuildMercenaryInfo( ... )
	self.m_DataTable = {}
	local destPlayer = nil
	for k, v in pairs(CLuaGuildMgr.m_MercenaryInfo) do
		if v.PlayerId == CLuaCityWarSoldierWnd.FistPlayerId then
			destPlayer = v
		else
			table.insert(self.m_DataTable, v)
		end
	end
	if destPlayer then table.insert(self.m_DataTable, 1, destPlayer) end
	self:ChooseSortBtn(self.m_SortBtnObjTable[1])
end

function CLuaCityWarSoldierWnd:SetFlagSucceed(result)
	local playerid, flag = string.match(result, "(%d+),(%d+)")
	local obj = self.m_PlayerId2ObjTable[tonumber(playerid)]
	if obj then
		obj.transform:Find("LabelFlag"):GetComponent(typeof(UILabel)).text = self.m_FlagNameTable[tonumber(flag) + 1]
	end
	playerid = tonumber(playerid)
	if CLuaGuildMgr.m_MercenaryInfo[playerid] then CLuaGuildMgr.m_MercenaryInfo[playerid].BattleGroundFlag = flag end
end

function CLuaCityWarSoldierWnd:SetTypeSucceed(result)
	local playerid, flag = string.match(result, "(%d+),(%d+)")
	local obj = self.m_PlayerId2ObjTable[tonumber(playerid)]
	if obj then
		obj.transform:Find("LabelType"):GetComponent(typeof(UILabel)).text = self.m_TypeNameTable[tonumber(flag) + 1]
	end
	playerid = tonumber(playerid)
	if CLuaGuildMgr.m_MercenaryInfo[playerid] then CLuaGuildMgr.m_MercenaryInfo[playerid].MercenaryType = flag end
end

function CLuaCityWarSoldierWnd:ClearSucceed( ... )
	for k, v in pairs(CLuaGuildMgr.m_MercenaryInfo) do
		v.BattleGroundFlag = 0
		v.MercenaryType = 0
	end
	self:RefreshData(1)
end

function CLuaCityWarSoldierWnd:RequestOperationInGuildSucceed(argv)
	if argv[0] == "SetCityWarMercenaryType" then
		self:SetTypeSucceed(argv[1])
	elseif argv[0] == "SetCityWarBattleGroundFlag" then
		self:SetFlagSucceed(argv[1])
	elseif argv[0] == "ClearCityWarAppointInfo" then
		self:ClearSucceed()
	end
end

function CLuaCityWarSoldierWnd:OnEnable( ... )
	g_ScriptEvent:AddListener("SendGuildInfo_GuildMercenaryInfo", self, "SendGuildInfo_GuildMercenaryInfo")
	g_ScriptEvent:AddListener("RequestOperationInGuildSucceed", self, "RequestOperationInGuildSucceed")
end

function CLuaCityWarSoldierWnd:OnDisable( ... )
	g_ScriptEvent:RemoveListener("SendGuildInfo_GuildMercenaryInfo", self, "SendGuildInfo_GuildMercenaryInfo")
	g_ScriptEvent:RemoveListener("RequestOperationInGuildSucceed", self, "RequestOperationInGuildSucceed")
end
