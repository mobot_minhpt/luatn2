local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"

LuaDoubleOneBaotuanTipWnd = class()
RegistChildComponent(LuaDoubleOneBaotuanTipWnd,"closeBtn", GameObject)

RegistClassMember(LuaDoubleOneBaotuanTipWnd, "maxChooseNum")

function LuaDoubleOneBaotuanTipWnd:Close()
	CUIManager.CloseUI("DoubleOneBaotuanTipWnd")
end

function LuaDoubleOneBaotuanTipWnd:OnEnable()
	--g_ScriptEvent:AddListener("", self, "")
end

function LuaDoubleOneBaotuanTipWnd:OnDisable()
	--g_ScriptEvent:RemoveListener("", self, "")
end

function LuaDoubleOneBaotuanTipWnd:Init()
	local onCloseClick = function(go)
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.closeBtn,DelegateFactory.Action_GameObject(onCloseClick),false)
end

return LuaDoubleOneBaotuanTipWnd
