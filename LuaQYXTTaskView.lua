local GameObject		    = import "UnityEngine.GameObject"
local UILabel				= import "UILabel"
local DelegateFactory		= import "DelegateFactory"
local CScene                = import "L10.Game.CScene"
local CGamePlayMgr          = import "L10.Game.CGamePlayMgr"
local CServerTimeMgr        = import "L10.Game.CServerTimeMgr"

CLuaQYXTTaskView = class()

RegistChildComponent(CLuaQYXTTaskView, "TaskName",		UILabel)
RegistChildComponent(CLuaQYXTTaskView, "CountDownLabel",UILabel)
RegistChildComponent(CLuaQYXTTaskView, "TaskDesc",      UILabel)
RegistChildComponent(CLuaQYXTTaskView, "LeaveBtn",		GameObject)
RegistChildComponent(CLuaQYXTTaskView, "SituationBtn",  GameObject)

RegistClassMember(CLuaQYXTTaskView, "m_RoundEndTime")
RegistClassMember(CLuaQYXTTaskView, "m_Round")
RegistClassMember(CLuaQYXTTaskView, "m_PrepareText")

function CLuaQYXTTaskView:Awake()
    self.TaskName.text = LocalString.GetString("[情人节]情意相通")
    self.CountDownLabel.text = nil
    self.TaskDesc.text = nil
    --self.m_RoundEndTime = CLuaQingMing2020Mgr.RoundEndTime
    self.m_Round = 0
    self.m_PrepareText = "00:00"
    
    UIEventListener.Get(self.LeaveBtn).onClick  =DelegateFactory.VoidDelegate(function(go)
        CGamePlayMgr.Inst:LeavePlay()
    end)
    UIEventListener.Get(self.SituationBtn).onClick  =DelegateFactory.VoidDelegate(function(go)
        if CLuaQYXTMgr.IsInPlay() then
            Gac2Gas.RequestQYXTBattleInfo()
        elseif CLuaQingMing2020Mgr.IsInPlay() then
            Gac2Gas.QingMing2020PvpQueryPlayInfo()
        elseif CLuaQingMing2020Mgr.IsInPvePlay() then
            g_MessageMgr:ShowMessage("QingMing2020_JZXC_Rule")
        end
    end)

end

function CLuaQYXTTaskView:Init()
    self.SituationBtn:SetActive(true)
    local btnLabel2 = self.SituationBtn.transform:Find("Label"):GetComponent(typeof(UILabel))
    btnLabel2.text = LocalString.GetString("战况")
    if CLuaQYXTMgr.IsInPlay() then
        self:OnRefreshQYXTTaskView(CLuaQYXTMgr.curSatge)
    elseif CLuaQingMing2020Mgr.IsInPlay() then
        self.TaskName.text = LocalString.GetString("幻境迷踪")
        self.TaskDesc.text = QingMing2019_QingMing2020Pvp.GetData().GameplayDescription
    elseif CLuaQingMing2020Mgr.IsInPvePlay() then
        --self.SituationBtn:SetActive(false)
        btnLabel2.text = LocalString.GetString("规则")
        self.TaskName.text = LocalString.GetString("解字消愁")
        self.TaskDesc.text = QingMing2019_QingMing2020Pve.GetData().PveGameplayDescription
        local duration = QingMing2019_QingMing2020Pve.GetData().RoundDuration
        local min = math.floor(duration/60)
		local sec = math.floor(duration - min * 60)
		if sec < 10 then
			sec = '0'..sec
		end
		local s = min .. ':' .. sec
        self.m_PrepareText = s
    end
end

function CLuaQYXTTaskView:GetRemainTimeText(totalSeconds)
    if totalSeconds < 0 then
        totalSeconds = 0
    end
    if totalSeconds >= 3600 then
        return SafeStringFormat3("%02d:%02d:%02d", math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
    else
        return SafeStringFormat3("%02d:%02d", math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end

function CLuaQYXTTaskView:OnSceneRemainTimeUpdate(args)
    if CLuaQingMing2020Mgr.IsInPvePlay() then return end
    if CScene.MainScene then
        if CScene.MainScene.ShowTimeCountDown then
            self.CountDownLabel.text = SafeStringFormat3(LocalString.GetString("计时 [00FF00]%s[-]"), self:GetRemainTimeText(CScene.MainScene.ShowTime))
        else
            self.CountDownLabel.text = nil
        end
    else
        self.CountDownLabel.text = nil
    end
end

function CLuaQYXTTaskView:OnRefreshQYXTTaskView(stage)
    if stage == 1 then
        self.TaskDesc.text = Valentine2020_Setting.GetData().QYXTCaiHuaMsg
    elseif stage == 2 then
        self.TaskDesc.text = Valentine2020_Setting.GetData().QYXTDouQingMsg
    end
end

function CLuaQYXTTaskView:Update()
    if CLuaQingMing2020Mgr.RoundEndTime and CLuaQingMing2020Mgr.IsInPvePlay() then
		local restTime = CLuaQingMing2020Mgr.RoundEndTime - CServerTimeMgr.Inst.timeStamp
		if restTime <= 0 then
            self.CountDownLabel.text = SafeStringFormat3(LocalString.GetString("计时 [00FF00]%s[-]"),self.m_PrepareText)
		else
			local min = math.floor(restTime/60)
			local sec = math.floor(restTime - min * 60)
			if sec < 10 then
				sec = '0'..sec
			end
			local s = min .. ':' .. sec
			self.CountDownLabel.text = SafeStringFormat3(LocalString.GetString("计时 [00FF00]%s[-]"),s)
		end
	end
end

function CLuaQYXTTaskView:OnEnable()
    g_ScriptEvent:AddListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
    g_ScriptEvent:AddListener("RefreshQYXTTaskView", self, "OnRefreshQYXTTaskView")
end

function CLuaQYXTTaskView:OnDisable()
    g_ScriptEvent:RemoveListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
    g_ScriptEvent:RemoveListener("RefreshQYXTTaskView", self, "OnRefreshQYXTTaskView")
end
