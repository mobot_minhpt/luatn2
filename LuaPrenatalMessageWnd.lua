require("common/common_include")

local UIProgressBar = import "UIProgressBar"
local QnCheckBox = import "L10.UI.QnCheckBox"
local UIGrid = import "UIGrid"
local MessageMgr = import "L10.Game.MessageMgr"
--local Baby_Talk = import "L10.Game.Baby_Talk"
local Object = import "System.Object"
local Baby_Setting = import "L10.Game.Baby_Setting"

LuaPrenatalMessageWnd = class()

RegistClassMember(LuaPrenatalMessageWnd, "TimeProgressBar")
RegistClassMember(LuaPrenatalMessageWnd, "TimeCountDownLabel")
RegistClassMember(LuaPrenatalMessageWnd, "MessageGrid")
RegistClassMember(LuaPrenatalMessageWnd, "MessageTemplate")
RegistClassMember(LuaPrenatalMessageWnd, "TellBtn")
RegistClassMember(LuaPrenatalMessageWnd, "HintLabel")

RegistClassMember(LuaPrenatalMessageWnd, "ChooseTotalTime")
RegistClassMember(LuaPrenatalMessageWnd, "WarningTime")
RegistClassMember(LuaPrenatalMessageWnd, "CurrentTime")
RegistClassMember(LuaPrenatalMessageWnd, "TimeTick")
RegistClassMember(LuaPrenatalMessageWnd, "MessageNum")

RegistClassMember(LuaPrenatalMessageWnd, "SelectedMessages")



function LuaPrenatalMessageWnd:Init()
	self:InitClassMembers()
	self:InitValues()

end

function LuaPrenatalMessageWnd:InitClassMembers()
	self.TimeProgressBar =  self.transform:Find("Anchor/Header/TimeProgress"):GetComponent(typeof(UIProgressBar))
	self.TimeCountDownLabel = self.transform:Find("Anchor/Header/TimeCountDownLabel"):GetComponent(typeof(UILabel))

	self.MessageGrid = self.transform:Find("Anchor/Messages/MessagePanel/MessageGrid"):GetComponent(typeof(UIGrid))
	self.MessageTemplate = self.transform:Find("Anchor/Messages/MessageTemplate").gameObject
	self.MessageTemplate:SetActive(false)

	self.TellBtn = self.transform:Find("Anchor/TellBtn").gameObject
	self.HintLabel = self.transform:Find("Anchor/HintLabel"):GetComponent(typeof(UILabel))
end

function LuaPrenatalMessageWnd:InitValues()

	local setting = Baby_Setting.GetData()
	self.MessageNum = setting.SelectDialogCount
	self.HintLabel.text = SafeStringFormat3(LocalString.GetString("请选择%s句"), tostring(setting.SelectDialogCount))

	self.ChooseTotalTime = 30
	self.WarningTime = 10
	self.CurrentTime = self.ChooseTotalTime
	self.TimeProgressBar.value = 1

	if self.TimeTick ~= nil then
      UnRegisterTick(self.TimeTick)
      self.TimeTick=nil
    end

   self:UpdateMessages()

	self.TimeTick = RegisterTickWithDuration(function ()
		if self.CurrentTime > 0 then
			self.CurrentTime = self.CurrentTime - 1
			self.TimeCountDownLabel.text = SafeStringFormat3(LocalString.GetString("%ds"), self.CurrentTime)
			self.TimeProgressBar.value = self.CurrentTime / self.ChooseTotalTime

			if self.CurrentTime == self.WarningTime then
				-- 变红 common_hpandmp_hp
				local Foreground = self.TimeProgressBar.transform:Find("Foreground"):GetComponent(typeof(UISprite))
				Foreground.spriteName = "common_hpandmp_hp"
				-- 提示
				MessageMgr.Inst:ShowMessage("CHOOSE_MESSAGE_BEFORE_PRODUCTION", {})
			end
		else
			CUIManager.CloseUI(CLuaUIResources.PrenatalMessageWnd)
		end
	end, 1000, 1000 * (self.ChooseTotalTime+1))

	local onTellBtnClicked = function (go)
		self:OnTellBtnClicked(go)
	end
	CommonDefs.AddOnClickListener(self.TellBtn, DelegateFactory.Action_GameObject(onTellBtnClicked), false)


end

function LuaPrenatalMessageWnd:UpdateMessages()

	self.SelectedMessages = {}
	CUICommonDef.ClearTransform(self.MessageGrid.transform)

	Baby_Talk.ForeachKey(function (key) 
        local data = Baby_Talk.GetData(key)
        local msg = g_MessageMgr:FormatMessage(data.MessageTip)

        local go = NGUITools.AddChild(self.MessageGrid.gameObject, self.MessageTemplate)
        self:InitItem(go, key, msg)
        go:SetActive(true)

    end)

	self.MessageGrid:Reposition()
end

function LuaPrenatalMessageWnd:InitItem(go, key, msg)
	local CheckBox =  go.transform:Find("QnCheckBox"):GetComponent(typeof(QnCheckBox))
	if not CheckBox then return end
	CheckBox.Text = msg
	CheckBox.Selected = false
	CheckBox.OnValueChanged = DelegateFactory.Action_bool(function(value) self:OnMessageCheckBoxValueChanged(CheckBox, key, value) end)

end

function LuaPrenatalMessageWnd:OnMessageCheckBoxValueChanged(checkBox, key, value)
	if value then
		if self:GetSelectedMessageCount() >= self.MessageNum then
			checkBox:SetSelected(false, true)
			MessageMgr.Inst:ShowMessage("ENOUGH_PRENATAL_MESSAGE", {})
			return
		end
	end
	self.SelectedMessages[key] = value
end

-- 获取已经选择的信息数量
function LuaPrenatalMessageWnd:GetSelectedMessageCount()
	local count = 0
	for k, v in pairs(self.SelectedMessages) do
		if v then
			count = count + 1
		end
	end
	return count
end

function LuaPrenatalMessageWnd:OnTellBtnClicked(go)
	local count = self:GetSelectedMessageCount()
	if count < self.MessageNum then
		MessageMgr.Inst:ShowMessage("PRENATAL_MESSAGE_NOT_ENOUGH", {})
		return
	end

	local selectedList = CreateFromClass(MakeGenericClass(List, Object))
	for k, v in pairs(self.SelectedMessages) do
		if v then
			CommonDefs.ListAdd(selectedList, typeof(Int32), k)
		end
	end
	Gac2Gas.FinishSelectDialog(MsgPackImpl.pack(selectedList))
	CUIManager.CloseUI(CLuaUIResources.PrenatalMessageWnd)
end

function LuaPrenatalMessageWnd:OnEnable()
	--g_ScriptEvent:AddListener("UpdateBirthPermissionResult", self, "UpdatePermissionResult")
end

function LuaPrenatalMessageWnd:OnDisable()
	--g_ScriptEvent:RemoveListener("UpdateBirthPermissionResult", self, "UpdatePermissionResult")
	if self.TimeTick ~= nil then
      UnRegisterTick(self.TimeTick)
      self.TimeTick=nil
    end
end



return LuaPrenatalMessageWnd