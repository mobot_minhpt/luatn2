-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"
local CXialvPKMgr = import "L10.Game.CXialvPKMgr"
local CXialvPKTempSkillRightView = import "L10.UI.CXialvPKTempSkillRightView"
local Gac2Gas = import "L10.Game.Gac2Gas"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CXialvPKTempSkillRightView.m_Awake_CS2LuaHook = function (this)    if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.IsCrossServerPlayer then
        Gac2Gas.CrossXiaLvPkQueryTempSkill()
    else
        Gac2Gas.XiaLvPkQueryTempSkill()
    end
end
CXialvPKTempSkillRightView.m_Start_CS2LuaHook = function (this) 
    this:LoadLearnedSkills()
    UIEventListener.Get(this.m_MoreInfo.gameObject).onClick = MakeDelegateFromCSFunction(this.OnMoreInfoClicked, VoidDelegate, this)

    do
        local i = 0
        while i < this.m_TempSkills.Length do
            UIEventListener.Get(this.m_TempSkills[i].gameObject).onClick = MakeDelegateFromCSFunction(this.OnLearnedSkillClicked, VoidDelegate, this)
            i = i + 1
        end
    end
end
CXialvPKTempSkillRightView.m_LoadLearnedSkills_CS2LuaHook = function (this) 
    do
        local i = 0
        while i < this.m_TempSkills.Length do
            local skill = Skill_AllSkills.GetData(this.learnedSkill[i])
            if skill ~= nil then
                this.m_TempSkills[i]:LoadSkillIcon(skill.SkillIcon)
                this.m_TempSkillLevels[i].gameObject:SetActive(true)
                this.m_TempSkillLevels[i].text = System.String.Format("Lv. {0}", CLuaDesignMgr.Skill_AllSkills_GetLevel(this.learnedSkill[i]))
                if CXialvPKMgr.Inst:IsColorSystemSkill(CLuaDesignMgr.Skill_AllSkills_GetClass(this.learnedSkill[i])) or CXialvPKMgr.Inst:IsColorSystemBaseSkill(CLuaDesignMgr.Skill_AllSkills_GetClass(this.learnedSkill[i])) then
                    this.m_TempSkillColors[i].gameObject:SetActive(true)
                    this.m_TempSkillColors[i].spriteName = CXialvPKMgr.Inst:GetColorSystemSkillIcon(CLuaDesignMgr.Skill_AllSkills_GetClass(this.learnedSkill[i]))
                else
                    this.m_TempSkillColors[i].gameObject:SetActive(false)
                end
            else
                this.m_TempSkills[i]:Clear()
                this.m_TempSkillLevels[i].gameObject:SetActive(false)
                this.m_TempSkillColors[i].gameObject:SetActive(false)
            end
            i = i + 1
        end
    end
end
CXialvPKTempSkillRightView.m_ToLearnSkill_CS2LuaHook = function (this, srcSkillId, go, srcActiveSkillIndex) 
    do
        local i = 0
        while i < this.m_TempSkills.Length do
            if this.m_TempSkills[i].gameObject:Equals(go) then
                local skill = Skill_AllSkills.GetData(srcSkillId)
                if skill ~= nil then
                    if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.IsCrossServerPlayer then
                        Gac2Gas.CrossXiaLvPkLearnTempSkill((i + 1), CLuaDesignMgr.Skill_AllSkills_GetClass(srcSkillId))
                    else
                        Gac2Gas.XiaLvPkLearnTempSkill((i + 1), CLuaDesignMgr.Skill_AllSkills_GetClass(srcSkillId))
                    end
                end
            end
            i = i + 1
        end
    end
end
CXialvPKTempSkillRightView.m_OnXiaLvPkQueryTempSkillResult_CS2LuaHook = function (this, learned) 

    do
        local i = 0
        while i < this.learnedSkill.Length do
            if i < learned.Count then
                this.learnedSkill[i] = learned[i]
            else
                this.learnedSkill[i] = System.UInt32.MaxValue
            end
            i = i + 1
        end
    end
    this:LoadLearnedSkills()
end
CXialvPKTempSkillRightView.m_OnLearnedSkillClicked_CS2LuaHook = function (this, go) 
    do
        local i = 0
        while i < this.m_TempSkills.Length do
            if this.m_TempSkills[i].gameObject == go and this.learnedSkill[i] ~= System.UInt32.MaxValue then
                CSkillInfoMgr.ShowSkillInfoWnd(this.learnedSkill[i], this.m_TipTopPos.transform.position, CSkillInfoMgr.EnumSkillInfoContext.XiaLvSelectedSkill, true, 0, 0, nil)
                break
            end
            i = i + 1
        end
    end
end
CXialvPKTempSkillRightView.m_OnUpdateSelectedColorSkill_CS2LuaHook = function (this, cls, index) 
    do
        local i = 0
        while i < this.learnedSkill.Length do
            if CLuaDesignMgr.Skill_AllSkills_GetClass(this.learnedSkill[i]) == cls then
                local newSkillID = CXialvPKMgr.Inst:GetReplaceColorSystemSkillCls(cls, index)
                if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.IsCrossServerPlayer then
                    Gac2Gas.CrossXiaLvPkLearnTempSkill((i + 1), newSkillID)
                else
                    Gac2Gas.XiaLvPkLearnTempSkill((i + 1), newSkillID)
                end
                CSkillInfoMgr.CloseSkillInfoWnd()
            end
            i = i + 1
        end
    end
end
