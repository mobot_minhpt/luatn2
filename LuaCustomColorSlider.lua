local DelegateFactory  = import "DelegateFactory"
local QnTipButton = import "L10.UI.QnTipButton"
local GameObject = import "UnityEngine.GameObject"
local UITexture = import "UITexture"
local UIGrid = import "UIGrid"
local Texture2D = import "UnityEngine.Texture2D"
local TextureFormat = import "UnityEngine.TextureFormat"
local ColorExt = import "L10.Game.ColorExt"

LuaCustomColorSlider = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaCustomColorSlider, "OptionQnTipButton", "OptionQnTipButton", QnTipButton)
RegistChildComponent(LuaCustomColorSlider, "OptionTemplate", "OptionTemplate", GameObject)
RegistChildComponent(LuaCustomColorSlider, "OneDimenColorTex", "OneDimenColorTex", UITexture)
RegistChildComponent(LuaCustomColorSlider, "TwoDimenColorTex", "TwoDimenColorTex", UITexture)
RegistChildComponent(LuaCustomColorSlider, "OneDimenThumb", "OneDimenThumb", GameObject)
RegistChildComponent(LuaCustomColorSlider, "TwoDimenThumb", "TwoDimenThumb", GameObject)
RegistChildComponent(LuaCustomColorSlider, "OptionViewGrid", "OptionViewGrid", UIGrid)
RegistChildComponent(LuaCustomColorSlider, "OptionBg", "OptionBg", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaCustomColorSlider, "m_TexWidth")
RegistClassMember(LuaCustomColorSlider, "m_TexHeight")
RegistClassMember(LuaCustomColorSlider, "m_TexWidth2")
RegistClassMember(LuaCustomColorSlider, "m_TexHeight2")
RegistClassMember(LuaCustomColorSlider, "m_OneDimenTex")
RegistClassMember(LuaCustomColorSlider, "m_TwoDimenTex")
RegistClassMember(LuaCustomColorSlider, "m_IsOneDimenDragging")
RegistClassMember(LuaCustomColorSlider, "m_IsTwoDimenDragging")
RegistClassMember(LuaCustomColorSlider, "m_OnSelectColorAction")
RegistClassMember(LuaCustomColorSlider, "m_OnStartSelectAction")
RegistClassMember(LuaCustomColorSlider, "m_OnEndSelectAction")
RegistClassMember(LuaCustomColorSlider, "m_MinHValue")
RegistClassMember(LuaCustomColorSlider, "m_MaxHValue")
RegistClassMember(LuaCustomColorSlider, "m_MinSValue")
RegistClassMember(LuaCustomColorSlider, "m_MaxSValue")
RegistClassMember(LuaCustomColorSlider, "m_MinVValue")
RegistClassMember(LuaCustomColorSlider, "m_MaxVValue")
RegistClassMember(LuaCustomColorSlider, "m_HValue")
RegistClassMember(LuaCustomColorSlider, "m_SValue")
RegistClassMember(LuaCustomColorSlider, "m_VValue")
RegistClassMember(LuaCustomColorSlider, "m_OptionTextList")
RegistClassMember(LuaCustomColorSlider, "m_OptionObjList")
RegistClassMember(LuaCustomColorSlider, "m_OnSelectOptionAction")
RegistClassMember(LuaCustomColorSlider, "m_OptionSelectIndex")
RegistClassMember(LuaCustomColorSlider, "m_IsIgnoreCallback")
RegistClassMember(LuaCustomColorSlider, "m_UseRealRangeSV")

function LuaCustomColorSlider:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.OneDimenColorTex.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOneDimenColorTexClick()
	end)


	
	UIEventListener.Get(self.TwoDimenColorTex.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTwoDimenColorTexClick()
	end)


	
	UIEventListener.Get(self.OptionBg.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOptionBgClick()
	end)


    --@endregion EventBind end

    UIEventListener.Get(self.OneDimenThumb).onDragStart = DelegateFactory.VoidDelegate(function(go)
        self:OnDragOneDimenThumbStart(go)
    end)
    
    UIEventListener.Get(self.OneDimenThumb).onDragEnd = DelegateFactory.VoidDelegate(function(go)
        self:OnDragOneDimenThumbEnd(go)
    end)

    UIEventListener.Get(self.TwoDimenThumb).onDragStart = DelegateFactory.VoidDelegate(function(go)
        self:OnDragTwoDimenThumbStart(go)
    end)
    
    UIEventListener.Get(self.TwoDimenThumb).onDragEnd = DelegateFactory.VoidDelegate(function(go)
        self:OnDragTwoDimenThumbEnd(go)
    end)

    self.OptionQnTipButton.OnClick = DelegateFactory.Action_QnButton(function (button)
        self:OnOptionQnTipButtonClick()
    end)
end

function LuaCustomColorSlider:SetHSV(h,s,v,ignoreCallback)
    local minH, maxH = h, h
    while(maxH < self.m_MinHValue) do
        maxH = maxH + 1
    end
    while(minH > self.m_MaxHValue) do
        minH = minH - 1
    end
    if maxH <= self.m_MaxHValue then
        h = maxH
    elseif minH >= self.m_MinHValue then
        h = minH
    elseif minH < self.m_MinHValue and maxH > self.m_MaxHValue then
        local absMin = math.abs(minH - self.m_MinHValue)
        local absMax = math.abs(maxH - self.m_MaxHValue)
        if absMin < absMax then
            h = self.m_MinHValue
        else
            h = self.m_MaxHValue
        end
    end
    self.m_IsIgnoreCallback = ignoreCallback
    if not self.m_HValue or math.abs(self.m_HValue - h) ~= 1 then
        self.m_HValue = h
    end
    local smin, smax, vmin, vmax = self.m_MinSValue, self.m_MaxSValue, self.m_MinVValue, self.m_MaxVValue
    self.m_SValue = math.min(smax, math.max(smin, s))
    self.m_VValue = math.min(vmax, math.max(vmin, v))
    self:OnSelectOneDimenColor(self.m_HValue, true)
    self.m_IsIgnoreCallback = false
end

function LuaCustomColorSlider:Init(hmin, hmax, smin, smax, vmin, vmax, customSVValue, onSelectColorAction, onStartSelectAction, onEndSelectAction)
    print("LuaCustomColorSlider:Init", hmin, hmax, smin, smax, vmin, vmax, customSVValue)
    self.m_MinHValue, self.m_MaxHValue, self.m_MinSValue, self.m_MaxSValue, self.m_MinVValue, self.m_MaxVValue = hmin,  hmax, smin, smax, vmin, vmax
    self:CreateCommmonTex()
    self.TwoDimenColorTex.gameObject:SetActive(customSVValue)
    self.TwoDimenThumb.transform.parent.gameObject:SetActive(customSVValue)
    self:InitOption(self.m_OptionTextList, self.m_OnSelectOptionAction)
    self.m_HValue, self.m_SValue, self.m_VValue = hmin, smax, vmax
    self:SetOneDimenTexColorSV(self.m_SValue, self.m_VValue)
    self:OnSelectOneDimenColor(self.m_HValue, true)
    self.m_OnSelectColorAction = onSelectColorAction
    self.m_OnStartSelectAction = onStartSelectAction
    self.m_OnEndSelectAction = onEndSelectAction
end

function LuaCustomColorSlider:InitOption(optionTextList, onSelectOptionAction)
    self.m_OptionTextList = optionTextList
    self.m_OnSelectOptionAction = onSelectOptionAction
    self.OptionQnTipButton.gameObject:SetActive(optionTextList ~= nil and #optionTextList > 1)
    self.m_OptionObjList = {}
    Extensions.RemoveAllChildren(self.OptionViewGrid.transform)
    if self.m_OptionTextList ~= nil and #self.m_OptionTextList > 1 then
        self.OptionQnTipButton.Text = self.m_OptionTextList[1]
        for i = 1, #self.m_OptionTextList do
            local text = self.m_OptionTextList[i]
            local obj = NGUITools.AddChild(self.OptionViewGrid.gameObject, self.OptionTemplate.gameObject)
            local label = obj.transform:Find("Label"):GetComponent(typeof(UILabel))
            label.text = text
            obj:SetActive(true)
            local index = i
            UIEventListener.Get(obj).onClick = DelegateFactory.VoidDelegate(function (go)
                self:OnSelectOption(index)
            end)
            table.insert(self.m_OptionObjList, obj)
        end
        self.OptionViewGrid:Reposition()
        self:OnSelectOption(1)
    end
end

function LuaCustomColorSlider:SetTwoDimenTexPixelsType(useRealRangeSV)
    self.m_UseRealRangeSV = useRealRangeSV
    self:SetTwoDimenTexPixels()
end

function LuaCustomColorSlider:Start()
    self.OptionViewGrid.gameObject:SetActive(false)
    self.OptionQnTipButton:SetTipStatus(false)
    self.OptionTemplate.gameObject:SetActive(false)
    self.OptionBg.gameObject:SetActive(false)
end

function LuaCustomColorSlider:CreateCommmonTex()
    self.m_TexWidth, self.m_TexHeight = 32, 4
    self.m_TexWidth2, self.m_TexHeight2 = 32, 8

    if self.m_OneDimenTex == nil then
        self.m_OneDimenTex = Texture2D(self.m_TexWidth, self.m_TexHeight, TextureFormat.RGBA32, false)
        self.OneDimenColorTex.mainTexture = self.m_OneDimenTex
    end

    if self.m_TwoDimenTex == nil then
        self.m_TwoDimenTex = Texture2D(self.m_TexWidth2, self.m_TexHeight2, TextureFormat.RGBA32, false)
        self.TwoDimenColorTex.mainTexture = self.m_TwoDimenTex
    end
end

function LuaCustomColorSlider:OnDestroy()
    if self.m_OneDimenTex ~= nil then
        GameObject.Destroy(self.m_OneDimenTex)
        self.m_OneDimenTex = nil
    end
    if self.m_TwoDimenTex ~= nil then
        GameObject.Destroy(self.m_TwoDimenTex)
        self.m_TwoDimenTex = nil
    end
end

function LuaCustomColorSlider:Update()
    if self.m_IsOneDimenDragging or self.m_IsTwoDimenDragging then
        local screenPos = Input.mousePosition
        if CommonDefs.IsInMobileDevice() then
            screenPos = Input.GetTouch(0).position
            screenPos = Vector3(screenPos.x, screenPos.y, 0)
        end

        if self.m_IsOneDimenDragging then
            self:SetThumbScreenPos(self.OneDimenThumb,screenPos)
        end
        if self.m_IsTwoDimenDragging then
            self:SetThumbScreenPos(self.TwoDimenThumb,screenPos)
        end
    end
end

function LuaCustomColorSlider:SetThumbScreenPos(thumb, screenPos)
    local worldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(screenPos)
    local relLocalPos = thumb.transform.parent:InverseTransformPoint(worldPos)
   
    if thumb == self.OneDimenThumb then
        local width = self.OneDimenColorTex.width
        local halfWidth = self.OneDimenColorTex.width * 0.5
        if relLocalPos.x < -halfWidth * 1.2 or relLocalPos.x > halfWidth * 1.2 then
            self.m_IsOneDimenDragging = false
        end
        local halfHeight = self.OneDimenColorTex.height * 0.5
        if relLocalPos.y < -halfHeight * 1.2 or relLocalPos.y > halfHeight * 1.2 then
            self.m_IsOneDimenDragging = false
        end
        local x = (relLocalPos.x + halfWidth) / width
        x = math.max(0, math.min(1,x))
        local h = math.lerp(self.m_MinHValue , self.m_MaxHValue, x)
        self:OnSelectOneDimenColor(h, false)
        if self.m_OnSelectColorAction then
            self.m_OnSelectColorAction(Vector3(h, self.m_SValue, self.m_VValue))
        end
    else
        local width = self.TwoDimenColorTex.width
        local halfWidth = self.TwoDimenColorTex.width * 0.5
        if relLocalPos.x < -halfWidth * 1.2 or relLocalPos.x > halfWidth * 1.2 then
            self.m_IsTwoDimenDragging = false
        end
        local height = self.TwoDimenColorTex.height
        local halfHeight = self.TwoDimenColorTex.height * 0.5
        if relLocalPos.y < -halfHeight * 1.2 or relLocalPos.y > halfHeight * 1.2 then
            self.m_IsTwoDimenDragging = false
        end
        local x = (relLocalPos.x + halfWidth) / width
        local y = (relLocalPos.y + halfHeight) / height
        self:SetTwoDimenThumb(self.m_HValue, x, y)
    end
end

function LuaCustomColorSlider:SetOneDimenTexColorSV(s, v)
    for x = 0, self.m_TexWidth - 1 do
        local h = math.lerp(self.m_MinHValue , self.m_MaxHValue, x / self.m_TexWidth)
        local color = ColorExt.HSV2RGB(h, 1, 1)
        for y = 0, self.m_TexHeight do
            self.m_OneDimenTex:SetPixel(x, y, color)
        end
    end
    self.m_OneDimenTex:Apply()
end

function LuaCustomColorSlider:SetOneDimenThumb(h)
    local width = self.OneDimenColorTex.width
    local x = (h - self.m_MinHValue) / (self.m_MaxHValue - self.m_MinHValue) 
    self.OneDimenThumb.transform.localPosition = Vector3(width * (x - 0.5), 0, 0)
end

function LuaCustomColorSlider:SetTwoDimenThumb(h, x, y)
    self.m_HValue = h
    x = math.min(1, math.max(0, x))
    y = math.min(1, math.max(0, y))
    self.m_SValue = math.lerp(self.m_MinSValue, self.m_MaxSValue, x)
    self.m_VValue = math.lerp(self.m_MinVValue, self.m_MaxVValue, y)
    local width = self.TwoDimenColorTex.width
    local height = self.TwoDimenColorTex.height
    self.TwoDimenThumb.transform.localPosition = Vector3(width * (x - 0.5), height * (y - 0.5), 0)
    if self.m_OnSelectColorAction and not self.m_IsIgnoreCallback then
        self.m_OnSelectColorAction(Vector3(h, self.m_SValue, self.m_VValue))
    end 
end

function LuaCustomColorSlider:GetCurThumbXY()
    local x = (self.m_SValue - self.m_MinSValue) / (self.m_MaxSValue - self.m_MinSValue)
    local y = (self.m_VValue - self.m_MinVValue) / (self.m_MaxVValue - self.m_MinVValue)
    return x, y
end

function LuaCustomColorSlider:SetTwoDimenTexPixels()
    local width2, height2 = self.m_TexWidth2, self.m_TexHeight2
    local smin, smax, vmin, vmax = 0, 1, 0, 1
    if self.m_UseRealRangeSV then
        smin, smax, vmin, vmax = self.m_MinSValue, self.m_MaxSValue, self.m_MinVValue, self.m_MaxVValue
    end
    for x = 0, width2 - 1 do
        for y = 0, height2 - 1 do
            local v1, v2 = x / width2, y / height2
            local s = math.lerp(smin, smax, v1)
            local v = math.lerp(vmin, vmax, v2)
            local color = ColorExt.HSV2RGB(self.m_HValue, s, v)
            self.m_TwoDimenTex:SetPixel(x, y, color)
        end
    end
    self.m_TwoDimenTex:Apply()
end
--@region UIEvent

function LuaCustomColorSlider:OnOptionBgClick()
    self.OptionViewGrid.gameObject:SetActive(false)
    self.OptionQnTipButton:SetTipStatus(false)
    self.OptionBg.gameObject:SetActive(false)
end

function LuaCustomColorSlider:OnOptionQnTipButtonClick()
    self.OptionViewGrid.gameObject:SetActive(true)
    self.OptionQnTipButton:SetTipStatus(true)
    self.OptionBg.gameObject:SetActive(true)
    self.OptionViewGrid:Reposition()
end

function LuaCustomColorSlider:OnOneDimenColorTexClick()
    if self.m_OnStartSelectAction then
        self.m_OnStartSelectAction(Vector3(self.m_HValue, self.m_SValue, self.m_VValue))
    end
    local screenPos = Input.mousePosition
    if CommonDefs.IsInMobileDevice() then
        screenPos = Input.GetTouch(0).position
        screenPos = Vector3(screenPos.x, screenPos.y, 0)
    end
    self:SetThumbScreenPos(self.OneDimenThumb,screenPos)
    self:SetTwoDimenTexPixels()
    if self.m_OnEndSelectAction then
        self.m_OnEndSelectAction(Vector3(self.m_HValue, self.m_SValue, self.m_VValue))
    end
end

function LuaCustomColorSlider:OnTwoDimenColorTexClick()
    if self.m_OnStartSelectAction then
        self.m_OnStartSelectAction(Vector3(self.m_HValue, self.m_SValue, self.m_VValue))
    end
    local screenPos = Input.mousePosition
    if CommonDefs.IsInMobileDevice() then
        screenPos = Input.GetTouch(0).position
        screenPos = Vector3(screenPos.x, screenPos.y, 0)
    end
    self:SetThumbScreenPos(self.TwoDimenThumb,screenPos)
    if self.m_OnEndSelectAction then
        self.m_OnEndSelectAction(Vector3(self.m_HValue, self.m_SValue, self.m_VValue))
    end
end
function LuaCustomColorSlider:OnDragOneDimenThumbStart(go)
    self.m_IsOneDimenDragging = true
    self.m_IsTwoDimenDragging = false
    if self.m_OnStartSelectAction then
        self.m_OnStartSelectAction(Vector3(self.m_HValue, self.m_SValue, self.m_VValue))
    end
end

function LuaCustomColorSlider:OnDragOneDimenThumbEnd(go)
    self.m_IsOneDimenDragging = false
    self:OnSelectOneDimenColor(self.m_HValue, true)
    if self.m_OnEndSelectAction then
        self.m_OnEndSelectAction(Vector3(self.m_HValue, self.m_SValue, self.m_VValue))
    end
end

function LuaCustomColorSlider:OnDragTwoDimenThumbStart(go)
    self.m_IsTwoDimenDragging = true
    self.m_IsOneDimenDragging = false
    if self.m_OnStartSelectAction then
        self.m_OnStartSelectAction(Vector3(self.m_HValue, self.m_SValue, self.m_VValue))
    end
end

function LuaCustomColorSlider:OnDragTwoDimenThumbEnd(go)
    self.m_IsTwoDimenDragging = false
    if self.m_OnEndSelectAction then
        self.m_OnEndSelectAction(Vector3(self.m_HValue, self.m_SValue, self.m_VValue))
    end
end

function LuaCustomColorSlider:OnSelectOneDimenColor(h, updateTx)
    self.m_HValue = h
    local minHValue = self.m_MinHValue
    local maxHValue = self.m_MaxHValue
    if h < minHValue and ((h + 1) >= minHValue) and ((h + 1) <= maxHValue) then
        h = h + 1
    elseif h > maxHValue  and ((h - 1) >= minHValue) and ((h - 1) <= maxHValue) then
        h = h - 1
    else
        h = math.min(maxHValue, math.max(minHValue, h))
    end
    self.m_HValue = h
    if updateTx then
        self:SetTwoDimenTexPixels()
        local x, y = self:GetCurThumbXY()
        self:SetTwoDimenThumb(h, x, y)
    end
    self:SetOneDimenThumb(h)
end

function LuaCustomColorSlider:OnSelectOption(index, ignoreCallback)
    if not self.m_OptionTextList then return end
    self.m_OptionSelectIndex = index
    self.OptionQnTipButton.Text = self.m_OptionTextList[index]
    self.OptionViewGrid.gameObject:SetActive(false)
    self.OptionQnTipButton:SetTipStatus(false)
    self.OptionBg.gameObject:SetActive(false)
    for i, obj in pairs(self.m_OptionObjList) do
        obj:SetActive(index ~= i)
    end
    if self.m_OnSelectOptionAction and not ignoreCallback then
        self.m_OnSelectOptionAction(index)
    end
    self.OptionViewGrid:Reposition()
end
--@endregion UIEvent

