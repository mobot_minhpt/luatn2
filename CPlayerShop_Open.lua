-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CPlayerShop_Open = import "L10.UI.CPlayerShop_Open"
local CPlayerShopData = import "L10.UI.CPlayerShopData"
local CPlayerShopMgr = import "L10.UI.CPlayerShopMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local DelegateFactory = import "DelegateFactory"
local Extensions = import "Extensions"
local MessageWndManager = import "L10.UI.MessageWndManager"
CPlayerShop_Open.m_OnOpenShopButtonClick_CS2LuaHook = function (this, button)
    local shopName = Extensions.RemoveBlank(this.m_ShopNameInput.Text)
    if CClientMainPlayer.Inst.MaxLevel < CPlayerShopMgr.Open_Shop_Level then
        g_MessageMgr:ShowMessage("PLAYERSHOP_GRADE_NOT_ENOUGH", CPlayerShopMgr.Open_Shop_Level)
    elseif CClientMainPlayer.Inst.FreeSilver + CClientMainPlayer.Inst.Silver < CPlayerShopData.s_OpenMoney then
        g_MessageMgr:ShowMessage("PLAYERSHOP_NOT_ENOUGH_YINLIANG_CHANGE_NAME", CPlayerShopData.s_OpenMoney)
    elseif System.String.IsNullOrEmpty(shopName) then
        g_MessageMgr:ShowMessage("PLAYERSHOP_NAME_NONE")
    elseif not CWordFilterMgr.Inst:CheckName(shopName, LocalString.GetString("玩家商店命名")) then
        g_MessageMgr:ShowMessage("Name_Violation")
    elseif CUICommonDef.GetStrByteLength(shopName) > CPlayerShopMgr.Shop_Name_Num_Max then
        g_MessageMgr:ShowMessage("PLAYERSHOP_INPUT_NAME_INTERFACE", CPlayerShopMgr.Shop_Name_Num_Max)
    else
        local msg = g_MessageMgr:FormatMessage("PLAYERSHOP_OPEN_CONFIRM", CPlayerShopData.s_OpenMoney)
        MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function () 
            Gac2Gas.RequestOpenPlayerShop(shopName)
        end), nil, nil, nil, false)
    end
end
