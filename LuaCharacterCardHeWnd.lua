local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local Color = import "UnityEngine.Color"
local DelegateFactory = import "DelegateFactory"
local CUIFx = import "L10.UI.CUIFx"
local CUITexture = import "L10.UI.CUITexture"
local CButton = import "L10.UI.CButton"
local CItemMgr = import "L10.Game.CItemMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local Animator = import "UnityEngine.Animator"

LuaCharacterCardHeWnd = class()
RegistChildComponent(LuaCharacterCardHeWnd,"closeBtn", GameObject)
RegistChildComponent(LuaCharacterCardHeWnd,"resNode", GameObject)
RegistChildComponent(LuaCharacterCardHeWnd,"heBtn", GameObject)
RegistChildComponent(LuaCharacterCardHeWnd,"he3Btn", GameObject)
RegistChildComponent(LuaCharacterCardHeWnd,"ShowRoot2", GameObject)
RegistChildComponent(LuaCharacterCardHeWnd,"subBtn", GameObject)
RegistChildComponent(LuaCharacterCardHeWnd,"node1", GameObject)
RegistChildComponent(LuaCharacterCardHeWnd,"node2", GameObject)
RegistChildComponent(LuaCharacterCardHeWnd,"node3", GameObject)
RegistChildComponent(LuaCharacterCardHeWnd,"btn1text", UILabel)
RegistChildComponent(LuaCharacterCardHeWnd,"btn2text", UILabel)

RegistClassMember(LuaCharacterCardHeWnd, "maxChooseNum")

function LuaCharacterCardHeWnd:Close()
	CUIManager.CloseUI(CLuaUIResources.CharacterCardHeWnd)
end

function LuaCharacterCardHeWnd:OnEnable()
	g_ScriptEvent:AddListener("CharacterCardHeUpdate", self, "UpdateNewInfo")
end

function LuaCharacterCardHeWnd:OnDisable()
	g_ScriptEvent:RemoveListener("CharacterCardHeUpdate", self, "UpdateNewInfo")
end

function LuaCharacterCardHeWnd:UpdateNewInfo(infoTable)
	self:Init()
	self.ShowRoot2:SetActive(true)
	local onClick = function(go)
		self.ShowRoot2:SetActive(false)
	end
	CommonDefs.AddOnClickListener(self.subBtn,DelegateFactory.Action_GameObject(onClick),false)
	local animator = self.gameObject:GetComponent(typeof(Animator))
	local fxNode = self.ShowRoot2.transform:Find('fxNode'):GetComponent(typeof(CUIFx))
	if infoTable then
		fxNode:DestroyFx()
		if #infoTable == 1 then
			animator:Play('CharacterCardHeWnd_HeCheng_01',0,0)
			fxNode:LoadFx("Fx/UI/Prefab/UI_CharacterCardHeWnd_HeCheng_01.prefab")
		elseif #infoTable == 3 then
			animator:Play('CharacterCardHeWnd_HeCheng_02',0,0)
			fxNode:LoadFx("Fx/UI/Prefab/UI_CharacterCardHeWnd_HeCheng_02.prefab")
		end
	end

	local nodeTable = {self.node2,self.node1,self.node3}
	for i,v in ipairs(nodeTable) do
		if infoTable[i] then
			v:SetActive(true)
			local data = RoleDrawingCard_CardItem2Idx.GetData(infoTable[i])
			v.transform:Find('bg'):GetComponent(typeof(CUITexture)):LoadMaterial(data.SmallPic)
		else
			v:SetActive(false)
		end
	end
end

function LuaCharacterCardHeWnd:InitNeedItem(go, itemId)
	local item = Item_Item.GetData(itemId)
	if not item then return end

	local IconTexture = go.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
	local DisableSprite = go.transform:Find("DisableSprite").gameObject
	local GetHint = go.transform:Find("GetHint").gameObject
	local CountLabel = go.transform:Find("CountLabel"):GetComponent(typeof(UILabel))
	go.transform:Find('Label'):GetComponent(typeof(UILabel)).text = item.Name
	--local NameLabel = go.transform:Find("Label"):GetComponent(typeof(UILabel))
	--NameLabel.text = item.Name

	IconTexture:LoadMaterial(item.Icon)
	--local bindItemCountInBag, notBindItemCountInBag = CItemMgr.Inst:CalcItemCountInBagByTemplateId(itemId)
	--local totalCount = bindItemCountInBag + notBindItemCountInBag
  local totalCount= CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, RoleDrawingCard_Setting.GetData().ComposeMaterialId)
	DisableSprite:SetActive(false)
	GetHint:SetActive(false)
	CountLabel.text = SafeStringFormat3("%d", totalCount)
end

function LuaCharacterCardHeWnd:Init()
	local onCloseClick = function(go)
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.closeBtn,DelegateFactory.Action_GameObject(onCloseClick),false)

	self:InitNeedItem(self.resNode,RoleDrawingCard_Setting.GetData().ComposeMaterialId)
  local totalCount= CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, RoleDrawingCard_Setting.GetData().ComposeMaterialId)

	local onHeClick = function(go)
		Gac2Gas.RequestRdxComposeCard(1)
	end
	CommonDefs.AddOnClickListener(self.heBtn,DelegateFactory.Action_GameObject(onHeClick),false)
	local onHe3Click = function(go)
		Gac2Gas.RequestRdxComposeCard(3)
	end
	CommonDefs.AddOnClickListener(self.he3Btn,DelegateFactory.Action_GameObject(onHe3Click),false)
	if totalCount < 3 then
		self.heBtn:GetComponent(typeof(CButton)).Enabled = false
		self.he3Btn:GetComponent(typeof(CButton)).Enabled = false
		self.btn1text.color = Color.red
		self.btn2text.color = Color.red
	elseif totalCount < 9 then
		self.heBtn:GetComponent(typeof(CButton)).Enabled = true
		self.he3Btn:GetComponent(typeof(CButton)).Enabled = false
		self.btn1text.color = Color.red
		self.btn2text.color = Color.white
	else
		self.heBtn:GetComponent(typeof(CButton)).Enabled = true
		self.he3Btn:GetComponent(typeof(CButton)).Enabled = true
		self.btn1text.color = Color.white
		self.btn2text.color = Color.white
	end
	self.ShowRoot2:SetActive(false)
end

return LuaCharacterCardHeWnd
