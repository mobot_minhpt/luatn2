local Vector4 = import "UnityEngine.Vector4"
local LuaTweenUtils = import "LuaTweenUtils"
local PathMode = import "DG.Tweening.PathMode"
local PathType = import "DG.Tweening.PathType"
local Ease = import "DG.Tweening.Ease"

CLuaQingRenJie2021EntryWnd = class()
RegistClassMember(CLuaQingRenJie2021EntryWnd, "m_GetButton")

function CLuaQingRenJie2021EntryWnd:Init()

    local button = self.transform:Find("Button").gameObject
    UIEventListener.Get(button).onClick = DelegateFactory.VoidDelegate(function(go)
        Gac2Gas.TeleportToJianJiaCangCangScene()
    end)

    local content = g_MessageMgr:FormatMessage("QingRenJie2021_Desc")


    local template = self.transform:Find("Template").gameObject
    template:SetActive(false)
    local table = self.transform:Find("ScrollView/Table"):GetComponent(typeof(UITable))
    for p in string.gmatch(content, "<p>(.-)</p>") do
        local pObj = NGUITools.AddChild(table.gameObject, template)
        pObj:SetActive(true)
        pObj.transform:Find("Label"):GetComponent(typeof(UILabel)).text = p
    end
    table:Reposition()

    self.m_GetButton  = self.transform:Find("GetButton").gameObject
    UIEventListener.Get(self.m_GetButton).onClick = DelegateFactory.VoidDelegate(function(go)
        Gac2Gas.GetJianJiaCangCangFlowerSeed()
    end)
    CUICommonDef.SetActive(self.m_GetButton,false,true)
    Gac2Gas.QueryJianJiaCangCangPlayInfo()
end

function CLuaQingRenJie2021EntryWnd:OnEnable()
    g_ScriptEvent:AddListener("ReplyJianJiaCangCangPlayInfo",self,"OnReplyJianJiaCangCangPlayInfo")
end
function CLuaQingRenJie2021EntryWnd:OnDisable()
    g_ScriptEvent:RemoveListener("ReplyJianJiaCangCangPlayInfo",self,"OnReplyJianJiaCangCangPlayInfo")
end
function CLuaQingRenJie2021EntryWnd:OnReplyJianJiaCangCangPlayInfo(bReceive)
    CUICommonDef.SetActive(self.m_GetButton,not bReceive,true)

    local trans = self.m_GetButton.transform
    local fx = trans:Find("Fx"):GetComponent(typeof(CUIFx))
    if not bReceive then
        LuaTweenUtils.DOKill(fx.transform, false)
        local b = NGUIMath.CalculateRelativeWidgetBounds(trans)
        local waypoints = CUICommonDef.GetWayPoints(Vector4(b.min.x, b.max.y, b.size.x, b.size.y), 1, false)
        fx.transform.localPosition = waypoints[0]
        fx:LoadFx("fx/ui/prefab/UI_kuang_yellow02.prefab")
        LuaTweenUtils.DOLuaLocalPath(fx.transform, waypoints, 2, PathType.Linear, PathMode.Ignore, 2, - 1, Ease.Linear)
    else
        fx:DestroyFx()
        LuaTweenUtils.DOKill(fx.transform, false)
    end
end