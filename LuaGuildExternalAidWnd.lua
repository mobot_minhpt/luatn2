local CButton = import "L10.UI.CButton"
local UILabel = import "UILabel"
local Profession = import "L10.Game.Profession"
local QnTableView = import "L10.UI.QnTableView"
local QnRadioBox = import "L10.UI.QnRadioBox"
local CSortButton = import "L10.UI.CSortButton"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UITabBar = import "L10.UI.UITabBar"
local GameObject = import "UnityEngine.GameObject"
local Constants = import "L10.Game.Constants"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local AlignType = import "CPlayerInfoMgr+AlignType"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local EnumClass = import "L10.Game.EnumClass"
local QnCheckBox = import "L10.UI.QnCheckBox"

LuaGuildExternalAidWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaGuildExternalAidWnd, "Tabs", "Tabs", UITabBar)
RegistChildComponent(LuaGuildExternalAidWnd, "MyGuildExternalAssistView", "MyGuildExternalAssistView", GameObject)
RegistChildComponent(LuaGuildExternalAidWnd, "ApplicantsListView", "ApplicantsListView", GameObject)
RegistChildComponent(LuaGuildExternalAidWnd, "Alert2Sprite", "Alert2Sprite", GameObject)
RegistChildComponent(LuaGuildExternalAidWnd, "MyGuildExternalAssistViewHeader", "MyGuildExternalAssistViewHeader", QnRadioBox)
RegistChildComponent(LuaGuildExternalAidWnd, "MyGuildExternalAssistViewHeaderWithElite", "MyGuildExternalAssistViewHeaderWithElite", QnRadioBox)
RegistChildComponent(LuaGuildExternalAidWnd, "MyGuildExternalAssistTableView", "MyGuildExternalAssistTableView", QnTableView)
RegistChildComponent(LuaGuildExternalAidWnd, "OnlineNumLabel", "OnlineNumLabel", UILabel)
RegistChildComponent(LuaGuildExternalAidWnd, "EliteLabel", "EliteLabel", UILabel)
RegistChildComponent(LuaGuildExternalAidWnd, "EliteOnlineNumLabel", "EliteOnlineNumLabel", UILabel)
RegistChildComponent(LuaGuildExternalAidWnd, "TipButton", "TipButton", GameObject)
RegistChildComponent(LuaGuildExternalAidWnd, "InviteButton", "InviteButton", GameObject)
RegistChildComponent(LuaGuildExternalAidWnd, "ApplicantsListViewHeader", "ApplicantsListViewHeader", QnRadioBox)
RegistChildComponent(LuaGuildExternalAidWnd, "ApplicantsListTableView", "ApplicantsListTableView", QnTableView)
RegistChildComponent(LuaGuildExternalAidWnd, "MyGuildExternalAssistViewNoneDataLabel", "MyGuildExternalAssistViewNoneDataLabel", UILabel)
RegistChildComponent(LuaGuildExternalAidWnd, "ApplicantsListViewNoneDataLabel", "ApplicantsListViewNoneDataLabel", UILabel)
RegistChildComponent(LuaGuildExternalAidWnd, "ClearButton", "ClearButton", CButton)
RegistChildComponent(LuaGuildExternalAidWnd, "RefuseButton", "RefuseButton", CButton)
RegistChildComponent(LuaGuildExternalAidWnd, "AcceptButton", "AcceptButton", CButton)
RegistChildComponent(LuaGuildExternalAidWnd, "QnCheckBox", "QnCheckBox", QnCheckBox)

--@endregion RegistChildComponent end
RegistClassMember(LuaGuildExternalAidWnd,"m_GuildExternalSortFlags")
RegistClassMember(LuaGuildExternalAidWnd,"m_GameplayContext")
RegistClassMember(LuaGuildExternalAidWnd,"m_ExternalAidList")
RegistClassMember(LuaGuildExternalAidWnd,"m_GuildExternalCoolingTimeLabelList")
RegistClassMember(LuaGuildExternalAidWnd,"m_UpdateTimeTick")
RegistClassMember(LuaGuildExternalAidWnd,"m_ApplicantsListSortFlags")
RegistClassMember(LuaGuildExternalAidWnd,"m_ApplicantsList")
RegistClassMember(LuaGuildExternalAidWnd,"m_SelectApplicantsListIndex")
RegistClassMember(LuaGuildExternalAidWnd,"m_SortFlagIndex")
RegistClassMember(LuaGuildExternalAidWnd,"m_SortFlagIndex2")

function LuaGuildExternalAidWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)


	
	UIEventListener.Get(self.InviteButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnInviteButtonClick()
	end)


	
	UIEventListener.Get(self.ClearButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnClearButtonClick()
	end)


	
	UIEventListener.Get(self.RefuseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRefuseButtonClick()
	end)


	
	UIEventListener.Get(self.AcceptButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnAcceptButtonClick()
	end)


    --@endregion EventBind end
end

function LuaGuildExternalAidWnd:Init()
    self.m_ApplicantsList = {}
    self.m_ExternalAidList = {}
    --这里有个假设必须成立，即打开窗口是通过服务器rpc触发的，并且返回了context信息，否则逻辑会有问题
    self.m_GameplayContext = LuaGuildExternalAidMgr.m_ForeignAidInfo.m_Context
    self.Alert2Sprite.gameObject:SetActive(false)
    self.Tabs.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
	    self:OnTabsTabChange(index)
	end)
    self.QnCheckBox.OnValueChanged = DelegateFactory.Action_bool(
        function(select)
            self:OnSelectQnCheckBox(select)
        end
    )
    self.QnCheckBox:SetSelected(LuaGuildExternalAidMgr.m_ForeignAidInfo.m_CanApply,true) 
    self:InitApplicantsListView()
    self:InitMyGuildExternalAssistView()
    self.Tabs:ChangeTab(0, false)
end

function LuaGuildExternalAidWnd:InitApplicantsListView()
    self.m_ApplicantsListSortFlags = {}
    for i = 1, self.ApplicantsListViewHeader.m_RadioButtons.Length do
        self.m_ApplicantsListSortFlags[i] = 1
    end
    self.ApplicantsListViewHeader.OnSelect = DelegateFactory.Action_QnButton_int(function(btn, index)
		self:OnSelectApplicantsListView(btn, index)
	end)
    self.ApplicantsListViewHeader:Awake()
    self.ApplicantsListViewHeader:ChangeTo(1, true)
    self.ApplicantsListTableView.m_DataSource = DefaultTableViewDataSource.Create(function() 
        return self.m_ApplicantsList and #self.m_ApplicantsList or 0
    end, function(item, index)
        self:InitApplicantsListItem(item, index)
    end)
    self.ApplicantsListTableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
        self:OnSelectApplicantsListTableViewAtRow(row)
    end)
    self.ApplicantsListViewNoneDataLabel.text = g_MessageMgr:FormatMessage("GuildExternalAssist_NoneApplicantsListData")
end

function LuaGuildExternalAidWnd:InitApplicantsListItem(item, index)
    local data = self.m_ApplicantsList[index + 1]
    local hasFeiSheng = data.hasFeiSheng

    local itemSprite = item.transform:GetComponent(typeof(UISprite))
    local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local classSprite = item.transform:Find("NameLabel/ClassSprite"):GetComponent(typeof(UISprite))
    local levelLabel = item.transform:Find("LevelLabel"):GetComponent(typeof(UILabel))
    local powerLabel = item.transform:Find("PowerLabel"):GetComponent(typeof(UILabel))
    local equipScoreLabel = item.transform:Find("EquipScoreLabel"):GetComponent(typeof(UILabel))
    local guildNameLabel = item.transform:Find("GuildNameLabel"):GetComponent(typeof(UILabel))
    local declarationLabel = item.transform:Find("DeclarationLabel"):GetComponent(typeof(UILabel))

    itemSprite.spriteName =  index % 2 == 0 and Constants.NewOddBgSprite or Constants.NewEvenBgSprite
    nameLabel.text = data.playerName
    classSprite.spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), data.class))
    levelLabel.text = data.level
    powerLabel.text = data.zhanli
    equipScoreLabel.text = data.equipScore
    guildNameLabel.text = data.guildName
    declarationLabel.text = data.declaration
end

function LuaGuildExternalAidWnd:InitMyGuildExternalAssistView()
    local needShowElite = LuaGuildExternalAidMgr:NeedShowEliteOnAidWnd(self.m_GameplayContext)
    self.MyGuildExternalAssistViewHeader.gameObject:SetActive(not needShowElite)
    self.MyGuildExternalAssistViewHeaderWithElite.gameObject:SetActive(needShowElite)
    self.EliteLabel.gameObject:SetActive(needShowElite)

    local activeViewHeader = needShowElite and self.MyGuildExternalAssistViewHeaderWithElite or self.MyGuildExternalAssistViewHeader
    self.m_GuildExternalSortFlags = {}
    for i = 1, activeViewHeader.m_RadioButtons.Length do
        self.m_GuildExternalSortFlags[i] = 1
    end
    activeViewHeader.OnSelect = DelegateFactory.Action_QnButton_int(function(btn, index)
		self:OnSelectMyGuildExternalAssistView(btn, index)
	end)
    activeViewHeader:Awake()
    activeViewHeader:On_Click(nil)
    self.MyGuildExternalAssistTableView.m_DataSource = DefaultTableViewDataSource.Create2(function() 
        return self.m_ExternalAidList and #self.m_ExternalAidList or 0
    end, function(view, index)
        local item = needShowElite and view:GetFromPool(1) or view:GetFromPool(0)
        self:InitMyGuildExternalAssistItem(item, index)
        return item
    end)
    self.MyGuildExternalAssistTableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
        self:OnSelectRankTableViewAtRow(row)
    end)
    self.m_GuildExternalCoolingTimeLabelList = {}
    self:CancelUpdateTimeLabelTick()
	self.m_UpdateTimeTick = RegisterTick(function()
		self:UpdateTimeLabel()
	end,500)
    self.MyGuildExternalAssistViewNoneDataLabel.text = g_MessageMgr:FormatMessage("MyGuildExternalAssistView_NoneData")

    local defaultPlayerId = LuaGuildExternalAidMgr.m_DefaultPlayerIdForAidWnd
    LuaGuildExternalAidMgr.m_DefaultPlayerIdForAidWnd = 0
    if defaultPlayerId and defaultPlayerId>0 then
        local aidList = LuaGuildExternalAidMgr.m_ForeignAidInfo.m_DataTbl
        for index,data in pairs(aidList) do
            if data.playerId == defaultPlayerId then
                self.MyGuildExternalAssistTableView:SetSelectRow(index-1, true)
            end
        end
    end
end

function LuaGuildExternalAidWnd:InitMyGuildExternalAssistItem(item, index)
    local data = self.m_ExternalAidList[index + 1]
    local inOnline = data.isOnline
    local hasFeiSheng = data.hasFeiSheng

    local itemSprite = item.transform:GetComponent(typeof(UISprite))
    local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local classSprite = item.transform:Find("NameLabel/ClassSprite"):GetComponent(typeof(UISprite))
    local levelLabel = item.transform:Find("LevelLabel"):GetComponent(typeof(UILabel))
    local xiuWeiLabel = item.transform:Find("XiuWeiLabel"):GetComponent(typeof(UILabel))
    local xiuLianLabel = item.transform:Find("XiuLianLabel"):GetComponent(typeof(UILabel))
    local powerLabel = item.transform:Find("PowerLabel"):GetComponent(typeof(UILabel))
    local equipScoreLabel = item.transform:Find("EquipScoreLabel"):GetComponent(typeof(UILabel))
    local guildNameLabel = item.transform:Find("GuildNameLabel"):GetComponent(typeof(UILabel))
    local coolingTimeLabel = item.transform:Find("CoolingTimeLabel"):GetComponent(typeof(UILabel))
    local tickSpriteTrans = item.transform:Find("TickSprite") --精英才有
    local tickSprite = tickSpriteTrans and tickSpriteTrans:GetComponent(typeof(UISprite)) or nil

    self.m_GuildExternalCoolingTimeLabelList[index] = coolingTimeLabel
    itemSprite.spriteName =  index % 2 == 0 and Constants.NewOddBgSprite or Constants.NewEvenBgSprite
    levelLabel.color = hasFeiSheng and NGUIText.ParseColor24("fe7900", 0) or Color.white
    nameLabel.text = data.playerName
    classSprite.spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), data.class))
    levelLabel.text = data.level
    xiuWeiLabel.text = data.xiuwei
    xiuLianLabel.text = data.xiulian
    powerLabel.text = data.zhanli
    equipScoreLabel.text = data.equipScore
    guildNameLabel.text = data.guildName
    self:UpdateCoolingTimeLabel(coolingTimeLabel, data)
    Extensions.SetLocalPositionZ(classSprite.transform, inOnline and 0 or -1)
    itemSprite.alpha = inOnline and 1 or 0.5

    if tickSprite then
        local playerId = data.playerId
        tickSprite.enabled = data.isElite
        UIEventListener.Get(tickSprite.transform:GetChild(0).gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            LuaGuildExternalAidMgr:RequestSetGuildForeignAidType(playerId, not tickSprite.enabled)
        end)
    end
end

function LuaGuildExternalAidWnd:UpdateTimeLabel()
    for index, coolingTimeLabel in pairs(self.m_GuildExternalCoolingTimeLabelList) do
        local data = self.m_ExternalAidList[index + 1]
        self:UpdateCoolingTimeLabel(coolingTimeLabel,data)
    end
end

function LuaGuildExternalAidWnd:UpdateCoolingTimeLabel(label, data)
    local canFightTime = data.canFightTime
    local now = CServerTimeMgr.Inst.timeStamp
    local t = canFightTime - now
    label.text = canFightTime <= now and LocalString.GetString("[00ff60]可参战") or SafeStringFormat3("%02d:%02d:%02d", math.floor(t / 3600), math.floor((t % 3600) / 60), t % 60)
end

function LuaGuildExternalAidWnd:SortApplicantsList()
    local sortKeyArray = {"level","zhanli","equipScore"}
    local flag = self.m_ApplicantsListSortFlags[self.m_SortFlagIndex2]
    local sortKey = sortKeyArray[self.m_SortFlagIndex2]
    table.sort(self.m_ApplicantsList,function (a,b)
        if not sortKey or not flag then
            return a.zhanli > b.zhanli
        else
            local val = a[sortKey] - b[sortKey]
            return (val * flag) > 0
        end
    end)
end

function LuaGuildExternalAidWnd:SortGuildExternalList()
    local now = CServerTimeMgr.Inst.timeStamp
    local sortKeyArray = {"level","xiuwei","xiulian","zhanli","equipScore","canFightTime", "isElite"}
    local flag = self.m_GuildExternalSortFlags[self.m_SortFlagIndex]
    local sortKey = sortKeyArray[self.m_SortFlagIndex]
    table.sort(self.m_ExternalAidList,function (a,b)
        if not sortKey or not flag then
            if a.isOnline ~= b.isOnline then
                return a.isOnline == true
            else
                return a.zhanli > b.zhanli
            end
        else
            local val = a.playerId - b.playerId
            if sortKey == "canFightTime" then
                local canFightTime1 = a.canFightTime - now
                local canFightTime2 = b.canFightTime - now
                if canFightTime1 <= 0 and canFightTime2 <= 0 then
                    val = a.zhanli - b.zhanli
                else
                    val = canFightTime2 - canFightTime1
                end
            elseif sortKey == "isElite" then
                local isElite1 = a.isElite
                local isElite2 = b.isElite
                if a.isElite ~= b.isElite then
                    val = a.isElite and -1 or 1
                end
            else
                val = a[sortKey] - b[sortKey]
            end
            return (val * flag) > 0
        end
    end)
end

--@region UIEvent

function LuaGuildExternalAidWnd:OnTipButtonClick()
    g_MessageMgr:ShowMessage("GuildExternalAidWn_ReadMe")
end

function LuaGuildExternalAidWnd:OnInviteButtonClick()
    CUIManager.ShowUI(CLuaUIResources.CommonSearchPlayerWnd)
end


function LuaGuildExternalAidWnd:OnClearButtonClick()
    LuaGuildExternalAidMgr:GuildForeignAidRefuseAll()
end

function LuaGuildExternalAidWnd:OnRefuseButtonClick()
    if not self.m_SelectApplicantsListIndex then
        g_MessageMgr:ShowMessage("Guild_Choose_Player")
        return
    end
    local row = self.m_SelectApplicantsListIndex
    local data = self.m_ApplicantsList[row + 1]
    local playerId = data.playerId
    LuaGuildExternalAidMgr:GuildForeignAidRefusePlayer(playerId)
end

function LuaGuildExternalAidWnd:OnAcceptButtonClick()
    if not self.m_SelectApplicantsListIndex then
        g_MessageMgr:ShowMessage("Guild_Choose_Player")
        return
    end
    local row = self.m_SelectApplicantsListIndex
    local data = self.m_ApplicantsList[row + 1]
    local playerId = data.playerId
    LuaGuildExternalAidMgr:GuildForeignAidAcceptPlayer(playerId)
end


--@endregion UIEvent

function LuaGuildExternalAidWnd:OnTabsTabChange(index)
    self.MyGuildExternalAssistView:SetActive(index == 0)
	self.ApplicantsListView:SetActive(index == 1)
    if index == 0 then
        LuaGuildExternalAidMgr:RequestGuildForeignAidInfo(self.m_GameplayContext)
    else
        LuaGuildExternalAidMgr:RequestGuildForeignAidApplyInfo()
    end
end


function LuaGuildExternalAidWnd:OnSelectMyGuildExternalAssistView(btn, btnindex)
    local index = btnindex + 1
    self.m_SortFlagIndex = index
    self.m_GuildExternalSortFlags[index] = - self.m_GuildExternalSortFlags[index]
    for i = 1,#self.m_GuildExternalSortFlags do
        if i ~= index then
            self.m_GuildExternalSortFlags[i] = 1
        end
    end
    local sortBtn = TypeAs(btn, typeof(CSortButton))
    sortBtn:SetSortTipStatus(self.m_GuildExternalSortFlags[index] < 0)

    self:SortGuildExternalList()
    self.m_GuildExternalCoolingTimeLabelList = {}
    self.MyGuildExternalAssistTableView:ReloadData(true, false)
    self.MyGuildExternalAssistViewNoneDataLabel.gameObject:SetActive(#self.m_ExternalAidList == 0)
end

function LuaGuildExternalAidWnd:OnSelectApplicantsListView(btn, btnindex)
    local index = btnindex + 1
    self.m_SortFlagIndex2 = index
    self.m_ApplicantsListSortFlags[index] = -self.m_ApplicantsListSortFlags[index]
    for i = 1,#self.m_ApplicantsListSortFlags do
        if i ~= index then
            self.m_ApplicantsListSortFlags[i] = 1
        end
    end
    local sortBtn = TypeAs(btn, typeof(CSortButton))
    sortBtn:SetSortTipStatus(self.m_ApplicantsListSortFlags[index] < 0)

    self:SortApplicantsList()
    self.ApplicantsListTableView:ReloadData(true, false)
    self.ApplicantsListViewNoneDataLabel.gameObject:SetActive(#self.m_ApplicantsList == 0)
    self.ClearButton.Enabled = #self.m_ApplicantsList > 0
    self.RefuseButton.Enabled = #self.m_ApplicantsList > 0
    self.AcceptButton.Enabled = #self.m_ApplicantsList > 0
end

function LuaGuildExternalAidWnd:OnSelectRankTableViewAtRow(row)
    local data = self.m_ExternalAidList[row + 1]
    local playerId = data.playerId
    CPlayerInfoMgr.ShowPlayerPopupMenu(playerId, EnumPlayerInfoContext.GuildExternalAid, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
end

function LuaGuildExternalAidWnd:OnSelectApplicantsListTableViewAtRow(row)
    self.m_SelectApplicantsListIndex = row
    local data = self.m_ApplicantsList[row + 1]
    local playerId = data.playerId
    CPlayerInfoMgr.ShowPlayerPopupMenu(playerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
end

function LuaGuildExternalAidWnd:OnSelectQnCheckBox(select)
    local msg = g_MessageMgr:FormatMessage(select and "SetGuildForeignAidApplyStatus_True" or "SetGuildForeignAidApplyStatus_False")
    MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
        Gac2Gas.RequestSetGuildForeignAidApplyStatus(select)
    end),nil,nil,nil,false)
    self.QnCheckBox:SetSelected(not select,true) 
end

function LuaGuildExternalAidWnd:OnEnable()
    g_ScriptEvent:AddListener("SelectPlayer", self, "SelectPlayer")
    g_ScriptEvent:AddListener("OnSendGuildForeignAidInfo", self, "OnSendGuildForeignAidInfo")
    g_ScriptEvent:AddListener("OnSendGuildForeignAidApplyInfo", self, "OnSendGuildForeignAidApplyInfo")
    g_ScriptEvent:AddListener("OnSetGuildForeignAidTypeSuccess", self, "OnSetGuildForeignAidTypeSuccess")
    g_ScriptEvent:AddListener("OnSendGuildForeignAidApplicationStatus", self, "OnSendGuildForeignAidApplicationStatus")
end

function LuaGuildExternalAidWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SelectPlayer", self, "SelectPlayer")
    g_ScriptEvent:RemoveListener("OnSendGuildForeignAidInfo", self, "OnSendGuildForeignAidInfo")
    g_ScriptEvent:RemoveListener("OnSendGuildForeignAidApplyInfo", self, "OnSendGuildForeignAidApplyInfo")
    g_ScriptEvent:RemoveListener("OnSetGuildForeignAidTypeSuccess", self, "OnSetGuildForeignAidTypeSuccess")
    g_ScriptEvent:RemoveListener("OnSendGuildForeignAidApplicationStatus", self, "OnSendGuildForeignAidApplicationStatus")
    self:CancelUpdateTimeLabelTick()
end

function LuaGuildExternalAidWnd:OnDestroy()
    LuaGuildExternalAidMgr:OnGuildExternalAidWndClose()
end

function LuaGuildExternalAidWnd:CancelUpdateTimeLabelTick()
    if self.m_UpdateTimeTick then
        UnRegisterTick(self.m_UpdateTimeTick)
        self.m_UpdateTimeTick = nil
    end
end

function LuaGuildExternalAidWnd:SelectPlayer(args)
    local id, name = args[0], args[1]
    LuaGuildExternalAidMgr:GuildForeignAidInvitePlayer(id)
end

function LuaGuildExternalAidWnd:OnSendGuildForeignAidApplicationStatus(canApply)
    self.QnCheckBox:SetSelected(canApply,true) 
end

function LuaGuildExternalAidWnd:OnSendGuildForeignAidInfo(hasApplication)
    local foreignAidInfo = LuaGuildExternalAidMgr.m_ForeignAidInfo
    self.m_GameplayContext = foreignAidInfo.m_Context
    self.m_ExternalAidList = foreignAidInfo.m_DataTbl
    self:UpdatePlayerNumLabel()
    self.ApplicantsListTableView:ReloadData(true, true)
    self.Alert2Sprite.gameObject:SetActive(hasApplication)
    self:SortGuildExternalList()
    self.m_GuildExternalCoolingTimeLabelList = {}
    self.MyGuildExternalAssistTableView:ReloadData(true, false)
    self.MyGuildExternalAssistViewNoneDataLabel.gameObject:SetActive(#self.m_ExternalAidList == 0)
end

function LuaGuildExternalAidWnd:OnSendGuildForeignAidApplyInfo()
    self.m_ApplicantsList = LuaGuildExternalAidMgr.m_ApplyInfo
    local foreignAidInfo = LuaGuildExternalAidMgr.m_ForeignAidInfo
    self.m_GameplayContext = foreignAidInfo.m_Context
    self.Alert2Sprite.gameObject:SetActive(#self.m_ApplicantsList > 0)
    self:SortApplicantsList()
    self.ApplicantsListTableView:ReloadData(true, false)
    self.ApplicantsListViewNoneDataLabel.gameObject:SetActive(#self.m_ApplicantsList == 0)
    self.ClearButton.Enabled = #self.m_ApplicantsList > 0
    self.RefuseButton.Enabled = #self.m_ApplicantsList > 0
    self.AcceptButton.Enabled = #self.m_ApplicantsList > 0
    self.QnCheckBox:SetSelected(LuaGuildExternalAidMgr.m_ForeignAidInfo.m_CanApply,true) 
end

function LuaGuildExternalAidWnd:OnSetGuildForeignAidTypeSuccess(targetPlayerId)
    local foreignAidInfo = LuaGuildExternalAidMgr.m_ForeignAidInfo
    self:UpdatePlayerNumLabel()
    for index,data in pairs(self.m_ExternalAidList) do
        if data.playerId == targetPlayerId then
            local item =  self.MyGuildExternalAssistTableView:GetItemAtRow(index-1)
            local tickSpriteTrans = item and item.transform:Find("TickSprite") --精英才有
            local tickSprite = tickSpriteTrans and tickSpriteTrans:GetComponent(typeof(UISprite)) or nil
            if tickSprite then
                tickSprite.enabled = data.isElite
            end
            break
        end
    end
end

function LuaGuildExternalAidWnd:UpdatePlayerNumLabel()
    local foreignAidInfo = LuaGuildExternalAidMgr.m_ForeignAidInfo
    local eliteAidTotal = 0
    for index,data in pairs(self.m_ExternalAidList) do
        if data.isElite then
            eliteAidTotal = eliteAidTotal+1
        end
    end
    self.OnlineNumLabel.text = SafeStringFormat3("[00ff60]%d/[ffffff]%d/%d",foreignAidInfo.m_AidOnlineNum,#self.m_ExternalAidList,foreignAidInfo.m_MaxAidOnlineCount)
    self.EliteOnlineNumLabel.text = SafeStringFormat3("[00ff60]%d/[ffffff]%d/%d",foreignAidInfo.m_EliteAidOnlineNum,eliteAidTotal,foreignAidInfo.m_MaxEliteAidOnlineCount)
end
