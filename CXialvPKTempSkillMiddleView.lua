-- Auto Generated!!
local CommonDefs = import "L10.Game.CommonDefs"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"
local CXialvPKMgr = import "L10.Game.CXialvPKMgr"
local CXialvPKSkillItem = import "L10.UI.CXialvPKSkillItem"
local CXialvPKTempSkillMiddleView = import "L10.UI.CXialvPKTempSkillMiddleView"
local DelegateFactory = import "DelegateFactory"
local Extensions = import "Extensions"
local GameObject = import "UnityEngine.GameObject"
local Int32 = import "System.Int32"
local Quaternion = import "UnityEngine.Quaternion"
local TempSkillItem = import "L10.UI.TempSkillItem"
local UInt32 = import "System.UInt32"
local Vector3 = import "UnityEngine.Vector3"
local XiaLvPK_Skill = import "L10.Game.XiaLvPK_Skill"

CXialvPKTempSkillMiddleView.m_Init_CS2LuaHook = function (this, classId) 

    this:InitTempSkillsForClass(classId)

    -- 设置翻页相关的处理
    this.m_TotalPages = math.max(1, (math.floor((this.m_TempSkillsForClass.Count - 1) / 9)) + 1)
    this.m_PageButton:SetMinMax(1, this.m_TotalPages, 1)
    this.m_PageButton:SetValue(1, false)
    this.m_PageButton:SetInputEnabled(false)

    this:InitItemsForPage(1)
    this.m_ClonedIcon.gameObject:SetActive(false)
end
CXialvPKTempSkillMiddleView.m_InitItemsForPage_CS2LuaHook = function (this, page) 

    this.m_CurrentPage = page
    Extensions.RemoveAllChildren(this.m_SkillGrid.transform)

    local start = math.max((page - 1) * 9, 0)
    local end_ = math.min(this.m_TempSkillsForClass.Count, page * 9)

    do
        local i = start
        while i < end_ do
            local instance = CommonDefs.Object_Instantiate(this.m_SkillCellItem)
            instance.transform.parent = this.m_SkillGrid.transform
            instance.transform.localPosition = Vector3.zero
            instance.transform.localRotation = Quaternion.identity
            instance.transform.localScale = Vector3.one
            instance:SetActive(true)

            local item = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CXialvPKSkillItem))
            if item ~= nil then
                local tempInfo = this.m_TempSkillsForClass[i]
                item:Init(tempInfo)
                item.OnItemClickDelegate = MakeDelegateFromCSFunction(this.OnItemClick, MakeGenericClass(Action1, CXialvPKSkillItem), this)
                item.OnDragStart = MakeDelegateFromCSFunction(this.OnDragSkillIconStart, MakeGenericClass(Action3, GameObject, UInt32, Int32), this)
            end
            i = i + 1
        end
    end
    this.m_SkillGrid:Reposition()
    this.m_ScrollView:ResetPosition()

    this.m_PageButton:OverrideText(System.String.Format("{0}/{1}", this.m_CurrentPage, this.m_TotalPages))
end
CXialvPKTempSkillMiddleView.m_InitTempSkillsForClass_CS2LuaHook = function (this, classId) 
    -- 针对画魂的特殊处理
    if classId == 10 then
        classId = 0
    end

    CommonDefs.ListClear(this.m_TempSkillsForClass)
    local prefix = 9290 + classId

    XiaLvPK_Skill.ForeachKey(DelegateFactory.Action_object(function (skillId) 
        local skill = XiaLvPK_Skill.GetData(skillId)

        local canApplyToClass = true

        if (skill.Class ~= nil and CommonDefs.Array_IndexOf_uint(skill.Class, classId) ~= - 1) or skill.Status ~= 0 then
            canApplyToClass = false
        end

        -- 色系技能-默认为基础技能
        if CXialvPKMgr.Inst:IsColorSystemSkill(skillId) then
            canApplyToClass = false
        end

        if math.floor(skillId / 100) == prefix and canApplyToClass then
            local tempSkillItem = CreateFromClass(TempSkillItem)
            tempSkillItem.ID = skill.SkillId
            tempSkillItem.Class = classId
            CommonDefs.ListAdd(this.m_TempSkillsForClass, typeof(TempSkillItem), tempSkillItem)
        end
    end))
end
CXialvPKTempSkillMiddleView.m_OnItemClick_CS2LuaHook = function (this, item) 
    local skills = CommonDefs.GetComponentsInChildren_Component_Type(this.m_SkillGrid.transform, typeof(CXialvPKSkillItem))
    do
        local i = 0
        while i < skills.Length do
            skills[i]:SetSelected(skills[i] == item, false)
            i = i + 1
        end
    end
    local info = item.tempSkill
    CSkillInfoMgr.ShowSkillInfoWnd(CLuaDesignMgr.Skill_AllSkills_GetSkillId(info.ID, 1), this.m_TipTopPos.transform.position, CSkillInfoMgr.EnumSkillInfoContext.XiaLvSkill, true, 0, 0, nil)
end
