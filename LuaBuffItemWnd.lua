--[[
    药匣窗口
    author:CodeGize
    time:2019-05-21 15:27:47
]]

--@region import

local GameObject        = import "UnityEngine.GameObject"
local Color             = import "UnityEngine.Color"

local DelegateFactory   = import "DelegateFactory"
local UILabel           = import "UILabel"
local LocalString       = import "LocalString"

local QnTableView                   = import "L10.UI.QnTableView"
local CUITexture		            = import "L10.UI.CUITexture"
local DefaultTableViewDataSource	= import "L10.UI.DefaultTableViewDataSource"
local AlignType                     = import "L10.UI.CTooltip+AlignType"
local AlignType2                    = import "L10.UI.CItemInfoMgr+AlignType"
local CItemAccessListMgr            = import "L10.UI.CItemAccessListMgr"
local MessageWndManager             = import "L10.UI.MessageWndManager"
local CItemInfoMgr                  = import "L10.UI.CItemInfoMgr"
local CButton                       = import "L10.UI.CButton"
local DefaultItemActionDataSource   = import "L10.UI.DefaultItemActionDataSource"
local CUIManager                    = import "L10.UI.CUIManager"
local CIndirectUIResources          = import "L10.UI.CIndirectUIResources"
local CMiddleNoticeMgr              = import "L10.UI.CMiddleNoticeMgr"

local CommonDefs        = import "L10.Game.CommonDefs"
local CServerTimeMgr    = import "L10.Game.CServerTimeMgr"

--@endregion

CLuaBuffItemWnd = class()

--@region 绑定控件和注册变量
RegistChildComponent(CLuaBuffItemWnd, "QnTableView1",	QnTableView)
RegistChildComponent(CLuaBuffItemWnd, "QnTableView2",	QnTableView)
RegistChildComponent(CLuaBuffItemWnd, "QnTableView3",	QnTableView)
RegistChildComponent(CLuaBuffItemWnd, "CollectBtn",	    GameObject)
RegistChildComponent(CLuaBuffItemWnd, "RuleBtn",		GameObject)
RegistChildComponent(CLuaBuffItemWnd, "UseBtn",		    GameObject)
RegistChildComponent(CLuaBuffItemWnd, "ChooseBtn",		CButton)

RegistClassMember(CLuaBuffItemWnd,  "m_groups")
RegistClassMember(CLuaBuffItemWnd,  "m_counts")
RegistClassMember(CLuaBuffItemWnd,  "m_selects")
RegistClassMember(CLuaBuffItemWnd,  "m_dirty")
RegistClassMember(CLuaBuffItemWnd,  "m_inited")
RegistClassMember(CLuaBuffItemWnd,  "delayTick")

--@endregion

--@region 和服务器无关的界面初始化

function CLuaBuffItemWnd:Init()
    self:InitDatas()
    Gac2Gas.RequestOpenMedicineBox()
    for i=1,3 do
        self:InitGroup(i)
    end
    self.m_inited = true
end

function CLuaBuffItemWnd:InitDatas()
    self.m_groups = {}
    self.m_groups[1] = {}
    self.m_groups[2] = {}
    self.m_groups[3] = {}
    self.m_counts = {}--int tid -> int count
    self.m_selects = {}--int tid -> int isselect 
    self.m_dirty = false
    self.m_inited = false
    MedicineBox_Item.Foreach(function(tid, data)
        local type = data.Type
        local newdata ={TemplateId = tid,OrderIndex = data.OrderIndex,Des =data.Des,Group = data.Group}
        table.insert(self.m_groups[type],newdata)
    end)

    local sortfunction = function(a,b)
        return a.OrderIndex<b.OrderIndex
    end
    for i=1,3 do
        table.sort( self.m_groups[i], sortfunction )
    end
    
    local gp = self.m_groups[1]
end

function CLuaBuffItemWnd:InitGroup(group)
    local grouplst = self.m_groups[group]
    local ct = #grouplst

    local table =self:GetTable(group)

    local initItem = function (item, index)
        self:InitItem(item, index,group)
    end

    local selectItem =function (row)
        self:OnSelectAtRow(row,group)
    end

    table.m_DataSource = DefaultTableViewDataSource.CreateByCount(ct, initItem)
    table.OnSelectAtRow = DelegateFactory.Action_int(selectItem)
    table:ReloadData(true,true)

end

--@endregion

--@region 和服务器数据有关的初始化

--[[
    @desc: 接受服务器数据之后刷新界面
    author:CodeGize
    time:2019-05-23 17:05:41
    @return:
]]
function CLuaBuffItemWnd:OnDataChange()
    local data = CLuaBuffItemWnd.Data

    self:InitUseBtn(data.lastOneKeyUseTime)
    self:InitItems(data.itemDataUD,data.lastTemplateIdStr)
end

function CLuaBuffItemWnd:InitUseBtn(time)
    if self.delayTick then 
        UnRegisterTick(self.delayTick)
        self.delayTick = nil
    end

    local sd = (CServerTimeMgr.Inst.timeStamp-time);
    local btn =self.UseBtn:GetComponent(typeof(CButton))
    local waittime = MedicineBox_Setting.GetData().OneKeyUseCD
    if sd < waittime then
        if sd <0 then 
            sd = 0
        end
        btn.Enabled = false
        local last =math.ceil(waittime-sd)
        btn.Text = LocalString.GetString("一键使用("..last..")")

        local tick = function()
            sd = math.ceil(sd+1)
            if sd >=waittime then
                UnRegisterTick(self.delayTick)
                self.delayTick = nil
                btn.Text = LocalString.GetString("一键使用")
                btn.Enabled = true
            else
                local last = waittime-sd
                btn.Text = LocalString.GetString("一键使用("..last..")")
            end
        end
        self.delayTick = RegisterTick(tick,1000)
    else
        btn.Enabled = true
    end
end

--[[
    @desc: 初始化所有状态药品
    author:CodeGize
    time:2019-05-23 17:52:00
    @return:
]]
function CLuaBuffItemWnd:InitItems(countlst,selectstr)
    self.m_counts = {}
    self.m_selects = {}
    local lstct = countlst.Count
    for i=0,lstct-1,2 do
        self.m_counts[countlst[i]]=countlst[i+1]
    end
    local selectlst = g_LuaUtil:StrSplit(selectstr, ",")
    for i=1,#selectlst do

        local key = tonumber(selectlst[i])
        if key ~= nil then
            self.m_selects[key] = 1
        end
    end
    self:ReloadAllTable()
end

function CLuaBuffItemWnd:ReloadAllTable()
    for i=1,3 do
        local table = self:GetTable(i)
        table:ReloadData(true,true)
    end
end

--@endregion

function CLuaBuffItemWnd:GetTable(group)
    if group ==1 then
        return self.QnTableView1
    elseif group ==2 then 
        return self.QnTableView2
    elseif group == 3 then
        return self.QnTableView3
    end
end

function CLuaBuffItemWnd:OnSelectAtRow(row,group)
    local gp = self.m_groups[group]
    local tdata = gp[row+1]
    local key = tdata.TemplateId
    local selected = false
    local reflush = false
    if not self.m_selects[key] or self.m_selects[key] == 0 then
        for k,v in pairs(self.m_selects) do
            if k ~= key then
                local kdata = MedicineBox_Item.GetData(k)
                if kdata.Group == tdata.Group and tdata.Group ~= 0 then
                    self.m_selects[k]=0
                    reflush = true
                end
            end
        end
        self.m_selects[key] = 1
        selected = true
    else
        self.m_selects[key] = 0
        selected = false
    end
    self.m_dirty = true
    if reflush then--刷新全部
        for i=1,3 do
            local table = self:GetTable(i)
            table:ReloadData(true,true)
        end
    else--只刷新自己
        local table = self:GetTable(group)
        local item = table:GetItemAtRow(row)
        local checkboxctrl = item.transform:Find("checkbox").gameObject
        checkboxctrl:SetActive(selected)
    end
end

--[[
    @desc: 初始化增加一个药品控件
    author:CodeGize
    time:2019-05-23 17:52:35
    --@data: 
    @return:
]]
function CLuaBuffItemWnd:InitItem(item,index,group)
    
    local grouplst = self.m_groups[group]
    local tdata = grouplst[index+1]
    local ct = 0
    if self.m_counts[tdata.TemplateId] then 
        ct = self.m_counts[tdata.TemplateId]
    end

    local selected = false
    local key = tdata.TemplateId
    if self.m_selects[key]==1 then
        selected = true
    end
    local itemdata = Item_Item.GetData(tdata.TemplateId)

    local trans = item.transform 
    local ctctrl = trans:Find("count"):GetComponent(typeof(UILabel))
    local iconctrl = trans:Find("icon"):GetComponent(typeof(CUITexture))
    local namectrl = trans:Find("name"):GetComponent(typeof(UILabel))
    local attrctrl = trans:Find("attr"):GetComponent(typeof(UILabel))
    local checkboxctrl = trans:Find("checkbox").gameObject
    local maskctrl = trans:Find("mask").gameObject

    ctctrl.text = tostring(ct)
    if ct<=0 then
        ctctrl.color = Color.red
    else
        ctctrl.color = Color.white
    end

    checkboxctrl:SetActive(selected)

    if not self.m_inited then
        iconctrl:LoadMaterial(itemdata.Icon)
    end

    local takeBackAct = function()
        if self.m_dirty then    -- 加一个勾选存档，以免玩家先勾选道具选项后再将道具移出，导致勾选情况还原
            local str = self:GetStrSelects()
            Gac2Gas.SaveMedicineBoxChoices(str)
        end
        Gac2Gas.TakeOutItemFromMedicineBox(tdata.TemplateId)
        CUIManager.CloseUI(CIndirectUIResources.ItemInfoWnd)
    end
    local t_action = {}
    t_action[1] = takeBackAct
    local t_name={}
    t_name[1] = LocalString.GetString("取出")
    local actsource = DefaultItemActionDataSource.Create(1,t_action,t_name)
    
    --显示tip
    local iconclick = function (go)
        CItemInfoMgr.ShowLinkItemTemplateInfo(tdata.TemplateId, false, actsource, AlignType2.Default, 0, 0, 0, 0) 
    end
    CommonDefs.AddOnClickListener(iconctrl.gameObject,DelegateFactory.Action_GameObject(iconclick), false)

    namectrl.text = itemdata.Name
    attrctrl.text = tdata.Des

    maskctrl:SetActive(ct <= 0)
    local itemmaskclick = function(go)
        CItemAccessListMgr.Inst:ShowItemAccessInfo(tdata.TemplateId, false, go.transform, AlignType.Right)
    end
    CommonDefs.AddOnClickListener(maskctrl,DelegateFactory.Action_GameObject(itemmaskclick), false)
end

function CLuaBuffItemWnd:OnEnable()

    local CollectBtnClick = function (go)
        self:OnCollectBtnClick()
    end

    local UseBtnClick = function (go)
        self:OnUseBtnClick()
    end

    local RuleBtnClick = function(go)
        self:OnRuleBtnClick()
    end

    CommonDefs.AddOnClickListener(self.CollectBtn,DelegateFactory.Action_GameObject(CollectBtnClick), false)
    CommonDefs.AddOnClickListener(self.UseBtn,DelegateFactory.Action_GameObject(UseBtnClick), false)
    CommonDefs.AddOnClickListener(self.RuleBtn,DelegateFactory.Action_GameObject(RuleBtnClick), false)

    UIEventListener.Get(self.ChooseBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnChooseBtnClick()
	end)
    g_ScriptEvent:AddListener("OnBuffItemWndDataChange",self,"OnDataChange")
end

function CLuaBuffItemWnd:OnDisable()
    if self.delayTick then 
        UnRegisterTick(self.delayTick)
        self.delayTick = nil
    end
    g_ScriptEvent:RemoveListener("OnBuffItemWndDataChange",self,"OnDataChange")
    self:OnClose()
end

--@region UI事件

--[[
    @desc: 收纳按钮点击事件
    author:CodeGize
    time:2019-05-21 15:39:25
    @return:
]]
function CLuaBuffItemWnd:OnCollectBtnClick()
    local savestr = self:GetStrSelects()
    local reqfunc = function()
        Gac2Gas.OneKeyCollectItemToMedicineBox()--请求一键收纳
        Gac2Gas.SaveMedicineBoxChoices(savestr)
        Gac2Gas.RequestOpenMedicineBox()--向服务器取数据
    end 
	local str = g_MessageMgr:FormatMessage("YAOXIA_COLLECT_CONFIRM")
    MessageWndManager.ShowOKCancelMessage(str, DelegateFactory.Action(reqfunc),nil, nil, nil, false)
end

--[[
    @desc: 使用按钮点击事件
    author:CodeGize
    time:2019-05-21 15:39:11
    @return:
]]
function CLuaBuffItemWnd:OnUseBtnClick()

    for i,v in pairs(self.m_selects) do
        if v ==1 then
            local ct = self.m_counts[i]
            if ct==nil or ct<=0 then
                local msg = g_MessageMgr:FormatMessage("MedicineBox_OneKeyUse_Item_Not_Enough")
                CMiddleNoticeMgr.ShowMiddleNotice(msg,3)
                return--勾选数量不足的物品时，无法一键使用
            end
        end
    end

    local str = self:GetStrSelects()
    if str == nil or str == "" then 
        local msg2 = g_MessageMgr:FormatMessage("YAOXIA_CHOOSE_MEDICINE")
        CMiddleNoticeMgr.ShowMiddleNotice(msg2,3)
        return
    end

    Gac2Gas.OneKeyUseMedicineBoxItem(str)--请求一键使用
end

--[[
    @desc: 帮助按钮事件
    author:CodeGize
    time:2019-06-25 14:06:52
    @return:
]]
function CLuaBuffItemWnd:OnRuleBtnClick()
    g_MessageMgr:ShowMessage("YAOXIA_RULE")
end

function CLuaBuffItemWnd:OnChooseBtnClick()
    local selectGroup = {}
    local hasSelect = false
    local originalStr = self:GetStrSelects()   -- 记录一下原选项
    for groupNum=1,3 do
        for row=1,#self.m_groups[groupNum] do
            local data = self.m_groups[groupNum][row]
            local itemId = data.TemplateId
            local count = self.m_counts[itemId]
            local tableView = self:GetTable(groupNum)
            local item = tableView:GetItemAtRow(row-1)
            local checkboxctrl = item.transform:Find("checkbox").gameObject
            self.m_selects[itemId] = 0
            checkboxctrl:SetActive(false)
            if count and count > 0 then 
                local group = data.Group
                if group ~= 0 then 
                    if not selectGroup[group] then selectGroup[group] = {} end
                    selectGroup[group].Id = itemId
                    selectGroup[group].GroupNum = groupNum
                    selectGroup[group].Row = row - 1
                else
                    hasSelect = true
                    self.m_selects[itemId] = 1
                    checkboxctrl:SetActive(true)
                end
            end
        end
    end
    for k,v in pairs(selectGroup) do
        local tableView = self:GetTable(v.GroupNum)
        local item = tableView:GetItemAtRow(v.Row)
        local checkboxctrl = item.transform:Find("checkbox").gameObject
        hasSelect = true
        self.m_selects[v.Id] = 1
        checkboxctrl:SetActive(true)
    end

    if hasSelect then g_MessageMgr:ShowMessage("BaiChaoGeChooseComplete") else g_MessageMgr:ShowMessage("BaiChaoGeChooseNone") end
    local str = self:GetStrSelects()
    if str ~= originalStr then Gac2Gas.SaveMedicineBoxChoices(str) end
end

function CLuaBuffItemWnd:OnClose()
    if self.m_dirty then
        local str = self:GetStrSelects()
        Gac2Gas.SaveMedicineBoxChoices(str)
    end
end


function CLuaBuffItemWnd:GetStrSelects()
    local res =""
    for i,v in pairs(self.m_selects) do
        if v == 1 then
            if res ~= "" then 
                res = res..","..i
            else
                res = res..i
            end
        end
    end
    return res
end
--@endregion

CLuaBuffItemWnd.Data = {}
