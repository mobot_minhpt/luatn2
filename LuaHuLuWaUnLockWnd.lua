local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CChatLinkMgr = import "CChatLinkMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"

LuaHuLuWaUnLockWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaHuLuWaUnLockWnd, "NameLabel", "NameLabel", UILabel)
RegistChildComponent(LuaHuLuWaUnLockWnd, "DescLabel", "DescLabel", UILabel)
RegistChildComponent(LuaHuLuWaUnLockWnd, "UnLockBtn", "UnLockBtn", GameObject)
RegistChildComponent(LuaHuLuWaUnLockWnd, "UnLockTag", "UnLockTag", GameObject)

--@endregion RegistChildComponent end

function LuaHuLuWaUnLockWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.UnLockBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnUnLockBtnClick()
	end)


    --@endregion EventBind end
end

function LuaHuLuWaUnLockWnd:Init()
	local idx = LuaHuLuWa2022Mgr.m_CurHuLuWaIdx
	local data = HuluBrothers_Transform.GetData(idx)
	local monsterId = data.MonsterID
	local monster = Monster_Monster.GetData(monsterId)
	if not monster then  return end

	self.NameLabel.text = monster.Name
	self.DescLabel.text = CChatLinkMgr.TranslateToNGUIText(data.SkillDesc, false)

	self.UnLockTag:SetActive(LuaHuLuWa2022Mgr.m_CurHuLuWaUnLock)
	self.UnLockBtn:SetActive(not LuaHuLuWa2022Mgr.m_CurHuLuWaUnLock)
end

--@region UIEvent

function LuaHuLuWaUnLockWnd:OnUnLockBtnClick()
	local idx = LuaHuLuWa2022Mgr.m_CurHuLuWaIdx
	local data = HuluBrothers_Transform.GetData(idx)
	local templateId = data.ItemId 
	
	local exsitCount = CItemMgr.Inst:GetItemCount(templateId)
    if exsitCount > 0 then
        local find, pos, itemId = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag,templateId)
        if find then
            CItemInfoMgr.ShowQuickUseItem(itemId)
        end 
	else
		local itemData = Item_Item.GetData(templateId)
		if itemData then
			g_MessageMgr:ShowMessage("Unlock_Huluwa_NoItem",itemData.Name)
		end
		
    end
	CUIManager.CloseUI(CLuaUIResources.HuLuWaUnLockWnd)
end

--@endregion UIEvent

