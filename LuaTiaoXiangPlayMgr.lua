LuaTiaoXiangPlayMgr = class()

function LuaTiaoXiangPlayMgr.InitCookBook(taskId)
    if taskId == 22022888 or taskId == 22029452 then--[巫夜锦]百和调香
        LuaTiaoXiangPlayMgr.m_Textures = {
            [1] = "UI/Texture/Transparent/Material/BookshelfSearchWnd_xinyihua_1.mat",--桃花
            [2] = "UI/Texture/Transparent/Material/tiaoxiangplaywnd_tanhua.mat",--优昙
            [3] = "UI/Texture/Transparent/Material/tiaoxiangplaywnd_baihe.mat",--百合
            [4] = "UI/Texture/Transparent/Material/tiaoxiangplaywnd_qiangweilu.mat"--蔷薇露
        }
        LuaTiaoXiangPlayMgr.m_Colors = {
            [1] = "FF80A7",--"FB9CCD",
            [2] = "FFF16C",--"FF6C6C",
            [3] = "CCFFA3",
            [4] = "8AE6FF",
            [5] = "161D34"
        }
        LuaTiaoXiangPlayMgr.m_cookBook = {
            [1] = 2,  --桃花2份
            [2] = 4,  --优昙4份
            [3] = 3,  --百合3份
            [4] = 1   --蔷薇露1份
        }
        
        LuaTiaoXiangPlayMgr.m_Idx2Name = {
            [1] = LocalString.GetString("桃花"),
            [2] = LocalString.GetString("优昙"),
            [3] = LocalString.GetString("百合"),
            [4] = LocalString.GetString("蔷薇露")
        }
        LuaTiaoXiangPlayMgr.m_ResultName = LocalString.GetString("百和香")
        LuaTiaoXiangPlayMgr.m_MsgTxt = g_MessageMgr:FormatMessage("NanDieKe_ZJJQ_Perfume1")
        if taskId == 22022888 then
            LuaTiaoXiangPlayMgr.m_GuideKey = EnumGuideKey.TiaoXiangPlayWnd
        elseif taskId == 22029452 then
            LuaTiaoXiangPlayMgr.m_GuideKey = EnumGuideKey.TiaoXiangPlayWnd3
        end

    elseif taskId == 22022948 or taskId == 22029406 or taskId == 22207634 then--22022948[巫夜锦]红艳凝香 
        LuaTiaoXiangPlayMgr.m_Textures = {
            [1] = "UI/Texture/Transparent/Material/tiaoxiangplaywnd_baihe.mat",--百合
            [2] = "UI/Texture/Transparent/Material/qingrenjie_meigui.mat",--玫瑰
            [3] = "",
            [4] = "UI/Texture/Transparent/Material/tiaoxiangplaywnd_qiangweilu.mat"--蔷薇露
        }
        LuaTiaoXiangPlayMgr.m_Colors = {
            [1] = "CCFFA3",
            [2] = "FF6C6C",
            [3] = "CCFFA3",
            [4] = "8AE6FF",
            [5] = "161D34"
        }
        LuaTiaoXiangPlayMgr.m_cookBook = {
            [1] = 2,  --百合2份
            [2] = 4,  --玫瑰4份
            [3] = 0,  
            [4] = 1   --蔷薇露1份
        }
        
        LuaTiaoXiangPlayMgr.m_Idx2Name = {
            [1] = LocalString.GetString("百合"),
            [2] = LocalString.GetString("玫瑰"),
            [3] = "",
            [4] = LocalString.GetString("蔷薇露")
        }
        LuaTiaoXiangPlayMgr.m_ResultName = LocalString.GetString("百和香")
        LuaTiaoXiangPlayMgr.m_MsgTxt = g_MessageMgr:FormatMessage("NanDieKe_ZJJQ_Perfume2")
        if taskId == 22022948 then
            LuaTiaoXiangPlayMgr.m_GuideKey = EnumGuideKey.TiaoXiangPlayWndHYNX
        elseif taskId == 22029406 then
            LuaTiaoXiangPlayMgr.m_GuideKey = EnumGuideKey.TiaoXiangPlayWnd4
        else
            LuaTiaoXiangPlayMgr.m_GuideKey = nil
        end
    end
end
