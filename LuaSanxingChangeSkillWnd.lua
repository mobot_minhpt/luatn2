local GameObject = import "UnityEngine.GameObject"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local Color = import "UnityEngine.Color"
local DelegateFactory = import "DelegateFactory"
local UISprite = import "UISprite"
local LocalString = import "LocalString"
local CUITexture = import "L10.UI.CUITexture"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Money = import "L10.Game.Money"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local Profession = import "L10.Game.Profession"
local EnumClass = import "L10.Game.EnumClass"

LuaSanxingChangeSkillWnd = class()
RegistChildComponent(LuaSanxingChangeSkillWnd,"CloseButton", GameObject)
RegistChildComponent(LuaSanxingChangeSkillWnd,"needExpIcon", UISprite)
RegistChildComponent(LuaSanxingChangeSkillWnd,"needExpValue", UILabel)
RegistChildComponent(LuaSanxingChangeSkillWnd,"ownExpIcon", UISprite)
RegistChildComponent(LuaSanxingChangeSkillWnd,"ownExpValue", UILabel)
RegistChildComponent(LuaSanxingChangeSkillWnd,"needMoneyIcon", UISprite)
RegistChildComponent(LuaSanxingChangeSkillWnd,"needMoneyValue", UILabel)
RegistChildComponent(LuaSanxingChangeSkillWnd,"ownMoneyIcon", UISprite)
RegistChildComponent(LuaSanxingChangeSkillWnd,"ownMoneyValue", UILabel)
RegistChildComponent(LuaSanxingChangeSkillWnd,"replaceBtn", GameObject)
RegistChildComponent(LuaSanxingChangeSkillWnd,"tipBtn", GameObject)
RegistChildComponent(LuaSanxingChangeSkillWnd,"node1", GameObject)
RegistChildComponent(LuaSanxingChangeSkillWnd,"node2", GameObject)
RegistChildComponent(LuaSanxingChangeSkillWnd,"tipLabel", UILabel)
RegistChildComponent(LuaSanxingChangeSkillWnd,"node2Empty", GameObject)

RegistClassMember(LuaSanxingChangeSkillWnd, "lightNode")
RegistClassMember(LuaSanxingChangeSkillWnd, "chooseSkillIndex")
RegistClassMember(LuaSanxingChangeSkillWnd, "chooseSkillId")
RegistClassMember(LuaSanxingChangeSkillWnd, "chooseSkillInfoTable")
RegistClassMember(LuaSanxingChangeSkillWnd, "m_Tick")

function LuaSanxingChangeSkillWnd:Close()
	CUIManager.CloseUI(CLuaUIResources.SanxingChangeSkillWnd)
end

function LuaSanxingChangeSkillWnd:OnEnable()
	g_ScriptEvent:AddListener("SanxingBattleSkillUpdate", self, "Init")
end

function LuaSanxingChangeSkillWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SanxingBattleSkillUpdate", self, "Init")
end

function LuaSanxingChangeSkillWnd:Init()
	local onCloseClick = function(go)
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.CloseButton,DelegateFactory.Action_GameObject(onCloseClick),false)

	local onTipClick = function(go)
		g_MessageMgr:ShowMessage('SanxingChangeSkillTip')
	end
	CommonDefs.AddOnClickListener(self.tipBtn,DelegateFactory.Action_GameObject(onTipClick),false)

	if self.lightNode then
		self.lightNode:SetActive(false)
		self.lightNode = nil
	end
	self.chooseSkillId = nil

	self.tipLabel.text = g_MessageMgr:FormatMessage('SanxingChangeSkillTip2')--,LuaSanxingGamePlayMgr.SkillTotalCount)
	--temp data
	--LuaSanxingGamePlayMgr.SkillReplaceCount = 1
	--LuaSanxingGamePlayMgr.SkillTotalCount = 20
	--local nowTime = CServerTimeMgr.Inst.timeStamp + 3600*12
	--LuaSanxingGamePlayMgr.SkillTable = {{92257602,nowTime},{92258204,nowTime}}
	--
	self:InitResCost()

	local replaceLabel = self.replaceBtn.transform:Find('Label'):GetComponent(typeof(UILabel))
	replaceLabel.text = SafeStringFormat3(LocalString.GetString('替换(%s/%s)'),LuaSanxingGamePlayMgr.SkillReplaceCount,LuaSanxingGamePlayMgr.SkillTotalCount)
	if LuaSanxingGamePlayMgr.SkillReplaceCount < LuaSanxingGamePlayMgr.SkillTotalCount then
		--self.replaceBtn:GetComponent(typeof(CButton)).enabled = true
		local onReplaceClick = function(go)
			self:ReplaceSkill()
		end
		CommonDefs.AddOnClickListener(self.replaceBtn,DelegateFactory.Action_GameObject(onReplaceClick),false)
	else
		--self.replaceBtn:GetComponent(typeof(CButton)).enabled = false
		local onReplaceClick = function(go)
			g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString('今日替换次数已满'))
		end
		CommonDefs.AddOnClickListener(self.replaceBtn,DelegateFactory.Action_GameObject(onReplaceClick),false)
	end

	self:InitSkillNode(self.node1,1)
	self:InitSkillNode(self.node2,2)

	if self.m_Tick then
		UnRegisterTick(self.m_Tick)
		self.m_Tick = nil
	end
	self.m_Tick = RegisterTickWithDuration(function ()
		self:CalTime()
	end, 1000, 1000 * 90000)
end

function LuaSanxingChangeSkillWnd:ReplaceSkill()
	if self.chooseSkillId and self.chooseSkillInfoTable and self.chooseSkillInfoTable[self.chooseSkillId] then
		if self.chooseSkillInfoTable[self.chooseSkillId][1] < 3600*3 then
			MessageWndManager.ShowOKCancelMessage(LocalString.GetString('该技能有效期不足3小时,确定要替换吗?'), DelegateFactory.Action(function ()
				self:ReplaceSkill2()
			end), nil, nil, nil, false)
		else
			self:ReplaceSkill2()
		end
	else
		g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString('请选中一个替换技能'))
	end
end

function LuaSanxingChangeSkillWnd:ReplaceSkill2()
	local formulaId = SanXingBattle_Setting.GetData().ReplaceHuTongSkillCostExp
	local formula = formulaId and AllFormulas.Action_Formula[formulaId] and
					AllFormulas.Action_Formula[formulaId].Formula
	local costExp = formula and formula(1,1,{CClientMainPlayer.Inst.Level})
	local costMoney = SanXingBattle_Setting.GetData().ReplaceHuTongSkillCostMoney

	local skillPrivateExp = CClientMainPlayer.Inst.SkillProp.SkillPrivateExp
	local exp = CClientMainPlayer.Inst.PlayProp.Exp

	local freeSilver = CClientMainPlayer.Inst.FreeSilver
	local silver = CClientMainPlayer.Inst.Silver

	local qianshiExp = CClientMainPlayer.Inst.QianShiExp
	--前世经验

	local expEnough = (skillPrivateExp + exp + qianshiExp) >= costExp
	local moneyEnough = (freeSilver + silver >= 0) and ((freeSilver + silver) >= costMoney)
	if not expEnough then
		g_MessageMgr:ShowMessage('CUSTOM_STRING2',LocalString.GetString('经验不足'))
		return
	end
	if not moneyEnough then
		g_MessageMgr:ShowMessage('CUSTOM_STRING2',LocalString.GetString('银票及银两不足'))
		return
	end

	local expEnough1 = skillPrivateExp >= costExp and skillPrivateExp > 0
	local moneyEnough1 = (freeSilver + silver >= 0) and (freeSilver >= costMoney) and freeSilver > 0

	if expEnough1 and moneyEnough1 then
		MessageWndManager.ShowOKCancelMessage(
		SafeStringFormat3(LocalString.GetString('确认替换技能?')),
		DelegateFactory.Action(function ()
			self:RequestChange()
		end), nil, nil, nil, false)
	elseif expEnough1 and not moneyEnough1 then
		if freeSilver > 0 then
			MessageWndManager.ShowOKCancelMessage(
			SafeStringFormat3(LocalString.GetString('银票不足,替换改技能将消耗%s银两,是否继续替换?'),costMoney-freeSilver),
			DelegateFactory.Action(function ()
				self:RequestChange()
			end), nil, nil, nil, false)
		else
			MessageWndManager.ShowOKCancelMessage(
			SafeStringFormat3(LocalString.GetString('确认替换技能?')),
			DelegateFactory.Action(function ()
				self:RequestChange()
			end), nil, nil, nil, false)
		end
	elseif not expEnough1 and  moneyEnough1 then
		if skillPrivateExp > 0 then
			MessageWndManager.ShowOKCancelMessage(
			SafeStringFormat3(LocalString.GetString('专用经验不足,替换该技能将消耗%s角色经验,是否继续替换?'),costExp-skillPrivateExp),
			DelegateFactory.Action(function ()
				self:RequestChange()
			end), nil, nil, nil, false)
		else
			MessageWndManager.ShowOKCancelMessage(
			SafeStringFormat3(LocalString.GetString('确认替换技能?')),
			DelegateFactory.Action(function ()
				self:RequestChange()
			end), nil, nil, nil, false)
		end
	else
		MessageWndManager.ShowOKCancelMessage(
		SafeStringFormat3(LocalString.GetString('专用经验和银票均不足,替换该技能将消耗%s角色经验和%s银两,是否继续替换?'),costExp-skillPrivateExp,costMoney-freeSilver),
		DelegateFactory.Action(function ()
			self:RequestChange()
		end), nil, nil, nil, false)
	end
end

function LuaSanxingChangeSkillWnd:RequestChange()
	Gac2Gas.RequestReplaceMenPaiHuTongSkill(self.chooseSkillIndex,self.chooseSkillId)
	--self:Close()
end

function LuaSanxingChangeSkillWnd:CalTime()
	if self.chooseSkillInfoTable then
		for i,v in pairs(self.chooseSkillInfoTable) do
			local restTime = v[1] - CServerTimeMgr.Inst.timeStamp
			if restTime < 0 then
				restTime = 0
			end
			local hour = math.floor(restTime / 3600)
			local min = math.floor(restTime / 60) - hour*60
			if min == 0 then
				min = 1
			end
			if min < 10 then
				min = '0' ..  min
			end

			v[2].text = LocalString.GetString('有效期:')..hour..':'..min

			if restTime <= 3600*3 then
				v[2].color = Color.red
			end
		end
	end
end

function LuaSanxingChangeSkillWnd:InitSkillNode(node,index)
	if LuaSanxingGamePlayMgr.SkillTable and LuaSanxingGamePlayMgr.SkillTable[index] then
		node:SetActive(true)
		if index == 2 then
			self.node2Empty:SetActive(false)
		end
		local skillId = LuaSanxingGamePlayMgr.SkillTable[index][1]
		local skillClass = math.floor(skillId / 100)
		local skillLevel = skillId - skillClass * 100
		local lastTimeStamp = LuaSanxingGamePlayMgr.SkillTable[index][2]
		if not self.chooseSkillInfoTable then
			self.chooseSkillInfoTable = {}
		end
		self.chooseSkillInfoTable[skillId] = {lastTimeStamp,node.transform:Find('timeLabel'):GetComponent(typeof(UILabel))}

		local skillData = Skill_AllSkills.GetData(skillId)
		node.transform:Find('item/SkillIcon'):GetComponent(typeof(CUITexture)):LoadSkillIcon(skillData.SkillIcon)
		node.transform:Find('name'):GetComponent(typeof(UILabel)).text = skillData.Name
		node.transform:Find('item/LevelLabel'):GetComponent(typeof(UILabel)).text = skillLevel

		node.transform:Find('SkillDescRoot/ScrollView/Table/CurSkillDescItem'):GetComponent(typeof(UILabel)).text = CUICommonDef.TranslateToNGUIText(skillData.Display)

		local clazz = 0
		SanXingBattle_TempSkill.Foreach(function(key, data)
			if data.SkillCls == skillClass then
				clazz = data.Class
			end
		end)

		node.transform:Find('clazz'):GetComponent(typeof(UISprite)).spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), clazz))

		local btn = node.transform:Find('bg').gameObject
		local scrollBtn = node.transform:Find('SkillDescRoot/ScrollViewBg').gameObject
		local selectedNode = node.transform:Find('selected').gameObject

		local onBgClick = function(go)
			if self.lightNode then
				self.lightNode:SetActive(false)
			end
			self.lightNode = selectedNode
			self.lightNode:SetActive(true)

			self.chooseSkillIndex = index
			self.chooseSkillId = skillId
		end
		CommonDefs.AddOnClickListener(btn,DelegateFactory.Action_GameObject(onBgClick),false)
		CommonDefs.AddOnClickListener(scrollBtn,DelegateFactory.Action_GameObject(onBgClick),false)

		if self.chooseSkillIndex and self.chooseSkillIndex == index then
			if self.lightNode then
				self.lightNode:SetActive(false)
			end
			self.lightNode = selectedNode
			self.lightNode:SetActive(true)

			self.chooseSkillIndex = index
			self.chooseSkillId = skillId
		end
	else
		node:SetActive(false)
		if index == 2 then
			self.node2Empty:SetActive(true)
		end
	end
end

function LuaSanxingChangeSkillWnd:InitResCost()
	local formulaId = SanXingBattle_Setting.GetData().ReplaceHuTongSkillCostExp
	local formula = formulaId and AllFormulas.Action_Formula[formulaId] and
					AllFormulas.Action_Formula[formulaId].Formula
	local costExp = formula and formula(1,1,{CClientMainPlayer.Inst.Level})
	local costMoney = SanXingBattle_Setting.GetData().ReplaceHuTongSkillCostMoney

	local skillPrivateExp = CClientMainPlayer.Inst.SkillProp.SkillPrivateExp
	local exp = CClientMainPlayer.Inst.PlayProp.Exp

	local freeSilver = CClientMainPlayer.Inst.FreeSilver
	local silver = CClientMainPlayer.Inst.Silver

	local qianshiExp = CClientMainPlayer.Inst.QianShiExp
	--前世经验

	local expEnough = (skillPrivateExp + exp + qianshiExp) >= costExp
	local moneyEnough = (freeSilver + silver >= 0) and ((freeSilver + silver) >= costMoney)
	self.needExpValue.text = costExp
	self.needMoneyValue.text = costMoney
  --优先消耗专有经验
  if skillPrivateExp > 0 then
    self.ownExpIcon.spriteName = Money.GetIconName(EnumMoneyType.PrivateExp, EnumPlayScoreKey.NONE)
    self.needExpIcon.spriteName = self.ownExpIcon.spriteName
    self.ownExpValue.text = skillPrivateExp
  elseif qianshiExp > 0 then
    self.ownExpIcon.spriteName = Money.GetIconName(EnumMoneyType.QianShiExp, EnumPlayScoreKey.NONE)
    self.needExpIcon.spriteName = self.ownExpIcon.spriteName
    self.ownExpValue.text = qianshiExp
  else
    self.ownExpIcon.spriteName = Money.GetIconName(EnumMoneyType.Exp, EnumPlayScoreKey.NONE)
    self.needExpIcon.spriteName = self.ownExpIcon.spriteName
    self.ownExpValue.text = exp
  end
  --优先消耗银票
  if freeSilver > 0 then
    self.ownMoneyIcon.spriteName = Money.GetIconName(EnumMoneyType.YinPiao, EnumPlayScoreKey.NONE)
    self.needMoneyIcon.spriteName = self.ownMoneyIcon.spriteName
    self.ownMoneyValue.text = freeSilver
  else
    self.ownMoneyIcon.spriteName = Money.GetIconName(EnumMoneyType.YinLiang, EnumPlayScoreKey.NONE)
    self.needMoneyIcon.spriteName = self.ownMoneyIcon.spriteName
    self.ownMoneyValue.text = silver
  end

	if expEnough then
    self.ownExpValue.color = Color.white
	else
    self.ownExpValue.color = Color.red
	end
	if moneyEnough then
		self.ownMoneyValue.color = Color.white
	else
		self.ownMoneyValue.color = Color.red
	end
end

function LuaSanxingChangeSkillWnd:OnDestroy()
	if self.m_Tick then
		UnRegisterTick(self.m_Tick)
		self.m_Tick = nil
	end
end

return LuaSanxingChangeSkillWnd
