local TweenAlpha = import "TweenAlpha"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CPlayerHeadInfoItem = import "L10.UI.CPlayerHeadInfoItem"
local CSkillMgr = import "L10.Game.CSkillMgr"
local CCustomHpAndMpBar = import "L10.UI.CCustomHpAndMpBar"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"

LuaPlayerHeadInfoItemDieKeProgress = class()

RegistChildComponent(LuaPlayerHeadInfoItemDieKeProgress,"m_DiekeProgressBar","DiekeProgressBar", UISprite)
RegistChildComponent(LuaPlayerHeadInfoItemDieKeProgress,"m_DieKeProgressFx","DieKeProgressFx", CUIFx)
RegistChildComponent(LuaPlayerHeadInfoItemDieKeProgress,"m_DiekeProgress","DiekeProgress", GameObject)
RegistChildComponent(LuaPlayerHeadInfoItemDieKeProgress,"m_DiekeProgressTexture","DiekeProgressTexture", UITexture)
RegistChildComponent(LuaPlayerHeadInfoItemDieKeProgress,"m_DiekeProgressBarBg","DiekeProgressBarBg", UISprite)
RegistChildComponent(LuaPlayerHeadInfoItemDieKeProgress,"m_DiekeProcessingHpBar","DiekeProcessingHpBar", CCustomHpAndMpBar)

RegistClassMember(LuaPlayerHeadInfoItemDieKeProgress, "m_FlickerProgressBarTween")
RegistClassMember(LuaPlayerHeadInfoItemDieKeProgress, "m_FlickerProgressTextureTween")
RegistClassMember(LuaPlayerHeadInfoItemDieKeProgress, "m_HideTick")
RegistClassMember(LuaPlayerHeadInfoItemDieKeProgress, "m_Head")
RegistClassMember(LuaPlayerHeadInfoItemDieKeProgress, "m_EngineId")
RegistClassMember(LuaPlayerHeadInfoItemDieKeProgress, "m_LastUpdateTime")

function LuaPlayerHeadInfoItemDieKeProgress:OnDisable()
    self:Hide()
end

function LuaPlayerHeadInfoItemDieKeProgress:OnEnable()
    self.m_Head = CommonDefs.NGUI_FindInParents(self.gameObject, typeof(CPlayerHeadInfoItem))
    self.m_EngineId = self.m_Head.objId
    local success = self:IsShowPlayerHeadInfoItemDieKeProgress()
    self.gameObject:SetActive(success)
    if success then
        self:InitDieKeProgress()
    end
end

function LuaPlayerHeadInfoItemDieKeProgress:Awake()
    self.m_DiekeProcessingHpBar.gameObject:SetActive(false)
end

function LuaPlayerHeadInfoItemDieKeProgress:Update()
    if Time.realtimeSinceStartup < (self.m_LastUpdateTime+ 0.1) then return end
    self.m_LastUpdateTime = Time.realtimeSinceStartup
    local mainPlayer = CClientMainPlayer.Inst
    if not mainPlayer then return end
    local totalTime = mainPlayer.DieKePossessingTotalTime
    local theRestOfTime = mainPlayer.DieKeEndPossessingExpiredTime - CServerTimeMgr.Inst.timeStamp
    local v = theRestOfTime  / totalTime
    self.m_DiekeProgressBar.fillAmount = v > 1 and 1 or v
    if theRestOfTime <= 3 and theRestOfTime > 0 then
        self.m_FlickerProgressBarTween.enabled = true
        self.m_FlickerProgressTextureTween.enabled = true
    end
    if theRestOfTime <= 0 then
        self:TryToHide()
    end
end

function LuaPlayerHeadInfoItemDieKeProgress:IsShowPlayerHeadInfoItemDieKeProgress()
    return CClientMainPlayer.Inst and 
        CClientMainPlayer.Inst.DieKeEndPossessingExpiredTime > CServerTimeMgr.Inst.timeStamp and 
        CSkillMgr.Inst:GetPossessTargetEngineId(CClientMainPlayer.Inst) == self.m_EngineId
end

function LuaPlayerHeadInfoItemDieKeProgress:OnHpOrCurrentHpFullUpdate(args)
    local co = CClientObjectMgr.Inst:GetObject(self.m_EngineId)
    if not co then return end
    self.m_DiekeProcessingHpBar:UpdateValue(co.Hp,co.CurrentHpFull,co.HpFull)
end

function LuaPlayerHeadInfoItemDieKeProgress:InitDieKeProgress()
    local mainPlayer = CClientMainPlayer.Inst
    if not mainPlayer then return end

    self.m_LastUpdateTime= Time.realtimeSinceStartup
    self.m_FlickerProgressBarTween = self.m_DiekeProgressBar.gameObject:GetComponent(typeof(TweenAlpha))
    self.m_FlickerProgressTextureTween = self.m_DiekeProgressTexture.gameObject:GetComponent(typeof(TweenAlpha))
    self.m_FlickerProgressBarTween.enabled = false
    self.m_FlickerProgressTextureTween.enabled = false
    self.m_DiekeProgressBar.alpha = 1
    self.m_DiekeProgressTexture.alpha = 1
    self.m_DiekeProgressBarBg.alpha = 1

    local isAggressivePossessing = CSkillMgr.Inst:IsAggressivePossessing(mainPlayer)
    self.m_DiekeProgressBar.color = NGUIText.ParseColor24(isAggressivePossessing and "FF3434" or "92FFD6", 0)
    self.m_DiekeProgressTexture.gameObject:GetComponent(typeof(CUITexture)):LoadMaterial(SafeStringFormat3("UI/Texture/Transparent/Material/mainplayerframe_dieke_butterfly_%s.mat",isAggressivePossessing and "red" or "green"))
    self.m_DiekeProgressBar.gameObject:SetActive(mainPlayer.DieKeEndPossessingExpiredTime > CServerTimeMgr.Inst.timeStamp)
    
    self.m_DiekeProcessingHpBar.gameObject:SetActive(true)
    self.m_DiekeProcessingHpBar.curValueBar.foregroundWidget.spriteName = isAggressivePossessing and "common_hpandmp_hp" or "common_hpandmp_mp"
    self:OnHpOrCurrentHpFullUpdate()

    self.m_Head.needReposition = true
end

function LuaPlayerHeadInfoItemDieKeProgress:TryToHide()
    self.m_FlickerProgressBarTween.enabled = false
    self.m_FlickerProgressTextureTween.enabled = false
    local tween1 = LuaTweenUtils.TweenAlpha(self.m_DiekeProgressTexture, 1,0, 0.2,function()
        self.gameObject:SetActive(false)
    end)
    local tween2 = LuaTweenUtils.TweenAlpha(self.m_DiekeProgressBar, 1,0, 0.2,function() end)
    local tween3 = LuaTweenUtils.TweenScale(self.m_DiekeProgress.transform, Vector3(1, 1, 1), Vector3(0.8 , 0.8, 1), 0.2)
    local tween4 = LuaTweenUtils.TweenAlpha(self.m_DiekeProgressBarBg, 1,0, 0.2,function() end)
    LuaTweenUtils.SetTarget(tween1,self.transform)
    LuaTweenUtils.SetTarget(tween2,self.transform)
    LuaTweenUtils.SetTarget(tween3,self.transform)
    LuaTweenUtils.SetTarget(tween4,self.transform)
end

function LuaPlayerHeadInfoItemDieKeProgress:Hide()
    LuaTweenUtils.DOKill(self.transform, false)
    if self.m_Head then
        self.m_Head.needReposition = true
    end
    self.m_DiekeProcessingHpBar.gameObject:SetActive(false)
end
