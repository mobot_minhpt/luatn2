local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local CMainCamera = import "L10.Engine.CMainCamera"
local CGamePlayMgr=import "L10.Game.CGamePlayMgr"
local CScene = import "L10.Game.CScene"
local Color = import "UnityEngine.Color"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local Ease = import "DG.Tweening.Ease"
local UIRoot = import "UIRoot"
local Quaternion = import "UnityEngine.Quaternion"
local Screen = import "UnityEngine.Screen"
local Application = import "UnityEngine.Application"
local RuntimePlatform = import "UnityEngine.RuntimePlatform"
local Physics = import "UnityEngine.Physics"
local UILabelOverflow = import "UILabel+Overflow"
local Alignment = import "NGUIText.Alignment"
local CSocialWndMgr = import "L10.UI.CSocialWndMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local CMiddleNoticeCenterWnd = import "L10.UI.CMiddleNoticeCenterWnd"
local LayerDefine = import "L10.Engine.LayerDefine"
local UIWidget = import "UIWidget"

LuaJiaoYueDuoSuYiInGameWnd = class()

RegistClassMember(LuaJiaoYueDuoSuYiInGameWnd, "m_MsgTick")
RegistClassMember(LuaJiaoYueDuoSuYiInGameWnd, "m_MsgOffsetTick")
RegistClassMember(LuaJiaoYueDuoSuYiInGameWnd, "m_CameraCtrlTick")
RegistClassMember(LuaJiaoYueDuoSuYiInGameWnd, "m_Tweeners")
RegistClassMember(LuaJiaoYueDuoSuYiInGameWnd, "m_Left")
RegistClassMember(LuaJiaoYueDuoSuYiInGameWnd, "m_Right")
RegistClassMember(LuaJiaoYueDuoSuYiInGameWnd, "m_IsDragging")
RegistClassMember(LuaJiaoYueDuoSuYiInGameWnd, "m_ModelLoader")
RegistClassMember(LuaJiaoYueDuoSuYiInGameWnd, "m_OriVPos") -- 挡板初始位置（即UI上的位置
RegistClassMember(LuaJiaoYueDuoSuYiInGameWnd, "m_OriHPos")
RegistClassMember(LuaJiaoYueDuoSuYiInGameWnd, "m_OriVScale") -- 挡板初始大小（即UI上的大小 
RegistClassMember(LuaJiaoYueDuoSuYiInGameWnd, "m_OriHScale")
RegistClassMember(LuaJiaoYueDuoSuYiInGameWnd, "m_OriVTex")
RegistClassMember(LuaJiaoYueDuoSuYiInGameWnd, "m_OriHTex")
RegistClassMember(LuaJiaoYueDuoSuYiInGameWnd, "m_DragX")
RegistClassMember(LuaJiaoYueDuoSuYiInGameWnd, "m_DragY")


--@region RegistChildComponent: Dont Modify Manually!


--@endregion RegistChildComponent end

function LuaJiaoYueDuoSuYiInGameWnd:Awake()
    self.m_MsgTick = {}
    self.m_MsgOffsetTick = {}

    self.m_ModelLoader = LuaDefaultModelTextureLoader.Create(function (ro)
        ro.Scale = 1
        self:LoadModel(ro)
    end)

    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

    CommonDefs.AddOnClickListener(
        self.transform:Find("Anchor/Right/CloseButton").gameObject, 
        DelegateFactory.Action_GameObject(function()
            if not LuaZhongQiu2022Mgr.bIsGameEnd then
                g_MessageMgr:ShowOkCancelMessage(LocalString.GetString("确定要#R认输#n并退出吗？"), function()
                    CGamePlayMgr.Inst:LeavePlay()
                end, nil, nil, nil, false)
            else
                CGamePlayMgr.Inst:LeavePlay()
            end
        end), 
        false
    ) 

    CommonDefs.AddOnClickListener(
        self.transform:Find("Anchor/Left/ChatButton").gameObject, 
        DelegateFactory.Action_GameObject(function()
            CSocialWndMgr.ShowChatWnd()
        end), 
        false
    ) 

    CommonDefs.AddOnClickListener(
        self.transform:Find("Anchor/Right/ChatButton").gameObject, 
        DelegateFactory.Action_GameObject(function()
            CSocialWndMgr.ShowChatWnd()
        end), 
        false
    ) 

    self.m_Left = self.transform:Find("Anchor/Left")
    self.m_Right = self.transform:Find("Anchor/Right")
    --self.m_Left:Find("Highlighted").gameObject:SetActive(false)
    --self.m_Right:Find("Highlighted").gameObject:SetActive(false)
    self.m_Left:Find("Mask").gameObject:SetActive(false)
    self.m_Right:Find("Mask").gameObject:SetActive(false)

    self.transform:Find("Anchor/Left/Countdown").gameObject:SetActive(false)
    self.transform:Find("Anchor/Right/Countdown").gameObject:SetActive(false)
    self.transform:Find("Anchor/Left/MyBarrier").gameObject:SetActive(false)
    self.transform:Find("Anchor/Right/MyBarrier").gameObject:SetActive(false)
    self.transform:Find("Anchor/Left/EnemyBarrier").gameObject:SetActive(false)
    self.transform:Find("Anchor/Right/EnemyBarrier").gameObject:SetActive(false)
end

function LuaJiaoYueDuoSuYiInGameWnd:Init()
    RegisterTickOnce(function()
        if LuaZhongQiu2022Mgr:IsInPlay() then
            self:JYDSY_SyncPlayerInfo()
        end
    end, 33)
    self.transform:GetComponent(typeof(UIPanel)).depth = 1
end

function LuaJiaoYueDuoSuYiInGameWnd:Update()
    if LuaZhongQiu2022Mgr.bIsOnMove and not LuaZhongQiu2022Mgr.bIsGameEnd and not CUICommonDef.IsOverUI and Input.GetMouseButtonDown(0) then       
        local ray = CMainCamera.Main:ScreenPointToRay(Input.mousePosition)
        --local hits = Physics.RaycastAll(ray, 99999, bit.lshift(1, LayerDefine.Default))
        local hits = Physics.RaycastAll(ray, 99999, CMainCamera.Main.cullingMask)
        for i = 0, hits.Length - 1 do
            if hits[i].collider.transform.parent.name == "GridsRoot" then
                LuaZhongQiu2022Mgr:GridClickFunc(hits[i].collider.gameObject)
            end
        end
    end
end

function LuaJiaoYueDuoSuYiInGameWnd:OnDestroy()
    self:ClearTweeners()
    self:CancelMsgTick(0)
    self:CancelMsgTick(1)
    self:CancelMsgOffsetTick(0)
    self:CancelMsgOffsetTick(1)
    self:CancelCameraCtrlTick()
end

function LuaJiaoYueDuoSuYiInGameWnd:OnEnable()
    self:JYDSY_UpdateRoundInfo()

    g_ScriptEvent:AddListener("JYDSY_SyncPlayerInfo", self, "JYDSY_SyncPlayerInfo")
    g_ScriptEvent:AddListener("JYDSY_SyncRoundTime", self, "JYDSY_SyncRoundTime")
    g_ScriptEvent:AddListener("JYDSY_UpdateRoundInfo", self, "JYDSY_UpdateRoundInfo")
    --g_ScriptEvent:AddListener("JYDSY_SyncBarrierInfo", self, "JYDSY_SyncBarrierInfo")
    g_ScriptEvent:AddListener("JYDSY_ShowBubbleMsg", self, "JYDSY_ShowBubbleMsg")

    local baseUI = CUIManager.instance:GetBaseUIRoot()
    for i = 0, baseUI.childCount - 1 do
        local ch = baseUI:GetChild(i)
        if not ch:GetComponent(typeof(CMiddleNoticeCenterWnd)) then
            ch:GetComponent(typeof(UIPanel)).alpha = 0
        end
    end

    CameraFollow.Inst:BeginAICamera(Vector3.zero, Vector3.zero, 3600, true)

    self:CancelCameraCtrlTick()
    RegisterTickOnce(function()
        if LuaZhongQiu2022Mgr:IsInPlay() then
            self:SetCamera()
        end
    end, 33)
    self.m_CameraCtrlTick = RegisterTick(function()
        if LuaZhongQiu2022Mgr:IsInPlay() then
            self:SetCamera()
        end
    end, 1000)
end

function LuaJiaoYueDuoSuYiInGameWnd:OnDisable()
    g_ScriptEvent:RemoveListener("JYDSY_SyncPlayerInfo", self, "JYDSY_SyncPlayerInfo")
    g_ScriptEvent:RemoveListener("JYDSY_SyncRoundTime", self, "JYDSY_SyncRoundTime")
    g_ScriptEvent:RemoveListener("JYDSY_UpdateRoundInfo", self, "JYDSY_UpdateRoundInfo")
    --g_ScriptEvent:RemoveListener("JYDSY_SyncBarrierInfo", self, "JYDSY_SyncBarrierInfo")
    g_ScriptEvent:RemoveListener("JYDSY_ShowBubbleMsg", self, "JYDSY_ShowBubbleMsg")

    CameraFollow.Inst:EndAICamera()

    local baseUI = CUIManager.instance:GetBaseUIRoot()
    for i = 0, baseUI.childCount - 1 do
        local ch = baseUI:GetChild(i)
        ch:GetComponent(typeof(UIPanel)).alpha = 1
    end

    --if CameraFollow.Inst and CClientMainPlayer.Inst and CClientMainPlayer.Inst.RO then
    --    CameraFollow.Inst:SetFollowObj(CClientMainPlayer.Inst.RO, nil)
    --end
end

--@region UIEvent

--@endregion UIEvent

function LuaJiaoYueDuoSuYiInGameWnd:SetCamera()
    if CameraFollow.Inst then
        local camCtrl = ZhongQiu2022_Setting.GetData().MoonCakeCameraCtrl
        local camCtrl2 = ZhongQiu2022_Setting.GetData().MoonCakeCameraCtrl2

        CameraFollow.Inst:StopFollow()

        local curRatio = CommonDefs.GameScreenWidth * 1.0 / CommonDefs.GameScreenHeight
        if (Application.platform == RuntimePlatform.WindowsPlayer or Application.platform == RuntimePlatform.WindowsEditor)
            and curRatio < ZhongQiu2022_Setting.GetData().MoonCakeLimitResolution then
            g_MessageMgr:ShowMessage("2022Zhongqiu_MoonCakeLimitResolution")
        end
        if curRatio < ZhongQiu2022_Setting.GetData().MoonCakeCamera2Resolution then
            CameraFollow.Inst.transform.localPosition = Vector3(camCtrl2[0], camCtrl2[1], camCtrl2[2])
            CameraFollow.Inst.rotateObj.eulerAngles = Vector3(camCtrl2[3], camCtrl2[4], camCtrl2[5])
        else
            CameraFollow.Inst.transform.localPosition = Vector3(camCtrl[0], camCtrl[1], camCtrl[2])
            CameraFollow.Inst.rotateObj.eulerAngles = Vector3(camCtrl[3], camCtrl[4], camCtrl[5])
        end
    end
end

function LuaJiaoYueDuoSuYiInGameWnd:GetRoundSide()
    if LuaZhongQiu2022Mgr.bIsOnMove then
        if LuaZhongQiu2022Mgr.MyRole == 1 then
            return self.transform:Find("Anchor/Left")
        else
            return self.transform:Find("Anchor/Right")
        end
    else
        if LuaZhongQiu2022Mgr.MyRole == 0 then
            return self.transform:Find("Anchor/Left")
        else
            return self.transform:Find("Anchor/Right")
        end
    end
end

function LuaJiaoYueDuoSuYiInGameWnd:JYDSY_SyncPlayerInfo()
    local L = self.m_Left
    local R = self.m_Right
    local my, enemy
    if LuaZhongQiu2022Mgr.MyRole == 1 then
        my = L
        enemy = R
    else
        my = R
        enemy = L
    end

    local myInfo = LuaZhongQiu2022Mgr:GetPlayerInfo(LuaZhongQiu2022Mgr.MyRole)
    local enemyInfo = LuaZhongQiu2022Mgr:GetPlayerInfo(1-LuaZhongQiu2022Mgr.MyRole)
    
    my:Find("Avatar"):GetComponent(typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(myInfo.cls, myInfo.gender, -1))
    enemy:Find("Avatar"):GetComponent(typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(enemyInfo.cls, enemyInfo.gender, -1))
    my:Find("Name"):GetComponent(typeof(UILabel)).text = myInfo.name
    enemy:Find("Name"):GetComponent(typeof(UILabel)).text = enemyInfo.name
    my:Find("ChatButton").gameObject:SetActive(true)
    enemy:Find("ChatButton").gameObject:SetActive(false)
end

function LuaJiaoYueDuoSuYiInGameWnd:JYDSY_SyncRoundTime(roundTime)
    self.transform:Find("Anchor/Left/Countdown").gameObject:SetActive(false)
    self.transform:Find("Anchor/Right/Countdown").gameObject:SetActive(false)
    local root = self:GetRoundSide()
    local cd = root:Find("Countdown")
    cd.gameObject:SetActive(true)
    cd:GetComponent(typeof(UILabel)).text = roundTime
    local foreSp = cd:Find("Foreground"):GetComponent(typeof(UISprite))
    if ZhongQiu2022_Setting.GetData().TimeNotenough >= roundTime then
        local col = NGUIText.ParseColor("e72460", 0)
        foreSp.color = Color(col.r, col.g, col.b, 153 / 255)
        self:ClearTweeners()
        local tweener = LuaTweenUtils.TweenFloat(0, 153 / 255, 0.3, function(val)
            foreSp.alpha = val
        end)
        LuaTweenUtils.SetEase(tweener, Ease.Linear)
        table.insert(self.m_Tweeners, tweener)
    else
        local col = NGUIText.ParseColor("23984c", 0)
        foreSp.color = Color(col.r, col.g, col.b, 153 / 255)
    end
    foreSp.fillAmount = roundTime / ZhongQiu2022_Setting.GetData().MoonCakeMaxTime
end

function LuaJiaoYueDuoSuYiInGameWnd:JYDSY_UpdateRoundInfo()
    local L = self.m_Left
    local R = self.m_Right
    local my, enemy
    if LuaZhongQiu2022Mgr.MyRole == 1 then
        my = L
        enemy = R
    else
        my = R
        enemy = L
    end

    my:Find("MyBarrier").gameObject:SetActive(true)
    --my:Find("MyBarrier/Mask").gameObject:SetActive(not LuaZhongQiu2022Mgr.bIsOnMove)
    my:Find("EnemyBarrier").gameObject:SetActive(false)
    --my:Find("Highlighted").gameObject:SetActive(LuaZhongQiu2022Mgr.bIsOnMove)
    my:Find("Mask").gameObject:SetActive(not LuaZhongQiu2022Mgr.bIsOnMove)
    enemy:Find("MyBarrier").gameObject:SetActive(false)
    enemy:Find("EnemyBarrier").gameObject:SetActive(true)
    --enemy:Find("Highlighted").gameObject:SetActive(not LuaZhongQiu2022Mgr.bIsOnMove)
    enemy:Find("Mask").gameObject:SetActive(LuaZhongQiu2022Mgr.bIsOnMove)
    enemy:Find("Mask/Label").gameObject:SetActive(false)
    
    my:Find("MyBarrier/Remain/Label"):GetComponent(typeof(UILabel)).text = LuaZhongQiu2022Mgr.BarrierNum[LuaZhongQiu2022Mgr.MyRole]
    enemy:Find("EnemyBarrier/Remain/Label"):GetComponent(typeof(UILabel)).text = LuaZhongQiu2022Mgr.BarrierNum[1 - LuaZhongQiu2022Mgr.MyRole]

    local v = my:Find("MyBarrier/VertBg/Vertical").gameObject
    local h = my:Find("MyBarrier/HoriBg/Horizontal").gameObject

    self.m_IsDragging = false
    self:OnDragEnd(v, true)
    self:OnDragEnd(h, true)

    if self.m_OriVPos then
        v.transform.localPosition = self.m_OriVPos
    end
    if self.m_OriHPos then
        h.transform.localPosition = self.m_OriHPos
    end
    
    self.m_OriVPos = v.transform.localPosition
    self.m_OriHPos = h.transform.localPosition
    self.m_OriVScale = v.transform.localScale
    self.m_OriHScale = h.transform.localScale
    
    self.m_OriVTex = v:GetComponent(typeof(CUITexture)).material
    self.m_OriHTex = h:GetComponent(typeof(CUITexture)).material

    CommonDefs.AddOnDragListener(v, DelegateFactory.Action_GameObject_Vector2(function (go, delta)
        self:OnDrag(go, delta)
    end), false)
    UIEventListener.Get(v).onDragStart = DelegateFactory.VoidDelegate(function(go)
        self:OnDragStart(go)
    end)
    UIEventListener.Get(v).onDragEnd = DelegateFactory.VoidDelegate(function(go)
        self:OnDragEnd(go)
    end)
    CommonDefs.AddOnDragListener(h, DelegateFactory.Action_GameObject_Vector2(function (go, delta)
        self:OnDrag(go, delta)
    end), false)
    UIEventListener.Get(h).onDragStart = DelegateFactory.VoidDelegate(function(go)
        self:OnDragStart(go)
    end)
    UIEventListener.Get(h).onDragEnd = DelegateFactory.VoidDelegate(function(go)
        self:OnDragEnd(go)
    end)
end

function LuaJiaoYueDuoSuYiInGameWnd:LoadModel(ro)
    local modelPath = "assets/res/character/npc/lnpc942/prefab/lnpc942_01.prefab"
    ro:LoadMain(modelPath, nil, false, false, false)
end

function LuaJiaoYueDuoSuYiInGameWnd:OnDrag(go, delta)
    if not LuaZhongQiu2022Mgr.bIsOnMove or LuaZhongQiu2022Mgr.bIsGameEnd or CUIManager.IsLoaded(CUIResources.SocialWnd) then
        return 
    end
    if LuaZhongQiu2022Mgr.BarrierNum[LuaZhongQiu2022Mgr.MyRole] == 0 then
        return
    end
    local scale = UIRoot.GetPixelSizeAdjustment(go)
    local pos = go.transform.localPosition
    pos.x = pos.x + delta.x * scale
    pos.y = pos.y + delta.y * scale
    go.transform.localPosition = pos

    LuaZhongQiu2022Mgr:ClearGridObjs()
    local i, j = LuaZhongQiu2022Mgr:GetGridByNGUIPos(go.transform.position)
    -- 坐标原点左上角
    -- print(i, j)

    local isV = go.name == "Vertical"
    -- 坐标调整为和服务器一致
    -- 客户端基准点右下，服务器基准点左上
    if isV then
        self.m_DragX = i - 2
        self.m_DragY = j - 3
    else
        self.m_DragX = i - 3
        self.m_DragY = j - 2
    end
    local ok = LuaZhongQiu2022Mgr:CheckBoundary(isV, self.m_DragX, self.m_DragY) and LuaZhongQiu2022Mgr:CheckOverlap(isV, self.m_DragX, self.m_DragY)
    local dir = { {-1, -3}, {-3, -3}, {-1, -1}, {-3, -1} }
    for k = 1, 4 do
        local nx = i + dir[k][1]
        local ny = j + dir[k][2]
        if nx >= 1 and nx <= LuaZhongQiu2022Mgr.m_RowNum and ny >= 1 and ny <= LuaZhongQiu2022Mgr.m_ColNum then
            LuaZhongQiu2022Mgr:HighlightGrid(not ok, nx, ny)
        end
    end
end

function LuaJiaoYueDuoSuYiInGameWnd:OnDragStart(go)
    if not LuaZhongQiu2022Mgr.bIsOnMove or LuaZhongQiu2022Mgr.bIsGameEnd or CUIManager.IsLoaded(CUIResources.SocialWnd) then
        return 
    end
    if LuaZhongQiu2022Mgr.BarrierNum[LuaZhongQiu2022Mgr.MyRole] == 0 then
        g_ScriptEvent:BroadcastInLua("JYDSY_ShowBubbleMsg", 13)
        return
    end
    if not self.m_IsDragging then
        local isV = go.name == "Vertical" 
        local tex = go:GetComponent(typeof(CUITexture))
        tex.material = nil
        if isV then 
            tex.mainTexture = CUIManager.CreateModelTexture(SafeStringFormat3("__%s__", tostring(go:GetInstanceID())), self.m_ModelLoader, 180, 0, 0, 6, false, true, 2, true, false)
            local vmodelRt = CUIManager.instance.transform:Find(SafeStringFormat3("__%s__", tostring(go:GetInstanceID())).."/ModelRoot")
            if vmodelRt then vmodelRt.localRotation = Quaternion.Euler(50, 180, 0.6) end
            go:GetComponent(typeof(UIWidget)).pivot = UIWidget.Pivot.Center
            LuaUtils.SetLocalRotationZ(go.transform, 0)
            go:GetComponent(typeof(UIWidget)).pivot = UIWidget.Pivot.BottomRight
        else
            tex.mainTexture = CUIManager.CreateModelTexture(SafeStringFormat3("__%s__", tostring(go:GetInstanceID())), self.m_ModelLoader, 90, 0, 0, 6, false, true, 2, true, false)
            local hmodelRt = CUIManager.instance.transform:Find(SafeStringFormat3("__%s__", tostring(go:GetInstanceID())).."/ModelRoot")
            if hmodelRt then hmodelRt.localRotation = Quaternion.Euler(0, 90, -60) end
        end
        local scale = go.transform.localScale
        go.transform.localScale = Vector3(scale.x * 2, scale.y * 2, scale.z * 2)
    end
    self.m_IsDragging = true
end

function LuaJiaoYueDuoSuYiInGameWnd:OnDragEnd(go, init)
    if not init then
        LuaZhongQiu2022Mgr:ClearGridObjs()
        LuaZhongQiu2022Mgr:ShowNextMove()
    end
    local isV = go.name == "Vertical" 
    if isV then
        go:GetComponent(typeof(UIWidget)).pivot = UIWidget.Pivot.Center
        LuaUtils.SetLocalRotationZ(go.transform, 90)
        go:GetComponent(typeof(UIWidget)).pivot = UIWidget.Pivot.BottomRight
        if self.m_OriVPos then go.transform.localPosition = self.m_OriVPos end
        if self.m_OriVScale then go.transform.localScale = self.m_OriVScale end
        if self.m_OriVTex then 
            local tex = go:GetComponent(typeof(CUITexture))
            tex.mainTexture = nil
            CUIManager.DestroyModelTexture(SafeStringFormat3("__%s__", tostring(go:GetInstanceID())))
            tex.material = self.m_OriVTex
        end
    else
        if self.m_OriHPos then go.transform.localPosition = self.m_OriHPos end
        if self.m_OriHScale then go.transform.localScale = self.m_OriHScale end
        if self.m_OriHTex then 
            local tex = go:GetComponent(typeof(CUITexture))
            tex.mainTexture = nil
            CUIManager.DestroyModelTexture(SafeStringFormat3("__%s__", tostring(go:GetInstanceID())))
            tex.material = self.m_OriHTex 
        end
    end
    if self.m_IsDragging then
        if LuaZhongQiu2022Mgr:CheckBoundary(isV, self.m_DragX, self.m_DragY, true) and LuaZhongQiu2022Mgr:CheckOverlap(isV, self.m_DragX, self.m_DragY, true) then
            isV = not isV -- 取反，调整为和服务器一致
            Gac2Gas.JYDSY_RequestPutBarrier(isV, self.m_DragX, self.m_DragY)
        end
    end
    self.m_IsDragging = false
end

--function LuaJiaoYueDuoSuYiInGameWnd:JYDSY_SyncBarrierInfo(boardBarrierInfo)

--end

function LuaJiaoYueDuoSuYiInGameWnd.GetTextLength(text) --计算汉字的长度(表情算2个汉字)
    local cur = 1
    local cnt = 0
    local emo = 0
    local len = string.len(text)
    while true do
        local step = 1
        local byteVal = string.byte(text, cur)
        if byteVal > 239 then
            step = 4
        elseif byteVal > 223 then
            step = 3
        elseif byteVal > 191 then
            step = 2
        else
            local char = string.char(byteVal)
            if char == '#' or char >= 'a' and char <= 'z' or char >= 'A' and char <= 'Z' then
                cnt = cnt - 1
            end
            if char >= '0' and char <= '9' then
                cnt = cnt - 1 
                if cur - 1 > 0 then 
                    local preChar = string.sub(text, cur - 1, cur - 1)
                    if preChar and preChar == '#' then
                        emo = emo + 1
                    end
                end
            end
            step = 1
        end
        cnt = cnt + 1
        cur = cur + step
        if cur > len then
            break
        end
    end
    return cnt + emo * 2
end

function LuaJiaoYueDuoSuYiInGameWnd:JYDSY_ShowBubbleMsg(msgId, msgContent)
    if msgId == 0 then
        self:CancelMsgTick(LuaZhongQiu2022Mgr.MyRole)
        if LuaZhongQiu2022Mgr.MyRole == 1 then
            self.transform:Find("Anchor/Left/Header/Msg").gameObject:SetActive(false)
        else
            self.transform:Find("Anchor/Right/Header/Msg").gameObject:SetActive(false)
        end
        return
    end
    local msgData = ZhongQiu2022_MoonCakeMsg.GetData(msgId)
    if not msgData then
        return 
    end
    local msg
    if LuaZhongQiu2022Mgr.MyRole == 1 then
        msg = self.transform:Find("Anchor/Left/Header/Msg")
    else
        msg = self.transform:Find("Anchor/Right/Header/Msg")
    end
    
    local side = msg.parent.parent.gameObject.name == "Left" and 1 or 0
    self:CancelMsgTick(side)
    msg.gameObject:SetActive(false)

    local hl = msg:Find("Highlighted")
    local bg = msg:Find("Bg")
    hl.gameObject:SetActive(false)
    bg.gameObject:SetActive(false)

    local msgLabel = msg:Find("Bg/MsgLabel"):GetComponent(typeof(UILabel))
    if side == 0 then
        msgLabel.alignment = Alignment.Right
    end
    msgLabel.text = msgContent or CUICommonDef.TranslateToNGUIText(msgData.Message)

    -- 15个汉字的width~=480     1:32
    local adjustAlign = false
    local width = 32 * self.GetTextLength(msgContent or msgData.Message) + 40
    if width > 480 + 40 then
        width = 480 + 40
        if side == 0 then
            adjustAlign = true
        end
    end

    if adjustAlign then
        RegisterTickOnce(function()
            if not CommonDefs.IsNull(msgLabel) then
                msgLabel.alignment = Alignment.Right 
                msgLabel.alignment = Alignment.Left 
            end
        end, 66)
    end

    hl:GetComponent(typeof(UITexture)).width = width
    bg:GetComponent(typeof(UITexture)).width = width
    
    local close = msg:Find("CloseMsg")
    if side == 1 then
        LuaUtils.SetLocalPositionX(close, bg:GetComponent(typeof(UITexture)).width + bg.localPosition.x)
        LuaUtils.SetLocalPositionY(close, 42)
    else
        LuaUtils.SetLocalPositionX(close, bg.localPosition.x - bg:GetComponent(typeof(UITexture)).width)
        LuaUtils.SetLocalPositionY(close, 35)
    end
    self:CancelMsgOffsetTick(side)
    self.m_MsgOffsetTick[side] = RegisterTickOnce(function()
        if not CommonDefs.IsNull(msg) then
            local close = msg:Find("CloseMsg")
            if not CommonDefs.IsNull(close) then
                if side == 1 then
                    LuaUtils.SetLocalPositionX(close, bg:GetComponent(typeof(UITexture)).width + bg.localPosition.x)
                else
                    LuaUtils.SetLocalPositionX(close, bg.localPosition.x - bg:GetComponent(typeof(UITexture)).width)
                end
            end
        end
    end, 500)

    hl.gameObject:SetActive(msgData.DisplayMode == 2)
    bg.gameObject:SetActive(true)

    msg.gameObject:SetActive(true)

    self.m_MsgTick[side] = RegisterTickOnce(function()
        if not CommonDefs.IsNull(msg) then
            msg.gameObject:SetActive(false)
        end
    end, 1000 * msgData.Duration)

    CommonDefs.AddOnClickListener(
        msg:Find("CloseMsg").gameObject, 
        DelegateFactory.Action_GameObject(function()
            self:CancelMsgTick(side)
            msg.gameObject:SetActive(false)
        end), 
        false
    ) 
end

function LuaJiaoYueDuoSuYiInGameWnd:CancelMsgTick(side) -- 0:right 1:left
    if self.m_MsgTick[side] then
        invoke(self.m_MsgTick[side])
        self.m_MsgTick[side] = nil
    end
end

function LuaJiaoYueDuoSuYiInGameWnd:CancelMsgOffsetTick(side) -- 0:right 1:left
    if self.m_MsgOffsetTick[side] then
        invoke(self.m_MsgOffsetTick[side])
        self.m_MsgOffsetTick[side] = nil
    end
end

function LuaJiaoYueDuoSuYiInGameWnd:CancelCameraCtrlTick()
    if self.m_CameraCtrlTick then
        invoke(self.m_CameraCtrlTick)
        self.m_CameraCtrlTick = nil
    end
end

function LuaJiaoYueDuoSuYiInGameWnd:ClearTweeners()
    if self.m_Tweeners then
        for i = 1, #self.m_Tweeners do
            LuaTweenUtils.Kill(self.m_Tweeners[i], false)
        end
    end
    self.m_Tweeners = {}
end
