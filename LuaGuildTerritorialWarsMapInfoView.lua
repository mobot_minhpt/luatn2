local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local CLoginMgr = import "L10.Game.CLoginMgr"
local CGuildMgr = import "L10.Game.CGuildMgr"
local CMiniMap = import "L10.UI.CMiniMap"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"
local CTeamMgr = import "L10.Game.CTeamMgr"
local CSocialWndMgr = import "L10.UI.CSocialWndMgr"
local CChatHelper = import "L10.UI.CChatHelper"
local EChatPanel = import "L10.Game.EChatPanel"
local CTeamGroupMgr = import "L10.Game.CTeamGroupMgr"

LuaGuildTerritorialWarsMapInfoView = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaGuildTerritorialWarsMapInfoView, "TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsMapInfoView, "Grid", "Grid",UIGrid)
RegistChildComponent(LuaGuildTerritorialWarsMapInfoView, "Template", "Template", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsMapInfoView, "ShareButton", "ShareButton", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsMapInfoView, "RedTitleBg", "RedTitleBg", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsMapInfoView, "WhiteTitleBg", "WhiteTitleBg", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsMapInfoView, "GreenTitleBg", "GreenTitleBg", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsMapInfoView, "BlueTitleBg", "BlueTitleBg", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsMapInfoView, "MapInfoViewBg", "MapInfoViewBg", UIWidget)
--@endregion RegistChildComponent end
RegistClassMember(LuaGuildTerritorialWarsMapInfoView, "m_RedColor")
RegistClassMember(LuaGuildTerritorialWarsMapInfoView, "m_BlueColor")
RegistClassMember(LuaGuildTerritorialWarsMapInfoView, "m_GreenColor")
RegistClassMember(LuaGuildTerritorialWarsMapInfoView, "m_YellowColor")
RegistClassMember(LuaGuildTerritorialWarsMapInfoView, "m_MapInfoViewBgInitialHeight")
RegistClassMember(LuaGuildTerritorialWarsMapInfoView, "m_StateLabel")
RegistClassMember(LuaGuildTerritorialWarsMapInfoView, "m_MonsterLabel")
RegistClassMember(LuaGuildTerritorialWarsMapInfoView, "m_MapIdxLabel")
RegistClassMember(LuaGuildTerritorialWarsMapInfoView, "m_UpdateStateLabelTick")

function LuaGuildTerritorialWarsMapInfoView:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    UIEventListener.Get(self.ShareButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareButtonClick()
	end)
	self.m_MapInfoViewBgInitialHeight = self.MapInfoViewBg.height
end

function LuaGuildTerritorialWarsMapInfoView:OnEnable()
	self:Init()
	g_ScriptEvent:AddListener("OnSendTerritoryWarMapDetail", self, "OnSendTerritoryWarMapDetail")
	g_ScriptEvent:AddListener("OnSendTerritoryWarPlayInfo", self, "UpdateMapIdx")
	self:CancelStateLabelTick()
	self.m_UpdateStateLabelTick = RegisterTick(function()
		self:UpdateState()
	end,500)
end

function LuaGuildTerritorialWarsMapInfoView:OnDisable()
	g_ScriptEvent:RemoveListener("OnSendTerritoryWarMapDetail", self, "OnSendTerritoryWarMapDetail")
	g_ScriptEvent:RemoveListener("OnSendTerritoryWarPlayInfo", self, "UpdateMapIdx")
	self:CancelStateLabelTick()
end

function LuaGuildTerritorialWarsMapInfoView:CancelStateLabelTick()
	if self.m_UpdateStateLabelTick then
		UnRegisterTick(self.m_UpdateStateLabelTick)
		self.m_UpdateStateLabelTick = nil
	end
end

function LuaGuildTerritorialWarsMapInfoView:Init()
    self.Template.gameObject:SetActive(false)
	self.m_RedColor = "[FF0000]"
	self.m_BlueColor = "[66CCFF]"
	self.m_GreenColor = "[03B836]"
	self.m_YellowColor = "[ffff00]"

    local isInMapView = self.transform.name ~= "Background"
    self:InitTitle()
	Extensions.RemoveAllChildren(self.Grid.transform)
    if isInMapView then
        self:AddMapIdx()
    end
	self:AddGridPos()
	self:AddGridGenerateScore()
	self:AddGridGuild()
	if LuaGuildTerritorialWarsMgr:IsPeripheryPlay() then
		self:AddMonster()
	else
		self:AddState()
	end
	self:AddSceneCount()
	self:AddCapture()
	self.Grid:Reposition()
	self:InitMapInfoViewBg()
end

function LuaGuildTerritorialWarsMapInfoView:InitMapInfoViewBg()
	if self.MapInfoViewBg then
		local labelCount = self.Grid.transform.childCount
		self.MapInfoViewBg.height = self.m_MapInfoViewBgInitialHeight + self.Grid.cellHeight * (labelCount - 1)
	end
end

function LuaGuildTerritorialWarsMapInfoView:AddMapIdx()
	local obj = NGUITools.AddChild(self.Grid.gameObject,self.Template.gameObject)
	obj.gameObject:SetActive(true)
	self.m_MapIdxLabel = obj.transform:Find("Label"):GetComponent(typeof(UILabel))
	self:UpdateMapIdx()
end

function LuaGuildTerritorialWarsMapInfoView:UpdateMapIdx()
	if not self.m_MapIdxLabel then return end
	local mapIdx = CMiniMap.Instance ~= nil and CMiniMap.Instance.mapIdx or 0
	if LuaGuildTerritorialWarsMgr.m_SceneIdx ~= 0 then
		mapIdx = LuaGuildTerritorialWarsMgr.m_SceneIdx
	end
	self.m_MapIdxLabel.text = SafeStringFormat3(LocalString.GetString("[acf8ff]当前:  [ffffff]分线%d"), mapIdx)
end

function LuaGuildTerritorialWarsMapInfoView:AddSceneCount()
	local isInMapView = self.transform.name ~= "Background"
	if isInMapView then return end
	local obj = NGUITools.AddChild(self.Grid.gameObject,self.Template.gameObject)
	obj.gameObject:SetActive(true)
	local label = obj.transform:Find("Label"):GetComponent(typeof(UILabel))
	local t = LuaGuildTerritorialWarsMgr.m_GridTipData
	local data = GuildTerritoryWar_Map.GetData(t.id)
	local gridInfo = LuaGuildTerritorialWarsMgr.m_GridInfo[t.id]
	local isCity = gridInfo and gridInfo.isCity
	label.text = SafeStringFormat3(LocalString.GetString("[acf8ff]初始分线数分线:  [ffffff]%d"), isCity and 3 or data.InitSceneCount)
end

function LuaGuildTerritorialWarsMapInfoView:AddGridPos()
	local t = LuaGuildTerritorialWarsMgr.m_GridTipData
	if not t then return end
	local obj = NGUITools.AddChild(self.Grid.gameObject,self.Template.gameObject)
	obj.gameObject:SetActive(true)
	obj.transform:Find("Label"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("[acf8ff]位置:  [ffffff](%d,%d)"), t.x, t.y)
end

function LuaGuildTerritorialWarsMapInfoView:AddGridGenerateScore()
	local t = LuaGuildTerritorialWarsMgr.m_GridTipData
	if not t then return end
	local resourceLevelGenerateScorePerMin = GuildTerritoryWar_Setting.GetData().ResourceLevelGenerateScorePerMin
	local speed = 0
	for i = 0, resourceLevelGenerateScorePerMin.Length - 1 do
		if resourceLevelGenerateScorePerMin[i][0] == t.level then
			speed = resourceLevelGenerateScorePerMin[i][1]
		end
	end
	local gridInfo = LuaGuildTerritorialWarsMgr.m_GridInfo[t.id]
	if gridInfo and gridInfo.isCity then speed = 0 end
	local obj = NGUITools.AddChild(self.Grid.gameObject,self.Template.gameObject)
	obj.gameObject:SetActive(true)
	obj.transform:Find("Label"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("[acf8ff]属性:  [ffffff]%d积分/分钟"), speed)
end

function LuaGuildTerritorialWarsMapInfoView:AddGridGuild()
	local t = LuaGuildTerritorialWarsMgr.m_GridTipData
	if not t then return end
	local id = t.id
	local gridInfo = LuaGuildTerritorialWarsMgr.m_GridInfo[id]
	local guildInfo = LuaGuildTerritorialWarsMgr.m_GuildInfo[gridInfo.belongGuildId]
	local isMyArea = LuaGuildTerritorialWarsMgr:IsMyArea(id)
	local isFriendArea = LuaGuildTerritorialWarsMgr:IsFriendArea(id)
	local isEnemyArea = LuaGuildTerritorialWarsMgr:IsEnemyArea(id)
	local obj = NGUITools.AddChild(self.Grid.gameObject,self.Template.gameObject)
	obj.gameObject:SetActive(true)
	local info = LocalString.GetString("[808080]无")
	local ownGuildName,ownGuildServerName = guildInfo and guildInfo.guildName or "",guildInfo and guildInfo.serverName or ""
	if isMyArea then
		if CClientMainPlayer.Inst and CClientMainPlayer.Inst:IsInGuild() then
			local gameServer = CLoginMgr.Inst:GetSelectedGameServer()
			info = SafeStringFormat3(LocalString.GetString("%s%s-%s(占领)"),self.m_GreenColor,ownGuildName,ownGuildServerName)
		elseif LuaGuildTerritorialWarsMgr:IsInGuide() then
			info = SafeStringFormat3(LocalString.GetString("%s%s-%s(占领)"),self.m_GreenColor,LocalString.GetString("我的帮会") ,  LocalString.GetString("我的服务器"))
		end
	elseif isFriendArea then
		info = SafeStringFormat3(LocalString.GetString("[ffffff]%s-%s%s(同盟占领)"),ownGuildName,ownGuildServerName,self.m_BlueColor)
	elseif isEnemyArea then
		info = SafeStringFormat3(LocalString.GetString("[ffffff]%s-%s%s(非同盟占领)"),ownGuildName,ownGuildServerName,self.m_RedColor)
	end
	obj.transform:Find("Label"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("[acf8ff]归属:  %s"),info)
end

function LuaGuildTerritorialWarsMapInfoView:AddState()
	local obj = NGUITools.AddChild(self.Grid.gameObject,self.Template.gameObject)
	obj.gameObject:SetActive(true)
	self.m_StateLabel = obj.transform:Find("Label"):GetComponent(typeof(UILabel))
	self:UpdateState()
end

function LuaGuildTerritorialWarsMapInfoView:UpdateState()
	if not self.m_StateLabel then return end

	local t = LuaGuildTerritorialWarsMgr.m_GridTipData
	if not t then return end
	local id = t.id
	local gridInfo = LuaGuildTerritorialWarsMgr.m_GridInfo[id]
	local leftTime = gridInfo.stausParam - CServerTimeMgr.Inst.timeStamp
	local leftTimeStr = leftTime >= 0 and SafeStringFormat3("%02d:%02d", math.floor(leftTime % 3600 / 60),leftTime % 60) or ""
	local colorStr, stateStr = "",""
	
	if gridInfo.status == 1 then
		colorStr = self.m_RedColor
		stateStr = LocalString.GetString("交战中")
	elseif gridInfo.status == 2 then
		colorStr = self.m_RedColor
		stateStr = SafeStringFormat3(LocalString.GetString("放弃中(%s后生效)"),leftTimeStr)
	elseif gridInfo.status == 3 then
		colorStr = self.m_YellowColor
		stateStr = SafeStringFormat3(LocalString.GetString("保护期(%s)"),leftTimeStr)
	elseif gridInfo.status == 4 then
		colorStr = self.m_YellowColor
		stateStr = SafeStringFormat3(LocalString.GetString("免战中(%s)"),leftTimeStr)
	elseif gridInfo.status == 5 then
		colorStr = self.m_YellowColor
		stateStr = SafeStringFormat3(LocalString.GetString("申请免战(%s后生效)"),leftTimeStr)
	elseif gridInfo.belongGuildId > 0 then
		colorStr = "[ffffff]"
		stateStr = LocalString.GetString("已占领")
	else
		colorStr = "[ffffff]"
		stateStr = LocalString.GetString("可占领")
	end

	local mapData = GuildTerritoryWar_Map.GetData(id)
	local regionData = GuildTerritoryWar_Region.GetData(mapData.Region)
	if LuaGuildTerritorialWarsMgr.m_CurrentWeek < regionData.Opendate then
		colorStr = self.m_RedColor
		stateStr = LocalString.GetString("下周一0:00开放")
	end
	self.m_StateLabel.text = SafeStringFormat3(LocalString.GetString("[acf8ff]状态:  %s%s"),colorStr,stateStr)
end

function LuaGuildTerritorialWarsMapInfoView:AddMonster()
	local obj = NGUITools.AddChild(self.Grid.gameObject,self.Template.gameObject)
	obj.gameObject:SetActive(true)
	self.m_MonsterLabel = obj.transform:Find("Label"):GetComponent(typeof(UILabel))
	self:UpdateMonster()
end

function LuaGuildTerritorialWarsMapInfoView:UpdateMonster()
	if not self.m_MonsterLabel then return end
	local t = LuaGuildTerritorialWarsMgr.m_GridTipData
	if not t then return end
	local monsterNum = LuaGuildTerritorialWarsMgr:GetMonsterCount(t.id)
	self.m_MonsterLabel.text = SafeStringFormat3(LocalString.GetString("[acf8ff]妖魔:  [ffffff]%s"), monsterNum)
end

function LuaGuildTerritorialWarsMapInfoView:AddCapture()
	local t = LuaGuildTerritorialWarsMgr.m_GridTipData
	if not t then return end
	local id = t.id
	local gridInfo = LuaGuildTerritorialWarsMgr.m_GridInfo[id]
	local guildId = gridInfo.belongGuildId
	local guildInfo = LuaGuildTerritorialWarsMgr.m_GuildInfo[guildId]
	--local ownGuildId = gridInfo.belongGuildId
	local masterGuildId = guildInfo and guildInfo.masterGuildId or 0
	local masterGuild =  LuaGuildTerritorialWarsMgr.m_GuildInfo[masterGuildId]
	if masterGuild then
		local obj = NGUITools.AddChild(self.Grid.gameObject,self.Template.gameObject)
		obj.gameObject:SetActive(true)
		local ownGuildName, ownGuildServerName = masterGuild.guildName, masterGuild.serverName
		local isMyArea = masterGuildId == guildId
		local isFriendArea = guildInfo and guildInfo.allianceGuildId == masterGuildId and (gridInfo.belongGuildId > 0)
		--local isEnemyArea = not isMyArea and not isFriendArea
		local masterGuildColor = isMyArea and self.m_GreenColor or (isFriendArea and self.m_BlueColor or self.m_RedColor)
		obj.transform:Find("Label"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("%s被%s%s-%s%s俘虏"),
			self.m_YellowColor,masterGuildColor,ownGuildName,ownGuildServerName,self.m_YellowColor)
	end
end

function LuaGuildTerritorialWarsMapInfoView:InitTitle()
	local t = LuaGuildTerritorialWarsMgr.m_GridTipData
	if not t then return end
	local data = GuildTerritoryWar_Region.GetData(t.region)
	local id = t.id
	local guildId = LuaGuildTerritorialWarsMgr.m_GuildId
	local gridInfo = LuaGuildTerritorialWarsMgr.m_GridInfo[id]
	local guildInfo = LuaGuildTerritorialWarsMgr.m_GuildInfo[guildId]
	local isMyArea = gridInfo.belongGuildId == guildId
	local isFriendArea = guildInfo and guildInfo.allianceGuildId == gridInfo.belongGuildId and (gridInfo.belongGuildId > 0)
	local isEnemyArea = not isMyArea and not isFriendArea and (gridInfo.belongGuildId > 0)
	local isOccupied = gridInfo.belongGuildId > 0

	self.GreenTitleBg.gameObject:SetActive(isMyArea)
	self.BlueTitleBg.gameObject:SetActive(isFriendArea)
	self.RedTitleBg.gameObject:SetActive(isEnemyArea)
	self.WhiteTitleBg.gameObject:SetActive(not isOccupied)
	self.TitleLabel.text = SafeStringFormat3(LocalString.GetString("%s%d级领土"), data.Name, t.level)
end

--@region UIEvent
function LuaGuildTerritorialWarsMapInfoView:OnShareButtonClick()
	local t = LuaGuildTerritorialWarsMgr.m_GridTipData
	local region = t.region
	local regionData = GuildTerritoryWar_Region.GetData(region)
	local msg = g_MessageMgr:FormatMessage("GuildTerritorialWarsMap_ShareMessage", regionData.Name,t.x,t.y,t.id)
	local items = CreateFromClass(MakeGenericClass(List, PopupMenuItemData))
	CommonDefs.ListAdd(items, typeof(PopupMenuItemData), PopupMenuItemData(LocalString.GetString("分享至世界"), DelegateFactory.Action_int(function (index)
		self:ShareMsg(EChatPanel.World,msg)
	end), false, nil, EnumPopupMenuItemStyle.Default))
	if CClientMainPlayer.Inst and CClientMainPlayer.Inst:IsInGuild() and (LuaGuildTerritorialWarsMgr.m_GuildId == CClientMainPlayer.Inst.BasicProp.GuildId) then
		CommonDefs.ListAdd(items, typeof(PopupMenuItemData), PopupMenuItemData(LocalString.GetString("分享至帮会"), DelegateFactory.Action_int(function (index)
			self:ShareMsg(EChatPanel.Guild,msg)
		end), false, nil, EnumPopupMenuItemStyle.Default))
	end
	if CClientMainPlayer.Inst and CClientMainPlayer.Inst:IsInGuild() and (LuaGuildTerritorialWarsMgr.m_GuildId ~= CClientMainPlayer.Inst.BasicProp.GuildId) then
		CommonDefs.ListAdd(items, typeof(PopupMenuItemData), PopupMenuItemData(LocalString.GetString("分享至外援频道"), DelegateFactory.Action_int(function (index)
			self:ShareMsg(EChatPanel.AID,msg)
		end), false, nil, EnumPopupMenuItemStyle.Default))
	end
	if CTeamMgr.Inst:TeamExists() then
		CommonDefs.ListAdd(items, typeof(PopupMenuItemData), PopupMenuItemData(LocalString.GetString("分享至队伍"), DelegateFactory.Action_int(function (index)
			self:ShareMsg(EChatPanel.Team,msg)
		end), false, nil, EnumPopupMenuItemStyle.Default))
	end
	if CTeamGroupMgr.Instance:InTeamGroup() then
		CommonDefs.ListAdd(items, typeof(PopupMenuItemData), PopupMenuItemData(LocalString.GetString("分享至团队"), DelegateFactory.Action_int(function (index)
			self:ShareMsg(EChatPanel.TeamGroup,msg)
		end), false, nil, EnumPopupMenuItemStyle.Default))
	end
    CPopupMenuInfoMgr.ShowPopupMenu(CommonDefs.ListToArray(items), self.ShareButton.transform, CPopupMenuInfoMgr.AlignType.Right)
end
--@endregion UIEvent

function LuaGuildTerritorialWarsMapInfoView:OnSendTerritoryWarMapDetail()
	self:UpdateState()
end

function LuaGuildTerritorialWarsMapInfoView:ShareMsg(channel,msg)
	CChatHelper.SendMsgWithFilterOption(channel,msg,0, true)
	CSocialWndMgr.ShowChatWnd(channel)
end
