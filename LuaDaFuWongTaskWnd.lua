local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UITable = import "UITable"
local UILabel = import "UILabel"
local UISlider = import "UISlider"
local CUITexture = import "L10.UI.CUITexture"

LuaDaFuWongTaskWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaDaFuWongTaskWnd, "TaskItemTemplate", "TaskItemTemplate", GameObject)
RegistChildComponent(LuaDaFuWongTaskWnd, "TaskTable", "TaskTable", UITable)
RegistChildComponent(LuaDaFuWongTaskWnd, "TipLabel", "TipLabel", UILabel)
RegistChildComponent(LuaDaFuWongTaskWnd, "Progress", "Progress", UISlider)
RegistChildComponent(LuaDaFuWongTaskWnd, "CountDownLabel", "CountDownLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaDaFuWongTaskWnd, "m_ColorList")
RegistClassMember(LuaDaFuWongTaskWnd, "m_headIconName")
function LuaDaFuWongTaskWnd:Awake()
    local CloseBtn = self.transform:Find("WndBg/CloseButton")
    --@region EventBind: Dont Modify Manually!
    UIEventListener.Get(CloseBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCloseBtnClick()
	end)
    --@endregion EventBind end
    self.m_ColorList = {"E39842","C85659","AE5DA7","43C553"}
    self.m_headIconName = {"dafuwongmainplaywnd_wanjiaxinxi_yellow_n_02","dafuwongmainplaywnd_wanjiaxinxi_red_n_02","dafuwongmainplaywnd_wanjiaxinxi_purple_n_02","dafuwongmainplaywnd_wanjiaxinxi_green_n_02"}
end

function LuaDaFuWongTaskWnd:Init()
    if not LuaDaFuWongMgr.TaskInfo or not LuaDaFuWongMgr.TaskInfo.task then return end
    SoundManager.Inst:PlayOneShot("event:/L10/L10_UI/DaFuWeng/UI_DaFuWeng_GongGaoLan", Vector3.zero, nil, 0)
    Extensions.RemoveAllChildren(self.TaskTable.transform)
    self.TaskItemTemplate.gameObject:SetActive(false)
    self:UpdateTaskInfo()
    local CountDown = DaFuWeng_Countdown.GetData(EnumDaFuWengStage.RoundGridBulletinBoard).Time
    local endFunc = function() self:OnCloseBtnClick() end
	local durationFunc = function(time) 
        if self.CountDownLabel then self.CountDownLabel.text = time end
    end
	LuaDaFuWongMgr:StartCountDownTick(EnumDaFuWengCountDownStage.Task,CountDown,durationFunc,endFunc)
end

function LuaDaFuWongTaskWnd:UpdateTaskInfo()
    local TaskList = LuaDaFuWongMgr.TaskInfo.task 
    for k,v in pairs(TaskList) do
        local go = CUICommonDef.AddChild(self.TaskTable.gameObject,self.TaskItemTemplate)
        go.gameObject:SetActive(true)
        self:InitTaskTemplate(go,v)
    end
    self.TaskTable:Reposition()
end

function LuaDaFuWongTaskWnd:InitTaskTemplate(go,data)
    local taskId = data.Id
    local awardId = data.awardId
    local receiveId = data.receive
    local taskInfo = DaFuWeng_GongGaoTask.GetData(taskId)
    local award = DaFuWeng_GongGaoReward.GetData(awardId)
    go.transform:Find("DesLabel"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(taskInfo.Describe,tonumber(taskInfo.Target))
    go.transform:Find("Texture"):GetComponent(typeof(CUITexture)):LoadMaterial(taskInfo.picPath)

    if award.Money and award.Money > 0 then
        go.transform:Find("Score"):GetComponent(typeof(UILabel)).text = award.Money
        go.transform:Find("Score/Sprite"):GetComponent(typeof(UISprite)).spriteName = "dafuwongmainplaywnd_nandutongbao"
    elseif award.Card then
        go.transform:Find("Score/Sprite"):GetComponent(typeof(UISprite)).spriteName = "dafuwongmainplaywnd_mibao"
        local count = 0
        for i = 0,award.Card.Length - 1 do
            count = count + award.Card[i][1]
        end
        go.transform:Find("Score"):GetComponent(typeof(UILabel)).text = count
    end

    local Mask = go.transform:Find("Mask")
    local HaveReceive = go.transform:Find("HaveReceive")
    local ReceiveBtn =  go.transform:Find("ReceiveButton")
    if receiveId and LuaDaFuWongMgr.PlayerInfo[receiveId] then
        local playerInfo = LuaDaFuWongMgr.PlayerInfo[receiveId]
        Mask.gameObject:SetActive(true)
        HaveReceive.gameObject:SetActive(true)
        ReceiveBtn.gameObject:SetActive(false)
        HaveReceive.transform:Find("dafuwongtaskwnd_task_received_bg"):GetComponent(typeof(UITexture)).color = NGUIText.ParseColor24(self.m_ColorList[playerInfo.round], 0)
        HaveReceive.transform:Find("Portrait/BG"):GetComponent(typeof(UISprite)).spriteName = self.m_headIconName[playerInfo.round]
        HaveReceive.transform:Find("Portrait"):GetComponent(typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(playerInfo.class, playerInfo.gender, -1), false)
    else
        Mask.gameObject:SetActive(false)
        HaveReceive.gameObject:SetActive(false)
        ReceiveBtn.gameObject:SetActive(true)
        UIEventListener.Get(ReceiveBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnRequestReceiveTask(taskId)
        end)
    end

end

function LuaDaFuWongTaskWnd:OnCloseBtnClick()
    Gac2Gas.DaFuWengFinishCurStage(EnumDaFuWengStage.RoundGridBulletinBoard)
    self:CloseWnd()
end

function LuaDaFuWongTaskWnd:CloseWnd(curStage)
    if curStage ~= EnumDaFuWengStage.RoundGridBulletinBoard then
        CUIManager.CloseUI(CLuaUIResources.DaFuWongTaskWnd)
    end
end

function LuaDaFuWongTaskWnd:OnRequestReceiveTask(taskid)
    if not LuaDaFuWongMgr.IsMyRound then 
        local id = LuaDaFuWongMgr.CurRoundPlayer
        local name = LuaDaFuWongMgr.PlayerInfo[id].name
        g_MessageMgr:ShowMessage("DaFuWeng_OtherPlayerOperate",name)
        return
    end
    Gac2Gas.DaFuWengBulletinBoardReceiveTask(taskid)
end

function LuaDaFuWongTaskWnd:OnEnable()
    g_ScriptEvent:AddListener("OnDaFuWengStageUpdate",self,"CloseWnd")
    g_ScriptEvent:AddListener("OnDaFuWongGongGaoTaskUpdate",self,"UpdateTaskInfo")
end

function LuaDaFuWongTaskWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnDaFuWengStageUpdate",self,"OnCloseBtnClick")
    g_ScriptEvent:RemoveListener("OnDaFuWongGongGaoTaskUpdate",self,"UpdateTaskInfo")
    LuaDaFuWongMgr:EndCountDownTick(EnumDaFuWengCountDownStage.Task)
end

--@region UIEvent

--@endregion UIEvent

