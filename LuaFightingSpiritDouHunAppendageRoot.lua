local CUIFx = import "L10.UI.CUIFx"
local QnMoneyCostMesssageMgr = import "L10.UI.QnMoneyCostMesssageMgr"
local UITexture = import "UITexture"
local UILabel = import "UILabel"
local UIGrid = import "UIGrid"
local CUICommonDef = import "L10.UI.CUICommonDef"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UIProgressBar = import "UIProgressBar"
local CPackageItemCell = import "L10.UI.CPackageItemCell"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CItemMgr = import "L10.Game.CItemMgr"
local CFightingSpiritMgr = import "L10.Game.CFightingSpiritMgr"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local String = import "System.String"
local Gac2Gas2 = import "L10.Game.Gac2Gas"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"

LuaFightingSpiritDouHunAppendageMgr = {}
LuaFightingSpiritDouHunAppendageMgr.m_JingLiData = nil
LuaFightingSpiritDouHunAppendageMgr.m_ShenLiData = nil
LuaFightingSpiritDouHunAppendageMgr.m_QiLiData = nil

function LuaFightingSpiritDouHunAppendageMgr:ClearData()
	LuaFightingSpiritDouHunAppendageMgr.m_JingLiData = nil
	LuaFightingSpiritDouHunAppendageMgr.m_ShenLiData = nil
	LuaFightingSpiritDouHunAppendageMgr.m_QiLiData = nil
end

LuaFightingSpiritDouHunAppendageRoot = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaFightingSpiritDouHunAppendageRoot, "JingLiBtn", "JingLiBtn", GameObject)
RegistChildComponent(LuaFightingSpiritDouHunAppendageRoot, "QiLiBtn", "QiLiBtn", GameObject)
RegistChildComponent(LuaFightingSpiritDouHunAppendageRoot, "ShenLiBtn", "ShenLiBtn", GameObject)
RegistChildComponent(LuaFightingSpiritDouHunAppendageRoot, "RuleBtn", "RuleBtn", GameObject)
RegistChildComponent(LuaFightingSpiritDouHunAppendageRoot, "JingLiDisableLabel", "JingLiDisableLabel", UILabel)
RegistChildComponent(LuaFightingSpiritDouHunAppendageRoot, "JingLiLabel", "JingLiLabel", UILabel)
RegistChildComponent(LuaFightingSpiritDouHunAppendageRoot, "QiLiDisableLabel", "QiLiDisableLabel", UILabel)
RegistChildComponent(LuaFightingSpiritDouHunAppendageRoot, "QiLiLabel", "QiLiLabel", UILabel)
RegistChildComponent(LuaFightingSpiritDouHunAppendageRoot, "ShenLiDisableLabel", "ShenLiDisableLabel", UILabel)
RegistChildComponent(LuaFightingSpiritDouHunAppendageRoot, "ShenLiLabelGrid", "ShenLiLabelGrid", UIGrid)
RegistChildComponent(LuaFightingSpiritDouHunAppendageRoot, "AppendageState", "AppendageState", GameObject)
RegistChildComponent(LuaFightingSpiritDouHunAppendageRoot, "FuTiTexture", "FuTiTexture", UITexture)
RegistChildComponent(LuaFightingSpiritDouHunAppendageRoot, "StateFx", "StateFx", CUIFx)
RegistChildComponent(LuaFightingSpiritDouHunAppendageRoot, "Jing", "Jing", GameObject)
RegistChildComponent(LuaFightingSpiritDouHunAppendageRoot, "Qi", "Qi", GameObject)
RegistChildComponent(LuaFightingSpiritDouHunAppendageRoot, "Shen", "Shen", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaFightingSpiritDouHunAppendageRoot, "m_JingLiAction")
RegistClassMember(LuaFightingSpiritDouHunAppendageRoot, "m_ShenLiAction")
RegistClassMember(LuaFightingSpiritDouHunAppendageRoot, "m_QiLiAction")
RegistClassMember(LuaFightingSpiritDouHunAppendageRoot, "m_ShenLiDescList")
RegistClassMember(LuaFightingSpiritDouHunAppendageRoot, "m_FxList")
RegistClassMember(LuaFightingSpiritDouHunAppendageRoot, "m_HasInit")
RegistClassMember(LuaFightingSpiritDouHunAppendageRoot, "m_FxState")

function LuaFightingSpiritDouHunAppendageRoot:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.JingLiBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnJingLiBtnClick()
	end)


	
	UIEventListener.Get(self.QiLiBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnQiLiBtnClick()
	end)


	
	UIEventListener.Get(self.ShenLiBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShenLiBtnClick()
	end)


	
	UIEventListener.Get(self.RuleBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRuleBtnClick()
	end)


    --@endregion EventBind end
end

function LuaFightingSpiritDouHunAppendageRoot:Init()
	if self.m_HasInit then
		return
	end
	self.m_HasInit = true
	self.m_ShenLiDescList = {}
	for i=1,5 do
		table.insert(self.m_ShenLiDescList, self.ShenLiLabelGrid.transform:Find(tostring(i)):GetComponent(typeof(UILabel)))
	end

	-- 特效
	self.m_FxState = {}
	for i=1,3 do
		self.m_FxState[i] = false
	end

	self.StateFx.OnLoadFxFinish = DelegateFactory.Action(function()
		self.m_FxList = {}
		local root = self.StateFx.transform:Find("__FxRoot__"):GetChild(0).transform

		table.insert(self.m_FxList, root:Find("zongti/jing").gameObject)
		table.insert(self.m_FxList, root:Find("zongti/qi").gameObject)
		table.insert(self.m_FxList, root:Find("zongti/shen").gameObject)
		table.insert(self.m_FxList, root:Find("zongti/luoxuan").gameObject)
		table.insert(self.m_FxList, root:Find("zongti/luoxuan (1)").gameObject)
		table.insert(self.m_FxList, root:Find("zongti/luoxuan (2)").gameObject)
		table.insert(self.m_FxList, root:Find("zongti/smoke (4)").gameObject)
		table.insert(self.m_FxList, root:Find("zongti/center").gameObject)

		self:UpdateFx()
	end)
	self.StateFx:LoadFx("fx/ui/prefab/UI_DouHunFunTi.prefab")
end

function LuaFightingSpiritDouHunAppendageRoot:UpdateFx()
	local allTrue = true
	for i=1,3 do
		if not self.m_FxState[i] then
			allTrue = false
		end
	end

	if self.m_FxList then
		for i=1, #self.m_FxList do
			if i<= 3 then
				if CommonDefs.IS_VN_CLIENT and not LocalString.isCN then
					self.m_FxList[i]:SetActive(false)
				else
					self.m_FxList[i]:SetActive(self.m_FxState[i])
				end
			else
				self.m_FxList[i]:SetActive(allTrue)
			end
		end
	end
end

-- hasSet:表示开启状态
-- enable:队长是否激活 不激活的话点击的时候提示
-- canEnable：表示可以付费开启，也就是队长
-- canSet：是否可以设置
function LuaFightingSpiritDouHunAppendageRoot:SetJingLi(level, hasSet, enabled, canEnable, canSet)
	LuaFightingSpiritDouHunAppendageMgr.m_JingLiData = {}
	LuaFightingSpiritDouHunAppendageMgr.m_JingLiData.level = level
	LuaFightingSpiritDouHunAppendageMgr.m_JingLiData.hasSet = hasSet
	LuaFightingSpiritDouHunAppendageMgr.m_JingLiData.enabled = enabled
	LuaFightingSpiritDouHunAppendageMgr.m_JingLiData.canEnable = canEnable
	LuaFightingSpiritDouHunAppendageMgr.m_JingLiData.canSet = canSet

	CUICommonDef.SetActive(self.Jing, false, true)
	if hasSet then
		-- 变彩色
		CUICommonDef.SetActive(self.FuTiTexture.gameObject, true, true)
		CUICommonDef.SetActive(self.Jing, true, true)
		self.FuTiTexture.color = Color(1,1,1,1)
		self.m_FxState[1] = true
		self:UpdateFx()

		--已附体
		if level >= 1 then
			level = level - 1
		end
		-- 技能等级
		self.JingLiLabel.text = LocalString.GetString("角色全部技能等级[FFFE91] +") .. tostring(DouHunTan_Setting.GetData().JingLiFuTiEffect[level][2])

		self.JingLiDisableLabel.gameObject:SetActive(false)
		self.JingLiLabel.gameObject:SetActive(true)
		self:SetBtnState(self.JingLiBtn, LocalString.GetString("已附体"), "common_btn_01_blue", false)
	else
		self.JingLiDisableLabel.gameObject:SetActive(true)
		self.JingLiLabel.gameObject:SetActive(false)
		if canSet then
			-- 点击有用
			self:SetBtnState(self.JingLiBtn, LocalString.GetString("附体"), "common_btn_01_yellow", true)
		elseif canEnable then
			-- 队长可以设置
			self:SetBtnState(self.JingLiBtn, LocalString.GetString("开启"), "common_btn_01_yellow", true)
		else
			-- 普通玩家点击没用
			self:SetBtnState(self.JingLiBtn, LocalString.GetString("附体"), "common_btn_01_blue", true)
		end
	end
end

function LuaFightingSpiritDouHunAppendageRoot:GetJingLiCost(level)
	local formulaId = DouHunTan_Setting.GetData().NewEnableFuTiSilverFormulaId[0][1]
	return GetFormula(formulaId)(nil, nil, {level})
end

function LuaFightingSpiritDouHunAppendageRoot:GetQiLiCost(level)
	local formulaId = DouHunTan_Setting.GetData().NewEnableFuTiSilverFormulaId[1][1]
	return GetFormula(formulaId)(nil, nil, {level})
end

function LuaFightingSpiritDouHunAppendageRoot:GetShenLiCost(level)
	local formulaId = DouHunTan_Setting.GetData().NewEnableFuTiSilverFormulaId[2][1]
	return GetFormula(formulaId)(nil, nil, {level})
end

function LuaFightingSpiritDouHunAppendageRoot:SetBtnState(btn, text, bg, enable)
	local label = btn.transform:Find("Label"):GetComponent(typeof(UILabel))
	local sprite = btn.transform:GetComponent(typeof(UISprite))
	label.text = text
	sprite.spriteName = bg
	if bg == "common_btn_01_blue" then
		label.color = Color(14/255,50/255,84/255,1)
	else
		label.color = Color(53/255,27/255,1/255,1)
	end
	CUICommonDef.SetActive(btn, enable, true)
end

-- enable:队长是否激活 不激活的话点击的时候提示
function LuaFightingSpiritDouHunAppendageRoot:SetQiLi(level, pointType, enabled, canEnable, canSet)
	LuaFightingSpiritDouHunAppendageMgr.m_QiLiData = {}
	LuaFightingSpiritDouHunAppendageMgr.m_QiLiData.level = level
	LuaFightingSpiritDouHunAppendageMgr.m_QiLiData.pointType = pointType
	LuaFightingSpiritDouHunAppendageMgr.m_QiLiData.enabled = enabled
	LuaFightingSpiritDouHunAppendageMgr.m_QiLiData.canEnable = canEnable
	LuaFightingSpiritDouHunAppendageMgr.m_QiLiData.canSet = canSet

	local index = CFightingSpiritMgr.Instance:EnumDouHunTrainPointTypeToIndex(pointType)

	CUICommonDef.SetActive(self.Qi, false, true)
	if index == -1 then
		self.QiLiDisableLabel.gameObject:SetActive(true)
		self.QiLiLabel.gameObject:SetActive(false)

		if canSet then
			-- 点击有用
			self:SetBtnState(self.QiLiBtn, LocalString.GetString("附体"), "common_btn_01_yellow", true)
		elseif canEnable then
			-- 队长
			self:SetBtnState(self.QiLiBtn, LocalString.GetString("开启"), "common_btn_01_yellow", true)
		else
			-- 点击没用
			self:SetBtnState(self.QiLiBtn, LocalString.GetString("附体"), "common_btn_01_blue", true)
		end
	else
		-- 激活了
		self.QiLiDisableLabel.gameObject:SetActive(false)
		self.QiLiLabel.gameObject:SetActive(true)
		CUICommonDef.SetActive(self.Qi, true, true)

		-- 变彩色
		CUICommonDef.SetActive(self.FuTiTexture.gameObject, true, true)
		self.FuTiTexture.color = Color(1,1,1,1)
		-- 更新特效
		self.m_FxState[2] = true
		self:UpdateFx()

		local data = DouHunTan_QiLiFuTi.GetData(level)
		local dataList = {data.GenGuEffect, data.LiLiangEffect, data.ZhiLiEffect, data.MinJieEffect, data.JingLiEffect, data.JianShangEffect}
		local dataDesc = {LocalString.GetString("灵兽根骨"), LocalString.GetString("灵兽力量"), LocalString.GetString("灵兽智力"), 
			LocalString.GetString("灵兽敏捷"), LocalString.GetString("灵兽精力"), LocalString.GetString("灵兽范围伤害减免")}
		
		self.QiLiLabel.text = dataDesc[index+1] .. "[FFFE91]+" .. dataList[index+1]

		self:SetBtnState(self.QiLiBtn, LocalString.GetString("调整"), "common_btn_01_blue", true)
	end
end

function LuaFightingSpiritDouHunAppendageRoot:SetShenLi(level, current, enabled, canEnable, canSet, futiLv)

	LuaFightingSpiritDouHunAppendageMgr.m_ShenLiData = {}
	LuaFightingSpiritDouHunAppendageMgr.m_ShenLiData.level = level
	LuaFightingSpiritDouHunAppendageMgr.m_ShenLiData.current = current
	LuaFightingSpiritDouHunAppendageMgr.m_ShenLiData.enabled = enabled
	LuaFightingSpiritDouHunAppendageMgr.m_ShenLiData.canEnable = canEnable
	LuaFightingSpiritDouHunAppendageMgr.m_ShenLiData.canSet = canSet
	LuaFightingSpiritDouHunAppendageMgr.m_ShenLiData.futiLv = futiLv

	CUICommonDef.SetActive(self.Shen, false, true)
	-- 显示信息
	-- 变彩色
	CUICommonDef.SetActive(self.FuTiTexture.gameObject, true, true)
	self.FuTiTexture.color = Color(1,1,1,1)

	
	local dataDesc = {LocalString.GetString("角色根骨"), LocalString.GetString("角色力量"), LocalString.GetString("角色智力"), 
		LocalString.GetString("角色敏捷"), LocalString.GetString("角色精力")}
	
	-- 判断哪些项目需要显示
	local enableData = {}
	for i = 0, current.Count-1 do
		if current[i] >0 then
			table.insert(enableData, i+1)
		end
	end

	if #enableData> 0 then
		CUICommonDef.SetActive(self.Shen, true, true)
		self.m_FxState[3] = true
		self:UpdateFx()
		self.ShenLiLabelGrid.gameObject:SetActive(true)
		self.ShenLiDisableLabel.gameObject:SetActive(false)
	else
		self.ShenLiLabelGrid.gameObject:SetActive(false)
		self.ShenLiDisableLabel.gameObject:SetActive(true)
	end

	for i = 1, 5 do
		local desc = self.ShenLiLabelGrid.transform:Find(tostring(i)):GetComponent(typeof(UILabel))
		if i <= #enableData then
			desc.gameObject:SetActive(true)
			local index = enableData[i]
			desc.text = dataDesc[index] .. "[FFFE91]+" .. tostring(current[index-1])
		else
			desc.gameObject:SetActive(false)
		end
		
	end

	-- 按钮信息
	if canSet then
		-- 点击有用
		self:SetBtnState(self.ShenLiBtn, LocalString.GetString("调整"), "common_btn_01_yellow", true)
	elseif canEnable then
		-- 队长
		self:SetBtnState(self.ShenLiBtn, LocalString.GetString("开启"), "common_btn_01_yellow", true)
	else
		-- 点击没用
		self:SetBtnState(self.ShenLiBtn, LocalString.GetString("附体"), "common_btn_01_blue", true)
	end
end

function LuaFightingSpiritDouHunAppendageRoot:OnEnable()
	self:Init()
	-- 变灰
	CUICommonDef.SetActive(self.FuTiTexture.gameObject, false, true)
	self.FuTiTexture.color = Color(1,1,1,0.7)
    Gac2Gas2.QueryDouHunTrainEffectByType(1)
    Gac2Gas2.QueryDouHunTrainEffectByType(2)
	Gac2Gas2.QueryDouHunTrainEffectByType(3)
    if self.m_JingLiAction == nil then
        self.m_JingLiAction = DelegateFactory.Action_uint_bool_bool_bool_bool(function(level, hasSet, enabled, canEnable, canSet)
			self:SetJingLi(level, hasSet, enabled, canEnable, canSet)
        end)
    end
    if self.m_QiLiAction == nil then
        self.m_QiLiAction = DelegateFactory.Action_uint_string_bool_bool_bool(function(level, pointType, enabled, canEnable, canSet)
			self:SetQiLi(level, pointType, enabled, canEnable, canSet)
        end)
    end
	if self.m_ShenLiAction == nil then
        self.m_ShenLiAction = DelegateFactory.Action_uint_List_uint_bool_bool_bool_uint(function(level, current, enabled, canEnable, canSet, futiLv)
			self:SetShenLi(level, current, enabled, canEnable, canSet, futiLv)
        end)
    end
    EventManager.AddListenerInternal(EnumEventType.FightingSpiritJingliFutiReceived, self.m_JingLiAction)
    EventManager.AddListenerInternal(EnumEventType.FightingSpiritQiliFutiReceived, self.m_QiLiAction)
	EventManager.AddListenerInternal(EnumEventType.FightingSpiritShenliFutiReceived, self.m_ShenLiAction)
end

function LuaFightingSpiritDouHunAppendageRoot:OnDisable()
    EventManager.RemoveListenerInternal(EnumEventType.FightingSpiritJingliFutiReceived, self.m_JingLiAction)
	EventManager.RemoveListenerInternal(EnumEventType.FightingSpiritQiliFutiReceived, self.m_QiLiAction)
    EventManager.RemoveListenerInternal(EnumEventType.FightingSpiritShenliFutiReceived, self.m_ShenLiAction)
end

function LuaFightingSpiritDouHunAppendageRoot:RequestEnable(cost, type)
    local freeSilver = CClientMainPlayer.Inst.FreeSilver
	local silver = CClientMainPlayer.Inst.Silver

	local freeSilverNotEnough = freeSilver < cost
	local moneyEnough = freeSilver + silver >= cost

	local msgList = {"Enable_DouHun_JingLi_Futi_Comform", "Enable_DouHun_QiLi_Futi_Comform", "Enable_DouHun_ShenLi_Futi_Comform"}
	-- 队长
	local msg = g_MessageMgr:FormatMessage(msgList[type])

	QnMoneyCostMesssageMgr.ShowMoneyCostMessageBox(EnumMoneyType.YinPiao, msg, cost, DelegateFactory.Action(function ()
		if not moneyEnough then
			g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("NotEnough_Silver_QuickBuy"), function()
				CShopMallMgr.ShowLinyuShoppingMall(21000668)
			end, nil, nil, nil, false)
		elseif freeSilverNotEnough then
			local text = SafeStringFormat3(LocalString.GetString("银票不足，将消耗银两#287%s，是否确认消耗？"), tostring(cost - freeSilver))
			g_MessageMgr:ShowOkCancelMessage(text, function()
				Gac2Gas2.RequestEnableDouHunFuTi(type)
			end, nil, nil, nil, false)
		else
			Gac2Gas2.RequestEnableDouHunFuTi(type)
		end
	end), nil, false, true, EnumPlayScoreKey.NONE, false)
end

--@region UIEvent

-- 精力为1
function LuaFightingSpiritDouHunAppendageRoot:OnJingLiBtnClick()
	local data = LuaFightingSpiritDouHunAppendageMgr.m_JingLiData
	if data then
		if data.canSet then
			-- 队员
			Gac2Gas2.SetDouHunTrainJingLiFuTiEffect()
		elseif data.canEnable then
			local cost = self:GetJingLiCost(data.level)
			self:RequestEnable(cost, 1)
		else
			g_MessageMgr:ShowMessage("DOU_HUN_FU_TI_NOT_ENABLED")
		end
	end
end

-- 气力为2
function LuaFightingSpiritDouHunAppendageRoot:OnQiLiBtnClick()
	local data = LuaFightingSpiritDouHunAppendageMgr.m_QiLiData
	if data then
		if data.canSet then
			-- 队员
			CUIManager.ShowUI(CLuaUIResources.FightingSpiritDouHunAppendageQiLiWnd)
		elseif data.canEnable then
			local cost = self:GetQiLiCost(data.level)
			self:RequestEnable(cost, 2)
		else
			-- 队长还未激活
			g_MessageMgr:ShowMessage("DOU_HUN_FU_TI_NOT_ENABLED")
		end
	end
end

-- 神力为3
function LuaFightingSpiritDouHunAppendageRoot:OnShenLiBtnClick()
	local data = LuaFightingSpiritDouHunAppendageMgr.m_ShenLiData
	if data then
		if data.canSet then
			-- 队员
			CUIManager.ShowUI(CLuaUIResources.FightingSpiritDouHunAppendageShenLiWnd)
		elseif data.canEnable then
			local lv = data.futiLv
			if lv <=0 then
				lv = 1
			end
			local cost = self:GetShenLiCost(data.futiLv)
			self:RequestEnable(cost, 3)
		else
			g_MessageMgr:ShowMessage("DOU_HUN_FU_TI_NOT_ENABLED")
		end
	end
end

function LuaFightingSpiritDouHunAppendageRoot:OnRuleBtnClick()
	g_MessageMgr:ShowMessage("DouHun_Futi_Rule_Desc")
end

--@endregion UIEvent

