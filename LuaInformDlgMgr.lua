local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local LocalString = import "LocalString"


EnumInformDlgInfoType = {
    GuildInvitation = 1,--邀请入帮
    GroupIMInvitation = 2, --邀请加入群聊
    GuildLeagueConvene = 3, --召集进入帮会联赛主副战场
}

LuaInformDlgMgr = {}

LuaInformDlgMgr.m_ExpiredTimeForGuildLeagueConvene = 10 * 60
LuaInformDlgMgr.m_InfoList = {}
LuaInformDlgMgr.m_GuildLeagueMainConveneInfo = nil
LuaInformDlgMgr.m_GuildLeagueViceConveneInfo = nil

function LuaInformDlgMgr:GetInformData(type, message, onRefuse, onAccept, refuseText, acceptText)
    local data = {}
    data.type = type
    data.message = message
    data.onRefuse = onRefuse
    data.onAccept = onAccept
    data.refuseText = refuseText
    data.acceptText = acceptText
    data.timeStamp = CServerTimeMgr.Inst.timeStamp
    return data
end

function LuaInformDlgMgr:AppendInfo(data)
    table.insert(self.m_InfoList, data)
end

function LuaInformDlgMgr:AddGuildInvitationInform(playerId, playerName, guildName)
    local message = cs_string.Format(LocalString.GetString("{0}邀请你加入{1}帮会，是否确认加入？"), playerName, guildName)
    if LuaCommonSideDialogMgr.m_IsOpen then
        LuaCommonSideDialogMgr:ClearSameKeyDataWithSelectAction("GuildInvitationInform", function(data)
            return data.extraData and data.extraData.playerId ~= playerId
        end)
        LuaCommonSideDialogMgr:ShowDialog(message, "GuildInvitationInform",
            LuaCommonSideDialogMgr:GetBtnInfo(LocalString.GetString("拒绝"), function()
				Gac2Gas.RefuseInvitedAsGuildMember(playerId)
			end, false, false, 0, false, true), 
            LuaCommonSideDialogMgr:GetBtnInfo(LocalString.GetString("同意"), function()
                Gac2Gas.ConfirmInvitedAsGuildMember(playerId)
            end, true, true),nil, nil, {playerId = playerId})
        return
    end
    local data = self:GetInformData(EnumInformDlgInfoType.GuildInvitation, message, function()
            
    end, function()
        Gac2Gas.ConfirmInvitedAsGuildMember(playerId)
    end, nil, nil)
    self:AppendInfo(data)
    CUIManager.ShowUI("InformDlg") --打开或刷新
end

function LuaInformDlgMgr:AddGroupIMInvitationInform(inviterId, serialNum, groupName, inviterName)
    local message = cs_string.Format(LocalString.GetString("{0}邀请你进入群聊{1}，是否接受邀请？"), inviterName, groupName)
    if LuaCommonSideDialogMgr.m_IsOpen then
        LuaCommonSideDialogMgr:ClearSameKeyDataWithSelectAction("GroupIMInvitationInform",function(data)
            return data.extraData and ((data.extraData.inviterId ~= inviterId) and (data.extraData.groupName ~= groupName))
        end)
        LuaCommonSideDialogMgr:ShowDialog(message, "GroupIMInvitationInform",
            LuaCommonSideDialogMgr:GetBtnInfo(LocalString.GetString("拒绝"), function()
                Gac2Gas.RefuseInvitePlayerJoinGroupIM(inviterId)
            end, false, false, 0, false, true), 
            LuaCommonSideDialogMgr:GetBtnInfo(LocalString.GetString("同意"), function()
                Gac2Gas.AcceptInvitePlayerJoinGroupIM(inviterId)
            end, true, false),nil, nil,{inviterId = inviterId, groupName = groupName})
        return
    end
    local data = self:GetInformData(EnumInformDlgInfoType.GroupIMInvitation, message, function()
            Gac2Gas.RefuseInvitePlayerJoinGroupIM(inviterId)
        end, function()
            Gac2Gas.AcceptInvitePlayerJoinGroupIM(inviterId)
        end, nil, nil)
    self:AppendInfo(data)
    CUIManager.ShowUI("InformDlg") --打开或刷新
end

function LuaInformDlgMgr:AddGuildLeagueConveneInform(callUpType, callerId, callerName)
    local message = nil
    if callUpType == EnumGuildLeagueCallUpType_lua.eMain then
        message = cs_string.Format(LocalString.GetString("[c][FFFF00]{0}[-][/c]召集你进入[c][79C8EE]帮会联赛主战场[-][/c]"), callerName)
    elseif callUpType == EnumGuildLeagueCallUpType_lua.eVice then
        message = cs_string.Format(LocalString.GetString("[c][FFFF00]{0}[-][/c]召集你进入[c][79C8EE]帮会联赛副战场[-][/c]"), callerName)
    else
        message = cs_string.Format(LocalString.GetString("[c][FFFF00]{0}[-][/c]召集你进入帮会联赛战场"), callerName)
    end
    if LuaCommonSideDialogMgr.m_IsOpen then
        local key = "GuildLeagueConveneInform"
        if callUpType == EnumGuildLeagueCallUpType_lua.eMain then
            key = "GuildMainLeagueConveneInform"
        elseif callUpType == EnumGuildLeagueCallUpType_lua.eVice then
            key = "GuildViceLeagueConveneInform"
        end
        LuaCommonSideDialogMgr:ClearSameKeyDataWithSelectAction(key, function(data)
            return data.extraData and data.extraData.callerId ~= callerId
        end)
        LuaCommonSideDialogMgr:ShowDialog(message, key,
            LuaCommonSideDialogMgr:GetBtnInfo(LocalString.GetString("拒绝"), function() end, false, false, 60, true, true), 
            LuaCommonSideDialogMgr:GetBtnInfo(LocalString.GetString("同意"), function()
                Gac2Gas.PlayerBeCalledUpInGuildLeagueDone(callUpType, callerId)
            end, true, true),nil,nil,{callerId = callerId})
        return
    end
    local data = self:GetInformData(EnumInformDlgInfoType.GuildLeagueConvene, message, function()
        
    end, function()
        Gac2Gas.PlayerBeCalledUpInGuildLeagueDone(callUpType, callerId)
    end, nil, nil)

    if callUpType == EnumGuildLeagueCallUpType_lua.eMain then
        self.m_GuildLeagueMainConveneInfo = data
    elseif callUpType == EnumGuildLeagueCallUpType_lua.eVice then
        self.m_GuildLeagueViceConveneInfo = data
    else
        self:AppendInfo(data)
    end
    CUIManager.ShowUI("InformDlg") --打开或刷新
end
--[[
    InformDlg之前的实现存在一个bug，就是每次删除最后一个，如果消息显示过程中来了新的消息又没有刷新，
    导致可能删错（和放兄确认过这确实是bug，应该改为实现成来消息就刷新并且优先显示最新的）本次对该组件
    进行迭代重构顺便纠正一下，同时规则做了如下调整

    1. 优先显示主战场帮会征召，其次显示副战场帮会征召，其他（主要是邀请入帮和邀请群聊）则按后来的优先显示
    2. 主副战场帮会征召分别只保留最近的一个消息，且征召消息保留10分钟
]]
function LuaInformDlgMgr:GetLatestInformInfo()
    if self.m_GuildLeagueMainConveneInfo then
        if CServerTimeMgr.Inst.timeStamp - self.m_GuildLeagueMainConveneInfo.timeStamp < self.m_ExpiredTimeForGuildLeagueConvene then
            return self.m_GuildLeagueMainConveneInfo
        else
            self.m_GuildLeagueMainConveneInfo = nil
        end
    end
    if self.m_GuildLeagueViceConveneInfo then
        if CServerTimeMgr.Inst.timeStamp - self.m_GuildLeagueViceConveneInfo.timeStamp < self.m_ExpiredTimeForGuildLeagueConvene then
            return self.m_GuildLeagueViceConveneInfo
        else
            self.m_GuildLeagueViceConveneInfo = nil
        end
    end
    if #self.m_InfoList>0 then
        return self.m_InfoList[#self.m_InfoList]
    else
        return nil
    end
end

function LuaInformDlgMgr:CheckIfExpired()
    if self.m_GuildLeagueMainConveneInfo then
        if CServerTimeMgr.Inst.timeStamp - self.m_GuildLeagueMainConveneInfo.timeStamp > self.m_ExpiredTimeForGuildLeagueConvene then
            self.m_GuildLeagueMainConveneInfo = nil
            return true
        end
    end
    if self.m_GuildLeagueViceConveneInfo then
        if CServerTimeMgr.Inst.timeStamp - self.m_GuildLeagueViceConveneInfo.timeStamp > self.m_ExpiredTimeForGuildLeagueConvene then
            self.m_GuildLeagueViceConveneInfo = nil
            return true
        end
    end
    return false
end

function LuaInformDlgMgr:ClearInfo(info, clearAllSameType)
    if info == nil then return end
    local found = false
    if info == self.m_GuildLeagueMainConveneInfo then
        found = true
        self.m_GuildLeagueMainConveneInfo = nil
        if clearAllSameType then
            self.m_GuildLeagueViceConveneInfo = nil
        end
    elseif info == self.m_GuildLeagueViceConveneInfo then
        found = true
        self.m_GuildLeagueViceConveneInfo = nil
        if clearAllSameType then
            self.m_GuildLeagueMainConveneInfo = nil
        end
    end
        
    if not clearAllSameType then
        for k,v in ipairs(self.m_InfoList) do
            if info == v then
                found = true
                table.remove(self.m_InfoList, k)
                break
            end
        end
    else
        local tbl = {}
        for k,v in ipairs(self.m_InfoList) do
            if info.type ~= v.type then
                table.insert(tbl, v)
            elseif info == v then
                found = true
            end
        end
        self.m_InfoList = tbl
    end
    if not found then
        print("something is wrong when clear inform info", info.type, clearAllSameType)
    end
end

function LuaInformDlgMgr:OnRefuse(info)

    if info.onRefuse then
        info.onRefuse()
    end

    self:ClearInfo(info, false)
end

function LuaInformDlgMgr:OnAccept(info)

    if info.onAccept then
        info.onAccept()
    end

    if info.type == EnumInformDlgInfoType.GuildInvitation then
        self:ClearInfo(info, true)
    elseif info.type == EnumInformDlgInfoType.GroupIMInvitation then
        self:ClearInfo(info, false)
    elseif info.type == EnumInformDlgInfoType.GuildLeagueConvene then
        self:ClearInfo(info, true)
    else
        self:ClearInfo(info, true) --这里目前不会执行到
    end
end

function LuaInformDlgMgr:OnClose(info)
    self:ClearInfo(info, false)
end




