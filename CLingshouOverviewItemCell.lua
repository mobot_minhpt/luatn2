-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CLingshouOverviewItemCell = import "L10.UI.CLingshouOverviewItemCell"
local Color = import "UnityEngine.Color"
local CUICommonDef = import "L10.UI.CUICommonDef"
local EnumQualityType = import "L10.Game.EnumQualityType"
local LingShou_LingShou = import "L10.Game.LingShou_LingShou"
CLingshouOverviewItemCell.m_Init_CS2LuaHook = function (this, key, own) 
    this.lingshouId = key
    this.iconTexture.material = nil
    this.selectSprite.enabled = false
    this.maskSprite.enabled = false
    this.levelLabel.text = ""
    this.qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(EnumQualityType.White)

    local lingshou = LingShou_LingShou.GetData(key)
    if lingshou == nil then
        return
    end
    this:InitQuality(lingshou.ShenShou)
    this.iconTexture:LoadNPCPortrait(lingshou.Portrait, false)
    this.levelLabel.text = tostring(lingshou.Grade)
    this.maskSprite.enabled = not own
    if CClientMainPlayer.Inst.MaxLevel < lingshou.Grade then
        this.levelLabel.color = Color.red
    else
        this.levelLabel.color = Color.white
    end
end
CLingshouOverviewItemCell.m_InitQuality_CS2LuaHook = function (this, quality) 
    repeat
        local default = quality
        if default == (1) then
            this.qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(EnumQualityType.DarkPurple)
            break
        elseif default == (2) then
            this.qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(EnumQualityType.Purple)
            break
        else
            break
        end
    until 1
end
