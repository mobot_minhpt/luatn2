require("common/common_include")
local Gac2Gas=import "L10.Game.Gac2Gas"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local MapiMysticalShop_SaledItem = import "L10.Game.MapiMysticalShop_SaledItem"
local MapiMysticalShop_Setting = import "L10.Game.MapiMysticalShop_Setting"
local DelegateFactory = import "DelegateFactory"
local CShopMallGoodsItem = import "L10.UI.CShopMallGoodsItem"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local Money = import "L10.Game.Money"
local ShopMallTemlate = import "L10.UI.ShopMallTemlate"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local CShopMallItemPreviewer = import "L10.UI.CShopMallItemPreviewer"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local QnMoneyCostMesssageMgr = import "L10.UI.QnMoneyCostMesssageMgr"
local LocalString = import "LocalString"
local PlayerSettings = import "L10.Game.PlayerSettings"
local MessageWndManager = import "L10.UI.MessageWndManager"
local MessageMgr = import "L10.Game.MessageMgr"

CLuaMapiMysticlShopWnd=class()
RegistClassMember(CLuaMapiMysticlShopWnd,"m_TableView")
RegistClassMember(CLuaMapiMysticlShopWnd,"m_ItemPreviewer")
RegistClassMember(CLuaMapiMysticlShopWnd,"m_BuyCountButton")
RegistClassMember(CLuaMapiMysticlShopWnd, "m_MoneyCtrl")
RegistClassMember(CLuaMapiMysticlShopWnd,"m_BuyButton")
RegistClassMember(CLuaMapiMysticlShopWnd,"m_RefreshButton")
RegistClassMember(CLuaMapiMysticlShopWnd,"m_LeftTimeLabel")
RegistClassMember(CLuaMapiMysticlShopWnd,"m_AddTimeButton")

RegistClassMember(CLuaMapiMysticlShopWnd, "m_CurrentSelectedItem")
RegistClassMember(CLuaMapiMysticlShopWnd, "m_Ids")
RegistClassMember(CLuaMapiMysticlShopWnd, "m_Items")
RegistClassMember(CLuaMapiMysticlShopWnd, "m_ItemMoneyTypes")
RegistClassMember(CLuaMapiMysticlShopWnd, "m_ItemScoreTypes")
RegistClassMember(CLuaMapiMysticlShopWnd, "m_TimeStamp")
RegistClassMember(CLuaMapiMysticlShopWnd, "m_CountDownTick")
RegistClassMember(CLuaMapiMysticlShopWnd, "m_HasRaleItem")
RegistClassMember(CLuaMapiMysticlShopWnd, "m_HasOpenedDelayMessage")

function CLuaMapiMysticlShopWnd:Init()
	self.m_BuyCountButton = self.m_BuyCountButton:GetComponent(typeof(QnAddSubAndInputButton))
	self.m_MoneyCtrl = self.m_MoneyCtrl:GetComponent(typeof(CCurentMoneyCtrl))
	self.m_ItemPreviewer = self.m_ItemPreviewer:GetComponent(typeof(CShopMallItemPreviewer))
	self.m_CurrentSelectedItem = 0
	self.m_Items = {}
	self.m_Ids = {}
	self.m_ItemMoneyTypes = {}
	self.m_ItemScoreTypes = {}
	self.m_HasRaleItem = false

	local itemsData = LuaGetGlobal("MapiShopWnd_ItemData")
	local pricesData = LuaGetGlobal("MapiShopWnd_PriceData")
	self.m_TimeStamp = LuaGetGlobal("MapiShopWnd_TimeStamp")

	CommonDefs.DictIterate(itemsData, DelegateFactory.Action_object_object(
		function(key, value)
			local data = MapiMysticalShop_SaledItem.GetData(key)
			--记录是否有稀有物品
			if data.Rare > 0 then
				self.m_HasRaleItem = true
			end
			local itemTemplate = ShopMallTemlate()
			itemTemplate.ItemId = data.ItemId
			--价格折扣设置
			if pricesData[data.ID] < data.CostNum then
				itemTemplate.Discount = 10 * pricesData[data.ID]/data.CostNum
			else
				itemTemplate.Discount = 0;
			end
			itemTemplate.Price = pricesData[data.ID]
			itemTemplate.AvalibleCount = value
			itemTemplate.Status = 0
			table.insert(self.m_Items, itemTemplate)
			table.insert(self.m_Ids, data.ID)
			table.insert(self.m_ItemMoneyTypes, data.CostType)
			table.insert(self.m_ItemScoreTypes, data.ScoreType)
		end 
	))

	local OnItemAt = function (template, row)
		local item = template.gameObject:GetComponent(typeof(CShopMallGoodsItem))
		item:UpdateData(self.m_Items[row + 1], self.m_ItemMoneyTypes[row + 1], true, false)

		if item.m_CurrencySprite and self.m_ItemMoneyTypes[row + 1] == EnumToInt(EnumMoneyType.Score) then
			item.m_CurrencySprite.spriteName = Money.GetIconName(EnumMoneyType.Score, CommonDefs.ConvertIntToEnum(typeof(EnumPlayScoreKey), self.m_ItemScoreTypes[row + 1]))
		end
	end

	self.m_TableView.m_DataSource = DefaultTableViewDataSource.CreateByCount(#self.m_Items, OnItemAt)

	self.m_AddTimeButton.OnClick = DelegateFactory.Action_QnButton(function(button) self:OnAddTimeButtonClick(button) end)
	self.m_RefreshButton.OnClick = DelegateFactory.Action_QnButton(function(button) self:OnRefreshButtonClick(button) end)
	self.m_BuyButton.OnClick = DelegateFactory.Action_QnButton(function(button) self:OnBuyButtonClick(button) end)

	self:updateCountDown()
	if self.m_CountDownTick then
		UnRegisterTick(self.m_CountDownTick)
		self.m_CountDownTick = nil
	end
	self.m_CountDownTick = RegisterTick(function() self:updateCountDown() end, 1000)
	self.m_TableView.OnSelectAtRow = DelegateFactory.Action_int(function(row) self:OnItemClick(row) end)
	self.m_TableView:ReloadData(true, false)
	self.m_TableView:SetSelectRow(0, true)
end


--确认延长时间
function CLuaMapiMysticlShopWnd:confirmAddTime()
	local cost = MapiMysticalShop_Setting.GetData().ShopWndLast[0]
	if CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp.YumaPlayData.Jifen >= cost then
		Gac2Gas.ExtendMapiShopWnd()
	else
		MessageMgr.Inst:ShowMessage("MAPI_JIFEN_NOT_ENOUGH",{})
	end
end

function CLuaMapiMysticlShopWnd:OnAddTimeButtonClick(button)
	local msg = LocalString.GetString("是否消耗积分延长时间？")
	local cost = MapiMysticalShop_Setting.GetData().ShopWndLast[0]
	local okAction = DelegateFactory.Action(function() self:confirmAddTime() end)
	QnMoneyCostMesssageMgr.ShowMoneyCostMessageBox(EnumMoneyType.Score, msg, cost, okAction, nil, false, true, EnumPlayScoreKey.YuMa, false)
end

--确认刷新
function CLuaMapiMysticlShopWnd:comfirmRefresh()
	if self.m_HasRaleItem then
		local msg = MessageMgr.Inst:FormatMessage("MAPI_SHOP_RAEW_ITEM_CONFIRM", {})
		MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(
			function()
				Gac2Gas.OpenSecretShop(EnumSecretShopType.eMapiShop)
			end
		), nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
	else
		Gac2Gas.OpenSecretShop(EnumSecretShopType.eMapiShop)
	end
end

--刷新按钮点击
function CLuaMapiMysticlShopWnd:OnRefreshButtonClick(button)
	if PlayerSettings.NotNoticeMaPiJiFen then
		self:comfirmRefresh()
	else
		local msg = LocalString.GetString("是否消耗积分用以刷新商盟")
		local cost = MapiMysticalShop_Setting.GetData().OpenShopCost[0]
		local okAction = DelegateFactory.Action(function() self:comfirmRefresh() end)
		QnMoneyCostMesssageMgr.ShowMoneyCostMessageBox(EnumMoneyType.Score, msg, cost, okAction, nil, false, true, EnumPlayScoreKey.YuMa, true)
	end
end
--购买按钮点击
function CLuaMapiMysticlShopWnd:OnBuyButtonClick(button)
	if self.m_CurrentSelectedItem >= 0 and self.m_CurrentSelectedItem < #self.m_Items then
	 	Gac2Gas.RequestBuySecretShopItem(EnumSecretShopType.eMapiShop, self.m_Ids[self.m_CurrentSelectedItem + 1])
	end
end

--更新倒计时
function CLuaMapiMysticlShopWnd:updateCountDown()
	local deltaTime = math.floor(self.m_TimeStamp - CServerTimeMgr.Inst.timeStamp)
	if(deltaTime < 0) then
		CUIManager.CloseUI(CUIResources.MapiMysticalShopWnd)
	else
		if deltaTime > 10 then
		 	self.m_HasOpenedDelayMessage = false
		else
			if not self.m_HasOpenedDelayMessage then
				self.m_HasOpenedDelayMessage = true
				local cost = MapiMysticalShop_Setting.GetData().ShopWndLast[0]
				local msg = SafeStringFormat3(LocalString.GetString("商盟即将关闭,是否使用%d积分来延时?"), cost)
				MessageWndManager.ShowConfirmMessage(msg, 5, false, DelegateFactory.Action(function() self:confirmAddTime() end), nil)
			end
		end
		local fen = math.floor(deltaTime / 60)
		local miao = deltaTime % 60
		if fen > 0 then
			self.m_LeftTimeLabel.text = SafeStringFormat3("[00ff00]%02d:%02d[-]", fen, miao)
		else
			self.m_LeftTimeLabel.text = SafeStringFormat3("[ff0000]%02d:%02d[-]", fen, miao)
		end
	end
end

--选中某件物品
function CLuaMapiMysticlShopWnd:OnItemClick(row)
	self.m_CurrentSelectedItem = row
    self.m_BuyCountButton:SetValue(1)
    self.m_MoneyCtrl:SetType(self.m_ItemMoneyTypes[row + 1], self.m_ItemScoreTypes[row + 1], false)
    self:UpdateTotalPrice()
    self:UpdatePreview(row)
end

function CLuaMapiMysticlShopWnd:UpdatePreview(row)
	local itemData = self.m_Items[row + 1]
	self.m_BuyCountButton:SetMinMax(1, 1, 1)
	self.m_ItemPreviewer:UpdateData(itemData, 0, "")
end

function CLuaMapiMysticlShopWnd:UpdateTotalPrice(count)
	local totalprice = 0;
    if self.m_CurrentSelectedItem >=0 and self.m_CurrentSelectedItem < #self.m_Items then
        totalprice = self.m_Items[self.m_CurrentSelectedItem + 1].Price * self.m_BuyCountButton:GetValue()
    end
    self.m_MoneyCtrl:SetCost(totalprice)
end

function CLuaMapiMysticlShopWnd:OnEnable()
	g_ScriptEvent:AddListener("BuyItemFromSecretShopSuccess", self, "BuyItemFromSecretShopSuccess")
end

function CLuaMapiMysticlShopWnd:OnDisable()
	g_ScriptEvent:RemoveListener("BuyItemFromSecretShopSuccess", self, "BuyItemFromSecretShopSuccess")
end

function CLuaMapiMysticlShopWnd:BuyItemFromSecretShopSuccess()
	local msg = LocalString.GetString("是否消耗积分用以刷新商盟")
	local cost = MapiMysticalShop_Setting.GetData().OpenShopCost[0]
	local okAction = DelegateFactory.Action(function() self:comfirmRefresh() end)
	local cancelAction = DelegateFactory.Action(function() CUIManager.CloseUI(CUIResources.MapiMysticalShopWnd) end)
	QnMoneyCostMesssageMgr.ShowMoneyCostMessageBox(EnumMoneyType.Score, msg, cost, okAction, cancelAction, false, true, EnumPlayScoreKey.YuMa, false)
	
	self.m_HasRaleItem = false
end

function CLuaMapiMysticlShopWnd:OnDestroy()
	if self.m_CountDownTick then
		UnRegisterTick(self.m_CountDownTick)
		self.m_CountDownTick = nil
	end
	LuaDelGlobal("MapiShopWnd_ItemData")
	LuaDelGlobal("MapiShopWnd_PriceData")
	LuaDelGlobal("MapiShopWnd_TimeStamp")
end

return CLuaMapiMysticlShopWnd
