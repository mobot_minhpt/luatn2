local GameObject			= import "UnityEngine.GameObject"
local UITable				= import "UITable"
local UILabel				= import "UILabel"
local UIScrollView			= import "UIScrollView"
local UIScrollViewIndicator = import "UIScrollViewIndicator"
local CTipTitleItem			= import "CTipTitleItem"

local DelegateFactory		= import "DelegateFactory"
local Extensions			= import "Extensions"

local CUIManager			= import "L10.UI.CUIManager"
local CUICommonDef			= import "L10.UI.CUICommonDef"
local CMessageTipMgr		= import "L10.UI.CMessageTipMgr"
local CTipParagraphItem		= import "L10.UI.CTipParagraphItem"

local CommonDefs			= import "L10.Game.CommonDefs"
local MessageMgr			= import "L10.Game.MessageMgr"
local CClientMainPlayer     = import "L10.Game.CClientMainPlayer"

CLuaDwZongziRuleWnd = class()

RegistChildComponent(CLuaDwZongziRuleWnd, "BtnSignUp",			GameObject)
RegistChildComponent(CLuaDwZongziRuleWnd, "Table",				UITable)
RegistChildComponent(CLuaDwZongziRuleWnd, "TitleTemplate",		GameObject)
RegistChildComponent(CLuaDwZongziRuleWnd, "ParagraphTemplate",	GameObject)
RegistChildComponent(CLuaDwZongziRuleWnd, "ScrollView",			UIScrollView)
RegistChildComponent(CLuaDwZongziRuleWnd, "Indicator",			UIScrollViewIndicator)
RegistChildComponent(CLuaDwZongziRuleWnd, "Count",				UILabel)

function CLuaDwZongziRuleWnd.Show()
	CUIManager.ShowUI("DwZongziRuleWnd")
end

function CLuaDwZongziRuleWnd:Init()
    self:ParseRuleText()

    local ct = self:InitCount()
    self.Count.text = tostring(ct)
end

function CLuaDwZongziRuleWnd:InitCount()

    local dwsetting = DuanWu_Setting.GetData()
    if dwsetting == nil then return 0 end

    local taskid = dwsetting.ZongZi2019TaskId
    --local taskdata = Task_Task.GetData(taskid)
    --if taskdata == nil then return 0 end

    local max = 1--写死一次

    local player = CClientMainPlayer.Inst
    if player == nil then return max end

    local tasks = player.TaskProp.RepeatableTasks
    if tasks == nil then return max end

    
    if CommonDefs.DictContains(tasks, typeof(UInt32), taskid) then
        local repeatableTaskInfo = CommonDefs.DictGetValue(tasks, typeof(UInt32), taskid)
        local finishedSubTimes = repeatableTaskInfo.FinishedSubTimes
        local lastct = max - finishedSubTimes
        if lastct < 0 then 
            lastct = 0
        end
        return lastct
    end
    return max
end


function CLuaDwZongziRuleWnd:OnEnable()
	local submitClick = function ( go )
		self:OnSignUpBtnClick()
	end

	CommonDefs.AddOnClickListener(self.BtnSignUp, DelegateFactory.Action_GameObject(submitClick), false)
end

function CLuaDwZongziRuleWnd:OnSignUpBtnClick()
	Gac2Gas.RequestEnterZongZi2019Play()
end


function CLuaDwZongziRuleWnd:ParseRuleText()
	
	Extensions.RemoveAllChildren(self.Table.transform)

    local msg =	MessageMgr.Inst:FormatMessage("DwZongzi_Rule",{});
    if System.String.IsNullOrEmpty(msg) then
        return
    end

    local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(msg)
    if info == nil then return end

    if info.titleVisible then
        local titleGo = CUICommonDef.AddChild(self.Table.gameObject, self.TitleTemplate)
        titleGo:SetActive(true)
        CommonDefs.GetComponent_GameObject_Type(titleGo, typeof(CTipTitleItem)):Init(info.title)
    end

    do
        local i = 0
        while i < info.paragraphs.Count do
            local paragraphGo = CUICommonDef.AddChild(self.Table.gameObject, self.ParagraphTemplate)
            paragraphGo:SetActive(true)
			local tip = CommonDefs.GetComponent_GameObject_Type(paragraphGo, typeof(CTipParagraphItem))
            tip:Init(info.paragraphs[i], 4294967295)
            i = i + 1
        end
    end

    self.ScrollView:ResetPosition()
    self.Indicator:Layout()
    self.Table:Reposition()
end