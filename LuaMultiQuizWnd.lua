require("3rdParty/ScriptEvent")
require("common/common_include")

local UISprite = import "UISprite"
local Gac2Gas = import "L10.Game.Gac2Gas"
local CQuizMgr = import "L10.Game.CQuizMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CUIManager = import "L10.UI.CUIManager"
local Color = import "UnityEngine.Color"
local CUIRes = import "L10.UI.CUIResources"
 
CLuaMultiQuizWnd=class()
RegistClassMember(CLuaMultiQuizWnd,"CloseBtn")
RegistClassMember(CLuaMultiQuizWnd,"Sprite1")
RegistClassMember(CLuaMultiQuizWnd,"Sprite2")
RegistClassMember(CLuaMultiQuizWnd,"Sprite3")
RegistClassMember(CLuaMultiQuizWnd,"Sprite4")

function CLuaMultiQuizWnd:isQuestionAnswered(index)
	return CQuizMgr.Inst:GetMultipleQuizQuestionState(index)
end

function CLuaMultiQuizWnd:Init()

	local function OnQuestionClicked(go)
		local index = -1;
		if go == self.Sprite1 then 
			index =1
		elseif go == self.Sprite2 then
			index = 2
		elseif go == self.Sprite3 then
			index = 3
		elseif go == self.Sprite4 then
			index = 4
		end
		if self:isQuestionAnswered(index) then 
			return 
		end
		CQuizMgr.Inst:SetMultiQuizIndex(index)
		Gac2Gas.OpenMultipleQuestionsWnd(CQuizMgr.Inst.TaskQuestionInfo.taskId,index)
	end
	local function OnQuestionPress(go,flag)
		local trans =  CUICommonDef.TraverseFindChild("FrameSprite",go.transform)
		trans.gameObject:SetActive(flag)
	end
	local function CloseWnd(go)
		CUIManager.CloseUI(CUIRes.MultiQuizWnd)
	end
	
	self:refreshQuestions(nil)
	CommonDefs.AddOnClickListener(self.Sprite1,DelegateFactory.Action_GameObject(OnQuestionClicked),false)
	CommonDefs.AddOnClickListener(self.Sprite2,DelegateFactory.Action_GameObject(OnQuestionClicked),false)
	CommonDefs.AddOnClickListener(self.Sprite3,DelegateFactory.Action_GameObject(OnQuestionClicked),false)
	CommonDefs.AddOnClickListener(self.Sprite4,DelegateFactory.Action_GameObject(OnQuestionClicked),false)
	CommonDefs.AddOnPressListener(self.Sprite1,DelegateFactory.Action_GameObject_bool(OnQuestionPress),false)
	CommonDefs.AddOnPressListener(self.Sprite2,DelegateFactory.Action_GameObject_bool(OnQuestionPress),false)
	CommonDefs.AddOnPressListener(self.Sprite3,DelegateFactory.Action_GameObject_bool(OnQuestionPress),false)
	CommonDefs.AddOnPressListener(self.Sprite4,DelegateFactory.Action_GameObject_bool(OnQuestionPress),false)
	
	CommonDefs.AddOnClickListener(self.CloseBtn,DelegateFactory.Action_GameObject(CloseWnd),false)			
end


function CLuaMultiQuizWnd:OnEnable()
	g_ScriptEvent:AddListener("MultiQuizAnswered", self, "refreshQuestions")
end
function CLuaMultiQuizWnd:OnDisable()
	g_ScriptEvent:RemoveListener("MultiQuizAnswered", self, "refreshQuestions")
end

function CLuaMultiQuizWnd:refreshQuestions(args)
	for index = 1,4 do 
		self:resetState(index)
	end
end
function CLuaMultiQuizWnd:resetState(index)
	local tar = nil
	if index == 1 then 
		tar=self.Sprite1
	elseif index ==2 then 
		tar=self.Sprite2
	elseif index ==3 then 
		tar=self.Sprite3
	elseif index ==4 then 
		tar=self.Sprite4
	end
	if tar~=nil then 	
		local trans = CUICommonDef.TraverseFindChild("CharacterSprite",tar.transform)
		local csprite = trans:GetComponent(typeof(UISprite))
		local bgtrans = CUICommonDef.TraverseFindChild("BGSprite",tar.transform)
		if self:isQuestionAnswered(index) then
			tar:GetComponent(typeof(UISprite)).spriteName = "multiquizwnd_wancheng_03"	
			csprite.color = Color(1,0.945,0.5215)
			bgtrans.gameObject:SetActive(true)
		else
			tar:GetComponent(typeof(UISprite)).spriteName = "multiquizwnd_weiwancheng_01"
			csprite.color = Color(1,1,1)
			bgtrans.gameObject:SetActive(false)
		end
	end
end


return CLuaMultiQuizWnd