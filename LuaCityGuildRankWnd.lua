require("common/common_include")

local UIGrid = import "UIGrid"
local Extensions = import "Extensions"
local UILabel = import "UILabel"
local UISprite = import "UISprite"
local CRankData = import "L10.UI.CRankData"
local NGUITools = import "NGUITools"
local Constants = import "L10.Game.Constants"

CLuaCityGuildRankWnd = class()
CLuaCityGuildRankWnd.Path = "ui/citywar/LuaCityGuildRankWnd"    

RegistClassMember(CLuaCityGuildRankWnd, "m_MyGuildObj")
RegistClassMember(CLuaCityGuildRankWnd, "m_TemplateObj")
RegistClassMember(CLuaCityGuildRankWnd, "m_GuildId2CityDataTable")
RegistClassMember(CLuaCityGuildRankWnd, "m_HaveRankData")

function CLuaCityGuildRankWnd:Awake( ... )
	Gac2Gas.QueryRank(45100002)
	Gac2Gas.QueryCityWarGuildToCityHash()
	self.m_MyGuildObj = self.transform:Find("Rank/MyGuild").gameObject
	self.m_MyGuildObj:SetActive(false)
	self.m_TemplateObj = self.transform:Find("Rank/AdvView/Pool/Template").gameObject
	self.m_TemplateObj:SetActive(false)
	self.m_HaveRankData = false
end

function CLuaCityGuildRankWnd:Init()
	
end

function CLuaCityGuildRankWnd:OnRankDataReady( ... )
	if CLuaRankData.m_CurRankId ~= 45100002 then return end
	self.m_HaveRankData = true
	if self.m_GuildId2CityDataTable then
		self:InitTable()
	end
end

function CLuaCityGuildRankWnd:InitTable( ... )
	local grid = self.transform:Find("Rank/AdvView/Scroll View/Grid"):GetComponent(typeof(UIGrid))
	self.m_MyGuildObj:SetActive(true)
	self:InitItem(self.m_MyGuildObj.transform, CRankData.Inst.MainPlayerRankInfo, true, true)
	Extensions.RemoveAllChildren(grid.transform)
	for i = 0, CRankData.Inst.RankList.Count - 1 do
		local obj = NGUITools.AddChild(grid.gameObject, self.m_TemplateObj)
		obj:SetActive(true)
		self:InitItem(obj.transform, CRankData.Inst.RankList[i], false, i % 2 == 0)
	end
	grid:Reposition()
end

function CLuaCityGuildRankWnd:InitItem(rootTrans, data, isMyGuild, isOdd)
	rootTrans:Find("GuildNameLabel"):GetComponent(typeof(UILabel)).text = data.Guild_Name
	rootTrans:Find("ServerNameLabel"):GetComponent(typeof(UILabel)).text = data.serverName
	rootTrans:Find("GuildGradeLabel"):GetComponent(typeof(UILabel)).text = data.Value
	local guildId = data.PlayerIndex
	if isMyGuild then
		guildId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.GuildId or 0
	end
	local cityData = self.m_GuildId2CityDataTable[guildId]
	if cityData then
		rootTrans:Find("GuildPeopleLabel"):GetComponent(typeof(UILabel)).text = CityWar_Map.GetData(cityData.MapId).Name
		rootTrans:Find("ProgressLabel"):GetComponent(typeof(UILabel)).text = CityWar_MapCity.GetData(cityData.TemplateId).Name
	end
	
	if not isMyGuild then
		rootTrans:GetComponent(typeof(UISprite)).spriteName = isOdd and Constants.GreyItemBgSpriteName or Constants.WhiteItemBgSpriteName
	end

	local rankSprite = rootTrans:Find("RankSprite"):GetComponent(typeof(UISprite))
	rankSprite.gameObject:SetActive(data.Rank <= 3 and data.Rank >= 1)
	if data.Rank > 3 then
		rootTrans:Find("RankLabel"):GetComponent(typeof(UILabel)).text = data.Rank
	elseif data.Rank == 0 then
		rootTrans:Find("RankLabel"):GetComponent(typeof(UILabel)).text = LocalString.GetString("未上榜")
	else
		local spriteNames = {"Rank_No.1", "Rank_No.2png", "Rank_No.3png"}
		rankSprite.spriteName = spriteNames[data.Rank]
	end
end

function CLuaCityGuildRankWnd:QueryCityWarGuildToCityHashResult( dataUD )
	local list = MsgPackImpl.unpack(dataUD)
	if not list then return end

	self.m_GuildId2CityDataTable = {}
	for i = 0, list.Count - 1, 3 do
		local guildId = list[i]
		local mapId = list[i + 1]
		local templateId = list[i + 2]
		self.m_GuildId2CityDataTable[guildId] =  {MapId = mapId, TemplateId = templateId}
	end

	if self.m_HaveRankData then
		self:InitTable()
	end
end

function CLuaCityGuildRankWnd:OnEnable( ... )
	g_ScriptEvent:AddListener("OnRankDataReady", self, "OnRankDataReady")
	g_ScriptEvent:AddListener("QueryCityWarGuildToCityHashResult", self, "QueryCityWarGuildToCityHashResult")
end

function CLuaCityGuildRankWnd:OnDisable( ... )
	g_ScriptEvent:RemoveListener("OnRankDataReady", self, "OnRankDataReady")
	g_ScriptEvent:RemoveListener("QueryCityWarGuildToCityHashResult", self, "QueryCityWarGuildToCityHashResult")
end