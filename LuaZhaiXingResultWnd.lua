local UIWidget = import "UIWidget"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CUIFx = import "L10.UI.CUIFx"
local UILabel = import "UILabel"
local CButton = import "L10.UI.CButton"
local CUICommonDef = import "L10.UI.CUICommonDef"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local EShareType = import "L10.UI.EShareType"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CLoginMgr = import "L10.Game.CLoginMgr"

LuaZhaiXingResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaZhaiXingResultWnd, "m_Fx", "Fx", CUIFx)
RegistChildComponent(LuaZhaiXingResultWnd, "m_NumLabel", "NumLabel", UILabel)
RegistChildComponent(LuaZhaiXingResultWnd, "m_TimeLabel", "TimeLabel", UILabel)
RegistChildComponent(LuaZhaiXingResultWnd, "m_Anchor", "Anchor", UIWidget)

--@endregion RegistChildComponent end
RegistClassMember(LuaZhaiXingResultWnd, "m_Score")
RegistClassMember(LuaZhaiXingResultWnd, "m_Time")
RegistClassMember(LuaZhaiXingResultWnd, "m_Tweener")
RegistClassMember(LuaZhaiXingResultWnd, "m_RankBtn")
RegistClassMember(LuaZhaiXingResultWnd, "m_ShareBtn")
RegistClassMember(LuaZhaiXingResultWnd, "m_HideWhenCapture")
RegistClassMember(LuaZhaiXingResultWnd, "m_PlayerInfoNode")

RegistClassMember(LuaZhaiXingResultWnd, "m_Title1")
RegistClassMember(LuaZhaiXingResultWnd, "m_Title2")
RegistClassMember(LuaZhaiXingResultWnd, "m_Arrow")
RegistClassMember(LuaZhaiXingResultWnd, "m_RecordLab")

function LuaZhaiXingResultWnd:Awake()
    self.m_RankBtn  = self.transform:Find("Anchor/HideWhenCapture/RankBtn"):GetComponent(typeof(CButton))
	self.m_ShareBtn = self.transform:Find("Anchor/HideWhenCapture/ShareBtn"):GetComponent(typeof(CButton))
    self.m_HideWhenCapture  = self.transform:Find("Anchor/HideWhenCapture").gameObject
    self.m_PlayerInfoNode   = self.transform:Find("Anchor/PlayerInfoNode").gameObject
    self.m_Title1   =  self.transform:Find("Anchor/Title/Label").gameObject
    self.m_Title2   =  self.transform:Find("Anchor/Title/NewRecord").gameObject
    self.m_Arrow    =  self.transform:Find("Anchor/Main/NumLabel/Arrow"):GetComponent(typeof(UIWidget))
    self.m_RecordLab=  self.transform:Find("Anchor/Main/RecordLab"):GetComponent(typeof(UILabel))
    --@region EventBind: Dont Modify Manually!
    self.m_ShareBtn.gameObject:SetActive(not CommonDefs.IS_VN_CLIENT)
    UIEventListener.Get(self.m_RankBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRankBtnClick()
	end)

	UIEventListener.Get(self.m_ShareBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareBtnClick()
	end)

    --@endregion EventBind end
    self.m_Score = LuaZhaiXingMgr.m_Score
    self.m_Time  = LuaZhaiXingMgr.m_Time
    self.m_HistoryBest = LuaZhaiXingMgr.m_HistoryBest
end

function LuaZhaiXingResultWnd:Init()
    if CLoginMgr.Inst then
		local myGameServer = CLoginMgr.Inst:GetSelectedGameServer()
		CommonDefs.GetComponent_Component_Type(self.m_PlayerInfoNode.transform:Find("Server"), typeof(UILabel)).text = myGameServer.name
	end

	if CClientMainPlayer.Inst then
		CommonDefs.GetComponent_Component_Type(self.m_PlayerInfoNode.transform:Find("Name"), typeof(UILabel)).text = CClientMainPlayer.Inst.Name
		CommonDefs.GetComponent_Component_Type(self.m_PlayerInfoNode.transform:Find("Icon/Lv"), typeof(UILabel)).text = tostring(CClientMainPlayer.Inst.Level)
		CommonDefs.GetComponent_Component_Type(self.m_PlayerInfoNode.transform:Find("Icon"), typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(CClientMainPlayer.Inst.Class, CClientMainPlayer.Inst.Gender, -1), false)
		CommonDefs.GetComponent_Component_Type(self.m_PlayerInfoNode.transform:Find("ID"), typeof(UILabel)).text = tostring(CClientMainPlayer.Inst.Id)
		CommonDefs.GetComponent_Component_Type(self.m_PlayerInfoNode.transform:Find("Server"), typeof(UILabel)).text = CClientMainPlayer.Inst:GetMyServerName()
	end

    LuaTweenUtils.TweenAlpha(self.m_Anchor, 0, 1, 0.2)
    LuaTweenUtils.TweenScale(self.m_Anchor.transform, Vector3(0.9, 0.9, 0.9), Vector3(0.9, 0.9, 0.9), 0.2)
    LuaTweenUtils.Kill(self.m_Tweener, false)
    self.m_Tweener = LuaTweenUtils.TweenFloat(0, self.m_Score, 0.5, function ( val )
		self.m_NumLabel.text = tonumber(SafeStringFormat3("%.f", val))
	end)
    
    self.m_TimeLabel.text = self:GetRemainTimeText(self.m_Time)
    self.m_RecordLab.text = SafeStringFormat3(LocalString.GetString("历史最高分 %d"), self.m_HistoryBest)
    local isNewRecord = self.m_Score > self.m_HistoryBest
    self.m_Title1:SetActive(not isNewRecord)
    self.m_Title2:SetActive(isNewRecord)
    self.m_Arrow.gameObject:SetActive(false)
    if isNewRecord then
        CommonDefs.OnComplete_Tweener(self.m_Tweener, DelegateFactory.TweenCallback(function()
            self.m_Arrow:ResetAndUpdateAnchors()
            self.m_Arrow.gameObject:SetActive(true)
        end))
    end
end

--@region UIEvent

function LuaZhaiXingResultWnd:OnRankBtnClick()
    CUIManager.ShowUI(CLuaUIResources.ZhaiXingRankWnd)
end

function LuaZhaiXingResultWnd:OnShareBtnClick()
    CUICommonDef.CaptureScreen(
        "screenshot",
        true,
        false,
        self.m_HideWhenCapture,
        DelegateFactory.Action_string_bytes(
            function(fullPath, jpgBytes)
                ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, fullPath)
            end
        ),
        false
    )
end


--@endregion UIEvent

function LuaZhaiXingResultWnd:OnDestroy()
    LuaZhaiXingMgr.m_Score = 0
    LuaZhaiXingMgr.m_Time  = 0
    LuaTweenUtils.Kill(self.m_Tweener, false)
end

function LuaZhaiXingResultWnd:GetRemainTimeText(totalSeconds)
    if totalSeconds < 0 then
        totalSeconds = 0
    end
    if totalSeconds >= 3600 then
        return SafeStringFormat3(LocalString.GetString("%02d:%02d:%02d"), math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
    else
        return SafeStringFormat3(LocalString.GetString("%02d:%02d"), math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end
