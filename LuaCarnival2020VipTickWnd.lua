local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local CUITexture = import "L10.UI.CUITexture"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemMgr = import "L10.Game.CItemMgr"
local CButton = import "L10.UI.CButton"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local DateTime = import "System.DateTime"

LuaCarnival2020VipTickWnd = class() --et less : ignore
RegistChildComponent(LuaCarnival2020VipTickWnd,"CloseButton", GameObject)
RegistChildComponent(LuaCarnival2020VipTickWnd,"ItemNode1", GameObject)
RegistChildComponent(LuaCarnival2020VipTickWnd,"ItemNode2", GameObject)

RegistClassMember(LuaCarnival2020VipTickWnd, "maxChooseNum")

function LuaCarnival2020VipTickWnd:Close()
	CUIManager.CloseUI(CLuaUIResources.Carnival2020VipTicketWnd)
end

function LuaCarnival2020VipTickWnd:InitBuyTipText()
  self.BuyTipTextTable = {}
  local locationString = LocalString.GetString('活动地点：杭州国际博览中心1D会场（萧山区奔竞大道353号）\n[c][FF0000]本商品不支持退款，购票后所有福利须线下现场检票后领取。[-][c]')
  self.BuyTipTextTable[5] = LocalString.GetString('[c][E3F88F]白色小福蝶票[-][c]购买须知：\n请确认您现在购买的是：\n白色小福蝶票（仅8月30日可用）\n') .. locationString
  self.BuyTipTextTable[6] = LocalString.GetString('[c][E3F88F]银发大魔王票[-][c]购买须知：\n请确认您现在购买的是：\n银发大魔王票（仅8月30日可用）\n') .. locationString
end

function LuaCarnival2020VipTickWnd:OnEnable()
	--g_ScriptEvent:AddListener("", self, "")
end

function LuaCarnival2020VipTickWnd:OnDisable()
	--g_ScriptEvent:RemoveListener("", self, "")
end

function LuaCarnival2020VipTickWnd:InitSpeBuyBtn(buyBtn,goodsId,index)
  local now = CServerTimeMgr.Inst:GetZone8Time()
  local beginOfTheBuyDay = CreateFromClass(DateTime, 2020, 8, 20, 0, 0, 0)
  local endOfTheBuyDay = CreateFromClass(DateTime, 2020, 8, 27, 0, 0, 0)
  --local endOfTheShowDay = CreateFromClass(DateTime, 2020, 8, 31, 0, 0, 0)
  local seconds = beginOfTheBuyDay:Subtract(now).TotalSeconds
  local endSeconds = endOfTheBuyDay:Subtract(now).TotalSeconds
  --local endShowSeconds = endOfTheShowDay:Subtract(now).TotalSeconds
  if seconds >= 0 then
    buyBtn:GetComponent(typeof(CButton)).Enabled = false
    buyBtn.transform:Find('Label'):GetComponent(typeof(UILabel)).text = LocalString.GetString('8月20日开抢')
  elseif seconds < 0 and endSeconds >= 0 then
    if LuaWelfareMgr.Carnival2020AlreadyBuy then
      buyBtn:GetComponent(typeof(CButton)).Enabled = false
      buyBtn.transform:Find('Label'):GetComponent(typeof(UILabel)).text = LocalString.GetString('已抢购')
    elseif LuaWelfareMgr.Carnival2020TodayRestNum and LuaWelfareMgr.Carnival2020TodayRestNum[goodsId] and LuaWelfareMgr.Carnival2020TodayRestNum[goodsId] > 0 then
      if now.Hour < 12 then
        buyBtn:GetComponent(typeof(CButton)).Enabled = false
        buyBtn.transform:Find('Label'):GetComponent(typeof(UILabel)).text = LocalString.GetString('12点开抢')
      else
        buyBtn:GetComponent(typeof(CButton)).Enabled = true
        buyBtn.transform:Find('Label'):GetComponent(typeof(UILabel)).text = LocalString.GetString('购买')
        local onBuyClick = function(go)
          if self.BuyTipTextTable and self.BuyTipTextTable[index] then
            local message = self.BuyTipTextTable[index]
            MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function ()
              Gac2Gas.RequestBuyJiaNianHua2020Ticket(goodsId)
            end), nil, LocalString.GetString("购买"), nil, false)
          end
        end
        CommonDefs.AddOnClickListener(buyBtn,DelegateFactory.Action_GameObject(onBuyClick),false)
      end
    else--if LuaWelfareMgr.Carnival2020TodayCannotBuy then
      buyBtn:GetComponent(typeof(CButton)).Enabled = false
      buyBtn.transform:Find('Label'):GetComponent(typeof(UILabel)).text = LocalString.GetString('今日已售罄')
    end
  elseif endSeconds < 0 then
    buyBtn:GetComponent(typeof(CButton)).Enabled = false
    buyBtn.transform:Find('Label'):GetComponent(typeof(UILabel)).text = LocalString.GetString('售票结束')
  end
end

function LuaCarnival2020VipTickWnd:InitNodeItem(node,index)
  local data = JiaNianHua_2020Sale.GetData(index)
  if data then
    local itemsId = data.ItemsId
    local goodsId = data.GoodsId
    local buyBtn = node.transform:Find('BuyButton').gameObject

    if goodsId then
      self:InitSpeBuyBtn(buyBtn,goodsId,index)
    end

    for i=1,6 do
      local itemNode = node.transform:Find('Item'..i)
      if itemNode then
        itemNode.gameObject:SetActive(false)
      end
    end

    local itemsTable = g_LuaUtil:StrSplit(itemsId, ",")
    if itemsTable then
      for i,v in pairs(itemsTable) do
        local itemId = tonumber(v)
        local itemData = CItemMgr.Inst:GetItemTemplate(itemId)

        local itemNode = node.transform:Find('Item'..i).gameObject
        itemNode:SetActive(true)
        local bonusIcon = node.transform:Find('Item'..i..'/Normal/IconTexture'):GetComponent(typeof(CUITexture))
        bonusIcon:LoadMaterial(itemData.Icon)
        local iconLabel = node.transform:Find('Item'..i..'/Normal/DescLabel').gameObject
        iconLabel:SetActive(false)

        local clickNode = node.transform:Find('Item'..i).gameObject
        UIEventListener.Get(clickNode).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(clickNode).onClick, DelegateFactory.VoidDelegate(function (go)
          CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType.Default, 0, 0, 0, 0)
        end), true)

      end
    end
  end
end

function LuaCarnival2020VipTickWnd:Init()
  self:InitBuyTipText()
	local onCloseClick = function(go)
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.CloseButton,DelegateFactory.Action_GameObject(onCloseClick),false)

  local ticketNum = JiaNianHua_Setting.GetData().Carnival2020TicketNum
  local buyLabel1 = self.ItemNode1.transform:Find('PriceLabel'):GetComponent(typeof(UILabel))
  buyLabel1.text = tostring(ticketNum) .. LocalString.GetString('当票积分+688元')
  local buyLabel2 = self.ItemNode2.transform:Find('PriceLabel'):GetComponent(typeof(UILabel))
  buyLabel2.text = tostring(ticketNum) .. LocalString.GetString('当票积分+1288元')
  self:InitNodeItem(self.ItemNode1,5)
  self:InitNodeItem(self.ItemNode2,6)
end

return LuaCarnival2020VipTickWnd
