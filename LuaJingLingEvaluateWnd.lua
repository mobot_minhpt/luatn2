local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CButton = import "L10.UI.CButton"
local CJingLingMgr = import "L10.Game.CJingLingMgr"
local JingLingMessageExtraInfoKey = import "L10.Game.JingLingMessageExtraInfoKey"

LuaJingLingEvaluateWnd = class()

RegistClassMember(LuaJingLingEvaluateWnd,"starArray")       -- 星星集合
RegistClassMember(LuaJingLingEvaluateWnd,"desLabel")        -- 星星下的描述文本
RegistClassMember(LuaJingLingEvaluateWnd,"evaluatePanel")   -- 评价面板
RegistClassMember(LuaJingLingEvaluateWnd,"tagPanel")        -- 标签面板
RegistClassMember(LuaJingLingEvaluateWnd,"tagScrollView")   -- 标签滚动视图
RegistClassMember(LuaJingLingEvaluateWnd,"tagGrid")         -- 标签网格
RegistClassMember(LuaJingLingEvaluateWnd,"tagTemplate")     -- 标签模板
RegistClassMember(LuaJingLingEvaluateWnd,"otherWordInput")  -- 其他建议输入框
RegistClassMember(LuaJingLingEvaluateWnd,"submitBtn")       -- 提交按钮

RegistClassMember(LuaJingLingEvaluateWnd,"starCount")       -- 选择的星星数
RegistClassMember(LuaJingLingEvaluateWnd,"selectedTagTb") -- 保存被选中标签的文本
RegistClassMember(LuaJingLingEvaluateWnd,"selectedTagCount") -- 被选标签个数
RegistClassMember(LuaJingLingEvaluateWnd,"minGoodLevel")   -- 好评价的最低星星数

function LuaJingLingEvaluateWnd:Awake()
    self.minGoodLevel = 4
    self.desLabel = self.transform:Find("Parent/DesLabel"):GetComponent(typeof(UILabel))
    self.evaluatePanel = self.transform:Find("Parent/InitNode").gameObject
    self.tagPanel = self.transform:Find("Parent/InfoNode").gameObject
    self.tagScrollView = self.tagPanel.transform:Find("ScrollView"):GetComponent(typeof(UIScrollView))
    self.tagGrid = self.tagScrollView.transform:Find("Grid"):GetComponent(typeof(UIGrid))
    self.tagTemplate = self.transform:Find("Parent/ItemNode").gameObject
    self.otherWordInput = self.tagPanel.transform:Find("StatusText"):GetComponent(typeof(UIInput))
    self.submitBtn = self.transform:Find("Parent/SubmitBtn"):GetComponent(typeof(CButton))
end

function LuaJingLingEvaluateWnd:Init()
    -- 获取精灵评价标签
    LuaJingLingMgr:QueryJingLingEvaluateTag()
    -- 5个星星
    self.starArray = {}
    local starParentNode = self.transform:Find("Parent/area3/star")
    for i = 0, starParentNode.childCount - 1 do
        local go = starParentNode:GetChild(i).gameObject
        local star = go.transform:Find("star").gameObject
        table.insert(self.starArray, star)
        UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnStarClick(go)
        end)
    end
    -- 提交按钮
    UIEventListener.Get(self.submitBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnSubmitBtnClick()
    end)
end

function LuaJingLingEvaluateWnd:InitEvaluatePanel()
    -- 各星级评价条目
    for i = 0, self.evaluatePanel.transform.childCount - 1 do
        local label = self.evaluatePanel.transform:GetChild(i):Find("text"):GetComponent(typeof(UILabel))
        label.text = LuaJingLingMgr.m_EvaluateDict[tostring(i + 1)]
    end
    self:InitPanel(0)
end

function LuaJingLingEvaluateWnd:InitPanel(count)
    self.starCount = count
    local isShow = false
    if count > 0 and count <= 5 then isShow = true end
    self.tagPanel:SetActive(isShow)
    self.evaluatePanel:SetActive(not isShow)
    self.submitBtn.Enabled = isShow
    for i = 1, #self.starArray do
        if i <= count then self.starArray[i]:SetActive(true)
        else self.starArray[i]:SetActive(false) end
    end
    if LuaJingLingMgr.m_EvaluateDict ~= nil and LuaJingLingMgr.m_EvaluateTagMap ~= nil and count > 0 then
        self.desLabel.text = LuaJingLingMgr.m_EvaluateDict[tostring(count)]
        self:InitTagScrollView(count)
    end
end

function LuaJingLingEvaluateWnd:InitTagScrollView(count)
    Extensions.RemoveAllChildren(self.tagGrid.transform)
    self.selectedTagTb = {}
    self.selectedTagCount = 0
    local tagList = LuaJingLingMgr.m_EvaluateTagMap[tostring(count)]
    if tagList == nil then return end
    for i = 2, #tagList do
        local tag = NGUITools.AddChild(self.tagGrid.gameObject, self.tagTemplate)
        tag:SetActive(true)
        tag.transform:Find("text"):GetComponent(typeof(UILabel)).text = tagList[i]
        UIEventListener.Get(tag).onClick = DelegateFactory.VoidDelegate(function (go) 
            self:OnTagClcik(i, tag)
        end)
    end
    self.tagGrid:Reposition()
    self.tagScrollView:ResetPosition()
end

function LuaJingLingEvaluateWnd:GetTagString()
    local tagStr = nil
    for _, text in pairs(self.selectedTagTb) do
        tagStr = tagStr == nil and text or tagStr .. "," .. text
    end
    return tagStr
end

--@region UIEvent
function LuaJingLingEvaluateWnd:OnStarClick(go)
    local count = tonumber(go.name)
    self:InitPanel(count)
end

function LuaJingLingEvaluateWnd:OnTagClcik(index, tag)
    local selectBg = tag.transform:Find("clicksign").gameObject
    selectBg:SetActive(not selectBg.activeSelf)
    self.selectedTagTb[index] = selectBg.activeSelf and LuaJingLingMgr.m_EvaluateTagMap[tostring(self.starCount)][index] or nil
    self.selectedTagCount = selectBg.activeSelf and self.selectedTagCount + 1 or self.selectedTagCount - 1
end

function LuaJingLingEvaluateWnd:OnSubmitBtnClick()
    if self.starCount <= 0 or self.starCount >5 then return end
    -- 未选择任意标签
    if self.starCount <5 and (self.selectedTagCount == nil or self.selectedTagCount == 0) then
        g_MessageMgr:ShowMessage("CHOOSE_ONE_OPTION_ALERT")
        return
    end
    local msg = CJingLingMgr.Inst.saveMsg
    local isHelpful = self.starCount >= self.minGoodLevel and true or false
    --专区的评价不经过精灵评价接口，防止给精灵带来差评的
    if msg.extraInfo ~= nil and CommonDefs.DictContains(msg.extraInfo, typeof(String), JingLingMessageExtraInfoKey.ZhuanQuIssueID) then
        LuaJingLingMgr:SendEvaluateZhuanQu(isHelpful)
    else
        LuaJingLingMgr:SendEvaluateJingLing(self.otherWordInput.value, self:GetTagString(), self.starCount, isHelpful)
    end
end
--@endregion UIEvent
function LuaJingLingEvaluateWnd:OnEnable()
    g_ScriptEvent:AddListener("InitEvaluatePanel", self, "InitEvaluatePanel")
    g_ScriptEvent:AddListener("EvaluateContactAssistantSign", self, "InitTagScrollView")
end

function LuaJingLingEvaluateWnd:OnDisable()
    g_ScriptEvent:RemoveListener("InitEvaluatePanel", self, "InitEvaluatePanel")
    g_ScriptEvent:RemoveListener("EvaluateContactAssistantSign", self, "InitTagScrollView")
end