local UITabBar = import "L10.UI.UITabBar"
local  UIAlertIcon = import "L10.UI.UIAlertIcon"
local CItemMgr = import "L10.Game.CItemMgr"
local CItem = import "L10.Game.CItem"
local QualityColor = import "L10.Game.QualityColor"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CButton = import "L10.UI.CButton"
local CItemExchangeMaterial = import "L10.UI.CItemExchangeMaterial"

LuaSpokesmanCardsComposeWnd = class()

RegistChildComponent(LuaSpokesmanCardsComposeWnd, "m_UITabBar","UITabBar",UITabBar)
RegistChildComponent(LuaSpokesmanCardsComposeWnd, "m_InfoLabel1","InfoLabel1",UILabel)
RegistChildComponent(LuaSpokesmanCardsComposeWnd, "m_InfoLabel2","InfoLabel2",UILabel)
RegistChildComponent(LuaSpokesmanCardsComposeWnd, "m_ItemLabel","ItemLabel",UILabel)
RegistChildComponent(LuaSpokesmanCardsComposeWnd, "m_Table","Table",UITable)
RegistChildComponent(LuaSpokesmanCardsComposeWnd, "m_DuplicateComposeButton","DuplicateComposeButton",CButton)
RegistChildComponent(LuaSpokesmanCardsComposeWnd, "m_DuplicateNumLabel","DuplicateNumLabel",UILabel)
RegistChildComponent(LuaSpokesmanCardsComposeWnd, "m_SingleComposeButton","SingleComposeButton",CButton)
RegistChildComponent(LuaSpokesmanCardsComposeWnd, "m_SingleNumLabel","SingleNumLabel",UILabel)
RegistChildComponent(LuaSpokesmanCardsComposeWnd, "m_ConsumeNumLabel","ConsumeNumLabel",UILabel)
RegistChildComponent(LuaSpokesmanCardsComposeWnd, "m_Alert1","Alert1", UIAlertIcon)
RegistChildComponent(LuaSpokesmanCardsComposeWnd, "m_Alert2","Alert2", UIAlertIcon)
RegistChildComponent(LuaSpokesmanCardsComposeWnd, "m_ItemConsume","ItemConsume", CItemExchangeMaterial)

RegistClassMember(LuaSpokesmanCardsComposeWnd, "m_Index")

function LuaSpokesmanCardsComposeWnd:Init()
    self.m_UITabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
        self:OnTabChange(go, index)
    end)
    self:DefaultInitTabBar()
    self:InitInfoLabel()
    UIEventListener.Get(self.m_DuplicateComposeButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
        self.m_UITabBar:ChangeTab(self.m_Index, false)
        Gac2Gas.SpokesmanTCGComposeCard(self.m_Index == 1,true)
    end)
    UIEventListener.Get(self.m_SingleComposeButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
        self.m_UITabBar:ChangeTab(self.m_Index, false)
        Gac2Gas.SpokesmanTCGComposeCard(self.m_Index == 1,false)
    end)
end

function LuaSpokesmanCardsComposeWnd:OnEnable()
    g_ScriptEvent:AddListener("SpokesmanCardsWnd_LoadData",self,"InitInfoLabel")
    g_ScriptEvent:AddListener("SpokesmanCardsWnd_UpdateData",self,"UpdateData")
    g_ScriptEvent:AddListener("SendItem", self, "UpdateData")
    g_ScriptEvent:AddListener("SetItemAt", self, "UpdateData")
end

function LuaSpokesmanCardsComposeWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SpokesmanCardsWnd_LoadData",self,"InitInfoLabel")
    g_ScriptEvent:RemoveListener("SpokesmanCardsWnd_UpdateData",self,"UpdateData")
    g_ScriptEvent:RemoveListener("SendItem", self, "UpdateData")
    g_ScriptEvent:RemoveListener("SetItemAt", self, "UpdateData")
end

function LuaSpokesmanCardsComposeWnd:UpdateData()
    self.m_UITabBar:ChangeTab(self.m_Index, false)
    self:InitInfoLabel()
end

function LuaSpokesmanCardsComposeWnd:OnTabChange(go, index)
    self.m_Index = index
    local haiTangEnough, superHaiTangEnough = self:IsConsumeItemEnough()
    self.m_Alert1.Visible = index == 1 and haiTangEnough
    self.m_Alert2.Visible = index == 0 and superHaiTangEnough
    local itemId = index == 0 and  tonumber(SpokesmanTCG_Setting.GetData("HaiTangItemId").Value) or
            tonumber(SpokesmanTCG_Setting.GetData("SuperHaiTangItemId").Value)
    local item = Item_Item.GetData(itemId)
    local haiTangCount = CItemMgr.Inst:GetItemCount(itemId)
    local cost = index == 0 and tonumber(SpokesmanTCG_Setting.GetData("ComposeHaiTangCost").Value) or  tonumber(SpokesmanTCG_Setting.GetData("ComposeSuperHaiTangCost").Value)
    self.m_DuplicateNumLabel.text = CUICommonDef.TranslateToNGUIText(
            SafeStringFormat3((cost * 3) <= haiTangCount and "%sx%d" or "%s#Rx%d",item.Name, cost * 3))
    self.m_SingleNumLabel.text = CUICommonDef.TranslateToNGUIText(
            SafeStringFormat3(cost <= haiTangCount and "%sx%d" or "%s#Rx%d",item.Name, cost))
    self.m_ConsumeNumLabel.text = CUICommonDef.TranslateToNGUIText(
            SafeStringFormat3(0 < haiTangCount and "%sx%d" or "%s#Rx%d",item.Name, haiTangCount))
    self.m_SingleComposeButton.Enabled = cost <= haiTangCount
    self.m_DuplicateComposeButton.Enabled = (cost * 3) <= haiTangCount
    self.m_ItemConsume:Init(itemId,1,haiTangCount == 0 and 1 or haiTangCount, haiTangCount == 0 and true or false)
    self.m_ItemConsume.amount.gameObject:SetActive(false)
end

function LuaSpokesmanCardsComposeWnd:InitInfoLabel()
    local composeExtraReward = SpokesmanTCG_Setting.GetData("ComposeExtraRewardItems").Value
    local splits = g_LuaUtil:StrSplit(composeExtraReward, ";")
    local last = - 1
    local itemId = 0
    local composeTimes = CLuaSpokesmanMgr.m_SpokesmanCardsComposeNum
    for _,s in pairs(splits) do
        local info = g_LuaUtil:StrSplit(s, ",")
        local num = tonumber(info[1])
        if not num then
            break
        end
        itemId = tonumber(info[2])
        if composeTimes < num then
            last = num -  composeTimes
            break
        end
    end
    local item = Item_Item.GetData(itemId)

    if (last == -1) or (not item) then
        self.m_Table.gameObject:SetActive(false)
        return
    end
    local qualityType = CItem.GetQualityType(item.ID)
    local qualityColor = QualityColor.GetRGBValue(qualityType)
    self.m_InfoLabel1.text = CUICommonDef.TranslateToNGUIText(g_MessageMgr:FormatMessage("SpokesmanCardsComposeWnd_InfoLabel1", last))
    self.m_InfoLabel2.text = CUICommonDef.TranslateToNGUIText(g_MessageMgr:FormatMessage("SpokesmanCardsComposeWnd_InfoLabel2", composeTimes))
    self.m_ItemLabel.text = SafeStringFormat3("[%s]",item.Name)
    self.m_ItemLabel.color = qualityColor
    UIEventListener.Get(self.m_ItemLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemId)
    end)
    self.m_Table:Reposition()
end

function LuaSpokesmanCardsComposeWnd:DefaultInitTabBar()
    local haiTangEnough, superHaiTangEnough = self:IsConsumeItemEnough()
    self.m_UITabBar:ChangeTab((superHaiTangEnough and (not haiTangEnough)) and 1 or 0, false)
end

function LuaSpokesmanCardsComposeWnd:IsConsumeItemEnough()
    local haiTangItemId = tonumber(SpokesmanTCG_Setting.GetData("HaiTangItemId").Value)
    local superHaiTangItemId = tonumber(SpokesmanTCG_Setting.GetData("SuperHaiTangItemId").Value)
    local composeHaiTangCost = tonumber(SpokesmanTCG_Setting.GetData("ComposeHaiTangCost").Value)
    local composeSuperHaiTangCost = tonumber(SpokesmanTCG_Setting.GetData("ComposeSuperHaiTangCost").Value)
    local haiTangCount = CItemMgr.Inst:GetItemCount(haiTangItemId)
    local superHaiTangCount = CItemMgr.Inst:GetItemCount(superHaiTangItemId)
    local haiTangEnough = composeHaiTangCost <= haiTangCount
    local superHaiTangEnough = composeSuperHaiTangCost <= superHaiTangCount
    return haiTangEnough, superHaiTangEnough
end

