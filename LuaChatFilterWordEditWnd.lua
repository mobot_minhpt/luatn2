local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local QnInput = import "L10.UI.QnInput"
local QnDragableView = import "L10.UI.QnDragableView"
local QnTableItem = import "L10.UI.QnTableItem"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"

LuaChatFilterWordEditWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaChatFilterWordEditWnd, "TableView", "TableView", QnAdvanceGridView)
RegistChildComponent(LuaChatFilterWordEditWnd, "TipButton", "TipButton", GameObject)
RegistChildComponent(LuaChatFilterWordEditWnd, "AddButton", "AddButton", GameObject)
RegistChildComponent(LuaChatFilterWordEditWnd, "DesLabel", "DesLabel", UILabel)

--@endregion RegistChildComponent end

RegistClassMember(LuaChatFilterWordEditWnd,"m_dirty")
RegistClassMember(LuaChatFilterWordEditWnd,"m_datas")
RegistClassMember(LuaChatFilterWordEditWnd,"m_dragview")
RegistClassMember(LuaChatFilterWordEditWnd,"m_ds")

function LuaChatFilterWordEditWnd:Awake()
	LuaChatMgr.LoadFilterWords()
	self.TableView.m_ScrollView.disableDragIfFits = true
	self.m_dragview = self.TableView.gameObject:GetComponent(typeof(QnDragableView))

    --@region EventBind: Dont Modify Manually!
	UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)

	UIEventListener.Get(self.AddButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnAddButtonClick()
	end)

    --@endregion EventBind end
end

function LuaChatFilterWordEditWnd:OnEnable()
	g_ScriptEvent:AddListener("OnChatFilterWordsChanged",self,"OnChatFilterWordsChanged")
end

function LuaChatFilterWordEditWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnChatFilterWordsChanged",self,"OnChatFilterWordsChanged")
    self:SaveData()
end

function LuaChatFilterWordEditWnd:SaveData()
	if CClientMainPlayer.Inst == nil then return end 
	if self.m_dirty then
		LuaChatMgr.SaveFilterWords(self.m_datas)
	else
		g_ScriptEvent:BroadcastInLua("OnChatFilterWordsChanged")
	end
end

function LuaChatFilterWordEditWnd:OnChatFilterWordsChanged()
	self:Init()
end

function LuaChatFilterWordEditWnd:Init()
	self.m_datas = LuaChatMgr.FilterWords
	self:Refresh()
end

function LuaChatFilterWordEditWnd:Refresh()
	if self.m_datas == nil then 
		self.m_datas = {} 
	end

	local function initItem(item,index)
		self:InitWordItem(item,index)
	end

	local function getCount()
		return #self.m_datas
	end
	self.m_ds = DefaultTableViewDataSource.Create(getCount,initItem)
	self.TableView.m_DataSource = self.m_ds

	self.m_dragview.OnObjectDraged = DelegateFactory.Action_GameObject_int(function (go,index)
		self:InitWordItemBasic(go,index)
	end)

	self.m_dragview.OnObjectDragedEnd = DelegateFactory.Action_int_int(function (srcRow,tarRow)
		self:Resort(srcRow,tarRow)
	end)

	self:ShowWords()
end

function LuaChatFilterWordEditWnd:Resort(srcRow,tarRow)
	if srcRow == tarRow or tarRow <0 or srcRow<0 or tarRow>8 then
		return
	end
	local data = self.m_datas[srcRow+1]
	if tarRow > srcRow then
		table.remove(self.m_datas, srcRow+1)
		table.insert(self.m_datas, tarRow+1, data)
	else
		table.remove(self.m_datas,srcRow+1)
		table.insert(self.m_datas, tarRow+1, data)
	end

	self.m_dirty = true
	self:ShowWords()
end

--拖拽控件数据初始化
function LuaChatFilterWordEditWnd:InitWordItemBasic(item,index)
	local rindex = index+1
	local data = self.m_datas[rindex]
	local trans = item.transform
	local indexLabel = LuaGameObject.GetChildNoGC(trans,"IndexLabel").label
	local switchBtn = LuaGameObject.GetChildNoGC(trans,"SwitchButton").qnButton
	local nameInput = LuaGameObject.GetChildNoGC(trans,"NameInput").gameObject
	local wordInput = nameInput:GetComponent(typeof(QnInput))

	indexLabel.text = tostring(rindex)
	wordInput.Text = data.Word
	switchBtn:SetSelected(data.Enabled,false)

end

function LuaChatFilterWordEditWnd:InitWordItem(item,index)
	local rindex = index+1
	local data = self.m_datas[rindex]
	local trans = item.transform
	local indexLabel = LuaGameObject.GetChildNoGC(trans,"IndexLabel").label
	local delBtn = LuaGameObject.GetChildNoGC(trans,"DelButton").gameObject
	local switchBtn = LuaGameObject.GetChildNoGC(trans,"SwitchButton").qnButton
	local nameInput = LuaGameObject.GetChildNoGC(trans,"NameInput").gameObject
	local wordInput = nameInput:GetComponent(typeof(QnInput))
	
	local qnitem = item.gameObject:GetComponent(typeof(QnTableItem))
	qnitem.enableHighlightedSprite = false

	wordInput:ClearReference()
	switchBtn.OnButtonSelected = nil

	indexLabel.text = tostring(rindex)
	wordInput.Text = data.Word
	wordInput.m_Input.characterLimit = 0
	switchBtn:SetSelected(data.Enabled,false)

	UIEventListener.Get(delBtn).onClick = DelegateFactory.VoidDelegate(function (go)
		self:DeleteWord(rindex)
	end)

	switchBtn.OnButtonSelected = DelegateFactory.Action_bool(function (value)
		if value then
			local enabledCount = self:GetEnabledWordsCount()
			if enabledCount >= 8 then
				switchBtn:SetSelected(false,false)
				return
			end
		end
		if data.Enabled ~= value then
			data.Enabled = value
			self.m_dirty=true
		end
	end)

	local func = DelegateFactory.Action_string(function (value)
		data.Word = value
		self.m_dirty = true
	end)

	wordInput.OnValueChanged = func
end

function LuaChatFilterWordEditWnd:DeleteWord(index)
	local data = self.m_datas[index]

	local okfunc = function()
		self.m_dirty = true
		table.remove(self.m_datas,index)
		self:ShowWords()
	end

	if System.String.IsNullOrEmpty(data.Word) then
		okfunc()
		return
	end

	local msg = ""
	if data.Enabled then
		msg = g_MessageMgr:FormatMessage("Monitor_Channel_Delete_Enable",data.Word)
	else
		msg = g_MessageMgr:FormatMessage("Monitor_Channel_Delete_Disable",data.Word)
	end

	local okstr = LocalString.GetString("删除")
	g_MessageMgr:ShowOkCancelMessage(msg,okfunc,nil,okstr,nil,true)
end

function LuaChatFilterWordEditWnd:GetEnabledWordsCount()
	local res =0
	for i=1,#self.m_datas do
		if self.m_datas[i].Enabled then
			res = res + 1
		end
	end
	return res
end

function LuaChatFilterWordEditWnd:ShowWords()
	self.TableView:ReloadData(true,false)
	self.m_dragview:Init()
	CUICommonDef.SetActive(self.AddButton, #self.m_datas<8, true)
end

--@region UIEvent

function LuaChatFilterWordEditWnd:OnTipButtonClick()
	g_MessageMgr:ShowMessage("Monitor_Channel_Rules")
end

function LuaChatFilterWordEditWnd:OnAddButtonClick()
	local count = #self.m_datas
	if count >=8 then return end
	self.m_datas[count+1]={Enabled = true,Word = ""}
	self.m_dirty = true
	self:ShowWords()
end

--@endregion UIEvent

