local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local Screen=import "UnityEngine.Screen"
local System = import "System"
local EPlayerFightProp = import "L10.Game.EPlayerFightProp"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local Constants = import "L10.Game.Constants"
local CLingShouMgr = import "L10.Game.CLingShouMgr"
local CInputBoxMgr = import "L10.UI.CInputBoxMgr"
local CIndirectUIResources = import "L10.UI.CIndirectUIResources"
local CHousePuppetMgr = import "L10.Game.CHousePuppetMgr"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local CClientFakePlayer = import "L10.Game.CClientFakePlayer"


CLuaPlayerPuppetInfoWnd = class()
RegistClassMember(CLuaPlayerPuppetInfoWnd,"backBtn")
RegistClassMember(CLuaPlayerPuppetInfoWnd,"modelTexture")
RegistClassMember(CLuaPlayerPuppetInfoWnd,"templateNode")
RegistClassMember(CLuaPlayerPuppetInfoWnd,"table")
RegistClassMember(CLuaPlayerPuppetInfoWnd,"scrollView")
RegistClassMember(CLuaPlayerPuppetInfoWnd,"swipeGo")
RegistClassMember(CLuaPlayerPuppetInfoWnd,"renameBtn")
RegistClassMember(CLuaPlayerPuppetInfoWnd,"titleName")
RegistClassMember(CLuaPlayerPuppetInfoWnd,"sourceName")
RegistClassMember(CLuaPlayerPuppetInfoWnd,"canWalkBtn")
RegistClassMember(CLuaPlayerPuppetInfoWnd,"canWalkSign")
RegistClassMember(CLuaPlayerPuppetInfoWnd,"curRotation")
RegistClassMember(CLuaPlayerPuppetInfoWnd,"sections")
RegistClassMember(CLuaPlayerPuppetInfoWnd,"propertiesDict")
RegistClassMember(CLuaPlayerPuppetInfoWnd, "m_ModelTextureLoader")

function CLuaPlayerPuppetInfoWnd:Awake()
    -- self.backBtn = self.transform:Find("Wnd_Bg_Primary_Normal/CloseButton").gameObject
    self.modelTexture = self.transform:Find("Anchor/PlayerPropertyView/Panel/ModelTexture"):GetComponent(typeof(UITexture))
    self.templateNode = self.transform:Find("Anchor/PlayerPropertyView/PlayerDetailPropertyView/Item").gameObject
    self.table = self.transform:Find("Anchor/PlayerPropertyView/PlayerDetailPropertyView/Scroll View/Table"):GetComponent(typeof(UITable))
    self.scrollView = self.transform:Find("Anchor/PlayerPropertyView/PlayerDetailPropertyView/Scroll View"):GetComponent(typeof(UIScrollView))
    self.swipeGo = self.transform:Find("Anchor/PlayerPropertyView/Panel/ModelTexture").gameObject
    self.renameBtn = self.transform:Find("Anchor/PlayerPropertyView/renameBtn").gameObject
    self.titleName = self.transform:Find("Anchor/PlayerPropertyView/Title"):GetComponent(typeof(UILabel))
    self.sourceName = self.transform:Find("Anchor/PlayerPropertyView/Source/text"):GetComponent(typeof(UILabel))
    self.canWalkBtn = self.transform:Find("Anchor/PlayerPropertyView/WalkSwitchButton").gameObject
    self.canWalkSign = self.transform:Find("Anchor/PlayerPropertyView/WalkSwitchButton/Checkmark").gameObject
self.curRotation = 180
self.sections = nil
self.propertiesDict = nil

end

-- Auto Generated!!
function CLuaPlayerPuppetInfoWnd:Init( )
    -- UIEventListener.Get(self.backBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
    --     self:Close()
    -- end)
    UIEventListener.Get(self.swipeGo).onDrag = DelegateFactory.VectorDelegate(function(go,v)
        self:OnDragModel(go,v)
    end)
    -- < MakeDelegateFromCSFunction >(self.OnDragModel, VectorDelegate, self)
    UIEventListener.Get(self.renameBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:Rename(go)
    end)

    -- < MakeDelegateFromCSFunction >(self.Rename, VoidDelegate, self)
    UIEventListener.Get(self.canWalkBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:SetWalkState(go)
    end)
    -- < MakeDelegateFromCSFunction >(self.SetWalkState, VoidDelegate, self)
    self.templateNode:SetActive(false)
    self:InitProp()
    self.m_ModelTextureLoader = LuaDefaultModelTextureLoader.Create(function (ro)
        self:Load(ro)
    end)
    self:LoadModel()
    self:InitDetailInfo()


end

function CLuaPlayerPuppetInfoWnd:OnEnable()
    g_ScriptEvent:AddListener("HouseSetPlayerPuppetNameSucc", self, "SetNameSucc")
    g_ScriptEvent:AddListener("HouseUpdatePuppetData", self, "InitDetailInfo")
end

function CLuaPlayerPuppetInfoWnd:OnDisable()
    g_ScriptEvent:RemoveListener("HouseSetPlayerPuppetNameSucc", self, "SetNameSucc")
    g_ScriptEvent:RemoveListener("HouseUpdatePuppetData", self, "InitDetailInfo")
end

function CLuaPlayerPuppetInfoWnd:SetNameSucc(args)
    local name=args[0]
    self.titleName.text = name
end
function CLuaPlayerPuppetInfoWnd:OnDragModel(go,delta)
    local deltaVal = -delta.x / Screen.width * 360
    self.curRotation = self.curRotation + deltaVal
    CUIManager.SetModelRotation("__MainPlayerModelCamera__", self.curRotation)
end
function CLuaPlayerPuppetInfoWnd:LoadModel()
    self.modelTexture.mainTexture = CUIManager.CreateModelTexture("__MainPlayerModelCamera__",self.m_ModelTextureLoader,180,0.05,-1.06,4.66,false,true,1,false,false)
end
function CLuaPlayerPuppetInfoWnd:OnDestroy()
    CUIManager.DestroyModelTexture("__MainPlayerModelCamera__")
end

function CLuaPlayerPuppetInfoWnd:InitDetailInfo( )
    if CommonDefs.DictContains(CHousePuppetMgr.Inst.EngineToIdxDic, typeof(UInt32), CHousePuppetMgr.Inst.SavePuppetEngineId) then
        local idx = CommonDefs.DictGetValue(CHousePuppetMgr.Inst.EngineToIdxDic, typeof(UInt32), CHousePuppetMgr.Inst.SavePuppetEngineId)
        if idx > 0 and CClientHouseMgr.Inst.mCurFurnitureProp ~= nil and CommonDefs.DictContains(CClientHouseMgr.Inst.mCurFurnitureProp.FriendPuppetInfo, typeof(Byte), idx) then
            local playerPuppetInfo = CommonDefs.DictGetValue(CClientHouseMgr.Inst.mCurFurnitureProp.FriendPuppetInfo, typeof(Byte), idx)
            if System.String.IsNullOrEmpty(playerPuppetInfo.AssignName) then
                self.titleName.text = playerPuppetInfo.Name
            else
                self.titleName.text = playerPuppetInfo.AssignName
            end
            self.sourceName.text = playerPuppetInfo.Name

            if playerPuppetInfo.IsPatrol > 0 then
                self.canWalkSign:SetActive(true)
            else
                self.canWalkSign:SetActive(false)
            end
        end
    end
end
function CLuaPlayerPuppetInfoWnd:SetWalkState( go) 
    if CommonDefs.DictContains(CHousePuppetMgr.Inst.EngineToIdxDic, typeof(UInt32), CHousePuppetMgr.Inst.SavePuppetEngineId) then
        local idx = CommonDefs.DictGetValue(CHousePuppetMgr.Inst.EngineToIdxDic, typeof(UInt32), CHousePuppetMgr.Inst.SavePuppetEngineId)
        Gac2Gas.SetPuppetIsPatrol(idx, not self.canWalkSign.activeSelf)
        --canWalkSign.SetActive(!canWalkSign.activeSelf);
    end
end
function CLuaPlayerPuppetInfoWnd:Rename( go) 
    local uplimit = CLingShouMgr.Inst.maxNameLength
    -- int.Parse(limit);
    local content = System.String.Format(LocalString.GetString("请输入傀儡名称(最多{0}个字)"), 7 --[[Constants.MaxCharLimitForPlayerName / 2]])
    if CommonDefs.IsSeaMultiLang() then
        content = System.String.Format(LocalString.GetString("请输入傀儡名称(最多{0}个字)"), Constants.SeaMaxCharLimitForPlayerName)
    end
    CInputBoxMgr.ShowInputBox(content, DelegateFactory.Action_string(function (str) 
        --int uplimit = CLingShouMgr.Inst.maxNameLength;// int.Parse(limit);
        --string str = Extensions.RemoveBlank(input.value);//移除空格
        local nameStr = string.gsub(str, " ", "")

        if CommonDefs.IsSeaMultiLang() then
            local length = CommonDefs.StringLength(nameStr)
            if length < Constants.SeaMinCharLimitForPlayerName or length > Constants.SeaMaxCharLimitForPlayerName then
                g_MessageMgr:ShowMessage("Sea_Login_Create_Role_Failed_Name_Length_Not_Fit", Constants.SeaMinCharLimitForPlayerName, Constants.SeaMaxCharLimitForPlayerName)
                return
            end
        else
            local length = CUICommonDef.GetStrByteLength(nameStr)
            if length < Constants.MinCharLimitForPlayerName or CUICommonDef.GetStrByteLength(nameStr) > Constants.MaxCharLimitForPlayerName then
                g_MessageMgr:ShowMessage("Login_Create_Role_Failed_Name_Length_Not_Fit")
                return
            end
        end
        if System.Text.RegularExpressions.Regex.IsMatch(nameStr, "^[0-9]+$") then
            g_MessageMgr:ShowMessage("Login_Create_Role_Failed_Name_Cannot_Be_All_Numbers")
            return
        end
        if not CWordFilterMgr.Inst:CheckName(nameStr) then
            g_MessageMgr:ShowMessage("Name_Violation")
            return
        end
        --采用新的名字
        Gac2Gas.RenameHousePuppet(CHousePuppetMgr.Inst.SavePuppetEngineId, nameStr)
        CUIManager.CloseUI(CIndirectUIResources.InputBox)
    end), 70 --[[Constants.MaxCharLimitForPlayerName * 5]], true, nil, nil)
    --名字输入时不做限制
end
function CLuaPlayerPuppetInfoWnd:Load( renderObj) 
    local obj = CClientObjectMgr.Inst:GetObject(CHousePuppetMgr.Inst.SavePuppetEngineId)
    local mainPlayer = TypeAs(obj, typeof(CClientFakePlayer))
    if mainPlayer ~= nil then
        CClientMainPlayer.LoadResource(renderObj, mainPlayer.AppearanceProp, true, 1, 0, false, 0, false, 0, false, nil)

        if mainPlayer.AppearanceProp.ResourceId ~= 0 then
            local scale = 1
            local data = Transform_Transform.GetData(mainPlayer.AppearanceProp.ResourceId)
            if data ~= nil then
                scale = data.PreviewScale
            end
            renderObj.Scale = scale
        end
    end
end
function CLuaPlayerPuppetInfoWnd:InitProp( )
    self.sections = {
        LocalString.GetString("续航"), 
        LocalString.GetString("攻击"), 
        LocalString.GetString("防御"), 
        LocalString.GetString("抗性"), 
        LocalString.GetString("加强"), 
        LocalString.GetString("忽视"), 
        LocalString.GetString("其他")
    }
    self.propertiesDict = {}
    for i,v in ipairs(self.sections) do
        table.insert( self.propertiesDict,{} )
    end
    --续航 6项
    table.insert(self.propertiesDict[1],{LocalString.GetString("当前气血"), EPlayerFightProp.Hp, "FightProp_Tips_Hp"})
    table.insert(self.propertiesDict[1],{LocalString.GetString("当前气血上限"), EPlayerFightProp.CurrentHpFull, "FightProp_Tips_CurrentHpFull"})
    table.insert(self.propertiesDict[1],{LocalString.GetString("最大气血上限"), EPlayerFightProp.HpFull, "FightProp_Tips_HpFull"})
    table.insert(self.propertiesDict[1],{LocalString.GetString("当前法力"), EPlayerFightProp.Mp, "FightProp_Tips_Mp"})
    table.insert(self.propertiesDict[1],{LocalString.GetString("当前法力上限"), EPlayerFightProp.CurrentMpFull, "FightProp_Tips_CurrentMpFull"})
    table.insert(self.propertiesDict[1],{LocalString.GetString("最大法力上限"), EPlayerFightProp.MpFull, "FightProp_Tips_MpFull"})
    --攻击 11项
    table.insert(self.propertiesDict[2],{LocalString.GetString("最小物攻"), EPlayerFightProp.pAttMin, "FightProp_Tips_pAttMin"})
    table.insert(self.propertiesDict[2],{LocalString.GetString("最大物攻"), EPlayerFightProp.pAttMax, "FightProp_Tips_pAttMax"})
    table.insert(self.propertiesDict[2],{LocalString.GetString("最小法攻"), EPlayerFightProp.mAttMin, "FightProp_Tips_mAttMin"})
    table.insert(self.propertiesDict[2],{LocalString.GetString("最大法攻"), EPlayerFightProp.mAttMax, "FightProp_Tips_mAttMax"})
    table.insert(self.propertiesDict[2],{LocalString.GetString("物攻速度"), EPlayerFightProp.pSpeed, "FightProp_Tips_pSpeed"})
    table.insert(self.propertiesDict[2],{LocalString.GetString("法攻速度"), EPlayerFightProp.mSpeed, "FightProp_Tips_mSpeed"})
    table.insert(self.propertiesDict[2],{LocalString.GetString("物理命中"), EPlayerFightProp.pHit, "FightProp_Tips_pHit"})
    table.insert(self.propertiesDict[2],{LocalString.GetString("法术命中"), EPlayerFightProp.mHit, "FightProp_Tips_mHit"})
    table.insert(self.propertiesDict[2],{LocalString.GetString("物理致命"), EPlayerFightProp.pFatal, "FightProp_Tips_pFatal"})
    table.insert(self.propertiesDict[2],{LocalString.GetString("物理致命伤害"), EPlayerFightProp.pFatalDamage, "FightProp_Tips_pFatalDamage"})
    table.insert(self.propertiesDict[2],{LocalString.GetString("法术致命"), EPlayerFightProp.mFatal, "FightProp_Tips_mFatal"})
    table.insert(self.propertiesDict[2],{LocalString.GetString("法术致命伤害"), EPlayerFightProp.mFatalDamage, "FightProp_Tips_mFatalDamage"})
    table.insert(self.propertiesDict[2],{LocalString.GetString("破盾"), EPlayerFightProp.AntiBlock, "FightProp_Tips_AntiBlock"})
    --防御 8项
    table.insert(self.propertiesDict[3],{LocalString.GetString("物理防御"), EPlayerFightProp.pDef, "FightProp_Tips_pDef"})
    table.insert(self.propertiesDict[3],{LocalString.GetString("法术防御"), EPlayerFightProp.mDef, "FightProp_Tips_mDef"})
    table.insert(self.propertiesDict[3],{LocalString.GetString("物理躲避"), EPlayerFightProp.pMiss, "FightProp_Tips_pMiss"})
    table.insert(self.propertiesDict[3],{LocalString.GetString("法术躲避"), EPlayerFightProp.mMiss, "FightProp_Tips_mMiss"})
    table.insert(self.propertiesDict[3],{LocalString.GetString("格挡"), EPlayerFightProp.Block, "FightProp_Tips_Block"})
    table.insert(self.propertiesDict[3],{LocalString.GetString("格挡伤害减免"), EPlayerFightProp.BlockDamage, "FightProp_Tips_BlockDamage"})
    table.insert(self.propertiesDict[3],{LocalString.GetString("抗物理致命"), EPlayerFightProp.AntipFatal, "FightProp_Tips_AntipFatal"})
    table.insert(self.propertiesDict[3],{LocalString.GetString("抗法术致命"), EPlayerFightProp.AntimFatal, "FightProp_Tips_AntimFatal"})
    table.insert(self.propertiesDict[3],{LocalString.GetString("物理减伤比例"), EPlayerFightProp.MulpHurt, "FightProp_Tips_MulpHurt"})
    table.insert(self.propertiesDict[3],{LocalString.GetString("法术减伤比例"), EPlayerFightProp.MulmHurt, "FightProp_Tips_MulmHurt"})
    table.insert(self.propertiesDict[3],{LocalString.GetString("物理受伤减少"), EPlayerFightProp.AdjpHurt, "FightProp_Tips_AdjpHurt"})
    table.insert(self.propertiesDict[3],{LocalString.GetString("法术受伤减少"), EPlayerFightProp.AdjmHurt, "FightProp_Tips_AdjmHurt"})
    --抗性 26项
    table.insert(self.propertiesDict[4],{LocalString.GetString("眩晕抗性"), EPlayerFightProp.AntiDizzy, "FightProp_Tips_AntiDizzy"})
    table.insert(self.propertiesDict[4],{LocalString.GetString("睡眠抗性"), EPlayerFightProp.AntiSleep, "FightProp_Tips_AntiSleep"})
    table.insert(self.propertiesDict[4],{LocalString.GetString("混乱抗性"), EPlayerFightProp.AntiChaos, "FightProp_Tips_AntiChaos"})
    table.insert(self.propertiesDict[4],{LocalString.GetString("定身抗性"), EPlayerFightProp.AntiBind, "FightProp_Tips_AntiBind"})
    table.insert(self.propertiesDict[4],{LocalString.GetString("沉默抗性"), EPlayerFightProp.AntiSilence, "FightProp_Tips_AntiSilence"})
    table.insert(self.propertiesDict[4],{LocalString.GetString("束缚抗性"), EPlayerFightProp.AntiTie, "FightProp_Tips_AntiTie"})
    table.insert(self.propertiesDict[4],{LocalString.GetString("石化抗性"), EPlayerFightProp.AntiPetrify, "FightProp_Tips_AntiPetrify"})
    table.insert(self.propertiesDict[4],{LocalString.GetString("变狐抗性"), EPlayerFightProp.AntiBianHu, "FightProp_Tips_AntiBianHu"})
    table.insert(self.propertiesDict[4],{LocalString.GetString("冰冻抗性"), EPlayerFightProp.AntiFreeze, "FightProp_Tips_AntiFreeze"})
    table.insert(self.propertiesDict[4],{LocalString.GetString("眩晕抵当"), EPlayerFightProp.DizzyTimeChange, "FightProp_Tips_DizzyTimeChange"})
    table.insert(self.propertiesDict[4],{LocalString.GetString("睡眠抵当"), EPlayerFightProp.SleepTimeChange, "FightProp_Tips_SleepTimeChange"})
    table.insert(self.propertiesDict[4],{LocalString.GetString("混乱抵当"), EPlayerFightProp.ChaosTimeChange, "FightProp_Tips_ChaosTimeChange"})
    table.insert(self.propertiesDict[4],{LocalString.GetString("定身抵当"), EPlayerFightProp.BindTimeChange, "FightProp_Tips_BindTimeChange"})
    table.insert(self.propertiesDict[4],{LocalString.GetString("沉默抵当"), EPlayerFightProp.SilenceTimeChange, "FightProp_Tips_SilenceTimeChange"})
    table.insert(self.propertiesDict[4],{LocalString.GetString("束缚抵当"), EPlayerFightProp.TieTimeChange, "FightProp_Tips_TieTimeChange"})
    table.insert(self.propertiesDict[4],{LocalString.GetString("石化抵当"), EPlayerFightProp.PetrifyTimeChange, "FightProp_Tips_PetrifyTimeChange"})
    table.insert(self.propertiesDict[4],{LocalString.GetString("火抗性"), EPlayerFightProp.AntiFire, "FightProp_Tips_AntiFire"})
    table.insert(self.propertiesDict[4],{LocalString.GetString("电抗性"), EPlayerFightProp.AntiThunder, "FightProp_Tips_AntiThunder"})
    table.insert(self.propertiesDict[4],{LocalString.GetString("风抗性"), EPlayerFightProp.AntiWind, "FightProp_Tips_AntiWind"})
    table.insert(self.propertiesDict[4],{LocalString.GetString("冰抗性"), EPlayerFightProp.AntiIce, "FightProp_Tips_AntiIce"})
    table.insert(self.propertiesDict[4],{LocalString.GetString("毒抗性"), EPlayerFightProp.AntiPoison, "FightProp_Tips_AntiPoison"})
    table.insert(self.propertiesDict[4],{LocalString.GetString("光抗性"), EPlayerFightProp.AntiLight, "FightProp_Tips_AntiLight"})
    table.insert(self.propertiesDict[4],{LocalString.GetString("幻抗性"), EPlayerFightProp.AntiIllusion, "FightProp_Tips_AntiIllusion"})
    table.insert(self.propertiesDict[4],{LocalString.GetString("水抗性"), EPlayerFightProp.AntiWater, "FightProp_Tips_AntiWater"})
    table.insert(self.propertiesDict[4],{LocalString.GetString("抗物理致命伤害"), EPlayerFightProp.AntipFatalDamage, "FightProp_Tips_AntipFatalDamage"})
    table.insert(self.propertiesDict[4],{LocalString.GetString("抗法术致命伤害"), EPlayerFightProp.AntimFatalDamage, "FightProp_Tips_AntimFatalDamage"})

    --加强 6项
    table.insert(self.propertiesDict[5],{LocalString.GetString("强眩晕"), EPlayerFightProp.EnhanceDizzy, "FightProp_Tips_EnhanceDizzy"})
    table.insert(self.propertiesDict[5],{LocalString.GetString("强睡眠"), EPlayerFightProp.EnhanceSleep, "FightProp_Tips_EnhanceSleep"})
    table.insert(self.propertiesDict[5],{LocalString.GetString("强混乱"), EPlayerFightProp.EnhanceChaos, "FightProp_Tips_EnhanceChaos"})
    table.insert(self.propertiesDict[5],{LocalString.GetString("强定身"), EPlayerFightProp.EnhanceBind, "FightProp_Tips_EnhanceBind"})
    table.insert(self.propertiesDict[5],{LocalString.GetString("强沉默"), EPlayerFightProp.EnhanceSilence, "FightProp_Tips_EnhanceSilence"})
    table.insert(self.propertiesDict[5],{LocalString.GetString("强束缚"), EPlayerFightProp.EnhanceTie, "FightProp_Tips_EnhanceTie"})
    table.insert(self.propertiesDict[5],{LocalString.GetString("强变狐"), EPlayerFightProp.EnhanceBianHu, "FightProp_Tips_EnhanceBianHu"})
    table.insert(self.propertiesDict[5],{LocalString.GetString("强石化"), EPlayerFightProp.EnhancePetrify, "FightProp_Tips_EnhancePetrify"})

    --忽视 7项
    table.insert(self.propertiesDict[6],{LocalString.GetString("忽视火抗"), EPlayerFightProp.IgnoreAntiFire, "FightProp_Tips_IgnoreAntiFire"})
    table.insert(self.propertiesDict[6],{LocalString.GetString("忽视电抗"), EPlayerFightProp.IgnoreAntiThunder, "FightProp_Tips_IgnoreAntiThunder"})
    table.insert(self.propertiesDict[6],{LocalString.GetString("忽视风抗"), EPlayerFightProp.IgnoreAntiWind, "FightProp_Tips_IgnoreAntiWind"})
    table.insert(self.propertiesDict[6],{LocalString.GetString("忽视冰抗"), EPlayerFightProp.IgnoreAntiIce, "FightProp_Tips_IgnoreAntiIce"})
    table.insert(self.propertiesDict[6],{LocalString.GetString("忽视毒抗"), EPlayerFightProp.IgnoreAntiPoison, "FightProp_Tips_IgnoreAntiPoison"})
    table.insert(self.propertiesDict[6],{LocalString.GetString("忽视光抗"), EPlayerFightProp.IgnoreAntiLight, "FightProp_Tips_IgnoreAntiLight"})
    table.insert(self.propertiesDict[6],{LocalString.GetString("忽视幻抗"), EPlayerFightProp.IgnoreAntiIllusion, "FightProp_Tips_IgnoreAntiIllusion"})
    table.insert(self.propertiesDict[6],{LocalString.GetString("忽视格挡伤害减免"), EPlayerFightProp.AntiBlockDamage, "FightProp_Tips_AntiBlockDamage"})

    --其他 2项
    table.insert(self.propertiesDict[7],{LocalString.GetString("隐身"), EPlayerFightProp.Invisible, "FightProp_Tips_Invisible"})
    table.insert(self.propertiesDict[7],{LocalString.GetString("反隐身"), EPlayerFightProp.TrueSight, "FightProp_Tips_TrueSight"})


    Extensions.RemoveAllChildren(self.table.transform)
    for i,v in ipairs(self.sections) do
        local go = CUICommonDef.AddChild(self.table.gameObject, self.templateNode)
        go:SetActive(true)
        local script = go:GetComponent(typeof(CCommonLuaScript))
        script.m_LuaSelf:Init(v,self.propertiesDict[i],CHousePuppetMgr.Inst.SavePuppetFightProperty, nil)
    end

    self.table:Reposition()
    self.scrollView:ResetPosition()
end

