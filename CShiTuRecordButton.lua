-- Auto Generated!!
local CRecordingInfoMgr = import "L10.UI.CRecordingInfoMgr"
local CShiTuRecordButton = import "L10.UI.CShiTuRecordButton"
local CUICommonDef = import "L10.UI.CUICommonDef"
local EnumRecordContext = import "L10.Game.EnumRecordContext"
local EnumVoicePlatform = import "L10.Game.EnumVoicePlatform"
local EChatPanel = import "L10.Game.EChatPanel"
CShiTuRecordButton.m_OnPress_CS2LuaHook = function (this, press) 
    if press then
        CRecordingInfoMgr.ShowRecordingWnd(EnumRecordContext.Chat, EChatPanel.Team, this.gameObject, EnumVoicePlatform.Default, false)
    else
        CRecordingInfoMgr.CloseRecordingWnd(not this.gameObject:Equals(CUICommonDef.SelectedUI))
    end
end
