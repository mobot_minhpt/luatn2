local CHTTPForm = import "L10.Game.CHTTPForm"
local CServerTimeMgr=import "L10.Game.CServerTimeMgr"
local CLingShouMgr=import "L10.Game.CLingShouMgr"
local EPlayerFightProp=import "L10.Game.EPlayerFightProp"
local Json = import "L10.Game.Utils.Json"
local CPersonalSpaceMgr=import "L10.Game.CPersonalSpaceMgr"
local Main = import "L10.Engine.Main"
local CDeepLinkMgr = import "L10.Game.CDeepLinkMgr"

--reload的处理
if rawget(_G, "CLuaPersonalSpaceMgr") then
    g_ScriptEvent:RemoveListener("GetAllLingShouOverviewWithContext", CLuaPersonalSpaceMgr, "OnGetAllLingShouOverviewWithContext")
    g_ScriptEvent:RemoveListener("GetLingShouDetails", CLuaPersonalSpaceMgr, "OnGetLingShouDetails")
else
    CLuaPersonalSpaceMgr = {}
end
g_ScriptEvent:AddListener("GetAllLingShouOverviewWithContext", CLuaPersonalSpaceMgr, "OnGetAllLingShouOverviewWithContext")
g_ScriptEvent:AddListener("GetLingShouDetails", CLuaPersonalSpaceMgr, "OnGetLingShouDetails")

--todo
function CLuaPersonalSpaceMgr.PushRoleInfo()
end

CLuaPersonalSpaceMgr.m_PushExtraInfoTime = 0
function CLuaPersonalSpaceMgr.PushPlayerExtraInfo()
    if not CClientMainPlayer.Inst then return end

    local time1 = math.floor(CServerTimeMgr.Inst:GetZone8Time().Ticks/10000000)
    local time2 = CLuaPersonalSpaceMgr.m_PushExtraInfoTime
    -- print(time1,time2,time1-time2)
    if time1-time2>30*60 then
        RegisterTickOnce(function()--这里加tick主要是为了保证拿到战斗力数据
            CLuaPersonalSpaceMgr.m_PushExtraInfoTime = time1
            CLuaPersonalSpaceMgr.m_LastRequestLingShouId = nil
            Gac2Gas.RequestAllLingShouOverview(1)--lingshou
            CLuaPersonalSpaceMgr.PushPlayerCapacity()
        end,1000)
    end
end

CLuaPersonalSpaceMgr.m_LastRequestLingShouId = nil
function CLuaPersonalSpaceMgr.OnGetAllLingShouOverviewWithContext(self,args)
    local context = args[0]
    if context~=1 then return end

    local list = CLingShouMgr.Inst:GetLingShouOverviewList()
    if list and list.Count>0 then
        for i=1,list.Count do
            local lingshouId = list[i-1].id
            CLuaPersonalSpaceMgr.m_LastRequestLingShouId = lingshouId
            Gac2Gas.RequestLingShouDetails(lingshouId, 0)
        end
    end
end
function CLuaPersonalSpaceMgr.OnGetLingShouDetails(self,args)
    local lingshouId = args[0]
    if CLuaPersonalSpaceMgr.m_LastRequestLingShouId ~= lingshouId then return end
    CLuaPersonalSpaceMgr.m_LastRequestLingShouId = nil
    local t = {}
    t["lingshous"] = {}
    local list = CLingShouMgr.Inst:GetLingShouOverviewList()
    for i=1,list.Count do
        local details = CLingShouMgr.Inst:GetLingShouDetails(list[i-1].id)
        if not details then--直接终止
            return
        end
        local lingshou = details.data
        local templateId = lingshou.TemplateId
        local data = LingShou_LingShou.GetData(templateId)
        local item = {
            lingshouId = details.id,
            templateName = data and data.Name or "",
            aliasName = lingshou.Name,
            quality = CLuaLingShouMgr.GetLingShouQuality(lingshou.Props.Quality),
            nature = lingshou.Props.Nature,
            type = CLuaLingShouMgr.GetLingShouType(templateId),
            level = lingshou.Level,
            isMorphed = CLuaLingShouMgr.IsMorphed(lingshou),
            morphTemplateName = CLuaLingShouMgr.GetMorphedName(lingshou),
            isMarried = lingshou.Props.MarryInfo.MarryTime > 0,
            gender = lingshou.Props.MarryInfo.AdultStartTime > 0 and lingshou.Props.MarryInfo.Gender or 0,
            skills = CLuaLingShouMgr.GetSkills(details),
            attack = CLuaLingShouMgr.GetAttacks(details),
            hp = math.floor(details.fightProperty:GetParam(EPlayerFightProp.HpFull)),
            wuxing = details.strWuxing,
            chengzhang = details.strChengzhang,
            zizhi = math.floor(lingshou.Props.ZizhiLevel),
            corZiZhi = math.floor(details.fightProperty:GetParam(EPlayerFightProp.CorZizhi)),
            staZiZhi = math.floor(details.fightProperty:GetParam(EPlayerFightProp.StaZizhi)),
            strZiZhi = math.floor(details.fightProperty:GetParam(EPlayerFightProp.StrZizhi)),
            intZiZhi = math.floor(details.fightProperty:GetParam(EPlayerFightProp.IntZizhi)),
            agiZiZhi = math.floor(details.fightProperty:GetParam(EPlayerFightProp.AgiZizhi)),
            zhandouli = CLuaLingShouMgr.GetLingShouZhandouli(lingshou),
        }
        table.insert( t["lingshous"],item )
    end

    local strJSON = luaJson.table2json(t)

    local serverId = CPersonalSpaceMgr.Inst:GetOriginalServerId()
    local roleid=CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0

    local url = CPersonalSpaceMgr.BASE_URL.."gdc/qnm.do"..SafeStringFormat3("?roleid=%s&serverid=%s&name=%s",roleid,serverId,"rolelingshou")

    local form = CHTTPForm()
    local time = math.floor(CServerTimeMgr.Inst.timeStamp * 1000)
    local skey = CPersonalSpaceMgr.Inst.Token

    form:AddField("time", tostring(time))
    form:AddField("roleid", tostring(roleid))
    form:AddField("serverid", tostring(serverId))
    form:AddField("skey", skey)
    form:AddField("name", "rolelingshou")
    form:AddField("token",CPersonalSpaceMgr.MakeToken(roleid,time,strJSON))
    form:AddField("rawcontent", strJSON)

    Main.Inst:StartCoroutine(CPersonalSpaceMgr.Inst:DoPost(url, form, DelegateFactory.Action_bool_string(function (success, json)
    end)))
end

function CLuaPersonalSpaceMgr.PushPlayerCapacity()
    if not CClientMainPlayer.Inst then return end
    local serverId = CPersonalSpaceMgr.Inst:GetOriginalServerId()
    local roleid=CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0

    local url = CPersonalSpaceMgr.BASE_URL.."qnm/info/set_fighting_capacity"

    local form = CHTTPForm()
    local time = math.floor(CServerTimeMgr.Inst.timeStamp * 1000)
    local skey = CPersonalSpaceMgr.Inst.Token

    form:AddField("time", tostring(time))
    form:AddField("roleid", tostring(roleid))
    form:AddField("serverid", tostring(serverId))
    form:AddField("skey", skey)
    local t = {}
    local capacity = SafeStringFormat3("0,%s",CClientMainPlayer.Inst.PlayProp.PlayerCapacity)
    table.insert( t,capacity )

    CLuaPlayerCapacityMgr.InitModuleDef()
    for i,v in ipairs(CLuaPlayerCapacityMgr.m_ModuleDef) do
    if not v.ignore then
        for j,subv in ipairs(v.sub) do
            local val = CLuaPlayerCapacityMgr.GetValue(subv[1])
            table.insert( t,SafeStringFormat3("%s,%s",subv[1],val))
        end
    end
    end
    local content = table.concat( t, ";")

    form:AddField("value", content)

    Main.Inst:StartCoroutine(CPersonalSpaceMgr.Inst:DoPost(url, form, DelegateFactory.Action_bool_string(function (success, json)
    end)))
end

function CLuaPersonalSpaceMgr.GetPlayerCapacity(playerid,playername)
    local serverId = CPersonalSpaceMgr.Inst:GetOriginalServerId()
    local roleid=CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0

    CLuaPersonalSpacePlayerCapacityWnd.playerCapacity_detailData={}

    local url = CPersonalSpaceMgr.BASE_URL.."qnm/info/get_fighting_capacity"

    local form = CHTTPForm()
    local time = math.floor(CServerTimeMgr.Inst.timeStamp * 1000)
    local skey = CPersonalSpaceMgr.Inst.Token

    form:AddField("time", tostring(time))
    form:AddField("roleid", tostring(roleid))
    form:AddField("serverid", tostring(serverId))
    form:AddField("skey", skey)
    form:AddField("targetid", tostring(playerid))

    Main.Inst:StartCoroutine(CPersonalSpaceMgr.Inst:DoPost(url, form, DelegateFactory.Action_bool_string(function (success, json)
        if success then
            local dict = Json.Deserialize(json)
            local code = CommonDefs.DictGetValue_LuaCall(dict,"code")
            if code==0 then
                local msg=CommonDefs.DictGetValue_LuaCall(dict,"msg")
                if msg and msg~="" then
                    CLuaPersonalSpacePlayerCapacityWnd.playerCapacity_playerid = playerid
                    CLuaPersonalSpacePlayerCapacityWnd.playerCapacity_playername = playername

                    local array = g_LuaUtil:StrSplit(msg,";")
                    if array and #array>0 then
                        for i,v in ipairs(array) do
                            local array2 = g_LuaUtil:StrSplit(v,",")
                            if #array2==2 then
                                local key = array2[1]
                                local val = tonumber(array2[2])
                                if key == "0" then
                                    CLuaPersonalSpacePlayerCapacityWnd.playerCapacity_totalZhanli = val
                                else
                                    CLuaPersonalSpacePlayerCapacityWnd.playerCapacity_detailData[key] = val
                                end
                            end
                        end
                        CUIManager.ShowUI(CUIResources.PersonalSpacePlayerCapacityWnd)
                    end
                else
                    g_MessageMgr:ShowMessage("CANNOT_FIND_FIGHTING_CAPACITY")
                end
            end
        end
    end)))
end

function CLuaPersonalSpaceMgr:QueryPersonalSpaceBackGroundList()
    if not self.m_IsOpenNewBg then
        return
    end
    Gac2Gas.QueryPersonalSpaceBackGroundList()
end

function CLuaPersonalSpaceMgr:CancelUsePersonalSpaceBackGround()
    Gac2Gas.RequestUsePersonalSpaceBackGround(0)
end

local Application = import "UnityEngine.Application"
local RuntimePlatform = import "UnityEngine.RuntimePlatform"
local C3DTouchMgr = import "L10.UI.C3DTouchMgr"

function Gas2Gac.SetPersonalToken(token,bLogin,expiredTime)
    CPersonalSpaceMgr.Inst.Token = token
    CPersonalSpaceMgr.Inst.TokenExpiredTime = expiredTime

    if bLogin then
        CPersonalSpaceMgr.Inst:PushRoleInfo()
        CPersonalSpaceMgr.Inst:SyncFriend()
        CLuaPersonalSpaceMgr.PushPlayerExtraInfo()
    end
    if Application.platform == RuntimePlatform.IPhonePlayer and CommonDefs.IS_CN_CLIENT then
        if CDeepLinkMgr.Inst:HasInGameAction() and CDeepLinkMgr.Inst:HasOpenPersonalSpaceAction() then
            CDeepLinkMgr.Inst:DoInGameAction()
        else
            C3DTouchMgr.Inst:OpenPersonalSpaceWnd(CPersonalSpaceMgr.OpenPersonalSpace)
        end
    end
end

CLuaPersonalSpaceMgr.m_IsOpenNewBg = false
if CommonDefs.IS_KR_CLIENT or CommonDefs.IS_HMT_CLIENT or CommonDefs.IS_VN_CLIENT then
	CLuaPersonalSpaceMgr.m_IsOpenNewBg = false
end
CLuaPersonalSpaceMgr.m_PersonalSpaceBackGroundList = {}
CLuaPersonalSpaceMgr.m_CurrentBgId = 0
CLuaPersonalSpaceMgr.m_NewGetBgIds = nil
function Gas2Gac.SendPersonalSpaceBackGroundList(currentBgId, dataUD)
    local list = MsgPackImpl.unpack(dataUD)
    CLuaPersonalSpaceMgr.m_CurrentBgId = currentBgId
    CLuaPersonalSpaceMgr.m_PersonalSpaceBackGroundList = {}
    for i = 0, list.Count - 1, 2 do
        local bId = list[i]
        local expiredTime = list[i+1]
        local designData = PersonalSpace_Background.GetData(bId)  
        if designData then
            CLuaPersonalSpaceMgr.m_PersonalSpaceBackGroundList[bId] = {designData = designData, id = bId, expiredTime = expiredTime}
        end
    end
    if not CUIManager.IsLoaded(CLuaUIResources.ChoosePersonalSpaceNewBgWnd) then
        CUIManager.ShowUI(CLuaUIResources.ChoosePersonalSpaceNewBgWnd)
    end
    g_ScriptEvent:BroadcastInLua("OnLoadPersonalSpaceNewBgData",currentBgId)
end

function Gas2Gac.SendActivePersonalSpaceBackGround(bgIdArray)
    local list = MsgPackImpl.unpack(bgIdArray)
    CLuaPersonalSpaceMgr.m_NewGetBgIds = list
    CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.GetPersonalSpaceNewBg)
    CUIManager.ShowUI(CLuaUIResources.GetNewPersonalSpaceBgWnd)
end

function Gas2Gac.SendUsePersonalSpaceBackGround(bgId)
    CLuaPersonalSpaceMgr.m_CurrentBgId = bgId
    g_ScriptEvent:BroadcastInLua("OnLoadPersonalSpaceNewBgData",bgId)
end