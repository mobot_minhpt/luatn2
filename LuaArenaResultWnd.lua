local EnumChineseDigits            = import "L10.Game.EnumChineseDigits"
local ShareBoxMgr                  = import "L10.UI.ShareBoxMgr"
local EShareType                   = import "L10.UI.EShareType"
local CServerTimeMgr               = import "L10.Game.CServerTimeMgr"
local CLoginMgr                    = import "L10.Game.CLoginMgr"
local CPropertyAppearance          = import "L10.Game.CPropertyAppearance"
local EnumClass                    = import "L10.Game.EnumClass"
local EnumGender                   = import "L10.Game.EnumGender"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local Animation                    = import "UnityEngine.Animation"
local Profession                   = import "L10.Game.Profession"
local StringBuilder                = import "System.Text.StringBuilder"

LuaArenaResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaArenaResultWnd, "upgrade_TopLabel", "TopLabel", UILabel)
RegistChildComponent(LuaArenaResultWnd, "upgrade_OldDanLarge", "OldDanLarge", CUITexture)
RegistChildComponent(LuaArenaResultWnd, "upgrade_OldDanSmall", "OldDanSmall", CUITexture)
RegistChildComponent(LuaArenaResultWnd, "upgrade_NewDanLarge", "NewDanLarge", CUITexture)
RegistChildComponent(LuaArenaResultWnd, "upgrade_NewDanSmall", "NewDanSmall", CUITexture)
RegistChildComponent(LuaArenaResultWnd, "upgrade_Portrait", "Portrait", CUITexture)
RegistChildComponent(LuaArenaResultWnd, "upgrade_PlayerName", "PlayerName", UILabel)
RegistChildComponent(LuaArenaResultWnd, "playerInfo_Player", "Player", CUITexture)
RegistChildComponent(LuaArenaResultWnd, "playerInfo_Level", "Lv", UILabel)
RegistChildComponent(LuaArenaResultWnd, "playerInfo_Name", "Name", UILabel)
RegistChildComponent(LuaArenaResultWnd, "playerInfo_Server", "Server", UILabel)
RegistChildComponent(LuaArenaResultWnd, "playerInfo_Time", "Time", UILabel)
RegistChildComponent(LuaArenaResultWnd, "playerInfo", "PlayerInfo", GameObject)
RegistChildComponent(LuaArenaResultWnd, "bottomBtns", "BottomBtns", GameObject)
RegistChildComponent(LuaArenaResultWnd, "detailInfoButton", "DetailInfoButton", GameObject)
RegistChildComponent(LuaArenaResultWnd, "shareButton", "ShareButton", GameObject)
RegistChildComponent(LuaArenaResultWnd, "closeButton", "CloseButton", GameObject)
RegistChildComponent(LuaArenaResultWnd, "logo", "Logo", GameObject)
RegistChildComponent(LuaArenaResultWnd, "result_MatchType", "MatchType", UILabel)
RegistChildComponent(LuaArenaResultWnd, "result_AddRankScore", "AddRankScore", UILabel)
RegistChildComponent(LuaArenaResultWnd, "result_ShopScoreSlider", "ShopScoreSlider", UISlider)
RegistChildComponent(LuaArenaResultWnd, "result_ShopScoreNum", "ShopScoreNum", UILabel)
RegistChildComponent(LuaArenaResultWnd, "result_PassportExpSlider", "PassportExpSlider", UISlider)
RegistChildComponent(LuaArenaResultWnd, "result_PassportExpNum", "PassportExpNum", UILabel)
RegistChildComponent(LuaArenaResultWnd, "result_DanLarge", "DanLarge", CUITexture)
RegistChildComponent(LuaArenaResultWnd, "result_DanSmall", "DanSmall", CUITexture)
RegistChildComponent(LuaArenaResultWnd, "result_TaskName", "TaskName", UILabel)
RegistChildComponent(LuaArenaResultWnd, "result_TaskTarget", "TaskTarget", UILabel)
RegistChildComponent(LuaArenaResultWnd, "result_TaskGrid", "TaskGrid", Transform)
--@endregion RegistChildComponent end

RegistClassMember(LuaArenaResultWnd, "result_Left")
RegistClassMember(LuaArenaResultWnd, "result_TeamInfo")
RegistClassMember(LuaArenaResultWnd, "result_TaskInfoAnim")
RegistClassMember(LuaArenaResultWnd, "detailDataView")
RegistClassMember(LuaArenaResultWnd, "teamTables")
RegistClassMember(LuaArenaResultWnd, "identifiers")
RegistClassMember(LuaArenaResultWnd, "result_Root")
RegistClassMember(LuaArenaResultWnd, "upgrade_Root")
RegistClassMember(LuaArenaResultWnd, "result_DanLabel")
RegistClassMember(LuaArenaResultWnd, "result_SubRankScore")

RegistClassMember(LuaArenaResultWnd, "upgradeShowTick")
RegistClassMember(LuaArenaResultWnd, "taskShowTickTbl")
RegistClassMember(LuaArenaResultWnd, "starShowTickTbl")
RegistClassMember(LuaArenaResultWnd, "isDetailPage")

RegistClassMember(LuaArenaResultWnd, "playerId2FavorInfo")

function LuaArenaResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	UIEventListener.Get(self.detailInfoButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnDetailInfoButtonClick()
	end)

	UIEventListener.Get(self.shareButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareButtonClick()
	end)
	if CommonDefs.IS_VN_CLIENT then
		self.shareButton:SetActive(false)
	end

    --@endregion EventBind end
	self.result_Left = self.transform:Find("Result/Left")
	self.result_TeamInfo = self.transform:Find("Result/TeamInfo").gameObject
	self.result_TaskInfoAnim = self.transform:Find("Result/TaskInfo"):GetComponent(typeof(Animation))
	self.result_Root = self.transform:Find("Result").gameObject
	self.result_DanLabel = self.result_Left:Find("ScoreInfo/Dan/Label"):GetComponent(typeof(UILabel))
	self.upgrade_Root = self.transform:Find("Upgrade").gameObject
	self.detailDataView = self.result_Root.transform:Find("DetailDataView").gameObject
	self.playerInfo:SetActive(false)
	self.logo:SetActive(false)

	self.teamTables = {}
	for i = 1, 5 do
		local teamTable = self.transform:Find(SafeStringFormat3("Result/TeamInfo/TeamTable%d", i))
		if teamTable then
			self.teamTables[i] = teamTable
		end
	end

	self.result_SubRankScore = self.result_Left:Find("ScoreInfo/SubRankScore"):GetComponent(typeof(UILabel))
end

function LuaArenaResultWnd:Init()
	local info = LuaArenaMgr.playResultInfo
	local oldLargeDan, oldSmallDan = LuaArenaMgr:GetLargeAndSmallDan(info.newRankScore - info.changeRankScore)
	local newLargeDan, newSmallDan = LuaArenaMgr:GetLargeAndSmallDan(info.newRankScore)
	local isLargeDanChange = oldLargeDan ~= newLargeDan
	local isSmallDanChange = oldSmallDan ~= newSmallDan
	if info.changeRankScore > 0 and (isLargeDanChange or isSmallDanChange) then
		self.upgrade_Root:SetActive(true)
		self.result_Root:SetActive(false)

		local newDanData = Arena_DanInfo.GetData(newLargeDan)
		local danStr = SafeStringFormat3(LocalString.GetString("[%s]%s·%s段[-]"), newDanData.DanColor, newDanData.DanName, EnumChineseDigits.GetDigit(newSmallDan))
		self.upgrade_TopLabel.text = SafeStringFormat3(LocalString.GetString("恭喜你，已经晋升至%s段位"), danStr)
		self.upgrade_NewDanLarge:LoadMaterial(newDanData.Icon)
		self.upgrade_NewDanSmall:LoadMaterial(LuaArenaMgr.smallDanMatPaths[newSmallDan])
		self.upgrade_Portrait:LoadPortrait(CClientMainPlayer.Inst and CClientMainPlayer.Inst.PortraitName, false)
		self.upgrade_PlayerName.text = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Name or ""

		for i = 2, 5 do
			self.upgrade_NewDanLarge.transform:Find(i).gameObject:SetActive(i == newLargeDan)
		end

		local animation = self.upgrade_Root.transform:GetComponent(typeof(Animation))
		animation:Play(isLargeDanChange and "arenaresultwnd_upgrade_qiang" or "arenaresultwnd_upgrade_ruo")

		self:ClearUpgradeShowTick()
		self.upgradeShowTick = RegisterTickOnce(function()
			self:EnterResultWnd()
		end, 2000)
	else
		self:EnterResultWnd()
	end
	self:InitResultInfo()
	self.isDetailPage = false
	self:UpdateDetailInfoButton()
end

function LuaArenaResultWnd:EnterResultWnd()
	self.upgrade_Root:SetActive(false)
	self.result_Root:SetActive(true)
	self.result_TeamInfo:SetActive(true)
	self.detailDataView:SetActive(false)
	self:PlayResultAnimation()
	self:ShowTaskInfo()
end

function LuaArenaResultWnd:PlayResultAnimation()
	local animation = self.result_Root.transform:GetComponent(typeof(Animation))
	local info = LuaArenaMgr.playResultInfo
	animation:Play(info.winType == 0 and "arenaresultwnd_result_win" or "arenaresultwnd_result_lose")
end

function LuaArenaResultWnd:ShowTaskInfo()
	local taskData = LuaArenaMgr.playResultInfo.taskData
	self.result_TaskInfoAnim.gameObject:SetActive(false)
	self:ClearTaskShowTickTbl()
	for taskId, starInfo in pairs(taskData) do
		if not self.taskShowTickTbl then
			self.taskShowTickTbl = {}
			self:ShowATaskInfo(taskId)
		else
			table.insert(self.taskShowTickTbl, RegisterTickOnce(function()
				self:ShowATaskInfo(taskId)
			end, 1700))
		end
	end
end

function LuaArenaResultWnd:ShowATaskInfo(taskId)
	local starInfo = LuaArenaMgr.playResultInfo.taskData[taskId]
	self.result_TaskInfoAnim.gameObject:SetActive(false)
	local data = Arena_Task.GetData(taskId)
	local oldStar = starInfo.oldstar
	local newStar = starInfo.newstar
	self.result_TaskInfoAnim.gameObject:SetActive(true)
	self.result_TaskInfoAnim:Play()
	self.result_TaskName.text = data.Name

	local total = 0
	if newStar == 1 then
		total = data.Num1
	elseif newStar == 2 then
		total = data.Num2
	elseif newStar == 3 then
		total = data.Num3
	end
	local sb = NewStringBuilderWraper(StringBuilder, string.gsub(data.Desc, "%%s", tostring(total)))
	self.result_TaskTarget.text = sb.Length > 7 and SafeStringFormat3("%s...", sb:ToString(0, 7)) or sb:ToString()
	for i = 1, 3 do
		local child = self.result_TaskGrid.transform:GetChild(i - 1)
		local highlight = child:Find("Highlight")
		highlight.gameObject:SetActive(i <= oldStar)
		highlight:Find("CUIFx").gameObject:SetActive(false)
	end

	self:ClearStarShowTickTbl()
	if newStar > oldStar then
		self.starShowTickTbl = {}
		for i = oldStar + 1, newStar do
			table.insert(self.starShowTickTbl, RegisterTickOnce(function()
				local highlight = self.result_TaskGrid.transform:GetChild(i - 1):Find("Highlight")
				highlight.gameObject:SetActive(true)
				highlight:Find("CUIFx").gameObject:SetActive(true)
			end, 500 + (i - oldStar - 1) * 250))
		end
	end
end

function LuaArenaResultWnd:InitResultInfo()
	local info = LuaArenaMgr.playResultInfo
	self.result_MatchType.text = SafeStringFormat3(LocalString.GetString("竞技场·%s"), Arena_OpenClosePlay.GetData(info.matchType).PlayTypeName)

	local newLargeDan, newSmallDan = LuaArenaMgr:GetLargeAndSmallDan(info.newRankScore)
	local newDanData = Arena_DanInfo.GetData(newLargeDan)
	self.result_DanLarge:LoadMaterial(newDanData.Icon)
	self.result_DanSmall:LoadMaterial(LuaArenaMgr.smallDanMatPaths[newSmallDan])
	self.result_AddRankScore.text = info.changeRankScore > 0 and info.changeRankScore or ""
	self.result_SubRankScore.text = info.changeRankScore <= 0 and info.changeRankScore or ""
	self.result_DanLabel.text = SafeStringFormat3(LocalString.GetString("[%s]%s·%s段[-]"), newDanData.DanColor, newDanData.DanName, EnumChineseDigits.GetDigit(newSmallDan))

	local maxShopScore = Arena_Setting.GetData().WeeklyShopScoreReward
	-- self.result_ShopScoreSlider.value = math.min(1, info.totalShopScore / maxShopScore)
	self.result_ShopScoreSlider.gameObject:SetActive(false)
	self.result_ShopScoreNum.text = SafeStringFormat3("%d/%d", info.totalShopScore, maxShopScore)

	local expLimit = info.weekPassportExpLimit
	self.result_PassportExpSlider.value = math.min(1, expLimit > 0 and info.weekPassportExp / expLimit or 0)
	self.result_PassportExpNum.text = SafeStringFormat3("%d/%d", info.weekPassportExp, expLimit)
	
	self.playerId2FavorInfo = {}
	self:InitPlayerInfo()
	self:InitTeamInfo()
	self:InitDetailInfo()
end

function LuaArenaResultWnd:InitPlayerInfo()
	self.playerInfo_Player:LoadPortrait(CClientMainPlayer.Inst and CClientMainPlayer.Inst.PortraitName, false)
	self.playerInfo_Level.text = CUICommonDef.GetMainPlayerColoredLevelString(Color.white)
	self.playerInfo_Name.text = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Name or ""

	local dataTime = CServerTimeMgr.ConvertTimeStampToZone8Time(LuaArenaMgr.playResultInfo.playStartTime)
	self.playerInfo_Time.text = SafeStringFormat3("%d.%d.%d %d:%02d", dataTime.Year, dataTime.Month, dataTime.Day, dataTime.Hour, dataTime.Minute)
	local myGameServer = CLoginMgr.Inst:GetSelectedGameServer()
    self.playerInfo_Server.text = myGameServer and myGameServer.name or ""
end

function LuaArenaResultWnd:InitTeamInfo()
	for i = 1, 5 do
		if self.teamTables[i] then
			self.teamTables[i].gameObject:SetActive(false)
		end
	end

	self:DestroyModels()
	self.identifiers = {}
	local teamData = LuaArenaMgr.playResultInfo.teamInfo
	local count = #teamData
	local teamTable = self.teamTables[#teamData]
	if not teamTable then return end
	teamTable.gameObject:SetActive(true)

	local myId = CClientMainPlayer.Inst.Id
	for i, data in ipairs(teamData) do
		local child = teamTable:GetChild(i - 1)

		-- local titleData = Arena_PlayResultTitle.GetData(data.titleId)
		-- child:Find("Info/Title"):GetComponent(typeof(UILabel)).text = titleData.Name
		child:Find("Info/Name"):GetComponent(typeof(UILabel)).text = data.name

		local identifier = SafeStringFormat3("__%s__", tostring(child:GetInstanceID()))
		local appearance = CPropertyAppearance()
        appearance:LoadFromString(data.appearance, CommonDefs.ConvertIntToEnum(typeof(EnumClass), data.class), CommonDefs.ConvertIntToEnum(typeof(EnumGender), data.gender))
		local loader = LuaDefaultModelTextureLoader.Create(function (ro)
			CClientMainPlayer.LoadResource(ro, appearance, true, 1.0,
				0, false, 0, false, 0, false, nil, false, false)
		end)
		child:Find("ModelMask/Model"):GetComponent(typeof(UITexture)).mainTexture = CUIManager.CreateModelTexture(identifier, loader,
			180, 0.05, -1, 4.66, false, true, 1.0, true, false)

		table.insert(self.identifiers, identifier)

		local playerId = data.playerId
		local isMe = myId == playerId
		local likeBtn = child:Find("Info/LikeBtn").gameObject
		likeBtn:SetActive(not isMe)
		if not isMe then
			self:AddLikeGo(playerId, likeBtn)
			UIEventListener.Get(likeBtn).onClick = DelegateFactory.VoidDelegate(function (go)
				self:OnLikeButtonClick(playerId)
			end)
		end
	end
	if count == 1 then
		LuaUtils.SetLocalPositionX(self.result_Left, -300)
	elseif count == 2 then
		LuaUtils.SetLocalPositionX(self.result_Left, -450)
	else
		LuaUtils.SetLocalPositionX(self.result_Left, -610)
	end
end

function LuaArenaResultWnd:AddLikeGo(playerId, go)
	if not self.playerId2FavorInfo[playerId] then
		self.playerId2FavorInfo[playerId] = {
			gos = {},
			isFavored = false,
		}
	end
	go.transform:Find("n").gameObject:SetActive(true)
	go.transform:Find("s").gameObject:SetActive(false)
	table.insert(self.playerId2FavorInfo[playerId].gos, go)
end

function LuaArenaResultWnd:UpdateDetailInfoButton()
	local sprite = self.detailInfoButton.transform:GetComponent(typeof(UISprite))
	local label = self.detailInfoButton.transform:Find("Label"):GetComponent(typeof(UILabel))
    if self.isDetailPage then
        sprite.spriteName = "common_btn_01_yellow"
		label.text = LocalString.GetString("返回")
        label.color = NGUIText.ParseColor24("351B01",0)
    else
		sprite.spriteName = "common_btn_01_blue"
		label.text = LocalString.GetString("本场详情")
        label.color = NGUIText.ParseColor24("0E3254",0)
    end
end

function LuaArenaResultWnd:InitDetailInfo()
	local playerData = LuaArenaMgr.playResultInfo.playerData
	local force1Tbl = {}
	local force2Tbl = {}
	for i = 1, #playerData, 12 do
		local force = playerData[i + 11]
		table.insert(force == 0 and force1Tbl or force2Tbl, {
			playerId = playerData[i],
			name = playerData[i + 1],
			class = playerData[i + 2],
			playerWinType = playerData[i + 3], -- 0胜利 1失败 2平局
			kill = playerData[i + 4],
			die = playerData[i + 5],
			ctrl = playerData[i + 6],
			rmCtrl = playerData[i + 7],
			relive = playerData[i + 8],
			damage = playerData[i + 9],
			heal = playerData[i + 10]
		})
	end

	local matchType = LuaArenaMgr.playResultInfo.matchType
	local template = self.detailDataView.transform:Find("Template").gameObject
	template:SetActive(false)

	local force1 = self.detailDataView.transform:Find("Force1")
	local force2 = self.detailDataView.transform:Find("Force2")
	local force1Grid = force1:Find("Grid"):GetComponent(typeof(UIGrid))
	local force2Grid = force2:Find("Grid"):GetComponent(typeof(UIGrid))
	local force1Bg = force1:Find("Bg")
	local force2Bg = force2:Find("Bg")

	self:InitDetailGrid(force1Grid, template, force1Tbl)
	self:InitDetailGrid(force2Grid, template, force2Tbl)
	self:InitDetailBg(force1Bg, matchType)
	self:InitDetailBg(force2Bg, matchType)
end

function LuaArenaResultWnd:InitDetailGrid(grid, template, dataTbl)
	Extensions.RemoveAllChildren(grid.transform)
	local myId = CClientMainPlayer.Inst.Id
	for _, data in ipairs(dataTbl) do
		local child = NGUITools.AddChild(grid.gameObject, template)
		child:SetActive(true)
		local nameLabel = child.transform:Find("Name"):GetComponent(typeof(UILabel))
		nameLabel.text = data.name
		local killLabel = child.transform:Find("Kill"):GetComponent(typeof(UILabel))
		killLabel.text = data.kill
		local dieLabel = child.transform:Find("Die"):GetComponent(typeof(UILabel))
		dieLabel.text = data.die
		local ctrlLabel = child.transform:Find("Ctrl"):GetComponent(typeof(UILabel))
		ctrlLabel.text = math.floor(data.ctrl)
		local rmCtrlLabel = child.transform:Find("RmCtrl"):GetComponent(typeof(UILabel))
		rmCtrlLabel.text = math.floor(data.rmCtrl)
		local reliveLabel = child.transform:Find("Relive"):GetComponent(typeof(UILabel))
		reliveLabel.text = math.floor(data.relive)
		local damageLabel = child.transform:Find("Damage"):GetComponent(typeof(UILabel))
		damageLabel.text = math.floor(data.damage)
		local healLabel = child.transform:Find("Heal"):GetComponent(typeof(UILabel))
		healLabel.text = math.floor(data.heal)
		child.transform:Find("Class"):GetComponent(typeof(UISprite)).spriteName = Profession.GetIconByNumber(tonumber(data.class))

		local playerId = data.playerId
		local isMe = myId == playerId
		local labelTbl = {nameLabel, killLabel, dieLabel, ctrlLabel, rmCtrlLabel, reliveLabel, damageLabel, healLabel}
		for _, label in pairs(labelTbl) do
			label.color = isMe and NGUIText.ParseColor24("00FF00", 0) or Color.white
		end

		local likeBtn = child.transform:Find("LikeBtn").gameObject
		likeBtn:SetActive(not isMe)
		if not isMe then
			self:AddLikeGo(playerId, likeBtn)
			UIEventListener.Get(likeBtn).onClick = DelegateFactory.VoidDelegate(function (go)
				self:OnLikeButtonClick(playerId)
			end)
		end
	end
	grid:Reposition()
end

function LuaArenaResultWnd:InitDetailBg(bgRoot, matchType)
	for i = 1, 5 do
		bgRoot:GetChild(i - 1).gameObject:SetActive(i <= matchType)
	end
end

function LuaArenaResultWnd:DestroyModels()
	if self.identifiers and #self.identifiers > 0 then
		for _, identifier in pairs(self.identifiers) do
			CUIManager.DestroyModelTexture(identifier)
		end
	end
end

function LuaArenaResultWnd:ClearStarShowTickTbl()
	if not self.starShowTickTbl then return end

	for _, tick in pairs(self.starShowTickTbl) do
		UnRegisterTick(tick)
	end
	self.starShowTickTbl = nil
end

function LuaArenaResultWnd:ClearTaskShowTickTbl()
	if not self.taskShowTickTbl then return end

	for _, tick in pairs(self.taskShowTickTbl) do
		UnRegisterTick(tick)
	end
	self.taskShowTickTbl = nil
end

function LuaArenaResultWnd:ClearUpgradeShowTick()
	if self.upgradeShowTick then
		UnRegisterTick(self.upgradeShowTick)
		self.upgradeShowTick = nil
	end
end

function LuaArenaResultWnd:OnDestroy()
	self:DestroyModels()
	self:ClearTaskShowTickTbl()
	self:ClearStarShowTickTbl()
	self:ClearUpgradeShowTick()
end

--@region UIEvent

function LuaArenaResultWnd:OnDetailInfoButtonClick()
	self.isDetailPage = not self.isDetailPage
	self:UpdateDetailInfoButton()
	self.detailDataView:SetActive(self.isDetailPage)
	self.result_TeamInfo:SetActive(not self.isDetailPage)
	if not self.isDetailPage then
		self.result_TeamInfo.transform:GetComponent(typeof(Animation)):Play()
	end
end

function LuaArenaResultWnd:OnShareButtonClick()
	self.playerInfo:SetActive(true)
	self.bottomBtns:SetActive(false)
	self.closeButton:SetActive(false)
	self.logo:SetActive(true)

    CUIManager.SetUITop(CLuaUIResources.ArenaResultWnd)
    CUICommonDef.CaptureScreen("screenshot", false, false, nil,
        DelegateFactory.Action_string_bytes(function(fullPath, jpgBytes)
			self.playerInfo:SetActive(false)
			self.bottomBtns:SetActive(true)
			self.closeButton:SetActive(true)
			self.logo:SetActive(false)

            CUIManager.ResetUITop(CLuaUIResources.ArenaResultWnd)
            ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, fullPath)
        end), false)
end

function LuaArenaResultWnd:OnLikeButtonClick(playerId)
	local favorInfo = self.playerId2FavorInfo[playerId]
	if not favorInfo then
		return
	end

	if favorInfo.isFavored then
		g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("本场已经给这位玩家点过赞了"))
		return
	end

	for _, go in pairs(favorInfo.gos) do
		go.transform:Find("n").gameObject:SetActive(false)
		go.transform:Find("s").gameObject:SetActive(true)
	end

	Gac2Gas.ArenaAddFavToTarget(playerId)
	favorInfo.isFavored = true
end

--@endregion UIEvent
