local CUIFx = import "L10.UI.CUIFx"

local UIInput = import "UIInput"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local BoxCollider = import "UnityEngine.BoxCollider"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local PopupMenuItemData=import "L10.UI.PopupMenuItemData"
local CPopupMenuInfoMgr=import "L10.UI.CPopupMenuInfoMgr"
local AlignType2=import "L10.UI.CPopupMenuInfoMgr+AlignType"
local CUICommonDef = import "L10.UI.CUICommonDef"
local EChatPanel = import "L10.Game.EChatPanel"
local CChatHelper = import "L10.UI.CChatHelper"
local CSocialWndMgr = import "L10.UI.CSocialWndMgr"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local CCarnivalLotteryMgr = import "L10.Game.CCarnivalLotteryMgr"

LuaWNXYGiveWishWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaWNXYGiveWishWnd, "Portrait", "Portrait", CUITexture)
RegistChildComponent(LuaWNXYGiveWishWnd, "PlayerName", "PlayerName", UILabel)
RegistChildComponent(LuaWNXYGiveWishWnd, "Gift", "Gift", GameObject)
RegistChildComponent(LuaWNXYGiveWishWnd, "GiftIcon", "GiftIcon", CUITexture)
RegistChildComponent(LuaWNXYGiveWishWnd, "AddNode", "AddNode", GameObject)
RegistChildComponent(LuaWNXYGiveWishWnd, "GiftCount", "GiftCount", UILabel)
RegistChildComponent(LuaWNXYGiveWishWnd, "WishBtn", "WishBtn", GameObject)
RegistChildComponent(LuaWNXYGiveWishWnd, "WishBtnLabel", "WishBtnLabel", UILabel)
RegistChildComponent(LuaWNXYGiveWishWnd, "MakeWishView", "MakeWishView", GameObject)
RegistChildComponent(LuaWNXYGiveWishWnd, "WishDetaiView", "WishDetaiView", GameObject)
RegistChildComponent(LuaWNXYGiveWishWnd, "Input", "Input", UIInput)
RegistChildComponent(LuaWNXYGiveWishWnd, "AllWishBtn", "AllWishBtn", GameObject)
RegistChildComponent(LuaWNXYGiveWishWnd, "ShareBtn", "ShareBtn", GameObject)
RegistChildComponent(LuaWNXYGiveWishWnd, "WishContent", "WishContent", UILabel)
RegistChildComponent(LuaWNXYGiveWishWnd, "SucceedView", "SucceedView", GameObject)
RegistChildComponent(LuaWNXYGiveWishWnd, "CanRewardFx", "CanRewardFx", CUIFx)

--@endregion RegistChildComponent end
RegistClassMember(LuaWNXYGiveWishWnd,"m_WndType")
RegistClassMember(LuaWNXYGiveWishWnd,"m_GiftQnBtn")
RegistClassMember(LuaWNXYGiveWishWnd,"m_GiftCollider")
RegistClassMember(LuaWNXYGiveWishWnd,"m_GiveGift")
RegistClassMember(LuaWNXYGiveWishWnd,"m_HasRewarded")
function LuaWNXYGiveWishWnd:Awake()
    self.m_GiftCollider = self.Gift.transform:GetComponent(typeof(BoxCollider))
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.Gift.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnGiftClick()
	end)


	
	UIEventListener.Get(self.WishBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnWishBtnClick()
	end)


	
	UIEventListener.Get(self.AllWishBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnAllWishBtnClick()
	end)


	
	UIEventListener.Get(self.ShareBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareBtnClick(go)
	end)


    --@endregion EventBind end
end

function LuaWNXYGiveWishWnd:OnEnable()
    g_ScriptEvent:AddListener("RefreshGiveCardGift",self,"OnRefreshGiveCardGift")
    g_ScriptEvent:AddListener("GiveWishSucceed",self,"OnGiveWishSucceed")
    g_ScriptEvent:AddListener("GetChrismas2021CardGiftSuccess",self,"OnGetChrismas2021CardGiftSuccess")
end

function LuaWNXYGiveWishWnd:OnDisable()
    g_ScriptEvent:RemoveListener("RefreshGiveCardGift",self,"OnRefreshGiveCardGift")
    g_ScriptEvent:RemoveListener("GiveWishSucceed",self,"OnGiveWishSucceed")
    g_ScriptEvent:RemoveListener("GetChrismas2021CardGiftSuccess",self,"OnGetChrismas2021CardGiftSuccess")
end

function LuaWNXYGiveWishWnd:Init()
    self.WishDetaiView:SetActive(false)
    self.MakeWishView:SetActive(false)
    self.SucceedView:SetActive(false)

    self.m_WndType = LuaChristmas2021Mgr.SingleWishWndType

    if self.m_WndType == LuaChristmas2021Mgr.WishType.eDetailWish then----查看他人的祈愿
        self.WishDetaiView:SetActive(true)
        --self:InitWishDetailView()
        local info = LuaChristmas2021Mgr.m_CurShowingCardInfo
        if info then
            local sendPlayerIdMap = info.SendPlayerId
            self.m_HasRewarded = false
            if CClientMainPlayer.Inst then
                local myId = CClientMainPlayer.Inst.Id
                if sendPlayerIdMap and CommonDefs.IsDic(sendPlayerIdMap) and CommonDefs.DictTryGet(sendPlayerIdMap,typeof(String),tostring(myId),typeof(UInt64)) then
                    self.m_HasRewarded = true
                end
            end
            
            if not LuaChristmas2021Mgr.PlayerId2MengDaoInfo[info.PlayerId] then
                CPersonalSpaceMgr.Inst:GetUserProfile(info.PlayerId, DelegateFactory.Action_CPersonalSpace_UserProfile_Ret(function (ret)
                    if ret.code ~= 0 or CClientMainPlayer.Inst == nil then
                        return
                    end
                    local playerInfo = {}
                    playerInfo.RoleName = ret.data.rolename
                    playerInfo.Photo = ret.data.photo
                    playerInfo.Cls = ret.data.clazz
                    playerInfo.Gender = ret.data.gender
                    LuaChristmas2021Mgr.PlayerId2MengDaoInfo[info.PlayerId] = playerInfo
                    self:InitWishDetailView()
                end), nil)
            else
                self:InitWishDetailView()
            end
        end
        

    elseif self.m_WndType == LuaChristmas2021Mgr.WishType.eeWishSucceed then --许愿成功后的界面
        self:InitPlayerInfo()
        self.SucceedView:SetActive(true)
        self:InitSucceedView()
    else                                                                  --正在祈愿
        self:InitPlayerInfo()
        self.MakeWishView:SetActive(true)
        self:InitMakeWishView()
    end
    
    --海外版本屏蔽分享
    local needShare = not CommonDefs.IS_VN_CLIENT and not CommonDefs.IS_HMT_CLIENT
    self.ShareBtn:SetActive(needShare)
end

function LuaWNXYGiveWishWnd:InitPlayerInfo()
    local player = CClientMainPlayer.Inst
    if not player then return end
    self.PlayerName.text = player.RealName

    if CommonDefs.IS_CN_CLIENT then
        self.Portrait:LoadNPCPortrait(player.PortraitName,false)
    else
        local path = SafeStringFormat3("UI/Texture/Portrait/Material/lnpc606_0%d.mat",(player.Id%3 + 1))
        self.Portrait:LoadMaterial(path)
    end
end

--正在祈愿
function LuaWNXYGiveWishWnd:InitMakeWishView()
    self.m_GiftCollider.enabled = true
    --init gift
    self:OnRefreshGiveCardGift(nil)
    --init count
    local madeCount = CCarnivalLotteryMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eChristmas2021CardPutTimes)
    local limitCount = Christmas2021_Setting.GetData().MaxCardPerDay
    self.WishBtnLabel.text = SafeStringFormat3(LocalString.GetString("发布祈愿%d/%d"),madeCount,limitCount)
end

--查看他人发布的祈愿
function LuaWNXYGiveWishWnd:InitWishDetailView()

    self.AddNode:SetActive(false)
    self.GiftCount.text = nil

    local info = LuaChristmas2021Mgr.m_CurShowingCardInfo
    local playerInfo = LuaChristmas2021Mgr.PlayerId2MengDaoInfo[info.PlayerId]

    if not info then return end
    if not playerInfo then return end

    self.PlayerName.text = info.PlayerName
    self.Portrait.material = nil
	if playerInfo.Photo then
		CPersonalSpaceMgr.Inst:DownLoadPortraitPic(playerInfo.Photo,self.Portrait.transform:GetComponent(typeof(UITexture)), nil)
	elseif playerInfo.Cls and playerInfo.Gender then
		self.Portrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(playerInfo.Cls, playerInfo.Gender, -1), false)
    elseif playerInfo.path then
        self.Portrait:LoadMaterial(playerInfo.path)
    else
        local path = SafeStringFormat3("UI/Texture/Portrait/Material/lnpc606_0%d.mat",(info.PlayerId%3 + 1))
		self.Portrait:LoadMaterial(path)
		playerInfo.path = path
	end

	local hasGift = false
    local giftIndex
    local giftCount = 0
    
	if info.Gifts and info.Gifts.Count >= 2 then 
		for i=0,info.Gifts.Count-1,1 do 
			if info.Gifts[i] > 0 then
				hasGift = true
                giftIndex = i 
                giftCount = info.Gifts[i]
				break
			end
		end
	end

    local restCount = giftCount
    local restCountLabel = self.WishDetaiView.transform:Find("RestCountLabel"):GetComponent(typeof(UILabel))
    restCountLabel.gameObject:SetActive(hasGift)
    self.CanRewardFx:DestroyFx()
    if hasGift then
        if info.Sended and info.Sended.Count >= 2 then
            restCount = giftCount - info.Sended[giftIndex]
        end
        self.Gift:SetActive(true)
        local giftId = Christmas2021_Setting.GetData().CardGift[giftIndex*3]
        if giftId then
            self.m_WantGetGiftItemId = giftId
            local data = Item_Item.GetData(giftId)
            if data then
                self.GiftIcon:LoadMaterial(data.Icon)
            end
        end
        if restCount > 0 then
            restCountLabel.text = SafeStringFormat3("%d/%d",restCount,giftCount)
            if info.PlayerId ~= CClientMainPlayer.Inst.Id then
                if not self.m_HasRewarded then
                    self.CanRewardFx:LoadFx("Fx/UI/Prefab/UI_meirichoujiang_liuguang.prefab")
                else
                    restCountLabel.text = LocalString.GetString("已领取")
                end
            end
        else
            restCountLabel.text = LocalString.GetString("已抢光")
        end
    else
        self.Gift:SetActive(false)
    end

    self.WishDetaiView.transform:Find("WishContent"):GetComponent(typeof(UILabel)).text = info.Msg
end

--许愿成功后的界面
function LuaWNXYGiveWishWnd:InitSucceedView()
    self.m_GiftCollider.enabled = false
    if not self.m_GiveGift then
        self.Gift:SetActive(false)
    end
end

function LuaWNXYGiveWishWnd:OnRefreshGiveCardGift(gift)
    self.m_GiveGift = gift
    if not gift then
        self.GiftIcon.material = nil
        self.AddNode:SetActive(true)
        self.GiftCount.text = nil
    else
        self.AddNode:SetActive(false)
        local itemId = gift.ItemId
        local count = gift.Count
        local item = Item_Item.GetData(itemId)
        self.GiftIcon:LoadMaterial(item.Icon)
        self.GiftCount.text = count
    end
end

function LuaWNXYGiveWishWnd:OnGiveWishSucceed(guildId,cardId)
    self.WishDetaiView:SetActive(false)
    self.MakeWishView:SetActive(false)
    self.SucceedView:SetActive(true)
    LuaChristmas2021Mgr.m_ShareGuildId = guildId
    LuaChristmas2021Mgr.m_ShareCardId = cardId
    self:InitSucceedView()
    self.WishContent.text = self.Input.value
end

function LuaWNXYGiveWishWnd:OnGetChrismas2021CardGiftSuccess(cardId, giftItemTempId)
    self.m_HasRewarded = true
    self:InitWishDetailView()
end
--@region UIEvent

function LuaWNXYGiveWishWnd:OnGiftClick()
    if self.m_WndType ~= LuaChristmas2021Mgr.WishType.eDetailWish and self.m_WndType ~= LuaChristmas2021Mgr.WishType.eeWishSucceed then
        LuaChristmas2021Mgr:OpenAddGiftWnd()
        return
    end

    if self.m_WndType == LuaChristmas2021Mgr.WishType.eDetailWish then------查看领取他人的祈愿
        --不能领取自己的礼物
        local myPlayerId = CClientMainPlayer.Inst.Id
        local cardPlayerId = LuaChristmas2021Mgr.m_CurShowingCardInfo.PlayerId
        if myPlayerId == cardPlayerId then
            g_MessageMgr:ShowMessage("Christmas2021_WNXY_CantGet_SelfGift")
            return
        end
        --只能领取一次
        if self.m_HasRewarded then
            return
        end

        local cardId = LuaChristmas2021Mgr.m_CurShowingCardInfo.Id 
        Gac2Gas.RequestGetCardGift(cardId,self.m_WantGetGiftItemId)
        --CUIManager.CloseUI(CLuaUIResources.WNXYGiveWishWnd)
    end
end

function LuaWNXYGiveWishWnd:OnWishBtnClick()
    local wishContent = self.Input.value
    local gift = self.m_GiveGift--[ItemId,Count]
    
    if System.String.IsNullOrEmpty(wishContent) then
        g_MessageMgr:ShowMessage("Christmas2021_WNXY_Wish_Empty")
        return
    end

    local giftItemTempId = not gift and 0 or gift.ItemId
    local giftNum = not gift and 0 or gift.Count

    --文字内容要审核
    local ret = CWordFilterMgr.Inst:DoFilterOnSend(wishContent, nil, nil, false)
    if ret.msg and not ret.shouldBeIgnore then
        if giftItemTempId and giftItemTempId ~= 0 then
            local moneyType = gift.CostType ==1 and LocalString.GetString("银两") or LocalString.GetString("灵玉")
            local money = gift.CostType ==1 and gift.YinLiang or gift.LingYu
            money = money * giftNum
            local giftName = Item_Item.GetData(giftItemTempId).Name
            MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("PutChristmas2021Card_WithGift_Confirm",money,moneyType,giftName), 
                DelegateFactory.Action(function ()
                    Gac2Gas.RequestPutChristmasCard(wishContent,giftItemTempId,giftNum)
                end),
                nil,
                LocalString.GetString("确定"), LocalString.GetString("取消"), false)
        else
            Gac2Gas.RequestPutChristmasCard(wishContent,giftItemTempId,giftNum)
        end
    else
        g_MessageMgr:ShowMessage("ChristmasWish_Violation")
        return
    end
end

function LuaWNXYGiveWishWnd:OnAllWishBtnClick()
    if CUIManager.IsLoaded(CLuaUIResources.WNXYGuildWishesWnd) then
		CUIManager.CloseUI(CLuaUIResources.WNXYGuildWishesWnd)
	end
    LuaChristmas2021Mgr.OpenGuildWishesWnd()
    CUIManager.CloseUI(CLuaUIResources.WNXYGiveWishWnd)
end

function LuaWNXYGiveWishWnd:OnShareBtnClick(go)
    local function SelectAction(index)
        if index==0 then--梦岛
            CUICommonDef.CaptureScreenAndShare()     
        elseif index==1 then--帮会
            self:ShareXiangshiQuestion(EChatPanel.Guild)
        end       
    end

    local selectShareItem=DelegateFactory.Action_int(SelectAction)

    local t={}
    local item=PopupMenuItemData(LocalString.GetString("分享至梦岛"),selectShareItem,false,nil)
    table.insert(t, item)
    local item=PopupMenuItemData(LocalString.GetString("分享至帮会"),selectShareItem,false,nil)
    table.insert(t, item)

    local array=Table2Array(t,MakeArrayClass(PopupMenuItemData))
    CPopupMenuInfoMgr.ShowPopupMenu(array, go.transform, AlignType2.Top,1,nil,600,true,266)
end

--@endregion UIEvent
function LuaWNXYGiveWishWnd:ShareXiangshiQuestion(channel)
    local guildid = LuaChristmas2021Mgr.m_ShareGuildId
    local cardid = LuaChristmas2021Mgr.m_ShareCardId
    local link
    if self.m_GiveGift then
        -- 挂礼物
        link = g_MessageMgr:FormatMessage("Christmas_Share_To_Guild_Link", guildid,cardid)
    else
        -- 没挂礼物
        link = g_MessageMgr:FormatMessage("Christmas_Share_To_Guild_Link_NoGift", guildid,cardid)
    end
    CChatHelper.SendMsgWithFilterOption(channel,link,0, true)
    CSocialWndMgr.ShowChatWnd(EChatPanel.Guild)
end
