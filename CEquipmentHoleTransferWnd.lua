-- Auto Generated!!
local CEquipmentHoleTransferWnd = import "L10.UI.CEquipmentHoleTransferWnd"
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CProfessionTransferMgr = import "L10.Game.CProfessionTransferMgr"
local DelegateFactory = import "DelegateFactory"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CEquipmentHoleTransferWnd.m_Init_CS2LuaHook = function (this) 

    this.sourceItemId = nil
    this.targetItemId = nil
    this:LoadItems()
    --TODO 元宝消耗
    this.moneyCtrl:SetCost(0)
end
CEquipmentHoleTransferWnd.m_LoadItems_CS2LuaHook = function (this) 

    local sourceExist = (CItemMgr.Inst:GetById(this.sourceItemId) ~= nil)
    local targetExist = (CItemMgr.Inst:GetById(this.targetItemId) ~= nil)
    if not sourceExist then
        this.selectedIndex = 0
    elseif not targetExist then
        this.selectedIndex = 1
    end
    this.sourceItem:Init(this.sourceItemId, this.selectedIndex == 0)
    this.targetItem:Init(this.targetItemId, this.selectedIndex == 1)

    this.transferBtn.Enabled = sourceExist and targetExist
end
CEquipmentHoleTransferWnd.m_Start_CS2LuaHook = function (this) 

    UIEventListener.Get(this.transferBtn.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.transferBtn.gameObject).onClick, MakeDelegateFromCSFunction(this.OnTransferButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.infoBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.infoBtn).onClick, MakeDelegateFromCSFunction(this.OnInfoButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.closeBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeBtn).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), true)
    this.sourceItem.OnClickDelegate = MakeDelegateFromCSFunction(this.OnSourceItemClick, Action0, this)
    this.targetItem.OnClickDelegate = MakeDelegateFromCSFunction(this.OnTargetItemClick, Action0, this)
end
CEquipmentHoleTransferWnd.m_OnSourceItemClick_CS2LuaHook = function (this) 
    this.selectedIndex = 0
    this.sourceItem.Selected = true
    this.targetItem.Selected = false
    CProfessionTransferMgr.Inst:ShowSourceEquipmentOptionsWnd()
end
CEquipmentHoleTransferWnd.m_OnTargetItemClick_CS2LuaHook = function (this) 
    if CItemMgr.Inst:GetById(this.sourceItemId) ~= nil then
        this.selectedIndex = 1
        this.sourceItem.Selected = false
        this.targetItem.Selected = true
        CProfessionTransferMgr.Inst:ShowTargetEquipmentOptionsWnd(this.sourceItemId)
    end
end
CEquipmentHoleTransferWnd.m_OnTransferButtonClick_CS2LuaHook = function (this, go) 
    local sourceEquip = CItemMgr.Inst:GetById(this.sourceItemId)
    local targetEquip = CItemMgr.Inst:GetById(this.targetItemId)
    if sourceEquip == nil or not sourceEquip.IsEquip or targetEquip == nil or not targetEquip.IsEquip then
        return
    end
    local pos1 = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, sourceEquip.Id)
    local pos2 = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, targetEquip.Id)
    local message = g_MessageMgr:FormatMessage("EquipHolesTransfer_CONFIRM", sourceEquip.ColoredName, sourceEquip.Equip.Hole, targetEquip.ColoredName, targetEquip.Equip.Hole)
    MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function () 
        CProfessionTransferMgr.Inst:ConfirmTransferEquipHoleTarget(pos1, pos2)
        this:Close()
    end), nil, LocalString.GetString("转移"), nil, false)
end
CEquipmentHoleTransferWnd.m_OnProfessionTransferEquipmentSelected_CS2LuaHook = function (this, isSource, itemId, costYuanBao) 
    if isSource then
        if this.sourceItemId ~= itemId then
            this.sourceItemId = itemId
            this.targetItemId = nil
            this.moneyCtrl:SetCost(0)
            --目前服务器端在target返回时带上元宝消耗
        end
    else
        if this.targetItemId ~= itemId then
            this.targetItemId = itemId
        end
        this.moneyCtrl:SetCost(costYuanBao)
        --目前服务器端在target返回时带上元宝消耗
    end
    this:LoadItems()
end
