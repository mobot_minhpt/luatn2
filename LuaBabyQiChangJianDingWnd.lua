require("common/common_include")

local CBabyModelTextureLoader = import "L10.UI.CBabyModelTextureLoader"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local Baby_Setting = import "L10.Game.Baby_Setting"
local CUIFx = import "L10.UI.CUIFx"
--local Baby_QiChang = import "L10.Game.Baby_QiChang"
local CUITexture = import "L10.UI.CUITexture"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local UIWidget = import "UIWidget"
local Vector4 = import "UnityEngine.Vector4"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CButton = import "L10.UI.CButton"
local BabyMgr = import "L10.Game.BabyMgr"
local QnCheckBox = import "L10.UI.QnCheckBox"
local CItemMgr = import "L10.Game.CItemMgr"
local PlayerPrefs = import "UnityEngine.PlayerPrefs"
local Item_Item = import "L10.Game.Item_Item"
local CItemAccessTipMgr = import "L10.UI.CItemAccessTipMgr"
local CTooltipAlignType = import "L10.UI.CTooltip+AlignType"
local CItemAccessListMgr=import "L10.UI.CItemAccessListMgr"

LuaBabyQiChangJianDingWnd = class()

RegistChildComponent(LuaBabyQiChangJianDingWnd, "MoreInfo", GameObject)
RegistChildComponent(LuaBabyQiChangJianDingWnd, "BabyTexture", CBabyModelTextureLoader)
RegistChildComponent(LuaBabyQiChangJianDingWnd, "BGFX", CUIFx)
RegistChildComponent(LuaBabyQiChangJianDingWnd, "JianDingFX", CUIFx)
RegistChildComponent(LuaBabyQiChangJianDingWnd, "JuLongFX", CUIFx)
RegistChildComponent(LuaBabyQiChangJianDingWnd, "QiChangLabel", UILabel)
RegistChildComponent(LuaBabyQiChangJianDingWnd, "QiChangBG", CUITexture)
RegistChildComponent(LuaBabyQiChangJianDingWnd, "QiChangFX", CUIFx)
RegistChildComponent(LuaBabyQiChangJianDingWnd, "QiChangShowFX", CUIFx)
RegistChildComponent(LuaBabyQiChangJianDingWnd, "QiChangShowVect", UIWidget)
RegistChildComponent(LuaBabyQiChangJianDingWnd, "IsNew", GameObject)
RegistChildComponent(LuaBabyQiChangJianDingWnd, "BabyNameLabel", UILabel)
RegistChildComponent(LuaBabyQiChangJianDingWnd, "BabyLevelLabel", UILabel)
RegistChildComponent(LuaBabyQiChangJianDingWnd, "AllBtn", GameObject)
RegistChildComponent(LuaBabyQiChangJianDingWnd, "RewardAlert", GameObject)
RegistChildComponent(LuaBabyQiChangJianDingWnd, "FxBtn", GameObject)
RegistChildComponent(LuaBabyQiChangJianDingWnd, "JianDingBtn", CButton)
RegistChildComponent(LuaBabyQiChangJianDingWnd, "QnCostAndOwnMoney", CCurentMoneyCtrl)
RegistChildComponent(LuaBabyQiChangJianDingWnd, "NeedItem", GameObject)
RegistChildComponent(LuaBabyQiChangJianDingWnd, "QnCheckBox", QnCheckBox)

RegistClassMember(LuaBabyQiChangJianDingWnd, "SelectedBaby")
RegistClassMember(LuaBabyQiChangJianDingWnd, "JianDingTick")
RegistClassMember(LuaBabyQiChangJianDingWnd, "JianDingInterval")
RegistClassMember(LuaBabyQiChangJianDingWnd, "ButtonEnableTick")
RegistClassMember(LuaBabyQiChangJianDingWnd, "LastEvaluateTime")
-- 用来显示是否是新的气场
RegistClassMember(LuaBabyQiChangJianDingWnd, "IsNewQiChang")
RegistClassMember(LuaBabyQiChangJianDingWnd, "SavedQiChang")

RegistClassMember(LuaBabyQiChangJianDingWnd, "Key_BabyQiChangAutoUseMoney")

-- 2023
RegistClassMember(LuaBabyQiChangJianDingWnd, "QiChangAlert")

function LuaBabyQiChangJianDingWnd:Init()
	self:InitClassMembers()
	self:InitValues()

end

function LuaBabyQiChangJianDingWnd:InitClassMembers()
	self.IsNew:SetActive(false)
	self.IsNewQiChang = false
	self.SavedQiChang = CreateFromClass(MakeGenericClass(Dictionary, UInt16, UInt16))
	self:DestroyJianDingTick()
	self:DestroyButtonEnableTick()
	
	self.QiChangAlert = self.QiChangLabel.gameObject.transform:Find("QiChangAlert")
end

function LuaBabyQiChangJianDingWnd:InitValues()

	if not LuaBabyMgr.m_ChosenBaby then
		CUIManager.CloseUI(CLuaUIResources.BabyQiChangJianDingWnd)
	end
	self.Key_BabyQiChangAutoUseMoney = "Key_BabyQiChangAutoUseMoney"
	self.SelectedBaby = LuaBabyMgr.m_ChosenBaby
	self.JianDingInterval = 3
	self.LastEvaluateTime = 0

	self.BabyTexture:Init(self.SelectedBaby, -180, false, 0)
	self.BabyNameLabel.text = self.SelectedBaby.Name
	self.BabyLevelLabel.text = SafeStringFormat3(LocalString.GetString("%s级"), tostring(self.SelectedBaby.Grade))

	local setting = Baby_Setting.GetData()
	self.QnCostAndOwnMoney:SetType(1, 0, false)
	self.QnCostAndOwnMoney:SetCost(setting.ActiveQiChangCost)

	local currentQiChangId = self.SelectedBaby.Props.QiChangData.CurrentQiChangId
	self:UpdateQiChangInfo(currentQiChangId)

	self.BGFX:DestroyFx()
	self.BGFX:LoadFx(CUIFxPaths.BabyQiChangBG)

	local onMoreInfoClicked = function (go)
		self:OnMoreInfoClicked(go)
	end
	CommonDefs.AddOnClickListener(self.MoreInfo, DelegateFactory.Action_GameObject(onMoreInfoClicked), false)

	local onAllBtnClicked = function (go)
		self:OnAllBtnClicked(go)
	end
	CommonDefs.AddOnClickListener(self.AllBtn, DelegateFactory.Action_GameObject(onAllBtnClicked), false)

	local onFxBtnClicked = function (go)
		self:OnFxBtnClicked(go)
	end
	CommonDefs.AddOnClickListener(self.FxBtn, DelegateFactory.Action_GameObject(onFxBtnClicked), false)

	local onJianDingBtn = function (go)
		self:OnJianDingBtnClicked(go)
	end
	CommonDefs.AddOnClickListener(self.JianDingBtn.gameObject, DelegateFactory.Action_GameObject(onJianDingBtn), false)

	CommonDefs.AddOnClickListener(self.QiChangBG.gameObject, DelegateFactory.Action_GameObject(function (go)
		CUIManager.ShowUI(CLuaUIResources.BabyQiChangShuWnd)
	end), false)

	local isSelected = self:AutoUseMoney()
	self.QnCheckBox:SetSelected(isSelected, false)
	self.QnCheckBox.OnValueChanged = DelegateFactory.Action_bool(function (value)
		self:OnQnCheckBoxChanged(value)
	end)
	self:UpdateJianDingCost()
	self:UpdateQiChangRewardAlert()

	if self.QiChangAlert then
		local hasNewQiChang = self:CheckBabyHasNewQiChang()
		self.QiChangAlert.gameObject:SetActive(hasNewQiChang)
	end
end

function LuaBabyQiChangJianDingWnd:CheckBabyHasNewQiChang()
	local activedQiChang = self.SelectedBaby.Props.QiChangData.ActivedQiChang
	local newQiChangs = {}

	Baby_QiChang.ForeachKey(function (key)
		if not CommonDefs.DictContains_LuaCall(activedQiChang, key) and #newQiChangs <= 0 then
			local canBeCheck = LuaBabyMgr.IsQiChangUnlocked(key, self.SelectedBaby)
			if canBeCheck then
				table.insert(newQiChangs, key)
			end
		end
	end)

	return #newQiChangs > 0
end

-- 更新鉴定花费
function LuaBabyQiChangJianDingWnd:UpdateJianDingCost()
	local totalCount = self:GetNeedItemCount()
	if totalCount > 0 then
		self.QnCostAndOwnMoney.gameObject:SetActive(false)
		self.NeedItem:SetActive(true)
	else
		local isSelected = self:AutoUseMoney()
		if isSelected then
			self.QnCostAndOwnMoney.gameObject:SetActive(true)
			self.NeedItem:SetActive(false)
		else
			self.QnCostAndOwnMoney.gameObject:SetActive(false)
			self.NeedItem:SetActive(true)
		end
	end
	self:UpdateNeedItem()
end

function LuaBabyQiChangJianDingWnd:UpdateNeedItem()
	local IconTexture = self.NeedItem.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
	local DisableSprite = self.NeedItem.transform:Find("DisableSprite").gameObject
	local GetHint = self.NeedItem.transform:Find("GetHint").gameObject
	local CountLabel = self.NeedItem.transform:Find("CountLabel"):GetComponent(typeof(UILabel))

	local setting = Baby_Setting.GetData()
	local itemId = setting.BabyQiChangJianDingItem
	local item = Item_Item.GetData(itemId)
	local count = 1
	if item then
		IconTexture:LoadMaterial(item.Icon)
		local totalCount = self:GetNeedItemCount()
		if totalCount < count then
			DisableSprite:SetActive(true)
			GetHint:SetActive(true)
			CountLabel.text = SafeStringFormat3("[FF0000]%d[-]/%d", totalCount, count)

			local onNeedItemClicked = function (go)
				self:OnNeedItemClicked(go, itemId)
			end
			CommonDefs.AddOnClickListener(DisableSprite.gameObject, DelegateFactory.Action_GameObject(onNeedItemClicked), false)
		else
			DisableSprite:SetActive(false)
			GetHint:SetActive(false)
			CountLabel.text = SafeStringFormat3("%d/%d", totalCount, count)
		end
	end
end

function LuaBabyQiChangJianDingWnd:OnQnCheckBoxChanged(value)
	if value then
		PlayerPrefs.SetInt(self.Key_BabyQiChangAutoUseMoney, 1)
	else
		PlayerPrefs.SetInt(self.Key_BabyQiChangAutoUseMoney, 0)
	end
	self:UpdateJianDingCost()
end

function LuaBabyQiChangJianDingWnd:OnNeedItemClicked(go, itemId)
	CItemAccessListMgr.Inst:ShowItemAccessInfo(itemId, false, go.transform, CTooltipAlignType.Right)
	--CItemAccessTipMgr.Inst:ShowAccessTip(nil, itemId, false, go.transform, CTooltipAlignType.Top)
end

function LuaBabyQiChangJianDingWnd:UpdateQiChangInfo(qichangId)
	
	local qichang = Baby_QiChang.GetData(qichangId)
	if not qichang then 
		self.QiChangLabel.text = LocalString.StrH2V(self.QiChangLabel.text,false)
		return 
	end

	local bgPath = LuaBabyMgr.GetQiChangBG(self.SelectedBaby.Gender, qichang.Quality)
	self.QiChangBG:LoadMaterial(bgPath)

	local name = qichang.NameM
	if self.SelectedBaby.Gender == 1 then
		name = qichang.NameF
	end
	local qctext = SafeStringFormat3("[%s]%s[-]", LuaBabyMgr.GetQiChangNameColor(qichang.Quality-1), name)
	self.QiChangLabel.text = LocalString.StrH2V(qctext,false)
	self.QiChangFX:DestroyFx()
	local qichangfxPath = LuaBabyMgr.GetQiChangFx(qichang.Quality-1)
	if qichangfxPath then
		self.QiChangFX:LoadFx(qichangfxPath)
	end
end

-- 获得鉴定物品的数量
function LuaBabyQiChangJianDingWnd:GetNeedItemCount()
	local setting = Baby_Setting.GetData()
	local itemId = setting.BabyQiChangJianDingItem

	local bindItemCountInBag, notBindItemCountInBag = CItemMgr.Inst:CalcItemCountInBagByTemplateId(itemId)
	local totalCount = bindItemCountInBag + notBindItemCountInBag
	return totalCount
end

-- 道具不够是否直接消耗银两
function LuaBabyQiChangJianDingWnd:AutoUseMoney()
	return PlayerPrefs.GetInt(self.Key_BabyQiChangAutoUseMoney, 1) > 0
end

function LuaBabyQiChangJianDingWnd:OnMoreInfoClicked(go)
	g_MessageMgr:ShowMessage("BABY_QICHANG_TIPS")
end

function LuaBabyQiChangJianDingWnd:OnAllBtnClicked(go)
	CUIManager.ShowUI(CLuaUIResources.BabyQiChangShuWnd)
end

function LuaBabyQiChangJianDingWnd:OnFxBtnClicked(go)
	CUIManager.ShowUI(CLuaUIResources.BabyQiChangFxWnd)
end

function LuaBabyQiChangJianDingWnd:OnJianDingBtnClicked(go)

	-- 物品是否足够（优先消耗物品）
	local autoUseMoney = self:AutoUseMoney()
	local needItemCount = self:GetNeedItemCount()
	if needItemCount > 0 then
		self:RequestJianDingQiChang(autoUseMoney)
	else
		-- 物品不足够
		if autoUseMoney then
			if self.QnCostAndOwnMoney.moneyEnough then
				self:RequestJianDingQiChang(autoUseMoney)
			else
				g_MessageMgr:ShowMessage("BABY_QICHANG_JIANDING_MONEY_NOTENOUGH")
			end
		else
			-- 物品不够也选择不消耗银两
			local setting = Baby_Setting.GetData()
			CItemAccessTipMgr.Inst:ShowAccessTip(nil, setting.BabyQiChangJianDingItem, false, go.transform, CTooltipAlignType.Top)
		end
	end	
end

function LuaBabyQiChangJianDingWnd:RequestJianDingQiChang(useSilver)
	-- 二次确认
	local currentQiChangId = self.SelectedBaby.Props.QiChangData.CurrentQiChangId
	local qichang = Baby_QiChang.GetData(currentQiChangId)
	if (qichang and qichang.Quality >= 4) or self.IsNewQiChang then
		local text = g_MessageMgr:FormatMessage("BABY_QICHANG_JIANDING_CONFIRM")
    	MessageWndManager.ShowOKCancelMessage(text, DelegateFactory.Action(function ()
    		self:SaveActiveQiChang()
        	Gac2Gas.RequestActiveBabyQiChang(self.SelectedBaby.Id, useSilver)
        	self.JianDingBtn.Enabled = false
        	self.ButtonEnableTick = RegisterTickOnce(function ()
        		self.JianDingBtn.Enabled = true
        	end, 2700)
    	end), nil, nil, nil, false)
	else
		self:SaveActiveQiChang()
		self.JianDingBtn.Enabled = false
        self.ButtonEnableTick = RegisterTickOnce(function ()
        	self.JianDingBtn.Enabled = true
        end, 2700)
		Gac2Gas.RequestActiveBabyQiChang(self.SelectedBaby.Id, useSilver)
	end
end

function LuaBabyQiChangJianDingWnd:SaveActiveQiChang()
	CommonDefs.DictClear(self.SavedQiChang)
	CommonDefs.DictIterate(self.SelectedBaby.Props.QiChangData.ActivedQiChang, DelegateFactory.Action_object_object(function(key, val)
		CommonDefs.DictAdd_LuaCall(self.SavedQiChang, key, val)
	end))
end

function LuaBabyQiChangJianDingWnd:OnActiveBabyQiChangSuccess(babyId, newQiChangId)
	if self.SelectedBaby.Id == babyId then
		self.IsNewQiChang = not CommonDefs.DictContains_LuaCall(self.SavedQiChang, newQiChangId)
		if self.QiChangAlert then
			local hasNewQiChang = self:CheckBabyHasNewQiChang()
			self.QiChangAlert.gameObject:SetActive(hasNewQiChang)
		end
		self.QiChangShowFX:DestroyFx()
		self.IsNew:SetActive(false)
		-- Step 1 播放宝宝动作
		local setting = Baby_Setting.GetData()
		self.BabyTexture:Init(self.SelectedBaby, -180, false, setting.QiChangExpressionId)

		-- Step 2 播放特效
		self.JuLongFX:DestroyFx()
		self.JuLongFX:LoadFx("fx/ui/prefab/UI_baobaoqichang001.prefab")

		local qichang = Baby_QiChang.GetData(newQiChangId)
		local fxStr = self:GetQiChangActiveFX(qichang.Quality-1)
		self.JianDingFX:DestroyFx()
		self.JianDingFX:LoadFx(fxStr)

		-- Step 3 显示新的气场名
		self:DestroyJianDingTick()
		self.JianDingTick = RegisterTickOnce(function ()
			self:UpdateQiChangInfo(newQiChangId)
			self.QiChangShowFX:DestroyFx()
			self.QiChangShowFX:LoadFx(CUIFxPaths.CommonYellowLineFxPath)
			self.IsNew:SetActive(self.IsNewQiChang)
			CUIFx.DoAni(Vector4(0 - self.QiChangShowVect.width/2, 0 + self.QiChangShowVect.height/2, self.QiChangShowVect.width, self.QiChangShowVect.height), false, self.QiChangShowFX)
		end, 1500)
		
	end
end

-- 获得鉴定气场的特效
function LuaBabyQiChangJianDingWnd:GetQiChangActiveFX(quality)
	if quality == 0 then
		return "fx/ui/prefab/UI_baobaoqichang002_bai.prefab"
	elseif quality == 1 then
		return "fx/ui/prefab/UI_baobaoqichang002_huang.prefab"
	elseif quality == 2 then
		return "fx/ui/prefab/UI_baobaoqichang002_cheng.prefab"
	elseif quality == 3 then
		return "fx/ui/prefab/UI_baobaoqichang002_lan.prefab"
	elseif quality == 3 then
		return "fx/ui/prefab/UI_baobaoqichang002_hong.prefab"
	elseif quality == 4 then
		return "fx/ui/prefab/UI_baobaoqichang002_zi.prefab"
	end
	return "fx/ui/prefab/UI_baobaoqichang002_bai.prefab"
end

function LuaBabyQiChangJianDingWnd:OnBabyInfoDelete()
	CUIManager.CloseUI(CLuaUIResources.BabyQiChangJianDingWnd)
end

function LuaBabyQiChangJianDingWnd:DestroyJianDingTick()
	if self.JianDingTick ~= nil then
      	UnRegisterTick(self.JianDingTick)
      	self.JianDingTick = nil
    end
end

function LuaBabyQiChangJianDingWnd:DestroyButtonEnableTick()
	if self.ButtonEnableTick ~= nil then
		UnRegisterTick(self.ButtonEnableTick)
		self.ButtonEnableTick = nil
	end
end

function LuaBabyQiChangJianDingWnd:UpdateBabyQiChangFx(qichangId)
	local babyAppearance = BabyMgr.Inst:ToBabyAppearance(self.SelectedBaby)
	babyAppearance.QiChangId = qichangId
	self.BabyTexture:Init(babyAppearance, -180, 0, 0)
end

function LuaBabyQiChangJianDingWnd:OnSetShowQiChangIdSuccess(babyId, qiChangId)
	if self.SelectedBaby and babyId == self.SelectedBaby.Id then
		self.BabyTexture:Init(self.SelectedBaby, -180, false, 0)
	end
end

function LuaBabyQiChangJianDingWnd:OnResetQiChangFx()
	self.BabyTexture:Init(self.SelectedBaby, -180, false, 0)
end

function LuaBabyQiChangJianDingWnd:UpdateQiChangRewardAlert(babyId)
	self.RewardAlert:SetActive(LuaBabyMgr.HasNewQiChangReward(self.SelectedBaby))
end

function LuaBabyQiChangJianDingWnd:OnEnable()
	g_ScriptEvent:AddListener("ActiveBabyQiChangSuccess", self, "OnActiveBabyQiChangSuccess")
	g_ScriptEvent:AddListener("BabyInfoDelete", self, "OnBabyInfoDelete")
	g_ScriptEvent:AddListener("PreviewBabyQiChangFx",self, "UpdateBabyQiChangFx")
	g_ScriptEvent:AddListener("SetShowQiChangIdSuccess", self, "OnSetShowQiChangIdSuccess")
	g_ScriptEvent:AddListener("ResetQiChangFx", self, "OnResetQiChangFx")
	g_ScriptEvent:AddListener("SendItem", self, "UpdateJianDingCost")
	g_ScriptEvent:AddListener("SetItemAt", self, "UpdateJianDingCost")
	g_ScriptEvent:AddListener("ReceiveQiChangActiveRewardSuccess", self, "UpdateQiChangRewardAlert")
end

function LuaBabyQiChangJianDingWnd:OnDisable()
	g_ScriptEvent:RemoveListener("ActiveBabyQiChangSuccess", self, "OnActiveBabyQiChangSuccess")
	g_ScriptEvent:RemoveListener("BabyInfoDelete", self, "OnBabyInfoDelete")
	g_ScriptEvent:RemoveListener("PreviewBabyQiChangFx",self, "UpdateBabyQiChangFx")
	g_ScriptEvent:RemoveListener("SetShowQiChangIdSuccess", self, "OnSetShowQiChangIdSuccess")
	g_ScriptEvent:RemoveListener("ResetQiChangFx", self, "OnResetQiChangFx")
	g_ScriptEvent:RemoveListener("SendItem", self, "UpdateJianDingCost")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "UpdateJianDingCost")
	g_ScriptEvent:RemoveListener("ReceiveQiChangActiveRewardSuccess", self, "UpdateQiChangRewardAlert")
	self:DestroyJianDingTick()
	self:DestroyButtonEnableTick()
end

function LuaBabyQiChangJianDingWnd:GetGuideGo(methodName)
	if methodName=="AllQiChangButton" then
		return self.AllBtn
	end
	return nil
end

return LuaBabyQiChangJianDingWnd
