local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local UISprite = import "UISprite"
local UISlider = import "UISlider"

--主界面活动按钮
LuaUpdownWorldButton = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaUpdownWorldButton, "Icon", "Icon", UISprite)
RegistChildComponent(LuaUpdownWorldButton, "Slider", "Slider", UISlider)
RegistChildComponent(LuaUpdownWorldButton, "Foreground", "Foreground", UISprite)

--@endregion RegistChildComponent end

function LuaUpdownWorldButton:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaUpdownWorldButton:Init(data)
    self.Slider.value = data.Value
    self.Foreground.color = data.Color
    UIEventListener.Get(self.gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            LuaHanJia2024Mgr:ReqUpDownWorldData()
        end
    )
end

--@region UIEvent

--@endregion UIEvent
