local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local Profession = import "L10.Game.Profession"
local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"
local EGetPlayerAppearanceContext = import "L10.Game.EGetPlayerAppearanceContext"

LuaBiWuCrossTeamWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaBiWuCrossTeamWnd, "TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaBiWuCrossTeamWnd, "TotleLabel", "TotleLabel", UILabel)
RegistChildComponent(LuaBiWuCrossTeamWnd, "HelpBtn", "HelpBtn", GameObject)
RegistChildComponent(LuaBiWuCrossTeamWnd, "OKBtn", "OKBtn", GameObject)
RegistChildComponent(LuaBiWuCrossTeamWnd, "ItemTemplate1", "ItemTemplate1", GameObject)
RegistChildComponent(LuaBiWuCrossTeamWnd, "ItemTemplate2", "ItemTemplate2", GameObject)
RegistChildComponent(LuaBiWuCrossTeamWnd, "ItemTemplate3", "ItemTemplate3", GameObject)
RegistChildComponent(LuaBiWuCrossTeamWnd, "ItemTemplate4", "ItemTemplate4", GameObject)
RegistChildComponent(LuaBiWuCrossTeamWnd, "ItemTemplate5", "ItemTemplate5", GameObject)
RegistChildComponent(LuaBiWuCrossTeamWnd, "TeamWnd", "TeamWnd", QnTabView)

--@endregion RegistChildComponent end
RegistClassMember(LuaBiWuCrossTeamWnd, "m_pid")
RegistClassMember(LuaBiWuCrossTeamWnd, "m_cdtick")

function LuaBiWuCrossTeamWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    UIEventListener.Get(self.HelpBtn.gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:OnHelpBtnClick()
        end
    )

    UIEventListener.Get(self.OKBtn.gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:OnOKBtnClick()
        end
    )

    self.TeamWnd.OnSelect =
        DelegateFactory.Action_QnTabButton_int(
        function(btn, index)
            self:OnTabsSelected(btn, index)
        end
    )

    --@endregion EventBind end
end

function LuaBiWuCrossTeamWnd:OnGasDisconnect()
    CUIManager.CloseUI(CLuaUIResources.BiWuCrossTeamWnd)
end

function LuaBiWuCrossTeamWnd:OnEnable()
    g_ScriptEvent:AddListener("GasDisconnect", self, "OnGasDisconnect")
end

function LuaBiWuCrossTeamWnd:OnDisable()
    g_ScriptEvent:RemoveListener("GasDisconnect", self, "OnGasDisconnect")
    if self.m_cdtick then
        UnRegisterTick(self.m_cdtick)
    end
    self.m_cdtick = nil
end

function LuaBiWuCrossTeamWnd:Init()
    self:Refresh()
end

function LuaBiWuCrossTeamWnd:Refresh()
    local team = LuaBiWuCrossMgr.TeamInfo
    local type = LuaBiWuCrossMgr.TeamSelectType
    if team == nil then
        return
    end
    if type == 0 then
        self.TitleLabel.text = LocalString.GetString("队伍详情")

        local t = 0
        for i = 1, #team do
            t = t + team[i].ZhuangPing
        end
        self.TotleLabel.text = SafeStringFormat3(LocalString.GetString("总装评 %s"), t)
        self.TotleLabel.gameObject:SetActive(true)

        self.HelpBtn:SetActive(false)
        self.OKBtn:SetActive(false)
    else
        self.m_pid = team[1].MemberId
        self.TitleLabel.text = LocalString.GetString("英雄选择")
        self.TotleLabel.gameObject:SetActive(false)
        self.HelpBtn:SetActive(true)
        self.OKBtn:SetActive(true)

        local oklabel = FindChildWithType(self.OKBtn.transform, "Label", typeof(UILabel))
        local time = LuaBiWuCrossMgr.TeamSelectLeftTime
        oklabel.text = SafeStringFormat3(LocalString.GetString("确定(%ss)"), time)
        if self.m_cdtick then
            UnRegisterTick(self.m_cdtick)
        end
        self.m_cdtick =
            RegisterTick(
            function()
                time = time - 1
                oklabel.text = SafeStringFormat3(LocalString.GetString("确定(%ss)"), time)
                if time <= 0 then
                    self:OnOKBtnClick()
                    UnRegisterTick(self.m_cdtick)
                end
            end,
            1000
        )
    end

    local ctrls = {
        self.ItemTemplate1.transform,
        self.ItemTemplate2.transform,
        self.ItemTemplate3.transform,
        self.ItemTemplate4.transform,
        self.ItemTemplate5.transform
    }
    local max = 0
    local maxindex = 0
    for i = 1, #ctrls do
        if i <= #team then
            ctrls[i].gameObject:SetActive(true)
            self:InitTeamMemberCtrl(ctrls[i], team[i])
            if team[i].ZhuangPing > max then
                maxindex = i
                max = team[i].ZhuangPing
            end
        else
            ctrls[i].gameObject:SetActive(false)
        end
    end
    self.TeamWnd:ChangeTo(maxindex - 1)
end

function LuaBiWuCrossTeamWnd:OnTabsSelected(btn, row)
    local team = LuaBiWuCrossMgr.TeamInfo
    if team == nil then
        return
    end
    local mem = team[row + 1]
    self.m_pid = mem.MemberId
end

function LuaBiWuCrossTeamWnd:InitTeamMemberCtrl(ctrl, memdata)
    local icon = FindChildWithType(ctrl, "Row/HeadIcon", typeof(CUITexture))
    local namelb = FindChildWithType(ctrl, "Row/LbName", typeof(UILabel))
    local xwlb = FindChildWithType(ctrl, "Row/LbXiuWei", typeof(UILabel))
    local xllb = FindChildWithType(ctrl, "Row/LbXiuLian", typeof(UILabel))
    local zplb = FindChildWithType(ctrl, "Row/LbZhuanPin", typeof(UILabel))
    local lvlb = FindChildWithType(ctrl, "Row/LbLevel", typeof(UILabel))

    icon:LoadNPCPortrait(CUICommonDef.GetPortraitName(memdata.Job, memdata.Gender, -1), false)
    namelb.text = memdata.Name
    xwlb.text = SafeStringFormat3("%.1f", memdata.XiuWeiGrade)
    xllb.text = SafeStringFormat3("%.1f", memdata.XiuLianGrade)
    zplb.text = tostring(memdata.ZhuangPing)
    if memdata.XianFanStatus > 0 then
        local color = L10.Game.Constants.ColorOfFeiSheng
        lvlb.text = System.String.Format("[c][{0}]{1}[-][/c]", color, memdata.Grade)
    else
        lvlb.text = memdata.Grade
    end
end

--@region UIEvent

function LuaBiWuCrossTeamWnd:OnHelpBtnClick()
    g_MessageMgr:ShowMessage("BIWUCROSS_HERO_RULE")
end

function LuaBiWuCrossTeamWnd:OnOKBtnClick()
    LuaBiWuCrossMgr.ReqSelectAsHero(self.m_pid)
    CUIManager.CloseUI(CLuaUIResources.BiWuCrossTeamWnd)
end

--@endregion UIEvent
