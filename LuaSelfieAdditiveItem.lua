require("3rdParty/ScriptEvent")
require("common/common_include")

local LuaGameObject=import "LuaGameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local PaiZhao_TieZhi = import "L10.Game.PaiZhao_TieZhi"
local PaiZhao_QiPao = import "L10.Game.PaiZhao_QiPao"
local PaiZhao_LvJing = import "L10.Game.PaiZhao_LvJing"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local AlignType = import "L10.UI.CTooltip+AlignType"
CLuaSelfieAdditiveItem=class()
RegistClassMember(CLuaSelfieAdditiveItem,"BG")
RegistClassMember(CLuaSelfieAdditiveItem,"Texture")
RegistClassMember(CLuaSelfieAdditiveItem,"Label")
RegistClassMember(CLuaSelfieAdditiveItem,"onClick")
RegistClassMember(CLuaSelfieAdditiveItem,"id")
RegistClassMember(CLuaSelfieAdditiveItem,"HBG")
RegistClassMember(CLuaSelfieAdditiveItem,"Access")
RegistClassMember(CLuaSelfieAdditiveItem,"DisableBg")
RegistClassMember(CLuaSelfieAdditiveItem,"UnLocked")
RegistClassMember(CLuaSelfieAdditiveItem,"ItemId")
RegistClassMember(CLuaSelfieAdditiveItem,"Density")
function CLuaSelfieAdditiveItem:AttachToGo(go)
	self.Label = LuaGameObject.GetChildNoGC(go.transform,"Label").label
	self.Texture = LuaGameObject.GetChildNoGC(go.transform,"Texture").cTexture
	self.BG = LuaGameObject.GetChildNoGC(go.transform,"BG").sprite
	self.HBG = LuaGameObject.GetChildNoGC(go.transform,"HBG").sprite
	self.Access = LuaGameObject.GetChildNoGC(go.transform,"Access").label
	self.DisableBg = LuaGameObject.GetChildNoGC(go.transform,"DisableBg").sprite
	local clickcb = function(go)
		if not self.UnLocked then
			CItemAccessListMgr.Inst:ShowItemAccessInfo(self.ItemId, true, nil, AlignType.Right)
			return
		end
		if self.onClick ~= nil then 
			self.onClick(self.id, self.Density)
		end
	end
	CommonDefs.AddOnClickListener(go,DelegateFactory.Action_GameObject(clickcb),false)
end
function CLuaSelfieAdditiveItem:SetData(id,itemtype)
	self.id = id
	local data = nil
	self.UnLocked = true
	if itemtype == 0 then 
		data = PaiZhao_TieZhi.GetData(id)
	elseif itemtype == 1 then 	
		data = PaiZhao_LvJing.GetData(id)
		self.UnLocked = self:GetLvJingUnLock(data.LockGroup)
		self.ItemId = data.Locked
		self.Density = data.Density
	elseif itemtype == 2 then 	
		data = PaiZhao_QiPao.GetData(id)
	end
	if data ~= nil then 
		self.Texture:LoadMaterial(data.IconTexture,false)
		self.Label.text = data.Name
		self.Access.gameObject:SetActive(not self.UnLocked)
		self.DisableBg.gameObject:SetActive(not self.UnLocked)
	end
end
function CLuaSelfieAdditiveItem:Highlight(flag)
	self.HBG.gameObject:SetActive(flag)
end
function CLuaSelfieAdditiveItem:GetLvJingUnLock(lockGroup)
	if lockGroup == 0 then
		return true
	end
	local lockNum = 0
    if CommonDefs.DictContains(CClientMainPlayer.Inst.PlayProp.PersistPlayData, typeof(UInt32), EnumPersistPlayDataKey_lua.eUnLockPaiZhaoLvJing) then
		lockNum = tonumber(CClientMainPlayer.Inst.PlayProp.PersistPlayData[EnumPersistPlayDataKey_lua.eUnLockPaiZhaoLvJing].StringData)
	end
	local unlockTable = {}
	if lockNum then
		while lockNum > 0 do
			local r = lockNum % 2
			lockNum = math.floor(lockNum/2)
			table.insert(unlockTable,r)
		end
	end
	if unlockTable[lockGroup] and unlockTable[lockGroup] == 1 then
		return true
	end
	return false
end

return CLuaSelfieAdditiveItem








