local CUITexture = import "L10.UI.CUITexture"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local Extensions = import "Extensions"
local CButton = import "L10.UI.CButton"
local EnumGender = import "L10.Game.EnumGender"
local UISprite = import "UISprite"
local LuaTweenUtils = import "LuaTweenUtils"

LuaAppearanceUmbrellaSubview = class()

RegistChildComponent(LuaAppearanceUmbrellaSubview,"m_UmbrellaItem", "CommonItemCell", GameObject)
RegistChildComponent(LuaAppearanceUmbrellaSubview,"m_ItemDisplay", "AppearanceCommonItemDisplayView", CCommonLuaScript)
RegistChildComponent(LuaAppearanceUmbrellaSubview,"m_ContentGrid", "ContentGrid", UIGrid)
RegistChildComponent(LuaAppearanceUmbrellaSubview,"m_ContentScrollView", "ContentScrollView", UIScrollView)

RegistClassMember(LuaAppearanceUmbrellaSubview, "m_AllUmbrellas")
RegistClassMember(LuaAppearanceUmbrellaSubview, "m_SelectedDataId")

function LuaAppearanceUmbrellaSubview:Awake()
end

function LuaAppearanceUmbrellaSubview:Init()
    self:SetDefaultSelection()
    self:LoadData()
end

function LuaAppearanceUmbrellaSubview:LoadData()
    self.m_AllUmbrellas = LuaAppearancePreviewMgr:GetAllUmbrellaInfo(false)

    Extensions.RemoveAllChildren(self.m_ContentGrid.transform)
    self.m_ContentGrid.gameObject:SetActive(true)

    for i = 1, # self.m_AllUmbrellas do
        local child = CUICommonDef.AddChild(self.m_ContentGrid.gameObject, self.m_UmbrellaItem)
        child:SetActive(true)
        self:InitItem(child, self.m_AllUmbrellas[i])
    end
    self.m_ContentGrid:Reposition()
    self.m_ContentScrollView:ResetPosition()
    self:InitSelection()
end

function LuaAppearanceUmbrellaSubview:SetDefaultSelection()
    self.m_SelectedDataId = LuaAppearancePreviewMgr:GetCurrentInUseUmbrella()
end

function LuaAppearanceUmbrellaSubview:InitSelection()
    if self.m_SelectedDataId == 0 then
        self:OnItemClick(nil)
        return
    end

    local childCount = self.m_ContentGrid.transform.childCount
    for i=0, childCount-1 do
        local childGo = self.m_ContentGrid.transform:GetChild(i).gameObject
        local appearanceData = self.m_AllUmbrellas[i+1]
        if appearanceData.id == self.m_SelectedDataId then
            self:OnItemClick(childGo)
            break
        end
    end
end

function LuaAppearanceUmbrellaSubview:InitItem(itemGo, appearanceData)
    local iconTexture = itemGo.transform:GetComponent(typeof(CUITexture))
    local disabledGo = itemGo.transform:Find("Disabled").gameObject
    local lockedGo = itemGo.transform:Find("Locked").gameObject
    local cornerGo = itemGo.transform:Find("Corner").gameObject
    local selectedGo = itemGo.transform:Find("Selected").gameObject
    iconTexture:LoadMaterial(CClientMainPlayer.Inst and CClientMainPlayer.Inst.Gender == EnumGender.Male and appearanceData.icon or appearanceData.femaleIcon)
    lockedGo:SetActive(not LuaAppearancePreviewMgr:MainPlayerHasUmbrella(appearanceData.id))
    disabledGo:SetActive(lockedGo.activeSelf or LuaAppearancePreviewMgr:IsUmbrellaExpired(appearanceData.id))
    cornerGo:SetActive(LuaAppearancePreviewMgr:IsCurrentInUseUmbrella(appearanceData.id))
    selectedGo:SetActive(self.m_SelectedDataId and self.m_SelectedDataId==appearanceData.id)

    UIEventListener.Get(itemGo).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnItemClick(go)
    end)
end

function LuaAppearanceUmbrellaSubview:OnItemClick(go)
    local expressionId = 0
    local childCount = self.m_ContentGrid.transform.childCount
    for i=0, childCount-1 do
        local childGo = self.m_ContentGrid.transform:GetChild(i).gameObject
        if childGo == go then
            childGo.transform:Find("Selected").gameObject:SetActive(true)
            self.m_SelectedDataId = self.m_AllUmbrellas[i+1].id
            expressionId = self.m_AllUmbrellas[i+1].expressionId
        else
            childGo.transform:Find("Selected").gameObject:SetActive(false)
        end
    end
    self:UpdateItemDisplay()
    g_ScriptEvent:BroadcastInLua("RequestPreviewMainPlayerUmbrella", expressionId)
end

function LuaAppearanceUmbrellaSubview:UpdateItemDisplay()
    self.m_ItemDisplay.gameObject:SetActive(true)
    local id = self.m_SelectedDataId and self.m_SelectedDataId or 0
    if id==0 then
        self.m_ItemDisplay:Clear()
        return
    end
    local appearanceData = nil
    for i=1,#self.m_AllUmbrellas do
        if self.m_AllUmbrellas[i].id == id then
            appearanceData = self.m_AllUmbrellas[i]
            break
        end
    end
    
    local icon = (CClientMainPlayer.Inst and CClientMainPlayer.Inst.Gender == EnumGender.Male and appearanceData.icon or appearanceData.femaleIcon)
    local disabled = true
    local locked = true
    local buttonTbl = {}
    local desc = LuaAppearancePreviewMgr:GetUmbrellaConditionText(appearanceData.id)
    local exist = LuaAppearancePreviewMgr:MainPlayerHasUmbrella(appearanceData.id)
    local inUse = LuaAppearancePreviewMgr:IsCurrentInUseUmbrella(appearanceData.id)
    local expired = LuaAppearancePreviewMgr:IsUmbrellaExpired(appearanceData.id)
    locked = not exist
    disabled = locked or expired

    if exist and not inUse and not expired then
        table.insert(buttonTbl, {text=LocalString.GetString("更换"), isYellow=false, action=function(go) self:OnApplyButtonClick() end})
    end
    if self.m_SelectedDataId>0 and not exist or expired then
        if expired then
            table.insert(buttonTbl, {text=LocalString.GetString("丢弃"), isYellow=false, action=function(go) self:OnDiscardButtonClick() end})
        else
            table.insert(buttonTbl, {text=LocalString.GetString("获取"), isYellow=true, action=function(go) self:OnMoreButtonClick(go) end})
        end
    end
    if appearanceData.availableTime==0 and LuaAppearancePreviewMgr:GetDefaultUmbrellaDesignInfo().ID~=appearanceData.id then --永久的非默认伞显示风尚度
        self.m_ItemDisplay:Init(icon, appearanceData.name, desc, SafeStringFormat3(LocalString.GetString("风尚度+%d"), appearanceData.fashionability), true, disabled, locked, buttonTbl)
    else
        self.m_ItemDisplay:Init(icon, appearanceData.name, desc, "", true, disabled, locked, buttonTbl)
    end
end

function LuaAppearanceUmbrellaSubview:OnApplyButtonClick()
    local exist = LuaAppearancePreviewMgr:MainPlayerHasUmbrella(self.m_SelectedDataId)
    if exist then
        LuaAppearancePreviewMgr:RequestSetUmbrella(self.m_SelectedDataId)
    end
end

function LuaAppearanceUmbrellaSubview:OnDiscardButtonClick()
    local exist = LuaAppearancePreviewMgr:MainPlayerHasUmbrella(self.m_SelectedDataId)
    if exist then
        LuaAppearancePreviewMgr:RequestDiscardUmbrella(self.m_SelectedDataId)
    end
end

function LuaAppearanceUmbrellaSubview:OnMoreButtonClick(go)
    local id = self.m_SelectedDataId and self.m_SelectedDataId or 0
    local appearanceData = nil
    for i=1,#self.m_AllUmbrellas do
        if self.m_AllUmbrellas[i].id == id then
            appearanceData = self.m_AllUmbrellas[i]
            break
        end
    end
    if appearanceData then
        LuaItemAccessListMgr:ShowItemAccessInfoAtLeft(appearanceData.itemGetId, true, go.transform)
    end
end

function LuaAppearanceUmbrellaSubview:OnEnable()
    g_ScriptEvent:AddListener("UpdatePlayerExpressionAppearance", self, "OnUpdatePlayerExpressionAppearance")
end

function LuaAppearanceUmbrellaSubview:OnDisable()
    g_ScriptEvent:RemoveListener("UpdatePlayerExpressionAppearance", self, "OnUpdatePlayerExpressionAppearance")
end

function LuaAppearanceUmbrellaSubview:OnUpdatePlayerExpressionAppearance(args)
    self:LoadData()
end