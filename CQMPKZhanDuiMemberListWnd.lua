-- Auto Generated!!
local CommonDefs = import "L10.Game.CommonDefs"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local CQMPKMemberInfoItem = import "L10.UI.CQMPKMemberInfoItem"
local CQMPKZhanDuiMemberListWnd = import "L10.UI.CQMPKZhanDuiMemberListWnd"
local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"
local Double = import "System.Double"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumEventType = import "EnumEventType"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EnumQMPKZhanduiMemberWndType = import "L10.Game.EnumQMPKZhanduiMemberWndType"
local EventManager = import "EventManager"
local GameObject = import "UnityEngine.GameObject"
local LocalString = import "LocalString"
local UIEventListener = import "UIEventListener"
local Vector3 = import "UnityEngine.Vector3"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CQMPKZhanDuiMemberListWnd.m_OnJoinQMPKZhanDuiSuccess_CS2LuaHook = function (this, zhanduiId) 
    if zhanduiId ~= CQuanMinPKMgr.Inst.m_ZhanDuiList[CQuanMinPKMgr.Inst.m_CurrentZhanDuiIndex].m_Id then
        return
    end
    this.RequestLabel.text = LocalString.GetString("取消申请")
    this.m_HasApplied = true
    CQuanMinPKMgr.Inst.m_ZhanDuiList[CQuanMinPKMgr.Inst.m_CurrentZhanDuiIndex].m_HasApplied = 1
end
CQMPKZhanDuiMemberListWnd.m_Init_CS2LuaHook = function (this) 
    this:ShowZhanDui(CQuanMinPKMgr.Inst.m_CurrentZhanDuiIndex)
    if this.MemberRoot.childCount == 0 then
        this:initMembers()
    end
    if CQuanMinPKMgr.Inst.m_HasZhanDui or CQuanMinPKMgr.Inst.m_ZhanduiMemberWndType == EnumQMPKZhanduiMemberWndType.eScoreZhandui or CQuanMinPKMgr.Inst.m_HideApplyButtonInMemberWnd then
        this.RequestButton:SetActive(false)
    end
end
CQMPKZhanDuiMemberListWnd.m_ShowZhanDui_CS2LuaHook = function (this, index) 
    if CQuanMinPKMgr.Inst.m_ZhanduiMemberWndType == EnumQMPKZhanduiMemberWndType.eSearchZhandui then
        if index < 0 or index >= CQuanMinPKMgr.Inst.m_ZhanDuiList.Count then
            this:Close()
            return
        end
        Gac2Gas.QueryQmpkZhanDuiInfoByZhanDuiId(CQuanMinPKMgr.Inst.m_ZhanDuiList[index].m_Id)
        if index == CQuanMinPKMgr.Inst.m_ZhanDuiList.Count - 1 then
            this.RightArrow:SetActive(false)
        else
            this.RightArrow:SetActive(true)
        end
    elseif CQuanMinPKMgr.Inst.m_ZhanduiMemberWndType == EnumQMPKZhanduiMemberWndType.eScoreZhandui then
        if index < 0 or index >= CQuanMinPKMgr.Inst.m_ScoreRankList.Count then
            this:Close()
            return
        end
        Gac2Gas.QueryQmpkZhanDuiInfoByZhanDuiId(CQuanMinPKMgr.Inst.m_ScoreRankList[index].m_Id)
        if index == CQuanMinPKMgr.Inst.m_ScoreRankList.Count - 1 then
            this.RightArrow:SetActive(false)
        else
            this.RightArrow:SetActive(true)
        end
    elseif CQuanMinPKMgr.Inst.m_ZhanduiMemberWndType == EnumQMPKZhanduiMemberWndType.eOneZhandui then
        Gac2Gas.QueryQmpkZhanDuiInfoByZhanDuiId(CQuanMinPKMgr.Inst.m_CurrentZhanduiId)
        this.LeftArrow:SetActive(false)
        this.RightArrow:SetActive(false)
        return
    end


    if index == 0 then
        this.LeftArrow:SetActive(false)
    else
        this.LeftArrow:SetActive(true)
    end
end
CQMPKZhanDuiMemberListWnd.m_initMembers_CS2LuaHook = function (this) 
    this.memberList = CreateFromClass(MakeGenericClass(List, CQMPKMemberInfoItem))
    do
        local i = 0
        while i < CQuanMinPKMgr.Inst.m_ZhanDuiMemberMaxCount do
            local go = TypeAs(CommonDefs.Object_Instantiate(this.MemberItemPrefab), typeof(GameObject))
            local trans = go.transform
            trans.parent = this.MemberRoot
            trans.localScale = Vector3.one
            trans.localPosition = Vector3((i % 5) * CQMPKZhanDuiMemberListWnd.xInterval, - (math.floor(i / 5)) * CQMPKZhanDuiMemberListWnd.yInterval)
            go:SetActive(false)
            local item = CommonDefs.GetComponent_GameObject_Type(go, typeof(CQMPKMemberInfoItem))
            item.ClickCallback = MakeDelegateFromCSFunction(this.onItemClick, MakeGenericClass(Action2, Double, GameObject), this)
            CommonDefs.ListAdd(this.memberList, typeof(CQMPKMemberInfoItem), item)
            i = i + 1
        end
    end
    this.MemberItemPrefab:SetActive(false)
end
CQMPKZhanDuiMemberListWnd.m_Start_CS2LuaHook = function (this) 
    UIEventListener.Get(this.RequestButton).onClick = MakeDelegateFromCSFunction(this.onRequestClick, VoidDelegate, this)
    UIEventListener.Get(this.CloseButton).onClick = MakeDelegateFromCSFunction(this.OnClickCloseButton, VoidDelegate, this)
    UIEventListener.Get(this.LeftArrow).onClick = MakeDelegateFromCSFunction(this.onLeftArrowClick, VoidDelegate, this)
    UIEventListener.Get(this.RightArrow).onClick = MakeDelegateFromCSFunction(this.onRightArrowClick, VoidDelegate, this)
    UIEventListener.Get(this.m_RecruitInfoBtn).onClick = MakeDelegateFromCSFunction(this.OnRecruitInfoBtnClicked, VoidDelegate, this)
end
CQMPKZhanDuiMemberListWnd.m_OnReplyZhanDuiInfo_CS2LuaHook = function (this) 
    do
        local i = 0
        while i < CQuanMinPKMgr.Inst.m_ZhanDuiMemberMaxCount do
            this.memberList[i].gameObject:SetActive(true)
            if i < CQuanMinPKMgr.Inst.m_MemberInfoList.Count then
                this.memberList[i]:SetMemberInfo(CQuanMinPKMgr.Inst.m_MemberInfoList[i], i, nil)
            else
                this.memberList[i]:SetMemberInfo(nil, i, nil)
            end
            i = i + 1
        end
    end
    this.SloganLabel.text = CQuanMinPKMgr.Inst.m_CurrentZhanduiSlogan
    this.NameLabel.text = CQuanMinPKMgr.Inst.m_CurrentZhanduiName
    if CQuanMinPKMgr.Inst.m_MemberInfoList.Count >= CQuanMinPKMgr.Inst.m_ZhanDuiMemberMaxCount then
        this.RequestButton:SetActive(false)
    elseif CQuanMinPKMgr.Inst.m_ZhanduiMemberWndType ~= EnumQMPKZhanduiMemberWndType.eScoreZhandui then
        this.m_HasApplied = CQuanMinPKMgr.Inst.m_HasApplyCurrentZhandui > 0
        if this.m_HasApplied then
            this.RequestLabel.text = LocalString.GetString("取消申请")
        else
            this.RequestLabel.text = LocalString.GetString("申请")
        end
    end
    if not CQuanMinPKMgr.Inst.m_ZhanDuiList or CQuanMinPKMgr.Inst.m_ZhanDuiList.Count == 0 then
        this.RequestButton:SetActive(false)
    end
end
CQMPKZhanDuiMemberListWnd.m_onRequestClick_CS2LuaHook = function (this, go) 
    if not CQuanMinPKMgr.Inst.m_ZhanDuiList or CQuanMinPKMgr.Inst.m_ZhanDuiList.Count == 0 then
        return
    end

    if not this.m_HasApplied then
        Gac2Gas.RequestJoinQmpkZhanDui(CQuanMinPKMgr.Inst.m_ZhanDuiList[CQuanMinPKMgr.Inst.m_CurrentZhanDuiIndex].m_Id)
    else
        this.RequestLabel.text = LocalString.GetString("申请")
        Gac2Gas.RequestCancelJoinQmpkZhanDui(CQuanMinPKMgr.Inst.m_ZhanDuiList[CQuanMinPKMgr.Inst.m_CurrentZhanDuiIndex].m_Id)
        this.m_HasApplied = false
        CQuanMinPKMgr.Inst.m_ZhanDuiList[CQuanMinPKMgr.Inst.m_CurrentZhanDuiIndex].m_HasApplied = 0
        EventManager.BroadcastInternalForLua(EnumEventType.QMPKRequestCancelJoinZhanDui, {CQuanMinPKMgr.Inst.m_ZhanDuiList[CQuanMinPKMgr.Inst.m_CurrentZhanDuiIndex].m_Id})
    end
end
CQMPKZhanDuiMemberListWnd.m_onItemClick_CS2LuaHook = function (this, id, go) 
    if id <= 0 then
        return
    end
    CPlayerInfoMgr.ShowPlayerPopupMenu(math.floor(id), EnumPlayerInfoContext.QMPK, EChatPanel.Undefined, nil, nil, go.transform.position, CPlayerInfoMgr.AlignType.Right)
    do
        local i = 0 local cnt = this.memberList.Count
        while i < cnt do
            this.memberList[i]:SetSelected(this.memberList[i].gameObject == go)
            i = i + 1
        end
    end
end
