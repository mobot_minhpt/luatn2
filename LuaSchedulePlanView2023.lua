require("common/common_include")

local UIGrid = import "UIGrid"
local UIProgressBar = import "UIProgressBar"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local QnButton = import "L10.UI.QnButton"
local MessageMgr = import "L10.Game.MessageMgr"
local BabyMgr = import "L10.Game.BabyMgr"
local LuaUtils = import "LuaUtils"
local Baby_Setting = import "L10.Game.Baby_Setting"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local Baby_Plan = import "L10.Game.Baby_Plan"
local DateTime = import "System.DateTime"
local CUITexture = import "L10.UI.CUITexture"
local Item_Item = import "L10.Game.Item_Item"
local CItemMgr = import "L10.Game.CItemMgr"
local CItemAccessTipMgr = import "L10.UI.CItemAccessTipMgr"
local CTooltipAlignType = import "L10.UI.CTooltip+AlignType"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"
local Baby_BabyExp = import "L10.Game.Baby_BabyExp"
local MessageWndManager = import "L10.UI.MessageWndManager"
local Object = import "System.Object"
local Ease = import "DG.Tweening.Ease"
local Vector3 = import "UnityEngine.Vector3"
local NGUIText = import "NGUIText"
local QnCheckBox = import "L10.UI.QnCheckBox"
local PlayerPrefs = import "UnityEngine.PlayerPrefs"
local Mall_LingYuMall = import "L10.Game.Mall_LingYuMall"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local GameObject = import "UnityEngine.GameObject"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local DelegateFactory = import "DelegateFactory"
local CommonDefs = import "L10.Game.CommonDefs"

LuaSchedulePlanView2023 = class()

-- baby root --
RegistClassMember(LuaSchedulePlanView2023, "Portrait1")
RegistClassMember(LuaSchedulePlanView2023, "Portrait2")
RegistClassMember(LuaSchedulePlanView2023, "Portrait1Container")
RegistClassMember(LuaSchedulePlanView2023, "Portrait2Container")

RegistClassMember(LuaSchedulePlanView2023, "BabyInfo")
RegistClassMember(LuaSchedulePlanView2023, "BabyNameLabel")
RegistClassMember(LuaSchedulePlanView2023, "BabyLevelLabel")
RegistClassMember(LuaSchedulePlanView2023, "ProgressLabel")
RegistClassMember(LuaSchedulePlanView2023, "ExpProgress")
RegistClassMember(LuaSchedulePlanView2023, "GainButton")
RegistClassMember(LuaSchedulePlanView2023, "GainButtonAlert")

-- schedule root --
RegistClassMember(LuaSchedulePlanView2023, "ScheduleGrid")
RegistClassMember(LuaSchedulePlanView2023, "ScheduleTemplate")

-- operation root --
RegistClassMember(LuaSchedulePlanView2023, "QnCostAndOwnMoney")
RegistChildComponent(LuaSchedulePlanView2023, "AddBtn", QnButton)
RegistClassMember(LuaSchedulePlanView2023, "NeedGrid")
RegistClassMember(LuaSchedulePlanView2023, "NeedTemplate")
RegistClassMember(LuaSchedulePlanView2023, "MoreInfo")
RegistClassMember(LuaSchedulePlanView2023, "PlanBtn")
RegistClassMember(LuaSchedulePlanView2023, "ScheduleHint")
--RegistChildComponent(LuaSchedulePlanView2023, "NeedItemHint", UILabel)

-- KouDai Root --
RegistChildComponent(LuaSchedulePlanView2023, "KouDaiRoot", GameObject)
RegistChildComponent(LuaSchedulePlanView2023, "ExchangeBtn", GameObject)

-- 口袋直接消耗灵玉
RegistChildComponent(LuaSchedulePlanView2023, "QnCheckBox", QnCheckBox)
RegistChildComponent(LuaSchedulePlanView2023, "LingYuCost", CCurentMoneyCtrl)
RegistClassMember(LuaSchedulePlanView2023, "Key_BabyPlanAutoUseLingYu")

RegistClassMember(LuaSchedulePlanView2023, "SelectedAddPlanIndex")	-- 选中的添加派遣的位置
RegistClassMember(LuaSchedulePlanView2023, "SelectedBaby")	-- 本界面选中的CBaby
RegistClassMember(LuaSchedulePlanView2023, "OtherBaby")	-- 如果有两个宝宝另一个宝宝
RegistClassMember(LuaSchedulePlanView2023, "LeftBabyId")
RegistClassMember(LuaSchedulePlanView2023, "RightBabyId")

RegistClassMember(LuaSchedulePlanView2023, "NeedItemsEnough") -- 所需物品是否足够

RegistClassMember(LuaSchedulePlanView2023, "ScheduleItemList") -- <index, scheduleItem>
RegistClassMember(LuaSchedulePlanView2023, "SelectedPlans") -- <index, planId>


function LuaSchedulePlanView2023:Init()
	self:InitClassMembers()
	self:InitValues()
end

function LuaSchedulePlanView2023:InitClassMembers()

	self.Portrait1 = self.transform:Find("BabyRoot/Portrait1").gameObject
	self.Portrait1Container = self.transform:Find("BabyRoot/Portrait1/Portrait1Container").gameObject
	self.Portrait2 = self.transform:Find("BabyRoot/Portrait2").gameObject
	self.Portrait2Container = self.transform:Find("BabyRoot/Portrait2/Portrait2Container").gameObject

	self.BabyInfo = self.transform:Find("BabyRoot/BabyInfo").gameObject
	self.BabyNameLabel = self.transform:Find("BabyRoot/BabyInfo/BabyNameLabel"):GetComponent(typeof(UILabel))
	self.BabyLevelLabel = self.transform:Find("BabyRoot/BabyInfo/BabyLevelLabel"):GetComponent(typeof(UILabel))
	self.ProgressLabel = self.transform:Find("BabyRoot/BabyInfo/ExpProgress/ProgressLabel"):GetComponent(typeof(UILabel))
	self.ExpProgress = self.transform:Find("BabyRoot/BabyInfo/ExpProgress"):GetComponent(typeof(UIProgressBar))
	self.GainButton = self.transform:Find("BabyRoot/GainButton").gameObject
	self.GainButtonAlert = self.transform:Find("BabyRoot/GainButton/Alert").gameObject

	self.ScheduleGrid = self.transform:Find("ScheduleRoot/ScheduleGrid"):GetComponent(typeof(UIGrid))
	self.ScheduleTemplate = self.transform:Find("ScheduleRoot/ScheduleTemplate").gameObject
	self.ScheduleTemplate:SetActive(false)

	self.QnCostAndOwnMoney = self.transform:Find("OperationRoot/QnCostAndOwnMoney"):GetComponent(typeof(CCurentMoneyCtrl))
	self.NeedGrid = self.transform:Find("OperationRoot/NeedGrid"):GetComponent(typeof(UIGrid))
	self.NeedTemplate = self.transform:Find("OperationRoot/NeedTemplate").gameObject
	self.NeedTemplate:SetActive(false)
	self.MoreInfo = self.transform:Find("OperationRoot/MoreInfo").gameObject
	self.PlanBtn = self.transform:Find("OperationRoot/PlanBtn"):GetComponent(typeof(QnButton))
	self.ScheduleHint = self.transform:Find("OperationRoot/ScheduleHint"):GetComponent(typeof(UILabel))
	self.ScheduleHint.text = ""

	self.Key_BabyPlanAutoUseLingYu = "Key_BabyPlanAutoUseLingYu"
	--self.NeedItemHint.text = g_MessageMgr:FormatMessage("SCHEDULE_PLAN_NEED_ITEM_HINT")
	--self.NeedItemHint.gameObject:SetActive(true)
end

function LuaSchedulePlanView2023:InitValues()

	local onMoreInfoClicked = function (go)
		self:OnMoreInfoClicked(go)
	end
	CommonDefs.AddOnClickListener(self.MoreInfo, DelegateFactory.Action_GameObject(onMoreInfoClicked), false)

	local onGainButtonClicked = function (go)
		self:OnGainButtonClicked(go)
	end
	CommonDefs.AddOnClickListener(self.GainButton, DelegateFactory.Action_GameObject(onGainButtonClicked), false)
	
	local onExchangeBtnClicked = function (go)
		self:OnExchangeBtnClicked(go)
	end
	CommonDefs.AddOnClickListener(self.ExchangeBtn, DelegateFactory.Action_GameObject(onExchangeBtnClicked), false)

	self.QnCheckBox.OnValueChanged = DelegateFactory.Action_bool(function (value)
        self:OnQnCheckBoxChanged(value)
    end)

	local autoUseMoney = self:AutoUseLingYu()
	self.QnCheckBox:SetSelected(autoUseMoney, true)

	self:InitPortraits()
	self:InitScheduleGrid()
	self:InitKouDai()

end

function LuaSchedulePlanView2023:InitKouDai()
	local KouDaiItemList = Baby_Setting.GetData().BagsId
	for i = 0, 3 do
		local data = Item_Item.GetData(KouDaiItemList[i])
		local KouDaiTex = self.KouDaiRoot.transform:Find("Grid/KouDai"..i + 1 .."/Icon"):GetComponent(typeof(CUITexture))
		local KouDaiCnt = self.KouDaiRoot.transform:Find("Grid/KouDai"..i + 1 .."/Count"):GetComponent(typeof(UILabel))
		local KouDai = self.KouDaiRoot.transform:Find("Grid/KouDai"..i + 1 .."/Sprite").gameObject
		
		KouDaiTex:LoadMaterial(data.Icon)
		KouDaiCnt.text = CItemMgr.Inst:GetItemCount(KouDaiItemList[i])
		
		local onKouDaiClick = function(go)
			CItemInfoMgr.ShowLinkItemTemplateInfo(KouDaiItemList[i], false, nil, CItemInfoMgr.AlignType.Default, 0, 0, 0, 0)
		end
		CommonDefs.AddOnClickListener(KouDai, DelegateFactory.Action_GameObject(onKouDaiClick), false)
	end
end

-- 初始化头像
function LuaSchedulePlanView2023:InitPortraits()

	local selectedBabyId = BabyMgr.Inst:TrySelectBaby()
	if not selectedBabyId then return end

	local selectedBaby = BabyMgr.Inst:GetSelectedBaby()
	self.SelectedBaby = selectedBaby

	if BabyMgr.Inst.BabyDictionary.Count < 1 then
		-- 玩家身上没有宝宝，关闭界面
		CUIManager.CloseUI(CLuaUIResources.BabyWnd)
		return
	elseif BabyMgr.Inst.BabyDictionary.Count == 1 then
		-- 玩家身上只有一个宝宝，选中该宝宝，初始化一个头像框
		self.Portrait1:SetActive(true)
		self.Portrait2:SetActive(false)
		LuaUtils.SetLocalPositionX(self.Portrait1.transform, -660)
		LuaUtils.SetLocalPositionX(self.BabyInfo.transform, -537)

		local portraitTexture = self.Portrait1Container.transform:Find("PortraitTexture"):GetComponent(typeof(CUITexture))
		local portraint = BabyMgr.Inst:GetBabyPortrait(self.SelectedBaby)
		portraitTexture:LoadNPCPortrait(portraint, false)

		if LuaBabyMgr.m_BabyOnPlayerAndPregnant then
			self.Portrait2:SetActive(true)
			LuaUtils.SetLocalPositionX(self.Portrait1.transform, -678)
			LuaUtils.SetLocalPositionX(self.Portrait2.transform, -571)
			LuaUtils.SetLocalPositionX(self.BabyInfo.transform, -495)
			LuaTweenUtils.TweenAlpha(self.Portrait2Container.transform, 1, 0.5, 0)
			LuaTweenUtils.TweenScale(self.Portrait2Container.transform, Vector3(1, 1, 1), Vector3(0.7 , 0.7, 1), 0)

			local portraitTexture2 = self.Portrait2Container.transform:Find("PortraitTexture"):GetComponent(typeof(CUITexture))
			local portraint2 = "ynpc001b"
			portraitTexture2:LoadNPCPortrait(portraint2, false)

			CommonDefs.AddOnClickListener(self.Portrait2.gameObject, DelegateFactory.Action_GameObject(function (go)
				Gac2Gas.RequestInspectInfo()
			end), false)
		else
			self:ClearPortraitClick(self.Portrait2.gameObject)
		end

	elseif BabyMgr.Inst.BabyDictionary.Count == 2 then
		-- 玩家身上有两个宝宝，选中出生早的宝宝，初始化两个头像框
		self.Portrait1:SetActive(true)
		self.Portrait2:SetActive(true)
		LuaUtils.SetLocalPositionX(self.Portrait1.transform, -678)
		LuaUtils.SetLocalPositionX(self.Portrait2.transform, -571)
		LuaUtils.SetLocalPositionX(self.BabyInfo.transform, -495)

		local babyIdList = BabyMgr.Inst:GetBabyIdList()

		self.LeftBabyId = babyIdList[0]
		self.RightBabyId = babyIdList[1]

		local portraitTexture1 = self.Portrait1Container.transform:Find("PortraitTexture"):GetComponent(typeof(CUITexture))
		local portraint1 = BabyMgr.Inst:GetBabyPortraitById(self.LeftBabyId)
		portraitTexture1:LoadNPCPortrait(portraint1, false)

		local otherBaby = BabyMgr.Inst:GetTheOtherBaby()
		self.OtherBaby = otherBaby
		local portraitTexture2 = self.Portrait2Container.transform:Find("PortraitTexture"):GetComponent(typeof(CUITexture))
		local portraint2 = BabyMgr.Inst:GetBabyPortraitById(self.RightBabyId)
		portraitTexture2:LoadNPCPortrait(portraint2, false)

		self:ProcessTwoBabyPortrait()
	end

	self:InitBabyInfo()
end

function LuaSchedulePlanView2023:ProcessTwoBabyPortrait()
	if self.LeftBabyId == self.SelectedBaby.Id then

		LuaTweenUtils.TweenAlpha(self.Portrait2Container.transform, 1, 0.5, 0)
		LuaTweenUtils.TweenScale(self.Portrait2Container.transform, Vector3(1, 1, 1), Vector3(0.7 , 0.7, 1), 0)

		LuaTweenUtils.TweenAlpha(self.Portrait1Container.transform, 0.5, 1, 0)
		LuaTweenUtils.TweenScale(self.Portrait1Container.transform, Vector3(0.7, 0.7, 1), Vector3(1, 1, 1), 0)

		self:ClearPortraitClick(self.Portrait1.gameObject)

		CommonDefs.AddOnClickListener(self.Portrait2.gameObject, DelegateFactory.Action_GameObject(function (go)
			self:SwitchBaby(true)
		end), false)
	else

		LuaTweenUtils.TweenAlpha(self.Portrait1Container.transform, 1, 0.5, 0)
		LuaTweenUtils.TweenScale(self.Portrait1Container.transform, Vector3(1, 1, 1), Vector3(0.7 , 0.7, 1), 0)

		LuaTweenUtils.TweenAlpha(self.Portrait2Container.transform, 0.5, 1, 0)
		LuaTweenUtils.TweenScale(self.Portrait2Container.transform, Vector3(0.7, 0.7, 1), Vector3(1, 1, 1), 0)

		self:ClearPortraitClick(self.Portrait2.gameObject)

		CommonDefs.AddOnClickListener(self.Portrait1.gameObject, DelegateFactory.Action_GameObject(function (go)
			self:SwitchBaby(false)
		end), false)
	end
end

function LuaSchedulePlanView2023:SwitchBaby(left2right)

	if self.OtherBaby.Grade < 11 then
		MessageMgr.Inst:ShowMessage("BABY_NOT_OLD_ENOUGH_OPEN_SCHEDULE", {})
		return
	end
	-- Step 1 头像大小修改
	self:SwitchPortrait(left2right)

	-- Step 2 BabyMgr处理
	local switchedBaby = BabyMgr.Inst:TrySwitchBaby()
	if switchedBaby then
		-- Step 3 更新界面信息
		self.SelectedBaby = BabyMgr.Inst:GetSelectedBaby()
		self.OtherBaby = BabyMgr.Inst:GetTheOtherBaby()
		self:InitBabyInfo()
		self:InitScheduleGrid()
		self:ProcessTwoBabyPortrait()
	end
end

function LuaSchedulePlanView2023:SwitchPortrait(left2right)
	if left2right then
		LuaTweenUtils.TweenAlpha(self.Portrait1Container.transform, 1, 0.5, 0.8)
		LuaTweenUtils.SetEase(LuaTweenUtils.TweenScale(self.Portrait1Container.transform, Vector3(1, 1, 1), Vector3(0.7 , 0.7, 1),0.8), Ease.OutBack)

		LuaTweenUtils.TweenAlpha(self.Portrait2Container.transform, 0.5, 1, 0.8)
		LuaTweenUtils.SetEase(LuaTweenUtils.TweenScale(self.Portrait2Container.transform, Vector3(0.7, 0.7, 1), Vector3(1, 1, 1), 0.8), Ease.OutBack)
	else
		LuaTweenUtils.TweenAlpha(self.Portrait2Container.transform, 1, 0.5, 0.8)
		LuaTweenUtils.SetEase(LuaTweenUtils.TweenScale(self.Portrait2Container.transform, Vector3(1, 1, 1), Vector3(0.7 , 0.7, 1),0.8), Ease.OutBack)

		LuaTweenUtils.TweenAlpha(self.Portrait1Container.transform, 0.5, 1, 0.8)
		LuaTweenUtils.SetEase(LuaTweenUtils.TweenScale(self.Portrait1Container.transform, Vector3(0.7, 0.7, 1), Vector3(1, 1, 1), 0.8), Ease.OutBack)
	end
end

function LuaSchedulePlanView2023:ClearPortraitClick(go)
	CommonDefs.AddOnClickListener(go, DelegateFactory.Action_GameObject(function (go) end), false)
end

-- 初始化宝宝信息
function LuaSchedulePlanView2023:InitBabyInfo()
	if not self.SelectedBaby then return end

	self.BabyNameLabel.text = self.SelectedBaby.Name
	self.BabyLevelLabel.text = SafeStringFormat3(LocalString.GetString("%d级"), self.SelectedBaby.Grade)
	
	g_ScriptEvent:BroadcastInLua("BabyWndSelectBaby", self.SelectedBaby.Grade)

	local grade = self.SelectedBaby.Grade
	local babyExp = Baby_BabyExp.GetData(grade)
	if babyExp then
		self.ExpProgress.value = (self.SelectedBaby.Exp / babyExp.Value)
		self.ProgressLabel.text = SafeStringFormat3(LocalString.GetString("%d/%d"), self.SelectedBaby.Exp, babyExp.Value)
	else
		self.ExpProgress.value = 1
		self.ProgressLabel.gameObject:SetActive(false)
	end

	self:UpdateGainButtonAlert()
end

function LuaSchedulePlanView2023:UpdateGainButtonAlert()
	self.GainButtonAlert:SetActive(false)

	if not self.SelectedBaby then return end
	local planRecord = self.SelectedBaby.Props.ScheduleData.PlanRecordData
	if not planRecord then return end

	local result = false

	CommonDefs.DictIterate(planRecord, DelegateFactory.Action_object_object(function(key, val)
		local receivedAward = val.ReceivedAward
		if receivedAward ~= 1 then
			result = true
		end
	end))
	self.GainButtonAlert:SetActive(result)

end

-- 初始化日程格子
function LuaSchedulePlanView2023:InitScheduleGrid()

	self.SelectedPlans = {}
	LuaBabyMgr.m_SelectedPlan = {}

	self.ScheduleItemList = {}
	self.SelectedAddPlanIndex = 0
	self.NeedItemsEnough = true

	local setting = Baby_Setting.GetData()

	CUICommonDef.ClearTransform(self.ScheduleGrid.transform)
	for i = 0, setting.ScheduleInfo.Length - 1 do
		local go = NGUITools.AddChild(self.ScheduleGrid.gameObject, self.ScheduleTemplate)
		self:InitScheduleTemplate(go.transform, i, setting.ScheduleInfo[i])
		self.ScheduleItemList[i+1] = go
		go:SetActive(true)
	end
	self.ScheduleGrid:Reposition()

	-- 设置cost
	self:UpdateNeedTili()
	-- 设置花费物品
	self:UpdateNeedItems()
	-- 设置派遣按钮
	self:UpdatePlanBtn()
	-- 设置提示
	self:UpdateScheduleHint()

end

function LuaSchedulePlanView2023:InitScheduleTemplate(go, index, scheduleTime)
	self:UpdateCell(go, index, scheduleTime)
end

-- 更新日程格子的信息
function LuaSchedulePlanView2023:UpdateCell(go, index, scheduleTime)

	local BG = go.transform:Find("BG"):GetComponent(typeof(UISprite))
	local PeriodLabel = go.transform:Find("PeriodLabel"):GetComponent(typeof(UILabel))
	local StatusSprite = go.transform:Find("StatusSprite"):GetComponent(typeof(UISprite))
	local StatusLabel = go.transform:Find("StatusSprite/Label"):GetComponent(typeof(UILabel))
	local AddPlanSprite = go.transform:Find("AddPlanSprite").gameObject
	local PlanLabel = go.transform:Find("PlanLabel"):GetComponent(typeof(UILabel))
	local DeniedSprite = go.transform:Find("DeniedSprite").gameObject
	local NeedTiLiLabel = go.transform:Find("NeedTiLiLabel"):GetComponent(typeof(UILabel))
	local PlannedTexture = go.transform:Find("PlannedTexture"):GetComponent(typeof(UITexture))

	BG.spriteName = "common_btn_07_normal"
	BG.alpha = 1
	StatusSprite.gameObject:SetActive(false)
	AddPlanSprite:SetActive(false)
	PlanLabel.text = ""
	DeniedSprite:SetActive(false)
	PeriodLabel.alpha = 1
	NeedTiLiLabel.gameObject:SetActive(false)
	PlannedTexture.gameObject:SetActive(false)

	PeriodLabel.text = SafeStringFormat3("%s:00-%s:59", tostring(scheduleTime), tostring(scheduleTime))

	local todayPlanInfo = self.SelectedBaby.Props.ScheduleData.TodayPlanData

	local now = CServerTimeMgr.Inst:GetZone8Time()
	local scheduleStartTime = CreateFromClass(DateTime, now.Year, now.Month, now.Day, scheduleTime, 0 ,0)
	-- 如果10点以后，则跟明天时间比较
	if now.Hour >= 22 then
		scheduleStartTime = scheduleStartTime:AddDays(1)
	end
	

	if not CommonDefs.DictContains_LuaCall(todayPlanInfo, index+1) then
		-- 没有安排对应的plan 根据对应的时间判断看是否可进行安排

		-- plan的结束时间比现在早
		if DateTime.Compare(scheduleStartTime, now) < 0 then
			DeniedSprite:SetActive(true)
			PeriodLabel.alpha = 0.2
			local onDeniedSpriteClicked = function ()
				MessageMgr.Inst:ShowMessage("BABY_SCHEDULE_PLAN_TIME_IS_OVER", {})
			end
			CommonDefs.AddOnClickListener(DeniedSprite, DelegateFactory.Action_GameObject(onDeniedSpriteClicked), false)
		else
			-- 如果已经选择了对应的任务
			if self.SelectedPlans[index+1] then
				self:UpdateCellPlan(go, index+1, self.SelectedPlans[index+1])
			else
				AddPlanSprite:SetActive(true)
				local onAddPlanClicked = function (go)
					self:OnAddPlanClicked(go, index+1)
				end
				CommonDefs.AddOnClickListener(AddPlanSprite, DelegateFactory.Action_GameObject(onAddPlanClicked), false)
			end
		end

	else
		local planInfo = todayPlanInfo[index+1] -- index+1为从1开始的格子ID

		-- 已经安排了对应的plan，均不可修改
		local planId = planInfo.PlanId
		local isFinished = planInfo.Finished == 1 -- 1代表已经完成
		local finishTime = planInfo.FinishTime

		local babyPlan = Baby_Plan.GetData(planId)
		if not babyPlan then return end

		PlanLabel.text = SafeStringFormat3("[%s]%s[-]", self:GetColor(babyPlan.Color), babyPlan.Name)
		DeniedSprite:SetActive(false)
		PeriodLabel.alpha = 1
		AddPlanSprite:SetActive(false)
		BG.alpha = 0
		PlannedTexture.gameObject:SetActive(true)
		PlannedTexture.alpha = 0.7

		local finishDate = CServerTimeMgr.ConvertTimeStampToZone8Time(finishTime)
		local spriteColor = Color.white
		local statusText = ""
		if isFinished then -- 已完成，完成时间比现在早
			spriteColor = NGUIText.ParseColor24("5fcd00", 0)
			statusText = LocalString.GetString("完成")
			StatusSprite.color = spriteColor
			StatusLabel.text = statusText
			StatusSprite.gameObject:SetActive(true)
		elseif finishTime - CServerTimeMgr.Inst.timeStamp >= 0 then
			if finishTime - CServerTimeMgr.Inst.timeStamp < 3600  then
				spriteColor = NGUIText.ParseColor24("E8D222", 0)
				statusText = LocalString.GetString("进行中")
			else
				spriteColor = NGUIText.ParseColor24("595959", 0)
				statusText = LocalString.GetString("未开始")
			end
			StatusSprite.color = spriteColor
			StatusLabel.text = statusText
			StatusSprite.gameObject:SetActive(true)
		end
		CommonDefs.AddOnClickListener(go.gameObject, DelegateFactory.Action_GameObject(function (go)
			self:UpdatePlanDetailInfo(index+1, go, planId, true, planInfo)
		end), false)
	end

end

-- 显示日程任务的详情
-- 新增参数：isPlanned 是否是一个已经派遣的任务
-- 新增参数：planInfo 如果已经派遣，是否已经完成，完成时间的数据
function LuaSchedulePlanView2023:UpdatePlanDetailInfo(index, go, planId, isPlanned, planInfo)
	LuaBabyMgr.m_SelectedDetailPlanId = planId
	LuaBabyMgr.m_AddTargetColumn = math.floor((index - 1) / 4)
	LuaBabyMgr.ShowBabySchedulePlanDetail(go.transform, index, isPlanned, planInfo)
end

-- 更新某个选中schedule的计划
function LuaSchedulePlanView2023:UpdateCellPlan(go, index, planId)

	local BG = go.transform:Find("BG"):GetComponent(typeof(UISprite))
	local StatusSprite = go.transform:Find("StatusSprite"):GetComponent(typeof(UISprite))
	local StatusLabel = go.transform:Find("StatusSprite/Label"):GetComponent(typeof(UILabel))
	local AddPlanSprite = go.transform:Find("AddPlanSprite").gameObject
	local PlanLabel = go.transform:Find("PlanLabel"):GetComponent(typeof(UILabel))
	local DeniedSprite = go.transform:Find("DeniedSprite").gameObject
	local NeedTiLiLabel = go.transform:Find("NeedTiLiLabel"):GetComponent(typeof(UILabel))
	local PlannedTexture = go.transform:Find("PlannedTexture"):GetComponent(typeof(UITexture))

	local babyPlan = Baby_Plan.GetData(planId)

	if not babyPlan then
		self.StatusSprite:SetActive(false)
		local onPlanClicked = function (go)

		end
		PlannedTexture.gameObject:SetActive(false)
		BG.alpha = 1
		CommonDefs.AddOnClickListener(go, DelegateFactory.Action_GameObject(onPlanClicked), false)

		return
	else
		local onPlanClicked = function (go)
			self:OnPlanCancelClicked(go, index)
		end
		BG.alpha = 0
		PlannedTexture.gameObject:SetActive(true)
		PlannedTexture.alpha = 1

		local spriteColor = NGUIText.ParseColor24("515ee1", 0)
		local statusText = LocalString.GetString("待派遣")
		StatusSprite.color = spriteColor
		StatusLabel.text = statusText
		StatusSprite.gameObject:SetActive(true)


		AddPlanSprite:SetActive(false)
		PlanLabel.text = SafeStringFormat3("[%s]%s[-]", self:GetColor(babyPlan.Color), babyPlan.Name)
		NeedTiLiLabel.gameObject:SetActive(true)
		NeedTiLiLabel.text = babyPlan.TiLiCost
		CommonDefs.AddOnClickListener(go, DelegateFactory.Action_GameObject(function (go)
			self:UpdatePlanDetailInfo(index, go, planId, false, nil)
		end), false)
	end

end

-- 弹出取消按钮
function LuaSchedulePlanView2023:OnPlanCancelClicked(go, index)
	local menuList = CreateFromClass(MakeGenericClass(List, PopupMenuItemData))
	CommonDefs.ListAdd(menuList, typeof(PopupMenuItemData), PopupMenuItemData(LocalString.GetString("取消"), DelegateFactory.Action_int(function (idx)
                self:OnCancelSelectedPlan(go, idx, index)
            end), false, nil, EnumPopupMenuItemStyle.Default))
	CPopupMenuInfoMgr.ShowPopupMenu(CommonDefs.ListToArray(menuList), go.transform, CPopupMenuInfoMgr.AlignType.Bottom, 1, nil, 350, true, 330)
end

-- 取消选中的日程派遣
function LuaSchedulePlanView2023:OnCancelSelectedPlan(go, idx, selectedIndex)
	self.SelectedPlans[selectedIndex] = nil
	local setting = Baby_Setting.GetData()
	self:UpdateCell(go, selectedIndex-1, setting.ScheduleInfo[selectedIndex-1])
	-- 取消点击背景的效果
	CommonDefs.AddOnClickListener(go, DelegateFactory.Action_GameObject(function(go) end), false)
	-- 设置cost
	self:UpdateNeedTili()
	-- 设置花费物品
	self:UpdateNeedItems()
	-- 设置派遣按钮
	self:UpdatePlanBtn()
	-- 设置提示
	self:UpdateScheduleHint()
end

function LuaSchedulePlanView2023:CancelSelectedPlan(selectedIndex)
	self.SelectedPlans[selectedIndex] = nil
	local setting = Baby_Setting.GetData()
	local go = self.ScheduleItemList[selectedIndex]
	self:UpdateCell(go, selectedIndex-1, setting.ScheduleInfo[selectedIndex-1])
	-- 取消点击背景的效果
	CommonDefs.AddOnClickListener(go, DelegateFactory.Action_GameObject(function(go) end), false)
	-- 设置cost
	self:UpdateNeedTili()
	-- 设置花费物品
	self:UpdateNeedItems()
	-- 设置派遣按钮
	self:UpdatePlanBtn()
	-- 设置提示
	self:UpdateScheduleHint()
end

function LuaSchedulePlanView2023:GetColor(index)
	if index == 1 then
		return "FFFE91"
	elseif index == 2 then
		return "519FFF"
	elseif index == 3 then
		return "FF5050"
	elseif index == 4 then
		return "C000C0"
	end
	return "FFFFFF"
end

-- 打开可选择的日程列表
function LuaSchedulePlanView2023:OnAddPlanClicked(go, index)
	-- todo 检查是否有还未进行的派遣
	local planCount = 0
 	for k, v in pairs(self.SelectedPlans) do
 		if v then
 			planCount = planCount + 1
 		end
 	end
 	if planCount >= 1 then
 		MessageMgr.Inst:ShowMessage("BABY_HAS_MORE_THAN_ONE_PLANS", {})
 		return
 	end
	self.SelectedAddPlanIndex = index
	LuaBabyMgr.m_SelectedPlan = self.SelectedPlans
	LuaBabyMgr.ShowBabySchedulePlanList(go.transform, math.floor((index - 1) / 4))
end

function LuaSchedulePlanView2023:OnSelectBabySchedulePlan(planId)
	local selectedScheduleItem = self.ScheduleItemList[self.SelectedAddPlanIndex]
	if not selectedScheduleItem then return end
	self.SelectedPlans[self.SelectedAddPlanIndex] = planId
	self:UpdateCellPlan(selectedScheduleItem, self.SelectedAddPlanIndex, planId)

	-- 设置cost
	self:UpdateNeedTili()
	-- 设置花费物品
	self:UpdateNeedItems()
	-- 设置派遣按钮
	self:UpdatePlanBtn()
	-- 设置提示
	self:UpdateScheduleHint()
end

-- 更新所需物品
function LuaSchedulePlanView2023:UpdateNeedItems()

	local KouDaiItemList = Baby_Setting.GetData().BagsId
	for i = 0, 3 do
		local KouDaiCnt = self.KouDaiRoot.transform:Find("Grid/KouDai"..i + 1 .."/Count"):GetComponent(typeof(UILabel))
		KouDaiCnt.text = CItemMgr.Inst:GetItemCount(KouDaiItemList[i])
	end
	
	local setting = Baby_Setting.GetData()
	local needItems = {}

	for i = 1, setting.ScheduleInfo.Length do
		local schedulePlan = self.SelectedPlans[i]
		if schedulePlan then
			local babyPlan = Baby_Plan.GetData(schedulePlan)
			if not needItems[babyPlan.ItemCost] then
				needItems[babyPlan.ItemCost] = 1
			else
				needItems[babyPlan.ItemCost] = needItems[babyPlan.ItemCost] + 1
			end
		end
	end

	CUICommonDef.ClearTransform(self.NeedGrid.transform)

	self.NeedItemsEnough = true
	local needJade = 0

	for itemId, count in pairs(needItems) do
		local go = NGUITools.AddChild(self.NeedGrid.gameObject, self.NeedTemplate)
		self:InitNeedItem(go, itemId, count)
		go:SetActive(true)

		local bindItemCountInBag, notBindItemCountInBag = CItemMgr.Inst:CalcItemCountInBagByTemplateId(itemId)
		local totalCount = bindItemCountInBag + notBindItemCountInBag

		self.NeedItemsEnough = self.NeedItemsEnough and (totalCount >= count)
		if totalCount < count then
			local lackCount = count - totalCount
			local mallItem = Mall_LingYuMall.GetData(itemId)
			if mallItem then
				needJade = needJade + mallItem.Jade * lackCount
			end
		end
	end

	self.NeedGrid:Reposition()
	self.LingYuCost:SetType(13, 0, true)
	self.LingYuCost:SetCost(needJade)

	local autoUseMoney = self:AutoUseLingYu()
	if self.NeedItemsEnough then
		self.NeedGrid.gameObject:SetActive(true)
		--self.NeedItemHint.gameObject:SetActive(true)
		self.LingYuCost.gameObject:SetActive(false)
	else
		if autoUseMoney then
			self.NeedGrid.gameObject:SetActive(false)
			--self.NeedItemHint.gameObject:SetActive(false)
			self.LingYuCost.gameObject:SetActive(true)
		else
			self.NeedGrid.gameObject:SetActive(true)
			--self.NeedItemHint.gameObject:SetActive(true)
			self.LingYuCost.gameObject:SetActive(false)
		end
	end
end

function LuaSchedulePlanView2023:OnExchangeBtnClicked(go)

	local data = Dialog_Dialog.GetData(34000488)
	local confirmText = ""
	if data.ChoiceConfirmMessage ~= nil and not System.String.IsNullOrEmpty(data.ChoiceConfirmMessage[0]) then
		confirmText = data.ChoiceConfirmMessage[0]
	end
	if not (confirmText == nil or confirmText == "") then
		MessageWndManager.ShowOKCancelMessage(confirmText, DelegateFactory.Action(function ()
			Gac2Gas.ExchangeBabyKouDaiLiBao()
		end), nil, nil, nil, false)
	end
end

function LuaSchedulePlanView2023:OnQnCheckBoxChanged(value)
	if value then
        PlayerPrefs.SetInt(self.Key_BabyPlanAutoUseLingYu, 1)
    else
        PlayerPrefs.SetInt(self.Key_BabyPlanAutoUseLingYu, 0)
    end
	self:UpdateNeedItems()
end

function LuaSchedulePlanView2023:AutoUseLingYu()
	return PlayerPrefs.GetInt(self.Key_BabyPlanAutoUseLingYu, 1) > 0
end

function LuaSchedulePlanView2023:UpdateNeedTili()
	self.QnCostAndOwnMoney:SetType(17, 0, true)
	local setting = Baby_Setting.GetData()
	local tiliCost = 0
	for i = 1, setting.ScheduleInfo.Length do
		local schedulePlan = self.SelectedPlans[i]
		if schedulePlan then
			local babyPlan = Baby_Plan.GetData(schedulePlan)
			tiliCost = tiliCost + babyPlan.TiLiCost
		end
	end
	self.QnCostAndOwnMoney:SetCost(tiliCost)
	self.AddBtn.OnClick = DelegateFactory.Action_QnButton(function (go)
		LuaBabyMgr.ShowGetBabyTiLiTip(self.SelectedBaby.Id)
	end)
end


function LuaSchedulePlanView2023:UpdatePlanBtn()
	local todayPlanInfo = self.SelectedBaby.Props.ScheduleData.TodayPlanData
	local usedTime = todayPlanInfo.Count

	local setting = Baby_Setting.GetData()
	local max = setting.DailyPlanCount

	local left = max - usedTime
	if left < 0 then
		left = 0
	end

	self.PlanBtn.Enabled = left > 0
	self.PlanBtn.Text = SafeStringFormat3(LocalString.GetString("派遣(%d/%d)"), usedTime, max)

	local onPlanBtnClicked = function (go)
		self:OnPlanBtnClicked(go)
	end
	CommonDefs.AddOnClickListener(self.PlanBtn.gameObject, DelegateFactory.Action_GameObject(onPlanBtnClicked), false)
end

function LuaSchedulePlanView2023:UpdateScheduleHint()
	local setting = Baby_Setting.GetData()

	local lastTime = setting.ScheduleInfo[setting.ScheduleInfo.Length-1]
	local now = CServerTimeMgr.Inst:GetZone8Time()
	local lastScheduleTime = CreateFromClass(DateTime, now.Year, now.Month, now.Day, lastTime, 0 ,0)
	-- 最后一个plan的开始时间比现在早
	if DateTime.Compare(lastScheduleTime, now) < 0 then
		local msg = g_MessageMgr:FormatMessage("SCHEDULE_PLAN_OVERTIME")
		self.ScheduleHint.text = msg
	else
		self.ScheduleHint.text = ""
	end	
end

function LuaSchedulePlanView2023:OnPlanBtnClicked(go)

 	local planCount = 0
 	for k, v in pairs(self.SelectedPlans) do
 		if v then
 			planCount = planCount + 1
 		end
 	end

 	-- 是否有选中的派遣
 	if planCount == 0 then
 		MessageMgr.Inst:ShowMessage("NO_BABY_PLAN_SELECTED", {})
 		return
 	end

 	-- 派遣数量是否超过剩余派遣次数
 	local todayPlanInfo = self.SelectedBaby.Props.ScheduleData.TodayPlanData
	local usedTime = todayPlanInfo.Count
	local setting = Baby_Setting.GetData()
	local max = setting.DailyPlanCount
	local left = max - usedTime
	if planCount > left then
		MessageMgr.Inst:ShowMessage("SELECT_TOO_MUCH_PLAN", {})
 		return
	end

	-- 体力检查
	if not self.QnCostAndOwnMoney.moneyEnough then
		MessageMgr.Inst:ShowMessage("BABY_TILI_NOT_ENOUGH", {})
		return
	end

	-- 派遣道具检查
	if not self.NeedItemsEnough then
		-- 是否使用灵玉
		if self:AutoUseLingYu() then
			if self.LingYuCost.moneyEnough then
				self:RequestPlanBaby(true)
				return
			else
				MessageWndManager.ShowOKCancelMessage(LocalString.GetString("灵玉不足，是否前往充值？"), DelegateFactory.Action(function () 
                    CShopMallMgr.ShowChargeWnd()
                end), nil, LocalString.GetString("充值"), nil, false)
				return
			end
		end
		MessageMgr.Inst:ShowMessage("PLAN_NEED_ITEMS_NOT_ENOUGH", {})
		return
	end

	self:RequestPlanBaby(false)

end

function LuaSchedulePlanView2023:RequestPlanBaby(useJade)
	-- 二次确认框
	local msg = MessageMgr.Inst:FormatMessage("SCHEDULE_PLAN_SECOND_COMFIRN", {})
	MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(
        function()
        	local selectedBaby = BabyMgr.Inst:GetSelectedBaby()
        	if not selectedBaby then return end
        	local planList = CreateFromClass(MakeGenericClass(List, Object))
        	for k, v in pairs(self.SelectedPlans) do
        		planList:Add(k)
        		planList:Add(v)
        	end
        	Gac2Gas.RequestPlanBaby(selectedBaby.Id, MsgPackImpl.pack(planList), useJade)
        end
    ), nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
end


function LuaSchedulePlanView2023:InitNeedItem(go, itemId, count)
	local item = Item_Item.GetData(itemId)
	if not item then return end

	local IconTexture = go.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
	local DisableSprite = go.transform:Find("DisableSprite").gameObject
	local GetHint = go.transform:Find("GetHint").gameObject
	local CountLabel = go.transform:Find("CountLabel"):GetComponent(typeof(UILabel))

	IconTexture:LoadMaterial(item.Icon)
	local bindItemCountInBag, notBindItemCountInBag = CItemMgr.Inst:CalcItemCountInBagByTemplateId(itemId)
	local totalCount = bindItemCountInBag + notBindItemCountInBag
	if totalCount < count then
		DisableSprite:SetActive(true)
		GetHint:SetActive(true)
		CountLabel.text = SafeStringFormat3("[FF0000]%d[-]/%d", totalCount,count)

		local onNeedItemClicked = function (go)
			self:OnNeedItemClicked(go, itemId)
		end
		CommonDefs.AddOnClickListener(IconTexture.gameObject, DelegateFactory.Action_GameObject(onNeedItemClicked), false)
	else
		DisableSprite:SetActive(false)
		GetHint:SetActive(false)
		CountLabel.text = SafeStringFormat3("%d/%d", totalCount, count)
	end
end

function LuaSchedulePlanView2023:OnNeedItemClicked(go, itemId)
	CItemAccessTipMgr.Inst:ShowAccessTip(nil, itemId, false, go.transform, CTooltipAlignType.Top)
end


function LuaSchedulePlanView2023:OnGainButtonClicked(go)
	CUIManager.ShowUI(CLuaUIResources.BabyScheduleGainWnd)
end

function LuaSchedulePlanView2023:OnMoreInfoClicked(go)
	MessageMgr.Inst:ShowMessage("BABY_SCHEDULE_TIPS", {})
end

function LuaSchedulePlanView2023:OnEnable()
	g_ScriptEvent:AddListener("OnSelectBabySchedulePlan", self, "OnSelectBabySchedulePlan")
	g_ScriptEvent:AddListener("SendItem", self, "UpdateNeedItems")
	g_ScriptEvent:AddListener("SetItemAt", self, "UpdateNeedItems")
	g_ScriptEvent:AddListener("RefreshBabyPlanSuccess", self, "RefreshBabyPlanSuccess")
	g_ScriptEvent:AddListener("PlayerPlanBabySuccess", self, "PlayerPlanBabySuccess")
	g_ScriptEvent:AddListener("UpdateBabyPlanAward", self, "InitBabyInfo")
	g_ScriptEvent:AddListener("BabyCancelSelectedPlan", self, "CancelSelectedPlan")
end

function LuaSchedulePlanView2023:OnDisable()
	g_ScriptEvent:RemoveListener("OnSelectBabySchedulePlan", self, "OnSelectBabySchedulePlan")
	g_ScriptEvent:RemoveListener("SendItem", self, "UpdateNeedItems")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "UpdateNeedItems")
	g_ScriptEvent:RemoveListener("RefreshBabyPlanSuccess", self, "RefreshBabyPlanSuccess")
	g_ScriptEvent:RemoveListener("PlayerPlanBabySuccess", self, "PlayerPlanBabySuccess")
	g_ScriptEvent:RemoveListener("UpdateBabyPlanAward", self, "InitBabyInfo")
	g_ScriptEvent:RemoveListener("BabyCancelSelectedPlan", self, "CancelSelectedPlan")
end

-- 玩家刷新可选择日程
function LuaSchedulePlanView2023:RefreshBabyPlanSuccess()
	local tempSelectedPlans = {}

	for idx, planId in pairs(self.SelectedPlans) do
		tempSelectedPlans[idx] = planId
	end

	for idx, planId in pairs(tempSelectedPlans) do
		local go = self.ScheduleItemList[idx]
		if go then
			self:OnCancelSelectedPlan(go, 0, idx)
		end
	end
end

function LuaSchedulePlanView2023:PlayerPlanBabySuccess(babyId)
	self:InitScheduleGrid()
	self:InitKouDai()
end

return LuaSchedulePlanView2023
