-- Auto Generated!!
local AlignType1 = import "L10.UI.CItemInfoMgr+AlignType"
local AlignType2 = import "L10.UI.CTooltip+AlignType"
local Byte = import "System.Byte"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CConstellation = import "L10.UI.CConstellation"
local CConstellationWnd = import "L10.UI.CConstellationWnd"
local CFeiShengMgr = import "L10.Game.CFeiShengMgr"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CLogMgr = import "L10.CLogMgr"
local Color = import "UnityEngine.Color"
local CommonDefs = import "L10.Game.CommonDefs"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local CXingGuanItem = import "L10.UI.CXingGuanItem"
local CXingGuanWnd = import "L10.UI.CXingGuanWnd"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local GameObject = import "UnityEngine.GameObject"
local Int32 = import "System.Int32"
local Item_Item = import "L10.Game.Item_Item"
local L10 = import "L10"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local NGUIText = import "NGUIText"
local NGUITools = import "NGUITools"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local String = import "System.String"
local StringActionKeyValuePair = import "L10.Game.StringActionKeyValuePair"
local Transform = import "UnityEngine.Transform"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local UInt32 = import "System.UInt32"
local UISprite = import "UISprite"
local Vector3 = import "UnityEngine.Vector3"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local EnumChineseDigits = import "L10.Game.EnumChineseDigits"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local EPlayerFightProp = import "L10.Game.EPlayerFightProp"
local EnumGender = import "L10.Game.EnumGender"
--local Xingguan_StarGroup = import "L10.Game.Xingguan_StarGroup"

CConstellationWnd.m_Awake_CS2LuaHook = function (this) 
    this.m_LineColor = NGUIText.ParseColor32("356092FF", 0)
    this.m_XingGuanTex.texture:ResetAndUpdateAnchors()
    this.m_CostItemInfo:SetActive(false)
    CConstellationWnd.Inst = this
    
end
CConstellationWnd.m_Init_CS2LuaHook = function (this, constellationId) 
    this:LoadDesignData()
    this.m_TabBar:ChangeTab(0)
    CLuaConstellationWnd:Init(this)
end
CConstellationWnd.m_Init_UInt32_CS2LuaHook = function (this, constellationId) 
    this:LoadDesignData()
    local data = Xingguan_StarGroup.GetData(constellationId)

    local row = 0
    if data ~= nil then
        this.m_TabBar:ChangeTab(data.Area, false)
        if this.m_ConstellationList.Count > data.Area then
            local list = this.m_ConstellationList[data.Area]
            do
                local i = 0 local cnt = list.Count
                while i < cnt do
                    if list[i].m_Id == constellationId then
                        row = i
                        break
                    end
                    i = i + 1
                end
            end
        end
        this.m_TableView:ReloadData(true, false)
        this.m_TableView:SetSelectRow(row, true)
        this.m_TableView:ScrollToRow(row)
    end

    CLuaConstellationWnd:Init(this)
end
CConstellationWnd.m_LoadDesignData_CS2LuaHook = function (this) 
    if nil == this.m_ConstellationList then
        this.m_ConstellationList = CreateFromClass(MakeGenericClass(List, MakeGenericClass(List, CConstellation)))
        this.m_AttrConstellationDict = CreateFromClass(MakeGenericClass(Dictionary, String, MakeGenericClass(List, CConstellation)))
        this.m_AttrTypePopupList = CreateFromClass(MakeGenericClass(List, PopupMenuItemData))
        CommonDefs.ListAdd(this.m_AttrTypePopupList, typeof(PopupMenuItemData), PopupMenuItemData(LocalString.GetString("全部"), MakeDelegateFromCSFunction(this.OnAttrTypeSelected, MakeGenericClass(Action1, Int32), this), false, nil, EnumPopupMenuItemStyle.Light))
        CommonDefs.DictAdd(this.m_AttrConstellationDict, typeof(String), LocalString.GetString("全部"), typeof(MakeGenericClass(List, CConstellation)), CreateFromClass(MakeGenericClass(List, CConstellation)))
        for i = 0, 4 do
            CommonDefs.ListAdd(this.m_ConstellationList, typeof(MakeGenericClass(List, CConstellation)), CreateFromClass(MakeGenericClass(List, CConstellation)))
        end
        Xingguan_StarGroup.Foreach(function (key, val) 
            if not CommonDefs.DictContains(CXingGuanWnd.Inst.m_Constellation2StarDict, typeof(UInt32), key) then
                CLogMgr.Log("This constellation no star: " .. key)
                return
            end
            local con = CreateFromClass(CConstellation)
            con.m_Name = val.NameColor
            con.m_Id = key
            con.m_XingXiu = val.Xingxiu
            con.m_Des = val.Des
            con.m_Area = val.Area
            con.m_TypeName = this.m_TypeName[val.Area]
            con.m_StarList = CommonDefs.DictGetValue(CXingGuanWnd.Inst.m_Constellation2StarDict, typeof(UInt32), key)
            con.m_Pos = val.Pos
            con.m_ImageName = val.ImagePath
            con.m_WordDesc = val.Desc
            con.m_Score = val.Score
            con.m_CoupleDesc = val.CoupleDesc
            con.m_GameEventBonus = val.GameEventBonus
            con.m_GameEventParam = val.GameEventParam
            con.m_GameStatusName = val.GameStatusName
            con.m_FightPropDesc = val.FightPropDesc
            con.m_XingmangUsed = val.XingmangUsed
            con.m_TexScale = val.TexScale
            CommonDefs.ListAdd(this.m_ConstellationList[0], typeof(CConstellation), con)
            CommonDefs.ListAdd(this.m_ConstellationList[val.Area], typeof(CConstellation), con)

            if not CommonDefs.DictContains(this.m_AttrConstellationDict, typeof(String), val.TypeGroup) then
                CommonDefs.DictAdd(this.m_AttrConstellationDict, typeof(String), val.TypeGroup, typeof(MakeGenericClass(List, CConstellation)), CreateFromClass(MakeGenericClass(List, CConstellation)))
                CommonDefs.ListAdd(this.m_AttrTypePopupList, typeof(PopupMenuItemData), PopupMenuItemData(val.TypeGroup, MakeDelegateFromCSFunction(this.OnAttrTypeSelected, MakeGenericClass(Action1, Int32), this), false, nil, EnumPopupMenuItemStyle.Light))
            end
            CommonDefs.ListAdd(CommonDefs.DictGetValue(this.m_AttrConstellationDict, typeof(String), val.TypeGroup), typeof(CConstellation), con)
            CommonDefs.ListAdd(CommonDefs.DictGetValue(this.m_AttrConstellationDict, typeof(String), LocalString.GetString("全部")), typeof(CConstellation), con)
        end)
        this.m_AllConstellationList = this.m_ConstellationList[0]
    end
end
CConstellationWnd.m_RefreshAllConstellationOrder_CS2LuaHook = function (this, index) 
    local insertPos = 0
    do
        local j = 0 local cnt = this.m_ConstellationList[index].Count
        while j < cnt do
            local lightedCount, count
            lightedCount, count = CXingGuanWnd.Inst:GetConstellationStarCount(this.m_ConstellationList[index][j].m_Id)
            if lightedCount > 0 and j ~= insertPos then
                local con = this.m_ConstellationList[index][j]
                CommonDefs.ListRemoveAt(this.m_ConstellationList[index], j)
                CommonDefs.ListInsert(this.m_ConstellationList[index], insertPos, typeof(CConstellation), con)
            end
            insertPos = insertPos + lightedCount == count and 1 or 0
            j = j + 1
        end
    end
end
CConstellationWnd.m_CalRaDiff_CS2LuaHook = function (this, ra1, ra2) 
    local diff = ra1 - ra2
    if diff > 180 then
        diff = diff - 360
    elseif diff < - 180 then
        diff = diff + 360
    end
    return diff
end
CConstellationWnd.m_OnEnable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.m_CloseBtn).onClick = MakeDelegateFromCSFunction(this.OnClickCloseButton, VoidDelegate, this)
    UIEventListener.Get(this.m_LightUpBtn).onClick = MakeDelegateFromCSFunction(this.OnLightUpBtnClick, VoidDelegate, this)
    UIEventListener.Get(this.m_ResetBtn).onClick = MakeDelegateFromCSFunction(this.OnResetConstellationClick, VoidDelegate, this)
    UIEventListener.Get(this.m_AddXingmangBtn).onClick = MakeDelegateFromCSFunction(this.OnAddXingmangBtnClick, VoidDelegate, this)
    UIEventListener.Get(this.m_ItemIcon.gameObject).onClick = MakeDelegateFromCSFunction(this.OnItemIconClick, VoidDelegate, this)
    UIEventListener.Get(this.m_AttrTypeBtn).onClick = MakeDelegateFromCSFunction(this.OnAttrTypeBtnClick, VoidDelegate, this)
    UIEventListener.Get(this.m_ShareBtn).onClick = MakeDelegateFromCSFunction(this.OnShareBtnClick, VoidDelegate, this)
    UIEventListener.Get(this.m_ViewCostBtn).onClick = MakeDelegateFromCSFunction(this.OnViewCostBtnClick, VoidDelegate, this)
    this.m_TabBar.OnTabChange = MakeDelegateFromCSFunction(this.OnTabChange, MakeGenericClass(Action2, GameObject, Int32), this)
    this.m_TableView.m_DataSource = this
    this.m_TableView.OnSelectAtRow = MakeDelegateFromCSFunction(this.OnRowSelected, MakeGenericClass(Action1, Int32), this)

    this.m_XingGuanObj:SetActive(false)
    this.m_LineObj:SetActive(false)
    this.m_SearchView.OnSearch = MakeDelegateFromCSFunction(this.OnSearchValueChanged, MakeGenericClass(Action1, String), this)
    this.m_SearchView.OnValueChanged = MakeDelegateFromCSFunction(this.OnSearchValueChanged, MakeGenericClass(Action1, String), this)

    EventManager.AddListener(EnumEventType.SyncXingguanProp, MakeDelegateFromCSFunction(this.UpdateCurrentRow, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.SendItem, MakeDelegateFromCSFunction(this.OnSendItem, MakeGenericClass(Action1, String), this))
    --数据变化
    EventManager.AddListenerInternal(EnumEventType.SetItemAt, MakeDelegateFromCSFunction(this.OnSetItemAt, MakeGenericClass(Action4, EnumItemPlace, UInt32, String, String), this))
    --数据有无变化
    g_ScriptEvent:AddListener("SwitchXingguanTemplateSuccess", CLuaConstellationWnd, "OnSwitchXingguanTemplateSuccess")
    g_ScriptEvent:AddListener("SyncXingguanProp", CLuaConstellationWnd, "RefreshTableView")

    CLuaConstellationWnd:OnEnable(this)
end
CConstellationWnd.m_hookOnDisable = function (this) 
    EventManager.RemoveListener(EnumEventType.SyncXingguanProp, MakeDelegateFromCSFunction(this.UpdateCurrentRow, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.SendItem, MakeDelegateFromCSFunction(this.OnSendItem, MakeGenericClass(Action1, String), this))
    --数据变化
    EventManager.RemoveListenerInternal(EnumEventType.SetItemAt, MakeDelegateFromCSFunction(this.OnSetItemAt, MakeGenericClass(Action4, EnumItemPlace, UInt32, String, String), this))
    --数据有无变化
    g_ScriptEvent:RemoveListener("SwitchXingguanTemplateSuccess", CLuaConstellationWnd, "OnSwitchXingguanTemplateSuccess")
    g_ScriptEvent:RemoveListener("SyncXingguanProp", CLuaConstellationWnd, "RefreshTableView")

    CLuaConstellationWnd:OnDisable(this)
end
CConstellationWnd.m_OnAttrTypeBtnClick_CS2LuaHook = function (this, go) 
    this:LoadDesignData()
    CLuaConstellationWnd.m_AttrTypeArrow.transform.localEulerAngles = Vector3(0, 0, 180)
    CPopupMenuInfoMgr.ShowPopupMenu(CommonDefs.ListToArray(this.m_AttrTypePopupList), go.transform, CPopupMenuInfoMgr.AlignType.Right, 1, DelegateFactory.Action(function () 
        CLuaConstellationWnd.m_AttrTypeArrow.transform.localEulerAngles = Vector3.zero
    end), 600, true, 296)
end
CConstellationWnd.m_OnAttrTypeSelected_CS2LuaHook = function (this, index) 
    local attrType = this.m_AttrTypePopupList[index].text
    CLuaConstellationWnd.m_AttrTypeName.text = attrType
    if not CommonDefs.DictContains(this.m_AttrConstellationDict, typeof(String), attrType) then
        return
    end
    this.m_ConstellationList[0] = CommonDefs.DictGetValue(this.m_AttrConstellationDict, typeof(String), attrType)
    this.m_TabBar:ChangeTab(0, true)
    this.m_TabIndex = 0
    --m_TableView.RefreshData(0, m_RowIndex);
    this:RefreshAllConstellationOrder(0)
    this.m_TableView:ReloadData(true, false)
    this.m_TableView:SetSelectRow(0, true)
    do
        local i = 0 local cnt = this.m_TabWidgets.Length
        while i < cnt do
            this.m_TabWidgets[i].color = i == 0 and Color.white or NGUIText.ParseColor24(this.m_TabOriginalColor, 0)
            i = i + 1
        end
    end
end
CConstellationWnd.m_OnResetConstellationClick_CS2LuaHook = function (this, go) 
    MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("Xingguan_Reset_Confirm_Msg"), DelegateFactory.Action(function () 
        if CClientMainPlayer.Inst ~= nil then
            Gac2Gas.Xingguan_PlayerRequestLightDownStars(CClientMainPlayer.Inst.Id, this.m_CurrentConstellationId, "")
        end
    end), nil, nil, nil, false)
end
CConstellationWnd.m_OnTabChange_CS2LuaHook = function (this, go, index) 
    -- For Search List
    if index == 0 then
        this.m_ConstellationList[0] = this.m_AllConstellationList
    end
    this:RefreshAllConstellationOrder(index)
    this.m_TabIndex = index
    --m_TableView.RefreshData(0, m_RowIndex);
    this.m_TableView:ReloadData(true, false)
    this.m_TableView:SetSelectRow(0, true)
    do
        local i = 0 local cnt = this.m_TabWidgets.Length
        while i < cnt do
            this.m_TabWidgets[i].color = i == index and Color.white or NGUIText.ParseColor24(this.m_TabOriginalColor, 0)
            i = i + 1
        end
    end
end
CConstellationWnd.m_ItemAt_CS2LuaHook = function (this, view, row) 
    local item = TypeAs(view:GetFromPool(0), typeof(CXingGuanItem))
    if item ~= nil then
        item:Init(this.m_ConstellationList[this.m_TabIndex][row])
    end
    return item
end
CConstellationWnd.m_OnRowSelected_CS2LuaHook = function (this, index) 
    this.m_RowIndex = index
    local con = this.m_ConstellationList[this.m_TabIndex][index]
    this.m_CurrentConstellationId = con.m_Id
    this.m_ConstellationNameLabel.text = con.m_Name
    this.m_XingXiuLabel.text = con.m_XingXiu
    this.m_DescriptionLabel.text = con.m_Des
    this.m_XingGuanTex:LoadMaterial(System.String.Format(CXingGuanWnd.IMAGE_PATTERN, con.m_ImageName))
    this.m_EffectLabel.text = con.m_WordDesc
    this.m_ScoreLabel.text = tostring(con.m_Score)
    this.m_CurrentConVisiable = true

    this:UpdateCurrentRow()
end
CConstellationWnd.m_UpdateCurrentRow_CS2LuaHook = function (this) 
    -- Update Xingmang Count
    this.m_XingmangCountLabel.text = (CFeiShengMgr.Inst.m_TotalXingmangCount - CFeiShengMgr.Inst.m_UsedXingmangCount .. "/") .. CFeiShengMgr.Inst.m_TotalXingmangCount

    local con = this.m_ConstellationList[this.m_TabIndex][this.m_RowIndex]
    local staredCount = 0
    if CClientMainPlayer.Inst ~= nil and CommonDefs.DictContains(CClientMainPlayer.Inst.PlayProp.XingguanProperty.XingguanData, typeof(Byte), this.m_CurrentConstellationId) then
        staredCount = CommonDefs.DictGetValue(CClientMainPlayer.Inst.PlayProp.XingguanProperty.XingguanData, typeof(Byte), this.m_CurrentConstellationId).StaredCount
    end
    this.m_StarNumLabel.text = (staredCount .. "/") .. tostring(con.m_StarList.Count)

    local tableItem = this.m_TableView:GetItemAtRow(this.m_RowIndex)
    -- m_TableView.GetCellByIndex(m_RowIndex);
    if tableItem ~= nil then
        local item = TypeAs(tableItem, typeof(CXingGuanItem))
        if item ~= nil then
            item:UpdateStarCount()
        end
    end

    local visiable = CXingGuanWnd.Inst:IsConstellationVisiable(con.m_Id)
    if visiable and not this.m_CurrentConVisiable then
        this.m_LightedEffectFx:LoadFx(CConstellationWnd.LightConFx)
    end
    this.m_CurrentConVisiable = visiable
    this.m_EffectLabel.color = this.m_CurrentConVisiable and CLuaConstellationWnd:GetDescColor(con) or NGUIText.ParseColor24(this.m_EffectOriginalColor, 0)
    this.m_EffectLabel.text = CLuaConstellationWnd:GetDescText(con)

    --CXingGuanWnd.Inst.UpdateConstellation(m_CurrentConstellationId);
    local default
    if staredCount < con.m_StarList.Count then
        default = con.m_StarList[staredCount]
    else
        default = nil
    end
    this:UpdateOperation(default)

    Extensions.RemoveAllChildren(this.m_XingGuanTex.transform)
    -- Init Constellation Texture
    if nil == con.m_Pos or con.m_Pos.Length ~= 4 then
        return
    end
    local totalRaDiff = this:CalRaDiff(con.m_Pos[0], con.m_Pos[2])
    local totalDecDiff = this:CalDecDiff(con.m_Pos[1], con.m_Pos[3])
    --Transform[] starObjs = new Transform[con.m_StarList.Count];
    local key2Trans = CreateFromClass(MakeGenericClass(Dictionary, UInt32, Transform))

    this.m_XingGuanTex.texture.height = CLuaConstellationWnd.m_TexSize * con.m_TexScale
    this.m_XingGuanTex.texture.width = CLuaConstellationWnd.m_TexSize * con.m_TexScale
    
    do
        local i = 0 local cnt = con.m_StarList.Count
        while i < cnt do
            local star = con.m_StarList[i]
            local raDiff = this:CalRaDiff(con.m_Pos[0], star.m_Ra)
            local decDiff = this:CalDecDiff(con.m_Pos[1], star.m_Dec)
            local go = NGUITools.AddChild(this.m_XingGuanTex.gameObject, this.m_StarObj)
            go:SetActive(true)
            go.transform.localPosition = Vector3(raDiff / totalRaDiff * this.m_XingGuanTex.texture.width / 2, decDiff / totalDecDiff * this.m_XingGuanTex.texture.height / 2)
            CommonDefs.DictAdd(key2Trans, typeof(UInt32), star.m_Id, typeof(Transform), go.transform)

            local sprite = nil
            if not System.String.IsNullOrEmpty(star.m_Color) then
                sprite = CommonDefs.GetComponent_GameObject_Type(go, typeof(UISprite))
                if sprite ~= nil then
                    sprite.color = NGUIText.ParseColor24(star.m_Color, 1)
                end
            end
            if not CXingGuanWnd.Inst:IsStarLighted(star) then
                if nil == sprite then
                    sprite = CommonDefs.GetComponent_GameObject_Type(go, typeof(UISprite))
                end
                sprite.alpha = CXingGuanWnd.HALF_ALPHA
            else
                this:ShowStarFx(go.transform)
            end
            local label = CommonDefs.GetComponentsInChildren_GameObject_Type_Boolean(go, typeof(UILabel), true)
            if label ~= nil then
                label[0].gameObject:SetActive(true)
                label[0].text = star.m_Name
            end
            i = i + 1
        end
    end
    do
        local i = 0 local cnt = con.m_StarList.Count
        while i < cnt do
            local continue
            repeat
                local star = con.m_StarList[i]
                if nil == star.m_LineTo or star.m_LineTo.Length <= 0 then
                    continue = true
                    break
                end
                local cur = CommonDefs.DictGetValue(key2Trans, typeof(UInt32), star.m_Id)
                do
                    local j = 0 local len = star.m_LineTo.Length
                    while j < len do
                        local continue
                        repeat
                            if not CommonDefs.DictContains(key2Trans, typeof(UInt32), star.m_LineTo[j]) then
                                CLogMgr.LogError((("LineTo Error: " .. star.m_LineTo[j]) .. " star not in ") .. con.m_Name)
                                continue = true
                                break
                            end
                            local trans = this:DrawLine(cur, CommonDefs.DictGetValue(key2Trans, typeof(UInt32), star.m_LineTo[j]), CXingGuanWnd.Inst:IsStarLighted(star) and CXingGuanWnd.Inst:IsStarLighted(star.m_LineTo[j]))
                            continue = true
                        until 1
                        if not continue then
                            break
                        end
                        j = j + 1
                    end
                end
                continue = true
            until 1
            if not continue then
                break
            end
            i = i + 1
        end
    end

    CLuaConstellationWnd:RefreshTotalCost()
end
CConstellationWnd.m_UpdateOperation_CS2LuaHook = function (this, star) 
    if nil == star then
        this.m_OperateRoot:SetActive(false)
        return
    end
    this.m_OperateRoot:SetActive(true)
    if star.m_ItemCost ~= nil and star.m_ItemCost.Length == 2 and CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.PlayProp ~= nil and not CClientMainPlayer.Inst.PlayProp.XingguanProperty.StarItemConsumed:GetBit(star.m_Id - 999) then
        this.m_ItemIcon.gameObject:SetActive(true)
        this.m_ItemCostId = star.m_ItemCost[0]
        this.m_ItemCostNum = star.m_ItemCost[1]
        this:UpdateCount()
        local data = Item_Item.GetData(star.m_ItemCost[0])
        if data ~= nil then
            this.m_ItemIcon:LoadMaterial(data.Icon)
        end
    else
        this.m_ItemCostId = 0
        this.m_ItemIcon.gameObject:SetActive(false)
    end
    this.m_XingmangCtrl:SetCost(star.m_XingmangCost)
end
CConstellationWnd.m_OnSearchValueChanged_CS2LuaHook = function (this, value) 
    -- erase button
    if System.String.IsNullOrEmpty(value) then
        this.m_TabBar:ChangeTab(0, false)
        return
    end
    local searchList = CreateFromClass(MakeGenericClass(List, CConstellation))
    do
        local i = 0 local cnt = this.m_AllConstellationList.Count
        while i < cnt do
            if (string.find(this.m_AllConstellationList[i].m_Name, value, 1, true) ~= nil) or (string.find(this.m_AllConstellationList[i].m_WordDesc, value, 1, true) ~= nil) then
                CommonDefs.ListAdd(searchList, typeof(CConstellation), this.m_AllConstellationList[i])
            end
            i = i + 1
        end
    end
    if searchList.Count == 0 then
        CommonDefs.ListAdd(searchList, typeof(CConstellation), this.m_AllConstellationList[0])
    end

    if searchList.Count >= 0 then
        this.m_ConstellationList[0] = searchList
        this.m_TabBar:ChangeTab(0, true)
        this.m_TabIndex = 0
        --m_TableView.RefreshData(0, m_RowIndex);
        this:RefreshAllConstellationOrder(0)
        --m_TableView.LoadData(0, true);
        this.m_TableView:ReloadData(true, false)
        this.m_TableView:SetSelectRow(0, true)
        do
            local i = 0 local cnt = this.m_TabWidgets.Length
            while i < cnt do
                this.m_TabWidgets[i].color = i == 0 and Color.white or NGUIText.ParseColor24(this.m_TabOriginalColor, 0)
                i = i + 1
            end
        end
    end
end
CConstellationWnd.m_OnItemIconClick_CS2LuaHook = function (this, go) 
    if this.m_ItemEnough then
        CItemInfoMgr.ShowLinkItemTemplateInfo(this.m_ItemCostId, false, nil, AlignType1.Default, 0, 0, 0, 0)
    else
        CItemInfoMgr.ShowLinkItemTemplateInfo(this.m_ItemCostId, false, this, AlignType1.Default, 0, 0, 0, 0)
    end
end
CConstellationWnd.m_OnSendItem_CS2LuaHook = function (this, itemId) 
    if this.m_ItemCostId <= 0 then
        return
    end
    local item = CItemMgr.Inst:GetById(itemId)
    if item ~= nil and this.m_ItemCostId == item.TemplateId then
        this:UpdateCount()
    end
end
CConstellationWnd.m_OnSetItemAt_CS2LuaHook = function (this, place, position, oldItemId, newItemId) 
    if oldItemId == nil and newItemId == nil then
        return
    end
    local default
    if oldItemId ~= nil then
        default = oldItemId
    else
        default = newItemId
    end
    local itemId = default
    this:OnSendItem(itemId)
end
CConstellationWnd.m_UpdateCount_CS2LuaHook = function (this) 
    local itemCount = CItemMgr.Inst:GetItemCount(this.m_ItemCostId)
    this.m_ItemEnough = itemCount >= this.m_ItemCostNum
    this.m_ItemGetObj:SetActive(not this.m_ItemEnough)
    if this.m_ItemEnough then
        this.m_ItemCntLabel.text = (itemCount .. "/") .. this.m_ItemCostNum
    else
        this.m_ItemCntLabel.text = (("[ff0000]" .. itemCount) .. "[-]/") .. this.m_ItemCostNum
    end
end
CConstellationWnd.m_GetActionPairs_CS2LuaHook = function (this, itemId, templateId) 
    local putinTrade = CreateFromClass(StringActionKeyValuePair, LocalString.GetString("获取"), DelegateFactory.Action(function () 
        CItemAccessListMgr.Inst:ShowItemAccessInfo(this.m_ItemCostId, false, this.transform, AlignType2.Right)
    end))
    local actionPairs = CreateFromClass(MakeGenericClass(List, StringActionKeyValuePair))
    CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), putinTrade)
    return actionPairs
end
CConstellationWnd.m_GetXingGuanItem_CS2LuaHook = function (this) 
    if this.m_TableView.Count > 0 then
        local ret = this.m_TableView:GetItemAtRow(0).gameObject
        if ret ~= nil then
            return ret
        end
        if L10.Game.Guide.CGuideMgr.Inst:IsInPhase(L10.Game.Guide.GuideDefine.Phase_FeiShengXingGuan) then
            L10.Game.Guide.CGuideMgr.Inst:EndCurrentPhase()
        end
    end
    return nil
end
CConstellationWnd.m_GetLightUpButton_CS2LuaHook = function (this) 
    if L10.Game.Guide.CGuideMgr.Inst:IsInPhase(53) then
        if not this.m_LightUpBtn.activeInHierarchy then
            L10.Game.Guide.CGuideMgr.Inst:EndCurrentPhase()
        end
    end
    return this.m_LightUpBtn
end

CConstellationWnd.m_hookOnClickCloseButton = function (this) 
    this.gameObject:SetActive(false)
    CXingGuanWnd.Inst.m_CanOperate = true
    g_ScriptEvent:BroadcastInLua("XingguanConstellationWndClose")
end

CLuaConstellationWnd = {}
CLuaConstellationWnd.m_Wnd = nil
CLuaConstellationWnd.m_TemplateSwitchBtn = nil
CLuaConstellationWnd.m_TemplateSwitchName = nil
CLuaConstellationWnd.m_TemplateSwitchArrow = nil
CLuaConstellationWnd.m_TemplateSwitchList = nil
CLuaConstellationWnd.m_ItemTemplate = nil
CLuaConstellationWnd.m_Grid = nil
CLuaConstellationWnd.m_ScrollView = nil
CLuaConstellationWnd.m_ResetTemplateBtn = nil
CLuaConstellationWnd.m_TotalCost = nil
CLuaConstellationWnd.m_TotalDesc = nil
CLuaConstellationWnd.m_AttrTypeArrow = nil
CLuaConstellationWnd.m_AttrTypeName = nil
CLuaConstellationWnd.m_StarGroupCoupleWorld = nil
CLuaConstellationWnd.m_TexSize = 512
CLuaConstellationWnd.m_ItemsGo = {}

function CLuaConstellationWnd:Init(this)
    self.m_Wnd = this

    self.m_TotalCost = self.m_Wnd.transform:Find("Description/Cost").gameObject
    self.m_TotalDesc = self.m_Wnd.transform:Find("Description/Cost/CostLab"):GetComponent(typeof(UILabel))
    self.m_ResetTemplateBtn = self.m_Wnd.transform:Find("ResetTemplateBtn").gameObject
    self.m_AttrTypeArrow = self.m_Wnd.transform:Find("AttrTypeBtn/Arrow").gameObject
    self.m_AttrTypeName = self.m_Wnd.transform:Find("AttrTypeBtn/Input/Label"):GetComponent(typeof(UILabel))

    UIEventListener.Get(self.m_ResetTemplateBtn).onClick = LuaUtils.VoidDelegate(function(go)
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("XingGuan_Clear_All_Msg"), DelegateFactory.Action(function () 
            Gac2Gas.Xingguan_PlayerRequestClearStars(CClientMainPlayer.Inst.Id, "")
        end), nil, nil, nil, false)
    end)
    
    self:RefreshTotalCost()
    self:TemplateSwitchInit()
end

function CLuaConstellationWnd:RefreshTotalCost()
    if self.m_Wnd == nil then
        return
    end
    local con = self.m_Wnd.m_ConstellationList[self.m_Wnd.m_TabIndex][self.m_Wnd.m_RowIndex]
    local staredCount = 0
    if CClientMainPlayer.Inst ~= nil and CommonDefs.DictContains(CClientMainPlayer.Inst.PlayProp.XingguanProperty.XingguanData, typeof(Byte), self.m_Wnd.m_CurrentConstellationId) then
        staredCount = CommonDefs.DictGetValue(CClientMainPlayer.Inst.PlayProp.XingguanProperty.XingguanData, typeof(Byte), self.m_Wnd.m_CurrentConstellationId).StaredCount
    end

    local xingmangCost = 0
    local costItemId2NumDic = {}
    local totalCount = con.m_StarList.Count
    for i = staredCount, totalCount - 1 do
        local star = con.m_StarList[i]
        if star ~= nil and star.m_ItemCost and star.m_ItemCost.Length == 2 and CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp and (not CClientMainPlayer.Inst.PlayProp.XingguanProperty.StarItemConsumed:GetBit(star.m_Id - 999)) then
            local costItemId = star.m_ItemCost[0]
            local costItemNum = star.m_ItemCost[1]
            local oriItemNum = costItemId2NumDic[costItemId] or 0
            costItemId2NumDic[costItemId] = oriItemNum + costItemNum
        end
        if star ~= nil then
            xingmangCost = xingmangCost + star.m_XingmangCost
        end
    end

    local descStr = ""
    if xingmangCost > 0 then
        descStr = LocalString.GetString("星芒") .. xingmangCost
    end
    for k, v in pairs(costItemId2NumDic) do
        local data = Item_Item.GetData(k)
        if data then
            local itemStr = data.Name .. v
            descStr = descStr .. " + " .. itemStr
        end
    end

    self.m_TotalCost:SetActive(descStr ~= "")
    self.m_TotalDesc.text = descStr
end

function CLuaConstellationWnd:TemplateSwitchInit()
    self.m_TemplateSwitchBtn = self.m_Wnd.transform:Find("TemplateSwitch").gameObject
    self.m_TemplateSwitchName = self.m_Wnd.transform:Find("TemplateSwitch/NameLab"):GetComponent(typeof(UILabel))
    self.m_TemplateSwitchArrow = self.m_Wnd.transform:Find("TemplateSwitch/Arrow").gameObject
    self.m_TemplateSwitchList = self.m_Wnd.transform:Find("TemplateSwitch/List").gameObject
    self.m_ItemTemplate = FindChild(self.m_TemplateSwitchBtn.transform,"ItemTemplate").gameObject
    self.m_Grid = FindChild(self.m_TemplateSwitchBtn.transform,"Grid"):GetComponent(typeof(UIGrid))
    self.m_ScrollView = FindChild(self.m_TemplateSwitchBtn.transform,"ScrollView"):GetComponent(typeof(CUIRestrictScrollView))
    self.m_ItemTemplate:SetActive(false)
    self.m_TemplateSwitchList:SetActive(false)

    UIEventListener.Get(self.m_TemplateSwitchBtn).onClick=LuaUtils.VoidDelegate(function(go)
        self.m_TemplateSwitchList:SetActive(not self.m_TemplateSwitchList.activeSelf)
        Extensions.SetLocalRotationZ(self.m_TemplateSwitchArrow.transform, self.m_TemplateSwitchList.activeSelf and 0 or 180)
        if self.m_TemplateSwitchList.activeSelf then
            self:TemplateSwitchInitList()
        end
    end)

    self:TemplateSwitchRefresh()
end


function CLuaConstellationWnd:TemplateSwitchRefresh()
    local curTemplateId = CClientMainPlayer.Inst.PlayProp.XingguanProperty.ActiveTemplateId
    self.m_TemplateSwitchName.text = SafeStringFormat3(LocalString.GetString("模板%s"), EnumChineseDigits.GetDigit(curTemplateId + 1))
end

function CLuaConstellationWnd:TemplateSwitchInitList()
    if nil == CClientMainPlayer.Inst or nil == CClientMainPlayer.Inst.PlayProp then
        return
    end

    if self.m_ItemsGo then
        for i, v in ipairs(self.m_ItemsGo) do
            GameObject.Destroy(v)
        end
    end
    self.m_ItemsGo = {}
    local i = 0
    while i < Xingguan_Setting.GetData().XingguanTempletNum do
        if i ~= CClientMainPlayer.Inst.PlayProp.XingguanProperty.ActiveTemplateId then
            local unlocked = i == 0 and true or CClientMainPlayer.Inst.PlayProp.XingguanProperty.TemplateUnlocked:GetBit(i)
            
            if unlocked then
                local item = NGUITools.AddChild(self.m_Grid.gameObject, self.m_ItemTemplate)
                table.insert(self.m_ItemsGo, item)
                item:SetActive(true)
                FindChild(item.transform,"Content"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("模板%s"), EnumChineseDigits.GetDigit(1 + i))
                
                local index = i
                UIEventListener.Get(item).onClick = LuaUtils.VoidDelegate(function(go)
                    self:TemplateSwitchOnClick(go, index)
                end)
            end
        end
        
        i = i + 1
    end

    self.m_Grid:Reposition()
    self.m_ScrollView:ResetPosition()
end

function CLuaConstellationWnd:TemplateSwitchOnClick(go, index)
    if CClientMainPlayer.Inst == nil then
        return 
    end
    self.m_TemplateSwitchList:SetActive(false)
    Extensions.SetLocalRotationZ(self.m_TemplateSwitchArrow.transform, 0)
    local playerId = CClientMainPlayer.Inst.Id
    Gac2Gas.Xingguan_PlayerRequestSwitchTemplate(playerId, index, "")
end

function CLuaConstellationWnd:OnSwitchXingguanTemplateSuccess(templateId)
    self.m_TemplateSwitchList:SetActive(false)
    Extensions.SetLocalRotationZ(self.m_TemplateSwitchArrow.transform, 0)
    self:TemplateSwitchRefresh()
    self.m_Wnd.m_TableView:ReloadData(true, false)
end

function CLuaConstellationWnd:OnEnable(this)
end

function CLuaConstellationWnd:OnDisable(this)
    self.m_Wnd = nil
end

function CLuaConstellationWnd:GetDescColor(con)
    return self:IsPass(con) and Color.green or Color.red
end

function CLuaConstellationWnd:IsPass(con)
    local isPass = true

    if not System.String.IsNullOrEmpty(con.m_GameStatusName) then
        isPass = self:ProcessGameStatus(con.m_GameStatusName)
    elseif not System.String.IsNullOrEmpty(con.m_FightPropDesc) and CClientMainPlayer.Inst then
        local fightPropName, paramStr = string.match(con.m_FightPropDesc, "([^;]+),(%d+)")
        local fightProp = CClientMainPlayer.Inst.FightProp
        local curVal = tonumber(fightProp:GetParam(EPlayerFightProp[fightPropName]))
        local targetVal = tonumber(paramStr)
        isPass = curVal >= targetVal
    elseif con.m_GameEventBonus == "Equipment_SpecialLevelLimit" then
        local xingmangUsed = tonumber(g_LuaUtil:StrSplit(con.m_GameEventParam,",")[1])
        isPass = (CFeiShengMgr.Inst.m_UsedXingmangCount >= xingmangUsed and 
        CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp.FeiShengData.FeiShengLevel >= Xingguan_Setting.GetData().EquipmentLevelLimitFeiShengLevelLimit)
    elseif con.m_XingmangUsed ~= 0 then
        isPass = CFeiShengMgr.Inst.m_UsedXingmangCount >= con.m_XingmangUsed
    end

    return isPass
end

function CLuaConstellationWnd:ProcessGameStatus(gameStatus)
    if gameStatus == "BothHandWeapon" or gameStatus == "SingleHandWeapon" then
        if CClientMainPlayer.Inst then
            local itemProp = CClientMainPlayer.Inst.ItemProp
            local itemId = itemProp:GetItemAt(EnumItemPlace.Body, EnumBodyPosition_lua.Weapon)
            if not System.String.IsNullOrEmpty(itemId) then
                local item = CItemMgr.Inst:GetById(itemId)
                local isBothHand =  EquipmentTemplate_Equip.GetData(item.TemplateId).BothHand == 1
                
                if gameStatus == "BothHandWeapon" then
                    return isBothHand
                elseif gameStatus == "SingleHandWeapon" then
                    return not isBothHand
                end
            end
        end
    elseif gameStatus == "GenderFemale" or gameStatus == "GenderMale" then
        -- if CClientMainPlayer.Inst then
        --     local isFemale = CClientMainPlayer.Inst.Gender == EnumGender.Female

        --     if gameStatus == "GenderFemale" then
        --         return isFemale
        --     elseif gameStatus == "GenderMale" then
        --         return not isFemale
        --     end
        -- end
        return true
    end

    return false
end

function CLuaConstellationWnd:GetDescText(con)
    self:TryInitCoupleWorld(con)
    local coupleId = self.m_StarGroupCoupleWorld[con.m_Id]
    if coupleId then
        local isPass = CXingGuanWnd.Inst:IsConstellationVisiable(coupleId) and CXingGuanWnd.Inst:IsConstellationVisiable(con.m_Id)
        return isPass and con.m_CoupleDesc or con.m_WordDesc
    elseif not System.String.IsNullOrEmpty(con.m_GameStatusName) and (con.m_GameStatusName == "GenderFemale" or con.m_GameStatusName == "GenderMale") then
        local isPass = false
        local gameStatus = con.m_GameStatusName
        if CClientMainPlayer.Inst then
            local isFemale = CClientMainPlayer.Inst.Gender == EnumGender.Female
            if gameStatus == "GenderFemale" then
                isPass = isFemale
            elseif gameStatus == "GenderMale" then
                isPass = not isFemale
            end
        end
        return isPass and con.m_CoupleDesc or con.m_WordDesc
    end
    return con.m_WordDesc
end

function CLuaConstellationWnd:TryInitCoupleWorld(con)
    if self.m_StarGroupCoupleWorld == nil then
        self.m_StarGroupCoupleWorld = {}
        Xingguan_StarGroupCoupleWorld.Foreach(function (key, val)
            if val.StarGroupID and val.StarGroupID.Length == 2 then
                local one = val.StarGroupID[0]
                local two = val.StarGroupID[1]
                self.m_StarGroupCoupleWorld[one] = two
                self.m_StarGroupCoupleWorld[two] = one
            end
        end)
    end
end

function CLuaConstellationWnd:RefreshTableView()
    self.m_Wnd.m_TableView:ReloadData(false, true)
end

function CLuaConstellationWnd:OnXingGuanWndDestroy()
    self.m_ItemsGo = {}
end
