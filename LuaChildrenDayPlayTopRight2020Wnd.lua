local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local Color = import "UnityEngine.Color"
local CUIResources = import "L10.UI.CUIResources"
local CTopAndRightTipWnd=import "L10.UI.CTopAndRightTipWnd"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"


LuaChildrenDayPlayTopRight2020Wnd = class()
RegistChildComponent(LuaChildrenDayPlayTopRight2020Wnd,"rightBtn", GameObject)
RegistChildComponent(LuaChildrenDayPlayTopRight2020Wnd,"node", GameObject)

function LuaChildrenDayPlayTopRight2020Wnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateChildrenDayPlayForceScoreInfo2020", self, "UpdateInfo")
end

function LuaChildrenDayPlayTopRight2020Wnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateChildrenDayPlayForceScoreInfo2020", self, "UpdateInfo")
end

function LuaChildrenDayPlayTopRight2020Wnd:Init()
	UIEventListener.Get(self.rightBtn).onClick=LuaUtils.VoidDelegate(function(go)
		LuaUtils.SetLocalRotation(self.rightBtn.transform,0,0,0)
		CTopAndRightTipWnd.showPackage = false
		CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
	end)

	local node1 = self.node.transform:Find('tipNode1').gameObject
	local node2 = self.node.transform:Find('tipNode2').gameObject
	local node3 = self.node.transform:Find('tipNode3').gameObject
	local node4 = self.node.transform:Find('tipNode4').gameObject
	local node5 = self.node.transform:Find('tipNode5').gameObject
	local node1Table = {node1.transform:Find('text'):GetComponent(typeof(UILabel)),node1.transform:Find('num'):GetComponent(typeof(UILabel))}
	local node2Table = {node2.transform:Find('text'):GetComponent(typeof(UILabel)),node2.transform:Find('num'):GetComponent(typeof(UILabel))}
	local node3Table = {node3.transform:Find('text'):GetComponent(typeof(UILabel)),node3.transform:Find('num'):GetComponent(typeof(UILabel))}
	local node4Table = {node4.transform:Find('text'):GetComponent(typeof(UILabel)),node4.transform:Find('num'):GetComponent(typeof(UILabel))}
	local node5Table = {node5.transform:Find('text'):GetComponent(typeof(UILabel)),node5.transform:Find('num'):GetComponent(typeof(UILabel))}
	self.nodeTable = {node1Table,node2Table,node3Table,node4Table,node5Table}
	self.saveColor = node1Table[1].color
	self.selfColor = Color.green
	self:UpdateInfo()
end

function LuaChildrenDayPlayTopRight2020Wnd:UpdateInfo()
	if not self.nodeTable then
		return
	end
	local num = 5
	for i=1,num do
		if LuaChildrenDay2020Mgr.gamePlayerInfo and LuaChildrenDay2020Mgr.gamePlayerInfo[i] then
			self.nodeTable[i][1].text = LuaChildrenDay2020Mgr.gamePlayerInfo[i].name
			self.nodeTable[i][2].text = 'x' .. LuaChildrenDay2020Mgr.gamePlayerInfo[i].playerLive
			local labelColor = self.saveColor
			if LuaChildrenDay2020Mgr.gamePlayerInfo[i].playerId == CClientMainPlayer.Inst.Id then
				labelColor = self.selfColor
			end

			if LuaChildrenDay2020Mgr.gamePlayerInfo[i].playerLive > 0 then
				self.nodeTable[i][1].color = Color(labelColor.r,labelColor.g,labelColor.b,1)
				self.nodeTable[i][2].color = Color(labelColor.r,labelColor.g,labelColor.b,1)
			else
				self.nodeTable[i][1].color = Color(labelColor.r,labelColor.g,labelColor.b,0.3)
				self.nodeTable[i][2].color = Color(labelColor.r,labelColor.g,labelColor.b,0.3)
			end
		else
			self.nodeTable[i][1].text = ''
			self.nodeTable[i][2].text = ''
		end
	end
end

return LuaChildrenDayPlayTopRight2020Wnd
