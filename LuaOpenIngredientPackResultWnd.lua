local DelegateFactory  = import "DelegateFactory"
local UITable = import "UITable"
local GameObject = import "UnityEngine.GameObject"
local Ease = import "DG.Tweening.Ease"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"

LuaOpenIngredientPackResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaOpenIngredientPackResultWnd, "Table", "Table", UITable)
RegistChildComponent(LuaOpenIngredientPackResultWnd, "Item", "Item", GameObject)
RegistChildComponent(LuaOpenIngredientPackResultWnd, "Button", "Button", GameObject)
RegistChildComponent(LuaOpenIngredientPackResultWnd, "RootPanel", "RootPanel", UIPanel)
--@endregion RegistChildComponent end

function LuaOpenIngredientPackResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	UIEventListener.Get(self.Button.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnButtonClick()
	end)

    --@endregion EventBind end
end

function LuaOpenIngredientPackResultWnd:Init()
	print(self.RootPanel)
	LuaCookBookMgr:InitMyFoodIngredientData()
	self.Item.gameObject:SetActive(false)
	Extensions.RemoveAllChildren(self.Table.transform)
	for i = 1,#LuaCookBookMgr.m_OpenIngredientPackResult do
		local t = LuaCookBookMgr.m_OpenIngredientPackResult[i]
		local data = LuaCookBookMgr.m_MyFoodIngredientData[t.ingredientId]
		local obj = NGUITools.AddChild(self.Table.gameObject, self.Item.gameObject)
		obj.gameObject:SetActive(true)
		local iconPath = SafeStringFormat3("UI/Texture/Item_Other/Material/%s.mat",data.data.Icon)
		obj.transform:Find("IconTexture"):GetComponent(typeof(CUITexture)):LoadMaterial(iconPath)
		obj.transform:Find("NumLabel"):GetComponent(typeof(UILabel)).text = data.data.Name
		obj.transform:Find("New").gameObject:SetActive(t.isNew)
		UIEventListener.Get(obj.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
			self:OnItemClick(data)
		end)
	end
	self.Table:Reposition()
end

function LuaOpenIngredientPackResultWnd:OnDisable()
	LuaTweenUtils.DOKill(self.transform, false)
end

--@region UIEvent

function LuaOpenIngredientPackResultWnd:OnButtonClick()
	local panel = self.RootPanel
	local tweener = LuaTweenUtils.TweenFloat(1, 0, 0.3, function(val)
        panel.alpha = val
    end)
    LuaTweenUtils.SetEase(tweener, Ease.OutQuint)
	LuaTweenUtils.SetTarget(tweener,self.transform)
	LuaTweenUtils.OnComplete(tweener, function ()
		CUIManager.CloseUI(CLuaUIResources.OpenIngredientPackResultWnd)
		g_ScriptEvent:BroadcastInLua("OnOpenIngredientPackResultWndClose")
	end)
end

--@endregion UIEvent

function LuaOpenIngredientPackResultWnd:OnItemClick(data)
	CItemInfoMgr.ShowLinkItemTemplateInfo(data.data.ItemId,false, nil, AlignType.Top, 0, 0, 0, 0)
end
