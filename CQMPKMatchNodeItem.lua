-- Auto Generated!!
local CQMPKMatchNodeItem = import "L10.UI.CQMPKMatchNodeItem"
local Vector3 = import "UnityEngine.Vector3"
CQMPKMatchNodeItem.m_refreshStatus_CS2LuaHook = function (this, showPrev) 
    if this.m_ZhanduiId > 0 and this.Level ~= 0 then
        this.BGSprite.spriteName = "common_thumb_orange"
    elseif this.m_ZhanduiId < 0 and (this.Level ~= 0 and this.m_CurrentStage == this.Level) then
        this.BGSprite.spriteName = "fightingspiritwnd_attack"
    else
        this.BGSprite.spriteName = "common_thumb"
    end
    if showPrev then
        this:showPreviewNodeInfo()
    end
end
CQMPKMatchNodeItem.m_showPreviewNodeInfo_CS2LuaHook = function (this) 
    local index = - 1
    do
        local i = 0
        while i < this.PrevNode.Length do
            if this.PrevNode[i].m_ZhanduiId == this.m_ZhanduiId and this.m_ZhanduiId > 0 then
                this.PrevNode[i]:SetLineGreen()
                index = i
            else
                this.PrevNode[i]:SetLineNormal()
            end
            i = i + 1
        end
    end
    -- index >= 0 有比赛结果
    if index >= 0 then
        this:ShowName(this.PrevNode[index].m_Name)
    else
        this:ShowName("")
    end
    -- 小组决赛
    if this.Level == 3 then
        if this.m_ZhanduiId > 0 then
            this:SetLineGreen()
        else
            this:SetLineNormal()
        end
    end
end
CQMPKMatchNodeItem.m_SetLineGreen_CS2LuaHook = function (this) 
    do
        local i = 0
        while i < this.SelfLines.Length do
            this.SelfLines[i].color = CQMPKMatchNodeItem.green
            i = i + 1
        end
    end
end
CQMPKMatchNodeItem.m_SetLineNormal_CS2LuaHook = function (this) 
    do
        local i = 0
        while i < this.SelfLines.Length do
            this.SelfLines[i].color = CQMPKMatchNodeItem.normalColor
            i = i + 1
        end
    end
end
CQMPKMatchNodeItem.m_showSelf_CS2LuaHook = function (this) 
    this.SelfNode.localPosition = Vector3(this:GetXCoord(this.Level, this.Index), this:GetYCoord(this.Level, this.Index), 0)
    if this.Level >= 3 then
        --[[
                BGSprite.enabled = false;
                nameLabel.enabled = false;
                 * ]]
    end
end
CQMPKMatchNodeItem.m_showLines_CS2LuaHook = function (this) 
    local totalCount = math.floor((16 / 2 ^ this.Level))
    local selfX = this:GetXCoord(this.Level, this.Index)
    local selfY = this:GetYCoord(this.Level, this.Index)
    local nextY = this:GetYCoord(this.Level + 1, math.floor(this.Index / 2))
    local nextX = this:GetXCoord(this.Level + 1, math.floor(this.Index / 2))
    if this.Index < math.floor(totalCount / 2) then
        if this.Level == 0 then
            this.SelfLines[0].leftAnchor.absolute = - 444
        else
            this.SelfLines[0].leftAnchor.absolute = selfX
        end
        this.SelfLines[0].rightAnchor.absolute = nextX
        this.SelfLines[0].topAnchor.absolute = selfY + 2
        this.SelfLines[0].bottomAnchor.absolute = selfY - 2
    else
        if this.Level == 0 then
            this.SelfLines[0].rightAnchor.absolute = 444
        else
            this.SelfLines[0].rightAnchor.absolute = selfX
        end
        this.SelfLines[0].leftAnchor.absolute = nextX
        this.SelfLines[0].topAnchor.absolute = selfY + 2
        this.SelfLines[0].bottomAnchor.absolute = selfY - 2
    end
    this.SelfLines[0]:ResetAndUpdateAnchors()

    if nextY > selfY then
        this.SelfLines[1].topAnchor.absolute = nextY + 2
        this.SelfLines[1].bottomAnchor.absolute = selfY - 2
    else
        this.SelfLines[1].topAnchor.absolute = selfY + 2
        this.SelfLines[1].bottomAnchor.absolute = nextY - 2
    end

    this.SelfLines[1].leftAnchor.absolute = nextX - 2
    this.SelfLines[1].rightAnchor.absolute = nextX + 2
    this.SelfLines[1]:ResetAndUpdateAnchors()
end
CQMPKMatchNodeItem.m_GetXCoord_CS2LuaHook = function (this, level, index) 

    local x = 0
    repeat
        local default = level
        if default == 0 then
            x = 740
            break
        elseif default == 1 then
            x = 370
            break
        elseif default == 2 then
            x = 270
            break
        elseif default == 3 then
            x = 200
            break
        end
    until 1
    local totalCount = math.floor((16 / 2 ^ level))
    if index < math.floor(totalCount / 2) then
        return - x
    else
        return x
    end
end
CQMPKMatchNodeItem.m_GetYCoord_CS2LuaHook = function (this, level, index) 
    if level == 0 then
        return math.floor(((3.5 - index % 8) * CQMPKMatchNodeItem.level0YInterval))
    end
    if level > 3 then
        return 0
    end
    return math.floor((this:GetYCoord(level - 1, index * 2) + this:GetYCoord(level - 1, index * 2 + 1)) / 2)
end
