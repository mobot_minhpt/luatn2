CLuaRemoteDouDiZhuInviteDlg = class()

RegistClassMember(CLuaRemoteDouDiZhuInviteDlg,"m_Tick")
RegistClassMember(CLuaRemoteDouDiZhuInviteDlg,"m_CountdownLabel")
RegistClassMember(CLuaRemoteDouDiZhuInviteDlg,"m_LeftTime")

CLuaRemoteDouDiZhuInviteDlg.m_Info = {
    containerId = 0, 
    gasId = 0, 
    serverId = 0, 
    inviterId = 0, 
    inviterName = "",
    index = 0,
}

function CLuaRemoteDouDiZhuInviteDlg:Init()
    local acceptButton = self.transform:Find("Anchor/AcceptButton").gameObject
    UIEventListener.Get(acceptButton).onClick = DelegateFactory.VoidDelegate(function(go)
        local info = CLuaRemoteDouDiZhuInviteDlg.m_Info
        Gac2Gas.AcceptJoinRemoteDouDiZhu(info.containerId, info.gasId, info.serverId, info.inviterId, info.index)
        CUIManager.CloseUI(CLuaUIResources.RemoteDouDiZhuInviteDlg)
    end)
    local refuseButton = self.transform:Find("Anchor/RefuseButton").gameObject
    UIEventListener.Get(refuseButton).onClick = DelegateFactory.VoidDelegate(function(go)
        local info = CLuaRemoteDouDiZhuInviteDlg.m_Info
        Gac2Gas.RefuseJoinRemoteDouDiZhu(info.containerId, info.gasId, info.serverId, info.inviterId, info.index)
        CUIManager.CloseUI(CLuaUIResources.RemoteDouDiZhuInviteDlg)
    end)
    local label = self.transform:Find("Anchor/InfoLabel"):GetComponent(typeof(UILabel))
    label.text = g_MessageMgr:FormatMessage("RemoteDouDiZhu_Invite_Msg",CLuaRemoteDouDiZhuInviteDlg.m_Info.inviterName)


    self.m_CountdownLabel = refuseButton.transform:Find("Label"):GetComponent(typeof(UILabel))
    self.m_CountdownLabel.text =SafeStringFormat3(LocalString.GetString("拒绝(%d)"),60)

    self:StartCountdown(60)
end


function CLuaRemoteDouDiZhuInviteDlg:StartCountdown(leftTime)
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
    end

    self.m_LeftTime=leftTime
    self.m_Tick = RegisterTickWithDuration(function ()
		if not self:OnTick() then 
			UnRegisterTick(self.m_Tick)
            self.m_Tick=nil
            CUIManager.CloseUI(CLuaUIResources.RemoteDouDiZhuInviteDlg)
        end
    end,1000,(leftTime)*1000)
end

function CLuaRemoteDouDiZhuInviteDlg:OnTick()
	if self.m_CountdownLabel == nil then return false end

    self.m_LeftTime = self.m_LeftTime - 1

    if self.m_LeftTime>0 then
        self.m_CountdownLabel.text =SafeStringFormat3(LocalString.GetString("拒绝(%d)"),self.m_LeftTime)
        return true
    end
    self.m_CountdownLabel.text = LocalString.GetString("拒绝")
    return false
end

function CLuaRemoteDouDiZhuInviteDlg:OnDestroy()
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick=nil
    end
end