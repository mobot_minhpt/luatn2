local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CNumberKeyBoardMgr = import "CNumberKeyBoardMgr"
local AlignType = import "L10.UI.CTooltip+AlignType"
local QnCheckBox = import "L10.UI.QnCheckBox"

LuaBusinessInvestmentWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaBusinessInvestmentWnd, "InvestmentLab", "InvestmentLab", UILabel)
RegistChildComponent(LuaBusinessInvestmentWnd, "InCheckmark", "InCheckmark", GameObject)
RegistChildComponent(LuaBusinessInvestmentWnd, "OutCheckmark", "OutCheckmark", GameObject)
RegistChildComponent(LuaBusinessInvestmentWnd, "QnIncreseAndDecreaseButton", "QnIncreseAndDecreaseButton", GameObject)
RegistChildComponent(LuaBusinessInvestmentWnd, "Btn", "Btn", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaBusinessInvestmentWnd, "m_DecreaseButton")
RegistClassMember(LuaBusinessInvestmentWnd, "m_IncreaseButton")
RegistClassMember(LuaBusinessInvestmentWnd, "m_InputLab")
RegistClassMember(LuaBusinessInvestmentWnd, "m_InputNum")
RegistClassMember(LuaBusinessInvestmentWnd, "m_InputNumMax")
RegistClassMember(LuaBusinessInvestmentWnd, "m_InBtn")
RegistClassMember(LuaBusinessInvestmentWnd, "m_OutBtn")
RegistClassMember(LuaBusinessInvestmentWnd, "m_IsIn")
RegistClassMember(LuaBusinessInvestmentWnd, "m_TipLab")

function LuaBusinessInvestmentWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.m_DecreaseButton   = self.QnIncreseAndDecreaseButton.transform:Find("DecreaseButton"):GetComponent(typeof(QnButton))
    self.m_IncreaseButton   = self.QnIncreseAndDecreaseButton.transform:Find("IncreaseButton"):GetComponent(typeof(QnButton))
    self.m_InputLab         = self.QnIncreseAndDecreaseButton.transform:Find("InputLab"):GetComponent(typeof(UILabel))
    self.m_InBtn            = self.transform:Find("Anchor/Middle/SwitchButtons/InBtn"):GetComponent(typeof(QnCheckBox))
    self.m_OutBtn           = self.transform:Find("Anchor/Middle/SwitchButtons/OutBtn"):GetComponent(typeof(QnCheckBox))
    self.m_TipLab           = self.transform:Find("Anchor/Top/Tip"):GetComponent(typeof(UILabel))

    UIEventListener.Get(self.m_DecreaseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnDecreaseButtonClick()
	end)

	UIEventListener.Get(self.m_IncreaseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnIncreaseButtonClick()
	end)

	UIEventListener.Get(self.m_InputLab.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnInputLabClick()
	end)

    UIEventListener.Get(self.Btn).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOKBtnClick()
	end)

    self.m_InBtn.OnClick = DelegateFactory.Action_QnButton(function(btn)
        self:SwitchToInInvestment()
    end)

    self.m_OutBtn.OnClick = DelegateFactory.Action_QnButton(function(btn)
        self:SwitchToOutInvestment()
    end)

    self.m_TipLab.text = g_MessageMgr:FormatMessage("BUSINESS_INVESTMENT_TIP", tonumber(SafeStringFormat3("%.2f", Business_Setting.GetData().InvestRate * 100)))
end

function LuaBusinessInvestmentWnd:Start()
    self:SwitchToInInvestment()
    self.InvestmentLab.text = LuaBusinessMgr.m_Investment
end

--@region UIEvent

--@endregion UIEvent


function LuaBusinessInvestmentWnd:SwitchToInInvestment()
    self.m_IsIn = true
    self.m_InputNum = math.min(math.floor(LuaBusinessMgr.m_OwnMoney / 2), System.Int32.MaxValue - 2)
    self.m_InputNumMax = math.min(LuaBusinessMgr.m_OwnMoney, System.Int32.MaxValue - 2)
    self.m_InputNumMax = math.max(0, self.m_InputNumMax)
    self.InCheckmark:SetActive(true)
    self.OutCheckmark:SetActive(false)
    self.m_InputLab.text = self.m_InputNum
    self:SetInputNum(self.m_InputNum)
end

function LuaBusinessInvestmentWnd:SwitchToOutInvestment()
    self.m_IsIn = false
    self.m_InputNum = math.min(LuaBusinessMgr.m_Investment, System.Int32.MaxValue - 2)
    self.m_InputNumMax = math.min(LuaBusinessMgr.m_Investment, System.Int32.MaxValue - 2)
    self.m_InputNumMax = math.max(0, self.m_InputNumMax)
    self.InCheckmark:SetActive(false)
    self.OutCheckmark:SetActive(true)
    self.m_InputLab.text = self.m_InputNum
    self:SetInputNum(self.m_InputNum)
end

function LuaBusinessInvestmentWnd:OnDecreaseButtonClick()
    self:SetInputNum(self.m_InputNum - 1)
end

function LuaBusinessInvestmentWnd:OnIncreaseButtonClick()
    self:SetInputNum(self.m_InputNum + 1)
end

function LuaBusinessInvestmentWnd:OnInputLabClick()
    CNumberKeyBoardMgr.ShowNumberKeyBoardWithRange(0, self.m_InputNumMax, self.m_InputNum, 100, DelegateFactory.Action_int(function (val) 
		self.m_InputLab.text = val
    end), DelegateFactory.Action_int(function (val) 
        self:SetInputNum(val)
    end), self.m_InputLab, AlignType.Right, true)
end

function LuaBusinessInvestmentWnd:SetInputNum(num)
	if num < 0 then
		num = 0
	end

	if num > self.m_InputNumMax then
		num = self.m_InputNumMax
	end

	self.m_InputNum = num
	self.m_InputLab.text = num

	self.m_IncreaseButton.Enabled = self.m_InputNumMax - num > 0
	self.m_DecreaseButton.Enabled = num > 0
	if self.m_InputNumMax - num < 0 then
		if self.m_IncreaseTick ~= nil then
			UnRegisterTick(self.m_IncreaseTick)
			self.m_IncreaseTick = nil
		end
	end

	if num < 0 then
		if self.m_DecreaseTick ~= nil then
			UnRegisterTick(self.m_DecreaseTick)
			self.m_DecreaseTick = nil
		end
	end
end

function LuaBusinessInvestmentWnd:OnOKBtnClick()
    if self.m_IsIn then
        Gac2Gas.PutMoneyInBank(self.m_InputNum)
    else 
        Gac2Gas.GetMoneyFromBank(self.m_InputNum)
    end
    CUIManager.CloseUI(CLuaUIResources.BusinessInvestmentWnd)
end