local CUITexture=import "L10.UI.CUITexture"

CLuaManYueJiuChooseBabyWnd=class()
RegistClassMember(CLuaManYueJiuChooseBabyWnd,"m_OkButton")


CLuaManYueJiuChooseBabyWnd.m_BabyInfo=nil
function CLuaManYueJiuChooseBabyWnd:Init()
    local tfs={}
    table.insert( tfs,FindChild(self.transform,"Left") )
    table.insert( tfs,FindChild(self.transform,"Right") )
    if CLuaManYueJiuChooseBabyWnd.m_BabyInfo then
        for i,info in ipairs(CLuaManYueJiuChooseBabyWnd.m_BabyInfo) do
            local tf=tfs[i]
            local icon = FindChild(tf,"Texture"):GetComponent(typeof(CUITexture))
            icon:LoadNPCPortrait(LuaBabyMgr.GetBabyPortrait(info.status,info.gender,info.hairstyle), false)
            local nameLabel=FindChild(tf,"NameLabel"):GetComponent(typeof(UILabel))
            nameLabel.text=info.name.." "..SafeStringFormat3(LocalString.GetString("%d级"),info.grade)

            UIEventListener.Get(tf.gameObject).onClick=DelegateFactory.VoidDelegate(function(go)
                CUIManager.CloseUI(CLuaUIResources.ManYueJiuChooseBabyWnd)
                Gac2Gas.RequestApplyManYueJiuForBaby(info.babyId)
            end)
        end
    end
end

