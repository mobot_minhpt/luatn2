-- Auto Generated!!
local CBWZQAttendWnd = import "L10.UI.CBWZQAttendWnd"
local CBWZQMgr = import "L10.Game.CBWZQMgr"
local DelegateFactory = import "DelegateFactory"
local UIEventListener = import "UIEventListener"
CBWZQAttendWnd.m_Init_CS2LuaHook = function (this) 
    this.defaultText.text = System.String.Format(this.defaultText.text, CBWZQMgr.Inst.dingJinMin)

    this.numberInput:SetMinMax(CBWZQMgr.Inst.dingJinMin, CBWZQMgr.Inst.dingJinMax, 1)

    UIEventListener.Get(this.submitBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        local number = this.numberInput:GetValue()
        if number > 0 then
            Gac2Gas.RequestCallZhaoQin(number)
            this:Close()
        end
    end)

    UIEventListener.Get(this.cancelBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        this:Close()
    end)

    UIEventListener.Get(this.infoBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        g_MessageMgr:ShowMessage("ZHAOQIN_DEPOSIT_TIP")
    end)
end
