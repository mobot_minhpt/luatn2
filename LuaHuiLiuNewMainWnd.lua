local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local UISprite = import "UISprite"
local LocalString = import "LocalString"
local CUITexture = import "L10.UI.CUITexture"
local Extensions = import "Extensions"
local Vector3 = import "UnityEngine.Vector3"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local DelegateFactory = import "DelegateFactory"
local UIEventListener = import "UIEventListener"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local UITabBar = import "L10.UI.UITabBar"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CIMMgr = import "L10.Game.CIMMgr"
local FriendItemData = import "FriendItemData"
local UIScrollView = import "UIScrollView"
local CButton = import "L10.UI.CButton"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
local Constants = import "L10.Game.Constants"

LuaHuiLiuNewMainWnd = class()
RegistChildComponent(LuaHuiLiuNewMainWnd,"closeBtn", GameObject)
RegistChildComponent(LuaHuiLiuNewMainWnd,"Tabs", UITabBar)
RegistChildComponent(LuaHuiLiuNewMainWnd,"PlayerListView", GameObject)
RegistChildComponent(LuaHuiLiuNewMainWnd,"InfoView", GameObject)
RegistChildComponent(LuaHuiLiuNewMainWnd,"BackView", GameObject)
RegistChildComponent(LuaHuiLiuNewMainWnd,"BackPlayerListView", GameObject)
RegistChildComponent(LuaHuiLiuNewMainWnd,"EmptyView", GameObject)
RegistChildComponent(LuaHuiLiuNewMainWnd,"PlayerListEmpty", GameObject)
RegistChildComponent(LuaHuiLiuNewMainWnd,"TipBtn", GameObject)

--RegistClassMember(LuaHuiLiuNewMainWnd, "maxChooseNum")

function LuaHuiLiuNewMainWnd:Close()
	CUIManager.CloseUI(CLuaUIResources.HuiLiuNewMainWnd)
end

function LuaHuiLiuNewMainWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateZhaoHuiFriendInfo", self, "InitCanBack")
	g_ScriptEvent:AddListener("UpdateAlreadyZhaoHuiFriendInfo", self, "InitAlreadyBack")
	g_ScriptEvent:AddListener("UpdateZhaoHuiTaskInfo", self, "ZhaoHuiTaskInfoBack")
end

function LuaHuiLiuNewMainWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateZhaoHuiFriendInfo", self, "InitCanBack")
	g_ScriptEvent:RemoveListener("UpdateAlreadyZhaoHuiFriendInfo", self, "InitAlreadyBack")
	g_ScriptEvent:RemoveListener("UpdateZhaoHuiTaskInfo", self, "ZhaoHuiTaskInfoBack")
end

function LuaHuiLiuNewMainWnd:InitItemTableShow(rewardInfo,rewardFatherNode,itemTemplate,distance)
	local count = 0
	local length = #rewardInfo
	for i=1,length,2 do
		local itemTemplateId = tonumber(rewardInfo[i])
		local itemData = CItemMgr.Inst:GetItemTemplate(itemTemplateId)
		if itemData then
			local itemNum = tonumber(rewardInfo[i+1])
			local rewardNode = NGUITools.AddChild(rewardFatherNode,itemTemplate)
			rewardNode:SetActive(true)
			rewardNode.transform.localPosition = Vector3(count * distance,0,0)
			if itemNum and itemNum > 1 then
				rewardNode.transform:Find('Normal/DescLabel'):GetComponent(typeof(UILabel)).text = itemNum
			else
				rewardNode.transform:Find('Normal/DescLabel'):GetComponent(typeof(UILabel)).text = ''
			end
			rewardNode.transform:Find('Normal/IconTexture'):GetComponent(typeof(CUITexture)):LoadMaterial(itemData.Icon)
			UIEventListener.Get(rewardNode).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(rewardNode).onClick, DelegateFactory.VoidDelegate(function (go)
				CItemInfoMgr.ShowLinkItemTemplateInfo(itemTemplateId, false, nil, AlignType.Default, 0, 0, 0, 0)
			end), true)
			count = count + 1
		end
	end
end

function LuaHuiLiuNewMainWnd:InitCanBack()
	self.PlayerListView:SetActive(true)
	self.InfoView:SetActive(true)
	self.InfoView.transform:Find('defaultText').gameObject:SetActive(true)
	self.InfoView.transform:Find('backText').gameObject:SetActive(false)
	local bonusInfo = g_LuaUtil:StrSplit(HuiLiuNew_Setting.GetData().ZhaoHuiRewardsSum,',')
	Extensions.RemoveAllChildren(self.InfoView.transform:Find('ItemFather'))
	self.InfoView.transform:Find('Item').gameObject:SetActive(false)
	self:InitItemTableShow(bonusInfo,self.InfoView.transform:Find('ItemFather').gameObject,self.InfoView.transform:Find('Item').gameObject,128)

	self.BackPlayerListView:SetActive(false)
	self.BackView:SetActive(false)
	self.EmptyView:SetActive(false)

  local _scrollView = self.PlayerListView.transform:Find('ScrollView'):GetComponent(typeof(UIScrollView))
  local _table = self.PlayerListView.transform:Find('ScrollView/Grid'):GetComponent(typeof(UIGrid))
  local _templateNode = self.PlayerListView.transform:Find('TemplateNode').gameObject
  _templateNode:SetActive(false)

	Extensions.RemoveAllChildren(_table.transform)

	local count = 0
  for i,v in ipairs(LuaHuiLiuMgr.ZhaoHuiFriendList) do
		count = count + 1
    local playerId = v
    local playerInfo = CreateFromClass(FriendItemData, CIMMgr.Inst:GetBasicInfo(playerId))
    if playerInfo then
      local offlineTime = CIMMgr.Inst:GetFriendOfflineTime(playerId)
      local totalLeaveDay = math.ceil((CServerTimeMgr.Inst.timeStamp - offlineTime)/3600/24)
      local node = NGUITools.AddChild(_table.gameObject,_templateNode)
      node:SetActive(true)
      node.transform:Find('text'):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString('已离开江湖%s天'),totalLeaveDay)
      node.transform:Find('NameLabel'):GetComponent(typeof(UILabel)).text = playerInfo.name
      node.transform:Find('Border/Portrait'):GetComponent(typeof(CUITexture)):LoadNPCPortrait(playerInfo.portraitName, true)
      node.transform:Find('LevelLabel'):GetComponent(typeof(UILabel)).text = playerInfo.isInXianShenStatus and SafeStringFormat3("[c][%s]%d[-][/c]", Constants.ColorOfFeiSheng, playerInfo.level) or SafeStringFormat3("%d", playerInfo.level)
			local opBtn = node.transform:Find('OpButton').gameObject
			local onOpClick = function(go)
				Gac2Gas.RequestZhaoHuiInviteFriend(playerId)
			end
			CommonDefs.AddOnClickListener(opBtn,DelegateFactory.Action_GameObject(onOpClick),false)
			local onPlayerIconClick = function(go)
        CPlayerInfoMgr.ShowPlayerPopupMenu(playerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), CPlayerInfoMgr.AlignType.Default)
			end
			CommonDefs.AddOnClickListener(node.transform:Find('Border/Portrait').gameObject,DelegateFactory.Action_GameObject(onPlayerIconClick),false)
    end
  end

	if count == 0 then
		self.PlayerListView:SetActive(false)
		self.PlayerListEmpty:SetActive(true)
		self.PlayerListEmpty.transform:Find('text3'):GetComponent(typeof(UILabel)).text = LocalString.GetString('暂时没有可召回的好友')
	else
		self.PlayerListEmpty:SetActive(false)
	end

	_scrollView:ResetPosition()
	_table:Reposition()
end

---------------


function LuaHuiLiuNewMainWnd:InitAlreadyBack()
	self.PlayerListView:SetActive(false)
	self.InfoView:SetActive(false)
	self.BackPlayerListView:SetActive(true)
	self.BackView:SetActive(false)
	self.EmptyView:SetActive(true)

  local _scrollView = self.BackPlayerListView.transform:Find('ScrollView'):GetComponent(typeof(UIScrollView))
  local _table = self.BackPlayerListView.transform:Find('ScrollView/Grid'):GetComponent(typeof(UIGrid))
  local _templateNode = self.BackPlayerListView.transform:Find('TemplateNode').gameObject
  _templateNode:SetActive(false)

	self.cButtonSelect = nil
	Extensions.RemoveAllChildren(_table.transform)

	local count = 0

  for i,v in ipairs(LuaHuiLiuMgr.AlreadyZhaoHuiFriendList) do
		count = count + 1
    local playerId = v[1]
		local isBind = v[2]
		local taskExpireTime = v[3]
    local playerInfo = CreateFromClass(FriendItemData, CIMMgr.Inst:GetBasicInfo(playerId))
    if playerInfo then
			local taskRestSec = taskExpireTime - CServerTimeMgr.Inst.timeStamp
      local taskRestDay = math.floor(taskRestSec/3600/24)
			local taskRestHour = math.floor((taskRestSec - taskRestDay * 3600 * 24)/3600)

      local node = NGUITools.AddChild(_table.gameObject,_templateNode)
      node:SetActive(true)
			if isBind then
				if taskRestSec <= 0 then
					node.transform:Find('text'):GetComponent(typeof(UILabel)).text = LocalString.GetString('已过期')
				elseif taskRestDay > 0 then
					node.transform:Find('text'):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString('回流任务期限 %s天%s小时'),taskRestDay,taskRestHour)
				elseif taskRestHour > 0 then
					node.transform:Find('text'):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString('回流任务期限 %s小时'),taskRestHour)
				elseif taskRestSec > 0 then
					local min = math.floor(taskRestSec/60)
					local sec = math.floor(taskRestSec%60)
					node.transform:Find('text'):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString('回流任务期限 %s分%s秒'),min,sec)
				else
					node.transform:Find('text'):GetComponent(typeof(UILabel)).text = LocalString.GetString('回流任务期限 已过期')
				end
			else
				node.transform:Find('text'):GetComponent(typeof(UILabel)).text = LocalString.GetString('成功回归，快去邀请绑定吧')
			end
      node.transform:Find('NameLabel'):GetComponent(typeof(UILabel)).text = playerInfo.name
			local clickBtn = node.transform:Find('OpButton').gameObject
			local onIconClick = function(go)
				CPlayerInfoMgr.ShowPlayerPopupMenu(playerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), CPlayerInfoMgr.AlignType.Default)
			end
			CommonDefs.AddOnClickListener(clickBtn,DelegateFactory.Action_GameObject(onIconClick),false)
			local porBtn = node.transform:Find('Border/Portrait').gameObject
			CommonDefs.AddOnClickListener(porBtn,DelegateFactory.Action_GameObject(onIconClick),false)

      node.transform:Find('Border/Portrait'):GetComponent(typeof(CUITexture)):LoadNPCPortrait(playerInfo.portraitName, false)
      node.transform:Find('LevelLabel'):GetComponent(typeof(UILabel)).text = playerInfo.isInXianShenStatus and SafeStringFormat3("[c][%s]%d[-][/c]", Constants.ColorOfFeiSheng, playerInfo.level) or SafeStringFormat3("%d", playerInfo.level)
			local opBtn = node
			local cbutton = node:GetComponent(typeof(CButton))
			local onNodeClick = function(go)
				self.EmptyView:SetActive(false)
				self.BackView:SetActive(false)
				if isBind then
					self.InfoView:SetActive(false)
					Gac2Gas.QueryZhaoHuiTaskInfo(playerId)
				else
					self.InfoView:SetActive(true)
					self.InfoView.transform:Find('defaultText').gameObject:SetActive(false)
					self.InfoView.transform:Find('backText').gameObject:SetActive(true)
					local bonusInfo = g_LuaUtil:StrSplit(HuiLiuNew_Setting.GetData().ZhaoHuiRewardsSum,',')
					Extensions.RemoveAllChildren(self.InfoView.transform:Find('ItemFather'))
					self.InfoView.transform:Find('Item').gameObject:SetActive(false)
					self:InitItemTableShow(bonusInfo,self.InfoView.transform:Find('ItemFather').gameObject,self.InfoView.transform:Find('Item').gameObject,128)
					local inviterBindBtn = self.InfoView.transform:Find('backText/OpButton').gameObject
					inviterBindBtn:GetComponent(typeof(CButton)).Enabled = true
					inviterBindBtn.transform:Find('text'):GetComponent(typeof(UILabel)).text = LocalString.GetString('邀请绑定')
					--RequestHuiLiuBindWithMe
					local onClick = function(go)
						Gac2Gas.RequestHuiLiuBindWithMe(playerId)
						go:GetComponent(typeof(CButton)).Enabled = false
						go.transform:Find('text'):GetComponent(typeof(UILabel)).text = LocalString.GetString('已发送邀请')
					end
					CommonDefs.AddOnClickListener(inviterBindBtn,DelegateFactory.Action_GameObject(onClick),false)
				end

				if not self.cButtonSelect then
					self.cButtonSelect = cbutton
				elseif self.cButtonSelect then
					self.cButtonSelect.Selected = false
				end
				cbutton.Selected = true
				self.cButtonSelect = cbutton
			end

			CommonDefs.AddOnClickListener(opBtn,DelegateFactory.Action_GameObject(onNodeClick),false)
    end
  end

	if count == 0 then
		self.EmptyView:SetActive(false)
		self.BackPlayerListView:SetActive(false)
		self.PlayerListEmpty:SetActive(true)
		self.PlayerListEmpty.transform:Find('text3'):GetComponent(typeof(UILabel)).text = LocalString.GetString('暂时没有已召回的好友')

		self.InfoView:SetActive(true)
		self.InfoView.transform:Find('defaultText').gameObject:SetActive(true)
		self.InfoView.transform:Find('backText').gameObject:SetActive(false)
	else
		self.PlayerListEmpty:SetActive(false)
	end

	_scrollView:ResetPosition()
	_table:Reposition()
end

function LuaHuiLiuNewMainWnd:UpdateTaskInfoSingleNode(node,v)
	local taskId = v[1]
	local progress = v[2]
	local isRewarded = v[3]
	local stage = v[4] or 1

	local taskInfo = HuiLiuNew_ZhaoHuiTask.GetData(taskId)
	if taskInfo then
		node.transform:Find('text1'):GetComponent(typeof(UILabel)).text = taskInfo.Name
		local targetTable = g_LuaUtil:StrSplit(taskInfo.Target,',')
		local rewardTable = g_LuaUtil:StrSplit(taskInfo.Reward,';')

		local targetNum = tonumber(targetTable[stage])
		local rewardInfo = g_LuaUtil:StrSplit(rewardTable[stage],',')

		local percent = progress/targetNum
		if percent > 1 then
			percent = 1
		end

		node.transform:Find('status').gameObject:SetActive(true)

		if isRewarded then
			node.transform:Find('status/OpButton').gameObject:SetActive(false)
			node.transform:Find('status/done').gameObject:SetActive(true)
			node.transform:Find('status/doing').gameObject:SetActive(false)
		else
			if percent >= 1 then
				local receiveBtn = node.transform:Find('status/OpButton').gameObject
				receiveBtn:SetActive(true)
				node.transform:Find('status/done').gameObject:SetActive(false)
				node.transform:Find('status/doing').gameObject:SetActive(false)

				local onClick = function(go)
					Gac2Gas.RequestGetZhaoHuiTaskReward(LuaHuiLiuMgr.AlreadyZhaoHuiFriendId,taskId)
				end
				CommonDefs.AddOnClickListener(receiveBtn,DelegateFactory.Action_GameObject(onClick),false)
			else
				node.transform:Find('status/OpButton').gameObject:SetActive(false)
				node.transform:Find('status/done').gameObject:SetActive(false)
				node.transform:Find('status/doing').gameObject:SetActive(true)
				local restTime = LuaHuiLiuMgr.AlreadyZhaoHuiTaskExpireTime - CServerTimeMgr.Inst.timeStamp
				if restTime <= 0 then
					node.transform:Find('status/doing'):GetComponent(typeof(UILabel)).text = LocalString.GetString('未完成')
				else
					node.transform:Find('status/doing'):GetComponent(typeof(UILabel)).text = LocalString.GetString('进行中')
				end
			end
		end

		node.transform:Find('text2'):GetComponent(typeof(UILabel)).text = progress .. '/' .. targetNum
		node.transform:Find('slider'):GetComponent(typeof(UISprite)).width = percent * 295

		local itemTemplate = node.transform:Find('Item1').gameObject
		local rewardFatherNode = node.transform:Find('ItemNode').gameObject
		itemTemplate:SetActive(false)

		local count = 0
		local length = #rewardInfo
		for i=1,length,2 do
			local itemTemplateId = tonumber(rewardInfo[i])
			local itemData = CItemMgr.Inst:GetItemTemplate(itemTemplateId)
			local itemNum = tonumber(rewardInfo[i+1])
			local rewardNode = NGUITools.AddChild(rewardFatherNode,itemTemplate)
			rewardNode:SetActive(true)
			rewardNode.transform.localPosition = Vector3(count * 128,0,0)
			rewardNode.transform:Find('Normal/DescLabel'):GetComponent(typeof(UILabel)).text = itemNum
			rewardNode.transform:Find('Normal/IconTexture'):GetComponent(typeof(CUITexture)):LoadMaterial(itemData.Icon)

			UIEventListener.Get(rewardNode).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(rewardNode).onClick, DelegateFactory.VoidDelegate(function (go)
				CItemInfoMgr.ShowLinkItemTemplateInfo(itemTemplateId, false, nil, AlignType.Default, 0, 0, 0, 0)
			end), true)
			count = count + 1
		end

	end
end

function LuaHuiLiuNewMainWnd:ZhaoHuiTaskInfoBack()
	self.InfoView:SetActive(false)
	self.EmptyView:SetActive(false)
	self.BackView:SetActive(true)

  local _scrollView = self.BackView.transform:Find('ScrollView'):GetComponent(typeof(UIScrollView))
  local _table = self.BackView.transform:Find('ScrollView/Grid'):GetComponent(typeof(UIGrid))
  local _templateNode = self.BackView.transform:Find('TemplateNode').gameObject
  _templateNode:SetActive(false)

	Extensions.RemoveAllChildren(_table.transform)

	for i,v in ipairs(LuaHuiLiuMgr.AlreadyZhaoHuiTaskInfo) do
		local node = NGUITools.AddChild(_table.gameObject,_templateNode)
		node:SetActive(true)
		self:UpdateTaskInfoSingleNode(node,v)
	end
	_scrollView:ResetPosition()
	_table:Reposition()
end

function LuaHuiLiuNewMainWnd:OnTabChange(go, index)
  if index == 0 then
    Gac2Gas.QueryZhaoHuiFriendList()
    --self:InitCanBack()
  elseif index == 1 then
    Gac2Gas.QueryAlreadyZhaoHuiFriendList()
    --self:InitAlreadyBack()
  end
end

function LuaHuiLiuNewMainWnd:InitTab()
  self.Tabs.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
    self:OnTabChange(go, index)
  end)

  self.Tabs:ChangeTab(0, false)
end

function LuaHuiLiuNewMainWnd:Init()
	local onCloseClick = function(go)
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.closeBtn,DelegateFactory.Action_GameObject(onCloseClick),false)

	local onTipClick = function(go)
		g_MessageMgr:ShowMessage('HuiLiuMainTip')
	end
	CommonDefs.AddOnClickListener(self.TipBtn,DelegateFactory.Action_GameObject(onTipClick),false)
  self:InitTab()
end

return LuaHuiLiuNewMainWnd
