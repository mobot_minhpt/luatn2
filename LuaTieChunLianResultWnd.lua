CLuaTieChunLianResultWnd = class()


CLuaTieChunLianResultWnd.m_Score = 0

function CLuaTieChunLianResultWnd:Awake()
    local scoreLabel = self.transform:Find("ScoreLabel"):GetComponent(typeof(UILabel))
    scoreLabel.text = tostring(CLuaTieChunLianResultWnd.m_Score)

    scoreLabel.alpha = 0

    local button = self.transform:Find("Button").gameObject
    UIEventListener.Get(button).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.ShowUI(CLuaUIResources.TieChunLianRankWnd)
    end)

    LuaUtils.SetWidgetsAlpha(button.transform,0)

    LuaTweenUtils.SetDelay(LuaTweenUtils.TweenAlpha(scoreLabel.transform, 0,1,0.8),1)
    LuaTweenUtils.SetDelay(LuaTweenUtils.TweenAlpha(button.transform, 0,1,0.8),1)

end
