local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local Extensions = import "Extensions"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local DateTime = import "System.DateTime"
local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local UITexture = import "UITexture"
local CItemMgr = import "L10.Game.CItemMgr"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CUIFx = import "L10.UI.CUIFx"
local COpenEntryMgr = import "L10.Game.COpenEntryMgr"
local LuaTweenUtils = import "LuaTweenUtils"
local Vector3 = import "UnityEngine.Vector3"
local CUICommonDef = import "L10.UI.CUICommonDef"
local PathMode = import "DG.Tweening.PathMode"
local PathType = import "DG.Tweening.PathType"
local Ease = import "DG.Tweening.Ease"
local Vector4 = import "UnityEngine.Vector4"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr=import "L10.UI.CItemInfoMgr"

CLuaSpokesmanHouseZhongChouWnd = class()

RegistChildComponent(CLuaSpokesmanHouseZhongChouWnd, "m_ProgressTable","ProgressTable", UITable)
RegistChildComponent(CLuaSpokesmanHouseZhongChouWnd, "m_BigItem","BigItem", GameObject)
RegistChildComponent(CLuaSpokesmanHouseZhongChouWnd, "m_SmallItem","SmallItem", GameObject)
RegistChildComponent(CLuaSpokesmanHouseZhongChouWnd, "m_ArrowItem","ArrowItem", GameObject)
RegistChildComponent(CLuaSpokesmanHouseZhongChouWnd, "m_SpokesHomeBtn","SpokesHomeBtn", GameObject)
RegistChildComponent(CLuaSpokesmanHouseZhongChouWnd, "m_DescItem","Desc", GameObject)
RegistChildComponent(CLuaSpokesmanHouseZhongChouWnd, "m_MidDescItem","MidDesc", GameObject)
RegistChildComponent(CLuaSpokesmanHouseZhongChouWnd, "m_DescTable","DescTable", UITable)
RegistChildComponent(CLuaSpokesmanHouseZhongChouWnd, "m_ComiteLabel","ComiteLabel", UILabel)
RegistChildComponent(CLuaSpokesmanHouseZhongChouWnd, "m_Reward1","Reward1", GameObject)
RegistChildComponent(CLuaSpokesmanHouseZhongChouWnd, "m_Reward2","Reward2", GameObject)
RegistChildComponent(CLuaSpokesmanHouseZhongChouWnd, "m_Reward3","Reward3", GameObject)
RegistChildComponent(CLuaSpokesmanHouseZhongChouWnd, "m_Reward3","Reward3", GameObject)
RegistChildComponent(CLuaSpokesmanHouseZhongChouWnd, "m_Input","QnIncreseAndDecreaseButton", QnAddSubAndInputButton)
RegistChildComponent(CLuaSpokesmanHouseZhongChouWnd, "m_ComiteBtn","ComiteBtn", GameObject)
RegistChildComponent(CLuaSpokesmanHouseZhongChouWnd, "m_ItemNumLabel","ItemNumLabel", UILabel)
RegistChildComponent(CLuaSpokesmanHouseZhongChouWnd, "m_ComiteLabel","ComiteLabel", UILabel)
RegistChildComponent(CLuaSpokesmanHouseZhongChouWnd, "m_IncreaseButton","IncreaseButton", GameObject)
RegistChildComponent(CLuaSpokesmanHouseZhongChouWnd, "m_TipBtn", "TipButton", GameObject)
RegistClassMember(CLuaSpokesmanHouseZhongChouWnd,"m_CurTime")
RegistClassMember(CLuaSpokesmanHouseZhongChouWnd,"m_CurStageStartTime")
RegistClassMember(CLuaSpokesmanHouseZhongChouWnd,"m_NextStageStartTime")
RegistClassMember(CLuaSpokesmanHouseZhongChouWnd,"m_CurStageId")
RegistClassMember(CLuaSpokesmanHouseZhongChouWnd,"m_CurStageSpan")
RegistClassMember(CLuaSpokesmanHouseZhongChouWnd,"m_RewardIndex2ItemId")
RegistClassMember(CLuaSpokesmanHouseZhongChouWnd,"m_RewardTbl")
RegistClassMember(CLuaSpokesmanHouseZhongChouWnd,"m_RewardIndex2Cimited")
RegistClassMember(CLuaSpokesmanHouseZhongChouWnd,"m_HaveComitedNum")
RegistClassMember(CLuaSpokesmanHouseZhongChouWnd,"m_HouseBtnFx")

function CLuaSpokesmanHouseZhongChouWnd:Awake()
    --默认0 活动还没有开始
    self.m_CurStageId = 0
    self.m_HaveComitedNum = 0
    self.m_RewardIndex2ItemId = {}
    self.m_RewardIndex2Cimited = {}
    self.m_RewardTbl = {self.m_Reward1,self.m_Reward2,self.m_Reward3}

    self.m_BigItem:SetActive(false)
    self.m_SmallItem:SetActive(false)
    self.m_ArrowItem:SetActive(false)
    self.m_DescItem:SetActive(false)
    self.m_DescTable.gameObject:SetActive(false)
    self.m_MidDescItem:SetActive(false)
    self.m_SpokesHomeBtn:SetActive(false)

    self.m_HouseBtnFx = self.m_SpokesHomeBtn.transform:Find("FxNode"):GetComponent(typeof(CUIFx)) 
    
    UIEventListener.Get(self.m_SpokesHomeBtn).onClick  =DelegateFactory.VoidDelegate(function(go)
        if not COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.SpokesmanDonate) then
            COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.SpokesmanDonate)
            self.m_HouseBtnFx:DestroyFx()
            LuaTweenUtils.DOKill(self.m_HouseBtnFx.transform, false)
        end
        self:OnGoToLuoYunXiHouse()
    end)
    UIEventListener.Get(self.m_TipBtn).onClick  =DelegateFactory.VoidDelegate(function(go)
        g_MessageMgr:ShowMessage("Spokesman_Donate_Tips")
    end)
end

function CLuaSpokesmanHouseZhongChouWnd:OnEnable()
    g_ScriptEvent:AddListener("QueryTempPlayDataInUDResult", self, "OnQueryTempPlayDataInUDResult")
    g_ScriptEvent:AddListener("UpdateTempPlayDataWithKey", self, "OnUpdateTempPlayDataWithKey")
    g_ScriptEvent:AddListener("SendItem", self, "InitComiteView")
    g_ScriptEvent:AddListener("SetItemAt", self, "InitComiteView")
    g_ScriptEvent:AddListener("OnRequestSpokesmanHouseBtnStatus", self, "OnRefreshBtnFx")
end

function CLuaSpokesmanHouseZhongChouWnd:OnDisable()
    g_ScriptEvent:RemoveListener("QueryTempPlayDataInUDResult", self, "OnQueryTempPlayDataInUDResult")
    g_ScriptEvent:RemoveListener("UpdateTempPlayDataWithKey", self, "OnUpdateTempPlayDataWithKey")
    g_ScriptEvent:RemoveListener("SendItem", self, "InitComiteView")
    g_ScriptEvent:RemoveListener("SetItemAt", self, "InitComiteView")
    g_ScriptEvent:RemoveListener("OnRequestSpokesmanHouseBtnStatus", self, "OnRefreshBtnFx")
end

function CLuaSpokesmanHouseZhongChouWnd:OnGoToLuoYunXiHouse()
    Gac2Gas.RequestEnterSpokesmanHouse()
end

function CLuaSpokesmanHouseZhongChouWnd:Init()
    Gac2Gas.OpenDonateSpokesmanHouseWnd()
    self.m_SpokesHomeBtn.transform:Find("Label"):GetComponent(typeof(UILabel)).text = Spokesman_Setting.GetData().SpokesmanHouseName
    
    Gac2Gas.QueryTempPlayDataInUD(238)
    self:InitProgressTable()
    self:InitDescTable()
    self:InitAwardView()
    self:InitComiteView()

    if self.m_CurStageId >0 then
        if not COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.SpokesmanDonate) then
            self.m_HouseBtnFx:DestroyFx()
            LuaTweenUtils.DOKill(self.m_HouseBtnFx.transform, false)
            self.m_HouseBtnFx.transform.localPosition = Vector3(0,0,0)
            local b = NGUIMath.CalculateRelativeWidgetBounds(self.m_SpokesHomeBtn.transform)
			local waypoints = CUICommonDef.GetWayPoints(Vector4(b.min.x, b.max.y, b.size.x, b.size.y), 1, true)
			self.m_HouseBtnFx.transform.localPosition = waypoints[0]
            self.m_HouseBtnFx:LoadFx("fx/ui/prefab/UI_kuang_yellow02.prefab")
            LuaTweenUtils.DOLuaLocalPath(self.m_HouseBtnFx.transform, waypoints, 2, PathType.Linear, PathMode.Ignore, 2, - 1, Ease.Linear)
        end
    end
end

function CLuaSpokesmanHouseZhongChouWnd:InitProgressTable()
    Extensions.RemoveAllChildren(self.m_ProgressTable.transform)

    local now = CServerTimeMgr.Inst:GetZone8Time()
    self.m_CurTime = now

    local stageCount = 0
    Spokesman_Progress.ForeachKey(function (id)
        local data = Spokesman_Progress.GetData(id)
        local year, month, day, hour, min = string.match(data.Time, "(%d+)/(%d+)/(%d+) (%d+):(%d+)")
        local stageBeginTime = CreateFromClass(DateTime, year, month, day, hour, min, 0)
        if DateTime.Compare(now, stageBeginTime) >= 0 then
            if id > self.m_CurStageId then
                --进入下一个阶段
                self.m_CurStageId = id
                self.m_CurStageStartTime = stageBeginTime
            end
        end
        stageCount = stageCount + 1
    end)

    if self.m_CurStageId == 0 then--当前阶段没有家园，进度从活动开始时间开始走
        local timestr = Spokesman_Setting.GetData().StartTime
        local year, month, day, hour, min = string.match(timestr, "(%d+)/(%d+)/(%d+) (%d+):(%d+)")
        self.m_CurStageStartTime = CreateFromClass(DateTime, year, month, day, hour, min, 0)
    end

    if self.m_CurStageId + 1 <= stageCount then
        local data = Spokesman_Progress.GetData(self.m_CurStageId+1)
        local year, month, day, hour, min = string.match(data.Time, "(%d+)/(%d+)/(%d+) (%d+):(%d+)")
        self.m_NextStageStartTime = CreateFromClass(DateTime, year, month, day, hour, min, 0)       
    else
        --如果当前是最后一个阶段 进度条直接走完
        self.m_NextStageStartTime = self.m_CurStageStartTime
    end

    for id=0,stageCount,1 do--id =1
        local item
        if id == self.m_CurStageId then
            item = NGUITools.AddChild(self.m_ProgressTable.gameObject,self.m_BigItem)
        else
            item = NGUITools.AddChild(self.m_ProgressTable.gameObject,self.m_SmallItem)
        end
        item:SetActive(true)
        self:InitProgressItem(item, id)
        --添加箭头
        if id ~= stageCount then
            local arrowItem = NGUITools.AddChild(self.m_ProgressTable.gameObject,self.m_ArrowItem)
            arrowItem:SetActive(true)
        end
    end
    self.m_ProgressTable:Reposition()

    if self.m_CurStageId > 0 then
        self.m_SpokesHomeBtn:SetActive(true)
    end
end

function CLuaSpokesmanHouseZhongChouWnd:InitProgressItem(item,id)
    local icon = item.transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local progress = item.transform:Find("Progress")
    if progress then
        progress = progress:GetComponent(typeof(UITexture))
    end
    
    local data = Spokesman_PregressDes.GetData(id+1)
    local iconPath = data.Icon
    icon:LoadMaterial(iconPath)

    if id > self.m_CurStageId then
        Extensions.SetLocalPositionZ(icon.transform, -1)
    elseif id == self.m_CurStageId then
        Extensions.SetLocalPositionZ(icon.transform, 0)
        local timespanTotal = CommonDefs.op_Subtraction_DateTime_DateTime(self.m_NextStageStartTime, self.m_CurStageStartTime)
        local timespanPass = CommonDefs.op_Subtraction_DateTime_DateTime(self.m_CurTime, self.m_CurStageStartTime)
        local startHour = self.m_CurStageStartTime.Hour
        local endHour = self.m_NextStageStartTime.Hour
        local totalSpanHour = timespanTotal.TotalHours
        local passSpanHour =  timespanPass.TotalHours
        local wholeWeight = 0
        local weight = 0
        local nextHour = startHour
        local curHour = self.m_CurTime.Hour

        for passed = 1,totalSpanHour,1 do
            local data = Spokesman_SpeedPerTime.GetData(nextHour)
            wholeWeight = wholeWeight + data.Speed
            if passed <= passSpanHour then               
                weight = weight + data.Speed
            end

            nextHour = nextHour + 1
            if nextHour == 24 then
                nextHour = 0
            end
        end
        progress.fillAmount = weight/wholeWeight  
    else
        Extensions.SetLocalPositionZ(icon.transform, 0)
    end
end

function CLuaSpokesmanHouseZhongChouWnd:InitDescTable()
    Extensions.RemoveAllChildren(self.m_DescTable.transform)
    local Data = Spokesman_PregressDes.GetData(1)
    local descData = Spokesman_PregressDes.GetData(self.m_CurStageId + 1)
    if descData then
        local currentDes = descData.CurrentDes
        local unlockDes = descData.UnlockDes
        if currentDes == "" or unlockDes == "" then
            local text = currentDes == "" and unlockDes or currentDes
            local title = currentDes == "" and LocalString.GetString("即将解锁") or LocalString.GetString("当前进度")
            self.m_DescTable.gameObject:SetActive(false)
            self.m_MidDescItem:SetActive(true)
            self.m_MidDescItem.transform:GetComponent(typeof(UILabel)).text = text
            self.m_MidDescItem.transform:Find("Tittle"):GetComponent(typeof(UILabel)).text = title
            return
        end 

        if currentDes and currentDes ~= "" then
            local descGo = NGUITools.AddChild(self.m_DescTable.gameObject,self.m_DescItem)
            descGo:SetActive(true)
            descGo.transform:GetComponent(typeof(UILabel)).text = currentDes
            descGo.transform:Find("Tittle"):GetComponent(typeof(UILabel)).text = LocalString.GetString("当前进度")
        end

        if unlockDes and unlockDes ~= "" then
            local descGo = NGUITools.AddChild(self.m_DescTable.gameObject,self.m_DescItem)
            descGo:SetActive(true)
            descGo.transform:GetComponent(typeof(UILabel)).text = unlockDes
            descGo.transform:Find("Tittle"):GetComponent(typeof(UILabel)).text = LocalString.GetString("即将解锁")
        end
        self.m_DescTable.gameObject:SetActive(true)
        self.m_MidDescItem:SetActive(false)
        self.m_DescTable:Reposition()
    end
end

function CLuaSpokesmanHouseZhongChouWnd:InitAwardView()
    self.m_ComiteLabel.text = self.m_HaveComitedNum

    local settingData = Spokesman_Setting.GetData()
    local rewardInfo = settingData.SpokesmanHouseReward
    local rewardIcons = settingData.SpokesmanHouseRewardIcon

	for segment in string.gmatch(rewardInfo, "([^;]+)") do
		for num, itemId in string.gmatch(segment, "([%d-]+),([%d-]+)") do
            num = tonumber(num)
            itemId = tonumber(itemId)
            table.insert(self.m_RewardIndex2Cimited, num)
            table.insert(self.m_RewardIndex2ItemId, itemId)
		end
    end
    
    local i = 0
    for iconPath in string.gmatch(rewardIcons,"([^;]+)") do
        i = i + 1
        local icon = self.m_RewardTbl[i].transform:GetComponent(typeof(CUITexture))
        icon:LoadMaterial(iconPath)
    end

    self:InitRewardsTag()
end

function CLuaSpokesmanHouseZhongChouWnd:InitRewardsTag(rewardedSet)
    for index,reward in ipairs(self.m_RewardTbl) do
        local tag = reward.transform:Find("FinishTag").gameObject
        local label = reward.transform:Find("Label"):GetComponent(typeof(UILabel))
        local fxNode = reward.transform:Find("FxNode"):GetComponent(typeof(CUIFx))
        local finishNum = self.m_RewardIndex2Cimited[index]
        label.text = SafeStringFormat3(LocalString.GetString("提交达%d个"),finishNum)
        local itemId = self.m_RewardIndex2ItemId[index]
        if rewardedSet and self.m_HaveComitedNum >= finishNum then
            local isGotAward = bit.band(rewardedSet, bit.lshift(1, index)) ~= 0
            if isGotAward then--已经领取过了
                tag:SetActive(true)
                fxNode:DestroyFx()
                LuaTweenUtils.DOKill(fxNode.transform, false)
                UIEventListener.Get(reward).onClick = DelegateFactory.VoidDelegate(function (go) 
                    CItemInfoMgr.ShowLinkItemTemplateInfo(itemId,false,nil,AlignType.Default, 0, 0, 0, 0)
                end)
            else--等待领取
                UIEventListener.Get(reward).onClick = DelegateFactory.VoidDelegate(function (go) 
                    Gac2Gas.RequestDonateToSpokesmanAward(finishNum)
                end)
                tag:SetActive(false)
                --todo特效
                fxNode:DestroyFx()
                LuaTweenUtils.DOKill(fxNode.transform, false)
                fxNode.transform.localPosition = Vector3(0,0,0)
                local b = NGUIMath.CalculateRelativeWidgetBounds(reward.transform)
			    local waypoints = CUICommonDef.GetWayPoints(Vector4(b.min.x, b.max.y, b.size.x, b.size.y), 1, true)
			    fxNode.transform.localPosition = waypoints[0]
                fxNode:LoadFx("fx/ui/prefab/UI_kuang_yellow02.prefab")
                LuaTweenUtils.DOLuaLocalPath(fxNode.transform, waypoints, 2, PathType.Linear, PathMode.Ignore, 2, - 1, Ease.Linear)
            end
        else
            UIEventListener.Get(reward).onClick = DelegateFactory.VoidDelegate(function (go) 
                CItemInfoMgr.ShowLinkItemTemplateInfo(itemId,false,nil,AlignType.Default, 0, 0, 0, 0)
            end)
            tag:SetActive(false)
        end
    end
end

function CLuaSpokesmanHouseZhongChouWnd:InitComiteView()
    local itemId = Spokesman_Setting.GetData().SpokesmanHouseItem
    local bindCount, notbindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(itemId)
    local canComiteNum = bindCount + notbindCount--众筹道具的数量
    local maxNum = canComiteNum
    self.m_ItemNumLabel.text = SafeStringFormat3(LocalString.GetString("可提交衔云道具:%d"),canComiteNum)
    if maxNum < 1 then
        maxNum = 1
    end
    self.m_Input:SetMinMax(1, maxNum, 1)

    UIEventListener.Get(self.m_ComiteBtn).onClick = DelegateFactory.VoidDelegate(function (go) 
        local val = self.m_Input:GetValue()
        if canComiteNum <= 0 then
            local msg = g_MessageMgr:FormatMessage("Sposkesman_Donate_NotEnough")
            MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
                CLuaNPCShopInfoMgr.ShowScoreShop("GlobalSpokesmanScore")
            end),nil,LocalString.GetString("前往兑换"),LocalString.GetString("取消"),false)
        else
            Gac2Gas.DonateToSpokesmanHouse(val)
        end       
    end)

    UIEventListener.Get(self.m_IncreaseButton).onClick = DelegateFactory.VoidDelegate(function (go) 
        CLuaNPCShopInfoMgr.ShowScoreShop("GlobalSpokesmanScore")
    end)
end

function CLuaSpokesmanHouseZhongChouWnd:OnQueryTempPlayDataInUDResult(key, ud, data)
    if key ~= 238 then
        return
    end

    if not data then 
        return 
    end

    local donateValue = data[0]
    local rewardedSet = data[1]
    self:InitComiteView()
    self.m_HaveComitedNum = tonumber(donateValue)
    self:InitRewardsTag(rewardedSet)
    self.m_ComiteLabel.text = self.m_HaveComitedNum
end

function CLuaSpokesmanHouseZhongChouWnd:OnUpdateTempPlayDataWithKey(argv)
    if argv[0] ~= 238 then return end
    Gac2Gas.QueryTempPlayDataInUD(238)
end

function CLuaSpokesmanHouseZhongChouWnd:OnRefreshBtnFx(isShowBtn)
    ---- 1:show, 0: hide
    if isShowBtn == 1 then
        self.m_HouseBtnFx:DestroyFx()
        LuaTweenUtils.DOKill(self.m_HouseBtnFx.transform, false)
        self.m_HouseBtnFx.transform.localPosition = Vector3(0,0,0)
        local b = NGUIMath.CalculateRelativeWidgetBounds(self.m_SpokesHomeBtn.transform)
		local waypoints = CUICommonDef.GetWayPoints(Vector4(b.min.x, b.max.y, b.size.x, b.size.y), 1, true)
		self.m_HouseBtnFx.transform.localPosition = waypoints[0]
        self.m_HouseBtnFx:LoadFx("fx/ui/prefab/UI_kuang_yellow02.prefab")
        LuaTweenUtils.DOLuaLocalPath(self.m_HouseBtnFx.transform, waypoints, 2, PathType.Linear, PathMode.Ignore, 2, - 1, Ease.Linear)
    end
end
