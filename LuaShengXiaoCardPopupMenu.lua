local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CChatHelper=import "L10.UI.CChatHelper"
local EChatPanel=import "L10.Game.EChatPanel"

LuaShengXiaoCardPopupMenu = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaShengXiaoCardPopupMenu, "Button1", "Button1", GameObject)
RegistChildComponent(LuaShengXiaoCardPopupMenu, "Button2", "Button2", GameObject)
RegistChildComponent(LuaShengXiaoCardPopupMenu, "Button3", "Button3", GameObject)
RegistChildComponent(LuaShengXiaoCardPopupMenu, "Anchor", "Anchor", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaShengXiaoCardPopupMenu,"m_Wnd")

LuaShengXiaoCardPopupMenu.s_Position = nil
LuaShengXiaoCardPopupMenu.s_TargetId = 0
function LuaShengXiaoCardPopupMenu:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.Button1.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnButton1Click()
	end)


	
	UIEventListener.Get(self.Button2.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnButton2Click()
	end)


	
	UIEventListener.Get(self.Button3.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnButton3Click()
	end)

    --@endregion EventBind end

    self.m_Wnd=self.gameObject:GetComponent(typeof(CCommonLuaWnd))

	local t = {self.Button1,self.Button2,self.Button3}
	if LuaShengXiaoCardPopupMenu.s_TargetId>0 then
		for i=1,3 do
			t[i].transform:Find("Icon").gameObject:SetActive(true)
			t[i].transform:Find("Label").gameObject:SetActive(false)
		end
	else
		for i=1,3 do
			t[i].transform:Find("Icon").gameObject:SetActive(false)
			t[i].transform:Find("Label").gameObject:SetActive(true)
		end
	end
	
end

function LuaShengXiaoCardPopupMenu:Init()
	if LuaShengXiaoCardPopupMenu.s_Position then
		self.Anchor.transform.position = LuaShengXiaoCardPopupMenu.s_Position
	end
end

--@region UIEvent

function LuaShengXiaoCardPopupMenu:OnButton1Click()
	if LuaShengXiaoCardPopupMenu.s_TargetId>0 then
		Gac2Gas.ShengXiaoCard_Interact(LuaShengXiaoCardPopupMenu.s_TargetId,0)
	else
		CChatHelper.SendMsg(EChatPanel.Current, "#93", 0)
		Gac2Gas.ShengXiaoCard_Interact(CClientMainPlayer.Inst.Id,93)
	end
	CUIManager.CloseUI(CLuaUIResources.ShengXiaoCardPopupMenu)

end

function LuaShengXiaoCardPopupMenu:OnButton2Click()
	if LuaShengXiaoCardPopupMenu.s_TargetId>0 then
		Gac2Gas.ShengXiaoCard_Interact(LuaShengXiaoCardPopupMenu.s_TargetId,1)
	else
		CChatHelper.SendMsg(EChatPanel.Current, "#141", 0)
		Gac2Gas.ShengXiaoCard_Interact(CClientMainPlayer.Inst.Id,141)
	end
	CUIManager.CloseUI(CLuaUIResources.ShengXiaoCardPopupMenu)
end

function LuaShengXiaoCardPopupMenu:OnButton3Click()
	if LuaShengXiaoCardPopupMenu.s_TargetId>0 then
		Gac2Gas.ShengXiaoCard_Interact(LuaShengXiaoCardPopupMenu.s_TargetId,2)
	else
		CChatHelper.SendMsg(EChatPanel.Current, "#35", 0)
		Gac2Gas.ShengXiaoCard_Interact(CClientMainPlayer.Inst.Id,35)
	end
	CUIManager.CloseUI(CLuaUIResources.ShengXiaoCardPopupMenu)
end


--@endregion UIEvent

function LuaShengXiaoCardPopupMenu:Update( )
    self.m_Wnd:ClickThroughToClose()
end
function LuaShengXiaoCardPopupMenu:OnDestroy()
	LuaShengXiaoCardPopupMenu.s_TargetId = 0
end
