require("common/common_include")

local CUIFxPaths = import "L10.UI.CUIFxPaths"

EnumWaKuangUpgradeType = {
	WaKuangSkill = 1,
	JiaSuSkill = 2,
	PackageSize = 3,
	GridCapacity = 4,
}

LuaWuYiMgr = {}

function LuaWuYiMgr.IsInWaKuang()
	if not CClientMainPlayer.Inst then return false end
	local setting = WuYi_WaKuangSetting.GetData()
	return CClientMainPlayer.Inst.PlayProp.PlayId == setting.GamePlayId
end

LuaWuYiMgr.m_LeftRewardTimes = 0
function LuaWuYiMgr.OpenWaKuangStartWnd(leftRewardTimes)
	LuaWuYiMgr.m_LeftRewardTimes = leftRewardTimes
	CUIManager.ShowUI("WaKuangStartWnd")
end

LuaWuYiMgr.m_CurrentRound = -1
LuaWuYiMgr.m_FinishedRound = 0
LuaWuYiMgr.m_TotalRound = 0
LuaWuYiMgr.m_StoneInfos = nil
function LuaWuYiMgr.UpdateTaskView(currentRound, finishedRound, totalRound, stoneInfos)
	if currentRound > LuaWuYiMgr.m_CurrentRound then
		LuaWuYiMgr.TaskDemandRefresh()
	end
	LuaWuYiMgr.m_CurrentRound = currentRound
	LuaWuYiMgr.m_FinishedRound = finishedRound
	LuaWuYiMgr.m_TotalRound = totalRound
	LuaWuYiMgr.m_StoneInfos = stoneInfos

	g_ScriptEvent:BroadcastInLua("SynWaKuangTaskView")
end

-- 刷新任务特效
function LuaWuYiMgr.TaskDemandRefresh()
	g_ScriptEvent:BroadcastInLua("TaskDemandRefresh")
end

-- 显示完成特效
function LuaWuYiMgr.ShowFinishTask()
	EventManager.BroadcastInternalForLua(EnumEventType.OnLoadCommonUIFx, {CUIFxPaths.TaskFinishedFxPath})
end

-- 更新挖矿状态界面
LuaWuYiMgr.m_PackageSize = 0
LuaWuYiMgr.m_GridCapacity = 0
LuaWuYiMgr.m_GridInfos = {}
function LuaWuYiMgr.UpdateWaKuangState(packageSize, gridCapacity, gridInfos)
	LuaWuYiMgr.m_PackageSize = packageSize
	LuaWuYiMgr.m_GridCapacity = gridCapacity
	LuaWuYiMgr.m_GridInfos = gridInfos
	g_ScriptEvent:BroadcastInLua("UpdateWaKuangState")
end

-- 挖矿状态增加石头
function LuaWuYiMgr.AddStone(engineId, x, y, idx, itemId, count)
	for i = 1, #LuaWuYiMgr.m_GridInfos do
		if LuaWuYiMgr.m_GridInfos[i].monsterId == itemId then
			LuaWuYiMgr.m_GridInfos[i].count = count
		end
	end
	g_ScriptEvent:BroadcastInLua("WaKuangAddStone", engineId, x, y, idx, itemId, count)
end

-- 挖矿状态丢弃石头
function LuaWuYiMgr.RemoveStone(idx, itemId)
	for i = 1, #LuaWuYiMgr.m_GridInfos do
		if LuaWuYiMgr.m_GridInfos[i].monsterId == itemId then
			LuaWuYiMgr.m_GridInfos[i].monsterId = 0
			LuaWuYiMgr.m_GridInfos[i].count = 0
		end
	end
	g_ScriptEvent:BroadcastInLua("WaKuangRemoveStone", idx, itemId)
end

-- 挖矿升级
function LuaWuYiMgr.ShowWaKuangUpgrade(points, canUpgrade)
	if canUpgrade then
		if CUIManager.IsLoaded("WaKuangUpgradeWnd") then
			g_ScriptEvent:BroadcastInLua("UpdateWaKuangUpgrade")
		else
			CUIManager.ShowUI("WaKuangUpgradeWnd")
		end
	else
		if CUIManager.IsLoaded("WaKuangUpgradeWnd") then
			CUIManager.CloseUI("WaKuangUpgradeWnd")
		end
	end
end

-- 打开成绩界面
LuaWuYiMgr.m_FinishTime = 0
LuaWuYiMgr.m_PlayerInfos = {}
function LuaWuYiMgr.OpenWaKuangResultWnd(finishTime, playerInfos)
	LuaWuYiMgr.m_FinishTime = finishTime
	LuaWuYiMgr.m_PlayerInfos = playerInfos
	CUIManager.ShowUI("WaKuangResultWnd")
end

-------------------------------- Gas2Gac --------------------------------

function Gas2Gac.OpenEnterWaKuangPlayWnd(leftRewardTimes)
	LuaWuYiMgr.OpenWaKuangStartWnd(leftRewardTimes)
end

-- 任务相关
function Gas2Gac.SyncWaKuangTaskInfo(round, finished, total, data_U)
	local list = MsgPackImpl.unpack(data_U)
	if not list then return end

	local infos = {}

	for i = 0, list.Count - 1, 3 do
		local monsterId = list[i]
		local got = list[i+1]
		local need = list[i+2]
		table.insert(infos, {
			monsterId = monsterId,
			got = got,
			need = need,
		})
	end

	LuaWuYiMgr.UpdateTaskView(round, finished, total, infos)
end

-- 更新包裹
function Gas2Gac.SyncWaKuangPackageInfo(packageSize, gridCapacity, data_U)
	local list = MsgPackImpl.unpack(data_U)
	if not list then return end

	local gridInfos = {}
	for i = 0, list.Count - 1, 2 do
		local monsterId = list[i]
		local count = list[i+1]
		table.insert(gridInfos, {
			monsterId = monsterId,
			count = count,
		})
	end

	LuaWuYiMgr.UpdateWaKuangState(packageSize, gridCapacity, gridInfos)
end

-- 获得一块矿石
function Gas2Gac.AddKuangShiToPackage(engineId, x, y, idx, itemId, count)
	-- 将世界坐标转为屏幕坐标，播放一个特效
	LuaWuYiMgr.AddStone(engineId, x, y, idx, itemId, count)
end

function Gas2Gac.RemoveKuangShiFromPackage(idx, itemId)
	LuaWuYiMgr.RemoveStone(idx, itemId)
end

function Gas2Gac.ShowWaKuangPlayResult(finishTime, result_U)
	local list = MsgPackImpl.unpack(result_U)
	if not list then return end

	local playerInfos = {}
	for i = 0, list.Count - 1, 6 do
		local playerId = list[i]
		local name = list[i+1]
		local role = list[i+2]
		local level = list[i+3]
		local score = list[i+4]
		local special = list[i+5]
		table.insert(playerInfos, {
			playerId = playerId,
			name = name,
			role = role,
			level = level,
			score = score,
			special = special,
		})
	end

	table.sort(playerInfos, function (a, b)
		return b.score < a.score
	end)
	LuaWuYiMgr.OpenWaKuangResultWnd(finishTime, playerInfos)
end

-- 升级技能/包裹界面
function Gas2Gac.UpdateWaKuangUpgradePoints(points, canUpgrade)
	LuaWuYiMgr.ShowWaKuangUpgrade(points, canUpgrade)
end
