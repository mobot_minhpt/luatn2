
HanJia2020Mgr = {}

HanJia2020Mgr.snowmanAnimationTickDict = {}

EnumYanChiXiaWorkManual ={
    TianYun = 1,
    TianYi = 2,
    TianMing = 3,
}
HanJia2020Mgr.isGamePlayOpen = true
HanJia2020Mgr.sortedYanChiXiaWorkManualList = {EnumYanChiXiaWorkManual.TianYun, EnumYanChiXiaWorkManual.TianYi, EnumYanChiXiaWorkManual.TianMing}
HanJia2020Mgr.OpenBottleTimes = 0
HanJia2020Mgr.isOpenFlagonWnd = false
HanJia2020Mgr.FlagonWndData = nil
function HanJia2020Mgr:CloseGamePlay()
    self.isGamePlayOpen = false
end

function HanJia2020Mgr:ShowFlagonWnd()
    if HanJia2020Mgr.isGamePlayOpen then
        Gac2Gas.RequestOpenHanJiaBottle()
        --CUIManager.ShowUI(CLuaUIResources.FlagonOpenWnd)
    end
end

function HanJia2020Mgr:GetYanChiXiaWorkManualBuyInfo(index)
    local data = HanJia2020_ManualPrice.GetData(index)
    return String.Format(LocalString.GetString("{0}元购买{1}"), data.Price, data.Name)
end

function HanJia2020Mgr:GetYanChiXiaWorkManualName(index)
    return HanJia2020_ManualPrice.GetData(index).Name
end
----------------------------------------
---家园
----------------------------------------

HanJia2020Mgr.unfinishedSnowManID = nil
HanJia2020Mgr.curSubmitSnowBallCount = 0
HanJia2020Mgr.maxSubmitSnowBalllCount = 100
HanJia2020Mgr.submitSnowBallState = 1
function HanJia2020Mgr:RequestInteractWithUnfinishedSnowMan(SnowManID, currentCount, maxCount, currentStage)
    self.unfinishedSnowManID = SnowManID
    self.curSubmitSnowBallCount = currentCount
    self.maxSubmitSnowBalllCount = maxCount
    self.submitSnowBallState = currentStage
    if currentStage <= 3 then
        CUIManager.ShowUI(CLuaUIResources.SubmitSnowballsForMakingSnowManWnd)
    end
end

HanJia2020Mgr.decorateSnowManID = nil
function HanJia2020Mgr:RequestInteractWithfinishedSnowMan(furnitureId)
    self.decorateSnowManID = furnitureId
    CUIManager.ShowUI(CLuaUIResources.DecorateSnowmanWnd)
end
