local CLoginMgr = import "L10.Game.CLoginMgr"
local CLoadingWnd = import "L10.UI.CLoadingWnd"
local Setting = import "L10.Engine.Setting"
local Main = import "L10.Engine.Main"
local Gac2Login = import "L10.Game.Gac2Login"

LuaLoginQueueWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaLoginQueueWnd, "QuitButton", "QuitButton", GameObject)
RegistChildComponent(LuaLoginQueueWnd, "QuitButton2", "QuitButton2", GameObject)
RegistChildComponent(LuaLoginQueueWnd, "OpenWndBtn", "OpenWndBtn", GameObject)

--@endregion RegistChildComponent end

function LuaLoginQueueWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.QuitButton2.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnQuitButton2Click()
	end)


	
	UIEventListener.Get(self.OpenWndBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOpenWndBtnClick()
	end)


    --@endregion EventBind end
	self.QuitButton.gameObject:SetActive(not LuaLoginMgr.m_PichFaceLoginQueueWnd)
	self.QuitButton2.gameObject:SetActive(LuaLoginMgr.m_PichFaceLoginQueueWnd)
	self.OpenWndBtn.gameObject:SetActive(LuaLoginMgr.m_PichFaceLoginQueueWnd)
	CUIManager.ShowUI("TopNoticeCenter")
end


--@region UIEvent

function LuaLoginQueueWnd:OnQuitButton2Click()
	local msg = g_MessageMgr:FormatMessage("Out_Of_Line")
	MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function () 
		Gac2Login.RequestExitQueue("")
		CUIManager.CloseUI("LoginQueueWnd")
	end), nil, nil, nil, false)
end

function LuaLoginQueueWnd:OnOpenWndBtnClick()
	LuaPinchFaceMgr.m_IsOffline = true
	LuaLoginMgr:ShowQueueStatusTopNotice()
	CLoadingWnd.Inst:LoadLevel(Setting.sDengLu_Level, CUIManager.EUIModuleGroup.ENone, false)
	Main.Inst:StartCoroutine(CLoginMgr.Inst:LoadELoginUI())
end

--@endregion UIEvent

function LuaLoginQueueWnd:OnEnable()
	LuaLoginMgr:HideQueueStatusTopNotice()
end

function LuaLoginQueueWnd:OnDisable()

end