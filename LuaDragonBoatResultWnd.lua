local GameObject = import "UnityEngine.GameObject"
local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local CUIFx = import "L10.UI.CUIFx"

local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local EShareType = import "L10.UI.EShareType"
local CLoginMgr = import "L10.Game.CLoginMgr"

LuaDragonBoatResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaDragonBoatResultWnd, "TryAgainButton", "TryAgainButton", GameObject)
RegistChildComponent(LuaDragonBoatResultWnd, "ShareButton", "ShareButton", GameObject)
RegistChildComponent(LuaDragonBoatResultWnd, "Portrait", "Portrait", CUITexture)
RegistChildComponent(LuaDragonBoatResultWnd, "LvLabel", "LvLabel", UILabel)
RegistChildComponent(LuaDragonBoatResultWnd, "NameLabel", "NameLabel", UILabel)
RegistChildComponent(LuaDragonBoatResultWnd, "ServerLabel", "ServerLabel", UILabel)
RegistChildComponent(LuaDragonBoatResultWnd, "IDLabel", "IDLabel", UILabel)
RegistChildComponent(LuaDragonBoatResultWnd, "Pickup1Label", "Pickup1Label", UILabel)
RegistChildComponent(LuaDragonBoatResultWnd, "Pickup2Label", "Pickup2Label", UILabel)
RegistChildComponent(LuaDragonBoatResultWnd, "Pickup3Label", "Pickup3Label", UILabel)
RegistChildComponent(LuaDragonBoatResultWnd, "Pickup4Label", "Pickup4Label", UILabel)
RegistChildComponent(LuaDragonBoatResultWnd, "ShowSuccessResult", "ShowSuccessResult", GameObject)
RegistChildComponent(LuaDragonBoatResultWnd, "RankLabel", "RankLabel", UILabel)
RegistChildComponent(LuaDragonBoatResultWnd, "TimeLabel", "TimeLabel", UILabel)
RegistChildComponent(LuaDragonBoatResultWnd, "Fx", "Fx", CUIFx)
RegistChildComponent(LuaDragonBoatResultWnd, "CloseButton", "CloseButton", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaDragonBoatResultWnd,"m_PickupLabels")

function LuaDragonBoatResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.TryAgainButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTryAgainButtonClick()
	end)

	UIEventListener.Get(self.ShareButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareButtonClick()
	end)

    --@endregion EventBind end
end

function LuaDragonBoatResultWnd:Init()
	local t = LuaDuanWu2021Mgr.m_DragonBoatResultData
	local isSuccess = t.isSuccess
	if CClientMainPlayer.Inst then
		local myGameServer = CLoginMgr.Inst:GetSelectedGameServer()
		self.NameLabel.text = CClientMainPlayer.Inst.Name
        self.Portrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(CClientMainPlayer.Inst.Class, CClientMainPlayer.Inst.Gender, -1), false)
        self.LvLabel.text = CClientMainPlayer.Inst.Level
		self.IDLabel.text = CClientMainPlayer.Inst.Id
		self.ServerLabel.text = myGameServer.name
	end
	self.m_PickupLabels = {self.Pickup1Label,self.Pickup2Label,self.Pickup3Label,self.Pickup4Label}
	for i, label in pairs(self.m_PickupLabels) do
		label.text = "x" .. t.pickupDataList[i]
	end
	self.Fx:LoadFx(isSuccess and "fx/ui/prefab/UI_longzhou_wancheng.prefab" or "fx/ui/prefab/UI_longzhou_shibai.prefab")
	self.RankLabel.text = t.rank
	self.RankLabel.gameObject:SetActive(t.rank > 0 and isSuccess)
	self.TimeLabel.gameObject:SetActive(isSuccess)
	local minute,second = math.modf(t.time / 60)
	self.TimeLabel.text = minute..":".. math.floor(second * 60)
end

--@region UIEvent

function LuaDragonBoatResultWnd:OnTryAgainButtonClick()
	CUIManager.ShowUI(CLuaUIResources.DragonBoatGamePlayWnd)
	CUIManager.CloseUI(CLuaUIResources.DragonBoatResultWnd)
end

function LuaDragonBoatResultWnd:OnShareButtonClick()
	self.TryAgainButton:SetActive(false)
	self.ShareButton:SetActive(false)
	self.CloseButton:SetActive(false)
	CUICommonDef.CaptureScreen(
            "screenshot",
            true,
            false,
            self.ShareButton,
            DelegateFactory.Action_string_bytes(
                    function(fullPath, jpgBytes)
						self.TryAgainButton:SetActive(true)
						self.ShareButton:SetActive(true)
						self.CloseButton:SetActive(true)
                        if  CLuaShareMgr.IsOpenShare() then
                            ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, fullPath)
                        else
                            CLuaShareMgr.SimpleShareLocalImage2PersonalSpace(fullPath)
                        end
                    end
            ),
            false
    )
end

--@endregion UIEvent

