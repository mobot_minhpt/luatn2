-- Auto Generated!!
local CQingQiuBaoWeiStartWnd = import "L10.UI.CQingQiuBaoWeiStartWnd"
local CQingQiuMgr = import "L10.UI.CQingQiuMgr"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local LocalString = import "LocalString"
local NGUIText = import "NGUIText"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local VoidDelegate = import "UIEventListener+VoidDelegate"

CQingQiuBaoWeiStartWnd.m_Awake_CS2LuaHook = function (this) 
    this.m_ChuYaoBtn.Enabled = false
    this.m_FightBossBtn.Enabled = false
    this.m_ChuYaoStateSprite.color = NGUIText.ParseColor24(CQingQiuBaoWeiStartWnd.s_NotOpenColor, 0)
    this.m_FightBossStateSprite.color = NGUIText.ParseColor24(CQingQiuBaoWeiStartWnd.s_NotOpenColor, 0)
    this.m_ChuYaoStateLabel.text = LocalString.GetString("未开启")
    this.m_FightBossStateLabel.text = LocalString.GetString("未开启")
end
CQingQiuBaoWeiStartWnd.m_OnEnable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.m_CloseObj).onClick = DelegateFactory.VoidDelegate(function (go) 
        this:Close()
    end)
    UIEventListener.Get(this.m_TipObj).onClick = MakeDelegateFromCSFunction(this.ShowTip, VoidDelegate, this)
    UIEventListener.Get(this.m_ChuYaoBtn.gameObject).onClick = MakeDelegateFromCSFunction(this.GoChuYao, VoidDelegate, this)
    UIEventListener.Get(this.m_FightBossBtn.gameObject).onClick = MakeDelegateFromCSFunction(this.GoFightBoss, VoidDelegate, this)
    UIEventListener.Get(this.m_ScoreRankObj).onClick = MakeDelegateFromCSFunction(this.ShowRankWnd, VoidDelegate, this)
    EventManager.AddListenerInternal(EnumEventType.ReplyQingQiuPlayState, MakeDelegateFromCSFunction(this.ReplyQingQiuPlayState, MakeGenericClass(Action1, UInt32), this))
end
CQingQiuBaoWeiStartWnd.m_Init_CS2LuaHook = function (this) 
    this.m_TipLabel.text = g_MessageMgr:FormatMessage("QQBWZ_UI_MESSAGE1")
    this.m_ChuYaoTipLabel.text = g_MessageMgr:FormatMessage("QQBWZ_UI_MESSAGE3")
    this.m_FightBossTipLabel.text = g_MessageMgr:FormatMessage("QQBWZ_UI_MESSAGE2")
    Gac2Gas.QingQiuQueryPlayState()
    this:ReplyQingQiuPlayState(CQingQiuMgr.Inst.m_PlayState)
end
CQingQiuBaoWeiStartWnd.m_ReplyQingQiuPlayState_CS2LuaHook = function (this, state) 
    if state == EnumQingQiuPlayState_lua.Part_1 then
        this.m_ChuYaoStateSprite.color = NGUIText.ParseColor24(CQingQiuBaoWeiStartWnd.s_OpenColor, 0)
        this.m_ChuYaoStateLabel.text = LocalString.GetString("进行中")
        this.m_ChuYaoBtn.Enabled = true
    elseif state == EnumQingQiuPlayState_lua.Part_2 then
        this.m_ChuYaoStateSprite.color = NGUIText.ParseColor24(CQingQiuBaoWeiStartWnd.s_OpenColor, 0)
        this.m_ChuYaoStateLabel.text = LocalString.GetString("进行中")
        this.m_FightBossStateSprite.color = NGUIText.ParseColor24(CQingQiuBaoWeiStartWnd.s_OpenColor, 0)
        this.m_FightBossStateLabel.text = LocalString.GetString("进行中")
        this.m_ChuYaoBtn.Enabled = true
        this.m_FightBossBtn.Enabled = true
    elseif state == EnumQingQiuPlayState_lua.BossKilled then
        this.m_ChuYaoStateSprite.color = NGUIText.ParseColor24(CQingQiuBaoWeiStartWnd.s_FinishedColor, 0)
        this.m_ChuYaoStateLabel.text = LocalString.GetString("已结束")
        this.m_FightBossStateSprite.color = NGUIText.ParseColor24(CQingQiuBaoWeiStartWnd.s_FinishedColor, 0)
        this.m_FightBossStateLabel.text = LocalString.GetString("已结束")
        this.m_ChuYaoBtn.Enabled = false
        this.m_FightBossBtn.Enabled = false
    end
end
