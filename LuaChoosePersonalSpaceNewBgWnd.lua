
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local QnTableView = import "L10.UI.QnTableView"
local Extensions = import "Extensions"
local BoxCollider = import "UnityEngine.BoxCollider"

LuaChoosePersonalSpaceNewBgWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaChoosePersonalSpaceNewBgWnd, "PeriodTimeLabel", "PeriodTimeLabel", UILabel)
RegistChildComponent(LuaChoosePersonalSpaceNewBgWnd, "ChooseButton", "ChooseButton", GameObject)
RegistChildComponent(LuaChoosePersonalSpaceNewBgWnd, "ScrollView", "ScrollView", CUIRestrictScrollView)
RegistChildComponent(LuaChoosePersonalSpaceNewBgWnd, "QnTabView", "QnTabView", QnTableView)

--@endregion RegistChildComponent end
RegistClassMember(LuaChoosePersonalSpaceNewBgWnd, "m_List")
RegistClassMember(LuaChoosePersonalSpaceNewBgWnd, "m_SelectIndex")

function LuaChoosePersonalSpaceNewBgWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.ChooseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnChooseButtonClick()
	end)


    --@endregion EventBind end
end

function  LuaChoosePersonalSpaceNewBgWnd:OnEnable()
    self.PeriodTimeLabel.text = ""
    g_ScriptEvent:AddListener("OnLoadPersonalSpaceNewBgData", self, "OnLoadPersonalSpaceNewBgData")
end

function  LuaChoosePersonalSpaceNewBgWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnLoadPersonalSpaceNewBgData", self, "OnLoadPersonalSpaceNewBgData")
    g_ScriptEvent:BroadcastInLua("OnLoadPersonalSpaceNewBgData",CLuaPersonalSpaceMgr.m_CurrentBgId)
end

function LuaChoosePersonalSpaceNewBgWnd:OnLoadPersonalSpaceNewBgData()
    self.QnTabView:ReloadData(true, false)
    local now = CServerTimeMgr.Inst.timeStamp
    for id,data in pairs(CLuaPersonalSpaceMgr.m_PersonalSpaceBackGroundList) do
        if data.expiredTime then
            self.PeriodTimeLabel.text = System.String.Format(now < data.expiredTime and LocalString.GetString("有效期至 {0}") or LocalString.GetString("[E52626]已失效 {0}"), ToStringWrap(CServerTimeMgr.ConvertTimeStampToZone8Time(data.expiredTime), "yyyy-MM-dd HH:mm"))
            break
        end
    end
    if self.m_SelectIndex then
        self.QnTabView:SetSelectRow(self.m_SelectIndex, false)
    end
end

function LuaChoosePersonalSpaceNewBgWnd:Init()
    self.m_List = {}
    PersonalSpace_Background.ForeachKey(function (key)
        local k,v = key,PersonalSpace_Background.GetData(key)
        table.insert(self.m_List,{id = k,designData = v})
    end)
	self.QnTabView.m_DataSource = DefaultTableViewDataSource.Create(
        function() 
            return self.m_List and #self.m_List or 0
        end,
        function(item, index)
            self:ItemAt(item,index)
        end)
	self.QnTabView.OnSelectAtRow =DelegateFactory.Action_int(function (row)
        self:OnSelectAtRow(row)
    end)
    self:OnLoadPersonalSpaceNewBgData()
end

function LuaChoosePersonalSpaceNewBgWnd:ItemAt(item,index)
	if not self.m_List then return end
    local data = self.m_List[index + 1]
    if not data then return end
    item:GetComponent(typeof(CUITexture)):LoadMaterial(data.designData.Picture)
    item.transform:Find("Click/Selected").gameObject:SetActive(data.id == CLuaPersonalSpaceMgr.m_CurrentBgId)
    UIEventListener.Get(item.transform:Find("Click").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self.QnTabView:SetSelectRow(index, data.id ~= CLuaPersonalSpaceMgr.m_CurrentBgId)
        if data.id ~= CLuaPersonalSpaceMgr.m_CurrentBgId then
            self:OnChooseButtonClick()
        else
            Gac2Gas.RequestUsePersonalSpaceBackGround(0)
        end
    end)
end
--@region UIEvent

function LuaChoosePersonalSpaceNewBgWnd:OnChooseButtonClick()
    if not self.m_List then return end
    if not self.m_SelectIndex then 
        g_MessageMgr:ShowMessage("ChoosePersonalSpaceNewBgWnd_NoneSelect_SetBag")
        return
    end
    local data = self.m_List[self.m_SelectIndex + 1]
    if not data then return end
    Gac2Gas.RequestUsePersonalSpaceBackGround(data.id)
end

function LuaChoosePersonalSpaceNewBgWnd:OnSelectAtRow(row)
    self.m_SelectIndex = row
    local data = self.m_List[row + 1]
    if not data then return end
    local ownBag = CLuaPersonalSpaceMgr.m_PersonalSpaceBackGroundList[data.id] ~= nil
    Extensions.SetLocalPositionZ(self.ChooseButton.transform, ownBag and 0 or -1)
    self.ChooseButton:GetComponent(typeof(BoxCollider)).enabled = ownBag
    g_ScriptEvent:BroadcastInLua("OnPreviewPersonalSpaceNewBgData",data.id)
end
--@endregion UIEvent

