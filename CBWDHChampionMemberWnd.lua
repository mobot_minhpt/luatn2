-- Auto Generated!!
local CBWDHChampionMemberItem = import "L10.UI.CBWDHChampionMemberItem"
local CBWDHChampionMemberWnd = import "L10.UI.CBWDHChampionMemberWnd"
CBWDHChampionMemberWnd.m_NumberOfRows_CS2LuaHook = function (this, view) 
    if CBWDHChampionMemberWnd.info ~= nil then
        return CBWDHChampionMemberWnd.info.members.Count
    end
    return 0
end
CBWDHChampionMemberWnd.m_ItemAt_CS2LuaHook = function (this, view, row) 
    if CBWDHChampionMemberWnd.info ~= nil then
        local cmp = TypeAs(view:GetFromPool(0), typeof(CBWDHChampionMemberItem))
        if row < CBWDHChampionMemberWnd.info.members.Count then
            local isLeader = CBWDHChampionMemberWnd.info.members[row].name == CBWDHChampionMemberWnd.info.leaderName
            cmp:Init(CBWDHChampionMemberWnd.info.members[row], row, isLeader)
            return cmp
        end
    end
    return nil
end
