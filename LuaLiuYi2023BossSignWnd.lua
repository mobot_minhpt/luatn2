local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local UITable = import "UITable"
local CTipParagraphItem = import "L10.UI.CTipParagraphItem"
local UIGrid = import "UIGrid"
local QnButton = import "L10.UI.QnButton"
local CButton = import "L10.UI.CButton"
local Extensions = import "Extensions"
local MessageMgr = import "L10.Game.MessageMgr"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local CUITexture = import "L10.UI.CUITexture"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemMgr = import "L10.Game.CItemMgr"

LuaLiuYi2023BossSignWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaLiuYi2023BossSignWnd, "TitleLab", "TitleLab", UILabel)
RegistChildComponent(LuaLiuYi2023BossSignWnd, "RuleTable", "RuleTable", UITable)
RegistChildComponent(LuaLiuYi2023BossSignWnd, "ParagraphTemplate", "ParagraphTemplate", CTipParagraphItem)
RegistChildComponent(LuaLiuYi2023BossSignWnd, "Rewards", "Rewards", UIGrid)
RegistChildComponent(LuaLiuYi2023BossSignWnd, "Reward1", "Reward1", QnButton)
RegistChildComponent(LuaLiuYi2023BossSignWnd, "Reward2", "Reward2", QnButton)
RegistChildComponent(LuaLiuYi2023BossSignWnd, "SignBtn", "SignBtn", CButton)
RegistChildComponent(LuaLiuYi2023BossSignWnd, "TimesLab", "TimesLab", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaLiuYi2023BossSignWnd, "m_BaseRewardItemId")
RegistClassMember(LuaLiuYi2023BossSignWnd, "m_AdvancedRewardItemId")
RegistClassMember(LuaLiuYi2023BossSignWnd, "m_MaxBaseRewardNum")
RegistClassMember(LuaLiuYi2023BossSignWnd, "m_MaxAdvancedRewardNum")
RegistClassMember(LuaLiuYi2023BossSignWnd, "m_MaxJoinNum")

function LuaLiuYi2023BossSignWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.m_BaseRewardItemId     = tonumber(LiuYi2023_BossFight.GetData("BaseRewardItemId").Value) or 0
    self.m_AdvancedRewardItemId = tonumber(LiuYi2023_BossFight.GetData("AdvancedRewardItemId").Value) or 0
    self.m_MaxBaseRewardNum     = tonumber(LiuYi2023_BossFight.GetData("MaxBaseRewardNum").Value) or 1
    self.m_MaxAdvancedRewardNum = tonumber(LiuYi2023_BossFight.GetData("MaxAdvancedRewardNum").Value) or 1
    self.m_MaxJoinNum           = tonumber(LiuYi2023_BossFight.GetData("MaxJoinNum").Value) or 1

    UIEventListener.Get(self.SignBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go) 
        Gac2Gas.TangGuoGuiEnterGame()
    end)

    self.ParagraphTemplate.gameObject:SetActive(false)
end

function LuaLiuYi2023BossSignWnd:Init()
    Extensions.RemoveAllChildren(self.RuleTable.transform)
    local ruleMessage = MessageMgr.Inst:FormatMessage("LIUYI2023_TANGGUOGUI_SIGNDESC", {})
    local ruleTipInfo = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(ruleMessage)
    if ruleTipInfo ~= nil then
        for i = 0, ruleTipInfo.paragraphs.Count - 1 do
            local rule = CUICommonDef.AddChild(self.RuleTable.gameObject, self.ParagraphTemplate.gameObject):GetComponent(typeof(CTipParagraphItem))
            rule.gameObject:SetActive(true)
            rule:Init(ruleTipInfo.paragraphs[i], 0xffffffff)
        end
    end
    self.RuleTable:Reposition()

    self:InitReward(self.Reward1, self.m_BaseRewardItemId, LocalString.GetString("参与奖励"), 0, self.m_MaxBaseRewardNum)
    self:InitReward(self.Reward2, self.m_AdvancedRewardItemId, LocalString.GetString("挑战奖励"), 0, self.m_MaxAdvancedRewardNum)

    self.TimesLab.text = SafeStringFormat3("%d/%d", 0, self.m_MaxJoinNum) 

    Gac2Gas.PlayerQueryTangGuoGuiData()
end

--@region UIEvent

--@endregion UIEvent

function LuaLiuYi2023BossSignWnd:OnEnable()
    g_ScriptEvent:AddListener("PlayerQueryTangGuoGuiDataResult", self, "OnPlayerQueryTangGuoGuiDataResult")
end

function LuaLiuYi2023BossSignWnd:OnDisable()
    g_ScriptEvent:RemoveListener("PlayerQueryTangGuoGuiDataResult", self, "OnPlayerQueryTangGuoGuiDataResult")
end

function LuaLiuYi2023BossSignWnd:OnPlayerQueryTangGuoGuiDataResult(baseRewardTime, winRewardTime, joinTime)
    self:InitReward(self.Reward1, self.m_BaseRewardItemId, LocalString.GetString("参与奖励"), baseRewardTime, self.m_MaxBaseRewardNum)
    self:InitReward(self.Reward2, self.m_AdvancedRewardItemId, LocalString.GetString("挑战奖励"), winRewardTime, self.m_MaxAdvancedRewardNum)

    -- MaxJoinNum
    self.TimesLab.text = SafeStringFormat3("%d/%d", joinTime, self.m_MaxJoinNum) 
end

function LuaLiuYi2023BossSignWnd:InitReward(rewardBtn, itemTemplateId, descText, curTimes, maxTimes)
    local itemTex   = rewardBtn:GetComponent(typeof(CUITexture))
    local descLab   = rewardBtn.transform:Find("RewardDesc/DescLab"):GetComponent(typeof(UILabel))
    local timesLab  = rewardBtn.transform:Find("RewardDesc/RewardTimesLab"):GetComponent(typeof(UILabel))

    local item = CItemMgr.Inst:GetItemTemplate(itemTemplateId)
    itemTex:LoadMaterial(item.Icon)
    rewardBtn.OnClick = DelegateFactory.Action_QnButton(function(btn)
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemTemplateId, false, nil, AlignType.Default, 0, 0, 0, 0)
    end)
    descLab.text = descText
    timesLab.text = SafeStringFormat3("%d/%d", curTimes, maxTimes)
end
