-- Auto Generated!!
local CHuluBrothersChooseWnd = import "L10.UI.CHuluBrothersChooseWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"
local DelegateFactory = import "DelegateFactory"
--local HuluBrothers_Transform = import "L10.Game.HuluBrothers_Transform"
local Monster_Monster = import "L10.Game.Monster_Monster"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CHuluBrothersChooseWnd.m_OnEnable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.m_CloseBtn).onClick = MakeDelegateFromCSFunction(this.OnClickCloseButton, VoidDelegate, this)
    UIEventListener.Get(this.m_ChooseBtn).onClick = MakeDelegateFromCSFunction(this.OnClickChooseButton, VoidDelegate, this)
    UIEventListener.Get(this.m_LeftArrowBtn).onClick = MakeDelegateFromCSFunction(this.OnClickLeftButton, VoidDelegate, this)
    UIEventListener.Get(this.m_RightArrowBtn).onClick = MakeDelegateFromCSFunction(this.OnClickRightButton, VoidDelegate, this)
end
CHuluBrothersChooseWnd.m_OnClickLeftButton_CS2LuaHook = function (this, go) 
    this.m_CurrentId = this.m_CurrentId - 1
    if this.m_CurrentId < 0 then
        this.m_CurrentId = this.m_ModelNum - 1
    end
    this:LoadInfo(this.m_CurrentId)
end
CHuluBrothersChooseWnd.m_OnClickRightButton_CS2LuaHook = function (this, go) 
    this.m_CurrentId = this.m_CurrentId + 1
    if this.m_CurrentId >= this.m_ModelNum then
        this.m_CurrentId = 0
    end
    this:LoadInfo(this.m_CurrentId)
end
CHuluBrothersChooseWnd.m_Init_CS2LuaHook = function (this) 
    if nil == this.m_SkillIds then
        this.m_SkillIds = CreateFromClass(MakeGenericClass(List, MakeGenericClass(List, UInt32)))
        this.m_ModelName = CreateFromClass(MakeGenericClass(List, String))
        this.m_MonsterIds = CreateFromClass(MakeGenericClass(List, UInt32))
        HuluBrothers_Transform.Foreach(function (key, data) 
            CommonDefs.ListAdd(this.m_SkillIds, typeof(MakeGenericClass(List, UInt32)), InitializeListWithArray(CreateFromClass(MakeGenericClass(List, UInt32)), UInt32, data.Skill))
            CommonDefs.ListAdd(this.m_ModelName, typeof(String), data.Model)
            CommonDefs.ListAdd(this.m_MonsterIds, typeof(UInt32), data.MonsterID)
        end)
        this.m_ModelNum = this.m_SkillIds.Count
    end
    this:LoadInfo(this.m_CurrentId)
end
CHuluBrothersChooseWnd.m_LoadInfo_CS2LuaHook = function (this, id) 
    if this.m_SkillIds[id] ~= nil then
        do
            local i = 0 local cnt = this.m_SkillIds[id].Count
            while i < cnt do
                local skillId = this.m_SkillIds[id][i]
                local skill = Skill_AllSkills.GetData(skillId)
                if skill ~= nil then
                    this.m_SkillTexture[i]:LoadSkillIcon(skill.SkillIcon)
                    UIEventListener.Get(this.m_SkillTexture[i].gameObject).onClick = DelegateFactory.VoidDelegate(function (go) 
                        CSkillInfoMgr.ShowSkillInfoWnd(skillId, go.transform.position, CSkillInfoMgr.EnumSkillInfoContext.Default, true, 0, 0, nil)
                    end)
                end
                i = i + 1
            end
        end
    end
    local monsterData = Monster_Monster.GetData(this.m_MonsterIds[id])
    if monsterData ~= nil then
        this.m_NameLabel.text = monsterData.Name
    end
    this.m_ModelTexture.mainTexture = this:CreateTexture()
end
