local QnAdvanceGridView     = import "L10.UI.QnAdvanceGridView"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local CPlayerInfoMgr        = import "CPlayerInfoMgr"
local EChatPanel            = import "L10.Game.EChatPanel"
local AlignType             = import "CPlayerInfoMgr+AlignType"

LuaChunJie2024FuYanBuyRankWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaChunJie2024FuYanBuyRankWnd, "tableView", "TableView", QnAdvanceGridView)
--@endregion RegistChildComponent end

RegistClassMember(LuaChunJie2024FuYanBuyRankWnd, "rankList")

function LuaChunJie2024FuYanBuyRankWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaChunJie2024FuYanBuyRankWnd:Init()
    self:InitTableView()
end

function LuaChunJie2024FuYanBuyRankWnd:InitTableView()
    self.rankList = {}
    self.tableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.rankList
        end,
        function(item, index)
            item:SetBackgroundTexture(index % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite)

            self:InitItem(item, index)
        end
    )
    self.tableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        local data = self.rankList[row + 1]
        if data then
            CPlayerInfoMgr.ShowPlayerPopupMenu(data.PlayerIndex, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, "", nil, Vector3.zero, AlignType.Default)
        end
    end)
end

function LuaChunJie2024FuYanBuyRankWnd:InitItem(item, index)
end

--@region UIEvent

--@endregion UIEvent
