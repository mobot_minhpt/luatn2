-- Auto Generated!!
local CChristmasTreeRankInfo = import "L10.UI.CChristmasTreeRankInfo"
local CChristmasTreeRankWnd = import "L10.UI.CChristmasTreeRankWnd"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CCommonUIFxWnd = import "L10.UI.CCommonUIFxWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local EnumClass = import "L10.Game.EnumClass"
local EnumHongBaoRainType = import "L10.UI.EnumHongBaoRainType"
local Extensions = import "Extensions"
local LocalString = import "LocalString"
local MsgPackImpl = import "MsgPackImpl"
local NGUITools = import "NGUITools"
local Object = import "System.Object"
local Profession = import "L10.Game.Profession"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local UISprite = import "UISprite"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CChristmasTreeRankInfo.m_FromDict_CS2LuaHook = function (info) 
    local rankInfo = CreateFromClass(CChristmasTreeRankInfo)
    if info ~= nil then
        local playerId = math.floor(tonumber(CommonDefs.DictGetValue(info, typeof(String), "id") or 0))
        local count = math.floor(tonumber(CommonDefs.DictGetValue(info, typeof(String), "count") or 0))
        local playerInfo = nil
        if CommonDefs.DictContains(info, typeof(String), "info") then
            playerInfo = TypeAs(CommonDefs.DictGetValue(info, typeof(String), "info"), typeof(MakeGenericClass(Dictionary, String, Object)))
        end
        if playerInfo ~= nil then
            local playerName = tostring(CommonDefs.DictGetValue(playerInfo, typeof(String), "Name"))
            local clazz = math.floor(tonumber(CommonDefs.DictGetValue(playerInfo, typeof(String), "Class") or 0))
            rankInfo.playerId = playerId
            rankInfo.playerName = playerName
            rankInfo.clazz = clazz
            rankInfo.count = count
        end
    end
    return rankInfo
end
CChristmasTreeRankWnd.m_Init_CS2LuaHook = function (this) 
    UIEventListener.Get(this.tipNode).onClick = DelegateFactory.VoidDelegate(function (p) 
        g_MessageMgr:ShowMessage("CHRISTMAS_RANK_TIP")
    end)
    UIEventListener.Get(this.closeBtn).onClick = MakeDelegateFromCSFunction(this.OnClickCloseButton, VoidDelegate, this)
    this.templdateNode:SetActive(false)

    this:InitList()
end
CChristmasTreeRankWnd.m_SetEmptySelfInfo_CS2LuaHook = function (this) 
    if CClientMainPlayer.Inst == nil then
        return
    end

    CommonDefs.GetComponent_Component_Type(this.selfNode.transform:Find("Name/text"), typeof(UILabel)).text = CClientMainPlayer.Inst.Name
    CommonDefs.GetComponent_Component_Type(this.selfNode.transform:Find("Name/JobIcon"), typeof(UISprite)).spriteName = Profession.GetIcon(CClientMainPlayer.Inst.Class)

    if CChristmasTreeRankWnd.s_MyInfo ~= nil then
        CommonDefs.GetComponent_Component_Type(this.selfNode.transform:Find("time"), typeof(UILabel)).text = tostring(CChristmasTreeRankWnd.s_MyInfo.count)
    else
        CommonDefs.GetComponent_Component_Type(this.selfNode.transform:Find("time"), typeof(UILabel)).text = "0"
    end
    CommonDefs.GetComponent_Component_Type(this.selfNode.transform:Find("rank"), typeof(UILabel)).text = LocalString.GetString("未上榜")
end
CChristmasTreeRankWnd.m_InitSelfInfo_CS2LuaHook = function (this, info, index) 
    CommonDefs.GetComponent_Component_Type(this.selfNode.transform:Find("Name/text"), typeof(UILabel)).text = info.playerName
    CommonDefs.GetComponent_Component_Type(this.selfNode.transform:Find("Name/JobIcon"), typeof(UISprite)).spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), (info.clazz)))
    CommonDefs.GetComponent_Component_Type(this.selfNode.transform:Find("time"), typeof(UILabel)).text = tostring(info.count)
    CommonDefs.GetComponent_Component_Type(this.selfNode.transform:Find("rank"), typeof(UILabel)).text = tostring(index)
end
CChristmasTreeRankWnd.m_InitList_CS2LuaHook = function (this) 
    this:SetEmptySelfInfo()
    Extensions.RemoveAllChildren(this.table.transform)
    if CChristmasTreeRankWnd.s_RankInfo.Count > 0 then
        do
            local i = 0
            while i < CChristmasTreeRankWnd.s_RankInfo.Count do
                local info = CChristmasTreeRankWnd.s_RankInfo[i]
                local node = NGUITools.AddChild(this.table.gameObject, this.templdateNode)
                node:SetActive(true)

                CommonDefs.GetComponent_Component_Type(node.transform:Find("Name/text"), typeof(UILabel)).text = info.playerName
                CommonDefs.GetComponent_Component_Type(node.transform:Find("Name/JobIcon"), typeof(UISprite)).spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), (info.clazz)))
                CommonDefs.GetComponent_Component_Type(node.transform:Find("time"), typeof(UILabel)).text = tostring(info.count)
                CommonDefs.GetComponent_Component_Type(node.transform:Find("rank"), typeof(UILabel)).text = tostring((i + 1))

                if info.playerId == CClientMainPlayer.Inst.Id then
                    this:InitSelfInfo(info, i + 1)
                end
                i = i + 1
            end
        end
        this.table:Reposition()
        this.scrollView:ResetPosition()
    end
end
Gas2Gac.ChristmasPutTreeSuccess = function (playerData, sceneData) 
    local playerInfo = TypeAs(MsgPackImpl.unpack(playerData), typeof(MakeGenericClass(Dictionary, String, Object)))
    local sceneInfo = TypeAs(MsgPackImpl.unpack(sceneData), typeof(MakeGenericClass(Dictionary, String, Object)))
    if playerData ~= nil and sceneInfo ~= nil then
        local playerId = math.floor(tonumber(CommonDefs.DictGetValue(playerInfo, typeof(String), "Id") or 0))
        local playerName = tostring(CommonDefs.DictGetValue(playerInfo, typeof(String), "Name"))
        local sceneTemplateId = math.floor(tonumber(CommonDefs.DictGetValue(sceneInfo, typeof(String), "TemplateId") or 0))
        local sceneName = tostring(CommonDefs.DictGetValue(sceneInfo, typeof(String), "Name"))
        local sceneId = tostring(CommonDefs.DictGetValue(sceneInfo, typeof(String), "Id"))
        local posX = math.floor(tonumber(CommonDefs.DictGetValue(sceneInfo, typeof(String), "posX") or 0))
        local posZ = math.floor(tonumber(CommonDefs.DictGetValue(sceneInfo, typeof(String), "posZ") or 0))
        local mapIndex = math.floor(tonumber(CommonDefs.DictGetValue(sceneInfo, typeof(String), "MapIndex") or 0))
        g_MessageMgr:ShowMessage("TUHAO_TREE_BROADCAST", playerId, playerName, sceneTemplateId, sceneName, posX, posZ, sceneId, mapIndex)
        CCommonUIFxWnd.Instance:PlayHongBaoFx(EnumHongBaoRainType.Chirstmas)
    end
end
