require("common/common_include")

local UIGrid = import "UIGrid"
local LuaTweenUtils = import "LuaTweenUtils"
local NGUIText = import "NGUIText"
local Color = import "UnityEngine.Color"
local MessageMgr = import "L10.Game.MessageMgr"

LuaBirthPermissionWnd = class()

RegistClassMember(LuaBirthPermissionWnd, "InfoGrid")
RegistClassMember(LuaBirthPermissionWnd, "InfoTemplate")

RegistClassMember(LuaBirthPermissionWnd, "ResultInfo")
RegistClassMember(LuaBirthPermissionWnd, "IsCoupleDesc")
RegistClassMember(LuaBirthPermissionWnd, "NotCoupleDesc")
RegistClassMember(LuaBirthPermissionWnd, "PermissionGOs")

RegistClassMember(LuaBirthPermissionWnd, "DelayInterval")
RegistClassMember(LuaBirthPermissionWnd, "CheckingInterval")
RegistClassMember(LuaBirthPermissionWnd, "ResultInterval")
RegistClassMember(LuaBirthPermissionWnd, "CheckingItemTime")

RegistClassMember(LuaBirthPermissionWnd, "IsPermitted")
RegistClassMember(LuaBirthPermissionWnd, "TweenList")
RegistClassMember(LuaBirthPermissionWnd, "CloseTick")

function LuaBirthPermissionWnd:Init()
	self:InitClassMembers()
	self:InitValues()

end

function LuaBirthPermissionWnd:InitClassMembers()
	self.InfoGrid = self.transform:Find("Anchor/BG/ContentScrollView/InfoGrid"):GetComponent(typeof(UIGrid))
	self.InfoTemplate = self.transform:Find("Anchor/BG/InfoTemplate").gameObject
	self.InfoTemplate:SetActive(false)
	
end

function LuaBirthPermissionWnd:InitValues()

	self.ResultInfo = {}

	self.TweenList = {}
	self.DelayInterval = 0.2
	self.CheckingInterval = 0.6
	self.ResultInterval = 0.6
	self.CheckingItemTime = 1.7
	self.IsPermitted = false

	self.IsCoupleDesc = {}
	table.insert(self.IsCoupleDesc, LocalString.GetString("夫妻组队前来申请（二人均需到场）"))
	table.insert(self.IsCoupleDesc, LocalString.GetString("夫妻之间友好度达到50000"))
	table.insert(self.IsCoupleDesc, LocalString.GetString("夫妻双方获得养育资格"))
	table.insert(self.IsCoupleDesc, LocalString.GetString("夫妻拥有家园，且家园达到5级"))
	table.insert(self.IsCoupleDesc, LocalString.GetString("女方当前未孕"))
	table.insert(self.IsCoupleDesc, LocalString.GetString("夫妻拥有的宝宝数量不足2个"))

	self.NotCoupleDesc = {}
	table.insert(self.NotCoupleDesc,  LocalString.GetString("目前单身"))
	table.insert(self.NotCoupleDesc,  LocalString.GetString("已经获得养育资格"))
	table.insert(self.NotCoupleDesc,  LocalString.GetString("家园达到5级"))
	table.insert(self.NotCoupleDesc,  LocalString.GetString("当前还未怀孕"))
	table.insert(self.NotCoupleDesc,  LocalString.GetString("当前还没有宝宝"))

	self:UpdatePermissionResult(LuaBabyMgr.m_BirthPermissionResult)
end	


function LuaBirthPermissionWnd:UpdatePermissionResult(infos)
	if not infos then
		return
	end

	self.ResultInfo = infos
	LuaBabyMgr.m_BirthPermissionResult = nil

	self.IsPermitted = true
	-- 遍历
	for i = 1, #self.ResultInfo, 1 do
		if not self.ResultInfo[i] then
			self.IsPermitted = false
		end
	end

	-- 初始化条件显示
	local descInfo = self.IsCoupleDesc
	if not LuaBabyMgr.m_IsCoupleBirthPermission then
		descInfo = self.NotCoupleDesc
	end

	CUICommonDef.ClearTransform(self.InfoGrid.transform)
	self.PermissionGOs = {}

	for i = 1, #descInfo, 1 do
		local go = NGUITools.AddChild(self.InfoGrid.gameObject, self.InfoTemplate)
		self:InitItem(go, descInfo[i])
		go:SetActive(true)
		table.insert(self.PermissionGOs, go)
	end
	self.InfoGrid:Reposition()

	-- 显示检查过程
	for i = 1, #self.ResultInfo, 1 do
		self:ShowItemResult(self.PermissionGOs[i], i, self.ResultInfo[i], i == #self.ResultInfo)
	end

end

function LuaBirthPermissionWnd:ShowItemResult(go, index, result, isLast)
	-- 3个步骤 BG消失 Checking显示 Check消失 结果图标显示(包括背景)

	local ContentLabel = go.transform:Find("ContentLabel"):GetComponent(typeof(UILabel))
	local Success = go.transform:Find("Status/Success").gameObject
	local Fail = go.transform:Find("Status/Fail").gameObject
	local BG = go.transform:Find("Status/BG").gameObject
	local Checking = go.transform:Find("Status/Checking").gameObject
	local JoinLine = go.transform:Find("Status/JoinLine").gameObject

	local bgTween = LuaTweenUtils.TweenAlpha(BG.transform, 1, 0, 0.2)
	LuaTweenUtils.OnStart(bgTween, function ()
		ContentLabel.color = Color.white
	end)
	LuaTweenUtils.SetDelay(bgTween, (index - 1) * self.CheckingItemTime + self.DelayInterval)
	table.insert(self.TweenList, bgTween)

	local checkingShowTween = LuaTweenUtils.TweenAlpha(Checking.transform, 0, 1, 0.2)
	LuaTweenUtils.SetDelay(checkingShowTween, (index - 1) * self.CheckingItemTime + self.DelayInterval)
	table.insert(self.TweenList, checkingShowTween)

	local checkingDisappearTween = LuaTweenUtils.TweenAlpha(Checking.transform, 1, 0, 0.3)
	LuaTweenUtils.SetDelay(checkingDisappearTween, (index - 1) * self.CheckingItemTime + self.DelayInterval + self.CheckingInterval)
	table.insert(self.TweenList,checkingDisappearTween)

	local bgShowUpTween = LuaTweenUtils.TweenAlpha(BG.transform, 0, 1, 0.3)
	LuaTweenUtils.SetDelay(bgShowUpTween, (index - 1) * self.CheckingItemTime + self.DelayInterval + self.CheckingInterval)
	table.insert(self.TweenList, bgShowUpTween)

	
	if not isLast then
		if result then
			local successTween = LuaTweenUtils.TweenAlpha(Success.transform, 0, 1, 0.5)
			LuaTweenUtils.SetDelay(successTween, (index - 1) * self.CheckingItemTime + self.DelayInterval + self.CheckingInterval)
			table.insert(self.TweenList, successTween)
		else
			local failTween = LuaTweenUtils.TweenAlpha(Fail.transform, 0, 1, 0.5)
			LuaTweenUtils.SetDelay(failTween, (index - 1) * self.CheckingItemTime + self.DelayInterval + self.CheckingInterval)
			table.insert(self.TweenList, failTween)
		end
	else
		if result then
			local successTween = LuaTweenUtils.TweenAlpha(Success.transform, 0, 1, 0.5, function ()
				self:AddResultItem()
			end)
			LuaTweenUtils.SetDelay(successTween, (index - 1) * self.CheckingItemTime + self.DelayInterval + self.CheckingInterval)
			table.insert(self.TweenList, successTween)
		else
			local failTween = LuaTweenUtils.TweenAlpha(Fail.transform, 0, 1, 0.5, function ()
				self:AddResultItem()
			end)
			LuaTweenUtils.SetDelay(failTween, (index - 1) * self.CheckingItemTime + self.DelayInterval + self.CheckingInterval)
			table.insert(self.TweenList, failTween)
		end
	end

end

function LuaBirthPermissionWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateBirthPermissionResult", self, "UpdatePermissionResult")
end

function LuaBirthPermissionWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateBirthPermissionResult", self, "UpdatePermissionResult")
end

function LuaBirthPermissionWnd:InitItem(go, desc, index)
	local ContentLabel = go.transform:Find("ContentLabel"):GetComponent(typeof(UILabel))
	local Success = go.transform:Find("Status/Success"):GetComponent(typeof(UISprite))
	local Fail = go.transform:Find("Status/Fail"):GetComponent(typeof(UISprite))
	local BG = go.transform:Find("Status/BG"):GetComponent(typeof(UISprite))
	local Checking = go.transform:Find("Status/Checking"):GetComponent(typeof(UILabel))
	local JoinLine = go.transform:Find("Status/JoinLine").gameObject

	ContentLabel.text = desc
	ContentLabel.color = NGUIText.ParseColor24("B2B2B2", 0)
	Success.alpha = 0
	Fail.alpha = 0
	BG.alpha = 1
	Checking.alpha = 0
	JoinLine:SetActive(index ~= 1)
end


function LuaBirthPermissionWnd:AddResultItem()
	local go = NGUITools.AddChild(self.InfoGrid.gameObject, self.InfoTemplate)
	self:InitPermittedItem(go, self.IsPermitted)
	go:SetActive(true)
	self.InfoGrid:Reposition()

	if self.IsPermitted then
		if self.CloseTick~=nil then
			UnRegisterTick(self.CloseTick)
		end
		self.CloseTick = RegisterTickOnce(function()
			if LuaBabyMgr.m_IsCoupleBirthPermission then
				MessageMgr.Inst:ShowMessage("BIRTH_PERMISSION_TIPS", {})
			else
				MessageMgr.Inst:ShowMessage("BIRTH_PERMISSION_TIPS_SINGLE", {})
			end
			CUIManager.CloseUI(CLuaUIResources.BirthPermissionWnd)
	    end, 3000)
	end
end

function LuaBirthPermissionWnd:InitPermittedItem(go, isPermitted)
	local ContentLabel = go.transform:Find("ContentLabel"):GetComponent(typeof(UILabel))
	local Success = go.transform:Find("Status/Success"):GetComponent(typeof(UISprite))
	local Fail = go.transform:Find("Status/Fail"):GetComponent(typeof(UISprite))
	local BG = go.transform:Find("Status/BG"):GetComponent(typeof(UISprite))
	local Checking = go.transform:Find("Status/Checking"):GetComponent(typeof(UILabel))
	local JoinLine = go.transform:Find("Status/JoinLine").gameObject
	local Line = go.transform:Find("Line").gameObject

	Success.gameObject:SetActive(false)
	Fail.gameObject:SetActive(false)
	BG.gameObject:SetActive(false)
	Checking.gameObject:SetActive(false)
	JoinLine:SetActive(false)
	Line:SetActive(false)
	if isPermitted then
		ContentLabel.text = LocalString.GetString("[00FF00]申请成功[-]")
	else
		ContentLabel.text = LocalString.GetString("[FF0000]申请失败[-]")
	end
end

function LuaBirthPermissionWnd:OnDestroy()
	if not self.TweenList then return end

	for i = 1, #self.TweenList, 1 do
		LuaTweenUtils.Kill(self.TweenList[i], false)
	end
end

return LuaBirthPermissionWnd