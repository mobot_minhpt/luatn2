local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
--local Shop_PlayScore=import "L10.Game.Shop_PlayScore"
local EnumMoneyType=import "L10.Game.EnumMoneyType"
local EnumPlayScoreString=import "L10.Game.EnumPlayScoreString"
local MsgPackImpl=import "MsgPackImpl"
local CFreightMgr=import "L10.Game.CFreightMgr"
local EnumPlayScoreKey=import "L10.Game.EnumPlayScoreKey"
local CEquipment=import "L10.Game.CEquipment"
local IdPartition=import "L10.Game.IdPartition"
local EquipmentTemplate_Equip=import "L10.Game.EquipmentTemplate_Equip"
local CTaskMgr=import "L10.Game.CTaskMgr"
local CItemMgr=import "L10.Game.CItemMgr"
local CNPCShopInfoMgr=import "L10.UI.CNPCShopInfoMgr"
local ItemGet_item=import "L10.Game.ItemGet_item"
local EnumGuildFreightStatus=import "L10.Game.EnumGuildFreightStatus"
local CGuanNingMgr=import "L10.Game.CGuanNingMgr"
local MenPaiTiaoZhan_Setting=import "L10.Game.MenPaiTiaoZhan_Setting"
local CScheduleMgr = import "L10.Game.CScheduleMgr"

CLuaNPCShopInfoMgr={}
CLuaNPCShopInfoMgr.m_npcShopInfo=nil
CLuaNPCShopInfoMgr.LastOpenShopName=nil
CLuaNPCShopInfoMgr.taskNeededItems={}
CLuaNPCShopInfoMgr.m_ExchangeItemIdToDesireItemIdTbl = nil
CLuaNPCShopInfoMgr.m_ModelSet = {}

CLuaNPCShopInfoMgr.m_IsExtraSliverCostShop = false
CLuaNPCShopInfoMgr.m_SrcItemTemplateId =nil

function CLuaNPCShopInfoMgr.Count()
    if CLuaNPCShopInfoMgr.m_npcShopInfo then
        return #CLuaNPCShopInfoMgr.m_npcShopInfo.ItemGoods + #CLuaNPCShopInfoMgr.m_npcShopInfo.EquipmentGoods
    end
    return 0
end

function CLuaNPCShopInfoMgr:OpenMenPaiContributionShop()
    local menPaiContributionShopId
    if CClientMainPlayer.Inst then
        local class = EnumToInt(CClientMainPlayer.Inst.Class)
        local k =  "SchoolContribution"
        Shop_PlayScore.ForeachKey(function (key)
            if menPaiContributionShopId then return end
            local data = Shop_PlayScore.GetData(key)
            if data.Key == k then
                local ClassLimit = data.ClassLimit
                for i = 0 , ClassLimit.Length - 1 do
                    if ClassLimit[i] == class then
                        menPaiContributionShopId = key
                    end
                end
            end
        end)
    end
    if menPaiContributionShopId then
        CLuaNPCShopInfoMgr.ShowScoreShopById(menPaiContributionShopId)
    end
end

function CLuaNPCShopInfoMgr.RowAt(row)
    if row<#CLuaNPCShopInfoMgr.m_npcShopInfo.ItemGoods then
        return CLuaNPCShopInfoMgr.m_npcShopInfo.ItemGoods[row+1]
    else
        return CLuaNPCShopInfoMgr.m_npcShopInfo.EquipmentGoods[row+1]
    end
end
function CLuaNPCShopInfoMgr.GetPrice(templateId)
    local price=CLuaNPCShopInfoMgr.m_npcShopInfo.Prices[templateId] or 0
    return price
end

function CLuaNPCShopInfoMgr.SetRemainCount(templateId,remainCount)
    if CLuaNPCShopInfoMgr.m_npcShopInfo.Limits[templateId] then
        CLuaNPCShopInfoMgr.m_npcShopInfo.Limits[templateId].limit=remainCount
    end
end
function CLuaNPCShopInfoMgr.GetRemainCount(templateId)
    if CLuaNPCShopInfoMgr.m_npcShopInfo.Limits[templateId] then
        return CLuaNPCShopInfoMgr.m_npcShopInfo.Limits[templateId].limit
    end
    return 999
end

function CLuaNPCShopInfoMgr.GetEquipment(templateId)
    return CLuaNPCShopInfoMgr.m_npcShopInfo.templateId2Equip[templateId]
end
function CLuaNPCShopInfoMgr.GetLimitType(templateId)
    if CLuaNPCShopInfoMgr.m_npcShopInfo.Limits[templateId] then
        return CLuaNPCShopInfoMgr.m_npcShopInfo.Limits[templateId].limitType
    end
    return 0
end

function CLuaNPCShopInfoMgr:ItemExists(templateId)
    local goods = self.m_npcShopInfo.ItemGoods
    if goods then
        for i = 1, #goods do
            if goods[i] == templateId then
                return true
            end
        end
    end
    return CLuaNPCShopInfoMgr.GetEquipment(templateId)~=nil
end

function CLuaNPCShopInfoMgr.InitNpcShopInfo(npcEngineId, shopId, moneyType, itemGoods, equipments, limits) 
    CLuaNPCShopInfoMgr.m_npcShopInfo={
        NpcEngineId = npcEngineId,
        ShopId = shopId,
        MoneyType = moneyType,
        PlayScoreKey = EnumPlayScoreKey.NONE,
        InitKey = nil,
        ItemGoods = {},
        EquipmentGoods = {},
        templateId2Equip = {},
        Limits={},
        Prices={},
    }

    local itemList = MsgPackImpl.unpack(itemGoods)
    do
        local i = 0
        while i < itemList.Count do
            table.insert( CLuaNPCShopInfoMgr.m_npcShopInfo.ItemGoods,  itemList[i])
            i = i + 1
        end
    end

    local equipmentList = MsgPackImpl.unpack(equipments)
    do
        local i = 0
        while i < equipmentList.Count do
            local equipment = CreateFromClass(CEquipment)
            equipment:LoadFromString(TypeAs(equipmentList[i], typeof(MakeArrayClass(System.Byte))))
            table.insert( CLuaNPCShopInfoMgr.m_npcShopInfo.EquipmentGoods,  equipment.TemplateId)
            CLuaNPCShopInfoMgr.m_npcShopInfo.templateId2Equip[equipment.TemplateId] = equipment
            i = i + 1
        end
    end

    local list = MsgPackImpl.unpack(limits)
    if list ~= nil and list.Count % 3 == 0 then
        do
            local i = 0
            while i < list.Count do
                local info = {
                    templateId = list[i],
                    limit = list[i + 1],
                    limitType = list[i + 2]
                }
                CLuaNPCShopInfoMgr.m_npcShopInfo.Limits[info.templateId]=info
                i = i + 3
            end
        end
    end



    --记录价格信息
    do
        local i = 0
        while i < #CLuaNPCShopInfoMgr.m_npcShopInfo.ItemGoods do
            local id=CLuaNPCShopInfoMgr.m_npcShopInfo.ItemGoods[i+1]
            if IdPartition.IdIsItem(id) then
                local item = CItemMgr.Inst:GetItemTemplate(id)
                CLuaNPCShopInfoMgr.m_npcShopInfo.Prices[item.ID] = item.Price
            end
            i = i + 1
        end
    end

    do
        local i = 0
        while i < #CLuaNPCShopInfoMgr.m_npcShopInfo.EquipmentGoods do
            local id=CLuaNPCShopInfoMgr.m_npcShopInfo.EquipmentGoods[i+1]
            if IdPartition.IdIsEquip(id) then
                local equip = EquipmentTemplate_Equip.GetData(id)
                CLuaNPCShopInfoMgr.m_npcShopInfo.Prices[equip.ID] = equip.Price
            end
            i = i + 1
        end
    end

    
end
function CLuaNPCShopInfoMgr.Sort()
    --排序
    local selectItemTemplateId=CNPCShopInfoMgr.selectItemTemplateId
    local needList = {}
    local otherList = {}
    for i,v in ipairs(CLuaNPCShopInfoMgr.m_npcShopInfo.ItemGoods) do
        if v ~= selectItemTemplateId then
            if CLuaNPCShopInfoMgr.taskNeededItems[v] then
                table.insert( needList,v )
            else
                table.insert( otherList,v )
            end
        end
    end
    local list={}
    if selectItemTemplateId > 0 then
        table.insert( list, selectItemTemplateId)
        CNPCShopInfoMgr.selectItemTemplateId = 0
    end
    for i,v in ipairs(needList) do
        table.insert( list, v)
    end
    for i,v in ipairs(otherList) do
        table.insert( list, v)
    end
    CLuaNPCShopInfoMgr.m_npcShopInfo.ItemGoods=list

    local needList = {}
    local otherList = {}
    for i,v in ipairs(CLuaNPCShopInfoMgr.m_npcShopInfo.EquipmentGoods) do
        if CTaskMgr.IsEquipSubmitItem(v) then
            table.insert( needList,v )
        else
            table.insert( otherList,v )
        end
    end
    local list={}
    for i,v in ipairs(needList) do
        table.insert( list, v)
    end
    for i,v in ipairs(otherList) do
        table.insert( list, v)
    end
    CLuaNPCShopInfoMgr.m_npcShopInfo.EquipmentGoods=list
end

function CLuaNPCShopInfoMgr.GetCostItem(tempid)
    local ary = CLuaNPCShopInfoMgr.m_npcShopInfo.CostItem
    if not ary then return nil end 
    for i = 0, ary.Length - 1, 3 do
        if ary[i] == tempid then
            return {ary[i + 1], ary[i + 2]}
        end
    end
    return nil
end

function CLuaNPCShopInfoMgr.InitPlayScoreShopInfo(npcEngineId, shopId, moneyType, itemGoods, equipments, limits) 

    if not Shop_PlayScore.Exists(shopId) then
        return
    end

    local data = Shop_PlayScore.GetData(shopId)
    local key = data.Key
    CLuaNPCShopInfoMgr.m_npcShopInfo={
        PlayScoreKey = EnumPlayScoreString.GetKey(key),
        InitKey = key,
        NpcEngineId = npcEngineId,
        ShopId = shopId,
        MoneyType = moneyType,
        ItemGoods = {},
        Prices={},
        EquipmentGoods={},
        templateId2Equip={},
        Limits={},
        ExtraSliverCost={},
        ExtraFreeSliverCost={},
        CostItem = data.PearlCost
    }
    CLuaNPCShopInfoMgr.m_IsExtraSliverCostShop = false

    local itemList = MsgPackImpl.unpack(itemGoods)
    for i=0,itemList.Count-4,4 do
        local templateId = itemList[i]
        table.insert(  CLuaNPCShopInfoMgr.m_npcShopInfo.ItemGoods, templateId )
        CLuaNPCShopInfoMgr.m_npcShopInfo.Prices[templateId] = itemList[i + 1]
        CLuaNPCShopInfoMgr.m_npcShopInfo.ExtraSliverCost[templateId] = itemList[i+2]
        --需要显示积分+银两、银票的特殊积分商店
        if itemList[i+2]>0 then
            CLuaNPCShopInfoMgr.m_IsExtraSliverCostShop = true
        end
        CLuaNPCShopInfoMgr.m_npcShopInfo.ExtraFreeSliverCost[templateId] = itemList[i+3]
        if itemList[i+3]>0 then
            CLuaNPCShopInfoMgr.m_IsExtraSliverCostShop = true
        end
    end

    local equipmentList = MsgPackImpl.unpack(equipments)

    do
        local i = 0
        while i < equipmentList.Count do
            local equipment = CreateFromClass(CEquipment)
            equipment:LoadFromString(TypeAs(equipmentList[i], typeof(MakeArrayClass(System.Byte))))
            table.insert(CLuaNPCShopInfoMgr.m_npcShopInfo.EquipmentGoods,equipment.TemplateId)
            CLuaNPCShopInfoMgr.m_npcShopInfo.templateId2Equip[equipment.TemplateId]=equipment
            CLuaNPCShopInfoMgr.m_npcShopInfo.Prices[equipment.TemplateId] = equipmentList[i + 1]
            i = i + 2
        end
    end

    local list = MsgPackImpl.unpack(limits)
    if list ~= nil and list.Count % 3 == 0 then
        do
            local i = 0
            while i < list.Count do
                local info = {
                    templateId =list[i],
                    limit = list[i + 1],
                    limitType = list[i + 2],
                }
                CLuaNPCShopInfoMgr.m_npcShopInfo.Limits[info.templateId]=info
                i = i + 3
            end
        end
    end
end

--根据ShopId打开积分商店，将入口集中到这里，避免裸调RPC
--第二个参数可选，有些逻辑如果需要追踪通过哪个道具打开的积分商店，可以传入该参数
function CLuaNPCShopInfoMgr.ShowScoreShopById(shopId, srcTemplateId)
    CLuaNPCShopInfoMgr.m_SrcItemTemplateId = srcTemplateId
    --这里可以考虑调用CLuaNPCShopInfoMgr.ShowScoreShop，为了兼容已有的逻辑，暂时不做处理
    Gac2Gas.OpenShop(shopId)
end

--根据Shop Key打开积分商店
--第二个参数可选，有些逻辑如果需要追踪通过哪个道具打开的积分商店，可以传入该参数
function CLuaNPCShopInfoMgr.ShowScoreShop(scoreKey, srcTemplateId)
    CLuaNPCShopInfoMgr.m_SrcItemTemplateId = srcTemplateId
    if CClientMainPlayer.Inst == nil then
        return
    end
    local shopEnabled = false
    local default = scoreKey
    if default == "GNJC" then
        if CGuanNingMgr.Inst.GnjcGradeLimit <= CClientMainPlayer.Inst.MaxLevel then
            shopEnabled = true
        end
        -- break
    elseif default == "MPTZ" then
        if MenPaiTiaoZhan_Setting.GetData().PlayerMinGrade <= CClientMainPlayer.Inst.MaxLevel then
            shopEnabled = true
        end
        -- break
    elseif default == "QYJF" then
        shopEnabled = true
        -- break
    elseif default == "DuoBao" then
        shopEnabled = true
        -- break
    elseif default == "SchoolContribution" then
        CLuaNPCShopInfoMgr:OpenMenPaiContributionShop()
        return
        -- break
    elseif default == "GXZ" then
        if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst:IsInGuild() then
            shopEnabled = true
        else
            g_MessageMgr:ShowMessage("NOT_IN_ANY_GUILD")
        end
        -- break
    elseif default == "HanJiaScore" then
        local activityId = 42030296
        if CScheduleMgr.Inst:IsCanJoinSchedule(activityId, true) then
            shopEnabled = true
        else
            g_MessageMgr:ShowMessage("HanJia2023_Shop_Not_Open")
        end    
    else
        shopEnabled = true
    end

    if not shopEnabled then
        return
    end
    Shop_PlayScore.ForeachKey(function (key) 
        local playScore = Shop_PlayScore.GetData(key)
        if playScore.Key == scoreKey then
            CLuaNPCShopInfoMgr.LastOpenShopName = playScore.Name
            Gac2Gas.OpenShop(key)
            return
        end
    end)
end
function CLuaNPCShopInfoMgr.ShowNPCShopWnd(npcEngineId, shopId, goods, equipments, limits)
    CLuaNPCShopInfoMgr.m_IsExtraSliverCostShop = false
    CLuaNPCShopInfoMgr.taskNeededItems = {}
    local scoreShop = Shop_PlayScore.GetData(shopId)
    if scoreShop ~= nil then
        CLuaNPCShopInfoMgr.LastOpenShopName=scoreShop.Name
        local key = scoreShop.Key
        if key == "MingWang" then
            CLuaNPCShopInfoMgr.InitPlayScoreShopInfo(npcEngineId, shopId, EnumMoneyType.MingWang, goods, equipments, limits)
        elseif key == "BangGong" then
            CLuaNPCShopInfoMgr.InitPlayScoreShopInfo(npcEngineId, shopId, EnumMoneyType.GuildContri, goods, equipments, limits)
        elseif key == "GNBS" then
            CLuaNPCShopInfoMgr.InitPlayScoreShopInfo(npcEngineId, shopId, EnumMoneyType.YuanBao, goods, equipments, limits)
        elseif key == "NeteaseMember" then
            CLuaNPCShopInfoMgr.InitPlayScoreShopInfo(npcEngineId, shopId, EnumMoneyType.LingYu, goods, equipments, limits)
        else
            CLuaNPCShopInfoMgr.InitPlayScoreShopInfo(npcEngineId, shopId, EnumMoneyType.Score, goods, equipments, limits)
        end
    else
        CLuaNPCShopInfoMgr.LastOpenShopName=CNPCShopInfoMgr.LastOpenShopName
        -- local shopData = Shop_Shop.GetData(shopId)
        -- if shopData then
        --     CLuaNPCShopInfoMgr.LastOpenShopName=shopData.Name
        -- end

        CLuaNPCShopInfoMgr.InitNpcShopInfo(npcEngineId, shopId, EnumMoneyType.YinPiao, goods, equipments, limits)
        local tasks=CTaskMgr.Inst:GetTaskFindItems()
        for i=1,tasks.Count do
            CLuaNPCShopInfoMgr.taskNeededItems[tasks[i-1]]=true
        end
        CLuaNPCShopInfoMgr.Sort()
    end
    if CNPCShopInfoMgr.selectItemTemplateId > 0 then
        CLuaNPCShopInfoMgr.Sort()
    end

    if scoreShop and scoreShop.Key == "ZhanLingScore" then -- 周年庆2023战令积分商店不使用通用界面
        g_ScriptEvent:BroadcastInLua("UpdateNPCShopInfo")
        return
    end

    if scoreShop and scoreShop.Key == "AppearanceScore" then
        LuaAppearancePreviewMgr:OpenAppearanceShop()
        return
    end

    local huoyunrelated=false

    --货运特殊处理
    local huoyunTaskId = CFreightMgr.Inst.taskId
    if CommonDefs.ListContains(CClientMainPlayer.Inst.TaskProp.CurrentTaskList, typeof(UInt32), huoyunTaskId) 
    and not CClientMainPlayer.Inst.TaskProp.CurrentTasks[huoyunTaskId].IsFailed 
    and (CFreightMgr.Inst.playerId == 0 or CFreightMgr.Inst.playerId == CClientMainPlayer.Inst.Id) then
        huoyunrelated=true
        CFreightMgr.Inst.bIsOpenWnd = false
        Gac2Gas.GetCurrentFreightItems(CClientMainPlayer.Inst.Id)
        g_ScriptEvent:AddListener("FreightItemInfoGet", CLuaNPCShopInfoMgr, "OnFreightItemInfoGet")
    end
    if not huoyunrelated then
        if CLuaNPCShopInfoMgr.m_IsExtraSliverCostShop then
            CUIManager.ShowUI(CLuaUIResources.NPCShopWnd2)
        elseif PengDaoFuYao_Setting.GetData().PengDaoFuYaoShopId == shopId then
            CUIManager.ShowUI(CLuaUIResources.PengDaoShopWnd)
        else
            CUIManager.ShowUI(CUIResources.NPCShopWnd)
        end
    end
end

function CLuaNPCShopInfoMgr.OnFreightItemInfoGet(self)
    local npcShop = false
    do
        local i = 0
        while i < CFreightMgr.Inst.cargoList.Count do
            local cargoInfo = CFreightMgr.Inst.cargoList[i]
            local item = ItemGet_item.GetData(cargoInfo.templateId)
            if item ~= nil and (cargoInfo.status == EnumGuildFreightStatus.eNotFilled 
            or cargoInfo.status == EnumGuildFreightStatus.eNotFilledNeedHelp) then
                do
                    local j = 0
                    while j < item.GetPath.Length do
                        if item.GetPath[j] == 7 then
                            CLuaNPCShopInfoMgr.taskNeededItems[item.ID]=true
                            npcShop = true
                        end
                        j = j + 1
                    end
                end
            end
            i = i + 1
        end
    end
    if npcShop then
        CLuaNPCShopInfoMgr.Sort()
    end
    -- CUIManager.ShowUI(CUIResources.NPCShopWnd)
    if CLuaNPCShopInfoMgr.m_IsExtraSliverCostShop then
        CUIManager.ShowUI(CLuaUIResources.NPCShopWnd2)
    else
        CUIManager.ShowUI(CUIResources.NPCShopWnd)
    end
    g_ScriptEvent:RemoveListener("FreightItemInfoGet", CLuaNPCShopInfoMgr, "OnFreightItemInfoGet")
end

function CLuaNPCShopInfoMgr.GetExchangeItemIdToDesireItemIdTbl()
    if CLuaNPCShopInfoMgr.m_ExchangeItemIdToDesireItemIdTbl == nil then
        local tbl = Shop_Setting.GetData().ExchangeItemIdToDesireItemId
        CLuaNPCShopInfoMgr.m_ExchangeItemIdToDesireItemIdTbl = {}
        for i = 0, tbl.Length - 1 do
            CLuaNPCShopInfoMgr.m_ExchangeItemIdToDesireItemIdTbl[tbl[i][0]] = tbl[i][1]
        end
    end
    return CLuaNPCShopInfoMgr.m_ExchangeItemIdToDesireItemIdTbl
end