local Vector3 = import "UnityEngine.Vector3"
local GameObject = import "UnityEngine.GameObject"
local EventManager = import "EventManager"
local EnumQMPKZhanduiMemberWndType = import "L10.Game.EnumQMPKZhanduiMemberWndType"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EnumEventType = import "EnumEventType"
local EChatPanel = import "L10.Game.EChatPanel"
local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"
local CQMPKMemberInfoItem = import "L10.UI.CQMPKMemberInfoItem"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CIndirectUIResources = import "L10.UI.CIndirectUIResources"


CLuaQMPKZhanDuiMemberListWnd = class()
RegistClassMember(CLuaQMPKZhanDuiMemberListWnd,"LeftArrow")
RegistClassMember(CLuaQMPKZhanDuiMemberListWnd,"RightArrow")
RegistClassMember(CLuaQMPKZhanDuiMemberListWnd,"RequestButton")
RegistClassMember(CLuaQMPKZhanDuiMemberListWnd,"MemberItemPrefab")
RegistClassMember(CLuaQMPKZhanDuiMemberListWnd,"MemberRoot")
RegistClassMember(CLuaQMPKZhanDuiMemberListWnd,"SloganLabel")
RegistClassMember(CLuaQMPKZhanDuiMemberListWnd,"RequestLabel")
RegistClassMember(CLuaQMPKZhanDuiMemberListWnd,"NameLabel")
RegistClassMember(CLuaQMPKZhanDuiMemberListWnd,"m_RecruitInfoBtn")
RegistClassMember(CLuaQMPKZhanDuiMemberListWnd,"memberList")
RegistClassMember(CLuaQMPKZhanDuiMemberListWnd,"m_HasApplied")
RegistClassMember(CLuaQMPKZhanDuiMemberListWnd,"RequestButtonShow")

function CLuaQMPKZhanDuiMemberListWnd:Awake()
    self.LeftArrow = self.transform:Find("Wnd_Bg_Secondary_1/ShowArea/LeftArrow").gameObject
    self.RightArrow = self.transform:Find("Wnd_Bg_Secondary_1/ShowArea/RightArrow").gameObject
    self.RequestButton = self.transform:Find("Wnd_Bg_Secondary_1/ShowArea/RequestButton").gameObject
    self.MemberItemPrefab = self.transform:Find("Wnd_Bg_Secondary_1/ShowArea/MemberInfoItem").gameObject
    self.MemberRoot = self.transform:Find("Wnd_Bg_Secondary_1/ShowArea/MemberRoot")
    self.SloganLabel = self.transform:Find("Wnd_Bg_Secondary_1/ShowArea/TopArea/SloganLabel"):GetComponent(typeof(UILabel))
    self.RequestLabel = self.transform:Find("Wnd_Bg_Secondary_1/ShowArea/RequestButton/Label"):GetComponent(typeof(UILabel))
    self.NameLabel = self.transform:Find("Wnd_Bg_Secondary_1/TitleLabel"):GetComponent(typeof(UILabel))
    self.m_RecruitInfoBtn = self.transform:Find("Wnd_Bg_Secondary_1/ShowArea/RecruitButton").gameObject
    self.memberList = {}
    self.m_HasApplied = false

end

-- Auto Generated!!
function CLuaQMPKZhanDuiMemberListWnd:OnJoinQMPKZhanDuiSuccess( args) 
    local zhanduiId=args[0]
    if zhanduiId ~= CQuanMinPKMgr.Inst.m_ZhanDuiList[CQuanMinPKMgr.Inst.m_CurrentZhanDuiIndex].m_Id then
        return
    end
    self.RequestLabel.text = LocalString.GetString("取消申请")
    self.m_HasApplied = true
    CQuanMinPKMgr.Inst.m_ZhanDuiList[CQuanMinPKMgr.Inst.m_CurrentZhanDuiIndex].m_HasApplied = 1
end
function CLuaQMPKZhanDuiMemberListWnd:Init( )
    self:ShowZhanDui(CQuanMinPKMgr.Inst.m_CurrentZhanDuiIndex)
    if self.MemberRoot.childCount == 0 then
        self:initMembers()
    end
    self.RequestButtonShow = true
    if CQuanMinPKMgr.Inst.m_HasZhanDui or CQuanMinPKMgr.Inst.m_ZhanduiMemberWndType == EnumQMPKZhanduiMemberWndType.eScoreZhandui or CQuanMinPKMgr.Inst.m_HideApplyButtonInMemberWnd then
        self.RequestButtonShow = false
    end
    self.RequestButton:SetActive(false)
    self.m_RecruitInfoBtn:SetActive(false)
end
function CLuaQMPKZhanDuiMemberListWnd:ShowZhanDui( index) 
    if CQuanMinPKMgr.Inst.m_ZhanduiMemberWndType == EnumQMPKZhanduiMemberWndType.eSearchZhandui then
        if index < 0 or index >= CQuanMinPKMgr.Inst.m_ZhanDuiList.Count then
            -- self:Close()
            CUIManager.CloseUI(CIndirectUIResources.QMPKZhanDuiMemberListWnd)
            return
        end
        Gac2Gas.QueryQmpkZhanDuiInfoByZhanDuiId(CQuanMinPKMgr.Inst.m_ZhanDuiList[index].m_Id)
        if index == CQuanMinPKMgr.Inst.m_ZhanDuiList.Count - 1 then
            self.RightArrow:SetActive(false)
        else
            self.RightArrow:SetActive(true)
        end
    elseif CQuanMinPKMgr.Inst.m_ZhanduiMemberWndType == EnumQMPKZhanduiMemberWndType.eScoreZhandui then
        if index < 0 or index >= CQuanMinPKMgr.Inst.m_ScoreRankList.Count then
            -- self:Close()
            CUIManager.CloseUI(CIndirectUIResources.QMPKZhanDuiMemberListWnd)
            return
        end
        Gac2Gas.QueryQmpkZhanDuiInfoByZhanDuiId(CQuanMinPKMgr.Inst.m_ScoreRankList[index].m_Id)
        if index == CQuanMinPKMgr.Inst.m_ScoreRankList.Count - 1 then
            self.RightArrow:SetActive(false)
        else
            self.RightArrow:SetActive(true)
        end
    elseif CQuanMinPKMgr.Inst.m_ZhanduiMemberWndType == EnumQMPKZhanduiMemberWndType.eOneZhandui then
        Gac2Gas.QueryQmpkZhanDuiInfoByZhanDuiId(CQuanMinPKMgr.Inst.m_CurrentZhanduiId)
        self.LeftArrow:SetActive(false)
        self.RightArrow:SetActive(false)
        return
    end


    if index == 0 then
        self.LeftArrow:SetActive(false)
    else
        self.LeftArrow:SetActive(true)
    end
end
function CLuaQMPKZhanDuiMemberListWnd:initMembers( )
    self.memberList ={}
    do
        local i = 0
        while i < CQuanMinPKMgr.Inst.m_ZhanDuiMemberMaxCount do
            local go = TypeAs(CommonDefs.Object_Instantiate(self.MemberItemPrefab), typeof(GameObject))
            local trans = go.transform
            trans.parent = self.MemberRoot
            trans.localScale = Vector3.one
            trans.localPosition = Vector3((i % 5) * 255, - (math.floor(i / 5)) * 270)
            go:SetActive(false)
            local item =go:GetComponent(typeof(CQMPKMemberInfoItem))

            UIEventListener.Get(go).onClick=DelegateFactory.VoidDelegate(function(p)
                local cmp=p:GetComponent(typeof(CQMPKMemberInfoItem))
                self:onItemClick(cmp.m_PlayerId,p)
            end)

            table.insert( self.memberList,item )
            i = i + 1
        end
    end
    self.MemberItemPrefab:SetActive(false)
end
function CLuaQMPKZhanDuiMemberListWnd:Start( )
    UIEventListener.Get(self.RequestButton).onClick = DelegateFactory.VoidDelegate(function(go) self:onRequestClick(go) end)
    -- UIEventListener.Get(self.CloseButton).onClick = DelegateFactory.VoidDelegate(function(go) self:OnClickCloseButton(go) end)
    UIEventListener.Get(self.LeftArrow).onClick = DelegateFactory.VoidDelegate(function(go) self:onLeftArrowClick(go) end)
    UIEventListener.Get(self.RightArrow).onClick = DelegateFactory.VoidDelegate(function(go) self:onRightArrowClick(go) end)
    UIEventListener.Get(self.m_RecruitInfoBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnRecruitInfoBtnClicked(go) end)
end
function CLuaQMPKZhanDuiMemberListWnd:OnEnable( )
    -- EventManager.AddListener(EnumEventType.QMPKQueryZhanDuiInfoResult, MakeDelegateFromCSFunction(self.OnReplyZhanDuiInfo, Action0, self))
    -- EventManager.AddListenerInternal(EnumEventType.QMPKRequestJoinQmpkZhanDuiSuccess, MakeDelegateFromCSFunction(self.OnJoinQMPKZhanDuiSuccess, MakeGenericClass(Action1, Double), self))
    g_ScriptEvent:AddListener("QMPKQueryZhanDuiInfoResult", self, "OnReplyZhanDuiInfo")
    g_ScriptEvent:AddListener("QMPKRequestJoinQmpkZhanDuiSuccess", self, "OnJoinQMPKZhanDuiSuccess")
end
function CLuaQMPKZhanDuiMemberListWnd:OnDisable( )
    -- EventManager.RemoveListener(EnumEventType.QMPKQueryZhanDuiInfoResult, MakeDelegateFromCSFunction(self.OnReplyZhanDuiInfo, Action0, self))
    -- EventManager.RemoveListenerInternal(EnumEventType.QMPKRequestJoinQmpkZhanDuiSuccess, MakeDelegateFromCSFunction(self.OnJoinQMPKZhanDuiSuccess, MakeGenericClass(Action1, Double), self))

    g_ScriptEvent:RemoveListener("QMPKQueryZhanDuiInfoResult", self, "OnReplyZhanDuiInfo")
    g_ScriptEvent:RemoveListener("QMPKRequestJoinQmpkZhanDuiSuccess", self, "OnJoinQMPKZhanDuiSuccess")
end
function CLuaQMPKZhanDuiMemberListWnd:OnRecruitInfoBtnClicked( go) 
    CUIManager.ShowUI(CLuaUIResources.QMPKRecruitWnd)
end
function CLuaQMPKZhanDuiMemberListWnd:OnReplyZhanDuiInfo( )
    self.m_RecruitInfoBtn:SetActive(true)
    do
        local i = 0
        while i < CQuanMinPKMgr.Inst.m_ZhanDuiMemberMaxCount do
            self.memberList[i+1].gameObject:SetActive(true)
            if i < CQuanMinPKMgr.Inst.m_MemberInfoList.Count then
                self.memberList[i+1]:SetMemberInfo(CQuanMinPKMgr.Inst.m_MemberInfoList[i], i, nil)
            else
                self.memberList[i+1]:SetMemberInfo(nil, i, nil)
            end
            i = i + 1
        end
    end
    self.SloganLabel.text = CQuanMinPKMgr.Inst.m_CurrentZhanduiSlogan
    self.NameLabel.text = CQuanMinPKMgr.Inst.m_CurrentZhanduiName
    local isShowRequestButton = self.RequestButtonShow
    if CQuanMinPKMgr.Inst.m_MemberInfoList.Count >= CQuanMinPKMgr.Inst.m_ZhanDuiMemberMaxCount then
        isShowRequestButton = false
    elseif CQuanMinPKMgr.Inst.m_ZhanduiMemberWndType ~= EnumQMPKZhanduiMemberWndType.eScoreZhandui then
        self.m_HasApplied = CQuanMinPKMgr.Inst.m_HasApplyCurrentZhandui > 0
        if self.m_HasApplied then
            self.RequestLabel.text = LocalString.GetString("取消申请")
        else
            self.RequestLabel.text = LocalString.GetString("申请")
        end
    end
    self.RequestButton:SetActive(isShowRequestButton)
end
function CLuaQMPKZhanDuiMemberListWnd:onRequestClick( go) 
    if not self.m_HasApplied then
        Gac2Gas.RequestJoinQmpkZhanDui(CQuanMinPKMgr.Inst.m_ZhanDuiList[CQuanMinPKMgr.Inst.m_CurrentZhanDuiIndex].m_Id)
    else
        self.RequestLabel.text = LocalString.GetString("申请")
        Gac2Gas.RequestCancelJoinQmpkZhanDui(CQuanMinPKMgr.Inst.m_ZhanDuiList[CQuanMinPKMgr.Inst.m_CurrentZhanDuiIndex].m_Id)
        self.m_HasApplied = false
        CQuanMinPKMgr.Inst.m_ZhanDuiList[CQuanMinPKMgr.Inst.m_CurrentZhanDuiIndex].m_HasApplied = 0
        EventManager.BroadcastInternalForLua(EnumEventType.QMPKRequestCancelJoinZhanDui, {CQuanMinPKMgr.Inst.m_ZhanDuiList[CQuanMinPKMgr.Inst.m_CurrentZhanDuiIndex].m_Id})
    end
end
function CLuaQMPKZhanDuiMemberListWnd:onItemClick( id, go) 
    if id <= 0 then
        return
    end
    CPlayerInfoMgr.ShowPlayerPopupMenu(math.floor(id), EnumPlayerInfoContext.QMPK, EChatPanel.Undefined, nil, nil, go.transform.position, CPlayerInfoMgr.AlignType.Right)
    do
        local i = 0 local cnt = self.memberList.Count
        while i < cnt do
            self.memberList[i+1]:SetSelected(self.memberList[i+1].gameObject == go)
            i = i + 1
        end
    end
end
function CLuaQMPKZhanDuiMemberListWnd:onLeftArrowClick( go) 
    local default = CQuanMinPKMgr.Inst
    default.m_CurrentZhanDuiIndex = default.m_CurrentZhanDuiIndex - 1
    self:ShowZhanDui(CQuanMinPKMgr.Inst.m_CurrentZhanDuiIndex)
end
function CLuaQMPKZhanDuiMemberListWnd:onRightArrowClick( go) 
    local default = CQuanMinPKMgr.Inst
    default.m_CurrentZhanDuiIndex = default.m_CurrentZhanDuiIndex + 1
    self:ShowZhanDui(CQuanMinPKMgr.Inst.m_CurrentZhanDuiIndex)
end

return CLuaQMPKZhanDuiMemberListWnd
