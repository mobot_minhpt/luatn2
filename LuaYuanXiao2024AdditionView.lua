local GameObject = import "UnityEngine.GameObject"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"

LuaYuanXiao2024AdditionView = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaYuanXiao2024AdditionView, "GoLightingPartyBtn", "GoLightingPartyBtn", GameObject)

--@endregion RegistChildComponent end

function LuaYuanXiao2024AdditionView:Awake()
    --@region EventBind: Dont Modify Manually!

		UIEventListener.Get(self.GoLightingPartyBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)	    self:OnGoLightingPartyBtnClick()	end)

    --@endregion EventBind end
end

function LuaYuanXiao2024AdditionView:Init()

end

--@region UIEvent

function LuaYuanXiao2024AdditionView:OnGoLightingPartyBtnClick()
end


--@endregion UIEvent

