local Extensions = import "Extensions"
local CSkillMgr = import "L10.Game.CSkillMgr"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"
local EnumYingLingState = import "L10.Game.EnumYingLingState"
local CGuideScheduler = import "L10.Game.Guide.CGuideScheduler"
local CGuideMgr = import "L10.Game.Guide.CGuideMgr"
local GuideDefine = import "L10.Game.Guide.GuideDefine"
local CButton = import "L10.UI.CButton"
local Object = import "System.Object"
local TouchPhase = import "UnityEngine.TouchPhase"

LuaSkillPreferenceViewSkillButtonBoard = class()

RegistChildComponent(LuaSkillPreferenceViewSkillButtonBoard,"m_RecommandBtn","RecommandButton", GameObject)
RegistChildComponent(LuaSkillPreferenceViewSkillButtonBoard,"m_SwitchActiveSkillBtn","SwitchBtn", CButton)
RegistChildComponent(LuaSkillPreferenceViewSkillButtonBoard,"m_FollowFingerIcon","CloneItem", GameObject)
RegistChildComponent(LuaSkillPreferenceViewSkillButtonBoard,"m_TipTopPos","TipTopPos", Transform)

RegistClassMember(LuaSkillPreferenceViewSkillButtonBoard,"m_SkillButtons")
RegistClassMember(LuaSkillPreferenceViewSkillButtonBoard,"m_ShowFirstActiveSkillSuite")
RegistClassMember(LuaSkillPreferenceViewSkillButtonBoard,"m_SkillId")
RegistClassMember(LuaSkillPreferenceViewSkillButtonBoard,"m_IsDragging")
RegistClassMember(LuaSkillPreferenceViewSkillButtonBoard,"m_LastDragPos")
RegistClassMember(LuaSkillPreferenceViewSkillButtonBoard,"m_FingerIndex")
RegistClassMember(LuaSkillPreferenceViewSkillButtonBoard,"m_SrcSkillIndex")
RegistClassMember(LuaSkillPreferenceViewSkillButtonBoard,"m_YingLingCurSwitchSkillIds")
RegistClassMember(LuaSkillPreferenceViewSkillButtonBoard,"m_CurSwitchYingLingState")

function LuaSkillPreferenceViewSkillButtonBoard:Awake()

    self.m_ShowFirstActiveSkillSuite = true

    UIEventListener.Get(self.m_RecommandBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnRecommandButtonClick(go)
    end)
    UIEventListener.Get(self.m_SwitchActiveSkillBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnSwitchActiveSkillButtonClick(go)
    end)

    self.m_SkillButtons = {}
    for i = 0,4 do
        local go = self.transform:GetChild(i)
        local t = self:InitSkillButton(go)
        table.insert(self.m_SkillButtons, t)
    end

    self:UpdateSkills()
    self:OnMainPlayerLevelUp()
end

function LuaSkillPreferenceViewSkillButtonBoard:OnEnable()
    g_ScriptEvent:AddListener("SkillPreferenceViewEnableYingLingBianShenSkill", self, "OnEnableYingLingBianShenSkill")
    g_ScriptEvent:AddListener("MainPlayerSkillPropUpdate", self, "OnMainPlayerSkillPropUpdate")
    g_ScriptEvent:AddListener("MainPlayerLevelChange", self, "OnMainPlayerLevelUp")
    g_ScriptEvent:AddListener("UpdateCooldown", self, "OnUpdateCooldown")
    g_ScriptEvent:AddListener("SkillPreferenceViewLoadYingLingSkillIcons", self, "LoadYingLingSkillIcons")
    g_ScriptEvent:AddListener("SkillPreferenceView_SkillTree_OnDragComplete", self, "ReplaceSkill")
    g_ScriptEvent:AddListener("SkillPreferenceViewSwitchYingLingState",self,"OnYingLingStateSwitch")
end

function LuaSkillPreferenceViewSkillButtonBoard:OnDisable()
    g_ScriptEvent:RemoveListener("SkillPreferenceViewEnableYingLingBianShenSkill", self, "OnEnableYingLingBianShenSkill")
    g_ScriptEvent:RemoveListener("MainPlayerSkillPropUpdate", self, "OnMainPlayerSkillPropUpdate")
    g_ScriptEvent:RemoveListener("MainPlayerLevelChange", self, "OnMainPlayerLevelUp")
    g_ScriptEvent:RemoveListener("UpdateCooldown", self, "OnUpdateCooldown")
    g_ScriptEvent:RemoveListener("SkillPreferenceViewLoadYingLingSkillIcons", self, "LoadYingLingSkillIcons")
    g_ScriptEvent:RemoveListener("SkillPreferenceView_SkillTree_OnDragComplete", self, "ReplaceSkill")
    g_ScriptEvent:RemoveListener("SkillPreferenceViewSwitchYingLingState",self,"OnYingLingStateSwitch")
end

function LuaSkillPreferenceViewSkillButtonBoard:Update()
    if nil == self.m_LastDragPos then
        self.m_LastDragPos = Vector3.zero
    end
    if not self.m_IsDragging then
        return
    end

    if CommonDefs.IsInMobileDevice() then
        for i = 0,Input.touchCount - 1 do
            local touch = Input.GetTouch(i)
            if touch.fingerId == self.m_FingerIndex then
                if touch.phase ~= TouchPhase.Ended and touch.phase ~= TouchPhase.Canceled then
                    if touch.phase ~= TouchPhase.Stationary then
                        CSkillInfoMgr.CloseSkillInfoWnd()
                        local pos = touch.position
                        self.m_LastDragPos = Vector3(pos.x, pos.y, 0)
                        self.m_FollowFingerIcon.transform.position = CUIManager.UIMainCamera:ScreenToWorldPoint(self.m_LastDragPos)
                    end
                    return
                else
                    break
                end
            end
        end
    else
        if Input.GetMouseButton(0) then
            if self.m_LastDragPos ~= Input.mousePosition then
                CSkillInfoMgr.CloseSkillInfoWnd()
            end
            self.m_FollowFingerIcon.transform.position = CUIManager.UIMainCamera:ScreenToWorldPoint(Input.mousePosition)
            self.m_LastDragPos = Input.mousePosition

            if not Input.GetMouseButtonUp(0) then
                return
            end
        end
    end
    self.m_FollowFingerIcon.gameObject:SetActive(false)
    local hoveredObject = CUICommonDef.SelectedUIWithRacast
    if hoveredObject and hoveredObject.name == "__SkillIcon__" then
        g_ScriptEvent:BroadcastInLua("SkillPreferenceView_SkillTree_OnDragComplete",
                {self.m_SkillId, hoveredObject, self.m_SrcSkillIndex})
    end
    self.m_IsDragging = false
    self.m_FingerIndex = -1
    self.m_SkillId = UInt32.MaxValue
    self.m_SrcSkillIndex = 1
end

function LuaSkillPreferenceViewSkillButtonBoard:GetCurSwitchYingLingState()
    if (self.m_CurSwitchYingLingState == nil) or (self.m_CurSwitchYingLingState == EnumToInt(EnumYingLingState.eDefault)) then
        self.m_CurSwitchYingLingState = LuaSkillMgr.GetDefaultYingLingState()
    end
    return self.m_CurSwitchYingLingState
end

function LuaSkillPreferenceViewSkillButtonBoard:InitSkillButton(go)
    local t = {}
    t.activeSkillIcon = go.transform:Find("Mask/__SkillIcon__"):GetComponent(typeof(CUITexture))
    UIEventListener.Get(t.activeSkillIcon.gameObject).onDragStart = DelegateFactory.VoidDelegate(function (go)
        self:OnDragIconStart(go)
    end)
    UIEventListener.Get(t.activeSkillIcon.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnSkillIconClick(go)
    end)
    t.activeSkillColorIcon = go.transform:Find("Panel/ColorSystemIcon"):GetComponent(typeof(UISprite))
    t.activeSkillCDIcon = go.transform:Find("Mask/CD"):GetComponent(typeof(UISprite))
    return t
end

function LuaSkillPreferenceViewSkillButtonBoard:UpdateSwitchActiveSkillBtn()
    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer == nil then
        return
    end

    local skillProperty = mainPlayer.SkillProp

    self.m_SwitchActiveSkillBtn.gameObject:SetActive(skillProperty.IsSkillSuite2Opened == 1)
    if skillProperty.IsSkillSuite2Opened == 1 then
        self.m_SwitchActiveSkillBtn.Text = self.m_ShowFirstActiveSkillSuite and "1" or "2"
    else
        self.m_ShowFirstActiveSkillSuite = true
    end
end

function LuaSkillPreferenceViewSkillButtonBoard:LoadActiveSkills()
    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer == nil then
        return
    end

    self:UpdateSwitchActiveSkillBtn()
    local skillProperty = mainPlayer.SkillProp
    local skillIds = self.m_ShowFirstActiveSkillSuite and skillProperty.NewActiveSkill or skillProperty.NewActiveSkill2

    for i = 1,#self.m_SkillButtons do
        if i > skillIds.Length or skillIds[i] == SkillButtonSkillType_lua.NoSkill or not Skill_AllSkills.Exists(skillIds[i]) then
            self.m_SkillButtons[i].activeSkillIcon:LoadSkillIcon(nil)
            self.m_SkillButtons[i].activeSkillColorIcon.gameObject:SetActive(false)
        else
            self.m_SkillButtons[i].activeSkillIcon:LoadSkillIcon(Skill_AllSkills.GetData(skillIds[i]).SkillIcon)
            local classId = CLuaDesignMgr.Skill_AllSkills_GetClass(skillIds[i])
            if CSkillMgr.Inst:IsColorSystemSkill(classId) or CSkillMgr.Inst:IsColorSystemBaseSkill(classId) then
                self.m_SkillButtons[i].activeSkillColorIcon.gameObject:SetActive(true)
                self.m_SkillButtons[i].activeSkillColorIcon.spriteName = CSkillMgr.Inst:GetColorSystemSkillIcon(classId)
            else
                self.m_SkillButtons[i].activeSkillColorIcon.gameObject:SetActive(false)
            end
        end
    end

    self:OnUpdateCooldown()
end

function LuaSkillPreferenceViewSkillButtonBoard:UpdateSkills()
    if LuaSkillMgr.CheckMainPlayerYingLingBianShenSkill() then
        self:LoadYingLingSkillIcons()
    else
        self:LoadActiveSkills()
    end
end
----------------------------------------
---Event
----------------------------------------
function LuaSkillPreferenceViewSkillButtonBoard:OnRecommandButtonClick(go)
    local state = self:GetCurSwitchYingLingState()
    if state ~= EnumToInt(EnumYingLingState.eDefault) then
        if LuaSkillMgr.IsOpenNewSkillSuggestion() then
            LuaSuggestSkillWnd.m_YingLingState = state
            CUIManager.ShowUI("SuggestSkillWnd")
        else
            g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("Recommended_Skill_Confirm", playerName), function()
                Gac2Gas.RequestSuggestSkill(state or 0)
            end, nil, nil, nil, false)
        end
    end
end

function LuaSkillPreferenceViewSkillButtonBoard:OnYingLingStateSwitch(state)
    self.m_CurSwitchYingLingState = state
end

function LuaSkillPreferenceViewSkillButtonBoard:OnSwitchActiveSkillButtonClick(go)
    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer == nil then
        return
    end
    if mainPlayer.SkillProp.IsSkillSuite2Opened == 1 then
        self.m_ShowFirstActiveSkillSuite = not self.m_ShowFirstActiveSkillSuite
        if LuaSkillMgr.CheckMainPlayerYingLingBianShenSkill() then
            self:LoadYingLingSkillIcons()
        else
            self:LoadActiveSkills()
        end
    end
end

function LuaSkillPreferenceViewSkillButtonBoard:OnDragIconStart(go)
    for i = 1,#self.m_SkillButtons do
        if self.m_SkillButtons[i].activeSkillIcon.gameObject == go then
            if CClientMainPlayer.Inst ~= nil then
                self.m_SkillId = self.m_ShowFirstActiveSkillSuite and CClientMainPlayer.Inst.SkillProp.NewActiveSkill[i] or CClientMainPlayer.Inst.SkillProp.NewActiveSkill2[i]
                if LuaSkillMgr.CheckMainPlayerYingLingBianShenSkill() then
                    self.m_SkillId = self.m_YingLingCurSwitchSkillIds[i]
                end
                if self.m_SkillId == SkillButtonSkillType_lua.NoSkill then
                    return
                end
                local data =  Skill_AllSkills.GetData(self.m_SkillId)
                self.m_SrcSkillIndex = i
                self.m_FollowFingerIcon.gameObject:SetActive(true)
                local tex = self.m_FollowFingerIcon.transform:Find("SkillIcon"):GetComponent(typeof(CUITexture))
                tex:LoadSkillIcon(data.SkillIcon)
                self.m_IsDragging = true
                self.m_LastDragPos = Input.mousePosition
                if CommonDefs.IsInMobileDevice() then
                    self.m_FingerIndex = Input.GetTouch(0).fingerId
                    self.m_LastDragPos = Input.GetTouch(0).position
                end
            end
            break
        end
    end
    SoundManager.Inst:PlayOneShot(SoundManager.UISoundEvents.Skill_Switch, Vector3.zero)
end

function LuaSkillPreferenceViewSkillButtonBoard:OnSkillIconClick(go)

    local mainPlayer = CClientMainPlayer.Inst
    if not mainPlayer then
        return
    end
    for i = 1,#self.m_SkillButtons do
        if self.m_SkillButtons[i].activeSkillIcon.gameObject == go then
            local skillId = self.m_ShowFirstActiveSkillSuite and mainPlayer.SkillProp.NewActiveSkill[i] or mainPlayer.SkillProp.NewActiveSkill2[i]
            if LuaSkillMgr.CheckMainPlayerYingLingBianShenSkill() and self.m_YingLingCurSwitchSkillIds and i <= #self.m_YingLingCurSwitchSkillIds then
                skillId = self.m_YingLingCurSwitchSkillIds[i]
            end
            if skillId == SkillButtonSkillType_lua.NoSkill then
                return
            end

            local equipId = nil
            local skillCls = CLuaDesignMgr.Skill_AllSkills_GetClass(skillId)
            if CSkillMgr.Inst:IsIdentifySkill(skillCls) then
                (function ()
                    local __try_get_result
                    __try_get_result, equipId = CommonDefs.DictTryGet(mainPlayer.SkillProp.IdentifySkills, typeof(UInt32), skillCls, typeof(String))
                    return __try_get_result
                end)()
                --查找生效技能对应的装备ID
            end

            CSkillInfoMgr.ShowSkillInfoWnd(skillId, equipId, true, self.m_TipTopPos.position, CSkillInfoMgr.EnumSkillInfoContext.MainPlayerSkillPreference, true, 0, 0, nil)
            break
        end
    end
end

function LuaSkillPreferenceViewSkillButtonBoard:OnEnableYingLingBianShenSkill()
    Extensions.SetLocalPositionX(self.transform, self.transform.localPosition.x + 50)
end

function LuaSkillPreferenceViewSkillButtonBoard:OnMainPlayerLevelUp()
    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer then
        self.m_RecommandBtn:SetActive(mainPlayer.MaxLevel >= 25)
    end
end

function LuaSkillPreferenceViewSkillButtonBoard:OnMainPlayerSkillPropUpdate()
    self:UpdateSkills()
end

function LuaSkillPreferenceViewSkillButtonBoard:OnUpdateCooldown()
    local mainPlayer = CClientMainPlayer.Inst
    if not mainPlayer then
        return
    end

    local globalRemain = mainPlayer.CooldownProp:GetServerRemainTime(1000)
    local globalTotal = mainPlayer.CooldownProp:GetServerTotalTime(1000)
    local globalPercent = (globalTotal == 0) and 0 or (globalRemain / globalTotal)
    local skillProperty = mainPlayer.SkillProp
    local activeSkills = self.m_ShowFirstActiveSkillSuite and skillProperty.NewActiveSkill or skillProperty.NewActiveSkill2

    if LuaSkillMgr.CheckMainPlayerYingLingBianShenSkill() then
        local extraActiveSkills = mainPlayer:GetYingLingExtraActiveSkills(
                skillProperty.CurrentTemplateIdx,
                CommonDefs.ConvertIntToEnum(typeof(EnumYingLingState), self:GetCurSwitchYingLingState()), self.m_ShowFirstActiveSkillSuite and 1 or 2)
        activeSkills = {}
        for i = 0, extraActiveSkills.Length - 1 do
            table.insert(activeSkills,skillProperty:GetSkillIdWithDeltaByCls(extraActiveSkills[i], mainPlayer.Level))
        end
    end

    for i = 1,#self.m_SkillButtons do
        local continue
        repeat
            local skillId = activeSkills[i]
            if skillId == SkillButtonSkillType_lua.NoSkill then
                self.m_SkillButtons[i].activeSkillCDIcon.fillAmount = 1
                continue = true
                break
            end
            local remain = mainPlayer.CooldownProp:GetServerRemainTime(skillId)
            local total = mainPlayer.CooldownProp:GetServerTotalTime(skillId)
            local percent = total == 0 and 0 or remain / total
            if globalRemain > remain then
                percent = globalPercent
            end
            self.m_SkillButtons[i].activeSkillCDIcon.fillAmount = percent
            continue = true
        until 1
        if not continue then
            break
        end
    end
end

function LuaSkillPreferenceViewSkillButtonBoard:LoadYingLingSkillIcons()
    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer == nil then
        return
    end
    local state = self:GetCurSwitchYingLingState()

    self:UpdateSwitchActiveSkillBtn()
    local activeSkills = mainPlayer:GetYingLingExtraActiveSkills(
            mainPlayer.SkillProp.CurrentTemplateIdx,
            CommonDefs.ConvertIntToEnum(typeof(EnumYingLingState), state),self.m_ShowFirstActiveSkillSuite and 1 or 2)
    local skillIds = {}
    for i = 0, activeSkills.Length - 1 do
        table.insert(skillIds,mainPlayer.SkillProp:GetSkillIdWithDeltaByCls(activeSkills[i], mainPlayer.Level))
    end
    for i = 1,#self.m_SkillButtons do
        if skillIds[i] == SkillButtonSkillType_lua.NoSkill or not Skill_AllSkills.Exists(skillIds[i]) then
            self.m_SkillButtons[i].activeSkillIcon:LoadSkillIcon(nil)
            self.m_SkillButtons[i].activeSkillColorIcon.gameObject:SetActive(false)
        else
            self.m_SkillButtons[i].activeSkillIcon:LoadSkillIcon(Skill_AllSkills.GetData(skillIds[i]).SkillIcon)
            self.m_SkillButtons[i].activeSkillColorIcon.gameObject:SetActive(false)
        end
    end
    self.m_YingLingCurSwitchSkillIds = skillIds
    self:OnUpdateCooldown()
end

function LuaSkillPreferenceViewSkillButtonBoard:GetYingLingActiveSkills(curSkillIds, activeSkillIds1, activeSkillIds2)
    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer == nil then
        return nil, nil, nil
    end

    local skillProperty = mainPlayer.SkillProp

    curSkillIds = self.m_YingLingCurSwitchSkillIds

    if LuaSkillMgr.CheckMainPlayerYingLingBianShenSkill() and self.m_YingLingCurSwitchSkillIds then
        if self:GetCurSwitchYingLingState() ~= EnumToInt(mainPlayer.CurYingLingState) then
            local activeSkillCls2 = mainPlayer:GetYingLingExtraActiveSkills(
                    skillProperty.CurrentTemplateIdx,
                    CommonDefs.ConvertIntToEnum(typeof(EnumYingLingState), self:GetCurSwitchYingLingState()), 2)
            activeSkillIds2 = {}
            for i = 0, activeSkillCls2.Length - 1 do
                table.insert(activeSkillIds2, skillProperty:GetSkillIdWithDeltaByCls(activeSkillCls2[i], mainPlayer.Level))
            end

            local activeSkillCls1 = mainPlayer:GetYingLingExtraActiveSkills(
                    skillProperty.CurrentTemplateIdx,
                    CommonDefs.ConvertIntToEnum(typeof(EnumYingLingState), self:GetCurSwitchYingLingState()), 1)
            activeSkillIds1 = {}
            for i = 0, activeSkillCls1.Length - 1 do
                table.insert(activeSkillIds1, skillProperty:GetSkillIdWithDeltaByCls(activeSkillCls1[i], mainPlayer.Level))
            end
        end
    end
    return curSkillIds, activeSkillIds1, activeSkillIds2
end

function LuaSkillPreferenceViewSkillButtonBoard:RequestSaveSkills(activeSkillIds1, activeSkillIds2, normalSkills)
    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer == nil then
        return
    end
    local skillProperty = mainPlayer.SkillProp
    local tianfuSkills = CreateFromClass(MakeGenericClass(List, Object))
    skillProperty:ForeachOriginalSkillId(DelegateFactory.Action_KeyValuePair_uint_uint(function (kv)
        local skillId = kv.Key
        local skillTemp = Skill_AllSkills.GetData(skillId)
        if skillTemp.Kind == SkillKind_lua.TianFuSkill then
            CommonDefs.ListAdd(tianfuSkills, typeof(UInt32), skillId)
        end
    end))
    local list1 = CreateFromClass(MakeGenericClass(List, Object))
    local list2 = CreateFromClass(MakeGenericClass(List, Object))
    if self.m_ShowFirstActiveSkillSuite then
        list1 = normalSkills
        for k = 1, #activeSkillIds2 do
            CommonDefs.ListAdd(list2, typeof(UInt32), activeSkillIds2[k])
        end
    else
        list2 = normalSkills
        for k = 1, #activeSkillIds1 do
            CommonDefs.ListAdd(list1, typeof(UInt32), activeSkillIds1[k])
        end
    end
    if LuaSkillMgr.CheckMainPlayerYingLingBianShenSkill() and self:GetCurSwitchYingLingState() ~= EnumToInt(mainPlayer.CurYingLingState) then
        local list_1 = CreateFromClass(MakeGenericClass(List, Object))
        for i = 0, list1.Count - 1 do
            CommonDefs.ListAdd(list_1, typeof(UInt32), list1[i])
        end

        local list_2 = CreateFromClass(MakeGenericClass(List, Object))
        for i = 0, list2.Count - 1 do
            CommonDefs.ListAdd(list_2, typeof(UInt32), list2[i])
        end

        Gac2Gas.RequestSaveYingLingExtraSkillTemplate(skillProperty.CurrentTemplateIdx, self:GetCurSwitchYingLingState(), MsgPackImpl.pack(list_1), MsgPackImpl.pack(list_2), MsgPackImpl.pack(tianfuSkills))
    else
        Gac2Gas.RequestSaveSkill2Template(skillProperty.CurrentTemplateIdx, skillProperty.CurrentSkillTemplateName, MsgPackImpl.pack(list1), MsgPackImpl.pack(tianfuSkills), MsgPackImpl.pack(list2))
    end
end

function LuaSkillPreferenceViewSkillButtonBoard:ReplaceSkill(data)
    local srcSkillId, go, srcActiveSkillIndex = data[1], data[2], data[3]
    local mainPlayer = CClientMainPlayer.Inst
    if srcSkillId == SkillButtonSkillType_lua.NoSkill or mainPlayer == nil or Skill_AllSkills.GetData(srcSkillId) == nil then
        return
    end
    local skillProperty = mainPlayer.SkillProp
    local skillIds = self.m_ShowFirstActiveSkillSuite and skillProperty.NewActiveSkill or skillProperty.NewActiveSkill2

    local curSkillIds = {}
    for i = 1, skillIds.Length - 1 do
        table.insert(curSkillIds, skillIds[i])
    end
    local activeSkillIds1 = {}
    for i = 1, skillProperty.NewActiveSkill.Length - 1 do
        table.insert(activeSkillIds1, skillProperty.NewActiveSkill[i])
    end
    local activeSkillIds2 = {}
    for i = 1, skillProperty.NewActiveSkill2.Length - 1 do
        table.insert(activeSkillIds2, skillProperty.NewActiveSkill2[i])
    end

    if LuaSkillMgr.CheckMainPlayerYingLingBianShenSkill() and self.m_YingLingCurSwitchSkillIds then
        curSkillIds, activeSkillIds1, activeSkillIds2 = self:GetYingLingActiveSkills(curSkillIds, activeSkillIds1, activeSkillIds2)

        if curSkillIds == nil or activeSkillIds1 == nil or activeSkillIds2 == nil then
            return
        end
    end
    local needSwitch = false
    if srcActiveSkillIndex then
        needSwitch = ((srcActiveSkillIndex > 0) and (srcActiveSkillIndex <= #curSkillIds) and (curSkillIds[srcActiveSkillIndex] == srcSkillId))
    end
    for i = 1,#self.m_SkillButtons do
        if self.m_SkillButtons[i].activeSkillIcon.gameObject:Equals(go) then
            local destSkillId = curSkillIds[i]
            if destSkillId ~= SkillButtonSkillType_lua.NoSkill and destSkillId ~= srcSkillId then
                local dstRemain = mainPlayer.CooldownProp:GetServerRemainTime(destSkillId)
                if dstRemain <= 0 then
                    local normalSkills = CreateFromClass(MakeGenericClass(List, Object))
                    if not needSwitch then
                        for j = 1, #curSkillIds do
                            CommonDefs.ListAdd(normalSkills, typeof(UInt32), (j == i) and srcSkillId or curSkillIds[j])
                        end
                    else
                        for j = 1,#curSkillIds do
                            if j == i then
                                CommonDefs.ListAdd(normalSkills, typeof(UInt32), srcSkillId)
                            elseif j == srcActiveSkillIndex then
                                CommonDefs.ListAdd(normalSkills, typeof(UInt32), curSkillIds[i])
                            else
                                CommonDefs.ListAdd(normalSkills, typeof(UInt32), curSkillIds[j])
                            end
                        end
                    end

                    self:RequestSaveSkills(activeSkillIds1, activeSkillIds2, normalSkills)
                    CGuideScheduler.bSkillDragGuideReplaceCompleted = true
                else
                    g_MessageMgr:ShowMessage("SKILL_CHANGE_FAILED_COOLDOWN")
                end
            end
            break
        end
    end
end
----------------------------------------
---引导相关
----------------------------------------
function LuaSkillPreferenceViewSkillButtonBoard:GetSwitchSkill()
    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer == nil then
        return nil
    end
    local findIds = InitializeList(CreateFromClass(MakeGenericClass(List, UInt32)), UInt32, 90100001, 90200001, 90500001, 90600001, 90700001, 90800001)
    local skillIds = mainPlayer.SkillProp.NewActiveSkill
    --ActiveSkill索引从1开始，第0个位置占位，无用
    local findIndex = 2
    for i = 1,skillIds.Length - 1 do
        local skillId = skillIds[i]
        if CommonDefs.ListContains(findIds, typeof(UInt32), skillId) then
            findIndex = i
            break
        end
    end
    return self.m_SkillButtons[findIndex].activeSkillIcon.gameObject
end

function LuaSkillPreferenceViewSkillButtonBoard:GetSwitchButton()
    if self.m_SwitchActiveSkillBtn.gameObject.activeSelf then
        return self.m_SwitchActiveSkillBtn.gameObject
    end
    if CGuideMgr.Inst:IsInPhase(GuideDefine.Phase_SkillSuiteOpen) then
        CGuideMgr.Inst:EndCurrentPhase()
    end
    return nil
end
