local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local Enum = import "System.Enum"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaVehicleRenewalWnd = class()

RegistChildComponent(LuaVehicleRenewalWnd, "DescriptionLabel1", "DescriptionLabel1", UILabel)
RegistChildComponent(LuaVehicleRenewalWnd, "DescriptionLabel2", "DescriptionLabel2", UILabel)
RegistChildComponent(LuaVehicleRenewalWnd, "CancelButton", "CancelButton", GameObject)
RegistChildComponent(LuaVehicleRenewalWnd, "OkButton", "OkButton", GameObject)
RegistChildComponent(LuaVehicleRenewalWnd, "QnCostAndOwnMoney", "QnCostAndOwnMoney", CCurentMoneyCtrl)
RegistChildComponent(LuaVehicleRenewalWnd, "QnCostAndOwnJifen", "QnCostAndOwnJifen", CCurentMoneyCtrl)

RegistClassMember(LuaVehicleRenewalWnd,  "m_RenewalDay")

function LuaVehicleRenewalWnd:Awake()
	UIEventListener.Get(self.CancelButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCancelButtonClick()
	end)

	UIEventListener.Get(self.OkButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOkButtonClick()
	end)
end

function LuaVehicleRenewalWnd:Init()
	local data = ZuoQi_ZuoQi.GetData(LuaVehicleRenewalMgr.m_RenewalZuoqiData.templateId)
	local scoreType = Enum.Parse(typeof(EnumPlayScoreKey), data.RenewalScore[1])
	local expTime = LuaVehicleRenewalMgr.m_RenewalZuoqiData.expTime
	local dt = CServerTimeMgr.ConvertTimeStampToZone8Time(expTime)
	local renewalCutOffTime = CServerTimeMgr.Inst:GetDayBegin(dt)
	renewalCutOffTime = renewalCutOffTime:AddDays(data.RenewalRange[0] + 1)
	local timeStr = System.String.Format("{0:yyyy-MM-dd}", renewalCutOffTime)
	local renewalCount = data.RenewalAvailableTime[1] - LuaVehicleRenewalMgr.m_RenewalZuoqiData.renewalCount;
	local canLengthenTime = data.RenewalAvailableTime[0]
	local timeStr2 = System.String.Format("{0:yyyy-MM-dd}", CServerTimeMgr.Inst:GetZone8Time():AddDays(canLengthenTime))
	self.m_RenewalDay = data.RenewalRange[0]
	self.QnCostAndOwnJifen:SetType(EnumMoneyType.Score, scoreType, true)
	self.QnCostAndOwnJifen:SetCost(tonumber(data.RenewalScore[0]))
	self.QnCostAndOwnMoney:SetCost(data.RenewalPrice * tonumber(data.RenewalRange[0]))
	self.DescriptionLabel2.text = g_MessageMgr:FormatMessage("Message_VehicleRenewal_Notice",timeStr, renewalCount, canLengthenTime, timeStr2)
	self.DescriptionLabel1.text = g_MessageMgr:FormatMessage("Message_VehicleRenewal_Title")
end

--@region UIEvent

function LuaVehicleRenewalWnd:OnCancelButtonClick()
	CUIManager.CloseUI(CLuaUIResources.VehicleRenewalWnd)
end

function LuaVehicleRenewalWnd:OnOkButtonClick()
	Gac2Gas.RequestRenewMoreZuoQiBegin();
    Gac2Gas.RequestRenewMoreZuoQi(LuaVehicleRenewalMgr.m_RenewalZuoqiData.id, self.m_RenewalDay);
	Gac2Gas.RequestRenewMoreZuoQiEnd();
	CUIManager.CloseUI(CLuaUIResources.VehicleRenewalWnd)
end

--@endregion

LuaVehicleRenewalMgr = {}
LuaVehicleRenewalMgr.m_RenewalZuoqiData = nil

function LuaVehicleRenewalMgr:OpenVehicleRenewalWnd(data)
	self.m_RenewalZuoqiData = data
	CUIManager.ShowUI(CLuaUIResources.VehicleRenewalWnd)
end

LuaVehicleRenewalMgr.m_RenewZuoQiByItemWndData = {}
function LuaVehicleRenewalMgr:OpenRenewZuoQiByItemWnd(place, pos, itemId, dataUD, days)
	print("OpenRenewZuoQiByItemWnd")
	-- days 是续费天数
	local data = g_MessagePack.unpack(dataUD)

	local zuoqiDataList = {}
	for i = 1, #data, 3 do
		print("zuoqiId ", data[i])
		print("zuoqiTemplateId ", data[i+1])
		print("expireTime ", data[i+2])
		table.insert(zuoqiDataList, {id = data[i], templateId = data[i+1], expTime = data[i+2]})
	end
	self.m_RenewZuoQiByItemWndData = {place = place, pos = pos, itemId = itemId, days = days, zuoqiDataList = zuoqiDataList}
	CUIManager.ShowUI(CLuaUIResources.VehicleBatchRenewalFeesWnd)
	--Gac2Gas.RequestRenewZuoQiByItem(place, pos, itemId, zuoqiId)
end