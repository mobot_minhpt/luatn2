local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local Physics = import "UnityEngine.Physics"
local CGuideMgr=import "L10.Game.Guide.CGuideMgr"

LuaFSCCardDetailWnd = class()

LuaFSCCardDetailWnd.s_CardId = 0
LuaFSCCardDetailWnd.s_IsRare = nil
LuaFSCCardDetailWnd.s_Status = nil
LuaFSCCardDetailWnd.s_Borrowed = false

RegistClassMember(LuaFSCCardDetailWnd, "m_FSCCard")
RegistClassMember(LuaFSCCardDetailWnd, "m_Name")
RegistClassMember(LuaFSCCardDetailWnd, "m_Dot")
RegistClassMember(LuaFSCCardDetailWnd, "m_Subject")
RegistClassMember(LuaFSCCardDetailWnd, "m_Judgment")
RegistClassMember(LuaFSCCardDetailWnd, "m_Compose")
RegistClassMember(LuaFSCCardDetailWnd, "m_SwitchBtn")

RegistClassMember(LuaFSCCardDetailWnd, "m_Page")
RegistClassMember(LuaFSCCardDetailWnd, "m_Page1")
RegistClassMember(LuaFSCCardDetailWnd, "m_Page2")

function LuaFSCCardDetailWnd:Awake()
    self.m_Page1 = self.transform:Find("Background/Page1")
    self.m_Page2 = self.transform:Find("Background/Page2")
    self.m_FSCCard = self.transform:Find("Background/Page1/FSCCard"):GetComponent(typeof(CCommonLuaScript))
    self.m_Name = self.transform:Find("Background/Page1/Info/Name"):GetComponent(typeof(UILabel))
    self.m_Dot = self.transform:Find("Background/Page1/Info/Name/Name"):GetComponent(typeof(UILabel))
    self.m_Subject = self.transform:Find("Background/Page1/Info/Name/Name (1)"):GetComponent(typeof(UILabel))
    self.m_Judgment = self.transform:Find("Background/Page1/Info/Judgment"):GetComponent(typeof(UILabel))
    self.m_Compose = self.m_Page1:Find("Info/Compose")
    self.m_SwitchBtn = self.transform:Find("Background/SwitchBtn").gameObject
    self.m_Page = 1
    

    UIEventListener.Get(self.m_SwitchBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self.m_Page = 3 - self.m_Page
        self.m_Page1.gameObject:SetActive(self.m_Page == 1)
        self.m_Page2.gameObject:SetActive(self.m_Page == 2)
        self.m_SwitchBtn.transform:Find("Label"):GetComponent(typeof(UILabel)).text = self.m_Page == 1 and LocalString.GetString("查看相关组合") or LocalString.GetString("查看卡牌详情")
    end)
end

function LuaFSCCardDetailWnd:Init()
    if LuaFSCCardDetailWnd.s_CardId > 0 then
        self.m_FSCCard.m_LuaSelf:Init(LuaFSCCardDetailWnd.s_CardId, LuaFSCCardDetailWnd.s_IsRare, LuaFSCCardDetailWnd.s_Status, LuaFSCCardDetailWnd.s_Borrowed)
        local data = LuaFourSeasonCardMgr:GetCardData(LuaFSCCardDetailWnd.s_CardId)
        local color = LuaFourSeasonCardMgr:GetSeasonColor(data.Season)
        self.m_Name.color = color
        self.m_Dot.color = color
        self.m_Subject.color = color
        self.m_Name.text = data.Title
        if LuaFSCCardDetailWnd.s_IsRare then
            self.m_Subject.text = LuaFourSeasonCardMgr:GetSubject(data.SpecialTitle)
        else
            self.m_Dot.gameObject:SetActive(false)
            self.m_Subject.gameObject:SetActive(false)
        end
        self.m_Judgment.text = LuaFSCCardDetailWnd.s_IsRare and data.SpecialDescription or data.Description 

        local cards, combinations = LuaFourSeasonCardMgr:GetCardCombination(LuaFSCCardDetailWnd.s_CardId)

        local grid1 = self.m_Compose:Find("Grid"):GetComponent(typeof(UIGrid))
        Extensions.RemoveAllChildren(grid1.transform)
        local template1 = self.m_Compose:Find("Template").gameObject
        template1:SetActive(false)
        for cardId, _ in pairs(cards) do --wyh
            local obj = NGUITools.AddChild(grid1.gameObject, template1)
            obj:SetActive(true)
            obj:GetComponent(typeof(UILabel)).text = LuaFourSeasonCardMgr:GetCardData(cardId).Title
            if not LuaFourSeasonCardMgr.CollectedCards[LuaFourSeasonCardMgr.SelfInfo.gameId][cardId] then
                CUICommonDef.SetActive(obj, false)
            end
        end
        grid1:Reposition()

        local grid2 = self.m_Page2:Find("ScrollView/Grid"):GetComponent(typeof(UIGrid))
        Extensions.RemoveAllChildren(grid2.transform)
        local template2 = self.m_Page2:Find("ScrollView/Template").gameObject
        template2:SetActive(false)
        for combineId, _ in pairs(combinations) do
            local obj = NGUITools.AddChild(grid2.gameObject, template2)
            local data = ZhouNianQing2023_CardCombination.GetData(combineId)
            obj:SetActive(true)
            obj.transform:Find("Name"):GetComponent(typeof(UILabel)).text = data.Name
            obj.transform:Find("Score"):GetComponent(typeof(UILabel)).text = "+"..data.Value
            local grid = obj.transform:Find("Grid"):GetComponent(typeof(UIGrid))
            local template = obj.transform:Find("Template").gameObject
            template:SetActive(false)
            for i = 0, data.CardId.Length - 1 do
                local cardId = data.CardId[i]
                local go = NGUITools.AddChild(grid.gameObject, template)
                go:SetActive(true)
                go:GetComponent(typeof(UILabel)).text = LuaFourSeasonCardMgr:GetCardData(cardId).Title
                if not LuaFourSeasonCardMgr.CollectedCards[LuaFourSeasonCardMgr.SelfInfo.gameId][cardId] then
                    CUICommonDef.SetActive(go, false)
                end
            end
            grid:Reposition()
        end
        grid2.transform:GetChild(grid2.transform.childCount - 1):Find("SplitLine").gameObject:SetActive(false)
        grid2:Reposition() 
    end
end

function LuaFSCCardDetailWnd:Update()
    if Input.GetMouseButtonDown(0) then
        local cam = UICamera.currentCamera
        local hits = Physics.RaycastAll(cam:ScreenPointToRay(Input.mousePosition), cam.farClipPlane, cam.cullingMask)
        if hits then
            for i = 0, hits.Length - 1 do
                if NGUITools.IsChild(self.transform, hits[i].collider.transform) then
                    return
                end
            end
            CUIManager.CloseUI(CLuaUIResources.FSCCardDetailWnd)
        end
    end
end
