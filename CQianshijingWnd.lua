-- Auto Generated!!
local CChatLinkMgr = import "CChatLinkMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CQianshijingWnd = import "L10.UI.CQianshijingWnd"
local EnumNumber = import "L10.Game.EnumNumber"
local Gac2Gas = import "L10.Game.Gac2Gas"
local LocalString = import "LocalString"
local ZhouMoActivity_ClientSetting = import "L10.Game.ZhouMoActivity_ClientSetting"
CQianshijingWnd.m_Init_CS2LuaHook = function (this) 
    if CQianshijingWnd.openState then
        this.titleLabel.text = CChatLinkMgr.TranslateToNGUIText(g_MessageMgr:FormatMessage("ZhouMoActivity_Submit_1", EnumNumber.GetNum(CQianshijingWnd.nextStage), CQianshijingWnd.needCount), false)
    else
        if CQianshijingWnd.needCount == 0 then
            this.titleLabel.text = CChatLinkMgr.TranslateToNGUIText(g_MessageMgr:FormatMessage("ZhouMoActivity_Submit_2", EnumNumber.GetNum(CQianshijingWnd.nextStage - 1), EnumNumber.GetNum(CQianshijingWnd.nextStage)), false)
        else
            local totalHours = CQianshijingWnd.needCount
            local day = math.floor(totalHours/24)
            local hour = totalHours % 24
            this.titleLabel.text = CChatLinkMgr.TranslateToNGUIText(g_MessageMgr:FormatMessage("ZhouMoActivity_Submit_3", CQianshijingWnd.nextStage - 1, CQianshijingWnd.nextStage, day, hour), false)           
        end
    end
end
CQianshijingWnd.m_OnSubmitButtonClick_CS2LuaHook = function (this, go) 

    local bindCount, notBindCount
    bindCount, notBindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(System.UInt32.Parse(ZhouMoActivity_ClientSetting.GetData("Need_Item_TemplateId").Value))
    if bindCount + notBindCount > 0 then
        CLuaNumberInputMgr.ShowNumInputBox(1, bindCount + notBindCount, 1, function (amount) 
            Gac2Gas.RequestSubmitZhouMoActivityCHJP(amount)
        end, LocalString.GetString("请输入要提交的数量"), -1)
    else
        g_MessageMgr:ShowMessage("NO_ENOUGH_CHJP")
    end
end
