local CTeamMgr           = import "L10.Game.CTeamMgr"
local QnModelPreviewer   = import "L10.UI.QnModelPreviewer"
local CEffectMgr         = import "L10.Game.CEffectMgr"
local EnumWarnFXType     = import "L10.Game.EnumWarnFXType"
local EnumGender         = import "L10.Game.EnumGender"
local CSocialWndMgr      = import "L10.UI.CSocialWndMgr"
local EChatPanel         = import "L10.Game.EChatPanel"
local CThumbnailChatItem = import "L10.UI.CThumbnailChatItem"
local CChatMgr           = import "L10.Game.CChatMgr"
local Message            = import "L10.UI.CThumbnailChatWnd+Message"
local Object             = import "System.Object"
local CChatLinkMgr       = import "CChatLinkMgr"
local LinkSource         = import "CChatLinkMgr.LinkSource"
local CButton            = import "L10.UI.CButton"
local CIMMgr             = import "L10.Game.CIMMgr"
local Animation           = import "UnityEngine.Animation"

LuaArenaMainWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaArenaMainWnd, "main")
RegistClassMember(LuaArenaMainWnd, "modeSelect")

RegistClassMember(LuaArenaMainWnd, "playerInfo_WinNum")
RegistClassMember(LuaArenaMainWnd, "playerInfo_FavorNum")
RegistClassMember(LuaArenaMainWnd, "playerInfo_DanLarge")
RegistClassMember(LuaArenaMainWnd, "playerInfo_DanSmall")
RegistClassMember(LuaArenaMainWnd, "passport_Open")
RegistClassMember(LuaArenaMainWnd, "passport_NotOpen")
RegistClassMember(LuaArenaMainWnd, "passport_Slider")
RegistClassMember(LuaArenaMainWnd, "passport_WeekScore")
RegistClassMember(LuaArenaMainWnd, "passport_WinScore")
RegistClassMember(LuaArenaMainWnd, "passport_FailScore")
RegistClassMember(LuaArenaMainWnd, "passport_RedDot")
RegistClassMember(LuaArenaMainWnd, "seasonTask_Progress")
RegistClassMember(LuaArenaMainWnd, "seasonTask_Template")
RegistClassMember(LuaArenaMainWnd, "seasonTask_Table")
RegistClassMember(LuaArenaMainWnd, "seasonTask_RedDot")
RegistClassMember(LuaArenaMainWnd, "shopScore")
RegistClassMember(LuaArenaMainWnd, "team_CreateTeamButton")
RegistClassMember(LuaArenaMainWnd, "team_TeamInfo")
RegistClassMember(LuaArenaMainWnd, "team_Template")
RegistClassMember(LuaArenaMainWnd, "team_Num")
RegistClassMember(LuaArenaMainWnd, "team_Grid")
RegistClassMember(LuaArenaMainWnd, "team_Animation")
RegistClassMember(LuaArenaMainWnd, "friend_Template")
RegistClassMember(LuaArenaMainWnd, "friend_Table")
RegistClassMember(LuaArenaMainWnd, "modelPreview")
RegistClassMember(LuaArenaMainWnd, "fxKeys")
RegistClassMember(LuaArenaMainWnd, "startButton")
RegistClassMember(LuaArenaMainWnd, "matchingButton")
RegistClassMember(LuaArenaMainWnd, "matchingVfx")
RegistClassMember(LuaArenaMainWnd, "chat_ChatItem")
RegistClassMember(LuaArenaMainWnd, "chat_ScrollView")
RegistClassMember(LuaArenaMainWnd, "chat_Table")

RegistClassMember(LuaArenaMainWnd, "modeSelect_ComingSoon")
RegistClassMember(LuaArenaMainWnd, "modeSelect_Play")
RegistClassMember(LuaArenaMainWnd, "modeSelect_Button")
RegistClassMember(LuaArenaMainWnd, "modeSelect_ScrollView")
RegistClassMember(LuaArenaMainWnd, "modeSelect_Grid")
RegistClassMember(LuaArenaMainWnd, "modeSelect_Matching")
RegistClassMember(LuaArenaMainWnd, "modeSelect_CurrentSelect")
RegistClassMember(LuaArenaMainWnd, "modeSelect_Id2Trans")

RegistClassMember(LuaArenaMainWnd, "messages")
RegistClassMember(LuaArenaMainWnd, "friendData")
RegistClassMember(LuaArenaMainWnd, "matchType")
RegistClassMember(LuaArenaMainWnd, "isPlayOpen")
RegistClassMember(LuaArenaMainWnd, "openedTypeData")
RegistClassMember(LuaArenaMainWnd, "weekStarDict")
RegistClassMember(LuaArenaMainWnd, "currentWnd")
RegistClassMember(LuaArenaMainWnd, "rankScore")

function LuaArenaMainWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

    self.main = self.transform:Find("Main").gameObject
    self.main:SetActive(true)
    self.modeSelect = self.transform:Find("ModeSelect").gameObject
    self.modeSelect:SetActive(false)
    self.currentWnd = "Main"

    self:InitPlayerInfo()
    self:InitLeftCenter()
    self:InitTeam()
    self:InitFriend()
    self:InitModelPreview()
    self:InitChat()

    self.startButton = self.transform:Find("Main/Btns/StartButton"):GetComponent(typeof(CButton))
    self.matchingButton = self.transform:Find("Main/Btns/MatchingButton"):GetComponent(typeof(CButton))
    self.matchingVfx = self.matchingButton.transform:Find("Matching").gameObject
    self.startButton.gameObject:SetActive(false)
    self.matchingButton.gameObject:SetActive(false)
    UIEventListener.Get(self.startButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnStartButtonClick()
    end)
    UIEventListener.Get(self.matchingButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnMatchingButtonClick()
    end)
    self:InitModeSelect()
    Gac2Gas.QueryNewArenaInfo()
end

function LuaArenaMainWnd:InitPlayerInfo()
    local playerInfo = self.transform:Find("Main/PlayerInfo")
    local portrait = playerInfo:Find("Container/Portrait"):GetComponent(typeof(CUITexture))
    portrait:LoadPortrait(CClientMainPlayer.Inst and CClientMainPlayer.Inst.PortraitName, false)
    local name = playerInfo:Find("Container/Name"):GetComponent(typeof(UILabel))
    name.text = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Name or ""
    local level = playerInfo:Find("Container/Portrait/Level"):GetComponent(typeof(UILabel))
    level.text = CUICommonDef.GetMainPlayerColoredLevelString(Color.white)
    self.playerInfo_WinNum = playerInfo:Find("Container/WinNum"):GetComponent(typeof(UILabel))
    self.playerInfo_WinNum.text = ""
    self.playerInfo_FavorNum = playerInfo:Find("Container/FavorNum"):GetComponent(typeof(UILabel))
    self.playerInfo_FavorNum.text = ""
    self.playerInfo_DanLarge = playerInfo:Find("Container/Dan/Large"):GetComponent(typeof(CUITexture))
    self.playerInfo_DanLarge:LoadMaterial("")
    self.playerInfo_DanSmall = playerInfo:Find("Container/Dan/Small"):GetComponent(typeof(CUITexture))
    self.playerInfo_DanSmall:LoadMaterial("")
    UIEventListener.Get(playerInfo.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnPlayerInfoClick()
    end)
end

function LuaArenaMainWnd:InitLeftCenter()
    local leftCenter = self.transform:Find("Main/LeftCenter")
    local passport = leftCenter:Find("Passport")
    self.passport_Open = passport:Find("Open").gameObject
    self.passport_NotOpen = passport:Find("NotOpen").gameObject
    self.passport_Open:SetActive(false)
    self.passport_NotOpen:SetActive(false)
    self.passport_Slider = passport:Find("Open/Slider"):GetComponent(typeof(UISlider))
    self.passport_WeekScore = passport:Find("Open/Slider/Score"):GetComponent(typeof(UILabel))
    self.passport_WinScore = passport:Find("Open/WinScore"):GetComponent(typeof(UILabel))
    self.passport_FailScore = passport:Find("Open/FailScore"):GetComponent(typeof(UILabel))
    self.passport_RedDot = passport:Find("RedDot").gameObject
    self.passport_RedDot:SetActive(false)
    UIEventListener.Get(passport.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnPassportClick()
    end)

    local seasonTask = leftCenter:Find("SeasonTask")
    self.seasonTask_Progress = seasonTask:Find("Progress"):GetComponent(typeof(UILabel))
    self.seasonTask_Progress.text = ""
    self.seasonTask_Template = seasonTask:Find("Template").gameObject
    self.seasonTask_Template:SetActive(false)
    self.seasonTask_Table = seasonTask:Find("Table"):GetComponent(typeof(UITable))
    self.seasonTask_RedDot = seasonTask:Find("RedDot").gameObject
    self.seasonTask_RedDot:SetActive(false)
    UIEventListener.Get(seasonTask.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnSeasonTaskClick()
    end)

    UIEventListener.Get(leftCenter:Find("Buttons/RuleAwardButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnRuleAwardButtonClick()
    end)
    self.shopScore = leftCenter:Find("Buttons/ShopButton/Score"):GetComponent(typeof(UILabel))
    self.shopScore.text = ""
    UIEventListener.Get(leftCenter:Find("Buttons/ShopButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnShopButtonClick()
    end)
    UIEventListener.Get(leftCenter:Find("Buttons/RankButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnRankButtonClick()
    end)

    self.weekStarDict = LuaArenaMgr:GetWeekStarDict()
end

function LuaArenaMainWnd:InitTeam()
    local team = self.transform:Find("Team/Container")
    self.team_CreateTeamButton = team:Find("CreateTeamButton").gameObject
    self.team_TeamInfo = team:Find("TeamInfo").gameObject
    self.team_Template = team:Find("TeamInfo/Template").gameObject
    self.team_Template:SetActive(false)
    self.team_Num = team:Find("TeamInfo/Num"):GetComponent(typeof(UILabel))
    self.team_Grid = team:Find("TeamInfo/Grid"):GetComponent(typeof(UIGrid))
    self.team_Animation = self.transform:Find("Team"):GetComponent(typeof(Animation))

    UIEventListener.Get(self.team_CreateTeamButton).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnCreateTeamButtonClick()
    end)
    UIEventListener.Get(self.team_TeamInfo).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnTeamInfoClick()
    end)
    self:UpdateTeam()
end

function LuaArenaMainWnd:UpdateTeam()
    if not CTeamMgr.Inst:TeamExists() then
        self.team_CreateTeamButton:SetActive(true)
        self.team_TeamInfo:SetActive(false)
    else
        self.team_CreateTeamButton:SetActive(false)
        self.team_TeamInfo:SetActive(true)

        self.team_Num.text = SafeStringFormat3(LocalString.GetString("%d/5\n队伍"), CTeamMgr.Inst.TotalMemebersCount)
        local teamMemberList = CTeamMgr.Inst:GetTeamMembersExceptMe()
        local count = teamMemberList.Count
        if count > 0 then
            self:AddChild(self.team_Grid.gameObject, self.team_Template, count)
            for i = 1, count do
                local member = teamMemberList[i - 1]
                local child = self.team_Grid.transform:GetChild(i - 1)
                local portrait = child:Find("Portrait"):GetComponent(typeof(CUITexture))
                portrait:LoadPortrait(CUICommonDef.GetPortraitName(member.m_MemberClass, member.m_MemberGender, member.m_Expression), true)
                local level = child:Find("Level"):GetComponent(typeof(UILabel))
                level.text = CUICommonDef.GetColoredLevelString(member.IsInXianShenStatus, member.m_MemberLevel, Color.white)
                local leader = child:Find("Leader").gameObject
                leader:SetActive(CTeamMgr.Inst.LeaderId == member.m_MemberId)
            end
            self.team_Grid:Reposition()
        else
            self:AddChild(self.team_Grid.gameObject, self.team_Template, 0)
        end
    end
end

function LuaArenaMainWnd:InitFriend()
    local friend = self.transform:Find("Main/Friend")
    self.friend_Template = friend:Find("Container/Template").gameObject
    self.friend_Template:SetActive(false)
    self.friend_Table = friend:Find("Container/Grid"):GetComponent(typeof(UIGrid))

    UIEventListener.Get(friend.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnFriendClick()
    end)

    UIEventListener.Get(friend:Find("Container/ArrowBtn").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnFriendClick()
    end)
end

function LuaArenaMainWnd:InitModelPreview()
    if CClientMainPlayer.Inst then
        self.modelPreview = self.transform:Find("Main/ModelPreview"):GetComponent(typeof(QnModelPreviewer))
        local appearance = CClientMainPlayer.Inst.AppearanceProp:Clone()
        appearance.YingLingState = EnumYingLingState.eDefault
        self.modelPreview.IsShowHighRes = true
        self.modelPreview:PreviewFakeAppear(appearance)
        if not LuaArenaMgr.hasPlayedExpression then
            self.fxKeys = {}
            self:ShowExpressionAction(Arena_Setting.GetData().ExpressionInMainWnd)
            LuaArenaMgr.hasPlayedExpression = true
        end
    end
end

function LuaArenaMainWnd:InitChat()
    local chat = self.transform:Find("Main/Chat/Container")
    self.chat_ChatItem = chat:Find("Bg/ChatItem").gameObject
    self.chat_ChatItem.gameObject:SetActive(false)
    self.chat_ScrollView = chat:Find("Bg/ScrollView"):GetComponent(typeof(UIScrollView))
    self.chat_Table = chat:Find("Bg/ScrollView/Table"):GetComponent(typeof(UITable))

    UIEventListener.Get(chat:Find("Bg").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnChatBgClick()
    end)

    self.messages = {}
    local allMsgs = CChatMgr.Inst:GetAllChatMsgs()
    for i = 0, allMsgs.Count - 1 do
        local msg = allMsgs[i]
        if msg.channelName == CChatMgr.CHANNEL_NAME_TEAM or msg.channelName == CChatMgr.CHANNEL_NAME_ARENA then
            table.insert(self.messages, Message(msg))
        end
    end
    self:UpdateChat()
end

function LuaArenaMainWnd:UpdateChat()
    local num = #self.messages
    self:AddChild(self.chat_Table.gameObject, self.chat_ChatItem.gameObject, num)
    for i = 1, num do
        local chatItem = self.chat_Table.transform:GetChild(i - 1):GetComponent(typeof(CThumbnailChatItem))
        chatItem:InitWithMsg(self.messages[i])
        UIEventListener.Get(chatItem.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnChatItemClick(go, self.messages[i].channel)
        end)
    end
    self.chat_Table:Reposition()
    self.chat_ScrollView:ResetPosition()
end

function LuaArenaMainWnd:InitModeSelect()
    local modeSelect = self.transform:Find("ModeSelect")
    local desc = modeSelect:Find("Top/Desc"):GetComponent(typeof(UILabel))
    desc.text = g_MessageMgr:FormatMessage("ARENA_MODE_SELECT_DESC")
    UIEventListener.Get(modeSelect:Find("ReturnButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnReturnButtonClick()
    end)
    UIEventListener.Get(modeSelect:Find("TipButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnTipButtonClick()
    end)
    UIEventListener.Get(modeSelect:Find("Bottom/StartButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnModeSelectButtonClick()
    end)

    self.modeSelect_CurrentSelect = 0
    self.modeSelect_ComingSoon = modeSelect:Find("Pool/ComingSoon").gameObject
    self.modeSelect_ComingSoon:SetActive(false)
    self.modeSelect_Play = {}
    local count = Arena_OpenClosePlay.GetDataCount()
    for i = 1, count do
        local template = modeSelect:Find("Pool/Play_" .. i)
        if template then
            self.modeSelect_Play[i] = template.gameObject
            template.gameObject:SetActive(false)
        end
    end
    self.modeSelect_Button = modeSelect:Find("Bottom/StartButton"):GetComponent(typeof(CButton))
    self.modeSelect_ScrollView = modeSelect:Find("ScrollView"):GetComponent(typeof(UIScrollView))
    self.modeSelect_Grid = modeSelect:Find("ScrollView/Grid"):GetComponent(typeof(UIGrid))
    self.modeSelect_Matching = modeSelect:Find("Bottom/Matching").gameObject
end

-- 令一个ro做一个表情动作
function LuaArenaMainWnd:ShowExpressionAction(expressionId)
    local define = Expression_Define.GetData(expressionId)
    local ro = self.modelPreview.m_RO
    if not define then
        ro:DoAni(ro.NewRoleStandAni, true, 0, 1.0, 0.15, true, 1.0)
        return
    end

    local aniName = define.AniName
    ro.NeedUpdateAABB = true

    -- 特效
    local gender = self.modelPreview.m_FakeAppear.Gender
    local fxids = GetExpressionDefineFx(define, EnumToInt(gender))
    for i = 1, #fxids do
        local fx = CEffectMgr.Inst:AddObjectFX(fxids[i], ro, 0, 1.0, 1.0, nil, false, EnumWarnFXType.None,
        Vector3(0, 0, 0), Vector3(0, 0, 0), nil)
        ro:AddFX(aniName, fx, -1)
    end
    if self.fxKeys[aniName] == nil then
        self.fxKeys[aniName] = true
    end

    -- 手持物
    local additionalWeaponId = CClientMainPlayer.Inst.Gender == EnumGender.Male and define.MaleAdditionalEquip or define.FemaleAdditionalEquip
    local equip = EquipmentTemplate_Equip.GetData(additionalWeaponId)
    if equip then
        ro:AddChild(equip.Prefab, "weapon03", "weapon01")
    end

    ro:AddLoadAllFinishedListener(DelegateFactory.Action_CRenderObject(function(renderObj)
        local loop = define.Loop > 0
        local startTime = define.StartTime
        ro:DoAni(aniName, loop, startTime, 1.0, 0.15, false, 1.0)

        if not loop then
            if define.StopAtEnd <= 0 then
                local nextExpressionId = define.NextExpressionID

                if nextExpressionId > 0 then
                    ro.AniEndCallback = DelegateFactory.Action(function ()
                        ro.AniEndCallback = nil
                        self:ShowExpressionAction(nextExpressionId)
                    end)
                else
                    ro.AniEndCallback = DelegateFactory.Action(function ()
                        self:ClearExpression(ro)
                    end)
                end
            end
        end
    end))
end

-- 清除表情，包括手持物、特效等
function LuaArenaMainWnd:ClearExpression(ro)
    if not ro then return end

    ro:RemoveChild("weapon03")
    ro.AniEndCallback = nil
    ro:DoAni(ro.NewRoleStandAni, true, 0, 1.0, 0.15, true, 1.0)

    for key, _ in pairs(self.fxKeys) do
        ro:RemoveFX(key)
    end
end

function LuaArenaMainWnd:AddChild(parent, template, num)
    local childCount = parent.transform.childCount
    if childCount < num then
        for i = childCount + 1, num do
            NGUITools.AddChild(parent, template)
        end
    end

    childCount = parent.transform.childCount
    for i = 0, childCount - 1 do
        parent.transform:GetChild(i).gameObject:SetActive(i < num)
    end
end

function LuaArenaMainWnd:OnEnable()
    g_ScriptEvent:AddListener("TeamInfoChange", self, "UpdateTeam")
    g_ScriptEvent:AddListener("RecvChatMsg", self, "OnRecvChatMsg")
    g_ScriptEvent:AddListener("SendNewArenaInfo", self, "OnSendNewArenaInfo")
    g_ScriptEvent:AddListener("MainplayerRelationshipPropUpdate", self, "UpdateFriend")
    g_ScriptEvent:AddListener("MainplayerRelationshipBasicInfoUpdate", self, "UpdateFriend")
    g_ScriptEvent:AddListener("MainPlayerRelationship_InformOnline", self, "UpdateFriend")
    g_ScriptEvent:AddListener("SendArenaOpenInfo", self, "OnSendArenaOpenInfo")
    g_ScriptEvent:AddListener("SyncArenaSignupState", self, "OnSyncArenaSignupState")
    g_ScriptEvent:AddListener("ArenaEnterModeSelect", self, "EnterModeSelect")
    g_ScriptEvent:AddListener("SendArenaPassportInfo", self, "OnSendArenaPassportInfo")
    g_ScriptEvent:AddListener("ArenaTaskDataUpdate", self, "OnArenaTaskDataUpdate")
end

function LuaArenaMainWnd:OnDisable()
    g_ScriptEvent:RemoveListener("TeamInfoChange", self, "UpdateTeam")
    g_ScriptEvent:RemoveListener("RecvChatMsg", self, "OnRecvChatMsg")
    g_ScriptEvent:RemoveListener("SendNewArenaInfo", self, "OnSendNewArenaInfo")
    g_ScriptEvent:RemoveListener("MainplayerRelationshipPropUpdate", self, "UpdateFriend")
    g_ScriptEvent:RemoveListener("MainplayerRelationshipBasicInfoUpdate", self, "UpdateFriend")
    g_ScriptEvent:RemoveListener("MainPlayerRelationship_InformOnline", self, "UpdateFriend")
    g_ScriptEvent:RemoveListener("SendArenaOpenInfo", self, "OnSendArenaOpenInfo")
    g_ScriptEvent:RemoveListener("SyncArenaSignupState", self, "OnSyncArenaSignupState")
    g_ScriptEvent:RemoveListener("ArenaEnterModeSelect", self, "EnterModeSelect")
    g_ScriptEvent:RemoveListener("SendArenaPassportInfo", self, "OnSendArenaPassportInfo")
    g_ScriptEvent:RemoveListener("ArenaTaskDataUpdate", self, "OnArenaTaskDataUpdate")
end

function LuaArenaMainWnd:OnArenaTaskDataUpdate(activedWeek, starData)
    local taskData = {
        activedWeek = activedWeek,
        currentStar = starData.currentStar,
    }
    self:UpdateSeasonTask(taskData)
end

function LuaArenaMainWnd:OnRecvChatMsg(args)
    local msg = CChatMgr.Inst:GetLatestMsg()
    if msg.channelName == CChatMgr.CHANNEL_NAME_TEAM or msg.channelName == CChatMgr.CHANNEL_NAME_ARENA then
        table.insert(self.messages, 1, Message(msg))
    end
    self:UpdateChat()
end

function LuaArenaMainWnd:OnSendNewArenaInfo(rankScore, shopScore, winCount, favCount, isPlayOpen, matchType, passportData, taskData, friendData)
    self.rankScore = rankScore
    local largeDan, smallDan = LuaArenaMgr:GetLargeAndSmallDan(rankScore)
    self.playerInfo_DanLarge:LoadMaterial(Arena_DanInfo.GetData(largeDan).Icon)
    self.playerInfo_DanSmall:LoadMaterial(LuaArenaMgr.smallDanMatPaths[smallDan])
    self.playerInfo_WinNum.text = winCount
    self.playerInfo_FavorNum.text = favCount
    self.shopScore.text = shopScore

    self:UpdatePassport(g_MessagePack.unpack(passportData))
    self:UpdateSeasonTask(g_MessagePack.unpack(taskData))
    self.friendData = g_MessagePack.unpack(friendData)
    self:UpdateFriend()

    self.isPlayOpen = isPlayOpen
    self.matchType = matchType
    self:UpdateStartButtonState()
end

function LuaArenaMainWnd:OnSendArenaPassportInfo(curExp, curLevel, rewardedLevel, weekExp, weekExpLimit)
    self.passport_RedDot:SetActive(curLevel > rewardedLevel)
    self.passport_WeekScore.text = SafeStringFormat3(LocalString.GetString("本周 %d/%d"), weekExp, weekExpLimit)
    self.passport_Slider.value = weekExpLimit == 0 and 1 or math.min(weekExp / weekExpLimit, 1)
end

function LuaArenaMainWnd:UpdatePassport(passportData)
    self.passport_Open:SetActive(passportData.actived or false)
    self.passport_NotOpen:SetActive(not (passportData.actived or false))

    if passportData.actived then
        self.passport_RedDot:SetActive(passportData.hasReward or false)
        self.passport_WinScore.text = passportData.winExp or 0
        self.passport_FailScore.text = passportData.failExp or 0
        local maxWeeklyExp = passportData.weekExpLimit or 0
        self.passport_WeekScore.text = SafeStringFormat3(LocalString.GetString("本周 %d/%d"), passportData.weeklyExp or 0, maxWeeklyExp)
        self.passport_Slider.value = maxWeeklyExp == 0 and 1 or math.min(passportData.weeklyExp / maxWeeklyExp, 1)
    end
end

function LuaArenaMainWnd:UpdateSeasonTask(taskData)
    local totalStar = 18
    local currentStar = taskData.currentStar or 0
    self.seasonTask_Progress.text = SafeStringFormat3(LocalString.GetString("当前激活第%d周任务 %d/%d"), taskData.activedWeek or 1, currentStar, totalStar)
    Extensions.RemoveAllChildren(self.seasonTask_Table.transform)
    local weekStar = self.weekStarDict[taskData.activedWeek] or {}
    for i = 1, totalStar do
        local child = NGUITools.AddChild(self.seasonTask_Table.gameObject, self.seasonTask_Template)
        child:SetActive(true)
        local square = child.transform:Find("Square")
        local diamond = child.transform:Find("Diamond")
        local activeNode
        if weekStar[i] then
            square.gameObject:SetActive(false)
            diamond.gameObject:SetActive(true)
            activeNode = diamond
        else
            square.gameObject:SetActive(true)
            diamond.gameObject:SetActive(false)
            activeNode = square
        end
        activeNode:Find("Normal").gameObject:SetActive(i > currentStar)
        activeNode:Find("Highlight").gameObject:SetActive(i <= currentStar)
    end
    self.seasonTask_Table:Reposition()
    self.seasonTask_RedDot:SetActive(false)
end

function LuaArenaMainWnd:UpdateFriend()
    if not self.friendData then return end
    LuaArenaMgr:UpdateFriendInfos(self.friendData)
    local infos = LuaArenaMgr.friendInfos
    local num = math.min(#infos, 5)
    self:AddChild(self.friend_Table.gameObject, self.friend_Template, num)
    for i = 1, num do
        local basicInfo = CIMMgr.Inst:GetBasicInfo(infos[i].id)
        local child = self.friend_Table.transform:GetChild(i - 1)
        local portrait = child:Find("Portrait"):GetComponent(typeof(CUITexture))
        portrait:LoadPortrait(CUICommonDef.GetPortraitName(basicInfo.Class, basicInfo.Gender, basicInfo.Expression), true)
        local level = child:Find("Level"):GetComponent(typeof(UILabel))
        level.text = CUICommonDef.GetColoredLevelString(basicInfo.XianFanStatus > 0, basicInfo.Level, Color.white)
        LuaUtils.SetLocalPositionZ(child, infos[i].isOnline and 0 or -1)

        local danIcon = child:Find("Portrait/DanIcon")
        local rankScore = infos[i].rankScore
        danIcon.gameObject:SetActive(rankScore > 0)
        if rankScore > 0 then
            local largeDan, smallDan = LuaArenaMgr:GetLargeAndSmallDan(rankScore)
            danIcon:GetComponent(typeof(CUITexture)):LoadMaterial(Arena_DanInfo.GetData(largeDan).Icon)
        end
    end
    self.friend_Table:Reposition()
end

function LuaArenaMainWnd:OnSendArenaOpenInfo(matchType, openedTypeData)
    self.matchType = matchType
    openedTypeData = g_MessagePack.unpack(openedTypeData)
    self.openedTypeData = {}
    self.isPlayOpen = false
    for _, playId in pairs(openedTypeData) do
        self.openedTypeData[playId] = true
        self.isPlayOpen = true
    end
    if self.modeSelect.activeSelf then
        self:UpdateModeSelect()
    end
    self:UpdateStartButtonState()
end

function LuaArenaMainWnd:OnSyncArenaSignupState(matchType)
    self.matchType = matchType
    self:UpdateStartButtonState()

    if self.modeSelect.activeSelf and self.openedTypeData then
        if matchType > 0 and self.modeSelect_CurrentSelect ~= matchType then
            self:SetModelSelect(matchType)
        else
            self:UpdateModeSelectButtonState()
        end
    end
end

function LuaArenaMainWnd:UpdateStartButtonState()
    if self.isPlayOpen and self.matchType == 0 then
        self.startButton.gameObject:SetActive(true)
        self.matchingButton.gameObject:SetActive(false)
    else
        self.startButton.gameObject:SetActive(false)
        self.matchingButton.gameObject:SetActive(true)

        if self.matchType == 0 then
            self.matchingButton.Text = LocalString.GetString("开始竞技")
        else
            self.matchingButton.Text = SafeStringFormat3(LocalString.GetString("%s匹配中"), Arena_OpenClosePlay.GetData(self.matchType).PlayTypeName)
        end
        self.matchingVfx:SetActive(self.matchType ~= 0)
    end
end

function LuaArenaMainWnd:UpdateModeSelect()
    self.modeSelect_Id2Trans = {}
    Extensions.RemoveAllChildren(self.modeSelect_Grid.transform)
    self.modeSelect_Button.gameObject:SetActive(self.openedTypeData and true or false)
    self.modeSelect_Matching:SetActive(false)
    if self.openedTypeData then
        local count = Arena_OpenClosePlay.GetDataCount()
        local list = {}
        for i = 1, count do
            if Arena_OpenClosePlay.GetData(i).Status == 0 and self.openedTypeData[i] then
                table.insert(list, i)
            end
        end
        for i = 1, count do
            if Arena_OpenClosePlay.GetData(i).Status == 0 and not self.openedTypeData[i] then
                table.insert(list, i)
            end
        end
        for _, i in ipairs(list) do
            local data = Arena_OpenClosePlay.GetData(i)

            local child = NGUITools.AddChild(self.modeSelect_Grid.gameObject, self.modeSelect_Play[i])
            child:SetActive(true)

            child.transform:Find("Type"):GetComponent(typeof(UILabel)).text = data.PlayType
            child.transform:Find("OpenTime"):GetComponent(typeof(UILabel)).text = data.OpenTime
            child.transform:Find("Highlight").gameObject:SetActive(false)
            child.transform:Find("Highlight/VFX").gameObject:SetActive(self.matchType == i)

            self.modeSelect_Id2Trans[i] = child.transform
            UIEventListener.Get(child.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
                self:OnModeSelectClick(i)
            end)
            child.transform:Find("Lock").gameObject:SetActive(not self.openedTypeData[i])
        end
        local comingSoon = NGUITools.AddChild(self.modeSelect_Grid.gameObject, self.modeSelect_ComingSoon)
        comingSoon:SetActive(true)
        self.modeSelect_Grid:Reposition()

        local defaultSelect = 0
        if self.matchType > 0 then
            defaultSelect = self.matchType
        elseif self.modeSelect_Id2Trans[self.modeSelect_CurrentSelect] then
            defaultSelect = self.modeSelect_CurrentSelect
        else
            defaultSelect = list[1] or 0
        end
        self:SetModelSelect(defaultSelect)

        -- if defaultSelect > 0 then
        --     CUICommonDef.SetFullyVisible(self.modeSelect_Id2Trans[defaultSelect].gameObject, self.modeSelect_ScrollView)
        -- end
    end
end

function LuaArenaMainWnd:SetModelSelect(i)
    if self.modeSelect_CurrentSelect > 0 then
        local trans = self.modeSelect_Id2Trans[self.modeSelect_CurrentSelect]
        if trans then
            trans:Find("Highlight").gameObject:SetActive(false)
        end
    end

    self.modeSelect_CurrentSelect = i
    local trans = self.modeSelect_Id2Trans[i]
    if trans then
        trans:Find("Highlight").gameObject:SetActive(true)
    end
    self:UpdateModeSelectButtonState()
end

function LuaArenaMainWnd:UpdateModeSelectButtonState()
    local cuiTexture = self.modeSelect_Button.transform:GetComponent(typeof(CUITexture))
    if self.openedTypeData[self.modeSelect_CurrentSelect] and self.matchType == 0 then
        cuiTexture:LoadMaterial("UI/Texture/Transparent/Material/newmainwelfarewnd_btn_01.mat")
        self.modeSelect_Button.label.color = NGUIText.ParseColor24("351B01", 0)
    else
        cuiTexture:LoadMaterial("UI/Texture/Transparent/Material/multipleexpwnd_btn.mat")
        self.modeSelect_Button.label.color = NGUIText.ParseColor24("0E3254", 0)
    end

    self.modeSelect_Button.Text = self.matchType > 0 and LocalString.GetString("取消匹配") or LocalString.GetString("开始匹配")
    self.modeSelect_Matching:SetActive(self.matchType > 0)
    if self.matchType > 0 then
        self.modeSelect_Matching.transform:GetComponent(typeof(UILabel)).text = self.matchType == 1 and LocalString.GetString("单人战匹配中") or LocalString.GetString("团体战匹配中")
    end

    for id, trans in pairs(self.modeSelect_Id2Trans) do
        trans:Find("Highlight/VFX").gameObject:SetActive(self.matchType == id)
    end
end

function LuaArenaMainWnd:EnterModeSelect()
    self.main:SetActive(false)
    self.modeSelect:SetActive(true)
    self.currentWnd = "ModeSelect"
    self.team_Animation:Play()
    self:UpdateModeSelect()
    Gac2Gas.QueryArenaOpenInfo()
end

function LuaArenaMainWnd:Init()

end

function LuaArenaMainWnd:OnDestroy()
    LuaArenaMgr.friendInfos = {}
    Gac2Gas.RequestLeaveArenaChatChannel()
    CUIManager.CloseUI(CLuaUIResources.ArenaFriendWnd)
end

--@region UIEvent

function LuaArenaMainWnd:OnPlayerInfoClick()
    LuaArenaMgr:ShowPlayerInfoWnd(CClientMainPlayer.Inst.Id)
end

function LuaArenaMainWnd:OnPassportClick()
    CUIManager.ShowUI(CLuaUIResources.ArenaPassportWnd)
end

function LuaArenaMainWnd:OnSeasonTaskClick()
    CUIManager.ShowUI(CLuaUIResources.ArenaSeasonTaskWnd)
end

function LuaArenaMainWnd:OnRuleAwardButtonClick()
    LuaArenaMgr.awardInfo = {
        danScore = self.rankScore or -1,
        rank = 1
    }
    CUIManager.ShowUI(CLuaUIResources.ArenaRuleAwardWnd)
end

function LuaArenaMainWnd:OnShopButtonClick()
end

function LuaArenaMainWnd:OnRankButtonClick()
    CUIManager.ShowUI(CLuaUIResources.ArenaRankWnd)
end

function LuaArenaMainWnd:OnCreateTeamButtonClick()
    CTeamMgr.Inst:RequestCreateTeam()
    CUIManager.ShowUI(CUIResources.TeamWnd)
end

function LuaArenaMainWnd:OnTeamInfoClick()
    CUIManager.ShowUI(CUIResources.TeamWnd)
end

function LuaArenaMainWnd:OnFriendClick()
    if CUIManager.IsLoaded(CUIResources.SocialWnd) then
        CUIManager.CloseUI(CUIResources.SocialWnd)
    end
    CUIManager.ShowUI(CLuaUIResources.ArenaFriendWnd)
end

function LuaArenaMainWnd:OnStartButtonClick()
    self:EnterModeSelect()
end

function LuaArenaMainWnd:OnMatchingButtonClick()
    self:EnterModeSelect()
end

function LuaArenaMainWnd:OnChatBgClick()
    if CUIManager.IsLoaded(CLuaUIResources.ArenaFriendWnd) then
        CUIManager.CloseUI(CLuaUIResources.ArenaFriendWnd)
    end
    CSocialWndMgr.ShowChatWnd(EChatPanel.ARENA)
end

function LuaArenaMainWnd:OnChatItemClick(go, channel)
    local url = go.transform:GetComponent(typeof(UILabel)):GetUrlAtPosition(UICamera.lastWorldPosition)
    if url then
        local dict = CreateFromClass(MakeGenericClass(Dictionary, cs_string, Object))
        CommonDefs.DictAdd_LuaCall(dict, "chatchannel", EnumToInt(channel))
        if CChatLinkMgr.ProcessLinkClick(url, LinkSource.Chat, dict) then
            return
        end
    end
    if CUIManager.IsLoaded(CLuaUIResources.ArenaFriendWnd) then
        CUIManager.CloseUI(CLuaUIResources.ArenaFriendWnd)
    end
    CSocialWndMgr.ShowChatWnd(EChatPanel.ARENA)
end

function LuaArenaMainWnd:OnReturnButtonClick()
    self.main:SetActive(true)
    self.modeSelect:SetActive(false)
    self.currentWnd = "Main"
    self.seasonTask_Table:Reposition() -- 如果节点隐藏，且UITable勾选了Hide Inactive，那么Reposition不生效
    self.chat_Table:Reposition()
    self.chat_ScrollView:ResetPosition()
end

function LuaArenaMainWnd:OnTipButtonClick()
    g_MessageMgr:ShowMessage("ARENA_MODE_SELECT_TIP")
end

function LuaArenaMainWnd:OnModeSelectClick(i)
    if self.modeSelect_CurrentSelect > 0 and self.modeSelect_CurrentSelect == i then
        return
    end

    if self.modeSelect_CurrentSelect == self.matchType and self.matchType > 0 and i ~= self.matchType then
        g_MessageMgr:ShowMessage("ARENA_MODE_SELECT_IS_MATCHING")
        return
    end

    self:SetModelSelect(i)
end

function LuaArenaMainWnd:OnModeSelectButtonClick()
    if self.matchType > 0 and self.matchType == self.modeSelect_CurrentSelect then
        Gac2Gas.RequestCancelSignupArena()
    elseif self.matchType == 0 then
        Gac2Gas.RequestSignupArena(self.modeSelect_CurrentSelect)
    end
end

--@endregion UIEvent
