LuaValentine2022Mgr = {}

LuaValentine2022Mgr.m_IsSpecialDriftBottleItem = false --是否是特殊简帛
LuaValentine2022Mgr.m_IsShowDriftBottle = false --是否是查看包裹内漂流瓶道具
LuaValentine2022Mgr.m_DriftBottleItemId = ""
LuaValentine2022Mgr.m_DriftBottleItemPlace = 0
LuaValentine2022Mgr.m_DriftBottleItemPos = 0

function LuaValentine2022Mgr:ShowDriftBottleEditWnd(itemTemplateId, itemId, place, pos)
    self.m_DriftBottleItemId,self.m_DriftBottleItemPlace,self.m_DriftBottleItemPos = itemId, place, pos
    self.m_IsSpecialDriftBottleItem = itemTemplateId == Valentine_DriftBottle.GetData().SpecialJuanBoItemID
    self.m_IsShowDriftBottle = false
    CUIManager.ShowUI(CLuaUIResources.Valentine2022DriftBottleWnd)
end

LuaValentine2022Mgr.m_ShowDriftBottleData = nil
function LuaValentine2022Mgr:ShowDriftBottleViewWnd(anonymous, receiverId, receiverName, content, voiceUrl, voiceDuration, senderId, senderName, senderClass, senderGender, senderExpression, senderExpressionTxt, senderSticker)
    self.m_IsShowDriftBottle = true
    self.m_ShowDriftBottleData = {
        anonymous = anonymous, receiverId = receiverId, receiverName = receiverName, content = content,
        voiceUrl = voiceUrl, voiceDuration = voiceDuration, senderId = senderId, senderName = senderName, 
        senderClass = senderClass, senderGender = senderGender, senderExpression = senderExpression, 
        senderExpressionTxt = senderExpressionTxt, senderSticker = senderSticker
    }
    CUIManager.ShowUI(CLuaUIResources.Valentine2022DriftBottleWnd)
end

function LuaValentine2022Mgr:PutDriftBottleSuccess()
    CUIManager.CloseUI(CUIResources.PackageWnd)
end

function LuaValentine2022Mgr:GenerateDriftBottleSuccess()
    g_ScriptEvent:BroadcastInLua("GenerateDriftBottleSuccess")
end