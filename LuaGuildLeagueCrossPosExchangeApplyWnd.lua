local DelegateFactory  = import "DelegateFactory"

LuaGuildLeagueCrossPosExchangeApplyWnd = class()


RegistClassMember(LuaGuildLeagueCrossPosExchangeApplyWnd, "m_GuildTemplate")
RegistClassMember(LuaGuildLeagueCrossPosExchangeApplyWnd, "m_ScrollView")
RegistClassMember(LuaGuildLeagueCrossPosExchangeApplyWnd, "m_Table")
RegistClassMember(LuaGuildLeagueCrossPosExchangeApplyWnd, "m_EmptyLabel")

function LuaGuildLeagueCrossPosExchangeApplyWnd:Awake()
    self.m_GuildTemplate = self.transform:Find("Anchor/ScrollView/Item").gameObject
    self.m_ScrollView = self.transform:Find("Anchor/ScrollView"):GetComponent(typeof(UIScrollView))
    self.m_Table = self.transform:Find("Anchor/ScrollView/Table"):GetComponent(typeof(UITable))
    self.m_EmptyLabel = self.transform:Find("Anchor/EmptyLabel").gameObject
    self.m_GuildTemplate:SetActive(false)
end

function LuaGuildLeagueCrossPosExchangeApplyWnd:Init()
    self:LoadData()
end

function LuaGuildLeagueCrossPosExchangeApplyWnd:OnEnable()
    g_ScriptEvent:AddListener("OnQueryGLCGuildPosInfoResult", self, "OnQueryGLCGuildPosInfoResult")
    g_ScriptEvent:AddListener("OnRefuseGLCExchangeGuildPosSuccess", self, "OnRefuseGLCExchangeGuildPosSuccess") 
end

function LuaGuildLeagueCrossPosExchangeApplyWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnQueryGLCGuildPosInfoResult", self, "OnQueryGLCGuildPosInfoResult")
    g_ScriptEvent:RemoveListener("OnRefuseGLCExchangeGuildPosSuccess", self, "OnRefuseGLCExchangeGuildPosSuccess")
end

function LuaGuildLeagueCrossPosExchangeApplyWnd:OnQueryGLCGuildPosInfoResult()
    self:LoadData()
end

function LuaGuildLeagueCrossPosExchangeApplyWnd:OnRefuseGLCExchangeGuildPosSuccess()
    self:LoadData()
end

function LuaGuildLeagueCrossPosExchangeApplyWnd:LoadData()
    local data = LuaGuildLeagueCrossMgr.m_GuildPosExchangeInfo.requestInfoTbl
    self.m_EmptyLabel:SetActive(#data==0)
    local n = self.m_Table.transform.childCount
    for i=0,n-1 do
        self.m_Table.transform:GetChild(i).gameObject:SetActive(false)
    end
    local idx = 0
    for i=1, #data do
        local child = CUICommonDef.AddChild(self.m_Table.gameObject, self.m_GuildTemplate)
        local child = nil
		if idx<n then
			child = self.m_Table.transform:GetChild(idx).gameObject
		else
			child = CUICommonDef.AddChild(self.m_Table.gameObject, self.m_GuildTemplate)
		end
		idx = idx+1
        child:SetActive(true)
        local info = data[i]
        self:InitOneItem(child.transform, info.index, info.guildId, info.guildName, info.rankStr)
    end
    self.m_Table:Reposition()
    self.m_ScrollView:ResetPosition()

end

function LuaGuildLeagueCrossPosExchangeApplyWnd:InitOneItem(transRoot, index, guildId, guildName, rankStr)
    
    local guildNameLabel = transRoot:Find("GuildNameLabel"):GetComponent(typeof(UILabel))
    local indexLabel = transRoot:Find("IndexLabel"):GetComponent(typeof(UILabel))
    local rankLabel = transRoot:Find("RankLabel"):GetComponent(typeof(UILabel))
    local rejectButton = transRoot:Find("RejectButton").gameObject
    local acceptButton = transRoot:Find("AcceptButton").gameObject

    guildNameLabel.text = SafeStringFormat3(LocalString.GetString("%s%s%s申请换位出战"), "[fffe91]", guildName, "[-]")
    indexLabel.text = SafeStringFormat3(LocalString.GetString("出战序号 %d"), index)
    rankLabel.text = rankStr

    UIEventListener.Get(rejectButton).onClick = DelegateFactory.VoidDelegate(function() self:OnRejectButtonClick(rejectButton, index, guildId, guildName) end)
    UIEventListener.Get(acceptButton).onClick = DelegateFactory.VoidDelegate(function() self:OnAcceptButtonClick(acceptButton, index, guildId, guildName) end)
end

function LuaGuildLeagueCrossPosExchangeApplyWnd:OnRejectButtonClick(go, index, guildId, guildName)
    LuaGuildLeagueCrossMgr:RefuseGLCExchangeGuildPos(guildId)
end

function LuaGuildLeagueCrossPosExchangeApplyWnd:OnAcceptButtonClick(go, index, guildId, guildName)
    LuaGuildLeagueCrossMgr:ConfirmGLCExchangeGuildPos(guildId, index)
end


