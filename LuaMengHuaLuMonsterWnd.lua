require("common/common_include")

local UIScrollView=import "UIScrollView"
local UIWidget = import "UIWidget"
local UITable = import "UITable"
local UIRoot = import "UIRoot"
local Screen = import "UnityEngine.Screen"
local NGUIMath = import "NGUIMath"
local Mathf = import "UnityEngine.Mathf"
local Extensions = import "Extensions"
local MengHuaLu_Buff = import "L10.Game.MengHuaLu_Buff"
local CUITexture = import "L10.UI.CUITexture"
local MengHuaLu_Monster = import "L10.Game.MengHuaLu_Monster"

LuaMengHuaLuMonsterWnd=class()

RegistChildComponent(LuaMengHuaLuMonsterWnd, "Table", UITable)
RegistChildComponent(LuaMengHuaLuMonsterWnd, "ScrollView", UIScrollView)
RegistChildComponent(LuaMengHuaLuMonsterWnd, "Background", UIWidget)
RegistChildComponent(LuaMengHuaLuMonsterWnd, "Header", UIWidget)

RegistChildComponent(LuaMengHuaLuMonsterWnd, "BuffItem", GameObject)
RegistChildComponent(LuaMengHuaLuMonsterWnd, "CurrentQiChangItem", GameObject)

RegistChildComponent(LuaMengHuaLuMonsterWnd, "MonsterIcon", CUITexture)
RegistChildComponent(LuaMengHuaLuMonsterWnd, "MonsterNameLabel", UILabel)
RegistChildComponent(LuaMengHuaLuMonsterWnd, "AttackValueLabel", UILabel)
RegistChildComponent(LuaMengHuaLuMonsterWnd, "HPValueLabel", UILabel)
RegistChildComponent(LuaMengHuaLuMonsterWnd, "RoundValueLabel", UILabel)
RegistChildComponent(LuaMengHuaLuMonsterWnd, "RoundProperty", GameObject)


RegistClassMember(LuaMengHuaLuMonsterWnd, "MazeInfo")

function LuaMengHuaLuMonsterWnd:Init()
    
    local mazeInfo = LuaMengHuaLuMgr.m_MazeInfos[LuaMengHuaLuMgr.m_SelectedMonsterGrid]
    if not mazeInfo or not mazeInfo.MonsterInfo then
        CUIManager.CloseUI(CLuaUIResources.MengHuaLuMonsterWnd)
        return
    end

    self.MazeInfo = mazeInfo
    self:UpdateMonsterInfo()

    self.BuffItem:SetActive(false)
    self.CurrentQiChangItem:SetActive(false)

    Extensions.RemoveAllChildren(self.Table.transform)
    self:UpdateBuffs()
    
    self.Table:Reposition()

    local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject)
    local virtualScreenHeight = Screen.height * scale
    local contentTopPadding = Mathf.Abs(self.ScrollView.panel.topAnchor.absolute)
    local contentBottomPadding = Mathf.Abs(self.ScrollView.panel.bottomAnchor.absolute)
    local totalHeight = NGUIMath.CalculateRelativeWidgetBounds(self.Table.transform).size.y + (self.Table.padding.y * 2) + contentTopPadding + contentBottomPadding + (self.ScrollView.panel.clipSoftness.y * 2)
    local displayWndHeight = Mathf.Clamp(totalHeight, contentTopPadding + contentBottomPadding + 80, virtualScreenHeight - 100)
    self.Background:GetComponent(typeof(UIWidget)).height = Mathf.CeilToInt(displayWndHeight)
    self.ScrollView.panel:ResetAndUpdateAnchors()
    self.ScrollView:ResetPosition()
end

function LuaMengHuaLuMonsterWnd:UpdateMonsterInfo()
    local monster = MengHuaLu_Monster.GetData(self.MazeInfo.MonsterInfo.MonsterId)
    self.MonsterIcon:LoadNPCPortrait(monster.Icon, false)
    self.MonsterNameLabel.text = monster.Name
    self.AttackValueLabel.text = self.MazeInfo.MonsterInfo.Attack
    self.HPValueLabel.text = self.MazeInfo.MonsterInfo.Hp
    -- 主动怪显示
    self.RoundProperty:SetActive(monster.ActionRound ~= 0)
    if monster.ActionRound ~= 0 then
        self.RoundValueLabel.text = (self.MazeInfo.MonsterInfo.ActionRound+1)
    end
end

function LuaMengHuaLuMonsterWnd:UpdateBuffs()
    -- BuffData
    for i = 0, self.MazeInfo.MonsterInfo.BuffData.Count-1 do
        local buffInfo = self.MazeInfo.MonsterInfo.BuffData[i]
        local buffId = buffInfo[0]
        local lastRound = buffInfo[1]
        local go = CUICommonDef.AddChild(self.Table.gameObject, self.BuffItem)
        self:InitBuffItem(go, {
            buffId = buffId, 
            lastRound = lastRound})
        go:SetActive(true)
    end
end

function LuaMengHuaLuMonsterWnd:InitBuffItem(go, info)
    local Icon = go.transform:Find("BuffIcon/Icon"):GetComponent(typeof(CUITexture))
    Icon:Clear()
    local LastRoundLabel = go.transform:Find("BuffIcon/LastRoundLabel"):GetComponent(typeof(UILabel))
    local NameLabel = go.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local DescLabel = go.transform:Find("DescLabel"):GetComponent(typeof(UILabel))

    local buff = MengHuaLu_Buff.GetData(info.buffId)
    if buff then
        Icon:LoadMaterial(buff.Icon)
        LastRoundLabel.gameObject:SetActive(info.lastRound > 0)
        if info.lastRound >= 0 then
            LastRoundLabel.text = tostring(info.lastRound)
        else
            LastRoundLabel.text = nil
        end

        NameLabel.text = buff.Name
        DescLabel.text = buff.Description
    end
end

function LuaMengHuaLuMonsterWnd:Update()
    self:ClickThroughToClose()
end

function LuaMengHuaLuMonsterWnd:ClickThroughToClose()
    if Input.GetMouseButtonDown(0) then       
        if (not CUICommonDef.IsOverUI or (UICamera.lastHit.collider and
           not CUICommonDef.IsChild(self.transform, UICamera.lastHit.collider.transform) and
            CUICommonDef.GetNearestPanelDepth(self.transform) > CUICommonDef.GetNearestPanelDepth(UICamera.lastHit.collider.transform))) then
            CUIManager.CloseUI(CLuaUIResources.MengHuaLuBuffListWnd)
        end
    end
end


function LuaMengHuaLuMonsterWnd:MengHuaLuUpdateMonsterBuffs(grid, buffList)
    if self.MazeInfo.MazeID == grid then
        self:Init()
    end
end

function LuaMengHuaLuMonsterWnd:OnEnable()
    g_ScriptEvent:AddListener("MengHuaLuUpdateMonsterBuffs", self, "MengHuaLuUpdateMonsterBuffs")
end

function LuaMengHuaLuMonsterWnd:OnDisable()
    g_ScriptEvent:RemoveListener("MengHuaLuUpdateMonsterBuffs", self, "MengHuaLuUpdateMonsterBuffs")
end

return LuaMengHuaLuMonsterWnd