local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local QnTabView=import "L10.UI.QnTabView"
local LocalString = import "LocalString"
local QnTabButton = import "L10.UI.QnTabButton"


CLuaGrabChristmasGiftWnd = class()

RegistClassMember(CLuaGrabChristmasGiftWnd,"ChooseView")
RegistClassMember(CLuaGrabChristmasGiftWnd,"MatchView")
RegistClassMember(CLuaGrabChristmasGiftWnd,"CountLabel")
RegistClassMember(CLuaGrabChristmasGiftWnd,"MatchBtn")
RegistClassMember(CLuaGrabChristmasGiftWnd,"TabView")
RegistClassMember(CLuaGrabChristmasGiftWnd,"HatName")
RegistClassMember(CLuaGrabChristmasGiftWnd,"SkillIcon")
RegistClassMember(CLuaGrabChristmasGiftWnd,"Hat")
RegistClassMember(CLuaGrabChristmasGiftWnd,"SkillDesc")
RegistClassMember(CLuaGrabChristmasGiftWnd,"CancelRoot")
RegistClassMember(CLuaGrabChristmasGiftWnd,"CancelButton")
RegistClassMember(CLuaGrabChristmasGiftWnd,"CloseButton")
RegistClassMember(CLuaGrabChristmasGiftWnd,"BlueIcon")
RegistClassMember(CLuaGrabChristmasGiftWnd,"BlueDesc")
RegistClassMember(CLuaGrabChristmasGiftWnd,"RedIcon")
RegistClassMember(CLuaGrabChristmasGiftWnd,"RedDesc")
RegistClassMember(CLuaGrabChristmasGiftWnd,"YellowIcon")
RegistClassMember(CLuaGrabChristmasGiftWnd,"YellowDesc")
RegistClassMember(CLuaGrabChristmasGiftWnd,"NameStr")

function CLuaGrabChristmasGiftWnd:Awake()
    self.ChooseView = self.transform:Find("Anchor/ChooseView").gameObject
    self.MatchView = self.transform:Find("Anchor/MatchView").gameObject
    self.CountLabel = self.transform:Find("Anchor/CountLabel"):GetComponent(typeof(UILabel))
    self.MatchBtn = self.transform:Find("Anchor/ChooseView/MatchBtn").gameObject
    self.TabView = self.transform:Find("Anchor/ChooseView/TabView"):GetComponent(typeof(QnTabView))
    self.HatName = self.transform:Find("Anchor/MatchView/Choosed/HatName"):GetComponent(typeof(UILabel))
    self.SkillIcon = self.transform:Find("Anchor/MatchView/Choosed/Skill/Mask/SkillIcon"):GetComponent(typeof(CUITexture))
    self.Hat = self.transform:Find("Anchor/MatchView/Choosed/Hat"):GetComponent(typeof(CUITexture))
    self.SkillDesc = self.transform:Find("Anchor/MatchView/Choosed/SkillDesc"):GetComponent(typeof(UILabel))
    self.CancelRoot = self.transform:Find("Anchor/MatchView/CancelRoot").gameObject
    self.CancelButton = self.transform:Find("Anchor/MatchView/CancelRoot/CancelButton").gameObject
    self.CloseButton = self.transform:Find("Wnd_Bg_Primary_Normal/CloseButton").gameObject
    self.TitleLabel = self.ChooseView.transform:Find("TittleLabel"):GetComponent(typeof(UILabel))

    self.BlueIcon = self.transform:Find("Anchor/ChooseView/TabView/BlueBtn/Skill/Mask/Icon"):GetComponent(typeof(CUITexture)).material
    self.BlueDesc = self.transform:Find("Anchor/ChooseView/TabView/BlueBtn/DescLabel"):GetComponent(typeof(UILabel)).text
    self.BlueNameLabel = self.transform:Find("Anchor/ChooseView/TabView/BlueBtn/NameLabel"):GetComponent(typeof(UILabel))
    self.RedIcon = self.transform:Find("Anchor/ChooseView/TabView/RedBtn/Skill/Mask/Icon"):GetComponent(typeof(CUITexture)).material
    self.RedDesc = self.transform:Find("Anchor/ChooseView/TabView/RedBtn/DescLabel"):GetComponent(typeof(UILabel)).text
    self.RedNameLabel = self.transform:Find("Anchor/ChooseView/TabView/RedBtn/NameLabel"):GetComponent(typeof(UILabel))
    self.YellowIcon = self.transform:Find("Anchor/ChooseView/TabView/YellowBtn/Skill/Mask/Icon"):GetComponent(typeof(CUITexture)).material
    self.YellowDesc = self.transform:Find("Anchor/ChooseView/TabView/YellowBtn/DescLabel"):GetComponent(typeof(UILabel)).text
    self.YellowNameLabel = self.transform:Find("Anchor/ChooseView/TabView/YellowBtn/NameLabel"):GetComponent(typeof(UILabel))
end

function CLuaGrabChristmasGiftWnd:Init()
    self:InitSelectView()
    UIEventListener.Get(self.MatchBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        self:OnStarMatch()
    end)

    self.TabView.OnSelect = DelegateFactory.Action_QnTabButton_int(function (go, index)
        LuaGrabChristmasGiftMgr.CurSelectedIndex = index
        self:InitMatchView(index)
    end)

    UIEventListener.Get(self.CancelButton).onClick = DelegateFactory.VoidDelegate(function (p)
        self:OnCancelMatch()
    end)

    UIEventListener.Get(self.CloseButton).onClick = DelegateFactory.VoidDelegate(function (p)
        LuaGrabChristmasGiftMgr.CurSelectedIndex = -1
        CUIManager.CloseUI("GrabChristmasGiftWnd")
    end)
    --default
    self.TabView:ChangeTo(LuaGrabChristmasGiftMgr.CurSelectedIndex)
    if LuaGrabChristmasGiftMgr.CurSelectedIndex == -1 then
        self.TabView:GetTabGameObject(self.TabView.CurrentSelectTab).transform:GetComponent(typeof(QnTabButton)).Selected = false
    end

    if LuaGrabChristmasGiftMgr.RefreshSignWndNotShow == true then
        self:OnRefreshChristmasGiftBattleSignUpWnd(LuaGrabChristmasGiftMgr.IsSignUp,LuaGrabChristmasGiftMgr.RestAwardNum,LuaGrabChristmasGiftMgr.Choice)
        LuaGrabChristmasGiftMgr.RefreshSignWndNotShow = false
    end
end

function CLuaGrabChristmasGiftWnd:InitSelectView()
    self.NameStr = LocalString.GetString("圣诞")
    if CLuaRescueChristmasTreeMgr.curState == 2 then
        self.NameStr = LocalString.GetString("圣蛋")
    end
    self.BlueNameLabel.text = SafeStringFormat3(LocalString.GetString("蓝帽子%s老人"), self.NameStr)
    self.RedNameLabel.text = SafeStringFormat3(LocalString.GetString("红帽子%s老人"), self.NameStr)
    self.YellowNameLabel.text = SafeStringFormat3(LocalString.GetString("黄帽子%s老人"), self.NameStr)
    self.TitleLabel.text = SafeStringFormat3(LocalString.GetString("请选择你要装扮的%s老人"), self.NameStr)
end

function CLuaGrabChristmasGiftWnd:InitMatchView(index)
    if index == 0 then
        self.HatName.text = SafeStringFormat3(LocalString.GetString("蓝帽子%s老人"), self.NameStr)
        self.SkillIcon.material = self.BlueIcon
        self.SkillDesc.text = LocalString.GetString(self.BlueDesc)
        self.Hat:LoadMaterial("UI/Texture/Transparent/Material/grabchristmasgiftwnd_icon_03.mat")
    elseif index == 1 then
        self.HatName.text = SafeStringFormat3(LocalString.GetString("红帽子%s老人"), self.NameStr)
        self.SkillIcon.material = self.RedIcon
        self.SkillDesc.text = LocalString.GetString(self.RedDesc)
        self.Hat:LoadMaterial("UI/Texture/Transparent/Material/grabchristmasgiftwnd_icon_01.mat")
    elseif index == 2 then
        self.HatName.text = SafeStringFormat3(LocalString.GetString("黄帽子%s老人"), self.NameStr)
        self.SkillIcon.material = self.YellowIcon
        self.SkillDesc.text = LocalString.GetString(self.YellowDesc)
        self.Hat:LoadMaterial("UI/Texture/Transparent/Material/grabchristmasgiftwnd_icon_02.mat")
    end
end

function CLuaGrabChristmasGiftWnd:OnCancelMatch()
    --to do: tell sever
    self.MatchView:SetActive(false)
    self.ChooseView:SetActive(true)
end

function CLuaGrabChristmasGiftWnd:OnStarMatch()
    local player = CClientMainPlayer.Inst
    if player then
        local level = player.Level
        if player.HasFeiSheng then
            level = player.XianShenLevel
        end
        if level < 50 then
            g_MessageMgr:ShowMessage("BAGUALU_GRADELIMIT",50)
            return
        end
    end
    
    if LuaGrabChristmasGiftMgr.CurSelectedIndex == -1 then
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", SafeStringFormat3(LocalString.GetString("请选择你要装扮的%s老人"), self.NameStr))
        return
    end
    self.MatchView:SetActive(true)
    self.ChooseView:SetActive(false)
    Gac2Gas.RequestSignUpChristmasGiftBattle(LuaGrabChristmasGiftMgr.CurSelectedIndex + 1)
end

function CLuaGrabChristmasGiftWnd:OnRefreshChristmasGiftBattleSignUpWnd(bSignUp, restAwardNum, choice)
    if bSignUp then--匹配中
        self.MatchView:SetActive(true)
        self.ChooseView:SetActive(false)
        LuaGrabChristmasGiftMgr.CurSelectedIndex = choice-1
        self:InitMatchView(choice-1)
    else--选择
        self.MatchView:SetActive(false)
        self.ChooseView:SetActive(true)
    end
    self.CountLabel.text = restAwardNum
end

function CLuaGrabChristmasGiftWnd:OnEnable()
    g_ScriptEvent:AddListener("RefreshChristmasGiftBattleSignUpWnd",self,"OnRefreshChristmasGiftBattleSignUpWnd")
end

function CLuaGrabChristmasGiftWnd:OnDisable()
    g_ScriptEvent:RemoveListener("RefreshChristmasGiftBattleSignUpWnd",self,"OnRefreshChristmasGiftBattleSignUpWnd")
end
