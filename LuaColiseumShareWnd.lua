local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local EShareType = import "L10.UI.EShareType"
local CLoginMgr = import "L10.Game.CLoginMgr"

LuaColiseumShareWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaColiseumShareWnd, "TopLeftLabel", "TopLeftLabel", UILabel)
RegistChildComponent(LuaColiseumShareWnd, "WinResultTexture", "WinResultTexture", CUITexture)
RegistChildComponent(LuaColiseumShareWnd, "DanLabel", "DanLabel", UILabel)
RegistChildComponent(LuaColiseumShareWnd, "WinningStreakLabel", "WinningStreakLabel", UILabel)
RegistChildComponent(LuaColiseumShareWnd, "ScoreLabel", "ScoreLabel", UILabel)
RegistChildComponent(LuaColiseumShareWnd, "RankLabel", "RankLabel", UILabel)
RegistChildComponent(LuaColiseumShareWnd, "KillNumLabel", "KillNumLabel", UILabel)
RegistChildComponent(LuaColiseumShareWnd, "DieNumLabel", "DieNumLabel", UILabel)
RegistChildComponent(LuaColiseumShareWnd, "ControllNumLabel", "ControllNumLabel", UILabel)
RegistChildComponent(LuaColiseumShareWnd, "DecontrollNumLabel", "DecontrollNumLabel", UILabel)
RegistChildComponent(LuaColiseumShareWnd, "ResurgenceTimesLabel", "ResurgenceTimesLabel", UILabel)
RegistChildComponent(LuaColiseumShareWnd, "DamageLabel", "DamageLabel", UILabel)
RegistChildComponent(LuaColiseumShareWnd, "CurativeDoseLabel", "CurativeDoseLabel", UILabel)
RegistChildComponent(LuaColiseumShareWnd, "Portait", "Portait", CUITexture)
RegistChildComponent(LuaColiseumShareWnd, "AccountLabel", "AccountLabel", UILabel)
RegistChildComponent(LuaColiseumShareWnd, "ServerNameLabel", "ServerNameLabel", UILabel)
RegistChildComponent(LuaColiseumShareWnd, "IdLabel", "IdLabel", UILabel)
RegistChildComponent(LuaColiseumShareWnd, "LevelLabel", "LevelLabel", UILabel)
RegistChildComponent(LuaColiseumShareWnd, "Bg", "Bg", CUITexture)
--@endregion RegistChildComponent end

function LuaColiseumShareWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaColiseumShareWnd:Init()
	self:InitViewWithData()
    self:Share()
end

function LuaColiseumShareWnd:InitViewWithData()
    local titleTextArray = {LocalString.GetString("一\n对\n一"),LocalString.GetString("二\n对\n二"),LocalString.GetString("三\n对\n三"),LocalString.GetString("四\n对\n四"),LocalString.GetString("五\n对\n五")}
    self.TopLeftLabel.text = titleTextArray[LuaColiseumMgr.m_LastGameType]
    --段位
    local dan = LuaColiseumMgr:GetDan(LuaColiseumMgr.m_ScoreAfterBattle)
	local danInfoData = Arena_DanInfo.GetData(dan)
    --段位名称文字和颜色
    local danLabelText, danLabelColor = danInfoData.DanName,danInfoData.DanColor
    --段位->小段位文字 映射
	local smallDanTexts = {LocalString.GetString("一段"),LocalString.GetString("二段"),LocalString.GetString("三段"),LocalString.GetString("四段"),LocalString.GetString("五段")}
     --进度条数值, 小段位点数
	local processValue, showDanSpritesNum = LuaColiseumMgr:GetProcessValueAndDanSpritesNum(LuaColiseumMgr.m_ScoreAfterBattle)
    local smallDan = smallDanTexts[showDanSpritesNum] and showDanSpritesNum or (dan ~= 5 and 1 or 5)
    self.DanLabel.color = NGUIText.ParseColor24(danLabelColor, 0)
    self.DanLabel.text = CUICommonDef.TranslateToNGUIText(SafeStringFormat3(LocalString.GetString("%s·%s"),danLabelText,smallDanTexts[smallDan]))
    local data = LuaColiseumMgr.m_ArenaPlayMyResultData
    local tagSprites = {
		[0] = "UI/Texture/Transparent/Material/liuyipengpengche_sheng.mat",
		[1] = "UI/Texture/Transparent/Material/liuyipengpengche_bai.mat",
		[2] = "UI/Texture/Transparent/Material/liuyipengpengche_ping.mat"
	}
    self.WinResultTexture:LoadMaterial(tagSprites[data.playerWinType])
    self.WinningStreakLabel.text = SafeStringFormat3(LocalString.GetString("%d连胜"),data.winningStreakNum)
    self.WinningStreakLabel.enabled = data.winningStreakNum and data.winningStreakNum > 1
    self.ScoreLabel.text = LuaColiseumMgr.m_ScoreAfterBattle
    self.RankLabel.enabled = false
    self.KillNumLabel.text = data.killNum
    self.DieNumLabel.text = data.dieTimes
    self.ControllNumLabel.text = data.controllNum
    self.DecontrollNumLabel.text = data.decontrollNum
    self.ResurgenceTimesLabel.text = data.resurgenceTimes
    self.DamageLabel.text = data.damage and math.floor(tonumber(data.damage))
    self.CurativeDoseLabel.text = data.curativeDose
    self.LevelLabel.text = CClientMainPlayer.Inst.Level
    local hasFeiSheng = CClientMainPlayer.Inst and CClientMainPlayer.Inst.HasFeiSheng or false
    self.LevelLabel.color = hasFeiSheng and NGUIText.ParseColor24("fe7900", 0) or Color.white
    local server = CLoginMgr.Inst:GetSelectedGameServer()
    if server then
        self.ServerNameLabel.text = server.name
    end
    if CClientMainPlayer.Inst then
        self.IdLabel.text = CClientMainPlayer.Inst.Id
        self.AccountLabel.text = CClientMainPlayer.Inst.Name
        self.Portait:LoadNPCPortrait(CUICommonDef.GetPortraitName(CClientMainPlayer.Inst.Class, CClientMainPlayer.Inst.Gender, -1), false)
    end
    self.Bg.gameObject:SetActive(LuaColiseumMgr.m_LastGameType == 1)
    self.transform:Find("Anchor/Bg (1)").gameObject:SetActive(LuaColiseumMgr.m_LastGameType ~= 1)
end

function LuaColiseumShareWnd:Share()
    CUICommonDef.CaptureScreen(
        "screenshot",
        true,
        false,
        nil,
        DelegateFactory.Action_string_bytes(
            function(fullPath, jpgBytes)
                ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, fullPath)
                CUIManager.CloseUI("ColiseumShareWnd")
            end
        ),
        false
    )
end
--@region UIEvent

--@endregion UIEvent

