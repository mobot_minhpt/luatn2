local UILabel = import "UILabel"

local GameObject = import "UnityEngine.GameObject"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"

local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"

LuaChristmas2023BingXuePlayResultWnd = class()
RegistClassMember(LuaChristmas2023BingXuePlayResultWnd, "m_BingxueItemId")  -- 礼盒ID
RegistClassMember(LuaChristmas2023BingXuePlayResultWnd, "m_BingdiaoItemId") -- 冰雪家具兑换ID

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaChristmas2023BingXuePlayResultWnd, "RewardInfo", "RewardInfo", GameObject)
RegistChildComponent(LuaChristmas2023BingXuePlayResultWnd, "NoReward", "NoReward", UILabel)
RegistChildComponent(LuaChristmas2023BingXuePlayResultWnd, "TipsLabel", "TipsLabel", UILabel)
RegistChildComponent(LuaChristmas2023BingXuePlayResultWnd, "GotoBtn", "GotoBtn", GameObject)

--@endregion RegistChildComponent end

function LuaChristmas2023BingXuePlayResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end
-- 初始化奖励信息
function LuaChristmas2023BingXuePlayResultWnd:InitReward(isaward)
    if not isaward then
        self.RewardInfo:SetActive(false)
        self.NoReward.gameObject:SetActive(true)
        return
    end
    self.RewardInfo:SetActive(true)
    self.NoReward.gameObject:SetActive(false)

    self.m_BingxueItemId = Christmas2023_BingXuePlay.GetData().BingxueItemId
    local item = Item_Item.GetData(self.m_BingxueItemId)
    local icon = self.RewardInfo.transform:GetChild(0):Find("Mask/IconTexture"):GetComponent(typeof(CUITexture))
    local label = self.RewardInfo.transform:GetChild(0):Find("Label"):GetComponent(typeof(UILabel))
    icon:LoadMaterial(item.Icon)
    label.text = item.Name
    UIEventListener.Get(icon.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        CItemInfoMgr.ShowLinkItemTemplateInfo(self.m_BingxueItemId, false, nil, AlignType.Default, 0, 0, 0, 0)
    end)

    self.m_BingdiaoItemId = Christmas2023_BingXuePlay.GetData().BingdiaoItemId
    item = Item_Item.GetData(self.m_BingdiaoItemId)
    icon = self.RewardInfo.transform:GetChild(1):Find("Mask/IconTexture"):GetComponent(typeof(CUITexture))
    label = self.RewardInfo.transform:GetChild(1):Find("Label"):GetComponent(typeof(UILabel))
    icon:LoadMaterial(item.Icon)
    label.text = item.Name
    UIEventListener.Get(icon.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        CItemInfoMgr.ShowLinkItemTemplateInfo(self.m_BingdiaoItemId, false, nil, AlignType.Default, 0, 0, 0, 0)
    end)
end
function LuaChristmas2023BingXuePlayResultWnd:Init()
    self:InitReward(LuaChristmas2023Mgr.m_IsAward)

    self.TipsLabel.text = g_MessageMgr:FormatMessage("CHRISTMAS2023_BINGXUEPLAY_Jifen", LuaChristmas2023Mgr.m_LastScore)
    UIEventListener.Get(self.GotoBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        CLuaNPCShopInfoMgr.ShowScoreShop(Christmas2023_Setting.GetData().ScoreShopKey) -- 打开积分商店
    end)
end

--@region UIEvent

--@endregion UIEvent

