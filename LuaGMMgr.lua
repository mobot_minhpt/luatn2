local Object = import "System.Object"
local CWebBrowserMgr = import "L10.Game.CWebBrowserMgr"
local CJingLingMgr = import "L10.Game.CJingLingMgr"
local CScene = import "L10.Game.CScene"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CLoginMgr = import "L10.Game.CLoginMgr"

LuaGMMgr = {}

LuaGMMgr.m_BubblePopupInfo = {}

EnumGMBubblePopupActionType = {
    eShowWnd = 1, --打开窗口
    eClick = 2, --点击弹窗
    eClose = 3, --手动关闭弹窗（不包括倒计时结束等情况）
}

function LuaGMMgr:ShowBubblePopupWnd(type, title, content_text, content_image, urlOrText, choice, duration, limitUd)
    if not self:CheckBubblePopupLimit(limitUd) then return end
    
    self.m_BubblePopupInfo.type = type
    self.m_BubblePopupInfo.title = title
    self.m_BubblePopupInfo.content_text = content_text
    self.m_BubblePopupInfo.content_image = content_image and string.find(content_image, "http", 1, true)~=nil and content_image or nil
    self.m_BubblePopupInfo.urlOrText = urlOrText
    self.m_BubblePopupInfo.choice = choice
    self.m_BubblePopupInfo.duration = duration and duration>0 and duration or 20 --duration小于等于0时使用运营文档规定的默认值
    CUIManager.ShowUI("GMBubblePopupWnd")
    self:RecordBubblePopupLog(type, urlOrText, EnumGMBubblePopupActionType.eShowWnd)
end

function LuaGMMgr:CheckBubblePopupLimit(limitUd)
    local limit = g_MessagePack.unpack(limitUd)
	local level_limit = limit.level_limit
    local vip_limit = limit.vip_limit
    local create_time_limit = limit.create_time_limit
    local skip_channels = limit.skip_channels
    local is_pc = limit.is_pc
    local is_adult = limit.is_adult
    local force_ignore_play = limit.force_ignore_play

    local mainplayer = CClientMainPlayer.Inst
    if mainplayer == nil then return false end
    -- check level
    if level_limit and #level_limit==2 and (mainplayer.MaxLevel<level_limit[1] or mainplayer.MaxLevel>level_limit[2]) then
        return false
    end
    -- check vip
    if vip_limit and #vip_limit==2 and (mainplayer.ItemProp.Vip.Level<vip_limit[1] or mainplayer.ItemProp.Vip.Level>vip_limit[2]) then
        return false
    end
    -- check create time
    if create_time_limit and #create_time_limit==2 and (mainplayer.BasicProp.CreateTime<create_time_limit[1] or mainplayer.BasicProp.CreateTime>create_time_limit[2]) then
        return false
    end
    -- check channels
    if skip_channels and #skip_channels>0 then
        local channel = CLoginMgr.Inst:GetLoginChannelConverted()
        for ch in string.gmatch(skip_channels, "([^,]+)") do
            if ch == channel then
                return false
            end
        end
    end
    --check is pc
    if is_pc then
        if is_pc==1 and not CommonDefs.Is_PC_PLATFORM() then
            return false
        elseif is_pc==2 and CommonDefs.Is_PC_PLATFORM() then
            return false
        end
    end
    --check is adult
    if is_adult then
        mainplayerIsAdult = (mainplayer.BasicProp.ClientData.Extra.AgeRangeType == 4) -- 对应服务器 EnumAgeRangeType.eAdult
        if is_adult==1 and not mainplayerIsAdult then
            return false
        elseif is_adult==2 and mainplayerIsAdult then
            return false
        end
    end
    --check ignore gameplay
    if force_ignore_play==nil or force_ignore_play~=1 then
        local playId = CScene.MainScene and CScene.MainScene.GamePlayDesignId or 0
        local data = playId>0 and Gameplay_Gameplay.GetData(playId) or nil
        if data and data.EnableGMBubblePopupWnd~=1 then
            return false--大部分副本禁止显示气泡弹窗，需要显示的通过配表控制
        end
    end

    return true
end

function LuaGMMgr:GetIconByBubblePopupType()
    local type = self.m_BubblePopupInfo.type
    if type==1 then
        return "UI/Texture/Portrait/Material/hnpc_c.mat" --CC
    elseif type==2 then
        return "UI/Texture/Portrait/Material/task_092.mat" --精灵
    else
        return "UI/Texture/Portrait/Material/im_pmsg01.mat" --其他
    end
end

function LuaGMMgr:GetTipByBubblePopupType()
    local type = self.m_BubblePopupInfo.type
    if type==1 then
        return LocalString.GetString("点击弹窗 打开直播") --CC
    elseif type==2 then
        return LocalString.GetString("点击弹窗 打开精灵") --精灵
    else
        return LocalString.GetString("点击弹窗 打开链接") --其他
    end
end

function LuaGMMgr:DoActionByBubblePopupType()
    local type = self.m_BubblePopupInfo.type
    if type==1 then
        if self.m_BubblePopupInfo.choice == 0 then
            CWebBrowserMgr.Inst:OpenUrl(self.m_BubblePopupInfo.urlOrText)
        else
            CWebBrowserMgr.Inst:OpenExtenalUrl(self.m_BubblePopupInfo.urlOrText)
        end
    elseif type==2 then
        CJingLingMgr.Inst:ShowJingLingWnd(self.m_BubblePopupInfo.urlOrText, "o_push", true, false, nil, false)
    else
        --精灵解析回答时会触发打开网页
        CJingLingMgr.Inst:ShowJingLingWnd(self.m_BubblePopupInfo.urlOrText, "o_push", true, false, nil, false)
    end
    self:RecordBubblePopupLog(type, self.m_BubblePopupInfo.urlOrText, EnumGMBubblePopupActionType.eClick)
end

function LuaGMMgr:RecordBubblePopupLog(type, urlOrText, actionType)
    local dict = CreateFromClass(MakeGenericClass(Dictionary, String, Object))
    CommonDefs.DictAdd_LuaCall(dict, "type", type)
    CommonDefs.DictAdd_LuaCall(dict, "url",  urlOrText)
    CommonDefs.DictAdd_LuaCall(dict, "actionType",  actionType)
    Gac2Gas.RequestRecordClientLog("GMBubblePopup", MsgPackImpl.pack(dict))
end