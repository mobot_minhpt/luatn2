local CTalismanWndMgr = import "L10.UI.CTalismanWndMgr"
local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"
local CTalismanEquipWindow=import "L10.UI.CTalismanEquipWindow"
local CTalismanFuzhuWindow=import "L10.UI.CTalismanFuzhuWindow"
local QnTabView=import "L10.UI.QnTabView"
local CCommonLuaScript=import "L10.UI.CCommonLuaScript"

CLuaTalismanWnd = class()
RegistClassMember(CLuaTalismanWnd,"closeButton")
RegistClassMember(CLuaTalismanWnd,"tabView")
RegistClassMember(CLuaTalismanWnd,"equipWindow")
RegistClassMember(CLuaTalismanWnd,"baptizeWindow")
RegistClassMember(CLuaTalismanWnd,"upgradeWindow")
RegistClassMember(CLuaTalismanWnd,"fuzhuWindow")
RegistClassMember(CLuaTalismanWnd,"initEquipWnd")

function CLuaTalismanWnd:Awake()
    self.closeButton = self.transform:Find("Wnd_Bg_Primary_Tab/CloseButton").gameObject
    self.tabView = self.transform:Find("QnTabView"):GetComponent(typeof(QnTabView))
    self.equipWindow = self.transform:Find("QnTabView/EquipWindow"):GetComponent(typeof(CTalismanEquipWindow))
    local script = self.transform:Find("QnTabView/UpgradeWindow"):GetComponent(typeof(CCommonLuaScript))
    script:Awake()
    self.upgradeWindow = script.m_LuaSelf
    
    self.fuzhuWindow = self.transform:Find("QnTabView/FuzhuWindow"):GetComponent(typeof(CTalismanFuzhuWindow))
    self.initEquipWnd = false
    local script = self.transform:Find("QnTabView/BaptizeWindow"):GetComponent(typeof(CCommonLuaScript))
    script:Awake()
    self.baptizeWindow =script.m_LuaSelf
end

function CLuaTalismanWnd:Init()
    self.tabView.OnSelect = DelegateFactory.Action_QnTabButton_int(function(btn,index)
        self:OnTabViewChanged(btn,index)
    end)
    self.tabView:ChangeTo(CTalismanWndMgr.tabViewIndex)
end

function CLuaTalismanWnd:OnTabViewChanged( button, index) 

    -- 特殊处理一下全民PK
    if CQuanMinPKMgr.Inst.m_IsQuanMinPKServer and index ~= 0 then
        g_MessageMgr:ShowMessage("NOT_ALLOW_IN_QUANMINPK")
        self.tabView:ChangeTo(0)
        return
    end

    repeat
        local default = index
        if default == (0) then
            self.equipWindow:Init()
            break
        elseif default == (1) then
            self.baptizeWindow:Init()
            break
        elseif default == (2) then
            self.upgradeWindow:Init()
            break
        elseif default == (3) then
            self.fuzhuWindow:Init()
            break
        else
            break
        end
    until 1
end
