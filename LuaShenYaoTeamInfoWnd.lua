local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CTeamMgr = import "L10.Game.CTeamMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local CPlayerInfoMgr = import "CPlayerInfoMgr"

LuaShenYaoTeamInfoWnd = class()
LuaShenYaoTeamInfoWnd.s_Info = nil

RegistClassMember(LuaShenYaoTeamInfoWnd, "m_Grid")
RegistClassMember(LuaShenYaoTeamInfoWnd, "m_Template")
RegistClassMember(LuaShenYaoTeamInfoWnd, "m_MaxLevel")

function LuaShenYaoTeamInfoWnd:Awake()
    self.m_MaxLevel = 0
    XinBaiLianDong_ShenYaoNpc.Foreach(function(k, v)
        self.m_MaxLevel = math.max(self.m_MaxLevel, k)
    end)

    self.m_Grid = self.transform:Find("Anchor/InfoTable/Table"):GetComponent(typeof(UIGrid))
    self.m_Template = self.transform:Find("Anchor/InfoTable/Pool/Item").gameObject
    self.m_Template:SetActive(false)
end

function LuaShenYaoTeamInfoWnd:Init()
    if not CClientMainPlayer.Inst then return end
    Extensions.RemoveAllChildren(self.m_Grid.transform)
    if LuaShenYaoTeamInfoWnd.s_Info then
        local exist = {}
        for _, p in ipairs(LuaShenYaoTeamInfoWnd.s_Info) do
            exist[p.Id] = p
        end
        
        local p = exist[CClientMainPlayer.Inst.Id]
        local trans = NGUITools.AddChild(self.m_Grid.gameObject, self.m_Template).transform
        trans.gameObject:SetActive(true)
        trans:Find("Player/LeaderSprite").gameObject:SetActive(p.IsLeader)
        local portrait = trans:Find("Player/Portrait")
        portrait:GetComponent(typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(CClientMainPlayer.Inst.BasicProp.Class, CClientMainPlayer.Inst.BasicProp.Gender, -1), false)
        local nameLabel = trans:Find("Player/Name"):GetComponent(typeof(UILabel))
        nameLabel.color = Color.green
        nameLabel.text = p.Name
        local str1 = tostring(p.RewardTimes)..LocalString.GetString("次")
        if p.RewardTimes == 0 then str1 = "[ff0000]"..str1 end
        local str2 = tostring(p.JoinTimes)..LocalString.GetString("次")
        if p.JoinTimes == 0 then str2 = "[ff0000]"..str2 end
        trans:Find("Count"):GetComponent(typeof(UILabel)).text = str1.."[-] / "..str2
        if #p.NotPass == 0 then 
            trans:Find("NotPass"):GetComponent(typeof(UILabel)).text = LocalString.GetString("暂无")
        else
            local notPass = ""
            for i = 1, math.min(3, #p.NotPass) do
                if i > 1 then notPass = notPass.."," end
                notPass = notPass..p.NotPass[i]
            end
            trans:Find("NotPass"):GetComponent(typeof(UILabel)).text = notPass..LocalString.GetString("阶")
        end
        trans:Find("UnlockLv"):GetComponent(typeof(UILabel)).text = math.min(self.m_MaxLevel, p.LeaderLv)..LocalString.GetString("阶")

        CommonDefs.DictIterate(CTeamMgr.Inst.m_MemberMap, DelegateFactory.Action_object_object(function(key,val)
            if exist[key] and CClientMainPlayer.Inst.Id ~= key then 
                local p = exist[key]
                local trans = NGUITools.AddChild(self.m_Grid.gameObject, self.m_Template).transform
                trans.gameObject:SetActive(true)
                trans:Find("Player/LeaderSprite").gameObject:SetActive(p.IsLeader)
                local portrait = trans:Find("Player/Portrait")
                portrait:GetComponent(typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(val.BasicProp.Class, val.BasicProp.Gender, -1), false)
                UIEventListener.Get(portrait.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
                    CPlayerInfoMgr.ShowPlayerPopupMenu(p.Id, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), CPlayerInfoMgr.AlignType.Default)
                end)
                trans:Find("Player/Name"):GetComponent(typeof(UILabel)).text = p.Name
                local str1 = tostring(p.RewardTimes)..LocalString.GetString("次")
                if p.RewardTimes == 0 then str1 = "[ff0000]"..str1 end
                local str2 = tostring(p.JoinTimes)..LocalString.GetString("次")
                if p.JoinTimes == 0 then str2 = "[ff0000]"..str2 end
                trans:Find("Count"):GetComponent(typeof(UILabel)).text = str1.."[-] / "..str2
                if #p.NotPass == 0 then
                    trans:Find("NotPass"):GetComponent(typeof(UILabel)).text = LocalString.GetString("暂无")
                else
                    local notPass = ""
                    for i = 1, math.min(3, #p.NotPass) do
                        if i > 1 then notPass = notPass.."," end
                        notPass = notPass..p.NotPass[i]
                    end
                    trans:Find("NotPass"):GetComponent(typeof(UILabel)).text = notPass..LocalString.GetString("阶")
                end
                trans:Find("UnlockLv"):GetComponent(typeof(UILabel)).text = math.min(self.m_MaxLevel, p.LeaderLv)..LocalString.GetString("阶")
            end
        end))

        CommonDefs.DictIterate(CTeamMgr.Inst.m_MemberMap, DelegateFactory.Action_object_object(function(key,val)
            if not exist[key] then 
                local trans = NGUITools.AddChild(self.m_Grid.gameObject, self.m_Template).transform
                trans.gameObject:SetActive(true)
                trans:Find("Player/LeaderSprite").gameObject:SetActive(CTeamMgr.Inst.LeaderId == key)
                local portrait = trans:Find("Player/Portrait")
                portrait:GetComponent(typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(val.BasicProp.Class, val.BasicProp.Gender, -1), false)
                UIEventListener.Get(portrait.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
                    CPlayerInfoMgr.ShowPlayerPopupMenu(key, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), CPlayerInfoMgr.AlignType.Default)
                end)
                trans:Find("Player/Name"):GetComponent(typeof(UILabel)).text = val.BasicProp.Name
                trans:Find("Count").gameObject:SetActive(false)
                trans:Find("NotPass").gameObject:SetActive(false)
                trans:Find("UnlockLv").gameObject:SetActive(false)
                trans:Find("NotInfo").gameObject:SetActive(true)
            end
        end))
    end
    self.m_Grid:Reposition()
end

