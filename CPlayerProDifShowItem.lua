-- Auto Generated!!
local Color = import "UnityEngine.Color"
local CommonDefs = import "L10.Game.CommonDefs"
local CPlayerProDifShowItem = import "CPlayerProDifShowItem"
local TweenAlpha = import "TweenAlpha"
local UILabel = import "UILabel"
CPlayerProDifShowItem.m_init_CS2LuaHook = function (this, info, pos, _index, _totalNum) 
    this.mDifInfo = info
    this.transform.localPosition = pos
    this.desPos = pos

    CommonDefs.GetComponent_Component_Type(this.transform:Find("Name"), typeof(UILabel)).text = info.name
    local sign = info.nowValue - info.oldValue > 0 and true or false

    this.beginNum = math.floor((info.oldValue))
    this.endNum = math.floor((info.nowValue))
    this.numShowRate = (this.endNum - this.beginNum) / this.numShowTime

    if info.showPercent then
        if info.percentPrecision == 2 then
            local oldStr = System.String.Format("{0:f2}%", NumberTruncate((info.oldValue * 100), 2))
            --info.oldValue * 100).ToString("f3");
            local newStr = System.String.Format("{0:f2}%", NumberTruncate((info.nowValue * 100), 2))
            --info.nowValue * 100).ToString("f3");
            CommonDefs.GetComponent_Component_Type(this.transform:Find("SaveValue"), typeof(UILabel)).text = oldStr
            CommonDefs.GetComponent_Component_Type(this.transform:Find("NowValue"), typeof(UILabel)).text = newStr
        else
            local oldStr = System.String.Format("{0}%", NumberTruncate((info.oldValue * 100), info.percentPrecision))
            --info.oldValue * 100).ToString("f3");
            local newStr = System.String.Format("{0}%", NumberTruncate((info.nowValue * 100), info.percentPrecision))
            --info.nowValue * 100).ToString("f3");
            CommonDefs.GetComponent_Component_Type(this.transform:Find("SaveValue"), typeof(UILabel)).text = oldStr
            CommonDefs.GetComponent_Component_Type(this.transform:Find("NowValue"), typeof(UILabel)).text = newStr
        end
    elseif info.showFractional then
        local oldStr = System.String.Format("{0}", NumberTruncate(info.oldValue, info.percentPrecision))
        --info.oldValue * 100).ToString("f3");
        local newStr = System.String.Format("{0}", NumberTruncate(info.nowValue, info.percentPrecision))
        --info.nowValue * 100).ToString("f3");
        CommonDefs.GetComponent_Component_Type(this.transform:Find("SaveValue"), typeof(UILabel)).text = oldStr
        CommonDefs.GetComponent_Component_Type(this.transform:Find("NowValue"), typeof(UILabel)).text = newStr
    else
        CommonDefs.GetComponent_Component_Type(this.transform:Find("SaveValue"), typeof(UILabel)).text = tostring(this.beginNum)
        CommonDefs.GetComponent_Component_Type(this.transform:Find("NowValue"), typeof(UILabel)).text = tostring(this.beginNum)
    end
    --if (!info.showPercent)
    --{
    --    this.transform.FindChild("SaveValue").GetComponent<UILabel>().text = beginNum.ToString();
    --    this.transform.FindChild("NowValue").GetComponent<UILabel>().text = beginNum.ToString();
    --}
    --else
    --{
    --    string oldStr = string.Format("{0:f2}%", (info.oldValue * 100).Truncate(2));//info.oldValue * 100).ToString("f3");
    --    string newStr = string.Format("{0:f2}%", (info.nowValue * 100).Truncate(2));//info.nowValue * 100).ToString("f3");
    --    this.transform.FindChild("SaveValue").GetComponent<UILabel>().text = oldStr;
    --    this.transform.FindChild("NowValue").GetComponent<UILabel>().text = newStr;
    --}

    if info.nowValue - info.oldValue > 0 then
        this.transform:Find("Arrow/up").gameObject:SetActive(true)
        this.transform:Find("Arrow/down").gameObject:SetActive(false)
        CommonDefs.GetComponent_Component_Type(this.transform:Find("NowValue"), typeof(UILabel)).color = Color.green
    elseif info.nowValue - info.oldValue < 0 then
        this.transform:Find("Arrow/up").gameObject:SetActive(false)
        this.transform:Find("Arrow/down").gameObject:SetActive(true)
        CommonDefs.GetComponent_Component_Type(this.transform:Find("NowValue"), typeof(UILabel)).color = Color.red
    else
        this.transform:Find("Arrow/up").gameObject:SetActive(false)
        this.transform:Find("Arrow/down").gameObject:SetActive(false)
        CommonDefs.GetComponent_Component_Type(this.transform:Find("NowValue"), typeof(UILabel)).color = Color.white
    end

    this.index = _index
    this.totalNum = _totalNum

    local ArrowNode = this.transform:Find("Arrow").gameObject
    if not ArrowNode.activeSelf then
        ArrowNode:SetActive(true)
        this:addAlphaChange(ArrowNode, 0.2, 0.1 * this.totalNum + 0.3)
    end

    this:StartCoroutine(this:show())
end
CPlayerProDifShowItem.m_addAlphaChange_CS2LuaHook = function (this, node, time, delayTime) 
    local ta = CommonDefs.AddComponent_GameObject_Type(node, typeof(TweenAlpha))
    ta.duration = time
    ta.from = 0.1
    ta.to = 1
    ta.delay = delayTime
end
