local CScene = import "L10.Game.CScene"

LuaHalloween2021Mgr = {}
LuaHalloween2021Mgr.todayMatchNum = 0
LuaHalloween2021Mgr.flowerParadeX = 0
LuaHalloween2021Mgr.flowerParadeY = 0
LuaHalloween2021Mgr.jieYuanState = 1
LuaHalloween2021Mgr.HuaCheState = 1
LuaHalloween2021Mgr.isSpy = true
LuaHalloween2021Mgr.jieYuanVoteExpireTime = 0
LuaHalloween2021Mgr.jieYuanVoteInfo = {}
LuaHalloween2021Mgr.jieYuanVoteEnd = false
LuaHalloween2021Mgr.neiGuiPlayerId = 0
LuaHalloween2021Mgr.votePlayerId = 0
LuaHalloween2021Mgr.voteResult = 0
LuaHalloween2021Mgr.jieyuanPuzzlesInfo = {}
LuaHalloween2021Mgr.jieyuanAwardGot = false
LuaHalloween2021Mgr.jieyuanCanGetAward = false

function LuaHalloween2021Mgr:IsSpyRunAway()
    return (self.isSpy and (self.voteResult == EnumPlayResult.eVictory)) or 
        (not self.isSpy and (self.voteResult == EnumPlayResult.eLose)) or 
        self.voteResult == EnumPlayResult.eDraw
end

function LuaHalloween2021Mgr:IsInGamePlay()
    return self:IsInYouHuaChePlay() or self:IsInJieLiangYuanPlay()
end

function LuaHalloween2021Mgr:IsInYouHuaChePlay()
    if not CScene.MainScene then return false end
    local gamePlayId = CScene.MainScene.GamePlayDesignId
    return gamePlayId == Halloween2021_YouHuaChe.GetData().GamePlayId
end

function LuaHalloween2021Mgr:IsInJieLiangYuanPlay()
    if not CScene.MainScene then return false end
    local gamePlayId = CScene.MainScene.GamePlayDesignId
    return gamePlayId == Halloween2021_JieLiangYuan.GetData().GamePlayId
end

function LuaHalloween2021Mgr:SyncHalloweenYouHuaChePlayInfo(state)
    self.HuaCheState = state
    g_ScriptEvent:BroadcastInLua("OnSyncHalloweenYouHuaChePlayInfo")
end

function LuaHalloween2021Mgr:UpdateHuaCheMapPos(engineId,posX, posY)
    self.flowerParadeX = posX
    self.flowerParadeY = posY
    g_ScriptEvent:BroadcastInLua("OnUpdateHuaCheMapPos")
end

function LuaHalloween2021Mgr:OpenJieLiangYuanSignUpWnd(bSignUp, dailyAwardTimes)
    if not CUIManager.IsLoaded(CLuaUIResources.Halloween2021GamePlayWnd) then
        CUIManager.ShowUI(CLuaUIResources.Halloween2021GamePlayWnd)
    else
        g_ScriptEvent:BroadcastInLua("OnOpenJieLiangYuanSignUpWnd",bSignUp, dailyAwardTimes)
    end
end

function LuaHalloween2021Mgr:SyncJieLiangYuanSignUpResult(bSignUp)
    g_ScriptEvent:BroadcastInLua("OnSyncJieLiangYuanSignUpResult",bSignUp)
end

function LuaHalloween2021Mgr:JieLianYuanNotifyNeiGui(neiGuiPlayerId)
    CUIManager.ShowUI(CLuaUIResources.Halloween2021SpyTipWnd)
end

function LuaHalloween2021Mgr:JieLiangYuanSyncPlayInfo(stage, isNeiGui, puzzleListUD)
    self.isSpy = isNeiGui
    self.jieYuanState = stage
    self.jieyuanPuzzlesInfo = {}
	local list = MsgPackImpl.unpack(puzzleListUD)
	if not list then return end
	for i = 0, list.Count - 1, 2 do
		local id = list[i]
		local finished = list[i+1]
        table.insert(self.jieyuanPuzzlesInfo,{id = id, finished = finished})
	end
    g_ScriptEvent:BroadcastInLua("OnJieLiangYuanSyncPlayInfo")
end

function LuaHalloween2021Mgr:JieLiangYuanSyncVoteNeiGuiInfo(expireTime, voteInfoUD, votePlayerId, voteConfirmed, voteEnd, neiGuiPlayerId, result, canGetAward, awardGot,  forceOpen)
	self.jieYuanVoteExpireTime = expireTime
    self.jieYuanVoteEnd = voteEnd
    self.neiGuiPlayerId = neiGuiPlayerId
    self.jieyuanCanGetAward = canGetAward
    if CClientMainPlayer.Inst and voteEnd then
        self.isSpy = CClientMainPlayer.Inst.Id == neiGuiPlayerId
    end
    local t = {}
    local list = MsgPackImpl.unpack(voteInfoUD)
	if not list then return end
	for i = 0, list.Count - 1, 5 do
		local playerId = list[i]
		local playerName = list[i+1]
		local playerClass = list[i+2]
		local playerGender = list[i+3]
		local votedNum = list[i+4]
        table.insert(t,{playerId = playerId,playerName = playerName,playerClass = playerClass,playerGender = playerGender,votedNum = votedNum})
	end
    self.jieYuanVoteInfo = t
    self.voteResult = result
    if forceOpen then
        CUIManager.ShowUI(CLuaUIResources.Halloween2021JieYuanVoteWnd)
        return
    end
    if CUIManager.IsLoaded(CLuaUIResources.Halloween2021JieYuanVoteWnd) then
        g_ScriptEvent:BroadcastInLua("OnJieLiangYuanSyncVoteNeiGuiInfo")
	end
end

function LuaHalloween2021Mgr:JieLiangYuanGetAwardSuccess()
    self.jieyuanCanGetAward = false
    if CUIManager.IsLoaded(CLuaUIResources.Halloween2021JieYuanVoteWnd) then
        g_ScriptEvent:BroadcastInLua("OnJieLiangYuanSyncVoteNeiGuiInfo")
	end
end
