local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local LayerDefine = import "L10.Engine.LayerDefine"
local CommonDefs = import "L10.Game.CommonDefs"
local CRenderObject = import "L10.Engine.CRenderObject"
local Quaternion = import "UnityEngine.Quaternion"
local CBoidManager = import "L10.Game.CBoidManager"
local CBoid = import "L10.Game.CBoid"
local BoidSettings = import "L10.Game.BoidSettings"

CLuaHouseFishMgr = {}

CLuaHouseFishMgr.m_FishList = nil
CLuaHouseFishMgr.m_PlacedZhenZhaiYu = nil
CLuaHouseFishMgr.m_PlacedGuanShangYu = nil
CLuaHouseFishMgr.m_FishZswTemplateIdList = nil
CLuaHouseFishMgr.m_FishCount = 0
CLuaHouseFishMgr.m_LastFishCount = 0
CLuaHouseFishMgr.m_IsHouseFish = false
CLuaHouseFishMgr.m_IsMingYuan = false

function CLuaHouseFishMgr:Init(sceneName)
    if not IsHouseFishOpen() then
        return
    end

    self.m_FishList = {}
    
    self.m_IsMingYuan = sceneName == "jiayuan02new"
    self.m_IsHouseFish = self.m_IsMingYuan or sceneName == "jiayuan01new"

    CBoidManager.Inst.m_IsInHouse = self.m_IsHouseFish
    if self.m_IsHouseFish then
        if self.m_IsMingYuan then 
            self:InitMingYuanHouseFishSettings()
        else
            self:InitHouseFishSettings()
        end
        self:OnUpdateHouseFishInfo()
    else 
        self:InitSeaFishSettings()
        self:OnUpdateUnderSeaFishInfo()
    end
    
    CBoidManager.Inst:StartFishBoid()
end

function CLuaHouseFishMgr:OnUpdateHouseFishInfo()
    if not self.m_IsHouseFish then
        return
    end
    self:InitPlacedZhenZhaiYu()

    self.m_FishZswTemplateIdList = {}
    for i,data in ipairs(self.m_PlacedZhenZhaiYu) do
        local zswId = data.itemId
        local count = data.count
        for c=1,count,1 do
            table.insert(self.m_FishZswTemplateIdList, zswId)
        end
    end
    self:InitFish(CBoidManager.Inst.gameObject)
    CBoidManager.Inst:UpdateFishBoid()
end

function CLuaHouseFishMgr:OnUpdateUnderSeaFishInfo()
    self:InitPlacedZhenZhaiYu()
    self:InitPlacedGuanShangYu()

    self.m_FishZswTemplateIdList = {}
    for i,data in ipairs(self.m_PlacedZhenZhaiYu) do
        local zswId = data.itemId
        local count = data.count
        for c=1,count,1 do
            table.insert(self.m_FishZswTemplateIdList, zswId)
        end
    end
    for i,data in ipairs(self.m_PlacedGuanShangYu) do
        local zswId = data.itemId
        local count = data.count
        for c=1,count,1 do
            table.insert(self.m_FishZswTemplateIdList, zswId)
        end
    end
    self:InitFish(CBoidManager.Inst.gameObject)
    CBoidManager.Inst:UpdateFishBoid()
end

function CLuaHouseFishMgr:InitFish(node) 
    for i = 1, #self.m_FishZswTemplateIdList do
        local id = self.m_FishZswTemplateIdList[i]
        local data = Zhuangshiwu_Zhuangshiwu.GetData(id)
        local itemId = data.ItemId
        local fishdata = HouseFish_AllFishes.GetData(itemId)
        if not fishdata then break end

        local prefabname = fishdata.PrefabPath
        local mfish = self.m_FishList[i]
        local newRo = nil
        if mfish and mfish.ro then
            newRo = mfish.ro
        else
            newRo = CRenderObject.CreateRenderObject(node, "fish_"..i)           
        end
        newRo.Layer = LayerDefine.Pet
        newRo:LoadMain(prefabname)

        local boid = CommonDefs.AddComponent_GameObject_Type(newRo.gameObject, typeof(CBoid))
        local fish = {}
        fish.ro = newRo
        local boidSettings
        if self.m_IsHouseFish and fishdata.FarawayFish == 1 then
            boid.kindId = 1
            boidSettings = CommonDefs.DictGetValue_LuaCall(CBoidManager.Inst.settingDic, 0)
            boid:Initialize(boidSettings, nil)
        else 
            boid.kindId = 0
            boidSettings = CBoidManager.Inst.defaultSettings
            boid:Initialize(boidSettings, self.m_IsHouseFish and CClientMainPlayer.Inst.RO.transform or nil)
        end

        local scale = data.Scale * 1.5
        newRo.transform.localRotation = Quaternion.Euler(0,-90,0)
        newRo.transform.localScale = Vector3(scale, scale, scale)
        newRo.transform.position = boidSettings:GetRandomPosInBounds(0)
    end
end

function CLuaHouseFishMgr:LeaveJiaYuan()
    if not IsHouseFishOpen() then
        return
    end
    if not self.m_FishList then
        return
    end
    CBoidManager.Inst:EndFishBoid()
    self.m_FishList = nil
    self.m_PlacedZhenZhaiYu = nil
    self.m_FishZswTemplateIdList = nil
    self.m_FishCount = 0
    self.m_LastFishCount = 0
    self.m_IsHouseFish = false
    self.m_IsMingYuan = false
end

function CLuaHouseFishMgr:InitMingYuanHouseFishSettings()
    local defaultSettings = CBoidManager.Inst.defaultSettings
    local centers = {Vector3(7, 18, 102), Vector3(22, 18, 123), Vector3(22, 18, 66) }
    local sizes = {Vector3(16, 4, 110), Vector3(16, 4, 55), Vector3(16, 4, 8) }
    local route = {}
    self:InitSettings(defaultSettings, centers, sizes, route)

    CommonDefs.DictClear(CBoidManager.Inst.settingDic)
    local bigFishSettings0 = CreateFromClass(BoidSettings)
    centers = {Vector3(-70, 18, 102)}
    sizes = {Vector3(16, 4, 110) }
    route = {Vector3(-70, 18, 47), Vector3(-70, 18, 157)}
    self:InitSettings(bigFishSettings0, centers, sizes, route, nil, nil, nil, nil, 16)
    CommonDefs.DictAdd_LuaCall(CBoidManager.Inst.settingDic, 0, bigFishSettings0)

    local bigFishSettings1 = CreateFromClass(BoidSettings)
    centers = {Vector3(-20, 18, 102)}
    sizes = {Vector3(16, 4, 110) }
    route = {Vector3(-20, 18, 47), Vector3(-20, 18, 157)}
    self:InitSettings(bigFishSettings1, centers, sizes, route, nil, nil, nil, nil, 16)
    CommonDefs.DictAdd_LuaCall(CBoidManager.Inst.settingDic, 1, bigFishSettings1)
end

function CLuaHouseFishMgr:InitHouseFishSettings()
    local defaultSettings = CBoidManager.Inst.defaultSettings
    local centers = {Vector3(2, 24, 84) }
    local sizes = {Vector3(26, 4, 110) }
    local route = {}
    self:InitSettings(defaultSettings, centers, sizes, route)

    CommonDefs.DictClear(CBoidManager.Inst.settingDic)
    local bigFishSettings0 = CreateFromClass(BoidSettings)
    centers = {Vector3(-75, 24, 84)}
    sizes = {Vector3(16, 4, 110) }
    route = {Vector3(-75, 24, 29), Vector3(-75, 24, 139)}
    self:InitSettings(bigFishSettings0, centers, sizes, route, nil, nil, nil, nil, 16)
    CommonDefs.DictAdd_LuaCall(CBoidManager.Inst.settingDic, 0, bigFishSettings0)

    local bigFishSettings1 = CreateFromClass(BoidSettings)
    centers = {Vector3(-25, 26, 84)}
    sizes = {Vector3(16, 4, 110) }
    route = {Vector3(-25, 24, 29), Vector3(-25, 24, 139)}
    self:InitSettings(bigFishSettings1, centers, sizes, route, nil, nil, nil, nil, 16)
    CommonDefs.DictAdd_LuaCall(CBoidManager.Inst.settingDic, 1, bigFishSettings1)
end

function CLuaHouseFishMgr:InitSeaFishSettings()
    local defaultSettings = CBoidManager.Inst.defaultSettings
    local centers = {Vector3(60, 23, 87)}
    local sizes = {Vector3(100, 10, 90)}
    local route = {}
    self:InitSettings(defaultSettings, centers, sizes, route)

    CommonDefs.DictClear(CBoidManager.Inst.settingDic)
end

function CLuaHouseFishMgr:InitSettings(settings, centers, sizes, route, reachDst, maxSpeed, minSpeed, boidRate, targetEffectMul)
    CommonDefs.ListClear(settings.tankCenters)
    for i = 1, #centers do
        CommonDefs.ListAdd_LuaCall(settings.tankCenters, centers[i])
    end
    CommonDefs.ListClear(settings.tankSizes)
    for i = 1, #sizes do
        CommonDefs.ListAdd_LuaCall(settings.tankSizes, sizes[i])
    end
    CommonDefs.ListClear(settings.route)
    for i = 1, #route do
        CommonDefs.ListAdd_LuaCall(settings.route, route[i])
    end

    settings.reachDst = reachDst or 8
    settings.maxSpeed = maxSpeed or 4
    settings.minSpeed = minSpeed or 2
    settings.boidRate = boidRate or 0.2
    settings.targetEffectMul = targetEffectMul or 1
    settings:TryInitBounds()
end

function CLuaHouseFishMgr:InitPlacedZhenZhaiYu()
    self.m_PlacedZhenZhaiYu = {}
    if not CClientHouseMgr.Inst.mCurFurnitureProp2 then return end
    local fishInfo = CClientHouseMgr.Inst and CClientHouseMgr.Inst.mCurFurnitureProp2.FishInfo
    if fishInfo then
        local dic = fishInfo and fishInfo.ZhengzhaiFish
        local playerId = CClientMainPlayer.Inst.Id
        if dic then
            CommonDefs.DictIterate(dic, DelegateFactory.Action_object_object(function (___key1, ___value1) 
                local playId = tonumber(___key1)
                local zhenzhaiFishMap = ___value1.AllFishes
                CommonDefs.DictIterate(zhenzhaiFishMap, DelegateFactory.Action_object_object(function (___key2, ___value2) 
                    local data = {}
                    data.fishmapId = tonumber(___key2)
                    data.itemId = tonumber(___value2.TemplateId)
                    data.count = 1
                    if Zhuangshiwu_Zhuangshiwu.GetData(data.itemId) then
                        table.insert(self.m_PlacedZhenZhaiYu, data)
                    end
                end))
            end))
        end
    end
end

function CLuaHouseFishMgr:InitPlacedGuanShangYu()
    self.m_PlacedGuanShangYu = {}
    if not CClientHouseMgr.Inst.mCurFurnitureProp2 then return end
    local yardFurnitureData = CClientHouseMgr.Inst and CClientHouseMgr.Inst.mCurFurnitureProp.YardFurnitureData
    CommonDefs.DictIterate(yardFurnitureData, DelegateFactory.Action_object_object(function (___key, ___value) 
        local itemId = Zhuangshiwu_Zhuangshiwu.GetData(___value.FurnitureTemplateId).ItemId
        
        if HouseFish_AllFishes.Exists(itemId) then
            local data = {}
            data.itemId = ___value.FurnitureTemplateId
            data.count = 1
            table.insert(self.m_PlacedGuanShangYu, data)
        end
    end))
end
