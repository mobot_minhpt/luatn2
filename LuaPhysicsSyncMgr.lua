local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CPhysicsObjectMgr=import "L10.Engine.CPhysicsObjectMgr"

EnumNavalPlayShipStatus = {
	Normal = 1,
	Supply = 2,
	Escape = 3,
}

EnumLuaPacketType = {
    eSteering = 1,
    eFreeze = 2,
    eTeleport = 3,
    eWorldForce = 4,
    eSnapshot = 5,
    eDriverLeaveRiding = 6,
    eEffector = 7,
    eMulSpeed = 8,
    eSendBullet = 9,
}

CPhysicsObjectMgr.m_hookShowMonsterWithPhysicsTemplateId = function(this,engineId,physicsTemplateId)
    local obj = CClientObjectMgr.Inst:GetObject(engineId)
    if obj then
        local pos = obj.RO.Position
        local controllerType = Physics_Object.GetData(physicsTemplateId).controllerType
        if controllerType == EnumPhysicsController.eNone then
            LuaHaiZhanMgr.CreateStillPhysicsObject(-1,-engineId,physicsTemplateId,pos.x,pos.z,obj.Dir)
        else
            LuaHaiZhanMgr.CreateShip(-1,-engineId, pos.x, pos.z, obj.Dir,physicsTemplateId,controllerType)
        end
    end
end