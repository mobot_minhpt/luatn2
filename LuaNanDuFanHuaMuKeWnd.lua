local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local TouchPhase = import "UnityEngine.TouchPhase"
local CMainCamera = import "L10.Engine.CMainCamera"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local Texture = import "UnityEngine.Texture"
local Texture2D = import "UnityEngine.Texture2D"
local TextureFormat = import "UnityEngine.TextureFormat"
local Vector4 = import "UnityEngine.Vector4"
local TextureWrapMode = import "UnityEngine.TextureWrapMode"
local Animation = import "UnityEngine.Animation"
local CMuKeDraw = import "L10.UI.CMuKeDraw"
local Vector2 = import "UnityEngine.Vector2"

LuaNanDuFanHuaMuKeWnd = class()
--m_CurPhase 1:初始,需要进行雕刻 2:已雕刻,需要进行上色 3:已上色,需要进行印制
LuaNanDuFanHuaMuKeWnd.m_CurPhase = 1
--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaNanDuFanHuaMuKeWnd, "LeftWidget", "LeftWidget", GameObject)
RegistChildComponent(LuaNanDuFanHuaMuKeWnd, "Draft", "Draft", GameObject)
RegistChildComponent(LuaNanDuFanHuaMuKeWnd, "DraftTextBg", "DraftTextBg", GameObject)
RegistChildComponent(LuaNanDuFanHuaMuKeWnd, "DraftInUse", "DraftInUse", GameObject)
RegistChildComponent(LuaNanDuFanHuaMuKeWnd, "LeftPlank1", "LeftPlank1", GameObject)
RegistChildComponent(LuaNanDuFanHuaMuKeWnd, "LeftPlank2", "LeftPlank2", GameObject)
RegistChildComponent(LuaNanDuFanHuaMuKeWnd, "LeftPlank3", "LeftPlank3", GameObject)
RegistChildComponent(LuaNanDuFanHuaMuKeWnd, "LeftPlank4", "LeftPlank4", GameObject)
RegistChildComponent(LuaNanDuFanHuaMuKeWnd, "MiddleWidget", "MiddleWidget", GameObject)
RegistChildComponent(LuaNanDuFanHuaMuKeWnd, "MiddlePlank1", "MiddlePlank1", GameObject)
RegistChildComponent(LuaNanDuFanHuaMuKeWnd, "MiddlePlank2", "MiddlePlank2", GameObject)
RegistChildComponent(LuaNanDuFanHuaMuKeWnd, "MiddlePlank3", "MiddlePlank3", GameObject)
RegistChildComponent(LuaNanDuFanHuaMuKeWnd, "MiddlePlank4", "MiddlePlank4", GameObject)
RegistChildComponent(LuaNanDuFanHuaMuKeWnd, "PaperWidget", "PaperWidget", GameObject)
RegistChildComponent(LuaNanDuFanHuaMuKeWnd, "PaperPlank1", "PaperPlank1", GameObject)
RegistChildComponent(LuaNanDuFanHuaMuKeWnd, "PaperPlank2", "PaperPlank2", GameObject)
RegistChildComponent(LuaNanDuFanHuaMuKeWnd, "PaperPlank3", "PaperPlank3", GameObject)
RegistChildComponent(LuaNanDuFanHuaMuKeWnd, "PaperPlank4", "PaperPlank4", GameObject)
RegistChildComponent(LuaNanDuFanHuaMuKeWnd, "RicePaper", "RicePaper", GameObject)
RegistChildComponent(LuaNanDuFanHuaMuKeWnd, "Example", "Example", GameObject)
RegistChildComponent(LuaNanDuFanHuaMuKeWnd, "AboveAll", "AboveAll", GameObject)
RegistChildComponent(LuaNanDuFanHuaMuKeWnd, "Finger", "Finger", GameObject)
RegistChildComponent(LuaNanDuFanHuaMuKeWnd, "TintText", "TintText", UILabel)
RegistChildComponent(LuaNanDuFanHuaMuKeWnd, "RightWidget", "RightWidget", GameObject)
RegistChildComponent(LuaNanDuFanHuaMuKeWnd, "KeDao", "KeDao", GameObject)
RegistChildComponent(LuaNanDuFanHuaMuKeWnd, "MaoBi", "MaoBi", GameObject)
RegistChildComponent(LuaNanDuFanHuaMuKeWnd, "XuanZhi", "XuanZhi", GameObject)
RegistChildComponent(LuaNanDuFanHuaMuKeWnd, "YinTuo", "YinTuo", GameObject)
RegistChildComponent(LuaNanDuFanHuaMuKeWnd, "BrushWidget", "BrushWidget", GameObject)
RegistChildComponent(LuaNanDuFanHuaMuKeWnd, "Red", "Red", GameObject)
RegistChildComponent(LuaNanDuFanHuaMuKeWnd, "Green", "Green", GameObject)
RegistChildComponent(LuaNanDuFanHuaMuKeWnd, "Black", "Black", GameObject)
RegistChildComponent(LuaNanDuFanHuaMuKeWnd, "Brown", "Brown", GameObject)

--@endregion RegistChildComponent end

function LuaNanDuFanHuaMuKeWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self:SetWndState(LuaNanDuFanHuaMuKeWnd.m_CurPhase)
end

function LuaNanDuFanHuaMuKeWnd:Init()

end

--@region UIEvent

--@endregion UIEvent
function LuaNanDuFanHuaMuKeWnd:SetWndState(wndState)
    self.leftPlankList = {self.LeftPlank1, self.LeftPlank2, self.LeftPlank3, self.LeftPlank4}
    self.middlePlankList = {self.MiddlePlank1, self.MiddlePlank2, self.MiddlePlank3, self.MiddlePlank4}
    self.paperPlankList = {self.PaperPlank1, self.PaperPlank2, self.PaperPlank3, self.PaperPlank4}
    self.copyPaperPlankList = {self.PaperWidget.transform:Find("PaperPlankCopy1").gameObject,
                               self.PaperWidget.transform:Find("PaperPlankCopy2").gameObject,
                               self.PaperWidget.transform:Find("PaperPlankCopy3").gameObject,
                               self.PaperWidget.transform:Find("PaperPlankCopy4").gameObject}
    for i = 1, #self.copyPaperPlankList do
        self.copyPaperPlankList[i]:SetActive(true)
    end
    self.toolsList = {self.KeDao, self.MaoBi, self.XuanZhi, self.YinTuo}
    self.BrushList = {self.Red, self.Black, self.Green, self.Brown}
    self.leftSelectPlankIndex = 1
    if wndState == 1 then
        self:InitWndStateOne(1)
        CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.MuKeGuide)
    elseif wndState == 2 then
        self:InitWndStateTwo(2)
    elseif wndState == 3 then
        self:InitWndStateThree(3)
    end
end

function LuaNanDuFanHuaMuKeWnd:InitWndStateOne(wndState)
    self.finishPlankList = {}
    self.graveRange = 100
    self.m_IsDragging = false   
    self.checkDragDotIndex = nil
    self.checkDragPlankIndex = nil
    self.Finger:SetActive(false)
    self.Finger.transform:Find("MaoBi").gameObject:SetActive(false)
    self.Finger.transform:Find("KeDao").gameObject:SetActive(true)
    self.Finger.transform:Find("YinTuo").gameObject:SetActive(false)
    
    for i = 1, #self.leftPlankList do
        self:SetPlank2OriginalState(self.leftPlankList[i])

        local obj = self.leftPlankList[i].transform:Find("Plank").gameObject
        UIEventListener.Get(obj).onClick = DelegateFactory.VoidDelegate(function (go)
            self:SetDraftOrLeftPlankClick(i)

            if not self.finishPlankList[i] then
                self.TintText.text = g_MessageMgr:FormatMessage("NANDUFANHUA_DIAOKE_TIPS")
            else
                self.TintText.text = g_MessageMgr:FormatMessage("NANDUFANHUA_DIAOKE_NEXT_TIPS")
            end
            self.TintText.transform:GetComponent(typeof(Animation)):Play("nandufanhuamukewnd_tinttext")
        end)
    end
    UIEventListener.Get(self.Draft).onClick = DelegateFactory.VoidDelegate(function (go)
        self:SetDraftOrLeftPlankClick(0)
    end)
    for i = 1, #self.middlePlankList do
        self.middlePlankList[i].transform:Find("RouteTips").gameObject:SetActive(true)
        self:SetPlank2OriginalState(self.middlePlankList[i])

        local obj = self.middlePlankList[i].transform:Find("Plank").gameObject
        UIEventListener.Get(obj).onDragStart = DelegateFactory.VoidDelegate(function (go)
            if not self.finishPlankList[i] then
                self.Finger:SetActive(true)
                self.m_IsDragging = true
                if CommonDefs.IsInMobileDevice() then
                    self.m_FingerIndex = Input.GetTouch(0).fingerId
                end
                self.checkDragDotIndex = 1
                self.checkDragPlankIndex = i
            end
        end)
    end
    self.PaperWidget:SetActive(false)
    self.BrushWidget:SetActive(true)
    for i = 1, #self.BrushList do
        self.BrushList[i].transform:Find("Active").gameObject:SetActive(false)
        self.BrushList[i].transform:Find("Selected").gameObject:SetActive(false)
    end
    
    for i = 1, #self.toolsList do
        Extensions.SetLocalPositionZ(self.toolsList[i].transform, i == wndState and 0 or -1)
        Extensions.SetLocalPositionX(self.toolsList[i].transform, i == wndState and -50 or 0)
    end
    self.TintText.text = g_MessageMgr:FormatMessage("NANDUFANHUA_DIAOKE_TIPS")
    self:SetDraftOrLeftPlankClick(1)
end

function LuaNanDuFanHuaMuKeWnd:InitWndStateTwo(wndState)
    self.finishColorPlankList = {}
    self.paintColorIndex = 1
    --对齐, 把index=1空出来
    self.maskSizeList = {{0,0}, {1024,512}, {256,256}, {512,512}}
    self.colorCfg = {{}}
    
    local colorStr = NanDuFanHua_MuKeSetting.GetData().MuKeColor
    local arr = g_LuaUtil:StrSplit(colorStr,";")
    for i = 1, #arr do
        local subString = arr[i]
        local subStringList = g_LuaUtil:StrSplit(subString,",")
        local tbl = {}
        for j = 1, #subStringList do
            table.insert(tbl, tonumber(subStringList[j]))
        end
        self.colorCfg[i] = tbl
    end
    self.Finger:SetActive(false)
    self.Finger.transform:Find("MaoBi").gameObject:SetActive(true)
    self.Finger.transform:Find("KeDao").gameObject:SetActive(false)
    self.Finger.transform:Find("YinTuo").gameObject:SetActive(false)
    
    for i = 1, #self.leftPlankList do
        self:SetPlank2AfterGraveState(self.leftPlankList[i])
        self.finishColorPlankList[i] = {0,0,0,0}
        local obj = self.leftPlankList[i].transform:Find("Plank").gameObject
        UIEventListener.Get(obj).onClick = DelegateFactory.VoidDelegate(function (go)
            self:SetDraftOrLeftPlankClick(i)
            if i == 1 then
                self.state2CheckTex = nil
            else
                self.state2CheckTex = self.middlePlankList[i].transform:Find("PaintSculpture"):GetComponent(typeof(UITexture))
            end
            local ret = true
            for j = 1, 3 do
                if self.finishColorPlankList[i][j] == 0 then
                    ret = false
                end
            end
            if ret == false then
                self.TintText.text = g_MessageMgr:FormatMessage("NANDUFANHUA_SHANGSE_TIPS")
            else
                self.TintText.text = g_MessageMgr:FormatMessage("NANDUFANHUA_SHANGSE_NEXT_TIPS")
            end
            self.TintText.transform:GetComponent(typeof(Animation)):Play("nandufanhuamukewnd_tinttext")
        end)
    end
    UIEventListener.Get(self.Draft).onClick = DelegateFactory.VoidDelegate(function (_)
        self:SetDraftOrLeftPlankClick(0)
        self.state2CheckTex = nil
    end)
    for i = 1, #self.middlePlankList do
        self.middlePlankList[i].transform:Find("RouteTips").gameObject:SetActive(false)
        self:SetPlank2AfterGraveState(self.middlePlankList[i])

        if i == 1 then
            local paintTransform = self.middlePlankList[i].transform:Find("Plank")
            if paintTransform then
                local obj = paintTransform.gameObject
                UIEventListener.Get(obj).onClick = DelegateFactory.VoidDelegate(function (go)
                    if CommonDefs.IsInMobileDevice() then
                        self.m_FingerIndex = Input.GetTouch(0).fingerId
                    end
                    self.checkDragPlankIndex = i

                    local pos = Input.mousePosition
                    self.Finger.transform.position = CUIManager.UIMainCamera:ScreenToWorldPoint(Input.mousePosition)
                    local res = self:CheckPickColorLegal(self.checkDragPlankIndex, pos)
                    if res then
                        self.Finger:SetActive(true)
                        self.Finger.transform:Find("MaoBi"):GetComponent(typeof(Animation)):Stop()
                        self.Finger.transform:Find("MaoBi"):GetComponent(typeof(Animation)):Play("nandufanhuamukewnd_maobi")
                    else
                        g_MessageMgr:ShowMessage("NANDUFANHUA_MUKE_SHANGESE_WRONG_COLOR")
                    end
                end)
            end
        else
            local paintTransform = self.middlePlankList[i].transform:Find("PaintSculpture")
            if paintTransform then
                local obj = paintTransform.gameObject
                UIEventListener.Get(obj).onClick = DelegateFactory.VoidDelegate(function (go)
                    self.leftState2Tex = self.leftPlankList[i].transform:Find("PaintSculpture"):GetComponent(typeof(UITexture))
                    self.state2CheckTexWidth = self.state2CheckTex.width
                    self.state2CheckTexHeight = self.state2CheckTex.height
                    self.maskTex = TypeAs(self.state2CheckTex.material:GetTexture("_ColorMaskTex1"), typeof(Texture2D))
                    if CommonDefs.IsInMobileDevice() then
                        self.m_FingerIndex = Input.GetTouch(0).fingerId
                    end
                    self.checkDragPlankIndex = i

                    local pos = Input.mousePosition
                    self.Finger.transform.position = CUIManager.UIMainCamera:ScreenToWorldPoint(Input.mousePosition)
                    local res, reason = self:CheckPickColorLegal(self.checkDragPlankIndex, pos)
                    if res then
                        self.Finger:SetActive(true)
                        self.Finger.transform:Find("MaoBi"):GetComponent(typeof(Animation)):Stop()
                        self.Finger.transform:Find("MaoBi"):GetComponent(typeof(Animation)):Play("nandufanhuamukewnd_maobi")
                    else
                        if reason == "emptyField" then
                            g_MessageMgr:ShowMessage("NANDUFANHUA_MUKE_SHANGESE_EMPTY_AREA")
                        elseif reason == "paintAlready" then
                            g_MessageMgr:ShowMessage("NANDUFANHUA_MUKE_SHANGESE_PAINT_COLOR_ALREADY")
                        else
                            g_MessageMgr:ShowMessage("NANDUFANHUA_MUKE_SHANGESE_WRONG_COLOR")
                        end
                    end
                end)
            end
        end
    end
    self.PaperWidget:SetActive(false)
    self.BrushWidget:SetActive(true)
    for i = 1, #self.toolsList do
        Extensions.SetLocalPositionZ(self.toolsList[i].transform, i == wndState and 0 or -1)
        Extensions.SetLocalPositionX(self.toolsList[i].transform, i == wndState and -50 or 0)
    end
    self.TintText.text = g_MessageMgr:FormatMessage("NANDUFANHUA_SHANGSE_TIPS")
    self:SetDraftOrLeftPlankClick(1)

    for i = 1, #self.BrushList do
        self.BrushList[i].transform:Find("Selected").gameObject:SetActive(i == self.paintColorIndex)

        local obj = self.BrushList[i]
        UIEventListener.Get(obj).onClick = DelegateFactory.VoidDelegate(function (go)
            self.paintColorIndex = i
            self:TryShineOnMiddlePlank(self.leftSelectPlankIndex, i)
            for t = 1, #self.BrushList do
                self.BrushList[t].transform:Find("Selected").gameObject:SetActive(t == self.paintColorIndex)
            end
        end)
    end
end

function LuaNanDuFanHuaMuKeWnd:InitWndStateThree(wndState)
    self.maskSizeList = {{1024,512}, {1024,512}, {256,256}, {512,512}}
    self.copyTex = {}
    self.lastRecordScreenPosition = nil
    self.printTexIndex = 1
    self.finishPrintPlankList = {}
    self.Finger:SetActive(false)
    self.Finger.transform:Find("MaoBi").gameObject:SetActive(false)
    self.Finger.transform:Find("KeDao").gameObject:SetActive(false)
    self.Finger.transform:Find("YinTuo").gameObject:SetActive(true)
    for i = 1, #self.paperPlankList do
        self.paperPlankList[i]:SetActive(false)
    end
    self.mukeDrawComp = self.RicePaper.transform:GetComponent(typeof(CMuKeDraw))
    self.RicePaper:SetActive(true)
    self.PaperWidget:SetActive(true)
    self.BrushWidget:SetActive(false)
    for i = 1, #self.leftPlankList do
        self:SetPlank2AfterColorState(self.leftPlankList[i])

        local obj = self.leftPlankList[i].transform:Find("Plank").gameObject
        UIEventListener.Get(obj).onClick = DelegateFactory.VoidDelegate(function (go)
            if not self.copyTex[i] then
                self:CreateOneCopyTexture(i)
            end
            self.lastRecordScreenPosition = nil
            self.printTexIndex = i
            self:SetDraftOrLeftPlankClick(i)
            self.RicePaper:SetActive(true)
            self.RicePaper:GetComponent(typeof(Animation)):Stop()
            self.RicePaper:GetComponent(typeof(Animation)):Play("nandufanhuamukewnd_paper_in")
            for t = 1, #self.copyPaperPlankList do
                self.copyPaperPlankList[t].transform:GetComponent(typeof(UITexture)).alpha = 0
            end
            RegisterTickOnce(function()
                for t = 1, #self.copyPaperPlankList do
                    if not CommonDefs.IsNull(self.copyPaperPlankList[t]) then
                        self.copyPaperPlankList[t].transform:GetComponent(typeof(UITexture)).alpha = 1
                    end
                end
            end,500)

            if self.finishPrintPlankList[i] then
                self.TintText.text = g_MessageMgr:FormatMessage("NANDUFANHUA_YINTUO_NEXT_TIPS")
            else
                self.TintText.text = g_MessageMgr:FormatMessage("NANDUFANHUA_YINTUO_TIPS")
            end
            self.TintText.transform:GetComponent(typeof(Animation)):Play("nandufanhuamukewnd_tinttext")
        end)
    end
    UIEventListener.Get(self.Draft).onClick = DelegateFactory.VoidDelegate(function (go)
        self.lastRecordScreenPosition = nil
        self:SetDraftOrLeftPlankClick(0)
        self.RicePaper:SetActive(false)
    end)
    for i = 1, #self.middlePlankList do
        self.middlePlankList[i].transform:Find("RouteTips").gameObject:SetActive(false)
        self:SetPlank2AfterColorState(self.middlePlankList[i])
    end
    for i = 1, #self.toolsList do
        Extensions.SetLocalPositionZ(self.toolsList[i].transform, (i == 3 or i == 4) and 0 or -1)
        Extensions.SetLocalPositionX(self.toolsList[i].transform, (i == 3 or i == 4) and -50 or 0)
    end
    
    UIEventListener.Get(self.RicePaper).onDragStart = DelegateFactory.VoidDelegate(function (go)
        self.state3CheckTex = self.paperPlankList[self.printTexIndex].transform:GetComponent(typeof(UITexture))
        self.state3CheckTexWidth = self.state3CheckTex.width
        self.state3CheckTexHeight = self.state3CheckTex.height
        self.maskTex = TypeAs(self.state3CheckTex.material:GetTexture("_MainTex"), typeof(Texture2D))
        self.alphaTex = TypeAs(self.state3CheckTex.material:GetTexture("_AlphaTex"), typeof(Texture2D))
        if CommonDefs.IsInMobileDevice() then
            self.m_FingerIndex = Input.GetTouch(0).fingerId
        end
        self.checkDragPlankIndex = self.printTexIndex
        self.m_IsDragging = true
        self.Finger:SetActive(true)
    end)
    self.TintText.text = g_MessageMgr:FormatMessage("NANDUFANHUA_YINTUO_TIPS")
    self:CreateOneCopyTexture(1)
    self:SetDraftOrLeftPlankClick(1)
    self.printTexIndex = 1
    self.RicePaper:GetComponent(typeof(Animation)):Stop()
    self.RicePaper:GetComponent(typeof(Animation)):Play("nandufanhuamukewnd_paper_in")
    --self.copyPaperPlankList[1]:SetActive(true)
end

function LuaNanDuFanHuaMuKeWnd:CreateOneCopyTexture(plankIndex)
    local refTex = self.paperPlankList[plankIndex].transform:GetComponent(typeof(UITexture)).material.mainTexture
    local baseTex = Texture2D(refTex.width, refTex.height)
    baseTex.wrapMode = TextureWrapMode.Clamp
    local paintColor = Color(1, 1, 1, 0)
    for x = 0, refTex.width do
        for y = 0, refTex.height do
            baseTex:SetPixel(x, y, paintColor);
        end
    end
    baseTex:Apply()
    self.copyPaperPlankList[plankIndex].transform:GetComponent(typeof(UITexture)).mainTexture = baseTex
    self.copyTex[plankIndex] = baseTex
end

function LuaNanDuFanHuaMuKeWnd:Update()
    if LuaNanDuFanHuaMuKeWnd.m_CurPhase == 1 and self.m_IsDragging then
        if CommonDefs.IsInMobileDevice() then
            for i = 0, Input.touchCount - 1 do
                local touch = Input.GetTouch(i)
                if touch.fingerId == self.m_FingerIndex then
                    if touch.phase ~= TouchPhase.Ended and touch.phase ~= TouchPhase.Canceled then
                        if touch.phase ~= TouchPhase.Stationary then
                            local pos = touch.position
                            local result = self:CheckDragLegal(self.checkDragPlankIndex, self.checkDragDotIndex, pos)
                            self.Finger.transform.position = CUIManager.UIMainCamera:ScreenToWorldPoint(Vector3(pos.x, pos.y, 0))
                            if result ~= 0 then
                                self.checkDragDotIndex = self.checkDragDotIndex + result
                            end
                        end
                        return
                    else
                        break
                    end
                end
            end
        else
            if Input.GetMouseButton(0) then
                local result = self:CheckDragLegal(self.checkDragPlankIndex, self.checkDragDotIndex, Input.mousePosition)
                self.Finger.transform.position = CUIManager.UIMainCamera:ScreenToWorldPoint(Input.mousePosition)
                if result ~= 0 then
                    self.checkDragDotIndex = self.checkDragDotIndex + result
                end
                if not Input.GetMouseButtonUp(0) then
                    return
                end
            end
        end
    end

    if LuaNanDuFanHuaMuKeWnd.m_CurPhase == 3 and self.m_IsDragging then
        local checkSimilarity = false
        if CommonDefs.IsInMobileDevice() then
            for i = 0, Input.touchCount - 1 do
                local touch = Input.GetTouch(i)
                if touch.fingerId == self.m_FingerIndex then
                    if touch.phase ~= TouchPhase.Ended and touch.phase ~= TouchPhase.Canceled then
                        if touch.phase ~= TouchPhase.Stationary then
                            local pos = touch.position
                            self.Finger.transform.position = CUIManager.UIMainCamera:ScreenToWorldPoint(Vector3(pos.x, pos.y, 0))
                            self:CheckPaint2Paper(self.checkDragPlankIndex, pos)
                        end
                        return
                    else
                        self.m_IsDragging = false
                        self.Finger:SetActive(false)
                        checkSimilarity = true
                        break
                    end
                end
            end
        else
            if Input.GetMouseButton(0) then
                self:CheckPaint2Paper(self.checkDragPlankIndex, Input.mousePosition)
                self.Finger.transform.position = CUIManager.UIMainCamera:ScreenToWorldPoint(Input.mousePosition)
            elseif Input.GetMouseButtonUp(0) then
                self.m_IsDragging = false
                self.Finger:SetActive(false)
                checkSimilarity = true
            end
        end

        if checkSimilarity then
            self:OnCheckSimlarity(self.checkDragPlankIndex)
        end
    end
end

function LuaNanDuFanHuaMuKeWnd:OnCheckSimlarity(dragPlankIndex)

    local newTex = self.copyTex[dragPlankIndex]
    local maskSize = self.maskSizeList[dragPlankIndex]
    local ratio = self.mukeDrawComp:CheckSimlarityOnlyAlpha(newTex, self.alphaTex, maskSize[1], maskSize[2], 5)

    if ratio >= 0.8 then
        self.m_IsDragging = false
        self.Finger:SetActive(false)

        self.finishPrintPlankList[dragPlankIndex] = true
        self.TintText.text = g_MessageMgr:FormatMessage("NANDUFANHUA_YINTUO_NEXT_TIPS")
        self.TintText.transform:GetComponent(typeof(Animation)):Play("nandufanhuamukewnd_tinttext")

        local cnt = 0
        for i = 1, #self.middlePlankList do
            cnt = cnt + ((not self.finishPrintPlankList[i]) and 0 or 1)
        end
        if cnt >= 4 then
            EventManager.BroadcastInternalForLua(EnumEventType.OnLoadCommonUIFx, {CUIFxPaths.TaskFinishedFxPath})
            Gac2Gas.MuKeShuiYinFinish(3)
            CUIManager.CloseUI(CLuaUIResources.NanDuFanHuaMuKeWnd)
        end
        
        self.mukeDrawComp:Set2FinishState(self.maskTex, self.alphaTex, self.copyTex[dragPlankIndex],
                self.maskSizeList[dragPlankIndex][1], self.maskSizeList[dragPlankIndex][2])
    end
end

function LuaNanDuFanHuaMuKeWnd:CheckPaint2Paper(dragPlankIndex, screenPosition)
    if self.lastRecordScreenPosition == nil then
        self.lastRecordScreenPosition = screenPosition
        return
    end
    
    local worldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(Vector3(screenPosition.x, screenPosition.y, 0))
    local localPos = self.state3CheckTex.transform:InverseTransformPoint(worldPos)
    local uv = { math.max(0, math.min(localPos.x/self.state3CheckTexWidth+0.5, 1)),
        math.max(0, math.min(localPos.y/self.state3CheckTexHeight+0.5, 1))}
    local maskSize = self.maskSizeList[dragPlankIndex]
    local x = math.max(0, math.min(maskSize[1], math.floor(uv[1]*maskSize[1])))
    local y = math.max(0, math.min(maskSize[2], math.floor(uv[2]*maskSize[2])))

    local lastWorldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(Vector3(self.lastRecordScreenPosition.x, self.lastRecordScreenPosition.y, 0))
    local lastLocalPos = self.state3CheckTex.transform:InverseTransformPoint(lastWorldPos)
    local lastuv = { math.max(0, math.min(lastLocalPos.x/self.state3CheckTexWidth+0.5, 1)),
                 math.max(0, math.min(lastLocalPos.y/self.state3CheckTexHeight+0.5, 1))}
    local lastx = math.max(0, math.min(maskSize[1], math.floor(lastuv[1]*maskSize[1])))
    local lasty = math.max(0, math.min(maskSize[2], math.floor(lastuv[2]*maskSize[2])))
    
    local blurRange = 20
    self.mukeDrawComp:PaintColor(self.maskTex, self.alphaTex, self.copyTex[dragPlankIndex],
            Vector2(lastx, lasty), Vector2(x,y), 
            self.maskSizeList[dragPlankIndex][1], self.maskSizeList[dragPlankIndex][2],
            blurRange)
    self.lastRecordScreenPosition = screenPosition
end

function LuaNanDuFanHuaMuKeWnd:TryShineOnMiddlePlank(plankIndex, colorIndex)
    --如果colorIndex在待选颜色中,且未上色的话, 就打开对应MiddlePlank中的参数
    if plankIndex == 1 then
        --第一块板子没这事
        return
    end
    local color = self.finishColorPlankList[plankIndex]
    if self.state2CheckTex then
        if colorIndex == self.colorCfg[plankIndex][1] and color[1] == 0 then
            self.state2CheckTex.material:SetVector("_ShineMaskVector1", Vector4(1, 0, 0, 0 ))
        elseif colorIndex == self.colorCfg[plankIndex][2] and color[2] == 0 then
            self.state2CheckTex.material:SetVector("_ShineMaskVector1", Vector4(0, 1, 0, 0 ))
        elseif colorIndex == self.colorCfg[plankIndex][3] and color[3] == 0 then
            self.state2CheckTex.material:SetVector("_ShineMaskVector1", Vector4(0, 0, 1, 0 ))
        else
            self.state2CheckTex.material:SetVector("_ShineMaskVector1", Vector4(0, 0, 0, 0 ))
        end
    end
end


function LuaNanDuFanHuaMuKeWnd:CheckPickColorLegal(dragPlankIndex, screenPosition)
    local colorThrehold = 0.8
    if dragPlankIndex == 1 then
        --第一个板子特殊处理的
        if self.paintColorIndex == self.colorCfg[1][1] then
            self.finishColorPlankList[1][1] = 1
            self.finishColorPlankList[1][2] = 1
            self.finishColorPlankList[1][3] = 1

            self:FinishOneColorBlock(dragPlankIndex)
            self.leftPlankList[dragPlankIndex].transform:Find("Sculpture"):GetComponent(typeof(UITexture)).color = NGUIText.ParseColor24("2E1B0F", 0)
            self.middlePlankList[dragPlankIndex].transform:Find("Sculpture"):GetComponent(typeof(UITexture)).color = NGUIText.ParseColor24("2E1B0F", 0)
            return true
        end
        return false, "firstPlankWrongColor"
    end
    
    local worldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(Vector3(screenPosition.x, screenPosition.y, 0))
    local localPos = self.state2CheckTex.transform:InverseTransformPoint(worldPos)
    local diffX = localPos.x
    local diffY = localPos.y
    local uv = { math.max(0, math.min(diffX/self.state2CheckTexWidth+0.5, 1)),
                 math.max(0, math.min(diffY/self.state2CheckTexHeight+0.5, 1))}

    local color = self.finishColorPlankList[dragPlankIndex]
    local maskSize = self.maskSizeList[dragPlankIndex]
    
    --(uv[1]*maskSize[1],uv[2]*maskSize[2])为中心, 左5右5上5下5的扩大范围寻找
    for offsetX = -5, 5 do
        for offsetY = -5, 5 do
            local offsetCol = self.maskTex:GetPixel( math.max(0, math.min(self.state2CheckTexWidth, math.floor(uv[1]*maskSize[1]+offsetX))),
                   math.max(0, math.min(self.state2CheckTexHeight, math.floor(uv[2]*maskSize[2])+offsetY)))
            if offsetCol.r >= colorThrehold and self.paintColorIndex == self.colorCfg[dragPlankIndex][1] and color[1] == 0 then
                self.state2CheckTex.gameObject:SetActive(false)
                self.state2CheckTex.material:SetVector("_ColorMaskVector1", Vector4(1, color[2], color[3], 0 ))
                self.state2CheckTex.material:SetVector("_ShineMaskVector1", Vector4(0, 0, 0, 0 ))
                self.leftState2Tex.material:SetVector("_ColorMaskVector1", Vector4(1, color[2], color[3], 0 ))
                self.state2CheckTex.gameObject:SetActive(true)
                self.finishColorPlankList[dragPlankIndex][1] = 1
                self:FinishOneColorBlock(dragPlankIndex)
                return true
            elseif offsetCol.g >= colorThrehold and self.paintColorIndex == self.colorCfg[dragPlankIndex][2] and color[2] == 0 then
                self.state2CheckTex.gameObject:SetActive(false)
                self.state2CheckTex.material:SetVector("_ColorMaskVector1", Vector4(color[1], 1, color[3], 0 ))
                self.state2CheckTex.material:SetVector("_ShineMaskVector1", Vector4(0, 0, 0, 0 ))
                self.leftState2Tex.material:SetVector("_ColorMaskVector1", Vector4(color[1], 1, color[3], 0 ))
                self.state2CheckTex.gameObject:SetActive(true)
                self.finishColorPlankList[dragPlankIndex][2] = 1
                self:FinishOneColorBlock(dragPlankIndex)
                return true
            elseif offsetCol.b >= colorThrehold and self.paintColorIndex == self.colorCfg[dragPlankIndex][3] and color[3] == 0 then
                self.state2CheckTex.gameObject:SetActive(false)
                self.state2CheckTex.material:SetVector("_ColorMaskVector1", Vector4(color[1], color[2], 1, 0 ))
                self.state2CheckTex.material:SetVector("_ShineMaskVector1", Vector4(0, 0, 0, 0 ))
                self.leftState2Tex.material:SetVector("_ColorMaskVector1", Vector4(color[1], color[2], 1, 0 ))
                self.state2CheckTex.gameObject:SetActive(true)
                self.finishColorPlankList[dragPlankIndex][3] = 1
                self:FinishOneColorBlock(dragPlankIndex)
                return true
            end
        end
    end
    
    local col = self.maskTex:GetPixel(math.floor(uv[1]*maskSize[1]), math.floor(uv[2]*maskSize[2]))
    if col.r >= colorThrehold and self.paintColorIndex == self.colorCfg[dragPlankIndex][1]then
        if color[1] == 0 then
            self.state2CheckTex.gameObject:SetActive(false)
            self.state2CheckTex.material:SetVector("_ColorMaskVector1", Vector4(1, color[2], color[3], 0 ))
            self.state2CheckTex.material:SetVector("_ShineMaskVector1", Vector4(0, 0, 0, 0 ))
            self.leftState2Tex.material:SetVector("_ColorMaskVector1", Vector4(1, color[2], color[3], 0 ))
            self.state2CheckTex.gameObject:SetActive(true)
            self.finishColorPlankList[dragPlankIndex][1] = 1
            self:FinishOneColorBlock(dragPlankIndex)
            return true
        else
            return false, "paintAlready"
        end
    elseif col.g >= colorThrehold and self.paintColorIndex == self.colorCfg[dragPlankIndex][2] then
        if color[2] == 0 then
            self.state2CheckTex.gameObject:SetActive(false)
            self.state2CheckTex.material:SetVector("_ColorMaskVector1", Vector4(color[1], 1, color[3], 0 ))
            self.state2CheckTex.material:SetVector("_ShineMaskVector1", Vector4(0, 0, 0, 0 ))
            self.leftState2Tex.material:SetVector("_ColorMaskVector1", Vector4(color[1], 1, color[3], 0 ))
            self.state2CheckTex.gameObject:SetActive(true)
            self.finishColorPlankList[dragPlankIndex][2] = 1
            self:FinishOneColorBlock(dragPlankIndex)
            return true
        else
            return false, "paintAlready"
        end
    elseif col.b >= colorThrehold and self.paintColorIndex == self.colorCfg[dragPlankIndex][3] then
        if color[3] == 0 then
            self.state2CheckTex.gameObject:SetActive(false)
            self.state2CheckTex.material:SetVector("_ColorMaskVector1", Vector4(color[1], color[2], 1, 0 ))
            self.state2CheckTex.material:SetVector("_ShineMaskVector1", Vector4(0, 0, 0, 0 ))
            self.leftState2Tex.material:SetVector("_ColorMaskVector1", Vector4(color[1], color[2], 1, 0 ))
            self.state2CheckTex.gameObject:SetActive(true)
            self.finishColorPlankList[dragPlankIndex][3] = 1
            self:FinishOneColorBlock(dragPlankIndex)
            return true
        else
            return false, "paintAlready"
        end
    end

    if col.r < colorThrehold and col.g < colorThrehold and col.b < colorThrehold then
        return false, "emptyField"
    end
    
    return false
end

function LuaNanDuFanHuaMuKeWnd:FinishOneColorBlock(curPlankIndex)
    local ret = true
    local curBlockRet = true
    for i = 1, #self.finishColorPlankList do
        for j = 1, 3 do
            if self.finishColorPlankList[i][j] == 0 then
                ret = false

                if curPlankIndex == i then
                    curBlockRet = false
                end
            end
        end
    end
    if not CommonDefs.IsNull(self.middlePlankList[curPlankIndex]) then
        self.middlePlankList[curPlankIndex].transform:Find("YunRan").gameObject:SetActive(true)
        self.leftPlankList[curPlankIndex].transform:Find("YunRan").gameObject:SetActive(true)

    end
    if curBlockRet then
        self.TintText.text = g_MessageMgr:FormatMessage("NANDUFANHUA_SHANGSE_NEXT_TIPS")
        self.TintText.transform:GetComponent(typeof(Animation)):Play("nandufanhuamukewnd_tinttext")
    end
    if ret then
        EventManager.BroadcastInternalForLua(EnumEventType.OnLoadCommonUIFx, {CUIFxPaths.TaskFinishedFxPath})
        Gac2Gas.MuKeShuiYinFinish(2)
        CUIManager.CloseUI(CLuaUIResources.NanDuFanHuaMuKeWnd)
    end
end

function LuaNanDuFanHuaMuKeWnd:FinishOneGravePlank(plankIndex)
    self.finishPlankList[plankIndex] = true
    self.TintText.text = g_MessageMgr:FormatMessage("NANDUFANHUA_DIAOKE_NEXT_TIPS")
    self.TintText.transform:GetComponent(typeof(Animation)):Play("nandufanhuamukewnd_tinttext")
    local cnt = 0
    for i = 1, #self.middlePlankList do
        cnt = cnt + ((not self.finishPlankList[i]) and 0 or 1)
    end
    self:SetPlank2AfterGraveState(self.middlePlankList[plankIndex])
    self:SetPlank2AfterGraveState(self.leftPlankList[plankIndex])
    self.middlePlankList[plankIndex].transform:Find("Sculpture"):GetComponent(typeof(Animation)):Play("nandufanhuamukewnd_yuanzuo_in")
    self.middlePlankList[plankIndex].transform:Find("RouteTips").gameObject:SetActive(false)
    if cnt >= #self.middlePlankList then
        EventManager.BroadcastInternalForLua(EnumEventType.OnLoadCommonUIFx, {CUIFxPaths.TaskFinishedFxPath})
        Gac2Gas.MuKeShuiYinFinish(1)
        CUIManager.CloseUI(CLuaUIResources.NanDuFanHuaMuKeWnd)
    end
end

function LuaNanDuFanHuaMuKeWnd:CheckDragLegal(dragPlankIndex, dragDotIndex, screenPosition)
    local plankObj = self.middlePlankList[dragPlankIndex]
    local routeObj = plankObj.transform:Find("RouteTips")
    if dragDotIndex >= routeObj.childCount then
        --完成了
        self.m_IsDragging = false
        self.Finger:SetActive(false)
        self:FinishOneGravePlank(dragPlankIndex)
        return 1
    end
    --多增加一点容错性
    local checkPos = screenPosition
    local recordMinDistance = 0
    local recordMinDistanceOffset = -999
    for offset = -1, 2, 1 do
        local searchDotIndex = dragDotIndex-1 + offset
        if searchDotIndex >= 0 and searchDotIndex < routeObj.childCount then
            local searchDotObj = routeObj.transform:GetChild(searchDotIndex)
            local searchDotPos = CUIManager.UIMainCamera:WorldToScreenPoint(searchDotObj.position)
            searchDotPos.z = 0
            local searchDotDistance = Vector3.Distance(Vector3(checkPos.x, checkPos.y, 0), searchDotPos)
            if recordMinDistanceOffset == -999 then
                recordMinDistance = searchDotDistance
                recordMinDistanceOffset = offset
            else
                if searchDotDistance < recordMinDistance then
                    recordMinDistance = searchDotDistance
                    recordMinDistanceOffset = offset
                end
            end
        end
    end

    if recordMinDistance <= self.graveRange and recordMinDistance ~= 0 then
        return recordMinDistanceOffset + 1
    else
        self.m_IsDragging = false
        self.Finger:SetActive(false)
        return 0
    end
end

function LuaNanDuFanHuaMuKeWnd:SetDraftOrLeftPlankClick(plankOrDraftIndex)
    self.Example:SetActive(plankOrDraftIndex == 0)
    self.DraftTextBg:SetActive(plankOrDraftIndex ~= 0)
    self.DraftInUse:SetActive(plankOrDraftIndex == 0)
    for i = 1, #self.middlePlankList do
        self.middlePlankList[i]:SetActive(plankOrDraftIndex == i)
    end
    for i = 1, #self.leftPlankList do
        self.leftPlankList[i].transform:Find("InUse").gameObject:SetActive(plankOrDraftIndex == i)
        self.leftPlankList[i].transform:Find("DraftTextBg").gameObject:SetActive(plankOrDraftIndex ~= i)
    end

    if self.leftSelectPlankIndex ~= plankOrDraftIndex then
        local fadeInAniComp = nil
        if self.leftSelectPlankIndex == 0 then
            fadeInAniComp = self.Draft.transform:Find("vfx_nandufanhuamukewnd_yuanzuo"):GetComponent(typeof(Animation))
        else
            fadeInAniComp = self.leftPlankList[self.leftSelectPlankIndex].transform:GetComponent(typeof(Animation))
        end
        if fadeInAniComp then
            fadeInAniComp:Play(self.leftSelectPlankIndex == 0 and "nandufanhuamukewnd_yuanzuo_in" or "nandufanhuamukewnd_plank_in")
        end

        local fadeOutAniComp = nil
        if plankOrDraftIndex == 0 then
            fadeOutAniComp = self.Draft.transform:Find("vfx_nandufanhuamukewnd_yuanzuo"):GetComponent(typeof(Animation))
        else
            fadeOutAniComp = self.leftPlankList[plankOrDraftIndex].transform:GetComponent(typeof(Animation))
        end
        if fadeOutAniComp then
            fadeOutAniComp:Play(plankOrDraftIndex == 0 and "nandufanhuamukewnd_yuanzuo_out" or "nandufanhuamukewnd_plank_out")
        end
        self.leftSelectPlankIndex = plankOrDraftIndex
    end
end

--把obj这块木头(leftplank or middleplank 设置成初始状态
function LuaNanDuFanHuaMuKeWnd:SetPlank2OriginalState(obj)
    obj.transform:Find("YunRan").gameObject:SetActive(false)
    obj.transform:Find("Sculpture").gameObject:SetActive(false)
    
    local trans = obj.transform:Find("PaintSculpture")
    if trans then
        obj.transform:Find("PaintSculpture").gameObject:SetActive(false)
    end
end


--把obj这块木头(leftplank or middleplank 设置成雕刻后状态
function LuaNanDuFanHuaMuKeWnd:SetPlank2AfterGraveState(obj)
    obj.transform:Find("YunRan").gameObject:SetActive(false)
    obj.transform:Find("Sculpture").gameObject:SetActive(true)
    local trans = obj.transform:Find("PaintSculpture")
    if trans then
        trans:GetComponent(typeof(UITexture)).material:SetVector("_ColorMaskVector1", Vector4(0, 0, 0, 0))
        trans:GetComponent(typeof(UITexture)).material:SetVector("_ShineMaskVector1", Vector4(0, 0, 0, 0))
        trans.gameObject:SetActive(true)
    end
end

--把obj这块木头(leftplank or middleplank 设置成上色后状态
function LuaNanDuFanHuaMuKeWnd:SetPlank2AfterColorState(obj)
    obj.transform:Find("YunRan").gameObject:SetActive(true)
    obj.transform:Find("Sculpture").gameObject:SetActive(true)

    local trans = obj.transform:Find("PaintSculpture")
    if trans then
        trans:GetComponent(typeof(UITexture)).material:SetVector("_ColorMaskVector1", Vector4(1, 1, 1, 0))
        trans:GetComponent(typeof(UITexture)).material:SetVector("_ShineMaskVector1", Vector4(0, 0, 0, 0))
        trans.gameObject:SetActive(true)
    end
end

function LuaNanDuFanHuaMuKeWnd:OnDisable()
end 

function LuaNanDuFanHuaMuKeWnd:OnDestroy()
end 