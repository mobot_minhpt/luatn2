
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CRankData = import "L10.UI.CRankData"
local RankPlayerBaseInfo = import "L10.UI.RankPlayerBaseInfo"
local EnumRankSheet = import "L10.UI.EnumRankSheet"
local EnumClass = import "L10.Game.EnumClass"
local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local PlayerSettings = import "L10.Game.PlayerSettings"
local CSkillButtonBoardWnd = import "L10.UI.CSkillButtonBoardWnd"
local CCarnivalLotteryMgr = import "L10.Game.CCarnivalLotteryMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaQingMing2023Mgr = {}
---------------
-----主界面-----
---------------
function LuaQingMing2023Mgr:ShowQingMing2023MainWnd()
	LuaCommonJieRiMgr.jieRiId = 28
	CUIManager.ShowUI(CLuaUIResources.QingMing2023MainWnd)
end

-- 宝库之门ScheduleInfo（两段时间显示）
function LuaQingMing2023Mgr:ShowQingMing2023ScheduleInfo(scheduleId)
    local schedule = Task_Schedule.GetData(scheduleId)
    local timeStr = QingMing2023_Setting.GetData().BaoKuZhiMenOpenTime
    CLuaScheduleMgr.selectedScheduleInfo={
        activityId = scheduleId,
        taskId = schedule.TaskID[0],

        TotalTimes = 0,--不显示次数
        DailyTimes = 0,--不显示活力
        FinishedTimes = 0,
        
        ActivityTime = SafeStringFormat3( LocalString.GetString("%s, %s"), schedule.ExternTip, timeStr),
        ShowJoinBtn = false
    }
    CLuaScheduleInfoWnd.s_UseCustomInfo = true
    CUIManager.ShowUI(CLuaUIResources.ScheduleInfoWnd)
end

---------------
-----PVE-------
---------------
function LuaQingMing2023Mgr:OnMainPlayerCreated(playId)
    if playId == self:GetPVEGamePlayId() then
        LuaCommonGamePlayTaskViewMgr:AppendGameplayInfo(playId, 
        true, nil,
        false,
        true, function() return Gameplay_Gameplay.GetData(playId).Name end,
        true, function() return g_MessageMgr:FormatMessage("QingMing2023_Pve_TaskBar_Rule") end,
        true, nil, nil,
        true, nil, function() return g_MessageMgr:ShowMessage("QingMing2023_Pve_Rule") end)
    elseif playId == self:GetPVPGamePlayId() then
        -- 进入PVP玩法设置默认人数
        PlayerSettings.RSUpdateSetting("PlayerLimit", 20, 500,"qingming2023", false)
        CClientPlayerMgr.Inst:SetVisiblePlayerLimit(PlayerSettings.VisiblePlayerLimit)
    end
end

---获取PVE的GamePlayId
LuaQingMing2023Mgr.m_PVE_GamePlayId = nil
function LuaQingMing2023Mgr:GetPVEGamePlayId()
    if self.m_PVE_GamePlayId == nil then
        self.m_PVE_GamePlayId = QingMing2023_PVESetting.GetData().GameplayId
    end
    return self.m_PVE_GamePlayId
end

LuaQingMing2023Mgr.isPVESelectDifficult = nil
---PVE排行榜
function LuaQingMing2023Mgr:SendQingMing2023PveRank(myRank, finishTime, rankDataU)
    CommonDefs.ListClear(CRankData.Inst.RankList)
    local rankListData = MsgPackImpl.unpack(rankDataU)
    if CClientMainPlayer.Inst then
        CRankData.Inst.MainPlayerRankInfo = RankPlayerBaseInfo(0, 0, CClientMainPlayer.Inst.Name, "", 0, 0, CClientMainPlayer.Inst.Class, 0, 0, EnumRankSheet.eGuild, "", "", "", "", "", "", 0, 0, "", "", nil, nil, 0, "")
        CRankData.Inst.MainPlayerRankInfo.PlayerIndex = CClientMainPlayer.Inst.Id
        CRankData.Inst.MainPlayerRankInfo.Rank = myRank
        CRankData.Inst.MainPlayerRankInfo.Value = finishTime
    end
    for i = 0, rankListData.Count - 1, 4 do
        CommonDefs.ListAdd(CRankData.Inst.RankList, typeof(RankPlayerBaseInfo), RankPlayerBaseInfo(
            0, rankListData[i + 1], rankListData[i + 2], "", 0, rankListData[i], EnumClass.Undefined, 0, rankListData[i + 3], EnumRankSheet.eGuild, "", "", "", "", "", "", 0, 0, "", "", nil, nil, 0, ""))
    end

    LuaCommonRankWndMgr.m_BottomText = nil
    LuaCommonRankWndMgr.m_TitleText = LocalString.GetString("排行榜")
    LuaCommonRankWndMgr.m_ThirdHeadText = LocalString.GetString("通关时间")
    LuaCommonRankWndMgr.m_ValueProcessFunc = function (info)
        return CUICommonDef.SecondsToTimeString(info.Value, true)
    end
    CUIManager.ShowUI(CLuaUIResources.CommonRankWnd)
end

-- PVE结算
LuaQingMing2023Mgr.m_PVE_Result = nil
function LuaQingMing2023Mgr:QingMing2023PvePlaySuccess(bWin, mode, finishTime, myRank, minTime, award)
    self.m_PVE_Result = {}
    self.m_PVE_Result.isWin = bWin
    self.m_PVE_Result.isDifficult = mode == 2 and true or false
    self.m_PVE_Result.costTime = finishTime
    self.m_PVE_Result.bestTime = minTime
    self.m_PVE_Result.rank = myRank > 0 and tostring(myRank) or LocalString.GetString("未上榜")
    self.m_PVE_Result.isReward = award > 0
    CUIManager.ShowUI(CLuaUIResources.QingMing2023PVEResultWnd)
end

---------------
-----PVP-------
---------------
function LuaQingMing2023Mgr:OnMainPlayerDestroyed()
    -- 结束时清空本轮游戏数据，避免下一轮错误的信息提前出现
    LuaQingMing2023Mgr.m_PVP_CurrentLevelId = nil
    LuaQingMing2023Mgr.m_PVP_RankInfo = {}
    LuaQingMing2023Mgr.m_PVP_CurrentLevelData = {}
    -- 若过程中玩家未修改显示人数，则重新改回进入玩法时的设置
    PlayerSettings.RSUpdateSetting("PlayerLimit", 20, 500,"qingming2023", true)
    CClientPlayerMgr.Inst:SetVisiblePlayerLimit(PlayerSettings.VisiblePlayerLimit)
end

function LuaQingMing2023Mgr:AdjustCameraParam()
    local CameraView = QingMing2023_MapSetting.GetData(self.m_PVP_CurrentLevelId).CameraView
	local vec3 = Vector3(CameraView[0], CameraView[1], CameraView[2])
    CameraFollow.Inst.targetRZY = vec3
    CameraFollow.Inst.RZY = vec3
    CameraFollow.Inst.CurCameraParam:ApplyRZY(vec3)
    if CameraView[3] then CameraFollow.Inst:SetPinchIn(CameraView[3], true, true) end
end

---获取PVP的GamePlayId
LuaQingMing2023Mgr.m_PVP_GamePlayId = nil
function LuaQingMing2023Mgr:GetPVPGamePlayId()
    if self.m_PVP_GamePlayId == nil then
        self.m_PVP_GamePlayId = QingMing2023_PVPSetting.GetData().GamePlayId
    end
    return self.m_PVP_GamePlayId
end

---获取规则（图片、文本）列表
LuaQingMing2023Mgr.m_PVP_RuleList = nil
function LuaQingMing2023Mgr:GetRuleList()
    if self.m_PVP_RuleList == nil then
        self.m_PVP_RuleList = {} 
        QingMing2023_MapSetting.Foreach(function (key, data)
            local ruleInfo = {}
            ruleInfo.Name = data.Name
            ruleInfo.ImageRulePath = data.ImageRulePath
            ruleInfo.ImageRuleString = data.ImageRuleString
            ruleInfo.TaskViewRuleString = data.TaskViewRuleString
            ruleInfo.RuleBtnMsg = data.RuleBtnMsg
            self.m_PVP_RuleList[key] = ruleInfo
        end)
    end
end

---每关进入显示规则界面
LuaQingMing2023Mgr.m_PVP_CurrentLevelIndex = nil
LuaQingMing2023Mgr.m_PVP_CurrentLevelId = nil
function LuaQingMing2023Mgr:QingMing2023SyncLevelList(currentLevelIndex, currentLevelId, levelListU, showImageRule)
    self:GetRuleList()
    self.m_PVP_CurrentLevelIndex = currentLevelIndex
    self.m_PVP_CurrentLevelId = currentLevelId

    -- 除第五关外，隐藏技能栏
    if currentLevelId == 5 then
        CSkillButtonBoardWnd.Show(true)
    else
		CSkillButtonBoardWnd.Hide(true)
    end
    if currentLevelId == 0 then return end
    g_ScriptEvent:BroadcastInLua("QingMing2023SyncPlayRankInfo")
    LuaCommonGamePlayTaskViewMgr:OnUpdateTitleAndContent()
    self:AdjustCameraParam()
    if showImageRule then
        local imagePaths = {} 
        local msgs = {}
        table.insert(imagePaths, self.m_PVP_RuleList[self.m_PVP_CurrentLevelId].ImageRulePath)
        table.insert(msgs, self.m_PVP_RuleList[self.m_PVP_CurrentLevelId].ImageRuleString)
        LuaImageRuleMgr:ShowCommonImageRuleWnd(imagePaths, msgs)
    end
end

---PVP结算
LuaQingMing2023Mgr.m_PVP_Result = {}
function LuaQingMing2023Mgr:QingMing2023PVPSendFinalResult(rank, score, engageAward, winnerAward, playerDetailsU)
    self.m_PVP_Result.rank = rank
    self.m_PVP_Result.totalScore = score
    self.m_PVP_Result.winnerAward = winnerAward
    self.m_PVP_Result.engageAward = engageAward

    local subPlayInfo = {}
    local scoreData = g_MessagePack.unpack(playerDetailsU)
    for i, v in pairs(scoreData) do
        v.Name = self.m_PVP_RuleList[v.SubPlayId].Name
        subPlayInfo[i] = v
    end
    self.m_PVP_Result.subPlayInfo = subPlayInfo
    CUIManager.ShowUI(CLuaUIResources.QingMing2023PVPResultWnd)
end

---PVP当前排行
LuaQingMing2023Mgr.m_PVP_RankInfo = {}
LuaQingMing2023Mgr.m_PVP_MainPlayerRank = nil
function LuaQingMing2023Mgr:QingMing2023SyncPlayRankInfo(playerRankListU)
    self.m_PVP_RankInfo = g_MessagePack.unpack(playerRankListU)
    for i = 1, #self.m_PVP_RankInfo do
        if CClientMainPlayer.IsPlayerId(self.m_PVP_RankInfo[i].PlayerId) then
            self.m_PVP_MainPlayerRank = self.m_PVP_RankInfo[i]
        end
        -- 每轮游戏开局重置头顶血条，非血条游戏，小游戏内不更新
        local obj = CClientPlayerMgr.Inst:GetPlayer(self.m_PVP_RankInfo[i].PlayerId)
        if obj then 
            LuaGamePlayHpMgr:AddHpInfo(self.m_PVP_RankInfo[i].PlayerId, 100, 100, false, 1, false)
            EventManager.BroadcastInternalForLua(EnumEventType.RefreshHeadInfoVisible,{obj.EngineId})
        end
    end
    -- 每轮游戏开局重置头顶钥匙，除抢钥匙玩法外，小游戏内不更新
    EventManager.BroadcastInternalForLua(EnumEventType.UpdatePlayerHeadInfoSepIcon, {true})
    g_ScriptEvent:BroadcastInLua("QingMing2023SyncPlayRankInfo")
end

LuaQingMing2023Mgr.m_PVP_CurrentLevelData = {}
function LuaQingMing2023Mgr:QingMing2023SyncSubPlayInfo(subplayId, playerSubPlayInfoU)
    local preLevelData = LuaQingMing2023Mgr.m_PVP_CurrentLevelData
    self.m_PVP_CurrentLevelData = g_MessagePack.unpack(playerSubPlayInfoU)
    local updateIcon = false
    for playerId, data in pairs(self.m_PVP_CurrentLevelData) do
        if self:ShowHeadInfoGameplayHp() then
            LuaGamePlayHpMgr:AddHpInfo(playerId, data.Score, 100, false, 1, false)
        elseif self.m_PVP_CurrentLevelId == 4 then -- 首次或数据更改
            updateIcon = updateIcon or preLevelData[playerId] == nil or preLevelData[playerId].Score ~= data.Score
        end
    end
    if updateIcon then -- 抢钥匙玩法更新头顶钥匙图标
        EventManager.BroadcastInternalForLua(EnumEventType.UpdatePlayerHeadInfoSepIcon, {true})
    end
    g_ScriptEvent:BroadcastInLua("QingMing2023SyncSubPlayInfo")
end

function LuaQingMing2023Mgr:ShowHeadInfoGameplayHp()
    local notShowHp = {0, 3, 4, 6}
    for i = 1, #notShowHp do
        if LuaQingMing2023Mgr.m_PVP_CurrentLevelId == notShowHp[i] then return false end
    end
    return true
end
---任务栏
function LuaQingMing2023Mgr:GetTaskViewContent()
    self:GetRuleList()
    if self.m_PVP_CurrentLevelId and self.m_PVP_RuleList[self.m_PVP_CurrentLevelId] then
        return self.m_PVP_RuleList[self.m_PVP_CurrentLevelId].TaskViewRuleString
    end
    return g_MessageMgr:FormatMessage("QingMing2023_Main_TaskBar")
end
function LuaQingMing2023Mgr:GetTaskViewTitle()
    self:GetRuleList()
    if self.m_PVP_CurrentLevelId and self.m_PVP_RuleList[self.m_PVP_CurrentLevelId] then
        return self.m_PVP_RuleList[self.m_PVP_CurrentLevelId].Name
    end
    return Gameplay_Gameplay.GetData(self.m_PVP_GamePlayId).Name
end
function LuaQingMing2023Mgr:OnRuleButtonClick()
    if self.m_PVP_CurrentLevelId ~= nil and self.m_PVP_RuleList[self.m_PVP_CurrentLevelId] then
        return g_MessageMgr:ShowMessage(self.m_PVP_RuleList[self.m_PVP_CurrentLevelId].RuleBtnMsg)
    else
        return g_MessageMgr:ShowMessage("QingMing2023_Main_Rule")
    end
end

-- 头顶显示
function LuaQingMing2023Mgr:ShowPlayerHeadInfoKey(playerId)
    if self.m_PVP_CurrentLevelId == 4 and self.m_PVP_CurrentLevelData[playerId] and self.m_PVP_CurrentLevelData[playerId].Score ~= 0 then
        return 'UI/Texture/FestivalActivity/Festival_QingMing/QingMing2023/Material/qingming2023pvptoprightwnd_yaoshi.mat'
    end
end

function LuaQingMing2023Mgr:QingMing2023PvpRank(rankDataU)
    CommonDefs.ListClear(CRankData.Inst.RankList)
    local myId = CClientMainPlayer.Inst.Id
    local rankListData = MsgPackImpl.unpack(rankDataU)
    local myRank = 0
    local myValue = 0
    for i = 0, rankListData.Count - 1, 4 do
        if myId == rankListData[i + 1] then
            myRank = rankListData[i]
            myValue = rankListData[i + 3]
            break
        end
    end
    
    CRankData.Inst.MainPlayerRankInfo = RankPlayerBaseInfo(0, 0, CClientMainPlayer.Inst.Name, "", 0, 0, CClientMainPlayer.Inst.Class, 0, 0, EnumRankSheet.eGuild, "", "", "", "", "", "", 0, 0, "", "", nil, nil, 0, "")
    CRankData.Inst.MainPlayerRankInfo.PlayerIndex = CClientMainPlayer.Inst.Id
    CRankData.Inst.MainPlayerRankInfo.Rank = myRank
    CRankData.Inst.MainPlayerRankInfo.Value = myValue
    
    for i = 0, rankListData.Count - 1, 4 do
        CommonDefs.ListAdd(CRankData.Inst.RankList, typeof(RankPlayerBaseInfo), RankPlayerBaseInfo(
            0, rankListData[i + 1], rankListData[i + 2], "", 0, rankListData[i], EnumClass.Undefined, 0, rankListData[i + 3], EnumRankSheet.eGuild, "", "", "", "", "", "", 0, 0, "", "", nil, nil, 0, ""))
    end

    LuaCommonRankWndMgr.m_BottomText = nil
    LuaCommonRankWndMgr.m_TitleText = LocalString.GetString("排行榜")
    LuaCommonRankWndMgr.m_ThirdHeadText = LocalString.GetString("夺冠次数")
    LuaCommonRankWndMgr.m_ValueProcessFunc = nil
    CUIManager.ShowUI(CLuaUIResources.CommonRankWnd)
end

function LuaQingMing2023Mgr:QingMing2023PVPCheck()
	local win = CCarnivalLotteryMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eQingMing2023PVPWinnerAward)
	local engage = CCarnivalLotteryMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eQingMing2023PVPEngageAward)
	if win == 0 or engage == 0 then
    	local now = CServerTimeMgr.Inst:GetZone8Time()
   		return (now.Hour >= 14 and now.Hour < 16) or (now.Hour >= 21 and now.Hour < 23)
	end
	return false
end