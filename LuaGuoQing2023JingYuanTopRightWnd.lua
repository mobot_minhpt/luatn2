local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CUITexture = import "L10.UI.CUITexture"

LuaGuoQing2023JingYuanTopRightWnd = class()

RegistClassMember(LuaGuoQing2023JingYuanTopRightWnd, "m_rightBtn")
RegistClassMember(LuaGuoQing2023JingYuanTopRightWnd, "m_TeamInfo")
RegistClassMember(LuaGuoQing2023JingYuanTopRightWnd, "m_rightBtn")
RegistClassMember(LuaGuoQing2023JingYuanTopRightWnd, "m_TeamInfo")
RegistClassMember(LuaGuoQing2023JingYuanTopRightWnd, "m_BgSprite_1")
RegistClassMember(LuaGuoQing2023JingYuanTopRightWnd, "m_LoudSpeak")
RegistClassMember(LuaGuoQing2023JingYuanTopRightWnd, "m_LoudSpeakIcon")
RegistClassMember(LuaGuoQing2023JingYuanTopRightWnd, "m_LoudSpeakBg1")
RegistClassMember(LuaGuoQing2023JingYuanTopRightWnd, "m_LoudSpeakBg2")
RegistClassMember(LuaGuoQing2023JingYuanTopRightWnd, "m_LoudSpeakLabel")

RegistClassMember(LuaGuoQing2023JingYuanTopRightWnd, "m_MsgList")
RegistClassMember(LuaGuoQing2023JingYuanTopRightWnd, "m_MsgTick")
RegistClassMember(LuaGuoQing2023JingYuanTopRightWnd, "m_MsgTime")
RegistClassMember(LuaGuoQing2023JingYuanTopRightWnd, "m_MsgTimeMin")
RegistClassMember(LuaGuoQing2023JingYuanTopRightWnd, "m_MsgTimeMax")


function LuaGuoQing2023JingYuanTopRightWnd:Awake()
    self.m_rightBtn = self.transform:Find("Anchor/Tip/rightBtn").gameObject
    self.m_LoudSpeak = self.transform:Find("Anchor/LoudSpeak").gameObject
    self.m_LoudSpeakIcon = self.transform:Find("Anchor/LoudSpeak/LoudSpeakIcon"):GetComponent(typeof(CUITexture))
    self.m_LoudSpeakBg1 = self.transform:Find("Anchor/LoudSpeak/BgSprite_1").gameObject
    self.m_LoudSpeakBg2 = self.transform:Find("Anchor/LoudSpeak/BgSprite_2").gameObject
    self.m_LoudSpeakLabel = self.transform:Find("Anchor/LoudSpeak/LoudSpeakLabel"):GetComponent(typeof(UILabel))
    UIEventListener.Get(self.m_rightBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnRightBtnClick()
    end)
end

function LuaGuoQing2023JingYuanTopRightWnd:Init()
    self.m_MsgList = {}
    self.m_WinJingYuanCount = GuoQing2023_JingYuanPlay.GetData().JingYuanNumForCountdown
    local msgShowTime = GuoQing2023_JingYuanPlay.GetData().ShowMsgDuringTime
    self.m_MsgTimeMin = msgShowTime[0]
    self.m_MsgTimeMax = msgShowTime[1]
    self.m_LoudSpeak:SetActive(false)

    self.m_TeamInfo = {}
    for i = 1, 2 do
        local teamNode = self.transform:Find("Anchor/tipPanel/Table/Team" .. tostring(i))
        local teamInfo = {}
        teamInfo.CountLabel = teamNode:Find("CountLabel"):GetComponent(typeof(UILabel))
        teamInfo.CountSlider = teamNode:Find("CountSlider"):GetComponent(typeof(UISlider))
        table.insert(self.m_TeamInfo, teamInfo)
    end
    self:OnSyncArenaTeamJingYuanInfo()
end

function LuaGuoQing2023JingYuanTopRightWnd:OnSyncArenaTeamJingYuanInfo()
    local infoTbl = LuaGuoQing2023Mgr.m_JingYuanPlay_GameInfo and LuaGuoQing2023Mgr.m_JingYuanPlay_GameInfo.teamInfo or {}
    for i = 1, 2 do
        local teamInfo = self.m_TeamInfo[i]
        local jingyuanCount = infoTbl and infoTbl[i - 1] and infoTbl[i - 1] or 0
        teamInfo.CountLabel.text = tostring(jingyuanCount)
        teamInfo.CountSlider.value = jingyuanCount / self.m_WinJingYuanCount
    end
end

function LuaGuoQing2023JingYuanTopRightWnd:OnGuoQing2023ShowMessage(force, msg)
    table.insert(self.m_MsgList, {force = force, msg = msg})
    self:UpdateMsg()
end

function LuaGuoQing2023JingYuanTopRightWnd:UpdateMsg()
    local minTimeLimit = self.m_MsgTime == nil or self.m_MsgTime >= self.m_MsgTimeMin    -- 上一条消息以达到最短播放时长
    local maxTimeLimit = self.m_MsgTime == nil or self.m_MsgTime >= self.m_MsgTimeMax    -- 上一条消息以达到最长播放时长
    local newMsg = #self.m_MsgList > 0    -- 队列中有新消息待播
    if minTimeLimit and newMsg then
        -- 更新新的消息
        self:InitMsgInfo()
        -- 开始新的Tick
        if self.m_MsgTick then UnRegisterTick(self.m_MsgTick) end
        self.m_MsgTick = RegisterTick(function ()
            self.m_MsgTime = self.m_MsgTime + 1
            self:UpdateMsg()
        end, 1000)
    elseif maxTimeLimit and not newMsg then
        -- 播放完毕，关闭tick
        if self.m_MsgTick then UnRegisterTick(self.m_MsgTick) end
        self.m_LoudSpeak:SetActive(false)
    end
end

function LuaGuoQing2023JingYuanTopRightWnd:InitMsgInfo()
    local msgInfo = table.remove(self.m_MsgList, 1)
    local iconPath = {
        'UI/Texture/FestivalActivity/Festival_GuoQing/GuoQing2023/Material/guoqing2023jingyuanresultwnd_zhuyan_small.mat',
        'UI/Texture/FestivalActivity/Festival_GuoQing/GuoQing2023/Material/guoqing2023jingyuanresultwnd_cangming_small.mat',
    }
    self.m_LoudSpeakLabel.text = msgInfo.msg
    self.m_LoudSpeakIcon:LoadMaterial(iconPath[msgInfo.force + 1])
    self.m_LoudSpeakIcon.texture:ResetAndUpdateAnchors()
    self.m_LoudSpeakBg1:SetActive(msgInfo.force == 0)
    self.m_LoudSpeakBg2:SetActive(msgInfo.force == 1)
    self.m_LoudSpeak:SetActive(true)
    self.m_MsgTime = 0
end

--@region UIEvent

function LuaGuoQing2023JingYuanTopRightWnd:OnRightBtnClick()
    self.m_rightBtn.transform.localEulerAngles = Vector3(0, 0, 0)
	CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end

function LuaGuoQing2023JingYuanTopRightWnd:OnHideTopAndRightTipWnd()
    self.m_rightBtn.transform.localEulerAngles = Vector3(0, 0, -180)
end

--@endregion UIEvent

function LuaGuoQing2023JingYuanTopRightWnd:OnEnable()
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd",self,"OnHideTopAndRightTipWnd")
    g_ScriptEvent:AddListener("SyncArenaTeamJingYuanInfo",self,"OnSyncArenaTeamJingYuanInfo")
    g_ScriptEvent:AddListener("GuoQing2023ShowMessage",self,"OnGuoQing2023ShowMessage")
end

function LuaGuoQing2023JingYuanTopRightWnd:OnDisable()
    if self.m_MsgTick then UnRegisterTick(self.m_MsgTick) end
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd",self,"OnHideTopAndRightTipWnd")
    g_ScriptEvent:RemoveListener("SyncArenaTeamJingYuanInfo",self,"OnSyncArenaTeamJingYuanInfo")
    g_ScriptEvent:RemoveListener("GuoQing2023ShowMessage",self,"OnGuoQing2023ShowMessage")
end