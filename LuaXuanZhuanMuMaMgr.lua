local EnumActionState=import "L10.Game.EnumActionState"
local CClientObjectMgr=import "L10.Game.CClientObjectMgr"
local CClientNpc=import "L10.Game.CClientNpc"
CLuaXuanZhuanMuMaMgr={}
CLuaXuanZhuanMuMaMgr.m_PlayerInfos={}
CLuaXuanZhuanMuMaMgr.m_MuMaEngineId = 0

function CLuaXuanZhuanMuMaMgr:OnMainPlayerCreated(playId)
    CLuaXuanZhuanMuMaMgr.SyncCarouselPassengers()
end
function CLuaXuanZhuanMuMaMgr:OnMainPlayerDestroyed()
    CLuaXuanZhuanMuMaMgr.m_PlayerInfos={}
    CLuaXuanZhuanMuMaMgr.m_MuMaEngineId = 0
end

function CLuaXuanZhuanMuMaMgr.StartRotateCarousel()
    local obj=CClientObjectMgr.Inst:GetObject(CLuaXuanZhuanMuMaMgr.m_MuMaEngineId)
    if obj then
        obj.RO:DoAni("walk01",true,0,1,0.15,true,1)

        -- local target = obj.RO.transform
        for k,v in pairs(CLuaXuanZhuanMuMaMgr.m_PlayerInfos) do
            CLuaXuanZhuanMuMaMgr.BindPlayer(k,v,obj)
        end
    end
end

function CLuaXuanZhuanMuMaMgr.StopRotateCarousel()
    local obj=CClientObjectMgr.Inst:GetObject(CLuaXuanZhuanMuMaMgr.m_MuMaEngineId)
    if obj then
        obj.RO:DoAni("stand01",true,0,1,0,true,1)

        for k,v in pairs(CLuaXuanZhuanMuMaMgr.m_PlayerInfos) do
            local player=CClientObjectMgr.Inst:GetObject(k)
            if player then
                player:ShowActionState(EnumActionState.Idle,nil)
                player.RO.Position = player.WorldPos
                -- player.RO:GetOffVehicle()
                obj.RO:DetachRO(player.RO,0,0,0,0,0,0,1)
    
                -- player.RO:DoAni("stand01",false,0,1,0.15,true,1)
            end
        end
    end


    CLuaXuanZhuanMuMaMgr.m_PlayerInfos={}
end

function CLuaXuanZhuanMuMaMgr.SyncCarouselPassengers()
    local obj=CClientObjectMgr.Inst:GetObject(CLuaXuanZhuanMuMaMgr.m_MuMaEngineId)
    if obj then
        for k,v in pairs(CLuaXuanZhuanMuMaMgr.m_PlayerInfos) do
            CLuaXuanZhuanMuMaMgr.BindPlayer(k,v,obj)
        end
    end
end
function CLuaXuanZhuanMuMaMgr.TryBindPlayer(engineId)
    local pos =CLuaXuanZhuanMuMaMgr.m_PlayerInfos[engineId]
    if pos and pos>0 then
        local obj=CClientObjectMgr.Inst:GetObject(CLuaXuanZhuanMuMaMgr.m_MuMaEngineId)
        if obj then
            CLuaXuanZhuanMuMaMgr.BindPlayer(engineId,pos,obj)
        end
    end
end
function CLuaXuanZhuanMuMaMgr.UnBindPlayer(engineId)
    local player=CClientObjectMgr.Inst:GetObject(engineId)
    local obj=CClientObjectMgr.Inst:GetObject(CLuaXuanZhuanMuMaMgr.m_MuMaEngineId)
    if player then
        player.RO.Position = player.WorldPos
        obj.RO:DetachRO(player.RO,0,0,0,0,0,0,1)
    end
end
function CLuaXuanZhuanMuMaMgr.BindPlayer(engineId,pos,obj)
    local player=CClientObjectMgr.Inst:GetObject(engineId)
    if player then
        player:ShowActionState(EnumActionState.Idle,nil)
        local slotName = SafeStringFormat3("ride%02d",pos)
        obj.RO:AttachRO(player.RO,slotName,0,0,0,0,0,0,1)
        if pos%2==0 then--骑在马上
            -- player.RO:DoAni("ride01",false,0,1,0.15,true,1)
            player:ShowExpressionActionState(47000041)
        else--座位上
            -- player.RO:DoAni("sitdown01",false,0,1,0.15,true,1)
            player:ShowExpressionActionState(47000036)
        end

        if TypeAs(player,typeof(CClientNpc)) then
            Gac2Gas.RequestBabyDoExpression(engineId,pos%2==0 and 47000041 or 47000036, 0)
        end
    end
end
