local DelegateFactory = import "DelegateFactory"
local UILabel         = import "UILabel"
local CServerTimeMgr  = import "L10.Game.CServerTimeMgr"
local CGamePlayMgr    = import "L10.Game.CGamePlayMgr"
local CScene          = import "L10.Game.CScene"

LuaWeddingView = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaWeddingView, "base")
RegistClassMember(LuaWeddingView, "groomName")
RegistClassMember(LuaWeddingView, "brideName")
RegistClassMember(LuaWeddingView, "guestNum")
RegistClassMember(LuaWeddingView, "time")
RegistClassMember(LuaWeddingView, "leaveButton")
RegistClassMember(LuaWeddingView, "pairName")
RegistClassMember(LuaWeddingView, "taskName")
RegistClassMember(LuaWeddingView, "tick")

RegistClassMember(LuaWeddingView, "maxGuestCount")

function LuaWeddingView:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

	self:InitUIComponents()
	self:InitEventListener()
end

-- 初始化UI组件
function LuaWeddingView:InitUIComponents()
	self.taskName = self.transform:Find("TaskName"):GetComponent(typeof(UILabel))

	self.base = self.transform:Find("Base")
	self.groomName = self.base:Find("GroomName"):GetComponent(typeof(UILabel))
	self.brideName = self.base:Find("BrideName"):GetComponent(typeof(UILabel))
	self.guestNum = self.base:Find("GuestNum"):GetComponent(typeof(UILabel))
	self.time = self.transform:Find("Time"):GetComponent(typeof(UILabel))
	self.leaveButton = self.transform:Find("LeaveBtn"):GetComponent(typeof(UIWidget))
	self.pairName = self.transform:Find("Pair"):GetComponent(typeof(UILabel))
end

-- 初始化点击响应
function LuaWeddingView:InitEventListener()
	UIEventListener.Get(self.leaveButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLeaveButtonClick()
	end)
end


function LuaWeddingView:OnEnable()
	g_ScriptEvent:AddListener("SyncNewWeddingSceneInfo", self, "OnSyncSceneInfo")
	g_ScriptEvent:AddListener("SyncNewWeddingStageInfo", self, "OnSyncStageInfo")
	g_ScriptEvent:AddListener("SceneRemainTimeUpdate", self, "OnRemainTimeUpdate")
	g_ScriptEvent:AddListener("NewWeddingSyncYiXianQianSelfMatchInfo", self, "OnSyncYiXianQianSelfMatchInfo")
	g_ScriptEvent:AddListener("NewWeddingSyncGuestCount", self, "OnSyncGuestCount")
end

function LuaWeddingView:OnDisable()
	g_ScriptEvent:RemoveListener("SyncNewWeddingSceneInfo", self, "OnSyncSceneInfo")
	g_ScriptEvent:RemoveListener("SyncNewWeddingStageInfo", self, "OnSyncStageInfo")
	g_ScriptEvent:RemoveListener("SceneRemainTimeUpdate", self, "OnRemainTimeUpdate")
	g_ScriptEvent:RemoveListener("NewWeddingSyncYiXianQianSelfMatchInfo", self, "OnSyncYiXianQianSelfMatchInfo")
	g_ScriptEvent:RemoveListener("NewWeddingSyncGuestCount", self, "OnSyncGuestCount")

	self:ClearTick()
end

function LuaWeddingView:OnSyncSceneInfo()
	self:UpdateDisplay(false, true)
end

function LuaWeddingView:OnSyncStageInfo()
	self:UpdateDisplay(false, false)
end

function LuaWeddingView:OnRemainTimeUpdate(args)
	if self.pairName.gameObject.active then return end
    self:SetRemainTime()
end

function LuaWeddingView:OnSyncYiXianQianSelfMatchInfo()
	self:UpdateDisplay(true, false)
end

function LuaWeddingView:OnSyncGuestCount()
	self:UpdateDisplay(false, true)
end


function LuaWeddingView:Init()
	self.maxGuestCount = WeddingIteration_Setting.GetData().MaxGuestCount

	self:SetActive(false, false)
	self:UpdateDisplay(true, true)
	self:SetRemainTime()
end

-- 切换显示配对或新郎新娘姓名
function LuaWeddingView:UpdateDisplay(needUpdatePairInfo, needUpdateSceneInfo)
	local stage = LuaWeddingIterationMgr.stage
	local stageInfo = LuaWeddingIterationMgr.stageInfo
	local pairInfo = LuaWeddingIterationMgr.pairInfo

	if stage == 5 and stageInfo[1] and stageInfo[1] == "Started" and pairInfo then
		-- 显示配对信息
		if needUpdatePairInfo or not self.pairName.gameObject.active then
			self:UpdatePairInfo()
		end
		self:SetActive(false, true)
		self:StartPairTick()
	else
		-- 显示场景信息
		if needUpdateSceneInfo or not self.base.gameObject.active then
			self:UpdateSceneInfo()
		end
		self:SetActive(true, false)
	end
end

-- 设置active
function LuaWeddingView:SetActive(baseActive, pairActive)
	self.base.gameObject:SetActive(baseActive)
	self.pairName.gameObject:SetActive(pairActive)
end

-- 更新场景信息
function LuaWeddingView:UpdateSceneInfo()
	self.taskName.text = LocalString.GetString("三生湖")

	local isGroomOrBride = LuaWeddingIterationMgr:IsGroomOrBride()
	if isGroomOrBride == nil then
		self.groomName.text = ""
		self.brideName.text = ""
		return
	end

	local sceneInfo = LuaWeddingIterationMgr.sceneInfo
	self.groomName.text = sceneInfo.groomName
	self.brideName.text = sceneInfo.brideName

	if isGroomOrBride then
		local guestCount = sceneInfo.guestCount
		if guestCount < self.maxGuestCount then
			self.guestNum.text = SafeStringFormat3(LocalString.GetString("宾客数 [00FF00]%d[-]"), guestCount)
		else
			self.guestNum.text = SafeStringFormat3(LocalString.GetString("宾客数 [FF0000]%d(已满)[-]"), self.maxGuestCount)
		end
		self.guestNum.gameObject:SetActive(true)
		self:ResetAnchor(350)
	else
		self.guestNum.gameObject:SetActive(false)
		self:ResetAnchor(300)
	end
end

-- 剩余时间
function LuaWeddingView:SetRemainTime()
    local mainScene = CScene.MainScene
    if mainScene then
        if mainScene.ShowTimeCountDown then
            self.time.text = self:GetRemainTimeText(mainScene.ShowTime)
        else
            self.time.text = ""
        end
    else
        self.time.text = ""
    end
end

function LuaWeddingView:GetRemainTimeText(totalSeconds)
    if totalSeconds < 0 then
        totalSeconds = 0
    end
    if totalSeconds >= 3600 then
        return cs_string.Format(LocalString.GetString("[ACF9FF]{0:00}:{1:00}:{2:00}[-]"),
         math.floor(totalSeconds / 3600),
         math.floor((totalSeconds % 3600) / 60),
         totalSeconds % 60)
    else
        return cs_string.Format(LocalString.GetString("[ACF9FF]{0:00}:{1:00}[-]"), math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end


-- 更新配对信息
function LuaWeddingView:UpdatePairInfo()
	local pairInfo = LuaWeddingIterationMgr.pairInfo

	self.taskName.text = SafeStringFormat3(LocalString.GetString("姻缘一线牵 第%d/4场"), pairInfo.round)
	if pairInfo.bSigned and not pairInfo.bCutOff then
		if pairInfo.targetId == 0 then
			self.pairName.text = LocalString.GetString("本场轮空")
		else
			self.pairName.text = SafeStringFormat3(LocalString.GetString("[79C2FF]配对:[-] %s"), pairInfo.targetName)
		end
		self:ResetAnchor(175)
	else
		self.pairName.text = ""
		self:ResetAnchor(115)
	end
end

-- 以背景为基准对倒计时和离开按钮重新布置
function LuaWeddingView:ResetAnchor(height)
	self.transform:GetComponent(typeof(UISprite)).height = height

	self.time:ResetAndUpdateAnchors()
	self.leaveButton:ResetAndUpdateAnchors()
end

-- 开始一线牵计时
function LuaWeddingView:StartPairTick()
	self:ClearTick()
	self:UpdatePairTime()
	self.tick = RegisterTick(function()
		self:UpdatePairTime()
	end, 1000)
end

-- 更新一线牵时间显示
function LuaWeddingView:UpdatePairTime()
	local pairInfo = LuaWeddingIterationMgr.pairInfo
	if not pairInfo then
		self:ClearTick()
		return
	end

	local leftTime = math.floor(pairInfo.endTimeStamp - CServerTimeMgr.Inst.timeStamp)
	self.time.text = self:GetRemainTimeText(leftTime)
	if leftTime <= 0 then
		self:ClearTick()
	end
end

-- 清除计时器
function LuaWeddingView:ClearTick()
	if self.tick then
		UnRegisterTick(self.tick)
		self.tick = nil
	end
end

--@region UIEvent

function LuaWeddingView:OnLeaveButtonClick()
	local isGroomOrBride = LuaWeddingIterationMgr:IsGroomOrBride()

	if isGroomOrBride == nil then return end

	if isGroomOrBride then
		local message = g_MessageMgr:FormatMessage("WEDDING_COUPLE_LEAVE_SCENE_CONFIRM")
		MessageWndManager.ShowDelayOKCancelMessage(message, DelegateFactory.Action(function ()
			Gac2Gas.RequestLeavePlay()
		end), nil, 5)
	else
		CGamePlayMgr.Inst:LeavePlay()
	end
end

--@endregion UIEvent

