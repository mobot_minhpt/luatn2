local DelegateFactory        = import "DelegateFactory"
local QnModelPreviewer       = import "L10.UI.QnModelPreviewer"
local ClientAction           = import "L10.UI.ClientAction"
local CScheduleMgr           = import "L10.Game.CScheduleMgr"
local TweenScale             = import "TweenScale"
local TweenAlpha             = import "TweenAlpha"
local SkeletonAnimation      = import "Spine.Unity.SkeletonAnimation"
local Animator               = import "UnityEngine.Animator"
local CPropertyAppearance    = import "L10.Game.CPropertyAppearance"
local CPropertySkill         = import "L10.Game.CPropertySkill"
local EWeaponVisibleReason   = import "L10.Engine.EWeaponVisibleReason"
local EHideKuiLeiStatus      = import "L10.Engine.EHideKuiLeiStatus"
local CQnReturnAwardTemplate = import "L10.UI.CQnReturnAwardTemplate"
local Item_Item              = import "L10.Game.Item_Item"
local CScene                 = import "L10.Game.CScene"
local CUIFxPaths             = import "L10.UI.CUIFxPaths"
local CButton                = import "L10.UI.CButton"
local EnumGender             = import "L10.Game.EnumGender"
local UITableTween           = import "L10.UI.UITableTween"

LuaQianYingChuWenWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaQianYingChuWenWnd, "normalWnd")
RegistClassMember(LuaQianYingChuWenWnd, "taskTemplate")
RegistClassMember(LuaQianYingChuWenWnd, "taskScrollView")
RegistClassMember(LuaQianYingChuWenWnd, "taskGrid")
RegistClassMember(LuaQianYingChuWenWnd, "taskTableTween")
RegistClassMember(LuaQianYingChuWenWnd, "tagTemplate")
RegistClassMember(LuaQianYingChuWenWnd, "tagGrid")
RegistClassMember(LuaQianYingChuWenWnd, "bottomTip")
RegistClassMember(LuaQianYingChuWenWnd, "modelPreviewer")
RegistClassMember(LuaQianYingChuWenWnd, "aChuModel")
RegistClassMember(LuaQianYingChuWenWnd, "aChuWord")
RegistClassMember(LuaQianYingChuWenWnd, "aChuWidget")
RegistClassMember(LuaQianYingChuWenWnd, "aChuBubble")
RegistClassMember(LuaQianYingChuWenWnd, "aChuAnim")
RegistClassMember(LuaQianYingChuWenWnd, "bubbleAppearTweenScale")
RegistClassMember(LuaQianYingChuWenWnd, "bubbleDisappearTweenScale")
RegistClassMember(LuaQianYingChuWenWnd, "bubbleDisappearTweenAlpha")
RegistClassMember(LuaQianYingChuWenWnd, "finishWnd")
RegistClassMember(LuaQianYingChuWenWnd, "clickTip")
RegistClassMember(LuaQianYingChuWenWnd, "awardIcon")
RegistClassMember(LuaQianYingChuWenWnd, "closeButton")
RegistClassMember(LuaQianYingChuWenWnd, "butterflies")
RegistClassMember(LuaQianYingChuWenWnd, "mainAnimator")
RegistClassMember(LuaQianYingChuWenWnd, "flyAnimator")
RegistClassMember(LuaQianYingChuWenWnd, "oneKeyButton")

RegistClassMember(LuaQianYingChuWenWnd, "bubbleTick")
RegistClassMember(LuaQianYingChuWenWnd, "curSelectTagId") -- 当前选择的标签id, 1-7
RegistClassMember(LuaQianYingChuWenWnd, "clickTick") -- 点击气泡计时
RegistClassMember(LuaQianYingChuWenWnd, "closeTick") -- 关闭界面倒计时
RegistClassMember(LuaQianYingChuWenWnd, "flyTick") -- 蝴蝶飞倒计时
RegistClassMember(LuaQianYingChuWenWnd, "name2Time") -- 动画时间
RegistClassMember(LuaQianYingChuWenWnd, "bubbleAppearTick") -- 气泡出现倒计时

RegistClassMember(LuaQianYingChuWenWnd, "specialAwardTaskIdDict")
RegistClassMember(LuaQianYingChuWenWnd, "day2AwardIcon")

function LuaQianYingChuWenWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self:InitUIComponents()
    self:InitEventListener()
    self:InitActive()
end

function LuaQianYingChuWenWnd:InitUIComponents()
    self.normalWnd = self.transform:Find("Normal")
    self.taskTemplate = self.normalWnd:Find("Task/TaskTemplate").gameObject
    self.taskScrollView = self.normalWnd:Find("Task/ScrollView"):GetComponent(typeof(UIScrollView))
    self.taskGrid = self.normalWnd:Find("Task/ScrollView/Widget/Grid"):GetComponent(typeof(UIGrid))
    self.taskTableTween = self.taskGrid.transform:GetComponent(typeof(UITableTween))
    self.tagTemplate = self.normalWnd:Find("Task/Tag/TagTemplate").gameObject
    self.tagGrid = self.normalWnd:Find("Task/Tag/Widget/Grid"):GetComponent(typeof(UIGrid))
    self.bottomTip = self.normalWnd:Find("Task/Tip"):GetComponent(typeof(UILabel))
    self.bottomTip2 = self.normalWnd:Find("Task/Tip/Tip2"):GetComponent(typeof(UILabel))
    self.modelPreviewer = self.normalWnd:Find("ModelPreviewer"):GetComponent(typeof(QnModelPreviewer))
    self.aChuModel = self.normalWnd:Find("AChu/AChu"):GetComponent(typeof(CUIFx))
    self.aChuWidget = self.normalWnd:Find("AChu/Widget"):GetComponent(typeof(UIWidget))
    self.aChuWord = self.normalWnd:Find("AChu/Widget/Word"):GetComponent(typeof(UILabel))
    self.aChuBubble = self.normalWnd:Find("AChu/Widget/Word/Bubble"):GetComponent(typeof(UIWidget))
    self.closeButton = self.normalWnd:Find("CloseButton").gameObject
    self.mainAnimator = self.transform:GetComponent(typeof(Animator))
    self.flyAnimator = self.transform:Find("HuDieAnimator"):GetComponent(typeof(Animator))
    self.oneKeyButton = self.normalWnd:Find("OneKeyButton"):GetComponent(typeof(CButton))

    self.bubbleAppearTweenScale = self.aChuWord.transform:GetComponent(typeof(TweenScale))
    self.bubbleDisappearTweenScale = self.aChuWidget.transform:GetComponent(typeof(TweenScale))
    self.bubbleDisappearTweenAlpha = self.aChuWidget.transform:GetComponent(typeof(TweenAlpha))

    self.finishWnd = self.transform:Find("Finish")
    self.clickTip = self.finishWnd:Find("Tip"):GetComponent(typeof(UILabel))
    self.awardIcon = self.finishWnd:Find("Icon"):GetComponent(typeof(CUITexture))

    self.butterflies = {}
    local parent = self.transform:Find("UI_hudiezong")
    for i = 1, 7 do
        self.butterflies[i] = parent:Find(SafeStringFormat3("UI_hudie0%d", i)).gameObject
    end
end

function LuaQianYingChuWenWnd:InitEventListener()
    UIEventListener.Get(self.aChuModel.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnAChuClick()
	end)

    UIEventListener.Get(self.aChuBubble.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnAChuBubbleClick()
	end)

    UIEventListener.Get(self.closeButton).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnCloseButtonClick()
	end)

    UIEventListener.Get(self.oneKeyButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnOneKeyButtonClick()
	end)
end

function LuaQianYingChuWenWnd:InitActive()
    self.taskTemplate:SetActive(false)
    self.tagTemplate:SetActive(false)
    self.aChuWidget.alpha = 0
end

function LuaQianYingChuWenWnd:OnEnable()
    if not CClientMainPlayer.Inst then
        CUIManager.CloseUI(CLuaUIResources.QianYingChuWenWnd)
        return
    end

    g_ScriptEvent:AddListener("SyncBeginnerGuideInfo", self, "OnSyncBeginnerGuideInfo")
    g_ScriptEvent:AddListener("RequestFixBeginnerGuideDay2FinishSuccess", self, "OnRequestFixBeginnerGuideDay2FinishSuccess")
    g_ScriptEvent:AddListener("RequestBeginnerGuideTaskRewardSuccess", self, "OnRequestBeginnerGuideTaskRewardSuccess")
    g_ScriptEvent:AddListener("RequestBeginnerGuideBatchTaskRewardSuccess", self, "OnRequestBeginnerGuideBatchTaskRewardSuccess")
    g_ScriptEvent:AddListener("UpdateActivity", self, "OnUpdateActivity")
    g_ScriptEvent:AddListener("QianYingChuWenShowMessage", self, "OnShowMessage")
    g_ScriptEvent:AddListener("MainPlayerCreated", self, "OnMainPlayerCreated")

    self.mainAnimator.enabled = true
    if LuaQianYingChuWenMgr:IsAllTaskFinish() then
        if CClientMainPlayer.Inst.PlayProp.BeginnerGuideInfo.Stage == EnumBeginnerGuideStage.eFinalRewarding then
            self:EnterFinishWnd("qianyingchuwenwnd_finishshow2")
        else
            self:EnterFinishWnd("qianyingchuwenwnd_finishshow")
        end
        return
    end

    self.mainAnimator:Play("qianyingchuwenwnd_normalshow", 0, 0)
end

function LuaQianYingChuWenWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncBeginnerGuideInfo", self, "OnSyncBeginnerGuideInfo")
    g_ScriptEvent:RemoveListener("RequestFixBeginnerGuideDay2FinishSuccess", self, "OnRequestFixBeginnerGuideDay2FinishSuccess")
    g_ScriptEvent:RemoveListener("RequestBeginnerGuideTaskRewardSuccess", self, "OnRequestBeginnerGuideTaskRewardSuccess")
    g_ScriptEvent:RemoveListener("RequestBeginnerGuideBatchTaskRewardSuccess", self, "OnRequestBeginnerGuideBatchTaskRewardSuccess")
    g_ScriptEvent:RemoveListener("UpdateActivity", self, "OnUpdateActivity")
    g_ScriptEvent:RemoveListener("QianYingChuWenShowMessage", self, "OnShowMessage")
    g_ScriptEvent:RemoveListener("MainPlayerCreated", self, "OnMainPlayerCreated")
end

-- 传送到有些场景需要额外处理关闭界面
function LuaQianYingChuWenWnd:OnMainPlayerCreated()
    if CScene.MainScene then
        local array = BeginnerGuide_Setting.GetData().CloseWndSceneTemplateId
        for i = 0, array.Length - 1 do
            if CScene.MainScene.SceneTemplateId == array[i] then
                CUIManager.CloseUI(CLuaUIResources.QianYingChuWenWnd)
                return
            end
        end
    end
end

function LuaQianYingChuWenWnd:OnSyncBeginnerGuideInfo()
    if CClientMainPlayer.Inst.PlayProp.BeginnerGuideInfo.Stage == EnumBeginnerGuideStage.eFinish then
        CUIManager.CloseUI(CLuaUIResources.QianYingChuWenWnd)
        return
    end

    if CClientMainPlayer.Inst.PlayProp.BeginnerGuideInfo.Stage == EnumBeginnerGuideStage.eFinalRewarding then
        self:UpdateHuDieActive()
        self:EnterFinishWnd("qianyingchuwenwnd_finishshow2")
        return
    end

    self:UpdateSpecialAwardInfo()
    self:UpdateTagAndOneKeyButtonState()
    self:UpdateTaskGrid()
    self:UpdateBottomTip()
end

function LuaQianYingChuWenWnd:OnRequestBeginnerGuideTaskRewardSuccess(taskId, postTaskId, todayAllFinish)
    self:AChuAnimPlay("wave")
    if todayAllFinish then
        local day = BeginnerGuide_Task.GetData(taskId).OpenDay
        self.flyAnimator.enabled = true
        local name = SafeStringFormat3("qianyingchuwenwnd_hudieguiji_0%d", day)
        self.flyAnimator:Play(name, 0, 0)

        self:ClearTick(self.flyTick)
        self.flyTick = RegisterTickOnce(function()
            self:UpdateHuDieActive()

            if LuaQianYingChuWenMgr:IsAllTaskFinish() then
                self:EnterFinishWnd("qianyingchuwenwnd_normalhide")
            end
        end, self.name2Time["qianyingchuwenwnd_hudieguiji_01"] * 1000)

        if not LuaQianYingChuWenMgr:IsAllTaskFinish() then
            self:SayAWord(g_MessageMgr:FormatMessage("QIANYINGCHUWEN_TODAY_ALL_FINISH"), BeginnerGuide_Setting.GetData().GoToMessageStayTime)
        end
        return
    end

    local word = BeginnerGuide_Task.GetData(taskId).ReceiveWord
    if System.String.IsNullOrEmpty(word) then return end
    self:SayAWord(word, BeginnerGuide_Setting.GetData().GoToMessageStayTime)
end

function LuaQianYingChuWenWnd:OnRequestBeginnerGuideBatchTaskRewardSuccess(tasks, todayAllFinishT)
    self:AChuAnimPlay("wave")

    local days = g_MessagePack.unpack(todayAllFinishT)
    if not LuaQianYingChuWenMgr:IsAllTaskFinish() and #days == 1 then
        self:SayAWord(g_MessageMgr:FormatMessage("QIANYINGCHUWEN_TODAY_ALL_FINISH"), BeginnerGuide_Setting.GetData().GoToMessageStayTime)
    end

    if #days > 0 then
        self.flyAnimator.enabled = true
        local name = SafeStringFormat3("qianyingchuwenwnd_hudieguiji_0%d", days[1])
        self.flyAnimator:Play(name, 0, 0)

        self:ClearTick(self.flyTick)
        self.flyTick = RegisterTickOnce(function()
            self:UpdateHuDieActive()

            if LuaQianYingChuWenMgr:IsAllTaskFinish() then
                self:EnterFinishWnd("qianyingchuwenwnd_normalhide")
            end
        end, self.name2Time["qianyingchuwenwnd_hudieguiji_01"] * 1000)
    end
end

-- 活动更新时刷新下显示，例如行酒令的前往按钮是否显示依赖于活动信息
function LuaQianYingChuWenWnd:OnUpdateActivity()
    LuaQianYingChuWenMgr:UpdateAcceptDayInfo()
    self:UpdateTaskGrid()
end

function LuaQianYingChuWenWnd:OnShowMessage(param1, param2, param3)
    self:SayAWord(g_MessageMgr:FormatMessage(param1, param3), BeginnerGuide_Setting.GetData().GoToMessageStayTime)
    if param2 == 1 then
        self:AChuAnimPlay("question")
    end
end

function LuaQianYingChuWenWnd:OnRequestFixBeginnerGuideDay2FinishSuccess(day)
    self:UpdateHuDieActive()
end


function LuaQianYingChuWenWnd:Init()
    if not CClientMainPlayer.Inst then
        CUIManager.CloseUI(CLuaUIResources.QianYingChuWenWnd)
        return
    end

    if LuaQianYingChuWenMgr:IsAllTaskFinish() then
        return
    end

    self:InitModel()
    self.aChuModel:LoadFx(g_UIFxPaths.QianYingChuWenAChuFxPath)

    self.bubbleAppearTick = RegisterTickOnce(function()
        self:RandomWord()
    end, 1500)

    LuaQianYingChuWenMgr:UpdateAcceptDayInfo()
    self:InitTagGrid()
    self:UpdateSpecialAwardInfo()
    self:UpdateTagAndOneKeyButtonState()
    self:UpdateBottomTip()
    self:UpdateHuDieActive()
    self:InitName2Time()
    self:CheckAllTaskStatus()

    Gac2Gas.QueryTaskHuoLiTimes()
    Gac2Gas.QuerySchoolTaskOpenStatus()
    CScheduleMgr.Inst:RequestActivity()
end

-- 更新数据
function LuaQianYingChuWenWnd:UpdateSpecialAwardInfo()
    self.specialAwardTaskIdDict = {}
    self.day2AwardIcon = {}
    local day2Finish = LuaQianYingChuWenMgr:GetDay2Finish()

    for taskId, icon in string.gmatch(BeginnerGuide_Setting.GetData().AwardIcons, "(%d+),([%w_]+);?") do
        local id = tonumber(taskId)
        local day = BeginnerGuide_Task.GetData(id).OpenDay
        local needShow = false
        if day2Finish[day] == EnumBeginnerGuideDayStatus.eEmpty then
            needShow = true
        elseif day2Finish[day] == EnumBeginnerGuideDayStatus.eAccept then
            local taskInfo = LuaQianYingChuWenMgr:GetTaskInfo(day)
            for _, data in pairs(taskInfo) do
                if data.id == id then
                    if data.status ~= EnumBeginnerGuideTaskStatus.eFinish then
                        needShow = true
                    end
                    break
                end
            end
        end

        if needShow then
            self.specialAwardTaskIdDict[id] = true
            self.day2AwardIcon[day] = icon
        end
    end
end

function LuaQianYingChuWenWnd:InitName2Time()
    self.name2Time = {
        qianyingchuwenwnd_hide = 0.233,
        qianyingchuwenwnd_hudieguiji_01 = 1.6,
    }
end

-- 更新左边蝴蝶的显示
function LuaQianYingChuWenWnd:UpdateHuDieActive()
    local day2Finish = LuaQianYingChuWenMgr:GetDay2Finish()

    for i = 1, 7 do
        self.butterflies[i]:SetActive(day2Finish[i] == EnumBeginnerGuideDayStatus.eFinish)
    end
end

-- 初始化人物模型
function LuaQianYingChuWenWnd:InitModel()
    self.modelPreviewer.IsShowHighRes = true

    local appear = CPropertyAppearance.GetDataFromCustom(CClientMainPlayer.Inst.Class, CClientMainPlayer.Inst.Gender,
        CPropertySkill.GetDefaultYingLingStateByGender(CClientMainPlayer.Inst.Gender), nil, nil, 0, 0, 0, 0,
        BeginnerGuide_Setting.GetData().HeadFashionId,
        BeginnerGuide_Setting.GetData().BodyFashionId,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, nil, nil, nil, nil, 0, 0, 0, 0, 0, 0, nil, 0)

    self.modelPreviewer:PreviewFakeAppear(appear)
    LuaModelTextureLoaderMgr:SetModelCameraColor(self.modelPreviewer.identifier)

    -- 隐藏武器
    self.modelPreviewer.m_RO:SetWeaponVisible(EWeaponVisibleReason.Expression, false)
    self.modelPreviewer.m_RO:ForceHideKuiLei(EHideKuiLeiStatus.ForceHideKuiLeiAndClose)

    local isMale = CClientMainPlayer.Inst.Gender == EnumGender.Male
    LuaUtils.SetLocalPositionY(self.modelPreviewer.transform:Find("Texture"), isMale and 65 or 105)
    LuaUtils.SetLocalPositionY(self.modelPreviewer.transform:Find("Sprite"), isMale and -334 or -294)
end

-- 任务
function LuaQianYingChuWenWnd:UpdateTaskGrid()
    local day = self.curSelectTagId
    if day <= 0 then return end

    local taskInfo = LuaQianYingChuWenMgr:GetTaskInfo(day)

    local taskCount = #taskInfo
    local childCount = self.taskGrid.transform.childCount
    if taskCount < childCount then
        for i = taskCount + 1, childCount do
            self.taskGrid.transform:GetChild(i - 1).gameObject:SetActive(false)
        end
    else
        for i = childCount + 1, taskCount do
            NGUITools.AddChild(self.taskGrid.gameObject, self.taskTemplate)
        end
    end

    local isEmpty = LuaQianYingChuWenMgr:GetDay2Finish()[day] == EnumBeginnerGuideDayStatus.eEmpty
    for i = 1, taskCount do
        local id = taskInfo[i].id
        local data = BeginnerGuide_Task.GetData(id)

        local child = self.taskGrid.transform:GetChild(i - 1)
        child.gameObject:SetActive(true)

        local qnReturnAward = child:Find("Award"):GetComponent(typeof(CQnReturnAwardTemplate))
        qnReturnAward:Init(Item_Item.GetData(data.Reward), data.RewardCount)

        local highlight = child:Find("Highlight").gameObject
        local fx = child:Find("Award/Fx"):GetComponent(typeof(CUIFx))
        if self.specialAwardTaskIdDict[id] then
            fx.gameObject:SetActive(true)
            fx:LoadFx(CUIFxPaths.ItemRemindYellowFxPath)

            highlight:SetActive(true)
        else
            fx.gameObject:SetActive(false)
            highlight:SetActive(false)
        end

        child:Find("Title"):GetComponent(typeof(UILabel)).text = data.Title
        local subtitle = child:Find("Title/Subtitle"):GetComponent(typeof(UILabel))
        subtitle.text = data.Subtitle
        subtitle:ResetAndUpdateAnchors()
        child:Find("Bar/Desc"):GetComponent(typeof(UILabel)).text = data.Desc

        child:Find("InactiveMask").gameObject:SetActive(isEmpty)
        if isEmpty then
            child:Find("InactiveMask/Text"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("第%d天开启"), day)
        end

        local finishMask = child:Find("Award/Finish").gameObject
        local finish = child:Find("Finish").gameObject
        local goToButton = child:Find("GoToButton").gameObject
        local submitButton = child:Find("SubmitButton").gameObject
        local condition = child:Find("Condition"):GetComponent(typeof(UILabel))
        local slider = child:Find("Bar/Slider"):GetComponent(typeof(UISlider))
        local state = child:Find("Bar/State"):GetComponent(typeof(UILabel))

        local isFinish = taskInfo[i].status == EnumBeginnerGuideTaskStatus.eFinish
        local isCanSubmit = taskInfo[i].status == EnumBeginnerGuideTaskStatus.eCanSubmit
        local isNew = taskInfo[i].status < EnumBeginnerGuideTaskStatus.eCanSubmit

        finishMask:SetActive(isFinish)
        finish:SetActive(isFinish)

        if isNew then
            submitButton:SetActive(false)

            if taskInfo[i].condition then
                condition.gameObject:SetActive(true)
                condition.text = taskInfo[i].condition
                goToButton:SetActive(false)
            else
                condition.gameObject:SetActive(false)
                goToButton:SetActive(not isEmpty)
                if isEmpty then
                else
                    UIEventListener.Get(goToButton).onClick = DelegateFactory.VoidDelegate(function (go)
                        self:OnGoToButtonClick(id)
                    end)
                end
            end

            if data.Event == "Shitu" then -- 师徒任务特殊处理，因为服务器处理会有点费
                slider.value = 0
                state.text = "0/1"
            else
                slider.value = Mathf.Clamp(taskInfo[i].accumulate / data.Target, 0, 1)
                state.text = SafeStringFormat3("%d/%d", taskInfo[i].accumulate, data.Target)
            end
        else
            if isFinish then
                submitButton:SetActive(false)
            elseif isCanSubmit then
                submitButton:SetActive(true)
                UIEventListener.Get(submitButton).onClick = DelegateFactory.VoidDelegate(function (go)
                    self:OnSubmitButtonClick(id)
                end)
            end

            goToButton:SetActive(false)
            condition.gameObject:SetActive(false)
            slider.value = 1
            state.text = ""
        end
    end
    if self.taskTableTween.progress ~= 1 then
        self.taskTableTween:Reset()
    end
    self.taskGrid:Reposition()
    self.taskScrollView:ResetPosition()
end


-- 初始化标签
function LuaQianYingChuWenWnd:InitTagGrid()
    self.curSelectTagId = -1
    local Number2Chinese = {
        LocalString.GetString("一"),
        LocalString.GetString("二"),
        LocalString.GetString("三"),
        LocalString.GetString("四"),
        LocalString.GetString("五"),
        LocalString.GetString("六"),
        LocalString.GetString("七")
    }

    Extensions.RemoveAllChildren(self.tagGrid.transform)
    for i = 1, 7 do
        local child = NGUITools.AddChild(self.tagGrid.gameObject, self.tagTemplate)
        child:SetActive(true)

        child.transform:Find("Num"):GetComponent(typeof(UILabel)).text = Number2Chinese[i]

        UIEventListener.Get(child).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnTagClick(i)
        end)
    end
    self.tagGrid:Reposition()
end

-- 更新标签和一键领取按钮显示
function LuaQianYingChuWenWnd:UpdateTagAndOneKeyButtonState()
    local day2Finish = LuaQianYingChuWenMgr:GetDay2Finish()

    local hasAwardCanReceive = false
    for i = 1, 7 do
        local tag = self.tagGrid.transform:GetChild(i - 1)
        local normal = tag:Find("Normal"):GetComponent(typeof(UITexture))
        local butterfly = tag:Find("Butterfly"):GetComponent(typeof(UITexture))
        local redDot = tag:Find("RedDot").gameObject
        local select = tag:Find("Select"):GetComponent(typeof(UITexture))
        tag:Find("Select/Fx").gameObject:SetActive(day2Finish[i] == EnumBeginnerGuideDayStatus.eAccept)
        butterfly.gameObject:SetActive(day2Finish[i] ~= EnumBeginnerGuideDayStatus.eFinish)
        redDot:SetActive(false)

        if day2Finish[i] == EnumBeginnerGuideDayStatus.eEmpty then
            normal.color = NGUIText.ParseColor24("98CCD3", 0)
            select.color = NGUIText.ParseColor24("98CCD3", 0)
            butterfly.color = NGUIText.ParseColor24("98CCD3", 0)
        else
            normal.color = NGUIText.ParseColor24("FFFFFF", 0)
            select.color = NGUIText.ParseColor24("FFFFFF", 0)
            butterfly.color = NGUIText.ParseColor24("FFFFFF", 0)
        end

        if day2Finish[i] == EnumBeginnerGuideDayStatus.eAccept then
            local firstTaskInfo = LuaQianYingChuWenMgr:GetTaskInfo(i)[1]
            if firstTaskInfo and firstTaskInfo.status == EnumBeginnerGuideTaskStatus.eCanSubmit then
                redDot:SetActive(true)
                hasAwardCanReceive = true
            end
        end
        self:UpdateAwardDisplay(i)
    end
    self.oneKeyButton.Enabled = hasAwardCanReceive

    if self.curSelectTagId <= 0 then
        local newId = -1
        for i = 7, 1, -1 do
            if day2Finish[i] == EnumBeginnerGuideDayStatus.eAccept then
                newId = i
                break
            end
            if day2Finish[i] == EnumBeginnerGuideDayStatus.eFinish and newId == -1 then
                newId = i
            end
        end

        self:OnTagClick(newId)
    end
end

-- 更新奖励图标的显示
function LuaQianYingChuWenWnd:UpdateAwardDisplay(i)
    local tag = self.tagGrid.transform:GetChild(i - 1)
    local num = tag:Find("Num").gameObject
    local award = tag:Find("Award"):GetComponent(typeof(CUITexture))
    if self.day2AwardIcon[i] and self.curSelectTagId ~= i then
        num:SetActive(false)
        award:LoadMaterial(SafeStringFormat3("UI/Texture/Transparent/Material/%s.mat", self.day2AwardIcon[i]))
    else
        num:SetActive(true)
        award:LoadMaterial("")
    end
end

-- 更新底部提示
function LuaQianYingChuWenWnd:UpdateBottomTip()
    self.bottomTip.text = SafeStringFormat3(LocalString.GetString("完成七页目标,  唤醒灵蝶"), LuaQianYingChuWenMgr:GetFinishedDayCount())
    self.bottomTip2.text = SafeStringFormat3(LocalString.GetString("( %d/7 ) 获取时装奖励!"), LuaQianYingChuWenMgr:GetFinishedDayCount())

end

-- 进入结束界面
function LuaQianYingChuWenWnd:EnterFinishWnd(animName)
    self.mainAnimator:Play(animName, 0, 0)

    local stage = CClientMainPlayer.Inst.PlayProp.BeginnerGuideInfo.Stage
    if stage == EnumBeginnerGuideStage.eOpen then
        self:ReceiveAward(BeginnerGuide_Setting.GetData().HeadFashionId)
    elseif stage == EnumBeginnerGuideStage.eFinalRewarding then
        self:ReceiveAward(BeginnerGuide_Setting.GetData().BodyFashionId)
    end
end

-- 领取奖励
function LuaQianYingChuWenWnd:ReceiveAward(fashionId)
    self.awardIcon:LoadMaterial(Fashion_Fashion.GetData(fashionId).Icon)
    UIEventListener.Get(self.finishWnd.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnAwardClick()
    end)

    self.clickTip.gameObject:SetActive(false)
    self:ClearTick(self.clickTick)
    self.clickTick = RegisterTickOnce(function ()
        self.clickTip.gameObject:SetActive(true)
    end, 2000)
end

-- 阿初
function LuaQianYingChuWenWnd:AChuAnimPlay(action)
    if not self.aChuAnim then
        self.aChuAnim = self.aChuModel.transform:GetComponentInChildren(typeof(SkeletonAnimation))
    end

    if not self.aChuAnim then return end

    self.aChuAnim.state:SetAnimation(0, action, false)
    self.aChuAnim.state:AddAnimation(0, "idle", true, 0)
end

-- 随机对话话语
function LuaQianYingChuWenWnd:RandomWord()
    if not CClientMainPlayer.Inst then return end
    if LuaQianYingChuWenMgr:GetTotalLoginDays() == 0 then return end

    local id = LuaQianYingChuWenMgr:GetNewWord()
    local data = BeginnerGuide_AChuWord.GetData(id)
    self:SayAWord(data.Content, data.StayTime)
end

function LuaQianYingChuWenWnd:SayAWord(text, stayTime)
    self:ClearTick(self.bubbleTick)

    self.aChuWord.text = text
    self.aChuWord:UpdateNGUIText()
    NGUIText.regionWidth = 1000000
    local size = NGUIText.CalculatePrintedSize(self.aChuWord.text)
    self.aChuWord.width = size.x > 774 and 774 or math.ceil(size.x)
    self.aChuBubble:ResetAndUpdateAnchors()

    -- 出现动效
    self.bubbleAppearTweenScale.enabled = false
    self.aChuWidget.alpha = 1
    LuaUtils.SetLocalScale(self.aChuWidget.transform, 1, 1, 1)
    self.bubbleAppearTweenScale:ResetToBeginning()
    self.bubbleAppearTweenScale:PlayForward()

    self.bubbleTick = RegisterTickOnce(function ()
        self:WaitToRandom()
    end, 1000 * stayTime)
end

-- 等几秒再弹出来新的话语
function LuaQianYingChuWenWnd:WaitToRandom()
    self:ClearTick(self.bubbleTick)

    -- 消失动效
    self.bubbleDisappearTweenScale:ResetToBeginning()
    self.bubbleDisappearTweenScale:PlayForward()
    self.bubbleDisappearTweenAlpha:ResetToBeginning()
    self.bubbleDisappearTweenAlpha:PlayForward()

    local interval = BeginnerGuide_Setting.GetData().BubbleIntervalTimeRange
    self.bubbleTick = RegisterTickOnce(function ()
        self:RandomWord()
    end, 1000 * math.random(interval[0], interval[1]))
end

function LuaQianYingChuWenWnd:ClearTick(tick)
    if tick then
        UnRegisterTick(tick)
    end
    return nil
end

function LuaQianYingChuWenWnd:OnDestroy()
    self:ClearTick(self.bubbleTick)
    self:ClearTick(self.clickTick)
    self:ClearTick(self.closeTick)
    self:ClearTick(self.flyTick)
    self:ClearTick(self.bubbleAppearTick)
    CUIManager.DestroyModelTexture(self.modelPreviewer.identifier)
end

--@region UIEvent

function LuaQianYingChuWenWnd:OnAChuClick()
    self:RandomWord()
end

function LuaQianYingChuWenWnd:OnAChuBubbleClick()
    self:RandomWord()
end

function LuaQianYingChuWenWnd:OnTagClick(id)
    if id == self.curSelectTagId then return end

    local day2Finish = LuaQianYingChuWenMgr:GetDay2Finish()

    local child = self.tagGrid.transform:GetChild(id - 1)
    LuaUtils.SetLocalPositionX(child:Find("Num"), -12)
    LuaUtils.SetLocalPositionX(child:Find("Butterfly"), 60)
    LuaUtils.SetLocalPositionX(child:Find("RedDot"), 31)
    child:Find("Normal").gameObject:SetActive(false)
    child:Find("Select").gameObject:SetActive(true)
    child:Find("Select/Fx").gameObject:SetActive(day2Finish[id] == EnumBeginnerGuideDayStatus.eAccept)
    child:Find("Butterfly").gameObject:SetActive(day2Finish[id] == EnumBeginnerGuideDayStatus.eEmpty)

    if self.curSelectTagId > 0 then
        child = self.tagGrid.transform:GetChild(self.curSelectTagId - 1)
        LuaUtils.SetLocalPositionX(child:Find("Num"), -35)
        LuaUtils.SetLocalPositionX(child:Find("Butterfly"), 27)
        LuaUtils.SetLocalPositionX(child:Find("RedDot"), 0)
        child:Find("Normal").gameObject:SetActive(true)
        child:Find("Select").gameObject:SetActive(false)
        child:Find("Butterfly").gameObject:SetActive(day2Finish[self.curSelectTagId] ~= EnumBeginnerGuideDayStatus.eFinish)
    end

    local beforeId = self.curSelectTagId
    self.curSelectTagId = id
    self:UpdateAwardDisplay(id)
    if beforeId > 0 then
        self:UpdateAwardDisplay(beforeId)
    end
    self:UpdateTaskGrid()
    self.taskTableTween:PlayAnim()
end

function LuaQianYingChuWenWnd:OnSubmitButtonClick(id)
    Gac2Gas.RequestBeginnerGuideTaskReward(id)
end

function LuaQianYingChuWenWnd:OnGoToButtonClick(id)
    local data = BeginnerGuide_Task.GetData(id)
    if not System.String.IsNullOrEmpty(data.Action) then
        for action in string.gmatch(data.Action, "([^;]+)") do
            ClientAction.DoAction(action)
        end

        if data.ClickCloseWnd > 0 then
            CUIManager.CloseUI(CLuaUIResources.QianYingChuWenWnd)
        end
    elseif EnumBeginnerGuideGoToActionFunc[data.Event] then
        LuaQianYingChuWenMgr[EnumBeginnerGuideGoToActionFunc[data.Event]](LuaQianYingChuWenMgr, data.GoToParams)
    end
end

function LuaQianYingChuWenWnd:OnAwardClick()
    local stage = CClientMainPlayer.Inst.PlayProp.BeginnerGuideInfo.Stage
    if stage == EnumBeginnerGuideStage.eOpen then
        Gac2Gas.RequestBeginnerGuideFinalReward(1)
    elseif stage == EnumBeginnerGuideStage.eFinalRewarding then
        Gac2Gas.RequestBeginnerGuideFinalReward(2)
    end
end

function LuaQianYingChuWenWnd:OnCloseButtonClick()
    CUIManager.CloseUI(CLuaUIResources.QianYingChuWenWnd)
end

function LuaQianYingChuWenWnd:OnOneKeyButtonClick()
    Gac2Gas.RequestBeginnerGuideBatchTaskReward()
end

--@endregion UIEvent

-- 如果外网打了patch删除了某个任务，可能会导致服务器不能正确的更新day2finish，因此添加一个保险，每次打开界面的时候，对所有任务状态进行检查
-- 如果所有任务都完成了，但是day2finish仍然处于未完成的状态，则给服务器发送一个RPC让服务器更新一下数据
function LuaQianYingChuWenWnd:CheckAllTaskStatus()
    local day2Finish = LuaQianYingChuWenMgr:GetDay2Finish()
    for i = 1, 7 do
        if day2Finish[i] == EnumBeginnerGuideDayStatus.eAccept then
            local taskInfo = LuaQianYingChuWenMgr:GetTaskInfo(i)
            local needUpdate = true
            for k, info in pairs(taskInfo) do
                if info.status ~= EnumBeginnerGuideTaskStatus.eFinish then
                    needUpdate = false
                    break
                end
            end
            if needUpdate then
                Gac2Gas.RequestFixBeginnerGuideDay2Finish(i)
            end
        end
    end
end
