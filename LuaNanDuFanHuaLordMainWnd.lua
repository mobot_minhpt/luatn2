local GameObject = import "UnityEngine.GameObject"

local QnSelectableButton = import "L10.UI.QnSelectableButton"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnRadioBox=import "L10.UI.QnRadioBox"

LuaNanDuFanHuaLordMainWnd = class()
LuaNanDuFanHuaLordMainWnd.OpenDefaultTabIndex = 1
--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaNanDuFanHuaLordMainWnd, "EntranceQnRadioBox", "EntranceQnRadioBox", QnRadioBox)
RegistChildComponent(LuaNanDuFanHuaLordMainWnd, "Tab1", "Tab1", QnSelectableButton)
RegistChildComponent(LuaNanDuFanHuaLordMainWnd, "Tab2", "Tab2", QnSelectableButton)
RegistChildComponent(LuaNanDuFanHuaLordMainWnd, "Tab3", "Tab3", QnSelectableButton)
RegistChildComponent(LuaNanDuFanHuaLordMainWnd, "Tab4", "Tab4", QnSelectableButton)
RegistChildComponent(LuaNanDuFanHuaLordMainWnd, "OverallWnd", "OverallWnd", GameObject)
RegistChildComponent(LuaNanDuFanHuaLordMainWnd, "LordWayWnd", "LordWayWnd", GameObject)
RegistChildComponent(LuaNanDuFanHuaLordMainWnd, "JuBaoPenWnd", "JuBaoPenWnd", GameObject)

--@endregion RegistChildComponent end

function LuaNanDuFanHuaLordMainWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    
    self:InitData()
    self:InitUIEvent()
end

function LuaNanDuFanHuaLordMainWnd:Init()
    self.Tab4.transform:Find("Selected").gameObject:SetActive(false)
    self:RefreshConstUI()

    g_ScriptEvent:AddListener("NanDuFanHua_OpenJuBaoPenAndHideOverall", self, "OnOpenJuBaoPenAndHideOverall")
end

--@region UIEvent

--@endregion UIEvent

function LuaNanDuFanHuaLordMainWnd:InitData()
    self.rightTabList = {self.Tab1, self.Tab2, self.Tab3, self.Tab4}
end

function LuaNanDuFanHuaLordMainWnd:InitUIEvent()
    self.EntranceQnRadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn, index)
        if index+1 == 4 then
            self.EntranceQnRadioBox:ChangeTo(0, false)
            self:OpenWndIndex(1)
            CUIManager.ShowUI(CLuaUIResources.NanDuFanHuaFashionPreviewWnd)
        else
            self:OpenWndIndex(index+1)
        end
    end)
end

function LuaNanDuFanHuaLordMainWnd:RefreshConstUI()
    if LuaNanDuFanHuaLordMainWnd.OpenDefaultTabIndex ~= 1 then
        self.EntranceQnRadioBox:ChangeTo(LuaNanDuFanHuaLordMainWnd.OpenDefaultTabIndex-1, false)
        self:OpenWndIndex(LuaNanDuFanHuaLordMainWnd.OpenDefaultTabIndex)
    end
    LuaNanDuFanHuaLordMainWnd.OpenDefaultTabIndex = 1
end

function LuaNanDuFanHuaLordMainWnd:OpenWndIndex(subIndex)
    for i = 1, #self.rightTabList do
        self.rightTabList[i].transform:Find("NormalState").gameObject:SetActive(subIndex ~= i)
    end
    self:OpenSubWnd(subIndex)
end

function LuaNanDuFanHuaLordMainWnd:OnDestroy()
    g_ScriptEvent:RemoveListener("NanDuFanHua_OpenJuBaoPenAndHideOverall", self, "OnOpenJuBaoPenAndHideOverall")
end

function LuaNanDuFanHuaLordMainWnd:OpenSubWnd(subIndex)
    if subIndex == 1 then
        --南都总览
        self.OverallWnd:SetActive(true)
        self.JuBaoPenWnd:SetActive(false)
        self.LordWayWnd:SetActive(false)
    elseif subIndex == 2 then
        --城主之路
        self.OverallWnd:SetActive(false)
        self.JuBaoPenWnd:SetActive(false)
        self.LordWayWnd:SetActive(true)
    elseif subIndex == 3 then
        --聚宝盆
        self.OverallWnd:SetActive(false)
        self.JuBaoPenWnd:SetActive(true)
        self.LordWayWnd:SetActive(false)
    elseif subIndex == 4 then    
        --仪仗
        --打开新界面 这里保持原装
        --self.OverallWnd:SetActive(false)
        --self.JuBaoPenWnd:SetActive(false)
        --CUIManager.ShowUI(CLuaUIResources.NanDuFanHuaFashionPreviewWnd)
    end
    
end 

function LuaNanDuFanHuaLordMainWnd:OnOpenJuBaoPenAndHideOverall(activeFameId)
    LuaNanDuFanHuaJuBaoPenWnd.ActiveFameId = activeFameId
    self.EntranceQnRadioBox:ChangeTo(2, true)
end

function LuaNanDuFanHuaLordMainWnd:GetGuideGo(methodName)
    if methodName == "GetNanDuLordChenJianButton" then
        return self.LordWayWnd.transform:Find("ScheduleWidget/ChenJian/Border/AddScheduleButton").gameObject
    end
end