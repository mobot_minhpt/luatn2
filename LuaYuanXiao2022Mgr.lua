LuaYuanXiao2022Mgr = {}
LuaYuanXiao2022Mgr.m_EndDengMiPlayTime = 0 --灯谜玩法结束时间
LuaYuanXiao2022Mgr.m_EndCurDengMiTime = 0 --当前灯谜结束时间
LuaYuanXiao2022Mgr.m_CurChaiHuaDengResult = nil -- or {{itemId =,num= }} 拆花灯奖励
LuaYuanXiao2022Mgr.m_CurDengMiIsCorrect = false --当前灯谜是否猜中
LuaYuanXiao2022Mgr.m_CurDengMiId = 0
LuaYuanXiao2022Mgr.m_CurDengMiIndex = 1 --当前灯谜题号
LuaYuanXiao2022Mgr.m_DengMiCount = 0
LuaYuanXiao2022Mgr.m_CurDengMyAnswer = ""

function LuaYuanXiao2022Mgr:UpdateYuanXiaoRiddle2022Question(questionCount, questionIdx, questionId, questionExpireTime, roundExpireTime)
    self.m_DengMiCount = questionCount
    self.m_CurDengMiIndex = questionIdx
    if questionId ~= self.m_CurDengMiId then
        self.m_CurChaiHuaDengResult = nil
        self.m_CurDengMiIsCorrect = false
        g_ScriptEvent:BroadcastInLua("OnSwitchYuanXiaoRiddle2022Question")
    end
    self.m_CurDengMiId = questionId
    self.m_EndCurDengMiTime = questionExpireTime
    self.m_EndDengMiPlayTime = roundExpireTime
    g_ScriptEvent:BroadcastInLua("UpdateYuanXiaoRiddle2022Question")
end

function LuaYuanXiao2022Mgr:UpdateYuanXiaoRiddle2022GuessResult(questionId, guessed, answer)
    if questionId ~= self.m_CurDengMiId then return end
    self.m_CurDengMiIsCorrect = guessed
    self.m_CurDengMyAnswer = answer
    g_ScriptEvent:BroadcastInLua("UpdateYuanXiaoRiddle2022Question")
end

function LuaYuanXiao2022Mgr:UpdateYuanXiaoRiddle2022OpenStatus(questionId, opened, itemNum1, itemNum2)
    if questionId ~= self.m_CurDengMiId then return end
    self.m_CurChaiHuaDengResult = opened and {} or nil
    if not opened then return end
    self.m_CurChaiHuaDengResult = {
        {itemId = YuanXiao_Setting.GetData().OpenLanternItemID1, num = itemNum1},
        {itemId = YuanXiao_Setting.GetData().OpenLanternItemID2, num = itemNum2}
    }
    g_ScriptEvent:BroadcastInLua("UpdateYuanXiaoRiddle2022OpenStatus")
end

function LuaYuanXiao2022Mgr:OpenWnd(data)
    local list = MsgPackImpl.unpack(data)
	if list and list.Count == 10 then
		self.m_DengMiCount = tonumber(list[0])
        self.m_CurDengMiIndex = tonumber(list[1])
        self.m_CurDengMiId = tonumber(list[2])
        self.m_EndCurDengMiTime = tonumber(list[3])
        self.m_EndDengMiPlayTime = tonumber(list[4])
        self.m_CurDengMiIsCorrect = list[5]
        self.m_CurDengMyAnswer = list[6]
        local opened = list[7]
        self.m_CurChaiHuaDengResult = opened and {} or nil
        if opened then 
            self.m_CurChaiHuaDengResult = {
                {itemId = YuanXiao_Setting.GetData().OpenLanternItemID1, num = tonumber(list[8])},
                {itemId = YuanXiao_Setting.GetData().OpenLanternItemID2, num = tonumber(list[9])}
            }
        end
	end
    if CUIManager.IsLoaded(CLuaUIResources.YuanXiao2022DengMiWnd) then
        g_ScriptEvent:BroadcastInLua("UpdateYuanXiaoRiddle2022Question")
    end
    
    CUIManager.ShowUI(CLuaUIResources.YuanXiao2022DengMiWnd)
end