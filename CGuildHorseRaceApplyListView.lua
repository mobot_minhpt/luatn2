-- Auto Generated!!
local CGuildHorseRaceApplyListView = import "L10.UI.CGuildHorseRaceApplyListView"
local CGuildHorseRaceApplyPlayerTemplate = import "L10.UI.CGuildHorseRaceApplyPlayerTemplate"
local CGuildHorseRaceMgr = import "L10.Game.CGuildHorseRaceMgr"
local Extensions = import "Extensions"
local NGUITools = import "NGUITools"
CGuildHorseRaceApplyListView.m_Init_CS2LuaHook = function (this) 
    Extensions.RemoveAllChildren(this.table.transform)
    this.playerTemplate:SetActive(false)
    local list = CGuildHorseRaceMgr.Inst.applyPlayerList
    do
        local i = 0
        while i < list.Count do
            local instance = NGUITools.AddChild(this.table.gameObject, this.playerTemplate)
            instance:SetActive(true)
            local template = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CGuildHorseRaceApplyPlayerTemplate))
            if template ~= nil then
                local default
                if i % 2 == 0 then
                    default = this.evenSprite
                else
                    default = this.oddSprite
                end
                local bg = default
                template:Init(list[i], bg)
            end
            i = i + 1
        end
    end
    this.table:Reposition()
    this.scrollView:ResetPosition()
end
