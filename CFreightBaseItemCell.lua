-- Auto Generated!!
local CFreightBaseItemCell = import "L10.UI.CFreightBaseItemCell"
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CUICommonDef = import "L10.UI.CUICommonDef"
local EnumQualityType = import "L10.Game.EnumQualityType"
local GuildFreight_Equipments = import "L10.Game.GuildFreight_Equipments"
CFreightBaseItemCell.m_Init_CS2LuaHook = function (this, ID, count) 
    this.item = CItemMgr.Inst:GetItemTemplate(ID)
    if this.item == nil then
        this.equip = GuildFreight_Equipments.GetData(ID)
    end
    this.amount.text = count == 0 and "" or tostring(count)
    if this.item ~= nil then
        this.icon:LoadMaterial(this.item.Icon)
        if this.quality ~= nil then
            this.quality.spriteName = CUICommonDef.GetItemCellBorder(EnumQualityType.White)
        end
    elseif this.equip ~= nil then
        this.icon:LoadMaterial(this.equip.Icon)
        if this.quality ~= nil then
            this.quality.spriteName = CUICommonDef.GetItemCellBorder(CommonDefs.ConvertIntToEnum(typeof(EnumQualityType), this.equip.Color))
        end
    end
end
