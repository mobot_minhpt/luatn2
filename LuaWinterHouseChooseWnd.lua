local CClientHouseMgr=import "L10.Game.CClientHouseMgr"
local QnRadioBox=import "L10.UI.QnRadioBox"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CShopMallMgr=import "L10.UI.CShopMallMgr"
local Constants=import "L10.Game.Constants"
local MsgPackImpl=import "MsgPackImpl"
local Object = import "System.Object"
local CClientFurnitureMgr=import "L10.Game.CClientFurnitureMgr"

CLuaWinterHouseChooseWnd = class()
RegistClassMember(CLuaWinterHouseChooseWnd, "m_DescLabel")
RegistClassMember(CLuaWinterHouseChooseWnd, "m_Cost")
RegistClassMember(CLuaWinterHouseChooseWnd, "m_TemplateIds")
RegistClassMember(CLuaWinterHouseChooseWnd, "m_WinterHousePrice")

CLuaWinterHouseChooseWnd.m_StatusLookup = {}

function CLuaWinterHouseChooseWnd:Init()
    self.m_WinterHousePrice = WinterHouse_Setting.GetData().WinterHousePrice

    local radioBox = FindChild(self.transform,"RadioBox"):GetComponent(typeof(QnRadioBox))
    local choice1 = radioBox.transform:Find("Choice1")
    choice1:Find("DescLabel"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("开启家园冬季场景，花费%s#287"),self.m_WinterHousePrice)
    local choice2 = radioBox.transform:Find("Choice2")
    self.m_DescLabel = choice2:Find("DescLabel"):GetComponent(typeof(UILabel))

    local unlockButton=FindChild(self.transform,"UnlockButton").gameObject
    UIEventListener.Get(unlockButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnClickButton()
    end)

    local viewButton = FindChild(self.transform,"ViewButton").gameObject
    UIEventListener.Get(viewButton).onClick=DelegateFactory.VoidDelegate(function(go)
        CUIManager.ShowUI(CLuaUIResources.WinterHouseUnlockWnd)
    end)

    -- Gac2Gas.RequestEnableWinterSceneAtHome()

    self.m_TemplateIds = {}
    local prop = CClientHouseMgr.Inst.mFurnitureProp

    local have = {}
    CommonDefs.DictIterate(CClientFurnitureMgr.Inst.FurnitureList, DelegateFactory.Action_object_object(function (___key, ___value) 
        local id = ___value.FurnishTemplateId
        if id>0 then
            have[id] = true
        end
    end))
    WinterHouse_Zhuangshiwu.ForeachKey(function(k)
        local wh = WinterHouse_Zhuangshiwu.GetData(k)
        if wh.Currency==1 then
            if not have[k] then
                CLuaWinterHouseChooseWnd.m_StatusLookup[k] = true
            end
            table.insert( self.m_TemplateIds,k )
        end
    end)

    self:OnUpdateWinterHouseCost()
end

function CLuaWinterHouseChooseWnd:OnClickButton()
    if CClientHouseMgr.Inst:IsCurHouseRoomer() then
        g_MessageMgr:ShowMessage("WINTER_OPEN_FANGKE_NOT_ALLOW")
        return
    end

    local mgr = CClientHouseMgr.Inst
    local level = mgr.mConstructProp.ZhengwuGrade

    local message = LocalString.GetString("你确定要解锁家园雪景吗？")
    MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function () 
        self:Buy()
    end),nil, nil, nil, false)
end
function  CLuaWinterHouseChooseWnd:Buy(  )
    local cost = WinterHouse_Setting.GetData().WinterHousePrice
    local radioBox = FindChild(self.transform,"RadioBox"):GetComponent(typeof(QnRadioBox))
    if radioBox.CurrentSelectIndex==1 then
        cost = self.m_Cost
    end

    --判断钱够不够
    local own = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Silver or 0
    if own<cost then
        local msg = g_MessageMgr:FormatMessage("NotEnough_Silver_QuickBuy")
        MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function () 
            CShopMallMgr.ShowLinyuShoppingMall(Constants.SilverItemId)
        end), 
        nil, LocalString.GetString("前往"), LocalString.GetString("取消"), false)
    else
        if radioBox.CurrentSelectIndex==0 then
            local empty = g_MessagePack.empty()
            Gac2Gas.RequestEnableWinterSceneAtHome(empty,empty,empty,empty)
        else
            --请求多个
            self:RequestMultiFuniture()
        end
    end
end

function CLuaWinterHouseChooseWnd:RequestMultiFuniture()
    local list = {}
    for i,v in ipairs(self.m_TemplateIds) do
        if not CLuaWinterHouseChooseWnd.m_StatusLookup[v] then
            table.insert( list,v )
        end
    end
    local span=60--60个为一组
    local count = math.ceil( #list/span )
    local params = {}
    for i=1,count do
        local max = math.min(i*span,#list)
        local itemlist = {}
        for j=(i-1)*span+1,max do
            print(list[j])
            table.insert( itemlist,list[j] )
        end
        table.insert( params,g_MessagePack.pack(itemlist) )
    end
    local empty = g_MessagePack.empty()
    local cnt = math.ceil(#params/4)
    for i = 1, cnt do
        Gac2Gas.RequestEnableWinterSceneAtHome(
            params[(i-1)*4+1] or empty,
            params[(i-1)*4+2] or empty,
            params[(i-1)*4+3] or empty,
            params[(i-1)*4+4] or empty)
    end
end

function CLuaWinterHouseChooseWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateWinterHouseCost", self, "OnUpdateWinterHouseCost")

end

function CLuaWinterHouseChooseWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateWinterHouseCost", self, "OnUpdateWinterHouseCost")

end

function CLuaWinterHouseChooseWnd:OnDestroy()
    CLuaWinterHouseChooseWnd.m_StatusLookup = {}
end

function CLuaWinterHouseChooseWnd:OnUpdateWinterHouseCost()
    -- self.m_Cost=cost
    local total = self.m_WinterHousePrice
    for i,v in ipairs(self.m_TemplateIds) do
        local wh = WinterHouse_Zhuangshiwu.GetData(v)
        if wh.Currency==1 and not CLuaWinterHouseChooseWnd.m_StatusLookup[v]  then
            total = total + wh.Price
        end
    end
    self.m_Cost=total
    self.m_DescLabel.text=SafeStringFormat3(LocalString.GetString("开启家园冬季场景以及小景花木类装饰物的雪景效果，花费%s#287"),total)
end