local CItemCountUpdate=import "L10.UI.CItemCountUpdate"
local CItemInfoMgr=import "L10.UI.CItemInfoMgr"
local CLivingSkillQuickMakeMgr=import "L10.UI.CLivingSkillQuickMakeMgr"
local LifeSkill_SkillType = import "L10.Game.LifeSkill_SkillType"
local LifeSkill_Item = import "L10.Game.LifeSkill_Item"
local CLivingSkillMgr = import "L10.Game.CLivingSkillMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local QnAddSubAndInputButton= import "L10.UI.QnAddSubAndInputButton"
local CUITexture=import "L10.UI.CUITexture"


CLuaLivingSkillMakeWnd = class()
RegistClassMember(CLuaLivingSkillMakeWnd,"titleLabel")
RegistClassMember(CLuaLivingSkillMakeWnd,"inputButton")
RegistClassMember(CLuaLivingSkillMakeWnd,"startMakeBtn")
RegistClassMember(CLuaLivingSkillMakeWnd,"huoliLabel")
RegistClassMember(CLuaLivingSkillMakeWnd,"curHuoliLabel")
RegistClassMember(CLuaLivingSkillMakeWnd,"makeLabel")
RegistClassMember(CLuaLivingSkillMakeWnd,"icon")
RegistClassMember(CLuaLivingSkillMakeWnd,"itemTemplate")
RegistClassMember(CLuaLivingSkillMakeWnd,"items")
RegistClassMember(CLuaLivingSkillMakeWnd,"arrow")
RegistClassMember(CLuaLivingSkillMakeWnd,"multiLabel")
RegistClassMember(CLuaLivingSkillMakeWnd,"unitCost")
RegistClassMember(CLuaLivingSkillMakeWnd,"itemData")

RegistClassMember(CLuaLivingSkillMakeWnd,"m_ConsumeItemInfo")


function CLuaLivingSkillMakeWnd:Awake()
    self.m_ConsumeItemInfo={}

    self.titleLabel = self.transform:Find("Wnd_Bg_Secondary_2/TitleLabel"):GetComponent(typeof(UILabel))
    self.inputButton = self.transform:Find("Anchor/Offset/StartMake/InputButton"):GetComponent(typeof(QnAddSubAndInputButton))
    self.startMakeBtn = self.transform:Find("Anchor/Offset/StartMake/StartMakeBtn").gameObject
    self.huoliLabel = self.transform:Find("Anchor/Offset/StartMake/HuoliLabel"):GetComponent(typeof(UILabel))
    self.curHuoliLabel = self.transform:Find("Anchor/Offset/StartMake/CurHuoliLabel"):GetComponent(typeof(UILabel))
    self.makeLabel = self.transform:Find("Anchor/Offset/StartMake/StartMakeBtn/MakeLabel"):GetComponent(typeof(UILabel))
    self.icon = self.transform:Find("Anchor/Offset/StartMake/Generated/Icon"):GetComponent(typeof(CUITexture))

    self.itemTemplate = self.transform:Find("Anchor/Offset/StartMake/ConsumeItem").gameObject--:GetComponent(typeof(CLivingMakeConsumeItem))
self.items = {}
    self.arrow = self.transform:Find("Anchor/Offset/StartMake/Generated/Arrow")
    self.multiLabel = self.transform:Find("Anchor/Offset/StartMake/MultiLabel"):GetComponent(typeof(UILabel))
self.itemData = nil
    UIEventListener.Get(self.startMakeBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnClickStartMakeBtn(go)
    end)
    
end
function CLuaLivingSkillMakeWnd:Init()
    self:UpdateContent()
end
function CLuaLivingSkillMakeWnd:GetMaxCanBuyCount( )
    local rtn = 999
    for i,v in ipairs(self.items) do
        local countUpdate = v:GetComponent(typeof(CItemCountUpdate))
        local val = math.floor(countUpdate.count/self.m_ConsumeItemInfo[i].needCount)
        if val<rtn then
            rtn=val
        end
    end

    return rtn
end
function CLuaLivingSkillMakeWnd:InitConsumeItem( data) 
    self.m_ConsumeItemInfo={}

    local count = data.ConsumeItems.Length

    for i=1,count do
        local item = data.ConsumeItems[i-1]
        table.insert( self.m_ConsumeItemInfo, {
            id = item.ID,
            needCount = item.count
        } )
    end

    if count == 1 then
        self.arrow.localPosition = Vector3(- 128, 75)
        self.items = { self.itemTemplate.transform }
        self:InitItem(1,self.itemTemplate.transform,data.ConsumeItems[0].ID, data.ConsumeItems[0].count)
        self.itemTemplate.transform.localPosition = Vector3(- 275, 138)
    elseif count == 2 then
        self.items = { self.itemTemplate.transform }--Table2Array({self.itemTemplate, nil}, MakeArrayClass(CLivingMakeConsumeItem))
        self.items[2] = NGUITools.AddChild(self.itemTemplate.transform.parent.gameObject, self.itemTemplate.gameObject).transform

        self:InitItem(1,self.items[1], data.ConsumeItems[0].ID, data.ConsumeItems[0].count)
        self:InitItem(2,self.items[2], data.ConsumeItems[1].ID, data.ConsumeItems[1].count)

        self.items[1].localPosition = Vector3(- 275, 215)
        self.items[2].localPosition = Vector3(- 275, 40)

    elseif count == 3 then
        self.items = { self.itemTemplate.transform }
        self.items[2] = NGUITools.AddChild(self.itemTemplate.transform.parent.gameObject, self.itemTemplate).transform
        self.items[3] = NGUITools.AddChild(self.itemTemplate.transform.parent.gameObject, self.itemTemplate).transform

        self:InitItem(1,self.items[1],data.ConsumeItems[0].ID, data.ConsumeItems[0].count)
        self:InitItem(2,self.items[2],data.ConsumeItems[1].ID, data.ConsumeItems[1].count)
        self:InitItem(3,self.items[3],data.ConsumeItems[2].ID, data.ConsumeItems[2].count)

        self.items[1].localPosition = Vector3(- 275, 215)
        self.items[2].localPosition = Vector3(- 353, 40)
        self.items[3].localPosition = Vector3(- 200, 40)

    elseif count == 4 then
        self.items = { self.itemTemplate.transform }
        self.items[2] = NGUITools.AddChild(self.itemTemplate.transform.parent.gameObject, self.itemTemplate).transform
        self.items[3] = NGUITools.AddChild(self.itemTemplate.transform.parent.gameObject, self.itemTemplate).transform
        self.items[4] = NGUITools.AddChild(self.itemTemplate.transform.parent.gameObject, self.itemTemplate).transform

        self:InitItem(1,self.items[1],data.ConsumeItems[0].ID, data.ConsumeItems[0].count)
        self:InitItem(2,self.items[2],data.ConsumeItems[1].ID, data.ConsumeItems[1].count)
        self:InitItem(3,self.items[3],data.ConsumeItems[2].ID, data.ConsumeItems[2].count)
        self:InitItem(4,self.items[4],data.ConsumeItems[3].ID, data.ConsumeItems[3].count)

        self.items[1].localPosition = Vector3(- 275, 215)
        self.items[2].localPosition = Vector3(- 353, 40)
        self.items[3].localPosition = Vector3(- 200, 40)
        self.items[4].localPosition = Vector3(- 200, 40)
    end
end
function CLuaLivingSkillMakeWnd:OnChangeAction( cmp) 
    self:UpdateMinMax()
    local num = self.inputButton:GetValue()
    if num == 0 then
        num = 1
    end
    cmp:SetMakeCount(num)
end

function CLuaLivingSkillMakeWnd:OnClickStartMakeBtn( go) 
    local data = LifeSkill_Item.GetData(CLivingSkillMgr.Inst.selectedItemId)
    if data ~= nil then
        --有次数
        local count = self.inputButton:GetValue()
        CLivingSkillMgr.Inst:RequestUseLifeSkill(data.ID, count)
    end
end
function CLuaLivingSkillMakeWnd:UpdateContent( )
    self.itemData = LifeSkill_Item.GetData(CLivingSkillMgr.Inst.selectedItemId)
    self.multiLabel.text = System.String.Format("X{0}", self.itemData.GroupNum)

    local template = CItemMgr.Inst:GetItemTemplate(self.itemData.ID)
    if template ~= nil then
        self.icon:LoadMaterial(template.Icon)
    else
        self.icon.material = nil
    end


    local skillTypeData = LifeSkill_SkillType.GetData(CLivingSkillMgr.Inst.selectedSkill)
    if skillTypeData ~= nil then
        repeat
            local default = skillTypeData.Type
            if default == 3 then
                self.makeLabel.text = LocalString.GetString("炼药")
                self.titleLabel.text = LocalString.GetString("药材炼制")
                break
            elseif default == 4 then
                self.makeLabel.text = LocalString.GetString("烹饪")
                self.titleLabel.text = LocalString.GetString("食材制作")
                break
            end
        until 1
    end

    self.huoliLabel.text = tostring(self.itemData.HuoLi)
    self:UpdateHuoli()

    self:InitConsumeItem(self.itemData)
    self:UpdateMinMax()
    self.inputButton.onValueChanged = DelegateFactory.Action_uint(function(val)
        self:UpdateCost(val)
    end)
    self.inputButton:SetValue(1, true)
end
function CLuaLivingSkillMakeWnd:UpdateMinMax( )
    local max = self:GetMaxCanBuyCount()
    if max == 0 then
        max = 1
    end
    self.inputButton:SetMinMax(1, max, 1)
    if self.inputButton:GetValue() > max then
        self.inputButton:SetValue(max, true)
    end
end
function CLuaLivingSkillMakeWnd:UpdateCost( count) 
    self.huoliLabel.text = tostring((self.itemData.HuoLi * count))
    if count > 0 then
        CUICommonDef.SetActive(self.startMakeBtn, true, true)
    else
        CUICommonDef.SetActive(self.startMakeBtn, false, true)
    end

    --再检查一下等级
    if self.itemData.SkillLev > CLivingSkillMgr.Inst:GetSkillLevel(CLivingSkillMgr.Inst.selectedSkill) then
        CUICommonDef.SetActive(self.startMakeBtn, false, true)
    end

    local makeCount = self.inputButton:GetValue()
    if makeCount == 0 then
        makeCount = 1
    end
    for i,v in ipairs(self.items) do
        self:SetMakeCount(v,i,makeCount)
    end
end

function CLuaLivingSkillMakeWnd:InitItem(index,tf,templateId)
    local countUpdate = tf.gameObject:GetComponent(typeof(CItemCountUpdate))
    local getNode = tf:Find("GetNode").gameObject

    countUpdate.templateId = templateId
    countUpdate.onChange = DelegateFactory.Action_int(function (count) 
        local inputCount=self.inputButton:GetValue()
        local needCount = self.m_ConsumeItemInfo[index].needCount*inputCount
        if needCount <= count then
            countUpdate.format = "{0}/"..tostring(needCount)
            getNode:SetActive(false)
        else
            countUpdate.format = "[ff0000]{0}[-]/"..tostring(needCount)
            getNode:SetActive(true)
        end
    end)
    countUpdate:UpdateCount()

    local icon = tf:GetComponent(typeof(CUITexture))
    local template = CItemMgr.Inst:GetItemTemplate(templateId)
    icon:LoadMaterial(template.Icon)

    UIEventListener.Get(tf.gameObject).onClick=DelegateFactory.VoidDelegate(function(go)
        local inputCount=self.inputButton:GetValue()
        local needCount = self.m_ConsumeItemInfo[index].needCount*inputCount

        if countUpdate.count >= needCount then
            CItemInfoMgr.ShowLinkItemTemplateInfo(templateId, false)
        else
            CLivingSkillQuickMakeMgr.Inst:ShowMakeTipWnd(templateId)
        end
    end)

end

function CLuaLivingSkillMakeWnd:SetMakeCount(tf,index,count)
    local countUpdate = tf.gameObject:GetComponent(typeof(CItemCountUpdate))
    local needCount = self.m_ConsumeItemInfo[index].needCount*count

    if needCount<=countUpdate.count then
        countUpdate.format = "{0}/"..tostring(needCount)
    else
        countUpdate.format = "[ff0000]{0}[-]/"..tostring(needCount)
    end
    countUpdate:Reformat()
end

function CLuaLivingSkillMakeWnd:OnEnable()
    g_ScriptEvent:AddListener("MainPlayerPlayPropUpdate", self, "UpdateHuoli")
end
function CLuaLivingSkillMakeWnd:OnDisable()
    g_ScriptEvent:RemoveListener("MainPlayerPlayPropUpdate", self, "UpdateHuoli")
end
function CLuaLivingSkillMakeWnd:UpdateHuoli()
    local curHuoli = CClientMainPlayer.Inst.PlayProp.HuoLi
    local huoliCost = self.itemData and self.itemData.HuoLi or 0
    self.curHuoliLabel.text = curHuoli >= huoliCost and tostring(curHuoli) or SafeStringFormat3("[c][ff0000]%d[-][/c]", curHuoli)
end