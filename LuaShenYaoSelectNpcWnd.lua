local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CItemMgr = import "L10.Game.CItemMgr"
local CSocialWndMgr = import "L10.UI.CSocialWndMgr"

LuaShenYaoSelectNpcWnd = class()
LuaShenYaoSelectNpcWnd.s_Level = nil
LuaShenYaoSelectNpcWnd.s_ItemId = nil
LuaShenYaoSelectNpcWnd.s_NpcTbl = {}

RegistClassMember(LuaShenYaoSelectNpcWnd, "m_Npcs")
RegistClassMember(LuaShenYaoSelectNpcWnd, "m_CurIdx")
RegistClassMember(LuaShenYaoSelectNpcWnd, "m_Wnd")

function LuaShenYaoSelectNpcWnd:Awake()
    self.m_Wnd = self.transform:GetComponent(typeof(CCommonLuaWnd))

    UIEventListener.Get(self.transform:Find("Anchor/ChatButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        CSocialWndMgr.ShowChatWnd()
    end)

    --self.transform:Find("ShenYaoSelectNpcWnd_Bg/CloseButton").gameObject:SetActive(false)
    self.transform:Find("Anchor/HintLabel"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("选择要召唤的%s阶蜃妖"), LuaShenYaoSelectNpcWnd.s_Level) 
    
    self.m_Npcs = {}
    self.m_CurIdx = 1
    for i = 1, #LuaShenYaoSelectNpcWnd.s_NpcTbl do
        local npcId = LuaShenYaoSelectNpcWnd.s_NpcTbl[i]
        local npc = self.transform:Find("Anchor/Npc"..i)
        table.insert(self.m_Npcs, npc)

        local npcData = NPC_NPC.GetData(npcId)
        npc:GetComponent(typeof(CUITexture)):LoadNPCPortrait(npcData.Portrait)
        npc:Find("NameLabel"):GetComponent(typeof(UILabel)).text = npcData.Name

        UIEventListener.Get(npc.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            for i = 1, #self.m_Npcs do
                self.m_Npcs[i]:Find("Selected").gameObject:SetActive(false)
            end
            npc:Find("Selected").gameObject:SetActive(true)
            self.m_CurIdx = i
        end)
    end
    self.m_Npcs[self.m_CurIdx]:Find("Selected").gameObject:SetActive(true)

    UIEventListener.Get(self.transform:Find("Anchor/OkButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        local npcId = LuaShenYaoSelectNpcWnd.s_NpcTbl[self.m_CurIdx]
        local pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, LuaShenYaoSelectNpcWnd.s_ItemId)
        if npcId and pos > 0 then
            Gac2Gas.ShenYao_CreatePersonalNpcByExchangeTicket(pos, LuaShenYaoSelectNpcWnd.s_ItemId, npcId)
            self.m_Wnd:Close()
        end
    end)
end

function LuaShenYaoSelectNpcWnd:Init()

end
