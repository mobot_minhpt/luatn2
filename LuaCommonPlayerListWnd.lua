local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UISimpleTableView = import "L10.UI.UISimpleTableView"
local EnumCommonPlayerListUpdateType = import "L10.Game.EnumCommonPlayerListUpdateType"
local DefaultUISimpleTableViewDataSource = import "L10.UI.DefaultUISimpleTableViewDataSource"
local CCommonPlayerListItem = import "L10.UI.CCommonPlayerListItem"
local CCommonPlayerListMgr = import "L10.Game.CCommonPlayerListMgr"
local Object = import "System.Object"
local Vector4 = import "UnityEngine.Vector4"
local Regex = import "System.Text.RegularExpressions.Regex"
local CCommonPlayerDisplayData = import "L10.Game.CCommonPlayerDisplayData"

LuaCommonPlayerListWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistClassMember(LuaCommonPlayerListWnd, "titleLabel")
RegistClassMember(LuaCommonPlayerListWnd, "template")
RegistClassMember(LuaCommonPlayerListWnd, "tableView")
RegistClassMember(LuaCommonPlayerListWnd, "searchView")
RegistClassMember(LuaCommonPlayerListWnd, "searchInput")
RegistClassMember(LuaCommonPlayerListWnd, "clearSearchButton")
RegistClassMember(LuaCommonPlayerListWnd, "searchButton")

RegistClassMember(LuaCommonPlayerListWnd, "m_DefaultSimpleTableViewDataSource")
RegistClassMember(LuaCommonPlayerListWnd, "m_DisabledItems")

RegistClassMember(LuaCommonPlayerListWnd, "m_Data")

--@endregion RegistChildComponent end

function LuaCommonPlayerListWnd:Awake()
    self.titleLabel = self.transform:Find("Wnd_Bg_Secondary_2/TitleLabel"):GetComponent(typeof(UILabel))
    self.template = self.transform:Find("Anchor/ScrollView/Pool/Item").gameObject
    self.tableView = self.transform:GetComponent(typeof(UISimpleTableView))
    self.searchView = self.transform:Find("Anchor/SearchView").gameObject
    self.searchInput = self.transform:Find("Anchor/SearchView/SearchInput"):GetComponent(typeof(UIInput))
    self.clearSearchButton = self.transform:Find("Anchor/SearchView/ClearSearchButton").gameObject
    self.searchButton = self.transform:Find("Anchor/SearchView/SearchButton").gameObject

    UIEventListener.Get(self.clearSearchButton).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnClearSearchButtonClick()
    end)
    UIEventListener.Get(self.searchButton).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnSearchButtonClick()
    end)
    CommonDefs.AddEventDelegate(self.searchInput.onChange, DelegateFactory.Action(function ()
        self:OnInputValueChanged()
    end))
end

function LuaCommonPlayerListWnd:Init()
    self.m_DisabledItems = {}
    self.titleLabel.text = CCommonPlayerListMgr.Inst.WndTitle
    self.template:SetActive(false)
    self:OnClearSearchButtonClick()
    
    self.m_Data = List2Table(CCommonPlayerListMgr.Inst.allData)
    if not self.m_DefaultSimpleTableViewDataSource then
		self.m_DefaultSimpleTableViewDataSource = DefaultUISimpleTableViewDataSource.Create(function ()
			return #self.m_Data
		end,
		function (index)
			return self:CellForRowAtIndex(index + 1)
		end)
	end
    self.tableView:Clear()
    self.tableView.dataSource = self.m_DefaultSimpleTableViewDataSource
    self.tableView:LoadData(0, false)

    if CCommonPlayerListMgr.Inst.UpdateType == EnumCommonPlayerListUpdateType.AtGuildMember then
        self.searchView:SetActive(true)
        self.tableView.scrollView.panel.baseClipRegion = Vector4(-2, -30, 892, 452)
        Gac2Gas.QueryOnlineMembersInMyGuild(EnumQueryOnlineGuildMembersContext_lua.AtGuildMember)
    else
        self.searchView:SetActive(false)
        self.tableView.scrollView.panel.baseClipRegion = Vector4(-2, 25, 892, 562)
    end
    local pos = self.tableView.table.transform.localPosition
    pos.y = Extensions.GetTopPos(self.tableView.scrollView).y
    self.tableView.table.transform.localPosition = pos
end

function LuaCommonPlayerListWnd:OnCommonPlayerListDataUpdate(args)
    if args[0] == EnumCommonPlayerListUpdateType_lua.AtGuildMember then
        self.m_Data = List2Table(CCommonPlayerListMgr.Inst.allData)
        self.tableView:Clear()
        self.tableView.dataSource = self.m_DefaultSimpleTableViewDataSource
        self.tableView:LoadData(0, false)
    end
end

function LuaCommonPlayerListWnd:CellForRowAtIndex(index)
    local cellIdentifier = "RowCell"

    local cell = self.tableView:DequeueReusableCellWithIdentifier(cellIdentifier)
    if cell == nil then
        cell = self.tableView:AllocNewCellWithIdentifier(self.template, cellIdentifier)
    end
    cell:SetActive(true)
    if self.m_Data[index] then
        local data = self.m_Data[index]
        local disabled = self.m_DisabledItems[data.playerId] or false
        local itemScript = cell:GetComponent(typeof(CCommonPlayerListItem))
        itemScript:Init(data, CCommonPlayerListMgr.Inst.ButtonText, not disabled)
        itemScript.OnButtonClickDelegate = DelegateFactory.Action_ulong_CCommonPlayerListItem(function(playerId, item)
            self:OnItemButtonClick(playerId, item)
        end)
    end

    return cell
end

--@region UIEvent
function LuaCommonPlayerListWnd:OnItemButtonClick(playerId, item) 
    if CCommonPlayerListMgr.Inst.OnPlayerSelected ~= nil then
        GenericDelegateInvoke(CCommonPlayerListMgr.Inst.OnPlayerSelected, Table2ArrayWithCount({playerId}, 1, MakeArrayClass(Object)))
    end
    self.m_DisabledItems[playerId] = true
    item:SetButtonEnabled(false)
end

function LuaCommonPlayerListWnd:OnClearSearchButtonClick()
    self.searchInput.value = ""
    self:OnInputValueChanged()
    self.m_Data = List2Table(CCommonPlayerListMgr.Inst.allData)
    self.tableView:Clear()
    self.tableView.dataSource = self.m_DefaultSimpleTableViewDataSource
    self.tableView:LoadData(0, false)
end

function LuaCommonPlayerListWnd:OnSearchButtonClick()
    local resTable = {}
    local searchKey = self.searchInput.value
    if System.String.IsNullOrEmpty(searchKey) or #searchKey < 4 then
        g_MessageMgr:ShowMessage("CommonPlayerList_No_Search_Input")
        return
    end

    local default, searchId = System.UInt64.TryParse(searchKey)
    for i = 0, CCommonPlayerListMgr.Inst.allData.Count - 1 do
        local data = CCommonPlayerListMgr.Inst.allData[i]
        if default and data.playerId == searchId then
            table.insert(resTable, data)
        end
        if string.find(data.playerName, searchKey, 1, true) then
            local name = CommonDefs.StringReplace(data.playerName, searchKey, SafeStringFormat3("[c][FF9052]%s[-][/c]", searchKey))
            local _data = CreateFromClass(CCommonPlayerDisplayData, data.playerId, name, data.level, data.cls, data.gender, data.expressionIndex, data.isOnline)
            table.insert(resTable, _data)
        end
    end
    if #resTable == 0 then
        g_MessageMgr:ShowMessage("CommonPlayerList_No_Search_Result")
        return
    end
    self.m_Data = resTable
    self.tableView:Clear()
    self.tableView.dataSource = self.m_DefaultSimpleTableViewDataSource
    self.tableView:LoadData(0, false)
end

function LuaCommonPlayerListWnd:OnInputValueChanged()
    self.clearSearchButton.gameObject:SetActive(not System.String.IsNullOrEmpty(self.searchInput.value))
end

--@endregion UIEvent

function LuaCommonPlayerListWnd:OnEnable()
	g_ScriptEvent:AddListener("OnCommonPlayerListDataUpdate", self, "OnCommonPlayerListDataUpdate")
end

function LuaCommonPlayerListWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnCommonPlayerListDataUpdate", self, "OnCommonPlayerListDataUpdate")
end
