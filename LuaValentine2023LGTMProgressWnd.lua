
LuaValentine2023LGTMProgressWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaValentine2023LGTMProgressWnd, "slider")
RegistClassMember(LuaValentine2023LGTMProgressWnd, "highlight")
RegistClassMember(LuaValentine2023LGTMProgressWnd, "highlight_Left")
RegistClassMember(LuaValentine2023LGTMProgressWnd, "highlight_Right")
RegistClassMember(LuaValentine2023LGTMProgressWnd, "tweenFloat")

function LuaValentine2023LGTMProgressWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

    self.slider = self.transform:Find("Slider"):GetComponent(typeof(UISlider))
    self.highlight = self.slider.transform:Find("Highlight"):GetComponent(typeof(UIWidget))
    self.highlight_Left = self.highlight.transform:Find("Left"):GetComponent(typeof(UIWidget))
    self.highlight_Right = self.highlight.transform:Find("Right"):GetComponent(typeof(UIWidget))
end

function LuaValentine2023LGTMProgressWnd:OnEnable()
    g_ScriptEvent:AddListener("PlayerDoLGTMFlySkill", self, "OnPlayerDoLGTMFlySkill")
end

function LuaValentine2023LGTMProgressWnd:OnDisable()
    g_ScriptEvent:RemoveListener("PlayerDoLGTMFlySkill", self, "OnPlayerDoLGTMFlySkill")
end

function LuaValentine2023LGTMProgressWnd:OnPlayerDoLGTMFlySkill(doSkillPos, isSucc)
    if self.tweenFloat then
        LuaTweenUtils.Kill(self.tweenFloat, false)
    end

    local totalTime = Valentine2023_LGTM_Setting.GetData().OperatDuration
    local curTime = self.slider.value * totalTime
    local info = LuaValentine2023Mgr.LGTMInfo
    local curIsSucc = curTime >= info.rangeStart and curTime <= info.rangeStart + info.rangeWidth
    if curIsSucc == isSucc then return end

    self.slider.value = math.min(doSkillPos / totalTime, 1)
end

function LuaValentine2023LGTMProgressWnd:Init()
    if self.tweenFloat then
        LuaTweenUtils.Kill(self.tweenFloat, false)
    end

    local totalTime = Valentine2023_LGTM_Setting.GetData().OperatDuration
    self.tweenFloat = LuaTweenUtils.TweenFloat(0, 1, totalTime, function (val)
        self.slider.value = val
	end)

    local foreground = self.slider.foregroundWidget
    local info = LuaValentine2023Mgr.LGTMInfo
    local width = (info.rangeWidth / totalTime) * foreground.width
    self.highlight.width = width
    local x = foreground.transform.localPosition.x + (info.rangeStart / totalTime) * foreground.width + width / 2
    LuaUtils.SetLocalPositionX(self.highlight.transform, x)
    self.highlight_Left:ResetAndUpdateAnchors()
    self.highlight_Right:ResetAndUpdateAnchors()
end

function LuaValentine2023LGTMProgressWnd:OnDestroy()
    if self.tweenFloat then
        LuaTweenUtils.Kill(self.tweenFloat, false)
    end
end

--@region UIEvent
--@endregion UIEvent
