-- Auto Generated!!
local CommonDefs = import "L10.Game.CommonDefs"
local CRankClassTemplate = import "L10.UI.CRankClassTemplate"
local EnumClass = import "L10.Game.EnumClass"
local Extensions = import "Extensions"
local NGUITools = import "NGUITools"
local Profession = import "L10.Game.Profession"
local UISprite = import "UISprite"
CRankClassTemplate.m_Init_CS2LuaHook = function (this, cls) 
    Extensions.RemoveAllChildren(this.table.transform)
    this.classTemplate:SetActive(false)
    if cls == nil then
        return
    end
    do
        local i = 0
        while i < cls.Count do
            local go = NGUITools.AddChild(this.table.gameObject, this.classTemplate)
            go:SetActive(true)
            local sprite = CommonDefs.GetComponent_GameObject_Type(go, typeof(UISprite))
            if sprite ~= nil and cls[i] ~= 0 then
                sprite.spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), cls[i]))
            end
            i = i + 1
        end
    end
    this.table:Reposition()
end
