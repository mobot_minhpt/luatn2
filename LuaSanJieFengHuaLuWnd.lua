local UISprite = import "UISprite"
local CUIFx = import "L10.UI.CUIFx"
local CommonDefs        = import "L10.Game.CommonDefs"
local DelegateFactory   = import "DelegateFactory"
local UILabel           = import "UILabel"
local UIGrid            = import "UIGrid"
local CUITexture        = import "L10.UI.CUITexture"
local UITabBar          = import "L10.UI.UITabBar"
local GameObject        = import "UnityEngine.GameObject"
local CItemInfoMgr		= import "L10.UI.CItemInfoMgr"
local AlignType			= import "L10.UI.CItemInfoMgr+AlignType"
local Animator          = import "UnityEngine.Animator"
local CUIRestrictScrollView  = import "L10.UI.CUIRestrictScrollView"

LuaSanJieFengHuaLuWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaSanJieFengHuaLuWnd, "SubTitleLabel", "SubTitleLabel", UILabel)
RegistChildComponent(LuaSanJieFengHuaLuWnd, "Icon", "Icon", CUITexture)
RegistChildComponent(LuaSanJieFengHuaLuWnd, "ContentGrid", "ContentGrid", GameObject)
RegistChildComponent(LuaSanJieFengHuaLuWnd, "Tabs", "Tabs", UITabBar)
RegistChildComponent(LuaSanJieFengHuaLuWnd, "ContentDetail", "ContentDetail", GameObject)
RegistChildComponent(LuaSanJieFengHuaLuWnd, "PageTitle", "PageTitle", UILabel)
RegistChildComponent(LuaSanJieFengHuaLuWnd, "ContentLabel", "ContentLabel", UILabel)
RegistChildComponent(LuaSanJieFengHuaLuWnd, "ArrowRight", "ArrowRight", GameObject)
RegistChildComponent(LuaSanJieFengHuaLuWnd, "ArrowLeft", "ArrowLeft", GameObject)
RegistChildComponent(LuaSanJieFengHuaLuWnd, "PageIndex", "PageIndex", UILabel)
RegistChildComponent(LuaSanJieFengHuaLuWnd, "PageTitle2", "PageTitle2", UILabel)
RegistChildComponent(LuaSanJieFengHuaLuWnd, "Style1", "Style1", GameObject)
RegistChildComponent(LuaSanJieFengHuaLuWnd, "Style2", "Style2", GameObject)
RegistChildComponent(LuaSanJieFengHuaLuWnd, "ContentLabel2", "ContentLabel2", UILabel)
RegistChildComponent(LuaSanJieFengHuaLuWnd, "RewardBtn", "RewardBtn", GameObject)
RegistChildComponent(LuaSanJieFengHuaLuWnd, "ItemsGrid", "ItemsGrid", UIGrid)
RegistChildComponent(LuaSanJieFengHuaLuWnd, "GotTag", "GotTag", GameObject)
RegistChildComponent(LuaSanJieFengHuaLuWnd, "RuleBtn", "RuleBtn", GameObject)
RegistChildComponent(LuaSanJieFengHuaLuWnd, "ProgressImg", "ProgressImg", UISprite)
RegistChildComponent(LuaSanJieFengHuaLuWnd, "UnlockRewardBtn", "UnlockRewardBtn", GameObject)
RegistChildComponent(LuaSanJieFengHuaLuWnd, "UnlockRewardFx", "UnlockRewardFx", CUIFx)
RegistChildComponent(LuaSanJieFengHuaLuWnd, "ProgressLabel", "ProgressLabel", UILabel)
RegistChildComponent(LuaSanJieFengHuaLuWnd, "NodeSprite", "NodeSprite", GameObject)

--@endregion RegistChildComponent end

RegistClassMember(LuaSanJieFengHuaLuWnd, "m_PageIndex")--当前页数,1开始
RegistClassMember(LuaSanJieFengHuaLuWnd, "m_TabType")--1目录;2灵物;3异闻;4方外;5岁时;
RegistClassMember(LuaSanJieFengHuaLuWnd, "m_TabNames")
RegistClassMember(LuaSanJieFengHuaLuWnd, "m_Datas")--所有的词条，按照tab分类储存
RegistClassMember(LuaSanJieFengHuaLuWnd, "m_State")--当前页面状态，0:标题页;1:详细页
RegistClassMember(LuaSanJieFengHuaLuWnd, "m_SelectIndex")--当前点击项的索引
RegistClassMember(LuaSanJieFengHuaLuWnd, "m_AllCount")
RegistClassMember(LuaSanJieFengHuaLuWnd, "m_HaveUnlockReward")

RegistClassMember(LuaSanJieFengHuaLuWnd, "m_Ctrls")
RegistClassMember(LuaSanJieFengHuaLuWnd, "m_RewardBtnFx")

function LuaSanJieFengHuaLuWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    self.Tabs.IgonreRepeatClick=false
	self.Tabs.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
	    self:OnTabClick(index)
	end)

	UIEventListener.Get(self.ArrowRight.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnArrowRightClick()
	end)

	UIEventListener.Get(self.ArrowLeft.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnArrowLeftClick()
	end)

	UIEventListener.Get(self.RewardBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRewardBtnClick()
	end)

	UIEventListener.Get(self.RuleBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRuleBtnClick()
	end)

	UIEventListener.Get(self.UnlockRewardBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnUnlockRewardBtnClick()
	end)

    --@endregion EventBind end

    self.m_AllCount = 0
    self.m_HaveUnlockReward = false

    self.m_RewardBtnFx = LuaGameObject.GetChildNoGC(self.RewardBtn.transform,"Fx").uiFx
    self.m_RewardBtnFx:LoadFx("fx/ui/prefab/UI_zbsjf_goumaianniu02.prefab")

    self.m_Ctrls = {}
    for i=1,16 do
        local ctrl = LuaGameObject.GetChildNoGC(self.ContentGrid.transform,"Item"..i).gameObject
        self.m_Ctrls[i] = ctrl
        CommonDefs.AddOnClickListener(ctrl, DelegateFactory.Action_GameObject(function (go)
            self:OnItemClick(i)
        end), false)
    end
end

function LuaSanJieFengHuaLuWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncFengHuaItemStatus", self, "OnSyncData")
    g_ScriptEvent:AddListener("SyncFengHuaProgressReward", self, "OnSyncProgressReward")
end

function LuaSanJieFengHuaLuWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncFengHuaItemStatus", self, "OnSyncData")
    g_ScriptEvent:RemoveListener("SyncFengHuaProgressReward", self, "OnSyncProgressReward")
end

--只更新进度奖励信息
function LuaSanJieFengHuaLuWnd:OnSyncProgressReward()
    self:InitProgress()
end

function LuaSanJieFengHuaLuWnd:OnSyncData()
    self:ShowTabsAlert()
    self:InitProgress()
    if self.m_State == 0 then--如果是标题页面
        self:TurnPage(0)--刷新当前页面
    else
        self:TurnItemDetail(0)--刷新当前详细页
    end
end

function LuaSanJieFengHuaLuWnd:ShowTabsAlert()
    local datas = LuaWorldEventMgr2021.FengHuaLuDatas
    local alerts = {}
    for i=1,#datas do
        local cfgid = datas[i].ID
        local cfgdata = self:GetDataByCfgID(cfgid)
        if cfgdata.Rewards and #cfgdata.Rewards > 0 and not datas[i].Rewarded then
            alerts[cfgdata.Type] = true
            alerts[1] = true
        end
    end
    local tabscount = #self.m_TabNames
    for i=1,tabscount do
        local tab = self.Tabs:GetTabGoByIndex(i-1)
        local ctrl = LuaGameObject.GetChildNoGC(tab.transform,"AlertSprite").gameObject
        ctrl:SetActive(alerts[i])
    end
end

function LuaSanJieFengHuaLuWnd:Init()
    
    self.m_TabType = 1
    self.m_PageIndex = 1

    local tabstr = WorldEvent_Setting.GetData().FengHuaLuTabName
    self.m_TabNames = g_LuaUtil:StrSplitAdv(tabstr, ";")

    self:InitData(false)

    if LuaWorldEventMgr2021.CurID == -1 then
        self.Tabs:ChangeTab(0,false)
    else
        local data = self:GetDataByCfgID(LuaWorldEventMgr2021.CurID)
        self:ShowDetail(data)
    end

    self:ShowTabsAlert()

    self:InitProgress()
end

function LuaSanJieFengHuaLuWnd:InitProgress()
    local step = 0
    local rewards = {}
    
    WorldEvent_FengHuaLuAward.Foreach(function(k,cfg)
        rewards[k] = cfg.Account
    end)
    
    local allstep = #rewards
    local maxcount = rewards[allstep]

    local unlockcount = #LuaWorldEventMgr2021.FengHuaLuDatas
    unlockcount = math.min(unlockcount,maxcount)
    
    self.ProgressLabel.text = unlockcount.."/"..maxcount
    self.ProgressImg.fillAmount = unlockcount / maxcount

    local parent = self.NodeSprite.transform.parent.gameObject
    local width = self.ProgressImg.width
    for i=1,allstep do
        if rewards[i] < maxcount then
            local nodego = NGUITools.AddChild(parent,self.NodeSprite)
            nodego:SetActive(true)
            local pos = nodego.transform.localPosition
            pos.x = width * rewards[i]/maxcount
            nodego.transform.localPosition = pos
            local lb = LuaGameObject.GetChildNoGC(nodego.transform,"CountLabel").label
            lb.text = rewards[i]
        end
        if unlockcount >= rewards[i] then
            step = i
        end
    end

    self.m_HaveUnlockReward = step >LuaWorldEventMgr2021.ProgressReward

    local anim = self.UnlockRewardBtn:GetComponent(typeof(Animator))
    if self.m_HaveUnlockReward then
        self.UnlockRewardBtn:SetActive(true)
        anim.enabled = true
        CUICommonDef.SetGrey(self.UnlockRewardBtn,false)
        self.UnlockRewardFx.gameObject:SetActive(true)
    else
        anim.enabled = false
        if step == allstep then
            self.UnlockRewardBtn:SetActive(false)
        else
            CUICommonDef.SetGrey(self.UnlockRewardBtn,true)
            self.UnlockRewardFx.gameObject:SetActive(false)
        end
    end
end

function LuaSanJieFengHuaLuWnd:GetGotIndex(cfgid)
    local datas = LuaWorldEventMgr2021.FengHuaLuDatas
    for i=1,#datas do
        if datas[i].ID == cfgid then 
            return i
        end
    end
    return 0
end

function LuaSanJieFengHuaLuWnd:GetDataByCfgID(cfgid)
    local datas = self.m_Datas[1]
    for i=1,#datas do
        if datas[i].ID == cfgid then 
            return datas[i]
        end
    end
    return nil
end

--[[
    @desc: 读取配置表数据
    author:{author}
    time:2021-01-12 15:32:47
    --@force: 
    @return:
]]
function LuaSanJieFengHuaLuWnd:InitData(force)
    if not force and self.m_Datas ~=nil then return end

    self.m_Datas = {}
    self.m_Datas[1] = {}

    WorldEvent_FengHuaLu.Foreach(function(k,cfg)
        local tabtype = cfg.Type + 1

        local awardstr = cfg.Award

        local op = System.StringSplitOptions.RemoveEmptyEntries
        local splits = Table2ArrayWithCount({",",";"}, 2, MakeArrayClass(System.String))
        local strs = CommonDefs.StringSplit_ArrayChar_StringSplitOptions(awardstr, splits, op)

        local rewards = {}
        local len = strs.Length

        if len >= 2 then
            for i=0,len-1,2 do
                rewards[#rewards+1] = {
                    ItemID = tonumber(strs[i]),
                    Count = tonumber(strs[1])
                }
            end
        end

        local data = {
            ID = cfg.ID,
            Index = cfg.Index,
            Name = cfg.Name,
            Icon = cfg.Icon,
            Des = cfg.Describe,
            Type = tabtype,
            Rewards = rewards,
            Open = cfg.Open,
            CanWish = cfg.CanWish == 1,
            HuoliNeed = cfg.WishNeedHuoLi,
            WishProb = cfg.WishProbability
        }
        if self.m_Datas[tabtype] == nil then
            self.m_Datas[tabtype] = {}
        end

        local len = #self.m_Datas[tabtype]
        self.m_Datas[tabtype][len+1] = data

        local len2= #self.m_Datas[1]
        self.m_Datas[1][len2+1] = data

        self.m_AllCount = self.m_AllCount + 1
    end)

    self:Sort()
end

function LuaSanJieFengHuaLuWnd:Sort()
    local tabs = #self.m_Datas
    for i=1,tabs do
        local datas = self.m_Datas[i]
        self:SortDatas(datas)
    end
end

function LuaSanJieFengHuaLuWnd:SortDatas(datas)
    table.sort(datas, function(a, b)
        if a.Type == b.Type then
            if a.Index == b.Index then 
                return a.ID < b.ID
            else
                return a.Index < b.Index
            end
        else
            return a.Type < b.Type
        end 
    end)
end

function LuaSanJieFengHuaLuWnd:IsWished(id)
    local wished = LuaWorldEventMgr2021.FengHuaLuWishedDatas[id]
    if wished == nil then 
        return false
    else
        return wished
    end 
end

function LuaSanJieFengHuaLuWnd:ShowDetail(data)
    local gotindex = self:GetGotIndex(data.ID)
    local pdata = LuaWorldEventMgr2021.FengHuaLuDatas[gotindex]
    if pdata == nil then -- 未获得
        if data.Open == 0 then
            g_MessageMgr:ShowMessage("FengHuaLu_Collection_Close_Tip")
            return
        end
        local canwish = data.CanWish --配置表可许愿
        if canwish then 
            local iswished = self:IsWished(data.ID)
            if iswished then 
                g_MessageMgr:ShowMessage("FengHuaLu_Collection_Already_Wish")
                return
            end
        end

        if canwish then --可许愿且未许愿
            LuaWorldEventMgr2021.ShowFengHuaLuWishWnd(data.ID)
        else
            g_MessageMgr:ShowMessage("FENGHUALU_UNLOCK_TIP")
        end
        return
    end

    self.m_SelectIndex = gotindex

    self.m_State = 1
    self.ContentGrid:SetActive(false)
    self.ContentDetail:SetActive(true)

    self:RefreshDetail(data,pdata.Rewarded)
end

--@region UIEvent

function LuaSanJieFengHuaLuWnd:OnItemClick(index)

    local rindex = (self.m_PageIndex-1)*16+index

    local datas = self.m_Datas[self.m_TabType]
    if datas == nil then return end

    local data = datas[rindex]
    if data == nil then return end

    self:ShowDetail(data)

end

function LuaSanJieFengHuaLuWnd:OnTabClick(index)--index从0开始
    self:RefreshTab(index)
end

function LuaSanJieFengHuaLuWnd:OnArrowRightClick()
    if self.m_State == 0 then
        self:TurnPage(1)
    else
        self:TurnItemDetail(1)
    end
end

function LuaSanJieFengHuaLuWnd:OnArrowLeftClick()
    if self.m_State == 0 then
        self:TurnPage(-1)
    else
        self:TurnItemDetail(-1)
    end
end

function LuaSanJieFengHuaLuWnd:OnRewardBtnClick()
    local pdata = LuaWorldEventMgr2021.FengHuaLuDatas[self.m_SelectIndex]
    if pdata == nil or pdata.Rewarded then return end
    Gac2Gas.RequestGetFengHuaLuItemAward(pdata.ID)
end

function LuaSanJieFengHuaLuWnd:OnRuleBtnClick()
    g_MessageMgr:ShowMessage("FENGHUALU_RULE")
end

function LuaSanJieFengHuaLuWnd:OnUnlockRewardBtnClick()
    if self.m_HaveUnlockReward then
        LuaWorldEventMgr2021.GetProgressReward()
    else
        g_MessageMgr:ShowMessage("FengHuaLu_Award_Notice_Tip")
    end
end


--@endregion UIEvent

function LuaSanJieFengHuaLuWnd:TurnItemDetail(delta)
    local index = self.m_SelectIndex + delta
    if index <= 0 then return end

    local pdata = LuaWorldEventMgr2021.FengHuaLuDatas[index]
    if pdata == nil then return end

    local cfgid = pdata.ID
    local data = self:GetDataByCfgID(cfgid)
    if data == nil then return end

    self.m_SelectIndex = index
    self:RefreshDetail(data,pdata.Rewarded)
end

function LuaSanJieFengHuaLuWnd:TurnPage(delta)
    local page = self.m_PageIndex + delta
    self:RefreshPage(page)
end

--[[
    @desc: 根据指定的页数刷新图鉴标题页面
    author:{author}
    time:2020-12-17 17:36:54
    --@page: 指定页数
    @return:
]]
function LuaSanJieFengHuaLuWnd:RefreshPage(page)
    if page <= 0 then return end
    local datas = self.m_Datas[self.m_TabType]
    local pagemax = 1
    if datas ~= nil then 
        local count = #datas
        if count > 0 then
            pagemax = 1 + math.floor((count-1)/16)
        end
    end

    if page > pagemax then return end
    
    self.m_PageIndex = page

    self:RefreshItems(page)

    self:RefreshPageLabel(self.m_PageIndex,pagemax)
end

--[[
    @desc: 刷新某页的所有图鉴控件
    author:{author}
    time:2021-01-19 15:25:09
    --@page: 
    @return:
]]
function LuaSanJieFengHuaLuWnd:RefreshItems(page)
    local datas = self.m_Datas[self.m_TabType]
    for i=1,16 do
        local ctrl = self.m_Ctrls[i]
        
        local rindex = (page -1) * 16 + i
        if datas == nil or rindex > #datas then 
            ctrl:SetActive(false)
        else
            local lb = LuaGameObject.GetChildNoGC(ctrl.transform,"ItemLabel").label
            local alert = LuaGameObject.GetChildNoGC(ctrl.transform,"AlertSprite").gameObject
            local wishsp = LuaGameObject.GetChildNoGC(ctrl.transform,"WishSprite").gameObject
            local wishbg = LuaGameObject.GetChildNoGC(ctrl.transform,"WishBg").gameObject
            local gotindex = self:GetGotIndex(datas[rindex].ID)

            local color = lb.color
            local grey = false
            local needalert = false
            local txt = ""

            local pdata = LuaWorldEventMgr2021.FengHuaLuDatas[gotindex]

            local enablebg = false
            local enablelogo = false

            if pdata then--已经获得
                txt = datas[rindex].Name
                
                if datas[rindex].Rewards and #datas[rindex].Rewards > 0 and not pdata.Rewarded then
                    needalert = true
                end
                enablelogo = false
                enablebg = false
                color.a = 1
            else
                if datas[rindex].Open == 0 then
                    txt = LocalString.GetString("未开放")
                    grey = true
                    color.a = 80/255
                    enablelogo = false
                    enablebg = false
                else
                    local canwish = datas[rindex].CanWish
                    if canwish then --可许愿的词条
                        color.a = 1
                        local iswished = self:IsWished(datas[rindex].ID)
                        if iswished then 
                            txt = datas[rindex].Name
                            --txt = LocalString.GetString("已许愿")
                            enablelogo = true
                            enablebg = false
                        else
                            txt = LocalString.GetString("可许愿")
                            enablelogo = false
                            enablebg = true
                        end
                    else --不可许愿的词条
                        txt = LocalString.GetString("待解锁")
                        color.a = 80/255
                        enablelogo = false
                        enablebg = false
                    end
                end
            end

            lb.text = txt 
            lb.color = color

            alert:SetActive(needalert)
            wishsp:SetActive(enablelogo)
            wishbg:SetActive(enablebg)

            CUICommonDef.SetGrey(ctrl.gameObject,grey)
            ctrl:SetActive(true)
        end
    end
end

function LuaSanJieFengHuaLuWnd:RefreshPageLabel(index,pagemax)
    local format = LocalString.GetString("第  %d/%d  页")
    self.PageIndex.text = SafeStringFormat3(format,index,pagemax)

    self.ArrowLeft:SetActive(index > 1)
    self.ArrowRight:SetActive(index < pagemax)
end

--[[
    @desc: 刷新指定数据的详细页面
    author:{author}
    time:2020-12-18 16:37:13
    --@data: 指定的数据
    @return:
]]
function LuaSanJieFengHuaLuWnd:RefreshDetail(data,rewarded)
    local format = LocalString.GetString("%s卷·第%s章")
    local chapterStr = Extensions.ToChineseWithinTenThousand(data.Index)
    self.PageTitle2.text = SafeStringFormat3(format,self.m_TabNames[data.Type],chapterStr)
    
    local subtitlego = self.SubTitleLabel.gameObject
    subtitlego:SetActive(false)
    self.SubTitleLabel.text = data.Name
    subtitlego:SetActive(true)
    
    self.Icon:LoadMaterial(data.Icon)

    local rewards = data.Rewards
    local haveReward = rewards ~= nil and #rewards > 0
    self.Style1:SetActive(not haveReward)
    self.Style2:SetActive(haveReward)
    if not haveReward then --无奖励
        self.ContentLabel.text = data.Des
    else
        self.ContentLabel2.text = data.Des
        local dragview = self.ContentLabel2.transform.parent:GetComponent(typeof(CUIRestrictScrollView))
        dragview:ResetPosition()
        self:FillRewards(rewards)
        CUICommonDef.SetActive(self.RewardBtn,not rewarded, true)
        self.GotTag:SetActive(rewarded)
        if self.m_RewardBtnFx then
            self.m_RewardBtnFx.gameObject:SetActive(not rewarded)
        end
    end
    self.Tabs:ChangeTab(data.Type-1,true)

    self.ArrowLeft:SetActive(self.m_SelectIndex > 1)
    self.ArrowRight:SetActive(self.m_SelectIndex < #LuaWorldEventMgr2021.FengHuaLuDatas)
end

function LuaSanJieFengHuaLuWnd:FillRewards(rewards)
    local g1 = 0
    local g2 = 0
    local count = #rewards
    if count <= 2 then 
        g1 = count
    else
        g2 = math.ceil(count/2)
        g1 = count - g2
    end
    self:FillGridRewards(1,1,g1,rewards)
    self:FillGridRewards(2,g1+1,g2,rewards)
    self.ItemsGrid:Reposition()
end

function LuaSanJieFengHuaLuWnd:FillGridRewards(gridIndex,start,count,rewards)
    local grid = self.ItemsGrid.transform:Find("Grid"..gridIndex)
    grid.gameObject:SetActive(count>0)
    if count > 0 then
        for i=1,3 do
            local itemcell = grid:Find("ItemCell"..i)
            if i <= count then
                self:FillItem(rewards[start+i-1],itemcell)
                itemcell.gameObject:SetActive(true)
            else
                itemcell.gameObject:SetActive(false)
            end
        end
        local gcom = grid.gameObject:GetComponent(typeof(UIGrid))
        gcom:Reposition()
    end
end

function LuaSanJieFengHuaLuWnd:FillItem(reward,celltrans)
    local icon = LuaGameObject.GetChildNoGC(celltrans,"IconTexture").cTexture
    local countlb = LuaGameObject.GetChildNoGC(celltrans,"AmountLabel").label

    local itemcfg = Item_Item.GetData(reward.ItemID)
    if itemcfg then
        icon:LoadMaterial(itemcfg.Icon)
    end

    if reward.Count <= 1 then
        countlb.text = ""
    else
        countlb.text = tostring(reward.Count)
    end

    UIEventListener.Get(celltrans.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        CItemInfoMgr.ShowLinkItemTemplateInfo(reward.ItemID, false, nil, AlignType.Left, 0, 0, 0, 0) 
    end)
end

--[[
    @desc: 根据指定的页签刷新图鉴标题页面到该页签的第一页
    author:{author}
    time:2020-12-17 17:37:30
    --@tabindex: 页签index，从0开始
    @return:
]]
function LuaSanJieFengHuaLuWnd:RefreshTab(tabindex)
    local tabtype = tabindex + 1
    self.m_TabType = tabtype
    self.m_PageIndex = 1
    self.m_SelectIndex = 0

    self.PageTitle.text = self.m_TabNames[tabtype]


    self.m_State = 0
    self.ContentGrid:SetActive(true)
    self.ContentDetail:SetActive(false)

    self:RefreshPage(1)--刷新第一页
end
