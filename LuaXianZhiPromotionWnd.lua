require("common/common_include")

local UIGrid = import "UIGrid"
local CButton = import "L10.UI.CButton"
local QnCheckBox = import "L10.UI.QnCheckBox"
local CUITexture = import "L10.UI.CUITexture"
local CItemMgr = import "L10.Game.CItemMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemAccessTipMgr = import "L10.UI.CItemAccessTipMgr"
local CTooltipAlignType = import "L10.UI.CTooltip+AlignType"
local Item_Item = import "L10.Game.Item_Item"
local MessageMgr = import "L10.Game.MessageMgr"

LuaXianZhiPromotionWnd = class()

RegistChildComponent(LuaXianZhiPromotionWnd, "DescLabel", UILabel)
RegistChildComponent(LuaXianZhiPromotionWnd, "NeedItemGrid", UIGrid)
RegistChildComponent(LuaXianZhiPromotionWnd, "ItemTemplate", GameObject)
RegistChildComponent(LuaXianZhiPromotionWnd, "ExtendButton", CButton)
RegistChildComponent(LuaXianZhiPromotionWnd, "PromoteButton", CButton)
RegistChildComponent(LuaXianZhiPromotionWnd, "CancelButton", CButton)
RegistChildComponent(LuaXianZhiPromotionWnd, "QnCheckBox", QnCheckBox)

RegistClassMember(LuaXianZhiPromotionWnd, "NeedItems")

function LuaXianZhiPromotionWnd:Init()

	self.ItemTemplate:SetActive(false)

	self:InitPromotion()
	self:UpdateNeedItemList()

	CommonDefs.AddOnClickListener(self.PromoteButton.gameObject, DelegateFactory.Action_GameObject(function(go)
		self:OnPromoteButtonClicked(go)
	end), false)

	CommonDefs.AddOnClickListener(self.CancelButton.gameObject, DelegateFactory.Action_GameObject(function (go)
		self:OnCancelButtonClicked(go)
	end), false)
	
end

function LuaXianZhiPromotionWnd:UpdateNeedItemList()
	self.NeedItems = {}
	local status = CLuaXianzhiMgr.GetXianZhiStatus()

	if status == EnumXianZhiStatus.NotFengXian then
		CUIManager.CloseUI(CLuaUIResources.XianZhiPromotionWnd)
		return
	end

	local setting = XianZhi_Setting.GetData()
	local items = nil
	-- 晋升
	if status == EnumXianZhiStatus.DistrictRegion then
		items = setting.CityPromoteItems
	elseif status == EnumXianZhiStatus.CityRegion then
		items = setting.SanJiePromoteItems
	end

	if not items then
		CUIManager.CloseUI(CLuaUIResources.XianZhiPromotionWnd)
		return
	end

	for i = 0, items.Length-1, 2 do
		local itemId = items[i]
		local count = items[i+1]
		table.insert(self.NeedItems, {
			itemId = itemId,
			count = count,
			})
	end

	self:UpdateNeedItems()
	self:UpdatePromotionDesc()
end

-- 晋升界面(重新分配仙职)
function LuaXianZhiPromotionWnd:InitPromotion()
	
	self.CancelButton.gameObject:SetActive(false)
	self.QnCheckBox.gameObject:SetActive(true)
	self.QnCheckBox:SetSelected(false, true)
	self.PromoteButton.gameObject:SetActive(true)
	self.ExtendButton.gameObject:SetActive(false)
end

-- 晋升界面描述
function LuaXianZhiPromotionWnd:UpdatePromotionDesc()
	local msg = g_MessageMgr:FormatMessage("XianZhi_Promotion_Confirm")
	self.DescLabel.text = msg
end

-- 更新所需物品
function LuaXianZhiPromotionWnd:UpdateNeedItems()

	CUICommonDef.ClearTransform(self.NeedItemGrid.transform)

	for i = 1, #self.NeedItems do
		local go = NGUITools.AddChild(self.NeedItemGrid.gameObject, self.ItemTemplate)
		self:InitItemTemplate(go, self.NeedItems[i])
		go:SetActive(true)
	end
	self.NeedItemGrid:Reposition()
end

function LuaXianZhiPromotionWnd:InitItemTemplate(go, info)
	local Icon = go.transform:Find("Icon"):GetComponent(typeof(CUITexture))
	local CountLabel = go.transform:Find("CountLabel"):GetComponent(typeof(UILabel))
	local DisableIcon = go.transform:Find("DisableIcon").gameObject
	local itemId = info.itemId
	local count = info.count

	local item = Item_Item.GetData(tonumber(itemId))
	Icon:LoadMaterial(item.Icon)
	local bindItemCountInBag, notBindItemCountInBag = CItemMgr.Inst:CalcItemCountInBagByTemplateId(itemId)
	local totalCount = bindItemCountInBag + notBindItemCountInBag
	CountLabel.text = SafeStringFormat3("%d/%d", totalCount, count)
	if totalCount < count then
		DisableIcon:SetActive(true)
	else
		DisableIcon:SetActive(false)
	end

	CommonDefs.AddOnClickListener(Icon.gameObject, DelegateFactory.Action_GameObject(function (gameObject)
			self:OnItemClicked(gameObject, itemId, totalCount >= count)
		end), false)

end

function LuaXianZhiPromotionWnd:OnItemClicked(go, itemId, isEnough)
	if not isEnough then
		CItemAccessTipMgr.Inst:ShowAccessTip(nil, itemId, false, go.transform, CTooltipAlignType.Top)
	else
		CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType.Default, 0, 0, 0, 0)
	end
end

function LuaXianZhiPromotionWnd:OnPromoteButtonClicked(go)
	if not self:IsNeedItemsEnough() then
		MessageMgr.Inst:ShowMessage("XianZhi_Promote_Items_Not_Enough", {})
		return
	end

	Gac2Gas.RequestPromoteXianZhi(self.QnCheckBox.Selected)
	CUIManager.CloseUI(CLuaUIResources.XianZhiPromotionWnd)
end

function LuaXianZhiPromotionWnd:IsNeedItemsEnough()
	for i = 1, #self.NeedItems do
		local needCount = self.NeedItems[i].count
		local bindItemCountInBag, notBindItemCountInBag = CItemMgr.Inst:CalcItemCountInBagByTemplateId(self.NeedItems[i].itemId)
		local totalCount = bindItemCountInBag + notBindItemCountInBag
		if totalCount < needCount then
			return false
		end
	end
		
	return true
end

function LuaXianZhiPromotionWnd:OnCancelButtonClicked(go)
	CUIManager.CloseUI(CLuaUIResources.XianZhiPromotionWnd)
end

function LuaXianZhiPromotionWnd:SendItem(args)
	self:UpdateNeedItems()
end


function LuaXianZhiPromotionWnd:SetItemAt(args)
	self:UpdateNeedItems()
end


function LuaXianZhiPromotionWnd:OnEnable()
	g_ScriptEvent:AddListener("SendItem", self, "SendItem")
	g_ScriptEvent:AddListener("SetItemAt", self, "SetItemAt")
end

function LuaXianZhiPromotionWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SendItem", self, "SendItem")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "SetItemAt")
end

return LuaXianZhiPromotionWnd
