local DelegateFactory  = import "DelegateFactory"
local CButton = import "L10.UI.CButton"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local DateTime = import "System.DateTime"

LuaShiTuFruitPartyNormalStateWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaShiTuFruitPartyNormalStateWnd, "m_ExpandButton", "ExpandButton", CButton)
RegistChildComponent(LuaShiTuFruitPartyNormalStateWnd, "m_ScoreLabel", "ScoreLabel", UILabel)
RegistChildComponent(LuaShiTuFruitPartyNormalStateWnd, "m_MaxScoreLabel", "MaxScoreLabel", UILabel)
RegistChildComponent(LuaShiTuFruitPartyNormalStateWnd, "m_ComboRoot", "ComboRoot", GameObject)
RegistChildComponent(LuaShiTuFruitPartyNormalStateWnd, "m_Label", "Label", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaShiTuFruitPartyNormalStateWnd, "m_StartTime")
RegistClassMember(LuaShiTuFruitPartyNormalStateWnd, "m_EndTime")
RegistClassMember(LuaShiTuFruitPartyNormalStateWnd, "m_Tick")

function LuaShiTuFruitPartyNormalStateWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.m_ExpandButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnExpandButtonClick()
	end)


    --@endregion EventBind end
end

function LuaShiTuFruitPartyNormalStateWnd:Init()
	self.m_ComboRoot:SetActive(false)
	self:UpdateScore()
	self.m_Tick = RegisterTick(function()
        self:OnTick()
    end, 1000)
end

--@region UIEvent

function LuaShiTuFruitPartyNormalStateWnd:OnExpandButtonClick()
	LuaUtils.SetLocalRotation(self.m_ExpandButton.transform,0,0,0)
	CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end

--@endregion UIEvent

function LuaShiTuFruitPartyNormalStateWnd:OnEnable()
	g_ScriptEvent:AddListener("InitShuiGuoPaiDuiScore", self, "OnInitShuiGuoPaiDuiScore")
    g_ScriptEvent:AddListener("SyncShuiGuoPaiDuiScore", self, "OnSyncShuiGuoPaiDuiScore")
	g_ScriptEvent:AddListener("SyncShuiGuoPaiDuiLianJi", self, "OnSyncShuiGuoPaiDuiLianJi")
end

function LuaShiTuFruitPartyNormalStateWnd:OnDisable()
	g_ScriptEvent:RemoveListener("InitShuiGuoPaiDuiScore", self, "OnInitShuiGuoPaiDuiScore")
    g_ScriptEvent:RemoveListener("SyncShuiGuoPaiDuiScore", self, "OnSyncShuiGuoPaiDuiScore")
	g_ScriptEvent:RemoveListener("SyncShuiGuoPaiDuiLianJi", self, "OnSyncShuiGuoPaiDuiLianJi")
end

function LuaShiTuFruitPartyNormalStateWnd:OnInitShuiGuoPaiDuiScore(score, maxScore)
	self:UpdateScore()
end

function LuaShiTuFruitPartyNormalStateWnd:OnSyncShuiGuoPaiDuiScore(score)
	self:UpdateScore()
end

function LuaShiTuFruitPartyNormalStateWnd:UpdateScore()
	self.m_ScoreLabel.text = tostring(LuaFruitPartyMgr.m_Score)
	self.m_MaxScoreLabel.text = tostring(LuaFruitPartyMgr.m_MaxScore)
end

function LuaShiTuFruitPartyNormalStateWnd:OnSyncShuiGuoPaiDuiLianJi(lianJiCount)
	self:ShowCombo(lianJiCount, 2)
end

function LuaShiTuFruitPartyNormalStateWnd:ShowCombo(num, time)
	self.m_StartTime = CServerTimeMgr.Inst:GetZone8Time()
    self.m_EndTime = self.m_StartTime:AddSeconds(time)
	self.m_Label.text = num
	self.m_ComboRoot:SetActive(true)
end

function LuaShiTuFruitPartyNormalStateWnd:OnTick()
	local now = CServerTimeMgr.Inst:GetZone8Time()
	if self.m_EndTime and DateTime.Compare(now, self.m_EndTime) > 0 then
		self.m_ComboRoot:SetActive(false)
	end
end

function LuaShiTuFruitPartyNormalStateWnd:OnDestroy()
    if self.m_Tick ~= nil then
        UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
    end
end
