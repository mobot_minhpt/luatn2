local UIInput = import "UIInput"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local GameObject = import "UnityEngine.GameObject"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local CUIFx = import "L10.UI.CUIFx"
local UILabel = import "UILabel"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CItemMgr = import "L10.Game.CItemMgr"
local CButton = import "L10.UI.CButton"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CCarnivalLotteryMgr = import "L10.Game.CCarnivalLotteryMgr"
local TweenPosition = import "TweenPosition"

LuaYuanXiao2022DengMiWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaYuanXiao2022DengMiWnd, "RightCountDownLabel", "RightCountDownLabel", UILabel)
RegistChildComponent(LuaYuanXiao2022DengMiWnd, "LeftCountDownLabel", "LeftCountDownLabel", UILabel)
RegistChildComponent(LuaYuanXiao2022DengMiWnd, "SwitchFx", "SwitchFx", CUIFx)
RegistChildComponent(LuaYuanXiao2022DengMiWnd, "TopBarrageView", "TopBarrageView", GameObject)
RegistChildComponent(LuaYuanXiao2022DengMiWnd, "BottomBarrageView", "BottomBarrageView", GameObject)
RegistChildComponent(LuaYuanXiao2022DengMiWnd, "Item1", "Item1", GameObject)
RegistChildComponent(LuaYuanXiao2022DengMiWnd, "Item2", "Item2", GameObject)
RegistChildComponent(LuaYuanXiao2022DengMiWnd, "CostAndOwnMoney", "CostAndOwnMoney", CCurentMoneyCtrl)
RegistChildComponent(LuaYuanXiao2022DengMiWnd, "ChaiButton", "ChaiButton", GameObject)
RegistChildComponent(LuaYuanXiao2022DengMiWnd, "TipButton", "TipButton", GameObject)
RegistChildComponent(LuaYuanXiao2022DengMiWnd, "QuestionIndexLabel", "QuestionIndexLabel", UILabel)
RegistChildComponent(LuaYuanXiao2022DengMiWnd, "QuestionLabel", "QuestionLabel", UILabel)
RegistChildComponent(LuaYuanXiao2022DengMiWnd, "OkButton", "OkButton", CButton)
RegistChildComponent(LuaYuanXiao2022DengMiWnd, "TopRightLabel", "TopRightLabel", UILabel)
RegistChildComponent(LuaYuanXiao2022DengMiWnd, "LeftResultView", "LeftResultView", GameObject)
RegistChildComponent(LuaYuanXiao2022DengMiWnd, "RewardItemLabel", "RewardItemLabel", UILabel)
RegistChildComponent(LuaYuanXiao2022DengMiWnd, "SendBarrageButton", "SendBarrageButton", GameObject)
RegistChildComponent(LuaYuanXiao2022DengMiWnd, "BottomRightLabel", "BottomRightLabel", UILabel)
RegistChildComponent(LuaYuanXiao2022DengMiWnd, "AnswerInput", "AnswerInput", UIInput)
RegistChildComponent(LuaYuanXiao2022DengMiWnd, "RightResultView", "RightResultView", GameObject)
RegistChildComponent(LuaYuanXiao2022DengMiWnd, "RightAnswerView", "RightAnswerView", GameObject)
RegistChildComponent(LuaYuanXiao2022DengMiWnd, "RightResultLabel", "RightResultLabel", UILabel)
RegistChildComponent(LuaYuanXiao2022DengMiWnd, "Template", "Template", GameObject)
RegistChildComponent(LuaYuanXiao2022DengMiWnd, "DengLongFx", "DengLongFx", CUIFx)
RegistChildComponent(LuaYuanXiao2022DengMiWnd, "GuangQuanFx1", "GuangQuanFx1", CUIFx)
RegistChildComponent(LuaYuanXiao2022DengMiWnd, "GuangQuanFx2", "GuangQuanFx2", CUIFx)
RegistChildComponent(LuaYuanXiao2022DengMiWnd, "TrailFx1", "TrailFx1", CUIFx)
RegistChildComponent(LuaYuanXiao2022DengMiWnd, "TrailFx2", "TrailFx2", CUIFx)
RegistChildComponent(LuaYuanXiao2022DengMiWnd, "CountDownDen", "CountDownDen", UISlider)
RegistChildComponent(LuaYuanXiao2022DengMiWnd, "RightView", "RightView", GameObject)
--@endregion RegistChildComponent end
RegistClassMember(LuaYuanXiao2022DengMiWnd, "m_BarragesQueue")
RegistClassMember(LuaYuanXiao2022DengMiWnd, "m_TrailFxTick")
RegistClassMember(LuaYuanXiao2022DengMiWnd, "m_UpdateTimeTick")
RegistClassMember(LuaYuanXiao2022DengMiWnd, "m_UpdateQuestionTick")
function LuaYuanXiao2022DengMiWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.ChaiButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnChaiButtonClick()
	end)


	
	UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)


	
	UIEventListener.Get(self.OkButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOkButtonClick()
	end)


	
	UIEventListener.Get(self.SendBarrageButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSendBarrageButtonClick()
	end)


    --@endregion EventBind end
end

function LuaYuanXiao2022DengMiWnd:Init()
    self.m_BarragesQueue = {}
    for i = 1, 10 do
        self.m_BarragesQueue[i] = {}
    end
    self.CostAndOwnMoney:SetType(EnumMoneyType.LingYu,EnumPlayScoreKey.NONE,true)
    self.CostAndOwnMoney:SetCost(YuanXiao_Setting.GetData().OpenLanternCost)
    self:InitChaiHuaDengItems()
    self.Template.gameObject:SetActive(false)
    self:RefreshLeftAndRightView()
    self:CancelUpdateTimeTick()
    self:UpdateTimeLabel()
	self.m_UpdateTimeTick = RegisterTick(function ()
		self:UpdateTimeLabel()
        local t2 = LuaYuanXiao2022Mgr.m_EndDengMiPlayTime - CServerTimeMgr.Inst.timeStamp
        if t2 <= 0 then
            g_MessageMgr:ShowMessage("YuanXiao2022DengMiWnd_End")
            CUIManager.CloseUI(CLuaUIResources.YuanXiao2022DengMiWnd)
        end
	end, 500)
    --self.DengLongFx:LoadFx("fx/ui/prefab/UI_dengmi_denglong.prefab")
end

function LuaYuanXiao2022DengMiWnd:InitChaiHuaDengItems()
    local itemIdArray = {YuanXiao_Setting.GetData().OpenLanternItemID1,YuanXiao_Setting.GetData().OpenLanternItemID2}
    local itemArray = {self.Item1, self.Item2}
    for i = 1,2 do
        local itemId = itemIdArray[i]
        local data = Item_Item.GetData(itemId)
        local item = itemArray[i]
        UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            CItemInfoMgr.ShowLinkItemTemplateInfo(itemId)
        end)
        item.transform:Find("IconTexture"):GetComponent(typeof(CUITexture)):LoadMaterial(data.Icon)
    end
end

function LuaYuanXiao2022DengMiWnd:RefreshLeftAndRightView()
    self.RightView.gameObject:SetActive(true)
    self.CostAndOwnMoney.gameObject:SetActive(LuaYuanXiao2022Mgr.m_CurChaiHuaDengResult == nil)
    self.ChaiButton.gameObject:SetActive(LuaYuanXiao2022Mgr.m_CurChaiHuaDengResult == nil)
    self.LeftResultView.gameObject:SetActive(LuaYuanXiao2022Mgr.m_CurChaiHuaDengResult ~= nil)
    if LuaYuanXiao2022Mgr.m_CurChaiHuaDengResult then
        local s = ""
        local len = #LuaYuanXiao2022Mgr.m_CurChaiHuaDengResult
        for i = 1,len do
            local t = LuaYuanXiao2022Mgr.m_CurChaiHuaDengResult[i]
            local data = Item_Item.GetData(t.itemId) 
            local num = t.num
            s = SafeStringFormat3(LocalString.GetString("%s%s*%d%s"),s, data.Name,num, i < len and "\n" or "")
        end
        self.RewardItemLabel.text = s
    end

    self.RightAnswerView.gameObject:SetActive(not LuaYuanXiao2022Mgr.m_CurDengMiIsCorrect)
    self.RightResultView.gameObject:SetActive(LuaYuanXiao2022Mgr.m_CurDengMiIsCorrect)
    self.BottomRightLabel.text = LuaYuanXiao2022Mgr.m_CurDengMiIsCorrect and LocalString.GetString("将随机发送正/误答案") or
        SafeStringFormat3(LocalString.GetString("剩余奖励次数%s(使用猜灯谜劵获得)"),CCarnivalLotteryMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eRiddle2022GuessRemainTimes))
    self.OkButton.Text = LuaYuanXiao2022Mgr.m_CurDengMiIsCorrect and LocalString.GetString("秀出答案") or LocalString.GetString("确定")
    local data = YuanXiao_QuestionPool2.GetData(LuaYuanXiao2022Mgr.m_CurDengMiId)
    if data then
        self.RightResultLabel.text = SafeStringFormat3(LocalString.GetString("恭喜你猜中正确答案: %s"),data.Answer)
        self.QuestionLabel.text = data.Question
    end
    self.TopRightLabel.text = (CCarnivalLotteryMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eRiddle2022TitleRewardTimes) > 0) and
        SafeStringFormat3(LocalString.GetString("已获得称号【%s】"),Title_Title.GetData(YuanXiao_Setting.GetData().TitleOfDengMiID).Name) or
        SafeStringFormat3(LocalString.GetString("累积答对%d/%d次可获得称号"),CCarnivalLotteryMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eRiddle2022GuessCorrectTimes),YuanXiao_Setting.GetData().TitleNeedCorrect)
    self.QuestionIndexLabel.text = SafeStringFormat3(LocalString.GetString("题号%02d/%02d"),LuaYuanXiao2022Mgr.m_CurDengMiIndex,LuaYuanXiao2022Mgr.m_DengMiCount)
end

function LuaYuanXiao2022DengMiWnd:OnEnable()
    g_ScriptEvent:AddListener("YuanXiao2022DengMiWnd_ShowBarrage", self, "ShowBarrage")
    g_ScriptEvent:AddListener("UpdateYuanXiaoRiddle2022Question", self, "UpdateYuanXiaoRiddle2022Question")
    g_ScriptEvent:AddListener("SendDanMu",self,"OnSendDanMu") 
    g_ScriptEvent:AddListener("UpdateTempPlayTimesWithKey", self, "OnUpdateTempPlayTimesWithKey")
    g_ScriptEvent:AddListener("OnSwitchYuanXiaoRiddle2022Question", self, "OnSwitchYuanXiaoRiddle2022Question")
    g_ScriptEvent:AddListener("UpdateYuanXiaoRiddle2022OpenStatus", self, "UpdateYuanXiaoRiddle2022OpenStatus")
end

function LuaYuanXiao2022DengMiWnd:OnDisable()
    g_ScriptEvent:RemoveListener("YuanXiao2022DengMiWnd_ShowBarrage", self, "ShowBarrage")
    g_ScriptEvent:RemoveListener("UpdateYuanXiaoRiddle2022Question", self, "UpdateYuanXiaoRiddle2022Question")
    g_ScriptEvent:RemoveListener("SendDanMu",self,"OnSendDanMu") 
    g_ScriptEvent:RemoveListener("UpdateTempPlayTimesWithKey", self, "OnUpdateTempPlayTimesWithKey")
    g_ScriptEvent:RemoveListener("OnSwitchYuanXiaoRiddle2022Question", self, "OnSwitchYuanXiaoRiddle2022Question")
    g_ScriptEvent:RemoveListener("UpdateYuanXiaoRiddle2022OpenStatus", self, "UpdateYuanXiaoRiddle2022OpenStatus")
    self:CancelUpdateTimeTick()
    self:CancelTrailFxTick()
    self:CancelUpdateQuestionTick()
    LuaTweenUtils.DOKill(self.transform, false)
    Gac2Gas.RequestCloseYuanXiaoRiddle2022Wnd()
end

function LuaYuanXiao2022DengMiWnd:UpdateYuanXiaoRiddle2022OpenStatus()
    self:RefreshLeftAndRightView()
    self.TrailFx1:LoadFx("fx/ui/prefab/UI_dengmi_Trail.prefab")
    self.TrailFx2:LoadFx("fx/ui/prefab/UI_dengmi_Trail.prefab")
    local tween1 = self.TrailFx1.transform:GetComponent(typeof(TweenPosition))
    local tween2 = self.TrailFx2.transform:GetComponent(typeof(TweenPosition))
    tween1:ResetToBeginning()
    tween1:PlayForward()
    tween2:ResetToBeginning()
    tween2:PlayForward()
    self:CancelTrailFxTick()
    self.m_TrailFxTick = RegisterTickOnce(function ()
        self.GuangQuanFx1:DestroyFx()
        self.GuangQuanFx1:LoadFx("fx/ui/prefab/UI_dengmi_guangquan01.prefab")
        self.GuangQuanFx2:DestroyFx()
        self.GuangQuanFx2:LoadFx("fx/ui/prefab/UI_dengmi_guangquan02.prefab")
    end,800)
end

function LuaYuanXiao2022DengMiWnd:OnSwitchYuanXiaoRiddle2022Question()
    self.SwitchFx:DestroyFx()
    self.SwitchFx:LoadFx("fx/ui/prefab/UI_dengmi_jiemian.prefab")
    self.RightView.gameObject:SetActive(false)
end

function LuaYuanXiao2022DengMiWnd:OnUpdateTempPlayTimesWithKey(args)
    local key = args[0]
    if key == EnumPlayTimesKey_lua.eRiddle2022GuessRemainTimes then
        self.BottomRightLabel.text = LuaYuanXiao2022Mgr.m_CurDengMiIsCorrect and LocalString.GetString("将随机发送正/误答案") or
        SafeStringFormat3(LocalString.GetString("剩余奖励次数%s(使用猜灯谜劵获得)"),CCarnivalLotteryMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eRiddle2022GuessRemainTimes))
    elseif key == EnumPlayTimesKey_lua.eRiddle2022TitleRewardTimes or key == EnumPlayTimesKey_lua.eRiddle2022GuessCorrectTimes then
        self.TopRightLabel.text = (CCarnivalLotteryMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eRiddle2022TitleRewardTimes) > 0) and
        SafeStringFormat3(LocalString.GetString("已获得称号【%s】"),Title_Title.GetData(YuanXiao_Setting.GetData().TitleOfDengMiID).Name) or
        SafeStringFormat3(LocalString.GetString("累积答对%d/%d次可获得称号"),CCarnivalLotteryMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eRiddle2022GuessCorrectTimes),YuanXiao_Setting.GetData().TitleNeedCorrect)
    end
end

function LuaYuanXiao2022DengMiWnd:OnSendDanMu(args)
    if args.Length ~= 3 then return end
    local strs, ids, key = args[0],args[1],args[2]
    if strs.Count ~= ids.Count then return end
    local isAnswerBarrage = key == "Riddle2022Guess"
    for i = 0,strs.Count - 1 do
        local playerId = ids[i]
        local text = strs[i]
        local barragePos = math.random(1,8)
        local isMyBarrage = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == playerId
        self:ShowBarrage(isAnswerBarrage, text, isMyBarrage, barragePos)
    end
end

function LuaYuanXiao2022DengMiWnd:UpdateYuanXiaoRiddle2022Question()
    self:CancelUpdateQuestionTick()
    self.m_UpdateQuestionTick = RegisterTickOnce(function()
        self:RefreshLeftAndRightView()
    end,500)
end

function LuaYuanXiao2022DengMiWnd:ShowBarrage(isAnswerBarrage, text, isMyBarrage, barragePos)
    local obj = NGUITools.AddChild(barragePos <= 4 and self.TopBarrageView.gameObject or self.BottomBarrageView.gameObject, self.Template.gameObject)
    obj.gameObject:SetActive(true)
    obj.transform:Find("Answer").gameObject:SetActive(isAnswerBarrage)
    obj.transform:Find("Chai").gameObject:SetActive(not isAnswerBarrage)
    local textLabel = obj.transform:Find("Label"):GetComponent(typeof(UILabel))
    local myWidget = obj.transform:Find("My"):GetComponent(typeof(UIWidget))

    local initialRoot = (barragePos <= 4 and self.TopBarrageView or self.BottomBarrageView).transform:GetChild(barragePos - ((barragePos <= 4) and 1 or 5))
    local initialPosX = initialRoot.transform.localPosition.x
    local initialPosY = initialRoot.transform.localPosition.y
    local startMovePosX = initialPosX
    local endMovePosX = - initialPosX - myWidget.width
    local duration = 10
    local dX = (startMovePosX - endMovePosX) / duration

    --调整弹幕长度
    textLabel.text = CUICommonDef.TranslateToNGUIText(text)
    myWidget:Update()
    myWidget.gameObject:SetActive(isMyBarrage)
    endMovePosX = - initialPosX - myWidget.width
    --新的弹幕要保证在同行弹幕后面
    if #self.m_BarragesQueue[barragePos] > 0 then 
        local lastObj = self.m_BarragesQueue[barragePos][#self.m_BarragesQueue[barragePos]]
        local lastWidget = lastObj.transform:Find("My"):GetComponent(typeof(UIWidget))
        startMovePosX = math.max(startMovePosX, lastObj.transform.localPosition.x + lastWidget.width + 10)
    end
    duration = (startMovePosX - endMovePosX) / dX
    --弹幕移动动画
    obj.transform.localPosition = Vector3(startMovePosX, initialPosY, 0)
    local tweener = LuaTweenUtils.TweenFloat(startMovePosX, endMovePosX, duration, function (val)
		obj.transform.localPosition = Vector3(val, initialPosY, 0)
	end)
    LuaTweenUtils.OnComplete(tweener, function ()

        --弹幕销毁
        if self.m_BarragesQueue[barragePos][1] and obj == self.m_BarragesQueue[barragePos][1] then
            table.remove(self.m_BarragesQueue[barragePos],1)
        end
        GameObject.Destroy(obj)
	end)
    LuaTweenUtils.SetTarget(tweener,self.transform)
    table.insert(self.m_BarragesQueue[barragePos], obj)
end

function LuaYuanXiao2022DengMiWnd:CancelUpdateTimeTick()
	if self.m_UpdateTimeTick then
		UnRegisterTick(self.m_UpdateTimeTick)
		self.m_UpdateTimeTick = nil
	end
end

function LuaYuanXiao2022DengMiWnd:CancelTrailFxTick()
	if self.m_TrailFxTick then
		UnRegisterTick(self.m_TrailFxTick)
		self.m_TrailFxTick = nil
	end
end

function LuaYuanXiao2022DengMiWnd:CancelUpdateQuestionTick()
	if self.m_UpdateQuestionTick then
		UnRegisterTick(self.m_UpdateQuestionTick)
		self.m_UpdateQuestionTick = nil
	end
end

--@region UIEvent
function LuaYuanXiao2022DengMiWnd:OnChaiButtonClick()
    Gac2Gas.RequestOpenRiddle2022Lantern(LuaYuanXiao2022Mgr.m_CurDengMiId)
end


function LuaYuanXiao2022DengMiWnd:OnTipButtonClick()
    g_MessageMgr:ShowMessage("YuanXiao2022DengMiWnd_ReadMe")
end


function LuaYuanXiao2022DengMiWnd:OnSendBarrageButtonClick()
    Gac2Gas.SendRiddle2022AskHelpDanMu(LuaYuanXiao2022Mgr.m_CurDengMiId)
end

function LuaYuanXiao2022DengMiWnd:OnOkButtonClick()
    if LuaYuanXiao2022Mgr.m_CurDengMiIsCorrect then
        Gac2Gas.SendRiddle2022GuessedDanMu(LuaYuanXiao2022Mgr.m_CurDengMiId)
        return
    end
    local text = self.AnswerInput.value
    if System.String.IsNullOrEmpty(text) then
        g_MessageMgr:ShowMessage("YuanXiao2022DengMiWnd_NoneAnswer")
        return
    end
    if CCarnivalLotteryMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eRiddle2022GuessRemainTimes) == 0 then
        local itemId = YuanXiao_Setting.GetData().RiddleTicketItemId
	    local default, pos, itemId = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, itemId)
        if default then
            local msg = g_MessageMgr:FormatMessage("Valentine2022DriftBottleWnd_GuessConfirm")
            MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
                Gac2Gas.RequestGuessRiddle2022Question(LuaYuanXiao2022Mgr.m_CurDengMiId, text, true)
            end),nil,nil,nil,false)
        else
            g_MessageMgr:ShowMessage("YuanXiao2022DengMiWnd_NoneRewardTimes")
        end
        return
    end
    Gac2Gas.RequestGuessRiddle2022Question(LuaYuanXiao2022Mgr.m_CurDengMiId, text, false)
end


--@endregion UIEvent

function LuaYuanXiao2022DengMiWnd:UpdateTimeLabel()
    local t1 = LuaYuanXiao2022Mgr.m_EndCurDengMiTime - CServerTimeMgr.Inst.timeStamp
    local t2 = LuaYuanXiao2022Mgr.m_EndDengMiPlayTime - CServerTimeMgr.Inst.timeStamp
    if t1 <= 30 then
        self.LeftCountDownLabel.text = math.floor(t1)
        if t1 <= 0 then self.LeftCountDownLabel.text = "" end
        self.CountDownDen.value = 1 - t1 / 30
    end
    if t2 > 0 then
        self.RightCountDownLabel.text = SafeStringFormat3(LocalString.GetString("本次集市剩余%d分%d秒"),math.floor(t2 % 3600 / 60),t2 % 60)
    end
end
