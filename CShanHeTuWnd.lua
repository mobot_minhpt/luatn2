-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CCoroutineMgr = import "L10.Engine.CCoroutineMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CPos = import "L10.Engine.CPos"
local CShanHeTuMgr = import "L10.UI.CShanHeTuMgr"
local CShanHeTuWnd = import "L10.UI.CShanHeTuWnd"
local CTickMgr = import "L10.Engine.CTickMgr"
local CTrackMgr = import "L10.Game.CTrackMgr"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local DelegateFactory = import "DelegateFactory"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local ETickType = import "L10.Engine.ETickType"
local Extensions = import "Extensions"
local GameplayItem_YiZhangLocations = import "L10.Game.GameplayItem_YiZhangLocations"
local PublicMap_PublicMap = import "L10.Game.PublicMap_PublicMap"
local UInt32 = import "System.UInt32"
CShanHeTuWnd.m_Init_CS2LuaHook = function (this) 

    this:CancelDelayCloseTick()
    this.itemId = CShanHeTuMgr.Inst.ItemId
    local item = CItemMgr.Inst:GetById(this.itemId)
    local pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, this.itemId)
    if pos == 0 or item == nil or not CommonDefs.HashSetContains(CShanHeTuMgr.Inst.ShanHeTuTemplateId_V2, typeof(UInt32), item.TemplateId) then
        this:Close()
        return
    end

    local locInfo = GameplayItem_YiZhangLocations.GetData(item.TemplateId)
    this.itemTemplateId = locInfo.ItemID
    this.sceneTemplateId = locInfo.PublicMapID
    this:InitMiniMap(locInfo.PublicMapID)
end
CShanHeTuWnd.m_InitMiniMap_CS2LuaHook = function (this, sceneTemplateId) 

    this:CancelLoadMiniMapCoroutine()
    this.mapTexture.mainTexture = nil
    Extensions.RemoveAllChildren(this.mapTexture.transform)
    CommonDefs.ListClear(this.items)
    local data = PublicMap_PublicMap.GetData(sceneTemplateId)
    if data ~= nil then
        this.m_LoadMiniMapCoroutine = CCoroutineMgr.Inst:StartCoroutine(this:LoadMiniMap(data.Res))
    end
end
CShanHeTuWnd.m_CancelLoadMiniMapCoroutine_CS2LuaHook = function (this) 

    if this.m_LoadMiniMapCoroutine ~= nil then
        CCoroutineMgr.Inst:CancelCoroutine(this.m_LoadMiniMapCoroutine)
        this.m_LoadMiniMapCoroutine = nil
    end
end
CShanHeTuWnd.m_OnItemClick_CS2LuaHook = function (this, target) 
    do
        local i = 0
        while i < this.items.Count do
            local item = this.items[i]
            item.Selected = (target == item)
            if target == item then
                item:DoAni()
                this:CancelDelayCloseTick()
                CShanHeTuMgr.Inst:FindTreasure(this.itemId, this.sceneTemplateId, target.PosX, target.PosY)
                this.cancelDelayCloseTick = CTickMgr.Register(DelegateFactory.Action(function () 
                    this:DelayCloseAction()
                end), this.delaySeconds * 1000, ETickType.Once)
            else
                item:StopAni()
            end
            i = i + 1
        end
    end
end
CShanHeTuWnd.m_CancelDelayCloseTick_CS2LuaHook = function (this) 
    if this.cancelDelayCloseTick ~= nil then
        invoke(this.cancelDelayCloseTick)
        this.cancelDelayCloseTick = nil
    end
end

CShanHeTuMgr.m_Show_CS2LuaHook = function (this, itemId) 

    this.m_ItemId = itemId
    CUIManager.ShowUI(CUIResources.ShanHeTuWnd)
    --关闭tips和包裹窗口
    CItemInfoMgr.CloseItemInfoWnd()
    CUIManager.CloseUI(CUIResources.PackageWnd)
end
CShanHeTuMgr.m_FindTreasure_CS2LuaHook = function (this, itemId, sceneTemplateId, posX, posY) 

    if CShanHeTuMgr.EnableTeleportTracking then
        local pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, itemId)
        if pos > 0 then
            local gridPos = CreateFromClass(CPos, posX, posY)
            CTrackMgr.Inst:YiZhangV2Track(EnumItemPlace_lua.Bag, pos, itemId, nil, sceneTemplateId, gridPos, 0, DelegateFactory.Action(function () 
                pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, itemId)
                if pos > 0 then
                    Gac2Gas.OpenYiZhangShanHeTuV2(EnumItemPlace_lua.Bag, pos, itemId)
                end
            end), nil)
        end
    else
        CTrackMgr.Inst:FindLocation(nil, sceneTemplateId, posX, posY, DelegateFactory.Action(function () 
            local pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, itemId)
            if pos > 0 then
                Gac2Gas.OpenYiZhangShanHeTuV2(EnumItemPlace_lua.Bag, pos, itemId)
            end
        end), nil, nil, 0)
    end
end
CShanHeTuMgr.m_TryOpenNextShanHeTu_CS2LuaHook = function (this, previousItemId, previousTemplateId) 

    local itemId = nil
    local pos = 0
    local default
    default, itemId, pos = this:TryGetFirstShanHeTu(previousItemId, previousTemplateId)
    if default then
        Gac2Gas.RequestPreCheckYiZhangV2(EnumItemPlace_lua.Bag, pos, itemId)
    end
end
CShanHeTuMgr.m_TryGetFirstShanHeTu_CS2LuaHook = function (this, previousItemId, previousTemplateId, itemId, pos) 
    itemId = nil
    pos = 0
    if CClientMainPlayer.Inst == nil then
        return false, itemId, pos
    end
    local itemProp = CClientMainPlayer.Inst.ItemProp

    local findPos = itemProp:GetItemPos(EnumItemPlace.Bag, previousItemId)
    if findPos > 0 then
        itemId = previousItemId
        pos = findPos
        return true, itemId, pos
    end
    local found = false
    local tmpItemId = nil
    local tmpPos = 0
    local size = itemProp:GetPlaceSize(EnumItemPlace.Bag)
    do
        local i = 1
        while i <= size do
            local continue
            repeat
                local id = itemProp:GetItemAt(EnumItemPlace.Bag, i)
                if System.String.IsNullOrEmpty(id) then
                    continue = true
                    break
                end
                local item = CItemMgr.Inst:GetById(id)
                if item ~= nil and item.IsItem and CommonDefs.HashSetContains(this.ShanHeTuTemplateId_V2, typeof(UInt32), item.TemplateId) then
                    if item.TemplateId == previousTemplateId then
                        itemId = id
                        pos = i
                        return true, itemId, pos
                    end
                    if not found then
                        found = true
                        tmpItemId = id
                        tmpPos = i
                    end
                end
                continue = true
            until 1
            if not continue then
                break
            end
            i = i + 1
        end
    end
    if found then
        itemId = tmpItemId
        pos = tmpPos
    end
    return found, itemId, pos
end
