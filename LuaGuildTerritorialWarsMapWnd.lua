local UITable = import "UITable"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CMiniMapRoutingWnd = import "L10.UI.CMiniMapRoutingWnd"
local CPos = import "L10.Engine.CPos"
local Utility = import "L10.Engine.Utility"
local CTrackMgr = import "L10.Game.CTrackMgr"
local TweenAlpha = import "TweenAlpha"
local CButton = import "L10.UI.CButton"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local TouchPhase = import "UnityEngine.TouchPhase"
local CResourceMgr = import "L10.Engine.CResourceMgr"
local CMiniMap = import "L10.UI.CMiniMap"
local Input = import "UnityEngine.Input"
local Physics = import "UnityEngine.Physics"
local KeyCode = import "UnityEngine.KeyCode"
local CMapPathDrawer = import "L10.UI.CMapPathDrawer"
local CGuideMgr=import "L10.Game.Guide.CGuideMgr"
local GestureRecognizer = import "L10.Engine.GestureRecognizer"
local GestureType = import "L10.Engine.GestureType"
local QnCheckBox = import "L10.UI.QnCheckBox"
local DateTime = import "System.DateTime"
local PlayerPrefs = import "UnityEngine.PlayerPrefs"
local UIBasicSprite = import "UIBasicSprite"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"

LuaGuildTerritorialWarsMapWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "SwitchButton", "SwitchButton", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "RoutingButton", "RoutingButton", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "TipButton", "TipButton", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "InfoButton", "InfoButton", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "ResetButton", "ResetButton", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "TopLeftTable", "TopLeftTable", UITable)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "BottomLeftTable", "BottomLeftTable", UITable)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "RightViewTable", "RightViewTable", UITable)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "LegendButton", "LegendButton", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "BigMapView", "BigMapView", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "SmallMapView", "SmallMapView", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "BigMapRoot", "BigMapRoot", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "GridTemplate", "GridTemplate", UIWidget)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "BigMapBottomLeftRoot", "BigMapBottomLeftRoot", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "ScaleSlider", "ScaleSlider", UISlider)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "Map", "Map", CUITexture)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "IncreaseScaleButton", "IncreaseScaleButton", CButton)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "ReduceScaleButton", "ReduceScaleButton", CButton)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "AxisNumTemplate", "AxisNumTemplate", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "BottomLeftAxisPos", "BottomLeftAxisPos", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "BottomAxis", "BottomAxis", UIPanel)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "LeftAxis", "LeftAxis", UIPanel)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "Axis", "Axis", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "BigMapViewTopLabel", "BigMapViewTopLabel", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "SituatuionButton", "SituatuionButton", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "RebackCityButton", "RebackCityButton", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "RebackGuildButton", "RebackGuildButton", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "MyOccupyNumLabel", "MyOccupyNumLabel", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "SmallMapSituationView", "SmallMapSituationView", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "HideSmallMapSituationView", "HideSmallMapSituationView", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "MapInfoViewBg", "MapInfoViewBg", UIWidget)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "SmallMapSituationViewArrow1", "SmallMapSituationViewArrow1", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "SmallMapSituationViewArrow2", "SmallMapSituationViewArrow2", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "OccupySlider", "OccupySlider", UISlider)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "OccupyProgressLabel", "OccupyProgressLabel", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "MarkTemplate", "MarkTemplate", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "SmallMapRoot", "SmallMapRoot", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "SmallMapTexture", "SmallMapTexture", UITexture)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "SmallMapTipFx", "SmallMapTipFx", CUIFx)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "SmallMapTipFx2", "SmallMapTipFx2", CUIFx)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "TeleportMarkTemplate", "TeleportMarkTemplate", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "BigMapCenter", "BigMapCenter", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "MapPathDrawer", "MapPathDrawer", CMapPathDrawer)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "MainPlayerSpirite", "MainPlayerSpirite", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "CloudFx", "CloudFx", CUIFx)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "MapPanel", "MapPanel", UIPanel)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "ColorEnemyAreaQnCheckBox", "ColorEnemyAreaQnCheckBox", QnCheckBox)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "OccupyNumTipButton", "OccupyNumTipButton", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "GuildTerritorialWarsMapRankView", "GuildTerritorialWarsMapRankView", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "MonsterTemplate", "MonsterTemplate", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "CommandButton", "CommandButton", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "TablePanel", "TablePanel", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "GuildTerritorialWarsDrawMapPanel", "GuildTerritorialWarsDrawMapPanel", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "CloseButton", "CloseButton", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "ChallengeClearTimeView", "GuildTerritorialWarsMapChallengeClearTimeView", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "RebackOriginButton", "RebackOriginButton", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "RebackBattlegroundButton", "RebackBattlegroundButton", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsMapWnd, "BossIconTexture", "BossIconTexture", CUITexture)
--@endregion RegistChildComponent end
RegistClassMember(LuaGuildTerritorialWarsMapWnd, "m_BigMapGridData")
RegistClassMember(LuaGuildTerritorialWarsMapWnd, "m_PosIndex2GridId")
RegistClassMember(LuaGuildTerritorialWarsMapWnd, "m_RadioBoxIndex2GridId")
RegistClassMember(LuaGuildTerritorialWarsMapWnd, "m_BigMapXSize")
RegistClassMember(LuaGuildTerritorialWarsMapWnd, "m_BigMapYSize")
RegistClassMember(LuaGuildTerritorialWarsMapWnd, "m_GridBorderSpan")
RegistClassMember(LuaGuildTerritorialWarsMapWnd, "m_JunTianZhuChengId")
RegistClassMember(LuaGuildTerritorialWarsMapWnd, "m_CenterGridId")
RegistClassMember(LuaGuildTerritorialWarsMapWnd, "m_BigMapScale")
RegistClassMember(LuaGuildTerritorialWarsMapWnd, "m_BigMapMaxScale")
RegistClassMember(LuaGuildTerritorialWarsMapWnd, "m_IsShowMostInfo")
RegistClassMember(LuaGuildTerritorialWarsMapWnd, "m_GridWidth")
RegistClassMember(LuaGuildTerritorialWarsMapWnd, "m_GridHeight")
RegistClassMember(LuaGuildTerritorialWarsMapWnd, "m_XAxisFigureObjectsList")
RegistClassMember(LuaGuildTerritorialWarsMapWnd, "m_YAxisFigureObjectsList")
RegistClassMember(LuaGuildTerritorialWarsMapWnd, "m_MapRadioBox")
RegistClassMember(LuaGuildTerritorialWarsMapWnd, "m_UpdateMapTimeTick")
RegistClassMember(LuaGuildTerritorialWarsMapWnd, "m_RequestMapTick")
RegistClassMember(LuaGuildTerritorialWarsMapWnd, "m_IsInitForCenterGrid")
RegistClassMember(LuaGuildTerritorialWarsMapWnd, "m_IsDragging")
RegistClassMember(LuaGuildTerritorialWarsMapWnd, "m_LastDragPos")
RegistClassMember(LuaGuildTerritorialWarsMapWnd, "m_FingerIndex")
RegistClassMember(LuaGuildTerritorialWarsMapWnd, "m_MapCachePos")
RegistClassMember(LuaGuildTerritorialWarsMapWnd, "m_TeleportList")
RegistClassMember(LuaGuildTerritorialWarsMapWnd, "m_IsInitGridItems")
RegistClassMember(LuaGuildTerritorialWarsMapWnd, "m_QueryMyMapPosInfoTick")
RegistClassMember(LuaGuildTerritorialWarsMapWnd, "m_IsPlayTeleportFailedFx")
RegistClassMember(LuaGuildTerritorialWarsMapWnd, "m_ShowPlayTeleportFailedFxTick")
RegistClassMember(LuaGuildTerritorialWarsMapWnd, "m_IsPlayTeleportFailedFx")
RegistClassMember(LuaGuildTerritorialWarsMapWnd, "m_PlayerMarkList")
RegistClassMember(LuaGuildTerritorialWarsMapWnd, "m_EnemyPlayerMarkList")
RegistClassMember(LuaGuildTerritorialWarsMapWnd, "m_IsShowJiantouFx")
RegistClassMember(LuaGuildTerritorialWarsMapWnd, "m_OnPinchInDelegate")
RegistClassMember(LuaGuildTerritorialWarsMapWnd, "m_OnPinchOutDelegate")
RegistClassMember(LuaGuildTerritorialWarsMapWnd, "m_IsPaintingColorEnemyArea")
RegistClassMember(LuaGuildTerritorialWarsMapWnd, "m_MonsterItems")
RegistClassMember(LuaGuildTerritorialWarsMapWnd, "m_IsShowCommandButton")
RegistClassMember(LuaGuildTerritorialWarsMapWnd, "m_IsDrawingMap")

function LuaGuildTerritorialWarsMapWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	UIEventListener.Get(self.SwitchButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSwitchButtonClick()
	end)
	UIEventListener.Get(self.RoutingButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRoutingButtonClick()
	end)
	UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)
	UIEventListener.Get(self.LegendButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLegendButtonClick()
	end)
	UIEventListener.Get(self.IncreaseScaleButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnIncreaseScaleButtonClick()
	end)
	UIEventListener.Get(self.ReduceScaleButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnReduceScaleButtonClick()
	end)
	UIEventListener.Get(self.SituatuionButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSituatuionButtonClick()
	end)
	UIEventListener.Get(self.RebackCityButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRebackCityButtonClick()
	end)
	UIEventListener.Get(self.RebackGuildButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRebackGuildButtonClick()
	end)
	UIEventListener.Get(self.SmallMapSituationViewArrow1.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSmallMapSituationViewArrowClick()
	end)
	UIEventListener.Get(self.SmallMapSituationViewArrow2.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSmallMapSituationViewArrowClick()
	end)
	UIEventListener.Get(self.ResetButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnResetButtonClick()
	end)
	UIEventListener.Get(self.Map.gameObject).onDragStart = DelegateFactory.VoidDelegate(function (go)
		self:OnDragMapStart()
	end)
	UIEventListener.Get(self.SmallMapTexture.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
		self:OnSmallMapTextureClick()
	end)
	UIEventListener.Get(self.OccupyNumTipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
		self:OnOccupyNumTipButtonClick()
	end)
	UIEventListener.Get(self.CommandButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
		self:OnCommandButtonClick()
	end)
	UIEventListener.Get(self.CloseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
		self:OnCloseButtonClick()
	end)
	UIEventListener.Get(self.RebackOriginButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
		self:OnRebackOriginButtonClick()
	end)
	UIEventListener.Get(self.RebackBattlegroundButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
		self:OnRebackBattlegroundButtonClick()
	end)
	self.ColorEnemyAreaQnCheckBox.OnValueChanged = DelegateFactory.Action_bool(function (value)
		self:OnColorEnemyAreaQnCheckBoxValueChanged(value)
	end)
	self.ColorEnemyAreaQnCheckBox:SetSelected(true, false)
    --@endregion EventBind end
end

function LuaGuildTerritorialWarsMapWnd:OnEnable()
	g_ScriptEvent:AddListener("GuildTerritorialWarsMap_TryToTeleport", self, "TryToTeleport")
	g_ScriptEvent:AddListener("OnSendTerritoryWarMap_GridId2Level", self, "OnSendGridId2Level")
	g_ScriptEvent:AddListener("OnSendTerritoryWarMapDetail", self, "OnSendTerritoryWarMapDetail")
	g_ScriptEvent:AddListener("UpdateObjPosInfo", self, "UpdateObjPosInfo")
	g_ScriptEvent:AddListener("QueryEnemyPlayerPosResult", self, "QueryEnemyPlayerPosResult")
	g_ScriptEvent:AddListener("RenderSceneInit", self, "OnRenderSceneInit")
	g_ScriptEvent:AddListener("MouseScrollWheel",self,"OnMouseScrollWheel")
	g_ScriptEvent:AddListener("OnSyncTerritoryWarPlayProgress",self,"OnSyncTerritoryWarPlayProgress")
	g_ScriptEvent:AddListener("MainPlayerMoveEnded", self, "OnMainPlayerMoveEnded")
	g_ScriptEvent:AddListener("MainPlayerMoveStepped", self, "MainPlayerMoveStepped")
	g_ScriptEvent:AddListener("MouseScrollWheel",self,"OnMouseScrollWheel")
	g_ScriptEvent:AddListener("UpdateGuildLeagueInfo",self,"OnUpdateGuildLeagueInfo")
	g_ScriptEvent:AddListener("SendTerritoryWarCanDrawCommand",self,"OnSendTerritoryWarCanDrawCommand")
	g_ScriptEvent:AddListener("CloseGuildTerritorialWarsDrawMapPanel",self,"CloseGuildTerritorialWarsDrawMapPanel")
	g_ScriptEvent:AddListener("UpdateNpcInfo", self, "UpdateNpcInfo")
	g_ScriptEvent:AddListener("SendGTWChallengeBattleFieldInfo", self, "OnSendGTWChallengeBattleFieldInfo")
	GestureRecognizer.Inst:AddNeedUIPinchWnd(CLuaUIResources.GuildTerritorialWarsMapWnd)
	self.m_OnPinchInDelegate = DelegateFactory.Action_GenericGesture(function(gesture) self:OnPinchIn(gesture) end)
    self.m_OnPinchOutDelegate = DelegateFactory.Action_GenericGesture(function(gesture) self:OnPinchOut(gesture) end)
    GestureRecognizer.AddGestureListener(GestureType.OnUIPinchIn, self.m_OnPinchInDelegate)
    GestureRecognizer.AddGestureListener(GestureType.OnUIPinchOut, self.m_OnPinchOutDelegate)
end

function LuaGuildTerritorialWarsMapWnd:OnDisable()
	g_ScriptEvent:RemoveListener("GuildTerritorialWarsMap_TryToTeleport", self, "TryToTeleport")
	g_ScriptEvent:RemoveListener("OnSendTerritoryWarMap_GridId2Level", self, "OnSendGridId2Level")
	g_ScriptEvent:RemoveListener("OnSendTerritoryWarMapDetail", self, "OnSendTerritoryWarMapDetail")
	g_ScriptEvent:RemoveListener("UpdateObjPosInfo", self, "UpdateObjPosInfo")
	g_ScriptEvent:RemoveListener("QueryEnemyPlayerPosResult", self, "QueryEnemyPlayerPosResult")
	g_ScriptEvent:RemoveListener("RenderSceneInit", self, "OnRenderSceneInit")
	g_ScriptEvent:RemoveListener("MouseScrollWheel",self,"OnMouseScrollWheel")
	g_ScriptEvent:RemoveListener("OnSyncTerritoryWarPlayProgress",self,"OnSyncTerritoryWarPlayProgress")
	g_ScriptEvent:RemoveListener("MainPlayerMoveEnded", self, "OnMainPlayerMoveEnded")
	g_ScriptEvent:RemoveListener("MainPlayerMoveStepped", self, "MainPlayerMoveStepped")
	g_ScriptEvent:RemoveListener("MouseScrollWheel",self,"OnMouseScrollWheel")
	g_ScriptEvent:RemoveListener("UpdateGuildLeagueInfo",self,"OnUpdateGuildLeagueInfo")
	g_ScriptEvent:RemoveListener("SendTerritoryWarCanDrawCommand",self,"OnSendTerritoryWarCanDrawCommand")
	g_ScriptEvent:RemoveListener("CloseGuildTerritorialWarsDrawMapPanel",self,"CloseGuildTerritorialWarsDrawMapPanel")
	g_ScriptEvent:RemoveListener("UpdateNpcInfo", self, "UpdateNpcInfo")
	g_ScriptEvent:RemoveListener("SendGTWChallengeBattleFieldInfo", self, "OnSendGTWChallengeBattleFieldInfo")
	GestureRecognizer.RemoveGestureListener(GestureType.OnUIPinchIn, self.m_OnPinchInDelegate)
    GestureRecognizer.RemoveGestureListener(GestureType.OnUIPinchOut, self.m_OnPinchOutDelegate)
	LuaGuildTerritorialWarsMgr.m_IsShowBigMap = true
	LuaGuildTerritorialWarsMgr.m_CurPosGridId = 0
	LuaGuildTerritorialWarsMgr.m_IsInGuide = false
	self:CancelUpdateMapTimeTick()
	self:CancelRequestMapTick()
	self:CancelQueryMyMapPosInfoTick()
	self:CancelShowPlayTeleportFailedFxTick()
end

function LuaGuildTerritorialWarsMapWnd:OnSendGTWChallengeBattleFieldInfo(myGuildId, remainTime, bossInfo, passGuildInfo)
	self:UpdateBigMapViewTopLabel()
	if self.m_BigMapGridData then
		for id,t in pairs(self.m_BigMapGridData) do
			local obj = t.go
			if LuaGuildTerritorialWarsMgr.m_GridInfo then
				self:InitGridItem(obj, id)
			end
		end
	end
end

function LuaGuildTerritorialWarsMapWnd:OnSendTerritoryWarCanDrawCommand(requestType, can)
	self.m_IsShowCommandButton = can and not LuaGuildTerritorialWarsMgr:IsChallengePlayOpen()
	self.CommandButton.gameObject:SetActive(self.m_IsShowCommandButton and LuaGuildTerritorialWarsMgr.m_IsShowBigMap )
	self.RightViewTable:Reposition()
end

function LuaGuildTerritorialWarsMapWnd:OnMouseScrollWheel()
	local v = Input.GetAxis("Mouse ScrollWheel")
	v = math.max(math.min(1,self.ScaleSlider.value + v),0)
	self.ScaleSlider.value = v
end

function LuaGuildTerritorialWarsMapWnd:OnPinchIn(gesture)
	local deltaPinch = gesture.deltaPinch
    local v = math.min(math.max(deltaPinch / 400, 0),1)
	v = math.max(math.min(1,self.ScaleSlider.value - v),0)
	self.ScaleSlider.value = v
end

function LuaGuildTerritorialWarsMapWnd:OnPinchOut(gesture)
	local deltaPinch = gesture.deltaPinch
    local v = math.min(math.max(deltaPinch / 400, 0),1)
	v = math.max(math.min(1,self.ScaleSlider.value + v),0)
	self.ScaleSlider.value = v
end

function LuaGuildTerritorialWarsMapWnd:Init()
	self.SmallMapSituationViewArrow1.gameObject:SetActive(false)
	self.GridTemplate.gameObject:SetActive(false)
	self.AxisNumTemplate.gameObject:SetActive(false)
	self.MarkTemplate.gameObject:SetActive(false)
	self.TeleportMarkTemplate.gameObject:SetActive(false)
	self.MonsterTemplate.gameObject:SetActive(false)
	self:CreateBigMapView()
	self:InitMapView()
	Gac2Gas.RequestTerritoryWarCanDrawCommand(0)
end

function LuaGuildTerritorialWarsMapWnd:InitMapView()
	LuaGuildTerritorialWarsMgr.m_GridTipData = self.m_BigMapGridData[LuaGuildTerritorialWarsMgr.m_CurPosGridId]
	self.SwitchButton.gameObject:SetActive(not LuaGuildTerritorialWarsMgr.m_IsShowBigMap or LuaGuildTerritorialWarsMgr:IsInPlay() or LuaGuildTerritorialWarsMgr:IsPeripheryPlay() or LuaGuildTerritorialWarsMgr:IsChallengePlayOpen())
	self.BigMapView.gameObject:SetActive(LuaGuildTerritorialWarsMgr.m_IsShowBigMap)
	self.SmallMapView.gameObject:SetActive(not LuaGuildTerritorialWarsMgr.m_IsShowBigMap)
	self.InfoButton.gameObject:SetActive(not LuaGuildTerritorialWarsMgr.m_IsShowBigMap)
	self.LegendButton.gameObject:SetActive(LuaGuildTerritorialWarsMgr.m_IsShowBigMap)
	self.RoutingButton.gameObject:SetActive(not LuaGuildTerritorialWarsMgr.m_IsShowBigMap)
	self.ScaleSlider.gameObject:SetActive(LuaGuildTerritorialWarsMgr.m_IsShowBigMap)
	self.MyOccupyNumLabel.gameObject:SetActive(LuaGuildTerritorialWarsMgr.m_IsShowBigMap and LuaGuildTerritorialWarsMgr.m_ObserveType > 0 and not LuaGuildTerritorialWarsMgr:IsPeripheryPlay() and not LuaGuildTerritorialWarsMgr:IsChallengePlayOpen())
	self.ResetButton.gameObject:SetActive(LuaGuildTerritorialWarsMgr.m_IsShowBigMap)
	self.TipButton.gameObject:SetActive(not LuaGuildTerritorialWarsMgr:IsChallengePlayOpen())
	self.SituatuionButton.gameObject:SetActive(LuaGuildTerritorialWarsMgr.m_ObserveType > 0 or LuaGuildTerritorialWarsMgr:IsChallengePlayOpen())
	self.RebackGuildButton.gameObject:SetActive(LuaGuildTerritorialWarsMgr.m_ObserveType > 0 and not LuaGuildTerritorialWarsMgr:IsChallengePlayOpen())
	self.RebackCityButton.gameObject:SetActive(not LuaGuildTerritorialWarsMgr:IsChallengePlayOpen())
	self.SmallMapSituationView.gameObject:SetActive(LuaGuildTerritorialWarsMgr.m_GridTipData and not LuaGuildTerritorialWarsMgr:IsChallengePlayOpen())
	self.HideSmallMapSituationView.gameObject:SetActive(false)
	self.BigMapViewTopLabel.gameObject:SetActive(false)
	self.GuildTerritorialWarsMapRankView.gameObject:SetActive(not LuaGuildTerritorialWarsMgr:IsPeripheryPlay() and not LuaGuildTerritorialWarsMgr:IsInGuide() and not LuaGuildTerritorialWarsMgr:IsChallengePlayOpen())
	self.ChallengeClearTimeView.gameObject:SetActive(false)
	self.RightViewTable.gameObject:SetActive(not LuaGuildTerritorialWarsMgr:IsInGuide())
	self.CommandButton.gameObject:SetActive(self.m_IsShowCommandButton and LuaGuildTerritorialWarsMgr.m_IsShowBigMap and not LuaGuildTerritorialWarsMgr:IsChallengePlayOpen())
	self.ColorEnemyAreaQnCheckBox.gameObject:SetActive(LuaGuildTerritorialWarsMgr.m_IsShowBigMap and not LuaGuildTerritorialWarsMgr:IsInGuide())
	self.RebackOriginButton.gameObject:SetActive(LuaGuildTerritorialWarsMgr:IsChallengePlayOpen())
	self.RebackBattlegroundButton.gameObject:SetActive(LuaGuildTerritorialWarsMgr:IsChallengePlayOpen())
	self.TopLeftTable:Reposition()
	self.BottomLeftTable:Reposition()
	self.RightViewTable:Reposition()
	self.TablePanel.gameObject:SetActive(not self.m_IsDrawingMap)
	self.GuildTerritorialWarsDrawMapPanel.gameObject:SetActive(self.m_IsDrawingMap)
	CUIManager.CloseUI(CLuaUIResources.GuildTerritorialWarsMapTipWnd)
	self:CancelRequestMapTick()
	self:CancelQueryMyMapPosInfoTick()
	if not LuaGuildTerritorialWarsMgr:IsInGuide() and not LuaGuildTerritorialWarsMgr:IsChallengePlayOpen() then
		Gac2Gas.RequestTerritoryWarMapDetail()
		if LuaGuildTerritorialWarsMgr.m_ObserveType > 0 then
			self.m_RequestMapTick = RegisterTick(function()
				Gac2Gas.RequestTerritoryWarMapDetail()
			end,2000)
		end
	end
	if LuaGuildTerritorialWarsMgr:IsChallengePlayOpen() then
		Gac2Gas.RequestGTWChallengeBattleFieldInfo()
	end
	if LuaGuildTerritorialWarsMgr.m_IsShowBigMap then
		self:InitForCenterGrid()
		self:ShowCloudFx()
	elseif not LuaGuildTerritorialWarsMgr:IsInGuide() then
		self:InitTeleport()
		if CClientMainPlayer.Inst then
			Gac2Gas.QuerySceneInfo(CClientMainPlayer.Inst.EngineId)
		end
		Gac2Gas.QueryMMapPosInfo()
		self.m_QueryMyMapPosInfoTick = RegisterTick(function()
			Gac2Gas.QueryMMapPosInfo()
		end,1000)
	end
end

function LuaGuildTerritorialWarsMapWnd:ShowCloudFx()
	local key = "GuildTerritorialWarsMapWnd_ShowCloudFx"
	local now = CServerTimeMgr.Inst:GetZone8Time()
	local dayofweek = EnumToInt(now.DayOfWeek)
	if dayofweek == 0 then
        dayofweek = 7
    end
	local dataTime = CreateFromClass(DateTime, now.Year, now.Month, now.Day - dayofweek + 1, 0, 0, 0)
	local s = ToStringWrap(dataTime, "yyyy-MM-dd")
	local cache = PlayerPrefs.GetString(key)
	if String.IsNullOrEmpty(cache) or cache ~= s then
		RegisterTickOnce(function ()
			if self.CloudFx then
				self.CloudFx:LoadFx("fx/ui/prefab/UI_Cloud_disappear.prefab")
			end
		end, 100) 
	end
	PlayerPrefs.SetString(key, s)
end
--@region BigMap

function LuaGuildTerritorialWarsMapWnd:CreateBigMapView()
	self.m_GridBorderSpan = 2
	self.m_BigMapMaxScale = 3
	self.m_BigMapScale = self.m_BigMapMaxScale
	self.m_GridWidth, self.m_GridHeight = self.GridTemplate.width + self.m_GridBorderSpan, self.GridTemplate.height + self.m_GridBorderSpan
	self.m_BigMapGridData = {}
	self.m_PosIndex2GridId = {}
	self.m_BigMapXSize, self.m_BigMapYSize = 1,1
	GuildTerritoryWar_Map.Foreach(function (key, data)
		self.m_BigMapGridData[data.Id] = {id = data.Id,region = data.Region,level = data.Level,x = data.PosX, y = data.PosY}
		if data.Level == 4 then
			self.m_JunTianZhuChengId = key
		end
		self.m_BigMapXSize = math.max(self.m_BigMapXSize,data.PosX)
		self.m_BigMapYSize = math.max(self.m_BigMapYSize,data.PosY)
	end)
	for id, level in pairs(LuaGuildTerritorialWarsMgr.m_GridId2Level) do
		self.m_BigMapGridData[id].level = level
	end
	for id, t in pairs(self.m_BigMapGridData) do
		local index = t.x * self.m_BigMapXSize + t.y
		self.m_PosIndex2GridId[index] = id
	end
	self:CreateBigMapAxis(self.m_BigMapXSize, self.m_BigMapYSize)

	self:InitForCenterGrid()

	self:InitMapRadioBox()

	self:InitScaleSlider()

	self:CancelUpdateMapTimeTick()
	self.m_UpdateMapTimeTick = RegisterTick(function()
		self:UpdateGridTimeLabel()
		self:UpdateBigMapViewTopLabel()
	end,500)

	self:UpdateMyOccupyNumLabel()
	self:OnSyncTerritoryWarPlayProgress()
end

function LuaGuildTerritorialWarsMapWnd:UpdateMyOccupyNumLabel()
	self.MyOccupyNumLabel.text = SafeStringFormat3(LocalString.GetString("我帮占领%d/%d"), LuaGuildTerritorialWarsMgr.m_MyGuildGridCount,GuildTerritoryWar_Setting.GetData().MaxOccupyAreaNum) 
	local isFull = LuaGuildTerritorialWarsMgr.m_MyGuildGridCount == GuildTerritoryWar_Setting.GetData().MaxOccupyAreaNum
	self.MyOccupyNumLabel.transform:Find("Sprite"):GetComponent(typeof(UISprite)).color = NGUIText.ParseColor24(isFull and "fffe91" or "349759", 0)
	self.OccupyNumTipButton.gameObject:SetActive(isFull)
end

function LuaGuildTerritorialWarsMapWnd:InitMapRadioBox()
	self.m_RadioBoxIndex2GridId = {}
	self.m_MapRadioBox = self.Map.gameObject:GetComponent(typeof(QnRadioBox))
	if not self.m_MapRadioBox then
		self.m_MapRadioBox = self.Map.gameObject:AddComponent(typeof(QnRadioBox))
	end
	self.m_MapRadioBox.m_RadioButtons = CreateFromClass(MakeArrayClass(QnSelectableButton), GuildTerritoryWar_Map.GetDataCount())
	local index = 0
	GuildTerritoryWar_Map.Foreach(function (key, data)
		local x, y = data.PosX, data.PosY
		local obj = NGUITools.AddChild(self.BigMapBottomLeftRoot.gameObject, self.GridTemplate.gameObject)
		obj.gameObject:SetActive(true)
		obj.transform.localPosition = self:GetGridPos(x, y)
		self.m_BigMapGridData[data.Id].go = obj
		UIEventListener.Get(obj).onDragStart = DelegateFactory.VoidDelegate(function (go)
			self:OnDragMapStart()
		end)
		if LuaGuildTerritorialWarsMgr.m_GridInfo then
			self:InitGridItem(obj, data.Id)
		end
		self.m_MapRadioBox.m_RadioButtons[index] = obj:GetComponent(typeof(QnSelectableButton))
		self.m_MapRadioBox.m_RadioButtons[index].OnClick = MakeDelegateFromCSFunction(self.m_MapRadioBox.On_Click, MakeGenericClass(Action1, QnButton), self.m_MapRadioBox)
		self.m_RadioBoxIndex2GridId[index] = data.Id
		index = index + 1
	end)
	self.m_MapRadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn,index)
        self:OnMapRadioBoxSelected(btn,index)
    end)
end

function LuaGuildTerritorialWarsMapWnd:InitScaleSlider()
	self.ScaleSlider.value = 0
	self.ScaleSlider.OnChangeValue = DelegateFactory.Action_float(function(value)
        self:OnScaleSliderChangeValue(value)
	end)
	self.ScaleSlider.value = (1.5 - 1)/(self.m_BigMapMaxScale - 1)
end

function LuaGuildTerritorialWarsMapWnd:UpdateGridTimeLabel()
	for id,t in pairs(self.m_BigMapGridData) do
		local gridInfo = LuaGuildTerritorialWarsMgr.m_GridInfo and LuaGuildTerritorialWarsMgr.m_GridInfo[id]
		if gridInfo then
			local obj = t.go
			if obj then
				local giveUpLabel = obj.transform:Find("GiveUpLabel"):GetComponent(typeof(UILabel))
				local protectLabel = obj.transform:Find("ProtectLabel"):GetComponent(typeof(UILabel))
				local protectSpriteAni = protectLabel.transform:Find("Sprite"):GetComponent(typeof(TweenAlpha))
				local isGiveUp = gridInfo.status == 2
				local isProtect = gridInfo.status >= 3 and gridInfo.status <= 5
				local leftTime = gridInfo.stausParam - CServerTimeMgr.Inst.timeStamp
				local leftTimeStr = leftTime >= 0 and SafeStringFormat3("%02d:%02d", math.floor(leftTime % 3600 / 60),leftTime % 60) or ""
		
				giveUpLabel.gameObject:SetActive(isGiveUp and self.m_IsShowMostInfo)
				protectLabel.gameObject:SetActive(isProtect and self.m_IsShowMostInfo)
				protectSpriteAni.enabled = gridInfo.status == 5 and self.m_IsShowMostInfo	
				giveUpLabel.text = (isGiveUp and self.m_IsShowMostInfo) and leftTimeStr or ""
				protectLabel.text = (isProtect and self.m_IsShowMostInfo) and leftTimeStr or ""
			end
		end
	end
end

function LuaGuildTerritorialWarsMapWnd:UpdateBigMapViewTopLabel()
	if LuaGuildTerritorialWarsMgr:IsPeripheryPlay() then
		self.BigMapViewTopLabel.gameObject:SetActive(false)
		return
	end
	if LuaGuildTerritorialWarsMgr:IsChallengePlayOpen() then
		local t = LuaGuildTerritorialWarsMgr.m_ChallengeBattleFieldInfo
		self.BigMapViewTopLabel.gameObject:SetActive(true)
		local restTime = t.remainTime - CServerTimeMgr.Inst.timeStamp
		self.BigMapViewTopLabel.text = SafeStringFormat3(LocalString.GetString("棋局挑战正在进行 %02d:%02d:%02d"), math.floor(restTime / 3600), math.floor((restTime % 3600) / 60), restTime % 60)
		self.BigMapViewTopLabel.color = NGUIText.ParseColor24("fffe91", 0)
		return
	end

	local time1 = LuaGuildTerritorialWarsMgr.m_BattleStartTime - math.floor(CServerTimeMgr.Inst.timeStamp)
	if time1 > 0 then
		self.BigMapViewTopLabel.gameObject:SetActive(LuaGuildTerritorialWarsMgr.m_IsShowBigMap)
		self.BigMapViewTopLabel.text = SafeStringFormat3(LocalString.GetString("天地棋局即将开启 %02d:%02d"), math.floor((time1 % 3600) / 60), time1 % 60)
		self.BigMapViewTopLabel.color = Color.green
		return 
	end
	local time2 = LuaGuildTerritorialWarsMgr.m_PlayEndTime - math.floor(CServerTimeMgr.Inst.timeStamp)
	local needAlert = time2 < GuildTerritoryWar_Setting.GetData().AlertPlayEndCountDown
	self.BigMapViewTopLabel.gameObject:SetActive(LuaGuildTerritorialWarsMgr.m_IsShowBigMap and time2 > 0)
	self.BigMapViewTopLabel.text = SafeStringFormat3(LocalString.GetString("天地棋局正在进行中 %02d:%02d"), math.floor((time2 % 3600) / 60), time2 % 60)
	self.BigMapViewTopLabel.color = NGUIText.ParseColor24(needAlert and "ff5050" or "fffe91", 0)
end

function LuaGuildTerritorialWarsMapWnd:CancelUpdateMapTimeTick()
	if self.m_UpdateMapTimeTick then
		UnRegisterTick(self.m_UpdateMapTimeTick)
		self.m_UpdateMapTimeTick = nil
	end
end

function LuaGuildTerritorialWarsMapWnd:CancelRequestMapTick()
	if self.m_RequestMapTick then
		UnRegisterTick(self.m_RequestMapTick)
		self.m_RequestMapTick = nil
	end
end

function LuaGuildTerritorialWarsMapWnd:CancelQueryMyMapPosInfoTick()
	if self.m_QueryMyMapPosInfoTick then
		UnRegisterTick(self.m_QueryMyMapPosInfoTick)
		self.m_QueryMyMapPosInfoTick = nil
	end
end

function LuaGuildTerritorialWarsMapWnd:CancelShowPlayTeleportFailedFxTick()
	if self.m_ShowPlayTeleportFailedFxTick then
		UnRegisterTick(self.m_ShowPlayTeleportFailedFxTick)
		self.m_ShowPlayTeleportFailedFxTick = nil
	end
end

function LuaGuildTerritorialWarsMapWnd:GetGridPos(x, y)
	return Vector3((x - 0.5) * self.m_GridWidth,(y - 0.5) * self.m_GridHeight,0)
end

function LuaGuildTerritorialWarsMapWnd:CreateBigMapAxis(xSize, ySize)
	self.m_XAxisFigureObjectsList = {}
	for x = 1, xSize do
		local obj = NGUITools.AddChild(self.BottomAxis.gameObject, self.AxisNumTemplate.gameObject)
		local label = obj.transform:Find("Label"):GetComponent(typeof(UILabel))
		label.text = x
		obj.gameObject:SetActive(true)
		self.m_XAxisFigureObjectsList[x] = {obj = obj, label = label}
	end
	self.m_YAxisFigureObjectsList = {}
	for y = 1, ySize do
		local obj = NGUITools.AddChild(self.LeftAxis.gameObject, self.AxisNumTemplate.gameObject)
		local label = obj.transform:Find("Label"):GetComponent(typeof(UILabel))
		label.text = y
		obj.gameObject:SetActive(true)
		self.m_YAxisFigureObjectsList[y] = {obj = obj, label = label}
	end
end

function LuaGuildTerritorialWarsMapWnd:InitAxisFigureObjectsPos()
	local gridPos = self:GetGridPos(0, 0)
	local xOffset = gridPos.x + self.BigMapBottomLeftRoot.transform.localPosition.x
	local yOffset = gridPos.y + self.BigMapBottomLeftRoot.transform.localPosition.y
	local dx = self.m_GridWidth
	local dy = self.m_GridHeight
	local additionalX = self.Map.transform.localPosition.x + self.MapPanel.transform.localPosition.x - self.Axis.transform.localPosition.x - self.BottomAxis.transform.localPosition.x
	local additionalY = self.Map.transform.localPosition.y + self.MapPanel.transform.localPosition.y - self.Axis.transform.localPosition.y - self.LeftAxis.transform.localPosition.y

	for x, t in pairs(self.m_XAxisFigureObjectsList) do
		xOffset = xOffset + dx
		t.obj.transform.localPosition = Vector3(xOffset * self.m_BigMapScale + additionalX, 0, 0)
	end

	for y, t in pairs(self.m_YAxisFigureObjectsList) do
		yOffset = yOffset + dy
		t.obj.transform.localPosition = Vector3(0, yOffset * self.m_BigMapScale + additionalY, 0)
	end
end

function LuaGuildTerritorialWarsMapWnd:OnMouseScrollWheel()
	local delta = Input.GetAxis("Mouse ScrollWheel")
	local newScale = self.ScaleSlider.value + delta * 0.1
	self.ScaleSlider.value = math.max(0,math.min(1,newScale))
end

function LuaGuildTerritorialWarsMapWnd:InitGridItem(obj, id)
	local t = self.m_BigMapGridData[id]
	local level = t.level
	local gridInfo = LuaGuildTerritorialWarsMgr.m_GridInfo[id]
	local challengePlayBoss = obj.transform:Find("ChallengePlayBoss").gameObject
	self:InitChallengePlayBossItem(challengePlayBoss, obj,  id)
	if gridInfo == nil then
		return
	end
	local isCenterMap = t.level == 4
	local isMyArea = LuaGuildTerritorialWarsMgr:IsMyArea(id)
	local isFriendArea = LuaGuildTerritorialWarsMgr:IsFriendArea(id)
	local isCaptiveArea =  LuaGuildTerritorialWarsMgr:IsCaptiveArea(id)
	local isMyCaptiveArea = (isCaptiveArea and isMyArea) or (LuaGuildTerritorialWarsMgr:GetMyCaptiveArea(id))
	local isFriendCaptiveArea = isCaptiveArea and isFriendArea
	local isMyCity = isMyArea and gridInfo.isCity
	local isBattle = gridInfo.status == 1
	local isGiveUp = gridInfo.status == 2
	local isProtect = gridInfo.status >= 3 and gridInfo.status <= 5
	local isInCurPos = LuaGuildTerritorialWarsMgr.m_CurPosGridId == id
	local isFavor = LuaGuildTerritorialWarsMgr.m_FavGridIdInfo[id]
	local leftTime = gridInfo.stausParam - CServerTimeMgr.Inst.timeStamp
	local leftTimeStr = leftTime >= 0 and SafeStringFormat3("%02d:%02d", math.floor(leftTime % 3600 / 60),leftTime % 60) or ""
	--self.m_IsShowMostInfo
	obj.name = id
	local level1Map = obj.transform:Find("Level1Map").gameObject
	local level2Map = obj.transform:Find("Level2Map").gameObject
	local level3Map = obj.transform:Find("Level3Map").gameObject
	local centerMap = obj.transform:Find("CenterMap").gameObject
	local friendArea = obj.transform:Find("FriendArea").gameObject
	local myCaptiveArea = obj.transform:Find("MyCaptiveArea").gameObject
	local friendCaptiveArea = obj.transform:Find("FriendCaptiveArea").gameObject
	local myGuild = obj.transform:Find("MyGuild").gameObject
	local myPosGo = obj.transform:Find("MyPos").gameObject
	local starGo = obj.transform:Find("Star").gameObject
	local battleGo = obj.transform:Find("Battle").gameObject
	local giveUpLabel = obj.transform:Find("GiveUpLabel"):GetComponent(typeof(UILabel))
	local protectLabel = obj.transform:Find("ProtectLabel"):GetComponent(typeof(UILabel))
	local protectSpriteAni = protectLabel.transform:Find("Sprite"):GetComponent(typeof(TweenAlpha))
	local yellowHighlight = obj.transform:Find("YellowHighlight").gameObject
	local closedGo = obj.transform:Find("Closed").gameObject
	local monsterGo = obj.transform:Find("Monster").gameObject
	local myArea = obj.transform:Find("MyArea").gameObject

	yellowHighlight.gameObject:SetActive(false)
	level1Map.gameObject:SetActive(level == 1 and not isCenterMap and (self.m_IsShowMostInfo or LuaGuildTerritorialWarsMgr:IsPeripheryPlay()))
	level2Map.gameObject:SetActive(level == 2 and not isCenterMap and (self.m_IsShowMostInfo or LuaGuildTerritorialWarsMgr:IsPeripheryPlay()))
	level3Map.gameObject:SetActive(level == 3 and not isCenterMap and (self.m_IsShowMostInfo or LuaGuildTerritorialWarsMgr:IsPeripheryPlay()))
	centerMap.gameObject:SetActive(isCenterMap and (self.m_IsShowMostInfo or LuaGuildTerritorialWarsMgr:IsPeripheryPlay()))
	myArea.gameObject:SetActive(isMyArea and not isMyCaptiveArea and not LuaGuildTerritorialWarsMgr:IsChallengePlayOpen())
	friendArea.gameObject:SetActive(isFriendArea and not isFriendCaptiveArea and not LuaGuildTerritorialWarsMgr:IsChallengePlayOpen())
	myCaptiveArea.gameObject:SetActive(isMyCaptiveArea and not LuaGuildTerritorialWarsMgr:IsChallengePlayOpen())
	myCaptiveArea.transform:Find("Sprite"):GetComponent(typeof(UITexture)).flip = LuaGuildTerritorialWarsMgr:GetMyCaptiveArea(id) and UIBasicSprite.Flip.Vertically or UIBasicSprite.Flip.Nothing
	friendCaptiveArea.gameObject:SetActive(isFriendCaptiveArea and not LuaGuildTerritorialWarsMgr:IsChallengePlayOpen())
	myGuild.gameObject:SetActive(isMyCity and not LuaGuildTerritorialWarsMgr:IsChallengePlayOpen())
	self:InitGridItemArrows(obj, id)
	self:InitGridItemEnemyArea(obj,id)
	self:InitGridItemOtherGuild(obj, id)
	battleGo.gameObject:SetActive(isBattle and self.m_IsShowMostInfo)
	giveUpLabel.gameObject:SetActive(isGiveUp and self.m_IsShowMostInfo)
	protectLabel.gameObject:SetActive(isProtect and self.m_IsShowMostInfo)
	myPosGo.gameObject:SetActive(isInCurPos)
	starGo.gameObject:SetActive(isFavor and (self.m_IsShowMostInfo or LuaGuildTerritorialWarsMgr:IsPeripheryPlay()))
	protectSpriteAni.enabled = gridInfo.status == 5 and self.m_IsShowMostInfo
	giveUpLabel.text = (isGiveUp and self.m_IsShowMostInfo) and leftTimeStr or ""
	protectLabel.text = (isProtect and self.m_IsShowMostInfo) and leftTimeStr or ""
	monsterGo.gameObject:SetActive((LuaGuildTerritorialWarsMgr:GetMonsterCount(id) > 0) and LuaGuildTerritorialWarsMgr:IsPeripheryPlay()) 
	local mapData = GuildTerritoryWar_Map.GetData(id)
	local regionData = GuildTerritoryWar_Region.GetData(mapData.Region)
	closedGo.gameObject:SetActive(LuaGuildTerritorialWarsMgr.m_CurrentWeek < regionData.Opendate and not LuaGuildTerritorialWarsMgr:IsChallengePlayOpen()) 
end

function LuaGuildTerritorialWarsMapWnd:InitChallengePlayBossItem(item, obj, id)
	item.gameObject:SetActive(false)
	if LuaGuildTerritorialWarsMgr:IsChallengePlayOpen() and LuaGuildTerritorialWarsMgr.m_ChallengeBattleFieldInfo and LuaGuildTerritorialWarsMgr.m_ChallengeBattleFieldInfo.gridInfo then 
		local icon = item.transform:Find("Icon").gameObject:GetComponent(typeof(CUITexture)) 
		local redBattleBg = item.transform:Find("RedBattleBg"):GetComponent(typeof(CUIFx)) 
		local lock = item.transform:Find("Lock")
		local myPosGo = obj.transform:Find("MyPos").gameObject
		local finishedObj = item.transform:Find("Finished")
		redBattleBg:DestroyFx()
		local gridInfo = LuaGuildTerritorialWarsMgr.m_ChallengeBattleFieldInfo.gridInfo[id]
		if gridInfo then
			item.gameObject:SetActive(true)
			local bossIdx = gridInfo.bossIdx
			local isKill = gridInfo.state == 1
			local isBattle = gridInfo.state == 2
			local islock = gridInfo.state == 3
			local data = GuildTerritoryWar_ChallengeBoss.GetData(bossIdx)
			local monsterId = LuaGuildTerritorialWarsMgr:IsEliteChallengePlayOpen() and data.EliteMonsterId or data.NormalMonsterId
			local monsterData = Monster_Monster.GetData(monsterId)
			redBattleBg.gameObject:SetActive(isBattle)
			if isBattle then
				redBattleBg:LoadFx("fx/ui/prefab/UI_duizhanzhuangtai.prefab")
			end
			lock.gameObject:SetActive(islock)
			finishedObj.gameObject:SetActive(isKill)
			Extensions.SetLocalRotationZ(icon.transform, isKill and -1 or 0)
			icon.color = islock and Color.black or Color.white
			icon.alpha = isKill and 0.6 or (islock and 0.8 or 1)
			icon:LoadNPCPortrait(monsterData and monsterData.HeadIcon or nil)
			myPosGo.gameObject:SetActive(LuaGuildTerritorialWarsMgr.m_ChallengeBattleFieldInfo.curbossIdx == bossIdx)
		end
	end
end

function LuaGuildTerritorialWarsMapWnd:InitGridItemArrows(obj, id)
	local topArrow = obj.transform:Find("TopArrow").gameObject
	local bottomArrow = obj.transform:Find("BottomArrow").gameObject
	local leftArrow = obj.transform:Find("LeftArrow").gameObject
	local rightArrow = obj.transform:Find("RightArrow").gameObject

	local isMyArea = LuaGuildTerritorialWarsMgr:IsMyArea(id)
	local isCaptiveArea =  LuaGuildTerritorialWarsMgr:IsCaptiveArea(id)
	local isMyCaptiveArea = isCaptiveArea and isMyArea

	topArrow.gameObject:SetActive(false)
	bottomArrow.gameObject:SetActive(false)
	leftArrow.gameObject:SetActive(false)
	rightArrow.gameObject:SetActive(false)
	if LuaGuildTerritorialWarsMgr:IsChallengePlayOpen() then
		return
	end
	if (isMyArea or isMyCaptiveArea) then
		local data = self.m_BigMapGridData[id]
		if not data then return end
		local x,y = data.x, data.y
		local index = x * self.m_BigMapXSize + y
		local arrowArr = {leftArrow, rightArrow, bottomArrow, topArrow}
		local indexArr = {index - self.m_BigMapXSize, index + self.m_BigMapXSize, y > 1 and (index - 1) or 0, y < self.m_BigMapYSize and (index + 1) or 0}
		for i,index in pairs(indexArr) do
			local id = self.m_PosIndex2GridId[index]
			local t = self.m_BigMapGridData[id]
			local arrow = arrowArr[i]
			if t then
				local gridInfo = LuaGuildTerritorialWarsMgr.m_GridInfo[t.id]
				local isMyCaptiveArea = LuaGuildTerritorialWarsMgr:GetMyCaptiveArea(id)
				local isEnemyArea = LuaGuildTerritorialWarsMgr:IsEnemyArea(t.id) and (not (isMyCaptiveArea and gridInfo.isCity))
				local mapData = GuildTerritoryWar_Map.GetData(id)
				local regionData = GuildTerritoryWar_Region.GetData(mapData.Region)
				local isEmptyArea = (gridInfo.belongGuildId == 0) and (LuaGuildTerritorialWarsMgr.m_CurrentWeek >= regionData.Opendate)
				arrow.gameObject:SetActive(isEnemyArea or isEmptyArea)
			end
		end
	end
end

function LuaGuildTerritorialWarsMapWnd:InitGridItemEnemyArea(obj, id)
	local enemyArea = obj.transform:Find("EnemyArea").gameObject
	local isEnemyArea = LuaGuildTerritorialWarsMgr:IsEnemyArea(id)
	local isMyCaptiveArea = LuaGuildTerritorialWarsMgr:GetMyCaptiveArea(id)
	enemyArea.gameObject:SetActive(isEnemyArea and not isMyCaptiveArea)
	local color = NGUIText.ParseColor24("E58875", 0)
	if isEnemyArea and self.m_IsPaintingColorEnemyArea then
		color = LuaGuildTerritorialWarsMgr:GetGridColor(id)
	end
	if color then
		enemyArea.transform:Find("Frame"):GetComponent(typeof(UITexture)).color = color
	end
end

function LuaGuildTerritorialWarsMapWnd:InitGridItemOtherGuild(obj, id)
	local otherGuild = obj.transform:Find("OtherGuild").gameObject
	local gridInfo = LuaGuildTerritorialWarsMgr.m_GridInfo[id]
	local guildInfo = LuaGuildTerritorialWarsMgr.m_GuildInfo[gridInfo.belongGuildId]
	local isMyArea = LuaGuildTerritorialWarsMgr:IsMyArea(id)
	--local isFriendArea = LuaGuildTerritorialWarsMgr:IsFriendArea(id)
	local isOtherCity = not isMyArea and gridInfo.isCity

	--local friendArea = obj.transform:Find("FriendArea").gameObject
	--local enemyArea = obj.transform:Find("EnemyArea").gameObject
	--local area = isFriendArea and friendArea or enemyArea
	--local color = area.transform:Find("Frame"):GetComponent(typeof(UITexture)).color
	local color = LuaGuildTerritorialWarsMgr:GetGridColor(id)
	otherGuild.transform:Find("Sprite"):GetComponent(typeof(UITexture)).color = color
	otherGuild.gameObject:SetActive(isOtherCity and (self.m_IsShowMostInfo or LuaGuildTerritorialWarsMgr:IsPeripheryPlay()))
	if isOtherCity and (self.m_IsShowMostInfo or LuaGuildTerritorialWarsMgr:IsPeripheryPlay()) then
		local name =  guildInfo and guildInfo.guildName or LocalString.GetString("帮")
		local ss1, ss2 = self:FilterSpecChars(name)
		if #ss1 > 0 then
			name = ss1[1]
		elseif #ss2 > 0 then
			name = ss2[1]
		end
		otherGuild.transform:Find("Label"):GetComponent(typeof(UILabel)).text =name
	end
end

function LuaGuildTerritorialWarsMapWnd:FilterSpecChars(s)
	local ss1, ss2 = {},{}
	local k = 1
	while true do
		if k > #s then break end
		local c = string.byte(s,k)
		if not c then break end
		if c<192 then
			if (c>=48 and c<=57) or (c>= 65 and c<=90) or (c>=97 and c<=122) then
				table.insert(ss2, string.char(c))
			end
			k = k + 1
		elseif c<224 then
			k = k + 2
		elseif c<240 then
			if c>=228 and c<=233 then
				local c1 = string.byte(s,k+1)
				local c2 = string.byte(s,k+2)
				if c1 and c2 then
					local a1,a2,a3,a4 = 128,191,128,191
					if c == 228 then a1 = 184
					elseif c == 233 then a2,a4 = 190,c1 ~= 190 and 191 or 165
					end
					if c1>=a1 and c1<=a2 and c2>=a3 and c2<=a4 then
						table.insert(ss1, string.char(c,c1,c2)) --中文
					end
				end
			end
			k = k + 3
		elseif c<248 then
			k = k + 4
		elseif c<252 then
			k = k + 5
		elseif c<254 then
			k = k + 6
		end
	end
	return ss1, ss2
end

function LuaGuildTerritorialWarsMapWnd:LoadMapCenterGridId(myMapId)
	local data = self.m_BigMapGridData[myMapId]
	local go = data.go
	if not go then return end
	local offsetX = self.BigMapBottomLeftRoot.transform.localPosition.x + go.transform.localPosition.x
	local offsetY = self.BigMapBottomLeftRoot.transform.localPosition.y + go.transform.localPosition.y
	local size = self.MapPanel:GetViewSize()
	local mapWidth, mapHeight = size.x, size.y 
	local scale = 0.5 * self.m_BigMapScale - 0.5
	mapWidth, mapHeight = mapWidth * scale, mapHeight * scale
	local centerPos = self.BigMapCenter.transform.localPosition
	offsetX = centerPos.x - offsetX
	offsetY = centerPos.y - offsetY
	offsetX = math.max(-mapWidth, math.min(offsetX, mapWidth))
	offsetY = math.max(-mapHeight, math.min(offsetY, mapHeight))
	self.Map.transform.localPosition = Vector3(offsetX,offsetY, 0)
end

function LuaGuildTerritorialWarsMapWnd:TryToTeleport(gridId)
	if LuaGuildTerritorialWarsMgr.m_CurPosGridId == gridId then
		g_MessageMgr:ShowMessage("GuildTerritorialWarsMapWnd_TeleportToCurPos")
		return
	end

	local mapData = GuildTerritoryWar_Map.GetData(gridId)
	local regionData = GuildTerritoryWar_Region.GetData(mapData.Region)
	if LuaGuildTerritorialWarsMgr.m_CurrentWeek < regionData.Opendate then
		g_MessageMgr:ShowMessage("GTW_GRID_REGION_NOT_OPEN")
		return
	end

	if self.m_IsPlayTeleportFailedFx then return end
	local data = self.m_BigMapGridData[gridId]
	if not data then return end
	local x,y = data.x, data.y
	local index = x * self.m_BigMapXSize + y
	local indexArr = {index, index - self.m_BigMapXSize, index + self.m_BigMapXSize,y > 1 and (index - 1) or 0, y < self.m_BigMapYSize and (index + 1) or 0}
	local isFailed = true
	for _,index in pairs(indexArr) do
		local id = self.m_PosIndex2GridId[index]
		local t = self.m_BigMapGridData[id]
		if t then
			local isMyArea = LuaGuildTerritorialWarsMgr:IsMyArea(t.id)
				local isFriendArea = LuaGuildTerritorialWarsMgr:IsFriendArea(t.id)
				isFailed = isFailed and (not isMyArea) and (not isFriendArea)
		end
	end
	if isFailed and not LuaGuildTerritorialWarsMgr:IsPeripheryPlay() then
		local duration = 0
		for _,index in pairs(indexArr) do
			local id = self.m_PosIndex2GridId[index]
			local t = self.m_BigMapGridData[id]
			if t then
				local yellowHighlight = t.go.transform:Find("YellowHighlight"):GetComponent(typeof(TweenAlpha))
				if duration <= 0 then
					duration = yellowHighlight.duration
				end
				yellowHighlight:ResetToBeginning()
				yellowHighlight.gameObject:SetActive(true)
				yellowHighlight:PlayForward()
			end
		end
		self.m_IsPlayTeleportFailedFx = true
		self:CancelShowPlayTeleportFailedFxTick()
		self.m_ShowPlayTeleportFailedFxTick = RegisterTickOnce(function()
			self.m_IsPlayTeleportFailedFx = false
		end,duration * 1000)
		g_MessageMgr:ShowMessage("GuildTerritorialWarsMapWnd_TeleportFailed_Alert")
	else
		Gac2Gas.RequestTerritoryWarTeleport(gridId, 0)
	end
end

function LuaGuildTerritorialWarsMapWnd:OnSendGridId2Level(gridId2Level)
	for id, level in pairs(gridId2Level) do
		self.m_BigMapGridData[id].level = level
		local obj = self.m_BigMapGridData[id].go
		if LuaGuildTerritorialWarsMgr.m_GridInfo then
			self:InitGridItem(obj, id)
		end
	end
	self:InitMapView()
end

function LuaGuildTerritorialWarsMapWnd:OnSyncTerritoryWarPlayProgress()
	self.OccupySlider.gameObject:SetActive(not LuaGuildTerritorialWarsMgr:IsPeripheryPlay() and not LuaGuildTerritorialWarsMgr:IsChallengePlayOpen())
	self.OccupySlider.value = LuaGuildTerritorialWarsMgr.m_ProgressList[0] and (LuaGuildTerritorialWarsMgr.m_ProgressList[0] / 100) or 0
	self.OccupyProgressLabel.text = math.floor(LuaGuildTerritorialWarsMgr.m_ProgressList[0] and LuaGuildTerritorialWarsMgr.m_ProgressList[0] or 0) .."%"
end

function LuaGuildTerritorialWarsMapWnd:OnSendTerritoryWarMapDetail()
	self:UpdateBigMapViewTopLabel()
	self:ShowJiantouFx()
	self:UpdateMyOccupyNumLabel()
	for id,t in pairs(self.m_BigMapGridData) do
		local obj = t.go
		if LuaGuildTerritorialWarsMgr.m_GridInfo then
			self:InitGridItem(obj, id)
		end
	end
	if not LuaGuildTerritorialWarsMgr.m_IsShowBigMap then
		LuaGuildTerritorialWarsMgr.m_GridTipData = self.m_BigMapGridData[LuaGuildTerritorialWarsMgr.m_CurPosGridId]
		self.SmallMapSituationView.gameObject:SetActive(false)
		self.SmallMapSituationView.gameObject:SetActive(not LuaGuildTerritorialWarsMgr:IsChallengePlayOpen())
	end
	if self.m_IsInitForCenterGrid then return end
	self.m_IsInitForCenterGrid = true
	self:InitForCenterGrid()
end

function LuaGuildTerritorialWarsMapWnd:InitForCenterGrid()
	self.m_CenterGridId = self.m_JunTianZhuChengId
	if LuaGuildTerritorialWarsMgr.m_CurPosGridId ~= 0 and (LuaGuildTerritorialWarsMgr:IsInPlay() or LuaGuildTerritorialWarsMgr:IsPeripheryPlay() ) then
		self.m_CenterGridId = LuaGuildTerritorialWarsMgr.m_CurPosGridId
	end
	local val = (1.5 - 1)/(self.m_BigMapMaxScale - 1)
	if val == self.ScaleSlider.value then
		self:OnScaleSliderChangeValue(val)
	end
	self.ScaleSlider.value = val
end
--@endregion BigMap

--@region SmallMap

function LuaGuildTerritorialWarsMapWnd:OnUpdateGuildLeagueInfo(argv)
	local infos = argv[0]
	if not self.m_MonsterItems then
		self.m_MonsterItems = {}
	end
	for i = 1,#self.m_MonsterItems do
		self.m_MonsterItems[i].gameObject:SetActive(false)
	end
	for i = 0, infos.Count - 1 do
		local info = infos[i]
		local item = self.m_MonsterItems[i]
		if not item then
			item = NGUITools.AddChild(self.SmallMapRoot.gameObject,self.MonsterTemplate.gameObject)
			item.gameObject:SetActive(true)
			self.m_MonsterItems[i] = item
		else
			item.gameObject:SetActive(true)
		end
		local icon = item.transform:Find("Icon"):GetComponent(typeof(CUITexture))
		local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
		local pos = Utility.GridPos2WorldPos(info.posX,info.posY)
		local data = Monster_Monster.GetData(info.templateId)
		icon:LoadNPCPortrait(data.HeadIcon)
		nameLabel.text = data.Name
		item.transform.localPosition = self:CalculateMapPos(pos)
	end
end

function LuaGuildTerritorialWarsMapWnd:UpdateObjPosInfo(argv)
	local playerList = argv[0]
	if not self.m_PlayerMarkList then
		self.m_PlayerMarkList = {}
	end
	local index = 1
	local mainplayerEngineId = CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.EngineId or 0
	for i = 0, playerList.Count - 1, 6 do
		local playerId = math.floor(tonumber(playerList[i] or 0))
		local engineId = math.floor(tonumber(playerList[i + 1] or 0))
		if engineId ~= mainplayerEngineId then 
			local x = math.floor(tonumber(playerList[i + 2] or 0))
			local z = math.floor(tonumber(playerList[i + 3] or 0))
			local pos = Utility.GridPos2WorldPos(x,z)
			local obj = self.m_PlayerMarkList[index]
			if not obj then
				obj = NGUITools.AddChild(self.SmallMapRoot.gameObject,self.MarkTemplate.gameObject)
				local sprite = obj:GetComponent(typeof(UISprite))
				sprite.spriteName = "minimap_teammember"
				obj.gameObject:SetActive(true)
				self.m_PlayerMarkList[index] = obj 
			end
			obj.transform.localPosition = self:CalculateMapPos(pos)
			index = index + 1
		end
	end
end

function LuaGuildTerritorialWarsMapWnd:QueryEnemyPlayerPosResult( dataUD )
	if not self.m_EnemyPlayerMarkList then
		self.m_EnemyPlayerMarkList = {}
	end
	local index = 1
	local playerList = MsgPackImpl.unpack(dataUD)
	for i = 0, playerList.Count - 1, 4 do
		local x = math.floor(tonumber(playerList[i + 2] or 0))
		local z = math.floor(tonumber(playerList[i + 3] or 0))
		local pos = Utility.GridPos2WorldPos(x,z)
		local obj = self.m_EnemyPlayerMarkList[index]
		if not obj then
			obj = NGUITools.AddChild(self.SmallMapRoot.gameObject,self.MarkTemplate.gameObject)
			local sprite = obj:GetComponent(typeof(UISprite))
			sprite.spriteName = "minimap_otherplayer"
			obj.gameObject:SetActive(true)
			self.m_EnemyPlayerMarkList[index] = obj
		end
		obj.transform.localPosition = self:CalculateMapPos(pos)
		index = index + 1
	end
end

function LuaGuildTerritorialWarsMapWnd:CalculateMapPos(worldPos)
	local helpCamera = CMiniMap.Instance.helpCamera
	local viewPos = helpCamera:WorldToViewportPoint(worldPos)
	viewPos.x = (viewPos.x - 0.5)
	viewPos.y = (viewPos.y - 0.5)
	return Vector3(self.SmallMapTexture.width * viewPos.x, self.SmallMapTexture.height * viewPos.y, 0)
end

function LuaGuildTerritorialWarsMapWnd:OnRenderSceneInit()
	self:InitTeleport()
end

function LuaGuildTerritorialWarsMapWnd:InitTeleport()
	if self.m_TeleportList then
		for i = 1,#self.m_TeleportList do
			CResourceMgr.Inst:Destroy(self.m_TeleportList[i])
		end
	end
	self.m_TeleportList = {}
	local landTeleportCoordinates = GuildTerritoryWar_Setting.GetData().LandTeleportCoordinates
	for i = 0, landTeleportCoordinates.Length - 1 do
		local labelText,x,y = landTeleportCoordinates[i][0],tonumber(landTeleportCoordinates[i][1]) ,tonumber(landTeleportCoordinates[i][2])
		local pos = Utility.GridPos2WorldPos(x,y)
		local obj = NGUITools.AddChild(self.SmallMapRoot.gameObject,self.TeleportMarkTemplate.gameObject)
		obj:SetActive(true)
		obj.transform.localPosition = self:CalculateMapPos(pos)
		local label = obj.transform:Find("Label"):GetComponent(typeof(UILabel))
		label.text = labelText
		label.gameObject:SetActive(true)
		table.insert(self.m_TeleportList, obj)
	end
	self:ShowMainPlayerSpirite()
end

function LuaGuildTerritorialWarsMapWnd:UpdateNpcInfo(args)
	local npcList = args[0]
	for i = 0, npcList.Count - 1, 4 do
		local templateId = npcList[i + 1]
		if CommonDefs.HashSetContains(GuildTerritoryWar_Setting.GetData().MapNpcIds, typeof(UInt32), templateId)  then
			local pos = Utility.GridPos2WorldPos(npcList[i + 2], npcList[i + 3])
			local data = NPC_NPC.GetData(templateId)
			local obj = NGUITools.AddChild(self.SmallMapRoot.gameObject,self.TeleportMarkTemplate.gameObject)
			obj:SetActive(true)
			obj.transform.localPosition = self:CalculateMapPos(pos)
			local label = obj.transform:Find("Label"):GetComponent(typeof(UILabel))
			label.text = data.Name
			label.gameObject:SetActive(true)
		end
	end
end

function LuaGuildTerritorialWarsMapWnd:ShowJiantouFx()
	if self.m_IsShowJiantouFx then return end
	if LuaGuildTerritorialWarsMgr.m_IsShowBigMap then return end
	local capturePointCoordinates = GuildTerritoryWar_Setting.GetData().CapturePointCoordinates
	local x,y,labelText = tonumber(capturePointCoordinates[0]),tonumber(capturePointCoordinates[1]) ,capturePointCoordinates[2] 
	local pos = Utility.GridPos2WorldPos(x,y)
	local obj = NGUITools.AddChild(self.SmallMapRoot.gameObject,self.TeleportMarkTemplate.gameObject)
	obj:SetActive(false)
	obj.transform.localPosition = self:CalculateMapPos(pos)
	local label = obj.transform:Find("Label"):GetComponent(typeof(UILabel))
	label.text = labelText
	self.SmallMapTipFx2.transform.position = obj.transform.position
	local isMyArea = LuaGuildTerritorialWarsMgr:IsMyArea(LuaGuildTerritorialWarsMgr.m_CurPosGridId)
	local isFriendArea = LuaGuildTerritorialWarsMgr:IsFriendArea(LuaGuildTerritorialWarsMgr.m_CurPosGridId)
	if not isMyArea and not isFriendArea and not LuaGuildTerritorialWarsMgr:IsPeripheryPlay() then
		self.SmallMapTipFx2:LoadFx("fx/ui/prefab/UI_Jiantou.prefab")
	end
	self.m_IsShowJiantouFx = true
end
--@endregion SmallMap

--@region UIEvent
function LuaGuildTerritorialWarsMapWnd:OnResetButtonClick()
	self.ScaleSlider.value = 1
	self:InitForCenterGrid()
end

function LuaGuildTerritorialWarsMapWnd:OnSwitchButtonClick()
	LuaGuildTerritorialWarsMgr.m_IsShowBigMap = not LuaGuildTerritorialWarsMgr.m_IsShowBigMap
	self:InitMapView()
end

function LuaGuildTerritorialWarsMapWnd:OnRoutingButtonClick()
	CUIManager.ShowUI(CUIResources.MiniMapRoutingWnd)
	CMiniMapRoutingWnd.RouteTo = DelegateFactory.Func_int_int_bool(function (x, y)
		CTrackMgr.Inst:Track(CPos(x, y), Utility.Grid2Pixel(2.0), nil, nil, true)
		CUIManager.CloseUI(CLuaUIResources.GuildTerritorialWarsMapWnd)
		return true
	end)
end

function LuaGuildTerritorialWarsMapWnd:OnTipButtonClick()
	g_MessageMgr:ShowMessage("GuildTerritorialWarsMapWnd_ReadMe")
end

function LuaGuildTerritorialWarsMapWnd:OnLegendButtonClick()
	if CGuideMgr.Inst:IsInPhase(EnumGuideKey.GuildTerritorialWarsMap) and CGuideMgr.Inst:IsInSubPhase(1) then
		return
	end
	CUIManager.ShowUI(CLuaUIResources.GuildTerritorialWarsMapIconWnd)
end

function LuaGuildTerritorialWarsMapWnd:OnIncreaseScaleButtonClick()
	local scale = math.floor(self.m_BigMapScale * 10) / 10
	scale = math.min((self.m_BigMapScale + 0.1),self.m_BigMapMaxScale)
	local val = (scale - 1) /(self.m_BigMapMaxScale - 1)
	self.ScaleSlider.value = val
end

function LuaGuildTerritorialWarsMapWnd:OnReduceScaleButtonClick()
	local scale = math.max(self.m_BigMapScale - 0.1,1)
	scale = math.floor(scale * 10) / 10
	local val = (scale - 1) /(self.m_BigMapMaxScale - 1)
	self.ScaleSlider.value = val
end

function LuaGuildTerritorialWarsMapWnd:OnSituatuionButtonClick()
	if LuaGuildTerritorialWarsMgr:IsChallengePlayOpen() then
		LuaBattleSituationMgr.OpenBattleSituationWnd(EnumSituationType.eGuildTerritorialWarsChallenge)
		return
	end
	Gac2Gas.RequestTerritoryWarScoreData()
end

function LuaGuildTerritorialWarsMapWnd:OnRebackCityButtonClick()
	Gac2Gas.RequestTerritoryWarTeleport(0, 0)
end

function LuaGuildTerritorialWarsMapWnd:OnRebackGuildButtonClick()
	Gac2Gas.RequestLeavePlay()
end

function LuaGuildTerritorialWarsMapWnd:OnScaleSliderChangeValue(value)
	local scale = math.lerp(1,self.m_BigMapMaxScale,value)
	self.m_BigMapScale = scale
	self.Map.transform.localScale = Vector3(scale,scale,1)
	self:LoadMapCenterGridId(self.m_CenterGridId)
	self.IncreaseScaleButton.Enabled = value < 1
	self.ReduceScaleButton.Enabled = value > 0
	
	local isShowMostInfo = self.m_BigMapScale > 1.25 and not LuaGuildTerritorialWarsMgr:IsPeripheryPlay() and not LuaGuildTerritorialWarsMgr:IsChallengePlayOpen()

	if isShowMostInfo ~= self.m_IsShowMostInfo then
		self.m_IsShowMostInfo = isShowMostInfo
		for id,t in pairs(self.m_BigMapGridData) do
			local obj = t.go
			if LuaGuildTerritorialWarsMgr.m_GridInfo then
				self:InitGridItem(obj, id)
			end
		end
	end

	self:InitAxisFigureObjectsPos()
end

function LuaGuildTerritorialWarsMapWnd:OnMapRadioBoxSelected(btn,index)
	local id = self.m_RadioBoxIndex2GridId[index]
	local data = self.m_BigMapGridData[id]
	local selectX,selectY = data.x,data.y
	for x, t in pairs(self.m_XAxisFigureObjectsList) do
		t.label.color = x == selectX and Color.yellow or Color.black
	end

	for y, t in pairs(self.m_YAxisFigureObjectsList) do
		t.label.color = y == selectY and Color.yellow or Color.black
	end
	if LuaGuildTerritorialWarsMgr:IsChallengePlayOpen() then
		if LuaGuildTerritorialWarsMgr.m_ChallengeBattleFieldInfo and LuaGuildTerritorialWarsMgr.m_ChallengeBattleFieldInfo.gridInfo and LuaGuildTerritorialWarsMgr.m_ChallengeBattleFieldInfo.gridInfo[id] then
			local items = CreateFromClass(MakeGenericClass(List, PopupMenuItemData))
			local action = PopupMenuItemData(LocalString.GetString("传送前往"), DelegateFactory.Action_int(function (index) 
				self:OnChallengePlayTeleportButtonClick(id)
			end), false, nil)
			CommonDefs.ListAdd(items, typeof(PopupMenuItemData), action)
			CPopupMenuInfoMgr.ShowPopupMenu(CommonDefs.ListToArray(items), btn.transform, CPopupMenuInfoMgr.AlignType.Right)
		else
			g_MessageMgr:ShowMessage("GuildTerritorialWars_ChallengePlay_NoneBoss")
		end
		return
	end
	LuaGuildTerritorialWarsMgr:ShowMapGridTip(data,btn)
end

function LuaGuildTerritorialWarsMapWnd:OnChallengePlayTeleportButtonClick(id)
	local t = LuaGuildTerritorialWarsMgr.m_ChallengeBattleFieldInfo
	if not t then return end
	if t.curbossIdx == t.gridInfo[id].bossIdx then
		g_MessageMgr:ShowMessage("Message_ChuanSong_Current_Scene_Tips")	
		return
	end
	local isLock = t and t.gridInfo and t.gridInfo[id].state == 3
	if isLock then
		g_MessageMgr:ShowMessage("GTW_CHALLENGE_BOSS_LOCKED")	
		return
	end
	Gac2Gas.RequestEnterGTWChallengePlay(LuaGuildTerritorialWarsMgr.m_ChallengeIdx,t.gridInfo[id].bossIdx)
end

function LuaGuildTerritorialWarsMapWnd:OnSmallMapSituationViewArrowClick()
	LuaGuildTerritorialWarsMgr.m_GridTipData = self.m_BigMapGridData[LuaGuildTerritorialWarsMgr.m_CurPosGridId]
	self.SmallMapSituationView.gameObject:SetActive(not self.SmallMapSituationView.gameObject.activeSelf)
	self.HideSmallMapSituationView.gameObject:SetActive(false)
end

function LuaGuildTerritorialWarsMapWnd:OnDragMapStart()
	self.m_IsDragging = true
	self.m_LastDragPos = Input.mousePosition
	if CommonDefs.IsInMobileDevice() then
		self.m_FingerIndex = Input.GetTouch(0).fingerId
		self.m_LastDragPos = Input.GetTouch(0).position
		self.m_LastDragPos = Vector3(self.m_LastDragPos.x,self.m_LastDragPos.y, 0)
	end
	self.m_MapCachePos = CUIManager.UIMainCamera:ScreenToWorldPoint(self.m_LastDragPos)
end

function LuaGuildTerritorialWarsMapWnd:OnSmallMapTextureClick()
	local worldPos = UICamera.lastWorldPosition
	local localPos = self.SmallMapTexture.transform:InverseTransformPoint(worldPos)
	self.SmallMapTipFx.transform.position = worldPos
	self.SmallMapTipFx.gameObject:SetActive(true)
    self.SmallMapTipFx:LoadFx("fx/ui/prefab/UI_ditudianji.prefab")
	local viewPos = Vector3(localPos.x / self.SmallMapTexture.width + 0.5, localPos.y / self.SmallMapTexture.height + 0.5, 0)
	local helpCamera = CMiniMap.Instance.helpCamera
	local ray = helpCamera:ViewportPointToRay(viewPos)
	local hits = Physics.RaycastAll(ray)
	if hits then
		local list = {}
		for i = 0, hits.Length - 1 do
			table.insert(list, hits[i])
		end
		table.sort(list,function (a,b)
			return a.distance < b.distance
		end)
		local isBreak = false
		for i = 1,#list do
			if not isBreak and list[i].collider:CompareTag("Ground") then
				local pixelPos = Utility.WorldPos2PixelPos(list[i].point)
				if CommonDefs.IS_PUB_RELEASE then
					EventManager.BroadcastInternalForLua(EnumEventType.PlayerClickMiniMap, {pixelPos, nil, nil})
				else
					local hasCtrl = Input.GetKey(KeyCode.LeftControl)
					if not hasCtrl then
						EventManager.BroadcastInternalForLua(EnumEventType.PlayerClickMiniMap, {pixelPos, nil, nil})
					end
				end
				isBreak = true
			end
		end
	end
end

function LuaGuildTerritorialWarsMapWnd:OnColorEnemyAreaQnCheckBoxValueChanged(value)
	self.m_IsPaintingColorEnemyArea = value
	if self.m_BigMapGridData then
		for id,t in pairs(self.m_BigMapGridData) do
			local obj = t.go
			if LuaGuildTerritorialWarsMgr.m_GridInfo then
				self:InitGridItem(obj, id)
			end
		end
	end
end

function LuaGuildTerritorialWarsMapWnd:OnOccupyNumTipButtonClick()
	g_MessageMgr:ShowMessage("GuildTerritorialWarsMapWnd_OccupyNumTip")
end

function LuaGuildTerritorialWarsMapWnd:OnCommandButtonClick()
	local msg = g_MessageMgr:FormatMessage("GuildTerritorialWarsMapWnd_Command_Draw_Confirm")
	MessageWndManager.ShowConfirmMessage(msg, 10, false, DelegateFactory.Action(function () 
		self.m_IsDrawingMap = true
        self.TablePanel.gameObject:SetActive(not self.m_IsDrawingMap)
		self.GuildTerritorialWarsDrawMapPanel.gameObject:SetActive(self.m_IsDrawingMap)
    end), nil)
end

function LuaGuildTerritorialWarsMapWnd:CloseGuildTerritorialWarsDrawMapPanel()
	self.m_IsDrawingMap = false
	self.TablePanel.gameObject:SetActive(not self.m_IsDrawingMap)
	self.GuildTerritorialWarsDrawMapPanel.gameObject:SetActive(self.m_IsDrawingMap)
end

function LuaGuildTerritorialWarsMapWnd:OnCloseButtonClick()
	-- if self.m_IsDrawingMap then
	-- 	local msg = g_MessageMgr:FormatMessage("GuildTerritorialWarsDrawMapPanel_CloseButtonClick_Confirm")
	-- 	MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
	-- 		CUIManager.CloseUI(CLuaUIResources.GuildTerritorialWarsMapWnd)
	-- 	end),nil,LocalString.GetString("确定"),LocalString.GetString("取消"),false)
	-- 	return
	-- end
	CUIManager.CloseUI(CLuaUIResources.GuildTerritorialWarsMapWnd)
end

function LuaGuildTerritorialWarsMapWnd:OnRebackOriginButtonClick()
	Gac2Gas.RequestEnterGTWChallengePlay(LuaGuildTerritorialWarsMgr.m_ChallengeIdx,1)
end

function LuaGuildTerritorialWarsMapWnd:OnRebackBattlegroundButtonClick()
	local t = LuaGuildTerritorialWarsMgr.m_ChallengeBattleFieldInfo
	local battingBossIdx = t and t.battingBossIdx or 1
	Gac2Gas.RequestEnterGTWChallengePlay(LuaGuildTerritorialWarsMgr.m_ChallengeIdx,battingBossIdx)
end

--@endregion UIEvent
function LuaGuildTerritorialWarsMapWnd:OnMainPlayerMoveEnded()
	self.SmallMapTipFx.gameObject:SetActive(false)
	self.MapPathDrawer:ClearCurrentPath()
end

function LuaGuildTerritorialWarsMapWnd:MainPlayerMoveStepped()
	local mainPlayer = CClientMainPlayer.Inst
	if not mainPlayer then return end
	local movePath = mainPlayer.EngineObject.CurrentMovePath
    local wayPoint = mainPlayer.EngineObject.WayPoint
	if not movePath then return end
	local listPos = CreateFromClass(MakeGenericClass(List, Vector3))
	for i = wayPoint, movePath.PathSize - 1 do
		local tmp1 = i
		local tmp2 = 0
		local default
		default, tmp1, tmp2 = movePath:GetPixelPosAt(tmp1, tmp2)
		local pixelPos = default
		local worldPos = Utility.PixelPos2WorldPos(pixelPos)
		local mapPos = self:CalculateMapPos(worldPos)
		CommonDefs.ListAdd(listPos, typeof(Vector3), mapPos)
	end
	--local pos = Utility.PixelPos2GridPos(mainPlayer.Pos)
    local worldPos = Utility.PixelPos2WorldPos(mainPlayer.Pos)
    local playerMapPos = self:CalculateMapPos(worldPos)
	self.MapPathDrawer:UpdatePlayerPath(listPos, playerMapPos)
	local endPos = mainPlayer.EngineObject.EndPos
	local endWorldPos = Utility.PixelPos2WorldPos(endPos)
	local endMapPos = self:CalculateMapPos(endWorldPos)
	self:ShowMainPlayerSpirite()
	self.SmallMapTipFx.transform.localPosition = endMapPos
	self.SmallMapTipFx.gameObject:SetActive(true)
    self.SmallMapTipFx:LoadFx("fx/ui/prefab/UI_ditudianji.prefab")
end

function LuaGuildTerritorialWarsMapWnd:ShowMainPlayerSpirite()
	local mainPlayer = CClientMainPlayer.Inst
	if not mainPlayer then return end
	--local pos = Utility.PixelPos2GridPos(mainPlayer.Pos)
    local worldPos = Utility.PixelPos2WorldPos(mainPlayer.Pos)
    local playerMapPos = self:CalculateMapPos(worldPos)
	self.MainPlayerSpirite.transform.localPosition = playerMapPos
	local dirPos = self:CalculateMapPos(CommonDefs.op_Addition_Vector3_Vector3(worldPos, CommonDefs.op_Multiply_Vector3_Single(mainPlayer.RO.transform.forward, 100)))
    local viewdir = CommonDefs.op_Subtraction_Vector3_Vector3(dirPos, playerMapPos)
    if viewdir.magnitude > 0.1 then
        local angle = math.asin(viewdir.y / viewdir.magnitude) / math.pi * 180
        if viewdir.x < 0 then
            angle = 180 - angle
        end
        self.MainPlayerSpirite.transform.localEulerAngles = Vector3(0, 0, angle)
    end
end

function LuaGuildTerritorialWarsMapWnd:Update()
	if nil == self.m_LastDragPos then
        self.m_LastDragPos = Vector3.zero
    end
    if not self.m_IsDragging then
        return
    end

	if CommonDefs.IsInMobileDevice() then
        for i = 0,Input.touchCount - 1 do
            local touch = Input.GetTouch(i)
            if touch.fingerId == self.m_FingerIndex then
                if touch.phase ~= TouchPhase.Ended and touch.phase ~= TouchPhase.Canceled then
                    if touch.phase ~= TouchPhase.Stationary then
						local pos = touch.position
                        self.m_LastDragPos = Vector3(pos.x, pos.y, 0)
						self:UpdateMap()
                    end
                    return
                else
                    break
                end
            end
        end
    else
        if Input.GetMouseButton(0) then
            self.m_LastDragPos = Input.mousePosition
			self:UpdateMap()
            if not Input.GetMouseButtonUp(0) then
                return
            end
        end
    end

    self.m_IsDragging = false
    self.m_FingerIndex = -1
end

function LuaGuildTerritorialWarsMapWnd:UpdateMap()
	local mouseWorldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(Vector3(self.m_LastDragPos.x,self.m_LastDragPos.y,0))
	local subPos = CommonDefs.op_Subtraction_Vector3_Vector3(mouseWorldPos, self.m_MapCachePos)
	local worldPos = CommonDefs.op_Addition_Vector3_Vector3(subPos, self.Map.transform.position)
	local localPos = self.MapPanel.transform:InverseTransformPoint(worldPos)
	local size = self.MapPanel:GetViewSize()
	local mapWidth, mapHeight = size.x, size.y 
	local scale = 0.5 * self.m_BigMapScale - 0.5
	local offsetX, offsetY = localPos.x, localPos.y 
	mapWidth, mapHeight = mapWidth * scale, mapHeight * scale
	offsetX = math.max(-mapWidth, math.min(offsetX, mapWidth))
	offsetY = math.max(-mapHeight, math.min(offsetY, mapHeight))
	self.Map.transform.localPosition = Vector3(offsetX,offsetY, 0)
	self.m_MapCachePos = mouseWorldPos

	self:InitAxisFigureObjectsPos()
end

function LuaGuildTerritorialWarsMapWnd:GetGuideGo(methodName)
	if LuaGuildTerritorialWarsMgr:IsInPlay() then
		if methodName=="GetResetButton" then
			return self.ResetButton.gameObject
		elseif methodName=="GetLegendButton" then
			return self.LegendButton.gameObject
		elseif methodName=="GetScaleSlider" then
			return self.ScaleSlider.transform:Find("Container").gameObject
		elseif methodName=="GetCity" then
			local gridId  = self.m_CenterGridId
			local data = self.m_BigMapGridData[gridId]
			local go = data and data.go
			return go
		elseif methodName=="GetSituatuionButton" then
			return self.SituatuionButton.gameObject
		end
	elseif LuaGuildTerritorialWarsMgr:IsInGuide() then
		if methodName=="GetGuideGridButton" then
			return self:GetGridButtonNearBy(LuaGuildTerritorialWarsMgr.m_GuideGridId)
		end
	elseif LuaGuildTerritorialWarsMgr:IsPeripheryPlay()then
		if methodName == "GetCloseButton" then
			return self.CloseButton.gameObject
		elseif methodName =="GetGuideNearByGridButton" then
			return self:GetGridButtonNearBy(LuaGuildTerritorialWarsMgr.m_CurPosGridId)
		end
	end
	return nil
end

function LuaGuildTerritorialWarsMapWnd:GetGridButtonNearBy(gridId)
	local data = self.m_BigMapGridData[gridId]
	if not data then return nil end
	local x,y = data.x, data.y
	local index = x * self.m_BigMapXSize + y
	local indexArr = {index - self.m_BigMapXSize, index + self.m_BigMapXSize,y > 1 and (index - 1) or 0, y < self.m_BigMapYSize and (index + 1) or 0}
	for _,index in pairs(indexArr) do
		local id = self.m_PosIndex2GridId[index]
		local t = self.m_BigMapGridData[id]
		if t then
			gridId = id
			break
		end
	end
	local data = self.m_BigMapGridData[gridId]
	local go = data and data.go
	return go
end