-- Auto Generated!!
local CChargeActivityAwardTemplate = import "L10.UI.CChargeActivityAwardTemplate"
local Charge_ShopActivityGift = import "L10.Game.Charge_ShopActivityGift"
local CQnReturnAwardTemplate = import "L10.UI.CQnReturnAwardTemplate"
local Extensions = import "Extensions"
local Item_Item = import "L10.Game.Item_Item"
local NGUITools = import "NGUITools"
local StringSplitOptions = import "System.StringSplitOptions"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CChargeActivityAwardTemplate.m_Init_CS2LuaHook = function (this, actID, giftID, needChargeAmount, currentChargeAmount, receivedAward, index) 
    this.chargeAmountLabel.text = tostring(needChargeAmount)
    this.giftID = giftID
    this.actID = actID
    this.index = index
    this.needCharge = needChargeAmount
    this.currentCharge = currentChargeAmount
    this.receivedAward = receivedAward
    Extensions.RemoveAllChildren(this.awardTable.transform)
    this.awardTemplate.gameObject:SetActive(false)
    this.yuanbaoAward.gameObject:SetActive(false)
    this.jadeAward.gameObject:SetActive(false)
    this:InitGift()
    this:InitProgress()
    UIEventListener.Get(this.receiveButton).onClick = MakeDelegateFromCSFunction(this.OnReceiveButtonClick, VoidDelegate, this)
end
CChargeActivityAwardTemplate.m_InitProgress_CS2LuaHook = function (this) 
    this.progress.gameObject:SetActive(false)
    this.receiveButton:SetActive(false)
    this.alreadyReceiveTag:SetActive(false)
    if this.receivedAward then
        this.alreadyReceiveTag:SetActive(true)
    elseif this.needCharge <= this.currentCharge then
        this.receiveButton:SetActive(true)
    else
        this.progressValue.text = System.String.Format("{0}/{1}", this.currentCharge, this.needCharge)
        this.progress.gameObject:SetActive(true)
        this.progress.value = this.currentCharge / this.needCharge
    end
end
CChargeActivityAwardTemplate.m_InitGift_CS2LuaHook = function (this) 
    local gift = Charge_ShopActivityGift.GetData(this.giftID)
    if gift == nil then
        return
    end
    if not System.String.IsNullOrEmpty(gift.Item) then
        this:InitAwardItem(gift.Item)
    elseif gift.YuanBao > 0 then
        this.yuanbaoAward.text = tostring(gift.YuanBao)
        this.yuanbaoAward.gameObject:SetActive(true)
    elseif gift.Jade > 0 then
        this.jadeAward.text = tostring(gift.Jade)
        this.jadeAward.gameObject:SetActive(true)
    end
    this.awardTable:Reposition()
    this.background.height = this.baseBackgroundHeight + math.floor((this.awardTable.transform.childCount - 1) / this.awardTable.columns) * this.awardTemplate.height
end
CChargeActivityAwardTemplate.m_InitAwardItem_CS2LuaHook = function (this, data) 
    local list = CommonDefs.StringSplit_ArrayChar_StringSplitOptions(data, Table2ArrayWithCount({",", ";"}, 2, MakeArrayClass(String)), StringSplitOptions.RemoveEmptyEntries)
    if list ~= nil and list.Length % 2 == 0 then
        do
            local i = 0
            while i < list.Length do
                local instance = NGUITools.AddChild(this.awardTable.gameObject, this.awardTemplate.gameObject)
                instance:SetActive(true)
                local templateId, amount
                local default
                default, templateId = System.UInt32.TryParse(list[i])
                local extern
                extern, amount = System.UInt32.TryParse(list[i + 1])
                if default and extern then
                    local item = Item_Item.GetData(templateId)
                    local template = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CQnReturnAwardTemplate))
                    if template ~= nil and item ~= nil then
                        template:Init(item, amount)
                    end
                end
                i = i + 2
            end
        end
    end
end
