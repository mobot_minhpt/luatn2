local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CUIFx = import "L10.UI.CUIFx"
local UIProgressBar = import "UIProgressBar"

LuaGuildCustomBuildPreTaskView = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaGuildCustomBuildPreTaskView, "Fx", "Fx", CUIFx)
RegistChildComponent(LuaGuildCustomBuildPreTaskView, "Progress", "Progress", UIProgressBar)

--@endregion RegistChildComponent end

function LuaGuildCustomBuildPreTaskView:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    UIEventListener.Get(self.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBtnClick()
	end)

    self:InitProgress()
end

function LuaGuildCustomBuildPreTaskView:OnEnable()
	g_ScriptEvent:AddListener("GuildCustomBuildPreTaskSyncStatus", self, "InitProgress")
	
end

function LuaGuildCustomBuildPreTaskView:OnDisable()
	g_ScriptEvent:RemoveListener("GuildCustomBuildPreTaskSyncStatus", self, "InitProgress")
end

function LuaGuildCustomBuildPreTaskView:OnBtnClick()
    LuaGuildCustomBuildMgr.OpenPreTaskProgressWnd()
end

function LuaGuildCustomBuildPreTaskView:InitProgress(st)
    local status = LuaGuildCustomBuildMgr.mPreTaskStatus
	local setting = GuildCustomBuildPreTask_GameSetting.GetData()
	local taskCount = setting.PreTaskIds.Length

    local progress = 0
	if status <= EnumGuildCustomBuildPreTaskStatus.eTask1 then
		progress = 0
	else
		progress = (status-EnumGuildCustomBuildPreTaskStatus.eTask1)/taskCount
	end
	self.Progress.value = progress
end
--@region UIEvent

--@endregion UIEvent

