local max = math.max
local min = math.min
local abs = math.abs
local ceil = math.ceil
local floor = math.floor
local log = math.log
local power = function(a, b) return a^b end

local EnumPlayerFightProp = {
	PermanentCor = 1,
	PermanentSta = 2,
	PermanentStr = 3,
	PermanentInt = 4,
	PermanentAgi = 5,
	PermanentHpFull = 6,
	Hp = 7,
	Mp = 8,
	CurrentHpFull = 9,
	CurrentMpFull = 10,
	EquipExchangeMF = 11,
	RevisePermanentCor = 12,
	RevisePermanentSta = 13,
	RevisePermanentStr = 14,
	RevisePermanentInt = 15,
	RevisePermanentAgi = 16,
	Class = 2001,
	Grade = 2002,
	Cor = 2003,
	AdjCor = 2004,
	MulCor = 2005,
	Sta = 2006,
	AdjSta = 2007,
	MulSta = 2008,
	Str = 2009,
	AdjStr = 2010,
	MulStr = 2011,
	Int = 2012,
	AdjInt = 2013,
	MulInt = 2014,
	Agi = 2015,
	AdjAgi = 2016,
	MulAgi = 2017,
	HpFull = 2018,
	AdjHpFull = 2019,
	MulHpFull = 2020,
	HpRecover = 2021,
	AdjHpRecover = 2022,
	MulHpRecover = 2023,
	MpFull = 2024,
	AdjMpFull = 2025,
	MulMpFull = 2026,
	MpRecover = 2027,
	AdjMpRecover = 2028,
	MulMpRecover = 2029,
	pAttMin = 2030,
	AdjpAttMin = 2031,
	MulpAttMin = 2032,
	pAttMax = 2033,
	AdjpAttMax = 2034,
	MulpAttMax = 2035,
	pHit = 2036,
	AdjpHit = 2037,
	MulpHit = 2038,
	pMiss = 2039,
	AdjpMiss = 2040,
	MulpMiss = 2041,
	pDef = 2042,
	AdjpDef = 2043,
	MulpDef = 2044,
	AdjpHurt = 2045,
	MulpHurt = 2046,
	pSpeed = 2047,
	OripSpeed = 2048,
	MulpSpeed = 2049,
	pFatal = 2050,
	AdjpFatal = 2051,
	AntipFatal = 2052,
	AdjAntipFatal = 2053,
	pFatalDamage = 2054,
	AdjpFatalDamage = 2055,
	AntipFatalDamage = 2056,
	AdjAntipFatalDamage = 2057,
	Block = 2058,
	AdjBlock = 2059,
	MulBlock = 2060,
	BlockDamage = 2061,
	AdjBlockDamage = 2062,
	AntiBlock = 2063,
	AdjAntiBlock = 2064,
	AntiBlockDamage = 2065,
	AdjAntiBlockDamage = 2066,
	mAttMin = 2067,
	AdjmAttMin = 2068,
	MulmAttMin = 2069,
	mAttMax = 2070,
	AdjmAttMax = 2071,
	MulmAttMax = 2072,
	mHit = 2073,
	AdjmHit = 2074,
	MulmHit = 2075,
	mMiss = 2076,
	AdjmMiss = 2077,
	MulmMiss = 2078,
	mDef = 2079,
	AdjmDef = 2080,
	MulmDef = 2081,
	AdjmHurt = 2082,
	MulmHurt = 2083,
	mSpeed = 2084,
	OrimSpeed = 2085,
	MulmSpeed = 2086,
	mFatal = 2087,
	AdjmFatal = 2088,
	AntimFatal = 2089,
	AdjAntimFatal = 2090,
	mFatalDamage = 2091,
	AdjmFatalDamage = 2092,
	AntimFatalDamage = 2093,
	AdjAntimFatalDamage = 2094,
	EnhanceFire = 2095,
	AdjEnhanceFire = 2096,
	MulEnhanceFire = 2097,
	EnhanceThunder = 2098,
	AdjEnhanceThunder = 2099,
	MulEnhanceThunder = 2100,
	EnhanceIce = 2101,
	AdjEnhanceIce = 2102,
	MulEnhanceIce = 2103,
	EnhancePoison = 2104,
	AdjEnhancePoison = 2105,
	MulEnhancePoison = 2106,
	EnhanceWind = 2107,
	AdjEnhanceWind = 2108,
	MulEnhanceWind = 2109,
	EnhanceLight = 2110,
	AdjEnhanceLight = 2111,
	MulEnhanceLight = 2112,
	EnhanceIllusion = 2113,
	AdjEnhanceIllusion = 2114,
	MulEnhanceIllusion = 2115,
	AntiFire = 2116,
	AdjAntiFire = 2117,
	MulAntiFire = 2118,
	AntiThunder = 2119,
	AdjAntiThunder = 2120,
	MulAntiThunder = 2121,
	AntiIce = 2122,
	AdjAntiIce = 2123,
	MulAntiIce = 2124,
	AntiPoison = 2125,
	AdjAntiPoison = 2126,
	MulAntiPoison = 2127,
	AntiWind = 2128,
	AdjAntiWind = 2129,
	MulAntiWind = 2130,
	AntiLight = 2131,
	AdjAntiLight = 2132,
	MulAntiLight = 2133,
	AntiIllusion = 2134,
	AdjAntiIllusion = 2135,
	MulAntiIllusion = 2136,
	IgnoreAntiFire = 2137,
	IgnoreAntiThunder = 2138,
	IgnoreAntiIce = 2139,
	IgnoreAntiPoison = 2140,
	IgnoreAntiWind = 2141,
	IgnoreAntiLight = 2142,
	IgnoreAntiIllusion = 2143,
	EnhanceDizzy = 2144,
	AdjEnhanceDizzy = 2145,
	MulEnhanceDizzy = 2146,
	EnhanceSleep = 2147,
	AdjEnhanceSleep = 2148,
	MulEnhanceSleep = 2149,
	EnhanceChaos = 2150,
	AdjEnhanceChaos = 2151,
	MulEnhanceChaos = 2152,
	EnhanceBind = 2153,
	AdjEnhanceBind = 2154,
	MulEnhanceBind = 2155,
	EnhanceSilence = 2156,
	AdjEnhanceSilence = 2157,
	MulEnhanceSilence = 2158,
	AntiDizzy = 2159,
	AdjAntiDizzy = 2160,
	MulAntiDizzy = 2161,
	AntiSleep = 2162,
	AdjAntiSleep = 2163,
	MulAntiSleep = 2164,
	AntiChaos = 2165,
	AdjAntiChaos = 2166,
	MulAntiChaos = 2167,
	AntiBind = 2168,
	AdjAntiBind = 2169,
	MulAntiBind = 2170,
	AntiSilence = 2171,
	AdjAntiSilence = 2172,
	MulAntiSilence = 2173,
	DizzyTimeChange = 2174,
	SleepTimeChange = 2175,
	ChaosTimeChange = 2176,
	BindTimeChange = 2177,
	SilenceTimeChange = 2178,
	BianhuTimeChange = 2179,
	FreezeTimeChange = 2180,
	PetrifyTimeChange = 2181,
	EnhanceHeal = 2182,
	AdjEnhanceHeal = 2183,
	MulEnhanceHeal = 2184,
	Speed = 2185,
	OriSpeed = 2186,
	AdjSpeed = 2187,
	MulSpeed = 2188,
	MF = 2189,
	AdjMF = 2190,
	Range = 2191,
	AdjRange = 2192,
	HatePlus = 2193,
	AdjHatePlus = 2194,
	HateDecrease = 2195,
	Invisible = 2196,
	AdjInvisible = 2197,
	TrueSight = 2198,
	OriTrueSight = 2199,
	AdjTrueSight = 2200,
	EyeSight = 2201,
	OriEyeSight = 2202,
	AdjEyeSight = 2203,
	EnhanceTian = 2204,
	EnhanceTianMul = 2205,
	EnhanceEGui = 2206,
	EnhanceEGuiMul = 2207,
	EnhanceXiuLuo = 2208,
	EnhanceXiuLuoMul = 2209,
	EnhanceDiYu = 2210,
	EnhanceDiYuMul = 2211,
	EnhanceRen = 2212,
	EnhanceRenMul = 2213,
	EnhanceChuSheng = 2214,
	EnhanceChuShengMul = 2215,
	EnhanceBuilding = 2216,
	EnhanceBuildingMul = 2217,
	EnhanceBoss = 2218,
	EnhanceBossMul = 2219,
	EnhanceSheShou = 2220,
	EnhanceSheShouMul = 2221,
	EnhanceJiaShi = 2222,
	EnhanceJiaShiMul = 2223,
	EnhanceFangShi = 2224,
	EnhanceFangShiMul = 2225,
	EnhanceYiShi = 2226,
	EnhanceYiShiMul = 2227,
	EnhanceMeiZhe = 2228,
	EnhanceMeiZheMul = 2229,
	EnhanceYiRen = 2230,
	EnhanceYiRenMul = 2231,
	IgnorepDef = 2232,
	IgnoremDef = 2233,
	EnhanceZhaoHuan = 2234,
	EnhanceZhaoHuanMul = 2235,
	DizzyProbability = 2236,
	SleepProbability = 2237,
	ChaosProbability = 2238,
	BindProbability = 2239,
	SilenceProbability = 2240,
	HpFullPow2 = 2241,
	HpFullPow1 = 2242,
	HpFullInit = 2243,
	MpFullPow1 = 2244,
	MpFullInit = 2245,
	MpRecoverPow1 = 2246,
	MpRecoverInit = 2247,
	PAttMinPow2 = 2248,
	PAttMinPow1 = 2249,
	PAttMinInit = 2250,
	PAttMaxPow2 = 2251,
	PAttMaxPow1 = 2252,
	PAttMaxInit = 2253,
	PhitPow1 = 2254,
	PHitInit = 2255,
	PMissPow1 = 2256,
	PMissInit = 2257,
	PSpeedPow1 = 2258,
	PSpeedInit = 2259,
	PDefPow1 = 2260,
	PDefInit = 2261,
	PfatalPow1 = 2262,
	PFatalInit = 2263,
	MAttMinPow2 = 2264,
	MAttMinPow1 = 2265,
	MAttMinInit = 2266,
	MAttMaxPow2 = 2267,
	MAttMaxPow1 = 2268,
	MAttMaxInit = 2269,
	MSpeedPow1 = 2270,
	MSpeedInit = 2271,
	MDefPow1 = 2272,
	MDefInit = 2273,
	MHitPow1 = 2274,
	MHitInit = 2275,
	MMissPow1 = 2276,
	MMissInit = 2277,
	MFatalPow1 = 2278,
	MFatalInit = 2279,
	BlockDamagePow1 = 2280,
	BlockDamageInit = 2281,
	RunSpeed = 2282,
	HpRecoverPow1 = 2283,
	HpRecoverInit = 2284,
	AntiBlockDamagePow1 = 2285,
	AntiBlockDamageInit = 2286,
	BBMax = 2287,
	AdjBBMax = 2288,
	AntiBreak = 2289,
	AdjAntiBreak = 2290,
	AntiPushPull = 2291,
	AdjAntiPushPull = 2292,
	AntiPetrify = 2293,
	AdjAntiPetrify = 2294,
	MulAntiPetrify = 2295,
	AntiTie = 2296,
	AdjAntiTie = 2297,
	MulAntiTie = 2298,
	EnhancePetrify = 2299,
	AdjEnhancePetrify = 2300,
	MulEnhancePetrify = 2301,
	EnhanceTie = 2302,
	AdjEnhanceTie = 2303,
	MulEnhanceTie = 2304,
	TieProbability = 2305,
	StrZizhi = 2306,
	CorZizhi = 2307,
	StaZizhi = 2308,
	AgiZizhi = 2309,
	IntZizhi = 2310,
	InitStrZizhi = 2311,
	InitCorZizhi = 2312,
	InitStaZizhi = 2313,
	InitAgiZizhi = 2314,
	InitIntZizhi = 2315,
	Wuxing = 2316,
	Xiuwei = 2317,
	SkillNum = 2318,
	PType = 2319,
	MType = 2320,
	PHType = 2321,
	GrowFactor = 2322,
	WuxingImprove = 2323,
	XiuweiImprove = 2324,
	EnhanceBeHeal = 2325,
	AdjEnhanceBeHeal = 2326,
	MulEnhanceBeHeal = 2327,
	LookUpCor = 2328,
	LookUpSta = 2329,
	LookUpStr = 2330,
	LookUpInt = 2331,
	LookUpAgi = 2332,
	LookUpHpFull = 2333,
	LookUpMpFull = 2334,
	LookUppAttMin = 2335,
	LookUppAttMax = 2336,
	LookUppHit = 2337,
	LookUppMiss = 2338,
	LookUppDef = 2339,
	LookUpmAttMin = 2340,
	LookUpmAttMax = 2341,
	LookUpmHit = 2342,
	LookUpmMiss = 2343,
	LookUpmDef = 2344,
	AdjHpFull2 = 2345,
	AdjpAttMin2 = 2346,
	AdjpAttMax2 = 2347,
	AdjmAttMin2 = 2348,
	AdjmAttMax2 = 2349,
	LingShouType = 2350,
	MHType = 2351,
	EnhanceLingShou = 2352,
	EnhanceLingShouMul = 2353,
	AntiAoe = 2354,
	AdjAntiAoe = 2355,
	AntiBlind = 2356,
	AdjAntiBlind = 2357,
	MulAntiBlind = 2358,
	AntiDecelerate = 2359,
	AdjAntiDecelerate = 2360,
	MulAntiDecelerate = 2361,
	MinGrade = 2362,
	CharacterCor = 2363,
	CharacterSta = 2364,
	CharacterStr = 2365,
	CharacterInt = 2366,
	CharacterAgi = 2367,
	EnhanceDaoKe = 2368,
	EnhanceDaoKeMul = 2369,
	ZuoQiSpeed = 2370,
	EnhanceBianHu = 2371,
	AdjEnhanceBianHu = 2372,
	MulEnhanceBianHu = 2373,
	AntiBianHu = 2374,
	AdjAntiBianHu = 2375,
	MulAntiBianHu = 2376,
	EnhanceXiaKe = 2377,
	EnhanceXiaKeMul = 2378,
	TieTimeChange = 2379,
	EnhanceYanShi = 2380,
	EnhanceYanShiMul = 2381,
	PAType = 2382,
	MAType = 2383,
	lifetimeNoReduceRate = 2384,
	AddLSHpDurgImprove = 2385,
	AddHpDurgImprove = 2386,
	AddMpDurgImprove = 2387,
	FireHurtReduce = 2388,
	ThunderHurtReduce = 2389,
	IceHurtReduce = 2390,
	PoisonHurtReduce = 2391,
	WindHurtReduce = 2392,
	LightHurtReduce = 2393,
	IllusionHurtReduce = 2394,
	FakepDef = 2395,
	FakemDef = 2396,
	EnhanceWater = 2397,
	AdjEnhanceWater = 2398,
	MulEnhanceWater = 2399,
	AntiWater = 2400,
	AdjAntiWater = 2401,
	MulAntiWater = 2402,
	WaterHurtReduce = 2403,
	IgnoreAntiWater = 2404,
	AntiFreeze = 2405,
	AdjAntiFreeze = 2406,
	MulAntiFreeze = 2407,
	EnhanceFreeze = 2408,
	AdjEnhanceFreeze = 2409,
	MulEnhanceFreeze = 2410,
	EnhanceHuaHun = 2411,
	EnhanceHuaHunMul = 2412,
	TrueSightProb = 2413,
	LSBBGrade = 2414,
	LSBBQuality = 2415,
	LSBBAdjHp = 2416,
	AdjIgnoreAntiFire = 2417,
	AdjIgnoreAntiThunder = 2418,
	AdjIgnoreAntiIce = 2419,
	AdjIgnoreAntiPoison = 2420,
	AdjIgnoreAntiWind = 2421,
	AdjIgnoreAntiLight = 2422,
	AdjIgnoreAntiIllusion = 2423,
	IgnoreAntiFireLSMul = 2424,
	IgnoreAntiThunderLSMul = 2425,
	IgnoreAntiIceLSMul = 2426,
	IgnoreAntiPoisonLSMul = 2427,
	IgnoreAntiWindLSMul = 2428,
	IgnoreAntiLightLSMul = 2429,
	IgnoreAntiIllusionLSMul = 2430,
	AdjIgnoreAntiWater = 2431,
	IgnoreAntiWaterLSMul = 2432,
	MulSpeed2 = 2433,
	MulConsumeMp = 2434,
	AdjAgi2 = 2435,
	AdjAntiBianHu2 = 2436,
	AdjAntiBind2 = 2437,
	AdjAntiBlind2 = 2438,
	AdjAntiBlock2 = 2439,
	AdjAntiBlockDamage2 = 2440,
	AdjAntiChaos2 = 2441,
	AdjAntiDecelerate2 = 2442,
	AdjAntiDizzy2 = 2443,
	AdjAntiFire2 = 2444,
	AdjAntiFreeze2 = 2445,
	AdjAntiIce2 = 2446,
	AdjAntiIllusion2 = 2447,
	AdjAntiLight2 = 2448,
	AdjAntimFatal2 = 2449,
	AdjAntimFatalDamage2 = 2450,
	AdjAntiPetrify2 = 2451,
	AdjAntipFatal2 = 2452,
	AdjAntipFatalDamage2 = 2453,
	AdjAntiPoison2 = 2454,
	AdjAntiSilence2 = 2455,
	AdjAntiSleep2 = 2456,
	AdjAntiThunder2 = 2457,
	AdjAntiTie2 = 2458,
	AdjAntiWater2 = 2459,
	AdjAntiWind2 = 2460,
	AdjBlock2 = 2461,
	AdjBlockDamage2 = 2462,
	AdjCor2 = 2463,
	AdjEnhanceBianHu2 = 2464,
	AdjEnhanceBind2 = 2465,
	AdjEnhanceChaos2 = 2466,
	AdjEnhanceDizzy2 = 2467,
	AdjEnhanceFire2 = 2468,
	AdjEnhanceFreeze2 = 2469,
	AdjEnhanceIce2 = 2470,
	AdjEnhanceIllusion2 = 2471,
	AdjEnhanceLight2 = 2472,
	AdjEnhancePetrify2 = 2473,
	AdjEnhancePoison2 = 2474,
	AdjEnhanceSilence2 = 2475,
	AdjEnhanceSleep2 = 2476,
	AdjEnhanceThunder2 = 2477,
	AdjEnhanceTie2 = 2478,
	AdjEnhanceWater2 = 2479,
	AdjEnhanceWind2 = 2480,
	AdjIgnoreAntiFire2 = 2481,
	AdjIgnoreAntiIce2 = 2482,
	AdjIgnoreAntiIllusion2 = 2483,
	AdjIgnoreAntiLight2 = 2484,
	AdjIgnoreAntiPoison2 = 2485,
	AdjIgnoreAntiThunder2 = 2486,
	AdjIgnoreAntiWater2 = 2487,
	AdjIgnoreAntiWind2 = 2488,
	AdjInt2 = 2489,
	AdjmDef2 = 2490,
	AdjmFatal2 = 2491,
	AdjmFatalDamage2 = 2492,
	AdjmHit2 = 2493,
	AdjmHurt2 = 2494,
	AdjmMiss2 = 2495,
	AdjmSpeed2 = 2496,
	AdjpDef2 = 2497,
	AdjpFatal2 = 2498,
	AdjpFatalDamage2 = 2499,
	AdjpHit2 = 2500,
	AdjpHurt2 = 2501,
	AdjpMiss2 = 2502,
	AdjpSpeed2 = 2503,
	AdjStr2 = 2504,
	LSpAttMin = 2505,
	LSpAttMax = 2506,
	LSmAttMin = 2507,
	LSmAttMax = 2508,
	LSpFatal = 2509,
	LSpFatalDamage = 2510,
	LSmFatal = 2511,
	LSmFatalDamage = 2512,
	LSAntiBlock = 2513,
	LSAntiBlockDamage = 2514,
	LSIgnoreAntiFire = 2515,
	LSIgnoreAntiThunder = 2516,
	LSIgnoreAntiIce = 2517,
	LSIgnoreAntiPoison = 2518,
	LSIgnoreAntiWind = 2519,
	LSIgnoreAntiLight = 2520,
	LSIgnoreAntiIllusion = 2521,
	LSIgnoreAntiWater = 2522,
	LSpAttMin_adj = 2523,
	LSpAttMax_adj = 2524,
	LSmAttMin_adj = 2525,
	LSmAttMax_adj = 2526,
	LSpFatal_adj = 2527,
	LSpFatalDamage_adj = 2528,
	LSmFatal_adj = 2529,
	LSmFatalDamage_adj = 2530,
	LSAntiBlock_adj = 2531,
	LSAntiBlockDamage_adj = 2532,
	LSIgnoreAntiFire_adj = 2533,
	LSIgnoreAntiThunder_adj = 2534,
	LSIgnoreAntiIce_adj = 2535,
	LSIgnoreAntiPoison_adj = 2536,
	LSIgnoreAntiWind_adj = 2537,
	LSIgnoreAntiLight_adj = 2538,
	LSIgnoreAntiIllusion_adj = 2539,
	LSIgnoreAntiWater_adj = 2540,
	LSpAttMin_jc = 2541,
	LSpAttMax_jc = 2542,
	LSmAttMin_jc = 2543,
	LSmAttMax_jc = 2544,
	LSpFatal_jc = 2545,
	LSpFatalDamage_jc = 2546,
	LSmFatal_jc = 2547,
	LSmFatalDamage_jc = 2548,
	LSAntiBlock_jc = 2549,
	LSAntiBlockDamage_jc = 2550,
	LSIgnoreAntiFire_jc = 2551,
	LSIgnoreAntiThunder_jc = 2552,
	LSIgnoreAntiIce_jc = 2553,
	LSIgnoreAntiPoison_jc = 2554,
	LSIgnoreAntiWind_jc = 2555,
	LSIgnoreAntiLight_jc = 2556,
	LSIgnoreAntiIllusion_jc = 2557,
	LSIgnoreAntiWater_jc = 2558,
	LSIgnoreAntiFire_mul = 2559,
	LSIgnoreAntiThunder_mul = 2560,
	LSIgnoreAntiIce_mul = 2561,
	LSIgnoreAntiPoison_mul = 2562,
	LSIgnoreAntiWind_mul = 2563,
	LSIgnoreAntiLight_mul = 2564,
	LSIgnoreAntiIllusion_mul = 2565,
	LSIgnoreAntiWater_mul = 2566,
	JieBan_Child_QiChangColor = 2567,
	JieBan_LingShou_Ratio = 2568,
	JieBan_LingShou_QinMi = 2569,
	JieBan_LingShou_CorZizhi = 2570,
	JieBan_LingShou_StaZizhi = 2571,
	JieBan_LingShou_StrZizhi = 2572,
	JieBan_LingShou_IntZizhi = 2573,
	JieBan_LingShou_AgiZizhi = 2574,
	Buff_DisplayValue1 = 2575,
	Buff_DisplayValue2 = 2576,
	Buff_DisplayValue3 = 2577,
	Buff_DisplayValue4 = 2578,
	Buff_DisplayValue5 = 2579,
	LSNature = 2580,
	PrayMulti = 2581,
	EnhanceYingLing = 2582,
	EnhanceYingLingMul = 2583,
	EnhanceFlag = 2584,
	EnhanceFlagMul = 2585,
	ObjectType = 2586,
	MonsterType = 2587,
	FightPropType = 2588,
	PlayHpFull = 2589,
	PlayHp = 2590,
	PlayAtt = 2591,
	PlayDef = 2592,
	PlayHit = 2593,
	PlayMiss = 2594,
	EnhanceDieKe = 2595,
	EnhanceDieKeMul = 2596,
	DotRemain = 2597,
	IsConfused = 2598,
	EnhanceSheShouKefu = 2599,
	EnhanceJiaShiKefu = 2600,
	EnhanceDaoKeKefu = 2601,
	EnhanceXiaKeKefu = 2602,
	EnhanceFangShiKefu = 2603,
	EnhanceYiShiKefu = 2604,
	EnhanceMeiZheKefu = 2605,
	EnhanceYiRenKefu = 2606,
	EnhanceYanShiKefu = 2607,
	EnhanceHuaHunKefu = 2608,
	EnhanceYingLingKefu = 2609,
	EnhanceDieKeKefu = 2610,
	AdjustPermanentCor = 2611,
	AdjustPermanentSta = 2612,
	AdjustPermanentStr = 2613,
	AdjustPermanentInt = 2614,
	AdjustPermanentAgi = 2615,
	SoulCoreLevel = 2616,
	AdjustPermanentMidCor = 2617,
	AdjustPermanentMidSta = 2618,
	AdjustPermanentMidStr = 2619,
	AdjustPermanentMidInt = 2620,
	AdjustPermanentMidAgi = 2621,
	SumPermanent = 2622,
	AntiTian = 2623,
	AntiEGui = 2624,
	AntiXiuLuo = 2625,
	AntiDiYu = 2626,
	AntiRen = 2627,
	AntiChuSheng = 2628,
	AntiBuilding = 2629,
	AntiZhaoHuan = 2630,
	AntiLingShou = 2631,
	AntiBoss = 2632,
	AntiSheShou = 2633,
	AntiJiaShi = 2634,
	AntiDaoKe = 2635,
	AntiXiaKe = 2636,
	AntiFangShi = 2637,
	AntiYiShi = 2638,
	AntiMeiZhe = 2639,
	AntiYiRen = 2640,
	AntiYanShi = 2641,
	AntiHuaHun = 2642,
	AntiYingLing = 2643,
	AntiDieKe = 2644,
	AntiFlag = 2645,
	EnhanceZhanKuang = 2646,
	EnhanceZhanKuangMul = 2647,
	EnhanceZhanKuangKefu = 2648,
	AntiZhanKuang = 2649,
	JumpSpeed = 2650,
	OriJumpSpeed = 2651,
	AdjJumpSpeed = 2652,
	GravityAcceleration = 2653,
	OriGravityAcceleration = 2654,
	AdjGravityAcceleration = 2655,
	HorizontalAcceleration = 2656,
	OriHorizontalAcceleration = 2657,
	AdjHorizontalAcceleration = 2658,
	SwimSpeed = 2659,
	OriSwimSpeed = 2660,
	AdjSwimSpeed = 2661,
	StrengthPoint = 2662,
	StrengthPointMax = 2663,
	OriStrengthPointMax = 2664,
	AdjStrengthPointMax = 2665,
	AntiMulEnhanceIllusion = 2666,
	GlideHorizontalSpeedWithUmbrella = 2667,
	GlideVerticalSpeedWithUmbrella = 2668,
	AdjpSpeed = 3001,
	AdjmSpeed = 3002,
	
}


CLuaPlayerPropertyFight = {}

CLuaPlayerPropertyFight.Param = nil
CLuaPlayerPropertyFight.TmpParam = nil
local function SetParam_At(id, v)
	CLuaPlayerPropertyFight.Param[id] = v
end
local function GetParam_At(id)
	return CLuaPlayerPropertyFight.Param[id] or 0
end
local function SetTmpParam_At(id, v)
	CLuaPlayerPropertyFight.TmpParam[id] = v
end
local function GetTmpParam_At(id)
	return CLuaPlayerPropertyFight.TmpParam[id] or 0
end

local function LookUpGrow1(clazz, target, keys, defaultValue)
	local key = 0
	for i,v in ipairs(keys) do
		if i==1 then
			key = v*1000
		else
			key = key + v
		end
	end
	local data = Grow_Grow1.GetData(key)
	if data then
		return CommonDefs.DictGetValue_LuaCall(data.PropValues,target)
	end
	return defaultValue
end

local function LookUpGrow2(clazz, target, keys, defaultValue)
	local key = ""
	for i,v in ipairs(keys) do
		if i==1 then
			key = tostring(v)
		else
			key = key.."_"..v
		end
	end
	local data = Grow_Grow2.GetData(key)
	if data then
		return CommonDefs.DictGetValue_LuaCall(data.PropValues,target)
	end
	return defaultValue
end

local CPlayerPropertyFight = import "L10.Game.CPlayerPropertyFight"
local SetParamFunctions = {}
CPlayerPropertyFight.m_hookSetParam = function(this,id,newv)
	CLuaPlayerPropertyFight.Param = this.Param
	CLuaPlayerPropertyFight.TmpParam = this.TmpParam
	if SetParamFunctions[id] then
		SetParamFunctions[id](newv)
	end
	CLuaPlayerPropertyFight.Param = nil
	CLuaPlayerPropertyFight.TmpParam = nil
end
CLuaPlayerPropertyFight.SetParamPermanentCor = function(newv)
	SetParam_At(1, newv)

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentCor()

	CLuaPlayerPropertyFight.RefreshParamCharacterCor()

	CLuaPlayerPropertyFight.RefreshParamCor()

	CLuaPlayerPropertyFight.RefreshParamHpFull()
end
CLuaPlayerPropertyFight.SetParamPermanentSta = function(newv)
	SetParam_At(2, newv)

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentSta()

	CLuaPlayerPropertyFight.RefreshParamSta()

	CLuaPlayerPropertyFight.RefreshParamCharacterSta()

	CLuaPlayerPropertyFight.RefreshParamMpFull()
end
CLuaPlayerPropertyFight.SetParamPermanentStr = function(newv)
	SetParam_At(3, newv)

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentStr()

	CLuaPlayerPropertyFight.RefreshParamStr()

	CLuaPlayerPropertyFight.RefreshParamCharacterStr()

	CLuaPlayerPropertyFight.RefreshParampAttMax()

	CLuaPlayerPropertyFight.RefreshParampDef()

	CLuaPlayerPropertyFight.RefreshParampAttMin()
end
CLuaPlayerPropertyFight.SetParamPermanentInt = function(newv)
	SetParam_At(4, newv)

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentInt()

	CLuaPlayerPropertyFight.RefreshParamInt()

	CLuaPlayerPropertyFight.RefreshParamCharacterInt()

	CLuaPlayerPropertyFight.RefreshParammAttMin()

	CLuaPlayerPropertyFight.RefreshParammAttMax()

	CLuaPlayerPropertyFight.RefreshParammDef()
end
CLuaPlayerPropertyFight.SetParamPermanentAgi = function(newv)
	SetParam_At(5, newv)

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentAgi()

	CLuaPlayerPropertyFight.RefreshParamAgi()

	CLuaPlayerPropertyFight.RefreshParamCharacterAgi()

	CLuaPlayerPropertyFight.RefreshParammMiss()

	CLuaPlayerPropertyFight.RefreshParammHit()

	CLuaPlayerPropertyFight.RefreshParampMiss()

	CLuaPlayerPropertyFight.RefreshParampHit()
end
CLuaPlayerPropertyFight.SetParamPermanentHpFull = function(newv)
	SetParam_At(6, newv)
end
CLuaPlayerPropertyFight.SetParamHp = function(newv)
	SetParam_At(7, newv)
end
CLuaPlayerPropertyFight.SetParamMp = function(newv)
	SetParam_At(8, newv)
end
CLuaPlayerPropertyFight.SetParamCurrentHpFull = function(newv)
	SetParam_At(9, newv)
end
CLuaPlayerPropertyFight.SetParamCurrentMpFull = function(newv)
	SetParam_At(10, newv)
end
CLuaPlayerPropertyFight.SetParamEquipExchangeMF = function(newv)
	SetParam_At(11, newv)

	CLuaPlayerPropertyFight.RefreshParamMF()
end
CLuaPlayerPropertyFight.SetParamRevisePermanentCor = function(newv)
	SetParam_At(12, newv)

	CLuaPlayerPropertyFight.RefreshParamSumPermanent()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidSta()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidInt()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidCor()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidAgi()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidStr()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentCor()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentInt()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentSta()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentStr()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentAgi()

	CLuaPlayerPropertyFight.RefreshParamCharacterCor()

	CLuaPlayerPropertyFight.RefreshParamCor()

	CLuaPlayerPropertyFight.RefreshParamInt()

	CLuaPlayerPropertyFight.RefreshParamCharacterInt()

	CLuaPlayerPropertyFight.RefreshParamSta()

	CLuaPlayerPropertyFight.RefreshParamCharacterSta()

	CLuaPlayerPropertyFight.RefreshParamStr()

	CLuaPlayerPropertyFight.RefreshParamCharacterStr()

	CLuaPlayerPropertyFight.RefreshParamAgi()

	CLuaPlayerPropertyFight.RefreshParamCharacterAgi()

	CLuaPlayerPropertyFight.RefreshParamHpFull()

	CLuaPlayerPropertyFight.RefreshParammAttMin()

	CLuaPlayerPropertyFight.RefreshParammAttMax()

	CLuaPlayerPropertyFight.RefreshParammDef()

	CLuaPlayerPropertyFight.RefreshParamMpFull()

	CLuaPlayerPropertyFight.RefreshParampAttMax()

	CLuaPlayerPropertyFight.RefreshParampDef()

	CLuaPlayerPropertyFight.RefreshParampAttMin()

	CLuaPlayerPropertyFight.RefreshParammMiss()

	CLuaPlayerPropertyFight.RefreshParammHit()

	CLuaPlayerPropertyFight.RefreshParampMiss()

	CLuaPlayerPropertyFight.RefreshParampHit()
end
CLuaPlayerPropertyFight.SetParamRevisePermanentSta = function(newv)
	SetParam_At(13, newv)

	CLuaPlayerPropertyFight.RefreshParamSumPermanent()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidSta()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidInt()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidCor()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidAgi()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidStr()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentCor()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentInt()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentSta()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentStr()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentAgi()

	CLuaPlayerPropertyFight.RefreshParamCharacterCor()

	CLuaPlayerPropertyFight.RefreshParamCor()

	CLuaPlayerPropertyFight.RefreshParamInt()

	CLuaPlayerPropertyFight.RefreshParamCharacterInt()

	CLuaPlayerPropertyFight.RefreshParamSta()

	CLuaPlayerPropertyFight.RefreshParamCharacterSta()

	CLuaPlayerPropertyFight.RefreshParamStr()

	CLuaPlayerPropertyFight.RefreshParamCharacterStr()

	CLuaPlayerPropertyFight.RefreshParamAgi()

	CLuaPlayerPropertyFight.RefreshParamCharacterAgi()

	CLuaPlayerPropertyFight.RefreshParamHpFull()

	CLuaPlayerPropertyFight.RefreshParammAttMin()

	CLuaPlayerPropertyFight.RefreshParammAttMax()

	CLuaPlayerPropertyFight.RefreshParammDef()

	CLuaPlayerPropertyFight.RefreshParamMpFull()

	CLuaPlayerPropertyFight.RefreshParampAttMax()

	CLuaPlayerPropertyFight.RefreshParampDef()

	CLuaPlayerPropertyFight.RefreshParampAttMin()

	CLuaPlayerPropertyFight.RefreshParammMiss()

	CLuaPlayerPropertyFight.RefreshParammHit()

	CLuaPlayerPropertyFight.RefreshParampMiss()

	CLuaPlayerPropertyFight.RefreshParampHit()
end
CLuaPlayerPropertyFight.SetParamRevisePermanentStr = function(newv)
	SetParam_At(14, newv)

	CLuaPlayerPropertyFight.RefreshParamSumPermanent()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidSta()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidInt()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidCor()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidAgi()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidStr()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentCor()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentInt()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentSta()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentStr()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentAgi()

	CLuaPlayerPropertyFight.RefreshParamCharacterCor()

	CLuaPlayerPropertyFight.RefreshParamCor()

	CLuaPlayerPropertyFight.RefreshParamInt()

	CLuaPlayerPropertyFight.RefreshParamCharacterInt()

	CLuaPlayerPropertyFight.RefreshParamSta()

	CLuaPlayerPropertyFight.RefreshParamCharacterSta()

	CLuaPlayerPropertyFight.RefreshParamStr()

	CLuaPlayerPropertyFight.RefreshParamCharacterStr()

	CLuaPlayerPropertyFight.RefreshParamAgi()

	CLuaPlayerPropertyFight.RefreshParamCharacterAgi()

	CLuaPlayerPropertyFight.RefreshParamHpFull()

	CLuaPlayerPropertyFight.RefreshParammAttMin()

	CLuaPlayerPropertyFight.RefreshParammAttMax()

	CLuaPlayerPropertyFight.RefreshParammDef()

	CLuaPlayerPropertyFight.RefreshParamMpFull()

	CLuaPlayerPropertyFight.RefreshParampAttMax()

	CLuaPlayerPropertyFight.RefreshParampDef()

	CLuaPlayerPropertyFight.RefreshParampAttMin()

	CLuaPlayerPropertyFight.RefreshParammMiss()

	CLuaPlayerPropertyFight.RefreshParammHit()

	CLuaPlayerPropertyFight.RefreshParampMiss()

	CLuaPlayerPropertyFight.RefreshParampHit()
end
CLuaPlayerPropertyFight.SetParamRevisePermanentInt = function(newv)
	SetParam_At(15, newv)

	CLuaPlayerPropertyFight.RefreshParamSumPermanent()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidSta()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidInt()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidCor()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidAgi()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidStr()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentCor()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentInt()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentSta()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentStr()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentAgi()

	CLuaPlayerPropertyFight.RefreshParamCharacterCor()

	CLuaPlayerPropertyFight.RefreshParamCor()

	CLuaPlayerPropertyFight.RefreshParamInt()

	CLuaPlayerPropertyFight.RefreshParamCharacterInt()

	CLuaPlayerPropertyFight.RefreshParamSta()

	CLuaPlayerPropertyFight.RefreshParamCharacterSta()

	CLuaPlayerPropertyFight.RefreshParamStr()

	CLuaPlayerPropertyFight.RefreshParamCharacterStr()

	CLuaPlayerPropertyFight.RefreshParamAgi()

	CLuaPlayerPropertyFight.RefreshParamCharacterAgi()

	CLuaPlayerPropertyFight.RefreshParamHpFull()

	CLuaPlayerPropertyFight.RefreshParammAttMin()

	CLuaPlayerPropertyFight.RefreshParammAttMax()

	CLuaPlayerPropertyFight.RefreshParammDef()

	CLuaPlayerPropertyFight.RefreshParamMpFull()

	CLuaPlayerPropertyFight.RefreshParampAttMax()

	CLuaPlayerPropertyFight.RefreshParampDef()

	CLuaPlayerPropertyFight.RefreshParampAttMin()

	CLuaPlayerPropertyFight.RefreshParammMiss()

	CLuaPlayerPropertyFight.RefreshParammHit()

	CLuaPlayerPropertyFight.RefreshParampMiss()

	CLuaPlayerPropertyFight.RefreshParampHit()
end
CLuaPlayerPropertyFight.SetParamRevisePermanentAgi = function(newv)
	SetParam_At(16, newv)

	CLuaPlayerPropertyFight.RefreshParamSumPermanent()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidSta()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidInt()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidCor()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidAgi()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidStr()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentCor()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentInt()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentSta()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentStr()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentAgi()

	CLuaPlayerPropertyFight.RefreshParamCharacterCor()

	CLuaPlayerPropertyFight.RefreshParamCor()

	CLuaPlayerPropertyFight.RefreshParamInt()

	CLuaPlayerPropertyFight.RefreshParamCharacterInt()

	CLuaPlayerPropertyFight.RefreshParamSta()

	CLuaPlayerPropertyFight.RefreshParamCharacterSta()

	CLuaPlayerPropertyFight.RefreshParamStr()

	CLuaPlayerPropertyFight.RefreshParamCharacterStr()

	CLuaPlayerPropertyFight.RefreshParamAgi()

	CLuaPlayerPropertyFight.RefreshParamCharacterAgi()

	CLuaPlayerPropertyFight.RefreshParamHpFull()

	CLuaPlayerPropertyFight.RefreshParammAttMin()

	CLuaPlayerPropertyFight.RefreshParammAttMax()

	CLuaPlayerPropertyFight.RefreshParammDef()

	CLuaPlayerPropertyFight.RefreshParamMpFull()

	CLuaPlayerPropertyFight.RefreshParampAttMax()

	CLuaPlayerPropertyFight.RefreshParampDef()

	CLuaPlayerPropertyFight.RefreshParampAttMin()

	CLuaPlayerPropertyFight.RefreshParammMiss()

	CLuaPlayerPropertyFight.RefreshParammHit()

	CLuaPlayerPropertyFight.RefreshParampMiss()

	CLuaPlayerPropertyFight.RefreshParampHit()
end
CLuaPlayerPropertyFight.SetParamClass = function(newv)
	SetTmpParam_At(1, newv)

	CLuaPlayerPropertyFight.RefreshParamLookUppAttMax()

	CLuaPlayerPropertyFight.RefreshParamLookUpHpFull()

	CLuaPlayerPropertyFight.RefreshParamLookUppAttMin()

	CLuaPlayerPropertyFight.RefreshParamLookUpmAttMax()

	CLuaPlayerPropertyFight.RefreshParamLookUpInt()

	CLuaPlayerPropertyFight.RefreshParamLookUppHit()

	CLuaPlayerPropertyFight.RefreshParamLookUpAgi()

	CLuaPlayerPropertyFight.RefreshParamLookUppMiss()

	CLuaPlayerPropertyFight.RefreshParamLookUpMpFull()

	CLuaPlayerPropertyFight.RefreshParamLookUpmMiss()

	CLuaPlayerPropertyFight.RefreshParamLookUpSta()

	CLuaPlayerPropertyFight.RefreshParamLookUpmAttMin()

	CLuaPlayerPropertyFight.RefreshParamLookUpStr()

	CLuaPlayerPropertyFight.RefreshParamLookUpCor()

	CLuaPlayerPropertyFight.RefreshParamLookUpmHit()

	CLuaPlayerPropertyFight.RefreshParamLookUppDef()

	CLuaPlayerPropertyFight.RefreshParamLookUpmDef()

	CLuaPlayerPropertyFight.RefreshParamCharacterCor()

	CLuaPlayerPropertyFight.RefreshParamCor()

	CLuaPlayerPropertyFight.RefreshParamInt()

	CLuaPlayerPropertyFight.RefreshParamCharacterInt()

	CLuaPlayerPropertyFight.RefreshParamSta()

	CLuaPlayerPropertyFight.RefreshParamCharacterSta()

	CLuaPlayerPropertyFight.RefreshParamStr()

	CLuaPlayerPropertyFight.RefreshParamCharacterStr()

	CLuaPlayerPropertyFight.RefreshParamAgi()

	CLuaPlayerPropertyFight.RefreshParamCharacterAgi()

	CLuaPlayerPropertyFight.RefreshParamHpFull()

	CLuaPlayerPropertyFight.RefreshParammAttMin()

	CLuaPlayerPropertyFight.RefreshParammAttMax()

	CLuaPlayerPropertyFight.RefreshParammDef()

	CLuaPlayerPropertyFight.RefreshParamMpFull()

	CLuaPlayerPropertyFight.RefreshParampAttMax()

	CLuaPlayerPropertyFight.RefreshParampDef()

	CLuaPlayerPropertyFight.RefreshParampAttMin()

	CLuaPlayerPropertyFight.RefreshParammMiss()

	CLuaPlayerPropertyFight.RefreshParammHit()

	CLuaPlayerPropertyFight.RefreshParampMiss()

	CLuaPlayerPropertyFight.RefreshParampHit()
end
CLuaPlayerPropertyFight.SetParamGrade = function(newv)
	SetTmpParam_At(2, newv)

	CLuaPlayerPropertyFight.RefreshParamLookUpInt()

	CLuaPlayerPropertyFight.RefreshParamLookUpAgi()

	CLuaPlayerPropertyFight.RefreshParamLookUpSta()

	CLuaPlayerPropertyFight.RefreshParamLookUpStr()

	CLuaPlayerPropertyFight.RefreshParamLookUpCor()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidSta()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidInt()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidCor()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidAgi()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidStr()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentCor()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentInt()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentSta()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentStr()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentAgi()

	CLuaPlayerPropertyFight.RefreshParamCharacterCor()

	CLuaPlayerPropertyFight.RefreshParamCor()

	CLuaPlayerPropertyFight.RefreshParamInt()

	CLuaPlayerPropertyFight.RefreshParamCharacterInt()

	CLuaPlayerPropertyFight.RefreshParamSta()

	CLuaPlayerPropertyFight.RefreshParamCharacterSta()

	CLuaPlayerPropertyFight.RefreshParamStr()

	CLuaPlayerPropertyFight.RefreshParamCharacterStr()

	CLuaPlayerPropertyFight.RefreshParamAgi()

	CLuaPlayerPropertyFight.RefreshParamCharacterAgi()

	CLuaPlayerPropertyFight.RefreshParamHpFull()

	CLuaPlayerPropertyFight.RefreshParammAttMin()

	CLuaPlayerPropertyFight.RefreshParammAttMax()

	CLuaPlayerPropertyFight.RefreshParammDef()

	CLuaPlayerPropertyFight.RefreshParamMpFull()

	CLuaPlayerPropertyFight.RefreshParampAttMax()

	CLuaPlayerPropertyFight.RefreshParampDef()

	CLuaPlayerPropertyFight.RefreshParampAttMin()

	CLuaPlayerPropertyFight.RefreshParammMiss()

	CLuaPlayerPropertyFight.RefreshParammHit()

	CLuaPlayerPropertyFight.RefreshParampMiss()

	CLuaPlayerPropertyFight.RefreshParampHit()
end
CLuaPlayerPropertyFight.RefreshParamCor = function()
	local AdjCor2 = GetTmpParam_At(463)
	local AdjCor = GetTmpParam_At(4)
	local MulCor = GetTmpParam_At(5)
	local LookUpCor = GetTmpParam_At(328)
	local AdjustPermanentCor = GetTmpParam_At(611)

	local newv = (LookUpCor+AdjustPermanentCor+AdjCor)*(1+ MulCor)+AdjCor2
	SetTmpParam_At(3, newv)
end
CLuaPlayerPropertyFight.SetParamAdjCor = function(newv)
	SetTmpParam_At(4, newv)

	CLuaPlayerPropertyFight.RefreshParamCor()

	CLuaPlayerPropertyFight.RefreshParamHpFull()
end
CLuaPlayerPropertyFight.SetParamMulCor = function(newv)
	SetTmpParam_At(5, newv)

	CLuaPlayerPropertyFight.RefreshParamCor()

	CLuaPlayerPropertyFight.RefreshParamHpFull()
end
CLuaPlayerPropertyFight.RefreshParamSta = function()
	local AdjSta = GetTmpParam_At(7)
	local MulSta = GetTmpParam_At(8)
	local LookUpSta = GetTmpParam_At(329)
	local AdjustPermanentSta = GetTmpParam_At(612)

	local newv = (LookUpSta+AdjustPermanentSta+AdjSta)*(1+ MulSta)
	SetTmpParam_At(6, newv)
end
CLuaPlayerPropertyFight.SetParamAdjSta = function(newv)
	SetTmpParam_At(7, newv)

	CLuaPlayerPropertyFight.RefreshParamSta()

	CLuaPlayerPropertyFight.RefreshParamMpFull()
end
CLuaPlayerPropertyFight.SetParamMulSta = function(newv)
	SetTmpParam_At(8, newv)

	CLuaPlayerPropertyFight.RefreshParamSta()

	CLuaPlayerPropertyFight.RefreshParamMpFull()
end
CLuaPlayerPropertyFight.RefreshParamStr = function()
	local AdjStr2 = GetTmpParam_At(504)
	local AdjStr = GetTmpParam_At(10)
	local MulStr = GetTmpParam_At(11)
	local LookUpStr = GetTmpParam_At(330)
	local AdjustPermanentStr = GetTmpParam_At(613)

	local newv = (LookUpStr+AdjustPermanentStr+AdjStr)*(1+ MulStr)+AdjStr2
	SetTmpParam_At(9, newv)
end
CLuaPlayerPropertyFight.SetParamAdjStr = function(newv)
	SetTmpParam_At(10, newv)

	CLuaPlayerPropertyFight.RefreshParamStr()

	CLuaPlayerPropertyFight.RefreshParampAttMax()

	CLuaPlayerPropertyFight.RefreshParampDef()

	CLuaPlayerPropertyFight.RefreshParampAttMin()
end
CLuaPlayerPropertyFight.SetParamMulStr = function(newv)
	SetTmpParam_At(11, newv)

	CLuaPlayerPropertyFight.RefreshParamStr()

	CLuaPlayerPropertyFight.RefreshParampAttMax()

	CLuaPlayerPropertyFight.RefreshParampDef()

	CLuaPlayerPropertyFight.RefreshParampAttMin()
end
CLuaPlayerPropertyFight.RefreshParamInt = function()
	local AdjInt2 = GetTmpParam_At(489)
	local AdjInt = GetTmpParam_At(13)
	local MulInt = GetTmpParam_At(14)
	local LookUpInt = GetTmpParam_At(331)
	local AdjustPermanentInt = GetTmpParam_At(614)

	local newv = (LookUpInt+AdjustPermanentInt+AdjInt)*(1+ MulInt)+AdjInt2
	SetTmpParam_At(12, newv)
end
CLuaPlayerPropertyFight.SetParamAdjInt = function(newv)
	SetTmpParam_At(13, newv)

	CLuaPlayerPropertyFight.RefreshParamInt()

	CLuaPlayerPropertyFight.RefreshParammAttMin()

	CLuaPlayerPropertyFight.RefreshParammAttMax()

	CLuaPlayerPropertyFight.RefreshParammDef()
end
CLuaPlayerPropertyFight.SetParamMulInt = function(newv)
	SetTmpParam_At(14, newv)

	CLuaPlayerPropertyFight.RefreshParamInt()

	CLuaPlayerPropertyFight.RefreshParammAttMin()

	CLuaPlayerPropertyFight.RefreshParammAttMax()

	CLuaPlayerPropertyFight.RefreshParammDef()
end
CLuaPlayerPropertyFight.RefreshParamAgi = function()
	local AdjAgi2 = GetTmpParam_At(435)
	local AdjAgi = GetTmpParam_At(16)
	local MulAgi = GetTmpParam_At(17)
	local LookUpAgi = GetTmpParam_At(332)
	local AdjustPermanentAgi = GetTmpParam_At(615)

	local newv = (LookUpAgi+AdjustPermanentAgi+AdjAgi)*(1+ MulAgi)+AdjAgi2
	SetTmpParam_At(15, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAgi = function(newv)
	SetTmpParam_At(16, newv)

	CLuaPlayerPropertyFight.RefreshParamAgi()

	CLuaPlayerPropertyFight.RefreshParammMiss()

	CLuaPlayerPropertyFight.RefreshParammHit()

	CLuaPlayerPropertyFight.RefreshParampMiss()

	CLuaPlayerPropertyFight.RefreshParampHit()
end
CLuaPlayerPropertyFight.SetParamMulAgi = function(newv)
	SetTmpParam_At(17, newv)

	CLuaPlayerPropertyFight.RefreshParamAgi()

	CLuaPlayerPropertyFight.RefreshParammMiss()

	CLuaPlayerPropertyFight.RefreshParammHit()

	CLuaPlayerPropertyFight.RefreshParampMiss()

	CLuaPlayerPropertyFight.RefreshParampHit()
end
CLuaPlayerPropertyFight.RefreshParamHpFull = function()
	local AdjHpFull2 = GetTmpParam_At(345)
	local AdjHpFull = GetTmpParam_At(19)
	local MulHpFull = GetTmpParam_At(20)
	local LookUpHpFull = GetTmpParam_At(333)
	local Cor = GetTmpParam_At(3)

	local newv = max(1,(LookUpHpFull*Cor + AdjHpFull)*(1+ MulHpFull)+AdjHpFull2)
	SetTmpParam_At(18, newv)
end
CLuaPlayerPropertyFight.SetParamAdjHpFull = function(newv)
	SetTmpParam_At(19, newv)

	CLuaPlayerPropertyFight.RefreshParamHpFull()
end
CLuaPlayerPropertyFight.SetParamMulHpFull = function(newv)
	SetTmpParam_At(20, newv)

	CLuaPlayerPropertyFight.RefreshParamHpFull()
end
CLuaPlayerPropertyFight.RefreshParamHpRecover = function()
	local AdjHpRecover = GetTmpParam_At(22)
	local MulHpRecover = GetTmpParam_At(23)

	local newv = AdjHpRecover*(1+ MulHpRecover)
	SetTmpParam_At(21, newv)
end
CLuaPlayerPropertyFight.SetParamAdjHpRecover = function(newv)
	SetTmpParam_At(22, newv)

	CLuaPlayerPropertyFight.RefreshParamHpRecover()
end
CLuaPlayerPropertyFight.SetParamMulHpRecover = function(newv)
	SetTmpParam_At(23, newv)

	CLuaPlayerPropertyFight.RefreshParamHpRecover()
end
CLuaPlayerPropertyFight.RefreshParamMpFull = function()
	local AdjMpFull = GetTmpParam_At(25)
	local MulMpFull = GetTmpParam_At(26)
	local LookUpMpFull = GetTmpParam_At(334)
	local Sta = GetTmpParam_At(6)

	local newv = max(1,(LookUpMpFull*Sta + AdjMpFull)*(1+ MulMpFull))
	SetTmpParam_At(24, newv)
end
CLuaPlayerPropertyFight.SetParamAdjMpFull = function(newv)
	SetTmpParam_At(25, newv)

	CLuaPlayerPropertyFight.RefreshParamMpFull()
end
CLuaPlayerPropertyFight.SetParamMulMpFull = function(newv)
	SetTmpParam_At(26, newv)

	CLuaPlayerPropertyFight.RefreshParamMpFull()
end
CLuaPlayerPropertyFight.RefreshParamMpRecover = function()
	local AdjMpRecover = GetTmpParam_At(28)
	local MulMpRecover = GetTmpParam_At(29)

	local newv = AdjMpRecover*(1+ MulMpRecover)
	SetTmpParam_At(27, newv)
end
CLuaPlayerPropertyFight.SetParamAdjMpRecover = function(newv)
	SetTmpParam_At(28, newv)

	CLuaPlayerPropertyFight.RefreshParamMpRecover()
end
CLuaPlayerPropertyFight.SetParamMulMpRecover = function(newv)
	SetTmpParam_At(29, newv)

	CLuaPlayerPropertyFight.RefreshParamMpRecover()
end
CLuaPlayerPropertyFight.RefreshParampAttMin = function()
	local AdjpAttMax2 = GetTmpParam_At(347)
	local AdjpAttMin2 = GetTmpParam_At(346)
	local AdjpAttMax = GetTmpParam_At(34)
	local MulpAttMax = GetTmpParam_At(35)
	local AdjpAttMin = GetTmpParam_At(31)
	local MulpAttMin = GetTmpParam_At(32)
	local LookUppAttMax = GetTmpParam_At(336)
	local Str = GetTmpParam_At(9)
	local LookUppAttMin = GetTmpParam_At(335)

	local newv = max(1,min((LookUppAttMax*Str + AdjpAttMax)*(1+ MulpAttMax)+AdjpAttMax2,(LookUppAttMin*Str + AdjpAttMin)*(1+ MulpAttMin)+AdjpAttMin2))
	SetTmpParam_At(30, newv)
end
CLuaPlayerPropertyFight.SetParamAdjpAttMin = function(newv)
	SetTmpParam_At(31, newv)

	CLuaPlayerPropertyFight.RefreshParampAttMin()
end
CLuaPlayerPropertyFight.SetParamMulpAttMin = function(newv)
	SetTmpParam_At(32, newv)

	CLuaPlayerPropertyFight.RefreshParampAttMin()
end
CLuaPlayerPropertyFight.RefreshParampAttMax = function()
	local AdjpAttMax2 = GetTmpParam_At(347)
	local AdjpAttMax = GetTmpParam_At(34)
	local MulpAttMax = GetTmpParam_At(35)
	local LookUppAttMax = GetTmpParam_At(336)
	local Str = GetTmpParam_At(9)

	local newv = max(1,(LookUppAttMax*Str + AdjpAttMax)*(1+ MulpAttMax)+AdjpAttMax2)
	SetTmpParam_At(33, newv)
end
CLuaPlayerPropertyFight.SetParamAdjpAttMax = function(newv)
	SetTmpParam_At(34, newv)

	CLuaPlayerPropertyFight.RefreshParampAttMax()

	CLuaPlayerPropertyFight.RefreshParampAttMin()
end
CLuaPlayerPropertyFight.SetParamMulpAttMax = function(newv)
	SetTmpParam_At(35, newv)

	CLuaPlayerPropertyFight.RefreshParampAttMax()

	CLuaPlayerPropertyFight.RefreshParampAttMin()
end
CLuaPlayerPropertyFight.RefreshParampHit = function()
	local AdjpHit2 = GetTmpParam_At(500)
	local AdjpHit = GetTmpParam_At(37)
	local MulpHit = GetTmpParam_At(38)
	local LookUppHit = GetTmpParam_At(337)
	local Agi = GetTmpParam_At(15)

	local newv = (LookUppHit*Agi + AdjpHit)*(1+ MulpHit)+AdjpHit2
	SetTmpParam_At(36, newv)
end
CLuaPlayerPropertyFight.SetParamAdjpHit = function(newv)
	SetTmpParam_At(37, newv)

	CLuaPlayerPropertyFight.RefreshParampHit()
end
CLuaPlayerPropertyFight.SetParamMulpHit = function(newv)
	SetTmpParam_At(38, newv)

	CLuaPlayerPropertyFight.RefreshParampHit()
end
CLuaPlayerPropertyFight.RefreshParampMiss = function()
	local AdjpMiss2 = GetTmpParam_At(502)
	local AdjpMiss = GetTmpParam_At(40)
	local MulpMiss = GetTmpParam_At(41)
	local LookUppMiss = GetTmpParam_At(338)
	local Agi = GetTmpParam_At(15)

	local newv = (LookUppMiss*Agi+ AdjpMiss)*(1+ MulpMiss)+AdjpMiss2
	SetTmpParam_At(39, newv)
end
CLuaPlayerPropertyFight.SetParamAdjpMiss = function(newv)
	SetTmpParam_At(40, newv)

	CLuaPlayerPropertyFight.RefreshParampMiss()
end
CLuaPlayerPropertyFight.SetParamMulpMiss = function(newv)
	SetTmpParam_At(41, newv)

	CLuaPlayerPropertyFight.RefreshParampMiss()
end
CLuaPlayerPropertyFight.RefreshParampDef = function()
	local AdjpDef2 = GetTmpParam_At(497)
	local AdjpDef = GetTmpParam_At(43)
	local MulpDef = GetTmpParam_At(44)
	local LookUppDef = GetTmpParam_At(339)
	local Str = GetTmpParam_At(9)

	local newv = max(1,(LookUppDef*Str+ AdjpDef)*(1+ MulpDef)+AdjpDef2)
	SetTmpParam_At(42, newv)
end
CLuaPlayerPropertyFight.SetParamAdjpDef = function(newv)
	SetTmpParam_At(43, newv)

	CLuaPlayerPropertyFight.RefreshParampDef()
end
CLuaPlayerPropertyFight.SetParamMulpDef = function(newv)
	SetTmpParam_At(44, newv)

	CLuaPlayerPropertyFight.RefreshParampDef()
end
CLuaPlayerPropertyFight.SetParamAdjpHurt = function(newv)
	SetTmpParam_At(45, newv)
end
CLuaPlayerPropertyFight.SetParamMulpHurt = function(newv)
	SetTmpParam_At(46, newv)
end
CLuaPlayerPropertyFight.RefreshParampSpeed = function()
	local OripSpeed = GetTmpParam_At(48)
	local AdjpSpeed = GetTmpParam_At(1001)
	local MulpSpeed = GetTmpParam_At(49)

	local newv = max(0.2,min(2.5,(OripSpeed+AdjpSpeed)*(1+ MulpSpeed)))
	SetTmpParam_At(47, newv)
end
CLuaPlayerPropertyFight.SetParamOripSpeed = function(newv)
	SetTmpParam_At(48, newv)

	CLuaPlayerPropertyFight.RefreshParampSpeed()
end
CLuaPlayerPropertyFight.SetParamMulpSpeed = function(newv)
	SetTmpParam_At(49, newv)

	CLuaPlayerPropertyFight.RefreshParampSpeed()
end
CLuaPlayerPropertyFight.RefreshParampFatal = function()
	local AdjpFatal = GetTmpParam_At(51)
	local AdjpFatal2 = GetTmpParam_At(498)

	local newv = AdjpFatal+AdjpFatal2
	SetTmpParam_At(50, newv)
end
CLuaPlayerPropertyFight.SetParamAdjpFatal = function(newv)
	SetTmpParam_At(51, newv)

	CLuaPlayerPropertyFight.RefreshParampFatal()
end
CLuaPlayerPropertyFight.RefreshParamAntipFatal = function()
	local AdjAntipFatal = GetTmpParam_At(53)
	local AdjAntipFatal2 = GetTmpParam_At(452)

	local newv = AdjAntipFatal+AdjAntipFatal2
	SetTmpParam_At(52, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntipFatal = function(newv)
	SetTmpParam_At(53, newv)

	CLuaPlayerPropertyFight.RefreshParamAntipFatal()
end
CLuaPlayerPropertyFight.RefreshParampFatalDamage = function()
	local AdjpFatalDamage2 = GetTmpParam_At(499)
	local AdjpFatalDamage = GetTmpParam_At(55)

	local newv = (AdjpFatalDamage + 0.5+AdjpFatalDamage2)
	SetTmpParam_At(54, newv)
end
CLuaPlayerPropertyFight.SetParamAdjpFatalDamage = function(newv)
	SetTmpParam_At(55, newv)

	CLuaPlayerPropertyFight.RefreshParampFatalDamage()
end
CLuaPlayerPropertyFight.RefreshParamAntipFatalDamage = function()
	local AdjAntipFatalDamage = GetTmpParam_At(57)
	local AdjAntipFatalDamage2 = GetTmpParam_At(453)

	local newv = AdjAntipFatalDamage+AdjAntipFatalDamage2
	SetTmpParam_At(56, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntipFatalDamage = function(newv)
	SetTmpParam_At(57, newv)

	CLuaPlayerPropertyFight.RefreshParamAntipFatalDamage()
end
CLuaPlayerPropertyFight.RefreshParamBlock = function()
	local AdjBlock2 = GetTmpParam_At(461)
	local AdjBlock = GetTmpParam_At(59)
	local MulBlock = GetTmpParam_At(60)

	local newv = max(0,AdjBlock * (1+MulBlock)+AdjBlock2)
	SetTmpParam_At(58, newv)
end
CLuaPlayerPropertyFight.SetParamAdjBlock = function(newv)
	SetTmpParam_At(59, newv)

	CLuaPlayerPropertyFight.RefreshParamBlock()
end
CLuaPlayerPropertyFight.SetParamMulBlock = function(newv)
	SetTmpParam_At(60, newv)

	CLuaPlayerPropertyFight.RefreshParamBlock()
end
CLuaPlayerPropertyFight.RefreshParamBlockDamage = function()
	local AdjBlockDamage2 = GetTmpParam_At(462)
	local AdjBlockDamage = GetTmpParam_At(62)

	local newv = max(0,200+AdjBlockDamage+AdjBlockDamage2)
	SetTmpParam_At(61, newv)
end
CLuaPlayerPropertyFight.SetParamAdjBlockDamage = function(newv)
	SetTmpParam_At(62, newv)

	CLuaPlayerPropertyFight.RefreshParamBlockDamage()
end
CLuaPlayerPropertyFight.RefreshParamAntiBlock = function()
	local AdjAntiBlock = GetTmpParam_At(64)
	local AdjAntiBlock2 = GetTmpParam_At(439)

	local newv = AdjAntiBlock+AdjAntiBlock2
	SetTmpParam_At(63, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntiBlock = function(newv)
	SetTmpParam_At(64, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiBlock()
end
CLuaPlayerPropertyFight.RefreshParamAntiBlockDamage = function()
	local AdjAntiBlockDamage = GetTmpParam_At(66)
	local AdjAntiBlockDamage2 = GetTmpParam_At(440)

	local newv = AdjAntiBlockDamage+AdjAntiBlockDamage2
	SetTmpParam_At(65, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntiBlockDamage = function(newv)
	SetTmpParam_At(66, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiBlockDamage()
end
CLuaPlayerPropertyFight.RefreshParammAttMin = function()
	local AdjmAttMax2 = GetTmpParam_At(349)
	local AdjmAttMin2 = GetTmpParam_At(348)
	local AdjmAttMax = GetTmpParam_At(71)
	local MulmAttMax = GetTmpParam_At(72)
	local AdjmAttMin = GetTmpParam_At(68)
	local MulmAttMin = GetTmpParam_At(69)
	local LookUpmAttMax = GetTmpParam_At(341)
	local Int = GetTmpParam_At(12)
	local LookUpmAttMin = GetTmpParam_At(340)

	local newv = max(1,min((LookUpmAttMax*Int + AdjmAttMax)*(1+ MulmAttMax)+AdjmAttMax2,(LookUpmAttMin*Int + AdjmAttMin)*(1+ MulmAttMin)+AdjmAttMin2))
	SetTmpParam_At(67, newv)
end
CLuaPlayerPropertyFight.SetParamAdjmAttMin = function(newv)
	SetTmpParam_At(68, newv)

	CLuaPlayerPropertyFight.RefreshParammAttMin()
end
CLuaPlayerPropertyFight.SetParamMulmAttMin = function(newv)
	SetTmpParam_At(69, newv)

	CLuaPlayerPropertyFight.RefreshParammAttMin()
end
CLuaPlayerPropertyFight.RefreshParammAttMax = function()
	local AdjmAttMax2 = GetTmpParam_At(349)
	local AdjmAttMax = GetTmpParam_At(71)
	local MulmAttMax = GetTmpParam_At(72)
	local LookUpmAttMax = GetTmpParam_At(341)
	local Int = GetTmpParam_At(12)

	local newv = max(1,(LookUpmAttMax*Int + AdjmAttMax)*(1+ MulmAttMax)+AdjmAttMax2)
	SetTmpParam_At(70, newv)
end
CLuaPlayerPropertyFight.SetParamAdjmAttMax = function(newv)
	SetTmpParam_At(71, newv)

	CLuaPlayerPropertyFight.RefreshParammAttMin()

	CLuaPlayerPropertyFight.RefreshParammAttMax()
end
CLuaPlayerPropertyFight.SetParamMulmAttMax = function(newv)
	SetTmpParam_At(72, newv)

	CLuaPlayerPropertyFight.RefreshParammAttMin()

	CLuaPlayerPropertyFight.RefreshParammAttMax()
end
CLuaPlayerPropertyFight.RefreshParammHit = function()
	local AdjmHit2 = GetTmpParam_At(493)
	local AdjmHit = GetTmpParam_At(74)
	local MulmHit = GetTmpParam_At(75)
	local LookUpmHit = GetTmpParam_At(342)
	local Agi = GetTmpParam_At(15)

	local newv = (LookUpmHit*Agi + AdjmHit)*(1+ MulmHit)+AdjmHit2
	SetTmpParam_At(73, newv)
end
CLuaPlayerPropertyFight.SetParamAdjmHit = function(newv)
	SetTmpParam_At(74, newv)

	CLuaPlayerPropertyFight.RefreshParammHit()
end
CLuaPlayerPropertyFight.SetParamMulmHit = function(newv)
	SetTmpParam_At(75, newv)

	CLuaPlayerPropertyFight.RefreshParammHit()
end
CLuaPlayerPropertyFight.RefreshParammMiss = function()
	local AdjmMiss2 = GetTmpParam_At(495)
	local AdjmMiss = GetTmpParam_At(77)
	local MulmMiss = GetTmpParam_At(78)
	local LookUpmMiss = GetTmpParam_At(343)
	local Agi = GetTmpParam_At(15)

	local newv = (LookUpmMiss*Agi + AdjmMiss)*(1+ MulmMiss)+AdjmMiss2
	SetTmpParam_At(76, newv)
end
CLuaPlayerPropertyFight.SetParamAdjmMiss = function(newv)
	SetTmpParam_At(77, newv)

	CLuaPlayerPropertyFight.RefreshParammMiss()
end
CLuaPlayerPropertyFight.SetParamMulmMiss = function(newv)
	SetTmpParam_At(78, newv)

	CLuaPlayerPropertyFight.RefreshParammMiss()
end
CLuaPlayerPropertyFight.RefreshParammDef = function()
	local AdjmDef2 = GetTmpParam_At(490)
	local AdjmDef = GetTmpParam_At(80)
	local MulmDef = GetTmpParam_At(81)
	local LookUpmDef = GetTmpParam_At(344)
	local Int = GetTmpParam_At(12)

	local newv = max(1,(LookUpmDef*Int + AdjmDef)*(1+ MulmDef)+AdjmDef2)
	SetTmpParam_At(79, newv)
end
CLuaPlayerPropertyFight.SetParamAdjmDef = function(newv)
	SetTmpParam_At(80, newv)

	CLuaPlayerPropertyFight.RefreshParammDef()
end
CLuaPlayerPropertyFight.SetParamMulmDef = function(newv)
	SetTmpParam_At(81, newv)

	CLuaPlayerPropertyFight.RefreshParammDef()
end
CLuaPlayerPropertyFight.SetParamAdjmHurt = function(newv)
	SetTmpParam_At(82, newv)
end
CLuaPlayerPropertyFight.SetParamMulmHurt = function(newv)
	SetTmpParam_At(83, newv)
end
CLuaPlayerPropertyFight.RefreshParammSpeed = function()
	local OrimSpeed = GetTmpParam_At(85)
	local AdjmSpeed = GetTmpParam_At(1002)
	local MulmSpeed = GetTmpParam_At(86)

	local newv = max(0.2,min(2.5,(OrimSpeed+AdjmSpeed)*(1+ MulmSpeed)))
	SetTmpParam_At(84, newv)
end
CLuaPlayerPropertyFight.SetParamOrimSpeed = function(newv)
	SetTmpParam_At(85, newv)

	CLuaPlayerPropertyFight.RefreshParammSpeed()
end
CLuaPlayerPropertyFight.SetParamMulmSpeed = function(newv)
	SetTmpParam_At(86, newv)

	CLuaPlayerPropertyFight.RefreshParammSpeed()
end
CLuaPlayerPropertyFight.RefreshParammFatal = function()
	local AdjmFatal = GetTmpParam_At(88)
	local AdjmFatal2 = GetTmpParam_At(491)

	local newv = AdjmFatal+AdjmFatal2
	SetTmpParam_At(87, newv)
end
CLuaPlayerPropertyFight.SetParamAdjmFatal = function(newv)
	SetTmpParam_At(88, newv)

	CLuaPlayerPropertyFight.RefreshParammFatal()
end
CLuaPlayerPropertyFight.RefreshParamAntimFatal = function()
	local AdjAntimFatal = GetTmpParam_At(90)
	local AdjAntimFatal2 = GetTmpParam_At(449)

	local newv = AdjAntimFatal+AdjAntimFatal2
	SetTmpParam_At(89, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntimFatal = function(newv)
	SetTmpParam_At(90, newv)

	CLuaPlayerPropertyFight.RefreshParamAntimFatal()
end
CLuaPlayerPropertyFight.RefreshParammFatalDamage = function()
	local AdjmFatalDamage2 = GetTmpParam_At(492)
	local AdjmFatalDamage = GetTmpParam_At(92)

	local newv = (AdjmFatalDamage+0.5+AdjmFatalDamage2)
	SetTmpParam_At(91, newv)
end
CLuaPlayerPropertyFight.SetParamAdjmFatalDamage = function(newv)
	SetTmpParam_At(92, newv)

	CLuaPlayerPropertyFight.RefreshParammFatalDamage()
end
CLuaPlayerPropertyFight.RefreshParamAntimFatalDamage = function()
	local AdjAntimFatalDamage = GetTmpParam_At(94)
	local AdjAntimFatalDamage2 = GetTmpParam_At(450)

	local newv = AdjAntimFatalDamage+AdjAntimFatalDamage2
	SetTmpParam_At(93, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntimFatalDamage = function(newv)
	SetTmpParam_At(94, newv)

	CLuaPlayerPropertyFight.RefreshParamAntimFatalDamage()
end
CLuaPlayerPropertyFight.RefreshParamEnhanceFire = function()
	local AdjEnhanceFire = GetTmpParam_At(96)
	local AdjEnhanceFire2 = GetTmpParam_At(468)

	local newv = AdjEnhanceFire+AdjEnhanceFire2
	SetTmpParam_At(95, newv)
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceFire = function(newv)
	SetTmpParam_At(96, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceFire()
end
CLuaPlayerPropertyFight.SetParamMulEnhanceFire = function(newv)
	SetTmpParam_At(97, newv)
end
CLuaPlayerPropertyFight.RefreshParamEnhanceThunder = function()
	local AdjEnhanceThunder = GetTmpParam_At(99)
	local AdjEnhanceThunder2 = GetTmpParam_At(477)

	local newv = AdjEnhanceThunder+AdjEnhanceThunder2
	SetTmpParam_At(98, newv)
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceThunder = function(newv)
	SetTmpParam_At(99, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceThunder()
end
CLuaPlayerPropertyFight.SetParamMulEnhanceThunder = function(newv)
	SetTmpParam_At(100, newv)
end
CLuaPlayerPropertyFight.RefreshParamEnhanceIce = function()
	local AdjEnhanceIce = GetTmpParam_At(102)
	local AdjEnhanceIce2 = GetTmpParam_At(470)

	local newv = AdjEnhanceIce+AdjEnhanceIce2
	SetTmpParam_At(101, newv)
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceIce = function(newv)
	SetTmpParam_At(102, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceIce()
end
CLuaPlayerPropertyFight.SetParamMulEnhanceIce = function(newv)
	SetTmpParam_At(103, newv)
end
CLuaPlayerPropertyFight.RefreshParamEnhancePoison = function()
	local AdjEnhancePoison = GetTmpParam_At(105)
	local AdjEnhancePoison2 = GetTmpParam_At(474)

	local newv = AdjEnhancePoison+AdjEnhancePoison2
	SetTmpParam_At(104, newv)
end
CLuaPlayerPropertyFight.SetParamAdjEnhancePoison = function(newv)
	SetTmpParam_At(105, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhancePoison()
end
CLuaPlayerPropertyFight.SetParamMulEnhancePoison = function(newv)
	SetTmpParam_At(106, newv)
end
CLuaPlayerPropertyFight.RefreshParamEnhanceWind = function()
	local AdjEnhanceWind = GetTmpParam_At(108)
	local AdjEnhanceWind2 = GetTmpParam_At(480)

	local newv = AdjEnhanceWind+AdjEnhanceWind2
	SetTmpParam_At(107, newv)
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceWind = function(newv)
	SetTmpParam_At(108, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceWind()
end
CLuaPlayerPropertyFight.SetParamMulEnhanceWind = function(newv)
	SetTmpParam_At(109, newv)
end
CLuaPlayerPropertyFight.RefreshParamEnhanceLight = function()
	local AdjEnhanceLight = GetTmpParam_At(111)
	local AdjEnhanceLight2 = GetTmpParam_At(472)

	local newv = AdjEnhanceLight+AdjEnhanceLight2
	SetTmpParam_At(110, newv)
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceLight = function(newv)
	SetTmpParam_At(111, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceLight()
end
CLuaPlayerPropertyFight.SetParamMulEnhanceLight = function(newv)
	SetTmpParam_At(112, newv)
end
CLuaPlayerPropertyFight.RefreshParamEnhanceIllusion = function()
	local AdjEnhanceIllusion = GetTmpParam_At(114)
	local AdjEnhanceIllusion2 = GetTmpParam_At(471)

	local newv = AdjEnhanceIllusion+AdjEnhanceIllusion2
	SetTmpParam_At(113, newv)
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceIllusion = function(newv)
	SetTmpParam_At(114, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceIllusion()
end
CLuaPlayerPropertyFight.SetParamMulEnhanceIllusion = function(newv)
	SetTmpParam_At(115, newv)
end
CLuaPlayerPropertyFight.RefreshParamAntiFire = function()
	local AdjAntiFire2 = GetTmpParam_At(444)
	local AdjAntiFire = GetTmpParam_At(117)
	local MulAntiFire = GetTmpParam_At(118)

	local newv = AdjAntiFire*(1+MulAntiFire)+AdjAntiFire2
	SetTmpParam_At(116, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntiFire = function(newv)
	SetTmpParam_At(117, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiFire()
end
CLuaPlayerPropertyFight.SetParamMulAntiFire = function(newv)
	SetTmpParam_At(118, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiFire()
end
CLuaPlayerPropertyFight.RefreshParamAntiThunder = function()
	local AdjAntiThunder2 = GetTmpParam_At(457)
	local AdjAntiThunder = GetTmpParam_At(120)
	local MulAntiThunder = GetTmpParam_At(121)

	local newv = AdjAntiThunder*(1+MulAntiThunder)+AdjAntiThunder2
	SetTmpParam_At(119, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntiThunder = function(newv)
	SetTmpParam_At(120, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiThunder()
end
CLuaPlayerPropertyFight.SetParamMulAntiThunder = function(newv)
	SetTmpParam_At(121, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiThunder()
end
CLuaPlayerPropertyFight.RefreshParamAntiIce = function()
	local AdjAntiIce2 = GetTmpParam_At(446)
	local AdjAntiIce = GetTmpParam_At(123)
	local MulAntiIce = GetTmpParam_At(124)

	local newv = AdjAntiIce*(1+MulAntiIce)+AdjAntiIce2
	SetTmpParam_At(122, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntiIce = function(newv)
	SetTmpParam_At(123, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiIce()
end
CLuaPlayerPropertyFight.SetParamMulAntiIce = function(newv)
	SetTmpParam_At(124, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiIce()
end
CLuaPlayerPropertyFight.RefreshParamAntiPoison = function()
	local AdjAntiPoison2 = GetTmpParam_At(454)
	local AdjAntiPoison = GetTmpParam_At(126)
	local MulAntiPoison = GetTmpParam_At(127)

	local newv = AdjAntiPoison*(1+MulAntiPoison)+AdjAntiPoison2
	SetTmpParam_At(125, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntiPoison = function(newv)
	SetTmpParam_At(126, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiPoison()
end
CLuaPlayerPropertyFight.SetParamMulAntiPoison = function(newv)
	SetTmpParam_At(127, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiPoison()
end
CLuaPlayerPropertyFight.RefreshParamAntiWind = function()
	local AdjAntiWind2 = GetTmpParam_At(460)
	local AdjAntiWind = GetTmpParam_At(129)
	local MulAntiWind = GetTmpParam_At(130)

	local newv = AdjAntiWind*(1+MulAntiWind)+AdjAntiWind2
	SetTmpParam_At(128, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntiWind = function(newv)
	SetTmpParam_At(129, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiWind()
end
CLuaPlayerPropertyFight.SetParamMulAntiWind = function(newv)
	SetTmpParam_At(130, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiWind()
end
CLuaPlayerPropertyFight.RefreshParamAntiLight = function()
	local AdjAntiLight2 = GetTmpParam_At(448)
	local AdjAntiLight = GetTmpParam_At(132)
	local MulAntiLight = GetTmpParam_At(133)

	local newv = AdjAntiLight*(1+MulAntiLight)+AdjAntiLight2
	SetTmpParam_At(131, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntiLight = function(newv)
	SetTmpParam_At(132, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiLight()
end
CLuaPlayerPropertyFight.SetParamMulAntiLight = function(newv)
	SetTmpParam_At(133, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiLight()
end
CLuaPlayerPropertyFight.RefreshParamAntiIllusion = function()
	local AdjAntiIllusion2 = GetTmpParam_At(447)
	local AdjAntiIllusion = GetTmpParam_At(135)
	local MulAntiIllusion = GetTmpParam_At(136)

	local newv = AdjAntiIllusion*(1+MulAntiIllusion)+AdjAntiIllusion2
	SetTmpParam_At(134, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntiIllusion = function(newv)
	SetTmpParam_At(135, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiIllusion()
end
CLuaPlayerPropertyFight.SetParamMulAntiIllusion = function(newv)
	SetTmpParam_At(136, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiIllusion()
end
CLuaPlayerPropertyFight.RefreshParamIgnoreAntiFire = function()
	local AdjIgnoreAntiFire = GetTmpParam_At(417)
	local AdjIgnoreAntiFire2 = GetTmpParam_At(481)

	local newv = AdjIgnoreAntiFire+AdjIgnoreAntiFire2
	SetTmpParam_At(137, newv)
end
CLuaPlayerPropertyFight.RefreshParamIgnoreAntiThunder = function()
	local AdjIgnoreAntiThunder = GetTmpParam_At(418)
	local AdjIgnoreAntiThunder2 = GetTmpParam_At(486)

	local newv = AdjIgnoreAntiThunder+AdjIgnoreAntiThunder2
	SetTmpParam_At(138, newv)
end
CLuaPlayerPropertyFight.RefreshParamIgnoreAntiIce = function()
	local AdjIgnoreAntiIce = GetTmpParam_At(419)
	local AdjIgnoreAntiIce2 = GetTmpParam_At(482)

	local newv = AdjIgnoreAntiIce+AdjIgnoreAntiIce2
	SetTmpParam_At(139, newv)
end
CLuaPlayerPropertyFight.RefreshParamIgnoreAntiPoison = function()
	local AdjIgnoreAntiPoison = GetTmpParam_At(420)
	local AdjIgnoreAntiPoison2 = GetTmpParam_At(485)

	local newv = AdjIgnoreAntiPoison+AdjIgnoreAntiPoison2
	SetTmpParam_At(140, newv)
end
CLuaPlayerPropertyFight.RefreshParamIgnoreAntiWind = function()
	local AdjIgnoreAntiWind = GetTmpParam_At(421)
	local AdjIgnoreAntiWind2 = GetTmpParam_At(488)

	local newv = AdjIgnoreAntiWind+AdjIgnoreAntiWind2
	SetTmpParam_At(141, newv)
end
CLuaPlayerPropertyFight.RefreshParamIgnoreAntiLight = function()
	local AdjIgnoreAntiLight = GetTmpParam_At(422)
	local AdjIgnoreAntiLight2 = GetTmpParam_At(484)

	local newv = AdjIgnoreAntiLight+AdjIgnoreAntiLight2
	SetTmpParam_At(142, newv)
end
CLuaPlayerPropertyFight.RefreshParamIgnoreAntiIllusion = function()
	local AdjIgnoreAntiIllusion = GetTmpParam_At(423)
	local AdjIgnoreAntiIllusion2 = GetTmpParam_At(483)

	local newv = AdjIgnoreAntiIllusion+AdjIgnoreAntiIllusion2
	SetTmpParam_At(143, newv)
end
CLuaPlayerPropertyFight.RefreshParamEnhanceDizzy = function()
	local AdjEnhanceDizzy2 = GetTmpParam_At(467)
	local AdjEnhanceDizzy = GetTmpParam_At(145)
	local MulEnhanceDizzy = GetTmpParam_At(146)

	local newv = AdjEnhanceDizzy*(1+MulEnhanceDizzy)+AdjEnhanceDizzy2
	SetTmpParam_At(144, newv)
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceDizzy = function(newv)
	SetTmpParam_At(145, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceDizzy()
end
CLuaPlayerPropertyFight.SetParamMulEnhanceDizzy = function(newv)
	SetTmpParam_At(146, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceDizzy()
end
CLuaPlayerPropertyFight.RefreshParamEnhanceSleep = function()
	local AdjEnhanceSleep2 = GetTmpParam_At(476)
	local AdjEnhanceSleep = GetTmpParam_At(148)
	local MulEnhanceSleep = GetTmpParam_At(149)

	local newv = AdjEnhanceSleep*(1+MulEnhanceSleep)+AdjEnhanceSleep2
	SetTmpParam_At(147, newv)
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceSleep = function(newv)
	SetTmpParam_At(148, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceSleep()
end
CLuaPlayerPropertyFight.SetParamMulEnhanceSleep = function(newv)
	SetTmpParam_At(149, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceSleep()
end
CLuaPlayerPropertyFight.RefreshParamEnhanceChaos = function()
	local AdjEnhanceChaos2 = GetTmpParam_At(466)
	local AdjEnhanceChaos = GetTmpParam_At(151)
	local MulEnhanceChaos = GetTmpParam_At(152)

	local newv = AdjEnhanceChaos*(1+MulEnhanceChaos)+AdjEnhanceChaos2
	SetTmpParam_At(150, newv)
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceChaos = function(newv)
	SetTmpParam_At(151, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceChaos()
end
CLuaPlayerPropertyFight.SetParamMulEnhanceChaos = function(newv)
	SetTmpParam_At(152, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceChaos()
end
CLuaPlayerPropertyFight.RefreshParamEnhanceBind = function()
	local AdjEnhanceBind2 = GetTmpParam_At(465)
	local AdjEnhanceBind = GetTmpParam_At(154)
	local MulEnhanceBind = GetTmpParam_At(155)

	local newv = AdjEnhanceBind*(1+MulEnhanceBind)+AdjEnhanceBind2
	SetTmpParam_At(153, newv)
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceBind = function(newv)
	SetTmpParam_At(154, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceBind()
end
CLuaPlayerPropertyFight.SetParamMulEnhanceBind = function(newv)
	SetTmpParam_At(155, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceBind()
end
CLuaPlayerPropertyFight.RefreshParamEnhanceSilence = function()
	local AdjEnhanceSilence2 = GetTmpParam_At(475)
	local AdjEnhanceSilence = GetTmpParam_At(157)
	local MulEnhanceSilence = GetTmpParam_At(158)

	local newv = AdjEnhanceSilence*(1+MulEnhanceSilence)+AdjEnhanceSilence2
	SetTmpParam_At(156, newv)
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceSilence = function(newv)
	SetTmpParam_At(157, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceSilence()
end
CLuaPlayerPropertyFight.SetParamMulEnhanceSilence = function(newv)
	SetTmpParam_At(158, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceSilence()
end
CLuaPlayerPropertyFight.RefreshParamAntiDizzy = function()
	local AdjAntiDizzy2 = GetTmpParam_At(443)
	local AdjAntiDizzy = GetTmpParam_At(160)
	local MulAntiDizzy = GetTmpParam_At(161)

	local newv = AdjAntiDizzy*(1+MulAntiDizzy)+AdjAntiDizzy2
	SetTmpParam_At(159, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntiDizzy = function(newv)
	SetTmpParam_At(160, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiDizzy()
end
CLuaPlayerPropertyFight.SetParamMulAntiDizzy = function(newv)
	SetTmpParam_At(161, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiDizzy()
end
CLuaPlayerPropertyFight.RefreshParamAntiSleep = function()
	local AdjAntiSleep2 = GetTmpParam_At(456)
	local AdjAntiSleep = GetTmpParam_At(163)
	local MulAntiSleep = GetTmpParam_At(164)

	local newv = AdjAntiSleep*(1+MulAntiSleep)+AdjAntiSleep2
	SetTmpParam_At(162, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntiSleep = function(newv)
	SetTmpParam_At(163, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiSleep()
end
CLuaPlayerPropertyFight.SetParamMulAntiSleep = function(newv)
	SetTmpParam_At(164, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiSleep()
end
CLuaPlayerPropertyFight.RefreshParamAntiChaos = function()
	local AdjAntiChaos2 = GetTmpParam_At(441)
	local AdjAntiChaos = GetTmpParam_At(166)
	local MulAntiChaos = GetTmpParam_At(167)

	local newv = AdjAntiChaos*(1+MulAntiChaos)+AdjAntiChaos2
	SetTmpParam_At(165, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntiChaos = function(newv)
	SetTmpParam_At(166, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiChaos()
end
CLuaPlayerPropertyFight.SetParamMulAntiChaos = function(newv)
	SetTmpParam_At(167, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiChaos()
end
CLuaPlayerPropertyFight.RefreshParamAntiBind = function()
	local AdjAntiBind2 = GetTmpParam_At(437)
	local AdjAntiBind = GetTmpParam_At(169)
	local MulAntiBind = GetTmpParam_At(170)

	local newv = AdjAntiBind*(1+MulAntiBind)+AdjAntiBind2
	SetTmpParam_At(168, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntiBind = function(newv)
	SetTmpParam_At(169, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiBind()
end
CLuaPlayerPropertyFight.SetParamMulAntiBind = function(newv)
	SetTmpParam_At(170, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiBind()
end
CLuaPlayerPropertyFight.RefreshParamAntiSilence = function()
	local AdjAntiSilence2 = GetTmpParam_At(455)
	local AdjAntiSilence = GetTmpParam_At(172)
	local MulAntiSilence = GetTmpParam_At(173)

	local newv = AdjAntiSilence*(1+MulAntiSilence)+AdjAntiSilence2
	SetTmpParam_At(171, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntiSilence = function(newv)
	SetTmpParam_At(172, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiSilence()
end
CLuaPlayerPropertyFight.SetParamMulAntiSilence = function(newv)
	SetTmpParam_At(173, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiSilence()
end
CLuaPlayerPropertyFight.SetParamDizzyTimeChange = function(newv)
	SetTmpParam_At(174, newv)
end
CLuaPlayerPropertyFight.SetParamSleepTimeChange = function(newv)
	SetTmpParam_At(175, newv)
end
CLuaPlayerPropertyFight.SetParamChaosTimeChange = function(newv)
	SetTmpParam_At(176, newv)
end
CLuaPlayerPropertyFight.SetParamBindTimeChange = function(newv)
	SetTmpParam_At(177, newv)
end
CLuaPlayerPropertyFight.SetParamSilenceTimeChange = function(newv)
	SetTmpParam_At(178, newv)
end
CLuaPlayerPropertyFight.SetParamBianhuTimeChange = function(newv)
	SetTmpParam_At(179, newv)
end
CLuaPlayerPropertyFight.SetParamFreezeTimeChange = function(newv)
	SetTmpParam_At(180, newv)
end
CLuaPlayerPropertyFight.SetParamPetrifyTimeChange = function(newv)
	SetTmpParam_At(181, newv)
end
CLuaPlayerPropertyFight.RefreshParamEnhanceHeal = function()
	local AdjEnhanceHeal = GetTmpParam_At(183)

	local newv = AdjEnhanceHeal
	SetTmpParam_At(182, newv)
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceHeal = function(newv)
	SetTmpParam_At(183, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceHeal()
end
CLuaPlayerPropertyFight.SetParamMulEnhanceHeal = function(newv)
	SetTmpParam_At(184, newv)
end
CLuaPlayerPropertyFight.RefreshParamSpeed = function()
	local ZuoQiSpeed = GetTmpParam_At(370)
	local OriSpeed = GetTmpParam_At(186)
	local AdjSpeed = GetTmpParam_At(187)
	local MulSpeed = GetTmpParam_At(188)

	local newv = (OriSpeed + AdjSpeed)*max(0.3,1+MulSpeed)+ZuoQiSpeed
	SetTmpParam_At(185, newv)
end
CLuaPlayerPropertyFight.SetParamOriSpeed = function(newv)
	SetTmpParam_At(186, newv)

	CLuaPlayerPropertyFight.RefreshParamSpeed()
end
CLuaPlayerPropertyFight.SetParamAdjSpeed = function(newv)
	SetTmpParam_At(187, newv)

	CLuaPlayerPropertyFight.RefreshParamSpeed()
end
CLuaPlayerPropertyFight.SetParamMulSpeed = function(newv)
	SetTmpParam_At(188, newv)

	CLuaPlayerPropertyFight.RefreshParamSpeed()
end
CLuaPlayerPropertyFight.RefreshParamMF = function()
	local EquipExchangeMF = GetParam_At(11)
	local AdjMF = GetTmpParam_At(190)

	local newv = 0.001*floor(1000*EquipExchangeMF + 1000*AdjMF)
	SetTmpParam_At(189, newv)
end
CLuaPlayerPropertyFight.SetParamAdjMF = function(newv)
	SetTmpParam_At(190, newv)

	CLuaPlayerPropertyFight.RefreshParamMF()
end
CLuaPlayerPropertyFight.RefreshParamRange = function()
	local AdjRange = GetTmpParam_At(192)

	local newv = AdjRange
	SetTmpParam_At(191, newv)
end
CLuaPlayerPropertyFight.SetParamAdjRange = function(newv)
	SetTmpParam_At(192, newv)

	CLuaPlayerPropertyFight.RefreshParamRange()
end
CLuaPlayerPropertyFight.RefreshParamHatePlus = function()
	local AdjHatePlus = GetTmpParam_At(194)

	local newv = AdjHatePlus
	SetTmpParam_At(193, newv)
end
CLuaPlayerPropertyFight.SetParamAdjHatePlus = function(newv)
	SetTmpParam_At(194, newv)

	CLuaPlayerPropertyFight.RefreshParamHatePlus()
end
CLuaPlayerPropertyFight.SetParamHateDecrease = function(newv)
	SetTmpParam_At(195, newv)
end
CLuaPlayerPropertyFight.RefreshParamInvisible = function()
	local AdjInvisible = GetTmpParam_At(197)

	local newv = max(0,AdjInvisible)
	SetTmpParam_At(196, newv)
end
CLuaPlayerPropertyFight.SetParamAdjInvisible = function(newv)
	SetTmpParam_At(197, newv)

	CLuaPlayerPropertyFight.RefreshParamInvisible()
end
CLuaPlayerPropertyFight.RefreshParamTrueSight = function()
	local OriTrueSight = GetTmpParam_At(199)
	local AdjTrueSight = GetTmpParam_At(200)

	local newv = OriTrueSight + AdjTrueSight
	SetTmpParam_At(198, newv)
end
CLuaPlayerPropertyFight.RefreshParamOriTrueSight = function()

	local newv = 65
	SetTmpParam_At(199, newv)
end
CLuaPlayerPropertyFight.SetParamAdjTrueSight = function(newv)
	SetTmpParam_At(200, newv)

	CLuaPlayerPropertyFight.RefreshParamTrueSight()
end
CLuaPlayerPropertyFight.RefreshParamEyeSight = function()
	local OriEyeSight = GetTmpParam_At(202)
	local AdjEyeSight = GetTmpParam_At(203)

	local newv = OriEyeSight + AdjEyeSight
	SetTmpParam_At(201, newv)
end
CLuaPlayerPropertyFight.SetParamOriEyeSight = function(newv)
	SetTmpParam_At(202, newv)

	CLuaPlayerPropertyFight.RefreshParamEyeSight()
end
CLuaPlayerPropertyFight.SetParamAdjEyeSight = function(newv)
	SetTmpParam_At(203, newv)

	CLuaPlayerPropertyFight.RefreshParamEyeSight()
end
CLuaPlayerPropertyFight.SetParamEnhanceTian = function(newv)
	SetTmpParam_At(204, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceTianMul = function(newv)
	SetTmpParam_At(205, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceEGui = function(newv)
	SetTmpParam_At(206, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceEGuiMul = function(newv)
	SetTmpParam_At(207, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceXiuLuo = function(newv)
	SetTmpParam_At(208, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceXiuLuoMul = function(newv)
	SetTmpParam_At(209, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceDiYu = function(newv)
	SetTmpParam_At(210, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceDiYuMul = function(newv)
	SetTmpParam_At(211, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceRen = function(newv)
	SetTmpParam_At(212, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceRenMul = function(newv)
	SetTmpParam_At(213, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceChuSheng = function(newv)
	SetTmpParam_At(214, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceChuShengMul = function(newv)
	SetTmpParam_At(215, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceBuilding = function(newv)
	SetTmpParam_At(216, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceBuildingMul = function(newv)
	SetTmpParam_At(217, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceBoss = function(newv)
	SetTmpParam_At(218, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceBossMul = function(newv)
	SetTmpParam_At(219, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceSheShou = function(newv)
	SetTmpParam_At(220, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceSheShouMul = function(newv)
	SetTmpParam_At(221, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceJiaShi = function(newv)
	SetTmpParam_At(222, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceJiaShiMul = function(newv)
	SetTmpParam_At(223, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceFangShi = function(newv)
	SetTmpParam_At(224, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceFangShiMul = function(newv)
	SetTmpParam_At(225, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceYiShi = function(newv)
	SetTmpParam_At(226, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceYiShiMul = function(newv)
	SetTmpParam_At(227, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceMeiZhe = function(newv)
	SetTmpParam_At(228, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceMeiZheMul = function(newv)
	SetTmpParam_At(229, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceYiRen = function(newv)
	SetTmpParam_At(230, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceYiRenMul = function(newv)
	SetTmpParam_At(231, newv)
end
CLuaPlayerPropertyFight.SetParamIgnorepDef = function(newv)
	SetTmpParam_At(232, newv)
end
CLuaPlayerPropertyFight.SetParamIgnoremDef = function(newv)
	SetTmpParam_At(233, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceZhaoHuan = function(newv)
	SetTmpParam_At(234, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceZhaoHuanMul = function(newv)
	SetTmpParam_At(235, newv)
end
CLuaPlayerPropertyFight.SetParamDizzyProbability = function(newv)
	SetTmpParam_At(236, newv)
end
CLuaPlayerPropertyFight.SetParamSleepProbability = function(newv)
	SetTmpParam_At(237, newv)
end
CLuaPlayerPropertyFight.SetParamChaosProbability = function(newv)
	SetTmpParam_At(238, newv)
end
CLuaPlayerPropertyFight.SetParamBindProbability = function(newv)
	SetTmpParam_At(239, newv)
end
CLuaPlayerPropertyFight.SetParamSilenceProbability = function(newv)
	SetTmpParam_At(240, newv)
end
CLuaPlayerPropertyFight.SetParamHpFullPow2 = function(newv)
	SetTmpParam_At(241, newv)
end
CLuaPlayerPropertyFight.SetParamHpFullPow1 = function(newv)
	SetTmpParam_At(242, newv)
end
CLuaPlayerPropertyFight.SetParamHpFullInit = function(newv)
	SetTmpParam_At(243, newv)
end
CLuaPlayerPropertyFight.SetParamMpFullPow1 = function(newv)
	SetTmpParam_At(244, newv)
end
CLuaPlayerPropertyFight.SetParamMpFullInit = function(newv)
	SetTmpParam_At(245, newv)
end
CLuaPlayerPropertyFight.SetParamMpRecoverPow1 = function(newv)
	SetTmpParam_At(246, newv)
end
CLuaPlayerPropertyFight.SetParamMpRecoverInit = function(newv)
	SetTmpParam_At(247, newv)
end
CLuaPlayerPropertyFight.SetParamPAttMinPow2 = function(newv)
	SetTmpParam_At(248, newv)
end
CLuaPlayerPropertyFight.SetParamPAttMinPow1 = function(newv)
	SetTmpParam_At(249, newv)
end
CLuaPlayerPropertyFight.SetParamPAttMinInit = function(newv)
	SetTmpParam_At(250, newv)
end
CLuaPlayerPropertyFight.SetParamPAttMaxPow2 = function(newv)
	SetTmpParam_At(251, newv)
end
CLuaPlayerPropertyFight.SetParamPAttMaxPow1 = function(newv)
	SetTmpParam_At(252, newv)
end
CLuaPlayerPropertyFight.SetParamPAttMaxInit = function(newv)
	SetTmpParam_At(253, newv)
end
CLuaPlayerPropertyFight.SetParamPhitPow1 = function(newv)
	SetTmpParam_At(254, newv)
end
CLuaPlayerPropertyFight.SetParamPHitInit = function(newv)
	SetTmpParam_At(255, newv)
end
CLuaPlayerPropertyFight.SetParamPMissPow1 = function(newv)
	SetTmpParam_At(256, newv)
end
CLuaPlayerPropertyFight.SetParamPMissInit = function(newv)
	SetTmpParam_At(257, newv)
end
CLuaPlayerPropertyFight.SetParamPSpeedPow1 = function(newv)
	SetTmpParam_At(258, newv)
end
CLuaPlayerPropertyFight.SetParamPSpeedInit = function(newv)
	SetTmpParam_At(259, newv)
end
CLuaPlayerPropertyFight.SetParamPDefPow1 = function(newv)
	SetTmpParam_At(260, newv)
end
CLuaPlayerPropertyFight.SetParamPDefInit = function(newv)
	SetTmpParam_At(261, newv)
end
CLuaPlayerPropertyFight.SetParamPfatalPow1 = function(newv)
	SetTmpParam_At(262, newv)
end
CLuaPlayerPropertyFight.SetParamPFatalInit = function(newv)
	SetTmpParam_At(263, newv)
end
CLuaPlayerPropertyFight.SetParamMAttMinPow2 = function(newv)
	SetTmpParam_At(264, newv)
end
CLuaPlayerPropertyFight.SetParamMAttMinPow1 = function(newv)
	SetTmpParam_At(265, newv)
end
CLuaPlayerPropertyFight.SetParamMAttMinInit = function(newv)
	SetTmpParam_At(266, newv)
end
CLuaPlayerPropertyFight.SetParamMAttMaxPow2 = function(newv)
	SetTmpParam_At(267, newv)
end
CLuaPlayerPropertyFight.SetParamMAttMaxPow1 = function(newv)
	SetTmpParam_At(268, newv)
end
CLuaPlayerPropertyFight.SetParamMAttMaxInit = function(newv)
	SetTmpParam_At(269, newv)
end
CLuaPlayerPropertyFight.SetParamMSpeedPow1 = function(newv)
	SetTmpParam_At(270, newv)
end
CLuaPlayerPropertyFight.SetParamMSpeedInit = function(newv)
	SetTmpParam_At(271, newv)
end
CLuaPlayerPropertyFight.SetParamMDefPow1 = function(newv)
	SetTmpParam_At(272, newv)
end
CLuaPlayerPropertyFight.SetParamMDefInit = function(newv)
	SetTmpParam_At(273, newv)
end
CLuaPlayerPropertyFight.SetParamMHitPow1 = function(newv)
	SetTmpParam_At(274, newv)
end
CLuaPlayerPropertyFight.SetParamMHitInit = function(newv)
	SetTmpParam_At(275, newv)
end
CLuaPlayerPropertyFight.SetParamMMissPow1 = function(newv)
	SetTmpParam_At(276, newv)
end
CLuaPlayerPropertyFight.SetParamMMissInit = function(newv)
	SetTmpParam_At(277, newv)
end
CLuaPlayerPropertyFight.SetParamMFatalPow1 = function(newv)
	SetTmpParam_At(278, newv)
end
CLuaPlayerPropertyFight.SetParamMFatalInit = function(newv)
	SetTmpParam_At(279, newv)
end
CLuaPlayerPropertyFight.SetParamBlockDamagePow1 = function(newv)
	SetTmpParam_At(280, newv)
end
CLuaPlayerPropertyFight.SetParamBlockDamageInit = function(newv)
	SetTmpParam_At(281, newv)
end
CLuaPlayerPropertyFight.SetParamRunSpeed = function(newv)
	SetTmpParam_At(282, newv)
end
CLuaPlayerPropertyFight.SetParamHpRecoverPow1 = function(newv)
	SetTmpParam_At(283, newv)
end
CLuaPlayerPropertyFight.SetParamHpRecoverInit = function(newv)
	SetTmpParam_At(284, newv)
end
CLuaPlayerPropertyFight.SetParamAntiBlockDamagePow1 = function(newv)
	SetTmpParam_At(285, newv)
end
CLuaPlayerPropertyFight.SetParamAntiBlockDamageInit = function(newv)
	SetTmpParam_At(286, newv)
end
CLuaPlayerPropertyFight.RefreshParamBBMax = function()
	local AdjBBMax = GetTmpParam_At(288)

	local newv = AdjBBMax
	SetTmpParam_At(287, newv)
end
CLuaPlayerPropertyFight.SetParamAdjBBMax = function(newv)
	SetTmpParam_At(288, newv)

	CLuaPlayerPropertyFight.RefreshParamBBMax()
end
CLuaPlayerPropertyFight.RefreshParamAntiBreak = function()
	local AdjAntiBreak = GetTmpParam_At(290)

	local newv = AdjAntiBreak
	SetTmpParam_At(289, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntiBreak = function(newv)
	SetTmpParam_At(290, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiBreak()
end
CLuaPlayerPropertyFight.RefreshParamAntiPushPull = function()
	local AdjAntiPushPull = GetTmpParam_At(292)

	local newv = AdjAntiPushPull
	SetTmpParam_At(291, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntiPushPull = function(newv)
	SetTmpParam_At(292, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiPushPull()
end
CLuaPlayerPropertyFight.RefreshParamAntiPetrify = function()
	local AdjAntiPetrify2 = GetTmpParam_At(451)
	local AdjAntiPetrify = GetTmpParam_At(294)
	local MulAntiPetrify = GetTmpParam_At(295)

	local newv = AdjAntiPetrify*(1+MulAntiPetrify)+AdjAntiPetrify2
	SetTmpParam_At(293, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntiPetrify = function(newv)
	SetTmpParam_At(294, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiPetrify()
end
CLuaPlayerPropertyFight.SetParamMulAntiPetrify = function(newv)
	SetTmpParam_At(295, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiPetrify()
end
CLuaPlayerPropertyFight.RefreshParamAntiTie = function()
	local AdjAntiTie2 = GetTmpParam_At(458)
	local AdjAntiTie = GetTmpParam_At(297)
	local MulAntiTie = GetTmpParam_At(298)

	local newv = AdjAntiTie*(1+MulAntiTie)+AdjAntiTie2
	SetTmpParam_At(296, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntiTie = function(newv)
	SetTmpParam_At(297, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiTie()
end
CLuaPlayerPropertyFight.SetParamMulAntiTie = function(newv)
	SetTmpParam_At(298, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiTie()
end
CLuaPlayerPropertyFight.RefreshParamEnhancePetrify = function()
	local AdjEnhancePetrify2 = GetTmpParam_At(473)
	local AdjEnhancePetrify = GetTmpParam_At(300)
	local MulEnhancePetrify = GetTmpParam_At(301)

	local newv = AdjEnhancePetrify*(1+MulEnhancePetrify)+AdjEnhancePetrify2
	SetTmpParam_At(299, newv)
end
CLuaPlayerPropertyFight.SetParamAdjEnhancePetrify = function(newv)
	SetTmpParam_At(300, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhancePetrify()
end
CLuaPlayerPropertyFight.SetParamMulEnhancePetrify = function(newv)
	SetTmpParam_At(301, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhancePetrify()
end
CLuaPlayerPropertyFight.RefreshParamEnhanceTie = function()
	local AdjEnhanceTie2 = GetTmpParam_At(478)
	local AdjEnhanceTie = GetTmpParam_At(303)
	local MulEnhanceTie = GetTmpParam_At(304)

	local newv = AdjEnhanceTie*(1+MulEnhanceTie)+AdjEnhanceTie2
	SetTmpParam_At(302, newv)
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceTie = function(newv)
	SetTmpParam_At(303, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceTie()
end
CLuaPlayerPropertyFight.SetParamMulEnhanceTie = function(newv)
	SetTmpParam_At(304, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceTie()
end
CLuaPlayerPropertyFight.SetParamTieProbability = function(newv)
	SetTmpParam_At(305, newv)
end
CLuaPlayerPropertyFight.SetParamStrZizhi = function(newv)
	SetTmpParam_At(306, newv)
end
CLuaPlayerPropertyFight.SetParamCorZizhi = function(newv)
	SetTmpParam_At(307, newv)
end
CLuaPlayerPropertyFight.SetParamStaZizhi = function(newv)
	SetTmpParam_At(308, newv)
end
CLuaPlayerPropertyFight.SetParamAgiZizhi = function(newv)
	SetTmpParam_At(309, newv)
end
CLuaPlayerPropertyFight.SetParamIntZizhi = function(newv)
	SetTmpParam_At(310, newv)
end
CLuaPlayerPropertyFight.SetParamInitStrZizhi = function(newv)
	SetTmpParam_At(311, newv)
end
CLuaPlayerPropertyFight.SetParamInitCorZizhi = function(newv)
	SetTmpParam_At(312, newv)
end
CLuaPlayerPropertyFight.SetParamInitStaZizhi = function(newv)
	SetTmpParam_At(313, newv)
end
CLuaPlayerPropertyFight.SetParamInitAgiZizhi = function(newv)
	SetTmpParam_At(314, newv)
end
CLuaPlayerPropertyFight.SetParamInitIntZizhi = function(newv)
	SetTmpParam_At(315, newv)
end
CLuaPlayerPropertyFight.SetParamWuxing = function(newv)
	SetTmpParam_At(316, newv)
end
CLuaPlayerPropertyFight.SetParamXiuwei = function(newv)
	SetTmpParam_At(317, newv)
end
CLuaPlayerPropertyFight.SetParamSkillNum = function(newv)
	SetTmpParam_At(318, newv)
end
CLuaPlayerPropertyFight.SetParamPType = function(newv)
	SetTmpParam_At(319, newv)
end
CLuaPlayerPropertyFight.SetParamMType = function(newv)
	SetTmpParam_At(320, newv)
end
CLuaPlayerPropertyFight.SetParamPHType = function(newv)
	SetTmpParam_At(321, newv)
end
CLuaPlayerPropertyFight.SetParamGrowFactor = function(newv)
	SetTmpParam_At(322, newv)
end
CLuaPlayerPropertyFight.SetParamWuxingImprove = function(newv)
	SetTmpParam_At(323, newv)
end
CLuaPlayerPropertyFight.SetParamXiuweiImprove = function(newv)
	SetTmpParam_At(324, newv)
end
CLuaPlayerPropertyFight.RefreshParamEnhanceBeHeal = function()
	local AdjEnhanceBeHeal = GetTmpParam_At(326)

	local newv = AdjEnhanceBeHeal
	SetTmpParam_At(325, newv)
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceBeHeal = function(newv)
	SetTmpParam_At(326, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceBeHeal()
end
CLuaPlayerPropertyFight.SetParamMulEnhanceBeHeal = function(newv)
	SetTmpParam_At(327, newv)
end
CLuaPlayerPropertyFight.RefreshParamLookUpCor = function()
	local Class = GetTmpParam_At(1)
	local Grade = GetTmpParam_At(2)

	local newv = LookUpGrow1(Class, "Cor", {Class, Grade}, 0)
	SetTmpParam_At(328, newv)
end
CLuaPlayerPropertyFight.RefreshParamLookUpSta = function()
	local Class = GetTmpParam_At(1)
	local Grade = GetTmpParam_At(2)

	local newv = LookUpGrow1(Class, "Sta", {Class, Grade}, 0)
	SetTmpParam_At(329, newv)
end
CLuaPlayerPropertyFight.RefreshParamLookUpStr = function()
	local Class = GetTmpParam_At(1)
	local Grade = GetTmpParam_At(2)

	local newv = LookUpGrow1(Class, "Str", {Class, Grade}, 0)
	SetTmpParam_At(330, newv)
end
CLuaPlayerPropertyFight.RefreshParamLookUpInt = function()
	local Class = GetTmpParam_At(1)
	local Grade = GetTmpParam_At(2)

	local newv = LookUpGrow1(Class, "Int", {Class, Grade}, 0)
	SetTmpParam_At(331, newv)
end
CLuaPlayerPropertyFight.RefreshParamLookUpAgi = function()
	local Class = GetTmpParam_At(1)
	local Grade = GetTmpParam_At(2)

	local newv = LookUpGrow1(Class, "Agi", {Class, Grade}, 0)
	SetTmpParam_At(332, newv)
end
CLuaPlayerPropertyFight.RefreshParamLookUpHpFull = function()
	local Class = GetTmpParam_At(1)

	local newv = LookUpGrow2(Class, "HpFull", {Class, "Cor"}, 0)
	SetTmpParam_At(333, newv)
end
CLuaPlayerPropertyFight.RefreshParamLookUpMpFull = function()
	local Class = GetTmpParam_At(1)

	local newv = LookUpGrow2(Class, "MpFull", {Class, "Sta"}, 0)
	SetTmpParam_At(334, newv)
end
CLuaPlayerPropertyFight.RefreshParamLookUppAttMin = function()
	local Class = GetTmpParam_At(1)

	local newv = LookUpGrow2(Class, "pAttMin", {Class, "Str"}, 0)
	SetTmpParam_At(335, newv)
end
CLuaPlayerPropertyFight.RefreshParamLookUppAttMax = function()
	local Class = GetTmpParam_At(1)

	local newv = LookUpGrow2(Class, "pAttMax", {Class, "Str"}, 0)
	SetTmpParam_At(336, newv)
end
CLuaPlayerPropertyFight.RefreshParamLookUppHit = function()
	local Class = GetTmpParam_At(1)

	local newv = LookUpGrow2(Class, "pHit", {Class, "Agi"}, 0)
	SetTmpParam_At(337, newv)
end
CLuaPlayerPropertyFight.RefreshParamLookUppMiss = function()
	local Class = GetTmpParam_At(1)

	local newv = LookUpGrow2(Class, "pMiss", {Class, "Agi"}, 0)
	SetTmpParam_At(338, newv)
end
CLuaPlayerPropertyFight.RefreshParamLookUppDef = function()
	local Class = GetTmpParam_At(1)

	local newv = LookUpGrow2(Class, "pDef", {Class, "Str"}, 0)
	SetTmpParam_At(339, newv)
end
CLuaPlayerPropertyFight.RefreshParamLookUpmAttMin = function()
	local Class = GetTmpParam_At(1)

	local newv = LookUpGrow2(Class, "mAttMin", {Class, "Int"}, 0)
	SetTmpParam_At(340, newv)
end
CLuaPlayerPropertyFight.RefreshParamLookUpmAttMax = function()
	local Class = GetTmpParam_At(1)

	local newv = LookUpGrow2(Class, "mAttMax", {Class, "Int"}, 0)
	SetTmpParam_At(341, newv)
end
CLuaPlayerPropertyFight.RefreshParamLookUpmHit = function()
	local Class = GetTmpParam_At(1)

	local newv = LookUpGrow2(Class, "mHit", {Class, "Agi"}, 0)
	SetTmpParam_At(342, newv)
end
CLuaPlayerPropertyFight.RefreshParamLookUpmMiss = function()
	local Class = GetTmpParam_At(1)

	local newv = LookUpGrow2(Class, "mMiss", {Class, "Agi"}, 0)
	SetTmpParam_At(343, newv)
end
CLuaPlayerPropertyFight.RefreshParamLookUpmDef = function()
	local Class = GetTmpParam_At(1)

	local newv = LookUpGrow2(Class, "mDef", {Class, "Int"}, 0)
	SetTmpParam_At(344, newv)
end
CLuaPlayerPropertyFight.SetParamAdjHpFull2 = function(newv)
	SetTmpParam_At(345, newv)

	CLuaPlayerPropertyFight.RefreshParamHpFull()
end
CLuaPlayerPropertyFight.SetParamAdjpAttMin2 = function(newv)
	SetTmpParam_At(346, newv)

	CLuaPlayerPropertyFight.RefreshParampAttMin()
end
CLuaPlayerPropertyFight.SetParamAdjpAttMax2 = function(newv)
	SetTmpParam_At(347, newv)

	CLuaPlayerPropertyFight.RefreshParampAttMax()

	CLuaPlayerPropertyFight.RefreshParampAttMin()
end
CLuaPlayerPropertyFight.SetParamAdjmAttMin2 = function(newv)
	SetTmpParam_At(348, newv)

	CLuaPlayerPropertyFight.RefreshParammAttMin()
end
CLuaPlayerPropertyFight.SetParamAdjmAttMax2 = function(newv)
	SetTmpParam_At(349, newv)

	CLuaPlayerPropertyFight.RefreshParammAttMin()

	CLuaPlayerPropertyFight.RefreshParammAttMax()
end
CLuaPlayerPropertyFight.SetParamLingShouType = function(newv)
	SetTmpParam_At(350, newv)
end
CLuaPlayerPropertyFight.SetParamMHType = function(newv)
	SetTmpParam_At(351, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceLingShou = function(newv)
	SetTmpParam_At(352, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceLingShouMul = function(newv)
	SetTmpParam_At(353, newv)
end
CLuaPlayerPropertyFight.RefreshParamAntiAoe = function()
	local AdjAntiAoe = GetTmpParam_At(355)

	local newv = AdjAntiAoe
	SetTmpParam_At(354, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntiAoe = function(newv)
	SetTmpParam_At(355, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiAoe()
end
CLuaPlayerPropertyFight.RefreshParamAntiBlind = function()
	local AdjAntiBlind2 = GetTmpParam_At(438)
	local AdjAntiBlind = GetTmpParam_At(357)
	local MulAntiBlind = GetTmpParam_At(358)

	local newv = AdjAntiBlind*(1+MulAntiBlind)+AdjAntiBlind2
	SetTmpParam_At(356, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntiBlind = function(newv)
	SetTmpParam_At(357, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiBlind()
end
CLuaPlayerPropertyFight.SetParamMulAntiBlind = function(newv)
	SetTmpParam_At(358, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiBlind()
end
CLuaPlayerPropertyFight.RefreshParamAntiDecelerate = function()
	local AdjAntiDecelerate2 = GetTmpParam_At(442)
	local AdjAntiDecelerate = GetTmpParam_At(360)
	local MulAntiDecelerate = GetTmpParam_At(361)

	local newv = AdjAntiDecelerate*(1+MulAntiDecelerate)+AdjAntiDecelerate2
	SetTmpParam_At(359, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntiDecelerate = function(newv)
	SetTmpParam_At(360, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiDecelerate()
end
CLuaPlayerPropertyFight.SetParamMulAntiDecelerate = function(newv)
	SetTmpParam_At(361, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiDecelerate()
end
CLuaPlayerPropertyFight.SetParamMinGrade = function(newv)
	SetTmpParam_At(362, newv)
end
CLuaPlayerPropertyFight.RefreshParamCharacterCor = function()
	local LookUpCor = GetTmpParam_At(328)
	local AdjustPermanentCor = GetTmpParam_At(611)

	local newv = LookUpCor+AdjustPermanentCor
	SetTmpParam_At(363, newv)
end
CLuaPlayerPropertyFight.RefreshParamCharacterSta = function()
	local LookUpSta = GetTmpParam_At(329)
	local AdjustPermanentSta = GetTmpParam_At(612)

	local newv = LookUpSta+AdjustPermanentSta
	SetTmpParam_At(364, newv)
end
CLuaPlayerPropertyFight.RefreshParamCharacterStr = function()
	local LookUpStr = GetTmpParam_At(330)
	local AdjustPermanentStr = GetTmpParam_At(613)

	local newv = LookUpStr+AdjustPermanentStr
	SetTmpParam_At(365, newv)
end
CLuaPlayerPropertyFight.RefreshParamCharacterInt = function()
	local LookUpInt = GetTmpParam_At(331)
	local AdjustPermanentInt = GetTmpParam_At(614)

	local newv = LookUpInt+AdjustPermanentInt
	SetTmpParam_At(366, newv)
end
CLuaPlayerPropertyFight.RefreshParamCharacterAgi = function()
	local LookUpAgi = GetTmpParam_At(332)
	local AdjustPermanentAgi = GetTmpParam_At(615)

	local newv = LookUpAgi+AdjustPermanentAgi
	SetTmpParam_At(367, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceDaoKe = function(newv)
	SetTmpParam_At(368, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceDaoKeMul = function(newv)
	SetTmpParam_At(369, newv)
end
CLuaPlayerPropertyFight.SetParamZuoQiSpeed = function(newv)
	SetTmpParam_At(370, newv)

	CLuaPlayerPropertyFight.RefreshParamSpeed()
end
CLuaPlayerPropertyFight.RefreshParamEnhanceBianHu = function()
	local AdjEnhanceBianHu2 = GetTmpParam_At(464)
	local AdjEnhanceBianHu = GetTmpParam_At(372)
	local MulEnhanceBianHu = GetTmpParam_At(373)

	local newv = AdjEnhanceBianHu*(1+MulEnhanceBianHu)+AdjEnhanceBianHu2
	SetTmpParam_At(371, newv)
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceBianHu = function(newv)
	SetTmpParam_At(372, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceBianHu()
end
CLuaPlayerPropertyFight.SetParamMulEnhanceBianHu = function(newv)
	SetTmpParam_At(373, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceBianHu()
end
CLuaPlayerPropertyFight.RefreshParamAntiBianHu = function()
	local AdjAntiBianHu2 = GetTmpParam_At(436)
	local AdjAntiBianHu = GetTmpParam_At(375)
	local MulAntiBianHu = GetTmpParam_At(376)

	local newv = AdjAntiBianHu*(1+MulAntiBianHu)+AdjAntiBianHu2
	SetTmpParam_At(374, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntiBianHu = function(newv)
	SetTmpParam_At(375, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiBianHu()
end
CLuaPlayerPropertyFight.SetParamMulAntiBianHu = function(newv)
	SetTmpParam_At(376, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiBianHu()
end
CLuaPlayerPropertyFight.SetParamEnhanceXiaKe = function(newv)
	SetTmpParam_At(377, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceXiaKeMul = function(newv)
	SetTmpParam_At(378, newv)
end
CLuaPlayerPropertyFight.SetParamTieTimeChange = function(newv)
	SetTmpParam_At(379, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceYanShi = function(newv)
	SetTmpParam_At(380, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceYanShiMul = function(newv)
	SetTmpParam_At(381, newv)
end
CLuaPlayerPropertyFight.SetParamPAType = function(newv)
	SetTmpParam_At(382, newv)
end
CLuaPlayerPropertyFight.SetParamMAType = function(newv)
	SetTmpParam_At(383, newv)
end
CLuaPlayerPropertyFight.SetParamlifetimeNoReduceRate = function(newv)
	SetTmpParam_At(384, newv)
end
CLuaPlayerPropertyFight.SetParamAddLSHpDurgImprove = function(newv)
	SetTmpParam_At(385, newv)
end
CLuaPlayerPropertyFight.SetParamAddHpDurgImprove = function(newv)
	SetTmpParam_At(386, newv)
end
CLuaPlayerPropertyFight.SetParamAddMpDurgImprove = function(newv)
	SetTmpParam_At(387, newv)
end
CLuaPlayerPropertyFight.SetParamFireHurtReduce = function(newv)
	SetTmpParam_At(388, newv)
end
CLuaPlayerPropertyFight.SetParamThunderHurtReduce = function(newv)
	SetTmpParam_At(389, newv)
end
CLuaPlayerPropertyFight.SetParamIceHurtReduce = function(newv)
	SetTmpParam_At(390, newv)
end
CLuaPlayerPropertyFight.SetParamPoisonHurtReduce = function(newv)
	SetTmpParam_At(391, newv)
end
CLuaPlayerPropertyFight.SetParamWindHurtReduce = function(newv)
	SetTmpParam_At(392, newv)
end
CLuaPlayerPropertyFight.SetParamLightHurtReduce = function(newv)
	SetTmpParam_At(393, newv)
end
CLuaPlayerPropertyFight.SetParamIllusionHurtReduce = function(newv)
	SetTmpParam_At(394, newv)
end
CLuaPlayerPropertyFight.SetParamFakepDef = function(newv)
	SetTmpParam_At(395, newv)
end
CLuaPlayerPropertyFight.SetParamFakemDef = function(newv)
	SetTmpParam_At(396, newv)
end
CLuaPlayerPropertyFight.RefreshParamEnhanceWater = function()
	local AdjEnhanceWater = GetTmpParam_At(398)
	local AdjEnhanceWater2 = GetTmpParam_At(479)

	local newv = AdjEnhanceWater+AdjEnhanceWater2
	SetTmpParam_At(397, newv)
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceWater = function(newv)
	SetTmpParam_At(398, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceWater()
end
CLuaPlayerPropertyFight.SetParamMulEnhanceWater = function(newv)
	SetTmpParam_At(399, newv)
end
CLuaPlayerPropertyFight.RefreshParamAntiWater = function()
	local AdjAntiWater2 = GetTmpParam_At(459)
	local AdjAntiWater = GetTmpParam_At(401)
	local MulAntiWater = GetTmpParam_At(402)

	local newv = AdjAntiWater*(1+MulAntiWater)+AdjAntiWater2
	SetTmpParam_At(400, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntiWater = function(newv)
	SetTmpParam_At(401, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiWater()
end
CLuaPlayerPropertyFight.SetParamMulAntiWater = function(newv)
	SetTmpParam_At(402, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiWater()
end
CLuaPlayerPropertyFight.SetParamWaterHurtReduce = function(newv)
	SetTmpParam_At(403, newv)
end
CLuaPlayerPropertyFight.RefreshParamIgnoreAntiWater = function()
	local AdjIgnoreAntiWater = GetTmpParam_At(431)
	local AdjIgnoreAntiWater2 = GetTmpParam_At(487)

	local newv = AdjIgnoreAntiWater+AdjIgnoreAntiWater2
	SetTmpParam_At(404, newv)
end
CLuaPlayerPropertyFight.RefreshParamAntiFreeze = function()
	local AdjAntiFreeze2 = GetTmpParam_At(445)
	local AdjAntiFreeze = GetTmpParam_At(406)
	local MulAntiFreeze = GetTmpParam_At(407)

	local newv = AdjAntiFreeze*(1+MulAntiFreeze)+AdjAntiFreeze2
	SetTmpParam_At(405, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntiFreeze = function(newv)
	SetTmpParam_At(406, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiFreeze()
end
CLuaPlayerPropertyFight.SetParamMulAntiFreeze = function(newv)
	SetTmpParam_At(407, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiFreeze()
end
CLuaPlayerPropertyFight.RefreshParamEnhanceFreeze = function()
	local AdjEnhanceFreeze2 = GetTmpParam_At(469)
	local AdjEnhanceFreeze = GetTmpParam_At(409)
	local MulEnhanceFreeze = GetTmpParam_At(410)

	local newv = AdjEnhanceFreeze*(1+MulEnhanceFreeze)+AdjEnhanceFreeze2
	SetTmpParam_At(408, newv)
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceFreeze = function(newv)
	SetTmpParam_At(409, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceFreeze()
end
CLuaPlayerPropertyFight.SetParamMulEnhanceFreeze = function(newv)
	SetTmpParam_At(410, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceFreeze()
end
CLuaPlayerPropertyFight.SetParamEnhanceHuaHun = function(newv)
	SetTmpParam_At(411, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceHuaHunMul = function(newv)
	SetTmpParam_At(412, newv)
end
CLuaPlayerPropertyFight.SetParamTrueSightProb = function(newv)
	SetTmpParam_At(413, newv)
end
CLuaPlayerPropertyFight.SetParamLSBBGrade = function(newv)
	SetTmpParam_At(414, newv)
end
CLuaPlayerPropertyFight.SetParamLSBBQuality = function(newv)
	SetTmpParam_At(415, newv)
end
CLuaPlayerPropertyFight.SetParamLSBBAdjHp = function(newv)
	SetTmpParam_At(416, newv)
end
CLuaPlayerPropertyFight.SetParamAdjIgnoreAntiFire = function(newv)
	SetTmpParam_At(417, newv)

	CLuaPlayerPropertyFight.RefreshParamIgnoreAntiFire()
end
CLuaPlayerPropertyFight.SetParamAdjIgnoreAntiThunder = function(newv)
	SetTmpParam_At(418, newv)

	CLuaPlayerPropertyFight.RefreshParamIgnoreAntiThunder()
end
CLuaPlayerPropertyFight.SetParamAdjIgnoreAntiIce = function(newv)
	SetTmpParam_At(419, newv)

	CLuaPlayerPropertyFight.RefreshParamIgnoreAntiIce()
end
CLuaPlayerPropertyFight.SetParamAdjIgnoreAntiPoison = function(newv)
	SetTmpParam_At(420, newv)

	CLuaPlayerPropertyFight.RefreshParamIgnoreAntiPoison()
end
CLuaPlayerPropertyFight.SetParamAdjIgnoreAntiWind = function(newv)
	SetTmpParam_At(421, newv)

	CLuaPlayerPropertyFight.RefreshParamIgnoreAntiWind()
end
CLuaPlayerPropertyFight.SetParamAdjIgnoreAntiLight = function(newv)
	SetTmpParam_At(422, newv)

	CLuaPlayerPropertyFight.RefreshParamIgnoreAntiLight()
end
CLuaPlayerPropertyFight.SetParamAdjIgnoreAntiIllusion = function(newv)
	SetTmpParam_At(423, newv)

	CLuaPlayerPropertyFight.RefreshParamIgnoreAntiIllusion()
end
CLuaPlayerPropertyFight.SetParamIgnoreAntiFireLSMul = function(newv)
	SetTmpParam_At(424, newv)
end
CLuaPlayerPropertyFight.SetParamIgnoreAntiThunderLSMul = function(newv)
	SetTmpParam_At(425, newv)
end
CLuaPlayerPropertyFight.SetParamIgnoreAntiIceLSMul = function(newv)
	SetTmpParam_At(426, newv)
end
CLuaPlayerPropertyFight.SetParamIgnoreAntiPoisonLSMul = function(newv)
	SetTmpParam_At(427, newv)
end
CLuaPlayerPropertyFight.SetParamIgnoreAntiWindLSMul = function(newv)
	SetTmpParam_At(428, newv)
end
CLuaPlayerPropertyFight.SetParamIgnoreAntiLightLSMul = function(newv)
	SetTmpParam_At(429, newv)
end
CLuaPlayerPropertyFight.SetParamIgnoreAntiIllusionLSMul = function(newv)
	SetTmpParam_At(430, newv)
end
CLuaPlayerPropertyFight.SetParamAdjIgnoreAntiWater = function(newv)
	SetTmpParam_At(431, newv)

	CLuaPlayerPropertyFight.RefreshParamIgnoreAntiWater()
end
CLuaPlayerPropertyFight.SetParamIgnoreAntiWaterLSMul = function(newv)
	SetTmpParam_At(432, newv)
end
CLuaPlayerPropertyFight.SetParamMulSpeed2 = function(newv)
	SetTmpParam_At(433, newv)
end
CLuaPlayerPropertyFight.SetParamMulConsumeMp = function(newv)
	SetTmpParam_At(434, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAgi2 = function(newv)
	SetTmpParam_At(435, newv)

	CLuaPlayerPropertyFight.RefreshParamAgi()

	CLuaPlayerPropertyFight.RefreshParammMiss()

	CLuaPlayerPropertyFight.RefreshParammHit()

	CLuaPlayerPropertyFight.RefreshParampMiss()

	CLuaPlayerPropertyFight.RefreshParampHit()
end
CLuaPlayerPropertyFight.SetParamAdjAntiBianHu2 = function(newv)
	SetTmpParam_At(436, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiBianHu()
end
CLuaPlayerPropertyFight.SetParamAdjAntiBind2 = function(newv)
	SetTmpParam_At(437, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiBind()
end
CLuaPlayerPropertyFight.SetParamAdjAntiBlind2 = function(newv)
	SetTmpParam_At(438, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiBlind()
end
CLuaPlayerPropertyFight.SetParamAdjAntiBlock2 = function(newv)
	SetTmpParam_At(439, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiBlock()
end
CLuaPlayerPropertyFight.SetParamAdjAntiBlockDamage2 = function(newv)
	SetTmpParam_At(440, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiBlockDamage()
end
CLuaPlayerPropertyFight.SetParamAdjAntiChaos2 = function(newv)
	SetTmpParam_At(441, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiChaos()
end
CLuaPlayerPropertyFight.SetParamAdjAntiDecelerate2 = function(newv)
	SetTmpParam_At(442, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiDecelerate()
end
CLuaPlayerPropertyFight.SetParamAdjAntiDizzy2 = function(newv)
	SetTmpParam_At(443, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiDizzy()
end
CLuaPlayerPropertyFight.SetParamAdjAntiFire2 = function(newv)
	SetTmpParam_At(444, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiFire()
end
CLuaPlayerPropertyFight.SetParamAdjAntiFreeze2 = function(newv)
	SetTmpParam_At(445, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiFreeze()
end
CLuaPlayerPropertyFight.SetParamAdjAntiIce2 = function(newv)
	SetTmpParam_At(446, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiIce()
end
CLuaPlayerPropertyFight.SetParamAdjAntiIllusion2 = function(newv)
	SetTmpParam_At(447, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiIllusion()
end
CLuaPlayerPropertyFight.SetParamAdjAntiLight2 = function(newv)
	SetTmpParam_At(448, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiLight()
end
CLuaPlayerPropertyFight.SetParamAdjAntimFatal2 = function(newv)
	SetTmpParam_At(449, newv)

	CLuaPlayerPropertyFight.RefreshParamAntimFatal()
end
CLuaPlayerPropertyFight.SetParamAdjAntimFatalDamage2 = function(newv)
	SetTmpParam_At(450, newv)

	CLuaPlayerPropertyFight.RefreshParamAntimFatalDamage()
end
CLuaPlayerPropertyFight.SetParamAdjAntiPetrify2 = function(newv)
	SetTmpParam_At(451, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiPetrify()
end
CLuaPlayerPropertyFight.SetParamAdjAntipFatal2 = function(newv)
	SetTmpParam_At(452, newv)

	CLuaPlayerPropertyFight.RefreshParamAntipFatal()
end
CLuaPlayerPropertyFight.SetParamAdjAntipFatalDamage2 = function(newv)
	SetTmpParam_At(453, newv)

	CLuaPlayerPropertyFight.RefreshParamAntipFatalDamage()
end
CLuaPlayerPropertyFight.SetParamAdjAntiPoison2 = function(newv)
	SetTmpParam_At(454, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiPoison()
end
CLuaPlayerPropertyFight.SetParamAdjAntiSilence2 = function(newv)
	SetTmpParam_At(455, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiSilence()
end
CLuaPlayerPropertyFight.SetParamAdjAntiSleep2 = function(newv)
	SetTmpParam_At(456, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiSleep()
end
CLuaPlayerPropertyFight.SetParamAdjAntiThunder2 = function(newv)
	SetTmpParam_At(457, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiThunder()
end
CLuaPlayerPropertyFight.SetParamAdjAntiTie2 = function(newv)
	SetTmpParam_At(458, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiTie()
end
CLuaPlayerPropertyFight.SetParamAdjAntiWater2 = function(newv)
	SetTmpParam_At(459, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiWater()
end
CLuaPlayerPropertyFight.SetParamAdjAntiWind2 = function(newv)
	SetTmpParam_At(460, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiWind()
end
CLuaPlayerPropertyFight.SetParamAdjBlock2 = function(newv)
	SetTmpParam_At(461, newv)

	CLuaPlayerPropertyFight.RefreshParamBlock()
end
CLuaPlayerPropertyFight.SetParamAdjBlockDamage2 = function(newv)
	SetTmpParam_At(462, newv)

	CLuaPlayerPropertyFight.RefreshParamBlockDamage()
end
CLuaPlayerPropertyFight.SetParamAdjCor2 = function(newv)
	SetTmpParam_At(463, newv)

	CLuaPlayerPropertyFight.RefreshParamCor()

	CLuaPlayerPropertyFight.RefreshParamHpFull()
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceBianHu2 = function(newv)
	SetTmpParam_At(464, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceBianHu()
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceBind2 = function(newv)
	SetTmpParam_At(465, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceBind()
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceChaos2 = function(newv)
	SetTmpParam_At(466, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceChaos()
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceDizzy2 = function(newv)
	SetTmpParam_At(467, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceDizzy()
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceFire2 = function(newv)
	SetTmpParam_At(468, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceFire()
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceFreeze2 = function(newv)
	SetTmpParam_At(469, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceFreeze()
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceIce2 = function(newv)
	SetTmpParam_At(470, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceIce()
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceIllusion2 = function(newv)
	SetTmpParam_At(471, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceIllusion()
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceLight2 = function(newv)
	SetTmpParam_At(472, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceLight()
end
CLuaPlayerPropertyFight.SetParamAdjEnhancePetrify2 = function(newv)
	SetTmpParam_At(473, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhancePetrify()
end
CLuaPlayerPropertyFight.SetParamAdjEnhancePoison2 = function(newv)
	SetTmpParam_At(474, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhancePoison()
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceSilence2 = function(newv)
	SetTmpParam_At(475, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceSilence()
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceSleep2 = function(newv)
	SetTmpParam_At(476, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceSleep()
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceThunder2 = function(newv)
	SetTmpParam_At(477, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceThunder()
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceTie2 = function(newv)
	SetTmpParam_At(478, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceTie()
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceWater2 = function(newv)
	SetTmpParam_At(479, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceWater()
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceWind2 = function(newv)
	SetTmpParam_At(480, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceWind()
end
CLuaPlayerPropertyFight.SetParamAdjIgnoreAntiFire2 = function(newv)
	SetTmpParam_At(481, newv)

	CLuaPlayerPropertyFight.RefreshParamIgnoreAntiFire()
end
CLuaPlayerPropertyFight.SetParamAdjIgnoreAntiIce2 = function(newv)
	SetTmpParam_At(482, newv)

	CLuaPlayerPropertyFight.RefreshParamIgnoreAntiIce()
end
CLuaPlayerPropertyFight.SetParamAdjIgnoreAntiIllusion2 = function(newv)
	SetTmpParam_At(483, newv)

	CLuaPlayerPropertyFight.RefreshParamIgnoreAntiIllusion()
end
CLuaPlayerPropertyFight.SetParamAdjIgnoreAntiLight2 = function(newv)
	SetTmpParam_At(484, newv)

	CLuaPlayerPropertyFight.RefreshParamIgnoreAntiLight()
end
CLuaPlayerPropertyFight.SetParamAdjIgnoreAntiPoison2 = function(newv)
	SetTmpParam_At(485, newv)

	CLuaPlayerPropertyFight.RefreshParamIgnoreAntiPoison()
end
CLuaPlayerPropertyFight.SetParamAdjIgnoreAntiThunder2 = function(newv)
	SetTmpParam_At(486, newv)

	CLuaPlayerPropertyFight.RefreshParamIgnoreAntiThunder()
end
CLuaPlayerPropertyFight.SetParamAdjIgnoreAntiWater2 = function(newv)
	SetTmpParam_At(487, newv)

	CLuaPlayerPropertyFight.RefreshParamIgnoreAntiWater()
end
CLuaPlayerPropertyFight.SetParamAdjIgnoreAntiWind2 = function(newv)
	SetTmpParam_At(488, newv)

	CLuaPlayerPropertyFight.RefreshParamIgnoreAntiWind()
end
CLuaPlayerPropertyFight.SetParamAdjInt2 = function(newv)
	SetTmpParam_At(489, newv)

	CLuaPlayerPropertyFight.RefreshParamInt()

	CLuaPlayerPropertyFight.RefreshParammAttMin()

	CLuaPlayerPropertyFight.RefreshParammAttMax()

	CLuaPlayerPropertyFight.RefreshParammDef()
end
CLuaPlayerPropertyFight.SetParamAdjmDef2 = function(newv)
	SetTmpParam_At(490, newv)

	CLuaPlayerPropertyFight.RefreshParammDef()
end
CLuaPlayerPropertyFight.SetParamAdjmFatal2 = function(newv)
	SetTmpParam_At(491, newv)

	CLuaPlayerPropertyFight.RefreshParammFatal()
end
CLuaPlayerPropertyFight.SetParamAdjmFatalDamage2 = function(newv)
	SetTmpParam_At(492, newv)

	CLuaPlayerPropertyFight.RefreshParammFatalDamage()
end
CLuaPlayerPropertyFight.SetParamAdjmHit2 = function(newv)
	SetTmpParam_At(493, newv)

	CLuaPlayerPropertyFight.RefreshParammHit()
end
CLuaPlayerPropertyFight.SetParamAdjmHurt2 = function(newv)
	SetTmpParam_At(494, newv)
end
CLuaPlayerPropertyFight.SetParamAdjmMiss2 = function(newv)
	SetTmpParam_At(495, newv)

	CLuaPlayerPropertyFight.RefreshParammMiss()
end
CLuaPlayerPropertyFight.SetParamAdjmSpeed2 = function(newv)
	SetTmpParam_At(496, newv)
end
CLuaPlayerPropertyFight.SetParamAdjpDef2 = function(newv)
	SetTmpParam_At(497, newv)

	CLuaPlayerPropertyFight.RefreshParampDef()
end
CLuaPlayerPropertyFight.SetParamAdjpFatal2 = function(newv)
	SetTmpParam_At(498, newv)

	CLuaPlayerPropertyFight.RefreshParampFatal()
end
CLuaPlayerPropertyFight.SetParamAdjpFatalDamage2 = function(newv)
	SetTmpParam_At(499, newv)

	CLuaPlayerPropertyFight.RefreshParampFatalDamage()
end
CLuaPlayerPropertyFight.SetParamAdjpHit2 = function(newv)
	SetTmpParam_At(500, newv)

	CLuaPlayerPropertyFight.RefreshParampHit()
end
CLuaPlayerPropertyFight.SetParamAdjpHurt2 = function(newv)
	SetTmpParam_At(501, newv)
end
CLuaPlayerPropertyFight.SetParamAdjpMiss2 = function(newv)
	SetTmpParam_At(502, newv)

	CLuaPlayerPropertyFight.RefreshParampMiss()
end
CLuaPlayerPropertyFight.SetParamAdjpSpeed2 = function(newv)
	SetTmpParam_At(503, newv)
end
CLuaPlayerPropertyFight.SetParamAdjStr2 = function(newv)
	SetTmpParam_At(504, newv)

	CLuaPlayerPropertyFight.RefreshParamStr()

	CLuaPlayerPropertyFight.RefreshParampAttMax()

	CLuaPlayerPropertyFight.RefreshParampDef()

	CLuaPlayerPropertyFight.RefreshParampAttMin()
end
CLuaPlayerPropertyFight.RefreshParamLSpAttMin = function()
	local LSpAttMin_adj = GetTmpParam_At(523)
	local LSpAttMin_jc = GetTmpParam_At(541)

	local newv = LSpAttMin_adj+LSpAttMin_jc
	SetTmpParam_At(505, newv)
end
CLuaPlayerPropertyFight.RefreshParamLSpAttMax = function()
	local LSpAttMax_adj = GetTmpParam_At(524)
	local LSpAttMax_jc = GetTmpParam_At(542)

	local newv = LSpAttMax_adj+LSpAttMax_jc
	SetTmpParam_At(506, newv)
end
CLuaPlayerPropertyFight.RefreshParamLSmAttMin = function()
	local LSmAttMin_adj = GetTmpParam_At(525)
	local LSmAttMin_jc = GetTmpParam_At(543)

	local newv = LSmAttMin_adj+LSmAttMin_jc
	SetTmpParam_At(507, newv)
end
CLuaPlayerPropertyFight.RefreshParamLSmAttMax = function()
	local LSmAttMax_adj = GetTmpParam_At(526)
	local LSmAttMax_jc = GetTmpParam_At(544)

	local newv = LSmAttMax_adj+LSmAttMax_jc
	SetTmpParam_At(508, newv)
end
CLuaPlayerPropertyFight.RefreshParamLSpFatal = function()
	local LSpFatal_adj = GetTmpParam_At(527)
	local LSpFatal_jc = GetTmpParam_At(545)

	local newv = LSpFatal_adj+LSpFatal_jc
	SetTmpParam_At(509, newv)
end
CLuaPlayerPropertyFight.RefreshParamLSpFatalDamage = function()
	local LSpFatalDamage_adj = GetTmpParam_At(528)
	local LSpFatalDamage_jc = GetTmpParam_At(546)

	local newv = LSpFatalDamage_adj+LSpFatalDamage_jc
	SetTmpParam_At(510, newv)
end
CLuaPlayerPropertyFight.RefreshParamLSmFatal = function()
	local LSmFatal_adj = GetTmpParam_At(529)
	local LSmFatal_jc = GetTmpParam_At(547)

	local newv = LSmFatal_adj+LSmFatal_jc
	SetTmpParam_At(511, newv)
end
CLuaPlayerPropertyFight.RefreshParamLSmFatalDamage = function()
	local LSmFatalDamage_adj = GetTmpParam_At(530)
	local LSmFatalDamage_jc = GetTmpParam_At(548)

	local newv = LSmFatalDamage_adj+LSmFatalDamage_jc
	SetTmpParam_At(512, newv)
end
CLuaPlayerPropertyFight.RefreshParamLSAntiBlock = function()
	local LSAntiBlock_adj = GetTmpParam_At(531)
	local LSAntiBlock_jc = GetTmpParam_At(549)

	local newv = LSAntiBlock_adj+LSAntiBlock_jc
	SetTmpParam_At(513, newv)
end
CLuaPlayerPropertyFight.RefreshParamLSAntiBlockDamage = function()
	local LSAntiBlockDamage_adj = GetTmpParam_At(532)
	local LSAntiBlockDamage_jc = GetTmpParam_At(550)

	local newv = LSAntiBlockDamage_adj+LSAntiBlockDamage_jc
	SetTmpParam_At(514, newv)
end
CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiFire = function()
	local LSIgnoreAntiFire_adj = GetTmpParam_At(533)
	local LSIgnoreAntiFire_jc = GetTmpParam_At(551)
	local LSIgnoreAntiFire_mul = GetTmpParam_At(559)

	local newv = (LSIgnoreAntiFire_adj+LSIgnoreAntiFire_jc)*(1+LSIgnoreAntiFire_mul)
	SetTmpParam_At(515, newv)
end
CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiThunder = function()
	local LSIgnoreAntiThunder_adj = GetTmpParam_At(534)
	local LSIgnoreAntiThunder_jc = GetTmpParam_At(552)
	local LSIgnoreAntiThunder_mul = GetTmpParam_At(560)

	local newv = (LSIgnoreAntiThunder_adj+LSIgnoreAntiThunder_jc)*(1+LSIgnoreAntiThunder_mul)
	SetTmpParam_At(516, newv)
end
CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiIce = function()
	local LSIgnoreAntiIce_adj = GetTmpParam_At(535)
	local LSIgnoreAntiIce_jc = GetTmpParam_At(553)
	local LSIgnoreAntiIce_mul = GetTmpParam_At(561)

	local newv = (LSIgnoreAntiIce_adj+LSIgnoreAntiIce_jc)*(1+LSIgnoreAntiIce_mul)
	SetTmpParam_At(517, newv)
end
CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiPoison = function()
	local LSIgnoreAntiPoison_adj = GetTmpParam_At(536)
	local LSIgnoreAntiPoison_jc = GetTmpParam_At(554)
	local LSIgnoreAntiPoison_mul = GetTmpParam_At(562)

	local newv = (LSIgnoreAntiPoison_adj+LSIgnoreAntiPoison_jc)*(1+LSIgnoreAntiPoison_mul)
	SetTmpParam_At(518, newv)
end
CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiWind = function()
	local LSIgnoreAntiWind_adj = GetTmpParam_At(537)
	local LSIgnoreAntiWind_jc = GetTmpParam_At(555)
	local LSIgnoreAntiWind_mul = GetTmpParam_At(563)

	local newv = (LSIgnoreAntiWind_adj+LSIgnoreAntiWind_jc)*(1+LSIgnoreAntiWind_mul)
	SetTmpParam_At(519, newv)
end
CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiLight = function()
	local LSIgnoreAntiLight_adj = GetTmpParam_At(538)
	local LSIgnoreAntiLight_jc = GetTmpParam_At(556)
	local LSIgnoreAntiLight_mul = GetTmpParam_At(564)

	local newv = (LSIgnoreAntiLight_adj+LSIgnoreAntiLight_jc)*(1+LSIgnoreAntiLight_mul)
	SetTmpParam_At(520, newv)
end
CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiIllusion = function()
	local LSIgnoreAntiIllusion_adj = GetTmpParam_At(539)
	local LSIgnoreAntiIllusion_jc = GetTmpParam_At(557)
	local LSIgnoreAntiIllusion_mul = GetTmpParam_At(565)

	local newv = (LSIgnoreAntiIllusion_adj+LSIgnoreAntiIllusion_jc)*(1+LSIgnoreAntiIllusion_mul)
	SetTmpParam_At(521, newv)
end
CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiWater = function()
	local LSIgnoreAntiWater_adj = GetTmpParam_At(540)
	local LSIgnoreAntiWater_jc = GetTmpParam_At(558)
	local LSIgnoreAntiWater_mul = GetTmpParam_At(566)

	local newv = (LSIgnoreAntiWater_adj+LSIgnoreAntiWater_jc)*(1+LSIgnoreAntiWater_mul)
	SetTmpParam_At(522, newv)
end
CLuaPlayerPropertyFight.SetParamLSpAttMin_adj = function(newv)
	SetTmpParam_At(523, newv)

	CLuaPlayerPropertyFight.RefreshParamLSpAttMin()
end
CLuaPlayerPropertyFight.SetParamLSpAttMax_adj = function(newv)
	SetTmpParam_At(524, newv)

	CLuaPlayerPropertyFight.RefreshParamLSpAttMax()
end
CLuaPlayerPropertyFight.SetParamLSmAttMin_adj = function(newv)
	SetTmpParam_At(525, newv)

	CLuaPlayerPropertyFight.RefreshParamLSmAttMin()
end
CLuaPlayerPropertyFight.SetParamLSmAttMax_adj = function(newv)
	SetTmpParam_At(526, newv)

	CLuaPlayerPropertyFight.RefreshParamLSmAttMax()
end
CLuaPlayerPropertyFight.SetParamLSpFatal_adj = function(newv)
	SetTmpParam_At(527, newv)

	CLuaPlayerPropertyFight.RefreshParamLSpFatal()
end
CLuaPlayerPropertyFight.SetParamLSpFatalDamage_adj = function(newv)
	SetTmpParam_At(528, newv)

	CLuaPlayerPropertyFight.RefreshParamLSpFatalDamage()
end
CLuaPlayerPropertyFight.SetParamLSmFatal_adj = function(newv)
	SetTmpParam_At(529, newv)

	CLuaPlayerPropertyFight.RefreshParamLSmFatal()
end
CLuaPlayerPropertyFight.SetParamLSmFatalDamage_adj = function(newv)
	SetTmpParam_At(530, newv)

	CLuaPlayerPropertyFight.RefreshParamLSmFatalDamage()
end
CLuaPlayerPropertyFight.SetParamLSAntiBlock_adj = function(newv)
	SetTmpParam_At(531, newv)

	CLuaPlayerPropertyFight.RefreshParamLSAntiBlock()
end
CLuaPlayerPropertyFight.SetParamLSAntiBlockDamage_adj = function(newv)
	SetTmpParam_At(532, newv)

	CLuaPlayerPropertyFight.RefreshParamLSAntiBlockDamage()
end
CLuaPlayerPropertyFight.SetParamLSIgnoreAntiFire_adj = function(newv)
	SetTmpParam_At(533, newv)

	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiFire()
end
CLuaPlayerPropertyFight.SetParamLSIgnoreAntiThunder_adj = function(newv)
	SetTmpParam_At(534, newv)

	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiThunder()
end
CLuaPlayerPropertyFight.SetParamLSIgnoreAntiIce_adj = function(newv)
	SetTmpParam_At(535, newv)

	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiIce()
end
CLuaPlayerPropertyFight.SetParamLSIgnoreAntiPoison_adj = function(newv)
	SetTmpParam_At(536, newv)

	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiPoison()
end
CLuaPlayerPropertyFight.SetParamLSIgnoreAntiWind_adj = function(newv)
	SetTmpParam_At(537, newv)

	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiWind()
end
CLuaPlayerPropertyFight.SetParamLSIgnoreAntiLight_adj = function(newv)
	SetTmpParam_At(538, newv)

	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiLight()
end
CLuaPlayerPropertyFight.SetParamLSIgnoreAntiIllusion_adj = function(newv)
	SetTmpParam_At(539, newv)

	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiIllusion()
end
CLuaPlayerPropertyFight.SetParamLSIgnoreAntiWater_adj = function(newv)
	SetTmpParam_At(540, newv)

	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiWater()
end
CLuaPlayerPropertyFight.SetParamLSpAttMin_jc = function(newv)
	SetTmpParam_At(541, newv)

	CLuaPlayerPropertyFight.RefreshParamLSpAttMin()
end
CLuaPlayerPropertyFight.SetParamLSpAttMax_jc = function(newv)
	SetTmpParam_At(542, newv)

	CLuaPlayerPropertyFight.RefreshParamLSpAttMax()
end
CLuaPlayerPropertyFight.SetParamLSmAttMin_jc = function(newv)
	SetTmpParam_At(543, newv)

	CLuaPlayerPropertyFight.RefreshParamLSmAttMin()
end
CLuaPlayerPropertyFight.SetParamLSmAttMax_jc = function(newv)
	SetTmpParam_At(544, newv)

	CLuaPlayerPropertyFight.RefreshParamLSmAttMax()
end
CLuaPlayerPropertyFight.SetParamLSpFatal_jc = function(newv)
	SetTmpParam_At(545, newv)

	CLuaPlayerPropertyFight.RefreshParamLSpFatal()
end
CLuaPlayerPropertyFight.SetParamLSpFatalDamage_jc = function(newv)
	SetTmpParam_At(546, newv)

	CLuaPlayerPropertyFight.RefreshParamLSpFatalDamage()
end
CLuaPlayerPropertyFight.SetParamLSmFatal_jc = function(newv)
	SetTmpParam_At(547, newv)

	CLuaPlayerPropertyFight.RefreshParamLSmFatal()
end
CLuaPlayerPropertyFight.SetParamLSmFatalDamage_jc = function(newv)
	SetTmpParam_At(548, newv)

	CLuaPlayerPropertyFight.RefreshParamLSmFatalDamage()
end
CLuaPlayerPropertyFight.SetParamLSAntiBlock_jc = function(newv)
	SetTmpParam_At(549, newv)

	CLuaPlayerPropertyFight.RefreshParamLSAntiBlock()
end
CLuaPlayerPropertyFight.SetParamLSAntiBlockDamage_jc = function(newv)
	SetTmpParam_At(550, newv)

	CLuaPlayerPropertyFight.RefreshParamLSAntiBlockDamage()
end
CLuaPlayerPropertyFight.SetParamLSIgnoreAntiFire_jc = function(newv)
	SetTmpParam_At(551, newv)

	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiFire()
end
CLuaPlayerPropertyFight.SetParamLSIgnoreAntiThunder_jc = function(newv)
	SetTmpParam_At(552, newv)

	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiThunder()
end
CLuaPlayerPropertyFight.SetParamLSIgnoreAntiIce_jc = function(newv)
	SetTmpParam_At(553, newv)

	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiIce()
end
CLuaPlayerPropertyFight.SetParamLSIgnoreAntiPoison_jc = function(newv)
	SetTmpParam_At(554, newv)

	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiPoison()
end
CLuaPlayerPropertyFight.SetParamLSIgnoreAntiWind_jc = function(newv)
	SetTmpParam_At(555, newv)

	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiWind()
end
CLuaPlayerPropertyFight.SetParamLSIgnoreAntiLight_jc = function(newv)
	SetTmpParam_At(556, newv)

	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiLight()
end
CLuaPlayerPropertyFight.SetParamLSIgnoreAntiIllusion_jc = function(newv)
	SetTmpParam_At(557, newv)

	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiIllusion()
end
CLuaPlayerPropertyFight.SetParamLSIgnoreAntiWater_jc = function(newv)
	SetTmpParam_At(558, newv)

	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiWater()
end
CLuaPlayerPropertyFight.SetParamLSIgnoreAntiFire_mul = function(newv)
	SetTmpParam_At(559, newv)

	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiFire()
end
CLuaPlayerPropertyFight.SetParamLSIgnoreAntiThunder_mul = function(newv)
	SetTmpParam_At(560, newv)

	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiThunder()
end
CLuaPlayerPropertyFight.SetParamLSIgnoreAntiIce_mul = function(newv)
	SetTmpParam_At(561, newv)

	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiIce()
end
CLuaPlayerPropertyFight.SetParamLSIgnoreAntiPoison_mul = function(newv)
	SetTmpParam_At(562, newv)

	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiPoison()
end
CLuaPlayerPropertyFight.SetParamLSIgnoreAntiWind_mul = function(newv)
	SetTmpParam_At(563, newv)

	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiWind()
end
CLuaPlayerPropertyFight.SetParamLSIgnoreAntiLight_mul = function(newv)
	SetTmpParam_At(564, newv)

	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiLight()
end
CLuaPlayerPropertyFight.SetParamLSIgnoreAntiIllusion_mul = function(newv)
	SetTmpParam_At(565, newv)

	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiIllusion()
end
CLuaPlayerPropertyFight.SetParamLSIgnoreAntiWater_mul = function(newv)
	SetTmpParam_At(566, newv)

	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiWater()
end
CLuaPlayerPropertyFight.SetParamJieBan_Child_QiChangColor = function(newv)
	SetTmpParam_At(567, newv)
end
CLuaPlayerPropertyFight.SetParamJieBan_LingShou_Ratio = function(newv)
	SetTmpParam_At(568, newv)
end
CLuaPlayerPropertyFight.SetParamJieBan_LingShou_QinMi = function(newv)
	SetTmpParam_At(569, newv)
end
CLuaPlayerPropertyFight.SetParamJieBan_LingShou_CorZizhi = function(newv)
	SetTmpParam_At(570, newv)
end
CLuaPlayerPropertyFight.SetParamJieBan_LingShou_StaZizhi = function(newv)
	SetTmpParam_At(571, newv)
end
CLuaPlayerPropertyFight.SetParamJieBan_LingShou_StrZizhi = function(newv)
	SetTmpParam_At(572, newv)
end
CLuaPlayerPropertyFight.SetParamJieBan_LingShou_IntZizhi = function(newv)
	SetTmpParam_At(573, newv)
end
CLuaPlayerPropertyFight.SetParamJieBan_LingShou_AgiZizhi = function(newv)
	SetTmpParam_At(574, newv)
end
CLuaPlayerPropertyFight.SetParamBuff_DisplayValue1 = function(newv)
	SetTmpParam_At(575, newv)
end
CLuaPlayerPropertyFight.SetParamBuff_DisplayValue2 = function(newv)
	SetTmpParam_At(576, newv)
end
CLuaPlayerPropertyFight.SetParamBuff_DisplayValue3 = function(newv)
	SetTmpParam_At(577, newv)
end
CLuaPlayerPropertyFight.SetParamBuff_DisplayValue4 = function(newv)
	SetTmpParam_At(578, newv)
end
CLuaPlayerPropertyFight.SetParamBuff_DisplayValue5 = function(newv)
	SetTmpParam_At(579, newv)
end
CLuaPlayerPropertyFight.SetParamLSNature = function(newv)
	SetTmpParam_At(580, newv)
end
CLuaPlayerPropertyFight.SetParamPrayMulti = function(newv)
	SetTmpParam_At(581, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceYingLing = function(newv)
	SetTmpParam_At(582, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceYingLingMul = function(newv)
	SetTmpParam_At(583, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceFlag = function(newv)
	SetTmpParam_At(584, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceFlagMul = function(newv)
	SetTmpParam_At(585, newv)
end
CLuaPlayerPropertyFight.SetParamObjectType = function(newv)
	SetTmpParam_At(586, newv)
end
CLuaPlayerPropertyFight.SetParamMonsterType = function(newv)
	SetTmpParam_At(587, newv)
end
CLuaPlayerPropertyFight.SetParamFightPropType = function(newv)
	SetTmpParam_At(588, newv)
end
CLuaPlayerPropertyFight.SetParamPlayHpFull = function(newv)
	SetTmpParam_At(589, newv)
end
CLuaPlayerPropertyFight.SetParamPlayHp = function(newv)
	SetTmpParam_At(590, newv)
end
CLuaPlayerPropertyFight.SetParamPlayAtt = function(newv)
	SetTmpParam_At(591, newv)
end
CLuaPlayerPropertyFight.SetParamPlayDef = function(newv)
	SetTmpParam_At(592, newv)
end
CLuaPlayerPropertyFight.SetParamPlayHit = function(newv)
	SetTmpParam_At(593, newv)
end
CLuaPlayerPropertyFight.SetParamPlayMiss = function(newv)
	SetTmpParam_At(594, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceDieKe = function(newv)
	SetTmpParam_At(595, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceDieKeMul = function(newv)
	SetTmpParam_At(596, newv)
end
CLuaPlayerPropertyFight.SetParamDotRemain = function(newv)
	SetTmpParam_At(597, newv)
end
CLuaPlayerPropertyFight.SetParamIsConfused = function(newv)
	SetTmpParam_At(598, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceSheShouKefu = function(newv)
	SetTmpParam_At(599, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceJiaShiKefu = function(newv)
	SetTmpParam_At(600, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceDaoKeKefu = function(newv)
	SetTmpParam_At(601, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceXiaKeKefu = function(newv)
	SetTmpParam_At(602, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceFangShiKefu = function(newv)
	SetTmpParam_At(603, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceYiShiKefu = function(newv)
	SetTmpParam_At(604, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceMeiZheKefu = function(newv)
	SetTmpParam_At(605, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceYiRenKefu = function(newv)
	SetTmpParam_At(606, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceYanShiKefu = function(newv)
	SetTmpParam_At(607, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceHuaHunKefu = function(newv)
	SetTmpParam_At(608, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceYingLingKefu = function(newv)
	SetTmpParam_At(609, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceDieKeKefu = function(newv)
	SetTmpParam_At(610, newv)
end
CLuaPlayerPropertyFight.RefreshParamAdjustPermanentCor = function()
	local PermanentCor = GetParam_At(1)
	local AdjustPermanentMidCor = GetTmpParam_At(617)
	local RevisePermanentCor = GetParam_At(12)
	local AdjustPermanentMidAgi = GetTmpParam_At(621)
	local AdjustPermanentMidInt = GetTmpParam_At(620)
	local AdjustPermanentMidStr = GetTmpParam_At(619)
	local AdjustPermanentMidSta = GetTmpParam_At(618)
	local Grade = GetTmpParam_At(2)

	local newv = PermanentCor + AdjustPermanentMidCor + min(1,RevisePermanentCor-AdjustPermanentMidCor)*min(1,(min(35,floor(max(0,Grade-50)/20)*5+floor(max(0,Grade-100)/20)*5)-AdjustPermanentMidCor-AdjustPermanentMidSta-AdjustPermanentMidStr-AdjustPermanentMidInt-AdjustPermanentMidAgi))
	SetTmpParam_At(611, newv)
end
CLuaPlayerPropertyFight.RefreshParamAdjustPermanentSta = function()
	local PermanentSta = GetParam_At(2)
	local AdjustPermanentMidSta = GetTmpParam_At(618)
	local RevisePermanentSta = GetParam_At(13)
	local AdjustPermanentMidAgi = GetTmpParam_At(621)
	local AdjustPermanentMidInt = GetTmpParam_At(620)
	local AdjustPermanentMidStr = GetTmpParam_At(619)
	local RevisePermanentCor = GetParam_At(12)
	local AdjustPermanentMidCor = GetTmpParam_At(617)
	local Grade = GetTmpParam_At(2)

	local newv = PermanentSta + AdjustPermanentMidSta + min(1,RevisePermanentSta-AdjustPermanentMidSta)*min(1,max(0,min(35,floor(max(0,Grade-50)/20)*5+floor(max(0,Grade-100)/20)*5)-AdjustPermanentMidCor-AdjustPermanentMidSta-AdjustPermanentMidStr-AdjustPermanentMidInt-AdjustPermanentMidAgi-min(1,RevisePermanentCor-AdjustPermanentMidCor)))
	SetTmpParam_At(612, newv)
end
CLuaPlayerPropertyFight.RefreshParamAdjustPermanentStr = function()
	local PermanentStr = GetParam_At(3)
	local AdjustPermanentMidStr = GetTmpParam_At(619)
	local RevisePermanentStr = GetParam_At(14)
	local AdjustPermanentMidAgi = GetTmpParam_At(621)
	local RevisePermanentAgi = GetParam_At(16)
	local AdjustPermanentMidInt = GetTmpParam_At(620)
	local RevisePermanentSta = GetParam_At(13)
	local AdjustPermanentMidSta = GetTmpParam_At(618)
	local RevisePermanentCor = GetParam_At(12)
	local AdjustPermanentMidCor = GetTmpParam_At(617)
	local Grade = GetTmpParam_At(2)

	local newv = PermanentStr + AdjustPermanentMidStr + min(1,RevisePermanentStr-AdjustPermanentMidStr)*min(1,max(0,min(35,floor(max(0,Grade-50)/20)*5+floor(max(0,Grade-100)/20)*5)-AdjustPermanentMidCor-AdjustPermanentMidSta-AdjustPermanentMidStr-AdjustPermanentMidInt-AdjustPermanentMidAgi-min(1,RevisePermanentCor-AdjustPermanentMidCor)-min(1,RevisePermanentSta-AdjustPermanentMidSta)-min(1,RevisePermanentAgi-AdjustPermanentMidAgi)))
	SetTmpParam_At(613, newv)
end
CLuaPlayerPropertyFight.RefreshParamAdjustPermanentInt = function()
	local PermanentInt = GetParam_At(4)
	local AdjustPermanentMidInt = GetTmpParam_At(620)
	local RevisePermanentInt = GetParam_At(15)
	local RevisePermanentStr = GetParam_At(14)
	local AdjustPermanentMidStr = GetTmpParam_At(619)
	local AdjustPermanentMidAgi = GetTmpParam_At(621)
	local RevisePermanentAgi = GetParam_At(16)
	local RevisePermanentSta = GetParam_At(13)
	local AdjustPermanentMidSta = GetTmpParam_At(618)
	local RevisePermanentCor = GetParam_At(12)
	local AdjustPermanentMidCor = GetTmpParam_At(617)
	local Grade = GetTmpParam_At(2)

	local newv = PermanentInt + AdjustPermanentMidInt + min(1,RevisePermanentInt-AdjustPermanentMidInt)*min(1,max(0,min(35,floor(max(0,Grade-50)/20)*5+floor(max(0,Grade-100)/20)*5)-AdjustPermanentMidCor-AdjustPermanentMidSta-AdjustPermanentMidStr-AdjustPermanentMidInt-AdjustPermanentMidAgi-min(1,RevisePermanentCor-AdjustPermanentMidCor)-min(1,RevisePermanentSta-AdjustPermanentMidSta)-min(1,RevisePermanentAgi-AdjustPermanentMidAgi)-min(1,RevisePermanentStr-AdjustPermanentMidStr)))
	SetTmpParam_At(614, newv)
end
CLuaPlayerPropertyFight.RefreshParamAdjustPermanentAgi = function()
	local PermanentAgi = GetParam_At(5)
	local AdjustPermanentMidAgi = GetTmpParam_At(621)
	local RevisePermanentAgi = GetParam_At(16)
	local AdjustPermanentMidInt = GetTmpParam_At(620)
	local RevisePermanentSta = GetParam_At(13)
	local AdjustPermanentMidSta = GetTmpParam_At(618)
	local AdjustPermanentMidStr = GetTmpParam_At(619)
	local RevisePermanentCor = GetParam_At(12)
	local AdjustPermanentMidCor = GetTmpParam_At(617)
	local Grade = GetTmpParam_At(2)

	local newv = PermanentAgi + AdjustPermanentMidAgi + min(1,RevisePermanentAgi-AdjustPermanentMidAgi)*min(1,max(0,min(35,floor(max(0,Grade-50)/20)*5+floor(max(0,Grade-100)/20)*5)-AdjustPermanentMidCor-AdjustPermanentMidSta-AdjustPermanentMidStr-AdjustPermanentMidInt-AdjustPermanentMidAgi-min(1,RevisePermanentCor-AdjustPermanentMidCor)-min(1,RevisePermanentSta-AdjustPermanentMidSta)))
	SetTmpParam_At(615, newv)
end
CLuaPlayerPropertyFight.SetParamSoulCoreLevel = function(newv)
	SetTmpParam_At(616, newv)
end
CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidCor = function()
	local RevisePermanentCor = GetParam_At(12)
	local SumPermanent = GetTmpParam_At(622)
	local Grade = GetTmpParam_At(2)

	local newv = floor(RevisePermanentCor*min(1,min(35,floor(max(0,Grade-50)/20)*5+floor(max(0,Grade-100)/20)*5)/max(SumPermanent,1)))
	SetTmpParam_At(617, newv)
end
CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidSta = function()
	local RevisePermanentSta = GetParam_At(13)
	local SumPermanent = GetTmpParam_At(622)
	local Grade = GetTmpParam_At(2)

	local newv = floor(RevisePermanentSta*min(1,min(35,floor(max(0,Grade-50)/20)*5+floor(max(0,Grade-100)/20)*5)/max(SumPermanent,1)))
	SetTmpParam_At(618, newv)
end
CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidStr = function()
	local RevisePermanentStr = GetParam_At(14)
	local SumPermanent = GetTmpParam_At(622)
	local Grade = GetTmpParam_At(2)

	local newv = floor(RevisePermanentStr*min(1,min(35,floor(max(0,Grade-50)/20)*5+floor(max(0,Grade-100)/20)*5)/max(SumPermanent,1)))
	SetTmpParam_At(619, newv)
end
CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidInt = function()
	local RevisePermanentInt = GetParam_At(15)
	local SumPermanent = GetTmpParam_At(622)
	local Grade = GetTmpParam_At(2)

	local newv = floor(RevisePermanentInt*min(1,min(35,floor(max(0,Grade-50)/20)*5+floor(max(0,Grade-100)/20)*5)/max(SumPermanent,1)))
	SetTmpParam_At(620, newv)
end
CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidAgi = function()
	local RevisePermanentAgi = GetParam_At(16)
	local SumPermanent = GetTmpParam_At(622)
	local Grade = GetTmpParam_At(2)

	local newv = floor(RevisePermanentAgi*min(1,min(35,floor(max(0,Grade-50)/20)*5+floor(max(0,Grade-100)/20)*5)/max(SumPermanent,1)))
	SetTmpParam_At(621, newv)
end
CLuaPlayerPropertyFight.RefreshParamSumPermanent = function()
	local RevisePermanentAgi = GetParam_At(16)
	local RevisePermanentInt = GetParam_At(15)
	local RevisePermanentStr = GetParam_At(14)
	local RevisePermanentCor = GetParam_At(12)
	local RevisePermanentSta = GetParam_At(13)

	local newv = RevisePermanentCor+RevisePermanentSta+RevisePermanentStr+RevisePermanentInt+RevisePermanentAgi
	SetTmpParam_At(622, newv)
end
CLuaPlayerPropertyFight.SetParamAntiTian = function(newv)
	SetTmpParam_At(623, newv)
end
CLuaPlayerPropertyFight.SetParamAntiEGui = function(newv)
	SetTmpParam_At(624, newv)
end
CLuaPlayerPropertyFight.SetParamAntiXiuLuo = function(newv)
	SetTmpParam_At(625, newv)
end
CLuaPlayerPropertyFight.SetParamAntiDiYu = function(newv)
	SetTmpParam_At(626, newv)
end
CLuaPlayerPropertyFight.SetParamAntiRen = function(newv)
	SetTmpParam_At(627, newv)
end
CLuaPlayerPropertyFight.SetParamAntiChuSheng = function(newv)
	SetTmpParam_At(628, newv)
end
CLuaPlayerPropertyFight.SetParamAntiBuilding = function(newv)
	SetTmpParam_At(629, newv)
end
CLuaPlayerPropertyFight.SetParamAntiZhaoHuan = function(newv)
	SetTmpParam_At(630, newv)
end
CLuaPlayerPropertyFight.SetParamAntiLingShou = function(newv)
	SetTmpParam_At(631, newv)
end
CLuaPlayerPropertyFight.SetParamAntiBoss = function(newv)
	SetTmpParam_At(632, newv)
end
CLuaPlayerPropertyFight.SetParamAntiSheShou = function(newv)
	SetTmpParam_At(633, newv)
end
CLuaPlayerPropertyFight.SetParamAntiJiaShi = function(newv)
	SetTmpParam_At(634, newv)
end
CLuaPlayerPropertyFight.SetParamAntiDaoKe = function(newv)
	SetTmpParam_At(635, newv)
end
CLuaPlayerPropertyFight.SetParamAntiXiaKe = function(newv)
	SetTmpParam_At(636, newv)
end
CLuaPlayerPropertyFight.SetParamAntiFangShi = function(newv)
	SetTmpParam_At(637, newv)
end
CLuaPlayerPropertyFight.SetParamAntiYiShi = function(newv)
	SetTmpParam_At(638, newv)
end
CLuaPlayerPropertyFight.SetParamAntiMeiZhe = function(newv)
	SetTmpParam_At(639, newv)
end
CLuaPlayerPropertyFight.SetParamAntiYiRen = function(newv)
	SetTmpParam_At(640, newv)
end
CLuaPlayerPropertyFight.SetParamAntiYanShi = function(newv)
	SetTmpParam_At(641, newv)
end
CLuaPlayerPropertyFight.SetParamAntiHuaHun = function(newv)
	SetTmpParam_At(642, newv)
end
CLuaPlayerPropertyFight.SetParamAntiYingLing = function(newv)
	SetTmpParam_At(643, newv)
end
CLuaPlayerPropertyFight.SetParamAntiDieKe = function(newv)
	SetTmpParam_At(644, newv)
end
CLuaPlayerPropertyFight.SetParamAntiFlag = function(newv)
	SetTmpParam_At(645, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceZhanKuang = function(newv)
	SetTmpParam_At(646, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceZhanKuangMul = function(newv)
	SetTmpParam_At(647, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceZhanKuangKefu = function(newv)
	SetTmpParam_At(648, newv)
end
CLuaPlayerPropertyFight.SetParamAntiZhanKuang = function(newv)
	SetTmpParam_At(649, newv)
end
CLuaPlayerPropertyFight.RefreshParamJumpSpeed = function()
	local OriJumpSpeed = GetTmpParam_At(651)
	local AdjJumpSpeed = GetTmpParam_At(652)

	local newv = OriJumpSpeed+AdjJumpSpeed
	SetTmpParam_At(650, newv)
end
CLuaPlayerPropertyFight.SetParamOriJumpSpeed = function(newv)
	SetTmpParam_At(651, newv)

	CLuaPlayerPropertyFight.RefreshParamJumpSpeed()
end
CLuaPlayerPropertyFight.SetParamAdjJumpSpeed = function(newv)
	SetTmpParam_At(652, newv)

	CLuaPlayerPropertyFight.RefreshParamJumpSpeed()
end
CLuaPlayerPropertyFight.RefreshParamGravityAcceleration = function()
	local OriGravityAcceleration = GetTmpParam_At(654)
	local AdjGravityAcceleration = GetTmpParam_At(655)

	local newv = OriGravityAcceleration+AdjGravityAcceleration
	SetTmpParam_At(653, newv)
end
CLuaPlayerPropertyFight.SetParamOriGravityAcceleration = function(newv)
	SetTmpParam_At(654, newv)

	CLuaPlayerPropertyFight.RefreshParamGravityAcceleration()
end
CLuaPlayerPropertyFight.SetParamAdjGravityAcceleration = function(newv)
	SetTmpParam_At(655, newv)

	CLuaPlayerPropertyFight.RefreshParamGravityAcceleration()
end
CLuaPlayerPropertyFight.RefreshParamHorizontalAcceleration = function()
	local OriHorizontalAcceleration = GetTmpParam_At(657)
	local AdjHorizontalAcceleration = GetTmpParam_At(658)

	local newv = OriHorizontalAcceleration+AdjHorizontalAcceleration
	SetTmpParam_At(656, newv)
end
CLuaPlayerPropertyFight.SetParamOriHorizontalAcceleration = function(newv)
	SetTmpParam_At(657, newv)

	CLuaPlayerPropertyFight.RefreshParamHorizontalAcceleration()
end
CLuaPlayerPropertyFight.SetParamAdjHorizontalAcceleration = function(newv)
	SetTmpParam_At(658, newv)

	CLuaPlayerPropertyFight.RefreshParamHorizontalAcceleration()
end
CLuaPlayerPropertyFight.RefreshParamSwimSpeed = function()
	local OriSwimSpeed = GetTmpParam_At(660)
	local AdjSwimSpeed = GetTmpParam_At(661)

	local newv = OriSwimSpeed+AdjSwimSpeed
	SetTmpParam_At(659, newv)
end
CLuaPlayerPropertyFight.SetParamOriSwimSpeed = function(newv)
	SetTmpParam_At(660, newv)

	CLuaPlayerPropertyFight.RefreshParamSwimSpeed()
end
CLuaPlayerPropertyFight.SetParamAdjSwimSpeed = function(newv)
	SetTmpParam_At(661, newv)

	CLuaPlayerPropertyFight.RefreshParamSwimSpeed()
end
CLuaPlayerPropertyFight.SetParamStrengthPoint = function(newv)
	SetTmpParam_At(662, newv)
end
CLuaPlayerPropertyFight.RefreshParamStrengthPointMax = function()
	local OriStrengthPointMax = GetTmpParam_At(664)
	local AdjStrengthPointMax = GetTmpParam_At(665)

	local newv = OriStrengthPointMax+AdjStrengthPointMax
	SetTmpParam_At(663, newv)
end
CLuaPlayerPropertyFight.SetParamOriStrengthPointMax = function(newv)
	SetTmpParam_At(664, newv)

	CLuaPlayerPropertyFight.RefreshParamStrengthPointMax()
end
CLuaPlayerPropertyFight.SetParamAdjStrengthPointMax = function(newv)
	SetTmpParam_At(665, newv)

	CLuaPlayerPropertyFight.RefreshParamStrengthPointMax()
end
CLuaPlayerPropertyFight.SetParamAntiMulEnhanceIllusion = function(newv)
	SetTmpParam_At(666, newv)
end
CLuaPlayerPropertyFight.SetParamGlideHorizontalSpeedWithUmbrella = function(newv)
	SetTmpParam_At(667, newv)
end
CLuaPlayerPropertyFight.SetParamGlideVerticalSpeedWithUmbrella = function(newv)
	SetTmpParam_At(668, newv)
end
CLuaPlayerPropertyFight.SetParamAdjpSpeed = function(newv)
	SetTmpParam_At(1001, newv)

	CLuaPlayerPropertyFight.RefreshParampSpeed()
end
CLuaPlayerPropertyFight.SetParamAdjmSpeed = function(newv)
	SetTmpParam_At(1002, newv)

	CLuaPlayerPropertyFight.RefreshParammSpeed()
end
CLuaPlayerPropertyFight.SetParamPermanentCor = function(newv)
	SetParam_At(1, newv)

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentCor()

	CLuaPlayerPropertyFight.RefreshParamCharacterCor()

	CLuaPlayerPropertyFight.RefreshParamCor()

	CLuaPlayerPropertyFight.RefreshParamHpFull()
end
CLuaPlayerPropertyFight.SetParamPermanentSta = function(newv)
	SetParam_At(2, newv)

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentSta()

	CLuaPlayerPropertyFight.RefreshParamSta()

	CLuaPlayerPropertyFight.RefreshParamCharacterSta()

	CLuaPlayerPropertyFight.RefreshParamMpFull()
end
CLuaPlayerPropertyFight.SetParamPermanentStr = function(newv)
	SetParam_At(3, newv)

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentStr()

	CLuaPlayerPropertyFight.RefreshParamStr()

	CLuaPlayerPropertyFight.RefreshParamCharacterStr()

	CLuaPlayerPropertyFight.RefreshParampAttMax()

	CLuaPlayerPropertyFight.RefreshParampDef()

	CLuaPlayerPropertyFight.RefreshParampAttMin()
end
CLuaPlayerPropertyFight.SetParamPermanentInt = function(newv)
	SetParam_At(4, newv)

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentInt()

	CLuaPlayerPropertyFight.RefreshParamInt()

	CLuaPlayerPropertyFight.RefreshParamCharacterInt()

	CLuaPlayerPropertyFight.RefreshParammAttMin()

	CLuaPlayerPropertyFight.RefreshParammAttMax()

	CLuaPlayerPropertyFight.RefreshParammDef()
end
CLuaPlayerPropertyFight.SetParamPermanentAgi = function(newv)
	SetParam_At(5, newv)

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentAgi()

	CLuaPlayerPropertyFight.RefreshParamAgi()

	CLuaPlayerPropertyFight.RefreshParamCharacterAgi()

	CLuaPlayerPropertyFight.RefreshParammMiss()

	CLuaPlayerPropertyFight.RefreshParammHit()

	CLuaPlayerPropertyFight.RefreshParampMiss()

	CLuaPlayerPropertyFight.RefreshParampHit()
end
CLuaPlayerPropertyFight.SetParamPermanentHpFull = function(newv)
	SetParam_At(6, newv)
end
CLuaPlayerPropertyFight.SetParamHp = function(newv)
	SetParam_At(7, newv)
end
CLuaPlayerPropertyFight.SetParamMp = function(newv)
	SetParam_At(8, newv)
end
CLuaPlayerPropertyFight.SetParamCurrentHpFull = function(newv)
	SetParam_At(9, newv)
end
CLuaPlayerPropertyFight.SetParamCurrentMpFull = function(newv)
	SetParam_At(10, newv)
end
CLuaPlayerPropertyFight.SetParamEquipExchangeMF = function(newv)
	SetParam_At(11, newv)

	CLuaPlayerPropertyFight.RefreshParamMF()
end
CLuaPlayerPropertyFight.SetParamRevisePermanentCor = function(newv)
	SetParam_At(12, newv)

	CLuaPlayerPropertyFight.RefreshParamSumPermanent()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidSta()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidInt()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidCor()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidAgi()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidStr()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentCor()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentInt()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentSta()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentStr()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentAgi()

	CLuaPlayerPropertyFight.RefreshParamCharacterCor()

	CLuaPlayerPropertyFight.RefreshParamCor()

	CLuaPlayerPropertyFight.RefreshParamInt()

	CLuaPlayerPropertyFight.RefreshParamCharacterInt()

	CLuaPlayerPropertyFight.RefreshParamSta()

	CLuaPlayerPropertyFight.RefreshParamCharacterSta()

	CLuaPlayerPropertyFight.RefreshParamStr()

	CLuaPlayerPropertyFight.RefreshParamCharacterStr()

	CLuaPlayerPropertyFight.RefreshParamAgi()

	CLuaPlayerPropertyFight.RefreshParamCharacterAgi()

	CLuaPlayerPropertyFight.RefreshParamHpFull()

	CLuaPlayerPropertyFight.RefreshParammAttMin()

	CLuaPlayerPropertyFight.RefreshParammAttMax()

	CLuaPlayerPropertyFight.RefreshParammDef()

	CLuaPlayerPropertyFight.RefreshParamMpFull()

	CLuaPlayerPropertyFight.RefreshParampAttMax()

	CLuaPlayerPropertyFight.RefreshParampDef()

	CLuaPlayerPropertyFight.RefreshParampAttMin()

	CLuaPlayerPropertyFight.RefreshParammMiss()

	CLuaPlayerPropertyFight.RefreshParammHit()

	CLuaPlayerPropertyFight.RefreshParampMiss()

	CLuaPlayerPropertyFight.RefreshParampHit()
end
CLuaPlayerPropertyFight.SetParamRevisePermanentSta = function(newv)
	SetParam_At(13, newv)

	CLuaPlayerPropertyFight.RefreshParamSumPermanent()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidSta()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidInt()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidCor()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidAgi()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidStr()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentCor()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentInt()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentSta()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentStr()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentAgi()

	CLuaPlayerPropertyFight.RefreshParamCharacterCor()

	CLuaPlayerPropertyFight.RefreshParamCor()

	CLuaPlayerPropertyFight.RefreshParamInt()

	CLuaPlayerPropertyFight.RefreshParamCharacterInt()

	CLuaPlayerPropertyFight.RefreshParamSta()

	CLuaPlayerPropertyFight.RefreshParamCharacterSta()

	CLuaPlayerPropertyFight.RefreshParamStr()

	CLuaPlayerPropertyFight.RefreshParamCharacterStr()

	CLuaPlayerPropertyFight.RefreshParamAgi()

	CLuaPlayerPropertyFight.RefreshParamCharacterAgi()

	CLuaPlayerPropertyFight.RefreshParamHpFull()

	CLuaPlayerPropertyFight.RefreshParammAttMin()

	CLuaPlayerPropertyFight.RefreshParammAttMax()

	CLuaPlayerPropertyFight.RefreshParammDef()

	CLuaPlayerPropertyFight.RefreshParamMpFull()

	CLuaPlayerPropertyFight.RefreshParampAttMax()

	CLuaPlayerPropertyFight.RefreshParampDef()

	CLuaPlayerPropertyFight.RefreshParampAttMin()

	CLuaPlayerPropertyFight.RefreshParammMiss()

	CLuaPlayerPropertyFight.RefreshParammHit()

	CLuaPlayerPropertyFight.RefreshParampMiss()

	CLuaPlayerPropertyFight.RefreshParampHit()
end
CLuaPlayerPropertyFight.SetParamRevisePermanentStr = function(newv)
	SetParam_At(14, newv)

	CLuaPlayerPropertyFight.RefreshParamSumPermanent()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidSta()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidInt()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidCor()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidAgi()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidStr()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentCor()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentInt()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentSta()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentStr()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentAgi()

	CLuaPlayerPropertyFight.RefreshParamCharacterCor()

	CLuaPlayerPropertyFight.RefreshParamCor()

	CLuaPlayerPropertyFight.RefreshParamInt()

	CLuaPlayerPropertyFight.RefreshParamCharacterInt()

	CLuaPlayerPropertyFight.RefreshParamSta()

	CLuaPlayerPropertyFight.RefreshParamCharacterSta()

	CLuaPlayerPropertyFight.RefreshParamStr()

	CLuaPlayerPropertyFight.RefreshParamCharacterStr()

	CLuaPlayerPropertyFight.RefreshParamAgi()

	CLuaPlayerPropertyFight.RefreshParamCharacterAgi()

	CLuaPlayerPropertyFight.RefreshParamHpFull()

	CLuaPlayerPropertyFight.RefreshParammAttMin()

	CLuaPlayerPropertyFight.RefreshParammAttMax()

	CLuaPlayerPropertyFight.RefreshParammDef()

	CLuaPlayerPropertyFight.RefreshParamMpFull()

	CLuaPlayerPropertyFight.RefreshParampAttMax()

	CLuaPlayerPropertyFight.RefreshParampDef()

	CLuaPlayerPropertyFight.RefreshParampAttMin()

	CLuaPlayerPropertyFight.RefreshParammMiss()

	CLuaPlayerPropertyFight.RefreshParammHit()

	CLuaPlayerPropertyFight.RefreshParampMiss()

	CLuaPlayerPropertyFight.RefreshParampHit()
end
CLuaPlayerPropertyFight.SetParamRevisePermanentInt = function(newv)
	SetParam_At(15, newv)

	CLuaPlayerPropertyFight.RefreshParamSumPermanent()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidSta()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidInt()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidCor()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidAgi()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidStr()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentCor()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentInt()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentSta()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentStr()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentAgi()

	CLuaPlayerPropertyFight.RefreshParamCharacterCor()

	CLuaPlayerPropertyFight.RefreshParamCor()

	CLuaPlayerPropertyFight.RefreshParamInt()

	CLuaPlayerPropertyFight.RefreshParamCharacterInt()

	CLuaPlayerPropertyFight.RefreshParamSta()

	CLuaPlayerPropertyFight.RefreshParamCharacterSta()

	CLuaPlayerPropertyFight.RefreshParamStr()

	CLuaPlayerPropertyFight.RefreshParamCharacterStr()

	CLuaPlayerPropertyFight.RefreshParamAgi()

	CLuaPlayerPropertyFight.RefreshParamCharacterAgi()

	CLuaPlayerPropertyFight.RefreshParamHpFull()

	CLuaPlayerPropertyFight.RefreshParammAttMin()

	CLuaPlayerPropertyFight.RefreshParammAttMax()

	CLuaPlayerPropertyFight.RefreshParammDef()

	CLuaPlayerPropertyFight.RefreshParamMpFull()

	CLuaPlayerPropertyFight.RefreshParampAttMax()

	CLuaPlayerPropertyFight.RefreshParampDef()

	CLuaPlayerPropertyFight.RefreshParampAttMin()

	CLuaPlayerPropertyFight.RefreshParammMiss()

	CLuaPlayerPropertyFight.RefreshParammHit()

	CLuaPlayerPropertyFight.RefreshParampMiss()

	CLuaPlayerPropertyFight.RefreshParampHit()
end
CLuaPlayerPropertyFight.SetParamRevisePermanentAgi = function(newv)
	SetParam_At(16, newv)

	CLuaPlayerPropertyFight.RefreshParamSumPermanent()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidSta()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidInt()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidCor()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidAgi()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidStr()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentCor()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentInt()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentSta()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentStr()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentAgi()

	CLuaPlayerPropertyFight.RefreshParamCharacterCor()

	CLuaPlayerPropertyFight.RefreshParamCor()

	CLuaPlayerPropertyFight.RefreshParamInt()

	CLuaPlayerPropertyFight.RefreshParamCharacterInt()

	CLuaPlayerPropertyFight.RefreshParamSta()

	CLuaPlayerPropertyFight.RefreshParamCharacterSta()

	CLuaPlayerPropertyFight.RefreshParamStr()

	CLuaPlayerPropertyFight.RefreshParamCharacterStr()

	CLuaPlayerPropertyFight.RefreshParamAgi()

	CLuaPlayerPropertyFight.RefreshParamCharacterAgi()

	CLuaPlayerPropertyFight.RefreshParamHpFull()

	CLuaPlayerPropertyFight.RefreshParammAttMin()

	CLuaPlayerPropertyFight.RefreshParammAttMax()

	CLuaPlayerPropertyFight.RefreshParammDef()

	CLuaPlayerPropertyFight.RefreshParamMpFull()

	CLuaPlayerPropertyFight.RefreshParampAttMax()

	CLuaPlayerPropertyFight.RefreshParampDef()

	CLuaPlayerPropertyFight.RefreshParampAttMin()

	CLuaPlayerPropertyFight.RefreshParammMiss()

	CLuaPlayerPropertyFight.RefreshParammHit()

	CLuaPlayerPropertyFight.RefreshParampMiss()

	CLuaPlayerPropertyFight.RefreshParampHit()
end
CLuaPlayerPropertyFight.SetParamClass = function(newv)
	SetTmpParam_At(1, newv)

	CLuaPlayerPropertyFight.RefreshParamLookUppAttMax()

	CLuaPlayerPropertyFight.RefreshParamLookUpHpFull()

	CLuaPlayerPropertyFight.RefreshParamLookUppAttMin()

	CLuaPlayerPropertyFight.RefreshParamLookUpmAttMax()

	CLuaPlayerPropertyFight.RefreshParamLookUpInt()

	CLuaPlayerPropertyFight.RefreshParamLookUppHit()

	CLuaPlayerPropertyFight.RefreshParamLookUpAgi()

	CLuaPlayerPropertyFight.RefreshParamLookUppMiss()

	CLuaPlayerPropertyFight.RefreshParamLookUpMpFull()

	CLuaPlayerPropertyFight.RefreshParamLookUpmMiss()

	CLuaPlayerPropertyFight.RefreshParamLookUpSta()

	CLuaPlayerPropertyFight.RefreshParamLookUpmAttMin()

	CLuaPlayerPropertyFight.RefreshParamLookUpStr()

	CLuaPlayerPropertyFight.RefreshParamLookUpCor()

	CLuaPlayerPropertyFight.RefreshParamLookUpmHit()

	CLuaPlayerPropertyFight.RefreshParamLookUppDef()

	CLuaPlayerPropertyFight.RefreshParamLookUpmDef()

	CLuaPlayerPropertyFight.RefreshParamCharacterCor()

	CLuaPlayerPropertyFight.RefreshParamCor()

	CLuaPlayerPropertyFight.RefreshParamInt()

	CLuaPlayerPropertyFight.RefreshParamCharacterInt()

	CLuaPlayerPropertyFight.RefreshParamSta()

	CLuaPlayerPropertyFight.RefreshParamCharacterSta()

	CLuaPlayerPropertyFight.RefreshParamStr()

	CLuaPlayerPropertyFight.RefreshParamCharacterStr()

	CLuaPlayerPropertyFight.RefreshParamAgi()

	CLuaPlayerPropertyFight.RefreshParamCharacterAgi()

	CLuaPlayerPropertyFight.RefreshParamHpFull()

	CLuaPlayerPropertyFight.RefreshParammAttMin()

	CLuaPlayerPropertyFight.RefreshParammAttMax()

	CLuaPlayerPropertyFight.RefreshParammDef()

	CLuaPlayerPropertyFight.RefreshParamMpFull()

	CLuaPlayerPropertyFight.RefreshParampAttMax()

	CLuaPlayerPropertyFight.RefreshParampDef()

	CLuaPlayerPropertyFight.RefreshParampAttMin()

	CLuaPlayerPropertyFight.RefreshParammMiss()

	CLuaPlayerPropertyFight.RefreshParammHit()

	CLuaPlayerPropertyFight.RefreshParampMiss()

	CLuaPlayerPropertyFight.RefreshParampHit()
end
CLuaPlayerPropertyFight.SetParamGrade = function(newv)
	SetTmpParam_At(2, newv)

	CLuaPlayerPropertyFight.RefreshParamLookUpInt()

	CLuaPlayerPropertyFight.RefreshParamLookUpAgi()

	CLuaPlayerPropertyFight.RefreshParamLookUpSta()

	CLuaPlayerPropertyFight.RefreshParamLookUpStr()

	CLuaPlayerPropertyFight.RefreshParamLookUpCor()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidSta()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidInt()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidCor()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidAgi()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidStr()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentCor()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentInt()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentSta()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentStr()

	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentAgi()

	CLuaPlayerPropertyFight.RefreshParamCharacterCor()

	CLuaPlayerPropertyFight.RefreshParamCor()

	CLuaPlayerPropertyFight.RefreshParamInt()

	CLuaPlayerPropertyFight.RefreshParamCharacterInt()

	CLuaPlayerPropertyFight.RefreshParamSta()

	CLuaPlayerPropertyFight.RefreshParamCharacterSta()

	CLuaPlayerPropertyFight.RefreshParamStr()

	CLuaPlayerPropertyFight.RefreshParamCharacterStr()

	CLuaPlayerPropertyFight.RefreshParamAgi()

	CLuaPlayerPropertyFight.RefreshParamCharacterAgi()

	CLuaPlayerPropertyFight.RefreshParamHpFull()

	CLuaPlayerPropertyFight.RefreshParammAttMin()

	CLuaPlayerPropertyFight.RefreshParammAttMax()

	CLuaPlayerPropertyFight.RefreshParammDef()

	CLuaPlayerPropertyFight.RefreshParamMpFull()

	CLuaPlayerPropertyFight.RefreshParampAttMax()

	CLuaPlayerPropertyFight.RefreshParampDef()

	CLuaPlayerPropertyFight.RefreshParampAttMin()

	CLuaPlayerPropertyFight.RefreshParammMiss()

	CLuaPlayerPropertyFight.RefreshParammHit()

	CLuaPlayerPropertyFight.RefreshParampMiss()

	CLuaPlayerPropertyFight.RefreshParampHit()
end
CLuaPlayerPropertyFight.RefreshParamCor = function()
	local AdjCor2 = GetTmpParam_At(463)
	local AdjCor = GetTmpParam_At(4)
	local MulCor = GetTmpParam_At(5)
	local LookUpCor = GetTmpParam_At(328)
	local AdjustPermanentCor = GetTmpParam_At(611)

	local newv = (LookUpCor+AdjustPermanentCor+AdjCor)*(1+ MulCor)+AdjCor2
	SetTmpParam_At(3, newv)
end
CLuaPlayerPropertyFight.SetParamAdjCor = function(newv)
	SetTmpParam_At(4, newv)

	CLuaPlayerPropertyFight.RefreshParamCor()

	CLuaPlayerPropertyFight.RefreshParamHpFull()
end
CLuaPlayerPropertyFight.SetParamMulCor = function(newv)
	SetTmpParam_At(5, newv)

	CLuaPlayerPropertyFight.RefreshParamCor()

	CLuaPlayerPropertyFight.RefreshParamHpFull()
end
CLuaPlayerPropertyFight.RefreshParamSta = function()
	local AdjSta = GetTmpParam_At(7)
	local MulSta = GetTmpParam_At(8)
	local LookUpSta = GetTmpParam_At(329)
	local AdjustPermanentSta = GetTmpParam_At(612)

	local newv = (LookUpSta+AdjustPermanentSta+AdjSta)*(1+ MulSta)
	SetTmpParam_At(6, newv)
end
CLuaPlayerPropertyFight.SetParamAdjSta = function(newv)
	SetTmpParam_At(7, newv)

	CLuaPlayerPropertyFight.RefreshParamSta()

	CLuaPlayerPropertyFight.RefreshParamMpFull()
end
CLuaPlayerPropertyFight.SetParamMulSta = function(newv)
	SetTmpParam_At(8, newv)

	CLuaPlayerPropertyFight.RefreshParamSta()

	CLuaPlayerPropertyFight.RefreshParamMpFull()
end
CLuaPlayerPropertyFight.RefreshParamStr = function()
	local AdjStr2 = GetTmpParam_At(504)
	local AdjStr = GetTmpParam_At(10)
	local MulStr = GetTmpParam_At(11)
	local LookUpStr = GetTmpParam_At(330)
	local AdjustPermanentStr = GetTmpParam_At(613)

	local newv = (LookUpStr+AdjustPermanentStr+AdjStr)*(1+ MulStr)+AdjStr2
	SetTmpParam_At(9, newv)
end
CLuaPlayerPropertyFight.SetParamAdjStr = function(newv)
	SetTmpParam_At(10, newv)

	CLuaPlayerPropertyFight.RefreshParamStr()

	CLuaPlayerPropertyFight.RefreshParampAttMax()

	CLuaPlayerPropertyFight.RefreshParampDef()

	CLuaPlayerPropertyFight.RefreshParampAttMin()
end
CLuaPlayerPropertyFight.SetParamMulStr = function(newv)
	SetTmpParam_At(11, newv)

	CLuaPlayerPropertyFight.RefreshParamStr()

	CLuaPlayerPropertyFight.RefreshParampAttMax()

	CLuaPlayerPropertyFight.RefreshParampDef()

	CLuaPlayerPropertyFight.RefreshParampAttMin()
end
CLuaPlayerPropertyFight.RefreshParamInt = function()
	local AdjInt2 = GetTmpParam_At(489)
	local AdjInt = GetTmpParam_At(13)
	local MulInt = GetTmpParam_At(14)
	local LookUpInt = GetTmpParam_At(331)
	local AdjustPermanentInt = GetTmpParam_At(614)

	local newv = (LookUpInt+AdjustPermanentInt+AdjInt)*(1+ MulInt)+AdjInt2
	SetTmpParam_At(12, newv)
end
CLuaPlayerPropertyFight.SetParamAdjInt = function(newv)
	SetTmpParam_At(13, newv)

	CLuaPlayerPropertyFight.RefreshParamInt()

	CLuaPlayerPropertyFight.RefreshParammAttMin()

	CLuaPlayerPropertyFight.RefreshParammAttMax()

	CLuaPlayerPropertyFight.RefreshParammDef()
end
CLuaPlayerPropertyFight.SetParamMulInt = function(newv)
	SetTmpParam_At(14, newv)

	CLuaPlayerPropertyFight.RefreshParamInt()

	CLuaPlayerPropertyFight.RefreshParammAttMin()

	CLuaPlayerPropertyFight.RefreshParammAttMax()

	CLuaPlayerPropertyFight.RefreshParammDef()
end
CLuaPlayerPropertyFight.RefreshParamAgi = function()
	local AdjAgi2 = GetTmpParam_At(435)
	local AdjAgi = GetTmpParam_At(16)
	local MulAgi = GetTmpParam_At(17)
	local LookUpAgi = GetTmpParam_At(332)
	local AdjustPermanentAgi = GetTmpParam_At(615)

	local newv = (LookUpAgi+AdjustPermanentAgi+AdjAgi)*(1+ MulAgi)+AdjAgi2
	SetTmpParam_At(15, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAgi = function(newv)
	SetTmpParam_At(16, newv)

	CLuaPlayerPropertyFight.RefreshParamAgi()

	CLuaPlayerPropertyFight.RefreshParammMiss()

	CLuaPlayerPropertyFight.RefreshParammHit()

	CLuaPlayerPropertyFight.RefreshParampMiss()

	CLuaPlayerPropertyFight.RefreshParampHit()
end
CLuaPlayerPropertyFight.SetParamMulAgi = function(newv)
	SetTmpParam_At(17, newv)

	CLuaPlayerPropertyFight.RefreshParamAgi()

	CLuaPlayerPropertyFight.RefreshParammMiss()

	CLuaPlayerPropertyFight.RefreshParammHit()

	CLuaPlayerPropertyFight.RefreshParampMiss()

	CLuaPlayerPropertyFight.RefreshParampHit()
end
CLuaPlayerPropertyFight.RefreshParamHpFull = function()
	local AdjHpFull2 = GetTmpParam_At(345)
	local AdjHpFull = GetTmpParam_At(19)
	local MulHpFull = GetTmpParam_At(20)
	local LookUpHpFull = GetTmpParam_At(333)
	local Cor = GetTmpParam_At(3)

	local newv = max(1,(LookUpHpFull*Cor + AdjHpFull)*(1+ MulHpFull)+AdjHpFull2)
	SetTmpParam_At(18, newv)
end
CLuaPlayerPropertyFight.SetParamAdjHpFull = function(newv)
	SetTmpParam_At(19, newv)

	CLuaPlayerPropertyFight.RefreshParamHpFull()
end
CLuaPlayerPropertyFight.SetParamMulHpFull = function(newv)
	SetTmpParam_At(20, newv)

	CLuaPlayerPropertyFight.RefreshParamHpFull()
end
CLuaPlayerPropertyFight.RefreshParamHpRecover = function()
	local AdjHpRecover = GetTmpParam_At(22)
	local MulHpRecover = GetTmpParam_At(23)

	local newv = AdjHpRecover*(1+ MulHpRecover)
	SetTmpParam_At(21, newv)
end
CLuaPlayerPropertyFight.SetParamAdjHpRecover = function(newv)
	SetTmpParam_At(22, newv)

	CLuaPlayerPropertyFight.RefreshParamHpRecover()
end
CLuaPlayerPropertyFight.SetParamMulHpRecover = function(newv)
	SetTmpParam_At(23, newv)

	CLuaPlayerPropertyFight.RefreshParamHpRecover()
end
CLuaPlayerPropertyFight.RefreshParamMpFull = function()
	local AdjMpFull = GetTmpParam_At(25)
	local MulMpFull = GetTmpParam_At(26)
	local LookUpMpFull = GetTmpParam_At(334)
	local Sta = GetTmpParam_At(6)

	local newv = max(1,(LookUpMpFull*Sta + AdjMpFull)*(1+ MulMpFull))
	SetTmpParam_At(24, newv)
end
CLuaPlayerPropertyFight.SetParamAdjMpFull = function(newv)
	SetTmpParam_At(25, newv)

	CLuaPlayerPropertyFight.RefreshParamMpFull()
end
CLuaPlayerPropertyFight.SetParamMulMpFull = function(newv)
	SetTmpParam_At(26, newv)

	CLuaPlayerPropertyFight.RefreshParamMpFull()
end
CLuaPlayerPropertyFight.RefreshParamMpRecover = function()
	local AdjMpRecover = GetTmpParam_At(28)
	local MulMpRecover = GetTmpParam_At(29)

	local newv = AdjMpRecover*(1+ MulMpRecover)
	SetTmpParam_At(27, newv)
end
CLuaPlayerPropertyFight.SetParamAdjMpRecover = function(newv)
	SetTmpParam_At(28, newv)

	CLuaPlayerPropertyFight.RefreshParamMpRecover()
end
CLuaPlayerPropertyFight.SetParamMulMpRecover = function(newv)
	SetTmpParam_At(29, newv)

	CLuaPlayerPropertyFight.RefreshParamMpRecover()
end
CLuaPlayerPropertyFight.RefreshParampAttMin = function()
	local AdjpAttMax2 = GetTmpParam_At(347)
	local AdjpAttMin2 = GetTmpParam_At(346)
	local AdjpAttMax = GetTmpParam_At(34)
	local MulpAttMax = GetTmpParam_At(35)
	local AdjpAttMin = GetTmpParam_At(31)
	local MulpAttMin = GetTmpParam_At(32)
	local LookUppAttMax = GetTmpParam_At(336)
	local Str = GetTmpParam_At(9)
	local LookUppAttMin = GetTmpParam_At(335)

	local newv = max(1,min((LookUppAttMax*Str + AdjpAttMax)*(1+ MulpAttMax)+AdjpAttMax2,(LookUppAttMin*Str + AdjpAttMin)*(1+ MulpAttMin)+AdjpAttMin2))
	SetTmpParam_At(30, newv)
end
CLuaPlayerPropertyFight.SetParamAdjpAttMin = function(newv)
	SetTmpParam_At(31, newv)

	CLuaPlayerPropertyFight.RefreshParampAttMin()
end
CLuaPlayerPropertyFight.SetParamMulpAttMin = function(newv)
	SetTmpParam_At(32, newv)

	CLuaPlayerPropertyFight.RefreshParampAttMin()
end
CLuaPlayerPropertyFight.RefreshParampAttMax = function()
	local AdjpAttMax2 = GetTmpParam_At(347)
	local AdjpAttMax = GetTmpParam_At(34)
	local MulpAttMax = GetTmpParam_At(35)
	local LookUppAttMax = GetTmpParam_At(336)
	local Str = GetTmpParam_At(9)

	local newv = max(1,(LookUppAttMax*Str + AdjpAttMax)*(1+ MulpAttMax)+AdjpAttMax2)
	SetTmpParam_At(33, newv)
end
CLuaPlayerPropertyFight.SetParamAdjpAttMax = function(newv)
	SetTmpParam_At(34, newv)

	CLuaPlayerPropertyFight.RefreshParampAttMax()

	CLuaPlayerPropertyFight.RefreshParampAttMin()
end
CLuaPlayerPropertyFight.SetParamMulpAttMax = function(newv)
	SetTmpParam_At(35, newv)

	CLuaPlayerPropertyFight.RefreshParampAttMax()

	CLuaPlayerPropertyFight.RefreshParampAttMin()
end
CLuaPlayerPropertyFight.RefreshParampHit = function()
	local AdjpHit2 = GetTmpParam_At(500)
	local AdjpHit = GetTmpParam_At(37)
	local MulpHit = GetTmpParam_At(38)
	local LookUppHit = GetTmpParam_At(337)
	local Agi = GetTmpParam_At(15)

	local newv = (LookUppHit*Agi + AdjpHit)*(1+ MulpHit)+AdjpHit2
	SetTmpParam_At(36, newv)
end
CLuaPlayerPropertyFight.SetParamAdjpHit = function(newv)
	SetTmpParam_At(37, newv)

	CLuaPlayerPropertyFight.RefreshParampHit()
end
CLuaPlayerPropertyFight.SetParamMulpHit = function(newv)
	SetTmpParam_At(38, newv)

	CLuaPlayerPropertyFight.RefreshParampHit()
end
CLuaPlayerPropertyFight.RefreshParampMiss = function()
	local AdjpMiss2 = GetTmpParam_At(502)
	local AdjpMiss = GetTmpParam_At(40)
	local MulpMiss = GetTmpParam_At(41)
	local LookUppMiss = GetTmpParam_At(338)
	local Agi = GetTmpParam_At(15)

	local newv = (LookUppMiss*Agi+ AdjpMiss)*(1+ MulpMiss)+AdjpMiss2
	SetTmpParam_At(39, newv)
end
CLuaPlayerPropertyFight.SetParamAdjpMiss = function(newv)
	SetTmpParam_At(40, newv)

	CLuaPlayerPropertyFight.RefreshParampMiss()
end
CLuaPlayerPropertyFight.SetParamMulpMiss = function(newv)
	SetTmpParam_At(41, newv)

	CLuaPlayerPropertyFight.RefreshParampMiss()
end
CLuaPlayerPropertyFight.RefreshParampDef = function()
	local AdjpDef2 = GetTmpParam_At(497)
	local AdjpDef = GetTmpParam_At(43)
	local MulpDef = GetTmpParam_At(44)
	local LookUppDef = GetTmpParam_At(339)
	local Str = GetTmpParam_At(9)

	local newv = max(1,(LookUppDef*Str+ AdjpDef)*(1+ MulpDef)+AdjpDef2)
	SetTmpParam_At(42, newv)
end
CLuaPlayerPropertyFight.SetParamAdjpDef = function(newv)
	SetTmpParam_At(43, newv)

	CLuaPlayerPropertyFight.RefreshParampDef()
end
CLuaPlayerPropertyFight.SetParamMulpDef = function(newv)
	SetTmpParam_At(44, newv)

	CLuaPlayerPropertyFight.RefreshParampDef()
end
CLuaPlayerPropertyFight.SetParamAdjpHurt = function(newv)
	SetTmpParam_At(45, newv)
end
CLuaPlayerPropertyFight.SetParamMulpHurt = function(newv)
	SetTmpParam_At(46, newv)
end
CLuaPlayerPropertyFight.RefreshParampSpeed = function()
	local OripSpeed = GetTmpParam_At(48)
	local AdjpSpeed = GetTmpParam_At(1001)
	local MulpSpeed = GetTmpParam_At(49)

	local newv = max(0.2,min(2.5,(OripSpeed+AdjpSpeed)*(1+ MulpSpeed)))
	SetTmpParam_At(47, newv)
end
CLuaPlayerPropertyFight.SetParamOripSpeed = function(newv)
	SetTmpParam_At(48, newv)

	CLuaPlayerPropertyFight.RefreshParampSpeed()
end
CLuaPlayerPropertyFight.SetParamMulpSpeed = function(newv)
	SetTmpParam_At(49, newv)

	CLuaPlayerPropertyFight.RefreshParampSpeed()
end
CLuaPlayerPropertyFight.RefreshParampFatal = function()
	local AdjpFatal = GetTmpParam_At(51)
	local AdjpFatal2 = GetTmpParam_At(498)

	local newv = AdjpFatal+AdjpFatal2
	SetTmpParam_At(50, newv)
end
CLuaPlayerPropertyFight.SetParamAdjpFatal = function(newv)
	SetTmpParam_At(51, newv)

	CLuaPlayerPropertyFight.RefreshParampFatal()
end
CLuaPlayerPropertyFight.RefreshParamAntipFatal = function()
	local AdjAntipFatal = GetTmpParam_At(53)
	local AdjAntipFatal2 = GetTmpParam_At(452)

	local newv = AdjAntipFatal+AdjAntipFatal2
	SetTmpParam_At(52, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntipFatal = function(newv)
	SetTmpParam_At(53, newv)

	CLuaPlayerPropertyFight.RefreshParamAntipFatal()
end
CLuaPlayerPropertyFight.RefreshParampFatalDamage = function()
	local AdjpFatalDamage2 = GetTmpParam_At(499)
	local AdjpFatalDamage = GetTmpParam_At(55)

	local newv = (AdjpFatalDamage + 0.5+AdjpFatalDamage2)
	SetTmpParam_At(54, newv)
end
CLuaPlayerPropertyFight.SetParamAdjpFatalDamage = function(newv)
	SetTmpParam_At(55, newv)

	CLuaPlayerPropertyFight.RefreshParampFatalDamage()
end
CLuaPlayerPropertyFight.RefreshParamAntipFatalDamage = function()
	local AdjAntipFatalDamage = GetTmpParam_At(57)
	local AdjAntipFatalDamage2 = GetTmpParam_At(453)

	local newv = AdjAntipFatalDamage+AdjAntipFatalDamage2
	SetTmpParam_At(56, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntipFatalDamage = function(newv)
	SetTmpParam_At(57, newv)

	CLuaPlayerPropertyFight.RefreshParamAntipFatalDamage()
end
CLuaPlayerPropertyFight.RefreshParamBlock = function()
	local AdjBlock2 = GetTmpParam_At(461)
	local AdjBlock = GetTmpParam_At(59)
	local MulBlock = GetTmpParam_At(60)

	local newv = max(0,AdjBlock * (1+MulBlock)+AdjBlock2)
	SetTmpParam_At(58, newv)
end
CLuaPlayerPropertyFight.SetParamAdjBlock = function(newv)
	SetTmpParam_At(59, newv)

	CLuaPlayerPropertyFight.RefreshParamBlock()
end
CLuaPlayerPropertyFight.SetParamMulBlock = function(newv)
	SetTmpParam_At(60, newv)

	CLuaPlayerPropertyFight.RefreshParamBlock()
end
CLuaPlayerPropertyFight.RefreshParamBlockDamage = function()
	local AdjBlockDamage2 = GetTmpParam_At(462)
	local AdjBlockDamage = GetTmpParam_At(62)

	local newv = max(0,200+AdjBlockDamage+AdjBlockDamage2)
	SetTmpParam_At(61, newv)
end
CLuaPlayerPropertyFight.SetParamAdjBlockDamage = function(newv)
	SetTmpParam_At(62, newv)

	CLuaPlayerPropertyFight.RefreshParamBlockDamage()
end
CLuaPlayerPropertyFight.RefreshParamAntiBlock = function()
	local AdjAntiBlock = GetTmpParam_At(64)
	local AdjAntiBlock2 = GetTmpParam_At(439)

	local newv = AdjAntiBlock+AdjAntiBlock2
	SetTmpParam_At(63, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntiBlock = function(newv)
	SetTmpParam_At(64, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiBlock()
end
CLuaPlayerPropertyFight.RefreshParamAntiBlockDamage = function()
	local AdjAntiBlockDamage = GetTmpParam_At(66)
	local AdjAntiBlockDamage2 = GetTmpParam_At(440)

	local newv = AdjAntiBlockDamage+AdjAntiBlockDamage2
	SetTmpParam_At(65, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntiBlockDamage = function(newv)
	SetTmpParam_At(66, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiBlockDamage()
end
CLuaPlayerPropertyFight.RefreshParammAttMin = function()
	local AdjmAttMax2 = GetTmpParam_At(349)
	local AdjmAttMin2 = GetTmpParam_At(348)
	local AdjmAttMax = GetTmpParam_At(71)
	local MulmAttMax = GetTmpParam_At(72)
	local AdjmAttMin = GetTmpParam_At(68)
	local MulmAttMin = GetTmpParam_At(69)
	local LookUpmAttMax = GetTmpParam_At(341)
	local Int = GetTmpParam_At(12)
	local LookUpmAttMin = GetTmpParam_At(340)

	local newv = max(1,min((LookUpmAttMax*Int + AdjmAttMax)*(1+ MulmAttMax)+AdjmAttMax2,(LookUpmAttMin*Int + AdjmAttMin)*(1+ MulmAttMin)+AdjmAttMin2))
	SetTmpParam_At(67, newv)
end
CLuaPlayerPropertyFight.SetParamAdjmAttMin = function(newv)
	SetTmpParam_At(68, newv)

	CLuaPlayerPropertyFight.RefreshParammAttMin()
end
CLuaPlayerPropertyFight.SetParamMulmAttMin = function(newv)
	SetTmpParam_At(69, newv)

	CLuaPlayerPropertyFight.RefreshParammAttMin()
end
CLuaPlayerPropertyFight.RefreshParammAttMax = function()
	local AdjmAttMax2 = GetTmpParam_At(349)
	local AdjmAttMax = GetTmpParam_At(71)
	local MulmAttMax = GetTmpParam_At(72)
	local LookUpmAttMax = GetTmpParam_At(341)
	local Int = GetTmpParam_At(12)

	local newv = max(1,(LookUpmAttMax*Int + AdjmAttMax)*(1+ MulmAttMax)+AdjmAttMax2)
	SetTmpParam_At(70, newv)
end
CLuaPlayerPropertyFight.SetParamAdjmAttMax = function(newv)
	SetTmpParam_At(71, newv)

	CLuaPlayerPropertyFight.RefreshParammAttMin()

	CLuaPlayerPropertyFight.RefreshParammAttMax()
end
CLuaPlayerPropertyFight.SetParamMulmAttMax = function(newv)
	SetTmpParam_At(72, newv)

	CLuaPlayerPropertyFight.RefreshParammAttMin()

	CLuaPlayerPropertyFight.RefreshParammAttMax()
end
CLuaPlayerPropertyFight.RefreshParammHit = function()
	local AdjmHit2 = GetTmpParam_At(493)
	local AdjmHit = GetTmpParam_At(74)
	local MulmHit = GetTmpParam_At(75)
	local LookUpmHit = GetTmpParam_At(342)
	local Agi = GetTmpParam_At(15)

	local newv = (LookUpmHit*Agi + AdjmHit)*(1+ MulmHit)+AdjmHit2
	SetTmpParam_At(73, newv)
end
CLuaPlayerPropertyFight.SetParamAdjmHit = function(newv)
	SetTmpParam_At(74, newv)

	CLuaPlayerPropertyFight.RefreshParammHit()
end
CLuaPlayerPropertyFight.SetParamMulmHit = function(newv)
	SetTmpParam_At(75, newv)

	CLuaPlayerPropertyFight.RefreshParammHit()
end
CLuaPlayerPropertyFight.RefreshParammMiss = function()
	local AdjmMiss2 = GetTmpParam_At(495)
	local AdjmMiss = GetTmpParam_At(77)
	local MulmMiss = GetTmpParam_At(78)
	local LookUpmMiss = GetTmpParam_At(343)
	local Agi = GetTmpParam_At(15)

	local newv = (LookUpmMiss*Agi + AdjmMiss)*(1+ MulmMiss)+AdjmMiss2
	SetTmpParam_At(76, newv)
end
CLuaPlayerPropertyFight.SetParamAdjmMiss = function(newv)
	SetTmpParam_At(77, newv)

	CLuaPlayerPropertyFight.RefreshParammMiss()
end
CLuaPlayerPropertyFight.SetParamMulmMiss = function(newv)
	SetTmpParam_At(78, newv)

	CLuaPlayerPropertyFight.RefreshParammMiss()
end
CLuaPlayerPropertyFight.RefreshParammDef = function()
	local AdjmDef2 = GetTmpParam_At(490)
	local AdjmDef = GetTmpParam_At(80)
	local MulmDef = GetTmpParam_At(81)
	local LookUpmDef = GetTmpParam_At(344)
	local Int = GetTmpParam_At(12)

	local newv = max(1,(LookUpmDef*Int + AdjmDef)*(1+ MulmDef)+AdjmDef2)
	SetTmpParam_At(79, newv)
end
CLuaPlayerPropertyFight.SetParamAdjmDef = function(newv)
	SetTmpParam_At(80, newv)

	CLuaPlayerPropertyFight.RefreshParammDef()
end
CLuaPlayerPropertyFight.SetParamMulmDef = function(newv)
	SetTmpParam_At(81, newv)

	CLuaPlayerPropertyFight.RefreshParammDef()
end
CLuaPlayerPropertyFight.SetParamAdjmHurt = function(newv)
	SetTmpParam_At(82, newv)
end
CLuaPlayerPropertyFight.SetParamMulmHurt = function(newv)
	SetTmpParam_At(83, newv)
end
CLuaPlayerPropertyFight.RefreshParammSpeed = function()
	local OrimSpeed = GetTmpParam_At(85)
	local AdjmSpeed = GetTmpParam_At(1002)
	local MulmSpeed = GetTmpParam_At(86)

	local newv = max(0.2,min(2.5,(OrimSpeed+AdjmSpeed)*(1+ MulmSpeed)))
	SetTmpParam_At(84, newv)
end
CLuaPlayerPropertyFight.SetParamOrimSpeed = function(newv)
	SetTmpParam_At(85, newv)

	CLuaPlayerPropertyFight.RefreshParammSpeed()
end
CLuaPlayerPropertyFight.SetParamMulmSpeed = function(newv)
	SetTmpParam_At(86, newv)

	CLuaPlayerPropertyFight.RefreshParammSpeed()
end
CLuaPlayerPropertyFight.RefreshParammFatal = function()
	local AdjmFatal = GetTmpParam_At(88)
	local AdjmFatal2 = GetTmpParam_At(491)

	local newv = AdjmFatal+AdjmFatal2
	SetTmpParam_At(87, newv)
end
CLuaPlayerPropertyFight.SetParamAdjmFatal = function(newv)
	SetTmpParam_At(88, newv)

	CLuaPlayerPropertyFight.RefreshParammFatal()
end
CLuaPlayerPropertyFight.RefreshParamAntimFatal = function()
	local AdjAntimFatal = GetTmpParam_At(90)
	local AdjAntimFatal2 = GetTmpParam_At(449)

	local newv = AdjAntimFatal+AdjAntimFatal2
	SetTmpParam_At(89, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntimFatal = function(newv)
	SetTmpParam_At(90, newv)

	CLuaPlayerPropertyFight.RefreshParamAntimFatal()
end
CLuaPlayerPropertyFight.RefreshParammFatalDamage = function()
	local AdjmFatalDamage2 = GetTmpParam_At(492)
	local AdjmFatalDamage = GetTmpParam_At(92)

	local newv = (AdjmFatalDamage+0.5+AdjmFatalDamage2)
	SetTmpParam_At(91, newv)
end
CLuaPlayerPropertyFight.SetParamAdjmFatalDamage = function(newv)
	SetTmpParam_At(92, newv)

	CLuaPlayerPropertyFight.RefreshParammFatalDamage()
end
CLuaPlayerPropertyFight.RefreshParamAntimFatalDamage = function()
	local AdjAntimFatalDamage = GetTmpParam_At(94)
	local AdjAntimFatalDamage2 = GetTmpParam_At(450)

	local newv = AdjAntimFatalDamage+AdjAntimFatalDamage2
	SetTmpParam_At(93, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntimFatalDamage = function(newv)
	SetTmpParam_At(94, newv)

	CLuaPlayerPropertyFight.RefreshParamAntimFatalDamage()
end
CLuaPlayerPropertyFight.RefreshParamEnhanceFire = function()
	local AdjEnhanceFire = GetTmpParam_At(96)
	local AdjEnhanceFire2 = GetTmpParam_At(468)

	local newv = AdjEnhanceFire+AdjEnhanceFire2
	SetTmpParam_At(95, newv)
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceFire = function(newv)
	SetTmpParam_At(96, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceFire()
end
CLuaPlayerPropertyFight.SetParamMulEnhanceFire = function(newv)
	SetTmpParam_At(97, newv)
end
CLuaPlayerPropertyFight.RefreshParamEnhanceThunder = function()
	local AdjEnhanceThunder = GetTmpParam_At(99)
	local AdjEnhanceThunder2 = GetTmpParam_At(477)

	local newv = AdjEnhanceThunder+AdjEnhanceThunder2
	SetTmpParam_At(98, newv)
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceThunder = function(newv)
	SetTmpParam_At(99, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceThunder()
end
CLuaPlayerPropertyFight.SetParamMulEnhanceThunder = function(newv)
	SetTmpParam_At(100, newv)
end
CLuaPlayerPropertyFight.RefreshParamEnhanceIce = function()
	local AdjEnhanceIce = GetTmpParam_At(102)
	local AdjEnhanceIce2 = GetTmpParam_At(470)

	local newv = AdjEnhanceIce+AdjEnhanceIce2
	SetTmpParam_At(101, newv)
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceIce = function(newv)
	SetTmpParam_At(102, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceIce()
end
CLuaPlayerPropertyFight.SetParamMulEnhanceIce = function(newv)
	SetTmpParam_At(103, newv)
end
CLuaPlayerPropertyFight.RefreshParamEnhancePoison = function()
	local AdjEnhancePoison = GetTmpParam_At(105)
	local AdjEnhancePoison2 = GetTmpParam_At(474)

	local newv = AdjEnhancePoison+AdjEnhancePoison2
	SetTmpParam_At(104, newv)
end
CLuaPlayerPropertyFight.SetParamAdjEnhancePoison = function(newv)
	SetTmpParam_At(105, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhancePoison()
end
CLuaPlayerPropertyFight.SetParamMulEnhancePoison = function(newv)
	SetTmpParam_At(106, newv)
end
CLuaPlayerPropertyFight.RefreshParamEnhanceWind = function()
	local AdjEnhanceWind = GetTmpParam_At(108)
	local AdjEnhanceWind2 = GetTmpParam_At(480)

	local newv = AdjEnhanceWind+AdjEnhanceWind2
	SetTmpParam_At(107, newv)
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceWind = function(newv)
	SetTmpParam_At(108, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceWind()
end
CLuaPlayerPropertyFight.SetParamMulEnhanceWind = function(newv)
	SetTmpParam_At(109, newv)
end
CLuaPlayerPropertyFight.RefreshParamEnhanceLight = function()
	local AdjEnhanceLight = GetTmpParam_At(111)
	local AdjEnhanceLight2 = GetTmpParam_At(472)

	local newv = AdjEnhanceLight+AdjEnhanceLight2
	SetTmpParam_At(110, newv)
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceLight = function(newv)
	SetTmpParam_At(111, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceLight()
end
CLuaPlayerPropertyFight.SetParamMulEnhanceLight = function(newv)
	SetTmpParam_At(112, newv)
end
CLuaPlayerPropertyFight.RefreshParamEnhanceIllusion = function()
	local AdjEnhanceIllusion = GetTmpParam_At(114)
	local AdjEnhanceIllusion2 = GetTmpParam_At(471)

	local newv = AdjEnhanceIllusion+AdjEnhanceIllusion2
	SetTmpParam_At(113, newv)
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceIllusion = function(newv)
	SetTmpParam_At(114, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceIllusion()
end
CLuaPlayerPropertyFight.SetParamMulEnhanceIllusion = function(newv)
	SetTmpParam_At(115, newv)
end
CLuaPlayerPropertyFight.RefreshParamAntiFire = function()
	local AdjAntiFire2 = GetTmpParam_At(444)
	local AdjAntiFire = GetTmpParam_At(117)
	local MulAntiFire = GetTmpParam_At(118)

	local newv = AdjAntiFire*(1+MulAntiFire)+AdjAntiFire2
	SetTmpParam_At(116, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntiFire = function(newv)
	SetTmpParam_At(117, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiFire()
end
CLuaPlayerPropertyFight.SetParamMulAntiFire = function(newv)
	SetTmpParam_At(118, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiFire()
end
CLuaPlayerPropertyFight.RefreshParamAntiThunder = function()
	local AdjAntiThunder2 = GetTmpParam_At(457)
	local AdjAntiThunder = GetTmpParam_At(120)
	local MulAntiThunder = GetTmpParam_At(121)

	local newv = AdjAntiThunder*(1+MulAntiThunder)+AdjAntiThunder2
	SetTmpParam_At(119, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntiThunder = function(newv)
	SetTmpParam_At(120, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiThunder()
end
CLuaPlayerPropertyFight.SetParamMulAntiThunder = function(newv)
	SetTmpParam_At(121, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiThunder()
end
CLuaPlayerPropertyFight.RefreshParamAntiIce = function()
	local AdjAntiIce2 = GetTmpParam_At(446)
	local AdjAntiIce = GetTmpParam_At(123)
	local MulAntiIce = GetTmpParam_At(124)

	local newv = AdjAntiIce*(1+MulAntiIce)+AdjAntiIce2
	SetTmpParam_At(122, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntiIce = function(newv)
	SetTmpParam_At(123, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiIce()
end
CLuaPlayerPropertyFight.SetParamMulAntiIce = function(newv)
	SetTmpParam_At(124, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiIce()
end
CLuaPlayerPropertyFight.RefreshParamAntiPoison = function()
	local AdjAntiPoison2 = GetTmpParam_At(454)
	local AdjAntiPoison = GetTmpParam_At(126)
	local MulAntiPoison = GetTmpParam_At(127)

	local newv = AdjAntiPoison*(1+MulAntiPoison)+AdjAntiPoison2
	SetTmpParam_At(125, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntiPoison = function(newv)
	SetTmpParam_At(126, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiPoison()
end
CLuaPlayerPropertyFight.SetParamMulAntiPoison = function(newv)
	SetTmpParam_At(127, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiPoison()
end
CLuaPlayerPropertyFight.RefreshParamAntiWind = function()
	local AdjAntiWind2 = GetTmpParam_At(460)
	local AdjAntiWind = GetTmpParam_At(129)
	local MulAntiWind = GetTmpParam_At(130)

	local newv = AdjAntiWind*(1+MulAntiWind)+AdjAntiWind2
	SetTmpParam_At(128, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntiWind = function(newv)
	SetTmpParam_At(129, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiWind()
end
CLuaPlayerPropertyFight.SetParamMulAntiWind = function(newv)
	SetTmpParam_At(130, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiWind()
end
CLuaPlayerPropertyFight.RefreshParamAntiLight = function()
	local AdjAntiLight2 = GetTmpParam_At(448)
	local AdjAntiLight = GetTmpParam_At(132)
	local MulAntiLight = GetTmpParam_At(133)

	local newv = AdjAntiLight*(1+MulAntiLight)+AdjAntiLight2
	SetTmpParam_At(131, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntiLight = function(newv)
	SetTmpParam_At(132, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiLight()
end
CLuaPlayerPropertyFight.SetParamMulAntiLight = function(newv)
	SetTmpParam_At(133, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiLight()
end
CLuaPlayerPropertyFight.RefreshParamAntiIllusion = function()
	local AdjAntiIllusion2 = GetTmpParam_At(447)
	local AdjAntiIllusion = GetTmpParam_At(135)
	local MulAntiIllusion = GetTmpParam_At(136)

	local newv = AdjAntiIllusion*(1+MulAntiIllusion)+AdjAntiIllusion2
	SetTmpParam_At(134, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntiIllusion = function(newv)
	SetTmpParam_At(135, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiIllusion()
end
CLuaPlayerPropertyFight.SetParamMulAntiIllusion = function(newv)
	SetTmpParam_At(136, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiIllusion()
end
CLuaPlayerPropertyFight.RefreshParamIgnoreAntiFire = function()
	local AdjIgnoreAntiFire = GetTmpParam_At(417)
	local AdjIgnoreAntiFire2 = GetTmpParam_At(481)

	local newv = AdjIgnoreAntiFire+AdjIgnoreAntiFire2
	SetTmpParam_At(137, newv)
end
CLuaPlayerPropertyFight.RefreshParamIgnoreAntiThunder = function()
	local AdjIgnoreAntiThunder = GetTmpParam_At(418)
	local AdjIgnoreAntiThunder2 = GetTmpParam_At(486)

	local newv = AdjIgnoreAntiThunder+AdjIgnoreAntiThunder2
	SetTmpParam_At(138, newv)
end
CLuaPlayerPropertyFight.RefreshParamIgnoreAntiIce = function()
	local AdjIgnoreAntiIce = GetTmpParam_At(419)
	local AdjIgnoreAntiIce2 = GetTmpParam_At(482)

	local newv = AdjIgnoreAntiIce+AdjIgnoreAntiIce2
	SetTmpParam_At(139, newv)
end
CLuaPlayerPropertyFight.RefreshParamIgnoreAntiPoison = function()
	local AdjIgnoreAntiPoison = GetTmpParam_At(420)
	local AdjIgnoreAntiPoison2 = GetTmpParam_At(485)

	local newv = AdjIgnoreAntiPoison+AdjIgnoreAntiPoison2
	SetTmpParam_At(140, newv)
end
CLuaPlayerPropertyFight.RefreshParamIgnoreAntiWind = function()
	local AdjIgnoreAntiWind = GetTmpParam_At(421)
	local AdjIgnoreAntiWind2 = GetTmpParam_At(488)

	local newv = AdjIgnoreAntiWind+AdjIgnoreAntiWind2
	SetTmpParam_At(141, newv)
end
CLuaPlayerPropertyFight.RefreshParamIgnoreAntiLight = function()
	local AdjIgnoreAntiLight = GetTmpParam_At(422)
	local AdjIgnoreAntiLight2 = GetTmpParam_At(484)

	local newv = AdjIgnoreAntiLight+AdjIgnoreAntiLight2
	SetTmpParam_At(142, newv)
end
CLuaPlayerPropertyFight.RefreshParamIgnoreAntiIllusion = function()
	local AdjIgnoreAntiIllusion = GetTmpParam_At(423)
	local AdjIgnoreAntiIllusion2 = GetTmpParam_At(483)

	local newv = AdjIgnoreAntiIllusion+AdjIgnoreAntiIllusion2
	SetTmpParam_At(143, newv)
end
CLuaPlayerPropertyFight.RefreshParamEnhanceDizzy = function()
	local AdjEnhanceDizzy2 = GetTmpParam_At(467)
	local AdjEnhanceDizzy = GetTmpParam_At(145)
	local MulEnhanceDizzy = GetTmpParam_At(146)

	local newv = AdjEnhanceDizzy*(1+MulEnhanceDizzy)+AdjEnhanceDizzy2
	SetTmpParam_At(144, newv)
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceDizzy = function(newv)
	SetTmpParam_At(145, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceDizzy()
end
CLuaPlayerPropertyFight.SetParamMulEnhanceDizzy = function(newv)
	SetTmpParam_At(146, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceDizzy()
end
CLuaPlayerPropertyFight.RefreshParamEnhanceSleep = function()
	local AdjEnhanceSleep2 = GetTmpParam_At(476)
	local AdjEnhanceSleep = GetTmpParam_At(148)
	local MulEnhanceSleep = GetTmpParam_At(149)

	local newv = AdjEnhanceSleep*(1+MulEnhanceSleep)+AdjEnhanceSleep2
	SetTmpParam_At(147, newv)
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceSleep = function(newv)
	SetTmpParam_At(148, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceSleep()
end
CLuaPlayerPropertyFight.SetParamMulEnhanceSleep = function(newv)
	SetTmpParam_At(149, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceSleep()
end
CLuaPlayerPropertyFight.RefreshParamEnhanceChaos = function()
	local AdjEnhanceChaos2 = GetTmpParam_At(466)
	local AdjEnhanceChaos = GetTmpParam_At(151)
	local MulEnhanceChaos = GetTmpParam_At(152)

	local newv = AdjEnhanceChaos*(1+MulEnhanceChaos)+AdjEnhanceChaos2
	SetTmpParam_At(150, newv)
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceChaos = function(newv)
	SetTmpParam_At(151, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceChaos()
end
CLuaPlayerPropertyFight.SetParamMulEnhanceChaos = function(newv)
	SetTmpParam_At(152, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceChaos()
end
CLuaPlayerPropertyFight.RefreshParamEnhanceBind = function()
	local AdjEnhanceBind2 = GetTmpParam_At(465)
	local AdjEnhanceBind = GetTmpParam_At(154)
	local MulEnhanceBind = GetTmpParam_At(155)

	local newv = AdjEnhanceBind*(1+MulEnhanceBind)+AdjEnhanceBind2
	SetTmpParam_At(153, newv)
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceBind = function(newv)
	SetTmpParam_At(154, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceBind()
end
CLuaPlayerPropertyFight.SetParamMulEnhanceBind = function(newv)
	SetTmpParam_At(155, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceBind()
end
CLuaPlayerPropertyFight.RefreshParamEnhanceSilence = function()
	local AdjEnhanceSilence2 = GetTmpParam_At(475)
	local AdjEnhanceSilence = GetTmpParam_At(157)
	local MulEnhanceSilence = GetTmpParam_At(158)

	local newv = AdjEnhanceSilence*(1+MulEnhanceSilence)+AdjEnhanceSilence2
	SetTmpParam_At(156, newv)
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceSilence = function(newv)
	SetTmpParam_At(157, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceSilence()
end
CLuaPlayerPropertyFight.SetParamMulEnhanceSilence = function(newv)
	SetTmpParam_At(158, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceSilence()
end
CLuaPlayerPropertyFight.RefreshParamAntiDizzy = function()
	local AdjAntiDizzy2 = GetTmpParam_At(443)
	local AdjAntiDizzy = GetTmpParam_At(160)
	local MulAntiDizzy = GetTmpParam_At(161)

	local newv = AdjAntiDizzy*(1+MulAntiDizzy)+AdjAntiDizzy2
	SetTmpParam_At(159, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntiDizzy = function(newv)
	SetTmpParam_At(160, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiDizzy()
end
CLuaPlayerPropertyFight.SetParamMulAntiDizzy = function(newv)
	SetTmpParam_At(161, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiDizzy()
end
CLuaPlayerPropertyFight.RefreshParamAntiSleep = function()
	local AdjAntiSleep2 = GetTmpParam_At(456)
	local AdjAntiSleep = GetTmpParam_At(163)
	local MulAntiSleep = GetTmpParam_At(164)

	local newv = AdjAntiSleep*(1+MulAntiSleep)+AdjAntiSleep2
	SetTmpParam_At(162, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntiSleep = function(newv)
	SetTmpParam_At(163, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiSleep()
end
CLuaPlayerPropertyFight.SetParamMulAntiSleep = function(newv)
	SetTmpParam_At(164, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiSleep()
end
CLuaPlayerPropertyFight.RefreshParamAntiChaos = function()
	local AdjAntiChaos2 = GetTmpParam_At(441)
	local AdjAntiChaos = GetTmpParam_At(166)
	local MulAntiChaos = GetTmpParam_At(167)

	local newv = AdjAntiChaos*(1+MulAntiChaos)+AdjAntiChaos2
	SetTmpParam_At(165, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntiChaos = function(newv)
	SetTmpParam_At(166, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiChaos()
end
CLuaPlayerPropertyFight.SetParamMulAntiChaos = function(newv)
	SetTmpParam_At(167, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiChaos()
end
CLuaPlayerPropertyFight.RefreshParamAntiBind = function()
	local AdjAntiBind2 = GetTmpParam_At(437)
	local AdjAntiBind = GetTmpParam_At(169)
	local MulAntiBind = GetTmpParam_At(170)

	local newv = AdjAntiBind*(1+MulAntiBind)+AdjAntiBind2
	SetTmpParam_At(168, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntiBind = function(newv)
	SetTmpParam_At(169, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiBind()
end
CLuaPlayerPropertyFight.SetParamMulAntiBind = function(newv)
	SetTmpParam_At(170, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiBind()
end
CLuaPlayerPropertyFight.RefreshParamAntiSilence = function()
	local AdjAntiSilence2 = GetTmpParam_At(455)
	local AdjAntiSilence = GetTmpParam_At(172)
	local MulAntiSilence = GetTmpParam_At(173)

	local newv = AdjAntiSilence*(1+MulAntiSilence)+AdjAntiSilence2
	SetTmpParam_At(171, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntiSilence = function(newv)
	SetTmpParam_At(172, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiSilence()
end
CLuaPlayerPropertyFight.SetParamMulAntiSilence = function(newv)
	SetTmpParam_At(173, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiSilence()
end
CLuaPlayerPropertyFight.SetParamDizzyTimeChange = function(newv)
	SetTmpParam_At(174, newv)
end
CLuaPlayerPropertyFight.SetParamSleepTimeChange = function(newv)
	SetTmpParam_At(175, newv)
end
CLuaPlayerPropertyFight.SetParamChaosTimeChange = function(newv)
	SetTmpParam_At(176, newv)
end
CLuaPlayerPropertyFight.SetParamBindTimeChange = function(newv)
	SetTmpParam_At(177, newv)
end
CLuaPlayerPropertyFight.SetParamSilenceTimeChange = function(newv)
	SetTmpParam_At(178, newv)
end
CLuaPlayerPropertyFight.SetParamBianhuTimeChange = function(newv)
	SetTmpParam_At(179, newv)
end
CLuaPlayerPropertyFight.SetParamFreezeTimeChange = function(newv)
	SetTmpParam_At(180, newv)
end
CLuaPlayerPropertyFight.SetParamPetrifyTimeChange = function(newv)
	SetTmpParam_At(181, newv)
end
CLuaPlayerPropertyFight.RefreshParamEnhanceHeal = function()
	local AdjEnhanceHeal = GetTmpParam_At(183)

	local newv = AdjEnhanceHeal
	SetTmpParam_At(182, newv)
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceHeal = function(newv)
	SetTmpParam_At(183, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceHeal()
end
CLuaPlayerPropertyFight.SetParamMulEnhanceHeal = function(newv)
	SetTmpParam_At(184, newv)
end
CLuaPlayerPropertyFight.RefreshParamSpeed = function()
	local ZuoQiSpeed = GetTmpParam_At(370)
	local OriSpeed = GetTmpParam_At(186)
	local AdjSpeed = GetTmpParam_At(187)
	local MulSpeed = GetTmpParam_At(188)

	local newv = (OriSpeed + AdjSpeed)*max(0.3,1+MulSpeed)+ZuoQiSpeed
	SetTmpParam_At(185, newv)
end
CLuaPlayerPropertyFight.SetParamOriSpeed = function(newv)
	SetTmpParam_At(186, newv)

	CLuaPlayerPropertyFight.RefreshParamSpeed()
end
CLuaPlayerPropertyFight.SetParamAdjSpeed = function(newv)
	SetTmpParam_At(187, newv)

	CLuaPlayerPropertyFight.RefreshParamSpeed()
end
CLuaPlayerPropertyFight.SetParamMulSpeed = function(newv)
	SetTmpParam_At(188, newv)

	CLuaPlayerPropertyFight.RefreshParamSpeed()
end
CLuaPlayerPropertyFight.RefreshParamMF = function()
	local EquipExchangeMF = GetParam_At(11)
	local AdjMF = GetTmpParam_At(190)

	local newv = 0.001*floor(1000*EquipExchangeMF + 1000*AdjMF)
	SetTmpParam_At(189, newv)
end
CLuaPlayerPropertyFight.SetParamAdjMF = function(newv)
	SetTmpParam_At(190, newv)

	CLuaPlayerPropertyFight.RefreshParamMF()
end
CLuaPlayerPropertyFight.RefreshParamRange = function()
	local AdjRange = GetTmpParam_At(192)

	local newv = AdjRange
	SetTmpParam_At(191, newv)
end
CLuaPlayerPropertyFight.SetParamAdjRange = function(newv)
	SetTmpParam_At(192, newv)

	CLuaPlayerPropertyFight.RefreshParamRange()
end
CLuaPlayerPropertyFight.RefreshParamHatePlus = function()
	local AdjHatePlus = GetTmpParam_At(194)

	local newv = AdjHatePlus
	SetTmpParam_At(193, newv)
end
CLuaPlayerPropertyFight.SetParamAdjHatePlus = function(newv)
	SetTmpParam_At(194, newv)

	CLuaPlayerPropertyFight.RefreshParamHatePlus()
end
CLuaPlayerPropertyFight.SetParamHateDecrease = function(newv)
	SetTmpParam_At(195, newv)
end
CLuaPlayerPropertyFight.RefreshParamInvisible = function()
	local AdjInvisible = GetTmpParam_At(197)

	local newv = max(0,AdjInvisible)
	SetTmpParam_At(196, newv)
end
CLuaPlayerPropertyFight.SetParamAdjInvisible = function(newv)
	SetTmpParam_At(197, newv)

	CLuaPlayerPropertyFight.RefreshParamInvisible()
end
CLuaPlayerPropertyFight.RefreshParamTrueSight = function()
	local OriTrueSight = GetTmpParam_At(199)
	local AdjTrueSight = GetTmpParam_At(200)

	local newv = OriTrueSight + AdjTrueSight
	SetTmpParam_At(198, newv)
end
CLuaPlayerPropertyFight.RefreshParamOriTrueSight = function()

	local newv = 65
	SetTmpParam_At(199, newv)
end
CLuaPlayerPropertyFight.SetParamAdjTrueSight = function(newv)
	SetTmpParam_At(200, newv)

	CLuaPlayerPropertyFight.RefreshParamTrueSight()
end
CLuaPlayerPropertyFight.RefreshParamEyeSight = function()
	local OriEyeSight = GetTmpParam_At(202)
	local AdjEyeSight = GetTmpParam_At(203)

	local newv = OriEyeSight + AdjEyeSight
	SetTmpParam_At(201, newv)
end
CLuaPlayerPropertyFight.SetParamOriEyeSight = function(newv)
	SetTmpParam_At(202, newv)

	CLuaPlayerPropertyFight.RefreshParamEyeSight()
end
CLuaPlayerPropertyFight.SetParamAdjEyeSight = function(newv)
	SetTmpParam_At(203, newv)

	CLuaPlayerPropertyFight.RefreshParamEyeSight()
end
CLuaPlayerPropertyFight.SetParamEnhanceTian = function(newv)
	SetTmpParam_At(204, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceTianMul = function(newv)
	SetTmpParam_At(205, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceEGui = function(newv)
	SetTmpParam_At(206, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceEGuiMul = function(newv)
	SetTmpParam_At(207, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceXiuLuo = function(newv)
	SetTmpParam_At(208, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceXiuLuoMul = function(newv)
	SetTmpParam_At(209, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceDiYu = function(newv)
	SetTmpParam_At(210, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceDiYuMul = function(newv)
	SetTmpParam_At(211, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceRen = function(newv)
	SetTmpParam_At(212, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceRenMul = function(newv)
	SetTmpParam_At(213, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceChuSheng = function(newv)
	SetTmpParam_At(214, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceChuShengMul = function(newv)
	SetTmpParam_At(215, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceBuilding = function(newv)
	SetTmpParam_At(216, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceBuildingMul = function(newv)
	SetTmpParam_At(217, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceBoss = function(newv)
	SetTmpParam_At(218, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceBossMul = function(newv)
	SetTmpParam_At(219, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceSheShou = function(newv)
	SetTmpParam_At(220, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceSheShouMul = function(newv)
	SetTmpParam_At(221, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceJiaShi = function(newv)
	SetTmpParam_At(222, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceJiaShiMul = function(newv)
	SetTmpParam_At(223, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceFangShi = function(newv)
	SetTmpParam_At(224, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceFangShiMul = function(newv)
	SetTmpParam_At(225, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceYiShi = function(newv)
	SetTmpParam_At(226, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceYiShiMul = function(newv)
	SetTmpParam_At(227, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceMeiZhe = function(newv)
	SetTmpParam_At(228, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceMeiZheMul = function(newv)
	SetTmpParam_At(229, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceYiRen = function(newv)
	SetTmpParam_At(230, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceYiRenMul = function(newv)
	SetTmpParam_At(231, newv)
end
CLuaPlayerPropertyFight.SetParamIgnorepDef = function(newv)
	SetTmpParam_At(232, newv)
end
CLuaPlayerPropertyFight.SetParamIgnoremDef = function(newv)
	SetTmpParam_At(233, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceZhaoHuan = function(newv)
	SetTmpParam_At(234, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceZhaoHuanMul = function(newv)
	SetTmpParam_At(235, newv)
end
CLuaPlayerPropertyFight.SetParamDizzyProbability = function(newv)
	SetTmpParam_At(236, newv)
end
CLuaPlayerPropertyFight.SetParamSleepProbability = function(newv)
	SetTmpParam_At(237, newv)
end
CLuaPlayerPropertyFight.SetParamChaosProbability = function(newv)
	SetTmpParam_At(238, newv)
end
CLuaPlayerPropertyFight.SetParamBindProbability = function(newv)
	SetTmpParam_At(239, newv)
end
CLuaPlayerPropertyFight.SetParamSilenceProbability = function(newv)
	SetTmpParam_At(240, newv)
end
CLuaPlayerPropertyFight.SetParamHpFullPow2 = function(newv)
	SetTmpParam_At(241, newv)
end
CLuaPlayerPropertyFight.SetParamHpFullPow1 = function(newv)
	SetTmpParam_At(242, newv)
end
CLuaPlayerPropertyFight.SetParamHpFullInit = function(newv)
	SetTmpParam_At(243, newv)
end
CLuaPlayerPropertyFight.SetParamMpFullPow1 = function(newv)
	SetTmpParam_At(244, newv)
end
CLuaPlayerPropertyFight.SetParamMpFullInit = function(newv)
	SetTmpParam_At(245, newv)
end
CLuaPlayerPropertyFight.SetParamMpRecoverPow1 = function(newv)
	SetTmpParam_At(246, newv)
end
CLuaPlayerPropertyFight.SetParamMpRecoverInit = function(newv)
	SetTmpParam_At(247, newv)
end
CLuaPlayerPropertyFight.SetParamPAttMinPow2 = function(newv)
	SetTmpParam_At(248, newv)
end
CLuaPlayerPropertyFight.SetParamPAttMinPow1 = function(newv)
	SetTmpParam_At(249, newv)
end
CLuaPlayerPropertyFight.SetParamPAttMinInit = function(newv)
	SetTmpParam_At(250, newv)
end
CLuaPlayerPropertyFight.SetParamPAttMaxPow2 = function(newv)
	SetTmpParam_At(251, newv)
end
CLuaPlayerPropertyFight.SetParamPAttMaxPow1 = function(newv)
	SetTmpParam_At(252, newv)
end
CLuaPlayerPropertyFight.SetParamPAttMaxInit = function(newv)
	SetTmpParam_At(253, newv)
end
CLuaPlayerPropertyFight.SetParamPhitPow1 = function(newv)
	SetTmpParam_At(254, newv)
end
CLuaPlayerPropertyFight.SetParamPHitInit = function(newv)
	SetTmpParam_At(255, newv)
end
CLuaPlayerPropertyFight.SetParamPMissPow1 = function(newv)
	SetTmpParam_At(256, newv)
end
CLuaPlayerPropertyFight.SetParamPMissInit = function(newv)
	SetTmpParam_At(257, newv)
end
CLuaPlayerPropertyFight.SetParamPSpeedPow1 = function(newv)
	SetTmpParam_At(258, newv)
end
CLuaPlayerPropertyFight.SetParamPSpeedInit = function(newv)
	SetTmpParam_At(259, newv)
end
CLuaPlayerPropertyFight.SetParamPDefPow1 = function(newv)
	SetTmpParam_At(260, newv)
end
CLuaPlayerPropertyFight.SetParamPDefInit = function(newv)
	SetTmpParam_At(261, newv)
end
CLuaPlayerPropertyFight.SetParamPfatalPow1 = function(newv)
	SetTmpParam_At(262, newv)
end
CLuaPlayerPropertyFight.SetParamPFatalInit = function(newv)
	SetTmpParam_At(263, newv)
end
CLuaPlayerPropertyFight.SetParamMAttMinPow2 = function(newv)
	SetTmpParam_At(264, newv)
end
CLuaPlayerPropertyFight.SetParamMAttMinPow1 = function(newv)
	SetTmpParam_At(265, newv)
end
CLuaPlayerPropertyFight.SetParamMAttMinInit = function(newv)
	SetTmpParam_At(266, newv)
end
CLuaPlayerPropertyFight.SetParamMAttMaxPow2 = function(newv)
	SetTmpParam_At(267, newv)
end
CLuaPlayerPropertyFight.SetParamMAttMaxPow1 = function(newv)
	SetTmpParam_At(268, newv)
end
CLuaPlayerPropertyFight.SetParamMAttMaxInit = function(newv)
	SetTmpParam_At(269, newv)
end
CLuaPlayerPropertyFight.SetParamMSpeedPow1 = function(newv)
	SetTmpParam_At(270, newv)
end
CLuaPlayerPropertyFight.SetParamMSpeedInit = function(newv)
	SetTmpParam_At(271, newv)
end
CLuaPlayerPropertyFight.SetParamMDefPow1 = function(newv)
	SetTmpParam_At(272, newv)
end
CLuaPlayerPropertyFight.SetParamMDefInit = function(newv)
	SetTmpParam_At(273, newv)
end
CLuaPlayerPropertyFight.SetParamMHitPow1 = function(newv)
	SetTmpParam_At(274, newv)
end
CLuaPlayerPropertyFight.SetParamMHitInit = function(newv)
	SetTmpParam_At(275, newv)
end
CLuaPlayerPropertyFight.SetParamMMissPow1 = function(newv)
	SetTmpParam_At(276, newv)
end
CLuaPlayerPropertyFight.SetParamMMissInit = function(newv)
	SetTmpParam_At(277, newv)
end
CLuaPlayerPropertyFight.SetParamMFatalPow1 = function(newv)
	SetTmpParam_At(278, newv)
end
CLuaPlayerPropertyFight.SetParamMFatalInit = function(newv)
	SetTmpParam_At(279, newv)
end
CLuaPlayerPropertyFight.SetParamBlockDamagePow1 = function(newv)
	SetTmpParam_At(280, newv)
end
CLuaPlayerPropertyFight.SetParamBlockDamageInit = function(newv)
	SetTmpParam_At(281, newv)
end
CLuaPlayerPropertyFight.SetParamRunSpeed = function(newv)
	SetTmpParam_At(282, newv)
end
CLuaPlayerPropertyFight.SetParamHpRecoverPow1 = function(newv)
	SetTmpParam_At(283, newv)
end
CLuaPlayerPropertyFight.SetParamHpRecoverInit = function(newv)
	SetTmpParam_At(284, newv)
end
CLuaPlayerPropertyFight.SetParamAntiBlockDamagePow1 = function(newv)
	SetTmpParam_At(285, newv)
end
CLuaPlayerPropertyFight.SetParamAntiBlockDamageInit = function(newv)
	SetTmpParam_At(286, newv)
end
CLuaPlayerPropertyFight.RefreshParamBBMax = function()
	local AdjBBMax = GetTmpParam_At(288)

	local newv = AdjBBMax
	SetTmpParam_At(287, newv)
end
CLuaPlayerPropertyFight.SetParamAdjBBMax = function(newv)
	SetTmpParam_At(288, newv)

	CLuaPlayerPropertyFight.RefreshParamBBMax()
end
CLuaPlayerPropertyFight.RefreshParamAntiBreak = function()
	local AdjAntiBreak = GetTmpParam_At(290)

	local newv = AdjAntiBreak
	SetTmpParam_At(289, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntiBreak = function(newv)
	SetTmpParam_At(290, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiBreak()
end
CLuaPlayerPropertyFight.RefreshParamAntiPushPull = function()
	local AdjAntiPushPull = GetTmpParam_At(292)

	local newv = AdjAntiPushPull
	SetTmpParam_At(291, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntiPushPull = function(newv)
	SetTmpParam_At(292, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiPushPull()
end
CLuaPlayerPropertyFight.RefreshParamAntiPetrify = function()
	local AdjAntiPetrify2 = GetTmpParam_At(451)
	local AdjAntiPetrify = GetTmpParam_At(294)
	local MulAntiPetrify = GetTmpParam_At(295)

	local newv = AdjAntiPetrify*(1+MulAntiPetrify)+AdjAntiPetrify2
	SetTmpParam_At(293, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntiPetrify = function(newv)
	SetTmpParam_At(294, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiPetrify()
end
CLuaPlayerPropertyFight.SetParamMulAntiPetrify = function(newv)
	SetTmpParam_At(295, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiPetrify()
end
CLuaPlayerPropertyFight.RefreshParamAntiTie = function()
	local AdjAntiTie2 = GetTmpParam_At(458)
	local AdjAntiTie = GetTmpParam_At(297)
	local MulAntiTie = GetTmpParam_At(298)

	local newv = AdjAntiTie*(1+MulAntiTie)+AdjAntiTie2
	SetTmpParam_At(296, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntiTie = function(newv)
	SetTmpParam_At(297, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiTie()
end
CLuaPlayerPropertyFight.SetParamMulAntiTie = function(newv)
	SetTmpParam_At(298, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiTie()
end
CLuaPlayerPropertyFight.RefreshParamEnhancePetrify = function()
	local AdjEnhancePetrify2 = GetTmpParam_At(473)
	local AdjEnhancePetrify = GetTmpParam_At(300)
	local MulEnhancePetrify = GetTmpParam_At(301)

	local newv = AdjEnhancePetrify*(1+MulEnhancePetrify)+AdjEnhancePetrify2
	SetTmpParam_At(299, newv)
end
CLuaPlayerPropertyFight.SetParamAdjEnhancePetrify = function(newv)
	SetTmpParam_At(300, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhancePetrify()
end
CLuaPlayerPropertyFight.SetParamMulEnhancePetrify = function(newv)
	SetTmpParam_At(301, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhancePetrify()
end
CLuaPlayerPropertyFight.RefreshParamEnhanceTie = function()
	local AdjEnhanceTie2 = GetTmpParam_At(478)
	local AdjEnhanceTie = GetTmpParam_At(303)
	local MulEnhanceTie = GetTmpParam_At(304)

	local newv = AdjEnhanceTie*(1+MulEnhanceTie)+AdjEnhanceTie2
	SetTmpParam_At(302, newv)
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceTie = function(newv)
	SetTmpParam_At(303, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceTie()
end
CLuaPlayerPropertyFight.SetParamMulEnhanceTie = function(newv)
	SetTmpParam_At(304, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceTie()
end
CLuaPlayerPropertyFight.SetParamTieProbability = function(newv)
	SetTmpParam_At(305, newv)
end
CLuaPlayerPropertyFight.SetParamStrZizhi = function(newv)
	SetTmpParam_At(306, newv)
end
CLuaPlayerPropertyFight.SetParamCorZizhi = function(newv)
	SetTmpParam_At(307, newv)
end
CLuaPlayerPropertyFight.SetParamStaZizhi = function(newv)
	SetTmpParam_At(308, newv)
end
CLuaPlayerPropertyFight.SetParamAgiZizhi = function(newv)
	SetTmpParam_At(309, newv)
end
CLuaPlayerPropertyFight.SetParamIntZizhi = function(newv)
	SetTmpParam_At(310, newv)
end
CLuaPlayerPropertyFight.SetParamInitStrZizhi = function(newv)
	SetTmpParam_At(311, newv)
end
CLuaPlayerPropertyFight.SetParamInitCorZizhi = function(newv)
	SetTmpParam_At(312, newv)
end
CLuaPlayerPropertyFight.SetParamInitStaZizhi = function(newv)
	SetTmpParam_At(313, newv)
end
CLuaPlayerPropertyFight.SetParamInitAgiZizhi = function(newv)
	SetTmpParam_At(314, newv)
end
CLuaPlayerPropertyFight.SetParamInitIntZizhi = function(newv)
	SetTmpParam_At(315, newv)
end
CLuaPlayerPropertyFight.SetParamWuxing = function(newv)
	SetTmpParam_At(316, newv)
end
CLuaPlayerPropertyFight.SetParamXiuwei = function(newv)
	SetTmpParam_At(317, newv)
end
CLuaPlayerPropertyFight.SetParamSkillNum = function(newv)
	SetTmpParam_At(318, newv)
end
CLuaPlayerPropertyFight.SetParamPType = function(newv)
	SetTmpParam_At(319, newv)
end
CLuaPlayerPropertyFight.SetParamMType = function(newv)
	SetTmpParam_At(320, newv)
end
CLuaPlayerPropertyFight.SetParamPHType = function(newv)
	SetTmpParam_At(321, newv)
end
CLuaPlayerPropertyFight.SetParamGrowFactor = function(newv)
	SetTmpParam_At(322, newv)
end
CLuaPlayerPropertyFight.SetParamWuxingImprove = function(newv)
	SetTmpParam_At(323, newv)
end
CLuaPlayerPropertyFight.SetParamXiuweiImprove = function(newv)
	SetTmpParam_At(324, newv)
end
CLuaPlayerPropertyFight.RefreshParamEnhanceBeHeal = function()
	local AdjEnhanceBeHeal = GetTmpParam_At(326)

	local newv = AdjEnhanceBeHeal
	SetTmpParam_At(325, newv)
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceBeHeal = function(newv)
	SetTmpParam_At(326, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceBeHeal()
end
CLuaPlayerPropertyFight.SetParamMulEnhanceBeHeal = function(newv)
	SetTmpParam_At(327, newv)
end
CLuaPlayerPropertyFight.RefreshParamLookUpCor = function()
	local Class = GetTmpParam_At(1)
	local Grade = GetTmpParam_At(2)

	local newv = LookUpGrow1(Class, "Cor", {Class, Grade}, 0)
	SetTmpParam_At(328, newv)
end
CLuaPlayerPropertyFight.RefreshParamLookUpSta = function()
	local Class = GetTmpParam_At(1)
	local Grade = GetTmpParam_At(2)

	local newv = LookUpGrow1(Class, "Sta", {Class, Grade}, 0)
	SetTmpParam_At(329, newv)
end
CLuaPlayerPropertyFight.RefreshParamLookUpStr = function()
	local Class = GetTmpParam_At(1)
	local Grade = GetTmpParam_At(2)

	local newv = LookUpGrow1(Class, "Str", {Class, Grade}, 0)
	SetTmpParam_At(330, newv)
end
CLuaPlayerPropertyFight.RefreshParamLookUpInt = function()
	local Class = GetTmpParam_At(1)
	local Grade = GetTmpParam_At(2)

	local newv = LookUpGrow1(Class, "Int", {Class, Grade}, 0)
	SetTmpParam_At(331, newv)
end
CLuaPlayerPropertyFight.RefreshParamLookUpAgi = function()
	local Class = GetTmpParam_At(1)
	local Grade = GetTmpParam_At(2)

	local newv = LookUpGrow1(Class, "Agi", {Class, Grade}, 0)
	SetTmpParam_At(332, newv)
end
CLuaPlayerPropertyFight.RefreshParamLookUpHpFull = function()
	local Class = GetTmpParam_At(1)

	local newv = LookUpGrow2(Class, "HpFull", {Class, "Cor"}, 0)
	SetTmpParam_At(333, newv)
end
CLuaPlayerPropertyFight.RefreshParamLookUpMpFull = function()
	local Class = GetTmpParam_At(1)

	local newv = LookUpGrow2(Class, "MpFull", {Class, "Sta"}, 0)
	SetTmpParam_At(334, newv)
end
CLuaPlayerPropertyFight.RefreshParamLookUppAttMin = function()
	local Class = GetTmpParam_At(1)

	local newv = LookUpGrow2(Class, "pAttMin", {Class, "Str"}, 0)
	SetTmpParam_At(335, newv)
end
CLuaPlayerPropertyFight.RefreshParamLookUppAttMax = function()
	local Class = GetTmpParam_At(1)

	local newv = LookUpGrow2(Class, "pAttMax", {Class, "Str"}, 0)
	SetTmpParam_At(336, newv)
end
CLuaPlayerPropertyFight.RefreshParamLookUppHit = function()
	local Class = GetTmpParam_At(1)

	local newv = LookUpGrow2(Class, "pHit", {Class, "Agi"}, 0)
	SetTmpParam_At(337, newv)
end
CLuaPlayerPropertyFight.RefreshParamLookUppMiss = function()
	local Class = GetTmpParam_At(1)

	local newv = LookUpGrow2(Class, "pMiss", {Class, "Agi"}, 0)
	SetTmpParam_At(338, newv)
end
CLuaPlayerPropertyFight.RefreshParamLookUppDef = function()
	local Class = GetTmpParam_At(1)

	local newv = LookUpGrow2(Class, "pDef", {Class, "Str"}, 0)
	SetTmpParam_At(339, newv)
end
CLuaPlayerPropertyFight.RefreshParamLookUpmAttMin = function()
	local Class = GetTmpParam_At(1)

	local newv = LookUpGrow2(Class, "mAttMin", {Class, "Int"}, 0)
	SetTmpParam_At(340, newv)
end
CLuaPlayerPropertyFight.RefreshParamLookUpmAttMax = function()
	local Class = GetTmpParam_At(1)

	local newv = LookUpGrow2(Class, "mAttMax", {Class, "Int"}, 0)
	SetTmpParam_At(341, newv)
end
CLuaPlayerPropertyFight.RefreshParamLookUpmHit = function()
	local Class = GetTmpParam_At(1)

	local newv = LookUpGrow2(Class, "mHit", {Class, "Agi"}, 0)
	SetTmpParam_At(342, newv)
end
CLuaPlayerPropertyFight.RefreshParamLookUpmMiss = function()
	local Class = GetTmpParam_At(1)

	local newv = LookUpGrow2(Class, "mMiss", {Class, "Agi"}, 0)
	SetTmpParam_At(343, newv)
end
CLuaPlayerPropertyFight.RefreshParamLookUpmDef = function()
	local Class = GetTmpParam_At(1)

	local newv = LookUpGrow2(Class, "mDef", {Class, "Int"}, 0)
	SetTmpParam_At(344, newv)
end
CLuaPlayerPropertyFight.SetParamAdjHpFull2 = function(newv)
	SetTmpParam_At(345, newv)

	CLuaPlayerPropertyFight.RefreshParamHpFull()
end
CLuaPlayerPropertyFight.SetParamAdjpAttMin2 = function(newv)
	SetTmpParam_At(346, newv)

	CLuaPlayerPropertyFight.RefreshParampAttMin()
end
CLuaPlayerPropertyFight.SetParamAdjpAttMax2 = function(newv)
	SetTmpParam_At(347, newv)

	CLuaPlayerPropertyFight.RefreshParampAttMax()

	CLuaPlayerPropertyFight.RefreshParampAttMin()
end
CLuaPlayerPropertyFight.SetParamAdjmAttMin2 = function(newv)
	SetTmpParam_At(348, newv)

	CLuaPlayerPropertyFight.RefreshParammAttMin()
end
CLuaPlayerPropertyFight.SetParamAdjmAttMax2 = function(newv)
	SetTmpParam_At(349, newv)

	CLuaPlayerPropertyFight.RefreshParammAttMin()

	CLuaPlayerPropertyFight.RefreshParammAttMax()
end
CLuaPlayerPropertyFight.SetParamLingShouType = function(newv)
	SetTmpParam_At(350, newv)
end
CLuaPlayerPropertyFight.SetParamMHType = function(newv)
	SetTmpParam_At(351, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceLingShou = function(newv)
	SetTmpParam_At(352, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceLingShouMul = function(newv)
	SetTmpParam_At(353, newv)
end
CLuaPlayerPropertyFight.RefreshParamAntiAoe = function()
	local AdjAntiAoe = GetTmpParam_At(355)

	local newv = AdjAntiAoe
	SetTmpParam_At(354, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntiAoe = function(newv)
	SetTmpParam_At(355, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiAoe()
end
CLuaPlayerPropertyFight.RefreshParamAntiBlind = function()
	local AdjAntiBlind2 = GetTmpParam_At(438)
	local AdjAntiBlind = GetTmpParam_At(357)
	local MulAntiBlind = GetTmpParam_At(358)

	local newv = AdjAntiBlind*(1+MulAntiBlind)+AdjAntiBlind2
	SetTmpParam_At(356, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntiBlind = function(newv)
	SetTmpParam_At(357, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiBlind()
end
CLuaPlayerPropertyFight.SetParamMulAntiBlind = function(newv)
	SetTmpParam_At(358, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiBlind()
end
CLuaPlayerPropertyFight.RefreshParamAntiDecelerate = function()
	local AdjAntiDecelerate2 = GetTmpParam_At(442)
	local AdjAntiDecelerate = GetTmpParam_At(360)
	local MulAntiDecelerate = GetTmpParam_At(361)

	local newv = AdjAntiDecelerate*(1+MulAntiDecelerate)+AdjAntiDecelerate2
	SetTmpParam_At(359, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntiDecelerate = function(newv)
	SetTmpParam_At(360, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiDecelerate()
end
CLuaPlayerPropertyFight.SetParamMulAntiDecelerate = function(newv)
	SetTmpParam_At(361, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiDecelerate()
end
CLuaPlayerPropertyFight.SetParamMinGrade = function(newv)
	SetTmpParam_At(362, newv)
end
CLuaPlayerPropertyFight.RefreshParamCharacterCor = function()
	local LookUpCor = GetTmpParam_At(328)
	local AdjustPermanentCor = GetTmpParam_At(611)

	local newv = LookUpCor+AdjustPermanentCor
	SetTmpParam_At(363, newv)
end
CLuaPlayerPropertyFight.RefreshParamCharacterSta = function()
	local LookUpSta = GetTmpParam_At(329)
	local AdjustPermanentSta = GetTmpParam_At(612)

	local newv = LookUpSta+AdjustPermanentSta
	SetTmpParam_At(364, newv)
end
CLuaPlayerPropertyFight.RefreshParamCharacterStr = function()
	local LookUpStr = GetTmpParam_At(330)
	local AdjustPermanentStr = GetTmpParam_At(613)

	local newv = LookUpStr+AdjustPermanentStr
	SetTmpParam_At(365, newv)
end
CLuaPlayerPropertyFight.RefreshParamCharacterInt = function()
	local LookUpInt = GetTmpParam_At(331)
	local AdjustPermanentInt = GetTmpParam_At(614)

	local newv = LookUpInt+AdjustPermanentInt
	SetTmpParam_At(366, newv)
end
CLuaPlayerPropertyFight.RefreshParamCharacterAgi = function()
	local LookUpAgi = GetTmpParam_At(332)
	local AdjustPermanentAgi = GetTmpParam_At(615)

	local newv = LookUpAgi+AdjustPermanentAgi
	SetTmpParam_At(367, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceDaoKe = function(newv)
	SetTmpParam_At(368, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceDaoKeMul = function(newv)
	SetTmpParam_At(369, newv)
end
CLuaPlayerPropertyFight.SetParamZuoQiSpeed = function(newv)
	SetTmpParam_At(370, newv)

	CLuaPlayerPropertyFight.RefreshParamSpeed()
end
CLuaPlayerPropertyFight.RefreshParamEnhanceBianHu = function()
	local AdjEnhanceBianHu2 = GetTmpParam_At(464)
	local AdjEnhanceBianHu = GetTmpParam_At(372)
	local MulEnhanceBianHu = GetTmpParam_At(373)

	local newv = AdjEnhanceBianHu*(1+MulEnhanceBianHu)+AdjEnhanceBianHu2
	SetTmpParam_At(371, newv)
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceBianHu = function(newv)
	SetTmpParam_At(372, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceBianHu()
end
CLuaPlayerPropertyFight.SetParamMulEnhanceBianHu = function(newv)
	SetTmpParam_At(373, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceBianHu()
end
CLuaPlayerPropertyFight.RefreshParamAntiBianHu = function()
	local AdjAntiBianHu2 = GetTmpParam_At(436)
	local AdjAntiBianHu = GetTmpParam_At(375)
	local MulAntiBianHu = GetTmpParam_At(376)

	local newv = AdjAntiBianHu*(1+MulAntiBianHu)+AdjAntiBianHu2
	SetTmpParam_At(374, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntiBianHu = function(newv)
	SetTmpParam_At(375, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiBianHu()
end
CLuaPlayerPropertyFight.SetParamMulAntiBianHu = function(newv)
	SetTmpParam_At(376, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiBianHu()
end
CLuaPlayerPropertyFight.SetParamEnhanceXiaKe = function(newv)
	SetTmpParam_At(377, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceXiaKeMul = function(newv)
	SetTmpParam_At(378, newv)
end
CLuaPlayerPropertyFight.SetParamTieTimeChange = function(newv)
	SetTmpParam_At(379, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceYanShi = function(newv)
	SetTmpParam_At(380, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceYanShiMul = function(newv)
	SetTmpParam_At(381, newv)
end
CLuaPlayerPropertyFight.SetParamPAType = function(newv)
	SetTmpParam_At(382, newv)
end
CLuaPlayerPropertyFight.SetParamMAType = function(newv)
	SetTmpParam_At(383, newv)
end
CLuaPlayerPropertyFight.SetParamlifetimeNoReduceRate = function(newv)
	SetTmpParam_At(384, newv)
end
CLuaPlayerPropertyFight.SetParamAddLSHpDurgImprove = function(newv)
	SetTmpParam_At(385, newv)
end
CLuaPlayerPropertyFight.SetParamAddHpDurgImprove = function(newv)
	SetTmpParam_At(386, newv)
end
CLuaPlayerPropertyFight.SetParamAddMpDurgImprove = function(newv)
	SetTmpParam_At(387, newv)
end
CLuaPlayerPropertyFight.SetParamFireHurtReduce = function(newv)
	SetTmpParam_At(388, newv)
end
CLuaPlayerPropertyFight.SetParamThunderHurtReduce = function(newv)
	SetTmpParam_At(389, newv)
end
CLuaPlayerPropertyFight.SetParamIceHurtReduce = function(newv)
	SetTmpParam_At(390, newv)
end
CLuaPlayerPropertyFight.SetParamPoisonHurtReduce = function(newv)
	SetTmpParam_At(391, newv)
end
CLuaPlayerPropertyFight.SetParamWindHurtReduce = function(newv)
	SetTmpParam_At(392, newv)
end
CLuaPlayerPropertyFight.SetParamLightHurtReduce = function(newv)
	SetTmpParam_At(393, newv)
end
CLuaPlayerPropertyFight.SetParamIllusionHurtReduce = function(newv)
	SetTmpParam_At(394, newv)
end
CLuaPlayerPropertyFight.SetParamFakepDef = function(newv)
	SetTmpParam_At(395, newv)
end
CLuaPlayerPropertyFight.SetParamFakemDef = function(newv)
	SetTmpParam_At(396, newv)
end
CLuaPlayerPropertyFight.RefreshParamEnhanceWater = function()
	local AdjEnhanceWater = GetTmpParam_At(398)
	local AdjEnhanceWater2 = GetTmpParam_At(479)

	local newv = AdjEnhanceWater+AdjEnhanceWater2
	SetTmpParam_At(397, newv)
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceWater = function(newv)
	SetTmpParam_At(398, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceWater()
end
CLuaPlayerPropertyFight.SetParamMulEnhanceWater = function(newv)
	SetTmpParam_At(399, newv)
end
CLuaPlayerPropertyFight.RefreshParamAntiWater = function()
	local AdjAntiWater2 = GetTmpParam_At(459)
	local AdjAntiWater = GetTmpParam_At(401)
	local MulAntiWater = GetTmpParam_At(402)

	local newv = AdjAntiWater*(1+MulAntiWater)+AdjAntiWater2
	SetTmpParam_At(400, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntiWater = function(newv)
	SetTmpParam_At(401, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiWater()
end
CLuaPlayerPropertyFight.SetParamMulAntiWater = function(newv)
	SetTmpParam_At(402, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiWater()
end
CLuaPlayerPropertyFight.SetParamWaterHurtReduce = function(newv)
	SetTmpParam_At(403, newv)
end
CLuaPlayerPropertyFight.RefreshParamIgnoreAntiWater = function()
	local AdjIgnoreAntiWater = GetTmpParam_At(431)
	local AdjIgnoreAntiWater2 = GetTmpParam_At(487)

	local newv = AdjIgnoreAntiWater+AdjIgnoreAntiWater2
	SetTmpParam_At(404, newv)
end
CLuaPlayerPropertyFight.RefreshParamAntiFreeze = function()
	local AdjAntiFreeze2 = GetTmpParam_At(445)
	local AdjAntiFreeze = GetTmpParam_At(406)
	local MulAntiFreeze = GetTmpParam_At(407)

	local newv = AdjAntiFreeze*(1+MulAntiFreeze)+AdjAntiFreeze2
	SetTmpParam_At(405, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAntiFreeze = function(newv)
	SetTmpParam_At(406, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiFreeze()
end
CLuaPlayerPropertyFight.SetParamMulAntiFreeze = function(newv)
	SetTmpParam_At(407, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiFreeze()
end
CLuaPlayerPropertyFight.RefreshParamEnhanceFreeze = function()
	local AdjEnhanceFreeze2 = GetTmpParam_At(469)
	local AdjEnhanceFreeze = GetTmpParam_At(409)
	local MulEnhanceFreeze = GetTmpParam_At(410)

	local newv = AdjEnhanceFreeze*(1+MulEnhanceFreeze)+AdjEnhanceFreeze2
	SetTmpParam_At(408, newv)
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceFreeze = function(newv)
	SetTmpParam_At(409, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceFreeze()
end
CLuaPlayerPropertyFight.SetParamMulEnhanceFreeze = function(newv)
	SetTmpParam_At(410, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceFreeze()
end
CLuaPlayerPropertyFight.SetParamEnhanceHuaHun = function(newv)
	SetTmpParam_At(411, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceHuaHunMul = function(newv)
	SetTmpParam_At(412, newv)
end
CLuaPlayerPropertyFight.SetParamTrueSightProb = function(newv)
	SetTmpParam_At(413, newv)
end
CLuaPlayerPropertyFight.SetParamLSBBGrade = function(newv)
	SetTmpParam_At(414, newv)
end
CLuaPlayerPropertyFight.SetParamLSBBQuality = function(newv)
	SetTmpParam_At(415, newv)
end
CLuaPlayerPropertyFight.SetParamLSBBAdjHp = function(newv)
	SetTmpParam_At(416, newv)
end
CLuaPlayerPropertyFight.SetParamAdjIgnoreAntiFire = function(newv)
	SetTmpParam_At(417, newv)

	CLuaPlayerPropertyFight.RefreshParamIgnoreAntiFire()
end
CLuaPlayerPropertyFight.SetParamAdjIgnoreAntiThunder = function(newv)
	SetTmpParam_At(418, newv)

	CLuaPlayerPropertyFight.RefreshParamIgnoreAntiThunder()
end
CLuaPlayerPropertyFight.SetParamAdjIgnoreAntiIce = function(newv)
	SetTmpParam_At(419, newv)

	CLuaPlayerPropertyFight.RefreshParamIgnoreAntiIce()
end
CLuaPlayerPropertyFight.SetParamAdjIgnoreAntiPoison = function(newv)
	SetTmpParam_At(420, newv)

	CLuaPlayerPropertyFight.RefreshParamIgnoreAntiPoison()
end
CLuaPlayerPropertyFight.SetParamAdjIgnoreAntiWind = function(newv)
	SetTmpParam_At(421, newv)

	CLuaPlayerPropertyFight.RefreshParamIgnoreAntiWind()
end
CLuaPlayerPropertyFight.SetParamAdjIgnoreAntiLight = function(newv)
	SetTmpParam_At(422, newv)

	CLuaPlayerPropertyFight.RefreshParamIgnoreAntiLight()
end
CLuaPlayerPropertyFight.SetParamAdjIgnoreAntiIllusion = function(newv)
	SetTmpParam_At(423, newv)

	CLuaPlayerPropertyFight.RefreshParamIgnoreAntiIllusion()
end
CLuaPlayerPropertyFight.SetParamIgnoreAntiFireLSMul = function(newv)
	SetTmpParam_At(424, newv)
end
CLuaPlayerPropertyFight.SetParamIgnoreAntiThunderLSMul = function(newv)
	SetTmpParam_At(425, newv)
end
CLuaPlayerPropertyFight.SetParamIgnoreAntiIceLSMul = function(newv)
	SetTmpParam_At(426, newv)
end
CLuaPlayerPropertyFight.SetParamIgnoreAntiPoisonLSMul = function(newv)
	SetTmpParam_At(427, newv)
end
CLuaPlayerPropertyFight.SetParamIgnoreAntiWindLSMul = function(newv)
	SetTmpParam_At(428, newv)
end
CLuaPlayerPropertyFight.SetParamIgnoreAntiLightLSMul = function(newv)
	SetTmpParam_At(429, newv)
end
CLuaPlayerPropertyFight.SetParamIgnoreAntiIllusionLSMul = function(newv)
	SetTmpParam_At(430, newv)
end
CLuaPlayerPropertyFight.SetParamAdjIgnoreAntiWater = function(newv)
	SetTmpParam_At(431, newv)

	CLuaPlayerPropertyFight.RefreshParamIgnoreAntiWater()
end
CLuaPlayerPropertyFight.SetParamIgnoreAntiWaterLSMul = function(newv)
	SetTmpParam_At(432, newv)
end
CLuaPlayerPropertyFight.SetParamMulSpeed2 = function(newv)
	SetTmpParam_At(433, newv)
end
CLuaPlayerPropertyFight.SetParamMulConsumeMp = function(newv)
	SetTmpParam_At(434, newv)
end
CLuaPlayerPropertyFight.SetParamAdjAgi2 = function(newv)
	SetTmpParam_At(435, newv)

	CLuaPlayerPropertyFight.RefreshParamAgi()

	CLuaPlayerPropertyFight.RefreshParammMiss()

	CLuaPlayerPropertyFight.RefreshParammHit()

	CLuaPlayerPropertyFight.RefreshParampMiss()

	CLuaPlayerPropertyFight.RefreshParampHit()
end
CLuaPlayerPropertyFight.SetParamAdjAntiBianHu2 = function(newv)
	SetTmpParam_At(436, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiBianHu()
end
CLuaPlayerPropertyFight.SetParamAdjAntiBind2 = function(newv)
	SetTmpParam_At(437, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiBind()
end
CLuaPlayerPropertyFight.SetParamAdjAntiBlind2 = function(newv)
	SetTmpParam_At(438, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiBlind()
end
CLuaPlayerPropertyFight.SetParamAdjAntiBlock2 = function(newv)
	SetTmpParam_At(439, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiBlock()
end
CLuaPlayerPropertyFight.SetParamAdjAntiBlockDamage2 = function(newv)
	SetTmpParam_At(440, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiBlockDamage()
end
CLuaPlayerPropertyFight.SetParamAdjAntiChaos2 = function(newv)
	SetTmpParam_At(441, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiChaos()
end
CLuaPlayerPropertyFight.SetParamAdjAntiDecelerate2 = function(newv)
	SetTmpParam_At(442, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiDecelerate()
end
CLuaPlayerPropertyFight.SetParamAdjAntiDizzy2 = function(newv)
	SetTmpParam_At(443, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiDizzy()
end
CLuaPlayerPropertyFight.SetParamAdjAntiFire2 = function(newv)
	SetTmpParam_At(444, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiFire()
end
CLuaPlayerPropertyFight.SetParamAdjAntiFreeze2 = function(newv)
	SetTmpParam_At(445, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiFreeze()
end
CLuaPlayerPropertyFight.SetParamAdjAntiIce2 = function(newv)
	SetTmpParam_At(446, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiIce()
end
CLuaPlayerPropertyFight.SetParamAdjAntiIllusion2 = function(newv)
	SetTmpParam_At(447, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiIllusion()
end
CLuaPlayerPropertyFight.SetParamAdjAntiLight2 = function(newv)
	SetTmpParam_At(448, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiLight()
end
CLuaPlayerPropertyFight.SetParamAdjAntimFatal2 = function(newv)
	SetTmpParam_At(449, newv)

	CLuaPlayerPropertyFight.RefreshParamAntimFatal()
end
CLuaPlayerPropertyFight.SetParamAdjAntimFatalDamage2 = function(newv)
	SetTmpParam_At(450, newv)

	CLuaPlayerPropertyFight.RefreshParamAntimFatalDamage()
end
CLuaPlayerPropertyFight.SetParamAdjAntiPetrify2 = function(newv)
	SetTmpParam_At(451, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiPetrify()
end
CLuaPlayerPropertyFight.SetParamAdjAntipFatal2 = function(newv)
	SetTmpParam_At(452, newv)

	CLuaPlayerPropertyFight.RefreshParamAntipFatal()
end
CLuaPlayerPropertyFight.SetParamAdjAntipFatalDamage2 = function(newv)
	SetTmpParam_At(453, newv)

	CLuaPlayerPropertyFight.RefreshParamAntipFatalDamage()
end
CLuaPlayerPropertyFight.SetParamAdjAntiPoison2 = function(newv)
	SetTmpParam_At(454, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiPoison()
end
CLuaPlayerPropertyFight.SetParamAdjAntiSilence2 = function(newv)
	SetTmpParam_At(455, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiSilence()
end
CLuaPlayerPropertyFight.SetParamAdjAntiSleep2 = function(newv)
	SetTmpParam_At(456, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiSleep()
end
CLuaPlayerPropertyFight.SetParamAdjAntiThunder2 = function(newv)
	SetTmpParam_At(457, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiThunder()
end
CLuaPlayerPropertyFight.SetParamAdjAntiTie2 = function(newv)
	SetTmpParam_At(458, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiTie()
end
CLuaPlayerPropertyFight.SetParamAdjAntiWater2 = function(newv)
	SetTmpParam_At(459, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiWater()
end
CLuaPlayerPropertyFight.SetParamAdjAntiWind2 = function(newv)
	SetTmpParam_At(460, newv)

	CLuaPlayerPropertyFight.RefreshParamAntiWind()
end
CLuaPlayerPropertyFight.SetParamAdjBlock2 = function(newv)
	SetTmpParam_At(461, newv)

	CLuaPlayerPropertyFight.RefreshParamBlock()
end
CLuaPlayerPropertyFight.SetParamAdjBlockDamage2 = function(newv)
	SetTmpParam_At(462, newv)

	CLuaPlayerPropertyFight.RefreshParamBlockDamage()
end
CLuaPlayerPropertyFight.SetParamAdjCor2 = function(newv)
	SetTmpParam_At(463, newv)

	CLuaPlayerPropertyFight.RefreshParamCor()

	CLuaPlayerPropertyFight.RefreshParamHpFull()
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceBianHu2 = function(newv)
	SetTmpParam_At(464, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceBianHu()
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceBind2 = function(newv)
	SetTmpParam_At(465, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceBind()
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceChaos2 = function(newv)
	SetTmpParam_At(466, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceChaos()
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceDizzy2 = function(newv)
	SetTmpParam_At(467, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceDizzy()
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceFire2 = function(newv)
	SetTmpParam_At(468, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceFire()
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceFreeze2 = function(newv)
	SetTmpParam_At(469, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceFreeze()
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceIce2 = function(newv)
	SetTmpParam_At(470, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceIce()
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceIllusion2 = function(newv)
	SetTmpParam_At(471, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceIllusion()
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceLight2 = function(newv)
	SetTmpParam_At(472, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceLight()
end
CLuaPlayerPropertyFight.SetParamAdjEnhancePetrify2 = function(newv)
	SetTmpParam_At(473, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhancePetrify()
end
CLuaPlayerPropertyFight.SetParamAdjEnhancePoison2 = function(newv)
	SetTmpParam_At(474, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhancePoison()
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceSilence2 = function(newv)
	SetTmpParam_At(475, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceSilence()
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceSleep2 = function(newv)
	SetTmpParam_At(476, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceSleep()
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceThunder2 = function(newv)
	SetTmpParam_At(477, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceThunder()
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceTie2 = function(newv)
	SetTmpParam_At(478, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceTie()
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceWater2 = function(newv)
	SetTmpParam_At(479, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceWater()
end
CLuaPlayerPropertyFight.SetParamAdjEnhanceWind2 = function(newv)
	SetTmpParam_At(480, newv)

	CLuaPlayerPropertyFight.RefreshParamEnhanceWind()
end
CLuaPlayerPropertyFight.SetParamAdjIgnoreAntiFire2 = function(newv)
	SetTmpParam_At(481, newv)

	CLuaPlayerPropertyFight.RefreshParamIgnoreAntiFire()
end
CLuaPlayerPropertyFight.SetParamAdjIgnoreAntiIce2 = function(newv)
	SetTmpParam_At(482, newv)

	CLuaPlayerPropertyFight.RefreshParamIgnoreAntiIce()
end
CLuaPlayerPropertyFight.SetParamAdjIgnoreAntiIllusion2 = function(newv)
	SetTmpParam_At(483, newv)

	CLuaPlayerPropertyFight.RefreshParamIgnoreAntiIllusion()
end
CLuaPlayerPropertyFight.SetParamAdjIgnoreAntiLight2 = function(newv)
	SetTmpParam_At(484, newv)

	CLuaPlayerPropertyFight.RefreshParamIgnoreAntiLight()
end
CLuaPlayerPropertyFight.SetParamAdjIgnoreAntiPoison2 = function(newv)
	SetTmpParam_At(485, newv)

	CLuaPlayerPropertyFight.RefreshParamIgnoreAntiPoison()
end
CLuaPlayerPropertyFight.SetParamAdjIgnoreAntiThunder2 = function(newv)
	SetTmpParam_At(486, newv)

	CLuaPlayerPropertyFight.RefreshParamIgnoreAntiThunder()
end
CLuaPlayerPropertyFight.SetParamAdjIgnoreAntiWater2 = function(newv)
	SetTmpParam_At(487, newv)

	CLuaPlayerPropertyFight.RefreshParamIgnoreAntiWater()
end
CLuaPlayerPropertyFight.SetParamAdjIgnoreAntiWind2 = function(newv)
	SetTmpParam_At(488, newv)

	CLuaPlayerPropertyFight.RefreshParamIgnoreAntiWind()
end
CLuaPlayerPropertyFight.SetParamAdjInt2 = function(newv)
	SetTmpParam_At(489, newv)

	CLuaPlayerPropertyFight.RefreshParamInt()

	CLuaPlayerPropertyFight.RefreshParammAttMin()

	CLuaPlayerPropertyFight.RefreshParammAttMax()

	CLuaPlayerPropertyFight.RefreshParammDef()
end
CLuaPlayerPropertyFight.SetParamAdjmDef2 = function(newv)
	SetTmpParam_At(490, newv)

	CLuaPlayerPropertyFight.RefreshParammDef()
end
CLuaPlayerPropertyFight.SetParamAdjmFatal2 = function(newv)
	SetTmpParam_At(491, newv)

	CLuaPlayerPropertyFight.RefreshParammFatal()
end
CLuaPlayerPropertyFight.SetParamAdjmFatalDamage2 = function(newv)
	SetTmpParam_At(492, newv)

	CLuaPlayerPropertyFight.RefreshParammFatalDamage()
end
CLuaPlayerPropertyFight.SetParamAdjmHit2 = function(newv)
	SetTmpParam_At(493, newv)

	CLuaPlayerPropertyFight.RefreshParammHit()
end
CLuaPlayerPropertyFight.SetParamAdjmHurt2 = function(newv)
	SetTmpParam_At(494, newv)
end
CLuaPlayerPropertyFight.SetParamAdjmMiss2 = function(newv)
	SetTmpParam_At(495, newv)

	CLuaPlayerPropertyFight.RefreshParammMiss()
end
CLuaPlayerPropertyFight.SetParamAdjmSpeed2 = function(newv)
	SetTmpParam_At(496, newv)
end
CLuaPlayerPropertyFight.SetParamAdjpDef2 = function(newv)
	SetTmpParam_At(497, newv)

	CLuaPlayerPropertyFight.RefreshParampDef()
end
CLuaPlayerPropertyFight.SetParamAdjpFatal2 = function(newv)
	SetTmpParam_At(498, newv)

	CLuaPlayerPropertyFight.RefreshParampFatal()
end
CLuaPlayerPropertyFight.SetParamAdjpFatalDamage2 = function(newv)
	SetTmpParam_At(499, newv)

	CLuaPlayerPropertyFight.RefreshParampFatalDamage()
end
CLuaPlayerPropertyFight.SetParamAdjpHit2 = function(newv)
	SetTmpParam_At(500, newv)

	CLuaPlayerPropertyFight.RefreshParampHit()
end
CLuaPlayerPropertyFight.SetParamAdjpHurt2 = function(newv)
	SetTmpParam_At(501, newv)
end
CLuaPlayerPropertyFight.SetParamAdjpMiss2 = function(newv)
	SetTmpParam_At(502, newv)

	CLuaPlayerPropertyFight.RefreshParampMiss()
end
CLuaPlayerPropertyFight.SetParamAdjpSpeed2 = function(newv)
	SetTmpParam_At(503, newv)
end
CLuaPlayerPropertyFight.SetParamAdjStr2 = function(newv)
	SetTmpParam_At(504, newv)

	CLuaPlayerPropertyFight.RefreshParamStr()

	CLuaPlayerPropertyFight.RefreshParampAttMax()

	CLuaPlayerPropertyFight.RefreshParampDef()

	CLuaPlayerPropertyFight.RefreshParampAttMin()
end
CLuaPlayerPropertyFight.RefreshParamLSpAttMin = function()
	local LSpAttMin_adj = GetTmpParam_At(523)
	local LSpAttMin_jc = GetTmpParam_At(541)

	local newv = LSpAttMin_adj+LSpAttMin_jc
	SetTmpParam_At(505, newv)
end
CLuaPlayerPropertyFight.RefreshParamLSpAttMax = function()
	local LSpAttMax_adj = GetTmpParam_At(524)
	local LSpAttMax_jc = GetTmpParam_At(542)

	local newv = LSpAttMax_adj+LSpAttMax_jc
	SetTmpParam_At(506, newv)
end
CLuaPlayerPropertyFight.RefreshParamLSmAttMin = function()
	local LSmAttMin_adj = GetTmpParam_At(525)
	local LSmAttMin_jc = GetTmpParam_At(543)

	local newv = LSmAttMin_adj+LSmAttMin_jc
	SetTmpParam_At(507, newv)
end
CLuaPlayerPropertyFight.RefreshParamLSmAttMax = function()
	local LSmAttMax_adj = GetTmpParam_At(526)
	local LSmAttMax_jc = GetTmpParam_At(544)

	local newv = LSmAttMax_adj+LSmAttMax_jc
	SetTmpParam_At(508, newv)
end
CLuaPlayerPropertyFight.RefreshParamLSpFatal = function()
	local LSpFatal_adj = GetTmpParam_At(527)
	local LSpFatal_jc = GetTmpParam_At(545)

	local newv = LSpFatal_adj+LSpFatal_jc
	SetTmpParam_At(509, newv)
end
CLuaPlayerPropertyFight.RefreshParamLSpFatalDamage = function()
	local LSpFatalDamage_adj = GetTmpParam_At(528)
	local LSpFatalDamage_jc = GetTmpParam_At(546)

	local newv = LSpFatalDamage_adj+LSpFatalDamage_jc
	SetTmpParam_At(510, newv)
end
CLuaPlayerPropertyFight.RefreshParamLSmFatal = function()
	local LSmFatal_adj = GetTmpParam_At(529)
	local LSmFatal_jc = GetTmpParam_At(547)

	local newv = LSmFatal_adj+LSmFatal_jc
	SetTmpParam_At(511, newv)
end
CLuaPlayerPropertyFight.RefreshParamLSmFatalDamage = function()
	local LSmFatalDamage_adj = GetTmpParam_At(530)
	local LSmFatalDamage_jc = GetTmpParam_At(548)

	local newv = LSmFatalDamage_adj+LSmFatalDamage_jc
	SetTmpParam_At(512, newv)
end
CLuaPlayerPropertyFight.RefreshParamLSAntiBlock = function()
	local LSAntiBlock_adj = GetTmpParam_At(531)
	local LSAntiBlock_jc = GetTmpParam_At(549)

	local newv = LSAntiBlock_adj+LSAntiBlock_jc
	SetTmpParam_At(513, newv)
end
CLuaPlayerPropertyFight.RefreshParamLSAntiBlockDamage = function()
	local LSAntiBlockDamage_adj = GetTmpParam_At(532)
	local LSAntiBlockDamage_jc = GetTmpParam_At(550)

	local newv = LSAntiBlockDamage_adj+LSAntiBlockDamage_jc
	SetTmpParam_At(514, newv)
end
CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiFire = function()
	local LSIgnoreAntiFire_adj = GetTmpParam_At(533)
	local LSIgnoreAntiFire_jc = GetTmpParam_At(551)
	local LSIgnoreAntiFire_mul = GetTmpParam_At(559)

	local newv = (LSIgnoreAntiFire_adj+LSIgnoreAntiFire_jc)*(1+LSIgnoreAntiFire_mul)
	SetTmpParam_At(515, newv)
end
CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiThunder = function()
	local LSIgnoreAntiThunder_adj = GetTmpParam_At(534)
	local LSIgnoreAntiThunder_jc = GetTmpParam_At(552)
	local LSIgnoreAntiThunder_mul = GetTmpParam_At(560)

	local newv = (LSIgnoreAntiThunder_adj+LSIgnoreAntiThunder_jc)*(1+LSIgnoreAntiThunder_mul)
	SetTmpParam_At(516, newv)
end
CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiIce = function()
	local LSIgnoreAntiIce_adj = GetTmpParam_At(535)
	local LSIgnoreAntiIce_jc = GetTmpParam_At(553)
	local LSIgnoreAntiIce_mul = GetTmpParam_At(561)

	local newv = (LSIgnoreAntiIce_adj+LSIgnoreAntiIce_jc)*(1+LSIgnoreAntiIce_mul)
	SetTmpParam_At(517, newv)
end
CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiPoison = function()
	local LSIgnoreAntiPoison_adj = GetTmpParam_At(536)
	local LSIgnoreAntiPoison_jc = GetTmpParam_At(554)
	local LSIgnoreAntiPoison_mul = GetTmpParam_At(562)

	local newv = (LSIgnoreAntiPoison_adj+LSIgnoreAntiPoison_jc)*(1+LSIgnoreAntiPoison_mul)
	SetTmpParam_At(518, newv)
end
CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiWind = function()
	local LSIgnoreAntiWind_adj = GetTmpParam_At(537)
	local LSIgnoreAntiWind_jc = GetTmpParam_At(555)
	local LSIgnoreAntiWind_mul = GetTmpParam_At(563)

	local newv = (LSIgnoreAntiWind_adj+LSIgnoreAntiWind_jc)*(1+LSIgnoreAntiWind_mul)
	SetTmpParam_At(519, newv)
end
CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiLight = function()
	local LSIgnoreAntiLight_adj = GetTmpParam_At(538)
	local LSIgnoreAntiLight_jc = GetTmpParam_At(556)
	local LSIgnoreAntiLight_mul = GetTmpParam_At(564)

	local newv = (LSIgnoreAntiLight_adj+LSIgnoreAntiLight_jc)*(1+LSIgnoreAntiLight_mul)
	SetTmpParam_At(520, newv)
end
CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiIllusion = function()
	local LSIgnoreAntiIllusion_adj = GetTmpParam_At(539)
	local LSIgnoreAntiIllusion_jc = GetTmpParam_At(557)
	local LSIgnoreAntiIllusion_mul = GetTmpParam_At(565)

	local newv = (LSIgnoreAntiIllusion_adj+LSIgnoreAntiIllusion_jc)*(1+LSIgnoreAntiIllusion_mul)
	SetTmpParam_At(521, newv)
end
CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiWater = function()
	local LSIgnoreAntiWater_adj = GetTmpParam_At(540)
	local LSIgnoreAntiWater_jc = GetTmpParam_At(558)
	local LSIgnoreAntiWater_mul = GetTmpParam_At(566)

	local newv = (LSIgnoreAntiWater_adj+LSIgnoreAntiWater_jc)*(1+LSIgnoreAntiWater_mul)
	SetTmpParam_At(522, newv)
end
CLuaPlayerPropertyFight.SetParamLSpAttMin_adj = function(newv)
	SetTmpParam_At(523, newv)

	CLuaPlayerPropertyFight.RefreshParamLSpAttMin()
end
CLuaPlayerPropertyFight.SetParamLSpAttMax_adj = function(newv)
	SetTmpParam_At(524, newv)

	CLuaPlayerPropertyFight.RefreshParamLSpAttMax()
end
CLuaPlayerPropertyFight.SetParamLSmAttMin_adj = function(newv)
	SetTmpParam_At(525, newv)

	CLuaPlayerPropertyFight.RefreshParamLSmAttMin()
end
CLuaPlayerPropertyFight.SetParamLSmAttMax_adj = function(newv)
	SetTmpParam_At(526, newv)

	CLuaPlayerPropertyFight.RefreshParamLSmAttMax()
end
CLuaPlayerPropertyFight.SetParamLSpFatal_adj = function(newv)
	SetTmpParam_At(527, newv)

	CLuaPlayerPropertyFight.RefreshParamLSpFatal()
end
CLuaPlayerPropertyFight.SetParamLSpFatalDamage_adj = function(newv)
	SetTmpParam_At(528, newv)

	CLuaPlayerPropertyFight.RefreshParamLSpFatalDamage()
end
CLuaPlayerPropertyFight.SetParamLSmFatal_adj = function(newv)
	SetTmpParam_At(529, newv)

	CLuaPlayerPropertyFight.RefreshParamLSmFatal()
end
CLuaPlayerPropertyFight.SetParamLSmFatalDamage_adj = function(newv)
	SetTmpParam_At(530, newv)

	CLuaPlayerPropertyFight.RefreshParamLSmFatalDamage()
end
CLuaPlayerPropertyFight.SetParamLSAntiBlock_adj = function(newv)
	SetTmpParam_At(531, newv)

	CLuaPlayerPropertyFight.RefreshParamLSAntiBlock()
end
CLuaPlayerPropertyFight.SetParamLSAntiBlockDamage_adj = function(newv)
	SetTmpParam_At(532, newv)

	CLuaPlayerPropertyFight.RefreshParamLSAntiBlockDamage()
end
CLuaPlayerPropertyFight.SetParamLSIgnoreAntiFire_adj = function(newv)
	SetTmpParam_At(533, newv)

	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiFire()
end
CLuaPlayerPropertyFight.SetParamLSIgnoreAntiThunder_adj = function(newv)
	SetTmpParam_At(534, newv)

	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiThunder()
end
CLuaPlayerPropertyFight.SetParamLSIgnoreAntiIce_adj = function(newv)
	SetTmpParam_At(535, newv)

	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiIce()
end
CLuaPlayerPropertyFight.SetParamLSIgnoreAntiPoison_adj = function(newv)
	SetTmpParam_At(536, newv)

	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiPoison()
end
CLuaPlayerPropertyFight.SetParamLSIgnoreAntiWind_adj = function(newv)
	SetTmpParam_At(537, newv)

	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiWind()
end
CLuaPlayerPropertyFight.SetParamLSIgnoreAntiLight_adj = function(newv)
	SetTmpParam_At(538, newv)

	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiLight()
end
CLuaPlayerPropertyFight.SetParamLSIgnoreAntiIllusion_adj = function(newv)
	SetTmpParam_At(539, newv)

	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiIllusion()
end
CLuaPlayerPropertyFight.SetParamLSIgnoreAntiWater_adj = function(newv)
	SetTmpParam_At(540, newv)

	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiWater()
end
CLuaPlayerPropertyFight.SetParamLSpAttMin_jc = function(newv)
	SetTmpParam_At(541, newv)

	CLuaPlayerPropertyFight.RefreshParamLSpAttMin()
end
CLuaPlayerPropertyFight.SetParamLSpAttMax_jc = function(newv)
	SetTmpParam_At(542, newv)

	CLuaPlayerPropertyFight.RefreshParamLSpAttMax()
end
CLuaPlayerPropertyFight.SetParamLSmAttMin_jc = function(newv)
	SetTmpParam_At(543, newv)

	CLuaPlayerPropertyFight.RefreshParamLSmAttMin()
end
CLuaPlayerPropertyFight.SetParamLSmAttMax_jc = function(newv)
	SetTmpParam_At(544, newv)

	CLuaPlayerPropertyFight.RefreshParamLSmAttMax()
end
CLuaPlayerPropertyFight.SetParamLSpFatal_jc = function(newv)
	SetTmpParam_At(545, newv)

	CLuaPlayerPropertyFight.RefreshParamLSpFatal()
end
CLuaPlayerPropertyFight.SetParamLSpFatalDamage_jc = function(newv)
	SetTmpParam_At(546, newv)

	CLuaPlayerPropertyFight.RefreshParamLSpFatalDamage()
end
CLuaPlayerPropertyFight.SetParamLSmFatal_jc = function(newv)
	SetTmpParam_At(547, newv)

	CLuaPlayerPropertyFight.RefreshParamLSmFatal()
end
CLuaPlayerPropertyFight.SetParamLSmFatalDamage_jc = function(newv)
	SetTmpParam_At(548, newv)

	CLuaPlayerPropertyFight.RefreshParamLSmFatalDamage()
end
CLuaPlayerPropertyFight.SetParamLSAntiBlock_jc = function(newv)
	SetTmpParam_At(549, newv)

	CLuaPlayerPropertyFight.RefreshParamLSAntiBlock()
end
CLuaPlayerPropertyFight.SetParamLSAntiBlockDamage_jc = function(newv)
	SetTmpParam_At(550, newv)

	CLuaPlayerPropertyFight.RefreshParamLSAntiBlockDamage()
end
CLuaPlayerPropertyFight.SetParamLSIgnoreAntiFire_jc = function(newv)
	SetTmpParam_At(551, newv)

	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiFire()
end
CLuaPlayerPropertyFight.SetParamLSIgnoreAntiThunder_jc = function(newv)
	SetTmpParam_At(552, newv)

	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiThunder()
end
CLuaPlayerPropertyFight.SetParamLSIgnoreAntiIce_jc = function(newv)
	SetTmpParam_At(553, newv)

	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiIce()
end
CLuaPlayerPropertyFight.SetParamLSIgnoreAntiPoison_jc = function(newv)
	SetTmpParam_At(554, newv)

	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiPoison()
end
CLuaPlayerPropertyFight.SetParamLSIgnoreAntiWind_jc = function(newv)
	SetTmpParam_At(555, newv)

	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiWind()
end
CLuaPlayerPropertyFight.SetParamLSIgnoreAntiLight_jc = function(newv)
	SetTmpParam_At(556, newv)

	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiLight()
end
CLuaPlayerPropertyFight.SetParamLSIgnoreAntiIllusion_jc = function(newv)
	SetTmpParam_At(557, newv)

	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiIllusion()
end
CLuaPlayerPropertyFight.SetParamLSIgnoreAntiWater_jc = function(newv)
	SetTmpParam_At(558, newv)

	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiWater()
end
CLuaPlayerPropertyFight.SetParamLSIgnoreAntiFire_mul = function(newv)
	SetTmpParam_At(559, newv)

	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiFire()
end
CLuaPlayerPropertyFight.SetParamLSIgnoreAntiThunder_mul = function(newv)
	SetTmpParam_At(560, newv)

	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiThunder()
end
CLuaPlayerPropertyFight.SetParamLSIgnoreAntiIce_mul = function(newv)
	SetTmpParam_At(561, newv)

	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiIce()
end
CLuaPlayerPropertyFight.SetParamLSIgnoreAntiPoison_mul = function(newv)
	SetTmpParam_At(562, newv)

	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiPoison()
end
CLuaPlayerPropertyFight.SetParamLSIgnoreAntiWind_mul = function(newv)
	SetTmpParam_At(563, newv)

	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiWind()
end
CLuaPlayerPropertyFight.SetParamLSIgnoreAntiLight_mul = function(newv)
	SetTmpParam_At(564, newv)

	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiLight()
end
CLuaPlayerPropertyFight.SetParamLSIgnoreAntiIllusion_mul = function(newv)
	SetTmpParam_At(565, newv)

	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiIllusion()
end
CLuaPlayerPropertyFight.SetParamLSIgnoreAntiWater_mul = function(newv)
	SetTmpParam_At(566, newv)

	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiWater()
end
CLuaPlayerPropertyFight.SetParamJieBan_Child_QiChangColor = function(newv)
	SetTmpParam_At(567, newv)
end
CLuaPlayerPropertyFight.SetParamJieBan_LingShou_Ratio = function(newv)
	SetTmpParam_At(568, newv)
end
CLuaPlayerPropertyFight.SetParamJieBan_LingShou_QinMi = function(newv)
	SetTmpParam_At(569, newv)
end
CLuaPlayerPropertyFight.SetParamJieBan_LingShou_CorZizhi = function(newv)
	SetTmpParam_At(570, newv)
end
CLuaPlayerPropertyFight.SetParamJieBan_LingShou_StaZizhi = function(newv)
	SetTmpParam_At(571, newv)
end
CLuaPlayerPropertyFight.SetParamJieBan_LingShou_StrZizhi = function(newv)
	SetTmpParam_At(572, newv)
end
CLuaPlayerPropertyFight.SetParamJieBan_LingShou_IntZizhi = function(newv)
	SetTmpParam_At(573, newv)
end
CLuaPlayerPropertyFight.SetParamJieBan_LingShou_AgiZizhi = function(newv)
	SetTmpParam_At(574, newv)
end
CLuaPlayerPropertyFight.SetParamBuff_DisplayValue1 = function(newv)
	SetTmpParam_At(575, newv)
end
CLuaPlayerPropertyFight.SetParamBuff_DisplayValue2 = function(newv)
	SetTmpParam_At(576, newv)
end
CLuaPlayerPropertyFight.SetParamBuff_DisplayValue3 = function(newv)
	SetTmpParam_At(577, newv)
end
CLuaPlayerPropertyFight.SetParamBuff_DisplayValue4 = function(newv)
	SetTmpParam_At(578, newv)
end
CLuaPlayerPropertyFight.SetParamBuff_DisplayValue5 = function(newv)
	SetTmpParam_At(579, newv)
end
CLuaPlayerPropertyFight.SetParamLSNature = function(newv)
	SetTmpParam_At(580, newv)
end
CLuaPlayerPropertyFight.SetParamPrayMulti = function(newv)
	SetTmpParam_At(581, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceYingLing = function(newv)
	SetTmpParam_At(582, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceYingLingMul = function(newv)
	SetTmpParam_At(583, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceFlag = function(newv)
	SetTmpParam_At(584, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceFlagMul = function(newv)
	SetTmpParam_At(585, newv)
end
CLuaPlayerPropertyFight.SetParamObjectType = function(newv)
	SetTmpParam_At(586, newv)
end
CLuaPlayerPropertyFight.SetParamMonsterType = function(newv)
	SetTmpParam_At(587, newv)
end
CLuaPlayerPropertyFight.SetParamFightPropType = function(newv)
	SetTmpParam_At(588, newv)
end
CLuaPlayerPropertyFight.SetParamPlayHpFull = function(newv)
	SetTmpParam_At(589, newv)
end
CLuaPlayerPropertyFight.SetParamPlayHp = function(newv)
	SetTmpParam_At(590, newv)
end
CLuaPlayerPropertyFight.SetParamPlayAtt = function(newv)
	SetTmpParam_At(591, newv)
end
CLuaPlayerPropertyFight.SetParamPlayDef = function(newv)
	SetTmpParam_At(592, newv)
end
CLuaPlayerPropertyFight.SetParamPlayHit = function(newv)
	SetTmpParam_At(593, newv)
end
CLuaPlayerPropertyFight.SetParamPlayMiss = function(newv)
	SetTmpParam_At(594, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceDieKe = function(newv)
	SetTmpParam_At(595, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceDieKeMul = function(newv)
	SetTmpParam_At(596, newv)
end
CLuaPlayerPropertyFight.SetParamDotRemain = function(newv)
	SetTmpParam_At(597, newv)
end
CLuaPlayerPropertyFight.SetParamIsConfused = function(newv)
	SetTmpParam_At(598, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceSheShouKefu = function(newv)
	SetTmpParam_At(599, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceJiaShiKefu = function(newv)
	SetTmpParam_At(600, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceDaoKeKefu = function(newv)
	SetTmpParam_At(601, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceXiaKeKefu = function(newv)
	SetTmpParam_At(602, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceFangShiKefu = function(newv)
	SetTmpParam_At(603, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceYiShiKefu = function(newv)
	SetTmpParam_At(604, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceMeiZheKefu = function(newv)
	SetTmpParam_At(605, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceYiRenKefu = function(newv)
	SetTmpParam_At(606, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceYanShiKefu = function(newv)
	SetTmpParam_At(607, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceHuaHunKefu = function(newv)
	SetTmpParam_At(608, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceYingLingKefu = function(newv)
	SetTmpParam_At(609, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceDieKeKefu = function(newv)
	SetTmpParam_At(610, newv)
end
CLuaPlayerPropertyFight.RefreshParamAdjustPermanentCor = function()
	local PermanentCor = GetParam_At(1)
	local AdjustPermanentMidCor = GetTmpParam_At(617)
	local RevisePermanentCor = GetParam_At(12)
	local AdjustPermanentMidAgi = GetTmpParam_At(621)
	local AdjustPermanentMidInt = GetTmpParam_At(620)
	local AdjustPermanentMidStr = GetTmpParam_At(619)
	local AdjustPermanentMidSta = GetTmpParam_At(618)
	local Grade = GetTmpParam_At(2)

	local newv = PermanentCor + AdjustPermanentMidCor + min(1,RevisePermanentCor-AdjustPermanentMidCor)*min(1,(min(35,floor(max(0,Grade-50)/20)*5+floor(max(0,Grade-100)/20)*5)-AdjustPermanentMidCor-AdjustPermanentMidSta-AdjustPermanentMidStr-AdjustPermanentMidInt-AdjustPermanentMidAgi))
	SetTmpParam_At(611, newv)
end
CLuaPlayerPropertyFight.RefreshParamAdjustPermanentSta = function()
	local PermanentSta = GetParam_At(2)
	local AdjustPermanentMidSta = GetTmpParam_At(618)
	local RevisePermanentSta = GetParam_At(13)
	local AdjustPermanentMidAgi = GetTmpParam_At(621)
	local AdjustPermanentMidInt = GetTmpParam_At(620)
	local AdjustPermanentMidStr = GetTmpParam_At(619)
	local RevisePermanentCor = GetParam_At(12)
	local AdjustPermanentMidCor = GetTmpParam_At(617)
	local Grade = GetTmpParam_At(2)

	local newv = PermanentSta + AdjustPermanentMidSta + min(1,RevisePermanentSta-AdjustPermanentMidSta)*min(1,max(0,min(35,floor(max(0,Grade-50)/20)*5+floor(max(0,Grade-100)/20)*5)-AdjustPermanentMidCor-AdjustPermanentMidSta-AdjustPermanentMidStr-AdjustPermanentMidInt-AdjustPermanentMidAgi-min(1,RevisePermanentCor-AdjustPermanentMidCor)))
	SetTmpParam_At(612, newv)
end
CLuaPlayerPropertyFight.RefreshParamAdjustPermanentStr = function()
	local PermanentStr = GetParam_At(3)
	local AdjustPermanentMidStr = GetTmpParam_At(619)
	local RevisePermanentStr = GetParam_At(14)
	local AdjustPermanentMidAgi = GetTmpParam_At(621)
	local RevisePermanentAgi = GetParam_At(16)
	local AdjustPermanentMidInt = GetTmpParam_At(620)
	local RevisePermanentSta = GetParam_At(13)
	local AdjustPermanentMidSta = GetTmpParam_At(618)
	local RevisePermanentCor = GetParam_At(12)
	local AdjustPermanentMidCor = GetTmpParam_At(617)
	local Grade = GetTmpParam_At(2)

	local newv = PermanentStr + AdjustPermanentMidStr + min(1,RevisePermanentStr-AdjustPermanentMidStr)*min(1,max(0,min(35,floor(max(0,Grade-50)/20)*5+floor(max(0,Grade-100)/20)*5)-AdjustPermanentMidCor-AdjustPermanentMidSta-AdjustPermanentMidStr-AdjustPermanentMidInt-AdjustPermanentMidAgi-min(1,RevisePermanentCor-AdjustPermanentMidCor)-min(1,RevisePermanentSta-AdjustPermanentMidSta)-min(1,RevisePermanentAgi-AdjustPermanentMidAgi)))
	SetTmpParam_At(613, newv)
end
CLuaPlayerPropertyFight.RefreshParamAdjustPermanentInt = function()
	local PermanentInt = GetParam_At(4)
	local AdjustPermanentMidInt = GetTmpParam_At(620)
	local RevisePermanentInt = GetParam_At(15)
	local RevisePermanentStr = GetParam_At(14)
	local AdjustPermanentMidStr = GetTmpParam_At(619)
	local AdjustPermanentMidAgi = GetTmpParam_At(621)
	local RevisePermanentAgi = GetParam_At(16)
	local RevisePermanentSta = GetParam_At(13)
	local AdjustPermanentMidSta = GetTmpParam_At(618)
	local RevisePermanentCor = GetParam_At(12)
	local AdjustPermanentMidCor = GetTmpParam_At(617)
	local Grade = GetTmpParam_At(2)

	local newv = PermanentInt + AdjustPermanentMidInt + min(1,RevisePermanentInt-AdjustPermanentMidInt)*min(1,max(0,min(35,floor(max(0,Grade-50)/20)*5+floor(max(0,Grade-100)/20)*5)-AdjustPermanentMidCor-AdjustPermanentMidSta-AdjustPermanentMidStr-AdjustPermanentMidInt-AdjustPermanentMidAgi-min(1,RevisePermanentCor-AdjustPermanentMidCor)-min(1,RevisePermanentSta-AdjustPermanentMidSta)-min(1,RevisePermanentAgi-AdjustPermanentMidAgi)-min(1,RevisePermanentStr-AdjustPermanentMidStr)))
	SetTmpParam_At(614, newv)
end
CLuaPlayerPropertyFight.RefreshParamAdjustPermanentAgi = function()
	local PermanentAgi = GetParam_At(5)
	local AdjustPermanentMidAgi = GetTmpParam_At(621)
	local RevisePermanentAgi = GetParam_At(16)
	local AdjustPermanentMidInt = GetTmpParam_At(620)
	local RevisePermanentSta = GetParam_At(13)
	local AdjustPermanentMidSta = GetTmpParam_At(618)
	local AdjustPermanentMidStr = GetTmpParam_At(619)
	local RevisePermanentCor = GetParam_At(12)
	local AdjustPermanentMidCor = GetTmpParam_At(617)
	local Grade = GetTmpParam_At(2)

	local newv = PermanentAgi + AdjustPermanentMidAgi + min(1,RevisePermanentAgi-AdjustPermanentMidAgi)*min(1,max(0,min(35,floor(max(0,Grade-50)/20)*5+floor(max(0,Grade-100)/20)*5)-AdjustPermanentMidCor-AdjustPermanentMidSta-AdjustPermanentMidStr-AdjustPermanentMidInt-AdjustPermanentMidAgi-min(1,RevisePermanentCor-AdjustPermanentMidCor)-min(1,RevisePermanentSta-AdjustPermanentMidSta)))
	SetTmpParam_At(615, newv)
end
CLuaPlayerPropertyFight.SetParamSoulCoreLevel = function(newv)
	SetTmpParam_At(616, newv)
end
CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidCor = function()
	local RevisePermanentCor = GetParam_At(12)
	local SumPermanent = GetTmpParam_At(622)
	local Grade = GetTmpParam_At(2)

	local newv = floor(RevisePermanentCor*min(1,min(35,floor(max(0,Grade-50)/20)*5+floor(max(0,Grade-100)/20)*5)/max(SumPermanent,1)))
	SetTmpParam_At(617, newv)
end
CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidSta = function()
	local RevisePermanentSta = GetParam_At(13)
	local SumPermanent = GetTmpParam_At(622)
	local Grade = GetTmpParam_At(2)

	local newv = floor(RevisePermanentSta*min(1,min(35,floor(max(0,Grade-50)/20)*5+floor(max(0,Grade-100)/20)*5)/max(SumPermanent,1)))
	SetTmpParam_At(618, newv)
end
CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidStr = function()
	local RevisePermanentStr = GetParam_At(14)
	local SumPermanent = GetTmpParam_At(622)
	local Grade = GetTmpParam_At(2)

	local newv = floor(RevisePermanentStr*min(1,min(35,floor(max(0,Grade-50)/20)*5+floor(max(0,Grade-100)/20)*5)/max(SumPermanent,1)))
	SetTmpParam_At(619, newv)
end
CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidInt = function()
	local RevisePermanentInt = GetParam_At(15)
	local SumPermanent = GetTmpParam_At(622)
	local Grade = GetTmpParam_At(2)

	local newv = floor(RevisePermanentInt*min(1,min(35,floor(max(0,Grade-50)/20)*5+floor(max(0,Grade-100)/20)*5)/max(SumPermanent,1)))
	SetTmpParam_At(620, newv)
end
CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidAgi = function()
	local RevisePermanentAgi = GetParam_At(16)
	local SumPermanent = GetTmpParam_At(622)
	local Grade = GetTmpParam_At(2)

	local newv = floor(RevisePermanentAgi*min(1,min(35,floor(max(0,Grade-50)/20)*5+floor(max(0,Grade-100)/20)*5)/max(SumPermanent,1)))
	SetTmpParam_At(621, newv)
end
CLuaPlayerPropertyFight.RefreshParamSumPermanent = function()
	local RevisePermanentAgi = GetParam_At(16)
	local RevisePermanentInt = GetParam_At(15)
	local RevisePermanentStr = GetParam_At(14)
	local RevisePermanentCor = GetParam_At(12)
	local RevisePermanentSta = GetParam_At(13)

	local newv = RevisePermanentCor+RevisePermanentSta+RevisePermanentStr+RevisePermanentInt+RevisePermanentAgi
	SetTmpParam_At(622, newv)
end
CLuaPlayerPropertyFight.SetParamAntiTian = function(newv)
	SetTmpParam_At(623, newv)
end
CLuaPlayerPropertyFight.SetParamAntiEGui = function(newv)
	SetTmpParam_At(624, newv)
end
CLuaPlayerPropertyFight.SetParamAntiXiuLuo = function(newv)
	SetTmpParam_At(625, newv)
end
CLuaPlayerPropertyFight.SetParamAntiDiYu = function(newv)
	SetTmpParam_At(626, newv)
end
CLuaPlayerPropertyFight.SetParamAntiRen = function(newv)
	SetTmpParam_At(627, newv)
end
CLuaPlayerPropertyFight.SetParamAntiChuSheng = function(newv)
	SetTmpParam_At(628, newv)
end
CLuaPlayerPropertyFight.SetParamAntiBuilding = function(newv)
	SetTmpParam_At(629, newv)
end
CLuaPlayerPropertyFight.SetParamAntiZhaoHuan = function(newv)
	SetTmpParam_At(630, newv)
end
CLuaPlayerPropertyFight.SetParamAntiLingShou = function(newv)
	SetTmpParam_At(631, newv)
end
CLuaPlayerPropertyFight.SetParamAntiBoss = function(newv)
	SetTmpParam_At(632, newv)
end
CLuaPlayerPropertyFight.SetParamAntiSheShou = function(newv)
	SetTmpParam_At(633, newv)
end
CLuaPlayerPropertyFight.SetParamAntiJiaShi = function(newv)
	SetTmpParam_At(634, newv)
end
CLuaPlayerPropertyFight.SetParamAntiDaoKe = function(newv)
	SetTmpParam_At(635, newv)
end
CLuaPlayerPropertyFight.SetParamAntiXiaKe = function(newv)
	SetTmpParam_At(636, newv)
end
CLuaPlayerPropertyFight.SetParamAntiFangShi = function(newv)
	SetTmpParam_At(637, newv)
end
CLuaPlayerPropertyFight.SetParamAntiYiShi = function(newv)
	SetTmpParam_At(638, newv)
end
CLuaPlayerPropertyFight.SetParamAntiMeiZhe = function(newv)
	SetTmpParam_At(639, newv)
end
CLuaPlayerPropertyFight.SetParamAntiYiRen = function(newv)
	SetTmpParam_At(640, newv)
end
CLuaPlayerPropertyFight.SetParamAntiYanShi = function(newv)
	SetTmpParam_At(641, newv)
end
CLuaPlayerPropertyFight.SetParamAntiHuaHun = function(newv)
	SetTmpParam_At(642, newv)
end
CLuaPlayerPropertyFight.SetParamAntiYingLing = function(newv)
	SetTmpParam_At(643, newv)
end
CLuaPlayerPropertyFight.SetParamAntiDieKe = function(newv)
	SetTmpParam_At(644, newv)
end
CLuaPlayerPropertyFight.SetParamAntiFlag = function(newv)
	SetTmpParam_At(645, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceZhanKuang = function(newv)
	SetTmpParam_At(646, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceZhanKuangMul = function(newv)
	SetTmpParam_At(647, newv)
end
CLuaPlayerPropertyFight.SetParamEnhanceZhanKuangKefu = function(newv)
	SetTmpParam_At(648, newv)
end
CLuaPlayerPropertyFight.SetParamAntiZhanKuang = function(newv)
	SetTmpParam_At(649, newv)
end
CLuaPlayerPropertyFight.RefreshParamJumpSpeed = function()
	local OriJumpSpeed = GetTmpParam_At(651)
	local AdjJumpSpeed = GetTmpParam_At(652)

	local newv = OriJumpSpeed+AdjJumpSpeed
	SetTmpParam_At(650, newv)
end
CLuaPlayerPropertyFight.SetParamOriJumpSpeed = function(newv)
	SetTmpParam_At(651, newv)

	CLuaPlayerPropertyFight.RefreshParamJumpSpeed()
end
CLuaPlayerPropertyFight.SetParamAdjJumpSpeed = function(newv)
	SetTmpParam_At(652, newv)

	CLuaPlayerPropertyFight.RefreshParamJumpSpeed()
end
CLuaPlayerPropertyFight.RefreshParamGravityAcceleration = function()
	local OriGravityAcceleration = GetTmpParam_At(654)
	local AdjGravityAcceleration = GetTmpParam_At(655)

	local newv = OriGravityAcceleration+AdjGravityAcceleration
	SetTmpParam_At(653, newv)
end
CLuaPlayerPropertyFight.SetParamOriGravityAcceleration = function(newv)
	SetTmpParam_At(654, newv)

	CLuaPlayerPropertyFight.RefreshParamGravityAcceleration()
end
CLuaPlayerPropertyFight.SetParamAdjGravityAcceleration = function(newv)
	SetTmpParam_At(655, newv)

	CLuaPlayerPropertyFight.RefreshParamGravityAcceleration()
end
CLuaPlayerPropertyFight.RefreshParamHorizontalAcceleration = function()
	local OriHorizontalAcceleration = GetTmpParam_At(657)
	local AdjHorizontalAcceleration = GetTmpParam_At(658)

	local newv = OriHorizontalAcceleration+AdjHorizontalAcceleration
	SetTmpParam_At(656, newv)
end
CLuaPlayerPropertyFight.SetParamOriHorizontalAcceleration = function(newv)
	SetTmpParam_At(657, newv)

	CLuaPlayerPropertyFight.RefreshParamHorizontalAcceleration()
end
CLuaPlayerPropertyFight.SetParamAdjHorizontalAcceleration = function(newv)
	SetTmpParam_At(658, newv)

	CLuaPlayerPropertyFight.RefreshParamHorizontalAcceleration()
end
CLuaPlayerPropertyFight.RefreshParamSwimSpeed = function()
	local OriSwimSpeed = GetTmpParam_At(660)
	local AdjSwimSpeed = GetTmpParam_At(661)

	local newv = OriSwimSpeed+AdjSwimSpeed
	SetTmpParam_At(659, newv)
end
CLuaPlayerPropertyFight.SetParamOriSwimSpeed = function(newv)
	SetTmpParam_At(660, newv)

	CLuaPlayerPropertyFight.RefreshParamSwimSpeed()
end
CLuaPlayerPropertyFight.SetParamAdjSwimSpeed = function(newv)
	SetTmpParam_At(661, newv)

	CLuaPlayerPropertyFight.RefreshParamSwimSpeed()
end
CLuaPlayerPropertyFight.SetParamStrengthPoint = function(newv)
	SetTmpParam_At(662, newv)
end
CLuaPlayerPropertyFight.RefreshParamStrengthPointMax = function()
	local OriStrengthPointMax = GetTmpParam_At(664)
	local AdjStrengthPointMax = GetTmpParam_At(665)

	local newv = OriStrengthPointMax+AdjStrengthPointMax
	SetTmpParam_At(663, newv)
end
CLuaPlayerPropertyFight.SetParamOriStrengthPointMax = function(newv)
	SetTmpParam_At(664, newv)

	CLuaPlayerPropertyFight.RefreshParamStrengthPointMax()
end
CLuaPlayerPropertyFight.SetParamAdjStrengthPointMax = function(newv)
	SetTmpParam_At(665, newv)

	CLuaPlayerPropertyFight.RefreshParamStrengthPointMax()
end
CLuaPlayerPropertyFight.SetParamAntiMulEnhanceIllusion = function(newv)
	SetTmpParam_At(666, newv)
end
CLuaPlayerPropertyFight.SetParamGlideHorizontalSpeedWithUmbrella = function(newv)
	SetTmpParam_At(667, newv)
end
CLuaPlayerPropertyFight.SetParamGlideVerticalSpeedWithUmbrella = function(newv)
	SetTmpParam_At(668, newv)
end
CLuaPlayerPropertyFight.SetParamAdjpSpeed = function(newv)
	SetTmpParam_At(1001, newv)

	CLuaPlayerPropertyFight.RefreshParampSpeed()
end
CLuaPlayerPropertyFight.SetParamAdjmSpeed = function(newv)
	SetTmpParam_At(1002, newv)

	CLuaPlayerPropertyFight.RefreshParammSpeed()
end
CPlayerPropertyFight.m_hookRefreshAll = function(this)
	CLuaPlayerPropertyFight.Param = this.Param
	CLuaPlayerPropertyFight.TmpParam = this.TmpParam

	CLuaPlayerPropertyFight.RefreshParamAntiPushPull()
	CLuaPlayerPropertyFight.RefreshParamLookUppAttMax()
	CLuaPlayerPropertyFight.RefreshParamHpRecover()
	CLuaPlayerPropertyFight.RefreshParamMpRecover()
	CLuaPlayerPropertyFight.RefreshParamInvisible()
	CLuaPlayerPropertyFight.RefreshParamEnhanceWater()
	CLuaPlayerPropertyFight.RefreshParamAntiBreak()
	CLuaPlayerPropertyFight.RefreshParamLSAntiBlock()
	CLuaPlayerPropertyFight.RefreshParamHorizontalAcceleration()
	CLuaPlayerPropertyFight.RefreshParamAntiBlock()
	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiIce()
	CLuaPlayerPropertyFight.RefreshParamEnhanceFire()
	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiWater()
	CLuaPlayerPropertyFight.RefreshParamAntimFatal()
	CLuaPlayerPropertyFight.RefreshParamAntiDecelerate()
	CLuaPlayerPropertyFight.RefreshParamGravityAcceleration()
	CLuaPlayerPropertyFight.RefreshParamLookUpHpFull()
	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiThunder()
	CLuaPlayerPropertyFight.RefreshParamAntiIce()
	CLuaPlayerPropertyFight.RefreshParamEnhanceIce()
	CLuaPlayerPropertyFight.RefreshParamEnhanceWind()
	CLuaPlayerPropertyFight.RefreshParamBBMax()
	CLuaPlayerPropertyFight.RefreshParamLSpFatalDamage()
	CLuaPlayerPropertyFight.RefreshParamIgnoreAntiFire()
	CLuaPlayerPropertyFight.RefreshParamSpeed()
	CLuaPlayerPropertyFight.RefreshParamLSpAttMax()
	CLuaPlayerPropertyFight.RefreshParamSwimSpeed()
	CLuaPlayerPropertyFight.RefreshParamAntiSleep()
	CLuaPlayerPropertyFight.RefreshParamEnhanceHeal()
	CLuaPlayerPropertyFight.RefreshParamAntiDizzy()
	CLuaPlayerPropertyFight.RefreshParamAntiBind()
	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiWind()
	CLuaPlayerPropertyFight.RefreshParamStrengthPointMax()
	CLuaPlayerPropertyFight.RefreshParamLookUppAttMin()
	CLuaPlayerPropertyFight.RefreshParamLSpFatal()
	CLuaPlayerPropertyFight.RefreshParamAntiAoe()
	CLuaPlayerPropertyFight.RefreshParamAntiPetrify()
	CLuaPlayerPropertyFight.RefreshParamLSmFatalDamage()
	CLuaPlayerPropertyFight.RefreshParamEnhanceTie()
	CLuaPlayerPropertyFight.RefreshParamLookUpmAttMax()
	CLuaPlayerPropertyFight.RefreshParamIgnoreAntiPoison()
	CLuaPlayerPropertyFight.RefreshParamIgnoreAntiIce()
	CLuaPlayerPropertyFight.RefreshParamMF()
	CLuaPlayerPropertyFight.RefreshParamLookUpInt()
	CLuaPlayerPropertyFight.RefreshParamLSAntiBlockDamage()
	CLuaPlayerPropertyFight.RefreshParamAntiFire()
	CLuaPlayerPropertyFight.RefreshParamIgnoreAntiWind()
	CLuaPlayerPropertyFight.RefreshParamLookUppHit()
	CLuaPlayerPropertyFight.RefreshParamBlockDamage()
	CLuaPlayerPropertyFight.RefreshParamLSmAttMax()
	CLuaPlayerPropertyFight.RefreshParamHatePlus()
	CLuaPlayerPropertyFight.RefreshParamIgnoreAntiLight()
	CLuaPlayerPropertyFight.RefreshParamAntiSilence()
	CLuaPlayerPropertyFight.RefreshParamEnhanceChaos()
	CLuaPlayerPropertyFight.RefreshParamLookUpAgi()
	CLuaPlayerPropertyFight.RefreshParamAntiWater()
	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiFire()
	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiIllusion()
	CLuaPlayerPropertyFight.RefreshParamLookUppMiss()
	CLuaPlayerPropertyFight.RefreshParamLookUpMpFull()
	CLuaPlayerPropertyFight.RefreshParammFatalDamage()
	CLuaPlayerPropertyFight.RefreshParamIgnoreAntiThunder()
	CLuaPlayerPropertyFight.RefreshParamAntipFatal()
	CLuaPlayerPropertyFight.RefreshParammFatal()
	CLuaPlayerPropertyFight.RefreshParamAntiWind()
	CLuaPlayerPropertyFight.RefreshParamEnhancePetrify()
	CLuaPlayerPropertyFight.RefreshParamAntiChaos()
	CLuaPlayerPropertyFight.RefreshParampFatal()
	CLuaPlayerPropertyFight.RefreshParamLSpAttMin()
	CLuaPlayerPropertyFight.RefreshParamAntiIllusion()
	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiPoison()
	CLuaPlayerPropertyFight.RefreshParamBlock()
	CLuaPlayerPropertyFight.RefreshParamEnhanceBind()
	CLuaPlayerPropertyFight.RefreshParamAntiFreeze()
	CLuaPlayerPropertyFight.RefreshParamJumpSpeed()
	CLuaPlayerPropertyFight.RefreshParamLookUpmMiss()
	CLuaPlayerPropertyFight.RefreshParamIgnoreAntiWater()
	CLuaPlayerPropertyFight.RefreshParamAntiPoison()
	CLuaPlayerPropertyFight.RefreshParamEnhanceSleep()
	CLuaPlayerPropertyFight.RefreshParamIgnoreAntiIllusion()
	CLuaPlayerPropertyFight.RefreshParamLookUpSta()
	CLuaPlayerPropertyFight.RefreshParamAntimFatalDamage()
	CLuaPlayerPropertyFight.RefreshParamEnhanceIllusion()
	CLuaPlayerPropertyFight.RefreshParamEnhanceThunder()
	CLuaPlayerPropertyFight.RefreshParamEnhanceLight()
	CLuaPlayerPropertyFight.RefreshParamAntiBlockDamage()
	CLuaPlayerPropertyFight.RefreshParampSpeed()
	CLuaPlayerPropertyFight.RefreshParamEnhanceSilence()
	CLuaPlayerPropertyFight.RefreshParammSpeed()
	CLuaPlayerPropertyFight.RefreshParamLSmFatal()
	CLuaPlayerPropertyFight.RefreshParamEnhancePoison()
	CLuaPlayerPropertyFight.RefreshParamLookUpmAttMin()
	CLuaPlayerPropertyFight.RefreshParamAntiBianHu()
	CLuaPlayerPropertyFight.RefreshParamSumPermanent()
	CLuaPlayerPropertyFight.RefreshParamLookUpStr()
	CLuaPlayerPropertyFight.RefreshParamEyeSight()
	CLuaPlayerPropertyFight.RefreshParamRange()
	CLuaPlayerPropertyFight.RefreshParamAntiLight()
	CLuaPlayerPropertyFight.RefreshParamEnhanceBeHeal()
	CLuaPlayerPropertyFight.RefreshParamLookUpCor()
	CLuaPlayerPropertyFight.RefreshParampFatalDamage()
	CLuaPlayerPropertyFight.RefreshParamLookUpmHit()
	CLuaPlayerPropertyFight.RefreshParamOriTrueSight()
	CLuaPlayerPropertyFight.RefreshParamEnhanceBianHu()
	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidSta()
	CLuaPlayerPropertyFight.RefreshParamAntiBlind()
	CLuaPlayerPropertyFight.RefreshParamAntiTie()
	CLuaPlayerPropertyFight.RefreshParamAntiThunder()
	CLuaPlayerPropertyFight.RefreshParamLSIgnoreAntiLight()
	CLuaPlayerPropertyFight.RefreshParamLookUppDef()
	CLuaPlayerPropertyFight.RefreshParamEnhanceFreeze()
	CLuaPlayerPropertyFight.RefreshParamAntipFatalDamage()
	CLuaPlayerPropertyFight.RefreshParamLSmAttMin()
	CLuaPlayerPropertyFight.RefreshParamLookUpmDef()
	CLuaPlayerPropertyFight.RefreshParamEnhanceDizzy()
	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidInt()
	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidCor()
	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidAgi()
	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentMidStr()
	CLuaPlayerPropertyFight.RefreshParamTrueSight()
	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentCor()
	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentInt()
	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentSta()
	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentStr()
	CLuaPlayerPropertyFight.RefreshParamAdjustPermanentAgi()
	CLuaPlayerPropertyFight.RefreshParamCharacterCor()
	CLuaPlayerPropertyFight.RefreshParamCor()
	CLuaPlayerPropertyFight.RefreshParamInt()
	CLuaPlayerPropertyFight.RefreshParamCharacterInt()
	CLuaPlayerPropertyFight.RefreshParamSta()
	CLuaPlayerPropertyFight.RefreshParamCharacterSta()
	CLuaPlayerPropertyFight.RefreshParamStr()
	CLuaPlayerPropertyFight.RefreshParamCharacterStr()
	CLuaPlayerPropertyFight.RefreshParamAgi()
	CLuaPlayerPropertyFight.RefreshParamCharacterAgi()
	CLuaPlayerPropertyFight.RefreshParamHpFull()
	CLuaPlayerPropertyFight.RefreshParammAttMin()
	CLuaPlayerPropertyFight.RefreshParammAttMax()
	CLuaPlayerPropertyFight.RefreshParammDef()
	CLuaPlayerPropertyFight.RefreshParamMpFull()
	CLuaPlayerPropertyFight.RefreshParampAttMax()
	CLuaPlayerPropertyFight.RefreshParampDef()
	CLuaPlayerPropertyFight.RefreshParampAttMin()
	CLuaPlayerPropertyFight.RefreshParammMiss()
	CLuaPlayerPropertyFight.RefreshParammHit()
	CLuaPlayerPropertyFight.RefreshParampMiss()
	CLuaPlayerPropertyFight.RefreshParampHit()
	
	CLuaPlayerPropertyFight.Param = nil
	CLuaPlayerPropertyFight.TmpParam = nil
end
SetParamFunctions[EnumPlayerFightProp.PermanentCor] = CLuaPlayerPropertyFight.SetParamPermanentCor
SetParamFunctions[EnumPlayerFightProp.PermanentSta] = CLuaPlayerPropertyFight.SetParamPermanentSta
SetParamFunctions[EnumPlayerFightProp.PermanentStr] = CLuaPlayerPropertyFight.SetParamPermanentStr
SetParamFunctions[EnumPlayerFightProp.PermanentInt] = CLuaPlayerPropertyFight.SetParamPermanentInt
SetParamFunctions[EnumPlayerFightProp.PermanentAgi] = CLuaPlayerPropertyFight.SetParamPermanentAgi
SetParamFunctions[EnumPlayerFightProp.PermanentHpFull] = CLuaPlayerPropertyFight.SetParamPermanentHpFull
SetParamFunctions[EnumPlayerFightProp.Hp] = CLuaPlayerPropertyFight.SetParamHp
SetParamFunctions[EnumPlayerFightProp.Mp] = CLuaPlayerPropertyFight.SetParamMp
SetParamFunctions[EnumPlayerFightProp.CurrentHpFull] = CLuaPlayerPropertyFight.SetParamCurrentHpFull
SetParamFunctions[EnumPlayerFightProp.CurrentMpFull] = CLuaPlayerPropertyFight.SetParamCurrentMpFull
SetParamFunctions[EnumPlayerFightProp.EquipExchangeMF] = CLuaPlayerPropertyFight.SetParamEquipExchangeMF
SetParamFunctions[EnumPlayerFightProp.RevisePermanentCor] = CLuaPlayerPropertyFight.SetParamRevisePermanentCor
SetParamFunctions[EnumPlayerFightProp.RevisePermanentSta] = CLuaPlayerPropertyFight.SetParamRevisePermanentSta
SetParamFunctions[EnumPlayerFightProp.RevisePermanentStr] = CLuaPlayerPropertyFight.SetParamRevisePermanentStr
SetParamFunctions[EnumPlayerFightProp.RevisePermanentInt] = CLuaPlayerPropertyFight.SetParamRevisePermanentInt
SetParamFunctions[EnumPlayerFightProp.RevisePermanentAgi] = CLuaPlayerPropertyFight.SetParamRevisePermanentAgi
SetParamFunctions[EnumPlayerFightProp.Class] = CLuaPlayerPropertyFight.SetParamClass
SetParamFunctions[EnumPlayerFightProp.Grade] = CLuaPlayerPropertyFight.SetParamGrade
SetParamFunctions[EnumPlayerFightProp.AdjCor] = CLuaPlayerPropertyFight.SetParamAdjCor
SetParamFunctions[EnumPlayerFightProp.MulCor] = CLuaPlayerPropertyFight.SetParamMulCor
SetParamFunctions[EnumPlayerFightProp.AdjSta] = CLuaPlayerPropertyFight.SetParamAdjSta
SetParamFunctions[EnumPlayerFightProp.MulSta] = CLuaPlayerPropertyFight.SetParamMulSta
SetParamFunctions[EnumPlayerFightProp.AdjStr] = CLuaPlayerPropertyFight.SetParamAdjStr
SetParamFunctions[EnumPlayerFightProp.MulStr] = CLuaPlayerPropertyFight.SetParamMulStr
SetParamFunctions[EnumPlayerFightProp.AdjInt] = CLuaPlayerPropertyFight.SetParamAdjInt
SetParamFunctions[EnumPlayerFightProp.MulInt] = CLuaPlayerPropertyFight.SetParamMulInt
SetParamFunctions[EnumPlayerFightProp.AdjAgi] = CLuaPlayerPropertyFight.SetParamAdjAgi
SetParamFunctions[EnumPlayerFightProp.MulAgi] = CLuaPlayerPropertyFight.SetParamMulAgi
SetParamFunctions[EnumPlayerFightProp.AdjHpFull] = CLuaPlayerPropertyFight.SetParamAdjHpFull
SetParamFunctions[EnumPlayerFightProp.MulHpFull] = CLuaPlayerPropertyFight.SetParamMulHpFull
SetParamFunctions[EnumPlayerFightProp.AdjHpRecover] = CLuaPlayerPropertyFight.SetParamAdjHpRecover
SetParamFunctions[EnumPlayerFightProp.MulHpRecover] = CLuaPlayerPropertyFight.SetParamMulHpRecover
SetParamFunctions[EnumPlayerFightProp.AdjMpFull] = CLuaPlayerPropertyFight.SetParamAdjMpFull
SetParamFunctions[EnumPlayerFightProp.MulMpFull] = CLuaPlayerPropertyFight.SetParamMulMpFull
SetParamFunctions[EnumPlayerFightProp.AdjMpRecover] = CLuaPlayerPropertyFight.SetParamAdjMpRecover
SetParamFunctions[EnumPlayerFightProp.MulMpRecover] = CLuaPlayerPropertyFight.SetParamMulMpRecover
SetParamFunctions[EnumPlayerFightProp.AdjpAttMin] = CLuaPlayerPropertyFight.SetParamAdjpAttMin
SetParamFunctions[EnumPlayerFightProp.MulpAttMin] = CLuaPlayerPropertyFight.SetParamMulpAttMin
SetParamFunctions[EnumPlayerFightProp.AdjpAttMax] = CLuaPlayerPropertyFight.SetParamAdjpAttMax
SetParamFunctions[EnumPlayerFightProp.MulpAttMax] = CLuaPlayerPropertyFight.SetParamMulpAttMax
SetParamFunctions[EnumPlayerFightProp.AdjpHit] = CLuaPlayerPropertyFight.SetParamAdjpHit
SetParamFunctions[EnumPlayerFightProp.MulpHit] = CLuaPlayerPropertyFight.SetParamMulpHit
SetParamFunctions[EnumPlayerFightProp.AdjpMiss] = CLuaPlayerPropertyFight.SetParamAdjpMiss
SetParamFunctions[EnumPlayerFightProp.MulpMiss] = CLuaPlayerPropertyFight.SetParamMulpMiss
SetParamFunctions[EnumPlayerFightProp.AdjpDef] = CLuaPlayerPropertyFight.SetParamAdjpDef
SetParamFunctions[EnumPlayerFightProp.MulpDef] = CLuaPlayerPropertyFight.SetParamMulpDef
SetParamFunctions[EnumPlayerFightProp.AdjpHurt] = CLuaPlayerPropertyFight.SetParamAdjpHurt
SetParamFunctions[EnumPlayerFightProp.MulpHurt] = CLuaPlayerPropertyFight.SetParamMulpHurt
SetParamFunctions[EnumPlayerFightProp.OripSpeed] = CLuaPlayerPropertyFight.SetParamOripSpeed
SetParamFunctions[EnumPlayerFightProp.MulpSpeed] = CLuaPlayerPropertyFight.SetParamMulpSpeed
SetParamFunctions[EnumPlayerFightProp.AdjpFatal] = CLuaPlayerPropertyFight.SetParamAdjpFatal
SetParamFunctions[EnumPlayerFightProp.AdjAntipFatal] = CLuaPlayerPropertyFight.SetParamAdjAntipFatal
SetParamFunctions[EnumPlayerFightProp.AdjpFatalDamage] = CLuaPlayerPropertyFight.SetParamAdjpFatalDamage
SetParamFunctions[EnumPlayerFightProp.AdjAntipFatalDamage] = CLuaPlayerPropertyFight.SetParamAdjAntipFatalDamage
SetParamFunctions[EnumPlayerFightProp.AdjBlock] = CLuaPlayerPropertyFight.SetParamAdjBlock
SetParamFunctions[EnumPlayerFightProp.MulBlock] = CLuaPlayerPropertyFight.SetParamMulBlock
SetParamFunctions[EnumPlayerFightProp.AdjBlockDamage] = CLuaPlayerPropertyFight.SetParamAdjBlockDamage
SetParamFunctions[EnumPlayerFightProp.AdjAntiBlock] = CLuaPlayerPropertyFight.SetParamAdjAntiBlock
SetParamFunctions[EnumPlayerFightProp.AdjAntiBlockDamage] = CLuaPlayerPropertyFight.SetParamAdjAntiBlockDamage
SetParamFunctions[EnumPlayerFightProp.AdjmAttMin] = CLuaPlayerPropertyFight.SetParamAdjmAttMin
SetParamFunctions[EnumPlayerFightProp.MulmAttMin] = CLuaPlayerPropertyFight.SetParamMulmAttMin
SetParamFunctions[EnumPlayerFightProp.AdjmAttMax] = CLuaPlayerPropertyFight.SetParamAdjmAttMax
SetParamFunctions[EnumPlayerFightProp.MulmAttMax] = CLuaPlayerPropertyFight.SetParamMulmAttMax
SetParamFunctions[EnumPlayerFightProp.AdjmHit] = CLuaPlayerPropertyFight.SetParamAdjmHit
SetParamFunctions[EnumPlayerFightProp.MulmHit] = CLuaPlayerPropertyFight.SetParamMulmHit
SetParamFunctions[EnumPlayerFightProp.AdjmMiss] = CLuaPlayerPropertyFight.SetParamAdjmMiss
SetParamFunctions[EnumPlayerFightProp.MulmMiss] = CLuaPlayerPropertyFight.SetParamMulmMiss
SetParamFunctions[EnumPlayerFightProp.AdjmDef] = CLuaPlayerPropertyFight.SetParamAdjmDef
SetParamFunctions[EnumPlayerFightProp.MulmDef] = CLuaPlayerPropertyFight.SetParamMulmDef
SetParamFunctions[EnumPlayerFightProp.AdjmHurt] = CLuaPlayerPropertyFight.SetParamAdjmHurt
SetParamFunctions[EnumPlayerFightProp.MulmHurt] = CLuaPlayerPropertyFight.SetParamMulmHurt
SetParamFunctions[EnumPlayerFightProp.OrimSpeed] = CLuaPlayerPropertyFight.SetParamOrimSpeed
SetParamFunctions[EnumPlayerFightProp.MulmSpeed] = CLuaPlayerPropertyFight.SetParamMulmSpeed
SetParamFunctions[EnumPlayerFightProp.AdjmFatal] = CLuaPlayerPropertyFight.SetParamAdjmFatal
SetParamFunctions[EnumPlayerFightProp.AdjAntimFatal] = CLuaPlayerPropertyFight.SetParamAdjAntimFatal
SetParamFunctions[EnumPlayerFightProp.AdjmFatalDamage] = CLuaPlayerPropertyFight.SetParamAdjmFatalDamage
SetParamFunctions[EnumPlayerFightProp.AdjAntimFatalDamage] = CLuaPlayerPropertyFight.SetParamAdjAntimFatalDamage
SetParamFunctions[EnumPlayerFightProp.AdjEnhanceFire] = CLuaPlayerPropertyFight.SetParamAdjEnhanceFire
SetParamFunctions[EnumPlayerFightProp.MulEnhanceFire] = CLuaPlayerPropertyFight.SetParamMulEnhanceFire
SetParamFunctions[EnumPlayerFightProp.AdjEnhanceThunder] = CLuaPlayerPropertyFight.SetParamAdjEnhanceThunder
SetParamFunctions[EnumPlayerFightProp.MulEnhanceThunder] = CLuaPlayerPropertyFight.SetParamMulEnhanceThunder
SetParamFunctions[EnumPlayerFightProp.AdjEnhanceIce] = CLuaPlayerPropertyFight.SetParamAdjEnhanceIce
SetParamFunctions[EnumPlayerFightProp.MulEnhanceIce] = CLuaPlayerPropertyFight.SetParamMulEnhanceIce
SetParamFunctions[EnumPlayerFightProp.AdjEnhancePoison] = CLuaPlayerPropertyFight.SetParamAdjEnhancePoison
SetParamFunctions[EnumPlayerFightProp.MulEnhancePoison] = CLuaPlayerPropertyFight.SetParamMulEnhancePoison
SetParamFunctions[EnumPlayerFightProp.AdjEnhanceWind] = CLuaPlayerPropertyFight.SetParamAdjEnhanceWind
SetParamFunctions[EnumPlayerFightProp.MulEnhanceWind] = CLuaPlayerPropertyFight.SetParamMulEnhanceWind
SetParamFunctions[EnumPlayerFightProp.AdjEnhanceLight] = CLuaPlayerPropertyFight.SetParamAdjEnhanceLight
SetParamFunctions[EnumPlayerFightProp.MulEnhanceLight] = CLuaPlayerPropertyFight.SetParamMulEnhanceLight
SetParamFunctions[EnumPlayerFightProp.AdjEnhanceIllusion] = CLuaPlayerPropertyFight.SetParamAdjEnhanceIllusion
SetParamFunctions[EnumPlayerFightProp.MulEnhanceIllusion] = CLuaPlayerPropertyFight.SetParamMulEnhanceIllusion
SetParamFunctions[EnumPlayerFightProp.AdjAntiFire] = CLuaPlayerPropertyFight.SetParamAdjAntiFire
SetParamFunctions[EnumPlayerFightProp.MulAntiFire] = CLuaPlayerPropertyFight.SetParamMulAntiFire
SetParamFunctions[EnumPlayerFightProp.AdjAntiThunder] = CLuaPlayerPropertyFight.SetParamAdjAntiThunder
SetParamFunctions[EnumPlayerFightProp.MulAntiThunder] = CLuaPlayerPropertyFight.SetParamMulAntiThunder
SetParamFunctions[EnumPlayerFightProp.AdjAntiIce] = CLuaPlayerPropertyFight.SetParamAdjAntiIce
SetParamFunctions[EnumPlayerFightProp.MulAntiIce] = CLuaPlayerPropertyFight.SetParamMulAntiIce
SetParamFunctions[EnumPlayerFightProp.AdjAntiPoison] = CLuaPlayerPropertyFight.SetParamAdjAntiPoison
SetParamFunctions[EnumPlayerFightProp.MulAntiPoison] = CLuaPlayerPropertyFight.SetParamMulAntiPoison
SetParamFunctions[EnumPlayerFightProp.AdjAntiWind] = CLuaPlayerPropertyFight.SetParamAdjAntiWind
SetParamFunctions[EnumPlayerFightProp.MulAntiWind] = CLuaPlayerPropertyFight.SetParamMulAntiWind
SetParamFunctions[EnumPlayerFightProp.AdjAntiLight] = CLuaPlayerPropertyFight.SetParamAdjAntiLight
SetParamFunctions[EnumPlayerFightProp.MulAntiLight] = CLuaPlayerPropertyFight.SetParamMulAntiLight
SetParamFunctions[EnumPlayerFightProp.AdjAntiIllusion] = CLuaPlayerPropertyFight.SetParamAdjAntiIllusion
SetParamFunctions[EnumPlayerFightProp.MulAntiIllusion] = CLuaPlayerPropertyFight.SetParamMulAntiIllusion
SetParamFunctions[EnumPlayerFightProp.AdjEnhanceDizzy] = CLuaPlayerPropertyFight.SetParamAdjEnhanceDizzy
SetParamFunctions[EnumPlayerFightProp.MulEnhanceDizzy] = CLuaPlayerPropertyFight.SetParamMulEnhanceDizzy
SetParamFunctions[EnumPlayerFightProp.AdjEnhanceSleep] = CLuaPlayerPropertyFight.SetParamAdjEnhanceSleep
SetParamFunctions[EnumPlayerFightProp.MulEnhanceSleep] = CLuaPlayerPropertyFight.SetParamMulEnhanceSleep
SetParamFunctions[EnumPlayerFightProp.AdjEnhanceChaos] = CLuaPlayerPropertyFight.SetParamAdjEnhanceChaos
SetParamFunctions[EnumPlayerFightProp.MulEnhanceChaos] = CLuaPlayerPropertyFight.SetParamMulEnhanceChaos
SetParamFunctions[EnumPlayerFightProp.AdjEnhanceBind] = CLuaPlayerPropertyFight.SetParamAdjEnhanceBind
SetParamFunctions[EnumPlayerFightProp.MulEnhanceBind] = CLuaPlayerPropertyFight.SetParamMulEnhanceBind
SetParamFunctions[EnumPlayerFightProp.AdjEnhanceSilence] = CLuaPlayerPropertyFight.SetParamAdjEnhanceSilence
SetParamFunctions[EnumPlayerFightProp.MulEnhanceSilence] = CLuaPlayerPropertyFight.SetParamMulEnhanceSilence
SetParamFunctions[EnumPlayerFightProp.AdjAntiDizzy] = CLuaPlayerPropertyFight.SetParamAdjAntiDizzy
SetParamFunctions[EnumPlayerFightProp.MulAntiDizzy] = CLuaPlayerPropertyFight.SetParamMulAntiDizzy
SetParamFunctions[EnumPlayerFightProp.AdjAntiSleep] = CLuaPlayerPropertyFight.SetParamAdjAntiSleep
SetParamFunctions[EnumPlayerFightProp.MulAntiSleep] = CLuaPlayerPropertyFight.SetParamMulAntiSleep
SetParamFunctions[EnumPlayerFightProp.AdjAntiChaos] = CLuaPlayerPropertyFight.SetParamAdjAntiChaos
SetParamFunctions[EnumPlayerFightProp.MulAntiChaos] = CLuaPlayerPropertyFight.SetParamMulAntiChaos
SetParamFunctions[EnumPlayerFightProp.AdjAntiBind] = CLuaPlayerPropertyFight.SetParamAdjAntiBind
SetParamFunctions[EnumPlayerFightProp.MulAntiBind] = CLuaPlayerPropertyFight.SetParamMulAntiBind
SetParamFunctions[EnumPlayerFightProp.AdjAntiSilence] = CLuaPlayerPropertyFight.SetParamAdjAntiSilence
SetParamFunctions[EnumPlayerFightProp.MulAntiSilence] = CLuaPlayerPropertyFight.SetParamMulAntiSilence
SetParamFunctions[EnumPlayerFightProp.DizzyTimeChange] = CLuaPlayerPropertyFight.SetParamDizzyTimeChange
SetParamFunctions[EnumPlayerFightProp.SleepTimeChange] = CLuaPlayerPropertyFight.SetParamSleepTimeChange
SetParamFunctions[EnumPlayerFightProp.ChaosTimeChange] = CLuaPlayerPropertyFight.SetParamChaosTimeChange
SetParamFunctions[EnumPlayerFightProp.BindTimeChange] = CLuaPlayerPropertyFight.SetParamBindTimeChange
SetParamFunctions[EnumPlayerFightProp.SilenceTimeChange] = CLuaPlayerPropertyFight.SetParamSilenceTimeChange
SetParamFunctions[EnumPlayerFightProp.BianhuTimeChange] = CLuaPlayerPropertyFight.SetParamBianhuTimeChange
SetParamFunctions[EnumPlayerFightProp.FreezeTimeChange] = CLuaPlayerPropertyFight.SetParamFreezeTimeChange
SetParamFunctions[EnumPlayerFightProp.PetrifyTimeChange] = CLuaPlayerPropertyFight.SetParamPetrifyTimeChange
SetParamFunctions[EnumPlayerFightProp.AdjEnhanceHeal] = CLuaPlayerPropertyFight.SetParamAdjEnhanceHeal
SetParamFunctions[EnumPlayerFightProp.MulEnhanceHeal] = CLuaPlayerPropertyFight.SetParamMulEnhanceHeal
SetParamFunctions[EnumPlayerFightProp.OriSpeed] = CLuaPlayerPropertyFight.SetParamOriSpeed
SetParamFunctions[EnumPlayerFightProp.AdjSpeed] = CLuaPlayerPropertyFight.SetParamAdjSpeed
SetParamFunctions[EnumPlayerFightProp.MulSpeed] = CLuaPlayerPropertyFight.SetParamMulSpeed
SetParamFunctions[EnumPlayerFightProp.AdjMF] = CLuaPlayerPropertyFight.SetParamAdjMF
SetParamFunctions[EnumPlayerFightProp.AdjRange] = CLuaPlayerPropertyFight.SetParamAdjRange
SetParamFunctions[EnumPlayerFightProp.AdjHatePlus] = CLuaPlayerPropertyFight.SetParamAdjHatePlus
SetParamFunctions[EnumPlayerFightProp.HateDecrease] = CLuaPlayerPropertyFight.SetParamHateDecrease
SetParamFunctions[EnumPlayerFightProp.AdjInvisible] = CLuaPlayerPropertyFight.SetParamAdjInvisible
SetParamFunctions[EnumPlayerFightProp.AdjTrueSight] = CLuaPlayerPropertyFight.SetParamAdjTrueSight
SetParamFunctions[EnumPlayerFightProp.OriEyeSight] = CLuaPlayerPropertyFight.SetParamOriEyeSight
SetParamFunctions[EnumPlayerFightProp.AdjEyeSight] = CLuaPlayerPropertyFight.SetParamAdjEyeSight
SetParamFunctions[EnumPlayerFightProp.EnhanceTian] = CLuaPlayerPropertyFight.SetParamEnhanceTian
SetParamFunctions[EnumPlayerFightProp.EnhanceTianMul] = CLuaPlayerPropertyFight.SetParamEnhanceTianMul
SetParamFunctions[EnumPlayerFightProp.EnhanceEGui] = CLuaPlayerPropertyFight.SetParamEnhanceEGui
SetParamFunctions[EnumPlayerFightProp.EnhanceEGuiMul] = CLuaPlayerPropertyFight.SetParamEnhanceEGuiMul
SetParamFunctions[EnumPlayerFightProp.EnhanceXiuLuo] = CLuaPlayerPropertyFight.SetParamEnhanceXiuLuo
SetParamFunctions[EnumPlayerFightProp.EnhanceXiuLuoMul] = CLuaPlayerPropertyFight.SetParamEnhanceXiuLuoMul
SetParamFunctions[EnumPlayerFightProp.EnhanceDiYu] = CLuaPlayerPropertyFight.SetParamEnhanceDiYu
SetParamFunctions[EnumPlayerFightProp.EnhanceDiYuMul] = CLuaPlayerPropertyFight.SetParamEnhanceDiYuMul
SetParamFunctions[EnumPlayerFightProp.EnhanceRen] = CLuaPlayerPropertyFight.SetParamEnhanceRen
SetParamFunctions[EnumPlayerFightProp.EnhanceRenMul] = CLuaPlayerPropertyFight.SetParamEnhanceRenMul
SetParamFunctions[EnumPlayerFightProp.EnhanceChuSheng] = CLuaPlayerPropertyFight.SetParamEnhanceChuSheng
SetParamFunctions[EnumPlayerFightProp.EnhanceChuShengMul] = CLuaPlayerPropertyFight.SetParamEnhanceChuShengMul
SetParamFunctions[EnumPlayerFightProp.EnhanceBuilding] = CLuaPlayerPropertyFight.SetParamEnhanceBuilding
SetParamFunctions[EnumPlayerFightProp.EnhanceBuildingMul] = CLuaPlayerPropertyFight.SetParamEnhanceBuildingMul
SetParamFunctions[EnumPlayerFightProp.EnhanceBoss] = CLuaPlayerPropertyFight.SetParamEnhanceBoss
SetParamFunctions[EnumPlayerFightProp.EnhanceBossMul] = CLuaPlayerPropertyFight.SetParamEnhanceBossMul
SetParamFunctions[EnumPlayerFightProp.EnhanceSheShou] = CLuaPlayerPropertyFight.SetParamEnhanceSheShou
SetParamFunctions[EnumPlayerFightProp.EnhanceSheShouMul] = CLuaPlayerPropertyFight.SetParamEnhanceSheShouMul
SetParamFunctions[EnumPlayerFightProp.EnhanceJiaShi] = CLuaPlayerPropertyFight.SetParamEnhanceJiaShi
SetParamFunctions[EnumPlayerFightProp.EnhanceJiaShiMul] = CLuaPlayerPropertyFight.SetParamEnhanceJiaShiMul
SetParamFunctions[EnumPlayerFightProp.EnhanceFangShi] = CLuaPlayerPropertyFight.SetParamEnhanceFangShi
SetParamFunctions[EnumPlayerFightProp.EnhanceFangShiMul] = CLuaPlayerPropertyFight.SetParamEnhanceFangShiMul
SetParamFunctions[EnumPlayerFightProp.EnhanceYiShi] = CLuaPlayerPropertyFight.SetParamEnhanceYiShi
SetParamFunctions[EnumPlayerFightProp.EnhanceYiShiMul] = CLuaPlayerPropertyFight.SetParamEnhanceYiShiMul
SetParamFunctions[EnumPlayerFightProp.EnhanceMeiZhe] = CLuaPlayerPropertyFight.SetParamEnhanceMeiZhe
SetParamFunctions[EnumPlayerFightProp.EnhanceMeiZheMul] = CLuaPlayerPropertyFight.SetParamEnhanceMeiZheMul
SetParamFunctions[EnumPlayerFightProp.EnhanceYiRen] = CLuaPlayerPropertyFight.SetParamEnhanceYiRen
SetParamFunctions[EnumPlayerFightProp.EnhanceYiRenMul] = CLuaPlayerPropertyFight.SetParamEnhanceYiRenMul
SetParamFunctions[EnumPlayerFightProp.IgnorepDef] = CLuaPlayerPropertyFight.SetParamIgnorepDef
SetParamFunctions[EnumPlayerFightProp.IgnoremDef] = CLuaPlayerPropertyFight.SetParamIgnoremDef
SetParamFunctions[EnumPlayerFightProp.EnhanceZhaoHuan] = CLuaPlayerPropertyFight.SetParamEnhanceZhaoHuan
SetParamFunctions[EnumPlayerFightProp.EnhanceZhaoHuanMul] = CLuaPlayerPropertyFight.SetParamEnhanceZhaoHuanMul
SetParamFunctions[EnumPlayerFightProp.DizzyProbability] = CLuaPlayerPropertyFight.SetParamDizzyProbability
SetParamFunctions[EnumPlayerFightProp.SleepProbability] = CLuaPlayerPropertyFight.SetParamSleepProbability
SetParamFunctions[EnumPlayerFightProp.ChaosProbability] = CLuaPlayerPropertyFight.SetParamChaosProbability
SetParamFunctions[EnumPlayerFightProp.BindProbability] = CLuaPlayerPropertyFight.SetParamBindProbability
SetParamFunctions[EnumPlayerFightProp.SilenceProbability] = CLuaPlayerPropertyFight.SetParamSilenceProbability
SetParamFunctions[EnumPlayerFightProp.HpFullPow2] = CLuaPlayerPropertyFight.SetParamHpFullPow2
SetParamFunctions[EnumPlayerFightProp.HpFullPow1] = CLuaPlayerPropertyFight.SetParamHpFullPow1
SetParamFunctions[EnumPlayerFightProp.HpFullInit] = CLuaPlayerPropertyFight.SetParamHpFullInit
SetParamFunctions[EnumPlayerFightProp.MpFullPow1] = CLuaPlayerPropertyFight.SetParamMpFullPow1
SetParamFunctions[EnumPlayerFightProp.MpFullInit] = CLuaPlayerPropertyFight.SetParamMpFullInit
SetParamFunctions[EnumPlayerFightProp.MpRecoverPow1] = CLuaPlayerPropertyFight.SetParamMpRecoverPow1
SetParamFunctions[EnumPlayerFightProp.MpRecoverInit] = CLuaPlayerPropertyFight.SetParamMpRecoverInit
SetParamFunctions[EnumPlayerFightProp.PAttMinPow2] = CLuaPlayerPropertyFight.SetParamPAttMinPow2
SetParamFunctions[EnumPlayerFightProp.PAttMinPow1] = CLuaPlayerPropertyFight.SetParamPAttMinPow1
SetParamFunctions[EnumPlayerFightProp.PAttMinInit] = CLuaPlayerPropertyFight.SetParamPAttMinInit
SetParamFunctions[EnumPlayerFightProp.PAttMaxPow2] = CLuaPlayerPropertyFight.SetParamPAttMaxPow2
SetParamFunctions[EnumPlayerFightProp.PAttMaxPow1] = CLuaPlayerPropertyFight.SetParamPAttMaxPow1
SetParamFunctions[EnumPlayerFightProp.PAttMaxInit] = CLuaPlayerPropertyFight.SetParamPAttMaxInit
SetParamFunctions[EnumPlayerFightProp.PhitPow1] = CLuaPlayerPropertyFight.SetParamPhitPow1
SetParamFunctions[EnumPlayerFightProp.PHitInit] = CLuaPlayerPropertyFight.SetParamPHitInit
SetParamFunctions[EnumPlayerFightProp.PMissPow1] = CLuaPlayerPropertyFight.SetParamPMissPow1
SetParamFunctions[EnumPlayerFightProp.PMissInit] = CLuaPlayerPropertyFight.SetParamPMissInit
SetParamFunctions[EnumPlayerFightProp.PSpeedPow1] = CLuaPlayerPropertyFight.SetParamPSpeedPow1
SetParamFunctions[EnumPlayerFightProp.PSpeedInit] = CLuaPlayerPropertyFight.SetParamPSpeedInit
SetParamFunctions[EnumPlayerFightProp.PDefPow1] = CLuaPlayerPropertyFight.SetParamPDefPow1
SetParamFunctions[EnumPlayerFightProp.PDefInit] = CLuaPlayerPropertyFight.SetParamPDefInit
SetParamFunctions[EnumPlayerFightProp.PfatalPow1] = CLuaPlayerPropertyFight.SetParamPfatalPow1
SetParamFunctions[EnumPlayerFightProp.PFatalInit] = CLuaPlayerPropertyFight.SetParamPFatalInit
SetParamFunctions[EnumPlayerFightProp.MAttMinPow2] = CLuaPlayerPropertyFight.SetParamMAttMinPow2
SetParamFunctions[EnumPlayerFightProp.MAttMinPow1] = CLuaPlayerPropertyFight.SetParamMAttMinPow1
SetParamFunctions[EnumPlayerFightProp.MAttMinInit] = CLuaPlayerPropertyFight.SetParamMAttMinInit
SetParamFunctions[EnumPlayerFightProp.MAttMaxPow2] = CLuaPlayerPropertyFight.SetParamMAttMaxPow2
SetParamFunctions[EnumPlayerFightProp.MAttMaxPow1] = CLuaPlayerPropertyFight.SetParamMAttMaxPow1
SetParamFunctions[EnumPlayerFightProp.MAttMaxInit] = CLuaPlayerPropertyFight.SetParamMAttMaxInit
SetParamFunctions[EnumPlayerFightProp.MSpeedPow1] = CLuaPlayerPropertyFight.SetParamMSpeedPow1
SetParamFunctions[EnumPlayerFightProp.MSpeedInit] = CLuaPlayerPropertyFight.SetParamMSpeedInit
SetParamFunctions[EnumPlayerFightProp.MDefPow1] = CLuaPlayerPropertyFight.SetParamMDefPow1
SetParamFunctions[EnumPlayerFightProp.MDefInit] = CLuaPlayerPropertyFight.SetParamMDefInit
SetParamFunctions[EnumPlayerFightProp.MHitPow1] = CLuaPlayerPropertyFight.SetParamMHitPow1
SetParamFunctions[EnumPlayerFightProp.MHitInit] = CLuaPlayerPropertyFight.SetParamMHitInit
SetParamFunctions[EnumPlayerFightProp.MMissPow1] = CLuaPlayerPropertyFight.SetParamMMissPow1
SetParamFunctions[EnumPlayerFightProp.MMissInit] = CLuaPlayerPropertyFight.SetParamMMissInit
SetParamFunctions[EnumPlayerFightProp.MFatalPow1] = CLuaPlayerPropertyFight.SetParamMFatalPow1
SetParamFunctions[EnumPlayerFightProp.MFatalInit] = CLuaPlayerPropertyFight.SetParamMFatalInit
SetParamFunctions[EnumPlayerFightProp.BlockDamagePow1] = CLuaPlayerPropertyFight.SetParamBlockDamagePow1
SetParamFunctions[EnumPlayerFightProp.BlockDamageInit] = CLuaPlayerPropertyFight.SetParamBlockDamageInit
SetParamFunctions[EnumPlayerFightProp.RunSpeed] = CLuaPlayerPropertyFight.SetParamRunSpeed
SetParamFunctions[EnumPlayerFightProp.HpRecoverPow1] = CLuaPlayerPropertyFight.SetParamHpRecoverPow1
SetParamFunctions[EnumPlayerFightProp.HpRecoverInit] = CLuaPlayerPropertyFight.SetParamHpRecoverInit
SetParamFunctions[EnumPlayerFightProp.AntiBlockDamagePow1] = CLuaPlayerPropertyFight.SetParamAntiBlockDamagePow1
SetParamFunctions[EnumPlayerFightProp.AntiBlockDamageInit] = CLuaPlayerPropertyFight.SetParamAntiBlockDamageInit
SetParamFunctions[EnumPlayerFightProp.AdjBBMax] = CLuaPlayerPropertyFight.SetParamAdjBBMax
SetParamFunctions[EnumPlayerFightProp.AdjAntiBreak] = CLuaPlayerPropertyFight.SetParamAdjAntiBreak
SetParamFunctions[EnumPlayerFightProp.AdjAntiPushPull] = CLuaPlayerPropertyFight.SetParamAdjAntiPushPull
SetParamFunctions[EnumPlayerFightProp.AdjAntiPetrify] = CLuaPlayerPropertyFight.SetParamAdjAntiPetrify
SetParamFunctions[EnumPlayerFightProp.MulAntiPetrify] = CLuaPlayerPropertyFight.SetParamMulAntiPetrify
SetParamFunctions[EnumPlayerFightProp.AdjAntiTie] = CLuaPlayerPropertyFight.SetParamAdjAntiTie
SetParamFunctions[EnumPlayerFightProp.MulAntiTie] = CLuaPlayerPropertyFight.SetParamMulAntiTie
SetParamFunctions[EnumPlayerFightProp.AdjEnhancePetrify] = CLuaPlayerPropertyFight.SetParamAdjEnhancePetrify
SetParamFunctions[EnumPlayerFightProp.MulEnhancePetrify] = CLuaPlayerPropertyFight.SetParamMulEnhancePetrify
SetParamFunctions[EnumPlayerFightProp.AdjEnhanceTie] = CLuaPlayerPropertyFight.SetParamAdjEnhanceTie
SetParamFunctions[EnumPlayerFightProp.MulEnhanceTie] = CLuaPlayerPropertyFight.SetParamMulEnhanceTie
SetParamFunctions[EnumPlayerFightProp.TieProbability] = CLuaPlayerPropertyFight.SetParamTieProbability
SetParamFunctions[EnumPlayerFightProp.StrZizhi] = CLuaPlayerPropertyFight.SetParamStrZizhi
SetParamFunctions[EnumPlayerFightProp.CorZizhi] = CLuaPlayerPropertyFight.SetParamCorZizhi
SetParamFunctions[EnumPlayerFightProp.StaZizhi] = CLuaPlayerPropertyFight.SetParamStaZizhi
SetParamFunctions[EnumPlayerFightProp.AgiZizhi] = CLuaPlayerPropertyFight.SetParamAgiZizhi
SetParamFunctions[EnumPlayerFightProp.IntZizhi] = CLuaPlayerPropertyFight.SetParamIntZizhi
SetParamFunctions[EnumPlayerFightProp.InitStrZizhi] = CLuaPlayerPropertyFight.SetParamInitStrZizhi
SetParamFunctions[EnumPlayerFightProp.InitCorZizhi] = CLuaPlayerPropertyFight.SetParamInitCorZizhi
SetParamFunctions[EnumPlayerFightProp.InitStaZizhi] = CLuaPlayerPropertyFight.SetParamInitStaZizhi
SetParamFunctions[EnumPlayerFightProp.InitAgiZizhi] = CLuaPlayerPropertyFight.SetParamInitAgiZizhi
SetParamFunctions[EnumPlayerFightProp.InitIntZizhi] = CLuaPlayerPropertyFight.SetParamInitIntZizhi
SetParamFunctions[EnumPlayerFightProp.Wuxing] = CLuaPlayerPropertyFight.SetParamWuxing
SetParamFunctions[EnumPlayerFightProp.Xiuwei] = CLuaPlayerPropertyFight.SetParamXiuwei
SetParamFunctions[EnumPlayerFightProp.SkillNum] = CLuaPlayerPropertyFight.SetParamSkillNum
SetParamFunctions[EnumPlayerFightProp.PType] = CLuaPlayerPropertyFight.SetParamPType
SetParamFunctions[EnumPlayerFightProp.MType] = CLuaPlayerPropertyFight.SetParamMType
SetParamFunctions[EnumPlayerFightProp.PHType] = CLuaPlayerPropertyFight.SetParamPHType
SetParamFunctions[EnumPlayerFightProp.GrowFactor] = CLuaPlayerPropertyFight.SetParamGrowFactor
SetParamFunctions[EnumPlayerFightProp.WuxingImprove] = CLuaPlayerPropertyFight.SetParamWuxingImprove
SetParamFunctions[EnumPlayerFightProp.XiuweiImprove] = CLuaPlayerPropertyFight.SetParamXiuweiImprove
SetParamFunctions[EnumPlayerFightProp.AdjEnhanceBeHeal] = CLuaPlayerPropertyFight.SetParamAdjEnhanceBeHeal
SetParamFunctions[EnumPlayerFightProp.MulEnhanceBeHeal] = CLuaPlayerPropertyFight.SetParamMulEnhanceBeHeal
SetParamFunctions[EnumPlayerFightProp.AdjHpFull2] = CLuaPlayerPropertyFight.SetParamAdjHpFull2
SetParamFunctions[EnumPlayerFightProp.AdjpAttMin2] = CLuaPlayerPropertyFight.SetParamAdjpAttMin2
SetParamFunctions[EnumPlayerFightProp.AdjpAttMax2] = CLuaPlayerPropertyFight.SetParamAdjpAttMax2
SetParamFunctions[EnumPlayerFightProp.AdjmAttMin2] = CLuaPlayerPropertyFight.SetParamAdjmAttMin2
SetParamFunctions[EnumPlayerFightProp.AdjmAttMax2] = CLuaPlayerPropertyFight.SetParamAdjmAttMax2
SetParamFunctions[EnumPlayerFightProp.LingShouType] = CLuaPlayerPropertyFight.SetParamLingShouType
SetParamFunctions[EnumPlayerFightProp.MHType] = CLuaPlayerPropertyFight.SetParamMHType
SetParamFunctions[EnumPlayerFightProp.EnhanceLingShou] = CLuaPlayerPropertyFight.SetParamEnhanceLingShou
SetParamFunctions[EnumPlayerFightProp.EnhanceLingShouMul] = CLuaPlayerPropertyFight.SetParamEnhanceLingShouMul
SetParamFunctions[EnumPlayerFightProp.AdjAntiAoe] = CLuaPlayerPropertyFight.SetParamAdjAntiAoe
SetParamFunctions[EnumPlayerFightProp.AdjAntiBlind] = CLuaPlayerPropertyFight.SetParamAdjAntiBlind
SetParamFunctions[EnumPlayerFightProp.MulAntiBlind] = CLuaPlayerPropertyFight.SetParamMulAntiBlind
SetParamFunctions[EnumPlayerFightProp.AdjAntiDecelerate] = CLuaPlayerPropertyFight.SetParamAdjAntiDecelerate
SetParamFunctions[EnumPlayerFightProp.MulAntiDecelerate] = CLuaPlayerPropertyFight.SetParamMulAntiDecelerate
SetParamFunctions[EnumPlayerFightProp.MinGrade] = CLuaPlayerPropertyFight.SetParamMinGrade
SetParamFunctions[EnumPlayerFightProp.EnhanceDaoKe] = CLuaPlayerPropertyFight.SetParamEnhanceDaoKe
SetParamFunctions[EnumPlayerFightProp.EnhanceDaoKeMul] = CLuaPlayerPropertyFight.SetParamEnhanceDaoKeMul
SetParamFunctions[EnumPlayerFightProp.ZuoQiSpeed] = CLuaPlayerPropertyFight.SetParamZuoQiSpeed
SetParamFunctions[EnumPlayerFightProp.AdjEnhanceBianHu] = CLuaPlayerPropertyFight.SetParamAdjEnhanceBianHu
SetParamFunctions[EnumPlayerFightProp.MulEnhanceBianHu] = CLuaPlayerPropertyFight.SetParamMulEnhanceBianHu
SetParamFunctions[EnumPlayerFightProp.AdjAntiBianHu] = CLuaPlayerPropertyFight.SetParamAdjAntiBianHu
SetParamFunctions[EnumPlayerFightProp.MulAntiBianHu] = CLuaPlayerPropertyFight.SetParamMulAntiBianHu
SetParamFunctions[EnumPlayerFightProp.EnhanceXiaKe] = CLuaPlayerPropertyFight.SetParamEnhanceXiaKe
SetParamFunctions[EnumPlayerFightProp.EnhanceXiaKeMul] = CLuaPlayerPropertyFight.SetParamEnhanceXiaKeMul
SetParamFunctions[EnumPlayerFightProp.TieTimeChange] = CLuaPlayerPropertyFight.SetParamTieTimeChange
SetParamFunctions[EnumPlayerFightProp.EnhanceYanShi] = CLuaPlayerPropertyFight.SetParamEnhanceYanShi
SetParamFunctions[EnumPlayerFightProp.EnhanceYanShiMul] = CLuaPlayerPropertyFight.SetParamEnhanceYanShiMul
SetParamFunctions[EnumPlayerFightProp.PAType] = CLuaPlayerPropertyFight.SetParamPAType
SetParamFunctions[EnumPlayerFightProp.MAType] = CLuaPlayerPropertyFight.SetParamMAType
SetParamFunctions[EnumPlayerFightProp.lifetimeNoReduceRate] = CLuaPlayerPropertyFight.SetParamlifetimeNoReduceRate
SetParamFunctions[EnumPlayerFightProp.AddLSHpDurgImprove] = CLuaPlayerPropertyFight.SetParamAddLSHpDurgImprove
SetParamFunctions[EnumPlayerFightProp.AddHpDurgImprove] = CLuaPlayerPropertyFight.SetParamAddHpDurgImprove
SetParamFunctions[EnumPlayerFightProp.AddMpDurgImprove] = CLuaPlayerPropertyFight.SetParamAddMpDurgImprove
SetParamFunctions[EnumPlayerFightProp.FireHurtReduce] = CLuaPlayerPropertyFight.SetParamFireHurtReduce
SetParamFunctions[EnumPlayerFightProp.ThunderHurtReduce] = CLuaPlayerPropertyFight.SetParamThunderHurtReduce
SetParamFunctions[EnumPlayerFightProp.IceHurtReduce] = CLuaPlayerPropertyFight.SetParamIceHurtReduce
SetParamFunctions[EnumPlayerFightProp.PoisonHurtReduce] = CLuaPlayerPropertyFight.SetParamPoisonHurtReduce
SetParamFunctions[EnumPlayerFightProp.WindHurtReduce] = CLuaPlayerPropertyFight.SetParamWindHurtReduce
SetParamFunctions[EnumPlayerFightProp.LightHurtReduce] = CLuaPlayerPropertyFight.SetParamLightHurtReduce
SetParamFunctions[EnumPlayerFightProp.IllusionHurtReduce] = CLuaPlayerPropertyFight.SetParamIllusionHurtReduce
SetParamFunctions[EnumPlayerFightProp.FakepDef] = CLuaPlayerPropertyFight.SetParamFakepDef
SetParamFunctions[EnumPlayerFightProp.FakemDef] = CLuaPlayerPropertyFight.SetParamFakemDef
SetParamFunctions[EnumPlayerFightProp.AdjEnhanceWater] = CLuaPlayerPropertyFight.SetParamAdjEnhanceWater
SetParamFunctions[EnumPlayerFightProp.MulEnhanceWater] = CLuaPlayerPropertyFight.SetParamMulEnhanceWater
SetParamFunctions[EnumPlayerFightProp.AdjAntiWater] = CLuaPlayerPropertyFight.SetParamAdjAntiWater
SetParamFunctions[EnumPlayerFightProp.MulAntiWater] = CLuaPlayerPropertyFight.SetParamMulAntiWater
SetParamFunctions[EnumPlayerFightProp.WaterHurtReduce] = CLuaPlayerPropertyFight.SetParamWaterHurtReduce
SetParamFunctions[EnumPlayerFightProp.AdjAntiFreeze] = CLuaPlayerPropertyFight.SetParamAdjAntiFreeze
SetParamFunctions[EnumPlayerFightProp.MulAntiFreeze] = CLuaPlayerPropertyFight.SetParamMulAntiFreeze
SetParamFunctions[EnumPlayerFightProp.AdjEnhanceFreeze] = CLuaPlayerPropertyFight.SetParamAdjEnhanceFreeze
SetParamFunctions[EnumPlayerFightProp.MulEnhanceFreeze] = CLuaPlayerPropertyFight.SetParamMulEnhanceFreeze
SetParamFunctions[EnumPlayerFightProp.EnhanceHuaHun] = CLuaPlayerPropertyFight.SetParamEnhanceHuaHun
SetParamFunctions[EnumPlayerFightProp.EnhanceHuaHunMul] = CLuaPlayerPropertyFight.SetParamEnhanceHuaHunMul
SetParamFunctions[EnumPlayerFightProp.TrueSightProb] = CLuaPlayerPropertyFight.SetParamTrueSightProb
SetParamFunctions[EnumPlayerFightProp.LSBBGrade] = CLuaPlayerPropertyFight.SetParamLSBBGrade
SetParamFunctions[EnumPlayerFightProp.LSBBQuality] = CLuaPlayerPropertyFight.SetParamLSBBQuality
SetParamFunctions[EnumPlayerFightProp.LSBBAdjHp] = CLuaPlayerPropertyFight.SetParamLSBBAdjHp
SetParamFunctions[EnumPlayerFightProp.AdjIgnoreAntiFire] = CLuaPlayerPropertyFight.SetParamAdjIgnoreAntiFire
SetParamFunctions[EnumPlayerFightProp.AdjIgnoreAntiThunder] = CLuaPlayerPropertyFight.SetParamAdjIgnoreAntiThunder
SetParamFunctions[EnumPlayerFightProp.AdjIgnoreAntiIce] = CLuaPlayerPropertyFight.SetParamAdjIgnoreAntiIce
SetParamFunctions[EnumPlayerFightProp.AdjIgnoreAntiPoison] = CLuaPlayerPropertyFight.SetParamAdjIgnoreAntiPoison
SetParamFunctions[EnumPlayerFightProp.AdjIgnoreAntiWind] = CLuaPlayerPropertyFight.SetParamAdjIgnoreAntiWind
SetParamFunctions[EnumPlayerFightProp.AdjIgnoreAntiLight] = CLuaPlayerPropertyFight.SetParamAdjIgnoreAntiLight
SetParamFunctions[EnumPlayerFightProp.AdjIgnoreAntiIllusion] = CLuaPlayerPropertyFight.SetParamAdjIgnoreAntiIllusion
SetParamFunctions[EnumPlayerFightProp.IgnoreAntiFireLSMul] = CLuaPlayerPropertyFight.SetParamIgnoreAntiFireLSMul
SetParamFunctions[EnumPlayerFightProp.IgnoreAntiThunderLSMul] = CLuaPlayerPropertyFight.SetParamIgnoreAntiThunderLSMul
SetParamFunctions[EnumPlayerFightProp.IgnoreAntiIceLSMul] = CLuaPlayerPropertyFight.SetParamIgnoreAntiIceLSMul
SetParamFunctions[EnumPlayerFightProp.IgnoreAntiPoisonLSMul] = CLuaPlayerPropertyFight.SetParamIgnoreAntiPoisonLSMul
SetParamFunctions[EnumPlayerFightProp.IgnoreAntiWindLSMul] = CLuaPlayerPropertyFight.SetParamIgnoreAntiWindLSMul
SetParamFunctions[EnumPlayerFightProp.IgnoreAntiLightLSMul] = CLuaPlayerPropertyFight.SetParamIgnoreAntiLightLSMul
SetParamFunctions[EnumPlayerFightProp.IgnoreAntiIllusionLSMul] = CLuaPlayerPropertyFight.SetParamIgnoreAntiIllusionLSMul
SetParamFunctions[EnumPlayerFightProp.AdjIgnoreAntiWater] = CLuaPlayerPropertyFight.SetParamAdjIgnoreAntiWater
SetParamFunctions[EnumPlayerFightProp.IgnoreAntiWaterLSMul] = CLuaPlayerPropertyFight.SetParamIgnoreAntiWaterLSMul
SetParamFunctions[EnumPlayerFightProp.MulSpeed2] = CLuaPlayerPropertyFight.SetParamMulSpeed2
SetParamFunctions[EnumPlayerFightProp.MulConsumeMp] = CLuaPlayerPropertyFight.SetParamMulConsumeMp
SetParamFunctions[EnumPlayerFightProp.AdjAgi2] = CLuaPlayerPropertyFight.SetParamAdjAgi2
SetParamFunctions[EnumPlayerFightProp.AdjAntiBianHu2] = CLuaPlayerPropertyFight.SetParamAdjAntiBianHu2
SetParamFunctions[EnumPlayerFightProp.AdjAntiBind2] = CLuaPlayerPropertyFight.SetParamAdjAntiBind2
SetParamFunctions[EnumPlayerFightProp.AdjAntiBlind2] = CLuaPlayerPropertyFight.SetParamAdjAntiBlind2
SetParamFunctions[EnumPlayerFightProp.AdjAntiBlock2] = CLuaPlayerPropertyFight.SetParamAdjAntiBlock2
SetParamFunctions[EnumPlayerFightProp.AdjAntiBlockDamage2] = CLuaPlayerPropertyFight.SetParamAdjAntiBlockDamage2
SetParamFunctions[EnumPlayerFightProp.AdjAntiChaos2] = CLuaPlayerPropertyFight.SetParamAdjAntiChaos2
SetParamFunctions[EnumPlayerFightProp.AdjAntiDecelerate2] = CLuaPlayerPropertyFight.SetParamAdjAntiDecelerate2
SetParamFunctions[EnumPlayerFightProp.AdjAntiDizzy2] = CLuaPlayerPropertyFight.SetParamAdjAntiDizzy2
SetParamFunctions[EnumPlayerFightProp.AdjAntiFire2] = CLuaPlayerPropertyFight.SetParamAdjAntiFire2
SetParamFunctions[EnumPlayerFightProp.AdjAntiFreeze2] = CLuaPlayerPropertyFight.SetParamAdjAntiFreeze2
SetParamFunctions[EnumPlayerFightProp.AdjAntiIce2] = CLuaPlayerPropertyFight.SetParamAdjAntiIce2
SetParamFunctions[EnumPlayerFightProp.AdjAntiIllusion2] = CLuaPlayerPropertyFight.SetParamAdjAntiIllusion2
SetParamFunctions[EnumPlayerFightProp.AdjAntiLight2] = CLuaPlayerPropertyFight.SetParamAdjAntiLight2
SetParamFunctions[EnumPlayerFightProp.AdjAntimFatal2] = CLuaPlayerPropertyFight.SetParamAdjAntimFatal2
SetParamFunctions[EnumPlayerFightProp.AdjAntimFatalDamage2] = CLuaPlayerPropertyFight.SetParamAdjAntimFatalDamage2
SetParamFunctions[EnumPlayerFightProp.AdjAntiPetrify2] = CLuaPlayerPropertyFight.SetParamAdjAntiPetrify2
SetParamFunctions[EnumPlayerFightProp.AdjAntipFatal2] = CLuaPlayerPropertyFight.SetParamAdjAntipFatal2
SetParamFunctions[EnumPlayerFightProp.AdjAntipFatalDamage2] = CLuaPlayerPropertyFight.SetParamAdjAntipFatalDamage2
SetParamFunctions[EnumPlayerFightProp.AdjAntiPoison2] = CLuaPlayerPropertyFight.SetParamAdjAntiPoison2
SetParamFunctions[EnumPlayerFightProp.AdjAntiSilence2] = CLuaPlayerPropertyFight.SetParamAdjAntiSilence2
SetParamFunctions[EnumPlayerFightProp.AdjAntiSleep2] = CLuaPlayerPropertyFight.SetParamAdjAntiSleep2
SetParamFunctions[EnumPlayerFightProp.AdjAntiThunder2] = CLuaPlayerPropertyFight.SetParamAdjAntiThunder2
SetParamFunctions[EnumPlayerFightProp.AdjAntiTie2] = CLuaPlayerPropertyFight.SetParamAdjAntiTie2
SetParamFunctions[EnumPlayerFightProp.AdjAntiWater2] = CLuaPlayerPropertyFight.SetParamAdjAntiWater2
SetParamFunctions[EnumPlayerFightProp.AdjAntiWind2] = CLuaPlayerPropertyFight.SetParamAdjAntiWind2
SetParamFunctions[EnumPlayerFightProp.AdjBlock2] = CLuaPlayerPropertyFight.SetParamAdjBlock2
SetParamFunctions[EnumPlayerFightProp.AdjBlockDamage2] = CLuaPlayerPropertyFight.SetParamAdjBlockDamage2
SetParamFunctions[EnumPlayerFightProp.AdjCor2] = CLuaPlayerPropertyFight.SetParamAdjCor2
SetParamFunctions[EnumPlayerFightProp.AdjEnhanceBianHu2] = CLuaPlayerPropertyFight.SetParamAdjEnhanceBianHu2
SetParamFunctions[EnumPlayerFightProp.AdjEnhanceBind2] = CLuaPlayerPropertyFight.SetParamAdjEnhanceBind2
SetParamFunctions[EnumPlayerFightProp.AdjEnhanceChaos2] = CLuaPlayerPropertyFight.SetParamAdjEnhanceChaos2
SetParamFunctions[EnumPlayerFightProp.AdjEnhanceDizzy2] = CLuaPlayerPropertyFight.SetParamAdjEnhanceDizzy2
SetParamFunctions[EnumPlayerFightProp.AdjEnhanceFire2] = CLuaPlayerPropertyFight.SetParamAdjEnhanceFire2
SetParamFunctions[EnumPlayerFightProp.AdjEnhanceFreeze2] = CLuaPlayerPropertyFight.SetParamAdjEnhanceFreeze2
SetParamFunctions[EnumPlayerFightProp.AdjEnhanceIce2] = CLuaPlayerPropertyFight.SetParamAdjEnhanceIce2
SetParamFunctions[EnumPlayerFightProp.AdjEnhanceIllusion2] = CLuaPlayerPropertyFight.SetParamAdjEnhanceIllusion2
SetParamFunctions[EnumPlayerFightProp.AdjEnhanceLight2] = CLuaPlayerPropertyFight.SetParamAdjEnhanceLight2
SetParamFunctions[EnumPlayerFightProp.AdjEnhancePetrify2] = CLuaPlayerPropertyFight.SetParamAdjEnhancePetrify2
SetParamFunctions[EnumPlayerFightProp.AdjEnhancePoison2] = CLuaPlayerPropertyFight.SetParamAdjEnhancePoison2
SetParamFunctions[EnumPlayerFightProp.AdjEnhanceSilence2] = CLuaPlayerPropertyFight.SetParamAdjEnhanceSilence2
SetParamFunctions[EnumPlayerFightProp.AdjEnhanceSleep2] = CLuaPlayerPropertyFight.SetParamAdjEnhanceSleep2
SetParamFunctions[EnumPlayerFightProp.AdjEnhanceThunder2] = CLuaPlayerPropertyFight.SetParamAdjEnhanceThunder2
SetParamFunctions[EnumPlayerFightProp.AdjEnhanceTie2] = CLuaPlayerPropertyFight.SetParamAdjEnhanceTie2
SetParamFunctions[EnumPlayerFightProp.AdjEnhanceWater2] = CLuaPlayerPropertyFight.SetParamAdjEnhanceWater2
SetParamFunctions[EnumPlayerFightProp.AdjEnhanceWind2] = CLuaPlayerPropertyFight.SetParamAdjEnhanceWind2
SetParamFunctions[EnumPlayerFightProp.AdjIgnoreAntiFire2] = CLuaPlayerPropertyFight.SetParamAdjIgnoreAntiFire2
SetParamFunctions[EnumPlayerFightProp.AdjIgnoreAntiIce2] = CLuaPlayerPropertyFight.SetParamAdjIgnoreAntiIce2
SetParamFunctions[EnumPlayerFightProp.AdjIgnoreAntiIllusion2] = CLuaPlayerPropertyFight.SetParamAdjIgnoreAntiIllusion2
SetParamFunctions[EnumPlayerFightProp.AdjIgnoreAntiLight2] = CLuaPlayerPropertyFight.SetParamAdjIgnoreAntiLight2
SetParamFunctions[EnumPlayerFightProp.AdjIgnoreAntiPoison2] = CLuaPlayerPropertyFight.SetParamAdjIgnoreAntiPoison2
SetParamFunctions[EnumPlayerFightProp.AdjIgnoreAntiThunder2] = CLuaPlayerPropertyFight.SetParamAdjIgnoreAntiThunder2
SetParamFunctions[EnumPlayerFightProp.AdjIgnoreAntiWater2] = CLuaPlayerPropertyFight.SetParamAdjIgnoreAntiWater2
SetParamFunctions[EnumPlayerFightProp.AdjIgnoreAntiWind2] = CLuaPlayerPropertyFight.SetParamAdjIgnoreAntiWind2
SetParamFunctions[EnumPlayerFightProp.AdjInt2] = CLuaPlayerPropertyFight.SetParamAdjInt2
SetParamFunctions[EnumPlayerFightProp.AdjmDef2] = CLuaPlayerPropertyFight.SetParamAdjmDef2
SetParamFunctions[EnumPlayerFightProp.AdjmFatal2] = CLuaPlayerPropertyFight.SetParamAdjmFatal2
SetParamFunctions[EnumPlayerFightProp.AdjmFatalDamage2] = CLuaPlayerPropertyFight.SetParamAdjmFatalDamage2
SetParamFunctions[EnumPlayerFightProp.AdjmHit2] = CLuaPlayerPropertyFight.SetParamAdjmHit2
SetParamFunctions[EnumPlayerFightProp.AdjmHurt2] = CLuaPlayerPropertyFight.SetParamAdjmHurt2
SetParamFunctions[EnumPlayerFightProp.AdjmMiss2] = CLuaPlayerPropertyFight.SetParamAdjmMiss2
SetParamFunctions[EnumPlayerFightProp.AdjmSpeed2] = CLuaPlayerPropertyFight.SetParamAdjmSpeed2
SetParamFunctions[EnumPlayerFightProp.AdjpDef2] = CLuaPlayerPropertyFight.SetParamAdjpDef2
SetParamFunctions[EnumPlayerFightProp.AdjpFatal2] = CLuaPlayerPropertyFight.SetParamAdjpFatal2
SetParamFunctions[EnumPlayerFightProp.AdjpFatalDamage2] = CLuaPlayerPropertyFight.SetParamAdjpFatalDamage2
SetParamFunctions[EnumPlayerFightProp.AdjpHit2] = CLuaPlayerPropertyFight.SetParamAdjpHit2
SetParamFunctions[EnumPlayerFightProp.AdjpHurt2] = CLuaPlayerPropertyFight.SetParamAdjpHurt2
SetParamFunctions[EnumPlayerFightProp.AdjpMiss2] = CLuaPlayerPropertyFight.SetParamAdjpMiss2
SetParamFunctions[EnumPlayerFightProp.AdjpSpeed2] = CLuaPlayerPropertyFight.SetParamAdjpSpeed2
SetParamFunctions[EnumPlayerFightProp.AdjStr2] = CLuaPlayerPropertyFight.SetParamAdjStr2
SetParamFunctions[EnumPlayerFightProp.LSpAttMin_adj] = CLuaPlayerPropertyFight.SetParamLSpAttMin_adj
SetParamFunctions[EnumPlayerFightProp.LSpAttMax_adj] = CLuaPlayerPropertyFight.SetParamLSpAttMax_adj
SetParamFunctions[EnumPlayerFightProp.LSmAttMin_adj] = CLuaPlayerPropertyFight.SetParamLSmAttMin_adj
SetParamFunctions[EnumPlayerFightProp.LSmAttMax_adj] = CLuaPlayerPropertyFight.SetParamLSmAttMax_adj
SetParamFunctions[EnumPlayerFightProp.LSpFatal_adj] = CLuaPlayerPropertyFight.SetParamLSpFatal_adj
SetParamFunctions[EnumPlayerFightProp.LSpFatalDamage_adj] = CLuaPlayerPropertyFight.SetParamLSpFatalDamage_adj
SetParamFunctions[EnumPlayerFightProp.LSmFatal_adj] = CLuaPlayerPropertyFight.SetParamLSmFatal_adj
SetParamFunctions[EnumPlayerFightProp.LSmFatalDamage_adj] = CLuaPlayerPropertyFight.SetParamLSmFatalDamage_adj
SetParamFunctions[EnumPlayerFightProp.LSAntiBlock_adj] = CLuaPlayerPropertyFight.SetParamLSAntiBlock_adj
SetParamFunctions[EnumPlayerFightProp.LSAntiBlockDamage_adj] = CLuaPlayerPropertyFight.SetParamLSAntiBlockDamage_adj
SetParamFunctions[EnumPlayerFightProp.LSIgnoreAntiFire_adj] = CLuaPlayerPropertyFight.SetParamLSIgnoreAntiFire_adj
SetParamFunctions[EnumPlayerFightProp.LSIgnoreAntiThunder_adj] = CLuaPlayerPropertyFight.SetParamLSIgnoreAntiThunder_adj
SetParamFunctions[EnumPlayerFightProp.LSIgnoreAntiIce_adj] = CLuaPlayerPropertyFight.SetParamLSIgnoreAntiIce_adj
SetParamFunctions[EnumPlayerFightProp.LSIgnoreAntiPoison_adj] = CLuaPlayerPropertyFight.SetParamLSIgnoreAntiPoison_adj
SetParamFunctions[EnumPlayerFightProp.LSIgnoreAntiWind_adj] = CLuaPlayerPropertyFight.SetParamLSIgnoreAntiWind_adj
SetParamFunctions[EnumPlayerFightProp.LSIgnoreAntiLight_adj] = CLuaPlayerPropertyFight.SetParamLSIgnoreAntiLight_adj
SetParamFunctions[EnumPlayerFightProp.LSIgnoreAntiIllusion_adj] = CLuaPlayerPropertyFight.SetParamLSIgnoreAntiIllusion_adj
SetParamFunctions[EnumPlayerFightProp.LSIgnoreAntiWater_adj] = CLuaPlayerPropertyFight.SetParamLSIgnoreAntiWater_adj
SetParamFunctions[EnumPlayerFightProp.LSpAttMin_jc] = CLuaPlayerPropertyFight.SetParamLSpAttMin_jc
SetParamFunctions[EnumPlayerFightProp.LSpAttMax_jc] = CLuaPlayerPropertyFight.SetParamLSpAttMax_jc
SetParamFunctions[EnumPlayerFightProp.LSmAttMin_jc] = CLuaPlayerPropertyFight.SetParamLSmAttMin_jc
SetParamFunctions[EnumPlayerFightProp.LSmAttMax_jc] = CLuaPlayerPropertyFight.SetParamLSmAttMax_jc
SetParamFunctions[EnumPlayerFightProp.LSpFatal_jc] = CLuaPlayerPropertyFight.SetParamLSpFatal_jc
SetParamFunctions[EnumPlayerFightProp.LSpFatalDamage_jc] = CLuaPlayerPropertyFight.SetParamLSpFatalDamage_jc
SetParamFunctions[EnumPlayerFightProp.LSmFatal_jc] = CLuaPlayerPropertyFight.SetParamLSmFatal_jc
SetParamFunctions[EnumPlayerFightProp.LSmFatalDamage_jc] = CLuaPlayerPropertyFight.SetParamLSmFatalDamage_jc
SetParamFunctions[EnumPlayerFightProp.LSAntiBlock_jc] = CLuaPlayerPropertyFight.SetParamLSAntiBlock_jc
SetParamFunctions[EnumPlayerFightProp.LSAntiBlockDamage_jc] = CLuaPlayerPropertyFight.SetParamLSAntiBlockDamage_jc
SetParamFunctions[EnumPlayerFightProp.LSIgnoreAntiFire_jc] = CLuaPlayerPropertyFight.SetParamLSIgnoreAntiFire_jc
SetParamFunctions[EnumPlayerFightProp.LSIgnoreAntiThunder_jc] = CLuaPlayerPropertyFight.SetParamLSIgnoreAntiThunder_jc
SetParamFunctions[EnumPlayerFightProp.LSIgnoreAntiIce_jc] = CLuaPlayerPropertyFight.SetParamLSIgnoreAntiIce_jc
SetParamFunctions[EnumPlayerFightProp.LSIgnoreAntiPoison_jc] = CLuaPlayerPropertyFight.SetParamLSIgnoreAntiPoison_jc
SetParamFunctions[EnumPlayerFightProp.LSIgnoreAntiWind_jc] = CLuaPlayerPropertyFight.SetParamLSIgnoreAntiWind_jc
SetParamFunctions[EnumPlayerFightProp.LSIgnoreAntiLight_jc] = CLuaPlayerPropertyFight.SetParamLSIgnoreAntiLight_jc
SetParamFunctions[EnumPlayerFightProp.LSIgnoreAntiIllusion_jc] = CLuaPlayerPropertyFight.SetParamLSIgnoreAntiIllusion_jc
SetParamFunctions[EnumPlayerFightProp.LSIgnoreAntiWater_jc] = CLuaPlayerPropertyFight.SetParamLSIgnoreAntiWater_jc
SetParamFunctions[EnumPlayerFightProp.LSIgnoreAntiFire_mul] = CLuaPlayerPropertyFight.SetParamLSIgnoreAntiFire_mul
SetParamFunctions[EnumPlayerFightProp.LSIgnoreAntiThunder_mul] = CLuaPlayerPropertyFight.SetParamLSIgnoreAntiThunder_mul
SetParamFunctions[EnumPlayerFightProp.LSIgnoreAntiIce_mul] = CLuaPlayerPropertyFight.SetParamLSIgnoreAntiIce_mul
SetParamFunctions[EnumPlayerFightProp.LSIgnoreAntiPoison_mul] = CLuaPlayerPropertyFight.SetParamLSIgnoreAntiPoison_mul
SetParamFunctions[EnumPlayerFightProp.LSIgnoreAntiWind_mul] = CLuaPlayerPropertyFight.SetParamLSIgnoreAntiWind_mul
SetParamFunctions[EnumPlayerFightProp.LSIgnoreAntiLight_mul] = CLuaPlayerPropertyFight.SetParamLSIgnoreAntiLight_mul
SetParamFunctions[EnumPlayerFightProp.LSIgnoreAntiIllusion_mul] = CLuaPlayerPropertyFight.SetParamLSIgnoreAntiIllusion_mul
SetParamFunctions[EnumPlayerFightProp.LSIgnoreAntiWater_mul] = CLuaPlayerPropertyFight.SetParamLSIgnoreAntiWater_mul
SetParamFunctions[EnumPlayerFightProp.JieBan_Child_QiChangColor] = CLuaPlayerPropertyFight.SetParamJieBan_Child_QiChangColor
SetParamFunctions[EnumPlayerFightProp.JieBan_LingShou_Ratio] = CLuaPlayerPropertyFight.SetParamJieBan_LingShou_Ratio
SetParamFunctions[EnumPlayerFightProp.JieBan_LingShou_QinMi] = CLuaPlayerPropertyFight.SetParamJieBan_LingShou_QinMi
SetParamFunctions[EnumPlayerFightProp.JieBan_LingShou_CorZizhi] = CLuaPlayerPropertyFight.SetParamJieBan_LingShou_CorZizhi
SetParamFunctions[EnumPlayerFightProp.JieBan_LingShou_StaZizhi] = CLuaPlayerPropertyFight.SetParamJieBan_LingShou_StaZizhi
SetParamFunctions[EnumPlayerFightProp.JieBan_LingShou_StrZizhi] = CLuaPlayerPropertyFight.SetParamJieBan_LingShou_StrZizhi
SetParamFunctions[EnumPlayerFightProp.JieBan_LingShou_IntZizhi] = CLuaPlayerPropertyFight.SetParamJieBan_LingShou_IntZizhi
SetParamFunctions[EnumPlayerFightProp.JieBan_LingShou_AgiZizhi] = CLuaPlayerPropertyFight.SetParamJieBan_LingShou_AgiZizhi
SetParamFunctions[EnumPlayerFightProp.Buff_DisplayValue1] = CLuaPlayerPropertyFight.SetParamBuff_DisplayValue1
SetParamFunctions[EnumPlayerFightProp.Buff_DisplayValue2] = CLuaPlayerPropertyFight.SetParamBuff_DisplayValue2
SetParamFunctions[EnumPlayerFightProp.Buff_DisplayValue3] = CLuaPlayerPropertyFight.SetParamBuff_DisplayValue3
SetParamFunctions[EnumPlayerFightProp.Buff_DisplayValue4] = CLuaPlayerPropertyFight.SetParamBuff_DisplayValue4
SetParamFunctions[EnumPlayerFightProp.Buff_DisplayValue5] = CLuaPlayerPropertyFight.SetParamBuff_DisplayValue5
SetParamFunctions[EnumPlayerFightProp.LSNature] = CLuaPlayerPropertyFight.SetParamLSNature
SetParamFunctions[EnumPlayerFightProp.PrayMulti] = CLuaPlayerPropertyFight.SetParamPrayMulti
SetParamFunctions[EnumPlayerFightProp.EnhanceYingLing] = CLuaPlayerPropertyFight.SetParamEnhanceYingLing
SetParamFunctions[EnumPlayerFightProp.EnhanceYingLingMul] = CLuaPlayerPropertyFight.SetParamEnhanceYingLingMul
SetParamFunctions[EnumPlayerFightProp.EnhanceFlag] = CLuaPlayerPropertyFight.SetParamEnhanceFlag
SetParamFunctions[EnumPlayerFightProp.EnhanceFlagMul] = CLuaPlayerPropertyFight.SetParamEnhanceFlagMul
SetParamFunctions[EnumPlayerFightProp.ObjectType] = CLuaPlayerPropertyFight.SetParamObjectType
SetParamFunctions[EnumPlayerFightProp.MonsterType] = CLuaPlayerPropertyFight.SetParamMonsterType
SetParamFunctions[EnumPlayerFightProp.FightPropType] = CLuaPlayerPropertyFight.SetParamFightPropType
SetParamFunctions[EnumPlayerFightProp.PlayHpFull] = CLuaPlayerPropertyFight.SetParamPlayHpFull
SetParamFunctions[EnumPlayerFightProp.PlayHp] = CLuaPlayerPropertyFight.SetParamPlayHp
SetParamFunctions[EnumPlayerFightProp.PlayAtt] = CLuaPlayerPropertyFight.SetParamPlayAtt
SetParamFunctions[EnumPlayerFightProp.PlayDef] = CLuaPlayerPropertyFight.SetParamPlayDef
SetParamFunctions[EnumPlayerFightProp.PlayHit] = CLuaPlayerPropertyFight.SetParamPlayHit
SetParamFunctions[EnumPlayerFightProp.PlayMiss] = CLuaPlayerPropertyFight.SetParamPlayMiss
SetParamFunctions[EnumPlayerFightProp.EnhanceDieKe] = CLuaPlayerPropertyFight.SetParamEnhanceDieKe
SetParamFunctions[EnumPlayerFightProp.EnhanceDieKeMul] = CLuaPlayerPropertyFight.SetParamEnhanceDieKeMul
SetParamFunctions[EnumPlayerFightProp.DotRemain] = CLuaPlayerPropertyFight.SetParamDotRemain
SetParamFunctions[EnumPlayerFightProp.IsConfused] = CLuaPlayerPropertyFight.SetParamIsConfused
SetParamFunctions[EnumPlayerFightProp.EnhanceSheShouKefu] = CLuaPlayerPropertyFight.SetParamEnhanceSheShouKefu
SetParamFunctions[EnumPlayerFightProp.EnhanceJiaShiKefu] = CLuaPlayerPropertyFight.SetParamEnhanceJiaShiKefu
SetParamFunctions[EnumPlayerFightProp.EnhanceDaoKeKefu] = CLuaPlayerPropertyFight.SetParamEnhanceDaoKeKefu
SetParamFunctions[EnumPlayerFightProp.EnhanceXiaKeKefu] = CLuaPlayerPropertyFight.SetParamEnhanceXiaKeKefu
SetParamFunctions[EnumPlayerFightProp.EnhanceFangShiKefu] = CLuaPlayerPropertyFight.SetParamEnhanceFangShiKefu
SetParamFunctions[EnumPlayerFightProp.EnhanceYiShiKefu] = CLuaPlayerPropertyFight.SetParamEnhanceYiShiKefu
SetParamFunctions[EnumPlayerFightProp.EnhanceMeiZheKefu] = CLuaPlayerPropertyFight.SetParamEnhanceMeiZheKefu
SetParamFunctions[EnumPlayerFightProp.EnhanceYiRenKefu] = CLuaPlayerPropertyFight.SetParamEnhanceYiRenKefu
SetParamFunctions[EnumPlayerFightProp.EnhanceYanShiKefu] = CLuaPlayerPropertyFight.SetParamEnhanceYanShiKefu
SetParamFunctions[EnumPlayerFightProp.EnhanceHuaHunKefu] = CLuaPlayerPropertyFight.SetParamEnhanceHuaHunKefu
SetParamFunctions[EnumPlayerFightProp.EnhanceYingLingKefu] = CLuaPlayerPropertyFight.SetParamEnhanceYingLingKefu
SetParamFunctions[EnumPlayerFightProp.EnhanceDieKeKefu] = CLuaPlayerPropertyFight.SetParamEnhanceDieKeKefu
SetParamFunctions[EnumPlayerFightProp.SoulCoreLevel] = CLuaPlayerPropertyFight.SetParamSoulCoreLevel
SetParamFunctions[EnumPlayerFightProp.AntiTian] = CLuaPlayerPropertyFight.SetParamAntiTian
SetParamFunctions[EnumPlayerFightProp.AntiEGui] = CLuaPlayerPropertyFight.SetParamAntiEGui
SetParamFunctions[EnumPlayerFightProp.AntiXiuLuo] = CLuaPlayerPropertyFight.SetParamAntiXiuLuo
SetParamFunctions[EnumPlayerFightProp.AntiDiYu] = CLuaPlayerPropertyFight.SetParamAntiDiYu
SetParamFunctions[EnumPlayerFightProp.AntiRen] = CLuaPlayerPropertyFight.SetParamAntiRen
SetParamFunctions[EnumPlayerFightProp.AntiChuSheng] = CLuaPlayerPropertyFight.SetParamAntiChuSheng
SetParamFunctions[EnumPlayerFightProp.AntiBuilding] = CLuaPlayerPropertyFight.SetParamAntiBuilding
SetParamFunctions[EnumPlayerFightProp.AntiZhaoHuan] = CLuaPlayerPropertyFight.SetParamAntiZhaoHuan
SetParamFunctions[EnumPlayerFightProp.AntiLingShou] = CLuaPlayerPropertyFight.SetParamAntiLingShou
SetParamFunctions[EnumPlayerFightProp.AntiBoss] = CLuaPlayerPropertyFight.SetParamAntiBoss
SetParamFunctions[EnumPlayerFightProp.AntiSheShou] = CLuaPlayerPropertyFight.SetParamAntiSheShou
SetParamFunctions[EnumPlayerFightProp.AntiJiaShi] = CLuaPlayerPropertyFight.SetParamAntiJiaShi
SetParamFunctions[EnumPlayerFightProp.AntiDaoKe] = CLuaPlayerPropertyFight.SetParamAntiDaoKe
SetParamFunctions[EnumPlayerFightProp.AntiXiaKe] = CLuaPlayerPropertyFight.SetParamAntiXiaKe
SetParamFunctions[EnumPlayerFightProp.AntiFangShi] = CLuaPlayerPropertyFight.SetParamAntiFangShi
SetParamFunctions[EnumPlayerFightProp.AntiYiShi] = CLuaPlayerPropertyFight.SetParamAntiYiShi
SetParamFunctions[EnumPlayerFightProp.AntiMeiZhe] = CLuaPlayerPropertyFight.SetParamAntiMeiZhe
SetParamFunctions[EnumPlayerFightProp.AntiYiRen] = CLuaPlayerPropertyFight.SetParamAntiYiRen
SetParamFunctions[EnumPlayerFightProp.AntiYanShi] = CLuaPlayerPropertyFight.SetParamAntiYanShi
SetParamFunctions[EnumPlayerFightProp.AntiHuaHun] = CLuaPlayerPropertyFight.SetParamAntiHuaHun
SetParamFunctions[EnumPlayerFightProp.AntiYingLing] = CLuaPlayerPropertyFight.SetParamAntiYingLing
SetParamFunctions[EnumPlayerFightProp.AntiDieKe] = CLuaPlayerPropertyFight.SetParamAntiDieKe
SetParamFunctions[EnumPlayerFightProp.AntiFlag] = CLuaPlayerPropertyFight.SetParamAntiFlag
SetParamFunctions[EnumPlayerFightProp.EnhanceZhanKuang] = CLuaPlayerPropertyFight.SetParamEnhanceZhanKuang
SetParamFunctions[EnumPlayerFightProp.EnhanceZhanKuangMul] = CLuaPlayerPropertyFight.SetParamEnhanceZhanKuangMul
SetParamFunctions[EnumPlayerFightProp.EnhanceZhanKuangKefu] = CLuaPlayerPropertyFight.SetParamEnhanceZhanKuangKefu
SetParamFunctions[EnumPlayerFightProp.AntiZhanKuang] = CLuaPlayerPropertyFight.SetParamAntiZhanKuang
SetParamFunctions[EnumPlayerFightProp.OriJumpSpeed] = CLuaPlayerPropertyFight.SetParamOriJumpSpeed
SetParamFunctions[EnumPlayerFightProp.AdjJumpSpeed] = CLuaPlayerPropertyFight.SetParamAdjJumpSpeed
SetParamFunctions[EnumPlayerFightProp.OriGravityAcceleration] = CLuaPlayerPropertyFight.SetParamOriGravityAcceleration
SetParamFunctions[EnumPlayerFightProp.AdjGravityAcceleration] = CLuaPlayerPropertyFight.SetParamAdjGravityAcceleration
SetParamFunctions[EnumPlayerFightProp.OriHorizontalAcceleration] = CLuaPlayerPropertyFight.SetParamOriHorizontalAcceleration
SetParamFunctions[EnumPlayerFightProp.AdjHorizontalAcceleration] = CLuaPlayerPropertyFight.SetParamAdjHorizontalAcceleration
SetParamFunctions[EnumPlayerFightProp.OriSwimSpeed] = CLuaPlayerPropertyFight.SetParamOriSwimSpeed
SetParamFunctions[EnumPlayerFightProp.AdjSwimSpeed] = CLuaPlayerPropertyFight.SetParamAdjSwimSpeed
SetParamFunctions[EnumPlayerFightProp.StrengthPoint] = CLuaPlayerPropertyFight.SetParamStrengthPoint
SetParamFunctions[EnumPlayerFightProp.OriStrengthPointMax] = CLuaPlayerPropertyFight.SetParamOriStrengthPointMax
SetParamFunctions[EnumPlayerFightProp.AdjStrengthPointMax] = CLuaPlayerPropertyFight.SetParamAdjStrengthPointMax
SetParamFunctions[EnumPlayerFightProp.AntiMulEnhanceIllusion] = CLuaPlayerPropertyFight.SetParamAntiMulEnhanceIllusion
SetParamFunctions[EnumPlayerFightProp.GlideHorizontalSpeedWithUmbrella] = CLuaPlayerPropertyFight.SetParamGlideHorizontalSpeedWithUmbrella
SetParamFunctions[EnumPlayerFightProp.GlideVerticalSpeedWithUmbrella] = CLuaPlayerPropertyFight.SetParamGlideVerticalSpeedWithUmbrella
SetParamFunctions[EnumPlayerFightProp.AdjpSpeed] = CLuaPlayerPropertyFight.SetParamAdjpSpeed
SetParamFunctions[EnumPlayerFightProp.AdjmSpeed] = CLuaPlayerPropertyFight.SetParamAdjmSpeed

