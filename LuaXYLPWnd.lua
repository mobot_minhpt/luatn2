--洗冤灵谱
LuaXYLPWnd = class()

RegistChildComponent(LuaXYLPWnd, "m_Grid","Grid", UIGrid)
RegistChildComponent(LuaXYLPWnd, "m_Template","Template", GameObject)
RegistChildComponent(LuaXYLPWnd, "m_InfoLabel","InfoLabel", UILabel)
RegistChildComponent(LuaXYLPWnd, "m_BottomLabel","BottomLabel", UILabel)

RegistClassMember(LuaXYLPWnd, "m_Clues")
RegistClassMember(LuaXYLPWnd, "m_DataList")
RegistClassMember(LuaXYLPWnd, "m_LastSelectedGroupId")
RegistClassMember(LuaXYLPWnd, "m_LastSelectedIndex")
function LuaXYLPWnd:Init()
    local infotext = g_MessageMgr:FormatMessage( "XYLP_Introduction")
    self.m_InfoLabel.text = CUICommonDef.TranslateToNGUIText(infotext)
    self.m_Template:SetActive(false)
    self.m_Grid.gameObject:SetActive(false)
    self:InitView()
end

function LuaXYLPWnd:InitView()
    self.m_Clues = {}
    Extensions.RemoveAllChildren(self.m_Grid.transform)
    ZhongYuanJie_XYLP.Foreach(function (groupId,data)
        local t = {}
        local npc = NPC_NPC.GetData(data.NPCID)
        local obj = NGUITools.AddChild(self.m_Grid.gameObject, self.m_Template)
        obj:SetActive(true)
        obj.transform:Find("NameLabel"):GetComponent(typeof(UILabel)).text = data.Name
        obj.transform:Find("Portrait"):GetComponent(typeof(CUITexture)):LoadNPCPortrait(npc.Portrait)
        obj.transform:Find("ZuiMingLabel"):GetComponent(typeof(UILabel)).text = data.ZuiMing
        local threads = data.Threads
        t.threads = {}
        for i = 0, 2 do
            local clues = ZhongYuanJie_XYLPClues.GetData(threads[i])
            local path = SafeStringFormat3("Clues%d/NameLabel", i + 1)
            obj.transform:Find(path):GetComponent(typeof(UILabel)).text = clues.Name
            path = SafeStringFormat3("Clues%d/Icon", i + 1)
            local tex = obj.transform:Find(path):GetComponent(typeof(CUITexture))
            local index = i
            local _groupId = groupId
            UIEventListener.Get(tex.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
                self:OnTaskIconClick(_groupId, index + 1)
            end)
            path = SafeStringFormat3("Clues%d/FinishSprite", i + 1)
            local finishSprite = obj.transform:Find(path).gameObject
            finishSprite.gameObject:SetActive(false)
            table.insert(t.threads, {clues = clues,tex = tex, finishSprite = finishSprite})
        end
        t.btn = obj.transform:Find("Button").gameObject
        t.btnfx = obj.transform:Find("Button/Fx"):GetComponent(typeof(CUIFx))
        t.finishSprite = obj.transform:Find("FinishSprite").gameObject
        t.cluesLabel = obj.transform:Find("CluesLabel"):GetComponent(typeof(UILabel))
        t.moushui = obj.transform:Find("ZuiMingLabel/MoShui"):GetComponent(typeof(UITexture))
        t.moushui.fillAmount = 0
        t.moushui = obj.transform:Find("ZuiMingLabel/MoShui"):GetComponent(typeof(CUIFx))
        self.m_Clues[groupId] = t
    end)
    self.m_Grid:Reposition()
    Gac2Gas.XYLPQueryThreads()
end

function LuaXYLPWnd:OnEnable()
    g_ScriptEvent:AddListener("XYLPWnd_CluesUpdate",self,"OnCluesUpdate")
    g_ScriptEvent:AddListener("XYLPWnd_PassCluesSuccess",self,"OnPassCluesSuccess")
end

function LuaXYLPWnd:OnDisable()
    g_ScriptEvent:RemoveListener("XYLPWnd_CluesUpdate",self,"OnCluesUpdate")
    g_ScriptEvent:RemoveListener("XYLPWnd_PassCluesSuccess",self,"OnPassCluesSuccess")
end

function LuaXYLPWnd:OnPassCluesSuccess(groupId)
    local t = self.m_Clues[groupId]
    --g_MessageMgr:ShowMessage("XYLP_PassCluesSuccess", t.Name)
    t.btnfx:DestroyFx()
    t.moushui:LoadFx("Fx/UI/Prefab/UI_huaquzuiming.prefab")
    t.btn:SetActive(false)
    t.finishSprite:SetActive(true)
end

function LuaXYLPWnd:InitBottomLabel()
    for groupId, num in pairs(self.m_DataList.numsDict) do
        if num ~= 3 then return end
    end
    self.m_BottomLabel.gameObject:SetActive(false)
end

function LuaXYLPWnd:OnCluesUpdate(datalist)
    self.m_DataList = datalist
    self.m_DataList.numsDict = {}
    self.m_DataList.selectedSprites = {}
    for groupId, isFinshed in pairs(datalist.RewardsDict) do
        local t = self.m_Clues[groupId]
        t.btn:SetActive(not isFinshed)
        t.finishSprite:SetActive(isFinshed)
        if isFinshed then
            t.moushui:LoadFx("Fx/UI/Prefab/UI_huaquzuiming.prefab")
        end

        local numOfClues = 0
        self.m_DataList.selectedSprites[groupId] = {}
        for j = 1,3 do
            local thread = t.threads[j]
            local selectedSprite = thread.tex.transform:Find("Texture").gameObject
            table.insert(self.m_DataList.selectedSprites[groupId], selectedSprite)
            selectedSprite:SetActive(false)
            if datalist.Threads[thread.clues.TaskID] then
                thread.tex:LoadMaterial(thread.clues.Icon)
                numOfClues = numOfClues + 1
            else
                thread.tex:LoadMaterial(nil)
            end
            thread.finishSprite.gameObject:SetActive(datalist.Threads[groupId])
        end
        self.m_DataList.numsDict[groupId] = numOfClues
        t.cluesLabel.text = SafeStringFormat3(LocalString.GetString("洗冤线索%d/3"), numOfClues)
        if not isFinshed then
            local index =  groupId
            UIEventListener.Get(t.btn).onClick = DelegateFactory.VoidDelegate(function(go)
                self:OnPassCluesBtnClick(index)
            end)
            if 3 == numOfClues then
                t.btnfx:LoadFx("fx/ui/prefab/UI_kuang_blue02.prefab")
            else
                t.btnfx:DestroyFx()
            end
        end
    end
    self:InitBottomLabel()
    self.m_Grid.gameObject:SetActive(true)
end

function LuaXYLPWnd:OnTaskIconClick(groupId, index)
    local t = self.m_Clues[groupId]
    local thread = t.threads[index]
    self:UpdateTaskSelected(groupId, index)
    ZhongYuanJie2020Mgr:ShowTip(thread.clues.TaskID)
end

function LuaXYLPWnd:UpdateTaskSelected(groupId, index)
    if not self.m_DataList then return end
    if self.m_LastSelectedGroupId and self.m_LastSelectedIndex and self.m_DataList.selectedSprites[self.m_LastSelectedGroupId] then
        local t = self.m_DataList.selectedSprites[self.m_LastSelectedGroupId]
        t[self.m_LastSelectedIndex].gameObject:SetActive(false)
    end
    if self.m_DataList.selectedSprites[groupId] then
        self.m_DataList.selectedSprites[groupId][index].gameObject:SetActive(true)
    end
    self.m_LastSelectedIndex = index
    self.m_LastSelectedGroupId = groupId
end

function LuaXYLPWnd:OnPassCluesBtnClick(groupId)
    local numOfClues = self.m_DataList.numsDict[groupId]
    if numOfClues < 3 then
        g_MessageMgr:ShowMessage("XYLP_EXIT_NOT_FOUND_THREAD")
        return
    end
    if numOfClues == 3 then
        Gac2Gas.XYLPTryReward(groupId)
        return
    end
end