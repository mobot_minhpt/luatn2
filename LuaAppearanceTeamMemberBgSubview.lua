local CUITexture = import "L10.UI.CUITexture"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local Extensions = import "Extensions"
local CButton = import "L10.UI.CButton"
local EnumGender = import "L10.Game.EnumGender"
local UISprite = import "UISprite"
local LuaTweenUtils = import "LuaTweenUtils"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"

LuaAppearanceTeamMemberBgSubview = class()

RegistChildComponent(LuaAppearanceTeamMemberBgSubview,"m_TeamMemberBgItem", "CommonItemCell", GameObject)
RegistChildComponent(LuaAppearanceTeamMemberBgSubview,"m_ItemDisplay", "AppearanceCommonItemDisplayView", CCommonLuaScript)
RegistChildComponent(LuaAppearanceTeamMemberBgSubview,"m_TeamMemberDisplaySubview", "AppearanceTeamMemberDisplaySubview", CCommonLuaScript)
RegistChildComponent(LuaAppearanceTeamMemberBgSubview,"m_ContentGrid", "ContentGrid", UIGrid)
RegistChildComponent(LuaAppearanceTeamMemberBgSubview,"m_ContentScrollView", "ContentScrollView", UIScrollView)

RegistClassMember(LuaAppearanceTeamMemberBgSubview, "m_AllBgs")
RegistClassMember(LuaAppearanceTeamMemberBgSubview, "m_SelectedDataId")

function LuaAppearanceTeamMemberBgSubview:Awake()
end

function LuaAppearanceTeamMemberBgSubview:Init()
    self:PlayAppearAnimation()
    self:SetDefaultSelection()
    self:LoadData()
end

function LuaAppearanceTeamMemberBgSubview:PlayAppearAnimation()
    self.m_TeamMemberDisplaySubview.gameObject:SetActive(true)
    LuaTweenUtils.TweenAlpha(self.m_TeamMemberDisplaySubview.transform:GetComponent(typeof(UIWidget)), 0, 1, 0.8)
    self.m_TeamMemberDisplaySubview:Init()
end

function LuaAppearanceTeamMemberBgSubview:LoadData()
    self.m_AllBgs = LuaAppearancePreviewMgr:GetAllTeamMemberBgInfo(false)

    Extensions.RemoveAllChildren(self.m_ContentGrid.transform)
    self.m_ContentGrid.gameObject:SetActive(true)

    for i = 1, # self.m_AllBgs do
        local child = CUICommonDef.AddChild(self.m_ContentGrid.gameObject, self.m_TeamMemberBgItem)
        child:SetActive(true)
        self:InitItem(child, self.m_AllBgs[i])
    end
    self.m_ContentGrid:Reposition()
    self.m_ContentScrollView:ResetPosition()
    self:InitSelection()
end

function LuaAppearanceTeamMemberBgSubview:SetDefaultSelection()
    self.m_SelectedDataId = LuaAppearancePreviewMgr:GetCurrentInUseTeamMemberBg()
end

function LuaAppearanceTeamMemberBgSubview:InitSelection()
    if self.m_SelectedDataId == 0 then
        self:OnItemClick(nil)
        return
    end

    local childCount = self.m_ContentGrid.transform.childCount
    for i=0, childCount-1 do
        local childGo = self.m_ContentGrid.transform:GetChild(i).gameObject
        local appearanceData = self.m_AllBgs[i+1]
        if appearanceData.id == self.m_SelectedDataId then
            self:OnItemClick(childGo)
            break
        end
    end
end

function LuaAppearanceTeamMemberBgSubview:InitItem(itemGo, appearanceData)
    local iconTexture = itemGo.transform:GetComponent(typeof(CUITexture))
    local disabledGo = itemGo.transform:Find("Disabled").gameObject
    local lockedGo = itemGo.transform:Find("Locked").gameObject
    local cornerGo = itemGo.transform:Find("Corner").gameObject
    local selectedGo = itemGo.transform:Find("Selected").gameObject
    iconTexture:LoadMaterial(appearanceData.icon)
    
    if CClientMainPlayer.Inst then
        lockedGo:SetActive(not LuaAppearancePreviewMgr:MainPlayerHasTeamMemberBg(appearanceData.id))
        cornerGo:SetActive(LuaAppearancePreviewMgr:IsCurrentInUseTeamMemberBg(appearanceData.id))
    else
        lockedGo:SetActive(false)
        cornerGo:SetActive(false)
    end
    selectedGo:SetActive(self.m_SelectedDataId and self.m_SelectedDataId==appearanceData.id)
    disabledGo:SetActive(lockedGo.activeSelf)
    UIEventListener.Get(itemGo).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnItemClick(go)
    end)
end

function LuaAppearanceTeamMemberBgSubview:OnItemClick(go)
    local childCount = self.m_ContentGrid.transform.childCount
    for i=0, childCount-1 do
        local childGo = self.m_ContentGrid.transform:GetChild(i).gameObject
        if childGo == go then
            childGo.transform:Find("Selected").gameObject:SetActive(true)
            self.m_SelectedDataId = self.m_AllBgs[i+1].id
        else
            childGo.transform:Find("Selected").gameObject:SetActive(false)
        end
    end
    self:UpdateItemDisplay()
end

function LuaAppearanceTeamMemberBgSubview:UpdateItemDisplay()
    self.m_ItemDisplay.gameObject:SetActive(true)
    local id = self.m_SelectedDataId and self.m_SelectedDataId or 0
    if id==0 then
        self.m_TeamMemberDisplaySubview:SetContent("", LuaAppearancePreviewMgr:GetDefaultTeamMemberBgName(), "", "")
        self.m_ItemDisplay:Clear()
        return
    end
    local appearanceData = nil
    for i=1,#self.m_AllBgs do
        if self.m_AllBgs[i].id == id then
            appearanceData = self.m_AllBgs[i]
            break
        end
    end
    self.m_TeamMemberDisplaySubview:SetContent(appearanceData.name, appearanceData.memberBg, appearanceData.memberBorder, appearanceData.teamBg)
    
    local icon = appearanceData.icon
    local disabled = true
    local locked = true
    local buttonTbl = {}
    local exist = LuaAppearancePreviewMgr:MainPlayerHasTeamMemberBg(appearanceData.id)
    local inUse = LuaAppearancePreviewMgr:IsCurrentInUseTeamMemberBg(appearanceData.id)
    locked = not exist
    disabled = not exist

    if exist then
        if inUse then
            table.insert(buttonTbl, {text=LocalString.GetString("脱下"), isYellow=false, action=function(go) self:OnRemoveButtonClick() end})
        else
            table.insert(buttonTbl, {text=LocalString.GetString("更换"), isYellow=false, action=function(go) self:OnApplyButtonClick() end})
        end
    else
        table.insert(buttonTbl, {text=LocalString.GetString("获取"), isYellow=true, action=function(go) self:OnMoreButtonClick(go) end})
    end

    self.m_ItemDisplay:Init(icon, appearanceData.name, SafeStringFormat3(LocalString.GetString("风尚度+%d"), appearanceData.fashionability), LuaAppearancePreviewMgr:GetTeamMemberBgConditionText(appearanceData.id), true, disabled, locked, buttonTbl)
end

function LuaAppearanceTeamMemberBgSubview:OnApplyButtonClick()
    local exist = LuaAppearancePreviewMgr:MainPlayerHasTeamMemberBg(self.m_SelectedDataId)
    if exist then
        LuaAppearancePreviewMgr:RequestSetTeamMemberBg(self.m_SelectedDataId)
    end
end

function LuaAppearanceTeamMemberBgSubview:OnRemoveButtonClick()
    LuaAppearancePreviewMgr:RequestSetTeamMemberBg(0)
end

function LuaAppearanceTeamMemberBgSubview:OnMoreButtonClick(go)
    local id = self.m_SelectedDataId and self.m_SelectedDataId or 0
    local appearanceData = nil
    for i=1,#self.m_AllBgs do
        if self.m_AllBgs[i].id == id then
            appearanceData = self.m_AllBgs[i]
            break
        end
    end
    if appearanceData then
        LuaItemAccessListMgr:ShowItemAccessInfoAtLeft(appearanceData.itemGetId, true, go.transform)
    end
end

function LuaAppearanceTeamMemberBgSubview:OnEnable()
    g_ScriptEvent:AddListener("OnUnLockTeamMemberBgSuccess", self, "OnUnLockTeamMemberBgSuccess")
    g_ScriptEvent:AddListener("OnUseTeamMemberBgSuccess", self, "OnUseTeamMemberBgSuccess")
end

function LuaAppearanceTeamMemberBgSubview:OnDisable()
    g_ScriptEvent:RemoveListener("OnUnLockTeamMemberBgSuccess", self, "OnUnLockTeamMemberBgSuccess")
    g_ScriptEvent:RemoveListener("OnUseTeamMemberBgSuccess", self, "OnUseTeamMemberBgSuccess")
end

function LuaAppearanceTeamMemberBgSubview:OnUnLockTeamMemberBgSuccess(id)
    self:LoadData()
end

function LuaAppearanceTeamMemberBgSubview:OnUseTeamMemberBgSuccess(id)
    self:SetDefaultSelection()
    self:LoadData()
end
