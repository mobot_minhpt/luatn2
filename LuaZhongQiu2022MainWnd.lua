local CommonDefs = import "L10.Game.CommonDefs"
local Color = import "UnityEngine.Color"
local DelegateFactory  = import "DelegateFactory"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local DateTime = import "System.DateTime"
local CScheduleMgr = import "L10.Game.CScheduleMgr"

LuaZhongQiu2022MainWnd = class()
LuaZhongQiu2022MainWnd.s_ActivityOpenDict = {}
--@region RegistChildComponent: Dont Modify Manually!


--@endregion RegistChildComponent end

RegistClassMember(LuaZhongQiu2022MainWnd, "m_RefreshTick")

function LuaZhongQiu2022MainWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaZhongQiu2022MainWnd:OnDestroy()
    self:CancelRefreshTick()
end

function LuaZhongQiu2022MainWnd.GetCronDateTime(Cron)
    local offset = string.find(Cron, ',') and 4 or 3
    local dd = {}
    for d in string.gmatch(Cron, "%d+") do
        table.insert(dd, tonumber(d))
    end
    local dt = {}
    local now = CServerTimeMgr.Inst:GetZone8Time()
    for k = 2, #dd - offset do 
        table.insert(dt, CreateFromClass(DateTime, now.Year, now.Month, now.Day, dd[k], dd[1], 0))
    end
    return dt
end

function LuaZhongQiu2022MainWnd:GetActivityTime(taskId, scheduleId)
    local startTime = Task_Task.GetData(taskId).StartTime
    local lastTime = Task_Task.GetData(taskId).LastTime
    local schedule = Task_Schedule.GetData(scheduleId)
    
    local st = self.GetCronDateTime(startTime)
    local ed = {} 
    for _, v in ipairs(st) do
        table.insert(ed, v:AddMinutes(lastTime * 1.0))
    end

    if #st > 1 then
        return SafeStringFormat3( "%s   %02d:%02d-%02d:%02d, %02d:%02d-%02d:%02d", schedule.ExternTip, st[1].Hour, st[1].Minute, ed[1].Hour, ed[1].Minute, st[2].Hour, st[2].Minute, ed[2].Hour, ed[2].Minute)
    else
        return SafeStringFormat3( "%s   %02d:%02d-%02d:%02d", schedule.ExternTip, st[1].Hour, st[1].Minute, ed[1].Hour, ed[1].Minute)
    end
end

function LuaZhongQiu2022MainWnd:RefreshTickFunc()
    local scheduleId = {
        42030249, 42030248, 42030247, 42030246
    }
    local taskId = {
        22121158, 22121156, 22121154, 22121152
    }
    for i = 1, 4 do
        local entry = self.transform:Find("Anchor/Entry"..i)
        
        local taskLv = Task_Task.GetData(taskId[i]).Level
        local startTime = Task_Task.GetData(taskId[i]).StartTime
        local lastTime = Task_Task.GetData(taskId[i]).LastTime
        local hint = entry:Find("Hint"):GetComponent(typeof(UILabel))
        if CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.Level >= taskLv then
            hint.color = Color.white
            if taskId[i] == 22121154 and CScheduleMgr.Inst:GetScheduleTimeFromPlayProp(258) > 0 then
                hint.text = LocalString.GetString("已完成")
            else
                hint.text = LocalString.GetString("已结束")
                local dt = self.GetCronDateTime(startTime)
                local now = CServerTimeMgr.Inst:GetZone8Time()
                for k = 1, #dt do 
                    if now:CompareTo(dt[k]) < 0 then
                        local hour = dt[k].Hour < 10 and "0"..dt[k].Hour or dt[k].Hour
                        local minute = dt[k].Minute < 10 and "0"..dt[k].Minute or dt[k].Minute
                        hint.text = hour..":"..minute..LocalString.GetString("开启")
                        break
                    elseif now:CompareTo(dt[k]) >= 0 and now:CompareTo(dt[k]:AddMinutes(lastTime * 1.0)) <= 0 then
                        hint.text = LocalString.GetString("可参与")
                        break
                    end
                end
            end

            CommonDefs.AddOnClickListener(
                entry.gameObject, 
                DelegateFactory.Action_GameObject(function()
                    --CLuaScheduleMgr:ShowScheduleInfo(scheduleId[i])
                    CLuaScheduleMgr.selectedScheduleInfo={
                        activityId = scheduleId[i],
                        taskId = taskId[i],
        
                        TotalTimes = 0,--不显示次数
                        DailyTimes = 0,--不显示活力
                        FinishedTimes = 0,
                        
                        ActivityTime = self:GetActivityTime(taskId[i], scheduleId[i]),
                        -- 等级够的情况下，没到时间也强行显示参加按钮  /  经验雨在等级够+时间到+可领取的情况下才显示参加
                        ShowJoinBtn = i > 2 or i == 2 and hint.text == LocalString.GetString("可参与") and CClientMainPlayer.Inst and CClientMainPlayer.Inst.expRainStart
                    }
                    CLuaScheduleInfoWnd.s_UseCustomInfo = true
                    CUIManager.ShowUI(CLuaUIResources.ScheduleInfoWnd)
                end), 
                false
            )
        else
            CommonDefs.AddOnClickListener(
                entry.gameObject, 
                DelegateFactory.Action_GameObject(function()
                    --CLuaScheduleMgr:ShowScheduleInfo(scheduleId[i])
                    CLuaScheduleMgr.selectedScheduleInfo={
                        activityId = scheduleId[i],
                        taskId = taskId[i],
        
                        TotalTimes = 0,--不显示次数
                        DailyTimes = 0,--不显示活力
                        FinishedTimes = 0,
                        
                        ActivityTime = self:GetActivityTime(taskId[i], scheduleId[i]),
                        ShowJoinBtn = false -- 等级不够就不显示参加按钮了
                    }
                    CLuaScheduleInfoWnd.s_UseCustomInfo = true
                    CUIManager.ShowUI(CLuaUIResources.ScheduleInfoWnd)
                end), 
                false
            )
            hint.color = Color.red
            hint.text = taskLv..LocalString.GetString("级可参加")
        end
        local canJoin = (hint.text == LocalString.GetString("可参与"))
        LuaZhongQiu2022MainWnd.s_ActivityOpenDict[scheduleId[i]] = canJoin
        if taskId[i] == 22121156 then self.transform:Find("Anchor/Fx1/liuxing").gameObject:SetActive(canJoin) end
    end

    CommonDefs.AddOnClickListener(
        self.transform:Find("Anchor/Title/Gift").gameObject,
        DelegateFactory.Action_GameObject(function()
            --CLuaScheduleMgr:ShowScheduleInfo(42030250)
            CLuaScheduleMgr.selectedScheduleInfo={
                activityId = 42030250,
                taskId = 22121160,

                TotalTimes = 0,--不显示次数
                DailyTimes = 0,--不显示活力
                FinishedTimes = 0,

                ActivityTime = self:GetActivityTime(22121160, 42030250),
                ShowJoinBtn = false
            }
            CLuaScheduleInfoWnd.s_UseCustomInfo = true
            CUIManager.ShowUI(CLuaUIResources.ScheduleInfoWnd)
        end), 
        false
    )
end

function LuaZhongQiu2022MainWnd:CancelRefreshTick()
    if self.m_RefreshTick then
        invoke(self.m_RefreshTick)
        self.m_RefreshTick = nil
    end
end

function LuaZhongQiu2022MainWnd:Init()
    CScheduleMgr.Inst:RequestActivity()
    CLuaScheduleMgr.BuildInfos()

    self:CancelRefreshTick()
    self:RefreshTickFunc()
    self.m_RefreshTick = RegisterTick(function()
        self:RefreshTickFunc()
    end, 1000)
end

function LuaZhongQiu2022MainWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateActivity", self, "OnUpdateActivity")
end

function LuaZhongQiu2022MainWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateActivity", self, "OnUpdateActivity")
end

function LuaZhongQiu2022MainWnd:OnUpdateActivity()
	CLuaScheduleMgr.BuildInfos()
end
--@region UIEvent

--@endregion UIEvent

