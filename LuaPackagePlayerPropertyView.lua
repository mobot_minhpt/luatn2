local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local CTooltip = import "L10.UI.CTooltip"
local MessageMgr = import "L10.Game.MessageMgr"
LuaPackagePlayerPropertyView = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaPackagePlayerPropertyView, "Cor", "Cor", GameObject)
RegistChildComponent(LuaPackagePlayerPropertyView, "Sta", "Sta", GameObject)
RegistChildComponent(LuaPackagePlayerPropertyView, "Str", "Str", GameObject)
RegistChildComponent(LuaPackagePlayerPropertyView, "Int", "Int", GameObject)
RegistChildComponent(LuaPackagePlayerPropertyView, "Agi", "Agi", GameObject)
RegistChildComponent(LuaPackagePlayerPropertyView, "Attack", "Attack", GameObject)
RegistChildComponent(LuaPackagePlayerPropertyView, "HpFull", "HpFull", GameObject)
RegistChildComponent(LuaPackagePlayerPropertyView, "MpFull", "MpFull", GameObject)
RegistChildComponent(LuaPackagePlayerPropertyView, "AttackSpeed", "AttackSpeed", GameObject)
RegistChildComponent(LuaPackagePlayerPropertyView, "Hit", "Hit", GameObject)
RegistChildComponent(LuaPackagePlayerPropertyView, "Fatal", "Fatal", GameObject)
RegistChildComponent(LuaPackagePlayerPropertyView, "pDef", "pDef", GameObject)
RegistChildComponent(LuaPackagePlayerPropertyView, "mDef", "mDef", GameObject)
RegistChildComponent(LuaPackagePlayerPropertyView, "pMiss", "pMiss", GameObject)
RegistChildComponent(LuaPackagePlayerPropertyView, "mMiss", "mMiss", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaPackagePlayerPropertyView, "m_PropertyList")
RegistClassMember(LuaPackagePlayerPropertyView, "m_ClickTick")
RegistClassMember(LuaPackagePlayerPropertyView, "m_ChangeColor")
RegistClassMember(LuaPackagePlayerPropertyView, "m_OriColor")
RegistClassMember(LuaPackagePlayerPropertyView, "m_WordsColor")
function LuaPackagePlayerPropertyView:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.m_PropertyList = {}
    self.m_ChangeColor = "fffe91"
    self.m_OriColor = {}
    self.m_WordsColor = CreateFromClass(MakeGenericClass(List, UInt32))
    self.m_ClickTick = nil
    self:InitProperties()
end


function LuaPackagePlayerPropertyView:InitProperties()
    self:InitOneProperty(3,self.Cor)     -- 根骨
    self:InitOneProperty(4,self.Sta)     -- 精力
    self:InitOneProperty(5,self.Str)     -- 力量
    self:InitOneProperty(6,self.Int)     -- 智力
    self:InitOneProperty(7,self.Agi)     -- 敏捷
    self:InitOneProperty(14,self.HpFull) -- 最大气血
    self:InitOneProperty(15,self.MpFull) -- 最大法力
    self:InitOneProperty(20,self.pDef)   -- 物理防御
    self:InitOneProperty(21,self.mDef)   -- 法术防御
    self:InitOneProperty(22,self.pMiss)  -- 物理躲避
    self:InitOneProperty(23,self.mMiss)  -- 法术躲避

    
    if CClientMainPlayer.Inst == nil then return end 
    local physical = CClientMainPlayer.Inst.IsPhysicalAttackType
    if physical then
        self:InitOneProperty(8,self.Attack)     -- 物理攻击
        self:InitOneProperty(16,self.Hit)       -- 物理命中
        self:InitOneProperty(18,self.Fatal)     -- 物理致命
        self:InitOneProperty(24,self.AttackSpeed)-- 物理攻速
    else
        self:InitOneProperty(11,self.Attack)    -- 法术攻击
        self:InitOneProperty(17,self.Hit)       -- 法术命中
        self:InitOneProperty(19,self.Fatal)     -- 法术致命
        self:InitOneProperty(25,self.AttackSpeed)-- 法术攻速
    end

end

function LuaPackagePlayerPropertyView:InitOneProperty(propertyID,propertyGo)
    if not propertyGo then return end
    local go = propertyGo.transform:Find("NameLabel").gameObject
    local data = PropGuide_Property.GetData(propertyID) 
    if not data then return end

    self.m_PropertyList[propertyID] = propertyGo 
    self.m_OriColor[propertyID] = go:GetComponent(typeof(UILabel)).color
    
    UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function (go)
        -- 属性详析按钮
        if LuaPlayerPropertyMgr.PropertyGuide and data.ShowBtn ~= 0 then
            self:ShowBtn(go,propertyID)
        else
	        self:ShowMessage(go,data.Desc)
        end
        -- 关联属性变色
        if LuaPlayerPropertyMgr.PropertyGuide and data.RelateProperty then
            self:SetWorldColor(propertyID)
            -- 遍历RelateProperty
            for i = 0,data.RelateProperty.Length - 1 do
                self:SetWorldColor(data.RelateProperty[i])
            end
            -- 起Tick
            self:ClearTick()
            self.m_ClickTick = RegisterTickOnce(function()
                self:ClearTick()
                self:RevertColors()
            end,PropGuide_Setting.GetData().RelatePropertyTime * 1000)
        end
	end)
end

function LuaPackagePlayerPropertyView:Update()
    if Input.GetMouseButtonDown(0) then    
        self:ClearTick()
        self:RevertColors()
    end
end
function LuaPackagePlayerPropertyView:RevertColors()
    for i = self.m_WordsColor.Count - 1 , 0, -1 do
        self:RevertWordColor(self.m_WordsColor[i])
    end
end
function LuaPackagePlayerPropertyView:ShowBtn(go,propertyID)
    local showWnd = function()
        LuaPlayerPropertyMgr:ShowPlayerPropertyDetailWnd(propertyID)
    end
    LuaToolBtnMgr:ShowToolBtn(PropGuide_Setting.GetData().PropertyGuideName,showWnd,go.transform)
end
function LuaPackagePlayerPropertyView:ShowMessage(go,MessageName)
    CTooltip.Show(MessageMgr.Inst:FormatMessage(MessageName,{}), go.transform, CTooltip.AlignType.Top);
end
function LuaPackagePlayerPropertyView:SetWorldColor(propID)
    local go = self.m_PropertyList[propID]
    if not go then return end
    if self.m_WordsColor:Contains(propID) then return end
    go.transform:Find("NameLabel"):GetComponent(typeof(UILabel)).color = NGUIText.ParseColor24(self.m_ChangeColor, 0)
    self.m_WordsColor:Add(propID)
end
function LuaPackagePlayerPropertyView:RevertWordColor(propID)
    local go = self.m_PropertyList[propID]
    local oriColor = self.m_OriColor[propID]
    if not go or not oriColor then return end
    go.transform:Find("NameLabel"):GetComponent(typeof(UILabel)).color = oriColor
    if self.m_WordsColor:Contains(propID) then
        self.m_WordsColor:Remove(propID)
    end
end

function LuaPackagePlayerPropertyView:ClearTick()
    if self.m_ClickTick ~= nil then
        UnRegisterTick(self.m_ClickTick)
        self.m_ClickTick = nil
    end
end

function LuaPackagePlayerPropertyView:OnDisable()
    self:ClearTick()
end
--@region UIEvent

--@endregion UIEvent

