-- Auto Generated!!
local CPuppetExpressionActionItem = import "L10.UI.CPuppetExpressionActionItem"
local DelegateFactory = import "DelegateFactory"
local Expression_Show = import "L10.Game.Expression_Show"
local UILongPressButton = import "L10.UI.UILongPressButton"
CPuppetExpressionActionItem.m_Init_CS2LuaHook = function (this, expressionId, isInNeed) 
    local expressionAction = Expression_Show.GetData(expressionId)
    if expressionAction ~= nil then
        this.icon:LoadMaterial(expressionAction.Icon)
        this.nameLabel.text = expressionAction.ExpName
        CommonDefs.GetComponent_Component_Type(this, typeof(UILongPressButton)).OnLongPressDelegate = DelegateFactory.Action(function () 
            --if (OnItemLongPressed != null)
            --    OnItemLongPressed();
            --CExpressionActionMgr.ShowExpressionActionPreview(expressionAction.ID);
        end)

        local isLock = false
        --if (expressionAction.LockGroup != 0)
        --    isLock = CExpressionActionMgr.Inst.UnLockGroupInfo[expressionAction.LockGroup] == 0;

        if expressionAction.Status ~= 0 or isLock then
            this.disableSprite:SetActive(true)
            this.lockSprite:SetActive(true)
        else
            this.disableSprite:SetActive(false)
            this.lockSprite:SetActive(false)
        end

        if isInNeed then
            this.needSprite.alpha = 1
        else
            this.needSprite.alpha = 0
        end
    end
end
