require("common/common_include")
CLuaCommonAnnounceMgr = class()
CLuaCommonAnnounceMgr.m_WndTitle = nil
CLuaCommonAnnounceMgr.m_InputTip = nil
CLuaCommonAnnounceMgr.m_ChangeFunc = nil
CLuaCommonAnnounceMgr.m_MaxByteLength = nil
CLuaCommonAnnounceMgr.m_MaxByteLengthTip = nil
CLuaCommonAnnounceMgr.m_InputValue = nil

function CLuaCommonAnnounceMgr.ShowWnd(wndTitle, inputTip, changeFunc, maxByteLen, maxByteLenTip, inputValue)
	CLuaCommonAnnounceMgr.m_WndTitle = wndTitle
	CLuaCommonAnnounceMgr.m_InputTip = inputTip
	CLuaCommonAnnounceMgr.m_ChangeFunc = changeFunc
	CLuaCommonAnnounceMgr.m_MaxByteLength = maxByteLen
	CLuaCommonAnnounceMgr.m_MaxByteLengthTip = maxByteLenTip
	CLuaCommonAnnounceMgr.m_InputValue = inputValue
	CUIManager.ShowUI(CLuaUIResources.CommonAnnounceWnd)
end

function CLuaCommonAnnounceMgr.CloseWnd( ... )
	CUIManager.CloseUI(CLuaUIResources.CommonAnnounceWnd)
end
