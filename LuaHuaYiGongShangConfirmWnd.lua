local CommonDefs      = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CButton         = import "L10.UI.CButton"
local CServerTimeMgr  = import "L10.Game.CServerTimeMgr"
local CWordFilterMgr  = import "L10.Game.CWordFilterMgr"

LuaHuaYiGongShangConfirmWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaHuaYiGongShangConfirmWnd, "oldPortrait")
RegistClassMember(LuaHuaYiGongShangConfirmWnd, "newPortrait")
RegistClassMember(LuaHuaYiGongShangConfirmWnd, "oldButton")
RegistClassMember(LuaHuaYiGongShangConfirmWnd, "newButton")
RegistClassMember(LuaHuaYiGongShangConfirmWnd, "newButtonLabel")
RegistClassMember(LuaHuaYiGongShangConfirmWnd, "oldBottomLabel")
RegistClassMember(LuaHuaYiGongShangConfirmWnd, "part")
RegistClassMember(LuaHuaYiGongShangConfirmWnd, "oldWorkName")
RegistClassMember(LuaHuaYiGongShangConfirmWnd, "input")

RegistClassMember(LuaHuaYiGongShangConfirmWnd, "tick")
RegistClassMember(LuaHuaYiGongShangConfirmWnd, "endTimeStamp")

function LuaHuaYiGongShangConfirmWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self:InitUIComponents()
    self:InitEventListener()

    if not LuaWuMenHuaShiMgr.allowCustomTitle then
        self.oldWorkName.gameObject:SetActive(false)
        self.transform:Find("Anchor/New/Input").gameObject:SetActive(false)
    end
end

function LuaHuaYiGongShangConfirmWnd:InitUIComponents()
    local anchor = self.transform:Find("Anchor")
    self.part = anchor:Find("Part").gameObject
    self.oldPortrait = anchor:Find("Old/Portrait").gameObject
    self.newPortrait = anchor:Find("New/Portrait").gameObject
    self.oldButton = anchor:Find("Old/Button").gameObject
    self.newButton = anchor:Find("New/Button"):GetComponent(typeof(CButton))
    self.newButtonLabel = self.newButton.transform:Find("Label"):GetComponent(typeof(UILabel))
    self.oldBottomLabel = anchor:Find("Old/Label"):GetComponent(typeof(UILabel))
    self.oldWorkName = anchor:Find("Old/WorkName"):GetComponent(typeof(UILabel))
    self.input = anchor:Find("New/Input/Input"):GetComponent(typeof(UIInput))
end

function LuaHuaYiGongShangConfirmWnd:InitEventListener()
    UIEventListener.Get(self.oldButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOldButtonClick()
	end)

    UIEventListener.Get(self.newButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnNewButtonClick()
	end)
end

function LuaHuaYiGongShangConfirmWnd:OnEnable()
    g_ScriptEvent:AddListener("SeasonRank_QueryRankResult", self, "OnSeasonRank_QueryRankResult")
end

function LuaHuaYiGongShangConfirmWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SeasonRank_QueryRankResult", self, "OnSeasonRank_QueryRankResult")
end

function LuaHuaYiGongShangConfirmWnd:OnSeasonRank_QueryRankResult(rankType, seasonId, selfData, rankData)
    if rankType ~= EnumSeasonRankType.eXinShengHuaJi or seasonId ~= LuaWuMenHuaShiMgr.seasonInfo.id then return end

    local myRankInfo = {}
    local rawData = MsgPackImpl.unpack(selfData)
    if CommonDefs.IsDic(rawData) then
        CommonDefs.DictIterate(rawData,DelegateFactory.Action_object_object(function (key, value)
            myRankInfo[tostring(key)] = value
        end))
    end

    local myId = CClientMainPlayer.Inst.Id
    local rawData2 = MsgPackImpl.unpack(rankData)
    if rawData2.Count > 0 then
        for i = 1, rawData2.Count do
            local rawData3 = rawData2[i - 1]
            local dict = {}
            if CommonDefs.IsDic(rawData3) then
                CommonDefs.DictIterate(rawData3,DelegateFactory.Action_object_object(function (key, value)
                    dict[tostring(key)] = value
                end))
            end
            if dict.playerId and dict.playerId == myId then
                myRankInfo.rank = i
                break
            end
        end
    end

    local score = myRankInfo.Score and myRankInfo.Score or 0
    if myRankInfo.rank and myRankInfo.rank > 0 and score > 0 then
        self.oldBottomLabel.text = SafeStringFormat3(LocalString.GetString("当前排名%d，人气%d"), myRankInfo.rank, score)
    else
        self.oldBottomLabel.text = SafeStringFormat3(LocalString.GetString("当前排名未上榜，人气%d"), score)
    end
    self.oldWorkName.text = System.String.IsNullOrEmpty(myRankInfo.Title) and LocalString.GetString("无题") or myRankInfo.Title
end


function LuaHuaYiGongShangConfirmWnd:Init()
    if LuaWuMenHuaShiMgr.oldHYGSPicData then
        LuaWuMenHuaShiMgr:GeneratePortraitWithFlip(self.oldPortrait, self.part, LuaWuMenHuaShiMgr.oldHYGSPicData, self.oldPortrait.transform.localPosition.x)
        self:StartTick()
        Gac2Gas.SeasonRank_QueryRank(EnumSeasonRankType.eXinShengHuaJi, LuaWuMenHuaShiMgr.seasonInfo.id)
    else
        self.transform:Find("Anchor/New/Label").gameObject:SetActive(false)
        self.transform:Find("Anchor/Old").gameObject:SetActive(false)
        self.newButtonLabel.text = LocalString.GetString("提交画作")
        LuaUtils.SetLocalPositionX(self.transform:Find("Anchor/New"), 0)
    end

    LuaWuMenHuaShiMgr:GeneratePortraitWithFlip(self.newPortrait, self.part, LuaWuMenHuaShiMgr.newHYGSPicData, self.newPortrait.transform.localPosition.x)
end

function LuaHuaYiGongShangConfirmWnd:StartTick()
    self.newButton.Enabled = false
    self.endTimeStamp = CServerTimeMgr.Inst.timeStamp + WuMenHuaShi_XinShengHuaJiSetting.GetData().HuaYiGongShangConfirmTime
    self:UpdateTime()
    self.tick = RegisterTick(function()
        self:UpdateTime()
    end, 1000)
end

function LuaHuaYiGongShangConfirmWnd:UpdateTime()
    local leftTime = math.floor(self.endTimeStamp - CServerTimeMgr.Inst.timeStamp)
    if leftTime <= 0 then
        self:ClearTick()
        self.newButton.Enabled = true
        self.newButtonLabel.text = LocalString.GetString("采用新作")
        return
    end

    self.newButtonLabel.text = SafeStringFormat3(LocalString.GetString("采用新作(%d)"), leftTime)
end

function LuaHuaYiGongShangConfirmWnd:ClearTick()
    if self.tick then
        UnRegisterTick(self.tick)
    end
end

-- 提交画作，关闭界面
function LuaHuaYiGongShangConfirmWnd:UploadPic(title)
    LuaWuMenHuaShiMgr:UploadPic2(LuaWuMenHuaShiMgr.newHYGSPicData, title)
    CUIManager.CloseUI(CLuaUIResources.HuaYiGongShangConfirmWnd)
    LuaWuMenHuaShiMgr:CloseXinShengHuaJiDrawWnd()
end

function LuaHuaYiGongShangConfirmWnd:OnDestroy()
    self:ClearTick()
end

--@region UIEvent

function LuaHuaYiGongShangConfirmWnd:OnOldButtonClick()
    CUIManager.CloseUI(CLuaUIResources.HuaYiGongShangConfirmWnd)
end

function LuaHuaYiGongShangConfirmWnd:OnNewButtonClick()
    if not LuaWuMenHuaShiMgr.allowCustomTitle then
        self:UploadPic("")
        return
    end

    if System.String.IsNullOrEmpty(self.input.value) then
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("HUAYIGONGSHANG_NO_TITLE_CONFIRM"), function()
            self:UploadPic("")
        end, nil, LocalString.GetString("发布"), LocalString.GetString("返回"), false)
        return
    end

    -- 屏蔽词检查
	local ret = CWordFilterMgr.Inst:DoFilterOnSend(self.input.value, nil, nil, true)
	if not ret.msg or ret.shouldBeIgnore then
		return
	end

    if not LuaWuMenHuaShiMgr.oldHYGSPicData then
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("HUAYIGONGSHANG_USE_WORK_CONFIRM"), function()
            self:UploadPic(ret.msg)
        end, nil, nil, nil, false)
        return
    end

    self:UploadPic(ret.msg)
end

--@endregion UIEvent
