local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CQnReturnAwardTemplate = import "L10.UI.CQnReturnAwardTemplate"
local CItemMgr = import "L10.Game.CItemMgr"
local CUITexture = import "L10.UI.CUITexture"
local CTeamMgr = import "L10.Game.CTeamMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CCarnivalLotteryMgr = import "L10.Game.CCarnivalLotteryMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaHalloween2023OrderBattleEnterWnd = class()

RegistClassMember(LuaHalloween2023OrderBattleEnterWnd, "m_RuleButton")
RegistClassMember(LuaHalloween2023OrderBattleEnterWnd, "m_DailyReward")
RegistClassMember(LuaHalloween2023OrderBattleEnterWnd, "m_CompetitionRewrd")
-- 右侧
RegistClassMember(LuaHalloween2023OrderBattleEnterWnd, "m_CanMatchView")
RegistClassMember(LuaHalloween2023OrderBattleEnterWnd, "m_CantMatchView")
RegistClassMember(LuaHalloween2023OrderBattleEnterWnd, "m_TeamButton")
RegistClassMember(LuaHalloween2023OrderBattleEnterWnd, "m_MatchButton")
RegistClassMember(LuaHalloween2023OrderBattleEnterWnd, "m_CancelButton")
RegistClassMember(LuaHalloween2023OrderBattleEnterWnd, "m_TimeLimitLabel")
RegistClassMember(LuaHalloween2023OrderBattleEnterWnd, "m_ExchangeButton")

RegistClassMember(LuaHalloween2023OrderBattleEnterWnd, "m_Player1")     -- 骑手
RegistClassMember(LuaHalloween2023OrderBattleEnterWnd, "m_Player2")     -- 抢单
RegistClassMember(LuaHalloween2023OrderBattleEnterWnd, "isMatching")

function LuaHalloween2023OrderBattleEnterWnd:Awake()
    self.m_RuleButton = self.transform:Find("RuleButton").gameObject
    self.m_DailyReward = self.transform:Find("Left/Daily/ItemCell1"):GetComponent(typeof(CQnReturnAwardTemplate))
    self.m_CompetitionRewrd = self.transform:Find("Left/Competition/ItemCell2"):GetComponent(typeof(CQnReturnAwardTemplate))
    -- 右侧
    self.m_CanMatchView = self.transform:Find("Right/Yes").gameObject
    self.m_ExchangeButton = self.transform:Find("Right/Yes/ExchangeButton").gameObject
    self.m_CantMatchView = self.transform:Find("Right/No").gameObject
    self.m_TeamButton = self.transform:Find("Right/No/TeamButton").gameObject
    self.m_MatchButton = self.transform:Find("Right/MatchButton").gameObject
    self.m_CancelButton = self.transform:Find("Right/CancelButton").gameObject
    self.m_TimeLimitLabel = self.transform:Find("Right/TimeLimitLabel").gameObject

    self.m_Player1 = self.transform:Find("Right/Yes/Player1"):GetComponent(typeof(CUITexture))
    self.m_Player2 = self.transform:Find("Right/Yes/Player2"):GetComponent(typeof(CUITexture))

    UIEventListener.Get(self.m_RuleButton).onClick = DelegateFactory.VoidDelegate(function()
        self:OnRuleBtnClick()
    end)
    UIEventListener.Get(self.m_TeamButton).onClick = DelegateFactory.VoidDelegate(function()
        self:OnTeamBtnClick()
    end)
    UIEventListener.Get(self.m_MatchButton).onClick = DelegateFactory.VoidDelegate(function()
        self:OnMatchBtnClick()
    end)
    UIEventListener.Get(self.m_CancelButton).onClick = DelegateFactory.VoidDelegate(function()
        self:OnCancelButtonClick()
    end)
    UIEventListener.Get(self.m_ExchangeButton).onClick = DelegateFactory.VoidDelegate(function()
        self:OnExchangeButtonClick()
    end)
end

function LuaHalloween2023OrderBattleEnterWnd:Init()
    local setData = Halloween2023_Setting.GetData()
    self:InitReward(setData.CandyDeliveryDailyRewardId, setData.CandyDeliveryCompetitionRewardId)
    self:InitRuleImage(setData.CandyDeliveryRuleImagePath)
    self.isGameOpen = self:IsGameOpen(setData.CandyDeliveryPlayTime)

    self.m_TimeLimitLabel:SetActive(true)
    self.m_MatchButton:SetActive(false)
    self.m_CancelButton:SetActive(false)
    LuaHalloween2023Mgr.m_MyTeamRoleInfo = nil  -- 只要服务器不返回，就全部用默认形式
    self:InitTeamView()
    Gac2Gas.QueryPlayerIsMatchingCandyDelivery()
end

function LuaHalloween2023OrderBattleEnterWnd:InitTeamView()
    if CTeamMgr.Inst.TotalMemebersCount == 2 then
        self:InitPlayerInfo()
        self.m_CanMatchView.gameObject:SetActive(true)
        self.m_CantMatchView.gameObject:SetActive(false)
        CUICommonDef.SetActive(self.m_MatchButton, true, true)
    else
        LuaHalloween2023Mgr.m_MyTeamRoleInfo = nil  -- 队员人数不对清空缓存
        self.m_CanMatchView.gameObject:SetActive(false)
        self.m_CantMatchView.gameObject:SetActive(true)
        CUICommonDef.SetActive(self.m_MatchButton, false, true)
    end
end

function LuaHalloween2023OrderBattleEnterWnd:IsGameOpen(timeStrs)
    for i = 0, timeStrs.Length - 1 do
        if LuaHalloween2023Mgr:IsGameOpen(timeStrs[i]) then return true end
    end
    return false
end

function LuaHalloween2023OrderBattleEnterWnd:InitPlayerInfo()
    local teamPlayerTbl = {}
    local learderId = CTeamMgr.Inst.LeaderId
    local memberId = CClientMainPlayer.Inst.Id
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == learderId then
        memberId = CTeamMgr.Inst:GetTeamMembersExceptMe()[0].m_MemberId
    end
    teamPlayerTbl[learderId] = true
    teamPlayerTbl[memberId] = true

    if LuaHalloween2023Mgr.m_MyTeamRoleInfo == nil or
        not teamPlayerTbl[LuaHalloween2023Mgr.m_MyTeamRoleInfo.driverPlayerId] or
        not teamPlayerTbl[LuaHalloween2023Mgr.m_MyTeamRoleInfo.passengerPlayerId]  then
        LuaHalloween2023Mgr:SetMyTeamRoleInfo(learderId, memberId)
    end
    self:InitOnePlayer(self.m_Player1, LuaHalloween2023Mgr.m_MyTeamRoleInfo.driverPlayerId)
    self:InitOnePlayer(self.m_Player2, LuaHalloween2023Mgr.m_MyTeamRoleInfo.passengerPlayerId)
end

function LuaHalloween2023OrderBattleEnterWnd:InitReward(dailyRewardId, competitionRewardId)
    if CClientMainPlayer.Inst == nil then return end
    local dailyTime = CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eCandyDeliveryJoinDailyAwardTimes)
    self.m_DailyReward:Init(CItemMgr.Inst:GetItemTemplate(dailyRewardId), 1)
    self.m_DailyReward.transform:Find("Mask").gameObject:SetActive(dailyTime ~= 0)

    local winnerTime = CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eCandyDeliveryWinnerDailyAwardTimes)
    self.m_CompetitionRewrd:Init(CItemMgr.Inst:GetItemTemplate(competitionRewardId), 1)
    self.m_CompetitionRewrd.transform:Find("Mask").gameObject:SetActive(winnerTime ~= 0)
end

function LuaHalloween2023OrderBattleEnterWnd:InitRuleImage(path)
    self.transform:Find("Left/ImageRule"):GetComponent(typeof(CUITexture)):LoadMaterial(path)
end

function LuaHalloween2023OrderBattleEnterWnd:UpdateMatch(isMatching)
    self.isMatching = isMatching
    self.m_TimeLimitLabel:SetActive(not self.isGameOpen)
    self.m_MatchButton:SetActive(self.isGameOpen and not isMatching)
    self.m_CancelButton:SetActive(self.isGameOpen and isMatching)
end

function LuaHalloween2023OrderBattleEnterWnd:InitOnePlayer(playerGo, playerId)
    local member = CTeamMgr.Inst:GetMemberById(playerId)
    if member then
        local nameLabel = playerGo.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
        self:WrapText(nameLabel, member.m_MemberName, CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == playerId)
        playerGo:LoadNPCPortrait(CUICommonDef.GetPortraitName(member.m_MemberClass, member.m_MemberGender, member.m_Expression))
    end
end

function LuaHalloween2023OrderBattleEnterWnd:WrapText(label, originText, isMe)
    --赋值对label各项参数进行初始化，否则下面的计算会出问题，label类型需要clamp content， maxlines = 1
    label.text = originText
    local fit = true
    local outerText = ""
    fit, outerText = label:Wrap(originText)
    if not fit then
        outerText = CommonDefs.StringSubstring2(outerText, 0, CommonDefs.StringLength(outerText) - 1) .. "..."
        label.text = outerText
    end
    label.color = isMe and NGUIText.ParseColor24("22AB28",0) or NGUIText.ParseColor24("3E3458",0)
end

function LuaHalloween2023OrderBattleEnterWnd:OnTeamInfoChange()
    self:InitTeamView()
end

--@region UIEvent
function LuaHalloween2023OrderBattleEnterWnd:OnRuleBtnClick()
    LuaHalloween2023Mgr:OpenHalloween2023OrderBattleRuleWnd("EnterWndRuleBtn")
end

function LuaHalloween2023OrderBattleEnterWnd:OnTeamBtnClick()
    if CTeamMgr.Inst:TeamExists() then
        CTeamMgr.Inst:ShowTeamWnd()
    else
        g_MessageMgr:ShowMessage("2023Halloween_Takeout_HaveNoTeam")
    end
end

function LuaHalloween2023OrderBattleEnterWnd:OnMatchBtnClick()
    Gac2Gas.RequestSignUpCandyDelivery(LuaHalloween2023Mgr.m_MyTeamRoleInfo.driverPlayerId)
end

function LuaHalloween2023OrderBattleEnterWnd:OnCancelButtonClick()
    Gac2Gas.RequestCancelSignCandyDelivery()
end

function LuaHalloween2023OrderBattleEnterWnd:OnExchangeButtonClick()
    if self.isMatching then
        g_MessageMgr:ShowMessage("Halloween2023_OrderBattle_IsMatching")
    else
        if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == CTeamMgr.Inst.LeaderId then
            Gac2Gas.RequestChangeCandyDeliveryJob(LuaHalloween2023Mgr.m_MyTeamRoleInfo.passengerPlayerId, LuaHalloween2023Mgr.m_MyTeamRoleInfo.driverPlayerId)
            LuaHalloween2023Mgr:SetMyTeamRoleInfo(LuaHalloween2023Mgr.m_MyTeamRoleInfo.passengerPlayerId, LuaHalloween2023Mgr.m_MyTeamRoleInfo.driverPlayerId)
            self:InitPlayerInfo()
        else
            g_MessageMgr:ShowMessage("Halloween2023_OrderBattle_No_Qualified_Exchange")
        end
    end
end

--@endregion UIEvent

function LuaHalloween2023OrderBattleEnterWnd:OnEnable()
	g_ScriptEvent:AddListener("QueryPlayerIsMatchingCandyDelivery_Result", self, "UpdateMatch")
	g_ScriptEvent:AddListener("SyncCandyDeliverySignUpResult", self, "UpdateMatch")
	g_ScriptEvent:AddListener("TeamInfoChange", self, "OnTeamInfoChange")
	g_ScriptEvent:AddListener("SyncMyTeamRoleInfo", self, "OnTeamInfoChange")
end

function LuaHalloween2023OrderBattleEnterWnd:OnDisable()
	g_ScriptEvent:RemoveListener("QueryPlayerIsMatchingCandyDelivery_Result", self, "UpdateMatch")
	g_ScriptEvent:RemoveListener("SyncCandyDeliverySignUpResult", self, "UpdateMatch")
	g_ScriptEvent:RemoveListener("TeamInfoChange", self, "OnTeamInfoChange")
	g_ScriptEvent:RemoveListener("SyncMyTeamRoleInfo", self, "OnTeamInfoChange")
end