require("common/common_include")

local CDefaultHeadInfoItem = import "L10.UI.CDefaultHeadInfoItem"
local UILabel = import "UILabel"
local UISlider = import "UISlider"
local CCityWarMgr = import "L10.Game.CCityWarMgr"
local UITable = import "UITable"
local CGameVideoCharacterMgr = import "L10.Game.CGameVideoCharacterMgr"
local CClientObjectMgr=import "L10.Game.CClientObjectMgr"

CLuaHufuTemplate = class()
CLuaHufuTemplate.Path = "ui/citywar/LuaHufuTemplate"

RegistClassMember(CLuaHufuTemplate, "m_HeadInfoItem")
RegistClassMember(CLuaHufuTemplate, "m_RootTable")
RegistClassMember(CLuaHufuTemplate, "m_GuildNameLabel")
RegistClassMember(CLuaHufuTemplate, "m_OwnerNameLabel")
RegistClassMember(CLuaHufuTemplate, "m_ProgressSlider")
RegistClassMember(CLuaHufuTemplate, "m_FullProgress")

RegistClassMember(CLuaHufuTemplate, "m_HPUpdateListener")

function CLuaHufuTemplate:Awake( ... )
	self.m_HeadInfoItem = self.transform:GetComponent(typeof(CDefaultHeadInfoItem))
	self.m_RootTable = self.transform:Find("Table"):GetComponent(typeof(UITable))
	self.m_GuildNameLabel = self.transform:Find("Table/GuildNameBoard"):GetComponent(typeof(UILabel))
	self.m_OwnerNameLabel = self.transform:Find("Table/OwnerNameBoard"):GetComponent(typeof(UILabel))
	self.m_ProgressSlider = self.transform:Find("Table/QnProgressBar"):GetComponent(typeof(UISlider))
	--self.m_RootObj:SetActive(false)
	self.m_GuildNameLabel.gameObject:SetActive(false)
	self.m_OwnerNameLabel.gameObject:SetActive(false)
	self.m_ProgressSlider.gameObject:SetActive(false)
end

function CLuaHufuTemplate:SyncCityOccupyingProgress(npcEngineId, guildName, progress)
	if CLuaCityWarMgr:IsInWatchScene() then
		npcEngineId = CGameVideoCharacterMgr.Inst:GetFakeEngineId(npcEngineId)
	end
	if npcEngineId == self.m_HeadInfoItem.objId then
		self.m_GuildNameLabel.gameObject:SetActive(progress > 0)
		self.m_ProgressSlider.gameObject:SetActive(progress > 0)
		if progress > 0 then
			self.m_GuildNameLabel.text = guildName..LocalString.GetString("占领中...")
			self.m_ProgressSlider.value = progress / self.m_FullProgress
		end
		self.m_RootTable:Reposition()
	end
end

function CLuaHufuTemplate:SyncHufuName()
	self.m_OwnerNameLabel.gameObject:SetActive(CLuaCityWarMgr.CurrentGuildName ~= "")
	if CLuaCityWarMgr.CurrentGuildName ~= "" then
		self.m_OwnerNameLabel.text = CLuaCityWarMgr.CurrentGuildName..LocalString.GetString("的虎符")
	end
	self.m_RootTable:Reposition()
end
function CLuaHufuTemplate:UpdateFireHp()
	if not LuaHalloween2022Mgr.m_BonFireHP then return end
	local hpInfo = LuaHalloween2022Mgr.m_BonFireHP
	local val = hpInfo.hp / hpInfo.crtHpFull
	self.m_ProgressSlider.value = val
end
function CLuaHufuTemplate:OnEnable( ... )
	if LuaHalloween2022Mgr.IsInFireDefencePlay() then
		self.m_ProgressSlider.gameObject:SetActive(true)
		self.m_ProgressSlider.transform:Find("Foreground").gameObject:GetComponent(typeof(UISprite)).spriteName = "common_hpandmp_hp"
		self.m_OwnerNameLabel.gameObject:SetActive(false)
		self.m_GuildNameLabel.gameObject:SetActive(true)
		self.m_GuildNameLabel.text = LocalString.GetString("篝火")
		self.m_ProgressSlider.value = 1
		-- -- 同步血量
		-- if self.m_HPUpdateListener == nil then
		-- 	self.m_HPUpdateListener = DelegateFactory.Action_uint(function(engineId)
		-- 		if LuaHalloween2022Mgr.m_BonFireEnginedId == engineId then
		-- 			local Fire = CClientObjectMgr.Inst:GetObject(engineId)
		-- 			if Fire then
		-- 				local val = Fire.Hp / Fire.HpFull
		-- 				self.m_ProgressSlider.value = val
		-- 			end
		-- 		end
		-- 	end)
		-- end
		g_ScriptEvent:AddListener("UpdateProtectCampfireHp", self, "UpdateFireHp")
		--EventManager.AddListenerInternal(EnumEventType.HpUpdate, self.m_HPUpdateListener)
		return 
	end
	self.m_GuildNameLabel.gameObject:SetActive(false)
	self.m_ProgressSlider.gameObject:SetActive(false)
	self:SyncHufuName()
	self.m_FullProgress = CLuaCityWarMgr.TerritoryHuFuFullProgress
	if CCityWarMgr.Inst:IsInCityWarCopyScene() then
		self.m_FullProgress = CLuaCityWarMgr.MirrorWarHuFuFullProgress
	end

	g_ScriptEvent:AddListener("SyncCityOccupyingProgress", self, "SyncCityOccupyingProgress")
	g_ScriptEvent:AddListener("UpdateCityWarGrade", self, "SyncHufuName")
	g_ScriptEvent:AddListener("HideCity", self, "SyncHufuName")
end

function CLuaHufuTemplate:OnDisable( ... )
	if LuaHalloween2022Mgr.IsInFireDefencePlay() then
		self.m_ProgressSlider.transform:Find("Foreground").gameObject:GetComponent(typeof(UISprite)).spriteName = "common_hpandmp_max"
		g_ScriptEvent:RemoveListener("UpdateProtectCampfireHp", self, "UpdateFireHp")
		--EventManager.RemoveListenerInternal(EnumEventType.HpUpdate, self.m_HPUpdateListener)
		return 
	end
	g_ScriptEvent:RemoveListener("SyncCityOccupyingProgress", self, "SyncCityOccupyingProgress")
	g_ScriptEvent:RemoveListener("UpdateCityWarGrade", self, "SyncHufuName")
	g_ScriptEvent:RemoveListener("HideCity", self, "SyncHufuName")
end
