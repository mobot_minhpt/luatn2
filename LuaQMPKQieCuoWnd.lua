local MessageWndManager = import "L10.UI.MessageWndManager"
local UIGrid=import "UIGrid"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local CTeamMgr = import "L10.Game.CTeamMgr"
local QnTableView=import "L10.UI.QnTableView"
local UISearchView=import "L10.UI.UISearchView"
local Profession=import "L10.Game.Profession"
local QnButton=import "L10.UI.QnButton"
-- local QnAddSubAndInputButton=import "L10.UI.QnAddSubAndInputButton"
local CNumberKeyBoardMgr = import "CNumberKeyBoardMgr"
local CTooltip=import "L10.UI.CTooltip"

CLuaQMPKQieCuoWnd = class()
RegistClassMember(CLuaQMPKQieCuoWnd,"m_SearchBtn")
RegistClassMember(CLuaQMPKQieCuoWnd,"m_LeftTable")
RegistClassMember(CLuaQMPKQieCuoWnd,"m_PageBtn")
RegistClassMember(CLuaQMPKQieCuoWnd,"m_MatchingBtn")
RegistClassMember(CLuaQMPKQieCuoWnd,"m_SignupBtn")
RegistClassMember(CLuaQMPKQieCuoWnd,"m_SignupLabel")
RegistClassMember(CLuaQMPKQieCuoWnd,"m_CloseBtn")
RegistClassMember(CLuaQMPKQieCuoWnd,"m_TeamList")
RegistClassMember(CLuaQMPKQieCuoWnd,"m_HasSignup")
RegistClassMember(CLuaQMPKQieCuoWnd,"m_CurrentPage")
RegistClassMember(CLuaQMPKQieCuoWnd,"m_TotalPage")

RegistClassMember(CLuaQMPKQieCuoWnd,"m_PageLabel")
RegistClassMember(CLuaQMPKQieCuoWnd,"m_IncreseaButton")
RegistClassMember(CLuaQMPKQieCuoWnd,"m_DecreaseButton")

function CLuaQMPKQieCuoWnd:Awake()
    self.m_SearchBtn = self.transform:Find("Anchor/BottomView/SearchView"):GetComponent(typeof(UISearchView))
    self.m_LeftTable = self.transform:Find("Anchor/ZhanDuiList/ListView"):GetComponent(typeof(QnTableView))
    self.m_MatchingBtn = self.transform:Find("Anchor/BottomView/Matching").gameObject
    self.m_SignupBtn = self.transform:Find("Anchor/BottomView/Signup").gameObject
    self.m_SignupLabel = self.transform:Find("Anchor/BottomView/Signup/Label"):GetComponent(typeof(UILabel))
    self.m_CloseBtn = self.transform:Find("Wnd_Bg_Primary_Normal/CloseButton").gameObject
    self.m_TeamList = {}
    self.m_HasSignup = false
    self.m_CurrentPage = 1
    self.m_TotalPage = 1

    local tf=FindChild(self.transform,"QnIncreaseAndDecreaseButton")
    self.m_IncreseaButton = FindChild(tf,"IncreaseButton"):GetComponent(typeof(QnButton))
    self.m_DecreaseButton = FindChild(tf,"DecreaseButton"):GetComponent(typeof(QnButton))
    self.m_PageLabel=FindChild(tf,"Label"):GetComponent(typeof(UILabel))
    UIEventListener.Get(self.m_IncreseaButton.gameObject).onClick=DelegateFactory.VoidDelegate(function(go)
        if self.m_CurrentPage>=self.m_TotalPage then
            return
        end
        Gac2Gas.RequestQmpkQieCuoTeamList(self.m_CurrentPage+1)
    end)
    UIEventListener.Get(self.m_DecreaseButton.gameObject).onClick=DelegateFactory.VoidDelegate(function(go)
        if self.m_CurrentPage<=1 then
            return
        end
        Gac2Gas.RequestQmpkQieCuoTeamList(self.m_CurrentPage-1)
    end)
    UIEventListener.Get(self.m_PageLabel.gameObject).onClick=DelegateFactory.VoidDelegate(function(go)
        self:OnClickPageLabel(go)
    end)
end

function CLuaQMPKQieCuoWnd:OnClickPageLabel(go)
    local min=0
    local max=math.max(1,self.m_TotalPage)
    CNumberKeyBoardMgr.ShowNumberKeyBoardWithRange(min, max, self.m_CurrentPage, 2, DelegateFactory.Action_int(function (val) 
        self.m_PageLabel.text = tostring(val)
    end), 
    DelegateFactory.Action_int(function (val) 
        local page=math.max(1,val)
        page=math.min(self.m_TotalPage,page)
        self.m_PageLabel.text = SafeStringFormat3("%d/%d", page, math.max(self.m_TotalPage,1))
        Gac2Gas.RequestQmpkQieCuoTeamList(page)
    end), 
    self.m_PageLabel, CTooltip.AlignType.Top, true)
end

function CLuaQMPKQieCuoWnd:OnEnable( )
    self.m_SearchBtn.OnSearch = DelegateFactory.Action_string(function(str) self:OnSearchBtnClicked(str) end)
    UIEventListener.Get(self.m_SignupBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnSignupBtnClicked(go) end)
    UIEventListener.Get(self.m_MatchingBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnMatchingBtnClicked(go) end)

    g_ScriptEvent:AddListener("ReplyQmpkQieCuoTeamList", self, "OnReplyQmpkQieCuoTeamList")
    g_ScriptEvent:AddListener("SignUpQmpkQieCuoSuccess", self, "OnSignUpQmpkQieCuoSuccess")
end
function CLuaQMPKQieCuoWnd:OnDisable( )
    g_ScriptEvent:RemoveListener("ReplyQmpkQieCuoTeamList", self, "OnReplyQmpkQieCuoTeamList")
    g_ScriptEvent:RemoveListener("SignUpQmpkQieCuoSuccess", self, "OnSignUpQmpkQieCuoSuccess")
end
function CLuaQMPKQieCuoWnd:OnSignUpQmpkQieCuoSuccess( )
    self.m_HasSignup = true
    self.m_SignupLabel.text = LocalString.GetString("取消报名")
end
function CLuaQMPKQieCuoWnd:OnReplyQmpkQieCuoTeamList( selfQiecuoId, page, totalPage, teamList) 
    self.m_HasSignup = selfQiecuoId > 0

    if self.m_HasSignup then
        self.m_SignupLabel.text = LocalString.GetString("取消报名")
    else
        self.m_SignupLabel.text = LocalString.GetString("报名切磋")
    end
    self.m_TeamList = teamList
    self.m_LeftTable:ReloadData(false, false)

    self.m_CurrentPage = page
    self.m_TotalPage = totalPage

    if self.m_CurrentPage <= 1 then
        self.m_DecreaseButton.Enabled = false
    else
        self.m_DecreaseButton.Enabled = true
    end
    if self.m_CurrentPage >= totalPage then
        self.m_IncreseaButton.Enabled = false
    else
        self.m_IncreseaButton.Enabled = true
    end
    self.m_PageLabel.text = SafeStringFormat3( "%d/%d",page,math.max(totalPage,1) )

    -- self.m_PageBtn:SetMinMax(1, totalPage, 1)
    -- if self.m_TotalPage == 0 then
    --     self.m_TotalPage = 1
    -- end
    -- self.m_PageBtn:OverrideText(SafeStringFormat3("%d/%d", page, self.m_TotalPage))
end
function CLuaQMPKQieCuoWnd:Init( )

    local getNumFunc=function() return #self.m_TeamList end

    local initItemFunc=function(item,index) 
        if index % 4 == 1 or index % 4 == 0 then
            item:SetBackgroundTexture("common_textbg_02_light")
        else
            item:SetBackgroundTexture("common_textbg_02_dark")
        end
        self:InitItem(item,index,self.m_TeamList[index+1])
    end
    
    self.m_LeftTable.m_DataSource = DefaultTableViewDataSource.Create(getNumFunc,initItemFunc)
    -- print(self.m_LeftTable,self.m_LeftTable.m_DataSource)

    Gac2Gas.RequestQmpkQieCuoTeamList(self.m_CurrentPage)

    -- self.m_PageBtn.onValueChanged = DelegateFactory.Action_uint(function(page) 
    --     self:OnPageChanged(page) 
    -- end)
    -- self.m_PageBtn:SetMinMax(1, 1, 1)
    -- self.m_PageBtn:OverrideText("1/1")
    -- self.m_PageBtn.onIncAndDecButtonClicked = DelegateFactory.Action_uint(function(page) 
    --     -- Gac2Gas.RequestQmpkQieCuoTeamList(page)
    -- end)
end

-- function CLuaQMPKQieCuoWnd:OnPageChanged( page) 
--     -- self.m_PageBtn:OverrideText(SafeStringFormat3("%d/%d", self.m_CurrentPage, self.m_TotalPage))
-- end
function CLuaQMPKQieCuoWnd:OnSignupBtnClicked( go) 
    if not CTeamMgr.Inst:MainPlayerIsTeamLeader() and self.m_HasSignup then
        g_MessageMgr:ShowMessage("Qmpk_Only_Leader_Cancel_SignUp_QieCuo")
        return
    end
    if self.m_HasSignup then
        Gac2Gas.RequestCancelSingUpQmpkQieCuo()
        self.m_SignupLabel.text = LocalString.GetString("报名切磋")
        self.m_HasSignup = false
    else
        Gac2Gas.RequestSingUpQmpkQieCuo()
    end
end
function CLuaQMPKQieCuoWnd:OnMatchingBtnClicked( go) 
    MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("QMPK_Qiecuo_To_Matching"), DelegateFactory.Action(function () 
        if self.m_HasSignup then
            Gac2Gas.RequestCancelSingUpQmpkQieCuo()
        end
        Gac2Gas.RequestSingUpQmpkFreeMatch()
        CUIManager.CloseUI(CLuaUIResources.QMPKQieCuoWnd)
    end), nil, nil, nil, false)
end
function CLuaQMPKQieCuoWnd:OnSearchBtnClicked( value) 
    local id
    local default
    default, id = Double.TryParse(value)
    if not default then
        g_MessageMgr:ShowMessage("QMPK_Search_Zhandui_Only_By_Id")
        return
    end
    Gac2Gas.QueryQmpkQieCuoTeamByMemberId(id)
end

function CLuaQMPKQieCuoWnd:InitItem(item,index,team)
    local transform = item.transform
    local m_NameLabel = transform:Find("Name"):GetComponent(typeof(UILabel))
    local m_MemberGrid = transform:Find("Members/MemberGrid"):GetComponent(typeof(UIGrid))
    local m_MemberObj = transform:Find("Member").gameObject
    local m_QieCuoBtn = transform:Find("Btn").gameObject
    
    m_NameLabel.text = team.m_LeaderName
    Extensions.RemoveAllChildren(m_MemberGrid.transform)
    do
        local i = 0 local len = #team.m_Member
        while i < len do
            local go = NGUITools.AddChild(m_MemberGrid.gameObject, m_MemberObj)
            go:SetActive(true)
            go:GetComponent(typeof(UISprite)).spriteName = Profession.GetIconByNumber(team.m_Member[i+1])
            i = i + 1
        end
    end
    m_MemberGrid:Reposition()
    UIEventListener.Get(m_QieCuoBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        Gac2Gas.RequestQmpkQieCuoWithOtherTeam(team.m_Id)
    end)
end

return CLuaQMPKQieCuoWnd
