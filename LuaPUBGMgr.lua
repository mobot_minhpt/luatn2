local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"
local CPlayDropItemObject = import "L10.Game.CPlayDropItemObject"
local CPUBGMgr = import "L10.Game.CPUBGMgr"
local ChiJi_Setting = import "L10.Game.ChiJi_Setting"
local CTeamMgr = import "L10.Game.CTeamMgr"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local EPlayerFightProp = import "L10.Game.EPlayerFightProp"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local EDamageText = import "L10.Game.EDamageText"

LuaPUBGMgr = {}

--[[
1-199  Equip
200-399  Item
400-599  Skill
]]--

function LuaPUBGMgr.IsEquip(id)
    return id >= 1 and id <= 199
end

function LuaPUBGMgr.IsItem(id)
    return id >= 200 and id <= 399
end

function LuaPUBGMgr.IsSkill(id)
    return id >= 400 and id <= 499
end

--[[批处理ID
info: {itemId: x, enable: x, count: 0, id : 0}
]]--
function LuaPUBGMgr.ParseID(id)
    local itemId = math.floor(id % 1000)
    local enable = math.floor(id / 1000) % 10
    local count = math.floor( id / 10000 ) % 100
    return {itemId = itemId, enable = enable, count = count, id = id}
end

-- 打开报名界面
function LuaPUBGMgr.QueryChiJiApplyInfo()
    -- 请求报名信息
    Gac2Gas.QueryChiJiApplyInfo()
end

LuaPUBGMgr.m_IsApplied = false
function LuaPUBGMgr.OpenPUBGStartWnd(bApply)
    LuaPUBGMgr.m_IsApplied = bApply
    CUIManager.ShowUI(CLuaUIResources.PUBGStartWnd)
end

-- 请求
function LuaPUBGMgr.RequstSyncTeamInfo()
    Gac2Gas.RequestSyncChiJiTeamMemberHpInfo()
end
-- C#调用lua
CPUBGMgr.m_hookOpenChiJiTempBag = function (mgr)
    LuaPUBGMgr.OpenPUBGSelfBag()
end

-- C#调用lua
CTeamMgr.m_ChiJiWatchAction = function(mgr, isWatch, playerId)
    if isWatch then
        Gac2Gas.StartWatchChiJiTeamMember(playerId)
    else
        Gac2Gas.StopWatchChiJiTeamMember()
    end
end

-- 打开临时包裹
function LuaPUBGMgr.OpenPUBGSelfBag()
    Gac2Gas.RequestOpenChiJiBag()
end

-- 更新整体信息
LuaPUBGMgr.m_PackageInfos = {}
function LuaPUBGMgr.UpdateWholePackageInfos(list, bShowBagWnd)
    if not list then return end
    LuaPUBGMgr.m_PackageInfos = {}
    for i = 0, list.Count-1 do
        local packageInfo = LuaPUBGMgr.ParseID(list[i])
        table.insert(LuaPUBGMgr.m_PackageInfos, packageInfo)
    end
    if bShowBagWnd then
        if CUIManager.IsLoaded(CLuaUIResources.PUBGSelfPackageWnd) then
            g_ScriptEvent:BroadcastInLua("UpdateWholePUBGPackageInfos")
        else
            if not CUIManager.IsLoaded(CLuaUIResources.PUBGLickingBagWnd) then
                CUIManager.ShowUI(CLuaUIResources.PUBGSelfPackageWnd)
            end
        end
    end
end

-- 设置包裹道具 pos 包裹位置
function LuaPUBGMgr.SetChiJiBagItemId(pos, itemId)
    local packageInfo = LuaPUBGMgr.ParseID(itemId)
    if pos > #LuaPUBGMgr.m_PackageInfos then
        table.insert(LuaPUBGMgr.m_PackageInfos, packageInfo)
    else
        LuaPUBGMgr.m_PackageInfos[pos] = packageInfo
    end
    g_ScriptEvent:BroadcastInLua("UpdateSinglePUBGPackageInfo", pos, packageInfo)
end

-- 获得玩家已经激活的装备信息
function LuaPUBGMgr.GetEquipmentInfos()
    if not LuaPUBGMgr.m_PackageInfos then return {} end
    local equips = {}

    -- equip有10种类型, 为0表示该位置没有数据
    local default = {}
    for i = 1, 10 do
        table.insert(equips, 0)
    end

    for i = 1, #LuaPUBGMgr.m_PackageInfos do
        local info = LuaPUBGMgr.m_PackageInfos[i]
        if LuaPUBGMgr.IsEquip(info.itemId) and info.enable ~= 0 then
            local chijiItem = ChiJi_Item.GetData(info.itemId)
            if chijiItem then
                local equip = EquipmentTemplate_Equip.GetData(chijiItem.ItemId)
                if equip then
                    equips[equip.Type] = info
                end
            end
        end
    end

    return equips
end

function LuaPUBGMgr.GetQiangHuaLevel()
    local level = 0
    local setting = ChiJi_Setting.GetData()
    for i = 1, #LuaPUBGMgr.m_PackageInfos do
        local info = LuaPUBGMgr.m_PackageInfos[i]
        if info.itemId == setting.QiangHuaTemplateId then
            if info.count > level then
                level = info.count
            end
        end
    end
    return level
end

-- 打开舔包界面
LuaPUBGMgr.m_SelectedDropKey = 0
LuaPUBGMgr.m_OthersPackageInfos = {}
function LuaPUBGMgr.OpenLickingBag(key, list, bagList)
    if not list then return end
    LuaPUBGMgr.m_SelectedDropKey = key
    LuaPUBGMgr.m_OthersPackageInfos = {}
    for i = 0, list.Count-1 do
        local otherPackageInfo = LuaPUBGMgr.ParseID(list[i])
        table.insert(LuaPUBGMgr.m_OthersPackageInfos, otherPackageInfo)
    end
    LuaPUBGMgr.UpdateWholePackageInfos(bagList, false)
    if CUIManager.IsLoaded(CLuaUIResources.PUBGLickingBagWnd) then
        g_ScriptEvent:BroadcastInLua("UpdatePUBGOthersPackageInfos")
    else
        CUIManager.ShowUI(CLuaUIResources.PUBGLickingBagWnd)
    end
end

-- 更新舔包中他人包裹的信息
function LuaPUBGMgr.UpdateLickingBag(key, list)
    if not list then return end
    if key ~= LuaPUBGMgr.m_SelectedDropKey then return end

    LuaPUBGMgr.m_OthersPackageInfos = {}
    for i = 0, list.Count-1 do
        local otherPackageInfo = LuaPUBGMgr.ParseID(list[i])
        table.insert(LuaPUBGMgr.m_OthersPackageInfos, otherPackageInfo)
    end

    g_ScriptEvent:BroadcastInLua("UpdateLickingBag")
end

-- 创建一个周年庆PVP掉落物
function LuaPUBGMgr.CreateChiJiDropItem(key, itemId)
    local tempKey = tonumber(key)
    local itemId = tonumber(itemId)
    local posY = math.floor(tempKey % 1000)
    local posX = math.floor(tempKey / 1000)
    local itemObj = CPlayDropItemObject.CreateCPlayDropItemObject(itemId, posX, posY, key)
    if itemObj then
        local info = LuaPUBGMgr.ParseID(itemId)
        if info then
            itemObj:ShowItemObject(info.itemId, info.count, info.enable)
        end
    end
end

-- 删除周年庆PVP掉落物
function LuaPUBGMgr.DeleteChiJiDropItem(key)
    CPlayDropItemObject.DeleteCPlayDropItemObject(key)
end

-- 更新吃鸡小地图毒圈显示
LuaPUBGMgr.m_CenterPosX = 0
LuaPUBGMgr.m_CenterPosY = 0
LuaPUBGMgr.m_HalfSideLength = 0
LuaPUBGMgr.m_NextCenterPosX = 0
LuaPUBGMgr.m_NextCenterPosY = 0
LuaPUBGMgr.m_NextHalfSideLength = 0
function LuaPUBGMgr.UpdateChiJiMiniMap(centerPosX, centerPosY, halfSideLength, nextCenterPosX, nextCenterPosY, nextHalfSideLength)
    LuaPUBGMgr.m_CenterPosX = centerPosX
    LuaPUBGMgr.m_CenterPosY = centerPosY
    LuaPUBGMgr.m_HalfSideLength = halfSideLength
    LuaPUBGMgr.m_NextCenterPosX = nextCenterPosX
    LuaPUBGMgr.m_NextCenterPosY = nextCenterPosY
    LuaPUBGMgr.m_NextHalfSideLength = nextHalfSideLength
    g_ScriptEvent:BroadcastInLua("UpdateChiJiMiniMap")
end

-- 打开吃鸡淘汰信息界面
LuaPUBGMgr.m_ResultRank = 0
LuaPUBGMgr.m_ResultTotalTeamCounter = 0
LuaPUBGMgr.m_ResultTeamInfos = nil
function LuaPUBGMgr.OpenPUBGResultWnd(rank, totalTeamCount, teamList)
    LuaPUBGMgr.m_ResultRank = rank
    LuaPUBGMgr.m_ResultTotalTeamCounter = totalTeamCount
    LuaPUBGMgr.m_ResultTeamInfos = {}
    for i = 0, teamList.Count-1 do
        -- {name, gender, class, score, totalScorem, killNum, dps, liveDuration}
        local teamInfo = teamList[i]
        if teamInfo.Count >= 7 then
            local name = tostring(teamInfo[0])
            local gender = tonumber(teamInfo[1])
            local class = tonumber(teamInfo[2])
            local score = tonumber(teamInfo[3])
            local totalScorem = tonumber(teamInfo[4])
            local killNum = tonumber(teamInfo[5])
            local dps = tonumber(teamInfo[6])
            local liveDuration = tonumber(teamInfo[7])
            table.insert(LuaPUBGMgr.m_ResultTeamInfos, {
                name = name,
                gender = gender,
                class = class,
                score = score,
                totalScorem = totalScorem,
                killNum = killNum,
                dps = dps,
                liveDuration = liveDuration,
            })
        end
    end
    CUIManager.ShowUI(CLuaUIResources.PUBGResultWnd)
end


-- 同步吃鸡属性
function LuaPUBGMgr.SyncChiJiFightProp(engineId, propId, value)

    if engineId == CClientMainPlayer.Inst.EngineId then
        if not CClientMainPlayer.Inst or not CClientMainPlayer.Inst.FightProp then return end

        CClientMainPlayer.Inst.FightProp:SetParam(propId, value)
        g_ScriptEvent:BroadcastInLua("SyncChiJiFightProp")

        -- 用于更新mainFrame
        EventManager.Broadcast(EnumEventType.MainplayerFightPropUpdate)
    end

    local obj = CClientObjectMgr.Inst:GetObject(engineId)
    if obj then
        if propId == EnumToInt(EPlayerFightProp.PlayHp) then
            local delta = value - obj.PlayHp
            if delta ~= 0 then
                if engineId == CClientMainPlayer.Inst.EngineId then
                    obj:ShowDamageText(delta, EDamageText.Hp, false)
                else
                    obj:ShowDamageText(delta, EDamageText.OtherHp, false)
                end
            end
        end
        obj:UpdateFightParam(propId, value)
    end
end

function LuaPUBGMgr.SyncChiJiLeftTeamCountInfo(playerNum, teamNum)
    CPUBGMgr.Inst.ChiJiPlayerNum = playerNum
    CPUBGMgr.Inst.ChiJiTeamNum = teamNum
    g_ScriptEvent:BroadcastInLua("SyncChiJiLeftTeamCountInfo")
end

