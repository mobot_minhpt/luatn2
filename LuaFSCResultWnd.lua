local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local Animation = import "UnityEngine.Animation"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local EShareType = import "L10.UI.EShareType"

LuaFSCResultWnd = class()

LuaFSCResultWnd.s_Score = 0
LuaFSCResultWnd.s_Result = nil
LuaFSCResultWnd.s_bNewRecord = nil
LuaFSCResultWnd.s_ExtraData = nil

RegistClassMember(LuaFSCResultWnd, "m_Anim")
RegistClassMember(LuaFSCResultWnd, "m_UnlockAnim")
RegistClassMember(LuaFSCResultWnd, "m_WinInfo")
RegistClassMember(LuaFSCResultWnd, "m_LoseInfo")
RegistClassMember(LuaFSCResultWnd, "m_PingInfo")
RegistClassMember(LuaFSCResultWnd, "m_Card")
RegistClassMember(LuaFSCResultWnd, "m_Unlock")
RegistClassMember(LuaFSCResultWnd, "m_UnlockLabel")
RegistClassMember(LuaFSCResultWnd, "m_ShareBtn")
RegistClassMember(LuaFSCResultWnd, "m_RankBtn")
RegistClassMember(LuaFSCResultWnd, "m_CloseBtn")

RegistClassMember(LuaFSCResultWnd, "m_Tick")
RegistClassMember(LuaFSCResultWnd, "m_UnlockTick")
RegistClassMember(LuaFSCResultWnd, "m_Card2Npc")

function LuaFSCResultWnd:Awake()
    self.m_Card2Npc = {}
    ZhouNianQing2023_PlayWithNpc.Foreach(function(k, v) 
        self.m_Card2Npc[v.RewardSpCard] = k
    end)

    self.m_Anim = self.transform:GetComponent(typeof(Animation))
    self.m_WinInfo = self.transform:Find("Info/WinStyle/Win_Info")
    self.m_LoseInfo = self.transform:Find("Info/LoseStyle/Lose_Info")
    self.m_PingInfo = self.transform:Find("Info/PingStyle/Ping_Info")
    self.m_Card = self.transform:Find("Info/Unlock/FSCCard"):GetComponent(typeof(CCommonLuaScript))
    self.m_Unlock = self.transform:Find("Info/Unlock").gameObject
    self.m_UnlockAnim = self.m_Unlock:GetComponent(typeof(Animation))
    self.m_UnlockLabel = self.transform:Find("Info/Unlock/Label"):GetComponent(typeof(UILabel))
    self.m_ShareBtn = self.transform:Find("Other/BottomBtns/Btn01").gameObject
    self.m_RankBtn = self.transform:Find("Other/BottomBtns/Btn02").gameObject
    self.m_CloseBtn = self.transform:Find("Other/CloseButton").gameObject

    self.m_Unlock:SetActive(false)
    self.m_ShareBtn:SetActive(false)

    UIEventListener.Get(self.m_RankBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        Gac2Gas.Anniv2023FSC_QueryRankData()
    end)

    self.m_UnlockTick = {}
end

function LuaFSCResultWnd:Init()
    local cardId = LuaFSCResultWnd.s_ExtraData and LuaFSCResultWnd.s_ExtraData[1]
    local times = LuaFSCResultWnd.s_ExtraData and LuaFSCResultWnd.s_ExtraData[2]
    local score, newRecord
    if LuaFSCResultWnd.s_Result == 0 then 
        self.m_WinInfo:GetChild(0):GetComponent(typeof(UILabel)).text = ZhouNianQing2023_Setting.GetData().WinShow
        score = self.m_WinInfo:GetChild(2):GetComponent(typeof(UILabel))
        newRecord = self.m_WinInfo:GetChild(3).gameObject
        if times and times <= 3 then 
            self.m_Anim:Play("common_result_win_1")
            if LuaFSCResultWnd.s_bNewRecord then
                if not CommonDefs.IS_VN_CLIENT then
                    self.m_ShareBtn:SetActive(true)
                end
                UIEventListener.Get(self.m_ShareBtn).onClick = DelegateFactory.VoidDelegate(function (go)
                    self.m_ShareBtn:SetActive(false)
                    self.m_CloseBtn:SetActive(false)
                    self.m_RankBtn:SetActive(false)
                    CUIManager.SetUITop(CLuaUIResources.FSCResultWnd)
                    CUICommonDef.CaptureScreen("screenshot", true, false, nil, DelegateFactory.Action_string_bytes(function(fullPath, jpgBytes)
                        ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, fullPath)
                        self.m_ShareBtn:SetActive(true)
                        self.m_CloseBtn:SetActive(true)
                        self.m_RankBtn:SetActive(true)
                        CUIManager.ResetUITop(CLuaUIResources.FSCResultWnd)
                    end))
                end)
            end
            self.m_Unlock:SetActive(true)
            --local panel = self.m_Card:GetComponent(typeof(UIPanel))
            --if not panel then panel = self.m_Card.gameObject:AddComponent(typeof(UIPanel)) end
            --panel.depth = self.transform:GetComponent(typeof(UIPanel)).depth + 1
            local data = NPC_NPC.GetData(self.m_Card2Npc[cardId])
            local name = data and data.Name or ""
            self.m_UnlockLabel.text = LocalString.GetString("战胜")..name..times..LocalString.GetString("/3次可得")
        else
            self.m_Anim:Play("common_result_win_2")
        end
    elseif LuaFSCResultWnd.s_Result == 1 then
        self.m_Anim:Play("common_result_lose")
        self.m_LoseInfo:GetChild(0):GetComponent(typeof(UILabel)).text = ZhouNianQing2023_Setting.GetData().LossShow
        score = self.m_LoseInfo:GetChild(2):GetComponent(typeof(UILabel))
    else
        self.m_Anim:Play("common_result_ping")
        self.m_PingInfo:GetChild(0):GetComponent(typeof(UILabel)).text = ZhouNianQing2023_Setting.GetData().TieShow
        score = self.m_PingInfo:GetChild(2):GetComponent(typeof(UILabel))
    end
    
    if cardId and times and times <= 3 then 
        self.m_Card.m_LuaSelf:Init(cardId)
    end
    score.text = LuaFSCResultWnd.s_Score
    if newRecord then 
        newRecord:SetActive(LuaFSCResultWnd.s_bNewRecord)
    end

    UnRegisterTick(self.m_Tick)
    UnRegisterTick(self.m_UnlockTick[1])
    UnRegisterTick(self.m_UnlockTick[2])
    if LuaFSCResultWnd.s_Result == 0 and times and times <= 3 then 
        self.m_Tick = RegisterTickOnce(function()
            local len = {
                self.m_UnlockAnim["fscresultwnd_sui1"].length / 2,
                self.m_UnlockAnim["fscresultwnd_sui2"].length / 2,
            } 
            self.m_UnlockAnim:Play("fscresultwnd_sui"..1)
            SoundManager.Inst:PlayOneShot(LuaFourSeasonCardMgr.Music.crack, Vector3.zero, nil, 0)
            if times > 1 then
                self.m_UnlockTick[1] = RegisterTickOnce(function()
                    self.m_UnlockAnim:Play("fscresultwnd_sui"..2)
                    SoundManager.Inst:PlayOneShot(LuaFourSeasonCardMgr.Music.crack, Vector3.zero, nil, 0)
                end, len[1] * 1000)
            end
            if times > 2 then 
                self.m_UnlockTick[2] = RegisterTickOnce(function()
                    self.m_UnlockAnim:Play("fscresultwnd_sui"..3)
                    SoundManager.Inst:PlayOneShot(LuaFourSeasonCardMgr.Music.crack, Vector3.zero, nil, 0)
                end, (len[1] + len[2]) * 1000)
            end
        end, self.m_Anim["common_result_win_1"].length / 2 * 1000)
    end
end

function LuaFSCResultWnd:OnDestroy()
    UnRegisterTick(self.m_Tick)
    self.m_Tick = nil

    UnRegisterTick(self.m_UnlockTick[1])
    UnRegisterTick(self.m_UnlockTick[2])
    self.m_UnlockTick = {}
end

