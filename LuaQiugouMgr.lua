local MessageWndManager = import "L10.UI.MessageWndManager"
local DelegateFactory = import "DelegateFactory"
local CUIResources = import "L10.UI.CUIResources"
local CYuanbaoMarketMgr = import "L10.UI.CYuanbaoMarketMgr"

LuaQiugouMgr = {}

---

function Gas2Gac.SendRequestBuyItemPrice(itemTemplateId, basisPrice, minPrice, maxPrice)

    g_ScriptEvent:BroadcastInLua("QueryItemPriceBack",basisPrice, minPrice, maxPrice)
end

function Gas2Gac.CreateRequestBuyOrderSucc(orderData)
    local list = MsgPackImpl.unpack(orderData)
    local orderId = list[0]
    local itemTemplateId = list[1]
    local count = list[2]
    local price = list[3]
    local onShelfTime = list[4]
    local expireTime = list[5]
    if not LuaQiugouMgr.BuyOrderTable then
      LuaQiugouMgr.BuyOrderTable = {}
    end
    table.insert(LuaQiugouMgr.BuyOrderTable,{orderId,itemTemplateId,price,count,onShelfTime,expireTime})

    table.sort(LuaQiugouMgr.BuyOrderTable,function(a,b)
      return a[5] > b[5]
    end)
    g_ScriptEvent:BroadcastInLua("UpdateBuyOrderItems")
end

function Gas2Gac.CancelRequestBuyOrderSucc(orderData)
    local list = MsgPackImpl.unpack(orderData)
    local orderId = list[0]
    local itemTemplateId = list[1]
    local count = list[2]
    local price = list[3]
    local onShelfTime = list[4]
    local expireTime = list[5]

    if LuaQiugouMgr.BuyOrderTable then
      for i,v in pairs(LuaQiugouMgr.BuyOrderTable) do
        if v[1] == orderId then
          table.remove(LuaQiugouMgr.BuyOrderTable,i)
          break
        end
      end
    end

    g_ScriptEvent:BroadcastInLua("UpdateBuyOrderItems")
end

function Gas2Gac.SellItemToRequestBuyOrderSucc(itemTemplateId, orderId, count)

  CUIManager.CloseUI(CLuaUIResources.QiugouSellWnd)
  g_ScriptEvent:BroadcastInLua("UpdateSellItemInfo")
end

function Gas2Gac.SendMyRequestBuyOrder(myOrders)
    LuaQiugouMgr.BuyOrderTable = {}

    local list = MsgPackImpl.unpack(myOrders)
    for i = 0, list.Count - 1, 6 do
        local orderId = list[i]
        local itemTemplateId = list[i+1]
        local price = list[i+2]
        local count = list[i+3]
        local onShelfTime = list[i+4]
        local expireTime = list[i+5]
        table.insert(LuaQiugouMgr.BuyOrderTable,{orderId,itemTemplateId,price,count,onShelfTime,expireTime})
    end
    table.sort(LuaQiugouMgr.BuyOrderTable,function(a,b)
      return a[5] > b[5]
    end)
    g_ScriptEvent:BroadcastInLua("UpdateBuyOrderItems")
end

function Gas2Gac.SendRequestBuyOrderCountByType(type, data)

  LuaQiugouMgr.BuyOrderTypeTotalTable = {}
  LuaQiugouMgr.BuyOrderTypeTotalCount = 0

  local list = MsgPackImpl.unpack(data)
  for i = 0, list.Count - 1, 2 do
    local itemTemplateId = tonumber(list[i])
    local count = tonumber(list[i+1])
    LuaQiugouMgr.BuyOrderTypeTotalTable[itemTemplateId] = count
    LuaQiugouMgr.BuyOrderTypeTotalCount = LuaQiugouMgr.BuyOrderTypeTotalCount + count
  end

  g_ScriptEvent:BroadcastInLua("UpdateBuyOrderTotalItems")
end

function Gas2Gac.SendRequestBuyOrderList(itemTemplateId, page, totalPage, data)
  LuaQiugouMgr.BuyOrderListItemId = itemTemplateId
  LuaQiugouMgr.BuyOrderListPage = page
  LuaQiugouMgr.BuyOrderListTotalPage = totalPage
  LuaQiugouMgr.BuyOrderListItemTable = {}

  local list = MsgPackImpl.unpack(data)
  for i = 0, list.Count - 1, 6 do
    local orderId = tonumber(list[i])
    local playerId = list[i+1]
    local count = tonumber(list[i+2])
    local price = tonumber(list[i+3])
    local onShelfTime = list[i+4]
    local expireTime = list[i+5]
    table.insert(LuaQiugouMgr.BuyOrderListItemTable,{orderId,playerId,count,price,onShelfTime,expireTime})
  end
  g_ScriptEvent:BroadcastInLua("UpdateBuyOrderListItems")
end

function Gas2Gac.OpenRequestBuyWindowResult(shopId, itemId)
  if shopId > 0 then
    -- 有商店
    LuaQiugouMgr.RequestBuyItemId = itemId
    CUIManager.ShowUI(CLuaUIResources.QiugouWnd)
  end
end

function Gas2Gac.RequestBuyExistLowPriceItem(itemTempId, itemCount, singlePrice)
  -- Gac2Gas.CreateRequestBuyOrder用到的3个参数

  local item = Item_Item.GetData(itemTempId)
  local str = SafeStringFormat3(LocalString.GetString('玩家商店有在售的%s，且价格低于你的求购价格。是否立即前往购买？'),item.Name)
  MessageWndManager.ShowOKCancelMessage(str, DelegateFactory.Action(function ()
    if CUIManager.IsLoaded(CUIResources.YuanBaoMarketWnd) then
      g_ScriptEvent:BroadcastInLua("SearchFurnitureItem",itemTempId)
      CUIManager.CloseUI(CLuaUIResources.QiugouWnd)
    else
      CYuanbaoMarketMgr.m_ShowPlayerShop_CS2LuaHook(itemTempId)
    end
  end), DelegateFactory.Action(function()
    Gac2Gas.CreateRequestBuyOrder(itemTempId,itemCount,singlePrice,true)
    --CUIManager.CloseUI(CLuaUIResources.QiugouWnd)
  end), nil, nil, false)
end

function Gas2Gac.SendRequestBuyOrderCount(data)

  LuaQiugouMgr.BuyOrderTabNumTable = {}
  local list = MsgPackImpl.unpack(data)
  if list then
    for i = 0, list.Count - 1, 2 do
      local subType = tonumber(list[i])
      local count = tonumber(list[i+1])
      LuaQiugouMgr.BuyOrderTabNumTable[subType] = count
    end
    g_ScriptEvent:BroadcastInLua("UpdateBuyOrderTabNums")
  end
end
