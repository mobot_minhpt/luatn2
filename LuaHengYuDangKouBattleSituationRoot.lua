local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local UISprite = import "UISprite"
local CommonDefs = import "L10.Game.CommonDefs"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local DelegateFactory  = import "DelegateFactory"

LuaHengYuDangKouBattleSituationRoot = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaHengYuDangKouBattleSituationRoot, "Btn", "Btn", GameObject)
RegistChildComponent(LuaHengYuDangKouBattleSituationRoot, "CountDownLabel", "CountDownLabel", UILabel)
RegistChildComponent(LuaHengYuDangKouBattleSituationRoot, "Level_1", "Level_1", GameObject)
RegistChildComponent(LuaHengYuDangKouBattleSituationRoot, "Level_2", "Level_2", GameObject)
RegistChildComponent(LuaHengYuDangKouBattleSituationRoot, "Level_3", "Level_3", GameObject)

--@endregion RegistChildComponent end

function LuaHengYuDangKouBattleSituationRoot:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.Btn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBtnClick()
	end)


    --@endregion EventBind end
end

--  curDifficulty=sceneInfo.difficulty --当前难度
-- 	lastResetDifficultyTs=123,
-- 	stage=sceneInfo.stage,
-- 	progress=sceneInfo.progress,
-- 	difficulty={ --已经通通关的boss选择的难度
-- 		1: 1,
-- 		2: 3,
-- 		3: 2,
-- 	}
-- 	leftTime=1000, --限时小怪剩余时间
function LuaHengYuDangKouBattleSituationRoot:Init()
	if not LuaHengYuDangKouMgr:IsInEiltePlay() then
		self.transform.gameObject:SetActive(false)
		return
	end

	local sceneInfo = LuaHengYuDangKouMgr.m_SceneInfo
	self:SyncHengYuDangKouSceneInfo(sceneInfo)
	
	self:UpdateInfo()
	self.m_Tick = RegisterTick(function()
		self:UpdateInfo()
	end, 100)
end

function LuaHengYuDangKouBattleSituationRoot:UpdateInfo()
	local lastResetDifficultyTs = LuaHengYuDangKouMgr.m_SceneInfo.lastResetDifficultyTs
	local diff = HengYuDangKou_Setting.GetData().ResetDifficultyCD - (CServerTimeMgr.Inst.timeStamp - lastResetDifficultyTs)
	if diff <= 0 then
		CUICommonDef.SetActive(self.Btn.gameObject, true, true)
		self.CountDownLabel.gameObject:SetActive(false)
		self.CountDownLabel.text = ""
	else
		CUICommonDef.SetActive(self.Btn.gameObject, false, true)
		self.CountDownLabel.gameObject:SetActive(true)
		self.CountDownLabel.text = tostring(math.floor(diff))
	end

	self.transform:Find("Btn/LabelTable"):GetComponent(typeof(UITable)):Reposition()
end

function LuaHengYuDangKouBattleSituationRoot:SyncHengYuDangKouSceneInfo(sceneInfo)
	-- 设置难度
	local curDifficulty = sceneInfo.curDifficulty
	self.Level_1:SetActive(curDifficulty == 1)
	self.Level_2:SetActive(curDifficulty == 2)
	self.Level_3:SetActive(curDifficulty == 3)
end

function LuaHengYuDangKouBattleSituationRoot:OnEnable()

	g_ScriptEvent:AddListener("SyncHengYuDangKouSceneInfo", self, "SyncHengYuDangKouSceneInfo")
	
	self:Init()
end

function LuaHengYuDangKouBattleSituationRoot:OnDisable()
	UnRegisterTick(self.m_Tick)

	g_ScriptEvent:RemoveListener("SyncHengYuDangKouSceneInfo", self, "SyncHengYuDangKouSceneInfo")
end

--@region UIEvent

function LuaHengYuDangKouBattleSituationRoot:OnBtnClick()
	-- 打开选择界面
	CUIManager.ShowUI(CLuaUIResources.HengYuDangKouSelectDifficultyBox)
end


--@endregion UIEvent

