require("common/common_include")

local CommonDefs = import "L10.Game.CommonDefs"
local SoundManager = import "SoundManager"
local Vector3 = import "UnityEngine.Vector3"
local Animation = import "UnityEngine.Animation"
local CUITexture = import "L10.UI.CUITexture"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local EShareType = import "L10.UI.EShareType"
local CUICommonDef = import "L10.UI.CUICommonDef"
local UILabel = import "UILabel"

CLuaBabyDivinationWnd = class()
RegistClassMember(CLuaBabyDivinationWnd,"m_QianName")
RegistClassMember(CLuaBabyDivinationWnd,"m_ShareButton")
RegistClassMember(CLuaBabyDivinationWnd,"m_QianTongAni")
RegistClassMember(CLuaBabyDivinationWnd,"m_QianAni")
RegistClassMember(CLuaBabyDivinationWnd,"m_QianYuAni")

RegistClassMember(CLuaBabyDivinationWnd,"m_TitleLabel")
RegistClassMember(CLuaBabyDivinationWnd,"m_ContentLabel")

RegistClassMember(CLuaBabyDivinationWnd,"m_QianYuClipName")
RegistClassMember(CLuaBabyDivinationWnd,"m_QianClipName")
RegistClassMember(CLuaBabyDivinationWnd,"m_QianTongClipName")

function CLuaBabyDivinationWnd:Awake()
    self.m_QianName = self.transform:Find("Qian/QianName"):GetComponent(typeof(CUITexture))
    self.m_ShareButton = self.transform:Find("Ani/QianYu/ShareBtn").gameObject
    self.m_QianTongAni = self.transform:Find("QianTong"):GetComponent(typeof(Animation))
    self.m_QianAni = self.transform:Find("Qian"):GetComponent(typeof(Animation))
    self.m_QianYuAni = self.transform:Find("Ani"):GetComponent(typeof(Animation))

    self.m_TitleLabel = self.transform:Find("Ani/QianYu/TitleLabel"):GetComponent(typeof(UILabel))
    self.m_ContentLabel = self.transform:Find("Ani/QianYu/ContentLabel"):GetComponent(typeof(UILabel))

    self.m_QianYuClipName = "BabyQiuQian_QianYu"
    self.m_QianClipName = "BabyQiuQian_Qian"
    self.m_QianTongClipName = "BabyQiuQian_QianTong"


    CommonDefs.AddOnClickListener(self.m_ShareButton, DelegateFactory.Action_GameObject(function(go) self:OnShareButtonClick() end), false)
end

function CLuaBabyDivinationWnd:Init()
	
	self:SampleAni(self.m_QianYuAni, self.m_QianYuClipName, 0)
    self:SampleAni(self.m_QianTongAni, self.m_QianTongClipName, 0)
    self:SampleAni(self.m_QianAni, self.m_QianClipName, 0)
    
    self.m_TitleLabel.text = "#"..(LuaBabyMgr.DivinationTbl.title or "").."#"
    self.m_ContentLabel.text = LuaBabyMgr.DivinationTbl.content or ""
    self:DoAni()
end

function CLuaBabyDivinationWnd:SampleAni(anim, clipName, time)
	anim:get_Item(clipName).time = time
    anim:get_Item(clipName).enabled = true
    anim:get_Item(clipName).weight = 1
    anim:Sample()
    anim:get_Item(clipName).enabled = false
end

function CLuaBabyDivinationWnd:DoAni()
	self.m_QianYuAni:get_Item(self.m_QianYuClipName).time = 0
    self.m_QianTongAni:get_Item(self.m_QianTongClipName).time = 0
    self.m_QianAni:get_Item(self.m_QianClipName).time = 0
    self.m_QianTongAni:Play()
    self.m_QianYuAni:Play()
    self.m_QianAni:Play()
    SoundManager.Inst:PlayOneShot(SoundManager.UISoundEvents.YuanDanYaoQianound, Vector3.zero, nil, 0)
end

function CLuaBabyDivinationWnd:OnShareButtonClick()
    CUICommonDef.CaptureScreen(
        "screenshot",
        true,
        false,
        self.m_ShareButton,
        DelegateFactory.Action_string_bytes(
            function(fullPath, jpgBytes)
                ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, fullPath)
            end
        ),
        false
    )
end
