local UIInput = import "UIInput"

local CButton = import "L10.UI.CButton"

local QnInput = import "L10.UI.QnInput"

local CUITexture = import "L10.UI.CUITexture"

local UILabel = import "UILabel"

local GameObject = import "UnityEngine.GameObject"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"

local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local CCarnivalLotteryMgr = import "L10.Game.CCarnivalLotteryMgr"

LuaChristmas2023XueHuaXuYuSendGiftWnd = class()
RegistClassMember(LuaChristmas2023XueHuaXuYuSendGiftWnd, "Highlight")

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaChristmas2023XueHuaXuYuSendGiftWnd, "NoGift", "NoGift", GameObject)
RegistChildComponent(LuaChristmas2023XueHuaXuYuSendGiftWnd, "CountLabel", "CountLabel", UILabel)
RegistChildComponent(LuaChristmas2023XueHuaXuYuSendGiftWnd, "IconTexture", "IconTexture", CUITexture)
RegistChildComponent(LuaChristmas2023XueHuaXuYuSendGiftWnd, "SendBtn", "SendBtn", CButton)
RegistChildComponent(LuaChristmas2023XueHuaXuYuSendGiftWnd, "PlayerImage", "PlayerImage", CUITexture)
RegistChildComponent(LuaChristmas2023XueHuaXuYuSendGiftWnd, "NameLabel", "NameLabel", UILabel)
RegistChildComponent(LuaChristmas2023XueHuaXuYuSendGiftWnd, "Input", "Input", UIInput)

--@endregion RegistChildComponent end

function LuaChristmas2023XueHuaXuYuSendGiftWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end

    -- 选择礼物
    UIEventListener.Get(self.IconTexture.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.ShowUI(CLuaUIResources.Christmas2023XueHuaXuYuSelectGiftWnd)
    end)
    -- 许愿
    UIEventListener.Get(self.SendBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnSendBtnClick()
    end)

    self.Highlight = self.transform:Find("Anchor/Gift/GiftInfo/Highlight").gameObject
    LuaChristmas2023Mgr.m_SelectGift = -1 -- 初始化为没有选择礼物
    LuaChristmas2023Mgr.m_IconTexture = self.IconTexture
    LuaChristmas2023Mgr.m_CountLabel = self.CountLabel
    LuaChristmas2023Mgr.m_NoGift = self.NoGift
    LuaChristmas2023Mgr.m_Highlight = self.Highlight
end
-- 许愿按钮按下
function LuaChristmas2023XueHuaXuYuSendGiftWnd:OnSendBtnClick()
    local wishcontent = self.Input.value
    if System.String.IsNullOrEmpty(wishcontent) then
        g_MessageMgr:ShowMessage("Christmas2021_WNXY_Wish_Empty")
        return
    end
    local ret = CWordFilterMgr.Inst:DoFilterOnSend(wishcontent, nil, nil, false)
    if not ret.msg or ret.shouldBeIgnore then
        g_MessageMgr:ShowMessage("ChristmasWish_Violation")
        return
    end

    local posx = LuaChristmas2023Mgr.m_posx
    local posy = LuaChristmas2023Mgr.m_posy
    if LuaChristmas2023Mgr.m_SelectGift < 0 then
        Gac2Gas.RequestPutChristmasCard(wishcontent, 0, 0, posx, posy)
        CUIManager.CloseUI(CLuaUIResources.Christmas2023XueHuaXuYuSendGiftWnd)
        return
    end

    local itemid = LuaChristmas2023Mgr.m_SelectItemId
    local count = LuaChristmas2023Mgr.m_SelectCount
    local money = LuaChristmas2023Mgr.m_SelectPrice
    local msg = ""
    if LuaChristmas2023Mgr.m_SelectGift == LuaChristmas2023Mgr.EnumSelectGift.HuaLi then
        msg = g_MessageMgr:FormatMessage("Christmas_Share_Huali", count, money)
    else
        msg = g_MessageMgr:FormatMessage("Christmas_Share_Xinli", count, money)
    end
    MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function()
        Gac2Gas.RequestPutChristmasCard(wishcontent, itemid, count, posx, posy)
        CUIManager.CloseUI(CLuaUIResources.Christmas2023XueHuaXuYuSendGiftWnd)
    end), nil, nil, nil, false)
end
function LuaChristmas2023XueHuaXuYuSendGiftWnd:Init()
    if CClientMainPlayer.Inst then
        self.NameLabel.text = CClientMainPlayer.Inst.RealName
        self.PlayerImage:LoadNPCPortrait(CClientMainPlayer.Inst.PortraitName, false)
    end

    self.NoGift:SetActive(true)
    self.Highlight:SetActive(false)
    self.IconTexture:LoadMaterial("")
    self.CountLabel.gameObject:SetActive(false)
    self.Input.label.maxLineCount = 5
    self.Input.characterLimit = tonumber(Christmas2023_Setting.GetData().WishTextLimit)

    local madeCount = CCarnivalLotteryMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eChristmas2021CardPutTimes) -- 复用2021
    local limitCount = tonumber(Christmas2023_Setting.GetData().SendGiftTimePreDay)
    self.SendBtn.Enabled = (madeCount < limitCount)
    self.SendBtn.Text = SafeStringFormat3(LocalString.GetString("许愿 (%d/%d)"), madeCount, limitCount)
end
function LuaChristmas2023XueHuaXuYuSendGiftWnd:OnDisable()
    LuaChristmas2023Mgr.m_SelectGift = -1
    LuaChristmas2023Mgr.m_IconTexture = nil
    LuaChristmas2023Mgr.m_CountLabel = nil
    LuaChristmas2023Mgr.m_NoGift = nil
    LuaChristmas2023Mgr.m_Highlight = nil
end

--@region UIEvent

--@endregion UIEvent

