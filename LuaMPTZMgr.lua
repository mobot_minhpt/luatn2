local CPVPMgr = import "L10.Game.CPVPMgr"
local SettlementInfo = import "L10.Game.CPVPMgr+SettlementInfo"
local CUIManager = import "L10.UI.CUIManager"

LuaMPTZMgr = class()

LuaMPTZMgr.SelfRankId = 0
LuaMPTZMgr.SelfFightScore = 0
LuaMPTZMgr.TargetInfoTbl = {}
LuaMPTZMgr.StageRanks = {}

function LuaMPTZMgr.SetSelfAndTargetPlayerInfo(idx, fightScore, targetInfoTbl, lastIdx, lastScore, lastYinPiao, lastExp, avgRankTbl)
	LuaMPTZMgr.SelfRankId = idx
	LuaMPTZMgr.SelfFightScore = fightScore
	LuaMPTZMgr.TargetInfoTbl = targetInfoTbl
	LuaMPTZMgr.StageRanks = avgRankTbl
	CPVPMgr.Inst.m_SettlementInfo = SettlementInfo(lastIdx, lastScore, lastExp, lastYinPiao)
	if lastIdx > 0 then
		CUIManager.ShowUI("MPTZSettlementWnd")
	end
	g_ScriptEvent:BroadcastInLua("OnMPTZQueryPlayerTargetFinish")
end

function LuaMPTZMgr.IsInMPTZ()
	return CPVPMgr.Inst.IsInMPTZ
end
