local UITabBar = import "L10.UI.UITabBar"
local GameObject = import "UnityEngine.GameObject"
local QnTableView = import "L10.UI.QnTableView"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CRankData=import "L10.UI.CRankData"

LuaMusicPlayRankWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaMusicPlayRankWnd, "DifficultyTabBar", "DifficultyTabBar", UITabBar)
RegistChildComponent(LuaMusicPlayRankWnd, "MainPlayerInfo", "MainPlayerInfo", GameObject)
RegistChildComponent(LuaMusicPlayRankWnd, "TableView", "TableView", QnTableView)
RegistChildComponent(LuaMusicPlayRankWnd, "MasterView", "MasterView", QnTableView)
RegistChildComponent(LuaMusicPlayRankWnd, "PlayerNumTabBar", "PlayerNumTabBar", UITabBar)
RegistChildComponent(LuaMusicPlayRankWnd, "EmptyTipLabel", "EmptyTipLabel", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaMusicPlayRankWnd,"m_SelectDifficulty")
RegistClassMember(LuaMusicPlayRankWnd,"m_SelectPlayerNum")
RegistClassMember(LuaMusicPlayRankWnd,"m_MusicList")
RegistClassMember(LuaMusicPlayRankWnd,"m_CurRankPropertyName")
RegistClassMember(LuaMusicPlayRankWnd,"m_SelectMusicRow")
RegistClassMember(LuaMusicPlayRankWnd,"m_RankData")

function LuaMusicPlayRankWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.m_CurRankPropertyName = nil
    self.m_RankLabel = self.MainPlayerInfo.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
    self.m_NameLabel = self.MainPlayerInfo.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    self.m_ScoreLabel = self.MainPlayerInfo.transform:Find("ScoreLabel"):GetComponent(typeof(UILabel))

    self.DifficultyTabBar.OnTabChange = CommonDefs.CombineListner_Action_GameObject_int(self.DifficultyTabBar.OnTabChange, DelegateFactory.Action_GameObject_int(function(go, index)
        self:OnDifficultyTabChange(go, index)
    end), true)
    self.PlayerNumTabBar.OnTabChange = CommonDefs.CombineListner_Action_GameObject_int(self.PlayerNumTabBar.OnTabChange, DelegateFactory.Action_GameObject_int(function(go, index)
        self:OnPlayerNumTabChange(go, index)
    end), true)

    self.m_MusicList = {}
    self.m_MusicCount = 0
    MeiXiangLou_SongInfo.Foreach(function(songId,data)
		local t = {}
		t.SongId = data.SongId
		t.SongIdx = data.SongIdx
		t.Name = data.Name
		t.PassScore = data.PassScore
        t.OperatorNumber = data.OperatorNumber
        t.Difficulty = data.Difficulty

        if not self.m_MusicList[data.SongIdx] then
            self.m_MusicList[data.SongIdx] = {}
            self.m_MusicCount = self.m_MusicCount + 1
        end
        table.insert(self.m_MusicList[data.SongIdx],t)
	end)

	self.MasterView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return self.m_MusicCount
        end,
        function(item,row)
            self:InitMaterItem(item,row)
        end)
    self.MasterView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
			self:OnMusicChange(row)
		end)
    self.MasterView:SetSelectRow(0,true)

    self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_RankData
        end,
        function(item,row)
            self:InitRankItem(item,row)
        end)
end

function LuaMusicPlayRankWnd:OnEnable( ... )
	g_ScriptEvent:AddListener("OnRankDataReady", self, "OnRankDataReady")
end

function LuaMusicPlayRankWnd:OnDisable( ... )
	g_ScriptEvent:RemoveListener("OnRankDataReady", self, "OnRankDataReady")
end
function LuaMusicPlayRankWnd:Init()
    self.m_RankData = {}
    if LuaMeiXiangLouMgr.CurRankSongId and MeiXiangLou_SongInfo.GetData(LuaMeiXiangLouMgr.CurRankSongId) then
        local info = MeiXiangLou_SongInfo.GetData(LuaMeiXiangLouMgr.CurRankSongId)
        local dif = info.Difficulty
        local difIndex = 0
        for i,name in ipairs(LuaMeiXiangLouMgr.m_Difficulty2Name) do
            if dif == name then
                difIndex = i - 1
            end
        end
        self.DifficultyTabBar:ChangeTab(difIndex,false)
        self.PlayerNumTabBar:ChangeTab(info.OperatorNumber-1,false)
        local songIndex = info.SongIdx - 1
        self.MasterView:SetSelectRow(songIndex,true)
    else
        self.DifficultyTabBar:ChangeTab(0,false)
        self.PlayerNumTabBar:ChangeTab(0,false)
    end

    self.MasterView:ReloadData(false,false)
    self:RefreshDataView()
end

function LuaMusicPlayRankWnd:InitMaterItem(item,row)
    local songList = self.m_MusicList[row+1]
    local label = item.transform:Find("Label"):GetComponent(typeof(UILabel))
    if songList then
        local itemInfo
        local curDifName = LuaMeiXiangLouMgr.m_Difficulty2Name[self.m_SelectDifficulty]
        for i,songInfo in ipairs(songList) do
            local difName = songInfo.Difficulty
            if songInfo.OperatorNumber == self.m_SelectPlayerNum and curDifName == difName then
                itemInfo = songInfo
            end
        end
        if itemInfo then
            label.text = itemInfo.Name
        end     
    end    
end

function LuaMusicPlayRankWnd:InitRankItem(item,row)
    local rankLabel = item.transform:Find("Rank"):GetComponent(typeof(UILabel))
    local rankSprite = item.transform:Find("Rank/RankImage"):GetComponent(typeof(UISprite))
    local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local scoreLabel = item.transform:Find("Score"):GetComponent(typeof(UILabel))

    local rankInfo = self.m_RankData[row+1]
    rankSprite.gameObject:SetActive(rankInfo.rank <= 3)
    if rankInfo.rank ==1 then
        rankSprite.spriteName = "Rank_No.1"
    elseif rankInfo.rank <= 3 and rankInfo.rank > 1 then
        rankSprite.spriteName = SafeStringFormat3("Rank_No.%spng",rankInfo.rank)
    else
        rankSprite.spriteName = nil
    end
    rankLabel.text = rankInfo.rank
    nameLabel.text = rankInfo.name
    
    local score = rankInfo.score
    if self.m_SelectPlayerNum == 2 then
        if score % 2 ~= 0 then
            score = score -1
        end
        score = score / 10
    end
    scoreLabel.text = score
    --
    local bgSprite = item.transform:GetComponent(typeof(UISprite))
    local sname = row % 2 == 0 and "n" or "s"
    bgSprite.spriteName = SafeStringFormat3("common_bg_mission_background_%s",sname)
end

function LuaMusicPlayRankWnd:OnMusicChange(row)
    local songList = self.m_MusicList[row+1]
    if songList then
        local itemInfo
        local curDifName = LuaMeiXiangLouMgr.m_Difficulty2Name[self.m_SelectDifficulty]
        for i,songInfo in ipairs(songList) do
            local difName = songInfo.Difficulty
            if songInfo.OperatorNumber == self.m_SelectPlayerNum and curDifName == difName then
                itemInfo = songInfo
            end
        end
        if itemInfo then
            self.m_SelectSongIdx = itemInfo.SongIdx
        else
            self.m_SelectSongIdx = nil
        end
    else
        self.m_SelectSongIdx = nil  
    end

    self:RefreshRankPropertyName()
end

function LuaMusicPlayRankWnd:OnDifficultyTabChange(go,index)
    self.m_SelectDifficulty = index + 1
    self:RefreshRankPropertyName()
end

function LuaMusicPlayRankWnd:OnPlayerNumTabChange(go,index)
    self.m_SelectPlayerNum = index + 1
    self:RefreshRankPropertyName()
end

function LuaMusicPlayRankWnd:RefreshRankPropertyName()
    if not self.m_SelectSongIdx then
        return
    end

    self.m_CurRankProperty = SafeStringFormat3("MeiXiangLouSong%d%d%d",self.m_SelectSongIdx,self.m_SelectPlayerNum,self.m_SelectDifficulty)
    local rankId = LuaMeiXiangLouMgr.m_RankNameToId[self.m_CurRankProperty]

    if rankId then        
        Gac2Gas.QueryRank(rankId)
    end
end

function LuaMusicPlayRankWnd:RefreshDataView()
    self.TableView:ReloadData(false,false)
    if CClientMainPlayer.Inst then
        self.m_NameLabel.text = CClientMainPlayer.Inst.RealName
    else
        self.m_NameLabel.text = nil
    end
    
    local myRankInfo = CRankData.Inst.MainPlayerRankInfo
    if not myRankInfo then
        self.m_RankLabel.text = LocalString.GetString("未上榜")
        self.m_ScoreLabel.text = nil
    else
        self.m_RankLabel.text = myRankInfo.Rank>0 and myRankInfo.Rank or LocalString.GetString("未上榜")
        local myScore = myRankInfo.Value
        if self.m_SelectPlayerNum == 2 then
            if myScore % 2 ~= 0 then
                myScore = myScore -1
            end
            myScore = myScore / 10
        end
        self.m_ScoreLabel.text = myScore
    end
end

function LuaMusicPlayRankWnd:OnRankDataReady( ... )
    if CLuaRankData.m_CurRankId ~= LuaMeiXiangLouMgr.m_RankNameToId[self.m_CurRankProperty] then return end
	self.m_RankData = {}

    local myRankInfo = CRankData.Inst.MainPlayerRankInfo
	if CRankData.Inst.RankList and CRankData.Inst.RankList.Count > 0 then
		do
			local i = 0
			while i < CRankData.Inst.RankList.Count do
				local info = CRankData.Inst.RankList[i]
        		local extraList = MsgPackImpl.unpack(info.extraData)
				local data = {name = info.Name,rank = info.Rank,score = info.Value,job = info.Job}

                table.insert(self.m_RankData,data)
				i = i + 1
			end
			if i == 0 then
				self.EmptyTipLabel.gameObject:SetActive(true)
			else
				self.EmptyTipLabel.gameObject:SetActive(false)
			end
		end
    else
    	self.EmptyTipLabel.gameObject:SetActive(true)
	end

	self:RefreshDataView()
end

--@region UIEvent

--@endregion UIEvent

