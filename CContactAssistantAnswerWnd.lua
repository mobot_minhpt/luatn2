-- Auto Generated!!
local AssistantMessage = import "L10.Game.AssistantMessage"
local AssistantMessageType = import "L10.Game.AssistantMessageType"
local CContactAssistantAnswerWnd = import "L10.UI.CContactAssistantAnswerWnd"
local CContactAssistantMgr = import "L10.Game.CContactAssistantMgr"
local CContactAssistantWnd = import "L10.UI.CContactAssistantWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local DateTime = import "System.DateTime"
local DelegateFactory = import "DelegateFactory"
local L10 = import "L10"
local Object = import "System.Object"
local String = import "System.String"
local TimeSpan = import "System.TimeSpan"
local UIEventListener = import "UIEventListener"
CContactAssistantAnswerWnd.m_Awake_CS2LuaHook = function (this) 
    if CContactAssistantAnswerWnd.Instance ~= nil then
        L10.CLogMgr.LogError("more than one instance running!")
    end
    CContactAssistantAnswerWnd.Instance = this
end
CContactAssistantAnswerWnd.m_setInitState_CS2LuaHook = function (this, sign, _list) 
    this.answerList = _list
    this:InitReplyData()

    this.replyView.gameObject:SetActive(true)
    this.assistantReplyView:Init()
    this.replyView:init()
    this:updateAssistantReplyView()

    UIEventListener.Get(this.checkAnswerStateBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        this:CheckState(_list)
    end)
end
CContactAssistantAnswerWnd.m_CheckState_CS2LuaHook = function (this, list) 
    if list == nil or list.Count <= 0 then
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", CContactAssistantWnd.NoStateAlertString)
        return
    end

    local jsonDict = TypeAs(list[0], typeof(MakeGenericClass(Dictionary, String, Object)))
    local qid = System.UInt64.Parse(ToStringWrap(CommonDefs.DictGetValue(jsonDict, typeof(String), "qid")))
    CContactAssistantMgr.Inst.checkStateQid = qid
    local content = TypeAs(CommonDefs.DictGetValue(jsonDict, typeof(String), "content"), typeof(MakeGenericClass(Dictionary, String, Object)))
    local qtitle = ToStringWrap((TypeAs(CommonDefs.DictGetValue(content, typeof(String), "qtitle"), typeof(MakeGenericClass(List, Object))))[0])
    local remark = ToStringWrap((TypeAs(CommonDefs.DictGetValue(content, typeof(String), "remark"), typeof(MakeGenericClass(List, Object))))[0])

    qtitle = CContactAssistantMgr.Inst:FilterTitlePrefix(qtitle)
    remark = CContactAssistantMgr.Inst:FilterContentExtraInfo(remark)
    local showText = (("[" .. qtitle) .. "]") .. remark
    CContactAssistantMgr.Inst.checkStateInfo = showText
    CUIManager.ShowUI(CUIResources.ContactAssistantCheckWnd)
end
CContactAssistantAnswerWnd.m_Init_CS2LuaHook = function (this) 
    CUIManager.HideUIByChangeLayer(CUIResources.JingLingWnd)

    Gac2Gas.GetQuestionsByUrs()
end
CContactAssistantAnswerWnd.m_OnCloseButtonClick_CS2LuaHook = function (this, go) 
    CUIManager.ShowUIByChangeLayer(CUIResources.JingLingWnd)
    this:Close()
end
CContactAssistantAnswerWnd.m_InitReplyData_CS2LuaHook = function (this) 
    local answerState = false

    CommonDefs.ListClear(this.assistantMessageList)
    local sysTime = CServerTimeMgr.Inst:GetZone8Time()
    if this.answerList == nil then
        return answerState
    end

    do
        local i = 0
        while i < this.answerList.Count do
            local jsonDict = TypeAs(this.answerList[i], typeof(MakeGenericClass(Dictionary, String, Object)))

            local am = CreateFromClass(AssistantMessage)
            am.msgType = AssistantMessageType.Question
            am.info = jsonDict
            CommonDefs.ListAdd(this.assistantMessageList, typeof(AssistantMessage), am)

            local replynum = 0
            if CommonDefs.DictContains(jsonDict, typeof(String), "replynum") then
                replynum = System.Int32.Parse(ToStringWrap(CommonDefs.DictGetValue(jsonDict, typeof(String), "replynum")))
            end
            if replynum > 0 and CommonDefs.DictContains(jsonDict, typeof(String), "last_answer") then
                local bm = CreateFromClass(AssistantMessage)
                bm.msgType = AssistantMessageType.Answer
                bm.info = jsonDict
                CommonDefs.ListAdd(this.assistantMessageList, typeof(AssistantMessage), bm)
            else
            end

            local questionTime = DateTime.Parse(ToStringWrap(CommonDefs.DictGetValue(jsonDict, typeof(String), "qtime")))
            if CommonDefs.DictContains(jsonDict, typeof(String), "eval_aid") then
            else
                local ts = (CreateFromClass(TimeSpan, sysTime.Ticks)):Subtract(CreateFromClass(TimeSpan, questionTime.Ticks)):Duration()

                if ts.Days <= CContactAssistantAnswerWnd.WaitDay then
                    answerState = true
                end
            end
            i = i + 1
        end
    end

    return answerState
end
