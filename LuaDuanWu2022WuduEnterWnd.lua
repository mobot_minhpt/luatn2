local UITable = import "UITable"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local UILabel = import "UILabel"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType2 = import "L10.UI.CItemInfoMgr+AlignType"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local CTipParagraphItem = import "L10.UI.CTipParagraphItem"
local Extensions = import "Extensions"
local CCarnivalLotteryMgr = import "L10.Game.CCarnivalLotteryMgr"

LuaDuanWu2022WuduEnterWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaDuanWu2022WuduEnterWnd, "RankBtn", "RankBtn", GameObject)
RegistChildComponent(LuaDuanWu2022WuduEnterWnd, "TextTable", "TextTable", UITable)
RegistChildComponent(LuaDuanWu2022WuduEnterWnd, "TextTemplate", "TextTemplate", GameObject)
RegistChildComponent(LuaDuanWu2022WuduEnterWnd, "RewardInfo", "RewardInfo", GameObject)
RegistChildComponent(LuaDuanWu2022WuduEnterWnd, "ScrollView", "ScrollView", CUIRestrictScrollView)
RegistChildComponent(LuaDuanWu2022WuduEnterWnd, "ChooseBtn", "ChooseBtn", GameObject)
RegistChildComponent(LuaDuanWu2022WuduEnterWnd, "RecordLabel", "RecordLabel", UILabel)
RegistChildComponent(LuaDuanWu2022WuduEnterWnd, "RankLabel", "RankLabel", UILabel)
RegistChildComponent(LuaDuanWu2022WuduEnterWnd, "TimeLabel", "TimeLabel", UILabel)
RegistChildComponent(LuaDuanWu2022WuduEnterWnd, "DailyReward", "DailyReward", GameObject)
RegistChildComponent(LuaDuanWu2022WuduEnterWnd, "FirstReward", "FirstReward", GameObject)

--@endregion RegistChildComponent end

function LuaDuanWu2022WuduEnterWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.RankBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRankBtnClick()
	end)


	
	UIEventListener.Get(self.ChooseBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnChooseBtnClick()
	end)


    --@endregion EventBind end
end

function LuaDuanWu2022WuduEnterWnd:Init()
    self:InitTopItem()
    self:InitTextTable()

    local rankType = LuaDuanWu2022Mgr:GetPlayerRankType()
    local seasonId = 1001
    Gac2Gas.SeasonRank_QueryRank(rankType, seasonId)
end

function LuaDuanWu2022WuduEnterWnd:InitTextTable()
    -- 配表时间
    local setting = DuanWu_Setting.GetData()
    self.TimeLabel.text = setting.QuWuDuOpenTimeDescription

    self.TextTemplate:SetActive(false)
    Extensions.RemoveAllChildren(self.TextTable.transform)
    local msg = g_MessageMgr:FormatMessage("DuanWu_2022_WuDu_Introduction")
    
    local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(msg)
    if info == nil then
        return
    end
    for i = 0, info.paragraphs.Count - 1 do
        local paragraphGo = NGUITools.AddChild(self.TextTable.gameObject, self.TextTemplate)
        paragraphGo:SetActive(true)
        CommonDefs.GetComponent_GameObject_Type(paragraphGo, typeof(CTipParagraphItem))
                :Init(info.paragraphs[i], 4294967295)
    end
    self.ScrollView:ResetPosition()
    self.TextTable:Reposition()
end

function LuaDuanWu2022WuduEnterWnd:InitTopItem()
    local setting = DuanWu_Setting.GetData()
    local dailyItem = setting.QuWuDuDailyRewardItemId
    local passItem = setting.QuWuDuClearRewardItemId

    local dailyPlayTime = CCarnivalLotteryMgr.Inst:GetGamePlayTempData(LuaDuanWu2022Mgr.EnumDuanWuPlayTime.eDuanWu2022PVE)
    local passPlayTime = CCarnivalLotteryMgr.Inst:GetGamePlayTempData(LuaDuanWu2022Mgr.EnumDuanWuPlayTime.eDuanWu2022PV_PassTime)

    self:InitItem(self.DailyReward, dailyPlayTime>0, dailyItem)
    self:InitItem(self.FirstReward, passPlayTime>0, passItem)
end

function LuaDuanWu2022WuduEnterWnd:InitItem(item, hasReward, itemId)
    local icon = item.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    local hasRewardItem = item.transform:Find("HasReward").gameObject

    hasRewardItem:SetActive(hasReward)

    local itemData = Item_Item.GetData(itemId)
    icon:LoadMaterial(itemData.Icon)

    UIEventListener.Get(icon.gameObject).onClick = DelegateFactory.VoidDelegate(function ()
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType2.Default, 0, 0, 0, 0)
    end)
end

function LuaDuanWu2022WuduEnterWnd:OnData(rankType, seasonId, selfDataU, rankDataU)
    self.MyData = LuaDuanWu2022Mgr:TransformRankData(selfDataU, rankDataU)

    if self.MyData.rank == nil or self.MyData.rank == 0 then
        self.RankLabel.text = LocalString.GetString("未上榜")
    else
        self.RankLabel.text = tostring(self.MyData.rank)
    end

    if self.MyData.Score == nil then
        self.RecordLabel.text = "0"
    else
        self.RecordLabel.text = tostring(self.MyData.Score)
    end
end

function LuaDuanWu2022WuduEnterWnd:OnEnable()
    g_ScriptEvent:AddListener("SeasonRank_QueryRankResult", self, "OnData")
end

function LuaDuanWu2022WuduEnterWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SeasonRank_QueryRankResult", self, "OnData")
end


--@region UIEvent

function LuaDuanWu2022WuduEnterWnd:OnRankBtnClick()
    CUIManager.ShowUI(CLuaUIResources.DuanWu2022WuduRankWnd)
end

function LuaDuanWu2022WuduEnterWnd:OnChooseBtnClick()
    LuaDuanWu2022Mgr:OpenChooseWnd(self.MyData)
end


--@endregion UIEvent

