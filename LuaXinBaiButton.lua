local DelegateFactory = import "DelegateFactory"

LuaXinBaiButton = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaXinBaiButton, "progress")
RegistClassMember(LuaXinBaiButton, "icon")
RegistClassMember(LuaXinBaiButton, "alert")

function LuaXinBaiButton:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.progress = self.transform:Find("Progress"):GetComponent(typeof(UITexture))
    self.icon = self.transform:Find("Icon"):GetComponent(typeof(CUITexture))
    self.alert = self.transform:Find("Alert").gameObject
    self.alert:SetActive(false)

    UIEventListener.Get(self.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnClick()
    end)
end

function LuaXinBaiButton:Init()
    -- 计算当前正在进行的任务以及解锁进度
    local tbl = LuaXinBaiLianDongMgr.ProgressInfo.progressTbl or {}
    local totalCount = XinBaiLianDong_Progress.GetDataCount()

    local beforeIsFinished = true
    local curId = 1
    local curProgress = 0
    local isAlertVisible = false
    for i = 1, totalCount do
        local progress = tbl[i] or 0
        local maxProgress = XinBaiLianDong_Progress.GetData(i).MaxProgress

        if beforeIsFinished and not isAlertVisible then
            isAlertVisible = self:IsTaskCanAccept(i)
        end

        if progress < maxProgress then
            if beforeIsFinished then
                curId = i
                curProgress = math.min(progress / maxProgress, 1)
                break
            end
            beforeIsFinished = false
        else
            beforeIsFinished = true

            if i == totalCount then
                curId = i
                curProgress = 1
            end
        end
    end
    self.alert:SetActive(isAlertVisible)

    self.icon:LoadMaterial(XinBaiLianDong_Progress.GetData(curId).Icon)
    self.progress.fillAmount = curProgress
end

-- 某个节点的任务是否可接
function LuaXinBaiButton:IsTaskCanAccept(i)
    return not self:IsFirstTaskAccepted(i) and self:IsPreTaskFinished(i) and
        self:CheckTaskAcceptGrade(self:GetFirstTaskId(i)) and self:CheckTaskStatus(self:GetFirstTaskId(i))
end

-- 前置任务是否完成
function LuaXinBaiButton:IsPreTaskFinished(progressId)
    local preCheckTaskId = XinBaiLianDong_Progress.GetData(progressId).PreCheckTaskId
    if preCheckTaskId > 0 then
        if not CClientMainPlayer.Inst.TaskProp:IsMainPlayerTaskFinished(preCheckTaskId) then
            return false
        end
    end

    return true
end

-- 获取第一个任务
function LuaXinBaiButton:GetFirstTaskId(progressId)
    for taskId in string.gmatch(XinBaiLianDong_Progress.GetData(progressId).TaskIds, "(%d+)") do
        return tonumber(taskId)
    end
    return 0
end

-- 第一个任务正在进行中或者已完成
function LuaXinBaiButton:IsFirstTaskAccepted(progressId)
    local taskProp = CClientMainPlayer.Inst.TaskProp
    local taskId = self:GetFirstTaskId(progressId)
    if CommonDefs.ListContains_LuaCall(taskProp.CurrentTaskList, taskId) or taskProp:IsMainPlayerTaskFinished(taskId) then
        return true
    else
        return false
    end
end

-- 是否达到任务接受等级
function LuaXinBaiButton:CheckTaskAcceptGrade(taskId)
    local data = Task_Task.GetData(taskId)

    local minGrade = CClientMainPlayer.Inst.HasFeiSheng and data.GradeCheckFS1[0] or data.GradeCheck[0]
    if CClientMainPlayer.Inst.Level < minGrade then
        return false, minGrade
    else
        return true
    end
end

-- 检查任务状态
function LuaXinBaiButton:CheckTaskStatus(taskId)
    local statusTbl = LuaXinBaiLianDongMgr.ProgressInfo.statusTbl
    if not statusTbl or not statusTbl[taskId] then return false end

    return statusTbl[taskId] == 0
end

--@region UIEvent

function LuaXinBaiButton:OnClick()
    CLuaScheduleMgr.taskTabType = XinBaiLianDong_Setting.GetData().XinBaiProgress_TabType
    CUIManager.ShowUI(CLuaUIResources.ScheduleWnd)
end

--@endregion UIEvent
