local DelegateFactory  = import "DelegateFactory"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"

LuaChangeZongMenHongBaoItemWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaChangeZongMenHongBaoItemWnd, "QnIncreseAndDecreaseButton", "QnIncreseAndDecreaseButton", QnAddSubAndInputButton)
RegistChildComponent(LuaChangeZongMenHongBaoItemWnd, "CostLabel", "CostLabel", UILabel)
RegistChildComponent(LuaChangeZongMenHongBaoItemWnd, "Button", "Button", GameObject)
RegistChildComponent(LuaChangeZongMenHongBaoItemWnd, "DeleteButton", "DeleteButton", GameObject)
RegistChildComponent(LuaChangeZongMenHongBaoItemWnd, "Anchor", "Anchor", GameObject)

--@endregion RegistChildComponent end

function LuaChangeZongMenHongBaoItemWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.Button.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnButtonClick()
	end)


	
	UIEventListener.Get(self.DeleteButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnDeleteButtonClick()
	end)


    --@endregion EventBind end
end

function LuaChangeZongMenHongBaoItemWnd:Init()
	self.CostLabel.text = 0
	local data = LuaZongMenMgr.m_HongBaoItemList[LuaZongMenMgr.m_CurSelectHongBaoItemIndex]
	if data then
		self.QnIncreseAndDecreaseButton:SetValue(data.num, true)
		self.CostLabel.text = data.price
	end
	self.QnIncreseAndDecreaseButton.onValueChanged = DelegateFactory.Action_uint(function(v)
		self:OnNumChanged(v)
    end)
	self.Anchor.transform.position = LuaZongMenMgr.m_CurSelectHongBaoItemPos
	local itemData = Item_Item.GetData(data.itemId)
	local maxCount = 999
	if itemData then
		maxCount = math.min(maxCount,itemData.Overlap)
	end
	local limitPurchaseQuantityItemIds = g_LuaUtil:StrSplit(Mall_Setting.GetData("LimitPurchaseQuantityItemIds").Value,";")
	for _,itemId in pairs(limitPurchaseQuantityItemIds) do
		if itemId == tostring(data.itemId) then
			maxCount = 1
		end
	end
	local mallData = Mall_LingYuMall.GetData(data.itemId)
	if mallData  then
		if mallData.SubCategory == LuaShopMallFashionPreviewMgr.EnumLingYuMallSubCategory.ShiZhuang or mallData.SubCategory == LuaShopMallFashionPreviewMgr.EnumLingYuMallSubCategory.ZuoQi then
			maxCount = 1
		end
	end
	self.QnIncreseAndDecreaseButton:SetMinMax(1, maxCount, 1)
	self.QnIncreseAndDecreaseButton:SetValue(data.num, false)
end

function LuaChangeZongMenHongBaoItemWnd:OnNumChanged(v)
	local data = LuaZongMenMgr.m_HongBaoItemList[LuaZongMenMgr.m_CurSelectHongBaoItemIndex]
	if data then
		local mallData = Mall_LingYuMall.GetData(data.itemId)
		if mallData then
			local num = self.QnIncreseAndDecreaseButton:GetValue()
			local price = num * mallData.Jade
			LuaZongMenMgr.m_HongBaoItemList[LuaZongMenMgr.m_CurSelectHongBaoItemIndex] = {
				itemId = data.itemId,
				num = num,
				price = price,
			}
			self.CostLabel.text = price
			g_ScriptEvent:BroadcastInLua("SetSectHongBaoItem")
		end
	end
end

--@region UIEvent

function LuaChangeZongMenHongBaoItemWnd:OnButtonClick()
	CUIManager.CloseUI(CLuaUIResources.ChangeZongMenHongBaoItemWnd)
	CUIManager.ShowUI(CLuaUIResources.SectHongBaoItemSelectWnd)
end

function LuaChangeZongMenHongBaoItemWnd:OnDeleteButtonClick()
	CUIManager.CloseUI(CLuaUIResources.ChangeZongMenHongBaoItemWnd)
	LuaZongMenMgr.m_HongBaoItemList[LuaZongMenMgr.m_CurSelectHongBaoItemIndex] = nil
	for i = LuaZongMenMgr.m_CurSelectHongBaoItemIndex, 11 do
		LuaZongMenMgr.m_HongBaoItemList[i] = LuaZongMenMgr.m_HongBaoItemList[i + 1]
	end
	g_ScriptEvent:BroadcastInLua("SetSectHongBaoItem", true)
end


--@endregion UIEvent

