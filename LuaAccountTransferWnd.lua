local UIPanel = import "UIPanel"
local CButton = import "L10.UI.CButton"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local CTipParagraphItem = import "L10.UI.CTipParagraphItem"
local BoxCollider = import "UnityEngine.BoxCollider"

LuaAccountTransferWnd = class()

RegistChildComponent(LuaAccountTransferWnd, "m_MessageView","MessageView", GameObject)
RegistChildComponent(LuaAccountTransferWnd, "m_MessageViewPanel","MessageViewPanel", UIPanel)
RegistChildComponent(LuaAccountTransferWnd, "m_ExplanationView","ExplanationView", GameObject)
RegistChildComponent(LuaAccountTransferWnd, "m_NextStepButton","NextStepButton", BoxCollider)
RegistChildComponent(LuaAccountTransferWnd, "m_StandardTemplate","StandardTemplate", GameObject)
RegistChildComponent(LuaAccountTransferWnd, "m_Grid","Grid", UIGrid)
RegistChildComponent(LuaAccountTransferWnd, "m_Table","Table", UITable)
RegistChildComponent(LuaAccountTransferWnd, "m_ConfirmButton","ConfirmButton", GameObject)
RegistChildComponent(LuaAccountTransferWnd, "m_ScrollView","ScrollView", UIScrollView)
RegistChildComponent(LuaAccountTransferWnd, "m_ExplanationTemplate","ExplanationTemplate", GameObject)
RegistChildComponent(LuaAccountTransferWnd, "m_InputText","InputText", UIInput)
RegistChildComponent(LuaAccountTransferWnd, "m_Remind","Remind", UILabel)

RegistClassMember(LuaAccountTransferWnd, "m_StandardList")
RegistClassMember(LuaAccountTransferWnd, "m_ReviewResults")
RegistClassMember(LuaAccountTransferWnd, "m_UpdateMessageViewTip")
RegistClassMember(LuaAccountTransferWnd, "m_ReviewStandardIndex")

function LuaAccountTransferWnd:Init()
    self.m_MessageView.gameObject:SetActive(false)
    self.m_ExplanationView:SetActive(false)
    self.m_StandardTemplate:SetActive(false)
    self.m_ExplanationTemplate:SetActive(false)
    self:InitMessageView()
end

function LuaAccountTransferWnd:OnEnable()
    g_ScriptEvent:AddListener("AccountTransferInfoGet",self,"OnAccountTransferInfoGet")
end

function LuaAccountTransferWnd:OnDisable()
    g_ScriptEvent:RemoveListener("AccountTransferInfoGet",self,"OnAccountTransferInfoGet")
    self:CancelUpdateMessageViewTip()
end

function LuaAccountTransferWnd:InitMessageView()
    self.m_MessageViewPanel.alpha = 0.5
    Extensions.SetLocalPositionZ(self.m_NextStepButton.transform, -1)
    self.m_NextStepButton.enabled = false
    UIEventListener.Get(self.m_NextStepButton.gameObject).onClick = DelegateFactory.VoidDelegate(function()
        self:OnNextStepButtonClick()
    end)

    self.m_StandardList = {}
    for i = 1,5 do
        local obj = NGUITools.AddChild(self.m_Grid.gameObject, self.m_StandardTemplate)
        local t = {}
        t.label = obj.transform:Find("Label"):GetComponent(typeof(UILabel))
        t.trueSprite = obj.transform:Find("TrueSprite").gameObject
        t.trueSprite:SetActive(false)
        t.falseSprite = obj.transform:Find("FalseSprite").gameObject
        t.falseSprite:SetActive(false)
        t.button = obj.transform:Find("Button"):GetComponent(typeof(CButton))
        t.button.gameObject:SetActive(false)
        t.auditLabel = obj.transform:Find("AuditLabel").gameObject
        t.auditLabel.gameObject:SetActive(false)
        t.label.text = CUICommonDef.TranslateToNGUIText(AccountTransfer_Condition.GetData(i).Value)
        table.insert(self.m_StandardList, t)
        obj:SetActive(true)
    end
    self.m_Grid:Reposition()

    self.m_ReviewStandardIndex = 1
    self.m_StandardList[self.m_ReviewStandardIndex].auditLabel.gameObject:SetActive(true)
    self.m_MessageView.gameObject:SetActive(true)

    Gac2Gas.DetectAccountTransferCondition()
end

function LuaAccountTransferWnd:InitExplanationView()
    g_ScriptEvent:RemoveListener("AccountTransferInfoGet",self,"OnAccountTransferInfoGet")
    UIEventListener.Get(self.m_ConfirmButton).onClick = DelegateFactory.VoidDelegate(function()
        self:OnConfirmButtonClick()
    end)

    local msg = g_MessageMgr:FormatMessage("AccountTransfer_Readme")
    local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(msg)
    for i = 0, info.paragraphs.Count - 1 do
        local paragraphGo = NGUITools.AddChild(self.m_Table.gameObject, self.m_ExplanationTemplate)
        paragraphGo:SetActive(true)
        CommonDefs.GetComponent_GameObject_Type(paragraphGo, typeof(CTipParagraphItem))
                  :Init(info.paragraphs[i], 4294967295)
    end
    self.m_ScrollView:ResetPosition()
    self.m_Table:Reposition()
    self.m_Remind.text = AccountTransfer_Setting.GetData().ConfirmText
    self.m_ExplanationView:SetActive(true)
end

function LuaAccountTransferWnd:UpdateNextStepButton()
    for i = 1,#self.m_ReviewResults do
        if not self.m_ReviewResults[i].status then return end
    end
    Extensions.SetLocalPositionZ(self.m_NextStepButton.transform, 0)
    self.m_NextStepButton.enabled = true
end

function LuaAccountTransferWnd:OnAccountTransferInfoGet(reviewResults)
    self.m_MessageViewPanel.alpha = 1
    self.m_ReviewResults = reviewResults
    self:CancelUpdateMessageViewTip()
    self.m_ReviewStandardIndex = 1
    self.m_StandardList[self.m_ReviewStandardIndex].auditLabel.gameObject:SetActive(true)
    self.m_UpdateMessageViewTip = RegisterTick(function ()
        local t = self.m_StandardList[self.m_ReviewStandardIndex]
        local data = self.m_ReviewResults[self.m_ReviewStandardIndex]
        if not data then return end
        t.trueSprite:SetActive(data.status)
        t.falseSprite:SetActive(not data.status)
        if data.status == false then
            t.label.color = Color.red
            t.label.text = g_MessageMgr:FormatMessage(SafeStringFormat3("AccountTransfer_Condition_%d",self.m_ReviewStandardIndex))
        end
        if (data.status == false) and (self.m_ReviewStandardIndex < 3) then
            t.button.gameObject:SetActive(true)
            local index = self.m_ReviewStandardIndex
            UIEventListener.Get(t.button.gameObject).onClick = DelegateFactory.VoidDelegate(function()
                self:OnStandardButtonClick(index)
            end)
        end
        t.auditLabel.gameObject:SetActive(false)
        self.m_ReviewStandardIndex = self.m_ReviewStandardIndex + 1
        if self.m_ReviewStandardIndex <= #self.m_StandardList then
            self.m_StandardList[self.m_ReviewStandardIndex].auditLabel.gameObject:SetActive(true)
        else
            self:CancelUpdateMessageViewTip()
            self:UpdateNextStepButton()
        end
    end,1000)
end

function LuaAccountTransferWnd:OnStandardButtonClick(index)
    if index == 1 then
        Gac2Gas.QueryAccoutBindMobleInfo()
    elseif index == 2 then
        CUIManager.ShowUI(CUIResources.SecondPwdConfirmWnd)
    end
    CUIManager.CloseUI(CLuaUIResources.AccountTransferWnd)
end

function LuaAccountTransferWnd:OnNextStepButtonClick()
    self.m_MessageView.gameObject:SetActive(false)
    self:InitExplanationView()
end

function LuaAccountTransferWnd:OnConfirmButtonClick()
    local text = self.m_InputText.text
    if text ~= AccountTransfer_Setting.GetData().ConfirmText then
        g_MessageMgr:ShowMessage("AccountTransfer_ConfirmTextError")
        return
    end
    local msg = g_MessageMgr:FormatMessage("AccountTransfer_Confirm",AccountTransfer_Setting.GetData().LingYuCost)
    MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
        Gac2Gas.ConfirmAccountTransfer()
    end),nil,LocalString.GetString("确定"),LocalString.GetString("取消"),false)
end

function LuaAccountTransferWnd:CancelUpdateMessageViewTip()
    if self.m_UpdateMessageViewTip then
        UnRegisterTick(self.m_UpdateMessageViewTip)
        self.m_UpdateMessageViewTip = nil
    end
end

--LuaAccountTransferMgr
--Gas2Gac.SyncAccountTransferCondition 账号迁移检查条件
--Gas2Gac.SyncAccountBindMobile 查询账号的绑定手机信息
--Gas2Gac.ShowClassTransferSuccessDelayTip 迁移成功，提醒需要弹提示消息