require("3rdParty/ScriptEvent")
require("common/common_include")

local CButton = import "L10.UI.CButton"
local UITabBar = import "L10.UI.UITabBar"
local UITable = import "UITable"
local Extensions = import "Extensions"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local CTipParagraphItem = import "L10.UI.CTipParagraphItem"
local CTipTitleItem = import "CTipTitleItem"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local UIScrollViewIndicator = import "UIScrollViewIndicator"
local ChiJi_Setting = import "L10.Game.ChiJi_Setting"
local CRankData = import "L10.UI.CRankData"
local NGUITools = import "NGUITools"
local Profession = import "L10.Game.Profession"
local Constants = import "L10.Game.Constants"
local QnTableItem = import "L10.UI.QnTableItem"
local CPlayerInfoMgr=import "CPlayerInfoMgr"
local EnumPlayerInfoContext=import "L10.Game.EnumPlayerInfoContext"
local EChatPanel=import "L10.Game.EChatPanel"
local AlignType=import "CPlayerInfoMgr+AlignType"

LuaPUBGStartWnd = class()

RegistChildComponent(LuaPUBGStartWnd, "TabBar", UITabBar)

RegistChildComponent(LuaPUBGStartWnd, "PUBGRuleView", GameObject)
RegistChildComponent(LuaPUBGStartWnd, "Table", UITable)
RegistChildComponent(LuaPUBGStartWnd, "ContentScrollView", CUIRestrictScrollView)
RegistChildComponent(LuaPUBGStartWnd, "TitleTemplate", GameObject)
RegistChildComponent(LuaPUBGStartWnd, "ParagraphTemplate", GameObject)
RegistChildComponent(LuaPUBGStartWnd, "Indicator", UIScrollViewIndicator)

RegistChildComponent(LuaPUBGStartWnd, "PUBGRankView", GameObject)
RegistChildComponent(LuaPUBGStartWnd, "RankScrollView", CUIRestrictScrollView)
RegistChildComponent(LuaPUBGStartWnd, "RankGrid", UIGrid)
RegistChildComponent(LuaPUBGStartWnd, "ItemTemplate", GameObject)

RegistChildComponent(LuaPUBGStartWnd, "JoinButton", CButton)
RegistChildComponent(LuaPUBGStartWnd, "WaitingTeamLabel", UILabel)


RegistClassMember(LuaPUBGStartWnd, "SelectTabIndex")
RegistClassMember(LuaPUBGStartWnd, "RankItemList")
RegistClassMember(LuaPUBGStartWnd, "SelectedTankIndex")

function LuaPUBGStartWnd:Awake()
    self.SelectTabIndex = 0
    self.SelectedTankIndex = 0
end

function LuaPUBGStartWnd:Init()

    self:ParseRule()


    self.TabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
		self:OnTabChange(index)
    end)
    self.TabBar:ChangeTab(self.SelectTabIndex, true)
    self:OnTabChange(self.SelectTabIndex)

    CommonDefs.AddOnClickListener(self.JoinButton.gameObject, DelegateFactory.Action_GameObject(function (go)
        self:OnJoinButtonClicked(go)
    end), false)

    self:ProcessApplyStatus()
end

function LuaPUBGStartWnd:ParseRule()
    local msg = g_MessageMgr:FormatMessage("ZhouNianQing_PUBG_Rule")

    Extensions.RemoveAllChildren(self.Table.transform)

    if System.String.IsNullOrEmpty(msg) then
        return
    end

    local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(msg)

    if info == nil then
        return
    end

    if info.titleVisible then
        local titleGo = CUICommonDef.AddChild(self.Table.gameObject, self.TitleTemplate)
        titleGo:SetActive(true)
        CommonDefs.GetComponent_GameObject_Type(titleGo, typeof(CTipTitleItem)):Init(info.title)
    end

    do
        local i = 0
        while i < info.paragraphs.Count do
            local paragraphGo = CUICommonDef.AddChild(self.Table.gameObject, self.ParagraphTemplate)
            paragraphGo:SetActive(true)
            CommonDefs.GetComponent_GameObject_Type(paragraphGo, typeof(CTipParagraphItem)):Init(info.paragraphs[i], 4294967295)
            i = i + 1
        end
    end

    self.ContentScrollView:ResetPosition()
    self.Indicator:Layout()
    self.Table:Reposition()
end

function LuaPUBGStartWnd:ProcessApplyStatus()
    if LuaPUBGMgr.m_IsApplied then
        self.JoinButton.Enabled = true
        self.JoinButton.Text = LocalString.GetString("取消报名")
    else
        self.JoinButton.Enabled = true
        self.JoinButton.Text = LocalString.GetString("报名参与")

    end
end

function LuaPUBGStartWnd:OnTabChange(index)
    self.SelectTabIndex = index
    if index == 0 then
        self.PUBGRuleView:SetActive(true)
        self.PUBGRankView:SetActive(false)
    elseif index == 1 then
        self.PUBGRuleView:SetActive(false)
        self.PUBGRankView:SetActive(true)
        local setting = ChiJi_Setting.GetData()
        Gac2Gas.QueryRank(setting.ScoreRankId)
    end
    self.WaitingTeamLabel.text = ""
end


function LuaPUBGStartWnd:OnRankDataReady()
    self.ItemTemplate:SetActive(false)

    self.RankItemList = {}
    Extensions.RemoveAllChildren(self.RankGrid.transform)

    for i = 0, CRankData.Inst.RankList.Count - 1 do
        local obj = NGUITools.AddChild(self.RankGrid.gameObject, self.ItemTemplate)
        self:InitRankItem(obj, CRankData.Inst.RankList[i], i % 2 == 0, i)
        obj:SetActive(true)
        table.insert(self.RankItemList, obj)
    end
    self.RankGrid:Reposition()
    self.RankScrollView:ResetPosition()
end

function LuaPUBGStartWnd:InitRankItem(item, data, isOdd, index)
    if not item or not data then return end
    local RankImage = item.transform:Find("Table/RankLabel/RankImage"):GetComponent(typeof(UISprite))
    local RankLabel = item.transform:Find("Table/RankLabel"):GetComponent(typeof(UILabel))
    local NameLabel = item.transform:Find("Table/NameLabel"):GetComponent(typeof(UILabel))
    local JobLabel = item.transform:Find("Table/JobLabel"):GetComponent(typeof(UILabel))
    local ScoreLabel = item.transform:Find("Table/ScoreLabel"):GetComponent(typeof(UILabel))

    local BG = item.transform:GetComponent(typeof(UISprite))
    BG.spriteName = isOdd and Constants.GreyItemBgSpriteName or Constants.WhiteItemBgSpriteName

    RankImage.gameObject:SetActive(data.Rank <= 3 and data.Rank >= 1)
    if data.Rank > 3 then
        RankLabel.text = tostring(data.Rank)
    elseif data.Rank == 0 then
        RankLabel.text = LocalString.GetString("未上榜")
    else
        local spriteNames = {"Rank_No.1", "Rank_No.2png", "Rank_No.3png"}
		RankImage.spriteName = spriteNames[data.Rank]
    end

    NameLabel.text = data.Name
    ScoreLabel.text = tostring(data.Value)
    JobLabel.text = Profession.GetFullName(data.Job)

    CommonDefs.AddOnClickListener(item, DelegateFactory.Action_GameObject(function (go)
        self:OnItemClicked(go, data.PlayerIndex, index)
    end), false)
end

function LuaPUBGStartWnd:OnItemClicked(go, playerId, index)
    self.SelectedTankIndex = index+1
    self:SetSelections()
    CPlayerInfoMgr.ShowPlayerPopupMenu(playerId, EnumPlayerInfoContext.Undefined,EChatPanel.Undefined,"",nil,Vector3.zero,AlignType.Default)
end

function LuaPUBGStartWnd:SetSelections()
    for i = 1, #self.RankItemList do
        local qn = self.RankItemList[i].transform:GetComponent(typeof(QnTableItem))
        qn:SetSelected(i == self.SelectedTankIndex, false)
    end
end

function LuaPUBGStartWnd:OnJoinButtonClicked(go)
    if LuaPUBGMgr.m_IsApplied then
        Gac2Gas.CancelApplyChiJi()
    else
        Gac2Gas.RequestApplyChiJi()
    end
end


function LuaPUBGStartWnd:OnEnable()
    g_ScriptEvent:AddListener("ApplyChiJiSuccess", self, "ProcessApplyStatus")
    g_ScriptEvent:AddListener("OnRankDataReady", self, "OnRankDataReady")
end

function LuaPUBGStartWnd:OnDisable()
    g_ScriptEvent:RemoveListener("ApplyChiJiSuccess", self, "ProcessApplyStatus")
    g_ScriptEvent:RemoveListener("OnRankDataReady", self, "OnRankDataReady")
end

return LuaPUBGStartWnd
