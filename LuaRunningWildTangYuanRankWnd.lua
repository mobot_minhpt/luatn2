local DelegateFactory  = import "DelegateFactory"
local QnTableView = import "L10.UI.QnTableView"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CUITexture = import "L10.UI.CUITexture"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local Profession = import "L10.Game.Profession"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr=import "L10.UI.CItemInfoMgr"
local CRankData = import "L10.UI.CRankData"

LuaRunningWildTangYuanRankWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaRunningWildTangYuanRankWnd, "TableView", "TableView", QnTableView)
RegistChildComponent(LuaRunningWildTangYuanRankWnd, "RefreshLabel", "RefreshLabel", UILabel)
RegistChildComponent(LuaRunningWildTangYuanRankWnd, "EmptyTipLabel", "EmptyTipLabel", UILabel)
RegistChildComponent(LuaRunningWildTangYuanRankWnd, "Top10Gift", "Top10Gift", GameObject)
RegistChildComponent(LuaRunningWildTangYuanRankWnd, "Top10GiftIcon", "Top10GiftIcon", CUITexture)
RegistChildComponent(LuaRunningWildTangYuanRankWnd, "Top100Gift", "Top100Gift", GameObject)
RegistChildComponent(LuaRunningWildTangYuanRankWnd, "Top100GiftIcon", "Top100GiftIcon", CUITexture)

--@endregion RegistChildComponent end
RegistClassMember(LuaRunningWildTangYuanRankWnd,"m_RankSprites")
RegistClassMember(LuaRunningWildTangYuanRankWnd,"m_RankData")
function LuaRunningWildTangYuanRankWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
	self.m_RankSprites = {
		[1] = "Rank_No.1",
		[2] = "Rank_No.2",
		[3] = "Rank_No.3",
	}
	self.m_RankData = {}
	local setting = YuanXiao_TangYuan2022Setting.GetData()
	Gac2Gas.QueryRank(setting.ScoreRankId)
end

function LuaRunningWildTangYuanRankWnd:Init()
	self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function() 
            return #self.m_RankData
        end,
        function(item,row)
            self:InitItem(item,row)
        end)
	
    self.TableView:ReloadData(false,false)

	local setting = YuanXiao_TangYuan2022Setting.GetData()
	local rankRewards = setting.TopRankRewardItemIds
	if rankRewards.Length>=4 then
		local itemId10 = rankRewards[1]
		local itemId100 = rankRewards[3]
		local data10 = Item_Item.GetData(itemId10)
		local data100 = Item_Item.GetData(itemId100)
		if data10 then
			self.Top10GiftIcon:LoadMaterial(data10.Icon)
		end
		if data100 then
			self.Top100GiftIcon:LoadMaterial(data100.Icon)
		end
		UIEventListener.Get(self.Top10Gift).onClick = DelegateFactory.VoidDelegate(function (go)
			CItemInfoMgr.ShowLinkItemTemplateInfo(itemId10,false,nil,AlignType.Default, 0, 0, 0, 0)
		end)
		UIEventListener.Get(self.Top100Gift).onClick = DelegateFactory.VoidDelegate(function (go)
			CItemInfoMgr.ShowLinkItemTemplateInfo(itemId100,false,nil,AlignType.Default, 0, 0, 0, 0)
		end)
	end
end

function LuaRunningWildTangYuanRankWnd:InitItem(item,row)
	local info = self.m_RankData[row+1]
	local bg = item.transform:Find("ItemBgSprite"):GetComponent(typeof(UISprite))
	local rankLabel = item.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
	local rankSprite = rankLabel.transform:Find("RankSprite"):GetComponent(typeof(UISprite))
	local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
	local scoreLabel = item.transform:Find("ScoreLabel"):GetComponent(typeof(UILabel))
	local classSprite = item.transform:Find("ClassSprite"):GetComponent(typeof(UISprite))
	local selfTag = item.transform:Find("SelfTag").gameObject

	if row%2 == 0 then
		bg.spriteName = "common_bg_mission_background_n"
	else
		bg.spriteName = "common_bg_mission_background_s"
	end

	if info.rank <= 3 then
		rankSprite.spriteName = self.m_RankSprites[info.rank]
		rankSprite.gameObject:SetActive(true)
	else
		rankSprite.gameObject:SetActive(false)
	end
	rankLabel.text = info.rank

	nameLabel.text = info.name
	scoreLabel.text = info.score
	classSprite.spriteName = Profession.GetIconByNumber(EnumToInt(info.job))
	selfTag:SetActive(false)
end

--@region UIEvent
--@endregion UIEvent

function LuaRunningWildTangYuanRankWnd:OnEnable( ... )
	g_ScriptEvent:AddListener("OnRankDataReady", self, "OnRankDataReady")
end

function LuaRunningWildTangYuanRankWnd:OnDisable( ... )
	g_ScriptEvent:RemoveListener("OnRankDataReady", self, "OnRankDataReady")
end

function LuaRunningWildTangYuanRankWnd:OnRankDataReady( ... )
	self.m_RankData = {}
	if CRankData.Inst.RankList and CRankData.Inst.RankList.Count > 0 then
		do
			local i = 0
			while i < CRankData.Inst.RankList.Count do
				local info = CRankData.Inst.RankList[i]
        		local extraList = MsgPackImpl.unpack(info.extraData)
				local data = {name = info.Name,rank = info.Rank,score = info.Value,job = info.Job}
				table.insert(self.m_RankData,data)
				i = i + 1
			end
			if i == 0 then
				self.EmptyTipLabel.gameObject:SetActive(true)
			else
				self.EmptyTipLabel.gameObject:SetActive(false)
			end
		end
    else
    	self.EmptyTipLabel.gameObject:SetActive(true)
	end
	self.TableView:ReloadData(false,false)
end
