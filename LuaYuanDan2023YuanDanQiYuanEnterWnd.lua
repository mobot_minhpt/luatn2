local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UITable = import "UITable"
local CButton = import "L10.UI.CButton"
local UILabel = import "UILabel"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local CTipParagraphItem = import "L10.UI.CTipParagraphItem"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"

LuaYuanDan2023YuanDanQiYuanEnterWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaYuanDan2023YuanDanQiYuanEnterWnd, "bg", "bg", GameObject)
RegistChildComponent(LuaYuanDan2023YuanDanQiYuanEnterWnd, "Table", "Table", UITable)
RegistChildComponent(LuaYuanDan2023YuanDanQiYuanEnterWnd, "RewardTable", "RewardTable", UITable)
RegistChildComponent(LuaYuanDan2023YuanDanQiYuanEnterWnd, "ParagraphTemplate", "ParagraphTemplate", GameObject)
RegistChildComponent(LuaYuanDan2023YuanDanQiYuanEnterWnd, "ItemCellTemplate", "ItemCellTemplate", GameObject)
RegistChildComponent(LuaYuanDan2023YuanDanQiYuanEnterWnd, "Button", "Button", CButton)
RegistChildComponent(LuaYuanDan2023YuanDanQiYuanEnterWnd, "RewardTimes", "RewardTimes", UILabel)
RegistChildComponent(LuaYuanDan2023YuanDanQiYuanEnterWnd, "ScrollView", "ScrollView", CUIRestrictScrollView)

--@endregion RegistChildComponent end
RegistClassMember(LuaYuanDan2023YuanDanQiYuanEnterWnd, "m_AwardTimes")
function LuaYuanDan2023YuanDanQiYuanEnterWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.Button.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnButtonClick()
	end)


    --@endregion EventBind end
end

function LuaYuanDan2023YuanDanQiYuanEnterWnd:Init()
	self.m_AwardTimes = 0
	self:ShowPlayRule()
	self:ShowAwardInfo()
	self.ParagraphTemplate.gameObject:SetActive(false)
	self.ItemCellTemplate.gameObject:SetActive(false)
	self.RewardTimes.gameObject:SetActive(false)
	Gac2Gas.QueryYuanDanQiYuanAwardTimes()
end

function LuaYuanDan2023YuanDanQiYuanEnterWnd:ShowPlayRule()
	Extensions.RemoveAllChildren(self.Table.transform)
    local msg =	g_MessageMgr:FormatMessage("YuanDan2023_YuanDanQiYuan_Rule")
    if System.String.IsNullOrEmpty(msg) then
        return
    end

    local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(msg)

    if info == nil then
		local paragraphGo = CUICommonDef.AddChild(self.Table.gameObject, self.ParagraphTemplate)
		paragraphGo:SetActive(true)
		local tip = paragraphGo.gameObject:GetComponent(typeof(CTipParagraphItem))
		tip:Init(msg,4294967295)
        return
    end

	local i = 0
	while i < info.paragraphs.Count do
		local paragraphGo = CUICommonDef.AddChild(self.Table.gameObject, self.ParagraphTemplate)
		paragraphGo:SetActive(true)
		local tip = paragraphGo.gameObject:GetComponent(typeof(CTipParagraphItem))
		tip:Init(info.paragraphs[i],4294967295)
		i = i + 1
	end
    self.ScrollView:ResetPosition()
	self.Table:Reposition()
end
function LuaYuanDan2023YuanDanQiYuanEnterWnd:ShowAwardInfo()
	local awardList = YuanDan2023_YuanDanQiYuan.GetData().AwardItemList
	Extensions.RemoveAllChildren(self.RewardTable.transform)
	if awardList.Length > 0 then
		for i = 0, awardList.Length - 1 do
			local go = CUICommonDef.AddChild(self.RewardTable.gameObject, self.ItemCellTemplate)
			self:InitOneItem(go,awardList[i])
		end
	end
	self.RewardTable:Reposition()
end

function LuaYuanDan2023YuanDanQiYuanEnterWnd:UpdateAwardTimes(times)
	self.m_AwardTimes = times
	self.RewardTimes.gameObject:SetActive(true)
	local FullTime = YuanDan2023_YuanDanQiYuan.GetData().AwardLimit
	local Mesg = g_MessageMgr:FormatMessage("YuanDan2023_YuanDanQiYuan_RewardTimes",self.m_AwardTimes,FullTime)
	--local Mesg = SafeStringFormat3(LocalString.GetString("今日奖励次数%d/%d"),self.m_AwardTimes,FullTime)
	self.RewardTimes.text = Mesg
end

function LuaYuanDan2023YuanDanQiYuanEnterWnd:InitOneItem(curItem, itemID)
    local texture = curItem.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
	local Received = curItem.transform:Find("Received").gameObject
    local ItemData = Item_Item.GetData(itemID)
	curItem:SetActive(true)
	Received:SetActive(false)
    if ItemData then texture:LoadMaterial(ItemData.Icon) end

    CommonDefs.AddOnClickListener(
        curItem,
        DelegateFactory.Action_GameObject(
            function(go)
                CItemInfoMgr.ShowLinkItemTemplateInfo(itemID, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
            end
        ),
        false
    )
end
--@region UIEvent

function LuaYuanDan2023YuanDanQiYuanEnterWnd:OnButtonClick()
	-- 发RPC
	Gac2Gas.EnterYuanDanQiYuanPlay()
end

--@endregion UIEvent

function LuaYuanDan2023YuanDanQiYuanEnterWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateYuanDanQiAwardTimes",self,"UpdateAwardTimes")
end

function LuaYuanDan2023YuanDanQiYuanEnterWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateYuanDanQiAwardTimes",self,"UpdateAwardTimes")
end