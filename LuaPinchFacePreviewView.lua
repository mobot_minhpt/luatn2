local QnNewSlider = import "L10.UI.QnNewSlider"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local QnRadioBox = import "L10.UI.QnRadioBox"
local UILabel = import "UILabel"
local CButton = import "L10.UI.CButton"
local GameObject = import "UnityEngine.GameObject"
local CPinchFaceCinemachineCtrlMgr = import "L10.Game.CPinchFaceCinemachineCtrlMgr"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local EnumGender = import "L10.Game.EnumGender"
local Animation = import "UnityEngine.Animation"
local State = import "L10.UI.CButton+State"

LuaPinchFacePreviewView = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaPinchFacePreviewView, "HideUIAnchor", "HideUIAnchor", GameObject)
RegistChildComponent(LuaPinchFacePreviewView, "BottomBtnsTable", "BottomBtnsTable", UIGrid)
RegistChildComponent(LuaPinchFacePreviewView, "LookAtBtn", "LookAtBtn", CButton)
RegistChildComponent(LuaPinchFacePreviewView, "HideUIBtn", "HideUIBtn", CButton)
RegistChildComponent(LuaPinchFacePreviewView, "ShareBtn", "ShareBtn", CButton)
RegistChildComponent(LuaPinchFacePreviewView, "LuoZhuangBtn", "LuoZhuangBtn", CButton)
RegistChildComponent(LuaPinchFacePreviewView, "ChangeExpressionBtn", "ChangeExpressionBtn", CButton)
RegistChildComponent(LuaPinchFacePreviewView, "TabReadMeLabel", "TabReadMeLabel", UILabel)
RegistChildComponent(LuaPinchFacePreviewView, "LeftTabScrollView", "LeftTabScrollView", CUIRestrictScrollView)
RegistChildComponent(LuaPinchFacePreviewView, "LeftTabQnRadioBox", "LeftTabQnRadioBox", QnRadioBox)
RegistChildComponent(LuaPinchFacePreviewView, "LeftTabTemplate", "LeftTabTemplate", GameObject)
RegistChildComponent(LuaPinchFacePreviewView, "LeftBoxScrollView", "LeftBoxScrollView", CUIRestrictScrollView)
RegistChildComponent(LuaPinchFacePreviewView, "LeftQnRadioBox", "LeftQnRadioBox", QnRadioBox)
RegistChildComponent(LuaPinchFacePreviewView, "ItemTemplate", "ItemTemplate", GameObject)
RegistChildComponent(LuaPinchFacePreviewView, "RanFaController", "RanFaController", GameObject)
RegistChildComponent(LuaPinchFacePreviewView, "TextGrid", "TextGrid", UIGrid)
RegistChildComponent(LuaPinchFacePreviewView, "TextTemplate", "TextTemplate", GameObject)
RegistChildComponent(LuaPinchFacePreviewView, "Slider", "Slider", QnNewSlider)
RegistChildComponent(LuaPinchFacePreviewView, "PauseButton", "PauseButton", CButton)
RegistChildComponent(LuaPinchFacePreviewView, "FashionNameLabel", "FashionNameLabel", UILabel)
RegistChildComponent(LuaPinchFacePreviewView, "UploadBtn", "UploadBtn", CButton)
--@endregion RegistChildComponent end
RegistClassMember(LuaPinchFacePreviewView, "m_IsInit")
RegistClassMember(LuaPinchFacePreviewView, "m_LeftTabIndex")
RegistClassMember(LuaPinchFacePreviewView, "m_LastLeftTabIndex")
RegistClassMember(LuaPinchFacePreviewView, "m_AniTotalTime")
RegistClassMember(LuaPinchFacePreviewView, "m_DeltaTime")
RegistClassMember(LuaPinchFacePreviewView, "m_Paused")
RegistClassMember(LuaPinchFacePreviewView, "m_ExpressionID")
RegistClassMember(LuaPinchFacePreviewView, "m_HairColorViewIndex")
RegistClassMember(LuaPinchFacePreviewView, "m_FashionViewIndex")
RegistClassMember(LuaPinchFacePreviewView, "m_SkinColorViewIndex")
RegistClassMember(LuaPinchFacePreviewView, "m_EnvironmentFxViewIndex")
RegistClassMember(LuaPinchFacePreviewView, "m_ExpressionViewIndex")
RegistClassMember(LuaPinchFacePreviewView, "m_BgColorViewIndex")
RegistClassMember(LuaPinchFacePreviewView, "m_MovieCaptureTick")
RegistClassMember(LuaPinchFacePreviewView, "m_InCustomFashion")
RegistClassMember(LuaPinchFacePreviewView, "m_FashionDataArr")
RegistClassMember(LuaPinchFacePreviewView, "m_EquipmentDataArr")
RegistClassMember(LuaPinchFacePreviewView, "m_BgColorDataArr")
RegistClassMember(LuaPinchFacePreviewView, "m_EnvironmentFxDataArr")
RegistClassMember(LuaPinchFacePreviewView, "m_EquipmentViewIndex")
RegistClassMember(LuaPinchFacePreviewView, "m_ShenBingFashionIndex")
RegistClassMember(LuaPinchFacePreviewView, "m_ShenBingEquipmentIndex")
RegistClassMember(LuaPinchFacePreviewView, "m_ExpressionViewTabIndex")
RegistClassMember(LuaPinchFacePreviewView, "m_HairColorViewTabIndex")
RegistClassMember(LuaPinchFacePreviewView, "m_FashionViewTabIndex")
RegistClassMember(LuaPinchFacePreviewView, "m_SkinColorViewTabIndex")
RegistClassMember(LuaPinchFacePreviewView, "m_EnvironmentFxViewTabIndex")
RegistClassMember(LuaPinchFacePreviewView, "m_BgColorViewTabIndex")
RegistClassMember(LuaPinchFacePreviewView, "m_EquipmentViewTabIndex")

function LuaPinchFacePreviewView:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.LookAtBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLookAtBtnClick()
	end)


	
	UIEventListener.Get(self.HideUIBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnHideUIBtnClick()
	end)


	
	UIEventListener.Get(self.ShareBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareBtnClick()
	end)


	
	UIEventListener.Get(self.ChangeExpressionBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnChangeExpressionBtnClick()
	end)


	
	UIEventListener.Get(self.PauseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnPauseButtonClick()
	end)


    --@endregion EventBind end

    self.Slider.OnValueChanged = DelegateFactory.Action_float(function(value)
        self:OnSliderValueChanged(value)
    end)

    UIEventListener.Get(self.LuoZhuangBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLuoZhuangBtnClick()
	end)

    UIEventListener.Get(self.UploadBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnUploadBtnClick()
	end)

    UIEventListener.Get(self.HideUIBtn.gameObject).onPress = DelegateFactory.BoolDelegate(function (go, isPressed)
        self:OnHideUIBtnPress(isPressed)
    end)
end

function LuaPinchFacePreviewView:Start()
    self.LeftTabTemplate.gameObject:SetActive(false)
    self.ItemTemplate.gameObject:SetActive(false)
    self.RanFaController.gameObject:SetActive(false)
    self.FashionNameLabel.gameObject:SetActive(false)
    self.TextTemplate.gameObject:SetActive(false)
    self.ItemTemplate.transform:Find("DefaultLabel").gameObject:SetActive(false)
    self.ItemTemplate.transform:Find("WhiteSprite").gameObject:SetActive(false)
    self.ItemTemplate.transform:Find("IconTexture").gameObject:SetActive(false)
    self.ItemTemplate.transform:Find("Background").gameObject:SetActive(false)
    self.ChangeExpressionBtn.gameObject:SetActive(false)
    self.LuoZhuangBtn.gameObject:SetActive(true)
    self.Slider.gameObject:SetActive(false)
    self.BottomBtnsTable:Reposition()
    self.UploadBtn.gameObject:SetActive(not LuaCloudGameFaceMgr:IsCloudGameFace())
    self:Init()
    self:ShowView()
    if LuaCloudGameFaceMgr:IsCloudGameFace() then self.TabReadMeLabel.transform.parent.gameObject:SetActive(false) end
end

function LuaPinchFacePreviewView:Init()
    self.m_HairColorViewIndex = 0
    self.m_FashionViewIndex = 0
    if CClientMainPlayer.Inst then
        self.m_FashionViewIndex = nil
    end
    self.m_SkinColorViewIndex = 0
    self.m_EnvironmentFxViewIndex = 0
    self.m_BgColorViewIndex = 0
    self:InitFashionDataArr()
    self:InitEquipmentDataArr()
    self:InitBgColorDataArr()
    self:InitEnvironmentFxDataArr()
    self:InitTabIndexs()
    self:InitLeftTabQnRadioBox()
    self.m_IsInit = true
end

function LuaPinchFacePreviewView:InitTabIndexs()
    local previewViewTabData = PinchFace_Setting.GetData().PreviewViewTab
    for i = 0, previewViewTabData.Length - 1 do
        local str = previewViewTabData[i][1]
        if string.find(str, "Movement") then
            self.m_ExpressionViewTabIndex = i
        elseif string.find(str, "Hair")  then
            self.m_HairColorViewTabIndex = i
        elseif string.find(str, "Cloth") then
            self.m_FashionViewTabIndex = i
        elseif string.find(str, "SkinColor") then
            self.m_SkinColorViewTabIndex = i
        elseif string.find(str, "Environment") then
            self.m_EnvironmentFxViewTabIndex = i
        elseif string.find(str, "Background") then
            self.m_BgColorViewTabIndex = i
        elseif string.find(str, "Equipment") then
            self.m_EquipmentViewTabIndex = i
        end
    end
end

function LuaPinchFacePreviewView:InitLeftTabQnRadioBox()
    Extensions.RemoveAllChildren(self.LeftTabQnRadioBox.m_Table.transform)
    local previewViewTabData = PinchFace_Setting.GetData().PreviewViewTab
    self.LeftTabQnRadioBox.m_RadioButtons = CreateFromClass(MakeArrayClass(QnSelectableButton), previewViewTabData.Length)
    for i = 1, previewViewTabData.Length do
        local go = NGUITools.AddChild(self.LeftTabQnRadioBox.m_Table.gameObject, self.LeftTabTemplate.gameObject)
        go.gameObject:SetActive(true)
        local btn = go.gameObject:GetComponent(typeof(QnSelectableButton))
        btn.Text = previewViewTabData[i - 1][0]
        self.LeftTabQnRadioBox.m_RadioButtons[i - 1] = btn
		btn.OnClick = MakeDelegateFromCSFunction(self.LeftTabQnRadioBox.On_Click, MakeGenericClass(Action1, QnButton), self.LeftTabQnRadioBox)
    end
    self.LeftTabQnRadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn,index)
        self:OnSelectLeftTabQnRadioBox(btn, index)
    end)
    self.LeftTabScrollView:ResetPosition()
    self.LeftTabQnRadioBox.m_Table:Reposition()
end

function LuaPinchFacePreviewView:InitFashionDataArr()
    self.m_ShenBingFashionIndex = LuaPinchFaceMgr.m_ShenBingFashionIndex
    self.m_FashionDataArr = LuaPinchFaceMgr.m_FashionDataArr
end

function LuaPinchFacePreviewView:InitEquipmentDataArr()
    local list = {}
    local shenBingEquipmentIndex = -1
    PinchFace_PreviewEquipment.ForeachKey(function (key)
        local data = PinchFace_PreviewEquipment.GetData(key)
        if  data.Race == nil or (data.Race and LuaPinchFaceMgr.m_CurEnumClass and CommonDefs.HashSetContains(data.Race, typeof(UInt32), EnumToInt(LuaPinchFaceMgr.m_CurEnumClass))) then
            if data.GenderLimit == 0 or (LuaPinchFaceMgr.m_CurEnumGender == EnumGender.Male and data.GenderLimit == 1) or (LuaPinchFaceMgr.m_CurEnumGender == EnumGender.Female and data.GenderLimit == 2) then
                table.insert(list, data)
                if data.ShenBingOrNot == 1 then
                    shenBingEquipmentIndex = #list - 1
                end
            end
        end
    end)
    self.m_ShenBingEquipmentIndex = shenBingEquipmentIndex
    self.m_EquipmentDataArr = list
end

function LuaPinchFacePreviewView:InitBgColorDataArr()
    local list = {}
    PinchFace_BgColor.ForeachKey(function (key)
        table.insert(list, PinchFace_BgColor.GetData(key))
    end)
    self.m_BgColorDataArr = list
end

function LuaPinchFacePreviewView:InitEnvironmentFxDataArr()
    local list = {}
    table.insert(list, {isDefault = true})
    PinchFace_PreviewEnvironmentFx.ForeachKey(function (key)
        table.insert(list, PinchFace_PreviewEnvironmentFx.GetData(key))
    end)
    self.m_EnvironmentFxDataArr = list
end

function LuaPinchFacePreviewView:ShowView()
    if not self.m_IsInit then return end
    self:InitFashionDataArr()
    self:InitEquipmentDataArr()
    self:GetDefaultQnRadioBoxIndex()
    if self.m_EquipmentViewIndex then
        local data = self.m_EquipmentDataArr[self.m_EquipmentViewIndex + 1]
        LuaPinchFaceMgr:SetEquipment(data)
    end
    if CPinchFaceCinemachineCtrlMgr.Inst then
        CPinchFaceCinemachineCtrlMgr.Inst:ShowCam(4)
    end
    self:OnSetPinchFaceIKLookAtTo()
    self.HideUIBtn.Selected = false
    self.ChangeExpressionBtn.Selected = false
    self:OnShowLuoZhuangPinchFace()
    self.ChangeExpressionBtn.gameObject:SetActive(false)
    RegisterTickOnce(function()
        if self.BottomBtnsTable then
            self.BottomBtnsTable:Reposition()
        end
    end,100)
    self.LeftTabQnRadioBox:ChangeTo(0, true)
    self.m_Paused = true
    self:PauseOrPlay(self.m_Paused)
    if LuaPinchFaceMgr.m_IsInModifyMode then
        CLuaRanFaMgr:RequestSyncRanFaJiPlayData(EnumSyncRanFaJiType.eDefault)
    end
end

function LuaPinchFacePreviewView:GetDefaultQnRadioBoxIndex()
    if LuaPinchFaceMgr.m_FashionData then
        if LuaPinchFaceMgr.m_FashionData.isDefault then
            self.m_FashionViewIndex = 0
        else
            for i, data in pairs(self.m_FashionDataArr) do
                if data.data and data.data.ID == LuaPinchFaceMgr.m_FashionData.data.ID then
                    self.m_FashionViewIndex = i - 1
                    break
                end 
            end
        end
    end
    if LuaPinchFaceMgr.m_EnvironmentFxData then
        if LuaPinchFaceMgr.m_EnvironmentFxData.isDefault then
            self.m_EnvironmentFxViewIndex = 0
        else
            for i, data in pairs(self.m_EnvironmentFxDataArr) do
                if data.ID == LuaPinchFaceMgr.m_EnvironmentFxData.ID then
                    self.m_EnvironmentFxViewIndex = i - 1
                    break
                end 
            end
        end
    end
    if LuaPinchFaceMgr.m_BgColorData then
        for i, data in pairs(self.m_BgColorDataArr) do
            if data.ID == LuaPinchFaceMgr.m_BgColorData.ID then
                self.m_BgColorViewIndex = i - 1
                break
            end 
        end
    end
    self.m_SkinColorViewIndex = LuaPinchFaceMgr.m_SkinColorIndex
    self.m_HairColorViewIndex = (LuaPinchFaceMgr.m_IsInModifyMode and CLuaRanFaMgr.m_MyRanFaJiId ~= 0) and CLuaRanFaMgr.m_MyRanFaJiId or LuaPinchFaceMgr.m_AdvancedHairColorIndex
    local data = LuaPinchFaceMgr.m_FashionData 
    self.m_InCustomFashion = data and data.data and data.data.FashionId and data.data.FashionId.Length > 0
end

function LuaPinchFacePreviewView:InitLeftQnRadioBox(template, list, onSelectQnRadioBoxAction, initItemAction, table)
    self.LeftQnRadioBox.m_RadioButtons = CreateFromClass(MakeArrayClass(QnSelectableButton), #list)
    if table == nil then
        table = self.LeftQnRadioBox.m_Table
    end
    for i = 1, #list do
        local go = NGUITools.AddChild(table.gameObject, template.gameObject)
        go.gameObject:SetActive(true)
        local btn = go.gameObject:GetComponent(typeof(QnSelectableButton))
        if initItemAction then
            initItemAction(btn, list[i])
        end
        self.LeftQnRadioBox.m_RadioButtons[i - 1] = btn
		btn.OnClick = MakeDelegateFromCSFunction(self.LeftQnRadioBox.On_Click, MakeGenericClass(Action1, QnButton), self.LeftQnRadioBox)
    end
    self.LeftQnRadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn,index)
        if onSelectQnRadioBoxAction then
            onSelectQnRadioBoxAction(index)
        end
    end)
    self.LeftBoxScrollView:ResetPosition()
    table:Reposition()
end

function LuaPinchFacePreviewView:InitHairColorView()
    local list = LuaPinchFaceMgr.m_HairColorDataArr
    self:InitLeftQnRadioBox(self.ItemTemplate,list, function(index)
        if self.m_InCustomFashion then
            MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("RanFaJi_Fashion_TakeOff"),DelegateFactory.Action(function()
                self:OnSelectFashion(0)
                self:OnSelectHairColorViewQnRadioBox(self.m_HairColorViewIndex)
            end),DelegateFactory.Action(function()
                if self.m_LastLeftTabIndex == self.m_HairColorViewTabIndex or self.m_LastLeftTabIndex == nil then
                    self.m_LastLeftTabIndex = self.m_FashionViewTabIndex
                end
                self.LeftTabQnRadioBox:ChangeTo(self.m_LastLeftTabIndex, true)
            end),LocalString.GetString("确定"),LocalString.GetString("取消"),false)
            return
        end
        for i = 0, #list - 1 do
            local btn = self.LeftQnRadioBox.m_RadioButtons[i]
            local icon = btn.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
            icon.alpha = index == i and 1 or 0.65
        end
        self:OnSelectHairColorViewQnRadioBox(index)
    end, function (btn, data)
        self:InitHairColorViewItem(btn, data)
    end)
    if self.m_HairColorViewIndex then
        self.LeftQnRadioBox:ChangeTo(self.m_HairColorViewIndex,true)
    end
end

function LuaPinchFacePreviewView:InitHairColorViewItem(btn, data)
    local icon = btn.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    local defaultLabel = btn.transform:Find("DefaultLabel")
    defaultLabel.gameObject:SetActive(data.isDefault)
    icon.gameObject:SetActive(true)
    if data.Icon then
        icon:LoadMaterial(data.Icon)
    else
        icon.gameObject:SetActive(false)
        --icon:LoadMaterial("UI/Texture/Facial/Material/facial_preview_skin_06.mat")
    end
    btn:SetSelected(false, false)
end

function LuaPinchFacePreviewView:InitRanFaController(data)
    local topLabel = self.RanFaController.transform:Find("TopLabel"):GetComponent(typeof(UILabel))
    local lockedObj = self.RanFaController.transform:Find("Locked").gameObject
    local unlockedObj = self.RanFaController.transform:Find("Unlocked").gameObject
    local getButton = lockedObj.transform:Find("GetButton").gameObject
    local costLabel = unlockedObj.transform:Find("CostLabel"):GetComponent(typeof(UILabel))
    local addBtn = unlockedObj.transform:Find("CostLabel/AddBtn").gameObject
    local ranFaButton = unlockedObj.transform:Find("RanFaButton").gameObject

    lockedObj.gameObject:SetActive(not data.isDefault and CLuaRanFaMgr.m_OwnRanFaJiTable[data.ID] == nil)
    unlockedObj.gameObject:SetActive(CLuaRanFaMgr.m_OwnRanFaJiTable[data.ID] ~= nil or data.isDefault)
    topLabel.text = data.isDefault and LocalString.GetString("默认染色") or (data.ColorName..LocalString.GetString("染发剂"))
    costLabel.text = data.isDefault and 0 or RanFaJi_Setting.GetData().TakeOnCostMoney
    UIEventListener.Get(getButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        CLuaRanFaMgr:OpenRecipeWnd(data.ID)
	end)
    UIEventListener.Get(addBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    CShopMallMgr.ShowLinyuShoppingMall(Constants.SilverItemId)
	end)
    UIEventListener.Get(ranFaButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRanFaBtnClick(data)
	end)
end

function LuaPinchFacePreviewView:InitFashionView()
    local list = self.m_FashionDataArr
   
    self:InitLeftQnRadioBox(self.TextTemplate,list, function(index)
        self:OnSelectFashion(index)
    end, function (btn, data)
        self:InitFashionViewItem(btn, data.data.Number)
    end, self.TextGrid)
    if self.m_FashionViewIndex then
        self.LeftQnRadioBox:ChangeTo(self.m_FashionViewIndex,true)
    end
end

function LuaPinchFacePreviewView:InitFashionViewItem(btn, name)
    local label = btn.transform:Find("Label"):GetComponent(typeof(UILabel))
    label.text = name
    btn:SetSelected(false, false)
end

function LuaPinchFacePreviewView:OnSelectFashion(index)
    local data = self.m_FashionDataArr[index + 1]
    self.m_InCustomFashion = data.data and data.data.FashionId and data.data.FashionId.Length > 0
    if LuaPinchFaceMgr.m_RO then
        CommonDefs.DictClear(LuaPinchFaceMgr.m_RO.m_Changes)
    end
    if self.m_FashionViewIndex ~= index then
        LuaPinchFaceMgr:SetFashion(data, nil, false)
    end
    self.FashionNameLabel.gameObject:SetActive(true)
    if data.data then
        self.FashionNameLabel.text = data.data.Description
    end
    self.m_FashionViewIndex = index
    if self.m_ShenBingEquipmentIndex == self.m_EquipmentViewIndex and index == 0 then
        g_MessageMgr:ShowMessage("PinchFacePreview_ShenBingEquipment_TakeOffConfirm")
        self:OnSelectEquipment(1)
    end
    self.BottomBtnsTable:Reposition()
end

function LuaPinchFacePreviewView:InitSkinColorView()
    local list = {}
    table.insert(list, {isDefault = true})
    local tanningSkinColors = PinchFace_Setting.GetData().NewTanningSkinColor
    for i = 0, tanningSkinColors.Length - 1 do
        table.insert(list, {colorRGB = tanningSkinColors[i],Icon = SafeStringFormat("UI/Texture/Facial/Material/facial_preview_skin_0%d.mat", (i + 1))})
    end
    self:InitLeftQnRadioBox(self.ItemTemplate,list, function(index)
        self.m_SkinColorViewIndex = index
        LuaPinchFaceMgr:SetSkinColor(index)
    end, function (btn, data)
        self:InitSkinColorViewItem(btn, data)
    end)
    if self.m_SkinColorViewIndex then
        self.LeftQnRadioBox:ChangeTo(self.m_SkinColorViewIndex,true)
    end
end

function LuaPinchFacePreviewView:InitSkinColorViewItem(btn, data)
    local defaultLabel = btn.transform:Find("DefaultLabel")
    defaultLabel.gameObject:SetActive(data.isDefault)
    local icon = btn.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    icon.gameObject:SetActive(true)
    if data.Icon then
        icon.gameObject:SetActive(true)
        icon:LoadMaterial(data.Icon)
    else
        icon:LoadMaterial("UI/Texture/Facial/Material/facial_preview_skin_06.mat")
    end
    btn.m_SelectTip = btn.transform:Find("Background").gameObject
    btn.transform:Find("Highlight").gameObject:SetActive(false)
    btn:SetSelected(false, false)
end

function LuaPinchFacePreviewView:InitExpressionView()
    local list = {}
    PinchFace_PreviewExpression.ForeachKey(function (key)
        local data = PinchFace_PreviewExpression.GetData(key)
        if LuaPinchFaceMgr.m_CurEnumGender and (data.GenderLimit == 0 or ((EnumToInt(LuaPinchFaceMgr.m_CurEnumGender) + 1) == data.GenderLimit)) then
            table.insert(list, {ExpressionID = data.ExpressionId,Icon = SafeStringFormat("UI/Texture/Facial/Material/%s_z.mat", data.Icon)})
        end
    end)
    self:InitLeftQnRadioBox(self.ItemTemplate,list, function(index)
        self.m_ExpressionViewIndex = index
        local data = list[index + 1]
        self.m_ExpressionID = data.ExpressionID
        self.m_DeltaTime = 0
        self.m_AniTotalTime = LuaPinchFaceMgr:GetExpressionAnimationTime(self.m_ExpressionID)
        self:UpdateChangeExpressionBtnAndSlider()
        LuaPinchFaceMgr:ShowExpressionAction(data.ExpressionID, 0)
        self:PauseOrPlay(false)
    end, function (btn, data)
        self:InitExpressionViewItem(btn, data)
    end)
end

function LuaPinchFacePreviewView:UpdateChangeExpressionBtnAndSlider()
    self.ChangeExpressionBtn.gameObject:SetActive(not self.HideUIBtn.Selected and self.m_ExpressionViewTabIndex == self.m_LeftTabIndex and self.m_ExpressionViewIndex)
    self.BottomBtnsTable:Reposition()
    self.Slider.gameObject:SetActive(self.ChangeExpressionBtn.gameObject.activeSelf and self.ChangeExpressionBtn.Selected)
end

function LuaPinchFacePreviewView:UpdateLookAtBtn()
    self.LookAtBtn.gameObject:SetActive(not self.HideUIBtn.Selected and self.m_Paused)
    self.BottomBtnsTable:Reposition()
end

function LuaPinchFacePreviewView:InitExpressionViewItem(btn, data)
    local icon = btn.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    if data.Icon then
        icon.gameObject:SetActive(true)
        icon:LoadMaterial(data.Icon)
    end
    btn.m_SelectTip = btn.transform:Find("Background").gameObject
    btn.transform:Find("Highlight").gameObject:SetActive(false)
    btn:SetSelected(false, false)
end

function LuaPinchFacePreviewView:PauseOrPlay(pause)
    self.m_Paused = pause
    self.PauseButton.Selected = not self.m_Paused
    self:UpdateLookAtBtn()
end

function LuaPinchFacePreviewView:InitEnvironmentFxView()
    local list = self.m_EnvironmentFxDataArr
    self:InitLeftQnRadioBox(self.TextTemplate,list, function(index)
        self.m_EnvironmentFxViewIndex = index
        local data = list[index + 1]
        for i = 0, #list - 1 do
            local btn = self.LeftQnRadioBox.m_RadioButtons[i]
            local lableShadow = btn.transform:Find("Label/LableShadow")
            lableShadow.gameObject:SetActive(index ~= i)
        end
        LuaPinchFaceMgr:SetEnvironmentFx(data)
    end, function (btn, data)
        self:InitEnvironmentFxViewItem(btn, data)
    end, self.TextGrid)
    if self.m_EnvironmentFxViewIndex then
        self.LeftQnRadioBox:ChangeTo(self.m_EnvironmentFxViewIndex,true)
    end
end

function LuaPinchFacePreviewView:InitEnvironmentFxViewItem(btn, data)
    local label = btn.transform:Find("Label"):GetComponent(typeof(UILabel))
    label.text = data.isDefault and LocalString.GetString("无") or data.Name
    btn:SetSelected(false, false)
end

function LuaPinchFacePreviewView:InitBgColorView()
    local list = self.m_BgColorDataArr
    self:InitLeftQnRadioBox(self.ItemTemplate,list, function(index)
        self.m_BgColorViewIndex = index
        local data = list[index + 1]
        LuaPinchFaceMgr:SetCameraBgColor(data)
    end, function (btn, data)
        self:InitBgColorViewItem(btn, data)
    end)
    if self.m_BgColorViewIndex then
        self.LeftQnRadioBox:ChangeTo(self.m_BgColorViewIndex,true)
    end
end

function LuaPinchFacePreviewView:InitBgColorViewItem(btn, data)
    local icon = btn.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    if data.Icon then
        icon.gameObject:SetActive(true)
        icon:LoadMaterial(data.Icon)
    end
    btn.m_SelectTip = btn.transform:Find("Background").gameObject
    btn.transform:Find("Highlight").gameObject:SetActive(false)
    btn:SetSelected(false, false)
end

function LuaPinchFacePreviewView:CancelMovieCaptureTick()
    if self.m_MovieCaptureTick then
        UnRegisterTick(self.m_MovieCaptureTick)
        self.m_MovieCaptureTick = nil
    end
end

function LuaPinchFacePreviewView:InitEquipmentView()
    local list = self.m_EquipmentDataArr
    self:InitLeftQnRadioBox(self.TextTemplate,list, function(index)  
        if self.m_FashionViewIndex == 0 and index == self.m_ShenBingEquipmentIndex then
            MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("PinchFacePreview_ShenBingEquipment_DressConfirm"),DelegateFactory.Action(function()
                self:OnSelectFashion(self.m_ShenBingFashionIndex - 1)
                self:OnSelectEquipment(index)
            end),DelegateFactory.Action(function()
                if self.m_EquipmentViewIndex == index then
                    self:OnSelectEquipment(1)
                    self.LeftTabQnRadioBox:ChangeTo(self.m_LastLeftTabIndex, true)
                else
                    self.LeftQnRadioBox:ChangeTo(self.m_EquipmentViewIndex and self.m_EquipmentViewIndex or 0,true)
                end
            end),LocalString.GetString("确定"),LocalString.GetString("取消"),false)
            return
        end
        self:OnSelectEquipment(index)
    end, function (btn, data)
        self:InitEquipmentViewItem(btn, data)
    end, self.TextGrid)
    if self.m_EquipmentViewIndex then
        self.LeftQnRadioBox:ChangeTo(self.m_EquipmentViewIndex,true)
    end
end

function LuaPinchFacePreviewView:InitEquipmentViewItem(btn, data)
    local label = btn.transform:Find("Label"):GetComponent(typeof(UILabel))
    label.text = data.Number
    btn.name = data.ID
    btn:SetSelected(false, false)
end

function LuaPinchFacePreviewView:OnSelectEquipment(index)
    local data = self.m_EquipmentDataArr[index + 1]
    self.FashionNameLabel.text = ""
    if data and not cs_string.IsNullOrEmpty(data.Description) then
        self.FashionNameLabel.text = data.Description
    end
    LuaPinchFaceMgr:SetEquipment(data)
    self.m_EquipmentViewIndex = index
end
--@region UIEvent

function LuaPinchFacePreviewView:OnLookAtBtnClick()
    self.LookAtBtn.Selected = not self.LookAtBtn.Selected
    LuaPinchFaceMgr:SetIKLookAtTo(self.LookAtBtn.Selected)
end

function LuaPinchFacePreviewView:OnHideUIBtnClick()
    self.HideUIBtn.Selected = not self.HideUIBtn.Selected
    self.HideUIAnchor.gameObject:SetActive(not self.HideUIBtn.Selected)
    for i = 0, self.BottomBtnsTable.transform.childCount - 1 do
        local btn = self.BottomBtnsTable.transform:GetChild(i).gameObject
        btn.gameObject:SetActive(self.HideUIBtn.gameObject == btn.gameObject or not self.HideUIBtn.Selected)
    end

    if LuaCloudGameFaceMgr:IsCloudGameFace() then self.UploadBtn.gameObject:SetActive(false) end
    
    self:UpdateLookAtBtn()
    self:UpdateChangeExpressionBtnAndSlider()
    Extensions.SetLocalPositionX( self.BottomBtnsTable.transform, self.HideUIBtn.Selected and 975 or 464)
end

function LuaPinchFacePreviewView:OnShareBtnClick()
    LuaPinchFaceMgr:ShowShareWnd()
end

function LuaPinchFacePreviewView:OnSelectLeftTabQnRadioBox(btn, index)
    self.m_LastLeftTabIndex = self.m_LeftTabIndex
    self.m_LeftTabIndex = index
    self.RanFaController.gameObject:SetActive(false)
    self.FashionNameLabel.gameObject:SetActive(false)
    local previewViewTabData = PinchFace_Setting.GetData().PreviewViewTab
    local msgName = previewViewTabData[index][1]
    self.TabReadMeLabel.text = g_MessageMgr:FormatMessage(msgName)
    local ani = self.TabReadMeLabel.transform.parent:GetComponent(typeof(Animation))
    ani:Stop()
    ani:Play()
    self.m_ExpressionViewIndex = nil
    self.m_AniTotalTime = 0
    self.ChangeExpressionBtn.Selected = false
    self.Slider.gameObject:SetActive(false)
    self.ChangeExpressionBtn.gameObject:SetActive(false)
    self:UpdateLookAtBtn()
    Extensions.RemoveAllChildren(self.LeftQnRadioBox.m_Table.transform)
    Extensions.RemoveAllChildren(self.TextGrid.transform)
    if self.m_HairColorViewTabIndex == index then
        self:InitHairColorView()
    elseif self.m_FashionViewTabIndex == index then
        self:InitFashionView()
    elseif self.m_SkinColorViewTabIndex == index then
        self:InitSkinColorView()
    elseif self.m_ExpressionViewTabIndex == index then
        self:InitExpressionView()
    elseif self.m_EnvironmentFxViewTabIndex == index then
        self:InitEnvironmentFxView()
    elseif self.m_BgColorViewTabIndex == index then
        self:InitBgColorView()
    elseif self.m_EquipmentViewTabIndex == index then
        self:InitEquipmentView()
    end
    if CPinchFaceCinemachineCtrlMgr.Inst then
        local camIndex = 0
        if self.m_HairColorViewTabIndex == index or self.m_SkinColorViewTabIndex == index then
            camIndex = 4
        end
        CPinchFaceCinemachineCtrlMgr.Inst:ShowCam(camIndex)
    end
end

function LuaPinchFacePreviewView:OnSelectHairColorViewQnRadioBox(index)
    local data = LuaPinchFaceMgr.m_HairColorDataArr[index + 1]
    self.m_HairColorViewIndex = index
    self.FashionNameLabel.gameObject:SetActive(true)
    self.FashionNameLabel.text = data.isDefault and LocalString.GetString("默认染色") or (SafeStringFormat( LocalString.GetString("%s染色剂"), data.ColorName))
    if LuaPinchFaceMgr.m_IsInModifyMode and self.m_HairColorViewTabIndex == self.m_LeftTabIndex  then
        self.FashionNameLabel.gameObject:SetActive(false)
        self.RanFaController.gameObject:SetActive(CLuaRanFaMgr.m_MyRanFaJiId ~= index)
        self:InitRanFaController(data)
    end
    if LuaPinchFaceMgr.m_RO and not self.m_InCustomFashion and not LuaPinchFaceMgr:IsDressedFashionHair() then
        LuaPinchFaceMgr:SetAdvancedHairColorIndex(index)
    end
end

function LuaPinchFacePreviewView:OnRanFaBtnClick(data)
    if data.isDefault then
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("AdvancedRanfaji_Fade_Attention_PinchfacePreview"), function ()
            Gac2Gas.TakeOffAdvancedHairColor(CLuaRanFaMgr.m_MyRanFaJiId)
        end, nil, nil, nil, false)
        return
    end
    local id = data.ID
    g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("ChangeOrUseRanFaJi_AdvancedColor"), function()
        local curHeadFashionId = CClientMainPlayer.Inst.AppearanceProp.HeadFashionId
        if CClientMainPlayer.Inst and CClientMainPlayer.Inst.AppearanceProp.HideHeadFashionEffect <= 0 and CClientMainPlayer.Inst.AppearanceProp.HideHelmetEffect <= 0 and curHeadFashionId > 0 and not EquipmentTemplate_Equip.GetData(curHeadFashionId) then
            g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("RanFaJi_Fashion_TakeOff"), function()
                Gac2Gas.RequestTakeOffFashion(curHeadFashionId)
                Gac2Gas.TakeOnAdvancedHairColor(id)
            end, nil, nil, nil, false)
        else
            Gac2Gas.TakeOnAdvancedHairColor(id)
        end
    end, nil, nil, nil, false)
end

function LuaPinchFacePreviewView:OnChangeExpressionBtnClick()
    self.ChangeExpressionBtn.Selected = not self.ChangeExpressionBtn.Selected
    self:UpdateChangeExpressionBtnAndSlider()
end


function LuaPinchFacePreviewView:OnPauseButtonClick()
    self:PauseOrPlay(not self.m_Paused)
    if not self.m_Paused and self.Slider.m_Value >= 1 then
        self.Slider.m_Value = 0
        self.Slider:SetPercentage()
    end
end

function LuaPinchFacePreviewView:OnSliderValueChanged(value)
    if nil == self.m_AniTotalTime then return end
    self.m_DeltaTime = value * self.m_AniTotalTime
    LuaPinchFaceMgr:ShowExpressionAction(self.m_ExpressionID, value)
end

function LuaPinchFacePreviewView:OnLuoZhuangBtnClick()
    LuaPinchFaceMgr.m_IsShowLuoZhuangHair = not LuaPinchFaceMgr.m_IsShowLuoZhuangHair
    self:OnShowLuoZhuangPinchFace()
    LuaPinchFaceMgr:SetFashion(LuaPinchFaceMgr.m_FashionData, nil, LuaPinchFaceMgr.m_IsShowLuoZhuangHair)
end

function LuaPinchFacePreviewView:OnUploadBtnClick()
    CUIManager.ShowUI(CLuaUIResources.PinchFaceHubUploadWnd)
end

function LuaPinchFacePreviewView:OnHideUIBtnPress(isPressed)
    if not self.HideUIBtn.Enabled then return end
    if not self.HideUIBtn.EnablePressEffect then return end
    if isPressed and self.HideUIBtn.EnableSound then
        SoundManager.Inst:PlayOneShot(SoundManager.UISoundEvents.ButtonClick, Vector3.zero)
    end
    self.HideUIBtn:SetState(isPressed and State.Highlighted or State.Normal)
    self.HideUIBtn:SetAnimation(isPressed)
end

--@endregion UIEvent
function LuaPinchFacePreviewView:Update()
    if self.m_Paused or not self.m_AniTotalTime or not self.m_DeltaTime then return end
    if self.m_DeltaTime > self.m_AniTotalTime then
        self.m_DeltaTime = self.m_AniTotalTime
        self:PauseOrPlay(true)
    end
    self.Slider.m_Value = 0 == self.m_AniTotalTime and 0 or (self.m_DeltaTime / self.m_AniTotalTime)
    self.Slider:SetPercentage()
    
    self.m_DeltaTime = self.m_DeltaTime + Time.deltaTime
end

function LuaPinchFacePreviewView:OnEnable()
    if CPinchFaceCinemachineCtrlMgr.Inst then
        CPinchFaceCinemachineCtrlMgr.Inst:ShowCam(4)
    end
    g_ScriptEvent:AddListener("OnSetPinchFaceIKLookAtTo", self, "OnSetPinchFaceIKLookAtTo")
    g_ScriptEvent:AddListener("SyncRanFaJiPlayData", self, "OnSyncRanFaJiPlayData")
    g_ScriptEvent:AddListener("OnShowLuoZhuangPinchFace", self, "OnShowLuoZhuangPinchFace")
    self:ShowView()
end

function LuaPinchFacePreviewView:OnDisable()
    g_ScriptEvent:RemoveListener("OnSetPinchFaceIKLookAtTo", self, "OnSetPinchFaceIKLookAtTo")
    g_ScriptEvent:RemoveListener("SyncRanFaJiPlayData", self, "OnSyncRanFaJiPlayData")
    g_ScriptEvent:RemoveListener("OnShowLuoZhuangPinchFace", self, "OnShowLuoZhuangPinchFace")
    self:CancelMovieCaptureTick()
    LuaPinchFaceMgr:SetPinchEnabled(true)
    LuaPinchFaceMgr.m_IsEnableRotateRole = true
end

function LuaPinchFacePreviewView:PlayMovie(useMovieCapture)
    LuaPinchFaceMgr:PlayMovie(useMovieCapture, function()
        self.HideUIAnchor.gameObject:SetActive(true)
        self.BottomBtnsTable.gameObject:SetActive(true)
    end, function()
        self.HideUIAnchor.gameObject:SetActive(false)
        self.BottomBtnsTable.gameObject:SetActive(false)
    end)
end

function LuaPinchFacePreviewView:OnSetPinchFaceIKLookAtTo()
    self.LookAtBtn.Selected = LuaPinchFaceMgr.m_IsUseIKLookAt
end

function LuaPinchFacePreviewView:OnSyncRanFaJiPlayData()
    if self.m_LeftTabIndex == self.m_HairColorViewTabIndex then
        self.m_HairColorViewIndex = CLuaRanFaMgr.m_MyRanFaJiId
        self:OnSelectLeftTabQnRadioBox(nil, self.m_LeftTabIndex )
    end
end

function LuaPinchFacePreviewView:OnShowLuoZhuangPinchFace()
	self.LuoZhuangBtn.Selected = not LuaPinchFaceMgr.m_IsShowLuoZhuangHair
end