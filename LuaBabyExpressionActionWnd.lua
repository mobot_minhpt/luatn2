require("common/common_include")

local UIGrid = import "UIGrid"
local CUITexture = import "L10.UI.CUITexture"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CTooltip = import "L10.UI.CTooltip"
local CNearbyPlayerWndMgr = import "L10.UI.CNearbyPlayerWndMgr"

LuaBabyExpressionActionWnd = class()

RegistClassMember(LuaBabyExpressionActionWnd, "BornBabyRoot")
RegistClassMember(LuaBabyExpressionActionWnd, "OtherBabyRoot")

RegistClassMember(LuaBabyExpressionActionWnd, "ExpressionGrid_Born")
RegistClassMember(LuaBabyExpressionActionWnd, "ExpressionGrid_Other")
RegistClassMember(LuaBabyExpressionActionWnd, "ExpressionItem")

-- data
RegistClassMember(LuaBabyExpressionActionWnd, "ExpressionBabyEngineId")
RegistClassMember(LuaBabyExpressionActionWnd, "ExpressionInfoList")

RegistClassMember(LuaBabyExpressionActionWnd, "ExpressionItems")

function LuaBabyExpressionActionWnd:Init()
	self:InitClassMembers()
	self:InitValues()
end

function LuaBabyExpressionActionWnd:InitClassMembers()

	self.BornBabyRoot = self.transform:Find("Anchor/BornBabyRoot").gameObject
	self.OtherBabyRoot = self.transform:Find("Anchor/OtherBabyRoot").gameObject
	self.ExpressionItem = self.transform:Find("Anchor/ExpressionItem").gameObject
	self.ExpressionItem:SetActive(false)

	self.ExpressionGrid_Born = self.transform:Find("Anchor/BornBabyRoot/ExpressionGrid_Born"):GetComponent(typeof(UIGrid))
	self.ExpressionGrid_Other = self.transform:Find("Anchor/OtherBabyRoot/ExpressionGrid_Other"):GetComponent(typeof(UIGrid))

end

function LuaBabyExpressionActionWnd:InitValues()
	self:UpdateExpressions()
end

function LuaBabyExpressionActionWnd:UpdateExpressions()
	self.ExpressionBabyEngineId = LuaBabyMgr.m_ExpressionBabyEngineId
	self.ExpressionInfoList = {}

	for i = 0, LuaBabyMgr.m_BabyExpressionList.Count-1 do
		table.insert(self.ExpressionInfoList, LuaBabyMgr.m_BabyExpressionList[i])
	end
	LuaBabyMgr.m_BabyExpressionList = nil

	local function compare(a, b)
		if a.IsLock and not b.IsLock then
			return false
		elseif not a.IsLock and b.IsLock then
			return true
		else
			return a:GetOrder() < b:GetOrder()
		end
	end
	table.sort(self.ExpressionInfoList, compare)

	self.ExpressionItems = {}
	if #self.ExpressionInfoList <= 3 then
		self:ShowBornExpressions()
	else
		self:ShowOtherExpressions()
	end
end

function LuaBabyExpressionActionWnd:ShowBornExpressions()
	self.BornBabyRoot:SetActive(true)
	self.OtherBabyRoot:SetActive(false)

	CUICommonDef.ClearTransform(self.ExpressionGrid_Born.transform)
	CUICommonDef.ClearTransform(self.ExpressionGrid_Other.transform)

	for i = 1, #self.ExpressionInfoList do
		local go = NGUITools.AddChild(self.ExpressionGrid_Born.gameObject, self.ExpressionItem)
		self:InitExpressionItem(go, self.ExpressionInfoList[i])
		go:SetActive(true)
		table.insert(self.ExpressionItems, go)
	end
	self.ExpressionGrid_Born:Reposition()
end

function LuaBabyExpressionActionWnd:ShowOtherExpressions()
	self.BornBabyRoot:SetActive(false)
	self.OtherBabyRoot:SetActive(true)

	CUICommonDef.ClearTransform(self.ExpressionGrid_Born.transform)
	CUICommonDef.ClearTransform(self.ExpressionGrid_Other.transform)

	for i = 1, #self.ExpressionInfoList do
		local go = NGUITools.AddChild(self.ExpressionGrid_Other.gameObject, self.ExpressionItem)
		self:InitExpressionItem(go, self.ExpressionInfoList[i])
		go:SetActive(true)
		table.insert(self.ExpressionItems, go)
	end
	self.ExpressionGrid_Other:Reposition()
end

function LuaBabyExpressionActionWnd:InitExpressionItem(go, info)
	local Icon = go.transform:Find("Icon"):GetComponent(typeof(CUITexture))
	local DisableBg = go.transform:Find("Icon/DisableBg").gameObject
	local Access = go.transform:Find("Icon/Access").gameObject
	local Name = go.transform:Find("Name"):GetComponent(typeof(UILabel))

	Icon:LoadMaterial(info:GetIcon())
	Name.text = info:GetName()
	DisableBg:SetActive(info.IsLock)
	Access:SetActive(info.IsLock)

	local onExpressionItemClicked = function (go)
		self:OnExpressionItemClicked(go, info)
	end
	CommonDefs.AddOnClickListener(go, DelegateFactory.Action_GameObject(onExpressionItemClicked), false)
end

function LuaBabyExpressionActionWnd:OnExpressionItemClicked(go, info)
	if info.IsLock then
		CItemAccessListMgr.Inst:ShowItemAccessInfo(info:GetItemID(), true, go.transform, CTooltip.AlignType.Right)
	else
		if info:GetShowType() == 1 then
			-- 单人动作
			Gac2Gas.RequestBabyDoExpression(self.ExpressionBabyEngineId, info:GetID(), 0)
		elseif info:GetShowType() == 2 then
			-- 宝宝之间的动作 弹出附近的人的界面
			local hint = SafeStringFormat3(LocalString.GetString("请选择目标宝宝进行%s动作"), info:GetName())
			CNearbyPlayerWndMgr.ShowWnd(hint, info:GetName(), DelegateFactory.Action_ulong(function (otherEngineId)
				Gac2Gas.RequestBabyDoExpression(self.ExpressionBabyEngineId, info:GetID(), otherEngineId)
			end), info:GetStatus())
		end
	end
end

function LuaBabyExpressionActionWnd:RefreshExpressions(args)
	local expressionId = args[0]
	local engineId = args[1]

	if engineId == self.ExpressionBabyEngineId then
		for i = 1, #self.ExpressionItems do
			self:UpdateExpressionItem(self.ExpressionItems[i], self.ExpressionInfoList[i])
		end
	end
end

function LuaBabyExpressionActionWnd:UpdateExpressionItem(go, info)
	local Icon = go.transform:Find("Icon"):GetComponent(typeof(CUITexture))
	local Name = go.transform:Find("Name"):GetComponent(typeof(UILabel))
	Icon:LoadMaterial(info:GetIcon())
	Name.text = info:GetName()
end

function LuaBabyExpressionActionWnd:OnActiveBabyExpressionSuccess(babyId)
	Gac2Gas.QueryBabyUnLockExpressionGroupInfo(self.ExpressionBabyEngineId)
end

function LuaBabyExpressionActionWnd:OnEnable()
	g_ScriptEvent:AddListener("ActiveBabyExpressionSuccess", self, "OnActiveBabyExpressionSuccess")
	g_ScriptEvent:AddListener("UpdateBabyExpressionUnlockGroup", self, "UpdateExpressions")
	g_ScriptEvent:AddListener("BabyExpressionActionChange", self, "RefreshExpressions")
	
end

function LuaBabyExpressionActionWnd:OnDisable()
	g_ScriptEvent:RemoveListener("ActiveBabyExpressionSuccess", self, "OnActiveBabyExpressionSuccess")
	g_ScriptEvent:RemoveListener("UpdateBabyExpressionUnlockGroup", self, "UpdateExpressions")
	g_ScriptEvent:RemoveListener("BabyExpressionActionChange", self, "RefreshExpressions")
end

return LuaBabyExpressionActionWnd
