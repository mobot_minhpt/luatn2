-- Auto Generated!!
local BoolDelegate = import "UIEventListener+BoolDelegate"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CDuoMaoMaoMgr = import "L10.Game.CDuoMaoMaoMgr"
local CGuaJiMgr = import "L10.Game.CGuaJiMgr"
local CGuanNingMgr = import "L10.Game.CGuanNingMgr"
local UIProgressBar = import "UIProgressBar"
local CLogMgr = import "L10.CLogMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local Constants = import "L10.Game.Constants"
local COpenEntryMgr = import "L10.Game.COpenEntryMgr"
local CRightMenuWnd = import "L10.UI.CRightMenuWnd"
local CScene = import "L10.Game.CScene"
local CSkillButtonBoardWnd = import "L10.UI.CSkillButtonBoardWnd"
local CSkillButtonInfo = import "L10.UI.CSkillButtonInfo"
local CSnowBallMgr = import "L10.Game.CSnowBallMgr"
local CStatusMgr = import "L10.Game.CStatusMgr"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local CYuanDanMgr = import "L10.Game.CYuanDanMgr"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EPropStatus = import "L10.Game.EPropStatus"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local Gac2Gas = import "L10.Game.Gac2Gas"
local Gameplay_Gameplay = import "L10.Game.Gameplay_Gameplay"
local L10 = import "L10"
local LocalString = import "LocalString"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
local Skill_DoubleJoystick = import "L10.Game.Skill_DoubleJoystick"
--local Skill_TempSkill = import "L10.Game.Skill_TempSkill"
local SoundManager = import "SoundManager"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local UIPanel = import "UIPanel"
local UnityEngine = import "UnityEngine"
local Vector2 = import "UnityEngine.Vector2"
local Vector3 = import "UnityEngine.Vector3"
local VectorDelegate = import "UIEventListener+VectorDelegate"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local SkillCategory = import "L10.Game.SkillCategory"
local CUIFx = import "L10.UI.CUIFx"
local CPUBGMgr = import "L10.Game.CPUBGMgr"
local TweenAlpha = import "TweenAlpha"
local TweenScale = import "TweenScale"
local CPropertySkill = import "L10.Game.CPropertySkill"
local FXClientInfoReporter = import "L10.Game.FXClientInfoReporter"

local OnXianZhiBtnClicked = function (this, go)
    if CClientMainPlayer.Inst == nil or CClientMainPlayer.Inst.IsDie then
        return
    end
    SoundManager.Inst:PlayOneShot(SoundManager.UISoundEvents.SkillButton, Vector3.zero, nil, 0)
    local skillButtonInfo = CommonDefs.GetComponent_GameObject_Type(go, typeof(CSkillButtonInfo))
    if not skillButtonInfo.ShowDoubleStick then
        local xianZhiSkillId =  CLuaXianzhiMgr.GetXianZhiSKillId()
        CClientMainPlayer.Inst:TryCastSkill(CClientMainPlayer.Inst.SkillProp:GetAlternativeSkillIdWithDelta(xianZhiSkillId, CClientMainPlayer.Inst.Level), true, Vector2.zero)
    end

end

--!非技能按钮必须放在最后一个
CSkillButtonBoardWnd.m_Awake_CS2LuaHook = function (this) 
    if CSkillButtonBoardWnd.Instance ~= nil then
        UnityEngine.Object.Destroy(this)
    end
    CSkillButtonBoardWnd.Instance = this
    if this.activeSkillBtns1.Length ~= CPropertySkill.ActiveSkillCount or this.activeSkillBtns2.Length ~= CPropertySkill.ActiveSkillCount then
        L10.CLogMgr.LogError("skill button config error")
        CSkillButtonBoardWnd.Instance = nil
        return
    end

    do
        local i = 0
        while i < CPropertySkill.ActiveSkillCount do
            this.activeSkillBtns1[i].IsSkillButton = true
            UIEventListener.Get(this.activeSkillBtns1[i].gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.activeSkillBtns1[i].gameObject).onClick, MakeDelegateFromCSFunction(this.OnActiveSkillButtonClick, VoidDelegate, this), true)
            UIEventListener.Get(this.activeSkillBtns1[i].gameObject).onDrag = CommonDefs.CombineListner_VectorDelegate(UIEventListener.Get(this.activeSkillBtns1[i].gameObject).onDrag, MakeDelegateFromCSFunction(this.OnDrag, VectorDelegate, this), true)
            UIEventListener.Get(this.activeSkillBtns1[i].gameObject).onPress = CommonDefs.CombineListner_BoolDelegate(UIEventListener.Get(this.activeSkillBtns1[i].gameObject).onPress, MakeDelegateFromCSFunction(this.OnPress, BoolDelegate, this), true)
            this.activeSkillBtns2[i].IsSkillButton = true
            UIEventListener.Get(this.activeSkillBtns2[i].gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.activeSkillBtns2[i].gameObject).onClick, MakeDelegateFromCSFunction(this.OnActiveSkillButtonClick, VoidDelegate, this), true)
            UIEventListener.Get(this.activeSkillBtns2[i].gameObject).onDrag = CommonDefs.CombineListner_VectorDelegate(UIEventListener.Get(this.activeSkillBtns2[i].gameObject).onDrag, MakeDelegateFromCSFunction(this.OnDrag, VectorDelegate, this), true)
            UIEventListener.Get(this.activeSkillBtns2[i].gameObject).onPress = CommonDefs.CombineListner_BoolDelegate(UIEventListener.Get(this.activeSkillBtns2[i].gameObject).onPress, MakeDelegateFromCSFunction(this.OnPress, BoolDelegate, this), true)
            i = i + 1
        end
    end

    this.jueji70ButtonInfo.IsSkillButton = true
    UIEventListener.Get(this.jueji70ButtonInfo.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.jueji70ButtonInfo.gameObject).onClick, MakeDelegateFromCSFunction(this.OnJueJiSkillButtonClick, VoidDelegate, this), true)
    this.jueji100ButtonInfo.IsSkillButton = true
    UIEventListener.Get(this.jueji100ButtonInfo.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.jueji100ButtonInfo.gameObject).onClick, MakeDelegateFromCSFunction(this.OnJueJiSkillButtonClick, VoidDelegate, this), true)

    for i = 0, this.additionalButtonInfos.Length-1 do
        UIEventListener.Get(this.additionalButtonInfos[i].gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.additionalButtonInfos[i].gameObject).onClick, MakeDelegateFromCSFunction(this.OnAdditonalSkillButtonClick, VoidDelegate, this), true)
    end

    UIEventListener.Get(this.switchTargetBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.switchTargetBtn).onClick, MakeDelegateFromCSFunction(this.OnSwitchTargetButtonClick, VoidDelegate, this), true)

    UIEventListener.Get(this.guajiBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.guajiBtn).onClick, MakeDelegateFromCSFunction(this.OnGuaJiButtonClick, VoidDelegate, this), true)

    CommonDefs.AddOnClickListener(this.XianZhiBtn.gameObject, DelegateFactory.Action_GameObject(function (go)
        OnXianZhiBtnClicked(this, go)
    end), false)

    CommonDefs.AddOnClickListener(this.YanHuaViewBtn, DelegateFactory.Action_GameObject(function (go)
        --OnYanHuaViewBtnClicked(this, go)
        CLuaYanHuaEditorMgr.OnYanHuaViewBtnClicked()
    end), false)
    UIEventListener.Get(this.GuanNingFightingSpiritButton).onClick = DelegateFactory.VoidDelegate(function (go)
        CLuaGuanNingMgr:RequestPerformGnjcZhanYi()
	end)
    this.yinglingSkillRoot:SetActive(false)
    this.GuanNingFightingSpiritButton:SetActive(false)

    UIEventListener.Get(this.jumpBtn).onClick = DelegateFactory.VoidDelegate(function()
        if CClientMainPlayer.Inst then
            CClientMainPlayer.Inst:DoJump()
        end
    end)
end
CSkillButtonBoardWnd.m_OnEnable_CS2LuaHook = function (this) 
    CLuaSkillButtonBoardWnd:OnEnable(this)
    this:IPhoneXAdaptation()
    this:UpdateDrugVisible()
    this:OnGuaJiStatusChange()
    this:InitCurActiveSkillSuite()

    this:OnSkillsInfoInit()
    --订阅消息
    EventManager.AddListener(EnumEventType.MainPlayerCreated, MakeDelegateFromCSFunction(this.OnMainPlayerCreated, Action0, this))
    EventManager.AddListener(EnumEventType.MainPlayerSkillPropUpdate, MakeDelegateFromCSFunction(this.OnSkillsInfoInit, Action0, this))
    EventManager.AddListener(EnumEventType.GuaJiStatusChanged, MakeDelegateFromCSFunction(this.OnGuaJiStatusChange, Action0, this))
    EventManager.AddListener(EnumEventType.DirectShowJueJiSettingChanged, MakeDelegateFromCSFunction(this.OnDirectShowJueJiSettingChanged, Action0, this))
    EventManager.AddListener(EnumEventType.EnterNewbiePlay, MakeDelegateFromCSFunction(this.OnDirectShowJueJiSettingChanged, Action0, this))
    EventManager.AddListener(EnumEventType.LeaveNewbiePlay, MakeDelegateFromCSFunction(this.OnDirectShowJueJiSettingChanged, Action0, this))
    EventManager.AddListener(EnumEventType.OnMainPlayerActiveSkillSuiteUpdate, MakeDelegateFromCSFunction(this.OnMainPlayerActiveSkillSuiteUpdate, Action0, this))
    EventManager.AddListener(EnumEventType.MainPlayerSkillSuiteOpenStatusUpdate, MakeDelegateFromCSFunction(this.OnMainPlayerSkillSuiteOpenStatusUpdate, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.SyncSeekSkillCount, MakeDelegateFromCSFunction(this.OnSyncSeekSkillPoint, MakeGenericClass(Action1, UInt32), this))

    LuaGamePlayMgr:UpdateSkillButtonBoardState()
end
CSkillButtonBoardWnd.m_IPhoneXAdaptation_CS2LuaHook = function (this) 
end
CSkillButtonBoardWnd.m_OnDisable_CS2LuaHook = function (this)
    CLuaSkillButtonBoardWnd:OnDisable() 
    --取消订阅消息
    EventManager.RemoveListener(EnumEventType.MainPlayerCreated, MakeDelegateFromCSFunction(this.OnMainPlayerCreated, Action0, this))
    EventManager.RemoveListener(EnumEventType.MainPlayerSkillPropUpdate, MakeDelegateFromCSFunction(this.OnSkillsInfoInit, Action0, this))
    EventManager.RemoveListener(EnumEventType.GuaJiStatusChanged, MakeDelegateFromCSFunction(this.OnGuaJiStatusChange, Action0, this))
    EventManager.RemoveListener(EnumEventType.DirectShowJueJiSettingChanged, MakeDelegateFromCSFunction(this.OnDirectShowJueJiSettingChanged, Action0, this))
    EventManager.RemoveListener(EnumEventType.EnterNewbiePlay, MakeDelegateFromCSFunction(this.OnDirectShowJueJiSettingChanged, Action0, this))
    EventManager.RemoveListener(EnumEventType.LeaveNewbiePlay, MakeDelegateFromCSFunction(this.OnDirectShowJueJiSettingChanged, Action0, this))
    EventManager.RemoveListener(EnumEventType.OnMainPlayerActiveSkillSuiteUpdate, MakeDelegateFromCSFunction(this.OnMainPlayerActiveSkillSuiteUpdate, Action0, this))
    EventManager.RemoveListener(EnumEventType.MainPlayerSkillSuiteOpenStatusUpdate, MakeDelegateFromCSFunction(this.OnMainPlayerSkillSuiteOpenStatusUpdate, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.SyncSeekSkillCount, MakeDelegateFromCSFunction(this.OnSyncSeekSkillPoint, MakeGenericClass(Action1, UInt32), this))
end
CSkillButtonBoardWnd.m_InitAdditionalSkill_CS2LuaHook = function (this)
    if CScene.MainScene == nil then return end

    -- 清空并隐藏
    for i = 0, CSkillButtonBoardWnd.MAX_ADDITIONAL_SKILL_COUNT - 1 do
        this.additionalButtonInfos[i].gameObject:SetActive(false)
        this.m_AdditionalSkillIds[i] = SkillButtonSkillType_lua.NoSkill
        if this.additionalButtonInfos[i].GuideCircle then
            this.additionalButtonInfos[i].GuideCircle.gameObject:SetActive(false)
        end
    end

    -- 1-2是挂机位置  3-7是默认位置  8-9是绝技位置
    local newSkills1, newSkill1Counter = CLuaSkillButtonBoardWnd:SubAdditionalSkills(1, 2)
    local newSkills2, newSkill2Counter = CLuaSkillButtonBoardWnd:SubAdditionalSkills(3, 7)
    local newSkills3, newSkill3Counter = CLuaSkillButtonBoardWnd:SubAdditionalSkills(8, 9)

    local gamePlayId = CScene.MainScene.GamePlayDesignId
    if gamePlayId == 51102436 then--#refs 169385 【周年庆+葫芦娃】如意技能特殊处理 新做一个假的临时技能界面 LuaRuYiAdditionSkillWnd
        newSkills1 = {}
    end

    -- 默认位置或绝技位置上有临时技能，隐藏普通技能+绝技
    if newSkill2Counter ~= 0 or newSkill3Counter ~= 0 then
        this.ordinarySkillRoot:SetActive(false)
    end

    -- 仙职技能和烟花按钮的处理
    this.YanHuaViewBtn:SetActive(CLuaYanHuaEditorMgr.IsShowViewBtn)
    if newSkill1Counter > 0 or LuaLuoCha2021Mgr:IsInGamePlay() then
        this.XianZhiBtn.gameObject:SetActive(false)
        CLuaSkillButtonBoardWnd:SetYanHuaViewBtnPos(newSkill1Counter+1)
    else
        CLuaSkillButtonBoardWnd:InitXianZhiBtnShow()
    end

    -- 调整默认临时按钮位置
    if CSkillButtonBoardWnd.s_EnableAdjustTmpSkillLocation then
        for i = 0, #newSkills2 - 1 do
            this.additionalButtonInfos[i].transform.localPosition = this:GetTmpSkillLocation(i, #newSkills2)
        end
    end

    CLuaSkillButtonBoardWnd:InitAdditionalButtonInfos(newSkills2, 0)
    CLuaSkillButtonBoardWnd:InitAdditionalButtonInfos(newSkills1, 5)
    CLuaSkillButtonBoardWnd:InitAdditionalButtonInfos(newSkills3, 7)

    -- 针对吃鸡玩法的技能设置
    local skillSettingBtn = this.transform:Find("Anchor/Offset/SkillSettingBtn").gameObject
    if skillSettingBtn then
        if CPUBGMgr.Inst:IsInChiJi() then
            skillSettingBtn:SetActive(true)
            LuaPUBGMgr.RequstSyncTeamInfo()
            CommonDefs.AddOnClickListener(skillSettingBtn, DelegateFactory.Action_GameObject(function (go)
                -- 打开临时技能界面
                CUIManager.ShowUI(CLuaUIResources.PUBGSkillSetWnd)
            end), false)
        else
            skillSettingBtn:SetActive(false)
        end
    end
    g_ScriptEvent:BroadcastInLua("InitAdditionalSkill",this)
end
CSkillButtonBoardWnd.m_GetTmpSkillLocation_CS2LuaHook = function (this, pos, tmpSkillCount) 
    if tmpSkillCount <= 0 or tmpSkillCount > 5 then
        return Vector3.zero
    end
    local posArr = this.tmpSkillLocationArray[tmpSkillCount - 1]
    if pos < 0 or pos >= posArr.Length then
        return Vector3.zero
    end
    return posArr[pos]
end
CSkillButtonBoardWnd.m_Init_CS2LuaHook = function (this) 
    if CUIManager.IsLoaded(CUIResources.RightMenuWnd) and CRightMenuWnd.Instance ~= nil and CRightMenuWnd.Instance.IsExpanded then
        CSkillButtonBoardWnd.Hide(false)
    end
    this.GuanNingFightingSpiritButton:SetActive(CLuaGuanNingMgr.m_IsInGuanNingWeekendNewModePlay and CLuaGuanNingMgr.m_IsSundayPlayOpen and CGuanNingMgr.Inst.inGnjc)
end
CSkillButtonBoardWnd.m_Show_CS2LuaHook = function (animated) 
    if CSkillButtonBoardWnd.Instance ~= nil then
        if animated then
            LuaTweenUtils.DOLocalMoveX(CSkillButtonBoardWnd.Instance.aniRoot.transform, CSkillButtonBoardWnd.OriginX, Constants.GeneralAnimationDuration, false)
        else
            Extensions.SetLocalPositionX(CSkillButtonBoardWnd.Instance.aniRoot.transform, CSkillButtonBoardWnd.OriginX)
        end
        g_ScriptEvent:BroadcastInLua("CSkillButtonBoardWnd_Show")
    end
end
CSkillButtonBoardWnd.m_Hide_CS2LuaHook = function (animated) 
    if CSkillButtonBoardWnd.Instance ~= nil then
        if animated then
            LuaTweenUtils.DOLocalMoveX(CSkillButtonBoardWnd.Instance.aniRoot.transform, CSkillButtonBoardWnd.HiddenX, Constants.GeneralAnimationDuration, false)
        else
            Extensions.SetLocalPositionX(CSkillButtonBoardWnd.Instance.aniRoot.transform, CSkillButtonBoardWnd.HiddenX)
        end
    end
end
CSkillButtonBoardWnd.m_OnMainPlayerCreated_CS2LuaHook = function (this) 
    this:UpdateDrugVisible()
    this:OnGuaJiStatusChange()
    this:InitCurActiveSkillSuite()

    this:OnSkillsInfoInit()

    this:CheckDisplay()

    if CScene.MainScene and CScene.MainScene:Is3DScene() then
        this.jumpBtn:SetActive(true)
    else
        this.jumpBtn:SetActive(false)
    end
end
CSkillButtonBoardWnd.m_CheckDisplay_CS2LuaHook = function (this) 
    if CScene.MainScene == nil then
        return
    end
    local go = this.transform:Find("Anchor").gameObject
    if go == nil then
        return
    end
    --新年音乐会,海底梦泽特殊处理一下
    if CYuanDanMgr.Inst.IsInConcertPlay or CScene.MainScene.SceneTemplateId == 16102001 then
        go:SetActive(false)
        return
    end

    local playId = CScene.MainScene.GamePlayDesignId
    local data = Gameplay_Gameplay.GetData(playId)
    if data ~= nil then
		local hasHideTask = false
		local hideTasks = data.HideSkillBoardTasks
		if CClientMainPlayer.Inst ~= nil and hideTasks and hideTasks.Length > 0 then
			for i = 0, hideTasks.Length - 1 do 
				if CommonDefs.DictContains(CClientMainPlayer.Inst.TaskProp.CurrentTasks, typeof(UInt32), hideTasks[i]) then
					hasHideTask = true
					break
				end
			end
		end

		if (hasHideTask or data.HideSkillBoard > 0) and go.activeSelf then
			go:SetActive(false)
		elseif (not hasHideTask) and data.HideSkillBoard == 0 and not go.activeSelf then
			go:SetActive(true)
		end
    else
        if not go.activeSelf then
            go:SetActive(true)
        end
    end
end
CSkillButtonBoardWnd.m_InitCurActiveSkillSuite_CS2LuaHook = function (this) 
    if CClientMainPlayer.Inst ~= nil then
        local property = CClientMainPlayer.Inst.SkillProp
        if property.CurrentActiveSkillSuite == 1 then
            this.activeSkillRoot1.transform.localScale = Vector3.one
            this.activeSkillRoot2.transform.localScale = Vector3.zero
            CommonDefs.GetComponent_GameObject_Type(this.activeSkillRoot1, typeof(UIPanel)).alpha = 1
            CommonDefs.GetComponent_GameObject_Type(this.activeSkillRoot2, typeof(UIPanel)).alpha = 0
        else
            this.activeSkillRoot1.transform.localScale = Vector3.zero
            this.activeSkillRoot2.transform.localScale = Vector3.one
            CommonDefs.GetComponent_GameObject_Type(this.activeSkillRoot1, typeof(UIPanel)).alpha = 0
            CommonDefs.GetComponent_GameObject_Type(this.activeSkillRoot2, typeof(UIPanel)).alpha = 1
        end
    end
    this.canChangeSkillSuite = true
end

CSkillButtonBoardWnd.m_OnSkillsInfoInit_CS2LuaHook = function (this) 
    if CClientMainPlayer.Inst == nil then
        return
    end
    local mainplayer = CClientMainPlayer.Inst
    this:RefreshSkillButtonVisibility()

    this:InitActiveSkillSuite(1)
    this:InitActiveSkillSuite(2)

    --初始化绝技
    this._70SkillClass = mainplayer.SkillProp:GetAlternativeSkillCls(mainplayer.FirstJuejiSkillCls)
    this._100SkillClass = mainplayer.SkillProp:GetAlternativeSkillCls(mainplayer.SecondJuejiSkillCls)

    this.jueji70ButtonInfo:LoadIcon(nil, 0, false, true)
    this.jueji100ButtonInfo:LoadIcon(nil, 0, false, true)
    this.jueji70ButtonInfo.AdditionalIconlVisible = true
    this.jueji100ButtonInfo.AdditionalIconlVisible = true

    this.juejiSkillExist = false

    local skillIdWithDelta = mainplayer.SkillProp:GetSkillIdWithDeltaByCls(this._70SkillClass, mainplayer.Level)
    if Skill_AllSkills.Exists(skillIdWithDelta) then
        local template = Skill_AllSkills.GetData(skillIdWithDelta)
        --已经解锁70绝技

        this.jueji70ButtonInfo:LoadIcon(LuaSkillMgr:GetOriginOrReplaceSkillIcon(template), skillIdWithDelta, false, true)
        this.jueji70ButtonInfo.AdditionalIconlVisible = false

        this.juejiSkillExist = true
    end
    skillIdWithDelta = mainplayer.SkillProp:GetSkillIdWithDeltaByCls(this._100SkillClass, mainplayer.Level)
    if Skill_AllSkills.Exists(skillIdWithDelta) then
        local template = Skill_AllSkills.GetData(skillIdWithDelta)
        --已经解锁100绝技

        this.jueji100ButtonInfo:LoadIcon(LuaSkillMgr:GetOriginOrReplaceSkillIcon(template), skillIdWithDelta, false, true)
        this.jueji100ButtonInfo.AdditionalIconlVisible = false

        this.juejiSkillExist = true
    end
    this.ordinarySkillRoot:SetActive(true) --暂时提早到这里，保持和优化前时序一致，在InitAdditionalSkill和LuaLuoCha2021Mgr:IsInGamePlay()判断中都可能隐藏它
    this:OnUpdateCooldown()
    this:InitAdditionalSkill()

    if LuaLuoCha2021Mgr:IsInGamePlay() then
        this.ordinarySkillRoot:SetActive(false)
    end

    if LuaZhaiXingMgr:InZhaiXing() or LuaLuoCha2021Mgr:IsInGamePlay() then
        this.switchTargetBtn.gameObject:SetActive(false)
    else
        this.switchTargetBtn.gameObject:SetActive(true)
    end
    this:OnHideParts()

    LuaGamePlayMgr:UpdateSkillButtonBoardState()
end
CSkillButtonBoardWnd.m_OnHideParts_CS2LuaHook = function (this) 
    if CDuoMaoMaoMgr.Inst.InHideAndSeek or CSnowBallMgr.Inst:IsInSnowBallFight() or LuaCuJuMgr:IsInCuJu() then
        this:SetHideAndSeek()
    end
end
CSkillButtonBoardWnd.m_SetHideAndSeek_CS2LuaHook = function (this) 
    this.switchTargetBtn.gameObject:SetActive(false)
    this.hpRecoverButtonView.gameObject:SetActive(false)
end
CSkillButtonBoardWnd.m_InitActiveSkillSuite_CS2LuaHook = function (this, suite) 
    if suite ~= 1 and suite ~= 2 then
        return
    end
    local mainplayer = CClientMainPlayer.Inst
    if mainplayer == nil then
        return
    end

    local skillProp = mainplayer.SkillProp
    local skillIds = (suite == 1) and skillProp.NewActiveSkill or skillProp.NewActiveSkill2
    local curActiveSkillButtonInfos = (suite == 1) and this.activeSkillBtns1 or this.activeSkillBtns2
    --skillIds的索引是从1开始的，skillButtonInfos的索引是从0开始的
    local n = skillIds.Length
    for i=1, n-1 do
        local buttonInfo = curActiveSkillButtonInfos[i - 1]
        buttonInfo.ActiveSkillIdx = i
        local skillId = skillProp:GetAlternativeSkillIdWithDelta(skillIds[i], mainplayer.Level)
        --对潜在的被替换技能进行处理
        if skillId == SkillButtonSkillType_lua.NoSkill or not Skill_AllSkills.Exists(skillId) then
            buttonInfo:LoadIcon(nil, 0, false, true)
            buttonInfo.gameObject:SetActive(false)
        else
            local visible = COpenEntryMgr.Inst:IsSkillButtonVisible(i-1)
            buttonInfo.gameObject:SetActive(visible)
            buttonInfo:LoadIcon(LuaSkillMgr:GetOriginOrReplaceSkillIcon(Skill_AllSkills.GetData(skillId)), skillId, false, true)
        end
        buttonInfo:HideDoubleStickCircle()
    end
end
CSkillButtonBoardWnd.m_OnJueJiSkillButtonClick_CS2LuaHook = function (this, go) 

    if CClientMainPlayer.Inst == nil or CClientMainPlayer.Inst.IsDie then
        return
    end

    SoundManager.Inst:PlayOneShot(SoundManager.UISoundEvents.SkillButton, Vector3.zero, nil, 0)
    if go == this.jueji70ButtonInfo.gameObject then
        this:CastJueJi(this._70SkillClass)
        CLuaSkillButtonBoardWnd:OnJuejiButtonClick(this.jueji70ButtonInfo, this._70SkillClass)
        return
    elseif go == this.jueji100ButtonInfo.gameObject then
        this:CastJueJi(this._100SkillClass)
        CLuaSkillButtonBoardWnd:OnJuejiButtonClick(this.jueji100ButtonInfo, this._100SkillClass)
        return
    end
end
CSkillButtonBoardWnd.m_OnAdditonalSkillButtonClick_CS2LuaHook = function (this, go) 
    local mainplayer = CClientMainPlayer.Inst
    if mainplayer == nil or mainplayer.IsDie then
        return
    end

    SoundManager.Inst:PlayOneShot(SoundManager.UISoundEvents.SkillButton, Vector3.zero, nil, 0)
    local skillButtonInfo = CommonDefs.GetComponent_GameObject_Type(go, typeof(CSkillButtonInfo))
    for i = 0, CSkillButtonBoardWnd.MAX_ADDITIONAL_SKILL_COUNT - 1 do
        if this.additionalButtonInfos[i] == skillButtonInfo then
            if not skillButtonInfo.ShowDoubleStick then
                local skillId = mainplayer.SkillProp:GetAlternativeSkillIdWithDelta(this.m_AdditionalSkillIds[i], mainplayer.Level)
                mainplayer:TryCastSkill(skillId, true, Vector2.zero)
                FXClientInfoReporter.Inst:ReportCastTempSkill(skillId, "buttonclick")
            end
            break
        end
    end
end

CSkillButtonBoardWnd.m_OnActiveSkillButtonClick_CS2LuaHook = function (this, go) 
    
    local mainplayer = CClientMainPlayer.Inst

    if mainplayer == nil or mainplayer.IsDie then
        return
    end

    SoundManager.Inst:PlayOneShot(SoundManager.UISoundEvents.SkillButton, Vector3.zero, nil, 0)
    local skillButtonInfo = CommonDefs.GetComponent_GameObject_Type(go, typeof(CSkillButtonInfo))
    local default
    if mainplayer.SkillProp.CurrentActiveSkillSuite == 1 then
        default = this.activeSkillBtns1
    else
        default = this.activeSkillBtns2
    end
    local btns = default
    do
        local i = 0
        while i < CPropertySkill.ActiveSkillCount do
            if btns[i] == skillButtonInfo then
                if not skillButtonInfo.ShowDoubleStick then
                    local activeSkills = mainplayer.SkillProp:GetCurrentActiveSkill()
                    local id = activeSkills[this.activeSkillBtns1[i].ActiveSkillIdx]
                    CLuaSkillButtonBoardWnd:CastSkill(id, true, Vector2.zero)
                end
                break
            end
            i = i + 1
        end
    end
end
CSkillButtonBoardWnd.m_OnPress_CS2LuaHook = function (this, go, isPressed) 
    this.startDrag = isPressed
    CSkillButtonBoardWnd.DragParam = 0
end
CSkillButtonBoardWnd.m_OnDrag_CS2LuaHook = function (this, go, delta) 
    if CClientMainPlayer.Inst == nil then
        return
    end
    local skillButtonInfo = CommonDefs.GetComponent_GameObject_Type(go, typeof(CSkillButtonInfo))
    local default
    if CClientMainPlayer.Inst.SkillProp.CurrentActiveSkillSuite == 1 then
        default = this.activeSkillBtns1
    else
        default = this.activeSkillBtns2
    end
    local btns = default
    do
        local i = 0
        while i < CPropertySkill.ActiveSkillCount do
            if btns[i] == skillButtonInfo then
                if skillButtonInfo.ShowDoubleStick then
                    return
                end
            end
            i = i + 1
        end
    end
    if not (delta.x < 0 and delta.y < 0) and not (delta.x > 0 and delta.y > 0) then
        return
    end

    if not this.canChangeSkillSuite then
        return
    end

    if CSkillButtonBoardWnd.ActiveDelayTrigger then
        if not this.startDrag then
            return
        end
        local p = math.abs(delta.x) + math.abs(delta.y)
        CSkillButtonBoardWnd.DragParam = CSkillButtonBoardWnd.DragParam + p
        if CSkillButtonBoardWnd.DragParam < CSkillButtonBoardWnd.DragTriggerDelta then
            return
        end
        CSkillButtonBoardWnd.DragParam = 0
        this.startDrag = false
    end

    local property = CClientMainPlayer.Inst.SkillProp
    if property.IsSkillSuite2Opened == 0 then
        return
    end
    if property.CurrentActiveSkillSuite == 1 then
        Gac2Gas.SwitchActiveSkillSuite(2)
    else
        Gac2Gas.SwitchActiveSkillSuite(1)
    end
    this.canChangeSkillSuite = false
end
CSkillButtonBoardWnd.m_OnMainPlayerSkillSuiteOpenStatusUpdate_CS2LuaHook = function (this) 
    this:InitCurActiveSkillSuite()
end
CSkillButtonBoardWnd.m_OnSyncSeekSkillPoint_CS2LuaHook = function (this, seekSkillPoint) 
    if seekSkillPoint > 0 then
        this.skillLastCountLabel.gameObject:SetActive(true)
        this.skillLastCountLabel.text = tostring(seekSkillPoint)
    else
        this.skillLastCountLabel.gameObject:SetActive(false)
    end
end
CSkillButtonBoardWnd.m_CastJueJi_CS2LuaHook = function (this, juejiCls) 

    if CClientMainPlayer.Inst == nil then
        return
    end
    local skillId = System.UInt32.MaxValue
    if CClientMainPlayer.Inst.SkillProp:IsSkillClsExist(juejiCls) then
        skillId = CClientMainPlayer.Inst.SkillProp:GetSkillIdWithDeltaByCls(juejiCls, CClientMainPlayer.Inst.Level)
        CClientMainPlayer.Inst:TryCastSkill(skillId, true, Vector2.zero)
    elseif juejiCls == CClientMainPlayer.Inst.FirstJuejiSkillCls or juejiCls == CClientMainPlayer.Inst.SecondJuejiSkillCls then
        local skill = Skill_AllSkills.GetData(CLuaDesignMgr.Skill_AllSkills_GetSkillId(juejiCls, 1))
        if skill ~= nil then
            g_MessageMgr:ShowMessage("NotLearn_JueJi_Tips", skill.Name)
        end
    end
end
CSkillButtonBoardWnd.m_OnSwitchTargetButtonClick_CS2LuaHook = function (this, go) 
    if CClientMainPlayer.Inst ~= nil then
        CClientMainPlayer.Inst:SwitchTarget()
    end
end
CSkillButtonBoardWnd.m_OnGuaJiButtonClick_CS2LuaHook = function (this, go) 
    if CClientMainPlayer.Inst ~= nil then
        -- 
        if CStatusMgr.Inst:GetStatus(CClientMainPlayer.Inst, EPropStatus.GetId("GuaJi")) == 0 then
            CGuaJiMgr.Inst:StartGuaJi()
        else
            CGuaJiMgr.Inst:StopGuaJi()
        end
    end
end
CSkillButtonBoardWnd.m_OnGuaJiStatusChange_CS2LuaHook = function (this) 
    --从RefreshSkillButtonVisibility搬运到OnGuaJiStatusChange，技能刷新的时候和这里无关
    if L10.Game.Guide.CGuideMgr.Inst:IsNewbiePlayId() or CGuanNingMgr.Inst.inGnjc or CLuaStarBiwuMgr:IsInStarBiwu() or LuaZhaiXingMgr:InZhaiXing() or LuaLuoCha2021Mgr:IsInGamePlay() or LuaWeddingIterationMgr:IsInGamePlay() then
        this.guajiBtn:SetActive(false)
        return
    else
        local gamePlayId = CScene.MainScene and CScene.MainScene.GamePlayDesignId or 0
        if Gameplay_Gameplay.Exists(gamePlayId) and Gameplay_Gameplay.GetData(gamePlayId).HideGuaJiButton > 0 then
            this.guajiBtn:SetActive(false)
            return
        end
        this.guajiBtn:SetActive(true)
    end
    if CStatusMgr.Inst:GetStatus(CClientMainPlayer.Inst, EPropStatus.GetId("GuaJi")) == 0 then
        this.guajiEnabledSprite.gameObject:SetActive(true)
        this.guajiDisabledSprite.gameObject:SetActive(false)
    else
        this.guajiEnabledSprite.gameObject:SetActive(false)
        this.guajiDisabledSprite.gameObject:SetActive(true)
    end
end
CSkillButtonBoardWnd.m_OnDirectShowJueJiSettingChanged_CS2LuaHook = function (this) 
    this:OnSkillsInfoInit()
end
CSkillButtonBoardWnd.m_WinShortCutKeyCastSkill_CS2LuaHook = function (this, buttonIndex) 
    local mainplayer = CClientMainPlayer.Inst
    if CSkillButtonBoardWnd.Instance == nil or mainplayer == nil then
        return
    end

    if buttonIndex == 6 or buttonIndex == 7 then
        local index = buttonIndex - 1
        if this.m_AdditionalSkillIds.Length > index and Skill_AllSkills.Exists(this.m_AdditionalSkillIds[index]) then
            local skillId = mainplayer.SkillProp:GetAlternativeSkillIdWithDelta(this.m_AdditionalSkillIds[index], mainplayer.Level)
            if Skill_DoubleJoystick.Exists(CLuaDesignMgr.Skill_AllSkills_GetClass(this.m_AdditionalSkillIds[index])) and this.additionalButtonInfos.Length > index and this.additionalButtonInfos[index] ~= nil and this.additionalButtonInfos[index].ShowDoubleStick then
                local pos = this.additionalButtonInfos[index]:GetDoubleStickInitPos()
                mainplayer:TryCastSkill(skillId, false, pos)
            else
                mainplayer:TryCastSkill(skillId, true, Vector2.zero)
            end
            FXClientInfoReporter.Inst:ReportCastTempSkill(skillId, "winshortcutkey")
        end
    else
        local skillId = System.UInt32.MaxValue
        --正式的或者临时的技能
        local bFoundAdditionalSkill = this:ExistAdditionalSkillAtCommonSkillPlace()
        if bFoundAdditionalSkill then
            do
                local i = 0
                while i < this.m_AdditionalSkillIds.Length and i < CSkillButtonBoardWnd.MaxSkillCountPos2 do
                    if buttonIndex - 1 == i then
                        if mainplayer.SkillProp:IsOriginalSkillClsExist(CLuaDesignMgr.Skill_AllSkills_GetClass(this.m_AdditionalSkillIds[i])) then
                            skillId = mainplayer.SkillProp:GetAlternativeSkillIdWithDelta(this.m_AdditionalSkillIds[i], mainplayer.Level)
                            if Skill_DoubleJoystick.Exists(CLuaDesignMgr.Skill_AllSkills_GetClass(skillId)) and this.additionalButtonInfos.Length > i and this.additionalButtonInfos[i] ~= nil and this.additionalButtonInfos[i].ShowDoubleStick then
                                local pos = this.additionalButtonInfos[i]:GetDoubleStickInitPos()
                                mainplayer:TryCastSkill(skillId, false, pos)
                            else
                                mainplayer.Inst:TryCastSkill(skillId, true, Vector2.zero)
                            end
                            FXClientInfoReporter.Inst:ReportCastTempSkill(skillId, "winshortcutkey")
                        end
                        break
                    end
                    i = i + 1
                end
            end
        else
            local activeSkills = mainplayer.SkillProp:GetCurrentActiveSkill()
            if buttonIndex >= 1 and buttonIndex < activeSkills.Length then
                skillId = activeSkills[buttonIndex]
                local default
                if mainplayer.SkillProp.CurrentActiveSkillSuite == 1 then
                    default = this.activeSkillBtns1
                else
                    default = this.activeSkillBtns2
                end
                local btns = default
                if Skill_DoubleJoystick.Exists(CLuaDesignMgr.Skill_AllSkills_GetClass(skillId)) and btns.Length > buttonIndex - 1 and btns[buttonIndex - 1] ~= nil and btns[buttonIndex - 1].ShowDoubleStick then
                    local pos = btns[buttonIndex - 1]:GetDoubleStickInitPos()
                    CLuaSkillButtonBoardWnd:CastSkill(skillId, false, pos)
                else
                    CLuaSkillButtonBoardWnd:CastSkill(skillId, true, Vector2.zero)
                end
            end
        end
    end
end
CSkillButtonBoardWnd.m_WinShortCutKeyCastJueJi_CS2LuaHook = function (this, index) 
    if CClientMainPlayer.Inst ~= nil then
        if index == 1 then
            this:CastJueJi(this._70SkillClass)
        elseif index == 2 then
            this:CastJueJi(this._100SkillClass)
        end
    end
end
CSkillButtonBoardWnd.m_VoiceAssistantCastSkill_CS2LuaHook = function (this, skillCls) 
    if CSkillButtonBoardWnd.Instance == nil or CClientMainPlayer.Inst == nil then
        return
    end
    --不允许释放临时技能
    if Skill_TempSkill.Exists(skillCls) then
        return
    end
    if skillCls == CClientMainPlayer.Inst.FirstJuejiSkillCls or skillCls == CClientMainPlayer.Inst.SecondJuejiSkillCls then
        this:CastJueJi(CClientMainPlayer.Inst.SkillProp:GetAlternativeSkillCls(skillCls))
        return
    end

    local activeSkills = CClientMainPlayer.Inst.SkillProp:GetCurrentActiveSkill()
    do
        local i = 1
        while i < activeSkills.Length do
            if skillCls == CLuaDesignMgr.Skill_AllSkills_GetClass(activeSkills[i]) then
                if i - 1 >= this.m_AdditionalSkillIds.Length or not Skill_AllSkills.Exists(this.m_AdditionalSkillIds[i - 1]) then
                    CLuaSkillButtonBoardWnd:CastSkill(activeSkills[i], true, Vector2.zero)
                end
                break
            end
            i = i + 1
        end
    end
end
CSkillButtonBoardWnd.m_ExistAdditionalSkillAtCommonSkillPlace_CS2LuaHook = function (this) 
    if CClientMainPlayer.Inst == nil then
        return false
    end
    do
        local i = 0
        while i < this.m_AdditionalSkillIds.Length and i < CSkillButtonBoardWnd.MaxSkillCountPos2 do
            if Skill_AllSkills.Exists(this.m_AdditionalSkillIds[i]) then
                if CClientMainPlayer.Inst.SkillProp:IsOriginalSkillClsExist(CLuaDesignMgr.Skill_AllSkills_GetClass(this.m_AdditionalSkillIds[i])) then
                    return true
                end
            end
            i = i + 1
        end
    end
    return false
end

CSkillButtonBoardWnd.m_RefreshSkillButtonVisibility_CS2LuaHook = function (this) 

    local skillIds = CClientMainPlayer.Inst.SkillProp:GetCurrentActiveSkill()
    --ActiveSkill索引从1开始，第0个位置占位，无用
    if skillIds.Length ~= CPropertySkill.ActiveSkillCount + 1 then
        L10.CLogMgr.LogError("skill button num not match")
        return
    end

    do
        local i = 0
        while i < this.activeSkillBtns1.Length do
            local visible = COpenEntryMgr.Inst:IsSkillButtonVisible(i)
            visible = visible and Skill_AllSkills.Exists(skillIds[i + 1])
            --这里还是用原技能判断，不考虑被临时顶替的情形
            if this.activeSkillBtns1[i].gameObject.activeSelf ~= visible then
                this.activeSkillBtns1[i].gameObject:SetActive(visible)
            end
            i = i + 1
        end
    end
end
CSkillButtonBoardWnd.m_GetHpRecoverButton_CS2LuaHook = function (this) 
    if CClientMainPlayer.Inst == nil then
        return nil
    end
    if not CClientMainPlayer.Inst.ItemProp.ExistsQuickUseDrug then
        return nil
    else
        return this.hpRecoverButtonView.gameObject
    end
end
CSkillButtonBoardWnd.m_GetSwitchTargetButton_CS2LuaHook = function (this) 
    return this.switchTargetBtn
end
CSkillButtonBoardWnd.m_Get1stButton_CS2LuaHook = function (this) 
    if this.activeSkillBtns1.Length > 0 then
        return this.activeSkillBtns1[0].gameObject
    end
    return nil
end
CSkillButtonBoardWnd.m_Get1stButton2_CS2LuaHook = function (this) 
    if this.additionalButtonInfos.Length > 0 then
        return this.additionalButtonInfos[0].gameObject
    end
    return nil
end
CSkillButtonBoardWnd.m_Get2ndButton_CS2LuaHook = function (this) 
    if this.activeSkillBtns1.Length > 0 then
        return this.activeSkillBtns1[1].gameObject
    end
    return nil
end
CSkillButtonBoardWnd.m_Get2ndButton2_CS2LuaHook = function (this) 
    if this.additionalButtonInfos.Length > 0 then
        return this.additionalButtonInfos[1].gameObject
    end
    return nil
end
CSkillButtonBoardWnd.m_Get3rdButton_CS2LuaHook = function (this) 
    if this.activeSkillBtns1.Length > 0 then
        return this.activeSkillBtns1[2].gameObject
    end
    return nil
end
CSkillButtonBoardWnd.m_GetSwitchGo_CS2LuaHook = function (this) 
    return this.activeSkillBtns1[0].transform.parent.gameObject
end

CSkillButtonBoardWnd.m_GetGuideGo_CS2LuaHook = function (this, methodName) 
    if methodName == "GetHpRecoverButton" then
        return this:GetHpRecoverButton()
    elseif methodName == "GetSwitchTargetButton" then
        return this:GetSwitchTargetButton()
    elseif methodName == "Get1stButton" then
        return this:Get1stButton()
    elseif methodName == "Get1stButton2" then
        return this:Get1stButton2()
    elseif methodName == "Get2ndButton" then
        return this:Get2ndButton()
    elseif methodName == "Get2ndButton2" then
        return this:Get2ndButton2()
    elseif methodName == "Get3rdButton" then
        return this:Get3rdButton()
    elseif methodName == "GetSwitchGo" then
        return this:GetSwitchGo()
    else
        CLogMgr.LogError(LocalString.GetString("引导数据表填写错误"))
        return nil
    end
end
CSkillButtonBoardWnd.m_UnlockNewSkillButtonPlace_CS2LuaHook = function (this, index) 
    this:OnSkillsInfoInit()
end
CSkillButtonBoardWnd.m_GetJueJiButton_CS2LuaHook = function (this, skillClass) 
    if CClientMainPlayer.Inst == nil then
        return nil
    end

    if L10.Game.Guide.CGuideMgr.Inst:IsInPhase(L10.Game.Guide.GuideDefine.Phase_JueJi) then
        if L10.Game.PlayerSettings.DirectShowJueJiEnabled then
            L10.Game.Guide.CGuideMgr.Inst:EndCurrentPhase()
            return nil
        end
    end

    if CClientMainPlayer.Inst.FirstJuejiSkillCls == skillClass then
        return this.jueji70ButtonInfo.gameObject
    elseif CClientMainPlayer.Inst.SecondJuejiSkillCls == skillClass then
        return this.jueji100ButtonInfo.gameObject
    end
    return nil
end
CSkillButtonBoardWnd.m_UpdateDrugVisible_CS2LuaHook = function (this) 
    if COpenEntryMgr.Inst:IsDrugButtonVisible() then
        this.hpRecoverButtonView.gameObject:SetActive(true)
    else
        this.hpRecoverButtonView.gameObject:SetActive(false)
    end
    if CDuoMaoMaoMgr.Inst.InHideAndSeek or CSnowBallMgr.Inst:IsInSnowBallFight() or CPUBGMgr.Inst:IsInChiJi() or LuaZhaiXingMgr:InZhaiXing() or LuaLuoCha2021Mgr:IsInGamePlay() then
        this.hpRecoverButtonView.gameObject:SetActive(false)
    end
end

--------------------
-- 扩展CSkillButtonBoardWnd
--------------------

CLuaSkillButtonBoardWnd = {}
CLuaSkillButtonBoardWnd.m_Wnd = nil
CLuaSkillButtonBoardWnd.m_UpdateCoolDownTick = nil
CLuaSkillButtonBoardWnd.m_NeedUpdateCooldown = false

function CLuaSkillButtonBoardWnd:OnEnable(wnd)
    self.m_Wnd = wnd
    g_ScriptEvent:AddListener("UpdateCooldown", self, "OnUpdateCooldown")
    g_ScriptEvent:AddListener("SyncQingHongFlySkillStatus", self, "OnSyncQingHongFlySkillStatus")
    g_ScriptEvent:AddListener("UpdateYingLingState", self, "UpdateYingLingState")
    g_ScriptEvent:AddListener("ShowYingLingSkillRoot", self, "ShowYingLingSkillRoot")
    g_ScriptEvent:AddListener("SetActiveTempSkillSuccess", self, "OnSetActiveTempSkillSuccess")
    g_ScriptEvent:AddListener("OnTempReplaceSkillIconInfoUpdate", self, "OnTempReplaceSkillIconInfoUpdate")
    g_ScriptEvent:AddListener("SyncDuoMaoMaoAddHpSkillPoint", self, "SyncDuoMaoMaoAddHpSkillPoint")
    g_ScriptEvent:AddListener("OnSyncGnjcZhanYiProgressInfo", self, "OnSyncGnjcZhanYiProgressInfo")
    g_ScriptEvent:AddListener("HintHaiDiaoFuBenSkillBtn", self, "OnHintHaiDiaoFuBenSkillBtn")
    g_ScriptEvent:AddListener("XinBaiLianDongUpdateSkillHintFx", self, "OnXinBaiLianDongUpdateSkillHintFx")
    
    self:RegisterTick()
    local fx = self.m_Wnd.GuanNingFightingSpiritButton.transform:Find("ThumbFx"):GetComponent(typeof(CUIFx))
    fx:LoadFx("fx/ui/prefab/UI_guanning_zhanyi01.prefab")
end

function CLuaSkillButtonBoardWnd:OnDisable()
    self.m_Wnd = nil
    g_ScriptEvent:RemoveListener("UpdateCooldown", self, "OnUpdateCooldown")
    g_ScriptEvent:RemoveListener("SyncQingHongFlySkillStatus", self, "OnSyncQingHongFlySkillStatus")
    g_ScriptEvent:RemoveListener("UpdateYingLingState", self, "UpdateYingLingState")
    g_ScriptEvent:RemoveListener("ShowYingLingSkillRoot", self, "ShowYingLingSkillRoot")
    g_ScriptEvent:RemoveListener("SetActiveTempSkillSuccess", self, "OnSetActiveTempSkillSuccess")
    g_ScriptEvent:RemoveListener("OnTempReplaceSkillIconInfoUpdate", self, "OnTempReplaceSkillIconInfoUpdate")
    g_ScriptEvent:RemoveListener("SyncDuoMaoMaoAddHpSkillPoint", self, "SyncDuoMaoMaoAddHpSkillPoint")
    g_ScriptEvent:RemoveListener("OnSyncGnjcZhanYiProgressInfo", self, "OnSyncGnjcZhanYiProgressInfo")
    g_ScriptEvent:RemoveListener("HintHaiDiaoFuBenSkillBtn", self, "OnHintHaiDiaoFuBenSkillBtn")
    g_ScriptEvent:RemoveListener("XinBaiLianDongUpdateSkillHintFx", self, "OnXinBaiLianDongUpdateSkillHintFx")
    
    self:CancelTick()
end

function CLuaSkillButtonBoardWnd:OnSyncGnjcZhanYiProgressInfo(value)
    CLuaGuanNingMgr.m_ZhanYiProgressValue = value
    if not CLuaGuanNingMgr.m_IsSundayPlayOpen or not self.m_Wnd or not CGuanNingMgr.Inst.inGnjc then return end
    self.m_Wnd.GuanNingFightingSpiritButton:SetActive(CLuaGuanNingMgr.m_IsInGuanNingWeekendNewModePlay)
    self.m_Wnd.GuanNingFightingSpiritButton.transform:Find("ProgressBar"):GetComponent(typeof(UIProgressBar)).value = value
    Extensions.SetLocalPositionZ(self.m_Wnd.GuanNingFightingSpiritButton.transform:Find("Mask").transform, value < 1 and -1 or 0 )
    local fx = self.m_Wnd.GuanNingFightingSpiritButton.transform:Find("Fx"):GetComponent(typeof(CUIFx))
    if value >= 1 then
        fx:LoadFx("fx/ui/prefab/UI_guanning_zhanyi02.prefab")
    else
        fx:DestroyFx()
    end
    local angle = math.lerp(90, -270, value)
    fx = self.m_Wnd.GuanNingFightingSpiritButton.transform:Find("ThumbFx"):GetComponent(typeof(CUIFx))
    fx.transform.localPosition = CommonDefs.op_Multiply_Single_Vector3(50, Vector3(math.cos(angle * Deg2Rad), math.sin(angle * Deg2Rad),0))
    fx.gameObject:SetActive(value < 1)
end

function CLuaSkillButtonBoardWnd:RegisterTick()
    self:CancelTick()
    self.m_UpdateCoolDownTick = RegisterTick(function ( ... )
            self:OnTick()
        end, 50)
end

function CLuaSkillButtonBoardWnd:CancelTick()
    if self.m_UpdateCoolDownTick then
        UnRegisterTick(self.m_UpdateCoolDownTick)
        self.m_UpdateCoolDownTick = nil
    end
end

function CLuaSkillButtonBoardWnd:OnUpdateCooldown()
    self.m_NeedUpdateCooldown = true
end

function CLuaSkillButtonBoardWnd:OnSyncQingHongFlySkillStatus(blink)
    for i = 0, self.m_Wnd.additionalButtonInfos.Length-1 do
        local info = self.m_Wnd.additionalButtonInfos[i]
        info:UpdateHintFx(blink)
    end
end

function CLuaSkillButtonBoardWnd:UpdateYingLingState(args)
    if not args then return end
    
    local engineId = args[0]
    local mainplayer = CClientMainPlayer.Inst
    if mainplayer and mainplayer.EngineId == engineId then
        self.m_Wnd:OnSkillsInfoInit()
    end
end

function CLuaSkillButtonBoardWnd:SyncDuoMaoMaoAddHpSkillPoint(addHpSkillPoint)
    if not self.m_Wnd.SkillLastCountLabel2 then return end
    if addHpSkillPoint > 0 then
        self.m_Wnd.SkillLastCountLabel2.gameObject:SetActive(true)
        self.m_Wnd.SkillLastCountLabel2.text = tostring(addHpSkillPoint)
    else
        self.m_Wnd.SkillLastCountLabel2.gameObject:SetActive(false)
    end
end

function CLuaSkillButtonBoardWnd:ShowYingLingSkillRoot()
    self.m_Wnd.yinglingSkillRoot:SetActive(true)
end

function CLuaSkillButtonBoardWnd:OnTick()
    if self.m_NeedUpdateCooldown then
        self.m_NeedUpdateCooldown = false
        self.m_Wnd:OnUpdateCooldown()
    end
end

function CLuaSkillButtonBoardWnd:CastSkill(skillId, autoTarget, pos)
    if not self.m_Wnd then return end
    local mainplayer = CClientMainPlayer.Inst
    if not mainplayer then return end
    local realSkillId = mainplayer.SkillProp:GetAlternativeSkillIdWithDelta(skillId, mainplayer.Level)
    if Skill_AllSkills.GetClass(realSkillId) == Skill_Setting.GetData().YingLingBianShenSkillId then
        self.m_Wnd.yinglingSkillRoot:SetActive(true)
    else
        mainplayer:TryCastSkill(realSkillId, autoTarget, pos)
    end
end

function CLuaSkillButtonBoardWnd:OnSetActiveTempSkillSuccess()
    self.m_Wnd:InitAdditionalSkill()
end

function CLuaSkillButtonBoardWnd:OnTempReplaceSkillIconInfoUpdate(oriSkillCls)
    local mainplayer = CClientMainPlayer.Inst
    if not mainplayer then return end
    self.m_Wnd:OnSkillsInfoInit() --这里为了简化处理直接刷新所有技能按钮
end

function CLuaSkillButtonBoardWnd:OnJuejiButtonClick(buttonInfo, juejiCls)
    local mainplayer = CClientMainPlayer.Inst
    if mainplayer == nil or not mainplayer.SkillProp:IsSkillClsExist(juejiCls) then
        return
    end
    local cooldownProp = mainplayer.CooldownProp
    local globalRemain = cooldownProp:GetServerRemainTime(1000)
    local globalTotal = cooldownProp:GetServerTotalTime(1000)
    local globalPercent = globalTotal == 0 and 0 or globalRemain / globalTotal
    local skillId = mainplayer.SkillProp:GetSkillIdWithDeltaByCls(juejiCls, mainplayer.Level)
    local remain = cooldownProp:GetServerRemainTime(skillId)
    local total = cooldownProp:GetServerTotalTime(skillId)
    local percent = total == 0 and 0 or remain / total
    if percent <= 1E-06 and globalPercent  <= 1E-06 then
    -- CD ready
        local texture = buttonInfo.transform:Find("Click/Icon"):GetComponent(typeof(UITexture))
        texture.material = buttonInfo.skillIcon.material
        local tweenalpha = texture.gameObject:GetComponent(typeof(TweenAlpha))
        tweenalpha:ResetToBeginning()
        tweenalpha:PlayForward()
        local tweenscale = texture.gameObject:GetComponent(typeof(TweenScale))
        tweenalpha:ResetToBeginning()
        tweenalpha:PlayForward()
        local fx = buttonInfo.transform:Find("Click/ClickFx"):GetComponent(typeof(CUIFx))
        fx:DestroyFx()
        fx:LoadFx("fx/ui/prefab/UI_juejie_anniudianji.prefab")
    end
end

function CLuaSkillButtonBoardWnd:OnHintHaiDiaoFuBenSkillBtn(hintSkill,isHint)
    for i = 0, self.m_Wnd.additionalButtonInfos.Length-1 do
        local info = self.m_Wnd.additionalButtonInfos[i]
        if not hintSkill or info.skillTemplateID  ==  hintSkill then
            info:UpdateHintFx(isHint)
        end
    end
end

function CLuaSkillButtonBoardWnd:OnXinBaiLianDongUpdateSkillHintFx(skillId, isHint)
    for i = 0, self.m_Wnd.additionalButtonInfos.Length - 1 do

        local info = self.m_Wnd.additionalButtonInfos[i]
        if skillId and info.skillTemplateID == skillId then
            info:UpdateHintFx(isHint)
            break
        end
    end
end

function CLuaSkillButtonBoardWnd:InitXianZhiBtnShow()
    if CLuaXianzhiMgr.GetXianZhiStatus() == EnumXianZhiStatus.NotFengXian or CLuaXianzhiMgr.GetXianZhiSKillId() == 0 then
        self:SetYanHuaViewBtnPos(1)
        self.m_Wnd.XianZhiBtn.gameObject:SetActive(false)
        return
    end

    self:SetYanHuaViewBtnPos(2) 
    local skillId = CLuaXianzhiMgr.GetXianZhiSKillId()
    if CClientMainPlayer.Inst.SkillProp:IsOriginalSkillClsExist(Skill_AllSkills.GetClass(skillId)) then

        skillId = CClientMainPlayer.Inst.SkillProp:GetAlternativeSkillIdWithDelta(skillId, CClientMainPlayer.Inst.Level)
        local skill = Skill_AllSkills.GetData(skillId)

        -- 不是被动技能+玩家的确拥有这个技能
        if skill ~= nil and skill.ECategory == SkillCategory.Active then
            self.m_Wnd.XianZhiBtn:LoadIcon(skill.SkillIcon, skillId, false, true)
            -- todo 
            local luaScript = CommonDefs.GetComponent_GameObject_Type(self.m_Wnd.XianZhiBtn.gameObject, typeof(CCommonLuaScript))
            if luaScript then luaScript:Init({}) end
            self.m_Wnd.XianZhiBtn.gameObject:SetActive(true)
        end
    else
        self.m_Wnd.XianZhiBtn.gameObject:SetActive(false)
    end
end

function CLuaSkillButtonBoardWnd:SubAdditionalSkills(left, right)
    local newSkills = {}
    local newSkillsCounters = 0
    local tempSkillArray = CClientMainPlayer.Inst.SkillProp.ActiveTempSkill
    
    for i = left, right do
        if CClientMainPlayer.Inst.SkillProp:IsOriginalSkillClsExist(Skill_AllSkills.GetClass(tempSkillArray[i])) then
            table.insert(newSkills, tempSkillArray[i])
            newSkillsCounters = newSkillsCounters + 1
        else
            if CPUBGMgr.Inst:IsInChiJi() then
                table.insert(newSkills, 0)
            end
        end
    end
    return newSkills, newSkillsCounters
end

function CLuaSkillButtonBoardWnd:InitAdditionalButtonInfos(skillsTable, idxBegin)
    local pointTemporarySkill = Skill_Setting.GetData().PointTemporarySkill
    for idx = 0, #skillsTable-1 do
        local skillId = skillsTable[idx+1]
        if skillId ~= 0 then
            local i = idx + idxBegin       -- 将各类临时按钮的索引映射到总表
            self.m_Wnd.additionalButtonInfos[i].gameObject:SetActive(true)
            self.m_Wnd.m_AdditionalSkillIds[i] = skillId

            --对潜在的被替换技能进行处理
            skillId = CClientMainPlayer.Inst.SkillProp:GetAlternativeSkillIdWithDelta(skillId, CClientMainPlayer.Inst.Level)
            local skill = Skill_AllSkills.GetData(skillId)
            if skill ~= nil then
                self.m_Wnd.additionalButtonInfos[i]:LoadIcon(LuaSkillMgr:GetOriginOrReplaceSkillIcon(skill), skillId, false, true)
                if CommonDefs.HashSetContains(pointTemporarySkill,typeof(UInt32),math.floor(skillId/100)) then
                    self.m_Wnd.additionalButtonInfos[i]:UpdateHintFx(true)
                end

                if LuaHanJia2023Mgr.IsInXuePoLiXianPlay() then
                    self.m_Wnd.additionalButtonInfos[i]:UpdateHintFx(false)
                end
                if LuaQingHongFlyMgr.m_ShowBlink then
                    g_ScriptEvent:BroadcastInLua("SyncQingHongFlySkillStatus", true)
                    CLuaGuideMgr.TriggerQingHongFlySkillGuide(true)
                end
            end
            
            -- ShowGuide
            -- 主角剧情（小谢变身僵尸跳）
            CLuaGuideMgr.TryTriggerXiaoxieZombieJumpSkillGuide(skillId, self.m_Wnd.additionalButtonInfos[i])
            -- 2022世界杯
            LuaWorldCup2022Mgr:TryTriggerGJZLSkillGuide(skillId, i)
            -- 海钓副本
            local haidiaoSetting = HaiDiaoFuBen_Settings.GetData()
            if skillId == haidiaoSetting.WaterPoolMonster_Guide[1] then
                CLuaGuideMgr.TryTriggerGuide(154)
            elseif skillId == haidiaoSetting.HoleMonsterId_Guide[1] then
                CLuaGuideMgr.TryTriggerGuide(153)
            elseif skillId == haidiaoSetting.HoleWithWoodMonsterId_Guide[1] then
                CLuaGuideMgr.TryTriggerGuide(155)
            end
        end
    end
end

function CLuaSkillButtonBoardWnd:SetYanHuaViewBtnPos(count)
    local pos = self.m_Wnd.guajiBtn.transform.localPosition
    local pos2 = Vector3(pos.x -count*140,pos.y,pos.z)
    self.m_Wnd.YanHuaViewBtn.transform.localPosition = pos2
end

--------------------
-- CLuaSkillButtonBoardMgr
--------------------
CLuaSkillButtonBoardMgr = {}
function CLuaSkillButtonBoardMgr:AddListener()
	g_ScriptEvent:RemoveListener("UpdateCurrentTask", self, "OnUpdateCurrentTask")
	g_ScriptEvent:AddListener("UpdateCurrentTask", self, "OnUpdateCurrentTask")
end
CLuaSkillButtonBoardMgr:AddListener()

function CLuaSkillButtonBoardMgr:OnUpdateCurrentTask(args)
	if CScene.MainScene == nil then
		return
	end
	local taskId = args[0]
	local playId = CScene.MainScene.GamePlayDesignId
	local data = Gameplay_Gameplay.GetData(playId)
	if data ~= nil then
		local hideTasks = data.HideSkillBoardTasks
		if CClientMainPlayer.Inst ~= nil and CSkillButtonBoardWnd.Instance and hideTasks and hideTasks.Length > 0 then
			CSkillButtonBoardWnd.Instance:CheckDisplay()
		end
	end
end
