-- Auto Generated!!
local CMPTZRankListItem = import "L10.UI.CMPTZRankListItem"
local Constants = import "L10.Game.Constants"
local LocalString = import "LocalString"
CMPTZRankListItem.m_Init_CS2LuaHook = function (this, data, curCls, isOdd, showTotalRank) 

    this.playerId = math.floor(data.targetId)
    this.playerName = data.name
    this.rankLabel.text = tostring(data.rankIdx)
    this.totalRankLabel.text = showTotalRank and tostring(data.totalRankIdx) or ""
    if curCls ~= data.profession then
        this.nameLabel.text = System.String.Format(LocalString.GetString("{0} [00B8EE](镜像)[-]"), data.name)
    else
        this.nameLabel.text = data.name
    end
    this.levelLabel.text = tostring(data.grade)

    local default
    if not isOdd then
        default = this.CommonDarkTextBg
    else
        default = this.CommonLightTextBg
    end
    this.button:SetBackgroundSprite(default)

    local spriteName = this:GetRankImageByRankIndex(data.rankIdx)
    if not System.String.IsNullOrEmpty(spriteName) then
        this.rankImage.spriteName = spriteName
        this.rankImage:MakePixelPerfect()
    else
        this.rankImage.spriteName = nil
    end

    spriteName = this:GetRankImageByRankIndex(data.totalRankIdx)
    if not System.String.IsNullOrEmpty(spriteName) and showTotalRank then
        this.totalRankImage.spriteName = spriteName
        this.totalRankImage:MakePixelPerfect()
    else
        this.totalRankImage.spriteName = nil
    end
end
CMPTZRankListItem.m_GetRankImageByRankIndex_CS2LuaHook = function (this, index) 

    repeat
        local default = index
        if default == (1) then
            return Constants.RankFirstSpriteName
        elseif default == (2) then
            return Constants.RankSecondSpriteName
        elseif default == (3) then
            return Constants.RankThirdSpriteName
        else
            return nil
        end
    until 1
end
