local DelegateFactory = import "DelegateFactory"

LuaWorldCup2022HomeTeamView = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaWorldCup2022HomeTeamView, "tabTable")
RegistClassMember(LuaWorldCup2022HomeTeamView, "changeView")
RegistClassMember(LuaWorldCup2022HomeTeamView, "infoView")
RegistClassMember(LuaWorldCup2022HomeTeamView, "scheduleView")
RegistClassMember(LuaWorldCup2022HomeTeamView, "noHomeTeam")

RegistClassMember(LuaWorldCup2022HomeTeamView, "curTabId")

function LuaWorldCup2022HomeTeamView:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self:InitUIComponents()
    self:InitLeftTabs()

    UIEventListener.Get(self.noHomeTeam.transform:Find("SelectButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnSelectButtonClick()
    end)

    self.changeView.gameObject:SetActive(false)
    self.infoView.gameObject:SetActive(false)
    self.scheduleView.gameObject:SetActive(false)
    self.noHomeTeam:SetActive(false)
end

function LuaWorldCup2022HomeTeamView:InitUIComponents()
    self.tabTable = self.transform:Find("Left/Panel/Table")
    self.changeView = self.transform:Find("ChangeView"):GetComponent(typeof(CCommonLuaScript))
    self.infoView = self.transform:Find("InfoView"):GetComponent(typeof(CCommonLuaScript))
    self.scheduleView = self.transform:Find("ScheduleView"):GetComponent(typeof(CCommonLuaScript))
    self.noHomeTeam = self.transform:Find("NoHomeTeam").gameObject
end

function LuaWorldCup2022HomeTeamView:InitLeftTabs()
    for i = 0, 2 do
        local child = self.tabTable:GetChild(i)
        local normal = child:Find("Normal").gameObject
        local highlight = child:Find("Highlight").gameObject

        normal:SetActive(true)
        highlight:SetActive(false)
        UIEventListener.Get(normal).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnTabChange(i)
        end)
    end
end

function LuaWorldCup2022HomeTeamView:OnEnable()
    g_ScriptEvent:AddListener("WorldCupJingCai_UpdateAllJingCaiData", self, "UpdateAllJingCaiData")
    self:UpdateTeamInfo()
end

function LuaWorldCup2022HomeTeamView:OnDisable()
    g_ScriptEvent:RemoveListener("WorldCupJingCai_UpdateAllJingCaiData", self, "UpdateAllJingCaiData")
end

function LuaWorldCup2022HomeTeamView:UpdateAllJingCaiData()
    self:UpdateTeamInfo()
end

function LuaWorldCup2022HomeTeamView:UpdateTeamInfo()
    local jingCaiData = LuaWorldCup2022Mgr.lotteryInfo.jingCaiData
    local teamId = jingCaiData and jingCaiData.m_HomeTeamId
    if not teamId then return end

    if not self.curTabId then
        if teamId == 0 then
            self:OnTabChange(2)
        else
            self:OnTabChange(0)
        end
    else
        self:UpdateView()
    end
end

function LuaWorldCup2022HomeTeamView:UpdateView()
    local jingCaiData = LuaWorldCup2022Mgr.lotteryInfo.jingCaiData
    local teamId = jingCaiData and jingCaiData.m_HomeTeamId
    if not teamId then return end

    self.infoView.gameObject:SetActive(self.curTabId == 0 and teamId ~= 0)
    self.scheduleView.gameObject:SetActive(self.curTabId == 1 and teamId ~= 0)
    self.changeView.gameObject:SetActive(self.curTabId == 2)
    self.noHomeTeam:SetActive(teamId == 0 and self.curTabId ~= 2)
end

--@region UIEvent

function LuaWorldCup2022HomeTeamView:OnSelectButtonClick()
    if not self.curTabId or self.curTabId ~= 2 then
        self:OnTabChange(2)
    end
end

function LuaWorldCup2022HomeTeamView:OnTabChange(i)
    if self.curTabId then
        local before = self.tabTable:GetChild(self.curTabId)
        before:Find("Normal").gameObject:SetActive(true)
        before:Find("Highlight").gameObject:SetActive(false)
    end

    self.curTabId = i
    local tab = self.tabTable:GetChild(i)
    tab:Find("Normal").gameObject:SetActive(false)
    tab:Find("Highlight").gameObject:SetActive(true)

    self:UpdateView()
end

--@endregion UIEvent
