local NGUITools = import "NGUITools"
local UIGrid=import "UIGrid"
local UIEventListener=import "UIEventListener"
local UILabel=import "UILabel"

CLuaDouDiZhuQuickChatWnd=class()
RegistClassMember(CLuaDouDiZhuQuickChatWnd,"m_ItemTemplate")
RegistClassMember(CLuaDouDiZhuQuickChatWnd,"m_Grid")


function CLuaDouDiZhuQuickChatWnd:Init()
    local worldPos=CLuaDouDiZhuMgr.m_ChatClickWorldPos
    local localPos=self.transform:InverseTransformPoint(worldPos)
    LuaUtils.SetLocalPosition(FindChild(self.transform,"Anchor"),localPos.x-200+15,localPos.y+596/2+50+10,0)
    
    self.m_ItemTemplate=FindChild(self.transform,"ItemTemplate").gameObject
    self.m_ItemTemplate:SetActive(false)

    self.m_Grid=FindChild(self.transform,"Grid").gameObject


    DouDiZhu_ShortMessage.Foreach(function(k,v)
        local item = NGUITools.AddChild(self.m_Grid, self.m_ItemTemplate)
        item:SetActive(true)
        FindChild(item.transform,"Content"):GetComponent(typeof(UILabel)).text=v.Message
        
        UIEventListener.Get(item).onClick=LuaUtils.VoidDelegate(function(go)
            self:OnClick(go)
        end)
    end)

    self.m_Grid:GetComponent(typeof(UIGrid)):Reposition()
end
function CLuaDouDiZhuQuickChatWnd:OnClick(go)
    local index=go.transform:GetSiblingIndex()
    CUIManager.CloseUI(CUIResources.DouDiZhuQuickChatWnd)
    CLuaDouDiZhuMgr:OnQuickMsgClick(index+1)
end


return CLuaDouDiZhuQuickChatWnd