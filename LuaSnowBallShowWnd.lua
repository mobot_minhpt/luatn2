local DelegateFactory = import "DelegateFactory"
local NGUITools = import "NGUITools"
local CButton = import "L10.UI.CButton"
local Temple_Temple = import "L10.Game.Temple_Temple"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local XueQiuDaZhan_XueQiu = import "L10.Game.XueQiuDaZhan_XueQiu"
local CHideAndSeekSkillItem = import "L10.UI.CHideAndSeekSkillItem"
local UITable = import "UITable"
local UIScrollView = import "UIScrollView"
local GameObject = import "UnityEngine.GameObject"
local Gac2GasC = import "L10.Game.Gac2Gas"

LuaSnowBallShowWnd = class()
RegistChildComponent(LuaSnowBallShowWnd,"closeBtn", GameObject)
RegistChildComponent(LuaSnowBallShowWnd,"tipBtn", GameObject)
RegistChildComponent(LuaSnowBallShowWnd,"showTextLabel", UILabel)
RegistChildComponent(LuaSnowBallShowWnd,"skillNode1", CHideAndSeekSkillItem)
RegistChildComponent(LuaSnowBallShowWnd,"skillNode2", CHideAndSeekSkillItem)
RegistChildComponent(LuaSnowBallShowWnd,"skillNode3", CHideAndSeekSkillItem)
RegistChildComponent(LuaSnowBallShowWnd,"skillNode4", CHideAndSeekSkillItem)
RegistChildComponent(LuaSnowBallShowWnd,"templateNode", GameObject)
RegistChildComponent(LuaSnowBallShowWnd,"table", UITable)
RegistChildComponent(LuaSnowBallShowWnd,"scrollView", UIScrollView)
RegistChildComponent(LuaSnowBallShowWnd, "rewardLeftLabel", UILabel)

function LuaSnowBallShowWnd:Init()
  self.templateNode:SetActive(false)

  UIEventListener.Get(self.tipBtn).onClick = DelegateFactory.VoidDelegate(function (p)
    g_MessageMgr:ShowMessage("XQDZ_gaiyao")
  end)
  LuaSnowBallMgr.SignUp1Status = false
  LuaSnowBallMgr.SignUp2Status = false

  Gac2GasC.QuerySignUpXueQiuStatus()
  Gac2GasC.QuerySignUpXueQiuDoubleStatus()

  self.showTextLabel.text = g_MessageMgr:FormatMessage("XQDZ_jieshao", nil)

  local singleStartBtn = self.transform:Find("Anchor/singleStartBtn").gameObject
  local singleCancelBtn = self.transform:Find("Anchor/singleCancelBtn").gameObject
  local doubleStartBtn = self.transform:Find("Anchor/doubleStartBtn").gameObject
  local doubleCancelBtn = self.transform:Find("Anchor/doubleCancelBtn").gameObject

  UIEventListener.Get(singleStartBtn).onClick = DelegateFactory.VoidDelegate(function (p)
    Gac2Gas.RequestSignUpXueQiu(false)
  end)
  UIEventListener.Get(singleCancelBtn).onClick = DelegateFactory.VoidDelegate(function (p)
    Gac2Gas.RequestCancelSignUpXueQiu()
  end)
  UIEventListener.Get(doubleStartBtn).onClick = DelegateFactory.VoidDelegate(function (p)
    Gac2Gas.RequestSignUpXueQiuDouble(false)
  end)
  UIEventListener.Get(doubleCancelBtn).onClick = DelegateFactory.VoidDelegate(function (p)
    Gac2Gas.RequestCancelSignUpXueQiuDouble()
  end)

  local skillInfoStringArray = CommonDefs.StringSplit_ArrayChar(XueQiuDaZhan_XueQiu.GetData().SkillList, ",")
  local templeInfoStringArray = CommonDefs.StringSplit_ArrayChar(XueQiuDaZhan_XueQiu.GetData().TempleList, ",")
  local skillInfoNodeTable = {self.skillNode1,self.skillNode2,self.skillNode3,self.skillNode4}

  do
    local i = 1
    while i < skillInfoStringArray.Length + 1 do
      if i <= #skillInfoNodeTable then
        local skillId = System.UInt32.Parse(skillInfoStringArray[i-1])
        skillInfoNodeTable[i]:Init(skillId * 100 + 1)
      end
      i = i + 1
    end
  end

  do
    local i = 0
    while i < templeInfoStringArray.Length do
      local templeId = System.UInt32.Parse(templeInfoStringArray[i])
      local templeInfo = Temple_Temple.GetData(templeId)
      if templeInfo ~= nil then
        local node = NGUITools.AddChild(self.table.gameObject, self.templateNode)
        node:SetActive(true)
        CommonDefs.GetComponent_Component_Type(node.transform:Find("name"), typeof(UILabel)).text = templeInfo.Name
        CommonDefs.GetComponent_Component_Type(node.transform:Find("des"), typeof(UILabel)).text = templeInfo.Description
      end
      i = i + 1
    end
  end

  self.table:Reposition()
  self.scrollView:ResetPosition()
  self:SyncStatus()
end

function LuaSnowBallShowWnd:SyncStatus()
  local singleStartBtn = self.transform:Find("Anchor/singleStartBtn").gameObject
  local singleCancelBtn = self.transform:Find("Anchor/singleCancelBtn").gameObject
  local doubleStartBtn = self.transform:Find("Anchor/doubleStartBtn").gameObject
  local doubleCancelBtn = self.transform:Find("Anchor/doubleCancelBtn").gameObject

  local color = LuaColorUtils.WhiteColorTx
  if LuaSnowBallMgr.RemainWeeklyTimes <= 0 then
    color = LuaColorUtils.RedColorTx
  end
  self.rewardLeftLabel.text = SafeStringFormat3(LocalString.GetString("[ACF8FF]每周剩余参与奖励[-] [%s]%s[-]"), color, tostring(LuaSnowBallMgr.RemainWeeklyTimes))


  if LuaSnowBallMgr.SignUp1Status then
    singleStartBtn:SetActive(false)
    singleCancelBtn:SetActive(true)
    doubleStartBtn:SetActive(true)
    doubleCancelBtn:SetActive(false)
    CommonDefs.GetComponent_GameObject_Type(doubleStartBtn, typeof(CButton)).Enabled = false
    self.rewardLeftLabel.gameObject:SetActive(false)
  elseif LuaSnowBallMgr.SignUp2Status then
    singleStartBtn:SetActive(true)
    singleCancelBtn:SetActive(false)
    doubleStartBtn:SetActive(false)
    doubleCancelBtn:SetActive(true)
    CommonDefs.GetComponent_GameObject_Type(singleStartBtn, typeof(CButton)).Enabled = false
    self.rewardLeftLabel.gameObject:SetActive(false)
  else
    singleStartBtn:SetActive(true)
    singleCancelBtn:SetActive(false)
    doubleStartBtn:SetActive(true)
    doubleCancelBtn:SetActive(false)
    CommonDefs.GetComponent_GameObject_Type(singleStartBtn, typeof(CButton)).Enabled = true
    CommonDefs.GetComponent_GameObject_Type(doubleStartBtn, typeof(CButton)).Enabled = true
    self.rewardLeftLabel.gameObject:SetActive(true)
  end
end

function LuaSnowBallShowWnd:OnEnable( ... )
	g_ScriptEvent:AddListener("SyncSnowBallShowStatus", self, "SyncStatus")
end

function LuaSnowBallShowWnd:OnDisable( ... )
	g_ScriptEvent:RemoveListener("SyncSnowBallShowStatus", self, "SyncStatus")
end

function LuaSnowBallShowWnd:OnDestroy( ... )
end

return LuaSnowBallShowWnd
