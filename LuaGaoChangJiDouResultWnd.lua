local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CItemMgr = import "L10.Game.CItemMgr"

LuaGaoChangJiDouResultWnd = class()

RegistClassMember(LuaGaoChangJiDouResultWnd, "ScoreLabel")
RegistClassMember(LuaGaoChangJiDouResultWnd, "RankLabel")
RegistClassMember(LuaGaoChangJiDouResultWnd, "TreasureLabel")
RegistClassMember(LuaGaoChangJiDouResultWnd, "DefeatLabel")
RegistClassMember(LuaGaoChangJiDouResultWnd, "AssistLabel")
RegistClassMember(LuaGaoChangJiDouResultWnd, "MonsterLabel")
RegistClassMember(LuaGaoChangJiDouResultWnd, "ChestLabel")
RegistClassMember(LuaGaoChangJiDouResultWnd, "AwardListBtn")


function LuaGaoChangJiDouResultWnd:Awake()
    self.ScoreLabel = self.transform:Find("Anchor/Panel/Score/Current/Label"):GetComponent(typeof(UILabel))
    self.RankLabel = self.transform:Find("Anchor/Panel/Rank/Label"):GetComponent(typeof(UILabel))
    self.TreasureLabel = self.transform:Find("Anchor/Panel/GaoChangChest/Label"):GetComponent(typeof(UILabel))
    self.DefeatLabel = self.transform:Find("Anchor/Panel/Defeat/Label"):GetComponent(typeof(UILabel))
    self.AssistLabel = self.transform:Find("Anchor/Panel/Assist/Label"):GetComponent(typeof(UILabel))
    self.MonsterLabel = self.transform:Find("Anchor/Panel/Monster/Label"):GetComponent(typeof(UILabel))
    self.ChestLabel = self.transform:Find("Anchor/Panel/Chest/Label"):GetComponent(typeof(UILabel))
    self.AwardListBtn = self.transform:Find("Anchor/Panel/AwardList/Btn").gameObject

    UIEventListener.Get(self.AwardListBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnAwardListBtnClick()
	end)
end

function LuaGaoChangJiDouResultWnd:Init()

end

function LuaGaoChangJiDouResultWnd:OnEnable()
    self:SyncGaoChangJiDouCrossPlayInfo()
    g_ScriptEvent:AddListener("SyncGaoChangJiDouCrossPlayInfo", self, "SyncGaoChangJiDouCrossPlayInfo")
end

function LuaGaoChangJiDouResultWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncGaoChangJiDouCrossPlayInfo", self, "SyncGaoChangJiDouCrossPlayInfo")
end

function LuaGaoChangJiDouResultWnd:SyncGaoChangJiDouCrossPlayInfo()
    self.ScoreLabel.text = LuaGaoChangJiDouMgr.MyInfo.score or 0
    self.RankLabel.text = LocalString.GetString("排名 ")..(LuaGaoChangJiDouMgr.MyInfo.rank and LuaGaoChangJiDouMgr.PlayerNum > 0 and LuaGaoChangJiDouMgr.MyInfo.rank.."/"..LuaGaoChangJiDouMgr.PlayerNum or "0/0")
    self.DefeatLabel.text = LuaGaoChangJiDouMgr.MyInfo.kill or 0
    self.AssistLabel.text = LuaGaoChangJiDouMgr.MyInfo.help or 0
    self.MonsterLabel.text = LuaGaoChangJiDouMgr.MyInfo.monsterkill or 0
    self.ChestLabel.text = LuaGaoChangJiDouMgr.MyInfo.baoxiang or 0
    self.TreasureLabel.text = CItemMgr.Inst:GetItemCount(21000916)
end

function LuaGaoChangJiDouResultWnd:OnAwardListBtnClick()
    Gac2Gas.GaoChangJiDouQueryAward()
end