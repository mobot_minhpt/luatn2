require("3rdParty/ScriptEvent")
require("common/common_include")

local CUITexture = import "L10.UI.CUITexture"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local CButton = import "L10.UI.CButton"
local Constants = import "L10.Game.Constants"
local MessageWndManager = import "L10.UI.MessageWndManager"
local MessageMgr = import "L10.Game.MessageMgr"
local QnNumberInput = import "L10.UI.QnNumberInput"
local AlignType = import "L10.UI.CTooltip+AlignType"
local Cangbaoge_Setting = import "L10.Game.Cangbaoge_Setting"

LuaTreasureHousePutToSellWnd=class()

RegistClassMember(LuaTreasureHousePutToSellWnd, "PlayerTexture")
RegistClassMember(LuaTreasureHousePutToSellWnd, "PlayerLevelLabel")
RegistClassMember(LuaTreasureHousePutToSellWnd, "PlayerNameLabel")
RegistClassMember(LuaTreasureHousePutToSellWnd, "PlayerScoreLabel")

RegistClassMember(LuaTreasureHousePutToSellWnd, "ExpectedPriceInput")
RegistClassMember(LuaTreasureHousePutToSellWnd, "SellBtn")

function LuaTreasureHousePutToSellWnd:Init()
	self:InitClassMember()
	self:UpdateShowInfo()
end

-- 初始化成员变量
function LuaTreasureHousePutToSellWnd:InitClassMember()
	self.PlayerTexture = self.transform:Find("Anchor/Header/PlayerInfo/PlayerTexture"):GetComponent(typeof(CUITexture))
	self.PlayerLevelLabel = self.transform:Find("Anchor/Header/PlayerInfo/PlayerLevelLabel"):GetComponent(typeof(UILabel))
	self.PlayerNameLabel = self.transform:Find("Anchor/Header/PlayerInfo/PlayerNameLabel"):GetComponent(typeof(UILabel))
	self.PlayerScoreLabel = self.transform:Find("Anchor/Header/PlayerInfo/PlayerScoreLabel"):GetComponent(typeof(UILabel))

	self.ExpectedPriceInput = self.transform:Find("Anchor/Header/ExpectedPriceInput"):GetComponent(typeof(QnAddSubAndInputButton))
	self.SellBtn = self.transform:Find("Anchor/Bottom/SellBtn"):GetComponent(typeof(CButton))
end

function LuaTreasureHousePutToSellWnd:UpdateShowInfo()
	if not CClientMainPlayer.Inst then return end

	self.PlayerTexture:LoadNPCPortrait(CClientMainPlayer.Inst.PortraitName)

	local color = "FFFFFF"
	if CClientMainPlayer.Inst.IsInXianShenStatus then
		color = Constants.ColorOfFeiSheng
	end
	local levelStr = SafeStringFormat3("[c][%s]%s[-][/c]", color, tostring(CClientMainPlayer.Inst.Level))
	self.PlayerLevelLabel.text = levelStr

	self.PlayerNameLabel.text = CClientMainPlayer.Inst.Name
	self.PlayerScoreLabel.text = SafeStringFormat3(LocalString.GetString("[91FAFF]装评[-] %s"), tostring(CClientMainPlayer.Inst.EquipScore))

	local setting = Cangbaoge_Setting.GetData()
	self.ExpectedPriceInput:SetMinMax(setting.MinPrice, setting.MaxPrice, 1)
	local onValueChanged = function (value)
		self:OnPriceValueChanged(value)
	end
	self.ExpectedPriceInput:SetNumberInputAlignType(AlignType.Right)
	self.ExpectedPriceInput.onKeyBoardClosed = DelegateFactory.Action_uint(onValueChanged)

	-- 请求卖号
	local onSellBtnClicked = function (go)
		self:RequestToSell()
	end
	CommonDefs.AddOnClickListener(self.SellBtn.gameObject,DelegateFactory.Action_GameObject(onSellBtnClicked),false)
end

-- 输入的预期价格变动
function LuaTreasureHousePutToSellWnd:OnPriceValueChanged(value)
	local txt = SafeStringFormat3(LocalString.GetString("￥ %s"), tostring(value))
	self.ExpectedPriceInput:OverrideText(txt)
end

-- 请求上架藏宝阁
function LuaTreasureHousePutToSellWnd:RequestToSell()

	local numberInput = self.ExpectedPriceInput.transform:Find("QnNumberInput"):GetComponent(typeof(QnNumberInput))
	if not numberInput.Text or numberInput.Text == "" then
		MessageMgr.Inst:ShowMessage("CBG_NO_EXCEPTED_PRICE", {})
		return
	end

	local txt = MessageMgr.Inst:FormatMessage("CBG_SELL_COMFIRM", {self.ExpectedPriceInput:GetValue()})
	MessageWndManager.ShowOKCancelMessage(txt, DelegateFactory.Action(
        function()
        	Gac2Gas.CBG_RequestOnShelf(CClientMainPlayer.Inst.Id, self.ExpectedPriceInput:GetValue(), "")
        end
        ), nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
end

function LuaTreasureHousePutToSellWnd:OnEnable()
	--g_ScriptEvent:AddListener("SendBingQiPuRank", self, "UpdateRankInfo")
end

function LuaTreasureHousePutToSellWnd:OnDisable()
	--g_ScriptEvent:RemoveListener("SendBingQiPuRank", self, "UpdateRankInfo")
end


return LuaTreasureHousePutToSellWnd