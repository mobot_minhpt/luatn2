require("common/common_include")

local CBaseWnd = import "L10.UI.CBaseWnd"
local DelegateFactory = import "DelegateFactory"
local CommonDefs = import "L10.Game.CommonDefs"
local CUITexture = import "L10.UI.CUITexture"


LuaCityWarBiaoCheConfirmWnd = class()
RegistClassMember(LuaCityWarBiaoCheConfirmWnd,"m_Icon")
RegistClassMember(LuaCityWarBiaoCheConfirmWnd,"m_Button")

function LuaCityWarBiaoCheConfirmWnd:Awake()
    
end

function LuaCityWarBiaoCheConfirmWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaCityWarBiaoCheConfirmWnd:Init()

	self.m_Icon = self.transform:Find("Anchor/ItemCell/IconTexture"):GetComponent(typeof(CUITexture))
	self.m_Button = self.transform:Find("Anchor/Button").gameObject
	
	CommonDefs.AddOnClickListener(self.m_Button, DelegateFactory.Action_GameObject(function(go) self:OnButtonClick() end), false)
end

function LuaCityWarBiaoCheConfirmWnd:OnButtonClick()
	Gac2Gas.ConfirmFindYaYunBiaoChe()
	self:Close()
end

return LuaCityWarBiaoCheConfirmWnd
