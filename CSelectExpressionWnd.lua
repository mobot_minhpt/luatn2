-- Auto Generated!!
local CSelectExpressionItem = import "L10.UI.CSelectExpressionItem"
local CSelectExpressionWnd = import "L10.UI.CSelectExpressionWnd"
local Object = import "System.Object"
CSelectExpressionWnd.m_OnItemClick_CS2LuaHook = function (this, row) 
    if row >= 0 and row < this.dataSource.Count then
        local expressionId = this.dataSource[row]:GetID()
        if CSelectExpressionWnd.OnSelectExpression ~= nil then
            GenericDelegateInvoke(CSelectExpressionWnd.OnSelectExpression, Table2ArrayWithCount({expressionId}, 1, MakeArrayClass(Object)))
            this:Close()
        end
    end
end
CSelectExpressionWnd.m_ItemAt_CS2LuaHook = function (this, view, row) 
    if row < this.dataSource.Count then
        local item = TypeAs(view:GetFromPool(0), typeof(CSelectExpressionItem))
        local expressionId = this.dataSource[row]:GetID()
        item:Init(this.dataSource[row])
        return item
    end
    return nil
end
