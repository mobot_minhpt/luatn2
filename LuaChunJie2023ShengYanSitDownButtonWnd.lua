local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CMainCamera = import "L10.Engine.CMainCamera"
local Vector3 = import "UnityEngine.Vector3"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CCommonProgressInfoMgr = import "L10.UI.CCommonProgressInfoMgr"

LuaChunJie2023ShengYanSitDownButtonWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaChunJie2023ShengYanSitDownButtonWnd, "SitButton", GameObject)

RegistClassMember(LuaChunJie2023ShengYanSitDownButtonWnd, "m_CurrentStoolMonsterId")
RegistClassMember(LuaChunJie2023ShengYanSitDownButtonWnd, "m_TopAnchor")
RegistClassMember(LuaChunJie2023ShengYanSitDownButtonWnd, "m_CachedPos")
RegistClassMember(LuaChunJie2023ShengYanSitDownButtonWnd, "m_ViewPos")
RegistClassMember(LuaChunJie2023ShengYanSitDownButtonWnd, "m_ScreenPos")
RegistClassMember(LuaChunJie2023ShengYanSitDownButtonWnd, "m_TopWorldPos")
--@endregion RegistChildComponent end

function LuaChunJie2023ShengYanSitDownButtonWnd:Awake()
    UIEventListener.Get(self.SitButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSitButnClick()
	end)
end

function LuaChunJie2023ShengYanSitDownButtonWnd:Init()
	self.m_CachedPos = self.transform.position
    self:UpdateTopAnchor()
	self:Update()
end

function LuaChunJie2023ShengYanSitDownButtonWnd:Update()
    -- npcid发生更新时
	if self.m_CurrentStoolMonsterId ~= LuaZongMenMgr.TTSY_CurrentStoolMonster then
        self:UpdateTopAnchor()
    end

    if self.m_TopAnchor == nil then return end
    self.m_ViewPos = CMainCamera.Main:WorldToViewportPoint(self.m_TopAnchor)
    
    if self.m_ViewPos and self.m_ViewPos.z and self.m_ViewPos.z > 0 and 
            self.m_ViewPos.x > 0 and self.m_ViewPos.x < 1 and self.m_ViewPos.y > 0 and self.m_ViewPos.y < 1 then
        self.m_ScreenPos = CMainCamera.Main:WorldToScreenPoint(self.m_TopAnchor)
    else  
        self.m_ScreenPos = CommonDefs.op_Multiply_Vector3_Single(Vector3.up, 3000)
    end
    self.m_ScreenPos.z = 0
    self.m_TopWorldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(self.m_ScreenPos)

    if self.m_CachedPos.x ~= self.m_TopWorldPos.x or self.m_CachedPos.x ~= self.m_TopWorldPos.x then
        self.transform.position = self.m_TopWorldPos
        self.m_CachedPos = self.m_TopWorldPos
    end
end

function LuaChunJie2023ShengYanSitDownButtonWnd:UpdateTopAnchor()
    self.m_CurrentStoolMonsterId = LuaChunJie2023Mgr.TTSY_CurrentStoolMonster
	if self.m_CurrentStoolMonsterId ~= nil then
		local clientObj = CClientObjectMgr.Inst:GetObject(self.m_CurrentStoolMonsterId)
		if clientObj and clientObj.RO then
			self.m_TopAnchor = clientObj.RO.transform.position
		end
	end
end
    
--@region UIEvent
function LuaChunJie2023ShengYanSitDownButtonWnd:OnSitButnClick()
    Gac2Gas.Spring2023TTSY_SitDown(self.m_CurrentStoolMonsterId)
end

--@endregion UIEvent

