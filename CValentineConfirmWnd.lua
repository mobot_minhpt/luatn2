-- Auto Generated!!
local CValentineConfirmWnd = import "L10.UI.CValentineConfirmWnd"
local CValentineMgr = import "L10.UI.CValentineMgr"
local DelegateFactory = import "DelegateFactory"
local EnumValentineConfirmType = import "L10.UI.EnumValentineConfirmType"
local Gac2Gas = import "L10.Game.Gac2Gas"
local LocalString = import "LocalString"
local UIEventListener = import "UIEventListener"

CValentineConfirmWnd.m_OnEnable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.m_CloseBtn).onClick = DelegateFactory.VoidDelegate(function (obj) 
        this:Close()
    end)
    UIEventListener.Get(this.m_OkBtn).onClick = DelegateFactory.VoidDelegate(function (obj) 
        if CValentineMgr.Inst.m_ConfirmWndType == EnumValentineConfirmType.eAddFriend then
            Gac2Gas.AddFriend(CValentineMgr.Inst.m_TargetPlayerId, CValentineMgr.Inst.m_TargetPlayerName)
        elseif CValentineMgr.Inst.m_ConfirmWndType == EnumValentineConfirmType.eJoinTeam then
            Gac2Gas.InvitePlayerJoinTeam(CValentineMgr.Inst.m_TargetPlayerId)
        end
        this:Close()
    end)
    UIEventListener.Get(this.m_CancelBtn).onClick = DelegateFactory.VoidDelegate(function (obj) 
        this:Close()
    end)
end
CValentineConfirmWnd.m_Init_CS2LuaHook = function (this) 
    if CValentineMgr.Inst.m_ConfirmWndType == EnumValentineConfirmType.eAddFriend then
        this.m_ContentLabel.text = (LocalString.GetString("是否添加你的有缘人") .. CValentineMgr.Inst.m_TargetPlayerName) .. LocalString.GetString("为好友？")
        this.m_OkLabel.text = LocalString.GetString("添加好友")
    elseif CValentineMgr.Inst.m_ConfirmWndType == EnumValentineConfirmType.eJoinTeam then
        this.m_ContentLabel.text = (LocalString.GetString("是否邀请你的有缘人") .. CValentineMgr.Inst.m_TargetPlayerName) .. LocalString.GetString("组队？")
        this.m_OkLabel.text = LocalString.GetString("邀请组队")
    end
end
