local CUIFx = import "L10.UI.CUIFx"

local UIGrid = import "UIGrid"

local UITabBar = import "L10.UI.UITabBar"


local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CButton = import "L10.UI.CButton"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr=import "L10.UI.CItemInfoMgr"

LuaSeaFishingPlayWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaSeaFishingPlayWnd, "RecommendZhuangPingLebel", "RecommendZhuangPingLebel", UILabel)
RegistChildComponent(LuaSeaFishingPlayWnd, "DescLabel", "DescLabel", UILabel)
RegistChildComponent(LuaSeaFishingPlayWnd, "EnterBtn", "EnterBtn", GameObject)
RegistChildComponent(LuaSeaFishingPlayWnd, "TimesLabel", "TimesLabel", UILabel)
RegistChildComponent(LuaSeaFishingPlayWnd, "Grid", "Grid", UIGrid)
RegistChildComponent(LuaSeaFishingPlayWnd, "AwardItem", "AwardItem", GameObject)
RegistChildComponent(LuaSeaFishingPlayWnd, "AbsoluteDifficultyBtn", "AbsoluteDifficultyBtn", GameObject)
RegistChildComponent(LuaSeaFishingPlayWnd, "EasyBtn", "EasyBtn", GameObject)
RegistChildComponent(LuaSeaFishingPlayWnd, "NormalBtn", "NormalBtn", GameObject)
RegistChildComponent(LuaSeaFishingPlayWnd, "HardBtn", "HardBtn", GameObject)
RegistChildComponent(LuaSeaFishingPlayWnd, "DiyuBtn", "DiyuBtn", GameObject)
RegistChildComponent(LuaSeaFishingPlayWnd, "TabBar", "TabBar", UITabBar)

--@endregion RegistChildComponent end
RegistClassMember(LuaSeaFishingPlayWnd,"m_RewardedTimes")
RegistClassMember(LuaSeaFishingPlayWnd,"m_PassDifficult")
RegistClassMember(LuaSeaFishingPlayWnd,"m_SelectDifficult")
RegistClassMember(LuaSeaFishingPlayWnd,"m_CanEnterSpecialPlay")
function LuaSeaFishingPlayWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.EnterBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnEnterBtnClick()
	end)


	
	UIEventListener.Get(self.AbsoluteDifficultyBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnAbsoluteDifficultyBtnClick()
	end)


    --@endregion EventBind end
	self.m_PassDifficult = 0
	self.m_SelectDiffucult = 1
	self.TabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
		self:OnTabChange(go, index)
	end)
end

function LuaSeaFishingPlayWnd:Init()
	self.AwardItem:SetActive(false)
	self.m_RewardedTimes = LuaSeaFishingMgr.m_RewardedTimes
    self.m_PassDifficult = LuaSeaFishingMgr.m_PassDifficultRecord
    self.m_CanEnterSpecialPlay = LuaSeaFishingMgr.m_CanEnterSpecialPlay
	self.AbsoluteDifficultyBtn:SetActive(self.m_CanEnterSpecialPlay)
	if self.m_CanEnterSpecialPlay then
		self.AbsoluteDifficultyBtn:GetComponent(typeof(CUIFx)):LoadFx("fx/ui/prefab/UI_haidiao_rukou.prefab")
	end

	if not self.m_PassDifficult then
		self.m_PassDifficult = 0
	end

	local setting = HaiDiaoFuBen_Settings.GetData()
	self.TimesLabel.text = SafeStringFormat3(LocalString.GetString("本周参与 %d/%d"),self.m_RewardedTimes,setting.GameplayRewardLimit)
	self.TabBar:ChangeTab(0)
	self.m_SelectDifficult = 1

	self.DescLabel.text = setting.RuleDescription
	local recommendScore = setting.RecommendedEquipmentScore[self.m_SelectDifficult-1]
	self.RecommendZhuangPingLebel.text = SafeStringFormat3(LocalString.GetString("推荐装备评分 %d"),recommendScore)
end

--@region UIEvent
function LuaSeaFishingPlayWnd:OnEnterBtnClick()
	if self.m_SelectDifficult and self.m_SelectDifficult ~= 0 then
		local difficultyName = LuaSeaFishingMgr.DifficultName[self.m_SelectDifficult]
		local msg = g_MessageMgr:FormatMessage("HaiDaiFuBen_SignUp_Confim", difficultyName)
        MessageWndManager.ShowOKCancelMessage(msg,
            DelegateFactory.Action(function ()
                Gac2Gas.RequestEnterSeaFishingPlay(self.m_SelectDifficult,false)
            end),
            nil,
        LocalString.GetString("确定"), LocalString.GetString("取消"), false)
	end
end

function LuaSeaFishingPlayWnd:OnAbsoluteDifficultyBtnClick()
	if self.m_SelectDifficult and self.m_SelectDifficult ~= 0 then
		MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("Enter_SpecialHaiDiaoGameplay_Comfirm"), DelegateFactory.Action(function ()            
			Gac2Gas.RequestEnterSeaFishingPlay(self.m_SelectDifficult,true)
		end), nil, nil, nil, false)
	end
end

--@endregion UIEvent

function LuaSeaFishingPlayWnd:OnTabChange(go, index)
	for i=0,self.TabBar.tabButtons.Length-1,1 do
		local btn = self.TabBar:GetTabGoByIndex(i).transform:Find("Selected").gameObject
		btn:SetActive(i==index)
	end
	self.m_SelectDifficult = index + 1
	self:RefreshRewardView()

	local cbtn = self.EnterBtn.transform:GetComponent(typeof(CButton))
	if self.m_SelectDifficult > self.m_PassDifficult+1 then
		cbtn.Enabled = false
		g_MessageMgr:ShowMessage("SEAFISH_PLAY_LOCK_PREV_DIFFICULTY")
	else
		cbtn.Enabled = true
	end
end

function LuaSeaFishingPlayWnd:RefreshRewardView()
	Extensions.RemoveAllChildren(self.Grid.transform)
	local setting = HaiDiaoFuBen_Settings.GetData()
	local rewards = setting.GameplayRewardMails
	local mail = 0
	for i=0,rewards.Length-1,2 do
		local difficulty = rewards[i]
		local mailId = rewards[i+1]
		if difficulty == self.m_SelectDifficult then
			mail = mailId
			break
		end
	end

	if mail ~= 0 then
		local maildata = Mail_Mail.GetData(mail)
		local itemstr = maildata.Items
		for cronStr in string.gmatch(itemstr, "([^;]+);?") do
			local t = {}
			local itemId,count,bind = string.match(cronStr, "(%d+),(%d+),(%d+)")
			itemId,count,bind= tonumber(itemId),tonumber(count),tonumber(bind)
			local item = Item_Item.GetData(itemId)
			local award = NGUITools.AddChild(self.Grid.gameObject,self.AwardItem)
            award:SetActive(true)
			local icon = award.transform:Find("Icon"):GetComponent(typeof(CUITexture))
			icon:LoadMaterial(item.Icon)
			UIEventListener.Get(award).onClick = DelegateFactory.VoidDelegate(function (go)
				CItemInfoMgr.ShowLinkItemTemplateInfo(itemId,false,nil,AlignType.Default, 0, 0, 0, 0)
			end)
		
		end
	end
	self.Grid:Reposition()

	local recommendScore = setting.RecommendedEquipmentScore[self.m_SelectDifficult-1]
	self.RecommendZhuangPingLebel.text = SafeStringFormat3(LocalString.GetString("推荐装备评分 %d"),recommendScore)
end
