local Time = import "UnityEngine.Time"
local TweenAlpha=import "TweenAlpha"
local TextureFormat = import "UnityEngine.TextureFormat"
local Texture2D = import "UnityEngine.Texture2D"
local System = import "System"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local Screen = import "UnityEngine.Screen"
local RuntimePlatform = import "UnityEngine.RuntimePlatform"
local RenderTexture = import "UnityEngine.RenderTexture"
local Rect = import "UnityEngine.Rect"
local QnButton = import "L10.UI.QnButton"
local PlayerSettings = import "L10.Game.PlayerSettings"
local PaiZhao_Setting = import "L10.Game.PaiZhao_Setting"
local Object1 = import "UnityEngine.Object"
local NativeTools = import "L10.Engine.NativeTools"
local MouseInputHandler = import "L10.Engine.MouseInputHandler"
local HuYiTianHouse_Setting = import "L10.Game.HuYiTianHouse_Setting"
local GLTextManager = import "GLTextManager"
local GameObject = import "UnityEngine.GameObject"
local File = import "System.IO.File"
local EventManager = import "EventManager"
local ETickType = import "L10.Engine.ETickType"
local EShareType = import "L10.UI.EShareType"
local EnumEventType = import "EnumEventType"
local CTickMgr = import "L10.Engine.CTickMgr"
local CScreenCaptureWnd = import "L10.UI.CScreenCaptureWnd"
local CScene = import "L10.Game.CScene"
local CPostEffectMgr = import "L10.Engine.CPostEffectMgr"
local CPostProcessingMgr = import "L10.Engine.PostProcessing.CPostProcessingMgr"
local CPaiZhaoMgr = import "CPaiZhaoMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local Color = import "UnityEngine.Color"
local CMainCamera = import "L10.Engine.CMainCamera"
local CLoginMgr = import "L10.Game.CLoginMgr"
local CJoystickWnd = import "L10.UI.CJoystickWnd"
local CFileTools = import "CFileTools"
local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local CameraClearFlags = import "UnityEngine.CameraClearFlags"
local Application = import "UnityEngine.Application"
local Extensions = import "Extensions"
local CSettingWnd_BlockSetting = import "L10.UI.CSettingWnd_BlockSetting"
local UILabel = import "UILabel"
local QnCheckBox = import "L10.UI.QnCheckBox"
local GestureRecognizer = import "L10.Engine.GestureRecognizer"
local GestureType = import "L10.Engine.GestureType"
local EasyTouch = import "EasyTouch"
local HousePopupMenuItemData = import "L10.UI.HousePopupMenuItemData"
local CHousePopupMgr = import "L10.UI.CHousePopupMgr"
local Physics = import "UnityEngine.Physics"
local Tags = import "L10.Game.Tags"
local EBarrierType = import "L10.Engine.EBarrierType"
local CoreScene = import "L10.Engine.Scene.CoreScene"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CRenderObject = import "L10.Engine.CRenderObject"
local CWater = import "L10.Engine.CWater"
local TweenScale = import "TweenScale"
local Constants = import "L10.Game.Constants"
local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local AlignType = import "L10.UI.CPopupMenuInfoMgr+AlignType"
local CIKMgr = import "L10.Game.CIKMgr"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local UIScrollViewIndicator = import "UIScrollViewIndicator"
local CLookAtIKTargetCheck = import "L10.Game.CLookAtIKTargetCheck"
local CRenderScene = import "L10.Engine.Scene.CRenderScene"
local CClientOtherPlayer = import "L10.Game.CClientOtherPlayer"
local CClientObject = import "L10.Game.CClientObject"
local DelegateFactory = import "DelegateFactory"
local QnCheckBoxGroup = import "L10.UI.QnCheckBoxGroup"
local CClientObjectRoot = import "L10.Game.CClientObjectRoot"
local QnNewSlider=import "L10.UI.QnNewSlider"
local CTiezhiItem=import "L10.UI.CTiezhiItem"
local Camera=import "UnityEngine.Camera"
local Main = import "L10.Engine.Main"
local NormalCamera = import "L10.Engine.CameraControl.NormalCamera"
local CExpressionActionMgr = import "L10.Game.CExpressionActionMgr"
local EnumActionState = import "L10.Game.EnumActionState"

CLuaScreenCaptureWnd = class()
CLuaScreenCaptureWnd.Path = "ui/screencapture/LuaScreenCaptureWnd"
CLuaScreenCaptureWnd.bInScenePhoto = false
CLuaScreenCaptureWnd.ScenePhotoId = 1
CLuaScreenCaptureWnd.CameraFollowResetToDefaultOnDestroy = true
RegistClassMember(CLuaScreenCaptureWnd,"m_LocalSaveButton")
RegistClassMember(CLuaScreenCaptureWnd,"m_BlankTextureTween")
RegistClassMember(CLuaScreenCaptureWnd,"m_SettingButton")
RegistClassMember(CLuaScreenCaptureWnd,"m_expressionButton")
RegistClassMember(CLuaScreenCaptureWnd,"m_expressionView")
RegistClassMember(CLuaScreenCaptureWnd,"m_ExpressionCloseButton")
RegistClassMember(CLuaScreenCaptureWnd,"m_IconGO")
RegistClassMember(CLuaScreenCaptureWnd,"m_TiezhiButton")
RegistClassMember(CLuaScreenCaptureWnd,"m_TiezhiView")
RegistClassMember(CLuaScreenCaptureWnd,"m_Camera")
RegistClassMember(CLuaScreenCaptureWnd,"tiezhiPrefab")
RegistClassMember(CLuaScreenCaptureWnd,"qipaoPrefab")
RegistClassMember(CLuaScreenCaptureWnd,"additiveRoot")
RegistClassMember(CLuaScreenCaptureWnd,"colorSlider")
RegistClassMember(CLuaScreenCaptureWnd,"cameraSlider")
RegistClassMember(CLuaScreenCaptureWnd,"m_previewButton")
RegistClassMember(CLuaScreenCaptureWnd,"FunctionButtonRoot")
RegistClassMember(CLuaScreenCaptureWnd,"colorTexture")
RegistClassMember(CLuaScreenCaptureWnd,"m_CachedTexture")
RegistClassMember(CLuaScreenCaptureWnd,"oriMainPlayerROOcclusionEffect")
RegistClassMember(CLuaScreenCaptureWnd,"oriMainPlayerZuoQiROOcclusionEffect")
RegistClassMember(CLuaScreenCaptureWnd,"oriHideNPCHeadInfo")
RegistClassMember(CLuaScreenCaptureWnd,"m_HideExcept")
RegistClassMember(CLuaScreenCaptureWnd, "m_CloseBtnObj")

RegistClassMember(CLuaScreenCaptureWnd,"m_EvaluateInterval")
RegistClassMember(CLuaScreenCaptureWnd,"m_LastEvaluateTime")
RegistClassMember(CLuaScreenCaptureWnd,"m_RefreshButtonTick")

RegistClassMember(CLuaScreenCaptureWnd, "m_bBabyExpressionInited")

-- For render the icon
RegistClassMember(CLuaScreenCaptureWnd, "m_CaptureFrame")

-- Icon and QRCode
RegistClassMember(CLuaScreenCaptureWnd, "m_CodeObj")
RegistClassMember(CLuaScreenCaptureWnd, "m_IconPosX")
RegistClassMember(CLuaScreenCaptureWnd, "m_CodePosX")

-- forbid to reset camera pinch
RegistClassMember(CLuaScreenCaptureWnd, "m_CameraSliderSetCount")

RegistClassMember(CLuaScreenCaptureWnd, "m_AlbumRootObj")
RegistClassMember(CLuaScreenCaptureWnd, "m_PreviewPhotoRootObj")

RegistClassMember(CLuaScreenCaptureWnd, "m_LastShowAllObject")

RegistClassMember(CLuaScreenCaptureWnd, "m_CaptureHappened") --标记是否进行过拍照

RegistClassMember(CLuaScreenCaptureWnd, "m_OriginalNeedUpdateAABB")

RegistClassMember(CLuaScreenCaptureWnd, "m_SettingsNeedCache")
RegistClassMember(CLuaScreenCaptureWnd, "m_CachedPlayerSettings")
RegistClassMember(CLuaScreenCaptureWnd, "m_LastAppearPropertySettings")
RegistClassMember(CLuaScreenCaptureWnd, "m_IsdofInited")

RegistClassMember(CLuaScreenCaptureWnd,"m_CameraMinY")
RegistClassMember(CLuaScreenCaptureWnd,"m_CameraMaxY")

function CLuaScreenCaptureWnd:Awake()
    self.m_LocalSaveButton = self.transform:Find("Camera/Panel/Root/SaveLocalButton"):GetComponent(typeof(QnButton))
    self.m_BlankTextureTween = self.transform:Find("BlankTexture"):GetComponent(typeof(TweenAlpha))
    self.m_SettingButton = self.transform:Find("Camera/Panel/Root/Panel/SettingButton"):GetComponent(typeof(QnButton))
    self.m_expressionButton = self.transform:Find("Camera/Panel/Root/ExpressionButton"):GetComponent(typeof(QnButton))
    self.m_expressionView = self.transform:Find("Camera/Panel/Root/Expressions").gameObject
    self.m_expressionView:SetActive(false)
    self.m_ExpressionCloseButton = self.transform:Find("Camera/Panel/Root/Expressions/Bg/Close/CloseExpressionButton").gameObject
    self.m_IconGO = self.transform:Find("Texture").gameObject
    self.m_IconGO:SetActive(false)
    self.m_IconPosX = self.m_IconGO.transform.localPosition.x
    self.m_CodeObj = self.transform:Find("OfficialQRCode").gameObject
    self.m_CodeObj:SetActive(false)
    self.m_CodePosX = self.m_CodeObj.transform.localPosition.x
    self.m_CodeObj:GetComponent(typeof(UITexture)).enabled = false

    self.m_TiezhiButton = self.transform:Find("Camera/Panel/Root/TiezhiButton"):GetComponent(typeof(QnButton))
    self.m_TiezhiView = self.transform:Find("Camera/Panel/Root/AdditiveItems").gameObject
    self.m_Camera = self.transform:Find("Camera"):GetComponent(typeof(Camera))
    self.tiezhiPrefab = self.transform:Find("Prefabs/TiezhiTextureItem"):GetComponent(typeof(CTiezhiItem))
    self.qipaoPrefab = self.transform:Find("Prefabs/TiezhiTextItem"):GetComponent(typeof(CTiezhiItem))
    self.additiveRoot = self.transform:Find("AdditiveRoot").gameObject
    self.colorSlider = self.transform:Find("Camera/Panel/Root/AdditiveItems/ColorSlider"):GetComponent(typeof(QnNewSlider))
    self.cameraSlider = self.transform:Find("Camera/Panel/Root/PininSlider"):GetComponent(typeof(QnNewSlider))
    self.m_previewButton = self.transform:Find("Camera/Panel/TopRightRoot/PreviewButton").gameObject
    self:ChangeAlpha(self.m_previewButton, true)
    self.FunctionButtonRoot = self.transform:Find("Camera/Panel/Root").gameObject
    self.FunctionButtonRoot:SetActive(true)
    self.m_ConfigRootObj = self.transform:Find("Camera/Panel/ConfigRoot").gameObject
    self.m_ConfigRootObj:SetActive(false)
    self.m_AlbumRootObj = self.transform:Find("Camera/Panel/AlbumRoot").gameObject
    self.m_AlbumRootObj:SetActive(false)
    self.m_CloseBtnObj = self.transform:Find("Camera/Panel/CloseButton").gameObject

    self.m_EvaluateInterval = 2
    self.m_LastEvaluateTime = 0
    local hideExcept = { "HeadInfoWnd", "MiddleNoticeCenter", "PopupNoticeWnd", "Joystick" }
    self.m_HideExcept = Table2List(hideExcept, MakeGenericClass(List, cs_string))
    self.m_CaptureFrame = -1
    self.m_bBabyExpressionInited = false
    self.m_CameraSliderSetCount = 0
    self.m_LastShowAllObject = false
    self.m_CaptureHappened = false
    self.m_IsdofInited = false
    --使用HouseCameraMinY的话摄像机的最低高度 比 视角拉到最近的时候镜头对准角色的高度 更高，导致镜头对不准角色
    self.m_CameraMinY = 0.3
    self.m_CameraMaxY = Zhuangshiwu_Setting.GetData().HouseCameraMaxY
    self:InitTopRight()
end

-- Auto Generated!!
function CLuaScreenCaptureWnd:Init( )
    self.m_LocalSaveButton.OnClick = DelegateFactory.Action_QnButton(function (button)
      self:OnLocalSaveButtonClick(button)
    end)
    self.m_SettingButton.OnClick = DelegateFactory.Action_QnButton(function ()
      self:OnClickSettingButton()
    end)
    self.m_expressionButton.OnClick = DelegateFactory.Action_QnButton(function (button)
      self:OnExpressionButtonClick(button)
    end)
    self.m_TiezhiButton.OnClick = DelegateFactory.Action_QnButton(function ()
      self:OnTiezhiButtonClick()
    end)
    self.colorSlider.OnValueChanged = DelegateFactory.Action_float(function(v)
      self:onColorSliderValueChange(v)
    end)
    self.cameraSlider.OnValueChanged = DelegateFactory.Action_float(function(v)
      self:onCameraSliderValueChange(v)
    end)
    UIEventListener.Get(self.m_ExpressionCloseButton).onClick = LuaUtils.VoidDelegate(function()
      self.m_expressionView:SetActive(false)
    end)
    UIEventListener.Get(self.m_previewButton).onClick = LuaUtils.VoidDelegate(function(go)
      self:OnPreviewClick(go)
    end)
    CUIManager.instance:HideViewsByChangeLayer(CUIManager.EUIModuleGroup.EGameUI, self.m_HideExcept, true, false, "ScreenCaptureWnd")
    CUIManager.instance:HideViewsByChangeLayer(CUIManager.EUIModuleGroup.EBgGameUI, nil, false, false, "ScreenCaptureWnd")

    self.m_BlankTextureTween:SetEndToCurrentValue()
    CScreenCaptureWnd.IsInCaptureMode = true
    g_ScriptEvent:BroadcastInLua("CaptureModeOnOff", true)

    if self.m_RefreshButtonTick ~= nil then
      invoke(self.m_RefreshButtonTick)
        self.m_RefreshButtonTick = nil
    end
    self.m_RefreshButtonTick = RegisterTick(function()
      local isShareBoxLoaded = CUIManager.IsLoaded(CUIResources.ShareBox3)
      self.m_Camera.enabled = not isShareBoxLoaded
      self.m_Camera:GetComponent(typeof(UICamera)).enabled = not isShareBoxLoaded
    end, 300)

    if CommonDefs.IsPCGameMode() then
      local CWinSocialWndMgr = import "L10.UI.CWinSocialWndMgr"
      if CWinSocialWndMgr.Inst.m_IsWinSocialWndOpened and self.m_Camera then
        self.m_Camera.rect = Rect(0, 0, 1 - Constants.WinSocialWndRatio, 1)
      end
    end

    self:OnSettingChanged()

    self:MainPlayerMoveEnded()

    CLuaScreenCaptureMgr:ClearCurrentData()
    Gac2Gas.QueryLingShouBabyInfoForCamara()
    Gac2Gas.RequestAllSceneryPhotoAlbumUrl()

    CClientObjectRoot.Inst:HideMonster(PlayerSettings.HideMonsterInScreenCaptureWndEnabled)

    self:ProcessScenePhotoMode()
end

function CLuaScreenCaptureWnd:OnEnable( )
    self:CachePlayerSetting()
    if Application.platform == RuntimePlatform.IPhonePlayer and Main.Inst.EngineVersion > 426880 then
        --注意IsUseTempSetting只影响部分setting
        PlayerSettings.IsUseTempSetting = true
    end

    if CClientMainPlayer.Inst ~= nil then
        self.oriMainPlayerROOcclusionEffect = CClientMainPlayer.Inst.RO.OcclusionEffectVisible
        if CClientMainPlayer.Inst:IsInVehicle() then
            self.oriMainPlayerZuoQiROOcclusionEffect = CClientMainPlayer.Inst.VehicleRO.OcclusionEffectVisible
            CClientMainPlayer.Inst.VehicleRO.OcclusionEffectVisible = false
        end
        if CClientMainPlayer.Inst.RO.KuiLeiRO ~= nil then
            CClientMainPlayer.Inst.RO.KuiLeiRO.OcclusionEffectVisible = false
        end
        if CClientMainPlayer.Inst.RO.KuiLeiCloseRO ~= nil then
            CClientMainPlayer.Inst.RO.KuiLeiCloseRO.OcclusionEffectVisible = false
        end
        CClientMainPlayer.Inst.RO.OcclusionEffectVisible = false
    end

    g_ScriptEvent:AddListener("CaptureHideNoticeWndSettingChanged", self, "CaptureHideNoticeWndSettingChanged")
    g_ScriptEvent:AddListener("SelfieAddItem", self, "SelfieAddItem")
    g_ScriptEvent:AddListener("ClientObjDie", self, "ClientObjDie")
    g_ScriptEvent:AddListener("OnHeadInfoOptionChangeBySetting", self, "OnHeadInfoOptionChangeBySetting")
    g_ScriptEvent:AddListener("OnClientObjectSelected", self, "OnClientObjectSelected")
    g_ScriptEvent:AddListener("MainPlayerMoveEnded", self, "MainPlayerMoveEnded")
    g_ScriptEvent:AddListener("JoyStickDragging", self, "MainPlayerMove")
    g_ScriptEvent:AddListener("PlayerDragJoyStick", self, "MainPlayerMove")
    g_ScriptEvent:AddListener("OnRideOnOffVehicle", self, "MainPlayerMove")
    g_ScriptEvent:AddListener("PlayerDetachFurniture", self, "OnPlayerDetachFurniture")
    

    MouseInputHandler.Inst.ForbidClickMove = true

    CPaiZhaoMgr.Instance.BlockMode = true
    CPaiZhaoMgr.Instance:ResetShowType()
    Gac2Gas.RequestRelationPlayerIds()
    g_ScriptEvent:AddListener("ShareFinished", self, "OnShareFinished")
    self.m_OriginalNeedUpdateAABB = CClientMainPlayer.Inst and CClientMainPlayer.Inst.RO and CClientMainPlayer.Inst.RO.NeedUpdateAABB
    self:SetMainPlayerAABBUpdate(true)
    CIKMgr.Inst:TrySwitchIKHeadNearBy(true)
    CIKMgr.LookAtIKInteralSwitch = false
    CIKMgr.Inst:AdjustMainPlayerLookAtIK(true, true)
    if CRenderScene.Inst then
        self.m_LastShowAllObject = CRenderScene.Inst.ShowAllObject
        CRenderScene.Inst.ShowAllObject = true
    end

    self:LoadPaiZhaoSetting()

    CUIManager.instance:HideUI(InitializeList(CreateFromClass(MakeGenericClass(List, String)), String, "HouseRollerCoasterWnd"))
end
function CLuaScreenCaptureWnd:OnDisable( )
    MouseInputHandler.Inst.ForbidClickMove = false
    if CClientMainPlayer.Inst ~= nil then
        if CClientMainPlayer.Inst:IsInVehicle() then
            CClientMainPlayer.Inst.VehicleRO.OcclusionEffectVisible = self.oriMainPlayerZuoQiROOcclusionEffect
        end
        if CClientMainPlayer.Inst.RO.KuiLeiRO ~= nil then
            CClientMainPlayer.Inst.RO.KuiLeiRO.OcclusionEffectVisible = self.oriMainPlayerROOcclusionEffect
        end
        if CClientMainPlayer.Inst.RO.KuiLeiCloseRO ~= nil then
            CClientMainPlayer.Inst.RO.KuiLeiCloseRO.OcclusionEffectVisible = self.oriMainPlayerROOcclusionEffect
        end
        CClientMainPlayer.Inst.RO.OcclusionEffectVisible = self.oriMainPlayerROOcclusionEffect
    end

    g_ScriptEvent:RemoveListener("CaptureHideNoticeWndSettingChanged", self, "CaptureHideNoticeWndSettingChanged")
    g_ScriptEvent:RemoveListener("SelfieAddItem", self, "SelfieAddItem")
    g_ScriptEvent:RemoveListener("ClientObjDie", self, "ClientObjDie")
    g_ScriptEvent:RemoveListener("OnHeadInfoOptionChangeBySetting", self, "OnHeadInfoOptionChangeBySetting")
    g_ScriptEvent:RemoveListener("OnClientObjectSelected", self, "OnClientObjectSelected")
    g_ScriptEvent:RemoveListener("MainPlayerMoveEnded", self, "MainPlayerMoveEnded")
    g_ScriptEvent:RemoveListener("JoyStickDragging", self, "MainPlayerMove")
    g_ScriptEvent:RemoveListener("PlayerDragJoyStick", self, "MainPlayerMove")
    g_ScriptEvent:RemoveListener("OnRideOnOffVehicle", self, "MainPlayerMove")
    g_ScriptEvent:RemoveListener("PlayerDetachFurniture", self, "OnPlayerDetachFurniture")

    
    CIKMgr.Inst:TrySwitchIKHeadNearBy(false)
    CIKMgr.Inst:SetMainPlayerIKLookAtToTarget(nil, false)
    CScreenCaptureWnd.IsInCaptureIKMode = false
    if self.m_IKTargetInRoot then
        GameObject.Destroy(self.m_IKTargetInRoot)
    end
    if self.m_IKTarget and self.m_IKTarget:GetComponent(typeof(CLookAtIKTargetCheck)) then
        GameObject.Destroy(self.m_IKTarget:GetComponent(typeof(CLookAtIKTargetCheck)))
    end

    if CPostProcessingMgr.s_NewPostProcessingOpen then
      local postEffectCtrl = CPostProcessingMgr.Instance.MainController
      postEffectCtrl.DepthOfFieldEnable = false
      postEffectCtrl:LoadLvJingEffect(0, 0)
    else
      CPostEffectMgr.Instance.DepthOfFieldEnable = false
      CPostEffectMgr.Instance:DisableAllFilterEffect()
    end

    CPaiZhaoMgr.Instance.BlockMode = false
    if CClientMainPlayer.Inst ~= nil then
        CClientMainPlayer.Inst:LeaveOfflineExpressionAction()
        CClientMainPlayer.Inst:RequestShowExpressionAction(0)
        CClientPlayerMgr.Inst:RefreshAllVisablePlayer()
    end

    g_ScriptEvent:RemoveListener("ShareFinished", self, "OnShareFinished")

    self:SetMainPlayerAABBUpdate(self.m_OriginalNeedUpdateAABB)

    if CRenderScene.Inst then
      CRenderScene.Inst.ShowAllObject = self.m_LastShowAllObject
    end

    if Application.platform == RuntimePlatform.IPhonePlayer and Main.Inst.EngineVersion > 426880 then
      PlayerSettings.IsUseTempSetting = false
    end
    self:RecoveryCachePlayerSetting()

    CUIManager.instance:ShowUI(InitializeList(CreateFromClass(MakeGenericClass(List, String)), String, "HouseRollerCoasterWnd"))
end
function CLuaScreenCaptureWnd:SetMainPlayerAABBUpdate( needUpdate)
    if CClientMainPlayer.Inst ~= nil then
        CClientMainPlayer.Inst.RO.NeedUpdateAABB = needUpdate
    end
end

function CLuaScreenCaptureWnd:Update()
  self.cameraSlider.m_Value = CameraFollow.Inst.TValue / 2 + 0.5

  -- DO Capture Screen
  if self.m_CaptureFrame >= 0 then
    if self.m_CaptureFrame == 0 then
      self.m_CaptureFrame = self.m_CaptureFrame + 1
    else
      self:captureScreen()
      CJoystickWnd.Instance.gameObject:SetActive(true)
      self.m_IconGO:SetActive(false)
      self.m_CodeObj:SetActive(false)

      Gac2Gas.PlayerCaptureScreenInGame()
      self.m_CaptureHappened = true

      self.m_CaptureFrame = -1
    end
  end

  -- Show or hide the ConfigRoot
  if Input.GetMouseButtonDown(0) and self.m_ConfigRootObj.activeSelf then
    if (not CUICommonDef.IsOverUI) or (UICamera.lastHit.collider and (not CUICommonDef.IsChild(self.m_ConfigRootObj.transform, UICamera.lastHit.collider.transform))) then
      self.m_ConfigRootObj:SetActive(false)
      self.FunctionButtonRoot:SetActive(true)
      CJoystickWnd.Instance.gameObject:SetActive(true)
    end
  end

  -- Show or hide the AlbumRoot
  if Input.GetMouseButtonDown(0) and self.m_AlbumRootObj.activeSelf then
    if (not CUICommonDef.IsOverUI) or (UICamera.lastHit.collider and (not CUICommonDef.IsChild(self.m_AlbumRootObj.transform, UICamera.lastHit.collider.transform)) and (not CUICommonDef.IsChild(self.m_PhotoButtonObj.transform, UICamera.lastHit.collider.transform))) then
      self.m_AlbumRootObj:SetActive(false)
      self.FunctionButtonRoot:SetActive(true)
      CJoystickWnd.Instance.gameObject:SetActive(true)
      self:ChangeAlpha(self.m_PhotoButtonObj, true)
    end
  end

  self:ProcessPlaceMode()

  self:ProcessFocus()

  if self.m_IsdofInited == false then
    self:TryInitdofEffct()
  end

    if not self.m_IsPressing then
        --摄像机position.y上下限
        if self.m_CameraTrans == nil then
            self.m_CameraTrans = CameraFollow.Inst.transform:Find("camRotate/newParent")
        end

        local pos = Vector3(self.m_CameraTrans.position.x, self.m_CameraTrans.position.y, self.m_CameraTrans.position.z)
        if CScene.MainScene then
            local logicHeight = CScene.MainScene:GetLogicHeight(math.floor(self.m_CameraTrans.position.x * 64), math.floor(self.m_CameraTrans.position.z * 64))
            if self.m_CameraTrans.position.y < math.max(CWater.s_WaterHeight, logicHeight) + self.m_CameraMinY then
                pos.y = math.max(CWater.s_WaterHeight, logicHeight) + self.m_CameraMinY
                self.m_CameraTrans.position = pos
                self.m_CameraLocalPositionTable[2] = self.m_CameraTrans.localPosition.y
            end
            
            if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.RO ~= nil then
                local RO = CClientMainPlayer.Inst.RO
                local targetPos = RO:GetPosition()
                local playerLogicHeight = CScene.MainScene:GetLogicHeight(math.floor(targetPos.x * 64), math.floor(targetPos.z * 64))
                if self.m_CameraTrans.position.y > playerLogicHeight + self.m_CameraMaxY then
                    self.m_CameraTrans.position.y = playerLogicHeight + self.m_CameraMaxY
                    self.m_CameraLocalPositionTable[2] = self.m_CameraTrans.localPosition.y
                end
            end

        end
    end

end

function CLuaScreenCaptureWnd:OnLocalSaveButtonClick( button)

    --限制按钮点击频率
    if Time.realtimeSinceStartup - self.m_LastEvaluateTime < self.m_EvaluateInterval then
        --MessageMgr.Inst.ShowMessage("RPC_TOO_FREQUENT");
        return
    end
    self.m_LastEvaluateTime = Time.realtimeSinceStartup

    EventManager.BroadcastInternalForLua(EnumEventType.OnTiezhiItemEditEnable, {0})

    self:DelayCaptureScreen()
end

function CLuaScreenCaptureWnd:DelayCaptureScreen()
  CJoystickWnd.Instance.gameObject:SetActive(false)
  self.m_IconGO:SetActive(true)
  self.m_CodeObj:SetActive(true)
  self.m_CaptureFrame = 0
end

function CLuaScreenCaptureWnd:captureScreen( )
    local filename = Utility.persistentDataPath .. "/screenshot.jpg"
    local data = nil
      self.m_CachedTexture = self:CaptureScreen()
      data = CommonDefs.EncodeToJPG(self.m_CachedTexture)

    if Application.platform == RuntimePlatform.WindowsPlayer or Application.platform == RuntimePlatform.WindowsEditor then
        local CWinUtility = import "L10.UI.CWinUtility"
        local fileName
        local default
        default, fileName = CWinUtility.Inst:GetSaveFileName("jpg")
        if default then
            File.WriteAllBytes(fileName, data)

            -- ShareBoxMgr.OpenHouseCompetitionShareBox(fileName)
            self:Share(fileName)
        end
    else
        if pcall(function() NativeTools.SaveImage(data) end) then
            if Application.platform == RuntimePlatform.Android then
                g_MessageMgr:ShowMessage("SAVE_PICTURE_SUCCEED")
            end
        end
    end

    self:ClearTexture()

    self.m_BlankTextureTween:ResetToBeginning()
    self.m_BlankTextureTween:PlayForward()
    self:showShareBox(filename)
end
function CLuaScreenCaptureWnd:CaptureScreen( )
    CScreenCaptureWnd.IsCapturing = true
    local scale = 1 * 1920 / Screen.width

    local rect = nil
    if CPostProcessingMgr.s_NewPostProcessingOpen then
      rect = CreateFromClass(Rect, 0, 0, CMainCamera.CaptureCamera.pixelWidth, CMainCamera.CaptureCamera.pixelHeight)
    else
      rect = CreateFromClass(Rect, 0, 0, CPostEffectMgr.Instance.width, CPostEffectMgr.Instance.height)
    end

    GLTextManager.Visible = false
    local rt = CreateFromClass(RenderTexture, math.floor(rect.width), math.floor(rect.height), 24)
    local screenShot = CreateFromClass(Texture2D, math.floor(rect.width), math.floor(rect.height), TextureFormat.RGB24, false)
    local tmpFlags = CMainCamera.CaptureCamera.clearFlags
    if CScene.MainScene ~= nil and CScene.MainScene.SkyBoxEnabled then
        CMainCamera.CaptureCamera.clearFlags = CameraClearFlags.Skybox
    else
        CMainCamera.CaptureCamera.clearFlags = CameraClearFlags.Color
    end

    CMainCamera.CaptureCamera.targetTexture = rt
    local oldr = CMainCamera.CaptureCamera.rect
    CMainCamera.CaptureCamera.rect = CreateFromClass(Rect, 0, 0, 1, 1)
    CMainCamera.CaptureCamera:Render()
    CMainCamera.CaptureCamera.rect = oldr
    --      saveRenderTexture(rt, 1);
    CUIManager.UIMainCamera.rect = CreateFromClass(Rect, 0, 0, 1, 1)
    CUIManager.UIMainCamera.targetTexture = rt
    CUIManager.UIMainCamera:Render()
    CUIManager.UIMainCamera.rect = oldr
    --    saveRenderTexture(rt, 2);
    local old = RenderTexture.active
    RenderTexture.active = rt
    screenShot:ReadPixels(rect, 0, 0)
    screenShot:Apply()

    --   CMainCamera.Main.clearFlags = tmpFlags;
    CMainCamera.CaptureCamera.targetTexture = nil
    CUIManager.UIMainCamera.targetTexture = nil
    RenderTexture.active = old
    GameObject.Destroy(rt)

    local bytes = CommonDefs.EncodeToJPG(screenShot)
    local filename = Utility.persistentDataPath .. "/screenshot.jpg"
    System.IO.File.WriteAllBytes(filename, bytes)
    CFileTools.SetFileNoBackup(filename)

    CScreenCaptureWnd.IsCapturing = false
    GLTextManager.Visible = true
    return screenShot
end

function CLuaScreenCaptureWnd:Share(fileName)
    if not CLuaShareMgr.TryShareHouseCompetition(fileName) then
        ShareBoxMgr.OpenHouseCompetitionShareBox(fileName)
    end
end

function CLuaScreenCaptureWnd:showShareBox( fileName)
	if CommonDefs.IS_CN_CLIENT then
		if Application.platform == RuntimePlatform.WindowsPlayer or Application.platform == RuntimePlatform.WindowsEditor then
		    CTickMgr.Register(DelegateFactory.Action(function ()
		        -- ShareBoxMgr.OpenHouseCompetitionShareBox(fileName)
		        self:Share(fileName)
		    end), 1000, ETickType.Once)
		else
            --都需要打开分享界面，渠道服需要可以分享梦岛
            CTickMgr.Register(DelegateFactory.Action(function ()
                self:Share(fileName)
            end), 1000, ETickType.Once)
		end
    else
        CTickMgr.Register(DelegateFactory.Action(function ()
            ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, {fileName})
        end), 1000, ETickType.Once)
    end
end

function CLuaScreenCaptureWnd:ClearTexture( )
    if self.m_CachedTexture ~= nil then
        Object1.Destroy(self.m_CachedTexture)
        self.m_CachedTexture = nil
    end
end
function CLuaScreenCaptureWnd:onAddSelfieItem(id, type, density)
    local t = math.floor(type)
    repeat
        local default = t
        if default == 0 then
            self:addTiezhiItem(math.floor(id))
            break
        elseif default == 1 then
            self:addLvjing(math.floor(id), density)
            break
        elseif default == 2 then
            self:addQipaoItem(math.floor(id))
            break
        end
    until 1
end
function CLuaScreenCaptureWnd:addTiezhiItem( id)
    local count = self.additiveRoot.transform.childCount
    if count >= PaiZhao_Setting.GetData().Max_tiezhi then
        g_MessageMgr:ShowMessage("MAX_TIEZHICOUNT_REACHED")
        return
    end
    local go = CUICommonDef.AddChild(self.additiveRoot, self.tiezhiPrefab.gameObject)
    local tiezhi = CommonDefs.GetComponent_GameObject_Type(go, typeof(CTiezhiItem))
    tiezhi:SetDepth(count + 1)
    tiezhi:SetTypeAndID(0, id)
    go:SetActive(true)
end
function CLuaScreenCaptureWnd:addQipaoItem( id)
    local count = self.additiveRoot.transform.childCount
    if count >= PaiZhao_Setting.GetData().Max_tiezhi then
        g_MessageMgr:ShowMessage("MAX_TIEZHICOUNT_REACHED")
        return
    end
    local go = CUICommonDef.AddChild(self.additiveRoot, self.qipaoPrefab.gameObject)
    local qipao = CommonDefs.GetComponent_GameObject_Type(go, typeof(CTiezhiItem))
    qipao:SetDepth(count + 1)
    qipao:SetTypeAndID(2, id)
    go:SetActive(true)
end

function CLuaScreenCaptureWnd:addLvjing(id, density)
    if CPostProcessingMgr.s_NewPostProcessingOpen then
        local postEffectCtrl = CPostProcessingMgr.Instance.MainController
        postEffectCtrl:LoadLvJingEffect(id, density)
    else
        CPostEffectMgr.Instance:LoadEffectFromPrefab(id, density)
    end
end

function CLuaScreenCaptureWnd:OnExpressionButtonClick( go)
    self.m_expressionView:SetActive(not self.m_expressionView.activeSelf)
    if self.m_expressionView.activeSelf then
        self.m_TiezhiView:SetActive(false)
        --CIKMgr.LookAtIKInteralSwitch = false
        --CIKMgr.Inst:AdjustMainPlayerLookAtIK(true, true)
        self:CloseIKMode()
        --CIKMgr.Inst:SetMainPlayerIKLookAtToTarget(nil)
    end
end

function CLuaScreenCaptureWnd:onColorSliderValueChange( v)
    if self.colorTexture == nil then
        local tex = CommonDefs.GetComponent_Component_Type(self.colorSlider, typeof(UITexture))
        self.colorTexture = TypeAs(tex.mainTexture, typeof(Texture2D))
    end
    local c = self.colorTexture:GetPixel(math.floor(((1 - v) * self.colorTexture.width)), 1)
    EventManager.BroadcastInternalForLua(EnumEventType.OnTiezhiModifyColor, {c})
end
function CLuaScreenCaptureWnd:onCameraSliderValueChange( v)
  -- stupid idea, I do not want callback when window be inited
  if not (self.m_CameraSliderSetCount > 2) then
    self.m_CameraSliderSetCount = self.m_CameraSliderSetCount + 1
    return
  end
    v = v * 2 - 1
    CameraFollow.Inst:SetPinchIn(v, true, true)
    if v > 0.8 then
        PlayerSettings.HideHeadInfoEnabled = true
    else
        PlayerSettings.HideHeadInfoEnabled = self.oriHideNPCHeadInfo
    end
end
function CLuaScreenCaptureWnd:OnSettingChanged( )
    local hide = PlayerSettings.CaptureHideNoticeWnd
    local context = "ScreenCaptureWnd"
    if hide then
        CUIManager.instance:HideViewByChangeLayer(CUIResources.LoudSpeakerNoticeWnd, false, false, context)
        CUIManager.instance:HideViewByChangeLayer(CUIResources.PopupNoticeWnd, false, false, context)
        CUIManager.instance:HideViewByChangeLayer(CUIResources.MiddleNoticeCenter, false, false, context)
        CUIManager.instance:HideViewByChangeLayer(CUIResources.BottomNoticeCenter, false, false, context)
    else
        CUIManager.instance:ShowViewByChangeLayer(CUIResources.LoudSpeakerNoticeWnd, false, false, context)
        CUIManager.instance:ShowViewByChangeLayer(CUIResources.PopupNoticeWnd, false, false, context)
        CUIManager.instance:ShowViewByChangeLayer(CUIResources.MiddleNoticeCenter, false, false, context)
        CUIManager.instance:ShowViewByChangeLayer(CUIResources.BottomNoticeCenter, false, false, context)
    end
end
function CLuaScreenCaptureWnd:ShareFinished( code, finish)
    --发消息给服务器，报告分享成功
    if finish then
        if CScene.MainScene.SceneTemplateId == HuYiTianHouse_Setting.GetData().MapId then
            Gac2Gas.RequestGetHuYiTianFenXiangItem()
        end
    end
end

function CLuaScreenCaptureWnd:OnClickSettingButton()
--  CUIManager.ShowUI(CUIResources.ScreenCaptureTipWnd)
  self.m_ConfigRootObj:SetActive(true)
  self.FunctionButtonRoot:SetActive(false)
  CJoystickWnd.Instance.gameObject:SetActive(false)
  self:InitConfig()
end

function CLuaScreenCaptureWnd:OnTiezhiButtonClick()
  self.m_TiezhiView:SetActive(not self.m_TiezhiView.activeSelf)
  if self.m_TiezhiView.activeSelf then
    self.m_expressionView:SetActive(false)
  end
end

-- Store active info before hide
RegistClassMember(CLuaScreenCaptureWnd, "m_FocusBtnActive")
RegistClassMember(CLuaScreenCaptureWnd, "m_CameraBtnActive")
RegistClassMember(CLuaScreenCaptureWnd, "m_PlaceBtnActive")
RegistClassMember(CLuaScreenCaptureWnd, "m_CameraControlActive")
RegistClassMember(CLuaScreenCaptureWnd, "m_IKBtnActive")
function CLuaScreenCaptureWnd:OnPreviewClick(go)
  local bShow = not self.FunctionButtonRoot.activeSelf
  self.m_CloseBtnObj:SetActive(bShow)
  self.FunctionButtonRoot:SetActive(bShow)
  self.m_PhotoButtonObj:SetActive(bShow)
  if CommonDefs.IS_VN_CLIENT then
	self.m_PhotoButtonObj:SetActive(false)
  end
  self:ChangeAlpha(go, bShow)
  CJoystickWnd.Instance.gameObject:SetActive(bShow)

  if not bShow then
    self.m_FocusBtnActive = self.m_FocusAdjustBtnObj.activeSelf
    self.m_CameraBtnActive = self.m_CameraAdjustButtonObj.activeSelf
    self.m_PlaceBtnActive = self.m_PlaceBtnObj.activeSelf
    self.m_CameraControlActive = self.m_CameraControlRootObj.activeSelf
    self.m_IKBtnActive = self.m_IKButton.activeSelf
  end

  self.m_FocusAdjustBtnObj:SetActive(bShow and self.m_FocusBtnActive)
  self.m_CameraAdjustButtonObj:SetActive(bShow and self.m_CameraBtnActive)
  self.m_PlaceBtnObj:SetActive(bShow and self.m_PlaceBtnActive)
  self.m_CameraControlRootObj:SetActive(bShow and self.m_CameraControlActive)
  self.m_IKButton:SetActive(bShow and self.m_IKBtnActive)

  if bShow then self.m_TopRightUITable:Reposition() end
end

function CLuaScreenCaptureWnd:ChangeAlpha(go, bTransparent)
  local sprite = go:GetComponent(typeof(UISprite))
  if not sprite then
    sprite = go:GetComponent(typeof(UITexture))
  end
  local c = Color.white
  if bTransparent then
    c.a = 0.7
  end
  sprite.color = c
end

function CLuaScreenCaptureWnd:OnDestroy()
  CScreenCaptureWnd.IsInCaptureMode = false
  g_ScriptEvent:BroadcastInLua("CaptureModeOnOff", false)

  LuaBabyMgr.EnableBabyInteraction = true
  CLuaScreenCaptureWnd.bInScenePhoto = false
  CIKMgr.LookAtIKInteralSwitch = true

  CClientObjectRoot.Inst:HideMonster(false)

  GestureRecognizer.RemoveGestureListener(GestureType.Swipe, self.m_SwipeDelegate)
  Gac2Gas.OperateLingShouBabyAIForCamera(2)

  if self.m_RefreshButtonTick then
    UnRegisterTick(self.m_RefreshButtonTick)
    self.m_RefreshButtonTick = nil
  end
  CameraFollow.Inst:ResetToDefault(CLuaScreenCaptureWnd.CameraFollowResetToDefaultOnDestroy)
  CUIManager.instance:ShowViewsByChangeLayer(CUIManager.EUIModuleGroup.EGameUI, self.m_HideExcept, true, false, "ScreenCaptureWnd")
  CUIManager.instance:ShowViewsByChangeLayer(CUIManager.EUIModuleGroup.EBgGameUI, nil, false, false, "ScreenCaptureWnd")
  if CJoystickWnd.Instance then
    CJoystickWnd.Instance.gameObject:SetActive(true)
    CJoystickWnd.Instance:SetMountAndCaptureButtonVisible(true)
  end
  if PlayerSettings.CaptureHideNoticeWnd then
    CUIManager.instance:ShowViewByChangeLayer(CUIResources.LoudSpeakerNoticeWnd, false, false, "ScreenCaptureWnd")
    CUIManager.instance:ShowViewByChangeLayer(CUIResources.PopupNoticeWnd, false, false, "ScreenCaptureWnd")
    CUIManager.instance:ShowViewByChangeLayer(CUIResources.MiddleNoticeCenter, false, false, "ScreenCaptureWnd")
    CUIManager.instance:ShowViewByChangeLayer(CUIResources.BottomNoticeCenter, false, false, "ScreenCaptureWnd")
  end
  self:ClearTexture()

  self:ResetCamera()

  self:ResetSelfSetting()

  self:ResetMainPlayer()

  CLuaTaskMgr.OnCaptureScreenWndClose(self.m_CaptureHappened)

  CUIManager.CloseUI(CUIResources.HousePopupMenu)

  if self.m_PlaceTipTick then
    UnRegisterTick(self.m_PlaceTipTick)
    self.m_PlaceTipTick = nil
  end
  if self.m_FocusDisableTick then
    UnRegisterTick(self.m_FocusDisableTick)
    self.m_FocusDisableTick = nil
  end
  if self.m_IKTipTick then
    UnRegisterTick(self.m_IKTipTick)
    self.m_IKTipTick = nil
  end

end

function CLuaScreenCaptureWnd:CaptureHideNoticeWndSettingChanged()
  self:OnSettingChanged()
end

function CLuaScreenCaptureWnd:SelfieAddItem(id, type, density)
  self:onAddSelfieItem(id, type, density)
end

function CLuaScreenCaptureWnd:ClientObjDie(argv)
  if argv[0] == CClientMainPlayer.Inst.Id then
    CUIManager.CloseUI(CUIResources.ScreenCaptureWnd)
  end
end

function CLuaScreenCaptureWnd:OnHeadInfoOptionChangeBySetting()
  self.oriHideNPCHeadInfo = PlayerSettings.HideHeadInfoEnabled
end

function CLuaScreenCaptureWnd:OnShareFinished(argv)
  self:ShareFinished(argv[0], argv[1])
end

-- Configuration
RegistClassMember(CLuaScreenCaptureWnd, "m_ConfigRootObj")
RegistClassMember(CLuaScreenCaptureWnd, "m_SettingTitleObjTable")
RegistClassMember(CLuaScreenCaptureWnd, "m_SettingTitleSpriteTransTable")
RegistClassMember(CLuaScreenCaptureWnd, "m_SettingContentObjTable")
RegistClassMember(CLuaScreenCaptureWnd, "m_ConfigUITable")
RegistClassMember(CLuaScreenCaptureWnd, "m_bConfigInited")
RegistClassMember(CLuaScreenCaptureWnd, "m_OriginalSelfSettingTable")
function CLuaScreenCaptureWnd:InitConfig()
  if self.m_bConfigInited then return end
  -- tip button这里目前看没有用到，先屏蔽一下，解决自动化检查出的消息不存在问题

  self.m_ConfigUITable = self.m_ConfigRootObj.transform:Find("Table"):GetComponent(typeof(UITable))
  self.m_SettingTitleObjTable = {}
  self.m_SettingContentObjTable = {}
  self.m_SettingTitleSpriteTransTable = {}
  for i = 1 , 3 do
    local titleObj = self.m_ConfigUITable.transform:Find("SettingTitle"..i).gameObject
    table.insert(self.m_SettingTitleObjTable, titleObj)
    table.insert(self.m_SettingTitleSpriteTransTable, titleObj.transform:Find("Sprite"))
    local contentObj = self.m_ConfigUITable.transform:Find("SettingContent"..i).gameObject
    table.insert(self.m_SettingContentObjTable, contentObj)
    UIEventListener.Get(titleObj).onClick = LuaUtils.VoidDelegate(function()
      self:OnConfigTitleClick(titleObj)
    end)

    if i == 1 then
      self:InitGameConfig(contentObj)
    elseif i == 2 then
      self:InitCameraConfig(contentObj)
    elseif i == 3 then
      self:InitOtherConfig(contentObj)
    end
  end
  self:OnConfigTitleClick(self.m_SettingTitleObjTable[1])
  self.m_bConfigInited = true
end

function CLuaScreenCaptureWnd:InitRenderSettings(contentObj)
    local DetailSettings = {
        {title = LocalString.GetString("水面反射")      ,propertyName = "WaterReflectionEnabled"},
        {title = LocalString.GetString("屏蔽灵兽召唤物"),propertyName = "HidePetEnabled"},
        {title = LocalString.GetString("屏蔽NPC")       ,propertyName = "HideHpcEnabled"},
        {title = LocalString.GetString("屏蔽技能特效")  ,propertyName = "HideSkillEffect"},
        {title = LocalString.GetString("屏蔽神兵特效")  ,propertyName = "HideShenbingEffect"},
        {title = LocalString.GetString("屏蔽名字")      ,propertyName = "HideHeadInfoEnabled"},

    }
    local RenderDetailSettingsCheckBoxGroup = contentObj.transform:Find("RenderSettings/Root/Detail"):GetComponent(typeof(QnCheckBoxGroup))

    local detailtitles_buf = {}
    for i,v in ipairs(DetailSettings) do
        table.insert(detailtitles_buf, v.title)
    end
    local detailtitles = Table2Array(detailtitles_buf, MakeArrayClass(String))
    RenderDetailSettingsCheckBoxGroup:InitWithOptions(detailtitles, true, false)

    for i,v in ipairs(DetailSettings) do
        if v.propertyName == "RealTimeShadowEnabled" then
            RenderDetailSettingsCheckBoxGroup.m_CheckBoxes[i].Enabled = false
            RenderDetailSettingsCheckBoxGroup.m_CheckBoxes[i].Text = SafeStringFormat3("[808080]%s[-]",RenderDetailSettingsCheckBoxGroup.m_CheckBoxes[i].Text)
            break
        end
    end

    RenderDetailSettingsCheckBoxGroup.OnSelect = DelegateFactory.Action_QnCheckBox_int(function(checkbox,level)
        self:OnRenderDetailSettingsSelect(level, DetailSettings, RenderDetailSettingsCheckBoxGroup)
    end)

    for i = 1,#DetailSettings do
        local option = DetailSettings[i]
        local enable = PlayerSettings[option.propertyName]
        RenderDetailSettingsCheckBoxGroup.m_CheckBoxes[i - 1].Selected = enable
    end
end

function CLuaScreenCaptureWnd:OnRenderDetailSettingsSelect(level, DetailSettings, RenderDetailSettingsCheckBoxGroup)
    local option = DetailSettings[level + 1]
    local enable = RenderDetailSettingsCheckBoxGroup[level].Selected
    PlayerSettings[option.propertyName] = enable
    CLuaPlayerSettings.Set(option.propertyName, enable)
    if option.propertyName == "HideHeadInfoEnabled" then
        EventManager.BroadcastInternalForLua(EnumEventType.OnHeadInfoOptionChangeBySetting, {})
    end
end

function CLuaScreenCaptureWnd:InitGameConfig(contentObj)
  self:InitRenderSettings(contentObj)
  local blockMgsCheckBox = contentObj.transform:Find("QnCheckboxTpl_BlockMsg"):GetComponent(typeof(QnCheckBox))
  blockMgsCheckBox.Selected = PlayerSettings.CaptureHideNoticeWnd
  blockMgsCheckBox.OnValueChanged = DelegateFactory.Action_bool(function(v)
    PlayerSettings.CaptureHideNoticeWnd = v
    CLuaPlayerSettings.Set("CaptureHideNoticeWnd", v)
  end)
  local blockMonsterCheckBox = contentObj.transform:Find("QnCheckboxTpl_BlockMonster"):GetComponent(typeof(QnCheckBox))
  blockMonsterCheckBox.Selected = PlayerSettings.HideMonsterInScreenCaptureWndEnabled
  blockMonsterCheckBox.OnValueChanged = DelegateFactory.Action_bool(function(v)
    PlayerSettings.HideMonsterInScreenCaptureWndEnabled = v
    CLuaPlayerSettings.Set("HideMonsterInScreenCaptureWndEnabled", v)
  end)

  -- Add block config
  local blockSelfRootTrans = contentObj.transform:Find("BlockSelf")
  local blockPet = blockSelfRootTrans:Find("SelfPet"):GetComponent(typeof(QnCheckBox))
  blockPet.Selected = CLuaPlayerSettings.Get("BlockSelfPet", false)
  blockPet.OnValueChanged = DelegateFactory.Action_bool(function(v)
      self:BlockPet(v)
      CLuaPlayerSettings.Set("BlockSelfPet", v)
  end)

  local blockBaby = blockSelfRootTrans:Find("SelfBaby"):GetComponent(typeof(QnCheckBox))
  blockBaby.Selected  = CLuaPlayerSettings.Get("BlockSelfBaby", false)
  blockBaby.OnValueChanged = DelegateFactory.Action_bool(function(v)
    self:BlockBaby(v)
    CLuaPlayerSettings.Set("BlockSelfBaby", v)
  end)

  local transNameTbl = {"Talisam", "Stone", "SuitFx", "WingFx"}
  local appearProperty = CClientMainPlayer.Inst and CClientMainPlayer.Inst.AppearanceProp

  for i = 1, #transNameTbl do
    local blockCheckbox = blockSelfRootTrans:Find(transNameTbl[i]):GetComponent(typeof(QnCheckBox))
    if i == 1 then
      blockCheckbox.Selected = self.m_LastAppearPropertySettings[i]
    else
      blockCheckbox.Selected = self.m_LastAppearPropertySettings[i]
    end
    blockCheckbox.OnValueChanged = DelegateFactory.Action_bool(function (v)
    if not appearProperty then return end
    if i == 1 then
      appearProperty.TalismanAppearance = v and 0 or self.m_OriginalSelfSettingTable[1]
      CLuaPlayerSettings.Set("TalismanAppearance", v)
    elseif i == 2 then
      appearProperty.HideGemFxToOtherPlayer = v and 1 or 0
      CLuaPlayerSettings.Set("HideGemFxToOtherPlayer", v)
    elseif i == 3 then
      appearProperty.HideSuitIdToOtherPlayer = v and 1 or 0
      CLuaPlayerSettings.Set("HideSuitIdToOtherPlayer", v)
    elseif i == 4 then
      appearProperty.HideWing = v and 1 or 0
      CLuaPlayerSettings.Set("HideWing", v)
    end
    self:RefreshAppearance(appearProperty)
    end)
  end
end

function CLuaScreenCaptureWnd:RefreshAppearance(appearProperty)
  if not CClientMainPlayer.Inst then return end

  local ro = CClientMainPlayer.Inst.RO
  if ro:ContainFx("TalismanFx") then
    local v = appearProperty.TalismanAppearance
    ro:GetFx("TalismanFx").Visible = v>0 and true or false
  end
  if ro:ContainFx("WeaponGemGroup") then
    local v = appearProperty.HideGemFxToOtherPlayer
    ro:GetFx("WeaponGemGroup").Visible = v==0 and true or false
  end
  if ro:ContainFx("WeaponGemGroup_dup") then
    local v = appearProperty.HideGemFxToOtherPlayer
    ro:GetFx("WeaponGemGroup_dup").Visible = v==0 and true or false
    
  end
  if ro:ContainFx("DunGemGroup") then
    local v = appearProperty.HideGemFxToOtherPlayer
    ro:GetFx("DunGemGroup").Visible = v==0 and true or false
  end
  if ro:ContainFx("DunGemGroup_dup") then
    local v = appearProperty.HideGemFxToOtherPlayer
    ro:GetFx("DunGemGroup_dup").Visible = v==0 and true or false
  end
  if ro:ContainFx("RimLight") then
    local v = appearProperty.HideSuitIdToOtherPlayer
    ro:GetFx("RimLight").Visible = v==0 and true or false
  end
  if ro:ContainFx("Wing") then
    local v = appearProperty.HideWing
    ro:GetFx("Wing").Visible = v==0 and true or false
  end
end

function CLuaScreenCaptureWnd:ResetSelfSetting()
  self:BlockPet(false)
  self:BlockBaby(false)

  if self.m_OriginalSelfSettingTable then
    local appearProperty = CClientMainPlayer.Inst and CClientMainPlayer.Inst.AppearanceProp
    if appearProperty then
      appearProperty.TalismanAppearance = self.m_OriginalSelfSettingTable[1]
      appearProperty.HideGemFxToOtherPlayer = self.m_OriginalSelfSettingTable[2]
      appearProperty.HideSuitIdToOtherPlayer = self.m_OriginalSelfSettingTable[3]
      appearProperty.HideWing = self.m_OriginalSelfSettingTable[4]

      self:RefreshAppearance(appearProperty)
    end
  end
end

function CLuaScreenCaptureWnd:BlockPet(bBlock)
  if CLuaScreenCaptureMgr.CurrentLingshouEngineId <= 0 then return end
  local clientObject = CClientObjectMgr.Inst:GetObject(CLuaScreenCaptureMgr.CurrentLingshouEngineId)
  if clientObject then
    clientObject.Visible = not bBlock
  end
end

function CLuaScreenCaptureWnd:BlockBaby(bBlock)
  if CLuaScreenCaptureMgr.CurrentBabyEngineId <= 0 then return end
  local clientObject = CClientObjectMgr.Inst:GetObject(CLuaScreenCaptureMgr.CurrentBabyEngineId)
  if clientObject then
    clientObject.Visible = not bBlock
  end
end

function CLuaScreenCaptureWnd:InitCameraConfig(contentObj)
  local lineObj = self.transform:Find("Camera/Panel/Line").gameObject
  local lineCheckBox = contentObj.transform:Find("LineCheckbox"):GetComponent(typeof(QnCheckBox))
  lineCheckBox.Selected = CLuaPlayerSettings.Get("ShowLineCheck", false)
  lineCheckBox.OnValueChanged = DelegateFactory.Action_bool(function(v)
    lineObj:SetActive(v)
    CLuaPlayerSettings.Set("ShowLineCheck", v)
  end)

  local paramRootObj = contentObj.transform:Find("ParamRoot").gameObject
  local dofCheckBox = contentObj.transform:Find('DOFCheckbox'):GetComponent(typeof(QnCheckBox))
  local depthOfFieldEnable = CLuaPlayerSettings.Get("DepthOfFieldEnable", false)
  dofCheckBox.Selected = depthOfFieldEnable
  paramRootObj:SetActive(depthOfFieldEnable)
  dofCheckBox.OnValueChanged = DelegateFactory.Action_bool(function(v)
    if CPostProcessingMgr.s_NewPostProcessingOpen then
      local postEffectCtrl = CPostProcessingMgr.Instance.MainController
      postEffectCtrl.DepthOfFieldEnable = v
    else
      CPostEffectMgr.Instance.DepthOfFieldEnable = v
    end
    
    paramRootObj:SetActive(v)
    CLuaPlayerSettings.Set("DepthOfFieldEnable", v)
    self.m_ConfigUITable:Reposition()

    self.m_FocusAdjustBtnObj:SetActive(v)
    self.m_TopRightUITable:Reposition()
  end)

  
  local transNameTbl = {"DOFRange", "DOFSize", "DOFBrightness", "DOFIntensity"}
  local minValueTbl = {0, 2, 0, 0.99}
  local maxValueTbl = {1, 32, 1, 2.5}
  local defaultValueTbl = {1, 16, 0.1, 2.5}
  local initValueTbl

  if CPostProcessingMgr.s_NewPostProcessingOpen then
    local postEffectCtrl = CPostProcessingMgr.Instance.MainController
    local effect = postEffectCtrl.depthOfField

    if effect then
      effect.focusLength:Override(1)
      initValueTbl = {effect.dofRange.value, effect.blinRange.value, effect.blinIntensity.value, effect.threshold.value}
    end
  else
    if CPostEffectMgr.Instance.dofEffect then
      local effect = CPostEffectMgr.Instance.dofEffect
      effect.s_DepthToFocus = 4.0
      effect.DepthLen = 1
      initValueTbl = {effect.dofRange, effect.blinRange, effect.blinIntensity, effect.threshold}
    end
  end

  local sliderTbl = {}
  for i = 1, #transNameTbl do
    local slider = paramRootObj.transform:Find(transNameTbl[i].."/Slider"):GetComponent(typeof(QnNewSlider))
    table.insert(sliderTbl, slider)
    local valueLabel = paramRootObj.transform:Find(transNameTbl[i].."/ValueLabel"):GetComponent(typeof(UILabel))
    slider.OnValueChanged = DelegateFactory.Action_float(function(v)
      local actualValue = v * (maxValueTbl[i] - minValueTbl[i]) + minValueTbl[i]
      actualValue = math.floor(actualValue * 100) / 100
      valueLabel.text = actualValue
      if CPostProcessingMgr.s_NewPostProcessingOpen then
        local postEffectCtrl = CPostProcessingMgr.Instance.MainController
        local effect = postEffectCtrl.depthOfField

        if effect then
          if i == 1 then 
            effect.dofRange:Override(actualValue)
            CLuaPlayerSettings.Set("DofRange", tostring(actualValue))
          elseif i == 2 then 
            effect.blinRange:Override(actualValue)
            CLuaPlayerSettings.Set("BlinRange", tostring(actualValue))
          elseif i == 3 then 
            effect.blinIntensity:Override(actualValue)
            CLuaPlayerSettings.Set("BlinIntensity", tostring(actualValue))
          elseif i == 4 then 
            effect.threshold:Override(actualValue)
            CLuaPlayerSettings.Set("Threshold", tostring(actualValue))
          end
        end
      else
        if CPostEffectMgr.Instance.dofEffect then
          if i == 1 then 
            CPostEffectMgr.Instance.dofEffect.dofRange = actualValue
            CLuaPlayerSettings.Set("DofRange", tostring(actualValue))
          elseif i == 2 then 
            CPostEffectMgr.Instance.dofEffect.blinRange = actualValue
            CLuaPlayerSettings.Set("BlinRange", tostring(actualValue))
          elseif i == 3 then 
            CPostEffectMgr.Instance.dofEffect.blinIntensity = actualValue
            CLuaPlayerSettings.Set("BlinIntensity", tostring(actualValue))
          elseif i == 4 then 
            CPostEffectMgr.Instance.dofEffect.threshold = actualValue
            CLuaPlayerSettings.Set("Threshold", tostring(actualValue))
          end
        end
      end
    end)
    slider.m_Value = ((initValueTbl and initValueTbl[i] or defaultValueTbl[i]) - minValueTbl[i]) / (maxValueTbl[i] - minValueTbl[i])
  end

  UIEventListener.Get(contentObj.transform:Find("ResetBtn").gameObject).onClick = LuaUtils.VoidDelegate(function()
    for i = 1, #sliderTbl do
      sliderTbl[i].m_Value = (defaultValueTbl[i] - minValueTbl[i]) / (maxValueTbl[i] - minValueTbl[i])
    end
  end)
end

-- 其他设置
function CLuaScreenCaptureWnd:InitOtherConfig(contentObj)
  contentObj.transform:Find("BlockSettings"):GetComponent(typeof(CSettingWnd_BlockSetting)):Init()
  local MIN_PLAYERLIMIT = 5
  local MAX_PLAYERLIMIT = 100
  local playerlimitSlider = contentObj.transform:Find("PlayerLimit/NumberSlider"):GetComponent(typeof(QnNewSlider))
  local playerlimitLabel = contentObj.transform:Find("PlayerLimit/limitNumberLabel"):GetComponent(typeof(UILabel))
  local limit = PlayerSettings.VisiblePlayerLimit
  if Application.platform == RuntimePlatform.WindowsEditor then
    playerlimitSlider.m_Value = limit / MAX_PLAYERLIMIT
  else
    playerlimitSlider.m_Value = (limit - MIN_PLAYERLIMIT) / (MAX_PLAYERLIMIT - MIN_PLAYERLIMIT)
  end
  playerlimitLabel.text = limit
  playerlimitSlider.OnValueChanged = DelegateFactory.Action_float(function (v)
    local cnt = 0
    if Application.platform == RuntimePlatform.WindowsEditor then
      cnt = math.floor(v * MAX_PLAYERLIMIT)
    else
      cnt = math.floor(v * (MAX_PLAYERLIMIT - MIN_PLAYERLIMIT) + MIN_PLAYERLIMIT)
    end
    playerlimitLabel.text = cnt
    PlayerSettings.VisiblePlayerLimit = cnt
    CLuaPlayerSettings.Set("VisiblePlayerLimit", cnt)
  end)

  local bOfficial = CLoginMgr.Inst:IsNetEaseOfficialLogin()

  -- QRCode and Logo setting
  local showLogoCheckBox = contentObj.transform:Find("BlockSettings/Root/ShowLogoCheckbox"):GetComponent(typeof(QnCheckBox))
  showLogoCheckBox.Selected = CLuaPlayerSettings.Get("ShowLogo", true)
  showLogoCheckBox.OnValueChanged = DelegateFactory.Action_bool(function(v)
    self.m_IconGO:GetComponent(typeof(UITexture)).enabled = v
    CLuaPlayerSettings.Set("ShowLogo", v)
  end)
  local showCodeCheckBox = contentObj.transform:Find("BlockSettings/Root/ShowCodeCheckbox"):GetComponent(typeof(QnCheckBox))
  showCodeCheckBox.Selected = CLuaPlayerSettings.Get("ShowCode", false)
  showCodeCheckBox.OnValueChanged = DelegateFactory.Action_bool(function(v)
    self.m_CodeObj:GetComponent(typeof(UITexture)).enabled = v
    CLuaPlayerSettings.Set("ShowCode", v)
  end)
  showCodeCheckBox.gameObject:SetActive(bOfficial)

  local logoSettingCheckBoxTbl = {}
  for i = 1, 2 do
    local checkbox = contentObj.transform:Find("BlockSettings/Root/LogoSetting"..i.."Checkbox"):GetComponent(typeof(QnCheckBox))
    table.insert(logoSettingCheckBoxTbl, checkbox)
    checkbox.gameObject:SetActive(bOfficial)
    checkbox.OnValueChanged = DelegateFactory.Action_bool(function(v)
      if not v then
        checkbox:SetSelected(true, true)
      else
        if i == 1 then
          logoSettingCheckBoxTbl[2]:SetSelected(false, true)
          Extensions.SetLocalPositionX(self.m_IconGO.transform, self.m_IconPosX)
          Extensions.SetLocalPositionX(self.m_CodeObj.transform, self.m_CodePosX)
        else
          logoSettingCheckBoxTbl[1]:SetSelected(false, true)
          Extensions.SetLocalPositionX(self.m_IconGO.transform, -self.m_IconPosX)
          Extensions.SetLocalPositionX(self.m_CodeObj.transform, -self.m_CodePosX)
        end
      end
      CLuaPlayerSettings.Set("LogeSetting", i)
    end)
  end
  logoSettingCheckBoxTbl[CLuaPlayerSettings.Get("LogeSetting", 1)]:SetSelected(true, false)
end

function CLuaScreenCaptureWnd:OnConfigTitleClick(go)
  for i = 1, 3 do
    local bSelected = go == self.m_SettingTitleObjTable[i]
    Extensions.SetLocalRotationZ(self.m_SettingTitleSpriteTransTable[i], bSelected and 180 or 0)
    self.m_SettingContentObjTable[i]:SetActive(bSelected)
  end
  self.m_ConfigUITable:Reposition()
end

-- Camera Controller
RegistClassMember(CLuaScreenCaptureWnd, "m_TopRightUITable")
RegistClassMember(CLuaScreenCaptureWnd, "m_CameraAdjustButtonObj")
RegistClassMember(CLuaScreenCaptureWnd, "m_CameraControlRootObj")
RegistClassMember(CLuaScreenCaptureWnd, "m_CameraTrans")
RegistClassMember(CLuaScreenCaptureWnd, "m_CameraLocalPositionTable")
RegistClassMember(CLuaScreenCaptureWnd, "m_SwipeDelegate")
RegistClassMember(CLuaScreenCaptureWnd, "m_IsPressing")
RegistClassMember(CLuaScreenCaptureWnd, "m_PressTick")
function CLuaScreenCaptureWnd:InitTopRight()
  self.m_TopRightUITable = self.transform:Find("Camera/Panel/TopRightRoot"):GetComponent(typeof(UITable))
  self.m_CameraAdjustButtonObj = self.m_TopRightUITable.transform:Find("CameraButton").gameObject
  self.m_CameraAdjustButtonObj:SetActive(CommonDefs.IsSceneEnable3D())
  self.m_CameraControlRootObj = self.transform:Find("Camera/Panel/CameraControlRoot").gameObject
  self.m_CameraControlRootObj:SetActive(false)
  self.m_IKButton = self.m_TopRightUITable.transform:Find("IKButton").gameObject
  self.m_IKButton:SetActive(CIKMgr.Inst:CanSupportIK_ScreenCapture())
  -- todo
  self:ChangeAlpha(self.m_CameraAdjustButtonObj, not self.m_CameraControlRootObj.activeSelf)
  self:InitController()
  UIEventListener.Get(self.m_CameraAdjustButtonObj).onClick = LuaUtils.VoidDelegate(function(go)
    local bShow = not self.m_CameraControlRootObj.activeSelf
    self.m_CameraControlRootObj:SetActive(bShow)
    self:ChangeAlpha(go, not bShow)
    self:Disable3DCameraRotate(bShow)
  end)

  self:InitPlaceMode()
  self:InitFocus()
  self:InitPhotoMode()
  self:InitIK()

  self.m_TopRightUITable:Reposition()

  self.m_CameraTrans = CameraFollow.Inst.transform:Find("camRotate/newParent")
end

----Album begin
RegistClassMember(CLuaScreenCaptureWnd, "m_bAlbumInited")
RegistClassMember(CLuaScreenCaptureWnd, "m_LastSelectedPhotoObj")
RegistClassMember(CLuaScreenCaptureWnd, "m_PhotoId2UrlTable")
RegistClassMember(CLuaScreenCaptureWnd, "m_PhotoButtonObj")
function CLuaScreenCaptureWnd:InitPhotoMode()
  self.m_PhotoButtonObj = self.m_TopRightUITable.transform:Find("PhotoButton").gameObject
  if CommonDefs.IS_VN_CLIENT then
	  self.m_PhotoButtonObj:SetActive(false)
  end
  self:ChangeAlpha(self.m_PhotoButtonObj, true)
  UIEventListener.Get(self.m_PhotoButtonObj).onClick = LuaUtils.VoidDelegate(function(go)
    local bShow = not self.m_AlbumRootObj.activeSelf
    self:ChangeAlpha(go, not bShow)
    self.m_AlbumRootObj:SetActive(bShow)
    --self.FunctionButtonRoot:SetActive(not bShow)
    CJoystickWnd.Instance.gameObject:SetActive(not bShow)
    if bShow then
      self:InitAlbum()
    end
  end)

  self.m_PreviewPhotoRootObj = self.transform:Find("Camera/Panel/PreviewPhotoRoot").gameObject
  self.m_PreviewPhotoRootObj:SetActive(false)
  UIEventListener.Get(self.m_PreviewPhotoRootObj.transform:Find("CloseButton").gameObject).onClick = LuaUtils.VoidDelegate(function (go)
    self.m_PreviewPhotoRootObj:SetActive(false)
  end)
end
function CLuaScreenCaptureWnd:InitAlbum()
  local gridView = self.m_AlbumRootObj.transform:Find("AdvView"):GetComponent(typeof(QnAdvanceGridView))
  RegisterTickOnce(function()
    if gridView then
      gridView.transform:Find("ScrollViewIndicator"):GetComponent(typeof(UIScrollViewIndicator)):Layout()
    end
  end, 100)
  if self.m_bAlbumInited then
    gridView:ReloadData(true,false)
    return
  end
  self.m_bAlbumInited = true

  local getNumFunc = function() return PaiZhao_Photo.GetDataCount() end
  local initItemFunc = function(item, index)
    local id = index + 1
    local designData = PaiZhao_Photo.GetData(id)
    item.transform:Find("NameLabel"):GetComponent(typeof(UILabel)).text = designData.Name
    item.transform:Find("SelectBg").gameObject:SetActive(false)
    local isVideo = designData.CameraPath ~= nil
    item.transform:Find("VideoFlag").gameObject:SetActive(isVideo)
    local haveData = CLuaScreenCaptureMgr.AlbumTable[id] ~= nil
    UIEventListener.Get(item.gameObject).onClick = LuaUtils.VoidDelegate(function(go)
      if self.m_LastSelectedPhotoObj then
        self.m_LastSelectedPhotoObj.transform:Find("SelectBg").gameObject:SetActive(false)
      end
      self.m_LastSelectedPhotoObj = go
      go.transform:Find("SelectBg").gameObject:SetActive(true)
      self:OnAlbumClick(go, id, designData, isVideo)
    end)

    item.transform:Find("LockFlag").gameObject:SetActive(not haveData)
    local photoTex = item.transform:Find("Photo"):GetComponent(typeof(UITexture))
    photoTex.gameObject:SetActive(haveData and not isVideo)
    if haveData and not isVideo then
      CPersonalSpaceMgr.Inst:DownLoadPortraitPic(CLuaScreenCaptureMgr.AlbumTable[id], photoTex, nil)
    end
  end

  gridView.m_DataSource = DefaultTableViewDataSource.Create(getNumFunc,initItemFunc)
  gridView:ReloadData(true,false)
  self.m_AlbumRootObj.transform:Find("CountLabel"):GetComponent(typeof(UILabel)).text = CLuaScreenCaptureMgr.AlbumCount.."/"..PaiZhao_Photo.GetDataCount()
end

function CLuaScreenCaptureWnd:OnAlbumClick(go, id, designData, isVideo)
  if CLuaScreenCaptureMgr.AlbumTable[id] == nil then
    if isVideo then
      CLuaScreenCaptureMgr:DoRecordVideo(id, designData, true)
    else
      self:DoTakePhoto(id, designData)
    end
    return
  end
  local tbl = {}
  if isVideo then
    table.insert(tbl, PopupMenuItemData(LocalString.GetString("前往风景"),
        DelegateFactory.Action_int(function (index)
          CLuaScreenCaptureMgr:DoRecordVideo(id, designData, false)
        end), false, nil))
    table.insert(tbl, PopupMenuItemData(LocalString.GetString("重新录制"),
        DelegateFactory.Action_int(function (index)
          CLuaScreenCaptureMgr:DoRecordVideo(id, designData, true)
        end), false, nil))
  else
    table.insert(tbl, PopupMenuItemData(LocalString.GetString("前去拍照"),
        DelegateFactory.Action_int(function (index)
          self:DoTakePhoto(id, designData)
        end), false, nil))
    table.insert(tbl, PopupMenuItemData(LocalString.GetString("查看照片"),
        DelegateFactory.Action_int(function (index)
          self.m_PreviewPhotoRootObj.transform:Find("Texture"):GetComponent(typeof(UITexture)).mainTexture = go.transform:Find("Photo"):GetComponent(typeof(UITexture)).mainTexture
          self.m_PreviewPhotoRootObj:SetActive(true)
        end), false, nil))
  end

  local array = Table2Array(tbl, MakeArrayClass(PopupMenuItemData))
  CPopupMenuInfoMgr.ShowPopupMenu(array, go.transform, AlignType.Right)
end

function CLuaScreenCaptureWnd:DoTakePhoto(id, designData)
  if not CClientMainPlayer.Inst then return end
  if CClientMainPlayer.Inst.MaxLevel < 30 then
    g_MessageMgr:ShowMessage("CUSTOM_STRING1", LocalString.GetString("达到30级开启"))
    return
  end
  self:ResetCamera()
  CLuaScreenCaptureMgr:DoTrack(designData.SceneTemplateId, designData.ScenePos[0], designData.ScenePos[1], designData.NotNeedRideOffVehicle, function()
    if CRenderScene.Inst then
        local atNight = CRenderScene.Inst.AtNight
        if designData.DayOrNight == 1 and atNight then
            g_MessageMgr:ShowMessage("PaiZhao_Photo_DayOrNight", LocalString.GetString("白天"))
            return
        end
        if designData.DayOrNight == 2 and not atNight then
            g_MessageMgr:ShowMessage("PaiZhao_Photo_DayOrNight", LocalString.GetString("黑夜"))
            return
        end
    end
    CLuaScreenCaptureWnd.bInScenePhoto = true
    CLuaScreenCaptureWnd.ScenePhotoId = id
    if not CUIManager.IsLoaded(CUIResources.ScreenCaptureWnd) then
      CUIManager.ShowUI(CUIResources.ScreenCaptureWnd)
    else
      self:ProcessScenePhotoMode()
    end
  end)
end

-- 处理风景相册
function CLuaScreenCaptureWnd:ProcessScenePhotoMode()
  if not CLuaScreenCaptureWnd.bInScenePhoto then return end

  --这里可能会传入自定义的参数
  if CLuaScreenCaptureMgr.CustomPhotoCameraPos then
    CameraFollow.Inst.targetRZY = CLuaScreenCaptureMgr.CustomPhotoCameraPos.rzy
    CameraFollow.Inst.CurCameraParam:ApplyRZY(CLuaScreenCaptureMgr.CustomPhotoCameraPos.rzy)
    self.m_CameraLocalPositionTable =  CLuaScreenCaptureMgr.CustomPhotoCameraPos.pos
    self:SetCameraTransPosition()
    CLuaScreenCaptureMgr.CustomPhotoCameraPos=nil
    return
  end

  local designData = PaiZhao_Photo.GetData(CLuaScreenCaptureWnd.ScenePhotoId)
  local rzy = Vector3(designData.CameraPos1[0], designData.CameraPos1[1], designData.CameraPos1[2])
  CameraFollow.Inst.targetRZY = rzy
  CameraFollow.Inst.CurCameraParam:ApplyRZY(rzy)
  self.m_CameraLocalPositionTable =  {designData.CameraPos2[0], designData.CameraPos2[1], designData.CameraPos2[2], designData.CameraPos2[3], designData.CameraPos2[4], designData.CameraPos2[5]}
  self:SetCameraTransPosition()
end
----Album end

function CLuaScreenCaptureWnd:InitController()
  self.m_CameraLocalPositionTable = {0, 0, 0, 0, 0, 0}
  local transNameTbl = {"Left", "Right", "Up", "Down", "LeftRotate", "RightRotate", "Reset"}
  local deltaValTbl = {-0.1, 0.1, 0.1, -0.1, -1, 1}
  local indexValTbl = {1, 1, 2, 2, 6, 6}
  for i = 1, #transNameTbl do
    local obj = self.m_CameraControlRootObj.transform:Find(transNameTbl[i]).gameObject
    UIEventListener.Get(obj).onClick = LuaUtils.VoidDelegate(function()
      if i == 7 then
        self.m_CameraLocalPositionTable = {0, 0, 0, 0, 0, 0}
        self:SetCameraTransPosition()
      elseif (not self.m_IsPressing) then
        local tmp = self.m_CameraLocalPositionTable[indexValTbl[i]] + deltaValTbl[i] * 0.1
        if self:SetCameraTransPosition(i) then
          self.m_CameraLocalPositionTable[indexValTbl[i]] = tmp
        end
      end
    end)

    if i < 7 then
      UIEventListener.Get(obj).onPress = LuaUtils.BoolDelegate(function(go, state)
        if self.m_PressTick then
          UnRegisterTick(self.m_PressTick)
          self.m_PressTick = nil
        end
        self.m_IsPressing = state
        if state then
          self.m_PressTick = RegisterTick(function()
            --self.m_CameraLocalPositionTable[indexValTbl[i]] = self.m_CameraLocalPositionTable[indexValTbl[i]] + deltaValTbl[i] * 0.1
            local tmp = self.m_CameraLocalPositionTable[indexValTbl[i]] + deltaValTbl[i] * 0.1
            if self:SetCameraTransPosition(i) then
              self.m_CameraLocalPositionTable[indexValTbl[i]] = tmp
            end
          end, 10)
        end
      end)
    end
  end

  self.m_SwipeDelegate = DelegateFactory.Action_GenericGesture(function(gesture) self:OnSwipe(gesture) end)

  GestureRecognizer.AddGestureListener(GestureType.Swipe, self.m_SwipeDelegate)
end

function CLuaScreenCaptureWnd:OnSwipe(gesture)
  if not self.m_CameraControlRootObj.activeSelf then return end
  -- No swipe when moving object
  if self.m_bInPlaceMode and self.m_CurrentRenderObject then return end

  if self.m_bEasyTouchEnable then
    self:Disable3DCameraRotate(true)
  end
	if EasyTouch.GetTouchCount() == 1 then
		self.m_CameraLocalPositionTable[4] = self.m_CameraLocalPositionTable[4] + gesture.swipeVector.y / Screen.width * 20
    self.m_CameraLocalPositionTable[5] = self.m_CameraLocalPositionTable[5] - gesture.swipeVector.x / Screen.height * 20
    self:SetCameraTransPosition()
	end
end

function CLuaScreenCaptureWnd:SetCameraTransPosition(button)
    if self.m_CameraTrans == nil then
        self.m_CameraTrans = CameraFollow.Inst.transform:Find("camRotate/newParent")
    end
    
    self.m_CameraTrans.localPosition = Vector3(self.m_CameraLocalPositionTable[1], self.m_CameraLocalPositionTable[2], self.m_CameraLocalPositionTable[3])
    self.m_CameraTrans.localEulerAngles = Vector3(self.m_CameraLocalPositionTable[4], self.m_CameraLocalPositionTable[5], self.m_CameraLocalPositionTable[6])
  -- Camera Tween...
    CMainCamera.Inst:SetMainCameraYPos(self.m_CameraLocalPositionTable[2])
    
    if CScene.MainScene then
        local logicHeight = CScene.MainScene and CScene.MainScene:GetLogicHeight(math.floor(self.m_CameraTrans.position.x * 64), math.floor(self.m_CameraTrans.position.z * 64))
        if button == 4 and self.m_CameraTrans.position.y <= math.max(CWater.s_WaterHeight, logicHeight) + self.m_CameraMinY then
            return false
        end

        if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.RO ~= nil then
            local RO = CClientMainPlayer.Inst.RO
            local targetPos = RO:GetPosition()
            local playerLogicHeight = CScene.MainScene:GetLogicHeight(math.floor(targetPos.x * 64), math.floor(targetPos.z * 64))
            if button == 3 and self.m_CameraTrans.position.y >= playerLogicHeight + self.m_CameraMaxY then
                return false
            end
        end

    end
    
  return true
end

function CLuaScreenCaptureWnd:ResetCamera()
  self.m_CameraLocalPositionTable = {0, 0, 0, 0, 0, 0}
  self:SetCameraTransPosition()
  self:Disable3DCameraRotate(false)

  if CClientMainPlayer.Inst then
      if CClientMainPlayer.Inst.VehicleRO then
          local ypos = CClientObject.GetRideCameraPos(CClientMainPlayer.Inst.AppearanceProp.ZuoQiTemplateId)
          CMainCamera.Inst:SetMainCameraYPos(ypos)
          CameraFollow.Inst:SetFollowObj(CClientMainPlayer.Inst.VehicleRO)
      else
          CameraFollow.Inst:SetFollowObj(CClientMainPlayer.Inst.RO)
      end
  end

  if self.m_FakeMainplayerRO then
    self.m_FakeMainplayerRO:Destroy() 
    self.m_FakeMainplayerRO = nil
  end

  if CPostProcessingMgr.s_NewPostProcessingOpen then
    local postEffectCtrl = CPostProcessingMgr.Instance.MainController
    postEffectCtrl:SetCustomFocus(0, false)
  else
    if CPostEffectMgr.Instance.dofEffect then
      CPostEffectMgr.Instance.dofEffect.m_EnableCustomFocus = false
    end
  end
end

-- Place Mode
RegistClassMember(CLuaScreenCaptureWnd, "m_bInPlaceMode")
RegistClassMember(CLuaScreenCaptureWnd, "m_PlaceTipObj")
RegistClassMember(CLuaScreenCaptureWnd, "m_PlaceTipTick")
RegistClassMember(CLuaScreenCaptureWnd, "m_CurrentRenderObject")
RegistClassMember(CLuaScreenCaptureWnd, "m_CurrentClientObject")
RegistClassMember(CLuaScreenCaptureWnd, "m_RO2PositionTable")
RegistClassMember(CLuaScreenCaptureWnd, "m_PlaceBtnObj")
-- For camera operating
RegistClassMember(CLuaScreenCaptureWnd, "m_FakeMainplayerRO")
RegistClassMember(CLuaScreenCaptureWnd, "m_bEasyTouchEnable")
function CLuaScreenCaptureWnd:InitPlaceMode()
  self.m_bEasyTouchEnable = true
  self.m_PlaceTipObj = self.transform:Find("Camera/Panel/PlaceTip").gameObject
  self.m_PlaceTipObj:SetActive(false)
  local placeBtnObj = self.m_TopRightUITable.transform:Find("PlaceButton").gameObject
  self.m_PlaceBtnObj = placeBtnObj
  self:ChangeAlpha(placeBtnObj, true)
  placeBtnObj:SetActive(CommonDefs.IsSceneEnable3D())
  self.m_bInPlaceMode = false
  UIEventListener.Get(placeBtnObj).onClick = LuaUtils.VoidDelegate(function(go)
    self:OnPlaceBtnClick(go)
  end)
  self.m_RO2PositionTable = {}
end

function CLuaScreenCaptureWnd:OnPlaceBtnClick(go)
  LuaBabyMgr.EnableBabyInteraction = self.m_bInPlaceMode
  self.m_bInPlaceMode = not self.m_bInPlaceMode
  if self.m_bInPlaceMode then
    self:CloseIKMode()
    --CIKMgr.Inst:SetMainPlayerIKLookAtToTarget(nil)
    --CIKMgr.LookAtIKInteralSwitch = false
    --CIKMgr.Inst:AdjustMainPlayerLookAtIK(true, true)
  end
  self:ChangeAlpha(go, not self.m_bInPlaceMode)
  self.m_PlaceTipObj:SetActive(self.m_bInPlaceMode)
  Gac2Gas.OperateLingShouBabyAIForCamera(self.m_bInPlaceMode and 1 or 2)
  if CClientMainPlayer.Inst then
    if self.m_bInPlaceMode then
      --CameraFollow.Inst:StopFollow()
      if not self.m_FakeMainplayerRO then
        self.m_FakeMainplayerRO = CRenderObject.CreateRenderObject(nil, "ScreenCaptureRO")
        local go = NGUITools.AddChild(self.m_FakeMainplayerRO.gameObject)
        go.transform.position = CClientMainPlayer.Inst.RO.TopAnchorPos
        self.m_FakeMainplayerRO.m_TopAnchorTransform = go.transform
      end
      self.m_FakeMainplayerRO.Position = CClientMainPlayer.Inst.RO.Position
      self.m_FakeMainplayerRO.Direction = CClientMainPlayer.Inst.RO.Direction
      CameraFollow.Inst:SetFollowObj(self.m_FakeMainplayerRO)
    else
      if CClientMainPlayer.Inst then
        CameraFollow.Inst:SetFollowObj(CClientMainPlayer.Inst.RO)
      end
    end
  end
  if self.m_PlaceTipTick then
    UnRegisterTick(self.m_PlaceTipTick)
    self.m_PlaceTipTick = nil
  end
  if self.m_bInPlaceMode then
    self.m_PlaceTipTick = RegisterTickOnce(function()
      self.m_PlaceTipObj:SetActive(false)
      UnRegisterTick(self.m_PlaceTipTick)
      self.m_PlaceTipTick = nil
    end, 10000)
  end
end

function CLuaScreenCaptureWnd:OnClientObjectSelected(argv)
  if not self.m_bInPlaceMode then return end
  local clientObject = argv[0]
  if not (clientObject.EngineId == (CClientMainPlayer.Inst and CClientMainPlayer.Inst.EngineId) or clientObject.EngineId == CLuaScreenCaptureMgr.CurrentBabyEngineId or clientObject.EngineId == CLuaScreenCaptureMgr.CurrentLingshouEngineId) then
      -- Other Players...
      if not TypeIs(clientObject, typeof(CClientOtherPlayer)) then return end
  end

  if self.m_CurrentRenderObject ~= clientObject.RO then
    self.m_RO2PositionTable[clientObject.RO] = {Pos = clientObject.RO.Position, Dir = clientObject.RO.Direction}
  end

  clientObject:ShowSelectedFX(true, false)
  self.m_CurrentClientObject = clientObject
  self.m_CurrentRenderObject = clientObject.RO
end

function CLuaScreenCaptureWnd:ShowPopupMenu()
  local list = CommonDefs.ListCreate(typeof(HousePopupMenuItemData))
  CommonDefs.ListAdd_LuaCall(list, HousePopupMenuItemData("", DelegateFactory.Action_int(function ( index )
    if self.m_CurrentClientObject then
      self.m_CurrentClientObject:ShowSelectedFX(false, false)
    end
    self.m_CurrentRenderObject = nil
  end), "UI/Texture/Transparent/Material/housepopupmenu_loc.mat", true, true, -1))
  CommonDefs.ListAdd_LuaCall(list, HousePopupMenuItemData("", DelegateFactory.Action_int(function ( index )
    if self.m_CurrentRenderObject then
      self.m_CurrentRenderObject.Direction = self.m_CurrentRenderObject.Direction - 10
    end
  end), "UI/Texture/Transparent/Material/housepopupmenu_xuanzhuan1.mat", false, true, -1))
  CommonDefs.ListAdd_LuaCall(list, HousePopupMenuItemData("", DelegateFactory.Action_int(function ( index )
    if self.m_CurrentRenderObject then
      self.m_CurrentRenderObject.Direction = self.m_CurrentRenderObject.Direction + 10
    end
  end), "UI/Texture/Transparent/Material/housepopupmenu_xuanzhuan.mat", false, true, -1))
  CommonDefs.ListAdd_LuaCall(list, HousePopupMenuItemData("", DelegateFactory.Action_int(function ( index )
    local data = self.m_RO2PositionTable[self.m_CurrentRenderObject]
    if data then
      self.m_CurrentRenderObject.Position = data.Pos
      self.m_CurrentRenderObject.Direction = data.Dir
    end
    if self.m_CurrentClientObject then
      self.m_CurrentClientObject:ShowSelectedFX(false, false)
    end
    self.m_CurrentRenderObject = nil
  end), "UI/Texture/Transparent/Material/housepopupmenu_delete.mat", true, true, -1))
  CHousePopupMgr.ShowHousePopupMenu(CommonDefs.ListToArray(list), self.m_CurrentRenderObject:GetSlotTransform("TopAnchor"))
end

function CLuaScreenCaptureWnd:ScreenPoint2WorldPos(screenPoint)
	local hit = nil
	local ray = CMainCamera.Main:ScreenPointToRay(screenPoint)
	local dist = CMainCamera.Main.farClipPlane
	local hits = Physics.RaycastAll(ray, dist, CMainCamera.Main.cullingMask)
	if not hits or hits.Length == 0 then
		return nil
	end
	local hitDis = CMainCamera.Main.farClipPlane
	for i=0,hits.Length - 1 do
		local hitData = hits[i]
		if hitData.collider:CompareTag(Tags.Ground) then
			if hitDis > hitData.distance then
				hit = hitData
				hitDis = hitData.distance
			end
		end
	end

	if hit then
    local barrier = CoreScene.MainCoreScene:GetOriginalBarrierv(math.floor(hit.point.x), math.floor(hit.point.z))
    if barrier ~= EBarrierType.eBT_NoBarrier then return nil end
		return hit.point
	end
end

function CLuaScreenCaptureWnd:ProcessPlaceMode()
  local isNull = CommonDefs.IsUnityObjectNull(self.m_CurrentRenderObject)
  if (not self.m_bInPlaceMode) or isNull then
    if not self.m_bEasyTouchEnable and not self.m_CameraControlRootObj.activeSelf then
      --EasyTouch.SetEnabled(true)
      self:Disable3DCameraRotate(false)
    end
    if self.m_bInPlaceMode and isNull then
      CUIManager.CloseUI(CUIResources.HousePopupMenu)
    end
    return
  end

  -- Disable EasyTouch
  if self.m_bEasyTouchEnable then
    --EasyTouch.SetEnabled(false)
    self:Disable3DCameraRotate(true)
  end

  if Input.GetMouseButton(0) then
    if not CUICommonDef.IsOverUI then
      local posX = Input.mousePosition.x
      local posY = Input.mousePosition.y
      local screenPos = Vector3(posX,posY,0)
      local pixelPos = self:ScreenPoint2WorldPos(screenPos)
      if pixelPos then
        self.m_CurrentRenderObject.Position = pixelPos
      end
    end
  end
  if Input.GetMouseButtonUp(0) then
    self:ShowPopupMenu()
  end
end

-- Focus
RegistClassMember(CLuaScreenCaptureWnd, "m_FocusAdjustBtnObj")
RegistClassMember(CLuaScreenCaptureWnd, "m_FocusTipObj")
RegistClassMember(CLuaScreenCaptureWnd, "m_bInFocus")
RegistClassMember(CLuaScreenCaptureWnd, "m_FocusDisableTick")
function CLuaScreenCaptureWnd:InitFocus()
  self.m_FocusAdjustBtnObj = self.m_TopRightUITable.transform:Find("FocusButton").gameObject
  self.m_FocusTipObj = self.transform:Find("Camera/Panel/FocusTip").gameObject
  self.m_FocusTipObj:SetActive(false)

  if CPostProcessingMgr.s_NewPostProcessingOpen then
    local postEffectCtrl = CPostProcessingMgr.Instance.MainController
    self.m_FocusAdjustBtnObj:SetActive(postEffectCtrl.DepthOfFieldEnable)
  else
    self.m_FocusAdjustBtnObj:SetActive(CPostEffectMgr.Instance.DepthOfFieldEnable)
  end
  
  self:ChangeAlpha(self.m_FocusAdjustBtnObj, true)
  self.m_bInFocus = false
  UIEventListener.Get(self.m_FocusAdjustBtnObj).onClick = LuaUtils.VoidDelegate(function (go)
    self.m_bInFocus = not self.m_bInFocus
    self:ChangeAlpha(go, not self.m_bInFocus)
    self.m_FocusTipObj:SetActive(self.m_bInFocus)
    self:ResetFocusDisableTick()
  end)
end

function CLuaScreenCaptureWnd:ResetFocusDisableTick()
  if self.m_FocusDisableTick then
    UnRegisterTick(self.m_FocusDisableTick)
    self.m_FocusDisableTick = nil
  end
  if self.m_bInFocus then
    self.m_FocusDisableTick = RegisterTickOnce(function()
      self.m_bInFocus = false
      self:ChangeAlpha(self.m_FocusAdjustBtnObj, true)
      self.m_FocusTipObj:SetActive(false)
    end, 5000)
    local tween = TweenScale.Begin(self.m_FocusTipObj, 0.5, Vector3.one)
    tween.from = Vector3(1.5, 1.5, 1.5)
  end
end

function CLuaScreenCaptureWnd:GetNearestHit(screenPoint)
  local hit = nil
	local ray = CMainCamera.Main:ScreenPointToRay(screenPoint)
	local dist = CMainCamera.Main.farClipPlane
	local hits = Physics.RaycastAll(ray, dist, CMainCamera.Main.cullingMask)
	if not hits or hits.Length == 0 then
		return nil
	end
	local hitDis = CMainCamera.Main.farClipPlane
	for i=0,hits.Length - 1 do
		local hitData = hits[i]
		if hitDis > hitData.distance then
			hit = hitData
			hitDis = hitData.distance
		end
	end
  if hit then return hit.point end
end

function CLuaScreenCaptureWnd:ProcessFocus()
  if not self.m_bInFocus then return end
  
  if CPostProcessingMgr.s_NewPostProcessingOpen then
    local postEffectCtrl = CPostProcessingMgr.Instance.MainController
    if not postEffectCtrl.depthOfField then return end
  else
    if not CPostEffectMgr.Instance.dofEffect then return end
  end

  if Input.GetMouseButtonUp(0) then
    if not CUICommonDef.IsOverUI then
      local posX = Input.mousePosition.x
      local posY = Input.mousePosition.y
      local screenPos = Vector3(posX,posY,0)
      local point = self:GetNearestHit(screenPos)
      if point then
        local cameraPos = CMainCamera.Inst.transform.position
        local dist = {point.x - cameraPos.x, point.y - cameraPos.y, point.z - cameraPos.z}
        local depth = math.sqrt(dist[1] * dist[1] + dist[2] * dist[2] + dist[3] * dist[3])

        if CPostProcessingMgr.s_NewPostProcessingOpen then
          local postEffectCtrl = CPostProcessingMgr.Instance.MainController
          postEffectCtrl:SetCustomFocus(depth, true)
        else
          CPostEffectMgr.Instance.dofEffect.m_EnableCustomFocus = true
          CPostEffectMgr.Instance.dofEffect.m_CustomFocusDepth = depth
        end
        
        self:ResetFocusTip(screenPos)
        self:ResetFocusDisableTick()
      end
    end
  end
end

function CLuaScreenCaptureWnd:ResetFocusTip(screenPos)
  local pos = CUIManager.UIMainCamera:ScreenToWorldPoint(screenPos)
  self.m_FocusTipObj.transform.position = pos
end

-- MainPlayer Position and Direction
RegistClassMember(CLuaScreenCaptureWnd, "m_OriMainPlayerPosDir")
function CLuaScreenCaptureWnd:MainPlayerMoveEnded()
  if not CClientMainPlayer.Inst then return end
  self.m_OriMainPlayerPosDir = {Pos = CClientMainPlayer.Inst.RO.Position, Dir = CClientMainPlayer.Inst.RO.Direction}
end
function CLuaScreenCaptureWnd:OnPlayerDetachFurniture(playerId,furnitureId)
  if not CClientMainPlayer.Inst then return end
  if CClientMainPlayer.Inst.Id == playerId then
    self:MainPlayerMoveEnded()
  end
end


function CLuaScreenCaptureWnd:ResetMainPlayer()
  if not CClientMainPlayer.Inst or not self.m_OriMainPlayerPosDir or CClientMainPlayer.Inst.IsMoving then return end
  CClientMainPlayer.Inst.RO.Position = self.m_OriMainPlayerPosDir.Pos
  CClientMainPlayer.Inst.RO.Direction = self.m_OriMainPlayerPosDir.Dir
end

function CLuaScreenCaptureWnd:Disable3DCameraRotate(bDisable)
  self.m_bEasyTouchEnable = not bDisable
  if bDisable then
    NormalCamera.m_hookProcessTwoFingerInput = function() end
  else
    NormalCamera.m_hookProcessTwoFingerInput = nil
  end
end

RegistClassMember(CLuaScreenCaptureWnd, "m_IKButton")
RegistClassMember(CLuaScreenCaptureWnd, "m_IKTip")
RegistClassMember(CLuaScreenCaptureWnd, "m_IKTipTick")
RegistClassMember(CLuaScreenCaptureWnd, "m_IKHint")
RegistClassMember(CLuaScreenCaptureWnd, "m_IKTargetController")
RegistClassMember(CLuaScreenCaptureWnd, "m_bInIK")
RegistClassMember(CLuaScreenCaptureWnd, "m_IKTarget")
RegistClassMember(CLuaScreenCaptureWnd, "m_IKTargetInRoot")
function CLuaScreenCaptureWnd:InitIK()
    self.m_IKButton = self.m_TopRightUITable.transform:Find("IKButton").gameObject
    self.m_IKTip = self.transform:Find("Camera/Panel/IKTip").gameObject
    self.m_IKTip:SetActive(false)
    self.m_IKHint = self.transform:Find("Camera/Panel/IKTip/IKHint").gameObject
    self.m_IKTargetController = self.transform:Find("Camera/Panel/IKTip/IKTargetController").gameObject
    self.m_bInIK = false
    self:ChangeAlpha(self.m_IKButton, not self.m_bInIK)

    if self.m_IKTipTick then
        UnRegisterTick(self.m_IKTipTick)
        self.m_IKTipTick = nil
    end

    CommonDefs.AddOnDragListener(self.m_IKTargetController, DelegateFactory.Action_GameObject_Vector2(function (go, delta)
        self:OnEyeDrag(go, delta)
    end),false)

    UIEventListener.Get(self.m_IKButton).onClick = LuaUtils.VoidDelegate(function (go)
        self.m_bInIK = not self.m_bInIK
        self:ChangeAlpha(go, not self.m_bInIK)

        if self.m_IKTipTick then
            UnRegisterTick(self.m_IKTipTick)
            self.m_IKTipTick = nil
        end
        if self.m_bInIK then
            self.m_IKTipTick = RegisterTickOnce(function()
                self.m_IKHint:SetActive(false)
                UnRegisterTick(self.m_IKTipTick)
                self.m_IKTipTick = nil
            end, 5000)
        end
        self:ProcessIKMode()
  end)
end

function CLuaScreenCaptureWnd:ProcessIKMode()
    CScreenCaptureWnd.IsInCaptureIKMode = self.m_bInIK
    self.m_IKTip:SetActive(self.m_bInIK)

    if not self.m_IKTarget then
        self.m_IKTarget = CMainCamera.Inst:GetIKTarget()
        self.m_IKTarget.transform.position = CameraFollow.Inst.transform.position
    end

    if self.m_bInIK then
        -- 修正姿势，关闭动作界面
        self.m_expressionView:SetActive(false)

        -- 关闭摆拍模式
        if self.m_bInPlaceMode then
            self:ChangeAlpha(self.m_PlaceBtnObj, true)
            self:OnPlaceBtnClick(self.m_PlaceBtnObj)
        end

        if not self.m_IKTargetInRoot then
            self.m_IKTargetInRoot = GameObject("__IKTargetInRoot__")
        end

        -- 初始化位置
        self.m_IKTargetController.transform.localPosition = Vector3(0, 0, 0)
        self.m_IKTarget.transform.position = CameraFollow.Inst.transform.position
        self.m_IKTargetInRoot.transform.position = self.m_IKTarget.transform.position

        if self.m_IKTargetInRoot and self.m_IKTargetInRoot:GetComponent(typeof(CLookAtIKTargetCheck)) then
            GameObject.Destroy(self.m_IKTargetInRoot:GetComponent(typeof(CLookAtIKTargetCheck)))
        end
        if not self.m_IKTarget:GetComponent(typeof(CLookAtIKTargetCheck)) then
            self.m_IKTarget:AddComponent(typeof(CLookAtIKTargetCheck))
        end

        CIKMgr.Inst:SetMainPlayerIKLookAtToTarget(self.m_IKTarget.transform, true)


    else
        -- 退出IK Mode
        if not self.m_IKTargetInRoot then
            self.m_IKTargetInRoot = GameObject("__IKTargetInRoot__")
        end
        if self.m_IKTarget then
            -- 同步位置
            self.m_IKTargetInRoot.transform.position = self.m_IKTarget.transform.position

            -- 切换检查对象
            if self.m_IKTarget:GetComponent(typeof(CLookAtIKTargetCheck)) then
                GameObject.Destroy(self.m_IKTarget:GetComponent(typeof(CLookAtIKTargetCheck)))
            end
            if not self.m_IKTargetInRoot:GetComponent(typeof(CLookAtIKTargetCheck)) then
                self.m_IKTargetInRoot:AddComponent(typeof(CLookAtIKTargetCheck))
            end

            CIKMgr.Inst:SetMainPlayerIKLookAtToTarget(self.m_IKTargetInRoot.transform, false)
        end
    end
end

function CLuaScreenCaptureWnd:CloseIKMode()
    self.m_bInIK = false
    self.m_IKTip:SetActive(self.m_bInIK)
    self:ChangeAlpha(self.m_IKButton, not self.m_bInIK)
    CIKMgr.Inst:SetMainPlayerIKLookAtToTarget(nil, false)
end

function CLuaScreenCaptureWnd:OnEyeDrag(go, delta)

    if not self.m_IKTarget then
        self.m_IKTarget = CMainCamera.Inst:GetIKTarget()
        self.m_IKTarget.transform.position = CameraFollow.Inst.transform.position
    end

    if not self.m_IKTarget:GetComponent(typeof(CLookAtIKTargetCheck)) then
        self.m_IKTarget:AddComponent(typeof(CLookAtIKTargetCheck))
    end

    CIKMgr.Inst:SetMainPlayerIKLookAtToTarget(self.m_IKTarget.transform, true)
    self:UpdateIKPosition()

end

function CLuaScreenCaptureWnd:UpdateIKPosition()

    local currentPos = UICamera.currentTouch.pos
    self.m_IKTargetController.transform.position = UICamera.currentCamera:ScreenToWorldPoint(Vector3(currentPos.x, currentPos.y, 0))

    local targetPos = CMainCamera.Main:ScreenToWorldPoint(Vector3(CUIManager.MainScreenSize.x - currentPos.x, CUIManager.MainScreenSize.y - currentPos.y, -4))
    self.m_IKTarget.transform.position = targetPos
end

function CLuaScreenCaptureWnd:CachePlayerSetting()
  if not self.m_CachedPlayerSettings then
    self.m_CachedPlayerSettings = {}
  end

  if not self.m_SettingsNeedCache then
    self.m_SettingsNeedCache = {
    "WaterReflectionEnabled",             --水面反射
    "HidePetEnabled",                     --屏蔽灵兽召唤物
    "HideHpcEnabled",                     --屏蔽NPC
    "HideSkillEffect",                    --屏蔽技能特效
    "HideShenbingEffect",                 --屏蔽神兵特效
    "HideHeadInfoEnabled",                --屏蔽名字
    "VisiblePlayerLimit",                 --最大显示玩家数量
    "CaptureHideNoticeWnd",               --屏蔽喇叭和消息
    "HideMonsterInScreenCaptureWndEnabled"--屏蔽怪物
    }
  end

  for i=1,#self.m_SettingsNeedCache do
    local propertyName = self.m_SettingsNeedCache[i]
    self.m_CachedPlayerSettings[propertyName] = PlayerSettings[propertyName]
  end

  local appearProperty = CClientMainPlayer.Inst and CClientMainPlayer.Inst.AppearanceProp
  self.m_OriginalSelfSettingTable = {appearProperty and appearProperty.TalismanAppearance or 0,
                                     appearProperty and appearProperty.HideGemFxToOtherPlayer or 0,
                                     appearProperty and appearProperty.HideSuitIdToOtherPlayer or 0,
                                     appearProperty and appearProperty.HideWing or 0}
end

function CLuaScreenCaptureWnd:RecoveryCachePlayerSetting()
  if not self.m_CachedPlayerSettings then
    self.m_CachedPlayerSettings = {}
  end

  if not self.m_SettingsNeedCache then
    self.m_SettingsNeedCache = {}
  end

  for i=1,#self.m_SettingsNeedCache do
    local propertyName = self.m_SettingsNeedCache[i]
    PlayerSettings[propertyName] = self.m_CachedPlayerSettings[propertyName]
  end
end

function CLuaScreenCaptureWnd:LoadPaiZhaoSetting()
  --游戏设置
  for i=1,#self.m_SettingsNeedCache do
    local propertyName = self.m_SettingsNeedCache[i]
    local originalVale = self.m_CachedPlayerSettings[propertyName]
    PlayerSettings[propertyName] = CLuaPlayerSettings.Get(propertyName, originalVale)
  end

  self.m_LastAppearPropertySettings = {}
  local appearPropertySettings = {
    "TalismanAppearance",
    "HideGemFxToOtherPlayer",
    "HideSuitIdToOtherPlayer",
    "HideWing",
  }
  local appearProperty = CClientMainPlayer.Inst and CClientMainPlayer.Inst.AppearanceProp
  if appearProperty then
    for i=1,#appearPropertySettings do
      local appearName = appearPropertySettings[i]
      local isSelect = CLuaPlayerSettings.Get(appearName, self.m_OriginalSelfSettingTable[i] > 0)
      if appearName == "TalismanAppearance" then
        appearProperty[appearName] = isSelect and 0 or self.m_OriginalSelfSettingTable[i]
      else
        appearProperty[appearName] = isSelect and 1 or 0
      end
      table.insert(self.m_LastAppearPropertySettings, isSelect)
    end
    self:RefreshAppearance(appearProperty)
  end

  self:BlockPet(CLuaPlayerSettings.Get("BlockSelfPet", false))
  self:BlockBaby(CLuaPlayerSettings.Get("BlockSelfBaby", false))

  --镜头设置
  local lineObj = self.transform:Find("Camera/Panel/Line").gameObject
  lineObj:SetActive(CLuaPlayerSettings.Get("ShowLineCheck", false))

  if CPostProcessingMgr.s_NewPostProcessingOpen then
    local postEffectCtrl = CPostProcessingMgr.Instance.MainController
    postEffectCtrl.DepthOfFieldEnable = CLuaPlayerSettings.Get("DepthOfFieldEnable", false)
  else
    CPostEffectMgr.Instance.DepthOfFieldEnable = CLuaPlayerSettings.Get("DepthOfFieldEnable", false)
  end
  
  self:TryInitdofEffct()

  --其他设置
  CPaiZhaoMgr.Instance:setShowType(EnumPaizhaoRelation_lua.eBrother, CLuaPlayerSettings.Get("ShowBrother", true))
  CPaiZhaoMgr.Instance:setShowType(EnumPaizhaoRelation_lua.eFriend, CLuaPlayerSettings.Get("ShowFriend", true))
  CPaiZhaoMgr.Instance:setShowType(EnumPaizhaoRelation_lua.eGuildMember, CLuaPlayerSettings.Get("ShowGuildMember", true))
  CPaiZhaoMgr.Instance:setShowType(EnumPaizhaoRelation_lua.ePartner, CLuaPlayerSettings.Get("ShowPartner", true))
  CPaiZhaoMgr.Instance:setShowType(EnumPaizhaoRelation_lua.eOther, CLuaPlayerSettings.Get("ShowOther", true))
  self.m_IconGO:GetComponent(typeof(UITexture)).enabled = CLuaPlayerSettings.Get("ShowLogo", true)
  self.m_CodeObj:GetComponent(typeof(UITexture)).enabled = CLuaPlayerSettings.Get("ShowCode", false)

  --cache更新
  self.oriHideNPCHeadInfo = PlayerSettings.HideHeadInfoEnabled
end

function CLuaScreenCaptureWnd:TryInitdofEffct()
  --dofEffect在DepthOfFieldEnable启用时开协程加载,需要检查是否加载完毕
  if CPostProcessingMgr.s_NewPostProcessingOpen then
    local postEffectCtrl = CPostProcessingMgr.Instance.MainController
    if postEffectCtrl.depthOfField then
      postEffectCtrl.depthOfField.dofRange:Override(tonumber(CLuaPlayerSettings.Get("DofRange", "1")))
      postEffectCtrl.depthOfField.blinRange:Override(tonumber(CLuaPlayerSettings.Get("BlinRange", "16")))
      postEffectCtrl.depthOfField.blinIntensity:Override(tonumber(CLuaPlayerSettings.Get("BlinIntensity", "0.1")))
      postEffectCtrl.depthOfField.threshold:Override(tonumber(CLuaPlayerSettings.Get("Threshold", "2.5")))
      self.m_IsdofInited = true
    end
  else
    if CPostEffectMgr.Instance.dofEffect then
      CPostEffectMgr.Instance.dofEffect.dofRange = tonumber(CLuaPlayerSettings.Get("DofRange", "1"))
      CPostEffectMgr.Instance.dofEffect.blinRange = tonumber(CLuaPlayerSettings.Get("BlinRange", "16"))
      CPostEffectMgr.Instance.dofEffect.blinIntensity = tonumber(CLuaPlayerSettings.Get("BlinIntensity", "0.1"))
      CPostEffectMgr.Instance.dofEffect.threshold = tonumber(CLuaPlayerSettings.Get("Threshold", "2.5"))
      self.m_IsdofInited = true
    end
  end
end

function CLuaScreenCaptureWnd:MainPlayerMove(argv)
  if CClientMainPlayer.Inst.CurrentActionState == EnumActionState.Expression and CExpressionActionMgr.ExpressionActionID ~= 0 then
    Gac2Gas.RequestExpressionAction(0)
    CExpressionActionMgr.ExpressionActionID = 0
  end
  CClientMainPlayer.Inst:LeaveOfflineExpressionActionById(CClientMainPlayer.PrevOfflineExpressionID)
  CClientMainPlayer.Inst:LeaveOfflineExpressionAction()
end
