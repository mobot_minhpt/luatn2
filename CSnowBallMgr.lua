-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CEffectMgr = import "L10.Game.CEffectMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CPos = import "L10.Engine.CPos"
local CSnowBallMgr = import "L10.Game.CSnowBallMgr"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local EventManager = import "EventManager"
local IFX = import "L10.Game.IFX"
local Int32 = import "System.Int32"
local MsgPackImpl = import "MsgPackImpl"
local NPC_NPC = import "L10.Game.NPC_NPC"
local Object = import "System.Object"
local PlayerInfo = import "L10.Game.CSnowBallMgr+PlayerInfo"
local String = import "System.String"
local UInt32 = import "System.UInt32"
local Utility = import "L10.Engine.Utility"
local XueQiuDaZhan_XueQiu = import "L10.Game.XueQiuDaZhan_XueQiu"
CSnowBallMgr.m_GetSnowBallState_CS2LuaHook = function (this)
    if CClientMainPlayer.Inst == nil then
        return 0
    end

    local info = XueQiuDaZhan_XueQiu.GetData()
    if info ~= nil then
        if CClientMainPlayer.Inst.PlayProp.PlayId == info.GamePlayId then
            return 1
        end
        if CClientMainPlayer.Inst.PlayProp.PlayId == info.DoubleQualifyGamePlayId then
            return 2
        end
    end
    return 0
end
CSnowBallMgr.m_EnterSnowBallFight_CS2LuaHook = function (this)
    CUIManager.ShowUI(CUIResources.SnowBallTopRightWnd)
    CUIManager.ShowUI(CUIResources.SnowBallOperateWnd)
    CUIManager.CloseUI(CUIResources.ThumbnailChatWindow)
    g_MessageMgr:ShowMessage("HANJIA_XQDZ_TIP")
end

Gas2Gac.SyncXueQiuPlayFightInfo = function (allNum, aliveNum, executeNum, score, executeScore)
    --Debug.Log(string.Format("SyncXueQiuPlayInfo {0} {1} {2} {3} {4}", allNum, aliveNum, executeNum, score, executeScore));
    --Debug.Log(string.Format("SyncXueQiuPlayInfo {0} {1} {2} {3} {4}", allNum, aliveNum, executeNum, score, executeScore));
    CSnowBallMgr.Inst.allNum = allNum
    CSnowBallMgr.Inst.aliveNum = aliveNum
    CSnowBallMgr.Inst.executeNum = executeNum
    CSnowBallMgr.Inst.score = score
    CSnowBallMgr.Inst.executeScore = executeScore
    EventManager.Broadcast(EnumEventType.SnowBallUpdateInfo)
end
Gas2Gac.SyncXueQiuPlayResult = function (rank, executeNum, score)
    --Debug.Log(string.Format("SyncXueQiuPlayResult {0} {1} {2}", rank, executeNum, score));
    --Debug.Log(string.Format("SyncXueQiuPlayResult {0} {1} {2}", rank, executeNum, score));
    CSnowBallMgr.Inst.result_rank = rank
    CSnowBallMgr.Inst.result_executeNum = executeNum
    CSnowBallMgr.Inst.result_score = score
    CSnowBallMgr.Inst.showResultPanelType = 1
    RegisterTickWithDuration(function ()
      CUIManager.ShowUI(CUIResources.SnowBallResultWnd)
    end,1000,1000)
end
Gas2Gac.UpdateXueQiuSkillItemSlots = function (data, operation, pos, skillItemId, engineId, x, y)
    --Debug.Log(string.Format("UpdateXueQiuSkillItemSlots {0} {1} {2} {3} {4}", pos, skillItemId, engineId, x, y));
    --Debug.Log(string.Format("UpdateXueQiuSkillItemSlots {0} {1} {2} {3} {4}", pos, skillItemId, engineId, x, y));
    CommonDefs.ListClear(CSnowBallMgr.Inst.skill_list)
    local ids = TypeAs(MsgPackImpl.unpack(data), typeof(MakeGenericClass(List, Object)))
    do
        local i = 0
        while i < ids.Count do
            local id = math.floor(tonumber(ids[i] or 0))
            --Debug.Log(string.Format("XueQiuSkillItemSlot {0} {1}", i, id));
            CommonDefs.ListAdd(CSnowBallMgr.Inst.skill_list, typeof(UInt32), id)
            i = i + 1
        end
    end

    CSnowBallMgr.Inst.skill_operation = operation
    CSnowBallMgr.Inst.skill_pos = pos
    CSnowBallMgr.Inst.skill_itemId = skillItemId
    CSnowBallMgr.Inst.skill_engineId = engineId
    CSnowBallMgr.Inst.skill_x = x
    CSnowBallMgr.Inst.skill_y = y
    EventManager.Broadcast(EnumEventType.SnowBallUpdateSkillInfo)
end
Gas2Gac.QueryXueQiuPlayInfoResult = function (data, bPlayEnd)
    --UnityEngine.Debug.Log(string.Format("QueryXueQiuPlayInfoResult " + bPlayEnd));
    --UnityEngine.Debug.Log(string.Format("QueryXueQiuPlayInfoResult " + bPlayEnd));
    CommonDefs.ListClear(CSnowBallMgr.Inst.snowRankList)
    local datas = TypeAs(MsgPackImpl.unpack(data), typeof(MakeGenericClass(List, Object)))
    if datas ~= nil then
        do
            local i = 0
            while i < datas.Count do
                local info = CreateFromClass(PlayerInfo)
                info.id = tonumber(datas[i])
                info.name = tostring(datas[i + 1])
                info.killNum = math.floor(tonumber(datas[i + 2] or 0))
                info.score = math.floor(tonumber(datas[i + 3] or 0))
                info.rank = math.floor(tonumber(datas[i + 4] or 0))
                info.executed = CommonDefs.Convert_ToBoolean(datas[i + 5])
                --UnityEngine.Debug.Log(string.Format("QueryXueQiuPlayInfoResult info: {0} {1} {2} {3} {4} {5}", id, name, killNum, score, rank, executed));
                CommonDefs.ListAdd(CSnowBallMgr.Inst.snowRankList, typeof(PlayerInfo), info)
                i = i + 6
            end
        end
    end
    CUIManager.ShowUI(CUIResources.SnowBallFightInfoWnd)
end
Gas2Gac.AddXueQiuUnsafeRegionFxs = function (data, fxId, dir, bNeedRemove, removeData)

    if CClientMainPlayer.Inst ~= nil then
        local removePositions = TypeAs(MsgPackImpl.unpack(removeData), typeof(MakeGenericClass(List, Object)))
        if removePositions ~= nil then
            do
                local i = 0
                while i < removePositions.Count do
                    local posX = math.floor(tonumber(removePositions[i] or 0))
                    local posY = math.floor(tonumber(removePositions[i + 1] or 0))
                    local key = posX * 1000 + posY
                    local __try_get_result, removeFx = CommonDefs.DictTryGet(CSnowBallMgr.Inst.regionFxDict, typeof(Int32), key, typeof(IFX))
                    if removeFx ~= nil then
                        removeFx:Destroy()
                        removeFx = nil
                    end
                    CommonDefs.DictRemove(CSnowBallMgr.Inst.regionFxDict, typeof(Int32), key)
                    i = i + 2
                end
            end
        end

        local positions = TypeAs(MsgPackImpl.unpack(data), typeof(MakeGenericClass(List, Object)))
        if positions ~= nil then
            do
                local i = 0
                while i < positions.Count do
                    local posX = math.floor(tonumber(positions[i] or 0))
                    local posY = math.floor(tonumber(positions[i + 1] or 0))
                    local pos = CreateFromClass(CPos, posX, posY)
                    local fx = CEffectMgr.Inst:AddWorldPositionFX(fxId, Utility.GridPos2PixelPos(pos), dir, 0, 1, -1, EnumWarnFXType.None, nil, 0, 0, nil)
                    if bNeedRemove then
                        local key = posX * 1000 + posY
                        if CommonDefs.DictContains(CSnowBallMgr.Inst.regionFxDict, typeof(Int32), key) then
                            local __try_get_result, removeFx = CommonDefs.DictTryGet(CSnowBallMgr.Inst.regionFxDict, typeof(Int32), key, typeof(IFX))
                            if removeFx ~= nil then
                                removeFx:Destroy()
                                removeFx = nil
                            end
                            CommonDefs.DictRemove(CSnowBallMgr.Inst.regionFxDict, typeof(Int32), key)
                        end
                        CommonDefs.DictAdd(CSnowBallMgr.Inst.regionFxDict, typeof(Int32), key, typeof(IFX), fx)
                    end
                    i = i + 2
                end
            end
        end
    end
end
Gas2Gac.QueryXueQiuPlayLocationInfoResult = function (data)
    if data == nil then
        return
    end
    local locationData = TypeAs(MsgPackImpl.unpack(data), typeof(MakeGenericClass(Dictionary, String, Object)))
    if locationData == nil then
        return
    end

    CommonDefs.DictIterate(locationData, DelegateFactory.Action_object_object(function (___key, ___value)
        local v = {}
        v.Key = ___key
        v.Value = ___value
        local continue
        repeat
            if (v.Key == "Npc") then
                local locations = TypeAs(v.Value, typeof(MakeGenericClass(List, Object)))
                if locations == nil then
                    continue = true
                    break
                end
                do
                    local i = 0
                    while i < locations.Count do
                        local npcEngineId = math.floor(tonumber(locations[i] or 0))
                        local npcTemplateId = math.floor(tonumber(locations[i + 1] or 0))
                        local x = math.floor(tonumber(locations[i + 2] or 0))
                        local y = math.floor(tonumber(locations[i + 3] or 0))
                        local npc = NPC_NPC.GetData(npcTemplateId)
                        if npc ~= nil then
                            EventManager.BroadcastInternalForLua(EnumEventType.UpdateNPCMiniMapInfo, {npcEngineId, x, y})
                        end
                        i = i + 4
                    end
                end
            end
            continue = true
        until 1
        if not continue then
            return
        end
    end))
end



Gas2Gac.SyncXueQiuDoublePlayFightInfo = function (allNum, aliveNum, executeScore, teamPlayInfo_U)
    --Debug.Log(string.Format("SyncXueQiuPlayInfo {0} {1} {2} {3} {4}", allNum, aliveNum, executeNum, score, executeScore));
    --Debug.Log(string.Format("SyncXueQiuPlayInfo {0} {1} {2} {3} {4}", allNum, aliveNum, executeNum, score, executeScore));
    CSnowBallMgr.Inst.allNum = allNum
    CSnowBallMgr.Inst.aliveNum = aliveNum
    CSnowBallMgr.Inst.executeScore = executeScore

    if teamPlayInfo_U ~= nil then
        local teamDic = TypeAs(MsgPackImpl.unpack(teamPlayInfo_U), typeof(MakeGenericClass(Dictionary, String, Object)))
        if teamDic == nil then
            CSnowBallMgr.Inst.executeNum = 0
            CSnowBallMgr.Inst.score = 0
        else
            CSnowBallMgr.Inst.executeNum = 0
            if CommonDefs.DictContains(teamDic, typeof(String), "playerPlayInfo") then
                local teamplayerInfo = TypeAs(CommonDefs.DictGetValue(teamDic, typeof(String), "playerPlayInfo"), typeof(MakeGenericClass(Dictionary, String, Object)))
                CommonDefs.DictIterate(teamplayerInfo, DelegateFactory.Action_object_object(function (___key, ___value)
                    local v = {}
                    v.Key = ___key
                    v.Value = ___value
                    if System.UInt64.Parse(v.Key) == CClientMainPlayer.Inst.Id then
                        local playerValue = TypeAs(v.Value, typeof(MakeGenericClass(Dictionary, String, Object)))
                        CSnowBallMgr.Inst.executeNum = math.floor(tonumber(CommonDefs.DictGetValue(playerValue, typeof(String), "killNum") or 0))
                    end
                end))
            end
            if CommonDefs.DictContains(teamDic, typeof(String), "teamScore") then
                CSnowBallMgr.Inst.score = math.floor(tonumber(CommonDefs.DictGetValue(teamDic, typeof(String), "teamScore") or 0))
            else
                CSnowBallMgr.Inst.score = 0
            end
        end
    else
        CSnowBallMgr.Inst.executeNum = 0
        CSnowBallMgr.Inst.score = 0
    end

    EventManager.Broadcast(EnumEventType.SnowBallUpdateInfo)
end

Gas2Gac.SyncXueQiuDoublePlayResult = function (result_U, teamPlayInfo_U)
    --Debug.Log(string.Format("SyncXueQiuPlayResult {0} {1} {2}", rank, executeNum, score));
    --Debug.Log(string.Format("SyncXueQiuPlayResult {0} {1} {2}", rank, executeNum, score));
    local resultDic = TypeAs(MsgPackImpl.unpack(result_U), typeof(MakeGenericClass(Dictionary, String, Object)))
    if resultDic == nil then
        return
    end
    CSnowBallMgr.Inst.result_rank = math.floor(tonumber(CommonDefs.DictGetValue(resultDic, typeof(String), "teamRank") or 0))
    CSnowBallMgr.Inst.result_executeNum = 0
    CSnowBallMgr.Inst.result_score = math.floor(tonumber(CommonDefs.DictGetValue(resultDic, typeof(String), "teamScore") or 0))

    CSnowBallMgr.Inst.result_teammate_rank = math.floor(tonumber(CommonDefs.DictGetValue(resultDic, typeof(String), "teamRank") or 0))
    CSnowBallMgr.Inst.result_teammate_score = math.floor(tonumber(CommonDefs.DictGetValue(resultDic, typeof(String), "teamScore") or 0))

    local teammateDic = TypeAs(MsgPackImpl.unpack(teamPlayInfo_U), typeof(MakeGenericClass(Dictionary, String, Object)))
    if teammateDic == nil then
        return
    end
    local teamplayerInfo = TypeAs(CommonDefs.DictGetValue(teammateDic, typeof(String), "playerPlayInfo"), typeof(MakeGenericClass(Dictionary, String, Object)))
    CommonDefs.DictIterate(teamplayerInfo, DelegateFactory.Action_object_object(function (___key, ___value)
        local v = {}
        v.Key = ___key
        v.Value = ___value
        if System.UInt64.Parse(v.Key) ~= CClientMainPlayer.Inst.Id then
            local playerValue = TypeAs(v.Value, typeof(MakeGenericClass(Dictionary, String, Object)))
            CSnowBallMgr.Inst.result_teammate_name = tostring(CommonDefs.DictGetValue(playerValue, typeof(String), "name"))
            CSnowBallMgr.Inst.result_teammate_lv = math.floor(tonumber(CommonDefs.DictGetValue(playerValue, typeof(String), "grade") or 0))
            CSnowBallMgr.Inst.result_teammate_class = math.floor(tonumber(CommonDefs.DictGetValue(playerValue, typeof(String), "role") or 0))
            CSnowBallMgr.Inst.result_teammate_gender = math.floor(tonumber(CommonDefs.DictGetValue(playerValue, typeof(String), "gender") or 0))
            CSnowBallMgr.Inst.result_teammate_executeNum = math.floor(tonumber(CommonDefs.DictGetValue(playerValue, typeof(String), "killNum") or 0))
        end
        if System.UInt64.Parse(v.Key) == CClientMainPlayer.Inst.Id then
            local playerValue = TypeAs(v.Value, typeof(MakeGenericClass(Dictionary, String, Object)))
            CSnowBallMgr.Inst.result_executeNum = math.floor(tonumber(CommonDefs.DictGetValue(playerValue, typeof(String), "killNum") or 0))
        end
    end))
    CSnowBallMgr.Inst.showResultPanelType = 2

    RegisterTickWithDuration(function ()
      CUIManager.ShowUI(CUIResources.SnowBallResultWnd)
    end,1000,1000)
end
Gas2Gac.UpdateXueQiuDoubleSkillItemSlots = function (data, operation, pos, skillItemId, engineId, x, y)
    --Debug.Log(string.Format("UpdateXueQiuSkillItemSlots {0} {1} {2} {3} {4}", pos, skillItemId, engineId, x, y));
    --Debug.Log(string.Format("UpdateXueQiuSkillItemSlots {0} {1} {2} {3} {4}", pos, skillItemId, engineId, x, y));
    CommonDefs.ListClear(CSnowBallMgr.Inst.skill_list)
    local ids = TypeAs(MsgPackImpl.unpack(data), typeof(MakeGenericClass(List, Object)))
    do
        local i = 0
        while i < ids.Count do
            local id = math.floor(tonumber(ids[i] or 0))
            --Debug.Log(string.Format("XueQiuSkillItemSlot {0} {1}", i, id));
            CommonDefs.ListAdd(CSnowBallMgr.Inst.skill_list, typeof(UInt32), id)
            i = i + 1
        end
    end

    CSnowBallMgr.Inst.skill_operation = operation
    CSnowBallMgr.Inst.skill_pos = pos
    CSnowBallMgr.Inst.skill_itemId = skillItemId
    CSnowBallMgr.Inst.skill_engineId = engineId
    CSnowBallMgr.Inst.skill_x = x
    CSnowBallMgr.Inst.skill_y = y
    EventManager.Broadcast(EnumEventType.SnowBallUpdateSkillInfo)
end
Gas2Gac.QueryXueQiuDoublePlayInfoResult = function (data, bPlayEnd, teamPlayInfo)
    --UnityEngine.Debug.Log(string.Format("QueryXueQiuPlayInfoResult " + bPlayEnd));
    --UnityEngine.Debug.Log(string.Format("QueryXueQiuPlayInfoResult " + bPlayEnd));
    CommonDefs.ListClear(CSnowBallMgr.Inst.snowRankList)
    local datas = TypeAs(MsgPackImpl.unpack(data), typeof(MakeGenericClass(List, Object)))
    local teamInfo = TypeAs(MsgPackImpl.unpack(teamPlayInfo), typeof(MakeGenericClass(Dictionary, String, Object)))
    if datas ~= nil then
        do
            local i = 0
            while i < datas.Count do
                local info = CreateFromClass(PlayerInfo)
                info.id = tonumber(datas[i])
                info.name = tostring(datas[i + 1])
                info.killNum = math.floor(tonumber(datas[i + 2] or 0))
                info.score = math.floor(tonumber(datas[i + 3] or 0))
                info.rank = math.floor(tonumber(datas[i + 4] or 0))
                info.executed = CommonDefs.Convert_ToBoolean(datas[i + 5])
                --UnityEngine.Debug.Log(string.Format("QueryXueQiuPlayInfoResult info: {0} {1} {2} {3} {4} {5}", id, name, killNum, score, rank, executed));
                if teamPlayInfo ~= nil then
                    local playerInfos = TypeAs(CommonDefs.DictGetValue(teamInfo, typeof(String), "playerPlayInfo"), typeof(MakeGenericClass(Dictionary, String, Object)))
                    if playerInfos ~= nil and CommonDefs.DictContains(playerInfos, typeof(String), tostring(info.id)) then
                        info.teammate = true
                    end
                end
                CommonDefs.ListAdd(CSnowBallMgr.Inst.snowRankList, typeof(PlayerInfo), info)
                i = i + 6
            end
        end
    end
    CUIManager.ShowUI(CUIResources.SnowBallFightInfoWnd)
end
Gas2Gac.AddXueQiuDoubleUnsafeRegionFxs = function (data, fxId, dir, bNeedRemove, removeData)

    if CClientMainPlayer.Inst ~= nil then
        local removePositions = TypeAs(MsgPackImpl.unpack(removeData), typeof(MakeGenericClass(List, Object)))
        if removePositions ~= nil then
            do
                local i = 0
                while i < removePositions.Count do
                    local posX = math.floor(tonumber(removePositions[i] or 0))
                    local posY = math.floor(tonumber(removePositions[i + 1] or 0))
                    local key = posX * 1000 + posY
                    local __try_get_result, removeFx = CommonDefs.DictTryGet(CSnowBallMgr.Inst.regionFxDict, typeof(Int32), key, typeof(IFX))
                    if removeFx ~= nil then
                        removeFx:Destroy()
                        removeFx = nil
                    end
                    CommonDefs.DictRemove(CSnowBallMgr.Inst.regionFxDict, typeof(Int32), key)
                    i = i + 2
                end
            end
        end

        local positions = TypeAs(MsgPackImpl.unpack(data), typeof(MakeGenericClass(List, Object)))
        if positions ~= nil then
            do
                local i = 0
                while i < positions.Count do
                    local posX = math.floor(tonumber(positions[i] or 0))
                    local posY = math.floor(tonumber(positions[i + 1] or 0))
                    local pos = CreateFromClass(CPos, posX, posY)
                    local fx = CEffectMgr.Inst:AddWorldPositionFX(fxId, Utility.GridPos2PixelPos(pos), dir, 0, 1, -1, EnumWarnFXType.None, nil, 0, 0, nil)
                    if bNeedRemove then
                        local key = posX * 1000 + posY
                        if CommonDefs.DictContains(CSnowBallMgr.Inst.regionFxDict, typeof(Int32), key) then
                            local __try_get_result, removeFx = CommonDefs.DictTryGet(CSnowBallMgr.Inst.regionFxDict, typeof(Int32), key, typeof(IFX))
                            if removeFx ~= nil then
                                removeFx:Destroy()
                                removeFx = nil
                            end
                            CommonDefs.DictRemove(CSnowBallMgr.Inst.regionFxDict, typeof(Int32), key)
                        end
                        CommonDefs.DictAdd(CSnowBallMgr.Inst.regionFxDict, typeof(Int32), key, typeof(IFX), fx)
                    end
                    i = i + 2
                end
            end
        end
    end
end
Gas2Gac.QueryXueQiuDoublePlayLocationInfoResult = function (data)
    if data == nil then
        return
    end
    local locationData = TypeAs(MsgPackImpl.unpack(data), typeof(MakeGenericClass(Dictionary, String, Object)))
    if locationData == nil then
        return
    end

    CommonDefs.DictIterate(locationData, DelegateFactory.Action_object_object(function (___key, ___value)
        local v = {}
        v.Key = ___key
        v.Value = ___value
        local continue
        repeat
            if (v.Key == "Npc") then
                local locations = TypeAs(v.Value, typeof(MakeGenericClass(List, Object)))
                if locations == nil then
                    continue = true
                    break
                end
                do
                    local i = 0
                    while i < locations.Count do
                        local npcEngineId = math.floor(tonumber(locations[i] or 0))
                        local npcTemplateId = math.floor(tonumber(locations[i + 1] or 0))
                        local x = math.floor(tonumber(locations[i + 2] or 0))
                        local y = math.floor(tonumber(locations[i + 3] or 0))
                        local npc = NPC_NPC.GetData(npcTemplateId)
                        if npc ~= nil then
                            EventManager.BroadcastInternalForLua(EnumEventType.UpdateNPCMiniMapInfo, {npcEngineId, x, y})
                        end
                        i = i + 4
                    end
                end
            end
            continue = true
        until 1
        if not continue then
            return
        end
    end))
end
