local ShareMgr=import "ShareMgr"
local CUITexture=import "L10.UI.CUITexture"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local Main = import "L10.Engine.Main"
local HTTPHelper = import "L10.Game.HTTPHelper"
local Json = import "L10.Game.Utils.Json"
local CLingShouModelTextureLoader=import "L10.UI.CLingShouModelTextureLoader"
local CLoginMgr=import "L10.Game.CLoginMgr"


CLuaQMPKJiXiangWuShareWnd=class()
RegistClassMember(CLuaQMPKJiXiangWuShareWnd, "m_TitleLabel")
RegistClassMember(CLuaQMPKJiXiangWuShareWnd, "m_UrlCoroutine")
RegistClassMember(CLuaQMPKJiXiangWuShareWnd,"m_ModelTextureLoader")
RegistClassMember(CLuaQMPKJiXiangWuShareWnd,"m_ImgUrl")


function CLuaQMPKJiXiangWuShareWnd:Init()
	self.m_ImgUrl=nil
	self.m_TitleLabel=FindChild(self.transform,"TitleLabel"):GetComponent(typeof(UILabel))
	self.m_TitleLabel.text=nil

    local button1=FindChild(self.transform,"Button1").gameObject
    UIEventListener.Get(button1).onClick=DelegateFactory.VoidDelegate(function(go)
		--分享至梦岛
		self:Share(2)
    end)
    local button2=FindChild(self.transform,"Button2").gameObject
    UIEventListener.Get(button2).onClick=DelegateFactory.VoidDelegate(function(go)
        --分享至其他
		self:Share(1)
    end)
	self.m_ModelTextureLoader=FindChild(self.transform,"ModelTexture"):GetComponent(typeof(CLingShouModelTextureLoader))

	self.m_ModelTextureLoader:Init(CLuaQMPKMgr.m_ShareTemplateId,0,CLuaQMPKMgr.m_ShareEvolveGradeForSkin)
	local designData=QuanMinPK_Stage.GetData(CLuaQMPKMgr.m_ShareStage)
	if designData then
		self.m_TitleLabel.text=designData.Name
	end


	local tf=FindChild(self.transform,"PlayerInfoNode")
    self.m_SelfNameLabel = tf:Find("NameLabel"):GetComponent(typeof(UILabel))
	self.m_SelfServerLabel = tf:Find("ServerLabel"):GetComponent(typeof(UILabel))
	self.m_SelfLvlLabel = tf:Find("LevelLabel"):GetComponent(typeof(UILabel))
	self.m_SelfIDLabel = tf:Find("ID"):GetComponent(typeof(UILabel))
	self.m_SelfIconTexture = tf:Find("Icon"):GetComponent(typeof(CUITexture))
	if CClientMainPlayer.Inst then
		self.m_SelfNameLabel.text = CClientMainPlayer.Inst.Name
		local myserver = CLoginMgr.Inst:GetSelectedGameServer()
		self.m_SelfServerLabel.text = myserver.name
		self.m_SelfLvlLabel.text = CClientMainPlayer.Inst.Level .. ""
		self.m_SelfIDLabel.text = CClientMainPlayer.Inst.Id .. ""
		local portraitName = CUICommonDef.GetPortraitName(EnumToInt(CClientMainPlayer.Inst.Class), 
		EnumToInt(CClientMainPlayer.Inst.Gender),-1)
		self.m_SelfIconTexture:LoadNPCPortrait(portraitName,false)
	end

end


function CLuaQMPKJiXiangWuShareWnd:Share(type)
	if self.m_ImgUrl then
		--统一分享 2018.12.03 cgz
		ShareMgr.ShareWebImage2Other(self.m_ImgUrl)
		
		return
	end

	if self.m_UrlCoroutine ~= nil then
		Main.Inst:StopCoroutine(self.m_UrlCoroutine)
		self.m_UrlCoroutine=nil
	end
	
	local url=CLuaQMPKMgr.m_ShareUrl.."shareImg/mascot?"
	local inst=CClientMainPlayer.Inst
	if inst then
		local serverName=CLoginMgr.Inst:GetSelectedGameServer().name
		url = url..SafeStringFormat3("roleid=%s&rolename=%s&gender=%s&careerid=%s&servername=%s&grade=%s&stageid=%s",
			inst.Id,
			inst.Name,
			EnumToInt(inst.Gender),
			EnumToInt(inst.Class),
			serverName,
			inst.Level,
			CLuaQMPKMgr.m_ShareStage
		)
	end

	self.m_UrlCoroutine = Main.Inst:StartCoroutine(HTTPHelper.GetUrl(url, DelegateFactory.Action_bool_string(function (success, ret) 
		if success then
			local dict = Json.Deserialize(ret)
			local code = CommonDefs.DictGetValue_LuaCall(dict,"code")
			if code and code==1 then
				--成功
				self.m_ImgUrl = CommonDefs.DictGetValue_LuaCall(dict,"imgurl")
				if self.m_ImgUrl then
					if type==1 then
						local id=CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
						local origin_id=id
						CLuaShareMgr.ShareQMPKImage2PersonalSpace(id,origin_id,self.m_ImgUrl)
					else
						ShareMgr.ShareWebImage2Other(self.m_ImgUrl)
					end
				end
			end	
		else
		end
		self.m_UrlCoroutine=nil
	end)))
end

function CLuaQMPKJiXiangWuShareWnd:OnDestroy()
	if self.m_UrlCoroutine ~= nil then
		Main.Inst:StopCoroutine(self.m_UrlCoroutine)
		self.m_UrlCoroutine=nil
	end
end
return CLuaQMPKJiXiangWuShareWnd
