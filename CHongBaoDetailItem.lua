-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CHongBaoDetailItem = import "L10.UI.CHongBaoDetailItem"
local CHongBaoMgr = import "L10.UI.CHongBaoMgr"
local CHongbaoSnatchRecord = import "L10.UI.CHongbaoSnatchRecord"
local CItem = import "L10.Game.CItem"
local Color = import "UnityEngine.Color"
local EnumHongbaoType = import "L10.UI.EnumHongbaoType"
local LocalString = import "LocalString"
CHongBaoDetailItem.m_UpdateData_CS2LuaHook = function (this, data) 
    this.m_PlayerNameLabel.text = data.SnatchPlayerName
    this.m_PlayerNameLabel.color = Color.white
    if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.Id == data.PlayerId then
        this.m_PlayerNameLabel.text = LocalString.GetString("我的手气")
        this.m_PlayerNameLabel.color = Color.yellow
    end
    if CHongBaoMgr.m_HongbaoType == EnumHongbaoType.Gift then
        this.m_MoneyLabel.text = (CItem.GetName(data.ItemId) .. LocalString.GetString("*")) .. tostring(data.ItemNum)
        return
    end
    this.m_MoneyLabel.text = tostring(data.Silver)
    if data.Status == CHongbaoSnatchRecord.CSnatchStatus.Least then
        this.m_MarkLabel.color = Color.gray
        this.m_MarkLabel.gameObject:SetActive(true)
        this.m_MarkLabel.text = LocalString.GetString("手气最差")
    elseif data.Status == CHongbaoSnatchRecord.CSnatchStatus.Most then
        this.m_MarkLabel.color = Color.red
        this.m_MarkLabel.gameObject:SetActive(true)
        this.m_MarkLabel.text = LocalString.GetString("手气最佳")
    elseif data.Status == CHongbaoSnatchRecord.CSnatchStatus.CurrentLeast then
        this.m_MarkLabel.color = Color.gray
        this.m_MarkLabel.gameObject:SetActive(true)
        this.m_MarkLabel.text = LocalString.GetString("当前最差")
    elseif data.Status == CHongbaoSnatchRecord.CSnatchStatus.CurrentMost then
        this.m_MarkLabel.color = Color.red
        this.m_MarkLabel.gameObject:SetActive(true)
        this.m_MarkLabel.text = LocalString.GetString("当前最佳")
    else
        this.m_MarkLabel.gameObject:SetActive(false)
    end
end
