local UIGrid = import "UIGrid"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local UISprite = import "UISprite"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CItemMgr = import "L10.Game.CItemMgr"
local DelegateFactory = import "DelegateFactory"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CTooltipAlignType = import "L10.UI.CTooltip+AlignType"
local CommonDefs = import "L10.Game.CommonDefs"
local UInt32 = import "System.UInt32"
local UITexture = import "UITexture"
local CBaseWnd = import "L10.UI.CBaseWnd"

LuaEquipIdentifySkillItemChooseWnd = class()
RegistChildComponent(LuaEquipIdentifySkillItemChooseWnd ,"grid", UIGrid)
RegistChildComponent(LuaEquipIdentifySkillItemChooseWnd ,"template", GameObject)
RegistClassMember(LuaEquipIdentifySkillItemChooseWnd,"itemsInfo")
RegistClassMember(LuaEquipIdentifySkillItemChooseWnd,"isSelected")
RegistClassMember(LuaEquipIdentifySkillItemChooseWnd,"lvTianShuItemIdSet")
RegistClassMember(LuaEquipIdentifySkillItemChooseWnd,"curSelectableSequence")
RegistClassMember(LuaEquipIdentifySkillItemChooseWnd,"equipNeedLevel")
function LuaEquipIdentifySkillItemChooseWnd:Awake()
    self.template:SetActive(false)
    Extensions.RemoveAllChildren(self.grid.transform)

    self.itemsInfo = nil

    self.lvTianShuItemIdSet = nil
end

function LuaEquipIdentifySkillItemChooseWnd:OnDisable()
    LuaEquipIdentifySkillMgr:OnItemChooseWndDisable()
end

function LuaEquipIdentifySkillItemChooseWnd:Init()
    self.template:SetActive(false)
    self.equipNeedLevel = LuaEquipIdentifySkillMgr:GetItemChooseWnd_EquipNeedLevel()
    self.itemsInfo = {}
    local itemsData = LuaEquipIdentifySkillMgr:GetCostItemData()
    local SelectableSequence = LuaEquipIdentifySkillMgr:GetItemChooseWndSelectableCostItem()
    self.isSelected = SelectableSequence and true or false
    self.curSelectableSequence = SelectableSequence and SelectableSequence or {}

    local maxLevel = 0
    IdentifySkill_JianXiaLevel.Foreach(function (key,v)
        if key < self.equipNeedLevel and maxLevel < key then
            self.lvTianShuItemIdSet = v.LvTianShuItemId
            maxLevel = key
        end
    end)

    for i, v in ipairs(itemsData) do
        -- 生成可以使用的道具列表
        local ownNum = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, v.itemTemplateId)
        if (self.lvTianShuItemIdSet and CommonDefs.HashSetContains(self.lvTianShuItemIdSet, typeof(UInt32), v.itemTemplateId) and ownNum > 0 )or
                (v.washItemType == EnumIdentifyWashItemType.JianDingTianShu) or
                (v.washItemType == EnumIdentifyWashItemType.TaiChuJingYuan)
        then
            local obj = NGUITools.AddChild(self.grid.gameObject, self.template)

            local itemData = Item_Item.GetData(v.itemTemplateId)
            obj.transform:Find("NameLabel"):GetComponent(typeof(UILabel)).text = itemData.Name
            local iconTexture = obj.transform:Find("IconTexture")
            iconTexture:GetComponent(typeof(CUITexture)):LoadMaterial(itemData.Icon)

            local data = {
                go = obj,
                disableSprite = obj.transform:Find("DisableBg"):GetComponent(typeof(UISprite)),
                centerLabel = obj.transform:Find("CenterLabel"):GetComponent(typeof(UILabel)),
                amountLabel = obj.transform:Find("AmountLabel"):GetComponent(typeof(UILabel)),
                unUseSprite = obj.transform:Find("UnUseSprite"):GetComponent(typeof(UITexture)),
                selectSprite = obj.transform:Find("SelectSprite"):GetComponent(typeof(UISprite)),
                itemTemplateId = v.itemTemplateId,
                type = v.washItemType,
                num = v.num,
            }
            --索引1,2,3,4,5,6分别为1~4级鉴定天书，通用鉴定天书，太初精元
            self.itemsInfo[i] = data
            self.itemsInfo[i].unUseSprite.enabled = false
            local j = i
            self.itemsInfo[i].CanUse = function() return not self.itemsInfo[j].unUseSprite.enabled end
            self.itemsInfo[i].CanSelect = function()
                return (not self.itemsInfo[j].disableSprite.enabled) and self.itemsInfo[j].CanUse()
            end

            local index = i
            CommonDefs.AddOnClickListener(self.itemsInfo[i].go, DelegateFactory.Action_GameObject(function(go) self:OnSelect(go, index) end), false)
        else
            self.curSelectableSequence[v.itemTemplateId] = nil
        end
    end

    for i, v in ipairs(itemsData) do
        local ownNum = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, v.itemTemplateId)
        local templateID = v.itemTemplateId
        if self.curSelectableSequence[templateID] then
            -- 检查道具的选择状态
            if v.washItemType == EnumIdentifyWashItemType.TaiChuJingYuan then
                if ownNum < v.num then
                    self.curSelectableSequence[templateID] = nil
                end
            elseif v.washItemType == EnumIdentifyWashItemType.JianDingTianShu then
                --if ownNum < v.num then
                --    self.curSelectableSequence[templateID] = nil
                --end
            else
                local numOfJianDingTianShu = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, self.itemsInfo[5].itemTemplateId)
                if ((ownNum + numOfJianDingTianShu) < v.num) then
                    self.curSelectableSequence[templateID] = nil
                end
            end
        end
    end
    LuaEquipIdentifySkillMgr:OnItemChooseWndSelectCallback(self.curSelectableSequence, self:GetCostItemType())

    self:RefreshData()
    self.grid:Reposition()

end

function LuaEquipIdentifySkillItemChooseWnd:RefreshData()

    for k,v in pairs(self.itemsInfo) do
        if not self.isSelected then
            v.disableSprite.enabled = false
            v.unUseSprite.enabled = false
            v.selectSprite.gameObject:SetActive(false)
        end
        local ownNum = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, v.itemTemplateId)
        v.amountLabel.text = SafeStringFormat3("%d", ownNum)

        --不可用图标
        --if not CommonDefs.HashSetContains(self.lvTianShuItemIdSet, typeof(UInt32), v.itemTemplateId) then
        --    if v.type ~= EnumIdentifyWashItemType.JianDingTianShu and v.type ~= EnumIdentifyWashItemType.TaiChuJingYuan then
        --        v.disableSprite.enabled = true
        --        v.unUseSprite.enabled = true
        --    end
        --end

        --是否已勾选
        v.selectSprite.gameObject:SetActive(self.curSelectableSequence[v.itemTemplateId] and true or false)

        --是否有获取事件
        if v.type == EnumIdentifyWashItemType.TaiChuJingYuan then
            v.centerLabel.enabled  = (ownNum < v.num)
        elseif v.type == EnumIdentifyWashItemType.JianDingTianShu then
            v.centerLabel.enabled =  (ownNum < v.num)
        else
            local numOfJianDingTianShu = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, self.itemsInfo[5].itemTemplateId)
            v.centerLabel.enabled = ((ownNum + numOfJianDingTianShu) < v.num)
        end

        if not v.go.activeSelf then
            v.go:SetActive(true)
        end
    end

    -- 重新蒙黑
    self:ReDisableItemClick()
end

function LuaEquipIdentifySkillItemChooseWnd:OnSelect(go, index)

    --不可响应点击
    if not self.itemsInfo[index].CanSelect() then
        if not self.itemsInfo[index].CanUse() then
        end
        return
    end

    --触发获取字符事件
    if self.itemsInfo[index].centerLabel.enabled then
        self:OnItemCellClick(go, self.itemsInfo[index])
        return
    end

    --尚未勾选
    if not self.isSelected then
        self.isSelected = true
    end

    --已勾选
    if self.curSelectableSequence[self.itemsInfo[index].itemTemplateId] then

        if self.itemsInfo[index].type ~= EnumIdentifyWashItemType.JianDingTianShu then
            self.curSelectableSequence = {}
            self:UpdateSelecteableInfo(false)
        else
            self.curSelectableSequence[self.itemsInfo[index].itemTemplateId] = nil
            self:UpdateSelecteableInfo(false)
        end

    else
        --勾选
        self.curSelectableSequence[self.itemsInfo[index].itemTemplateId] = true

        --勾选1-5级鉴定天书，自动勾选鉴定天书
        if self.itemsInfo[index].type ~= EnumIdentifyWashItemType.JianDingTianShu and self.itemsInfo[index].type ~= EnumIdentifyWashItemType.TaiChuJingYuan then
            for i,v in pairs(self.itemsInfo) do
                if v.type == EnumIdentifyWashItemType.JianDingTianShu then
                    --self.itemsInfo[i].selectSprite.gameObject:SetActive(true)
                    self.curSelectableSequence[self.itemsInfo[i].itemTemplateId] = true
                end
            end
        end
        self:UpdateSelecteableInfo(true)
    end

    --self:RefreshData()

    LuaEquipIdentifySkillMgr:OnItemChooseWndSelectCallback(self.curSelectableSequence, self:GetCostItemType())
end

--ShowItemAccessInfo
function LuaEquipIdentifySkillItemChooseWnd:OnItemCellClick(go, data)

    if data.type == EnumIdentifyWashItemType.JianDingTianShu then
        if self.curSelectableSequence[data.itemTemplateId] then
            self.curSelectableSequence = {}
            self:UpdateSelecteableInfo(false)
            --self:RefreshData()
            LuaEquipIdentifySkillMgr:OnItemChooseWndSelectCallback(self.curSelectableSequence, self:GetCostItemType())
            return
        end
    end
    local ownNum = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, data.itemTemplateId)
    if ownNum < data.num then
        CItemAccessListMgr.Inst:ShowItemAccessInfo(data.itemTemplateId, false, go.transform, CTooltipAlignType.Right)
    end
end

--蒙黑
function LuaEquipIdentifySkillItemChooseWnd:ReDisableItemClick()
    if not self.isSelected then
        return
    end

    for i,v in pairs(self.itemsInfo) do
        v.disableSprite.enabled = false
    end

    for templateID,isSelected in pairs(self.curSelectableSequence) do
        local info = nil
        for i,v in pairs(self.itemsInfo) do
            if v.itemTemplateId == templateID then
                info = v
                break
            end
        end

        if info then
            local type = info.type
            for i,v in pairs(self.itemsInfo) do
                if type ~= v.type then
                    if type == EnumIdentifyWashItemType.TaiChuJingYuan then
                        if v.type ~= EnumIdentifyWashItemType.TaiChuJingYuan then
                            v.disableSprite.enabled = true
                        end
                    elseif type == EnumIdentifyWashItemType.JianDingTianShu then
                        if v.type == EnumIdentifyWashItemType.TaiChuJingYuan then
                            v.disableSprite.enabled = true
                        end
                    else
                        if v.type ~= EnumIdentifyWashItemType.JianDingTianShu then
                            v.disableSprite.enabled = true
                        end
                    end
                end
            end
        end
    end
end

--需要发送给服务端的EnumIdentifyWashItemType信息
function LuaEquipIdentifySkillItemChooseWnd:GetCostItemType()
    local type = nil
    for templateID,isSelected in pairs(self.curSelectableSequence) do
        local info = nil
        for i,v in pairs(self.itemsInfo) do
            if v.itemTemplateId == templateID then
                info = v
                break
            end
        end

        if type == nil then
            type = info.type
        elseif type == EnumIdentifyWashItemType.JianDingTianShu then
            type = info.type
        end
    end
    return type
end

function LuaEquipIdentifySkillItemChooseWnd:UpdateSelecteableInfo(isSelected)
    if _G.next(self.curSelectableSequence) == nil then
        isSelected = false
    end

    self:RefreshData()
    if isSelected then
        self:Close()
    end
end

function LuaEquipIdentifySkillItemChooseWnd:Close()
    self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

