local DelegateFactory  = import "DelegateFactory"
local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CUIFx = import "L10.UI.CUIFx"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local AlignType = import "L10.UI.CPopupMenuInfoMgr+AlignType"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local EShareType = import "L10.UI.EShareType"
local CChatMgr = import "L10.Game.CChatMgr"
local PlayerSettings = import "L10.Game.PlayerSettings"

LuaGetSectHongBaoWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaGetSectHongBaoWnd, "IconTexture", "IconTexture", CUITexture)
RegistChildComponent(LuaGetSectHongBaoWnd, "NumLabel", "NumLabel", UILabel)
RegistChildComponent(LuaGetSectHongBaoWnd, "InfoLabel", "InfoLabel", UILabel)
RegistChildComponent(LuaGetSectHongBaoWnd, "ReplayButton", "ReplayButton", GameObject)
RegistChildComponent(LuaGetSectHongBaoWnd, "ShareButton", "ShareButton", GameObject)
RegistChildComponent(LuaGetSectHongBaoWnd, "Fx", "Fx", CUIFx)
RegistChildComponent(LuaGetSectHongBaoWnd, "Fx2", "Fx2", CUIFx)
--@endregion RegistChildComponent end
RegistClassMember(LuaGetSectHongBaoWnd, "m_Tick")

function LuaGetSectHongBaoWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.ReplayButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnReplyButtonClick()
	end)


	
	UIEventListener.Get(self.ShareButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareButtonClick()
	end)


    --@endregion EventBind end
end

function LuaGetSectHongBaoWnd:Init()
	local itemId = LuaZongMenMgr.m_GetSectHongBaoResultData.itemId
	local name = LuaZongMenMgr.m_GetSectHongBaoResultData.ownerName
	local itemCount = LuaZongMenMgr.m_GetSectHongBaoResultData.itemCount
	local isBest = LuaZongMenMgr.m_GetSectHongBaoResultData.isBest
	local itemData = Item_Item.GetData(itemId)
	if itemData then
		self.InfoLabel.text = SafeStringFormat3(LocalString.GetString("%s的宗派福利"),name)
		self.IconTexture:LoadMaterial(itemData.Icon)
	end
	self.NumLabel.text = itemCount
	self.Fx:LoadFx("fx/ui/prefab/UI_chunjiehuodong_kaiqi.prefab")
	if isBest then
		self.Fx2:LoadFx("fx/ui/prefab/UI_caibingsha_meiwei.prefab")
	end
	if PlayerSettings.MusicEnabled then
		SoundManager.Inst:PlaySound(isBest and "event:/L10/L10_UI/UI_ZongPai_XingYun" or "event:/L10/L10_UI/UI_ZongPai_PuTong",Vector3.zero, 1, nil, 0)
	end
	self:CancelTick()
	self.m_Tick = RegisterTickOnce(function ()
		self.Fx:LoadFx("fx/ui/prefab/UI_chunjiehuodong_zhipian.prefab")
	end,1000)
end

function LuaGetSectHongBaoWnd:OnDisable()
	self:CancelTick()
end

function LuaGetSectHongBaoWnd:CancelTick()
	if self.m_Tick then
		UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
	end
end

--@region UIEvent

function LuaGetSectHongBaoWnd:OnReplyButtonClick()
	local t = {}
	local list  = {}
	local quickReplyList = SectItemRedPack_Setting.GetData().QuickReplyList
	for i = 0,quickReplyList.Length - 1 do
		local msg = quickReplyList[i]
		table.insert(list,g_MessageMgr:FormatMessage(msg))
	end
	for index,text in pairs(list) do
        local item = PopupMenuItemData(text,DelegateFactory.Action_int(function (idx)
            self:OnReplay(text)
        end),false,nil)
        table.insert(t, item)
    end
    local array=Table2Array(t,MakeArrayClass(PopupMenuItemData))
    CPopupMenuInfoMgr.ShowPopupMenu(array, self.ReplayButton.transform, AlignType.Top,1,DelegateFactory.Action(function()
    end),600,true,400)
end

function LuaGetSectHongBaoWnd:OnReplay(text)
	CChatMgr.Inst:SendChatMsg(CChatMgr.CHANNEL_NAME_SECT, text, false)
end

function LuaGetSectHongBaoWnd:OnShareButtonClick()
	CUICommonDef.CaptureScreen(
        "screenshot",
        true,
        false,
        nil,
        DelegateFactory.Action_string_bytes(
            function(fullPath, jpgBytes)
                ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, fullPath)
            end
        ),
        false
    )
end

--@endregion UIEvent

