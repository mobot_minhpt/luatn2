LuaDesireMirrorWnd = class()

RegistChildComponent(LuaDesireMirrorWnd, "m_OpenRankingListButton","OpenRankingListButton", GameObject)
RegistChildComponent(LuaDesireMirrorWnd, "m_CleanButton","CleanButton", GameObject)
RegistChildComponent(LuaDesireMirrorWnd, "m_ReadMeButton","ReadMeButton", GameObject)
RegistChildComponent(LuaDesireMirrorWnd, "m_TextLabel","TextLabel", UILabel)
RegistChildComponent(LuaDesireMirrorWnd, "m_Monster","Monster", CUITexture)
RegistChildComponent(LuaDesireMirrorWnd, "m_MirrorFx","MirrorFx", CUIFx)

RegistClassMember(LuaDesireMirrorWnd, "m_IsShowMonster")

function LuaDesireMirrorWnd:Init()
    self.m_Monster.gameObject:SetActive(false)
    local msg = g_MessageMgr:FormatMessage("DesireMirror_Words")
    self.m_TextLabel.text = msg
    UIEventListener.Get(self.m_OpenRankingListButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OpenRankingList()
    end)
    UIEventListener.Get(self.m_CleanButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:CleanMirror()
    end)
    UIEventListener.Get(self.m_ReadMeButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:ReadMe()
    end)
    Gac2Gas.YNZJQueryTransform(ZhongYuanJie2020Mgr.m_MirrorNPCEngineID)
end

function LuaDesireMirrorWnd:OnEnable()
    g_ScriptEvent:AddListener("DesireMirrorWnd_UpdateMirror",self,"OnMirrorUpdate")
end

function LuaDesireMirrorWnd:OnDisable()
    g_ScriptEvent:RemoveListener("DesireMirrorWnd_UpdateMirror",self,"OnMirrorUpdate")
end

function LuaDesireMirrorWnd:ReadMe()
    g_MessageMgr:ShowMessage("DesireMirror_ReadMe")
end

function LuaDesireMirrorWnd:OnMirrorUpdate(monsterId)
    local data = Monster_Monster.GetData(monsterId)
    self.m_Monster:LoadNPCPortrait(data.HeadIcon)
    self.m_Monster.gameObject:SetActive(true)
    self.m_IsShowMonster = true
end

function LuaDesireMirrorWnd:OpenRankingList()
    Gac2Gas.YNZJQueryRank()
end

function LuaDesireMirrorWnd:CleanMirror()
    if self.m_IsShowMonster then
        Gac2Gas.YNZJFindNpc(ZhongYuanJie2020Mgr.m_MirrorNPCEngineID)
        return
    end
    local msg = g_MessageMgr:FormatMessage("DesireMirror_PurificationConfirm")
    MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function()
        self.m_MirrorFx:LoadFx("Fx/UI/Prefab/UI_yunianzhijing_xianxing.prefab")
        Gac2Gas.YNZJFindNpc(ZhongYuanJie2020Mgr.m_MirrorNPCEngineID)
    end), nil ,LocalString.GetString("净化"), LocalString.GetString("取消"), false)
end
