require("common/common_include")
--require("design/GamePlay/JiaNianHua")

local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

CLuaCarnivalGiftView = class()
RegistClassMember(CLuaCarnivalGiftView, "m_Inited")
RegistClassMember(CLuaCarnivalGiftView, "m_GiftNameLabelTable")
RegistClassMember(CLuaCarnivalGiftView, "m_GiftItemTexTable")
RegistClassMember(CLuaCarnivalGiftView, "m_GiftOpenRootTable")
RegistClassMember(CLuaCarnivalGiftView, "m_GiftCloseRootTable")
RegistClassMember(CLuaCarnivalGiftView, "m_GiftOpenBuyBtnTable")
RegistClassMember(CLuaCarnivalGiftView, "m_GiftOpenBuyStatusLabelTable")
RegistClassMember(CLuaCarnivalGiftView, "m_GiftOpenBuyPriceLabelTable")
RegistClassMember(CLuaCarnivalGiftView, "m_GiftClosePriceLabelTable")
RegistClassMember(CLuaCarnivalGiftView, "m_GiftCloseLeftTimaLabelTable")
RegistClassMember(CLuaCarnivalGiftView, "m_GiftIdTable")

-- data member
RegistClassMember(CLuaCarnivalGiftView, "m_Tick")

function CLuaCarnivalGiftView:SyncCarnivalTickStock(status, onShelfTime, stockInfoUd)

end

function CLuaCarnivalGiftView:InitTick(onShelfTime)
	if onShelfTime <= 0 then
		return
	end
	local totalSeconds = onShelfTime - CServerTimeMgr.Inst.timeStamp
	if self.m_Tick then
		UnRegisterTick(self.m_Tick)
	end
	self:UpdateTime(onShelfTime)
	self.m_Tick = RegisterTickWithDuration(function ( ... )
		self:UpdateTime(onShelfTime)
	end, 1000, totalSeconds * 1000)
end

function CLuaCarnivalGiftView:UpdateTime(onShelfTime)
	local seconds = onShelfTime - CServerTimeMgr.Inst.timeStamp
	for k, v in pairs(self.m_GiftCloseLeftTimaLabelTable) do
		v.text = SafeStringFormat3("%02d:%02d:%02d", math.floor(seconds / 3600), math.floor(seconds % 3600 / 60), math.floor(seconds % 3600 % 60))
	end
	if seconds <= 0 then
		UnRegisterTick(self.m_Tick)
		Gac2Gas.QueryCarnivalTicketStock()
	end
end

function CLuaCarnivalGiftView:SyncBuyCarnivalTickDone(buyId)
	for i = 1, 3 do
		local id = self.m_GiftIdTable[i]
		if buyId == id then
			local canBuy = true
			local statusStr = LocalString.GetString("抢购")
			if CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp and CClientMainPlayer.Inst.PlayProp.CarnivalCollectData.BuyTicketId:GetBit(id) then
				canBuy = false
				statusStr = LocalString.GetString("已抢购")
			end
			self.m_GiftOpenBuyBtnTable[i].Enabled = canBuy
			self.m_GiftOpenBuyStatusLabelTable[i].text = statusStr
		end
	end
end

function CLuaCarnivalGiftView:Init()

end

function CLuaCarnivalGiftView:OnEnable( ... )
	g_ScriptEvent:AddListener("SyncCarnivalTickStock", self, "SyncCarnivalTickStock")
	g_ScriptEvent:AddListener("SyncBuyCarnivalTickDone", self, "SyncBuyCarnivalTickDone")
	self:Init()
	Gac2Gas.QueryCarnivalTicketStock()
end

function CLuaCarnivalGiftView:OnDisable( ... )
	g_ScriptEvent:RemoveListener("SyncCarnivalTickStock", self, "SyncCarnivalTickStock")
	g_ScriptEvent:RemoveListener("SyncBuyCarnivalTickDone", self, "SyncBuyCarnivalTickDone")

	if self.m_Tick then
		UnRegisterTick(self.m_Tick)
		self.m_Tick = nil
	end
end

return CLuaCarnivalGiftView
