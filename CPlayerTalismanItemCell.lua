-- Auto Generated!!
local Color = import "UnityEngine.Color"
local CPlayerTalismanItemCell = import "L10.UI.CPlayerTalismanItemCell"
local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"
local EquipmentTemplate_Type = import "L10.Game.EquipmentTemplate_Type"
local IdPartition = import "L10.Game.IdPartition"
CPlayerTalismanItemCell.m_Init_CS2LuaHook = function (this, pos, talisman) 

    this.position = pos
    local type = EquipmentTemplate_Type.GetData(pos)
    this.positionLabel.text = ""
    this.iconTexture:Clear()
    this.qualitySprite.color = Color.white
    if type ~= nil then
        this.positionLabel.text = type.Name
    end
    if talisman ~= nil and IdPartition.IdIsTalisman(talisman.TemplateId) then
        local template = EquipmentTemplate_Equip.GetData(talisman.TemplateId)
        this.iconTexture:LoadMaterial(template.Icon)
        this.qualitySprite.color = talisman.DisplayColor
        this.disableSprite.gameObject:SetActive(talisman.Disable > 0)
    else
        this.disableSprite.gameObject:SetActive(false)
    end
end
