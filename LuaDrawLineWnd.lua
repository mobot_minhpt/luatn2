local CMainCamera = import "L10.Engine.CMainCamera"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CUIManager = import "L10.UI.CUIManager"
local Color = import "UnityEngine.Color"
local UICamera = import "UICamera"
local Physics = import "UnityEngine.Physics"
local CUIFx = import "L10.UI.CUIFx"
local UITexture = import "UITexture"
local Extensions = import "Extensions"
local MessageWndManager = import "L10.UI.MessageWndManager"
local RenderTexture = import "UnityEngine.RenderTexture"
local Screen = import "UnityEngine.Screen"
local RenderTextureFormat = import "UnityEngine.RenderTextureFormat"
local RenderTextureReadWrite = import "UnityEngine.RenderTextureReadWrite"
local TextureWrapMode = import "UnityEngine.TextureWrapMode"
local FilterMode = import "UnityEngine.FilterMode"
local Camera = import "UnityEngine.Camera"
local GameObject = import "UnityEngine.GameObject"
local CameraClearFlags = import "UnityEngine.CameraClearFlags"
local LayerDefine = import "L10.Engine.LayerDefine"
local Quaternion = import "UnityEngine.Quaternion"
local Vector3 = import "UnityEngine.Vector3"
local CPreDrawMgr = import "L10.Engine.CPreDrawMgr"
local Object=import "System.Object"
local MsgPackImpl=import "MsgPackImpl"

LuaDrawLineWnd = class()

RegistChildComponent(LuaDrawLineWnd,"CloseBtn",GameObject)
RegistChildComponent(LuaDrawLineWnd,"ShowTexture",UITexture)
RegistChildComponent(LuaDrawLineWnd,"StarTemplate",GameObject)
RegistChildComponent(LuaDrawLineWnd,"PosNodeFather",GameObject)
RegistChildComponent(LuaDrawLineWnd,"DragMoveNode",GameObject)
RegistChildComponent(LuaDrawLineWnd,"TipTextNode",GameObject)
RegistChildComponent(LuaDrawLineWnd,"LineNodeFather",GameObject)
RegistChildComponent(LuaDrawLineWnd,"DrawFinishFxNode",GameObject)

local function split(str, pat)
   local t = {}  -- NOTE: use {n = 0} in Lua-5.0
   local fpat = "(.-)" .. pat
   local last_end = 1
   local s, e, cap = str:find(fpat, 1)
   while s do
      if s ~= 1 or cap ~= "" then
         table.insert(t,cap)
      end
      last_end = e+1
      s, e, cap = str:find(fpat, last_end)
   end
   if last_end <= #str then
      cap = str:sub(last_end)
      table.insert(t, cap)
   end
   return t
end

function LuaDrawLineWnd:SetFailed()
  self.DragMoveNode:SetActive(false)

	local lineTrans = self.LineNodeFather.transform
	for i = 1, lineTrans.childCount do
		local lineParent = lineTrans:GetChild(i - 1)
		if lineParent then
			lineParent.gameObject:SetActive(false)
		end
	end

	LuaWorldEventMgr.SaveFormerNode = nil

  self:InitStarNode()
end

function LuaDrawLineWnd:startDraw(node)
	self.DragMoveNode:SetActive(true)
	self.DragMoveNode.transform.position = UICamera.currentTouch.pos
	local _trans = self.DragMoveNode.transform
	local currentPos = UICamera.currentTouch.pos
	_trans.position = UICamera.currentCamera:ScreenToWorldPoint(Vector3(currentPos.x,currentPos.y,0))
  self:SetColor(node,Color.blue)
end

function LuaDrawLineWnd:CheckSucc()
	local lineTrans = self.LineNodeFather.transform
	for i = 1, lineTrans.childCount do
		local lineParent = lineTrans:GetChild(i - 1)
		if lineParent then
			if not lineParent.gameObject.activeSelf then
				return
			end
		end
	end

  self.DrawFinishFxNode:GetComponent(typeof(CUIFx)):LoadFx("fx/ui/prefab/UI_yibihua_hudie02.prefab")

  --Gac2Gas.FinishLianXingXingTask(CWorldEventMgr.Inst.xinxinTaskId)
  local empty = CreateFromClass(MakeGenericClass(List, Object))
  empty = MsgPackImpl.pack(empty)
  Gac2Gas.FinishClientTaskEvent(LuaWorldEventMgr.DrawLineTaskId, 'xingxinghudie',empty)

  RegisterTickWithDuration(function ()
    CUIManager.CloseUI(CLuaUIResources.DrawLineWnd)
  end,3000,3000)
end

function LuaDrawLineWnd:SetColor(node,m_color)
  -- node.transform:Find("node"):GetComponent(typeof(UISprite)).color = m_color
  -- node.transform:Find("node/Sprite"):GetComponent(typeof(UISprite)).color = m_color
end

function LuaDrawLineWnd:judgePass(node)
	if not LuaWorldEventMgr.SaveFormerNode then
		return
	end

	if node and node.transform.parent then
		local nodeNum = tonumber(node.transform.parent.name)
		if not nodeNum then
			return
		end
		if LuaWorldEventMgr.SaveFormerNode == nodeNum then
			return
		end
		local triggerLineNum = LuaWorldEventMgr.MatchFucntion(nodeNum,LuaWorldEventMgr.SaveFormerNode)
		if triggerLineNum > 0 then
      self:SetColor(node,Color.blue)
			local node = self.LineNodeFather.transform:Find(tostring(triggerLineNum))
			if node then
				if node.gameObject.activeSelf then
					self:SetFailed()
				else
					LuaWorldEventMgr.SaveFormerNode = nodeNum
					node.gameObject:SetActive(true)
					self:CheckSucc()
				end
			end
		else
			self:SetFailed()
		end
	end
end

function LuaDrawLineWnd:endDraw(node)
	self.DragMoveNode:SetActive(false)

	local lineTrans = self.LineNodeFather.transform
	for i = 1, lineTrans.childCount do
		local lineParent = lineTrans:GetChild(i - 1)
		if lineParent then
			lineParent.gameObject:SetActive(false)
		end
	end

  self:InitStarNode()
end

function LuaDrawLineWnd:setStarNode(dragNode,index)
	local onDrag = function(go,delta)
		local _trans = self.DragMoveNode.transform
		local currentPos = UICamera.currentTouch.pos
		_trans.position = UICamera.currentCamera:ScreenToWorldPoint(Vector3(currentPos.x,currentPos.y,0))

		local ray = UICamera.currentCamera:ScreenPointToRay(Vector3(currentPos.x,currentPos.y,0))
		local hits = Physics.RaycastAll(ray, 99999,  UICamera.currentCamera.cullingMask)

		for i =0,hits.Length - 1 do
			if hits[i].collider.gameObject.transform.parent then
				self:judgePass(hits[i].collider.gameObject)
			end
		end
	end

	local onPress = function(go,flag)
		if flag then
			local startNum = tonumber(go.transform.parent.name)
			if startNum then
				self:startDraw(go)
				LuaWorldEventMgr.SaveFormerNode = startNum
			end
		else
			self:endDraw(go)
		end
	end
	CommonDefs.AddOnDragListener(dragNode,DelegateFactory.Action_GameObject_Vector2(onDrag),false)
	CommonDefs.AddOnPressListener(dragNode,DelegateFactory.Action_GameObject_bool(onPress),false)
end


function LuaDrawLineWnd:GenerateStarNode(parentNodeTrans)
	local go = CUICommonDef.AddChild(parentNodeTrans.gameObject,self.StarTemplate)
	go:SetActive(true)
	self:setStarNode(go,parentNodeTrans.name)
end

function LuaDrawLineWnd:InitStarNode()
	local trans = self.PosNodeFather.transform
	for i =1, trans.childCount do
		local parent = trans:GetChild(i-1)
		if parent then
			Extensions.RemoveAllChildren(parent)
			self:GenerateStarNode(parent)
		end
	end


end

local cameraNode = nil

function LuaDrawLineWnd:Init()
	local onCloseClick = function(go)
    MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("CUSTOM_STRING2",LocalString.GetString("确认退出吗?")), DelegateFactory.Action(function ()
      CUIManager.CloseUI(CLuaUIResources.DrawLineWnd)
    end), nil, nil, nil, false)
	end
	CommonDefs.AddOnClickListener(self.CloseBtn,DelegateFactory.Action_GameObject(onCloseClick),false)

	self.StarTemplate:SetActive(false)
	self.DragMoveNode:SetActive(false)

  CPreDrawMgr.m_bEnableRectPreDraw = false

	if 1920 / Screen.width > 1080 / Screen.height then
		self.ShowTexture.width = 1920--Screen.width
		self.ShowTexture.height = Screen.height * 1920 / Screen.width--Screen.height
	else
		self.ShowTexture.width = Screen.width * 1080 / Screen.height--Screen.width
		self.ShowTexture.height = 1080--Screen.height
	end

	local renderTex = RenderTexture(self.ShowTexture.width,self.ShowTexture.height,32,RenderTextureFormat.ARGB32,RenderTextureReadWrite.Default)
	renderTex.wrapMode = TextureWrapMode.Clamp
	renderTex.filterMode = FilterMode.Bilinear
	renderTex.anisoLevel = 2
	self.ShowTexture.mainTexture = renderTex

	cameraNode = GameObject("__WorldEventDrawLineCamera__")
	local cameraScript = CommonDefs.AddCameraComponent(cameraNode)
    CMainCamera.Inst:SetCameraEnableStatus(false,"use_another_camera", false)
	cameraScript.clearFlags = CameraClearFlags.Skybox
  --cameraScript.backgroundColor = Color(49 / 255, 77 / 255, 121 / 255, 5 / 255)
  cameraScript.cullingMask = 2^LayerDefine.ModelForNGUI_3D + 2^LayerDefine.Effect_3D + 2^LayerDefine.Default + 2^LayerDefine.Water + 2^LayerDefine.WaterReflection + 2^LayerDefine.Terrain
  cameraScript.orthographic = false
  cameraScript.fieldOfView = 60
  cameraScript.farClipPlane = 140
  cameraScript.nearClipPlane = 0.1
  cameraScript.depth = -10
  cameraScript.transform.parent = CUIManager.instance.transform

	cameraNode.transform.position = Vector3(15,25,10)
  cameraNode.transform.rotation = Quaternion.Euler(-7.1,0,0)
  --cameraScript.transform.rotation = Quaternion.LookRotation(Vector3(selfData.kitePos[1] - selfData.playerPos[1],selfData.kitePos[2] - selfData.playerPos[2],selfData.kitePos[3] - selfData.playerPos[3]))

	cameraScript.targetTexture = renderTex
  self.DragMoveNode.transform:Find("FxNode"):GetComponent(typeof(CUIFx)):LoadFx("fx/ui/prefab/UI_yibihua01.prefab")
	--
	self:InitStarNode()
end

function LuaDrawLineWnd:OnDestroy()
    CMainCamera.Inst:SetCameraEnableStatus(true,"use_another_camera", false)
	CPreDrawMgr.m_bEnableRectPreDraw = true
	if cameraNode then
		GameObject.Destroy(cameraNode)
	end
end

return LuaDrawLineWnd
