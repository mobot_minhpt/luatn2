local CScene             = import "L10.Game.CScene"
local CMainCamera        = import "L10.Engine.CMainCamera"
local UIRoot             = import "UIRoot"
local Screen             = import "UnityEngine.Screen"
local CClientObjectMgr   = import "L10.Game.CClientObjectMgr"
local CClientOtherPlayer = import "L10.Game.CClientOtherPlayer"
local CClientMonster     = import "L10.Game.CClientMonster"
local StringBuilder      = import "System.Text.StringBuilder"

LuaDuanWu2023PVPVETopRightWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaDuanWu2023PVPVETopRightWnd, "expandButton", "ExpandButton", GameObject)
RegistChildComponent(LuaDuanWu2023PVPVETopRightWnd, "template", "Template", GameObject)
RegistChildComponent(LuaDuanWu2023PVPVETopRightWnd, "grid", "Grid", UIGrid)
RegistChildComponent(LuaDuanWu2023PVPVETopRightWnd, "bg", "Bg", UISprite)
RegistChildComponent(LuaDuanWu2023PVPVETopRightWnd, "time", "Time", UILabel)
RegistChildComponent(LuaDuanWu2023PVPVETopRightWnd, "myTeamScore", "MyTeamScore", UILabel)
RegistChildComponent(LuaDuanWu2023PVPVETopRightWnd, "otherTeamScore", "OtherTeamScore", UILabel)
RegistChildComponent(LuaDuanWu2023PVPVETopRightWnd, "overheadButtonTemplate", "OverheadButtonTemplate", GameObject)
RegistChildComponent(LuaDuanWu2023PVPVETopRightWnd, "overheadButtonsRoot", "OverheadButtonsRoot", GameObject)
RegistChildComponent(LuaDuanWu2023PVPVETopRightWnd, "bg1", "Bg1", UISprite)
RegistChildComponent(LuaDuanWu2023PVPVETopRightWnd, "myTeamAhead", "MyTeam", GameObject)
RegistChildComponent(LuaDuanWu2023PVPVETopRightWnd, "otherTeamAhead", "OtherTeam", GameObject)
--@endregion RegistChildComponent end

RegistClassMember(LuaDuanWu2023PVPVETopRightWnd, "engineIdDict")
RegistClassMember(LuaDuanWu2023PVPVETopRightWnd, "unusedGos")
RegistClassMember(LuaDuanWu2023PVPVETopRightWnd, "lastTime")

function LuaDuanWu2023PVPVETopRightWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	UIEventListener.Get(self.expandButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnExpandButtonClick()
	end)
    --@endregion EventBind end

	self.template:SetActive(false)
	self.overheadButtonTemplate:SetActive(false)
end

function LuaDuanWu2023PVPVETopRightWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncDuanWu2023PVPVEScore", self, "UpdateTeamInfo")
	g_ScriptEvent:AddListener("HideTopAndRightTipWnd",self,"OnHideTopAndRightTipWnd")
	g_ScriptEvent:AddListener("SceneRemainTimeUpdate", self, "UpdateTime")
end

function LuaDuanWu2023PVPVETopRightWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncDuanWu2023PVPVEScore", self, "UpdateTeamInfo")
	g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd",self,"OnHideTopAndRightTipWnd")
	g_ScriptEvent:RemoveListener("SceneRemainTimeUpdate", self, "UpdateTime")
end

function LuaDuanWu2023PVPVETopRightWnd:OnHideTopAndRightTipWnd()
	self.expandButton.transform.localEulerAngles = Vector3(0, 0, -180)
end

function LuaDuanWu2023PVPVETopRightWnd:UpdateTime()
    local mainScene = CScene.MainScene
    if mainScene then
        if mainScene.ShowTimeCountDown then
            self.time.text = self:GetRemainTimeText(mainScene.ShowTime)
        else
            self.time.text = ""
        end
    else
        self.time.text = ""
    end
end

function LuaDuanWu2023PVPVETopRightWnd:GetRemainTimeText(totalSeconds)
	if totalSeconds < 0 then
        totalSeconds = 0
    end
    if totalSeconds >= 3600 then
		return SafeStringFormat3("[ACF9FF]%d:%02d:%02d[-]", math.floor(totalSeconds / 3600), math.floor((totalSeconds % 3600) / 60), totalSeconds % 60)
    else
		return SafeStringFormat3("[ACF9FF]%02d:%02d[-]", math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end

function LuaDuanWu2023PVPVETopRightWnd:Init()
	self:UpdateTime()
	self:UpdateTeamInfo(0)
	self.engineIdDict = {}
	self.unusedGos = {}
end

function LuaDuanWu2023PVPVETopRightWnd:UpdateTeamInfo(scoreStatus)
	local info = LuaDuanWu2023Mgr.pvpveInfo
	if not info then
		self.myTeamScore.text = ""
		self.otherTeamScore.text = ""
	else
		self.myTeamScore.text = info.teamId == 1 and info.team1Score or info.team2Score
		self.otherTeamScore.text = info.teamId == 1 and info.team2Score or info.team1Score
	end

	local teamInfoList = info and info.teamInfoList or {}
	self:AddChild(self.grid.gameObject, self.template, #teamInfoList)
	local greenColor = NGUIText.ParseColor24("00FF60", 0)
	for i, data in ipairs(teamInfoList) do
		local child = self.grid.transform:GetChild(i - 1)

		local sb = NewStringBuilderWraper(StringBuilder, tostring(data.name))
		local str = sb.Length > 5 and SafeStringFormat3("%s...", sb:ToString(0, 4)) or sb:ToString()

		local name = child:Find("Name"):GetComponent(typeof(UILabel))
		local score = child:Find("Score"):GetComponent(typeof(UILabel))
		local contribution = child:Find("Contribution"):GetComponent(typeof(UILabel))

		name.text = str
		score.text = data.score
		contribution.text = data.contribution

		local mainPlayer = CClientMainPlayer.Inst
		local isMe = mainPlayer and mainPlayer.Id == data.playerId or false
		name.color = isMe and greenColor or Color.white
		score.color = isMe and greenColor or Color.white
		contribution.color = isMe and greenColor or Color.white
		child:Find("Label1"):GetComponent(typeof(UILabel)).color = isMe and greenColor or Color.white
		child:Find("Label2"):GetComponent(typeof(UILabel)).color = isMe and greenColor or Color.white
	end
	self.grid:Reposition()
	self.bg.height = #teamInfoList * 42 + 13
	self.bg1.height = #teamInfoList * 42 + 13
	self.time:ResetAndUpdateAnchors()

	self.myTeamAhead:SetActive(false)
	self.otherTeamAhead:SetActive(false)
	self.myTeamAhead:SetActive(scoreStatus == 1)
	self.otherTeamAhead:SetActive(scoreStatus == 2)
end

function LuaDuanWu2023PVPVETopRightWnd:AddChild(parent, template, num)
    local childCount = parent.transform.childCount
    if childCount < num then
        for i = childCount + 1, num do
            NGUITools.AddChild(parent, template)
        end
    end

    childCount = parent.transform.childCount
    for i = 0, childCount - 1 do
        parent.transform:GetChild(i).gameObject:SetActive(i < num)
    end
end


function LuaDuanWu2023PVPVETopRightWnd:Update()
	if not LuaDuanWu2023Mgr.pvpveInfo then return end

	local setting = Duanwu2023_PVPVESetting.GetData()
	if not self.lastTime or (Time.realtimeSinceStartup - self.lastTime) > 0.5 then
		self.lastTime = Time.realtimeSinceStartup
		local myTeamPlayerIds = LuaDuanWu2023Mgr.pvpveInfo.myTeamPlayerIds
		local otherBaseMonsterId = setting.BaseMonsterIds[2 - LuaDuanWu2023Mgr.pvpveInfo.teamId]
		CClientObjectMgr.Inst:ForeachObject(DelegateFactory.Action_CClientObject(function(obj)
			local engineId = obj.EngineId
			if TypeIs(obj, typeof(CClientOtherPlayer)) then
				local playerId = obj.PlayerId
				if obj and not myTeamPlayerIds[playerId] and not self.engineIdDict[engineId] then
					self.engineIdDict[engineId] = {
						go = self:AddOverheadButton(1, function()
							Gac2Gas.DuanWu2023PVPVECompareScore(playerId)
						end),
						cachedPos = Vector3.zero,
					}
				end
			elseif TypeIs(obj, typeof(CClientMonster)) then
				if obj and obj.TemplateId == otherBaseMonsterId and not self.engineIdDict[engineId] then
					self.engineIdDict[engineId] = {
						go = self:AddOverheadButton(2, function()
							Gac2Gas.DuanWu2023PVPVEBreakBase()
						end),
						cachedPos = Vector3.zero,
					}
				end
			end
		end))
	end

	for engineId, _ in pairs(self.engineIdDict) do
		local obj = CClientObjectMgr.Inst:GetObject(engineId)
		if not obj then
			local go = self.engineIdDict[engineId].go
			go.transform.position = Vector3.zero
			go:SetActive(false)
			table.insert(self.unusedGos, go)
			self.engineIdDict[engineId] = nil
		else
			local isOk = false
			if obj.Visible and obj.RO and not CClientMainPlayer.Inst.RO:GetFxById(setting.BuffFxId[1]) then
				if (TypeIs(obj, typeof(CClientOtherPlayer)) and not obj.RO:GetFxById(setting.BuffFxId[1])) then
					if self:IsDistanceOk(obj, setting.CompareDistance) then
						isOk = true
					end
				elseif (TypeIs(obj, typeof(CClientMonster)) and not obj.RO:GetFxById(setting.BuffFxId[0])) then
					if self:IsDistanceOk(obj, setting.BreakDistance) then
						isOk = true
					end
				end
			end
			if isOk then
				self:Layout(engineId, obj)
			else
				self.engineIdDict[engineId].go:SetActive(false)
			end
		end
	end
end

function LuaDuanWu2023PVPVETopRightWnd:AddOverheadButton(buttonType, func)
	local go
	if #self.unusedGos > 0 then
		go = table.remove(self.unusedGos)
	else
		go = NGUITools.AddChild(self.overheadButtonsRoot, self.overheadButtonTemplate)
	end
	go.transform:Find("Button/bi").gameObject:SetActive(buttonType == 1)
	go.transform:Find("Button/po").gameObject:SetActive(buttonType == 2)
	UIEventListener.Get(go.transform:Find("Button").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
		func()
	end)
	return go
end

function LuaDuanWu2023PVPVETopRightWnd:IsDistanceOk(obj, distance)
	local pos1 = Utility.PixelPos2GridPos(CClientMainPlayer.Inst.Pos)
	local pos2 = Utility.PixelPos2GridPos(obj.Pos)
	local dx = pos1.x - pos2.x
	local dy = pos1.y - pos2.y
	return dx * dx + dy * dy <= distance * distance
end

function LuaDuanWu2023PVPVETopRightWnd:Layout(engineId, obj)
	local go = self.engineIdDict[engineId].go
	go:SetActive(true)

	local target = obj.RO:GetSlotTransform("TopAnchor", false)
    local screenPos = CMainCamera.Main:WorldToScreenPoint(target.position)
    screenPos.z = 0
    local nguiWorldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(screenPos)
    if self.engineIdDict[engineId].cachedPos == nguiWorldPos then return end

    self.engineIdDict[engineId].cachedPos = nguiWorldPos
    go.transform.position = nguiWorldPos

    local localPos = go.transform.localPosition
    local bounds = NGUIMath.CalculateRelativeWidgetBounds(go.transform)
    local centerX = localPos.x
    local centerY = localPos.y

    local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject)
    local virtualScreenWidth = Screen.width * CUIManager.UIMainCamera.rect.width * scale
    local virtualScreenHeight = Screen.height * scale

    -- 左右不超出屏幕范围
    if centerX - bounds.size.x * 0.5 < -virtualScreenWidth * 0.5 then
        centerX = -virtualScreenWidth * 0.5 + bounds.size.x * 0.5
    end
    if centerX + bounds.size.x * 0.5 > virtualScreenWidth * 0.5 then
        centerX = virtualScreenWidth * 0.5 - bounds.size.x * 0.5
    end

    -- 上下不超出屏幕范围
    if centerY + bounds.size.y * 0.5 > virtualScreenHeight * 0.5 then
        centerY = virtualScreenHeight * 0.5 - bounds.size.y * 0.5
    end
    if centerY - bounds.size.y * 0.5 < -virtualScreenHeight * 0.5 then
        centerY = -virtualScreenHeight * 0.5 + bounds.size.y * 0.5
    end

    go.transform.localPosition = Vector3(centerX, centerY, 0)
end

function LuaDuanWu2023PVPVETopRightWnd:OnDestroy()
	LuaDuanWu2023Mgr.pvpveInfo = nil
end

--@region UIEvent

function LuaDuanWu2023PVPVETopRightWnd:OnExpandButtonClick()
	self.expandButton.transform.localEulerAngles = Vector3(0, 0, 0)
	CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end

--@endregion UIEvent
