local QnSelectableButton = import "L10.UI.QnSelectableButton"
local NGUITools = import "NGUITools"
local IdPartition = import "L10.Game.IdPartition"
local Extensions = import "Extensions"
local EnumTalismanTabWnd = import "L10.UI.EnumTalismanTabWnd"
local EnumQualityType = import "L10.Game.EnumQualityType"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CTalismanWndMgr = import "L10.UI.CTalismanWndMgr"
local CTalismanBaptizeListTemplate = import "L10.UI.CTalismanBaptizeListTemplate"
local CItemMgr = import "L10.Game.CItemMgr"
local CItemInfo = import "L10.Game.CItemInfo"
local CEquipmentProcessMgr = import "L10.Game.CEquipmentProcessMgr"
local CUIRestrictScrollView=import "L10.UI.CUIRestrictScrollView"

CLuaTalismanBaptizeItemList = class()
RegistClassMember(CLuaTalismanBaptizeItemList,"scrollView")
RegistClassMember(CLuaTalismanBaptizeItemList,"table")
RegistClassMember(CLuaTalismanBaptizeItemList,"talismanTemplate")
RegistClassMember(CLuaTalismanBaptizeItemList,"tips")
RegistClassMember(CLuaTalismanBaptizeItemList,"talismanList")
RegistClassMember(CLuaTalismanBaptizeItemList,"talismanGoList")
RegistClassMember(CLuaTalismanBaptizeItemList,"OnTalismanSelect")
RegistClassMember(CLuaTalismanBaptizeItemList,"curSelectIndex")
RegistClassMember(CLuaTalismanBaptizeItemList,"curTab")
RegistClassMember(CLuaTalismanBaptizeItemList,"baptizeBaseWord")

function CLuaTalismanBaptizeItemList:Awake()
    self.scrollView = self.transform:Find("ScrollView"):GetComponent(typeof(CUIRestrictScrollView))
    self.table = self.transform:Find("ScrollView/Table"):GetComponent(typeof(UIGrid))
    self.talismanTemplate = self.transform:Find("ScrollView/TalismanTemplate").gameObject
    self.tips = self.transform:Find("Tips").gameObject
    self.talismanList = {}
    self.talismanGoList = {}
    self.OnTalismanSelect = nil
    self.curSelectIndex = -1
    self.curTab = nil
    self.baptizeBaseWord = true

end

function CLuaTalismanBaptizeItemList:Init( tab, baptizeBaseWord) 

    self.tips:SetActive(false)
    self.baptizeBaseWord = baptizeBaseWord
    self.curTab = tab
    Extensions.RemoveAllChildren(self.table.transform)
    self.talismanTemplate:SetActive(false)
    self.talismanList = {}
    self.talismanGoList={}

    local equipListOnBody = CItemMgr.Inst:GetPlayerEquipmentListAtPlace(EnumItemPlace.Body)
    CommonDefs.ListSort1(equipListOnBody, typeof(CItemInfo), CEquipmentProcessMgr.Inst.colorComparison)
    local equipListInBag = CItemMgr.Inst:GetPlayerEquipmentListAtPlace(EnumItemPlace.Bag)
    CommonDefs.ListSort1(equipListInBag, typeof(CItemInfo), CEquipmentProcessMgr.Inst.colorComparison)

    local itemInfo = CItemMgr.Inst:GetItemInfo(CTalismanWndMgr.selectBaptizeTalismanId)
    local item = CItemMgr.Inst:GetById(CTalismanWndMgr.selectBaptizeTalismanId)
    if itemInfo ~= nil and item ~= nil then
        if tab == EnumTalismanTabWnd.inlay then
            if item.Equip.Color ~= EnumQualityType.Orange then
                table.insert( self.talismanList,itemInfo )
            end
        elseif tab == EnumTalismanTabWnd.baptize and baptizeBaseWord and not item.Equip.IsNormalTalisman then
            table.insert( self.talismanList,itemInfo )
        elseif tab == EnumTalismanTabWnd.baptize and not baptizeBaseWord and item.Equip.Color ~= EnumQualityType.Orange then
            table.insert( self.talismanList,itemInfo )
        elseif tab == EnumTalismanTabWnd.upgrade and item.Equip.IsFairyTalisman and Talisman_XianJia.GetData(itemInfo.templateId).Grade ~= 4 and not item.OnBody then
            table.insert( self.talismanList,itemInfo )
        end
    end
    do
        local i = 0
        while i < equipListOnBody.Count do
            if IdPartition.IdIsTalisman(equipListOnBody[i].templateId) and CTalismanWndMgr.selectBaptizeTalismanId ~= equipListOnBody[i].itemId then
                local talisman = CItemMgr.Inst:GetById(equipListOnBody[i].itemId)
                if tab == EnumTalismanTabWnd.baptize then
                    if baptizeBaseWord and not talisman.Equip.IsNormalTalisman then
                        table.insert( self.talismanList,equipListOnBody[i] )
                    elseif not baptizeBaseWord and talisman.Equip.Color ~= EnumQualityType.Orange then
                        table.insert( self.talismanList,equipListOnBody[i] )
                    end
                elseif tab == EnumTalismanTabWnd.inlay then
                    if talisman.Equip.Color ~= EnumQualityType.Orange then
                        table.insert( self.talismanList,equipListOnBody[i] )
                    end
                end
            end
            i = i + 1
        end
    end
    do
        local i = 0
        while i < equipListInBag.Count do
            if IdPartition.IdIsTalisman(equipListInBag[i].templateId) and CTalismanWndMgr.selectBaptizeTalismanId ~= equipListInBag[i].itemId then
                local talisman = CItemMgr.Inst:GetById(equipListInBag[i].itemId)
                if tab == EnumTalismanTabWnd.baptize then
                    if baptizeBaseWord and not talisman.Equip.IsNormalTalisman then
                        table.insert( self.talismanList,equipListInBag[i] )
                    elseif not baptizeBaseWord and talisman.Equip.Color ~= EnumQualityType.Orange then
                        table.insert( self.talismanList,equipListInBag[i] )
                    end
                elseif tab == EnumTalismanTabWnd.upgrade and talisman.Equip.IsFairyTalisman then
                    local xianjia = Talisman_XianJia.GetData(talisman.TemplateId)
                    local nextXianjia = Talisman_XianJia.GetData(talisman.TemplateId + 1)
                    if xianjia ~= nil and nextXianjia ~= nil and xianjia.Position == nextXianjia.Position and xianjia.Grade + 1 == nextXianjia.Grade then
                        table.insert( self.talismanList,equipListInBag[i] )
                    end
                elseif tab == EnumTalismanTabWnd.inlay then
                    if talisman.Equip.Color ~= EnumQualityType.Orange then
                        table.insert( self.talismanList,equipListInBag[i] )
                    end
                end
            end
            i = i + 1
        end
    end
    do
        local i = 0
        while i < #self.talismanList do
            local instance = NGUITools.AddChild(self.table.gameObject, self.talismanTemplate)
            instance:SetActive(true)
            table.insert( self.talismanGoList, instance)
            UIEventListener.Get(instance).onClick = DelegateFactory.VoidDelegate(function(go)
                self:OnTalismanClick(go)
            end)
            local template = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CTalismanBaptizeListTemplate))
            template:Init(self.talismanList[i+1].itemId)
            template.onIconClick = DelegateFactory.Action_GameObject(function(go)
                self:OnTalismanClick(go)
            end)
            i = i + 1
        end
    end
    if #self.talismanList <= 0 then
        self.tips:SetActive(true)
    elseif self.curSelectIndex < 0 then
        self.curSelectIndex = 0
    end
    self.table:Reposition()

    self.scrollView:ResetPosition()

    local selectedItem = nil
    if self.curSelectIndex >= 0 and self.curSelectIndex < #self.talismanGoList then
        selectedItem = self.talismanGoList[self.curSelectIndex+1]
        self:OnTalismanClick(selectedItem)
    elseif #self.talismanGoList > 0 then
        self.curSelectIndex = #self.talismanGoList - 1
        selectedItem = self.talismanGoList[#self.talismanGoList - 1+1]
        self:OnTalismanClick(selectedItem)
    else
        self:OnTalismanClick(nil)
    end

    --重新加载之后 设置一下
    if selectedItem then
        CUICommonDef.SetFullyVisible(selectedItem, self.scrollView)
    end

end
function CLuaTalismanBaptizeItemList:OnTalismanClick( go) 
    if go == nil then
        self.curSelectIndex = - 1
        if self.OnTalismanSelect then
            self.OnTalismanSelect(nil)
        end
        return
    end
    do
        local i = 0
        while i < #self.talismanGoList do
            CommonDefs.GetComponent_GameObject_Type(self.talismanGoList[i+1], typeof(QnSelectableButton)):SetSelected(self.talismanGoList[i+1] == go, false)
            if go == self.talismanGoList[i+1] and self.OnTalismanSelect then
                self.OnTalismanSelect(self.talismanList[i+1])
                self.curSelectIndex = i
            end
            i = i + 1
        end
    end
end


function CLuaTalismanBaptizeItemList:OnEnable()
    g_ScriptEvent:AddListener("SendItem",self,"OnSendItem")
    g_ScriptEvent:AddListener("SetItemAt",self,"OnSetItemAt")
end
function CLuaTalismanBaptizeItemList:OnDisable()
    g_ScriptEvent:RemoveListener("SendItem",self,"OnSendItem")
    g_ScriptEvent:RemoveListener("SetItemAt",self,"OnSetItemAt")
end
function CLuaTalismanBaptizeItemList:OnSendItem(args)
    --根据盛放的指导调整一下这两的逻辑
    local itemId = args[0]
    for i=1,#self.talismanList do
        if itemId == self.talismanList[i] then
            local instance = self.talismanGoList[i]
            local template = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CTalismanBaptizeListTemplate))
            template:Init(itemId)
        end
    end
end
function CLuaTalismanBaptizeItemList:OnSetItemAt(args)
    --根据盛放的指导调整一下这两的逻辑
    local place, pos, oldItemId, newItemId = args[0],args[1],args[2],args[3]

    local update = false
    if oldItemId and oldItemId~="" then
        local commonItem = CItemMgr.Inst:GetById(oldItemId)
        if not commonItem then return end
        if commonItem.IsEquip then
            update=true
        end
    end
    if newItemId and newItemId~="" then
        local commonItem = CItemMgr.Inst:GetById(newItemId)
        if not commonItem then return end
        if commonItem.IsEquip then
            update=true
        end
    end
    if update then
        self:Init(self.curTab,self.baptizeBaseWord)
    end
end
