
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UIInput = import "UIInput"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local CCommonDanMuCtrl = import "L10.UI.CCommonDanMuCtrl"
local CVoiceMgr = import "L10.Game.CVoiceMgr"
local PlayerSettings = import "L10.Game.PlayerSettings"
local CChatHelper = import "L10.UI.CChatHelper"
local EChatPanel = import "L10.Game.EChatPanel"


LuaCCDanmuWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaCCDanmuWnd, "SendButton", "SendButton", GameObject)
RegistChildComponent(LuaCCDanmuWnd, "ChatInput", "ChatInput", UIInput)
RegistChildComponent(LuaCCDanmuWnd, "CloseButton", "CloseButton", GameObject)
RegistChildComponent(LuaCCDanmuWnd, "Setting", "Setting", QnSelectableButton)
RegistChildComponent(LuaCCDanmuWnd, "BottomLeft", "BottomLeft", GameObject)
RegistChildComponent(LuaCCDanmuWnd, "ZhanKaiBtn", "ZhanKaiBtn", GameObject)
RegistChildComponent(LuaCCDanmuWnd, "BulletScreen", "BulletScreen", CCommonDanMuCtrl)
RegistChildComponent(LuaCCDanmuWnd, "ZhanKai", "ZhanKai", GameObject)

--@endregion RegistChildComponent end

function LuaCCDanmuWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.SendButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSendButtonClick()
	end)

	UIEventListener.Get(self.CloseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCloseButtonClick()
	end)

	self.Setting.OnButtonSelected = DelegateFactory.Action_bool(function (selected)
	    self:OnSettingSelected(selected)
	end)

	UIEventListener.Get(self.ZhanKaiBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnZhanKaiBtnClick()
	end)

    --@endregion EventBind end
end

function LuaCCDanmuWnd:Init()
	self.Setting:SetSelected(true,false)
end

function LuaCCDanmuWnd:OnRecvMsgFromCC(args)
	local msg = args[0]
	if CVoiceMgr.Inst:IsVoice(msg) then
		return --弹幕跳过语音消息
	end
	local danmuType = PlayerSettings.CCLiveDanMuType
	if danmuType == 0 then
		self.BulletScreen:AddBullet(msg, true)
	elseif danmuType == 1 then
		self.BulletScreen:AddBullet(msg, false)
	else
		--discard
	end
end

function LuaCCDanmuWnd:OnEnable()
	g_ScriptEvent:AddListener("OnRecvMsgFromCC", self, "OnRecvMsgFromCC")
end

function LuaCCDanmuWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnRecvMsgFromCC", self, "OnRecvMsgFromCC")
end

--@region UIEvent

function LuaCCDanmuWnd:OnSendButtonClick()
	local val = self.ChatInput.value
	if val == nil or val == "" then 
		return
	end
	CChatHelper.SendMsg(EChatPanel.CC, val, 0)
	self.ChatInput.value = ""
end

function LuaCCDanmuWnd:OnCloseButtonClick()
	self.ZhanKai:SetActive(false)
	self.BottomLeft:SetActive(true)
end

function LuaCCDanmuWnd:OnSettingSelected(selected)
	self.BulletScreen.gameObject:SetActive(selected)
end

function LuaCCDanmuWnd:OnZhanKaiBtnClick()
	self.ZhanKai:SetActive(true)
	self.BottomLeft:SetActive(false)
end

--@endregion UIEvent

