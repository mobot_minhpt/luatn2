

local QnSelectableButton=import "L10.UI.QnSelectableButton"

CLuaEquipWordBaptizeChooseWordWnd = class()
RegistClassMember(CLuaEquipWordBaptizeChooseWordWnd,"m_ButtonDataList")
function CLuaEquipWordBaptizeChooseWordWnd:Awake()
    local titleLabel = self.transform:Find("Wnd_Bg_Secondary_3/TitleLabel"):GetComponent(typeof(UILabel))
    if CLuaEquipWordBaptizeChooseWordWnd.s_OptionIndex==1 then
        titleLabel.text = LocalString.GetString("条件一")
    else
        titleLabel.text = LocalString.GetString("条件二")
    end
end

CLuaEquipWordBaptizeChooseWordWnd.s_OptionIndex = 1
function CLuaEquipWordBaptizeChooseWordWnd:Init()
    local list = LuaAutoBaptizeNecessaryConditionMgr.m_canGetWordCondsList
    self.m_ButtonDataList = {}
    local canInsert = true
    for key,value in pairs(list) do
        table.insert(self.m_ButtonDataList, value)
        canInsert = true   
    end

    table.sort(self.m_ButtonDataList,function(a,b) return a.Priority > b.Priority end)

    
    local item=self.transform:Find("Item").gameObject
    item:SetActive(false)

    local options = CLuaEquipXiLianMgr.m_OtherOptions[CLuaEquipWordBaptizeChooseWordWnd.s_OptionIndex]
    local grid = self.transform:Find("ScrollView/Grid").gameObject
    for i,data in ipairs(self.m_ButtonDataList) do
        local go = NGUITools.AddChild(grid,item)
        go:SetActive(true)
        local label = go.transform:Find("DescLabel"):GetComponent(typeof(UILabel))
        label.text = data.WordDescription
        local cmp = go:GetComponent(typeof(QnSelectableButton))
        cmp:SetSelected(options[data.ID],false)
    end


    local button = self.transform:Find("Button").gameObject
    UIEventListener.Get(button).onClick = DelegateFactory.VoidDelegate(function(go)

        local options = {}
        local grid = self.transform:Find("ScrollView/Grid").gameObject
        local parent = grid.transform
        for i=1,parent.childCount do
            local child = parent:GetChild(i-1)
            local cmp = child:GetComponent(typeof(QnSelectableButton))
            if cmp.m_IsSelected then
                options[self.m_ButtonDataList[i].ID] = true
            end
        end
        if CLuaEquipWordBaptizeChooseWordWnd.s_OptionIndex==1 then
            CLuaEquipXiLianMgr.m_OtherOptions[1] = options
        else
            CLuaEquipXiLianMgr.m_OtherOptions[2] = options
        end

        CUIManager.CloseUI(CLuaUIResources.EquipWordBaptizeChooseWordWnd)
	    g_ScriptEvent:BroadcastInLua("UpdateEquipWordBaptizeOption",CLuaEquipWordBaptizeChooseWordWnd.s_OptionIndex)
    end)
end