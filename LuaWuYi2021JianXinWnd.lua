

local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CButton = import "L10.UI.CButton"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"

LuaWuYi2021JianXinWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaWuYi2021JianXinWnd, "Tip", "Tip", GameObject)
RegistChildComponent(LuaWuYi2021JianXinWnd, "Items", "Items", GameObject)
RegistChildComponent(LuaWuYi2021JianXinWnd, "MainTip", "MainTip", GameObject)
RegistChildComponent(LuaWuYi2021JianXinWnd, "MainNotEnough", "MainNotEnough", GameObject)
RegistChildComponent(LuaWuYi2021JianXinWnd, "MainEnough", "MainEnough", GameObject)
RegistChildComponent(LuaWuYi2021JianXinWnd, "MainSuccess", "MainSuccess", GameObject)
RegistChildComponent(LuaWuYi2021JianXinWnd, "CompoundBtn", "CompoundBtn", CButton)
RegistChildComponent(LuaWuYi2021JianXinWnd, "TipTitle", "TipTitle", UILabel)
RegistChildComponent(LuaWuYi2021JianXinWnd, "Content", "Content", UILabel)
RegistChildComponent(LuaWuYi2021JianXinWnd, "TipNotAcquire", "TipNotAcquire", GameObject)
RegistChildComponent(LuaWuYi2021JianXinWnd, "TipAcquire", "TipAcquire", GameObject)
RegistChildComponent(LuaWuYi2021JianXinWnd, "Bg", "Bg", GameObject)
RegistChildComponent(LuaWuYi2021JianXinWnd, "SuiPianNumber", "SuiPianNumber", UILabel)
RegistChildComponent(LuaWuYi2021JianXinWnd, "Suipian", "Suipian", GameObject)
RegistChildComponent(LuaWuYi2021JianXinWnd, "TipBtn", "TipBtn", GameObject)
RegistChildComponent(LuaWuYi2021JianXinWnd, "RedDotBtn", "RedDotBtn", GameObject)
RegistChildComponent(LuaWuYi2021JianXinWnd, "NeedCount", "NeedCount", UILabel)
RegistChildComponent(LuaWuYi2021JianXinWnd, "ItemNotAcquire", "ItemNotAcquire", GameObject)
RegistChildComponent(LuaWuYi2021JianXinWnd, "ItemAcquire", "ItemAcquire", GameObject)
RegistChildComponent(LuaWuYi2021JianXinWnd, "FullNotAcquire", "FullNotAcquire", GameObject)
RegistChildComponent(LuaWuYi2021JianXinWnd, "FullAcquire", "FullAcquire", GameObject)
RegistChildComponent(LuaWuYi2021JianXinWnd, "FullReward", "FullReward", GameObject)
RegistChildComponent(LuaWuYi2021JianXinWnd, "ItemReward", "ItemReward", GameObject)
RegistChildComponent(LuaWuYi2021JianXinWnd, "OnlyFullReward", "OnlyFullReward", GameObject)
RegistChildComponent(LuaWuYi2021JianXinWnd, "WarmingLabel", "WarmingLabel", GameObject)
RegistChildComponent(LuaWuYi2021JianXinWnd, "FullRewardFx", "FullRewardFx", GameObject)

--@endregion RegistChildComponent end

RegistClassMember(LuaWuYi2021JianXinWnd, "m_Icons")
RegistClassMember(LuaWuYi2021JianXinWnd, "m_Titles")
RegistClassMember(LuaWuYi2021JianXinWnd, "m_SuccessWarns")
RegistClassMember(LuaWuYi2021JianXinWnd, "m_RedDots")
RegistClassMember(LuaWuYi2021JianXinWnd, "m_Highlights")
RegistClassMember(LuaWuYi2021JianXinWnd, "m_Fxs")
-- 数据
RegistClassMember(LuaWuYi2021JianXinWnd, "m_KeyWorlds")
RegistClassMember(LuaWuYi2021JianXinWnd, "m_Status")
RegistClassMember(LuaWuYi2021JianXinWnd, "m_IsFullRewarded")
RegistClassMember(LuaWuYi2021JianXinWnd, "m_MindPieceCount")
RegistClassMember(LuaWuYi2021JianXinWnd, "m_CurrentKey")
RegistClassMember(LuaWuYi2021JianXinWnd, "m_FxInit")

function LuaWuYi2021JianXinWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.CompoundBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCompoundBtnClick()
	end)


	UIEventListener.Get(self.Bg.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBgClick()
	end)

	UIEventListener.Get(self.TipBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipBtnClick()
	end)

    --@endregion EventBind end
end

function LuaWuYi2021JianXinWnd:Init()
    Gac2Gas.RequestWuYi2021FindMindData()
    self.m_CurrentKey = nil
    -- 初始化全置为0
    self.m_KeyWorlds = {}
    self.m_Status = {}
    self.m_MindPieceCount = 0
    self.m_FxInit = false
    WuYi2021_WuJianWuXin.ForeachKey(function(key)
        table.insert(self.m_KeyWorlds, key)
        self.m_Status[key] = 0
    end)

    self.SuiPianNumber.gameObject:SetActive(true)
    self:InitComponents()

    local itemReward = 0
    CommonDefs.DictIterate(WuYi2021_WuJianWuXin.GetData(self.m_KeyWorlds[1]).ItemRewards, 
        DelegateFactory.Action_object_object(function (k, v)
            itemReward = tonumber(k)
    end))

    local fullReward = 0
    CommonDefs.DictIterate(WuYi2021_Setting.GetData().MindCollectRewardItem, 
        DelegateFactory.Action_object_object(function (k, v)
            fullReward = tonumber(k)
    end))

    if itemReward~=0 then
        UIEventListener.Get(self.ItemReward).onClick = DelegateFactory.VoidDelegate(function()
            CItemInfoMgr.ShowLinkItemTemplateInfo(itemReward, false, nil, AlignType.Default, 0, 0, 0, 0)
        end)
        local itemRewardTexture = self.ItemReward.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
        local itemData = Item_Item.GetData(itemReward)
        if itemRewardTexture and itemData then
            itemRewardTexture:LoadMaterial(itemData.Icon)
        end
    end

    if fullReward~=0 then
        UIEventListener.Get(self.FullReward).onClick = DelegateFactory.VoidDelegate(function()
            CItemInfoMgr.ShowLinkItemTemplateInfo(fullReward, false, nil, AlignType.Default, 0, 0, 0, 0)
        end)
        local fullRewardTexture = self.FullReward.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
        local itemData = Item_Item.GetData(fullReward)
        if fullRewardTexture and itemData then
            fullRewardTexture:LoadMaterial(itemData.Icon)
        end

        UIEventListener.Get(self.OnlyFullReward).onClick = DelegateFactory.VoidDelegate(function()
            CItemInfoMgr.ShowLinkItemTemplateInfo(fullReward, false, nil, AlignType.Default, 0, 0, 0, 0)
        end)
        local onlyFullRewardTexture = self.OnlyFullReward.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
        if onlyFullRewardTexture and itemData then
            onlyFullRewardTexture:LoadMaterial(itemData.Icon)
        end
    end

    -- 显示主界面
    self:ShowMainTip()
end

function LuaWuYi2021JianXinWnd:InitComponents()
    self.m_Icons = {}
    self.m_Titles = {}
    self.m_SuccessWarns = {}
    self.m_RedDots = {}
    self.m_Highlights = {}
    self.m_Fxs = {}

    for i = 1, 6 do
        local key = self.m_KeyWorlds[i]
        local data = WuYi2021_WuJianWuXin.GetData(key)

        self.m_Icons[i] = self.transform:Find("Anchor/Items/" .. i .. "/Icon"):GetComponent(typeof(UITexture))
        self.m_Titles[i] = self.transform:Find("Anchor/Items/" .. i .. "/Title"):GetComponent(typeof(UILabel))
        self.m_SuccessWarns[i] = self.transform:Find("Anchor/Items/" .. i .. "/SuccessWarn").gameObject
        self.m_RedDots[i] = self.transform:Find("Anchor/Items/" .. i .. "/RedDot").gameObject
        self.m_Highlights[i] = self.transform:Find("Anchor/Items/" .. i .. "/highlight").gameObject
        self.m_Fxs[i] = self.transform:Find("Anchor/Items/" .. i .. "/Fx"):GetComponent(typeof(CUIFx))

        self.NeedCount.text = tostring(data.CostCount)
        self.m_Titles[i].text = data.Name
        self.m_Highlights[i]:SetActive(false)
        self.m_RedDots[i]:SetActive(false)

        UIEventListener.Get(self.m_Icons[i].gameObject).onClick = DelegateFactory.VoidDelegate(function()
            self:OnItemIconClick(i)
        end)
    end
end

function LuaWuYi2021JianXinWnd:InitState(mindPieceCount, isFullRewarded, list)
    self.m_IsFullRewarded = isFullRewarded
    self.m_MindPieceCount = mindPieceCount
    self.SuiPianNumber.text = tostring(mindPieceCount)
    for i = 0, list.Count - 1 do
        -- 已经获取到的id为1
        self.m_Status[list[i]] = 1
    end

    local currentCount = mindPieceCount
    for i=1, #self.m_KeyWorlds do
        local key = self.m_KeyWorlds[i]

        CUICommonDef.SetActive(self.m_Icons[i].gameObject, self.m_Status[key] == 1, false)
        -- 当前被选中的
        if self.m_Status[key] == 1 and self.m_Highlights[i].activeSelf then
            self.m_Titles[i].gameObject:SetActive(false)
            self.m_SuccessWarns[i]:SetActive(true)
            self.m_Fxs[i]:DestroyFx()
            self.m_Fxs[i]:LoadFx("fx/ui/prefab/UI_dianlianggezi_01.prefab")
            self.RedDotBtn:SetActive(false)
            self.TipBtn:SetActive(false)
        end

        self.m_RedDots[i]:SetActive(false)

        if self.m_Status[key] == 0 then
            currentCount = currentCount - 5
            if currentCount >=0 then
                self.m_RedDots[i]:SetActive(true)
            end
        end
    end
    self.FullRewardFx:SetActive(self.m_IsFullRewarded)
    self:RefreshMainTip()
end

function LuaWuYi2021JianXinWnd:RefreshRedDot()
    local currentCount = self.m_MindPieceCount
    for i=1, #self.m_KeyWorlds do
        local key = self.m_KeyWorlds[i]
        local count = WuYi2021_WuJianWuXin.GetData(key).CostCount
        self.m_RedDots[i]:SetActive(false)

        if self.m_Status[key] == 0 then
            currentCount = currentCount - count
            if currentCount >= 0 then
                self.m_RedDots[i]:SetActive(true)
            end
        end
    end
end

function LuaWuYi2021JianXinWnd:OnItemIconClick(index)
    self.Suipian:SetActive(true)
    self:ClearItemState()
    self.MainTip:SetActive(false)
    self.Tip:SetActive(true)

    local key = self.m_KeyWorlds[index]
    local data = WuYi2021_WuJianWuXin.GetData(key)
    if data~=nil then
        self.TipTitle.text = data.Name
        self.Content.text = "          " .. data.Desc
    end
    self.m_CurrentKey = key

    local isCollected = self.m_Status[key] == 1
    self.TipBtn:SetActive(not isCollected)
    self.m_SuccessWarns[index]:SetActive(isCollected)
    self.m_Titles[index].gameObject:SetActive(false)
    self.m_Highlights[index]:SetActive(true)

    if self.m_RedDots[index].activeSelf then
        self.RedDotBtn:SetActive(true)
        self.m_RedDots[index]:SetActive(false)
    else
        self.RedDotBtn:SetActive(false)
    end

    self.FullRewardFx:SetActive(false)
end

function LuaWuYi2021JianXinWnd:OnItemBtnClick()
    local key = self.m_CurrentKey
    local data = WuYi2021_WuJianWuXin.GetData(key)
    -- 已经获取了
    if self.m_Status[key] == 1 then
        self:ShowMainTip()
        return
    end

    if self.m_MindPieceCount and self.m_MindPieceCount >= data.CostCount then
        Gac2Gas.WuYi2021FindSingleMind(key)
        Gac2Gas.RequestWuYi2021FindMindData()
    else
        g_MessageMgr:ShowMessage("WUYI2021_MIND_PIECE_NOT_ENOUGH")
    end
end

function LuaWuYi2021JianXinWnd:ClearItemState()
    self:RefreshRedDot()
    self.TipBtn:SetActive(false)
    for i=1,6 do
        self.m_SuccessWarns[i]:SetActive(false)
        self.m_Titles[i].gameObject:SetActive(true)
        self.m_Highlights[i]:SetActive(false)
    end
end

function LuaWuYi2021JianXinWnd:ShowMainTip()
    self:ClearItemState()
    self.MainTip:SetActive(true)
    self.Tip:SetActive(false)
    self:RefreshMainTip()
end

function LuaWuYi2021JianXinWnd:RefreshMainTip()
    local isAllCollect = true
    for i=1, #self.m_KeyWorlds do
        if self.m_Status[self.m_KeyWorlds[i]] == 0 then
            isAllCollect = false
        end
    end

    self.FullReward:SetActive(not isAllCollect)
    self.ItemReward:SetActive(not isAllCollect)
    self.WarmingLabel:SetActive(not isAllCollect)
    self.OnlyFullReward:SetActive(isAllCollect)

    self.ItemNotAcquire:SetActive(not isAllCollect)
    self.ItemAcquire:SetActive(isAllCollect)
    self.FullAcquire:SetActive(self.m_IsFullRewarded)
    self.FullNotAcquire:SetActive(not self.m_IsFullRewarded)

    self.MainNotEnough:SetActive(not self.m_IsFullRewarded and not isAllCollect)
    self.MainEnough:SetActive(not self.m_IsFullRewarded and isAllCollect)
    self.MainSuccess:SetActive(self.m_IsFullRewarded)
    self.CompoundBtn.gameObject:SetActive(not self.m_IsFullRewarded and isAllCollect)
    self.Suipian:SetActive(not self.m_IsFullRewarded and not isAllCollect)
end

function LuaWuYi2021JianXinWnd:OnEnable()
	g_ScriptEvent:AddListener("SyncWuYi2021FindMindData", self, "InitState")
end

function LuaWuYi2021JianXinWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SyncWuYi2021FindMindData", self, "InitState")
end

--@region UIEvent

function LuaWuYi2021JianXinWnd:OnCompoundBtnClick()
    Gac2Gas.WuYi2021FindAllMind()
    Gac2Gas.RequestWuYi2021FindMindData()
end


function LuaWuYi2021JianXinWnd:OnBgClick()
    self:ShowMainTip()
end


function LuaWuYi2021JianXinWnd:OnTipBtnClick()
    self:OnItemBtnClick()
end


--@endregion UIEvent

