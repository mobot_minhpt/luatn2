require("common/common_include")

local CUIResources = import "L10.UI.CUIResources"
local UILabel = import "UILabel"
local UISprite = import "UISprite"
local GameObject  = import "UnityEngine.GameObject"
local CTopAndRightTipWnd=import "L10.UI.CTopAndRightTipWnd"

CLuaBaoWeiNanGuaTopRightWnd = class()

RegistChildComponent(CLuaBaoWeiNanGuaTopRightWnd, "rightBtn", GameObject)
RegistChildComponent(CLuaBaoWeiNanGuaTopRightWnd, "node1", GameObject)
RegistChildComponent(CLuaBaoWeiNanGuaTopRightWnd, "node2", GameObject)
RegistChildComponent(CLuaBaoWeiNanGuaTopRightWnd, "node3", GameObject)
RegistChildComponent(CLuaBaoWeiNanGuaTopRightWnd, "node4", GameObject)
RegistChildComponent(CLuaBaoWeiNanGuaTopRightWnd, "nangua", GameObject)
RegistChildComponent(CLuaBaoWeiNanGuaTopRightWnd, "percentLabel", UILabel)
RegistChildComponent(CLuaBaoWeiNanGuaTopRightWnd, "tipBtn", GameObject)
RegistChildComponent(CLuaBaoWeiNanGuaTopRightWnd, "tipPanel", GameObject)
RegistChildComponent(CLuaBaoWeiNanGuaTopRightWnd, "tipNode1", GameObject)
RegistChildComponent(CLuaBaoWeiNanGuaTopRightWnd, "tipNode2", GameObject)
RegistChildComponent(CLuaBaoWeiNanGuaTopRightWnd, "tipNode3", GameObject)
RegistChildComponent(CLuaBaoWeiNanGuaTopRightWnd, "tipNode4", GameObject)
RegistChildComponent(CLuaBaoWeiNanGuaTopRightWnd, "tipCloseBtn", GameObject)
RegistClassMember(CLuaBaoWeiNanGuaTopRightWnd, "savePercent")
RegistClassMember(CLuaBaoWeiNanGuaTopRightWnd, "m_Tick")

function CLuaBaoWeiNanGuaTopRightWnd:Init()
    UIEventListener.Get(self.rightBtn).onClick=LuaUtils.VoidDelegate(function(go)
        LuaUtils.SetLocalRotation(self.rightBtn.transform,0,0,0)
        CTopAndRightTipWnd.showPackage = false
        CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
    end)

    UIEventListener.Get(self.tipBtn).onClick=LuaUtils.VoidDelegate(function(go)
        self:ShowTipPanel()
    end)

    UIEventListener.Get(self.tipCloseBtn).onClick=LuaUtils.VoidDelegate(function(go)
        self:CloseTipPanel()
    end)

    self.tipPanel:SetActive(false)
    self.tipNode2:SetActive(false)
    self.tipNode2:SetActive(false)
    self.tipNode3:SetActive(false)
    self.tipNode4:SetActive(false)
    LuaBaoWeiNanGuaMgr.eventTable = nil
    LuaBaoWeiNanGuaMgr.percent = 100
    self:UpdateInfo()
end

function CLuaBaoWeiNanGuaTopRightWnd:CloseTipPanel()
  self.tipPanel:SetActive(false)
end

function CLuaBaoWeiNanGuaTopRightWnd:ShowTipPanel()
  if not LuaBaoWeiNanGuaMgr.eventTable then
    return
  end

  self.tipPanel:SetActive(true)

  local nodeTable = {self.tipNode1,self.tipNode2,self.tipNode3,self.tipNode4}
  local count = 0
  for i,v in pairs(nodeTable) do
    if LuaBaoWeiNanGuaMgr.eventTable and LuaBaoWeiNanGuaMgr.eventTable[i] then
      v:SetActive(true)
      v.transform.localPosition = Vector3(0,-40 - count * 55,0)
      count = count + 1
    else
      v:SetActive(false)
    end
  end

  if count == 0 then
    self.tipPanel:SetActive(false)
  end

  self.tipPanel.transform:Find("node"):GetComponent(typeof(UISprite)).height = 250 - 55* 4 + 55 *count
end

function CLuaBaoWeiNanGuaTopRightWnd:UpdateInfo()
  local percent = LuaBaoWeiNanGuaMgr.percent / 100

  if percent < 0.3 then
    self.nangua.transform:Find("alert").gameObject:SetActive(true)
  elseif self.savePercent and percent < self.savePercent then
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
    end
    self.nangua.transform:Find("alert").gameObject:SetActive(true)
    self.m_Tick = RegisterTickWithDuration(function ()
      self.nangua.transform:Find("alert").gameObject:SetActive(false)
    end,1000,1000)
  else
    self.nangua.transform:Find("alert").gameObject:SetActive(false)
  end

  self.nangua.transform:Find("sprite"):GetComponent(typeof(UITexture)).fillAmount = percent
  self.nangua.transform:Find("percentLabel"):GetComponent(typeof(UILabel)).text = LuaBaoWeiNanGuaMgr.percent .. '%'
  local nodeTable = {self.node1,self.node2,self.node3,self.node4}
  for i,v in pairs(nodeTable) do
    if LuaBaoWeiNanGuaMgr.eventTable and LuaBaoWeiNanGuaMgr.eventTable[i] then
      v.transform:Find("node").gameObject:SetActive(true)
    else
      v.transform:Find("node").gameObject:SetActive(false)
    end
  end

  self.savePercent = percent
end

function CLuaBaoWeiNanGuaTopRightWnd:OnEnable()
	g_ScriptEvent:AddListener("OnBaoWeiNanGuaSyncPlayInfo", self, "UpdateInfo")
end

function CLuaBaoWeiNanGuaTopRightWnd:OnDisable()
  g_ScriptEvent:RemoveListener("OnBaoWeiNanGuaSyncPlayInfo", self, "UpdateInfo")
end

function CLuaBaoWeiNanGuaTopRightWnd:OnDestroy()
  if self.m_Tick then
      UnRegisterTick(self.m_Tick)
  end
end

return CLuaBaoWeiNanGuaTopRightWnd
