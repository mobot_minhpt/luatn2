local Object=import "UnityEngine.Object"
local UISlider=import "UISlider"
local CItemInfoMgr=import "L10.UI.CItemInfoMgr"
local AlignType=import "L10.UI.CItemInfoMgr+AlignType"

CLuaNewYear2019CunQianGuanWnd = class()
RegistClassMember(CLuaNewYear2019CunQianGuanWnd, "m_ScoreLabel")
RegistClassMember(CLuaNewYear2019CunQianGuanWnd, "m_SaveButton")
RegistClassMember(CLuaNewYear2019CunQianGuanWnd, "m_Sliders")
RegistClassMember(CLuaNewYear2019CunQianGuanWnd, "m_Awards")
RegistClassMember(CLuaNewYear2019CunQianGuanWnd, "m_Coin")
RegistClassMember(CLuaNewYear2019CunQianGuanWnd, "m_Ticks")

function CLuaNewYear2019CunQianGuanWnd:Init()
    self.m_Ticks = {}
    self.m_Coin = FindChild(self.transform,"Coin").gameObject
    self.m_Coin:SetActive(false)

    self.m_ScoreLabel=FindChild(self.transform,"ScoreLabel"):GetComponent(typeof(UILabel))
    self.m_SaveButton=FindChild(self.transform,"SaveButton").gameObject
    UIEventListener.Get(self.m_SaveButton).onClick = DelegateFactory.VoidDelegate(function(go)
        Gac2Gas.MoveAllPigCoinToPot()
    end)
    self.m_Sliders={}
    local tf=FindChild(self.transform,"Sliders")
    for i=1,6 do
        self.m_Sliders[i]=tf:GetChild(i-1):GetComponent(typeof(UISlider))
    end

    self.m_Awards = {}
    local tf=FindChild(self.transform,"Awards")
    for i=1,6 do
        self.m_Awards[i]=tf:GetChild(i-1).gameObject
    end
    if CClientMainPlayer.Inst then
        self:InitProgress(CClientMainPlayer.Inst.PlayProp.PigSavePotCount)
    end

    g_MessageMgr:ShowMessage("NewYear2019_CunQianGuan_Tip")

    -- self:DoCoinDropAni(12)
end
function CLuaNewYear2019CunQianGuanWnd:IsPigRewardAlreadyReceived(idx)
    if not CClientMainPlayer.Inst then return false end
    local v = CClientMainPlayer.Inst.PlayProp.PigRewardGetRecord
	return bit.band(v, bit.lshift(1, idx-1))>0
end
function CLuaNewYear2019CunQianGuanWnd:OnEnable()
	g_ScriptEvent:AddListener("SyncPigCoinParams", self, "OnSyncPigCoinParams")

end

function CLuaNewYear2019CunQianGuanWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SyncPigCoinParams", self, "OnSyncPigCoinParams")

end

function CLuaNewYear2019CunQianGuanWnd:OnClickReward(go)
    for i,v in ipairs(self.m_Awards) do
        if v==go then
            Gac2Gas.ApplyPigCoinPotReward(i)
            break
        end
    end
end

function CLuaNewYear2019CunQianGuanWnd:OnClickTip(go)
    local ids = {
        21020267,
        21020268,
        21020269,
        21020270,
        21020271,
        21020282}

    for i,v in ipairs(self.m_Awards) do
        if v==go then
            CItemInfoMgr.ShowLinkItemTemplateInfo(ids[i],false,nil,AlignType.Default,0,0,0,0)
            break
        end
    end
end
function CLuaNewYear2019CunQianGuanWnd:InitProgress(count)

    for i,v in ipairs(self.m_Sliders) do
        v.value=0
    end

    local counts={0,2,8,24,40,56,88}
    for i,v in ipairs(counts) do
        if count>= v then
            if self.m_Sliders[i] then
                local val=(count-v)/(counts[i+1]-v)
                self.m_Sliders[i].value=val
            end
        end
    end

    self.m_ScoreLabel.text=tostring(count)

    local count = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp.PigSavePotCount or 0
    for i=1,6 do
        -- self.m_Awards[i]=tf:GetChild(i-1).gameObject
        local fx = self.m_Awards[i].transform:GetChild(0).gameObject
        if count>= counts[i+1] then
            UIEventListener.Get(self.m_Awards[i]).onClick = DelegateFactory.VoidDelegate(function(go)
                self:OnClickReward(go)
            end)
            
            local received = self:IsPigRewardAlreadyReceived(i)
            fx:SetActive(not received)
        else
            UIEventListener.Get(self.m_Awards[i]).onClick = DelegateFactory.VoidDelegate(function(go)
                self:OnClickTip(go)
            end)
            
            fx:SetActive(false)
        end
    end
end
function CLuaNewYear2019CunQianGuanWnd:OnSyncPigCoinParams(count,addCount)
    self:InitProgress(count)
    self:DoCoinDropAni(addCount)
end

function CLuaNewYear2019CunQianGuanWnd:DoCoinDropAni(addCount)
    for k,v in pairs(self.m_Ticks) do
        UnRegisterTick(v)
    end
    if addCount>0 then
        local delay = 0
        local parent = FindChild(self.transform,"Coins").gameObject
        for i=1,addCount do
            local go = NGUITools.AddChild(parent,self.m_Coin)
            LuaUtils.SetLocalPositionY(go.transform,430)
            if delay>0 then
                self.m_Ticks[i] = RegisterTickOnce(function()
                    if go then
                        go:SetActive(true)
                        LuaTweenUtils.TweenPositionY(go.transform,100,0.5)
                        Object.Destroy(go,0.5)
                    end
                    self.m_Ticks[i] = nil
                end,delay)
            else
                go:SetActive(true)
                LuaTweenUtils.TweenPositionY(go.transform,100,0.5)
                Object.Destroy(go,0.5)
            end
            delay = delay+250
        end
    end
end
function CLuaNewYear2019CunQianGuanWnd:OnDestroy()
    for k,v in pairs(self.m_Ticks) do
        UnRegisterTick(v)
    end
    self.m_Ticks = {}
end

