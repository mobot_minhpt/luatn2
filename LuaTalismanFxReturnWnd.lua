local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local DelegateFactory = import "DelegateFactory"
local QnTableView = import "L10.UI.QnTableView"
local CUITexture = import "L10.UI.CUITexture"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local QnModelPreviewer = import "L10.UI.QnModelPreviewer"
local CButton = import "L10.UI.CButton"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local CChatLinkMgr = import "CChatLinkMgr"
local UILabel = import "UILabel"
local UIVerticalLabel = import "UIVerticalLabel"
local CUICommonDef = import "L10.UI.CUICommonDef"
local NGUIMath = import "NGUIMath"
local Item_Item = import "L10.Game.Item_Item"

CLuaTalismanFxReturnWnd = class()

CLuaTalismanFxReturnWnd.s_TalismanFxId = 0

RegistChildComponent(CLuaTalismanFxReturnWnd, "TableView", QnTableView)
RegistChildComponent(CLuaTalismanFxReturnWnd, "ModelPreviewer", QnModelPreviewer)
RegistChildComponent(CLuaTalismanFxReturnWnd, "ItemName", UILabel)
RegistChildComponent(CLuaTalismanFxReturnWnd, "ExchangeButton", CButton)
RegistChildComponent(CLuaTalismanFxReturnWnd, "ExchangeLabel", UILabel)
RegistChildComponent(CLuaTalismanFxReturnWnd, "Cost", Transform)
RegistChildComponent(CLuaTalismanFxReturnWnd, "ItemCost", Transform)
RegistChildComponent(CLuaTalismanFxReturnWnd, "ContributionCost", CCurentMoneyCtrl)
RegistChildComponent(CLuaTalismanFxReturnWnd, "HouseResCost", UILabel)
RegistChildComponent(CLuaTalismanFxReturnWnd, "ItemDesc", UIVerticalLabel)
RegistChildComponent(CLuaTalismanFxReturnWnd, "talismanTab", GameObject)
RegistChildComponent(CLuaTalismanFxReturnWnd, "YueLiCost", CCurentMoneyCtrl)
RegistChildComponent(CLuaTalismanFxReturnWnd, "chargeNode", GameObject)
RegistChildComponent(CLuaTalismanFxReturnWnd, "returnNode", GameObject)
RegistChildComponent(CLuaTalismanFxReturnWnd, "notOpenNode", GameObject)
RegistChildComponent(CLuaTalismanFxReturnWnd, "YueliCost1", GameObject)
RegistChildComponent(CLuaTalismanFxReturnWnd, "YueliCost2", GameObject)

RegistClassMember(CLuaTalismanFxReturnWnd, "m_ItemTable")
RegistClassMember(CLuaTalismanFxReturnWnd, "m_SelectIndex")
RegistClassMember(CLuaTalismanFxReturnWnd, "m_SelectFxId")
RegistClassMember(CLuaTalismanFxReturnWnd, "m_ItemEnough")

function CLuaTalismanFxReturnWnd:Awake()
	--self.m_SelectFxId = CLuaTalismanFxReturnWnd.s_TalismanFxId

	self.m_RealSelectFxId = CLuaTalismanFxReturnWnd.s_TalismanFxId
	local data = ItemGet_TalismanFX.GetData(self.m_RealSelectFxId)
	if data then
		local dType = data.TalismanID % 100

		ItemGet_TalismanFX.Foreach(function (key, data)
			if data.TalismanID == dType  then
				self.m_SelectFxId = key
			end
		end)
	end

	self.talismanTab:SetActive(false)
end


function CLuaTalismanFxReturnWnd:Init()
	self.m_ItemTable = {}

	local itemTbl = {}
	local gotItemTbl = {}
	ItemGet_TalismanFX.Foreach(function(k, v)
		if v.TalismanID < 100 then
			local got = LuaTalismanMgr:CheckContain(v.TalismanID)
			local fxOrderDesign = Talisman_FXOrder.GetData(k)
			local fxOrder = fxOrderDesign and fxOrderDesign.Order or 0
			if got then
				table.insert(gotItemTbl, {id = k, got = true, order = fxOrder})
			else
				--table.insert(itemTbl, {id = k, got = false, order = fxOrder})
			end
		end
	end)

	table.sort(itemTbl, function(a, b) return a.order < b.order end)
	table.sort(gotItemTbl, function(a, b) return a.order < b.order end)
	for _, item in ipairs(itemTbl) do
		table.insert(self.m_ItemTable, item)
	end
	for _, item in ipairs(gotItemTbl) do
		table.insert(self.m_ItemTable, item)
	end

	self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
	function() return #self.m_ItemTable end,
	function(item, row) self:InitItem(item, row) end
)
self.TableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
	self.m_SelectIndex = row + 1
	self.m_SelectFxId = self.m_ItemTable[row + 1].id
	self:InitDetails()
end)

if self.m_SelectFxId and self.m_SelectFxId > 0 then
	for i = 1, #self.m_ItemTable do
		if self.m_ItemTable[i].id == self.m_SelectFxId then
			self.m_SelectIndex = i
			self.TableView:SetSelectRow(i - 1, true)
			break
		end
	end
else
	self.m_SelectIndex = 1
	self.TableView:SetSelectRow(0, true)
end
self.TableView:ReloadData(false, false)
end

function CLuaTalismanFxReturnWnd:InitItem(item, row)
	local tf = item.transform

	local id = self.m_ItemTable[row+1].id
	local got = self.m_ItemTable[row+1].got
	local data = ItemGet_TalismanFX.GetData(id)

	-- icon
	local iconTex = tf:Find("Icon"):GetComponent(typeof(CUITexture))
	iconTex:LoadMaterial(data.Icon)

	-- lock
	local lockGo = tf:Find("Icon/LockMask").gameObject
	lockGo:SetActive(not got)

	-- name
	local nameLabel = tf:Find("NameLabel"):GetComponent(typeof(UILabel))
	nameLabel.text = CChatLinkMgr.TranslateToNGUIText(data.Name, false)

	-- stage
	local stageLabel = tf:Find("StageLabel"):GetComponent(typeof(UILabel))
	stageLabel.text = SafeStringFormat3(LocalString.GetString("%s阶仙家"), data.Stage)
end

function CLuaTalismanFxReturnWnd:CheckItemState(data)
	local level = math.floor(data.TalismanID / 100)
	local tType = data.TalismanID % 100
	--if level <= 0 then
	--	return 1
	--else
		local nextId = (level+1) * 100 + tType
		local got = LuaTalismanMgr:CheckContain(nextId)
		if got then
			return 2
		else
			return 3
		end
	--end
end

function CLuaTalismanFxReturnWnd:GetFormerTalismanLevel(id)
	local data = ItemGet_TalismanFX.GetData(id)
  local tId = data.TalismanID
  local level = math.floor(tId / 100)
  if level > 0 then
    local ftId = (level-1) * 100 + tId % 100
    local txData = Talisman_XianJiaAppearanceLevel.GetData(ftId)
    if txData then
      return txData.Fx
    end
  end
end

function CLuaTalismanFxReturnWnd:InitSingleDetails(selectItem)
	self.Cost.gameObject:SetActive(false)

	--local selectItem = self.m_ItemTable[self.m_SelectIndex]
	local data = ItemGet_TalismanFX.GetData(selectItem.id)
	self.ModelPreviewer:PreviewMainPlayer(
	CClientMainPlayer.Inst.AppearanceProp.HideGemFxToOtherPlayer,
	CClientMainPlayer.Inst.AppearanceProp.IntesifySuitId,
	CClientMainPlayer.Inst.AppearanceProp.Wing,
	CClientMainPlayer.Inst.AppearanceProp.HideWing,
	data.TalismanID,
	0, 180, nil)

	local level = math.ceil(data.TalismanID/100)
	local name = SafeStringFormat3(LocalString.GetString('%s·%s级'),data.Name,level)
	local nameLength = string.len(name)
	local height = 12 * nameLength + 126
	self.ItemName.transform:Find('BackSprite'):GetComponent(typeof(UISprite)).width = height

	self.ItemName.text = CUICommonDef.GetVerticalLayoutText(name, false)
	self.ItemDesc.fontColor = NGUIMath.HexToColor(0x6b8eb3ff)
	self.ItemDesc.text = data.Desc

	if selectItem.got then
    local state = self:CheckItemState(data)
    if state == 1 then
			self.chargeNode:SetActive(false)
			self.returnNode:SetActive(false)
			self.notOpenNode:SetActive(true)
			local showString = LocalString.GetString('基础等级，不能回退')
			self.notOpenNode.transform:Find('Cost/Label'):GetComponent(typeof(UILabel)).text = showString
    elseif state == 2 then
			self.chargeNode:SetActive(false)
			self.returnNode:SetActive(false)
			self.notOpenNode:SetActive(true)
			local showString = SafeStringFormat3(LocalString.GetString('需逐级回退，请先回退[c][FFE345]%s·%s级[-][/c]外观'),data.Name,level+1)
			self.notOpenNode.transform:Find('Cost/Label'):GetComponent(typeof(UILabel)).text = showString
    elseif state == 3 then -- can return
			self.chargeNode:SetActive(false)
			self.returnNode:SetActive(true)
			self.notOpenNode:SetActive(false)

      local stringTable = {}
      if data.ItemReturn then
        local costItemId = data.ItemReturn[0]
        local costItemCount = data.ItemReturn[1]
        local item = Item_Item.GetData(costItemId)
        if item then
          local showString = SafeStringFormat3(LocalString.GetString('[c][FFE345]%s[-][/c]个%s'),costItemCount,item.Name)--g_MessageMgr:FormatMessage('TalismanUnlockTip')
          table.insert(stringTable,showString)
        end
      end

      if data.ContributionItem > 0 then
        local item = Item_Item.GetData(data.ContributionItem)
        if item then
          local showString = SafeStringFormat3(LocalString.GetString('[c][FFE345]%s[-][/c]'),item.Name)--g_MessageMgr:FormatMessage('TalismanUnlockTip')
          table.insert(stringTable,showString)
        end
      end
      if data.HouseResItem > 0 then
        local item = Item_Item.GetData(data.HouseResItem)
        if item then
          local showString = SafeStringFormat3(LocalString.GetString('[c][FFE345]%s[-][/c]'),item.Name)--g_MessageMgr:FormatMessage('TalismanUnlockTip')
          table.insert(stringTable,showString)
        end
      end
      if data.DouHunTrainScoreReturn > 0 then
        local showString = SafeStringFormat3(LocalString.GetString('[c][FFE345]%s[-][/c]斗魂培养永久积分'),data.DouHunTrainScoreReturn)--g_MessageMgr:FormatMessage('TalismanUnlockTip')
        table.insert(stringTable,showString)
      end
      if data.YueLiReturn > 0 then
        local showString = SafeStringFormat3(LocalString.GetString('[c][FFE345]%s[-][/c]斗魂成就点'),data.YueLiReturn)--g_MessageMgr:FormatMessage('TalismanUnlockTip')
        table.insert(stringTable,showString)
      end
      local totalString = LocalString.GetString('返还')
      for i,v in pairs(stringTable) do
        if i == #stringTable then
          totalString = totalString .. v
        else
          totalString = totalString .. v .. LocalString.GetString('、')
        end
      end
			self.returnNode.transform:Find('Cost/Label'):GetComponent(typeof(UILabel)).text = totalString

      local returnBtn = self.returnNode.transform:Find('ExchangeButton').gameObject
      UIEventListener.Get(returnBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        --local msg = g_MessageMgr:FormatMessage("MALL_ENTRANCE_TIANQI_TIP", jifen)
        local msg = SafeStringFormat3(LocalString.GetString('确定要回退[c][FF88FF]%s·%s级外观[-][/c]吗？请谨慎操作'),data.Name,level)
        MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function ()
          local sendId = self:GetFormerTalismanLevel(selectItem.id)
          if sendId then
            Gac2Gas.RequestLevelDownFaBaoSpecialFx(sendId)
          else
            Gac2Gas.RequestReturnFaBaoSpecialFx(selectItem.id)
          end

        end), nil, nil, nil, false)
      end)
    end

	end
end

function CLuaTalismanFxReturnWnd:InitTalismanSinTab(node,tFxId,tId,last,defaultClick)
  local lightNode = node:Find('light').gameObject
  local clickNode = node:Find('bg').gameObject
	local bar = node:Find('slider')
	local barBg = node:Find('sliderBg')
	lightNode:SetActive(false)

	local got = LuaTalismanMgr:CheckContain(tId)
	if got then
		clickNode.transform.localPosition = Vector3(clickNode.transform.localPosition.x,clickNode.transform.localPosition.y,0)
		lightNode.transform.localPosition = Vector3(lightNode.transform.localPosition.x,lightNode.transform.localPosition.y,0)
		if bar then
			bar.gameObject:SetActive(true)
		end
	else
		clickNode.transform.localPosition = Vector3(clickNode.transform.localPosition.x,clickNode.transform.localPosition.y,-1)
		lightNode.transform.localPosition = Vector3(lightNode.transform.localPosition.x,lightNode.transform.localPosition.y,-1)
		if bar then
			bar.gameObject:SetActive(false)
		end
	end

	local click = function(go)
		if self.lightNode then
			self.lightNode:SetActive(false)
		end
		self.lightNode = lightNode
		self.lightNode:SetActive(true)
		local selectData = {}
		selectData.id = tFxId
		selectData.got = got
		self:InitSingleDetails(selectData)
	end

	if defaultClick then
		if self.m_RealSelectFxId == tFxId then
			click()
			self.m_RealSelectFxId = nil
		end
	elseif not self.clickOnce then
		if not got then
			click()
			self.clickOnce = true
		elseif last == 0 then
			click()
		end
	end

	UIEventListener.Get(clickNode).onClick = DelegateFactory.VoidDelegate(click)

	if last == 0 then
		if bar then
			bar.gameObject:SetActive(false)
		end
		if barBg then
			barBg.gameObject:SetActive(false)
		end
	else
		if bar then
			bar.gameObject:SetActive(true)
		end
		if barBg then
			barBg.gameObject:SetActive(true)
		end
	end
end

function CLuaTalismanFxReturnWnd:InitDetails()
	local selectItem = self.m_ItemTable[self.m_SelectIndex]
	local tData = ItemGet_TalismanFX.GetData(selectItem.id)

  local talismanType = tData.TalismanID % 100
  local talismanTable = {}
  ItemGet_TalismanFX.Foreach(function (key, data)
    local tType = data.TalismanID % 100
    if tType == talismanType then
      local got = LuaTalismanMgr:CheckContain(data.TalismanID)
      if got then
        table.insert(talismanTable,{key,data.TalismanID})
      end
    end
  end)

	local tCount = #talismanTable
--  if tCount <= 1 then
--    self.talismanTab:SetActive(false)
--		self:InitSingleDetails()
--    return
--  end

	self.talismanTab:SetActive(true)
  table.sort(talismanTable,function(a,b)
    return a[2] < b[2]
  end)

	local defaultClick = nil
	if self.m_RealSelectFxId and self.m_RealSelectFxId > 0 then
		defaultClick = true
	end
	self.clickOnce = nil

  local talismanNodeTable = {self.talismanTab.transform:Find('node1'),self.talismanTab.transform:Find('node2'),self.talismanTab.transform:Find('node3'),self.talismanTab.transform:Find('node4'),self.talismanTab.transform:Find('node5')}
  for i,v in pairs(talismanNodeTable) do
    if talismanTable[i] then
      v.gameObject:SetActive(true)
      self:InitTalismanSinTab(v,talismanTable[i][1],talismanTable[i][2],tCount-i,defaultClick)
    else
      v.gameObject:SetActive(false)
    end
  end
end

function CLuaTalismanFxReturnWnd:OnEnable()
	g_ScriptEvent:AddListener("TalismanFxUpdate", self, "OnTalismanFxUpdate")
	g_ScriptEvent:AddListener("TalismanFxListUpdate", self, "OnTalismanFxListUpdate")
end

function CLuaTalismanFxReturnWnd:OnDisable()
	g_ScriptEvent:RemoveListener("TalismanFxUpdate", self, "OnTalismanFxUpdate")
	g_ScriptEvent:RemoveListener("TalismanFxListUpdate", self, "OnTalismanFxListUpdate")
end

function CLuaTalismanFxReturnWnd:OnTalismanFxUpdate(args)
	local obj = args[0]
	if TypeIs(obj, typeof(CClientMainPlayer)) then
		self:Init()
	end
end

function CLuaTalismanFxReturnWnd:OnTalismanFxListUpdate()
	self:Init()
end
