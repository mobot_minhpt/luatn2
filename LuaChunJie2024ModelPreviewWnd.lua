local QnModelPreviewer = import "L10.UI.QnModelPreviewer"

LuaChunJie2024ModelPreviewWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaChunJie2024ModelPreviewWnd, "modelPreview", "ModelPreview", QnModelPreviewer)
RegistChildComponent(LuaChunJie2024ModelPreviewWnd, "topLabel", "TopLabel", UILabel)
--@endregion RegistChildComponent end

function LuaChunJie2024ModelPreviewWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaChunJie2024ModelPreviewWnd:Init()
    local type = LuaChunJie2024Mgr.fuYanInfo.previewType
    if type == "MaxLevelFuYan" then
        self.topLabel.text = g_MessageMgr:FormatMessage("CHUNJIE2024_MAXLEVEL_FUYAN_BEISHI_TIP")
    elseif type == "YuZhuanManXi" then
        self.topLabel.text = g_MessageMgr:FormatMessage("CHUNJIE2024_YUZHUANMANXI_BEISHI_TIP")
    end

    local mainPlayer = CClientMainPlayer.Inst
    if not mainPlayer then return end
    local appearance = mainPlayer.AppearanceProp

    self.modelPreview.IsShowHighRes = true
    self.modelPreview:PreviewMainPlayer(appearance.HideGemFxToOtherPlayer, appearance.IntesifySuitId, 0, 0, 0, 0, 180, nil, 0)
end

--@region UIEvent

--@endregion UIEvent
