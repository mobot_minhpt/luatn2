LuaCompetitionHonorMgr = {}
----------------------------
--- 名人堂
----------------------------
LuaCompetitionHonorMgr.m_FameHallWndOpen = true
function LuaCompetitionHonorMgr:OpenCompetitionHonorFameHallWnd()
    if self.m_FameHallWndOpen then
        Gac2Gas.QueryPlayerCompetitionHonorRank(true)
    end
end

LuaCompetitionHonorMgr.m_SelfRank = nil
LuaCompetitionHonorMgr.m_SelfScore = nil
LuaCompetitionHonorMgr.m_ViewId = nil
LuaCompetitionHonorMgr.m_RankData = nil
function LuaCompetitionHonorMgr:PlayerQueryCompetitionHonorRankReturn(selfRank, selfScore, viewId, rankData)
    LuaCompetitionHonorMgr.m_SelfRank = selfRank
    LuaCompetitionHonorMgr.m_SelfScore = selfScore
    LuaCompetitionHonorMgr.m_ViewId = viewId
    LuaCompetitionHonorMgr.m_RankData = rankData
    if not CUIManager.IsLoaded(CLuaUIResources.CompetitionHonorFameHallWnd) then
        CUIManager.ShowUI(CLuaUIResources.CompetitionHonorFameHallWnd)
    else
        g_ScriptEvent:BroadcastInLua("PlayerQueryCompetitionHonorRankReturn")
    end
end