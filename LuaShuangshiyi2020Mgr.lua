local CFashionPreviewMgr=import "L10.UI.CFashionPreviewMgr"
local CScene=import "L10.Game.CScene"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local LocalString = import "LocalString"
local Main = import "L10.Engine.Main"
local ShaderEx = import "ShaderEx"
CLuaShuangshiyi2020Mgr={}

---------------------------------------------------------------------------------------------------------------------
--ZhongcaoMgr

CLuaShuangshiyi2020Mgr.m_DataList = nil
CLuaShuangshiyi2020Mgr.m_ZhongCaoItem = nil
CLuaShuangshiyi2020Mgr.m_ZhongCaoPlayerId = nil
CLuaShuangshiyi2020Mgr.m_PreviewItem = nil
CLuaShuangshiyi2020Mgr.m_CountLookup = nil
CLuaShuangshiyi2020Mgr.m_HotLookup = nil
CLuaShuangshiyi2020Mgr.m_FashionId = nil
CLuaShuangshiyi2020Mgr.m_EnableMultiLight = true

if Main.Inst.EngineVersion < ShaderEx.s_MinVersion and Main.Inst.EngineVersion ~= - 1 then
    CLuaShuangshiyi2020Mgr.m_EnableMultiLight = false
end

function CLuaShuangshiyi2020Mgr.CheckPlayerID()
    if CClientMainPlayer.Inst then
        if CClientMainPlayer.Inst.Id ~= CLuaShuangshiyi2020Mgr.m_ZhongCaoPlayerId then
            CLuaShuangshiyi2020Mgr.m_ZhongCaoPlayerId = CClientMainPlayer.Inst.Id
            CLuaShuangshiyi2020Mgr.m_ZhongCaoItem = nil
        end
    else
        CLuaShuangshiyi2020Mgr.m_ZhongCaoItem = nil
    end
end

function CLuaShuangshiyi2020Mgr.SetFashionId(fashionId)
    if fashionId==nil then
        return
    end

    CLuaShuangshiyi2020Mgr.m_FashionId = fashionId
    local fashion = Fashion_Fashion.GetData(fashionId)

    if fashion then 
        if fashion.Position == 0 then
            CFashionPreviewMgr.HeadFashionID = fashionId
        elseif fashion.Position == 1 then
            CFashionPreviewMgr.BodyFashionID = fashionId
            local entiretyHeadFashion = Fashion_Fashion.GetData(fashion.PreviewSuit)
            if entiretyHeadFashion then
                CFashionPreviewMgr.HeadFashionID = fashion.PreviewSuit
            end
        end
        CFashionPreviewMgr.ZuoQiFashionID = 0
        CFashionPreviewMgr.UmbrellaFashionID = 0
    end
end

---------------------------------------------------------------------------------------------------------------------
--VoucherMgr

CLuaShuangshiyi2020Mgr.pList = nil
CLuaShuangshiyi2020Mgr.vList = nil

function CLuaShuangshiyi2020Mgr.UseVoucher()
    Gac2Gas.QueryDouble11VouchersInfo()
end

function CLuaShuangshiyi2020Mgr:OnQueryDouble11VouchersInfoRet(pList, vList)
    CLuaShuangshiyi2020Mgr.pList = pList
    CLuaShuangshiyi2020Mgr.vList = vList
    CUIManager.ShowUI(CLuaUIResources.Shuangshiyi2020VoucherWnd)
end

---------------------------------------------------------------------------------------------------------------------
--ClearTrolleyMgr

CLuaShuangshiyi2020Mgr.m_Credit = nil
CLuaShuangshiyi2020Mgr.m_MonsterCount = nil
CLuaShuangshiyi2020Mgr.m_RewardItemId = nil
-- 保存player数据 以防止切换场景时访问不到CClientMainPlayer
CLuaShuangshiyi2020Mgr.m_Name = nil
CLuaShuangshiyi2020Mgr.m_Id = nil
CLuaShuangshiyi2020Mgr.m_Class = nil
CLuaShuangshiyi2020Mgr.m_Gender = nil
CLuaShuangshiyi2020Mgr.m_Level = nil
CLuaShuangshiyi2020Mgr.m_ServerName = nil

function CLuaShuangshiyi2020Mgr:OnClearTrolleyStatics(credit, monsterCount, monsterDeath, round)
    CLuaShuangshiyi2020Mgr.SetSceneMode()
    CLuaShuangshiyi2020Mgr.m_Credit = credit
    CLuaShuangshiyi2020Mgr.m_MonsterCount = monsterCount

    if not CUIManager.IsLoaded(CLuaUIResources.Shuangshiyi2020ClearTrolleyPlayingWnd) then
        CUIManager.ShowUI(CLuaUIResources.Shuangshiyi2020ClearTrolleyPlayingWnd)
    end
    g_ScriptEvent:BroadcastInLua("ClearTrolleyPlayingWndUpdate", credit, monsterCount, monsterDeath, round)
end

function CLuaShuangshiyi2020Mgr:OnClearTrolleyResult(credit, rewardItemId)
    CLuaShuangshiyi2020Mgr.m_Credit = credit
    CLuaShuangshiyi2020Mgr.m_RewardItemId = rewardItemId
    CUIManager.ShowUI(CLuaUIResources.Shuangshiyi2020ClearTrolleyResultWnd)
end

function CLuaShuangshiyi2020Mgr.EnterGamePlay()
    CLuaShuangshiyi2020Mgr.SetSceneMode()
    local gamePlayId = Double11_Setting.GetData().ClearTrolleyGamePlayId
    Gac2Gas.EnterShuangshiyi2020Gameplay()
    CUIManager.CloseUI(CLuaUIResources.Shuangshiyi2020ClearTrolleyEnterWnd)
end

function CLuaShuangshiyi2020Mgr.SetSceneMode()
    if CScene.MainScene then
        local gamePlayId=CScene.MainScene.GamePlayDesignId
        if gamePlayId == Double11_Setting.GetData().ClearTrolleyGamePlayId then
            CScene.MainScene.ShowLeavePlayButton = false
            CScene.MainScene.ShowTimeCountDown = true
            CScene.MainScene.ShowMonsterCountDown = false
        end
    end
    if CClientMainPlayer.Inst then
        CLuaShuangshiyi2020Mgr.m_Name = CClientMainPlayer.Inst.Name
        CLuaShuangshiyi2020Mgr.m_Id = CClientMainPlayer.Inst.Id
        CLuaShuangshiyi2020Mgr.m_Class = CClientMainPlayer.Inst.Class
        CLuaShuangshiyi2020Mgr.m_Gender = CClientMainPlayer.Inst.Gender
        CLuaShuangshiyi2020Mgr.m_Level = CClientMainPlayer.Inst.Level
        CLuaShuangshiyi2020Mgr.m_ServerName = CClientMainPlayer.Inst:GetMyServerName()
    end
end

---------------------------------------------------------------------------------------------------------------------
--LotteryMgr

CLuaShuangshiyi2020Mgr.m_LotteryData = nil
CLuaShuangshiyi2020Mgr.m_LotteryList = nil
CLuaShuangshiyi2020Mgr.m_MyLotteryInfo = nil
CLuaShuangshiyi2020Mgr.m_RewardFirstShowMark = nil
CLuaShuangshiyi2020Mgr.m_RewardLevel = nil

function CLuaShuangshiyi2020Mgr:CalculateLotteryData()
    if CClientMainPlayer.Inst then
        CLuaShuangshiyi2020Mgr.m_Id = CClientMainPlayer.Inst.Id
    end
    if CLuaShuangshiyi2020Mgr.m_LotteryData ~= nil then
        CLuaShuangshiyi2020Mgr.m_LotteryList = {}
        CLuaShuangshiyi2020Mgr.m_MyLotteryInfo = nil
        local rewardInfo = {LocalString.GetString("特等奖"),LocalString.GetString("一等奖"),LocalString.GetString("二等奖"),LocalString.GetString("三等奖")}
        for i=0, CLuaShuangshiyi2020Mgr.m_LotteryData.Count-1 do
            local list = CLuaShuangshiyi2020Mgr.m_LotteryData[i]
            local reward = rewardInfo[i+1]
            for j=0, list.Count-1 do
                local data = list[j]
                data["reward"] = reward
                -- 1：特等奖 2：一等奖 3：二等奖 4：三等奖
                data["rewardIndex"] = i+1
                table.insert(CLuaShuangshiyi2020Mgr.m_LotteryList, data)
                if CLuaShuangshiyi2020Mgr.m_Id == data["id"] then
                    CLuaShuangshiyi2020Mgr.m_MyLotteryInfo = data
                end
            end
        end
    end
end

function CLuaShuangshiyi2020Mgr:RewardFirstShow(prize, payJade, returnJade)
    CLuaShuangshiyi2020Mgr.m_RewardFirstShowMark = true
    CLuaShuangshiyi2020Mgr.m_RewardLevel = prize
end