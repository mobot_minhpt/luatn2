require("common/common_include")

local CRankData=import "L10.UI.CRankData"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local Gac2Gas=import "L10.Game.Gac2Gas"
local LuaGameObject=import "LuaGameObject"
local CPlayerInfoMgr=import "CPlayerInfoMgr"
local EnumPlayerInfoContext=import "L10.Game.EnumPlayerInfoContext"
local EChatPanel=import "L10.Game.EChatPanel"
local AlignType=import "CPlayerInfoMgr+AlignType"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"


LuaWaKuangRankWnd=class()
RegistClassMember(LuaWaKuangRankWnd,"m_MyRankLabel")
RegistClassMember(LuaWaKuangRankWnd,"m_MyNameLabel")
RegistClassMember(LuaWaKuangRankWnd,"m_MyTimeLabel")

RegistClassMember(LuaWaKuangRankWnd,"m_TitleLabel")

RegistClassMember(LuaWaKuangRankWnd,"m_TableViewDataSource")
RegistClassMember(LuaWaKuangRankWnd,"m_TableView")
RegistClassMember(LuaWaKuangRankWnd,"m_RankList")


function LuaWaKuangRankWnd:Init()
    local UILabelType=typeof(UILabel)

    self.m_MyRankLabel=LuaGameObject.GetChildNoGC(self.transform,"MyRankLabel").label
    self.m_MyNameLabel=LuaGameObject.GetChildNoGC(self.transform,"MyNameLabel").label
    self.m_MyTimeLabel=LuaGameObject.GetChildNoGC(self.transform,"MyTimeLabel").label

    if CClientMainPlayer.Inst then
        self.m_MyNameLabel.text= CClientMainPlayer.Inst.Name
    end
    self.m_MyRankLabel.text=LocalString.GetString("未上榜")
    self.m_MyTimeLabel.text=LocalString.GetString("—")

    self.m_TitleLabel=FindChild(self.transform, "TitleLabel"):GetComponent(UILabelType)
    local titleName = LocalString.GetString("挖矿挑战")
    self.m_TitleLabel.text = titleName

    self.m_TableView=LuaGameObject.GetChildNoGC(self.transform,"TableView").tableView
    local function initItem(item,index)
        if index % 2 == 1 then
            item:SetBackgroundTexture("common_textbg_02_dark")
        else
            item:SetBackgroundTexture("common_textbg_02_light")
        end
        self:InitItem(item,index)
    end

    self.m_TableViewDataSource=DefaultTableViewDataSource.CreateByCount(0,initItem)
    self.m_TableView.m_DataSource=self.m_TableViewDataSource
    self.m_TableView.OnSelectAtRow=DelegateFactory.Action_int(function (row)
        -- print("select ",row)
        self:OnSelectAtRow(row)
    end)
    Gac2Gas.QueryRank(41000179)
end

function LuaWaKuangRankWnd:OnSelectAtRow(row)
    local data=self.m_RankList[row]
    if data then
        CPlayerInfoMgr.ShowPlayerPopupMenu(data.PlayerIndex, EnumPlayerInfoContext.Undefined,EChatPanel.Undefined,"",nil,Vector3.zero,AlignType.Default)
    end
end
function LuaWaKuangRankWnd:InitItem(item,index)
    local tf=item.transform
    local timeLabel=LuaGameObject.GetChildNoGC(tf,"TimeLabel").label
    timeLabel.text=""
    local nameLabel=LuaGameObject.GetChildNoGC(tf,"NameLabel").label
    nameLabel.text=" "
    local rankLabel=LuaGameObject.GetChildNoGC(tf,"RankLabel").label
    rankLabel.text=""
    local rankSprite=LuaGameObject.GetChildNoGC(tf,"RankImage").sprite
    rankSprite.spriteName=""
    
    local info=self.m_RankList[index]

    local rank=info.Rank
    if rank==1 then
        rankSprite.spriteName="Rank_No.1"
    elseif rank==2 then
        rankSprite.spriteName="Rank_No.2png"
    elseif rank==3 then
        rankSprite.spriteName="Rank_No.3png"
    else
        rankLabel.text=tostring(rank)
    end

    nameLabel.text=info.Name

    timeLabel.text=self:FormatTime(info.Value)
end
function LuaWaKuangRankWnd:FormatTime(time)
    local minute=math.floor(time/60)
    local second=time%60
    return SafeStringFormat3(LocalString.GetString("%02d分%02d秒"),minute,second)
end
function LuaWaKuangRankWnd:OnEnable()
    g_ScriptEvent:AddListener("OnRankDataReady", self, "OnRankDataReady")
    
end
function LuaWaKuangRankWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnRankDataReady", self, "OnRankDataReady")

end

function LuaWaKuangRankWnd:OnRankDataReady()
    local myInfo=CRankData.Inst.MainPlayerRankInfo
    if myInfo.Rank>0 then
        self.m_MyRankLabel.text=myInfo.Rank
    else
        self.m_MyRankLabel.text=LocalString.GetString("未上榜")
    end
    self.m_MyNameLabel.text= myInfo.Name
    if CClientMainPlayer.Inst then
        self.m_MyNameLabel.text= CClientMainPlayer.Inst.Name
    end
    if myInfo.Value>0 then
        self.m_MyTimeLabel.text=self:FormatTime(myInfo.Value)
    else
        self.m_MyTimeLabel.text=LocalString.GetString("—")
    end

    self.m_RankList=CRankData.Inst.RankList

    self.m_TableViewDataSource.count=self.m_RankList.Count
    self.m_TableView:ReloadData(true,false)
end

return LuaWaKuangRankWnd
