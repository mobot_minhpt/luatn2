local Random = import "UnityEngine.Random"
local Object = import "UnityEngine.Object"
local UITexture = import "UITexture"
local Time = import "UnityEngine.Time"
local UICamera = import "UICamera"
local Physics = import "UnityEngine.Physics"
local CUIFx = import "L10.UI.CUIFx"

LuaZhuErDanChuoPaoPaoItem = class()

RegistClassMember(LuaZhuErDanChuoPaoPaoItem, "m_HitFx")

RegistClassMember(LuaZhuErDanChuoPaoPaoItem, "m_Rotation")
RegistClassMember(LuaZhuErDanChuoPaoPaoItem, "m_Scale")
RegistClassMember(LuaZhuErDanChuoPaoPaoItem, "m_DropSpeed")

RegistClassMember(LuaZhuErDanChuoPaoPaoItem, "m_LifeTime")
RegistClassMember(LuaZhuErDanChuoPaoPaoItem, "m_Fin")
RegistClassMember(LuaZhuErDanChuoPaoPaoItem, "m_Score")
RegistClassMember(LuaZhuErDanChuoPaoPaoItem, "m_FloatSpeed")
RegistClassMember(LuaZhuErDanChuoPaoPaoItem, "m_SinParam")

function LuaZhuErDanChuoPaoPaoItem:Init(args)
	if args and args.Length > 0 then
		self.m_Score = args[0]
	else
		self.m_Score = 0
	end

	self.m_LifeTime = 5
	Object.Destroy(self.gameObject, self.m_LifeTime)

	UIEventListener.Get(self.gameObject).onPress = DelegateFactory.BoolDelegate(function(go,ispressed)
		if ispressed then
			g_ScriptEvent:BroadcastInLua("ZhuErDan_ChuoPaoPao_AddScore",self.m_Score)
			self.m_HitFx:LoadFx("Fx/UI/Prefab/UI_jieyuanbao.prefab")
			Object.Destroy(self.gameObject, 0.5)
			self.m_Fin = true
		end
	end)
end
function LuaZhuErDanChuoPaoPaoItem:Awake()


	self.m_DropSpeed = Random.Range(300 - 100, 300 + 100)
	-- self.m_LargenSpeed = 0.1
	-- self.m_Rotation = Random.Range(-30, 30)
	self.m_Scale = Random.Range(0.4, 0.6)
	self.m_FloatSpeed = (math.random()-0.5)*500

	self.m_SinParam = math.random()*3.14

	self.m_Fin = false
end

function LuaZhuErDanChuoPaoPaoItem:OnEnable()
	self.m_HitFx = self.transform:Find("HitFx"):GetComponent(typeof(CUIFx))
	self.m_HitFx:DestroyFx()
end

function LuaZhuErDanChuoPaoPaoItem:Update()
	if self.m_Score==0 then return end

	if not self.m_Fin then
		local floatSpped = self.m_FloatSpeed* Time.deltaTime*math.sin(self.m_SinParam)
		self.m_SinParam = self.m_SinParam+Time.deltaTime*2*math.random()
		local newPos = Vector3(self.transform.localPosition.x + floatSpped, self.transform.localPosition.y - self.m_DropSpeed * Time.deltaTime,  0)
		self.transform.localPosition = newPos
	end
end
