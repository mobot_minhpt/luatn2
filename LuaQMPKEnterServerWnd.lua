local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnTableView = import "L10.UI.QnTableView"
local CButton = import "L10.UI.CButton"
local CUITexture = import "L10.UI.CUITexture"
local EServerStatus = import "L10.UI.EServerStatus"
local CGameServer = import "L10.UI.CGameServer"
local CLoginMgr = import "L10.Game.CLoginMgr"
local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"

LuaQMPKEnterServerWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaQMPKEnterServerWnd, "ServerTab", "ServerTab", QnTableView)
RegistChildComponent(LuaQMPKEnterServerWnd, "AvatarTab", "AvatarTab", QnTableView)
RegistChildComponent(LuaQMPKEnterServerWnd, "OpBtn", "OpBtn", CButton)

--@endregion RegistChildComponent end
RegistClassMember(LuaQMPKEnterServerWnd, "m_NoRoleTip")
RegistClassMember(LuaQMPKEnterServerWnd, "m_RoleInfos")
RegistClassMember(LuaQMPKEnterServerWnd, "m_ServerInfos")
RegistClassMember(LuaQMPKEnterServerWnd, "m_CurRoleInx")
RegistClassMember(LuaQMPKEnterServerWnd, "m_CurServerInx")
RegistClassMember(LuaQMPKEnterServerWnd, "m_CurBattleServerId")
RegistClassMember(LuaQMPKEnterServerWnd, "m_CurBattleServerInx")
RegistClassMember(LuaQMPKEnterServerWnd, "m_SplitSprite")

function LuaQMPKEnterServerWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.m_NoRoleTip = self.transform:Find("Anchor/AvatarList/NoRoleTip").gameObject
    self.m_SplitSprite = self.transform:Find("Anchor/Texture/Sprite").gameObject

    UIEventListener.Get(self.OpBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        local curServerInfo = self.m_ServerInfos[self.m_CurServerInx]
        local curRoleInfo = self.m_RoleInfos[self.m_CurRoleInx]
		if curServerInfo == nil or curRoleInfo == nil then
            return 
        end
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("QMPK_FASTSWITCHSERVER_MAKESURE"),DelegateFactory.Action(function()
            local gs = CLoginMgr.Inst:GetMainlandServerById(curServerInfo.id)
            if gs then
                CLoginMgr.Inst:FastSwitchGameServer(gs, curRoleInfo.PlayerId)
            end
        end),nil,LocalString.GetString("确定"),LocalString.GetString("取消"),false)
	end)

    self.AvatarTab.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_RoleInfos
        end,
        function(item, index)
            self:InitItem1(item, self.m_RoleInfos[index + 1])
        end
    )

    self.ServerTab.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_ServerInfos
        end,
        function(item, index)
            self:InitItem2(item, index + 1, self.m_ServerInfos[index + 1])
        end
    )

    self.AvatarTab.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        self.m_CurRoleInx = row + 1
    end)

    self.ServerTab.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        self.m_CurServerInx = row + 1
        self:RefreshRoleInfo()
    end)
end

function LuaQMPKEnterServerWnd:Init()
    self:RefreshServerInfo()
    self.ServerTab:SetSelectRow(self.m_CurBattleServerInx or 0, true)
end

function LuaQMPKEnterServerWnd:RefreshServerInfo()
    if CLuaQMPKMgr.m_RoleInfos then
        for i = 1, #CLuaQMPKMgr.m_RoleInfos do
            local roleInfo = CLuaQMPKMgr.m_RoleInfos[i]
            if roleInfo.JoinZhanDui == 1 then
                self.m_CurBattleServerId = roleInfo.ServerGroupId
                break
            end
        end
    end

    self.m_ServerInfos = {}
    if CLuaQMPKMgr.m_ServerInfos then
        for i = 1, #CLuaQMPKMgr.m_ServerInfos do
            table.insert(self.m_ServerInfos, CLuaQMPKMgr.m_ServerInfos[i])
            if CLuaQMPKMgr.m_ServerInfos[i].id == self.m_CurBattleServerId then
                self.m_CurBattleServerInx = i - 1
            end
        end
    end
    self.ServerTab:ReloadData(true, false)
end

function LuaQMPKEnterServerWnd:RefreshRoleInfo()
    self.m_RoleInfos = {}
    local curServerId = self.m_ServerInfos[self.m_CurServerInx].id
    if CLuaQMPKMgr.m_RoleInfos then
        for i = 1, #CLuaQMPKMgr.m_RoleInfos do
            local roleInfo = CLuaQMPKMgr.m_RoleInfos[i]
            if roleInfo.ServerGroupId == curServerId then
                table.insert(self.m_RoleInfos, roleInfo)
            end
        end
    end
    self.AvatarTab:ReloadData(true, false)
    self.m_CurRoleInx = nil
    self.m_NoRoleTip:SetActive(#self.m_RoleInfos == 0)
    self.m_SplitSprite:SetActive(#self.m_RoleInfos > 0)
    self.OpBtn.gameObject:SetActive(#self.m_RoleInfos > 0)
    if #self.m_RoleInfos > 0 then
        self.AvatarTab:SetSelectRow(0, true)
    end
end

--@region UIEvent

--@endregion UIEvent

function LuaQMPKEnterServerWnd:InitItem1(item, info)
    local playerIcon        = item.transform:Find("Portrait/Texture"):GetComponent(typeof(CUITexture))
    local playerNameLab     = item.transform:Find("Portrait/NameLabel"):GetComponent(typeof(UILabel))
    local playerLvLab       = item.transform:Find("Portrait/LevelLabel"):GetComponent(typeof(UILabel))
    local battleSprite      = item.transform:Find("BattleSprite").gameObject

    local portraitName = CUICommonDef.GetPortraitName(info.Profession, info.Gender, -1)
	playerIcon:LoadNPCPortrait(portraitName,false)
    playerNameLab.text = string.sub(info.Name, 2, #info.Name-1)
    playerLvLab.text = info.Grade
    battleSprite:SetActive(info.JoinZhanDui == 1)
end

function LuaQMPKEnterServerWnd:InitItem2(item, index, info)
    local serverNameLab     = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local battleSprite      = item.transform:Find("BattleSprite").gameObject
    local serverStateSpr    = item.transform:Find("severstate"):GetComponent(typeof(UISprite))

    serverNameLab.text = LocalString.GetString("全民争霸服") .. tostring(index)
    battleSprite:SetActive(info.id == self.m_CurBattleServerId)

    local status = EServerStatus.Idle
    if info.onlineNum > CQuanMinPKMgr.Inst.m_ServerLimitRed then
        status = EServerStatus.VeryBusy
    elseif info.onlineNum > CQuanMinPKMgr.Inst.m_ServerLimitYellow then
        status = EServerStatus.Busy
    end  
    serverStateSpr.spriteName = CGameServer.GetStatusSprite(status)
end

function LuaQMPKEnterServerWnd:OnDestroy()
    CLuaQMPKMgr.ClearFastEnterServerInfo()
end
