local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnTableView = import "L10.UI.QnTableView"
local UILabel = import "UILabel"
local CSkillItemCell = import "L10.UI.CSkillItemCell"
local GameObject = import "UnityEngine.GameObject"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local QnButton = import "L10.UI.QnButton"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local System = import "System"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local Constants = import "L10.Game.Constants"
local SoundManager = import "SoundManager"
local Extensions = import "Extensions"
local UIScrollView = import "UIScrollView"
local CUIFx = import "L10.UI.CUIFx"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"
local QnTableItem = import "L10.UI.QnTableItem"
local MsgPackImpl = import "MsgPackImpl"
local Object = import "System.Object"
local QnCheckBox = import "L10.UI.QnCheckBox"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CChatLinkMgr = import "CChatLinkMgr"
local CTrackMgr = import "L10.Game.CTrackMgr"

LuaZongMenSkillView = class()

RegistChildComponent(LuaZongMenSkillView, "SkillTitleLab", "SkillTitleLab", UILabel)
RegistChildComponent(LuaZongMenSkillView, "MaxLevelLab", "MaxLevelLab", UILabel)
RegistChildComponent(LuaZongMenSkillView, "FeiShengTipLab", "FeiShengTipLab", UILabel)
RegistChildComponent(LuaZongMenSkillView, "SkillItemTemplate", "SkillItemTemplate", CSkillItemCell)
RegistChildComponent(LuaZongMenSkillView, "CurDescLab", "CurDescLab", UILabel)
RegistChildComponent(LuaZongMenSkillView, "NoLab", "NoLab", UILabel)
RegistChildComponent(LuaZongMenSkillView, "NextDescLab", "NextDescLab", UILabel)
RegistChildComponent(LuaZongMenSkillView, "NextDesc", "NextDesc", GameObject)
-- costType1
RegistChildComponent(LuaZongMenSkillView, "QnCostAndOwnMoney1", "QnCostAndOwnMoney1", CCurentMoneyCtrl)
RegistChildComponent(LuaZongMenSkillView, "QnCostAndOwnMoney2", "QnCostAndOwnMoney2", CCurentMoneyCtrl)
RegistChildComponent(LuaZongMenSkillView, "Cost", "Cost", UILabel)
RegistChildComponent(LuaZongMenSkillView, "Own", "Own", UILabel)
-- costType1 end
RegistChildComponent(LuaZongMenSkillView, "CostTip", "CostTip", UILabel)
RegistChildComponent(LuaZongMenSkillView, "LvNeed", "LvNeed", GameObject)
RegistChildComponent(LuaZongMenSkillView, "LvNeedLab", "LvNeedLab", UILabel)
RegistChildComponent(LuaZongMenSkillView, "SoulCoreLvNeed", "SoulCoreLvNeed", GameObject)
RegistChildComponent(LuaZongMenSkillView, "SoulCoreLvNeedLab", "SoulCoreLvNeedLab", UILabel)
RegistChildComponent(LuaZongMenSkillView, "OpBtn", "OpBtn", QnButton)
RegistChildComponent(LuaZongMenSkillView, "TipBtn", "TipBtn", QnButton)

RegistClassMember(LuaZongMenSkillView, "TableViewTran")
RegistClassMember(LuaZongMenSkillView, "TableView")
RegistClassMember(LuaZongMenSkillView, "m_CurDesc")
RegistClassMember(LuaZongMenSkillView, "m_NextDesc")
RegistClassMember(LuaZongMenSkillView, "m_AllCost")
RegistClassMember(LuaZongMenSkillView, "m_DataList")
RegistClassMember(LuaZongMenSkillView, "m_CurSkillCls")
RegistClassMember(LuaZongMenSkillView, "m_CurSkillLvs")
RegistClassMember(LuaZongMenSkillView, "m_ShiMenMiShuCls")
RegistClassMember(LuaZongMenSkillView, "m_CostType1")
RegistClassMember(LuaZongMenSkillView, "m_CostType2")
RegistClassMember(LuaZongMenSkillView, "m_CostType3")
RegistClassMember(LuaZongMenSkillView, "m_CostType4")
RegistClassMember(LuaZongMenSkillView, "m_ScoreCostLab")
RegistClassMember(LuaZongMenSkillView, "m_ScoreOwnLab")
RegistClassMember(LuaZongMenSkillView, "m_LearnTip")
RegistClassMember(LuaZongMenSkillView, "m_Scroll")
RegistClassMember(LuaZongMenSkillView, "m_LevelUpFx")
RegistClassMember(LuaZongMenSkillView, "m_SkillDesc")
RegistClassMember(LuaZongMenSkillView, "m_ManaNeed")
RegistClassMember(LuaZongMenSkillView, "m_ManaCost")
RegistClassMember(LuaZongMenSkillView, "m_ManaOwn")
RegistClassMember(LuaZongMenSkillView, "m_ItemCells")
RegistClassMember(LuaZongMenSkillView, "m_QnExpCtrl")
RegistClassMember(LuaZongMenSkillView, "m_QnMoneyCtrl")
RegistClassMember(LuaZongMenSkillView, "m_Bg")
RegistClassMember(LuaZongMenSkillView, "m_NextDescWid")

RegistClassMember(LuaZongMenSkillView, "m_InMana")
RegistClassMember(LuaZongMenSkillView, "m_InLearnMana")
RegistClassMember(LuaZongMenSkillView, "m_GTXNextLevel")
RegistClassMember(LuaZongMenSkillView, "m_GuanTianXiangIcon")

RegistClassMember(LuaZongMenSkillView, "m_ScoreCostLab2")
RegistClassMember(LuaZongMenSkillView, "m_ScoreOwnLab2")
RegistClassMember(LuaZongMenSkillView, "m_QnMoneyCtrl2")
RegistClassMember(LuaZongMenSkillView, "m_DeductionBtn")

RegistClassMember(LuaZongMenSkillView, "m_MoneyCostAuto")
RegistClassMember(LuaZongMenSkillView, "m_ScoreCostLabAuto")
RegistClassMember(LuaZongMenSkillView, "m_ScoreOwnLabAuto")
RegistClassMember(LuaZongMenSkillView, "m_LevelUnableUseMoneny")

function LuaZongMenSkillView:Awake()
    self.TableViewTran  = self.transform:Find("SkillsView/TableView")
    self.m_CurDesc      = self.transform:Find("SkillInfoView/Desc/Scroll/CurDesc").gameObject
    self.m_CurDescCollider = self.transform:Find("SkillInfoView/Desc/Scroll/Offset").gameObject
    self.m_NextDesc     = self.transform:Find("SkillInfoView/Desc/Scroll/NextDesc").gameObject
    self.m_AllCost      = self.transform:Find("SkillInfoView/Cost").gameObject

    self.m_CostType1    = self.transform:Find("SkillInfoView/Cost/CostType1").gameObject
    self.m_CostType2    = self.transform:Find("SkillInfoView/Cost/CostType2").gameObject
    self.m_CostType3    = self.transform:Find("SkillInfoView/Cost/CostType3").gameObject
    self.m_CostType4    = self.transform:Find("SkillInfoView/Cost/CostType4").gameObject

    self.m_QnExpCtrl    = self.transform:Find("SkillInfoView/Cost/CostType3/ExpCost/QnCostAndOwnMoney1"):GetComponent(typeof(CCurentMoneyCtrl))
    self.m_QnMoneyCtrl  = self.transform:Find("SkillInfoView/Cost/CostType3/MoneyCost/QnCostAndOwnMoney2"):GetComponent(typeof(CCurentMoneyCtrl))

    self.m_ScoreCostLab = self.transform:Find("SkillInfoView/Cost/CostType2/ScoreCost/Cost/Cost"):GetComponent(typeof(UILabel))
    self.m_ScoreOwnLab  = self.transform:Find("SkillInfoView/Cost/CostType2/ScoreCost/Own/Own"):GetComponent(typeof(UILabel))
    self.m_LearnTip     = self.transform:Find("SkillInfoView/Op/LearnTip"):GetComponent(typeof(UILabel))
    self.m_Scroll       = self.transform:Find("SkillInfoView/Desc/Scroll"):GetComponent(typeof(UIScrollView))
    self.m_LevelUpFx    = self.transform:Find("SkillInfoView/Title/SkillItemTemplate/Fx"):GetComponent(typeof(CUIFx))
    if CommonDefs.IS_VN_CLIENT then
        self.m_SkillDesc    = self.transform:Find("SkillInfoView/Desc/SkillDescScroll/SkillDesc"):GetComponent(typeof(UILabel))
        self.m_SkillDescScroll = self.transform:Find("SkillInfoView/Desc/SkillDescScroll"):GetComponent(typeof(UIScrollView))
    else
        self.m_SkillDesc    = self.transform:Find("SkillInfoView/Desc/SkillDesc"):GetComponent(typeof(UILabel))
    end
    self.m_ManaNeed     = self.transform:Find("SkillInfoView/Op/ManaNeed").gameObject
    self.m_ManaCost     = self.transform:Find("SkillInfoView/Op/ManaNeed/Cost/Cost"):GetComponent(typeof(UILabel))
    self.m_ManaOwn      = self.transform:Find("SkillInfoView/Op/ManaNeed/Own/Own"):GetComponent(typeof(UILabel))
    self.m_Bg           = self.transform:Find("SkillInfoView/Desc/Bg").gameObject
    self.m_NextDescWid  = self.transform:Find("SkillInfoView/Desc/Scroll/NextDesc"):GetComponent(typeof(UIWidget))
    self.m_ScoreCostLab2= self.transform:Find("SkillInfoView/Cost/CostType4/ScoreCost/Cost/Cost"):GetComponent(typeof(UILabel))
    self.m_ScoreOwnLab2 = self.transform:Find("SkillInfoView/Cost/CostType4/ScoreCost/Own/Own"):GetComponent(typeof(UILabel))
    self.m_QnMoneyCtrl2 = self.transform:Find("SkillInfoView/Cost/CostType4/MoneyCost/QnCostAndOwnMoney2"):GetComponent(typeof(CCurentMoneyCtrl))
    self.m_DeductionBtn = self.transform:Find("SkillInfoView/Cost/DeductionBtn"):GetComponent(typeof(QnCheckBox))

    if self.TableViewTran then
        self.TableView = self.TableViewTran:GetComponent(typeof(QnTableView))
    end

    self.OpBtn.OnClick = DelegateFactory.Action_QnButton(function (go)
        local isJoinedSect = CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.SectId ~= 0
        if self.m_CurSkillCls == SoulCore_Settings.GetData().HpSkills then
            if not isJoinedSect then
                local msg = g_MessageMgr:FormatMessage("ZONGMEN_SKILLVIEW_HPSKILL_UPGRADE_NEED_SECT")
                self:TrackToJoinSectNPCWithMsgWnd(msg)
                return
            end
            self:HpSkillCostCheck()
        elseif self.m_CurSkillCls == 0 then
            Gac2Gas.RequestLearnGuanTianXiangSkill(self.m_GTXNextLevel)
        else
            if not isJoinedSect then
                local msg = g_MessageMgr:FormatMessage("ZONGMEN_SKILLVIEW_SHIMENMISHU_UPGRADE_NEED_SECT")
                self:TrackToJoinSectNPCWithMsgWnd(msg)
                return
            end
            self:OnShiMenMishuBtnClicked() 
        end
    end)

    self.TipBtn.OnClick = DelegateFactory.Action_QnButton(function (go)
        if self.TableView then
            g_MessageMgr:ShowMessage("ZONGMEN_SKILLVIEW_TIP")
            return
        end

        if self.m_CurSkillCls == SoulCore_Settings.GetData().HpSkills then
            g_MessageMgr:ShowMessage("ZONGMEN_SKILLVIEW_HPSKILL_TIP")
        elseif self.m_CurSkillCls ~= 0 then
            g_MessageMgr:ShowMessage("ZONGMEN_SKILLVIEW_SHIMENMISHU_TIP")
        end
    end)

    self.SkillItemTemplate:GetComponent(typeof(QnTableItem)).OnSelected = DelegateFactory.Action_GameObject(function (go)
        self:OnSkillIconClick()
    end)

    self.m_DeductionBtn.OnClick = DelegateFactory.Action_QnButton(function(btn)
        if self:GetShiMenMiShuLevel() >= self.m_LevelUnableUseMoneny then
            g_MessageMgr:ShowMessage("SHIMENMISHU_CAN_NOT_USE_MONEY")
            self.m_DeductionBtn.Selected = false
        end
        local consumeType = self.m_DeductionBtn.Selected == true and EnumShiMenMiShuConsumeType.GNJCScoreAndMoney or EnumShiMenMiShuConsumeType.OnlyGNJCScore
        Gac2Gas.ShiMenMiShu_UpdateConsumeType(tonumber(consumeType))
        self:InitSkillInfo(self.m_CurSkillCls)
    end)

    self.m_LevelUnableUseMoneny = SoulCore_Settings.GetData().ShiMenMiShuUseMoneyScale
    self.m_InMana = 0
    self.m_InLearnMana = 0
    self.m_CurSkillLvs = {}
    self.m_ItemCells = {}
    self.m_GuanTianXiangIcon = Menpai_Setting.GetData("GuanTianXiang_IconPath").Value
    if self:GetShiMenMiShuLevel() >= self.m_LevelUnableUseMoneny then
        self.m_DeductionBtn.Selected = false
    else 
        if(CommonDefs.DictContains(CClientMainPlayer.Inst.PlayProp.PersistPlayData, typeof(UInt32), EnumPersistPlayDataKey_lua.eShiMenMiShuSkillConsumeType))then
            self.m_DeductionBtn.Selected = tonumber(CommonDefs.DictGetValue(CClientMainPlayer.Inst.PlayProp.PersistPlayData, typeof(UInt32), EnumPersistPlayDataKey_lua.eShiMenMiShuSkillConsumeType).StringData) == EnumShiMenMiShuConsumeType.GNJCScoreAndMoney
        end
    end

    self.m_SkillDesc.text = g_MessageMgr:FormatMessage("ZONGMEN_SKILLVIEW_SHIMENMISHU_DESC")
end

function LuaZongMenSkillView:Start()
    --mana
    self:OnUpdateMainPlayerSoulCoreMana()

    self.m_DataList = {}
    local class = (CClientMainPlayer.Inst or {}).Class
    local ShiMenMiShuData = SoulCore_ShiMenMiShu.GetData(EnumToInt(class))
    if ShiMenMiShuData ~= nil then
        self.m_ShiMenMiShuCls = ShiMenMiShuData.SkillCls
        table.insert(self.m_DataList, self.m_ShiMenMiShuCls)
    end
    table.insert(self.m_DataList, SoulCore_Settings.GetData().HpSkills)
    -- 0代表观天象
    table.insert(self.m_DataList, 0)

    if self.TableView then
        self.TableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
            self:OnSelectAtRow(row)
        end)

        self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
            function()
                return #self.m_DataList
            end,
            
            function(item, index)
                self:InitItem(item, self.m_DataList[index + 1])
            end)

        self.TableView:ReloadData(true, false)
    end

    local index = LuaZongMenMgr.m_ZongMenSkillViewIndex and LuaZongMenMgr.m_ZongMenSkillViewIndex - 1 or 0
    LuaZongMenMgr.m_ZongMenSkillViewIndex = nil
    if self.TableView then
        self.TableView:SetSelectRow(index, true)
    else
        self:InitSkillInfo(SoulCore_Settings.GetData().HpSkills)
    end
end

function LuaZongMenSkillView:OnEnable()
    g_ScriptEvent:AddListener("UpdateMainPlayerSoulCoreMana", self, "OnUpdateMainPlayerSoulCoreMana")
    g_ScriptEvent:AddListener("UpdateMainPlayerSoulCoreLevel", self, "OnMainPlayerPlayPropUpdate")
    g_ScriptEvent:AddListener("MainPlayerPlayPropUpdate", self, "OnMainPlayerPlayPropUpdate")
    g_ScriptEvent:AddListener("MainPlayerSkillPropUpdate", self, "OnMainPlayerSkillPropUpdate")
    g_ScriptEvent:AddListener("UpdatePassiveSkillSetLevel", self, "OnUpdatePassiveSkillSetLevel")
    g_ScriptEvent:AddListener("LearnGuanTianXiangSkillSuccess", self, "OnLearnGuanTianXiangSkillSuccess")
end

function LuaZongMenSkillView:OnDisable()
    g_ScriptEvent:RemoveListener("UpdateMainPlayerSoulCoreMana", self, "OnUpdateMainPlayerSoulCoreMana")
    g_ScriptEvent:RemoveListener("UpdateMainPlayerSoulCoreLevel", self, "OnMainPlayerPlayPropUpdate")
    g_ScriptEvent:RemoveListener("MainPlayerPlayPropUpdate", self, "OnMainPlayerPlayPropUpdate")
    g_ScriptEvent:RemoveListener("MainPlayerSkillPropUpdate", self, "OnMainPlayerSkillPropUpdate")
    g_ScriptEvent:RemoveListener("UpdatePassiveSkillSetLevel", self, "OnUpdatePassiveSkillSetLevel")
    g_ScriptEvent:RemoveListener("LearnGuanTianXiangSkillSuccess", self, "OnLearnGuanTianXiangSkillSuccess")

    self.m_LevelUpFx:DestroyFx()
end

function LuaZongMenSkillView:OnUpdateMainPlayerSoulCoreMana()
    local mainPlayer = (CClientMainPlayer.Inst or {})
    local soulCoreProp = (mainPlayer.SkillProp or {}).SoulCore
    local mana = soulCoreProp.Mana
    self.Own.text = mana

    if self.m_InMana <= mana then
        self.Own.text = mana
    else
        self.Own.text = SafeStringFormat3("[c][ff0000]%s[-][/c]", mana)
    end

    if self.m_InLearnMana <= mana then
        self.m_ManaOwn.text = mana
    else
        self.m_ManaOwn.text = SafeStringFormat3("[c][ff0000]%s[-][/c]", mana)
    end
end

function LuaZongMenSkillView:OnMainPlayerPlayPropUpdate()
    self:InitSkillInfo(self.m_CurSkillCls)
end
-- SkillProp升级回调
function LuaZongMenSkillView:OnMainPlayerSkillPropUpdate()
    if self.TableView then
        self.TableView:ReloadData(false, false)
    end
    self:InitSkillInfo(self.m_CurSkillCls, true)
end
-- 长生诀升级回调
function LuaZongMenSkillView:OnUpdatePassiveSkillSetLevel(SkillType, Index, Level)
    if SkillType == 1 and Index == 1 then
        if self.TableView then
            self.TableView:ReloadData(false, false)
        end
        self:InitHpSkillInfo(self.m_CurSkillCls, true)
    end
end
-- 观天象升级回调
function LuaZongMenSkillView:OnLearnGuanTianXiangSkillSuccess()
    if self.TableView then
        self.TableView:ReloadData(false, false)
    end
    self:InitSkillInfo(self.m_CurSkillCls)
end

function LuaZongMenSkillView:OnShiMenMishuBtnClicked()
    local skillProperty = CClientMainPlayer.Inst.SkillProp
    if skillProperty:IsOriginalSkillClsExist(self.m_CurSkillCls) then
        if self.m_DeductionBtn.Selected == true then
            self:ShiMenMiShuCostCheck()
        else
            self:ShiMenMishuLevelup()
        end
    else
        --学习
        Gac2Gas.RequestLearnShiMenMiShuSkill()
    end
end

function LuaZongMenSkillView:ShiMenMishuLevelup()
    local selected = self.m_DeductionBtn.Selected
    local skillProperty = CClientMainPlayer.Inst.SkillProp
    --升级
    local skillId = skillProperty:GetOriginalSkillIdByCls(self.m_CurSkillCls)

    local extraInfo = CreateFromClass(MakeGenericClass(Dictionary, cs_string, Object))
    if selected == true then
        CommonDefs.DictAdd_LuaCall(extraInfo, "ConsumeType", EnumShiMenMiShuConsumeType.GNJCScoreAndMoney)
    else
        CommonDefs.DictAdd_LuaCall(extraInfo, "ConsumeType", EnumShiMenMiShuConsumeType.OnlyGNJCScore)
    end

    Gac2Gas.RequestUpgradeNormalSkill(skillId, MsgPackImpl.pack(extraInfo))
end

function LuaZongMenSkillView:ShiMenMiShuCostCheck()

    self:ShiMenMishuLevelup()
end

function LuaZongMenSkillView:HpSkillCostCheck()
    local freeSilver = CClientMainPlayer.Inst.FreeSilver
    local silver = CClientMainPlayer.Inst.Silver
    local cost = self.m_MoneyCostAuto.cost
    local freeEnough = freeSilver >= cost
    local totalEnough = silver + freeSilver >= cost
    if totalEnough and not freeEnough and freeSilver ~= 0 then
        local silverCost = cost - freeSilver
        MessageWndManager.ShowOKCancelMessage(SafeStringFormat3(LocalString.GetString("升级所需银票不足，是否消耗%d银两进行补足？"), silverCost), DelegateFactory.Action(function ()
            Gac2Gas.UpgradeHpSkill()
        end), nil, nil, nil, false)
    else 
        Gac2Gas.UpgradeHpSkill()
    end
end

function LuaZongMenSkillView:UpdateExp()
    if CClientMainPlayer.Inst == nil then
        return
    end

    local freeSilver = CClientMainPlayer.Inst.FreeSilver
    --优先消耗银票
    if self.m_MoneyCostAuto ~= self.m_QnMoneyCtrl2 then
        if freeSilver > 0 then
            self.m_MoneyCostAuto:SetType(EnumMoneyType_lua.YinPiao, 0, false)
        else
            self.m_MoneyCostAuto:SetType(EnumMoneyType_lua.YinLiang, 0, false)
        end
    end

    self.m_QnMoneyCtrl2:SetType(EnumMoneyType_lua.YinLiang, 0, false)

    --经验
    local exp = CClientMainPlayer.Inst.PlayProp.Exp
    local skillPrivateExp = CClientMainPlayer.Inst.SkillProp.SkillPrivateExp
    local costExp = self.m_QnExpCtrl.cost
    local ownStr = ""
    --优先消耗专有经验 得手动更新下当前经验
    if skillPrivateExp > 0 then
        self.m_QnExpCtrl:SetType(EnumMoneyType_lua.PrivateExp, 0, false)
        ownStr = tostring(skillPrivateExp)
    else
        self.m_QnExpCtrl:SetType(EnumMoneyType_lua.Exp, 0, false)
        ownStr = tostring(exp)
    end

    if costExp <= skillPrivateExp + exp then
        self.m_QnExpCtrl.m_OwnLabel.text = ownStr
    else
        self.m_QnExpCtrl.m_OwnLabel.text = SafeStringFormat3("[c][ff0000]%s[-][/c]", ownStr)
    end
end

function LuaZongMenSkillView:InitItem(item, skillClass)
    if CClientMainPlayer.Inst == nil then
        return
    end

    if skillClass == 0 then
        self:InitGuanTianXiangItem(item, skillClass)
    elseif skillClass == SoulCore_Settings.GetData().HpSkills then
        self:InitHpItem(item, skillClass)
    else
        self:InitShiMenMiShuItem(item, skillClass)
    end
end

function LuaZongMenSkillView:InitGuanTianXiangItem(item, skillClass)
    local cell = CommonDefs.GetComponent_GameObject_Type(item.gameObject, typeof(CSkillItemCell))
    local nameLab = item.transform:Find("SkillName"):GetComponent(typeof(UILabel))

    self.m_ItemCells[skillClass] = cell
    nameLab.text = LocalString.GetString("观天象")
    local clsExists = CommonDefs.DictContains(CClientMainPlayer.Inst.PlayProp.PersistPlayData, typeof(UInt32), EnumPersistPlayDataKey_lua.eGuanTianXiangSkillData)
    local playData = clsExists and CClientMainPlayer.Inst.PlayProp.PersistPlayData[EnumPersistPlayDataKey_lua.eGuanTianXiangSkillData] or nil
    local skillLevel = playData and tonumber(playData.StringData) or 0
    local originLevel = skillLevel

    cell:InitFeiSheng(false, 0)
    --TODO 图标
    cell:Init(skillClass, originLevel, 0, false, self.m_GuanTianXiangIcon, originLevel == 0, nil)
    cell.ShowPassiveIcon = false
end

function LuaZongMenSkillView:InitHpItem(item, skillClass)
    local cell = CommonDefs.GetComponent_GameObject_Type(item.gameObject, typeof(CSkillItemCell))
    local nameLab = item.transform:Find("SkillName"):GetComponent(typeof(UILabel))
    
    self.m_ItemCells[skillClass] = cell
    local originLevel = CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.SkillProp.HpSkillLevel, 1) and CClientMainPlayer.Inst.SkillProp.HpSkillLevel[1] or 0
    local skillData = Skill_AllSkills.GetData(skillClass * 100 + 1)
    nameLab.text = skillData.Name

    if CClientMainPlayer.Inst.IsInXianShenStatus then
        local feishengLevel = LuaZongMenMgr:GetAdjustHpSkillLevel(originLevel, CClientMainPlayer.Inst.Level)
        cell:InitFeiSheng(true, feishengLevel)
    else
        cell:InitFeiSheng(false, 0)
    end

    cell:Init(skillClass, originLevel, 0, false, skillData.SkillIcon, originLevel == 0, nil)
    cell.ShowPassiveIcon = false
end

function LuaZongMenSkillView:InitShiMenMiShuItem(item, skillClass)
    local cell = CommonDefs.GetComponent_GameObject_Type(item.gameObject, typeof(CSkillItemCell))
    local nameLab = item.transform:Find("SkillName"):GetComponent(typeof(UILabel))

    self.m_ItemCells[skillClass] = cell
    local skillProperty = CClientMainPlayer.Inst.SkillProp
    local data = Skill_AllSkills.GetData(skillClass * 100 + 1)
    nameLab.text = data.Name

    local clsExists = skillProperty:IsOriginalSkillClsExist(skillClass)
    local originSkillId = clsExists and skillProperty:GetOriginalSkillIdByCls(skillClass) or 0
    local originLevel = clsExists and CLuaDesignMgr.Skill_AllSkills_GetLevel(originSkillId) or 0
    local playerLevel = CClientMainPlayer.Inst.Level
    local feishengLevel = clsExists and LuaZongMenMgr:GetPlayerAdjustShiMenMiShuSkillLv(originSkillId, playerLevel, skillProperty) or 0

    if CClientMainPlayer.Inst.IsInXianShenStatus then
        cell:InitFeiSheng(true, feishengLevel)
    else
        cell:InitFeiSheng(false, 0)
    end
    cell:Init(skillClass, originLevel, 0, true, data.SkillIcon, originLevel == 0, nil)
    --cell.ShowPassiveIcon = false
end

function LuaZongMenSkillView:GetShiMenMiShuLevel()
    local skillClass = self.m_ShiMenMiShuCls
    local skillProperty = CClientMainPlayer.Inst.SkillProp
    local clsExists = skillProperty:IsOriginalSkillClsExist(skillClass)
    local originSkillId = clsExists and skillProperty:GetOriginalSkillIdByCls(skillClass) or 0
    local originLevel = clsExists and CLuaDesignMgr.Skill_AllSkills_GetLevel(originSkillId) or 0
    return originLevel
end

function LuaZongMenSkillView:OnSelectAtRow(row) 
    local skillClass = self.m_DataList[row+1]
    local itemCell = self.TableView:GetItemAtRow(row):GetComponent(typeof(CSkillItemCell))
    
    for i = 1, #self.m_DataList do
        if self.m_DataList[i] ~= skillClass then
            local otherItemCell = self.TableView:GetItemAtRow(i-1):GetComponent(typeof(CSkillItemCell))
            if otherItemCell ~= nil then
                otherItemCell.Selected = false
            end
        end
    end

    if itemCell ~= nil then
        itemCell.Selected = true
        self:InitSkillInfo(skillClass)
    end
end

function LuaZongMenSkillView:InitSkillInfo(skillClass, bTryToPlayLvUpSound)
    self.m_CurSkillCls = skillClass
    
    if skillClass == 0 then
        self:InitGuanTianXiangSkillInfo(skillClass, bTryToPlayLvUpSound)
    elseif skillClass == SoulCore_Settings.GetData().HpSkills then
        self:InitHpSkillInfo(skillClass, bTryToPlayLvUpSound)
    else
        self:InitShiMenMiShuSkillInfo(skillClass, bTryToPlayLvUpSound)
    end
end

function LuaZongMenSkillView:InitGuanTianXiangSkillInfo(skillClass, bTryToPlayLvUpSound)
    if CClientMainPlayer.Inst == nil then
        return
    end
    self:SwitchType(3)

    local clsExists = CommonDefs.DictContains(CClientMainPlayer.Inst.PlayProp.PersistPlayData, typeof(UInt32), EnumPersistPlayDataKey_lua.eGuanTianXiangSkillData)
    local playData = clsExists and CClientMainPlayer.Inst.PlayProp.PersistPlayData[EnumPersistPlayDataKey_lua.eGuanTianXiangSkillData] or nil
    local skillLevel = playData and tonumber(playData.StringData) or 0
    
    local nextSkillLevel =  skillLevel + 1
    clsExists = skillLevel ~= 0
    local originLevel = skillLevel
    local feishengLevel = originLevel
    local playerLevel = CClientMainPlayer.Inst.Level
    local originSkillId = 0

    local displayStr = CChatLinkMgr.TranslateToNGUIText(Menpai_Setting.GetData("GuanTianXiangSkillDesc").Value)
    local curData = Menpai_GuanTianXiang.GetData(skillLevel)
    local levelMax = Menpai_GuanTianXiang.GetDataCount()
    local name = LocalString.GetString("观天象")
    local icon = self.m_GuanTianXiangIcon

    local curSkillData = {SkillIcon = icon, Name = name, Display = curData and SafeStringFormat3(displayStr, curData.Hours, SafeStringFormat3("%d%%", curData.Effect * 100)) or "", _Before_Extend_Level = levelMax}
    local nextSkillData = nil
    local playerLvNeed = 0
    local soulCoreLvNeed = 0
    
    self.CostTip.text = ""
    local data = Menpai_GuanTianXiang.GetData(nextSkillLevel)
    if data ~= nil then
        self.m_QnExpCtrl:SetCost(data.Exp)
        self.m_MoneyCostAuto:SetCost(data.Money)
        self:UpdateExp()
        self.CostTip.text = self:GetExpAndMoneyTip(data.Exp, data.Money)
        soulCoreLvNeed = data.SoulCoreLevel
        self.m_GTXNextLevel = nextSkillLevel
        nextSkillData = {SkillIcon = icon, Name = name, Display = SafeStringFormat3(displayStr, data.Hours, SafeStringFormat3("%d%%", data.Effect * 100)), _Before_Extend_Level = levelMax}
    end

    if self.m_CurSkillLvs[skillClass] == nil then
        self.m_CurSkillLvs[skillClass] = originLevel
    end
    if bTryToPlayLvUpSound == true then
        if originLevel > self.m_CurSkillLvs[skillClass] then
            SoundManager.Inst:PlayOneShot(SoundManager.UISoundEvents.Skill_Update, Vector3.zero, nil, 0)
            self.m_LevelUpFx:DestroyFx()
            self.m_LevelUpFx:LoadFx("fx/ui/prefab/UI_zhiyekefu_tupo.prefab")
        end
        self.m_CurSkillLvs[skillClass] = originLevel
    end

    self:InitSkillInfoCommon(clsExists, curSkillData, nextSkillData, playerLvNeed, soulCoreLvNeed, originSkillId,  originLevel, feishengLevel, 3)
end

function LuaZongMenSkillView:InitHpSkillInfo(skillClass, bTryToPlayLvUpSound)
    if CClientMainPlayer.Inst == nil then
        return
    end
    self:SwitchType(1)

    local originLevel = CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.SkillProp.HpSkillLevel, 1) and CClientMainPlayer.Inst.SkillProp.HpSkillLevel[1] or 0
    local originSkillId = skillClass * 100 + originLevel
    local clsExists = originLevel ~= 0

    if not clsExists then
        originSkillId = skillClass * 100 
    end
    
    local curSkillData = Skill_AllSkills.GetData(originSkillId)
    local nextSkillData = Skill_AllSkills.GetData(originSkillId + 1)

    local feishengLevel = clsExists and LuaZongMenMgr:GetAdjustHpSkillLevel(originLevel, CClientMainPlayer.Inst.Level) or 0
    local soulCoreLvNeed = 0
    local playerLvNeed = 0
    local nextHpData = SoulCore_HpSkills.GetData(originSkillId + 1)
    self.CostTip.text = ""
    if nextHpData ~= nil then
        --BangGong
        self.QnCostAndOwnMoney1:SetCost(nextHpData.InContribution)
        self:UpdateExp()
        --money
        self.m_MoneyCostAuto:SetCost(nextHpData.InMoney)
        --mana
        self.Cost.text = nextHpData.InSoulCoreMana
        self.m_InMana = nextHpData.InSoulCoreMana
        self:OnUpdateMainPlayerSoulCoreMana() --更新一下颜色

        self.CostTip.text = self:GetExpAndMoneyTip(0, nextHpData.InMoney)

        soulCoreLvNeed = nextHpData.SoulCoreLearnLv
        playerLvNeed = nextHpData.PlayerLearnLv
    end

    if self.m_CurSkillLvs[skillClass] == nil then
        self.m_CurSkillLvs[skillClass] = originLevel
    end
    if bTryToPlayLvUpSound == true then
        if originLevel > self.m_CurSkillLvs[skillClass] then
            SoundManager.Inst:PlayOneShot(SoundManager.UISoundEvents.Skill_Update, Vector3.zero, nil, 0)
            self.m_LevelUpFx:DestroyFx()
            self.m_LevelUpFx:LoadFx("fx/ui/prefab/UI_zhiyekefu_tupo.prefab")
        end
        self.m_CurSkillLvs[skillClass] = originLevel
    end

    self:InitSkillInfoCommon(clsExists, curSkillData, nextSkillData, playerLvNeed, soulCoreLvNeed, originSkillId, originLevel, feishengLevel, 1)
end

function LuaZongMenSkillView:InitShiMenMiShuSkillInfo(skillClass, bTryToPlayLvUpSound)
    if CClientMainPlayer.Inst == nil then
        return
    end
    --self:SwitchType(self.m_DeductionBtn.Selected == true and 4 or 2) 需要通过originLevel控制Selected,下移

    local skillProperty = CClientMainPlayer.Inst.SkillProp
    local data = Skill_AllSkills.GetData(skillClass * 100 + 1)
    local clsExists = skillProperty:IsOriginalSkillClsExist(skillClass)

    local originSkillId = clsExists and skillProperty:GetOriginalSkillIdByCls(skillClass) or 0
    local originLevel = clsExists and CLuaDesignMgr.Skill_AllSkills_GetLevel(originSkillId) or 0
    local playerLevel = CClientMainPlayer.Inst.Level
    local feishengLevel = clsExists and LuaZongMenMgr:GetPlayerAdjustShiMenMiShuSkillLv(originSkillId, playerLevel, skillProperty) or 0

    if not clsExists then
        originSkillId = skillClass * 100 
    end
    
    local nextSkillId = originSkillId + 1
    local nextSkillLv = originLevel + 1
    local curSkillData = Skill_AllSkills.GetData(originSkillId)
    local nextSkillData = Skill_AllSkills.GetData(nextSkillId)

    if originLevel >= self.m_LevelUnableUseMoneny then
        self.m_DeductionBtn.Selected = false
    end
    self:SwitchType(self.m_DeductionBtn.Selected == true and 4 or 2)
    self.m_DeductionBtn.gameObject:SetActive(clsExists)
    local scoreNeed, playerLvNeed, soulCoreLvNeed, scoreOwn = 0, 0, 0, 0
    if nextSkillData ~= nil then
        if self.m_DeductionBtn.Selected == true then
            scoreNeed = self:GetShiMenMiShuSkillScore2Need(nextSkillLv)
            self.m_QnMoneyCtrl2:SetCost(self:GetShiMenMiShuSkillMoney2Need(nextSkillLv))
            self:UpdateExp()
        else
            scoreNeed = self:GetShiMenMiShuSkillScoreNeed(nextSkillLv)
        end
        playerLvNeed = self:GetShiMenMiShuSkillLvNeed(nextSkillLv)
        soulCoreLvNeed = self:GetShiMenMiShuSkillSoulCoreLvNeed(nextSkillLv)
        scoreOwn = CClientMainPlayer.Inst.PlayProp:GetScoreByKey(EnumPlayScoreKey.GNJC)

        self.CostTip.text = ""
        self.m_ScoreCostLabAuto.text = scoreNeed
        if scoreNeed <= scoreOwn then
            self.m_ScoreOwnLabAuto.text = scoreOwn
        else
            self.m_ScoreOwnLabAuto.text = SafeStringFormat3("[c][ff0000]%s[-][/c]", scoreOwn)
        end
    end

    if self.m_CurSkillLvs[skillClass] == nil then
        self.m_CurSkillLvs[skillClass] = originLevel
    end
    if bTryToPlayLvUpSound == true then
        if originLevel > self.m_CurSkillLvs[skillClass] then
            SoundManager.Inst:PlayOneShot(SoundManager.UISoundEvents.Skill_Update, Vector3.zero, nil, 0)
            self.m_LevelUpFx:DestroyFx()
            self.m_LevelUpFx:LoadFx("fx/ui/prefab/UI_zhiyekefu_tupo.prefab")
        end
        self.m_CurSkillLvs[skillClass] = originLevel
    end

    self:InitSkillInfoCommon(clsExists, curSkillData, nextSkillData, playerLvNeed, soulCoreLvNeed, originSkillId,  originLevel, feishengLevel, 2)
end

function LuaZongMenSkillView:InitSkillInfoCommon(clsExists, curSkillData, nextSkillData, playerLvNeed, soulCoreLvNeed, originSkillId, originLevel, feishengLevel, type)
    local isShiMenMiShu = type == 2
    local isHpSkill = type == 1
    self.m_CurDesc:SetActive(true)
    self.m_CurDescCollider:SetActive(true)
    self.m_NextDesc:SetActive(true)
    self.m_Bg:SetActive(true)
    self.m_SkillDesc.gameObject:SetActive(false)
    self.m_ManaNeed:SetActive(false)
    Extensions.SetLocalPositionY(self.LvNeed.transform, (isShiMenMiShu or isHpSkill) and 20 or 0)
    Extensions.SetLocalPositionY(self.SoulCoreLvNeed.transform, (isShiMenMiShu or isHpSkill) and -20 or 0)

    local soulCoreProp = CClientMainPlayer.Inst.SkillProp.SoulCore
    local curSoulCoreLvStr = soulCoreProp.Level < soulCoreLvNeed and SafeStringFormat3("[c][ff0000]%s[-][/c]", soulCoreProp.Level) or soulCoreProp.Level
    self.SoulCoreLvNeedLab.text = SafeStringFormat3(LocalString.GetString("%s/%s级"), curSoulCoreLvStr, soulCoreLvNeed)

    local playerLv = CClientMainPlayer.Inst.MaxLevel
    local curPlayerLvStr = playerLv < playerLvNeed and SafeStringFormat3("[c][ff0000]%s[-][/c]", playerLv) or playerLv
    self.LvNeedLab.text = SafeStringFormat3(LocalString.GetString("%s/%s级"), curPlayerLvStr, playerLvNeed)
    
    self.SkillItemTemplate.skillIcon:LoadSkillIcon(clsExists and curSkillData.SkillIcon or nextSkillData.SkillIcon)
    if not clsExists then
        -- 未习得
        if isShiMenMiShu then
            self.m_CurDesc:SetActive(false)
            self.m_CurDescCollider:SetActive(false)
            self.m_Bg:SetActive(false)
            
            self.m_SkillDesc.gameObject:SetActive(true)
            if self.m_SkillDescScroll then
                self.m_SkillDescScroll:ResetPosition()
            end
            self.m_CostType2:SetActive(false)
            self.m_CostType4:SetActive(false)

            self.m_ManaNeed:SetActive(true)
            self.m_InLearnMana = self:GetShiMenMiShuSkillLearnManaNeed()
            self.m_ManaCost.text = self.m_InLearnMana
            self:OnUpdateMainPlayerSoulCoreMana()
            self.SkillTitleLab.text = LocalString.GetString("师门秘术")
        else
            self.SkillTitleLab.text = SafeStringFormat3("%s", nextSkillData.Name)
        end
        self.SoulCoreLvNeed:SetActive(not isShiMenMiShu)
        self.m_NextDesc:SetActive(not isShiMenMiShu)

        self.LvNeed:SetActive(isHpSkill)
        self.m_LearnTip.gameObject:SetActive(false)
        self.OpBtn.Text = LocalString.GetString("学  习")

        self.OpBtn.Enabled = true
        self.MaxLevelLab.gameObject:SetActive(false)
        self.NoLab.gameObject:SetActive(true)
        self.CurDescLab.text = ""
        self.FeiShengTipLab.text = ""
        self.m_AllCost:SetActive(true)
        self.NextDescLab.text = CChatLinkMgr.TranslateToNGUIText(nextSkillData.Display .. (isHpSkill and LuaZongMenMgr:GetHpSkillExtraDisplay(nextSkillData) or ""), false)
    else
        -- 已习得,未满级
        self.MaxLevelLab.gameObject:SetActive(true)
        self.MaxLevelLab.text = SafeStringFormat3("%slv.%s", LocalString.GetString("等级上限"), curSkillData._Before_Extend_Level)
        self.NoLab.gameObject:SetActive(false)
        self.CurDescLab.text = CChatLinkMgr.TranslateToNGUIText(curSkillData.Display .. (isHpSkill and LuaZongMenMgr:GetHpSkillExtraDisplay(curSkillData) or ""), false)
        self.m_LearnTip.gameObject:SetActive(false)

        if CClientMainPlayer.Inst.IsInXianShenStatus then
            if feishengLevel ~= originLevel then
                self.SkillTitleLab.text = SafeStringFormat3("%s [c][%s]lv.%d[-][/c]", curSkillData.Name, Constants.ColorOfFeiSheng, feishengLevel)
                self.FeiShengTipLab.text = SafeStringFormat3("%s lv.%d", LocalString.GetString("飞升前等级"), originLevel)
            else 
                self.SkillTitleLab.text = SafeStringFormat3("%s lv.%d", curSkillData.Name, originLevel)
                self.FeiShengTipLab.text = ""
            end
        else
            self.SkillTitleLab.text = SafeStringFormat3("%s lv.%d", curSkillData.Name, originLevel)
            self.FeiShengTipLab.text = ""
        end
        
        if nextSkillData ~= nil then
            if isShiMenMiShu then
                self.LvNeed:SetActive(true)
                self.SoulCoreLvNeed:SetActive(true)
            elseif isHpSkill then
                self.LvNeed:SetActive(true)
                self.SoulCoreLvNeed:SetActive(true)
            else 
                self.LvNeed:SetActive(false)
                self.SoulCoreLvNeed:SetActive(true)
            end

            self.m_AllCost:SetActive(true)
            self.OpBtn.Text = LocalString.GetString("升  级")
            self.OpBtn.Enabled = true
            self.m_NextDesc:SetActive(true)
            self.NextDescLab.text = CChatLinkMgr.TranslateToNGUIText(nextSkillData.Display .. (isHpSkill and LuaZongMenMgr:GetHpSkillExtraDisplay(nextSkillData) or ""), false)
        else
            -- 满级
            self.LvNeed:SetActive(false)
            self.SoulCoreLvNeed:SetActive(false)

            self.m_AllCost:SetActive(false)
            self.OpBtn.Text = LocalString.GetString("已满级")
            self.OpBtn.Enabled = false
            self.m_NextDesc:SetActive(false)
            self.NextDescLab.text = ""
        end
    end

    self.m_Scroll:ResetPosition()
    self.m_NextDescWid:ResetAndUpdateAnchors()
end

function LuaZongMenSkillView:SwitchType(type)
    self.m_CostType1:SetActive(type == 1)
    self.m_CostType2:SetActive(type == 2)
    self.m_CostType3:SetActive(type == 3)
    self.m_CostType4:SetActive(type == 4)
    self.m_DeductionBtn.gameObject:SetActive(type == 2 or type == 4)

    if type == 1 then
        self.m_MoneyCostAuto = self.QnCostAndOwnMoney2
    elseif type == 2 then
        self.m_ScoreCostLabAuto = self.m_ScoreCostLab
        self.m_ScoreOwnLabAuto = self.m_ScoreOwnLab
    elseif type == 3 then
        self.m_MoneyCostAuto = self.m_QnMoneyCtrl
    elseif type == 4 then
        self.m_ScoreCostLabAuto = self.m_ScoreCostLab2
        self.m_ScoreOwnLabAuto = self.m_ScoreOwnLab2

        self.m_MoneyCostAuto = self.m_QnMoneyCtrl2
    end
end

function LuaZongMenSkillView:GetExpAndMoneyTip(needExp, needMoney)
    local skillPrivateExp = CClientMainPlayer.Inst.SkillProp.SkillPrivateExp
    local exp = CClientMainPlayer.Inst.PlayProp.Exp

    local freeSilver = CClientMainPlayer.Inst.FreeSilver
    local silver = CClientMainPlayer.Inst.Silver

    local expEnough = (skillPrivateExp + exp) >= needExp
    local moneyEnough = (freeSilver + silver >= 0) and ((freeSilver + silver) >= needMoney)

    if expEnough and moneyEnough then
        local icons1 = ""
        local icons2 = ""
        --专有经验，前世经验、个人经验
        local privateExpNotEnough = (skillPrivateExp > 0 and skillPrivateExp < needExp)
        
        if skillPrivateExp ~= 0 then
            --显示专有经验图标
            if skillPrivateExp < needExp then
                icons1 = icons1 .. "#291"
                icons2 = icons2 .. "#290"
                --个人经验图标
            end
        end

        if (freeSilver > 0 and freeSilver < needMoney) then
            icons1 = icons1 .. "#288"
            icons2 = icons2 .. "#287"
        end
        --银两图标

        if not System.String.IsNullOrEmpty(icons2) then
            return SafeStringFormat3(LocalString.GetString("%s不足的部分将从%s中补足"), icons1, icons2)
        end
    elseif expEnough and not moneyEnough then
        return LocalString.GetString("[c][ff5050]#288不足的部分无法从#287中补足[-][/c]")
    else
        return LocalString.GetString("[c][ff5050]#291#288不足的部分无法从#290或#287中补足[-][/c]")
    end

    return ""
end

function LuaZongMenSkillView:OnSkillIconClick()
    -- 秘术未学习时看不到技能说明,提供点击图标显示技能说明
    if self.m_CurSkillCls == SoulCore_Settings.GetData().HpSkills or self.m_ItemCells[self.m_CurSkillCls].OriginWithDeltaLevel ~= 0 then
        return 
    end

    if self.m_CurSkillCls ~= nil then
        CSkillInfoMgr.ShowSkillInfoWnd(CLuaDesignMgr.Skill_AllSkills_GetSkillId(
        self.m_ItemCells[self.m_CurSkillCls].ClassId,
        (self.m_ItemCells[self.m_CurSkillCls].OriginWithDeltaLevel == 0) and 1 or self.m_ItemCells[self.m_CurSkillCls].OriginWithDeltaLevel),
        nil, true, self.SkillItemTemplate.transform.position,
        CSkillInfoMgr.EnumSkillInfoContext.MainPlayerSkillPreference, true, 0, 0, nil)
    end
end

function LuaZongMenSkillView:GetShiMenMiShuSkillLvNeed(skillLv)
    return SoulCore_ShiMenMiShuLevel.GetData(skillLv).PlayerLevel
end

function LuaZongMenSkillView:GetShiMenMiShuSkillSoulCoreLvNeed(skillLv)
    return SoulCore_ShiMenMiShuLevel.GetData(skillLv).SoulCoreLevel
end

function LuaZongMenSkillView:GetShiMenMiShuSkillScoreNeed(skillLv)
    return SoulCore_ShiMenMiShuLevel.GetData(skillLv).GuanNingScore
end

function LuaZongMenSkillView:GetShiMenMiShuSkillScore2Need(skillLv)
    return SoulCore_ShiMenMiShuLevel.GetData(skillLv).GuanNingScore_2
end

function LuaZongMenSkillView:GetShiMenMiShuSkillMoney2Need(skillLv)
    return SoulCore_ShiMenMiShuLevel.GetData(skillLv).Money_2
end

function LuaZongMenSkillView:GetShiMenMiShuSkillLearnManaNeed()
    return SoulCore_ShiMenMiShuLevel.GetData(1).SoulCoreMana
end

function LuaZongMenSkillView:GetGuanTianXiangDisplay(level)
    local str = Menpai_Setting.GetData("GuanTianXiangSkillDesc").Value
    Menpai_GuanTianXiang.GetData(level)
end

function LuaZongMenSkillView:TrackToJoinSectNPCWithMsgWnd(msg)
    MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function ()
        CUIManager.CloseUI(CUIResources.SkillWnd)
        local npcId = tonumber(Menpai_Setting.GetData("YuanYiXianShengID").Value)
        local sceneData = g_LuaUtil:StrSplit(Menpai_Setting.GetData("YuanYiXianShengXunLu").Value,",")
        CTrackMgr.Inst:FindNPC(npcId, tonumber(sceneData[1]), tonumber(sceneData[2]), tonumber(sceneData[3]), nil, nil)
    end), nil, nil, nil, false)
end