require("common/common_include")

local TweenAlpha = import "TweenAlpha"
local Constants = import "L10.Game.Constants"
local Rect = import "UnityEngine.Rect"
local Camera = import "UnityEngine.Camera"
local CUICommonDef = import "L10.UI.CUICommonDef"


LuaAutoTakingPhotoWnd = class()

RegistClassMember(LuaAutoTakingPhotoWnd, "CaptureTexture")
RegistClassMember(LuaAutoTakingPhotoWnd, "CountDownLabel")
RegistClassMember(LuaAutoTakingPhotoWnd, "WhiteTexture")
RegistClassMember(LuaAutoTakingPhotoWnd, "Corner")

RegistClassMember(LuaAutoTakingPhotoWnd, "CountDownTick")
RegistClassMember(LuaAutoTakingPhotoWnd, "CountDownTime")
RegistClassMember(LuaAutoTakingPhotoWnd, "CurrentTime")
RegistClassMember(LuaAutoTakingPhotoWnd, "IsWinSocialWndOpened")

function LuaAutoTakingPhotoWnd:Init()
	self:InitClassMembers()
	self:InitValues()

	-- 应策划要求，关闭购买商品界面
	if CUIManager.IsLoaded(CUIResources.NPCShopWnd) then
		CUIManager.CloseUI(CUIResources.NPCShopWnd)
	end

	if CUIManager.IsLoaded(CLuaUIResources.NPCShopWnd2) then
		CUIManager.CloseUI(CLuaUIResources.NPCShopWnd2)
	end

end

function LuaAutoTakingPhotoWnd:InitClassMembers()
	self.CaptureTexture = self.transform:Find("CaptureTexture").gameObject
	self.CountDownLabel = self.transform:Find("Camera/Corner/CountDownLabel"):GetComponent(typeof(UILabel))
	self.CountDownLabel.gameObject:SetActive(true)
	self.WhiteTexture = self.transform:Find("WhiteTexture"):GetComponent(typeof(TweenAlpha))
	self.Corner = self.transform:Find("Camera/Corner").gameObject
	self.Corner:SetActive(true)

	self.WhiteTexture:SetEndToCurrentValue()
end

function LuaAutoTakingPhotoWnd:InitValues()
	self.CountDownTime = 5
	self.CurrentTime = self.CountDownTime
	self.IsWinSocialWndOpened = false

	self:AdjustScreeen()

	if self.CountDownTick ~= nil then
      UnRegisterTick(self.CountDownTick)
      self.CountDownTick=nil
    end

	self.CountDownTick = RegisterTickWithDuration(function ()
		if self.CurrentTime > 1 then
			self.CurrentTime = self.CurrentTime - 1
			self.CountDownLabel.text = tostring(self.CurrentTime)
		else
			self:CaptureScreen()
		end
	end, 1000, 1000 * (self.CountDownTime+1))
end

function LuaAutoTakingPhotoWnd:CaptureScreen()
	CommonDefs.AddEventDelegate(self.WhiteTexture.onFinished, DelegateFactory.Action(function ()
		CUIManager.instance:CloseAllPopupViews()
		CUICommonDef.CaptureScreenAndSave()
		CUIManager.CloseUI(CLuaUIResources.AutoTakingPhotoWnd)
	end))

	self.Corner:SetActive(false)
	self.CountDownLabel.gameObject:SetActive(false)
	self.WhiteTexture:ResetToBeginning()
	self.WhiteTexture:PlayForward()
end

function LuaAutoTakingPhotoWnd:OnEnable()
	
end

function LuaAutoTakingPhotoWnd:OnDisable()
	
end

function LuaAutoTakingPhotoWnd:AdjustScreeen()
	if CommonDefs.IsPCGameMode() then
		local CWinSocialWndMgr = import "L10.UI.CWinSocialWndMgr"
		local camera = CUICommonDef.TraverseFindChild("Camera", self.transform):GetComponent(typeof(Camera))
		if camera then
			if self.IsWinSocialWndOpened ~= CWinSocialWndMgr.Inst.m_IsWinSocialWndOpened then
				self.IsWinSocialWndOpened = CWinSocialWndMgr.Inst.m_IsWinSocialWndOpened
				if CWinSocialWndMgr.Inst.m_IsWinSocialWndOpened then
					local rect = Rect(0, 0, 1-Constants.WinSocialWndRatio, 1)
					camera.rect = rect
				else
					local rect = Rect(0, 0, 1, 1)
					camera.rect = rect
				end
			end
		end
	end
end
function LuaAutoTakingPhotoWnd:Update()
	self:AdjustScreeen()
end

function LuaAutoTakingPhotoWnd:OnDestroy()
	if self.CountDownTick ~= nil then
      UnRegisterTick(self.CountDownTick)
      self.CountDownTick=nil
    end

end

return LuaAutoTakingPhotoWnd
