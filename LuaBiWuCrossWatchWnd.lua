local QnExpandListBox = import "L10.UI.QnExpandListBox"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local QnTabView = import "L10.UI.QnTabView"
local GameObject = import "UnityEngine.GameObject"
local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local CBiWuDaHuiMgr = import "L10.Game.CBiWuDaHuiMgr"

LuaBiWuCrossWatchWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaBiWuCrossWatchWnd, "Tabs", "Tabs", QnTabView)
RegistChildComponent(LuaBiWuCrossWatchWnd, "SaiChengWnd", "SaiChengWnd", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaBiWuCrossWatchWnd, "m_group")
RegistClassMember(LuaBiWuCrossWatchWnd, "m_mapper")

function LuaBiWuCrossWatchWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    self.Tabs.OnSelect =
        DelegateFactory.Action_QnTabButton_int(
        function(btn, index)
            self:OnTabsSelected(btn, index)
        end
    )

    --@endregion EventBind end

    self.m_mapper = {
        {0, 0},
        {0, 0},
        {0, 1},
        {0, 2},
        {0, 3},
        {0, 4},
        {0, 0},
        {0, 0},
        {5, 6},
        {7, 8},
        {9, 10}
    }
end

function LuaBiWuCrossWatchWnd:OnEnable()
    g_ScriptEvent:AddListener("OnGetBiWuCrossMatchInfo", self, "OnGetBiWuCrossMatchInfo")
end

function LuaBiWuCrossWatchWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnGetBiWuCrossMatchInfo", self, "OnGetBiWuCrossMatchInfo")
end

function LuaBiWuCrossWatchWnd:OnGetBiWuCrossMatchInfo()
    self:InitBattleTab()
end

function LuaBiWuCrossWatchWnd:Init()
    self.m_group = CBiWuDaHuiMgr.Inst:GetMyStage()
    if self.m_group <= 0 then
        self.m_group = 1
    end
    self.Tabs:ChangeTo(self.m_group - 1)
end

function LuaBiWuCrossWatchWnd:InitBattleTab()
    local batinfos = LuaBiWuCrossMgr.BattleInfo
    if batinfos == nil then
        return
    end
    for i = 1, 11 do
        local ctrl = FindChild(self.SaiChengWnd.transform, "BattleTeamItem" .. i)
        if i <= #batinfos then
            local info = batinfos[i]
            self:InitBattleCtrl(ctrl, info, i)
        else
            self:InitBattleCtrl(ctrl, nil, i)
        end
    end

    local setting = BiWuCross_Setting.GetData()
    local rd1timelb = FindChildWithType(self.SaiChengWnd.transform, "Dec1/Label", typeof(UILabel))
    local rd2timelb = FindChildWithType(self.SaiChengWnd.transform, "Dec3/Label", typeof(UILabel))
    rd1timelb.text = setting.Round1TimeStr
    rd2timelb.text = setting.Round2TimeStr
end

function LuaBiWuCrossWatchWnd:InitBattleCtrl(ctrl, info, index)
    local teamlb1 = FindChildWithType(ctrl, "TeamName1", typeof(UILabel))
    local teamlb2 = FindChildWithType(ctrl, "TeamName2", typeof(UILabel))

    teamlb1.text = info.TeamName1
    teamlb2.text = info.TeamName2

    if info.WinTeamIndex > 0 then
        teamlb1.color = info.WinTeamIndex == info.TeamIndex1 and Color.white or Color.gray
        teamlb2.color = info.WinTeamIndex == info.TeamIndex2 and Color.white or Color.gray
    else
        teamlb1.color = info.TeamIndex1 > 0 and Color.white or Color.gray
        teamlb2.color = info.TeamIndex2 > 0 and Color.white or Color.gray
    end

    UIEventListener.Get(teamlb1.gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:ShowTeamWnd(info.TeamIndex1)
        end
    )

    UIEventListener.Get(teamlb2.gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:ShowTeamWnd(info.TeamIndex2)
        end
    )

    local canwatch = info.CanWatch
    local watchBtn = FindChild(ctrl, "Guanzhan_Button").gameObject
    watchBtn:SetActive(canwatch)
    if canwatch then
        UIEventListener.Get(watchBtn).onClick =
            DelegateFactory.VoidDelegate(
            function(go)
                self:OnWatchBtnClick(info.Matchindex)
            end
        )
    end
end

function LuaBiWuCrossWatchWnd:OnWatchBtnClick(midx)
    LuaBiWuCrossMgr.ReqEnterWatch(midx)
end

function LuaBiWuCrossWatchWnd:ShowTeamWnd(teamindex)
    LuaBiWuCrossMgr.ShowTeamWnd(0, teamindex)
end

--@region UIEvent

function LuaBiWuCrossWatchWnd:OnTabsSelected(btn, index)
    self.m_group = index + 1
    LuaBiWuCrossMgr.ReqInfoData(self.m_group)
end

--@endregion UIEvent
