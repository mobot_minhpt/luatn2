-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CellIndex = import "L10.UI.UITableView+CellIndex"
local CellIndexType = import "L10.UI.UITableView+CellIndexType"
local CTeamMatchActivityChildItem = import "L10.UI.CTeamMatchActivityChildItem"
local CTeamMatchActivityItem = import "L10.UI.CTeamMatchActivityItem"
local CTeamMatchActivityTableView = import "L10.UI.CTeamMatchActivityTableView"
local CTeamMatchMgr = import "L10.Game.CTeamMatchMgr"
local Object = import "System.Object"
local TeamMatch_Activities = import "L10.Game.TeamMatch_Activities"
CTeamMatchActivityTableView.m_Init_CS2LuaHook = function (this) 

    local index = nil
    if CTeamMatchMgr.Inst.selectedActivityId > 0 then
        do
            local i = 0
            while i < this.MatchActivitySections.Count and index == nil do
                if this.MatchActivitySections[i].activityId == CTeamMatchMgr.Inst.selectedActivityId then
                    index = CreateFromClass(CellIndex, i, 0, CellIndexType.Section)
                else
                    do
                        local j = 0
                        while j < this.MatchActivitySections[i].activities.Count do
                            if this.MatchActivitySections[i].activities[j] == CTeamMatchMgr.Inst.selectedActivityId then
                                index = CreateFromClass(CellIndex, i, j, CellIndexType.Row)
                                break
                            end
                            j = j + 1
                        end
                    end
                end
                i = i + 1
            end
        end
    end
    if index == nil then
        this:LoadData(CreateFromClass(CellIndex, 0, 0, CellIndexType.Section), true)
    else
        this:LoadData(index, true)
    end
end
CTeamMatchActivityTableView.m_OnSectionClicked_CS2LuaHook = function (this, index, expanded) 
    if index.type == CellIndexType.Section then
        local activityId = this.MatchActivitySections[index.section].activityId
        if activityId > 0 then
            if this.OnActivitySelected ~= nil then
                GenericDelegateInvoke(this.OnActivitySelected, Table2ArrayWithCount({activityId}, 1, MakeArrayClass(Object)))
            end
        elseif CClientMainPlayer.Inst ~= nil and expanded then
            --找到最接近玩家等级的活动，列表假定按等级升序排列
            local section = this.MatchActivitySections[index.section]
            if section.activities.Count > 1 then
                local lastRow = 0
                do
                    local i = 1
                    while i < section.activities.Count do
                        local id = section.activities[i]
                        local data = TeamMatch_Activities.GetData(id)
                        if data.MinPlayerLevel > CClientMainPlayer.Inst.Level then
                            break
                        end
                        lastRow = i
                        i = i + 1
                    end
                end
                this:SetRowSelected(CreateFromClass(CellIndex, index.section, lastRow, CellIndexType.Row))
            end
        end
    end
end
CTeamMatchActivityTableView.m_NumberOfRowsInSection_CS2LuaHook = function (this, section) 
    if this.MatchActivitySections.Count <= section then
        return 0
    end
    local data = this.MatchActivitySections[section]
    if data.activityId == 0 then
        return data.activities.Count
    else
        return 0
    end
end
CTeamMatchActivityTableView.m_RefreshCellForRowAtIndex_CS2LuaHook = function (this, cell, index) 
    if index.type == CellIndexType.Section then
        local cmp = CommonDefs.GetComponent_GameObject_Type(cell, typeof(CTeamMatchActivityItem))
        if cmp ~= nil then
            local data = this.MatchActivitySections[index.section]
            cmp:Init(data)
        end
    else
        local activityId = this.MatchActivitySections[index.section].activities[index.row]
        local cmp = CommonDefs.GetComponent_GameObject_Type(cell, typeof(CTeamMatchActivityChildItem))
        cmp:Init(activityId)
    end
end
