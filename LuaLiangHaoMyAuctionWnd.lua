require("common/common_include")

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CBaseWnd = import "L10.UI.CBaseWnd"
local CUICommonDef = import "L10.UI.CUICommonDef"
local Extensions = import "Extensions"
local UILabel = import "UILabel"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Color = import "UnityEngine.Color"

LuaLiangHaoMyAuctionWnd = class()

RegistClassMember(LuaLiangHaoMyAuctionWnd, "m_Template")
RegistClassMember(LuaLiangHaoMyAuctionWnd, "m_Table")


function LuaLiangHaoMyAuctionWnd:Init()
	self.m_Template = self.transform:Find("Anchor/Item").gameObject
	self.m_Table = self.transform:Find("Anchor/Table"):GetComponent(typeof(UITable))

	self.m_Template:SetActive(false)
	Extensions.RemoveAllChildren(self.m_Table.transform)
	for __,data in pairs(LuaLiangHaoMgr.MyAuctionLiangHaoInfo) do
		local go = CUICommonDef.AddChild(self.m_Table.gameObject, self.m_Template)
		go:SetActive(true)
		self:InitItem(go, data)
	end

	self.m_Table:Reposition()
end

function LuaLiangHaoMyAuctionWnd:InitItem(itemGo, auctionInfo)
	local m_LiangHaoLabel = itemGo.transform:Find("LiangHaoLabel"):GetComponent(typeof(UILabel))
	local m_TopBidderInfoLabel = itemGo.transform:Find("TopBidder/PlayerInfoLabel"):GetComponent(typeof(UILabel))
	local m_TopBidderPriceLabel = itemGo.transform:Find("TopBidder/PriceLabel"):GetComponent(typeof(UILabel))
	local m_MyPriceLabel = itemGo.transform:Find("MyAuction/PriceLabel"):GetComponent(typeof(UILabel))
	local m_AuctionButton = itemGo.transform:Find("AuctionButton").gameObject
	local m_NoNeedAuctionGo = itemGo.transform:Find("NoNeedAuctionLabel").gameObject
	
	CommonDefs.AddOnClickListener(m_AuctionButton, DelegateFactory.Action_GameObject(function(go) self:OnAuctionButtonClick(auctionInfo.lianghaoId) end), false)

	m_LiangHaoLabel.text = tostring(auctionInfo.lianghaoId)
	m_TopBidderInfoLabel.text = auctionInfo.roleName
	m_TopBidderPriceLabel.text = tostring(auctionInfo.currentPrice)
	m_MyPriceLabel.text = tostring(auctionInfo.myPrice)

	if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == auctionInfo.roleId then
		m_AuctionButton:SetActive(false)
		m_NoNeedAuctionGo:SetActive(true)
		m_TopBidderInfoLabel.color = Color.green
	else
		m_AuctionButton:SetActive(true)
		m_NoNeedAuctionGo:SetActive(false)
	end
end

function LuaLiangHaoMyAuctionWnd:OnAuctionButtonClick(lianghaoId)
	LuaLiangHaoMgr.QueryLiangHaoAuctionInfo(lianghaoId)
	self:Close()
end

function LuaLiangHaoMyAuctionWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end
