local UILabel = import "UILabel"
local SoundManager = import "SoundManager"
local PlayerSettings = import "L10.Game.PlayerSettings"
local CScrollTaskMusicGameHitItem = import "L10.Game.CScrollTaskMusicGameHitItem"
local CClientObject = import "L10.Game.CClientObject"

LuaScrollTaskMusicGameTaskView = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaScrollTaskMusicGameTaskView, "GameDesc", "GameDesc", UILabel)
RegistChildComponent(LuaScrollTaskMusicGameTaskView, "HitTemplate", "HitTemplate", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaScrollTaskMusicGameTaskView, "m_StartPos")
RegistClassMember(LuaScrollTaskMusicGameTaskView, "m_IsPlaying")
RegistClassMember(LuaScrollTaskMusicGameTaskView, "m_ItemPanel")
RegistClassMember(LuaScrollTaskMusicGameTaskView, "m_Sound")
RegistClassMember(LuaScrollTaskMusicGameTaskView, "m_Tick")
RegistClassMember(LuaScrollTaskMusicGameTaskView, "m_MusicId")
RegistClassMember(LuaScrollTaskMusicGameTaskView, "m_TexPanel")

RegistClassMember(LuaScrollTaskMusicGameTaskView, "m_WalkChangeInfo")
RegistClassMember(LuaScrollTaskMusicGameTaskView, "m_RunChangeInfo")
RegistClassMember(LuaScrollTaskMusicGameTaskView, "m_FlyChangeInfo")
RegistClassMember(LuaScrollTaskMusicGameTaskView, "m_CheckSpeed")

function LuaScrollTaskMusicGameTaskView:Awake()
    self.m_StartPos = self.transform:Find("TexPanel/StartPos").gameObject
    self.m_TexPanel = self.transform:Find("TexPanel"):GetComponent(typeof(UIPanel))
    self.m_Texture  = self.transform:Find("TexPanel/Texture"):GetComponent(typeof(UITexture))
    self.HitTemplate:SetActive(false)
    NGUITools.DestroyChildren(self.m_StartPos.transform)
    
    self.GameDesc.text = g_MessageMgr:FormatMessage("ScrollTask_MusicGame_Desc")
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaScrollTaskMusicGameTaskView:InitGame(gameId, taskId, callback)
    self:OnScrollTaskGamePanelChange()

    local texture = LuaScrollTaskMgr:GetScreenshot("GameScreen")
    local adjustment = UIRoot.GetPixelSizeAdjustment(self.gameObject)
    self.m_Texture.width = texture.width * adjustment
    self.m_Texture.height = texture.height * adjustment
    self.m_Texture.mainTexture = texture
    
    self:LoadDesignData(gameId)
    self:LoadMusic(0)

    self.m_IsPlaying = true
    self.m_Callback = callback
end

function LuaScrollTaskMusicGameTaskView:LoadDesignData(gameId)
    local data = ScrollTask_Music.GetData(gameId)
    if data == nil then
        return
    end

    self.m_MusicId = gameId
    self.m_MusicName = data.Music

    CClientObject.m_MusicRunningMaxWalkSpeed = data.WalkSpeed
    CClientObject.m_MusicRunningNormalRunSpeed = data.RunSpeed
    -- 特殊处理，奔跑最大速度和飞行最大速度相等时关闭飞行
    if data.RunSpeed == data.FlySpeed then
        CClientObject.m_MusicRunningMaxRunSpeed = 99
    else 
        CClientObject.m_MusicRunningMaxRunSpeed = data.RunSpeed
    end
end

--@region UIEvent

--@endregion UIEvent


function LuaScrollTaskMusicGameTaskView:OnEnable()
    g_ScriptEvent:AddListener("OnFmodMarkerTrigger", self, "OnFmodMarkerTrigger")
    g_ScriptEvent:AddListener("ScrollTaskMusicGameStopMusicRun", self, "OnScrollTaskMusicGameStopMusicRun")
    g_ScriptEvent:AddListener("ScrollTaskGamePanelChange", self, "OnScrollTaskGamePanelChange")
    g_ScriptEvent:AddListener("OnClickScrollTaskMusicGameHitItem", self, "OnHitItemClicked")
end

function LuaScrollTaskMusicGameTaskView:OnDisable()
    g_ScriptEvent:RemoveListener("OnFmodMarkerTrigger", self, "OnFmodMarkerTrigger")
    g_ScriptEvent:RemoveListener("ScrollTaskMusicGameStopMusicRun", self, "OnScrollTaskMusicGameStopMusicRun")
    g_ScriptEvent:RemoveListener("ScrollTaskGamePanelChange", self, "OnScrollTaskGamePanelChange")
    g_ScriptEvent:RemoveListener("OnClickScrollTaskMusicGameHitItem", self, "OnHitItemClicked")
end

function LuaScrollTaskMusicGameTaskView:OnScrollTaskGamePanelChange()
    local panel = self.transform:GetComponent(typeof(UIPanel))
    self.m_TexPanel.depth = panel.depth - 1
end

function LuaScrollTaskMusicGameTaskView:LoadMusic(time)
    if self.m_MusicName then
        SoundManager.Inst:ChangeBgMusicVolume("ScrollTaskMusicGame", 0, false)
        local volum = PlayerSettings.MusicEnabled and PlayerSettings.VolumeSetting or 0
        self.m_Sound = SoundManager.Inst:PlaySound(self.m_MusicName, Vector3.zero, volum, nil, time)
        local slen = SoundManager.Inst:GetEventLength(self.m_Sound)
        SoundManager.Inst:AddMarkerCallback(self.m_Sound)
        UnRegisterTick(self.m_Tick)
        self.m_Tick = RegisterTickOnce(function()
            self:OnMusicFinish()
        end, slen)
    end
    -- body
end

function LuaScrollTaskMusicGameTaskView:OnMusicFinish()
    -- 循环播放
    SoundManager.Inst:StopSound(self.m_Sound)
    self:LoadMusic(0)
end

function LuaScrollTaskMusicGameTaskView:EndMusic()
    SoundManager.Inst:StopSound(self.m_Sound)
    SoundManager.Inst:ChangeBgMusicVolume("ScrollTaskMusicGame", 1, true)
end

function LuaScrollTaskMusicGameTaskView:OnFmodMarkerTrigger(params)
    local path = params[0]
    local name = params[1]
    local position = params[2]
    local mark = tonumber(name)

    if mark then
        self:AddHitPoint(mark)
    end
end

function LuaScrollTaskMusicGameTaskView:AddHitPoint(mark)
    local go = NGUITools.AddChild(self.m_StartPos, self.HitTemplate)
    local hitItem = go:GetComponent(typeof(CScrollTaskMusicGameHitItem))
    local size = 75
    Extensions.SetLocalPositionY(go.transform, 40 + math.random(-size, size))
    hitItem.m_HitDelay = 4.5
    hitItem.m_Speed = 360
    hitItem.m_YMoveSpeedRate = 5

    hitItem:ActiveItem(mark)
    go:SetActive(true)
end

function LuaScrollTaskMusicGameTaskView:OnGameFinish()
    if self.m_IsPlaying then
        self.m_IsPlaying = false
        self.m_Texture.mainTexture = nil
        self:EndMusic()
        UnRegisterTick(self.m_Tick)
        NGUITools.DestroyChildren(self.m_StartPos.transform)
        if self.m_Callback then
            self.m_Callback()
        end
    end
end

function LuaScrollTaskMusicGameTaskView:OnDestroy()
    if self.m_Texture then
        self.m_Texture.mainTexture = nil
    end
    self:EndMusic()
    UnRegisterTick(self.m_Tick)
    if self.m_StartPos then
        NGUITools.DestroyChildren(self.m_StartPos.transform)
    end
end

function LuaScrollTaskMusicGameTaskView:OnScrollTaskMusicGameStopMusicRun()
    self:OnGameFinish()
end

function LuaScrollTaskMusicGameTaskView:OnHitItemClicked(args)
    local maker = args[0]
    local result = args[1]
    Gac2Gas.ReportMusicRunClick(self.m_MusicId, maker, result)
end
