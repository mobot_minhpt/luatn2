-- Auto Generated!!
local Callback = import "EventDelegate+Callback"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CGuildChangeMottoWnd = import "L10.UI.Guild.CGuildChangeMottoWnd"
local CGuildMgr = import "L10.Game.CGuildMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local EventDelegate = import "EventDelegate"
local LocalString = import "LocalString"
local Object = import "System.Object"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CGuildChangeMottoWnd.m_Awake_CS2LuaHook = function (this) 
    EventDelegate.Add(this.mottoInput.onChange, MakeDelegateFromCSFunction(this.OnInputChange, Callback, this))
    UIEventListener.Get(this.closeBtn).onClick = MakeDelegateFromCSFunction(this.OnClickCloseButton, VoidDelegate, this)
    UIEventListener.Get(this.changeBtn.gameObject).onClick = MakeDelegateFromCSFunction(this.OnClickChangeButton, VoidDelegate, this)
    UIEventListener.Get(this.cancleBtn).onClick = MakeDelegateFromCSFunction(this.OnClickCloseButton, VoidDelegate, this)
end
CGuildChangeMottoWnd.m_OnInputChange_CS2LuaHook = function (this) 
    local str = this.mottoInput.value
    if CUICommonDef.GetStrByteLength(str) > 200 then
        repeat
            str = CommonDefs.StringSubstring2(str, 0, CommonDefs.StringLength(str) - 1)
        until not (CUICommonDef.GetStrByteLength(str) > 200)
        this.mottoInput.value = str
    end
end
CGuildChangeMottoWnd.m_OnRequestOperationInGuildSucceed_CS2LuaHook = function (this, requestType, paramStr) 
    if (("AlterMission") == requestType) then
        CUIManager.CloseUI(CUIResources.GuildChangeMottoWnd)
        if CGuildMgr.Inst.m_GuildInfo ~= nil then
            CGuildMgr.Inst.m_GuildInfo.Mission = paramStr
            CGuildMgr.Inst:GetMyGuildInfo()
        end
    end
end
CGuildChangeMottoWnd.m_OnClickChangeButton_CS2LuaHook = function (this, go) 
    --修改帮会宗旨
    local content = this.mottoInput.value
    local length = CUICommonDef.GetStrByteLength(content)
    if length < 2 or length > 200 then
        --List<object> datas = new List<object>() {"帮会宗旨的字数为1~100个汉字"};
        g_MessageMgr:ShowMessage("Guild_Input_Mission_length")
        return
    end
    if CGuildMgr.Inst.m_GuildInfo ~= nil then
        if (content == CGuildMgr.Inst.m_GuildInfo.Mission) then
            local datas = InitializeList(CreateFromClass(MakeGenericClass(List, Object)), Object, LocalString.GetString("和原来的帮会宗旨一样"))
            g_MessageMgr:ShowMessage("CUSTOM_STRING", List2Params(datas))
            return
        end
    end
    if not CWordFilterMgr.Inst:CheckGuildMotto(content) then
        g_MessageMgr:ShowMessage("Guild_Motto_Violation")
        return
    end
    --Gac2Gas.RequestOperationInGuild(CClientMainPlayer.Inst.BasicProp.GuildId, "AlterMission", content);

    local len = CUICommonDef.GetStrByteLength(content)
    if (LocalString.GetString("请输入帮会宗旨") == content) or len < 2 or len > 200 then
        g_MessageMgr:ShowMessage("Guild_Input_Mission_length")
        return
    end

    if CommonDefs.StringLength(content) > 50 then
        local guildMission1 = CommonDefs.StringSubstring2(content, 0, 50)
        local guildMission2 = CommonDefs.StringSubstring1(content, 50)
        Gac2Gas.RequestAlterGuildMission(CClientMainPlayer.Inst.BasicProp.GuildId, guildMission1)
        Gac2Gas.RequestAlterGuildMissionEnd(CClientMainPlayer.Inst.BasicProp.GuildId, guildMission2)
    else
        Gac2Gas.RequestAlterGuildMissionEnd(CClientMainPlayer.Inst.BasicProp.GuildId, content)
    end
end
