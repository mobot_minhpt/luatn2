local QnTipButton = import "L10.UI.QnTipButton"
local CPlayerShopPopupMenuInfoMgr = import "L10.UI.CPlayerShopPopupMenuInfoMgr"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local CPopupMenuInfoMgrAlignType = import "L10.UI.CPopupMenuInfoMgr+AlignType"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CChatLinkMgr = import "CChatLinkMgr"

LuaShiTuHistoryWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaShiTuHistoryWnd, "QnTableView", "QnTableView", QnAdvanceGridView)
RegistChildComponent(LuaShiTuHistoryWnd, "QnButton", "QnButton", QnTipButton)

--@endregion RegistChildComponent end
RegistClassMember(LuaShiTuHistoryWnd, "m_List")
RegistClassMember(LuaShiTuHistoryWnd, "m_Action2NameArr")
RegistClassMember(LuaShiTuHistoryWnd, "m_Actions")
RegistClassMember(LuaShiTuHistoryWnd, "m_Index")
function LuaShiTuHistoryWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaShiTuHistoryWnd:Init()
    self.m_Actions = {}
    self.m_Action2NameArr = {LocalString.GetString("全部"),LocalString.GetString("成就获取"),LocalString.GetString("装备打造"),LocalString.GetString("培养计划"),LocalString.GetString("等级提升"),LocalString.GetString("其他")}
    for i,text in pairs(self.m_Action2NameArr) do
        local name = text
        table.insert(self.m_Actions,PopupMenuItemData(name, DelegateFactory.Action_int(function (index) 
			self.QnButton.Text = name
			self:OnSelectIndex(index)
		end), false, nil))
    end
    CommonDefs.AddOnClickListener(self.QnButton.gameObject,DelegateFactory.Action_GameObject(function (go)
	    self:OnQnButtonClick()
	end),false)
    self.QnTableView.m_DataSource = DefaultTableViewDataSource.Create(
        function() 
            return self.m_List and #self.m_List or 0
        end,
        function(item, index)
            self:ItemAt(item,index)
        end
    )
    self.m_Index = 0
    self.QnTableView:ReloadData(true, true)
    self.QnButton:SetTipStatus(true)
    Gac2Gas.RequestShiTuShiMenNews(LuaShiTuMgr.m_ShiTuMainWndTabIndex == 0)
end

function LuaShiTuHistoryWnd:ItemAt(item,index)
    local data = self.m_List[index + 1]
    item.transform:Find("TimeLabel"):GetComponent(typeof(UILabel)).text =  ToStringWrap(CServerTimeMgr.ConvertTimeStampToZone8Time(data.tm), "yyyy-MM-dd ") 
    local label = item.transform:Find("Label"):GetComponent(typeof(UILabel))
    local designData = ShiTu_ShiMenEvent.GetData(data.eventType)
    if #data.args == 0 then
        label.text = CUICommonDef.TranslateToNGUIText(SafeStringFormat3(designData.Desc))
    elseif #data.args == 1 then
        label.text = CUICommonDef.TranslateToNGUIText(SafeStringFormat3(designData.Desc, data.args[1]))
    elseif #data.args == 2 then
        label.text = CUICommonDef.TranslateToNGUIText(SafeStringFormat3(designData.Desc, data.args[1], data.args[2]))
    elseif #data.args == 3 then
        label.text = CUICommonDef.TranslateToNGUIText(SafeStringFormat3(designData.Desc, data.args[1], data.args[2], data.args[3]))
    elseif #data.args == 4 then
        label.text = CUICommonDef.TranslateToNGUIText(SafeStringFormat3(designData.Desc, data.args[1], data.args[2], data.args[3], data.args[4]))
    elseif #data.args == 5 then
        label.text = CUICommonDef.TranslateToNGUIText(SafeStringFormat3(designData.Desc, data.args[1], data.args[2], data.args[3], data.args[4], data.args[5]))
    elseif #data.args == 6 then
        label.text = CUICommonDef.TranslateToNGUIText(SafeStringFormat3(designData.Desc, data.args[1], data.args[2], data.args[3], data.args[4], data.args[5], data.args[6]))
    end
    label.color = Color.white
    UIEventListener.Get(label.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLabelClick(label)
	end)
end

function LuaShiTuHistoryWnd:OnLabelClick(label)
    local url = label:GetUrlAtPosition(UICamera.lastWorldPosition)
    if url then
        CChatLinkMgr.ProcessLinkClick(url, nil)
    end
end

function LuaShiTuHistoryWnd:OnEnable()
    g_ScriptEvent:AddListener("OnSendShiTuShiMenNews", self, "OnSendShiTuShiMenNews")
end

function LuaShiTuHistoryWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnSendShiTuShiMenNews", self, "OnSendShiTuShiMenNews")
end

function LuaShiTuHistoryWnd:OnSendShiTuShiMenNews()
    self.m_List = {}
    for i = 1,#LuaShiTuMgr.m_ShiTuShiMenNews do
        local data = LuaShiTuMgr.m_ShiTuShiMenNews[i] 
        local designData = ShiTu_ShiMenEvent.GetData(data.eventType)
        if self.m_Index == 0 or self.m_Index == designData.Class then
            table.insert(self.m_List, data)
        end
    end
    self.QnTableView:ReloadData(true, false)
end

function LuaShiTuHistoryWnd:OnSelectIndex(index)
    self.m_Index = index
    self:OnSendShiTuShiMenNews()
end

--@region UIEvent
function LuaShiTuHistoryWnd:OnQnButtonClick()
    self.QnButton:SetTipStatus(false)
    CPlayerShopPopupMenuInfoMgr.ShowPlayerShopPopupMenu(Table2Array(self.m_Actions, MakeArrayClass(PopupMenuItemData)), self.QnButton.transform, CPopupMenuInfoMgrAlignType.Bottom, 1, nil, nil, DelegateFactory.Action(function ()
        self.QnButton:SetTipStatus(true)
    end), 600,300)
end
--@endregion UIEvent

