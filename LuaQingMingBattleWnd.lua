local QnTableView=import "L10.UI.QnTableView"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local UILabel = import "UILabel"

CLuaQingMingBattleWnd = class()

RegistChildComponent(CLuaQingMingBattleWnd, "TableView", QnTableView)
RegistChildComponent(CLuaQingMingBattleWnd, "TipButton", GameObject)

function CLuaQingMingBattleWnd:Awake()
    UIEventListener.Get(self.TipButton).onClick  =DelegateFactory.VoidDelegate(function(go)
        g_MessageMgr:ShowMessage("QingMing2020_Pvp_Rule")
    end)
end

function CLuaQingMingBattleWnd:Init()
    self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function() 
            return #CLuaQingMing2020Mgr.PlayerBattleInfo
        end,
        function(item,row)
            self:InitItem(item,row)
        end)
    self.TableView:ReloadData(false, false)
end

function CLuaQingMingBattleWnd:InitItem(item, row)
    local info = CLuaQingMing2020Mgr.PlayerBattleInfo[row+1]
    if info then
        local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
        local statusLabel = item.transform:Find("StatusLabel"):GetComponent(typeof(UILabel))
        local bg1 = item.transform:Find("bg1").gameObject
        local bg2 = item.transform:Find("bg2").gameObject
        bg1:SetActive(row % 2 == 0)
        bg2:SetActive(row % 2 ~= 0)
        nameLabel.text = info.Name
        local status = info.Status
        if status == CLuaQingMing2020Mgr.PlayerStatus.Ghost then
            statusLabel.text = LocalString.GetString("携带游魂")
        elseif status == CLuaQingMing2020Mgr.PlayerStatus.Success then
            statusLabel.text = LocalString.GetString("已通关")
        else
            statusLabel.text = LocalString.GetString("无")
        end        
    end
end