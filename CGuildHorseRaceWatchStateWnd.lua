-- Auto Generated!!
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CGameVideoCharacterMgr = import "L10.Game.CGameVideoCharacterMgr"
local CGuildHorseRaceMgr = import "L10.Game.CGuildHorseRaceMgr"
local CGuildHorseRaceWatchStateWnd = import "L10.UI.CGuildHorseRaceWatchStateWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local Constants = import "L10.Game.Constants"
local CScene = import "L10.Game.CScene"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local QnCheckBox = import "L10.UI.QnCheckBox"
local RotateMode = import "DG.Tweening.RotateMode"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local UInt64 = import "System.UInt64"
local Vector3 = import "UnityEngine.Vector3"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local CRenderScene = import "L10.Engine.Scene.CRenderScene"
CGuildHorseRaceWatchStateWnd.m_Init_CS2LuaHook = function (this) 
    this.memberList.OnPlayerToWatchSelect = MakeDelegateFromCSFunction(this.OnPlayerToWatchSelect, MakeGenericClass(Action1, UInt64), this)
    this.memberList:Init()
    local perspectiveOptions = Table2ArrayWithCount({LocalString.GetString("自由视角"), LocalString.GetString("玩家视角")}, 2, MakeArrayClass(System.String))
    this.checkBoxGroup:InitWithOptions(perspectiveOptions, false, false)
    this.checkBoxGroup.OnSelect = MakeDelegateFromCSFunction(this.OnCheckBoxSelect, MakeGenericClass(Action2, QnCheckBox, Int32), this)
    CRenderScene.Inst.EnableChunkDynamicHide = false
end
CGuildHorseRaceWatchStateWnd.m_OnEnable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.m_LeaveButton.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_LeaveButton.gameObject).onClick, MakeDelegateFromCSFunction(this.OnLeaveButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.m_ExpandButton.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_ExpandButton.gameObject).onClick, MakeDelegateFromCSFunction(this.OnExpandButtonClicked, VoidDelegate, this), true)
    EventManager.AddListenerInternal(EnumEventType.SceneRemainTimeUpdate, MakeDelegateFromCSFunction(this.OnRemainTimeUpdate, MakeGenericClass(Action1, Int32), this))
    EventManager.AddListener(EnumEventType.OnSyncWatchGuildHorseRaceInfo, MakeDelegateFromCSFunction(this.UpdateWatchInfos, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.ClientObjDestroy, MakeDelegateFromCSFunction(this.OnWatchingPlayerLeaveScene, MakeGenericClass(Action1, UInt32), this))
end
CGuildHorseRaceWatchStateWnd.m_OnDisable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.m_LeaveButton.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_LeaveButton.gameObject).onClick, MakeDelegateFromCSFunction(this.OnLeaveButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.m_ExpandButton.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_ExpandButton.gameObject).onClick, MakeDelegateFromCSFunction(this.OnExpandButtonClicked, VoidDelegate, this), false)
    EventManager.RemoveListenerInternal(EnumEventType.SceneRemainTimeUpdate, MakeDelegateFromCSFunction(this.OnRemainTimeUpdate, MakeGenericClass(Action1, Int32), this))
    EventManager.RemoveListener(EnumEventType.OnSyncWatchGuildHorseRaceInfo, MakeDelegateFromCSFunction(this.UpdateWatchInfos, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.ClientObjDestroy, MakeDelegateFromCSFunction(this.OnWatchingPlayerLeaveScene, MakeGenericClass(Action1, UInt32), this))
end
CGuildHorseRaceWatchStateWnd.m_OnCheckBoxSelect_CS2LuaHook = function (this, box, index) 
    repeat
        local default = index
        if default == 0 then
            if CClientMainPlayer.Inst ~= nil then
                CRenderScene.Inst.UserDefineLocation = false
                CameraFollow.Inst:SetFollowObj(CClientMainPlayer.Inst.RO, nil)
                this.IsFreePerspective = true
            end
            break
        elseif default == 1 then
            if this.PlayerSelectedToWatch == 0 then
                g_MessageMgr:ShowMessage("GUILD_HORSERACE_WATCH_NOT_SELECT")
                this.checkBoxGroup:SetSelect(0, true)
                return
            end
            local player = CGameVideoCharacterMgr.Inst:GetGameVideoPlayer(this.PlayerSelectedToWatch)
            if player ~= nil then
                CRenderScene.Inst.UserDefineLocation = true
                CameraFollow.Inst:SetFollowObj(player.RO, nil)
                this.IsFreePerspective = false
            else
                g_MessageMgr:ShowMessage("GUILD_HORSERACE_WATCH_NOT_SELECT")
                this.checkBoxGroup:SetSelect(0, true)
                return
            end
            break
        else
            break
        end
    until 1
end
CGuildHorseRaceWatchStateWnd.m_OnPlayerToWatchSelect_CS2LuaHook = function (this, playerId) 
    this.PlayerSelectedToWatch = playerId
    local player = CGameVideoCharacterMgr.Inst:GetGameVideoPlayer(this.PlayerSelectedToWatch)
    if player ~= nil then
        this.PlayerSelectedEngineId = player.EngineId
        CGuildHorseRaceMgr.Inst.PlayerToWatch = player

        if not this.IsFreePerspective then
            this.checkBoxGroup:SetSelect(1, true)
        end
    end
end
CGuildHorseRaceWatchStateWnd.m_OnWatchingPlayerLeaveScene_CS2LuaHook = function (this, engindId) 
    if not this.IsFreePerspective and CGuildHorseRaceMgr.Inst.PlayerToWatch ~= nil and this.PlayerSelectedEngineId == engindId then
        g_MessageMgr:ShowMessage("GUILD_HORSE_RACE_WATCH_PLAYER_LEAVE")
        this.checkBoxGroup:SetSelect(0, true)
        this.PlayerSelectedToWatch = 0
        this.PlayerSelectedEngineId = 0
        CGuildHorseRaceMgr.Inst.PlayerToWatch = nil
    end
end
CGuildHorseRaceWatchStateWnd.m_OnRemainTimeUpdate_CS2LuaHook = function (this, leftTime) 
    if CScene.MainScene ~= nil then
        this.m_RemainTimeLabel.text = this:GetRemainTimeText(CScene.MainScene.ShowTime)
    else
        this.m_RemainTimeLabel.text = ""
    end
end
CGuildHorseRaceWatchStateWnd.m_GetRemainTimeText_CS2LuaHook = function (this, totalSeconds) 
    if totalSeconds < 0 then
        totalSeconds = 0
    end
    if totalSeconds >= 3600 then
        return System.String.Format("[ACF9FF]{0:00}:{1:00}:{2:00}[-]", math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
    else
        return System.String.Format("[ACF9FF]{0:00}:{1:00}[-]", math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end
CGuildHorseRaceWatchStateWnd.m_OnExpandButtonClicked_CS2LuaHook = function (this, go) 
    if this.m_Expand then
        LuaTweenUtils.DOLocalRotate(this.m_ExpandButton.transform, Vector3(0, 0, 180), CGuildHorseRaceWatchStateWnd.ExpandAniDuration, RotateMode.Fast)
        LuaTweenUtils.DOLocalMoveX(this.m_ExpandButtonOffset.transform, CGuildHorseRaceWatchStateWnd.ButtonSideX, Constants.GeneralAnimationDuration, false)
        LuaTweenUtils.DOLocalMoveX(this.m_LeftOffset.transform, CGuildHorseRaceWatchStateWnd.HiddenX, Constants.GeneralAnimationDuration, false)
    else
        LuaTweenUtils.DOLocalRotate(this.m_ExpandButton.transform, Vector3(0, 0, 0), CGuildHorseRaceWatchStateWnd.ExpandAniDuration, RotateMode.Fast)
        LuaTweenUtils.DOLocalMoveX(this.m_ExpandButtonOffset.transform, CGuildHorseRaceWatchStateWnd.OriginX, Constants.GeneralAnimationDuration, false)
        LuaTweenUtils.DOLocalMoveX(this.m_LeftOffset.transform, CGuildHorseRaceWatchStateWnd.OriginX, Constants.GeneralAnimationDuration, false)
    end
    this.m_Expand = not this.m_Expand
end
