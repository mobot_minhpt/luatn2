local DelegateFactory = import "DelegateFactory"

LuaWorldCup2022LotteryWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaWorldCup2022LotteryWnd, "tabs")
RegistClassMember(LuaWorldCup2022LotteryWnd, "redDots")
RegistClassMember(LuaWorldCup2022LotteryWnd, "dailyLotteryView")
RegistClassMember(LuaWorldCup2022LotteryWnd, "homeTeamView")

RegistClassMember(LuaWorldCup2022LotteryWnd, "curTab")

function LuaWorldCup2022LotteryWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

    self:InitUIComponents()
end

function LuaWorldCup2022LotteryWnd:InitUIComponents()
    local anchor = self.transform:Find("Anchor")

    self.tabs = {}
    table.insert(self.tabs, anchor:Find("Tab/LotteryTab"))
    table.insert(self.tabs, anchor:Find("Tab/HomeTeamTab"))

    self.redDots = {self.tabs[1]:Find("Normal/Sprite").gameObject, self.tabs[2]:Find("Normal/Sprite").gameObject}

    self.dailyLotteryView = anchor:Find("DailyLotteryView"):GetComponent(typeof(CCommonLuaScript))
    self.homeTeamView = anchor:Find("HomeTeamView"):GetComponent(typeof(CCommonLuaScript))
end

function LuaWorldCup2022LotteryWnd:OnEnable()
    g_ScriptEvent:AddListener("WorldCup2022UpdateLotteryRedDot", self, "UpdateLotteryRedDot")
end

function LuaWorldCup2022LotteryWnd:OnDisable()
    g_ScriptEvent:RemoveListener("WorldCup2022UpdateLotteryRedDot", self, "UpdateLotteryRedDot")
end

function LuaWorldCup2022LotteryWnd:UpdateLotteryRedDot()
    local info = LuaWorldCup2022Mgr.lotteryInfo
    self.redDots[1]:SetActive(info.hasLotteryAward and true or false)
    self.redDots[2]:SetActive(info.hasHomeTeamAward and true or false)
end


function LuaWorldCup2022LotteryWnd:Init()
    for i = 1, #self.tabs do
        local normal = self.tabs[i]:Find("Normal").gameObject
        local highlight = self.tabs[i]:Find("Highlight").gameObject

        normal:SetActive(true)
        highlight:SetActive(false)
        UIEventListener.Get(normal).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnTabChange(i)
        end)
    end

    self:UpdateLotteryRedDot()
    self:OnTabChange(1)
    Gac2Gas.WorldCupJingCai_RequestUpdateJingCaiData()
end

--@region UIEvent

function LuaWorldCup2022LotteryWnd:OnTabChange(id)
    if self.curTab then
        local beforeTab = self.tabs[self.curTab]
        beforeTab:Find("Normal").gameObject:SetActive(true)
        beforeTab:Find("Highlight").gameObject:SetActive(false)
    end

    self.curTab = id
    local tab = self.tabs[self.curTab]
    tab:Find("Normal").gameObject:SetActive(false)
    tab:Find("Highlight").gameObject:SetActive(true)

    self.dailyLotteryView.gameObject:SetActive(id == 1)
    self.homeTeamView.gameObject:SetActive(id == 2)
end

--@endregion UIEvent
