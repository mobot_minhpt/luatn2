local UIWidget             = import "UIWidget"
local BoxCollider          = import "UnityEngine.BoxCollider"
local UIScrollView         = import "UIScrollView"
local GameObject           = import "UnityEngine.GameObject"
local UILabel              = import "UILabel"
local DelegateFactory      = import "DelegateFactory"
local Extensions           = import "Extensions"
local UIVariableScrollView = import "L10.UI.UIVariableScrollView"
local CUICommonDef         = import "L10.UI.CUICommonDef"
local CUITexture           = import "L10.UI.CUITexture"
local UITexture            = import "UITexture"
local Color                = import "UnityEngine.Color"
local LuaUtils             = import "LuaUtils"
local UIEventListener      = import "UIEventListener"
local ShareBoxMgr          = import "L10.UI.ShareBoxMgr"
local EShareType           = import "L10.UI.EShareType"
local TweenPosition        = import "TweenPosition"
local TweenAlpha           = import "TweenAlpha"
local TweenRotation        = import "TweenRotation"

LuaRuMengJiWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaRuMengJiWnd, "Book", "Book", GameObject)
RegistChildComponent(LuaRuMengJiWnd, "RightButton", "RightButton", GameObject)
RegistChildComponent(LuaRuMengJiWnd, "LeftButton", "LeftButton", GameObject)
RegistChildComponent(LuaRuMengJiWnd, "ShareButton", "ShareButton", GameObject)
RegistChildComponent(LuaRuMengJiWnd, "IncludedLabel", "IncludedLabel", UILabel)
RegistChildComponent(LuaRuMengJiWnd, "PortraitTemplate", "PortraitTemplate", GameObject)
RegistChildComponent(LuaRuMengJiWnd, "Scroll", "Scroll", UIWidget)
RegistChildComponent(LuaRuMengJiWnd, "Down", "Down", UITexture)
RegistChildComponent(LuaRuMengJiWnd, "Up", "Up", UITexture)
RegistChildComponent(LuaRuMengJiWnd, "Other", "Other", GameObject)
RegistChildComponent(LuaRuMengJiWnd, "ScrollView", "ScrollView", UIVariableScrollView)
RegistChildComponent(LuaRuMengJiWnd, "CloseButton", "CloseButton", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaRuMengJiWnd, "itemList")
RegistClassMember(LuaRuMengJiWnd, "centerId")
RegistClassMember(LuaRuMengJiWnd, "EnumState")
RegistClassMember(LuaRuMengJiWnd, "unlockNum")
RegistClassMember(LuaRuMengJiWnd, "totalNum")

RegistClassMember(LuaRuMengJiWnd, "upFx")
RegistClassMember(LuaRuMengJiWnd, "downFx")
RegistClassMember(LuaRuMengJiWnd, "bgFx")
RegistClassMember(LuaRuMengJiWnd, "finger")

function LuaRuMengJiWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.RightButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRightButtonClick()
	end)

	UIEventListener.Get(self.LeftButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLeftButtonClick()
	end)

	UIEventListener.Get(self.ShareButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareButtonClick()
	end)

    --@endregion EventBind end
	self:InitActive()
	self:InitUIComponents()
	self.Scroll.alpha = 0
end

-- 初始化Active状态
function LuaRuMengJiWnd:InitActive()
	self.PortraitTemplate:SetActive(false)
	self.ShareButton:SetActive(not CommonDefs.IS_VN_CLIENT)
end

-- 初始化UI组件
function LuaRuMengJiWnd:InitUIComponents()
	self.upFx = self.Book.transform:Find("UpFx"):GetComponent(typeof(CUIFx))
	self.downFx = self.Book.transform:Find("DownFx"):GetComponent(typeof(CUIFx))
	self.bgFx = self.transform:Find("BgWnd/Panel/Fx"):GetComponent(typeof(CUIFx))
	self.finger = self.Scroll.transform:Find("Panel/Finger").gameObject
end


function LuaRuMengJiWnd:OnEnable()
	g_ScriptEvent:AddListener("SyncRuMengJiNpcStateResult", self, "OnSyncNpcStateResult")
end

function LuaRuMengJiWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SyncRuMengJiNpcStateResult", self, "OnSyncNpcStateResult")
end

function LuaRuMengJiWnd:OnSyncNpcStateResult(npcStateDict)
	self:InitItemList(npcStateDict)
	self:InitListener()
	self:InitNum()
end

function LuaRuMengJiWnd:Init()
	self:AddDragStartListener()
	self:InitClassMembers()
	Gac2Gas.RuMengJi_QueryState()
	self:InitBook()
end

-- 初始化成员变量
function LuaRuMengJiWnd:InitClassMembers()
	self.EnumState = {
		Locked = 0,
		Drawing = 1,
		Unlockable = 2,
		Unlocked = 3,
	}
end

-- 设置active
function LuaRuMengJiWnd:SetActive(bookActive, otherActive)
	self.Book:SetActive(bookActive)
	self.Other:SetActive(otherActive)
end

-- 初始化书本
function LuaRuMengJiWnd:InitBook()
	self:SetActive(true, false)
	self:SetBoxColliderEnabled(false)
	local tweenPosition = self.Up.transform:GetComponent(typeof(TweenPosition))
	local tweenRotation = self.Up.transform:GetComponent(typeof(TweenRotation))
	tweenPosition:ResetToBeginning()
	tweenRotation:ResetToBeginning()
	tweenPosition:PlayForward()
	tweenRotation:PlayForward()

	tweenPosition:AddOnFinished(DelegateFactory.Callback(function ()
		self:FadeInAndOut()
	end))

	self.upFx:LoadFx(g_UIFxPaths.RuMengJiUpFxPath)
	self.downFx:LoadFx(g_UIFxPaths.RuMengJiDownFxPath)
	self.bgFx:LoadFx(g_UIFxPaths.RuMengJiBgFxPath)
end

-- 封面逐渐消失，书签逐渐显现
function LuaRuMengJiWnd:FadeInAndOut()
	self.upFx:DestroyFx()
	self.downFx:DestroyFx()

	local tweenPosition = self.Down.transform:GetComponent(typeof(TweenPosition))
	local tweenAlpha = self.Down.transform:GetComponent(typeof(TweenAlpha))
	tweenPosition:ResetToBeginning()
	tweenAlpha:ResetToBeginning()
	tweenPosition:PlayForward()
	tweenAlpha:PlayForward()

	local duration = tweenPosition.duration
	LuaTweenUtils.TweenAlpha(self.Scroll, 0, 1, duration, function()
		self:SetActive(false, true)
		self:SetBoxColliderEnabled(true)
	end)
end

-- 设置box collider的active, 使播放动效时没有交互
function LuaRuMengJiWnd:SetBoxColliderEnabled(enabled)
	self.PortraitTemplate.transform:GetComponent(typeof(BoxCollider)).enabled = enabled
	self.Scroll.transform:Find("Container"):GetComponent(typeof(BoxCollider)).enabled = enabled
	local grid = self.ScrollView.transform:Find("Grid")
	for i = 1, grid.childCount do
		grid:GetChild(i - 1):GetComponent(typeof(BoxCollider)).enabled = enabled
	end
end

-- 初始化生成各个子项
function LuaRuMengJiWnd:InitItemList(npcStateDict)
	self.itemList = {}
	local grid = self.ScrollView.transform:Find("Grid")

	Extensions.RemoveAllChildren(grid)
	local type = WuMenHuaShi_XinShengHuaJiSetting.GetData().XinShengHuaJiNpcType
	local choice = LuaWuMenHuaShiMgr:Array2Tbl(WuMenHuaShi_XinShengHuaJi.GetData(type).Choice)
	local matPath = WuMenHuaShi_XinShengHuaJiSetting.GetData().RuMengJiNpcMatPath
	if not choice then return end

	for i = 1, #choice do
		local data = WuMenHuaShi_RuMengJiNpc.GetData(choice[i])
		local portraitItem = CUICommonDef.AddChild(grid.gameObject, self.PortraitTemplate)
		local trans = portraitItem.transform
		trans:Find("Label"):GetComponent(typeof(UILabel)).text = data.Name
		trans:Find("Label"):Find("Bg"):GetComponent(typeof(UIWidget)):ResetAndUpdateAnchors()

		local mat = SafeStringFormat3("%s/%s.mat", matPath, data.Picture)
		trans:Find("Npc"):GetComponent(typeof(CUITexture)):LoadMaterial(mat)

		self.itemList[i] = {trans = trans, npcId = choice[i]}

		if npcStateDict[choice[i]] then
			self:UpdateItem(i, npcStateDict[choice[i]])
		else
			self:UpdateItem(i, self.EnumState.Locked)
		end

		UIEventListener.Get(portraitItem).onClick = DelegateFactory.VoidDelegate(function (go)
			self:OnPortraitClick(i)
		end)

		portraitItem:SetActive(true)
	end
	self.centerId = self:GetInitCenterId()
	self.ScrollView:InitItems(self.centerId - 1)
	self:SetRedPointActive(self.itemList[self.centerId].trans, false)
	self:UpdateButtonRedPoint()
	self:UpdateButttonActive()
	self:AddClickTween()
end

-- 更新item状态
function LuaRuMengJiWnd:UpdateItem(id, state)
	local trans = self.itemList[id].trans
	local EnumState = self.EnumState

	self.itemList[id].state = state

	self.finger:SetActive(false)
	local cuiFx = trans:Find("Light"):Find("Fx"):GetComponent(typeof(CUIFx))
	if state == EnumState.Locked or state == EnumState.Drawing then
		self:SetItem(trans, 0.5, 0, false, false)
	elseif state == EnumState.Unlockable then
		self:SetItem(trans, 1, 0.1, false, true)
		cuiFx:LoadFx(g_UIFxPaths.RuMengJiGuangQuanFx)
	elseif state == EnumState.Unlocked then
		self:SetItem(trans, 1, 1, true, false)
		cuiFx:DestroyFx()
	end
end

function LuaRuMengJiWnd:SetItem(trans, portraitBrightness, npcBrightness, nameActive, redPointActive)
	self:SetPortraitBrightness(trans, portraitBrightness)
	self:SetNpcBrightness(trans, npcBrightness)
	self:SetNameActive(trans, nameActive)
	self:SetRedPointActive(trans, redPointActive)
end

-- 设置npc亮度
function LuaRuMengJiWnd:SetNpcBrightness(trans, brightness)
	local tex = trans:Find("Npc"):GetComponent(typeof(UITexture))
	tex.color = Color(brightness, brightness, brightness)
end

-- 获取npc亮度
function LuaRuMengJiWnd:GetNpcBrightness(trans)
	local tex = trans:Find("Npc"):GetComponent(typeof(UITexture))
	return tex.color.r
end

-- 设置书签亮度
function LuaRuMengJiWnd:SetPortraitBrightness(trans, brightness)
	local tex = trans:GetComponent(typeof(UITexture))
	tex.color = Color(brightness, brightness, brightness)
end

-- 设置是否显示姓名
function LuaRuMengJiWnd:SetNameActive(trans, active)
	local name = trans:Find("Label").gameObject
	name:SetActive(active)
end

-- 设置是否显示红点
function LuaRuMengJiWnd:SetRedPointActive(trans, active)
	local redPoint = trans:Find("Sprite").gameObject
	redPoint:SetActive(active)
end

-- 默认居中显示的书签：存在可解锁，显示最前的可解锁书签；无可解锁，显示最前的已解锁书签；
function LuaRuMengJiWnd:GetInitCenterId()
	local EnumState = self.EnumState
	for id, data in pairs(self.itemList) do
		if data.state == EnumState.Unlockable then
			return id
		end
	end
	for id, data in pairs(self.itemList) do
		if data.state == EnumState.Unlocked then
			return id
		end
	end
	return 1
end

-- 更新按键的红点
function LuaRuMengJiWnd:UpdateButtonRedPoint()
	local EnumState = self.EnumState
	local leftPointActive = false
	local rightPointActive = false

	local leftPoint = self.LeftButton.transform:Find("Sprite").gameObject
	local rightPoint = self.RightButton.transform:Find("Sprite").gameObject

	for i = self.centerId + 1, #self.itemList do
		if self.itemList[i].state == EnumState.Unlockable then
			rightPointActive = true
			break
		end
	end

	for i = self.centerId - 1, 1, -1 do
		if self.itemList[i].state == EnumState.Unlockable then
			leftPointActive = true
			break
		end
	end

	leftPoint:SetActive(leftPointActive)
	rightPoint:SetActive(rightPointActive)
end

-- 初始化居中开始和结束的回调函数
function LuaRuMengJiWnd:InitListener()
	local EnumState = self.EnumState

	self.ScrollView.onCenter = LuaUtils.OnCenterCallback(function(go)
		local item = self.itemList[self.centerId]
		if item.state == EnumState.Unlockable then
			self:SetRedPointActive(item.trans, true)
		end

		local nextCenterId = go.transform:GetSiblingIndex() + 1
		if nextCenterId ~= self.centerId then
			self:RemoveClickTween()
		end
		self.centerId = nextCenterId

		item = self.itemList[self.centerId]
		if item.state == EnumState.Unlockable then
			self:SetRedPointActive(item.trans, false)
		end
		self:UpdateButtonRedPoint()
		self:UpdateButttonActive()
	end)

	self.ScrollView.onFinished = LuaUtils.OnFinished(function()
		self:AddClickTween()
	end)
end

-- 初始化已解锁角色数以及总角色数
function LuaRuMengJiWnd:InitNum()
	if not self.itemList or #self.itemList == 0 then
		self.IncludedLabel.text = ""
		return
	end

	self.totalNum = #self.itemList
	self.unlockNum = 0

	for i = 1, self.totalNum do
		if self.itemList[i].state == self.EnumState.Unlocked then
			self.unlockNum = self.unlockNum + 1
		end
	end

	self:UpdateIncludedLabel()
end

-- 更新已收录数量
function LuaRuMengJiWnd:UpdateIncludedLabel()
	self.IncludedLabel.text = SafeStringFormat3(LocalString.GetString("已收录\n%d/%d"), self.unlockNum, self.totalNum)
end

-- 更新箭头是否显示
function LuaRuMengJiWnd:UpdateButttonActive()
	if self.centerId <= 1 then
		self.LeftButton:SetActive(false)
	else
		self.LeftButton:SetActive(true)
	end

	if self.centerId >= #self.itemList then
		self.RightButton:SetActive(false)
	else
		self.RightButton:SetActive(true)
	end
end

-- 计算右边下一个
function LuaRuMengJiWnd:GetNextRightId()
	for i = self.centerId + 1, #self.itemList do
		if self.itemList[i].state == self.EnumState.Unlockable then
			return i
		end
	end
	return self.centerId + 1
end

-- 计算左边下一个
function LuaRuMengJiWnd:GetNextLeftId()
	for i = self.centerId - 1, 1, -1 do
		if self.itemList[i].state == self.EnumState.Unlockable then
			return i
		end
	end
	return self.centerId - 1
end

-- 添加提示点击动效
function LuaRuMengJiWnd:AddClickTween()
	local item = self.itemList[self.centerId]
	if item.state == self.EnumState.Unlockable then
		self.finger:SetActive(true)
	end
end

-- 清除点击动效
function LuaRuMengJiWnd:RemoveClickTween()
	self.finger:SetActive(false)
end

-- 拖拽开始停止动效
function LuaRuMengJiWnd:AddDragStartListener()
	local uiScrollview = self.ScrollView.transform:GetComponent(typeof(UIScrollView))

	uiScrollview.onDragStarted = LuaUtils.OnDragNotification(function()
		self:RemoveClickTween()
    end)
end

function LuaRuMengJiWnd:OnDestroy()
	LuaTweenUtils.DOKill(self.Scroll.transform)
end

-- 去除解锁特效
function LuaRuMengJiWnd:DestroyUnlockFx()
	for i = 1, #self.itemList do
		local item = self.itemList[i]
		local cuiFx = item.trans:Find("UnlockFx"):GetComponent(typeof(CUIFx))
		cuiFx:DestroyFx()
	end
end

--@region UIEvent

function LuaRuMengJiWnd:OnRightButtonClick()
	if not self.itemList then return end
	if self.centerId >= #self.itemList then return end

	self.ScrollView:CenterOn(self:GetNextRightId() - 1)
end

function LuaRuMengJiWnd:OnLeftButtonClick()
	if not self.itemList then return end
	if self.centerId <= 1 then return end

	self.ScrollView:CenterOn(self:GetNextLeftId() - 1)
end

-- 点击书签
function LuaRuMengJiWnd:OnPortraitClick(id)
	if id ~= self.centerId then return end

	local item = self.itemList[id]
	local EnumState = self.EnumState

	if item.state == EnumState.Locked or item.state == EnumState.Drawing then
		g_MessageMgr:ShowMessage("XINSHENGHUAJI_NPC_LOCKED")
	elseif item.state == EnumState.Unlockable then
		local cuiFx = item.trans:Find("UnlockFx"):GetComponent(typeof(CUIFx))
		cuiFx:DestroyFx()
		cuiFx:LoadFx(g_UIFxPaths.RuMengJiJieSuoFx)

		local brightness = self:GetNpcBrightness(item.trans)
		if brightness < 0.2 then
			self:SetNpcBrightness(item.trans, 0.4)
		elseif brightness < 0.5 then
			self:SetNpcBrightness(item.trans, 0.7)
		else
			self:SetNpcBrightness(item.trans, 1)
			self:UpdateItem(id, EnumState.Unlocked)
			self.unlockNum = self.unlockNum + 1
			self:UpdateIncludedLabel()

			Gac2Gas.RuMengJi_Unlock(self.itemList[id].npcId)
			self:RemoveClickTween()
		end
	end
end

function LuaRuMengJiWnd:OnShareButtonClick()
	self:SetScreenShotActive(false)
	self:DestroyUnlockFx()

    CUICommonDef.CaptureScreen("screenshot", true, false, nil,
        DelegateFactory.Action_string_bytes(function(fullPath, jpgBytes)
			self:SetScreenShotActive(true)
			self:UpdateButttonActive()
			ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, fullPath)
        end), false)
end

-- 截图时的active
function LuaRuMengJiWnd:SetScreenShotActive(active)
	self.LeftButton:SetActive(active)
	self.RightButton:SetActive(active)
	self.CloseButton:SetActive(active)
	self.ShareButton:SetActive(active and not CommonDefs.IS_VN_CLIENT)
end

--@endregion UIEvent

