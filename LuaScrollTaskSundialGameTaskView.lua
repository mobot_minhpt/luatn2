local GameObject = import "UnityEngine.GameObject"
local CUITexture = import "L10.UI.CUITexture"
local UITexture = import "UITexture"
local UIPanel = import "UIPanel"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local UICamera = import "UICamera"
local Extensions = import "Extensions"
local CSharpResourceLoader = import "L10.Game.CSharpResourceLoader"
local CImageSpreadEffectHelper = import "CImageSpreadEffectHelper"
local Vector4 = import "UnityEngine.Vector4"
local Material = import "UnityEngine.Material"

LuaScrollTaskSundialGameTaskView = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaScrollTaskSundialGameTaskView, "GameDesc", "GameDesc", UILabel)
RegistChildComponent(LuaScrollTaskSundialGameTaskView, "Texture", "Texture", CUITexture)
RegistChildComponent(LuaScrollTaskSundialGameTaskView, "Shadow", "Shadow", UITexture)
RegistChildComponent(LuaScrollTaskSundialGameTaskView, "MaskPanel", "MaskPanel", UIPanel)
RegistChildComponent(LuaScrollTaskSundialGameTaskView, "Sundial", "Sundial", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaScrollTaskSundialGameTaskView, "m_FinishDelay")
RegistClassMember(LuaScrollTaskSundialGameTaskView, "m_Textures")
RegistClassMember(LuaScrollTaskSundialGameTaskView, "m_Helpers")
RegistClassMember(LuaScrollTaskSundialGameTaskView, "m_Count")
RegistClassMember(LuaScrollTaskSundialGameTaskView, "m_Progress")
RegistClassMember(LuaScrollTaskSundialGameTaskView, "m_TimeRate")
RegistClassMember(LuaScrollTaskSundialGameTaskView, "m_LastDir")
RegistClassMember(LuaScrollTaskSundialGameTaskView, "m_TexturePaths")
RegistClassMember(LuaScrollTaskSundialGameTaskView, "m_RotateSpeedRate")
RegistClassMember(LuaScrollTaskSundialGameTaskView, "m_Percent")
RegistClassMember(LuaScrollTaskSundialGameTaskView, "m_IsPlaying")
RegistClassMember(LuaScrollTaskSundialGameTaskView, "m_IsAnticlockwise")

RegistClassMember(LuaScrollTaskSundialGameTaskView, "m_DayNight")
RegistClassMember(LuaScrollTaskSundialGameTaskView, "m_Clockwise")
RegistClassMember(LuaScrollTaskSundialGameTaskView, "m_CounterClockWise")

function LuaScrollTaskSundialGameTaskView:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end

    self.m_DayNight = self.transform:Find("Sundial/DayNight").transform
    self.m_Clockwise = self.transform:Find("Sundial/DayNight/ClockWise").gameObject
    self.m_CounterClockWise = self.transform:Find("Sundial/DayNight/CounterClockWise").gameObject
    self.m_TimeRate = 1
    self.m_RotateSpeedRate = 1
    self.m_Percent = 0.5
    
    self.Texture.gameObject:SetActive(false)
end

function LuaScrollTaskSundialGameTaskView:Start()
    CommonDefs.AddOnDragListener(self.Sundial, DelegateFactory.Action_GameObject_Vector2(function (go, delta)
		self:OnScreenDrag(go, delta)
    end), false)
    
    UIEventListener.Get(self.Sundial).onDragStart = DelegateFactory.VoidDelegate(function(g)
        self:OnDragStart(g)
    end)

    UIEventListener.Get(self.Sundial).onDragEnd = DelegateFactory.VoidDelegate(function(g)
        self:OnDragEnd(g)
    end)
end

function LuaScrollTaskSundialGameTaskView:InitGame(gameId, taskId, callback)
    self:OnScrollTaskGamePanelChange()
    local panel = self.transform:GetComponent(typeof(UIPanel))
    self.MaskPanel.depth = panel.depth - 1

    self:LoadDesignData(gameId)

    self.m_Clockwise:SetActive(not self.m_IsAnticlockwise)
    self.m_CounterClockWise:SetActive(self.m_IsAnticlockwise)

    if self.m_Textures then
        for i, v in ipairs(self.m_Textures) do
            GameObject.Destroy(v.gameObject)
        end
    end

    self.m_Textures = {}
    self.m_Helpers = {}
    if self.m_TexturePaths then
        local textures = self.m_TexturePaths
        for i = 1, #textures do
            local texture = NGUITools.AddChild(self.MaskPanel.gameObject, self.Texture.gameObject):GetComponent(typeof(CUITexture))
            texture.gameObject:SetActive(true)
            texture.texture.material = CreateFromClass(Material, texture.texture.material)
            local helper = texture:GetComponent(typeof(CImageSpreadEffectHelper))
            helper.baseClipRegion = Vector4(
                0,
                0,
                1422,
                800)
            helper.m_MaxHeight = 800
            helper.m_MaxClipAmount = 1.622
            CSharpResourceLoader.Inst:LoadTexture2D("UI/Texture/ScrollTask/" .. textures[i] .. ".jpg", DelegateFactory.Action_Texture2D(function(tex)
                texture.texture.material.mainTexture = tex
                helper:RefreshOffsetAndSize()
            end))
            table.insert(self.m_Textures, texture)
            table.insert(self.m_Helpers, helper)
        end
    end
    self.m_Progress = 0
    self.m_Count = #self.m_Textures

    self:ProcessProgress()

    self.m_IsPlaying = true
    self.m_Callback = callback
end

function LuaScrollTaskSundialGameTaskView:LoadDesignData(gameId)
    local data = ScrollTask_SundialGame.GetData(gameId)
    if data == nil then
        return
    end
    self.GameDesc.text = data.Desc
    --data.PicturePaths
    self.m_TexturePaths = {}
    for i = 0, data.PicturePaths.Length - 1 do
        table.insert(self.m_TexturePaths, data.PicturePaths[i])
    end

    self.m_TimeRate = data.CircleRate or 1
    self.m_RotateSpeedRate = data.RotateSpeedRate or 1
    self.m_Percent = data.Percent or 0.5
    self.m_IsAnticlockwise = data.ClockwiseSense == 1
end

--@region UIEvent

--@endregion UIEvent

function LuaScrollTaskSundialGameTaskView:OnEnable()
    g_ScriptEvent:AddListener("ScrollTaskGamePanelChange", self, "OnScrollTaskGamePanelChange")
end

function LuaScrollTaskSundialGameTaskView:OnDisable()
    g_ScriptEvent:RemoveListener("ScrollTaskGamePanelChange", self, "OnScrollTaskGamePanelChange")
end

function LuaScrollTaskSundialGameTaskView:OnScrollTaskGamePanelChange()
    local panel = self.transform:GetComponent(typeof(UIPanel))
    self.MaskPanel.depth = panel.depth - 1
end

function LuaScrollTaskSundialGameTaskView:OnScreenDrag(go, delta)
    local currentPos = UICamera.currentTouch.pos
    local currentDir = CommonDefs.op_Subtraction_Vector3_Vector3(UICamera.currentCamera:ScreenToWorldPoint(Vector3(currentPos.x,currentPos.y,0)) , self.Sundial.transform.position)
    local clockwiseRate = self.m_IsAnticlockwise and -1 or 1

    if self.m_LastDir then
        self.m_Progress = self.m_Progress + clockwiseRate * CommonDefs.AngleSigned(self.m_LastDir, currentDir, Vector3.back) / 360 / self.m_Count / self.m_TimeRate * self.m_RotateSpeedRate
        self.m_Progress = Mathf.Clamp(self.m_Progress, 0, 1)
        self:ProcessProgress()
    end
    self.m_LastDir = currentDir
end

function LuaScrollTaskSundialGameTaskView:OnDragStart(g)
end

function LuaScrollTaskSundialGameTaskView:OnDragEnd(g)
    if self.m_Progress == 1 and self.m_IsPlaying then
        self.m_IsPlaying = false
        self:OnSuccess()
    end
end

function LuaScrollTaskSundialGameTaskView:ProcessProgress()
    local lerpPercent = (1 - self.m_Percent) / 2
    for i = 1, #self.m_Textures do
        local progress = self.m_Progress * self.m_Count
        local texture = self.m_Textures[i]
        local helper = self.m_Helpers[i]
        local start1 = i - 1 - lerpPercent
        local end1 = i - 1 + lerpPercent
        local start2 = i - lerpPercent
        local end2 = i + lerpPercent
        local alpha = 0
        if start1 < progress and progress < end1 then
            alpha = i == 1 and 1 or math.lerp(0, 1, (progress - start1) / (2 * lerpPercent))
        elseif end1 < progress and progress < start2 then
            alpha = 1
        elseif start2 < progress and progress < end2 then
            alpha = i == #self.m_Textures and 1 or math.lerp(1, 0, (progress - start2) / (2 * lerpPercent))
        else
            alpha = 0
        end
        helper.m_Alpha = alpha
        texture.material:SetFloat("_Alpha", alpha)
    end

    local clockwiseRate = self.m_IsAnticlockwise and -1 or 1
    local angle = (clockwiseRate * self.m_Progress * self.m_Count * 360 * self.m_TimeRate ) % 360
    Extensions.SetLocalRotationZ(self.Shadow.transform, - angle)
    Extensions.SetLocalRotationZ(self.m_DayNight, 180 - angle)
    
    self.MaskPanel:Refresh()
end

function LuaScrollTaskSundialGameTaskView:OnSuccess()
    UnRegisterTick(self.m_Tick)
    self.m_Tick = RegisterTickOnce(function()
        self:OnGameFinish()
    end, self.m_FinishDelay or 2000)
end

function LuaScrollTaskSundialGameTaskView:OnGameFinish()
    if self.m_Callback then
        self.m_Callback()
    end
end

function LuaScrollTaskSundialGameTaskView:OnDestroy()
    UnRegisterTick(self.m_Tick)
end
