-- Auto Generated!!
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CAuctionItem = import "L10.UI.CAuctionItem"
local CItem = import "L10.Game.CItem"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CPlayerShopMgr = import "L10.UI.CPlayerShopMgr"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local Item_Item = import "L10.Game.Item_Item"
local QualityColor = import "L10.Game.QualityColor"
local EnumQualityType = import "L10.Game.EnumQualityType"
local String = import "System.String"
local Time = import "UnityEngine.Time"
local UInt32 = import "System.UInt32"
local UIWidget = import "UIWidget"
CAuctionItem.m_OnEnable_CS2LuaHook = function (this) 
    EventManager.AddListenerInternal(EnumEventType.OnAuctionItemPriceRefresh, MakeDelegateFromCSFunction(this.onItemPriceRefresh, MakeGenericClass(Action3, String, UInt32, UInt32), this))
    EventManager.AddListenerInternal(EnumEventType.OnBidSuccess, MakeDelegateFromCSFunction(this.onBidSuccess, MakeGenericClass(Action2, String, UInt32), this))
    this.startCountingTime = Time.realtimeSinceStartup
    this:StartCoroutine(this:counting())
    this.CountDownLabel.gameObject:SetActive(false)
    if this.myAuctionRoot then
        this.myAuctionRoot.gameObject:SetActive(false)
    end
end
CAuctionItem.m_SetMode_CS2LuaHook = function (this, mode) 
    if mode == 0 then
        local w = CommonDefs.GetComponent_Component_Type(this, typeof(UIWidget))
        w.height = CAuctionItem.NormalHeight
        this.SelfPriceGO:SetActive(false)
    else
        local w = CommonDefs.GetComponent_Component_Type(this, typeof(UIWidget))
        w.height = CAuctionItem.SelfAuctionItemHeight
        this.SelfPriceGO:SetActive(true)
        Extensions.SetLocalPositionY(this.FixPriceRoot.transform, CAuctionItem.upperY)
    end
    local widgets = CommonDefs.GetComponentsInChildren_Component_Type(this, typeof(UIWidget))
    do
        local i = 0
        while i < widgets.Length do
            widgets[i]:ResetAndUpdateAnchors()
            i = i + 1
        end
    end
end
CAuctionItem.m_show_CS2LuaHook = function (this) 

    if this.item ~= nil then
        this.IconContainer:LoadMaterial(this.item.Icon)
        this.NameLabel.text = this.item.Name
        if this.item.IsItem then
            this.NameLabel.color = this.item.Item.Color
        elseif this.item.IsEquip then
            this.NameLabel.color = this.item.Equip.DisplayColor
        end
    else
        local itemTemplate = Item_Item.GetData(this.iteminfo.TemplateID)
        if itemTemplate then
            this.IconContainer:LoadMaterial(itemTemplate.Icon)
            this.NameLabel.text = itemTemplate.Name
            this.NameLabel.color = QualityColor.GetRGBValue(CItem.GetQualityType(this.iteminfo.TemplateID))
        else
            local equipment = EquipmentTemplate_Equip.GetData(this.iteminfo.TemplateID)
            if equipment then
                this.IconContainer:LoadMaterial(equipment.Icon)
                this.NameLabel.text = equipment.Name
                this.NameLabel.color = QualityColor.GetRGBValue(CommonDefs.ConvertIntToEnum(typeof(EnumQualityType), equipment.Color))
            end
        end
    end
    if this.iteminfo.Count > 1 then
        this.CountLabel.text = tostring(this.iteminfo.Count)
    else
        this.CountLabel.text = ""
    end
    if this.m_IdentifyIcon ~= nil then
        this.m_IdentifyIcon.enabled = (CPlayerShopMgr.s_EnableShowEquipIdentifyInfo and this.item ~= nil and this.item.IsEquip and this.item.Equip.IsIdentifiable)
    end
    this.currentPriceRoot:SetActive(this.iteminfo.Price > 0)
    this.CurrentPriceLabel.text = tostring(this.iteminfo.Price)
    this.FixPriceRoot:SetActive(this.iteminfo.FixPrice > 0)
    local y = this.iteminfo.Price > 0 and CAuctionItem.normalY or CAuctionItem.lowerY
    Extensions.SetLocalPositionY(this.FixPriceRoot.transform, y)
    this.FixPriceLabel.text = tostring(this.iteminfo.FixPrice)
    this.SelfPriceLabel.text = tostring(this.iteminfo.SelfPrice)
    this:StopAllCoroutines()
    this:StartCoroutine(this:counting())
    this.CountDownLabel.gameObject:SetActive(true)
    local isShowTakeOffPrice = LuaAuctionMgr:IsEnableNewKouDaoAuction() and LuaAuctionMgr.m_TakeOffPriceItemAuctionID == this.iteminfo.AuctionID
    local currentPriceLabel = this.currentPriceRoot.transform:Find("CurrentPriceLabel"):GetComponent(typeof(UILabel))
    currentPriceLabel.text = isShowTakeOffPrice and LocalString.GetString("起拍价") or LocalString.GetString("竞拍价")
end
CAuctionItem.m_onItemPriceRefresh_CS2LuaHook = function (this, id, newPrice, ownPrice) 
    if this.iteminfo == nil then
        return
    end
    if id == this.iteminfo.AuctionID then
        this.CurrentPriceLabel.text = tostring(newPrice)
    end
end
CAuctionItem.m_onBidSuccess_CS2LuaHook = function (this, auctionID, price) 
    if this.iteminfo.AuctionID == auctionID then
        this.SelfPriceLabel.text = tostring(price)
    end
end
CAuctionItem.m_onIconClick_CS2LuaHook = function (this, go) 
    if this.iteminfo == nil then
        return
    end

    if this.item ~= nil then
        CItemInfoMgr.ShowLinkItemInfo(this.item, true, nil, AlignType.Default, 0, 0, 0, 0, 0)
    else
        CItemInfoMgr.ShowLinkItemTemplateInfo(this.iteminfo.TemplateID, false, nil, AlignType.Default, 0, 0, 0, 0)
    end
    this:OnClick()
end
