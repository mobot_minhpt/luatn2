local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local QuanMinPK_OnlineTime=import "L10.Game.QuanMinPK_OnlineTime"
-- local QnAdvanceGridView=import "L10.UI.QnAdvanceGridView"
local QnTableView=import "L10.UI.QnTableView"
local QnTabButton=import "L10.UI.QnTabButton"
local Vector3 = import "UnityEngine.Vector3"
local MessageWndManager = import "L10.UI.MessageWndManager"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"
-- local CQMPKRequestPlayerItem = import "L10.UI.CQMPKRequestPlayerItem"
-- local CQMPKRequestPlayer = import "L10.UI.CQMPKRequestPlayer"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local AlignType = import "CPlayerInfoMgr+AlignType"
local Profession = import "L10.Game.Profession"
local CUITexture=import "L10.UI.CUITexture"


CLuaQMPKSelfZhanDuiWnd = class()
RegistClassMember(CLuaQMPKSelfZhanDuiWnd,"TeamTab")
RegistClassMember(CLuaQMPKSelfZhanDuiWnd,"ApplicationsTab")
RegistClassMember(CLuaQMPKSelfZhanDuiWnd,"TeamInfoRoot")
RegistClassMember(CLuaQMPKSelfZhanDuiWnd,"ApplicationsRoot")
RegistClassMember(CLuaQMPKSelfZhanDuiWnd,"TeamRoot")
RegistClassMember(CLuaQMPKSelfZhanDuiWnd,"ApplicationRoot")
RegistClassMember(CLuaQMPKSelfZhanDuiWnd,"TipLabel")
RegistClassMember(CLuaQMPKSelfZhanDuiWnd,"SloganLabel")
RegistClassMember(CLuaQMPKSelfZhanDuiWnd,"memberPrefab")
RegistClassMember(CLuaQMPKSelfZhanDuiWnd,"MemberRoot")
RegistClassMember(CLuaQMPKSelfZhanDuiWnd,"QuitButton")
RegistClassMember(CLuaQMPKSelfZhanDuiWnd,"SettingButton")
RegistClassMember(CLuaQMPKSelfZhanDuiWnd,"ModifySloganButton")
RegistClassMember(CLuaQMPKSelfZhanDuiWnd,"m_NameLabel")
RegistClassMember(CLuaQMPKSelfZhanDuiWnd,"NameInput")
RegistClassMember(CLuaQMPKSelfZhanDuiWnd,"SloganInput")
RegistClassMember(CLuaQMPKSelfZhanDuiWnd,"ModifyCancelButton")
RegistClassMember(CLuaQMPKSelfZhanDuiWnd,"ModifyConfirmButton")
RegistClassMember(CLuaQMPKSelfZhanDuiWnd,"memberList")
RegistClassMember(CLuaQMPKSelfZhanDuiWnd,"AdvGridView")
RegistClassMember(CLuaQMPKSelfZhanDuiWnd,"ClearButton")
RegistClassMember(CLuaQMPKSelfZhanDuiWnd,"AcceptButton")
RegistClassMember(CLuaQMPKSelfZhanDuiWnd,"RejectButton")
RegistClassMember(CLuaQMPKSelfZhanDuiWnd,"m_ApplicationRetDotObj")
RegistClassMember(CLuaQMPKSelfZhanDuiWnd,"m_CurrentRequestPlayerIndex")
RegistClassMember(CLuaQMPKSelfZhanDuiWnd,"m_ApplicationFirstFlag")
RegistClassMember(CLuaQMPKSelfZhanDuiWnd,"m_RequestPlayers")

function CLuaQMPKSelfZhanDuiWnd:Awake()
    self.TeamTab = self.transform:Find("Wnd_Bg_Primary_Tab/Tabs/Tab1"):GetComponent(typeof(QnTabButton))
    self.ApplicationsTab = self.transform:Find("Wnd_Bg_Primary_Tab/Tabs/Tab2"):GetComponent(typeof(QnTabButton))
    self.TeamInfoRoot = self.transform:Find("ShowArea/MemberInfoRoot").gameObject
    self.ApplicationsRoot = self.transform:Find("ShowArea/RequestListRoot").gameObject
    self.TeamRoot = self.transform:Find("ShowArea/MemberInfoRoot").gameObject
    self.ApplicationRoot = self.transform:Find("ShowArea/RequestListRoot").gameObject
    self.TipLabel = self.transform:Find("ShowArea/MemberInfoRoot/TipLabel"):GetComponent(typeof(UILabel))
    self.SloganLabel = self.transform:Find("ShowArea/MemberInfoRoot/TopArea/SloganBG/SloganLabel"):GetComponent(typeof(UILabel))
    self.memberPrefab = self.transform:Find("ShowArea/MemberInfoRoot/MemberBG/MemberInfoItem").gameObject
    self.MemberRoot = self.transform:Find("ShowArea/MemberInfoRoot/MemberBG/MemberRoot")
    self.QuitButton = self.transform:Find("ShowArea/MemberInfoRoot/QuitButton").gameObject
    self.SettingButton = self.transform:Find("ShowArea/MemberInfoRoot/SettingButton").gameObject
    self.ModifySloganButton = self.transform:Find("ShowArea/MemberInfoRoot/TopArea/SloganBG/Sprite").gameObject
    self.m_NameLabel = self.transform:Find("ShowArea/MemberInfoRoot/TopArea/Name"):GetComponent(typeof(UILabel))
    self.memberList = {}
    self.AdvGridView = self.transform:Find("ShowArea/RequestListRoot/ApplicationAdvScrollView"):GetComponent(typeof(QnTableView))
    self.ClearButton = self.transform:Find("ShowArea/RequestListRoot/BottomArea/ClearButton").gameObject
    self.AcceptButton = self.transform:Find("ShowArea/RequestListRoot/BottomArea/AcceptButton").gameObject
    self.RejectButton = self.transform:Find("ShowArea/RequestListRoot/BottomArea/RejectButton").gameObject
    self.m_ApplicationRetDotObj = self.transform:Find("Wnd_Bg_Primary_Tab/Tabs/Tab2/Sprite").gameObject
    self.m_CurrentRequestPlayerIndex = -1
    self.m_ApplicationFirstFlag = true
    UIEventListener.Get(self.TeamTab.gameObject).onClick =DelegateFactory.VoidDelegate(function(go) self:onTeamTabClick(go) end)
    UIEventListener.Get(self.ApplicationsTab.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) self:onApplicationTabClick(go) end)
    UIEventListener.Get(self.QuitButton).onClick = DelegateFactory.VoidDelegate(function(go) self:onQuitTeamClick(go) end)
    UIEventListener.Get(self.SettingButton).onClick = DelegateFactory.VoidDelegate(function(go) self:onSettingClick(go) end)
    UIEventListener.Get(self.ClearButton).onClick = DelegateFactory.VoidDelegate(function(go) self:onClearClick(go) end)
    UIEventListener.Get(self.RejectButton).onClick = DelegateFactory.VoidDelegate(function(go) self:onRejecetClick(go) end)
    UIEventListener.Get(self.AcceptButton).onClick = DelegateFactory.VoidDelegate(function(go) self:onAcceptClick(go) end)
    UIEventListener.Get(self.ModifySloganButton).onClick = DelegateFactory.VoidDelegate(function(go) self:onModifySloganClick(go) end)

    self.m_ApplicationRetDotObj:SetActive(false)
end

function CLuaQMPKSelfZhanDuiWnd:Init( )
    self.SettingButton.gameObject:SetActive(false)
    self.AdvGridView.m_DataSource=DefaultTableViewDataSource.CreateByCount(0,function(item,row)
        if row % 2 == 1 then
            item:SetBackgroundTexture("common_bg_mission_background_n")
        else
            item:SetBackgroundTexture("common_bg_mission_background_s")
        end

        self:InitRequestPlayerItem(item,row,self.m_RequestPlayers[row+1])
    end)

    self.AdvGridView.OnSelectAtRow =DelegateFactory.Action_int(function (row)
        self:OnRequestPlayerItemClicked(row)
    end)

    self:onTeamTabClick(self.gameObject)

    Gac2Gas.RequestSelfQmpkZhanDuiInfo()
    Gac2Gas.RequestQmpkZhanDuiApplyList(1)
end

function CLuaQMPKSelfZhanDuiWnd:InitRequestPlayerItem(item,row,player)
    local transform=item.transform
    local m_BgSprite = transform:GetComponent(typeof(UISprite))
    local m_PlayerProfessionSprite = transform:Find("ClazzIconSprite"):GetComponent(typeof(UISprite))
    local m_PlayerNameLabel = transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local m_GoodAtClassSprite = {}
    m_GoodAtClassSprite[0]=transform:Find("GoodAtSprite1"):GetComponent(typeof(UISprite))
    m_GoodAtClassSprite[1]=transform:Find("GoodAtSprite2"):GetComponent(typeof(UISprite))
    m_GoodAtClassSprite[2]=transform:Find("GoodAtSprite3"):GetComponent(typeof(UISprite))

    local m_PlayerTimeLabel = transform:Find("OnlineTimeLabel"):GetComponent(typeof(UILabel))
    local m_PlayerCanCommandObj = transform:Find("CanCommand").gameObject
    local m_PlayerRemarkLabel = transform:Find("RemarkLabel"):GetComponent(typeof(UILabel))

    m_PlayerProfessionSprite.spriteName = Profession.GetIconByNumber(player.m_Clazz)
    m_PlayerNameLabel.text = player.m_Name
    m_PlayerRemarkLabel.text = player.m_Remark

    if player.m_CanCommand > 0 then
        m_PlayerCanCommandObj:SetActive(true)
    else
        m_PlayerCanCommandObj:SetActive(false)
    end
    local index = 0
    if player.m_GoodAtClass > 0 then
        for i = 0, 14 do
            if (bit.band(player.m_GoodAtClass, (bit.lshift(1, i)))) > 0 then
                m_GoodAtClassSprite[index].gameObject:SetActive(true)
                local default = index
                index = default + 1
                m_GoodAtClassSprite[default].spriteName = Profession.GetIconByNumber(i + 1)
            end
        end
    end
    for i = index, 2 do
        m_GoodAtClassSprite[i].gameObject:SetActive(false)
    end
    if player.m_OnlineTimeId > 0 then
        local time = QuanMinPK_OnlineTime.GetData(player.m_OnlineTimeId)
        if time ~= nil then
            m_PlayerTimeLabel.text = time.First .. "  " .. time.Second
        end
    end

end

function CLuaQMPKSelfZhanDuiWnd:OnRequestPlayerItemClicked( index) 
    self.m_CurrentRequestPlayerIndex = index
    if index < #self.m_RequestPlayers then
        CPlayerInfoMgr.ShowPlayerPopupMenu(
            self.m_RequestPlayers[index+1].m_Id, 
            EnumPlayerInfoContext.QMPK, 
            EChatPanel.Undefined, 
            nil, 
            nil, 
            Vector3.zero, 
            AlignType.Default)
    end
end
function CLuaQMPKSelfZhanDuiWnd:OnEnable( )
    g_ScriptEvent:AddListener("QMPKQueryZhanDuiInfoResult", self, "OnReplyZhanDuiInfo")
    g_ScriptEvent:AddListener("ReplyQmpkZhanDuiApplyList", self, "OnReplyRequestPlayers")
    g_ScriptEvent:AddListener("QMPKUpdateZhanDuiNameAndSlogan", self, "UpdateZhanDuiNameAndSlogan")
end
function CLuaQMPKSelfZhanDuiWnd:OnDisable( )
    g_ScriptEvent:RemoveListener("QMPKQueryZhanDuiInfoResult", self, "OnReplyZhanDuiInfo")
    g_ScriptEvent:RemoveListener("ReplyQmpkZhanDuiApplyList", self, "OnReplyRequestPlayers")
    g_ScriptEvent:RemoveListener("QMPKUpdateZhanDuiNameAndSlogan", self, "UpdateZhanDuiNameAndSlogan")
end
function CLuaQMPKSelfZhanDuiWnd:UpdateZhanDuiNameAndSlogan( )
    self.m_NameLabel.text = CQuanMinPKMgr.Inst.m_CurrentZhanduiName
    self.SloganLabel.text = CQuanMinPKMgr.Inst.m_CurrentZhanduiSlogan
    -- self.NameInput.value = CQuanMinPKMgr.Inst.m_CurrentZhanduiName
    -- self.SloganInput.value = CQuanMinPKMgr.Inst.m_CurrentZhanduiSlogan
end
function CLuaQMPKSelfZhanDuiWnd:OnReplyZhanDuiInfo( )
    if CQuanMinPKMgr.Inst.m_MySelfZhanduiId ~= CQuanMinPKMgr.Inst.m_CurrentZhanduiId then
        return
    end

    do
        local i = 0
        while i < CQuanMinPKMgr.Inst.m_ZhanDuiMemberMaxCount do
            local go=self.memberList[i+1].gameObject
            local tf=go.transform
            go:SetActive(true)
            if i < CQuanMinPKMgr.Inst.m_MemberInfoList.Count then
                local info=CQuanMinPKMgr.Inst.m_MemberInfoList[i]
                -- self.memberList[i+1]:SetMemberInfo(CQuanMinPKMgr.Inst.m_MemberInfoList[i], i, CQuanMinPKMgr.Inst.m_ChuZhanInfoList)
                self:InitMemberItem(tf,CQuanMinPKMgr.Inst.m_MemberInfoList[i], i, CQuanMinPKMgr.Inst.m_ChuZhanInfoList)
                
                UIEventListener.Get(go).onClick=DelegateFactory.VoidDelegate(function(p)
                    self:OnItemClicked(info.m_Id,p)
                end)
            else
                -- self.memberList[i+1]:SetMemberInfo(nil, i, nil)
                self:InitMemberItem(tf,nil,i,nil)
                -- UIEventListener.Get(go).onClick=nil
                UIEventListener.Get(go).onClick=DelegateFactory.VoidDelegate(function(p)
                    CUIManager.ShowUI(CLuaUIResources.QMPKRecruitWnd)
                end)
            end
            i = i + 1
        end
    end
    self.SloganLabel.text = CQuanMinPKMgr.Inst.m_CurrentZhanduiSlogan
    self.m_NameLabel.text = CQuanMinPKMgr.Inst.m_CurrentZhanduiName
    -- self.NameInput.value = CQuanMinPKMgr.Inst.m_CurrentZhanduiName
    -- self.SloganInput.value = CQuanMinPKMgr.Inst.m_CurrentZhanduiSlogan
    self.TipLabel.text = CQuanMinPKMgr.Inst.m_MyZhanduiTip
    --refreshInfo();
end
function CLuaQMPKSelfZhanDuiWnd:OnReplyRequestPlayers( players ) 
    -- local players=args[0]
    self.m_RequestPlayers = players

    if not self.m_ApplicationFirstFlag then
        self.AdvGridView.m_DataSource.count=#self.m_RequestPlayers
        self.AdvGridView:ReloadData(true, false)
        self.m_CurrentRequestPlayerIndex = -1
    end
    if #players > 0 and self.m_ApplicationFirstFlag and CQuanMinPKMgr.Inst.m_IsTeamLeader then
        self.m_ApplicationRetDotObj:SetActive(true)
    else
        self.m_ApplicationRetDotObj:SetActive(false)
    end


end
function CLuaQMPKSelfZhanDuiWnd:OnClearZhanDuiApplyListSuccess( )
    self.m_RequestPlayers={}
    self.AdvGridView.m_DataSource.count=0
    self.AdvGridView:ReloadData(true, false)
    self.m_CurrentRequestPlayerIndex = -1
end
function CLuaQMPKSelfZhanDuiWnd:initMembers( )
    self.memberList ={}
    self.memberPrefab:SetActive(false)
    do
        local i = 0
        while i < CQuanMinPKMgr.Inst.m_ZhanDuiMemberMaxCount do
            local go = NGUITools.AddChild(self.MemberRoot.gameObject,self.memberPrefab)
            go:SetActive(false)
            local trans = go.transform
            self:SetItemSelected(trans,false)
            table.insert( self.memberList,trans )
            i = i + 1
        end
        self.MemberRoot:GetComponent(typeof(UIGrid)):Reposition()
    end
    
end
function CLuaQMPKSelfZhanDuiWnd:OnItemClicked( playerId, go) 
    if playerId <= 0 then
        CUIManager.ShowUI(CLuaUIResources.QMPKRecruitWnd)
        return
    end
    CPlayerInfoMgr.ShowPlayerPopupMenu(math.floor(playerId), EnumPlayerInfoContext.QMPKSelfTeam, EChatPanel.Undefined, nil, nil, go.transform.position, CPlayerInfoMgr.AlignType.Right)

    do
        local i = 0 local cnt = #self.memberList--.Count
        while i < cnt do
            -- self.memberList[i+1]:SetSelected(self.memberList[i+1].gameObject == go)
            self:SetItemSelected(self.memberList[i+1],self.memberList[i+1].gameObject == go)
            i = i + 1
        end
    end
end
function CLuaQMPKSelfZhanDuiWnd:onTeamTabClick( go) 
    self.TeamTab.Selected = true
    self.ApplicationsTab.Selected = false
    self.TeamRoot:SetActive(true)
    self.ApplicationRoot:SetActive(false)
    if self.MemberRoot.childCount == 0 then
        self:initMembers()
    end
end
function CLuaQMPKSelfZhanDuiWnd:onApplicationTabClick( go) 
    self.TeamTab.Selected = false
    self.ApplicationsTab.Selected = true
    self.TeamRoot:SetActive(false)
    self.ApplicationRoot:SetActive(true)
    self.AdvGridView:Clear()
    if not self.m_ApplicationFirstFlag then
        Gac2Gas.RequestQmpkZhanDuiApplyList(1)
    else
        self.AdvGridView.m_DataSource.count = #self.m_RequestPlayers
        self.AdvGridView:ReloadData(false, false)
    end
    self.m_ApplicationFirstFlag = false
    self.m_ApplicationRetDotObj:SetActive(false)
end
function CLuaQMPKSelfZhanDuiWnd:onQuitTeamClick( go) 
    MessageWndManager.ShowOKCancelMessage(LocalString.GetString("确定要退出战队吗？"), DelegateFactory.Action(function()
        self:confirmQuit()
    end), nil, nil, nil, false)
end
function CLuaQMPKSelfZhanDuiWnd:confirmQuit( )
    Gac2Gas.RequestLeaveQmpkZhanDui()
    -- self:Close()
    CUIManager.CloseUI(CLuaUIResources.QMPKSelfZhanDuiWnd)
end
function CLuaQMPKSelfZhanDuiWnd:onSettingClick( go) 
    CUIManager.ShowUI(CLuaUIResources.QMPKZhanDuiSettingWnd)
end
function CLuaQMPKSelfZhanDuiWnd:onModifySloganClick( go) 
    -- self.ModifySloganRoot:SetActive(true)
    CUIManager.ShowUI(CLuaUIResources.QMPKModifySloganWnd)
end
function CLuaQMPKSelfZhanDuiWnd:onAcceptClick( go) 
    if self.m_CurrentRequestPlayerIndex >= 0 and self.m_CurrentRequestPlayerIndex < #self.m_RequestPlayers then
        Gac2Gas.AcceptApplyPlayerJoinQmpkZhanDui(self.m_RequestPlayers[self.m_CurrentRequestPlayerIndex+1].m_Id)
    else
        g_MessageMgr:ShowMessage("DOUHUN_NEED_ONE_PLAYER")
    end
end
function CLuaQMPKSelfZhanDuiWnd:onClearClick( go) 
    Gac2Gas.RequestClearQmpkZhanDuiApplyList()
end
function CLuaQMPKSelfZhanDuiWnd:onRejecetClick( go) 
    if self.m_CurrentRequestPlayerIndex >= 0 and self.m_CurrentRequestPlayerIndex < #self.m_RequestPlayers then
        Gac2Gas.RefuseApplyPlayerJoinQmpkZhanDui(self.m_RequestPlayers[self.m_CurrentRequestPlayerIndex+1].m_Id)
    else
        g_MessageMgr:ShowMessage("DOUHUN_NEED_ONE_PLAYER")
    end
end

function CLuaQMPKSelfZhanDuiWnd:SetItemSelected(transform,state)
    local m_SelectedObj = transform:Find("InfoRoot/Sprite").gameObject
    m_SelectedObj:SetActive(state)
end
function CLuaQMPKSelfZhanDuiWnd:InitMemberItem(transform,member, index, chuZhanInfo)
    local IconTexture = transform:Find("InfoRoot/Icon"):GetComponent(typeof(CUITexture))
    local LevelLabel = transform:Find("InfoRoot/LevelBG/LevelLabel"):GetComponent(typeof(UILabel))
    local NameLabel = transform:Find("InfoRoot/NameLabel"):GetComponent(typeof(UILabel))
    local LeaderMark = transform:Find("InfoRoot/LeaderMark").gameObject
    local ClazzMarkSprite = transform:Find("InfoRoot/ClazzSprite"):GetComponent(typeof(UISprite))
    local m_GroupObj = transform:Find("InfoRoot/ChuZhanSprite1").gameObject
    local m_ThreeVThreeObj = transform:Find("InfoRoot/ChuZhanSprite2").gameObject
    local m_SigleObj = transform:Find("InfoRoot/ChuZhanSprite3").gameObject
    local m_SigleIndexLabel = transform:Find("InfoRoot/ChuZhanSprite3/BG/Label"):GetComponent(typeof(UILabel))
    local m_SelectedObj = transform:Find("InfoRoot/Sprite").gameObject
    local m_NoMemberObj = transform:Find("Label").gameObject
    local m_InfoRoot = transform:Find("InfoRoot").gameObject
    local NoCertification = transform:Find("InfoRoot/NoCertification").gameObject

    local m_PlayerId=0

    if nil == member then
        m_NoMemberObj:SetActive(true)
        m_InfoRoot:SetActive(false)
        m_PlayerId = 0
        return
    end
    m_NoMemberObj:SetActive(false)
    m_InfoRoot:SetActive(true)
    m_SelectedObj:SetActive(false)
    m_PlayerId = member.m_Id
    IconTexture:LoadNPCPortrait(CUICommonDef.GetPortraitName(member.m_Class, member.m_Gender, -1), false)
    LevelLabel.text = System.String.Format("lv.{0}", member.m_Grade)
    NameLabel.text = member.m_Name
    LeaderMark:SetActive(index == 0)
    ClazzMarkSprite.spriteName = Profession.GetIcon(member.m_Class)
    if CClientMainPlayer.Inst ~= nil and m_PlayerId == CClientMainPlayer.Inst.Id then
        NameLabel.color = Color(16 / 255, 140/ 255, 0)
    else
        NameLabel.color = Color.white
    end
    if chuZhanInfo == nil or chuZhanInfo.Count < 5 then
        return
    end
    if (bit.band(chuZhanInfo[0], (bit.lshift(1, index)))) ~= 0 or (bit.band(chuZhanInfo[3], (bit.lshift(1, index)))) ~= 0 then
        m_GroupObj:SetActive(true)
    else
        m_GroupObj:SetActive(false)
    end
    if (bit.band(chuZhanInfo[1], (bit.lshift(1, index)))) ~= 0 or (bit.band(chuZhanInfo[4], (bit.lshift(1, index)))) ~= 0 then
        m_SigleObj:SetActive(true)
        if (bit.band(chuZhanInfo[1], (bit.lshift(1, index)))) ~= 0 then
            m_SigleIndexLabel.text = LocalString.GetString("一")
        else
            m_SigleIndexLabel.text = LocalString.GetString("二")
        end
    else
        m_SigleObj:SetActive(false)
    end
    if (bit.band(chuZhanInfo[2], (bit.lshift(1, index)))) ~= 0 then
        m_ThreeVThreeObj:SetActive(true)
    else
        m_ThreeVThreeObj:SetActive(false)
    end

    NoCertification:SetActive(member.m_Status == 2)
end
return CLuaQMPKSelfZhanDuiWnd
