local UIRoot=import "UIRoot"
local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local Vector3 = import "UnityEngine.Vector3"
local Object = import "System.Object"
local MessageWndManager = import "L10.UI.MessageWndManager"
local UICamera = import "UICamera"
local Physics = import "UnityEngine.Physics"
local CGuideBubbleDisplay = import "L10.UI.CGuideBubbleDisplay"
local TweenAlpha = import 'TweenAlpha'
local TweenPosition = import 'TweenPosition'
local BoxCollider = import 'UnityEngine.BoxCollider'

LuaRabbitMakeDrugWnd = class()
RegistChildComponent(LuaRabbitMakeDrugWnd,"CloseButton", GameObject)
RegistChildComponent(LuaRabbitMakeDrugWnd,"cai1", GameObject)
RegistChildComponent(LuaRabbitMakeDrugWnd,"cai2", GameObject)
RegistChildComponent(LuaRabbitMakeDrugWnd,"cai3", GameObject)
RegistChildComponent(LuaRabbitMakeDrugWnd,"cai4", GameObject)
RegistChildComponent(LuaRabbitMakeDrugWnd,"item1", GameObject)
RegistChildComponent(LuaRabbitMakeDrugWnd,"item2", GameObject)
RegistChildComponent(LuaRabbitMakeDrugWnd,"item3", GameObject)
RegistChildComponent(LuaRabbitMakeDrugWnd,"item4", GameObject)
RegistChildComponent(LuaRabbitMakeDrugWnd,"putDrugNode", GameObject)
RegistChildComponent(LuaRabbitMakeDrugWnd,"tuzi1", GameObject)
RegistChildComponent(LuaRabbitMakeDrugWnd,"tuzi2", GameObject)
RegistChildComponent(LuaRabbitMakeDrugWnd,"guide1", CGuideBubbleDisplay)
RegistChildComponent(LuaRabbitMakeDrugWnd,"region1", GameObject)
RegistChildComponent(LuaRabbitMakeDrugWnd,"region2", GameObject)

--RegistClassMember(LuaRabbitMakeDrugWnd, "maxChooseNum")

function LuaRabbitMakeDrugWnd:Close()
	MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("Confirm_CloseWindow"), DelegateFactory.Action(function ()
		CUIManager.CloseUI(CLuaUIResources.RabbitMakeDrugWnd)
	end), nil, nil, nil, false)
end

function LuaRabbitMakeDrugWnd:OnEnable()
	--g_ScriptEvent:AddListener("", self, "")
end

function LuaRabbitMakeDrugWnd:OnDisable()
	--g_ScriptEvent:RemoveListener("", self, "")
end

function LuaRabbitMakeDrugWnd:Init()
	local onCloseClick = function(go)
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.CloseButton,DelegateFactory.Action_GameObject(onCloseClick),false)

	self.chooseIndex = {1,3,2,4}
	self.nowIndex = 1

	local onDrag = function(go,delta)
		if self.dragNode and self.dragNode ~= go then
			return
		end
		if self.dragStart then
			return
		end
		if self.Finished then
			return
		end
		local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject);
		local trans = go.transform
		local pos = trans.localPosition
		pos.x = pos.x + delta.x*scale
		pos.y = pos.y + delta.y*scale
		trans.localPosition = pos
	end

	local onPress = function(go,flag)
		if self.dragNode and self.dragNode ~= go then
			return
		end
		if not flag then
			self.dragNode = nil
			local currentPos = UICamera.currentTouch.pos
			local ray = UICamera.currentCamera:ScreenPointToRay(Vector3(currentPos.x,currentPos.y,0))
			local hits = Physics.RaycastAll(ray, 99999,  UICamera.currentCamera.cullingMask)
			for i =0,hits.Length -1 do
				if hits[i].collider.gameObject.transform == self.putDrugNode.transform then
					self:JudgeFinish(go)
					return
				end
			end

			self:ReturnNode(go)
			--go.transform.parent = self.ori[go]
			--go.transform.localPosition = Vector3.zero
			--go.transform.localScale = self.oriScale[go]
		else
			go.transform.localScale = Vector3.one
			self.dragNode = go
		end
	end

	CommonDefs.AddOnDragListener(self.cai1,DelegateFactory.Action_GameObject_Vector2(onDrag),false)
	CommonDefs.AddOnDragListener(self.cai2,DelegateFactory.Action_GameObject_Vector2(onDrag),false)
	CommonDefs.AddOnDragListener(self.cai3,DelegateFactory.Action_GameObject_Vector2(onDrag),false)
	CommonDefs.AddOnDragListener(self.cai4,DelegateFactory.Action_GameObject_Vector2(onDrag),false)
	CommonDefs.AddOnPressListener(self.cai1,DelegateFactory.Action_GameObject_bool(onPress),false)
	CommonDefs.AddOnPressListener(self.cai2,DelegateFactory.Action_GameObject_bool(onPress),false)
	CommonDefs.AddOnPressListener(self.cai3,DelegateFactory.Action_GameObject_bool(onPress),false)
	CommonDefs.AddOnPressListener(self.cai4,DelegateFactory.Action_GameObject_bool(onPress),false)

	self.saveRegion2Pos = self.region2.transform.localPosition

	self:InitChatNode(1,true)
end

function LuaRabbitMakeDrugWnd:InitChatNode(pha,state)
	local chooseString = {
		{
			LocalString.GetString('想捣些刀伤止血的药材。'),
			LocalString.GetString('错啦，治刀伤的是半枫荷！')},
		{
			LocalString.GetString('再捣一味散寒止呕的药材吧。'),
			LocalString.GetString('错啦，散寒止呕的药是吴茱萸！')},
		{
			LocalString.GetString('还需一味治疗烫伤的药材。'),
			LocalString.GetString('错啦，治烫伤的是玉芙蓉！')},
		{
			LocalString.GetString('来都来了，把最后那味药也捣了吧！'),
			''}
	}
	local chatString = {
		'',
		'',
		LocalString.GetString('嗯，就是这个！'),
		LocalString.GetString('多捣几次就完成了！'),
		LocalString.GetString('这味药完成了！')
	}
	local tipString = {
		LocalString.GetString('拖动上方草药至药舂处'),
		'',
		LocalString.GetString('点击药杵上下捣药'),
		'',
		''
	}
	if self.nowIndex and chooseString[self.nowIndex] then
		chatString[1] = chooseString[self.nowIndex][1]
		chatString[2] = chooseString[self.nowIndex][2]
	end

	self.regionMoveDis = 15
	if state then
		self.region1:SetActive(true)
		self.region2:SetActive(false)
		self.region1.transform:Find('Label'):GetComponent(typeof(UILabel)).text = chatString[pha]
	else
		self.region1:SetActive(false)
		self.region2:SetActive(true)
		self.region2.transform:Find('Label'):GetComponent(typeof(UILabel)).text = chatString[pha]
		if self.m_AlertTick then
			UnRegisterTick(self.m_AlertTick)
			self.region2.transform.localPosition = self.saveRegion2Pos
			self.m_AlertTick = nil
		end
		self.regionAlertCount = 0
		self.m_AlertTick = RegisterTickWithDuration(function()
			self.regionAlertCount = self.regionAlertCount + 1
			local div = math.ceil(self.regionAlertCount/2)
			local der = (self.regionAlertCount % 2 - 0.5) * 2
			self.region2.transform.localPosition = Vector3(self.saveRegion2Pos.x + der * self.regionMoveDis / div,self.saveRegion2Pos.y,self.saveRegion2Pos.z)
		end,30,600)
	end
	--
	if tipString[pha] and tipString[pha] ~= '' then
		self.guide1.gameObject:SetActive(true)
		self.guide1:Init(tipString[pha])
	else
		self.guide1.gameObject:SetActive(false)
	end
end

function LuaRabbitMakeDrugWnd:ReturnNode(node)
	local nodeTable = {self.cai1,self.cai2,self.cai3,self.cai4}
	local nodeFTable = {self.item1,self.item2,self.item3,self.item4}
	for i,v in pairs(nodeTable) do
		if v == node then
			local fNode = nodeFTable[i]
			node.transform.localPosition = fNode.transform.localPosition
			return
		end
	end
end

function LuaRabbitMakeDrugWnd:ChooseRightNode(node)
	node:GetComponent(typeof(TweenAlpha)).enabled = true
	node:GetComponent(typeof(TweenPosition)).enabled = true
	local nodeTable = {self.cai1,self.cai2,self.cai3,self.cai4}
	for i,v in pairs(nodeTable) do
		v:GetComponent(typeof(BoxCollider)).enabled = false
	end
	self.putDrugNode:GetComponent(typeof(BoxCollider)).enabled = true
	local onPress = function(go,flag)
		if not flag then
			self.tuzi1:SetActive(true)
			self.tuzi2:SetActive(false)
			if self.startPress then
				self:InitChatNode(4,true)
				if not self.pressCount then
					self.pressCount = 0
				end
				self.pressCount = self.pressCount + 1
				if self.pressCount >= 5 then
					--self:FinishGame()
					self:CheckFinish(node)
				end
			end
		else
			self.tuzi1:SetActive(false)
			self.tuzi2:SetActive(true)
			if not self.startPress then
				self.startPress = true
			end
		end
	end
	CommonDefs.AddOnPressListener(self.putDrugNode,DelegateFactory.Action_GameObject_bool(onPress),false)
end

function LuaRabbitMakeDrugWnd:CheckFinish(finishNode)
	if finishNode then
		finishNode:SetActive(false)
	end
	self.nowIndex = self.nowIndex + 1
	if self.nowIndex <= 4 then
		local nodeTable = {self.cai1,self.cai2,self.cai3,self.cai4}
		for i,v in pairs(nodeTable) do
			v:GetComponent(typeof(BoxCollider)).enabled = true
		end
		self.pressCount = 0
		local onPress = function(go,flag)
		end
		CommonDefs.AddOnPressListener(self.putDrugNode,DelegateFactory.Action_GameObject_bool(onPress),false)
		self:InitChatNode(1,true)
	else
		self:FinishGame()
	end
end

function LuaRabbitMakeDrugWnd:FinishGame()
	self.putDrugNode:GetComponent(typeof(BoxCollider)).enabled = false
	self:InitChatNode(5,true)
	local empty = CreateFromClass(MakeGenericClass(List, Object))
	Gac2Gas.FinishClientTaskEvent(LuaZhuJueJuQingMgr.TaskId,"YuTuDaoYao",MsgPackImpl.pack(empty))
	RegisterTickWithDuration(function ()
		CUIManager.CloseUI(CLuaUIResources.RabbitMakeDrugWnd)
	end,2000,2000)
end

function LuaRabbitMakeDrugWnd:JudgeFinish(node)
	local nodeTable = {self.cai1,self.cai2,self.cai3,self.cai4}
	for i,v in pairs(nodeTable) do
		if v == node then
			if i == self.chooseIndex[self.nowIndex] then
				self:InitChatNode(3,true)
				self:ChooseRightNode(v)
			else
				self:InitChatNode(2,false)
				self:ReturnNode(v)
			end
			return
		end
	end
end

function LuaRabbitMakeDrugWnd:OnDestroy()
	if self.m_AlertTick then
		UnRegisterTick(self.m_AlertTick)
		self.m_AlertTick = nil
	end
end

return LuaRabbitMakeDrugWnd
