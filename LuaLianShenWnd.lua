require("common/common_include")

local CBaseWnd = import "L10.UI.CBaseWnd"
local UITabBar = import "L10.UI.UITabBar"
local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local CommonDefs = import "L10.Game.CommonDefs"
local LocalString = import "LocalString"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
local CLianShenSkillItem = import "L10.UI.CLianShenSkillItem"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local CButton = import "L10.UI.CButton"
local StringBuilder = import "System.Text.StringBuilder"
local MessageWndManager = import "L10.UI.MessageWndManager"

LuaLianShenWnd = class()
RegistClassMember(LuaLianShenWnd,"closeBtn")
RegistClassMember(LuaLianShenWnd,"m_TabBar")
RegistClassMember(LuaLianShenWnd,"m_InfoBtn")

RegistClassMember(LuaLianShenWnd,"m_BgDecorationTexture")
RegistClassMember(LuaLianShenWnd,"m_AttackRoot")
RegistClassMember(LuaLianShenWnd,"m_DefendRoot")
RegistClassMember(LuaLianShenWnd,"m_PVERoot")

RegistClassMember(LuaLianShenWnd,"m_AttackPos2ClsTbl") 	--攻击技能的pos到cls映射
RegistClassMember(LuaLianShenWnd,"m_DefendPos2ClsTbl") 	--防御技能的pos到cls映射
RegistClassMember(LuaLianShenWnd,"m_PVEPos2ClsTbl")		--pve技能的pos到cls映射
RegistClassMember(LuaLianShenWnd,"m_Cls2MaxLevelTbl")	--cls到技能最高等级映射

RegistClassMember(LuaLianShenWnd,"m_AttackSkillItemTbl")
RegistClassMember(LuaLianShenWnd,"m_DefendSkillItemTbl")
RegistClassMember(LuaLianShenWnd,"m_PVESkillItemTbl")

--技能详情
RegistClassMember(LuaLianShenWnd,"m_SkillNameLabel")
RegistClassMember(LuaLianShenWnd,"m_SkillMaxLevelLabel")
RegistClassMember(LuaLianShenWnd,"m_SkillProgressBar")
RegistClassMember(LuaLianShenWnd,"m_SkillProgressLabel")
RegistClassMember(LuaLianShenWnd,"m_SkillDescScrollView")
RegistClassMember(LuaLianShenWnd,"m_SkillDescLabel")

RegistClassMember(LuaLianShenWnd,"m_TrainRoot")
RegistClassMember(LuaLianShenWnd,"m_UpgradeRoot")
RegistClassMember(LuaLianShenWnd,"m_YinLiangCtrl")
RegistClassMember(LuaLianShenWnd,"m_YinPiaoCtrl")
RegistClassMember(LuaLianShenWnd,"m_MingWangCostLabel")
RegistClassMember(LuaLianShenWnd,"m_MingWangOwnLabel")
RegistClassMember(LuaLianShenWnd,"m_YinPiaoTips")
RegistClassMember(LuaLianShenWnd,"m_TrainBtn")
RegistClassMember(LuaLianShenWnd,"m_TrainFullBtn")
RegistClassMember(LuaLianShenWnd,"m_GuildContriCtrl")
RegistClassMember(LuaLianShenWnd,"m_UpgradeBtn")

RegistClassMember(LuaLianShenWnd,"m_CurSkillCls")
RegistClassMember(LuaLianShenWnd,"m_ActivatedAltarCount")

function LuaLianShenWnd:Awake()
    
end

function LuaLianShenWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaLianShenWnd:Init()
	self.closeBtn = self.transform:Find("Wnd_Bg_Primary_Normal/CloseButton").gameObject
	CommonDefs.AddOnClickListener(self.closeBtn, DelegateFactory.Action_GameObject(function(go) self:Close() end), false)
	self.m_TabBar = self.transform:Find("Anchor/Header/TabBar"):GetComponent(typeof(UITabBar))
	self.m_InfoBtn = self.transform:Find("Anchor/Header/InfoButton").gameObject

	self.m_BgDecorationTexture = self.transform:Find("Anchor/SkillTree/Background/Texture").gameObject
	self.m_AttackRoot = self.transform:Find("Anchor/SkillTree/Attack").gameObject
	self.m_DefendRoot = self.transform:Find("Anchor/SkillTree/Defend").gameObject
	self.m_PVERoot = self.transform:Find("Anchor/SkillTree/PVE").gameObject

	self.m_SkillNameLabel = self.transform:Find("Anchor/SkillDetail/Title"):GetComponent(typeof(UILabel))
	self.m_SkillMaxLevelLabel = self.transform:Find("Anchor/SkillDetail/Title/MaxLevel"):GetComponent(typeof(UILabel))
	self.m_SkillProgressBar = self.transform:Find("Anchor/SkillDetail/Progress"):GetComponent(typeof(UISlider))
	self.m_SkillProgressLabel = self.transform:Find("Anchor/SkillDetail/Progress/Value"):GetComponent(typeof(UILabel))
	self.m_SkillDescScrollView = self.transform:Find("Anchor/SkillDetail/ScrollView"):GetComponent(typeof(UIScrollView))
	self.m_SkillDescLabel = self.transform:Find("Anchor/SkillDetail/ScrollView/DescLabel"):GetComponent(typeof(UILabel))

	self.m_TrainRoot = self.transform:Find("Anchor/SkillDetail/TrainRoot").gameObject
	self.m_UpgradeRoot = self.transform:Find("Anchor/SkillDetail/UpgradeRoot").gameObject

	self.m_YinLiangCtrl = self.transform:Find("Anchor/SkillDetail/TrainRoot/Yinliang"):GetComponent(typeof(CCurentMoneyCtrl))
	self.m_YinPiaoCtrl = self.transform:Find("Anchor/SkillDetail/TrainRoot/Yinpiao"):GetComponent(typeof(CCurentMoneyCtrl))
	self.m_MingWangCostLabel = self.transform:Find("Anchor/SkillDetail/TrainRoot/MingWang/Cost"):GetComponent(typeof(UILabel))
	self.m_MingWangOwnLabel = self.transform:Find("Anchor/SkillDetail/TrainRoot/MingWang/Own"):GetComponent(typeof(UILabel))
	self.m_YinPiaoTips = self.transform:Find("Anchor/SkillDetail/TrainRoot/Tips").gameObject
	self.m_TrainBtn = self.transform:Find("Anchor/SkillDetail/TrainRoot/TrainButton"):GetComponent(typeof(CButton))
	self.m_TrainFullBtn = self.transform:Find("Anchor/SkillDetail/TrainRoot/TrainFullButton"):GetComponent(typeof(CButton))
	self.m_GuildContriCtrl = self.transform:Find("Anchor/SkillDetail/UpgradeRoot/GuildContri"):GetComponent(typeof(CCurentMoneyCtrl))
	self.m_UpgradeBtn = self.transform:Find("Anchor/SkillDetail/UpgradeRoot/UpgradeButton"):GetComponent(typeof(CButton))

	self.m_AttackSkillItemTbl = {}
	for i=1,9 do
		local item = self.m_AttackRoot.transform:Find("Skill_0"..tostring(i)):GetComponent(typeof(CLianShenSkillItem))
		CommonDefs.AddOnClickListener(item.transform:Find("SkillIcon").gameObject, DelegateFactory.Action_GameObject(function(go) self:OnSkillItemClick(item) end), false)
		table.insert(self.m_AttackSkillItemTbl, i, item)
	end

	self.m_DefendSkillItemTbl = {}
	for i=1,9 do
		local item = self.m_DefendRoot.transform:Find("Skill_0"..tostring(i)):GetComponent(typeof(CLianShenSkillItem))
		CommonDefs.AddOnClickListener(item.transform:Find("SkillIcon").gameObject, DelegateFactory.Action_GameObject(function(go) self:OnSkillItemClick(item) end), false)
		table.insert(self.m_DefendSkillItemTbl, i, item)
	end

	self.m_PVESkillItemTbl = {}
	for i=1,8 do
		local item = self.m_PVERoot.transform:Find("Skill_0"..tostring(i)):GetComponent(typeof(CLianShenSkillItem))
		CommonDefs.AddOnClickListener(item.transform:Find("SkillIcon").gameObject, DelegateFactory.Action_GameObject(function(go) self:OnSkillItemClick(item) end), false)
		table.insert(self.m_PVESkillItemTbl, i, item)
	end
	
	self.m_ActivatedAltarCount = 0
	self:LoadDesignData()
	self.m_TabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index) self:OnTabChange(go, index) end)
	self.m_TabBar:ChangeTab(0)
	CommonDefs.AddOnClickListener(self.m_InfoBtn, DelegateFactory.Action_GameObject(function(go) self:OnInfoButtonClick() end), false)

	CommonDefs.AddOnClickListener(self.m_TrainBtn.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnTrainButtonClick() end), false)
	CommonDefs.AddOnClickListener(self.m_TrainFullBtn.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnTrainFullButtonClick() end), false)
	CommonDefs.AddOnClickListener(self.m_UpgradeBtn.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnUpgradeButtonClick() end), false)
end

function LuaLianShenWnd:LoadDesignData()
	self.m_AttackPos2ClsTbl = {}
	self.m_DefendPos2ClsTbl = {}
	self.m_PVEPos2ClsTbl = {}
	self.m_Cls2MaxLevelTbl = {}
	LianShen_LianShen.ForeachKey(function(key)
	 	
	 	local cls = math.floor(key/100)
	 	local lv = key % 100
	 	if lv == 1 then
	 		local data = LianShen_LianShen.GetData(key)
	 		if data.Type == 1 then
	 			self.m_AttackPos2ClsTbl[data.Position] = cls
	 		elseif data.Type == 2 then
	 			self.m_DefendPos2ClsTbl[data.Position] = cls
	 		elseif data.Type == 3 then
	 			self.m_PVEPos2ClsTbl[data.Position] = cls
	 		end
	 	end
	 	if (not self.m_Cls2MaxLevelTbl[cls]) or self.m_Cls2MaxLevelTbl[cls] < lv then
	 		self.m_Cls2MaxLevelTbl[cls] = lv
	 	end

	end)
end

function LuaLianShenWnd:OnTabChange(go, index)
	self.m_AttackRoot:SetActive(index == 0)
	self.m_DefendRoot:SetActive(index == 1)
	self.m_PVERoot:SetActive(index == 2)
	self.m_BgDecorationTexture:SetActive(index~=2)
	self:RefreshSkillItems(true)
end

function LuaLianShenWnd:OnSkillItemClick(item)
	local tbl = {}
	if self.m_AttackRoot and self.m_AttackRoot.activeSelf then
		tbl = self.m_AttackSkillItemTbl
	elseif self.m_DefendRoot and self.m_DefendRoot.activeSelf then
		tbl = self.m_DefendSkillItemTbl
	elseif self.m_PVERoot and self.m_PVERoot.activeSelf then
		tbl = self.m_PVESkillItemTbl
	end

	for _,curItem in pairs(tbl) do
		local selected = (curItem == item)
		curItem.Selected = selected
		if selected then
			self:ShowDetail(curItem.cls)
		end
	end
end

function LuaLianShenWnd:RefreshSkillItems(forceSelected)
	local pos2ClsTbl = {}
	local itemTbl = {}
	if self.m_AttackRoot and self.m_AttackRoot.activeSelf then
		pos2ClsTbl = self.m_AttackPos2ClsTbl
		itemTbl = self.m_AttackSkillItemTbl
	elseif self.m_DefendRoot and self.m_DefendRoot.activeSelf then
		pos2ClsTbl = self.m_DefendPos2ClsTbl
		itemTbl = self.m_DefendSkillItemTbl
	elseif self.m_PVERoot and self.m_PVERoot.activeSelf then
		pos2ClsTbl = self.m_PVEPos2ClsTbl
		itemTbl = self.m_PVESkillItemTbl
	end

	if CClientMainPlayer.Inst == nil then
		for pos,item in pairs(itemTbl) do
			-- 初始化为默认值
			item:Init(0, 0, false)
		end
		return
	end

	for pos,item in pairs(itemTbl) do
		if pos2ClsTbl[pos] then
			local cls =  pos2ClsTbl[pos]
			local lv = CClientMainPlayer.Inst.SkillProp:GetLevelByLianShenSkillCls(cls)

			local unlocked = self:CheckPreConditionFitOrUnlock(cls, lv, false)
			item:Init(cls, lv, unlocked)
		end
	end

	if forceSelected and itemTbl[1] then
		self:OnSkillItemClick(itemTbl[1]) -- 默认选中第一个
	end
end


function LuaLianShenWnd:OnInfoButtonClick()
	g_MessageMgr:ShowMessage("LianShen_Tips")
end

function LuaLianShenWnd:OnTrainButtonClick()
	if not CClientMainPlayer.Inst then return end
	local lv = CClientMainPlayer.Inst.SkillProp:GetLevelByLianShenSkillCls(self.m_CurSkillCls)
	Gac2Gas.RequestPracticeLianShenSkill(self.m_CurSkillCls*100 + lv + 1, 1)
end

function LuaLianShenWnd:OnTrainFullButtonClick()
	if not CClientMainPlayer.Inst then return end
	local lv = CClientMainPlayer.Inst.SkillProp:GetLevelByLianShenSkillCls(self.m_CurSkillCls)

	local practiceNum = CClientMainPlayer.Inst.SkillProp:GetPracticeNumByLianShenSkillCls(self.m_CurSkillCls)
	local nextSkill = LianShen_LianShen.GetData(self.m_CurSkillCls * 100 + lv + 1)

	if nextSkill then
		local needSilverAndFreeSilver = (nextSkill.TotalPracticeNum - practiceNum) * nextSkill.CostMoney
		local needMingWang = (nextSkill.TotalPracticeNum - practiceNum) * nextSkill.CostMingWang
		if needSilverAndFreeSilver <=0 or needMingWang <-0 then return end
		MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("LianShen_Cannot_Full_Message", needMingWang, needSilverAndFreeSilver), DelegateFactory.Action(function () 
            
			Gac2Gas.RequestPracticeLianShenSkill(self.m_CurSkillCls*100 + lv + 1, 2)

    	end), nil, nil, nil, false)
	end
end

function LuaLianShenWnd:OnUpgradeButtonClick()
	if not CClientMainPlayer.Inst then return end
	local lv = CClientMainPlayer.Inst.SkillProp:GetLevelByLianShenSkillCls(self.m_CurSkillCls)
	Gac2Gas.RequestPracticeLianShenSkill(self.m_CurSkillCls*100 + lv + 1, 3)
end

function LuaLianShenWnd:CheckPreConditionFitOrUnlock(cls, lv, checkFit)
	local ret = false
	if not cls  then return ret end
	if not lv then return ret end
	if not CClientMainPlayer.Inst then return ret end

	if lv == 0 then lv = 1 end --0级特殊处理，否则无法取数据

	if not checkFit then
		lv = 1 -- 用1级技能来检查是否解锁
	end

	local data = LianShen_LianShen.GetData(cls * 100 + lv)
	if not data then return ret end

	if data.PreCondition then
		for i=0,data.PreCondition.Length-1 do
			local fit = true
			for j=0,data.PreCondition[i].Length-1 do
				local skillId = data.PreCondition[i][j]
				local _cls = math.floor(skillId/100)
				local _level = skillId % 100
				if CClientMainPlayer.Inst.SkillProp:GetLevelByLianShenSkillCls(_cls)<_level then
					fit = false
					break
				end
			end
			if fit then 
				ret = true 
				break
			end
		end
	else
		ret = true
	end
	return ret
end

function LuaLianShenWnd:ShowDetail(cls)
	self.m_CurSkillCls = cls
	if not cls or CClientMainPlayer.Inst == nil then return end
	if not self.m_Cls2MaxLevelTbl[cls] then return end

	local lv = CClientMainPlayer.Inst.SkillProp:GetLevelByLianShenSkillCls(cls)
	local praciceNum = CClientMainPlayer.Inst.SkillProp:GetPracticeNumByLianShenSkillCls(cls)

	local curSkill = LianShen_LianShen.GetData(cls * 100 + lv)
	local nextSkill = LianShen_LianShen.GetData(cls * 100 + lv + 1)

	if curSkill then
		self.m_SkillNameLabel.text = SafeStringFormat3("%s lv.%d", curSkill.Name, lv)
	else
		self.m_SkillNameLabel.text = nextSkill.Name.." "..LocalString.GetString("[FF0000]未激活[-]")
	end
	
	self.m_SkillMaxLevelLabel.text = SafeStringFormat3(LocalString.GetString("等级上限 lv.%d"), self.m_Cls2MaxLevelTbl[cls])

	if nextSkill then
		self.m_SkillProgressBar.value = praciceNum/nextSkill.TotalPracticeNum
		self.m_SkillProgressLabel.text = SafeStringFormat3("%d/%d", praciceNum, nextSkill.TotalPracticeNum)
	else
		self.m_SkillProgressBar.value = 1
		self.m_SkillProgressLabel.text = "-/-"
	end

	local builder = NewStringBuilderWraper(StringBuilder)

	if curSkill then
		builder:AppendLine(LocalString.GetString("[ACF8FF]当前[-]"))
		builder:AppendLine(Skill_AllSkills.GetData(cls * 100 + lv).TranslatedDisplay)
	end

	local conditionFit = true

	if nextSkill then
		builder:AppendLine(LocalString.GetString("[ACF8FF]下一级[-]"))
		builder:AppendLine(Skill_AllSkills.GetData(cls * 100 + lv + 1).TranslatedDisplay)
		if nextSkill.PreCondition then
			conditionFit = self:CheckPreConditionFitOrUnlock(cls, lv + 1, true)

			builder:AppendLine(LocalString.GetString("[ACF8FF]前置[-]"))

			for i=0,nextSkill.PreCondition.Length-1 do

				if i>0 then builder:AppendLine(LocalString.GetString("或者"))  end

				for j=0,nextSkill.PreCondition[i].Length-1 do
					local skillId = nextSkill.PreCondition[i][j]
					local _cls = math.floor(skillId/100)
					local _level = skillId % 100
					local text = nil
					if CClientMainPlayer.Inst.SkillProp:GetLevelByLianShenSkillCls(_cls)<_level then
						text = SafeStringFormat3("[FF0000]%s lv.%d[-]", Skill_AllSkills.GetData(skillId).Name, _level)
					else
						text = SafeStringFormat3("%s lv.%d", Skill_AllSkills.GetData(skillId).Name, _level)
					end

					if j%2==0 then
						builder:Append(text)
						if j == nextSkill.PreCondition[i].Length-1 then
							builder:Append("\n")
						end
					else
						builder:Append("        ")
						builder:Append(text)
						builder:Append("\n")
					end
				end
			end
		end
	else
		builder:AppendLine(LocalString.GetString("[ACF8FF]已满级[-]"))
	end

	local n = curSkill and curSkill.ActiveShengTanNum or LianShen_LianShen.GetData(cls * 100 + 1).ActiveShengTanNum -- 如果技能未激活，取第一级的圣诞数量信息
	if n>0 then
		builder:AppendLine(LocalString.GetString("[ACF8FF]生效条件[-]"))
		if curSkill and self.m_ActivatedAltarCount >= n then
			builder:Append(SafeStringFormat3(LocalString.GetString("激活%d个圣坛，当前已生效"), n))
		elseif curSkill then
			builder:Append(SafeStringFormat3(LocalString.GetString("激活%d个圣坛，[FF0000]当前未完全生效[-]"), n))
		else
			builder:Append(SafeStringFormat3(LocalString.GetString("激活%d个圣坛"), n))
		end
	end

	self.m_SkillDescLabel.text = builder:ToString()
	self.m_SkillDescScrollView:ResetPosition()
	--TODO 圣坛

	if nextSkill and praciceNum >= nextSkill.TotalPracticeNum then
		self.m_TrainRoot:SetActive(false)
		self.m_UpgradeRoot:SetActive(true)
		self.m_GuildContriCtrl:SetCost(nextSkill.CostContribution)

	elseif nextSkill and conditionFit then
		self.m_TrainRoot:SetActive(true)
		self.m_UpgradeRoot:SetActive(false)

		local freeSilver = CClientMainPlayer.Inst.FreeSilver
		if freeSilver>0 then
			self.m_YinPiaoCtrl.gameObject:SetActive(true)
			self.m_YinLiangCtrl.gameObject:SetActive(false)
			self.m_YinPiaoCtrl:SetCost(nextSkill.CostMoney)
			self.m_YinPiaoTips:SetActive(freeSilver < nextSkill.CostMoney)
		else
			self.m_YinPiaoCtrl.gameObject:SetActive(false)
			self.m_YinLiangCtrl.gameObject:SetActive(true)
			self.m_YinLiangCtrl:SetCost(nextSkill.CostMoney)
			self.m_YinPiaoTips:SetActive(false)
		end

		local ownMingWang = CClientMainPlayer.Inst.PlayProp.MingWangData.Score
		if ownMingWang < nextSkill.CostMingWang then
			self.m_MingWangOwnLabel.text = SafeStringFormat3("[FF0000]%d[-]", ownMingWang)
		else
			self.m_MingWangOwnLabel.text = tostring(ownMingWang)
		end
		self.m_MingWangCostLabel.text = tostring(nextSkill.CostMingWang)
		local enabled = ((CClientMainPlayer.Inst.Silver + freeSilver)>=nextSkill.CostMoney and ownMingWang>=nextSkill.CostMingWang)
		self.m_TrainFullBtn.Enabled = enabled
		self.m_TrainBtn.Enabled = enabled

	else
		self.m_TrainRoot:SetActive(false)
		self.m_UpgradeRoot:SetActive(false)
	end
end

function LuaLianShenWnd:OnEnable()
	g_ScriptEvent:AddListener("MainPlayerUpdateMoney", self, "UpdateMoney")
	g_ScriptEvent:AddListener("MainPlayerPlayPropUpdate", self, "UpdateMoney")
	g_ScriptEvent:AddListener("OnSyncMingWangData", self, "UpdateMoney")
	g_ScriptEvent:AddListener("OnPracticeLianShenSkillSuccess", self, "OnPracticeLianShenSkillSuccess")
	g_ScriptEvent:AddListener("OnReplyActivatedAltarCount", self, "OnReplyActivatedAltarCount")
	Gac2Gas.RequestActivatedAltarCount()
end

function LuaLianShenWnd:OnDisable()
	g_ScriptEvent:RemoveListener("MainPlayerUpdateMoney", self, "UpdateMoney")
	g_ScriptEvent:RemoveListener("MainPlayerPlayPropUpdate", self, "UpdateMoney")
	g_ScriptEvent:RemoveListener("OnSyncMingWangData", self, "UpdateMoney")
	g_ScriptEvent:RemoveListener("OnPracticeLianShenSkillSuccess", self, "OnPracticeLianShenSkillSuccess")
	g_ScriptEvent:RemoveListener("OnReplyActivatedAltarCount", self, "OnReplyActivatedAltarCount")
end

function LuaLianShenWnd:UpdateMoney()
	self:ShowDetail(self.m_CurSkillCls)
end

function LuaLianShenWnd:OnPracticeLianShenSkillSuccess(args)
	if not args then return end
	local cls = args[0]
	self:RefreshSkillItems(false)
	self:ShowDetail(self.m_CurSkillCls)
end

function LuaLianShenWnd:OnReplyActivatedAltarCount(count)
	self.m_ActivatedAltarCount = count
	self:ShowDetail(self.m_CurSkillCls)
end



function LuaLianShenWnd:OnDestroy()

end

return LuaLianShenWnd
