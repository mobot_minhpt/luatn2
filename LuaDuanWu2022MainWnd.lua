local UILabel = import "UILabel"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local PlayerPrefs = import "UnityEngine.PlayerPrefs"
local CScheduleMgr=import "L10.Game.CScheduleMgr"
local CUIFx = import "L10.UI.CUIFx"

LuaDuanWu2022MainWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaDuanWu2022MainWnd, "OpenningTimeLabel", "OpenningTimeLabel", UILabel)
RegistChildComponent(LuaDuanWu2022MainWnd, "WuduBtn", "WuduBtn", GameObject)
RegistChildComponent(LuaDuanWu2022MainWnd, "PopoBtn", "PopoBtn", GameObject)
RegistChildComponent(LuaDuanWu2022MainWnd, "PopoAlert", "PopoAlert", GameObject)
RegistChildComponent(LuaDuanWu2022MainWnd, "TongXinBtn", "TongXinBtn", GameObject)
RegistChildComponent(LuaDuanWu2022MainWnd, "LiBaoBtn", "LiBaoBtn", GameObject)
RegistChildComponent(LuaDuanWu2022MainWnd, "JiFenBtn", "JiFenBtn", GameObject)
RegistChildComponent(LuaDuanWu2022MainWnd, "PopoTimeLabel", "PopoTimeLabel", UILabel)
RegistChildComponent(LuaDuanWu2022MainWnd, "PopoFx", "PopoFx", GameObject)

--@endregion RegistChildComponent end

function LuaDuanWu2022MainWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.WuduBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnWuduBtnClick()
	end)

	UIEventListener.Get(self.PopoBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnPopoBtnClick()
	end)

	UIEventListener.Get(self.TongXinBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTongXinBtnClick()
	end)

	UIEventListener.Get(self.LiBaoBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLiBaoBtnClick()
	end)

	UIEventListener.Get(self.JiFenBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnJiFenBtnClick()
	end)

    --@endregion EventBind end
end

function LuaDuanWu2022MainWnd:Init()
	CLuaScheduleMgr.BuildInfos()

	local setting = DuanWu_Setting.GetData()

	local popoTime = setting.BubbleOpenTimeDescription

	self.OpenningTimeLabel.text = setting.FestivalMainPagePlayTime

	local _, _, m1, d1, m2, d2, h1, h2 = string.find(popoTime, LocalString.GetString("(%d+)月(%d+)日%-(%d+)月(%d+)日 (%d+):00%-(%d+):00"))
	local now = CServerTimeMgr.Inst:GetZone8Time()

	self.PopoAlert:SetActive(false)
	if now.Month >= tonumber(m1) and now.Month <= tonumber(m2) and 
		now.Day >= tonumber(d1) and now.Day <= tonumber(d2) and now.Hour >= tonumber(h1) and now.Hour < tonumber(h2) then
		local record = PlayerPrefs.GetInt("duanwu_2022_popo_alert")
		local mark = now.Month *100 + now.Day
		if record ~= mark then
			self.PopoAlert:SetActive(true)
		end

		self.PopoFx:SetActive(true)
	end

	if CommonDefs.IS_HMT_CLIENT then
		self.TongXinBtn.gameObject:SetActive(false)
	end
end

--@region UIEvent

function LuaDuanWu2022MainWnd:OnWuduBtnClick()
	CUIManager.ShowUI(CLuaUIResources.DuanWu2022WuduEnterWnd)
end

function LuaDuanWu2022MainWnd:OnPopoBtnClick()
	-- 设置并且记录红点
	self.PopoAlert:SetActive(false)
	local now = CServerTimeMgr.Inst:GetZone8Time()
	PlayerPrefs.SetInt("duanwu_2022_popo_alert", now.Month *100 + now.Day)

	CUIManager.ShowUI(CLuaUIResources.LiuYi2022PopoEnterWnd)
end

function LuaDuanWu2022MainWnd:OnTongXinBtnClick()
	local id = DuanWu_Setting.GetData().FestivalMainPageDailyTaskScheduleId
	CLuaScheduleMgr:ShowScheduleInfo(id)
end

function LuaDuanWu2022MainWnd:OnLiBaoBtnClick()
	-- 礼包活动
	local id = DuanWu_Setting.GetData().FestivalMainPageRMBScheduleId
	CLuaScheduleMgr:ShowScheduleInfo(id)
end

function LuaDuanWu2022MainWnd:OnJiFenBtnClick()
	local key = DuanWu_Setting.GetData().FestivalMainPageShopKey
	CLuaNPCShopInfoMgr.ShowScoreShop(key)
end


--@endregion UIEvent

