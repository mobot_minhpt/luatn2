local UIGrid = import "UIGrid"
local GuoQingJiaoChang_Cell = import "L10.Game.GuoQingJiaoChang_Cell"
local Vector3 = import "UnityEngine.Vector3"
local CUIFx = import "L10.UI.CUIFx"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local Random = import "UnityEngine.Random"
local Ease = import "DG.Tweening.Ease"
local RotateMode = import "DG.Tweening.RotateMode"

EnumGQJCDirection = {
	Up = 90,
	Down = -90,
	Left = 180,
	Right = 0,
}
LuaGQJCStateWnd = class()

RegistClassMember(LuaGQJCStateWnd, "TeamNumLabel")
RegistClassMember(LuaGQJCStateWnd, "TeamNumFx")
RegistClassMember(LuaGQJCStateWnd, "TimeLabel")
RegistClassMember(LuaGQJCStateWnd, "MapGrid")
RegistClassMember(LuaGQJCStateWnd, "MapCellTemplate")
RegistClassMember(LuaGQJCStateWnd, "ExpandButton")

RegistClassMember(LuaGQJCStateWnd, "PosRoot")
RegistClassMember(LuaGQJCStateWnd, "MainPlayerPos")
RegistClassMember(LuaGQJCStateWnd, "TeamMemberPos")

RegistClassMember(LuaGQJCStateWnd, "CellInfos")
RegistClassMember(LuaGQJCStateWnd, "BossState")
RegistClassMember(LuaGQJCStateWnd, "RemainTime")
RegistClassMember(LuaGQJCStateWnd, "RemainTeam")

RegistClassMember(LuaGQJCStateWnd, "CountDownTick")

RegistClassMember(LuaGQJCStateWnd, "TurnAroundInterval")
RegistClassMember(LuaGQJCStateWnd, "MoveToNextInterval")

RegistClassMember(LuaGQJCStateWnd, "m_PosInfos")
RegistClassMember(LuaGQJCStateWnd, "m_PlayerPosTable")

function LuaGQJCStateWnd:Init()
	self:InitClassMembers()
	self:InitValues()

end

function LuaGQJCStateWnd:InitClassMembers()
	self.TeamNumLabel = self.transform:Find("Anchor/Content/Title/BG/TeamNumLabel"):GetComponent(typeof(UILabel))
	self.TeamNumFx = self.transform:Find("Anchor/Content/Title/BG/TeamNumFx"):GetComponent(typeof(CUIFx))
	self.TeamNumFx.gameObject:SetActive(false)

	self.TimeLabel = self.transform:Find("Anchor/Content/Title/BG/TimeLabel"):GetComponent(typeof(UILabel))
	self.MapGrid = self.transform:Find("Anchor/Content/MapGrid"):GetComponent(typeof(UIGrid))
	self.MapCellTemplate =  self.transform:Find("Anchor/Content/MapCellTemplate").gameObject
	self.MapCellTemplate:SetActive(false)

	self.ExpandButton = self.transform:Find("Anchor/Tip/ExpandButton").gameObject

	self.PosRoot = self.transform:Find("Anchor/Content/PosView/PosRoot").gameObject
	self.MainPlayerPos = self.transform:Find("Anchor/Content/PosView/MainPlayerPos").gameObject
	self.MainPlayerPos:SetActive(false)
	self.TeamMemberPos = self.transform:Find("Anchor/Content/PosView/TeamMemberPos").gameObject
	self.TeamMemberPos:SetActive(false)

end

function LuaGQJCStateWnd:InitValues()

	CLuaGuoQingJiaoChangMgr.m_MapCells = {}

	self.m_PosInfos = {}
	self.m_PlayerPosTable = {}

	CUICommonDef.ClearTransform(self.PosRoot.transform)

	self.CellInfos = {}
	self.TeamNumLabel.text = ""
	self.TimeLabel.text = ""
	self.TurnAroundInterval = 0.2
	self.MoveToNextInterval = 0.6

	local onExpandButtonClicked = function (go)
		self:OnExpandButtonClicked(go)
	end
	CommonDefs.AddOnClickListener(self.ExpandButton,DelegateFactory.Action_GameObject(onExpandButtonClicked), false)

	self:InitMap()

end

function LuaGQJCStateWnd:InitMap()
	CUICommonDef.ClearTransform(self.MapGrid.transform)

	GuoQingJiaoChang_Cell.ForeachKey(DelegateFactory.Action_object(function (key) 
        local data = GuoQingJiaoChang_Cell.GetData(key)
        local go = NGUITools.AddChild(self.MapGrid.gameObject, self.MapCellTemplate)
        go:SetActive(true)
        -- grid排序
        local positionId = self:CellId2PositionId(key)
        go.name =  SafeStringFormat3("%02d", positionId)
        self:InitMapCell(go, data, positionId)
    end))
    self.MapGrid:Reposition()
end

function LuaGQJCStateWnd:CellId2PositionId(cellId)
	local yushu = cellId - math.floor((cellId - 1) /5) * 5
    local positionId = 6 - yushu + math.floor((cellId - 1) / 5) * 5
    return positionId
end


function LuaGQJCStateWnd:InitMapCell(go, data, positionId)
	local cell = LuaGQJCStateCell:new()
	-- cleared = 0 未通关
	-- shrinkState = 0 不需要闪烁
	-- bossState = 0 boss未激活
	cell:Init(go.transform, data.ID, positionId, data.Level, 0, 0, 0)
	CLuaGuoQingJiaoChangMgr.m_MapCells[data.ID] = cell
end

function LuaGQJCStateWnd:OnExpandButtonClicked(go)
	self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 0)
    CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end

function LuaGQJCStateWnd:UpdateMapCells(remainTime, remainTeam, bossState, cellInfos)
	if not cellInfos then return end

	self.BossState = bossState
	self.CellInfos = cellInfos
	
	if self.RemainTeam ~= remainTeam  then
		self.TeamNumFx:LoadFx(CUIFxPaths.GQJCTeamNumChangeFx)
		self.TeamNumFx.gameObject:SetActive(true)
		self.RemainTeam = remainTeam
		self.TeamNumLabel.text = SafeStringFormat3(LocalString.GetString("[91FAFF]队伍数[-] %s"), tostring(self.RemainTeam))
	end
	self.RemainTime = remainTime

	if self.CountDownTick ~= nil then
      UnRegisterTick(self.CountDownTick)
      self.CountDownTick=nil
    end

    local totalTime = self.RemainTime+1
	self.CountDownTick = RegisterTickWithDuration(function ()
		if self.RemainTime > 0 then
			self.TeamNumLabel.text = SafeStringFormat3(LocalString.GetString("[91FAFF]队伍数[-] %s"), tostring(self.RemainTeam))
			self.RemainTime = self.RemainTime - 1
			self.TimeLabel.text = self:Second2Minus(self.RemainTime)
		end
	end, 1000, 1000 * totalTime)

	for i = 1, #self.CellInfos, 1 do
		local cellId = self.CellInfos[i].cellId
		local cleared = self.CellInfos[i].cleared
		local shrinkState = self.CellInfos[i].shrinkState
		local cell = CLuaGuoQingJiaoChangMgr.m_MapCells[cellId]
		if cell then
			cell:UpdateCell(cleared, shrinkState, self.BossState)
		end
	end
end

-- 更新玩家及队友位置
-- 玩家如果活着，就黄点显示所在区域；玩家如果死亡，有尸体的情况，黄点显示尸体所在区域；如果没有尸体（被淘汰），地图上不显示这个人的黄点
function LuaGQJCStateWnd:UpdatePlayerPositions(posInfos)

	if not posInfos then return end

	for i = 1, #posInfos, 1 do
		local playerId = posInfos[i].playerId
		local cellId = posInfos[i].cellId

		-- 第一次出现点，不需要移动动画

		if not self.m_PosInfos[playerId] and not self.m_PlayerPosTable[playerId] then
			self.m_PosInfos[playerId] = cellId

			local pos = self:GetPlayerPos(cellId)
			local posGo = nil
			if playerId == CClientMainPlayer.Inst.Id then
				posGo = NGUITools.AddChild(self.PosRoot.gameObject, self.MainPlayerPos)
			else
				posGo = NGUITools.AddChild(self.PosRoot.gameObject, self.TeamMemberPos)
			end
			self.m_PlayerPosTable[playerId] = posGo

			posGo:SetActive(true)
			posGo.transform.localPosition = pos

		else -- 移动动画
			if self.m_PosInfos[playerId] ~= cellId then
				local oldCellId = self.m_PosInfos[playerId]
				self.m_PosInfos[playerId] = cellId

				local newPos = self:GetPlayerPos(cellId)
				local posGo = self.m_PlayerPosTable[playerId]

				-- 调整主玩家箭头的方向
				if playerId == CClientMainPlayer.Inst.Id then
					
					local oldCell = CLuaGuoQingJiaoChangMgr.m_MapCells[oldCellId]
					local newCell = CLuaGuoQingJiaoChangMgr.m_MapCells[cellId]

					local oldPositionId = oldCell:GetPositionId()
					local newPositionId = newCell:GetPositionId()
					local posZ = self:GetMainPlayerDirection(oldPositionId, newPositionId)
					
					if posGo and posZ ~= -1 and posGo.transform then
						local direction = Vector3(0, 0, posZ)
						posGo.transform.localRotation = direction
						LuaTweenUtils.DOLocalRotate(posGo.transform, direction, self.TurnAroundInterval, RotateMode.Fast)
					end
				end

				if posGo then
					CommonDefs.SetEase_Tweener(LuaTweenUtils.DOLocalMove(posGo.transform, newPos, self.MoveToNextInterval, false), Ease.Linear)
				end
			end
		end
	end

	-- 处理玩家被淘汰的情况
	for playerId, cellId in pairs(self.m_PosInfos) do
		local isOut = self:IsOut(playerId, posInfos)
		if isOut then
			local posGo = self.m_PlayerPosTable[playerId]
			-- 删除
			if posGo then 
				GameObject.Destroy(posGo)
			end
			self.m_PlayerPosTable[playerId] = nil
			self.m_PosInfos[playerId] = nil
		end
	end
end

-- 玩家是否淘汰
function LuaGQJCStateWnd:IsOut(playerId, posInfos)
	local isIn = false

	for i = 1, #posInfos, 1 do
		local surviveId = posInfos[i].playerId
		if surviveId == playerId then
			isIn = true
		end
	end
	return (not isIn)
end

function LuaGQJCStateWnd:GetMainPlayerDirection(oldPositionId, newPositionId)
	if newPositionId - oldPositionId == 1 then
		return EnumGQJCDirection.Down
	elseif newPositionId - oldPositionId == -1 then
		return EnumGQJCDirection.Up
	elseif newPositionId - oldPositionId > 1 then
		return EnumGQJCDirection.Right
	elseif newPositionId - oldPositionId < -1 then
		return EnumGQJCDirection.Left
	end
	return -1
end


function LuaGQJCStateWnd:GetPlayerPos(cellId)
	local cell = CLuaGuoQingJiaoChangMgr.m_MapCells[cellId]
	local newPos = cell:GetPos()
	-- random 20
	local bias = Random.Range(-7, 7)
	local newX = newPos.x + bias
	local newY = newPos.y + bias
	newPos = Vector3(newX, newY, 0)
	return newPos
end


function LuaGQJCStateWnd:Second2Minus(time)
	local minutes = math.floor(time / 60)
	local seconds = time - minutes * 60
	return SafeStringFormat3("%02d:%02d", minutes, seconds)
end

function LuaGQJCStateWnd:OnEnable()
	g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
	g_ScriptEvent:AddListener("UpdateGQJCState", self, "UpdateMapCells")
	g_ScriptEvent:AddListener("UpdateGQJCPosition", self, "UpdatePlayerPositions")
end

function LuaGQJCStateWnd:OnDisable()
	g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
	g_ScriptEvent:RemoveListener("UpdateGQJCState", self, "UpdateMapCells")
	g_ScriptEvent:RemoveListener("UpdateGQJCPosition", self, "UpdatePlayerPositions")

	if self.CountDownTick ~= nil then
      UnRegisterTick(self.CountDownTick)
      self.CountDownTick=nil
    end
   	
end

function LuaGQJCStateWnd:OnDestroy()
	CLuaGuoQingJiaoChangMgr.m_MapCells = {}
	self.m_PosInfos = {}
	self.m_PlayerPosTable = {}
end

function LuaGQJCStateWnd:OnHideTopAndRightTipWnd()
    self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 180)
end

return LuaGQJCStateWnd
