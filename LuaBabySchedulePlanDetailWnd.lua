require("common/common_include")

local Baby_Plan = import "L10.Game.Baby_Plan"
local Item_Item = import "L10.Game.Item_Item"
local BabyMgr = import "L10.Game.BabyMgr"
local Vector3 = import "UnityEngine.Vector3"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaBabySchedulePlanDetailWnd = class()

RegistClassMember(LuaBabySchedulePlanDetailWnd, "Anchor")
RegistClassMember(LuaBabySchedulePlanDetailWnd, "PlanNameLabel")
RegistClassMember(LuaBabySchedulePlanDetailWnd, "PlanDescLabel")
RegistClassMember(LuaBabySchedulePlanDetailWnd, "NeedTiliLabel")
RegistClassMember(LuaBabySchedulePlanDetailWnd, "NeedItemLabel")
RegistClassMember(LuaBabySchedulePlanDetailWnd, "ExpLabel")
RegistClassMember(LuaBabySchedulePlanDetailWnd, "ExtraAwardLabel")
RegistClassMember(LuaBabySchedulePlanDetailWnd, "PropDeltaLabel")

-- UI迭代后增加
RegistChildComponent(LuaBabySchedulePlanDetailWnd, "BG", UISprite)
RegistChildComponent(LuaBabySchedulePlanDetailWnd, "Operations", GameObject)
RegistChildComponent(LuaBabySchedulePlanDetailWnd, "CancelBtn", GameObject)
RegistChildComponent(LuaBabySchedulePlanDetailWnd, "StatusLabel", UILabel)

function LuaBabySchedulePlanDetailWnd:Init()
	self:InitClassMembers()
	self:InitValues()
end

function LuaBabySchedulePlanDetailWnd:InitClassMembers()
	self.Anchor = self.transform:Find("Anchor").transform

	self.PlanNameLabel = self.transform:Find("Anchor/BG/PlanNameLabel"):GetComponent(typeof(UILabel))
	self.PlanDescLabel = self.transform:Find("Anchor/BG/Grid/PlanDescLabel"):GetComponent(typeof(UILabel))
	self.NeedTiliLabel = self.transform:Find("Anchor/BG/Grid/NeedTiliLabel"):GetComponent(typeof(UILabel))
	self.NeedItemLabel = self.transform:Find("Anchor/BG/Grid/NeedItemLabel"):GetComponent(typeof(UILabel))
	self.ExpLabel = self.transform:Find("Anchor/BG/Grid/ExpLabel"):GetComponent(typeof(UILabel))
	self.ExtraAwardLabel = self.transform:Find("Anchor/BG/Grid/ExtraAwardLabel"):GetComponent(typeof(UILabel))
	self.PropDeltaLabel = self.transform:Find("Anchor/BG/Grid/PropDeltaLabel"):GetComponent(typeof(UILabel))
	
end

function LuaBabySchedulePlanDetailWnd:InitValues()
	self:UpdatePlanDetailInfo(LuaBabyMgr.m_SelectedDetailPlanId)
	self:UpdatePlanStatus()

	if LuaBabyMgr.m_SelectedDetailPlanPos then
		local localPos = self.Anchor.parent:InverseTransformPoint(LuaBabyMgr.m_SelectedDetailPlanPos)
		if LuaBabyMgr.m_AddTargetColumn == 0 then
			self.Anchor.localPosition = Vector3(localPos.x + 500, 0, 0)
		else
			self.Anchor.localPosition = Vector3(localPos.x - 500, 0, 0)
		end
	end
end


function LuaBabySchedulePlanDetailWnd:UpdatePlanDetailInfo(planId)
	-- body
	local plan = Baby_Plan.GetData(planId)
	if not plan then return end

	self.PlanNameLabel.text = SafeStringFormat3("[%s]%s-%s[-]", LuaBabyMgr.GetSchedulePlanColor(plan.Color), plan.Name, plan.Description)
	self.NeedTiliLabel.text = SafeStringFormat3(LocalString.GetString("[ACF8FF]消耗体力[-]  %s"), tostring(plan.TiLiCost))

	--ItemCost
	local itemCost = Item_Item.GetData(plan.ItemCost)
	if itemCost then
		self.NeedItemLabel.text = SafeStringFormat3(LocalString.GetString("[ACF8FF]消耗道具[-]  %s"), itemCost.Name)
	else
		self.NeedItemLabel.text = SafeStringFormat3(LocalString.GetString("[ACF8FF]消耗道具[-]  无"))
	end

	self.ExpLabel.text = SafeStringFormat3(LocalString.GetString("[ACF8FF]获得经验[-]  %s"), tostring(plan.Exp))
	-- ExtraItemId
	if plan.ExtraItemProb == 0 then
		self.ExtraAwardLabel.gameObject:SetActive(false)
	else
		local extraItem = Item_Item.GetData(plan.ExtraItemId)
		if extraItem then
			self.ExtraAwardLabel.text = SafeStringFormat3(LocalString.GetString("[ACF8FF]可能获得道具[-]  %s"), extraItem.Name)
		else
			self.ExtraAwardLabel.text = SafeStringFormat3(LocalString.GetString("[ACF8FF]可能获得道具[-]  无"))
		end
	end

	local gainStr = ""
	local status = BabyMgr.Inst:GetSelectedBabyStatus()
	-- 根据宝宝的状态显示属性
	if status == EnumBabyStatus.eChild then
		local count = plan.ChildPropDelta.Length
		for i = 0, count-1 do
			local property = plan.ChildPropDelta[i][0]
			local delta = plan.ChildPropDelta[i][1]

			local addLabel = "+"
			if delta < 0 then
				addLabel = "-"
			end
			if i == count-1 then
				gainStr = SafeStringFormat3("%s%s%s%d", gainStr, LuaBabyMgr.GetPropName(EnumBabyStatus.eChild, property), addLabel, math.abs(delta))
			else
				gainStr = SafeStringFormat3(LocalString.GetString("%s%s%s%d,"), gainStr, LuaBabyMgr.GetPropName(EnumBabyStatus.eChild, property), addLabel, math.abs(delta))
			end
			
		end
		
	elseif status == EnumBabyStatus.eYoung then
		local count = plan.YoungPropDelta.Length
		for i = 0, count-1 do
			local property = plan.YoungPropDelta[i][0]
			local delta = plan.YoungPropDelta[i][1]

			local addLabel = "+"
			if delta < 0 then
				addLabel = "-"
			end
			if i == count-1 then
				gainStr = SafeStringFormat3("%s%s%s%d", gainStr, LuaBabyMgr.GetPropName(EnumBabyStatus.eYoung, property), addLabel, math.abs(delta))
			else
				gainStr = SafeStringFormat3(LocalString.GetString("%s%s%s%d,"), gainStr, LuaBabyMgr.GetPropName(EnumBabyStatus.eYoung, property), addLabel, math.abs(delta))
			end
			
		end
	end
	if plan.ShowPropDelta == 0 then
		self.PropDeltaLabel.text = LocalString.GetString("[ACF8FF]可能获得属性[-]  随机")
	else
		self.PropDeltaLabel.text = SafeStringFormat3(LocalString.GetString("[ACF8FF]可能获得属性[-]  %s"), gainStr)
	end
	
	self.PlanDescLabel.text = SafeStringFormat3(LocalString.GetString("[ACF8FF]任务描述[-]  %s"), plan.DescriptionGo)
end

function LuaBabySchedulePlanDetailWnd:UpdatePlanStatus()
	if not self.Operations then return end

	self.CancelBtn:SetActive(false)
	self.StatusLabel.gameObject:SetActive(false)

	if LuaBabyMgr.m_IsSchedulePlanned then
		self.CancelBtn:SetActive(false)
		self.StatusLabel.gameObject:SetActive(true)
		if LuaBabyMgr.m_SelectedPlanInfo then
			local isFinished = LuaBabyMgr.m_SelectedPlanInfo.Finished == 1 -- 1代表已经完成
			local finishTime = LuaBabyMgr.m_SelectedPlanInfo.FinishTime

			local statusText = ""

			if isFinished then -- 已完成，完成时间比现在早
				statusText = LocalString.GetString("[00ff00]已完成[-]")
				self.StatusLabel.text = statusText
			elseif finishTime - CServerTimeMgr.Inst.timeStamp >= 0 then
				if finishTime - CServerTimeMgr.Inst.timeStamp < 3600  then
					statusText = LocalString.GetString("[FFFE91]进行中[-]")
				else
					statusText = LocalString.GetString("[FFFFFF]未开始[-]")
				end
				self.StatusLabel.text = statusText
			end
		end
	else
		self.CancelBtn:SetActive(true)
		self.StatusLabel.gameObject:SetActive(false)
		-- todo 一个广播，
		CommonDefs.AddOnClickListener(self.CancelBtn, DelegateFactory.Action_GameObject(function ()
			g_ScriptEvent:BroadcastInLua("BabyCancelSelectedPlan", LuaBabyMgr.m_SelectedDetailPlanIndex)
			CUIManager.CloseUI(CLuaUIResources.BabySchedulePlanDetailWnd)
		end), false)
	end
end
return LuaBabySchedulePlanDetailWnd
