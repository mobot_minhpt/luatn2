local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local QnTableView = import "L10.UI.QnTableView"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local GameObject = import "UnityEngine.GameObject"

LuaLuoCha2021LearnSkillWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaLuoCha2021LearnSkillWnd, "TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaLuoCha2021LearnSkillWnd, "TableView", "TableView", QnTableView)
RegistChildComponent(LuaLuoCha2021LearnSkillWnd, "ScrollView", "ScrollView", CUIRestrictScrollView)
RegistChildComponent(LuaLuoCha2021LearnSkillWnd, "CommitBtn", "CommitBtn", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaLuoCha2021LearnSkillWnd,"m_TableViewDataSource")
RegistClassMember(LuaLuoCha2021LearnSkillWnd,"m_SkillData")
RegistClassMember(LuaLuoCha2021LearnSkillWnd,"m_CurrentSelect")

function LuaLuoCha2021LearnSkillWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.CommitBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCommitBtnClick()
	end)


    --@endregion EventBind end
end

function LuaLuoCha2021LearnSkillWnd:Init()
	self.m_CurrentSelect = nil

	if LuaLuoCha2021Mgr.m_SkillInfo then
		self:InitSkillInfo(LuaLuoCha2021Mgr.m_SkillInfo)
	else
		CUIManager.CloseUI(CLuaUIResources.LuoCha2021LearnSkillWnd)
		return
	end

	self.TitleLabel.text = SafeStringFormat3(LocalString.GetString("当前可学习%s个技能"), tostring(LuaLuoCha2021Mgr.m_LearnSkillCount))

	self.TableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        self:OnSelectAtRow(row)
    end)
end

function LuaLuoCha2021LearnSkillWnd:InitSkillInfo(info)
	if info then
		self.m_SkillData = {}
		for i=0, info.Count-1 do
			table.insert(self.m_SkillData, info[i])
		end
		local initFunc = function(item, index)
			self:InitItem(item, index)
		end
		self.m_TableViewDataSource = DefaultTableViewDataSource.CreateByCount(#self.m_SkillData, initFunc)
		self.TableView.m_DataSource = self.m_TableViewDataSource
		self.TableView:ReloadData(true,false)
	end
end

function LuaLuoCha2021LearnSkillWnd:InitItem(item, index)
	local tf = item.transform

	local iconTexture = tf:Find("IconTexture"):GetComponent(typeof(CUITexture))
	local levelLabel = tf:Find("LevelLabel"):GetComponent(typeof(UILabel))
	local nameLabel = tf:Find("NameLabel"):GetComponent(typeof(UILabel))
	local descLabel = tf:Find("DescLabel"):GetComponent(typeof(UILabel))

	local skillId = self.m_SkillData[index+1]
	local data = Skill_AllSkills.GetData(skillId)

	levelLabel.text = "lv".. tostring(skillId%100)
	nameLabel.text = data.Name
	descLabel.text = data.Display_jian
	iconTexture:LoadSkillIcon(data.SkillIcon)
end

function LuaLuoCha2021LearnSkillWnd:OnSelectAtRow(index)
	self.m_CurrentSelect = self.m_SkillData[index+1]
end

function LuaLuoCha2021LearnSkillWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncLuoChaCanLearnSkillInfo", self, "InitSkillInfo")
end

function LuaLuoCha2021LearnSkillWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncLuoChaCanLearnSkillInfo", self, "InitSkillInfo")
end

--@region UIEvent

function LuaLuoCha2021LearnSkillWnd:OnCommitBtnClick()
	if self.m_CurrentSelect then
		-- rpc
		Gac2Gas.PlayerSelectSkill(self.m_CurrentSelect)

		-- 关闭窗口或者更新数量
		if LuaLuoCha2021Mgr.m_LearnSkillCount <=1 then
			CUIManager.CloseUI(CLuaUIResources.LuoCha2021LearnSkillWnd)
		else
			self.TitleLabel.text = SafeStringFormat3(LocalString.GetString("当前可学习%s个技能"), tostring(LuaLuoCha2021Mgr.m_LearnSkillCount-1))
		end
		self.m_CurrentSelect = nil
	else
		g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("请先选择一个技能"))
	end
end

--@endregion UIEvent

