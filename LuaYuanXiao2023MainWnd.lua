local CScheduleMgr = import "L10.Game.CScheduleMgr"
local BoxCollider = import "UnityEngine.BoxCollider"
local CStatusMgr = import "L10.Game.CStatusMgr"
local EPropStatus = import "L10.Game.EPropStatus"
local Animation = import "UnityEngine.Animation"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CParallaxEffectHelper = import "L10.Engine.CParallaxEffectHelper"
local Mathf = import "UnityEngine.Mathf"
local CTaskMgr = import "L10.Game.CTaskMgr"

LuaYuanXiao2023MainWnd = class()

RegistClassMember(LuaYuanXiao2023MainWnd, "YuanXiaoDefense")
RegistClassMember(LuaYuanXiao2023MainWnd, "YuanXiaoDefenseRedDot")
RegistClassMember(LuaYuanXiao2023MainWnd, "GuessRiddle")
RegistClassMember(LuaYuanXiao2023MainWnd, "QiYuan")
RegistClassMember(LuaYuanXiao2023MainWnd, "QiYuanRedDot")
RegistClassMember(LuaYuanXiao2023MainWnd, "TianJiangBaoXiang")
RegistClassMember(LuaYuanXiao2023MainWnd, "DengHui")
RegistClassMember(LuaYuanXiao2023MainWnd, "DengHuiRedDot")

-- Anim
RegistClassMember(LuaYuanXiao2023MainWnd, "anim")
RegistClassMember(LuaYuanXiao2023MainWnd, "animLength")

-- Gyro
RegistClassMember(LuaYuanXiao2023MainWnd, "gyroOffset")
RegistClassMember(LuaYuanXiao2023MainWnd, "gyroLayer")
RegistClassMember(LuaYuanXiao2023MainWnd, "lastAccVal")

-- 1~5层, 层数越小越靠近背景
function LuaYuanXiao2023MainWnd:DEFINE_GYRO_EFFECT()
    if CParallaxEffectHelper.Inst:SupportsEffect() then
        self.gyroOffset = {
            {x = 10, y = 10}, 
            {x = 0, y = 10},
            {x = 20, y = 10},
            {x = 30, y = 10},
            {x = 60, y = 10},
        }

        self.gyroLayer = {}
        self.gyroLayer[1] = {
            self:GetTrans("Background/Sky"),
        }
        self.gyroLayer[2] = {
            self:GetTrans("Background/Building"),
        }
        self.gyroLayer[3] = {
            self:GetTrans("Background/Water"),
            self:GetTrans("Background/Tower02"),
            self:GetTrans("DengHui")
        }
        self.gyroLayer[4] = {
            self:GetTrans("Background/Tower01"),
            self:GetTrans("Background/Lantern_M"),
            self:GetTrans("YuanXiaoDefense"),
        }
        self.gyroLayer[5] = {
            self:GetTrans("Background/Lantern_L"),
            self:GetTrans("Background/Lantern_S"),
            self:GetTrans("Background/Tree"),
            self:GetTrans("Background/Cloud"),
            self:GetTrans("QiYuan"),
            self:GetTrans("GuessRiddle"),
            self:GetTrans("TianJiangBaoXiang"),
        }

        self.lastAccVal = CParallaxEffectHelper.Inst:GetCurrentMovement()
    end
end

function LuaYuanXiao2023MainWnd:Update()
    if CParallaxEffectHelper.Inst:SupportsEffect() then
        local curAccVal = CParallaxEffectHelper.Inst:GetCurrentMovement()
        self.lastAccVal = curAccVal
        for i = 1, #self.gyroOffset do
            self:MoveLayer(i, self.gyroOffset[i], self.lastAccVal)
        end
    end
end

function LuaYuanXiao2023MainWnd:GetTrans(path)
    local trans = self.transform:Find(path)
    if trans then
        return {trans, trans.localPosition}
    end
end

function LuaYuanXiao2023MainWnd:MoveLayer(layer, offset, accVal)
    if self.gyroLayer[layer] and offset and accVal then
        for _, trans in pairs(self.gyroLayer[layer]) do
            local xMove = Mathf.Clamp(accVal.x, -0.25, 0.25)
            local yMove = Mathf.Clamp(accVal.y, -0.25, 0.25)
            local x = xMove * 4 * offset.x
            local y = yMove * 4 * offset.y
            local targetX = trans[2].x + x
            local targetY = trans[2].y + y
            local targetZ = trans[2].z
            trans[1].localPosition = Vector3.Lerp(trans[1].localPosition, Vector3(targetX, targetY, targetZ), 0.1)
        end
    end
end

function LuaYuanXiao2023MainWnd:Awake()
    self:DEFINE_GYRO_EFFECT()

    self.transform:Find("Title/TimeLabel/Time"):GetComponent(typeof(UILabel)).text = YuanXiao2023_Setting.GetData().FestivalDuration
    self.YuanXiaoDefense = self.transform:Find("YuanXiaoDefense")
    --self.YuanXiaoDefenseRedDot = self.YuanXiaoDefense:Find("RedDot").gameObject
    self.GuessRiddle = self.transform:Find("GuessRiddle")
    self.QiYuan = self.transform:Find("QiYuan")
    --self.QiYuanRedDot = self.QiYuan:Find("Btn/RedDot").gameObject
    self.TianJiangBaoXiang = self.transform:Find("TianJiangBaoXiang")
    self.DengHui = self.transform:Find("DengHui")
    --self.DengHuiRedDot = self.DengHui:Find("RedDot").gameObject

    self.anim = self.transform:GetComponent(typeof(Animation))

    UIEventListener.Get(self.YuanXiaoDefense:GetChild(0).gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:OnYuanXiaoDefenseClick()
        end
    )

    UIEventListener.Get(self.GuessRiddle:GetChild(0).gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:OnGuessRiddleClick()
        end
    )

    UIEventListener.Get(self.QiYuan:GetChild(0).gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:OnQiYuanClick()
        end
    )

    UIEventListener.Get(self.TianJiangBaoXiang:GetChild(0).gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:OnTianJiangBaoXiangClick()
        end
    )

    UIEventListener.Get(self.DengHui:GetChild(0).gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:OnDengHuiClick()
        end
    )
end

function LuaYuanXiao2023MainWnd:OnEnable()
    --local setting = YuanXiao2023_Setting.GetData()
    --LuaActivityRedDotMgr:Register(self, self.YuanXiaoDefense:Find("Btn").gameObject, self.YuanXiaoDefenseRedDot, setting.NaoYuanXiaoRedDotId)
    --LuaActivityRedDotMgr:Register(self, self.QiYuan:Find("Btn").gameObject, self.QiYuanRedDot, setting.ShangDengRedDotId)
    --LuaActivityRedDotMgr:Register(self, self.DengHui:Find("Btn").gameObject, self.DengHuiRedDot, setting.DengHuiRedDotId)
end

function LuaYuanXiao2023MainWnd:OnDisable()
    --local setting = YuanXiao2023_Setting.GetData()
    --LuaActivityRedDotMgr:UnRegister(self, setting.NaoYuanXiaoRedDotId)
    --LuaActivityRedDotMgr:UnRegister(self, setting.ShangDengRedDotId)
    --LuaActivityRedDotMgr:UnRegister(self, setting.DengHuiRedDotId)
end

function LuaYuanXiao2023MainWnd:Init()
    CScheduleMgr.Inst:RequestActivity()
    
    if self.anim then
        if not LuaYuanXiao2023Mgr.FirstEnterMainWnd then 
            LuaYuanXiao2023Mgr.FirstEnterMainWnd = true
            self.anim:Play("yuanxiao2023mainwnd_show_long")
            self.animLength = self.anim["yuanxiao2023mainwnd_show_long"].length
        else
            self.transform:Find("black").gameObject:SetActive(false)
            self.anim:Play("yuanxiao2023mainwnd_show_short")
            self.animLength = self.anim["yuanxiao2023mainwnd_show_short"].length
        end
    end

    self:RefreshAll()

    local bgMask = self.transform:Find("_BgMask_")
    if bgMask then
        bgMask:GetComponent(typeof(UITexture)).alpha = 1
    end
end

function LuaYuanXiao2023MainWnd:RefreshAll()
    local setting = YuanXiao2023_Setting.GetData()
    self:SetState(self.YuanXiaoDefense, setting.NaoYuanXiaoScheduleId, setting.NaoYuanXiaoStartTime)
    self:SetState(self.QiYuan, setting.ShangDengScheduleId, setting.ShangDengStartTime)
    self:SetState(self.GuessRiddle, setting.RiddleScheduleId)
    self:SetState(self.TianJiangBaoXiang, setting.BaoXiangScheduleId, setting.BaoXiangStartTime)
    self:SetState(self.DengHui, setting.DengHuiScheduleId, setting.DengHuiStartTime)
end

function LuaYuanXiao2023MainWnd:SetState(trans, scheduleId, time)
    local timeLabel = trans:Find("TimeLabel"):GetComponent(typeof(UILabel))
    local btn = trans:GetChild(0):GetComponent(typeof(UITexture))
    local open = CLuaScheduleMgr:IsFestivalTaskOpen(Task_Schedule.GetData(scheduleId).TaskID[0])
    if timeLabel then
        timeLabel.gameObject:SetActive(true) 
        if open and LuaActivityRedDotMgr.m_ScheduleDict[scheduleId] then
            if not time then
                timeLabel.gameObject:SetActive(false) 
            else
                local segments = {}
                for t in string.gmatch(time, "[^;]+") do
                    local sted = {}
                    for tt in string.gmatch(t, "[^-]+") do
                        table.insert(sted, tt)
                    end
                    table.insert(segments, {
                        time = t,
                        st = sted[1],
                        ed = sted[2],
                    })
                end
                
                if #segments > 1 then
                    local now = CServerTimeMgr.Inst:GetZone8Time()
                    local t1 = CServerTimeMgr.Inst:GetTimeStampByStr(now.Year.."-"..now.Month.."-"..now.Day.." "..segments[1].ed.."00")
                    --local t2 = CServerTimeMgr.Inst:GetTimeStampByStr(now.Year.."-"..now.Month.."-"..now.Day.." "..segments[2].ed.."00")
                    now = CServerTimeMgr.Inst.timeStamp
                    if now <= t1 then
                        timeLabel.text = segments[1].time
                    else
                        timeLabel.text = segments[2].time
                    end
                else
                    timeLabel.text = segments[1].time
                end
                timeLabel.alpha = 0
                if self.animLength then
                    RegisterTickOnce(function()
                        if not CommonDefs.IsNull(timeLabel) then
                            LuaTweenUtils.TweenAlpha(timeLabel, 0, 1, 0.6)
                        end
                    end, (self.animLength / 3 * 2) * 1000)
                else
                    timeLabel.alpha = 1
                end

                btn.alpha = 1
            end
        else
            timeLabel.text = LocalString.GetString("未开启")
            timeLabel.alpha = 0
            if self.animLength then
                RegisterTickOnce(function()
                    if not CommonDefs.IsNull(timeLabel) then
                        LuaTweenUtils.TweenAlpha(timeLabel, 0, 0.6, 0.6)
                    end
                end, (self.animLength / 3 * 2) * 1000)
            else
                timeLabel.alpha = 0.6
            end

            btn.alpha = 0.6
        end
    end

    --trans:Find("Btn"):GetComponent(typeof(BoxCollider)).enabled = open
    --trans:Find("Btn/Label").gameObject:SetActive(open)
    --trans:Find("Btn/HideLabel").gameObject:SetActive(not open)
    --trans:Find("Icon").gameObject:SetActive(open)
    --trans:Find("HideIcon").gameObject:SetActive(not open)
    --trans:Find("Btn"):GetComponent(typeof(UITexture)).alpha = open and 1 or 0.6
end

function LuaYuanXiao2023MainWnd:OnClick(doFunc, scheduleId)
    local scheduleData = Task_Schedule.GetData(scheduleId)
    local taskData = Task_Task.GetData(scheduleData.TaskID[0])
    if CLuaScheduleMgr:IsFestivalTaskOpen(taskData.ID) then    
        if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Level >= taskData.Level then
            doFunc(scheduleId)
        else
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", SafeStringFormat3(LocalString.GetString("参加%s玩法需%s级"), scheduleData.TaskName, tostring(taskData.Level)))
        end
    else
        g_MessageMgr:ShowMessage("YuanXiao2023_Play_Not_Open")
    end
end

function LuaYuanXiao2023MainWnd:OnYuanXiaoDefenseClick()
    local setting = YuanXiao2023_Setting.GetData()
    self:OnClick(function() 
        CUIManager.ShowUI(CLuaUIResources.YuanXiaoDefenseSignUpWnd) 
    end, setting.NaoYuanXiaoScheduleId)
end

function LuaYuanXiao2023MainWnd:OnGuessRiddleClick()
    local setting = YuanXiao2023_Setting.GetData()
    self:OnClick(function(scheduleId) 
        local info = CLuaScheduleMgr:GetScheduleInfo(scheduleId)
        if info then
            local schedule = Task_Schedule.GetData(scheduleId)
            CLuaScheduleMgr.selectedScheduleInfo={
                activityId = scheduleId,
                taskId = schedule.TaskID[0],
    
                TotalTimes = 0,--不显示次数
                DailyTimes = 0,--不显示活力
                FinishedTimes = 0,
                
                ActivityTime = schedule.ExternTip,
                ShowJoinBtn = false
            }
            CLuaScheduleInfoWnd.s_UseCustomInfo = true
            CUIManager.ShowUI(CLuaUIResources.ScheduleInfoWnd)
        else
            g_MessageMgr:ShowMessage("YuanXiao2023_Play_Not_Open")
        end
    end, setting.RiddleScheduleId)
end

function LuaYuanXiao2023MainWnd:OnQiYuanClick()
    local setting = YuanXiao2023_Setting.GetData()
    self:OnClick(function(scheduleId) 
        local info = CLuaScheduleMgr:GetScheduleInfo(scheduleId)
        if info then
            CLuaScheduleMgr.selectedScheduleInfo = info
            CUIManager.ShowUI(CLuaUIResources.ScheduleInfoWnd)
        else
            g_MessageMgr:ShowMessage("YuanXiao2023_Play_Not_Open")
        end
    end, setting.ShangDengScheduleId)
end

function LuaYuanXiao2023MainWnd:OnTianJiangBaoXiangClick()
    local setting = YuanXiao2023_Setting.GetData()
    self:OnClick(function(scheduleId) 
        local schedule = Task_Schedule.GetData(scheduleId)
        local segments = {}
        for t in string.gmatch(setting.BaoXiangStartTime, "[^;]+") do
            table.insert(segments, t)
        end
        CLuaScheduleMgr.selectedScheduleInfo={
            activityId = scheduleId,
            taskId = schedule.TaskID[0],

            TotalTimes = 0,--不显示次数
            DailyTimes = 0,--不显示活力
            FinishedTimes = 0,
            
            ActivityTime = SafeStringFormat3( "%s   %s, %s", schedule.ExternTip, segments[1], segments[2]),
            ShowJoinBtn = false
        }
        CLuaScheduleInfoWnd.s_UseCustomInfo = true
        CUIManager.ShowUI(CLuaUIResources.ScheduleInfoWnd)
    end, setting.BaoXiangScheduleId)
end

function LuaYuanXiao2023MainWnd:OnDengHuiClick()
    local setting = YuanXiao2023_Setting.GetData()
    self:OnClick(function(scheduleId) 
        local info = CLuaScheduleMgr:GetScheduleInfo(scheduleId)
        if info then
            CLuaScheduleMgr.selectedScheduleInfo = info
            CUIManager.ShowUI(CLuaUIResources.ScheduleInfoWnd)
        else
            g_MessageMgr:ShowMessage("YuanXiao2023_Play_Not_Open")
        end
    end, setting.DengHuiScheduleId)
end
