local GameObject		    = import "UnityEngine.GameObject"
local Vector3               = import "UnityEngine.Vector3"

local UILabel               = import "UILabel"
local UITexture             = import "UITexture"
local UIProgressBar         = import "UIProgressBar"
local TweenRotation         = import "TweenRotation"

local CommonDefs            = import "L10.Game.CommonDefs"
local CItemMgr				= import "L10.Game.CItemMgr"
local EnumItemPlace         = import "L10.Game.EnumItemPlace"
local CEffectMgr            = import "L10.Game.CEffectMgr"
local EnumWarnFXType        = import "L10.Game.EnumWarnFXType"

local DelegateFactory		= import "DelegateFactory"
local LocalString           = import "LocalString"
local Extensions			= import "Extensions"

local CUIManager			        = import "L10.UI.CUIManager"
local CUIFx			                = import "L10.UI.CUIFx"
local CItemInfoMgr			        = import "L10.UI.CItemInfoMgr"
local AlignType				        = import "L10.UI.CItemInfoMgr+AlignType"
local CCommonActionWndMgr           = import "L10.UI.CCommonActionWndMgr"
local LuaDefaultModelTextureLoader  = import "L10.UI.LuaDefaultModelTextureLoader"
local CClientNpc                    = import "L10.Game.CClientNpc"
local NPC_NPC                       = import "L10.Game.NPC_NPC"

CLuaWorldQiyishuWnd = class()

RegistChildComponent(CLuaWorldQiyishuWnd,   "Item1",		    GameObject)
RegistChildComponent(CLuaWorldQiyishuWnd,   "Item2",		    GameObject)
RegistChildComponent(CLuaWorldQiyishuWnd,   "Item3",		    GameObject)
RegistChildComponent(CLuaWorldQiyishuWnd,   "Item4",		    GameObject)
RegistChildComponent(CLuaWorldQiyishuWnd,   "Item5",		    GameObject)
RegistChildComponent(CLuaWorldQiyishuWnd,   "Processing",	    GameObject)
RegistChildComponent(CLuaWorldQiyishuWnd,   "ProcessText",      UILabel)
RegistChildComponent(CLuaWorldQiyishuWnd,   "Completed",	    GameObject)
RegistChildComponent(CLuaWorldQiyishuWnd,   "ProgressValueSp",  UIProgressBar)
RegistChildComponent(CLuaWorldQiyishuWnd,   "ModelTexture",     UITexture)
RegistChildComponent(CLuaWorldQiyishuWnd,   "ModelFx",          CUIFx)


RegistClassMember(CLuaWorldQiyishuWnd,   "m_items")
RegistClassMember(CLuaWorldQiyishuWnd,   "m_stage")
RegistClassMember(CLuaWorldQiyishuWnd,   "m_modelname")
RegistClassMember(CLuaWorldQiyishuWnd,   "m_ModelTextureLoader")

function CLuaWorldQiyishuWnd:Init( )
    self.m_stage = 0
    self.m_items = {}
    self.m_modelname = "__QiyiShuPreview__"
    self.m_items[self.Item1] = {state = 0,stage = 0,eftid = 88801129}
    self.m_items[self.Item2] = {state = 0,stage = 1,eftid = 88801130}
    self.m_items[self.Item3] = {state = 0,stage = 2,eftid = 88801131}
    self.m_items[self.Item4] = {state = 0,stage = 3,eftid = 88801132}
    self.m_items[self.Item5] = {state = 0,stage = 4,eftid = 88801132}
    
    local setting = ShiJieShiJian_Setting.GetData()
    if setting == nil then return end

    local values = setting.TotalValue2CustomFunction
    self.m_items[self.Item1].pvalue = tonumber(values[0])
    self.m_items[self.Item2].pvalue = tonumber(values[2]) -- pvalue
    self.m_items[self.Item3].pvalue = tonumber(values[4])
    self.m_items[self.Item4].pvalue = tonumber(values[6])
    self.m_items[self.Item5].pvalue = tonumber(values[8])

    local infos = setting.QiYiHuaInfo
    self.m_items[self.Item1].NPCID = infos[1]
    self.m_items[self.Item2].NPCID = infos[4]
    self.m_items[self.Item3].NPCID = infos[7]
    self.m_items[self.Item4].NPCID = infos[10]
    self.m_items[self.Item5].NPCID = infos[13]

    self.m_items[self.Item1].ItemTID = infos[2]
    self.m_items[self.Item2].ItemTID = infos[5]
    self.m_items[self.Item3].ItemTID = infos[8]
    self.m_items[self.Item4].ItemTID = infos[11]
    self.m_items[self.Item5].ItemTID = infos[14]

    self:InitItem(self.Item2)
    self:InitItem(self.Item3)
    self:InitItem(self.Item4)
    self:InitItem(self.Item5)
    
    self.m_ModelTextureLoader = LuaDefaultModelTextureLoader.Create(function (ro)
        self:LoadModel(ro)
    end)

    self.ModelFx:LoadFx("fx/ui/prefab/UI_qiyihua_guangzhao.prefab")

    self:SetValue(0)
    
    Gac2Gas.QueryLieFengRepairProgressInfo()
end

function CLuaWorldQiyishuWnd:SetModelTexture()
    self.ModelTexture.mainTexture = CUIManager.CreateModelTexture(self.m_modelname, self.m_ModelTextureLoader,180,0,-1.17,4.66,false,true,1.7,true)
end

function CLuaWorldQiyishuWnd:LoadModel(ro)
    local data = self:GetDataByStage(self.m_stage)
    if data == nil then return end

    local npcdata = NPC_NPC.GetData(data.NPCID)
    if npcdata == nil then return end
    
    local prefabname = CClientNpc.GetNPCPrefabPath(npcdata)
    if System.String.IsNullOrEmpty(prefabname) then return end

    ro:LoadMain(prefabname)
    Extensions.SetLocalRotationX(ro.transform,30)
    CEffectMgr.Inst:AddObjectFX(data.eftid,ro,0,1.0,1.0,nil,false,EnumWarnFXType.None,Vector3.zero, Vector3.zero, nil)
end

function CLuaWorldQiyishuWnd:OnEnable()
    
    g_ScriptEvent:AddListener("OnQiyishuValueChange",self,"OnQiyishuValueChange")
    local itemClick = function(go)
        if go == self.Item1 then 
            self:OnKongMingDengClick()
        else
            local stage = self.m_items[go].stage
            self:OnItemClick(go,stage)
        end 
    end 
    CommonDefs.AddOnClickListener(self.Item1, DelegateFactory.Action_GameObject(itemClick), false)
    CommonDefs.AddOnClickListener(self.Item2, DelegateFactory.Action_GameObject(itemClick), false)
    CommonDefs.AddOnClickListener(self.Item3, DelegateFactory.Action_GameObject(itemClick), false)
    CommonDefs.AddOnClickListener(self.Item4, DelegateFactory.Action_GameObject(itemClick), false)
    CommonDefs.AddOnClickListener(self.Item5, DelegateFactory.Action_GameObject(itemClick), false)
end

function CLuaWorldQiyishuWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnQiyishuValueChange",self,"OnQiyishuValueChange")
    CUIManager.DestroyModelTexture(self.m_modelname)
end

function CLuaWorldQiyishuWnd:GetDataByStage(stage)
    if stage == 0 then
        return self.m_items[self.Item1]
    elseif stage == 1 then 
        return self.m_items[self.Item2]
    elseif stage == 2 then 
        return self.m_items[self.Item3]
    elseif stage == 3 then 
        return self.m_items[self.Item4]
    elseif stage == 4 then 
        return self.m_items[self.Item5]
    else
        return nil
    end
end

--[[
    @desc: 事件监听
    author:CodeGize
    time:2019-04-22 09:53:23
    --@value:   进度值
	--@dataud:  dic 阶段->是否领取礼包
    @return:
]]
function CLuaWorldQiyishuWnd:OnQiyishuValueChange(value,dataud)
    self:SetValue(value)

    local dic = MsgPackImpl.unpack(dataud)

    local items = {self.Item2,self.Item3,self.Item4,self.Item5}
    for i = 1, 4 do
        local state = 0
        local item = items[i]
        local index = i+1--服务器是1-5，客户端是0-4所以要做修正
        if CommonDefs.ListContains(dic,typeof(Int32),index) then
            state = 2
        elseif value >= self.m_items[item].pvalue then
            state = 1
        end
        self:SetItemState(item,state)
    end
end

function CLuaWorldQiyishuWnd:InitItem(itemgo)
    if itemgo == nil then return end
    local trans = itemgo.transform

    local data = self.m_items[itemgo]

    local label = trans:Find("Value"):GetComponent(typeof(UILabel))
    --local icon = trans:Find("Icon"):GetComponent(typeof(CUITexture))

    label.text = tostring(data.pvalue)

    local itemdata = Item_Item.GetData(data.ItemTID)
    --icon:LoadMaterial(itemdata.Icon)
end

--[[
    @desc: 设置礼包状态
    author:CodeGize
    time:2019-04-22 10:07:25
    --@itemctrl:   控件Go
    --@state:   0：不能领取，1：可领取，2已领取
    @return:
]]
function CLuaWorldQiyishuWnd:SetItemState(itemgo,state)

    if itemgo == nil then return end
    local trans = itemgo.transform

    local data = self.m_items[itemgo]

    local label = trans:Find("Value"):GetComponent(typeof(UILabel))
    local icontrans = trans:Find("Icon")
    local tween = icontrans:GetComponent(typeof(TweenRotation))
    

    data.state = state
    if state == 2 then
        label.text = LocalString.GetString("已领取")
        Extensions.SetLocalPositionZ(icontrans,0)
        Extensions.SetLocalRotationZ(icontrans,0)
        tween.enabled = false
    else
        label.text = tostring(data.pvalue)
        if state == 1 then 
            --可领取
            Extensions.SetLocalPositionZ(icontrans,0)
            tween.enabled = true
        else 
            --不可领取
            Extensions.SetLocalPositionZ(icontrans,-1)
            Extensions.SetLocalRotationZ(icontrans,0)
            tween.enabled = false
        end
    end
end
--[[
    @desc: 奖励道具图标点击事件
    author:CodeGize
    time:2019-04-19 17:39:30
    --@go:      按钮GO
	--@stage:   阶段，从1开始
    @return:
]]
function CLuaWorldQiyishuWnd:OnItemClick(go)

    local data = self.m_items[go]
    local state = data.state
    if state == 1 then --可领取
        local stage = data.stage + 1
        Gac2Gas.RequestReceiveQiYiHuaAward(stage)
    else --不能领取或者已经领取
        CItemInfoMgr.ShowLinkItemTemplateInfo(data.ItemTID, false, nil, AlignType.Default, 0, 0, 0, 0) 
    end
end

--[[
    @desc: 孔明灯图标点击事件
    author:CodeGize
    time:2019-04-19 17:35:47
    @return:
]]
function CLuaWorldQiyishuWnd:OnKongMingDengClick( )
    local kmdtid = ShiJieShiJian_Setting.GetData().XwzdID

    local tcfg = Item_Item.GetData(kmdtid)
    if tcfg ~= nil then
        local icon = tcfg.Icon
        local btnName = LocalString.GetString("使用")

        local pos
        local itemId
        local isfound
        isfound, pos, itemId = CItemMgr.Inst:FindItemByTemplateIdPriorToBinded(EnumItemPlace.Bag, tcfg.ID)
        if isfound then
            local usefunc = function()
                Gac2Gas.RequestUseItem(EnumItemPlace_lua.Bag, pos, itemId, "")
                CUIManager.CloseUI(CLuaUIResources.WorldQiyishuWnd)
            end
            local item = CItemMgr.Inst:GetById(itemId)
            CCommonActionWndMgr.Inst:ShowWithName(item.Icon,item.ColoredName,btnName,DelegateFactory.Action(usefunc),nil)
        else
            g_MessageMgr:ShowMessage("No_Xi_Wang_Deng")
        end
    end
end

--[[
    @desc: 设置进度
    author:CodeGize
    time:2019-04-19 17:40:59
    --@value:       当前值
    @return:
]]
function CLuaWorldQiyishuWnd:SetValue(value)

    if value >= self.m_items[self.Item5].pvalue then 
        self.ProgressValueSp.value = 1
        self.Processing:SetActive(false)
        self.Completed:SetActive(true)
        self.m_stage = 4
    else
        self.Processing:SetActive(true)
        self.Completed:SetActive(false)
        self.ProcessText.text = tostring(value)

        local p2 = self.m_items[self.Item2].pvalue
        local p3 = self.m_items[self.Item3].pvalue
        local p4 = self.m_items[self.Item4].pvalue
        local p5 = self.m_items[self.Item5].pvalue
        
        if value < self.m_items[self.Item2].pvalue then
            self.ProgressValueSp.value = 0.25 * value / p2
            self.m_stage = 0
        elseif value < self.m_items[self.Item3].pvalue then
            self.ProgressValueSp.value = 0.25 + 0.25 * (value - p2) / (p3 - p2)
            self.m_stage = 1
        elseif value < self.m_items[self.Item4].pvalue then
            self.ProgressValueSp.value = 0.5 + 0.25 * (value - p3) / (p4 - p3)
            self.m_stage = 2
        else
            self.ProgressValueSp.value = 0.75 + 0.25 * (value - p4) / (p5 - p4)
            self.m_stage = 3
        end
    end
    self:SetModelTexture()
end
