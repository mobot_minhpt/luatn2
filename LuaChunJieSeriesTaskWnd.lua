local CServerTimeMgr=import "L10.Game.CServerTimeMgr"
local DateTime = import "System.DateTime"
CLuaChunJieSeriesTaskWnd = class()
RegistClassMember(CLuaChunJieSeriesTaskWnd,"m_FxTick")

function CLuaChunJieSeriesTaskWnd:Init()

    --
    Gac2Gas.QueryChunJie2020SeriesTasksRewardStatus() --查询奖励是否已领

    local item = self.transform:Find("Item").gameObject
    item:SetActive(false)
    local grid = self.transform:Find("Grid").gameObject

    local taskProp = CClientMainPlayer.Inst and CClientMainPlayer.Inst.TaskProp or {}
    local setting = ChunJie2020_Setting.GetData()
    local tasks = setting.SeriesTasks
    local checkTasks = setting.SeriesFinalTasks
    local taskNames = setting.SeriesTaskNames


    local now = CServerTimeMgr.Inst:GetZone8Time()
    local time = DateTime(now.Year,now.Month,now.Day)--今天

    local texture_lock = "UI/Texture/Transparent/Material/chunjieseriestaskwnd_weijiesuo_2020.mat"
    local texture_ing = "UI/Texture/Transparent/Material/chunjieseriestaskwnd_jinxingzhong_2020.mat"
    local texture_active = "UI/Texture/Transparent/Material/chunjieseriestaskwnd_jinrijihuo_2020.mat"
    local texture_finish = "UI/Texture/Transparent/Material/chunjieseriestaskwnd_yiwancheng_2020.mat"
    local texture_no = "UI/Texture/Transparent/Material/chunjieseriestaskwnd_weilingqu_2020.mat"

    for i=1,tasks.Length do
        local taskId = tasks[i-1]
        local finished = taskProp:IsMainPlayerTaskFinished(checkTasks[i-1])
        local go = NGUITools.AddChild(grid,item)
        go:SetActive(true)
        local texture = go.transform:Find("Texture"):GetComponent(typeof(CUITexture))
        local fx = go.transform:Find("Fx"):GetComponent(typeof(CUIFx))
        fx.gameObject:SetActive(false)

        local nameLabel = go.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
        nameLabel.gameObject:SetActive(false)
        nameLabel.text = taskNames[i-1]

        local dayLabel = go.transform:Find("DayLabel"):GetComponent(typeof(UILabel))
        dayLabel.gameObject:SetActive(false)

        local noGo = go.transform:Find("NotLabel").gameObject
        noGo:SetActive(false)

        --local openTime = nil
        --20年是1.24-1.31，21年是2.11-2.18
--        if 24+i-1>31 then
--            openTime = DateTime(2021,2,24+i-1-31)
--        else
--            openTime = DateTime(2021,1,24+i-1)
--        end

        local openTime = DateTime(2021,2,11+i-1)

        if DateTime.Compare(time,openTime)>=0 then--
            --time>openTime 可以做的任务
            --任务开放，接或者没接
            if not finished then
                local got = CommonDefs.DictContains_LuaCall(taskProp.CurrentTasks,taskId) or taskProp:IsMainPlayerTaskFinished(taskId)
                if got then
                    texture:LoadMaterial(texture_ing)--进行中
                else
                    if DateTime.Compare(time,openTime)==0 then
                        texture:LoadMaterial(texture_active)--今日开放
                        fx.gameObject:SetActive(true)--显示特效
                    else
                        texture:LoadMaterial(texture_no)--未领取
                        noGo:SetActive(true)
                    end
                end
            else
                texture:LoadMaterial(texture_finish)
                nameLabel.gameObject:SetActive(true)

            end
            UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function(g)
                CLuaChunJieSeriesTaskDescWnd.m_TaskId = taskId
                CUIManager.ShowUI(CLuaUIResources.ChunJieSeriesTaskDescWnd)
            end)
        else--if DateTime.Compare(time,openTime)<0 then--time<openTime 时间没到
            texture:LoadMaterial(texture_lock)
            local sub = openTime:Subtract(time).Days
            dayLabel.gameObject:SetActive(true)
            dayLabel.text = SafeStringFormat3(LocalString.GetString("%d天后"),sub)

            UIEventListener.Get(go).onClick = nil
        end

    end

    -- local rewardGo = self.transform:Find("Reward").gameObject
    -- rewardGo:SetActive(false)

end


function CLuaChunJieSeriesTaskWnd:OnEnable()
g_ScriptEvent:AddListener("GetChunJie2020SeriesTasksRewardSuccess", self, "OnGetChunJie2020SeriesTasksRewardSuccess")
g_ScriptEvent:AddListener("SendChunJie2020SeriesTasksRewardStatus", self, "OnSendChunJie2020SeriesTasksRewardStatus")

end

function CLuaChunJieSeriesTaskWnd:OnDisable()
g_ScriptEvent:RemoveListener("GetChunJie2020SeriesTasksRewardSuccess", self, "OnGetChunJie2020SeriesTasksRewardSuccess")
g_ScriptEvent:RemoveListener("SendChunJie2020SeriesTasksRewardStatus", self, "OnSendChunJie2020SeriesTasksRewardStatus")
end

function CLuaChunJieSeriesTaskWnd:OnGetChunJie2020SeriesTasksRewardSuccess()--领取成功
    --播放动画

end

function CLuaChunJieSeriesTaskWnd:OnSendChunJie2020SeriesTasksRewardStatus( canGetReward, rewardGot )--奖励是否可领，是否已领
    --打开界面
    if canGetReward and not rewardGot then
        --所有的item合并
        local grid = self.transform:Find("Grid").transform
        for i=1,grid.childCount do
            local tf = grid:GetChild(i-1)
            LuaTweenUtils.TweenPositionX(tf,0,0.5)
        end

        self.m_FxTick = RegisterTickOnce(function()
            local rewardFx = self.transform:Find("RewardFx").gameObject
            rewardFx:SetActive(true)

            self.m_FxTick = RegisterTickOnce(function()
                CUIManager.CloseUI(CLuaUIResources.ChunJieSeriesTaskWnd)
                CUIManager.ShowUI(CLuaUIResources.ChunJieSeriesTaskRewardWnd)
                self.m_FxTick = nil
            end,1500)
        end,500)
    end
end
function CLuaChunJieSeriesTaskWnd:OnDestroy()
    if self.m_FxTick then
        UnRegisterTick(self.m_FxTick)
        self.m_FxTick = nil
    end
end
