local CSwitchMgr = import "L10.Engine.CSwitchMgr"
local String = import "System.String"
local QnCheckBoxGroup = import "L10.UI.QnCheckBoxGroup"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local PlayerSettings = import "L10.Game.PlayerSettings"
local EChatPanel = import "L10.Game.EChatPanel"
local CChatHelper = import "L10.UI.CChatHelper"

LuaSettingWnd_VoiceSetting = class()

RegistClassMember(LuaSettingWnd_VoiceSetting, "m_DetailSettings")
RegistClassMember(LuaSettingWnd_VoiceSetting, "m_DetailSettingsGroup")
RegistClassMember(LuaSettingWnd_VoiceSetting, "m_WifiButton")

function LuaSettingWnd_VoiceSetting:Awake()
    --按需设置，不常见的频道可以不放在这里，直接在频道顶部也有此设置的
    local needDisplayChannels = {EChatPanel.Guild, EChatPanel.World, EChatPanel.Team, 
        EChatPanel.Current, EChatPanel.Ally, EChatPanel.TeamGroup, EChatPanel.LocalWorld}
    
    self.m_DetailSettings = {}
    for __, channel in pairs(needDisplayChannels) do
        table.insert(self.m_DetailSettings, {title = CChatHelper.GetChannelName(channel), propertyName = CChatHelper.GetChannelName(channel) })
    end

    if CSwitchMgr.EnableZhanDui then
        table.insert(self.m_DetailSettings, {title = CChatHelper.GetChannelName(EChatPanel.Zhandui)   ,propertyName = CChatHelper.GetChannelName(EChatPanel.Zhandui)})
    end

    self:InitComponents()
    self:Init()
    self:LoadData()
end

function LuaSettingWnd_VoiceSetting:Init()
    local detailtitles_buf = {}
    for i,v in ipairs(self.m_DetailSettings) do
        table.insert(detailtitles_buf, v.title)
    end
    local detailtitles = Table2Array(detailtitles_buf, MakeArrayClass(String))
    self.m_DetailSettingsGroup:InitWithOptions(detailtitles, true, false)
    self.m_DetailSettingsGroup.OnSelect = DelegateFactory.Action_QnCheckBox_int(function(checkbox,level)
        self:OnSelect(level)
    end)
    self.m_WifiButton.OnClick = DelegateFactory.Action_QnButton(function(btn)
        self:On_WifiButtonButtonClicked(btn)
    end)
end

function LuaSettingWnd_VoiceSetting:InitComponents()
    self.m_DetailSettingsGroup = self.transform:Find("VoiceSettings/VoiceDetailSettings/QnCheckBoxGroup"):GetComponent(typeof(QnCheckBoxGroup))
    self.m_WifiButton = self.transform:Find("VoiceSettings/Wifi"):GetComponent(typeof(QnSelectableButton))
end

function LuaSettingWnd_VoiceSetting:OnSelect(level)
    local option = self.m_DetailSettings[level + 1]
    local enable = self.m_DetailSettingsGroup[level].Selected
    PlayerSettings.SetAutoVoiceInChannel(CChatHelper.GetChannelByName(option.propertyName), enable)
end

function LuaSettingWnd_VoiceSetting:On_WifiButtonButtonClicked(btn)
    local isSeleted = self.m_WifiButton:isSeleted()
    PlayerSettings.AutoVoiceInNoneWifiNetworkDisabled = isSeleted
end

function LuaSettingWnd_VoiceSetting:OnEnable()
    self:LoadData()
    g_ScriptEvent:AddListener("ReloadSettings", self, "LoadData")
end

function LuaSettingWnd_VoiceSetting:OnDisable()
    g_ScriptEvent:RemoveListener("ReloadSettings", self, "LoadData")
end

function LuaSettingWnd_VoiceSetting:LoadData()
    self.m_WifiButton:SetSelected(PlayerSettings.AutoVoiceInNoneWifiNetworkDisabled, false)
    for i = 1,#self.m_DetailSettings do
        local option = self.m_DetailSettings[i]
        self.m_DetailSettingsGroup[i - 1].Selected = PlayerSettings.GetAutoVoiceInChannel(CChatHelper.GetChannelByName(option.propertyName))
    end
end
