local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local UIScrollView = import "UIScrollView"
local UITable = import "UITable"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local CTipParagraphItem = import "L10.UI.CTipParagraphItem"

LuaQiXi2022EnterWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaQiXi2022EnterWnd, "TimeLabel", "TimeLabel", UILabel)
RegistChildComponent(LuaQiXi2022EnterWnd, "ParagraphTemplate", "ParagraphTemplate", GameObject)
RegistChildComponent(LuaQiXi2022EnterWnd, "ScrollView", "ScrollView", UIScrollView)
RegistChildComponent(LuaQiXi2022EnterWnd, "Table", "Table", UITable)
RegistChildComponent(LuaQiXi2022EnterWnd, "NewPlayButton", "NewPlayButton", GameObject)
RegistChildComponent(LuaQiXi2022EnterWnd, "FlowChartButton", "FlowChartButton", GameObject)
RegistChildComponent(LuaQiXi2022EnterWnd, "GetReward", "GetReward", GameObject)
RegistChildComponent(LuaQiXi2022EnterWnd, "NumberLabel", "NumberLabel", UILabel)

--@endregion RegistChildComponent end

function LuaQiXi2022EnterWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.NewPlayButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnNewPlayButtonClick()
	end)

	UIEventListener.Get(self.FlowChartButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnFlowChartButtonClick()
	end)

    --@endregion EventBind end
end

function LuaQiXi2022EnterWnd:Init()
    self.GetReward.gameObject:SetActive(false)
    if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eQiXi2022SFEQ_FinalRewardTime) > 0 then
        self.GetReward.gameObject:SetActive(true)
	end
    local maxtimes = 3
    local times = 0
    if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eQiXi2022SFEQ_DailyEnterTimes) > 0 then
        times = CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eQiXi2022SFEQ_DailyEnterTimes)
	end
    self.NumberLabel.text = SafeStringFormat3(times == maxtimes and LocalString.GetString("[FF4B36]%s[ffffff]/%s") or LocalString.GetString("[56FF36]%s[ffffff]/%s"),times,maxtimes)
    self.TimeLabel.text = g_MessageMgr:FormatMessage("QiXi2022EnterWnd_Time")
    self.ParagraphTemplate.gameObject:SetActive(false)
    Extensions.RemoveAllChildren(self.Table.transform)
	local msg = g_MessageMgr:FormatMessage("QiXi2022EnterWnd_Introduction")
	if System.String.IsNullOrEmpty(msg) then
        return
    end
    local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(msg)
    if info == nil then
        return
    end
    do
        local i = 0
        while i < info.paragraphs.Count do
            local paragraphGo = CUICommonDef.AddChild(self.Table.gameObject, self.ParagraphTemplate.gameObject)
            paragraphGo:SetActive(true)
            CommonDefs.GetComponent_GameObject_Type(paragraphGo, typeof(CTipParagraphItem)):Init(info.paragraphs[i], 4294967295)
            i = i + 1
        end
    end
    self.ScrollView:ResetPosition()
    self.Table:Reposition()
end

--@region UIEvent

function LuaQiXi2022EnterWnd:OnNewPlayButtonClick()
    Gac2Gas.SFEQ_StartPlay()
end

function LuaQiXi2022EnterWnd:OnFlowChartButtonClick()
    LuaQiXi2022Mgr.m_PlotWndShowFlowChart = true
    CUIManager.ShowUI(CLuaUIResources.QiXi2022PlotWnd)
end


--@endregion UIEvent

