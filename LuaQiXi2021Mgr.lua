local CScene = import "L10.Game.CScene"
local NGUITools = import "NGUITools"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local CLoginMgr = import "L10.Game.CLoginMgr"
local CGuideMgr=import "L10.Game.Guide.CGuideMgr"
local Guide_Logic = import "L10.Game.Guide_Logic"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local EnumGender = import "L10.Game.EnumGender"

LuaQiXi2021Mgr = {}

LuaQiXi2021Mgr.init = false
LuaQiXi2021Mgr.QueQiaoXianQuStageTable = {} -- 鹊桥仙曲阶段标题
-- 报名界面数据
LuaQiXi2021Mgr.todayRemainRewardTimes = 0 -- 今日剩余挑战次数
LuaQiXi2021Mgr.isSignUp = false
-- 答题数据
LuaQiXi2021Mgr.questionAnswers = {}     -- 题目答案
LuaQiXi2021Mgr.questionContent = ""     -- 题目内容
-- 玩法过程中数据
LuaQiXi2021Mgr.stage = -1               -- 玩法阶段
LuaQiXi2021Mgr.questionCount = 0        -- 回答问题总个数
LuaQiXi2021Mgr.questionFinishCount = 0  -- 已回答问题个数
LuaQiXi2021Mgr.queQiaoFished = false    -- 是否搭建鹊桥
LuaQiXi2021Mgr.monsterInfo = 0          -- 怪物信息
LuaQiXi2021Mgr.daTiTimeLeft = 0         -- 剩余答题时间
LuaQiXi2021Mgr.answerContent = ""       -- 玩家已回答的答案内容
-- 历史队友信息
LuaQiXi2021Mgr.partnerInfo = {}         -- 鹊桥仙曲队友数据
LuaQiXi2021Mgr.MyInfo = {}              -- 玩家信息
LuaQiXi2021Mgr.awardTimes = 0           -- 鹊桥仙曲奖励次数
LuaQiXi2021Mgr.lunarCalendar = nil      -- 日期转农历Dict
-- 结算数据
LuaQiXi2021Mgr.playResult = false
LuaQiXi2021Mgr.playResultParnter = nil
-- 当前是否已经进入鹊桥仙曲玩法
function LuaQiXi2021Mgr.IsQueQiaoXianQuPlay()
    if CScene.MainScene then
        local gamePlayId = CScene.MainScene.GamePlayDesignId
        if gamePlayId == QiXi_QiXi2021.GetData().GameplayId then
           return true
        end
    end
    return false
end
function LuaQiXi2021Mgr.InitData()
    if not LuaQiXi2021Mgr.init then
        LuaQiXi2021Mgr.QueQiaoXianQuStageTable = {}
        LuaQiXi2021Mgr.GetlunarCalendarDict()
        for i=1,3 do
            local titleString = g_MessageMgr:FormatMessage("2021QiXi_QueQiaoXianQu_Title"..i)
            LuaQiXi2021Mgr.QueQiaoXianQuStageTable[i] = titleString
        end
        LuaQiXi2021Mgr.init = true
    end
end

function LuaQiXi2021Mgr.SyncQueQiaoXianQuSignUpInfo(isSignUp, forceOpen, todayRemainRewardTimes)
    LuaQiXi2021Mgr.todayRemainRewardTimes = todayRemainRewardTimes
    LuaQiXi2021Mgr.isSignUp = isSignUp
    g_ScriptEvent:BroadcastInLua("SyncQueQiaoXianQuSignUpInfo")
end

function LuaQiXi2021Mgr.SyncQueQiaoXianQuPartnerInfo(result,partnerAwardTimes)
    LuaYiRenSiMgr:PrintTable(result)
    LuaQiXi2021Mgr.partnerInfo = result
    LuaQiXi2021Mgr.awardTimes = partnerAwardTimes
    
    --CUIManager.ShowUI(CLuaUIResources.QueQiaoXianQuHistoryTeammateWnd)
    CUIManager.ShowUI(CLuaUIResources.QiXi2023QueQiaoXianQuHistoryTeammateWnd)
end

function LuaQiXi2021Mgr.SyncQueQiaoXianQuPlayResultInfo(result, partnerPlayerId, partnerName, partnerClass, partnerGrade, partnerGender)
    LuaQiXi2021Mgr.playResult = result
    LuaQiXi2021Mgr.playResultParnter = {playerId = partnerPlayerId,name = partnerName, class = partnerClass, gender = partnerGender, grade = partnerGrade}
    LuaQiXi2021Mgr.GetPlayerInfo()
    CUIManager.ShowUI(CLuaUIResources.QiXi2023QueQiaoXianQuResultWnd)
end

function LuaQiXi2021Mgr.GetPlayerInfo()
    local ServerName = nil
    local playerName = nil
    local playerLv = nil
    local playerClass = nil
    local PlayerGender = nil
    local playerId = nil

    if CLoginMgr.Inst then
		local myGameServer = CLoginMgr.Inst:GetSelectedGameServer()
        ServerName = myGameServer.name
    end
    if CClientMainPlayer.Inst then
        playerName = CClientMainPlayer.Inst.Name
        playerLv = CClientMainPlayer.Inst.Level
        playerClass = CClientMainPlayer.Inst.Class
        PlayerGender = CClientMainPlayer.Inst.Gender
        playerId = CClientMainPlayer.Inst.Id
        ServerName = CClientMainPlayer.Inst:GetMyServerName()
    end

    LuaQiXi2021Mgr.MyInfo = {Id = playerId, Name = playerName, Lv = playerLv, Class = playerClass, Gender = PlayerGender, ServerName = ServerName}
end

function LuaQiXi2021Mgr.GetQuestionInfo(questionContent, questionId, duration, answerContent)
    LuaQiXi2021Mgr.questionAnswers = {}
    LuaQiXi2021Mgr.questionContent = questionContent
    LuaQiXi2021Mgr.daTiTimeLeft = duration
    LuaQiXi2021Mgr.answerContent = answerContent
    local data = QiXi_Question2021.GetData(questionId)
    if not data then return end
    table.insert(LuaQiXi2021Mgr.questionAnswers,data.Answer1)
    table.insert(LuaQiXi2021Mgr.questionAnswers,data.Answer2)
    table.insert(LuaQiXi2021Mgr.questionAnswers,data.Answer3)
    table.insert(LuaQiXi2021Mgr.questionAnswers,data.Answer4)
    for i=1,4 do
        local randomIndex = NGUITools.RandomRange(i,4)
        local temp = LuaQiXi2021Mgr.questionAnswers[i]
        LuaQiXi2021Mgr.questionAnswers[i] = LuaQiXi2021Mgr.questionAnswers[randomIndex]
        LuaQiXi2021Mgr.questionAnswers[randomIndex] = temp
    end
    CUIManager.ShowUI("QiXiDaTiWnd")
end

function LuaQiXi2021Mgr.SendQueQiaoXianQuAnswerQuestion(index)
    Gac2Gas.QueQiaoXianQuAnswerQuestion(LuaQiXi2021Mgr.questionAnswers[index])

end

function LuaQiXi2021Mgr.SyncQueQiaoXianQuPlayInfo(stage, questionCount, questionFinishCount, daJianQueQiaoFinished, monsterInfo, needHighlightSkill)
    local gender = nil
    if CClientMainPlayer.Inst then gender = CClientMainPlayer.Inst.Gender end
    LuaQiXi2021Mgr.stage = stage
    LuaQiXi2021Mgr.questionCount = questionCount
    LuaQiXi2021Mgr.questionFinishCount = questionFinishCount
    LuaQiXi2021Mgr.queQiaoFished = daJianQueQiaoFinished
    LuaQiXi2021Mgr.monsterInfo = monsterInfo
    LuaQiXi2021Mgr.GetPlayerInfo()
    if needHighlightSkill then 
        LuaQiXi2021Mgr.ShowBtnHighLight(gender)
        LuaQiXi2021Mgr.ShowBtnHighLight(gender)
    else
        CGuideMgr.Inst:EndCurrentPhase()
    end
    g_ScriptEvent:BroadcastInLua("SyncQueQiaoXianQuPlayInfo")
end

function LuaQiXi2021Mgr.GetlunarCalendarDict()
    local dict = CreateFromClass(MakeGenericClass(Dictionary, String, String))
    local lunarCalendarTable = g_LuaUtil:StrSplit(QiXi_QiXi2021.GetData().LunarCalendar,";")
    for i,v in ipairs(lunarCalendarTable) do
        if (not System.String.IsNullOrEmpty(v)) then
            local dataInfo = g_LuaUtil:StrSplit(v,",")
            local date = dataInfo[1]
            local value = dataInfo[2]
            CommonDefs.DictAdd_LuaCall(dict, date, value)
        end
    end
    LuaQiXi2021Mgr.lunarCalender = dict
end

function  LuaQiXi2021Mgr.ShowBtnHighLight(gender)
    if not LuaQiXi2021Mgr.IsQueQiaoXianQuPlay() then return end

    if gender == EnumGender.Female then  -- 女方玩家
        local logic = Guide_Logic.GetData(149)
        if logic then
            CGuideMgr.Inst:TriggerGuide(logic)
        end
    elseif gender == EnumGender.Male then -- 男方玩家
        local logic = Guide_Logic.GetData(150)
        if logic then
            CGuideMgr.Inst:TriggerGuide(logic)
        end
    end
end
