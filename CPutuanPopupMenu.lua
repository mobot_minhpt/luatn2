-- Auto Generated!!
local CommonDefs = import "L10.Game.CommonDefs"
local CPutuanPopupMenu = import "L10.UI.CPutuanPopupMenu"
local CPutuanPopupMenuItem = import "L10.UI.CPutuanPopupMenuItem"
local CPutuanPopupMgr = import "L10.UI.CPutuanPopupMgr"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local GameObject = import "UnityEngine.GameObject"
local Object = import "System.Object"
local UIEventListener = import "UIEventListener"
local Vector3 = import "UnityEngine.Vector3"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CPutuanPopupMenu.m_LoadItems_CS2LuaHook = function (this) 
    local items = CPutuanPopupMgr.Items
    if items == nil or items.Length == 0 then
        this:Close()
        return
    end
    if this.buttons ~= nil then
        do
            local i = 0
            while i < this.buttons.Length do
                GameObject.Destroy(this.buttons[i])
                i = i + 1
            end
        end
    end
    this.buttons = CreateFromClass(MakeArrayClass(GameObject), items.Length)
    do
        local i = 0
        while i < this.buttons.Length do
            local data = items[i]
            local button = TypeAs(CommonDefs.Object_Instantiate(this.imageButtonTemplate), typeof(GameObject))
            button.transform.parent = this.anchor.transform
            this.buttons[i] = button
            button:SetActive(true)
            local menuItem = CommonDefs.GetComponent_GameObject_Type(button, typeof(CPutuanPopupMenuItem))
            menuItem:Init(data.text, data.imageName, true)

            UIEventListener.Get(button).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(button).onClick, MakeDelegateFromCSFunction(this.OnMenuitemClicked, VoidDelegate, this), true)
            i = i + 1
        end
    end

    this:RepositionButtons()

    this.cachedPos = CommonDefs.op_Multiply_Vector3_Single(Vector3.one, 1000)
    this:LayoutWnd()
    --
end
CPutuanPopupMenu.m_OnMenuitemClicked_CS2LuaHook = function (this, go) 
    do
        local i = 0
        while i < this.buttons.Length do
            if this.buttons[i]:Equals(go) then
                if CPutuanPopupMgr.Items[i].action ~= nil then
                    GenericDelegateInvoke(CPutuanPopupMgr.Items[i].action, Table2ArrayWithCount({i}, 1, MakeArrayClass(Object)))
                end
                if CPutuanPopupMgr.Items[i].bCloseWnd and CPutuanPopupMgr.Items[i].bCanClick then
                    CUIManager.CloseUI(CUIResources.PutuanPopupMenu)
                end
                break
            end
            i = i + 1
        end
    end
end
CPutuanPopupMenu.m_RepositionButtons_CS2LuaHook = function (this) 
    -- 比较土的办法算这个弧形的位置，根据具体的按钮数分别算各个按钮的位置
    --   |<------ ButtonBoardWidth ----->|  ____
    --                  b5                    ^
    --              b4      b6                  
    --          b3              b7       ButtonBoardHeight
    --      b2                      b8        ~
    --   b1                            b9   ____
    if this.buttons ~= nil and this.buttons.Length ~= 0 then
        local indices = CreateFromClass(MakeArrayClass(System.Int32), this.buttons.Length)
        if this.buttons.Length == 1 then
            indices[0] = 5
        elseif this.buttons.Length == 2 then
            indices[0] = 4
            indices[1] = 6
        elseif this.buttons.Length == 3 then
            indices[0] = 3
            indices[1] = 5
            indices[2] = 7
        elseif this.buttons.Length == 4 then
            indices[0] = 2
            indices[1] = 4
            indices[2] = 6
            indices[3] = 8
        elseif this.buttons.Length == 5 then
            indices[0] = 1
            indices[1] = 3
            indices[2] = 5
            indices[3] = 7
            indices[4] = 9
        end

        do
            local i = 0
            while i < this.buttons.Length do
                local xoff, zoff
                xoff, zoff = this:GetPosition(indices[i])
                this.buttons[i].transform.localPosition = Vector3(xoff, zoff, 0)
                i = i + 1
            end
        end
    end
end

