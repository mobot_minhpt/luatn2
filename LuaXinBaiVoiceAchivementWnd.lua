local CUIFx = import "L10.UI.CUIFx"

local CUITexture = import "L10.UI.CUITexture"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnTableView = import "L10.UI.QnTableView"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local PlayerSettings = import "L10.Game.PlayerSettings"
local PopupMenuItemData=import "L10.UI.PopupMenuItemData"
local CPopupMenuInfoMgr=import "L10.UI.CPopupMenuInfoMgr"
local AlignType2=import "L10.UI.CPopupMenuInfoMgr+AlignType"
local CTooltip = import "L10.UI.CTooltip"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local AlignType = import "L10.UI.CTooltip.AlignType"
local WndType = import "L10.UI.WndType"
local Color = import "UnityEngine.Color"
local CCommonPlayerListMgr = import "L10.Game.CCommonPlayerListMgr"
local CChatHelper = import "L10.UI.CChatHelper"
local EChatPanel = import "L10.Game.EChatPanel"
local MessageMgr = import "L10.Game.MessageMgr"
local PathMode = import "DG.Tweening.PathMode"
local PathType = import "DG.Tweening.PathType"
local LuaTweenUtils = import "LuaTweenUtils"
local NGUIMath = import "NGUIMath"
local Vector2 = import "UnityEngine.Vector2"
local Vector3 = import "UnityEngine.Vector3"
local Vector4 = import "UnityEngine.Vector4"
local Ease = import "DG.Tweening.Ease"
local CCarnivalLotteryMgr = import "L10.Game.CCarnivalLotteryMgr"
local TweenPosition = import "TweenPosition"
local TweenAlpha = import "TweenAlpha"

LuaXinBaiVoiceAchivementWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaXinBaiVoiceAchivementWnd, "RightTableView", "RightTableView", QnTableView)
RegistChildComponent(LuaXinBaiVoiceAchivementWnd, "NpcPortrait", "NpcPortrait", CUITexture)
RegistChildComponent(LuaXinBaiVoiceAchivementWnd, "WordTextLabel", "WordTextLabel", UILabel)
RegistChildComponent(LuaXinBaiVoiceAchivementWnd, "ShareBtn", "ShareBtn", GameObject)
RegistChildComponent(LuaXinBaiVoiceAchivementWnd, "ContentView", "ContentView", GameObject)
RegistChildComponent(LuaXinBaiVoiceAchivementWnd, "UnlockedCountLabel", "UnlockedCountLabel", UILabel)
RegistChildComponent(LuaXinBaiVoiceAchivementWnd, "TotalCountLabel", "TotalCountLabel", UILabel)
RegistChildComponent(LuaXinBaiVoiceAchivementWnd, "NpcNameLabel", "NpcNameLabel", UILabel)
RegistChildComponent(LuaXinBaiVoiceAchivementWnd, "NpcBg", "NpcBg", CUITexture)
RegistChildComponent(LuaXinBaiVoiceAchivementWnd, "AwardBtn", "AwardBtn", GameObject)
RegistChildComponent(LuaXinBaiVoiceAchivementWnd, "FxNode", "FxNode", CUIFx)
RegistChildComponent(LuaXinBaiVoiceAchivementWnd, "GetTag", "GetTag", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaXinBaiVoiceAchivementWnd,"m_Achievements")
RegistClassMember(LuaXinBaiVoiceAchivementWnd,"m_SelectRowIndex")
RegistClassMember(LuaXinBaiVoiceAchivementWnd,"m_LastAudioId")
RegistClassMember(LuaXinBaiVoiceAchivementWnd,"m_Sound")
RegistClassMember(LuaXinBaiVoiceAchivementWnd,"m_ContentTick")
RegistClassMember(LuaXinBaiVoiceAchivementWnd,"m_Id2TableIndex")
RegistClassMember(LuaXinBaiVoiceAchivementWnd,"m_IsSelectedForInit")
RegistClassMember(LuaXinBaiVoiceAchivementWnd,"m_BgAlphaTween")
RegistClassMember(LuaXinBaiVoiceAchivementWnd,"m_NpcAlphaTween")
RegistClassMember(LuaXinBaiVoiceAchivementWnd,"m_NpcPositionTween")

function LuaXinBaiVoiceAchivementWnd:Awake()
    Gac2Gas.XinBaiAchv_QueryAchievementInfo()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.AwardBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnAwardBtnClick()
	end)


    --@endregion EventBind end
    self.m_Id2TableIndex = {}
    self.m_Achievements = {}
    self.m_LastAudioId = nil
    self.m_IsSelectedForInit = true
    self.m_SelectRowIndex = 0
    self.GetTag:SetActive(false)
    self.m_BgAlphaTween = self.NpcBg.transform:GetComponent(typeof(TweenAlpha))
    self.m_NpcAlphaTween = self.NpcPortrait.transform:GetComponent(typeof(TweenAlpha))
    self.m_NpcPositionTween = self.NpcPortrait.transform:GetComponent(typeof(TweenPosition))
end

function LuaXinBaiVoiceAchivementWnd:OnEnable()
    g_ScriptEvent:AddListener("UpdateTempPlayTimesWithKey", self, "OnUpdateTempPlayTimesWithKey")
    g_ScriptEvent:AddListener("SyncVoiceAchievementInfo", self, "OnSyncVoiceAchievementInfo")
end

function LuaXinBaiVoiceAchivementWnd:OnDisable()
    g_ScriptEvent:RemoveListener("UpdateTempPlayTimesWithKey", self, "OnUpdateTempPlayTimesWithKey")
    g_ScriptEvent:RemoveListener("SyncVoiceAchievementInfo", self, "OnSyncVoiceAchievementInfo")
    LuaVoiceAchievementMgr.SelectAchievementId = nil
    if self.m_ContentTick then
        UnRegisterTick(self.m_ContentTick)
        self.m_ContentTick = nil
    end

    local info = self.m_Achievements[self.m_SelectRowIndex]
    if info and info.IsNew then
        Gac2Gas.XinBaiAchv_OnClickAchievementInfo(info.ID)
    end

    self:StopAudio(self.m_LastAudioId)
end

function LuaXinBaiVoiceAchivementWnd:Init()
    self.ContentView:SetActive(false)
    self.m_Achievements = LuaVoiceAchievementMgr.AchievementsInfo
    local sortfuc = function(a,b)
        local data1 = VoiceAchievement_VoiceAchievement.GetData(a.ID)
        local data2 = VoiceAchievement_VoiceAchievement.GetData(b.ID)
        if a.Status == b.Status and a.Status == 0 then
            return data1.SN < data2.SN
        else
            if a.IsNew and not b.IsNew then
                return true
            elseif b.IsNew and not a.IsNew then
                return false
            end
            if a.Status == b.Status then
                return data1.SN < data2.SN
            end
            return a.Status > b.Status
        end
    end
    table.sort(self.m_Achievements,sortfuc)

    self.RightTableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_Achievements
        end,
        function(item,row)
            self:InitItem(item,row)
        end)
    self.RightTableView:ReloadData(true,false)

    self.RightTableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
        self:OnSelectItem(row)    
    end)

    --self.RightTableView:SetSelectRow(0,true)

    local totalCount = #self.m_Achievements
    local unlockedCount = 0
    for i,info in ipairs(self.m_Achievements) do
        if info.Status > 0 then
            unlockedCount = unlockedCount + 1 
        end
    end
    self.UnlockedCountLabel.text = unlockedCount
    self.TotalCountLabel.text = SafeStringFormat3("/%d",totalCount)

    --select
    local isFind = false
    --print(LuaVoiceAchievementMgr.SelectAchievementId)
    if LuaVoiceAchievementMgr.SelectAchievementId then
        LuaVoiceAchievementMgr.SelectAchievementId = tonumber(LuaVoiceAchievementMgr.SelectAchievementId)
        for index,v in ipairs(self.m_Achievements) do
            if v.ID == LuaVoiceAchievementMgr.SelectAchievementId then
                isFind = true
                self.RightTableView:SetSelectRow(index-1,true)
            end
        end
    end
    if not isFind then
        self.RightTableView:SetSelectRow(0,true)
    end

    local val = CCarnivalLotteryMgr.Inst and CCarnivalLotteryMgr.Inst:GetGamePlayTempData(248) or 0
    self:InitAwardBtn(tonumber(val))
end

function LuaXinBaiVoiceAchivementWnd:InitAwardBtn(val)--1 可领取 2 已经领取
    if val == 1 then
        LuaTweenUtils.DOKill(self.FxNode.transform, false)
        self.FxNode.transform.localPosition=Vector3(0,0,0)
        local b = NGUIMath.CalculateRelativeWidgetBounds(self.AwardBtn.transform)
        local waypoints = CUICommonDef.GetWayPoints(Vector4(b.min.x, b.max.y, b.size.x, b.size.y), 1, true)
        self.FxNode.transform.localPosition = waypoints[0]
        self.FxNode:LoadFx("fx/ui/prefab/UI_kuang_yellow02.prefab")
        LuaTweenUtils.DOLuaLocalPath(self.FxNode.transform, waypoints, 2, PathType.Linear, PathMode.Ignore, 2, - 1, Ease.Linear)
        self.GetTag:SetActive(false)
    elseif val == 2 then
        self.GetTag:SetActive(true)
        self.FxNode:DestroyFx()
    else
        self.FxNode:DestroyFx()
        self.GetTag:SetActive(false)
    end
    
end

function LuaXinBaiVoiceAchivementWnd:OnUpdateTempPlayTimesWithKey(args)
    local key = args[0]
    if key == 248 then
        local val = CCarnivalLotteryMgr.Inst:GetGamePlayTempData(key)
        self:InitAwardBtn(tonumber(val))
    end
end

function LuaXinBaiVoiceAchivementWnd:Update()
    --self:ClickThroughToClose()
end

function LuaXinBaiVoiceAchivementWnd:OnSelectItem(row)
    self:StopAudio(self.m_LastAudioId)
    if row + 1 == self.m_SelectRowIndex and not self.m_IsSelectedForInit then
        return
    end
    self.m_SelectRowIndex = row + 1
    local achi = self.m_Achievements[row+1]
    if achi then
        local data = VoiceAchievement_VoiceAchievement.GetData(achi.ID)
        local icon = data.ICON 
        local npcid = data.NPC
        local npc = NPC_NPC.GetData(npcid)
        --动效
        if not self.m_IsSelectedForInit then
            self.m_NpcAlphaTween:PlayReverse()
            self.m_NpcPositionTween:PlayReverse()
            self.m_BgAlphaTween.duration = 0.5
            self.m_BgAlphaTween:PlayReverse()

            self.m_NpcAlphaTween:SetOnFinished(DelegateFactory.Callback(function ()
                if self.m_NpcAlphaTween.value == 0 then
                    self.NpcPortrait:LoadMaterial(icon)   
                    self.m_NpcAlphaTween:PlayForward()
                    self.m_NpcPositionTween:PlayForward()
                end
            end))
            self.m_BgAlphaTween:SetOnFinished(DelegateFactory.Callback(function ()
                if self.m_BgAlphaTween.value == 0 then
                    self.NpcBg:LoadMaterial(data.BgPath)
                    self.m_BgAlphaTween.duration = 2
                    self.m_BgAlphaTween:PlayForward()
                end
            end))
        else
            self.NpcPortrait:LoadMaterial(icon)
            self.NpcBg:LoadMaterial(data.BgPath) 
            self.NpcPortrait.transform.localPosition = self.m_NpcPositionTween.from
            self.NpcPortrait.alpha = 1
            self.NpcBg.alpha = 1
        end
        
        

        if npc then
            self.NpcNameLabel.text = LocalString.CnStrH2V(npc.Name,true)
        else
            self.NpcNameLabel.text = ""
        end
        
        if not self.m_IsSelectedForInit and achi.IsNew == true then
            achi.IsNew = false
            self.m_IsSelectedForInit = false
            Gac2Gas.XinBaiAchv_OnClickAchievementInfo(achi.ID)
            self.RightTableView:ReloadData(false,false)
            return
        end
    end
    self.m_IsSelectedForInit = false
end

function LuaXinBaiVoiceAchivementWnd:InitItem(item,row)
    local audioBtn = item.transform:Find("AudioBtn").gameObject
    local audioSprite = audioBtn.transform:Find("AudioSprite")
    local lockSprite = item.transform:Find("LockSprite")
    local titleLabel = item.transform:Find("TitleLabel"):GetComponent(typeof(UILabel))
    local newTag = item.transform:Find("NewTag")
    
    local info = self.m_Achievements[row+1]
    if info then
        local status = info.Status
        local id = info.ID
        
        local data = VoiceAchievement_VoiceAchievement.GetData(id)
        if not data then
            return
        end
        local achievementID = data.AchievementID
        local achData = Achievement_Achievement.GetData(achievementID)
        if not achData then  return end
        titleLabel.text = achData.Name
        newTag.gameObject:SetActive(info.IsNew)--0未解锁,1已分享,2未分享
        audioBtn.gameObject:SetActive(status > 0)
        audioSprite.gameObject:SetActive(false)
        --lockSprite.gameObject:SetActive(status==0) 
        self:InitItemBg(row,item)
        
        UIEventListener.Get(audioBtn).onClick = DelegateFactory.VoidDelegate(function(go)
            self:OnAudioBtnClick(row+1)
        end)

        UIEventListener.Get(lockSprite.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            self:OnLockBtnClick(row+1,go)
        end)
    end
end

function LuaXinBaiVoiceAchivementWnd:InitItemBg(row,item)
    local normal = item.transform:Find("BgSprite")
    local highlight = item.transform:Find("Highlight")
    local lockSprite = item.transform:Find("LockSprite")
    local titleLabel = item.transform:Find("TitleLabel"):GetComponent(typeof(UILabel))
    lockSprite.gameObject:SetActive(false)
    normal.gameObject:SetActive(false)

    local info = self.m_Achievements[row+1]
    local status = info.Status

    if status == 0 then
        lockSprite.gameObject:SetActive(true)
        titleLabel.color = NGUIText.ParseColor24("CAD7DA", 0)
    else
        normal.gameObject:SetActive(true)
        titleLabel.color = NGUIText.ParseColor24("212121", 0)
    end    
end

function LuaXinBaiVoiceAchivementWnd:OnAudioBtnClick(index)
    if index ~= self.m_SelectRowIndex then
        self.RightTableView:SetSelectRow(index-1,true)
    end
    local id = self.m_Achievements[index].ID
    local data = VoiceAchievement_VoiceAchievement.GetData(id)
    self:StopAudio(self.m_LastAudioId)
    if data then
        if not PlayerSettings.SoundEnabled then
            local okfunc = function()
                PlayerSettings.SoundEnabled = true
                self:PlayAudio(index)
            end
            local msg = g_MessageMgr:FormatMessage("NPCCHAT_SOUND_OPEN")
            g_MessageMgr:ShowOkCancelMessage(msg,okfunc,nil,nil,nil,false)
        else
            self:PlayAudio(index)
        end      
    end
end

function LuaXinBaiVoiceAchivementWnd:PlayAudio(index)
    local id = self.m_Achievements[index].ID
    local data = VoiceAchievement_VoiceAchievement.GetData(id)
    self.m_LastAudioId = index
    local path = data.VoicePath
    self.m_Sound = SoundManager.Inst:PlayOneShot(path, Vector3.zero, nil, 0, -1)
    self.ContentView:SetActive(true)
    self.WordTextLabel.text = data.VoiceContent
    if self.m_ContentTick then
        UnRegisterTick(self.m_ContentTick)
        self.m_ContentTick = nil
    end
    self.m_ContentTick = RegisterTickOnce(function()
        self:StopAudio(index)         
    end,data.AudioLength*1000)
    --share
    UIEventListener.Get(self.ShareBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnShareBtnClick(index,go)
    end)
    -- audio sprite
    local item = self.RightTableView:GetItemAtRow(self.m_SelectRowIndex-1)
    if item then
        local audioSprite = item.transform:Find("AudioBtn/AudioSprite")
        audioSprite.gameObject:SetActive(true)
    end
end

function LuaXinBaiVoiceAchivementWnd:OnShareBtnClick(index,go)
    local achi = self.m_Achievements[index]
    local id = achi.ID

    local function SelectAction(index)           
        local channel = index+2 -- 1世界 2帮会 3队伍 4好友(屏蔽世界，从2开始)
        if channel ~= 4 then
            Gac2Gas.XinBaiAchv_ShareToChannel(id,channel)  
        else
            self:ShareToFriend()
        end
              
    end

    local selectShareItem=DelegateFactory.Action_int(SelectAction)

    local t={}
    --屏蔽世界，从2开始
    --local item=PopupMenuItemData(LocalString.GetString("分享至世界"),selectShareItem,false,nil)
    --table.insert(t, item)
    local item=PopupMenuItemData(LocalString.GetString("分享至帮会"),selectShareItem,false,nil)
    table.insert(t, item)
    local item=PopupMenuItemData(LocalString.GetString("分享至队伍"),selectShareItem,false,nil)
    table.insert(t, item)
    local item=PopupMenuItemData(LocalString.GetString("分享至好友"),selectShareItem,false,nil)
    table.insert(t, item)

    local array=Table2Array(t,MakeArrayClass(PopupMenuItemData))
    CPopupMenuInfoMgr.ShowPopupMenu(array, go.transform, AlignType2.Right,1,nil,600,true,266)
end

function LuaXinBaiVoiceAchivementWnd:ShareToFriend()
    CCommonPlayerListMgr.Inst:ShowFriendsToShareJingLingAnswer(DelegateFactory.Action_ulong(function (playerId) 
        local info = self.m_Achievements[self.m_SelectRowIndex]
        if info then
            local data = VoiceAchievement_VoiceAchievement.GetData(info.ID)
            local achidata = Achievement_Achievement.GetData(data.AchievementID)
            local messageId = MessageMgr.Inst:GetMessageIdByName("XINBAI_VOC_ACHV_MESSAGE")
            local msg = Message_Message.GetData(messageId).Message
            msg = SafeStringFormat3(msg,data.ShareText,achidata.Name,info.ID)
            CChatLinkMgr.TranslateToNGUIText(LocalString.GetString(msg),false)
            local channel = EChatPanel.Friend

            CChatHelper.SendMsgWithFilterOption(channel, msg, playerId,true)
            g_MessageMgr:ShowMessage("Share_VoiceAchievement_Succeed")
        end
        
    end))
end

function LuaXinBaiVoiceAchivementWnd:OnLockBtnClick(index,go)
    local id = self.m_Achievements[index].ID
    local data = VoiceAchievement_VoiceAchievement.GetData(id)
    if data then
        local achiid = data.AchievementID
        local achievement = Achievement_Achievement.GetData(achiid)
        if achievement then
            local condition = achievement.RequirementDisplay
            --progress
            local progress = LuaAchievementMgr:GetAchievementProgress(achiid)
            local totalProgress = 0
            if not System.String.IsNullOrEmpty(achievement.NeedProgress) then
                local strs = CommonDefs.StringSplit_ArrayChar(CommonDefs.StringSplit_ArrayChar(achievement.NeedProgress, ";")[0], ",")
                totalProgress = System.Int32.Parse(strs[strs.Length - 1])
                if progress > 0 and math.floor(progress) < progress then
                    condition = SafeStringFormat3("%s ([ff0000]%.1f[-]/%d)",condition,progress,totalProgress)
                else
                    condition = SafeStringFormat3("%s ([ff0000]%d[-]/%d)",condition,progress,totalProgress)
                end
                
            end
            

            local tips = CreateFromClass(MakeGenericClass(List, String))
            CommonDefs.ListAdd(tips, typeof(String), condition)
            if go ~= nil then
                local title = SafeStringFormat3(LocalString.GetString("【%s】解锁条件"),achievement.Name)
                CMessageTipMgr.Inst:Display(WndType.Tooltip, true, title, tips, 600, go.transform, AlignType.Right, 4294967295)
            end
        end
    end
    
    --Gac2Gas.XinBaiAchv_QueryAchievementInfo(id)
end

function LuaXinBaiVoiceAchivementWnd:StopAudio(index)
    self.ContentView:SetActive(false)
    if self.m_Sound then
        SoundManager.Inst:StopSound(self.m_Sound)
        self.m_Sound = nil
    end
    if index then
        -- audio sprite
        local item = self.RightTableView:GetItemAtRow(index-1)
        if item then
            local audioSprite = item.transform:Find("AudioBtn/AudioSprite")
            audioSprite.gameObject:SetActive(false)
        end
    end
    if not self.m_LastAudioId then return end
end

function LuaXinBaiVoiceAchivementWnd:ClickThroughToClose()
    if self.ContentView.activeSelf and Input.GetMouseButtonDown(0) then    
        if (not CUICommonDef.IsOverUI or (UICamera.lastHit.collider and
           not CUICommonDef.IsChild(self.ContentView.transform, UICamera.lastHit.collider.transform) and
            CUICommonDef.GetNearestPanelDepth(self.ContentView.transform) > CUICommonDef.GetNearestPanelDepth(UICamera.lastHit.collider.transform))) then
            self.ContentView:SetActive(false)
            if self.m_ContentTick then
                UnRegisterTick(self.m_ContentTick)
                self.m_ContentTick = nil
            end
            
        end
    end
end

function LuaXinBaiVoiceAchivementWnd:OnSyncVoiceAchievementInfo()
    self.m_IsSelectedForInit = true
    self:Init()
end
--@region UIEvent

function LuaXinBaiVoiceAchivementWnd:OnAwardBtnClick()
    local val = CCarnivalLotteryMgr.Inst:GetGamePlayTempData(248)
    if val and tonumber(val) == 1 then
        Gac2Gas.XinBaiAchv_RequestAllClearReward()
    else
        LuaVoiceAchievementMgr:OpenPreviewWnd()
    end
    
end


--@endregion UIEvent

