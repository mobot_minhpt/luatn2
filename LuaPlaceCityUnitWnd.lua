local LuaGameObject=import "LuaGameObject"
local UIScrollView=import "UIScrollView"
local UITable = import "UITable"
local Extensions = import "Extensions"
local UILabel = import "UILabel"
local UISprite = import "UISprite"
local NGUITools = import "NGUITools"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local CCityWarMgr = import "L10.Game.CCityWarMgr"
local CUITexture = import "L10.UI.CUITexture"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CForcesMgr = import "L10.Game.CForcesMgr"
local EnumCommonForce = import "L10.UI.EnumCommonForce"
local Lua = import "L10.Engine.Lua"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"

CLuaPlaceCityUnitWnd = class()
CLuaPlaceCityUnitWnd.Path = "ui/citywar/LuaPlaceCityUnitWnd"

RegistChildComponent(CLuaPlaceCityUnitWnd, "Zongzicai", UILabel)
RegistChildComponent(CLuaPlaceCityUnitWnd, "Jianzaoshu", UILabel)
RegistChildComponent(CLuaPlaceCityUnitWnd, "Fuhuodian", UILabel)
RegistChildComponent(CLuaPlaceCityUnitWnd, "LeaveModeBtn", GameObject)
RegistChildComponent(CLuaPlaceCityUnitWnd, "LayoutTemplateBtn", GameObject)
RegistChildComponent(CLuaPlaceCityUnitWnd, "LayoutTemplateWnd", GameObject)
RegistChildComponent(CLuaPlaceCityUnitWnd, "SmallScrollView", CUIRestrictScrollView)
RegistChildComponent(CLuaPlaceCityUnitWnd, "SmallTable", UITable)
RegistChildComponent(CLuaPlaceCityUnitWnd, "SmallTypeTemplate", GameObject)
RegistChildComponent(CLuaPlaceCityUnitWnd, "BigScrollView", CUIRestrictScrollView)
RegistChildComponent(CLuaPlaceCityUnitWnd, "BigTable", UITable)
RegistChildComponent(CLuaPlaceCityUnitWnd, "BigTypeTemplate", GameObject)
RegistChildComponent(CLuaPlaceCityUnitWnd, "LevelList", GameObject)
RegistChildComponent(CLuaPlaceCityUnitWnd, "FurthestCameraBtn", GameObject)

RegistClassMember(CLuaPlaceCityUnitWnd, "m_CurType")
RegistClassMember(CLuaPlaceCityUnitWnd, "m_BigTypeGO2Type")
RegistClassMember(CLuaPlaceCityUnitWnd, "m_SmallTypeGO2ID")
RegistClassMember(CLuaPlaceCityUnitWnd, "m_CurSmallTypeID")
RegistClassMember(CLuaPlaceCityUnitWnd, "m_CurJiantaLevel")
RegistClassMember(CLuaPlaceCityUnitWnd, "m_LevelBtnList")
RegistClassMember(CLuaPlaceCityUnitWnd, "m_bShowLayoutWnd")
RegistClassMember(CLuaPlaceCityUnitWnd, "m_Idx2UseBtn")
RegistClassMember(CLuaPlaceCityUnitWnd, "m_Idx2UseLabel")
RegistClassMember(CLuaPlaceCityUnitWnd, "m_Idx2SaveBtn")

RegistClassMember(CLuaPlaceCityUnitWnd, "m_OnPlaceEventTick")

RegistClassMember(CLuaPlaceCityUnitWnd, "m_IsFurthestCamera")

RegistClassMember(CLuaPlaceCityUnitWnd, "m_GuideDaTing")

RegistClassMember(CLuaPlaceCityUnitWnd, "m_UnitDDataList")

EnumCityUnitBigType_Lua = {
	eUnknown = 0,
	eChengJian = 1,
    eChengfang = 2,
    eJianta = 3,
    eFuhuodian = 4,
}


function CLuaPlaceCityUnitWnd:Awake( ... )
	self.BigTypeTemplate:SetActive(false)
	self.SmallTypeTemplate:SetActive(false)
	self.m_IsFurthestCamera = false

	self.m_UnitDDataList = {}
	CityWar_Unit.Foreach(function(k, v)
		self.m_UnitDDataList[k] = {
			ID = k,
			UIType = v.UIType,
			UIOrder = v.UIOrder,
			Type = v.Type,
			Material = v.Material,
			Icon = v.Icon,
			Status = v.Status,
		}
	end)
end

function CLuaPlaceCityUnitWnd:Init()
	if CCityWarMgr.Inst:IsInCityWarCopyScene() then
		Gac2Gas.QueryGuildCityWarMaterial()
	end

	self.m_CurType = 1
	if CCityWarMgr.Inst:IsInCityWarCopyScene() then
		self.m_CurType = 4
	end
	self:InitLeftTop()
	self:InitLeft()
	self:InitRightTop()
	self:InitDown()
end

function CLuaPlaceCityUnitWnd:OnEnable( ... )
	g_ScriptEvent:AddListener("SyncGuildCityWarMaterial", self, "OnSyncGuildCityWarMaterial")
	g_ScriptEvent:AddListener("UpdateCityWarGrade", self, "OnUpdateCityWarGrade")
	g_ScriptEvent:AddListener("CityWarUnitPlaceEvent", self, "OnCityWarUnitPlaceEvent")
	g_ScriptEvent:AddListener("OnLoadCityWarLayoutSuccess", self, "OnLoadCityWarLayoutSuccess")
	g_ScriptEvent:AddListener("UpdateMirrorWarRebornPoints", self, "OnUpdateMirrorWarRebornPoints")

	CCityWarMgr.s_IsFurthestCameraFunc = function() Lua.s_IsHook = self.m_IsFurthestCamera end
end

function CLuaPlaceCityUnitWnd:OnDisable( ... )
	g_ScriptEvent:RemoveListener("SyncGuildCityWarMaterial", self, "OnSyncGuildCityWarMaterial")
	g_ScriptEvent:RemoveListener("UpdateCityWarGrade", self, "OnUpdateCityWarGrade")
	g_ScriptEvent:RemoveListener("CityWarUnitPlaceEvent", self, "OnCityWarUnitPlaceEvent")
	g_ScriptEvent:RemoveListener("OnLoadCityWarLayoutSuccess", self, "OnLoadCityWarLayoutSuccess")
	g_ScriptEvent:RemoveListener("UpdateMirrorWarRebornPoints", self, "OnUpdateMirrorWarRebornPoints")

	self:UnRegisterOnPlaceEventTick()
	CCityWarMgr.s_IsFurthestCameraFunc = nil
end

function CLuaPlaceCityUnitWnd:InitDown()
	self:InitLevelList()
	self:InitSmallTypeList()
end

function CLuaPlaceCityUnitWnd:InitSmallTypeList()
	local unitList = {}
	for id, ddata in pairs(self.m_UnitDDataList) do
		if ddata and ddata.Status == 0 and not unitList[ddata.ID] and self.m_CurType == ddata.UIType then
			if ddata.UIType ~= EnumCityUnitBigType_Lua.eJianta or id % 10 == self.m_CurJiantaLevel then
				table.insert(unitList, ddata)
			end
		end
	end
	table.sort(unitList, function(ddata1, ddata2) if ddata1.UIOrder == ddata2.UIOrder then return ddata1.ID < ddata2.ID else return ddata1.UIOrder < ddata2.UIOrder end end)

	local level = 0
	if CLuaCityWarMgr.CurrentBasicInfo then
		level = CLuaCityWarMgr.CurrentBasicInfo.Grade
	end
	local gradeData = CityWar_CityGrade.GetData(level)

	self.m_CurSmallTypeID = 0
	self.m_SmallTypeGO2ID = {}
	Extensions.RemoveAllChildren(self.SmallTable.transform)
	self.m_GuideDaTing=nil
	for _, ddata in ipairs(unitList) do
		local ID = ddata.ID
		local go = NGUITools.AddChild(self.SmallTable.gameObject, self.SmallTypeTemplate)
		go:SetActive(true)
		if ID == 2301 then
			self.m_GuideDaTing=go
		end

		if self.m_CurSmallTypeID == 0 then
			self.m_CurSmallTypeID = ID
		end

		UIEventListener.Get(go).onClick = LuaUtils.VoidDelegate(function ( gameObject )
				self:OnSmallTypeClicked(gameObject)
			end)

		self.m_SmallTypeGO2ID[go] = ID

		local por = go.transform:Find("Texture"):GetComponent(typeof(CUITexture))
		por:LoadMaterial(ddata.Icon)

		local placedCount = CLuaCityWarMgr:GetPlacedCountForType(ddata.Type)
		local limitCount = 0
		if gradeData then
			limitCount = CLuaCityWarMgr:GetTypeLimit(gradeData, ddata.Type)
		end

		if EnumCityUnitBigType_Lua.eFuhuodian == self.m_CurType then
			placedCount = #CLuaCityWarMgr.WarRebornPointInfo
			limitCount = CLuaCityWarMgr.MaxRebornPointPerCity
		end

		local disableSprite = go.transform:Find("DisableSprite").gameObject
		local zicai = CLuaCityWarMgr.CurrentMaterial
		disableSprite:SetActive(zicai < ddata.Material or placedCount >= limitCount)

		local selectGO = go.transform:Find("SelectedSprite").gameObject
		selectGO:SetActive(ID == self.m_CurSmallTypeID)

		local amountLabel = go.transform:Find("AmountLabel"):GetComponent(typeof(UILabel))
		amountLabel.text = tostring(ddata.Material)
	end

	self.SmallTable:Reposition()
	self.SmallScrollView:ResetPosition()
end

function CLuaPlaceCityUnitWnd:RefreshSmallTypeList()
	local level = 0
	if CLuaCityWarMgr.CurrentBasicInfo then
		level = CLuaCityWarMgr.CurrentBasicInfo.Grade
	end
	local gradeData = CityWar_CityGrade.GetData(level)
	for go, id in pairs(self.m_SmallTypeGO2ID) do
		local ddata = self.m_UnitDDataList[id]
		if ddata then
			local placedCount = CLuaCityWarMgr:GetPlacedCountForType(ddata.Type)
			local limitCount = 0
			if gradeData then
				limitCount = CLuaCityWarMgr:GetTypeLimit(gradeData, ddata.Type)
			end

			if EnumCityUnitBigType_Lua.eFuhuodian == self.m_CurType then
				placedCount = #CLuaCityWarMgr.WarRebornPointInfo
				limitCount = CLuaCityWarMgr.MaxRebornPointPerCity
			end

			local disableSprite = go.transform:Find("DisableSprite").gameObject
			local zicai = CLuaCityWarMgr.CurrentMaterial
			disableSprite:SetActive(zicai < ddata.Material or placedCount >= limitCount)
		end
	end
end

function CLuaPlaceCityUnitWnd:InitLevelList()
	local level = 1
	if CLuaCityWarMgr.CurrentBasicInfo then
		level = CLuaCityWarMgr.CurrentBasicInfo.Grade
	end

	self.m_CurJiantaLevel = level
	self.m_LevelBtnList = {}

	self.LevelList:SetActive(self.m_CurType == EnumCityUnitBigType_Lua.eJianta)

	if self.m_CurType == EnumCityUnitBigType_Lua.eJianta then
		for i = 1, 5 do
			local name = "SelectBtn" .. i
			local btnGO = self.LevelList.transform:Find(name).gameObject

			UIEventListener.Get(btnGO).onClick = LuaUtils.VoidDelegate(function ( gameObject )
				self:OnLevelBtnClick(gameObject)
			end)

			local selectSprite = btnGO.transform:Find("SelectedSprite").gameObject
			selectSprite:SetActive(self.m_CurJiantaLevel == i)

			self.m_LevelBtnList[btnGO] = i
		end
	end
end

function CLuaPlaceCityUnitWnd:InitRightTop()
	self.m_bShowLayoutWnd = false
	self:InitLayoutWnd()

	CUICommonDef.SetActive(self.LayoutTemplateBtn, not CCityWarMgr.Inst:IsInCityWarCopyScene(), true)

	UIEventListener.Get(self.LeaveModeBtn).onClick = LuaUtils.VoidDelegate(function ( ... )
			self:OnLeaveModeBtnClicked(...)
		end)
	UIEventListener.Get(self.LayoutTemplateBtn).onClick = LuaUtils.VoidDelegate(function ( ... )
			self:OnLayoutTemplateBtnClicked(...)
		end)
	UIEventListener.Get(self.FurthestCameraBtn).onClick = LuaUtils.VoidDelegate(function( ... )
			self:SwitchFurthestCamera()
		end)
end

function CLuaPlaceCityUnitWnd:OnLoadCityWarLayoutSuccess()
	self:InitLayoutWnd()
end

function CLuaPlaceCityUnitWnd:InitLayoutWnd()
	self.LayoutTemplateWnd:SetActive(self.m_bShowLayoutWnd)
	if self.m_bShowLayoutWnd then
		CCityWarMgr.Inst:LoadLayout()

		local packBtn = self.LayoutTemplateWnd.transform:Find("OneKeyPackBtn").gameObject
		UIEventListener.Get(packBtn).onClick = LuaUtils.VoidDelegate(function ( ... )
			self:OnOneKeyPackBtnClicked(...)
		end)

		self.m_Idx2SaveBtn = {}
		self.m_Idx2UseLabel = {}
		self.m_Idx2UseBtn = {}
		for i = 1, 3 do
			self:InitTemplate(i)
		end

	end
end

function CLuaPlaceCityUnitWnd:InitTemplate(idx)
	local template = self.LayoutTemplateWnd.transform:Find("Template" .. idx).gameObject
	local btnUse = template.transform:Find("BtnUse").gameObject
	local btnStore = template.transform:Find("BtnStore").gameObject
	local labelStatus = template.transform:Find("LabelStatus").gameObject
	self.m_Idx2SaveBtn[idx] = btnStore
	self.m_Idx2UseLabel[idx] = labelStatus
	self.m_Idx2UseBtn[idx] = btnUse

	UIEventListener.Get(btnStore).onClick = LuaUtils.VoidDelegate(function ( ... )
		self:OnSaveTemplateBtnClick(...)
	end)
	UIEventListener.Get(btnUse).onClick = LuaUtils.VoidDelegate(function ( ... )
		self:OnUseTemplateBtnClick(...)
	end)

	local layout = CCityWarMgr.Inst.LayoutList.LayoutList[idx-1]
	CUICommonDef.SetActive(btnUse, layout.UnitList.Count > 0, true)

	labelStatus:SetActive(CLuaCityWarMgr.LastUsedTemplateIdx == (idx-1))

end

function CLuaPlaceCityUnitWnd:InitLeft()
	local typeList = {}
	for id, ddata in pairs(self.m_UnitDDataList) do
		if ddata and ddata.Status == 0 and not typeList[ddata.UIType] and ddata.UIType ~= 0 then
			if not(ddata.UIType ==  EnumCityUnitBigType_Lua.eFuhuodian and not CCityWarMgr.Inst:IsInCityWarCopyScene()) then
				typeList[ddata.UIType] = true
			end
		end
	end

	self.m_BigTypeGO2Type = {}
	Extensions.RemoveAllChildren(self.BigTable.transform)
	local iconList = CLuaCityWarMgr.UnitTypeIcon
	local nameList = CLuaCityWarMgr.UnitTypeName
	for nType, _ in pairs(typeList) do
		local icon = iconList[nType-1]

		local go = NGUITools.AddChild(self.BigTable.gameObject, self.BigTypeTemplate)
		go:SetActive(true)

		UIEventListener.Get(go).onClick = LuaUtils.VoidDelegate(function ( gameObject )
				self:OnBigTypeClicked(gameObject)
			end)

		self.m_BigTypeGO2Type[go] = nType

		local por = go.transform:Find("Texture"):GetComponent(typeof(CUITexture))
		por:LoadMaterial(icon)

		local selectGO = go.transform:Find("SelectSprite").gameObject
		selectGO:SetActive(nType == self.m_CurType)

		local nameLabel = go.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
		local name = nameList[nType-1]
		nameLabel.gameObject:SetActive(nType == self.m_CurType)
		if nType == self.m_CurType then
			nameLabel.text = name
		end

		if nType ~= EnumCityUnitBigType_Lua.eFuhuodian and CCityWarMgr.Inst:IsInCityWarCopyScene() then
			CUICommonDef.SetActive(go, false, true)
		end
	end

	self.BigTable:Reposition()
	self.BigScrollView:ResetPosition()
end

function CLuaPlaceCityUnitWnd:InitLeftTop()
	local bShowJianzaosu = not CCityWarMgr.Inst:IsInCityWarCopyScene()

	local guildId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.GuildId or 0
	local zicai = CLuaCityWarMgr.CurrentMaterial
	self.Zongzicai.text = tostring(zicai)
	local zicaiText = bShowJianzaosu and LocalString.GetString("总资材") or LocalString.GetString("我方资材")
	local zicaiLabel = LuaGameObject.GetChildNoGC(self.Zongzicai.transform,"Label").label
	zicaiLabel.text = zicaiText

	self.Jianzaoshu.gameObject:SetActive(bShowJianzaosu)
	if bShowJianzaosu then
		local total = CLuaCityWarMgr:GetTotalJianzhuCount()
		local placed = CLuaCityWarMgr:GetPlacedCount()
		self.Jianzaoshu.text = SafeStringFormat3("%d/%d", placed, total)
	end

	self.Fuhuodian.gameObject:SetActive(not bShowJianzaosu)
	if not bShowJianzaosu then
		local count = #CLuaCityWarMgr.WarRebornPointInfo
		self.Fuhuodian.text = SafeStringFormat3("%d/%d", count, CLuaCityWarMgr.MaxRebornPointPerCity)
	end
end

function CLuaPlaceCityUnitWnd:OnLeaveModeBtnClicked(go)
	if CCityWarMgr.Inst:IsInCityWarCopyScene() then
		CLuaCityWarMgr:StopPlaceMode()
		return
	end
	local msg = g_MessageMgr:FormatMessage("City_War_Quit_Construction_Mode_Confirm")
	MessageWndManager.ShowOKCancelMessage(msg,
		DelegateFactory.Action(function() CLuaCityWarMgr:StopPlaceMode() end),
		nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
end

function CLuaPlaceCityUnitWnd:OnLayoutTemplateBtnClicked(go)
	self.m_bShowLayoutWnd = not self.m_bShowLayoutWnd
	self:InitLayoutWnd()
end

function CLuaPlaceCityUnitWnd:OnBigTypeClicked(go)
	local newType = self.m_BigTypeGO2Type[go]
	if newType and newType ~= self.m_CurType then
		self.m_CurType = newType
		for nGo, nType in pairs(self.m_BigTypeGO2Type) do
			local selectGO = nGo.transform:Find("SelectSprite").gameObject
			selectGO:SetActive(nGo == go)

			local nameLabel = nGo.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
			nameLabel.gameObject:SetActive(nGo == go)

			if nGo == go then
				local nameList = CLuaCityWarMgr.UnitTypeName
				local name = nameList[nType-1]
				nameLabel.text = name
			end
		end

		self:InitDown()
	end
end

function CLuaPlaceCityUnitWnd:OnSmallTypeClicked(go)
	CLuaCityWarMgr:ClearAdjacentUnit()

	local unitId = self.m_SmallTypeGO2ID[go]
	if unitId then
		self.m_CurSmallTypeID = unitId

		for nGo, _ in pairs(self.m_SmallTypeGO2ID) do
			local selectGO = nGo.transform:Find("SelectedSprite").gameObject
			selectGO:SetActive(nGo == go)
		end

		local level = 0
		if CLuaCityWarMgr.CurrentBasicInfo then
			level = CLuaCityWarMgr.CurrentBasicInfo.Grade
		end
		local gradeData = CityWar_CityGrade.GetData(level)
		local ddata = CityWar_Unit.GetData(self.m_CurSmallTypeID)
		if ddata and gradeData then
			local placedCount = CLuaCityWarMgr:GetPlacedCountForType(ddata.Type)
			local limitCount = CLuaCityWarMgr:GetTypeLimit(gradeData, ddata.Type)

			if EnumCityUnitBigType_Lua.eFuhuodian == self.m_CurType then
				placedCount = #CLuaCityWarMgr.WarRebornPointInfo
				limitCount = CLuaCityWarMgr.MaxRebornPointPerCity
			end

			CItemInfoMgr.ShowCityWarUnitInfo(self.m_CurSmallTypeID, nil)
			local zicai = CLuaCityWarMgr.CurrentMaterial

			if zicai >= ddata.Material and limitCount > placedCount then
				local cityId = CLuaCityWarMgr.CurrentCityId
				if EnumCityUnitBigType_Lua.eFuhuodian == self.m_CurType then
					CCityWarMgr.Inst:ClearCurUnit()
					local monsterId = CLuaCityWarMgr.AttackRebornPointMonsterId
					local mainplayerId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
					if mainplayerId ~= 0 then
						local myForce = CForcesMgr.Inst:GetPlayerForce(mainplayerId)
						if myForce ~= 0 and myForce == EnumCommonForce.eDefend then
							monsterId = CLuaCityWarMgr.DefendRebornPointMonsterId
						end
					end

					local unit = CCityWarMgr.Inst:CreateUnit(CCityWarMgr.DefaultRebornPointUnitId, CLuaCityWarMgr.RebornPointUnitTemplateId, monsterId, false)
					CCityWarMgr.Inst.CurUnit = unit
					CCityWarMgr.Inst.CurUnit.Selected = true
					CCityWarMgr.Inst.CurUnit.Moving = true
				else
					local templateId = self.m_CurSmallTypeID
					Gac2Gas.CreateCityUnit(cityId, templateId)
				end
			end
		end
	end
end

function CLuaPlaceCityUnitWnd:OnLevelBtnClick(go)
	local targetLevel = self.m_LevelBtnList[go]
	if targetLevel and targetLevel ~= self.m_CurJiantaLevel then
		self.m_CurJiantaLevel = targetLevel
		for btnGO, i in pairs(self.m_LevelBtnList) do
			local selectSprite = btnGO.transform:Find("SelectedSprite").gameObject
			selectSprite:SetActive(self.m_CurJiantaLevel == i)
		end

		self:InitSmallTypeList()
	end
end

function CLuaPlaceCityUnitWnd:OnSaveTemplateBtnClick(go)
	local idx = nil
	for _idx, g in pairs(self.m_Idx2SaveBtn) do
		if g == go then
			idx = _idx
		end
	end
	if idx then
		CCityWarMgr.Inst:SaveLayout(idx-1)
	end
end

function CLuaPlaceCityUnitWnd:OnUseTemplateBtnClick(go)
	local idx = nil
	for _idx, g in pairs(self.m_Idx2UseBtn) do
		if g == go then
			idx = _idx
		end
	end
	if CLuaCityWarMgr.CurrentCityId ~= "" and idx and idx > 0 and idx <= 3 and CCityWarMgr.Inst.LayoutList and CCityWarMgr.Inst.LayoutList.LayoutList.Count >= idx and CCityWarMgr.Inst.LayoutList.LayoutList[idx-1] then
		Gac2Gas.RequestUseCityWarLayoutTemplateCheck(CLuaCityWarMgr.CurrentCityId, idx-1)
	end
end

function CLuaPlaceCityUnitWnd:OnOneKeyPackBtnClicked(go)
	local msg = g_MessageMgr:FormatMessage("City_War_OneKeyPack_Confirm")
    MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function ()
    	if CLuaCityWarMgr.CurrentCityId ~= "" then
	        Gac2Gas.RequestOneKeyPackAllUnit(CLuaCityWarMgr.CurrentCityId)
	    end
    end), nil, nil, nil, false)
end

function CLuaPlaceCityUnitWnd:OnSyncGuildCityWarMaterial()
	self:InitLeftTop()
	--self:InitDown()
	self:RefreshSmallTypeList()
end

function CLuaPlaceCityUnitWnd:OnUpdateCityWarGrade()
	self:InitLeftTop()
	--self:InitDown()
	self:RefreshSmallTypeList()
end

function CLuaPlaceCityUnitWnd:OnCityWarUnitPlaceEvent()
	if not self.m_OnPlaceEventTick then
		self:RegisterOnPlaceEventTick()
	end
end

function CLuaPlaceCityUnitWnd:OnUpdateMirrorWarRebornPoints()
	self:InitLeftTop()
end

function CLuaPlaceCityUnitWnd:SwitchFurthestCamera()
	self.m_IsFurthestCamera = not self.m_IsFurthestCamera
	local spriteName = self.m_IsFurthestCamera and "placecitywarunitwnd_suoxiao" or "placecitywarunitwnd_fangda"
	self.FurthestCameraBtn:GetComponent(typeof(UISprite)).spriteName = spriteName

	CameraFollow.Inst:CityWarAdjustCamera()
end

function CLuaPlaceCityUnitWnd:RegisterOnPlaceEventTick()
	self.m_OnPlaceEventTick = RegisterTick(
		function()
			--self:InitDown()
			self:RefreshSmallTypeList()
			self:InitLeftTop()

			self:UnRegisterOnPlaceEventTick()
		end
		, 1000)
end

function CLuaPlaceCityUnitWnd:UnRegisterOnPlaceEventTick()
	if self.m_OnPlaceEventTick then
		UnRegisterTick(self.m_OnPlaceEventTick)
		self.m_OnPlaceEventTick = nil
	end
end

function CLuaPlaceCityUnitWnd:GetGuideGo( methodName )
	--判断是否有权限执行操作
	if methodName=="GetDaTingItem" then
		if self.m_CurType ==1 and self.m_GuideDaTing then
			CUICommonDef.SetFullyVisible(self.m_GuideDaTing,self.SmallTable.transform.parent:GetComponent(typeof(UIScrollView)))
			return self.m_GuideDaTing
			-- return self.SmallTable.transform:GetChild(4).gameObject
		end
	end
	return nil
end
