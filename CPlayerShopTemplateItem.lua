-- Auto Generated!!
local Boolean = import "System.Boolean"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CEquipment = import "L10.Game.CEquipment"
local CFreightMgr = import "L10.Game.CFreightMgr"
local CItem = import "L10.Game.CItem"
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CPlayerShopMgr = import "L10.UI.CPlayerShopMgr"
local CPlayerShopTemplateItem = import "L10.UI.CPlayerShopTemplateItem"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CTickMgr = import "L10.Engine.CTickMgr"
local DateTime = import "System.DateTime"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EnumGuildFreightStatus = import "L10.Game.EnumGuildFreightStatus"
local ETickType = import "L10.Engine.ETickType"
local EventManager = import "EventManager"
local ItemGet_item = import "L10.Game.ItemGet_item"
local LocalString = import "LocalString"
local PlayerShop_TimeLimitListing = import "L10.Game.PlayerShop_TimeLimitListing"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CPlayerShopTemplateItem.m_UpdateData_CS2LuaHook = function (this, templateId, sellCount, focus, showCheckBox, showRecommandTag) 
    this.m_TemplateId = templateId
    this.m_Focus = focus
    this.m_ShowCheckBox = showCheckBox
    this.m_ShowRecommandTag = showRecommandTag
    local item = CItemMgr.Inst:GetItemTemplate(templateId)
    if item ~= nil then
        this.m_ItemNameLabel.text = item.Name
        this.m_ItemNameLabel.color = CItem.GetColor(templateId)
        this.m_ItemTexture:LoadMaterial(item.Icon)
    else
        local equip = CItemMgr.Inst:GetEquipTemplate(templateId)
        if equip ~= nil then
            this.m_ItemNameLabel.text = equip.Name
            this.m_ItemNameLabel.color = CEquipment.GetColor(templateId)
            this.m_ItemTexture:LoadMaterial(equip.Icon)
        else
            local aliasId = CPlayerShopMgr.Inst:FromAliasTemplateId(templateId)
            local equip2 = CItemMgr.Inst:GetEquipTemplate(aliasId)
            if equip2 ~= nil then
                this.m_ItemNameLabel.text = equip2.AliasName
                this.m_ItemNameLabel.color = CEquipment.GetColor(aliasId)
                this.m_ItemTexture:LoadMaterial(equip2.Icon)
            end
        end
    end
    this.m_SellCount = sellCount
    this:InitCountLabel()
    if this.m_RecommandTag ~= nil then
        this.m_RecommandTag:SetActive(showRecommandTag)
    end
    local focusVisible = false 
    if this.m_FocusTag ~= nil then
        focusVisible = (focus and not showRecommandTag)
        this.m_FocusTag:SetActive(focusVisible)
    end
    if this.m_NeedTag ~= nil then
        this.m_NeedTag:SetActive(this:IsNeeded(templateId) and not focusVisible)
    end
    if this.m_FocusCheckBox ~= nil then
        this.m_FocusCheckBox.Visible = this.m_ShowCheckBox and not showRecommandTag
        this.m_FocusCheckBox:SetSelected(focus, true)
    end
end
CPlayerShopTemplateItem.m_InitCountLabel_CS2LuaHook = function (this) 
    if this.m_SellCount == 0 then
        this.m_CountLabel.text = LocalString.GetString("在售：[ff0000]0[-]")
    else
        this.m_CountLabel.text = System.String.Format(LocalString.GetString("在售：[ffff00]{0}[-]"), this.m_SellCount > 99 and "99+" or this.m_SellCount)
    end

    if this.m_CancelAction ~= nil then
        invoke(this.m_CancelAction)
        this.m_CancelAction = nil
    end

    if PlayerShop_TimeLimitListing.Exists(this.m_TemplateId) then
        local data = PlayerShop_TimeLimitListing.GetData(this.m_TemplateId)

        local endTime = DateTime.Parse(data.EndTime)

        local seconds = endTime:Subtract(CServerTimeMgr.Inst:GetZone8Time()).TotalSeconds
        if seconds <= 0 then
            this.m_CountLabel.text = nil
        else
            local action = DelegateFactory.Action(function () 
                if this.m_SellCount == 0 then
                    this.m_CountLabel.text = LocalString.GetString("在售：[ff0000]0[-]")
                else
                    this.m_CountLabel.text = SafeStringFormat3(LocalString.GetString("在售：[ffff00]%s[-]"), this.m_SellCount > 99 and "99+" or this.m_SellCount)
                end

                local diff = endTime:Subtract(CServerTimeMgr.Inst:GetZone8Time()).TotalSeconds
                local dayDiff = math.floor(math.floor(diff / 86400))
                if dayDiff > 0 then
                    this.m_CountLabel.text = SafeStringFormat3(LocalString.GetString("剩余[ffff00]%d[-]天 %s"), dayDiff, this.m_CountLabel.text)
                else
                    local hourDiff = math.floor(math.floor(diff / 3600))
                    local minuteDiff = math.floor(math.floor((diff % 3600) / 60))

                    if hourDiff > 0 then
                        this.m_CountLabel.text = SafeStringFormat3(LocalString.GetString("剩余[ff0000]%02d:%02d[-] %s"), hourDiff, minuteDiff, this.m_CountLabel.text)
                    else
                        if minuteDiff > 0 then
                            this.m_CountLabel.text = SafeStringFormat3(LocalString.GetString("剩余[ff0000]%02d:%02d[-] %s"), 0, minuteDiff, this.m_CountLabel.text)
                        else
                            if diff <= 0 then
                                this.m_CountLabel.text = nil
                            else
                                this.m_CountLabel.text = SafeStringFormat3(LocalString.GetString("剩余[ff0000]00:00[/c] %s"), this.m_CountLabel.text)
                            end
                        end
                    end
                end
            end)
            this.m_CancelAction = CTickMgr.Register(action, 5000, ETickType.Loop)
            invoke(action)
        end
    end
    LuaPlayerShopTemplateItem.InitContentForFashionPreview(this)
end
CPlayerShopTemplateItem.m_OnEnable_CS2LuaHook = function (this) 
    if this.m_ItemTexture ~= nil then
        UIEventListener.Get(this.m_ItemTexture.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_ItemTexture.gameObject).onClick, MakeDelegateFromCSFunction(this.OnItemTextureClick, VoidDelegate, this), true)
    end
    if this.m_FocusCheckBox ~= nil then
        this.m_FocusCheckBox.OnValueChanged = CommonDefs.CombineListner_Action_bool(this.m_FocusCheckBox.OnValueChanged, MakeDelegateFromCSFunction(this.OnCheckBoxValueChanged, MakeGenericClass(Action1, Boolean), this), true)
    end
    EventManager.AddListenerInternal(EnumEventType.QueryPlayerShopOnShelfNumResult, MakeDelegateFromCSFunction(this.OnQueryPlayerShopOnShelfNumResult, MakeGenericClass(Action2, MakeGenericClass(List, UInt32), Boolean), this))
end
CPlayerShopTemplateItem.m_OnDisable_CS2LuaHook = function (this) 
    if this.m_ItemTexture ~= nil then
        UIEventListener.Get(this.m_ItemTexture.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_ItemTexture.gameObject).onClick, MakeDelegateFromCSFunction(this.OnItemTextureClick, VoidDelegate, this), false)
    end
    if this.m_FocusCheckBox ~= nil then
        this.m_FocusCheckBox.OnValueChanged = CommonDefs.CombineListner_Action_bool(this.m_FocusCheckBox.OnValueChanged, MakeDelegateFromCSFunction(this.OnCheckBoxValueChanged, MakeGenericClass(Action1, Boolean), this), false)
    end
    EventManager.RemoveListenerInternal(EnumEventType.QueryPlayerShopOnShelfNumResult, MakeDelegateFromCSFunction(this.OnQueryPlayerShopOnShelfNumResult, MakeGenericClass(Action2, MakeGenericClass(List, UInt32), Boolean), this))

    if this.m_CancelAction ~= nil then
        invoke(this.m_CancelAction)
        this.m_CancelAction = nil
    end
end
CPlayerShopTemplateItem.m_OnDestroy_CS2LuaHook = function (this) 
    if this.m_CancelAction ~= nil then
        invoke(this.m_CancelAction)
        this.m_CancelAction = nil
    end
end
CPlayerShopTemplateItem.m_IsNeeded_CS2LuaHook = function (this, templateId) 
    if CClientMainPlayer.Inst ~= nil and CommonDefs.ListContains(CClientMainPlayer.Inst.TaskProp.CurrentTaskList, typeof(UInt32), CFreightMgr.Inst.taskId) and not CommonDefs.DictGetValue(CClientMainPlayer.Inst.TaskProp.CurrentTasks, typeof(UInt32), CFreightMgr.Inst.taskId).IsFailed then
        do
            local i = 0
            while i < CFreightMgr.Inst.cargoList.Count do
                local item = ItemGet_item.GetData(CFreightMgr.Inst.cargoList[i].templateId)
                if item ~= nil and (CFreightMgr.Inst.cargoList[i].status == EnumGuildFreightStatus.eNotFilled or CFreightMgr.Inst.cargoList[i].status == EnumGuildFreightStatus.eNotFilledNeedHelp) then
                    if templateId == item.ID then
                        return true
                    end
                end
                i = i + 1
            end
        end
    end
    return false
end
CPlayerShopTemplateItem.m_OnQueryPlayerShopOnShelfNumResult_CS2LuaHook = function (this, selledCounts, publicity)
    if publicity then
        return
    end

    do
        local i = 0
        while i < selledCounts.Count do
            local id = selledCounts[i]
            local count = selledCounts[i + 1]

            if id == this.m_TemplateId then
                this:UpdateData(id, count, this.m_Focus, this.m_ShowCheckBox, this.m_ShowRecommandTag)
                break
            end
            i = i + 2
        end
    end
end

LuaPlayerShopTemplateItem = {}
function LuaPlayerShopTemplateItem.InitContentForFashionPreview(this)
    local leftCountLabelGo = this.transform:Find("LeftCountLabel")
    local previewFashionButtonGo = this.transform:Find("PreviewFashionButton")
    if leftCountLabelGo and previewFashionButtonGo then
        local leftCountLabel = leftCountLabelGo:GetComponent(typeof(UILabel))
        local fashionSuitId = LuaAppearancePreviewMgr:GetPreviewFashonSuitByItem(this.m_TemplateId)
        if fashionSuitId>0 then
            this.m_CountLabel.gameObject:SetActive(false)
            leftCountLabelGo.gameObject:SetActive(true)
            leftCountLabel.text = this.m_CountLabel.text
            previewFashionButtonGo.gameObject:SetActive(true)
            UIEventListener.Get(previewFashionButtonGo.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) 
                LuaAppearancePreviewMgr:ShowFashionSuitSubview(fashionSuitId) 
            end)
        else
            this.m_CountLabel.gameObject:SetActive(true)
            leftCountLabelGo.gameObject:SetActive(false)
            previewFashionButtonGo.gameObject:SetActive(false)
        end
    end
    
end  
