-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CommonDefs = import "L10.Game.CommonDefs"
local COtherSkillDetailView = import "L10.UI.COtherSkillDetailView"
local CWeddingMgr = import "L10.Game.CWeddingMgr"
local L10 = import "L10"
local LocalString = import "LocalString"
local Object = import "System.Object"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
local Skill_OtherSkill = import "L10.Game.Skill_OtherSkill"
local SkillCategory = import "L10.Game.SkillCategory"
local StringBuilder = import "System.Text.StringBuilder"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local UILabel = import "UILabel"
local CChatLinkMgr = import "CChatLinkMgr"
local UICamera = import "UICamera"

COtherSkillDetailView.m_Init_CS2LuaHook = function (this, otherSkill) 
    this:Clear()
    this.xianZhiSkillShowUp.gameObject:SetActive(false)
    this.xianZhiSkillLevelUp.gameObject:SetActive(false)
    this.xianZhiSkillStatus.gameObject:SetActive(false)
    this.xianZhiSkillTime.gameObject:SetActive(false)
    local mainplayer = CClientMainPlayer.Inst
    if mainplayer == nil then
        return
    end

    local other = Skill_OtherSkill.GetData(otherSkill)
    if other == nil then
        return
    end
    local skillClass = other.SkillID

    local skill = nil
    local skillLearned = false
    if mainplayer.SkillProp:IsOriginalSkillClsExist(skillClass) then
        this.skillId = mainplayer.SkillProp:GetSkillIdWithDeltaByCls(skillClass, mainplayer.Level)
        skill = Skill_AllSkills.GetData(this.skillId)
        skillLearned = true
    else
        skill = Skill_AllSkills.GetData(CLuaDesignMgr.Skill_AllSkills_GetSkillId(skillClass, 1))
    end
    if skill ~= nil then
        this.icon:LoadSkillIcon(skill.SkillIcon)
        this.skillNameLabel.text = skill.Name
        if skillLearned then
            this.levelLabel.gameObject:SetActive(true)
            this.levelLabel.text = tostring(skill.Level)
            this.disableSprite.enabled = false
        end
        this.passiveSprite.enabled = (skill.ECategory == SkillCategory.Passive)

        this:ShowDescription(skill, other, skillLearned)
        this:OnUpdateCooldown()
    end
end
COtherSkillDetailView.m_ShowDescription_CS2LuaHook = function (this, skill, other, learned) 
    if skill == nil or other == nil then
        return
    end

    if not System.String.IsNullOrEmpty(other.Condition) then
        this.conditionItem.gameObject:SetActive(true)
        this.conditionItem:Init(LocalString.GetString("使用条件"), other.Condition)
    else
        this.conditionItem.gameObject:SetActive(false)
    end
    this.descriptionItem.gameObject:SetActive(true)
    if learned then
        local builder = NewStringBuilderWraper(StringBuilder)
        builder:Append(skill.Display)
        if CWeddingMgr.IsCoupleSkill(skill.SkillClass) then
            local nextSkillId = Skill_AllSkills.GetNextLevelSkillId(skill.ID)
            if Skill_AllSkills.Exists(nextSkillId) then
                --升级条件
                local formula = CommonDefs.GetFormulaObject(209, typeof(Formula2))
                if formula ~= nil then
                    local friendliness = GenericDelegateInvoke(formula, Table2ArrayWithCount({CLuaDesignMgr.Skill_AllSkills_GetLevel(nextSkillId)}, 1, MakeArrayClass(Object)))
                    builder:Append("\n")
                    builder:Append(g_MessageMgr:FormatMessage("Wedding_Skill_Upgrade_Consume", friendliness))
                else
                    L10.CLogMgr.Log("client formula 209 error.")
                end
            else
                --满级
                builder:Append("\n")
                builder:Append(g_MessageMgr:FormatMessage("Wedding_Skill_TotalLevel"))
            end
        end
        this.descriptionItem:Init(LocalString.GetString("技能详情"), ToStringWrap(builder))
    else
        this.descriptionItem:Init(LocalString.GetString("技能详情"), other.Display_Unlearn)
    end

    this.detailTable:Reposition()
    this.detailScrollView:ResetPosition()
end
COtherSkillDetailView.m_Clear_CS2LuaHook = function (this) 
    this.skillId = 0
    this.icon:Clear()
    this.disableSprite.enabled = true
    this.levelLabel.gameObject:SetActive(false)
    this.passiveSprite.enabled = false
    this.skillNameLabel.text = nil
    this.skillCDLabel.text = nil
    this.conditionItem.gameObject:SetActive(false)
    this.descriptionItem.gameObject:SetActive(false)
end
COtherSkillDetailView.m_OnUpdateCooldown_CS2LuaHook = function (this) 
    if CClientMainPlayer.Inst == nil or not Skill_AllSkills.Exists(this.skillId) then
        return
    end
    --只处理结婚技能
    if not CWeddingMgr.IsCoupleSkill(CLuaDesignMgr.Skill_AllSkills_GetClass(this.skillId)) then
        return
    end

    local globalRemain = CClientMainPlayer.Inst.CooldownProp:GetServerRemainTime(1000)
    -- TODO : global skill cd
    local globalTotal = CClientMainPlayer.Inst.CooldownProp:GetServerTotalTime(1000)
    --float globalPercent = globalTotal == 0 ? 0 : (float)globalRemain / (float)globalTotal;

    local remain = CClientMainPlayer.Inst.CooldownProp:GetServerRemainTime(this.skillId)
    local total = CClientMainPlayer.Inst.CooldownProp:GetServerTotalTime(this.skillId)
    --float percent = total == 0 ? 0 : (float)remain / (float)total;
    if globalRemain > remain then
        remain = globalRemain
    end
    remain = math.floor(remain / 1000)
    if remain < 1 then
        if not System.String.IsNullOrEmpty(this.skillCDLabel.text) then
            this.skillCDLabel.text = nil
        end
    else
        this.skillCDLabel.text = g_MessageMgr:FormatMessage("Wedding_Skill_CD", remain)
    end
end
COtherSkillDetailView.m_InitXianZhiSkill_CS2LuaHook = function (this, xianzhiSkill) 
    this:Clear()
    this.xianZhiSkillId = xianzhiSkill
    local mainplayer = CClientMainPlayer.Inst
    if mainplayer == nil then
        return
    end

    local skill = Skill_AllSkills.GetData(xianzhiSkill)
    if skill ~= nil then
        this.icon:LoadSkillIcon(skill.SkillIcon)
        this.skillNameLabel.text = skill.Name
        this.levelLabel.gameObject:SetActive(true)
        this.levelLabel.text = tostring(skill.Level)
        this.disableSprite.enabled = false
        this.passiveSprite.enabled = (skill.ECategory == SkillCategory.Passive)

        this:OnUpdateCooldown()
    end

    -- 是否是默认的伪仙职技能
    if CLuaDesignMgr.Skill_AllSkills_GetClass(xianzhiSkill) ~= XianZhi_Setting.GetData().FakeSkillId then
        this:ShowXianZhiDescription(skill, false)
        this.xianZhiSkillLevelUp.gameObject:SetActive(true)
        -- 主动被动技能都显示
        this.xianZhiSkillShowUp.gameObject:SetActive(true)
        local showLabel = this.xianZhiSkillShowUp.transform:Find("Label"):GetComponent(typeof(UILabel))
        showLabel.text = LocalString.GetString("勾选技能生效")
        -- 是否被激活在主界面
        if CClientMainPlayer.Inst.SkillProp:IsOriginalSkillClsExist(Skill_AllSkills.GetClass(xianzhiSkill)) then
            this.xianZhiSkillShowUp:SetSelected(true, true)
        else
            this.xianZhiSkillShowUp:SetSelected(false, true)
        end
        -- 描述仙职技能的时间/状态
        local xianZhiData =  CClientMainPlayer.Inst.PlayProp.XianZhiData
        if xianZhiData.OperateType == EnumXianZhiOperateType.eNone then
            this.xianZhiSkillStatus.text = nil
            this.xianZhiSkillTime.text = nil
        else
            local operateStr = nil
            local skillId = CLuaXianzhiMgr.GetXianZhiSkillByDelta()
            local level = Skill_AllSkills.GetLevel(skillId)
            if xianZhiData.OperateType == EnumXianZhiOperateType.ePromote then
                operateStr = SafeStringFormat3(LocalString.GetString("提升至%s级"), tostring(level))
            elseif xianZhiData.OperateType == EnumXianZhiOperateType.eWeaken then
                operateStr = SafeStringFormat3(LocalString.GetString("下降至%s级"), tostring(level))
            elseif xianZhiData.OperateType == EnumXianZhiOperateType.eForbid then
                operateStr = LocalString.GetString("封印无法使用")
            end
            
            if operateStr~=nil then
                this.xianZhiSkillStatus.text = SafeStringFormat3(LocalString.GetString("被%s%s"), xianZhiData.OperatePlayerName, operateStr)
                local totalSeconds = xianZhiData.OperateExpiredTime - CServerTimeMgr.Inst.timeStamp
                this.xianZhiSkillStatusRemainTime = totalSeconds
                local skillTimeStr = nil
                if totalSeconds <= 0 then
                    this.xianZhiSkillStatus.gameObject:SetActive(false)
                    this.xianZhiSkillTime.gameObject:SetActive(false)
                else
                    this.xianZhiSkillStatus.gameObject:SetActive(true)
                    this.xianZhiSkillTime.gameObject:SetActive(true)
                    if totalSeconds >= 3600 then
                        skillTimeStr = SafeStringFormat3("%02d:%02d:%02d", math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
                    else
                        skillTimeStr = SafeStringFormat3("%02d:%02d", math.floor(totalSeconds / 60), totalSeconds % 60)
                    end
                    this.xianZhiSkillTime.text = skillTimeStr
                    if this.xianZhiSkillStatusTick == nil then
                        this.xianZhiSkillStatusTick = RegisterTick(function ()
                            this.xianZhiSkillStatusRemainTime = this.xianZhiSkillStatusRemainTime - 1
                            if this.xianZhiSkillStatusRemainTime >= 0 then
                                local totalTime = this.xianZhiSkillStatusRemainTime
                                local timeStr = nil
                                if totalTime >= 3600 then
                                    timeStr = SafeStringFormat3("%02d:%02d:%02d", math.floor(totalTime / 3600), math.floor(totalTime % 3600 / 60), totalTime % 60)
                                else
                                    timeStr = SafeStringFormat3("%02d:%02d", math.floor(totalTime / 60), totalTime % 60)
                                end
                                this.xianZhiSkillTime.text = timeStr
                            else
                                this.xianZhiSkillTime.text = nil
                                if this.xianZhiSkillStatusTick then
                                    this.xianZhiSkillStatusTick:Invoke()
                                    this.xianZhiSkillStatusTick = nil
                                    this:InitXianZhiSkill(this.xianZhiSkillId)
                                end
                            end
                        end, 1000)
                    end
                end
            end
        end
    else
        this:ShowXianZhiDescription(skill, true)
        this.levelLabel.gameObject:SetActive(false)
        this.disableSprite.enabled = true
        this.xianZhiSkillShowUp.gameObject:SetActive(false)
        this.xianZhiSkillLevelUp.gameObject:SetActive(false)
    end

    this.xianZhiSkillShowUp.OnValueChanged = DelegateFactory.Action_bool(
		function(select)
            if select then
                Gac2Gas.RequestActiveXianZhiSkill()
            else
                Gac2Gas.RequestCancelXianZhiSkill()
            end
        end
    )
    
    CommonDefs.AddOnClickListener(this.xianZhiSkillLevelUp.gameObject, DelegateFactory.Action_GameObject(function (go)
		CUIManager.ShowUI(CLuaUIResources.XianZhiSkillUpgradeWnd)
    end), false)
end

COtherSkillDetailView.m_ShowXianZhiDescription_CS2LuaHook = function (this, skill, isFakeSkill) 
    if skill == nil then
        return
    end

    this.conditionItem.gameObject:SetActive(true)
    this.conditionItem:Init(LocalString.GetString("使用条件"), g_MessageMgr:FormatMessage("XianZhi_Skill_Use_Condition"))
    local conditionLabel = this.conditionItem.gameObject.transform:Find("ContentLabel").gameObject

    UIEventListener.Get(conditionLabel).onClick = DelegateFactory.VoidDelegate(function (p)
        local url = p:GetComponent(typeof(UILabel)):GetUrlAtPosition(UICamera.lastWorldPosition)
        if url ~= nil then
            CChatLinkMgr.ProcessLinkClick(url, nil)
        end
    end)

    this.descriptionItem.gameObject:SetActive(true)

    local display = skill.Display
    if not isFakeSkill then
        local now = CServerTimeMgr.Inst.timeStamp
        local daysRemain = math.floor((CLuaXianzhiMgr.m_RemainXianZhiTime - now) / 3600 / 24)
        if daysRemain < 0 then
            daysRemain = 0
        else
            display = display .."\n".. SafeStringFormat3(LocalString.GetString("当前仙职技能有效期剩余[FA800A]%s天[-]"), tostring(daysRemain))
        end
    end
    
    this.descriptionItem:Init(LocalString.GetString("技能详情"), display)
    this.detailTable:Reposition()
    this.detailScrollView:ResetPosition()
end
