local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CTaskMgr = import "L10.Game.CTaskMgr"
local CScene = import "L10.Game.CScene"
local CGamePlayMgr = import "L10.Game.CGamePlayMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"

LuaShiTuJianLaJiTaskView = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaShiTuJianLaJiTaskView, "m_TaskName", "TaskName", UILabel)
RegistChildComponent(LuaShiTuJianLaJiTaskView, "m_CountDownLabel", "CountDownLabel", UILabel)
RegistChildComponent(LuaShiTuJianLaJiTaskView, "m_LeaveBtn", "LeaveBtn", GameObject)
RegistChildComponent(LuaShiTuJianLaJiTaskView, "m_RuleBtn", "RuleBtn", GameObject)
RegistChildComponent(LuaShiTuJianLaJiTaskView, "m_DescLab", "DescLab", UILabel)
RegistChildComponent(LuaShiTuJianLaJiTaskView, "m_ProgressRoot", "ProgressRoot", GameObject)

--@endregion RegistChildComponent end

function LuaShiTuJianLaJiTaskView:Awake()
    UIEventListener.Get(self.m_LeaveBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLeaveBtnClick(go)
	end)

    UIEventListener.Get(self.m_RuleBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRuleBtnClick(go)
	end)
end

function LuaShiTuJianLaJiTaskView:Init()
    self:UpdateTaskInfo()
end

function LuaShiTuJianLaJiTaskView:UpdateTaskInfo()
    local task = Task_Task.GetData(ShiTuTask_JianLaJiSetting.GetData().TaskId)
    local info = LuaShiTuMgr.m_JianLaJiScoreInfo
    local curScore = info.curScore and info.curScore or 0
    local needScore = info.needScore and info.needScore or 0
    self.m_TaskName.text = task.Display
    self.m_DescLab.text = SafeStringFormat3(LocalString.GetString("%s（%d/%d）"), g_MessageMgr:FormatMessage("ShiTu_JianLaJi_Description"), curScore, needScore)
    local texture = self.m_ProgressRoot.transform:Find("mogu_Texture"):GetComponent(typeof(UITexture))
    texture.fillAmount = needScore>0 and curScore/needScore or 0
end

function LuaShiTuJianLaJiTaskView:OnEnable()
    g_ScriptEvent:AddListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
    g_ScriptEvent:AddListener("SyncShiTuJianLaJiScore", self, "OnSyncShiTuJianLaJiScore")
end

function LuaShiTuJianLaJiTaskView:OnDisable()
    g_ScriptEvent:RemoveListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
    g_ScriptEvent:RemoveListener("SyncShiTuJianLaJiScore", self, "OnSyncShiTuJianLaJiScore")
end

function LuaShiTuJianLaJiTaskView:OnSceneRemainTimeUpdate(args)
    if CScene.MainScene then
        if CScene.MainScene.ShowTimeCountDown then
            self.m_CountDownLabel.text = SafeStringFormat3(LocalString.GetString("剩余 [c][00ff00]%s[-][/c]"), LuaGamePlayMgr:GetRemainTimeText(CScene.MainScene.ShowTime))
        else
            self.m_CountDownLabel.text = nil
        end
    else
        self.m_CountDownLabel.text = nil
    end
end

function LuaShiTuJianLaJiTaskView:OnSyncShiTuJianLaJiScore(curScore, needScore)
    self:UpdateTaskInfo()
end

function LuaShiTuJianLaJiTaskView:OnLeaveBtnClick(go)
    CGamePlayMgr.Inst:LeavePlay()
end

function LuaShiTuJianLaJiTaskView:OnRuleBtnClick(go)
    g_MessageMgr:ShowMessage("ShiTu_Jian_La_Ji_Rule")
end

