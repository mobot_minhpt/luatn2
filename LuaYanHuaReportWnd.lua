local UIInput = import "UIInput"
local UITexture = import "UITexture"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local CommonDefs = import "L10.Game.CommonDefs"

CLuaYanHuaReportWnd = class()

RegistChildComponent(CLuaYanHuaReportWnd, "m_ReportBtn","ReportBtn", GameObject)
RegistChildComponent(CLuaYanHuaReportWnd, "m_InputReason","StatusText", UIInput)
RegistChildComponent(CLuaYanHuaReportWnd, "m_SreenShotTexture","SreenShotTexture", UITexture)


function CLuaYanHuaReportWnd:Awake()
    UIEventListener.Get(self.m_ReportBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        self:ReportFireworks()
    end)

end

function CLuaYanHuaReportWnd:Init()
    CUICommonDef.LoadLocalFile2Texture(self.m_SreenShotTexture,CLuaYanHuaEditorMgr.ImagePath,815,475)
end

function CLuaYanHuaReportWnd:ReportFireworks()
    local tex = self.m_SreenShotTexture.mainTexture
    if tex ~= nil then
        local data = CommonDefs.EncodeToJPG(tex)
        local onFinished = DelegateFactory.Action_string(function (url)
            if url ==nil then
                url = ""
            end

            local reportReason = self.m_InputReason.value == nil and "" or StringTrim(self.m_InputReason.value)
            Gac2Gas.YanHuaLiBaoScreenshotReport(url,reportReason)
        end)
        CPersonalSpaceMgr.Inst:UploadPic("screenshot", data, onFinished)
    end
end