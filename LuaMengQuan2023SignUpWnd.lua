local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"

local DelegateFactory  = import "DelegateFactory"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CQnSymbolParser = import "CQnSymbolParser"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CRankData = import "L10.UI.CRankData"

LuaMengQuan2023SignUpWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaMengQuan2023SignUpWnd, "RuleButton", "RuleButton", GameObject)
RegistChildComponent(LuaMengQuan2023SignUpWnd, "RankButton", "RankButton", GameObject)
RegistChildComponent(LuaMengQuan2023SignUpWnd, "PlayRewardTime", "PlayRewardTime", UILabel)
RegistChildComponent(LuaMengQuan2023SignUpWnd, "TimeInfo", "TimeInfo", UILabel)
RegistChildComponent(LuaMengQuan2023SignUpWnd, "TypeInfo", "TypeInfo", UILabel)
RegistChildComponent(LuaMengQuan2023SignUpWnd, "RequireLevelInfo", "RequireLevelInfo", UILabel)
RegistChildComponent(LuaMengQuan2023SignUpWnd, "TaskDescribeInfo", "TaskDescribeInfo", UILabel)
RegistChildComponent(LuaMengQuan2023SignUpWnd, "Item1", "Item1", GameObject)
RegistChildComponent(LuaMengQuan2023SignUpWnd, "Item2", "Item2", GameObject)
RegistChildComponent(LuaMengQuan2023SignUpWnd, "Item3", "Item3", GameObject)
RegistChildComponent(LuaMengQuan2023SignUpWnd, "Item4", "Item4", GameObject)
RegistChildComponent(LuaMengQuan2023SignUpWnd, "EnterButton", "EnterButton", GameObject)
RegistChildComponent(LuaMengQuan2023SignUpWnd, "MatchingMark", "MatchingMark", UILabel)
RegistChildComponent(LuaMengQuan2023SignUpWnd, "EnterButtonLabel", "EnterButtonLabel", UILabel)

--@endregion RegistChildComponent end

function LuaMengQuan2023SignUpWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end

    self:InitWndData()
    self:RefreshConstUI()
    self:InitUIEvent()
    self:InitSendMengQuanHengXingPlayTimes()
end

function LuaMengQuan2023SignUpWnd:Init()
    g_ScriptEvent:BroadcastInLua("Double11MQHX_OpenSignUpWnd")
end

--@region UIEvent

--@endregion UIEvent


function LuaMengQuan2023SignUpWnd:InitWndData()
    self.MQHXConfigData = Double11_MQLD.GetData()
    self.gameplayId = self.MQHXConfigData.GamePlayId
    self.joinRewardLimit = self.MQHXConfigData.DailyAwardInfo[0]
    self.isMatching = false

    if self.gameplayId and CClientMainPlayer.Inst then
        Gac2Gas.GlobalMatch_RequestCheckSignUp(self.gameplayId, CClientMainPlayer.Inst.Id)
    end
end

function LuaMengQuan2023SignUpWnd:RefreshConstUI()
    self.TaskDescribeInfo.text = LocalString.GetString(self.MQHXConfigData.MQHXTaskDisc)
    self.TimeInfo.text = CQnSymbolParser.ConvertQnTextToNGUIText(LocalString.GetString(self.MQHXConfigData.MQHXTime))
    self.RequireLevelInfo.text = SafeStringFormat3(LocalString.GetString("%s级"), tostring(self.MQHXConfigData.LevelLimit))
    self.TypeInfo.text = self.MQHXConfigData.MQHXTaskType
    
    self:InitOneItem(self.Item1.gameObject, self.MQHXConfigData.TopRankRewardItemId)
    self:InitOneItem(self.Item2.gameObject, self.MQHXConfigData.ScoreRewardItemId)
    self:InitOneItem(self.Item3.gameObject, self.MQHXConfigData.JoinRewardItemId[1])
    self:InitOneItem(self.Item4.gameObject, self.MQHXConfigData.JoinRewardItemId[0])
end

function LuaMengQuan2023SignUpWnd:InitUIEvent()
    UIEventListener.Get(self.EnterButton).onClick = DelegateFactory.VoidDelegate(function (_)
        if self.isMatching then
            Gac2Gas.GlobalMatch_RequestCancelSignUp(self.gameplayId)
        else
            Gac2Gas.GlobalMatch_RequestSignUp(self.gameplayId)
        end
    end)

    UIEventListener.Get(self.RuleButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        g_MessageMgr:ShowMessage("Double11_MengQuanHengXing_Tips")
    end)

    UIEventListener.Get(self.RankButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        Gac2Gas.QueryRank(self.MQHXConfigData.RankId)
    end)
end

function LuaMengQuan2023SignUpWnd:OnRankDataReady()
    if CLuaRankData.m_CurRankId ~= self.MQHXConfigData.RankId then return end
    CUIManager.ShowUI(CLuaUIResources.MengQuan2023RankWnd)
end

function LuaMengQuan2023SignUpWnd:OnEnable()
    g_ScriptEvent:AddListener("GlobalMatch_CheckInMatchingResult", self, "OnGlobalMatch_CheckInMatchingResultWithInfo")
    g_ScriptEvent:AddListener("GlobalMatch_SignUpPlayResult", self, "OnGlobalMatch_SignUpPlayResult")
    g_ScriptEvent:AddListener("GlobalMatch_CancelSignUpResult", self, "OnGlobalMatch_CancelSignUpResult")
    g_ScriptEvent:AddListener("SyncMengQuanHengXingRankInfo", self, "OnCloseSignUpWnd")
    g_ScriptEvent:AddListener("OnRankDataReady", self, "OnRankDataReady")

end

function LuaMengQuan2023SignUpWnd:OnDisable()
    g_ScriptEvent:RemoveListener("GlobalMatch_CheckInMatchingResult", self, "OnGlobalMatch_CheckInMatchingResultWithInfo")
    g_ScriptEvent:RemoveListener("GlobalMatch_SignUpPlayResult", self, "OnGlobalMatch_SignUpPlayResult")
    g_ScriptEvent:RemoveListener("GlobalMatch_CancelSignUpResult", self, "OnGlobalMatch_CancelSignUpResult")
    g_ScriptEvent:RemoveListener("SyncMengQuanHengXingRankInfo", self, "OnCloseSignUpWnd")
    g_ScriptEvent:RemoveListener("OnRankDataReady", self, "OnRankDataReady")

end

function LuaMengQuan2023SignUpWnd:InitOneItem(curItem, itemID)
    local texture = curItem.transform:Find("Item"):GetComponent(typeof(CUITexture))
    local ItemData = Item_Item.GetData(itemID)
    if ItemData then texture:LoadMaterial(ItemData.Icon) end
    UIEventListener.Get(curItem).onClick = DelegateFactory.VoidDelegate(function (_)
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemID, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
    end)
end

function LuaMengQuan2023SignUpWnd:InitSendMengQuanHengXingPlayTimes()
    local joinPlayTimes = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eDouble11MQLDDailyAwardTimes) or 0
    local remainRewardTimes = self.joinRewardLimit - joinPlayTimes
    if remainRewardTimes > 0 then
        self.PlayRewardTime.text = CUICommonDef.TranslateToNGUIText("[c][8B3B11]" .. remainRewardTimes .. LocalString.GetString("/") .. self.joinRewardLimit .. "[-][/c]")
    else
        self.PlayRewardTime.text = CUICommonDef.TranslateToNGUIText("[c][ff5050]" .. remainRewardTimes .. LocalString.GetString("[-][/c][c][8B3B11]/") .. self.joinRewardLimit .. "[-][/c]")
    end
end

function LuaMengQuan2023SignUpWnd:OnState(isMatching)
    self.isMatching = isMatching
    self.MatchingMark.gameObject:SetActive(isMatching)
    self.EnterButton.transform:Find("vfx_PlayButton").gameObject:SetActive(not isMatching)
    
    if not isMatching then
        self.EnterButtonLabel.text = LocalString.GetString("报名匹配")
        --baoyuduoyu2023resultwnd_anniu
        self.EnterButton.transform:GetComponent(typeof(CUITexture)):LoadMaterial("UI/Texture/FestivalActivity/Festival_Double11/Shuangshiyi2023/Material/baoyuduoyu2023resultwnd_anniu.mat")
    else
        self.EnterButtonLabel.text = LocalString.GetString("取消匹配")
        --mengquan2023resultwnd_anniu_02
        self.EnterButton.transform:GetComponent(typeof(CUITexture)):LoadMaterial("UI/Texture/FestivalActivity/Festival_Double11/Shuangshiyi2023/Material/mengquan2023resultwnd_anniu_02.mat")
    end
end

function LuaMengQuan2023SignUpWnd:OnGlobalMatch_CheckInMatchingResultWithInfo(playerId, playId, isInMatching, resultStr)
    if CClientMainPlayer.Inst == nil or playerId ~= CClientMainPlayer.Inst.Id or playId ~= self.gameplayId then
        return
    end

    self:OnState(isInMatching)
end

function LuaMengQuan2023SignUpWnd:OnGlobalMatch_SignUpPlayResult(playId, success)
    if success and playId == self.gameplayId then
        self:OnState(true)
    end
end

function LuaMengQuan2023SignUpWnd:OnGlobalMatch_CancelSignUpResult(playId, success)
    if success and playId == self.gameplayId then
        self:OnState(false)
    end
end

function LuaMengQuan2023SignUpWnd:OnCloseSignUpWnd()
    CUIManager.CloseUI(CLuaUIResources.MengQuan2023SignUpWnd)
    CUIManager.CloseUI(CLuaUIResources.Shuangshiyi2023MainWnd)
end 