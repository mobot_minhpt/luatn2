-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CommonDefs = import "L10.Game.CommonDefs"
local CSnowBallMgr = import "L10.Game.CSnowBallMgr"
local CSnowBallResultWnd = import "L10.UI.CSnowBallResultWnd"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUITexture = import "L10.UI.CUITexture"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local XueQiuDaZhan_FinalMsg = import "L10.Game.XueQiuDaZhan_FinalMsg"
local UITexture = import "UITexture"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local Camera = import "UnityEngine.Camera"
local Extensions = import "Extensions"

CSnowBallResultWnd.m_Init_CS2LuaHook = function (this)

    this.iconNode:SetActive(false)

    this.fxNode:LoadFx("Fx/UI/Prefab/UI_xueqiudazuozhan.prefab")

    this.rankLabel.text = tostring(CSnowBallMgr.Inst.result_rank)
    this.executeLabel.text = tostring(CSnowBallMgr.Inst.result_executeNum)
    this.scoreLabel.text = tostring(CSnowBallMgr.Inst.result_score)

    this:SetRankTipText(CSnowBallMgr.Inst.result_rank)

    UIEventListener.Get(this.backBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        this:Close()
    end)

    UIEventListener.Get(this.shareBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        --OpenSharePanel();
        this:OnLocalSaveButtonClick()
    end)

    local teammateInfoNode = this.transform:Find("Anchor/info/teammateInfoNode").gameObject
    local singleFxNode = this.transform:Find("Anchor/info/singleShow").gameObject
    local doubleFxNode = this.transform:Find("Anchor/info/doubleShow").gameObject
    local titleNode = CommonDefs.GetComponent_Component_Type(this.transform:Find("Anchor/info/title"), typeof(UILabel))

    if CClientMainPlayer.Inst ~= nil then
        --playerInfoNode.SetActive(true);
        --iconNode.SetActive(true);

        CommonDefs.GetComponent_Component_Type(this.playerInfoNode.transform:Find("name"), typeof(UILabel)).text = CClientMainPlayer.Inst.Name
        CommonDefs.GetComponent_Component_Type(this.playerInfoNode.transform:Find("score"), typeof(UILabel)).text = tostring(CSnowBallMgr.Inst.result_score)
        CommonDefs.GetComponent_Component_Type(this.playerInfoNode.transform:Find("desnum"), typeof(UILabel)).text = tostring(CSnowBallMgr.Inst.result_executeNum)
        CommonDefs.GetComponent_Component_Type(this.playerInfoNode.transform:Find("icon"), typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(CClientMainPlayer.Inst.Class, CClientMainPlayer.Inst.Gender, -1), false)
        CommonDefs.GetComponent_Component_Type(this.playerInfoNode.transform:Find("icon/lv"), typeof(UILabel)).text = tostring(CClientMainPlayer.Inst.Level)
        LuaSnowBallResultWnd:Init()
        if CSnowBallMgr.Inst.showResultPanelType == 2 then
            teammateInfoNode:SetActive(true)
            CommonDefs.GetComponent_Component_Type(teammateInfoNode.transform:Find("name"), typeof(UILabel)).text = CSnowBallMgr.Inst.result_teammate_name
            CommonDefs.GetComponent_Component_Type(teammateInfoNode.transform:Find("score"), typeof(UILabel)).text = tostring(CSnowBallMgr.Inst.result_teammate_score)
            CommonDefs.GetComponent_Component_Type(teammateInfoNode.transform:Find("desnum"), typeof(UILabel)).text = tostring(CSnowBallMgr.Inst.result_teammate_executeNum)
            CommonDefs.GetComponent_Component_Type(teammateInfoNode.transform:Find("icon"), typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(CSnowBallMgr.Inst.result_teammate_class, CSnowBallMgr.Inst.result_teammate_gender, -1), false)
            CommonDefs.GetComponent_Component_Type(teammateInfoNode.transform:Find("icon/lv"), typeof(UILabel)).text = tostring(CSnowBallMgr.Inst.result_teammate_lv)
            singleFxNode:SetActive(false)
            doubleFxNode:SetActive(true)
            LuaSnowBallResultWnd:ReloadModel(doubleFxNode.transform:Find("texiao1").gameObject,"Assets/Res/Fx/UI/Prefab/UI_xueqiudazuozhan_nanxueren.prefab","_CSnowBallResultWnd_1_")
            LuaSnowBallResultWnd:ReloadModel(doubleFxNode.transform:Find("texiao2").gameObject,"Assets/Res/Fx/UI/Prefab/UI_xueqiudazuozhan_nvxueren.prefab","_CSnowBallResultWnd_2_")

            titleNode.text = LocalString.GetString("雪球大战双人赛")
        else
            teammateInfoNode:SetActive(false)
            singleFxNode:SetActive(true)
            if CClientMainPlayer.Inst.Gender == 0 then
                LuaSnowBallResultWnd:ReloadModel(singleFxNode.transform:Find("texiao").gameObject,"Assets/Res/Fx/UI/Prefab/UI_xueqiudazuozhan_nanxueren.prefab","_CSnowBallResultWnd_0_")
            else
                LuaSnowBallResultWnd:ReloadModel(singleFxNode.transform:Find("texiao").gameObject,"Assets/Res/Fx/UI/Prefab/UI_xueqiudazuozhan_nvxueren.prefab","_CSnowBallResultWnd_0_")
            end

            doubleFxNode:SetActive(false)
            titleNode.text = LocalString.GetString("雪球大战单人赛")
        end
    end
end
CSnowBallResultWnd.m_SetRankTipText_CS2LuaHook = function (this, rank)
    local count = XueQiuDaZhan_FinalMsg.GetDataCount()
    do
        local i = 1
        while i <= count do
            local info = XueQiuDaZhan_FinalMsg.GetData(i)
            if info ~= nil then
                if info.Type == 1 then
                    CommonDefs.ListAdd(this.firstTipList, typeof(String), info.Content)
                elseif info.Type == 2 then
                    CommonDefs.ListAdd(this.secondTipList, typeof(String), info.Content)
                else
                    CommonDefs.ListAdd(this.otherTipList, typeof(String), info.Content)
                end
            end
            i = i + 1
        end
    end
    local tipString = ""
    if rank == 1 then
        tipString = this.firstTipList[UnityEngine_Random(0, this.firstTipList.Count)]
    elseif rank >= 2 and rank <= 5 then
        tipString = this.secondTipList[UnityEngine_Random(0, this.secondTipList.Count)]
    else
        tipString = this.otherTipList[UnityEngine_Random(0, this.otherTipList.Count)]
    end
    this.tipLabel.text = tipString
end
CSnowBallResultWnd.m_OnLocalSaveButtonClick_CS2LuaHook = function (this)
    CUICommonDef.CaptureScreenAndShare()
end
CSnowBallResultWnd.m_OnDestroy_CS2LuaHook = function (this)
    LuaSnowBallResultWnd:OnDestroy()
end

LuaSnowBallResultWnd = {}
LuaSnowBallResultWnd.m_MainTexture = nil
LuaSnowBallResultWnd.m_ModelTextureLoader = nil
LuaSnowBallResultWnd.m_ModelNames = {}
function LuaSnowBallResultWnd:Init()
    self.m_ModelNames = {}
end
function LuaSnowBallResultWnd:LoadModel(ro, prefabname, modelname)
    ro:LoadMain(prefabname, nil, false, false)
    if CUIManager.instance then
        local t = CUIManager.instance.transform:Find(modelname)
        if t then
            local camera = t:GetComponent(typeof(Camera))
            if camera then
                camera.orthographic = true
                camera.orthographicSize = 16
                Extensions.SetLocalPositionY(camera.transform, #self.m_ModelNames * 40)
            end
        end
    end
end
function LuaSnowBallResultWnd:ReloadModel(go, prefabname, modelname)
    self.m_MainTexture = go:GetComponent(typeof(UITexture))
    if not self.m_MainTexture then return end
    self.m_ModelTextureLoader = LuaDefaultModelTextureLoader.Create(function (ro)
        self:LoadModel(ro, prefabname, modelname)
    end)
    table.insert(self.m_ModelNames, modelname)
    self.m_MainTexture.mainTexture = CUIManager.CreateModelTexture(modelname, self.m_ModelTextureLoader,0,0,-16.5,11.4,false,true,0.1,true)
end
function LuaSnowBallResultWnd:OnDestroy()
    for _,name in pairs(self.m_ModelNames) do
        CUIManager.DestroyModelTexture(name)
    end
    self.m_ModelNames = {}
end
