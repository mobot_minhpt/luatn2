local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local CButton = import "L10.UI.CButton"
local UIGrid = import "UIGrid"
local GameObject = import "UnityEngine.GameObject"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CHongBaoSettings = import "L10.UI.CHongBaoSettings"
LuaHongBaoCoverInfoWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaHongBaoCoverInfoWnd, "NameLabel", "NameLabel", UILabel)
RegistChildComponent(LuaHongBaoCoverInfoWnd, "TimeLabel", "TimeLabel", UILabel)
RegistChildComponent(LuaHongBaoCoverInfoWnd, "DescribeLabel", "DescribeLabel", UILabel)
RegistChildComponent(LuaHongBaoCoverInfoWnd, "SetBtn", "SetBtn", CButton)
RegistChildComponent(LuaHongBaoCoverInfoWnd, "HongBaoItemGrid", "HongBaoItemGrid", UIGrid)
RegistChildComponent(LuaHongBaoCoverInfoWnd, "HongBaoCoverItem", "HongBaoCoverItem", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaHongBaoCoverInfoWnd, "m_HongBaoCoverId")
function LuaHongBaoCoverInfoWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.SetBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSetBtnClick()
	end)


    --@endregion EventBind end
end

function LuaHongBaoCoverInfoWnd:Init()
    local hongbaoData = CLuaHongBaoMgr.m_CurShowHongBaoCoverData
    local data = HongBao_Cover.GetData(hongbaoData.id)
    self.m_HongBaoCoverId = hongbaoData.id
    self.SetBtn.gameObject:SetActive(hongbaoData.canUse)
    self.HongBaoCoverItem:SetActive(false)
    self:InitHongBaoTitleInfo(data)
    self:InitHongBaoItemInfo(data)
end
function LuaHongBaoCoverInfoWnd:InitHongBaoTitleInfo(data)
    self.DescribeLabel.text = data.Description
    self.NameLabel.text = data.Name
    local endTimestamp = CServerTimeMgr.Inst:GetTimeStampByStr(data.EndTime)
    local nowTimeStamp = CServerTimeMgr.Inst.timeStamp
    if endTimestamp == 0 then   
        self.TimeLabel.text = LocalString.GetString("[00ff60]永久有效[-]")
    elseif endTimestamp <= nowTimeStamp then
        self.TimeLabel.text = LocalString.GetString("[ff5050]已失效[-]")
    else
        self.TimeLabel.text = SafeStringFormat3(LocalString.GetString("[ffff00]有效期至 %s[-]"),data.EndTime)
    end
end

function LuaHongBaoCoverInfoWnd:InitHongBaoItemInfo(data)
    Extensions.RemoveAllChildren(self.HongBaoItemGrid.transform)
    local lingyuList = HongBao_Setting.GetData().Show_Different_LingYu
    for i = 1,data.TypeNum do
        local hongbaoItem = CUICommonDef.AddChild(self.HongBaoItemGrid.gameObject,self.HongBaoCoverItem)
        hongbaoItem:SetActive(true)
        local HongbaoTex = hongbaoItem.transform:Find("HongBaoCoverTex").gameObject:GetComponent(typeof(CUITexture))
        local ResourcePath = "UI/Texture/Transparent/Material/"..string.gsub(data.DifferentResource,"{TypeNum}",tostring(i))..".mat"
        HongbaoTex:LoadMaterial(ResourcePath)

        local minNum = 0
        local maxNum = lingyuList[lingyuList.Length - 1]
        if i - 2 >= 0 and i - 2 < lingyuList.Length then minNum = lingyuList[i-2] end
        if i - 1 < lingyuList.Length and i - 1 >= 0 then maxNum = lingyuList[i-1] - 1 end
        if i - 1 == lingyuList.Length - 1 then maxNum = CHongBaoSettings.Value_Max_World end
        if i == 1 then minNum = CHongBaoSettings.Value_Min_World end
        hongbaoItem.transform:Find("LingYuLabel").gameObject:GetComponent(typeof(UILabel)).text = SafeStringFormat3("#285 %d ~ %d" ,minNum , maxNum )
    end
    self.HongBaoItemGrid:Reposition()
end

--@region UIEvent

function LuaHongBaoCoverInfoWnd:OnSetBtnClick()
    CLuaHongBaoMgr.SetCurHongBaoCover(self.m_HongBaoCoverId)

end

function LuaHongBaoCoverInfoWnd:OnSetCurCoverType(beforeId,curId)
    if self.m_HongBaoCoverId ~= curId then return end
    local message = g_MessageMgr:FormatMessage("HongBaoCover_SetSuccess")
    MessageWndManager.ShowOKMessage(message, DelegateFactory.Action(function ()
        CUIManager.CloseUI(CLuaUIResources.HongBaoCoverInfoWnd)
    end))
end

function LuaHongBaoCoverInfoWnd:OnEnable()
    g_ScriptEvent:AddListener("SetCurHongBaoCover",self,"OnSetCurCoverType")
end
function LuaHongBaoCoverInfoWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SetCurHongBaoCover",self,"OnSetCurCoverType")
end
--@endregion UIEvent

