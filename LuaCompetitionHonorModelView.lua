local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local Profession = import "L10.Game.Profession"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local EnumClass = import "L10.Game.EnumClass"
local EnumGender = import "L10.Game.EnumGender"
local Screen = import "UnityEngine.Screen"
local CPropertyAppearance = import "L10.Game.CPropertyAppearance"
local UITable = import "UITable"

LuaCompetitionHonorModelView = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaCompetitionHonorModelView, "ModelTexture", CUITexture)
RegistChildComponent(LuaCompetitionHonorModelView, "RankStamp", CUITexture)
RegistChildComponent(LuaCompetitionHonorModelView, "RankLabel", UILabel)
RegistChildComponent(LuaCompetitionHonorModelView, "ClassSprite", UISprite)
RegistChildComponent(LuaCompetitionHonorModelView, "NameLabel", UILabel)
RegistChildComponent(LuaCompetitionHonorModelView, "ServerLabel", UILabel)
RegistChildComponent(LuaCompetitionHonorModelView, "CheckBtn", GameObject)

RegistClassMember(LuaCompetitionHonorModelView, "InfoTable")
RegistClassMember(LuaCompetitionHonorModelView, "m_PlayerInfo")
RegistClassMember(LuaCompetitionHonorModelView, "m_IdentifierStr")
RegistClassMember(LuaCompetitionHonorModelView, "m_ModelRO")
--@endregion RegistChildComponent end

function LuaCompetitionHonorModelView:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    local infoTable = self.transform:Find("ModelInfo/InfoTable")
    if infoTable then self.InfoTable = infoTable:GetComponent(typeof(UITable)) end
    UIEventListener.Get(self.CheckBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnCheckBtnClick()
    end)
end

function LuaCompetitionHonorModelView:UpdateModel(playerId, class, gender, name, server, rank, appearanceProp, pos)
    self.m_PlayerInfo = {}
    self.m_PlayerInfo.playerId = playerId
    self.m_PlayerInfo.name = name
    self.CheckBtn:SetActive(false)
    
    self:SetRoModel(playerId, class, gender, appearanceProp, pos)
    -- 排名
    self.RankStamp.gameObject:SetActive(rank and rank ~= -1)
    if rank and rank ~= -1 then
        local rankIconIndex = rank
        if rankIconIndex >= 4 and rankIconIndex <= 10 then
            rankIconIndex = 4
        elseif rankIconIndex >= 11 then
            rankIconIndex = 5
        end
        self.RankStamp:LoadMaterial(SafeStringFormat3("UI/Texture/Transparent/Material/competitionhonorfamehallwnd_icon_ranking_%d.mat", rankIconIndex))
        self.RankLabel.gameObject:SetActive(rankIconIndex > 3)
        self.RankLabel.text = rank
    end
    -- 信息
    self.ClassSprite.spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), class))
    self.NameLabel.text = name
    self.ServerLabel.text = server
    self.ClassSprite:ResetAndUpdateAnchors()
    if self.InfoTable then self.InfoTable:Reposition() end
end

function LuaCompetitionHonorModelView:SetRoModel(playerId, class, gender, appearanceProp, pos)
    if self.m_ModelRO == nil then
        self.m_IdentifierStr = self.gameObject:GetInstanceID()
        local modelTextureLoader = LuaDefaultModelTextureLoader.Create(function(ro)
            self.m_ModelRO = ro
            self:ResetModel(playerId, class, gender, appearanceProp)
            self.m_CurrentRotation = 180
            UIEventListener.Get(self.ModelTexture.gameObject).onDrag = DelegateFactory.VectorDelegate(function(go, delta)
                self:OnModelDrag(delta)
            end)
        end)
        self.ModelTexture.mainTexture = CUIManager.CreateModelTexture(self.m_IdentifierStr, modelTextureLoader, 180, pos.x, pos.y, pos.z, false, true, 1, true, false)
    else
        self:ResetModel(playerId, class, gender, appearanceProp)
    end
end

function LuaCompetitionHonorModelView:ResetModel(playerId, class, gender, appearanceProp)
    local fakeAppearance = CreateFromClass(CPropertyAppearance)
    if playerId == CClientMainPlayer.Inst.Id and appearanceProp == nil then
        fakeAppearance = CClientMainPlayer.Inst.AppearanceProp
    elseif appearanceProp ~= nil then
        fakeAppearance:LoadFromString(appearanceProp, CommonDefs.ConvertIntToEnum(typeof(EnumClass), class), CommonDefs.ConvertIntToEnum(typeof(EnumGender), gender))
    else
        return
    end
    if CommonDefs.ConvertIntToEnum(typeof(EnumClass), class) == EnumClass.YingLing then
        fakeAppearance.YingLingState = gender == 0 and EnumYingLingState.eMale or EnumYingLingState.eFemale
    end
    CClientMainPlayer.LoadResource(self.m_ModelRO, fakeAppearance, true, 1.0, 0, false, 0, false, 0, false, nil, false)
    CUIManager.SetModelRotation(self.m_IdentifierStr, 180)
    self.m_CurrentRotation = 180
end

--@region UIEvent

function LuaCompetitionHonorModelView:OnCheckBtnClick()
    CPlayerInfoMgr.ShowPlayerInfoWnd(self.m_PlayerInfo.playerId, self.m_PlayerInfo.name)
end

function LuaCompetitionHonorModelView:OnModelDrag(delta)
    self.m_CurrentRotation = self.m_CurrentRotation - delta.x/Screen.width*360
    CUIManager.SetModelRotation(self.m_IdentifierStr, self.m_CurrentRotation)
end
--@endregion UIEvent

function LuaCompetitionHonorModelView:OnDisable()
	CUIManager.DestroyModelTexture(self.m_IdentifierStr)
    self.m_ModelRO = nil
end