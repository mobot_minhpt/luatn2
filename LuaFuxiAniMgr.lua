local StringBuilder         = import "System.Text.StringBuilder"
local Object                = import "System.Object"
local String                = import "System.String"
local Main                  = import "L10.Engine.Main"
local HTTPHelper            = import "L10.Game.HTTPHelper"
local CClientMainPlayer     = import "L10.Game.CClientMainPlayer"
local CServerTimeMgr        = import "L10.Game.CServerTimeMgr"
local CommonDefs            = import "L10.Game.CommonDefs"
local CPersonalSpaceMgr     = import "L10.Game.CPersonalSpaceMgr"
local Json                  = import "L10.Game.Utils.Json"
local CWebBrowserMgr        = import "L10.Game.CWebBrowserMgr"
local CItemConsumeWndMgr    = import "L10.UI.CItemConsumeWndMgr"
local CItemAccessListMgr    = import "L10.UI.CItemAccessListMgr"
local CTooltip              = import "L10.UI.CTooltip"
local DelegateFactory       = import "DelegateFactory"
local CustomPropertySerializable    = import "L10.Game.Properties.CustomPropertySerializable"
local QnMoneyCostMesssageMgr        = import "L10.UI.QnMoneyCostMesssageMgr"
local EnumPlayScoreKey      = import "L10.Game.EnumPlayScoreKey"
local EnumMoneyType         = import "L10.Game.EnumMoneyType"

EnumFuxiDanceMotionStatus ={
    eUnknow = 0,
    eChecking = 1,
    eCheckSuccess = 2,
    eCheckFaild = 3,
    eExpired = 4,
    eDeleted = 5
}

LuaFuxiAniMgr = class()

if CommonDefs.IS_CN_CLIENT then
    LuaFuxiAniMgr.IsOpen = true
else 
    LuaFuxiAniMgr.IsOpen = false
end

--@region 脏数据通知

function LuaFuxiAniMgr.ReqSyncNew(motionid)
    Gac2Gas.FindNewFuXiDanceMotionId(motionid)
end

function LuaFuxiAniMgr.ReqSyncStatus(index,motionid,newstatus)
    Gac2Gas.FuXiDanceMotionStatusChange(index,motionid,newstatus)
end

--@endregion

--@region 分享相关

LuaFuxiAniMgr.ShareMotionID = nil

function LuaFuxiAniMgr.OpenFromShare(motionid)
    if LuaFuxiAniMgr.IsFuxiLock() then
        local msg = g_MessageMgr:FormatMessage("ALREADY_NOT_UNLOCK_FUXIDANCE_TIPS")
        local okfunc = function()
            LuaFuxiAniMgr.ShowItemConsumeWnd()
        end
        g_MessageMgr:ShowOkCancelMessage(msg,okfunc,nil, nil, nil, false)
        return
    end
    LuaFuxiAniMgr.ShareMotionID = motionid
    CUIManager.ShowUI(CLuaUIResources.FuxiAniPreviewWnd)
end

--[[
    @desc: 请求分享链接
    author:Codee
    time:2021-04-25 17:11:38
    --@typeIndex:
	--@motionid: 
    @return:
]]
function LuaFuxiAniMgr.ShareMotionTo(typeIndex,motionid,parma)
    Gac2Gas.ShareFuXiDanceMotion(motionid,typeIndex,parma)
end

--@endregion

--@region 解锁相关

function LuaFuxiAniMgr.IsFuxiLock()
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp and CClientMainPlayer.Inst.PlayProp.InitFlag then
        local unlock = CClientMainPlayer.Inst.PlayProp.InitFlag:GetBit(2)
        return not unlock
    else 
        return true
    end
end

function LuaFuxiAniMgr.UnlockFuxiSuccess()
    g_MessageMgr:ShowMessage("UNLOCK_FUXIDANCE_SUCCESS")
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp and CClientMainPlayer.Inst.PlayProp.InitFlag then
        CClientMainPlayer.Inst.PlayProp.InitFlag:SetBit(2,true)
        g_ScriptEvent:BroadcastInLua("OnFuxiUnlock")
    end
end

--@endregion

--@region 玩家游戏数据相关

LuaFuxiAniMgr.FuxiData = nil

--[[
    @desc:初始化玩家拥有的伏羲动作数据 
    author:Codee
    time:2021-04-22 10:00:16
    @return:
]]
function LuaFuxiAniMgr.InitFuxiData()
    if CClientMainPlayer.Inst == nil then return end
    if CClientMainPlayer.Inst.PlayProp == nil then return end
    
    local datas = {}
    local dic = CClientMainPlayer.Inst.PlayProp.FuXiDanceData--dic
    if dic ~= nil then
        CommonDefs.DictIterate(dic, DelegateFactory.Action_object_object(function(key,val)
            local data = {
                motionName = val.Name.StringData,
                motionId = val.Id,
                status = val.Status,
                isMine = val.IsMine == 1,
                expire = val.ExpireTime,
                outtime = val.ExpireTime > 0 and val.ExpireTime <= CServerTimeMgr.Inst.timeStamp,
                index = key,
                downloadNum = -1, --用于区分是否初始化网络数据 
                roleId = 0,
                roleName= "",
            }
            if data.motionId > 0 then
                table.insert(datas,1,data)
            end
        end))

        LuaFuxiAniMgr.FuxiData = datas
        g_ScriptEvent:BroadcastInLua("OnGetFuxiPlayerOwnData")
    end
end

function LuaFuxiAniMgr.GetAvaliableFuxiExpressionData()
    LuaFuxiAniMgr.InitFuxiData()
    local datas = LuaFuxiAniMgr.FuxiData
    local tmp = {}
    if datas then
        for __, data in pairs(datas) do
            if data.motionId~=0 and (data.status == 0 or data.status == 2) and not data.outtime then
                table.insert(tmp, data)
            end
        end
    end
    return tmp
end

--[[
    @desc: 回调，上传添加空数据
    author:Codee
    time:2021-04-27 21:06:23
    --@index: 
    @return:
]]
function LuaFuxiAniMgr.AddEmptyEmotion(index)
    if CClientMainPlayer.Inst == nil then return end
    if CClientMainPlayer.Inst.PlayProp == nil then return end
    local dic = CClientMainPlayer.Inst.PlayProp.FuXiDanceData
    local data = CustomPropertySerializable("CFuXiDanceMotionInfo")
    data.Id = 0
    CommonDefs.DictSet_LuaCall(dic,index,data)
    LuaFuxiAniMgr.InitFuxiData()
end


--[[
    @desc: 回调，删除成功
    author:Codee
    time:2021-04-27 14:42:01
    --@index:
	--@motionid: 
    @return:
]]
function LuaFuxiAniMgr.DeleteFuXiDanceMotion(index,motionid)
    if CClientMainPlayer.Inst == nil then return end
    if CClientMainPlayer.Inst.PlayProp == nil then return end
    local dic = CClientMainPlayer.Inst.PlayProp.FuXiDanceData
    CommonDefs.DictRemove_LuaCall(dic,index)
    LuaFuxiAniMgr.InitFuxiData()
end

--[[
    @desc: 回调，重命名成功
    author:Codee
    time:2021-04-27 21:06:51
    --@index:
	--@motionId:
	--@newName: 
    @return:
]]
function LuaFuxiAniMgr.RenameFuXiDanceMotion(index, motionId, newName)
    if CClientMainPlayer.Inst == nil then return end
    if CClientMainPlayer.Inst.PlayProp == nil then return end
    local dic = CClientMainPlayer.Inst.PlayProp.FuXiDanceData
    local data = CommonDefs.DictGetValue_LuaCall(dic,index)

    local bytes = CommonDefs.UTF8Encoding_GetBytes(newName)
	local bufLen = bytes.Length
    local startIndex = 0
    data.Name:LoadFromBytes(bytes,bufLen,bufLen,startIndex)
    LuaFuxiAniMgr.InitFuxiData()
end

--[[
    @desc:回调，更新玩家的数据
    author:Codee
    time:2021-04-27 14:37:24
    --@reason:
        eNone = 0,           -- 单纯同步数据
	    eCollectSuccess = 1, -- 收藏成功
	    eFindNewMotion = 2,  -- 发现了自己新上传的动作
	    eStatusChange = 3,   -- 自己的动作状态发生了变化
	--@index:
	--@datas: 
    @return:
]]
function LuaFuxiAniMgr.UpdateFuXiDanceData(reason,index,datas)
    if CClientMainPlayer.Inst == nil then return end
    if CClientMainPlayer.Inst.PlayProp == nil then return end
    local dic = CClientMainPlayer.Inst.PlayProp.FuXiDanceData
    local data = CommonDefs.DictGetValue_LuaCall(dic,index)

    if data == nil then
        data = CustomPropertySerializable("CFuXiDanceMotionInfo")
    end
    data:LoadFromString(datas)
    CommonDefs.DictSet_LuaCall(dic,index,data)
    LuaFuxiAniMgr.InitFuxiData()

    if reason == 1 then 
        g_MessageMgr:ShowMessage("FUXIDANCE_DOWNLOAD_SUCCESS_NOTICE")
    end
end

--@endregion

--@region UI相关

--[[
    @desc: 显示道具解锁界面
    author:Codee
    time:2021-04-20 15:52:36
    @return:
]]
function LuaFuxiAniMgr.ShowItemConsumeWnd()
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.MaxLevel < 30 then
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("你的等级不足30级，不能解锁"))
        return
    end
    local setting = FuXiDance_Setting.GetData()
    local itemtemp = Item_Item.GetData(setting.UnlockFuxiDanceNeedItemID)
    CItemConsumeWndMgr.ShowItemConsumeWnd(
        setting.UnlockFuxiDanceNeedItemID, 
        setting.UnlockFuxiDanceNeedItemCount, 
        g_MessageMgr:FormatMessage("FUXIDANCE_UNLOCK_NEED_ITEM"), 
        LocalString.GetString("解锁"), 
        DelegateFactory.Action(function() 
            Gac2Gas.RequestUnLockFuXiDance() 
        end),
        DelegateFactory.Action(function() 
            CItemAccessListMgr.Inst:ShowItemAccessInfo(setting.UnlockFuxiDanceNeedItemID, true, nil, CTooltip.AlignType.Right)
        end),
        nil
    )
end

--[[
    @desc: 显示灵玉消耗界面
    author:Codee
    time:2021-04-23 16:53:37
    @return:
]]
function LuaFuxiAniMgr.ShowCostWnd()
    local jade = FuXiDance_Setting.GetData().AddNewFuxiDanceNeedJade
    local title = SafeStringFormat3(LocalString.GetString("上传视频生成动作需要%d灵玉"),jade) --g_MessageMgr:FormatMessage("BUY_DUOHUNFAN")
    QnMoneyCostMesssageMgr.ShowMoneyCostMessageBox(EnumMoneyType.LingYu, title, jade, DelegateFactory.Action(function ()
        Gac2Gas.SubJadeForFuXiDanceUpload()
    end), nil, false, true, EnumPlayScoreKey.NONE, false)
end

--@endregion

--@region Web相关

function LuaFuxiAniMgr.CreateSb(url)
    local sb = NewStringBuilderWraper(StringBuilder, url)
    CommonDefs.StringBuilder_Append_StringBuilder_UInt64(sb:Append("?roleId="), CClientMainPlayer.Inst.Id)
    CommonDefs.StringBuilder_Append_StringBuilder_UInt64(sb:Append("&jobId="), EnumToInt(CClientMainPlayer.Inst.Class))
    CommonDefs.StringBuilder_Append_StringBuilder_UInt32(sb:Append("&gender="), EnumToInt(CClientMainPlayer.Inst.Gender))

    local ctime = math.floor(CServerTimeMgr.Inst.timeStamp)
    sb:Append("&ctime="):Append(tostring(ctime))

    local salt = "5r3WHJAszptd7A"
    local str = CClientMainPlayer.Inst.Id..ctime..salt
    local hash = CPersonalSpaceMgr.GetHexMd5_Internal(str)
    sb:Append("&token="):Append(hash)
    return sb
end

--[[
    @desc: 请求玩家自己上传的动作数据
    author:Codee
    time:2021-04-21 16:04:42
    @return:
]]
function LuaFuxiAniMgr.ReqFuxiPlayerDataInfo()
    local url = ""
    if CommonDefs.IS_PUB_RELEASE then 
        url = "https://ssl.hi.163.com/file_mg/public/qnm/motion_retargetin/game/list/my"
    else
        url = "http://192.168.131.157:8081/file_mg/public/qnm/motion_retargetin/game/list/my"
    end

    local sb = LuaFuxiAniMgr.CreateSb(url)
    local rurl = ToStringWrap(sb)

    Main.Inst:StartCoroutine(HTTPHelper.GetUrl(rurl, DelegateFactory.Action_bool_string(function (success, ret) 
		if not success then return end
        local retDic = Json.Deserialize(ret)
        if CommonDefs.DictGetValue(retDic, typeof(String), "code") ~= 1 then return end
        
        local datas = {}
        local datalist = CommonDefs.DictGetValue(retDic,typeof(String),"data")
        local len = datalist.Count
        for i=0,len-1 do
            local data = {
                motionId = CommonDefs.DictGetValue(datalist[i],typeof(String),"motionId"),
                motionName = CommonDefs.DictGetValue(datalist[i],typeof(String),"motionName"),
                downloadNum = CommonDefs.DictGetValue(datalist[i],typeof(String),"downloadNum"),
                status = CommonDefs.DictGetValue(datalist[i],typeof(String),"status"),--1审核中，2审核通过，3审核不通过，4过期
                expire = CommonDefs.DictGetValue(datalist[i],typeof(String),"expire"),--过期的时间
                roleId = CClientMainPlayer.Inst.Id,
                roleName = CClientMainPlayer.Inst.Name,
                isMine = true,
            }
            table.insert(datas,data)
        end
        g_ScriptEvent:BroadcastInLua("OnGetFuxiPlayerDataInfo",datas)
    end)))
end

--[[
    @desc: 获取所有游戏动作列表
    author:Codee
    time:2021-04-21 15:57:55
    --@page:当前页码，默认1
	--@sortTag: 排序规则，0:hot/1:new，默认0:hot
    @return:
]]
function LuaFuxiAniMgr.ReqFuxiAllDataInfo(page,sortTag)
    local url = ""
    if CommonDefs.IS_PUB_RELEASE then 
        url = "https://ssl.hi.163.com/file_mg/public/qnm/motion_retargetin/game/list/all"
    else
        url = "http://192.168.131.157:8081/file_mg/public/qnm/motion_retargetin/game/list/all"
    end

    local sb = LuaFuxiAniMgr.CreateSb(url)

    if page == nil or page <= 0 then page = 1 end
    CommonDefs.StringBuilder_Append_StringBuilder_UInt32(sb:Append("&page="),page)
    CommonDefs.StringBuilder_Append_StringBuilder_UInt32(sb:Append("&pageSize="),10)
    sb:Append("&sort="..sortTag)
    local rurl = ToStringWrap(sb)
    Main.Inst:StartCoroutine(HTTPHelper.GetUrl(rurl, DelegateFactory.Action_bool_string(function (success, ret) 
		if not success then return end
        local res = LuaFuxiAniMgr.ParsePageDatas(ret)
        g_ScriptEvent:BroadcastInLua("OnGetFuxiAllDataInfo",res)
    end)))
end

function LuaFuxiAniMgr.ParsePageDatas(ret)
    local retDic = TypeAs(Json.Deserialize(ret), typeof(MakeGenericClass(Dictionary, String, Object)))
    if CommonDefs.DictGetValue(retDic, typeof(String), "code") ~= 1 then return end

    local respones = {}

    local datadic = CommonDefs.DictGetValue(retDic,typeof(String),"data")
    local datalist = TypeAs(CommonDefs.DictGetValue(datadic,typeof(String),"list"),typeof(MakeGenericClass(List, Object)))
    local datas = LuaFuxiAniMgr.ParseDatas(datalist)

    respones.totalPage = CommonDefs.DictGetValue(datadic,typeof(String),"totalPage")
    respones.page = CommonDefs.DictGetValue(datadic,typeof(String),"page")
    respones.pageSize = CommonDefs.DictGetValue(datadic,typeof(String),"pageSize")

    respones.datas = datas
    return respones
end

function LuaFuxiAniMgr.ParseDatas(datalist)
    local datas ={}
    local len = datalist.Count
    for i=0,len-1 do
        local data = {
            roleId = tonumber(CommonDefs.DictGetValue(datalist[i],typeof(String),"roleId")),
            roleName = CommonDefs.DictGetValue(datalist[i],typeof(String),"roleName"),
            motionId = CommonDefs.DictGetValue(datalist[i],typeof(String),"motionId"),
            motionName = CommonDefs.DictGetValue(datalist[i],typeof(String),"motionName"),
            downloadNum = CommonDefs.DictGetValue(datalist[i],typeof(String),"downloadNum"),
            expire = CommonDefs.DictGetValue(datalist[i],typeof(String),"expire"),
            status = EnumFuxiDanceMotionStatus.eCheckSuccess,
        }
        table.insert(datas,data)
    end
    return datas
end

function LuaFuxiAniMgr.ReqFuxiDataInfos(motionids)
    local url = ""
    if CommonDefs.IS_PUB_RELEASE then 
        url = "https://ssl.hi.163.com/file_mg/public/qnm/motion_retargetin/game/list/share"
    else
        url = "http://192.168.131.157:8081/file_mg/public/qnm/motion_retargetin/game/list/share"
    end
    local sb = LuaFuxiAniMgr.CreateSb(url)
    sb:Append("&motionIdArr=[")
    for i=1,#motionids do
        sb:Append(motionids[i])
        if i ~=#motionids then 
            sb:Append(",")
        else
            sb:Append("]")
        end
    end
    local rurl = ToStringWrap(sb)
    Main.Inst:StartCoroutine(HTTPHelper.GetUrl(rurl, DelegateFactory.Action_bool_string(function (success, ret) 
		if success then
            local retDic = Json.Deserialize(ret)
            if CommonDefs.DictGetValue(retDic, typeof(String), "code") ~= 1 then return end

            local datalist = TypeAs(CommonDefs.DictGetValue(retDic,typeof(String),"data"),typeof(MakeGenericClass(List, Object)))
            local datas = LuaFuxiAniMgr.ParseDatas(datalist)
            g_ScriptEvent:BroadcastInLua("OnGetFuxiAniSpeData",datas)
        end
    end)))
end

--[[
    @desc: 收藏请求
    author:Codee
    time:2021-04-22 09:06:44
    --@motionId: 
    @return:
]]
function LuaFuxiAniMgr.ReqDownload(motionId)
    local url = ""
    if CommonDefs.IS_PUB_RELEASE then 
        url = "https://ssl.hi.163.com/file_mg/public/qnm/motion_retargetin/game/download"
    else
        url = "http://192.168.131.157:8081/file_mg/public/qnm/motion_retargetin/game/download"
    end
    local sb = LuaFuxiAniMgr.CreateSb(url)
    CommonDefs.StringBuilder_Append_StringBuilder_UInt32(sb:Append("&motionId="),motionId)
    local rurl = ToStringWrap(sb)
    Main.Inst:StartCoroutine(HTTPHelper.GetUrl(rurl, DelegateFactory.Action_bool_string(function (success, ret) 
		if success then
            Gac2Gas.CollectFuXiDanceMotion(motionId)
        end
    end)))
end

--[[
    @desc: 打开上传页面
    author:Codee
    time:2021-04-25 15:47:55
    @return:
]]
function LuaFuxiAniMgr.OpenUploadWebPage()
    local activityName="2021motion_transfer"
    CWebBrowserMgr.Inst:OpenActivityUrl(activityName)
    CUIManager.CloseUI(CLuaUIResources.FuxiAniPreviewWnd)
end

--@endregion
