-- Auto Generated!!
local CJieBaiMgr = import "L10.Game.CJieBaiMgr"
local CJieBaiSelfWnd = import "L10.UI.CJieBaiSelfWnd"
local DelegateFactory = import "DelegateFactory"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local JieBai_Setting = import "L10.Game.JieBai_Setting"
local LocalString = import "LocalString"
local UIEventListener = import "UIEventListener"
local UIInput = import "UIInput"
CJieBaiSelfWnd.m_SubmitInfo_CS2LuaHook = function (this) 
    local nameInput = CommonDefs.GetComponent_GameObject_Type(this.NameInputNode, typeof(UIInput))
    local name = nameInput.value
    if not System.String.IsNullOrEmpty(name) then
        if CommonDefs.StringLength(name) > CJieBaiSelfWnd.MaxTitle1Length then
            g_MessageMgr:ShowMessage("CHUANJIABAO_NAME_LIMIT", LocalString.GetString("结拜个人称号"), CJieBaiSelfWnd.MaxTitle1Length)
            return
        end

        if this.MoneyCtrl.moneyEnough then
            if CJieBaiMgr.CheckJiebaiTitleName(this.NameLabel.text .. name) then
                Gac2Gas.SetJiebaiPersonalTitle(name)
                this:Close()
            end
        else
            g_MessageMgr:ShowMessage("NotEnough_Silver")
        end
    else
        g_MessageMgr:ShowMessage("PLAYERSHOP_NOT_NO_NAME")
    end
end
CJieBaiSelfWnd.m_Init_CS2LuaHook = function (this) 
    this.MoneyCtrl:SetType(EnumMoneyType.YinPiao, EnumPlayScoreKey.NONE, true)
    this.NameLabel.text = CJieBaiMgr.Inst.sessionData.title .. LocalString.GetString("之")
    this.MoneyCtrl:SetCost(JieBai_Setting.GetData().PersonalJieBaiTitleMoney)
    local nameInput = CommonDefs.GetComponent_GameObject_Type(this.NameInputNode, typeof(UIInput))
    nameInput.value = CJieBaiMgr.Inst.personalTitleName

    UIEventListener.Get(this.CancelBtn).onClick = DelegateFactory.VoidDelegate(function (go) 
        this:ClosePanel()
    end)

    UIEventListener.Get(this.CloseBtn).onClick = DelegateFactory.VoidDelegate(function (go) 
        this:ClosePanel()
    end)

    UIEventListener.Get(this.SubmitBtn).onClick = DelegateFactory.VoidDelegate(function (go) 
        this:SubmitInfo()
    end)
end
