local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local UIGrid = import "UIGrid"
local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local UISprite = import "UISprite"
local UISlider = import "UISlider"
local UILabel = import "UILabel"
local QnRadioBox = import "L10.UI.QnRadioBox"
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType2 = import "L10.UI.CItemInfoMgr+AlignType"
local AlignType = import "CPlayerInfoMgr+AlignType"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local UILongPressButton = import "L10.UI.UILongPressButton"
local EnumTempPlayDataKey = import "L10.Game.EnumTempPlayDataKey"
local CRankData = import "L10.UI.CRankData"
local Profession = import "L10.Game.Profession"

LuaSchoolRebuildingWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaSchoolRebuildingWnd, "SubmitView", "SubmitView", GameObject)
RegistChildComponent(LuaSchoolRebuildingWnd, "RankView", "RankView", GameObject)
RegistChildComponent(LuaSchoolRebuildingWnd, "TipButton", "TipButton", GameObject)
RegistChildComponent(LuaSchoolRebuildingWnd, "QnRadioBox", "QnRadioBox", QnRadioBox)
RegistChildComponent(LuaSchoolRebuildingWnd, "LeftView", "LeftView", GameObject)
RegistChildComponent(LuaSchoolRebuildingWnd, "CurShiMenMap", "CurShiMenMap", UISprite)
RegistChildComponent(LuaSchoolRebuildingWnd, "CurShiMenMaskSlider", "CurShiMenMaskSlider", UISlider)
RegistChildComponent(LuaSchoolRebuildingWnd, "CurShiMenTexture", "CurShiMenTexture", UISprite)
RegistChildComponent(LuaSchoolRebuildingWnd, "CurShiMenProgressLabel", "CurShiMenProgressLabel", UILabel)
RegistChildComponent(LuaSchoolRebuildingWnd, "CurShiMenFx", "CurShiMenFx", CUIFx)
RegistChildComponent(LuaSchoolRebuildingWnd, "TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaSchoolRebuildingWnd, "MainPlayerInfo", "MainPlayerInfo", GameObject)
RegistChildComponent(LuaSchoolRebuildingWnd, "MyRankLabel", "MyRankLabel", UILabel)
RegistChildComponent(LuaSchoolRebuildingWnd, "MyNameLabel", "MyNameLabel", UILabel)
RegistChildComponent(LuaSchoolRebuildingWnd, "MyThirdTextLabel", "MyThirdTextLabel", UILabel)
RegistChildComponent(LuaSchoolRebuildingWnd, "RankTableView", "RankTableView", QnAdvanceGridView)
RegistChildComponent(LuaSchoolRebuildingWnd, "NoneRankLabel", "NoneRankLabel", GameObject)
RegistChildComponent(LuaSchoolRebuildingWnd, "RankRewardGrid", "RankRewardGrid", UIGrid)
RegistChildComponent(LuaSchoolRebuildingWnd, "RewardItemTemplate", "RewardItemTemplate", GameObject)
RegistChildComponent(LuaSchoolRebuildingWnd, "SubmitViewHeadLabel2", "SubmitViewHeadLabel2", UILabel)
RegistChildComponent(LuaSchoolRebuildingWnd, "SubmitViewHeadLabel3", "SubmitViewHeadLabel3", UILabel)
RegistChildComponent(LuaSchoolRebuildingWnd, "SubmitViewHeadValueLabel1", "SubmitViewHeadValueLabel1", UILabel)
RegistChildComponent(LuaSchoolRebuildingWnd, "SubmitViewHeadValueLabel3", "SubmitViewHeadValueLabel3", UILabel)
RegistChildComponent(LuaSchoolRebuildingWnd, "SubmitViewHeadValueLabel2", "SubmitViewHeadValueLabel2", UILabel)
RegistChildComponent(LuaSchoolRebuildingWnd, "MainItemCell", "MainItemCell", GameObject)
RegistChildComponent(LuaSchoolRebuildingWnd, "ExpectProgressLabel1", "ExpectProgressLabel1", UILabel)
RegistChildComponent(LuaSchoolRebuildingWnd, "SubmitView1QnIncreseAndDecreaseButton", "SubmitView1QnIncreseAndDecreaseButton", QnAddSubAndInputButton)
RegistChildComponent(LuaSchoolRebuildingWnd, "SubmitButton1", "SubmitButton1", GameObject)
RegistChildComponent(LuaSchoolRebuildingWnd, "SubmitView1DetailRoot", "SubmitView1DetailRoot", GameObject)
RegistChildComponent(LuaSchoolRebuildingWnd, "SubmitView1BlankRoot", "SubmitView1BlankRoot", GameObject)
RegistChildComponent(LuaSchoolRebuildingWnd, "SubmitView2ProgressLabel", "SubmitView2ProgressLabel", UILabel)
RegistChildComponent(LuaSchoolRebuildingWnd, "SubmitView2BlankRoot", "SubmitView2BlankRoot", GameObject)
RegistChildComponent(LuaSchoolRebuildingWnd, "SubmitView2RightBlankRoot", "SubmitView2RightBlankRoot", GameObject)
RegistChildComponent(LuaSchoolRebuildingWnd, "SubmitView2DetailRoot", "SubmitView2DetailRoot", GameObject)
RegistChildComponent(LuaSchoolRebuildingWnd, "SubmitView2RightDetailRoot", "SubmitView2RightDetailRoot", GameObject)
RegistChildComponent(LuaSchoolRebuildingWnd, "SubmitView2QnTableView", "SubmitView2QnTableView", QnTableView)
RegistChildComponent(LuaSchoolRebuildingWnd, "SubmitView2ExpectProgressLabel1", "SubmitView2ExpectProgressLabel1", UILabel)
RegistChildComponent(LuaSchoolRebuildingWnd, "SubmitView2QnIncreseAndDecreaseButton", "SubmitView2QnIncreseAndDecreaseButton", QnAddSubAndInputButton)
RegistChildComponent(LuaSchoolRebuildingWnd, "SubmitView2Button", "SubmitView2Button", GameObject)
--@endregion RegistChildComponent end
RegistClassMember(LuaSchoolRebuildingWnd, "m_MapIndex")
RegistClassMember(LuaSchoolRebuildingWnd, "m_MapIdList")
RegistClassMember(LuaSchoolRebuildingWnd, "m_TabIndex")
RegistClassMember(LuaSchoolRebuildingWnd, "m_RebuildingItems")
RegistClassMember(LuaSchoolRebuildingWnd, "m_SubmitViewRow")
RegistClassMember(LuaSchoolRebuildingWnd, "m_LeftProcessList")
RegistClassMember(LuaSchoolRebuildingWnd, "m_IsInitDefault")
RegistClassMember(LuaSchoolRebuildingWnd, "m_RankList")
RegistClassMember(LuaSchoolRebuildingWnd, "m_ProgressPerDayList")
RegistClassMember(LuaSchoolRebuildingWnd, "m_Rank")
LuaSchoolRebuildingWnd.m_Index = -1
function LuaSchoolRebuildingWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)


	
	UIEventListener.Get(self.MainItemCell.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnMainItemCellClick()
	end)


	
	UIEventListener.Get(self.SubmitButton1.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSubmitButton1Click()
	end)

	UIEventListener.Get(self.SubmitView2Button.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSubmitView2ButtonClick()
	end)
    --@endregion EventBind end
end

function LuaSchoolRebuildingWnd:Init()
	self.m_LeftProcessList = {0,0,0,0,0}
	self.QnRadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn, index)
		self:OnSelect(btn, index)
	end)
	self.m_TabIndex = 0
	self.m_MapIdList = {16000013,16000022,16000011,16000012,16000029}
	self.m_MapIndex = 0
	for i = 2, #self.m_MapIdList do
		local item = self.LeftView.transform:GetChild(i - 1)
		UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
			self.m_MapIndex = math.fmod(self.m_MapIndex + i - 1, #self.m_MapIdList)
			self:OnLeftViewItemClick(self.m_MapIndex)
		end)
	end

	self:InitLeftView()
	self.RankTableView.m_DataSource=DefaultTableViewDataSource.Create(function()
        return self.m_RankList and #self.m_RankList or 0
    end, function(item, index)
        self:InitRankItem(item, index)
    end)
	self.RankTableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        self:OnSelectRankTableViewAtRow(row)
    end)
	self:InitRankView()
	self:InitSubmitView()
	Gac2Gas.QuerySchoolRebuildingProgress()
	self.QnRadioBox:ChangeTo(0,true)
end

function LuaSchoolRebuildingWnd:OnEnable()
	g_ScriptEvent:AddListener("OnQuerySchoolRebuildingProgressResult", self, "OnQuerySchoolRebuildingProgressResult")
	g_ScriptEvent:AddListener("OnRankDataReady", self, "OnRankDataReady")
	g_ScriptEvent:AddListener("OnSubmitSchoolRebuildMeterialSuccess", self, "OnSubmitSchoolRebuildMeterialSuccess")
end

function LuaSchoolRebuildingWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnQuerySchoolRebuildingProgressResult", self, "OnQuerySchoolRebuildingProgressResult")
	g_ScriptEvent:RemoveListener("OnRankDataReady", self, "OnRankDataReady")
	g_ScriptEvent:RemoveListener("OnSubmitSchoolRebuildMeterialSuccess", self, "OnSubmitSchoolRebuildMeterialSuccess")
	LuaSchoolRebuildingWnd.m_Index = -1
end

function LuaSchoolRebuildingWnd:OnSubmitSchoolRebuildMeterialSuccess()
	Gac2Gas.QuerySchoolRebuildingProgress()
	self:InitSubmitView()
end

function LuaSchoolRebuildingWnd:OnRankDataReady()
	self.m_RankList = {}
	self.MyRankLabel.text = LocalString.GetString("未上榜")
	local myRankSprite = self.MyRankLabel.transform:Find("RankImage"):GetComponent(typeof(UISprite))
	myRankSprite.spriteName = ""
	self.MyThirdTextLabel.text = 0
	if CommonDefs.DictContains(CClientMainPlayer.Inst.PlayProp.TempPlayData, typeof(UInt32), EnumToInt(EnumTempPlayDataKey.eSchoolRebuilding)) then
		local data = CClientMainPlayer.Inst.PlayProp.TempPlayData[EnumToInt(EnumTempPlayDataKey.eSchoolRebuilding)]
		if data then
			local list = MsgPackImpl.unpack(data.Data.Data)
			if list and list.Count >= 5 then
				for i = 1, 5 do
					if i == (self.m_MapIndex + 1) then
						self.MyThirdTextLabel.text = list[i - 1]
					end
				end
			end
		end
	end
	self.SubmitViewHeadValueLabel3.text = LocalString.GetString("未上榜")
	for i = 0, self.RankRewardGrid.transform.childCount - 1 do
		local obj = self.RankRewardGrid.transform:GetChild(i)
		obj.transform:Find("CurSprite").gameObject:SetActive(false)
	end
	if CRankData.Inst.RankList and CRankData.Inst.RankList.Count > 0 then
		for i = 0,CRankData.Inst.RankList.Count - 1 do
			local info = CRankData.Inst.RankList[i]
			if CClientMainPlayer.Inst and info.PlayerIndex == CClientMainPlayer.Inst.Id then
				local rank = info.Rank
				self.m_Rank = tonumber(rank)
				if self.RankRewardGrid.transform.childCount > 0 then
					local rankAwardData = SchoolRebuilding_Setting.GetData().RankAwardItem
					for i = 0, self.RankRewardGrid.transform.childCount - 1 do
						local obj = self.RankRewardGrid.transform:GetChild(i)
						local list = g_LuaUtil:StrSplit(rankAwardData[i],":")
						local rankText = list[1]
						local rankArea = g_LuaUtil:StrSplit(rankText,"-")

						obj.transform:Find("CurSprite").gameObject:SetActive(self.m_Rank and  tonumber(rankArea[1]) <= self.m_Rank and self.m_Rank <=  tonumber(rankArea[2]))
					end
				end
				self.SubmitViewHeadValueLabel3.text = rank
				self.MyThirdTextLabel.text = info.Value
				self.MyRankLabel.text = ""
				
				if rank==1 then
					myRankSprite.spriteName = "Rank_No.1"
				elseif rank==2 then
					myRankSprite.spriteName = "Rank_No.2png"
				elseif rank==3 then
					myRankSprite.spriteName = "Rank_No.3png"
				else
					self.MyRankLabel.text = rank
				end
			else
				table.insert(self.m_RankList, info)
			end
		end
	end

	table.sort(self.m_RankList, function(a,b) 
		if a.Rank < b.Rank then
			return true
		elseif a.Rank > b.Rank then
			return false
		else
			return a.PlayerIndex < b.PlayerIndex
		end
	end)
	self.RankTableView:ReloadData(true,true)
	self.NoneRankLabel.gameObject:SetActive(self.m_RankList == 0)
	self.MainPlayerInfo.gameObject:SetActive(true)
	if self.m_TabIndex == 1 then
		self.SubmitView.gameObject:SetActive(false)
		self.RankView.gameObject:SetActive(true)
	end
	self.RankTableView.m_ScrollView:ResetPosition()
end

function LuaSchoolRebuildingWnd:OnQuerySchoolRebuildingProgressResult(progressList, progressPerDayList)
	self.m_LeftProcessList = progressList
	self.m_ProgressPerDayList = progressPerDayList
	self:InitSubmitView()
	local maxProgress = SchoolRebuilding_Setting.GetData().MaxProgress
	for i = 0, #self.m_MapIdList - 1 do
		local index = math.fmod(self.m_MapIndex + i, #self.m_MapIdList) + 1
		if i == 0 then
			self.CurShiMenProgressLabel.text = SafeStringFormat3("%d/%d",math.floor(self.m_LeftProcessList[index]),maxProgress)
		else
			local item = self.LeftView.transform:GetChild(i)
			item.transform:Find("ProgressLabel"):GetComponent(typeof(UILabel)).text = math.floor(self.m_LeftProcessList[index] / maxProgress * 100) .. "%"
		end
	end
	self:InitDefaultMapIndex()
end

function LuaSchoolRebuildingWnd:InitDefaultMapIndex()
	if self.m_IsInitDefault then 
		self:OnLeftViewItemClick(self.m_MapIndex)
		return 
	end
	local maxProgress = SchoolRebuilding_Setting.GetData().MaxProgress
	self.m_MapIndex = 0
	if CClientMainPlayer.Inst then
		local class = EnumToInt(CClientMainPlayer.Inst.Class)
		if class < 10 then
			self.m_MapIndex = math.modf((class - 1)/ 2)
		end
	end
	if LuaSchoolRebuildingWnd.m_Index > 0 then
		self.m_MapIndex = LuaSchoolRebuildingWnd.m_Index - 1
	end
	if self.m_LeftProcessList[self.m_MapIndex + 1] == maxProgress then
		self.m_MapIndex = 1
		local minVal = self.m_LeftProcessList[1]
		for i = 2, 5 do
			if self.m_LeftProcessList[i] < minVal then
				self.m_MapIndex = i
				minVal = self.m_LeftProcessList[i]
			end
		end
	end
	self:OnLeftViewItemClick(self.m_MapIndex)
	self.m_IsInitDefault = true
end

function LuaSchoolRebuildingWnd:InitLeftView()
	local maxProgress = SchoolRebuilding_Setting.GetData().MaxProgress
	for i = 0, #self.m_MapIdList - 1 do
		local index = math.fmod(self.m_MapIndex + i, #self.m_MapIdList) + 1
		local id = self.m_MapIdList[index]
		local data = PublicMap_MiniMapRes.GetData(id)
		local scenename = data.ResName
		local mapSpriteName = scenename .."_building"
		local val = self.m_LeftProcessList[index] / maxProgress
		if i == 0 then
			self.CurShiMenMap.spriteName = mapSpriteName
			self.CurShiMenTexture.spriteName = scenename
			self.CurShiMenProgressLabel.text = SafeStringFormat3("%d/%d",math.floor(self.m_LeftProcessList[index]),maxProgress)
			self.CurShiMenMaskSlider:GetComponent(typeof(UITexture)).fillAmount = val
		else
			local item = self.LeftView.transform:GetChild(i)
			item.transform:Find("Map"):GetComponent(typeof(UISprite)).spriteName = mapSpriteName
			item.transform:Find("ProgressLabel"):GetComponent(typeof(UILabel)).text = math.floor(val * 100) .. "%"
			item.transform:Find("MaskSlider"):GetComponent(typeof(UITexture)).fillAmount = val
		end
	end
end

function LuaSchoolRebuildingWnd:InitRankView()
	self.RewardItemTemplate.gameObject:SetActive(false)
	Extensions.RemoveAllChildren(self.RankRewardGrid.transform)
	local rankAwardData = SchoolRebuilding_Setting.GetData().RankAwardItem
	for i = 0, rankAwardData.Length - 1 do
		local list = g_LuaUtil:StrSplit(rankAwardData[i],":")
		local rankText = list[1]
		local rankItemId = tonumber(list[2])
		local data = Item_Item.GetData(rankItemId)
		local rankArea = g_LuaUtil:StrSplit(rankText,"-")

		local obj = NGUITools.AddChild(self.RankRewardGrid.gameObject, self.RewardItemTemplate.gameObject)
		obj.gameObject:SetActive(true)

		local iconTexture = obj.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
		iconTexture:LoadMaterial(data.Icon)
		local rankLabel = obj.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
		rankLabel.text = rankText
		obj.transform:Find("NumLabel").gameObject:SetActive(false)
		obj.transform:Find("CurSprite").gameObject:SetActive(self.m_Rank and rankArea[1] <= self.m_Rank and self.m_Rank <= rankArea[2]) 

		UIEventListener.Get(obj.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
			CItemInfoMgr.ShowLinkItemTemplateInfo(rankItemId, false, nil, AlignType2.Default, 0, 0, 0, 0)
		end)
	end
	self.RankRewardGrid:Reposition()
end

function LuaSchoolRebuildingWnd:InitSubmitView()
	local iconTexture = self.MainItemCell.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
	local nameLabel = self.MainItemCell.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
	local numLabel = self.MainItemCell.transform:Find("NumLabel"):GetComponent(typeof(UILabel))
	local mainItemTempId = SchoolRebuilding_Setting.GetData().MainItemTempId
	local data = Item_Item.GetData(mainItemTempId)

	iconTexture:LoadMaterial(data.Icon)
	nameLabel.text = data.Name
	numLabel.text = ""

	local default, pos, itemId = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, mainItemTempId)
	
	self.SubmitView1DetailRoot.gameObject:SetActive(default)
	self.SubmitView1BlankRoot.gameObject:SetActive(not default)
	self.SubmitView1BlankRoot.transform:Find("Label2"):GetComponent(typeof(UILabel)).text = g_MessageMgr:FormatMessage("SchoolRebuildingWnd_ItemGet1")
	self.SubmitView2BlankRoot.transform:Find("Label2"):GetComponent(typeof(UILabel)).text = g_MessageMgr:FormatMessage("SchoolRebuildingWnd_ItemGet2")

	self.SubmitView1QnIncreseAndDecreaseButton.onValueChanged = DelegateFactory.Action_uint(function(value)
		self:OnSubmitView1Changed(value)
    end)
	self.SubmitView1QnIncreseAndDecreaseButton:SetValue(1, true)
	local item = CItemMgr.Inst:GetById(itemId)
	if item then
		numLabel.text = item.Amount
		local sub = SchoolRebuilding_Setting.GetData().MaxProgressPerDay - (self.m_ProgressPerDayList and self.m_ProgressPerDayList[self.m_MapIndex + 1] or 0)
		local maxVal = math.max(1,math.min(item.Amount,math.floor(sub / SchoolRebuilding_ItemValue.GetData(mainItemTempId).Score)))
		self.SubmitView1QnIncreseAndDecreaseButton:SetMinMax(1, maxVal, 1)
	end

	self:InitSubmitView2()
	self:InitSubmitViewHeader()
end

function LuaSchoolRebuildingWnd:InitSubmitView2()
	self.SubmitView2QnTableView.m_DataSource = DefaultTableViewDataSource.Create(
        function() 
            return self.m_RebuildingItems and #self.m_RebuildingItems or 0
        end,
        function(item, index)
            self:SubmitView2QnTableViewItemAt(item,index)
        end)
    self.SubmitView2QnTableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        self:OnSelectSubmitView2QnTableViewAtRow(row)
    end)
	self:InitSubmitView2QnTableView()
	self.SubmitView2RightDetailRoot.gameObject:SetActive(false)
	self.SubmitView2RightBlankRoot.gameObject:SetActive(#self.m_RebuildingItems ~= 0)
	self.SubmitView2BlankRoot.gameObject:SetActive(#self.m_RebuildingItems == 0)
	self.SubmitView2DetailRoot.transform:Find("Label2").gameObject:SetActive(#self.m_RebuildingItems > 0)
	local submitView2Item = self.SubmitView2RightDetailRoot.transform:Find("Item")
	UIEventListener.Get(submitView2Item.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
		local data = self.m_RebuildingItems[self.m_SubmitViewRow + 1]
		local id = data.TemplateId
		self:ShowItemTip(id)
	end)
end

function LuaSchoolRebuildingWnd:InitSubmitViewHeader()
	if CClientMainPlayer.Inst then
		self.MyNameLabel.text = CClientMainPlayer.Inst.Name
		self.MyNameLabel.transform:Find("Sprite"):GetComponent(typeof(UISprite)).spriteName = Profession.GetIcon(CClientMainPlayer.Inst.Class)
		self.SubmitViewHeadValueLabel2.text = 0
		local sum = 0
		if CommonDefs.DictContains(CClientMainPlayer.Inst.PlayProp.TempPlayData, typeof(UInt32), EnumToInt(EnumTempPlayDataKey.eSchoolRebuilding)) then
			local data = CClientMainPlayer.Inst.PlayProp.TempPlayData[EnumToInt(EnumTempPlayDataKey.eSchoolRebuilding)]
			if data then
				local list = MsgPackImpl.unpack(data.Data.Data)
				if list and list.Count >= 5 then
					for i = 1, 5 do
						sum  = list[i - 1] + sum
						if i == (self.m_MapIndex + 1) then
							self.SubmitViewHeadValueLabel2.text = list[i - 1]
						end
					end
				end
			end
		end
		self.SubmitViewHeadValueLabel1.text = sum
	end
end

function LuaSchoolRebuildingWnd:InitSubmitView2QnTableView()
	local mainItemTempId = SchoolRebuilding_Setting.GetData().MainItemTempId
	self.m_RebuildingItems = {}
	local ids = {}
	SchoolRebuilding_ItemValue.ForeachKey(function(key)
		if key ~= mainItemTempId then
			ids[key] = true
		end
	end)
	if CClientMainPlayer.Inst then
		local itemProp = CClientMainPlayer.Inst.ItemProp
		local bagSize =  itemProp:GetPlaceSize(EnumItemPlace.Bag)
		for i = 1, bagSize do
			local itemId = itemProp:GetItemAt(EnumItemPlace.Bag, i)
			local item = CItemMgr.Inst:GetById(itemId)
			if item then
				if ids[item.TemplateId] then
					table.insert(self.m_RebuildingItems,item)
				end
			end
		end
	end
	self.SubmitView2QnTableView:ReloadData(true, true)
	self.SubmitView2QnTableView.m_ScrollView:ResetPosition()
end

function LuaSchoolRebuildingWnd:SubmitView2QnTableViewItemAt(item,index)
	local iconTexture = item.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
	local bindSprite = item.transform:Find("BindSprite")
	local numLabel = item.transform:Find("NumLabel"):GetComponent(typeof(UILabel))

	local data = self.m_RebuildingItems[index + 1]

	iconTexture:LoadMaterial(data.Icon)
	bindSprite.gameObject:SetActive(data.IsBinded)
	numLabel.text = data.Amount

	item:GetComponent(typeof(UILongPressButton)).OnLongPressDelegate = DelegateFactory.Action(function ()
		self:ShowItemTip(data.TemplateId)
	end)
end

function LuaSchoolRebuildingWnd:OnSelectSubmitView2QnTableViewAtRow(row)
	self.m_SubmitViewRow = row

	local data = self.m_RebuildingItems[row + 1]
	self.SubmitView2RightDetailRoot.gameObject:SetActive(true)
	self.SubmitView2RightBlankRoot.gameObject:SetActive(false)

	local iconTexture = self.SubmitView2RightDetailRoot.transform:Find("Item/IconTexture"):GetComponent(typeof(CUITexture))
	local numLabel = self.SubmitView2RightDetailRoot.transform:Find("Item/NumLabel"):GetComponent(typeof(UILabel))
	local nameLabel = self.SubmitView2RightDetailRoot.transform:Find("Item/NameLabel"):GetComponent(typeof(UILabel))

	iconTexture:LoadMaterial(data.Icon)
	nameLabel.text = data.Name

	self.SubmitView2QnIncreseAndDecreaseButton.onValueChanged = DelegateFactory.Action_uint(function (value)
		self:OnSubmitView2Changed(value)
	end)
	self.SubmitView2QnIncreseAndDecreaseButton:SetValue(1, true)
	local sub = SchoolRebuilding_Setting.GetData().MaxProgressPerDay - (self.m_ProgressPerDayList and self.m_ProgressPerDayList[self.m_MapIndex + 1] or 0)
	local maxVal = math.max(1,math.min(data.Amount,math.floor(sub / SchoolRebuilding_ItemValue.GetData(data.TemplateId).Score)))
	self.SubmitView2QnIncreseAndDecreaseButton:SetMinMax(1, maxVal, 1)
end

function LuaSchoolRebuildingWnd:ShowItemTip(TemplateId)
	CItemInfoMgr.ShowLinkItemTemplateInfo(TemplateId, false, nil, AlignType2.Default, 0, 0, 0, 0)
end
--@region UIEvent

function LuaSchoolRebuildingWnd:OnTipButtonClick()
	g_MessageMgr:ShowMessage("SchoolRebuildingWnd_ReadMe")
end


function LuaSchoolRebuildingWnd:OnMainItemCellClick()
	local mainItemTempId = SchoolRebuilding_Setting.GetData().MainItemTempId
	CItemInfoMgr.ShowLinkItemTemplateInfo(mainItemTempId, false, nil, AlignType2.Default, 0, 0, 0, 0)
end


function LuaSchoolRebuildingWnd:OnSubmitButton1Click()
	local mainItemTempId = SchoolRebuilding_Setting.GetData().MainItemTempId
	local default, pos, itemId = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, mainItemTempId)
	if not default then return end
	local count = self.SubmitView1QnIncreseAndDecreaseButton:GetValue()
	Gac2Gas.RequestSubmitSchoolRebuildMeterial(self.m_MapIndex + 1, 1, pos, itemId, count)
end

function LuaSchoolRebuildingWnd:OnSubmitView2ButtonClick()
	local data = self.m_RebuildingItems[self.m_SubmitViewRow + 1]
	local id = data.TemplateId
	local default, pos, itemId = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, id)
	if not default then return end
	local count = self.SubmitView2QnIncreseAndDecreaseButton:GetValue()
	Gac2Gas.RequestSubmitSchoolRebuildMeterial(self.m_MapIndex + 1, 1, pos, itemId, count)
end
--@endregion UIEvent

function LuaSchoolRebuildingWnd:OnSubmitView1Changed(value)
	local mainItemTempId = SchoolRebuilding_Setting.GetData().MainItemTempId
	self.ExpectProgressLabel1.text = SafeStringFormat3("+%d",value * SchoolRebuilding_ItemValue.GetData(mainItemTempId).Score)
	-- local numLabel = self.MainItemCell.transform:Find("NumLabel"):GetComponent(typeof(UILabel))
	-- numLabel.text = value
end

function LuaSchoolRebuildingWnd:OnSubmitView2Changed(value)
	local data = self.m_RebuildingItems[self.m_SubmitViewRow + 1]
	local id = data.TemplateId
	self.SubmitView2ExpectProgressLabel1.text = SafeStringFormat3("+%d",value * SchoolRebuilding_ItemValue.GetData(id).Score)
	local numLabel = self.SubmitView2RightDetailRoot.transform:Find("Item/NumLabel"):GetComponent(typeof(UILabel))
	numLabel.text = value
end

function LuaSchoolRebuildingWnd:OnSelect(btn, index)
	self.m_TabIndex = index
	self.SubmitView.gameObject:SetActive(index == 0)
	self.RankView.gameObject:SetActive(false)
	--self.RankView.gameObject:SetActive(index == 1)
	self.SubmitView2QnTableView:ReloadData(true, true)
	Gac2Gas.QueryRank(SchoolRebuilding_Setting.GetData().RankId[self.m_MapIndex])
end

function LuaSchoolRebuildingWnd:OnLeftViewItemClick(index)
	self.m_MapIndex = index
	self:InitLeftView()
	local process = self.m_LeftProcessList[self.m_MapIndex + 1] / SchoolRebuilding_Setting.GetData().MaxProgress
	self.TitleLabel.text = process == 1 and LocalString.GetString("重建募集已完成") or LocalString.GetString("师门重建募集中")
	self.QnRadioBox:ChangeTo(self.m_TabIndex, true)
	local index = math.fmod(self.m_MapIndex, #self.m_MapIdList) + 1
	local id = self.m_MapIdList[index]
	local data = PublicMap_MiniMapRes.GetData(id)
	self.SubmitViewHeadLabel2.text = SafeStringFormat3(LocalString.GetString("%s功勋"), data.Name)
	self.SubmitViewHeadLabel3.text = SafeStringFormat3(LocalString.GetString("%s功勋排名"), data.Name)
	self.SubmitView2ProgressLabel.text = SafeStringFormat3(LocalString.GetString("今日%s进度上限%d/%d"), data.Name,self.m_ProgressPerDayList and self.m_ProgressPerDayList[self.m_MapIndex + 1] or 0,SchoolRebuilding_Setting.GetData().MaxProgressPerDay)
	local sizeArray1 = {{232,171},{134,237},{201,212},{181,218},{195,206}}
	local sizeArray2 = {{117,86},{67,119},{110,116},{79,95},{109,115}}
	self.CurShiMenMap.width = sizeArray1[index][1]
	self.CurShiMenMap.height = sizeArray1[index][2]
	for i = 2, #self.m_MapIdList do
		local item = self.LeftView.transform:GetChild(i - 1)
		index = math.fmod(self.m_MapIndex + i - 1, #self.m_MapIdList) + 1
		local sprite = item.transform:Find("Map"):GetComponent(typeof(UISprite))
		sprite.width = sizeArray2[index][1]
		sprite.height = sizeArray2[index][2]
	end
	self:InitSubmitView()
end

function LuaSchoolRebuildingWnd:InitRankItem(item, index)
	local rankLabel = item.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
	local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
	local thirdLabel = item.transform:Find("ThirdLabel"):GetComponent(typeof(UILabel))
	local rankSprite = rankLabel.transform:Find("RankImage"):GetComponent(typeof(UISprite))
	local classSprite = item.transform:Find("NameLabel/Sprite"):GetComponent(typeof(UISprite))

	local info = self.m_RankList[index + 1]

	rankSprite.spriteName = ""
	rankLabel.text = ""
    local rank = info.Rank
    if rank==1 then
        rankSprite.spriteName = "Rank_No.1"
    elseif rank==2 then
        rankSprite.spriteName = "Rank_No.2png"
    elseif rank==3 then
        rankSprite.spriteName = "Rank_No.3png"
    else
        rankLabel.text=tostring(rank)
    end

	thirdLabel.text = info.Value
	nameLabel.text = info.Name
	classSprite.spriteName = Profession.GetIcon(info.Job)
end

function LuaSchoolRebuildingWnd:OnSelectRankTableViewAtRow(row)
	if not self.m_RankList then return end
	local info = self.m_RankList[row + 1]
	local playerId = info.PlayerIndex
	CPlayerInfoMgr.ShowPlayerPopupMenu(playerId, EnumPlayerInfoContext.ChatLink, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
end
