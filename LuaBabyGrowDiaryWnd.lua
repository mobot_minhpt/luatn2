require("common/common_include")

local UITabBar = import "L10.UI.UITabBar"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local TweenAlpha = import "TweenAlpha"
local CUITexture = import "L10.UI.CUITexture"
local BabyMgr = import "L10.Game.BabyMgr"
--local Baby_QiYu = import "L10.Game.Baby_QiYu"
local Vector3 = import "UnityEngine.Vector3"
local CButton = import "L10.UI.CButton"
local Color = import "UnityEngine.Color"
local NGUIText = import "NGUIText"

LuaBabyGrowDiaryWnd = class()

RegistClassMember(LuaBabyGrowDiaryWnd, "Content")
RegistClassMember(LuaBabyGrowDiaryWnd, "DiaryView")
RegistClassMember(LuaBabyGrowDiaryWnd, "QiYuView")

RegistClassMember(LuaBabyGrowDiaryWnd, "TabBar")
RegistClassMember(LuaBabyGrowDiaryWnd, "DiaryTab")
RegistClassMember(LuaBabyGrowDiaryWnd, "QiYuTab")
RegistClassMember(LuaBabyGrowDiaryWnd, "DiaryAlert")
RegistClassMember(LuaBabyGrowDiaryWnd, "QiYuAlert")

function LuaBabyGrowDiaryWnd:Init()
	self:InitClassMembers()
	self:InitValues()
end

function LuaBabyGrowDiaryWnd:InitClassMembers()
	self.TabBar = self.transform:Find("Content/Tabs"):GetComponent(typeof(UITabBar))
	
	self.Content = self.transform:Find("Content").gameObject
	self.Content:SetActive(false)

	local tween = self.transform:Find("BG/BG"):GetComponent(typeof(TweenAlpha))
	CommonDefs.AddEventDelegate(tween.onFinished, DelegateFactory.Action(function ()
		self:ShowContent()
	end))
	self.DiaryView = self.transform:Find("Content/DiaryView").gameObject
	self.DiaryView:SetActive(false)
	self.QiYuView = self.transform:Find("Content/QiYuView").gameObject
	self.QiYuView:SetActive(false)

	self.DiaryTab = self.transform:Find("Content/Tabs/DiaryTab"):GetComponent(typeof(CUITexture))
	self.QiYuTab = self.transform:Find("Content/Tabs/QiYuTab"):GetComponent(typeof(CUITexture))

	self.DiaryAlert = self.transform:Find("Content/Tabs/DiaryTab/Alert").gameObject
	self.DiaryAlert:SetActive(false)
	self.QiYuAlert = self.transform:Find("Content/Tabs/QiYuTab/Alert").gameObject
	self.QiYuAlert:SetActive(false)
	
end

function LuaBabyGrowDiaryWnd:InitValues()
	self.TabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
		self:OnTabChange(go, index)
	end)
	self.DiaryAlert:SetActive(self:ShowDiaryAlert())
	self.QiYuAlert:SetActive(self:ShowQiYuAlert())
end

function LuaBabyGrowDiaryWnd:OnEnable()
	g_ScriptEvent:AddListener("AcceptTaskFromBabySuccess", self, "UpdateDiaryAlert")
	g_ScriptEvent:AddListener("ReceiveQiYuAwardSuccess", self, "UpdateQiYuAlert")
	g_ScriptEvent:AddListener("AcceptQiYuTaskFromBabySuccess", self, "UpdateQiYuAlert")
	g_ScriptEvent:AddListener("BabyInfoDelete", self, "OnBabyInfoDelete")
end

function LuaBabyGrowDiaryWnd:OnDisable()
	g_ScriptEvent:RemoveListener("AcceptTaskFromBabySuccess", self, "UpdateDiaryAlert")
	g_ScriptEvent:RemoveListener("ReceiveQiYuAwardSuccess", self, "ReceiveQiYuAwardSuccess")
	g_ScriptEvent:RemoveListener("AcceptQiYuTaskFromBabySuccess", self, "UpdateQiYuAlert")
	g_ScriptEvent:RemoveListener("BabyInfoDelete", self, "OnBabyInfoDelete")
end

function LuaBabyGrowDiaryWnd:OnBabyInfoDelete()
	CUIManager.CloseUI(CLuaUIResources.BabyGrowDiaryWnd)
end

function LuaBabyGrowDiaryWnd:UpdateQiYuAlert(babyId)
	self.QiYuAlert:SetActive(self:ShowQiYuAlert())
end

function LuaBabyGrowDiaryWnd:UpdateDiaryAlert(babyId)
	self.DiaryAlert:SetActive(self:ShowDiaryAlert())
end

function LuaBabyGrowDiaryWnd:ShowDiaryAlert()
	local result = false
	local selectedBaby = BabyMgr.Inst:GetSelectedBaby()

	if not selectedBaby then
		CUIManager.CloseUI(CLuaUIResources.BabyGrowDiaryWnd)
		return
	end
	if selectedBaby then

		local diaryDict = selectedBaby.Props.ScheduleData.DiaryData
		CommonDefs.DictIterate(diaryDict, DelegateFactory.Action_object_object(function (___key, ___value)
			if ___value.Accepted == 0 then -- 有任务没接
				result = true
			end
	    end))
	end
	return result
end

function LuaBabyGrowDiaryWnd:ShowQiYuAlert()
	local result = false

	local selectedBaby = BabyMgr.Inst:GetSelectedBaby()
	if not selectedBaby then
		CUIManager.CloseUI(CLuaUIResources.BabyGrowDiaryWnd)
		return
	end
	
	if selectedBaby then
		-- 有奖品时显示红点
		local qiyuDict = selectedBaby.Props.ScheduleData.QiYuData
		CommonDefs.DictIterate(qiyuDict, DelegateFactory.Action_object_object(function (___key, ___value)
			if ___value.ReceivedAward == 0 then -- 有奖品没接
				result = true
			end
	    end))

	    local taskData = selectedBaby.Props.TaskData

		local finishedTasks = taskData.FinishedTasks
		local acceptedTasks = taskData.AcceptedTasks
		-- 有任务可接时显示红点
		Baby_QiYu.ForeachKey(function (key)
			local data = Baby_QiYu.GetData(key)
    		if data.TaskId ~= 0 and selectedBaby.Grade >= data.GetLevel then
    			if not CommonDefs.DictContains_LuaCall(finishedTasks, data.TaskId) and not CommonDefs.DictContains_LuaCall(acceptedTasks, data.TaskId) then
    				result = true
    			end
    		end
		end)

	end
	return result
end

function LuaBabyGrowDiaryWnd:OnTabChange(gameObject, index)
	if index == 0 then
		self.DiaryView:SetActive(false)
		self.QiYuView:SetActive(true)
		self.DiaryTab:LoadMaterial("UI/Texture/Transparent/Material/raw_shoppingmallwnd_btn_orange_normal.mat")
		self.QiYuTab:LoadMaterial("UI/Texture/Transparent/Material/raw_shoppingmallwnd_btn_orange_highlight.mat")
		self.DiaryTab.transform.localScale = Vector3.one
		self.QiYuTab.transform.localScale = Vector3(1.2, 1.2, 1.2)
		self.DiaryTab.transform:GetComponent(typeof(CButton)):SetFontColor(NGUIText.ParseColor("FFF3C6", 0))
		self.QiYuTab.transform:GetComponent(typeof(CButton)):SetFontColor(Color.white)
		local luaScript = CommonDefs.GetComponent_GameObject_Type(self.QiYuView, typeof(CCommonLuaScript))
		luaScript:Init({})
	elseif index == 1 then
		self.DiaryView:SetActive(true)
		self.QiYuView:SetActive(false)
		self.DiaryTab:LoadMaterial("UI/Texture/Transparent/Material/raw_shoppingmallwnd_btn_orange_highlight.mat")
		self.QiYuTab:LoadMaterial("UI/Texture/Transparent/Material/raw_shoppingmallwnd_btn_orange_normal.mat")
		self.QiYuTab.transform.localScale = Vector3.one
		self.DiaryTab.transform.localScale = Vector3(1.2, 1.2, 1.2)
		self.QiYuTab.transform:GetComponent(typeof(CButton)):SetFontColor(NGUIText.ParseColor("FFF3C6", 0))
		self.DiaryTab.transform:GetComponent(typeof(CButton)):SetFontColor(Color.white)
        local script = self.DiaryView:GetComponent(typeof(CCommonLuaScript))
		script.m_LuaSelf:Init({})
	end
end

function LuaBabyGrowDiaryWnd:ShowContent()
	self.Content:SetActive(true)
	self.TabBar:ChangeTab(0)
end

return LuaBabyGrowDiaryWnd
