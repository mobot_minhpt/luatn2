local UITabBar               = import "L10.UI.UITabBar"
local CQnReturnAwardTemplate = import "L10.UI.CQnReturnAwardTemplate"
local Item_Item              = import "L10.Game.Item_Item"

LuaArenaRuleAwardWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaArenaRuleAwardWnd, "ruleRoot", "RuleRoot", GameObject)
RegistChildComponent(LuaArenaRuleAwardWnd, "awardRoot", "AwardRoot", GameObject)
RegistChildComponent(LuaArenaRuleAwardWnd, "ruleTemplate", "RuleTemplate", GameObject)
RegistChildComponent(LuaArenaRuleAwardWnd, "ruleTable", "RuleTable", UITable)
RegistChildComponent(LuaArenaRuleAwardWnd, "awardTabBar", "AwardTabs", UITabBar)
RegistChildComponent(LuaArenaRuleAwardWnd, "rankAwardView", "RankAwardView", GameObject)
RegistChildComponent(LuaArenaRuleAwardWnd, "danAwardView", "DanAwardView", GameObject)
RegistChildComponent(LuaArenaRuleAwardWnd, "rankAwardDesc", "RankAwardDesc", UILabel)
RegistChildComponent(LuaArenaRuleAwardWnd, "danAwardDesc", "DanAwardDesc", UILabel)
RegistChildComponent(LuaArenaRuleAwardWnd, "rightTabBar", "Tabs", UITabBar)
RegistChildComponent(LuaArenaRuleAwardWnd, "danAwardTemplate", "DanAwardTemplate", GameObject)
RegistChildComponent(LuaArenaRuleAwardWnd, "rankGrid", "RankGrid", UIGrid)
RegistChildComponent(LuaArenaRuleAwardWnd, "rankAwardTemplate", "RankAwardTemplate", GameObject)
RegistChildComponent(LuaArenaRuleAwardWnd, "rankTemplate", "RankTemplate", GameObject)
--@endregion RegistChildComponent end

RegistClassMember(LuaArenaRuleAwardWnd, "isRuleInitDone")
RegistClassMember(LuaArenaRuleAwardWnd, "isAwardInitDone")
RegistClassMember(LuaArenaRuleAwardWnd, "isRankAwardInitDone")
RegistClassMember(LuaArenaRuleAwardWnd, "isDanAwardInitDone")

function LuaArenaRuleAwardWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.ruleTemplate:SetActive(false)
    self.danAwardTemplate:SetActive(false)
    self.rankAwardTemplate:SetActive(false)
    self.rankTemplate:SetActive(false)
end

function LuaArenaRuleAwardWnd:Init()
    self:InitRightTabBar()
end

function LuaArenaRuleAwardWnd:InitRightTabBar()
    self.rightTabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
		self.ruleRoot:SetActive(index == 0)
		self.awardRoot:SetActive(index == 1)
        if index == 0 and not self.isRuleInitDone then
            self:InitRule()
        elseif index == 1 and not self.isAwardInitDone then
            self:InitAward()
        end
    end)
    self.rightTabBar:ChangeTab(0, false)
end

function LuaArenaRuleAwardWnd:InitRule()
    self.isRuleInitDone = true
    local rule = Arena_Setting.GetData().Rule
    local danScore = LuaArenaMgr.awardInfo.danScore
    Extensions.RemoveAllChildren(self.ruleTable.transform)
    for title, messageKey in string.gmatch(rule, "([^,^;]+),([^,^;]+)") do
        local child = NGUITools.AddChild(self.ruleTable.gameObject, self.ruleTemplate)
        child:SetActive(true)
        child.transform:Find("Sprite/Title"):GetComponent(typeof(UILabel)).text = title
        child.transform:Find("Content"):GetComponent(typeof(UILabel)).text = g_MessageMgr:FormatMessage(messageKey, danScore)
    end
    self.ruleTable:Reposition()
end

function LuaArenaRuleAwardWnd:InitAward()
    self.isAwardInitDone = true
    self.awardTabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
		self.danAwardView:SetActive(index == 0)
		self.rankAwardView:SetActive(index == 1)
        if index == 0 and not self.isDanAwardInitDone then
            self:InitDanAward()
        elseif index == 1 and not self.isRankAwardInitDone then
            self:InitRankAward()
        end
    end)
    self.awardTabBar:ChangeTab(0, false)
end

function LuaArenaRuleAwardWnd:InitDanAward()
    self.danAwardDesc.text = g_MessageMgr:FormatMessage("ARENA_DAN_AWARD_DESC")
    local parent = self.danAwardView.transform:Find("Table")
    local largeDan, smallDan = LuaArenaMgr:GetLargeAndSmallDan(LuaArenaMgr.awardInfo.danScore)
    largeDan = largeDan or -1
    for i = 1, 5 do
        local child = parent:GetChild(i - 1)
        local danData = Arena_DanInfo.GetData(i)
        child:Find("ScoreAreaLabel"):GetComponent(typeof(UILabel)).text = danData.ScoreText
        local danLabel = child:Find("DanLabel"):GetComponent(typeof(UILabel))
        danLabel.text = SafeStringFormat3("[%s]%s[-]", danData.DanColor, danData.DanName)
        child:Find("Icon"):GetComponent(typeof(CUITexture)):LoadMaterial(danData.Icon)
        child:Find("Bg").gameObject:SetActive(largeDan ~= i)
        child:Find("Me").gameObject:SetActive(largeDan == i)

        local danRaward = danData.DanReward
        if danRaward then
            local awardGrid = child:Find("AwardGrid"):GetComponent(typeof(UIGrid))
            Extensions.RemoveAllChildren(awardGrid.transform)
            for j = 0, danRaward.Length - 1, 2 do
                local awardChild = NGUITools.AddChild(awardGrid.gameObject, self.danAwardTemplate)
                awardChild:SetActive(true)

                local qnReturnAward = awardChild.transform:GetComponent(typeof(CQnReturnAwardTemplate))
                qnReturnAward:Init(Item_Item.GetData(danRaward[j]), danRaward[j + 1])
                awardChild.transform:Find("Received").gameObject:SetActive(largeDan >= i)
            end
            awardGrid:Reposition()
        end
    end
end

function LuaArenaRuleAwardWnd:InitRankAward()
    self.rankAwardDesc.text = g_MessageMgr:FormatMessage("ARENA_RANK_AWARD_DESC")

    local rankSpriteTbl = {g_sprites.RankFirstSpriteName, g_sprites.RankSecondSpriteName, g_sprites.RankThirdSpriteName}
    local count = Arena_RankInfo.GetDataCount()
    Extensions.RemoveAllChildren(self.rankGrid.transform)
    local myRank = LuaArenaMgr.awardInfo.rank
    for i = 1, count do
        local rankData = Arena_RankInfo.GetData(i)
        local child = NGUITools.AddChild(self.rankGrid.gameObject, self.rankTemplate)
        child:SetActive(true)

        local rankNum = child.transform:Find("RankNum"):GetComponent(typeof(UILabel))
        local rankSprite = child.transform:Find("RankNum/Sprite"):GetComponent(typeof(UISprite))
        local minRank = rankData.MinRank
        local maxRank = rankData.MaxRank
        if minRank <= 3 then
            rankNum.text = ""
            rankSprite.gameObject:SetActive(true)
            rankSprite.spriteName = rankSpriteTbl[minRank]
        else
            rankNum.text = SafeStringFormat3("%d-%d", minRank, maxRank)
            rankSprite.gameObject:SetActive(false)
        end

        local awardTable = child.transform:Find("Table"):GetComponent(typeof(UITable))
        Extensions.RemoveAllChildren(awardTable.transform)
        local seasonReward = CommonDefs.IS_VN_CLIENT and rankData.SeasonReward_VN or rankData.SeasonReward
        for j = 0, seasonReward.Length - 1, 2 do
            local awardChild = NGUITools.AddChild(awardTable.gameObject, self.rankAwardTemplate)
            awardChild:SetActive(true)
            local qnReturnAward = awardChild.transform:GetComponent(typeof(CQnReturnAwardTemplate))
            qnReturnAward:Init(Item_Item.GetData(tonumber(seasonReward[j])), 0)
            awardChild.transform:Find("Label"):GetComponent(typeof(UILabel)).text = seasonReward[j + 1]
        end
        awardTable:Reposition()

        child.transform:Find("Me").gameObject:SetActive(false)
    end
    self.rankGrid:Reposition()
end

--@region UIEvent

--@endregion UIEvent
