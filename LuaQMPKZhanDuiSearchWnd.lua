local EnumQMPKZhanduiMemberWndType = import "L10.Game.EnumQMPKZhanduiMemberWndType"
local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"
local UISearchView=import "L10.UI.UISearchView"
local QnButton=import "L10.UI.QnButton"
local QnTableView=import "L10.UI.QnTableView"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local Profession=import "L10.Game.Profession"
local EnumClass = import "L10.Game.EnumClass"
local CNumberKeyBoardMgr = import "CNumberKeyBoardMgr"
local CTooltip=import "L10.UI.CTooltip"

CLuaQMPKZhanDuiSearchWnd = class()
RegistClassMember(CLuaQMPKZhanDuiSearchWnd,"m_CloseBtn")
RegistClassMember(CLuaQMPKZhanDuiSearchWnd,"m_SearchBtn")
RegistClassMember(CLuaQMPKZhanDuiSearchWnd,"m_ZhanDuiBtn")
RegistClassMember(CLuaQMPKZhanDuiSearchWnd,"m_ZhanDuiBtnText")
RegistClassMember(CLuaQMPKZhanDuiSearchWnd,"m_IncreseaBtn")
RegistClassMember(CLuaQMPKZhanDuiSearchWnd,"m_DecreaseBtn")
RegistClassMember(CLuaQMPKZhanDuiSearchWnd,"m_PageLabel")
RegistClassMember(CLuaQMPKZhanDuiSearchWnd,"m_NoZhanDuiRoot")
RegistClassMember(CLuaQMPKZhanDuiSearchWnd,"m_HasZhanDuiRoot")
RegistClassMember(CLuaQMPKZhanDuiSearchWnd,"m_NoZhanDuiTable")
RegistClassMember(CLuaQMPKZhanDuiSearchWnd,"m_HasZhanDuiTable")
RegistClassMember(CLuaQMPKZhanDuiSearchWnd,"m_AdvSearchBtn")
RegistClassMember(CLuaQMPKZhanDuiSearchWnd,"m_AdvSearchBtn2")
RegistClassMember(CLuaQMPKZhanDuiSearchWnd,"m_AllZhanduiBtn")
RegistClassMember(CLuaQMPKZhanDuiSearchWnd,"m_NormalSearchRoot")
RegistClassMember(CLuaQMPKZhanDuiSearchWnd,"m_AdvSearchRoot")
RegistClassMember(CLuaQMPKZhanDuiSearchWnd,"m_CurrentPage")
RegistClassMember(CLuaQMPKZhanDuiSearchWnd,"m_HasZhanDui")
RegistClassMember(CLuaQMPKZhanDuiSearchWnd,"m_Source")
RegistClassMember(CLuaQMPKZhanDuiSearchWnd,"m_TotalPage")

RegistClassMember(CLuaQMPKZhanDuiSearchWnd,"m_TableViewDataSource")


function CLuaQMPKZhanDuiSearchWnd:Awake()
    -- self.m_CloseBtn = self.transform:Find("Wnd_Bg_Primary_Normal/CloseButton").gameObject
    self.m_SearchBtn = self.transform:Find("Anchor/BottomView/NormalRoot/SearchView"):GetComponent(typeof(UISearchView))
    self.m_ZhanDuiBtn = self.transform:Find("Anchor/BottomView/Create").gameObject
    self.m_ZhanDuiBtnText = self.transform:Find("Anchor/BottomView/Create/Label"):GetComponent(typeof(UILabel))
    self.m_IncreseaBtn = self.transform:Find("Anchor/BottomView/QnIncreseAndDecreaseButton/IncreaseButton"):GetComponent(typeof(QnButton))
    self.m_DecreaseBtn = self.transform:Find("Anchor/BottomView/QnIncreseAndDecreaseButton/DecreaseButton"):GetComponent(typeof(QnButton))
    self.m_PageLabel = self.transform:Find("Anchor/BottomView/QnIncreseAndDecreaseButton/Sprite/Label"):GetComponent(typeof(UILabel))
    self.m_NoZhanDuiRoot = self.transform:Find("Anchor/ZhanDuiList (1)").gameObject
    self.m_HasZhanDuiRoot = self.transform:Find("Anchor/ZhanDuiList").gameObject
    self.m_NoZhanDuiTable = self.transform:Find("Anchor/ZhanDuiList (1)/ListView"):GetComponent(typeof(QnTableView))
    self.m_HasZhanDuiTable = self.transform:Find("Anchor/ZhanDuiList/ListView"):GetComponent(typeof(QnTableView))
    self.m_AdvSearchBtn = self.transform:Find("Anchor/BottomView/NormalRoot/AdvSearchButton").gameObject
    self.m_AdvSearchBtn2 = self.transform:Find("Anchor/BottomView/AdvRoot/AdvSearchButton").gameObject
    self.m_AllZhanduiBtn = self.transform:Find("Anchor/BottomView/AdvRoot/AllZhanduiButton").gameObject
    self.m_NormalSearchRoot = self.transform:Find("Anchor/BottomView/NormalRoot").gameObject
    self.m_AdvSearchRoot = self.transform:Find("Anchor/BottomView/AdvRoot").gameObject
self.m_CurrentPage = 1
self.m_HasZhanDui = false
self.m_Source = 0
self.m_TotalPage = 1

end

-- Auto Generated!!
function CLuaQMPKZhanDuiSearchWnd:OnEnable( )
    UIEventListener.Get(self.m_ZhanDuiBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnZhanDuiBtnClicked(go) end)

    UIEventListener.Get(self.m_IncreseaBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) self:OnIncreaseBtnClicked(go) end)
    UIEventListener.Get(self.m_DecreaseBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) self:OnDecreaseBtnClicked(go) end)
    UIEventListener.Get(self.m_PageLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) self:OnClickPageLabel(go) end)

    UIEventListener.Get(self.m_AdvSearchBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnAdvSearchBtnClicked(go) end)
    UIEventListener.Get(self.m_AdvSearchBtn2).onClick = DelegateFactory.VoidDelegate(function(go) self:OnAdvSearchBtnClicked(go) end)
    UIEventListener.Get(self.m_AllZhanduiBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnAllZhanduiBtnClicked(go) end)

    g_ScriptEvent:AddListener("QMPKQueryZhanDuiResult", self, "OnQueryZhanDuiResult")
    g_ScriptEvent:AddListener("QMPKReplySelfPersonalInfo", self, "OnReplySelfPersonalInfo")
end
function CLuaQMPKZhanDuiSearchWnd:OnDisable( )
    g_ScriptEvent:RemoveListener("QMPKQueryZhanDuiResult", self, "OnQueryZhanDuiResult")
    g_ScriptEvent:RemoveListener("QMPKReplySelfPersonalInfo", self, "OnReplySelfPersonalInfo")
end

function CLuaQMPKZhanDuiSearchWnd:OnClickPageLabel(go)
    local min=0
    local max=math.max(1,self.m_TotalPage)

    local val=max
    local num=0
    while val>=1 do
        num = num+1
        val = val/10
    end

    CNumberKeyBoardMgr.ShowNumberKeyBoardWithRange(min, max, self.m_CurrentPage, num, DelegateFactory.Action_int(function (val) 
        self.m_PageLabel.text = tostring(val)
    end), DelegateFactory.Action_int(function (val) 
        local page=math.max(1,val)
        page=math.min(self.m_TotalPage,page)
        self.m_PageLabel.text = SafeStringFormat3("%d/%d", page, self.m_TotalPage)

        if self.m_Source == 1 then
            Gac2Gas.RequestQueryQmpkZhanDuiList(page)
        elseif self.m_Source == 2 then
            Gac2Gas.SearchQmpkZhanDuiByClassAndZhiHui(CQuanMinPKMgr.Inst.m_SearchZhanduiClass, CQuanMinPKMgr.Inst.m_SearchZhanduiCommandIndex, page)
        end
    end), self.m_PageLabel, CTooltip.AlignType.Top, true)
end

function CLuaQMPKZhanDuiSearchWnd:OnReplySelfPersonalInfo()
    local info=CQuanMinPKMgr.Inst.m_MyPersonalInfo
    if info then
        if info.m_ZhanDuiId>0 then--有战队
        else--没有战队
        end
        self:Init()
    end
end

function CLuaQMPKZhanDuiSearchWnd:OnIncreaseBtnClicked( go) 
    if self.m_CurrentPage >= self.m_TotalPage then
        return
    end
    if self.m_Source == 1 then
        Gac2Gas.RequestQueryQmpkZhanDuiList(self.m_CurrentPage + 1)
    elseif self.m_Source == 2 then
        Gac2Gas.SearchQmpkZhanDuiByClassAndZhiHui(CQuanMinPKMgr.Inst.m_SearchZhanduiClass, CQuanMinPKMgr.Inst.m_SearchZhanduiCommandIndex, self.m_CurrentPage + 1)
    end
end
function CLuaQMPKZhanDuiSearchWnd:OnDecreaseBtnClicked( go) 
    if self.m_CurrentPage <= 1 then
        return
    end
    if self.m_Source == 1 then
        Gac2Gas.RequestQueryQmpkZhanDuiList(self.m_CurrentPage - 1)
    elseif self.m_Source == 2 then
        Gac2Gas.SearchQmpkZhanDuiByClassAndZhiHui(CQuanMinPKMgr.Inst.m_SearchZhanduiClass, CQuanMinPKMgr.Inst.m_SearchZhanduiCommandIndex, self.m_CurrentPage - 1)
    end
end
function CLuaQMPKZhanDuiSearchWnd:OnAdvSearchBtnClicked( go) 
    CUIManager.ShowUI(CLuaUIResources.QMPKSearchRecruitWnd)
end
function CLuaQMPKZhanDuiSearchWnd:OnAllZhanduiBtnClicked( go) 
    self.m_CurrentPage = 1
    Gac2Gas.RequestQueryQmpkZhanDuiList(self.m_CurrentPage)
end
function CLuaQMPKZhanDuiSearchWnd:OnQueryZhanDuiResult( args) 
    -- print("OnQueryZhanDuiResult")
    local selfZhanDuiId, page, totalPage, source=args[0],args[1],args[2],args[3]

    self.m_Source = source
    self.m_HasZhanDui = selfZhanDuiId > 0
    self.m_NoZhanDuiRoot:SetActive(not self.m_HasZhanDui)
    self.m_HasZhanDuiRoot:SetActive(self.m_HasZhanDui)

    self.m_NoZhanDuiTable.m_DataSource.count=CQuanMinPKMgr.Inst.m_ZhanDuiList.Count

    if self.m_HasZhanDui then
        self.m_ZhanDuiBtnText.text = LocalString.GetString("我的战队")
        self.m_HasZhanDuiTable:ReloadData(true, false)
        self.m_HasZhanDuiTable.OnSelectAtRow = DelegateFactory.Action_int(function(index) self:OnSelectAtRow(index) end)
    else
        self.m_ZhanDuiBtnText.text = LocalString.GetString("创建战队")
        self.m_NoZhanDuiTable:ReloadData(true, false)
        self.m_NoZhanDuiTable.OnSelectAtRow = DelegateFactory.Action_int(function(index) self:OnSelectAtRow(index) end)
    end

    self.m_CurrentPage = page
    if self.m_CurrentPage <= 1 then
        self.m_DecreaseBtn.Enabled = false
    else
        self.m_DecreaseBtn.Enabled = true
    end
    if self.m_CurrentPage >= totalPage then
        self.m_IncreseaBtn.Enabled = false
    else
        self.m_IncreseaBtn.Enabled = true
    end
    if totalPage == 0 then
        totalPage = 1
    end
    self.m_TotalPage = totalPage
    self.m_PageLabel.text = SafeStringFormat3( "%d/%d",page,totalPage )

    self.m_AdvSearchRoot:SetActive(source == 2)
    self.m_NormalSearchRoot:SetActive(source == 1)
end
function CLuaQMPKZhanDuiSearchWnd:OnSelectAtRow( index) 
    CQuanMinPKMgr.Inst:ShowZhanDuiMemberWnd(index, EnumQMPKZhanduiMemberWndType.eSearchZhandui, 0, false)
end
function CLuaQMPKZhanDuiSearchWnd:Init( )
    local function initItem(item,row)
        if row < CQuanMinPKMgr.Inst.m_ZhanDuiList.Count then
            if self.m_HasZhanDui then
                self:InitQMPKZhanDuiItem(item,CQuanMinPKMgr.Inst.m_ZhanDuiList[row],row%2==0)
            else
                item:Init(CQuanMinPKMgr.Inst.m_ZhanDuiList[row], row)
            end
        end
    end
    local count=CQuanMinPKMgr.Inst.m_ZhanDuiList.Count

    local dataSource=DefaultTableViewDataSource.CreateByCount(count,initItem)
    self.m_NoZhanDuiTable.m_DataSource = dataSource
    self.m_HasZhanDuiTable.m_DataSource = dataSource

    self.m_SearchBtn.OnSearch =DelegateFactory.Action_string(function(str) self:OnSearchBtnClicked(str) end)
    
    Gac2Gas.RequestQueryQmpkZhanDuiList(self.m_CurrentPage)
    self.m_NoZhanDuiRoot:SetActive(not CQuanMinPKMgr.Inst.m_HasZhanDui)
    self.m_HasZhanDuiRoot:SetActive(CQuanMinPKMgr.Inst.m_HasZhanDui)
end
function CLuaQMPKZhanDuiSearchWnd:OnSearchBtnClicked( value) 
    local id=tonumber(value)
    if not id then
        g_MessageMgr:ShowMessage("QMPK_Search_Zhandui_Only_By_Id")
        return
    end
    Gac2Gas.QueryQmpkZhanDuiInfoByMemberId(id)
end
function CLuaQMPKZhanDuiSearchWnd:OnZhanDuiBtnClicked( go) 
    if self.m_HasZhanDui then
        CUIManager.ShowUI(CLuaUIResources.QMPKSelfZhanDuiWnd)
    else
        CUIManager.ShowUI(CLuaUIResources.QMPKCreateZhanDuiWnd)
    end
end

function CLuaQMPKZhanDuiSearchWnd:InitQMPKZhanDuiItem(tableItem,zhanDui, isOdd)
    local transform=tableItem.transform
    local m_NameLabel = transform:Find("Name"):GetComponent(typeof(UILabel))
    local m_SloganLabel = transform:Find("Slogan"):GetComponent(typeof(UILabel))
    local m_MemberGrid = transform:Find("Members (1)/MemberGrid"):GetComponent(typeof(UIGrid))
    local m_MemberObj = transform:Find("Member").gameObject

    -- local tableItem=transform:GetComponent(typeof(QnTableItem))
    if isOdd then
        tableItem:SetBackgroundTexture("common_textbg_02_light")
    else
        tableItem:SetBackgroundTexture("common_textbg_02_dark")
    end
    m_NameLabel.text = zhanDui.m_Name
    m_SloganLabel.text = zhanDui.m_Slogan
    Extensions.RemoveAllChildren(m_MemberGrid.transform)
    do
        local i = 0 local len = zhanDui.m_Member.Length
        while i < len do
            local go = NGUITools.AddChild(m_MemberGrid.gameObject, m_MemberObj)
            go:SetActive(true)
            go:GetComponent(typeof(UISprite)).spriteName= Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), zhanDui.m_Member[i]))
            -- CommonDefs.GetComponent_GameObject_Type(go, typeof(UISprite)).spriteName =
            i = i + 1
        end
    end
    m_MemberGrid:Reposition()
end

return CLuaQMPKZhanDuiSearchWnd
