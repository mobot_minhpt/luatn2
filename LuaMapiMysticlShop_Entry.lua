require("common/common_include")
local Gac2Gas=import "L10.Game.Gac2Gas"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local MapiMysticalShop_SaledItem = import "L10.Game.MapiMysticalShop_SaledItem"
local MapiMysticalShop_Setting = import "L10.Game.MapiMysticalShop_Setting"
local DelegateFactory = import "DelegateFactory"
local CShopMallGoodsItem = import "L10.UI.CShopMallGoodsItem"
local MessageMgr = import "L10.Game.MessageMgr"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local CommonDefs = import "L10.Game.CommonDefs"

CLuaMapiMysticlShop_Entry=class()
RegistClassMember(CLuaMapiMysticlShop_Entry,"m_TableView")
RegistClassMember(CLuaMapiMysticlShop_Entry,"m_EnterButton")
RegistClassMember(CLuaMapiMysticlShop_Entry, "m_MoneyCtrl")
RegistClassMember(CLuaMapiMysticlShop_Entry,"m_TipButton")

RegistClassMember(CLuaMapiMysticlShop_Entry, "m_RareIds")
RegistClassMember(CLuaMapiMysticlShop_Entry, "m_RareCounts")

function CLuaMapiMysticlShop_Entry:Init()
	self.m_MoneyCtrl = self.m_MoneyCtrl:GetComponent(typeof(CCurentMoneyCtrl))
	self.m_MoneyCtrl:SetType(EnumMoneyType.Score, EnumPlayScoreKey.YuMa)
	local cost = MapiMysticalShop_Setting.GetData().OpenShopCost[0]
	self.m_MoneyCtrl:SetCost(cost)

	self.m_EnterButton.OnClick = DelegateFactory.Action_QnButton(
		function(button)
			if self.m_MoneyCtrl.moneyEnough then
			 	Gac2Gas.OpenSecretShop(EnumSecretShopType.eMapiShop)
			else
				MessageMgr.Inst:ShowMessage("MAPI_JIFEN_NOT_ENOUGH",{})
			end
		end
	)
	self.m_TipButton.OnClick = DelegateFactory.Action_QnButton(
		function(button)
			MessageMgr.Inst:ShowMessage("MAPI_SHOP_TIP",{})
		end
	)

	local ItemAt = function(template, row)
		local item = template.gameObject:GetComponent(typeof(CShopMallGoodsItem))
		local data = MapiMysticalShop_SaledItem.GetData(self.m_RareIds[row + 1])
		local count = self.m_RareCounts[row + 1]
		if data then
			item:UpdateData(data.ItemId, 0, nil, count, true, false)
		end
	end

	self.m_RareIds = {}
	self.m_RareCounts = {}
	local itemData = LuaGetGlobal("PreMapiShopWnd_ItemData")
	CommonDefs.DictIterate(itemData, DelegateFactory.Action_object_object(
		function(key, value)
			if key and value then
				table.insert(self.m_RareIds, key)
				table.insert(self.m_RareCounts, value)
			end
		end
	))
	
	self.m_TableView.m_DataSource = DefaultTableViewDataSource.CreateByCount(#self.m_RareIds, ItemAt)
	self.m_TableView:ReloadData(true, false)
end

function CLuaMapiMysticlShop_Entry:OnDestroy()
	LuaDelGlobal("PreMapiShopWnd_ItemData")
end

return CLuaMapiMysticlShop_Entry
