local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local Animation = import "UnityEngine.Animation"
local CQnReturnAwardTemplate = import "L10.UI.CQnReturnAwardTemplate"
local CItemMgr = import "L10.Game.CItemMgr"

LuaQingMing2023PVEResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaQingMing2023PVEResultWnd, "EasyView", GameObject)
RegistChildComponent(LuaQingMing2023PVEResultWnd, "DifficultView", GameObject)
RegistChildComponent(LuaQingMing2023PVEResultWnd, "RewardView", GameObject)

RegistClassMember(LuaQingMing2023PVEResultWnd, "animation")

--@endregion RegistChildComponent end

function LuaQingMing2023PVEResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaQingMing2023PVEResultWnd:Init()
    self.animation = self.transform:GetComponent(typeof(Animation))
    if LuaQingMing2023Mgr.m_PVE_Result.isWin then
        self:InitWinStyle()
    else
        self:InitLoseStyle()
    end
end

function LuaQingMing2023PVEResultWnd:InitWinStyle()
    self.animation:Play("common_result_win")
    local resultInfo = LuaQingMing2023Mgr.m_PVE_Result
    -- 成绩部分
    if resultInfo.isDifficult then
        self.EasyView:SetActive(false)
        self.DifficultView:SetActive(true)
        self.DifficultView.transform:Find("TimeLabel"):GetComponent(typeof(UILabel)).text = LuaGamePlayMgr:GetRemainTimeText(resultInfo.costTime)
        self.DifficultView.transform:Find("TodayBestLabel"):GetComponent(typeof(UILabel)).text = LuaGamePlayMgr:GetRemainTimeText(resultInfo.bestTime)
        self.DifficultView.transform:Find("RankLabel"):GetComponent(typeof(UILabel)).text = resultInfo.rank
        local rankBtn = self.DifficultView.transform:Find("RankButton").gameObject
        UIEventListener.Get(rankBtn).onClick = DelegateFactory.VoidDelegate(function(go)
            Gac2Gas.QueryQingMing2023PveRank()
        end)
    else
        self.EasyView:SetActive(true)
        self.DifficultView:SetActive(false)
        self.EasyView.transform:Find("TimeLabel"):GetComponent(typeof(UILabel)).text = LuaGamePlayMgr:GetRemainTimeText(resultInfo.costTime)
    end
    -- 奖励部分
    local itemCell = self.RewardView.transform:Find("ItemCell")
    local fullLabel = self.RewardView.transform:Find("FullLabel")
    if resultInfo.isReward then
        itemCell.gameObject:SetActive(true)
        fullLabel.gameObject:SetActive(false)
        local setData = QingMing2023_PVESetting.GetData()
        local itemTemplateId = resultInfo.isDifficult and setData.HardAwardItemId or setData.EasyAwardItemId
        itemCell:GetComponent(typeof(CQnReturnAwardTemplate)):Init(CItemMgr.Inst:GetItemTemplate(itemTemplateId), 1)
    else
        itemCell.gameObject:SetActive(false)
        fullLabel.gameObject:SetActive(true)
    end
end

function LuaQingMing2023PVEResultWnd:InitLoseStyle()
    self.animation:Play("common_result_lose02")
    RegisterTickOnce(function ()
        CUIManager.CloseUI(CLuaUIResources.QingMing2023PVEResultWnd)
    end, 2000)
end

--@region UIEvent

--@endregion UIEvent

