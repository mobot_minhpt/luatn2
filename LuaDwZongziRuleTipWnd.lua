local GameObject			= import "UnityEngine.GameObject"
local UITable				= import "UITable"
local UIScrollView			= import "UIScrollView"
local UIScrollViewIndicator = import "UIScrollViewIndicator"
local CTipTitleItem			= import "CTipTitleItem"

local Extensions			= import "Extensions"

local CUICommonDef			= import "L10.UI.CUICommonDef"
local CMessageTipMgr		= import "L10.UI.CMessageTipMgr"
local CTipParagraphItem		= import "L10.UI.CTipParagraphItem"

local CommonDefs			= import "L10.Game.CommonDefs"
local MessageMgr			= import "L10.Game.MessageMgr"

CLuaDwZongziRuleTipWnd = class()

RegistChildComponent(CLuaDwZongziRuleTipWnd, "Table",				UITable)
RegistChildComponent(CLuaDwZongziRuleTipWnd, "TitleTemplate",		GameObject)
RegistChildComponent(CLuaDwZongziRuleTipWnd, "ParagraphTemplate",	GameObject)
RegistChildComponent(CLuaDwZongziRuleTipWnd, "ScrollView",			UIScrollView)
RegistChildComponent(CLuaDwZongziRuleTipWnd, "Indicator",			UIScrollViewIndicator)


function CLuaDwZongziRuleTipWnd:Init()
	self:ParseRuleText()
end

function CLuaDwZongziRuleTipWnd:OnEnable()
end


function CLuaDwZongziRuleTipWnd:ParseRuleText()
	Extensions.RemoveAllChildren(self.Table.transform)
	local msg =	MessageMgr.Inst:FormatMessage("DwZongzi_Rule",{});
    if System.String.IsNullOrEmpty(msg) then
        return
    end

    local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(msg)

    if info == nil then
        return
    end

    if info.titleVisible then
        local titleGo = CUICommonDef.AddChild(self.Table.gameObject, self.TitleTemplate)
        titleGo:SetActive(true)
        CommonDefs.GetComponent_GameObject_Type(titleGo, typeof(CTipTitleItem)):Init(info.title)
    end

    do
        local i = 0
        while i < info.paragraphs.Count do
            local paragraphGo = CUICommonDef.AddChild(self.Table.gameObject, self.ParagraphTemplate)
            paragraphGo:SetActive(true)
			local tip = CommonDefs.GetComponent_GameObject_Type(paragraphGo, typeof(CTipParagraphItem))
            tip:Init(info.paragraphs[i], 4294967295)
            i = i + 1
        end
    end

    self.ScrollView:ResetPosition()
    self.Indicator:Layout()
    self.Table:Reposition()
end
