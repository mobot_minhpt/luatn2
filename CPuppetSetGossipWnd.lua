-- Auto Generated!!
local CHousePuppetMgr = import "L10.Game.CHousePuppetMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CPuppetSetGossipWnd = import "L10.UI.CPuppetSetGossipWnd"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local DelegateFactory = import "DelegateFactory"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local HousePuppet_Setting = import "L10.Game.HousePuppet_Setting"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
CPuppetSetGossipWnd.m_Init_CS2LuaHook = function (this) 

    UIEventListener.Get(this.cancelBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        this:Close()
    end)

    UIEventListener.Get(this.backBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        this:Close()
    end)

    this.moneyCtrl:SetCost(HousePuppet_Setting.GetData().PuppetGossipMoney)
    this.moneyCtrl:SetType(EnumMoneyType.YinLiang, EnumPlayScoreKey.NONE, true)

    UIEventListener.Get(this.submitBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        if this.moneyCtrl.moneyEnough then
            local inputtext = CWordFilterMgr.Inst:DoFilterOnSendPersonalSpace(this.textInput.value, true)
            if not System.String.IsNullOrEmpty(inputtext) then
                if CommonDefs.StringLength(inputtext) <= CPuppetSetGossipWnd.MaxLength then
                    local engineId = CHousePuppetMgr.Inst.SaveInteractPuppetEngineId
                    local idex = CommonDefs.DictGetValue(CHousePuppetMgr.Inst.EngineToIdxDic, typeof(UInt32), engineId)
                    local index = CHousePuppetMgr.Inst.SaveGossipIndex + 1
                    Gac2Gas.SaveFriendPuppetGossip(idex, index, inputtext)
                    this:Close()
                else
                    g_MessageMgr:ShowMessage("CUSTOM_STRING2", CPuppetSetGossipWnd.ExceedMaxLength)
                end
            end
        else
            g_MessageMgr:ShowMessage("SET_GOSSIP_NO_MONEY")
        end
    end)
end
