require("3rdParty/ScriptEvent")
require("common/common_include")

local Gac2Gas = import "L10.Game.Gac2Gas"
local CUIManager = import "L10.UI.CUIManager"
local CIndirectUIResources = import "L10.UI.CIndirectUIResources"
local CommonDefs = import "L10.Game.CommonDefs"
local LocalString = import "LocalString"
local MessageMgr = import "L10.Game.MessageMgr"
local CValentineMgr = import "L10.UI.CValentineMgr"

CLuaValentineVoiceLoveWnd=class()

RegistClassMember(CLuaValentineVoiceLoveWnd,"CloseButton")
RegistClassMember(CLuaValentineVoiceLoveWnd,"ApplyButton")
RegistClassMember(CLuaValentineVoiceLoveWnd,"BossLeftLabel")
RegistClassMember(CLuaValentineVoiceLoveWnd,"MoreInfo")
RegistClassMember(CLuaValentineVoiceLoveWnd,"TimeLabel")
RegistClassMember(CLuaValentineVoiceLoveWnd,"RecommemdLabel")


function CLuaValentineVoiceLoveWnd:Init()

	
	local closeFunc = function(go)
		CUIManager.CloseUI("ValentineVoiceLoveWnd")
	end
	CommonDefs.AddOnClickListener(self.CloseButton,DelegateFactory.Action_GameObject(closeFunc),false)
	
	local applyFunc = function(button)
		Gac2Gas.RequestTeleportToVoiceLoveScene(CValentineMgr.Inst.BossCount > 0, '', 0, 0)
	end
	CommonDefs.AddOnClickListener(self.ApplyButton.gameObject,DelegateFactory.Action_GameObject(applyFunc),false)
	
	if CValentineMgr.Inst.BossCount > 0 then
		self.ApplyButton.Text = LocalString.GetString("击退胖大海")
	else
		self.ApplyButton.Text = LocalString.GetString("前往西子湖畔")
	end
	
	local moreInfoFunc = function(go)
		MessageMgr.Inst:ShowMessage("VALENTINE_VOICE_LOVE_INTRO", { })
	end
	CommonDefs.AddOnClickListener(self.MoreInfo,DelegateFactory.Action_GameObject(moreInfoFunc),false)
	
	local str = SafeStringFormat3(LocalString.GetString("[E0F8FF]胖大海数量[-]： [00FF00]%d[-]"), tonumber(CValentineMgr.Inst.BossCount) )
	self.BossLeftLabel.text = str
	
	local timeStr = MessageMgr.Inst:FormatMessage("VALENTINE_VOICE_LOVE_TIME", {})
	local recommendStr = MessageMgr.Inst:FormatMessage("VALENTINE_VOICE_LOVE_RECOMMEND", {})
	
	if timeStr and timeStr ~= '' then
		self.TimeLabel.text = timeStr
	end
	
	if recommendStr and recommendStr ~= '' then
		self.RecommemdLabel = recommendStr
	end
	
end

return CLuaValentineVoiceLoveWnd
