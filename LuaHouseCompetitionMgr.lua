local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CHouseCompetitionMgr = import "L10.Game.CHouseCompetitionMgr"
local CClientHouseMgr=import "L10.Game.CClientHouseMgr"
local EnumHouseCompetitionStage=import "L10.Game.EnumHouseCompetitionStage"

CLuaHouseCompetitionMgr = {}
CLuaHouseCompetitionMgr.HouseCountInSingleCommunity = 30
CLuaHouseCompetitionMgr.HouseList = {}
CLuaHouseCompetitionMgr.eHouseCompetitionEnterPlayGetVoteTimes = 79
CLuaHouseCompetitionMgr.sMaxUploadTimes = 100

g_ScriptEvent:RemoveListener("PlayerLogin", CLuaHouseCompetitionMgr, "OnPlayerLogin")
g_ScriptEvent:AddListener("PlayerLogin", CLuaHouseCompetitionMgr, "OnPlayerLogin")
function CLuaHouseCompetitionMgr.OnPlayerLogin(self)
    CHouseCompetitionMgr.Inst:SetCurrentStatge(EnumHouseCompetitionStage.eUnknown)
end

function CLuaHouseCompetitionMgr.CheckMirrorServer()
    local serverId =  CClientMainPlayer.Inst and CClientMainPlayer.Inst.MyServerId or 0
    CHouseCompetitionMgr.sb_IsInMirrorServer = serverId==9
end

function CLuaHouseCompetitionMgr.CanShareHouseCompetition()
    local bInOwnHouseAndHouseCompetition = (CHouseCompetitionMgr.Inst:GetCurrentStage() == EnumHouseCompetitionStage.eChusai or CHouseCompetitionMgr.sb_IsInMirrorServer) 
                                            and CClientHouseMgr.Inst:IsInOwnHouse()
    return bInOwnHouseAndHouseCompetition
end
