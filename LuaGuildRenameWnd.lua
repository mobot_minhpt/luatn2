local MessageWndManager = import "L10.UI.MessageWndManager"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local Color = import "UnityEngine.Color"
local CGuildMgr = import "L10.Game.CGuildMgr"
local CButton=import "L10.UI.CButton"
local Constants = import "L10.Game.Constants"


CLuaGuildRenameWnd = class()
RegistClassMember(CLuaGuildRenameWnd,"closeBtn")
RegistClassMember(CLuaGuildRenameWnd,"oldNameLabel1")
RegistClassMember(CLuaGuildRenameWnd,"oldNameLabel2")
RegistClassMember(CLuaGuildRenameWnd,"oldNameLabel3")
RegistClassMember(CLuaGuildRenameWnd,"oldNameLabel4")
RegistClassMember(CLuaGuildRenameWnd,"lastRenameTimeLabel")
RegistClassMember(CLuaGuildRenameWnd,"nameInput")
RegistClassMember(CLuaGuildRenameWnd,"costLabel")
RegistClassMember(CLuaGuildRenameWnd,"renameBtn")
RegistClassMember(CLuaGuildRenameWnd,"cancleBtn")

function CLuaGuildRenameWnd:Awake()
    self.closeBtn = self.transform:Find("Wnd_Bg_Secondary_2/CloseButton").gameObject
    self.oldNameLabel1 = self.transform:Find("Anchor/OldNameLabel1"):GetComponent(typeof(UILabel))
    self.oldNameLabel2 = self.transform:Find("Anchor/OldNameLabel2"):GetComponent(typeof(UILabel))
    self.oldNameLabel3 = self.transform:Find("Anchor/OldNameLabel3"):GetComponent(typeof(UILabel))
    self.oldNameLabel4 = self.transform:Find("Anchor/OldNameLabel4"):GetComponent(typeof(UILabel))
    self.lastRenameTimeLabel = self.transform:Find("Anchor/LastRenameTimeLabel"):GetComponent(typeof(UILabel))
    self.nameInput = self.transform:Find("Anchor/NameInput"):GetComponent(typeof(UIInput))
    self.costLabel = self.transform:Find("Anchor/CostLabel"):GetComponent(typeof(UILabel))
    self.renameBtn = self.transform:Find("Anchor/RenameBtn"):GetComponent(typeof(CButton))
    self.cancleBtn = self.transform:Find("Anchor/CancleBtn").gameObject

    UIEventListener.Get(self.renameBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnClickRenameButton(go)
    end)
    UIEventListener.Get(self.cancleBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.CloseUI(CUIResources.GuildRenameWnd)
    end)
end
function CLuaGuildRenameWnd:OnClickRenameButton( go) 
    local newName = self.nameInput.value
    newName = Extensions.RemoveBlank(newName)
    if Extensions.IsNumeric(newName) then
        g_MessageMgr:ShowMessage("Guild_Name_Numeric")
        return
    end
    if CommonDefs.IsSeaMultiLang() then
        local len = CommonDefs.StringLength(newName)
        -- 帮会名STRING(40)
        if len < Constants.SeaMinCharLimitForGuildName or len > Constants.SeaMaxCharLimitForGuildName then
            g_MessageMgr:ShowMessage("SEA_GUILD_INPUT_NAME_LENGTH", Constants.SeaMinCharLimitForGuildName, Constants.SeaMaxCharLimitForGuildName)
            return
        end
    else
        local len = CUICommonDef.GetStrByteLength(newName)
        if len < 4 or len > 10 then
            g_MessageMgr:ShowMessage("CUSTOM_STRING", LocalString.GetString("帮会名称为2~5个汉字"))
            return
        end
    end
    if CGuildMgr.Inst.m_GuildInfo ~= nil then
        if (newName == CGuildMgr.Inst.m_GuildInfo.Name) then
            g_MessageMgr:ShowMessage("CUSTOM_STRING", LocalString.GetString("和原来的帮会名称一样"))
            return
        end
    end
    if not CWordFilterMgr.Inst:CheckName(newName) then
        g_MessageMgr:ShowMessage("Guild_Name_Violation")
        return
    end
    MessageWndManager.ShowOKCancelMessage(LocalString.GetString("确定修改帮会名称？"), DelegateFactory.Action(function ()
        self:RequestRename()
    end),nil, nil, nil, false)
end

function CLuaGuildRenameWnd:RequestRename()
    Gac2Gas.RequestOperationInGuild(CClientMainPlayer.Inst.BasicProp.GuildId, "AlterName", self.nameInput.value, "")
end

function CLuaGuildRenameWnd:OnRequestOperationInGuildSucceed( args) 
    local requestType, paramStr = args[0],args[1]
    if (("AlterName") == requestType) then
        CUIManager.CloseUI(CUIResources.GuildRenameWnd)
        if CGuildMgr.Inst.m_GuildInfo ~= nil then
            CGuildMgr.Inst.m_GuildInfo.Name = paramStr
            CGuildMgr.Inst:GetMyGuildInfo()
        end
    end
end
function CLuaGuildRenameWnd:OnEnable( )
	g_ScriptEvent:AddListener("RequestOperationInGuildSucceed", self, "OnRequestOperationInGuildSucceed")
    -- 曾用名
    self.oldNameLabel1.text = LocalString.GetString("无")
    self.oldNameLabel2.gameObject:SetActive(false)
    self.oldNameLabel3.gameObject:SetActive(false)
    self.oldNameLabel4.gameObject:SetActive(false)

    local twoline = false    
    if CGuildMgr.Inst.m_GuildInfo == nil then
        return
    end

    local name = CGuildMgr.Inst.m_GuildInfo.NamesUsed[1]
    if name and name~="" then
        self.oldNameLabel1.text = name
        self.oldNameLabel1.color = Color.white
    else
        self.oldNameLabel1.color = Color.gray
    end
    name = CGuildMgr.Inst.m_GuildInfo.NamesUsed[2]
    if name and name~="" then
        self.oldNameLabel2.text = name
        self.oldNameLabel2.gameObject:SetActive(true)
    end
    name = CGuildMgr.Inst.m_GuildInfo.NamesUsed[3]
    if name and name~="" then
        self.oldNameLabel3.text = name
        self.oldNameLabel3.gameObject:SetActive(true)
        twoline=true
    end
    name = CGuildMgr.Inst.m_GuildInfo.NamesUsed[4]
    if name and name~="" then
        self.oldNameLabel4.text = name
        self.oldNameLabel4.gameObject:SetActive(true)
        twoline=true
    end

    if not twoline then
        local prePos = self.lastRenameTimeLabel.transform.localPosition
        self.lastRenameTimeLabel.transform.localPosition = Vector3(prePos.x,prePos.y+36,0)
        local prePos = self.costLabel.transform.localPosition
        self.costLabel.transform.localPosition =  Vector3(prePos.x,prePos.y+36,0)
    end

    -- 距离上次改名
    local c = Color.green
    local renameDuration = Guild_Setting.GetData().Guild_Change_Name_CD * 24 * 3600
    if CGuildMgr.Inst.m_GuildInfo.NameAlterTime <= 0 then
        self.lastRenameTimeLabel.text = LocalString.GetString("无")
    else
        local nameAlterTime = CServerTimeMgr.ConvertTimeStampToZone8Time(CGuildMgr.Inst.m_GuildInfo.NameAlterTime)
        local serverTime = CServerTimeMgr.Inst:GetZone8Time()
        local duration = CommonDefs.op_Subtraction_DateTime_DateTime(serverTime, nameAlterTime)

        if duration.TotalSeconds < renameDuration then
            c = Color.red
            self.renameBtn.Enabled = false
        end
        self.lastRenameTimeLabel.text = math.floor(duration.TotalDays) .. LocalString.GetString("天")
    end
    self.lastRenameTimeLabel.color = c

    --处理合服逻辑
    if CGuildMgr.Inst.m_GuildName == tostring(CClientMainPlayer.Inst.BasicProp.GuildId) then
        self.costLabel.text = LocalString.GetString("0")
        self.lastRenameTimeLabel.color = Color.green
        self.renameBtn.Enabled = true
    end
end
function CLuaGuildRenameWnd:OnDisable()
	g_ScriptEvent:RemoveListener("RequestOperationInGuildSucceed", self, "OnRequestOperationInGuildSucceed")

end
