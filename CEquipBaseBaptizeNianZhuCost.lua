-- Auto Generated!!
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local AlignType1 = import "L10.UI.CTooltip+AlignType"
local CEquipBaseBaptizeNianZhuCost = import "L10.UI.CEquipBaseBaptizeNianZhuCost"
local CEquipmentBaptizeMgr = import "L10.Game.CEquipmentBaptizeMgr"
local CEquipmentProcessMgr = import "L10.Game.CEquipmentProcessMgr"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local DelegateFactory = import "DelegateFactory"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local EventDelegate = import "EventDelegate"
local Item_Item = import "L10.Game.Item_Item"
local Mall_LingYuMall = import "L10.Game.Mall_LingYuMall"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CEquipBaseBaptizeNianZhuCost.m_Awake_CS2LuaHook = function (this) 
    this.autoCostJade.value = false
    this.lingyuCost.gameObject:SetActive(false)

    CommonDefs.ListAdd(this.autoCostJade.onChange, typeof(EventDelegate), CreateFromClass(EventDelegate, DelegateFactory.Callback(function () 
        this:UpdateShowState()
    end)))

    UIEventListener.Get(this.nianZhuGo).onClick = MakeDelegateFromCSFunction(this.ClickNianZhu, VoidDelegate, this)
end
CEquipBaseBaptizeNianZhuCost.m_UpdateShowState_CS2LuaHook = function (this) 
    if not this.autoCostJade.value then
        this.lingyuCost.gameObject:SetActive(false)
        this.nianZhuGo:SetActive(true)
    else
        if this.countUpdate.count < this.nianzhuCostNum then
            this.lingyuCost.gameObject:SetActive(true)
            this.nianZhuGo:SetActive(false)
        else
            this.lingyuCost.gameObject:SetActive(false)
            this.nianZhuGo:SetActive(true)
        end
    end
end
CEquipBaseBaptizeNianZhuCost.m_SetBaseInfo_CS2LuaHook = function (this) 
    local equip = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
    local equipItem = CEquipmentProcessMgr.Inst.BaptizingEquipment
    local cost = CEquipmentProcessMgr.Inst:GetBaptizeCost(equip)
    if cost ~= nil then
        this.nianzhuCostNum = cost.PropertyCost[1]

        this.TemplateId = cost.PropertyCost[0]
        local canUseBindLingyu = CShopMallMgr.CanUseBindJadeBuy(this.TemplateId)
        this.lingyuCost:SetType(canUseBindLingyu and EnumMoneyType.LingYu_WithBind or EnumMoneyType.LingYu, EnumPlayScoreKey.NONE, true)

        local costJade = Mall_LingYuMall.GetData(cost.PropertyCost[0])
        local val = (costJade.Jade * cost.PropertyCost[1])
        this.lingyuCost:SetCost(val)

        this.nianzhuTemplateId = cost.PropertyCost[0]
        local template = Item_Item.GetData(cost.PropertyCost[0])
        if template ~= nil then
            this.propertyCostMatNameLabel.text = template.Name
            this.propertyCostMatIcon:LoadMaterial(template.Icon)
        end

        --#region 设置银两
        local commonItem = CItemMgr.Inst:GetById(equip.itemId)
        this.yinliangCost.gameObject:SetActive(true)
        --看是否绑定
        if commonItem.IsBinded then
            this.yinliangCost:SetCost(0)
        else
            local tc = EquipmentTemplate_Equip.GetData(commonItem.TemplateId).TC
            local yinliang = CEquipmentBaptizeMgr.Inst:GetBaptizeWordYinLiangCost(tc, EnumToInt(commonItem.Equip.Color))
            this.yinliangCost:SetCost(yinliang)
        end
        --#endregion


        --更新数量
        this.countUpdate.templateId = cost.PropertyCost[0]
        this.countUpdate.format = "{0}/" .. tostring(cost.PropertyCost[1])
        --每次消耗的数量
        this.countUpdate.onChange = DelegateFactory.Action_int(function (count) 
            if count >= cost.PropertyCost[1] then
                this.matEnough = true
                this.countUpdate.format = "[00ff00]{0}[-]/" .. tostring(cost.PropertyCost[1])
                --每次消耗的数量
                if this.getGo ~= nil then
                    this.getGo:SetActive(false)
                end
            else
                this.matEnough = false
                this.countUpdate.format = "[ff0000]{0}[-]/" .. tostring(cost.PropertyCost[1])
                --每次消耗的数量
                if this.getGo ~= nil then
                    this.getGo:SetActive(true)
                end
            end
            this:UpdateShowState()
        end)
        this.countUpdate:UpdateCount()
    else
        this.getGo:SetActive(false)
        this.propertyCostMatNameLabel.text = ""
        this.countUpdate.countLabel.text = ""
    end
end
CEquipBaseBaptizeNianZhuCost.m_ClickNianZhu_CS2LuaHook = function (this, go) 
    if this.matEnough then
        if this.nianzhuTemplateId > 0 then
            CItemInfoMgr.ShowLinkItemTemplateInfo(this.nianzhuTemplateId, false, nil, AlignType.Default, 0, 0, 0, 0)
        end
    else
        --如果没有的时候 点击的时候显示获取途径
        CItemAccessListMgr.Inst:ShowItemAccessInfo(this.nianzhuTemplateId, false, this.transform, AlignType1.Right)
    end
end
