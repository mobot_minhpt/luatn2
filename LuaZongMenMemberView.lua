local UIBasicSprite = import "UIBasicSprite"
local QnLabel = import "L10.UI.QnLabel"
local Profession = import "L10.Game.Profession"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
local AlignType1 = import "CPlayerInfoMgr+AlignType"
local CMiniChatMgr = import "L10.UI.CMiniChatMgr"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local CGuildSortButton = import "L10.UI.CGuildSortButton"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local Constants = import "L10.Game.Constants"
local UITabBar = import "L10.UI.UITabBar"
local Object = import "System.Object"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"

LuaZongMenMemberView = class()

RegistChildComponent(LuaZongMenMemberView,"m_ChangePageButton","ChangePageButton", GameObject)
RegistChildComponent(LuaZongMenMemberView,"m_HeadTable1","HeadTable1", GameObject)
RegistChildComponent(LuaZongMenMemberView,"m_HeadTable2","HeadTable2", GameObject)
RegistChildComponent(LuaZongMenMemberView,"m_QnTableViewMember","QnTableViewMember", QnTableView)
RegistChildComponent(LuaZongMenMemberView,"m_JoinSetting","JoinSetting", UISprite)
RegistChildComponent(LuaZongMenMemberView,"m_PromoteSetting","PromoteSetting", UISprite)
RegistChildComponent(LuaZongMenMemberView,"m_LeaveBtn","LeaveBtn", GameObject)
RegistChildComponent(LuaZongMenMemberView,"m_SendMsgsBtn","SendMsgsBtn", GameObject)
RegistChildComponent(LuaZongMenMemberView,"m_InviteBtn","InviteBtn", GameObject)
RegistChildComponent(LuaZongMenMemberView,"m_ChangeWeiJieBtn","ChangeWeiJieBtn", GameObject)
RegistChildComponent(LuaZongMenMemberView,"m_HeaderQnRadioBox","HeaderQnRadioBox", QnRadioBox)
RegistChildComponent(LuaZongMenMemberView,"m_MemberNumLabel","MemberNumLabel", UILabel)
RegistChildComponent(LuaZongMenMemberView,"m_FirstItemCenter","FirstItemCenter", GameObject)
RegistChildComponent(LuaZongMenMemberView,"m_TabBar","TabBar", UITabBar)
RegistChildComponent(LuaZongMenMemberView,"m_ButtonView1","ButtonView1", GameObject)
RegistChildComponent(LuaZongMenMemberView,"m_ButtonView2","ButtonView2", GameObject)
RegistChildComponent(LuaZongMenMemberView,"m_ClearBtn","ClearBtn", GameObject)
RegistChildComponent(LuaZongMenMemberView,"m_InviteBtn2","InviteBtn2", GameObject)
RegistChildComponent(LuaZongMenMemberView,"m_HeaderQnRadioBox2","HeaderQnRadioBox2", QnRadioBox)

RegistClassMember(LuaZongMenMemberView,"m_Page")
RegistClassMember(LuaZongMenMemberView,"m_MemberList")
RegistClassMember(LuaZongMenMemberView,"m_CurrentPlayerIndex")
RegistClassMember(LuaZongMenMemberView,"m_PlayerIsZhangMen")
RegistClassMember(LuaZongMenMemberView,"m_PlayerIsZhangLao")
RegistClassMember(LuaZongMenMemberView,"m_isInit")
RegistClassMember(LuaZongMenMemberView,"m_SortFlags")
RegistClassMember(LuaZongMenMemberView,"m_SortFlags2")
RegistClassMember(LuaZongMenMemberView,"m_MembersSortType")
RegistClassMember(LuaZongMenMemberView,"m_MembersSortType2")
RegistClassMember(LuaZongMenMemberView,"m_TabIndex")

function LuaZongMenMemberView:Start()
    self.m_MemberNumLabel.gameObject:SetActive(false)
    self.m_JoinSetting.gameObject:SetActive(false)
    self:InitQnTableViewMember()
    self:ChangePage(1)
    UIEventListener.Get(self.m_ChangePageButton).onClick = DelegateFactory.VoidDelegate(function(go) self:OnClickPageChangeBtn(go) end)
    UIEventListener.Get(self.m_LeaveBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnLeaveBtnClicked(go) end)
    UIEventListener.Get(self.m_SendMsgsBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnSendMsgsBtnClicked(go) end)
    UIEventListener.Get(self.m_InviteBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnInviteBtnClicked(go) end)
    UIEventListener.Get(self.m_ChangeWeiJieBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnChangeWeiJieBtnClicked(go) end)
    UIEventListener.Get(self.m_JoinSetting.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) self:OnJoinSettingClicked(go) end)
    UIEventListener.Get(self.m_PromoteSetting.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) self:OnPromoteSettingClicked(go) end)
    self.m_SortFlags = {-1}
    self.m_SortFlags2 = {-1}
    self.m_MembersSortType = 1
    self.m_TabIndex = 0
    self.m_HeaderQnRadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn, index)
        self:OnHeadQnRadioBoxSelect(btn, index)
    end)
    self.m_ChangeWeiJieBtn:SetActive(false)
    if LuaZongMenMgr.m_IsOpen then
        Gac2Gas.GetSectXinWuInfo()
    end	
    self.m_isInit = true
    if LuaZongMenMgr.m_OpenNewSystem then
        if self.m_TabBar then
            self.m_TabBar.OnTabChange = CommonDefs.CombineListner_Action_GameObject_int(self.m_TabBar.OnTabChange, DelegateFactory.Action_GameObject_int(function(go, index)
                self:OnTabChange(go, index)
            end), true)
            self.m_TabBar:ChangeTab(LuaZongMenMgr.m_ZongMenMemberViewTabIndex, false)
            local btn = self.m_TabBar:GetTabGoByIndex(1)
            btn.transform:Find("RedDot").gameObject:SetActive(false)
        end
        if self.m_ClearBtn then
            UIEventListener.Get(self.m_ClearBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) self:OnClearBtnClicked(go) end)
        end
        if self.m_InviteBtn2 then
            UIEventListener.Get(self.m_InviteBtn2.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) self:OnInviteBtn2Clicked(go) end)
        end
        if self.m_HeaderQnRadioBox2 then
            self.m_HeaderQnRadioBox2.OnSelect = DelegateFactory.Action_QnButton_int(function(btn, index)
                self:OnHeadQnRadioBoxSelect2(btn, index)
            end)
        end
    else
        if CClientMainPlayer.Inst then
            Gac2Gas.QuerySectInfo(CClientMainPlayer.Inst.BasicProp.SectId, "member", 0)
        end
    end
end

function LuaZongMenMemberView:OnClearBtnClicked(go)
    if CClientMainPlayer.Inst then
        Gac2Gas.RequestCleanSectApply_Ite(CClientMainPlayer.Inst.BasicProp.SectId)
    end
end

function LuaZongMenMemberView:OnInviteBtn2Clicked(go)
    if not self.m_CurrentPlayerIndex then
        return
    end
    local memberInfo = self.m_MemberList[self.m_CurrentPlayerIndex+1]
    if CClientMainPlayer.Inst then
        Gac2Gas.RequestAcceptSectApply_Ite(memberInfo.PlayerId)
    end
end

function LuaZongMenMemberView:OnTabChange(go, index)
    self.m_TabIndex = index
    self.m_ButtonView1.gameObject:SetActive(index == 0)
    self.m_ButtonView2.gameObject:SetActive(index == 1)
    self.m_ChangePageButton.gameObject:SetActive(index == 0)
    self.m_MemberList = {}
    self.m_QnTableViewMember:ReloadData(true, true)
    self.m_HeaderQnRadioBox.gameObject:SetActive(index == 0)
    self.m_HeaderQnRadioBox2.gameObject:SetActive(index == 1)
    self.m_InviteBtn2.gameObject:SetActive(false)
    if CClientMainPlayer.Inst then
        Gac2Gas.QuerySectInfo(CClientMainPlayer.Inst.BasicProp.SectId, (index == 0) and "member" or "applylist", 0)
    end
end

function LuaZongMenMemberView:OnPromoteSettingClicked(go)
    if CClientMainPlayer.Inst then
        Gac2Gas.RequestSetSectEnableAd(CClientMainPlayer.Inst.BasicProp.SectId, self.m_PromoteSetting.spriteName ~= "common_thumb_open")
    end
end

function LuaZongMenMemberView:OnJoinSettingClicked(go)
    if CClientMainPlayer.Inst and self.m_PlayerIsZhangMen then
        if self.m_JoinSetting.spriteName == "common_thumb_open" then
            local msg = g_MessageMgr:FormatMessage("ZongMen_SetSectHideFromIdSearch_Confirm",Menpai_Setting.GetData("CannotSearchByIdCD").Value)
            local okAction = DelegateFactory.Action(function ()
                Gac2Gas.RequestSetSectHideFromIdSearch(CClientMainPlayer.Inst.BasicProp.SectId, self.m_JoinSetting.spriteName ~= "common_thumb_open")
            end) 
            MessageWndManager.ShowOKCancelMessage(msg,okAction,nil,LocalString.GetString("确定"), LocalString.GetString("取消"),false)
        else
            Gac2Gas.RequestSetSectHideFromIdSearch(CClientMainPlayer.Inst.BasicProp.SectId, self.m_JoinSetting.spriteName ~= "common_thumb_open")
        end
    end
end

function LuaZongMenMemberView:OnHeadQnRadioBoxSelect(btn,index)
    local sortButton = TypeAs(btn, typeof(CGuildSortButton))
    if self.m_SortFlags[index + 1] == nil then 
        self.m_SortFlags[index + 1] = -1 
    else
        self.m_SortFlags[index + 1] = -self.m_SortFlags[index + 1]
    end
    sortButton:SetSortTipStatus(self.m_SortFlags[index + 1] < 0)
    self:SortMembers(index + 1)
    self.m_QnTableViewMember:ReloadData(true, true)
end

function LuaZongMenMemberView:OnHeadQnRadioBoxSelect2(btn,index)
    local sortButton = TypeAs(btn, typeof(CGuildSortButton))
    if self.m_SortFlags2[index + 1] == nil then 
        self.m_SortFlags2[index + 1] = -1 
    else
        self.m_SortFlags2[index + 1] = -self.m_SortFlags2[index + 1]
    end
    sortButton:SetSortTipStatus(self.m_SortFlags2[index + 1] < 0)
    self:SortMembers2(index + 1)
    self.m_QnTableViewMember:ReloadData(true, true)
end

function LuaZongMenMemberView:OnLeaveBtnClicked(go)
    local zongMenName = LuaZongMenMgr.m_ZongMenProp and LuaZongMenMgr.m_ZongMenProp.Name.StringData or nil 
    local msg = g_MessageMgr:FormatMessage(self.m_PlayerIsZhangMen and "ZhangMen_LeaveZongMen_Confirm" or "LeaveZongMen_Confirm",zongMenName)
    local okAction = self.m_PlayerIsZhangMen and DelegateFactory.Action(function ()
       CUIManager.ShowUI(CLuaUIResources.ZongMenWeiJieWnd)
    end) or DelegateFactory.Action(function ()
        if CClientMainPlayer.Inst then
            Gac2Gas.RequestQuitSect(CClientMainPlayer.Inst.BasicProp.SectId)
        end
    end)
    local cancelAction = self.m_PlayerIsZhangMen and DelegateFactory.Action(function ()
        if CClientMainPlayer.Inst then
            Gac2Gas.RequestQuitSect(CClientMainPlayer.Inst.BasicProp.SectId)
        end
     end) or nil
     MessageWndManager.ShowOKCancelMessage(msg,okAction,cancelAction,self.m_PlayerIsZhangMen and LocalString.GetString("前往任免") or LocalString.GetString("确定"), self.m_PlayerIsZhangMen and LocalString.GetString("直接脱离") or LocalString.GetString("取消"),false)
end

function LuaZongMenMemberView:OnSendMsgsBtnClicked(go)
    if self.m_PlayerIsZhangMen or self.m_PlayerIsZhangLao then
        CMiniChatMgr.Show(LocalString.GetString("宗派群发"), 30 , CMiniChatMgr.ChatType.ZongMen, DelegateFactory.Action_MiniChatCallBackInfo(function (data)
            local msg = data.msg
            if not msg or msg=="" then
                return
            end
            local ret = CWordFilterMgr.Inst:DoFilterOnSendWithoutVoiceCheck(msg, true)
            msg = ret.msg
            if msg == nil then
                return
            end
            if CClientMainPlayer.Inst then
                -- todo 是否置顶
                local bTopHighline = false
                -- 置顶的服务端会下发 BroadcastSectMessageTopHighline
                Gac2Gas.RequestBatchSendSectMessage(CClientMainPlayer.Inst.BasicProp.SectId, msg, ret.shouldBeIgnore and true or false, bTopHighline)
            end
        end))
    else
        g_MessageMgr:ShowMessage("Only_ZhangMen_ZhangLao_Can_Operate")
    end
end

function LuaZongMenMemberView:OnInviteBtnClicked(go)
    CUIManager.ShowUI(CLuaUIResources.InviteToJoinZongMenWnd)
end

function LuaZongMenMemberView:OnChangeWeiJieBtnClicked(go)
    local msg = g_MessageMgr:FormatMessage(self.m_PlayerIsZhangMen and "ZhangMen_ChangeWeiJie" or "ZhangLao_ChangeWeiJie")
    local okAction = self.m_PlayerIsZhangMen and DelegateFactory.Action(function ()
        CUIManager.ShowUI(CLuaUIResources.ZongMenWeiJieWnd)
    end) or DelegateFactory.Action(function ()
        if CClientMainPlayer.Inst then
            Gac2Gas.RequestResignSectOffice(CClientMainPlayer.Inst.BasicProp.SectId)
        end
    end)
    local cancelAction = self.m_PlayerIsZhangMen and DelegateFactory.Action(function ()
        if CClientMainPlayer.Inst then
            Gac2Gas.RequestResignSectOffice(CClientMainPlayer.Inst.BasicProp.SectId)
        end
     end) or DelegateFactory.Action(function ()
         
     end)
     MessageWndManager.ShowOKCancelMessage(msg,okAction,cancelAction,self.m_PlayerIsZhangMen and LocalString.GetString("前往任免") or LocalString.GetString("确定"), self.m_PlayerIsZhangMen and LocalString.GetString("直接辞位") or LocalString.GetString("取消"),false)
end

function LuaZongMenMemberView:OnEnable()
    g_ScriptEvent:AddListener("OnSectApplyerInfoResult", self, "OnSectApplyerInfoResult")  
    g_ScriptEvent:AddListener("OnSectMemberInfoResult", self, "OnSectMemberInfoResult")  
    g_ScriptEvent:AddListener("OnPlayerSectSpeakStatusResult", self, "OnPlayerSectSpeakStatusResult")  
    g_ScriptEvent:AddListener("OnReplyBatchChangeSectOffice", self, "OnReplyBatchChangeSectOffice")
    g_ScriptEvent:AddListener("OnReplySetSectOffice", self, "OnReplySetSectOffice")
    g_ScriptEvent:AddListener("OnSyncSectSetHideFromIdSearch", self, "OnSyncSectSetHideFromIdSearch")
    g_ScriptEvent:AddListener("OnSyncSectOpenAd", self, "OnSyncSectOpenAd")
    g_ScriptEvent:AddListener("OnSyncSetSectMemberSpeakStatusResult", self, "OnSyncSetSectMemberSpeakStatusResult")
    g_ScriptEvent:AddListener("OnSyncCleanSectApply_Ite", self, "OnSyncCleanSectApply_Ite")
    g_ScriptEvent:AddListener("OnPlayerAcceptSectApplyResult_Ite", self, "OnPlayerAcceptSectApplyResult_Ite")
    g_ScriptEvent:AddListener("OnSyncSectRedAlert", self, "OnSyncSectRedAlert")
    if CClientMainPlayer.Inst and self.m_isInit then
        self.m_InviteBtn2.gameObject:SetActive(false)
        Gac2Gas.QuerySectInfo(CClientMainPlayer.Inst.BasicProp.SectId, (self.m_TabIndex == 0) and "member" or "applylist", 0)
    end
end

function LuaZongMenMemberView:OnDisable()
    g_ScriptEvent:RemoveListener("OnSectApplyerInfoResult", self, "OnSectApplyerInfoResult")  
    g_ScriptEvent:RemoveListener("OnSectMemberInfoResult", self, "OnSectMemberInfoResult")
    g_ScriptEvent:RemoveListener("OnPlayerSectSpeakStatusResult", self, "OnPlayerSectSpeakStatusResult")  
    g_ScriptEvent:RemoveListener("OnReplyBatchChangeSectOffice", self, "OnReplyBatchChangeSectOffice")
    g_ScriptEvent:RemoveListener("OnReplySetSectOffice", self, "OnReplySetSectOffice")
    g_ScriptEvent:RemoveListener("OnSyncSectSetHideFromIdSearch", self, "OnSyncSectSetHideFromIdSearch")
    g_ScriptEvent:RemoveListener("OnSyncSectOpenAd", self, "OnSyncSectOpenAd")
    g_ScriptEvent:RemoveListener("OnSyncSetSectMemberSpeakStatusResult", self, "OnSyncSetSectMemberSpeakStatusResult")
    g_ScriptEvent:RemoveListener("OnSyncCleanSectApply_Ite", self, "OnSyncCleanSectApply_Ite")
    g_ScriptEvent:RemoveListener("OnPlayerAcceptSectApplyResult_Ite", self, "OnPlayerAcceptSectApplyResult_Ite")
    g_ScriptEvent:RemoveListener("OnSyncSectRedAlert", self, "OnSyncSectRedAlert")
    LuaZongMenMgr.m_PlayerId2Forbidden = {}
    LuaZongMenMgr.m_ZongMenMemberViewTabIndex = 0
end

function LuaZongMenMemberView:OnSyncSectRedAlert(sectId, bHasRedAlert)
    if LuaZongMenMgr.m_OpenNewSystem and CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.SectId == sectId then
        local btn = self.m_TabBar:GetTabGoByIndex(1)
        btn.transform:Find("RedDot").gameObject:SetActive(bHasRedAlert)
    end
end

function LuaZongMenMemberView:OnPlayerAcceptSectApplyResult_Ite(bSuccess)
    if bSuccess then
        if CClientMainPlayer.Inst then
            self.m_InviteBtn2.gameObject:SetActive(false)
            Gac2Gas.QuerySectInfo(CClientMainPlayer.Inst.BasicProp.SectId, "applylist", 0)
        end
    end
end

function LuaZongMenMemberView:OnSyncCleanSectApply_Ite()
    local btn = self.m_TabBar:GetTabGoByIndex(1)
    btn.transform:Find("RedDot").gameObject:SetActive(false)
    self.m_InviteBtn2.gameObject:SetActive(false)
    self.m_MemberList = {}
    self.m_QnTableViewMember:ReloadData(true, true)
end

function LuaZongMenMemberView:OnSectApplyerInfoResult(list)
    local btn = self.m_TabBar:GetTabGoByIndex(1)
    LuaZongMenMgr.m_ShowSectRedAlert = false
    btn.transform:Find("RedDot").gameObject:SetActive(false)
    if (self.m_TabIndex == 0) then
        return
    end
    self.m_MemberList = list
    self.m_InviteBtn2.gameObject:SetActive(false)
    self:SortMembers(self.m_MembersSortType2)
    self.m_QnTableViewMember:ReloadData(true, true)
end

function LuaZongMenMemberView:OnSyncSectSetHideFromIdSearch(bOpen)
    self.m_JoinSetting.spriteName = bOpen and "common_thumb_open" or "common_thumb_close"
end

function LuaZongMenMemberView:OnSyncSectOpenAd(bOpen)
    self.m_PromoteSetting.spriteName = bOpen and "common_thumb_open" or "common_thumb_close"
end

function LuaZongMenMemberView:OnSyncSetSectMemberSpeakStatusResult(targetPlayerId, status)
    local index = 0
    for i,data in pairs(self.m_MemberList) do
        if targetPlayerId == data.PlayerId then
            index = i
            break
        end
    end
    if index  ~= 0 then
        self.m_MemberList[index].DisableSpeakStatus = status
        local item = self.m_QnTableViewMember:GetItemAtRow(index - 1)
        if item then
            self:InitItem(item.transform,self.m_MemberList[index])
        end
        --self.m_QnTableViewMember:ReloadData(true, true)
    end
end

function LuaZongMenMemberView:OnReplySetSectOffice(sectId, targetPlayerId,office)
    if office == EnumSectOffice.eZhangMen then
        if CClientMainPlayer.Inst then
            Gac2Gas.QuerySectInfo(CClientMainPlayer.Inst.BasicProp.SectId, "member", 0)
        end
        return
    end
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.SectId == sectId and self.m_MemberList then
        local index = 0
        for i,data in pairs(self.m_MemberList) do
            if targetPlayerId == data.PlayerId then
                index = i
                break
            end
        end
        if index  ~= 0 then
            self.m_MemberList[index].Office = office
            local item = self.m_QnTableViewMember:GetItemAtRow(index - 1)
            if item then
                self:InitItem(item.transform,self.m_MemberList[index])
            end
            --self.m_QnTableViewMember:ReloadData(true, true)
        end
    end

    for _,info in pairs(self.m_MemberList) do
        if CClientMainPlayer.Inst and info.PlayerId == CClientMainPlayer.Inst.Id then
            self.m_PlayerIsZhangMen = info.Office == EnumSectOffice.eZhangMen
            self.m_PlayerIsZhangLao = info.Office == EnumSectOffice.eZhangLao
        end
    end
    self.m_ChangeWeiJieBtn:SetActive(self.m_PlayerIsZhangMen or self.m_PlayerIsZhangLao)
end

function LuaZongMenMemberView:OnReplyBatchChangeSectOffice()
    if CClientMainPlayer.Inst then
        Gac2Gas.QuerySectInfo(CClientMainPlayer.Inst.BasicProp.SectId, "member", 0)
    end
end

function LuaZongMenMemberView:OnSectMemberInfoResult(list,isShowStatusBtn, JoinSettingStatus, jurisdictionToEnanlePromote, promoteSettingStatus)
    self.m_JoinSetting.spriteName = JoinSettingStatus == 0 and "common_thumb_open" or "common_thumb_close"
    self.m_JoinSetting.gameObject:SetActive(isShowStatusBtn == 1 and not LuaZongMenMgr.m_OpenNewSystem)
    self.m_PromoteSetting.gameObject:SetActive(jurisdictionToEnanlePromote == 1)
    self.m_PromoteSetting.spriteName = promoteSettingStatus == 1 and "common_thumb_open" or "common_thumb_close";
    self.m_MemberList = list
    for _,info in pairs(self.m_MemberList) do
        if CClientMainPlayer.Inst and info.PlayerId == CClientMainPlayer.Inst.Id then
            self.m_PlayerIsZhangMen = info.Office == EnumSectOffice.eZhangMen
            self.m_PlayerIsZhangLao = info.Office == EnumSectOffice.eZhangLao
        end
    end
    self.m_ChangeWeiJieBtn:SetActive(self.m_PlayerIsZhangMen or self.m_PlayerIsZhangLao)
    self:SortMembers(self.m_MembersSortType)
    self.m_MemberNumLabel.gameObject:SetActive(true)
    self.m_MemberNumLabel.text = SafeStringFormat("%d/30",#list)
    self.m_QnTableViewMember:ReloadData(true, true)
    if CClientMainPlayer.Inst then
        self:OnSyncSectRedAlert(CClientMainPlayer.Inst.BasicProp.SectId, LuaZongMenMgr.m_ShowSectRedAlert)
    end
end

function LuaZongMenMemberView:InitQnTableViewMember()
    self.m_QnTableViewMember.m_DataSource = DefaultTableViewDataSource.Create2(
            function() 
                return self:NumberOfRows()
            end,
            function(view,row)
                local item = view:GetFromPool(self.m_TabIndex and self.m_TabIndex or 0)
                if self.m_TabIndex == 0 then
                    self:ItemAt(item,row)
                else
                    self:ItemAt2(item,row)
                end
                return item
            end)
    local callback = DelegateFactory.Action_int(function(row) self:OnSelectAtRow(row) end)
    self.m_QnTableViewMember.OnSelectAtRow = callback
end

function LuaZongMenMemberView:NumberOfRows()
    return self.m_MemberList and #self.m_MemberList or 0
end

function LuaZongMenMemberView:ItemAt(item, row)
    if not self.m_MemberList then return end
    local info =self.m_MemberList[row+1]
   --local item = self.m_QnTableViewMember:GetFromPool(0)
    self:InitItem(item.transform,info)
    local default = row % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite
    item:SetBackgroundTexture(default)
end

function LuaZongMenMemberView:ItemAt2(item, row)
    if not self.m_MemberList then return end
    local info = self.m_MemberList[row+1]
    print(row, info)
   --local item = self.m_QnTableViewMember:GetFromPool(0)
    self:InitItem2(item,info)
    local default = row % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite
    item:SetBackgroundTexture(default)
end


function LuaZongMenMemberView:InitItem(transform,info)
    local forbiddenIcon = transform:Find("LabelName/ForbiddenIcon").gameObject
    local labelName = transform:Find("LabelName"):GetComponent(typeof(QnLabel))
    local clsSprite = transform:Find("LabelName/Sprite"):GetComponent(typeof(UISprite))
    local root1 = transform:Find("Root1")
    local root2 = transform:Find("Root2")
    local labelLevel = root1:Find("LabelLevel"):GetComponent(typeof(QnLabel))
    local labelOffice = root1:Find("LabelOffice"):GetComponent(typeof(QnLabel))
    local labelSourCoreLevel = root1:Find("LabelSourCoreLevel"):GetComponent(typeof(QnLabel))
    local labelShengWang = root1:Find("LabelShengWang"):GetComponent(typeof(QnLabel))
    local labelMiShuLevel = root1:Find("LabelMiShuLevel"):GetComponent(typeof(QnLabel))

    local labelShengWang2 = root2:Find("LabelShengWang2"):GetComponent(typeof(QnLabel))
    local labelEquipScore2 = root2:Find("LabelEquipScore2"):GetComponent(typeof(QnLabel))
    local labelOfflineTime = root2:Find("LabelOfflineTime"):GetComponent(typeof(QnLabel))
    local labelJoinTime = root2:Find("LabelJoinTime"):GetComponent(typeof(QnLabel))

    local dateFormat = "yyyy-MM-dd"
    LuaZongMenMgr.m_PlayerId2Forbidden[info.PlayerId] = info.DisableSpeakStatus == 1
    forbiddenIcon:SetActive(LuaZongMenMgr.m_PlayerId2Forbidden[info.PlayerId])
    labelName.Text = info.Name
    clsSprite.spriteName = Profession.GetIconByNumber(info.Class)
    labelLevel.Text = info.XianFanStatus > 0 and SafeStringFormat3("[c][%s]Lv.%d[-][/c]", Constants.ColorOfFeiSheng,  info.Level) or SafeStringFormat3("Lv.%d",  info.Level)
    labelOffice.Text = info.Office == EnumSectOffice.eDiZi and LocalString.GetString("弟子") or (info.Office == EnumSectOffice.eZhangMen and LocalString.GetString("门主") or LocalString.GetString("长老"))
    labelSourCoreLevel.Text = info.LingHeLevel
    labelShengWang.Text = SafeStringFormat3("%d/%d",info.ShengWangWeek,info.ShengWangTotal)
    labelShengWang2.Text = SafeStringFormat3("%d/%d",info.ShengWangWeek,info.ShengWangTotal)
    labelMiShuLevel.Text = info.MiShuLevel < 0 and LocalString.GetString("未知") or info.MiShuLevel
    labelEquipScore2.Text = info.EquipScore
    if info.LastOfflineTime <= 0 then
        labelOfflineTime.Text = LocalString.GetString("在线")
    else
        local offlineTime = CServerTimeMgr.ConvertTimeStampToZone8Time(info.LastOfflineTime)
        labelOfflineTime.Text = ToStringWrap(offlineTime, dateFormat)
    end
    CUICommonDef.SetActive(clsSprite.gameObject, info.LastOfflineTime <= 0, true)
    local joinTime = CServerTimeMgr.ConvertTimeStampToZone8Time(info.JoinTime)
    labelJoinTime.Text = ToStringWrap(joinTime, dateFormat)
    local m_Root1 = transform:Find("Root1").gameObject
    local m_Root2 = transform:Find("Root2").gameObject
    m_Root1:SetActive(self.m_Page == 1)
    m_Root2:SetActive(self.m_Page == 2)
end

function LuaZongMenMemberView:InitItem2(item,info)
    local forbiddenIcon = item.transform:Find("LabelName/ForbiddenIcon").gameObject
    local labelName = item.transform:Find("LabelName"):GetComponent(typeof(QnLabel))
    local clsSprite = item.transform:Find("LabelName/Sprite"):GetComponent(typeof(UISprite))
    local root1 = item.transform:Find("Root1")
    local labelLevel = root1:Find("LabelLevel"):GetComponent(typeof(QnLabel))
    local labelZhuoYaoLevel = root1:Find("LabelZhuoYaoLevel"):GetComponent(typeof(QnLabel))
    local labelEquipScoreLevel = root1:Find("LabelEquipScoreLevel"):GetComponent(typeof(QnLabel))
    --local labelShengWang = root1:Find("LabelShengWang"):GetComponent(typeof(QnLabel))

    local playerId = info.PlayerId
    forbiddenIcon:SetActive(false)
    labelName.Text = info.Name
    clsSprite.spriteName = Profession.GetIconByNumber(info.Class)
    labelLevel.Text = info.XianFanStatus > 0 and SafeStringFormat3("[c][%s]Lv.%d[-][/c]", Constants.ColorOfFeiSheng,  info.Level) or SafeStringFormat3("Lv.%d",  info.Level)
	labelZhuoYaoLevel.Text = info.ZhuoYaoLevel
    labelEquipScoreLevel.Text = info.EquipScore
    --labelShengWang.Text = info.ShengWang
    UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        local dict = CreateFromClass(MakeGenericClass(Dictionary, cs_string, Object))
        CommonDefs.DictSet(dict, typeof(cs_string), "RequestAcceptSectApplyId", typeof(Object), playerId)
        CPlayerInfoMgr.ShowPlayerPopupMenu(playerId, EnumPlayerInfoContext.ZongMenMember, EChatPanel.Undefined, nil, nil, dict, go.transform.position, AlignType1.Default)
    end)
end

function LuaZongMenMemberView:SortMembers(index)
    if nil == self.m_MemberList then
        return
    end
    if LuaZongMenMgr.m_OpenNewSystem and self.m_TabIndex == 1 then
        self:SortMembers2(index)
        return
    end
    self.m_MembersSortType = index
    local flag = self.m_SortFlags[index]
    local myId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
    local function defaultSort(a,b)
        return (a.PlayerId - b.PlayerId) * flag < 0
    end
    table.sort(self.m_MemberList, function(a,b)
        local res = 0
        if myId == a.PlayerId then 
            return true 
        elseif myId == b.PlayerId then 
            return false 
        end
        if index == 1 then
            res = (a.Class - b.Class) * flag
        elseif index == 2 then
            res = (a.Level - b.Level) * flag
        elseif index == 3 then
            res = (a.Office - b.Office) * flag
        elseif index == 4 then
            res = (a.LingHeLevel - b.LingHeLevel) * flag
        elseif index == 5 then
            res = (a.MiShuLevel - b.MiShuLevel) * flag
        elseif index == 6 or index == 7 then
            res = (a.ShengWangWeek - b.ShengWangWeek) * flag
        elseif index == 8 then
            res = (a.EquipScore - b.EquipScore) * flag
        elseif index == 9 then
            res = (a.JoinTime - b.JoinTime) * flag
        elseif index == 10 then
            res = (a.LastOfflineTime - b.LastOfflineTime) * flag
        end
        if res ~= 0 then
            return res >= 0
        end
        return defaultSort(a,b)
    end)
end

function LuaZongMenMemberView:SortMembers2(index)
    self.m_MembersSortType2 = index
    local flag = self.m_SortFlags2[index]
    local myId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
    local function defaultSort(a,b)
        return (a.PlayerId - b.PlayerId) * flag < 0
    end
    table.sort(self.m_MemberList, function(a,b)
        local res = 0
        if myId == a.PlayerId then 
            return true 
        elseif myId == b.PlayerId then 
            return false 
        end
        if not flag then
            return (a.PlayerId - b.PlayerId) > 0
        end
        if index == 1 then
            res = (a.Class - b.Class) * flag
        elseif index == 2 then
            res = (a.Level - b.Level) * flag
        elseif index == 3 then
            res = (a.ZhuoYaoLevel - b.ZhuoYaoLevel) * flag
        elseif index == 4 then
            res = (a.EquipScore - b.EquipScore) * flag
        end
        if res ~= 0 then
            return res >= 0
        end
        return defaultSort(a,b)
    end)
end


function LuaZongMenMemberView:OnSelectAtRow(row)
    self.m_CurrentPlayerIndex = row

    local memberInfo = self.m_MemberList[self.m_CurrentPlayerIndex+1]
    LuaZongMenMgr:ShowMemberViewPlayerPopupMenu(memberInfo)
end

function LuaZongMenMemberView:OnPlayerSectSpeakStatusResult(playerId)
    if not CClientMainPlayer.IsPlayerId(playerId) then
        local pos = Vector3(540, 0)
        pos = self.transform:TransformPoint(pos)
        CPlayerInfoMgr.ShowPlayerPopupMenu(playerId, EnumPlayerInfoContext.ZongMenMember, EChatPanel.Undefined, nil, nil, pos, AlignType1.Default)
    else
        if CClientMainPlayer.Inst and LuaZongMenMgr.m_PlayerId2Forbidden[CClientMainPlayer.Inst.Id] then
            local items = CreateFromClass(MakeGenericClass(List, PopupMenuItemData))
            local item = PopupMenuItemData(LocalString.GetString("解除禁言"),  DelegateFactory.Action_int(function (index) 
                if CClientMainPlayer.Inst then
                    Gac2Gas.RequestSetSectMemberSpeakStatus(CClientMainPlayer.Inst.BasicProp.SectId, CClientMainPlayer.Inst.Id, 0)
                end
            end), false, nil)
            CommonDefs.ListAdd(items, typeof(PopupMenuItemData), item)
            CPopupMenuInfoMgr.ShowPopupMenu(CommonDefs.ListToArray(items), self.m_FirstItemCenter.transform, CPopupMenuInfoMgr.AlignType.Center)
        end
    end
end

function LuaZongMenMemberView:OnClickPageChangeBtn(go)
    self:ChangePage()
end

function LuaZongMenMemberView:ChangePage(index)
    self.m_Page =  index and index or ((self.m_Page and self.m_Page == 1) and 2 or 1)
    self.m_ChangePageButton:GetComponent(typeof(UISprite)).flip =  self.m_Page == 1 and UIBasicSprite.Flip.Nothing or UIBasicSprite.Flip.Horizontally
    self:OnHeaderChangePage()
    self:OnMemberItemChangePage()
end

function LuaZongMenMemberView:OnHeaderChangePage()
    self.m_HeadTable1.gameObject:SetActive(self.m_Page == 1)
    self.m_HeadTable2.gameObject:SetActive(self.m_Page == 2)
end

function LuaZongMenMemberView:OnMemberItemChangePage()
    for i=1,self.m_QnTableViewMember.m_ItemList.Count do
        local item = self.m_QnTableViewMember.m_ItemList[i-1]
        if item then
            local transform = item.transform
            local m_Root1 = transform:Find("Root1").gameObject
            local m_Root2 = transform:Find("Root2").gameObject
            m_Root1:SetActive(self.m_Page == 1)
            m_Root2:SetActive(self.m_Page == 2)
        end
    end
end
