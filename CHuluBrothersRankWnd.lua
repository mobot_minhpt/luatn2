-- Auto Generated!!
local CHuluBrothersRank = import "L10.UI.CHuluBrothersRank"
local CHuluBrothersRankItem = import "L10.UI.CHuluBrothersRankItem"
local CHuluBrothersRankWnd = import "L10.UI.CHuluBrothersRankWnd"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local NGUITools = import "NGUITools"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"

CHuluBrothersRankWnd.m_OnEnable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.m_CloseBtn).onClick = MakeDelegateFromCSFunction(this.OnClickCloseButton, VoidDelegate, this)
    EventManager.AddListenerInternal(EnumEventType.HuluBrothersRankResult, MakeDelegateFromCSFunction(this.OnRankResult, MakeGenericClass(Action1, MakeGenericClass(List, CHuluBrothersRank)), this))
    this.m_RankTemplate:SetActive(false)
    this.m_MyRankItem.gameObject:SetActive(false)
end
CHuluBrothersRankWnd.m_OnRankResult_CS2LuaHook = function (this, rank) 
    if rank.Count >= 1 then
        this.m_MyRankItem.gameObject:SetActive(true)
        this.m_MyRankItem:Init(rank[0])
    end
    Extensions.RemoveAllChildren(this.m_RankGrid.transform)
    do
        local i = 1 local cnt = rank.Count
        while i < cnt do
            local go = NGUITools.AddChild(this.m_RankGrid.gameObject, this.m_RankTemplate)
            local item = CommonDefs.GetComponent_GameObject_Type(go, typeof(CHuluBrothersRankItem))
            if item ~= nil then
                go:SetActive(true)
                item:Init(rank[i])
            end
            i = i + 1
        end
    end
    this.m_RankGrid:Reposition()
end
