require("common/common_include")

LuaMengHuaLuStartWnd = class()

RegistChildComponent(LuaMengHuaLuStartWnd, "ContentLabel", UILabel)
RegistChildComponent(LuaMengHuaLuStartWnd, "RankBtn", GameObject)
RegistChildComponent(LuaMengHuaLuStartWnd, "EnterBtn", GameObject)

function LuaMengHuaLuStartWnd:Init()
	local text = g_MessageMgr:FormatMessage("MENGHUALU_DESC")
	self.ContentLabel.text = text

	local onRankBtnClicked = function (go)
		self:OnRankBtnClicked(go)
	end
	CommonDefs.AddOnClickListener(self.RankBtn, DelegateFactory.Action_GameObject(onRankBtnClicked), false)

	local onEnterBtnClicked = function (go)
		self:OnEnterBtnClicked(go)
	end
	CommonDefs.AddOnClickListener(self.EnterBtn, DelegateFactory.Action_GameObject(onEnterBtnClicked), false)
end

function LuaMengHuaLuStartWnd:OnRankBtnClicked(go)
	CUIManager.ShowUI(CLuaUIResources.MengHualuRankWnd)
end

function LuaMengHuaLuStartWnd:OnEnterBtnClicked(go)
	LuaMengHuaLuMgr.StartMengHuaLu()
	CUIManager.CloseUI(CLuaUIResources.MengHuaLuStartWnd)
end

function LuaMengHuaLuStartWnd:OnEnable()
	
end

function LuaMengHuaLuStartWnd:OnDisable()
	
end

return LuaMengHuaLuStartWnd