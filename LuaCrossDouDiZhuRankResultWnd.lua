CLuaCrossDouDiZhuRankResultWnd = class()


CLuaCrossDouDiZhuRankResultWnd.m_Rank = 0
CLuaCrossDouDiZhuRankResultWnd.m_Score = 0
function CLuaCrossDouDiZhuRankResultWnd:Init()
    local scoreLabel = self.transform:Find("ScoreLabel"):GetComponent(typeof(UILabel))
    scoreLabel.text = tostring(CLuaCrossDouDiZhuRankResultWnd.m_Score)

    local rankLabel = self.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
    rankLabel.text = tostring(CLuaCrossDouDiZhuRankResultWnd.m_Rank)
end
