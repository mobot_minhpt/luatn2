local DelegateFactory        = import "DelegateFactory"
local CQnReturnAwardTemplate = import "L10.UI.CQnReturnAwardTemplate"
local Item_Item              = import "L10.Game.Item_Item"
local CMessageTipMgr         = import "L10.UI.CMessageTipMgr"
local CTipParagraphItem      = import "L10.UI.CTipParagraphItem"

LuaWorldCup2022GJZLEnterWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaWorldCup2022GJZLEnterWnd, "ruleTable")
RegistClassMember(LuaWorldCup2022GJZLEnterWnd, "paragraphTemplate")
RegistClassMember(LuaWorldCup2022GJZLEnterWnd, "awardTemplate")
RegistClassMember(LuaWorldCup2022GJZLEnterWnd, "times")

function LuaWorldCup2022GJZLEnterWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self:InitUIComponents()
    self:InitEventListener()
end

-- 初始化UI组件
function LuaWorldCup2022GJZLEnterWnd:InitUIComponents()
    local anchor = self.transform:Find("Anchor")
    self.ruleTable = anchor:Find("Rule/ScrollView/Table"):GetComponent(typeof(UITable))
    self.paragraphTemplate = anchor:Find("Rule/ParagraphTemplate").gameObject
    self.awardTemplate = anchor:Find("Rule/AwardTemplate").gameObject
    self.times = anchor:Find("Times"):GetComponent(typeof(UILabel))
    self.paragraphTemplate:SetActive(false)
    self.awardTemplate:SetActive(false)
end

function LuaWorldCup2022GJZLEnterWnd:InitEventListener()
    local anchor = self.transform:Find("Anchor")
    local ruleButton = anchor:Find("RuleButton").gameObject
    local enterButton = anchor:Find("EnterButton").gameObject

    UIEventListener.Get(ruleButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRuleButtonClick()
	end)

    UIEventListener.Get(enterButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnEnterButtonClick()
	end)
end


function LuaWorldCup2022GJZLEnterWnd:Init()
    self:InitRule()
    local times = CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eWorldCup2022GJZL_RewardTime)
    local maxCount = WorldCup2022_GJZLSetting.GetData().WeeklyMaxCount
    self.times.text = SafeStringFormat3(LocalString.GetString("本周参与次数 %d/%d"), times, maxCount)
end

function LuaWorldCup2022GJZLEnterWnd:InitRule()
    Extensions.RemoveAllChildren(self.ruleTable.transform)
    local msg =	g_MessageMgr:FormatMessage("WORLDCUP2022_GUANJUNZHILU_ENTER_TIP")
    if System.String.IsNullOrEmpty(msg) then return end

    local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(msg)
    if not info then return end

    do
		local i = 0
		while i < info.paragraphs.Count do							-- 根据信息创建并显示段落
			local paragraph = CUICommonDef.AddChild(self.ruleTable.gameObject, self.paragraphTemplate) -- 添加段落Go
			paragraph:SetActive(true)
			local tip = CommonDefs.GetComponent_GameObject_Type(paragraph, typeof(CTipParagraphItem))
			tip:Init(info.paragraphs[i], 4294967295)				-- 初始化段落，color = 4294967295（0xFFFFFFFF）
			i = i + 1
		end
	end

    local award = CUICommonDef.AddChild(self.ruleTable.gameObject, self.awardTemplate)
    award:SetActive(true)
    local grid = award.transform:Find("Grid"):GetComponent(typeof(UIGrid))
    local template = award.transform:Find("Template").gameObject
    template:SetActive(false)

    local itemIds = WorldCup2022_GJZLSetting.GetData().RewardItemIds
    for i = 0, itemIds.Length - 1 do
        local child = CUICommonDef.AddChild(grid.gameObject, template)
        child:SetActive(true)
        local qnReturnAward = child.transform:GetComponent(typeof(CQnReturnAwardTemplate))
        qnReturnAward:Init(Item_Item.GetData(itemIds[i]), 0)
    end
    grid:Reposition()

	self.ruleTable:Reposition()
end

--@region UIEvent

function LuaWorldCup2022GJZLEnterWnd:OnRuleButtonClick()
    g_MessageMgr:ShowMessage("WORLDCUP2022_GUANJUNZHILU_RULE")
end

function LuaWorldCup2022GJZLEnterWnd:OnEnterButtonClick()
    Gac2Gas.GJZL_RequestEnterPlay()
end

--@endregion UIEvent
