require("common/common_include")

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local CBaseWnd = import "L10.UI.CBaseWnd"
local CUICommonDef = import "L10.UI.CUICommonDef"
local Extensions = import "Extensions"
local UILabel = import "UILabel"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Color = import "UnityEngine.Color"

LuaLiangHaoMyReqCustomizeCardWnd = class()

RegistClassMember(LuaLiangHaoMyReqCustomizeCardWnd, "m_Template")
RegistClassMember(LuaLiangHaoMyReqCustomizeCardWnd, "m_Table")


function LuaLiangHaoMyReqCustomizeCardWnd:Init()
	self.m_Template = self.transform:Find("Anchor/Item").gameObject
	self.m_Table = self.transform:Find("Anchor/Table"):GetComponent(typeof(UITable))

	self.m_Template:SetActive(false)
	Extensions.RemoveAllChildren(self.m_Table.transform)
	for __,data in pairs(LuaLiangHaoMgr.MyReqCustomizeCardInfo) do
		local go = CUICommonDef.AddChild(self.m_Table.gameObject, self.m_Template)
		go:SetActive(true)
		self:InitItem(go, data)
	end

	self.m_Table:Reposition()
end

function LuaLiangHaoMyReqCustomizeCardWnd:InitItem(itemGo, auctionInfo)
	local m_TitleLabel = itemGo.transform:Find("LiangHaoLabel"):GetComponent(typeof(UILabel))
	local m_OtherPlayerInfoTipLabel = itemGo.transform:Find("OtherPrice/PlayerInfoTipLabel"):GetComponent(typeof(UILabel))
	local m_OtherPlayerInfoLabel = itemGo.transform:Find("OtherPrice/PlayerInfoLabel"):GetComponent(typeof(UILabel))
	local m_OtherPlayerPriceLabel = itemGo.transform:Find("OtherPrice/PriceLabel"):GetComponent(typeof(UILabel))
	local m_MyPriceLabel = itemGo.transform:Find("MyPrice/PriceLabel"):GetComponent(typeof(UILabel))
	local m_AuctionButton = itemGo.transform:Find("AuctionButton").gameObject
	local m_NoNeedAuctionLabel = itemGo.transform:Find("NoNeedAuctionLabel"):GetComponent(typeof(UILabel))
	
	CommonDefs.AddOnClickListener(m_AuctionButton, DelegateFactory.Action_GameObject(function(go) self:OnAuctionButtonClick(auctionInfo.digitNum) end), false)

	m_TitleLabel.text = LuaLiangHaoMgr.GetCardTitleName(auctionInfo.digitNum)
	m_OtherPlayerInfoTipLabel.text =  SafeStringFormat3(LocalString.GetString("第%s名"), auctionInfo.lastBidOrder)
	m_OtherPlayerInfoLabel.text = auctionInfo.roleName
	m_OtherPlayerPriceLabel.text = tostring(auctionInfo.currentPrice)
	m_MyPriceLabel.text = tostring(auctionInfo.myPrice)

	if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == auctionInfo.roleId then
		m_OtherPlayerInfoLabel.color = Color.green
	end

	if auctionInfo.myPos <= auctionInfo.lastBidOrder then
		m_NoNeedAuctionLabel.text = SafeStringFormat3(LocalString.GetString("位于%s/%s名"), auctionInfo.myPos, auctionInfo.lastBidOrder)
		m_AuctionButton:SetActive(false)
	else
		m_NoNeedAuctionLabel.text = ""
		m_AuctionButton:SetActive(true)
	end
end

function LuaLiangHaoMyReqCustomizeCardWnd:OnAuctionButtonClick(digitNum)
	LuaLiangHaoMgr.OpenAuctionCustomizeCardWnd(digitNum)
	self:Close()
end

function LuaLiangHaoMyReqCustomizeCardWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end
