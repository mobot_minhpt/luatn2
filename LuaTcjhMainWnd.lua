--@region import
local GameObject                = import "UnityEngine.GameObject"
local CScheduleMgr              = import "L10.Game.CScheduleMgr"
local CommonDefs                = import "L10.Game.CommonDefs"
local CUITexture				= import "L10.UI.CUITexture"
local CUIManager                = import "L10.UI.CUIManager"
local QnTabButton               = import "L10.UI.QnTabButton"
local DelegateFactory		    = import "DelegateFactory"
local UIEventListener		    = import "UIEventListener"
local UILabel                   = import "UILabel"
local UISprite                  = import "UISprite"
local LocalString               = import "LocalString"
local LuaGameObject             = import "LuaGameObject"

--@endregion

--天成酒壶主界面
CLuaTcjhMainWnd = class()

--@region Regist

--RegistChildComponent

RegistChildComponent(CLuaTcjhMainWnd, "LvLb",		    UILabel)
RegistChildComponent(CLuaTcjhMainWnd, "LvBg",		    CUITexture)
RegistChildComponent(CLuaTcjhMainWnd, "TitleLb",		UILabel)
RegistChildComponent(CLuaTcjhMainWnd, "NeedExpLb",		UILabel)
RegistChildComponent(CLuaTcjhMainWnd, "ExpValueImg",	UISprite)
RegistChildComponent(CLuaTcjhMainWnd, "HelpBtn",	    GameObject)
RegistChildComponent(CLuaTcjhMainWnd, "BuyAdvPassBtn",	GameObject)
RegistChildComponent(CLuaTcjhMainWnd, "RewardBtn",	    GameObject)
RegistChildComponent(CLuaTcjhMainWnd, "TaskBtn",	    GameObject)
RegistChildComponent(CLuaTcjhMainWnd, "RewardTabPanel",	GameObject)
RegistChildComponent(CLuaTcjhMainWnd, "TaskTabPanel",	GameObject)
RegistChildComponent(CLuaTcjhMainWnd, "ExtSprite",	    GameObject)

--RegistClassMember
RegistClassMember(CLuaTcjhMainWnd,  "CurTabIndex")

--@endregion

function CLuaTcjhMainWnd:Awake()

    self.CurTabIndex = -1

    UIEventListener.Get(self.HelpBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnHelpBtnClick()
    end)

    UIEventListener.Get(self.BuyAdvPassBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnBuyAdvPassBtnClick()
    end)

    UIEventListener.Get(self.RewardBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnSelectTab(0)
    end)

    UIEventListener.Get(self.TaskBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnSelectTab(1)
    end)
    self:OnSelectTab(0)
    
    local fx = LuaGameObject.GetChildNoGC(self.BuyAdvPassBtn.transform,"Fx").uiFx
    fx:LoadFx("fx/ui/prefab/UI_zbsjf_goumaianniu.prefab")

    g_ScriptEvent:AddListener("OnTcjhDataChange", self, "OnTcjhDataChange")
end

function CLuaTcjhMainWnd:OnDestroy()
    LuaHanJiaMgr.TcjhMainData = nil
    g_ScriptEvent:RemoveListener("OnTcjhDataChange", self, "OnTcjhDataChange")
end

function CLuaTcjhMainWnd:Init()
    self:OnTcjhDataChange()
end

function CLuaTcjhMainWnd:OnTcjhDataChange()

    local cfg = LuaHanJiaMgr.TcjhMainData.Cfg
    local lv = LuaHanJiaMgr.TcjhMainData.Lv

    local lvexp = self:GetLvExp(lv)
    local curexp =  LuaHanJiaMgr.TcjhMainData.Exp - lvexp  --为当前等级下的经验值，不会超过当前等级所需经验的上上限

    local passtype = LuaHanJiaMgr.TcjhMainData.PassType --0 免费通行证, 1 高级通行证, 2 高级通行证豪华版

    local curcfg = cfg[lv]
    
    if curcfg then
        self.LvLb.text = tostring(lv)
        self.LvBg:LoadMaterial(curcfg.Icon)
        self.TitleLb.text = curcfg.Name
        if lv >= LuaHanJiaMgr.TcjhMainData.MaxLv then
            self.NeedExpLb.text = LocalString.GetString("已满级")
            self.ExpValueImg.fillAmount = 1
        else
            local nextcfg = cfg[lv+1]
            local needexp = nextcfg.NeedJiuQi - curexp
            self.NeedExpLb.text = LocalString.GetString("升至下级还需")..needexp
            self.ExpValueImg.fillAmount = curexp / nextcfg.NeedJiuQi
        end
        
    end

    local lb = LuaGameObject.GetChildNoGC(self.BuyAdvPassBtn.transform,"BuyLabel").label
    if lb then 
        if passtype ~= 0 then --已经购买高级战令
            lb.text = LocalString.GetString("购买酒气等级")
        else
            lb.text = LocalString.GetString("解锁翠琼酒壶")
        end
    end
    
    self.ExtSprite:SetActive(passtype == 2)--豪华酒壶

    local fx = LuaGameObject.GetChildNoGC(self.BuyAdvPassBtn.transform,"Fx").uiFx
    fx.Visible = passtype ~= 0

    local alertgo1 =  LuaGameObject.GetChildNoGC(self.RewardBtn.transform, "AlertSprite").gameObject
    local alertgo2 =  LuaGameObject.GetChildNoGC(self.TaskBtn.transform, "AlertSprite").gameObject

    local alert1 = self:HaveReward()
    local alert2 = self:HaveTaskReward()
    alertgo1:SetActive(alert1)
    alertgo2:SetActive(alert2)
    if alert1 or alert2 then
        CScheduleMgr.Inst:SetAlertState(42010059,CScheduleMgr.EnumAlertState.Show,true)
    else
        CScheduleMgr.Inst:SetAlertState(42010059,CScheduleMgr.EnumAlertState.Hide,true)
    end
end

function CLuaTcjhMainWnd:HaveTaskReward()
    local tasks = LuaHanJiaMgr.TcjhMainData.TaskData
    for i=1,#tasks do
        local cfg = HanJia2021_TianChengTask.GetData(tasks[i].TaskID)
        if tasks[i].IsRewarded == false then 
            if cfg.Target == 0 or cfg.Target <= tasks[i].Progress then
                return true 
            end
        end
    end
    return false
end

function CLuaTcjhMainWnd:HaveReward()
    local lv = LuaHanJiaMgr.TcjhMainData.Lv
    local lvlimit = LuaHanJiaMgr.TcjhMainData.MaxLv
    local passtype = LuaHanJiaMgr.TcjhMainData.PassType
    local nLv = LuaHanJiaMgr.TcjhMainData.RewardNmlLv
    local aLv = LuaHanJiaMgr.TcjhMainData.RewardAdvLv
    if nLv < lv and nLv < lvlimit then return true end
    if passtype ~= 0 and aLv < lv then return true end
    return false
end

--[[
    @desc: 获得从等级1到某个等级所需的经验值
    author:{author}
    time:2020-11-03 10:21:11
    --@lv: 
    @return:
]]
function CLuaTcjhMainWnd:GetLvExp(lv)
    local res = 0
    local cfg = LuaHanJiaMgr.TcjhMainData.Cfg
    local min = math.min(#cfg,lv)
    for i = 1, min do
        res  = res + cfg[i].NeedJiuQi
    end
    return res
end



--@region UIEvent

--[[
    @desc: 页签切换
    author:{author}
    time:2020-10-30 10:49:43
    --@tab: 0:奖励页签；1:任务页签
    @return:
]]
function CLuaTcjhMainWnd:OnSelectTab(tab)
    if self.CurTabIndex == tab then return end
    self.CurTabIndex = tab
    self.RewardTabPanel:SetActive(self.CurTabIndex == 0)
    self.TaskTabPanel:SetActive(self.CurTabIndex == 1)

    local rewardbtn = CommonDefs.GetComponent_GameObject_Type(self.RewardBtn,typeof(QnTabButton))
    local taskbtn = CommonDefs.GetComponent_GameObject_Type(self.TaskBtn,typeof(QnTabButton))
    rewardbtn.Selected = self.CurTabIndex == 0
    taskbtn.Selected = self.CurTabIndex == 1
end

function CLuaTcjhMainWnd:OnHelpBtnClick()
    local lvlimit = LuaHanJiaMgr.TcjhMainData.MaxLv
    g_MessageMgr:ShowMessage("TCJH_Rule_"..lvlimit)
end

function CLuaTcjhMainWnd:OnBuyAdvPassBtnClick()
    local passtype = LuaHanJiaMgr.TcjhMainData.PassType
    if passtype == 0 then
        CUIManager.ShowUI(CLuaUIResources.TcjhBuyAdvPassWnd)
    else
		if LuaHanJiaMgr.TcjhMainData.Lv < LuaHanJiaMgr.TcjhMainData.MaxLv then
		    CUIManager.ShowUI(CLuaUIResources.TcjhBuyLevelWnd)
		else
			g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("已满级"))
		end
    end
end

--@endregion
