local DelegateFactory = import "DelegateFactory"
local CButton         = import "L10.UI.CButton"
local CItemMgr        = import "L10.Game.CItemMgr"
local EnumItemPlace   = import "L10.Game.EnumItemPlace"

LuaWeddingFeastWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaWeddingFeastWnd, "feastName")
RegistClassMember(LuaWeddingFeastWnd, "effect")
RegistClassMember(LuaWeddingFeastWnd, "normalTable")
RegistClassMember(LuaWeddingFeastWnd, "luxuryTable")
RegistClassMember(LuaWeddingFeastWnd, "fillWnd")
RegistClassMember(LuaWeddingFeastWnd, "eatWnd")
RegistClassMember(LuaWeddingFeastWnd, "fillButton")
RegistClassMember(LuaWeddingFeastWnd, "fillItem")
RegistClassMember(LuaWeddingFeastWnd, "fillIcon")
RegistClassMember(LuaWeddingFeastWnd, "fillNum")
RegistClassMember(LuaWeddingFeastWnd, "fillItemName")
RegistClassMember(LuaWeddingFeastWnd, "fillPlayerName")
RegistClassMember(LuaWeddingFeastWnd, "eatButton")
RegistClassMember(LuaWeddingFeastWnd, "disableButton")

RegistClassMember(LuaWeddingFeastWnd, "isNormal")
RegistClassMember(LuaWeddingFeastWnd, "itemInfo")
RegistClassMember(LuaWeddingFeastWnd, "curSelectId")
RegistClassMember(LuaWeddingFeastWnd, "normalItemNum")
RegistClassMember(LuaWeddingFeastWnd, "luxuryItemNum")

function LuaWeddingFeastWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self:InitUIComponents()
    self:InitEventListener()
end

-- 初始化UI组件
function LuaWeddingFeastWnd:InitUIComponents()
    local anchor = self.transform:Find("Anchor")

    self.feastName = anchor:Find("FeastName"):GetComponent(typeof(UILabel))
    self.effect = anchor:Find("Effect"):GetComponent(typeof(UILabel))
    self.normalTable = anchor:Find("Table/Normal")
    self.luxuryTable = anchor:Find("Table/Luxury")

    self.fillWnd = anchor:Find("FillWnd")
    self.eatWnd = anchor:Find("EatWnd")
    self.fillButton = self.fillWnd:Find("Button"):GetComponent(typeof(CButton))
    self.fillItem = self.fillWnd:Find("Item")
    self.fillIcon = self.fillItem:Find("Icon"):GetComponent(typeof(CUITexture))
    self.fillNum = self.fillItem:Find("Num"):GetComponent(typeof(UILabel))
    self.fillItemName = self.fillItem:Find("ItemName"):GetComponent(typeof(UILabel))
    self.fillPlayerName = self.fillItem:Find("PlayerName"):GetComponent(typeof(UILabel))
    self.eatButton = self.eatWnd:Find("Button"):GetComponent(typeof(CButton))
    self.disableButton = self.eatWnd:Find("DisableButton")
end

-- 初始化响应
function LuaWeddingFeastWnd:InitEventListener()
    UIEventListener.Get(self.fillButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnFillButtonClick()
	end)

    UIEventListener.Get(self.eatButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnEatButtonClick()
	end)

    UIEventListener.Get(self.disableButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnDisableButtonClick()
	end)
end

function LuaWeddingFeastWnd:OnEnable()
    g_ScriptEvent:AddListener("SetItemAt", self, "OnSetItemAt")
    g_ScriptEvent:AddListener("SendItem", self, "OnSendItem")
    g_ScriptEvent:AddListener("NewWeddingSyncFillInDiskProgress", self, "OnSyncFillProgress")
    g_ScriptEvent:AddListener("NewWeddingSyncEnjoyFoodResult", self, "OnSyncEnjoyFoodResult")
end

function LuaWeddingFeastWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SetItemAt", self, "OnSetItemAt")
    g_ScriptEvent:RemoveListener("SendItem", self, "OnSendItem")
    g_ScriptEvent:RemoveListener("NewWeddingSyncFillInDiskProgress", self, "OnSyncFillProgress")
    g_ScriptEvent:RemoveListener("NewWeddingSyncEnjoyFoodResult", self, "OnSyncEnjoyFoodResult")
end

function LuaWeddingFeastWnd:OnSendItem(args)
    local item = args and args[0] and CItemMgr.Inst:GetById(args[0]) or nil
    if item and self.itemInfo[item.TemplateId] then
        self:UpdateItem(item.TemplateId)
        if item.TemplateId == self.curSelectId then
            self:UpdateFillWnd()
        end
    end
end

function LuaWeddingFeastWnd:OnSetItemAt(args)
    for itemId, info in pairs(self.itemInfo) do
        self:UpdateItem(itemId)
    end
    self:UpdateFillWnd()
end

-- 同步装填进度
function LuaWeddingFeastWnd:OnSyncFillProgress(itemId, count)
    if not self.itemInfo[itemId] then return end

    local feastInfo = LuaWeddingIterationMgr.feastInfo[itemId]
    feastInfo.count = count

    local player = CClientMainPlayer.Inst
    feastInfo.playerId = player.Id
    feastInfo.playerName = player.Name
    feastInfo.playerClass = player.Class
    feastInfo.playerGender = player.Gender

    self:UpdateItem(itemId)
    if itemId == self.curSelectId then
        self:UpdateFillWnd()
    end
    self:ShowFillOrEatWnd()
end

-- 同步享用结果
function LuaWeddingFeastWnd:OnSyncEnjoyFoodResult()
    self:UpdateEatButton()
end


function LuaWeddingFeastWnd:Init()
    self.curSelectId = -1
    self.normalItemNum = 5
    self.luxuryItemNum = 8

    self:InitFeast()
    self:ShowFillOrEatWnd()
    self:InitFeastName()
    self:UpdateFillWnd()
end

-- 初始化酒席数据
function LuaWeddingFeastWnd:InitFeast()
    local feastInfo = LuaWeddingIterationMgr.feastInfo

    local itemNum = 0
    for key, data in pairs(feastInfo) do
        itemNum = itemNum + 1
    end

    if itemNum == self.normalItemNum then
        self.isNormal = true
        self:SetTableActive(true, false)
    elseif itemNum == self.luxuryItemNum then
        self.isNormal = false
        self:SetTableActive(false, true)
    else
        self:SetTableActive(false, false)
        return
    end

    -- 排序一下，保证显示顺序一致
    local list = {}
    for itemId, data in pairs(feastInfo) do
        table.insert(list, itemId)
    end
    table.sort(list, function(a, b)
        return a < b
    end)

    local parent = self.isNormal and self.normalTable or self.luxuryTable
    self.itemInfo = {}
    local childId = 0

    for i = 1, #list do
        local itemId = list[i]
        local child = parent:GetChild(childId)
        self.itemInfo[itemId] = {trans = child}
        childId = childId + 1

        local item = CItemMgr.Inst:GetItemTemplate(itemId)
        child:Find("Icon"):GetComponent(typeof(CUITexture)):LoadMaterial(item.Icon)
        self:UpdateItem(itemId)
        self:SetSelectedActive(itemId, false)

        UIEventListener.Get(child.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnItemClick(itemId)
        end)
    end
end

-- 更新一项
function LuaWeddingFeastWnd:UpdateItem(itemId)
    local feastInfo = LuaWeddingIterationMgr.feastInfo[itemId]

    local needCount = self:GetNeedCount(itemId)
    local isFilled = feastInfo.count >= needCount

    local child = self.itemInfo[itemId].trans
    child:Find("Filled").gameObject:SetActive(isFilled)

    local label = child:Find("Label"):GetComponent(typeof(UILabel))
    local fillAvailable = child:Find("FillAvailable").gameObject

    fillAvailable:SetActive(false)
    if isFilled then
        label.text = feastInfo.playerName
    else
        local ownNum = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, itemId)
        if ownNum < needCount then
            label.text = SafeStringFormat("[c][FF0000]%d[-]/%d", ownNum, needCount)
        else
            fillAvailable:SetActive(true)
            label.text = SafeStringFormat("%d/%d", ownNum, needCount)
        end
    end

    local avatar = child:Find("Avatar")
    if isFilled then
        local mat = CUICommonDef.GetPortraitName(feastInfo.playerClass, feastInfo.playerGender, -1)
        avatar:GetComponent(typeof(CUITexture)):LoadNPCPortrait(mat)
        avatar.gameObject:SetActive(true)
    else
        avatar.gameObject:SetActive(false)
    end
end

-- 设置选中状态
function LuaWeddingFeastWnd:SetSelectedActive(itemId, active)
    if itemId < 0 then return end

    local child = self.itemInfo[itemId].trans
    child:Find("Icon/Selected").gameObject:SetActive(active)
end

-- 判断显示装填还是享用窗口
function LuaWeddingFeastWnd:ShowFillOrEatWnd()
    local feastInfo = LuaWeddingIterationMgr.feastInfo

    for itemId, info in pairs(feastInfo) do
        if info.count < self:GetNeedCount(itemId) then
            self:SetFillEatActive(true, false)
            return
        end
    end
    self:SetFillEatActive(false, true)
    self:UpdateEatButton()
end

-- 设置装填和食用active
function LuaWeddingFeastWnd:SetFillEatActive(fillActive, eatActive)
    self.fillWnd.gameObject:SetActive(fillActive)
    self.eatWnd.gameObject:SetActive(eatActive)
end

-- 设置普通和豪华酒席active
function LuaWeddingFeastWnd:SetTableActive(normalActive, luxuryActive)
    self.normalTable.gameObject:SetActive(normalActive)
    self.luxuryTable.gameObject:SetActive(luxuryActive)
end

-- 初始化酒席名称
function LuaWeddingFeastWnd:InitFeastName()
    local sign = LuaWeddingIterationMgr.feastBuffSign

    self.feastName.text = WeddingIteration_FeastBuff.GetData(sign).FeastName

    if self.isNormal == nil then
        self.effect.text = ""
        return
    end

    if self.isNormal then
        self.effect.text = WeddingIteration_Cost.GetData("SimpleFeast").Effect
    else
        self.effect.text = WeddingIteration_Cost.GetData("LuxuryFeast").Effect
    end
end

-- 更新fillwnd
function LuaWeddingFeastWnd:UpdateFillWnd()
    if self.fillWnd.gameObject.active == false then return end

    if self.curSelectId < 0 then
        self.fillButton.gameObject:SetActive(false)
        self.fillItem.gameObject:SetActive(false)
        return
    end

    self.fillItem.gameObject:SetActive(true)
    local itemId = self.curSelectId
    local item = CItemMgr.Inst:GetItemTemplate(itemId)
    self.fillIcon:LoadMaterial(item.Icon)
    self.fillItemName.text = item.Name

    local feastInfo = LuaWeddingIterationMgr.feastInfo[itemId]
    local needCount = self:GetNeedCount(itemId)
    local isFilled = feastInfo.count >= needCount

    if isFilled then
        self.fillButton.gameObject:SetActive(false)
        self.fillPlayerName.gameObject:SetActive(true)
        self.fillNum.text = needCount
        self.fillPlayerName.text = feastInfo.playerName
    else
        self.fillButton.gameObject:SetActive(true)
        self.fillPlayerName.gameObject:SetActive(false)

        local ownNum = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, itemId)
        if ownNum < needCount then
            self.fillNum.text = SafeStringFormat("[c][FF0000]%d[-]/%d", ownNum, needCount)
            self.fillButton.label.text = LocalString.GetString("购买并装填")
        else
            self.fillNum.text = SafeStringFormat("%d/%d", ownNum, needCount)
            self.fillButton.label.text = LocalString.GetString("装填")
        end
    end
end

-- 获取需要数量
function LuaWeddingFeastWnd:GetNeedCount(itemId)
    local feastItem = self.isNormal and WeddingIteration_SimpleFeast or WeddingIteration_LuxuryFeast
    local data = feastItem.GetData(itemId)
    return data.NeedCount
end

-- 更新食用按钮
function LuaWeddingFeastWnd:UpdateEatButton()
    local totalNum = self.isNormal and self.normalItemNum or self.luxuryItemNum
    local leftTime = LuaWeddingIterationMgr.feastLeftTimes
    local eatTime = totalNum - leftTime
    local isEnjoyed = LuaWeddingIterationMgr.feastIsEnjoyed

    self.eatButton.label.text = SafeStringFormat3(LocalString.GetString("食用(%d/%d)"), eatTime, totalNum)
    if isEnjoyed or leftTime <= 0 then
        self.eatButton.Enabled = false
    else
        self.eatButton.Enabled = true
    end
end

--@region UIEvent

function LuaWeddingFeastWnd:OnFillButtonClick()
    local itemId = self.curSelectId
    local needCount = self:GetNeedCount(itemId)

    local ownNum = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, itemId)
    if ownNum < needCount then
        local item = CItemMgr.Inst:GetItemTemplate(itemId)
        local cost = needCount * item.Price
        local msg = g_MessageMgr:FormatMessage("WEDDING_FEAST_BUY_CONFIRM", cost, needCount, item.Name)
        MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function ()
            Gac2Gas.NewWeddingRequestFillInDisk(LuaWeddingIterationMgr.feastId, itemId, needCount)
        end), nil, nil, nil, false)
    else
        Gac2Gas.NewWeddingRequestFillInDisk(LuaWeddingIterationMgr.feastId, itemId, needCount)
    end
end

function LuaWeddingFeastWnd:OnEatButtonClick()
    Gac2Gas.NewWeddingRequestEnjoyFood(LuaWeddingIterationMgr.feastId)
end

function LuaWeddingFeastWnd:OnDisableButtonClick()
    if self.eatButton.Enabled then return end

    Gac2Gas.NewWeddingRequestEnjoyFood(LuaWeddingIterationMgr.feastId)
end

function LuaWeddingFeastWnd:OnItemClick(itemId)
    if self.curSelectId == itemId then return end

    self:SetSelectedActive(self.curSelectId, false)
    self:SetSelectedActive(itemId, true)
    self.curSelectId = itemId
    self:UpdateFillWnd()
end

--@endregion UIEvent

