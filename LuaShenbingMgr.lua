require("3rdParty/ScriptEvent")

local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CEffectMgr = import "L10.Game.CEffectMgr"
local CUIManager = import "L10.UI.CUIManager"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CFreightEquipSubmitMgr = import "L10.UI.CFreightEquipSubmitMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local LocalString = import "LocalString"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"

LuaShenbingMgr = {}

LuaShenbingMgr.DLNState = 0
LuaShenbingMgr.DLNTaskId = 0
LuaShenbingMgr.LinkFxTable = {}

LuaShenbingMgr.ShenbingQianyuanItemCnt = 0
LuaShenbingMgr.ShenbingQianyuanItemTotalCnt = 30
LuaShenbingMgr.ShenbingQianyuanItemTable = {}
LuaShenbingMgr.ShenbingQianyuanEquipItemId = ""

LuaShenbingMgr.ShenbingEnterWndFinishTask = {}
LuaShenbingMgr.ShenbingRemainTimes = 0

LuaShenbingMgr.QianYuanBuyInfo = nil
LuaShenbingMgr.QianYuanFormulaIndex = 0

LuaShenbingMgr.m_CurrentTuJianId = 0

LuaShenbingMgr.BasicColorTexPath = "UI/Texture/Transparent/Material/ranliao.mat"
LuaShenbingMgr.ValueIncColorTexPath = "UI/Texture/Item_Other/Material/other_535.mat"
LuaShenbingMgr.ValueDecColorTexPath = "UI/Texture/Item_Other/Material/other_536.mat"
LuaShenbingMgr.SaturationIncColorTexPath = "UI/Texture/Item_Other/Material/other_537.mat"
LuaShenbingMgr.SaturationDecColorTexPath = "UI/Texture/Item_Other/Material/other_538.mat"

LuaShenbingMgr.ColorationSettingValueTable = nil

function LuaShenbingMgr:IsColorationOpen()
    return CommonDefs.IsPlayEnabled("ShenBingRanSe")
end

function LuaShenbingMgr:RestoreShenBing()
    g_ScriptEvent:RemoveListener("OnFreightSubmitEquipSelected", self, "SelectEquip")
    g_ScriptEvent:AddListener("OnFreightSubmitEquipSelected", self, "SelectEquip")

    if not CClientMainPlayer.Inst then
        return
    end

    local dict = CreateFromClass(MakeGenericClass(Dictionary, Int32, MakeGenericClass(List, cs_string)))
    local list = CreateFromClass(MakeGenericClass(List, cs_string))
    -- find
    local placeTable = {EnumItemPlace.Body, EnumItemPlace.Bag}
    for i = 1, #placeTable do
        local size = CClientMainPlayer.Inst.ItemProp:GetPlaceSize(placeTable[i])
        if size > 0 then
            for j = 1, size do
                local id = CClientMainPlayer.Inst.ItemProp:GetItemAt(placeTable[i], j)
                local equip = CItemMgr.Inst:GetById(id)
                if equip ~= nil and equip.IsEquip and equip.Equip.IsShenBing then
          CommonDefs.ListAdd(list, typeof(cs_string), id)
        end
      end
    end
  end
  CommonDefs.DictAdd(dict, TypeOfInt32, 0, typeof(MakeGenericClass(List, cs_string)), list)
  if list.Count > 0 then
    CFreightEquipSubmitMgr.OpenEquipmentChooseWnd("ShenbingRestore", dict, LocalString.GetString("请选择要还原的神兵"), 0, false, LocalString.GetString("还原"), LocalString.GetString("前往购买"), nil, LocalString.GetString("包裹中没有该装备"))
  else
    g_MessageMgr:ShowMessage("ShenBing_No_Equip_Restore", {})
  end
end

function LuaShenbingMgr:SelectEquip(args)
    if args[1] ~= "ShenbingRestore" then
        return
    end
    local equip = CItemMgr.Inst:GetById(args[0])
    local placeTable = {EnumItemPlace.Body, EnumItemPlace.Bag}
    for _, v in pairs(placeTable) do
        local pos = CItemMgr.Inst:FindItemPosition(v, equip.Id)
        if pos > 0 then
            Gac2Gas.QueryShenBingRestoreReturn(v, pos, equip.Id)
            break
        end
    end
end

-- 打开购买兑换前缘的物品界面
function LuaShenbingMgr:OpenBuyQianYuanEquips(info, index)
    LuaShenbingMgr.QianYuanBuyInfo = info
    LuaShenbingMgr.QianYuanFormulaIndex = index
    CUIManager.ShowUI(CLuaUIResources.ShenBingExchangeQyBuyWnd)
end


function LuaShenbingMgr:UpdateColor(colorCnt_U)
  if not CClientMainPlayer.Inst or not CClientMainPlayer.Inst.ItemProp then return end
  --TODO change to list
  local list = MsgPackImpl.unpack(colorCnt_U)
  if not list then return end
  for i = 0, list.Count - 1, 2 do
    CommonDefs.DictSet_LuaCall(CClientMainPlayer.Inst.ItemProp.ShenBingRanLiao, list[i], list[i + 1])
  end
end

function Gas2Gac.SBCSPlayAddLinkFx(srcEngineId, dstEngineId, fxId)
  if (LuaShenbingMgr.LinkFxTable[srcEngineId] and LuaShenbingMgr.LinkFxTable[srcEngineId][dstEngineId]) or (LuaShenbingMgr.LinkFxTable[dstEngineId] and LuaShenbingMgr.LinkFxTable[dstEngineId][srcEngineId]) then
    return
  end
  if not LuaShenbingMgr.LinkFxTable[srcEngineId] then
    LuaShenbingMgr.LinkFxTable[srcEngineId] = {}
  end
    local srcObj = CClientObjectMgr.Inst:GetObject(srcEngineId)
    local dstObj = CClientObjectMgr.Inst:GetObject(dstEngineId)
    if srcObj and dstObj then
        local fx = CEffectMgr.Inst:AddLinkFX(fxId, srcObj.RO, dstObj.RO, 0, 1, -1)
        if fx then
            LuaShenbingMgr.LinkFxTable[srcEngineId][dstEngineId] = fx
        end
    end
end

function Gas2Gac.SBCSPlayRemoveLinkFx(srcEngineId, dstEngineId)
    if LuaShenbingMgr.LinkFxTable[srcEngineId] and LuaShenbingMgr.LinkFxTable[srcEngineId][dstEngineId] then
        LuaShenbingMgr.LinkFxTable[srcEngineId][dstEngineId]:Destroy()
        LuaShenbingMgr.LinkFxTable[srcEngineId][dstEngineId] = nil
    end
    if LuaShenbingMgr.LinkFxTable[dstEngineId] and LuaShenbingMgr.LinkFxTable[dstEngineId][srcEngineId] then
        LuaShenbingMgr.LinkFxTable[dstEngineId][srcEngineId]:Destroy()
        LuaShenbingMgr.LinkFxTable[dstEngineId][srcEngineId] = nil
    end
end

function Gas2Gac.SBCSSyncFinishInfo(finishInfo, isFirst, remainTimes)
    LuaShenbingMgr.ShenbingEnterWndFinishTask = {}
    local list = MsgPackImpl.unpack(finishInfo)
    if list then
        for i = 0, list.Count - 1, 1 do
            table.insert(LuaShenbingMgr.ShenbingEnterWndFinishTask, list[i])
        end
    end
    LuaShenbingMgr.ShenbingRemainTimes = remainTimes
    CUIManager.ShowUI(CLuaUIResources.ShenbingEnterWnd)
end

function Gas2Gac.RequestMakeShenBingSuccess()
    g_ScriptEvent:BroadcastInLua("RequestMakeShenBingSuccess")
end

function Gas2Gac.RequestRecastShenBingSuccess()
    g_ScriptEvent:BroadcastInLua("RequestRecastShenBingSuccess")
end

function Gas2Gac.RequestTrainShenBingSuccess()
    g_ScriptEvent:BroadcastInLua("RequestTrainShenBingSuccess")
end

function Gas2Gac.RequestBreakShenBingTrainLevelSuccess()
    g_ScriptEvent:BroadcastInLua("RequestBreakShenBingTrainLevelSuccess")
end

function Gas2Gac.RequestRestoreShenBingSuccess()
    g_MessageMgr:ShowMessage("ShenBing_Restore_Success")
end

function Gas2Gac.RequestExchangeQianYuanSuccess()
    g_ScriptEvent:BroadcastInLua("RequestExchangeQianYuanSuccess")
end

function Gas2Gac.ProduceShenBingRanLiaoByMaterialSuccess(ranliaoCount_U)
    LuaShenbingMgr:UpdateColor(ranliaoCount_U)
    g_ScriptEvent:BroadcastInLua("ProduceShenBingColorSuccess")
    g_MessageMgr:ShowMessage("ShenBing_Compose_Color_Success")
end

function Gas2Gac.ProduceShenBingRanLiaoByRanLiaoSuccess(ranliaoCount_U)
    LuaShenbingMgr:UpdateColor(ranliaoCount_U)
    g_ScriptEvent:BroadcastInLua("ProduceShenBingColorSuccess")
    g_MessageMgr:ShowMessage("ShenBing_Compose_Color_Success")
end

function Gas2Gac.RequestShenBingRanSeSuccess(ranliaoCount_U, regionId)
    LuaShenbingMgr:UpdateColor(ranliaoCount_U)
    g_ScriptEvent:BroadcastInLua("ShenBingColorationSuccess")
    local regionStr = regionId == 1 and LocalString.GetString("一") or LocalString.GetString("二")
    g_MessageMgr:ShowMessage("ShenBing_Coloration_Success", regionStr)
    LuaSEASdkMgr:AppsflyerNotify("thoitrang_nhuomtrangbi", SafeStringFormat3("{}"))
end

function Gas2Gac.RequestResetShenBingRanSeSuccess()
    g_ScriptEvent:BroadcastInLua("ResetShenBingColorSuccess")
end

LuaShenbingMgr.RestoreEquipPlace = nil
LuaShenbingMgr.RestoreEquipPos = nil
LuaShenbingMgr.RestoreItemInfoUD = nil
LuaShenbingMgr.RestoreConsumeBJMNum = nil
LuaShenbingMgr.RestoreReturnJQZNum = nil
function Gas2Gac.QueryShenBingRestoreReturnResult(equipPlace, equipPos, returnItemInfo_U, conditionConsumeBJMNum, conditionReturnJQZNum)
    LuaShenbingMgr.RestoreEquipPlace = equipPlace
    LuaShenbingMgr.RestoreEquipPos = equipPos
    LuaShenbingMgr.RestoreItemInfoUD = returnItemInfo_U
    LuaShenbingMgr.RestoreConsumeBJMNum = conditionConsumeBJMNum
    LuaShenbingMgr.RestoreReturnJQZNum = conditionReturnJQZNum
    CUIManager.ShowUI(CLuaUIResources.ShenBingRestoreWnd)
end

LuaShenbingMgr.WeaponFx = nil
function LuaShenbingMgr:InitWeaponFx()
	LuaShenbingMgr.WeaponFx = {}
	local count = ShenBing_WeaponFx.GetDataCount()
	for i=1,count do
		local cfg = ShenBing_WeaponFx.GetData(i)
		if LuaShenbingMgr.WeaponFx[cfg.ShenBingType] == nil then
			LuaShenbingMgr.WeaponFx[cfg.ShenBingType] = {}
		end
		LuaShenbingMgr.WeaponFx[cfg.ShenBingType][cfg.GrowLevel] = cfg.FxId
	end
end

function LuaShenbingMgr:GetWeaponFxID(type,lv)
    if LuaShenbingMgr.WeaponFx == nil then 
		self:InitWeaponFx()
	end
    if LuaShenbingMgr.WeaponFx[type] == nil  then 
        return 0
    end
    local fxid = LuaShenbingMgr.WeaponFx[type][lv]
    if fxid == nil then return 0 end
    return fxid
end

function LuaShenbingMgr:AddWeaponFxToObj(ro,type,lv,colors)
	local fxid = LuaShenbingMgr:GetWeaponFxID(type,lv)
	if fxid > 0 then
		CEffectMgr.Inst:AddObjectFX(fxid,ro,0, 1.0, 1.0, colors, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
	end
end

function LuaShenbingMgr:AddAdvWeaponFx(ro,fxid,colors)
    if fxid.Length > 0 then
        local fxid1 = fxid[0]
        if fxid1 and fxid1 > 0 then
            CEffectMgr.Inst:AddObjectFX(fxid1,ro,0, 1.0, 1.0, colors, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
        end
    end
    if fxid.Length > 1 then
        local fxid2 = fxid[1]
        if fxid2 and fxid2 > 0 then
            CEffectMgr.Inst:AddObjectFX(fxid2,ro,0, 1.0, 1.0, colors, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
        end
    end
end
