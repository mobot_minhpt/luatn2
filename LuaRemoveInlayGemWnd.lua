local CEquipmentProcessMgr = import "L10.Game.CEquipmentProcessMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CCurentMoneyCtrl=import "L10.UI.CCurentMoneyCtrl"

CLuaRemoveInlayGemWnd = class()
RegistClassMember(CLuaRemoveInlayGemWnd,"okBtn")
RegistClassMember(CLuaRemoveInlayGemWnd,"descLabel")
RegistClassMember(CLuaRemoveInlayGemWnd,"yuanbaoCtrl")
CLuaRemoveInlayGemWnd.s_RemoveReferenceStone = false
function CLuaRemoveInlayGemWnd:Awake()
    self.okBtn = self.transform:Find("Anchor/OkButton").gameObject
    self.descLabel = self.transform:Find("Anchor/TextLabel"):GetComponent(typeof(UILabel))
    self.descLabel.text = nil
    self.yuanbaoCtrl = self.transform:Find("Anchor/QnCostAndOwnMoney"):GetComponent(typeof(CCurentMoneyCtrl))

    UIEventListener.Get(self.okBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:DoRemove()
    end)
end

function CLuaRemoveInlayGemWnd:Init( )
    if CClientMainPlayer.Inst == nil then
        return
    end

    if CLuaEquipMgr.m_GemRemoveCostYuanbao > 0 then
        self.yuanbaoCtrl:SetCost(CLuaEquipMgr.m_GemRemoveCostYuanbao)
    end
    if CLuaRemoveInlayGemWnd.s_RemoveReferenceStone then
        self.descLabel.text = g_MessageMgr:FormatMessage("Remove_Reference_Stone_Confirm")
    end
    if CLuaEquipMgr.m_GemRemoveBaseItemId>0 then
        local holeInfo = CLuaEquipMgr.SelectedEquipmentHoleInfo
        local template = Item_Item.GetData(holeInfo.stoneItemId)

        local jewel = Jewel_Jewel.GetData(CLuaEquipMgr.m_GemRemoveBaseItemId)
        local baseItem = Item_Item.GetData(CLuaEquipMgr.m_GemRemoveBaseItemId)
        if template == nil or jewel == nil or baseItem == nil then
            return
        end
        local baseStoneName = baseItem.Name
        
        local str = SafeStringFormat3(LocalString.GetString("卸下此宝石将拆解成%d个%s"), CLuaEquipMgr.m_GemRemoveSplitStoneCount, baseStoneName)
        if template.Type == EnumItemType_lua.WenShi then
            str = SafeStringFormat3(LocalString.GetString("卸下此纹饰将拆解成%d个%s"), CLuaEquipMgr.m_GemRemoveSplitStoneCount, baseStoneName)
        end
        if template.Type == EnumItemType_lua.WenShi then
            if CLuaEquipMgr.m_GemRemoveAdditionalItemId > 0 and CLuaEquipMgr.m_GemRemoveSplitAdditionalItemCount > 0 then
                local addtionalItem = Item_Item.GetData(CLuaEquipMgr.m_GemRemoveAdditionalItemId)
                if addtionalItem ~= nil then
                    str = str .. SafeStringFormat3(LocalString.GetString("以及%d个%s"), CLuaEquipMgr.m_GemRemoveSplitAdditionalItemCount, addtionalItem.Name)
                end
            end
        end
        self.descLabel.text = str
    end
end
function CLuaRemoveInlayGemWnd:DoRemove( )
    local holeInfo = CLuaEquipMgr.SelectedEquipmentHoleInfo
    local info = CEquipmentProcessMgr.Inst.SelectEquipment
    if info == nil or holeInfo == nil then
        return
    end
    if not CEquipmentProcessMgr.Inst:CheckOperationConflict(info) then
        return
    end
    Gac2Gas.RemoveStoneOnEquipment(EnumToInt(info.place), info.pos, info.itemId, holeInfo.holeIndex, CLuaCheckGemGroupWnd.m_HoleSetIndex)

    CUIManager.CloseUI(CUIResources.RemoveInlayGemWnd)
end

function CLuaRemoveInlayGemWnd:OnDestroy()
    CLuaRemoveInlayGemWnd.s_RemoveReferenceStone = false
end
