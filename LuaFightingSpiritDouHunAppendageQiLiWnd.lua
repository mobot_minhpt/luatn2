local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UIGrid = import "UIGrid"
local CButton = import "L10.UI.CButton"
local CFightingSpiritMgr=import "L10.Game.CFightingSpiritMgr"
local CShopMallMgr=import "L10.UI.CShopMallMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local String = import "System.String"
local Gac2Gas2 = import "L10.Game.Gac2Gas"

LuaFightingSpiritDouHunAppendageQiLiWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaFightingSpiritDouHunAppendageQiLiWnd, "OkBtn", "OkBtn", GameObject)
RegistChildComponent(LuaFightingSpiritDouHunAppendageQiLiWnd, "CancelBtn", "CancelBtn", GameObject)
RegistChildComponent(LuaFightingSpiritDouHunAppendageQiLiWnd, "ItemTemplate", "ItemTemplate", GameObject)
RegistChildComponent(LuaFightingSpiritDouHunAppendageQiLiWnd, "Grid", "Grid", UIGrid)

--@endregion RegistChildComponent end
RegistClassMember(LuaFightingSpiritDouHunAppendageQiLiWnd, "m_Data")
RegistClassMember(LuaFightingSpiritDouHunAppendageQiLiWnd, "m_Btn")
RegistClassMember(LuaFightingSpiritDouHunAppendageQiLiWnd, "m_CurrentIndex")
RegistClassMember(LuaFightingSpiritDouHunAppendageQiLiWnd, "m_CurrentSelect")

function LuaFightingSpiritDouHunAppendageQiLiWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.OkBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOkBtnClick()
	end)


	
	UIEventListener.Get(self.CancelBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCancelBtnClick()
	end)


    --@endregion EventBind end
end

function LuaFightingSpiritDouHunAppendageQiLiWnd:Init()
	if LuaFightingSpiritDouHunAppendageMgr.m_QiLiData then
		self.ItemTemplate:SetActive(false)
		self.m_CurrentSelect = 0
		self.m_Data = LuaFightingSpiritDouHunAppendageMgr.m_QiLiData
		self.m_Btn = {}
		self.m_CurrentIndex = CFightingSpiritMgr.Instance:EnumDouHunTrainPointTypeToIndex(self.m_Data.pointType) + 1

		local level = self.m_Data.level
		local data = DouHunTan_QiLiFuTi.GetData(level)
		local dataList = {data.GenGuEffect, data.LiLiangEffect, data.ZhiLiEffect, data.MinJieEffect, data.JingLiEffect, data.JianShangEffect}
		local dataDesc = {LocalString.GetString("灵兽根骨"), LocalString.GetString("灵兽力量"), LocalString.GetString("灵兽智力"), 
			LocalString.GetString("灵兽敏捷"), LocalString.GetString("灵兽精力"), LocalString.GetString("灵兽范围伤害减免")}

		for i =1, 6 do
			local g = NGUITools.AddChild(self.Grid.gameObject, self.ItemTemplate)
			g:SetActive(true)
			self:InitItem(g, dataDesc[i] .. "[FFFE91]+" .. dataList[i], i)
		end
		self.Grid:Reposition()
	else
		CUIManager.CloseUI(CLuaUIResources.FightingSpiritDouHunAppendageQiLiWnd)
	end
end

function LuaFightingSpiritDouHunAppendageQiLiWnd:InitItem(item, text, index)
	local descLabel = item.transform:Find("DesLabel"):GetComponent(typeof(UILabel))
	descLabel.text = text

	local btn = item.transform:GetComponent(typeof(CButton))

	local mark = item.transform:Find("CornorSprite").gameObject
	-- 设置当前状态
	mark:SetActive(index == self.m_CurrentIndex)

	table.insert(self.m_Btn, btn)

	UIEventListener.Get(item).onClick = DelegateFactory.VoidDelegate(function()
		self.m_CurrentSelect = index
		-- 设置选中状态
		for i=1, #self.m_Btn do
			self.m_Btn[i].Selected = i == self.m_CurrentSelect
		end
	end)
end

--@region UIEvent

function LuaFightingSpiritDouHunAppendageQiLiWnd:OnOkBtnClick()
	if self.m_CurrentSelect == 0 then
		g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("请先选择一项灵兽属性"))
	else
		Gac2Gas2.SetDouHunTrainQiLiFuTiEffect(CFightingSpiritMgr.Instance:IndexToEnumDouHunTrainPointType(self.m_CurrentSelect - 1))
	end
end

function LuaFightingSpiritDouHunAppendageQiLiWnd:OnCancelBtnClick()
	CUIManager.CloseUI(CLuaUIResources.FightingSpiritDouHunAppendageQiLiWnd)
end

--@endregion UIEvent

