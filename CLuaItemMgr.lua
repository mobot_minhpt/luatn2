local LocalString       = import "LocalString"
local DelegateFactory   = import "DelegateFactory"
local CItemMgr          = import "L10.Game.CItemMgr"
local CUIManager            = import "L10.UI.CUIManager"
local CCommonActionWndMgr   = import "L10.UI.CCommonActionWndMgr"
local CUIResources          = import "L10.UI.CUIResources"
local EnumQualityType = import "L10.Game.EnumQualityType"
local CClientChuanJiaBaoMgr = import "L10.Game.CClientChuanJiaBaoMgr"
local CZuoQiMgr = import "L10.Game.CZuoQiMgr"
local CMapiBabyInfo = import "L10.Game.CMapiBabyInfo"
local CMapiProp = import "L10.Game.CMapiProp"
CLuaItemMgr = {}

--[[
    @desc: 显示快速使用窗口
    author:CodeGize
    time:2019-09-03 11:20:22
    --@tempid:
	--@place: EnumItemPlace
    @return:
]]
function CLuaItemMgr.ShowFastUse(tempid,place)
    local tcfg = Item_Item.GetData(tempid)
    if tcfg ~= nil then
        local icon = tcfg.Icon
        local btnName = LocalString.GetString("使用")

        local pos
        local itemId
        local isfound
        isfound, pos, itemId = CItemMgr.Inst:FindItemByTemplateIdPriorToBinded(place, tempid)
        if isfound then
            local usefunc = function()
                Gac2Gas.RequestUseItem(EnumToInt(place), pos, itemId, "")
                CLuaItemMgr.CloseFastUse()
            end
            local item = CItemMgr.Inst:GetById(itemId)
            CCommonActionWndMgr.Inst:ShowWithName(item.Icon,item.ColoredName,btnName,DelegateFactory.Action(usefunc),nil)
        else
            --g_MessageMgr:ShowMessage("No_Xi_Wang_Deng")
        end
    end
end

function CLuaItemMgr.CloseFastUse()
    CUIManager.CloseUI(CUIResources.CommonActionWnd);
end

function CLuaItemMgr:GetItemCellBorder(id)
    local item = CItemMgr.Inst:GetById(id)
    local spriteName = EnumQualityType.White
    if item and item.IsItem then
        local itemTemplate = Item_Item.GetData(item.TemplateId)
        spriteName = CUICommonDef.GetItemCellBorder(itemTemplate and itemTemplate.NameColor or EnumQualityType.White)
        if item.Item.Type == EnumItemType_lua.EvaluatedZhushabi then
            spriteName = CClientChuanJiaBaoMgr.Inst:GetZhushabiBorder(item)
        elseif item.Item.Type == EnumItemType_lua.Chuanjiabao then
            spriteName = CClientChuanJiaBaoMgr.Inst:GetChuanjiabaoBorder(item)
        elseif CZuoQiMgr.GetLingfuPosByTemplateId(item.TemplateId) > 0 then
            spriteName = CZuoQiMgr.GetLingFuItemCellBorder(item.Item.LingfuItemInfo.Quality)
        elseif CZuoQiMgr.IsMaPi(item.TemplateId) then
            local setting = ZuoQi_Setting.GetData()
            local prop = nil
            if item.Item.TemplateId == setting.MapiXiaomaItemId then
                local babyInfo = CreateFromClass(CMapiBabyInfo)
                if babyInfo:LoadFromString(item.Item.ExtraVarData.Data) then
                    prop = CZuoQiMgr.CreateMapiPropFromBabyInfo(babyInfo)
                end
            else
                prop = CreateFromClass(CMapiProp)
                if not prop:LoadFromString(item.Item.ExtraVarData.Data) then
                    prop = nil
                end
            end
            if prop then
                spriteName = CZuoQiMgr.GetMapiItemCellBorder(prop.Quality)
            end
        end
    end
    return spriteName
end

--[[
    @desc: 有没有不可修词条
    author:Codee
    time:2023-05-11 16:55:40
    --@equip: CEquipment
    @return:
]]
function CLuaItemMgr.HaveCanotFixWord(equip)
    local fixlist= equip:GetFixedWordList(false)
    for i=1,fixlist.Count do
            if math.floor(fixlist[i-1]/100)==251025 then
            return true
        end
    end

    local extlist= equip:GetExtWordList(false)
    for i=1,extlist.Count do
        if math.floor(extlist[i-1]/100)==251025 then
            return true
        end
    end
    return false
end