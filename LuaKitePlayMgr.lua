require("3rdParty/ScriptEvent")
require("ui/kiteplay/LuaKitePlayWnd")

local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
LuaKitePlayMgr = {}

LuaKitePlayMgr.EnableGyroControl = false

LuaKitePlayMgr.rightMoveRatio = 0.1
LuaKitePlayMgr.leftMoveRatio = 0.1
LuaKitePlayMgr.lastSendTime = 0
LuaKitePlayMgr.perSendTime = 0.1
LuaKitePlayMgr.leftMove = {x=0,y=0,z=0}
LuaKitePlayMgr.rightMove = {x=0,y=0,z=0}
LuaKitePlayMgr.cameraUpNum = 1
LuaKitePlayMgr.DropItemTable = {}

LuaKitePlayMgr.RealTimeScore = 0
LuaKitePlayMgr.RealTimeTime = 0

LuaKitePlayMgr.ResultScore = 0
LuaKitePlayMgr.ResultTime = 0

LuaKitePlayMgr.ChooseKiteType = 1
LuaKitePlayMgr.ChooseNPCEngineId = 0

LuaKitePlayMgr.KiteCameraMoveTotalRatio = 5
LuaKitePlayMgr.KiteCameraMoveTriggerRatio = 1.5
LuaKitePlayMgr.KiteCameraMoveSpeed = 12

LuaKitePlayMgr.InjusticeRankTable = {}
LuaKitePlayMgr.InjusticeSelfSocre = 0

function Gas2Gac.SyncZhiYuanPos(playerId, posX, posY, posZ, accX, accY, accZ)
	if LuaKitePlayMgr.m_DataTable and LuaKitePlayMgr.m_DataTable[playerId] then
		LuaKitePlayMgr.m_DataTable[playerId].kitePos = {posX, posY, posZ}
		LuaKitePlayMgr.m_DataTable[playerId].acc = {accX, accY, accZ}
	end
	g_ScriptEvent:BroadcastInLua("UpdateKitePos",LuaKitePlayMgr.m_DataTable[playerId])
end

function Gas2Gac.BeginZhiYuanControl(data)
	local list = MsgPackImpl.unpack(data)
	if not list then return end
	LuaKitePlayMgr.m_DataTable = {}
	for i = 0, list.Count - 1, 11 do
		LuaKitePlayMgr.m_DataTable[list[i]] = {
			playerId = list[i],
			kitePos = {list[i + 1],list[i + 2],list[i + 3]},
			playerPos = {list[i + 4],list[i + 5],list[i + 6]},
			acc = {list[i + 7],list[i + 8],list[i + 9]},
			kiteType = list[i + 10],
		}
	end
	CUIManager.ShowUI(CUIResources.KitePlayWnd)
end

function Gas2Gac.BatchSyncZhiYuanDropInfo(data)
	local list = MsgPackImpl.unpack(data)
	if not list then return end
	for i = 0, list.Count - 1, 5 do
		local info = {}
		info.uuid = list[i]
		info.dropIdx = list[i+1]
		info.pos = {list[i+2],list[i+3],list[i+4]}
		LuaKitePlayMgr.DropItemTable[info.uuid] = info
	end
	--g_ScriptEvent:BroadcastInLua("UpdateKitePos",LuaKitePlayMgr.m_DataTable[playerId])
end

function Gas2Gac.SyncZhiYuanScore(score, leftTime)
	LuaKitePlayMgr.RealTimeScore = score
	LuaKitePlayMgr.RealTimeTime = leftTime
	g_ScriptEvent:BroadcastInLua("UpdateRealTimeScore")
end

-- 一个采集物在一个地方被消耗了,在另外一个地方重新创建了一个新的
function Gas2Gac.SyncZhiYuanOneDropReplaced(oldUuid, dropInfo)

end

-- 纸鸢特效发生改变
-- fxIdx -> 0代表移除特效,其他情况对应的是 ZhiYuan_Drop 中的 FxIdx
function Gas2Gac.SyncZhiYuanFxChange(playerId, fxIdx)

end


-- 离开纸鸢控制,这个时候关闭界面
function Gas2Gac.EndZhiYuanControl(score, timeUse)
	LuaKitePlayMgr.ResultScore = score
	LuaKitePlayMgr.ResultTime = timeUse
	CUIManager.CloseUI(CUIResources.KitePlayWnd)
	CUIManager.ShowUI(CUIResources.KitePlayResultWnd)
end
--某位玩家离开纸鸢玩法
function Gas2Gac.SyncZhiYuanLeave(playerId)
	--LuaKitePlayMgr.m_DataTable[playerId] = nil
	g_ScriptEvent:BroadcastInLua("DeleteKite",playerId)
end
--某位玩家新加入纸鸢玩法
function Gas2Gac.SyncNewZhiYuanPos(playerId, posX, posY, posZ, rolePosX, rolePosY, rolePosZ, accX, accY, accZ, kiteType)
	LuaKitePlayMgr.m_DataTable[playerId] = {
		playerId = playerId,
		kitePos = {posX,posY,posZ},
		playerPos = {rolePosX,rolePosY,rolePosZ},
		acc = {accX,accY,accZ},
		kiteType = kiteType,
	}
	g_ScriptEvent:BroadcastInLua("AddNewKite",LuaKitePlayMgr.m_DataTable[playerId])
end

-- 点击NPC请求开始放风筝之后会收到这个RPC,目的是获取当前角色的z坐标(posZ)
-- 客户端应该返回 RPC RequestJoinZhiYuan,带上 npcEngineId 和 posZ
function Gas2Gac.GetPlayerHighForZhiYuan(npcEngineId)
	--local kiteType = 1
    --Gac2Gas.RequestJoinZhiYuan(npcEngineId, CClientMainPlayer.Inst.WorldPos.y,kiteType)
		LuaKitePlayMgr.ChooseNPCEngineId = npcEngineId
		CUIManager.ShowUI(CUIResources.KitePlayOpenWnd)
end

-- ---------------------
-- rankType -> 1:纸鸢榜单 2:副本榜单
-- 注意这个RPC有变动,看交互,纸鸢排行榜只需要id,名字和分数
-- ---------------------
function Gas2Gac.SyncZhiYuanRankData(rankType, data, selfRankData)
	LuaKitePlayMgr.m_RankDataTable = {}
	local list = MsgPackImpl.unpack(data)
	if not list then return end

	if rankType == 1 then
		for i = 0, list.Count - 1, 3 do
			local info = {}
			table.insert(LuaKitePlayMgr.m_RankDataTable,{
				playerId = list[i],
				playerName = list[i+1],
				score = list[i+2],
				})
		end
		CUIManager.ShowUI(CUIResources.KitePlayRankWnd)
	else
		LuaKitePlayMgr.InjusticeRankTable = {}
		for i = 0, list.Count - 1, 5 do
			table.insert(LuaKitePlayMgr.InjusticeRankTable,{
				playerId = list[i],
				playerName = list[i+1],
				score = list[i+2],
				playerClass = list[i+3],
				playerGender = list[i+4],
				})
		end

		-- 自己的得分情况
		local selfList = MsgPackImpl.unpack(selfRankData)
		local selfPlayerId = selfList[0]
		local selfPlayerName = selfList[1]
		-- 没有参加的话,这里是0
		LuaKitePlayMgr.InjusticeSelfSocre = selfList[2]
		local selfClass = selfList[3]
		local selfGender = selfList[4]
		CUIManager.ShowUI(CUIResources.InjusticeWnd)
	end


end
