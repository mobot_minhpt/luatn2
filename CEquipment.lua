local CEquipment = import "L10.Game.CEquipment"
local CWeddingMgr = import "L10.Game.CWeddingMgr"
local EnumBodyPosition = import "L10.Game.EnumBodyPosition"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CCraveData = import "L10.Game.CCraveData"

CEquipment.m_hookGetIcon = function(this) 
    local template = EquipmentTemplate_Equip.GetData(this.TemplateId)

    if CWeddingMgr.Inst:IsWeddingRingTaben(this) then
        return CWeddingMgr.Inst:GetWeddingRingTabenBgIcon(this, false)
    end
    
    local res, icon = this:Check(CommonDefs.ConvertIntToEnum(typeof(EnumBodyPosition), template.Type))
    
    if res then
        return icon
    end

    if this.CraveData.Data then 
        local craveData = CreateFromClass(CCraveData)
        craveData:LoadFromString(this.CraveData.Data)
        if craveData.Key == 1 and craveData.ExpiredTime > CServerTimeMgr.Inst.timeStamp then -- 知己手镯拓本
            local list = MsgPackImpl.unpack(craveData.Data.Data)
            if list and list.Count >= 1 then
                local data = Item_Item.GetData(list[0])
                if data then
                    return data.Icon
                end
            end
        end
    end
    
    if this.IsShenBing then
        local iconData = ShenBing_EquipIcon.GetData(template.ShenBingType)
        if iconData ~= nil then
            if this.SubHand > 0 then
                return iconData.SubHandIcon
            else
                return iconData.Icon
            end
        end
    end
    if this.SubHand > 0 then
        local subhand = EquipBaptize_ZKWeaponTransform.GetData(this.TemplateId)
        if subhand then
            return subhand.SubHandIcon
        end
    end
    return template.Icon
end

CEquipment.m_hookGetBigicon = function(this) 
    local template = EquipmentTemplate_Equip.GetData(this.TemplateId)

    if CWeddingMgr.Inst:IsWeddingRingTaben(this) then
        return CWeddingMgr.Inst:GetWeddingRingTabenBgIcon(this, true)
    end
    
    local res, icon = this:Check(CommonDefs.ConvertIntToEnum(typeof(EnumBodyPosition), template.Type))
    
    if res then
        return icon
    end

    if this.CraveData.Data then 
        local craveData = CreateFromClass(CCraveData)
        craveData:LoadFromString(this.CraveData.Data)
        if craveData.Key == 1 and craveData.ExpiredTime > CServerTimeMgr.Inst.timeStamp then -- 知己手镯拓本
            local list = MsgPackImpl.unpack(craveData.Data.Data)
            if list and list.Count >= 1 then
                local data = Item_Item.GetData(list[0])
                if data then
                    return data.Icon
                end
            end
        end
    end
    
    if this.IsShenBing then
        local iconData = ShenBing_EquipIcon.GetData(template.ShenBingType)
        if iconData ~= nil then
            if this.SubHand > 0 then
                return iconData.SubHandBigIcon
            else
                return iconData.BigIcon
            end
        end
    end
    if this.SubHand > 0 then
        local subhand = EquipBaptize_ZKWeaponTransform.GetData(this.TemplateId)
        if subhand then
            return subhand.SubHandBigIcon
        end
    end
        return template.BigIcon
end
