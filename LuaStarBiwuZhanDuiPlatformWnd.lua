local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnTabView = import "L10.UI.QnTabView"
local UILabel = import "UILabel"

LuaStarBiwuZhanDuiPlatformWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaStarBiwuZhanDuiPlatformWnd, "Tabs", "Tabs", QnTabView)
RegistChildComponent(LuaStarBiwuZhanDuiPlatformWnd, "TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaStarBiwuZhanDuiPlatformWnd, "SelfZhanDui", "SelfZhanDui", GameObject)
RegistChildComponent(LuaStarBiwuZhanDuiPlatformWnd, "ZhanDuiListView", "ZhanDuiListView", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaStarBiwuZhanDuiPlatformWnd,"m_ApplicationRetDotObj")
RegistClassMember(LuaStarBiwuZhanDuiPlatformWnd,"m_ApplicationFirstFlag")
function LuaStarBiwuZhanDuiPlatformWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.m_ApplicationRetDotObj = self.Tabs.transform:Find("Tab2/Alert").gameObject
    self.m_ApplicationRetDotObj.gameObject:SetActive(false)
    self.m_ApplicationFirstFlag = false
end

function LuaStarBiwuZhanDuiPlatformWnd:Init()
    for i = 0,self.Tabs.transform.childCount - 1 do
        local tab = self.Tabs.transform:GetChild(i).gameObject
        UIEventListener.Get(tab.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) 
            self:OnChangeTab(i)
        end)
    end
    self:OnChangeTab(CLuaStarBiwuMgr.m_CurOpeZhanDuiWndIndex)
end

function LuaStarBiwuZhanDuiPlatformWnd:OnChangeTitleLabel(name)
    self.TitleLabel.text = name
end

function LuaStarBiwuZhanDuiPlatformWnd:OnChangeTab(index)
    self.Tabs:ChangeTo(index)
    self.SelfZhanDui.gameObject:SetActive(index == 1)
    self.ZhanDuiListView.gameObject:SetActive(index == 0)
end

function LuaStarBiwuZhanDuiPlatformWnd:ReplyStarBiwuZhanDuiInfo()
    if CLuaStarBiwuMgr.m_MySelfZhanduiId <= 0 then
        -- 没有战队,请求受邀列表
        Gac2Gas.QueryStarBiwuMyInviver()
    else
        -- 有战队,请求申请列表
        Gac2Gas.RequestStarBiwuZhanDuiApplyList(1)
    end
end

function LuaStarBiwuZhanDuiPlatformWnd:OnReplyRequestPlayers(players)
    if #players > 0 and not self.m_ApplicationFirstFlag and CLuaStarBiwuMgr.m_IsTeamLeader then
        self.m_ApplicationRetDotObj:SetActive(true)
    else
        self.m_ApplicationRetDotObj:SetActive(false)
    end
end

function LuaStarBiwuZhanDuiPlatformWnd:OnReplyInvitedTeams()
    local teams = CLuaStarBiwuMgr.m_InvitedZhanDuiMemberList
    if #teams > 0 and not self.m_ApplicationFirstFlag and CLuaStarBiwuMgr.m_MySelfZhanduiId <= 0 then
        self.m_ApplicationRetDotObj:SetActive(true)
    else
        self.m_ApplicationRetDotObj:SetActive(false)
    end
end

function LuaStarBiwuZhanDuiPlatformWnd:OnClickRedDot()
    self.m_ApplicationFirstFlag = true
    self.m_ApplicationRetDotObj:SetActive(false)
end

function LuaStarBiwuZhanDuiPlatformWnd:OnEnable()
    g_ScriptEvent:AddListener("OnStarBiWuZhanDuiPlatformClickRedDot",self, "OnClickRedDot")
    g_ScriptEvent:AddListener("OnStarBiWuZhanDuiPlatformWndChange",self, "OnChangeTitleLabel")
    g_ScriptEvent:AddListener("OnStarBiWuZhanDuiPlatformChangeTab",self, "OnChangeTab")
    g_ScriptEvent:AddListener("ReplyStarBiwuZhanDuiInfo", self, "ReplyStarBiwuZhanDuiInfo")
    g_ScriptEvent:AddListener("ReplyStarBiwuZhanDuiList", self, "ReplyStarBiwuZhanDuiInfo")
    g_ScriptEvent:AddListener("ReplyStarBiwuZhanDuiApplyList", self, "OnReplyRequestPlayers")
	g_ScriptEvent:AddListener("ReplyStarBiwuZhanDuiInvitedList", self, "OnReplyInvitedTeams")
end

function LuaStarBiwuZhanDuiPlatformWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnStarBiWuZhanDuiPlatformClickRedDot",self, "OnClickRedDot")
    g_ScriptEvent:RemoveListener("OnStarBiWuZhanDuiPlatformWndChange",self, "OnChangeTitleLabel")
    g_ScriptEvent:RemoveListener("OnStarBiWuZhanDuiPlatformChangeTab",self, "OnChangeTab")
    g_ScriptEvent:RemoveListener("ReplyStarBiwuZhanDuiInfo", self, "ReplyStarBiwuZhanDuiInfo")
    g_ScriptEvent:RemoveListener("ReplyStarBiwuZhanDuiList", self, "ReplyStarBiwuZhanDuiInfo")
    g_ScriptEvent:RemoveListener("ReplyStarBiwuZhanDuiApplyList", self, "OnReplyRequestPlayers")
	g_ScriptEvent:RemoveListener("ReplyStarBiwuZhanDuiInvitedList", self, "OnReplyInvitedTeams")
    CLuaStarBiwuMgr.m_InvitedZhanDuiMemberList = nil
end

--@region UIEvent

--@endregion UIEvent

