local QnCheckBoxGroup = import "L10.UI.QnCheckBoxGroup"
local UIPanel = import "UIPanel"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CButton = import "L10.UI.CButton"
local CGuideMgr=import "L10.Game.Guide.CGuideMgr"

LuaMakeClothesWnd = class()

RegistChildComponent(LuaMakeClothesWnd, "m_Button","Button", CButton)
RegistChildComponent(LuaMakeClothesWnd, "m_MaterialsGroup","MaterialsGroup", QnRadioBox)
RegistChildComponent(LuaMakeClothesWnd, "m_PatternGroup","PatternGroup", QnRadioBox)
RegistChildComponent(LuaMakeClothesWnd, "m_CloseButton","CloseButton", GameObject)
RegistChildComponent(LuaMakeClothesWnd, "m_UndoButton","UndoButton", GameObject)
RegistChildComponent(LuaMakeClothesWnd, "m_ChangePatternButton1","ChangePatternButton1", GameObject)
RegistChildComponent(LuaMakeClothesWnd, "m_ChangePatternButton2","ChangePatternButton2", GameObject)
RegistChildComponent(LuaMakeClothesWnd, "m_ChangePatternButton3","ChangePatternButton3", GameObject)
RegistChildComponent(LuaMakeClothesWnd, "m_ClothesSurface","ClothesSurface", UIPanel)
RegistChildComponent(LuaMakeClothesWnd, "m_ClothesInside","ClothesInside", UIPanel)
RegistChildComponent(LuaMakeClothesWnd, "m_Template","Template", CCommonLuaScript)
RegistChildComponent(LuaMakeClothesWnd, "m_CustomizeImageEditor","CustomizeImageEditor", CCommonLuaScript)
RegistChildComponent(LuaMakeClothesWnd, "m_Clothes","Clothes", CUITexture)
RegistChildComponent(LuaMakeClothesWnd, "m_QnCheckBoxGroup","QnCheckBoxGroup", QnCheckBoxGroup)

RegistClassMember(LuaMakeClothesWnd,"m_Patterns")
RegistClassMember(LuaMakeClothesWnd,"m_IsInside")
RegistClassMember(LuaMakeClothesWnd,"m_CurPatternView")

function LuaMakeClothesWnd:Init()
    LuaMakeClothesMgr.m_SelectMaterial = 1
    LuaMakeClothesMgr.m_SelectPattern = 1
    self.m_Patterns = { {}, {}, {} }

    self:InitMaterialGroup()
    self:InitPatternGroup()

    UIEventListener.Get(self.m_Button.gameObject).onClick = DelegateFactory.VoidDelegate(function()
        self:OnButtonClick()
    end)
    for i = 1,3 do
        Gac2Gas.RequestLoadPictureInfo(CLuaTaskMgr.m_MakeClothesWnd_TaskId, CLuaTaskMgr.m_MakeClothesWnd_ConditionId, i)
    end
    self:UpdateBtnState()
    UIEventListener.Get(self.m_CloseButton).onClick = DelegateFactory.VoidDelegate(function(go)
       self:OnClose()
    end)
    UIEventListener.Get(self.m_UndoButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnUndo()
    end)
    self.m_Template.gameObject:SetActive(false)
    self.m_CustomizeImageEditor.gameObject:SetActive(false)
    LuaMakeClothesMgr.m_PatternsListOnClothes = {}
    CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.MakeClothesPattern)
end

function LuaMakeClothesWnd:OnEnable()
    g_ScriptEvent:AddListener("MakeClothesWnd_OnPictureLoad", self, "OnPictureLoad")
end

function LuaMakeClothesWnd:OnDisable()
    g_ScriptEvent:RemoveListener("MakeClothesWnd_OnPictureLoad", self, "OnPictureLoad")
    for index,viewData in pairs(LuaMakeClothesMgr.m_PatternsListOnClothes) do
        viewData.view = nil
    end
end

function LuaMakeClothesWnd:Update()
    if not self.m_CurPatternView then return end
    local info = self.m_Patterns[LuaMakeClothesMgr.m_SelectPattern].info
    if self.m_IsInside ~= (self.m_CurPatternView.transform.localPosition.y < - 200) then
        self.m_IsInside = self.m_CurPatternView.transform.localPosition.y < - 200
        self.m_CurPatternView.transform.parent = self.m_IsInside and self.m_ClothesInside.transform or self.m_ClothesSurface.transform
        self.m_CurPatternView.gameObject:SetActive(false)
        self.m_CurPatternView.gameObject:SetActive(true)
    end
    if info then
        self.m_CurPatternView:Init({info})
    end
end

function LuaMakeClothesWnd:OnPictureLoad(data)
    if CGuideMgr.Inst:IsInPhase(EnumGuideKey.MakeClothesPattern) then
        CGuideMgr.Inst:EndCurrentPhase()
    end
    local patternIndex,patternInfo = data[1], data[2]
    self.m_Patterns[patternIndex].info = patternInfo
    local resultView = self.m_Patterns[patternIndex].resultView
    resultView:Init({patternInfo})
    self.m_Patterns[patternIndex].panel.gameObject:SetActive(patternInfo.Count > 0)
    self.m_Patterns[patternIndex].pattern = nil
    if patternInfo.Count > 0 then
        self.m_Patterns[patternIndex].pattern = resultView.transform:Find("Root").gameObject
    end
    self.m_Patterns[patternIndex].addRewardSprite.gameObject:SetActive(patternInfo.Count == 0)

    for _,viewData in pairs(LuaMakeClothesMgr.m_PatternsListOnClothes) do
        if viewData.index == patternIndex then
            viewData.view.m_LuaSelf:Init({[0] = patternInfo})
        end
    end

    self.m_ChangePatternButton1:SetActive(self.m_Patterns[1].pattern)
    self.m_ChangePatternButton2:SetActive(self.m_Patterns[2].pattern)
    self.m_ChangePatternButton3:SetActive(self.m_Patterns[3].pattern)

    self:UpdateBtnState()

    for index, t in pairs(self.m_Patterns) do
        if t.pattern then
            CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.MakeClothes)
        end
    end
end

function LuaMakeClothesWnd:UpdateBtnState()
    self.m_Button.Enabled =  (self.m_Patterns[LuaMakeClothesMgr.m_SelectPattern].pattern ~= nil) and (#LuaMakeClothesMgr.m_PatternsListOnClothes > 0)
end

function LuaMakeClothesWnd:InitMaterialGroup()
    self.m_MaterialsGroup.OnSelect = DelegateFactory.Action_QnButton_int(function(btn,index)
        self:OnSelectMaterial(btn, index, 1)
    end)
    local detailtitles = LuaMakeClothesMgr:GetQnCheckBoxGroupDetailtitles()
    self.m_QnCheckBoxGroup:InitWithOptions(detailtitles, false, false)
    self.m_QnCheckBoxGroup.OnSelect = DelegateFactory.Action_QnCheckBox_int(function(checkbox,level)
        self:OnSelectMaterial(checkbox, level, 2)
    end)
    self.m_MaterialsGroup:ChangeTo(LuaMakeClothesMgr.m_SelectMaterial - 1, true)
end

function LuaMakeClothesWnd:InitPatternGroup()
    self.m_PatternGroup.OnSelect = DelegateFactory.Action_QnButton_int(function(btn,index)
        self:OnSelectPattern(btn, index)
    end)
    self.m_PatternGroup:ChangeTo(LuaMakeClothesMgr.m_SelectPattern - 1, false)
    for i = 0, self.m_PatternGroup.m_RadioButtons.Length - 1 do
        local t = self.m_PatternGroup.m_RadioButtons[i].transform
        self.m_Patterns[i + 1].panel = t:Find("MainTexture"):GetComponent(typeof(UIPanel))
        self.m_Patterns[i + 1].addRewardSprite = t:Find("AddRewardSprite").gameObject
        self.m_Patterns[i + 1].resultView = t:Find("MainTexture"):GetComponent(typeof(CCommonLuaScript))
    end
    for i = 0, self.m_PatternGroup.m_RadioButtons.Length - 1 do
        self.m_Patterns[i + 1].panel.gameObject:SetActive(self.m_Patterns[i + 1].pattern)
        self.m_Patterns[i + 1].addRewardSprite.gameObject:SetActive(not self.m_Patterns[i + 1].pattern)
    end
    UIEventListener.Get(self.m_ChangePatternButton1).onClick = DelegateFactory.VoidDelegate(function()
        CUIManager.ShowUI(CLuaUIResources.MakeClothesPatternWnd)
    end)
    self.m_ChangePatternButton1:SetActive(false)
    UIEventListener.Get(self.m_ChangePatternButton2).onClick = DelegateFactory.VoidDelegate(function()
        CUIManager.ShowUI(CLuaUIResources.MakeClothesPatternWnd)
    end)
    self.m_ChangePatternButton2:SetActive(false)
    UIEventListener.Get(self.m_ChangePatternButton3).onClick = DelegateFactory.VoidDelegate(function()
        CUIManager.ShowUI(CLuaUIResources.MakeClothesPatternWnd)
    end)
    self.m_ChangePatternButton3:SetActive(false)
end

function LuaMakeClothesWnd:OnSelectMaterial(btn, index, btnType)
    LuaMakeClothesMgr.m_SelectMaterial = index + 1
    self.m_Clothes:LoadMaterial(LuaMakeClothesMgr:GetClothesMat(LuaMakeClothesMgr.m_SelectMaterial))
    if btnType == 1 then
        self.m_QnCheckBoxGroup:SetSelect(index, false)
    else
        self.m_MaterialsGroup:ChangeTo(index, false)
    end
end

function LuaMakeClothesWnd:OnSelectPattern(btn, index)
    LuaMakeClothesMgr.m_SelectPattern = index + 1
    self:UpdateBtnState()
    if not self.m_Patterns[LuaMakeClothesMgr.m_SelectPattern].pattern then
        self.m_CustomizeImageEditor.gameObject:SetActive(false)
        CUIManager.ShowUI(CLuaUIResources.MakeClothesPatternWnd)
    else
        self:EditPatternOnClothes()
    end
end

function LuaMakeClothesWnd:OnButtonClick()
    self:UpdateBtnState()
    if not self.m_Patterns[LuaMakeClothesMgr.m_SelectPattern].pattern then
        return
    end
    Gac2Gas.FinishEventTaskWithConditionId(CLuaTaskMgr.m_MakeClothesWnd_TaskId,"MakeClothes",tostring(CLuaTaskMgr.m_MakeClothesWnd_ConditionId))
    self:LoadResultData()
    CUIManager.CloseUI(CLuaUIResources.MakeClothesWnd)
    CUIManager.CloseUI(CLuaUIResources.MakeJiaYiWnd)
    CUIManager.ShowUI(CLuaUIResources.MakeClothesResultWnd)
end

function LuaMakeClothesWnd:LoadResultData()
    for index,viewData in pairs(LuaMakeClothesMgr.m_PatternsListOnClothes) do
        viewData.pos = viewData.view.transform.localPosition
        viewData.rot = viewData.view.transform.localEulerAngles
        local tex = viewData.view.gameObject:GetComponent(typeof(UITexture))
        viewData.flip = tex.flip
        viewData.info = self.m_Patterns[viewData.index].info
        viewData.isInSide = viewData.view.transform.localPosition.y < - 200
        viewData.scale = viewData.view.transform:Find("Root").localScale.x
    end
end

function LuaMakeClothesWnd:OnClose()
    local msg = g_MessageMgr:FormatMessage("MakeClothesWnd_CloseWnd_Confirm")
    MessageWndManager.ShowDelayOKCancelMessage(msg, DelegateFactory.Action(function()
        CUIManager.CloseUI(CLuaUIResources.MakeClothesWnd)
        CUIManager.CloseUI(CLuaUIResources.MakeJiaYiWnd)
    end), nil,3)
end

function LuaMakeClothesWnd:OnUndo()
    self:EndEditPatternOnClothes()
    local lastIndex = #LuaMakeClothesMgr.m_PatternsListOnClothes
    if lastIndex > 0 then
        self:DestroyPatternView(LuaMakeClothesMgr.m_PatternsListOnClothes[lastIndex].view)
        table.remove(LuaMakeClothesMgr.m_PatternsListOnClothes, lastIndex)
    end

    self:UpdateBtnState()
end

function LuaMakeClothesWnd:EditPatternOnClothes()
    local info = self.m_Patterns[LuaMakeClothesMgr.m_SelectPattern].info
    if (not info) or (info.Count == 0) then return end
    self.m_Template:Init({[0] = info})
    local instance = NGUITools.AddChild(self.m_ClothesSurface.gameObject, self.m_Template.gameObject)
    instance.transform.localPosition = Vector3(250,200,0)
    self.m_IsInside = instance.transform.localPosition.y < - 200
    instance:SetActive(true)
    self:DestroyPatternView(self.m_CurPatternView)
    self.m_CurPatternView = instance:GetComponent(typeof(CCommonLuaScript))
    self.m_CustomizeImageEditor:Init(function (go)
        table.insert(LuaMakeClothesMgr.m_PatternsListOnClothes,{view = self.m_CurPatternView, index = LuaMakeClothesMgr.m_SelectPattern})
        self.m_CurPatternView = nil
        self:UpdateBtnState()
    end,function (go)
        self:DestroyPatternView(self.m_CurPatternView)
        self.m_CurPatternView = nil
    end, 300, 90, instance.transform:GetComponent(typeof(UITexture)), self.m_ClothesSurface, 900, 900)
end

function LuaMakeClothesWnd:EndEditPatternOnClothes()
    self:DestroyPatternView(self.m_CurPatternView)
    self.m_CurPatternView = nil
    self.m_CustomizeImageEditor.gameObject:SetActive(false)
end

function LuaMakeClothesWnd:DestroyPatternView(view)
    if view then
        GameObject.Destroy(view.gameObject)
    end
end

function LuaMakeClothesWnd:GetGuideGo(methodName)
    if methodName == "GetEditorPatternButton" then
        return self.m_PatternGroup.m_RadioButtons[0].gameObject
    elseif methodName == "GetAddPatternButton" then
        for index, t in pairs(self.m_Patterns) do
            if t.pattern then
                return self.m_PatternGroup.m_RadioButtons[index - 1].gameObject
            end
        end
    elseif methodName == "GetRotateBtn" then
        return self.m_CustomizeImageEditor.transform:Find("RotateBtn").gameObject
    elseif methodName == "GetConfirmPatternButton" then
        return self.m_CustomizeImageEditor.transform:Find("ConfirmBtn").gameObject
    end
end

LuaMakeClothesMgr = class()
LuaMakeClothesMgr.m_SelectPattern = 1
LuaMakeClothesMgr.m_SelectMaterial = 1
LuaMakeClothesMgr.m_PatternsListOnClothes = {}
LuaMakeClothesMgr.m_WndType = 1

function LuaMakeClothesMgr:GetPicture(patternIndex, colorIndex)
    local patternArray = {"butterfly","flower","xianhe","xingxiu"}
    local colorArray ={"white","red","gold"}
    local pattern = patternArray[patternIndex]
    local color = colorArray[colorIndex]
    return SafeStringFormat3("UI/Texture/Transparent/Material/raw_makeclotheswnd_%s_%s.mat",pattern, color)
end

function LuaMakeClothesMgr:GetClothesMat(matIndex)
    local array = {"sha","ma","sichou"}
    local mat = array[matIndex]
    if self.m_WndType == 1 then
        return SafeStringFormat3("UI/Texture/Transparent/Material/raw_makeclotheswnd_yifu_%s.mat",mat)
    end
    return "UI/Texture/Transparent/Material/raw_makeclotheswnd_jiayi.mat"
end

function LuaMakeClothesMgr:GetQnCheckBoxGroupDetailtitles()
    if self.m_WndType == 1 then
        return Table2Array({LocalString.GetString("纱"),LocalString.GetString("棉"),LocalString.GetString("真丝")}, MakeArrayClass(cs_string))
    end
    return Table2Array({LocalString.GetString("嫁衣")}, MakeArrayClass(cs_string))
end
