local Random = import "UnityEngine.Random"
local Object = import "UnityEngine.Object"
local UITexture = import "UITexture"
local Time = import "UnityEngine.Time"
local UICamera = import "UICamera"
local Physics = import "UnityEngine.Physics"
local CUIFx = import "L10.UI.CUIFx"

LuaZhuErDanJieDianXinItem = class()

RegistClassMember(LuaZhuErDanJieDianXinItem, "m_ToyTexture")
RegistClassMember(LuaZhuErDanJieDianXinItem, "m_ToyType")
RegistClassMember(LuaZhuErDanJieDianXinItem, "m_TargetTexture")
RegistClassMember(LuaZhuErDanJieDianXinItem, "m_HitFx")

RegistClassMember(LuaZhuErDanJieDianXinItem, "m_Rotation")
RegistClassMember(LuaZhuErDanJieDianXinItem, "m_Scale")
RegistClassMember(LuaZhuErDanJieDianXinItem, "m_DropSpeed")
RegistClassMember(LuaZhuErDanJieDianXinItem, "m_LargenSpeed")

RegistClassMember(LuaZhuErDanJieDianXinItem, "m_LifeTime")
RegistClassMember(LuaZhuErDanJieDianXinItem, "m_Fin")
RegistClassMember(LuaZhuErDanJieDianXinItem, "m_Score")

function LuaZhuErDanJieDianXinItem:Init(args)
	if args and args.Length > 0 then
		self.m_Score = args[0]
	else
		self.m_Score = 0
	end

	self.m_LifeTime = 5
	Object.Destroy(self.gameObject, self.m_LifeTime)
end
function LuaZhuErDanJieDianXinItem:Awake()


	self.m_DropSpeed = Random.Range(500 - 100, 500 + 100)
	self.m_LargenSpeed = 0.1
	self.m_Rotation = Random.Range(-30, 30)
	self.m_Scale = Random.Range(0.4, 0.6)


	self.m_Fin = false
end

function LuaZhuErDanJieDianXinItem:OnEnable()
	self.m_ToyTexture = self.transform:GetComponent(typeof(UITexture))

	self.m_HitFx = self.transform:Find("HitFx"):GetComponent(typeof(CUIFx))
	self.m_HitFx:DestroyFx()

	self.m_ToyTexture.transform.localScale = Vector3(self.m_Scale, self.m_Scale, self.m_Scale)
	self.m_ToyTexture.transform.localEulerAngles = Vector3(0, 0, self.m_Rotation)

	g_ScriptEvent:AddListener("ZhuErDan_JieDianXin_Finish", self, "OnGameFinish")
end
function LuaZhuErDanJieDianXinItem:OnDisable()
	g_ScriptEvent:RemoveListener("ZhuErDan_JieDianXin_Finish", self, "OnGameFinish")
end

function LuaZhuErDanJieDianXinItem:OnGameFinish(success)
	self.m_Fin = true
end

function LuaZhuErDanJieDianXinItem:Update()
	if self.m_Score==0 then return end

	if not self.m_Fin then
		local newPos = Vector3(self.transform.localPosition.x, self.transform.localPosition.y - self.m_DropSpeed * Time.deltaTime,  0)
		self.transform.localPosition = newPos

		local screenPos = UICamera.currentCamera:WorldToScreenPoint(self.transform.position)
		local ray = UICamera.currentCamera:ScreenPointToRay(Vector3(screenPos.x, screenPos.y, 0))
		local hits = Physics.RaycastAll(ray, 99999,  UICamera.currentCamera.cullingMask)

		if not self.m_TargetTexture then
			local go = CUIManager.instance:GetGameObject(CLuaUIResources.ZhuErDanJieDianXinWnd)
			if go then
				self.m_TargetTexture = go.transform:Find("Player/PlayerTexture").gameObject
			end
		end
		if not self.m_TargetTexture then
			return
		end

		for i = 0, hits.Length - 1 do
			if hits[i].collider.gameObject == self.m_TargetTexture then
				if not self.m_Score then
				else
					if self.m_Score ~= 0 then
						self.m_HitFx:LoadFx("Fx/UI/Prefab/UI_jieyuanbao.prefab")
						g_ScriptEvent:BroadcastInLua("ZhuErDanJieDianXinAddScore",self.m_Score)
					end
					self.m_Fin = true
					Object.Destroy(self.gameObject, 0.5)
				end
			end
		end

		local newScale =self.transform.localScale.x + self.m_LargenSpeed * Time.deltaTime
		self.m_ToyTexture.transform.localScale = Vector3(newScale, newScale, newScale)
	end
end
