local QnTableView=import "L10.UI.QnTableView"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local UILabel = import "UILabel"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local EnumQualityType = import "L10.Game.EnumQualityType"
local QualityColor = import "L10.Game.QualityColor"
local NGUIText = import "NGUIText"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local StringStringKeyValuePair = import "L10.Game.StringStringKeyValuePair"

LuaZongMenJusticeContributeChooseWnd=class()
RegistClassMember(LuaZongMenJusticeContributeChooseWnd,"m_CommitBtn")
RegistClassMember(LuaZongMenJusticeContributeChooseWnd,"m_CommitBtnLabel")
RegistClassMember(LuaZongMenJusticeContributeChooseWnd,"m_VoidLabel")
RegistClassMember(LuaZongMenJusticeContributeChooseWnd,"m_TableView")
RegistClassMember(LuaZongMenJusticeContributeChooseWnd,"m_TitleLabel")
RegistClassMember(LuaZongMenJusticeContributeChooseWnd,"m_DescLabel")
RegistClassMember(LuaZongMenJusticeContributeChooseWnd,"m_CloseBtn")
--打开界面自动选中的第一个不显示info
RegistClassMember(LuaZongMenJusticeContributeChooseWnd,"m_IgnoreShowInfo")
--当前选中对象 一个Table
--单选时 永远只保存一个对象
RegistClassMember(LuaZongMenJusticeContributeChooseWnd,"m_CurrentMonsterData")
--展示的MonsterList
RegistClassMember(LuaZongMenJusticeContributeChooseWnd,"m_MonsterList")
RegistClassMember(LuaZongMenJusticeContributeChooseWnd,"m_Info2ColorEnum")

function LuaZongMenJusticeContributeChooseWnd:InitComponents()
    self.m_TableView = self.transform:Find("Anchor/Offset/TableView"):GetComponent(typeof(QnTableView))
    self.m_CommitBtn = self.transform:Find("Anchor/Offset/CommitBtn").gameObject
    self.m_CommitBtnLabel = self.transform:Find("Anchor/Offset/CommitBtn/Label"):GetComponent(typeof(UILabel))
    self.m_VoidLabel = self.transform:Find("Anchor/Offset/Bg/VoidLabel"):GetComponent(typeof(UILabel))

    self.m_TitleLabel = self.transform:Find("Anchor/Offset/Bg/TitleLabel"):GetComponent(typeof(UILabel))
    self.m_DescLabel = self.transform:Find("Anchor/Offset/DescLabel"):GetComponent(typeof(UILabel))
    self.m_CloseBtn = self.transform:Find("Anchor/Offset/CloseBtn").gameObject
end

function LuaZongMenJusticeContributeChooseWnd:Init()
    self:InitComponents()
    self.m_Info2ColorEnum = {}

    UIEventListener.Get(self.m_CommitBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        self:OnCommit()
    end)

    UIEventListener.Get(self.m_CloseBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        self:OnClose()
    end)

    self.m_TableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
        local monsterData = self.m_MonsterList[row+1]
        if self.m_IgnoreShowInfo == false then
            local time = CServerTimeMgr.ConvertTimeStampToZone8Time(monsterData.CatchTime)
            local labels = {}
            table.insert(labels, CreateFromClass(StringStringKeyValuePair, LocalString.GetString("评级"), tostring(ZhuoYao_TuJian.GetData(monsterData.TemplateId).Grade)))
            table.insert(labels, CreateFromClass(StringStringKeyValuePair, LocalString.GetString("怪物品质"), tostring(monsterData.Quality)))
            table.insert(labels, CreateFromClass(StringStringKeyValuePair, LocalString.GetString("捕获时间"), ToStringWrap(time, "yyyy-MM-dd")))
            LuaZhuoYaoMgr:ShowTip(monsterData, labels)
        end
        local tableItem = self.m_TableView:GetItemAtRow(row)
        self.m_CurrentMonsterData = monsterData

        self:OnItemClick()
    end)
    
    self:InitStatus()
end

function LuaZongMenJusticeContributeChooseWnd:InitStatus()
    self.m_MonsterList = {}
    self.m_CurrentMonsterData = nil

    for k,v in pairs(LuaZhuoYaoMgr.m_WarehouseYaoGuaiList) do
        table.insert(self.m_MonsterList, v)
    end

    if #self.m_MonsterList == 0 then
        self.m_VoidLabel.gameObject:SetActive(true)
        CUICommonDef.SetActive(self.m_CommitBtn, false, true)
        self.m_DescLabel.text = ""
    else
        self.m_VoidLabel.gameObject:SetActive(false)
        CUICommonDef.SetActive(self.m_CommitBtn, true, true)
    end

    -- 排序
    table.sort(self.m_MonsterList, function(monster1, monster2)
        if monster1.Quality ~= monster2.Quality then
            return monster1.Quality > monster2.Quality
        elseif monster1.Level ~= monster2.Level then
            return monster1.Level > monster2.Level
        elseif monster1.MonsterTempId ~= monster2.MonsterTempId then
            return monster1.MonsterTempId > monster2.MonsterTempId
        else
            return false
        end
    end)

    self.m_TableView.m_DataSource=DefaultTableViewDataSource.Create(
        function()
            return #self.m_MonsterList
        end,
        
        function(item,index)
            self:InitItem(item.transform, index)
        end)

    self.m_TableView:ReloadData(true, false)
    self.m_IgnoreShowInfo = true
    self.m_TableView:SetSelectRow(0, true)
    self.m_IgnoreShowInfo = false
    if CUIManager.IsLoaded(CLuaUIResources.YaoGuaiInfoWnd) then
        CUIManager.CloseUI(CLuaUIResources.YaoGuaiInfoWnd)
    end
end

function LuaZongMenJusticeContributeChooseWnd:InitItem(transform, index)
    local iconTexture = transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    local qualitySprite = transform:Find("QualitySprite"):GetComponent(typeof(UISprite))
    local monsterData = self.m_MonsterList[index+1]
    local data = ZhuoYao_TuJian.GetData(monsterData.TemplateId)
    iconTexture:LoadNPCPortrait(data.Icon)
    local qualityBorder, colorEnum = LuaZhuoYaoMgr:GetYaoGuaiQualityBorderColor(data, monsterData.Quality)
    qualitySprite.spriteName = qualityBorder
    self.m_Info2ColorEnum[monsterData] = colorEnum
end

function LuaZongMenJusticeContributeChooseWnd:OnCommit()
    local sectId = ((CClientMainPlayer.Inst or {}).BasicProp or {}).SectId
    if sectId ~= nil then
        local colorEnum = self.m_Info2ColorEnum[self.m_CurrentMonsterData]
        if colorEnum == EnumQualityType.Red or colorEnum == EnumQualityType.Purple then
            local data = ZhuoYao_TuJian.GetData(self.m_CurrentMonsterData.TemplateId)
            local colorName = SafeStringFormat3("[c][%s]%s[-][/c]", NGUIText.EncodeColor24(QualityColor.GetRGBValue(colorEnum)), data.Name)
            local message = g_MessageMgr:FormatMessage("Justice_Contribute_SubmitConfirm", colorName)
            MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function () 
                Gac2Gas.RequestSubmitSectEvilYaoGuai(sectId, 0, 0, self.m_CurrentMonsterData.Id)
            end), nil, nil, nil, false)
        else
            Gac2Gas.RequestSubmitSectEvilYaoGuai(sectId, 0, 0, self.m_CurrentMonsterData.Id)
        end
    end
end

function LuaZongMenJusticeContributeChooseWnd:OnClose()
    CUIManager.CloseUI(CLuaUIResources.ZongMenJusticeContributeChooseWnd)
end

function LuaZongMenJusticeContributeChooseWnd:OnItemClick()
    local shortenValue = LuaZongMenMgr:GetCultShortenValue(self.m_CurrentMonsterData.Quality)
    self.m_DescLabel.text = SafeStringFormat3(LocalString.GetString("[ACF9FF]提交后邪派值[-] %+d"), -shortenValue)
end

function LuaZongMenJusticeContributeChooseWnd:OnEnable()
	g_ScriptEvent:AddListener("OnWarehouseYaoGuaiListUpdate", self, "InitStatus")
end

function LuaZongMenJusticeContributeChooseWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnWarehouseYaoGuaiListUpdate", self, "InitStatus")
end
