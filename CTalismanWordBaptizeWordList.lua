-- Auto Generated!!
local CButton = import "L10.UI.CButton"
local CItemMgr = import "L10.Game.CItemMgr"
local CTalismanBaptizeWordCell = import "L10.UI.CTalismanBaptizeWordCell"
local CTalismanWordBaptizeMgr = import "L10.UI.CTalismanWordBaptizeMgr"
local CTalismanWordBaptizeWordList = import "L10.UI.CTalismanWordBaptizeWordList"
local Extensions = import "Extensions"
local Gac2Gas = import "L10.Game.Gac2Gas"
local IdPartition = import "L10.Game.IdPartition"
local LocalString = import "LocalString"
local NGUITools = import "NGUITools"
local Word_Word = import "L10.Game.Word_Word"
CTalismanWordBaptizeWordList.m_Init_CS2LuaHook = function (this) 

    this.wordCell:SetActive(false)
    this.oldScoreLabel.text = ""
    Extensions.RemoveAllChildren(this.oldTable.transform)
    this.newScoreLabel.text = ""
    Extensions.RemoveAllChildren(this.newTable.transform)
    local default
    if CTalismanWordBaptizeMgr.baseWordBaptize then
        default = LocalString.GetString("原基础属性")
    else
        default = LocalString.GetString("原额外属性")
    end
    this.oldWordTitleLabel.text = default
    local extern
    if CTalismanWordBaptizeMgr.baseWordBaptize then
        extern = LocalString.GetString("新基础属性")
    else
        extern = LocalString.GetString("新额外属性")
    end
    this.newWordTitleLabel.text = extern
    if CTalismanWordBaptizeMgr.selectTalisman == nil then
        return
    end
    this.talisman = CItemMgr.Inst:GetById(CTalismanWordBaptizeMgr.selectTalisman.itemId)
    if this.talisman ~= nil and this.talisman.IsEquip and IdPartition.IdIsTalisman(this.talisman.TemplateId) then
        this:UpdateOldWordList()
        this:UpdateNewWordList()
    else
        CommonDefs.GetComponent_GameObject_Type(this.replaceButton, typeof(CButton)).Enabled = false
    end
end
CTalismanWordBaptizeWordList.m_UpdateOldWordList_CS2LuaHook = function (this) 
    this.oldScoreLabel.text = System.String.Format(LocalString.GetString("评分：{0}"), this.talisman.Equip.Score)
    CTalismanWordBaptizeMgr.preScore = this.talisman.Equip.Score

    Extensions.RemoveAllChildren(this.oldTable.transform)
    local default
    if CTalismanWordBaptizeMgr.baseWordBaptize then
        default = this.talisman.Equip:GetFixedWordArray(false)
    else
        default = this.talisman.Equip:GetExtWordArray(false)
    end
    local wordContents = default
    local wordMaxTable = CLuaEquipMgr:EquipWordIdIsMaxWord(CTalismanWordBaptizeMgr.selectTalisman.itemId,Array2Table(wordContents))
    do
        local i = 0
        while i < wordContents.Length do
            local word = Word_Word.GetData(wordContents[i])
            if word ~= nil then
                local instance = NGUITools.AddChild(this.oldTable.gameObject, this.wordCell)
                instance:SetActive(true)
                CommonDefs.GetComponent_GameObject_Type(instance, typeof(CTalismanBaptizeWordCell)):Init(word, this.talisman.Equip.DisplayColor,wordMaxTable[i+1])
            end
            i = i + 1
        end
    end
    this.oldTable:Reposition()
    this.oldScrollView:ResetPosition()
end
CTalismanWordBaptizeWordList.m_UpdateNewWordList_CS2LuaHook = function (this) 

    this.newScoreLabel.text = ""
    Extensions.RemoveAllChildren(this.newTable.transform)

    if CTalismanWordBaptizeMgr.baseWordBaptize then
        if this.talisman.Equip.TempScoreWithFixTempWord > 0 then
            this.newScoreLabel.text = System.String.Format(LocalString.GetString("评分：{0}"), this.talisman.Equip.TempScoreWithFixTempWord)
            CTalismanWordBaptizeMgr.postScore = this.talisman.Equip.TempScoreWithFixTempWord
        end
    else
        if this.talisman.Equip.TempScoreWithExtraTempWord > 0 then
            this.newScoreLabel.text = System.String.Format(LocalString.GetString("评分：{0}"), this.talisman.Equip.TempScoreWithExtraTempWord)
            CTalismanWordBaptizeMgr.postScore = this.talisman.Equip.TempScoreWithExtraTempWord
        end
    end

    local default
    if CTalismanWordBaptizeMgr.baseWordBaptize then
        default = this.talisman.Equip:GetTempFixedWordArray()
    else
        default = this.talisman.Equip:GetTempExtWordArray()
    end
    local wordContents = default
    local wordCount = 0
    local baseList
    local adList
    if CTalismanWordBaptizeMgr.baseWordBaptize then
        baseList = this.talisman.Equip:GetTempFixedWordArray()
        adList = this.talisman.Equip:GetExtWordArray(false)
    else
        baseList = this.talisman.Equip:GetFixedWordArray(false)
        adList = this.talisman.Equip:GetTempExtWordArray()
    end
    do
        local i = 0
        while i < baseList.Length do
            local word = Word_Word.GetData(baseList[i])
            if word ~= nil then
                wordCount = wordCount + 1
            end
            i = i + 1
        end
    end
    do
        local i = 0
        while i < adList.Length do
            local word = Word_Word.GetData(adList[i])
            if word ~= nil then
                wordCount = wordCount + 1
            end
            i = i + 1
        end
    end

    local wordMaxTable = CLuaEquipMgr:EquipWordIdIsMaxWord(CTalismanWordBaptizeMgr.selectTalisman.itemId,Array2Table(wordContents))

    local count = 0
    do
        local i = 0
        while i < wordContents.Length do
            local word = Word_Word.GetData(wordContents[i])
            if word ~= nil then
                local instance = NGUITools.AddChild(this.newTable.gameObject, this.wordCell)
                instance:SetActive(true)
                CommonDefs.GetComponent_GameObject_Type(instance, typeof(CTalismanBaptizeWordCell)):Init(word, this.talisman.Equip:GetNewColor(wordCount),wordMaxTable[i+1])
                count = count + 1
            end
            i = i + 1
        end
    end
    CommonDefs.GetComponent_GameObject_Type(this.replaceButton, typeof(CButton)).Enabled = count > 0
    if this.newTable.transform.childCount == 0 then
        this.newScoreLabel.text = ""
    end
    this.newTable:Reposition()
    this.newScrollView:ResetPosition()
end
CTalismanWordBaptizeWordList.m_OnReplaceButtonClick_CS2LuaHook = function (this, go) 

    if CTalismanWordBaptizeMgr.selectTalisman == nil then
        return
    end
    this.needConfirm = false
    if CTalismanWordBaptizeMgr.baseWordBaptize then
        Gac2Gas.ConfirmResetTalismanBasicWord(EnumToInt(CTalismanWordBaptizeMgr.selectTalisman.place), CTalismanWordBaptizeMgr.selectTalisman.pos, CTalismanWordBaptizeMgr.selectTalisman.itemId, true)
    else
        Gac2Gas.ConfirmResetTalismanExtraWord(EnumToInt(CTalismanWordBaptizeMgr.selectTalisman.place), CTalismanWordBaptizeMgr.selectTalisman.pos, CTalismanWordBaptizeMgr.selectTalisman.itemId, true)
    end
end
