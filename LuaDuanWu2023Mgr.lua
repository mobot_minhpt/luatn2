local CClientPlayerMgr   = import "L10.Game.CClientPlayerMgr"
local CClientOtherPlayer = import "L10.Game.CClientOtherPlayer"

LuaDuanWu2023Mgr = {}

LuaDuanWu2023Mgr.pveResultInfo = {}
LuaDuanWu2023Mgr.pvpveInfo = nil
LuaDuanWu2023Mgr.pvpveCompareResultInfo = {}
LuaDuanWu2023Mgr.pvpveResultInfo = {}

function LuaDuanWu2023Mgr:ShowPVPVERuleWnd()
    local imagePaths = {
        "UI/Texture/FestivalActivity/Festival_DuanWu/DuanWu2023/Material/duanwu2023pvpveenterwnd_guide_01.mat",
        "UI/Texture/FestivalActivity/Festival_DuanWu/DuanWu2023/Material/duanwu2023pvpveenterwnd_guide_02.mat",
        "UI/Texture/FestivalActivity/Festival_DuanWu/DuanWu2023/Material/duanwu2023pvpveenterwnd_guide_03.mat",
        "UI/Texture/FestivalActivity/Festival_DuanWu/DuanWu2023/Material/duanwu2023pvpveenterwnd_guide_04.mat",
    }
    local msgs = {
        g_MessageMgr:FormatMessage("DUANWU_2023_PVPVE_ENTER_WND_GUIDE_01"),
        g_MessageMgr:FormatMessage("DUANWU_2023_PVPVE_ENTER_WND_GUIDE_02"),
        g_MessageMgr:FormatMessage("DUANWU_2023_PVPVE_ENTER_WND_GUIDE_03"),
        g_MessageMgr:FormatMessage("DUANWU_2023_PVPVE_ENTER_WND_GUIDE_04"),
    }
    LuaImageRuleMgr:ShowCommonImageRuleWnd(imagePaths, msgs)
end

function LuaDuanWu2023Mgr:ShowPVPVESkillSelectWnd(data)
    CUIManager.CloseUI(CLuaUIResources.CommonImageRuleWnd)
    LuaDuanWu2023Mgr.skillSelectEndTimeStamp = g_MessagePack.unpack(data)[1]
    CUIManager.ShowUI(CLuaUIResources.DuanWu2023PVPVESkillSelectWnd)
end

function LuaDuanWu2023Mgr:SortTeamInfoList(teamInfoList)
    table.sort(teamInfoList, function (a, b)
        return a.score > b.score
    end)
    return teamInfoList
end

-- PVPVE中定身技能只能给敌方玩家使用
function LuaDuanWu2023Mgr:CanCastSkillInPVPVE(skillId)
    local skillCls = math.floor(skillId / 100)
    if skillCls ~= 980493 then
        return true
    end

    local target = CClientMainPlayer.Inst.Target
    if not target then
        return true
    end

    if TypeIs(target, typeof(CClientOtherPlayer)) then
        if self.pvpveInfo and not self.pvpveInfo.myTeamPlayerIds[target.PlayerId] then
            return true
        end
    end
    g_MessageMgr:ShowMessage("DUANWU_2023_PVPVE_INVALID_TARGET")
    return false
end

function LuaDuanWu2023Mgr:GetPVEGameplayId()
    return Duanwu2023_EndlessChallengeSetting.GetData().GamePlayId
end

function LuaDuanWu2023Mgr:OnPVERuleButtonClick()
    g_MessageMgr:ShowMessage("DUANWU_2023_PVE_RULE")
end

--#region Gas2Gac

function LuaDuanWu2023Mgr:SyncEndlessChallengeResult(bossNumber, isGetBaseDailyReward, isGetAdvancedDailyReward)
    self.pveResultInfo = {
        bossNumber = bossNumber,
        isGetBaseDailyReward = isGetBaseDailyReward,
        isGetAdvancedDailyReward = isGetAdvancedDailyReward,
    }
    CUIManager.ShowUI(CLuaUIResources.DuanWu2023PVEResultWnd)
end

function LuaDuanWu2023Mgr:SyncDuanWu2023PVPVEScore(teamId, teamOneScore, teamTwoScore, scoreInfo_U)
    local scoreInfo = g_MessagePack.unpack(scoreInfo_U)
    local teamInfoList = {}
    local myTeamPlayerIds = {}
    for i = 1, #scoreInfo, 4 do
        table.insert(teamInfoList, {
            playerId = scoreInfo[i],
            name = scoreInfo[i + 1],
            score = scoreInfo[i + 2],
            contribution = scoreInfo[i + 3],
        })
        myTeamPlayerIds[scoreInfo[i]] = true
    end

    self.pvpveInfo = {
        teamId = teamId,
        team1Score = teamOneScore,
        team2Score = teamTwoScore,
        teamInfoList = self:SortTeamInfoList(teamInfoList),
        myTeamPlayerIds = myTeamPlayerIds,
    }
    g_ScriptEvent:BroadcastInLua("SyncDuanWu2023PVPVEScore", 0)
end

function LuaDuanWu2023Mgr:SyncDuanWu2023PVPVESelfTeamScoreChange(teamScore, playerId, playerScore, playerContribution, scoreStatus)
    if self.pvpveInfo then
        if self.pvpveInfo.teamId == 1 then
            self.pvpveInfo.team1Score = teamScore
        else
            self.pvpveInfo.team2Score = teamScore
        end

        local teamInfoList = self.pvpveInfo.teamInfoList
        for _, data in ipairs(teamInfoList) do
            if data.playerId == playerId then
                data.score = playerScore
                data.contribution = playerContribution
            end
        end
        self.pvpveInfo.teamInfoList = self:SortTeamInfoList(teamInfoList)
        g_ScriptEvent:BroadcastInLua("SyncDuanWu2023PVPVEScore", scoreStatus)
    end
end

function LuaDuanWu2023Mgr:SyncDuanWu2023PVPVEOtherTeamScoreChange(teamScore, scoreStatus)
    if self.pvpveInfo then
        if self.pvpveInfo.teamId == 1 then
            self.pvpveInfo.team2Score = teamScore
        else
            self.pvpveInfo.team1Score = teamScore
        end
        g_ScriptEvent:BroadcastInLua("SyncDuanWu2023PVPVEScore", scoreStatus)
    end
end

function LuaDuanWu2023Mgr:SyncDuanWu2023PVPVECompareResult(winnerId, winnerScore, loserId, loserScore, changeScore)
    if not CClientMainPlayer.Inst then return end

    local meIsWinner = CClientMainPlayer.Inst.Id == winnerId
    local otherId = meIsWinner and loserId or winnerId
    local otherPlayer = CClientPlayerMgr.Inst:GetPlayer(otherId)
    if not otherPlayer then return end

    self.pvpveCompareResultInfo = {
        meIsWinner = meIsWinner,
        myScore = meIsWinner and winnerScore or loserScore,
        otherScore = meIsWinner and loserScore or winnerScore,
        otherPlayerName = otherPlayer.Name,
        otherPlayerClass= otherPlayer.Class,
        otherPlayerGender= otherPlayer.Gender,
        changeScore = changeScore,
    }
    CUIManager.ShowUI(CLuaUIResources.DuanWu2023PVPVECompareResultWnd)
end

function LuaDuanWu2023Mgr:SyncDuanWu2023PVPVEPlayResult(getScore, dailyScore, winTeamId, teamOneScore, teamTwoScore, teamOneInfo_U, teamTwoInfo_U)
    if not self.pvpveInfo then return end

    local team1InfoList = self:GetTeamInfoList(g_MessagePack.unpack(teamOneInfo_U))
    local team2InfoList = self:GetTeamInfoList(g_MessagePack.unpack(teamTwoInfo_U))

    local myTeamId = self.pvpveInfo.teamId
    local meIsWinner = myTeamId == winTeamId
    self.pvpveResultInfo = {
        meIsWinner = meIsWinner,
        getScore = getScore,
        dailyScore = dailyScore,
        myTeamScore = myTeamId == 1 and teamOneScore or teamTwoScore,
        otherTeamScore = myTeamId == 1 and teamTwoScore or teamOneScore,
        myTeamInfo = myTeamId == 1 and team1InfoList or team2InfoList,
        otherTeamInfo = myTeamId == 1 and team2InfoList or team1InfoList,
    }
    CUIManager.ShowUI(CLuaUIResources.DuanWu2023PVPVEResultWnd)
end

function LuaDuanWu2023Mgr:GetTeamInfoList(teamInfo)
    local list = {}
    for i = 1, #teamInfo, 6 do
        table.insert(list, {
            playerId = teamInfo[i],
            name = teamInfo[i + 1],
            class = teamInfo[i + 2],
            serverName = teamInfo[i + 3],
            score = teamInfo[i + 4],
            contribution = teamInfo[i + 5],
        })
    end

    table.sort(list, function (a, b)
            return a.score > b.score
    end)
    return list
end

--#endregion Gas2Gac
