local DelegateFactory = import "DelegateFactory"
local TweenAlpha      = import "TweenAlpha"
local CServerTimeMgr  = import "L10.Game.CServerTimeMgr"
local CItemInfoMgr    = import "L10.UI.CItemInfoMgr"
local AlignType       = import "L10.UI.CItemInfoMgr+AlignType"

LuaDouDiZhuBaoXiangResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaDouDiZhuBaoXiangResultWnd, "base")
RegistClassMember(LuaDouDiZhuBaoXiangResultWnd, "win")
RegistClassMember(LuaDouDiZhuBaoXiangResultWnd, "civilianWin")
RegistClassMember(LuaDouDiZhuBaoXiangResultWnd, "landlordWin")
RegistClassMember(LuaDouDiZhuBaoXiangResultWnd, "fail")
RegistClassMember(LuaDouDiZhuBaoXiangResultWnd, "openBox")
RegistClassMember(LuaDouDiZhuBaoXiangResultWnd, "numDesc")
RegistClassMember(LuaDouDiZhuBaoXiangResultWnd, "openBoxClickTip")
RegistClassMember(LuaDouDiZhuBaoXiangResultWnd, "boxFx")
RegistClassMember(LuaDouDiZhuBaoXiangResultWnd, "result")
RegistClassMember(LuaDouDiZhuBaoXiangResultWnd, "awardTweenAlpha")
RegistClassMember(LuaDouDiZhuBaoXiangResultWnd, "awardGrid")
RegistClassMember(LuaDouDiZhuBaoXiangResultWnd, "awardTemplate")
RegistClassMember(LuaDouDiZhuBaoXiangResultWnd, "resultClickTip")
RegistClassMember(LuaDouDiZhuBaoXiangResultWnd, "boxOpenFx")

RegistClassMember(LuaDouDiZhuBaoXiangResultWnd, "openedCount")
RegistClassMember(LuaDouDiZhuBaoXiangResultWnd, "tick")
RegistClassMember(LuaDouDiZhuBaoXiangResultWnd, "endTimeStamp")

function LuaDouDiZhuBaoXiangResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

    self:InitUIComponents()
    self:InitEventListener()
    self:InitActive()
end

function LuaDouDiZhuBaoXiangResultWnd:InitUIComponents()
    self.base = self.transform:Find("Base")
    self.win = self.base:Find("Win")
    self.civilianWin = self.win:Find("CivilianWin").gameObject
    self.landlordWin = self.win:Find("LandlordWin").gameObject
    self.fail = self.base:Find("Fail")

    self.openBox = self.transform:Find("OpenBox")
    self.numDesc = self.openBox:Find("NumDesc"):GetComponent(typeof(UILabel))
    self.openBoxClickTip = self.openBox:Find("ClickTip"):GetComponent(typeof(UILabel))
    self.boxFx = self.openBox:Find("Fx"):GetComponent(typeof(CUIFx))

    self.result = self.transform:Find("Result")
    self.awardTweenAlpha = self.result:Find("Award"):GetComponent(typeof(TweenAlpha))
    self.awardGrid = self.result:Find("Award/Grid"):GetComponent(typeof(UIGrid))
    self.awardTemplate = self.result:Find("AwardTemplate").gameObject
    self.resultClickTip = self.result:Find("ClickTip"):GetComponent(typeof(UILabel))
    self.boxOpenFx = self.result:Find("Fx"):GetComponent(typeof(CUIFx))
end

function LuaDouDiZhuBaoXiangResultWnd:InitActive()
    self.awardTemplate:SetActive(false)
    self:SetWndActive(true, false, false)
end

function LuaDouDiZhuBaoXiangResultWnd:InitEventListener()
    UIEventListener.Get(self.base.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBaseClick()
	end)

    UIEventListener.Get(self.openBox.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOpenBoxClick()
	end)

    UIEventListener.Get(self.result.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnResultClick()
	end)
end

function LuaDouDiZhuBaoXiangResultWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncQiangXiangZiOpenChestResult", self, "OnSyncQiangXiangZiOpenChestResult")
end

function LuaDouDiZhuBaoXiangResultWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncQiangXiangZiOpenChestResult", self, "OnSyncQiangXiangZiOpenChestResult")
end

function LuaDouDiZhuBaoXiangResultWnd:OnSyncQiangXiangZiOpenChestResult(remainBoxCount, awards)
    local info = LuaDouDiZhuBaoXiangMgr.resultInfo

    self:EnterResult()
    self.openedCount = info.allBoxCount - remainBoxCount

    Extensions.RemoveAllChildren(self.awardGrid.transform)
    for k, v in pairs(awards) do
        local child = NGUITools.AddChild(self.awardGrid.gameObject, self.awardTemplate)
        child:SetActive(true)

        local item = Item_Item.GetData(k)

        child.transform:Find("Icon"):GetComponent(typeof(CUITexture)):LoadMaterial(item.Icon)
        child.transform:Find("Count"):GetComponent(typeof(UILabel)).text = SafeStringFormat3("X%d", v)

        UIEventListener.Get(child).onClick = DelegateFactory.VoidDelegate(function(go)
            CItemInfoMgr.ShowLinkItemTemplateInfo(k, false, nil, AlignType.Default, 0, 0, 0, 0)
        end)
    end
    self.awardGrid:Reposition()

    if remainBoxCount > 0 then
        self.resultClickTip.text = SafeStringFormat3(LocalString.GetString("点击继续开启宝箱 %d/%d"), self.openedCount, info.allBoxCount)
    else
        self.resultClickTip.text = LocalString.GetString("点击任意处关闭")
    end
end


function LuaDouDiZhuBaoXiangResultWnd:Init()
    self.openedCount = 0

    local info = LuaDouDiZhuBaoXiangMgr.resultInfo
    self.win.gameObject:SetActive(info.isWin)
    self.fail.gameObject:SetActive(not info.isWin)

    if info.isWin then
        self.landlordWin:SetActive(info.isDiZhu)
        self.civilianWin:SetActive(not info.isDiZhu)
    end
end

function LuaDouDiZhuBaoXiangResultWnd:SetWndActive(bBase, bOpenBox, bResult)
    self.base.gameObject:SetActive(bBase)
    self.openBox.gameObject:SetActive(bOpenBox)
    self.result.gameObject:SetActive(bResult)
end

function LuaDouDiZhuBaoXiangResultWnd:EnterOpenBox()
    self:SetWndActive(false, true, false)
    self.boxFx:LoadFx(g_UIFxPaths.DouDiZhuBaoXiangDouDongFxPath)
    self.boxOpenFx:DestroyFx()
end

function LuaDouDiZhuBaoXiangResultWnd:EnterResult()
    self:SetWndActive(false, false, true)
    self.boxFx:DestroyFx()
    self.boxOpenFx:LoadFx(g_UIFxPaths.DouDiZhuBaoXiangDaKaiFxPath)
end

function LuaDouDiZhuBaoXiangResultWnd:StartTick()
    self:ClearTick()

    self.endTimeStamp = CServerTimeMgr.Inst.timeStamp + 5
    self:UpdateOpenBoxClickTip()
    self.tick = RegisterTick(function()
        self:UpdateOpenBoxClickTip()
    end, 1000)
end

function LuaDouDiZhuBaoXiangResultWnd:UpdateOpenBoxClickTip()
    local leftTime = math.floor(self.endTimeStamp - CServerTimeMgr.Inst.timeStamp)

    if leftTime <= 0 then
        self:OnOpenBoxClick()
        return
    end

    self.openBoxClickTip.text = SafeStringFormat3(LocalString.GetString("点击打开宝箱 %d/%d（%d秒）"), self.openedCount,
        LuaDouDiZhuBaoXiangMgr.resultInfo.allBoxCount, leftTime)
end

function LuaDouDiZhuBaoXiangResultWnd:ClearTick()
    if self.tick then
        UnRegisterTick(self.tick)
        self.tick = nil
    end
end

function LuaDouDiZhuBaoXiangResultWnd:OnDestroy()
    self:ClearTick()
    Gac2Gas.RequestQiangXiangZiOpenAllChest()
end

--@region UIEvent

function LuaDouDiZhuBaoXiangResultWnd:OnBaseClick()
    local info = LuaDouDiZhuBaoXiangMgr.resultInfo
    if info.isPlayOver and info.allBoxCount > 0 then
        self:StartTick()
        self.numDesc.gameObject:SetActive(true)
        self.numDesc.text = SafeStringFormat3(LocalString.GetString("恭喜您本局共获得%d个宝箱!"), LuaDouDiZhuBaoXiangMgr.resultInfo.allBoxCount)
        self:EnterOpenBox()
    else
        CUIManager.CloseUI(CLuaUIResources.DouDiZhuBaoXiangResultWnd)
    end
end

function LuaDouDiZhuBaoXiangResultWnd:OnOpenBoxClick()
    self:ClearTick()
    Gac2Gas.RequestQiangXiangZiOpenOneChest()
end

function LuaDouDiZhuBaoXiangResultWnd:OnResultClick()
    if self.openedCount < LuaDouDiZhuBaoXiangMgr.resultInfo.allBoxCount then
        self:StartTick()
        self.numDesc.gameObject:SetActive(false)
        self:EnterOpenBox()
    else
        CUIManager.CloseUI(CLuaUIResources.DouDiZhuBaoXiangResultWnd)
    end
end

--@endregion UIEvent
