local CUITexture = import "L10.UI.CUITexture"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"

LuaChristmas2023GiftForSelfResultWnd = class()
RegistClassMember(LuaChristmas2023GiftForSelfResultWnd, "m_CardList")

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaChristmas2023GiftForSelfResultWnd, "CardTexture", "CardTexture", CUITexture)
RegistChildComponent(LuaChristmas2023GiftForSelfResultWnd, "OldCard", "OldCard", GameObject)
RegistChildComponent(LuaChristmas2023GiftForSelfResultWnd, "NewCard", "NewCard", GameObject)
RegistChildComponent(LuaChristmas2023GiftForSelfResultWnd, "CardNameLabel", "CardNameLabel", UILabel)
RegistChildComponent(LuaChristmas2023GiftForSelfResultWnd, "CardContentLabel", "CardContentLabel", UILabel)

--@endregion RegistChildComponent end

function LuaChristmas2023GiftForSelfResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end
-- 显示卡片信息
function LuaChristmas2023GiftForSelfResultWnd:ShowCard(cardid)
    local cardinfo = Christmas2023_CarftFormula.GetData(cardid)
    if not cardinfo then return end
    for i = 1, 6 do
        if self.m_CardList[i] then
            self.m_CardList[i]:SetActive(i == cardid)
        end
    end
    self.CardNameLabel.text = LocalString.GetString(cardinfo.Title)
    self.CardContentLabel.text = g_MessageMgr:Format(LocalString.GetString(cardinfo.Description))
end
function LuaChristmas2023GiftForSelfResultWnd:Init()
    self.m_CardList = {}
    for i = 0, 5 do
        table.insert(self.m_CardList, self.CardTexture.transform:GetChild(i).gameObject)
    end
    if LuaChristmas2023Mgr.m_IsFirstGet then
        self.NewCard:SetActive(true)
        self.OldCard:SetActive(false)
    else
        self.NewCard:SetActive(false)
        self.OldCard:SetActive(true)
    end
    if LuaChristmas2023Mgr.m_CardId and LuaChristmas2023Mgr.m_CardId > 0 then
        self:ShowCard(LuaChristmas2023Mgr.m_CardId)
    else
        self:ShowCard(1)
    end
end
function LuaChristmas2023GiftForSelfResultWnd:OnEnable()
    LuaChristmas2023Mgr:IncUpWndCount()
end
function LuaChristmas2023GiftForSelfResultWnd:OnDisable()
    LuaChristmas2023Mgr:DecUpWndCount()
end

--@region UIEvent

--@endregion UIEvent

