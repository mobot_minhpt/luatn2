local UILabel = import "UILabel"
local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local Profession = import "L10.Game.Profession"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local EnumClass = import "L10.Game.EnumClass"

LuaStarBiWuZhanDuiInvitedWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaStarBiWuZhanDuiInvitedWnd, "RejectButton", "RejectButton", GameObject)
RegistChildComponent(LuaStarBiWuZhanDuiInvitedWnd, "AcceptButton", "AcceptButton", GameObject)
RegistChildComponent(LuaStarBiWuZhanDuiInvitedWnd, "ClearButton", "ClearButton", GameObject)
RegistChildComponent(LuaStarBiWuZhanDuiInvitedWnd, "ApplicationAdvScrollView", "ApplicationAdvScrollView", QnAdvanceGridView)
RegistChildComponent(LuaStarBiWuZhanDuiInvitedWnd, "Empty", "Empty", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaStarBiWuZhanDuiInvitedWnd,"m_CurrentRequestPlayerIndex")
RegistClassMember(LuaStarBiWuZhanDuiInvitedWnd,"m_InvitePlayers")
RegistClassMember(LuaStarBiWuZhanDuiInvitedWnd,"m_MemberObj")
RegistClassMember(LuaStarBiWuZhanDuiInvitedWnd,"m_InvitedLabel")
function LuaStarBiWuZhanDuiInvitedWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.RejectButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRejectButtonClick()
	end)

	UIEventListener.Get(self.AcceptButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnAcceptButtonClick()
	end)

	UIEventListener.Get(self.ClearButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnClearButtonClick()
	end)

    --@endregion EventBind end
	self.m_CurrentRequestPlayerIndex = -1
	self.m_InvitePlayers = nil
    self.m_MemberObj = self.ApplicationAdvScrollView.transform:Find("Pool/ZhanDuiTemplate/Member").gameObject
    self.m_MemberObj.gameObject:SetActive(false)
    self.m_InvitedLabel = self.transform:Find("Anchor/RequestListRoot/BottomArea/InviteLabel"):GetComponent(typeof(UILabel))
    self.m_InvitedLabel.gameObject:SetActive(false)
    self.m_InvitedLabel.gameObject:SetActive(false)
    self.RejectButton.gameObject:SetActive(false)
    self.AcceptButton.gameObject:SetActive(false)
end

function LuaStarBiWuZhanDuiInvitedWnd:Init()
	self.ApplicationAdvScrollView.m_DataSource=DefaultTableViewDataSource.CreateByCount(0,function(item,row)
        if row % 2 == 1 then
            item:SetBackgroundTexture(g_sprites.OddBgSpirite)
        else
            item:SetBackgroundTexture(g_sprites.EvenBgSprite)
        end

        self:InitInvitePlayerItem(item,row,self.m_InvitePlayers[row+1])
    end)
	self.ApplicationAdvScrollView.OnSelectAtRow =DelegateFactory.Action_int(function (row)
        self:OnInvitePlayerItemClicked(row)
    end)
	if not CLuaStarBiwuMgr.m_InvitedZhanDuiMemberList then
		-- 请求受邀列表
        Gac2Gas.QueryStarBiwuMyInviver()
	else
		self:OnReplyInvitedTeams()
	end
end

function LuaStarBiWuZhanDuiInvitedWnd:OnInvitePlayerItemClicked(index)
    self.m_CurrentRequestPlayerIndex = index + 1
    self:UpdateAcceptInvitedStatus(self.m_InvitePlayers and self.m_InvitePlayers[index + 1].AcceptFlag == 1)
end

function LuaStarBiWuZhanDuiInvitedWnd:InitInvitePlayerItem(item,row,zhandui)
	local transform=item.transform
    local m_BgSprite = transform:GetComponent(typeof(UISprite))
	local m_MemberGrid = transform:Find("MemberGrid"):GetComponent(typeof(UIGrid))
    Extensions.RemoveAllChildren(m_MemberGrid.transform)
    do
        local i = 0 local len = zhandui.ZhanDuiMembers and #zhandui.ZhanDuiMembers or 0
        while i < len do
            local go = NGUITools.AddChild(m_MemberGrid.gameObject, self.m_MemberObj)
            go:SetActive(true)
            if zhandui.ZhanDuiMembers[i + 1].id > 0 then
                go:GetComponent(typeof(UISprite)).spriteName= Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), zhandui.ZhanDuiMembers[i + 1].class))
                go.transform:Find("Sprite").gameObject:SetActive(false)
            end
            i = i + 1
        end
    end
    m_MemberGrid:Reposition()
    local transTbl = {"Name", "Capacity", "Score", "InvitedName"}
    local valueTbl = {zhandui.ZhanDuiName, zhandui.Capacity, zhandui.StarScore, zhandui.InviterName}
    for i = 1, #transTbl do
      transform:Find(transTbl[i]):GetComponent(typeof(UILabel)).text = valueTbl[i]
    end

	local detailBtn = transform:Find("DetailBtn")
	UIEventListener.Get(detailBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    CLuaStarBiwuMgr:ShowZhanDuiMember2Wnd(0, zhandui.ZhanduiId, 1)
	end)
end

function LuaStarBiWuZhanDuiInvitedWnd:OnReplyInvitedTeams()
	self.m_InvitePlayers = CLuaStarBiwuMgr.m_InvitedZhanDuiMemberList
	if #self.m_InvitePlayers <= 0 then
		self.Empty.gameObject:SetActive(true)
		self.ApplicationAdvScrollView.gameObject:SetActive(false)
		return
	else
		self.Empty.gameObject:SetActive(false)
		self.ApplicationAdvScrollView.gameObject:SetActive(true)
	end
    self.ApplicationAdvScrollView.m_DataSource.count=#self.m_InvitePlayers
    self.ApplicationAdvScrollView:ReloadData(false, false)
end
--@region UIEvent

function LuaStarBiWuZhanDuiInvitedWnd:OnRejectButtonClick()
	if self.m_CurrentRequestPlayerIndex >= 0 and self.m_CurrentRequestPlayerIndex <= #self.m_InvitePlayers then
        Gac2Gas.StarBiwuFreeMemberRequestRefuseBeInvite(self.m_InvitePlayers[self.m_CurrentRequestPlayerIndex].ZhanduiId)
        table.remove(self.m_InvitePlayers, self.m_CurrentRequestPlayerIndex)
        if #self.m_InvitePlayers <= 0 then
            self.Empty.gameObject:SetActive(true)
            self.ApplicationAdvScrollView.gameObject:SetActive(false)
            return
        else
            self.Empty.gameObject:SetActive(false)
            self.ApplicationAdvScrollView.gameObject:SetActive(true)
        end
        self.ApplicationAdvScrollView.m_DataSource.count=#self.m_InvitePlayers
        self.ApplicationAdvScrollView:ReloadData(false, false)
    else
        g_MessageMgr:ShowMessage("DOUHUN_NEED_ONE_PLAYER")
    end
end

function LuaStarBiWuZhanDuiInvitedWnd:OnAcceptButtonClick()
	if self.m_CurrentRequestPlayerIndex >= 0 and self.m_CurrentRequestPlayerIndex <= #self.m_InvitePlayers then
        Gac2Gas.StarBiwuFreeMemberRequestAcceptBeInvite(self.m_InvitePlayers[self.m_CurrentRequestPlayerIndex].ZhanduiId)
        self:UpdateAcceptInvitedStatus(true)
    else
        g_MessageMgr:ShowMessage("DOUHUN_NEED_ONE_PLAYER")
    end
end

function LuaStarBiWuZhanDuiInvitedWnd:OnClearButtonClick()
    g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("StarBiWu_ClearInvitedList_confirm"), function()
        -- 清空列表
        Gac2Gas.RequestStarBiwuCleanMyInviverList()
        self.ApplicationAdvScrollView.m_DataSource.count=0
        self.ApplicationAdvScrollView:ReloadData(false, false)
        self.Empty.gameObject:SetActive(true)
    end, nil, nil, nil, false)
end

function LuaStarBiWuZhanDuiInvitedWnd:UpdateAcceptInvitedStatus(acceptInvited)
    self.m_InvitedLabel.gameObject:SetActive(acceptInvited)
    self.RejectButton.gameObject:SetActive(not acceptInvited)
    self.AcceptButton.gameObject:SetActive(not acceptInvited)
end


--@endregion UIEvent

function LuaStarBiWuZhanDuiInvitedWnd:OnEnable()
    g_ScriptEvent:AddListener("ReplyStarBiwuZhanDuiInvitedList", self, "OnReplyInvitedTeams")
end

function LuaStarBiWuZhanDuiInvitedWnd:OnDisable()
    g_ScriptEvent:RemoveListener("ReplyStarBiwuZhanDuiInvitedList", self, "OnReplyInvitedTeams")
    CLuaStarBiwuMgr.m_InvitedZhanDuiMemberList = nil
end