require("common/common_include")
require("ui/present/LuaPresentGivenWordsItem")

local CBaseWnd = import "L10.UI.CBaseWnd"
local CPresentMgr = import "L10.Game.CPresentMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local GameplayItem_PresentItem = import "L10.Game.GameplayItem_PresentItem"
local CUICommonDef = import "L10.UI.CUICommonDef"
local GameplayItem_PresentItemMessageContent = import "L10.Game.GameplayItem_PresentItemMessageContent"
local CommonDefs = import "L10.Game.CommonDefs"
local CIMMgr = import "L10.Game.CIMMgr"
local MessageWndManager = import "L10.UI.MessageWndManager"
local Gac2Gas = import "L10.Game.Gac2Gas"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CInputBoxMgr = import "L10.UI.CInputBoxMgr"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local LocalString = import "LocalString"
local UILabel = import "UILabel"
local UIScrollView = import "UIScrollView"
local UITable = import "UITable"
local CSpeechCtrlMgr = import "L10.Game.CSpeechCtrlMgr"
local CSpeechCtrlType = import "L10.Game.CSpeechCtrlType"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"

LuaPresentGivenWnd = class()
RegistClassMember(LuaPresentGivenWnd,"closeBtn")
RegistClassMember(LuaPresentGivenWnd,"playerInfoLabel")
RegistClassMember(LuaPresentGivenWnd,"scrollView")
RegistClassMember(LuaPresentGivenWnd,"table")
RegistClassMember(LuaPresentGivenWnd,"itemTemplate")
RegistClassMember(LuaPresentGivenWnd,"giveBtn")

RegistClassMember(LuaPresentGivenWnd,"itemId")
RegistClassMember(LuaPresentGivenWnd,"recvId")
RegistClassMember(LuaPresentGivenWnd,"recvName")
RegistClassMember(LuaPresentGivenWnd,"selectedIndex")
RegistClassMember(LuaPresentGivenWnd,"items")

function LuaPresentGivenWnd:Awake()
    
end

function LuaPresentGivenWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaPresentGivenWnd:Init()
	self.closeBtn = self.transform:Find("Wnd_Bg_Secondary_2/CloseButton").gameObject
	self.playerInfoLabel = self.transform:Find("Anchor/Label"):GetComponent(typeof(UILabel))
	self.scrollView = self.transform:Find("Anchor/Selections/ScrollView"):GetComponent(typeof(UIScrollView))
	self.table = self.transform:Find("Anchor/Selections/ScrollView/Table"):GetComponent(typeof(UITable))
	self.itemTemplate = self.transform:Find("Anchor/Selections/ScrollView/ItemTemplate").gameObject
	self.giveBtn = self.transform:Find("Anchor/GiveBtn").gameObject
	
	self.itemTemplate:SetActive(false)
	
	CommonDefs.AddOnClickListener(self.closeBtn, DelegateFactory.Action_GameObject(function(go) self:Close() end), false)
	CommonDefs.AddOnClickListener(self.giveBtn, DelegateFactory.Action_GameObject(function(go) self:OnGiveButtonClick(go) end), false)
	
	self.itemId = CPresentMgr.Inst.PresentToGiveId
    self.playerInfoLabel.text = SafeStringFormat3(LocalString.GetString("你要对 %s 说:"), CPresentMgr.Inst.RecvName)
    local item = CItemMgr.Inst:GetById(self.itemId)
	local data = nil
	if item then
		data = GameplayItem_PresentItem.GetData(item.TemplateId)
	end
	self.recvId = CPresentMgr.Inst.RecvId
	self.recvName = CPresentMgr.Inst.RecvName
	self.selectedIndex = -1
	
	if item and item.IsItem and item.Item.ExtraVarData and item.Item.ExtraVarData.StringData then
		self.recvId = tonumber(item.Item.ExtraVarData.StringData)
	end
	CUICommonDef.ClearTransform(self.table.transform)
	self.items = {}
	if data then
		for i = 0, data.ContentOptionOnSend.Length-1 do
			local contentIndex = data.ContentOptionOnSend[i]
			local contentData = GameplayItem_PresentItemMessageContent.GetData(contentIndex)
			if contentData then
				local go = CUICommonDef.AddChild(self.table.gameObject, self.itemTemplate)
				go:SetActive(true)
				
				local wordsItem = LuaPresentGivenWordsItem:new()
				wordsItem:Init(go, contentIndex, contentData.Content)
				CommonDefs.AddOnClickListener(go, DelegateFactory.Action_GameObject(function(go) self:OnItemClick(go) end), false)
				table.insert(self.items, wordsItem)
			end
		end

		local go = CUICommonDef.AddChild(self.table.gameObject, self.itemTemplate)
		go:SetActive(not CSpeechCtrlMgr.Inst:CheckIfNeedSpeechCtrl(CSpeechCtrlType.Type_YiTiaoLong_Present, false))
		local wordsItem = LuaPresentGivenWordsItem:new()
		wordsItem:Init(go, 0,  LocalString.GetString("自定义赠言"))
		CommonDefs.AddOnClickListener(go, DelegateFactory.Action_GameObject(function(go) self:OnItemClick(go) end), false)
		table.insert(self.items, wordsItem)
		self.table:Reposition()
		self.scrollView:ResetPosition()
	else
		self:Close()
	end
end

function LuaPresentGivenWnd:OnGiveButtonClick(go)
	if not CClientMainPlayer.Inst then
		self:Close()
		return
	end
	local item = CItemMgr.Inst:GetById(self.itemId)
	local notBeFriendLevel = 0
	local playerMaxLevel = CClientMainPlayer.Inst.MaxLevel
	local mustBeFriend = 1
	if item then
		local itemData = GameplayItem_PresentItem.GetData(item.TemplateId)
		if itemData then
			notBeFriendLevel = itemData.NotBeFriendLevel
			mustBeFriend = itemData.MustBeFriend
		end
	end

	if self.selectedIndex < 1 or self.selectedIndex > #self.items then
		g_MessageMgr:ShowMessage("YOU_MUST_SELECT_GIFT_MESSAGE")
		return
	elseif self.selectedIndex == #self.items and 
		(cs_string.IsNullOrEmpty(self.items[self.selectedIndex].Text) or self.items[self.selectedIndex].Text == LocalString.GetString("自定义赠言")) then
		g_MessageMgr:ShowMessage("Send_Gift_Input_Words_Message")
		return
	elseif CIMMgr.Inst:IsMyFriend(self.recvId) or mustBeFriend == 0 or 
		(notBeFriendLevel > 0 and playerMaxLevel >= notBeFriendLevel) then
		local pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, self.itemId)
		if pos > 0 then
			if self.selectedIndex == #self.items and self.items[self.selectedIndex].Index == 0 then
				CPresentMgr.Inst:RequestUsePresentItem(self.recvId, self.itemId, 0, self.items[self.selectedIndex].Text)
			else
				CPresentMgr.Inst:RequestUsePresentItem(self.recvId, self.itemId, self.items[self.selectedIndex].Index, "")
			end
			self:Close()
		end
	else
		local message = g_MessageMgr:FormatMessage("ONLY_SEND_GIFT_TO_FRIEND", self.recvName)
		MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function()
			Gac2Gas.AddFriend(self.recvId, "PresentGive")
		end), nil, nil, nil, false)
	end
end

function LuaPresentGivenWnd:OnItemClick(go)
	for i,item in pairs(self.items) do
		if go == item.gameObject then
			item:SetSelected(true)
			self.selectedIndex = i
			if i == #self.items then
				--最后一个元素
				self:OnInputText(item)
			end
		else
			item:SetSelected(false)
		end
	end
end

function LuaPresentGivenWnd:OnInputText(item)
	 CInputBoxMgr.ShowInputBox(g_MessageMgr:FormatMessage("Send_Gift_Input_Words_Message"), 
		DelegateFactory.Action_string(function(input)
			input = self:Trim(input)
			if input and input~= "" then
				if CWordFilterMgr.Inst:DoFilterOnPresentUserDefinedWords(input, false) ~= input then
					g_MessageMgr:ShowMessage("PresentItemContent_Illegal")
				else
					item:Init(item.gameObject, item.Index, input)
					item:SetSelected(true) --重新选中一下
				end
			end
		end), 30, true, nil, item.Text)
end

function LuaPresentGivenWnd:Trim(str)
	if str== nil or str == "" then
		return str
	end
	str = string.gsub(str, "^[ \t\n\r]+", "")
	return string.gsub(str, "[ \t\n\r]+$", "")
end

return LuaPresentGivenWnd