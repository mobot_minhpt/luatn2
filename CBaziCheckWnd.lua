-- Auto Generated!!
local CBaziCheckMgr = import "L10.UI.CBaziCheckMgr"
local CBaziCheckWnd = import "L10.UI.CBaziCheckWnd"
local CItemMgr = import "L10.Game.CItemMgr"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CBaziCheckWnd.m_Init_CS2LuaHook = function (this) 
    local item = CItemMgr.Inst:GetById(CBaziCheckMgr.itemId)
    if item == nil then
        return
    end
    this.iconTexture:LoadMaterial(item.Icon)
    if System.String.IsNullOrEmpty(CBaziCheckMgr.name) then
        this.nameLabel.text = item.Name
    else
        this.nameLabel.text = CBaziCheckMgr.name
    end
    this.buttonLabel.text = CBaziCheckMgr.buttonLabel

    UIEventListener.Get(this.button).onClick = MakeDelegateFromCSFunction(this.OnButtonClick, VoidDelegate, this)
    UIEventListener.Get(this.closeButton).onClick = MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this)
end
CBaziCheckMgr.m_ShowBaziCheckWnd_CS2LuaHook = function (id, button, title, buttonClick) 
    CBaziCheckMgr.itemId = id
    CBaziCheckMgr.name = title
    CBaziCheckMgr.buttonLabel = button
    CBaziCheckMgr.OnButtonClick = buttonClick
    CUIManager.ShowUI(CUIResources.BaziCheckWnd)
end
