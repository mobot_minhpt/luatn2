local DelegateFactory   = import "DelegateFactory"
local SceneBrush        = import "L10.Game.SceneBrush"
local Input             = import "UnityEngine.Input"
local MouseInputHandler = import "L10.Engine.MouseInputHandler"
local EUIModuleGroup    = import "L10.UI.CUIManager+EUIModuleGroup"
local CameraFollow      = import "L10.Engine.CameraControl.CameraFollow"
local CRenderScene      = import "L10.Engine.Scene.CRenderScene"
local MapGridHelper     = import "L10.Engine.Scene.MapGridHelper"
local Clipboard         = import "L10.Engine.Clipboard"

LuaMapGridEditWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaMapGridEditWnd, "brushPosition")
RegistClassMember(LuaMapGridEditWnd, "brushSize")
RegistClassMember(LuaMapGridEditWnd, "brushSizeLabel")
RegistClassMember(LuaMapGridEditWnd, "sceneBrush")
RegistClassMember(LuaMapGridEditWnd, "lastBrushPos")
RegistClassMember(LuaMapGridEditWnd, "mapGridHelper")

RegistClassMember(LuaMapGridEditWnd, "barrierGrid")

function LuaMapGridEditWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

    self.sceneBrush = SceneBrush(1, "BarrierBrushEditor")
    self.sceneBrush:Show()
    self.mapGridHelper = CRenderScene.Inst.transform:GetComponent(typeof(MapGridHelper))

    local anchor = self.transform:Find("Anchor")
    local slider = anchor:Find("Slider"):GetComponent(typeof(UISlider))
    self.brushSizeLabel = slider.transform:Find("Value"):GetComponent(typeof(UILabel))
    self.brushPosition = anchor:Find("Position"):GetComponent(typeof(UILabel))
    slider.OnChangeValue = DelegateFactory.Action_float(function(value)
        self:OnSliderValueChange(value)
    end)
    slider.value = 0
    self:OnSliderValueChange(0)

    UIEventListener.Get(anchor:Find("CopyButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCopyButtonClick()
	end)

    -- 初始化障碍二维数组
    self.barrierGrid = {}
    for x = 0, CRenderScene.Inst.MapWidth - 1 do
        self.barrierGrid[x] = {}
        for z = 0, CRenderScene.Inst.MapHeight - 1 do
            self.barrierGrid[x][z] = 0
        end
    end
end

function LuaMapGridEditWnd:OnEnable()
    MouseInputHandler.Inst.ForbidClickMove = true

    CameraFollow.Inst.mShowWnd = false
    CameraFollow.Inst.mAffectUI = false
	local List_String = MakeGenericClass(List, cs_string)
    CUIManager.HideUIGroupByChangeLayer(EUIModuleGroup.EGameUI, Table2List({"MiddleNoticeCenter"}, List_String), false, true)
    CUIManager.HideUIGroupByChangeLayer(EUIModuleGroup.EBgGameUI, Table2List({"MiddleNoticeCenter"}, List_String), false, true)

    self.mapGridHelper.b_ShowGrid = true
    self.mapGridHelper.b_ShowRoadInfo = false
end

function LuaMapGridEditWnd:OnDisable()
    MouseInputHandler.Inst.ForbidClickMove = false

    CameraFollow.Inst:ResetToDefault()
	local List_String = MakeGenericClass(List, cs_string)
	CUIManager.ShowUIGroupByChangeLayer(EUIModuleGroup.EGameUI, Table2List({"MiddleNoticeCenter"}, List_String), false, true)
    CUIManager.ShowUIGroupByChangeLayer(EUIModuleGroup.EBgGameUI, Table2List({"MiddleNoticeCenter"}, List_String), false, true)

    self.mapGridHelper.b_ShowGrid = false
    self.mapGridHelper.b_ShowRoadInfo = false
end

function LuaMapGridEditWnd:Init()

end


function LuaMapGridEditWnd:Update()
    self.sceneBrush:SetPosition(Input.mousePosition, true)
    self.brushPosition.text = self.sceneBrush.Position.x .. ", " .. self.sceneBrush.Position.z
    if Input.GetMouseButtonDown(0) then
        if not CUICommonDef.IsOverUI then
            self.lastBrushPos = self.sceneBrush.Position
            self:ApplyGridStroke(self.sceneBrush.BottomLeft, self.brushSize)
        end
    elseif Input.GetMouseButton(0) then
        if self.lastBrushPos and CommonDefs.op_Subtraction_Vector3_Vector3(self.sceneBrush.Position, self.lastBrushPos).magnitude > 0.5 then
            self.lastBrushPos = self.sceneBrush.Position
            self:ApplyGridStroke(self.sceneBrush.BottomLeft, self.brushSize)
        end
    elseif Input.GetMouseButtonUp(0) then
        self.lastBrushPos = nil
    end
end

function LuaMapGridEditWnd:OnDestroy()
    if self.sceneBrush then
        self.sceneBrush:OnDestroy()
    end
end

function LuaMapGridEditWnd:ApplyGridStroke(bottomLeft, size)
    for z = 0, size - 1 do
        for x = 0, size - 1 do
            local xGlobal = math.floor(bottomLeft.x + x + 0.5)
            local zGlobal = math.floor(bottomLeft.z + z + 0.5)
            if xGlobal >= 0 and xGlobal < CRenderScene.Inst.MapWidth and zGlobal >= 0 and zGlobal < CRenderScene.Inst.MapHeight then
                local quadIndex = zGlobal / MapGridHelper.MAX_HEIGHT_PER_MESH
                local zInQuad = zGlobal % MapGridHelper.MAX_HEIGHT_PER_MESH
                self.mapGridHelper.quadHelpers[quadIndex]:SetColor(Color.black, xGlobal, zInQuad)
                self.barrierGrid[xGlobal][zGlobal] = 1
            end
        end
    end
end

--@region UIEvent

function LuaMapGridEditWnd:OnSliderValueChange(value)
    self.brushSize = math.floor(1 + 9 * value)
    self.brushSizeLabel.text = self.brushSize
    self.sceneBrush:UpdateBrush(self.brushSize)
end

function LuaMapGridEditWnd:OnCopyButtonClick()
    -- 复制数组
    local grid = {}
    for x = 0, CRenderScene.Inst.MapWidth - 1 do
        grid[x] = {}
        for z = 0, CRenderScene.Inst.MapHeight - 1 do
            grid[x][z] = self.barrierGrid[x][z]
        end
    end

    local barrierBottomLeft = {}
    local barrierTopRight = {}
    for x = 0, CRenderScene.Inst.MapWidth - 1 do
        for z = 0, CRenderScene.Inst.MapHeight - 1 do
            if grid[x][z] == 1 then
                -- 先确定矩形右坐标
                local xMax = x
                for _x = x + 1, CRenderScene.Inst.MapWidth - 1 do
                    if grid[_x][z] == 1 then
                        xMax = _x
                    else
                        break
                    end
                end

                -- 根据右坐标确定上坐标
                local zMax = z
                for _z = z + 1, CRenderScene.Inst.MapHeight - 1 do
                    local allIsBarrier = true
                    for _x = x, xMax do
                        if grid[_x][_z] == 0 then
                            allIsBarrier = false
                            break
                        end
                    end

                    if allIsBarrier then
                        zMax = _z
                    else
                        break
                    end
                end

                table.insert(barrierBottomLeft, {x, z})
                table.insert(barrierTopRight, {xMax, zMax})

                -- 已经记录的矩形清空障碍信息
                for _x = x, xMax do
                    for _z = z, zMax do
                        grid[_x][_z] = 0
                    end
                end
            end
        end
    end

    if #barrierBottomLeft > 0 then
        local bottomLeft = ""
        for i = 1, #barrierBottomLeft do
            local data = barrierBottomLeft[i]
            bottomLeft = bottomLeft .. data[1] .. "," .. data[2] .. ";"
        end

        local topRight = ""
        for i = 1, #barrierTopRight do
            local data = barrierTopRight[i]
            topRight = topRight .. data[1] .. "," .. data[2] .. ";"
        end
        Clipboard.SetClipboradText(bottomLeft .. LocalString.GetString("	") .. topRight)
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("动态障碍坐标已复制到剪贴板"))
    end
end

--@endregion UIEvent
