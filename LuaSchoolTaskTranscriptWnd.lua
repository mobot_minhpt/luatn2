local CBaseWnd = import "L10.UI.CBaseWnd"
local DelegateFactory = import "DelegateFactory"
local CommonDefs = import "L10.Game.CommonDefs"
local LocalString = import "LocalString"
local UILabel = import "UILabel"

LuaSchoolTaskTranscriptWnd = class()
RegistClassMember(LuaSchoolTaskTranscriptWnd,"m_CloseButton")
RegistClassMember(LuaSchoolTaskTranscriptWnd,"m_ScrollView")
RegistClassMember(LuaSchoolTaskTranscriptWnd,"m_ContentTable")
RegistClassMember(LuaSchoolTaskTranscriptWnd,"m_Content1Label")
RegistClassMember(LuaSchoolTaskTranscriptWnd,"m_TranscriptRoot")
RegistClassMember(LuaSchoolTaskTranscriptWnd,"m_Content2Label")
RegistClassMember(LuaSchoolTaskTranscriptWnd,"m_WishingWordsLabel")
RegistClassMember(LuaSchoolTaskTranscriptWnd,"m_SchoolLabel")

function LuaSchoolTaskTranscriptWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaSchoolTaskTranscriptWnd:Init()

	self.m_CloseButton = self.transform:Find("Bg/CloseButton").gameObject
	CommonDefs.AddOnClickListener(self.m_CloseButton, DelegateFactory.Action_GameObject(function(go) self:Close() end), false)

	self.m_ScrollView = self.transform:Find("Bg/ScrollView"):GetComponent(typeof(UIScrollView))
	self.m_ContentTable = self.transform:Find("Bg/ScrollView/Table"):GetComponent(typeof(UITable))
	self.m_Content1Label = self.m_ContentTable.transform:Find("Content1Label"):GetComponent(typeof(UILabel))
	self.m_TranscriptRoot = self.m_ContentTable.transform:Find("Transcript")
	self.m_Content2Label = self.m_ContentTable.transform:Find("Content2Label"):GetComponent(typeof(UILabel))
	self.m_WishingWordsLabel = self.m_ContentTable.transform:Find("WishingWordsLabel"):GetComponent(typeof(UILabel))
	self.m_SchoolLabel = self.m_ContentTable.transform:Find("SchoolLabel"):GetComponent(typeof(UILabel))

	self:InitContent()
end

function LuaSchoolTaskTranscriptWnd:InitContent()
	
	local mainplayer = CClientMainPlayer.Inst

	local name = mainplayer and mainplayer.Name or ""
	local schoolName = mainplayer and mainplayer.SchoolName
	local costDays =LuaSchoolTaskMgr.m_TranscriptInfo.daydiff
	self.m_Content1Label.text = SafeStringFormat3(LocalString.GetString("宗派弟子 %s\n你成功通过了出师考验，成为了一名合格的%s正式弟子，在历时%d天的修习中，你共完成了以下课业的修习："), 
		name, schoolName, costDays)

	local n = self.m_TranscriptRoot:Find("Table").childCount
	for i=0,n-1 do
		local child = self.m_TranscriptRoot:Find("Table"):GetChild(i)
		local courseNameLabel = child:Find("NameLabel"):GetComponent(typeof(UILabel))
		local iconTexture = child:Find("IconTexture"):GetComponent(typeof(CUITexture))
		local obtainedScoreLabel = child:Find("ScoreLabel"):GetComponent(typeof(UILabel))
		courseNameLabel.text = LuaSchoolTaskMgr.m_TranscriptInfo.courseNames[i+1]
		obtainedScoreLabel.text = SafeStringFormat3(LocalString.GetString("%d学分"), LuaSchoolTaskMgr.m_TranscriptInfo.detailCredit[i+1])
	end


	--self.m_Content2Label.text = ""
	self.m_WishingWordsLabel.text = LuaSchoolTaskMgr.m_TranscriptInfo.wishContent
	self.m_SchoolLabel.text = SafeStringFormat3(LocalString.GetString("——%s"), schoolName)
	self.m_ContentTable:Reposition()
	self.m_ScrollView:ResetPosition()

end
