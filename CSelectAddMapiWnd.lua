-- Auto Generated!!
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CIndirectUIResources = import "L10.UI.CIndirectUIResources"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CSelectAddMapiWnd = import "L10.UI.CSelectAddMapiWnd"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local CUITexture = import "L10.UI.CUITexture"
local DelegateFactory = import "DelegateFactory"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local Extensions = import "Extensions"
local Gac2Gas = import "L10.Game.Gac2Gas"
local GameObject = import "UnityEngine.GameObject"
local Item_Item = import "L10.Game.Item_Item"
local LocalString = import "LocalString"
local NGUITools = import "NGUITools"
local String = import "System.String"
local StringActionKeyValuePair = import "L10.Game.StringActionKeyValuePair"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local UISprite = import "UISprite"
local ZuoQi_Setting = import "L10.Game.ZuoQi_Setting"
CSelectAddMapiWnd.m_Init_CS2LuaHook = function (this) 
    this.itemTemplate:SetActive(false)

    local setting = ZuoQi_Setting.GetData()
    local itemid2quality = CreateFromClass(MakeGenericClass(Dictionary, UInt32, UInt32))
    do
        local i = 0
        while i < setting.MapiItemId2Quality.Length do
            CommonDefs.DictAdd(itemid2quality, typeof(UInt32), setting.MapiItemId2Quality[i], typeof(UInt32), setting.MapiItemId2Quality[i + 1])
            i = i + 2
        end
    end

    CommonDefs.DictClear(this.mMapiDic)
    Extensions.RemoveAllChildren(this.grid.transform)

    local packageSize = CClientMainPlayer.Inst.ItemProp:GetPlaceMaxSize(EnumItemPlace.Bag)
    do
        local i = 1
        while i <= packageSize do
            local itemid = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Bag, i)
            if itemid ~= nil then
                local item = CItemMgr.Inst:GetById(itemid)
                if item ~= nil and CommonDefs.DictContains(itemid2quality, typeof(UInt32), item.TemplateId) then
                    local data = Item_Item.GetData(item.TemplateId)
                    if data ~= nil then
                        local itemUnit = NGUITools.AddChild(this.grid.gameObject, this.itemTemplate)
                        itemUnit:SetActive(true)

                        local qualityTrans = itemUnit.transform:Find("QualitySprite")
                        if qualityTrans ~= nil then
                            CommonDefs.GetComponent_GameObject_Type(qualityTrans.gameObject, typeof(UISprite)).spriteName = CUICommonDef.GetItemCellBorder(data, nil, false)
                        end

                        local selectTrans = itemUnit.transform:Find("BtnSelectSprite")
                        if selectTrans ~= nil then
                            selectTrans.gameObject:SetActive(false)
                        end

                        local textureTrans = itemUnit.transform:Find("Texture")
                        if textureTrans ~= nil then
                            local icon = data.Icon

                            if item.Item.MapiItemInfo ~= nil then
                                icon = setting.MapiIcon[item.Item.MapiItemInfo.FuseId]
                                if item.Item.MapiItemInfo.Quality == 4 then
                                    icon = setting.SpecialMapiIcon[0]
                                elseif item.Item.MapiItemInfo.Quality == 5 then
									icon = setting.SpecialMapiIcon[1]
								end
                            end
                            CommonDefs.GetComponent_GameObject_Type(textureTrans.gameObject, typeof(CUITexture)):LoadMaterial(icon)

                            UIEventListener.Get(textureTrans.gameObject).onClick = DelegateFactory.VoidDelegate(function (go) 
                                this:SelectMapi(itemid)
                            end)
                        end

                        CommonDefs.DictAdd(this.mMapiDic, typeof(String), itemid, typeof(GameObject), selectTrans.gameObject)
                    end
                end
            end
            i = i + 1
        end
    end

    this.grid:Reposition()
    this.scrollView:ResetPosition()
end
CSelectAddMapiWnd.m_SelectMapi_CS2LuaHook = function (this, itemid) 
    CommonDefs.DictIterate(this.mMapiDic, DelegateFactory.Action_object_object(function (___key, ___value) 
        local p = {}
        p.Key = ___key
        p.Value = ___value
        p.Value:SetActive(p.Key == itemid)
    end))

    CItemInfoMgr.ShowLinkItemInfo(itemid, false, this, AlignType.Default, 0, 0, 0, 0)
end
CSelectAddMapiWnd.m_GetActionPairs_CS2LuaHook = function (this, itemId, templateId) 
    local actions = CreateFromClass(MakeGenericClass(List, StringActionKeyValuePair))

    local addAction = CreateFromClass(StringActionKeyValuePair, LocalString.GetString("添加"), DelegateFactory.Action(function () 
        local pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, itemId)
        if pos > 0 then
            Gac2Gas.RequestUseItem(EnumItemPlace_lua.Bag, pos, itemId, "")
            CUIManager.CloseUI(CUIResources.SelectAddMapiWnd)
            CUIManager.CloseUI(CIndirectUIResources.ItemInfoWnd)
        end
    end))
    CommonDefs.ListAdd(actions, typeof(StringActionKeyValuePair), addAction)

    return actions
end
