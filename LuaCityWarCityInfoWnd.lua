require("common/common_include")

local UILabel = import "UILabel"
local UITable = import "UITable"
local CUICommonDef = import "L10.UI.CUICommonDef"
local LocalString = import "LocalString"
local Extensions = import "Extensions"

LuaCityWarCityInfoWnd = class()
RegistClassMember(LuaCityWarCityInfoWnd,"m_CityValueLabel")
RegistClassMember(LuaCityWarCityInfoWnd,"m_Table")
RegistClassMember(LuaCityWarCityInfoWnd,"m_Template")

function LuaCityWarCityInfoWnd:Awake()

end


function LuaCityWarCityInfoWnd:Init()
	self.m_CityValueLabel = self.transform:Find("Bg/City/ValueLabel"):GetComponent(typeof(UILabel))
	self.m_Table = self.transform:Find("Bg/Table"):GetComponent(typeof(UITable))
	self.m_Template = self.transform:Find("Bg/Template").gameObject
	
	self.m_Template:SetActive(false)

	local tbl = {}
	tbl[1] = {name = LocalString.GetString("领土"), value = ""}
	tbl[2] = {name = LocalString.GetString("主城"), value = ""}
	tbl[3] = {name = LocalString.GetString("城池等级"), value = ""}
	tbl[4] = {name = LocalString.GetString("帮会"), value = ""}
	tbl[5] = {name = LocalString.GetString("仓库等级"), value = ""}
	tbl[6] = {name = LocalString.GetString("资材"), value = ""}
	tbl[7] = {name = LocalString.GetString("城门"), value = ""}
	tbl[8] = {name = LocalString.GetString("城墙"), value = ""}
	tbl[9] = {name = LocalString.GetString("喷射火炮"), value = ""}
	tbl[10] = {name = LocalString.GetString("重炮"), value = ""}
	tbl[11] = {name = LocalString.GetString("元素塔"), value = ""}
	tbl[12] = {name = LocalString.GetString("控制塔"), value = ""}
	tbl[13] = {name = LocalString.GetString("陷阱"), value = ""}
	tbl[14] = {name = LocalString.GetString("弹射板"), value = ""}
	tbl[15] = {name = LocalString.GetString("炸弹"), value = ""}

	self:SendQueryCityInfoResult("", tbl)

	Gac2Gas.QueryCityInfo(CLuaCityWarMgr.CityInfoPlayerId, CLuaCityWarMgr.CityInfoGuildId)
end

function LuaCityWarCityInfoWnd:OnEnable()
	g_ScriptEvent:AddListener("SendQueryCityInfoResult", self, "SendQueryCityInfoResult")
end

function LuaCityWarCityInfoWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SendQueryCityInfoResult", self, "SendQueryCityInfoResult")
end

function LuaCityWarCityInfoWnd:SendQueryCityInfoResult(myCityName, infoTbl)
	if not self.m_CityValueLabel then return end
	self.m_CityValueLabel.text = myCityName
	Extensions.RemoveAllChildren(self.m_Table.transform)
	for i,data in pairs(infoTbl) do
		local instance = CUICommonDef.AddChild(self.m_Table.gameObject, self.m_Template)
		instance:SetActive(true)
		local nameLabel = instance.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
		local valueLabel = instance.transform:Find("ValueLabel"):GetComponent(typeof(UILabel))
		nameLabel.text = data.name
		valueLabel.text = data.value
	end

	self.m_Table:Reposition()
end

