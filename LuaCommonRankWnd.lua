local CPlayerInfoMgr=import "CPlayerInfoMgr"
local CRankData=import "L10.UI.CRankData"
local EnumPlayerInfoContext=import "L10.Game.EnumPlayerInfoContext"
local EChatPanel=import "L10.Game.EChatPanel"
local AlignType=import "CPlayerInfoMgr+AlignType"

LuaCommonRankWndMgr = {}
LuaCommonRankWndMgr.m_BottomText = ""
LuaCommonRankWndMgr.m_TitleText = ""
LuaCommonRankWndMgr.m_ThirdHeadText = ""
LuaCommonRankWndMgr.m_OnBottomTextClick = nil
LuaCommonRankWndMgr.m_ValueProcessFunc = nil

LuaCommonRankWnd = class()

RegistChildComponent(LuaCommonRankWnd, "m_ThirdHeadLabel","ThirdHeadLabel", UILabel)
RegistChildComponent(LuaCommonRankWnd, "m_MyRankLabel","MyRankLabel", UILabel)
RegistChildComponent(LuaCommonRankWnd, "m_MyNameLabel","MyNameLabel", UILabel)
RegistChildComponent(LuaCommonRankWnd, "m_MyThirdTextLabel","MyThirdTextLabel", UILabel)
RegistChildComponent(LuaCommonRankWnd, "m_TitleLabel","TitleLabel", UILabel)
RegistChildComponent(LuaCommonRankWnd, "m_BottomLabel","BottomLabel", UILabel)
RegistChildComponent(LuaCommonRankWnd, "m_TableView","TableView", QnTableView)

RegistClassMember(LuaCommonRankWnd, "m_Clues")
RegistClassMember(LuaCommonRankWnd, "m_RankList")

function LuaCommonRankWnd:Init()
    if CClientMainPlayer.Inst then
        self.m_MyNameLabel.text = CClientMainPlayer.Inst.Name
    end
    self.m_RankList={}
    self.m_TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_RankList
        end,
        function(item,index)
            item:SetBackgroundTexture(index % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite)

            self:InitItem(item,index,self.m_RankList[index+1])
        end
    )
    self.m_TableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        local data = self.m_RankList[row+1]
        if data then
            CPlayerInfoMgr.ShowPlayerPopupMenu(data.PlayerIndex, EnumPlayerInfoContext.Undefined,EChatPanel.Undefined,"",nil,Vector3.zero,AlignType.Default)
        end
    end)
    self.m_BottomLabel.text = LuaCommonRankWndMgr.m_BottomText
    self.m_TitleLabel.text = LuaCommonRankWndMgr.m_TitleText
    self.m_ThirdHeadLabel.text = LuaCommonRankWndMgr.m_ThirdHeadText
    UIEventListener.Get(self.m_BottomLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
        local index = self.m_BottomLabel:GetCharacterIndexAtPosition(UICamera.lastWorldPosition, true)
        if LuaCommonRankWndMgr.m_OnBottomTextClick then
            LuaCommonRankWndMgr.m_OnBottomTextClick(self.m_BottomLabel,index)
        end
    end)
    self:OnRankDataReady()
end

function LuaCommonRankWnd:InitItem(item,index,info)
    local tf=item.transform
    local timeLabel=tf:Find("TimeLabel"):GetComponent(typeof(UILabel))
    timeLabel.text=""
    local nameLabel=tf:Find("NameLabel"):GetComponent(typeof(UILabel))
    nameLabel.text=" "
    local rankLabel=tf:Find("RankLabel"):GetComponent(typeof(UILabel))
    rankLabel.text=""
    local rankSprite=tf:Find("RankLabel/RankImage"):GetComponent(typeof(UISprite))
    rankSprite.spriteName=""
    local rank=info.Rank
    if rank==1 then
        rankSprite.spriteName="Rank_No.1"
    elseif rank==2 then
        rankSprite.spriteName="Rank_No.2png"
    elseif rank==3 then
        rankSprite.spriteName="Rank_No.3png"
    else
        rankLabel.text=tostring(rank)
    end
    nameLabel.text = info.Name
    if LuaCommonRankWndMgr.m_ValueProcessFunc then
        timeLabel.text = LuaCommonRankWndMgr.m_ValueProcessFunc(info)
    else
        timeLabel.text = info.Value
    end
end

function LuaCommonRankWnd:OnEnable()
    g_ScriptEvent:AddListener("OnRankDataReady", self, "OnRankDataReady")
end

function LuaCommonRankWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnRankDataReady", self, "OnRankDataReady")
    LuaCommonRankWndMgr.m_OnBottomTextClick = nil
    LuaCommonRankWndMgr.m_ValueProcessFunc = nil
end

function LuaCommonRankWnd:OnRankDataReady()
    local myInfo = CRankData.Inst.MainPlayerRankInfo
    self.m_MyRankLabel.text = myInfo.Rank > 0 and myInfo.Rank or LocalString.GetString("未上榜")
    self.m_MyNameLabel.text = myInfo.Name
    if CClientMainPlayer.Inst then
        self.m_MyNameLabel.text = CClientMainPlayer.Inst.Name
    end
    if LuaCommonRankWndMgr.m_ValueProcessFunc then
        self.m_MyThirdTextLabel.text = LuaCommonRankWndMgr.m_ValueProcessFunc(myInfo)
    else
        self.m_MyThirdTextLabel.text = myInfo.Value > 0 and myInfo.Value or 0
    end

    self.m_RankList = {}
    for i=1,CRankData.Inst.RankList.Count do
        table.insert(self.m_RankList, CRankData.Inst.RankList[i-1])
    end
    self.m_TableView:ReloadData(true,false)
end