local UITable = import "UITable"
local GameObject = import "UnityEngine.GameObject"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local Animation = import "UnityEngine.Animation"
LuaDaFuWongJunWangLingWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaDaFuWongJunWangLingWnd, "PlayerInfoTable", "PlayerInfoTable", UITable)
RegistChildComponent(LuaDaFuWongJunWangLingWnd, "PlayerTemplate", "PlayerTemplate", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaDaFuWongJunWangLingWnd, "m_Anim")
RegistClassMember(LuaDaFuWongJunWangLingWnd, "m_CloseTick")
RegistClassMember(LuaDaFuWongJunWangLingWnd, "m_PlayerViewList")
RegistClassMember(LuaDaFuWongJunWangLingWnd, "m_WndList")
RegistClassMember(LuaDaFuWongJunWangLingWnd, "m_AniMoveTick")
function LuaDaFuWongJunWangLingWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.m_Anim = self.transform:GetComponent(typeof(Animation))
    self.m_CloseTick = nil
    self.m_AniMoveTick = nil
    self.m_WndList = {}
    self.m_WndList[1] = self.transform:Find("bg")
    self.m_WndList[2] = self.transform:Find("Item")
    self.m_WndList[3] = self.transform:Find("CUIFx")
    self.m_WndList[4] = self.transform:Find("CUIFxhou")
end

function LuaDaFuWongJunWangLingWnd:Init()
    if self.m_Anim then
        if self.m_CloseTick then
            UnRegisterTick(self.m_CloseTick)
            self.m_CloseTick = nil
        end
        self.m_CloseTick = RegisterTickOnce(function()
            CUIManager.CloseUI(CLuaUIResources.DaFuWongJunWangLingWnd)
        end, (self.m_Anim.clip.length - 8) * 1000)
    end

    if not self.m_PlayerViewList then 
		self.m_PlayerViewList = {} 
		Extensions.RemoveAllChildren(self.PlayerInfoTable.transform)
	end
    local order = LuaDaFuWongMgr.Index2Id
    for k,v in pairs(order) do
        local id = v
		if not self.m_PlayerViewList[id] then 
			local go = CUICommonDef.AddChild(self.PlayerInfoTable.gameObject,self.PlayerTemplate)
			self.m_PlayerViewList[id] = go
			go.gameObject:SetActive(true)
            go.transform:Find("HeightLight").gameObject:SetActive(id == LuaDaFuWongMgr.CurPlayerRound)
            go.transform:Find("HasTask").gameObject:SetActive(LuaDaFuWongMgr.PlayerInfo[id].taskId ~= 0)
		end
	end
	self.PlayerInfoTable:GetComponent(typeof(UITable)):Reposition()
    self:DoMoveAni()
end

function LuaDaFuWongJunWangLingWnd:DoMoveAni()
    local delayTime = 1.5
    local moveTime = 0.5
    local playerId = LuaDaFuWongMgr.CurRoundPlayer
    local playerInfo = self.transform:Find("PlayerInfo").transform
    local targetPos = self.m_PlayerViewList[playerId].transform.localPosition
    local direct = Vector3( self.PlayerInfoTable.transform.localPosition.x + targetPos.x,
                            self.PlayerInfoTable.transform.localPosition.y + targetPos.y ,0)

    local func = function()
        for k,v in pairs(self.m_WndList) do
            local curPos = v.transform.localPosition
            local point = playerInfo.transform:TransformPoint(direct)
	        local localTarget = self.transform:InverseTransformPoint(point)
            local tarpos = Vector3(curPos.x + localTarget.x,curPos.y + localTarget.y,0)
            LuaTweenUtils.TweenPosition(v.transform, tarpos.x, tarpos.y, tarpos.z, moveTime)
            LuaTweenUtils.TweenScaleTo(v.transform, Vector3(0.1,0.1,1),moveTime)
            if k == 3 then
                v.gameObject:SetActive(false)
            end
        end
    end
    if self.m_AniMoveTick then UnRegisterTick(self.m_AniMoveTick) self.m_AniMoveTick = nil end
    self.m_AniMoveTick = RegisterTickOnce(func,delayTime * 1000)
end

function LuaDaFuWongJunWangLingWnd:OnDisable()
    if self.m_CloseTick then UnRegisterTick(self.m_CloseTick) self.m_CloseTick = nil end
    if self.m_AniMoveTick then UnRegisterTick(self.m_AniMoveTick) self.m_AniMoveTick = nil end
end

--@region UIEvent

--@endregion UIEvent

