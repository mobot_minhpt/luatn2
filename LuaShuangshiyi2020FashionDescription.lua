local Extensions = import "Extensions"
local CFashionPreviewMgr = import "L10.UI.CFashionPreviewMgr"
local CItem = import "L10.Game.CItem"

CLuaShuangshiyi2020FashionDescription = class()

RegistChildComponent(CLuaShuangshiyi2020FashionDescription,"m_NameLabel","NameLabel", UILabel)
RegistChildComponent(CLuaShuangshiyi2020FashionDescription,"m_LabelTemplate","LabelTemplate", GameObject)
RegistChildComponent(CLuaShuangshiyi2020FashionDescription,"m_Table","Table", UITable)
RegistChildComponent(CLuaShuangshiyi2020FashionDescription,"m_Arrow","Arrow", UISprite)
RegistChildComponent(CLuaShuangshiyi2020FashionDescription,"m_ArrowBg","ArrowBg", GameObject)
RegistChildComponent(CLuaShuangshiyi2020FashionDescription,"m_ScrollView","ScrollView", UIScrollView)

RegistClassMember(CLuaShuangshiyi2020FashionDescription,"m_YellowColor")
RegistClassMember(CLuaShuangshiyi2020FashionDescription,"m_IsShow")
RegistClassMember(CLuaShuangshiyi2020FashionDescription,"m_InitialPosX")
function CLuaShuangshiyi2020FashionDescription:Awake()
    self.m_YellowColor = "[FFB74C]"
    self.m_InitialPosX = self.transform.localPosition.x
    self.m_IsShow = true
    self:UpdateState()
    self.m_LabelTemplate:SetActive(false)
    self:Hide()

    UIEventListener.Get(self.m_Arrow.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnArrowClick(go)
    end)
end

function CLuaShuangshiyi2020FashionDescription:OnEnable()
    g_ScriptEvent:AddListener("ShuangshiyiFashionPreviewWnd_UpdateDescription",self, "UpdateDescription")
end

function CLuaShuangshiyi2020FashionDescription:OnDisable()
    g_ScriptEvent:RemoveListener("ShuangshiyiFashionPreviewWnd_UpdateDescription",self, "UpdateDescription")
end

function CLuaShuangshiyi2020FashionDescription:Hide()
    self.m_NameLabel.gameObject:SetActive(false)
    Extensions.RemoveAllChildren(self.m_Table.transform)
    self.m_ArrowBg:SetActive(false)
    self.m_Table:Reposition()
    self.m_ScrollView:ResetPosition()
end

function CLuaShuangshiyi2020FashionDescription:AddItem(text)
    local go = NGUITools.AddChild(self.m_Table.gameObject, self.m_LabelTemplate)
    go.transform:Find("Label"):GetComponent(typeof(UILabel)).text = CUICommonDef.TranslateToNGUIText(text)
    go:SetActive(true)
end

function CLuaShuangshiyi2020FashionDescription:AddItemKeyValItem(key, value, isUseDefaultFormat)
    if isUseDefaultFormat then
        self:AddItem(SafeStringFormat3("%s: %s%s",key, self.m_YellowColor, value))
        return
    end
    self:AddItem(SafeStringFormat3("%s: %s",key, value))

end

function CLuaShuangshiyi2020FashionDescription:UpdateDescription(data)
    local itemID, templateID,curCategory = data[1], data[2], data[3]
    if itemID == nil then
        self:Hide()
        return
    end
    local item = Item_Item.GetData(itemID)
    if not item then
        self:Hide()
        return
    end
    Extensions.RemoveAllChildren(self.m_Table.transform)
    self.m_NameLabel.gameObject:SetActive(true)

    local Name = item.Name
    self.m_NameLabel.text = Name

    self:UpdateFashionDescription(item, templateID)

    self.m_Table:Reposition()
    self.m_ScrollView:ResetPosition()
    self.m_ArrowBg:SetActive(true)

    if not self.m_IsShow then
        self.m_Table.gameObject:SetActive(false)
        self.m_NameLabel.gameObject:SetActive(false)
    end
end

function CLuaShuangshiyi2020FashionDescription:AddItemDescription(itemId)
    local Description = CItem.GetItemDescription(itemId, true)
    local index = string.find(Description, "#r")
    if index <= 1 then
        return
    end
    Description = string.sub(Description, 1, index - 1)
    self:AddItem(Description)
end

function CLuaShuangshiyi2020FashionDescription:UpdateFashionDescription(item, templateID)
    self:AddItemDescription(templateID)

    local headFashion = Fashion_Fashion.GetData(CFashionPreviewMgr.HeadFashionID)
    local bodyFashion = Fashion_Fashion.GetData(CFashionPreviewMgr.BodyFashionID)
    if headFashion and headFashion.ItemID == item.ID then
        self:AddItemKeyValItem(LocalString.GetString("外观部位"),LocalString.GetString("头部"), true)
    elseif bodyFashion and bodyFashion.ItemID == item.ID then
        self:AddItemKeyValItem(LocalString.GetString("外观部位"),LocalString.GetString("身体和头部"), true)

        local lingYuMall = Mall_LingYuMallLimit.GetData(templateID)
        if lingYuMall and not String.IsNullOrEmpty(lingYuMall.Jade) then
            self:AddItemKeyValItem(LocalString.GetString("原价"), SafeStringFormat3(LocalString.GetString("%d灵玉"),math.ceil(lingYuMall.Jade /0.8)) ,true)
        end
    end
end

function CLuaShuangshiyi2020FashionDescription:UpdateState()
    Extensions.SetLocalRotationZ(self.m_Arrow.transform, self.m_IsShow and 90 or -90)
    self.m_Arrow.alpha = self.m_IsShow and 0.3 or 1
    local tween = LuaTweenUtils.TweenPositionX(self.transform, self.m_IsShow and self.m_InitialPosX or (self.m_InitialPosX - 329) ,0.3)
    if not self.m_IsShow then
        LuaTweenUtils.OnComplete(tween,function ()
            self.m_Table.gameObject:SetActive(false)
            self.m_NameLabel.gameObject:SetActive(false)
        end)
    else
        self.m_NameLabel.gameObject:SetActive(true)
        self.m_Table.gameObject:SetActive(true)
        self.m_Table:Reposition()
        self.m_ScrollView:ResetPosition()
    end

end

function CLuaShuangshiyi2020FashionDescription:OnArrowClick(go)
    self.m_IsShow = not self.m_IsShow
    self:UpdateState()
end
