-- Auto Generated!!
local Boolean = import "System.Boolean"
local CommonDefs = import "L10.Game.CommonDefs"
local CTeamApplyTabContent = import "L10.UI.Team.CTeamApplyTabContent"
local CTeamGroupMgr = import "L10.Game.CTeamGroupMgr"
local CTeamMember = import "L10.Game.CTeamMember"
local CTeamMemberItem = import "L10.UI.Team.CTeamMemberItem"
local CTeamMgr = import "L10.Game.CTeamMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local DelegateFactory = import "DelegateFactory"
local EnumApplyWndType = import "L10.UI.Team.CTeamApplyTabContent+EnumApplyWndType"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Gac2Gas = import "L10.Game.Gac2Gas"
local PlayerSettings = import "L10.Game.PlayerSettings"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CTeamApplyTabContent.m_RefreshButtonState_CS2LuaHook = function (this) 
    if this.curPage <= 0 then
        CUICommonDef.SetActive(this.pageUpButton, false, true)
    else
        CUICommonDef.SetActive(this.pageUpButton, true, true)
    end

    if this.curPage >= this.maxPageIndex then
        CUICommonDef.SetActive(this.pageDownButton, false, true)
    else
        CUICommonDef.SetActive(this.pageDownButton, true, true)
    end
end
CTeamApplyTabContent.m_RefreshMember_CS2LuaHook = function (this, data) 
    CommonDefs.ListIterate(this.memberItems, DelegateFactory.Action_object(function (___value) 
        local item = ___value
        if item.memberData ~= nil and item.memberData.BasicProp.Id == data.BasicProp.Id then
            item:Init(data, false)
        end
    end))
end
CTeamApplyTabContent.m_OnEnable_CS2LuaHook = function (this) 

    this.vibrateCheckbox.OnValueChanged = CommonDefs.CombineListner_Action_bool(this.vibrateCheckbox.OnValueChanged, MakeDelegateFromCSFunction(this.OnCheckboxValueChanged, MakeGenericClass(Action1, Boolean), this), true)
    if this.wndType == EnumApplyWndType.Team then
        this.vibrateCheckbox.Selected = PlayerSettings.VibrateWhenApplyToJoinTeamEnabled
        EventManager.AddListener(EnumEventType.AddTeamApply, MakeDelegateFromCSFunction(this.RefreshApplyList, Action0, this))
        EventManager.AddListener(EnumEventType.UpdateTeamApply, MakeDelegateFromCSFunction(this.RefreshApplyList, Action0, this))
        this:RefreshApplyList()
    elseif this.wndType == EnumApplyWndType.TeamGroup then
        this.vibrateCheckbox.Selected = PlayerSettings.VibrateWhenApplyTpJoinTeamGroupEnabled
        Gac2Gas.QueryTeamGroupApplicantInfo()
        EventManager.AddListener(EnumEventType.TeamGroupApplicationRefresh, MakeDelegateFromCSFunction(this.RefreshApplyList, Action0, this))
        CTeamGroupMgr.Instance.NewApplicationArrive = false
    end


    CommonDefs.ListIterate(this.memberItems, DelegateFactory.Action_object(function (___value) 
        local item = ___value
        item:RefreshAppearance()
    end))
end
CTeamApplyTabContent.m_OnDisable_CS2LuaHook = function (this) 
    this.vibrateCheckbox.OnValueChanged = CommonDefs.CombineListner_Action_bool(this.vibrateCheckbox.OnValueChanged, MakeDelegateFromCSFunction(this.OnCheckboxValueChanged, MakeGenericClass(Action1, Boolean), this), false)
    if this.wndType == EnumApplyWndType.Team then
        EventManager.RemoveListener(EnumEventType.AddTeamApply, MakeDelegateFromCSFunction(this.RefreshApplyList, Action0, this))
        EventManager.RemoveListener(EnumEventType.UpdateTeamApply, MakeDelegateFromCSFunction(this.RefreshApplyList, Action0, this))
    elseif this.wndType == EnumApplyWndType.TeamGroup then
        EventManager.RemoveListener(EnumEventType.TeamGroupApplicationRefresh, MakeDelegateFromCSFunction(this.RefreshApplyList, Action0, this))
    end
end
CTeamApplyTabContent.m_OnCheckboxValueChanged_CS2LuaHook = function (this, val) 
    repeat
        local default = this.wndType
        if default == EnumApplyWndType.Team then
            PlayerSettings.VibrateWhenApplyToJoinTeamEnabled = val
            break
        elseif default == EnumApplyWndType.TeamGroup then
            PlayerSettings.VibrateWhenApplyTpJoinTeamGroupEnabled = val
            break
        end
    until 1
end
CTeamApplyTabContent.m_UpdatePage_CS2LuaHook = function (this) 
    repeat
        local default = this.wndType
        if default == EnumApplyWndType.Team then
            this.memberList = CTeamMgr.Inst:GetTeamApplyerList()
            break
        elseif default == EnumApplyWndType.TeamGroup then
            this.memberList = CTeamGroupMgr.Instance:GetApplyMemberInfo()
            break
        end
    until 1
    local pageMembers = CreateFromClass(MakeGenericClass(List, CTeamMember))
    do
        local i = this.curPage * 5
        while i < math.min(this.memberList.Count, (this.curPage + 1) * 5) do
            CommonDefs.ListAdd(pageMembers, typeof(CTeamMember), this.memberList[i])
            CTeamMgr.Inst:ClearApplicantStatus(i)
            i = i + 1
        end
    end

    this.cacheList:RemoveAll()
    CommonDefs.ListClear(this.memberItems)
    this.cacheList:Init(pageMembers.Count, DelegateFactory.Action_GameObject_int(function (go, index) 
        local cmp = CommonDefs.GetComponent_GameObject_Type(go, typeof(CTeamMemberItem))
        if cmp ~= nil then
            cmp:Init(pageMembers[index], true)
            cmp.acceptAction = DelegateFactory.Action_CTeamMemberItem(function (memberItem) 
                if this.wndType == EnumApplyWndType.Team then
                    CTeamMgr.Inst:AcceptTeamApplyByPlayerId(memberItem.memberData.m_MemberId)
                elseif this.wndType == EnumApplyWndType.TeamGroup then
                    Gac2Gas.RequestAcceptTeamGroupApplicant(memberItem.memberData.m_MemberId)
                end
            end)
            cmp.refuseAction = DelegateFactory.Action_CTeamMemberItem(function (memberItem) 
                if this.wndType == EnumApplyWndType.Team then
                    CTeamMgr.Inst:RefuseTeamApplyByPlayerId(memberItem.memberData.m_MemberId)
                elseif this.wndType == EnumApplyWndType.TeamGroup then
                    Gac2Gas.RequestDelTeamGroupApplicant(memberItem.memberData.m_MemberId)
                end
            end)

            cmp.clickCallback = DelegateFactory.Action_CTeamMemberItem(function (memberItem) 
                if memberItem.acceptAction ~= nil and memberItem.refuseAction ~= nil then
                    --还要把其他的按钮都隐藏掉
                    local cmps = CommonDefs.GetComponentsInChildren_Component_Type(memberItem.transform.parent, typeof(CTeamMemberItem))
                    CommonDefs.EnumerableIterate(cmps, DelegateFactory.Action_object(function (___value) 
                        local item = ___value
                        item:SetButtons(false)
                    end))
                    memberItem:SetButtons(true)
                end
            end)
        end
        CommonDefs.ListAdd(this.memberItems, typeof(CTeamMemberItem), cmp)
    end))
    this:RefreshButtonState()
end
CTeamApplyTabContent.m_RefreshApplyList_CS2LuaHook = function (this) 
    this.curPage = math.max(0, this.curPage)
    this.curPage = math.min(this.maxPageIndex, this.curPage)

    this:UpdatePage()

    --如果翻到最后一页了，
    if this.curPage == this.maxPageIndex then
        EventManager.Broadcast(EnumEventType.UpdateTeamApplyAlertStatus)
    end
end
CTeamApplyTabContent.m_Start_CS2LuaHook = function (this) 
    --RefreshButtonState();
    UIEventListener.Get(this.clearListButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.clearListButton).onClick, MakeDelegateFromCSFunction(this.OnClearButtonClick, VoidDelegate, this), true)

    UIEventListener.Get(this.pageUpButton).onClick = DelegateFactory.VoidDelegate(function (p) 
        --上一页
        this.curPage = this.curPage - 1
        --= Mathf.Max(0, --curPage);
        this:RefreshApplyList()
    end)
    UIEventListener.Get(this.pageDownButton).onClick = DelegateFactory.VoidDelegate(function (p) 
        --下一页
        this.curPage = this.curPage + 1
        --= Mathf.Min(maxPageIndex, ++curPage);
        this:RefreshApplyList()
    end)
end
CTeamApplyTabContent.m_OnClearButtonClick_CS2LuaHook = function (this, go) 
    --请求清空申请列表
    if this.wndType == EnumApplyWndType.Team then
        CTeamMgr.Inst:RequestClearTeamJoinRequest()
    elseif this.wndType == EnumApplyWndType.TeamGroup then
        Gac2Gas.RequestClearTeamGroupApplicant()
    end
    this:RefreshApplyList()
end
