local UIGrid = import "UIGrid"
local CSkillButtonInfo = import "L10.UI.CSkillButtonInfo"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local QnTableView = import "L10.UI.QnTableView"
local GameObject = import "UnityEngine.GameObject"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
local Animation = import "UnityEngine.Animation"
local CScene=import "L10.Game.CScene"

LuaXueWuWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaXueWuWnd, "TipLabel", "TipLabel", UILabel)
RegistChildComponent(LuaXueWuWnd, "SkillSequenceTable", "SkillSequenceTable", QnTableView)
RegistChildComponent(LuaXueWuWnd, "SkillOperateTable", "SkillOperateTable", GameObject)
RegistChildComponent(LuaXueWuWnd, "LeftTime", "LeftTime", UILabel)
RegistChildComponent(LuaXueWuWnd, "Grid", "Grid", UIGrid)
RegistChildComponent(LuaXueWuWnd, "Button1", "Button1", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaXueWuWnd,"m_SkillIdList")
RegistClassMember(LuaXueWuWnd,"m_OrderList")
RegistClassMember(LuaXueWuWnd,"m_CurOrder")
RegistClassMember(LuaXueWuWnd,"m_Tick")
RegistClassMember(LuaXueWuWnd,"m_ClickDelayTick")
RegistClassMember(LuaXueWuWnd,"m_AnswerList")
RegistClassMember(LuaXueWuWnd,"m_Id2SkillBtn")
RegistClassMember(LuaXueWuWnd,"m_SkillSequenceList")
RegistClassMember(LuaXueWuWnd,"m_CanClick")
RegistClassMember(LuaXueWuWnd,"m_CancleClick")
RegistClassMember(LuaXueWuWnd,"m_MaxShowOrderOneLine")
RegistClassMember(LuaXueWuWnd,"m_ResetTick")
function LuaXueWuWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.m_SkillIdList = nil
    self.m_OrderList = nil
    self.m_Tick = nil
    self.m_AnswerList = nil
    self.m_Id2SkillBtn = nil
    self.m_SkillSequenceList = nil
    self.m_ClickDelayTick = nil
    self.m_CanClick = false
    self.Button1.gameObject:SetActive(false)
    self.m_CancleClick = nil
    self.m_ResetTick = nil
    self.m_MaxShowOrderOneLine = 4
    	
	UIEventListener.Get(self.LeftTime.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLeaveButtonClick()
	end)
end

function LuaXueWuWnd:Init()
    Extensions.RemoveAllChildren(self.Grid.transform)
    local grid = self.SkillSequenceTable.transform:Find("ScrollView/Grid"):GetComponent(typeof(UIGrid))
    Extensions.RemoveAllChildren(grid.transform)
    self.m_CurOrder = 0
    if self.m_ClickDelayTick then UnRegisterTick(self.m_ClickDelayTick) self.m_ClickDelayTick = nil end
    if self.m_Tick then UnRegisterTick(self.m_Tick) self.m_Tick = nil end
    local id = LuaZhuJueJuQingMgr.m_LearnSkillPlayId
    local data = ZhuJueJuQing_LearnSkillPlay.GetData(id)
    if not data then return end
    self.m_SkillIdList = Array2Table(data.Skills)
    self.m_OrderList = {}
    for i = 0, data.ClickOrder.Length-1 do
		table.insert(self.m_OrderList, {index = data.ClickOrder[i][0], time = data.ClickOrder[i][1]})
	end
    self.m_AnswerList = {}
    self:InitSkillOrderList()
    self:InitSkillOperateList()
    self:StartNextSkill()
end

function LuaXueWuWnd:InitSkillOperateList()
    self.m_Id2SkillBtn = {}
    for i = 1,#self.m_SkillIdList do
        local id = self.m_SkillIdList[i]
        local btn = NGUITools.AddChild(self.Grid.gameObject, self.Button1)
        btn:SetActive(true)
        local icon = btn.transform:Find("Mask/Icon"):GetComponent(typeof(CUITexture))
        local HitFx = btn.transform:Find("HintFx").gameObject
        local rightFx = btn.transform:Find("ClickFx/right").gameObject
        local wrongFx = btn.transform:Find("ClickFx/wrong").gameObject
        HitFx.gameObject:SetActive(false)
        rightFx.gameObject:SetActive(false)
        wrongFx.gameObject:SetActive(false)
        local skillTemplate = Skill_AllSkills.GetData(Skill_AllSkills.GetSkillId(id, 1))
        icon:LoadSkillIcon(skillTemplate.SkillIcon)
        UIEventListener.Get(btn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            self:OnClickSkillIcon(id)
        end)
        self.m_Id2SkillBtn[id] = {btn = btn, HitFx = HitFx, rightFx = rightFx, wrongFx = wrongFx}
    end
    self.Grid:Reposition()
end

function LuaXueWuWnd:InitSkillOrderList()
    if self.m_CurOrder > #self.m_OrderList then return end
    self.m_SkillSequenceList = {}
    local scrollview = self.SkillSequenceTable.transform:Find("ScrollView"):GetComponent(typeof(UIScrollView))
    local grid = self.SkillSequenceTable.transform:Find("ScrollView/Grid"):GetComponent(typeof(UIGrid))
    for i = 1,#self.m_OrderList do
        local btn = self.SkillSequenceTable:GetFromPool(0)
        self:InitItem(btn, i)
    end
    grid:Reposition()
    scrollview:ResetPosition() 
end

function LuaXueWuWnd:InitItem(btn,row)
    local data = self.m_OrderList[row]
    local skillId = self.m_SkillIdList[data.index]
    local icon = btn.transform:Find("Mask/Icon"):GetComponent(typeof(CUITexture))
    local HitFx = btn.transform:Find("HintFx").gameObject
    local rightFx = btn.transform:Find("ClickFx/right").gameObject
    local wrongFx = btn.transform:Find("ClickFx/wrong").gameObject
    local FailIcon = btn.transform:Find("FailIcon").gameObject
    local PassIcon = btn.transform:Find("PassIcon").gameObject
    HitFx.gameObject:SetActive(false)
    rightFx.gameObject:SetActive(false)
    wrongFx.gameObject:SetActive(false)
    FailIcon.gameObject:SetActive(false)
    PassIcon.gameObject:SetActive(false)
    local skillTemplate = Skill_AllSkills.GetData(Skill_AllSkills.GetSkillId(skillId, 1))
    icon:LoadSkillIcon(skillTemplate.SkillIcon)
    self.m_SkillSequenceList[row] = {
        item = btn,
        skillId = skillId,
        HitFx = HitFx,
        rightFx = rightFx,
        wrongFx = wrongFx,
        FailIcon = FailIcon,
        PassIcon = PassIcon,
    }
end

function LuaXueWuWnd:OnClickSkillIcon(id)
    local orderData = self.m_OrderList[self.m_CurOrder]
    if not orderData or not self.m_CanClick then return end
    self.m_CanClick = false
    local skillId = self.m_SkillIdList[orderData.index] or 0
    local clickItem = self.m_Id2SkillBtn[id]
    local orderItem = self.m_SkillSequenceList[self.m_CurOrder]
    local orderAni = orderItem and orderItem.item:GetComponent(typeof(Animation))
    if id == skillId then
        self.m_AnswerList[self.m_CurOrder] = 1
        clickItem.HitFx.gameObject:SetActive(false)
        clickItem.rightFx.gameObject:SetActive(true)
        clickItem.wrongFx.gameObject:SetActive(false)
        if orderAni then
            orderAni:Stop()
            orderAni:Play("skillbeginnerguidewnd_right")
        end
        orderItem.HitFx.gameObject:SetActive(false)
        orderItem.rightFx.gameObject:SetActive(true)
        orderItem.wrongFx.gameObject:SetActive(false)

    else
        self.m_AnswerList[self.m_CurOrder] = 0
        clickItem.HitFx.gameObject:SetActive(false)
        clickItem.rightFx.gameObject:SetActive(false)
        clickItem.wrongFx.gameObject:SetActive(true)
        if orderAni then
            orderAni:Stop()
            orderAni:Play("skillbeginnerguidewnd_wrong")
        end
        orderItem.HitFx.gameObject:SetActive(false)
        orderItem.rightFx.gameObject:SetActive(false)
        orderItem.wrongFx.gameObject:SetActive(true)
    end
    if self.m_ClickDelayTick then UnRegisterTick(self.m_ClickDelayTick) self.m_ClickDelayTick = nil end
    if self.m_Tick then UnRegisterTick(self.m_Tick) self.m_Tick = nil end
    self.m_ClickDelayTick = RegisterTickOnce(function()
        clickItem.rightFx.gameObject:SetActive(false)
        clickItem.wrongFx.gameObject:SetActive(false)
        orderItem.rightFx.gameObject:SetActive(false)
        orderItem.wrongFx.gameObject:SetActive(false)
        if id == skillId then
            self:StartNextSkill()
        else
            self:RestartFirstSkill()
        end
    end,500)
end
function LuaXueWuWnd:RestartFirstSkill()
    g_MessageMgr:ShowMessage("LearnSkillPlayFail_RestartToBegin")
    self.m_CurOrder = 0
    self.m_AnswerList = {}
    self:SetSkillSequenceGridPosition(self.m_CurOrder)
    self:StartNextSkill()
end
function LuaXueWuWnd:StartNextSkill()
    self.m_CurOrder = self.m_CurOrder + 1
    local orderData = self.m_OrderList[self.m_CurOrder]
    local skillId = 0
    if self.m_CurOrder % self.m_MaxShowOrderOneLine == 0 and self.m_CurOrder < #self.m_OrderList then self:SetSkillSequenceGridPosition(self.m_CurOrder) end
    if orderData then
        skillId = self.m_SkillIdList[orderData.index] or 0
    end
    for k,v in pairs(self.m_Id2SkillBtn) do
        v.HitFx.gameObject:SetActive(k == skillId)
        v.rightFx.gameObject:SetActive(false)
        v.wrongFx.gameObject:SetActive(false)
    end
    for k,v in pairs(self.m_SkillSequenceList) do
        v.FailIcon.gameObject:SetActive(self.m_AnswerList and self.m_AnswerList[k] == 0)
        v.PassIcon.gameObject:SetActive(self.m_AnswerList and self.m_AnswerList[k] == 1)
        v.rightFx.gameObject:SetActive(false)
        v.wrongFx.gameObject:SetActive(false)
        v.HitFx.gameObject:SetActive(k == self.m_CurOrder)
    end
    if self.m_Tick then UnRegisterTick(self.m_Tick) self.m_Tick = nil end
    if self.m_ClickDelayTick then UnRegisterTick(self.m_ClickDelayTick) self.m_ClickDelayTick = nil end
    if self.m_ResetTick then UnRegisterTick(self.m_ResetTick) self.m_ResetTick = nil end
    if not orderData then
        self:CheckResult()
        return
    end
    
    local scrollview = self.SkillSequenceTable.transform:Find("ScrollView"):GetComponent(typeof(UIScrollView))
    local orderItem = self.m_SkillSequenceList[self.m_CurOrder]
    local btnItem = self.m_Id2SkillBtn[skillId]
    local orderAni = orderItem and orderItem.item:GetComponent(typeof(Animation))
    self.m_CanClick = true
    if orderData.time <= 0 then return end
    self.m_Tick = RegisterTickOnce(function()
        self.m_CanClick = false
        self.m_AnswerList[self.m_CurOrder] = 0
        if orderAni then
            orderAni:Stop()
            orderAni:Play("skillbeginnerguidewnd_wrong")
        end
        self.m_ResetTick = RegisterTickOnce(function()
            if orderItem then
                orderItem.FailIcon.gameObject:SetActive(false)
                orderItem.PassIcon.gameObject:SetActive(false)
                orderItem.HitFx.gameObject:SetActive(false)
            end
            if btnItem then
                btnItem.HitFx.gameObject:SetActive(false)
            end
            self:RestartFirstSkill()
        end, 1000)
    end, orderData.time * 1000)
end

function LuaXueWuWnd:CheckResult()
    for k,v in pairs(self.m_SkillSequenceList) do
       if not self.m_AnswerList[k] or self.m_AnswerList[k] == 0 then
            self:RestartFirstSkill()
            return
       end
    end
    Gac2Gas.OnFinishJuQingLearnKungFuPlay()
    if self.m_CancleClick then UnRegisterTick(self.m_CancleClick) self.m_CancleClick = nil end
    self.m_CancleClick = RegisterTickOnce(function()
        CUIManager.CloseUI(CLuaUIResources.XueWuWnd)
    end, 1000)
end

function LuaXueWuWnd:UpdateCountDown()
    if CScene.MainScene then
        if CScene.MainScene.ShowTimeCountDown then
            self.LeftTime.text = self:GetRemainTimeText(CScene.MainScene.ShowTime)
        else
            self.LeftTime.text = nil
        end
    else
        self.LeftTime.text = nil
    end
end

function LuaXueWuWnd:GetRemainTimeText(totalSeconds)
    if totalSeconds < 0 then
        totalSeconds = 0
    end
    if totalSeconds >= 3600 then
        return SafeStringFormat3("%02d:%02d:%02d", math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
    else
        return SafeStringFormat3("%02d:%02d", math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end

function LuaXueWuWnd:OnLeaveButtonClick()
    g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("LEAVE_COPY_CONFIRM"), function ()
		Gac2Gas.RequestLeavePlay()
	end,nil, nil, nil, false)
end
function LuaXueWuWnd:SetSkillSequenceGridPosition(sequenceId)
    local grid = self.SkillSequenceTable.transform:Find("ScrollView/Grid"):GetComponent(typeof(UIGrid))
    local total = 234
    local distance = 105
    if sequenceId <= 0 then
        LuaTweenUtils.TweenPositionX(grid.transform,- total - distance*sequenceId,0.5)
    else
        LuaTweenUtils.TweenPositionX(grid.transform,-(total - distance * 2) - distance*sequenceId,0.5)
    end
end
function LuaXueWuWnd:OnEnable()
    g_ScriptEvent:AddListener("SceneRemainTimeUpdate", self, "UpdateCountDown")
end

function LuaXueWuWnd:OnDisable()
    if self.m_ClickDelayTick then UnRegisterTick(self.m_ClickDelayTick) self.m_ClickDelayTick = nil end
    if self.m_Tick then UnRegisterTick(self.m_Tick) self.m_Tick = nil end
    if self.m_CancleClick then UnRegisterTick(self.m_CancleClick) self.m_CancleClick = nil end
    if self.m_ResetTick then UnRegisterTick(self.m_ResetTick) self.m_ResetTick = nil end
    g_ScriptEvent:RemoveListener("SceneRemainTimeUpdate", self, "UpdateCountDown")
end


--@region UIEvent

--@endregion UIEvent

