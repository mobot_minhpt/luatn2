local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnButton = import "L10.UI.QnButton"
local GameObject = import "UnityEngine.GameObject"
local UITable = import "UITable"
local UILabel = import "UILabel"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local CTipParagraphItem = import "L10.UI.CTipParagraphItem"
local CCarnivalLotteryMgr = import "L10.Game.CCarnivalLotteryMgr"
local CTrackMgr = import "L10.Game.CTrackMgr"
local MessageMgr = import "L10.Game.MessageMgr"

LuaChristmasCreateSnowmanSignWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaChristmasCreateSnowmanSignWnd, "SignButton", "SignButton", QnButton)
RegistChildComponent(LuaChristmasCreateSnowmanSignWnd, "ParagraphTemplate", "ParagraphTemplate", GameObject)
RegistChildComponent(LuaChristmasCreateSnowmanSignWnd, "RuleTable", "RuleTable", UITable)
RegistChildComponent(LuaChristmasCreateSnowmanSignWnd, "RewardTimesLab", "RewardTimesLab", UILabel)

--@endregion RegistChildComponent end

function LuaChristmasCreateSnowmanSignWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.SignButton.OnClick = DelegateFactory.Action_QnButton(function ()
        self:OnSignBtnClicked()
    end)

    Extensions.RemoveAllChildren(self.RuleTable.transform)
    local ruleMessage = MessageMgr.Inst:FormatMessage("Christmas2022_CreateSnowman_Rules", {})
    local ruleTipInfo = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(ruleMessage)
    if ruleTipInfo ~= nil then
        for i = 0, ruleTipInfo.paragraphs.Count - 1 do
            local rule = CUICommonDef.AddChild(self.RuleTable.gameObject, self.ParagraphTemplate):GetComponent(typeof(CTipParagraphItem))
            rule.gameObject:SetActive(true)
            rule:Init(ruleTipInfo.paragraphs[i], 0xffffffff)
        end
    end
    self.RuleTable:Reposition()
end

function LuaChristmasCreateSnowmanSignWnd:Init()
    local playTimes = CCarnivalLotteryMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eShengDanXueRenPlayTimes)
    local maxRewardedTimes = 1
    local rewardedTimes = playTimes > maxRewardedTimes and maxRewardedTimes or playTimes
    local leftTimes = maxRewardedTimes - rewardedTimes
    if leftTimes > 0 then
        self.RewardTimesLab.text = SafeStringFormat3("%d/%d", leftTimes, maxRewardedTimes)
    else
        self.RewardTimesLab.text = SafeStringFormat3("[c][ff0000]%d[-][/c]/%d", leftTimes, maxRewardedTimes)    
    end
end

--@region UIEvent

--@endregion UIEvent

function LuaChristmasCreateSnowmanSignWnd:OnSignBtnClicked()
    CUIManager.instance:CloseAllPopupViews()
    CTrackMgr.Inst:FindNPC(20007310, 16000006, 178, 142, nil, nil)
end