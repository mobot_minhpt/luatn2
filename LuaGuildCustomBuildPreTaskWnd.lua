local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UISprite = import "UISprite"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local UITabBar = import "L10.UI.UITabBar"
local CTaskMgr = import "L10.Game.CTaskMgr"
local Constants = import "L10.Game.Constants"
local CTrackMgr = import "L10.Game.CTrackMgr"

LuaGuildCustomBuildPreTaskWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaGuildCustomBuildPreTaskWnd, "CurrentProgress", "CurrentProgress", UISprite)
RegistChildComponent(LuaGuildCustomBuildPreTaskWnd, "TaskDesc", "TaskDesc", UILabel)
RegistChildComponent(LuaGuildCustomBuildPreTaskWnd, "TaskCondition", "TaskCondition", UILabel)
RegistChildComponent(LuaGuildCustomBuildPreTaskWnd, "JoinBtn", "JoinBtn", GameObject)
RegistChildComponent(LuaGuildCustomBuildPreTaskWnd, "FinishTag", "FinishTag", GameObject)
RegistChildComponent(LuaGuildCustomBuildPreTaskWnd, "TaskTab", "TaskTab", UITabBar)

--@endregion RegistChildComponent end
RegistClassMember(LuaGuildCustomBuildPreTaskWnd,"m_PreTaskStatus")
RegistClassMember(LuaGuildCustomBuildPreTaskWnd,"m_Progress")
RegistClassMember(LuaGuildCustomBuildPreTaskWnd,"m_SelectTaskIdx")
function LuaGuildCustomBuildPreTaskWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	UIEventListener.Get(self.JoinBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnJoinBtnClick()
	end)

    --@endregion EventBind end
	self.m_Progress = 0
	self.m_SelectTaskIdx = 0
	self.TaskTab.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
        self.m_SelectTaskIdx = index
		self:InitTaskView(index)
    end)
end

function LuaGuildCustomBuildPreTaskWnd:Init()
	self.m_PreTaskStatus = LuaGuildCustomBuildMgr.mPreTaskStatus
	local setting = GuildCustomBuildPreTask_GameSetting.GetData()
	local taskCount = setting.PreTaskIds.Length
	if self.m_PreTaskStatus <= EnumGuildCustomBuildPreTaskStatus.eTask1 then
		self.m_Progress = 0
		self.m_SelectTaskIdx = 0
	else
		self.m_Progress = (self.m_PreTaskStatus-EnumGuildCustomBuildPreTaskStatus.eTask1)/taskCount
		self.m_SelectTaskIdx = self.m_PreTaskStatus-EnumGuildCustomBuildPreTaskStatus.eTask1
	end
	self.CurrentProgress.fillAmount = self.m_Progress

	--self.m_SelectTaskIdx = 0
	self.TaskTab:ChangeTab(self.m_SelectTaskIdx, false)
end

function LuaGuildCustomBuildPreTaskWnd:InitTaskView(index)
	local setting = GuildCustomBuildPreTask_GameSetting.GetData()
	local taskIds = setting.PreTaskIds
	local taskId = taskIds[index]
	if not taskId then
		return
	end
	local data = Task_Task.GetData(taskId)
	if not data then
		return
	end
	self.TaskDesc.text = data.TaskDescription
	self.TaskCondition.text = SafeStringFormat3(LocalString.GetString("入帮时间≥%d小时的帮众"),setting.MinJoinTime)

	if self.m_PreTaskStatus <= (EnumGuildCustomBuildPreTaskStatus.eTask1+index) then
		self.JoinBtn:SetActive(true)
		self.FinishTag:SetActive(false)
	else
		self.JoinBtn:SetActive(false)
		self.FinishTag:SetActive(true)
	end
end
--@region UIEvent

function LuaGuildCustomBuildPreTaskWnd:OnJoinBtnClick()
	--self.m_SelectTaskIdx = 0
	local setting = GuildCustomBuildPreTask_GameSetting.GetData()
	if setting.TaskNpcConfig.Length < 8 then
		return
	end
	local npcid = setting.TaskNpcConfig[4]
	local posx = setting.TaskNpcConfig[5]
	local posy = setting.TaskNpcConfig[6]

	CTrackMgr.Inst:FindNPC(npcid, Constants.GuildSceneId, posx, posy, nil, nil)
end

--@endregion UIEvent

