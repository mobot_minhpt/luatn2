local CTrackMgr = import "L10.Game.CTrackMgr"
local CPos = import "L10.Engine.CPos"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CExpressionActionMgr = import "L10.Game.CExpressionActionMgr"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local CCameraParam = import "L10.Engine.CameraControl.CCameraParam"
local CameraMode = import "L10.Engine.CameraControl.CameraMode"
local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"
local CClientObject = import "L10.Game.CClientObject"
local CClientObjectMgr=import "L10.Game.CClientObjectMgr"
local CClientObjectRoot = import "L10.Game.CClientObjectRoot"
local CRenderObject = import "L10.Engine.CRenderObject"
local CEffectMgr = import "L10.Game.CEffectMgr"
local CParticleEffect = import "L10.Game.CParticleEffect"
local EnumWarnFXType =import "L10.Game.EnumWarnFXType"
local CRenderScene = import "L10.Engine.Scene.CRenderScene"
local CWorldPositionFX = import "L10.Game.CWorldPositionFX"
local Quaternion = import "UnityEngine.Quaternion"
local NormalCamera=import "L10.Engine.CameraControl.NormalCamera"
local CHeadInfoWnd = import "L10.UI.CHeadInfoWnd"
local CPlayerHeadInfoItem = import "L10.UI.CPlayerHeadInfoItem"
local GameSetting_Client_Wapper = import "L10.Game.GameSetting_Client_Wapper"
local DelegateFactory = import "DelegateFactory"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local EKickOutType = import "L10.Engine.EKickOutType"
local CActionStateMgr = import "L10.Game.CActionStateMgr"
local EnumActionState = import "L10.Game.EnumActionState"
local EnumSpeakingBubbleType = import "L10.Game.EnumSpeakingBubbleType"
local StringBuilder = import "System.Text.StringBuilder"
local CChatHelper = import "L10.UI.CChatHelper"
local EChatPanel = import "L10.Game.EChatPanel"
LuaDaFuWongMgr = {}

LuaDaFuWongMgr.ZhanBaoOpen = false       -- 战报功能开关
LuaDaFuWongMgr.SmoothMoveCamera = true  -- 平滑移动相机开关

LuaDaFuWongMgr.TongXingZhengInfo = nil  -- 通行证数据
LuaDaFuWongMgr.DailyTaskInfo = nil      -- 每日任务
LuaDaFuWongMgr.RulePage = 1             -- 规则界面页码记录
LuaDaFuWongMgr.PlayResult = nil         -- 结算数据
LuaDaFuWongMgr.MatchStatus = false
LuaDaFuWongMgr.MatchTime = 0
-- 玩法中数据，离开玩法时清理
LuaDaFuWongMgr.Index2Id = nil           -- 索引对应的玩家ID
LuaDaFuWongMgr.PlayerInfo = nil         -- 所有玩家信息(id,轮次,金钱，道具数量，buff情况，名次，任务)
LuaDaFuWongMgr.RoundInfo = nil          -- 每阶段附加信息
LuaDaFuWongMgr.LandInfo = nil           -- 地块信息
LuaDaFuWongMgr.SeriesInfo = nil         -- 地块系列信息
LuaDaFuWongMgr.TaskInfo = nil           -- 公告板任务
LuaDaFuWongMgr.EventInfo = nil          -- 当前世界事件信息
LuaDaFuWongMgr.CurShopData = nil        -- 当前的商店货物信息
LuaDaFuWongMgr.CurHospitalInfo = nil    -- 药铺信息
LuaDaFuWongMgr.CurAniInfo = nil         -- 当前UI动画信息(回合动画,道具动画)
LuaDaFuWongMgr.CurRewardCardInfo = nil  -- 当时奖励的道具卡信息（用于道具卡动画）
LuaDaFuWongMgr.EventDialogInfo = nil    -- 广播事件信息
LuaDaFuWongMgr.VoiceListInfo = nil    -- 广播事件信息
LuaDaFuWongMgr.LandIDToFx = nil         -- 地块特效
LuaDaFuWongMgr.JiangYiHangLastPlayerId = 0 -- 交易行玩家ID

LuaDaFuWongMgr.CurLandId = 0            -- 当前查看的地块ID
LuaDaFuWongMgr.CurPlayerId = 0          -- 当前查看的玩家ID
LuaDaFuWongMgr.CurItemId = 0            -- 当前点击的秘宝ID
LuaDaFuWongMgr.CurGodId = 0             -- 当前触发的神仙ID
LuaDaFuWongMgr.CurQuestionId = 0        -- 当前题目Id
LuaDaFuWongMgr.CurRoundPlayer = 0       -- 当前的玩家回合
LuaDaFuWongMgr.CurRoundArgs = nil       -- 当前回合的参数

LuaDaFuWongMgr.CurRound = nil             -- 当前回合
LuaDaFuWongMgr.CurStage = 0             -- 当前阶段
LuaDaFuWongMgr.CurCameraFollow = 0      -- 当前阶段
LuaDaFuWongMgr.CurTime = 0              -- 当前回合的初始时间
LuaDaFuWongMgr.SpecialId = nil          -- 记录id(医院Id,医院BuffId)
LuaDaFuWongMgr.CurPlayerRound = 0       -- 当前回合玩家ID
LuaDaFuWongMgr.IsMyRound = false        -- 是否自己回合
LuaDaFuWongMgr.GetCardTime = 0          -- 获得道具卡动画时长
LuaDaFuWongMgr.Key = nil                -- 禁移动的key
LuaDaFuWongMgr.RandomEventRO = nil      -- 随机事件场景内npc
LuaDaFuWongMgr.DragCameraRo = nil       -- 控制镜头平移RO
LuaDaFuWongMgr.CardAniTick = nil        -- 道具动画表现的Tick
LuaDaFuWongMgr.MoveRoTick = nil         -- 平滑移动ticks
LuaDaFuWongMgr.BuffInfo = nil           -- buff状态
LuaDaFuWongMgr.GongGaoFxRO = nil        -- 公告栏挂FX的RO
LuaDaFuWongMgr.RoundColor = {"c2a003","db4646","981acd","16a857"}
LuaDaFuWongMgr.LandFxHSVColor = {"F17444","FA4848","6941D4","54FF4C","404DFF"}
LuaDaFuWongMgr.LandFxHSVColor[0] = "404DFF"
LuaDaFuWongMgr.MoveOffset = {{1,1},{-1,1},{1,-1},{-1,-1}}
LuaDaFuWongMgr.Tick = nil
LuaDaFuWongMgr.AniTick = nil
LuaDaFuWongMgr.SyncPosTick = nil  
LuaDaFuWongMgr.IsInSpecialMove = false 
LuaDaFuWongMgr.FakeEngineId2PlayerId = nil
LuaDaFuWongMgr.OnPlayerRefresh = nil
LuaDaFuWongMgr.SoundPlayingEvent = nil
LuaDaFuWongMgr.EventIdToVoicePath = nil
LuaDaFuWongMgr.SetMainEngineId = nil
LuaDaFuWongMgr.RunSoundTick = nil
LuaDaFuWongMgr.OutlineInit = nil
LuaDaFuWongMgr.SoundPlayTick = nil
LuaDaFuWongMgr.ShuaiGodBuffId = 7   -- 衰神buffID
LuaDaFuWongMgr.CanMoveCameraStage = nil
LuaDaFuWongMgr.ChannelInfo = nil
LuaDaFuWongMgr.NeedShowCountDown = nil
EnumDaFuWengStage = {
    GameStart = 1,              -- 游戏开始
    RoundStart = 2,             -- 回合开始
    RoundWorldEvent = 21,       -- 世界事件
    RoundCheck = 3,             -- 回合检查
    RoundCheckAnimation = 31,   -- 回合动画
    RoundCard = 4,              -- 道具和投骰子
    RoundCardAnimation = 41,    -- 道具动画
    RoundDice = 5,              -- 骰子动画
    RoundMove = 6,              -- 移动
    RoundRandomEvent = 7,       -- 随机事件
    RandomEventDog = 71,        -- 野狗事件
	RandomEventBeggar = 72,     -- 乞丐事件
	RandomEventTreasure = 73,   -- 宝箱事件
	RandomEventAverage = 74,    -- 散财事件
	RandomEventPostHouse = 75,  -- 驿站事件
    RoundGod = 79,              -- 神仙事件
    RoundGrid = 8,
    RoundGridEvent = 9,         -- 地块事件
    RoundGridShangHui = 91,     -- 地块商会
    RoundGridShuYuan = 92,      -- 书院
    RoundGridFoYuan = 93,       -- 佛院
    RoundGridBulletinBoard = 94,-- 告示牌
    RoundGridTrade = 10,        -- 交易
    RoundTrade = 101,           -- 地块交易
    RoundPawn = 102,            -- 质押
    RoundGridLvUp = 103,        -- 地块升级
    RoundOneEnd = 11,           -- 一个玩家结束回合
    RoundEnd = 12,              -- 所有玩家结束当前回合
    GameEnd = 13,               -- 游戏结束
}
-- 倒计时类型
EnumDaFuWengCountDownStage = {
    MianDice = 1,   -- 主界面骰子
    CardUse = 2,    -- 道具使用
    BuyLand = 3,    -- 地块交易
    Question = 4,   -- 答题
    ShangHui = 5,   -- 交易行
    WorldEvent = 6, -- 世界事件
    RandomEvent = 7,-- 随机事件
    YaoQian = 8,    -- 摇签
    ZhiYa = 9,      -- 质押
    Task = 10,      -- 公告板
    EventDialog = 11,   -- 事件广播
    WorldEventCard = 12,    -- 世界事件奖励道具
    PlayerCountDown = 13,    -- 玩家头像倒计时
}
EnumDaFuWengOperateEvent = {
    BaoXiang = 1,
    YaoQian = 2,
}
EnumDaFuWengEventType = {
	Dog = 1,
	Beggar= 2,
	Treasure = 3,
	Average = 4,
	PostHouse = 5,
	RandMax = 5,
}
-- { "DaFuWengFinishCurStage", "I", 1000, nil, "lua" }, -- 客户端提前结束当前阶段 ： EnumDaFuWengStage
-- { "DaFuWengThrowDice", "", 1000, nil, "lua" },       -- 客户端请求丢骰子
-- { "DaFuWengUseCard", "II", 1000, nil, "lua" },       -- 客户端请求使用道具卡 ： 道具卡id,选择对象
-- { "DaFuWengGoodsOperate", "bI", 1000, nil, "lua" },  -- 客户端交易商品 ： 买或卖，商品id
-- { "DaFuWengLandTrade", "b", 1000, nil, "lua" },      -- 客户端地块交易 ： 买或卖
-- { "DaFuWengPawn", "bbb", 1000, nil, "lua" },         -- 客户端质押    :   是否质押地块/货物/道具
-- { "DaFuWengShuYuanAnswer", "I", 1000, nil, "lua" }   -- 书院答题 ：选择选项



-- {"DaFuWengShuYuanAnswerResult", "IIbI"}, -- questionId, answer, result, rewardId
-- {"DaFuWengSyncGameInfo", "U"},                       -- 同步游戏数据
--[[
PlayStage,      -- 玩法阶段
GoodsInfo，     -- 交易行信息
RoundNum,       -- 回合
CurPlayIdx,     -- 当前回合的玩家
Order,          -- 一回合内玩家执行顺序
CountdownStartTs, -- 当前阶段倒计时的开始时间（如果有倒计时的话，比如使用道具）
RoundInfo,        -- 是某些阶段需要的额外信息（比如丢骰子的结果，比如播放道具动画的道具id
WorldEventInfo={self.m_WorldEventInfo.eventId, self.m_WorldEventInfo.times},    -- 世界事件信息
RandomEventInfo,    -- 随机事件
GodInfo,            -- 场景神仙事件信息
LandsInfo,         -- 地块信息
TaskInfo,          -- 公告栏任务
PlayerInfo,       -- 玩家信息
{
    Money,  -- 现金
    TotalMoney, -- 总现金
    Pos,        -- 所在地块id
    IsOut,      -- 是否淘汰
    Rank,       -- 排名
    LandsInfo,  -- 地块信息 
    CardInfo,   -- 道具信息
    BuffInfo,   -- buff信息
    GoodsInfo,  -- 货物信息
    BuyMask,    -- 每回合的道具购买信息
    fakePlayerId, -- fake玩家信息
}
]]

function LuaDaFuWongMgr:DaFuWengSyncGameInfo(data_U)
    local data = g_MessagePack.unpack(data_U)
    if not self.BuffInfo then self:InitDaFuWongData() end
    if not self.PlayerInfo then self.PlayerInfo = {} end
    if not self.LandInfo then self:InitDaFuWongLandData() end
    if not self.CurHospitalInfo then self.CurHospitalInfo = {} end
    if not self.FakeEngineId2PlayerId then self.FakeEngineId2PlayerId = {} end
    self:UpdateRoundInfo(data.RoundNum)
    self.Index2Id = data.Order
    self.RoundInfo = data.RoundInfo
    self.CurShopData = data.GoodsInfo
    local id = self.Index2Id[data.CurPlayIdx]
    self.CurPlayerRound = id
    local isMy = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == id
    self.IsMyRound = isMy
    local LastStage = self.CurStage
    self.CurStage = data.PlayStage

    
    local Order = {}
    for k,v in pairs(data.Order) do
        Order[v] = k
    end
    if not self.LandIDToFx and self.CurStage ~= EnumDaFuWengStage.GameStart and self.CurStage ~= EnumDaFuWengStage.GameEnd then 
        self:InitLandFx()
        self:InitGongGaoTaskFx()
    end
    if self.RunSoundTick then UnRegisterTick(self.RunSoundTick) self.RunSoundTick = nil end
    self:UpdateGongGaoTaskInfo(data.TaskInfo)
    self:UpdatePlayerInfo(data.PlayerInfo,Order,data.LandsInfo)
    self:OnPlayerRound(id)
    self:UpdateRandomEventRo(data.RandomEventInfo)
    self:UpdateGodEventRo(data.GodInfo)
    
    self:OnPlayStageUpdate(data.PlayStage,data.RoundInfo,data.CurPlayIdx,data.CountdownStartTs,data.WorldEventInfo)
    if LastStage ~= data.PlayStage or data.PlayStage == EnumDaFuWengStage.RoundCard  then
        g_ScriptEvent:BroadcastInLua("OnDaFuWengStageUpdate",data.PlayStage)
    end
    local stage = self.CurStage
    local id2 = data.RoundInfo and data.RoundInfo.select and self.Index2Id[data.RoundInfo.select]
    if self.CurStage == EnumDaFuWengStage.RoundCardAnimation and id2 then 
        stage = (EnumDaFuWengStage.RoundCardAnimation * 10) + data.RoundInfo.cardId
        self:AddVoiceToPlay(1000 + stage,id)
        self:AddVoiceToPlay(2000 + stage,id2)
    elseif self.CurStage == EnumDaFuWengStage.RoundGod and data.RoundInfo and data.RoundInfo.godId then
        self:AddVoiceToPlay(stage * 10 + data.RoundInfo.godId,id)
    else
        self:AddVoiceToPlay(stage,id)
    end
    
    if self.CurStage == EnumDaFuWengStage.GameEnd then
        self:ClearLandFx()
    end
    
end
function LuaDaFuWongMgr:UpdateRoundInfo(RoundNum)
    if not self.CurRound then self.CurRound = {} end
    self.CurRound.round = RoundNum
    local index = -1
    if not self.CurRound.RoundCount then self.CurRound.RoundCount = DaFuWeng_Setting.GetData().RoundCount end
    if not self.CurRound.CostCount then self.CurRound.CostCount = DaFuWeng_Setting.GetData().CostCount end
    if not self.CurRound.RoundBgColorCode then self.CurRound.RoundBgColorCode = DaFuWeng_Setting.GetData().RoundBgColorCode end
    if not self.CurRound.RoundShakeRate then self.CurRound.RoundShakeRate = DaFuWeng_Setting.GetData().RoundShakeRate end
    for i = 0,self.CurRound.RoundCount.Length - 1 do
        if i ~= self.CurRound.RoundCount.Length - 1 then
            if RoundNum >= self.CurRound.RoundCount[i] and RoundNum < self.CurRound.RoundCount[i + 1] then
                index = i
            end
        else
            if RoundNum >= self.CurRound.RoundCount[i] then index = i end
        end
    end
    self.CurRound.RoundIndex = index
    if index >= 0 then
        self.CurRound.Cost = self.CurRound.CostCount[math.min(index,self.CurRound.CostCount.Length - 1)]
        self.CurRound.BgColorCode = self.CurRound.RoundBgColorCode[math.min(index,self.CurRound.RoundBgColorCode.Length - 1)]
        self.CurRound.ShakeRate = self.CurRound.RoundShakeRate[math.min(index,self.CurRound.RoundShakeRate.Length - 1)]
    else
        self.CurRound.Cost = 1
        self.CurRound.BgColorCode = "453B3B"
        self.CurRound.ShakeRate = -1
    end
end
function LuaDaFuWongMgr:OnPlayStageUpdate(CurStage,roundInfo,CurPlayIdx,CountdownStartTs,WorldEventInfo)
    self.EventInfo = WorldEventInfo
    self.IsInSpecialMove = false  
    local id = self.Index2Id[CurPlayIdx]
    if self.EventDialogInfo and #self.EventDialogInfo.data > 0 then
        self:ShowEventDialog()
    end
    self.CurTime = CountdownStartTs
    if CurStage == EnumDaFuWengStage.RoundCard and self.IsMyRound then     -- 道具卡投骰子阶段
        self:WaitPlayerRole()
    elseif CurStage == EnumDaFuWengStage.RoundCardAnimation then   --道具卡动画
        if self.IsMyRound then
            LuaDaFuWongMgr:EndCountDownTick(EnumDaFuWengCountDownStage.MianDice)
        end
        self:DoRoundCardAnimation(roundInfo.cardId, id, self.Index2Id[roundInfo.select])
    elseif CurStage == EnumDaFuWengStage.RoundDice then -- 骰子动画阶段
        self:ShowTouZiAnim(roundInfo.diceResult)
    elseif CurStage == EnumDaFuWengStage.RoundMove then -- 移动
        --if self.IsMyRound then
            self:PlayerMove(roundInfo.diceResult)
        --end
    elseif CurStage == EnumDaFuWengStage.RoundTrade and self.IsMyRound then   -- 地块交易
        self:OnShowBuyLandWnd(self.PlayerInfo[id].Pos)
    elseif CurStage == EnumDaFuWengStage.RoundGridLvUp and self.IsMyRound then -- 地块升级
        self:OnShowLanLvdWnd(self.PlayerInfo[id].Pos)
    -- elseif CurStage == EnumDaFuWengStage.RoundGridShangHui and self.IsMyRound then    -- 商会交易
    --     g_ScriptEvent:BroadcastInLua("OnDaFuWongJiaoYiHangUpdate")
    --     if not CUIManager.IsLoaded(CLuaUIResources.DaFuWongJiaoyiHangWnd) then
    --         CUIManager.ShowUI(CLuaUIResources.DaFuWongJiaoyiHangWnd)
    --     end
    elseif CurStage == EnumDaFuWengStage.RoundGridShuYuan then -- 书院答题
        self:OnShowDaTiWnd(roundInfo.questionId)
    elseif CurStage == EnumDaFuWengStage.RoundCheckAnimation then   -- 回合动画
        self:DoRoundCheckAnimation(roundInfo.buffId, id)
    elseif CurStage == EnumDaFuWengStage.RoundWorldEvent then       -- 世界事件信息
        self.CurRewardCardInfo = roundInfo.cardReward and roundInfo.cardReward[CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0]
        self:ShowWorldEvent()
    elseif CurStage == EnumDaFuWengStage.RandomEventTreasure then -- 福缘宝箱
        self:OnPlayerEnterRandomEvent(EnumDaFuWengEventType.Treasure)
        self.CurRoundArgs = {}
        self.CurRoundArgs[1] = roundInfo.rewardId
        self.CurRoundArgs[2] = roundInfo.isOverflow
        self.CurRewardCardInfo =  roundInfo.rewardInfo and roundInfo.rewardInfo.card
        CUIManager.ShowUI(CLuaUIResources.DaFuWongBaoXiangWnd)
    elseif CurStage == EnumDaFuWengStage.RoundGridFoYuan then    -- 摇签
        self.CurRoundArgs = {}
        self.CurRoundArgs[1] = roundInfo.resultId
        self.CurRoundArgs[2] = roundInfo.isOverflow
        self.CurRewardCardInfo = roundInfo.rewardInfo and roundInfo.rewardInfo.card
        CUIManager.ShowUI(CLuaUIResources.DaFuWongYaoQianWnd)
    elseif CurStage == EnumDaFuWengStage.RandomEventAverage then    -- 散财童子
        self:OnPlayerEnterRandomEvent(EnumDaFuWengEventType.Average)
        self.CurRoundArgs = roundInfo.averageMoney
        CUIManager.ShowUI(CLuaUIResources.DaFuWongSanCaiWnd)
    elseif CurStage == EnumDaFuWengStage.RandomEventPostHouse then  -- 驿站事件
        self:OnPlayerEnterRandomEvent(EnumDaFuWengEventType.PostHouse)
        CUIManager.ShowUI(CLuaUIResources.DaFuWongYiZhanWnd)
    elseif CurStage == EnumDaFuWengStage.RoundPawn and self.IsMyRound then    -- 质押
        self.CurRoundArgs = roundInfo.needMoney
        CUIManager.ShowUI(CLuaUIResources.DaFuWongZhiYaWnd)
    elseif CurStage == EnumDaFuWengStage.RandomEventBeggar then --  乞讨事件
        self:OnPlayerEnterRandomEvent(EnumDaFuWengEventType.Beggar)
        CUIManager.ShowUI(CLuaUIResources.DaFuWongQiTaoWnd)
    elseif CurStage == EnumDaFuWengStage.RandomEventDog then --  野狗事件
        if roundInfo.immune then
            self:ShowRandomEventThrowAni(EnumDaFuWengEventType.Dog)
        else
            self:AddVoiceToPlay(2414,id)
            self:OnPlayerEnterRandomEvent(EnumDaFuWengEventType.Dog)
            -- local animArgs = {{"1","DaFuWongXiuYangWnd","1.5","6"},{"2","die01","0.5","5"},{"3","16","1"}}
            -- local animInfo = CreateFromClass(MakeArrayClass(MakeArrayClass(String)), #animArgs)
            -- SoundManager.Inst:PlayOneShot("event:/L10/L10_UI/DaFuWeng/UI_DaFuWeng_Bad_Dog", Vector3.zero, nil, 0)
            -- for i = 1,#animArgs do
            --     local ret = Table2Array(animArgs[i], MakeArrayClass(String))
            --     animInfo[i - 1] = ret
            -- end
            local animInfo = DaFuWeng_Setting.GetData().DogEventArgs
            self:AnalysisCardAni(animInfo,0,id,id)
        end
        --CUIManager.ShowUI(CLuaUIResources.DaFuWongXiuYangWnd)
    elseif CurStage == EnumDaFuWengStage.RoundGridBulletinBoard and self.IsMyRound then -- 公告栏
        g_ScriptEvent:BroadcastInLua("OnDaFuWongGongGaoTaskUpdate")
        if not CUIManager.IsLoaded(CLuaUIResources.DaFuWongTaskWnd) then
            CUIManager.ShowUI(CLuaUIResources.DaFuWongTaskWnd)
        end 
    elseif CurStage == EnumDaFuWengStage.RoundGod then      -- 神仙事件
        if self.OutlineInit then self.OutlineInit[self.CurPlayerRound] = false end
        self.CurRewardCardInfo = roundInfo.rewardInfo and roundInfo.rewardInfo.card
        if roundInfo.godId == 2 then self.CurRewardCardInfo = nil end
        self:OnPlayerEnterRandomEvent(roundInfo.godId + EnumDaFuWengStage.RoundGod)
        self:ShowGodEventWnd(roundInfo.godId)
    end
end

function LuaDaFuWongMgr:UpdatePlayerInfo(playerInfo,order,landData)
    local newPlayerData = false
    local hosipitalId = self.SpecialId[1].landId
    local hosipitalBuff = self.SpecialId[1].BuffId
    local ShuiLaoId = self.SpecialId[2].landId
    local shuiLaoBuffId = self.SpecialId[2].BuffId
    local hosipitalBuffInfo = {} 
    local shuiLaoBuffInfo = {}
    local rankInfo = self:GetRound(playerInfo)

    for k,v in pairs(playerInfo) do
        if not v.IsOut then
            self:CheckGuaJi(k,v.IsGuaJi)
            v.Rank = rankInfo[k]
            local BuffInfo = v.BuffInfo
            if BuffInfo[hosipitalBuff] and BuffInfo[hosipitalBuff] > 0 then
                table.insert(hosipitalBuffInfo,{k,BuffInfo[hosipitalBuff],hosipitalBuff})
            end
            if BuffInfo[shuiLaoBuffId] and BuffInfo[shuiLaoBuffId] > 0 then
                table.insert(shuiLaoBuffInfo,{k,BuffInfo[shuiLaoBuffId],shuiLaoBuffId})
            end
            
            self:CheckBuffInfo(BuffInfo,v.fakePlayerId)
        end
       
        if not self.PlayerInfo[k] then
            self.PlayerInfo[k] = {
                name = v.BasicInfo[1],--name,
                class = v.BasicInfo[2],--class,
                gender = v.BasicInfo[3],--gender,
                voicePackage = v.BasicInfo[4] or 0,-- 语音包
                Money = v.Money,
                TotalMoney = v.TotalMoney,
                Pos = v.Pos,
                IsOut =  v.IsOut,
                Rank = v.Rank,
                LandsInfo = v.LandsInfo,
                CardInfo = v.CardInfo,
                BuffInfo = v.BuffInfo,
                GoodsInfo = v.GoodsInfo,
                taskId = 0,
                round = order[k],
                BuyMask = v.BuyMask,
                fakePlayerId = v.fakePlayerId,
                extraRate = self:GetExtraLandCostRate(v.LandsInfo),
                isGuaJi = v.IsGuaJi,
            }
            if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == k then 
                newPlayerData = true
            end
            if newPlayerData then
               self:OnMainPlayerGoodsInfoUpdate() 
            end
            self:OnPlayerInfoChange(k)
        else
            self:CheckAndUpdatePlayerInfoUpdate(k,v)
        end
        if not self.OutlineInit or not self.OutlineInit[k] then
            self:OnPlayerCreate(v.fakePlayerId)
        end
    end

    self:OnMainPlayerCardChange()

    if #hosipitalBuffInfo ~= 0 or #self.CurHospitalInfo[hosipitalId] ~= 0 then
        self.CurHospitalInfo[hosipitalId] = hosipitalBuffInfo
        self:OnLandInfoUpdate(hosipitalId)
    end
    if #shuiLaoBuffInfo ~= 0 or #self.CurHospitalInfo[ShuiLaoId] ~= 0 then
        self.CurHospitalInfo[ShuiLaoId] = shuiLaoBuffInfo
        self:OnLandInfoUpdate(ShuiLaoId)
    end
    self:CheckAndUpdateLandInfo(landData,newPlayerData)

end

function LuaDaFuWongMgr:GetRound(playerInfo)
    local rankInfo = {}
    local totalMoney = {}
     -- 排名显示处理
     for k,v in pairs(playerInfo) do
        if not v.IsOut then 
            local data = {}
            data.id = k
            data.Money = v.TotalMoney
            table.insert(totalMoney,data)
        end
    end
    table.sort(totalMoney,function(a,b) return a.Money > b.Money end)
    local index = 1
    for i = 1,#totalMoney do
        rankInfo[totalMoney[i].id] = index
        if totalMoney[i - 1] and   totalMoney[i - 1].Money == totalMoney[i].Money then
            rankInfo[totalMoney[i].id] = rankInfo[totalMoney[i - 1].id]
        end
        index = index + 1
    end
    return rankInfo
end

function LuaDaFuWongMgr:CheckBuffInfo(BuffInfo,fakePlayerId)
    -- 动作清空
    local needDoAni = false
    local needShowFx = false
    for k,v in pairs(BuffInfo) do
        if self.BuffInfo and self.BuffInfo[k] and not System.String.IsNullOrEmpty(self.BuffInfo[k].DoAni) and self.OutlineInit then
            self.OutlineInit[k] = false
            needDoAni = true
        end
        if self.BuffInfo and self.BuffInfo[k] and self.BuffInfo[k].FxID ~= 0 and self.OutlineInit then
            self.OutlineInit[k] = false
            needShowFx = true
        end
    end
    if not needDoAni then
        local obj = CClientObjectMgr.Inst and CClientObjectMgr.Inst:GetObject(fakePlayerId)
        if obj then
            obj:ShowActionState(EnumActionState.Idle, {})
        end
    end
    -- 特效清空
    if not needShowFx then
        local obj = CClientObjectMgr.Inst and CClientObjectMgr.Inst:GetObject(fakePlayerId)
        if obj then
            obj.RO:RemoveFX("GodBuffFx")
        end
    end
end

function LuaDaFuWongMgr:CheckGuaJi(id,isGuaJi)
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == id then
        if isGuaJi then CUIManager.ShowUI(CLuaUIResources.DaFuWongGuaJiWnd) 
        else CUIManager.CloseUI(CLuaUIResources.DaFuWongGuaJiWnd) end
    end
end

function LuaDaFuWongMgr:CheckAndUpdatePlayerInfoUpdate(id,newdata)
    -- 视情况看能否做定点更新
    local curdata = self.PlayerInfo[id]

    self.PlayerInfo[id].Money = newdata.Money
    self.PlayerInfo[id].TotalMoney = newdata.TotalMoney
    self.PlayerInfo[id].Pos = newdata.Pos
    self.PlayerInfo[id].IsOut = newdata.IsOut
    self.PlayerInfo[id].Rank = newdata.Rank
    self.PlayerInfo[id].LandsInfo = newdata.LandsInfo
    self.PlayerInfo[id].CardInfo = newdata.CardInfo
    self.PlayerInfo[id].BuffInfo = newdata.BuffInfo
    self.PlayerInfo[id].GoodsInfo = newdata.GoodsInfo
    self.PlayerInfo[id].BuyMask = newdata.BuyMask
    self.PlayerInfo[id].fakePlayerId = newdata.fakePlayerId
    self.PlayerInfo[id].extraRate = self:GetExtraLandCostRate(newdata.LandsInfo)
    self.PlayerInfo[id].isGuaJi = newdata.IsGuaJi
    self:OnPlayerInfoChange(id)
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == id then
        --self:OnMainPlayerCardChange()
        self:OnMainPlayerGoodsInfoUpdate()
    end
end

function LuaDaFuWongMgr:CheckAndUpdateLandInfo(newLandInfo,newPlayerData)
    local hasChange = false
    local hasOwner = {}

    local oriOwner,curOwner,oriPrice,curshowBuff,oriCost,curCost,orilv,curlv,oriLvUp,curLvUp = nil
    for k,v in pairs(self.LandInfo) do
        local different = false
        oriOwner = v.Owner
        curOwner = newLandInfo[k] and newLandInfo[k].belong or 0
        v.Owner = curOwner

        oriPrice = v.price
        curPrice = self:GetCurLandPrice(k)
        orishowBuff = v.showBuff
        curshowBuff = self:GetLandShowBuff(k,curOwner)
        oriCost = v.cost
        orilv = v.lv
        curlv = newLandInfo[k] and newLandInfo[k].lv or 1
        v.lv = curlv
        oriLvUp = v.curLvupPrice
        curLvUp = self:GetCurLandLvUpPrice(k)

        if curshowBuff then curCost = 0
        else curCost = self:GetCurLandCost(k) end
        if curlv == orilv + 1 then
            self:ShowLandLvUpFx(k)
        end

        different = oriOwner ~= curOwner or oriPrice ~= curPrice or oriCost ~= curCost or orilv ~= curlv or oriLvUp ~= curLvUp

        if orishowBuff and curshowBuff and (orishowBuff.id ~= curshowBuff.id or orishowBuff.count ~= curshowBuff.count) then different = true end
        if (not orishowBuff and curshowBuff) or (not curshowBuff and orishowBuff) then different = true end

        if CClientMainPlayer.Inst then
            local id = CClientMainPlayer.Inst.Id
            if oriOwner == id or curOwner == id then
                hasChange = hasChange or different
            end
        end

        v.curPrice = curPrice
        v.curCost = curCost
        v.showBuff = curshowBuff
        v.curLvupPrice = curLvUp
        if different then
            self:OnLandInfoUpdate(k)
        end
    end
    if hasChange or newPlayerData then
        self:OnMainPlayerLandInfoUpdate()
    end
end
-- 地块升级特效
function LuaDaFuWongMgr:ShowLandLvUpFx(landId)
    if not landId or not self.LandInfo[landId] then return end
    local pos = self.LandInfo[landId].pos
    local worldPos = Utility.PixelPos2WorldPos(pos[0],pos[1])
    local fxId = DaFuWeng_Setting.GetData().LandLvUpFx
    CEffectMgr.Inst:AddWorldPositionFX(fxId,worldPos,0,0,1,-1,EnumWarnFXType.None,nil,0,0, nil)
end

function LuaDaFuWongMgr:UpdateRandomEventRo(RandomEventList)    -- 更新随机事件NPCRO
    if not self.RandomEventRO then self.RandomEventRO = {} end
    for k,v in pairs(self.RandomEventRO) do
        v.Ro.Visible = false
    end
    for k,v in pairs(RandomEventList) do
        if not self.RandomEventRO[v] then 
            self.RandomEventRO[v] = {} 
            local data = DaFuWeng_RandomEvent.GetData(v)
            local root = CClientObjectRoot.Inst.Other.gameObject
            local name = SafeStringFormat3("DaFuWengRandomNPC_%d",v)
		    self.RandomEventRO[v].Ro = CRenderObject.CreateRenderObject(root,name)
            self.RandomEventRO[v].Ro:LoadMain(data.ModelPath)
            self.RandomEventRO[v].Ro.Scale = data.Scale
            self.RandomEventRO[v].DefaultAni = data.DefaultAni
            self.RandomEventRO[v].EventAni = data.EventAni
            self.RandomEventRO[v].Ro:DoAni(data.DefaultAni,true,0,1,0,true,1)
        end
        self.RandomEventRO[v].Ro.Visible = true
        local landInfo = self.LandInfo[k]
        self.RandomEventRO[v].Ro.Position = Utility.PixelPos2WorldPos(landInfo.movePos[0],landInfo.movePos[1])
    end
end

function LuaDaFuWongMgr:UpdateGodEventRo(GodEventList)
    if not self.RandomEventRO then self.RandomEventRO = {} end
    for k,v in pairs(self.RandomEventRO) do
        if k > EnumDaFuWengStage.RoundGod then
            v.Ro.Visible = false
        end
    end
    for k,v in pairs(GodEventList) do
        local index = v + EnumDaFuWengStage.RoundGod
        if not self.RandomEventRO[index] then 
            self.RandomEventRO[index] = {} 
            local data = DaFuWeng_God.GetData(v)
            local root = CClientObjectRoot.Inst.Other.gameObject
            local name = SafeStringFormat3("DaFuWengGod_%d",v)
		    self.RandomEventRO[index].Ro = CRenderObject.CreateRenderObject(root,name)
            self.RandomEventRO[index].Ro:LoadMain(data.ModelPath)
            self.RandomEventRO[index].Ro.Scale = data.Scale
            self.RandomEventRO[index].DefaultAni = data.DefaultAni
            self.RandomEventRO[index].EventAni = data.EventAni
            self.RandomEventRO[index].Ro:DoAni(data.DefaultAni,true,0,1,0,true,1)
            local fxID = data.FxID
            local fx = CEffectMgr.Inst:AddObjectFX(fxID, self.RandomEventRO[index].Ro, 0, 1, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
            self.RandomEventRO[index].Ro:AddFX(fxID, fx)
        end
        self.RandomEventRO[index].Ro.Visible = true
        local landInfo = self.LandInfo[k]
        self.RandomEventRO[index].Ro.Position = Utility.PixelPos2WorldPos(landInfo.movePos[0],landInfo.movePos[1])
    end

end
function LuaDaFuWongMgr:ShowRandomEventThrowAni(id)
    if not self.RandomEventRO or not self.RandomEventRO[id] then return end
    local RO = self.RandomEventRO[id].Ro
    RO:SetKickOutStatus(true, EKickOutType.Parabola,1, -1, -100)
	local oriPos = RO.Position
	RO.Position = Vector3( oriPos.x - 20,oriPos.y,oriPos.z + 20)
end
function LuaDaFuWongMgr:UpdateGongGaoTaskInfo(taskInfo)
    if not self.TaskInfo then  self.TaskInfo = {} end
    local count = 0
    self.TaskInfo.task = {}
    for k,v in pairs(taskInfo) do
        local task = {}
        task.Id = k
        task.awardId = v.rewardId
        task.receive = v.receive
        task.progress = v.progress
        if v.receive then
            if self.PlayerInfo[v.receive] then
                if not v.finish then
                    self.PlayerInfo[v.receive].taskId = k
                else
                    self.PlayerInfo[v.receive].taskId = 0
                end
            end 
        else
            count = count + 1
        end
        self.TaskInfo.task[k] = task
        --table.insert(self.TaskInfo.task,task)
    end
    if self.TaskInfo.Count ~= count then
        local FxList = DaFuWeng_Setting.GetData().GongGaoTaskFx
        if self.GongGaoFxRO then
            for k,v in pairs(self.GongGaoFxRO) do
                local newFx = CEffectMgr.Inst:AddObjectFX(FxList[count],v,0,1,1,nil,false,EnumWarnFXType.None, Vector3.zero, Vector3.zero)
                v:AddFX("GongGaoTaskFx",newFx)
            end
            self.TaskInfo.Count = count
        end
        
    end
end

function LuaDaFuWongMgr:OnPlayerEnterRandomEvent(type)
    if self.RandomEventRO and self.RandomEventRO[type] then
        self.RandomEventRO[type].Ro:DoAni(self.RandomEventRO[type].EventAni,false,0,1,0,true,1)
        self.RandomEventRO[type].Ro.AniEndCallback = DelegateFactory.Action(function()
            self.RandomEventRO[type].Ro.AniEndCallback = nil
            self.RandomEventRO[type].Ro:DoAni(self.RandomEventRO[type].DefaultAni, true, 0, 1.0, 0, true, 1.0)
        end)
    end
end

function LuaDaFuWongMgr:GetExtraLandCostRate(landList)
    local res = 1
    if not landList then return res end
    for k,v in pairs(landList) do
        if v then
            local landData = self.LandInfo and self.LandInfo[k]
            if landData and landData.effect then
                for i = 0,landData.effect.Length - 1 do
                    if landData.effect[i][0] == 3 and landData.effect[i][1] ~= 0 then
                        res = res * (1 + landData.effect[i][1])
                    end
                end
            end
        end
    end
    return res
end

-- 当前地块售价
function LuaDaFuWongMgr:GetCurLandPrice(id)
    -- 世界事件影响地块售价
    local lv = self.LandInfo[id].lv
    local price = 0
    for i = 0,lv - 1 do
        local lvPrice = self.LandInfo[id].price and self.LandInfo[id].price[i] or 0
        price = price + lvPrice
    end

    if self.EventInfo then
        local eventid = self.EventInfo[1]
        if not self.EventInfo.Data then
            self.EventInfo.Data = DaFuWeng_WorldEvent.GetData(eventid)
        end
        if self.EventInfo.Data and self.EventInfo.Data.Type == 4 then
            price =  price * self.EventInfo.Data.ChangeValue
        end
    end
    return math.floor(price)
end
-- 当前地块升级费用
function LuaDaFuWongMgr:GetCurLandLvUpPrice(id)
    local lv = self.LandInfo[id].lv
    local price = self.LandInfo[id].price and self.LandInfo[id].price[lv] or 0
    -- 世界事件影响升级费用
    if self.EventInfo then
        local eventid = self.EventInfo[1]
        if not self.EventInfo.Data then
            self.EventInfo.Data = DaFuWeng_WorldEvent.GetData(eventid)
        end 
        if self.EventInfo.Data and self.EventInfo.Data.Type == 5 then
            price =  price * self.EventInfo.Data.ChangeValue
        end
    end

    return math.floor(price)
end

-- 当前地块过路费
function LuaDaFuWongMgr:GetCurLandCost(id)
    local lv = self.LandInfo[id].lv
    lv = math.max(0,lv - 1)
    local cost = self.LandInfo[id].cost and self.LandInfo[id].cost[lv] or 0
    local baseCost = 1
    if self.EventInfo then
        local eventid = self.EventInfo[1]
        if not self.EventInfo.Data then
            self.EventInfo.Data = DaFuWeng_WorldEvent.GetData(eventid)
        end
        -- 世界事件影响过路费
        if self.EventInfo.Data and self.EventInfo.Data.Type == 3 then
            baseCost = self.EventInfo.Data.ChangeValue - 1 + baseCost
        end
    end
    -- 房产系列影响过路费
    local owner = self.LandInfo[id].Owner
    local series = self.LandInfo[id].series
    local playerLands = self.PlayerInfo[owner] and self.PlayerInfo[owner].LandsInfo
    local count = 0
    if playerLands then
        for i,j in pairs(playerLands) do
            if j and self.LandInfo[i].series == series and series ~= 0 then
                count = count + 1
            end
        end
    end
    if count >= 2 and self.SeriesInfo[series] then
        local effect = self.SeriesInfo[series].effect
        if count <= effect.Length then
            baseCost = (effect[count - 1] - 1) + baseCost
        end
    end
    
    -- 扫把星buff影响过路费(不是自己的房)
    if CClientMainPlayer.Inst  then
        local mainPlayerInfo = self.PlayerInfo and self.PlayerInfo[CClientMainPlayer.Inst.Id]
        if owner ~= CClientMainPlayer.Inst.Id and mainPlayerInfo and mainPlayerInfo.BuffInfo and mainPlayerInfo.BuffInfo[LuaDaFuWongMgr.ShuaiGodBuffId] and mainPlayerInfo.BuffInfo[LuaDaFuWongMgr.ShuaiGodBuffId] > 0 then
            cost = cost * (1 + DaFuWeng_Setting.GetData().SaoBaXingAddRate)
        end
    end
    -- 特殊建筑影响过路费
    if self.PlayerInfo and self.PlayerInfo[owner] and self.PlayerInfo[owner].extraRate then
        cost = cost * self.PlayerInfo[owner].extraRate 
    end
    -- 回合效果影响过路费
    if self.CurRound and self.CurRound.RoundIndex then
        baseCost = (self.CurRound.Cost - 1) + baseCost
    end
    cost = cost * baseCost
    return math.floor(cost)
end
-- 当前地块不同等级下的过路费(读表)
function LuaDaFuWongMgr:GetCutLandCostByLv(id,lv)
    return self.LandInfo[id].cost and self.LandInfo[id].cost[lv - 1] or 0
end
-- 当前地块不同等级下的售价+升级费(读表)
function LuaDaFuWongMgr:GetCutLandPriceByLv(id,lv)
    return self.LandInfo[id].price and self.LandInfo[id].price[lv - 1] or 0
end


function LuaDaFuWongMgr:InitDaFuWongLandData()
    if not self.LandInfo then
        self.GetCardTime = DaFuWeng_Setting.GetData().AwardCardAniTime
        self.LandInfo = {}
        self.SeriesInfo = {}
        if not self.CurHospitalInfo then self.CurHospitalInfo = {} end
        self.CurHospitalInfo[self.SpecialId[1].landId] = {}
        self.CurHospitalInfo[self.SpecialId[2].landId] = {}
        DaFuWeng_Land.Foreach(function(k,v)
            self.LandInfo[k] = {
                Owner = 0,
                price = v.Price,
                curPrice = v.Price and v.Price[0] or 0,
                cost = v.Cost,
                curCost = v.Cost and v.Cost[0] or 0,
                curLvupPrice = v.Price and v.Price[1] or 0,
                type = v.Type,
                next = v.Next,
                series = 0,
                pawnPrice = v.PawnPrice,
                showBuff = nil,
                pos = v.Pos,
                movePos = v.MovePos,
                fxSize = v.FxSize,
                lv = 1,
                BuffPos = v.BuffPos,
                effect = v.Effect,
            }
        end)
        DaFuWeng_Series.Foreach(function(k,v)
            self.SeriesInfo[k] = {
                effect = v.Effect,
                lands = v.Lands,
                name = v.Name,
                desc = v.Describe,
                border = v.BorderName,
            }
            for i = 0,v.Lands.Length - 1 do
                if self.LandInfo[v.Lands[i]] then
                    self.LandInfo[v.Lands[i]].series = k
                end
            end
        end)
    end
end

function LuaDaFuWongMgr:InitDaFuWongData()
    self.SpecialId = {}
    self.SpecialId[1] = {}
    self.SpecialId[1].landId = DaFuWeng_Setting.GetData().HospitalId
    self.SpecialId[1].BuffId = DaFuWeng_Setting.GetData().HospitalBuffId
    self.SpecialId[2] = {}
    self.SpecialId[2].landId = DaFuWeng_Setting.GetData().ShuiLaoId
    self.SpecialId[2].BuffId = DaFuWeng_Setting.GetData().ShuiLaoBuffId
    self.SpecialId.CameraArgs = {}
    self.SpecialId.CameraArgs.Speed = DaFuWeng_Setting.GetData().MoveCameraSpeed
    self.SpecialId.CameraArgs.Area = DaFuWeng_Setting.GetData().MoveCameraArea
    self.BuffInfo = {}
    self.EventIdToVoicePath = {}
    DaFuWeng_Buff.Foreach(function(k,v)
        local GodData = DaFuWeng_God.GetDataBySubKey("BuffId",k)
        self.BuffInfo[k] = {
            DoAni = v.DoAction,
            FxID = GodData and GodData.FxID or 0,
        }
    end)
    DaFuWeng_VoiceList.Foreach(function(k,v)
        if not self.EventIdToVoicePath[v.Stage] then self.EventIdToVoicePath[v.Stage] = {} end
        self.EventIdToVoicePath[v.Stage][v.PackageID] = {v.VoicePath,v.Duration,v.DialogContent}
    end)
end

function LuaDaFuWongMgr:GetGoodsCurPriceWithId(id)
    local res = {}
    res.price = self.CurShopData[id].price
    res.changeValue = self.CurShopData[id].changeValue
    return res
end

function LuaDaFuWongMgr:GetLandShowBuff(id,curOwner)
    local data = nil
    local playerinfo = self.PlayerInfo[curOwner] 
    if not playerinfo or not playerinfo.BuffInfo  then return nil end
    for k,v in pairs(playerinfo.BuffInfo) do
        local data = DaFuWeng_Buff.GetData(k)
        if data and data.NoToll > 0 and v > 0 then 
            data = {id = k,count = v}  
            return data
        end
    end
    return data
end

function LuaDaFuWongMgr:DoRoundCheckAnimation(buffId, CurPlayerId) -- 做回合动画
    local data = DaFuWeng_Buff.GetData(buffId)
    if not data then return end
    self.CurAniInfo = {id = buffId, playerId = CurPlayerId,time = data.AnimationTime}
    local wndName = ""
    wndName = data and data.AnimationUI
    if not System.String.IsNullOrEmpty(wndName) then
        CUIManager.ShowUI(CLuaUIResources[wndName])
    end
end

function LuaDaFuWongMgr:DoRoundCardAnimation(CardId, CurPlayerId, SelectPlayerId) -- 做道具动画
    local data = DaFuWeng_Card.GetData(CardId)
    if not data then return end
    self.CurAniInfo = {id = CardId, playerId = CurPlayerId,selectPlayerId = SelectPlayerId,time = data.AnimationTime}
    local AnimInfo = data.AnimationUI
    if AnimInfo then
        self:AnalysisCardAni(AnimInfo,CardId,CurPlayerId,SelectPlayerId)
    end
end

function LuaDaFuWongMgr:AnalysisCardAni(AnimInfo,CardId,CurPlayerId,SelectPlayerId)
    self:ClearCardAniTick()
    self.CardAniTick = {}
    if CardId == 7 then -- 安眠香音效
        SoundManager.Inst:PlayOneShot("event:/L10/L10_UI/DaFuWeng/UI_DaFuWeng_Sleep", Vector3.zero, nil, 0)
    end
    local index = 1
    local hasMove = false
    for i = 0,AnimInfo.Length - 1 do
        local animinfo = AnimInfo[i]
        local type = tonumber(animinfo[0])
        if type == 1 then                   -- 显示UI界面
            local wndName = animinfo[1]
            local delayTime = tonumber(animinfo[2])
            local showTime = tonumber(animinfo[3])
            local showWnd = function()
                if showTime then
                    if self.CurAniInfo then self.CurAniInfo.time = showTime + delayTime
                    else self.CurAniInfo = {id = CardId, playerId = CurPlayerId,time = showTime + delayTime} end
                end
                if not System.String.IsNullOrEmpty(wndName) then
                    CUIManager.ShowUI(CLuaUIResources[wndName])
                end 
            end
            if delayTime and delayTime > 0 then
                self.CardAniTick[index] = RegisterTickOnce(showWnd,delayTime * 1000)
                index = index + 1
            else
                showWnd()  
            end
        elseif type == 2 then       -- 做表情动作
            local aniName = tonumber(animinfo[1])   -- 表情ID
            local delayTime = tonumber(animinfo[2])
            local showTime = tonumber(animinfo[3])
            local funcDoExpress = function()
                local playerid = SelectPlayerId
                local playerInfo = self.PlayerInfo[playerid]
                local obj = CClientObjectMgr.Inst and CClientObjectMgr.Inst:GetObject(playerInfo and playerInfo.fakePlayerId or 0)
                if obj then
                    obj:ShowActionState(EnumActionState.Expression, {aniName})
                end
            end
            if delayTime and delayTime > 0 then 
                self.CardAniTick[index] = RegisterTickOnce(funcDoExpress,delayTime * 1000)
                index = index + 1
            else
                funcDoExpress()
            end
        elseif type == 3 then   -- 选中目标传送
            local landId = tonumber(DaFuWeng_Setting.GetData()[animinfo[1]])
            local delayTime = tonumber(animinfo[2])
            local MovePos = function()
                local playerid = SelectPlayerId
                if self.PlayerInfo and self.PlayerInfo[playerid] and self.LandInfo[landId].BuffPos then
                    local landPos = self.LandInfo[landId].BuffPos[self.PlayerInfo[playerid].round - 1]
                    local startPos = self:GetPlayerPos(playerid,false)
                    local targetPos = Utility.PixelPos2WorldPos(landPos[0],landPos[1])
                    self.CurCameraFollow = 0
                    self.DragCameraRo.Position = startPos
                    CameraFollow.Inst:SetFollowObj(self.DragCameraRo,nil)
                    self:MoveROToTarget(self.DragCameraRo,targetPos,function() 
                        if CClientObjectMgr.Inst and CClientObjectMgr.Inst:GetObject(self.PlayerInfo[playerid].fakePlayerId) then
                            local obj = CClientObjectMgr.Inst:GetObject(self.PlayerInfo[playerid].fakePlayerId)
                            CameraFollow.Inst:SetFollowObj(obj.RO,nil) 
                        end
                        self.CurCameraFollow = playerid
                    end)
                end
            end
            if delayTime > 0 then
                self.CardAniTick[index] = RegisterTickOnce(function() self.IsInSpecialMove = true end,math.max(0,delayTime - 0.5) * 1000)
                index = index + 1
                self.CardAniTick[index] = RegisterTickOnce(MovePos,delayTime * 1000)
                index = index + 1
            else
                MovePos()
            end
        elseif type == 4 then   -- 镜头跟随
            local delayTime = tonumber(animinfo[1])
            local showTime = tonumber(animinfo[2])
            local lookAtTarget = function() self:OnCameraLookAtTarget(SelectPlayerId,false,true) end
            local ResetlookAtTarget = function() self:OnCameraLookAtTarget(CurPlayerId,true,true) end
            if delayTime > 0 then
                self.CardAniTick[index] = RegisterTickOnce(lookAtTarget,(delayTime) * 1000)
                index = index + 1
            else
                lookAtTarget()
            end
            if (delayTime + showTime) > 0 then 
                self.CardAniTick[index] = RegisterTickOnce(ResetlookAtTarget,(delayTime + showTime) * 1000)
                index = index + 1
            end
        elseif type == 5 then   --道具卡光线飞入
            if self.IsMyRound then
                g_ScriptEvent:BroadcastInLua("DaFuWengCardAniFlyToPlayer",CardId,SelectPlayerId)
            end
        elseif type == 6 then               --颠倒乾坤动效
            local newItemList = {}
            if self.IsMyRound then
                newItemList = self.PlayerInfo[SelectPlayerId].CardInfo
            elseif CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == SelectPlayerId then
                newItemList = self.PlayerInfo[CurPlayerId].CardInfo
            end
            self:OnItemExchage(newItemList)
        elseif type == 7 then               -- 事件播报
            local sPlayerInfo = self.PlayerInfo and self.PlayerInfo[SelectPlayerId]
            local SPlayerName = sPlayerInfo and self.PlayerInfo[SelectPlayerId].name or ""
            local playerName = self.PlayerInfo[CurPlayerId].name
            local sPlayerColor = self.RoundColor[sPlayerInfo and self.PlayerInfo[SelectPlayerId].round or 1]
            local PlayerColor = self.RoundColor[self.PlayerInfo[CurPlayerId].round]
            local messageName = animinfo[1]
            local showTime = animinfo[2]
            local Msg = g_MessageMgr:FormatMessage(messageName)
            Msg = string.gsub(Msg,"{Scolor}",sPlayerColor)
            Msg = string.gsub(Msg,"{Sname}",SPlayerName)
            Msg = string.gsub(Msg,"{color}",PlayerColor)
            Msg = string.gsub(Msg,"{name}",playerName)
            g_MessageMgr:ShowMessage("DaFuWeng_FightChannel_Msg",Msg)
            self:AddEventDialogToShow(Msg)
            if tonumber(showTime) and tonumber(showTime) == 0 then
                self:ShowEventDialog()
            end
        end
    end
end

function LuaDaFuWongMgr:MoveROToTarget(ro,targetPos,onCompleteFunc)
    if not ro then return end
    if self.MoveRoTick then UnRegisterTick(self.MoveRoTick) self.MoveRoTick = nil end
    if self.SmoothMoveCamera then
        local curPos = ro:GetPosition()
        local Dir = Vector3(targetPos.x - curPos.x,targetPos.y - curPos.y,targetPos.z - curPos.z)
        local time = 0.2
        local deltaTime = 0.01
        local times = time/deltaTime
        local deltaDir = Vector3(Dir.x / times,Dir.y / times,Dir.z / times)
        local curTime = 0
        self.MoveRoTick = RegisterTickWithDuration(function()
            curTime = curTime + 1
            ro.Position = Vector3(curPos.x + deltaDir.x * curTime,curPos.y + deltaDir.y * curTime,curPos.z + deltaDir.z * curTime)
            if curTime > times then
                ro.Position = targetPos
                if onCompleteFunc then onCompleteFunc() end
                UnRegisterTick(self.MoveRoTick)
                self.MoveRoTick = nil
                return
            end
        end,deltaTime * 1000,(time + deltaTime )* 1000)
    else
        ro.Position = targetPos
        if onCompleteFunc then onCompleteFunc() end
    end
end
-- 回合更新
function LuaDaFuWongMgr:OnPlayerRound(playerId)
    if self.CurStage ~= EnumDaFuWengStage.GameStart and self.CurStage ~= EnumDaFuWengStage.RoundStart then 
        self:OnCameraLookAtTarget(playerId,true,self.CurRoundPlayer ~= playerId)
    end
    if self.CurRoundPlayer == playerId then return end
    self.CurRoundPlayer = playerId
    self.JiangYiHangLastPlayerId = 0
    -- 玩家回合
    g_ScriptEvent:BroadcastInLua("OnDaFuWongPlayerRound",playerId)
end
-- 右上角玩家信息更新
function LuaDaFuWongMgr:OnPlayerInfoChange(playerId)
    -- 头顶信息检查
    local playerInfo = self.PlayerInfo[playerId]
    if self.PlayerInfo[playerId].fakePlayerId then
        if not self.FakeEngineId2PlayerId[self.PlayerInfo[playerId].fakePlayerId] then
            self.FakeEngineId2PlayerId[self.PlayerInfo[playerId].fakePlayerId] =  playerId
        end
    end
    if not self.SetMainEngineId then self.SetMainEngineId = {} end
    if playerInfo.fakePlayerId then self.SetMainEngineId[playerInfo.fakePlayerId] = true end
    if not LuaGamePlayMgr:GetNameBoardInfo(playerInfo.fakePlayerId) and playerInfo.fakePlayerId then
        LuaGamePlayMgr:SetNameBoardInfo(playerInfo.fakePlayerId, { 
        show = true,                                    
        displayName = playerInfo.name,
        fontSize = DaFuWeng_Setting.GetData().HeadInfoSize,
        --fontColor = GameSetting_Client_Wapper.Inst.PlayerNameColor_Default,       
        bgVisible = false,},
        true)
    end
    if not LuaGamePlayMgr:GetPlayerSpeakBubbleInfo(playerInfo.fakePlayerId) and playerInfo.fakePlayerId then
        LuaGamePlayMgr:SetPlayerSpeakBubbleInfo(playerInfo.fakePlayerId, { fontSize = DaFuWeng_Setting.GetData().HeadInfoSize})
    end
    -- 玩家信息更新
    g_ScriptEvent:BroadcastInLua("OnDaFuWongPlayerInfoChange",playerId)
end
-- 地块信息更新
function LuaDaFuWongMgr:OnLandInfoUpdate(id)
    self:UpdateLandFx(id)
    g_ScriptEvent:BroadcastInLua("OnDaFuWongLandInfoUpdate",id)
end
-- 玩家道具更新
function LuaDaFuWongMgr:OnMainPlayerCardChange()
    local useCardPlayer = 0
    local selectCardPlayer = 0
    if self.RoundInfo then
        if self.CurStage == EnumDaFuWengStage.RoundCardAnimation then
            useCardPlayer = self.CurRoundPlayer
            selectCardPlayer = self.Index2Id[self.RoundInfo.select]
            local CardId = self.RoundInfo.cardId
            local cardData = DaFuWeng_Card.GetData(CardId)
            if cardData and cardData.AnimationUI then
                for i = 0,cardData.AnimationUI.Length - 1 do
                    local type = tonumber(cardData.AnimationUI[i][0])
                    if type == 5 then           -- 道具成一束光飞到指定位置
                        if self.PlayerInfo[useCardPlayer].CardInfo[CardId] then
                            self.PlayerInfo[useCardPlayer].CardInfo[CardId] = self.PlayerInfo[useCardPlayer].CardInfo[CardId] + 1
                        else
                            self.PlayerInfo[useCardPlayer].CardInfo[CardId] = 1
                        end
                    elseif type == 6 then   -- 颠倒乾坤动效
                        local beforCard = {}
                        local afterCard = {}
                        for k,v in pairs(self.PlayerInfo[useCardPlayer].CardInfo) do
                            beforCard[k] = v
                        end
                        for k,v in pairs(self.PlayerInfo[selectCardPlayer].CardInfo) do
                            afterCard[k] = v
                        end
                        self.PlayerInfo[useCardPlayer].CardInfo = afterCard
                        self.PlayerInfo[selectCardPlayer].CardInfo = beforCard
                        -- self:OnPlayerInfoChange(useCardPlayer)
                        -- self:OnPlayerInfoChange(selectCardPlayer)
                    end
                end
            end
        end
    end
    -- 玩家信息更新
    g_ScriptEvent:BroadcastInLua("OnDaFuWongMainPlayerCardChange")
end
-- 玩家地块信息更新
function LuaDaFuWongMgr:OnMainPlayerLandInfoUpdate()
    g_ScriptEvent:BroadcastInLua("OnDaFuWongMainPlayerLandInfoUpdate")
end
-- 玩家持有货物更新
function LuaDaFuWongMgr:OnMainPlayerGoodsInfoUpdate()
    g_ScriptEvent:BroadcastInLua("OnDaFuWongMainPlayerGoodsInfoUpdate")
end
-- 显示骰子动画
function LuaDaFuWongMgr:ShowTouZiAnim(dot)
    g_ScriptEvent:BroadcastInLua("OnDaFuWongMainPlayerRolTouZi",dot)
end
-- 等待玩家投骰子
function LuaDaFuWongMgr:WaitPlayerRole()
    g_ScriptEvent:BroadcastInLua("OnDaFuWongMainWaitPlayerRolTouZi")
end
function LuaDaFuWongMgr:OnShowItemDetailWnd(id)
    self.CurItemId = id
    CUIManager.ShowUI(CLuaUIResources.DaFuWongItemUseTip)
end
function LuaDaFuWongMgr:OnMatchStatusChange(MatchStatus)
    self.MatchStatus = MatchStatus
    if MatchStatus and self.MatchTime == 0 then
        self.MatchTime = CServerTimeMgr.Inst.timeStamp
    elseif not MatchStatus then
        self.MatchTime = 0
    end
    g_ScriptEvent:BroadcastInLua("UpdateDaFuWongMatchStatus",MatchStatus)
end
function LuaDaFuWongMgr:OnMainPlayerCreated()
    self:SetMainUIVisiable(false)
    self:InitCanMoveCameraStage()
    self:InitNeedShowCountDown()
    self.CurRoundPlayer = 0
    self.CurRound = {}
    self.IsMyRound = false
    self.Tick = {}
    self.AniTick = {}
    self.Key = {}
    self.FakeEngineId2PlayerId = {}
    self.SetMainEngineId = {}
    if CClientMainPlayer.Inst then
        self.CurCameraFollow = CClientMainPlayer.Inst.Id
    else
        self.CurCameraFollow = -1
    end
    
    self:InitDaFuWongData() 
    self:InitDaFuWongLandData()
    -- 禁掉玩家主动移动
    if not self.Key[1] or self.Key[1] <= 0 then
        self.Key[1] = CClientMainPlayer.IsInGamePlayCannotMove:SetValue(false)
    else
        self.Key[1] = CClientMainPlayer.IsInGamePlayCannotMove:SetValue(false,self.Key[1])
    end
    if not self.Key[2] or self.Key[2] <= 0 then
        self.Key[2] = CRenderScene.s_UseDynamicHideFollowRO:SetValue(false)
    else
        self.Key[2] = CRenderScene.s_UseDynamicHideFollowRO:SetValue(false,self.Key[2])
    end
    if not self.DragCameraRo then
        local root = CClientObjectRoot.Inst.Other.gameObject
        self.DragCameraRo = CRenderObject.CreateRenderObject(root,"DaFuWengDragCameraRO")
        self.DragCameraRo.Position = CClientMainPlayer.Inst.RO:GetPosition()
    end
    
    CameraFollow.Inst.m_ForbiddDragOrZoom = true
    CameraFollow.Inst.s_CloseCameraPhysic = true
    CClientMainPlayer.s_EnableSetFollowWithSetPos = true
    if CClientMainPlayer.Inst then
        CClientMainPlayer.Inst.RO.Visible = false
        self.SetMainEngineId[CClientMainPlayer.Inst.EngineId] = true

        -- LuaGamePlayMgr:SetTitleBoardInfo(CClientMainPlayer.Inst.EngineId, {
        --     show = false,     
        -- },false)
        local headInfo = CommonDefs.DictGetValue_LuaCall(CHeadInfoWnd.Instance.headInfoDict, CClientMainPlayer.Inst.EngineId)
        if headInfo then
            headInfo.gameObject:SetActive(false)
        end
        if self.PlayerInfo and self.PlayerInfo[CClientMainPlayer.Inst.Id] then
            self:CheckGuaJi(CClientMainPlayer.Inst.Id,self.PlayerInfo[CClientMainPlayer.Inst.Id].isGuaJi)
        end
    end
    
    self:RegistSyncCameraPosTick()
    CUIManager.ShowUI(CLuaUIResources.DaFuWongPlayLandTopWnd)
    if self.OnPlayerRefresh == nil then
        self.OnPlayerRefresh = DelegateFactory.Action_uint(function(engineId)
            self:OnPlayerCreate(engineId)
        end)
    end
    EventManager.AddListenerInternal(EnumEventType.ClientObjCreate, self.OnPlayerRefresh)
    
end

function LuaDaFuWongMgr:OnMainPlayerDestroyed()
    self:SetMainUIVisiable(true)
    self:ResetDaFuWongChannel()
    if self.Key and self.Key[1] and self.Key[1] > 0 then
        CClientMainPlayer.IsInGamePlayCannotMove:SetValue(true,self.Key[1])
        self.Key[1] = 0
    end
    if self.Key and self.Key[2] and self.Key[2] > 0 then
        CRenderScene.s_UseDynamicHideFollowRO:SetValue(true,self.Key[2])
        self.Key[2] = 0
    end
    CameraFollow.Inst.m_ForbiddDragOrZoom = false
    CameraFollow.Inst.s_CloseCameraPhysic = false
    CClientMainPlayer.s_EnableSetFollowWithSetPos = false
    CUIManager.CloseUI(CLuaUIResources.DaFuWongPlayLandTopWnd)
    CUIManager.CloseUI(CLuaUIResources.DaFuWongGuaJiWnd)
    if CClientMainPlayer.Inst then
        CClientMainPlayer.Inst.RO.Visible = true
        -- LuaGamePlayMgr:SetNameBoardInfo(CClientMainPlayer.Inst.EngineId,nil,false)
        -- LuaGamePlayMgr:SetTitleBoardInfo(CClientMainPlayer.Inst.EngineId, nil, false)
    end
    -- if self.SetMainEngineId then
    --     LuaGamePlayMgr:SetMiniMapMarkInfo(self.SetMainEngineId, nil, true)
    -- end
    if self.SetMainEngineId then
        for k,v in pairs(self.SetMainEngineId) do
            if v then
                LuaGamePlayMgr:SetNameBoardInfo(k, nil, false) 
                LuaGamePlayMgr:SetPlayerSpeakBubbleInfo(k, nil)  
            end         
        end
    end
    -- if self.PlayerInfo then
    --     for k,v in pairs(self.PlayerInfo) do
    --        LuaGamePlayMgr:SetNameBoardInfo(v.fakePlayerId, nil, false) 
    --        LuaGamePlayMgr:SetPlayerSpeakBubbleInfo(v.fakePlayerId, nil)
    --     end
    -- end
    self:UnRegistSyncCameraPosTick()
    self:ClearData()
    EventManager.RemoveListenerInternal(EnumEventType.ClientObjCreate, self.OnPlayerRefresh)
    self.OnPlayerRefresh = nil
    if self.SoundPlayingEvent then
        SoundManager.Inst:StopSound(self.SoundPlayingEvent)
        self.SoundPlayingEvent=nil
    end
end
-- local CSocialWndMgr = import "L10.UI.CSocialWndMgr"
function LuaDaFuWongMgr:InitDaFuWongChannel()
    -- CSocialWndMgr.Channel = EChatPanel.Ally
    CLuaGuideMgr.TryTriggerGuide(243)
end

function LuaDaFuWongMgr:ResetDaFuWongChannel()
    -- if self.ChannelInfo then
    --     local data = CommonDefs.DictGetValue_LuaCall(CChatHelper.s_ChatPanel2DataDict,EChatPanel.Ally)
    --     if data then data.channelName = self.ChannelInfo.name end
    -- end
    -- self.ChannelInfo = nil
end

function LuaDaFuWongMgr:InitCanMoveCameraStage()
    self.CanMoveCameraStage = {}
    self.CanMoveCameraStage[EnumDaFuWengStage.RoundWorldEvent] = true
    self.CanMoveCameraStage[EnumDaFuWengStage.RoundCard] = true
    self.CanMoveCameraStage[EnumDaFuWengStage.RoundDice] = true
    self.CanMoveCameraStage[EnumDaFuWengStage.RoundMove] = true
end
function LuaDaFuWongMgr:InitNeedShowCountDown()
    self.NeedShowCountDown = {}
    self.NeedShowCountDown[EnumDaFuWengStage.RoundCard] = true
    self.NeedShowCountDown[EnumDaFuWengStage.RandomEventBeggar] = true
    self.NeedShowCountDown[EnumDaFuWengStage.RoundGridBulletinBoard] = true
    self.NeedShowCountDown[EnumDaFuWengStage.RoundTrade] = true
    self.NeedShowCountDown[EnumDaFuWengStage.RoundGridLvUp] = true
    self.NeedShowCountDown[EnumDaFuWengStage.RoundPawn] = true
end

function LuaDaFuWongMgr:SetMainUIVisiable(visible)
    --SetUIVisibility
    local Wnd = {CUIResources.CurrentTarget,CUIResources.MiniMap,CUIResources.TopRightMenu,
                CUIResources.RightMenuWnd,CUIResources.Joystick,CUIResources.SkillButtonBoard,
                CUIResources.TaskAndTeam,CUIResources.MainPlayerFrame,CUIResources.ActivityAlert}
    for i = 1,#Wnd do
        CUIManager.SetUIVisibility(Wnd[i], visible, "Dafuweng")
    end
end

function LuaDaFuWongMgr:ClearData()
    self.Index2Id = nil
    self.PlayerInfo = nil
    self.LandInfo = nil
    self.RoundInfo = nil
    self.CurHospitalInfo = nil
    self.CurAniInfo = nil
    self.SeriesInfo = nil
    self.TaskInfo = nil
    self.EventInfo = nil
    self.CurShopData = nil
    self.CurLandId = 0
    self.CurPlayerId = 0
    self.CurQuestionId = 0
    self.CurItemId = 0
    self.CurGodId = 0
    self.CurRound = nil
    self.JiangYiHangLastPlayerId = 0
    self.SpecialId = nil
    self.CurRoundArgs = nil
    self.CurRewardCardInfo = nil
    self.BuffInfo = nil
    self.EventDialogInfo = nil
    self.VoiceListInfo = nil
    self.Key = nil
    self.FakeEngineId2PlayerId = nil
    self.EventIdToVoicePath = nil
    self.OutlineInit = nil
    self.SetMainEngineId = nil
    self.CanMoveCameraStage = nil
    if self.RandomEventRO then
        for k,v in pairs(self.RandomEventRO) do
            if v.Ro then v.Ro:Destroy() end
            v.Ro = nil
        end
    end
    self.RandomEventRO = nil
    if self.DragCameraRo then self.DragCameraRo:Destroy() end
    self.DragCameraRo = nil
   self:ClearLandFx()
    
    if self.MoveRoTick then UnRegisterTick(self.MoveRoTick) self.MoveRoTick = nil end
    if self.RunSoundTick then UnRegisterTick(self.RunSoundTick) self.RunSoundTick = nil end
    if self.SoundPlayTick then UnRegisterTick(self.SoundPlayTick) self.SoundPlayTick = nil end
    self:ClearTick()
    self:ClearCardAniTick()
end

function LuaDaFuWongMgr:ClearLandFx()
    if self.LandIDToFx then
        for k,v in pairs(self.LandIDToFx) do
            if v.fx1 then v.fx1:Destroy() v.fx1 = nil end
            if v.fx2 then v.fx2:Destroy() v.fx2 = nil end
        end
    end
    self.LandIDToFx = nil
    if self.GongGaoFxRO then
        for k,v in pairs(self.GongGaoFxRO) do
            if v then v:Destroy() end
            v = nil
        end
    end
    self.GongGaoFxRO = nil
end
function LuaDaFuWongMgr:ClearTick()
    if self.Tick then
        for k,v in pairs(self.Tick) do
            if v then
                UnRegisterTick(v)
                v = nil
            end
        end
    end
    if self.AniTick then
        for k,v in pairs(self.AniTick) do
            if v then
                UnRegisterTick(v)
                v = nil
            end
        end
    end
    self.Tick = nil
    self.AniTick = nil
    
end

function LuaDaFuWongMgr:ClearCardAniTick()
    if not self.CardAniTick then return end
    for k,v in pairs(self.CardAniTick) do
        if v then UnRegisterTick(v) v = nil end
    end
    self.CardAniTick = nil
end
-- 寻路部分
function LuaDaFuWongMgr:PlayerMove(dot)
    -- if not CClientMainPlayer.Inst then return end
    -- local id = CClientMainPlayer.Inst.Id
    -- local endPos = self.PlayerInfo[id].Pos

    -- local landData = DaFuWeng_Land.GetData(endPos)
    -- if landData then
    --     local x = landData.MovePos[0] + self.MoveOffset[self.PlayerInfo[id].round][1]
    --     local y = landData.MovePos[1] + self.MoveOffset[self.PlayerInfo[id].round][2]
    --     CTrackMgr.Inst:Track(CPos(x, y),Utility.Grid2Pixel(0.0), DelegateFactory.Action(function()
    --         Gac2Gas.DaFuWengFinishCurStage(EnumDaFuWengStage.RoundMove)
    --     end),DelegateFactory.Action(function()
    --         Gac2Gas.DaFuWengFinishCurStage(EnumDaFuWengStage.RoundMove)
    --     end), false)
    -- end
    if self.RunSoundTick then UnRegisterTick(self.RunSoundTick) self.RunSoundTick = nil end
    self.RunSoundTick = RegisterTick(function()
        SoundManager.Inst:PlayOneShot("event:/L10/L10_UI/DaFuWeng/UI_DaFuWeng_FootStep", Vector3.zero, nil, 0)
    end,500)
end

function LuaDaFuWongMgr:StartCountDownTick(index,totalTime,duartionFunc,endFunc)
    if index ~= EnumDaFuWengCountDownStage.PlayerCountDown then
        g_ScriptEvent:BroadcastInLua("OnDaFuWengStartCountDownTick",totalTime)
    end
    if self.Tick and self.Tick[index] then UnRegisterTick(self.Tick[index]) self.Tick[index] = nil end
    if not self.Tick then return end
    local CountDown = math.ceil((LuaDaFuWongMgr.CurTime + totalTime) - CServerTimeMgr.Inst.timeStamp )
    if CountDown <= 0 then
        if duartionFunc then duartionFunc(0) end
    else
        local CurCount = math.floor((LuaDaFuWongMgr.CurTime + totalTime - 1) - CServerTimeMgr.Inst.timeStamp )
        if duartionFunc then duartionFunc(CurCount) end
        self.Tick[index] = RegisterTickWithDuration(function()
            if CurCount <= 0 then
                if endFunc then endFunc() end
                return 
            end
            CurCount = CurCount - 1 
            if duartionFunc then duartionFunc(CurCount)end
            
        end,1000,(CountDown) * 1000)
    end 
end
function LuaDaFuWongMgr:EndCountDownTick(index)
    if self.Tick and self.Tick[index] then UnRegisterTick(self.Tick[index]) self.Tick[index] = nil end
end

function LuaDaFuWongMgr:OnShowDetailLandWnd(landId)
    self.CurLandId = landId
    CUIManager.ShowUI(CLuaUIResources.DaFuWongLandTip)
end

function LuaDaFuWongMgr:OnShowPlayerDetailWnd(playerId)
    self.CurPlayerId = playerId
    CUIManager.ShowUI(CLuaUIResources.DaFuWongPlayerTip)
end

function LuaDaFuWongMgr:OnShowDaTiWnd(questionId)
    self.CurQuestionId = questionId
    CUIManager.ShowUI(CLuaUIResources.DaFuWongDaTiWnd)
end
function LuaDaFuWongMgr:DaFuWengShuYuanAnswerResult(questionId, answer, result, cardRewardInfo_U)
    local CardList = g_MessagePack.unpack(cardRewardInfo_U)
    if CardList then
        self.CurRewardCardInfo = CardList
    end
    g_ScriptEvent:BroadcastInLua("DaFuWengShuYuanAnswerResult",questionId, answer, result, CardList)
end

function LuaDaFuWongMgr:OnDaFuWengRandomEventBeggarSelect(select,cardRewardInfo_U)
    local CardList = g_MessagePack.unpack(cardRewardInfo_U)
    if CardList then
        self.CurRewardCardInfo = CardList
    end
    g_ScriptEvent:BroadcastInLua("DaFuWengRandomEventBeggarSelect",select, CardList)
end

function LuaDaFuWongMgr:OnShowItemWnd(itemId)
    if 	not CClientMainPlayer.Inst or 
    CClientMainPlayer.Inst.Id ~= LuaDaFuWongMgr.CurRoundPlayer or 
    LuaDaFuWongMgr.CurStage ~= EnumDaFuWengStage.RoundCard then 
        g_MessageMgr:ShowMessage("DaFuWeng_Cannot_UseItem")
        return
    end
    self.CurItemId = itemId
    local data = DaFuWeng_Card.GetData(itemId)
    local wndName = ""
    wndName = data and data.OpenWnd
    if data.SelectSelf == 1 then
        local buffList = data.CannotSelectBuff
        if buffList then
            local count = 0
            for k,v in pairs(self.PlayerInfo) do
                if CClientMainPlayer.Inst and k ~= CClientMainPlayer.Inst.Id then
                    local notSelect = false
                    for i = 0,buffList.Length - 1 do
                        local id = buffList[i]
                        if v.BuffInfo[id] and v.BuffInfo[id] > 0 then
                            notSelect = true
                        end
                    end
                    if not notSelect then count = count + 1 end
                end
            end
            if count == 0 then
                g_MessageMgr:ShowMessage("DaFuWeng_NoUseCardTarget")
                return 
            end
        end
    end
    if not System.String.IsNullOrEmpty(wndName) then
        CUIManager.ShowUI(CLuaUIResources[wndName])
    elseif data.SelectSelf == 2 and CClientMainPlayer.Inst then
        Gac2Gas.DaFuWengUseCard(itemId,self.PlayerInfo[CClientMainPlayer.Inst.Id].round)
    end
end

function LuaDaFuWongMgr:OnShowBuyLandWnd(landId)
    self.CurLandId = landId
    CUIManager.ShowUI(CLuaUIResources.DaFuWongBuyLandWnd)
end
function LuaDaFuWongMgr:OnShowLanLvdWnd(landId)
    self.CurLandId = landId
    CUIManager.ShowUI(CLuaUIResources.DaFuWongLandLvWnd)
end
function LuaDaFuWongMgr:ShowWorldEvent()
    if not self.EventInfo then return end
    local id = self.EventInfo[1]
    local eventInfo = DaFuWeng_WorldEvent.GetData(id)
    if not eventInfo then return end
    self.EventInfo.title = eventInfo.Title
    self.EventInfo.text = eventInfo.Describe
    self.EventInfo.effect = eventInfo.ResultDesc
    self.EventInfo.Data = eventInfo

    CUIManager.ShowUI(CLuaUIResources.DaFuWongWorldEventWnd)
    if self.CurRewardCardInfo then
        if self.Tick then
            local Time = DaFuWeng_Countdown.GetData(EnumDaFuWengStage.RoundWorldEvent).Time
            local id = EnumDaFuWengCountDownStage.WorldEventCard
            if self.Tick[id] then UnRegisterTick(self.Tick[id]) self.Tick[id] = nil end
            self.Tick[id] = RegisterTickOnce(function()
                g_ScriptEvent:BroadcastInLua("OnDaFuWengFinishCurStage",EnumDaFuWengStage.RoundWorldEvent)
            end,Time * 1000)
        end
    end
    g_ScriptEvent:BroadcastInLua("OnDaFuWengWordEventUpdate")
end

function LuaDaFuWongMgr:ShowGodEventWnd(godID)
    self.CurGodId = godID
    CUIManager.ShowUI(CLuaUIResources.DaFuWongGodEventWnd)
end
-- 镜头控制部分
function LuaDaFuWongMgr:OnCameraLookAtTarget(playerID, realPos,focus)
    if not CClientObjectMgr.Inst then return end
    if not self.DragCameraRo then return end

    local curObj = nil
    if self.CurCameraFollow == 0 then 
        curObj = self.DragCameraRo
    else
        curObj = CClientObjectMgr.Inst:GetObject(self.PlayerInfo[self.CurCameraFollow].fakePlayerId)
        if curObj then curObj = curObj.RO end
    end
    local startPos = nil
    if curObj then
        startPos = curObj:GetPosition()
    else
        local landPos = self.LandInfo[self.PlayerInfo[self.CurCameraFollow].Pos].movePos
        -- local x = landPos[0] + self.MoveOffset[self.PlayerInfo[self.CurCameraFollow].round][1] * DaFuWeng_Setting.GetData().MoveOffset
        -- local y = landPos[1] + self.MoveOffset[self.PlayerInfo[self.CurCameraFollow].round][2] * DaFuWeng_Setting.GetData().MoveOffset
        local x = landPos[0]
        local y = landPos[1]
        startPos = Utility.PixelPos2WorldPos(x,y)
    end
    local targetPos = self:GetPlayerPos(playerID,realPos)

    -- if self.CanMoveCameraStage and self.CanMoveCameraStage[self.CurStage] then
    --     self.DragCameraRo.Position = startPos
    --     CameraFollow.Inst:SetFollowObj(self.DragCameraRo,nil)
    --     self:MoveROToTarget(self.DragCameraRo,targetPos,nil)
    --     self.CurCameraFollow = 0
    -- elseif playerID ~= self.CurCameraFollow then
    if playerID ~= self.CurCameraFollow and (focus or not (self.CanMoveCameraStage and self.CanMoveCameraStage[self.CurStage])) then
        self.CurCameraFollow = 0
        self.DragCameraRo.Position = startPos
        CameraFollow.Inst:SetFollowObj(self.DragCameraRo,nil)
        self:MoveROToTarget(self.DragCameraRo,targetPos,function() 
            if CClientObjectMgr.Inst and CClientObjectMgr.Inst:GetObject(self.PlayerInfo[playerID].fakePlayerId) then
                local obj = CClientObjectMgr.Inst:GetObject(self.PlayerInfo[playerID].fakePlayerId)
                CameraFollow.Inst:SetFollowObj(obj.RO,nil) 
            end
            self.CurCameraFollow = playerID
        end)
    end
end

function LuaDaFuWongMgr:GetPlayerPos(playerID,realPos)
    local obj = CClientObjectMgr.Inst:GetObject(self.PlayerInfo[playerID].fakePlayerId)
    local targetPos = nil
    if not obj then
        local hosipitalBuff = self.SpecialId[1].BuffId
        local shuiLaoBuff = self.SpecialId[2].BuffId
        if self.PlayerInfo[playerID].BuffInfo[hosipitalBuff] and self.PlayerInfo[playerID].BuffInfo[hosipitalBuff] > 0 and realPos then
            local landPos = self.LandInfo[self.SpecialId[1].landId].BuffPos
            if landPos then landPos = landPos[self.PlayerInfo[playerID].round - 1]
            else landPos = self.LandInfo[self.PlayerInfo[playerID].Pos].movePos end
            
            local x = landPos[0]
            local y = landPos[1]
            targetPos = Utility.PixelPos2WorldPos(x,y)
        elseif self.PlayerInfo[playerID].BuffInfo[shuiLaoBuff] and self.PlayerInfo[playerID].BuffInfo[shuiLaoBuff] > 0 and realPos then
            local landPos = self.LandInfo[self.SpecialId[2].landId].BuffPos
            if landPos then landPos = landPos[self.PlayerInfo[playerID].round - 1]
            else landPos = self.LandInfo[self.PlayerInfo[playerID].Pos].movePos end
            
            local x = landPos[0]
            local y = landPos[1]
            targetPos = Utility.PixelPos2WorldPos(x,y)
        else
            local landPos = self.LandInfo[self.PlayerInfo[playerID].Pos].movePos
            local x = landPos[0]
            local y = landPos[1]
            targetPos = Utility.PixelPos2WorldPos(x,y)
        end
        
    else
        targetPos = obj.RO:GetPosition()
    end
    return targetPos
end

function LuaDaFuWongMgr:OnDragCamera(delta)
    if not (self.CanMoveCameraStage and self.CanMoveCameraStage[self.CurStage]) then return end
    local speed = self.SpecialId.CameraArgs.Speed
    local Area = self.SpecialId.CameraArgs.Area
    local oriPosition = self.DragCameraRo:GetPosition()
    local resPosition = {oriPosition.x + delta.x * speed,oriPosition.y,oriPosition.z + delta.y * speed}
    resPosition[1] = math.max(Area[0],math.min(resPosition[1],Area[2]))
    resPosition[3] = math.max(Area[1],math.min(resPosition[3],Area[3]))
    local curPosition = Vector3(resPosition[1],resPosition[2],resPosition[3])
    if self.CurCameraFollow ~= 0 then
        if NormalCamera.Inst and NormalCamera.Inst.FollowObj then
            self.DragCameraRo.Position = NormalCamera.Inst.FollowObj:GetPosition()
        end
        CameraFollow.Inst:SetFollowObj(self.DragCameraRo,nil)
        self.CurCameraFollow = 0
    end
    self.DragCameraRo.Position = curPosition
end
-- 看能否做一下优化
function LuaDaFuWongMgr:RegistSyncCameraPosTick()
    if self.SyncPosTick then UnRegisterTick(self.SyncPosTick) self.SyncPosTick = false end
    if not self.DragCameraRo or not NormalCamera.Inst then return end
    self.SyncPosTick = RegisterTick(function()
        if not self.IsInSpecialMove then 
            if NormalCamera.Inst and not NormalCamera.Inst.FollowObj then
                local obj = nil
                if self.CurCameraFollow == 0 then ro = self.DragCameraRo 
                elseif self.PlayerInfo and self.PlayerInfo[self.CurCameraFollow] then
                    obj = CClientObjectMgr.Inst and CClientObjectMgr.Inst:GetObject(self.PlayerInfo[self.CurCameraFollow].fakePlayerId)
                end
                if obj then CameraFollow.Inst:SetFollowObj(obj.RO,nil) end
                
            elseif NormalCamera.Inst and (NormalCamera.Inst.FollowObj == self.DragCameraRo and self.CurCameraFollow ~= 0 or 
                CClientMainPlayer.Inst and NormalCamera.Inst.FollowObj == CClientMainPlayer.Inst.RO)then
                local obj = nil
                if self.PlayerInfo and self.PlayerInfo[self.CurCameraFollow] then
                    obj = CClientObjectMgr.Inst and CClientObjectMgr.Inst:GetObject(self.PlayerInfo[self.CurCameraFollow].fakePlayerId)
                end
                if obj then CameraFollow.Inst:SetFollowObj(obj.RO,nil) end
            end
            if NormalCamera.Inst.FollowObj then 
                local pos = NormalCamera.Inst.FollowObj.transform.position
                local pixelPos = Utility.WorldPos2PixelPos(pos)
                Gac2Gas.DaFuWengSetPlayerPos(pixelPos.x,pixelPos.y)
            end
        end
    end,500)
end

function LuaDaFuWongMgr:UnRegistSyncCameraPosTick()
    if self.SyncPosTick then UnRegisterTick(self.SyncPosTick) self.SyncPosTick = false end
end

-- 玩家操作地块事件
-- type 1, 宝箱事件 2, 摇签事件
function LuaDaFuWongMgr:OnPlayerOpreateEvent(type)
    g_ScriptEvent:BroadcastInLua("OnDaFuWongPlayerOpreateEvent",type)
end

LuaDaFuWongMgr.ItemExchageTimeList = {0.3,0.5,0.35,2,0.35,0.2,0.5,0.3,  0.1,1.1,0.3,0.1}
LuaDaFuWongMgr.ItemExchagePos = Vector3(650, -10, 0)
-- 道具交换动效
function LuaDaFuWongMgr:OnItemExchage(newItemList)
    g_ScriptEvent:BroadcastInLua("OnDaFuWongItemExchage",newItemList)
end

function LuaDaFuWongMgr:OnDaFuWengGameResult(result, rank, palyData_U, report_U, reward_U)
    self.PlayResult = {}
    self.PlayResult.result = result
    self.PlayResult.rank = rank
    self.PlayResult.playData = g_MessagePack.unpack(palyData_U)
    self.PlayResult.report = g_MessagePack.unpack(report_U)
    self.PlayResult.reward = g_MessagePack.unpack(reward_U)
    CUIManager.ShowUI(CLuaUIResources.DaFuWongResultWnd)
end
EnumRewardType = {
    None = 0,
    Normal = 1,
    Vip = 2,
    All = 3,
    AllBefore = 7,
}
LuaDaFuWongMgr.ShowRewardInfo = nil
LuaDaFuWongMgr.TongXingZengLevelToExp = nil
LuaDaFuWongMgr.VoicePackage = nil
function LuaDaFuWongMgr:OnTongXingZhengDataResult(ret_U)
    local data = g_MessagePack.unpack(ret_U)
    local lv,curExp = self:GetTongXingZhengLevelAndProgress(data.progress)
    local awardInfo = self:GetTongXingZhengAwardInfo(data.r,lv)
    self.TongXingZhengInfo = {}
    self.TongXingZhengInfo.progress = data.progress
    self.TongXingZhengInfo.lv = lv
    self.TongXingZhengInfo.curExp = curExp
    self.TongXingZhengInfo.vip = data.vip
    self.TongXingZhengInfo.hasReward = awardInfo
    self.VoicePackage = data.voicePackage
    self.DailyTaskInfo = {}
    self.DailyTaskInfo = data.td -- 每日任务（{p=0, r=false} -- p:进度, r:是否完成）
    g_ScriptEvent:BroadcastInLua("OnTongXingZhengDataUpdate")
    if self.ShowRewardInfo and self.ShowRewardInfo.UnlockVip and self.ShowRewardInfo.UnlockVip.needShow then
        self:ShowUnLockAwardWnd()
    end
    if self.ShowRewardInfo and self.ShowRewardInfo.GetReward and self.ShowRewardInfo.GetReward.needShow then
        self:ShowGetRewardWnd()
    end
end

function LuaDaFuWongMgr:GetTongXingZhengLevelAndProgress(progress)
    if not self.TongXingZengLevelToExp then
        self.TongXingZengLevelToExp = {}
        local curExp = 0
        DaFuWeng_TongXingZheng.Foreach(function(k,v)
            self.TongXingZengLevelToExp[k] = {}
            self.TongXingZengLevelToExp[k].exp = curExp
            self.TongXingZengLevelToExp[k].progress = v.NeedExp
            self.TongXingZengLevelToExp[k].Item1 = v.ItemRewards1
            self.TongXingZengLevelToExp[k].Item2 = v.ItemRewards2
            curExp = curExp + v.NeedExp
        end)
    end
    local maxLv = 0
    for k,v in pairs(self.TongXingZengLevelToExp) do
        if progress >= v.exp and progress < v.exp + v.progress then
            return k - 1,progress - v.exp
        end
        maxLv = k
    end
    return maxLv,0
end
-- 通行证是否有可领取奖励
function LuaDaFuWongMgr:TongXingZhengReward()
    if not self.TongXingZhengInfo or not self.TongXingZhengInfo.hasReward then return false end
    local hasNewReward = false
    for k,v in pairs(self.TongXingZhengInfo.hasReward) do
        if k <= self.TongXingZhengInfo.lv then
            if self.TongXingZhengInfo.vip == 1 then
                if v ~= EnumRewardType.All then return true end
            else
                if v ~= EnumRewardType.Normal then return true end
            end
        end
    end
    return false
end

function LuaDaFuWongMgr:GetTongXingZhengAwardInfo(awardInfo,curLv)
    local data = {}
    local AllBefore = false
    for i = curLv,1,-1 do
        if awardInfo[i] then 
            if awardInfo[i] == EnumRewardType.AllBefore then
                AllBefore = true
                data[i] = EnumRewardType.All
            else
                data[i] = awardInfo[i]
            end
        else
            data[i] = AllBefore and EnumRewardType.All or EnumRewardType.None
        end
    end
    return data
end

function LuaDaFuWongMgr:UnLockDaFuWengVipSuccess(vip)
    if vip ~= 1 then return end
    if not  self.ShowRewardInfo then self.ShowRewardInfo = {} end
    self.ShowRewardInfo.UnlockVip = {}
    self.ShowRewardInfo.UnlockVip.needShow = true
    if self.TongXingZhengInfo then
        self:ShowUnLockAwardWnd()
    else
        Gac2Gas.RequestDaFuWengPlayData()
    end
end

function LuaDaFuWongMgr:ShowUnLockAwardWnd()
    self.ShowRewardInfo.UnlockVip.data1 = {}
    self.ShowRewardInfo.UnlockVip.data2 = {}
    local idToCount = {}
    idToCount[1] = {}
    idToCount[2] = {}
    local lv = self.TongXingZhengInfo.lv
    for k,v in pairs(self.TongXingZengLevelToExp) do
        local item = v.Item2[0]
        if k <= lv then
            if not idToCount[1][item[0]] then
                idToCount[1][item[0]] = item[1]
            else
                idToCount[1][item[0]] = idToCount[1][item[0]] + item[1]
            end
        else
            if not idToCount[2][item[0]] then
                idToCount[2][item[0]] = item[1]
            else
                idToCount[2][item[0]] = idToCount[2][item[0]] + item[1]
            end
        end
    end
    for k,v in pairs(idToCount[1]) do
        local data = {}
        data.ItemID = k
        data.Count = v
        table.insert(self.ShowRewardInfo.UnlockVip.data1,data)
    end
    for k,v in pairs(idToCount[2]) do
        local data = {}
        data.ItemID = k
        data.Count = v
        table.insert(self.ShowRewardInfo.UnlockVip.data2,data)
    end

    LuaCommonGetRewardWnd.m_materialName = "gongxijiesuo"
    LuaCommonGetRewardWnd.m_Reward1List = self.ShowRewardInfo.UnlockVip.data1
    if #self.ShowRewardInfo.UnlockVip.data2 > 0 then
        LuaCommonGetRewardWnd.m_Reward2Label = g_MessageMgr:FormatMessage("DaFuWeng_UpgradeVipCanGet")
        LuaCommonGetRewardWnd.m_Reward2List = self.ShowRewardInfo.UnlockVip.data2
    else
        LuaCommonGetRewardWnd.m_Reward2Label = ""
        LuaCommonGetRewardWnd.m_Reward2List = {}
    end
    LuaCommonGetRewardWnd.m_button = {
        {spriteName="blue", buttonLabel = LocalString.GetString("确定"), clickCB = function() 
            CUIManager.CloseUI(CLuaUIResources.DaFuWongTongXingZhengUnLockWnd)
            CUIManager.CloseUI(CLuaUIResources.CommonGetRewardWnd) 
        end},
    }
    CUIManager.ShowUI(CLuaUIResources.CommonGetRewardWnd)
    self.ShowRewardInfo.UnlockVip.needShow = false
end

function LuaDaFuWongMgr:ReceiveDaFuWengRewardSuccess(level, index, resultTbl)
    if not  self.ShowRewardInfo then self.ShowRewardInfo = {} end
    self.ShowRewardInfo.GetReward = {}
    self.ShowRewardInfo.GetReward.needShow = true

    self.ShowRewardInfo.GetReward.args = {level = level,index = index,resultTbl = g_MessagePack.unpack(resultTbl)}
    if self.TongXingZhengInfo then
        self:ShowGetRewardWnd()
    else
        Gac2Gas.RequestDaFuWengPlayData()
    end
end

function LuaDaFuWongMgr:ShowGetRewardWnd()
    self.ShowRewardInfo.GetReward.data1 = {}
    self.ShowRewardInfo.GetReward.data2 = {}
    local idToCount = {}
    idToCount[1] = {}
    idToCount[2] = {}
    local args = self.ShowRewardInfo.GetReward.args
    local unLock = self.TongXingZhengInfo.vip == 1
    local curlevel = self.TongXingZhengInfo.lv
    local level,index = args.level,args.index
    if level ~= 0 or index ~= 0 then
        local data = {}
        local rewardInfo = self.TongXingZengLevelToExp[level]
        local item = rewardInfo.Item1[0]
        if index == 2 then item = rewardInfo.Item2[0] end
        data.ItemID = item[0]
        data.Count = item[1]
        table.insert(self.ShowRewardInfo.GetReward.data1,data)
    elseif level == 0 and index == 0 then
        for i = 1,#args.resultTbl do
            local strinfo= g_LuaUtil:StrSplit(args.resultTbl[i],"-")
            local lv,ind = strinfo[1],strinfo[2]
            lv = tonumber(lv)
            ind = tonumber(ind)

            local item = self.TongXingZengLevelToExp[lv].Item1[0]
            if ind == 2 then item = self.TongXingZengLevelToExp[lv].Item2[0] end
            if not idToCount[1][item[0]] then
                idToCount[1][item[0]] = item[1]
            else
                idToCount[1][item[0]] = idToCount[1][item[0]] + item[1]
            end
        end
    end
    if not unLock then
        for i = 1,curlevel do
            local item = self.TongXingZengLevelToExp[i].Item2[0]
            if not idToCount[2][item[0]] then
                idToCount[2][item[0]] = item[1]
            else
                idToCount[2][item[0]] = idToCount[2][item[0]] + item[1]
            end
        end
    end
    for k,v in pairs(idToCount[1]) do
        local data = {}
        data.ItemID = k
        data.Count = v
        table.insert(self.ShowRewardInfo.GetReward.data1,data)
    end
    for k,v in pairs(idToCount[2]) do
        local data = {}
        data.ItemID = k
        data.Count = v
        table.insert(self.ShowRewardInfo.GetReward.data2,data)
    end
   
    LuaCommonGetRewardWnd.m_materialName = "gongxihuode"
    LuaCommonGetRewardWnd.m_Reward1List = self.ShowRewardInfo.GetReward.data1
    if not unLock then
        LuaCommonGetRewardWnd.m_Reward2Label = g_MessageMgr:FormatMessage("DaFuWeng_UnLockVipCanGet")
        LuaCommonGetRewardWnd.m_Reward2List = self.ShowRewardInfo.GetReward.data2
        LuaCommonGetRewardWnd.m_button = {
            {spriteName="blue", buttonLabel = LocalString.GetString("确定"), clickCB = function() CUIManager.CloseUI(CLuaUIResources.CommonGetRewardWnd)  end},
            {spriteName="yellow", buttonLabel = LocalString.GetString("解锁精装秘卷"), clickCB = function() 
                CUIManager.ShowUI(CLuaUIResources.DaFuWongTongXingZhengUnLockWnd)
                CUIManager.CloseUI(CLuaUIResources.CommonGetRewardWnd)  end},
        }
    else
        LuaCommonGetRewardWnd.m_Reward2Label = ""
        LuaCommonGetRewardWnd.m_Reward2List = {}
        LuaCommonGetRewardWnd.m_button = {
            {spriteName="blue", buttonLabel = LocalString.GetString("确定"), clickCB = function() CUIManager.CloseUI(CLuaUIResources.CommonGetRewardWnd) end},
        }
    end
    
    CUIManager.ShowUI(CLuaUIResources.CommonGetRewardWnd)
    self.ShowRewardInfo.GetReward.needShow = false
end

function LuaDaFuWongMgr:ShowWavewards(playerId, curMoney, changeValue, totalMoney)
    -- if self.CurStage == EnumDaFuWengStage.RoundGridShangHui and playerId ~= self.JiangYiHangLastPlayerId then    -- 商会交易
    --     self:AddVoiceToPlay(1000 + EnumDaFuWengStage.RoundGridShangHui,playerId)
    --     self.JiangYiHangLastPlayerId = playerId
    -- end
    local obj = CClientObjectMgr.Inst and CClientObjectMgr.Inst:GetObject(self.PlayerInfo[playerId].fakePlayerId)
    if not obj then return end
    local Color = NGUIText.ParseColor24("ffffff", 0)
    -- if amount >= 0 then
    --     Color = NGUIText.ParseColor24("ffffff", 0)
    -- else
    --     Color = NGUIText.ParseColor24("ba2727", 0)
    -- end
    SoundManager.Inst:PlayOneShot("event:/L10/L10_UI/DaFuWeng/UI_DaFuWeng_Money", Vector3.zero, nil, 0)
    obj.RO:ShowTextWithIcon("nandutongbao",changeValue,Color,1)
end

function LuaDaFuWongMgr:AddEventDialogToShow(message)
    if not self.EventDialogInfo then 
        self.EventDialogInfo = {}
        self.EventDialogInfo.isShow = false
        self.EventDialogInfo.data = {}
    end
    table.insert(self.EventDialogInfo.data,message)
end

function LuaDaFuWongMgr:AddVoiceToPlay(CurStage,curPlayer)
    if not self.VoiceListInfo then 
        self.VoiceListInfo = {}
        self.VoiceListInfo.isShow = false
        self.VoiceListInfo.data = {}
    end

    if CurStage == 1000 + EnumDaFuWengStage.RoundGridShangHui then
        if curPlayer == self.JiangYiHangLastPlayerId then return end
        table.insert(self.VoiceListInfo.data,{CurStage,curPlayer})
        self.JiangYiHangLastPlayerId = curPlayer
    else
        table.insert(self.VoiceListInfo.data,{CurStage,curPlayer}) 
    end
    
    self:PlayVoice()
end

function LuaDaFuWongMgr:ShowEventDialog()
    if not self.EventDialogInfo then return end
    if self.EventDialogInfo.isShow then return end
    local index = EnumDaFuWengCountDownStage.EventDialog
    self.EventDialogInfo.isShow = true
    if self.Tick and self.Tick[index] then UnRegisterTick(self.Tick[index]) self.Tick[index] = nil end
    if self.EventDialogInfo.data[1] then
        self.EventDialogInfo.text = self.EventDialogInfo.data[1]
        self.EventDialogInfo.data[1] = nil
        CUIManager.ShowUI(CLuaUIResources.DaFuWongEventDialogWnd)
    end
    self.Tick[index] = RegisterTickOnce(function()
        local data = {}
        for k,v in pairs(self.EventDialogInfo.data) do
            if v then table.insert(data,v) end
        end
        self.EventDialogInfo.isShow = false
        self.EventDialogInfo.data = data
        if #data > 0 then
            self:ShowEventDialog()
        end
    end,4000)
end
function LuaDaFuWongMgr:PlayVoice()
    if not self.VoiceListInfo then return end
    if self.VoiceListInfo.isShow then return end
    
    if self.SoundPlayTick then UnRegisterTick(self.SoundPlayTick) self.SoundPlayTick = nil end
    
    local delayTime = 0
    if self.VoiceListInfo.data[1] then
        local curStage = self.VoiceListInfo.data[1][1]
        local curPlayer = self.VoiceListInfo.data[1][2]
        local packageIndex = self.PlayerInfo and self.PlayerInfo[curPlayer] and (self.PlayerInfo[curPlayer].voicePackage) or 0
        self.VoiceListInfo.data[1] = nil
        if self.EventIdToVoicePath[curStage] and self.EventIdToVoicePath[curStage][packageIndex] then
            self.VoiceListInfo.isShow = true
            -- if self.SoundPlayingEvent then
            --     SoundManager.Inst:StopSound(self.SoundPlayingEvent)
            --     self.SoundPlayingEvent=nil
            -- end
            local data = self.EventIdToVoicePath[curStage][packageIndex]
            delayTime = data[2]
            --self.SoundPlayingEvent = SoundManager.Inst:PlayOneShot(data[1],Vector3.zero,nil,0)
            SoundManager.Inst:StartDialogSound(data[1])
            local engineId = self.PlayerInfo and self.PlayerInfo[curPlayer] and self.PlayerInfo[curPlayer].fakePlayerId
            if engineId then
                EventManager.BroadcastInternalForLua(EnumEventType.SpeakingCreate, {engineId,data[3],EnumSpeakingBubbleType.Default,delayTime})
            end
        end
    end
    local nextfunc = function()
        local data = {}
        for k,v in pairs(self.VoiceListInfo.data) do
            if v then table.insert(data,v) end
        end
        self.VoiceListInfo.isShow = false
        self.VoiceListInfo.data = data
        if #data > 0 then
            self:PlayVoice()
        end
    end
    if delayTime <= 0 then
        nextfunc()
    else
        self.SoundPlayTick = RegisterTickOnce(function() nextfunc() end,delayTime * 1000)
    end
end
function LuaDaFuWongMgr:needShowEventBroadCast(playerId,stage,result,extraInfo_U)
    local playerInfo = self.PlayerInfo[playerId]
    local stageInfo = DaFuWeng_BroadcastDialog.GetData(stage)
    if not playerInfo then return end
    local playerName = self.PlayerInfo[playerId].name
    local playerColor = self.RoundColor[self.PlayerInfo[playerId].round]
    local msgType,rewardMsg,buffname,cost,sPlayer,strInfo = self:GetRewardMsg(g_MessagePack.unpack(extraInfo_U))
    local resultMsg = nil
    if stageInfo and stageInfo.Type == 8 then
        if stage == 0 then
            SoundManager.Inst:PlayOneShot("event:/L10/L10_UI/DaFuWeng/UI_DaFuWeng_GongGaoLan_WanCheng", Vector3.zero, nil, 0)
        end
        if msgType == 2 then resultMsg = g_MessageMgr:FormatMessage(stageInfo.Type2)
        elseif msgType == 3 then  resultMsg = g_MessageMgr:FormatMessage(stageInfo.Type3)
        elseif msgType == 4 then  resultMsg = g_MessageMgr:FormatMessage(stageInfo.Type4)
        elseif msgType == 5 then resultMsg = g_MessageMgr:FormatMessage(stageInfo.Type5) end
        
    elseif stageInfo and stageInfo.Type == 1 then
        resultMsg = stageInfo.Type1
    elseif stageInfo and (stageInfo.Type == 2 or (stageInfo.Type == 4 and result)) then
        if msgType == 2 then resultMsg = stageInfo.Type2 end
        if msgType == 3 then resultMsg = stageInfo.Type3 end
        if msgType == 4 then resultMsg = stageInfo.Type4 end
        if msgType == 5 then resultMsg = stageInfo.Type5 end
    elseif stageInfo and stageInfo.Type == 3 then
        if not result then resultMsg = stageInfo.Type1
        else resultMsg = stageInfo.Type5 end
    elseif stageInfo and stageInfo.Type == 4 and not result then
        resultMsg = stageInfo.Type1
    elseif stageInfo and stageInfo.Type == 5 then
        if msgType == 1 then resultMsg = stageInfo.Type1 end
        if msgType == 2 then resultMsg = stageInfo.Type2 end
        if msgType == 3 then resultMsg = stageInfo.Type3 end
        if msgType == 4 then resultMsg = stageInfo.Type4 end
        if msgType == 5 then resultMsg = stageInfo.Type5 end
    elseif stageInfo and stageInfo.Type == 6 then
        self:AddVoiceToPlay(stage,playerId)
    end
    if not System.String.IsNullOrEmpty(resultMsg) then
        resultMsg = string.gsub(resultMsg,"{color}",playerColor)
        resultMsg = string.gsub(resultMsg,"{name}",playerName)
        
        if not System.String.IsNullOrEmpty(buffname) then
            resultMsg = string.gsub(resultMsg,"{buff}",buffname)
        end
        if not System.String.IsNullOrEmpty(rewardMsg) then
            resultMsg = string.gsub(resultMsg,"{reward}",rewardMsg)
        end
        if tonumber(cost) > 1 then 
            resultMsg = string.gsub(resultMsg,"{Cost}",tostring(cost))
        end
        if sPlayer then
            resultMsg = string.gsub(resultMsg,"{Scolor}",sPlayer.Color)
            resultMsg = string.gsub(resultMsg,"{Sname}",sPlayer.name)
        end
        if strInfo and #strInfo > 0 then
            for i = 1, #strInfo do
                local changeMsg = SafeStringFormat3("{strInfo%d}",i)
                resultMsg = string.gsub(resultMsg,changeMsg,strInfo[i])
            end
        end
        if stageInfo and bit.band(stageInfo.ShowChannelOrBroadcast , 1) ~= 0 then
            self:AddEventDialogToShow(resultMsg)
            self:ShowEventDialog()
        end
        if stageInfo and bit.band(stageInfo.ShowChannelOrBroadcast , 2) ~= 0 then
            g_MessageMgr:ShowMessage("DaFuWeng_FightChannel_Msg",resultMsg)
        end
    end
    if stageInfo and (stageInfo.Type == 3 or stageInfo.Type == 4) then
        if result then self:AddVoiceToPlay(1000 + stage,playerId)
        else self:AddVoiceToPlay(2000 + stage,playerId) end        
    end
end

function LuaDaFuWongMgr:GetRewardMsg(rewardInfo)
    local type,res,buff,cost,sPlayer,strInfo = 5,"","",1,nil,nil
    if not rewardInfo then return type,res,buff,cost end
    local cardData = {}
    local goodsData = {}
    if rewardInfo.card then
        for k,v in pairs(rewardInfo.card) do
            if v and v > 0 then table.insert(cardData,k) end
        end
    end
    if rewardInfo.goods then
        for k,v in pairs(rewardInfo.goods) do
            if v and v > 0 then table.insert(goodsData,k) end
        end
    end
        
    if rewardInfo.money and rewardInfo.money > 0 then type = 2 res = tostring(rewardInfo.money)
    elseif rewardInfo.addMoney and rewardInfo.addMoney > 0 then type = 2 res = tostring(rewardInfo.addMoney)
    elseif rewardInfo.godId and rewardInfo.godId > 0 then type = rewardInfo.godId
    elseif #cardData > 0 then
        type = 4
        local str = {}
        for i = 1,#cardData do
            local name = DaFuWeng_Card.GetData(cardData[i]).Name
            local num = rewardInfo.card[cardData[i]]
            table.insert(str,SafeStringFormat3("%sX%d",name,num))
        end
        res = table.concat(str,LocalString.GetString("、"))
    elseif #goodsData > 0 then
        type = 3
        local str = {}
        for i = 1,#goodsData do
            local name = DaFuWeng_Goods.GetData(goodsData[i]).Name
            local num = rewardInfo.goods[goodsData[i]]
            table.insert(str,SafeStringFormat3("%sX%d",name,num))
        end
        res = table.concat(str,LocalString.GetString("、"))
    elseif rewardInfo.landList and #rewardInfo.landList > 0 then
        type = 1
        local str = {}
        for i = 1,#rewardInfo.landList do
            local name = DaFuWeng_Land.GetData(rewardInfo.landList[i]).Name
            table.insert(str,name)
        end
        res = table.concat(str,LocalString.GetString("、"))
    end
    if rewardInfo.Cost and rewardInfo.Cost > 0 then  cost = tostring(rewardInfo.Cost) end
    if rewardInfo.SPlayerId and self.PlayerInfo[rewardInfo.SPlayerId] then 
        sPlayer = {
            name = self.PlayerInfo[rewardInfo.SPlayerId].name,
            Color = self.RoundColor[self.PlayerInfo[rewardInfo.SPlayerId].round],
        }
    end
    if rewardInfo.strInfo and #rewardInfo.strInfo > 0 then
        strInfo = rewardInfo.strInfo
    end
    if rewardInfo.buffId then
        local data = DaFuWeng_Buff.GetData(rewardInfo.buffId)
        if data then buff = data.Name end
    end
    if not res then res = "" end
    return type,res,buff,cost,sPlayer,strInfo
end
function LuaDaFuWongMgr:InitGongGaoTaskFx()
    if not self.GongGaoFxRO then
        self.GongGaoFxRO = {}
        local root = CClientObjectRoot.Inst.Other.gameObject
        local GongGaoTaskLand = DaFuWeng_Setting.GetData().BulletinBoardIdList
        for i = 0,GongGaoTaskLand.Length - 1 do
            self.GongGaoFxRO[i + 1] = CRenderObject.CreateRenderObject(root,SafeStringFormat3("DaFuWengGongGaoFxRO_%d",i))
            local landPos = DaFuWeng_Land.GetData(GongGaoTaskLand[i]).MovePos
            local pos = Utility.PixelPos2WorldPos(landPos[0], landPos[1])
            self.GongGaoFxRO[i + 1].Position = Vector3(pos.x ,pos.y + 0.5,pos.z)
            self.GongGaoFxRO[i + 1].Direction = 180
        end
    end
end
function LuaDaFuWongMgr:InitLandFx()
    local fxId = DaFuWeng_Setting.GetData().LandFx
    self.LandIDToFx = {}
    local GongGaoTaskLand = DaFuWeng_Setting.GetData().BulletinBoardIdList
    local NotShowLandFx = {}
    for i = 0,GongGaoTaskLand.Length - 1 do
        NotShowLandFx[GongGaoTaskLand[i]] = true
    end
    DaFuWeng_Land.Foreach(function(key,val)
        self.LandIDToFx[key] = {}
        if NotShowLandFx[key] then 
            self.LandIDToFx[key].needChangeColor = false
            self.LandIDToFx[key].fx1 = nil
            self.LandIDToFx[key].fx2 = nil
        else
            if val.Type == 2 then
                self.LandIDToFx[key].needChangeColor = true
            else
                self.LandIDToFx[key].needChangeColor = false
            end

            local pos1 = Utility.PixelPos2WorldPos(val.MovePos[0], val.MovePos[1])
            --local pos2 = Utility.PixelPos2WorldPos((val.FxSize[2] + val.FxSize[0]) / 2, (val.FxSize[3] + val.FxSize[1]) / 2)
            local size = {math.abs(val.FxSize[2] - val.FxSize[0]) / 64,math.abs(val.FxSize[3] - val.FxSize[1]) / 64}
            local dir = val.MovePos[2]

            local onLoadFinish1 = DelegateFactory.Action_GameObject(function(go)
                local wf = go.transform:GetComponent(typeof(CWorldPositionFX))
                wf.transform.localRotation = Quaternion.Euler(0, dir, 0)
                local index = 0
                if not self.LandIDToFx[key].needChangeColor then index = 5 else index = 0 end
                self.LandIDToFx[key].curOwner = 0
                
                if self.LandInfo and self.PlayerInfo and self.LandInfo[key].Owner ~= 0 then
                    local ownerId = self.LandInfo[key].Owner
                    index = self.PlayerInfo[ownerId].round
                    self.LandIDToFx[key].curOwner = self.LandInfo[key].Owner
                end
                local hsvList = self:GetHSVLandFxColor(index)
    
                wf:SetColor(nil,hsvList)

            end)
            -- local onLoadFinish2 = DelegateFactory.Action_GameObject(function(go)
            --     local wf = go.transform:GetComponent(typeof(CWorldPositionFX))
            --     local multyEffects = CommonDefs.GetComponentsInChildren_Component_Type(go.transform, typeof(CParticleEffect))
            --     if multyEffects then
            --         for i = 0,multyEffects.Length - 1 do
            --             multyEffects[i].transform.localScale = Vector3(size[1],size[2],1)
            --         end
            --     end
            --     local index = 0
            --     if not self.LandIDToFx[key].needChangeColor then index = 5 else index = 0 end
            --     self.LandIDToFx[key].curOwner = 0
            --     if self.LandInfo and self.PlayerInfo and self.LandInfo[key].Owner ~= 0 then
            --         local ownerId = self.LandInfo[key].Owner
            --         index = self.PlayerInfo[ownerId].round
            --         self.LandIDToFx[key].curOwner = self.LandInfo[key].Owner
            --     end
            --     local hsvList = self:GetHSVLandFxColor(index)
    
            --     wf:SetColor(nil,hsvList)
            -- end)

            local fx1 = CEffectMgr.Inst:AddWorldPositionFX(fxId[0],pos1,0,0,1,-1,EnumWarnFXType.None,nil,0,0, onLoadFinish1)
            --local fx2 = CEffectMgr.Inst:AddWorldPositionFX(fxId[1],pos2,0,0,1,-1,EnumWarnFXType.None,nil,0,0, onLoadFinish2)

            self.LandIDToFx[key].fx1 = fx1
            --self.LandIDToFx[key].fx2 = fx2
        end
    end)
end
function LuaDaFuWongMgr:UpdateLandFx(landId)
    if not self.LandIDToFx then return end
    -- if not self.LandIDToFx[landId] or not self.LandIDToFx[landId].fx1 or not self.LandIDToFx[landId].fx2 then return end
    if not self.LandIDToFx[landId] or not self.LandIDToFx[landId].fx1 or not self.LandIDToFx[landId].fx1.m_FX then return end
    if self.LandIDToFx[landId].curOwner == self.LandInfo[landId].Owner then return end
    -- if not self.LandIDToFx[landId].fx1.m_FX or not self.LandIDToFx[landId].fx2.m_FX then return end
   
    local ownerId = self.LandInfo[landId].Owner
    local index = self.PlayerInfo[ownerId] and self.PlayerInfo[ownerId].round or 0

    local hsvList = self:GetHSVLandFxColor(index)

    self.LandIDToFx[landId].fx1:SetColor(nil,hsvList)
    --self.LandIDToFx[landId].fx2:SetColor(nil,hsvList)
    self.LandIDToFx[landId].curOwner = ownerId
end

function LuaDaFuWongMgr:GetHSVLandFxColor(index)
    if not self.LandFxHSVColor then return end
    local color = NGUIText.ParseColor24(self.LandFxHSVColor[index],0)
    local colorArray = Table2Array({color},MakeArrayClass(Color))
    local hsvList = CEffectMgr.GetHSVColors(self.LandFxHSVColor[4],colorArray)
    return hsvList
end

function LuaDaFuWongMgr:OnPlayerCreate(engineId)
    if not self.FakeEngineId2PlayerId or not self.PlayerInfo then return end
    local obj = CClientObjectMgr.Inst and CClientObjectMgr.Inst:GetObject(engineId)
    local id = self.FakeEngineId2PlayerId[engineId]
    local playerInfo = self.PlayerInfo and self.PlayerInfo[id]
    local BuffInfo = self.PlayerInfo and self.PlayerInfo[id] and self.PlayerInfo[id].BuffInfo
    if self.PlayerInfo and self.PlayerInfo[id] and self.PlayerInfo[id].IsOut then return end
    if obj and BuffInfo then
        local expressid = 0
        for k,v in pairs(BuffInfo) do
            if v and v > 0 then
                if self.BuffInfo[k] and not System.String.IsNullOrEmpty(self.BuffInfo[k].DoAni) then
                    expressid = tonumber(self.BuffInfo[k].DoAni)
                end
                local fxID = self.BuffInfo[k] and self.BuffInfo[k].FxID or 0
                if fxID and fxID ~= 0 then
                    local fx = CEffectMgr.Inst:AddObjectFX(fxID, obj.RO, 0, 1, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
                    obj.RO:AddFX("GodBuffFx", fx)
                end
            end
        end
        if self.CurStage == EnumDaFuWengStage.RoundCardAnimation and id == self.Index2Id[self.RoundInfo.select] and self.RoundInfo.cardId == 4 or 
            self.CurStage == EnumDaFuWengStage.RandomEventDog and id == self.CurPlayerRound then    -- 进药铺动画优化

        else
            if not self.AniTick then self.AniTick = {} end
            if self.AniTick[engineId] then UnRegisterTick(self.AniTick[engineId]) self.AniTick[engineId] = nil end
            self.AniTick[engineId] = RegisterTickOnce(function()
                local obj = CClientObjectMgr.Inst and CClientObjectMgr.Inst:GetObject(engineId)
                if obj then
                    if expressid ~= 0 then
                        obj:ShowActionState(EnumActionState.Expression, {expressid})
                    else
                        obj:ShowActionState(EnumActionState.Idle, {})
                    end
                end
            end,100)
        end
        -- if id ~= 0 then
        --     obj:ShowActionState(EnumActionState.Expression, {id})
        -- else
        --     obj:ShowActionState(EnumActionState.Idle, {})
        -- end
    end
    if obj and playerInfo then  -- 阵营特效
        local fxID = DaFuWeng_Setting.GetData().PlayerRingFx[playerInfo.round - 1]
        local fx = CEffectMgr.Inst:AddObjectFX(fxID, obj.RO, 0, 1, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
        obj.RO:AddFX(fxID, fx)
    end
    if not self.OutlineInit then self.OutlineInit = {} end
    if id then self.OutlineInit[id] = true end
    
end

LuaDaFuWongMgr.CurBuyVoicePackageId = nil
function LuaDaFuWongMgr:ShowBuyVoicePackageWnd(id)
    self.CurBuyVoicePackageId = id
    CUIManager.ShowUI(CLuaUIResources.DaFuWongBuyVoicePackageWnd)
end
function LuaDaFuWongMgr:GetHeadBgPath(index)
    local nameList = {"dafuwongmainplaywnd_wanjiaxinxi_yellow_touxiang_01","dafuwongmainplaywnd_wanjiaxinxi_red_touxiang_01","dafuwongmainplaywnd_wanjiaxinxi_purple_touxiang_01","dafuwongmainplaywnd_wanjiaxinxi_green_touxiang_01"}
    return SafeStringFormat3("UI/Texture/FestivalActivity/Festival_DaFuWong/Material/%s.mat",nameList[index])
end

function LuaDaFuWongMgr:GetNameBgPath(index)
    local nameList = {"dafuwongmainplaywnd_wanjiaxinxi_bg_yellow","dafuwongmainplaywnd_wanjiaxinxi_bg_red","dafuwongmainplaywnd_wanjiaxinxi_bg_purple","dafuwongmainplaywnd_wanjiaxinxi_bg_green"}
    return SafeStringFormat3("UI/Texture/FestivalActivity/Festival_DaFuWong/Material/%s.mat",nameList[index])
end

function LuaDaFuWongMgr:GetTipsString(landId)
	local result = CreateFromClass(MakeGenericClass(List, String))
	local Land = self.LandInfo and self.LandInfo[landId]
	local series = Land  and Land.series
	local seriesData = self.SeriesInfo[series]
	if not seriesData then return result end
	local seriesList = NewStringBuilderWraper(StringBuilder)
	for i = 0,seriesData.lands.Length - 1 do
		local landdata = DaFuWeng_Land.GetData(seriesData.lands[i])
		if landdata then
			if seriesData.lands.Length > 1 and i == seriesData.lands.Length - 1 then seriesList:Append(LocalString.GetString("和"))
			elseif i ~= 0 then seriesList:Append(LocalString.GetString("、"))  end
			seriesList:Append(landdata.Name)
		end
	end
	local seriesDes = seriesData.desc
	local curSeries = NewStringBuilderWraper(StringBuilder)
	local id = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id
	local playData = self.PlayerInfo[id]
	if playData then
		local count = 0
		for k,v in pairs(playData.LandsInfo) do
			if v then
				local otherland = self.LandInfo and self.LandInfo[k]
				local data = DaFuWeng_Land.GetData(k)
				if data and otherland and otherland.series == Land.series then
					count = count + 1
					if count > 1 then curSeries:Append(LocalString.GetString("、"))  end
					curSeries:Append(data.Name)
				end
			end
		end
		if count <= 0 then
			curSeries:Append(LocalString.GetString("无"))
		end
	end

	local firstP = SafeStringFormat3(LocalString.GetString("%s系列的地块包括了：%s。%s"),seriesData.name,seriesList:ToString(),seriesDes)
	local secondP = SafeStringFormat3(LocalString.GetString("当前拥有%s地块：%s"),seriesData.name,curSeries:ToString())
	CommonDefs.ListAdd(result, typeof(String), firstP)
	CommonDefs.ListAdd(result, typeof(String), secondP)
	return result
end

function LuaDaFuWongMgr:GetTongXingZhengDataInfo()
    if not LuaDaFuWongMgr.TongXingZhengInfo then return nil end
    local scoreInfo = self.TongXingZhengInfo
    local maxLv = DaFuWeng_Setting.GetData().MaxBuyLevel
    local isMaxLv = scoreInfo.lv == maxLv
    local maxExp = isMaxLv and 0 or self.TongXingZengLevelToExp[math.min(scoreInfo.lv + 1,maxLv)].progress
    local lv = scoreInfo.lv
    local curExp = scoreInfo.curExp
    local hasDailyTask = false
    if self.DailyTaskInfo then 
        for k,v in pairs(self.DailyTaskInfo) do
            if not v.r then hasDailyTask = true break end
        end
    end
    local res = {
        lv = lv,    -- 当前等级
        curExp = curExp,  -- 当前经验
        maxExp = maxExp,    -- 当前等级最大经验
        isMaxLv = isMaxLv,  -- 是否满级
        hasDailyTask = hasDailyTask,    -- 是否有未完成的每日任务
    }
    return res
end

function LuaDaFuWongMgr:OnPlayerLogin()
    LuaDaFuWongMgr.TongXingZhengInfo = nil  
    LuaDaFuWongMgr.DailyTaskInfo = nil      
    LuaDaFuWongMgr.RulePage = 1             
    LuaDaFuWongMgr.PlayResult = nil         
    LuaDaFuWongMgr.MatchStatus = false
    LuaDaFuWongMgr.MatchTime = 0
    LuaDaFuWongMgr.ShowRewardInfo = nil
    LuaDaFuWongMgr.TongXingZengLevelToExp = nil
    LuaDaFuWongMgr.VoicePackage = nil
end