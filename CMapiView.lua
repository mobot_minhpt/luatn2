-- Auto Generated!!
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CFreightEquipSubmitMgr = import "L10.UI.CFreightEquipSubmitMgr"
local CIndirectUIResources = import "L10.UI.CIndirectUIResources"
local CInputBoxMgr = import "L10.UI.CInputBoxMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CLingShouMgr = import "L10.Game.CLingShouMgr"
local CMapiView = import "L10.UI.CMapiView"
local CMapiViewSimpleItem = import "L10.UI.CMapiViewSimpleItem"
local Color = import "UnityEngine.Color"
local CommonDefs = import "L10.Game.CommonDefs"
local CSelectMapiWnd = import "L10.UI.CSelectMapiWnd"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIDirectories = import "L10.UI.CUIDirectories"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local CZuoQiEquipProp = import "L10.Game.CZuoQiEquipProp"
local CZuoQiMgr = import "L10.Game.CZuoQiMgr"
local DelegateFactory = import "DelegateFactory"
local Double = import "System.Double"
local EnumEventType = import "EnumEventType"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local Gac2Gas = import "L10.Game.Gac2Gas"
local Int32 = import "System.Int32"
local Item_Item = import "L10.Game.Item_Item"
local LingShouDetails = import "L10.Game.CLingShouBaseMgr+LingShouDetails"
local LocalString = import "LocalString"
local MapiSelectWndType = import "L10.UI.MapiSelectWndType"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local UInt32 = import "System.UInt32"
local Vector3 = import "UnityEngine.Vector3"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local ZuoQi_MapiSkill = import "L10.Game.ZuoQi_MapiSkill"
local ZuoQi_MapiUpgrade = import "L10.Game.ZuoQi_MapiUpgrade"
local ZuoQi_Setting = import "L10.Game.ZuoQi_Setting"
local ZuoQi_ZuoQi = import "L10.Game.ZuoQi_ZuoQi"
local ZuoQiData = import "L10.Game.ZuoQiData"
CMapiView.m_Init_CS2LuaHook = function (this) 
    local list = CreateFromClass(MakeGenericClass(List, ZuoQiData))
    do
        local i = 0
        while i < CZuoQiMgr.Inst.zuoqis.Count do
            local templateId = CZuoQiMgr.Inst.zuoqis[i].templateId
            if CZuoQiMgr.IsMapiTemplateId(templateId) then
                CommonDefs.ListAdd(list, typeof(ZuoQiData), CZuoQiMgr.Inst.zuoqis[i])
            end
            i = i + 1
        end
    end

    local setting = ZuoQi_Setting.GetData()
    local zqtemplateId = setting.MapiTemplateId
    local data = ZuoQi_ZuoQi.GetData(zqtemplateId)
    if data == nil then
        return
    end

    CommonDefs.DictClear(this.ItemDic)
    this.mapiViewTemplate:SetActive(false)
    Extensions.RemoveAllChildren(this.zuoqiGrid.transform)
    local maxMapiCount = setting.MapiMaxCount
    local majiuMapiCount = CZuoQiMgr.Inst:GetMajiuMapiCount()

    do
        local i = 0
        while i < maxMapiCount + majiuMapiCount do
            local icon = nil
            local showAdd = true
            local name = ""
            local level = 0
            local textureAction = nil
            local addAction = nil
            local id = tostring(i)
            if i < list.Count then
                local zqdata = list[i]
                if zqdata.mapiInfo ~= nil then
                    icon = setting.MapiIcon[zqdata.mapiInfo.FuseId]
                    if zqdata.quality == 4 then
                        icon = setting.SpecialMapiIcon[0]
                    elseif zqdata.quality == 5 then
						icon = setting.SpecialMapiIcon[1]
					end
                end
                name = zqdata.mapiName
                level = zqdata.mapiLevel
                showAdd = false
                id = zqdata.id
                local closureId = zqdata.id
                textureAction = DelegateFactory.Action(function () 
                    this:ShowMapi(closureId)
                end)
            else
                addAction = DelegateFactory.Action(function () 
                    CUIManager.ShowUI(CUIResources.SelectAddMapiWnd)
                end)
            end


            local item = CommonDefs.Object_Instantiate(this.mapiViewTemplate)
            item.transform.parent = this.zuoqiGrid.transform
            item.transform.localScale = Vector3.one
            item.gameObject:SetActive(true)

            local simpleItem = CommonDefs.GetComponent_GameObject_Type(item, typeof(CMapiViewSimpleItem))
            CommonDefs.DictAdd(this.ItemDic, typeof(String), id, typeof(CMapiViewSimpleItem), simpleItem)
            simpleItem:Init(icon, name, level, showAdd, nil, addAction, textureAction, nil)
            i = i + 1
        end
    end
    this.zuoqiGrid:Reposition()
    this.zuoqiScrollView:ResetPosition()

    if list.Count ~= 0 then
        local zuoqiId = list[0].id
        if CZuoQiMgr.Inst.curSelectedZuoqiId ~= nil and CZuoQiMgr.Inst.curSelectedZuoqiTid == setting.MapiTemplateId then
            zuoqiId = CZuoQiMgr.Inst.curSelectedZuoqiId
        end
        this:ShowMapi(zuoqiId)
        this.detailGO:SetActive(true)
    else
        g_MessageMgr:ShowMessage("Mapi_View_No_Mapi_Found")
        this.detailGO:SetActive(false)
    end
end
CMapiView.m_ShowMapi_CS2LuaHook = function (this, zuoqiId) 
    if CommonDefs.StringLength(zuoqiId) < 10 then
        return
    end

    CMapiView.sCurZuoqiId = zuoqiId
    CZuoQiMgr.Inst.curSelectedZuoqiId = zuoqiId

    CommonDefs.DictIterate(this.ItemDic, DelegateFactory.Action_object_object(function (___key, ___value) 
        local p = {}
        p.Key = ___key
        p.Value = ___value
        p.Value:SetSelected((p.Key == zuoqiId))
    end))

    Gac2Gas.QueryMapiDetail(zuoqiId)
    Gac2Gas.QueryMapiPinfen(zuoqiId)
end
CMapiView.m_OnEnable_CS2LuaHook = function (this) 
    EventManager.AddListenerInternal(EnumEventType.OnSyncMapiInfo, MakeDelegateFromCSFunction(this.OnSyncMapiInfo, MakeGenericClass(Action1, String), this))
    EventManager.AddListenerInternal(EnumEventType.OnMapiShouhuLingshouChanged, MakeDelegateFromCSFunction(this.OnMapiShouhuLingshouChanged, MakeGenericClass(Action1, String), this))
    EventManager.AddListenerInternal(EnumEventType.GetLingShouDetails, MakeDelegateFromCSFunction(this.GetLingShouDetails, MakeGenericClass(Action2, String, LingShouDetails), this))
    EventManager.AddListener(EnumEventType.OnRideOnOffVehicle, MakeDelegateFromCSFunction(this.OnRideOnOffVehicle, Action0, this))
    EventManager.AddListener(EnumEventType.OnAddDeleteVehicle, MakeDelegateFromCSFunction(this.OnAddDeleteVehicle, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.OnSendMapiPinfen, MakeDelegateFromCSFunction(this.OnSendMapiPinfen, MakeGenericClass(Action2, String, UInt32), this))
    EventManager.AddListenerInternal(EnumEventType.OnFreightSubmitEquipSelected, MakeDelegateFromCSFunction(this.LearnMapiSkill, MakeGenericClass(Action2, String, String), this))
    EventManager.AddListenerInternal(EnumEventType.OnSendMajiuMapiEquipList, MakeDelegateFromCSFunction(this.OnSendMajiuMapiEquipList, MakeGenericClass(Action1, String), this))

    UIEventListener.Get(this.expBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.expBtn).onClick, MakeDelegateFromCSFunction(this.OnExpBtnClicked, VoidDelegate, this), true)
    UIEventListener.Get(this.lifeBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.lifeBtn).onClick, MakeDelegateFromCSFunction(this.OnLifeBtnClicked, VoidDelegate, this), true)
    UIEventListener.Get(this.fangshengBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.fangshengBtn).onClick, MakeDelegateFromCSFunction(this.OnFangshengBtnClicked, VoidDelegate, this), true)
    UIEventListener.Get(this.shouhuBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.shouhuBtn).onClick, MakeDelegateFromCSFunction(this.OnShouhuBtnClicked, VoidDelegate, this), true)
    UIEventListener.Get(this.shangmaBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.shangmaBtn).onClick, MakeDelegateFromCSFunction(this.OnShangmaBtnClicked, VoidDelegate, this), true)
    UIEventListener.Get(this.renameBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.renameBtn).onClick, MakeDelegateFromCSFunction(this.OnRenameBtnClicked, VoidDelegate, this), true)
    UIEventListener.Get(this.questionBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.questionBtn).onClick, MakeDelegateFromCSFunction(this.OnQuestionBtnClick, VoidDelegate, this), true)
    UIEventListener.Get(this.genderSprite.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.genderSprite.gameObject).onClick, MakeDelegateFromCSFunction(this.OnGenderSpriteClick, VoidDelegate, this), true)
end
CMapiView.m_OnDisable_CS2LuaHook = function (this) 
    EventManager.RemoveListenerInternal(EnumEventType.OnSyncMapiInfo, MakeDelegateFromCSFunction(this.OnSyncMapiInfo, MakeGenericClass(Action1, String), this))
    EventManager.RemoveListenerInternal(EnumEventType.OnMapiShouhuLingshouChanged, MakeDelegateFromCSFunction(this.OnMapiShouhuLingshouChanged, MakeGenericClass(Action1, String), this))
    EventManager.RemoveListenerInternal(EnumEventType.GetLingShouDetails, MakeDelegateFromCSFunction(this.GetLingShouDetails, MakeGenericClass(Action2, String, LingShouDetails), this))
    EventManager.RemoveListener(EnumEventType.OnRideOnOffVehicle, MakeDelegateFromCSFunction(this.OnRideOnOffVehicle, Action0, this))
    EventManager.RemoveListener(EnumEventType.OnAddDeleteVehicle, MakeDelegateFromCSFunction(this.OnAddDeleteVehicle, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.OnSendMapiPinfen, MakeDelegateFromCSFunction(this.OnSendMapiPinfen, MakeGenericClass(Action2, String, UInt32), this))
    EventManager.RemoveListenerInternal(EnumEventType.OnFreightSubmitEquipSelected, MakeDelegateFromCSFunction(this.LearnMapiSkill, MakeGenericClass(Action2, String, String), this))
    EventManager.RemoveListenerInternal(EnumEventType.OnSendMajiuMapiEquipList, MakeDelegateFromCSFunction(this.OnSendMajiuMapiEquipList, MakeGenericClass(Action1, String), this))

    UIEventListener.Get(this.expBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.expBtn).onClick, MakeDelegateFromCSFunction(this.OnExpBtnClicked, VoidDelegate, this), false)
    UIEventListener.Get(this.lifeBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.lifeBtn).onClick, MakeDelegateFromCSFunction(this.OnLifeBtnClicked, VoidDelegate, this), false)
    UIEventListener.Get(this.fangshengBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.fangshengBtn).onClick, MakeDelegateFromCSFunction(this.OnFangshengBtnClicked, VoidDelegate, this), false)
    UIEventListener.Get(this.shouhuBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.shouhuBtn).onClick, MakeDelegateFromCSFunction(this.OnShouhuBtnClicked, VoidDelegate, this), false)
    UIEventListener.Get(this.renameBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.renameBtn).onClick, MakeDelegateFromCSFunction(this.OnRenameBtnClicked, VoidDelegate, this), false)
    UIEventListener.Get(this.shangmaBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.shangmaBtn).onClick, MakeDelegateFromCSFunction(this.OnShangmaBtnClicked, VoidDelegate, this), false)
    UIEventListener.Get(this.questionBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.questionBtn).onClick, MakeDelegateFromCSFunction(this.OnQuestionBtnClick, VoidDelegate, this), false)
    UIEventListener.Get(this.genderSprite.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.genderSprite.gameObject).onClick, MakeDelegateFromCSFunction(this.OnGenderSpriteClick, VoidDelegate, this), false)

    CMapiView.sCurZuoqiId = nil
    CMapiView.sbOpenFromNpc = false
end
CMapiView.m_OnSendMajiuMapiEquipList_CS2LuaHook = function (this, zuoqiId) 
    this:ShowEquipList(zuoqiId, true)
end
CMapiView.m_OnSendMapiPinfen_CS2LuaHook = function (this, zuoqiId, pinfen) 
    if zuoqiId == CMapiView.sCurZuoqiId then
        this.gradeLabel.text = tostring(pinfen)
    end
end
CMapiView.m_OnRideOnOffVehicle_CS2LuaHook = function (this) 
    if CClientMainPlayer.Inst == nil then
        return
    end

    local curZuoQi = CClientMainPlayer.Inst.BasicProp.CurrentZuoQiId

    if curZuoQi == CMapiView.sCurZuoqiId then
        CommonDefs.GetComponentInChildren_GameObject_Type(this.shangmaBtn, typeof(UILabel)).text = LocalString.GetString("下马")
    else
        CommonDefs.GetComponentInChildren_GameObject_Type(this.shangmaBtn, typeof(UILabel)).text = LocalString.GetString("上马")
    end
end
CMapiView.m_OnShangmaBtnClicked_CS2LuaHook = function (this, go) 
    if CClientMainPlayer.Inst == nil then
        return
    end

    if CMapiView.sCurZuoqiId ~= nil then
        if not CZuoQiMgr.Inst:CheckMapiNotInMajiu(CMapiView.sCurZuoqiId) then
            return
        end

        if CMapiView.sCurZuoqiId == CClientMainPlayer.Inst.BasicProp.CurrentZuoQiId then
            Gac2Gas.RequestRideOffZuoQi(CMapiView.sCurZuoqiId)
        else
            Gac2Gas.RequestRideOnZuoQi(CMapiView.sCurZuoqiId)
        end
    end
end
CMapiView.m_OnRenameBtnClicked_CS2LuaHook = function (this, go) 
    if CMapiView.sCurZuoqiId == nil then
        return
    end

    if not CZuoQiMgr.Inst:CheckMapiNotInMajiu(CMapiView.sCurZuoqiId) then
        return
    end

    local uplimit = 7
    local content = System.String.Format(LocalString.GetString("请输入新的马匹名字，最多输入{0}个字符({1}个汉字)"), uplimit * 2, uplimit)

    if CommonDefs.IsSeaMultiLang() and (LocalString.language == "en" or LocalString.language == "ina") then
        content = System.String.Format(LocalString.GetString("请输入新的马匹名字，最多输入{0}个字母"), uplimit * 2)
    end

    CInputBoxMgr.ShowInputBox(content, DelegateFactory.Action_string(function (str) 
        if CUICommonDef.GetStrByteLength(str) > uplimit * 2 then
            g_MessageMgr:ShowMessage("Mapi_Rename_Too_Long")
            return
        elseif System.String.IsNullOrEmpty(str) then
            g_MessageMgr:ShowMessage("Mapi_Rename_Null")
            return
        elseif not CWordFilterMgr.Inst:CheckName(str, LocalString.GetString("马匹名")) then
            g_MessageMgr:ShowMessage("Mapi_Rename_Violation")
            return
        end

        Gac2Gas.RequestRenameMapi(CMapiView.sCurZuoqiId, str)
        CUIManager.CloseUI(CIndirectUIResources.InputBox)
    end), uplimit * 5, true, nil, nil)
    --名字输入时不做限制
end
CMapiView.m_OnMapiShouhuLingshouChanged_CS2LuaHook = function (this, zuoqiId) 
    if CMapiView.sCurZuoqiId == zuoqiId then
        local zqdata = CZuoQiMgr.Inst:GetZuoQiById(zuoqiId)
        if zqdata ~= nil and zqdata.mapiInfo ~= nil then
            local shouhuCount = 0
            do
                local i = 0
                while i < zqdata.mapiInfo.ShouhuLingshou.Length do
                    if not System.String.IsNullOrEmpty(zqdata.mapiInfo.ShouhuLingshou[i]) then
                        shouhuCount = shouhuCount + 1
                    end
                    i = i + 1
                end
            end
            this.lingshouNameLabel.text = System.String.Format(LocalString.GetString("{0}/2"), shouhuCount)

            this:ShowMapiTexture()
        end
    end
end
CMapiView.m_OnExpBtnClicked_CS2LuaHook = function (this, go) 

    if CMapiView.sCurZuoqiId == nil then
        return
    end
    local zqdata = CZuoQiMgr.Inst:GetZuoQiById(CMapiView.sCurZuoqiId)
    if zqdata == nil or zqdata.mapiInfo == nil then
        return
    end

    if not CZuoQiMgr.Inst:CheckMapiNotInMajiu(CMapiView.sCurZuoqiId) then
        return
    end

    if zqdata.mapiInfo.Level > 0 and ZuoQi_MapiUpgrade.GetData(zqdata.mapiInfo.Level + 1) == nil then
        g_MessageMgr:ShowMessage("Mapi_Add_Exp_Fail_Level_Full")
        return
    end

    CSelectMapiWnd.sType = MapiSelectWndType.eSelectExpItem
    CUIManager.ShowUI(CUIResources.SelectMapiWnd)
end
CMapiView.m_OnLifeBtnClicked_CS2LuaHook = function (this, go) 
    if CMapiView.sCurZuoqiId == nil then
        return
    end
    if not CZuoQiMgr.Inst:CheckMapiNotInMajiu(CMapiView.sCurZuoqiId) then
        return
    end
    local zqdata = CZuoQiMgr.Inst:GetZuoQiById(CMapiView.sCurZuoqiId)
    local setting = ZuoQi_Setting.GetData()
    if zqdata.mapiInfo.Life > setting.MapiUseLifeItemThresh then
        g_MessageMgr:ShowMessage("Mapi_Add_Life_Too_Young")
        return
    end
    
    CSelectMapiWnd.sType = MapiSelectWndType.eSelectLifeItem
    CUIManager.ShowUI(CUIResources.SelectMapiWnd)
end
CMapiView.m_LearnSkillFromPackage_CS2LuaHook = function (this) 
    if not CZuoQiMgr.Inst:CheckMapiNotInMajiu(CMapiView.sCurZuoqiId) then
        return
    end

    local bookIds = CreateFromClass(MakeGenericClass(List, UInt32))
    local mapiSkillBookIds = ZuoQi_Setting.GetData().MapiSkillBookIds
    do
        local i = 0 local cnt = mapiSkillBookIds.Length
        while i < cnt do
            CommonDefs.ListAdd(bookIds, typeof(UInt32), mapiSkillBookIds[i][0])
            i = i + 1
        end
    end
    local dic = CreateFromClass(MakeGenericClass(Dictionary, Int32, MakeGenericClass(List, String)))
    local equipList = CreateFromClass(MakeGenericClass(List, String))
    local bagSize = CClientMainPlayer.Inst.ItemProp:GetPlaceSize(EnumItemPlace.Bag)
    do
        local i = 1
        while i <= bagSize do
            local id = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Bag, i)
            if not System.String.IsNullOrEmpty(id) then
                local equip = CItemMgr.Inst:GetById(id)
                if equip ~= nil and CommonDefs.ListContains(bookIds, typeof(UInt32), equip.TemplateId) then
                    CommonDefs.ListAdd(equipList, typeof(String), id)
                end
            end
            i = i + 1
        end
    end
    CommonDefs.DictAdd(dic, typeof(Int32), 0, typeof(MakeGenericClass(List, String)), equipList)
    CFreightEquipSubmitMgr.OpenEquipmentChooseWnd("MapiSkill", dic, LocalString.GetString("包裹中马匹技能书"), 0, false, LocalString.GetString("选择"), LocalString.GetString("获得更多马匹技能书"), DelegateFactory.Action(function () 
        g_MessageMgr:ShowMessage("Get_More_Mapi_Skill")
    end), "")
end
CMapiView.m_ShowMapiInfo_CS2LuaHook = function (this, zuoqiId, mapiInfo, bIsMajiuMapi) 
    if CMapiView.sCurZuoqiId ~= zuoqiId then
        return
    end

    this.nameLabel.text = mapiInfo.Name
    this.genderSprite.spriteName = (mapiInfo.Gender == EnumMapiGender_lua.eMale) and "lingshoujiehun_yang" or "lingshoujiehun_yin"
    this.levelLebel.text = System.String.Format("Lv.{0}", mapiInfo.Level)
    this.generationLabel.text = System.String.Format(LocalString.GetString("{0}代"), mapiInfo.Generation)
    local newChicun = mapiInfo.Chicun * ZuoQi_Setting.GetData().MapiHight
    this.shengaoLabel.text = NumberComplexToString(newChicun, typeof(Double), "#0.00")

    local appearanceName = CZuoQiMgr.GetAppearanceName(mapiInfo.PinzhongId, mapiInfo.Quality)
    local attrName = CZuoQiMgr.GetAttributeName(mapiInfo.AttributeNameId)
    this.pinzhongLabel.text = System.String.Format(LocalString.GetString("{0}·{1}"), appearanceName, attrName)
    this.pinzhongLabel.color = CZuoQiMgr.GetMapiColor(mapiInfo.Quality)
    local pzLabel = CommonDefs.GetComponent_Component_Type(this.pinzhongLabel.transform:Find("label"), typeof(UILabel))
    pzLabel.color = CZuoQiMgr.GetMapiColor(mapiInfo.Quality)

    local shouhuCount = 0
    do
        local i = 0
        while i < mapiInfo.ShouhuLingshou.Length do
            if not System.String.IsNullOrEmpty(mapiInfo.ShouhuLingshou[i]) then
                shouhuCount = shouhuCount + 1
            end
            i = i + 1
        end
    end
    this.lingshouNameLabel.text = System.String.Format(LocalString.GetString("{0}/2"), shouhuCount)

    local setting = ZuoQi_Setting.GetData()
    local upgradeData = ZuoQi_MapiUpgrade.GetData(mapiInfo.Level)
    if upgradeData ~= nil then
        this.expLabel.text = System.String.Format("{0}/{1}", mapiInfo.Exp, upgradeData.Exp)
        this.expSlider.value = math.max(0, math.min(1, mapiInfo.Exp / upgradeData.Exp))
    end

    local lifeTxt = ""
    local hour = math.floor(mapiInfo.Life / 3600)
    if hour > 0 then
        lifeTxt = lifeTxt .. (hour .. LocalString.GetString("时"))
    end
    local min = math.floor((mapiInfo.Life % 3600) / 60)
    if min > 0 then
        lifeTxt = lifeTxt .. (min .. LocalString.GetString("分"))
    end
    if lifeTxt == "" then
        lifeTxt = lifeTxt .. (mapiInfo.Life % 60 .. LocalString.GetString("秒"))
    end
    this.lifeLabel.text = lifeTxt
    this.lifeSlider.value = math.max(0, math.min(1, mapiInfo.Life / setting.MapiLife))
    this:ShowLifeDesc(math.max(0, math.min(1, mapiInfo.Life / setting.MapiLife)))

    local quality = mapiInfo.Quality


    this.suduLabel.text = tostring(mapiInfo.Speed)
    this.lingqiaoLabel.text = tostring(mapiInfo.Lingqiao)
    this.nailiLabel.text = tostring(mapiInfo.Naili)
    this.caodishiyingLabel.text = tostring(mapiInfo.CaodiShiying)
    this.nidishiyingLabel.text = tostring(mapiInfo.NidiShiying)
    this.yandishiyingLabel.text = tostring(mapiInfo.YandiShiying)

    local openHole = 0
    local mapiUpgradeData = ZuoQi_MapiUpgrade.GetData(mapiInfo.Level)
    if mapiUpgradeData ~= nil then
        openHole = mapiUpgradeData.Hole
    end
    do
        local i = 0
        while i < this.SkillList.Length do
            local item = this.SkillList[i]
            local skillIcon = nil
            local showAdd = true
            local textureAction = nil
            if i + 1 < mapiInfo.Skills.Length then
                local skillId = mapiInfo.Skills[i + 1]
                local skill = ZuoQi_MapiSkill.GetData(skillId)
                if skill ~= nil then
                    skillIcon = (CUIDirectories.SkillIconsMaterialDir .. skill.Icon) .. ".mat"
                    showAdd = false

                    textureAction = DelegateFactory.Action(function () 
                        CSkillInfoMgr.ShowSkillInfoWnd(skillId, item.transform.position, CSkillInfoMgr.EnumSkillInfoContext.MapiSkill, true, 0, 0, nil)
                    end)
                end
            end
            item:Init(skillIcon, "", 0, showAdd, nil, MakeDelegateFromCSFunction(this.LearnSkillFromPackage, Action0, this), textureAction, nil)
            i = i + 1
        end
    end

    CZuoQiMgr.Inst.curSelectedZuoqiId = zuoqiId
    CZuoQiMgr.Inst.curSelectedZuoqiTid = setting.MapiTemplateId

    this:ShowEquipList(zuoqiId, bIsMajiuMapi)

    this:OnRideOnOffVehicle()
end
CMapiView.m_ShowEquipList_CS2LuaHook = function (this, zuoqiId, bIsMajiuMapi) 
    if CClientMainPlayer.Inst ~= nil then
        local zqdata = CZuoQiMgr.Inst:GetZuoQiById(zuoqiId)
        if zqdata == nil or zqdata.mapiInfo == nil then
            return
        end

        local prop = nil
        if bIsMajiuMapi then
            if zqdata ~= nil and zqdata.majiuMapiEquipList ~= nil and zqdata.majiuMapiEquipList.Count == 6 then
                prop = CreateFromClass(CZuoQiEquipProp)
                for i = 0, 5 do
                    prop.Equip[i + 1] = zqdata.majiuMapiEquipList[i]
                end
            end
        else
            (function () 
                local __try_get_result
                __try_get_result, prop = CommonDefs.DictTryGet(CClientMainPlayer.Inst.ItemProp.ZuoQiEquip, typeof(String), zuoqiId, typeof(CZuoQiEquipProp))
                return __try_get_result
            end)()
        end
        do
            local i = 0
            while i < this.EquipList.Length do
                local item = this.EquipList[i]

                local equipIcon = nil
                local showAdd = true
                local lockAction = nil
                local addAction = nil
                local textureAction = nil
                local qualitySpriteName = nil
                local needGrade = CZuoQiMgr.GetEquipHoleNeedMapiGrade(i + 1)
                if prop ~= nil and i < prop.Equip.Length and zqdata.mapiInfo.Level >= needGrade then
                    local equipId = prop.Equip[i + 1]
                    local equip = CItemMgr.Inst:GetById(equipId)
                    if equip ~= nil then
                        local itemData = Item_Item.GetData(equip.TemplateId)
                        if itemData ~= nil then
                            equipIcon = itemData.Icon
                            showAdd = false
                            textureAction = DelegateFactory.Action(function () 
                                CItemInfoMgr.ShowLinkItemInfo(equipId, true, nil, AlignType.Default, 0, 0, 0, 0)
                            end)

                            qualitySpriteName = CUICommonDef.GetItemCellBorder(itemData, equip, false)
                        end
                    end
                end
                if zqdata.mapiInfo.Level < needGrade then
                    showAdd = false
                    lockAction = DelegateFactory.Action(function () 
                        g_MessageMgr:ShowMessage("Mapi_Open_Hole_Need_Level", needGrade)
                    end)
                else
                    addAction = DelegateFactory.Action(function () 
                    end)
                end
                item:Init(equipIcon, "", 0, showAdd, lockAction, addAction, textureAction, qualitySpriteName)
                i = i + 1
            end
        end
    end
end
CMapiView.m_ShowLifeDesc_CS2LuaHook = function (this, percent) 
    local c = Color.white
    local desc = ""
    if percent >= 0.5 then
        c = Color(0, 1, 0)
        desc = LocalString.GetString("（身强体壮）")
    elseif percent >= 0.25 and percent < 0.5 then
        c = Color(1, 0.784, 0)
        desc = LocalString.GetString("（老当益壮）")
    elseif percent > 0 and percent < 0.25 then
        c = Color(1, 0.314, 0.314)
        desc = LocalString.GetString("（垂垂老矣）")
    else
        c = Color(0.5, 0.5, 0.5)
        desc = LocalString.GetString("（失魂）")
    end
    this.lifeDescLabel.text = desc
    this.lifeDescLabel.color = c
end
CMapiView.m_OnSyncMapiInfo_CS2LuaHook = function (this, zuoqiId) 

    local bIsInMajiu = false
    local mapiInfo = nil
    do
        local i = 0
        while i < CZuoQiMgr.Inst.zuoqis.Count do
            if zuoqiId == CZuoQiMgr.Inst.zuoqis[i].id then
                mapiInfo = CZuoQiMgr.Inst.zuoqis[i].mapiInfo
                bIsInMajiu = CZuoQiMgr.Inst.zuoqis[i].isMajiuMapi
                break
            end
            i = i + 1
        end
    end

    if mapiInfo ~= nil then
        if CMapiView.sCurZuoqiId == zuoqiId then
            this:ShowMapiInfo(zuoqiId, mapiInfo, bIsInMajiu)

            this:ShowMapiTexture()
        end
        local item = nil
        if (function () 
            local __try_get_result
            __try_get_result, item = CommonDefs.DictTryGet(this.ItemDic, typeof(String), zuoqiId, typeof(CMapiViewSimpleItem))
            return __try_get_result
        end)() then
            item:Reinit(mapiInfo)
        end
    end
end
CMapiView.m_ShowMapiTexture_CS2LuaHook = function (this) 
    CommonDefs.ListClear(this.mWaitLingshouList)
    local mapiInfo = nil
    do
        local i = 0
        while i < CZuoQiMgr.Inst.zuoqis.Count do
            if CMapiView.sCurZuoqiId == CZuoQiMgr.Inst.zuoqis[i].id then
                mapiInfo = CZuoQiMgr.Inst.zuoqis[i].mapiInfo
                break
            end
            i = i + 1
        end
    end

    if mapiInfo ~= nil then
        local detail1 = nil
        local lingshouId1 = mapiInfo.ShouhuLingshou[1]
        if not System.String.IsNullOrEmpty(lingshouId1) then
            detail1 = CLingShouMgr.Inst:GetLingShouDetails(lingshouId1)
            if detail1 == nil then
                CLingShouMgr.Inst:RequestLingShouDetails(lingshouId1)
                CommonDefs.ListAdd(this.mWaitLingshouList, typeof(String), lingshouId1)
            end
        end

        local detail2 = nil
        local lingshouId2 = mapiInfo.ShouhuLingshou[2]
        if not System.String.IsNullOrEmpty(lingshouId2) then
            detail2 = CLingShouMgr.Inst:GetLingShouDetails(lingshouId2)

            if detail2 == nil then
                CLingShouMgr.Inst:RequestLingShouDetails(lingshouId2)
                CommonDefs.ListAdd(this.mWaitLingshouList, typeof(String), lingshouId2)
            end
        end
        if this.mWaitLingshouList.Count == 0 then
            this.textureLoader:Init(mapiInfo, detail1, detail2, - 135, CMapiView.sCurZuoqiId)
        end
    end
end
CMapiView.m_GetLingShouDetails_CS2LuaHook = function (this, lingShouId, details) 
    if CommonDefs.ListContains(this.mWaitLingshouList, typeof(String), lingShouId) then
        CommonDefs.ListRemove(this.mWaitLingshouList, typeof(String), lingShouId)
    end
    if this.mWaitLingshouList.Count == 0 then
        this:ShowMapiTexture()
    end
end
